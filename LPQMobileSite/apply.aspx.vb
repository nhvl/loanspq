﻿Imports LPQMobile.BO
Imports LPQMobile.Utils
Imports System.IO
Imports System.Xml
Imports System.Web.Security.AntiXss
Partial Class _Default
    Inherits CBasePage
    Protected _LenderBaseUrl As String = ""
    Protected _LenderRef As String
    'Protected _LogoUrl As String
    Public _ThemeFileName As String
    Protected _hasXA As String = ""
    Protected _hasPL As String = ""
    Protected _hasVL As String = ""
    Protected _hasCC As String = ""
    Protected _hasHE As String = ""
    Protected _hasBL As String = ""
    Protected _hasSA As String = ""  'special XA , type=1s/2s
    Protected _hasBA As String = "" 'business XA , type=1b/2b
    Protected _URLParameterList As String = ""
    Protected _hasURLParameterList As String = "N"
    Protected _AvailableModules As String = ""
    'Protected _bgTheme As String = ""
    Protected _strHtmlLoans As String = ""
    Protected _hasLabelOption As Boolean
    Protected _iconContainerClass As String = ""
    Protected _loanHeader As String = "Apply today"
    Protected _xaHeader As String = ""
    Protected _statusHeader As String = ""
    Protected _statusFooter As String = ""
    Protected _noteFooter As String = ""
	''' <summary>
	''' Landing page
	''' Provide link bars or icons for redirect to actual module, via redirect to actual page or sslogin page
	''' </summary>
	''' <param name="sender"></param>
	''' <param name="e"></param>
	''' <remarks></remarks>
	Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
		_LogoUrl = Common.RenderLogo(_CurrentWebsiteConfig, _CurrentWebsiteConfig.LogoUrl)
		'Dim currentHostUrl As String = HttpContext.Current.Request.Url.Host
		Dim queryStr As String = HttpContext.Current.Request.Url.Query.safeQueryString
		Dim tokenUrl As String = getTokenUrl(queryStr)
		Dim defaultParas As String = getLoanParas(queryStr)
		
		'if not direct access then set platform_source value so it can be passed on to the SSOlogin page
		Dim referrer As Uri = Page.Request.UrlReferrer
		If Request("platform_source") <> "" Then 'pass from  ssologin
			tokenUrl &= "&platform_source=" & AntiXssEncoder.HtmlEncode(Request("platform_source"), True)	  'need to be here bc in sso mode most defaultParas is not used in the url
			log.Debug("reffer from ssologin: " & Request("platform_source"))
		ElseIf referrer IsNot Nothing Then
			log.Debug("reffer fro landing page: " & referrer.ToString)
			If referrer.ToString.ToUpper.Contains("DISSO") Then	 ''sso from digital
				defaultParas &= "&platform_source=DI"
			ElseIf Request.QueryString("Token") <> "" Then	 ''sso from other base on aasumption of a token
				defaultParas &= "&platform_source=SSO_OTHER"
			Else
				'don't care if it is redirected from non homebanking page
			End If
		Else
			'don't care for direct access
		End If

		_ThemeFileName = "<link rel='Stylesheet' href='/css/themes/default/" & Common.Theme_File(_CurrentWebsiteConfig) & "'/>"
		If (queryStr.Contains("list")) Then
			_hasURLParameterList = "Y"
		End If

		Dim visibleAttributes = readVisibleConfig()
		_hasLabelOption = Not (visibleAttributes.ContainsKey("label_option") AndAlso visibleAttributes("label_option").Trim() = "N")

		If Request.QueryString("list") IsNot Nothing Then
			'testcase
			'http://lpqmobile.localhost/apply.aspx?lenderref=lpquniversity_test&list=23%22%20%20onEvent=%22DX155532828Y2Z%20
			_URLParameterList = AntiXssEncoder.HtmlEncode(Request.QueryString("list"), True).NullSafeToUpper_
		End If
		If (Request.Params("lenderref").ToUpper <> _CurrentWebsiteConfig.LenderRef.ToUpper) Then Return

		_LenderRef = _CurrentWebsiteConfig.LenderRef
		_LenderBaseUrl = Request.Url.GetLeftPart(UriPartial.Authority)

		Dim layoutAttributes = readLayoutConfig()

		''need to  check if there exist loans in xml config->display the link 
		_hasXA = hasLoanTypes("XA_LOAN")
		_hasVL = hasLoanTypes("VEHICLE_LOAN")
		_hasCC = hasLoanTypes("CREDIT_CARD_LOAN")
		_hasPL = hasLoanTypes("PERSONAL_LOAN")
		_hasHE = hasLoanTypes("HOME_EQUITY_LOAN")
		_hasBL = hasLoanTypes("BUSINESS_LOAN")

		Dim htmlCC As String = ""
		Dim htmlHE As String = ""
		Dim htmlPL As String = ""
		Dim htmlVL As String = ""
		Dim htmlBL As String = ""
		Dim XMLNode2HtmlDic As New Dictionary(Of String, String)
		Dim UrlParamter2HtmlDic As New Dictionary(Of String, String)
		'Dim strLoanAttributes As String = " rel='external' data-role='button' data-theme='" & _FooterDataTheme & "' class='button-style div-continue-button'"
		Dim strLoanAttributes As String = " rel='external' data-role='button' type='button' class='button-style div-continue-button'"
		Dim strDivDefaultClass = ""
		Dim strDivOpenAccountStyle = ""

		'' Change headers
		Dim isChangeLoanHeader As Boolean = layoutAttributes.ContainsKey("landing_page_loan_header") AndAlso layoutAttributes("landing_page_loan_header").Trim() <> ""
		If isChangeLoanHeader Then
			_loanHeader = layoutAttributes("landing_page_loan_header")
		End If

		Dim isChangeXaHeader As Boolean = layoutAttributes.ContainsKey("landing_page_xa_header") AndAlso layoutAttributes("landing_page_xa_header").Trim() <> ""
		If isChangeXaHeader Then
			_xaHeader = layoutAttributes("landing_page_xa_header")
		End If

		Dim isChangeStatusHeader As Boolean = layoutAttributes.ContainsKey("landing_page_status_header") AndAlso layoutAttributes("landing_page_status_header").Trim() <> ""
		If isChangeStatusHeader Then
			_statusHeader = layoutAttributes("landing_page_status_header")
		End If

		Dim isChangeStatusFooter As Boolean = layoutAttributes.ContainsKey("landing_page_status_footer") AndAlso layoutAttributes("landing_page_status_footer").Trim() <> ""
		If isChangeStatusFooter Then
			_statusFooter = layoutAttributes("landing_page_status_footer")
		End If

		'' Change style if display icon
		Dim isIconDisplay As Boolean = layoutAttributes.ContainsKey("landing_page_design") AndAlso layoutAttributes("landing_page_design").ToLower() = "icon"
		If isIconDisplay Then
			strLoanAttributes = " rel='external' "
			strDivDefaultClass = " class='icon-button'"
			strDivOpenAccountStyle = ""	'" style='width: 105% !important'"
			_iconContainerClass = "icon-button-container"
		End If

		ucBusinessLoanSelector.Visible = False
		If _hasBL = "Y" Then
			ucBusinessLoanSelector.Visible = True
			ucBusinessLoanSelector.LenderRef = _LenderRef
			ucBusinessLoanSelector.ApplicantRoles = GetBusinessLoanApplicantRoles()
			ucBusinessLoanSelector.DefaultParams = defaultParas
			ucBusinessLoanSelector.EnableCreditCardLoan = CheckShowField("divCreditCardLoan", "", True) Or (IsInMode("777") AndAlso IsInFeature("visibility"))
			ucBusinessLoanSelector.EnableOtherLoan = CheckShowField("divOtherLoan", "", True) Or (IsInMode("777") AndAlso IsInFeature("visibility"))
			ucBusinessLoanSelector.EnableVehicleLoan = CheckShowField("divVehicleLoan", "", True) Or (IsInMode("777") AndAlso IsInFeature("visibility"))
			ucBusinessLoanSelector.EnableAppRole_P = CheckShowField("divAppRole_P", "", True) Or (IsInMode("777") AndAlso IsInFeature("visibility"))
			ucBusinessLoanSelector.EnableAppRole_C = CheckShowField("divAppRole_C", "", True) Or (IsInMode("777") AndAlso IsInFeature("visibility"))
			ucBusinessLoanSelector.EnableAppRole_G = CheckShowField("divAppRole_G", "", True) Or (IsInMode("777") AndAlso IsInFeature("visibility"))
			ucBusinessLoanSelector.EnableAppRole_S = CheckShowField("divAppRole_S", "", True) Or (IsInMode("777") AndAlso IsInFeature("visibility"))

			_AvailableModules += "BL"
			Dim blRename As String = "Business Loans"
			Dim bl_URL = " href='#' onclick='BL.FACTORY.openBusinessDialog()'"
			htmlBL += "<div id='divBL'" & strDivDefaultClass & strDivOpenAccountStyle & "><a" & bl_URL & strLoanAttributes & ">"
			htmlBL += blRename & "</a></div>"
			UrlParamter2HtmlDic.Add("BL", htmlBL)
			XMLNode2HtmlDic.Add("BUSINESS_LOAN", htmlBL)
		End If


		If _hasXA = "Y" Then
			_AvailableModules += "XA" + "SA"  'sa = secondary account
			_AvailableModules += "1S" + "2S"  'sepecial xa
			_AvailableModules += "1B" + "2B"  'business xa

			'' Add header
			Dim htmlXA As String = ""
			Dim htmlSA As String = ""
			If isIconDisplay = False And String.IsNullOrEmpty(_xaHeader) = False Then
				htmlXA += "<hr><div style='text-align:center'><h2>" & _xaHeader & "</h2></div>"
			End If

			Dim hasPersonalAccount As Boolean = If(_URLParameterList.Contains("XA") OrElse _URLParameterList.Contains("SA"), True, False)
			Dim hasSpecialAccount As Boolean = If(_URLParameterList.Contains("1S") OrElse _URLParameterList.Contains("2S"), True, False)
			Dim hasBusinessAccount As Boolean = If(_URLParameterList.Contains("1B") OrElse _URLParameterList.Contains("2B"), True, False)



			Dim hasPersonalAccountSecondary As Boolean = If(_URLParameterList.Contains("SA"), True, False)
			Dim hasSpecialAccountSecondary As Boolean = If(_URLParameterList.Contains("2S"), True, False)
			Dim hasBusinessAccountSecondary As Boolean = If(_URLParameterList.Contains("2B"), True, False)

			If IsInMode("777") Then
				'when is in 777 mode (APM design editor), force to show all options in order to allow APM user edit/update verbiage
				hasPersonalAccount = True
				hasSpecialAccount = True
				hasBusinessAccount = True
				hasPersonalAccountSecondary = True
				hasSpecialAccountSecondary = True
				hasBusinessAccountSecondary = True
			End If
			ucSpecialAccountSelector.Visible = False
			ucBusinessAccountSelector.Visible = False

			Dim xaBaseUrl = _LenderBaseUrl & "/xa/xpressApp.aspx?lenderref=" & _LenderRef
			Dim xaPersonalUrl As String = xaBaseUrl & "&type=1" & defaultParas
			Dim saPersonalUrl As String = xaBaseUrl & "&type=2" & defaultParas
			Dim xaPersonalAttr As String = " href='" & xaPersonalUrl & "'"
			Dim saPersonalAttr As String = " href='" & saPersonalUrl & "'"
			If Not String.IsNullOrEmpty(tokenUrl) Then
				xaPersonalAttr = " href='" & _LenderBaseUrl & tokenUrl & "&LoanType=xa'"
				saPersonalAttr = " href='" & _LenderBaseUrl & tokenUrl & "&LoanType=sa'"
			End If

			If hasSpecialAccount Then
				Dim specialAccountTypeList = Common.GetSpecialAccountTypeList(_CurrentWebsiteConfig)

				If specialAccountTypeList IsNot Nothing AndAlso specialAccountTypeList.Any() Then
					ucSpecialAccountSelector.Visible = True
					ucSpecialAccountSelector.SpecialAccountTypeList = specialAccountTypeList
					ucSpecialAccountSelector.LenderRef = _LenderRef
					ucSpecialAccountSelector.DefaultParams = defaultParas
					ucSpecialAccountSelector.ScenarioName = Common.SafeString(Request.QueryString("scenario"))
					ucSpecialAccountSelector.PreselectedAccountType = Common.SafeString(Request.QueryString("acctype"))
				ElseIf IsInMode("777") Then
					hasSpecialAccount = False  'download config doesn't have this account type, undo show in previos logic
					hasSpecialAccountSecondary = False
				End If
			End If

			If hasBusinessAccount Then
				Dim businessAccountTypeList = Common.GetBusinessAccountTypeList(_CurrentWebsiteConfig)
				'At least one role must be defined with a minimum value value greater than 0 (there must be at least 1 required role). Business account types that do not match this criteria do not appear as a choice for the consumer. 
				Dim hasBusinessAccounts As Boolean = False
				If businessAccountTypeList IsNot Nothing AndAlso businessAccountTypeList.Any() Then	'don't need(duplicate) AndAlso businessAccountTypeList.Any(Function(p) p.Roles IsNot Nothing AndAlso p.Roles.Count > 0 AndAlso p.Roles.Any(Function(s) s.InstanceMin > 0)
					ucBusinessAccountSelector.Visible = True
					ucBusinessAccountSelector.BusinessAccountTypeList = businessAccountTypeList
					ucBusinessAccountSelector.LenderRef = _LenderRef
					ucBusinessAccountSelector.DefaultParams = defaultParas
					ucBusinessAccountSelector.ScenarioName = Common.SafeString(Request.QueryString("scenario"))
					ucBusinessAccountSelector.PreselectedAccountType = Common.SafeString(Request.QueryString("acctype"))
				ElseIf IsInMode("777") Then
					hasBusinessAccount = False	'reset back due to download config doesn't have this account type
					hasBusinessAccountSecondary = False
				End If
			End If


			'TODO: need separate for primary & secondary
			ucAccountSelector.Visible = False
			ucAccountSelector.NewPersonalAccountUrl = xaPersonalUrl
			ucAccountSelector.ExistingPersonalAccountUrl = saPersonalUrl
			ucAccountSelector.HasPersonalAccounts = hasPersonalAccount
			ucAccountSelector.HasSpecialAccounts = hasSpecialAccount
			ucAccountSelector.HasBusinessAccounts = hasBusinessAccount

			plhApplicationDeclined.Visible = False
			If CustomListRuleList IsNot Nothing AndAlso CustomListRuleList.Any(Function(p) p.Code = "APPLICANT_BLOCK_RULE") Then
				plhApplicationDeclined.Visible = True
				Dim applicationBlockRulesPage As Inc_MainApp_ApplicationBlockRules = CType(LoadControl("~/Inc/MainApp/ApplicationBlockRules.ascx"), Inc_MainApp_ApplicationBlockRules)
				applicationBlockRulesPage.CustomListItem = CustomListRuleList.First(Function(p) p.Code = "APPLICANT_BLOCK_RULE")
				plhApplicationDeclined.Controls.Add(applicationBlockRulesPage)
			End If


			'TODO: need to support many permuation

			'if multiple multiple modules, display Account Select
			'If Not hasPersonalAccountOnly Then
			If (hasPersonalAccount AndAlso hasSpecialAccount) OrElse (hasPersonalAccount AndAlso hasBusinessAccount) OrElse (hasSpecialAccount AndAlso hasBusinessAccount) Then
				xaPersonalAttr = " href='#' onclick='AS.FACTORY.openAccountSelectorDialog(""xa"")'"
				saPersonalAttr = " href='#' onclick='AS.FACTORY.openAccountSelectorDialog(""sa"")'"
				htmlXA += "<div id='divXA'" & strDivDefaultClass & strDivOpenAccountStyle & "><a" & xaPersonalAttr & strLoanAttributes & ">" & IIf(isIconDisplay, "<img src='images/OpenAccount.png' />", IIf(_CurrentWebsiteConfig.InstitutionType = CEnum.InstitutionType.BANK, "New Account", "New Member Account")).ToString() & "</a></div>"
				htmlSA += "<div id='divSA'" & strDivDefaultClass & strDivOpenAccountStyle & "><a" & saPersonalAttr & strLoanAttributes & ">" & IIf(isIconDisplay, "<img src='images/OpenAccount.png' />", IIf(_CurrentWebsiteConfig.InstitutionType = CEnum.InstitutionType.BANK, "Existing Customer", "Existing Member Account")).ToString() & "</a></div>"
				ucAccountSelector.Visible = True

				''this is to avoid duplicate link in landing page
				If hasBusinessAccount Then
					UrlParamter2HtmlDic.Add("1B", htmlXA)
				ElseIf hasSpecialAccount Then
					UrlParamter2HtmlDic.Add("1S", htmlXA)
				ElseIf hasPersonalAccount Then
					UrlParamter2HtmlDic.Add("XA", htmlXA)
				End If

				If hasBusinessAccountSecondary Then
					UrlParamter2HtmlDic.Add("2B", htmlSA)
				ElseIf hasSpecialAccountSecondary Then
					UrlParamter2HtmlDic.Add("2S", htmlSA)
				ElseIf hasPersonalAccountSecondary Then
					UrlParamter2HtmlDic.Add("SA", htmlSA)
				End If

			ElseIf (hasPersonalAccount And (Not hasSpecialAccount And Not hasBusinessAccount)) OrElse _URLParameterList = "" Then 'if only personal, go direct
				htmlXA += "<div id='divXA'" & strDivDefaultClass & strDivOpenAccountStyle & "><a" & xaPersonalAttr & strLoanAttributes & ">" & IIf(isIconDisplay, "<img src='images/OpenAccount.png' />", IIf(_CurrentWebsiteConfig.InstitutionType = CEnum.InstitutionType.BANK, "New Account", "New Member Account")).ToString() & "</a></div>"
				htmlSA += "<div id='divSA'" & strDivDefaultClass & strDivOpenAccountStyle & "><a" & saPersonalAttr & strLoanAttributes & ">" & IIf(isIconDisplay, "<img src='images/OpenAccount.png' />", IIf(_CurrentWebsiteConfig.InstitutionType = CEnum.InstitutionType.BANK, "Existing Customer", "Existing Member Account")).ToString() & "</a></div>"
				UrlParamter2HtmlDic.Add("XA", htmlXA)
				UrlParamter2HtmlDic.Add("SA", htmlSA)
			ElseIf (hasSpecialAccount And (Not hasPersonalAccount And Not hasBusinessAccount)) Then	'if only XA special, go direct to special account selectot
				xaPersonalAttr = " href='#' onclick='SAS.FACTORY.openSelectorDialog(""1s"")'"
				saPersonalAttr = " href='#' onclick='SAS.FACTORY.openSelectorDialog(""2s"")'"
				htmlXA += "<div id='divXA'" & strDivDefaultClass & strDivOpenAccountStyle & "><a" & xaPersonalAttr & strLoanAttributes & ">" & IIf(isIconDisplay, "<img src='images/OpenAccount.png' />", IIf(_CurrentWebsiteConfig.InstitutionType = CEnum.InstitutionType.BANK, "New Special Account", "New Member Special Account")).ToString() & "</a></div>"
				htmlSA += "<div id='divSA'" & strDivDefaultClass & strDivOpenAccountStyle & "><a" & saPersonalAttr & strLoanAttributes & ">" & IIf(isIconDisplay, "<img src='images/OpenAccount.png' />", IIf(_CurrentWebsiteConfig.InstitutionType = CEnum.InstitutionType.BANK, "Existing Customer Special Account", "Existing Member Special Account")).ToString() & "</a></div>"
				UrlParamter2HtmlDic.Add("1S", htmlXA)
				UrlParamter2HtmlDic.Add("2S", htmlSA)
			ElseIf (hasBusinessAccount And (Not hasPersonalAccount And Not hasSpecialAccount)) Then	 'if only XA businsess, go direct to business account selector
				xaPersonalAttr = " href='#' onclick='BAS.FACTORY.openSelectorDialog(""1b"")'"
				saPersonalAttr = " href='#' onclick='BAS.FACTORY.openSelectorDialog(""2b"")'"
				htmlXA += "<div id='divXA'" & strDivDefaultClass & strDivOpenAccountStyle & "><a" & xaPersonalAttr & strLoanAttributes & ">" & IIf(isIconDisplay, "<img src='images/OpenAccount.png' />", IIf(_CurrentWebsiteConfig.InstitutionType = CEnum.InstitutionType.BANK, "New Business Account", "New Member Business Account")).ToString() & "</a></div>"
				htmlSA += "<div id='divSA'" & strDivDefaultClass & strDivOpenAccountStyle & "><a" & saPersonalAttr & strLoanAttributes & ">" & IIf(isIconDisplay, "<img src='images/OpenAccount.png' />", IIf(_CurrentWebsiteConfig.InstitutionType = CEnum.InstitutionType.BANK, "Existing Customer Business Account", "Existing Member Business Account")).ToString() & "</a></div>"
				UrlParamter2HtmlDic.Add("1B", htmlXA)
				UrlParamter2HtmlDic.Add("2B", htmlSA)
			End If

			Dim xaLoanHtml = htmlXA + htmlSA
			XMLNode2HtmlDic.Add("XA_LOAN", xaLoanHtml)

		End If
		'TODO: deprecate and remove code in 2019
		''-------xa for minor: if inputlist has type=1a or type=2a add it to current list------
		Dim htmlXA_minor As String = ""
		Dim htmlSA_minor As String = ""
		Dim xa_URL_minor As String = " href='" & _LenderBaseUrl & "/xa/xpressApp.aspx?lenderref=" & _LenderRef & "&type=1a" & defaultParas & "'"
		Dim sa_URL_minor As String = " href='" & _LenderBaseUrl & "/xa/xpressApp.aspx?lenderref=" & _LenderRef & "&type=2a" & defaultParas & "'"
		If _URLParameterList.Contains("1A") Then
			_AvailableModules += "1A"
			'' Rename XA minor
			Dim xaRename_minor As String = IIf(_CurrentWebsiteConfig.InstitutionType = CEnum.InstitutionType.BANK, "New Minor Account", "New Minor Member")
			If isIconDisplay Then
				xaRename_minor = "<img src='images/OpenAccount.png' />"
			ElseIf layoutAttributes.ContainsKey("landing_page_1a_rename") AndAlso layoutAttributes("landing_page_1a_rename") <> "" Then
				xaRename_minor = layoutAttributes("landing_page_1a_rename")
			End If
			htmlXA_minor += "<div id='divXA_minor'" & strDivDefaultClass & "><a" & xa_URL_minor & strLoanAttributes & ">"
			htmlXA_minor += xaRename_minor & "</a></div>"
			UrlParamter2HtmlDic.Add("1A", htmlXA_minor)
		End If
		If _URLParameterList.Contains("2A") Then
			_AvailableModules += "2A"
			'' Rename SA minor
			Dim saRename_minor As String = "Open Minor Checking"
			If isIconDisplay Then
				saRename_minor = "<img src='images/OpenAccount.png' />"
			ElseIf layoutAttributes.ContainsKey("landing_page_2a_rename") AndAlso layoutAttributes("landing_page_2a_rename") <> "" Then
				saRename_minor = layoutAttributes("landing_page_2a_rename")
			End If
			htmlSA_minor += "<div id='divSA_minor'" & strDivDefaultClass & "><a" & sa_URL_minor & strLoanAttributes & ">"
			htmlSA_minor += saRename_minor & "</a></div>"
			UrlParamter2HtmlDic.Add("2A", htmlSA_minor)
		End If
		''--------end xa for minor------------

		If _hasVL = "Y" Then
			_AvailableModules += "VL"
			'' Rename VL
			Dim vlRename As String = "Vehicle Loans"
			If isIconDisplay Then
				vlRename = "<img src='images/VehicleLoan.png' />"
			ElseIf layoutAttributes.ContainsKey("landing_page_vl_rename") AndAlso layoutAttributes("landing_page_vl_rename") <> "" Then
				vlRename = layoutAttributes("landing_page_vl_rename")
			End If

			Dim vl_URL As String = " href='" & _LenderBaseUrl & "/vl/VehicleLoan.aspx?lenderref=" & _LenderRef & defaultParas & "'"
			If Not String.IsNullOrEmpty(tokenUrl) Then
				vl_URL = " href='" & _LenderBaseUrl & tokenUrl & "&LoanType=vl'"
			End If
			htmlVL += "<div id='divVL'" & strDivDefaultClass & "><a" & vl_URL & strLoanAttributes & ">" & vlRename & "</a></div>"
			XMLNode2HtmlDic.Add("VEHICLE_LOAN", htmlVL)
			UrlParamter2HtmlDic.Add("VL", htmlVL)
		End If
		If _hasCC = "Y" Then
			_AvailableModules += "CC"
			'' Rename CC
			Dim ccRename As String = "Credit Cards"
			If isIconDisplay Then
				ccRename = "<img src='images/CreditCard.png' />"
			ElseIf layoutAttributes.ContainsKey("landing_page_cc_rename") AndAlso layoutAttributes("landing_page_cc_rename") <> "" Then
				ccRename = layoutAttributes("landing_page_cc_rename")
			End If

			Dim cc_URL As String = " href='" & _LenderBaseUrl & "/cc/CreditCard.aspx?lenderref=" & _LenderRef & defaultParas & "'"
			If Not String.IsNullOrEmpty(tokenUrl) Then
				cc_URL = " href='" & _LenderBaseUrl & tokenUrl & "&LoanType=cc'"
			End If
			htmlCC += "<div id='divCC'" & strDivDefaultClass & "><a" & cc_URL & strLoanAttributes & ">" & ccRename & "</a></div>"
			XMLNode2HtmlDic.Add("CREDIT_CARD_LOAN", htmlCC)
			UrlParamter2HtmlDic.Add("CC", htmlCC)
		End If
		If _hasPL = "Y" Then
			_AvailableModules += "PL"
			'' Rename PL
			Dim plRename As String = "Personal Loans"
			If isIconDisplay Then
				plRename = "<img src='images/PersonalLoan.png' />"
			ElseIf layoutAttributes.ContainsKey("landing_page_pl_rename") AndAlso layoutAttributes("landing_page_pl_rename") <> "" Then
				plRename = layoutAttributes("landing_page_pl_rename")
			End If

			Dim pl_URL As String = " href='" & _LenderBaseUrl & "/pl/PersonalLoan.aspx?lenderref=" & _LenderRef & defaultParas & "'"
			If Not String.IsNullOrEmpty(tokenUrl) Then
				pl_URL = " href='" & _LenderBaseUrl & tokenUrl & "&LoanType=pl'"
			End If
			htmlPL += "<div id='divPL'" & strDivDefaultClass & "><a" & pl_URL & strLoanAttributes & ">" & plRename & "<a/></div>"
			XMLNode2HtmlDic.Add("PERSONAL_LOAN", htmlPL)
			UrlParamter2HtmlDic.Add("PL", htmlPL)
		End If
		If _hasHE = "Y" Then
			_AvailableModules += "HE"
			'' Rename HE
			Dim heRename As String = "Home Equity Loans"
			If isIconDisplay Then
				heRename = "<img src='images/HomeEquity.png' />"
			ElseIf layoutAttributes.ContainsKey("landing_page_he_rename") AndAlso layoutAttributes("landing_page_he_rename") <> "" Then
				heRename = layoutAttributes("landing_page_he_rename")
			End If

			Dim he_URL As String = " href='" & _LenderBaseUrl & "/he/HomeEquityLoan.aspx?lenderref=" & _LenderRef & defaultParas & "'"
			If Not String.IsNullOrEmpty(tokenUrl) Then
				he_URL = " href='" & _LenderBaseUrl & tokenUrl & "&LoanType=he'"
			End If
			htmlHE += "<div id='divHE'" & strDivDefaultClass & "><a" & he_URL & strLoanAttributes & ">" & heRename & "</a></div>"
			XMLNode2HtmlDic.Add("HOME_EQUITY_LOAN", htmlHE)
			UrlParamter2HtmlDic.Add("HE", htmlHE)
		End If
		''add ST for Check Status to currentlist
		_AvailableModules += "ST"
		'' Rename ST
		Dim stRename As String = "Check Status"
		If isIconDisplay Then
			stRename = "<img src='images/AppStatus.png' />"
		ElseIf layoutAttributes.ContainsKey("landing_page_st_rename") AndAlso layoutAttributes("landing_page_st_rename") <> "" Then
			stRename = layoutAttributes("landing_page_st_rename")
		End If

		Dim st_URL As String = " href='" & _LenderBaseUrl & "/cu/ViewSubmittedLoans.aspx?lenderref=" & _LenderRef & defaultParas & "'"
		If Not String.IsNullOrEmpty(tokenUrl) Then
			st_URL = " href='" & _LenderBaseUrl & tokenUrl & "&LoanType=ls'"
		End If
		Dim strST As String = ""
		'' Add header
		If isIconDisplay = False And String.IsNullOrEmpty(_statusHeader) = False Then
			strST += "<hr><div style='text-align:center'><h2>" & _statusHeader & "</h2></div>"
		End If
		strST += "<div id='divST'" & strDivDefaultClass & "><a" & st_URL & strLoanAttributes & ">" & stRename & "</a></div>"
		'' Add footer
		If isIconDisplay = False And String.IsNullOrEmpty(_statusFooter) = False Then
			strST += "<div style='text-align:center'><h2>" & _statusFooter & "</h2></div>"
		End If

		UrlParamter2HtmlDic.Add("ST", strST)
		''add custom buttons(cb) to the landing page
		Dim htmlAdditionalButtons = buildAdditionalButtons(strDivDefaultClass, strLoanAttributes)
		If Not isIconDisplay And htmlAdditionalButtons <> "" And _URLParameterList.Contains("CB") Then
			_AvailableModules += "CB"  ''add CB(custom button) to the _Availablemodules
			UrlParamter2HtmlDic.Add("CB", htmlAdditionalButtons)
		End If
		''if URLParameterList is empty then use default xmlnode from xml config
		If _hasURLParameterList <> "Y" Then
			''by default there is no list input
			Dim loanTypes As List(Of String) = loanTypeList()
			Dim loanCount As Integer = loanTypes.Count
			Dim i As Integer
			For i = 0 To loanCount - 1
				Dim oName As String = loanTypes(i)
				If isIconDisplay AndAlso oName = "XA_LOAN" Then
					Continue For 'skip XA, add to the last
				End If

				If (XMLNode2HtmlDic.ContainsKey(oName)) Then
					_strHtmlLoans += XMLNode2HtmlDic.Item(oName)
				End If
			Next
			_strHtmlLoans += strST '' rendering available type by default(no parameter list)
			' add XA to the last
			If isIconDisplay Then
				_strHtmlLoans += XMLNode2HtmlDic.Item("XA_LOAN")
			End If
		Else
			Dim UrlParmeters As List(Of String) = getList(_URLParameterList)
			Dim isValidList As Boolean = hasValidList(UrlParmeters, _AvailableModules)
			If isValidList Then
				Dim htmlStrList As String = ""
				Dim loanSectionHeaderStr As String = ""
				If layoutAttributes.ContainsKey("landing_page_loan_section_header") Then
					loanSectionHeaderStr = layoutAttributes("landing_page_loan_section_header")
				End If
				For Each UrlParameter As String In UrlParmeters
					UrlParameter = UrlParameter.ToUpper()
					If (UrlParamter2HtmlDic.ContainsKey(UrlParameter)) Then
						If String.IsNullOrEmpty(loanSectionHeaderStr.Trim()) = False And (UrlParameter = "CC" Or UrlParameter = "HE" Or UrlParameter = "PL" Or UrlParameter = "VL" Or UrlParameter = "BL") And isIconDisplay = False Then
							htmlStrList += String.Format("<hr><div style='text-align:center;'><h2>{0}</h2></div>", loanSectionHeaderStr)
							loanSectionHeaderStr = "" ' add only one time for loan
						End If
						htmlStrList += UrlParamter2HtmlDic.Item(UrlParameter)
					End If
				Next
				_strHtmlLoans = htmlStrList	''rendering available loan types using input list
			Else
				If _hasURLParameterList = "Y" Then
					Dim strMessage As String = "the list is empty or incorrect format."
					If Not String.IsNullOrEmpty(_URLParameterList) Then
						strMessage = "'" & _URLParameterList & "' is invalid. Please enter the input correctly."
					End If
					CPBLogger.logInfo("Error list parameter: " & strMessage)
					_strHtmlLoans = ""
				End If
			End If
		End If

		''Addtional button via CUSTOM_LIST config xml
		If isIconDisplay = False AndAlso _URLParameterList = "" Then
			''Dim additionalButtonList = Common.getItemsFromConfig(_CurrentWebsiteConfig, "CUSTOM_LIST/LANDING_PAGE_BUTTONS/ITEM", "name", "url")
			''If additionalButtonList IsNot Nothing And additionalButtonList.Any() And _strHtmlLoans <> "" Then
			''    For Each btn As KeyValuePair(Of String, String) In additionalButtonList
			''        Dim strAdditionBtn As String = String.Format("<div id='div{0}' {1}><a href='{2}' {3}>{4}</a></div>", btn.Key.Replace(" ", ""), strDivDefaultClass, Server.HtmlDecode(btn.Value), strLoanAttributes, btn.Key)
			''        _strHtmlLoans += strAdditionBtn
			''    Next
			''End If
			If _strHtmlLoans <> "" Then
				_strHtmlLoans += htmlAdditionalButtons
			End If
		End If
	End Sub
	Function buildAdditionalButtons(ByVal strDivDefaultClass As String, ByVal strLoanAttributes As String) As String
		''Addtional button via CUSTOM_LIST config xml
		Dim additionalButtonList = Common.getItemsFromConfig(_CurrentWebsiteConfig, "CUSTOM_LIST/LANDING_PAGE_BUTTONS/ITEM", "name", "url")
		Dim strHtmlAdditionBtn As String = ""
		If additionalButtonList IsNot Nothing And additionalButtonList.Any() Then
			For Each btn As KeyValuePair(Of String, String) In additionalButtonList
				Dim strAdditionBtn As String = String.Format("<div id='div{0}' {1}><a href='{2}' {3}>{4}</a></div>", btn.Key.Replace(" ", ""), strDivDefaultClass, Server.HtmlDecode(btn.Value), strLoanAttributes, btn.Key)
				strHtmlAdditionBtn += strAdditionBtn
			Next
		End If
		Return strHtmlAdditionBtn
	End Function
	''get value from 'list' url parameter and return as list format
	Protected Function getList(ByVal oStrList As String) As List(Of String)
		Dim strLength As Integer = oStrList.Length
		Dim strList As New List(Of String)
		If strLength Mod 2 = 0 Then
			Dim strArray As Array = oStrList.ToCharArray()
			For i = 0 To strArray.Length - 1
				Dim strType As String = ""
				If (i + 1) Mod 2 = 0 Then
					strType = strArray(i - 1) & strArray(i)	'' each Type consists of 2 characters-st,vl,xa ...
					strList.Add(strType) '' add all types to the string list
				End If
			Next
		End If
		Return strList
	End Function
	''validate the input string list
	Protected Function hasValidList(ByVal oList As List(Of String), ByVal oCurrentList As String) As Boolean
		Dim oCurrentLength As Integer = oCurrentList.Length
		Dim oInputList As List(Of String) = oList
		Dim oListCount As Integer = oList.Count
		If oListCount <= 0 Then
			Return False ''no input list
		End If
		If oListCount > oCurrentLength / 2 Then
			Return False
		End If
		For i = 0 To oListCount - 1
			Dim sType As String = oList(i).ToUpper
			If oCurrentList.IndexOf(sType) = -1 Then '' invalid input type
				Return False
			End If
			'' check no duplicate types
			Dim oTypeCount As Integer = 0
			For j = 0 To oInputList.Count - 1
				If sType = oInputList(j).ToUpper Then
					oTypeCount += 1
				End If
				If (oTypeCount > 1) Then
					Return False
				End If
			Next
		Next
		Return True
	End Function

	Protected Function hasLoanTypes(ByVal loanTypes As String) As String
		Dim hasLoan As String = "N"	''by default
		Dim oNodes As XmlNodeList = _CurrentWebsiteConfig._webConfigXML.SelectNodes(loanTypes)
		If oNodes.Count > 0 Then
			hasLoan = "Y"
		Else
			hasLoan = "N"
		End If
		Return hasLoan
	End Function

	Protected Function readLayoutConfig() As Dictionary(Of String, String)
		Dim result As New Dictionary(Of String, String)

		Dim oNodes As XmlNodeList = _CurrentWebsiteConfig._webConfigXML.SelectNodes("LAYOUT")

		If oNodes.Count > 0 Then
			For Each attribute As XmlAttribute In oNodes(0).Attributes
				result.Add(attribute.Name, attribute.Value)
			Next
		End If

		Return result
	End Function

	Protected Function readVisibleConfig() As Dictionary(Of String, String)
		Dim result As New Dictionary(Of String, String)

		Dim oNodes As XmlNodeList = _CurrentWebsiteConfig._webConfigXML.SelectNodes("VISIBLE")

		If oNodes.Count > 0 Then
			For Each attribute As XmlAttribute In oNodes(0).Attributes
				result.Add(attribute.Name, attribute.Value)
			Next
		End If

		Return result
	End Function

	Protected Function readFooterNoteConfig() As String
		Dim result As String = ""

		Dim oNodes As XmlNodeList = _CurrentWebsiteConfig._webConfigXML.SelectNodes("CUSTOM_TEXT/LANDING_PAGE_FOOTER")

		If oNodes.Count > 0 Then
			result = Common.SafeString(oNodes(0).InnerXml)
		End If

		Return result
	End Function

	''get all loan types from xml file and store it in string list in order from top to bottom of the nodes
	Protected Function loanTypeList() As List(Of String)
		Dim oLoanTypeList As New List(Of String)
		Dim oElement As XmlElement = _CurrentWebsiteConfig._webConfigXML
		For Each childNode As XmlNode In oElement
			Dim element As String = childNode.Name
			If element.Contains("_LOAN") Then
				If (element.Length >= 5) Then
					Dim loanPrefix As String = element.Substring(element.Length - 5)
					If loanPrefix = "_LOAN" Then
						oLoanTypeList.Add(element)
					End If
				End If
			End If
		Next
		Return oLoanTypeList
	End Function

	Protected Function getTokenUrl(ByVal queryStr As String) As String
		Dim strToken As String = AntiXssEncoder.HtmlEncode(Request.QueryString("Token"), True)
		Dim strList As String = AntiXssEncoder.HtmlEncode(Request.QueryString("list"), True)
		Dim strUrl As String = ""

		If Not String.IsNullOrEmpty(strToken) Then
			Dim nvcParsedQuery = HttpUtility.ParseQueryString(queryStr)

			' Convert Token to Key
			nvcParsedQuery.Remove("Token")
			nvcParsedQuery.Add("Key", strToken)

			' If list exists, remove list parameter from query url
			If Not String.IsNullOrEmpty(strList) Then
				nvcParsedQuery.Remove("list")
			End If

			' The NameValueCollection returned from HttpUtility.ParseQueryString is actually an HttpValueCollection,
			' and its .ToString() correctly renders it as a URL Query.
			strUrl = "/Partners/SSOLogin.aspx?" & nvcParsedQuery.ToString()
		End If

		Return strUrl
	End Function

	Protected Function getLoanParas(ByVal queryStr As String) As String
		Dim nvcParsedQuery = HttpUtility.ParseQueryString(queryStr)

		' Remove list, token and lenderref parameters (case insensitive)
		nvcParsedQuery.Remove("token")
		nvcParsedQuery.Remove("list")
		nvcParsedQuery.Remove("lenderref")
		' The NameValueCollection returned from HttpUtility.ParseQueryString is actually an HttpValueCollection,
		' and its .ToString() correctly renders it as a URL Query.
		Return "&" & nvcParsedQuery.ToString()
	End Function

    Private _lenderrefWhitelist() As String = {"redwoodcu", "mcu", "ecu", "floridastateucu", "floridascu", "ccfcu", "solaritycu", "alliancecu", "cusocal", "sdfcu", "fedchoicefcu", "fcfcu", "fuscu", "floridacu", "sdccu", "cacu", "neighborscu", "pslfcu", "cefcu", "keeslerfcu", "gbfcu"}
    'return framebreaker code if cu is not in whitelist
    Public Function getFrameBreakerCode() As String
        If Request.QueryString("mode") = "777" Then Return ""
        Dim s As String = "<style id='antiClickjack'>body{display:none !important;}</style>" &
            "<script type='text/javascript'>" &
      "if (self === top){var antiClickjack = document.getElementById('antiClickjack'); antiClickjack.parentNode.removeChild(antiClickjack);} else {top.location = self.location;}" &
        "</script>"
        Dim sLenderRef As String = Request.Params("lenderref")
        If sLenderRef = "" Then Return s
        Dim disableFrameBreaker = Common.getNodeAttributes(_CurrentWebsiteConfig, "BEHAVIOR", "disable_frame_breaker").ToUpper = "Y"
        If disableFrameBreaker Then Return "" ''via xml config
        For Each lender As String In _lenderrefWhitelist
            If sLenderRef.ToLower.Contains(lender) Then
                Return Nothing
            End If
        Next

        Return s 'frame breaker code
    End Function

End Class
