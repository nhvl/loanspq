﻿
Imports System.Data
Imports System.IO
Imports System.Linq
Imports System
Imports System.Xml
Imports System.Xml.Serialization
Imports System.Threading
Imports LPQMobile.Utils
Imports LPQMobile.BO


Namespace LendingTree
	
	''' <summary>
	''' LendingTree(LT) uses this service to create app in LPQ
	''' Flow:
	''' 1) LT post app to this service.  2)This service validate request and kick off a separate thread(underwriteappservice) to submit app to LPQ
	''' 3) Within n second, this service returns a dt_ack (acknowledge)
	''' response that indicates whether the request processed normally or if an error occurred.
	''' this service use CServiceConfig(different db) instead of CWebsiteConfig 
	''' <LTXML></LTXML><ReferenceSource>26495</ReferenceSource> map to db/_LendingTreePartnerID, This service use this to get LPQ lenderID, OrgID, API credential from DB
	'''<LTXML><LoanApplicationID>100706202</LoanApplicationID> map to _LendingTreeAppId, LT use this to keep track of submitted app so this service always include _LendingTreeAppId in response
	''' 
	''' </summary>
	Partial Class CreateApp
		Inherits System.Web.UI.Page

		Private log As log4net.ILog = log4net.LogManager.GetLogger(Me.GetType)

#Region "Property"
		Private _RawInput As String = Nothing
		Protected ReadOnly Property RawInput() As String
			Get
				If _RawInput Is Nothing Then
					_RawInput = New StreamReader(Request.InputStream).ReadToEnd
				End If

				Return _RawInput
			End Get
		End Property

		Private _inputDoc As Xml.XmlDocument
		Protected ReadOnly Property InputDoc() As Xml.XmlDocument
			Get
				If _inputDoc Is Nothing Then
					'_log.Info(RawInput)

					_inputDoc = New Xml.XmlDocument()
					_inputDoc.LoadXml(RawInput)


				End If

				Return _inputDoc
			End Get
		End Property
#End Region

#Region "CheckWellFormed - Makes sure INPUT is actually XML"
		Protected Sub CheckWellFormed()
			Dim sRequest As String = ""
			Try
				'invoking InputDoc will automatically log the input
				Dim dummy As Xml.XmlDocument = InputDoc
				'log.Info(CSecureStringFormatter.MaskSensitiveXMLData(dummy.OuterXml))
			Catch ex As System.Xml.XmlException
				Throw New Exception("Input was not well formed and xml compliant.")
			End Try
		End Sub

#End Region

		Private _TransactionType As String = ""	'Mortgage, Home Equity, Personal, Auto, Credit Card, Student Loan
		Private _LendingTreeAppId As String
		Private _LendingTreePartnerID As String
		Private _VehicleLoan As CVehicleLoan
			Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
			'Response.Write("Under Construction. Please test this in another week.")
			'Response.End()

			''testing purpose
			If Request.QueryString("vlclf") = "y" Then
				Try
					Dim oXMLDocTest As New XmlDocument
					oXMLDocTest.Load(Server.MapPath("Georgia_auto_sample_post.xml"))  'beta 
					Dim nsmgr As XmlNamespaceManager = New XmlNamespaceManager(oXMLDocTest.NameTable)
					nsmgr.AddNamespace("ab", "http://www.meridianlink.com/CLF")
					Dim xmlNodes As XmlNodeList = oXMLDocTest.SelectNodes("//ab:LENDER", nsmgr)
					log.Debug("debug: " & xmlNodes.Count)
					_inputDoc = oXMLDocTest
					'Return
				Catch ex As Exception
					log.Warn("Can't execute testclf: " & ex.Message, ex)
					SendAcknowledgeResponse(_LendingTreeAppId, AcknowledgementTypes.SystemError, "Error parsing app(testclf): " & ex.Message)
					Return
				End Try
			End If

			Try
				CheckWellFormed()
			Catch ex As Exception
				log.Warn("LendingTree exception while attempting to parse request. Error: " & ex.Message, ex)
				SendAcknowledgeResponse(_LendingTreeAppId, AcknowledgementTypes.SystemError, "Error parsing app: " & ex.Message)
				Return
			End Try
			Dim xmlRequest As XmlElement = Nothing
			Dim oBogusConfi As New CServiceConfig ' don't need to be initialize
			Try
				xmlRequest = DirectCast(InputDoc.SelectSingleNode("LTXML"), XmlElement)
				' TEST CODE
				'requestString = New StreamReader("c:\documents and settings\kevinp\desktop\request.xml").ReadToEnd()
				log.Debug("LendingTree create app request received: " & CSecureStringFormatter.MaskSensitiveXMLData(xmlRequest.OuterXml))
				_LendingTreeAppId = New CXmlElement(xmlRequest).GetElementValue("LoanApplicationID")
				_LendingTreePartnerID = New CXmlElement(xmlRequest).GetElementValue("ReferenceSource")
				_TransactionType = New CXmlElement(xmlRequest).SelectSingleNode("TransactionType").FirstChild.Name
				If Common.SafeString(_TransactionType).ToUpper <> "AUTO" Then
					log.Warn("Not Supported Loan Type. Request: " & CSecureStringFormatter.MaskSensitiveXMLData(xmlRequest.OuterXml))
					SendAcknowledgeResponse(_LendingTreeAppId, AcknowledgementTypes.SystemError, "Not Supported Loan Type")
					Return
				End If
				_VehicleLoan = CLendingTreeManager.CreateVL(New CXmlElement(xmlRequest), oBogusConfi)
			Catch ex As Exception
				log.Warn("LendingTree exception while attempting to parse request. Error: " & ex.Message, ex)
				SendAcknowledgeResponse(_LendingTreeAppId, AcknowledgementTypes.SystemError, "Error parsing app: " & ex.Message)
				Return
			End Try

			'TODO: do we need this process? don't want to kill the service for just one bad application
			'schema validation poLoan.GetXml().OuterXml()
			'Dim sVdalidateMessage = ValidateXMLConstruction(_VehicleLoan.GetXml())
			'If sVdalidateMessage <> "" Then
			'	SendAcknowledgeResponse(_LendingTreeAppId, AcknowledgementTypes.SystemError, "Format Error: " & sVdalidateMessage)
			'	Return
			'End If
			Dim sSubmitURL As String = Request.Url.GetLeftPart(UriPartial.Authority) & "/LendingTree/underwriteappservice.aspx"
			Dim webRequest As Net.WebRequest = Net.HttpWebRequest.Create(sSubmitURL)
			webRequest.Method = "POST"
			webRequest.ContentType = "application/xml"
			Dim sRequestXML As XmlDocument = CLendingTreeManager.getSubmitLoanData(_VehicleLoan, oBogusConfi) 'bogus so we can check for formatting
			Dim requestData As New CRequestData(webRequest, InputDoc.OuterXml)
			webRequest.BeginGetRequestStream(New AsyncCallback(AddressOf BeginGetRequest), requestData)	'asynch, this thread do the actual submit to LPQ

			' Send back successful dt_ack (acknowledge) response, this is send while other process continue to process in background, this need to be done within n second
			SendAcknowledgeResponse(_LendingTreeAppId, AcknowledgementTypes.Success, "Success")

		End Sub


		Private Sub FinishLendingTreeApplication(ByVal pResult As IAsyncResult)
			Try
				Dim requestData As CRequestData = DirectCast(pResult.AsyncState, CRequestData)
				Dim webRequest As Net.WebRequest = requestData.request

				Dim response As Net.WebResponse = DirectCast(webRequest.EndGetResponse(pResult), Net.WebResponse)

				Dim s As Stream = DirectCast(response.GetResponseStream, Stream)
				Using sr As New StreamReader(s)
					Dim text As String = sr.ReadToEnd
					Log.Info(text)
				End Using
			Catch ex As Exception
				Log.Warn(ex)
			End Try
		End Sub

		Private Sub BeginGetRequest(ByVal pResult As IAsyncResult)
			Dim requestData As CRequestData = DirectCast(pResult.AsyncState, CRequestData)
			Dim request As Net.WebRequest = requestData.request
			Dim st As Stream = request.EndGetRequestStream(pResult)
			Using sw As New StreamWriter(st)
				sw.Write(requestData.data)
			End Using

			Dim result As IAsyncResult = request.BeginGetResponse(New AsyncCallback(AddressOf FinishLendingTreeApplication), requestData)
			'Register the timeout callback
			ThreadPool.RegisterWaitForSingleObject(result.AsyncWaitHandle, New WaitOrTimerCallback(AddressOf ScanTimeoutCallback), requestData, 30 * 1000, True)
		End Sub

		Private Shared Sub ScanTimeoutCallback(ByVal pState As Object, ByVal pTimeout As Boolean)
			If pTimeout Then
				Dim requestData As CRequestData = DirectCast(pState, CRequestData)
				Dim webRequest As Net.WebRequest = requestData.request
				If webRequest IsNot Nothing Then
					webRequest.Abort()
				End If
			End If
		End Sub

		Private Function ValidateXMLConstruction(ByVal pXML As XmlDocument) As String
			Dim oSchemaVal As New XMLTools.CSchemaValidator
			If oSchemaVal.validate(pXML.OuterXml, New XmlTextReader(Server.MapPath("~/CLF/vehicle_loan.xsd"))) = False Then
				Dim sXMLErrorMessage As String = oSchemaVal.ParseError
				log.Info("XML Construction failed XSD Validation: " & sXMLErrorMessage)
				Return sXMLErrorMessage
			Else
				Return "" 'pass
			End If
		End Function

		''' <summary>
		''' Helper function to generate the dt_ack (acknowledge) response that LendingTree is expecting to receive from
		''' this web service.
		''' </summary>
		Private Sub SendAcknowledgeResponse(ByVal pLoanApplicationID As String, ByVal pAcknowledgementType As AcknowledgementTypes, ByVal pErrorDescription As String)

			Dim doc As New XmlDocument()

			Dim LTACK As New CXmlElement(doc.CreateElement("LTACK"))
			doc.AppendChild(LTACK.Source)
			Dim ERRSTATUS As New CXmlElement(doc.CreateElement("LTACK"))
			LTACK.AppendChild(ERRSTATUS.Source)

			Dim type As String = ""

			Select Case pAcknowledgementType
				Case AcknowledgementTypes.Success
					type = "0"
				Case AcknowledgementTypes.Duplicate
					type = "1"
				Case AcknowledgementTypes.InvalidCoverageMatch
					type = "2"
				Case AcknowledgementTypes.DuplicateLendingTreeLead
					type = "3"

				Case AcknowledgementTypes.InvalidLeadFormat
					type = "2"
				Case AcknowledgementTypes.SystemError
					type = "2"
				Case AcknowledgementTypes.AccountInactive
					type = "2"
			End Select

			'ERRSTATUS.SetAttribute("type", type)
			ERRSTATUS.AddElementValue("QFNAME", pLoanApplicationID)
			ERRSTATUS.AddElementValue("ERRORNUM", type)
			ERRSTATUS.AddElementValue("ERRORDESCRIPTION", pErrorDescription)

			Log.Info("LendingTree LTACK response: " & doc.OuterXml)

			Response.Clear()
			Response.Write(doc.OuterXml)
			'Response.End()
			HttpContext.Current.Response.Flush() ' // Sends all currently buffered output to the client.
			HttpContext.Current.Response.SuppressContent = True	' // Gets or sets a value indicating whether to send HTTP content to the client.
			HttpContext.Current.ApplicationInstance.CompleteRequest()
		End Sub

		Private Enum AcknowledgementTypes
			'Values for accepted
			Success
			Duplicate 'Duplicate Email/SSN/Other Lead Data already exists
			InvalidCoverageMatch
			DuplicateLendingTreeLead '

			'VAlues for Errors
			InvalidLeadFormat
			SystemError
			AccountInactive

			'Note: If you require LendingTree to investigate failure then use Error code, for all other please send back accepted.
		End Enum

		Class CRequestData
			Public request As Net.WebRequest
			Public data As String

			Sub New(ByVal pRequest As Net.WebRequest, ByVal pData As String)
				Me.request = pRequest
				Me.data = pData
			End Sub
		End Class

	End Class

End Namespace
