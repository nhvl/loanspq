﻿
Imports System.Data
Imports System.IO
Imports System.Linq
Imports System
Imports System.Net
Imports System.Xml
Imports System.Xml.Serialization
Imports System.Threading
Imports LPQMobile.Utils
Imports LPQMobile.BO

Namespace LendingTree
	
	''' <summary>
	''' This web service is called by the LendingTree/CreateApp service to underwrite the application(post to LPQ). We need to break this out from the
	''' CreateApp service because the CreateApp service needs to respond instantly (otherwise LendingTree may try to send the same app again if
	''' it doesn't receive an acknowledgement response fast enough).
	''' 
	''' <LTXML></LTXML><ReferenceSource>26495</ReferenceSource> map to db/_LendingTreePartnerID, This service use this to get LPQ lenderID, OrgID, API credential from DB
	'''<LTXML><LoanApplicationID>100706202</LoanApplicationID> map to _LendingTreeAppId, LT use this to keep track of submitted app so this service alway inclued _LendingTreeAppId in response
	''' 
	''' </summary>
	Partial Class UnderwriteAppService
		Inherits System.Web.UI.Page

		Private Shared _log As log4net.ILog = log4net.LogManager.GetLogger(GetType(UnderwriteAppService))

#Region "Property"
		Private _RawInput As String = Nothing
		Protected ReadOnly Property RawInput() As String
			Get
				If _RawInput Is Nothing Then
					_RawInput = New StreamReader(Request.InputStream).ReadToEnd
				End If

				Return _RawInput
			End Get
		End Property

		Private _inputDoc As Xml.XmlDocument
		Protected ReadOnly Property InputDoc() As Xml.XmlDocument
			Get
				If _inputDoc Is Nothing Then
					'_log.Info(RawInput)

					_inputDoc = New Xml.XmlDocument()
					_inputDoc.LoadXml(RawInput)


				End If

				Return _inputDoc
			End Get
		End Property
#End Region

#Region "CheckWellFormed - Makes sure INPUT is actually XML"
		Protected Sub CheckWellFormed()
			Dim sRequest As String = ""
			Try
				'invoking InputDoc will automatically log the input
				Dim dummy As Xml.XmlDocument = InputDoc
				'log.Info(CSecureStringFormatter.MaskSensitiveXMLData(dummy.OuterXml))
			Catch ex As System.Xml.XmlException
				Throw New Exception("Input was not well formed and xml compliant.")
			End Try
		End Sub

#End Region

		Private _TransactionType As String = ""	'Mortgage, Home Equity, Personal, Auto, Credit Card, Student Loan
		Private _LendingTreeAppID As String
		Private _LendingTreePartnerID As String
		Private _VehicleLoan As CVehicleLoan
		'Private _SubmitURL As String = Request.Url.GetLeftPart(UriPartial.Authority) & "/LendingTree/underwriteappservice.aspx"
		'Const SUBMIT_URL As String = "https://beta.loanspq.com/services/decisionloan/2.0/decisionloan.aspx"	'TODO make this auto
		Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load

			Try
				CheckWellFormed()
			Catch ex As Exception
				_log.Warn("LendingTree exception while attempting to parse request. Error: " & ex.Message, ex)
				Return
			End Try
			Dim xmlRequest As XmlElement = Nothing
			Dim oConfig As New CServiceConfig
			Try
				xmlRequest = DirectCast(InputDoc.SelectSingleNode("LTXML"), XmlElement)
				'_log.Debug("LendingTree create app request received: " & CSecureStringFormatter.MaskSensitiveXMLData(xmlRequest.OuterXml))
				_LendingTreeAppID = New CXmlElement(xmlRequest).GetElementValue("LoanApplicationID")
				_TransactionType = New CXmlElement(xmlRequest).SelectSingleNode("TransactionType").FirstChild.Name
				_LendingTreePartnerID = New CXmlElement(xmlRequest).GetElementValue("ReferenceSource")
				oConfig = oConfig.GetConfigByLendingTreePartnerID(_LendingTreePartnerID)
				'log
				If oConfig Is Nothing Then
					Throw New Exception("LendingTreePartnerID: " & _LendingTreePartnerID & " is not in configuration.")
				End If
				_VehicleLoan = CLendingTreeManager.CreateVL(New CXmlElement(xmlRequest), oConfig)

			Catch ex As Exception
				_log.Warn("LendingTree exception while attempting to parse request. Request: " & Left(CSecureStringFormatter.MaskSensitiveXMLData(xmlRequest.OuterXml), 500) & ". Error: " & ex.Message, ex)
				Return
			End Try

            'TODO: do we need this process? don't want to kill the service for just one bad application
            'schema validation poLoan.GetXml().OuterXml()
            'Dim sVdalidateMessage = ValidateXMLConstruction(_VehicleLoan.GetXml())
            'If sVdalidateMessage <> "" Then
            '	SendAcknowledgeResponse(_LendingTreeAppId, AcknowledgementTypes.SystemError, "Format Error: " & sVdalidateMessage)
            '	Return
            'End If
            'Dim websiteConfig As CWebsiteConfig = Common.GetCurrentWebsiteConfig(Request.Url.Host, oConfig.LPQLenderID.ToString(), "")

            If Not (oConfig.LPQBranchID = Guid.Empty) Then
                _VehicleLoan.BranchID = Common.SafeString(oConfig.LPQBranchID).Replace("-", "")
            End If

            Dim sRequestXML As XmlDocument = CLendingTreeManager.getSubmitLoanData(_VehicleLoan, oConfig)
            Dim sResponse As String = Common.WebPost(sRequestXML, oConfig.LPQURL)
			If sResponse = "" Then Return

			'Send decision back to LendingTree
			Try
				CreateWebRequest(sResponse, oConfig)
			Catch ex As Exception
				_log.Warn("Exception occurred while attempting to add send decision to LendingTree: " & ex.Message, ex)
				Return
			End Try


			Return

		End Sub

		''' <summary>
		''' Creates a webrequest to push an app descision back out to LT
		''' </summary>
		Private Sub CreateWebRequest(ByVal psResponse As String, poConfig As CServiceConfig)
			
			'Dim username As String = "MLnk100"
			'Dim password As String = "ML100p05T"
			'Dim url As String = "https://lenderweb.staging.lendingtree.com/offers/AutoOfferPost.aspx"
			Dim username As String = poConfig.LendingTreeLogin
			Dim password As String = poConfig.LendingTreePW
			Dim url As String = poConfig.LendingTreeURL

			Dim bIsSumbitted = Common.IsSubmittedSuccessful(psResponse)
			Dim bIsQualified As Boolean = Common.checkIsQualified(psResponse)

			'default to declined; mode =2
			Dim sLendingTreeTemplate As String = String.Format("<LenderResponse><UserName>{0}</UserName><Password>{1}</Password><Mode>{2}</Mode>" _
											   & "<LoanApplicationID>{3}</LoanApplicationID> <LenderID>{4}</LenderID></LenderResponse>", _
											   username, password, 2, _LendingTreeAppID, _LendingTreePartnerID)
			Dim sResponse As String = ""
			'send decline ms to LT
			If Not bIsSumbitted Or Not bIsQualified Then
				Dim oXML As XmlDocument = New XmlDocument()
				oXML.LoadXml(sLendingTreeTemplate)
				Dim oDeclinedXML As New CXmlElement(oXML.DocumentElement)

				If psResponse.Contains("VALIDATION_FAILED") Then
					oDeclinedXML.AddElementValue("CustomizedMessageToBorrower", "Loan not created due to invalid data.")
					Try
						Dim xmlDoc As New XmlDocument
						xmlDoc.LoadXml(psResponse)
						Dim sErrMessage = xmlDoc.GetElementsByTagName("ERROR")(0).InnerText
						_log.Warn("VALIDATION_FAILED: " & sErrMessage)
					Catch ex As Exception

					End Try

				End If

				sResponse = Common.WebPost(oXML, url)
			End If
			''#########################
			'send approved ms to LT
			'mode =1
			'#########
			If bIsSumbitted And bIsQualified Then
				Dim oXML As XmlDocument = New XmlDocument()
				sLendingTreeTemplate = sLendingTreeTemplate.Replace("<Mode>2", "<Mode>1") 'apporved
				oXML.LoadXml(sLendingTreeTemplate)
				Dim oApprovedXML As New CXmlElement(oXML.DocumentElement)
				Dim xmlElementOffers As CXmlElement = oApprovedXML.CreateAndAppendElement("Offers")
				Dim oQualifyProducts As System.Collections.Generic.List(Of String) = getQualifyProduct2_0(psResponse)
				Dim nOfferID As Integer = 0
				Dim sStipulation As String = ""
				For Each oItem As String In oQualifyProducts
					Dim xmlElementOffer As CXmlElement = xmlElementOffers.CreateAndAppendElement("Offer")
					nOfferID += 1
					Dim oItemArray = oItem.Split("|"c)
					xmlElementOffer.AddElementValue("Offerid", nOfferID.ToString)
					xmlElementOffer.AddElementValue("LoanAmount", oItemArray(0))
					xmlElementOffer.AddElementValue("InterestRate", oItemArray(1))
					xmlElementOffer.AddElementValue("Term", oItemArray(2))
					xmlElementOffer.AddElementValue("TermConstant", "1")  'monthly
					xmlElementOffer.AddElementValue("MonthlyPayment", oItemArray(3))
					xmlElementOffer.AddElementValue("MonthlyPaymentOption", "2") 'dollar amount
					xmlElementOffer.AddElementValue("APR", oItemArray(1))
					sStipulation &= oItemArray(5)
				Next
				If sStipulation.Trim <> "" Then
					oApprovedXML.AddElementValue("CustomizedMessageToBorrower", sStipulation.Trim)
				End If

				sResponse = Common.WebPost(oXML, url)
			End If

			'Checkstatus
			'TODO: need to resend if fail on LT side, need to kick off another asynchrounous process -Probably don't need to do this because LPQ respsonse within 10 sec
			'
			Try
				Dim oXML As XmlDocument = New XmlDocument()
				oXML.LoadXml(sResponse)
				Dim xmlElementAck As New CXmlElement(oXML.DocumentElement)
				Dim xmlEmelemtErrorStatusNodes As CXmlElementList = xmlElementAck.SelectElements("ERRSTATUS")

				'success
				If xmlEmelemtErrorStatusNodes(0).GetElementValue("ERRORNUM") = "0" Then
					_log.Info("LT Submission successful for QFNAME#: " & xmlEmelemtErrorStatusNodes(0).GetElementValue("QFNAME"))
					Return
				End If

				'fail
				For Each oItem As CXmlElement In xmlEmelemtErrorStatusNodes
					_log.Warn("LT Submission fail for QFNAME#: " & oItem.GetElementValue("QFNAME") & "  Description: " & oItem.GetElementValue("ERRORDESCRIPTION"))
					Return
				Next
			Catch ex As Exception
				_log.Error("Invalid Response from LT: " & ex.Message, ex)
			End Try

			Return

		End Sub

		

		Private Function ValidateXMLConstruction(ByVal pXML As XmlDocument) As String
			Dim oSchemaVal As New XMLTools.CSchemaValidator
			If oSchemaVal.validate(pXML.OuterXml, New XmlTextReader(Server.MapPath("~/CLF/vehicle_loan.xsd"))) = False Then
				Dim sXMLErrorMessage As String = oSchemaVal.ParseError
				_log.Info("XML Construction failed XSD Validation: " & sXMLErrorMessage)
				Return sXMLErrorMessage
			Else
				Return "" 'pass
			End If
		End Function

		Private Function getQualifyProduct2_0(ByVal xmlResponse As String) As System.Collections.Generic.List(Of String)

			Dim strQualifyProducts As New System.Collections.Generic.List(Of String)
			Dim card_rate As Double = 0.0
			Dim temp_rate As Double = 0.0
			Dim index As Integer = 0
            If Not (Common.checkIsQualified(xmlResponse)) Then Return strQualifyProducts

            Dim doc As New XmlDocument()
			doc.LoadXml(xmlResponse)
			Dim oDecision As XmlElement = doc.SelectSingleNode("/OUTPUT/RESPONSE/DECISION")
			Dim strQualifyCard As String

			'oDecision may be qualify but has no product so null childnode
			Try
				strQualifyCard = oDecision.ChildNodes(0).InnerText
			Catch ex As Exception
				Return strQualifyProducts
			End Try

			Dim cdoc As New XmlDocument()
			cdoc.LoadXml(strQualifyCard)
			Dim oQualifyProducts As XmlNodeList = cdoc.SelectNodes("/PRODUCTS/VL_PRODUCT")
			If oQualifyProducts Is Nothing Then Return strQualifyProducts

			For Each oItem As XmlElement In oQualifyProducts  'prequal
				Dim oProductRate As XmlElement = oItem.SelectSingleNode("RATE")
				Dim sStipulation As String = ""
				Try
					Dim xmlStipulations As XmlNodeList = oItem.GetElementsByTagName("STIPULATION")
					For Each elem As XmlElement In xmlStipulations
						sStipulation &= elem.GetAttribute("stip_text") & "  "
					Next
				Catch ex As Exception
				End Try

				strQualifyProducts.Add(oItem.GetAttribute("amount_approved").Replace("$", "") + "|" + oProductRate.GetAttribute("rate").ToString() + "|" + oItem.GetAttribute("term").ToString() + "|" + oItem.GetAttribute("estimated_monthly_payment").ToString() + "|" + oItem.GetAttribute("max_amount_approved").Replace("$", "") + "|" + sStipulation)
			Next


			Return strQualifyProducts
		End Function
	End Class

End Namespace
