﻿
Imports System.Data
Imports System.IO
Imports System.Linq
Imports System
Imports System.Net
Imports System.Xml
Imports System.Xml.Serialization
Imports System.Threading
Imports LPQMobile.Utils
Imports LPQMobile.BO

Namespace LendingTree
	
	''' <summary>
	''' This web service is called by LPQ lender side to update status on LendingTree side.  This method is needed bc some lender need to push the decision back to LT after a manual decision. 
	''' Funded loan still used this page but will be posted to a different url
	''' Use the loan_number in system node(type="OTHER" which is LT QFName(appId)
	''' </summary>
	Partial Class UpdateStatus
		Inherits System.Web.UI.Page

		Private Shared _log As log4net.ILog = log4net.LogManager.GetLogger(GetType(UpdateStatus))

#Region "Property"
		Private _RawInput As String = Nothing
		Protected ReadOnly Property RawInput() As String
			Get
				If _RawInput Is Nothing Then
					_RawInput = New StreamReader(Request.InputStream).ReadToEnd
				End If

				Return _RawInput
			End Get
		End Property

		Private _inputDoc As Xml.XmlDocument
		Protected ReadOnly Property InputDoc() As Xml.XmlDocument
			Get
				If _inputDoc Is Nothing Then
					'_log.Info(RawInput)

					_inputDoc = New Xml.XmlDocument()
					_inputDoc.LoadXml(RawInput)


				End If

				Return _inputDoc
			End Get
		End Property
#End Region

#Region "CheckWellFormed - Makes sure INPUT is actually XML"
		Protected Sub CheckWellFormed()
			Dim sRequest As String = ""
			Try
				'invoking InputDoc will automatically log the input
				Dim dummy As Xml.XmlDocument = InputDoc
				If dummy.OuterXml = "" Then Throw New Exception("Input was empty.")
				'_log.Info(CSecureStringFormatter.MaskSensitiveXMLData(dummy.OuterXml))
			Catch ex As System.Xml.XmlException
				Throw New Exception("Input was not well formed and xml compliant.")
			End Try
		End Sub

#End Region

		'Private _TransactionType As String = ""	'Mortgage, Home Equity, Personal, Auto, Credit Card, Student Loan
		Private _LendingTreeAppID As String
		'Private _LendingTreePartnerID As String
		Private _LPQLenderID As String

		Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load

			''testing purpose
			If Request.QueryString("vlclf") = "y" Then
				Try
					Dim oXMLDocTest As New XmlDocument
					oXMLDocTest.Load(Server.MapPath("vl_clf_approved.xml"))	 'demo
					_inputDoc = oXMLDocTest
				Catch ex As Exception
					_log.Warn("Can't execute testclf: " & ex.Message, ex)
					SendAcknowledgeResponse("Error")
					Return
				End Try
			End If

			'parse input and get config
			Dim oConfig As New CServiceConfig
			Try
				CheckWellFormed()
				Dim sAppType = InputDoc.DocumentElement.Name
				If sAppType <> "VEHICLE_LOAN" Then
					_log.Warn("LoanType: " & sAppType & " is not supported.")
					SendAcknowledgeResponse("Error")
					Return
				End If


				oConfig = GetConfig()
				If oConfig Is Nothing Then Return
			Catch ex As Exception
				_log.Warn("LendingTree Update Status exception while attempting to parse request. Request: " & Left(CSecureStringFormatter.MaskSensitiveXMLData(InputDoc.OuterXml), 500) & ". Error: " & ex.Message, ex)
				SendAcknowledgeResponse("Error")
				Return
			End Try


			'Send decision back to LendingTree
			Try
				CreateWebRequest(oConfig)
			Catch ex As Exception
				_log.Warn("Exception occurred while attempting to add send decision to LendingTree: " & ex.Message, ex)
				SendAcknowledgeResponse("Error")
				Return
			End Try


			Return

		End Sub

		Private Function GetConfig() As CServiceConfig
			Dim oConfig As New CServiceConfig


			Dim oInputDoc As New CXmlElement(InputDoc.DocumentElement)

			'need to have this inorder to select
			oInputDoc.AddNamespace("default", "http://www.meridianlink.com/CLF")
			oInputDoc.CurrentNamespace = "default"

			'get _LendingTreeAppID
			Dim oOtherSystem As CXmlElement = oInputDoc.SelectSingleElement("SYSTEM[@type='OTHER']")
			If oOtherSystem Is Nothing Then
				_log.Warn("UpdateStatus exception while attempting to parse request for SYSTEM node.")
				SendAcknowledgeResponse("Error")
				Return Nothing
			End If
			_LendingTreeAppID = oOtherSystem.GetAttribute("loan_number")

			'get _LPQLenderID
			Dim oLender As CXmlElement = oInputDoc.SelectSingleElement("SYSTEM/LENDER")
			If oLender Is Nothing Then
				_log.Warn("UpdateStatus exception while attempting to parse request for LENDER node.")
				SendAcknowledgeResponse("Error")
				Return Nothing
			End If
			_LPQLenderID = oLender.GetAttribute("d2p1:internal_id")
			oConfig = oConfig.GetConfigByLPQLenderID(_LPQLenderID)


			If _LendingTreeAppID = "" Or _LendingTreeAppID = Nothing Then
				Throw New Exception("_LendingTreeAppID: " & _LendingTreeAppID & " is not in clf.")
			End If

			If oConfig Is Nothing Then
				Throw New Exception("_LPQLenderID: " & _LPQLenderID & " is not in configuration.")
			End If
			Return oConfig
		End Function

		''' <summary>
		''' Creates a webrequest to push an app descision back out to LT
		''' </summary>
		Private Sub CreateWebRequest(poConfig As CServiceConfig)

			Dim oInputDoc As New CXmlElement(InputDoc.DocumentElement)

			'need to have this inorder to select
			oInputDoc.AddNamespace("default", "http://www.meridianlink.com/CLF")
			oInputDoc.CurrentNamespace = "default"

			'get _LendingTreeAppID
			Dim oFUNDING As CXmlElement = oInputDoc.SelectSingleElement("FUNDING")
			Dim oLoanInfo As CXmlElement = oInputDoc.SelectSingleElement("LOAN_INFO")
			'Dim username As String = "MLnk100"
			'Dim password As String = "ML100p05T"
			'Dim url As String = "https://lenderweb.staging.lendingtree.com/offers/AutoOfferPost.aspx"
			Dim username As String = poConfig.LendingTreeLogin
			Dim password As String = poConfig.LendingTreePW
			Dim url As String = poConfig.LendingTreeURL

			Dim bIsApproved = IsApproved()

			Dim bIsFunded = IsFunded()

			'default to declined; mode =2
			Dim sLendingTreeTemplate As String = String.Format("<LenderResponse><UserName>{0}</UserName><Password>{1}</Password><Mode>{2}</Mode>" _
											   & "<LoanApplicationID>{3}</LoanApplicationID> <LenderID>{4}</LenderID></LenderResponse>", _
											   username, password, 2, _LendingTreeAppID, poConfig.LendingTreePartnerID)

		
			'CustomizedMessageToBorrower
			Dim sExternalComments = oInputDoc.GetElementValue("COMMENTS/EXTERNAL_COMMENTS")
			Dim sStipulations = oInputDoc.GetElementValue("COMMENTS/STIPULATIONS")
			Dim CustomizedMessageToBorrower As String = sExternalComments & "; " & sStipulations

			Dim sResponse As String = ""
			'send decline ms to LT
			If Not bIsApproved Then
				Dim oXML As XmlDocument = New XmlDocument()
				oXML.LoadXml(sLendingTreeTemplate)
				Dim oDeclinedXML As New CXmlElement(oXML.DocumentElement)
				oDeclinedXML.AddElementValue("CustomizedMessageToBorrower", CustomizedMessageToBorrower)

				sResponse = Common.WebPost(oXML, url)
			End If

			

			''#########################
			'send approved ms to LT
			'mode =1
			'#########
			If bIsApproved And Not bIsFunded Then
				Dim oXML As XmlDocument = New XmlDocument()
				sLendingTreeTemplate = sLendingTreeTemplate.Replace("<Mode>2", "<Mode>1") 'approved
				oXML.LoadXml(sLendingTreeTemplate)
				Dim oApprovedXML As New CXmlElement(oXML.DocumentElement)
				Dim xmlElementOffers As CXmlElement = oApprovedXML.CreateAndAppendElement("Offers")
				Dim oQualifyProducts As System.Collections.Generic.List(Of String) = getQualifyProduct()
				Dim nOfferID As Integer = 0
				For Each oItem As String In oQualifyProducts
					Dim xmlElementOffer As CXmlElement = xmlElementOffers.CreateAndAppendElement("Offer")
					nOfferID += 1
					Dim oItemArray = oItem.Split("|"c)
					xmlElementOffer.AddElementValue("Offerid", nOfferID.ToString)
					xmlElementOffer.AddElementValue("LoanAmount", oItemArray(0))
					xmlElementOffer.AddElementValue("InterestRate", oItemArray(1))
					xmlElementOffer.AddElementValue("Term", oItemArray(2))
					xmlElementOffer.AddElementValue("TermConstant", "1")  'monthly
					xmlElementOffer.AddElementValue("MonthlyPayment", oItemArray(3))
					xmlElementOffer.AddElementValue("MonthlyPaymentOption", "2") 'dollar amount
					xmlElementOffer.AddElementValue("APR", oItemArray(1))
				Next
				If CustomizedMessageToBorrower.Trim <> "" Then
					oApprovedXML.AddElementValue("CustomizedMessageToBorrower", CustomizedMessageToBorrower)
				End If
				sResponse = Common.WebPost(oXML, url)
			End If

			'funded loan will need to post to different LT url and use different xml format
			If bIsApproved And bIsFunded Then
				'url = https://lenderweb.staging.lendingtree.com/funding/booking.aspx 'stage
				'url = https://lenderweb.lendingtree.com/funding/booking.aspx ' production
				Dim sHost = url.Substring(0, url.IndexOf(".com/") + 5)
				url = sHost + "funding/booking.aspx"
				_log.Info("Prepare to post FUNDED loan to url: " & url & "   ")

				Dim oXML As XmlDocument = New XmlDocument()
				oXML.LoadXml("<AUTOMATEDFUNDING loadfile=''></AUTOMATEDFUNDING>")
				Dim oXmlRoot As New CXmlElement(oXML.DocumentElement)
				Dim oXmlFundedInfo As CXmlElement = oXmlRoot.CreateAndAppendElement("DATAROW")
				Dim oXmlColumn As CXmlElement = oXmlFundedInfo.CreateAndAppendElement("COLUMN")
				oXmlColumn.SetAttribute("NAME", "QF_NAME")
				oXmlColumn.Source.InnerText = _LendingTreeAppID

				oXmlColumn = oXmlFundedInfo.CreateAndAppendElement("COLUMN")
				oXmlColumn.SetAttribute("NAME", "FUNDED_LOAN_PURPOSE")
				oXmlColumn.Source.InnerText = "1" '1 for Vl TODO: do ofor other loan type

				oXmlColumn = oXmlFundedInfo.CreateAndAppendElement("COLUMN")
				oXmlColumn.SetAttribute("NAME", "FUNDED_DEAL_TYPE")
				oXmlColumn.Source.InnerText = "4" '4 for Vl TODO: do ofor other loan type

				oXmlColumn = oXmlFundedInfo.CreateAndAppendElement("COLUMN")
				oXmlColumn.SetAttribute("NAME", "FUNDED_LOAN_PROGRAM")
				oXmlColumn.Source.InnerText = "0"  '0 for Vl TODO: do ofor other loan type

				oXmlColumn = oXmlFundedInfo.CreateAndAppendElement("COLUMN")
				oXmlColumn.SetAttribute("NAME", "LOAN_AMOUNT")
				oXmlColumn.Source.InnerText = oLoanInfo.GetAttribute("amount_approved")

				oXmlColumn = oXmlFundedInfo.CreateAndAppendElement("COLUMN")
				oXmlColumn.SetAttribute("NAME", "RATE")
				oXmlColumn.Source.InnerText = oLoanInfo.GetAttribute("contract_rate")

				oXmlColumn = oXmlFundedInfo.CreateAndAppendElement("COLUMN")
				oXmlColumn.SetAttribute("NAME", "APR")
				oXmlColumn.Source.InnerText = oFUNDING.GetAttribute("apr")

				oXmlColumn = oXmlFundedInfo.CreateAndAppendElement("COLUMN")
				oXmlColumn.SetAttribute("NAME", "POINTS")
				
				oXmlColumn = oXmlFundedInfo.CreateAndAppendElement("COLUMN")
				oXmlColumn.SetAttribute("NAME", "TERM")
				oXmlColumn.Source.InnerText = oFUNDING.GetAttribute("loan_term")

				oXmlColumn = oXmlFundedInfo.CreateAndAppendElement("COLUMN")
				oXmlColumn.SetAttribute("NAME", "DATE_FUNDED")
				Dim oFundedDate = Common.SafeDate(oFUNDING.GetAttribute("funding_date"))
				oXmlColumn.Source.InnerText = oFundedDate.ToString("MM/dd/yyyy")

				oXmlColumn = oXmlFundedInfo.CreateAndAppendElement("COLUMN")
				oXmlColumn.SetAttribute("NAME", "ORIGINATION_FEE")
				
				oXmlColumn = oXmlFundedInfo.CreateAndAppendElement("COLUMN")
				oXmlColumn.SetAttribute("NAME", "DISCOUNT_POINTS")
				
				oXmlColumn = oXmlFundedInfo.CreateAndAppendElement("COLUMN")
				oXmlColumn.SetAttribute("NAME", "FUNDED_TIER")

				oXmlColumn = oXmlFundedInfo.CreateAndAppendElement("COLUMN")
				oXmlColumn.SetAttribute("NAME", "FUNDED_LTV_RANGE")

				oXmlColumn = oXmlFundedInfo.CreateAndAppendElement("COLUMN")
				oXmlColumn.SetAttribute("NAME", "LOAN_OFFICER_EMAIL")

				oXmlColumn = oXmlFundedInfo.CreateAndAppendElement("COLUMN")
				oXmlColumn.SetAttribute("NAME", "CUSTOMER_EMAIL_ADDRESS")

				oXmlColumn = oXmlFundedInfo.CreateAndAppendElement("COLUMN")
				oXmlColumn.SetAttribute("NAME", "STANDARD_LOAN_PROGRAM_NAME")

				Dim oXmlValidation As CXmlElement = oXmlRoot.CreateAndAppendElement("VALIDATION")
				oXmlValidation.AddElementValue("USERNAME", username)
				oXmlValidation.AddElementValue("PASSWORD", password)
				oXmlValidation.AddElementValue("LENDERUID", poConfig.LendingTreePartnerID)

				sResponse = Common.WebPost(oXML, url)
			End If


			'Checkstatus
			'TODO: need to resend if fail on LT side, need to kick off another asynchrounous process -Probably don't need to do this because LPQ respsonse within 10 sec
			'

			Dim respsonseXML As XmlDocument = New XmlDocument()
			respsonseXML.LoadXml(sResponse)
			Dim xmlElementAck As New CXmlElement(respsonseXML.DocumentElement)
			Dim xmlEmelemtErrorStatusNodes As CXmlElementList = xmlElementAck.SelectElements("ERRSTATUS")
			If xmlEmelemtErrorStatusNodes Is Nothing Then
				SendAcknowledgeResponse("Error")
				Return
			End If

			'success
			If xmlEmelemtErrorStatusNodes(0).GetElementValue("ERRORNUM") = "0" Then
				_log.Info("LT Submission successful for QFNAME#: " & xmlEmelemtErrorStatusNodes(0).GetElementValue("QFNAME"))
				SendAcknowledgeResponse("True")
				Return
			End If

			'fail
			_log.Warn("LT Submission fail for QFNAME#: " & xmlEmelemtErrorStatusNodes(0).GetElementValue("QFNAME") & "  Description: " & xmlEmelemtErrorStatusNodes(0).GetElementValue("ERRORDESCRIPTION"))
			SendAcknowledgeResponse(xmlEmelemtErrorStatusNodes(0).GetElementValue("ERRORDESCRIPTION"))
			Return

		End Sub


		Private Function ValidateXMLConstruction(ByVal pXML As XmlDocument) As String
			Dim oSchemaVal As New XMLTools.CSchemaValidator
			If oSchemaVal.validate(pXML.OuterXml, New XmlTextReader(Server.MapPath("~/CLF/vehicle_loan.xsd"))) = False Then
				Dim sXMLErrorMessage As String = oSchemaVal.ParseError
				_log.Info("XML Construction failed XSD Validation: " & sXMLErrorMessage)
				Return sXMLErrorMessage
			Else
				Return "" 'pass
			End If
		End Function

		'TODO: may need to move to Cenum
		Private Function IsApproved() As Boolean
			For Each oChildNode As XmlNode In InputDoc.DocumentElement.ChildNodes
				If (oChildNode.Name.ToUpper() = "LOAN_STATUS") Then
					Dim loan_status As String = oChildNode.Attributes("loan_status").InnerText
					Select Case loan_status
						Case "PRE-APP", "APP", "AA"
							Return True
						Case Else
							Return False
					End Select
				End If
			Next
			Return False

		End Function

		Private Function IsFunded() As Boolean
			Dim oInputDoc As New CXmlElement(InputDoc.DocumentElement)

			'need to have this inorder to select
			oInputDoc.AddNamespace("default", "http://www.meridianlink.com/CLF")
			oInputDoc.CurrentNamespace = "default"

			'get _LendingTreeAppID
			Dim oFUNDING As CXmlElement = oInputDoc.SelectSingleElement("FUNDING")
			Dim sFundStatus As String = oFUNDING.GetAttribute("funded_status")

			Select Case sFundStatus
				Case "FUN"
					Return True
				Case Else
					Return False
			End Select

		End Function

		Private Function getQualifyProduct() As System.Collections.Generic.List(Of String)

			Dim strQualifyProducts As New System.Collections.Generic.List(Of String)
			Dim card_rate As Double = 0.0
			Dim temp_rate As Double = 0.0
			Dim index As Integer = 0

			For Each oChildNode As XmlNode In InputDoc.DocumentElement.ChildNodes
				If (oChildNode.Name.ToUpper() = "LOAN_INFO") Then
					Dim loanInfoElement As CXmlElement = New CXmlElement(oChildNode)
					strQualifyProducts.Add(loanInfoElement.GetAttribute("amount_approved") + "|" + loanInfoElement.GetAttribute("contract_rate") + "|" + loanInfoElement.GetAttribute("loan_term") + "|" + loanInfoElement.GetAttribute("estimated_vehicle_payment") + "|" + loanInfoElement.GetAttribute("amount_approved"))
				End If
			Next

			Return strQualifyProducts
		End Function


		Private Sub SendAcknowledgeResponse(ByVal psMessage As String)
			Response.Clear()
			Response.Write(psMessage)
			HttpContext.Current.Response.Flush() ' // Sends all currently buffered output to the client.
			HttpContext.Current.Response.SuppressContent = True	' // Gets or sets a value indicating whether to send HTTP content to the client.
			HttpContext.Current.ApplicationInstance.CompleteRequest()
		End Sub


	End Class
	
	
End Namespace
