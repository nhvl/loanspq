﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="ActivateAccount.aspx.vb" Inherits="Sm_ActivateAccount" %>
<%@ Import Namespace="System.Web.Optimization" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1 user-scalable=no"/>
	<link rel="shortcut icon" href="https://app.loanspq.com/logo/lpq_favicon.ico"  type="image/ico"  />
	<%:Styles.Render("~/css/smcss")%>
</head>
<body>
    <div class="container activation-page">
	    <div class="row top30">
			<div class="col-xs-10 col-xs-offset-1 col-md-6 col-md-offset-3 main-form">
				<form id="frmActivateAccount">
					<div>
						<h2>APM Account Activation</h2>
						<%--<p>You are about to activate your account.</p>--%>
						<p>Please update profile and create your own password to finish the activation process.</p>
					</div>
					<div>
						<div class="form-group">
							<label class="required-field">First Name</label>
							<input type="text" class="form-control" name="FirstName" id="txtFirstName" placeholder="First Name" data-message-require="Please fill out this field" data-required="true" maxlength="50" />
						</div>
					</div>
					<div>
						<div class="form-group">
							<label class="required-field">Last Name</label>
							<input type="text" class="form-control" name="LastName" id="txtLastName" placeholder="Last Name" data-message-require="Please fill out this field" data-required="true" maxlength="50" />
						</div>
					</div>
					<div>
						<div class="form-group">
							<label class="required-field">Password</label>
							<input type="password" class="form-control" name="Password" id="txtPassword" data-format="password" data-message-require="Please fill out this field" data-required="true" placeholder="Password" maxlength="50" />
						</div>
					</div>
					<div>
						<div class="form-group">
							<label class="required-field">Confirm Password</label>
							<input type="password" class="form-control" name="ConfirmPassword" data-format="confirmPassword" id="txtConfirmPassword" data-password-not-match="Confirm password is not match." data-message-require="Please fill out this field" data-required="true" placeholder="Confirm Password" maxlength="50" />
						</div>
					</div>
					<div>
						<div class="form-group">
							<label>Avatar</label>
							<div class="logo-upload-zone dropzone" id="divAvatar"></div>
							<input type="hidden" value="" name="Avatar" id="hdAvatar"/>
						</div>
					</div>
					<div>
						<div class="form-group">
							<label class="required-field">Phone Number</label>
							<input type="text" class="form-control" name="Phone" id="txtPhone" data-message-invalid-data="Valid phone number is required" data-format="phone" placeholder="Phone Number" maxlength="20" />
						</div>
					</div>
					<div>
						<input type="hidden" name="Token" id="hdToken" value="<%=tokenStr%>"/>
						<button type="button" id="btnActiveAccount" class="btn btn-primary pull-right">Activate Account</button>
					</div>
				</form>
			</div>
		</div>
    </div>
	<%:Scripts.Render("~/js/smscript")%>
	<script type="text/javascript">
		Dropzone.autoDiscover = false;
		$(function () {
			var currentLogoFile = null;
			registerDataValidator("#frmActivateAccount", "validateActivateAccount");
			$("#divAvatar").dropzone({
				url: "/smauthhandler.aspx",
				dictDefaultMessage: "<p>Drop image file here or click to select image from your computer</p><p>Compatible file types: JPG, PNG. Recommended approximate dimension: 200 x 200.</p><p>Recommended file size: 10KB.</p>",
				maxFilesize: 2,
				acceptedFiles: "image/png,image/jpeg",
				params: { "command": "uploadAvatar" },
				maxFiles: 1,
				addRemoveLinks: true,
				dictRemoveFile: "Remove",
				maxfilesexceeded: function (file) {
					this.removeAllFiles();
					this.addFile(file);
				},
				thumbnailWidth: 200,
				thumbnailHeight: null,
				init: function () {
					currentLogoFile = null;
					this.on("addedfile", function (file) {
						if (currentLogoFile) {
							this.removeFile(currentLogoFile);
						}
						currentLogoFile = file;
					});
					this.on("success", function (file, response) {
						$("#hdAvatar").val(response);
						$.smValidate.hideValidation($("#hdAvatar").closest(".form-group"));
						currentLogoFile = file;
					});
					this.on("removedfile", function (file) {
						currentLogoFile = null;
						if (file.xhr) {
							$.ajax({
								url: "/smauthhandler.aspx",
								async: true,
								cache: false,
								type: 'POST',
								dataType: 'html',
								data: { command: 'deleteAvatar', fileurl: file.xhr.responseText },
								success: function (responseText) {
									$("#hdAvatar").val("");
								}
							});
						} else {
							$("#hdAvatar").val("");
						}
						
					});
				}
			});
			$("#btnActiveAccount").on("click", function () {
				if ($.smValidate("validateActivateAccount") == false) return;
				var userData = $("#frmActivateAccount").serializeJSON({ parseAll: true, checkboxUncheckedValue: "false" });
				//update data
				var postData = {
					command: "activateAccount",
					user_data: _COMMON.toJSON(userData)
				};
				$.ajax({
					url: "/smauthhandler.aspx",
					async: true,
					cache: false,
					type: 'POST',
					dataType: 'html',
					data: postData,
					success: function (responseText) {
						var response = $.parseJSON(responseText);
						if (response.IsSuccess) {
							_COMMON.noty("success", "Success. Redirecting to login screen", 500);
							setTimeout(function() {
								window.location = "login.aspx";
							}, 2000);
						} else if (response.Info == "INVALID") {
							$.smValidate.showValidation($("#txtPassword"), response.Message);
						} else {
							var errorMsg = response.Message !== "" ? response.Message : "Error";
							_COMMON.noty("error", errorMsg, 500);
						}
					}
				});
			});
			function registerDataValidator(container, groupName) {
				var $container = $(container);
				$.smValidate.removeValidationGroup(groupName);
				$("[data-required='true'],[data-format]", $container).each(function (idx, ele) {
					$(ele).observer({
						validators: [
							function (partial) {
								var $self = $(this);
								if ($self.data("required") && $.trim($self.val()) === "") {
									if ($self.data("message-require")) {
										return $self.data("message-require");
									} else {
										return "This field is required";
									}
								}
								var format = $self.data("format");
								if (format) {
									var msg = "Invalid format";
									if ($self.data("message-invalid-data")) {
										msg = $self.data("message-invalid-data");
									}
									if (format === "email") {
										if (_COMMON.ValidateEmail($self.val()) === false) {
											return msg;
										}
									} else if (format === "phone") {
										if (_COMMON.ValidatePhone($self.val()) === false) {
											return msg;
										}
									} else if (format === "password") {
										var result = _COMMON.validatePassword($self.val());
										if (result != "") {
											return result;
										}
									} else if (format === "confirmPassword") {
										if ($self.val() !== $("#txtPassword").val()) {
											return $self.data("password-not-match");
										}
									} else if ($self.val() != "" && format === "positive-number") {
										if ((parseInt($self.val()) > 0) == false) {
											return msg;
										}
									}
								}
								
								return "";
							}
						],
						validateOnBlur: true,
						group: groupName
					});
				});
			}
			$(document).ajaxStart(function () { Pace.restart(); });
		});
	</script>
</body>
</html>
