﻿
Imports System.Net
Imports System.Xml
Imports Microsoft.VisualBasic.Logging

''' <summary>
''' This page will simulate loading xa page for all lenderref in DB so to preload download configuration into cache so the first user will not encounter long latency
''' </summary>
''' <remarks>
''' use task scheduler
''' Program: cmd
''' argument:  -ExecutionPolicy unrestricted -Command "(New-Object Net.WebClient).DownloadString('http://lpqmobile.localhost/keepAlive.aspx')"
''' full:powershell -ExecutionPolicy unrestricted -Command "(New-Object Net.WebClient).DownloadString('http://lpqmobile.localhost/keepAlive.aspx')"
''' powershell -ExecutionPolicy unrestricted -Command "(New-Object Net.WebClient).DownloadString('https://apptest.loanspq.com/keepAlive.aspx')"
''' 
'''</remarks>
Partial Class Sm_KeepAlive
	Inherits System.Web.UI.Page
	Private _log As log4net.ILog = log4net.LogManager.GetLogger(Me.GetType)
	Private _lenderref As String = ""
	Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
		If Not Page.IsPostBack Then
            Try
                Dim smBl As New SmBL
                Dim allLenders As List(Of SmBackOfficeLender) = smBl.GetPortalForCache()

                Dim cachedItems As New Dictionary(Of String, String)
                Dim appServiceDomains = ConfigurationManager.AppSettings.Get("AZURE_APP_SERVICE_DOMAINS")
                Dim appServiceDomainList As New List(Of String)
                If Not String.IsNullOrWhiteSpace(appServiceDomains) Then
                    appServiceDomainList = appServiceDomains.Split(","c).Where(Function(p) Not String.IsNullOrWhiteSpace(p)).Distinct().ToList()
                End If
                If allLenders Is Nothing OrElse Not allLenders.Any() Then
                    _log.Error("Can't get any lender for resync")
                    Return
                End If
                _log.Debug("Total lenders for resync: " & allLenders.Count)
                Dim urlQueue1 As New List(Of String)
                Dim urlQueue2 As New List(Of String)
                'we separate the list to 2 queue because in case we have more than 01 app service. Running the specify url on 1st app service will clean the cache from that app service's memory and cached data in DB also then recached data by downloading it from lender side. But on 2nd app service, don't need to download cache data from lender side anymore, just recycle cache data in its memory.
                For Each lender As SmBackOfficeLender In allLenders
                    Try
                        If lender.PortalList Is Nothing OrElse Not lender.PortalList.Any() Then Continue For
                        Dim key As String = String.Format("_{0}_{1}", lender.LenderID.ToString().Replace("-", "").ToLower(), lender.Host)
                        If cachedItems.ContainsKey(key) Then Continue For
                        Dim portal As SmLenderConfig = lender.PortalList.First()
                        _lenderref = portal.LenderRef

                        If _lenderref.NullSafeToLower_.Contains("zzz") Then Continue For
                        If _lenderref.NullSafeToLower_.Contains("xxxx") Then Continue For

                        Dim lenderConfigXml As New XmlDocument
                        lenderConfigXml.LoadXml(portal.ConfigData)

                        Dim requestUrl As String = String.Empty
                        If lenderConfigXml.SelectSingleNode("WEBSITE/XA_LOAN") IsNot Nothing Then
                            requestUrl = String.Format("/xa/xpressapp.aspx?lenderref={0}&l=1&recyclememory=y", portal.LenderRef)
                        ElseIf lenderConfigXml.SelectSingleNode("WEBSITE/CREDIT_CARD_LOAN") IsNot Nothing Then
                            requestUrl = String.Format("/cc/creditcard.aspx?lenderref={0}&l=1&recyclememory=y", portal.LenderRef)
                        ElseIf lenderConfigXml.SelectSingleNode("WEBSITE/HOME_EQUITY_LOAN") IsNot Nothing Then
                            requestUrl = String.Format("/he/homeequityloan.aspx?lenderref={0}&l=1&recyclememory=y", portal.LenderRef)
                        ElseIf lenderConfigXml.SelectSingleNode("WEBSITE/PERSONAL_LOAN") IsNot Nothing Then
                            requestUrl = String.Format("/pl/personalloan.aspx?lenderref={0}&l=1&recyclememory=y", portal.LenderRef)
                        ElseIf lenderConfigXml.SelectSingleNode("WEBSITE/VEHICLE_LOAN") IsNot Nothing Then
                            requestUrl = String.Format("/vl/vehicleloan.aspx?lenderref={0}&l=1&recyclememory=y", portal.LenderRef)
                        ElseIf lenderConfigXml.SelectSingleNode("WEBSITE/BUSINESS_LOAN") IsNot Nothing Then
                            requestUrl = String.Format("/bl/BusinessLoan.aspx?lenderref={0}&l=1&loantype=card&recyclememory=y", portal.LenderRef)
                        Else
                            Continue For 'this is for scenarios where a portal config doesn't have any LPQ module enabled
                        End If
                        If appServiceDomainList.Count <= 1 Then
                            urlQueue1.Add(String.Format("{0}://{1}{2}&recycledb=y", Request.Url.Scheme, Request.Url.Host, requestUrl))
                        Else
                            Dim idx As Integer = 0
                            For Each domain As String In appServiceDomainList
                                Dim url As String = String.Format("{0}://{1}{2}", Request.Url.Scheme, domain, requestUrl)
                                If idx = 0 Then
                                    urlQueue1.Add(url & "&recycledb=y")
                                Else
                                    urlQueue2.Add(url)
                                End If
                                idx += 1
                            Next
                        End If
                        If Not cachedItems.ContainsKey(key) Then
                            cachedItems.Add(key, portal.ConfigData)
                        End If

                    Catch ex As Exception
                        _log.Warn("Sync load issue for: " & _lenderref, ex)
                    End Try
                Next
                _log.Debug("Total lenders with LPQ for resync: " & urlQueue1.Count)
                For Each url As String In urlQueue1
                    Try
                        If Not String.IsNullOrEmpty(url) Then
                            '_log.Debug("start sync for :" & url)
                            Dim req As WebRequest = WebRequest.Create(url)
                            req.Timeout = 600000  '600,000 millisecond from default = 100sec
                            Dim res As WebResponse = req.GetResponse()
                            _log.Debug(res)
                            Threading.Thread.Sleep(3000)    '3 second delay to reduce loading on the server
                        End If
                    Catch ex As Exception
                        _log.Warn("Sync load(post) issue for: " & url, ex)
                    End Try
                Next
                For Each url As String In urlQueue2
                    Try
                        If Not String.IsNullOrEmpty(url) Then
                            Dim req As WebRequest = WebRequest.Create(url)
                            Dim res As WebResponse = req.GetResponse()
                            _log.Debug(res)
                            Threading.Thread.Sleep(1000)    ' we only need 1 second delay to reduce loading on the server because this request is to recycle memory cached data only
                        End If
                    Catch ex As Exception
                        _log.Warn("Sync load(post) issue for redundant server: " & url, ex)
                    End Try
                Next

            Catch ex As Exception
                _log.Warn("Sync process terminated by lenderref(maybe): " & _lenderref, ex)
            End Try
		End If
	End Sub
End Class
