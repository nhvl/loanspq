﻿
Imports LPQMobile.Utils
Partial Class OnlineUserRegistrationQ2
    Inherits CBasePage

    Private Shared _log As log4net.ILog = log4net.LogManager.GetLogger(GetType(OnlineUserRegistrationQ2))

    'TODO: remove this once we update q2_enrollment_url fro 1st united
    'Const ActionURL = "https://onlinebanking.1stunitedcu.org/1stUnitedServices_ModOnlineE2E/Enroll.aspx"

    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        'If expired session then
        '	Response.Write("Your session has expired")
        'End If

        Try
            Dim sid As String = Request.QueryString("sid")
            If Session(sid) IsNot Nothing Then
                Dim postedData As NameValueCollection = CType(Session(sid), NameValueCollection)
                MemberId.Value = Common.SafeString(postedData.Get("MemberId"))
                MemberNumber.Value = Common.SafeString(postedData.Get("MemberId"))
                SocialSecurity.Value = Common.SafeString(Right(postedData.Get("SSN"), 4)) '1st united only need ssn4
                If _CurrentLenderRef.ToUpper.StartsWith("MAXCU") Then
                    SocialSecurity.Value = Common.FormatSSN(Common.SafeString(postedData.Get("SSN"))) 'full ssn with dash
                End If
                DOB.Value = Common.SafeString(postedData.Get("DOB"))
                ZipCode.Value = Left(Common.SafeString(postedData.Get("AddressZip")), 5)
            End If
            'TODO: clear the session after success to avoid memory leak
        Catch ex As Exception
            _log.Warn("Can't get user info for online user registration", ex)
        End Try
    End Sub

    Protected Sub Page_PreRenderComplete(sender As Object, e As EventArgs) Handles Me.PreRenderComplete

        Dim sUrl = _CurrentWebsiteConfig.GetWebConfigElement().GetAttribute("q2_enrollment_url")
        If String.IsNullOrEmpty(sUrl) Then
            Throw New Exception("Incorrect setup with q2_enrollment_url. It shouldn't be null.")
        End If

        ''need to be here so not to be overwritten by .NET during rendering
        OnlineEnrollment.Action = sUrl

    End Sub
End Class
