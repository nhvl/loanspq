﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="OnlineUserRegistrationDI.aspx.vb" Inherits="OnlineUserRegistrationDI" %>
<%@ Register Src="~/Inc/PageHeader.ascx" TagPrefix="uc" TagName="pageHeader" %>
<%@ Register TagPrefix="uc" TagName="pageFooter" Src="~/Inc/PageFooter.ascx" %>
<%@ Register TagPrefix="uc1" TagName="Piwik" Src="~/Inc/Piwik/PiwikTracking.ascx" %>
<%@ Import Namespace="System.Web.Optimization" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Online User Registration</title>
   	<uc:pageHeader ID="ucPageHeader" runat="server" />
	<%:Styles.Render("~/css/thirdparty/bootstrap") %>
	<%:Styles.Render("~/css/thirdparty/custom") %>
</head>
<body class="lpq_container">
    <input type="hidden" id="hdPlatformSource" value="<%=PlatformSource%>" />
  	<input type="hidden" id="hfLenderRef" value='<%=_CurrentLenderRef%>' />
	<input type="hidden" id="hfLoanOfficerID" value='<%=_CurrentLoanOfficerId%>' />
    <input type="hidden" id="hdHasFooterRight" value="<%=_hasFooterRight%>" />
	<div data-role="page" id="page1">
		<div data-role="header" style="display: none">
			<h1>Online User Registration</h1>
		</div>
		<div data-role="content">
			<%=HeaderUtils.RenderLogoAndSteps(_LogoUrl, -1, "")%>
			<br/>
			<%=HeaderUtils.RenderPageTitle(13, "Online Banking Registration", False)%>
			<div data-role="fieldcontain">
				<label for="txtUserName">User Name<span class="require-span">*</span></label>
				<input id="txtUserName" name="username" type="text"  autocomplete="off" maxlength ="50" readonly="true"  onfocus="if (this.hasAttribute('readonly')) {
    this.removeAttribute('readonly');
    // fix for mobile safari to show virtual keyboard
    this.blur();    this.focus();  }" />
			</div>                 
			<div data-role="fieldcontain">
				<label for="txtPassword">Password<span class="require-span">*</span></label>
				<input id="txtPassword" name="password" type="password" autocomplete="off"   maxlength ="32" readonly="true"  onfocus="if (this.hasAttribute('readonly')) {
    this.removeAttribute('readonly');
    // fix for mobile safari to show virtual keyboard
    this.blur();    this.focus();  }" />
			</div>  
		</div>
         
		
		<div class ="div-continue"  data-role="footer">
			<a href="#"  data-transition="slide" onclick="processRegistration(this);" type="button" class="div-continue-button">Register</a> 
		</div>		
       
	</div>
	<div data-role="page" id="pageSuccess">
		<div data-role="header" style="display: none">
			<h1>Online User Registration</h1>
		</div>
		<div data-role="content">
			<%=HeaderUtils.RenderLogoAndSteps(_LogoUrl, -1, "")%>
			<br/>
			<%=HeaderUtils.RenderPageTitle(13, "Online Banking Registration", False)%>
			<div style="font-size: 16px; padding: 10px;">
				Successfully Registered!
			</div>
		</div>
		<div class ="div-continue" data-role="footer">
			<a href="<%=IIf(String.IsNullOrEmpty(successRedirectUrl), "#", successRedirectUrl)%>" data-transition="slide" type="button" class="div-continue-button">Go to Login page</a> 
		</div>
    </div>
		<!--ui new footer content --> 
	<div class="divfooter">
		<%=_strRenderFooterContact%> 
		<div style="text-align :center"> 
				<!-- lender name -->
			<div class="divLenderName"><%=_strLenderName%></div> 
			<div class="divFooterLogo">            
				<% If String.IsNullOrEmpty(_hasFooterRight) Then%>
					<%-- 
					<div><a href="<%=_strNCUAUrl%>"  class="ui-link" ><%=_strFooterRight%></a><a href="<%=_strHUDUrl%>" class="ui-link"> <img src="<%=_strLogoUrl%>" alt ="Housing Lender" class="footerimage-style img-responsive" /></a></div>                 
					--%> 
					<div class="divfooterlabel"><a href="<%=_strNCUAUrl%>"  class="ui-link" ><%=_strFooterRight%></a> <a href="<%=_strHUDUrl%>" class="ui-link"><%=_equalHousingText%></a></div>                                      
				<% Else%>
					<%=_strFooterRight%>
				<%End If%>
			</div>  
			<div><%=_strFooterLeft  %></div>   
		</div> 
          
	</div> 
		<!--end new ui footer content -->
	<div id="divErrorPopup" data-role="popup" data-dismissible="true" data-history="false" style="max-width: 500px;padding: 20px;">
		<div data-role="header" style="display:none" >
				<h1>Alert Popup</h1>
		</div>
		<div data-role="content">
			<%--<div class="row">
				<div class="col-xs-12 header_theme2">
					<a data-rel="back" class="pull-right"><%=HeaderUtils.IconClose %></a>
				</div>
			</div>--%>
			<div class="row">
				<div class="col-sm-12">
					<div class="DialogMessageStyle">There was a problem with your request</div>
					<div style="margin: 5px 0 15px 0;">
						<span id="txtErrorPopupMessage"></span>
					</div>       
				</div>    
			</div>
			<div class="row text-center">
				<div class="col-xs-12 text-center">
					<a href="#" data-role="button" data-rel="back" type="button">Ok</a>
				</div>
			</div>
		</div>
	</div>

    <uc1:Piwik id="ucPiwik" runat="server" ></uc1:Piwik>      
	<uc:pageFooter ID="ucPageFooter" runat="server" />
	<script type="text/javascript">
		function registerValidator() {
			$('#txtUserName').observer({
				validators: [
					function (partial) {
						var text = $(this).val();
						if (!Common.ValidateText(text)) {
							return 'User Name is required';
						} else if (/^[a-zA-Z0-9]+$/.test(text) == false) {
							return 'Enter a valid user name. Allowed characters: letters "A-Z", number.';
						} else if (text.length < 6)  {
							return 'Password must have at least 6 characters.';
						}
						return "";
					}
				],
				validateOnBlur: true,
				group: "ValidateRegistrationForm"
			});
			$('#txtPassword').observer({
				validators: [
					function (partial) {
						var text = $(this).val();
						var user = $('#txtUserName').val();
						var validatePasswordResult = Common.validatePassword(text, user);
						if (validatePasswordResult.length > 0) {
							return validatePasswordResult;
						}
						return "";
					}
				],
				validateOnBlur: true,
				group: "ValidateRegistrationForm"
			});
		}

		registerValidator();
		function processRegistration() {
			if ($.lpqValidate("ValidateRegistrationForm")) {
				$.ajax({
					type: 'POST',
					url: 'OnlineUserRegistrationDI.aspx',
					dataType: 'json',
					data: {
						sid: '<%=System.Web.Security.AntiXss.AntiXssEncoder.HtmlEncode(Request.QueryString("sid"), True)%>',				
						command: "DoRegistration",
						LenderRef: $('#hfLenderRef').val(),
						UserName: $('#txtUserName').val(),
						Password: $('#txtPassword').val()
					},
					success: function (response) {
						if (response.IsSuccess) {
							goToNextPage("#pageSuccess");
						} else {
							var errMsg = "An error was encountered while processing your request. Please try again.";
							if (response.Info == "BAD_REQUEST") {
								errMsg = "An error was encountered while processing your request. Please try again.";
							} else if(response.Info == "FAILED") {
								errMsg = response.Message;
							}
							if ($("#divErrorPopup").length > 0) {
								$('#txtErrorPopupMessage').html(errMsg);
								$("#divErrorPopup").popup("open", { "positionTo": "window" });
							} else {
								alert(errMsg);
							}
						}
					},
					error: function(err) {
						if ($("#divErrorPopup").length > 0) {
							$('#txtErrorPopupMessage').html("An error was encountered while processing your request. Please try again.");
							$("#divErrorPopup").popup("open", { "positionTo": "window" });
						} else {
							alert("An error was encountered while processing your request. Please try again.");
						}
					}
				});
			}
		}
		$(function() {
			
		});
	</script>
</body>
</html>
