﻿
Imports System.Net
Imports Newtonsoft.Json
Imports LPQMobile.Utils
Imports System.Linq
Imports System.Xml

Partial Class OnlineUserRegistrationDI
	Inherits CBasePage

	Protected _SystemMessage As String
    'Protected _bgTheme As String
    Protected successRedirectUrl As String = ""
    Const TOKEN_PATH As String = "v1/oauth/token"
    Const REGISTRATION_PATH As String = "registration/v4/fis/{di_fiid}/fiCustomers"
    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
		ucPageHeader._currentConfig = _CurrentWebsiteConfig
		ucPageHeader._buttonFontColor = _ButtonFontTheme
		If Not IsPostBack Then
			Dim command As String = Common.SafeString(Request.Params("command"))
			If command = "DoRegistration" Then
				CreateMemberAccount()
			Else
				successRedirectUrl = _CurrentWebsiteConfig.GetWebConfigElement().GetAttribute("di_success_url")
				If String.IsNullOrEmpty(successRedirectUrl) Then
					successRedirectUrl = _CurrentWebsiteConfig.GetWebConfigElement().GetAttribute("finished_url")
				End If
				Dim sid As String = Common.SafeString(Request.QueryString("sid"))

                'for testing only
                'http://lpqmobile.localhost/homeBanking/OnlineUserRegistrationDI.aspx?sid=testing&lenderref=kitsapcu_test
                If sid = "testing" Then
					Dim postedData As New NameValueCollection()
					postedData.Add("LastName", "Marisol12")
					postedData.Add("FirstName", "Test")
					postedData.Add("EmailAddr", "marisol@test.com")
					postedData.Add("AddressStreet", "1234 Square Corle")
					postedData.Add("AddressCity", "Dalas")
					postedData.Add("AddressState", "LA")
					postedData.Add("AddressZip", "222222")
					postedData.Add("ContactMethod", "HomePhone")
					postedData.Add("HomePhone", "2012010215")
					postedData.Add("DOB", "01/01/1990")
					postedData.Add("SSN", "100000114")
					postedData.Add("MotherMaidenName", "Mary")
					postedData.Add("ContactMethod", "HomePhone")
                    postedData.Add("MemberID", "123456789")
                    Session.Add(Common.SafeString(Request.QueryString("sid")), postedData)
				End If
				If String.IsNullOrEmpty(sid) OrElse Session(sid) Is Nothing Then
					'TODO: SUGGEST. in this case, redirect user to somewhere like homepage or whatever because missing valid sid, the request is invalid
					Response.Clear()
					Response.Write("Invalid access")
					Response.End()
				End If

			End If
		End If

	End Sub

	''TODO: convert to handler to be called by the Register btn
	Private Sub CreateMemberAccount()
		log.Debug("Consumer visited the OnlineUserRegistrationDI Page")
		Dim res As New CJsonResponse()
		Try
			Dim sid As String = Common.SafeString(Request.Params("sid"))
			Dim postedData As NameValueCollection = Nothing
			If Not String.IsNullOrEmpty(sid) AndAlso Session(sid) IsNot Nothing Then
				postedData = CType(Session(sid), NameValueCollection)
			End If
			If postedData IsNot Nothing AndAlso postedData.Count > 0 Then
				Dim userName As String = Common.SafeString(Request.Params("UserName"))
				Dim password As String = Common.SafeString(Request.Params("Password"))

				'Config per lender
				Dim fiid As String = _CurrentWebsiteConfig.GetWebConfigElement().GetAttribute("di_fiid")
				'Dim consumerId As String = ConfigurationManager.AppSettings("ConsumerId")
				'Dim consumerSecret As String = ConfigurationManager.AppSettings("ConsumerSecret")
				Dim consumerId As String = _CurrentWebsiteConfig.GetWebConfigElement().GetAttribute("di_consumer_id")
				Dim consumerSecret As String = _CurrentWebsiteConfig.GetWebConfigElement().GetAttribute("di_consumer_secret")

                If String.IsNullOrEmpty(fiid) Or String.IsNullOrEmpty(consumerId) Or String.IsNullOrEmpty(consumerId) Then
                    log.Error("Missing specific  DI configuration for this FI")
                    Throw New Exception("Online DI registration configuration is not available.")
                End If

                Dim sDomain As String = ConfigurationManager.AppSettings("digital_insight_domain_production")
                Dim sAPEnv As String = ConfigurationManager.AppSettings("ENVIRONMENT")
                If sAPEnv <> "LIVE" Then
                    sDomain = ConfigurationManager.AppSettings("digital_insight_domain_staging")
                End If
                Dim sOverWriteDomain As String = _CurrentWebsiteConfig.GetWebConfigElement().GetAttribute("digital_insight_environment")
                If sOverWriteDomain.NullSafeToUpper_ = "PRODUCTION" Then
                    sDomain = ConfigurationManager.AppSettings("digital_insight_domain_production")
                End If
                If sOverWriteDomain.NullSafeToUpper_ = "STAGING" Then
                    sDomain = ConfigurationManager.AppSettings("digital_insight_domain_staging")
                End If

                Dim getTokenUrl As String = sDomain & "/" & TOKEN_PATH
                If String.IsNullOrEmpty(getTokenUrl) Then
                    log.Error("Missing  DI configuration in web setup")
                    Throw New Exception("Online DI registration configuration is not available.")
                End If
                Dim transactionId = Guid.NewGuid
                Dim token = getDIAccessToken(consumerId, consumerSecret, fiid, getTokenUrl, transactionId)
                'Dim sRegistrationUrl As String = ConfigurationManager.AppSettings("RegistrationUrl").Replace("{di_fiid}", fiid)
                Dim sRegistrationUrl As String = sDomain & "/" & REGISTRATION_PATH.Replace("{di_fiid}", fiid)
                Dim result = registerDICustomer(postedData, token, transactionId, sRegistrationUrl, userName, password, fiid)
                    If result = "success" Then
                        'TODO: clear the session after success to avoid memory leak
                        Session.Remove(sid)
                        res = New CJsonResponse(True, "", "OK")
                    Else
                        res = New CJsonResponse(False, result, "FAILED")
                    End If
                Else
                    Throw New Exception("Can't get user info for online user registration")
			End If
        Catch ex As Exception
			log.Info("DI SSO Integration failed.")
			log.Error(" Error in membership creation process: " & ex.ToString)
			log.Error(ex.Message, ex)
			res = New CJsonResponse(False, "Bad Request", "BAD_REQUEST")
		End Try
		Response.Clear()
		Response.Write(JsonConvert.SerializeObject(res))
		Response.End()
	End Sub

#Region "DI Customer Registration"
	Private Function getDIAccessToken(pConsumerId As String, pConsumerSecret As String, pFiid As String, pGetTokenUrl As String, pTransactionId As Guid) As String
        ''If Common.SafeString(Request.Params("sid") = "testing") Then
        ''	'for testing only
        ''	Return "xxx"
        ''End If

        Dim sUrl As String = pGetTokenUrl
        log.InfoFormat("Getting DI Access Token with from {0} for transaction id: {1}", sUrl, pTransactionId.ToString)

        'Use Basic Authentication to request for OAuth2.0 token
        Dim sAuth As String = String.Format("{0}:{1}", pConsumerId, pConsumerSecret)
		Dim sEnc As String = Common.SafeString(Convert.ToBase64String(Encoding.Default.GetBytes(sAuth)))
		Dim BasicAuth = String.Format("Basic {0}", sEnc)
		log.DebugFormat("BasicAuth: {0}", BasicAuth)
		'Basic ajVzc3RIeTR5RERLZlRhNEhHc3FXZGhpa2RkWUZGTGo6dm8wNWNHRlBHSGVnMWlmbQ==  for test credetnial

		Dim oWebRequest As New CWebRequest()
		oWebRequest.Params.Add("grant_type", "client_credentials")
		oWebRequest.Headers.Add("di_fiid", pFiid)
		oWebRequest.Headers.Add("di_tid", pTransactionId.ToString)
		'oWebRequest.Headers.Add("di_tid", "8313e173-c863-4a38-80be-afd59331e87a")
		oWebRequest.Headers.Add("Authorization", BasicAuth)
		oWebRequest.PostData(sUrl)

		Dim resp = Common.SafeString(oWebRequest.ResponseText)
		'getDIAccessToken response: <?xml version="1.0" encoding="UTF-8"?><token><di_fiid>01359</di_fiid><access_token>iWbod5ABdC3ifJrC00R7R9pArthr</access_token><expires_in>18000</expires_in></token>

		Dim xml = New System.Xml.XmlDocument
		xml.LoadXml(resp)

		Dim accessToken = xml.GetElementsByTagName("access_token")
		Dim token As String = ""

		If (accessToken IsNot Nothing AndAlso accessToken(0).InnerText <> "") Then
			token = accessToken(0).InnerText
			log.InfoFormat("Retrieved token {0}", token)
		Else
			log.InfoFormat("No token received from response {0}", resp)
		End If

		Return token
	End Function

    Private Function registerDICustomer(postedData As NameValueCollection, pToken As String, pTransactionId As Guid, pRegistrationUrl As String, userName As String, password As String, psfiid As String) As String
        log.InfoFormat("begin registerDICustomer with transactionId {0}", pTransactionId.ToString)

        Dim root = <FICustomer xmlns="http://schema.intuit.com/domain/banking/fiCustomer/v2" xmlns:ns2="http://schema.intuit.com/fs/common/v2">
                       <id>0</id>
                       <fiId>01359</fiId>
                       <memberNumber></memberNumber>
                       <person>
                           <ns2:personName>
                               <ns2:lastName></ns2:lastName>
                               <ns2:firstName></ns2:firstName>
                               '<ns2:middleName></ns2:middleName>
                           </ns2:personName>
                           <ns2:contactInfo>
                               <ns2:emailAddress></ns2:emailAddress>
                               <ns2:postalAddress>
                                   <ns2:address1></ns2:address1>
                                   <ns2:street></ns2:street>
                                   <ns2:city></ns2:city>
                                   <ns2:state></ns2:state>
                                   <ns2:postalCode></ns2:postalCode>
                                   <ns2:country></ns2:country>
                               </ns2:postalAddress>
                               <ns2:phoneNumber>
                                   <ns2:number></ns2:number>
                               </ns2:phoneNumber>
                           </ns2:contactInfo>
                           <ns2:birthDate></ns2:birthDate>
                       </person>
                       <channelInfos>
                           <channelInfo>
                               <channelType>TPV_API</channelType>
                               <credential>
                                   <ns2:loginId></ns2:loginId>
                                   <ns2:password></ns2:password>
                               </credential>
                           </channelInfo>
                       </channelInfos>
                       <billPayEnabled>false</billPayEnabled>
                       <acceptedDisclosure>true</acceptedDisclosure>
                       <userType>PRIMARY</userType>
                       <ssn></ssn>
                       <motherMaidenName></motherMaidenName>
                       <hostCredential>
                           <ns2:password></ns2:password>
                       </hostCredential>
                   </FICustomer>


        'do mapping here
        Dim defaultNs = root.GetDefaultNamespace
        Dim ns2 As XNamespace = "http://schema.intuit.com/fs/common/v2"

        'root.Element(defaultNs + "memberNumber").SetValue(Common.SafeString(postedData.Get("MemberId")))
        root.Element(defaultNs + "fiId").SetValue(Common.SafeString(psfiid))
        root.Element(defaultNs + "memberNumber").SetValue(Common.SafeString(postedData.Get("MemberID")))

        root.Descendants(ns2 + "lastName").First.SetValue(Common.SafeString(postedData.Get("LastName")))
        root.Descendants(ns2 + "firstName").First.SetValue(Common.SafeString(postedData.Get("FirstName")))
        'root.Descendants(ns2 + "middleName").First.SetValue(Common.SafeString(postedData.Get("MiddleName")))

        root.Descendants(ns2 + "emailAddress").First.SetValue(Common.SafeString(postedData.Get("EmailAddr")))

        'TODO:break AddressStreet into 2 address1 and street
        'Dim sIndex As Integer = Common.SafeString(postedData.Get("AddressStreet")).IndexOf(" ")
        'If sIndex > 0 Then
        '	root.Descendants(ns2 + "address1").First.SetValue(Common.SafeString(postedData.Get("AddressStreet")).Substring(0, sIndex))
        '	root.Descendants(ns2 + "street").First.SetValue(Common.SafeString(postedData.Get("AddressStreet")).Substring(sIndex + 1))
        'Else
        '	root.Descendants(ns2 + "address1").First.SetValue(Common.SafeString(postedData.Get("AddressStreet")))
        '	root.Descendants(ns2 + "street").First.SetValue(Common.SafeString(postedData.Get("AddressStreet")))
        'End If
        'TODO: LPQ is usning the same FirstLineAddress for both
        root.Descendants(ns2 + "address1").First.SetValue(Common.SafeString(postedData.Get("AddressStreet")))
        root.Descendants(ns2 + "street").First.SetValue(Common.SafeString(postedData.Get("AddressStreet")))
        root.Descendants(ns2 + "city").First.SetValue(Common.SafeString(postedData.Get("AddressCity")))
        root.Descendants(ns2 + "state").First.SetValue(Common.SafeString(postedData.Get("AddressState")))
        root.Descendants(ns2 + "postalCode").First.SetValue(Common.SafeString(postedData.Get("AddressZip")))
        root.Descendants(ns2 + "country").First.SetValue("US")

        'TODO: select the preferred contact for mapping
        Dim phoneValue As String = ""
        Select Case Common.SafeString(postedData.Get("ContactMethod"))
            Case "HomePhone"
                phoneValue = Common.SafeString(postedData.Get("HomePhone"))
            Case "CellPhone"
                phoneValue = Common.SafeString(postedData.Get("MobilePhone"))
            Case "WorkPhone"
                phoneValue = Common.SafeString(postedData.Get("WorkPhone"))
            Case Else
                phoneValue = Common.SafeString(postedData.Get("HomePhone"))
        End Select
        log.InfoFormat("Applicant Phone before cleanup {0}", phoneValue)
        phoneValue = phoneValue.Replace("-"c, "")
        phoneValue = phoneValue.Replace(" "c, "")
        phoneValue = phoneValue.Replace("("c, "")
        phoneValue = phoneValue.Replace(")"c, "")
        log.InfoFormat("Applicant Phone after cleanup {0}", phoneValue)

        root.Descendants(ns2 + "number").First.SetValue(phoneValue)
        root.Descendants(ns2 + "birthDate").First.SetValue(CDate(Common.SafeDate(postedData.Get("DOB"))).ToString("yyyy-MM-dd"))

        Dim cred = root.Descendants(defaultNs + "credential").First

        cred.Element(ns2 + "loginId").SetValue(userName)
        cred.Element(ns2 + "password").SetValue(password)

        root.Element(defaultNs + "ssn").SetValue(Common.SafeString(postedData.Get("SSN")))
        root.Element(defaultNs + "motherMaidenName").SetValue(Common.SafeString(postedData.Get("MotherMaidenName")))

        'root.Element(defaultNs + "motherMaidenName").SetValue("mosadasd")

        'TODO: not sure how this will be used by FI
        Dim hostCred = root.Element(defaultNs + "hostCredential")
        Dim sHostPW As String = "123456" 'default for backward compatible 
        Dim sHostPinPolicy As String = _CurrentWebsiteConfig.GetWebConfigElement().GetAttribute("digital_insight_host_pin_policy")
        If Not String.IsNullOrEmpty(sHostPinPolicy) AndAlso sHostPinPolicy = "SSN4" Then
            sHostPW = Common.SafeString(Right(Common.SafeString(postedData.Get("SSN")), 4)) 'Wright Patt wants SSN4
        End If
        If Not String.IsNullOrEmpty(sHostPinPolicy) AndAlso sHostPinPolicy = "SSN6" Then
            sHostPW = Common.SafeString(Right(Common.SafeString(postedData.Get("SSN")), 6)) '
        End If

        hostCred.Element(ns2 + "password").SetValue(sHostPW)

        'OAuth 2.0
        Dim sAuthToken = String.Format("Bearer {0}", pToken)

        Dim oWebRequest As New CWebRequest()
        oWebRequest.Headers.Add("di_tid", pTransactionId.ToString)
        oWebRequest.Headers.Add("Authorization", sAuthToken)



        Dim sURL As String = "https://diapis.stg1.digitalinsight.com/registration/v4/fis/01359/fiCustomers"
        sURL = pRegistrationUrl

        Dim reqXML = root.ToXmlDocument_
        log.InfoFormat("Outgoing XML: {0}", reqXML.OuterXml)
        log.InfoFormat("Outgoing URL: {0}", sURL)

        Try
            ''for testing only
            ''If Common.SafeString(Request.Params("sid") = "testing") Then
            ''	'Return "success"
            ''	Throw New WebException("testing")
            ''End If
            oWebRequest.PostData(sURL, reqXML, Nothing, "application/xml")
            Dim sResp = oWebRequest.ResponseText
            log.InfoFormat("Success, DI Returned: {0}", sResp)
            'can do more parsing if we need to store any info
            'may need to analyse ResponseText to determine whether the request is success or not
            Return "success"
        Catch e As System.Net.WebException
            log.Debug("This program is expected to throw WebException on successful run." + vbLf & vbLf & "Exception Message :" + e.Message)
            Dim respResult As String = ""

            If e.Response Is Nothing OrElse e.Status <> System.Net.WebExceptionStatus.ProtocolError Then
                Throw New Exception("Invalid response")
            End If

            Dim resp As System.Net.HttpWebResponse = DirectCast(e.Response, System.Net.HttpWebResponse)


            Using streamReader As New System.IO.StreamReader(resp.GetResponseStream)
                respResult = streamReader.ReadToEnd()
            End Using

            If e.Status = System.Net.WebExceptionStatus.ProtocolError Then
                log.Info("Status Code : " & CType(e.Response, System.Net.HttpWebResponse).StatusCode)
                log.Info("Status Description : " & CType(e.Response, System.Net.HttpWebResponse).StatusDescription)
                log.Info("Status respsonse : " & respResult)    ' this provide the statusMessage and 	errorInfo			
            End If

            ''for testing only
            ''If Common.SafeString(Request.Params("sid") = "testing") Then
            ''	respResult = "<Status xmlns=""http://schema.intuit.com/fs/common/v2"" xmlns:ns2=""http://schema.intuit.com/domain/banking/preference/V2""><statusMessage>Mother Maiden Name not found in input</statusMessage><errorInfo><errorType>APP_ERROR</errorType><errorCode>220012</errorCode><errorMessage>Mother Maiden Name not found in input</errorMessage></errorInfo></Status>"
            ''End If

            Dim xmlDoc As New XmlDocument
            xmlDoc.LoadXml(respResult)
            Dim statusMessageNode As XmlNode
            If xmlDoc.DocumentElement.HasAttribute("xmlns") Then
                Dim nsmgr As New XmlNamespaceManager(xmlDoc.NameTable)
                nsmgr.AddNamespace("ns2", "http://schema.intuit.com/fs/common/v2")
                statusMessageNode = xmlDoc.SelectSingleNode("//ns2:Status/ns2:statusMessage", nsmgr)
            Else
                statusMessageNode = xmlDoc.SelectSingleNode("Status/statusMessage")
            End If
            If statusMessageNode IsNot Nothing Then
                Return statusMessageNode.InnerText
            End If
            Throw New Exception("Can not extract status message")
        Catch ex As Exception
            log.Error("OnlineUserRegistrationDI", ex)
            Throw New Exception("Bad request")
        End Try




        '<Status xmlns="http://schema.intuit.com/fs/common/v2" xmlns:ns2="http://schema.intuit.com/domain/banking/preference/V2">
        '    <statusMessage>Mother Maiden Name not found in input</statusMessage>
        '    <errorInfo>
        '        <errorType>APP_ERROR</errorType>
        '        <errorCode>220012</errorCode>
        '        <errorMessage>Mother Maiden Name not found in input</errorMessage>
        '    </errorInfo>
        '</Status>

        '<Status xmlns="http://schema.intuit.com/fs/common/v2" xmlns:ns2="http://schema.intuit.com/domain/banking/preference/V2">
        '    <statusMessage>You are already a registered user.</statusMessage>
        '    <errorInfo>
        '        <errorType>APP_ERROR</errorType>
        '        <errorCode>26331</errorCode>
        '        <errorMessage>You are already a registered user.</errorMessage>
        '    </errorInfo>
        '</Status>


        '		<Status xmlns="http://schema.intuit.com/fs/common/v2" xmlns:ns2="http://schema.intuit.com/domain/banking/preference/V2">
        '    <statusMessage>Header 'di_tid' invalid</statusMessage>
        '    <errorInfo>
        '        <errorType>USER_ERROR</errorType>
        '        <errorCode>90002</errorCode>
        '        <errorMessage>Header 'di_tid' invalid</errorMessage>
        '    </errorInfo>
        '</Status>

        '		<Status xmlns="http://schema.intuit.com/fs/common/v2" xmlns:ns2="http://schema.intuit.com/domain/banking/preference/V2">
        '    <statusMessage>oauth token is not valid</statusMessage>
        '    <errorInfo>
        '        <errorType>USER_ERROR</errorType>
        '        <errorCode>90001</errorCode>
        '        <errorMessage>oauth token is not valid or expired</errorMessage>
        '    </errorInfo>
        '</Status>

        '		<Status xmlns="http://schema.intuit.com/fs/common/v2" xmlns:ns2="http://schema.intuit.com/domain/banking/preference/V2">
        '    <statusMessage>LoginID is already taken, please use another</statusMessage>
        '    <errorInfo>
        '        <errorType>APP_ERROR</errorType>
        '        <errorCode>26201</errorCode>
        '        <errorMessage>LoginID is already taken, please use another</errorMessage>
        '    </errorInfo>
        '</Status>
    End Function


#End Region


End Class
