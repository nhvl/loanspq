﻿
5/22/17
USE [LPQMobile]
GO

/****** Object:  Table [dbo].[LenderConfigs]    Script Date: 5/22/2017 1:05:25 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[LenderConfigs](
	[LenderConfigID] [uniqueidentifier] NOT NULL,
	[OrganizationID] [uniqueidentifier] NOT NULL,
	[LenderID] [uniqueidentifier] NOT NULL,
	[LenderRef] [varchar](50) NULL,
	[ReturnURL] [varchar](100) NOT NULL,
	[LPQLogin] [varchar](50) NOT NULL,
	[LPQPW] [varchar](50) NOT NULL,
	[LPQURL] [varchar](100) NOT NULL,
	[ConfigData] [varchar](max) NOT NULL,
	[note] [varchar](200) NULL,
	[LastModifiedDate] [datetime] NOT NULL CONSTRAINT [DF_LenderConfigs_LastModifiedDate]  DEFAULT (((12)/(12))/(1970)),
	[CreatedDate] [datetime] NOT NULL CONSTRAINT [uc_CreatedDate]  DEFAULT ('12/12/1970'),
	[LastModifiedBy] [varchar](100) NULL,
	[LastOpenedBy] [varchar](100) NULL,
	[IterationID] [int] NULL,
	[LastOpenedDate] [datetime] NULL,
 CONSTRAINT [PK_LenderConfigs] PRIMARY KEY CLUSTERED 
(
	[LenderConfigID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
 CONSTRAINT [uc_LenderID_lenderRef] UNIQUE NONCLUSTERED 
(
	[LenderConfigID] ASC,
	[LenderRef] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
 CONSTRAINT [uc_LenderRef] UNIQUE NONCLUSTERED 
(
	[LenderRef] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

8/17/17 rev#3769
CREATE TABLE [LPQMobile].[dbo].[LenderConfigDrafts](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[LenderID] [varchar](50) NULL,
	[LenderRef] [varchar](50) NULL,
	[CreatedBy] [varchar](50) NULL,
	[CreatedDate] [datetime] NOT NULL,
	[NodePath] [varchar](200) NOT NULL,
	[NodeContent] [varchar](max) NOT NULL,
	[Status] [varchar](50) NULL,
 CONSTRAINT [PK_LenderConfigDraft] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO


SET ANSI_PADDING OFF
GO

ALTER TABLE [LPQMobile].[dbo].[LenderConfigDrafts] ADD  CONSTRAINT [DF_LenderConfigDraft_CreatedDate]  DEFAULT (getdate()) FOR [CreatedDate]
GO

8/30 re#3809(preview TABLE), 
schedule task to clean up data in this table for those records having CreatedDate > Today - 30days

  CREATE TABLE [LPQMobile].[dbo].[LenderConfigPreviews](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[PreviewCode] [varchar](20) NOT NULL,
	[ConfigData] [varchar](max) NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[LenderID] [uniqueidentifier] NULL,
	[LenderRef] [varchar](50) NULL,
	[LenderConfigID] [uniqueidentifier] NOT NULL,
	[OrganizationID] [uniqueidentifier] NOT NULL,
	[ReturnURL] [varchar](100) NULL,
	[LPQLogin] [varchar](50) NULL,
	[LPQPW] [varchar](50) NULL,
	[LPQURL] [varchar](100) NULL,
	[note] [varchar](200) NULL,
	[LastModifiedDate] [datetime] NULL,
	[LastModifiedBy] [varchar](50) NULL,
	[LastOpenedBy] [varchar](50) NULL,
	[IterationID] [int] NULL,
	[LastOpenedDate] [datetime] NULL,
 CONSTRAINT [PK_LenderConfigPreviews] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

ALTER TABLE [LPQMobile].[dbo].[LenderConfigPreviews] ADD  CONSTRAINT [DF_LenderConfigPreviews_CreatedDate]  DEFAULT (getdate()) FOR [CreatedDate]
GO

9/11 Update Audit feature to use LenderConfigID/lenderRef instead of LenderID
--update to LenderConfigLogs so we can link to LenderConfig table via LenderConfigID instead of LenderID
--1. rename LenderConfigID to LenderConfigLogId via UI
--2. Add LenderConfigID: unique, allow null
--3. fill lenderConfigID with data via some method
--4. Change LenderConfigID to not allow null

9/15/2017 rev#3858


--Implement Mobile SiteManager (PART 4)
--Implement authentication/authorization feature
--Implement back office  module

-------------------------
use [LPQMobile]
/****** Object:  Table [dbo].[ActionLogs]    Script Date: 9/15/2017 11:40:01 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[ActionLogs](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[TransactionDate] [datetime] NOT NULL,
	[CreatedByUserID] [uniqueidentifier] NOT NULL,
	[IPAddress] [varchar](20) NOT NULL,
	[Action] [varchar](1000) NULL,
	[ActionNote] [nvarchar](4000) NULL,
	[Category] [varchar](100) NULL,
 CONSTRAINT [PK_ActionLogs] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

ALTER TABLE [dbo].[ActionLogs] ADD  CONSTRAINT [DF_ActionLogs_TransactionDate]  DEFAULT (getdate()) FOR [TransactionDate]
GO
--===================================================================================================================================
use [LPQMobile]
/****** Object:  Table [dbo].[BackOfficeLenders]    Script Date: 9/15/2017 11:40:13 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[BackOfficeLenders](
	[BackOfficeLenderID] [uniqueidentifier] NOT NULL,
	[LenderName] [varchar](50) NOT NULL,
	[LenderID] [uniqueidentifier] NOT NULL,
	[LpqUrl] [varchar](200) NULL,
	[City] [varchar](50) NULL,
	[State] [varchar](50) NULL,
 CONSTRAINT [PK_BackOfficeLenders] PRIMARY KEY CLUSTERED 
(
	[BackOfficeLenderID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

--===================================================================================================================================

ALTER TABLE LenderConfigDrafts DROP COLUMN CreatedBy

ALTER TABLE LenderConfigDrafts ADD CreatedByUserID UNIQUEIDENTIFIER

--===================================================================================================================================

/****** Object:  Table [dbo].[LenderConfigUsers]    Script Date: 9/15/2017 11:45:12 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[LenderConfigUsers](
	[ID] [uniqueidentifier] NOT NULL,
	[LenderConfigID] [uniqueidentifier] NOT NULL,
	[UserID] [uniqueidentifier] NOT NULL,
	[Status] [int] NOT NULL,
 CONSTRAINT [PK_LenderConfigUsers] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

--===================================================================================================================================

/****** Object:  Table [dbo].[UserActivateTokens]    Script Date: 9/15/2017 11:45:36 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[UserActivateTokens](
	[Token] [varchar](50) NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[Status] [int] NULL,
	[Email] [varchar](200) NULL,
	[ActivatedDate] [datetime] NULL,
	[UserID] [uniqueidentifier] NOT NULL,
 CONSTRAINT [PK_UserActivateTokens] PRIMARY KEY CLUSTERED 
(
	[Token] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

ALTER TABLE [dbo].[UserActivateTokens] ADD  CONSTRAINT [DF_UserActivateTokens_CreatedDate]  DEFAULT (getdate()) FOR [CreatedDate]
GO


--===================================================================================================================================

/****** Object:  Table [dbo].[Users]    Script Date: 9/15/2017 11:46:01 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[Users](
	[Email] [varchar](100) NOT NULL,
	[Password] [varchar](100) NOT NULL,
	[Salt] [char](32) NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[LastLoginDate] [datetime] NULL,
	[Status] [tinyint] NOT NULL,
	[Role] [varchar](500) NULL,
	[FirstName] [varchar](50) NULL,
	[LastName] [varchar](50) NULL,
	[Phone] [varchar](50) NULL,
	[LoginFailedCount] [int] NOT NULL,
	[ExpireDate] [datetime] NULL,
	[UserID] [uniqueidentifier] NOT NULL,
 CONSTRAINT [PK_Users_1] PRIMARY KEY CLUSTERED 
(
	[UserID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

ALTER TABLE [dbo].[Users] ADD  CONSTRAINT [DF_Users_Password]  DEFAULT ('') FOR [Password]
GO

ALTER TABLE [dbo].[Users] ADD  CONSTRAINT [DF_Users_Salt]  DEFAULT ('') FOR [Salt]
GO

ALTER TABLE [dbo].[Users] ADD  CONSTRAINT [DF_Table_1_DateCreated]  DEFAULT (getdate()) FOR [CreatedDate]
GO

ALTER TABLE [dbo].[Users] ADD  CONSTRAINT [DF_Users_Status]  DEFAULT ((0)) FOR [Status]
GO

ALTER TABLE [dbo].[Users] ADD  CONSTRAINT [DF_Users_Retry]  DEFAULT ((0)) FOR [LoginFailedCount]
GO


--===================================================================================================================================
---INITIAL USER: 
--USERNAME: test@gmail.com
--PASSWORD: 123456
  INSERT INTO Users(Email, Password, Salt, CreatedDate, LastLoginDate, Status, Role, FirstName, LastName, Phone, LoginFailedCount, ExpireDate, UserID)
  VALUES('tonyn@meridianlink.com','D8622D4046F5C88BD67339A49400DAB28384757CBDAACBEE6F7920DD905514C7','TDDlweVY0dXH4LwouBzrhQ80KRNq1S/5',getdate(), null, 1, 'SuperOperator', 'TonyDev', 'Ng', '7777777777', '0', NULL, NEWID())

  --TODO:drop SupportOfficers, 

--DB SCRIPT rev#3860
use [LPQMobile]
alter table Users drop column ExpireDate
alter table Users add ExpireDays int not null default 120

--rev3861
ALTER TABLE ActionLogs ADD ObjectID VARCHAR(50) null

rev#3878
ALTER TABLE LenderConfigDrafts add NodeGroup varchar(50) NULL

rev#3883
ALTER TABLE LenderConfigDrafts ADD LenderConfigID UNIQUEIDENTIFIER

Rev4000 Activate account feature LPQUsers - to be delete later

CREATE TABLE [dbo].[LPQUsers](
	[LPQUserId] [uniqueidentifier] NOT NULL,
	[OrganizationID] [uniqueidentifier] NOT NULL,
	[LenderID] [uniqueidentifier] NULL,
	[LName] [varchar](50) NOT NULL,
	[FName] [varchar](50) NOT NULL,
	[Email] [varchar](100) NOT NULL,
	[Phone] [varchar](20) NULL,
	[LastLoginDate] [datetime] NULL,
	[Host] [varchar](100) NULL,
 CONSTRAINT [PK_LPQUsers] PRIMARY KEY CLUSTERED 
(
	[LPQUserId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]

) ON [PRIMARY]


-------
rev 4149 (revision control
USE [LPQMobile]
GO
ALTER TABLE LenderConfigDrafts
ADD RevisionID INT NOT NULL DEFAULT(0)

GO

ALTER TABLE LenderConfigLogs
ADD RevisionID INT NOT NULL DEFAULT(0)

GO

ALTER TABLE LenderConfigLogs
ADD Comment nvarchar(max) NULL

GO

ALTER TABLE LenderConfigs
ADD RevisionID INT NOT NULL DEFAULT(1)

GO


/***rev4410 3/28/18 Userroles & lenderVendor***/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[UserRoles](
	[UserRoleID] [uniqueidentifier] NOT NULL,
	[Role] [varchar](500) NOT NULL,
	[LenderID] [uniqueidentifier] NOT NULL,
	[LenderConfigID] [uniqueidentifier] NULL,
	[VendorID] [varchar](50) NULL,
	[UserID] [uniqueidentifier] NOT NULL,
	[Status] [tinyint] NULL,
 CONSTRAINT [PK_UserRoles] PRIMARY KEY CLUSTERED 
(
	[UserRoleID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

ALTER TABLE [dbo].[UserRoles] ADD  CONSTRAINT [DF_UserRoles_Status]  DEFAULT ((1)) FOR [Status]
GO


/****** Object:  Table [dbo].[LenderVendors]    Script Date: 3/28/2018 3:45:49 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[LenderVendors](
	[LenderVendorID] [uniqueidentifier] NOT NULL,
	[VendorID] [varchar](50) NOT NULL,
	[LenderID] [uniqueidentifier] NOT NULL,
	[VendorName] [varchar](200) NOT NULL,
	[City] [varchar](200) NULL,
	[Zip] [varchar](10) NULL,
	[State] [varchar](50) NULL,
	[Phone] [varchar](50) NULL,
	[Fax] [varchar](50) NULL,
	[DefaultLpqWorkerID] [varchar](50) NULL,
	[DealerNumberLpq] [varchar](50) NULL,
	[Status] [tinyint] NULL,
	[Logo] [varchar](200) NULL,
	[Type] [varchar](50) NULL,
 CONSTRAINT [PK_LenderVendors] PRIMARY KEY CLUSTERED 
(
	[LenderVendorID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

ALTER TABLE [dbo].[LenderVendors] ADD  CONSTRAINT [DF_LenderVendors_Status]  DEFAULT ((0)) FOR [Status]
GO

ALTER TABLE BackOfficeLenders
ADD Host varchar(200) NULL



Rev#4282  MFA in status
CREATE TABLE [dbo].[GetLoanStatusSecurityCodes](
	[Code] [varchar](20) NOT NULL,
	[LenderRef] [varchar](50) NOT NULL,
	[Email] [varchar](200) NOT NULL,
	[LastName] [varchar](200) NOT NULL,
	[Status] [int] NOT NULL,
	[GeneratedDate] [datetime] NOT NULL,
	[ConfirmedDate] [datetime] NULL,
 CONSTRAINT [PK_SecurityCodes] PRIMARY KEY CLUSTERED 
(
	[Code] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

ALTER TABLE [dbo].[GetLoanStatusSecurityCodes] ADD  CONSTRAINT [DF_SecurityCodes_Status]  DEFAULT ((0)) FOR [Status]
GO

ALTER TABLE [dbo].[GetLoanStatusSecurityCodes] ADD  CONSTRAINT [DF_SecurityCodes_GeneratedDate]  DEFAULT (getdate()) FOR [GeneratedDate]
GO


/**Rev 4454 4/9/18  , 4461 Loans for Vendor***/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[Loans](
	[ID] [uniqueidentifier] NOT NULL,
	[LenderID] [uniqueidentifier] NOT NULL,
	[LenderVendorID] [uniqueidentifier] NOT NULL,
	[UserID] [uniqueidentifier] NOT NULL,
	[ApplicantName] [varchar](2000) NULL,
	[CreatedDate] [datetime] NULL,
	[SubmitDate] [datetime] NULL,
	[LoanType] [varchar](50) NULL,
	[LoanNumber] [varchar](50) NULL,
	[LoanID] [varchar](100) NULL,
	[RequestParam] [varchar](max) NULL,
	[RequestParam_Enc] [varchar](max) NULL,
	[XmlResponse] [varchar](max) NULL,
	[Status] [varchar](50) NULL,
	[StatusDate] [datetime] NULL,
	[FundStatus] [varchar](50) NULL,
	[FundDate] [datetime] NULL,
 CONSTRAINT [PK_Loans] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO
ALTER TABLE LenderVendors
ADD Address varchar(500) NULL

--delete all record from Loans table (if any)

ALTER TABLE Loans
DROP COLUMN LenderVendorID

ALTER TABLE LenderVendors
ADD VendorID varchar(50) NOT NULL


/**rev4513 **/
ALTER TABLE Users
ADD Avatar varchar(200) null


/******rev 4524 4/25 landing page
1. 0001864: APM - link to landing page editor for operator
2. Update layout related to profile image
3. Fix bug Paging control

Please also run DB script below to update database. It is to drop and re-create table LandingPages
===================================================================================
DROP TABLE LandingPages
go
CREATE TABLE LandingPages(
	[LandingPageID] [uniqueidentifier] NOT NULL,
	[LenderConfigID] [uniqueidentifier] NOT NULL,
	[VendorID] [varchar](50) NULL,
	[RefID] [varchar](50) NOT NULL,
	[Title] [varchar](200) NOT NULL,
	[PageData] [varchar](max) NOT NULL,
	[Status] [tinyint] NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[LastModifiedDate] [datetime] NULL,
	[CreatedBy] [uniqueidentifier] NOT NULL,
	[LastModifiedBy] [uniqueidentifier] NULL,
 CONSTRAINT [PK_LandingPages] PRIMARY KEY CLUSTERED 
(
	[LandingPageID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO





/**rev4571 5/2/18 ***/
CREATE TABLE VendorAgentProfiles(
	[VendorAgentProfileID] [uniqueidentifier] NOT NULL,
	[UserID] [uniqueidentifier] NOT NULL,
	[FirstName] [varchar](50) NOT NULL,
	[LastName] [varchar](50) NOT NULL,
	[Phone] [varchar](50) NULL,
	[Avatar] [varchar](200) NULL,
	[VendorID] [varchar](50) NOT NULL,
 CONSTRAINT [PK_VendorAgentProfiles] PRIMARY KEY CLUSTERED 
(
	[VendorAgentProfileID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

/*******Replace username with userID for most table because userNamce can be change 6/19/2018
/Rev4729 & rev4730

/**not current use anywhere in code**/
ALTER TABLE BackOfficeLenders 
add LastModifiedByUserID uniqueidentifier NULL
go
update B set B.LastModifiedByUserID = UserID
FROM BackOfficeLenders B left join Users U on B.LastModifiedBy = U.Email
where B.LastModifiedBy is not null
go


ALTER TABLE LenderConfigLogs 
add LastModifiedByUserID uniqueidentifier NULL
go
update B set B.LastModifiedByUserID = UserID
FROM LenderConfigLogs B left join Users U on B.UserName = U.Email
where B.UserName is not null
go

/****** how is this used? ****/
ALTER TABLE LenderConfigPreviews 
add LastModifiedByUserID uniqueidentifier NULL
go
update B set B.LastModifiedByUserID = UserID
FROM LenderConfigPreviews B left join Users U on (B.LastModifiedBy = U.Email or B.LastModifiedBy = U.FirstName + ' ' + U.LastName)
where B.LastModifiedBy is not null
go

/****** how is this used? ****/
ALTER TABLE LenderConfigPreviews 
add LastOpenedByUserID uniqueidentifier NULL
go
update B set B.LastOpenedByUserID = UserID
FROM LenderConfigPreviews B left join Users U on (B.LastOpenedBy = U.Email or B.LastOpenedBy = U.FirstName + ' ' + U.LastName)
where B.LastOpenedBy is not null
go

/****** LastOpenedByUserID* used by legacy config to provide warning of concurrent editing ****/
ALTER TABLE LenderConfigs 
add LastOpenedByUserID uniqueidentifier NULL
go
update B set B.LastOpenedByUserID = UserID
FROM LenderConfigs B left join Users U on (B.LastOpenedBy = U.Email or B.LastOpenedBy = U.FirstName + ' ' + U.LastName)
where B.LastOpenedBy is not null
go

/****** LastModifiedByUserID* used by legacy config to provide warning of concurrent editing ****/
ALTER TABLE LenderConfigs 
add LastModifiedByUserID uniqueidentifier NULL
go
update B set B.LastModifiedByUserID = COALESCE(UserID, '00000000-0000-0000-0000-000000000000')
FROM LenderConfigs B left join Users U on (B.LastModifiedBy = U.Email or B.LastModifiedBy = U.FirstName + ' ' + U.LastName)
where B.LastModifiedBy is not null
go



ALTER TABLE BackOfficeLenders
DROP COLUMN LastModifiedBy
go

ALTER TABLE LenderConfigLogs
DROP COLUMN UserName
go

ALTER TABLE LenderConfigPreviews
DROP COLUMN LastModifiedBy
go
ALTER TABLE LenderConfigPreviews
DROP COLUMN LastOpenedBy
go
ALTER TABLE LenderConfigs
DROP COLUMN LastOpenedBy
go
ALTER TABLE LenderConfigs
DROP COLUMN LastModifiedBy
go

/***REV4748 https://dnv.mantishub.io/view.php?id=1816  ***/
ALTER TABLE Loans
ADD RequestParamJson varchar(max) null
go
ALTER TABLE Loans
ADD LenderRef varchar(50) null
go


/***REV4888 https://denovu.atlassian.net/browse/APP-26  ***/
ALTER TABLE Users
ADD SSOLoanOfficerCode VARCHAR(50)
GO

CREATE TABLE [dbo].[SSOSessions](
	[SSOSessionID] [uniqueidentifier] NOT NULL,
	[OrganizationCode] [varchar](50) NOT NULL,
	[LenderCode] [varchar](50) NULL,
	[LoanOfficerCode] [varchar](50) NOT NULL,
	[Login] [varchar](50) NULL,
	[FirstName] [varchar](50) NULL,
	[MiddleName] [varchar](50) NULL,
	[LastName] [varchar](50) NULL,
	[Email] [varchar](100) NULL,
	[CreatedDate] [datetime] NOT NULL,
	[AccessRights] [varchar](max) NULL,
	[WorkingUrl] [varchar](max) NOT NULL,
 CONSTRAINT [PK_SSOSessions] PRIMARY KEY CLUSTERED 
(
	[SSOSessionID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

ALTER TABLE [dbo].[SSOSessions] ADD  CONSTRAINT [DF_SSOSessions_CreatedDate]  DEFAULT (getdate()) FOR [CreatedDate]
GO

/**Table use for LPQ to APM SSO and SSO via Homebanking **/

CREATE TABLE [dbo].[SSOTokens](
	[SSOTokenID] [uniqueidentifier] NOT NULL,
	[SSOToken] [varchar](50) NOT NULL,
	[SSOTokenCreateDate] [datetime] NOT NULL,
	[IsSSOTokenUsed] [char](1) NULL,
	[LenderRef] [varchar](50) NOT NULL,
	[LNAME] [varchar](50) NULL,
	[SSN_enc] [varchar](50) NULL,
	[Params] [varchar](4000) NULL,
	[RemoteIP] [varchar](50) NULL,
	[LoanType] [nchar](10) NULL,
 CONSTRAINT [pk_SSOTokenID] PRIMARY KEY CLUSTERED 
(
	[SSOTokenID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO
/**APP-26 LoanOfficerCode is unique per LPQ domain but it is not unique in AP since AP has users from all LPQ domains.**/
ALTER TABLE Users
DROP COLUMN SSOLoanOfficerCode
GO
ALTER TABLE Users
ADD SSOWorkspaceLoanOfficerCode varchar(200) NULL
GO

/**optimize IPtrackers operation ***/
CREATE NONCLUSTERED INDEX [nidx_IPTrackter_LogTime] ON [dbo].[IPTracker]
(
	[RemoteIP] ASC,
	[LogTime] ASC
)
GO


Rev5295 LPQ Downloaded Configuration Cache Persistence - APP-56
CREATE TABLE [dbo].[CachedConfigs](
	[CachedKey] [varchar](200) NOT NULL,
	[Config] [varchar](max) NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
 CONSTRAINT [PK_CachedConfigs] PRIMARY KEY CLUSTERED 
(
	[CachedKey] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

ALTER TABLE [dbo].[CachedConfigs] ADD  CONSTRAINT [DF_CachedConfigs_CreatedDate]  DEFAULT (getdate()) FOR [CreatedDate]
GO

Rev5453 - API credential encryption - APP-68
alter table LenderConfigs
alter column LPQPW varchar(500) not null
go
alter table LenderConfigLogs
alter column LPQPW varchar(500) not null
go
alter table LenderConfigPreviews
alter column LPQPW varchar(500) not null
go


Rev5577 - Add org level for vendors - APP-31

alter table LenderVendors
add Tags varchar(500) null

alter table UserRoles
add AssignedVendorGroupByTags varchar(500) null

Rev5676 - APP-100 LPQ-InstaTouch
CREATE TABLE [dbo].[EquifaxInstaTouchRecords](
	[ID] [uniqueidentifier] NOT NULL,
	[RecordType] [varchar](50) NOT NULL,
	[ConsumerFirstName] [varchar](100) NULL,
	[ConsumerLastName] [varchar](100) NULL,
	[DateRequested] [datetime] NOT NULL,
	[DateResponded] [datetime] NOT NULL,
	[TransactionID] [uniqueidentifier] NOT NULL,
	[SessionID] [varchar](300) NOT NULL,
	[LenderName] [varchar](200) NULL,
	[LenderID] [uniqueidentifier] NOT NULL,
	[LenderRef] [varchar](50) NOT NULL,
 CONSTRAINT [PK_EquifaxInstaTouchRecords] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO


Rev5798 APP-107  APM - SSO into MLU - Email validation

alter table Users
add SSOEmail varchar(100) null
go
alter table Users
add IsSSOEmailConfirmed varchar(1) null
go


SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[SSOEmailValidationTokens](
	[Token] [varchar](50) NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[Status] [int] NULL,
	[Email] [varchar](200) NULL,
	[ActivatedDate] [datetime] NULL,
	[UserID] [uniqueidentifier] NOT NULL,
 CONSTRAINT [PK_SSOEmailValidationTokens] PRIMARY KEY CLUSTERED 
(
	[Token] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

ALTER TABLE [dbo].[SSOEmailValidationTokens] ADD  CONSTRAINT [DF_SSOEmailValidationTokens_CreatedDate]  DEFAULT (getdate()) FOR [CreatedDate]
GO

Rev5917 APP-118 APM - Include "Equifax InstaTouch" report pulling
alter table EquifaxInstaTouchRecords
add LoanType varchar(2) null
go


Rev6071 APP-119 IP Restriction

alter table [dbo].[EquifaxInstaTouchRecords]
add IPAddress varchar(20) null


USE [LPQMobile]
GO

/****** Object:  Table [dbo].[EquifaxInstaTouchRequestTracking]    Script Date: 6/28/2019 3:13:08 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[EquifaxInstaTouchRequestTracking](
	[ID] [uniqueidentifier] NOT NULL,
	[RemoteIP] [varchar](20) NOT NULL,
	[LogTime] [datetime] NOT NULL,
	[LenderID] [uniqueidentifier] NOT NULL,
	[LenderRef] [varchar](50) NULL,
 CONSTRAINT [PK_EquifaxInstaTouchRequestTracking] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

USE [LPQMobile]
GO

/****** Object:  Index [IX_EquifaxInstaTouchRequestTracking]    Script Date: 6/28/2019 3:13:58 PM ******/
CREATE NONCLUSTERED INDEX [IX_EquifaxInstaTouchRequestTracking] ON [dbo].[EquifaxInstaTouchRequestTracking]
(
	[RemoteIP] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO

USE [LPQMobile]
GO

/****** Object:  Index [IX_EquifaxInstaTouchRequestTracking_1]    Script Date: 6/28/2019 3:14:09 PM ******/
CREATE NONCLUSTERED INDEX [IX_EquifaxInstaTouchRequestTracking_1] ON [dbo].[EquifaxInstaTouchRequestTracking]
(
	[LenderID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO

USE [LPQMobile]
GO

/****** Object:  Index [IX_EquifaxInstaTouchRequestTracking_2]    Script Date: 6/28/2019 3:14:16 PM ******/
CREATE NONCLUSTERED INDEX [IX_EquifaxInstaTouchRequestTracking_2] ON [dbo].[EquifaxInstaTouchRequestTracking]
(
	[LogTime] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO

USE [LPQMobile]
GO

/****** Object:  Table [dbo].[GlobalConfigs]    Script Date: 6/28/2019 3:14:34 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[GlobalConfigs](
	[ID] [uniqueidentifier] NOT NULL,
	[LenderID] [uniqueidentifier] NULL,
	[LenderConfigID] [uniqueidentifier] NULL,
	[VendorID] [varchar](50) NULL,
	[RevisionID] [int] NOT NULL,
	[ConfigJson] [ntext] NULL,
	[Name] [varchar](200) NOT NULL,
	[LastModifiedDate] [datetime] NULL,
 CONSTRAINT [PK_GlobalConfigs] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

ALTER TABLE [dbo].[GlobalConfigs] ADD  CONSTRAINT [DF_GlobalConfigs_Revision]  DEFAULT ((0)) FOR [RevisionID]
GO

ALTER TABLE [dbo].[GlobalConfigs] ADD  CONSTRAINT [DF_GlobalConfigs_LastModifiedDate]  DEFAULT (getdate()) FOR [LastModifiedDate]
GO

USE [LPQMobile]
GO

/****** Object:  Index [nidx_IPTrackter_LogTime]    Script Date: 6/28/2019 3:15:29 PM ******/
CREATE NONCLUSTERED INDEX [nidx_IPTrackter_LogTime] ON [dbo].[IPTracker]
(
	[RemoteIP] ASC,
	[LogTime] DESC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO

USE [LPQMobile]
GO
IF EXISTS(SELECT * FROM sys.tables WHERE name = 'EquifaxInstaTouchRecords' AND type='U') DROP TABLE EquifaxInstaTouchRecords;

CREATE TABLE [dbo].[EquifaxInstaTouchRecords](
	[ID] [uniqueidentifier] NOT NULL,
	[RecordType] [varchar](50) NOT NULL,
	[ConsumerFirstName] [varchar](100) NULL,
	[ConsumerLastName] [varchar](100) NULL,
	[DateRequested] [datetimeoffset] NOT NULL,
	[DateResponded] [datetimeoffset] NOT NULL,
	[TransactionID] [uniqueidentifier] NOT NULL,
	[SessionID] [varchar](300) NOT NULL,
	[MerchantId] [varchar](200) NULL,
	[LenderID] [uniqueidentifier] NOT NULL,
	[LenderRef] [varchar](50) NOT NULL,
	[LoanType] [varchar](2) NOT NULL,
	[IPAddress] [varchar](20) NOT NULL,
 CONSTRAINT [PK_EquifaxInstaTouchRecords] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

/******REV 6313 AP-1464 App Portal Reporting Dashboard (for Accounting and more) - UI**********************************/

ALTER TABLE [dbo].[BackOfficeLenders]
ADD CustomerInternalID VARCHAR(50) NULL

/****** AP-2474 APM Audits - Audit backend - Normal operation. Add a column that stores the plaintext diff of each log change. ******/

ALTER TABLE [dbo].[LenderConfigLogs]
ADD CachedDiff VARCHAR(MAX) NULL