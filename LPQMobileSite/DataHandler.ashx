﻿    <%@ WebHandler Language="VB" Class="DataHandler" %>

Imports System
Imports System.Web
Imports LPQMobile.Utils
Imports System.Xml
Imports System.Xml.Serialization
Imports System.IO

Public Class DataHandler : Implements IHttpHandler
	Protected _log As log4net.ILog = log4net.LogManager.GetLogger(Me.GetType)

	Public Sub ProcessRequest(ByVal context As HttpContext) Implements IHttpHandler.ProcessRequest
		'context.Response.ContentType = "text/plain"
		'context.Response.Write("Hello World")
		Dim cmd As String = Common.SafeString(context.Request.Params("command"))
		_log.Debug("Recieved command: " & Common.SafeString(cmd))
		Select cmd
			Case "getLPConfig"
				GetLandingPageConfig(context)
			Case "getConfig"
				getConfig(context)
		End Select

	End Sub

	Private Sub getConfig(context As HttpContext)
		Dim config As CWebsiteConfig
		Dim data As String = ""
		Dim lenderRef As String = Common.SafeString(context.Request.Params("lenderref"))
		Dim previewCode As String = Common.SafeString(context.Request.Params("previewCode"))
		Try
			_log.Debug(context.Request.UserHostAddress)
			If Common.IsInternalIP(context.Request.UserHostAddress) Then
				config = Common.GetCurrentWebsiteConfig(context.Request.Url.Host, Nothing, lenderRef, previewCode)
				Dim websiteNode = config._webConfigXML
				If config IsNot Nothing AndAlso websiteNode IsNot Nothing Then
					' Apply transformations
					' Transform Logo URL locally, depending on config for the logo source.
					Dim logoUrl = websiteNode.GetAttribute("logo_url")
					If logoUrl IsNot Nothing Then
						websiteNode.SetAttribute("logo_url", Common.CompileLogoUrl(logoUrl))
					End If
					' Transform Fav Icon URL locally, depending on config for the fac icon source.
					Dim favIconUrl = websiteNode.GetAttribute("fav_ico")
					If favIconUrl IsNot Nothing Then
						websiteNode.SetAttribute("fav_ico", Common.CompileAvatarUrl(favIconUrl))
					End If
					data = websiteNode.OuterXml
				End If
			Else
				_log.Warn("Unauthorized access from: " & context.Request.UserHostAddress)
			End If
		Catch ex As Exception
			_log.Debug("This host (" & context.Request.Url.Host & ": " & lenderRef & ") is not yet configured. - can't find configuration")
		End Try

		context.Response.Clear()
		context.Response.Write(data)
		context.Response.End()
	End Sub

	Private Sub GetLandingPageConfig(context As HttpContext)
		'Dim res As New CJsonResponse()
		Dim res As New LandingPageResp()

		Dim smBL As New SmBL()
		Dim id As String = Common.SafeString(context.Request.Params("id"))
		Dim result = smBL.GetLandingPageByRefID(id)

		res._PAGE_DATA = result.PageData
		res.HOME_PAGE_ID = result.RefID

		Dim sw As StringWriter = New StringWriter()
		Dim s As New XmlSerializer(GetType(LandingPageResp))
		s.Serialize(sw, res)
		context.Response.Clear()
		context.Response.Write(sw.ToString())
		context.Response.End()

	End Sub

	Public ReadOnly Property IsReusable() As Boolean Implements IHttpHandler.IsReusable
		Get
			Return False
		End Get
	End Property

	<Serializable(), XmlRoot("RESPONSE")>
	Public Class LandingPageResp
		<XmlElement("HOME_PAGE_ID")>
		Public HOME_PAGE_ID As String

		<XmlIgnore()>
		Public _PAGE_DATA As String
		<XmlElement("PAGE_DATA")>
		Public Property PAGE_DATA As XmlCDataSection
			Get
				Dim document As New XmlDocument()
				Return document.CreateCDataSection(_PAGE_DATA)
			End Get
			Set(ByVal Value As XmlCDataSection)
				_PAGE_DATA = Value.Value
			End Set
		End Property

	End Class

End Class