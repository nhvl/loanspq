﻿
Partial Class Sm_Logout
    Inherits System.Web.UI.Page

	Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
		FormsAuthentication.SignOut()
		Session.Clear()
		Session.Abandon()
		Response.Redirect("/login.aspx")
	End Sub
End Class
