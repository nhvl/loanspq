﻿Imports LPQMobile.Utils

''' <summary>
''' This is duplicate of sm/MluSso.aspx but for use when user is confirming email.
''' </summary>
Partial Class PerformMluSso
	Inherits SmBasePage
    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
		If Not IsPostBack Then
			Dim tokenStr = Common.SafeString(Request.QueryString("t"))
			If String.IsNullOrEmpty(tokenStr) Then
				Response.Redirect("/NoAccess.aspx")
			Else
				Dim smAuthBL As New SmAuthBL()
				Dim tokenObj = smAuthBL.GetSSOEmailValidationTokenInfo(tokenStr)
				Dim expireExceed As Integer = Common.SafeInteger(ConfigurationManager.AppSettings("UserActivationTokenExpireAfter"))
				If tokenObj Is Nothing OrElse tokenObj.Status <> SmSettings.TokenStatus.Waiting OrElse tokenObj.CreatedDate.AddDays(expireExceed) < Now Then
					Response.Redirect("/NoAccess.aspx", True)
				ElseIf smAuthBL.ProcessSSOEmailValidationToken(tokenObj, Request.ServerVariables("REMOTE_ADDR")) = "ok" Then
					Dim SSOLoginDocRequest = SmMluHandler.PrepareDocRequest("sso", UserInfo)
					Dim redirectUrl As String = ""
					If SmMluHandler.PostRequest(SSOLoginDocRequest, redirectUrl) Then
                        Response.Redirect("https://" & redirectUrl, True)
                    Else
						'Login failed already, lets try creating user & sso again
						Dim createUserAndSSODocRequest = SmMluHandler.PrepareDocRequest("create", UserInfo)
						SmMluHandler.PostRequest(createUserAndSSODocRequest, "")

						SSOLoginDocRequest = SmMluHandler.PrepareDocRequest("sso", UserInfo)
						redirectUrl = ""
						If SmMluHandler.PostRequest(SSOLoginDocRequest, redirectUrl) Then
                            Response.Redirect("https://" & redirectUrl, True)
                        Else
							Response.Redirect("/NoAccess.aspx", True)
						End If
					End If
				Else
					Response.Clear()
					Response.Write("<h1>Error!</h1>")
					Response.End()
				End If
				
			End If
		End If
	End Sub
End Class
