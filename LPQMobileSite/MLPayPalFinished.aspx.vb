﻿
Imports LPQMobile.Utils

Partial Class MLPayPalFinished
    Inherits CBasePage

    Protected _CheckMailAddress As String
    Protected _SystemMessage As String
	'Protected _bgTheme As String

	Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
		If Not IsPostBack Then
			log.Info("XA: MLPayPalFinished loading")
			ucPageHeader._currentConfig = _CurrentWebsiteConfig
			'override default so it cann be called from design page
			If _RedirectXAURL <> "" Then _RedirectURL = _RedirectXAURL
			ucPageHeader.TimeOutUrl = _RedirectURL
			Dim workflowSessionID As String = Common.SafeString(Request.QueryString("wfsid"))
			If Not String.IsNullOrEmpty(workflowSessionID) AndAlso Session("PAYPAL_PAYMENT_MESSAGE_" & workflowSessionID) IsNot Nothing Then
				_SystemMessage = CType(Session("PAYPAL_PAYMENT_MESSAGE_" & workflowSessionID), String)	'created in MLPaypal
			End If
			pageTitle.InnerText = If(String.IsNullOrEmpty(_SystemMessage), "Application Error", "Application Completed")
			divErrorMessage.Visible = String.IsNullOrEmpty(_SystemMessage)
			Session.Remove("RunServicesAfterPaypalReturn" & workflowSessionID)
			Session.Remove(workflowSessionID & "_State")
			Session.Remove(workflowSessionID & "_RequestParam")
			Session.Remove("StartedDate_" & workflowSessionID)
			log.Debug("clear session " & workflowSessionID)
			ucPageHeader._buttonFontColor = _ButtonFontTheme
		End If
	End Sub
End Class
