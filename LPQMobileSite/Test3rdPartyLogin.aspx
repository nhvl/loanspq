﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="Test3rdPartyLogin.aspx.vb" Inherits="Test3rdPartyLogin" %>
<%@ Register Src="~/Inc/ThirdPartyLogin.ascx" TagPrefix="uc" TagName="ThirdPartyLogin" %>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Test page</title>
    <style type="text/css">
         .float_left_half_screen {
             float: left; 
             width: 25%; 
             margin: 10px;
             border: 1px solid #ccc;
             border-radius: 5px;
             padding: 5px;
             font-family: Tahoma, Verdana, Segoe, sans-serif;
         }
         .float_left_half_screen input {
             width: 200px;
         }
    </style>
</head>
<body>
    <div>
        <uc:ThirdPartyLogin runat="server" ID="uc3rdPartyLogin" />
    </div>
    <div class="float_left_half_screen">
        <h3>
            LinkedIn
        </h3>
        <div id="div_linkedin_welcome" style="display: none">Welcome: <span id="sp_linkedin_welcome"></span> <a onclick="logoutLinkedIn()" style="cursor: pointer"><i>(Logout)</i></a></div>
        <br/>
        <div id="div_linkedin">
            <table>
                <tr>
                    <td>First name:</td>
                    <td><input type="text" id="sp_li_firstname" /></td>
                </tr>
                <tr>
                    <td>Last name:</td>
                    <td><input type="text" id="sp_li_lastname" /></td>
                </tr>
                <tr>
                    <td>Full name:</td>
                    <td><input type="text" id="sp_li_fullname" /></td>
                </tr>
                <tr>
                    <td>Email:</td>
                    <td><input type="text" id="sp_li_email" /></td>
                </tr>
                <tr>
                    <td>Title:</td>
                    <td><input type="text" id="sp_li_title" /></td>
                </tr>
                <tr>
                    <td>Company:</td>
                    <td><input type="text" id="sp_li_company" /></td>
                </tr>
                <tr>
                    <td>Country:</td>
                    <td><input type="text" id="sp_li_country" /></td>
                </tr>
            </table>
        </div>
    </div>
    <div class="float_left_half_screen">
        <h3>
            Facebook
        </h3>
        <div id="div_fb_welcome" style="display: none">Welcome: <span id="sp_fb_welcome"></span> <a onclick="logoutFacebook()" style="cursor: pointer"><i>(Logout)</i></a></div>
        <br/>
        <div id="div_facebook">
            <table>
                <tr>
                    <td>First name:</td>
                    <td><input type="text" id="sp_fb_firstname" /></td>
                </tr>
                <tr>
                    <td>Last name:</td>
                    <td><input type="text" id="sp_fb_lastname" /></td>
                </tr>
                <tr>
                    <td>Full name:</td>
                    <td><input type="text" id="sp_fb_fullname" /></td>
                </tr>
                <tr>
                    <td>Gender:</td>
                    <td><input type="text" id="sp_fb_gender" /></td>
                </tr>
                <tr>
                    <td>Email:</td>
                    <td><input type="text" id="sp_fb_email" /></td>
                </tr>
            </table>
        </div>
    </div>
    <script type="text/javascript">
        function BindLoggedInFbData(response) {
            document.getElementById('sp_fb_firstname').value = response.first_name;
            document.getElementById('sp_fb_lastname').value = response.last_name;
            document.getElementById('sp_fb_fullname').value = response.name;
            document.getElementById('sp_fb_gender').value = response.gender;
            document.getElementById('sp_fb_email').value = response.email;
            
            document.getElementById('sp_fb_welcome').innerHTML = response.name;
            document.getElementById('div_fb_welcome').style.display = 'block';
            document.getElementById('btn_fb_login').style.display = 'none';
        }
        
        function ClearLoggedInData() {
            document.getElementById('sp_fb_firstname').value = "";
            document.getElementById('sp_fb_lastname').value = "";
            document.getElementById('sp_fb_fullname').value = "";
            document.getElementById('sp_fb_gender').value = "";
            document.getElementById('sp_fb_email').value = "";
            
            document.getElementById('sp_fb_welcome').innerHTML = "";
            document.getElementById('div_fb_welcome').style.display = 'none';
            document.getElementById('btn_fb_login').style.display = 'block';
        }
        
        function BindLoggedInLinkedInData(data) {
            document.getElementById('sp_li_firstname').value = data.firstName;
            document.getElementById('sp_li_lastname').value = data.lastName;
            document.getElementById('sp_li_fullname').value = data.formattedName;
            document.getElementById('sp_li_email').value = data.emailAddress;
            document.getElementById('sp_li_country').value = data.location.name;
            
            if (data.positions.values.length > 0) {
                document.getElementById('sp_li_title').value = data.positions.values[0].title;
                document.getElementById('sp_li_company').value = data.positions.values[0].company.name;
            }

            document.getElementById('sp_linkedin_welcome').innerHTML = data.formattedName;
            document.getElementById('div_linkedin_welcome').style.display = 'block';
        }
        
        function ClearLinkedInData() {
            document.getElementById('sp_li_firstname').value = "";
            document.getElementById('sp_li_lastname').value = "";
            document.getElementById('sp_li_fullname').value = "";
            document.getElementById('sp_li_email').value = "";
            document.getElementById('sp_li_country').value = "";
            document.getElementById('sp_li_title').value = "";
            document.getElementById('sp_li_company').value = "";

            document.getElementById('sp_linkedin_welcome').innerHTML = "";
            document.getElementById('div_linkedin_welcome').style.display = 'none';
        }
    </script>
</body>
</html>
