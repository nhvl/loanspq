﻿
Partial Class IPAccessDenied
	Inherits CBasePage

	Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

		ucPageHeader._currentConfig = _CurrentWebsiteConfig
		'asign config theme for PageHeader UC, avoid reading config file unnescessary
		ucPageHeader._headerDataTheme = _HeaderDataTheme
		ucPageHeader._footerDataTheme = _FooterDataTheme
		ucPageHeader._contentDataTheme = _ContentDataTheme
		ucPageHeader._headerDataTheme2 = _HeaderDataTheme2
		ucPageHeader._backgroundDataTheme = _BackgroundTheme
		ucPageHeader._buttonDataTheme = _ButtonTheme
		ucPageHeader._buttonFontColor = _ButtonFontTheme
	End Sub
End Class