﻿
Imports LPQMobile.Utils

Partial Class Sm_ActivateAccount
	Inherits System.Web.UI.Page
	Protected tokenStr As String = ""
	Protected _log As log4net.ILog = log4net.LogManager.GetLogger(Me.GetType)
	Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
		If Not IsPostBack Then
			tokenStr = Common.SafeString(Request.QueryString("t"))
			If String.IsNullOrEmpty(tokenStr) Then
				Response.Redirect("/Login.aspx")
			Else
				Dim smAuthBL As New SmAuthBL()
				Dim tokenObj As SmUserActivateToken = smAuthBL.GetActivationTokenInfo(tokenStr)
				Dim expireExceed As Integer = Common.SafeInteger(ConfigurationManager.AppSettings("UserActivationTokenExpireAfter"))
				If tokenObj Is Nothing OrElse tokenObj.Status <> SmSettings.TokenStatus.Waiting OrElse tokenObj.CreatedDate.AddDays(expireExceed) < Now Then
					Response.Redirect("/Login.aspx")
				End If
			End If
		End If
	End Sub
	
End Class
