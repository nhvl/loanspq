﻿
Imports System.IO

Partial Class agent_Profile
	Inherits SmAgentBasePage

	Protected profileData As SmUser
	Public Property AvatarFileInfo As FileInfo

	Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load

		If Not IsPostBack Then
			Dim agentMasterPage = CType(Me.Master, agent_MasterPage)
			agentMasterPage.CurrentVendor = CurrentVendor
			agentMasterPage.CurrentPortal = CurrentPortal
			agentMasterPage.CurrentUserRole = CurrentUserRole
			Dim smAuthBL As New SmAuthBL
			profileData = smAuthBL.GetUserByID(UserInfo.UserID)
			If profileData Is Nothing Then
				Response.Redirect("/PageNotFound.aspx", True)
			End If
			If Not String.IsNullOrEmpty(profileData.Avatar) Then
				If profileData.Avatar.StartsWith("/images/avatars/") AndAlso File.Exists(Server.MapPath(profileData.Avatar)) Then
					AvatarFileInfo = New FileInfo(Server.MapPath(profileData.Avatar))
				End If
			End If
		End If
	End Sub
End Class
