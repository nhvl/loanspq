﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="Dashboard.aspx.vb" Inherits="agent_Dashboard"  MasterPageFile="MasterPage.master" %>

<asp:Content runat="server" ContentPlaceHolderID="plBody">
	<div class="container-fluid da-block-wrapper">
		<div class="row">
			<div class="col-sm-12 col-md-4 col-md-push-8 col-lg-3 col-lg-push-9 da-menu">
				<div class="da-block queues">
					<div class="block-header">
						<span>Your Queues</span>
						<ul>
							<li><a><i class="fa fa-cog"></i></a></li>
						</ul>
					</div>
					<div class="block-content">
						<ul class="queues-lst">
							<li>
								<div class="queue-num">
									<span>123</span>
								</div>
								<div class="queue-text">
									<div>Submitted Apps</div>
									<a>View Queue</a>
								</div>
							</li>
							<li>
								<div class="queue-num">
									<span>456</span>
								</div>
								<div class="queue-text">
									<div>Unsubmitted Apps</div>
									<a>View Queue</a>
								</div>
							</li>
						</ul>
					</div>
				</div>
				<div class="da-block insights">
					<div class="block-header">
						<span>Insights</span>
						<ul>
							<li><a><i class="fa fa-cog"></i></a></li>
						</ul>
					</div>
					<div class="block-content">
						<ul class="insight-lst">
							<li>
								<div class="insight-num">
									<span>123</span>
								</div>
								<div class="insight-text">
									You've submitted 25 applications to Underwriting this week
								</div>
							</li>
						</ul>
					</div>
				</div>
			</div>	
			<div class="col-sm-12 col-md-8 col-md-pull-4 col-lg-9 col-lg-pull-3 da-main">
				<div class="da-block">
					<div class="block-header">
						<span>Submitted Apps</span>
						<ul class="block-m-menu">
							<li>
								<div class="dropdown inline-drop-menu">
									<a class="btn dropdown-toggle btn-dropdown" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true"><i class="fa fa-align-justify"></i></a>
									<ul class="dropdown-menu dropdown-menu-right">
										<li><a href="#" data-command="settings">Settings</a></li>
										<li><a href="#" data-command="assign">Assign</a></li>
										<li><a href="#" data-command="move">Move</a></li>
										<li><a href="#" data-command="remove">Remove</a></li>
									</ul>
								</div>
							</li>
						</ul>
						<ul class="inline-btn-group">
							<li><a><i class="fa fa-user" data-command="assign"></i> ASSIGN</a></li>
							<li><a><i class="fa fa-exchange" data-command="move"></i> MOVE</a></li>
							<li><a><i class="fa fa-trash-o" data-command="remove"></i> REMOVE</a></li>
							<li><a><i class="fa fa-cog"></i></a></li>
						</ul>
					</div>
					<div class="block-content" id="divLoanList">
						<div class="spinner">
							<div class="rect1"></div>
							<div class="rect2"></div>
							<div class="rect3"></div>
							<div class="rect4"></div>
							<div class="rect5"></div>
						</div>
						<p>Loading...</p>
					</div>
				</div>
			</div>
			
		</div>
	</div>
	
</asp:Content>
<asp:Content runat="server" ContentPlaceHolderID="plScripts">
		<script type="text/javascript">
			_COMMON_PAGE_SIZE = 10;
			$(function () {
				var loanGrid;
				loanGrid = new _COMMON.Grid("/agent/agenthandler.aspx", "loanGrid", "divLoanList", manageLoans.FACTORY, manageLoans.FACTORY.onLoaded, { command: "loadLoanGrid", id:'<%=CurrentUserRole.UserRoleID%>' });
				loanGrid.init();
				manageLoans.FACTORY.init(loanGrid.GridObject);
			});
			(function(manageLoans, $, undefined) {
				manageLoans.DATA = {};
				manageLoans.FACTORY = {};
				manageLoans.FACTORY.init = function(gridInstance) {
					var self = this;
					manageLoans.DATA.grid = gridInstance;
				};
				manageLoans.FACTORY.getFilterInfo = function() {
					//implement this later
					return {};
				};
				//manageLoans.FACTORY.onLoaded = function () {
				//	$(".sm-tooltip-trigger>input[type='checkbox']", "#divLoanList").on("change", function (evt) {
				//		$(".sm-tooltip-trigger>input[type='checkbox']").not(this).prop("checked", false);
				//	});
				//};

			}(window.manageLoans = window.manageLoans || {}, jQuery));
			
	</script>
</asp:Content>