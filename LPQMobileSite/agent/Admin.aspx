﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="Admin.aspx.vb" Inherits="agent_Admin"  MasterPageFile="MasterPage.master" %>

<asp:Content runat="server" ContentPlaceHolderID="plBody">
	<div class="container-fluid da-block-wrapper">
		<div class="row">
			<div class="col-sm-12 col-md-12 col-lg-3 col-lg-push-9 da-menu">
				<%--<div class="da-block queues">
					<div class="block-header">
						<span>Your Queues</span>
						<ul>
							<li><a><i class="fa fa-cog"></i></a></li>
						</ul>
					</div>
					<div class="block-content">
						<ul class="queues-lst">
							<li>
								<div class="queue-num">
									<span>123</span>
								</div>
								<div class="queue-text">
									<div>Submitted Apps</div>
									<a>View Queue</a>
								</div>
							</li>
							<li>
								<div class="queue-num">
									<span>456</span>
								</div>
								<div class="queue-text">
									<div>Unsubmitted Apps</div>
									<a>View Queue</a>
								</div>
							</li>
						</ul>
					</div>
				</div>
				<div class="da-block insights">
					<div class="block-header">
						<span>Insights</span>
						<ul>
							<li><a><i class="fa fa-cog"></i></a></li>
						</ul>
					</div>
					<div class="block-content">
						<ul class="insight-lst">
							<li>
								<div class="insight-num">
									<span>123</span>
								</div>
								<div class="insight-text">
									You've submitted 25 applications to Underwriting this week
								</div>
							</li>
						</ul>
					</div>
				</div>--%>
			</div>	
			<div class="col-sm-12 col-md-12 col-lg-9 col-lg-pull-3 da-main">
				<div class="da-block">
					<div class="block-header">
						<span>Agents</span>
						<ul class="inline-btn-group no-context-menu">
							<li><a id="btnAddAgent"><i class="fa fa-plus-circle"></i> ADD</a></li>
						</ul>
					</div>
					<div class="block-content" id="divVendorAgentList">
						<div class="spinner">
							<div class="rect1"></div>
							<div class="rect2"></div>
							<div class="rect3"></div>
							<div class="rect4"></div>
							<div class="rect5"></div>
						</div>
						<p>Loading...</p>
					</div>
				</div>
			</div>
			
		</div>
	</div>
	
</asp:Content>
<asp:Content runat="server" ContentPlaceHolderID="plScripts">
		<script type="text/javascript">
			_COMMON_PAGE_SIZE = 5;
			$(function () {
				var agentGrid;
				agentGrid = new _COMMON.Grid("/agent/agenthandler.aspx", "agentGrid", "divVendorAgentList", manageAgents.FACTORY, manageAgents.FACTORY.onLoaded, { command: "loadAgentGrid", id: '<%=CurrentUserRole.UserRoleID%>' });
				agentGrid.init();
				manageAgents.FACTORY.init(agentGrid.GridObject);
				$("#btnAddAgent").click(manageAgents.FACTORY.loadAddAgentForm);
			});
			(function (manageAgents, $, undefined) {
				manageAgents.DATA = {};
				manageAgents.FACTORY = {};
				manageAgents.FACTORY.init = function (gridInstance) {
					var self = this;
					manageAgents.DATA.grid = gridInstance;
				};
				manageAgents.FACTORY.getFilterInfo = function () {
					//implement this later
					return {};
				};
				manageAgents.FACTORY.onLoaded = function (container) {
					$("input:checkbox[data-command='enable-btn']", $("#divVendorAgentList")).each(function (idx, ele) {
						var $self = $(ele);
						$self.on("change", function () {
							manageAgents.FACTORY.ShowChangeStateDialog($self.closest("div.flex-row").data("id"), $self);
						});
					});
					$("a[data-command='delete']", $("#divVendorAgentList")).each(function (idx, ele) {
						var $self = $(ele);
						$self.on("click", function () {
							manageAgents.FACTORY.ShowRemoveDialog($self.closest("div.flex-row").data("id"), $self);
						});
					});
					$("a[data-command='unlock']", $("#divVendorAgentList")).each(function (idx, ele) {
						var $self = $(ele);
						$self.on("click", function () {
							manageAgents.FACTORY.UnlockAgent($self.closest("div.flex-row").data("id"), $self);
						});
					});
				};
				manageAgents.FACTORY.UnlockAgent = function (rId, sourceEle) {
					var dataObj = { id: '<%=CurrentUserRole.UserRoleID%>', rId: rId, lenderID: '<%=CurrentUserRole.LenderID%>', lenderConfigID: '<%=CurrentUserRole.LenderConfigID%>', command: "unlockAgent" }
					$.ajax({
						url: "/agent/agenthandler.aspx",
						async: true,
						cache: false,
						type: 'POST',
						dataType: 'html',
						data: dataObj,
						success: function (responseText) {
							var response = $.parseJSON(responseText);
							if (response.IsSuccess) {
								_COMMON.noty("success", "Success", 500);
								var pageIndex = manageAgents.DATA.grid.getPaginationSetting().page;
								manageAgents.DATA.grid.setFilter(manageAgents.FACTORY.getFilterInfo()).setPageIndex(pageIndex).load();
								container.dialog.close();
							} else {
								_COMMON.noty("error", "Error", 500);
								$(sourceEle).prop("checked", !isEnabled);
							}
						}
					});
				};
				manageAgents.FACTORY.ShowRemoveDialog = function (rid, srcEle) {
					_COMMON.showDialogSaveClose("Leave a comment", 0, "takenote_dialog_0", '<div style="margin:10px 20px;"><textarea data-id="txtComment" class="form-control required" rows="3" data-message-require="Comment cannot be empty" placeholder="Write a comment" maxlength="1000" display-counter-control="spanCommentCounter"></textarea><div class="text-right char-count"><span id="spanCommentCounter"></span></div><label for="txtComment" class="has-error" style="display:none"></label></div>', "", null, function (container) {
						var comment = $("textarea[data-id='txtComment']", $("#takenote_dialog_0")).val();
						manageAgents.FACTORY.DeleteAgent(rid, srcEle, comment, container);
					}, function (container) {
						$("textarea[data-id='txtComment']", $(container)).counter();
					}, function () {
						//reload grid when dialog closed
						var pageIndex = manageAgents.DATA.grid.getPaginationSetting().page;
						manageAgents.DATA.grid.setFilter(manageAgents.FACTORY.getFilterInfo()).setPageIndex(pageIndex).load();
					});
				};
				manageAgents.FACTORY.DeleteAgent = function (rId, sourceEle, comment, container) {
					var dataObj = { id: '<%=CurrentUserRole.UserRoleID%>', rId: rId, comment: comment, lenderID: '<%=CurrentUserRole.LenderID%>', lenderConfigID: '<%=CurrentUserRole.LenderConfigID%>', command: "deleteAgent" }
					$.ajax({
						url: "/agent/agenthandler.aspx",
						async: true,
						cache: false,
						type: 'POST',
						dataType: 'html',
						data: dataObj,
						success: function (responseText) {
							var response = $.parseJSON(responseText);
							if (response.IsSuccess) {
								_COMMON.noty("success", "Success", 500);
								var pageIndex = manageAgents.DATA.grid.getPaginationSetting().page;
								manageAgents.DATA.grid.setFilter(manageAgents.FACTORY.getFilterInfo()).setPageIndex(pageIndex).load();
								container.dialog.close();
							} else {
								_COMMON.noty("error", "Error", 500);
							}
						}
					});
				};
				manageAgents.FACTORY.ShowChangeStateDialog = function (rId, srcEle) {
					_COMMON.showDialogSaveClose("Leave a comment", 0, "takenote_dialog_0", '<div style="margin:10px 20px;"><textarea data-id="txtComment" class="form-control required" rows="3" data-message-require="Comment cannot be empty" placeholder="Write a comment" maxlength="1000" display-counter-control="spanCommentCounter"></textarea><div class="text-right char-count"><span id="spanCommentCounter"></span></div><label for="txtComment" class="has-error" style="display:none"></label></div>', "", null, function (container) {
						var comment = $("textarea[data-id='txtComment']", $("#takenote_dialog_0")).val();
						manageAgents.FACTORY.ChangeState(rId, $(srcEle).is(":checked"), srcEle, comment, container);
					}, function (container) {
						$("textarea[data-id='txtComment']", $(container)).counter();
					}, function () {
						//reload grid when dialog closed
						var pageIndex = manageAgents.DATA.grid.getPaginationSetting().page;
						manageAgents.DATA.grid.setFilter(manageAgents.FACTORY.getFilterInfo()).setPageIndex(pageIndex).load();
					});
				};
				manageAgents.FACTORY.ChangeState = function (rId, isEnabled, sourceEle, comment, container) {
					var dataObj = { id: '<%=CurrentUserRole.UserRoleID%>', rID: rId, comment: comment, lenderConfigID: '<%=CurrentPortal.LenderConfigID%>', lenderID: '<%=CurrentPortal.LenderID%>', enable: isEnabled ? "Y" : "N", command: "changeAgentState" }
					$.ajax({
						url: "/agent/agenthandler.aspx",
						async: true,
						cache: false,
						type: 'POST',
						dataType: 'html',
						data: dataObj,
						success: function (responseText) {
							var response = $.parseJSON(responseText);
							if (response.IsSuccess) {
								_COMMON.noty("success", "Success", 500);
								var pageIndex = manageAgents.DATA.grid.getPaginationSetting().page;
								manageAgents.DATA.grid.setFilter(manageAgents.FACTORY.getFilterInfo()).setPageIndex(pageIndex).load();
								container.dialog.close();
							} else {
								_COMMON.noty("error", "Error", 500);
								$(sourceEle).prop("checked", !isEnabled);
							}
						}
					});
				};
				manageAgents.FACTORY.loadAddAgentForm = function () {
					_COMMON.showDialogSaveClose("Add Agent", 0, "addagent_dialog", "", "/agent/agenthandler.aspx", { id: '<%=CurrentUserRole.UserRoleID%>', command: "loadAddAgentForm", lenderConfigID: '<%=CurrentPortal.LenderConfigID%>' }, manageAgents.FACTORY.addAgent, function (container) {
						//add validator
						registerDataValidator($("#addagent_dialog"), "addagent_dialog");
				}, function () {
					//reload grid when dialog closed
					var pageIndex = manageAgents.DATA.grid.getPaginationSetting().page;
					manageAgents.DATA.grid.setFilter(manageAgents.FACTORY.getFilterInfo()).setPageIndex(pageIndex).load();
				});
				};

				manageAgents.FACTORY.addAgent = function (obj) {
					if ($.smValidate("addagent_dialog") == false) return;
					var agentData = obj.container.find("form").serializeJSON({ parseAll: true, checkboxUncheckedValue: "false" });
					//update data
					var postData = {
						command: "saveNewAgent",
						email: agentData.Email,
						id: '<%=CurrentUserRole.UserRoleID%>',
						lenderID: '<%=CurrentPortal.LenderID%>',
						lenderConfigID: '<%=CurrentPortal.LenderConfigID%>'
					};
					$.ajax({
						url: "/agent/agenthandler.aspx",
						async: true,
						cache: false,
						type: 'POST',
						dataType: 'html',
						data: postData,
						success: function (responseText) {
							var response = $.parseJSON(responseText);
							if (response.IsSuccess) {
								_COMMON.noty("success", "Success", 500);
								obj.dialog.close();
							} else if (response.Info == "ALREADY") {
								_COMMON.showAlertDialog(response.Message);
							} else {
								var errorMsg = response.Message !== "" ? response.Message : "Error";
								_COMMON.noty("error", errorMsg, 500);
							}
						}
					});
				};
			}(window.manageAgents = window.manageAgents || {}, jQuery));
			
	</script>
</asp:Content>