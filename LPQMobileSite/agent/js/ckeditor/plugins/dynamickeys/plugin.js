﻿CKEDITOR.plugins.add("dynamickeys", {
	requires: ["richcombo"],
	//icons: "dynamickeys",
	init:function(editor) {
		editor.addCommand("insertDynamicKeys", new CKEDITOR.dialogCommand("dynamicKeysDialog"));
		//editor.ui.addButton("DynamicKeys", { label: "Dynamic Keys", command: "insertDynamicKeys", toolbar: "insert" });
		CKEDITOR.dialog.add("dynamicKeysDialog", this.path + "dialogs/dynamicKeys.js");
		var config = editor.config;
		editor.ui.addRichCombo("DynamicKeys", {
			label: "Insert Dynamic Key",
			title: "Dynamic Keys",
			multiSelect: false,
			panel: {
				css: [CKEDITOR.skin.getPath("editor")].concat(config.contentsCss)
			},
			init: function() {				
				this.startGroup("Applicant Information");
				this.add("{{FIRST_NAME}}", "+ First Name", "Applicant First Name");
				this.add("{{FULL_NAME}}", "+ Full Name", "Applicant Full Name");
				this.startGroup("Application Information");
				this.add("{{LOAN_NUMBER}}", "+ Loan Number", "LPQ Loan Number");
				this.add("{{MEMBER_NUMBER}}", "+ Member Number", "Core Member Number");
				this.add("{{MEMBER_LOAN_NUMBER}}", "+ Member or Loan Number", "Use Member Number if available or use Loan Number");
				this.add("{{ACCOUNT_NUMBER}}", "+ Account Number", "Account Number for product");
				this.startGroup("Others");
				this.add("{{CARD_NAME}}", "+ Card Name", "Credit Card Name");
				this.add("", "Browse more...", "Browse more");
				//this.setValue("", "Insert Dynamic Key");
			},
			onClick: function (value) {
				if (value == "") {
					editor.execCommand("insertDynamicKeys");
				} else {
					editor.insertText(value);
				}
			}
		});
	}
})