﻿function registerDataValidator(container, groupName) {
	var $container = $(container);
	$.smValidate.removeValidationGroup(groupName);
	$("[data-required='true'],[data-format]", $container).each(function (idx, ele) {
		$(ele).observer({
			validators: [
				function (partial) {
					var $self = $(this);
					if ($self.data("required") && $.trim($self.val()) === "") {
						if ($self.data("message-require")) {
							return $self.data("message-require");
						} else {
							return "This field is required";
						}
					}
					var format = $self.data("format");
					if (format) {
						var msg = "Invalid format";
						if ($self.data("message-invalid-data")) {
							msg = $self.data("message-invalid-data");
						}
						if (format === "email") {
							if (_COMMON.ValidateEmail($self.val()) === false) {
								return msg;
							}
						} else if (format === "phone") {
							if (_COMMON.ValidatePhone($self.val()) === false) {
								return msg;
							}
						} else if (format === "password") {
							var result = _COMMON.validatePassword($self.val());
							if (result != "") {
								return result;
							}
						} else if (format === "confirmPassword") {
							if ($self.val() !== $("#txtPassword").val()) {
								return $self.data("password-not-match");
							}
						} else if ($self.val() != "" && format === "positive-number") {
							if ((parseInt($self.val()) > 0) == false) {
								return msg;
							}
						} else if (format === "guid") {
							if (/^[0-9A-Za-z]{8}-[0-9A-Za-z]{4}-[0-9A-Za-z]{4}-[0-9A-Za-z]{4}-[0-9A-Za-z]{12}$/.test($self.val()) == false) {
								return msg;
							}
						}
					}
					return "";
				}
			],
			validateOnBlur: true,
			group: groupName
		});
	});
}
var currentAvatarFile = null;
function initEditUploadUserAvatar(container, valueField) {
	$(container).dropzone({
		url: "/agent/agenthandler.aspx",
		dictDefaultMessage: "<p>Drop image file here or click to select image from your computer</p><p>Compatible file types: JPG, PNG. Recommended approximate dimension: 200 x 200.</p><p>Recommended file size: 10KB.</p>",
		maxFilesize: 2,
		acceptedFiles: "image/png,image/jpeg",
		params: { "command": "uploadavatar" },
		maxFiles: 1,
		addRemoveLinks: true,
		dictRemoveFile: "Remove",
		maxfilesexceeded: function (file) {
			this.removeAllFiles();
			this.addFile(file);
		},
		thumbnailWidth: 200,
		thumbnailHeight: null,
		init: function () {
			var $hdLogo = $(valueField);
			currentAvatarFile = null;
			if ($hdLogo.val() != "") {
				var mockFile = { name: $hdLogo.data("file-name"), size: $hdLogo.data("file-size") /*, type: 'image/png'*/, url: $(valueField).val() };
				this.options.addedfile.call(this, mockFile);
				this.options.thumbnail.call(this, mockFile, $(valueField).val());
				mockFile.previewElement.classList.add('dz-success');
				mockFile.previewElement.classList.add('dz-complete');
				$(mockFile.previewElement).find("img").attr("width", 200);
				currentAvatarFile = mockFile;
			}
			this.on("addedfile", function (file) {
				if (currentAvatarFile) {
					this.removeFile(currentAvatarFile);
				}
				currentAvatarFile = file;
			});
			this.on("success", function (file, response) {
				$(valueField).val(response);
				$.smValidate.hideValidation($(valueField).closest(".form-group"));
				currentAvatarFile = file;
			});
			this.on("removedfile", function (file) {
				currentAvatarFile = null;
				if (file.xhr) {
					$.ajax({
						url: "/agent/agenthandler.aspx",
						async: true,
						cache: false,
						type: 'POST',
						dataType: 'html',
						data: { command: 'deleteavatar', logourl: file.xhr.responseText },
						success: function (responseText) {
							$(valueField).val("");
						}
					});
				} else {
					$(valueField).val("");
				}

			});
		}
	});
}