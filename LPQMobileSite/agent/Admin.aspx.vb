﻿
Partial Class agent_Admin
	Inherits SmAgentBasePage

	Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load

		If Not IsPostBack Then
			Dim agentMasterPage = CType(Me.Master, agent_MasterPage)
			agentMasterPage.CurrentVendor = CurrentVendor
			agentMasterPage.CurrentPortal = CurrentPortal
			agentMasterPage.CurrentUserRole = CurrentUserRole
		End If
	End Sub
End Class
