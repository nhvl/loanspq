﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="ChangePasswordForm.ascx.vb" Inherits="agent_uc_ChangePasswordForm" %>
<div style="padding: 20px; max-height: 500px; overflow: auto;">
<form id="frm_changePassword">
    <div class="row">
        <div class="col-xs-12">
            <div class="form-group">
				<label class="required-field">Current Password</label>
				<input type="password" class="form-control" name="CurrentPassword" id="txtCurrentPassword" data-message-require="Please fill out this field" data-required="true" placeholder="Password" maxlength="50" />
			</div>
			<div class="form-group">
				<label class="required-field">New Password</label>
				<input type="password" class="form-control" name="NewPassword" id="txtPassword" data-format="password" data-message-require="Please fill out this field" data-required="true" placeholder="Password" maxlength="50" />
			</div>
			<div class="form-group">
				<label class="required-field">Confirm New Password</label>
				<input type="password" class="form-control" name="ConfirmNewPassword" data-format="confirmPassword" id="txtConfirmPassword" data-password-not-match="Confirm password is not match." data-message-require="Please fill out this field" data-required="true" placeholder="Confirm Password" maxlength="50" />
			</div>
        </div>
    </div>
</form>
</div>