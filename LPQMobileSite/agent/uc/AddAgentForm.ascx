﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="AddAgentForm.ascx.vb" Inherits="agent_uc_AddAgentForm" %>
<div style="padding: 20px; max-height: 500px; overflow: auto;">
<form id="frm_addAgent">
	<%--<%If Usr Is Nothing Then%>--%>
    <div class="row">
        <div class="col-xs-12">
            <div class="form-group">
                <label class="required-field">Email</label>
                <input type="text" class="form-control" value="" name="Email" id="txtEmail" data-type="email" placeholder="Email" data-message-invalid-data="Email is invalid" data-format="email" data-message-require="Please fill out this field" data-required="true" maxlength="50" />
            </div>
        </div>
    </div>
</form>
</div>