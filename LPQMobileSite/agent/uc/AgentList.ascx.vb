﻿
Partial Class agent_uc_AgentList
	Inherits SmBaseUserControl

	Public Property PagingInfo As New SmPagingInfoModel
	Public Property FilterInfo As SmVendorUserRoleListFilter
	Protected AgentList As List(Of SmUserRole)

	Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
		If Not IsPostBack Then
			Try
				AgentList = smBL.GetVendorUserRoles(PagingInfo, FilterInfo)
				ucGridFooter.PagingInfo = PagingInfo
			Catch ex As Exception
				log.Error("Could not load agent list", ex)
			End Try
		End If
	End Sub

End Class
