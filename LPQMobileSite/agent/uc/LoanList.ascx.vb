﻿
Partial Class agent_uc_LoanList
	Inherits SmBaseUserControl

	Public Property PagingInfo As New SmPagingInfoModel
	Public Property FilterInfo As SmLoanListFilter
	Protected LoanList As List(Of SmLoan)

	Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
		If Not IsPostBack Then
			Try
				LoanList = New SmBL().GetLoanList(PagingInfo, FilterInfo)
				ucGridFooter.PagingInfo = PagingInfo
			Catch ex As Exception
				log.Error("Could not load loan list", ex)
			End Try
		End If
	End Sub

	Protected Function BuildIncompletedLoanUrl(item As SmLoan) As String
		Dim result As String
		Dim url = "{0}?lenderref={1}&vendorid={2}&userid={3}&sid={4}{5}"
		Select Case item.LoanType
			Case SmSettings.LenderVendorType.CC
				result = String.Format(url, "/cc/creditcard.aspx", item.LenderRef, item.VendorID, CurrentUserInfo.UserID, item.LoanSavingID, "")
			Case SmSettings.LenderVendorType.PL
				result = String.Format(url, "/pl/personalloan.aspx", item.LenderRef, item.VendorID, CurrentUserInfo.UserID, item.LoanSavingID, "")
			Case SmSettings.LenderVendorType.VL
				result = String.Format(url, "/vl/VehicleLoan.aspx", item.LenderRef, item.VendorID, CurrentUserInfo.UserID, item.LoanSavingID, "")
			Case SmSettings.LenderVendorType.CC_COMBO
				result = String.Format(url, "/cc/creditcard.aspx", item.LenderRef, item.VendorID, CurrentUserInfo.UserID, item.LoanSavingID, "&type=1")
			Case SmSettings.LenderVendorType.PL_COMBO
				result = String.Format(url, "/pl/personalloan.aspx", item.LenderRef, item.VendorID, CurrentUserInfo.UserID, item.LoanSavingID, "&type=1")
			Case SmSettings.LenderVendorType.VL_COMBO
				result = String.Format(url, "/vl/VehicleLoan.aspx", item.LenderRef, item.VendorID, CurrentUserInfo.UserID, item.LoanSavingID, "&type=1")
			Case Else
				result = "#"
		End Select
		Return result.ToLower()
	End Function

End Class
