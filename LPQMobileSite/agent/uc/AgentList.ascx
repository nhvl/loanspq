﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="AgentList.ascx.vb" Inherits="agent_uc_AgentList" %>
<%@ Import Namespace="LPQMobile.Utils" %>
<%@ Register Src="~/Sm/Uc/Paging.ascx" TagPrefix="uc" TagName="GridFooter" %>

<div class="flex-table agent-lst">
	<div class="flex-row-header">
		<div class="flex-column flex-column-num"></div>
		<div class="flex-column flex-column-avatar"></div>
		<div class="flex-column-wrapper flex-column-name-email-phone">
			<div class="flex-column flex-column-agent">Agent</div>
			<div class="flex-column flex-column-name">Name</div>
			<div class="flex-column-wrapper flex-column-email-phone">
				<div class="flex-column flex-column-email">Email</div>
				<div class="flex-column flex-column-phone">Phone</div>
			</div>
		</div>
		<div class="flex-column flex-column-status">Status</div>
		<div class="flex-column flex-column-action">ACTIONS</div>
	</div>
	<%If AgentList IsNot Nothing AndAlso AgentList.Any() Then
			Dim idx As Integer = (PagingInfo.PageIndex - 1) * PagingInfo.PageSize
			Dim loginFailedCountExceed = Common.SafeInteger(ConfigurationManager.AppSettings("LoginFailedCountExceed"))
			For Each item As SmUserRole In AgentList
				idx = idx + 1
				Dim userStatus = item.User.Status
				If String.IsNullOrEmpty(item.User.FullName.Trim()) Then
					userStatus = SmSettings.UserStatus.InActive
				ElseIf userStatus = SmSettings.UserStatus.Active AndAlso (item.User.LoginFailedCount >= loginFailedCountExceed OrElse (item.User.LastLoginDate.HasValue AndAlso item.User.LastLoginDate.Value.AddDays(item.User.ExpireDays) < DateTime.Now)) Then
					userStatus = SmSettings.UserStatus.Locked
				End If
	%>
	<div class="flex-row" data-id="<%=item.UserRoleID %>">
		<div class="flex-column flex-column-num"><%=idx %></div>
		<div class="flex-column flex-column-avatar">
			<div class="avatar-wrapper"><span class="avatar" style="background-image: url('<%=IIf(String.IsNullOrEmpty(item.User.Avatar), "/images/avatar.jpg", item.User.Avatar)%>')"></span></div>
									
		</div>
		<div class="flex-column-wrapper flex-column-name-email-phone">
			<div class="flex-column flex-column-name"><%=IIf(string.IsNullOrEmpty(item.User.FullName.Trim()),"-",item.User.FullName) %></div>
			<div class="flex-column-wrapper flex-column-email-phone">
				<div class="flex-column flex-column-email <%=IIf(userStatus = SmSettings.UserStatus.Locked, " locked", "") %>"><%=item.User.Email%></div>
				<%If String.IsNullOrEmpty(item.User.Phone) Then%>
				<div class="flex-column flex-column-phone">-</div>
				<%Else%>
				<div class="flex-column flex-column-phone"><%=Regex.Replace(item.User.Phone, "(\d{3})(\d{3})(\d{4})", "$1-$2-$3") %></div>
				<%End If %>
			</div>
		</div>
		<div class="flex-column flex-column-status">
			<label class="toggle-btn" data-on="ON" data-off="OFF">
				<input type="checkbox" data-command="enable-btn" <%=SmUtil.BindCheckbox(IIf(item.Status = SmSettings.UserRoleStatus.Active, True, False))%>/>
				<span class="button-checkbox"></span>
			</label>
		</div>
		<div class="flex-column flex-column-action">
			<div class="link-buttons">
				<%If userStatus = SmSettings.UserStatus.Locked Then%>
				<a href="#" data-command="unlock"><span>Unlock</span></a>
				<span> | </span>
				<%End If %>
				<a href="#" data-command="delete"><span>Remove</span></a>
			</div>
			<div class="dropdown inline-drop-menu">
				<button class="btn dropdown-toggle btn-dropdown" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
					<img src="/agent/content/images/fa_circle_blue.png"/>
					<img src="/agent/content/images/fa_circle_blue.png"/>
					<img src="/agent/content/images/fa_circle_blue.png"/>
				</button>
				<ul class="dropdown-menu dropdown-menu-right">
					<%If userStatus = SmSettings.UserStatus.Locked Then%>
					<li><a href="#" data-command="unlock">Unlock</a></li>
					<%End If%>
					<li><a href="#" data-command="delete">Remove</a></li>
				</ul>
			</div>
		</div>
	</div>
	<%
	Next
End If%>
	<div class="js-paging-block">
		<uc:GridFooter ID="ucGridFooter" runat="server"></uc:GridFooter>
	</div>
</div>

