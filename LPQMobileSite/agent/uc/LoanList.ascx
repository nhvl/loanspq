﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="LoanList.ascx.vb" Inherits="agent_uc_LoanList" %>
<%@ Register Src="~/Sm/Uc/Paging.ascx" TagPrefix="uc" TagName="GridFooter" %>

<div class="flex-table loan-lst">
	<div class="flex-row-header">
		<div class="flex-column flex-column-checkbox"><input type="checkbox"/></div>
		<div class="flex-column-wrapper flex-column-1">
			<div class="flex-column flex-column-applicants">Applicants</div>
			<div class="flex-column flex-column-loaninfo">Loan Info</div>
		</div>
		<div class="flex-column-wrapper flex-column-2">
			<div class="flex-column flex-column-status">Status</div>
			<div class="flex-column flex-column-date">Decision&nbsp;Date</div>
		</div>
		<div class="flex-column-wrapper flex-column-3">
			<div class="flex-column flex-column-stipulation">Stipulations</div>
			<div class="flex-column flex-column-im">IM</div>
			<div class="flex-column flex-column-action">ACTIONS</div>
		</div>
	</div>
	<%If LoanList IsNot Nothing AndAlso LoanList.Any() Then
			For Each item As SmLoan In LoanList
				Dim loanInfo = item.GetLoanInfo()
				If loanInfo Is Nothing OrElse loanInfo.Count = 0 Then Continue For
				Dim stipulationItems = item.GetStipulations()
	%>
	<div class="flex-row">
		<div class="flex-column flex-column-checkbox"><input type="checkbox"/></div>
		<div class="flex-column-wrapper flex-column-1">
			<div class="flex-column flex-column-applicants">
				<a href="#"><%=item.ApplicantName%></a>
				<p><%=item.ApplicantInfo%></p>
			</div>
			<div class="flex-column flex-column-loaninfo">
				<p><%=loanInfo("amount")%></p>
				<p><%=loanInfo("duration")%>   <%=loanInfo("rate")%></p>
			</div>
									
		</div>
		<div class="flex-column-wrapper flex-column-2">
			<div class="flex-column flex-column-status">
				<p><span class="loan-status <%=IIf(item.Status = "QUALIFIED", "approved", "")%>"><%=item.Status%></span></p>
				<p><span class="fund-status"><%=item.FundStatus%></span></p>
			</div>
			<div class="flex-column flex-column-date">
				<%--TODO: use nonempty date in this order: fundDate, Status date, submit date--%>
				<%If item.Status = "QUALIFIED" Then%>		
					<%If item.FundDate.HasValue Then%>
					<p><%=item.FundDate.Value.ToString("MM/dd/yy hh:mm tt")%></p>
					<%ElseIf item.StatusDate.HasValue Then%>
					<p><%=item.StatusDate.Value.ToString("MM/dd/yy hh:mm tt")%></p>
					<%ElseIf item.SubmitDate.HasValue Then%>
					<%End If%>
				<%Else%>				
					<%If item.StatusDate.HasValue Then%>
					<p><%=item.StatusDate.Value.ToString("MM/dd/yy hh:mm tt")%></p>
					<%ElseIf item.CreatedDate.HasValue Then%>
					<p><%=item.CreatedDate.Value.ToString("MM/dd/yy hh:mm tt")%></p>					
					<%End If%>
				<%End If%>
			</div>
									
		</div>
		<div class="flex-column-wrapper">
			<div class="flex-column flex-column-stipulation sm-tooltop-wrapper">
				<%If stipulationItems IsNot Nothing AndAlso stipulationItems.Any() Then%>
				<div class="sm-tooltip-trigger">
					<input type="checkbox"/>
					<ul>
						<%For Each sti In stipulationItems%>
						<li><%=sti.Key%></li>
						<%Next%>
					</ul>
					<i class="fa fa-gavel"></i>
				</div>
				
				<%End If%>
			</div>
			<div class="flex-column flex-column-im">
				<a href="#"><i class="fa fa-envelope-o"></i></a>
			</div>
			<div class="flex-column flex-column-action">
				<div class="dropdown inline-drop-menu">
					<button class="btn dropdown-toggle btn-dropdown" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
						<img src="/agent/content/images/fa_circle_blue.png"/>
						<img src="/agent/content/images/fa_circle_blue.png"/>
						<img src="/agent/content/images/fa_circle_blue.png"/>
					</button>
					<ul class="dropdown-menu dropdown-menu-right">
						<%If item.Status = "INCOMPLETE" Then%>
						<li><a target="_blank" href="<%=BuildIncompletedLoanUrl(item)%>" data-command="continue-incomplete-app">Continue InComplete App</a></li>
						<%End If%>
						<li><a href="#" data-command="message-officer">Message Officer</a></li>
						<li><a href="#" data-command="view-stipulations">View Stipulations</a></li>
						<li><a href="#" data-command="view-denial-reasons">View Denial Reasons</a></li>
						<li><a href="#" data-command="upload-document">Upload Document</a></li>
					</ul>
				</div>
			</div>
		</div>
	</div>
	<%--<div class="flex-row">
		<div class="flex-column flex-column-checkbox"><input type="checkbox"/></div>
		<div class="flex-column-wrapper flex-column-1">
			<div class="flex-column flex-column-applicants">
				<a href="#">Jonathan Smith</a>
				<p>New 2018 Dodge Ram</p>
			</div>
			<div class="flex-column flex-column-loaninfo">
				<p>$35,000</p>
				<p>60 mo   3.25%</p>
			</div>
		</div>
		<div class="flex-column-wrapper flex-column-2">
			<div class="flex-column flex-column-status">
				<p>APPROVED</p>
				<p>NOT FUND</p>
			</div>
			<div class="flex-column flex-column-date">
				<p>03/12/18 3:30pm</p>
				<p>04/12/18 3am</p>
			</div>
		</div>
		<div class="flex-column-wrapper flex-column-3">
			<div class="flex-column flex-column-stipulation">
				<a href="#"><i class="fa fa-gavel"></i></a>
			</div>
			<div class="flex-column flex-column-im">
				<a href="#"><i class="fa fa-envelope-o"></i></a>
			</div>
			<div class="flex-column flex-column-action">
				<div class="dropdown inline-drop-menu">
					<button class="btn dropdown-toggle btn-dropdown" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
						<i class="fa fa-circle"></i>
						<i class="fa fa-circle"></i>
						<i class="fa fa-circle"></i>
					</button>
					<ul class="dropdown-menu dropdown-menu-right">
						<li><a href="#" data-command="continue-incomplete-app">Continue InComplete App</a></li>
						<li><a href="#" data-command="message-officer">Message Officer</a></li>
						<li><a href="#" data-command="view-stipulations">View Stipulations</a></li>
						<li><a href="#" data-command="view-denial-reasons">View Denial Reasons</a></li>
						<li><a href="#" data-command="upload-document">Upload Document</a></li>
					</ul>
				</div>
			</div>
		</div>
	</div>--%>

	<%
	Next
End If%>
	<div class="js-paging-block">
		<uc:GridFooter ID="ucGridFooter" runat="server"></uc:GridFooter>
	</div>
	<%--<div class="no-record">No record found</div>--%>
</div>

