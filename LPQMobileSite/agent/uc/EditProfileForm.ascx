﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="EditProfileForm.ascx.vb" Inherits="agent_uc_EditProfileForm" %>
<div style="padding: 20px; max-height: 500px; overflow: auto;">
<form id="frm_editProfile">
	<%If profileData IsNot Nothing Then%>
    <div class="row">
        <div class="col-xs-12">
            <div class="form-group">
				<label class="required-field">First Name</label>
				<input type="text" class="form-control" name="FirstName" id="txtFirstName" value="<%=profileData.FirstName%>" placeholder="First Name" data-message-require="Please fill out this field" data-required="true" maxlength="50" />
			</div>
			<div class="form-group">
				<label class="required-field">Last Name</label>
				<input type="text" class="form-control" name="LastName" id="txtLastName" placeholder="Last Name" value="<%=profileData.LastName%>" data-message-require="Please fill out this field" data-required="true" maxlength="50" />
			</div>
			<%--Todo: next phase, allow user change their email. After we have done with action logs--%>
			<%--<div class="form-group">
				<label class="required-field">Email</label>
				<input type="text" class="form-control" name="Email" id="txtEmail" value="<%=profileData.Email%>" placeholder="First Name" data-message-require="Please fill out this field" data-required="true" maxlength="50" />
			</div>--%>
			<div class="form-group">
				<label>Avatar</label>
				<div class="logo-upload-zone dropzone" id="divAvatar" data-current-logo=""></div>
				<%If AvatarFileInfo IsNot Nothing Then%>
				<input type="hidden" value="<%=profileData.Avatar%>" name="Avatar" id="hdAvatar" data-file-ext="<%=AvatarFileInfo.Extension%>" data-file-name="<%=AvatarFileInfo.Name%>" data-file-size="<%=AvatarFileInfo.Length%>"/>
				<%Else%>
				<input type="hidden" value="" name="Avatar" id="hdAvatar" data-file-ext="" data-file-name="filename" data-file-size="0"/>
				<%End If%>
			</div>
			<div class="form-group">
				<label class="required-field">Phone Number</label>
				<input type="text" class="form-control" name="Phone" id="txtPhone" value="<%=profileData.Phone%>" data-message-invalid-data="Valid phone number is required" data-format="phone" placeholder="Phone Number" maxlength="20" />
			</div>
        </div>
    </div>
	<%End If%>
</form>
</div>