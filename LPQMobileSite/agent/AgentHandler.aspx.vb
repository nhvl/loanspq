﻿
Imports System.IO
Imports Newtonsoft.Json
Imports LPQMobile.Utils
Imports Sm

Partial Class agent_AgentHandler
	Inherits SmBaseHandler
	Protected AllowedRoles As List(Of String)
	Protected CurrentUserRole As SmUserRole

	Private NotCheckUserRoleIDActionList As New List(Of String)

	Private Sub Page_PreLoad(sender As Object, e As EventArgs) Handles Me.PreLoad
		If Not IsPostBack Then
			NotCheckUserRoleIDActionList.Add("loadEditProfileForm")
			NotCheckUserRoleIDActionList.Add("updateProfile")
			NotCheckUserRoleIDActionList.Add("uploadavatar")
			NotCheckUserRoleIDActionList.Add("deleteavatar")
			NotCheckUserRoleIDActionList.Add("loadChangePasswordForm")
			NotCheckUserRoleIDActionList.Add("changePassword")

			Dim userRoleID = Common.SafeGUID(Request.Form("id"))
			Dim cmd As String = Common.SafeString(Request.Form("command"))
			If userRoleID = Guid.Empty AndAlso Not NotCheckUserRoleIDActionList.Any(Function(c) c = cmd) Then
				Response.StatusCode = 404 '' Forbidden
				Response.StatusDescription = "Not found"
				Response.End() '' Terminate all the rest processes
			End If
			AllowedRoles = New List(Of String)
			If userRoleID <> Guid.Empty Then
				CurrentUserRole = smBL.GetUserRoleByID(userRoleID)
				If CurrentUserRole IsNot Nothing AndAlso CurrentUserRole.UserID = UserInfo.UserID AndAlso CurrentUserRole.Status = SmSettings.UserRoleStatus.Active AndAlso Not String.IsNullOrEmpty(CurrentUserRole.VendorID) AndAlso CurrentUserRole.VendorID.ToString().ToUpper() <> Guid.Empty.ToString().ToUpper() AndAlso CurrentUserRole.Vendor IsNot Nothing Then
					AllowedRoles.Add(String.Format("VendorScope${0}${1}${2}${3}", SmSettings.Role.VendorAdmin.ToString(), CurrentUserRole.LenderID.ToString(), CurrentUserRole.LenderConfigID.ToString(), CurrentUserRole.VendorID))
					AllowedRoles.Add(String.Format("VendorScope${0}${1}${2}${3}", SmSettings.Role.VendorUser.ToString(), CurrentUserRole.LenderID.ToString(), CurrentUserRole.LenderConfigID.ToString(), CurrentUserRole.VendorID))
				Else
					Response.StatusCode = 404 '' Forbidden
					Response.StatusDescription = "Not found"
					Response.End() '' Terminate all the rest processes
				End If
			End If
		End If
	End Sub
	Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
		If Not IsPostBack Then
			Dim cmd As String = Common.SafeString(Request.Form("command"))
			If NotCheckUserRoleIDActionList.Any(Function(c) c = cmd) OrElse AllowedRoles IsNot Nothing AndAlso AllowedRoles.Any() AndAlso SmUtil.CheckRoleAccess(HttpContext.Current, AllowedRoles.ToDictionary(Function(p) p, Function(p) p)) Then
				_log.Debug("Recieved command: " & Common.SafeString(cmd))
				Select Case cmd
					Case "loadLoanGrid"
						LoadLoanGrid()
					Case "loadAgentGrid"
						LoadAgentGrid()
					Case "changeAgentState"
						ChangeAgentState()
					Case "deleteAgent"
						DeleteAgent()
					Case "unlockAgent"
						UnlockAgent()
					Case "uploadavatar"
						UploadAvatar()
					Case "deleteavatar"
						DeleteAvatar()
					Case "loadAddAgentForm"
						LoadAddAgentForm()
					Case "saveNewAgent"
						SaveNewAgent()
					Case "loadEditProfileForm"
						LoadEditProfileForm()
					Case "updateProfile"
						UpdateProfile()
					Case "loadChangePasswordForm"
						LoadChangePasswordForm()
					Case "changePassword"
						ChangePassword()
				End Select
			Else
				Response.StatusCode = 404 '' Forbidden
				Response.StatusDescription = "Not found"
				Response.End() '' Terminate all the rest processes
			End If
		End If
	End Sub
#Region "Misc"
	Private Sub UploadAvatar()
		Dim fileUrl = ""
		Try
			If Request.Files.Count > 0 Then
				Dim file As HttpPostedFile = Request.Files(0)
				If file IsNot Nothing AndAlso file.ContentLength > 0 AndAlso (file.ContentType = "image/png" Or file.ContentType = "image/jpeg") Then
					Dim fileExt As String = New FileInfo(file.FileName).Extension
					Dim fileName As String = Guid.NewGuid().ToString().Replace("-", "").Substring(0, 10) & fileExt
					If Not String.IsNullOrWhiteSpace(ConfigurationManager.AppSettings.Get("AZURE_MEDIA_ACCOUNT_NAME")) Then
						Using reader = New BinaryReader(file.InputStream)
							Dim fileContent As Byte() = reader.ReadBytes(file.ContentLength)
							Dim azureSrv As New CAzureStorageService(ConfigurationManager.AppSettings.Get("AZURE_MEDIA_ACCOUNT_NAME"), ConfigurationManager.AppSettings.Get("AZURE_MEDIA_ACCOUNT_KEY"))
							Dim avatarContainerName As String = ConfigurationManager.AppSettings.Get("AZURE_AVATAR_CONTAINER")
							If String.IsNullOrWhiteSpace(avatarContainerName) Then avatarContainerName = "avatars"
							fileUrl = azureSrv.UploadBlob(avatarContainerName, fileName, fileContent, file.ContentType)
						End Using
					Else
						Dim avatarFolderPath As String = IO.Path.Combine(Server.MapPath("~/images"), "avatars")
						If Not Directory.Exists(avatarFolderPath) Then
							Directory.CreateDirectory(avatarFolderPath)
						End If
						file.SaveAs(Path.Combine(avatarFolderPath, fileName))
						fileUrl = "/images/avatars/" & fileName
					End If
				End If
			End If
		Catch ex As Exception
			_log.Error("Error while saving avatar", ex)
		End Try
		Response.Write(fileUrl)
		Response.End()
	End Sub
	Private Sub DeleteAvatar()
		Dim result As String = ""
		Try
			'https://lpqhieplam.blob.core.windows.net/avatars/8915a919bc.png
			If Not String.IsNullOrWhiteSpace(ConfigurationManager.AppSettings.Get("AZURE_MEDIA_ACCOUNT_NAME")) Then
				Dim avatarContainerName As String = ConfigurationManager.AppSettings.Get("AZURE_AVATAR_CONTAINER")
				If String.IsNullOrWhiteSpace(avatarContainerName) Then avatarContainerName = "avatars"
				Dim m As Match = Regex.Match(Common.SafeStripHtmlString(Request.Form("fileurl")), String.Format("{0}/(?<filename>\w+.(?:png|jpg|jpeg))$", avatarContainerName))
				If m.Success Then
					Dim azureSrv As New CAzureStorageService(ConfigurationManager.AppSettings.Get("AZURE_MEDIA_ACCOUNT_NAME"), ConfigurationManager.AppSettings.Get("AZURE_MEDIA_ACCOUNT_KEY"))
					azureSrv.DeleteBlob(avatarContainerName, m.Groups("filename").Value)
				End If
			Else
				Dim m As Match = Regex.Match(Common.SafeStripHtmlString(Request.Form("fileurl")), "images/avatars/(?<filename>\w+.(?:png|jpg|jpeg))$")
				If m.Success Then
					Dim logoFilePath As String = IO.Path.Combine(Server.MapPath("~/images"), "avatars", m.Groups("filename").Value)
					If IO.File.Exists(logoFilePath) Then
						IO.File.Delete(logoFilePath)
					End If
				End If
			End If
			result = "done"
		Catch ex As Exception
			_log.Error("Error while deleting avatar", ex)
			result = "error"
		End Try
		Response.Write(result)
		Response.End()
	End Sub
#End Region
#Region "Loan"
	Private Sub LoadLoanGrid()
		Try
			Dim paging = JsonConvert.DeserializeObject(Of SmPagingInfoModel)(Request.Form("paging_info"))
			Dim filter = JsonConvert.DeserializeObject(Of SmLoanListFilter)(Request.Form("filter_info"))
			Dim control As agent_uc_LoanList = CType(LoadControl("~/agent/uc/LoanList.ascx"), agent_uc_LoanList)
			control.PagingInfo = paging
			If filter Is Nothing Then
				filter = New SmLoanListFilter()
			End If
			filter.UserID = UserInfo.UserID
			control.FilterInfo = filter
			Me.Controls.Add(control)
		Catch ex As Exception
			_log.Error("Could not load loan grid", ex)
			Response.Write("<h3 style='text-align: center; color: red;'>Unable to render content. Please try again later</h3>")
		End Try
	End Sub
#End Region
#Region "Agent"
	Private Sub LoadAgentGrid()
		Try
			Dim paging = JsonConvert.DeserializeObject(Of SmPagingInfoModel)(Request.Form("paging_info")).MakeItSafe()
			'Dim filter = JsonConvert.DeserializeObject(Of SmVendorUserRoleListFilter)(Request.Form("filter_info")).MakeItSafe()
			Dim lenderConfig = smBL.GetPortalBasicInfoByLenderConfigID(CurrentUserRole.LenderConfigID)
			If lenderConfig IsNot Nothing Then
				Dim control As agent_uc_AgentList = CType(LoadControl("~/Agent/Uc/AgentList.ascx"), agent_uc_AgentList)
				control.PagingInfo = paging
				Dim filter = New SmVendorUserRoleListFilter()
				filter.LenderConfigID = lenderConfig.LenderConfigID
				filter.VendorIDList.Add(CurrentUserRole.VendorID)
				filter.Roles.Add(SmSettings.Role.VendorUser)
				control.FilterInfo = filter.MakeItSafe()
				Me.Controls.Add(control)
			Else
				Response.StatusCode = 404 '' Forbidden
				Response.StatusDescription = "Not found"
				Response.End() '' Terminate all the rest processes
			End If
		Catch ex As Exception
			_log.Error("Could not load agent grid", ex)
			Response.Write("<h3 style='text-align: center; color: red;'>Unable to render content. Please try again later</h3>")
		End Try
	End Sub

	Private Sub DeleteAgent()
		Dim res As CJsonResponse
		Try
			Dim smBL As New SmBL()
			Dim smAuthBL As New SmAuthBL()
			Dim userRoleID As Guid = Common.SafeGUID(Request.Form("rId"))
			Dim comment As String = Common.SafeString(Request.Form("comment"))
			Dim ipAddress As String = Request.ServerVariables("REMOTE_ADDR")
			Dim lenderConfigID As Guid = Common.SafeGUID(Request.Form("lenderConfigID"))
			Dim lenderConfig = smBL.GetPortalBasicInfoByLenderConfigID(lenderConfigID)
			Dim userRole As SmUserRole = smBL.GetUserRoleByID(userRoleID)
			If userRole Is Nothing Then
				res = New CJsonResponse(True, "", "OK")
			Else
				Dim u = smAuthBL.GetUserByID(userRole.UserID)
				If lenderConfig IsNot Nothing AndAlso u IsNot Nothing Then
					Dim result = smBL.RemoveVendorUser(userRole, comment, lenderConfig, u, UserInfo.UserID, ipAddress)
					If result = "ok" Then
						res = New CJsonResponse(True, "", "OK")
					Else
						res = New CJsonResponse(False, "", "FAILED")
					End If
				Else
					res = New CJsonResponse(False, "", "FAILED")
					Response.StatusCode = 404 '' Forbidden
					Response.StatusDescription = "Not found"
					Response.End() '' Terminate all the rest processes
				End If
			End If
		Catch ex As Exception
			_log.Error("Could not DeleteAgent.", ex)
			res = New CJsonResponse(False, "Could not remove agent. Please try again", "FAILED")
		End Try
		Response.Write(JsonConvert.SerializeObject(res))
		Response.End()
	End Sub
	Private Sub UnlockAgent()
		Dim res As CJsonResponse
		Try
			Dim smAuthBL As New SmAuthBL()
			Dim userRoleID As Guid = Common.SafeGUID(Request.Form("rId"))
			Dim ipAddress As String = Request.ServerVariables("REMOTE_ADDR")
			Dim userRole As SmUserRole = smBL.GetUserRoleByID(userRoleID)
			If userRole Is Nothing Then
				Response.StatusCode = 404 '' Forbidden
				Response.StatusDescription = "Not found"
				Response.End() '' Terminate all the rest processes
			End If
			If userRole.User.Status = SmSettings.UserStatus.Active AndAlso userRole.User.LoginFailedCount >= Common.SafeInteger(ConfigurationManager.AppSettings("LoginFailedCountExceed")) Then
				userRole.User.LoginFailedCount = 0
			End If
			If userRole.User.Status = SmSettings.UserStatus.Active AndAlso userRole.User.LastLoginDate.HasValue AndAlso userRole.User.LastLoginDate.Value.AddDays(userRole.User.ExpireDays) < DateTime.Now Then
				userRole.User.LastLoginDate = Now
			End If
			Dim result = smAuthBL.UnlockUser(userRole.User, UserInfo.UserID, ipAddress)
			If result = "ok" Then
				res = New CJsonResponse(True, "", "OK")
			Else
				res = New CJsonResponse(False, "", "FAILED")
			End If
		Catch ex As Exception
			_log.Error("Could not UnlockAgent.", ex)
			res = New CJsonResponse(False, "Could not unlock agent. Please try again", "FAILED")
		End Try
		Response.Write(JsonConvert.SerializeObject(res))
		Response.End()
	End Sub
	Private Sub ChangeAgentState()
		Dim res As CJsonResponse
		Try
			Dim smBL As New SmBL()
			Dim smAuthBL As New SmAuthBL()
			Dim userRoleID As Guid = Common.SafeGUID(Request.Form("rID"))
			Dim comment As String = Common.SafeStripHtmlString(Request.Form("comment"))
			Dim isEnabled As String = Common.SafeStripHtmlString(Request.Form("enable")).ToUpper()
			Dim ipAddress As String = Request.ServerVariables("REMOTE_ADDR")
			Dim lenderConfigID As Guid = Common.SafeGUID(Request.Form("lenderConfigID"))
			Dim lenderConfig = smBL.GetPortalBasicInfoByLenderConfigID(lenderConfigID)
			Dim userRole As SmUserRole = smBL.GetUserRoleByID(userRoleID)
			Dim u As SmUser
			If userRole IsNot Nothing Then
				u = smAuthBL.GetUserByID(userRole.UserID)
			End If
			If lenderConfig IsNot Nothing AndAlso userRole IsNot Nothing AndAlso u IsNot Nothing Then
				Dim result = smBL.ChangeVendorUserRoleState(userRoleID, IIf(isEnabled = "Y", SmSettings.UserRoleStatus.Active, SmSettings.UserRoleStatus.InActive), lenderConfig, u, userRole.Vendor, UserInfo.UserID, ipAddress, comment)
				If result = "ok" Then
					res = New CJsonResponse(True, "", "OK")
				Else
					res = New CJsonResponse(False, "", "FAILED")
				End If
			Else
				Response.StatusCode = 404 '' Forbidden
				Response.StatusDescription = "Not found"
				Response.End() '' Terminate all the rest processes
			End If
		Catch ex As Exception
			_log.Error("Could not ChangeAgentState.", ex)
			res = New CJsonResponse(False, "Could not change role state of agent. Please try again", "FAILED")
		End Try
		Response.Write(JsonConvert.SerializeObject(res))
		Response.End()
	End Sub
	
	Private Sub LoadAddAgentForm()
		Dim res As CJsonResponse
		Try
			Dim smBL As New SmBL()
			If smBL.CheckLenderExistedByLenderConfigID(Common.SafeGUID(Request.Form("lenderConfigID"))) Then
				Dim control As agent_uc_AddAgentForm = CType(LoadControl("~/Agent/Uc/AddAgentForm.ascx"), agent_uc_AddAgentForm)
				Me.Controls.Add(control)
			Else
				Response.StatusCode = 404 '' Forbidden
				Response.StatusDescription = "Not found"
				Response.End() '' Terminate all the rest processes
			End If

		Catch ex As Exception
			_log.Error("Could not LoadAddAgentForm.", ex)
			res = New CJsonResponse(False, "Could not load add agent form. Please try again", "FAILED")
		End Try
	End Sub

	Private Sub SaveNewAgent()
		Dim res As CJsonResponse
		Try
			Dim smBL As New SmBL()
			Dim smAuthBL As New SmAuthBL()
			Dim lenderConfigID As Guid = Common.SafeGUID(Request.Form("lenderConfigID"))
			Dim lenderConfig As SmLenderConfigBasicInfo = smBL.GetPortalBasicInfoByLenderConfigID(lenderConfigID)
			Dim email As String = Common.SafeStripHtmlString(Request.Form("email"))
			Dim ipAddress As String = Request.ServerVariables("REMOTE_ADDR")
			If lenderConfig Is Nothing OrElse String.IsNullOrEmpty(email) Then
				Response.StatusCode = 404 '' Forbidden
				Response.StatusDescription = "Not found"
				Response.End() '' Terminate all the rest processes
				Return
			End If
			Dim existedUser As SmUser = smAuthBL.GetUserByEmail(email)
			Dim userObj As New SmUser
			If existedUser Is Nothing Then
				userObj.UserID = Guid.NewGuid()
				userObj.Role = SmSettings.Role.User
				userObj.Email = email
			Else
				userObj = existedUser
			End If

			Dim lenderRoleList As New List(Of SmUserRole)
			If existedUser IsNot Nothing Then
				lenderRoleList = smBL.GetLenderUserRoleList(existedUser.UserID, lenderConfig.LenderID)
			End If
			If lenderRoleList IsNot Nothing AndAlso lenderRoleList.Any(Function(r) r.Role = SmSettings.Role.VendorUser AndAlso r.LenderConfigID = lenderConfig.LenderConfigID AndAlso r.VendorID = CurrentUserRole.VendorID) Then
				'check vendoruser already existed
				res = New CJsonResponse(False, String.Format("This email has been being used by an {0} of {1} already.", SmSettings.Role.VendorUser.GetEnumDescription(), CurrentUserRole.Vendor.VendorName), "ALREADY")
			ElseIf lenderRoleList IsNot Nothing AndAlso lenderRoleList.Any(Function(r) r.Role = SmSettings.Role.VendorAdmin AndAlso r.LenderConfigID = lenderConfig.LenderConfigID AndAlso r.VendorID = CurrentUserRole.VendorID) Then
				'check vendoradmin already existed
				res = New CJsonResponse(False, String.Format("This email has been being used by an {0} of {1} already.", SmSettings.Role.VendorAdmin.GetEnumDescription(), CurrentUserRole.Vendor.VendorName), "ALREADY")
			Else
				Dim userRole As New SmUserRole() With {
										  .LenderConfigID = CurrentUserRole.LenderConfigID,
										  .LenderID = CurrentUserRole.LenderID,
										  .Role = SmSettings.Role.VendorUser,
										  .Status = SmSettings.UserRoleStatus.Active,
										  .UserID = userObj.UserID,
										  .UserRoleID = Guid.NewGuid(),
										  .VendorID = CurrentUserRole.VendorID
											}
				Dim result As String = smBL.AddUserRole(userRole, UserInfo.UserID, ipAddress, True)

				If result = "ok" Then
					If existedUser Is Nothing Then
						Dim token As String = Guid.NewGuid().ToString().Replace("-", "").Substring(0, 20)
						userObj.ExpireDays = 0 ' leave it by default
						result = smAuthBL.AddUser(userObj, UserInfo.UserID, ipAddress, token)
						If result <> "ok" Then
							'rollback adding user role action
							smBL.RemoveUserRoleByUserRoleID(userRole.UserRoleID)
							Response.Write(JsonConvert.SerializeObject(New CJsonResponse(False, result, "FAILED")))
							Response.End()
							Return
						End If
						SmUtil.SendEmail(userObj.Email, "MeridianLink's APM Account Activation", SmUtil.BuildActivateAccountEmail(token, _serverRoot))
					End If

					Dim backOfficeLender = smBL.GetBackOfficeLenderByLenderConfigID(lenderConfig.LenderConfigID)
					Dim emailContent As String = SmUtil.BuildVendorRoleAssignedNotification(backOfficeLender.LenderName, lenderConfig.Note, CurrentUserRole.Vendor.VendorName)
					SmUtil.SendEmail(userObj.Email, "MeridianLink's APM Role Assign Notification", emailContent)
					res = New CJsonResponse(True, "", "OK")
				Else
					res = New CJsonResponse(False, result, "FAILED")
				End If
			End If

		Catch ex As Exception
			_log.Error("Could not SaveNewAgent.", ex)
			res = New CJsonResponse(False, "Could not add new agent. Please try again", "FAILED")
		End Try
		Response.Write(JsonConvert.SerializeObject(res))
		Response.End()
	End Sub
#End Region
#Region "Profile"
	Private Sub LoadEditProfileForm()
		Dim res As CJsonResponse
		Try
			Dim smAuthBL As New SmAuthBL
			Dim profileData = smAuthBL.GetUserByID(UserInfo.UserID)
			If profileData IsNot Nothing AndAlso profileData.Status = SmSettings.UserStatus.Active Then
				Dim control As agent_uc_EditProfileForm = CType(LoadControl("~/Agent/Uc/EditProfileForm.ascx"), agent_uc_EditProfileForm)
				control.ProfileData = profileData
				If Not String.IsNullOrEmpty(profileData.Avatar) Then
					If profileData.Avatar.StartsWith("/images/avatars/") AndAlso File.Exists(Server.MapPath(profileData.Avatar)) Then
						control.AvatarFileInfo = New FileInfo(Server.MapPath(profileData.Avatar))
					End If
				End If
				Me.Controls.Add(control)
			Else
				Response.StatusCode = 404 '' Forbidden
				Response.StatusDescription = "Not found"
				Response.End() '' Terminate all the rest processes
			End If
			Catch ex As Exception
			_log.Error("Could not LoadEditProfileForm.", ex)
			res = New CJsonResponse(False, "Could not load edit profile form. Please try again", "FAILED")
		End Try
	End Sub

	Private Sub UpdateProfile()
		Dim res As CJsonResponse
		Try
			Dim smAuthBL As New SmAuthBL()
			Dim profileObj = JsonConvert.DeserializeObject(Of SmProfileModel)(Common.SafeString(Request.Form("profile_data")), New JsonSerializerSettings() With {.NullValueHandling = NullValueHandling.Ignore}).MakeItSafe()
			Dim currentUser As SmUser = smAuthBL.GetUserByID(UserInfo.UserID)
			If currentUser Is Nothing Then
				Response.StatusCode = 404 '' Forbidden
				Response.StatusDescription = "Not found"
				Response.End() '' Terminate all the rest processes
				Return
			End If
			Dim ipAddress As String = Request.ServerVariables("REMOTE_ADDR")
			currentUser.FirstName = profileObj.FirstName
			currentUser.LastName = profileObj.LastName
			currentUser.Phone = profileObj.Phone
			currentUser.Avatar = profileObj.Avatar
			Dim result = smAuthBL.UpdateUser(currentUser, UserInfo.UserID, ipAddress)
			If result = "ok" Then
				res = New CJsonResponse(True, "", New With {.avatar = IIf(String.IsNullOrEmpty(currentUser.Avatar.Trim()), "/images/avatar.jpg", currentUser.Avatar), .fullName = currentUser.FullName, .firstName = currentUser.FirstName, .phone = IIf(String.IsNullOrEmpty(currentUser.Phone), "", Regex.Replace(currentUser.Phone, "(\d{3})(\d{3})(\d{4})", "$1-$2-$3"))})
				currentUser.Password = String.Empty
				currentUser.Salt = String.Empty
				HttpContext.Current.Session("CURRENT_USER_INFO") = currentUser
			Else
				res = New CJsonResponse(False, "Could not update your profile. Please try again later.", "FAILED")
			End If
		Catch ex As Exception
			_log.Error("Could not UpdateProfile.", ex)
			res = New CJsonResponse(False, "Could not update your profile. Please try again", "FAILED")
		End Try
		Response.Write(JsonConvert.SerializeObject(res))
		Response.End()
	End Sub

	Private Sub LoadChangePasswordForm()
		Dim res As CJsonResponse
		Try
			Dim smAuthBL As New SmAuthBL
			Dim profileData = smAuthBL.GetUserByID(UserInfo.UserID)
			If profileData IsNot Nothing AndAlso profileData.Status = SmSettings.UserStatus.Active Then
				Dim control As agent_uc_ChangePasswordForm = CType(LoadControl("~/Agent/Uc/ChangePasswordForm.ascx"), agent_uc_ChangePasswordForm)
				Me.Controls.Add(control)
			Else
				Response.StatusCode = 404 '' Forbidden
				Response.StatusDescription = "Not found"
				Response.End() '' Terminate all the rest processes
			End If
		Catch ex As Exception
			_log.Error("Could not LoadChangePasswordForm.", ex)
			res = New CJsonResponse(False, "Could not load change password form. Please try again", "FAILED")
		End Try
	End Sub

	Private Sub ChangePassword()
		Dim res As CJsonResponse
		Try
			Dim smAuthBL As New SmAuthBL()
			Dim modelObj = JsonConvert.DeserializeObject(Of SmChangePasswordModel)(Common.SafeString(Request.Form("password_data")), New JsonSerializerSettings() With {.NullValueHandling = NullValueHandling.Ignore}).MakeItSafe()
			Dim currentUser As SmUser = smAuthBL.GetUserByID(UserInfo.UserID)
			If currentUser Is Nothing OrElse modelObj.NewPassword <> modelObj.ConfirmNewPassword Then
				Response.StatusCode = 404 '' Forbidden
				Response.StatusDescription = "Not found"
				Response.End() '' Terminate all the rest processes
				Return
			End If
			Dim hashPassword As String = SmUtil.GetSHA256Hash(modelObj.CurrentPassword, currentUser.Salt)
			If currentUser.LoginFailedCount >= Common.SafeInteger(ConfigurationManager.AppSettings("LoginFailedCountExceed")) Then
				res = New CJsonResponse(False, "Current password is not correct", "INCORRECT")
			ElseIf hashPassword <> currentUser.Password Then
				res = New CJsonResponse(False, "Current password is not correct", "INCORRECT")
				smAuthBL.UpdateLoginFailureCount(currentUser.UserID, currentUser.LoginFailedCount + 1)
			Else
				Dim ipAddress As String = Request.ServerVariables("REMOTE_ADDR")
				Dim newSalt As String = SmUtil.GetSalt()
				Dim checkValidPasswordResult = smAuthBL.CheckValidPassword(currentUser, modelObj.NewPassword)
				If checkValidPasswordResult = "" Then
					hashPassword = SmUtil.GetSHA256Hash(modelObj.NewPassword, newSalt)
					Dim result = smAuthBL.ChangePassword(currentUser.UserID, hashPassword, newSalt, UserInfo.UserID, ipAddress)
					If result = "ok" Then
						res = New CJsonResponse(True, "", "OK")
						smAuthBL.UpdateLoginFailureCount(currentUser.UserID, 0)
					Else
						res = New CJsonResponse(False, "Could not change you password. Please try again later.", "FAILED")
					End If
				Else
					res = New CJsonResponse(False, checkValidPasswordResult, "INVALID")
				End If
			End If
		Catch ex As Exception
			_log.Error("Could not ChangePassword.", ex)
			res = New CJsonResponse(False, "Could not change you password. Please try again", "FAILED")
		End Try
		Response.Write(JsonConvert.SerializeObject(res))
		Response.End()
	End Sub
#End Region
End Class
