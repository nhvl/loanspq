﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="Profile.aspx.vb" Inherits="agent_Profile"  MasterPageFile="MasterPage.master" %>

<asp:Content runat="server" ContentPlaceHolderID="plBody">
	<div class="container-fluid da-block-wrapper profile-container">
		<div class="row">	
			<div class="col-sm-12 col-md-8 col-md-offset-2 col-lg-6 col-lg-offset-3 da-main">
				<div class="da-block">
					<div class="block-header">
						<span>Personal Information</span>
						<ul class="inline-btn-group no-context-menu">
							<li><a href="#" id="btnEditProfile"><i class="fa fa-pencil-square-o"></i> EDIT</a></li>
						</ul>
					</div>
					<div class="block-content">
						<ul class="profile-fields">
							<li>
								<div>Name</div>
								<div class="js-profile-fullname"><%=profileData.FullName%></div>	
							</li>
							<li>
								<div>Phone</div>
								<div class="js-profile-phone"><%If String.IsNullOrEmpty(profileData.Phone) Then%>-<%Else%><%=Regex.Replace(profileData.Phone, "(\d{3})(\d{3})(\d{4})", "$1-$2-$3")%></div><%End If %>
							</li>
							<li class="avatar-field">
								<div>Avatar</div>
								<div class="avatar">
									<span class="js-profile-avatar" style="background-image: url('<%=IIf(String.IsNullOrEmpty(profileData.Avatar), "/images/avatar.jpg", profileData.Avatar)%>')"></span>
								</div>
							</li>
							<li>
								<div>Password</div>
								<div class="password-content">xxxxxxxxx <a href="#" id="btnChangePassword">(change password)</a></div>
							</li>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</div>
	
</asp:Content>
<asp:Content runat="server" ContentPlaceHolderID="plScripts">
		<script type="text/javascript">
			$(function () {
				$("#btnEditProfile").click(manageProfile.FACTORY.loadEditProfileForm);
				$("#btnChangePassword").click(manageProfile.FACTORY.loadChangePasswordForm);
			});
			(function (manageProfile, $, undefined) {
				manageProfile.DATA = {};
				manageProfile.FACTORY = {};
				manageProfile.FACTORY.loadEditProfileForm = function () {
					_COMMON.showDialogSaveClose("Edit Profile", 0, "editprofile_dialog", "", "/agent/agenthandler.aspx", {command: "loadEditProfileForm" }, manageProfile.FACTORY.updateProfile, function (container) {
						//add validator
						initEditUploadUserAvatar("#divAvatar", "#hdAvatar");
						registerDataValidator($("#editprofile_dialog"), "editprofile_dialog");
					}, function () {
						//when dialog closed
					});
				};

				manageProfile.FACTORY.updateProfile = function (obj) {
					if ($.smValidate("editprofile_dialog") == false) return;
					var profileData = obj.container.find("form").serializeJSON({ parseAll: true, checkboxUncheckedValue: "false" });
					//update data
					var postData = {
						command: "updateProfile",
						profile_data: _COMMON.toJSON(profileData)
					};
					$.ajax({
						url: "/agent/agenthandler.aspx",
						async: true,
						cache: false,
						type: 'POST',
						dataType: 'html',
						data: postData,
						success: function (responseText) {
							var response = $.parseJSON(responseText);
							if (response.IsSuccess) {
								_COMMON.noty("success", "Success", 500);
								$(".profile-info .js-profile-name").text(response.Info.firstName);
								$(".profile-tray .avatar").css("background-image", "url('" + response.Info.avatar + "')");
								$(".profile-container .profile-fields .js-profile-fullname").text(response.Info.fullName);
								$(".profile-container .profile-fields .js-profile-phone").text(response.Info.phone);
								$(".profile-container .profile-fields .js-profile-avatar").css("background-image", "url('" + response.Info.avatar + "')");
								obj.dialog.close();
							} else if (response.Info == "ALREADY") {
								_COMMON.showAlertDialog(response.Message);
							} else {
								var errorMsg = response.Message !== "" ? response.Message : "Error";
								_COMMON.noty("error", errorMsg, 500);
							}
						}
					});
				};
				manageProfile.FACTORY.loadChangePasswordForm = function () {
					_COMMON.showDialogSaveClose("Change Password", 0, "changepassword_dialog", "", "/agent/agenthandler.aspx", { command: "loadChangePasswordForm" }, manageProfile.FACTORY.changePassword, function (container) {
						//add validator
						registerDataValidator($("#changepassword_dialog"), "changepassword_dialog");
					}, function () {
						//when dialog closed
					});
				};

				manageProfile.FACTORY.changePassword = function (obj) {
					if ($.smValidate("changepassword_dialog") == false) return;
					var passwordData = obj.container.find("form").serializeJSON({ parseAll: true, checkboxUncheckedValue: "false" });
					//update data
					var postData = {
						command: "changePassword",
						password_data: _COMMON.toJSON(passwordData)
					};
					$.ajax({
						url: "/agent/agenthandler.aspx",
						async: true,
						cache: false,
						type: 'POST',
						dataType: 'html',
						data: postData,
						success: function (responseText) {
							var response = $.parseJSON(responseText);
							if (response.IsSuccess) {
								_COMMON.noty("success", "Success", 500);
								obj.dialog.close();
							} else if (response.Info == "INCORRECT") {
								$.smValidate.showValidation($("#txtCurrentPassword"), response.Message);
							} else if (response.Info == "INVALID") {
								$.smValidate.showValidation($("#txtNewPassword"), response.Message);
							} else {
								var errorMsg = response.Message !== "" ? response.Message : "Error";
								_COMMON.noty("error", errorMsg, 500);
							}
						}
					});
				};
			}(window.manageProfile = window.manageProfile || {}, jQuery));
	</script>
</asp:Content>