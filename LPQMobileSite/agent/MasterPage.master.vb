﻿
Imports Sm

Partial Class agent_MasterPage
	Inherits System.Web.UI.MasterPage

	Protected ProfileUrl As String = "/agent/profile.aspx"
	Public ReadOnly Property UserInfo() As SmUser
		Get
			Dim user As SmUser
			If Session("CURRENT_USER_INFO") IsNot Nothing Then
				user = DirectCast(Session("CURRENT_USER_INFO"), SmUser)
			Else
				Dim smAuthBL As New SmAuthBL()
				user = smAuthBL.GetCurrentUserFromContext(Context)
				user.Password = String.Empty
				user.Salt = String.Empty
				HttpContext.Current.Session.Add("CURRENT_USER_INFO", user)
			End If
			Return user
		End Get
	End Property

	Public Property CurrentVendor As SmLenderVendor
	Public Property CurrentPortal As SmLenderConfigBasicInfo

	Public Property CurrentUserRole As SmUserRole

	Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
		If Not IsPostBack Then
			If Not SmUtil.CheckRoleAccess(HttpContext.Current, SmSettings.Role.VendorUser, SmSettings.Role.VendorAdmin) Then
				Response.Redirect("Default.aspx", True)
			End If
			If CurrentUserRole IsNot Nothing Then
				ProfileUrl = ProfileUrl & "?id=" & CurrentUserRole.UserRoleID.ToString()
			End If
		End If
	End Sub

	Protected Function BuildNewAppUrl(type As String, Optional title As String = "") As String
		Dim result As String = "<li><a target='_blank' href='{0}?lenderref={1}&vendorid={2}&userid={3}'>{4}</a></li>"
		Dim eType = CType([Enum].Parse(GetType(SmSettings.LenderVendorType), type), SmSettings.LenderVendorType)
		Select Case eType
			Case SmSettings.LenderVendorType.CC
				result = String.Format(result, "/cc/creditcard.aspx", CurrentPortal.LenderRef, CurrentVendor.VendorID, UserInfo.UserID, IIf(String.IsNullOrEmpty(title), eType.GetEnumDescription(), title))
			Case SmSettings.LenderVendorType.PL
				result = String.Format(result, "/pl/personalloan.aspx", CurrentPortal.LenderRef, CurrentVendor.VendorID, UserInfo.UserID, IIf(String.IsNullOrEmpty(title), eType.GetEnumDescription(), title))
			Case SmSettings.LenderVendorType.VL
				result = String.Format(result, "/vl/VehicleLoan.aspx", CurrentPortal.LenderRef, CurrentVendor.VendorID, UserInfo.UserID, IIf(String.IsNullOrEmpty(title), eType.GetEnumDescription(), title))
			Case Else
				result = ""
		End Select
		Return result.ToLower()
	End Function
	Protected Function BuildEmailInviteUrl(type As String, Optional title As String = "") As String
		Dim result As String = "<li><a target='_blank' href='{0}?lenderref={1}&vendorid={2}&userid={3}'>{4}</a></li>"
		Dim eType = CType([Enum].Parse(GetType(SmSettings.LenderVendorType), type), SmSettings.LenderVendorType)
		Select Case eType
			Case SmSettings.LenderVendorType.CC
				result = String.Format(result, "/cc/creditcard.aspx", CurrentPortal.LenderRef, CurrentVendor.VendorID, UserInfo.UserID, IIf(String.IsNullOrEmpty(title), eType.GetEnumDescription(), title))
			Case SmSettings.LenderVendorType.PL
				result = String.Format(result, "/pl/personalloan.aspx", CurrentPortal.LenderRef, CurrentVendor.VendorID, UserInfo.UserID, IIf(String.IsNullOrEmpty(title), eType.GetEnumDescription(), title))
			Case SmSettings.LenderVendorType.VL
				result = String.Format(result, "/vl/VehicleLoan.aspx", CurrentPortal.LenderRef, CurrentVendor.VendorID, UserInfo.UserID, IIf(String.IsNullOrEmpty(title), eType.GetEnumDescription(), title))
			Case Else
				result = ""
		End Select
		Return result.ToLower()
	End Function
	Protected Function BuildTextInviteUrl(type As String, Optional title As String = "") As String
		Dim result As String = "<li><a target='_blank' href='{0}?lenderref={1}&vendorid={2}&userid={3}'>{4}</a></li>"
		Dim eType = CType([Enum].Parse(GetType(SmSettings.LenderVendorType), type), SmSettings.LenderVendorType)
		Select Case eType
			Case SmSettings.LenderVendorType.CC
				result = String.Format(result, "/cc/creditcard.aspx", CurrentPortal.LenderRef, CurrentVendor.VendorID, UserInfo.UserID, IIf(String.IsNullOrEmpty(title), eType.GetEnumDescription(), title))
			Case SmSettings.LenderVendorType.PL
				result = String.Format(result, "/pl/personalloan.aspx", CurrentPortal.LenderRef, CurrentVendor.VendorID, UserInfo.UserID, IIf(String.IsNullOrEmpty(title), eType.GetEnumDescription(), title))
			Case SmSettings.LenderVendorType.VL
				result = String.Format(result, "/vl/VehicleLoan.aspx", CurrentPortal.LenderRef, CurrentVendor.VendorID, UserInfo.UserID, IIf(String.IsNullOrEmpty(title), eType.GetEnumDescription(), title))
			Case Else
				result = ""
		End Select
		Return result.ToLower()
	End Function
	Protected Function BuildNonMemberAppUrl(type As String, Optional title As String = "") As String
		Dim result As String = "<li><a target='_blank' href='{0}?lenderref={1}&vendorid={2}&userid={3}&type=1'>{4}</a></li>"
		Dim eType = CType([Enum].Parse(GetType(SmSettings.LenderVendorType), type), SmSettings.LenderVendorType)
		Select Case eType
			Case SmSettings.LenderVendorType.CC_COMBO
				result = String.Format(result, "/cc/creditcard.aspx", CurrentPortal.LenderRef, CurrentVendor.VendorID, UserInfo.UserID, IIf(String.IsNullOrEmpty(title), eType.GetEnumDescription(), title))
			Case SmSettings.LenderVendorType.PL_COMBO
				result = String.Format(result, "/pl/personalloan.aspx", CurrentPortal.LenderRef, CurrentVendor.VendorID, UserInfo.UserID, IIf(String.IsNullOrEmpty(title), eType.GetEnumDescription(), title))
			Case SmSettings.LenderVendorType.VL_COMBO
				result = String.Format(result, "/vl/VehicleLoan.aspx", CurrentPortal.LenderRef, CurrentVendor.VendorID, UserInfo.UserID, IIf(String.IsNullOrEmpty(title), eType.GetEnumDescription(), title))
			Case Else
				result = ""
		End Select
		Return result.ToLower()
	End Function
End Class

