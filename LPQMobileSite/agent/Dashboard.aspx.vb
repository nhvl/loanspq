﻿
Imports LPQMobile.Utils

Partial Class agent_Dashboard
	Inherits SmAgentBasePage

	Protected Sub Page_PreLoad(sender As Object, e As EventArgs) Handles Me.PreLoad
		Dim agentMasterPage = CType(Me.Master, agent_MasterPage)
		agentMasterPage.CurrentVendor = CurrentVendor
		agentMasterPage.CurrentPortal = CurrentPortal
		agentMasterPage.CurrentUserRole = CurrentUserRole
	End Sub

End Class
