﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="Default.aspx.vb" Inherits="agent_Default" MasterPageFile="MasterPage.master" %>


<asp:Content runat="server" ContentPlaceHolderID="plBody">
	<div class="container-fluid workspace-section">
		<%--<div class="top-section">
			<div>
				<img src="/Sm/content/images/logo.png" class="img-responsive"/>
				<h1>Welcome to the Application Portal Manager</h1>
				<p>Manage the design and configuration of your Application Portal (aka Mobile Optimized Website)</p>
			</div>
		</div>--%>
		<h2>Please select workspace that you would like to work on</h2>
		<%--<div class="row">
			<div class="col-md-8 col-md-offset-2 workspace-list row">
				<%For Each item In UserRoleList%>
				<%If item.Vendor IsNot Nothing Then%>
				<a class="col-md-3" href="/agent/Dashboard.aspx?id=<%=item.UserRoleID%>">
					<%=item.Vendor.VendorName%>
				</a>
				<%End If%>
				<%Next%>
			</div>
		</div>--%>
		<ul class="workspace-list">
			<%If SmUtil.CheckRoleAccess(HttpContext.Current, SmSettings.Role.SuperUser, SmSettings.Role.LenderAdmin, SmSettings.Role.PortalAdmin, SmSettings.Role.VendorGroupAdmin) Then%>
			<li>
				<a class="vendor-logo" href="/sm/Default.aspx"><img src="/Sm/content/images/logo.png" class="img-responsive"/></a>
				<div class="vendor-info apm-item">
					<a title="Application Portal Manager" href="/sm/Default.aspx">Application Portal Manager</a>
					<p>Manage the design and configuration of your application portal</p>
				</div>
			</li>
			<%End If%>
			<%For Each item In UserRoleList%>
			<%If item.Vendor IsNot Nothing Then
					Dim portalInfo = PortalNameList(item.LenderConfigID)
			%>
			<li>
				<a class="vendor-logo" href="/agent/Dashboard.aspx?id=<%=item.UserRoleID%>"><img src="<%=item.Vendor.Logo%>" class="img-responsive"/></a>
				<div class="vendor-info">
					<a title="<%=item.Vendor.VendorName%>" href="/agent/Dashboard.aspx?id=<%=item.UserRoleID%>"><%=item.Vendor.VendorName%></a>
					<p><%=item.Vendor.Address%></p>
					<p><%=item.Vendor.City%>, <%=item.Vendor.State%> <%=item.Vendor.Zip%></p>
					<p title="<%=portalInfo.Item2%>" class="portal-name">
						<%If portalInfo IsNot Nothing Then%>
						<%=portalInfo.Item2%> <%--- <%=portalInfo.Item1%>--%>
						<%Else %>
						unknown
						<%End If%>
					</p>
				</div>
			</li>
			<%End If%>
			<%Next%>
		</ul>
	</div>
</asp:Content>