﻿
Partial Class agent_Default
	Inherits System.Web.UI.Page

	Protected UserRoleList As List(Of SmUserRole)
	Protected PortalNameList As New Dictionary(Of Guid, Tuple(Of String, String))

	Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
		If Not IsPostBack Then
			If SmUtil.CheckRoleAccess(HttpContext.Current, SmSettings.Role.VendorAdmin, SmSettings.Role.VendorUser) Then
				'show workspace selection panel
				Dim smBL As New SmBL()
				Dim usrRoles = smBL.GetActiveUserRolesByUserID(UserInfo.UserID)
				If usrRoles Is Nothing OrElse Not usrRoles.Any(Function(p) p.Role = SmSettings.Role.VendorAdmin OrElse p.Role = SmSettings.Role.VendorUser) Then
					Response.Redirect("~/NoAccess.aspx", True)
				End If
				UserRoleList = usrRoles.Where(Function(p) p.Role = SmSettings.Role.VendorAdmin OrElse p.Role = SmSettings.Role.VendorUser).ToList()
				'If UserRoleList.Count = 1 Then
				'	Response.Redirect("Dashboard.aspx?id=" & UserRoleList.First().UserRoleID.ToString(), True)
				'End If

				Dim lenderList = smBL.GetPortals(New SmPagingInfoModel(), Nothing)
				If lenderList IsNot Nothing AndAlso lenderList.Any() Then
					For Each lender In lenderList
						If lender.PortalList IsNot Nothing AndAlso lender.PortalList.Any() Then
							For Each portal In lender.PortalList
								If Not PortalNameList.ContainsKey(portal.LenderConfigID) Then
									PortalNameList.Add(portal.LenderConfigID, New Tuple(Of String, String)(lender.LenderName, portal.Note))
								End If
							Next
						End If
					Next
				End If

			ElseIf SmUtil.CheckRoleAccess(HttpContext.Current, SmSettings.Role.SuperUser, SmSettings.Role.Accountant, SmSettings.Role.LenderAdmin, SmSettings.Role.PortalAdmin, SmSettings.Role.VendorGroupAdmin) Then
				'goto APM
				Response.Redirect("~/Sm/Default.aspx", True)
			Else
				Response.Redirect("~/NoAccess.aspx", True)
			End If
		End If
	End Sub
	Public ReadOnly Property UserInfo() As SmUser
		Get
			Dim user As SmUser
			If Session("CURRENT_USER_INFO") IsNot Nothing Then
				user = DirectCast(Session("CURRENT_USER_INFO"), SmUser)
			Else
				Dim smAuthBL As New SmAuthBL()
				user = smAuthBL.GetCurrentUserFromContext(Context)
				user.Password = String.Empty
				user.Salt = String.Empty
				HttpContext.Current.Session.Add("CURRENT_USER_INFO", user)
			End If
			Return user
		End Get
	End Property
End Class
