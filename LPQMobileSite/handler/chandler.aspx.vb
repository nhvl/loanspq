﻿
Imports Newtonsoft.Json
Imports LPQMobile.Utils
Imports System.Net.Mail

Partial Class handler_chandler
	Inherits CBasePage
	Private _log As log4net.ILog = log4net.LogManager.GetLogger(GetType(handler_chandler))
	Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            Dim command As String = Common.SafeString(Request.Params("command"))
            Dim slenderref = Common.SafeString(Request.Params("lenderref"))
            _log.Info("Handler for " & command & " from " & slenderref)
            Select Case command
                Case "SendEmailDisclosures"
                    SendEmailDisclosures()
                Case "PersistXARoleInfo"
                    PersistRoleInfo()
                Case "PersistBLRoleInfo"
                    PersistRoleInfo()
            End Select
        End If
    End Sub
	Private Sub PersistRoleInfo()
		Dim token As String = ""
		Try
			Dim roleInfo As String = Common.SafeString(Request.Form("roleinfo"))
			token = Guid.NewGuid().ToString()
			Session.Add(token, roleInfo)
		Catch ex As Exception
			_log.Info("Could not PersistRoleInfo.", ex)
			token = ""
		End Try
		Response.Write(token)
		Response.End()
	End Sub

    Private Sub SendEmailDisclosures()
        Dim res As New CJsonResponse()
        Try
            Dim email As String = Common.SafeStripHtmlString(Request.Params("email"))
            Dim loanType As String = Common.SafeStripHtmlString(Request.Params("loantype")).ToUpper()
            Dim availability As String = ""
            Dim bIsComboMode As Boolean = False
            Dim sUrlParaType As String = Common.SafeString(Request.QueryString("type"))
            If loanType = "XA" Then
                availability = sUrlParaType
                Select Case availability
                    Case "1"
                        availability = "1"
                    Case "2"
                        availability = "2"
                    Case "1a"
                        availability = "1a"
                    Case "2a"
                        availability = "2a"
                    Case "1b"
                        availability = "1b"
                    Case "2b"
                        availability = "2b"
                    Case Else
                        availability = "1"
                End Select
            Else ''check combo
                bIsComboMode = (loanType = "PL" Or loanType = "VL" Or loanType = "CC" Or loanType = "HE" Or loanType = "HELOC") AndAlso sUrlParaType = "1"
            End If
            Dim data As Dictionary(Of String, String) = GetDisclosures(loanType, availability, bIsComboMode)
			If SmUtil.SendEmail(email, ConfigurationManager.AppSettings("EmailDisclosureSubject"), Common.BuildEmailDisclosureContent(data, _CurrentWebsiteConfig)) Then
				res = New CJsonResponse(True, "", "OK")
			Else
				res.Message = "Unable to email the disclosures. Please try again later."
            End If
        Catch ex As Exception
            _log.Info("Could not SendEmailDisclosures.", ex)
            res.Message = "Unable to email the disclosures. Please try again later."
        End Try
        Response.Write(JsonConvert.SerializeObject(res))
        Response.End()
    End Sub

    Private Function GetDisclosures(loanType As String, availability As String, bIsComboMode As Boolean) As Dictionary(Of String, String)
        Dim disclosures As List(Of String) = Common.GetDisclosures(_CurrentWebsiteConfig, loanType, bIsComboMode, availability)
        Return Common.CompileDisclosures(disclosures, Nothing, Nothing)
    End Function
End Class
