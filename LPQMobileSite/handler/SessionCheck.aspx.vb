﻿
Partial Class Inc_SessionCheck
	Inherits System.Web.UI.Page

	Private log As log4net.ILog = log4net.LogManager.GetLogger(Me.GetType)
	Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
		Dim dTime As String = Request.QueryString("time")
		Dim sessionId = System.Web.HttpContext.Current.Session.SessionID
		log.Info("XA: Execute sessionCheck for sessionID: " & sessionId & ";  " & dTime & " times(s).")
		Response.Clear()
		Response.Write("XA: Executed sessionCheck")
		Response.End()
	End Sub
End Class
