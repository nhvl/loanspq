﻿
Partial Class handler_DocuSignTracker
    Inherits System.Web.UI.Page
	Private _log As log4net.ILog = log4net.LogManager.GetLogger(GetType(handler_DocuSignTracker))
    Protected _status As String = ""
    Protected Sub Page_PreInit(sender As Object, e As EventArgs) Handles Me.PreInit
        Dim sUrl As String = Request.QueryString("url")
        Dim sStatus As String = Request.QueryString("status")
        Dim sDocusignWithIda = Request.QueryString("DocusignWithIda")
        If Not String.IsNullOrEmpty(sDocusignWithIda) AndAlso sDocusignWithIda = "Y" AndAlso Not String.IsNullOrEmpty(sStatus) Then
            _status = "_verifyID_" & sStatus
        End If
        If String.IsNullOrWhiteSpace(sUrl) = False Then
            _log.Info(String.Format("Docusign tracker status: {0}, url: {1}", sStatus, sUrl))
            'Response.Redirect(url)  'TODO: url is used for determine what happen with docusign, dont nee to do anything for now,  define in CDOcusign

        End If
    End Sub
End Class
