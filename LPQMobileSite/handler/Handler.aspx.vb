﻿Imports System.Web
Imports LPQMobile.Utils
Imports LPQMobile.BO
Imports System.IO
Imports System.Xml
Imports System.Net
Imports System.Web.Script.Serialization
Imports System.Net.Mail
Imports Newtonsoft.Json

Partial Class handler_Handler
	Inherits System.Web.UI.Page
	Private _log As log4net.ILog = log4net.LogManager.GetLogger(GetType(handler_Handler))
#Region "Page Events"


	Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

		Dim command As String = Common.SafeString(Request.Params("command"))
		Dim slenderref = Common.SafeString(Request.Params("lenderref"))
		_log.Info("Handler for " & command & " from " & slenderref)
		Select Case command
			Case "lookupZipcode"
				lookupZipcode()
			Case "verifyAddress"
				verifyAddress()
			Case "scandriverlicense"
				scandriverlicense(False)
			Case "scanbarcode"
				scandriverlicense(True)
			Case "LookupRoutingNumber"
				LookupRoutingNumber()
			Case "decodeVinNumber"
				DecodeVinNumber()
		End Select

	End Sub

#End Region

#Region "Zipcode And Address"

	'deprecated, use verifyAddress
	Private Sub lookupZipcode()
		Dim zipStr As String = Common.SafeStripHtmlString(Request.Params("zip"))
        If zipStr.Length > 5 Then zipStr = zipStr.Substring(0, 5)
        Dim zip As Integer = Common.SafeInteger(zipStr)
        If zipStr.Length < 5 OrElse zip < 1 Then
            Response.Write(zip & "|" & "|")  'invalid zip if the lengh isn't exactly 5
            Return
        End If
        Dim oZipCode As New CZipCode()
        Dim oZipCodeReturn As CZipCode = oZipCode.LookupByZip(zip)
		If oZipCodeReturn IsNot Nothing Then
			Response.Write(oZipCodeReturn.ZipCode & "|" & oZipCodeReturn.City & "|" & oZipCodeReturn.State)
		Else
			Response.Write(zip & "|" & "|")
		End If

	End Sub

	Private Sub verifyAddress()
		Try
			Dim address As String = Common.SafeString(Request.Params("address"))
			Dim url As String = "https://ams.loanspq.com/services/VerifyAddress.aspx?" + address + "&isDebug=1"
			Dim responseText As String = Common.webGet(url)

			Response.Write(responseText)
		Catch ex As Exception
			CPBLogger.logError("Error while verifyAddress", ex)
		End Try
	End Sub
	Private Sub scandriverlicense(ByVal pbIsBarcode As Boolean)
		Try

			Dim base64data As String = Common.SafeString(Request.Params("sBase64data"))
			Dim oImage As Byte() = Nothing
			'If base64data <> "" Then
			'	oImage = System.Convert.FromBase64String(base64data)
			'	If oImage IsNot Nothing Then
			'		_log.info("image size: " & oImage.Length)
			'		Dim img = New System.Drawing.Bitmap(New MemoryStream(oImage))
			'		img.Save(Server.MapPath("processed_image.jpg"))
			'	End If
			'End If
			Dim lenderId As String = Common.SafeString(Request.Params("lender_id"))
			Dim lenderRef As String = Common.SafeString(Request.Params("lenderref"))
			Dim stateCode As String = Common.SafeString(Request.Params("sStateCode"))
			Dim docKey As String = Common.SafeString(Request.Params("sKey"))
			Dim scandocRequest As String = ""
			scandocRequest = String.Format("<?xml version='1.0' encoding='UTF-8'?><INPUT><REQUEST front_base64Image='{0}' lender_ref ='{1}' lender_id='{2}' mpao_key='{3}' state_code='{4}'/></INPUT>" _
				 , base64data, lenderRef, lenderId, docKey, stateCode)
            Dim responseText As String = getScanDocument(scandocRequest, pbIsBarcode)
            '<OUTPUT version="1.0"><RESPONSE transaction_id="b6ce78c8-00e9-4b27-be18-a7c3ed4d5c2f" note="Testing barcode scan from imageclear"><FIELDS LName="Sample" FName="Holly" MName="" DOB="01/02/1996" Sex="F" Street="23 Hazen Dr None" City="Concord" State="NH" Zip="03305-0000" Country="USA" DriverLicense="NHI14831209" DriverLicenseIssueDate="01/11/2017" DriverLicenseExpirationDate="01/02/2022" DriverLicenseSate="NH" /></RESPONSE></OUTPUT>
            Dim errorMessage As String = ""
            If String.IsNullOrEmpty(responseText) Then
                errorMessage = "<div id='errorScanDocument'>Unable to scan document data.</div>"
                SaveImage2File(base64data, lenderRef, stateCode)
                Response.Write(errorMessage)
            End If

            Dim xmlDoc As New XmlDocument
			xmlDoc.LoadXml(responseText)
			Dim oDocs As New scandocumentinfo
			If (xmlDoc.GetElementsByTagName("ERROR").Count > 0) Then
				errorMessage += "<div id='errorScanDocument'>" & xmlDoc.InnerText & "</div>"
				SaveImage2File(base64data, lenderRef, stateCode)
				Response.Write(errorMessage)
			ElseIf Not (xmlDoc.GetElementsByTagName("RESPONSE").Count > 0) Then
				errorMessage += "<div id='errorScanDocument'>" & xmlDoc.InnerText & "</div>"
				SaveImage2File(base64data, lenderRef, stateCode)
				Response.Write(errorMessage)
			Else
				For Each item As XmlElement In xmlDoc.SelectNodes("OUTPUT/RESPONSE/FIELDS")
					oDocs.dlNumber = Common.SafeString(item.GetAttribute("DriverLicense"))
					oDocs.dlState = Common.SafeString(item.GetAttribute("DriverLicenseSate"))
					oDocs.dlIssueDate = Common.SafeString(item.GetAttribute("DriverLicenseIssueDate"))
					oDocs.dlExpirationDate = Common.SafeString(item.GetAttribute("DriverLicenseExpirationDate"))
					oDocs.dateOfBirth = Common.SafeString(item.GetAttribute("DOB"))
					oDocs.gender = Common.SafeString(item.GetAttribute("Sex"))
					oDocs.lastName = Common.SafeString(item.GetAttribute("LName"))
					oDocs.firstName = Common.SafeString(item.GetAttribute("FName"))
					oDocs.middleName = Common.SafeString(item.GetAttribute("MName"))
					If Common.SafeString(item.GetAttribute("Street")) <> "" Then
						oDocs.streetAddress = Common.SafeString(item.GetAttribute("Street")) 'from clear image
					Else
						oDocs.streetAddress = Common.SafeString(item.GetAttribute("StreetNumber")) + " " + Common.SafeString(item.GetAttribute("StreetName")) 'from MItek
					End If
					' Jira AP-399
					'due to bug in upstream lib or service, the keyword "None" is appeneded to the address field.  This code will remove it.
					'<OUTPUT version="1.0"><RESPONSE transaction_id="b6ce78c8-00e9-4b27-be18-a7c3ed4d5c2f" note="Testing barcode scan from imageclear"><FIELDS LName="Sample" FName="Holly" MName="" DOB="01/02/1996" Sex="F" Street="23 Hazen Dr None" City="Concord" State="NH" Zip="03305-0000" Country="USA" DriverLicense="NHI14831209" DriverLicenseIssueDate="01/11/2017" DriverLicenseExpirationDate="01/02/2022" DriverLicenseSate="NH" /></RESPONSE></OUTPUT>
					If oDocs.streetAddress.EndsWith(" None") Then
                        oDocs.streetAddress = oDocs.streetAddress.Replace(" None", "")
                    End If

                    oDocs.city = Common.SafeString(item.GetAttribute("City"))
					oDocs.state = Common.SafeString(item.GetAttribute("State"))

					' Jira AP-693
					' Sometimes, for Hawaii (HI) drivers licenses, the last character on Field 5 (Document Discriminator, or DD) seems to get copied into
					' the end of the street address. ExtractID.aspx doesn't send this information back, unfortunately. But addresses, especially in Hawaii,
					' don't typically end with a space followed by a single letter.
					' For Hawaii addresses, if it ends with a single space then a single letter, remove it.
					If oDocs.state = "HI" Then
						If Regex.IsMatch(oDocs.streetAddress, " [a-zA-Z]$") Then
							oDocs.streetAddress = oDocs.streetAddress.Substring(0, oDocs.streetAddress.Length - 2)
						End If
					End If

					If Common.SafeString(item.GetAttribute("Zip")).Length > 4 Then
						oDocs.zip = Common.SafeString(item.GetAttribute("Zip")).Substring(0, 5)
					End If
					'handle FName attribute contains both first name and middle name 
					Dim sMiddleName = oDocs.middleName
					Dim sFirstName = oDocs.firstName
					If sMiddleName = "" Then
						Dim lastIndexOfWhiteSpace = sFirstName.Trim.LastIndexOf(" ")
						If lastIndexOfWhiteSpace > -1 Then
							oDocs.firstName = sFirstName.Substring(0, lastIndexOfWhiteSpace)
							oDocs.middleName = sFirstName.Substring(lastIndexOfWhiteSpace + 1)
						End If
					End If
				Next

                Dim serializer As New JavaScriptSerializer()
				Response.Write(serializer.Serialize(oDocs))

			End If

		Catch ex As Exception
			CPBLogger.logError("driverlicense decode error", ex)
		End Try
	End Sub

	Private Sub SaveImage2File(ByVal sBase64Img As String, ByVal sLenderRef As String, ByVal sStateCode As String)
        'Dim oImage As Byte() = Nothing
        'If sBase64Img <> "" Then
        '	oImage = System.Convert.FromBase64String(sBase64Img)
        '	If oImage IsNot Nothing Then
        '		_log.Info("image size: " & oImage.Length)
        '		Dim img = New System.Drawing.Bitmap(New MemoryStream(oImage))
        '		Dim sDirFileName = "temp/" + sLenderRef + "_" + sStateCode + "_failed_image_" + Right(System.Guid.NewGuid.ToString, 16) + ".jpg"
        '		img.Save(Server.MapPath(sDirFileName))
        '	End If
        'End If
    End Sub
	Private Function getScanDocument(ByVal scandocRequest As String, ByVal psIsBarcode As Boolean) As String
		Dim url As String = "https://apptest.loanspq.com/services/mitek/ExtractID.aspx"
		If psIsBarcode Then
			url = "https://lenderiq.loanspq.com/Services/ExtractID.aspx"
		End If
		Dim req As WebRequest = WebRequest.Create(url)
		req.Method = "POST"
		req.ContentType = "text/xml"
		Dim sXml As String
		Using requestStream = req.GetRequestStream()
			Dim writer As New StreamWriter(requestStream)
			writer.Write(scandocRequest)
			writer.Close()
			Using res As WebResponse = req.GetResponse()
				Using stream As Stream = res.GetResponseStream()
					Dim reader As New StreamReader(stream)
					sXml = reader.ReadToEnd()
					_log.Info("Scan Documents: Response from server: " + CSecureStringFormatter.MaskSensitiveXMLData(sXml))
				End Using
				res.Close()
			End Using
			requestStream.Close()
		End Using
		Return sXml
	End Function


#End Region

	

	Private Sub LookupRoutingNumber()
		Dim res As New CJsonResponse()
		Try
			Dim routingNum As String = Request.Params("routing_num")
			Dim result = CRoutingInformation.Instance.GetData(routingNum)
			If result IsNot Nothing Then
				res = New CJsonResponse(True, "", result)
			Else
				res = New CJsonResponse(False, "You've entered an unrecognized routing #, please verify to make sure you've entered the correct value")
			End If
		Catch ex As Exception
			_log.Error("Could not LookupRoutingNumber.", ex)
			res.Message = "Unable to lookup routing number. Please try again later."
		End Try
		Response.Write(JsonConvert.SerializeObject(res))
		Response.End()
	End Sub
	Private Sub DecodeVinNumber()
		Dim res As New CJsonResponse(False, "Unable to decode vin number. Please try again", "FAILED")
		Try

			Dim vinNumber As String = Common.SafeStripHtmlString(Request.Params("vinNumber"))
			Dim lenderRef As String = Common.SafeString(Request.Params("lenderRef"))
			Dim decodeInput As String = String.Format("<?xml version=""1.0"" standalone=""yes""?><INPUT><REQUEST vin_key=""{0}"" lender_ref =""{1}"" vin=""{2}""/></INPUT>", "testkey", lenderRef, vinNumber)
			Dim req As WebRequest = WebRequest.Create("https://apptest.loanspq.com/services/NADA/VINDecoder.aspx")
			req.Method = "POST"
			req.ContentType = "text/xml"
			Dim responseText As String
			Using requestStream = req.GetRequestStream()
				Dim writer As New StreamWriter(requestStream)
				writer.Write(decodeInput)
				writer.Close()
				Using resp As WebResponse = req.GetResponse()
					Using stream As Stream = resp.GetResponseStream()
						Dim reader As New StreamReader(stream)
						responseText = reader.ReadToEnd()
						_log.Info("Vin Number Decode: Response from server: " + CSecureStringFormatter.MaskSensitiveXMLData(responseText))
					End Using
					resp.Close()
				End Using
				requestStream.Close()
			End Using
			If Not String.IsNullOrEmpty(responseText) AndAlso Not responseText.Contains("ERROR") Then
				Dim doc As New XmlDocument
				doc.LoadXml(responseText)
				Dim responseNode As XmlNode = doc.SelectSingleNode("/OUTPUT/VIN")
				If responseNode IsNot Nothing Then
					Dim item As New VinDecoderItem()
					item.Vin = responseNode.SelectSingleNode("@vin").Value
					item.Make = responseNode.SelectSingleNode("@make").Value
					item.Model = responseNode.SelectSingleNode("@model").Value
					item.Year = responseNode.SelectSingleNode("@year").Value
					item.Trim = responseNode.SelectSingleNode("@trim").Value
					res = New CJsonResponse(True, "", item)
				End If
			End If
		Catch ex As Exception
			_log.Error("vin number decode error", ex)
		End Try
		Response.Write(JsonConvert.SerializeObject(res))
		Response.End()
	End Sub
	Class VinDecoderItem
		Public Property Vin As String
		Public Property Make As String
		Public Property Model As String
		Public Property Year As String
		Public Property Trim As String
	End Class
	Class scandocumentinfo
		Public dlNumber As String
		Public dlState As String
		Public dateOfBirth As String
		Public streetAddress As String
		Public city As String
		Public zip As String
		Public firstName As String
		Public lastName As String
		Public middleName As String
		Public gender As String
		Public dlIssueDate As String
		Public dlExpirationDate As String
		Public state As String
	End Class
End Class
