﻿Imports System.Threading.Tasks
Imports System.Web.Security.AntiXss
Imports LPQMobile.BO
Imports DBUtils
Imports InstaTouch
Imports Newtonsoft.Json
Imports LPQMobile.Utils

Partial Class handler_InstaHandler
    Inherits System.Web.UI.Page
    Private _log As log4net.ILog = log4net.LogManager.GetLogger(GetType(handler_InstaHandler))
    ''Perform caching same as morgate loan (revision:5906) from James
    ''Static members For making the access token load faster.
    ''- The access token may be retrieved from /Account/Landing before this page, so that
    ''  this page loads faster.
    ''- The access token may be cached (indepedent of lender since it uses AP-level credentials)
    ''  For the entire website up To the time it expires.
    ''- The token will be expired 5 minutes before it actually expires, to be safe.
    ''- Accessing the token will be done via locks to prevent race conditions.
    Private Shared s_retrieveAccessTokenSemaphore As System.Threading.SemaphoreSlim = New System.Threading.SemaphoreSlim(1, 1)
    Private Shared s_lastRetrievedAccessToken As String = Nothing
    Private Shared s_lastRetrievedAccessTokenTime As DateTimeOffset = DateTimeOffset.Now
    Private Shared s_lastRetrievedAccessTokenExpires As TimeSpan = TimeSpan.Zero
    Private Const X_EFX_SESSIONID As String = "x-efx-sessionId"
	Private Const TRANSACTION_KEY As String = "InstaTouch_Transaction_Key"
	Private Const HAS_GIVEN_CONSENT As String = "HAS_GIVEN_CONSENT"
	Private Const CARRIER_URL As String = "InstaTouch_Carrier_Url"
    Public Const IDENTITY_DATA As String = "InstaTouch_Identity_Data"
#Region "Page Events"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim command As String = Common.SafeString(Request.Params("command"))
        Select Case command
            Case "carrierIdentification"
                CarrierIdentification()
            Case "processWifiConsent"
                ProcessWifiConsent()
            Case "validateOtp"
                ValidateOtp()
            Case "authenticate"
                Authenticate()
            Case "updateMdn"
                UpdateMdn()
            Case "checkRestriction"
                CheckRestriction()
        End Select
    End Sub

	Private Sub UpdateMdn()
		Dim resp As CJsonResponse
		Try
			'For testing
			'If ConfigurationManager.AppSettings.Get("ENVIRONMENT") = "DEV" Then
			'    resp = New CJsonResponse(True, "", "")
			'    Response.Clear()
			'    Response.Write(JsonConvert.SerializeObject(resp))
			'    Return
			'End If
			Dim accessToken = GetCachedAuthorizationAccessToken()
			Dim sessionId As String = ""
			If Session(X_EFX_SESSIONID) IsNot Nothing Then
				sessionId = Session(X_EFX_SESSIONID).ToString()
			End If
			Dim sMerchantId As String = Common.SafeString(Request.Params("InstaTouchMerchantId"))
			If String.IsNullOrWhiteSpace(accessToken) OrElse String.IsNullOrWhiteSpace(sessionId) Then
				Throw New Exception("Error at Call 4 - UpdateMDN: X_EFX_ACCESS_TOKEN and X_EFX_SESSIONID are required.")
			End If
			'Call 4 - Update MDN
			Dim updateMdnRequest As New UpdateMDNRequest(sMerchantId, accessToken, sessionId)
			Dim updateMdnAPI As New UpdateMDNAPI(updateMdnRequest)
			Dim updateMdnResponse = updateMdnAPI.Request(Of UpdateMDNResponse)()
			If updateMdnResponse Is Nothing OrElse Not updateMdnResponse.status.Equals("success", StringComparison.OrdinalIgnoreCase) Then
				resp = New CJsonResponse(False, "", "")
			Else
				resp = New CJsonResponse(True, "", "")
			End If

		Catch ex As Exception
			_log.Error(ex.Message, ex)
			resp = New CJsonResponse(False, "", "")
		End Try
		Response.Clear()
		Response.Write(JsonConvert.SerializeObject(resp))
	End Sub


	Private Sub Authenticate()
		Dim resp As CJsonResponse
		Dim zipCode As String = ""
		Dim ssn As String = ""
		Try
			zipCode = Common.SafeString(Request.Params("zipCode"))
			ssn = Common.SafeString(Request.Params("ssn"))

			Dim lenderId As Guid = Common.SafeGUID(Request.Params("lenderId"))
			Dim lenderRef As String = Common.SafeString(Request.Params("lenderRef"))
			Dim lenderName As String = Common.SafeString(Request.Params("lenderName"))
			Dim loanType As String = Common.SafeString(Request.Params("loanType"))
			Dim sMerchantId As String = Common.SafeString(Request.Params("InstaTouchMerchantId"))
			If String.IsNullOrWhiteSpace(zipCode) OrElse Not Regex.IsMatch(zipCode, "^([0-9]{5})[-\s]?([0-9]{4})?$") Then
				Throw New ArgumentException("Invalid Parameter: ZipCode")
			End If
			If String.IsNullOrWhiteSpace(ssn) OrElse Not Regex.IsMatch(ssn, "^[0-9]{4}$") Then
				Throw New ArgumentException("Invalid Parameter: SSN")
			End If
			'For testing
			'If ConfigurationManager.AppSettings.Get("ENVIRONMENT") = "DEV" Then
			'    Dim fakeResponse As New IdentityResponse
			'    fakeResponse.identity = New Identity() With {.name = New Name() With {.firstName = "Jane", .lastName = "Sherwood"}, .contact = New Contact() With {.email = "janesherwood@test.ccc", .homePhone = "2451214574", .mobile = "3621452144"}}
			'    Dim addresses As New List(Of Address)
			'    addresses.Add(New Address() With {.city = "Cupertino", .state = "CA", .zipCode = "95014", .streetAddress = "1 infinite loop"})
			'    fakeResponse.identity.addresses = addresses
			'    fakeResponse.identification = New Identification() With {.ssn = "666066134", .dob = "08/06/1967"}
			'    'TODO: write log
			'    Utils.WriteEquifaxInstaTouchLog(New EquifaxInstaTouchRecord() With {.RecordType = RecordType.Prefill, .DateRequested = DateTime.Now, .DateResponded = DateTime.Now, .ConsumerFirstName = "Jane", .ConsumerLastName = "Sherwood", .TransactionID = Guid.NewGuid(), .SessionID = Guid.NewGuid().ToString(), .LenderName = "", .LenderID = lenderId, .LenderRef = lenderRef, .LoanType = loanType, .IPAddress = Request.UserHostAddress})
			'    Utils.WriteInstaTouchRequestTracking(lenderId, lenderRef, Request.UserHostAddress)


			'    resp = New CJsonResponse(True, "", fakeResponse)
			'    Response.Clear()
			'    Response.Write(JsonConvert.SerializeObject(resp))
			'    Return
			'End If


			Dim accessToken = GetCachedAuthorizationAccessToken()
			Dim sessionId As String = ""
			If Session(X_EFX_SESSIONID) IsNot Nothing Then
				sessionId = Session(X_EFX_SESSIONID).ToString()
			End If

			Dim transactionKey As String = ""
			If Session(TRANSACTION_KEY) IsNot Nothing Then
				transactionKey = Session(TRANSACTION_KEY).ToString()
			End If
			Dim hasGivenConsent As Boolean = False
			If Session(HAS_GIVEN_CONSENT) IsNot Nothing Then
				hasGivenConsent = Session(HAS_GIVEN_CONSENT).ToString() = "Y"
			End If

			If String.IsNullOrWhiteSpace(accessToken) OrElse
				String.IsNullOrWhiteSpace(sessionId) OrElse
				(hasGivenConsent AndAlso String.IsNullOrWhiteSpace(transactionKey)) Then
				Throw New Exception("Error at Call 7 - Consent: X_EFX_ACCESS_TOKEN and X_EFX_SESSIONID AND TRANSACTION_KEY are required.")
			End If

			If Not hasGivenConsent Then
				Dim consentRequestTransactionDate = DateTimeOffset.Now
				'Call 5 Consent        
				Dim consentRequest As New ConsentRequest(sMerchantId, accessToken, sessionId)
				Dim consentAPI As New ConsentAPI(consentRequest)
				Dim consentResponse = consentAPI.Request(Of ConsentResponse)()
				If consentResponse Is Nothing Then
					Throw New Exception("InstaTouch API Error at Call 5 - Consent: Consent response was malformed")
				End If
				Dim consentResponseTransactionDate = DateTimeOffset.Now
				'TODO: write EquifaxLog
				Utils.WriteEquifaxInstaTouchLog(New EquifaxInstaTouchRecord() With {.RecordType = RecordType.OptIn, .DateRequested = consentRequestTransactionDate, .ConsumerFirstName = "", .ConsumerLastName = "", .DateResponded = consentResponseTransactionDate, .TransactionID = Guid.Parse(consentResponse.transactionID), .SessionID = sessionId, .MerchantId = sMerchantId, .LenderID = lenderId, .LenderRef = lenderRef, .LoanType = loanType, .IPAddress = Request.UserHostAddress})
			End If

			'Call 7 Identity         
			Dim identityRequestTransactionDate = DateTimeOffset.Now
			Dim identiyRequest As New IdentityRequest(sMerchantId, accessToken, sessionId, zipCode, ssn)
			Dim identityAPI As New IdentityAPI(identiyRequest)
			Dim identityResponse = identityAPI.Request(Of IdentityResponse)()
			If identityResponse Is Nothing Then
				Throw New Exception("InstaTouch API Error at Call 7 - Identity: Identity response was malformed")
			End If

			Dim identityResponseTransactionDate = DateTimeOffset.Now
			Dim firstName As String = ""
			Dim lastName As String = ""
			If identityResponse IsNot Nothing AndAlso identityResponse.identity IsNot Nothing Then
				If identityResponse.identity.contact IsNot Nothing Then
					' Remove the leading country code
					Dim homePhone = identityResponse.identity.contact.homePhone
					If Not String.IsNullOrWhiteSpace(homePhone) AndAlso homePhone.StartsWith("1") AndAlso homePhone.Length = 11 Then
						identityResponse.identity.contact.homePhone = homePhone.Substring(1)
					End If
					Dim mobilePhone = identityResponse.identity.contact.mobile
					If Not String.IsNullOrWhiteSpace(mobilePhone) AndAlso mobilePhone.StartsWith("1") AndAlso mobilePhone.Length = 11 Then
						identityResponse.identity.contact.mobile = mobilePhone.Substring(1)
					End If
					If Not String.IsNullOrWhiteSpace(identityResponse.identity.contact.email) Then
						identityResponse.identity.contact.email = identityResponse.identity.contact.email.ToLower()
					End If
				End If
				If identityResponse.identity.name IsNot Nothing Then
					'Transform needed values from UPPERCASE to Title Case.
					firstName = identityResponse.identity.name.firstName
					If Not String.IsNullOrWhiteSpace(firstName) Then
						identityResponse.identity.name.firstName = StrConv(firstName, VbStrConv.ProperCase)
					End If
					lastName = identityResponse.identity.name.lastName
					If Not String.IsNullOrWhiteSpace(lastName) Then
						identityResponse.identity.name.lastName = StrConv(lastName, VbStrConv.ProperCase)
					End If
				End If
				resp = New CJsonResponse(True, "", identityResponse)
			Else
				resp = New CJsonResponse(True, "", "")
			End If
			Utils.WriteEquifaxInstaTouchLog(New EquifaxInstaTouchRecord() With {.RecordType = RecordType.Prefill, .DateRequested = identityRequestTransactionDate, .DateResponded = identityResponseTransactionDate, .ConsumerFirstName = firstName, .ConsumerLastName = lastName, .TransactionID = Guid.Parse(identityResponse.transactionID), .SessionID = sessionId, .MerchantId = sMerchantId, .LenderID = lenderId, .LenderRef = lenderRef, .LoanType = loanType, .IPAddress = Request.UserHostAddress})
			EmailRestriction()
			Utils.WriteInstaTouchRequestTracking(lenderId, lenderRef, Request.UserHostAddress)
		Catch ex As Exception
			_log.Error(ex.Message, ex)
			resp = New CJsonResponse(False, "", New With {.zipCode = zipCode, .ssn = ssn})
		End Try
		Response.Clear()
		Response.Write(JsonConvert.SerializeObject(resp))
	End Sub
	Private Sub ValidateOtp()
		Dim resp As CJsonResponse
		Try
			Dim phoneNumber As String = Common.SafeString(Request.Params("phone"))
			Dim otp As String = Common.SafeString(Request.Params("otp"))
			Dim sMerchantId As String = Common.SafeString(Request.Params("InstaTouchMerchantId"))
			phoneNumber = Regex.Replace(phoneNumber, "[^0-9]", "")
			If String.IsNullOrWhiteSpace(otp) Then
				Throw New ArgumentException("Invalid Parameter: OTP")
			End If
			If String.IsNullOrWhiteSpace(phoneNumber) Then
				Throw New ArgumentException("Invalid Parameter: Phone Number")
			End If

			'For testing
			'If ConfigurationManager.AppSettings.Get("ENVIRONMENT") = "DEV" Then
			'    resp = New CJsonResponse(True, "", "")
			'    Response.Clear()
			'    Response.Write(JsonConvert.SerializeObject(resp))
			'    Return
			'End If

			Dim accessToken = GetCachedAuthorizationAccessToken()
			Dim sessionId As String = ""
			If Session(X_EFX_SESSIONID) IsNot Nothing Then
				sessionId = Session(X_EFX_SESSIONID).ToString()
			End If

			Dim transactionKey As String = ""
			If Session(TRANSACTION_KEY) IsNot Nothing Then
				transactionKey = Session(TRANSACTION_KEY).ToString()
			End If
			If String.IsNullOrWhiteSpace(accessToken) OrElse String.IsNullOrWhiteSpace(sessionId) OrElse String.IsNullOrWhiteSpace(transactionKey) Then
				Throw New Exception("Error at Call 6b - Consent: X_EFX_ACCESS_TOKEN and X_EFX_SESSIONID AND TRANSACTION_KEY are required.")
			End If
			'Call 6b Validate OTP          
			Dim validateOtpRequest As New ValidateOTPRequest(sMerchantId, accessToken, sessionId, phoneNumber, otp, transactionKey)
			Dim validateOtpAPI = New ValidateOTPAPI(validateOtpRequest)
			Dim validationOtpResponse = validateOtpAPI.Request(Of OTPResponse)()
			If validationOtpResponse Is Nothing OrElse validationOtpResponse.otpLifecycle Is Nothing OrElse Not validationOtpResponse.otpLifecycle.status.Equals("SUCCESS", StringComparison.OrdinalIgnoreCase) OrElse String.IsNullOrWhiteSpace(validationOtpResponse.otpLifecycle.transactionKey) Then
				Throw New Exception("InstaTouch API Error at Call 6b - Validate OTP: " + otp + ", OTP response wa not successful, or transaction key is blank.")
			End If

			Session(TRANSACTION_KEY) = validationOtpResponse.otpLifecycle.transactionKey
			resp = New CJsonResponse(True, "", "")
		Catch ex As Exception
			_log.Error(ex.Message, ex)
			resp = New CJsonResponse(False, "", "")
		End Try
		Response.Clear()
		Response.Write(JsonConvert.SerializeObject(resp))
	End Sub

	Private Sub ProcessWifiConsent()
		Dim resp As CJsonResponse
		Try

			Dim phoneNumber As String = Common.SafeString(Request.Params("phone"))
			phoneNumber = Regex.Replace(phoneNumber, "[^0-9]", "")
			If String.IsNullOrWhiteSpace(phoneNumber) Then
				Throw New ArgumentException("Invalid Parameter: Phone Number")
			End If
			Dim lenderName As String = Common.SafeString(Request.Params("lenderName"))
			Dim lenderId As Guid = Common.SafeGUID(Request.Params("lenderId"))
			Dim lenderRef As String = Common.SafeString(Request.Params("lenderRef"))
			Dim loanType As String = Common.SafeString(Request.Params("loanType"))
			Dim sMerchantId As String = Common.SafeString(Request.Params("InstaTouchMerchantId"))
			''For testing
			'If ConfigurationManager.AppSettings.Get("ENVIRONMENT") = "DEV" Then

			'    'TODO: write EquifaxLog
			'    Utils.WriteEquifaxInstaTouchLog(New EquifaxInstaTouchRecord() With {.RecordType = RecordType.OptIn, .DateRequested = DateTime.Now, .ConsumerFirstName = "", .ConsumerLastName = "", .DateResponded = DateTime.Now, .TransactionID = Guid.NewGuid(), .SessionID = Guid.NewGuid().ToString(), .LenderName = lenderName, .LenderID = lenderId, .LenderRef = lenderRef, .LoanType = loanType, .IPAddress = Request.UserHostAddress})


			'    resp = New CJsonResponse(True, "", New With {.phoneNumber = phoneNumber})
			'    Response.Clear()
			'    Response.Write(JsonConvert.SerializeObject(resp))
			'    Return
			'End If


			Dim accessToken = GetCachedAuthorizationAccessToken()
			Dim sessionId As String = ""
			If Session(X_EFX_SESSIONID) IsNot Nothing Then
				sessionId = Session(X_EFX_SESSIONID).ToString()
			End If
			If String.IsNullOrWhiteSpace(accessToken) OrElse String.IsNullOrWhiteSpace(sessionId) Then
				Throw New Exception("Error at Call 5 - Consent: X_EFX_ACCESS_TOKEN and X_EFX_SESSIONID are required.")
			End If
			Dim consentRequestTransactionDate = DateTimeOffset.Now
			'Call 5 Consent        
			Dim consentRequest As New ConsentRequest(sMerchantId, accessToken, sessionId)
			Dim consentAPI As New ConsentAPI(consentRequest)
			Dim consentResponse = consentAPI.Request(Of ConsentResponse)()
			If consentResponse Is Nothing Then
				Throw New Exception("InstaTouch API Error at Call 5 - Consent: Consent response was malformed")
			End If
			Dim consentResponseTransactionDate = DateTimeOffset.Now
			'TODO: write EquifaxLog
			Utils.WriteEquifaxInstaTouchLog(New EquifaxInstaTouchRecord() With {.RecordType = RecordType.OptIn, .DateRequested = consentRequestTransactionDate, .ConsumerFirstName = "", .ConsumerLastName = "", .DateResponded = consentResponseTransactionDate, .TransactionID = Guid.Parse(consentResponse.transactionID), .SessionID = sessionId, .MerchantId = sMerchantId, .LenderID = lenderId, .LenderRef = lenderRef, .LoanType = loanType, .IPAddress = Request.UserHostAddress})
			'Call 6a Send OTP    
			Dim sentOtpRequest As New SendOTPRequest(sMerchantId, accessToken, sessionId, phoneNumber)
			Dim sentOtpAPI As New SendOTPAPI(sentOtpRequest)
			Dim sentOtpResponse = sentOtpAPI.Request(Of OTPResponse)()
			If sentOtpResponse Is Nothing OrElse sentOtpResponse.otpLifecycle Is Nothing OrElse Not sentOtpResponse.otpLifecycle.status.Equals("SUCCESS", StringComparison.OrdinalIgnoreCase) OrElse String.IsNullOrWhiteSpace(sentOtpResponse.otpLifecycle.transactionKey) Then
				Throw New Exception("InstaTouch API Error at Call 6a - Send One-Time Passcode (OTP): OTP lifeccle response was not successful, or transaction key is blank.")
			End If
			Session(TRANSACTION_KEY) = sentOtpResponse.otpLifecycle.transactionKey
			Session(HAS_GIVEN_CONSENT) = "Y"
			resp = New CJsonResponse(True, "", New With {.phoneNumber = phoneNumber})
		Catch ex As Exception
			_log.Error(ex.Message, ex)
			resp = New CJsonResponse(False, "", "")
		End Try
		Response.Clear()
		Response.Write(JsonConvert.SerializeObject(resp))
	End Sub
	Private Sub CarrierIdentification()
		Dim resp As CJsonResponse
		Try
			''For testing
			'If ConfigurationManager.AppSettings.Get("ENVIRONMENT") = "DEV" Then
			'    resp = New CJsonResponse(True, "", New With {.mobile = True})
			'    Response.Clear()
			'    Response.Write(JsonConvert.SerializeObject(resp))
			'    Return
			'End If

			' Call 1 - Authorization
			Dim accessToken = GetCachedAuthorizationAccessToken()
			'Call 2 - Handshake
			Dim sMerchantId As String = Common.SafeString(Request.Params("InstaTouchMerchantId"))
			Dim handshakeRequest As New HandshakeRequest(sMerchantId, accessToken)
			Dim handshakeAPI As New HandshakeAPI(handshakeRequest)
			Dim handshakeResponse = handshakeAPI.Request(Of HandshakeResponse)()
			If handshakeResponse Is Nothing OrElse String.IsNullOrWhiteSpace(handshakeResponse.sessionID) Then
				Throw New Exception("InstaTouch API Error at Call 2 - Handshake: Handshake response is malformed, or session id is blank.")
			End If
			'store keys         
			Session(X_EFX_SESSIONID) = handshakeResponse.sessionID
			'wifi path
			If String.IsNullOrWhiteSpace(handshakeResponse.carrier) OrElse String.IsNullOrWhiteSpace(handshakeResponse.carrier) Then
				resp = New CJsonResponse(True, "", New With {.mobile = False})
			Else
				'Session(CARRIER_URL) = handshakeResponse.instaTouch
				'mobile path
				resp = New CJsonResponse(True, "", New With {.mobile = True, .instaUrl = handshakeResponse.carrier})
			End If
		Catch ex As Exception
			_log.Error(ex.Message, ex)
			resp = New CJsonResponse(False, "", "")
		End Try
		Response.Clear()
		Response.Write(JsonConvert.SerializeObject(resp))
	End Sub

	Private Sub GetRestriction(
                              ByRef lenderID As Guid,
                              ByRef instaTouchRestrictionInternalEmails As List(Of String),
                              ByRef instaTouchRestrictionMaxTimesBeforeBlocking As Integer,
                              ByRef instaTouchRestrictionMaxTimesBeforeNotification As Integer,
                              ByRef instaTouchRestrictionExemptIPs As List(Of CIPCommentsItem))

        lenderID = Common.SafeGUID(AntiXssEncoder.HtmlEncode(Request.Params("lenderid"), True))
        Dim lenderRef As String = AntiXssEncoder.HtmlEncode(Request.Params("lenderref"), True)

        If lenderID = Guid.Empty Then
            If Not String.IsNullOrEmpty(lenderRef) Then
                Dim oLender As CLenderConfig = Common.GetLenderConfigByLenderRef(lenderRef)
                If oLender IsNot Nothing Then
                    lenderID = oLender.LenderId
                End If
            End If
        End If
        instaTouchRestrictionInternalEmails = New List(Of String)
        instaTouchRestrictionMaxTimesBeforeBlocking = -1
        instaTouchRestrictionMaxTimesBeforeNotification = -1
        instaTouchRestrictionExemptIPs = New List(Of CIPCommentsItem)
        If lenderID <> Guid.Empty Then
            Dim globalConfigsDic As Dictionary(Of String, String) = Common.ReadGlobalConfigItems(New List(Of String) From {GlobalConfigDictionary.GENERAL_INSTATOUCH}, lenderID)
            If Not String.IsNullOrEmpty(globalConfigsDic(GlobalConfigDictionary.GENERAL_INSTATOUCH)) Then
                Dim obj = New With {
                    .InstaTouchInternalEmailAddresses = "",
                    .InstaTouchRestrictionIPs = New List(Of CIPCommentsItem),
                    .InstaTouchMaxTimesBeforeNotification = -1,
                    .InstaTouchMaxTimesBeforeBlocking = -1
                } ' by default
                obj = JsonConvert.DeserializeAnonymousType(globalConfigsDic(GlobalConfigDictionary.GENERAL_INSTATOUCH), obj)
                If obj IsNot Nothing Then
                    instaTouchRestrictionInternalEmails = obj.InstaTouchInternalEmailAddresses.Split(";"c).Where(Function(p) Not String.IsNullOrWhiteSpace(p)).ToList()
                    instaTouchRestrictionMaxTimesBeforeBlocking = obj.InstaTouchMaxTimesBeforeBlocking
                    instaTouchRestrictionMaxTimesBeforeNotification = obj.InstaTouchMaxTimesBeforeNotification
                    instaTouchRestrictionExemptIPs = obj.InstaTouchRestrictionIPs
                End If
            End If
        End If

    End Sub

    Private Sub CheckRestriction()
        Dim resp As CJsonResponse

        Try
            Dim lenderID As Guid
            Dim instaTouchRestrictionInternalEmails = New List(Of String)
            Dim instaTouchRestrictionMaxTimesBeforeBlocking = -1
            Dim instaTouchRestrictionMaxTimesBeforeNotification = -1
            Dim instaTouchRestrictionExemptIPs = New List(Of CIPCommentsItem)
            GetRestriction(lenderID, instaTouchRestrictionInternalEmails, instaTouchRestrictionMaxTimesBeforeBlocking, instaTouchRestrictionMaxTimesBeforeNotification, instaTouchRestrictionExemptIPs)

            If instaTouchRestrictionExemptIPs IsNot Nothing AndAlso instaTouchRestrictionExemptIPs.Any(Function(p)
                                                                                                           If p.IP = Request.UserHostAddress Then Return True
                                                                                                           If p.IP.EndsWith("*") Then '123.23.12.*
                                                                                                               Return Request.UserHostAddress.StartsWith(p.IP.Substring(0, p.IP.LastIndexOf("."c) + 1))
                                                                                                           End If
                                                                                                           Return False
                                                                                                       End Function) Then
                resp = New CJsonResponse(True, "", "")
                Response.Clear()
                Response.Write(JsonConvert.SerializeObject(resp))
                Return
            Else
                resp = New CJsonResponse(True, "", "")
                Dim counter As Integer = Utils.CountInstaTouchRequestTracking(lenderID, "", Request.UserHostAddress) + 1

				If instaTouchRestrictionMaxTimesBeforeBlocking > 0 AndAlso counter > instaTouchRestrictionMaxTimesBeforeBlocking Then
					resp = New CJsonResponse(False, "", "")
					Response.Clear()
					Response.Write(JsonConvert.SerializeObject(resp))
					Return
				End If
			End If
        Catch ex As Exception
            _log.Error(ex.Message, ex)
            resp = New CJsonResponse(False, "", "")
        End Try
        Response.Clear()
        Response.Write(JsonConvert.SerializeObject(resp))
    End Sub

    Private Sub EmailRestriction()
        Try
            Dim lenderID As Guid
            Dim instaTouchRestrictionInternalEmails = New List(Of String)
            Dim instaTouchRestrictionMaxTimesBeforeBlocking = -1
            Dim instaTouchRestrictionMaxTimesBeforeNotification = -1
            Dim instaTouchRestrictionExemptIPs = New List(Of CIPCommentsItem)
            GetRestriction(lenderID, instaTouchRestrictionInternalEmails, instaTouchRestrictionMaxTimesBeforeBlocking, instaTouchRestrictionMaxTimesBeforeNotification, instaTouchRestrictionExemptIPs)

            If instaTouchRestrictionExemptIPs IsNot Nothing AndAlso instaTouchRestrictionExemptIPs.Any(Function(p)
                                                                                                           If p.IP = Request.UserHostAddress Then Return True
                                                                                                           If p.IP.EndsWith("*") Then '123.23.12.*
                                                                                                               Return Request.UserHostAddress.StartsWith(p.IP.Substring(0, p.IP.LastIndexOf("."c) + 1))
                                                                                                           End If
                                                                                                           Return False
                                                                                                       End Function) Then

                Return
            Else
                Dim counter As Integer = Utils.CountInstaTouchRequestTracking(lenderID, "", Request.UserHostAddress) + 1
                If instaTouchRestrictionMaxTimesBeforeNotification > 0 AndAlso counter = instaTouchRestrictionMaxTimesBeforeNotification AndAlso counter < instaTouchRestrictionMaxTimesBeforeBlocking AndAlso instaTouchRestrictionInternalEmails IsNot Nothing AndAlso instaTouchRestrictionInternalEmails.Count > 0 Then
                    Common.SendInstaTouchRestrictionNotificationEmails(instaTouchRestrictionInternalEmails, counter, instaTouchRestrictionMaxTimesBeforeBlocking, Request.UserHostAddress)
                End If

                If instaTouchRestrictionMaxTimesBeforeBlocking > 0 AndAlso counter = instaTouchRestrictionMaxTimesBeforeBlocking Then
                    If instaTouchRestrictionInternalEmails IsNot Nothing AndAlso instaTouchRestrictionInternalEmails.Count > 0 Then
                        Common.SendInstaTouchRestrictionBlockingEmails(instaTouchRestrictionInternalEmails, instaTouchRestrictionMaxTimesBeforeBlocking, Request.UserHostAddress)
                    End If
                End If
            End If
        Catch ex As Exception
            _log.Error(ex.Message, ex)
        End Try
    End Sub


	Public Function GetCachedAuthorizationAccessToken() As String
		s_retrieveAccessTokenSemaphore.Wait()

		Try
			If Common.SafeString(s_lastRetrievedAccessToken) <> "" AndAlso s_lastRetrievedAccessTokenTime.Add(s_lastRetrievedAccessTokenExpires).AddMinutes(-5) >= DateTimeOffset.Now Then
				Return s_lastRetrievedAccessToken
			End If

			Dim authorizationRequest = New AuthorizationRequest()
			Dim authorizationAPI = New AuthorizationAPI(authorizationRequest)
			Dim authorizationResponse = authorizationAPI.Request(Of AuthorizationResponse)()

			If authorizationResponse Is Nothing OrElse String.IsNullOrWhiteSpace(authorizationResponse.access_token) Then
				Throw New Exception("InstaTouch API Error at Call 1 - Authorization: Auth response is malformed, or access token is blank.")
			End If
			s_lastRetrievedAccessToken = authorizationResponse.access_token
			s_lastRetrievedAccessTokenExpires = New TimeSpan(0, 0, seconds:=authorizationResponse.expires_in_secs)
			s_lastRetrievedAccessTokenTime = DateTimeOffset.Now
			Return s_lastRetrievedAccessToken
		Finally
			s_retrieveAccessTokenSemaphore.Release()
		End Try
	End Function
#End Region

End Class
