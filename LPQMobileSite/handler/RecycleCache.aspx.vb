﻿
Imports log4net
Imports LPQMobile.Utils

Partial Class handler_RecycleCache
	Inherits System.Web.UI.Page	' DO NOT inherits from CBasePage, it will make infinite loop due to clearing cache process
	Protected _Log As ILog = LogManager.GetLogger("handler_RecycleCache")
	Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
		If Not IsPostBack Then
			Dim suffix As String = Common.SafeString(Request.QueryString("suffix"))
			Dim recycledb As String = Common.SafeString(Request.QueryString("recycledb"))
			If Not String.IsNullOrEmpty(suffix) Then
				CDownloadedSettings.Recycle(suffix, recycledb.ToUpper().Equals("Y"))
			End If
		End If
	End Sub
End Class
