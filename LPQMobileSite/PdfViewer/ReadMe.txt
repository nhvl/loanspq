﻿This pdf.js plugin renders PDF file or base64 content

src of pdf got translated during DOM rendering.  Pdf is now cached and come from mlapp.loanspq.com
https://apptest.loanspq.com/pdfviewer/web/viewer.aspx?file=https://mlapp.loanspq.com:443/pdfviewer/pdf/11111153eed453b987367e26bd37d43_AyxSsz5XFGAnMrS_5TKAB_Op-MuaS5trGbwqbKraOFE5Dy2M1REh9PA3URMizZom8lmxxcxLa_97H0ABFdU2-bd_s-R_sH6BujPah1kw0AbQIGyDtgmmX7vMLS6VM-wxxj7DLGrgGFyfjs1PYkVFGZ.pdf



For every pdf.js update, perform these steps.
1) remove file origin check from viewer.js
   We cached the pdf so they are on the same domain even though the full url is different

2) ADA compliance fixed
   same modification as in rev#5907

3) Change _download() anchor target from _parent to _blank. See AP-2745, r6882.