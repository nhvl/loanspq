﻿
Imports System.Net
Imports LPQMobile.Utils

Namespace PdfViewer.web

	''' <summary>
	''' This handler calls FileDownloader service located on different server that has internet access.
	''' https://apptest.loanspq.com/PdfViewer/Web/Downloader.aspx?lenderref=11111153eed453b987367e26bd37d43&file=https://cs.loanspq.com/Information/DocViewer.aspx?enc2=TyJy5OIr6ItSafGtFH_mVV6Q0D40BRvrHY-AK5pSWniHLZkXCkAAxOxtE7xdqkrWvyyLt6T01as_AafmX2GXwtXKD-ajAiOL5zKBv44IuJxALzlm-YRXF5GH6NdstluDrpDV_T8x9JfCGSVfJo_dIJJOf3ohQrgxdsVf7YmfdsDeLSAyQXSf6oBX-UaYODXHfsYI7L4p_WzuK_u9LF9M-aHBnYthjmFaoESFRkrImvVFuX026oFLm1Ok5iEYFqy-l-J9pxIb4HP28rYlhtXJr4L8Fmhn2BJG13U7TfT6vDGsmomKlwlUrBT7Hv8UU5Zzj5P86xlOSWF-Vi2bz5q_0g**
	''' https://apptest.loanspq.com/PdfViewer/Web/Downloader.aspx?lenderref=11111153eed453b987367e26bd37d43&file=c:\windows\WindowsUpdate.log
	''' <remarks></remarks>
	Partial Class PdfViewer_web_Downloader
		Inherits System.Web.UI.Page
		Private _log As log4net.ILog = log4net.LogManager.GetLogger(Me.GetType)

		Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
			If IsPostBack = False Then
				Try
					If Request.Params("file") Is Nothing Then
						Throw New Exception("Parameter 'file' is required.")
					End If
					If Request.Params("lenderref") Is Nothing Then
						Throw New Exception("Parameter 'lenderref' is required.")
					End If

					Dim fileUrl = Request.Params("file") ''full document path from different domain. ex:https://remote.com/information/docviewer.aspx?enc=23123123123123123123
                    Dim lenderref = Request.Params("lenderref")
					Dim result As String

                    Using client As New WebClient()
                        Dim response As Byte() = client.UploadValues(Common.GetCrossDomainServicesUrl() + "FileDownloader.aspx", New NameValueCollection() From {
                            {"file", fileUrl},
                            {"lenderref", lenderref}
                        })

						result = Encoding.UTF8.GetString(response)	 'return full document path (modified file name) on the same domain, ex:loanspq.com/pdfviewer/pdf/1111112-dasdaa.pdf
                    End Using

                    Response.Write(result)
                    HttpContext.Current.ApplicationInstance.CompleteRequest()
				Catch ex As Exception
					_log.Error("Error download file " & ex.Message, ex)
					If (ex.Message <> "Thread was being aborted.") Then
						Response.Write("Error")
					End If
				End Try
			End If
		End Sub
	End Class
End Namespace