﻿
Imports System.Net
Imports LPQMobile.Utils

Namespace PdfViewer.web

    ''' <summary>
	''' This handler will call CrossDomainChecker service on another server with internet access.
	''' Expected responses:
	''' OK = allow framing
	''' CrossDomain = framing not allow
	''' Error = LPQ Remote server can't communicate with client server( will get a warning in the lol)
	''' </summary>
    ''' <remarks></remarks>
    Partial Class PdfViewer_web_CrossDomainChecker
        Inherits System.Web.UI.Page
		Private _log As log4net.ILog = log4net.LogManager.GetLogger(Me.GetType)

		Private Shared _CrossDomainCheckCacheLock As New Object
		Private Const sCacheKey = "CROSS_DOMAIN_CHECK_CACHE_KEY"

		Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
			If IsPostBack = False Then
				Dim result As String = ""
				Try
					If Request.Params("check") Is Nothing Then
						Throw New Exception("Parameter 'check' is required.")
					End If

					Dim url As String = Request.Params("check")
					Dim lenderref = Request.Params("lenderref")
					Dim sKey As String = Left(lenderref & "_" & Common.SafeString(url), 35)

					'For clearing the cache
					If Common.SafeString(Request.Params("IsRecycle")).ToUpper = "Y" Then
						HttpContext.Current.Cache.Remove(sCacheKey)
					End If

					Dim oCrossDomainCheckDic As Dictionary(Of String, String) = CType(HttpContext.Current.Cache.Get(sCacheKey), Dictionary(Of String, String))

					If oCrossDomainCheckDic IsNot Nothing Then
						If oCrossDomainCheckDic.ContainsKey(sKey) Then
							result = oCrossDomainCheckDic(sKey)
							_log.Info("Object " & sKey & ": " & result & " was retrieved from cache. The cache " & _
							"was last updated at " & HttpContext.Current.Application(sKey).ToString())
							Exit Try
						End If

					End If
					SyncLock _CrossDomainCheckCacheLock
						' Ensure that the data was not loaded by a concurrent thread 
						' while waiting for lock.
						If oCrossDomainCheckDic IsNot Nothing Then
							If oCrossDomainCheckDic.ContainsKey(sKey) Then
								result = oCrossDomainCheckDic(sKey)
								_log.Info("Object " & sKey & ": " & result & " was retrieved from cache. The cache " & _
								"was last updated at " & HttpContext.Current.Application(sKey).ToString())
								Exit Try
							End If
						End If

						Using client As New WebClient()
							_log.Debug("CrossDomain check for: " & url)
							Dim response As Byte() = client.UploadValues(Common.GetCrossDomainServicesUrl() + "Crossdomainchecker.aspx", New NameValueCollection() From {
								{"check", url},
								{"lenderref", lenderref}
							})

							result = Encoding.UTF8.GetString(response)
						End Using
						If result.ToUpper.Contains("ERROR") Then
							_log.Info("CrossDomainChecker(remote server) return: " & result)
							Exit Try
						End If
						'don't have to worry about the timeout event bc
						'timeout will raise exception which will not update the cache
						If oCrossDomainCheckDic Is Nothing Then
							oCrossDomainCheckDic = New Dictionary(Of String, String)
						End If
						oCrossDomainCheckDic(sKey) = result
						_log.Debug("CrossDomain done for: " & url & ":  " & result)

						If oCrossDomainCheckDic.Count > 0 Then ' count less than 0 mean there is probably error in download so don't wnat to store in session
							HttpContext.Current.Cache.Insert(sCacheKey, oCrossDomainCheckDic, Nothing, System.Web.Caching.Cache.NoAbsoluteExpiration, New TimeSpan(12, 0, 0))	'12hour&45sec
							HttpContext.Current.Application(sKey) = DateTime.Now
						End If
					End SyncLock

				Catch ex As Exception
					_log.Error("CrossDominaChecker(LPQMobile server) err: " & ex.Message, ex)
					result = ("Error")
				End Try
				Response.Write(result)
				'HttpContext.Current.ApplicationInstance.CompleteRequest()
			End If
		End Sub
    End Class
End Namespace