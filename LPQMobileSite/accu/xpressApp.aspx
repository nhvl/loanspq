﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="xpressApp.aspx.vb" Inherits="CXpressApp"
    EnableViewState="false" %>
<%--<%@ Register Src="~/Inc/MemberProtectionPlan.ascx" TagPrefix="uc" TagName="memberProtection" %>--%>
<%@ Register Src="~/Inc/PageHeader.ascx" TagPrefix="uc" TagName="pageHeader" %>
<%@ Register Src="~/Inc/Disclosure.ascx" TagPrefix="uc" TagName="disclosure" %>
<%@ Register Src="~/Inc/CustomQuestions.ascx" TagPrefix="uc" TagName="customQuestions" %>
<%@ Register Src="~/Inc/MainApp/xaApplicantInfo.ascx" TagPrefix="uc" TagName="xaApplicantInfo" %>
<%@ Register Src="~/Inc/MainApp/xaApplicantQuestion.ascx" TagPrefix="uc" TagName="xaApplicantQuestion" %>
<%@ Register Src="~/Inc/MainApp/xaApplicantContactInfo.ascx" TagPrefix="uc" TagName="xaApplicantContactInfo" %>
<%@ Register Src="~/Inc/MainApp/xaApplicantID.ascx" TagPrefix="uc" TagName="xaApplicantID" %>
<%@ Register Src="~/Inc/MainApp/xaApplicantAddress.ascx" TagPrefix="uc" TagName="xaApplicantAddress" %>
<%@ Register Src="~/Inc/MainApp/xaApplicantEmployment.ascx" TagPrefix="uc" TagName="xaApplicantEmployment" %>
<%@ Register Src="~/Inc/MainApp/xaFundingOptions.ascx" TagPrefix="uc" TagName="xaFundingOptions" %>
<%@ Register Src="~/Inc/MainApp/xaFOMQuestion.ascx" TagPrefix="uc" TagName="xaFOMQuestion" %>
<%--<%@ Register Src="~/Inc/MainApp/UploadDocDialog.ascx" TagPrefix="uc" TagName="ucUploadDocDialog" %>--%>
<%--<%@ Register Src="~/Inc/NewDocCapture.ascx" TagPrefix="uc" TagName="DocUpload" %>
<%@ Register Src="~/Inc/DocCaptureSourceSelector.ascx" TagPrefix="uc" TagName="DocUploadSrcSelector" %>--%>
<%@ Register Src="~/Inc/NewDocumentScan.ascx" TagPrefix="uc" TagName="driverLicenseScan" %>
<%@ Register Src="~/xa/inc/ProductSelection.ascx" TagPrefix="uc" TagName="ProductSelection" %>
<%@ Reference Control="~/xa/inc/ProductServices.ascx" %>

<%@ Register TagPrefix="uc1" TagName="Piwik" Src="~/Inc/Piwik/PiwikTracking.ascx" %>
<%@ Register TagPrefix="uc" TagName="pageFooter" Src="~/Inc/PageFooter.ascx" %>

<%@ Import Namespace="LPQMobile.Utils" %>
<%@ Import Namespace="System.Web.Optimization" %>

<!DOCTYPE html>

<head id="Head1" runat="server">
    <title>XA Mobile</title>
    <meta name="viewport" content="width=device-width, initial-scale=1" />

    <script type="text/javascript">
        var FILTERPRODUCTCODES=<%=_filterProductCodes%>;
        var MINORPRODUCTLIST=<%=_minoProductList%>;
        var MINORACCOUNTLIST=<%=_minorAccountList%>;
        var _ProductQuestionPackage = <%=_ProductPackageJsonString %>;
        var _FundingSourcePackage = <%=_FundingSourcePackageJsonString%>;
        <%--var _FooterThemeData ='<%=_FooterDataTheme%>';--%>
        var OCCUPATIONLIST = '<%=_occupationList%>';
        var ISLIVE='<%=_isLive%>';
    </script>
	<%:Styles.Render("~/css/thirdparty/bootstrap") %>
    <uc:pageHeader ID="ucPageHeader" ScriptFolder="accu" runat="server" />
	<%:Styles.Render("~/css/thirdparty/custom") %>
	  <style type="text/css">
		   .video-container {
			position: relative;
			padding-bottom: 56.25%;
			padding-top: 35px;
			height: 0;
			overflow: hidden;
		}

		.video-container iframe {
			position: absolute;
			top:0;
			left: 0;
			width: 100%;
			height: 100%;
		}
	</style>
</head>
<body class="lpq_container">
	<input type="hidden" id="hdPlatformSource" value="<%=PlatformSource%>" />
    <input type="hidden" id="hfLenderRef" value='<%=_CurrentLenderRef%>' />
    <input type="hidden" id="hfBranchId" value='<%=_CurrentBranchId%>' />
    <input type="hidden" id="hfCSRF" value='<%=_token%>' />
    <input type="hidden" id="hfReferralSource" value='<%=_ReferralSource%>' />
    <%--<input type="hidden" id="hdOccupancyStatus" value='<%=_OccupancyStatus %>' />--%>
    <input type="hidden" id="hdPrevAddressThreshold" value='<%= _previous_address_threshold%>' />
	<%--<input type="hidden" id="hdPrevEmploymentThreshold" value="<%=_previous_employment_threshold%>" />--%>
	<input type="hidden" id="hdPrevEmploymentThreshold" value="0" /> <%-- TODO: not yet suport on lender side so set a bogus value so this feature will not be activate--%>
    <input type="hidden" id="hdCheckMailAddress" value="<%=_CheckMailAddress%>" />
    <input type="hidden" id="hdAddressKey" value='<%= _address_key%>' />
    <%--<input type="hidden" id="hdMaxFunding" value='<%=_MaxFunding%>' />--%>
    <%--<input type="hidden" id="hdHeaderTheme" value="<%=_HeaderDataTheme%>" />--%>
    <%--<input type="hidden" id="hdFooterTheme" value="<%=_bgTheme %>" />
    <input type="hidden" id="hdBgTheme" value="<%=_BackgroundTheme%>" />--%>
    <input type="hidden" id="hdHasMinorApplicant" value="<%=_HasMinorApplicant%>" />
    <%--<input type="hidden" id="hdHasMinorId" value="<%=_HasMinorId%>" />--%>
    <input type="hidden" id="hdHasEnableSecUploadDocs" value="<%=_HasEnableSecondaryUpLoadDocs%>" />
    <input type="hidden" id="hdHasCoApplicant" value="N"/>
    <%--<input type="hidden" id="hdShowEmployment" value="<%=_ShowEmployment%>"/>
    <input type="hidden" id="hdShowMinorEmployment" value="<%=_HasMinorEmployment %>"/>--%>
    <input type="hidden" id="hdSecondRoleType" value="" />
    <%--<input type="hidden" id="hdMaritalStatus" value ="<%=_maritalStatus%>" />--%>
	<input type="hidden" id="hdStopInvalidRoutingNumber" value="<%=_xaStopInvalidRoutingNumber%>" />
    <%-- moved from inside page#1 don't wknow why page1 dissapear after submit & save--%>
    <input type="hidden" runat="server" id="hdNumWalletQuestions" />
    <input type="hidden" runat="server" id="hdAvailability" value="1" />  
    <input type="hidden" runat="server" id="HideFOMPage" />
    <input type="hidden" runat="server" id="hdForeignAppType" />
    <input type="hidden" runat="server" id="hdScanDocumentKey" />
	
    <%--<input type="hidden" runat="server" id="hdEnableJoint" />--%>
    <input type="hidden" id="hdForeignContact" runat ="server"  />
    <input type="hidden" id="hdMinorAccountCode" runat="server" value=""/>
    <input type="hidden" id="hdRequiredDLScan" value="<%=_requiredDLScanXA%>" />
     <!--hidden secondary fields -->
    <%--<input type="hidden" id="hdSecondaryMotherMaidenName" runat="server" />
    <input type="hidden" id="hdSecondaryCitizenship" runat ="server" />
    <input type="hidden" id="hdSecondaryEmployment" runat="server" />
    <input type="hidden" id="hdSecondaryIdPage" runat="server" />
    <input type="hidden" id="hdSecondaryOccupancy" runat="server" />
    <input type="hidden" id="hdSecondaryGrossIncome" runat="server" />--%>
    <input type="hidden" id="hdSpecialJoint" runat ="server" />    
	
	<input type="hidden" id="hdEnableEmploymentSection" value="<%=IIf(EnableEmploymentSection, "Y", "N") %>"/>
	<input type="hidden" id="hdEnableEmploymentSectionForCoApp" value="<%=IIf(EnableEmploymentSection("co_"), "Y", "N") %>"/>
	<input type="hidden" id="hdEnableEmploymentSectionForMinor" value="<%=IIf(EnableEmploymentSection("m_"), "Y", "N") %>"/>
	<input type="hidden" id="hdEnableIDSection" value="<%=IIf(EnableIDSection, "Y", "N") %>"/>
	<input type="hidden" id="hdEnableIDSectionForCoApp" value="<%=IIf(EnableIDSection("co_"), "Y", "N") %>"/>
	<input type="hidden" id="hdEnableIDSectionForMinor" value="<%=IIf(EnableIDSection("m_"), "Y", "N") %>"/>
	<input type="hidden" id="hdEnableDisclosureSection" value="<%=IIf(EnableDisclosureSection, "Y", "N") %>"/>
	<input type="hidden" id="hdEnableOccupancyStatusSection" value="<%=IIf(EnableOccupancyStatusSection, "Y", "N") %>"/>
	<input type="hidden" id="hdEnableOccupancyStatusSectionForCoApp" value="<%=IIf(EnableOccupancyStatusSection("co_"), "Y", "N")%>"/>
	<input type="hidden" id="hdEnableOccupancyStatusSectionForMinor" value="<%=IIf(EnableOccupancyStatusSection("m_"), "Y", "N") %>"/>
	
    <div>
       
		 <div id="accuWelcome" data-role="page">
            <div data-role="header" style="display: none">
              	<h1>New Membership</h1>
			</div>
            <div data-role="content">
				
				<div class='logo-container'>			
					<img class='logo-image' id="ConsumerLoginHeader1_IMG1" alt="" src="../logo/americaChristianccu.jpg" />
				</div>
				 
				<br />

                <%=HeaderUtils.RenderPageTitle(0, "AMERICA'S CHRISTIAN CREDIT UNION ONLINE APPLICATION", False)%>
             
				<%=HeaderUtils.RenderPageTitle(0, "Welcome Christian Care Ministry Medi-Share Members", True)%><br />

				<label id="welcome1" class="">We are thankful for our relationship with the Christian Care Ministry, and look forward to serving you.</label>
				 <br />

				<label id="Label1" class="">We've created a short video which explains the relationship between CCM and ACCU.</label> <br /><br />

				<div class="video-container">
					<%--use one of 2 method to load video--%>
			 
					<%--<iframe width="100%" height="100%" src="https://www.youtube.com/embed/HXqRStwEzpM" frameborder="0" allowfullscreen></iframe>--%>
					
					<%--MP4 and ogg type are supported by most browser or else browser need to have the extension installled autoplay="autoplay"--%>
					<%--<video width="100%" height="100%"  controls="controls" loop="loop">
						<source src="mov_bbb.mp4" type="video/mp4" />
						Your browser does not support the video tag.
					</video>--%>

					<iframe src="https://player.vimeo.com/video/171430321" height="315" width="560" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
			
				</div>
				<%If EnableDisclosureSection Then%>
				<%=HeaderUtils.RenderPageTitle(17, "Disclosures", True)%>
                <div style="margin-top: 10px;">
                    <div id="divSubmitMessage0">
					    <%If Not String.IsNullOrEmpty(_CreditPullMessage) Then%>
						    <label id="Label3" class="">
                             <%=_CreditPullMessage%>
							</label> 
                         <%Else%>  
						   <br />
						    <label id="Label2" class="">In order to create your <b>ACCU Medi-Share checking account</b>, we need you to review a few disclosures.
								  Click on each link to view the disclosure, then please click the check box to indicate that you've read and agree with its contents
						    </label> 
                              
                        <%End If%>
					</div>
					<%--TODO make all link use button color--%>
                    <uc:disclosure ID="ucDisclosures" LoanType="XA" runat="server" Name="disclosures" />  
                </div>           
				<%End If%>
            </div>
            <div class ="div-continue" data-role="footer">
                <a href="#"  data-transition="slide" onclick="UpdateCSRFNumber(hfCSRF); UpdateCSRFNumber(hfCSRF); return validateForm(this);"  type="button" class="btnSubmit div-continue-button" >Continue</a> 
                 <a id="href_show_last_dialog" href="#xa_last" style="display: none;"></a>
                   <%--<%=_HasNavLeft%>--%>
            </div>
            <%--Next Page transition and destination is done inside validateActivePage onclick="handleDepositChange(this); validateActivePage(this, true); "--%>
          
        </div>



        <div id="GettingStarted" data-role="page">
            <div data-role="header" style="display: none">
                <%If _Availability = "2" Or _Availability = "2a" Or _Availability = "2b" Then%>
                    <h1>Open an Account</h1>
                <%Else%>
					<%If _InstitutionType = CEnum.InstitutionType.BANK Then%>
						 <h1>Open an Account</h1>
					 <%Else%>
						<h1>New Membership</h1>
					 <%End If%>
                <%End If%>
            </div>
            <div data-role="content">
                <%If _Availability = "2" Or _Availability = "2a" Or _Availability = "2b" Then%>
                    <%=HeaderUtils.RenderLogoAndSteps(_LogoUrl, 0, "OPEN")%>
                    <%=HeaderUtils.RenderPageTitle(9, "Open an Account", False)%>
                <%Else%>
                    <%=HeaderUtils.RenderLogoAndSteps(_LogoUrl, 1, "JOIN")%>
                    <%=HeaderUtils.RenderPageTitle(21, "Eligibility", False)%>
                <%End If%>
                
				<%If _enableJoint Or _RenderSpecialAccountType.Length > 0 Then%>                	
					<%=HeaderUtils.RenderPageTitle(0, "Select if this applies to you", True)%>
					<div class="container" id="btnSelectionContainer">
						<div class="row">
							<div id="divAccountTypeCol">								
								<%=_RenderSpecialAccountType%> 
							</div>
							<div id="divBtnJointCol">
								 <%If _enableJoint Then %>                    
									 <a data-role="button" class="btn-header-theme" id="btnHasCoApplicant">Yes, I have a Joint Applicant</a>
								<%End If %>
							</div>
						</div>
					</div>
				 <%End If%>
				             
                <uc:xaFOMQuestion runat="server" ID='ucFomQuestion' />
                <%-- Page 1 --%>
                <fieldset data-role="controlgroup" style="display: none;" data-type="horizontal">
                    <%=_HiddenNumProducts%>
                    <%=_HiddenMinAmounts%>
                    <%=_HiddenProducts%>
                    <%=_HiddenRequiredProducts%>
                </fieldset>
               
                 <uc:ProductSelection ID="ucProductSelection" runat="server" />
               
            </div>
            <div class ="div-continue" data-role="footer">
                <a href="#"  data-transition="slide" onclick="validateActivePage(this, true);" type="button" class="div-continue-button">Continue</a> 
                <a href="#divErrorDialog" style="display: none;"></a>
                   <%--<%=_HasNavLeft%>--%>
            </div>
            <%--Next Page transition and destination is done inside validateActivePage onclick="handleDepositChange(this); validateActivePage(this, true); "--%>
          
        </div>

     
        <div data-role="dialog" id="scandocs" runat="server">
            <div data-role="header"  style="display:none" >
                <h1>Driver's License Scan</h1>
            </div>
            <div data-role="content">
                <a data-rel="back" class="pull-right"><%=HeaderUtils.IconClose %><span style="display: none;">close</span></a>                  
                <uc:driverLicenseScan ID="xaDriverlicenseScan" runat="server" />
				<div class ="div-continue" style="text-align: center;">
					<a href="#"  data-transition="slide" onclick="ScanAccept('');" type="button" data-role="button" class="div-continue-button">Done</a> 
					<a href="#divErrorDialog" style="display: none;"></a>              
				</div>
            </div>
            
        </div>

        <div data-role='page' id="pageMinorInfo">
            <div data-role="header" style="display: none">
                <h1>Minor Applicant Information</h1>
            </div>
            <div data-role="content">
                <%If _Availability = "2" Or _Availability = "2a" Or _Availability = "2b" Then%>
                    <%=HeaderUtils.RenderLogoAndSteps(_LogoUrl, 1, "OPEN")%>
                <%Else%>
                    <%=HeaderUtils.RenderLogoAndSteps(_LogoUrl, 1, "JOIN")%>
                <%End If%>
                 <%=HeaderUtils.RenderPageTitle(21, "Tell Us About Yourself", false)%>
                
               <div id="divMinorApplicant" style="display :<%=_visibleMinorApp%>" class="labeltext-bold">                  
                  <div id="divMinorPersonalInfo">                 
                    <%=HeaderUtils.RenderPageTitle(18, "Minor Information", true)%>
                    <uc:xaApplicantInfo ID="xaMinorApplicantInfo" IDPrefix="m_" runat="server" />
                      <br />
                  </div>
                 
                  <div id="divMinorContactInfo">
                    <%=HeaderUtils.RenderPageTitle(22, "Contact Information", true)%>
                    <uc:xaApplicantContactInfo ID="xaMinorApplicantContactInfo" IDPrefix="m_" runat="server" />
                      <br />
                  </div>

                   <div id="divMinorCurrentAddress">                     
                       <%=HeaderUtils.RenderPageTitle(14, "Current Physical Address", true)%>
                        <uc:xaApplicantAddress ID="xaMinorApplicantAddress" LoanType="XA" IDPrefix="m_" runat="server" />
                       <br />
                   </div>
                  <%If EnableIDSection("m_") Then %>
                    <uc:xaApplicantID ID="xaMinorApplicantID" IDPrefix="m_" Title=" Identification Information" runat="server" />
                      <br />
                  <%End If%>
                   <%If EnableEmploymentSection("m_") Then%>
                       <%--<%=HeaderUtils.RenderPageTitle(17, "About Your Employment", True)%>--%>
                       <uc:xaApplicantEmployment ID="xaMinorApplicantEmployment" IDPrefix="m_" runat="server" />
                   <%End If %>
				   <uc:xaApplicantQuestion ID="xaMinorApplicantQuestion" LoanType="XA" IDPrefix="m_" runat="server" />
                   
               </div>
             </div>       
			<div class ="div-continue"  data-role="footer">
					<a href="#"  data-transition="slide" onclick="validateActivePage(this, true);" type="button" class="div-continue-button">Continue</a> 
					<a href="#divErrorDialog" style="display: none;"></a>
				   Or <a href="#" class ="div-goback" onclick="goBack(this)" data-corners="false" data-shadow="false" data-theme="reset"><span class="hover-goback"> Go Back</span></a>   
			 </div>     
            
        </div>

        <div data-role='page' id='pageFS'>
            <div data-role="header" style="display: none">
                <h1>Funding</h1>
            </div>
            <div data-role="content">
                <%If _Availability = "2" Or _Availability = "2a" Or _Availability = "2b" Then%>
                    <%=HeaderUtils.RenderLogoAndSteps(_LogoUrl, 2, "OPEN")%>
                <%Else%>
                    <%=HeaderUtils.RenderLogoAndSteps(_LogoUrl, 2, "JOIN")%>
                <%End If%>
                 <%=HeaderUtils.RenderPageTitle(23, "Funding", false)%>
               
                <div id='DivDepositInput'>
                    <%=HeaderUtils.RenderPageTitle(0, "How much do you want to deposit?", True) %>
                    <div id="spProductAmount" style="padding-left: 30px;"></div>
                    <div class="LineSpace">
                        <%=_RenderInitialDepositProducts%>
                    </div>
                    <div data-role="fieldcontain">
                        <label><b>Total Deposit</b><span style='display: inline' class='require-span'>*</span></label>
                        <input type="<%=_textAndroidTel%>" id="txtTotalDeposit" class="money" readonly="readonly" style="font-weight: bold;" />
                    </div>
                </div>
                <div class='labeltext-bold' id='divFundingSource' style="margin-bottom: 15px">
                </div>
                <uc:xaFundingOptions ID="xaFundingOptions" runat="server" />
            </div>
            <div class ="div-continue"  data-role="footer">
                <a href="#"  data-transition="slide" onclick="validateActivePage(this, true);" type="button" class="div-continue-button">Continue</a> 
                <a href="#divErrorDialog" style="display: none;"></a>
                Or <a href="#" onclick="goBack(this);" class ="div-goback" data-corners="false" data-shadow="false" data-theme="reset"><span class="hover-goback"> Go Back</span></a>
            </div>
            <!--Popup content for question mark -->
            <div id="popupQuestionMark" data-role="popup">
                <div data-role="content">
                    <div class="row">
                        <div class="col-xs-12 header_theme2">
                            <a data-rel="back" class="pull-right"><%=HeaderUtils.IconClose %><span style="display: none;">close</span></a>
                        </div>
					 </div>
					 <div class="row">
                        <div class="col-sm-12">
                            <div style="text-align: center; margin: 10px 0;">
                                <img src="/images/SampleCC.jpg" alt="" style="width: 100%;"/>
                            </div>     
                        </div>    
                    </div>
                </div>
            </div>            
        </div>


        <div data-role="dialog" data-close-btn="none" id="fundingInternalTransfer">
            <div data-role="header" data-position="" style="display: none;">
                <h1>Fund via Internal Transfer</h1>
            </div>
            <div data-role="content">
                <div class="row">
                    <div class="col-xs-12 header_theme2" style="padding-right: 0px;">
                        <a onclick="goBackFInternalTransfer(this)" class="pull-right"><%=HeaderUtils.IconClose %><span style="display: none;">close</span></a>
                    </div>
                </div>
                <div data-placeholder="content"></div>
                <div class="div-continue" style="text-align: center;">
                    <a href="#" data-transition="slide" onclick="validateFInternalTransfer(this, true);" data-role="button" type="button" class="div-continue-button">Done</a> 
                    <a href="#divErrorDialog" style="display: none;"></a>
                  </div>
            </div>
        </div>

        <div data-role="dialog" data-close-btn="none" id="fundingCreditCard">
            <div data-role="header" data-position="" style="display: none;">
                <h1>Fund via Credit Card</h1>
            </div>
            <div data-role="content">
                <div class="row">
                    <div class="col-xs-12 header_theme2" style="padding-right: 0px;">
                        <a onclick="goBackFCreditCard(this)" class="pull-right"><%=HeaderUtils.IconClose %><span style="display: none;">close</span></a>
                    </div>
                </div>
                <div data-placeholder="content" style="overflow: auto;"></div>
                <div class="div-continue" style="text-align: center;">
                    <a href="#" data-transition="slide" onclick="validateFCreditCard(this, true);" type="button" data-role="button" class="div-continue-button">Done</a> 
                    <a href="#divErrorDialog" style="display: none;"></a>
                    <%--Or <a href="#" onclick="goBackFCreditCard(this)" class ="div-goback" data-corners="false" data-shadow="false" data-theme="reset"><span class="hover-goback"> Go Back</span></a>   --%>
                </div>
            </div>
        </div>
        <div data-role="dialog" data-close-btn="none" id="fundingFinancialIns">
            <div data-role="header" data-position="" style="display: none;">
                <h1>Transfer from another finacial institution</h1>
            </div>
            <div data-role="content">
                <div class="row">
                    <div class="col-xs-12 header_theme2" style="padding-right: 0px;">
                        <a onclick="goBackFFinacialIns(this)" class="pull-right"><%=HeaderUtils.IconClose %><span style="display: none;">close</span></a>
                    </div>
                </div>
                <div data-placeholder="content"></div>
                <div class="div-continue" style="text-align: center;">
                    <a href="#" data-transition="slide" onclick="validateFFinacialIns(this, true);" type="button" data-role="button" class="div-continue-button">Done</a> 
                    <a href="#divErrorDialog" style="display: none;"></a>
                    <%--Or <a href="#" onclick="goBackFFinacialIns(this)" class ="div-goback" data-corners="false" data-shadow="false" data-theme="reset"><span class="hover-goback"> Go Back</span></a>   --%>
                </div>
            </div>
            <div id="popUpAccountGuide" data-role="popup">
                <div data-role="content">
                    <div class="row">
                        <div class="col-xs-12 header_theme2">
                            <a data-rel="back" class="pull-right"><%=HeaderUtils.IconClose %><span style="display: none;">close</span></a>
                        </div>
					 </div>
					<div class="row">
                        <div class="col-sm-12">
                            <div style="text-align: center; margin: 10px 0;">
                                <img src="/images/SampleCC.jpg" alt="" style="width: 100%;"/>
                            </div>       
                        </div>    
                    </div>
                </div>
            </div>
        </div>

		<%--primary or custodian--%>
        <div data-role="page" id="page2">
            <div data-role="header" style="display: none">
                <h1><%=If(_HasMinorApplicant = "Y", "About Your Custodian or Joint Applicant", "Tell Us About Yourself")%></h1>
            </div>
            <div data-role="content" class="labeltext-bold">
                <%If _Availability = "2" Or _Availability = "2a" Or _Availability = "2b" Then%>
                    <%=HeaderUtils.RenderLogoAndSteps(_LogoUrl, 1, "OPEN")%>
                <%Else%>
                    <%=HeaderUtils.RenderLogoAndSteps(_LogoUrl, 1, "JOIN")%>
                <%End If%>
               
                <div class="pageTitle">
                    <%=HeaderUtils.RenderPageTitle(21, "Tell Us About Yourself", False)%>
                </div>
                <div class="minorPageTitle HiddenElement" >
                    <%=HeaderUtils.RenderPageTitle(21, "About Your Custodian or Joint Applicant", False)%>
                </div>
           
                <%=HeaderUtils.RenderPageTitle(18, "Personal Information", True)%>
                
                <uc:xaApplicantInfo ID="xaApplicantInfo" runat="server" />
                <div class="currentAddressTitle">
                    <%=HeaderUtils.RenderPageTitle(14, "Current Address", True)%>
                </div>
                <div class="minorCurrentAddressTitle HiddenElement">
                   <%=HeaderUtils.RenderPageTitle(14, "Current Address", True)%>
                </div>
                <uc:xaApplicantAddress ID="xaApplicantAddress" LoanType="XA" runat="server" />
                <%=HeaderUtils.RenderPageTitle(22, "Contact Information", true)%>
                <uc:xaApplicantContactInfo ID="xaApplicantContactInfo" runat="server" />
                <%If EnableIDSection Then%>
                    <uc:xaApplicantID ID="xaApplicantID" Title=" &nbsp; Identification" runat="server" />
                <%End If %>
                <%If EnableEmploymentSection Then%>
                    <%--<div id="divEmploymentStatus" >
                        <%=HeaderUtils.RenderPageTitle(17, "About Your Employment", True)%>--%>
                        <uc:xaApplicantEmployment ID="xaApplicantEmployment" runat="server" />
                    <%--</div>--%>
                <%End If%>

                <%--verify minor--%>
                <div id="divESignature" class="HiddenElement">
                    <br />
                    <div id="divMinorName">
                         <label>As the custodian for <span id="txtMinorName" class="VerifyMinor"></span>under the Virginia Uniform Transfers to Minor Act, I make this application on the minors</label>               
                     </div>                  
                    <div class="container">
                        <div class="row eSignature">
                            <div class="col-sm-5">      
                                <input type="checkbox" id="chkESignature"/>        
                               <label for="chkESignature" id="lblESignature"> <span class="RequiredIcon">eSignature</span>
                               </label>
                            </div>
                            <div class="col-sm-2"></div>
                            <div class="col-sm-5"> 
                                <div id="eSignDate" style="padding-top:15px" class="HiddenElement">  
                                    <label>Date:<span id="spDate" class="VerifyMinor"></span></label>
                               </div> </div> 
                        </div>
                    </div>                     
                </div>  <%--end verify minor --%>
               
                <%--<uc:DocUpload ID="ucDocUpload" IDPrefix=""  runat="server"/>--%>
				<uc:xaApplicantQuestion ID="xaApplicantQuestion" LoanType="XA" IDPrefix="" runat="server" />				
            </div>
            <div class ="div-continue"  data-role="footer">
                <a href="#"  data-transition="slide" onclick="validateActivePage(this, true); UpdateCSRFNumber(hfCSRF);" type="button" class="div-continue-button">Continue</a> 
                <a href="#divErrorDialog" style="display: none;"></a>
               Or <a href="#" onclick="goBack(this);" class ="div-goback" data-corners="false" data-shadow="false" data-theme="reset"><span class="hover-goback"> Go Back</span></a>   
             </div>
           
        </div>

        <div data-role="page" id="co_scandocs" runat="server" >
             <div data-role="header"  style="display:none" >
                <h1>Co-Driver's License Scan</h1>
            </div>
            <div data-role="content">                 
                <a data-rel="back" class="pull-right"><%=HeaderUtils.IconClose %><span style="display: none;">close</span></a>   
                <uc:driverLicenseScan ID="xaCoDriverlicenseScan" IDPrefix="co_"  runat="server" />
				<div class ="div-continue" style="text-align: center;">
					<a href="#"  data-transition="slide" onclick="ScanAccept('co_');" type="button" data-role="button" class="div-continue-button">Done</a> 
					<a href="#divErrorDialog" style="display: none;"></a>               
			   </div>
            </div>			          
        </div>

        <%--create final review page --%>
        <div data-role="page" id="reviewpage">
            <div data-role="header" style="display: none">
                <h1>Review and Submit</h1>
            </div>
            <div data-role="content">
                <%If _Availability = "2" Or _Availability = "2a" Or _Availability = "2b" Then%>
                    <%=HeaderUtils.RenderLogoAndSteps(_LogoUrl, 3, "OPEN")%>
                <%Else%>
                    <%=HeaderUtils.RenderLogoAndSteps(_LogoUrl, 3, "JOIN")%>
                <%End If%>
                <%=HeaderUtils.RenderPageTitle(19, "Review and Submit", false)%>

                <div id="reviewPanel" class="review-container container-fluid" data-role="fieldcontain">
                  
                    <%--PRODUCT SELECTIONS--%>
                    <div class="row">
                        <div class="row-title section-heading">
                            <b>Product Selections</b>        
                        </div>
                    </div>
                    <div class="row panel ViewProductSelection" style="padding-left: 50px;"></div>
                    <%--END PRODUCT SELECTIONS--%>
                    <%--OTHER PRODUCT INTEREST--%>
                    <div class="row">
                        <div class="row-title section-heading">
                            <b>Other Product Interest</b>        
                        </div>
                    </div>
                    <div class="row panel ViewOtherProductInterest"></div>
                    <%--END OTHER PRODUCT INTEREST--%>
                    <%--PRODUCT CUSTOM QUESTION--%>
                    <%--TEMPORARY HIDE THIS SECTION (CHANGE REQUEST #26)--%>
                    <%--<div class="row">
                        <div class="col-xs-6 col-sm-3 text-right row-title">
                            <label>Custom Questions:</label>        
                        </div>
                        <div class="col-xs-6 col-sm-9 text-left row-data"></div>
                    </div>
                    <div class="row panel ViewProductCustomQuestion"></div>--%>
                    <%--END PRODUCT CUSTOM QUESTION--%>
                    
                    <%--APPLICANT INFORMATION --%>
                    <div class="row">
                        <div class="row-title section-heading">
                            <b>Applicant Information</b>        
                        </div>
                    </div>
                     <div class="row panel ViewAcountInfo"></div>
                    <%--END APPLICANT INFORMATION --%>
                    <%--CONTACT INFORMATION --%>
                    <div class="row">
                        <div class="row-title section-heading">
                            <b>Contact Information</b>        
                        </div>
                    </div>
                    <div class="row panel ViewContactInfo"></div>
                    <%--END CONTACT INFORMATION --%>
                    <%--IDENTIFICATION INFORMATION --%>
					<%If EnableIDSection Then%>
                    <div class="row">
                        <div class="row-title section-heading">
                            <b>Identification Information</b>        
                        </div>
                    </div>
                    <div class="row panel ViewIdentification"></div>
					<%End If%>
                    <%--END IDENTIFICATION INFORMATION --%>
                    <%--PHYSICAL ADDRESS --%>
                    <div class="row">
                        <div class="row-title section-heading">
                            <b>Current Physical Address</b>        
                        </div>
                    </div>
                    <div class="row panel ViewAddress"></div>
                    <%--END PHYSICAL ADDRESS --%>
                    <%--EMPLOYMENT INFORMATION --%>
                    <%If EnableEmploymentSection Then%>
                    <div class="row">
                        <div class="row-title section-heading">
                            <b>Employment Information</b>        
                        </div>
                    </div>
                    <div class="row panel ViewEmploymentInfo"></div>
					<div class="row">
						<div class="row-title section-heading">
							<b>Previous Employment Information</b>        
						</div>
					</div>
					<div class="row panel ViewPrevEmploymentInfo"></div>
                    <%End If %>
                    <%--END EMPLOYMENT INFORMATION --%>
                    <%--CO APPLICANT--%>
                    <div data-section-name="reviewJointApplicant" style="display: <%=IIf(_enableJoint, "block", "none")%>">
                        <div class="row">
                            <div class="row-title section-heading">
                                <b>Joint Applicant Information</b>
                            </div>
                        </div>
                        <div class="row panel co_ViewAcountInfo"></div>
                        <div class="row">
                            <div class="row-title section-heading">
                                <b>Joint Applicant Contact Information</b>
                            </div>
                        </div>
                        <div class="row panel co_ViewContactInfo"></div>
						<%If EnableIDSection("co_") Then%>
                        <div class="row">
                            <div class="row-title section-heading">
                                <b>Joint Applicant Identification</b>
                            </div>
                        </div>
                        <div class="row panel co_ViewIdentification"></div>
						<%End If %>
                        <div class="row">
                            <div class="row-title section-heading">
                                <b>Joint Applicant Address</b>
                            </div>
                        </div>
                        <div class="row panel co_ViewAddress"></div>
                        <%If EnableEmploymentSection("co_") Then%>
                        <div class="row">
                            <div class="row-title section-heading">
                                <b>Joint Applicant Employment Information</b>
                            </div>
                        </div>
                        <div class="row panel co_ViewEmploymentInfo"></div>
						<div class="row" style="display: none;">
							<div class="row-title section-heading">
								<b>Joint Applicant Previous Employment Information</b>        
							</div>
						</div>
						<div class="row panel ViewJointApplicantPrevEmploymentInfo" style="display: none;"></div>
                        <%End If %>
                    </div>
                        <%--END CO APPLICANT--%>
                    <%--FUNDING SOURCE --%>
                    <div class="row">
                        <div class="row-title section-heading">
                            <b>Funding Source</b>        
                        </div>
                    </div>
                    <div class="row panel ViewFundingSource"></div>
                    <%--END FUNDING SOURCE --%>
                    <%--REVIEW MINOR INFORMATION --%>
                    <%If _HasMinorApplicant = "Y" Then%>
                    <div class="row text-center section-heading">
                        <b>Review Minor Information</b>
                    </div>
                    <%--MINOR INFORMATION --%>
                    <div class="row">
                        <div class="row-title section-heading">
                            <b>Minor Information</b>        
                        </div>
                    </div>
                    <div class="row panel ViewMinorAcountInfo"></div>
                    <%--END MINOR INFORMATION --%>
                    <%--MINOR CONTACT INFORMATION --%>
                    <div class="row">
                        <div class="row-title section-heading">
                            <b>Minor Contact Information</b>        
                        </div>
                    </div>
                    <div class="row panel ViewMinorContactInfo"></div>
                    <%--END MINOR CONTACT INFORMATION --%>
                    <%--MINOR IDENTIFICATION INFORMATION --%>
                    <%If EnableIDSection("m_") Then%>
                    <div class="row">
                        <div class="row-title section-heading">
                            <b>Minor Identification Information</b>        
                        </div>
                    </div>
                    <div class="row panel ViewMinorIdentification"></div>
                    <%End If %>
                    <%--END MINOR IDENTIFICATION INFORMATION --%>
                    <%--MINOR CURRENT PHYSICAL ADDRESS --%>
                    <div class="row">
                        <div class="row-title section-heading">
                            <b>Minor Physical Address</b>        
                        </div>
                    </div>
                    <div class="row panel ViewMinorAddress"></div>
                    <%--END MINOR CURRENT PHYSICAL ADDRESS --%>
                    <%--MINOR EMPLOYMENT INFORMATION --%>
                    <%If EnableEmploymentSection("m_") Then%>
                    <div class="row">
                        <div class="row-title section-heading">
                            <b>Minor Employment Information</b>        
                        </div>
                    </div>
                    <div class="row panel ViewMinorEmploymentInfo"></div>
					<div class="row" style="display: none;">
						<div class="row-title section-heading">
							<b>Minor Previous Employment Information</b>        
						</div>
					</div>
					<div class="row panel ViewMinorPrevEmploymentInfo" style="display: none;"></div>
                    <%End If %>
                    <%--END MINOR EMPLOYMENT INFORMATION --%>
                    <%End If%>
                    <%--END REVIEW MINOR INFORMATION --%>
                </div>
               
                <div id="xa_cq" style="display:<%=_visibleCustomQuestion%>">
					 <%=HeaderUtils.RenderPageTitle(0, "Please answer a few questions", true)%>
                    <uc:customQuestions ID="ucCustomQuestions" LoanType="XA" runat="server" />
                </div>
				               
                <%=HeaderUtils.RenderPageTitle(17, "Read, Sign and Submit", true)%>
                <div style="margin-top: 10px;">
                    <div id="divSubmitMessage">
					    <%If Not String.IsNullOrEmpty(_CreditPullMessage) Then%>
						    <p class="rename-able bold"><%=_CreditPullMessage%></p>
                         <%Else%>  
						    <p class="rename-able bold">Your application is not complete until you read the disclosure below and click the “I Agree” button in order to submit your application.</p>
						    <p class="rename-able">You are now ready to submit your application! By clicking on "I agree", you authorize us to verify the information you submitted and may obtain your credit report. Upon your request, we will tell you if a credit report was obtained and give you the name and address of the credit reporting agency that provided the report. You warrant to us that the information you are submitting is true and correct. By submitting this application, you agree to allow us to receive the information contained in your application, as well as the status of your application.</p>
                              
                        <%End If%>
					</div>
                    <%--<uc:disclosure ID="ucDisclosures" LoanType="XA" runat="server" Name="disclosures" />--%>                  
                </div>

				
                 <div class ="div-continue"  data-role="footer"> 
                    <a href="#"  data-transition="slide" onclick="UpdateCSRFNumber(hfCSRF); return validateForm(this);" class="btnSubmit div-continue-button" type="button" >I Agree</a>  
                   Or <a href="#" onclick="goBack(this);" class ="div-goback" data-corners="false" data-shadow="false" ><span class="hover-goback"> Go Back</span></a>   
                     <a id="href_show_last_dialog23" href="#xa_last" style="display: none;"></a>
                </div>
              </div>
             
        </div>

		<%--Joint/Co applicant--%>
        <div data-role="page" id="page6">           
            <div data-role="header" style="display: none">
                <h1>Joint Applicant Information</h1>
            </div>
            <div data-role="content" class="labeltext-bold">
                <%If _Availability = "2" Or _Availability = "2a" Or _Availability = "2b" Then%>
                    <%=HeaderUtils.RenderLogoAndSteps(_LogoUrl, 1, "OPEN")%>
                <%Else%>
                    <%=HeaderUtils.RenderLogoAndSteps(_LogoUrl, 1, "JOIN")%>
                <%End If%>
                <%=HeaderUtils.RenderPageTitle(21, "About Your Joint Applicant", false)%>
                <%=HeaderUtils.RenderPageTitle(18, "Personal Information", true)%>
                <%--<%=_OtherLogo%>--%>
                <div id="co_scanDocumentMessage" style="display: none;"><span id="co_spScanDocument" style="color: red"></span><a href="#co_scandocs"> Retake Photo</a></div>
                <uc:xaApplicantInfo ID="xaCoApplicantInfo" IDPrefix="co_" runat="server" />
                <%=HeaderUtils.RenderPageTitle(14, "Your Current Address", true)%>
                <uc:xaApplicantAddress ID="xaCoApplicantAddress" LoanType="XA" IDPrefix="co_" runat="server" />
                <%=HeaderUtils.RenderPageTitle(22, "Contact Information", true)%>
                <uc:xaApplicantContactInfo ID="xaCoApplicantContactInfo" IDPrefix="co_" runat="server" />
                <%If EnableIDSection("co_") Then%>
                    <uc:xaApplicantID ID="xaCoApplicantID" Title=" &nbsp; Identification" IDPrefix="co_" runat="server" />
                 <%End If%>
                <%If EnableEmploymentSection("co_") Then%>
                    <%--<div id="co_divEmploymentStatus">
                        <%=HeaderUtils.RenderPageTitle(17, "About Your Employment", true)%>--%>
                        <uc:xaApplicantEmployment ID="xaCoApplicantEmployment" IDPrefix="co_" runat="server" />
                     <%--</div>--%>
                <%End If %>  
                <%--<uc:DocUpload ID="ucCoDocUpload" IDPrefix="co_"  runat="server"/>--%>
				<uc:xaApplicantQuestion ID="xaCoApplicantQuestion" LoanType="XA" IDPrefix="co_" runat="server" />			
            </div>
            <div class ="div-continue"  data-role="footer">
                <a href="#"  data-transition="slide" onclick="validateActivePage(this, true);" type="button" class="div-continue-button">Continue</a> 
                <a href="#divErrorDialog" style="display: none;"></a>
               Or <a href="#" onclick="goBack(this);" class ="div-goback" data-corners="false" data-shadow="false" data-theme="reset"><span class="hover-goback"> Go Back</span></a>   
           </div>
           
        </div><%-- end Joint/Co applicant--%>
       <%--<uc:DocUploadSrcSelector ID="ucDocUploadSrcSelector" IDPrefix="" runat="server" />
		<uc:DocUploadSrcSelector ID="ucCoDocUploadSrcSelector" IDPrefix="co_" runat="server" />--%>
       
        <div data-role="page" id="walletQuestions">
            <div data-role="header" style="display: none">
                <h1>Authentication Questions</h1>
            </div>
            <div data-role="content">
               <p align="left">					
					<img id="Img1" style="border-top-width: 0px; border-left-width: 0px;
						border-bottom-width: 0px; border-right-width: 0px; max-width: 100%; max-height:150px; width:auto; height:auto;" alt="" src="../logo/americaChristianccu.jpg" />
				</p>
				 

                <%=HeaderUtils.RenderPageTitle(0, "AMERICA'S CHRISTIAN CREDIT UNION ONLINE APPLICATION", False)%>
             
				<br />
                <%=HeaderUtils.RenderPageTitle(19, "Authentication Questions", True)%>

                <div id="walletQuestionsDiv"></div>
                
				<div class="div-continue" style="margin-top: 10px; text-align: center;" data-role="footer">
                    <%--<input type="button" name="btnSubmitAnswer" value="Submit Answers" onclick="return validateWalletQuestions(this);" class="btnSubmitAnswer" autocomplete="off" />--%>
					<a href="#" data-transition="slide" onclick="return validateWalletQuestions(this);" type="button" class="btnSubmitAnswer div-continue-button">Submit Answers</a> 
                </div>
            </div>
        </div>

        <div data-role="page" id="xa_last">
			 <div data-role="header" style="display: none">
                <h1>Application Completed</h1>
            </div>
            <div data-role="content">
                <p align="left">					
					<img id="Img2" style="border-top-width: 0px; border-left-width: 0px;
						border-bottom-width: 0px; border-right-width: 0px; max-width: 100%; max-height:150px; width:auto; height:auto;" alt="" src="../logo/americaChristianccu.jpg" />
				</p>
				 

                <%=HeaderUtils.RenderPageTitle(0, "AMERICA'S CHRISTIAN CREDIT UNION ONLINE APPLICATION", False)%>
             
				<br />
               
                <div id="div_system_message">
                </div>
                <div id="divCheckMailAddress" style="text-align: left; display: none;">
                    <br />
                    <span><b>Mailing a check to fund your account? Please send the check to:</b></span>
                    <br />
                    <%=_CheckMailAddress%>
                </div>
            </div>
			<%If Not String.IsNullOrEmpty(_RedirectURL) Then%>
                <div class="div-continue" style="margin-top: 10px; text-align: center;" data-role="footer">
	                <a id="btnRedirect" href="<%=_RedirectURL%>" type="button" class="div-continue-button btn-redirect">Return to our website</a> 
                    <%--<input type="button" id="btnRedirect" class="btn-redirect div-continue-button" url='<%=_RedirectURL%>' value="Return to our website" />--%>
                </div>
                <%End If%>
        </div>


        <div id="divErrorDialog" data-role="dialog" style="min-height: 444px;">
            <div role="dialog">
                <div data-role="header" style="display:none" >
					  <h1>Alert Popup</h1>
			     </div>
                <div data-role="content">
                    <div class="DialogMessageStyle">There was a problem with your request</div>
                    <div style="margin: 5px 0 15px 0;">
                        <span id="txtErrorMessage"></span>
                    </div>
                    <a href="#" data-role="button" data-rel="back" type="button">Ok</a>
                </div>
            </div>
        </div>

		<div id="divErrorPopup" data-role="popup" data-dismissible="true" data-history="false" style="max-width: 500px;padding: 20px;">
			<div data-role="header" style="display:none" >
					<h1>Alert Popup</h1>
			</div>
			<div data-role="content">
				<%--<div class="row">
					<div class="col-xs-12 header_theme2">
						<a data-rel="back" class="pull-right"><%=HeaderUtils.IconClose %></a>
					</div>
				</div>--%>
				<div class="row">
					<div class="col-sm-12">
						<div class="DialogMessageStyle">There was a problem with your request</div>
						<div style="margin: 5px 0 15px 0;">
							<span id="txtErrorPopupMessage"></span>
						</div>       
					</div>    
				</div>
				<div class="row text-center">
					<div class="col-xs-12 text-center">
						<a href="#" data-role="button" data-rel="back" type="button">Ok</a>
					</div>
				</div>
			</div>
		</div>

        <div data-role="dialog" id="divConfirmDialog">
			<div data-role="header" style="display:none" ><h1>Restart Popup</h1></div>
			<div data-role="content">                 
				<div style='margin: 5px 0 15px 0;'><span style='color:red'>Restart will clear all data and return to the main page. Are you sure?</span></div>
				<div class='row' style="text-align: center;margin-top: 40px; margin-bottom: 10px;">
					<a href='#' type="button" style="width: 100px; display: inline;padding-left: 20px; padding-right: 20px;" data-role='button' id='btnOk'>Yes</a>
					<a href='#' type="button" style="width: 100px;display: inline;padding-left: 20px; padding-right: 20px;" data-role='button' data-rel='back' id='btnCancel'>No</a>
				</div>
			</div>
		</div>
                 
          <!--old ui footer logo -->   
        <!--
        <div class = "divfooter  ui-bar-<%=_FooterDataTheme%>" >
         <%=_strRenderFooterContact%>    
           <img  id="hdLogoImage" src="<%=_strLogoUrl%>" alt ="" style="display :none;"/>
            <div class="divfooterleft"><div class="divfooterleft-content"><%=_strFooterLeft  %></div></div>          
            <div class="divfooterright">
                <% If String.IsNullOrEmpty(_hasFooterRight) Then%>
                    <div class="divfooterright-content">
                        <div class="divfooterright-content-style"><a href="<%=_strNCUAUrl%>"  class="divfooterright-content-color ui-link" ><%=_strFooterRight%></a></div></div>
                    <div class="divfooterright-logo">
                         <div class="divfooterright-logo-style"><a href="<%=_strHUDUrl%>" class="ui-link"> <img src="<%=_strLogoUrl%>" alt ="" id="logoImage" class="footerimage-style" /></a></div></div> 
                    <div style ="clear:both"></div> 
                <%Else%>
                    <%=_strFooterRight%>
                    <div style ="clear:both"></div> 
                <%End If%>
            </div>
          <div style ="clear:both"></div>            
            </div>  
       -->
        <!--ui new footer content --> 
      <%-- <div class="divfooter">
           <%=_strRenderFooterContact%>  
            <div style="text-align :center"> 
                <div>          
                    <% If String.IsNullOrEmpty(_hasFooterRight) Then%>
                        <div><a href="<%=_strNCUAUrl%>"  class="ui-link" ><%=_strFooterRight%></a><a href="<%=_strHUDUrl%>" class="ui-link"> <img src="<%=_strLogoUrl%>" alt ="" class="footerimage-style img-responsive" /></a></div>                 
                    <% Else%>
                        <%=_strFooterRight%>
                    <%End If%>
                </div>  
                <div><%=_strFooterLeft  %></div>   
           </div>
       
       </div> --%>
         <!--end new ui footer content -->   
     </div>   
    

    <uc1:Piwik ID="ucPiwik" runat="server"></uc1:Piwik>
	<uc:pageFooter ID="ucPageFooter" runat="server" />
</body>
<script>
    var HIDDENQUESTIONS =[];
    var HOUSEHOLDNUMBER =  "<%=houseHoldNumber%>" ;
    var MEDISHARENUMBER='<%=MediShareNumber%>';
    var PRODUCTCODES ='<%=_productCodes%>';
    var HASCOAPP="";
 
    if("<%=co_DOB%>" !=""){
            HASCOAPP="Y";
        }
        function appQuestionObj(questionName,questionAnswer){
            this.q=questionName;
            this.a=questionAnswer;
        }    
        var appQuestionName ='<%=applicationQuestionName%>'.split(',');
           var appQuestionAnswer='<%=applicationQuestionAnswer%>'.split(',');
    if(appQuestionName.length>0 &&appQuestionName){
        var questionAnswer="";
        for(var i=0;i<appQuestionName.length;i++){
            if(i<appQuestionAnswer.length){
                questionAnswer=appQuestionAnswer[i];
            }
            HIDDENQUESTIONS.push(new appQuestionObj(appQuestionName[i],questionAnswer));
        }
    }
    function getDate(strDate){
        if(strDate =="")
            return strDate;
        var currDate=strDate.replace(/\D/g,'');
        currDate =currDate.substring(0,2)+"/"+currDate.substring(2,4)+"/"+currDate.substring(4,8);
        return currDate;
    }
    function safeHiddenAddress(sAddress){
        if(sAddress !=undefined){
            sAddress = sAddress.trim();
            return sAddress.replace(/\,/g,"").replace(/\./g,""); //remove . and , characters 
        }
        return "";
    }
    //set address
    function SetHiddenApplicantAddress(appInfo) {    
        appInfo.AddressStreet = safeHiddenAddress('<%=Address%>');
                appInfo.AddressZip = '<%=Zip%>';
                appInfo.AddressCity = safeHiddenAddress('<%=City%>');
                appInfo.AddressState = '<%=State%>';
                appInfo.OccupyingLocation =mappingOccupancyStatus( '<%=occupancyStatus%>');
                var year = '<%=occupancyDurationYear%>';
                var month = '<%=occupancyDurationMonth%>';
                var x = $.trim(year) == '' ? 0 : parseInt(year);
                var y = $.trim(month) == '' ? 0 : parseInt(month);
                appInfo.LiveMonths = x * 12 + y;
           if (HASCOAPP == "Y") {
                appInfo.co_AddressStreet =safeHiddenAddress('<%=co_Address%>');
                appInfo.co_AddressZip = '<%=co_Zip%>';
                appInfo.co_AddressCity = safeHiddenAddress('<%=co_City%>');
                 appInfo.co_AddressState = '<%=co_State%>';
                 appInfo.co_OccupyingLocation = mappingOccupancyStatus('<%=co_occupancyStatus%>');
                 var year = '<%=co_occupancyDurationYear%>';
                 var month = '<%=co_occupancyDurationMonth%>';
                 var x = $.trim(year) == '' ? 0 : parseInt(year);
                 var y = $.trim(month) == '' ? 0 : parseInt(month);
                 appInfo.co_LiveMonths = x * 12 + y;
             }
             return appInfo;

         }
         //end set address
         //set contact infor

         function SetHiddenContactInfo(appInfo) {
             appInfo.EmailAddr = '<%=Email%>';
            appInfo.HomePhone = '<%=HomePhone%>';
            appInfo.MobilePhone = '<%=MobilePhone%>';
            appInfo.WorkPhone = '<%=WorkPhone%>';
            appInfo.WorkPhoneEXT = '<%=WorkPhoneExt%>';
            appInfo.ContactMethod = '<%=ContactMethod%>';
            if (HASCOAPP == "Y") {
                appInfo.co_EmailAddr = '<%=co_Email%>';
                 appInfo.co_HomePhone = '<%=co_HomePhone%>';
                 appInfo.co_MobilePhone = '<%=co_MobilePhone%>';
                 appInfo.co_WorkPhone = '<%=co_WorkPhone%>';
                 appInfo.co_WorkPhoneEXT = '<%=co_WorkPhoneExt%>';
                 appInfo.co_ContactMethod = '<%=co_ContactMethod%>';
             }
             return appInfo;
         }
         //end set contact info
  
         //set employment infor
         function setHiddenApplicantEmployment(appInfo) {

             appInfo.EmploymentStatus = '<%=employmentStatus%>';
            appInfo.txtJobTitle = '<%=occupation%>';
            appInfo.txtEmployedDuration_month = '<%=employmentDurationYear%>';
            appInfo.txtEmployedDuration_year = '<%=employmentDurationYear%>';
            appInfo.txtEmployer = '<%=employer%>';
            if (HASCOAPP == "Y") {
                appInfo.co_EmploymentStatus = '<%=co_employmentStatus%>';
                 appInfo.co_txtJobTitle = '<%=co_occupation%>';
                 appInfo.co_txtEmployedDuration_month = '<%=co_employmentDurationYear%>';
                 appInfo.co_txtEmployedDuration_year = '<%=co_employmentDurationYear%>';
                 appInfo.co_txtEmployer = '<%=co_employer%>';
             }
      
             return appInfo;
         }
         //end set employment infor
            
         //set ID
         function SetHiddenApplicantID(appInfo) {
         
             appInfo.IDCardType =mappingIDCardType('<%=IDCardType%>') ;
             appInfo.IDCardNumber = '<%=IDCardNumber%>';
             appInfo.IDDateIssued = getDate('<%=IDDateIssued%>');
             appInfo.IDDateExpire = getDate('<%=IDDateExpired%>'); 
             appInfo.IDState = '<%=IDState%>';
            if (HASCOAPP == "Y") {
                appInfo.co_IDCardType =mappingIDCardType( '<%=co_IDCardType%>');
                appInfo.co_IDCardNumber = '<%=co_IDCardNumber%>';
                appInfo.co_IDDateIssued = getDate('<%=co_IDDateIssued%>');
                appInfo.co_IDDateExpire = getDate('<%=co_IDDateExpired%>'); 
                appInfo.co_IDState = '<%=co_IDState%>';
         }
     
         return appInfo;
     }// end set ID
     //set applicant infor
    function SetHiddenApplicantInfo(appInfo){
         appInfo.FirstName ='<%=FName%>';
            appInfo.MiddleName = '<%=MName%>';
            appInfo.LastName = '<%=LName%>';
            appInfo.NameSuffix = '<%=NameSuffix%>';
            appInfo.Gender = '<%=Gender%>';
            appInfo.DOB =getDate('<%=DOB%>');     
            appInfo.Citizenship = '<%=Citizenship%>';
            appInfo.SSN = '<%=SSN%>';
            appInfo.MaritalStatus = '<%=MaritalStatus%>';
         if(HASCOAPP=="Y"){    
                appInfo.co_FirstName ='<%=co_FName%>';
                appInfo.co_MiddleName = '<%=co_MName%>';
                appInfo.co_LastName = '<%=co_LName%>';
                appInfo.co_NameSuffix = '<%=co_NameSuffix%>';
                appInfo.co_Gender = '<%=co_Gender%>';
                appInfo.co_DOB =getDate('<%=co_DOB%>');     
                appInfo.co_Citizenship = '<%=co_Citizenship%>';
                appInfo.co_SSN = '<%=co_SSN%>';
                appInfo.co_MaritalStatus = '<%=co_MaritalStatus%>';
            }
            return appInfo;    
     }
    function mappingOccupancyStatus(occuStatus){
        var occStatusObject ={ "LIVEWITHPARENTS": "LIVE WITH PARENTS, LIVEWITHPARENTS",
                                "BUYING":"BUYING/OWN WITH MORTGAGE, BUYING",
                                "OWN":"OWN - FREE AND CLEAR, OWN",
                                "GOVQUARTERS":"GOVERNMENT QUARTERS, GOVQUARTERS",
                                "RENT":"RENT",
                                "OTHER" :"OTHER"};
        for (var key in occStatusObject){
            if(occStatusObject[key].indexOf(occuStatus) >-1){
                return key;
            }
        }
        return "";
    }
    function mappingIDCardType(IDCardType){
          var cardTypeObject ={  "DRIVERS_LICENSE":"DRIVERS LICENSE, DRIVERS_LICENSE",
                               "MILITARY_ID":"MILITARY_ID, MILITARY ID",
                               "PASSPORT":"PASSPORT",
                               "ALIEN_REG_CARD":"ALIEN_REG_CARD, PERMANENT RESIDENT CARD",
                               "ID_CARD": "ID_CARD, STATE ID"};
          for( var key in cardTypeObject){
              if(cardTypeObject[key].indexOf(IDCardType)>-1){
                  return key;
              }
          }
                         
        return "";
    }
        //end set applicant infor
</script>