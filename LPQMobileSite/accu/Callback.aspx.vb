﻿Imports LPQMobile.BO
Imports LPQMobile.Utils
Imports System.Xml
Imports System.Xml.Serialization
Imports System.Threading
Imports System.IO
Imports System.Net
Imports System.Web.Script.Serialization
Imports LPQMobile.Utils.Common
Imports Newtonsoft.Json
Partial Class xa_CallBack
	Inherits CBasePage

	Protected loanSubmitURL As String
	Protected decisionXAURL As String
    Protected getLoansURL As String

    '===IDA=========
    Protected getRSAQuestionsURL As String
    Protected getPIDQuestionsURL As String
    Protected getFISQuestionsURL As String
    Protected getDIDQuestionsURL As String
    Protected getEquifaxQuestionURL As String
    Protected getTIDQuestionsURL As String ''TransUnion

    Protected submitRSAAnswersURL As String
    Protected submitPIDAnswersURL As String
    Protected submitFISAnswersURL As String
    Protected submitDIDAnswersURL As String
    Protected submitEquifaxAnswerURL As String
    Protected submitTIDAnswersURL As String ''TransUnion

    '==Debit====
    Protected eFundsURL As String
    Protected RetailBankingURL As String
    Protected DeluxeDebitURL As String
    Protected ExperianEWSURL As String

    Protected creditPullURL As String

    Protected fundDepositsURL As String

    Protected symitarBookingURL As String
	Protected DNABookingURL As String
	Protected CohesionBookingURL As String ' use CBookCore, Cohesion / FIS Miser
    '
    Protected Const constLoanSubmitURL As String = "/SubmitLoan/SubmitLoan.aspx"
    'Protected Const constLoanSubmitURL As String = "/decisionloan/decisionloan.aspx"
    Protected Const constGetLoansURL As String = "/GetLoans/GetLoans.aspx"
    Protected Const constDecisionXAURL As String = "/decisionXA/decisionXA.aspx"
    '=====IDA==========
    'RSA
    Protected Const constGetRSAQuestionsURL As String = "/rsa/getquestions.aspx"
    Protected Const constSubmitRSAAnswersURL As String = "/rsa/submitanswers.aspx"
    'PID
    Protected Const constGetPIDQuestionsURL As String = "/PreciseID/GetQuestions.aspx"
    Protected Const constSubmitPIDAnswersURL As String = "/PreciseID/SubmitAnswers.aspx"
    'FIS
    Protected Const constGetFISQuestionsURL As String = "/eFunds/GetQuestions.aspx"
    Protected Const constSubmitFISAnswersURL As String = "/eFunds/SubmitAnswers.aspx"
    'DID
    Protected Const constGetDIDQuestionsURL As String = "/DeluxeIDA/GetQuestions.aspx"
    Protected Const constSubmitDIDAnswersURL As String = "/DeluxeIDA/SubmitAnswers.aspx"
    'EID
    Protected Const constGetEquifaxQuestionsURL As String = "/EquifaxInterConnect/GetQuestions.aspx"
    Protected Const constSubmitEquifaxAnswersURL As String = "/EquifaxInterConnect/SubmitAnswers.aspx"
    'TID - TransUnion
    Protected Const constGetTIDQuestionsURL As String = "/IDMA/GetQuestions.aspx"
    Protected Const constSubmitTIDAnswersURL As String = "/IDMA/SubmitAnswers.aspx"
    '===Debit===
    'DID
    Protected Const constDeluxeDebitURL As String = "/DeluxeDetection/DeluxeDetect.aspx"
    'EID
    Protected Const constRetailBankingURL As String = "/RetailBanking/RetailBanking.aspx"
    'FIS , telecheck
    Protected Const constEFundsURL As String = "/eFunds/eFunds.aspx"
    'EWS , Experian preciseID EWS
    Protected Const constExperianEWSURL As String = "/PreciseID/EWS.aspx"

    Protected Const constCreditPullURL As String = "/pullCreditReport/pullCreditReport.aspx"

    'fund deposits 
    Protected Const constFundDepositsURL As String = "/funddeposits/funddeposits.aspx"

    'symitar booking
    Protected Const constSymitarBookingURL As String = "/SymitarBookService/XABook.aspx"

    Protected Const constDNABookingURL As String = "/bookapp/bookapp.aspx"

    ''Protected Const constCohesionBookingURL As String = "/Integration/Cohesion/BookXAApp/BookXAApp.aspx"	 'Cohesion / FIS Miser


    '
    Protected customErrorMessage As String = "There was an error while submitting your application. <div id='MLerrorMessage'></div>"
    Protected customPreapprovedSuccessMessage As String = "Your application has been preapproved.  We will contact you shortly."
	Protected customSubmittedSuccessMessage As String = "Thank you for your application."
	Protected customDeclinedMessage As String = "Thank you for your application."
    Protected loanType As String = "XA_LOAN"
    Protected customSubmittedDataList As List(Of String) = New List(Of String)
	Protected customPreapprovedDateList As List(Of String) = New List(Of String)
	Protected customDeclinedDateList As List(Of String) = New List(Of String)

    Private _IsSecondary As Boolean = False

    Private _bIsFraud As Boolean = False    'set by updateLoan sub,  true will use sucessfully submitted message
    Private _bIsFailPreUnderWrite As Boolean = False
    Private _bIsUpdateReferralStatusRequired As Boolean = True ' other process should set this to fail if don't want to update status to referred
    Private _sInternalCommentAppend As String

#Region "property that need to persist"
    ''need to use for 2nsd postback
    Property loanRequestXMLStr As String
        Get
            Return Common.SafeString(Session("loanRequestXMLStr"))
        End Get
        Set(value As String)
            Session("loanRequestXMLStr") = value
        End Set
	End Property

	'this will store the original new loan request so can get the funding source bc getloan will modified fundingsource in get loan response
	Property newLoanRequestXMLStr As String
		Get
			Return Common.SafeString(Session("newLoanRequestXMLStr"))
		End Get
		Set(value As String)
			Session("newLoanRequestXMLStr") = value
		End Set
	End Property

    Property loanResponseXMLStr As String
        Get
            Return Common.SafeString(Session("loanResponseXMLStr"))
        End Get
        Set(value As String)
            Session("loanResponseXMLStr") = value
        End Set
    End Property

    'need to store this for comparision with answer in the next post back
    Property questionList_persist As List(Of WalletQuestionRendering.questionDataClass)
        Get
            If Session("questionList") IsNot Nothing Then
                Return CType(Session("questionList"), System.Collections.Generic.List(Of WalletQuestionRendering.questionDataClass))
            End If
            Return Nothing
        End Get
        Set(value As List(Of WalletQuestionRendering.questionDataClass))
            Session("questionList") = value
        End Set
    End Property

    Property IsJointApplication As Boolean
        Get
            If Common.SafeString(Session("IsJointApplication")) = "Y" Then
                Return True
            Else
                Return False
            End If

        End Get
        Set(value As Boolean)
            If value = True Then
                Session("IsJointApplication") = "Y"
            Else
                Session("IsJointApplication") = Nothing
            End If


        End Set
    End Property
    Property Co_FName As String
        Get
            Return Common.SafeString(Session("co_FirstName"))
        End Get
        Set(value As String)
            Session("co_FirstName") = value
        End Set
    End Property
    Property transaction_ID As String 'same as session_id
        Get
            Return Common.SafeString(Session("transaction_id"))
        End Get
        Set(value As String)
            Session("transaction_id") = value
        End Set
    End Property

    Property question_set_ID As String
        Get
            Return Common.SafeString(Session("question_set_ID"))
        End Get
        Set(value As String)
            Session("question_set_ID") = value
        End Set
    End Property 'same as quiz_id

    Property request_app_ID As String
        Get
            Return Common.SafeString(Session("request_app_ID"))
        End Get
        Set(value As String)
            Session("request_app_ID") = value
        End Set
    End Property
    Property request_client_ID As String
        Get
            Return Common.SafeString(Session("request_client_ID"))
        End Get
        Set(value As String)
            Session("request_client_ID") = value
        End Set
    End Property
    Property request_sequence_ID As String
        Get
            Return Common.SafeString(Session("request_sequence_ID"))
        End Get
        Set(value As String)
            Session("request_sequence_ID") = value
        End Set
    End Property

    Property loanID As String
        Get
            Return Common.SafeString(Session("loanID"))
        End Get
        Set(value As String)
            Session("loanID") = value
        End Set
    End Property
    Property loanNumber As String
        Get
            Return Common.SafeString(Session("loanNumber"))
        End Get
        Set(value As String)
            Session("loanNumber") = value
        End Set
    End Property
    Property reference_number As String
        Get
            Return Common.SafeString(Session("reference_number"))
        End Get
        Set(value As String)
            Session("reference_number") = value
        End Set
    End Property

    Property isLive As String
        Get
            Return Common.SafeString(Session("isLive"))
        End Get
        Set(value As String)
            Session("isLive") = value
        End Set
    End Property

    '' ''testing ida questions for joint
    ''Property testWalletQuestionSecondPostback As String
    ''    Get
    ''        Return Common.SafeString(Session("testWalletQuestionSecondPostback"))
    ''    End Get
    ''    Set(value As String)
    ''        Session("testWalletQuestionSecondPostback") = value
    ''    End Set
    ''End Property

#End Region
    
    Private Shared _threadLock_ As New Object
    Private Shared _dicAppInfo As New Dictionary(Of String, String)

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
		'log.Info("XA: Execute callback")

        'Validate token for cross site request forgery, mitigate only
        Dim rolledToken = Request.Form("CSRF")
        If Not ValidateToken(rolledToken) Then
            _bIsUpdateReferralStatusRequired = False
            log.Info("Invalid Token")
            Dim errMessage = "<b><span style='color: red'>For security reason, your session has timed out. Please restart your browser.</span></b><br/>"
            Response.Write(errMessage)
            Return
        End If

        _ReferralSource = Common.SafeString(Request.Form("ReferralSource"))
        _IsSecondary = Request.Form("TypeAccount") = "2"

        If SafeString(Request.Form("IsJoint")).ToLower = "" Then  'don't know why the value is empty on authentication question submit, so keep previous value
        ElseIf "true" = SafeString(Request.Form("IsJoint")).ToLower Then
            IsJointApplication = True
        Else
            IsJointApplication = False
        End If

        ' ''temp process for testing purpose(1k of hit)
        ''If _CurrentLenderRef = "bcu_test" Then
        ''    Common.getCustomResponseMessages(loanType, customErrorMessage, customPreapprovedSuccessMessage, customSubmittedSuccessMessage, customSubmittedDataList, customPreapprovedDateList)
        ''    customSubmittedSuccessMessage = buildNonApprovedMessage(customSubmittedSuccessMessage, customSubmittedDataList, Nothing)
        ''    Response.Write(customSubmittedSuccessMessage)
        ''    log.Info("XA: Execute BYpass callback")
        ''    Return
        ''End If


        Dim IP_ErrMessage As String = ValidateIP()
        If Not String.IsNullOrEmpty(IP_ErrMessage) Then
            Response.Write(IP_ErrMessage)
            log.Info(IP_ErrMessage & "  IP: " & Request.UserHostAddress & "/" & _CurrentLenderRef)
            Return
        End If

        ' Prepare the various URLs
        Dim baseSubmitURL = _CurrentWebsiteConfig.BaseSubmitLoanUrl

        Dim concatURL As String
        concatURL = baseSubmitURL + constLoanSubmitURL
        loanSubmitURL = concatURL

        concatURL = baseSubmitURL + constDecisionXAURL
        decisionXAURL = concatURL

        concatURL = baseSubmitURL + constGetRSAQuestionsURL
        getRSAQuestionsURL = concatURL

        concatURL = baseSubmitURL + constGetPIDQuestionsURL
        getPIDQuestionsURL = concatURL

        concatURL = baseSubmitURL + constSubmitRSAAnswersURL
        submitRSAAnswersURL = concatURL

        concatURL = baseSubmitURL + constSubmitPIDAnswersURL
        submitPIDAnswersURL = concatURL

        concatURL = baseSubmitURL + constGetLoansURL
        getLoansURL = concatURL

        concatURL = baseSubmitURL + constEFundsURL
        eFundsURL = concatURL

        concatURL = baseSubmitURL + constCreditPullURL
        creditPullURL = concatURL

        concatURL = baseSubmitURL + constGetEquifaxQuestionsURL
        getEquifaxQuestionURL = concatURL

        concatURL = baseSubmitURL + constSubmitEquifaxAnswersURL
        submitEquifaxAnswerURL = concatURL

        concatURL = baseSubmitURL + constGetFISQuestionsURL
        getFISQuestionsURL = concatURL

        concatURL = baseSubmitURL + constSubmitFISAnswersURL
        submitFISAnswersURL = concatURL

        concatURL = baseSubmitURL + constGetDIDQuestionsURL
        getDIDQuestionsURL = concatURL

        concatURL = baseSubmitURL + constSubmitDIDAnswersURL
        submitDIDAnswersURL = concatURL

        concatURL = baseSubmitURL + constDeluxeDebitURL
        DeluxeDebitURL = concatURL

        concatURL = baseSubmitURL + constDeluxeDebitURL
        DeluxeDebitURL = concatURL

        ''retailbanking
        concatURL = baseSubmitURL + constExperianEWSURL
        ExperianEWSURL = concatURL

        'funding
        concatURL = baseSubmitURL + constFundDepositsURL
        fundDepositsURL = concatURL

        'booking()
        concatURL = baseSubmitURL + constSymitarBookingURL
        symitarBookingURL = concatURL

        'DNA booking()
        concatURL = baseSubmitURL + constDNABookingURL
		DNABookingURL = concatURL

        ''concatURL = baseSubmitURL + constCohesionBookingURL
        ''CohesionBookingURL = concatURL

        ''TransUnion
        concatURL = baseSubmitURL + constGetTIDQuestionsURL
        getTIDQuestionsURL = concatURL

        concatURL = baseSubmitURL + constSubmitTIDAnswersURL
        submitTIDAnswersURL = concatURL

        ' Get the custom error and success messages
        ' Before we get the custom response messages, we should set the default messages just in case we can't read the values from the XML config file
		Common.GetCustomResponseMessages(_CurrentWebsiteConfig, loanType, customErrorMessage, customPreapprovedSuccessMessage, customSubmittedSuccessMessage, customDeclinedMessage, customSubmittedDataList, customPreapprovedDateList, customDeclinedDateList)

        ''Flow for LoanStatusEnum <> ""
        '' 1)PENDING -> 2) REFFERAL(fail PreUnderWrite)/REFFERAL(fail underwrite,status is updated in page_loadComplete sub)  /INSTANTAPPROVE

        ''flow for LoanStatusEnum == ""
        ''1)NEW -> 2) REFFERAL

        '' FYI: switching of status is need to trigger realtime automated action on lender side

        If (Request.Form("Task") = "SubmitLoan") Then '          
            Dim errMessage As String = ValidateInput()
            If Not String.IsNullOrEmpty(errMessage) Then
                Response.Write(errMessage)
                Return
            End If

            If Not PreUnderWrite() Then  'fail pre underwrite
                _bIsFailPreUnderWrite = True  'need thisfor escaping the loop later
                _bIsUpdateReferralStatusRequired = False ' need this to escape status update in Page_loadComplete sub
                If Not CreateNewLoan() Then  'set status to REFERRAL and add comment(fail preunderwrite) to intern decision 
                    Response.Write(customErrorMessage) ' Was not succesful, so don't bother trying the next step
                    Return
                End If
            ElseIf Not CreateNewLoan() Then
                _bIsUpdateReferralStatusRequired = False ' need this to escape status update in Page_loadComplete sub
                Response.Write(customErrorMessage) ' Was not succesful, so don't bother trying the next step
                Return
            End If
            'don't because the status is already available in the app
            setLoanID(loanResponseXMLStr)

            ' After having built the XML and receiving the response with LOAN_ID, we can build the custom non-approved success message
            Dim requestXML As XmlDocument = New XmlDocument()
            requestXML.LoadXml(loanRequestXMLStr)
            Dim loanXML As XmlDocument = New XmlDocument()
            loanXML.LoadXml(requestXML.InnerText)
            loanNumber = Common.getLoanNumber(loanResponseXMLStr)
            ''make sure loanNumber is not empty
            If Not String.IsNullOrEmpty(loanNumber) Then
                ''execute uploadDocument 
                Try
                    If executeUpLoadDocument() Then
                        log.Info("XA: upload document sucessfull")
                    End If
                Catch ex As Exception
                    log.Error("XA: upload document failed:", ex)
                End Try

            End If


            customSubmittedSuccessMessage = Common.buildNonApprovedMessage(customSubmittedSuccessMessage, customSubmittedDataList, loanXML, loanResponseXMLStr)
            customSubmittedSuccessMessage = PrependIconForNonApprovedMessage(customSubmittedSuccessMessage, "Thank You")

            customPreapprovedSuccessMessage = Common.buildNonApprovedMessage(customPreapprovedSuccessMessage, customPreapprovedDateList, loanXML, loanResponseXMLStr)
            customPreapprovedSuccessMessage = PrependIconForNonApprovedMessage(customPreapprovedSuccessMessage, "Congratulations")

            ''decode Message before rendering
            customSubmittedSuccessMessage = Server.HtmlDecode(customSubmittedSuccessMessage)
            customPreapprovedSuccessMessage = Server.HtmlDecode(customPreapprovedSuccessMessage)

            If _bIsFailPreUnderWrite Then
                Response.Write(customSubmittedSuccessMessage)
                Return
            End If

            'check for bad member enable attribute and run result
            Dim isBadMember As Boolean = False
            If _CurrentWebsiteConfig.LenderCodeBadMember <> "" Then
                Dim SSNList As New System.Collections.Generic.List(Of String)
                If Request.Form("SSN") <> "" Then
                    SSNList.Add(Request.Form("SSN"))
                End If
                If Request.Form("co_SSN") <> "" Then
                    SSNList.Add(Request.Form("co_SSN"))
                End If
                isBadMember = Common.GetBadMember(SSNList, _CurrentWebsiteConfig)
            End If
            If isBadMember Then
                _sInternalCommentAppend += "Found Bad Member.  "
                Response.Write(customSubmittedSuccessMessage)
                Return
            End If


            'get filtered products 
			Dim settingProductsList As List(Of CProduct) = CProduct.GetProducts(_CurrentWebsiteConfig, "1")

            Dim paypalReceipt As New List(Of CPayPalReceiptItem)

            ''for ida authentication
            Dim bIDA_LPQConfig As Boolean = False
            Dim idaIndex As Integer
            If settingProductsList.Count > 0 Then
                For idaIndex = 0 To settingProductsList.Count - 1
                    ''only check Primary IDA 
                    If settingProductsList(idaIndex) IsNot Nothing AndAlso settingProductsList(idaIndex).AutoPullIDAuthenticationConsumerPrimary Then
                        bIDA_LPQConfig = True
                        Exit For
                    End If
                Next
            End If

            Dim bIDA_LPQMobileWebsiteConfig As Boolean = (SafeString(_CurrentWebsiteConfig.AuthenticationType) <> "")
            If bIDA_LPQConfig And bIDA_LPQMobileWebsiteConfig Then
                'ida authentication
                Session("isJoinQuestionDone") = Nothing 'initialize just in case the process was abondaon the last time
                Dim sAuthMessage As String = ""
                Dim sWalletQuestions As String = ""
                Dim bExecWalletQuestions As Boolean = ExecWalletQuestions(sAuthMessage, sWalletQuestions) 'check for attribute "ida" in LPQMobileWebsite config
                If Not bExecWalletQuestions And sAuthMessage = "" Then
                    'Response.Write(customErrorMessage) 'submit result with error so stop and return error msg
                    'don 't want to sent the error message to consumer to avoid mulitple submission
                    _sInternalCommentAppend += "IDA not successfully run.  "
                    Response.Write(customSubmittedSuccessMessage)   '
                    ExecutePostDataToSaleForce(getLoanResponse(), "", True)
                    Return
                End If
                If Not bExecWalletQuestions And sAuthMessage <> "" Then    'stop and return failed message - can't locate person
                    Response.Write(customSubmittedSuccessMessage)
                    _sInternalCommentAppend += "IDA not successfully run.  "
                    ExecutePostDataToSaleForce(getLoanResponse(), "", True)
                    Return
                End If
                If sWalletQuestions <> "" Then 'stop and return wallet question
                    Dim sSessionID As String = Guid.NewGuid().ToString() ''generate unique sessionID
                    If sWalletQuestions.Contains("walletquestion") Then

                        ''testing IDA questions for joint 
                        ''Session("testWalletQuestionSecondPostback") = sWalletQuestions

                        sWalletQuestions += "<input type='hidden' id='hdSessionID' value ='" & sSessionID & "'/>"
                        SaveAppInfo(sSessionID)

                        Dim oThread As Thread
                        oThread = New Thread(DirectCast(Sub() updateIncompleteApp(sSessionID), ThreadStart))
                        oThread.Start()

                    End If
                    Response.Write(sWalletQuestions)
                    _bIsUpdateReferralStatusRequired = False 'don't to update loanstatus before user answer authenticate question
                    Return

                End If

                'if bExecWalletQuestions and sAuthMessage <> "" 
                '**IDA not require by Lender  so go on
            End If



            '========Debit=============
            ''for new format Debit
            Dim bDebitLPQConfig As Boolean = False
            Dim dIndex As Integer
            If settingProductsList.Count > 0 Then
                For dIndex = 0 To settingProductsList.Count - 1
                    If (settingProductsList(dIndex) IsNot Nothing AndAlso settingProductsList(dIndex).AutoPullDebitConsumerPrimary And Not _IsSecondary) Or _
                    (settingProductsList(dIndex) IsNot Nothing AndAlso settingProductsList(dIndex).AutoPullDebitConsumerSecondary And _IsSecondary) Then
                        bDebitLPQConfig = True
                        Exit For
                    End If
                Next
            End If

            Dim bDebitLPQMobileWebsiteConfig As String = SafeString(_CurrentWebsiteConfig.DebitType).ToUpper
            If bDebitLPQConfig And bDebitLPQMobileWebsiteConfig = "FIS" Then '' -->> for efund
                Dim sEfundMessage As String = ""
                If Not ExecuteEfund(sEfundMessage) Then
                    'Response.Write(customErrorMessage)	'submitt error
                    'don't want to sent the error message to consumer to avoid mulitple submission
                    _sInternalCommentAppend += "Debit not successfully run.  "
                    Response.Write(customSubmittedSuccessMessage)
                    Return
                End If
                If sEfundMessage = "ACCEPT" Then    '
                    Response.Write(customPreapprovedSuccessMessage)
                Else       'Refer or failed, abandon other steps
                    _sInternalCommentAppend += "Debit not successfully run.  "
                    Response.Write(customSubmittedSuccessMessage)
                    Return
                End If
            ElseIf bDebitLPQConfig And bDebitLPQMobileWebsiteConfig = "EID" Then  '' --> for retailbanking
                Dim sRBankingMessage As String = ""
                If Not ExecuteRetailBanking(sRBankingMessage) Then
                    _sInternalCommentAppend += "Debit not successfully run.  "
                    Response.Write(customSubmittedSuccessMessage)
                    Return
                End If
                If sRBankingMessage = "ACCEPT" Then    '
                    Response.Write(customPreapprovedSuccessMessage)
                Else       'Refer or failed, abandon other steps
                    _sInternalCommentAppend += "Debit not successfully run.  "
                    Response.Write(customSubmittedSuccessMessage)
                    Return
                End If
            ElseIf bDebitLPQConfig And bDebitLPQMobileWebsiteConfig = "DID" Then  '' --> for Deluxe detec
                Dim oDeluxe As CDeluxe = New CDeluxe()
                If Not oDeluxe.Execute(loanID, IsJointApplication, _CurrentWebsiteConfig, DeluxeDebitURL) Then 'Refer or failed, abandon other steps
                    _sInternalCommentAppend += "Debit not successfully run.  "
                    Response.Write(customSubmittedSuccessMessage)
                    Return
                Else '"PROCEED"
                    Response.Write(customPreapprovedSuccessMessage)
                End If
            ElseIf bDebitLPQConfig And bDebitLPQMobileWebsiteConfig = "EWS" Then  '' --> for Experian Precise EWS
                Dim oExperianEWS As CExperianEWS = New CExperianEWS()
                If Not oExperianEWS.Execute(loanID, IsJointApplication, _CurrentWebsiteConfig, ExperianEWSURL) Then 'Refer or failed, abandon other steps
                    _sInternalCommentAppend += "Debit not successfully run.  "
                    Response.Write(customSubmittedSuccessMessage)
                    Return
                Else '"PROCEED"
                    Response.Write(customPreapprovedSuccessMessage)
                End If
            Else '**efund not required, 

                'this is just the placeholder to be used if the follow process doesn't overwrite it
                Response.Write(customPreapprovedSuccessMessage) 'need at least one response in this "if" loop per postback
            End If


            ''pull credit or do decisionXA
            Dim isCreditPull As Boolean = False
            Dim cIndex As Integer
            If settingProductsList.Count > 0 Then
                For cIndex = 0 To settingProductsList.Count - 1
                    If (settingProductsList(cIndex) IsNot Nothing AndAlso settingProductsList(cIndex).AutoPullCreditsConsumerPrimary And Not _IsSecondary) Or _
                    (settingProductsList(cIndex) IsNot Nothing AndAlso settingProductsList(cIndex).AutoPullCreditsConsumerSecondary And _IsSecondary) Then
                        isCreditPull = True
                        Exit For
                    End If
                Next
            End If

            If isCreditPull And Not _CurrentWebsiteConfig.IsDecisionXAEnable Then
                If Not ExecuteCreditPull() Then
                    _sInternalCommentAppend += "Credit pull not successfully run.  "
                    Response.Clear()
                    Response.Write(customSubmittedSuccessMessage)
                    Return
                End If
            End If

            ''underwrite XA product, 
            ''for backward compatiblity, this feature need to be enabled for each cu based on available of lender side live code
            ''TODO: don't know how this will effect funding since it is selected before prodcut is approved
            If _CurrentWebsiteConfig.IsDecisionXAEnable Then
                Dim sXMLresponse = ExecuteDecisionXA()
                If Not isXAProductQualified(sXMLresponse) Then
                    Response.Clear()
                    Response.Write(customSubmittedSuccessMessage)
                    Return
                End If
            End If

            '
            ' We passed the authentication phase.
            ' If the application was approved, then we need to update the status of the application on the server
            ' We send the request again, but this time we list the approved products under the APPROVED category
            ' First, we have to do a "getloans" to get the loan data, then we make the changes to the loan data and 
            ' resubmit it back to the server with the "update" command
            If _CurrentWebsiteConfig.LoanStatusEnum.ToUpper() <> "" Then
                updateLoan(_CurrentWebsiteConfig.LoanStatusEnum.ToUpper())
                _bIsUpdateReferralStatusRequired = False
            End If

            If _bIsFraud Then
                Response.Clear()
                Response.Write(customSubmittedSuccessMessage)
                Return
            End If

            ''put it here so we don't have to deal with Fraud case and persist customSubmittedSuccessMessage, no need to update status
            '' --------------- PAYPAL funding ---------------------------------------------------------------------------------------''
            '' Application is Approved or InstantApproved and isPayPalFunding then execute paypal process
            '' Call getloan and updateloan to update funding status and internal comments.
            ''execute booking for symitar if enabled (done in MLPayPal.aspx)
            '' check PAYPAL email is available
            If Not String.IsNullOrEmpty(_CurrentWebsiteConfig.PayPalEmail) Then
                '' and also selected funding source is PAYPAL
                Dim isPayPalFunding = Request.Params("SelectedFundingSource") = "PAYPAL"
                If isPayPalFunding Then
                    '' Redirect to MLPayPal.aspx to process PayPal payment
                    Session("PAYPAL_RECEIPT") = paypalReceipt
                    Session("APPROVED_MESSAGE") = customPreapprovedSuccessMessage
                    '' build redirect url and params
                    '' Note: Since MLPayPal.aspx page inherits from CBasePage so it requires lenderref value to get website config data
                    Dim applicantEmail = Request.Form("EmailAddr")
                    Dim redirURL = String.Format("~/MLPayPal.aspx?lenderref={0}&sender={1}", _CurrentLenderRef, applicantEmail)
                    Response.Redirect(redirURL, True) '' End the response here, all codes below this line will be ignored. go directly to MLPay on server side
                End If
                '' --------------- END PAYPAL funding ---------------------------------------------------------------------------------------''
            End If
            If _CurrentWebsiteConfig.Booking.ToUpper() <> "" Then

                Dim sMemberNumber As String = CBookingManager.Book(_CurrentWebsiteConfig, loanID, "XA")
                Dim sLoanResponse As String = getLoanResponse()
                ''post data to salesforce
                ExecutePostDataToSaleForce(sLoanResponse, sMemberNumber)

                If Not String.IsNullOrEmpty(sMemberNumber) Then ' update preapproved message with membernumber
                    'Common.getCustomResponseMessages(_CurrentWebsiteConfig, loanType, customErrorMessage, customPreapprovedSuccessMessage, customSubmittedSuccessMessage, customDeclinedMessage, customSubmittedDataList, customPreapprovedDateList, customDeclinedDateList)
                    'customPreapprovedSuccessMessage = Common.buildNonApprovedMessage(customPreapprovedSuccessMessage, customPreapprovedDateList, loanXML, loanResponseXMLStr, sAccountNumber)

                    _bIsUpdateReferralStatusRequired = False

                    customPreapprovedSuccessMessage = getCustomPreApprovedMessages(sLoanResponse, sMemberNumber)
                    customPreapprovedSuccessMessage = PrependIconForNonApprovedMessage(customPreapprovedSuccessMessage, "Congratulations")
                    ''decode Message before rendering
                    customPreapprovedSuccessMessage = Server.HtmlDecode(customPreapprovedSuccessMessage)

                    Response.Clear()
                    Response.Write(customPreapprovedSuccessMessage)
                    Return
                Else
                    _bIsUpdateReferralStatusRequired = True '' can not book --> need to update status to referred
                    Response.Clear()
                    Response.Write(customSubmittedSuccessMessage)
                    Return
                End If
            End If
        End If

        'execute for id authentication answer submission
        If (Request.Form("Task") = "WalletQuestions") Then

            Dim strAppInforObj = ""
            ''check completing IDA questions for primary
            ''if IDA questions are completed then remove the data info from _dicAppInfo
            Dim sSessionID As String = SafeString(Request.Params("CurrentSessionID"))
            If Not String.IsNullOrEmpty(sSessionID) Then

                If (_dicAppInfo.ContainsKey(sSessionID)) Then
                    If IsJointApplication Then
                        ''save current data for Joint
                        strAppInforObj = _dicAppInfo.Item(sSessionID)
                    End If

                    SyncLock _threadLock_
                        _dicAppInfo.Remove(sSessionID)
                    End SyncLock
                Else
                    ''session expired 
                    Response.Write(Server.HtmlDecode(customSubmittedSuccessMessage & "<div id='expiredMessage'></div>"))
                    _bIsUpdateReferralStatusRequired = False
                    Return
                End If

            End If   '' check completing IDA questions for primary 

            ''check completing IDA questions for joint 
            ''if IDA questions are completed then remove the data info from _dicAppInfo
            Dim co_sSessionID As String = SafeString(Request.Params("co_CurrentSessionID"))
            If Not String.IsNullOrEmpty(co_sSessionID) Then
                If (_dicAppInfo.ContainsKey(co_sSessionID)) Then
                    SyncLock _threadLock_
                        _dicAppInfo.Remove(co_sSessionID)
                    End SyncLock
                Else
                    ''session expired 
                    Response.Write(Server.HtmlDecode(customSubmittedSuccessMessage & "<div id='expiredMessage'></div>"))
                    _bIsUpdateReferralStatusRequired = False
                    Return
                End If
            End If   '' end check completing IDA questions for joint  

            _bIsUpdateReferralStatusRequired = True

            ' After having built the XML and receiving the response with LOAN_ID, we can build the custom non-approved success message
            Dim requestXML As XmlDocument = New XmlDocument()
            requestXML.LoadXml(loanRequestXMLStr)
            Dim loanXML As XmlDocument = New XmlDocument()
            loanXML.LoadXml(requestXML.InnerText)

            customSubmittedSuccessMessage = Common.buildNonApprovedMessage(customSubmittedSuccessMessage, customSubmittedDataList, loanXML, loanResponseXMLStr)
            customSubmittedSuccessMessage = PrependIconForNonApprovedMessage(customSubmittedSuccessMessage, "Thank You")

            customPreapprovedSuccessMessage = Common.buildNonApprovedMessage(customPreapprovedSuccessMessage, customPreapprovedDateList, loanXML, loanResponseXMLStr)
            customPreapprovedSuccessMessage = PrependIconForNonApprovedMessage(customPreapprovedSuccessMessage, "Congratulations")

            ''decode Message before rendering
            customSubmittedSuccessMessage = Server.HtmlDecode(customSubmittedSuccessMessage)
            customPreapprovedSuccessMessage = Server.HtmlDecode(customPreapprovedSuccessMessage)
            ' The second time this page is visited will be for the purpose of submitting the answers to wallet questions
            ' Validate the answers then format the answers into proper XML and submit!

            'Submit the answers
            Dim sAnswerValidate As String = ""
            Dim sWalletMessage As String = ""

            If Not ExecWalletAnswers(sAnswerValidate, sWalletMessage) Then
                'Response.Write(customErrorMessage)	'submit erro
                'don't want to sent the error message to consumer to avoid mulitple submission
                _sInternalCommentAppend += "IDA not successfully run.  "
                Response.Write(customSubmittedSuccessMessage)
                ExecutePostDataToSaleForce(getLoanResponse(), "", True)
                Return
            End If
            If sAnswerValidate <> "" Then
                Response.Write(sAnswerValidate) 'incomplete answer
                ExecutePostDataToSaleForce(getLoanResponse(), "", True)
                Return
            End If

            If sWalletMessage <> "" Then
                If sWalletMessage.Contains("walletquestion") Then 'follow-on question for deluxe
                    Response.Write(sWalletMessage)
                    _bIsUpdateReferralStatusRequired = False 'don't to update loanstatus before user answer authenticate question
                    Return
                End If
                _sInternalCommentAppend += "IDA not successfully run.  "
                Response.Write(sWalletMessage)  'fail autehntication '
                ExecutePostDataToSaleForce(getLoanResponse(), "", True)
                Return
            End If

            ''run IDA for join
            'dont need to check for isIDAEnabled bc by design this else statement is executed when  IDA is enabled,
            If (IsJointApplication = True And Session("isJoinQuestionDone") = Nothing) Then
                Session("isJoinQuestionDone") = "False"
                ''ida authentication
                Dim sAuthMessage As String = ""
                Dim sWalletQuestions As String = ""
                Dim bExecWalletQuestions As Boolean = ExecWalletQuestions(sAuthMessage, sWalletQuestions)

                '' ''testing IDA questions for joint
                ''Dim bExecWalletQuestions As Boolean = True
                ''sWalletQuestions = testWalletQuestionSecondPostback
                ''log.Info("render second question" & testWalletQuestionSecondPostback)
                ''bExecWalletQuestions = True
                '' ''end testing IDA questions for joint

                If Not bExecWalletQuestions And sAuthMessage = "" Then
                    'Response.Write(customErrorMessage) 'submit result with error so stop and return error msg
                    'don't want to sent the error message to consumer to avoid mulitple submission
                    _sInternalCommentAppend += "IDA not successfully run.  "
                    Response.Write(customSubmittedSuccessMessage)
                    Session("isJoinQuestionDone") = Nothing '
                    ExecutePostDataToSaleForce(getLoanResponse(), "", True)
                    Return
                End If
                If Not bExecWalletQuestions And sAuthMessage <> "" Then    'stop and return failed message - can't locate person
                    _sInternalCommentAppend += "IDA not successfully run.  "
                    Response.Write(customSubmittedSuccessMessage)
                    Session("isJoinQuestionDone") = Nothing
                    ExecutePostDataToSaleForce(getLoanResponse(), "", True)
                    Return
                End If

                If sWalletQuestions <> "" Then 'stop and return wallet question 
                    If sWalletQuestions.Contains("walletquestion") Then
                        Dim coAppSessionID As String = Guid.NewGuid().ToString() ''generate unique sessionID for joint
                        sWalletQuestions += "<input type='hidden' id='co_hdSessionID' value ='" & coAppSessionID & "'/>"

                        ''save AppInfo for Joint
                        If Not _dicAppInfo.ContainsKey(coAppSessionID) Then
                            _dicAppInfo.Add(coAppSessionID, strAppInforObj)
                        End If

                        Dim oThread As Thread
                        oThread = New Thread(DirectCast(Sub() updateIncompleteApp(coAppSessionID, "JOINT: "), ThreadStart))
                        oThread.Start()

                    End If
                    Response.Write(sWalletQuestions)
                    'need to set this in submitAnswer so authentication will not run
                    _bIsUpdateReferralStatusRequired = False 'don't to update loanstatus before user answer authenticate question
                    Return
                End If

                'if bExecWalletQuestions and sAuthMessage <> "" 
                '**IDA not require by Lender  so go on
                'need to modify EIDA process
            End If
            Session("isJoinQuestionDone") = Nothing



            'if sAnswerValidate="" and sWalletMessage=""  '***pass then go on 
            ' There are no errors, so check to see if PID or RSA was approved. 
            ' If so, proceed 

            ''new format can have more than 3 products
            Dim oSerializer As New JavaScriptSerializer()
            Dim oProductsList As New List(Of SelectProducts)
            If Not String.IsNullOrEmpty(Request.Params("SelectedProducts")) Then
                oProductsList = oSerializer.Deserialize(Of List(Of SelectProducts))(Request.Params("SelectedProducts"))
            End If

            Dim paypalReceipt As New List(Of CPayPalReceiptItem)

            ''create settingProductsList instead of settingProduct1, settingProduct2, and settingProduct3
            Dim settingProductsList As New List(Of CProduct)
            Dim pNum As Integer
            If oProductsList.Count > 0 Then
                Dim downloadedProducts = CProduct.GetAllProducts(_CurrentWebsiteConfig, SafeString(Request.QueryString("type")))
                For pNum = 0 To oProductsList.Count - 1
                    Dim productCode As String = oProductsList(pNum).productName
                    For Each prod As CProduct In downloadedProducts
                        If (productCode = prod.ProductCode) Then
                            settingProductsList.Add(prod)

                            If prod.AccountType = "OTHER" Then Exit For 'dont want to fund future interest product
                            Dim pp As New CPayPalReceiptItem()
                            pp.ProductName = prod.AccountName
                            pp.ProductCode = prod.ProductCode
                            pp.DepositAmount = oProductsList(pNum).depositAmount
                            pp.ProductDescription = prod.ProductDescription
                            '' put it into list, we will use it below if FUNDING option is PAYPAL
                            paypalReceipt.Add(pp)

                            Exit For
                        End If
                    Next
                Next
            End If


            ''for efund
            Dim bDebitLPQConfig As Boolean = False
            Dim eIndex As Integer
            If settingProductsList.Count > 0 Then
                For eIndex = 0 To settingProductsList.Count - 1
                    If (settingProductsList(eIndex) IsNot Nothing AndAlso settingProductsList(eIndex).AutoPullDebitConsumerPrimary And Not _IsSecondary) Or _
                    (settingProductsList(eIndex) IsNot Nothing AndAlso settingProductsList(eIndex).AutoPullDebitConsumerSecondary And _IsSecondary) Then
                        bDebitLPQConfig = True
                        Exit For
                    End If
                Next
            End If

            Dim bDebitLPQMobileWebsiteConfig As String = SafeString(_CurrentWebsiteConfig.DebitType).ToUpper
            If bDebitLPQConfig And bDebitLPQMobileWebsiteConfig = "FIS" Then ''for efund
                Dim sEfundMessage As String = ""
                If Not ExecuteEfund(sEfundMessage) Then
                    'Response.Write(customErrorMessage)	'submitt error
                    'don't want to sent the error message to consumer to avoid mulitple submission
                    _sInternalCommentAppend += "Debit not successfully run.  "
                    Response.Write(customSubmittedSuccessMessage)
                    Return
                End If
                If sEfundMessage = "ACCEPT" Then    '
                    Response.Write(customPreapprovedSuccessMessage)
                Else       'Refer or failed, abandon other steps
                    _sInternalCommentAppend += "Debit not successfully run.  "
                    Response.Write(customSubmittedSuccessMessage)
                    Return
                End If
            ElseIf bDebitLPQConfig And bDebitLPQMobileWebsiteConfig = "EID" Then '' for retailbanking
                Dim sRBankingMessage As String = ""
                If Not ExecuteRetailBanking(sRBankingMessage) Then
                    _sInternalCommentAppend += "Debit not successfully run.  "
                    Response.Write(customSubmittedSuccessMessage)
                    Return
                End If
                If sRBankingMessage = "ACCEPT" Then    '
                    Response.Write(customPreapprovedSuccessMessage)
                Else       'Refer or failed, abandon other steps
                    _sInternalCommentAppend += "Debit not successfully run.  "
                    Response.Write(customSubmittedSuccessMessage)
                    Return
                End If
            ElseIf bDebitLPQConfig And bDebitLPQMobileWebsiteConfig = "DID" Then  '' --> for Deluxe detec
                Dim oDeluxe As CDeluxe = New CDeluxe()
                If Not oDeluxe.Execute(loanID, IsJointApplication, _CurrentWebsiteConfig, DeluxeDebitURL) Then 'Refer or failed, abandon other steps
                    _sInternalCommentAppend += "Debit not successfully run.  "
                    Response.Write(customSubmittedSuccessMessage)
                    Return
                Else '"PROCEED"
                    Response.Write(customPreapprovedSuccessMessage)
                End If
            ElseIf bDebitLPQConfig And bDebitLPQMobileWebsiteConfig = "EWS" Then  '' --> for Experian Precise EWS
                Dim oExperianEWS As CExperianEWS = New CExperianEWS()
                If Not oExperianEWS.Execute(loanID, IsJointApplication, _CurrentWebsiteConfig, ExperianEWSURL) Then 'Refer or failed, abandon other steps
                    _sInternalCommentAppend += "Debit not successfully run.  "
                    Response.Write(customSubmittedSuccessMessage)
                    Return
                Else '"PROCEED"
                    Response.Write(customPreapprovedSuccessMessage)
                End If
            Else 'skip efund
                Response.Write(customPreapprovedSuccessMessage)
            End If

            ''new format --creditpull
            Dim isCreditPull As Boolean = False
            Dim cIndex As Integer
            If settingProductsList.Count > 0 Then
                For cIndex = 0 To settingProductsList.Count - 1
                    If (settingProductsList(cIndex) IsNot Nothing AndAlso settingProductsList(cIndex).AutoPullCreditsConsumerPrimary And Not _IsSecondary) Or _
                    (settingProductsList(cIndex) IsNot Nothing AndAlso settingProductsList(cIndex).AutoPullCreditsConsumerSecondary And _IsSecondary) Then
                        isCreditPull = True
                        Exit For
                    End If
                Next
            End If

            If isCreditPull And Not _CurrentWebsiteConfig.IsDecisionXAEnable Then
                If Not ExecuteCreditPull() Then
                    _sInternalCommentAppend += "Credit not successfully run.  " '
                    Response.Clear()
                    Response.Write(customSubmittedSuccessMessage)
                    Return
                End If
            End If

            ''underwrite XA product, 
            ''for backward compatiblity, this feature need to be enabled for each cu based on available of lender side live code
            ''TODO: don't know how this will effect funding since it is selected before prodcut is approved
            If _CurrentWebsiteConfig.IsDecisionXAEnable Then
                Dim sXMLresponse = ExecuteDecisionXA()
                If Not isXAProductQualified(sXMLresponse) Then
                    Response.Clear()
                    Response.Write(customSubmittedSuccessMessage)
                    Return
                End If
            End If

            '' POST DATA to MLPayPal.aspx
            'Session("PAYPAL_SELECTED_PRODUCTS") = oProductsList
            'Response.Redirect("~/MLPayPal.aspx", True)

            ' We passed the authentication phase.
            ' If the application was approved, then we need to update the status of the application on the server
            ' We send the request again, but this time we list the approved products under the APPROVED category
            ' First, we have to do a "getloans" to get the loan data, then we make the changes to the loan data and 
            ' resubmit it back to the server with the "update" command
            If _CurrentWebsiteConfig.LoanStatusEnum.ToUpper() <> "" Then
                updateLoan(_CurrentWebsiteConfig.LoanStatusEnum.ToUpper()) 'set _bIsFraud
                _bIsUpdateReferralStatusRequired = False
            End If

            If _bIsFraud Then
                Response.Clear() 'need this to clear out previous approved message
                Response.Write(customSubmittedSuccessMessage)
                Return
            End If

            ''put it here so we don't have to deal with Fraud case and persist customSubmittedSuccessMessage, no need to update status
            '' --------------- PAYPAL funding ---------------------------------------------------------------------------------------''
            '' check PAYPAL email is available
            If Not String.IsNullOrEmpty(_CurrentWebsiteConfig.PayPalEmail) Then
                '' and also selected funding source is PAYPAL
                Dim isPayPalFunding = Request.Params("SelectedFundingSource") = "PAYPAL"
                If isPayPalFunding Then
                    '' Redirect to MLPayPal.aspx to process PayPal payment
                    Session("PAYPAL_RECEIPT") = paypalReceipt
                    Session("APPROVED_MESSAGE") = customPreapprovedSuccessMessage
                    '' build redirect url and params
                    '' Note: Since MLPayPal.aspx page inherits from CBasePage so it requires lenderref value to get website config data
                    Dim applicantEmail = Request.Form("EmailAddr")
                    Dim redirURL = String.Format("~/MLPayPal.aspx?lenderref={0}&sender={1}", _CurrentLenderRef, applicantEmail)
                    Response.Redirect(redirURL, True) '' End the response here, all codes below this line will be ignored.
                End If
                '' --------------- END PAYPAL funding ---------------------------------------------------------------------------------------''
            End If

            If _CurrentWebsiteConfig.Booking.ToUpper() <> "" Then

                Dim sMemberNumber As String = CBookingManager.Book(_CurrentWebsiteConfig, loanID, "XA")
                Dim sLoanResponse As String = getLoanResponse()
                ExecutePostDataToSaleForce(sLoanResponse, sMemberNumber)
                If Not String.IsNullOrEmpty(sMemberNumber) Then ' update preapproved message with membernumber
                    ''Common.getCustomResponseMessages(_CurrentWebsiteConfig, loanType, customErrorMessage, customPreapprovedSuccessMessage, customSubmittedSuccessMessage, customDeclinedMessage, customSubmittedDataList, customPreapprovedDateList, customDeclinedDateList)
                    ''customPreapprovedSuccessMessage = Common.buildNonApprovedMessage(customPreapprovedSuccessMessage, customPreapprovedDateList, loanXML, loanResponseXMLStr, sAccountNumber)

                    _bIsUpdateReferralStatusRequired = False

                    customPreapprovedSuccessMessage = getCustomPreApprovedMessages(sLoanResponse, sMemberNumber)

                    customPreapprovedSuccessMessage = PrependIconForNonApprovedMessage(customPreapprovedSuccessMessage, "Congratulations")
                    ''decode Message before rendering
                    customPreapprovedSuccessMessage = Server.HtmlDecode(customPreapprovedSuccessMessage)

                    Response.Clear()
                    Response.Write(customPreapprovedSuccessMessage)
                    Return
                Else
                    _bIsUpdateReferralStatusRequired = True '' can not book --> need to update status to referred
                    Response.Clear()
                    Response.Write(customSubmittedSuccessMessage)
                    Return
                End If
            End If

        End If

    End Sub

    Protected Function getAccountNumber(ByVal sLoanResponse As String) As String
        Dim sAccountNumber As String = ""
        Dim sResponseXmlDoc As XmlDocument = New XmlDocument()
        sResponseXmlDoc.LoadXml(sLoanResponse)
        Dim innerXMLStr As String = ""
        innerXMLStr = sResponseXmlDoc.InnerText
        Dim innerXMLDoc As XmlDocument = New XmlDocument()
        innerXMLDoc.LoadXml(innerXMLStr)
        ''get account number
        Dim ApprovedNodeList As XmlNodeList
        ApprovedNodeList = innerXMLDoc.GetElementsByTagName("ACCOUNT_TYPE")
        For Each childNode As XmlNode In ApprovedNodeList
            If childNode.ParentNode.Name = "APPROVED_ACCOUNTS" Then
                If childNode.Attributes("account_number") IsNot Nothing Then
                    sAccountNumber = SafeString(childNode.Attributes("account_number").InnerXml)
                    Exit For
                End If
            End If
        Next
        Return sAccountNumber
    End Function
    Protected Function getJointMemberNumber(ByVal sLoanResponse As String) As String
        Dim jointMemberNumber As String = ""
        Dim sResponseXmlDoc As XmlDocument = New XmlDocument()
        sResponseXmlDoc.LoadXml(sLoanResponse)
        Dim innerXMLStr As String = ""
        innerXMLStr = sResponseXmlDoc.InnerText
        Dim innerXMLDoc As XmlDocument = New XmlDocument()
        innerXMLDoc.LoadXml(innerXMLStr)
        Dim SpouseNode As XmlNodeList = innerXMLDoc.GetElementsByTagName("SPOUSE")
        If SpouseNode.Count = 0 Then
            Return jointMemberNumber ''no SPOUSE Node
        End If
        If SpouseNode(0).Attributes("member_number").InnerXml IsNot Nothing Then
            jointMemberNumber = SafeString(SpouseNode(0).Attributes("member_number").InnerXml)
        End If
        Return jointMemberNumber
    End Function
    ''-----------get Loan Response after booking ----------------
    Protected Function getLoanResponse() As String

        If loanID = "" Then Return ""
        Dim url As String = getLoansURL

        Dim getLoansXMLRequestStr As String = LoanRetrieval.BuildGetLoansXML(loanID, _CurrentWebsiteConfig.APIUser, _CurrentWebsiteConfig.APIPassword)

        ' ' Done building the XML, now ship it off
        Dim req As WebRequest = WebRequest.Create(url)

        req.Method = "POST"
        req.ContentType = "text/xml"

        log.Info("XA: Preparing to POST GetLoans Request: " & CSecureStringFormatter.MaskSensitiveXMLData(getLoansXMLRequestStr))

        Dim writer As New StreamWriter(req.GetRequestStream())
        writer.Write(getLoansXMLRequestStr)
        writer.Close()

        Dim res As WebResponse = req.GetResponse()

        Dim reader As New StreamReader(res.GetResponseStream())
        Dim sXml As String = reader.ReadToEnd()
        log.Info("XA: GetLoans Response: " + CSecureStringFormatter.MaskSensitiveXMLData(sXml))

        Return sXml
    End Function


    Protected Function getCustomPreApprovedMessages(ByVal sLoanResponse As String, ByVal sMemberNumber As String) As String

        Dim strCustomResponseMessage As String = ""

        Dim loanRequest As XmlDocument = New XmlDocument()
        loanRequest.LoadXml(loanRequestXMLStr)

        Dim innerXMLStr As String = ""
        innerXMLStr = loanRequest.InnerText

        Dim loanXML As XmlDocument = New XmlDocument
        loanXML.LoadXml(innerXMLStr)

        Dim oMessageNode As XmlNodeList = _CurrentWebsiteConfig._webConfigXML.SelectNodes("XA_LOAN/SUCCESS_MESSAGES/PREAPPROVED_MESSAGE")
        If oMessageNode.Count = 0 Then
            Return ""
        End If

        Dim numData As Integer = 0
        Dim hasData As Boolean = False
        Dim comboCustomDataList = New List(Of String)

        For Each childNode As XmlNode In oMessageNode
            Try
                strCustomResponseMessage = childNode.Attributes("message").InnerXml
                If (childNode.Attributes("num_data") IsNot Nothing) Then
                    hasData = Integer.TryParse(childNode.Attributes("num_data").InnerXml, numData)
                End If
                If hasData Then
                    For i As Integer = 0 To numData - 1
                        comboCustomDataList.Add(childNode.Attributes("message_data" + i.ToString()).InnerXml)
                    Next
                End If
                Exit For
            Catch
                Return "" ''return empty string
            End Try
        Next

        ' Create a new list of strings that will hold the data that was requested in the dataRequestList
        Dim dataList As List(Of String) = New List(Of String)

        For Each dataRequest As String In comboCustomDataList
            dataList.Add(getDataValueFromRequest(dataRequest, loanXML, sLoanResponse, sMemberNumber))
        Next

        Dim resultMessage As String = ""

        'this error occur so frequent so it help to display the message to suppot
        Try
            resultMessage = String.Format(strCustomResponseMessage, dataList.ToArray()) ' Now put the customMessage together with the data values
        Catch ex As Exception
            resultMessage = "Message setup error." & strCustomResponseMessage
            log.Error("Message setup error in xml config.  Index is not correct    " & ex.Message)
            Return ""
        End Try

        ''need to add joint member number if the application has joint
        Dim jointMemberNumber = getJointMemberNumber(sLoanResponse)
        If Not String.IsNullOrEmpty(jointMemberNumber) Then
            Dim jointMemberNumberMessage = " and the joint member number is " + jointMemberNumber
            Dim memberNumberIndex As Integer = resultMessage.IndexOf(sMemberNumber) + sMemberNumber.Length
            Dim primaryMemberNumberMessage = resultMessage.Substring(0, memberNumberIndex)
            ''resultMessage contain joint member number message
            resultMessage = primaryMemberNumberMessage + jointMemberNumberMessage + resultMessage.Substring(memberNumberIndex)
        End If

        Return resultMessage
    End Function

    Private Function getDataValueFromRequest(ByVal dataRequest As String, ByVal loanXML As XmlDocument, ByVal sLoanResponse As String, ByVal sMemberNumber As String) As String
        Dim applicantNodeList As XmlNodeList = loanXML.GetElementsByTagName("APPLICANT")
        Dim applicantNode As XmlElement = applicantNodeList(0)

        ''get first name
        Dim firstName As String = ""
        Dim lastName As String = ""

        If applicantNode IsNot Nothing Then
            If applicantNode.Attributes("first_name") IsNot Nothing Then
                firstName = applicantNode.Attributes("first_name").InnerXml
                firstName = firstName.Substring(0, 1).ToUpper & firstName.Substring(1, firstName.Length - 1)
            End If
            ''get last name
            If applicantNode.Attributes("last_name") IsNot Nothing Then
                lastName = applicantNode.Attributes("last_name").InnerXml
                lastName = lastName.Substring(0, 1).ToUpper & lastName.Substring(1, lastName.Length - 1)
            End If
        End If

        '' get request card name
        Dim loanInfoNodeList As XmlNodeList = loanXML.GetElementsByTagName("LOAN_INFO")
        Dim loanInfo As XmlElement = loanInfoNodeList(0)
        Dim cardName As String = ""

        If loanInfo IsNot Nothing Then
            If loanInfo.Attributes("requested_card_name") IsNot Nothing Then
                cardName = loanInfo.Attributes("requested_card_name").InnerXml
            End If
        End If

        Dim sLoanNumber As String = Common.getLoanNumber(sLoanResponse)
        Dim sAccountNumber As String = getAccountNumber(sLoanResponse)
        Select Case dataRequest
            Case "LOAN_NUMBER"
                Return sLoanNumber

            Case "MEMBER_LOAN_NUMBER", "MEMBER_NUMBER"
                Return sMemberNumber

            Case "ACCOUNT_NUMBER"
                Return sAccountNumber

            Case "FIRST_NAME"
                Return firstName

            Case "FULL_NAME"
                Return firstName + " " + lastName

            Case "CARD_NAME"
                Return cardName

        End Select

        Return ""
    End Function

    ''-----------end get Loan Response after booking ----------------

    ''set final app status to REFERRAL if it has not been changed to APPROVED/INSTANTAPPROVE and other condition?
    ''switching of status is needed to trigger realtime automated email action for referred application
    'only do this after last post; or after all borrower has answered IDA questions
    Protected Sub Page_LoadComplete(sender As Object, e As EventArgs) Handles Me.LoadComplete

        If _bIsUpdateReferralStatusRequired And _CurrentWebsiteConfig.LoanStatusEnum.ToUpper() <> "" Then
            updateLoan("REFERRED")
        End If

    End Sub

    Private Function ValidateInput() As String
        Dim errMessage As String = String.Empty

        Return errMessage
    End Function

    Public Function PreUnderWrite() As Boolean
        If _CurrentLenderRef.StartsWith("bcu") Or _CurrentLenderRef.StartsWith("iqcu") Then
            'look for xmlLoan atrribute to match  then   return fail
            If isIRSWithHolding() Or Not isValidDLExpirationDate() Then
                ''fail PreUnderWrite
                Return False
            End If
            Return True
        End If
        Return True
    End Function

    Protected Function isIRSWithHolding() As Boolean
        ' deserialize array string from json
        Dim listCQJsonQuestionAnswers As New List(Of XAQuestionAnswer)
        listCQJsonQuestionAnswers = (New JavaScriptSerializer()).Deserialize(Of List(Of XAQuestionAnswer))(HttpContext.Current.Request.Params("XAQuestionAnswers"))
        '' make sure the list is not empty
        If listCQJsonQuestionAnswers.Count = 0 Then
            Return False ''no questions 
        End If
        For i = 0 To listCQJsonQuestionAnswers.Count - 1
            If (listCQJsonQuestionAnswers(i).q.Length > 14) Then ''make sure the length of string at least the same as "IRS_WITHHOLDING" 
                Dim questionName As String = listCQJsonQuestionAnswers(i).q.Substring(0, 15)
                Dim questionAnswer As String = listCQJsonQuestionAnswers(i).a
                If questionName.ToUpper = "IRS_WITHHOLDING" Then 'bcu
                    If questionAnswer.ToUpper = "YES" Then
                        Return True
                    Else
                        Return False
                    End If
                ElseIf questionName.ToUpper = "SSN/TIN Certification and Backup Withholding" Then 'iqcu
                    If questionAnswer.ToUpper = "I am subject to backup withholding" Then
                        Return True
                    Else
                        Return False
                    End If
                End If
            End If
        Next
        Return False
    End Function

    Protected Function isValidDLExpirationDate() As Boolean
        Dim IDType As String = Common.SafeString(Request.Form("IDCardType"))
        Dim maxExpirationDate As New DateTime(2078, 12, 31) ''  "12/31/2078"
        If IDType.ToUpper = "DRIVERS_LICENSE" Then
            If Not String.IsNullOrEmpty(Request.Form("IDDateExpire")) Then
                Dim ExpirationDate As DateTime = Date.Parse(Request.Form("IDDateExpire"))
                If DateTime.Compare(ExpirationDate, maxExpirationDate) > 0 Then
                    Return False '' is not valid 
                End If
            End If
        End If '
        If IsJointApplication = False Then
            Return True
        Else ''check joint expiration date
            Dim co_IDType As String = Common.SafeString(Request.Form("co_IDCardType"))
            If co_IDType.ToUpper = "DRIVERS_LICENSE" Then
                If Not String.IsNullOrEmpty(Request.Form("co_IDDateExpire")) Then
                    Dim co_ExpirationDate As DateTime = Date.Parse(Request.Form("co_IDDateExpire"))
                    If DateTime.Compare(co_ExpirationDate, maxExpirationDate) > 0 Then
                        Return False '' is not valid 
                    End If
                End If
            End If
        End If
        Return True
    End Function

    Protected Function CreateNewLoan() As Boolean
        ' In order to submit the application, we need to create an XML document with the proper structure
        ' and fill it out with the data that the user has entered.

        ' TODO:  Move this URL into the XML config file and read it from there 
        Dim url As String = loanSubmitURL
        Dim xml As String = LoadTemplateXML()

        ' Save the xml request string so we can refer to it in future callbacks
        loanRequestXMLStr = xml
        newLoanRequestXMLStr = xml

        Dim requestXMLDoc As XmlDocument = New XmlDocument()
        requestXMLDoc.LoadXml(xml)

        Dim req As WebRequest = WebRequest.Create(url)

        req.Method = "POST"
        req.ContentType = "text/xml"

        'TODO: MaskSensitiveXMLData can't work into cdata
        log.Info("XA: Preparing to POST data(new loan): " & CSecureStringFormatter.MaskSensitiveXMLData(xml))

        Dim writer As New StreamWriter(req.GetRequestStream())
        writer.Write(xml)
        writer.Close()

        log.Info("Start the request to " + url)

        Dim res As WebResponse = req.GetResponse()

        Dim reader As StreamReader = New StreamReader(res.GetResponseStream())

        Dim sXml As String = reader.ReadToEnd()
        ' Save the xml respnonse string so we can refer to it in other func
        loanResponseXMLStr = sXml

        log.Info("XA: Response from server(new loan): " + sXml)

        reader.Close()
        res.GetResponseStream().Close()
        req.GetRequestStream().Close()
        Return IsSubmitted(sXml)

    End Function

    Protected Function ExecWalletQuestions(ByRef psMessage As String, ByRef psWalletQuestions As String) As Boolean
        'log.Info("XA: Preparing request for Out of Wallet Questions")

        ' Get the correct URL depending on which authentication method was given in the config.xml file.
        Dim questionsURL As String = ""
        Dim answersURL As String = ""
        Dim authenticationMethod As String = determineAuthenticationType(questionsURL, answersURL).Trim
        Dim url As String = questionsURL

        Dim walletRequest As String = "RSA"
        Select Case authenticationMethod.ToUpper()
            Case "RSA"
                walletRequest = GetOutOfWalletQuestionsRSA(loanResponseXMLStr)
            Case "PID"
                walletRequest = GetOutOfWalletQuestionsPID(loanResponseXMLStr)
            Case "FIS"
                walletRequest = GetOutOfWalletQuestionsFIS(loanResponseXMLStr)
            Case "DID"
                walletRequest = GetOutOfWalletQuestionsDID(loanResponseXMLStr)
                'equifax
            Case "EID"
                walletRequest = GetOutOfWalletQuestionsEquifax(loanResponseXMLStr)
            Case "TID" ''TransUnion
                walletRequest = GetOutOfWalletQuestionsTID(loanResponseXMLStr)
            Case "NONE"
                ' If the option was set to NONE or there was no option set, then the lender does not support ID authentication
                ' Then we're done.
                psMessage = customSubmittedSuccessMessage + "<div id='MLNOIDAUTHENTICATION'></div>"
                Return True
        End Select

        Dim req As WebRequest = WebRequest.Create(url)
        req.Method = "POST"
        req.ContentType = "text/xml"

        log.Info("XA: Preparing to POST Data(Authenticate): " & CSecureStringFormatter.MaskSensitiveXMLData(walletRequest))

        Dim writer As New StreamWriter(req.GetRequestStream())
        writer.Write(walletRequest)
        writer.Close()
        log.Info("Start the request to " + url)

        Dim res As WebResponse = req.GetResponse()
        Dim reader As StreamReader = New StreamReader(res.GetResponseStream())
        Dim sXml As String = reader.ReadToEnd()
        log.Info("XA: Response from server(Authenticate): " + sXml)

        reader.Close()
        res.GetResponseStream().Close()
        req.GetRequestStream().Close()

        Dim bIsFailedAuthenticate As Boolean = False
        If Not IsSubmitted(sXml, bIsFailedAuthenticate) Then
            Return False
        End If
        If bIsFailedAuthenticate Then
            psMessage = customSubmittedSuccessMessage   'case for unable to lcate the person, invalid social
            Return False
        End If

        Dim walletObject As WalletQuestionRendering
        ' Process the Out of Wallet Questions and return them so they can be displayed to the user
        walletObject = New WalletQuestionRendering(sXml, _CurrentWebsiteConfig)
        walletObject.setAuthenticationType(authenticationMethod)
        walletObject.populateQuestions(transaction_ID, question_set_ID, request_app_ID, request_client_ID, request_sequence_ID, reference_number)

        questionList_persist = walletObject.questionsList

        Dim walletQuestionsHTML As String
        If "False" = SafeString(Session("isJoinQuestionDone")) And IsJointApplication Then
            Dim sName = Co_FName
            walletQuestionsHTML = walletObject.RenderWalletQuestions(Co_FName, transaction_ID, question_set_ID, request_app_ID, request_client_ID, request_sequence_ID, reference_number)
        Else
            Dim sName = Request.Form("FirstName")
            walletQuestionsHTML = walletObject.RenderWalletQuestions(sName, transaction_ID, question_set_ID, request_app_ID, request_client_ID, request_sequence_ID, reference_number)
        End If


        Dim numQuestionsStr = Format(walletObject.NumQuestions, "00")

        psWalletQuestions = (numQuestionsStr + walletQuestionsHTML)
        Return True
    End Function

    Protected Function ExecuteCreditPull() As Boolean
        If SafeString(_CurrentWebsiteConfig.CreditPull).ToUpper <> "Y" Then Return True 'CreditPull not setup for this lender

        Dim creditPullXMLRequest As String = BuildCreditPullXMLRequest()
        Dim req As WebRequest = WebRequest.Create(creditPullURL)

        req.Method = "POST"
        req.ContentType = "text/xml"

        log.Info("XA: Preparing to POST credit pull Data: " & CSecureStringFormatter.MaskSensitiveXMLData(creditPullXMLRequest))

        Dim writer As New StreamWriter(req.GetRequestStream())
        writer.Write(creditPullXMLRequest)
        writer.Close()

        Dim res As WebResponse = req.GetResponse()

        Dim reader As New StreamReader(res.GetResponseStream())
        Dim sXml As String = reader.ReadToEnd()

        log.Info("XA: Response from server(credit pull): " & sXml)

        reader.Close()
        res.GetResponseStream().Close()
        req.GetRequestStream().Close()

        If sXml.Contains("ERROR") Then Return False

        Return True

    End Function
    ''' <summary>
    ''' Call decisionXA on lenderside, which perform the following
    ''' 1)pulls credit report based on credit pull preference in lender setting  or product setting (or operation)
    ''' 2)Does underwriting based on guidlines
    ''' </summary>
    ''' <returns>status and list of qualified products</returns>
    ''' <remarks></remarks>
    Protected Function ExecuteDecisionXA() As String
        If Not _CurrentWebsiteConfig.IsDecisionXAEnable Then Return True 'DecisionXA not setup for this lender

        Dim decisionXAXMLRequest As String = BuildDecisionXAXMLRequest()
        Dim req As WebRequest = WebRequest.Create(decisionXAURL)

        req.Method = "POST"
        req.ContentType = "text/xml"

        log.Info("Preparing to POST decisionXA Data: " & CSecureStringFormatter.MaskSensitiveXMLData(decisionXAXMLRequest))

        Dim writer As New StreamWriter(req.GetRequestStream())
        writer.Write(decisionXAXMLRequest)
        writer.Close()

        Dim res As WebResponse = req.GetResponse()

        Dim reader As New StreamReader(res.GetResponseStream())
        Dim sXml As String = reader.ReadToEnd()

        log.Info("Response from server(decisionXA): " & sXml)

        reader.Close()
        res.GetResponseStream().Close()
        req.GetRequestStream().Close()

        Return sXml

    End Function
    ''----------salesforce implementation--------------
    Protected Function BuildAccessTokenRequest() As String
        ' Now construct the XML response
        Dim sb As New StringBuilder()

		Dim SalesForceSettings As NameValueCollection = ConfigurationManager.GetSection("SalesForceSettings")
		Dim sUserName As String = SalesForceSettings("SalesForceUsername")
		Dim sPassWord As String = SalesForceSettings("SalesForcePassword")
		Dim sClientId As String = SalesForceSettings("SalesForceClientId")
		Dim sClientSecret As String = SalesForceSettings("SalesForceClientSecret")

		If isLive = "Y" Then ''switch sUrl to live
			sUserName = SalesForceSettings("SalesForceUsername_Live")
			sPassWord = SalesForceSettings("SalesForcePassword_Live")
			sClientId = SalesForceSettings("SalesForceClientId_Live")
			sClientSecret = SalesForceSettings("SalesForceClientSecret_Live")
		End If

        Dim sGrantType As String = "password"

        sb.AppendFormat("username={0}&password={1}&client_id={2}&client_secret={3}&grant_type={4}", sUserName, sPassWord, sClientId, sClientSecret, sGrantType)
        Dim enc = New System.Text.UTF8Encoding()
        Return enc.GetString(enc.GetBytes(sb.ToString()))
    End Function

    Protected Function getAuthorization() As String

        Dim strAccessToken = ExecuteAccessToken()
        ''check strAccessToken: json format, contain access_token and token_type
        If Not strAccessToken.Trim().StartsWith("{") Or Not strAccessToken.Contains("access_token") Or Not strAccessToken.Contains("token_type") Then
            Return ""
        End If

        Dim jsonAccessToken = JsonConvert.DeserializeObject(Of Dictionary(Of String, Object))(strAccessToken)
        If Not String.IsNullOrEmpty(jsonAccessToken.Item("access_token")) And Not String.IsNullOrEmpty(jsonAccessToken.Item("token_type")) Then
            Return jsonAccessToken.Item("token_type") & " " & jsonAccessToken.Item("access_token")
        End If
        Return ""
    End Function

	Protected Function ExecuteAccessToken() As String
		Dim SalesForceSettings As NameValueCollection = ConfigurationManager.GetSection("SalesForceSettings")
		Dim sUrl As String = SalesForceSettings("SalesForceUrlAccessToken")
		Dim strJSON As String = ""
		If isLive = "Y" Then ''switch sUrl to live
			sUrl = SalesForceSettings("SalesForceUrlAccessToken_Live")
		End If
		Try
			Dim req As WebRequest = WebRequest.Create(sUrl)
			Dim strAccessTokenRequest As String = BuildAccessTokenRequest()
			req.Method = "POST"
			req.ContentType = "application/x-www-form-urlencoded"
			log.Info("Preparing to POST Authenticating(Salesforce): " & strAccessTokenRequest)
			log.Info("Begin request to: " & sUrl)
			Dim writer As New StreamWriter(req.GetRequestStream())
			writer.Write(strAccessTokenRequest)
			writer.Close()
			Dim res As WebResponse = req.GetResponse()
			Dim reader As New StreamReader(res.GetResponseStream())
			strJSON = reader.ReadToEnd()

			log.Info("Response from server(-----SalesForce-------): " & strJSON)
			reader.Close()
			res.GetResponseStream().Close()
			req.GetRequestStream().Close()
		Catch ex As Exception
			log.Error("Error Access Token Response: " & ex.Message, ex)
			Return ""
		End Try
		Return strJSON
	End Function

	Protected Sub ExecutePostDataToSaleForce(ByVal sLoanResponse As String, ByVal sMemberNumber As String, Optional ByVal psFailedIDA As Boolean = False, Optional ByVal sFaildedIDAMsg As String = "")
		Dim strJSON As String = ""

		Dim SalesForceSettings As NameValueCollection = ConfigurationManager.GetSection("SalesForceSettings")
		Dim sUrl As String = SalesForceSettings("SalesForceUrlAccuAccountUpdate")

		If isLive = "Y" Then ''switch sUrl to live
			sUrl = SalesForceSettings("SalesForceUrlAccuAccountUpdate_Live")
		End If
		Dim sAuthorization As String = getAuthorization()
        If sAuthorization = "" Then
            log.Warn("Unable to get Authorization from Salesforce")
            Return  '' can not get authorization so exit 
        End If

        Try
            Dim req As HttpWebRequest = CType(WebRequest.Create(sUrl), HttpWebRequest)
            Dim responseFromServer As Byte() = Nothing

            Dim postData = getJSONData(sLoanResponse, sMemberNumber, psFailedIDA, sFaildedIDAMsg)
            req.Method = "POST"
            req.ContentType = "application/json"

            req.Headers.Add("Authorization", sAuthorization)

            log.Info("Preparing to POST Data with Salesforce: " & CSecureStringFormatter.MaskSensitiveData(postData))
            log.Info("Begin request to: " & sUrl)
            Dim dataByte = System.Text.Encoding.UTF8.GetBytes(postData)
            req.ContentLength = dataByte.Length

            Dim dataStream As Stream = req.GetRequestStream()
            dataStream.Write(dataByte, 0, dataByte.Length)
            dataStream.Close()

            Dim res As HttpWebResponse = CType(req.GetResponse(), HttpWebResponse)
            dataStream = res.GetResponseStream()
            Dim reader As New StreamReader(res.GetResponseStream())
            strJSON = reader.ReadToEnd()

            log.Info("Response from server(after post data to SalesForce): " & strJSON)
            reader.Close()
            res.GetResponseStream().Close()
            req.GetRequestStream().Close()
        Catch e As WebException
            log.Warn("This program is expected to throw WebException on successful run." + vbLf & vbLf & "Exception Message :" + e.Message)
            If e.Status = WebExceptionStatus.ProtocolError Then
                log.Warn("Status Code : " & CType(e.Response, HttpWebResponse).StatusCode)
                log.Warn("Status Description : " & CType(e.Response, HttpWebResponse).StatusDescription)
            End If
        End Try

    End Sub
    Protected Function getJSONData(ByVal sLoanResponse As String, ByVal sMemberNumber As String, Optional ByVal psFailedIDA As Boolean = False, Optional ByVal sFailedIDAMsg As String = "") As String

        Dim strHouseHoldNumber As String = ""
        Dim startIndex As Integer = sLoanResponse.IndexOf("HouseHoldNumber:[")
        If startIndex > -1 Then
            strHouseHoldNumber = sLoanResponse.Substring(startIndex, sLoanResponse.IndexOf("]</INTERNAL_COMMENTS>") - startIndex).Replace("HouseHoldNumber:[", "")
        End If

        Dim strDate As String = DateTime.Now.ToUniversalTime.ToString("u")
        Dim status As String = "success"
        Dim sAccountNumber As String = ""
        Dim statusMessage As String = ""
        ''status is success if HouseHoldNumber and AccountNumber are not empty
        If String.IsNullOrEmpty(strHouseHoldNumber) Then
            status = "failure"
            statusMessage += "The houseHoldNumber is invalid. "
        End If
        If (psFailedIDA) Then
            statusMessage = "IDA not successfully run.  "
            If sFailedIDAMsg <> "" Then
                statusMessage = sFailedIDAMsg
            End If
            status = "failure"
        Else
            sAccountNumber = getAccountNumber(sLoanResponse)
            If (String.IsNullOrEmpty(sAccountNumber)) Then
                status = "failure"
                statusMessage += "The account number is invalid. "
            ElseIf (String.IsNullOrEmpty(sMemberNumber)) Then
                status = "failure"
                statusMessage += "The member number is invalid. "
            End If
        End If

        Dim strData As String = "{""accuUpdates"":{""accuAccounts"":["
        strData += "{""houseHoldNumber"":""" & strHouseHoldNumber & """,""accuNumber"":""" & sAccountNumber & """,""accuCustomerNumber"":""" & sMemberNumber & """,""status"":""" & status

        If status = "failure" Then ''add statusMessage
            strData += """,""statusMessage"":""" & statusMessage & """,""statusDateTime"":""" & strDate & """}]}}"
        Else
            strData += """,""statusDateTime"":""" & strDate & """}]}}"
        End If

        Return strData

    End Function

    Protected Sub updateIncompleteApp(ByVal sSessionID As String, Optional ByVal strJoint As String = "")
        log.Info("---------" & strJoint & "Start to complete IDA questions-----------")

        Thread.Sleep(360000) '' 1 min=60000ms -->6mins= 360000

        If _dicAppInfo.ContainsKey(sSessionID) Then
            Dim oObject = New CIncompleteAppInfo()
            oObject = New JavaScriptSerializer().Deserialize(Of CIncompleteAppInfo)(_dicAppInfo.Item(sSessionID))
            log.Info("---------" & strJoint & "Incomplete IDA questions (Start to update status and internal comment)-----")
            Dim sIncompleteMessage As String = "The applicant does not complete the IDA questions."

            ''update status and internal comment to LPQ
            _sInternalCommentAppend += sIncompleteMessage
            updateLoan("REFERRED", oObject.sIncAppLoanID, oObject.sIncAppLoanRequestXML)
            _bIsUpdateReferralStatusRequired = False

            ''update Failured message and status to Salesforce
            ExecutePostDataToSaleForce(oObject.sIncAppGetLoanResponse, "", True, sIncompleteMessage)
            SyncLock _threadLock_
                ''after update removed appInfor from the dic
                _dicAppInfo.Remove(sSessionID)
            End SyncLock
        End If
    End Sub

    Protected Sub SaveAppInfo(ByVal sSessionID As String)

        Dim oObject = New CIncompleteAppInfo()
        oObject.sIncAppGetLoanResponse = getLoanResponse()
        oObject.sIncAppLoanID = loanID
        oObject.sIncAppLoanRequestXML = loanRequestXMLStr

        If Not _dicAppInfo.ContainsKey(sSessionID) Then
            _dicAppInfo.Add(sSessionID, New JavaScriptSerializer().Serialize(oObject))
        End If

    End Sub

    '''----------end salesforce implementation--------------
    ''' <summary>
    ''' True=process oK, False = not setup/ connection issue
    ''' psMessage return ACCEPT of REFER
    ''' </summary>
    ''' <param name="psMessage"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Protected Function ExecuteEfund(ByRef psMessage As String) As Boolean
        Dim eFundsXMLRequest As String = BuildeFundsXMLRequest()
        Dim req As WebRequest = WebRequest.Create(eFundsURL)

        req.Method = "POST"
        req.ContentType = "text/xml"

        log.Info("XA: Preparing to POST eFunds Data: " & CSecureStringFormatter.MaskSensitiveXMLData(eFundsXMLRequest))

        Dim writer As New StreamWriter(req.GetRequestStream())
        writer.Write(eFundsXMLRequest)
        writer.Close()

        Dim res As WebResponse = req.GetResponse()

        Dim reader As New StreamReader(res.GetResponseStream())
        Dim sXml As String = reader.ReadToEnd()

        log.Info("XA: Response from server(eFund): " + sXml)

        reader.Close()
        res.GetResponseStream().Close()
        req.GetRequestStream().Close()

        ' Check to see if the eFunds response is not an error
        If Not IsSubmitted(sXml) Then
            Return False
        End If


        If isEFundsSuccessful(sXml) Then   'reserve 10/2
            psMessage = "ACCEPT"    'pass question
        Else
            psMessage = "REFER"  'fail question
            Return False  'fail return false dont want to run join if the primary is referred
        End If

        ''======Run for joint==========
        If Not IsJointApplication Then Return True

        Dim eFundsXMLRequest2 As String = BuildeFundsXMLRequest(True)
        Dim req2 As WebRequest = WebRequest.Create(eFundsURL)

        req2.Method = "POST"
        req2.ContentType = "text/xml"

        log.Info("XA Joint: Preparing to POST eFunds Data: " & CSecureStringFormatter.MaskSensitiveXMLData(eFundsXMLRequest2))

        Dim writer2 As New StreamWriter(req2.GetRequestStream())
        writer2.Write(eFundsXMLRequest2)
        writer2.Close()

        Dim res2 As WebResponse = req2.GetResponse()

        Dim reader2 As New StreamReader(res2.GetResponseStream())
        Dim sXml2 As String = reader2.ReadToEnd()

        log.Info("XA Joint: Response from server(eFund): " + sXml2)

        reader2.Close()
        res2.GetResponseStream().Close()
        req2.GetRequestStream().Close()

        ' Check to see if the eFunds response is not an error
        If Not IsSubmitted(sXml2) Then
            Return False
        End If


        If isEFundsSuccessful(sXml2) Then   'reserve 10/2
            psMessage = "ACCEPT"    'pass question
        Else
            psMessage = "REFER"  'fail question
            Return False '' exit 
        End If
        Return True

    End Function
    ''' <summary>
    ''' True=process oK, False = not setup/ connection issue
    ''' psMessage return ACCEPT of REFER
    ''' </summary>
    ''' <param name="psMessage"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Protected Function ExecuteRetailBanking(ByRef psMessage As String) As Boolean
        Dim rBankingXMLRequest As String = BuildRetailBankingXMLRequest()
        Dim req As WebRequest = WebRequest.Create(RetailBankingURL)

        req.Method = "POST"
        req.ContentType = "text/xml"

        log.Info("XA: Preparing to POST RetailBanking Data: " & CSecureStringFormatter.MaskSensitiveXMLData(rBankingXMLRequest))

        Dim writer As New StreamWriter(req.GetRequestStream())
        writer.Write(rBankingXMLRequest)
        writer.Close()

        Dim res As WebResponse = req.GetResponse()

        Dim reader As New StreamReader(res.GetResponseStream())
        Dim sXml As String = reader.ReadToEnd()

        log.Info("XA: Response from server(RetailBanking): " + sXml)

        reader.Close()
        res.GetResponseStream().Close()
        req.GetRequestStream().Close()

        ' Check to see if the eFunds response is not an error
        If Not IsSubmitted(sXml) Then
            Return False
        End If

        If isRBankingSuccessful(sXml) Then
            psMessage = "ACCEPT" ''pass question
        Else
            psMessage = "REFER"  '' fail question
            Return False '' exit 
        End If
        ''======Run for joint==========
        If Not IsJointApplication Then Return True

        Dim rBankingXMLRequest2 As String = BuildRetailBankingXMLRequest(True)
        Dim req2 As WebRequest = WebRequest.Create(RetailBankingURL)

        req2.Method = "POST"
        req2.ContentType = "text/xml"

        log.Info("XA Joint: Preparing to POST RetailBanking Data: " & CSecureStringFormatter.MaskSensitiveXMLData(rBankingXMLRequest2))

        Dim writer2 As New StreamWriter(req2.GetRequestStream())
        writer2.Write(rBankingXMLRequest2)
        writer2.Close()

        Dim res2 As WebResponse = req2.GetResponse()

        Dim reader2 As New StreamReader(res2.GetResponseStream())
        Dim sXml2 As String = reader2.ReadToEnd()

        log.Info("XA Joint: Response from server(RetailBanking): " + sXml2)

        reader2.Close()
        res2.GetResponseStream().Close()
        req2.GetRequestStream().Close()

        ' Check to see if the eFunds response is not an error
        If Not IsSubmitted(sXml2) Then
            Return False
        End If

        If isRBankingSuccessful(sXml2) Then
            psMessage = "ACCEPT"    'pass question
        Else
            psMessage = "REFER"  'fail question
            Return False ''exit 
        End If
        Return True

    End Function

    Public Function getQuestionIDs(ByRef questionIDList As List(Of String), ByVal answerIDlist As List(Of String), ByVal poQuestionList As List(Of WalletQuestionRendering.questionDataClass)) As Boolean
        ' Based on the ids of the answers, match each answer with the correct question
        Dim foundMatch As Boolean = False
        'this loop only work if answer is in the same order as question order
        If _CurrentWebsiteConfig.AuthenticationType.ToUpper() = "EID" Or _CurrentWebsiteConfig.AuthenticationType.ToUpper() = "DID" Or _CurrentWebsiteConfig.AuthenticationType.ToUpper() = "PID" Then
            Dim answerIDCount As Integer = 0
            For Each questionObject As WalletQuestionRendering.questionDataClass In poQuestionList
                ' For each questionObject, find the choiceID that matches the answerID
                For Each choiceObject As WalletQuestionRendering.choiceClass In questionObject.choicesList
                    If (choiceObject.choiceID.ToUpper() = answerIDlist(answerIDCount).ToUpper()) Then
                        questionIDList.Add(questionObject.questionID)
                        'go to next answer ID
                        answerIDCount = answerIDCount + 1
                        foundMatch = True
                        Exit For
                    End If
                Next
                If (foundMatch = False) Then
                    Return False
                End If
                foundMatch = False
            Next
        Else ''Only work for RSA, FIS, TID.  this loop only work if answer id is unique otherwise it will keep selecting question#0 
            For Each answerID As String In answerIDlist
                ' For each answerID, find the questionID that it is associated with
                For Each questionObject As WalletQuestionRendering.questionDataClass In poQuestionList
                    ' For each questionObject, find the choiceID that matches the answerID
                    For Each choiceObject As WalletQuestionRendering.choiceClass In questionObject.choicesList
                        If (choiceObject.choiceID.ToUpper() = answerID.ToUpper()) Then
                            questionIDList.Add(questionObject.questionID)
                            foundMatch = True
                            Exit For
                        End If
                    Next
                    If (foundMatch) Then
                        Exit For
                    End If
                Next
                If (foundMatch = False) Then
                    Return False
                End If
                foundMatch = False
            Next
        End If
        Return True
    End Function
    Protected Function ExecWalletAnswers(ByRef psAnswerValidate As String, ByRef psWalletMessage As String) As Boolean
        ' First, we need to get the id's of the questions and answers
        Dim questionIDList As New List(Of String)
        Dim answerIDList As New List(Of String)

        answerIDList = parseAnswersForID()
        Dim areAnswersValid As Boolean = getQuestionIDs(questionIDList, answerIDList, questionList_persist)

        ' If the answer IDs didn't match up with any question, then there was something wrong
        If (areAnswersValid = False) Then
            psAnswerValidate = ("Answers to Out of Wallet Questions are invalid!")
            Return True
        End If

        ' Determine which authentication method to use.
        ' Each method has different URLs and different XML requests.
        Dim questionsURL As String = ""
        Dim answersURL As String = ""
        Dim authenticationMethod As String = determineAuthenticationType(questionsURL, answersURL)
        Dim url As String = answersURL

        ' The answers are valid, so we package them up in the proper XML and send it off
        Dim walletAnswerXML As String = buildWalletAnswerXML(questionIDList, answerIDList, authenticationMethod)

        ' Done building the XML, now ship it off
        Dim req As WebRequest = WebRequest.Create(url)

        req.Method = "POST"
        req.ContentType = "text/xml"

        log.Info("XA: Preparing to POST wallet answer Data: " & CSecureStringFormatter.MaskSensitiveXMLData(walletAnswerXML))
        Dim writer As New StreamWriter(req.GetRequestStream())
        writer.Write(walletAnswerXML)
        writer.Close()

        Dim res As WebResponse = req.GetResponse()

        Dim reader As New StreamReader(res.GetResponseStream())
        Dim sXml As String = reader.ReadToEnd()
        log.Info("XA: Response from server: " + sXml)

        reader.Close()
        res.GetResponseStream().Close()
        req.GetRequestStream().Close()

        ' Check to see if the server's response to the wallet answers is not an error
        If Not IsSubmitted(sXml) Then
            Return False
        End If


        ' There are no errors, so check to see if PID or RSA was approved. 
        If Not hasPassedAuthentication(authenticationMethod, sXml) Then  'execute this loop when not approved

            If hasFollowOnQuestion(authenticationMethod, sXml) Then
                psWalletMessage = GetFollowONQuestion(authenticationMethod, sXml)
            Else
                psWalletMessage = customSubmittedSuccessMessage 'question failed ' Return the custom "Non-approved" message
            End If
        End If

        Return True


    End Function

    'TODO: this is for DID only
    Protected Function GetFollowONQuestion(ByVal psAuthenticationType As String, ByVal psXMLresponse As String) As String
        Dim walletObject As WalletQuestionRendering
        ' Process the Out of Wallet Questions and return them so they can be displayed to the user
        walletObject = New WalletQuestionRendering(psXMLresponse, _CurrentWebsiteConfig)
        walletObject.setAuthenticationType(psAuthenticationType)
        walletObject.populateQuestions(transaction_ID, question_set_ID, request_app_ID, request_client_ID, request_sequence_ID, reference_number)

        questionList_persist = walletObject.questionsList

        Dim walletQuestionsHTML As String
        If SafeString(Session("isJoinQuestionDone")) <> "" And IsJointApplication Then
            Dim sName = Co_FName
            walletQuestionsHTML = walletObject.RenderWalletQuestions(Co_FName, transaction_ID, question_set_ID, request_app_ID, request_client_ID, request_sequence_ID, reference_number)
        Else
            Dim sName = Request.Form("FirstName")
            walletQuestionsHTML = walletObject.RenderWalletQuestions(sName, transaction_ID, question_set_ID, request_app_ID, request_client_ID, request_sequence_ID, reference_number)
        End If


        Dim numQuestionsStr = Format(walletObject.NumQuestions, "00")

        Dim sWalletQuestions = (numQuestionsStr + walletQuestionsHTML)
        'sWalletQuestions = sWalletQuestions.Replace("WALLETQUESTION", "WALLETQUESTION_FOLLOWON")

        Return sWalletQuestions
    End Function

    Protected Function determineAuthenticationType(ByRef questionsURL As String, ByRef answersURL As String) As String
        ' Read the attribute from the config.xml file and determinie which type of authentication we need to use

        Select Case _CurrentWebsiteConfig.AuthenticationType.ToUpper()
            Case "RSA"
                questionsURL = getRSAQuestionsURL
                answersURL = submitRSAAnswersURL
                Return "RSA"
            Case "PID"
                questionsURL = getPIDQuestionsURL
                answersURL = submitPIDAnswersURL
                Return "PID"
                'EquiFax
            Case "EID"
                questionsURL = getEquifaxQuestionURL
                answersURL = submitEquifaxAnswerURL
                Return "EID"

            Case "FIS"
                questionsURL = getFISQuestionsURL
                answersURL = submitFISAnswersURL
                Return "FIS"

            Case "DID"
                questionsURL = getDIDQuestionsURL
                answersURL = submitDIDAnswersURL
                Return "DID"
            Case "TID" '' TransUnion
                questionsURL = getTIDQuestionsURL
                answersURL = submitTIDAnswersURL
                Return "TID"
        End Select
        ' If nothing is specified, don't use any method at all
        questionsURL = getRSAQuestionsURL
        answersURL = submitRSAAnswersURL
        Return "NONE"
    End Function

    Protected Function GetOutOfWalletQuestionsRSA(ByVal responseStr As String) As String
        ' Build the XML to submit for getting the Out of Wallet Questions
        ' Build an XML object from the response string 
        Dim myDocument As New XmlDocument
        myDocument.LoadXml(responseStr)

        Dim tempNodes As XmlNodeList
        loanID = ""
        Dim loanIDXMLStr As String
        tempNodes = myDocument.GetElementsByTagName("RESPONSE")
        For Each childNode As XmlNode In tempNodes
            loanIDXMLStr = childNode.Attributes("loan_id").InnerXml
            If (Not String.IsNullOrEmpty(loanIDXMLStr)) Then
                loanID = loanIDXMLStr
            End If
        Next

        If (loanID = "") Then
            Return ""
        End If

        ' Now construct the XML response
        Dim sb As New StringBuilder()
        Dim writer As XmlWriter = XmlTextWriter.Create(sb)
        writer.WriteStartDocument()

        ' begin input
        writer.WriteStartElement("INPUT")

        writer.WriteStartElement("LOGIN")
        'writer.WriteAttributeString("login", "quanntest")
        'writer.WriteAttributeString("password", "lpq123")
        writer.WriteAttributeString("api_user_id", _CurrentWebsiteConfig.APIUser)
        writer.WriteAttributeString("api_password", _CurrentWebsiteConfig.APIPassword)
        writer.WriteEndElement()

        writer.WriteStartElement("REQUEST")
        writer.WriteAttributeString("app_id", loanID)
        'writer.WriteAttributeString("applicant_index", "0")
        If "False" = SafeString(Session("isJoinQuestionDone")) And IsJointApplication Then
            writer.WriteAttributeString("applicant_index", "0")
            writer.WriteAttributeString("is_joint", "true")
            'session("isJoinQuestionDone") = "Done"
        Else
            writer.WriteAttributeString("applicant_index", "0")
            writer.WriteAttributeString("is_joint", "false")
        End If

        'writer.WriteAttributeString("is_joint", IsJointApplication)
        'writer.WriteAttributeString("is_joint", "false")  'join is only used for spouse not-coborrower
        writer.WriteEndElement()

        ' end input
        writer.WriteEndElement()
        writer.Flush()
        writer.Close()

        Dim returnStr = sb.ToString()
        Return returnStr
    End Function

    Protected Function GetOutOfWalletQuestionsPID(ByVal responseStr As String) As String
        ' Build the XML to submit for getting the Out of Wallet Questions
        ' Build an XML object from the response string 
        Dim myDocument As New XmlDocument
        myDocument.LoadXml(responseStr)

        Dim tempNodes As XmlNodeList
        loanID = ""
        Dim loanIDXMLStr As String
        tempNodes = myDocument.GetElementsByTagName("RESPONSE")
        For Each childNode As XmlNode In tempNodes
            loanIDXMLStr = childNode.Attributes("loan_id").InnerXml
            If (Not String.IsNullOrEmpty(loanIDXMLStr)) Then
                loanID = loanIDXMLStr
            End If
        Next

        If (loanID = "") Then
            Return ""
        End If

        ' Now construct the XML response
        Dim sb As New StringBuilder()
        Dim writer As XmlWriter = XmlTextWriter.Create(sb)
        writer.WriteStartDocument()

        ' begin input
        writer.WriteStartElement("INPUT")

        writer.WriteStartElement("LOGIN")
        'writer.WriteAttributeString("api_user_id", "quanntest")
        'writer.WriteAttributeString("api_password", "lpq123")
        writer.WriteAttributeString("api_user_id", _CurrentWebsiteConfig.APIUser)
        writer.WriteAttributeString("api_password", _CurrentWebsiteConfig.APIPassword)

        writer.WriteEndElement()

        writer.WriteStartElement("REQUEST")
        writer.WriteAttributeString("app_id", loanID)
        'writer.WriteAttributeString("applicant_index", "0")
        If "False" = SafeString(Session("isJoinQuestionDone")) And IsJointApplication Then
            writer.WriteAttributeString("applicant_index", "0")
            writer.WriteAttributeString("is_joint", "true")
            'session("isJoinQuestionDone") = "Done"
        Else
            writer.WriteAttributeString("applicant_index", "0")
            writer.WriteAttributeString("is_joint", "false")
        End If

        'writer.WriteAttributeString("is_joint", _IsJointApplication)
        'writer.WriteAttributeString("is_joint", "false")  'join is only used for spouse not-coborrower
        writer.WriteEndElement()

        ' end input
        writer.WriteEndElement()
        writer.Flush()
        writer.Close()

        Dim returnStr = sb.ToString()
        Return returnStr
    End Function

    Protected Function GetOutOfWalletQuestionsFIS(ByVal responseStr As String) As String
        ' Build the XML to submit for getting the Out of Wallet Questions
        ' Build an XML object from the response string 
        Dim myDocument As New XmlDocument
        myDocument.LoadXml(responseStr)

        Dim tempNodes As XmlNodeList
        loanID = ""
        Dim loanIDXMLStr As String
        tempNodes = myDocument.GetElementsByTagName("RESPONSE")
        For Each childNode As XmlNode In tempNodes
            loanIDXMLStr = childNode.Attributes("loan_id").InnerXml
            If (Not String.IsNullOrEmpty(loanIDXMLStr)) Then
                loanID = loanIDXMLStr
            End If
        Next

        If (loanID = "") Then
            Return ""
        End If

        ' Now construct the XML response
        Dim sb As New StringBuilder()
        Dim writer As XmlWriter = XmlTextWriter.Create(sb)
        writer.WriteStartDocument()

        ' begin input
        writer.WriteStartElement("INPUT")

        writer.WriteStartElement("LOGIN")

        'FIS IDA ONLY work with lender level user
        If _CurrentWebsiteConfig.APIUserLender <> "" Then
            writer.WriteAttributeString("api_user_id", _CurrentWebsiteConfig.APIUserLender)
            writer.WriteAttributeString("api_password", _CurrentWebsiteConfig.APIPasswordLender)
        Else
            writer.WriteAttributeString("api_user_id", _CurrentWebsiteConfig.APIUser)
            writer.WriteAttributeString("api_password", _CurrentWebsiteConfig.APIPassword)
        End If

        writer.WriteEndElement()

        writer.WriteStartElement("REQUEST")
        writer.WriteAttributeString("xpressappid", loanID)

        If SafeString(Session("isJoinQuestionDone")) = "False" And IsJointApplication Then
            writer.WriteAttributeString("applicant_index", "0")
            writer.WriteAttributeString("applicant_type", "JOINT")  'primary is used for both primary and coborrower, there is no spouse     
        Else
            writer.WriteAttributeString("applicant_index", "0")
            writer.WriteAttributeString("applicant_type", "PRIMARY")    'primary is used for both primary and coborrower, there is no spouse     
        End If

        writer.WriteEndElement()

        ' end input
        writer.WriteEndElement()
        writer.Flush()
        writer.Close()

        Dim returnStr = sb.ToString()
        Return returnStr
    End Function

    Protected Function GetOutOfWalletQuestionsDID(ByVal responseStr As String) As String
        ' Build the XML to submit for getting the Out of Wallet Questions
        ' Build an XML object from the response string 
        Dim myDocument As New XmlDocument
        myDocument.LoadXml(responseStr)

        Dim tempNodes As XmlNodeList
        loanID = ""
        Dim loanIDXMLStr As String
        tempNodes = myDocument.GetElementsByTagName("RESPONSE")
        For Each childNode As XmlNode In tempNodes
            loanIDXMLStr = childNode.Attributes("loan_id").InnerXml
            If (Not String.IsNullOrEmpty(loanIDXMLStr)) Then
                loanID = loanIDXMLStr
            End If
        Next

        If (loanID = "") Then
            Return ""
        End If

        ' Now construct the XML response
        Dim sb As New StringBuilder()
        Dim writer As XmlWriter = XmlTextWriter.Create(sb)
        writer.WriteStartDocument()

        ' begin input
        writer.WriteStartElement("INPUT")

        writer.WriteStartElement("LOGIN")
        writer.WriteAttributeString("api_user_id", _CurrentWebsiteConfig.APIUser)
        writer.WriteAttributeString("api_password", _CurrentWebsiteConfig.APIPassword)

        writer.WriteEndElement()

        writer.WriteStartElement("REQUEST")
        writer.WriteAttributeString("xpress_app_id", loanID)

        If SafeString(Session("isJoinQuestionDone")) = "False" And IsJointApplication Then
            writer.WriteAttributeString("applicant_index", "0")
            writer.WriteAttributeString("is_joint", "true")
        Else
            writer.WriteAttributeString("applicant_index", "0")
            writer.WriteAttributeString("is_joint", "false")
        End If

        'writer.WriteAttributeString("is_joint", "false")  'join is only used for spouse not-coborrower
        writer.WriteEndElement()

        ' end input
        writer.WriteEndElement()
        writer.Flush()
        writer.Close()

        Dim returnStr = sb.ToString()
        Return returnStr
    End Function

    'Equifax
    Protected Function GetOutOfWalletQuestionsEquifax(ByVal responseStr As String) As String
        ' Build the XML to submit for getting the Out of Wallet Questions
        ' Build an XML object from the response string 
        Dim myDocument As New XmlDocument
        myDocument.LoadXml(responseStr)

        Dim tempNodes As XmlNodeList
        loanID = ""
        Dim loanIDXMLStr As String
        tempNodes = myDocument.GetElementsByTagName("RESPONSE")
        For Each childNode As XmlNode In tempNodes
            loanIDXMLStr = childNode.Attributes("loan_id").InnerXml
            If (Not String.IsNullOrEmpty(loanIDXMLStr)) Then
                loanID = loanIDXMLStr
            End If
        Next

        If (loanID = "") Then
            Return ""
        End If

        ' Now construct the XML response
        Dim sb As New StringBuilder()
        Dim writer As XmlWriter = XmlTextWriter.Create(sb)
        writer.WriteStartDocument()

        ' begin input
        writer.WriteStartElement("INPUT")

        writer.WriteStartElement("LOGIN")
        'writer.WriteAttributeString("login", "quanntest")
        'writer.WriteAttributeString("password", "lpq123")
        writer.WriteAttributeString("api_user_id", _CurrentWebsiteConfig.APIUser)
        writer.WriteAttributeString("api_password", _CurrentWebsiteConfig.APIPassword)
        writer.WriteEndElement()

        writer.WriteStartElement("REQUEST")
        writer.WriteAttributeString("app_id", loanID)
        If "False" = SafeString(Session("isJoinQuestionDone")) And IsJointApplication Then
            writer.WriteAttributeString("applicant_index", "0")
            writer.WriteAttributeString("is_joint", "true")
            'session("isJoinQuestionDone") = "Done"
        Else
            writer.WriteAttributeString("applicant_index", "0")
            writer.WriteAttributeString("is_joint", "false")
        End If

        writer.WriteEndElement()

        ' end input
        writer.WriteEndElement()
        writer.Flush()
        writer.Close()

        Dim returnStr = sb.ToString()
        Return returnStr
    End Function
    ''TransUnionIDMA
    Protected Function GetOutOfWalletQuestionsTID(ByVal responseStr As String) As String
        ' Build the XML to submit for getting the Out of Wallet Questions
        ' Build an XML object from the response string 
        Dim myDocument As New XmlDocument
        myDocument.LoadXml(responseStr)

        Dim tempNodes As XmlNodeList
        loanID = ""
        Dim loanIDXMLStr As String
        tempNodes = myDocument.GetElementsByTagName("RESPONSE")
        For Each childNode As XmlNode In tempNodes
            loanIDXMLStr = childNode.Attributes("loan_id").InnerXml
            If (Not String.IsNullOrEmpty(loanIDXMLStr)) Then
                loanID = loanIDXMLStr
            End If
        Next

        If (loanID = "") Then
            Return ""
        End If

        ' Now construct the XML response
        Dim sb As New StringBuilder()
        Dim writer As XmlWriter = XmlTextWriter.Create(sb)
        writer.WriteStartDocument()

        ' begin input
        writer.WriteStartElement("INPUT")

        writer.WriteStartElement("LOGIN")
        writer.WriteAttributeString("api_user_id", _CurrentWebsiteConfig.APIUser)
        writer.WriteAttributeString("api_password", _CurrentWebsiteConfig.APIPassword)
        writer.WriteEndElement()

        writer.WriteStartElement("REQUEST")
        writer.WriteAttributeString("xpressappid", loanID)
        writer.WriteAttributeString("mode", "IDA")
        writer.WriteAttributeString("applicant_index", "0")
        If "False" = SafeString(Session("isJoinQuestionDone")) And IsJointApplication Then
            writer.WriteAttributeString("applicant_type", "JOINT")
            'Session("isJoinQuestionDone") = "Done"
        Else
            writer.WriteAttributeString("applicant_type", "PRIMARY")
        End If

        writer.WriteEndElement()

        ' end input
        writer.WriteEndElement()
        writer.Flush()
        writer.Close()

        Dim returnStr = sb.ToString()
        Return returnStr
    End Function
    ''TransUnion build wallet answer
    Protected Function buildWalletAnswerXMLTID(ByVal questionIDList As List(Of String), ByVal answerIDList As List(Of String)) As String
        Dim sb As New StringBuilder()
        Dim writer As XmlWriter = XmlTextWriter.Create(sb)
        writer.WriteStartDocument()

        ' begin input
        writer.WriteStartElement("INPUT")

        writer.WriteStartElement("LOGIN")
        writer.WriteAttributeString("api_user_id", _CurrentWebsiteConfig.APIUser)
        writer.WriteAttributeString("api_password", _CurrentWebsiteConfig.APIPassword)
        writer.WriteEndElement()
        writer.WriteStartElement("REQUEST")
        writer.WriteAttributeString("xpressappid", loanID)
        writer.WriteAttributeString("applicant_index", "0")
        writer.WriteAttributeString("mode", "IDA")
        writer.WriteAttributeString("reference_number", reference_number)
        If "False" = SafeString(Session("isJoinQuestionDone")) And IsJointApplication Then
            writer.WriteAttributeString("applicant_type", "JOINT")
            Session("isJoinQuestionDone") = "Done"
        Else
            writer.WriteAttributeString("applicant_type", "PRIMARY")
        End If
        Dim numQuestions As Integer = questionIDList.Count
        writer.WriteStartElement("ANSWERS")
        For i As Integer = 0 To numQuestions - 1
            writer.WriteStartElement("ANSWER")
            writer.WriteAttributeString("question_name", questionIDList(i))
            writer.WriteAttributeString("choice_key", answerIDList(i))
            writer.WriteEndElement()
        Next
        ' END </ANSWERS>
        writer.WriteEndElement()

        ' END </REQUEST>
        writer.WriteEndElement()

        ' END </INPUT>
        writer.WriteEndElement()
        writer.Flush()
        writer.Close()

        Dim returnStr As String = sb.ToString()

        Return returnStr
    End Function

    ''end transunion

    Protected Function LoadTemplateXML() As String
        Dim sb As New StringBuilder()

        Dim writer As XmlWriter = XmlTextWriter.Create(sb)
        writer.WriteStartDocument()
        ' begin input
        writer.WriteStartElement("INPUT")

        ' begin login
        writer.WriteStartElement("LOGIN")
        '      writer.WriteAttributeString("login", "quanntest")
        'writer.WriteAttributeString("password", "lpq123")
        writer.WriteAttributeString("api_user_id", _CurrentWebsiteConfig.APIUser)
        writer.WriteAttributeString("api_password", _CurrentWebsiteConfig.APIPassword)
        writer.WriteEndElement()
        ' end login

        ' begin request
        writer.WriteStartElement("REQUEST")
        writer.WriteAttributeString("action_type", "NEW")

        writer.WriteStartElement("LOAN_DATA")
        writer.WriteAttributeString("loan_type", "XA")
        Dim oLoanApp As LoanAppInfo = CreateLoanApp()
        writer.WriteCData(oLoanApp.ToXmlString(IsJointApplication))
        writer.WriteEndElement()
        ' end request
        writer.WriteEndElement()
        ' end input
        writer.WriteEndElement()
        writer.WriteEndDocument()
        writer.Close()
        Return sb.ToString()
    End Function

    Protected Sub updateLoan(ByVal psLoanStatus As String, Optional ByVal sIncompleteAppLoanID As String = "", Optional ByVal sIncompleteAppLoanRequestXML As String = "")

        ' Make changes to it
        ' Submit the changes with the "UPDATE" command
        If loanID = "" Then Return

        Dim url As String = getLoansURL

        Dim getLoansXMLRequestStr As String = LoanRetrieval.BuildGetLoansXML(loanID, _CurrentWebsiteConfig.APIUser, _CurrentWebsiteConfig.APIPassword)

        If sIncompleteAppLoanID <> "" Then
            getLoansXMLRequestStr = LoanRetrieval.BuildGetLoansXML(sIncompleteAppLoanID, _CurrentWebsiteConfig.APIUser, _CurrentWebsiteConfig.APIPassword)
        End If

        '    ' Done building the XML, now ship it off
        Dim req As WebRequest = WebRequest.Create(url)

        req.Method = "POST"
        req.ContentType = "text/xml"

        log.Info("XA: Preparing to POST GetLoans Request: " & CSecureStringFormatter.MaskSensitiveXMLData(getLoansXMLRequestStr))

        Dim writer As New StreamWriter(req.GetRequestStream())
        writer.Write(getLoansXMLRequestStr)
        writer.Close()

        Dim res As WebResponse = req.GetResponse()

        Dim reader As New StreamReader(res.GetResponseStream())
        Dim sXml As String = reader.ReadToEnd()

        log.Info("XA: GetLoans Response: " + CSecureStringFormatter.MaskSensitiveXMLData(sXml))

        ' Extract the inner xml and update the status of the loans as "APPROVED"
        Dim responseXMLDoc As XmlDocument = New XmlDocument()
        responseXMLDoc.LoadXml(sXml)
        Dim innerXMLStr As String = ""
        innerXMLStr = responseXMLDoc.InnerText
        Dim innerXMLDoc As XmlDocument = New XmlDocument()
        innerXMLDoc.LoadXml(innerXMLStr)

        ' Grab all the interested accounts from the origianl request
        Dim origRequestXMLDoc As XmlDocument = New XmlDocument()
        If sIncompleteAppLoanRequestXML <> "" Then
            origRequestXMLDoc.LoadXml(sIncompleteAppLoanRequestXML)
        Else
            origRequestXMLDoc.LoadXml(loanRequestXMLStr)
        End If

        Dim origInnerXMLDoc As XmlDocument = New XmlDocument()
        origInnerXMLDoc.LoadXml(origRequestXMLDoc.InnerText)
        Dim xmlFundingSources As XmlNodeList = origInnerXMLDoc.GetElementsByTagName("FUNDING_SOURCE")
        Dim sFundingSoure As String = ""
        If xmlFundingSources IsNot Nothing AndAlso xmlFundingSources.Count > 0 Then
            sFundingSoure = CType(xmlFundingSources(0), XmlElement).GetAttribute("clf_funding_source_id")
        End If

        '---Update status
        Dim loanInfoNodes As XmlNodeList = innerXMLDoc.GetElementsByTagName("LOAN_INFO")
        Dim loanInfoNode As XmlElement = loanInfoNodes(0)
        If "FRAUD" = loanInfoNode.Attributes("status").InnerXml Or "DUP" = loanInfoNode.Attributes("status").InnerXml Then
            _bIsFraud = True
            Return ' don't update status when credit report has fraud warning or status is fraud or DUP
        End If
        loanInfoNode.SetAttribute("status", psLoanStatus)
        '' set the approval/denial date to current date
        loanInfoNode.SetAttribute("approval_date", SafeDateTimeXml(Now.ToString()))

        'Update Internnal comment
        If _sInternalCommentAppend <> "" Then
            Dim internCommentNodes As XmlNodeList = innerXMLDoc.GetElementsByTagName("INTERNAL_COMMENTS")
            Dim internCommentNode As XmlElement = internCommentNodes(0)
            _sInternalCommentAppend = "SYSTEM (" + DateTime.Now + "): " & _sInternalCommentAppend & Environment.NewLine & Environment.NewLine
            internCommentNode.InnerText = _sInternalCommentAppend & internCommentNode.InnerText
        End If

        ''Update Approved account
        ''''update Date & funding source(because decisionXA didn't copy funding source for approved account)
        Dim ApprovedNodeList As XmlNodeList
        ApprovedNodeList = innerXMLDoc.GetElementsByTagName("ACCOUNT_TYPE")
        Dim DateElement As XmlElement
        For Each childNode As XmlNode In ApprovedNodeList
            If childNode.ParentNode.Name <> "APPROVED_ACCOUNTS" Then Continue For
            DateElement = childNode
            DateElement.SetAttribute("issue_date", SafeDateXml(Now.ToString()))
            If DateElement.GetAttribute("clf_funding_source_id") = "" And sFundingSoure <> "" Then
                DateElement.SetAttribute("clf_funding_source_id", sFundingSoure)
            End If

        Next

        'fix missing clf_funding_source_id in FUNDING_SOURCE after getloan -
        Dim FundingSourceNodeList As XmlNodeList
        FundingSourceNodeList = innerXMLDoc.GetElementsByTagName("FUNDING_SOURCE")
        For Each childNode As XmlNode In FundingSourceNodeList
            Dim oXmnlFundingSource = CType(childNode, XmlElement)
            If oXmnlFundingSource.GetAttribute("clf_funding_source_id") = "" And sFundingSoure <> "" Then
                oXmnlFundingSource.SetAttribute("clf_funding_source_id", sFundingSoure)
            End If
        Next

        Dim cdataSection As XmlCDataSection
        origRequestXMLDoc.SelectSingleNode("//LOAN_DATA").RemoveAll()
        cdataSection = origRequestXMLDoc.CreateCDataSection(innerXMLDoc.InnerXml)
        origRequestXMLDoc.SelectSingleNode("//LOAN_DATA").AppendChild(cdataSection)

        ' Add the loan_id to LOAN_DATA
        Dim loanIDAttributeElement As XmlElement = origRequestXMLDoc.SelectSingleNode("//REQUEST")
        'loanIDAttributeElement.RemoveAllAttributes()
        loanIDAttributeElement.SetAttribute("loan_id", loanID)

        'responseXMLDoc.InnerText = innerXMLDoc.InnerXml
        ' Change LOAN_INFO status="UPDATE"
        ' For some reason, selectsinglenode doesn't work on the LOAN_INFO element, so I have to manually get it

        '' Set the version of the CLF
        'Dim versionNode As XmlNode = innerXMLDoc.DocumentElement
        'Dim versionElement As XmlElement = versionNode
        'versionElement.SetAttribute("version", "5.069")

        ' Change LOAN_DATA loan_type="XA" and REQUEST action_type="UPDATE"

        Dim loanDataElement As XmlElement = origRequestXMLDoc.SelectSingleNode("//LOAN_DATA")
        loanDataElement.SetAttribute("loan_type", "XA")
        loanDataElement = origRequestXMLDoc.SelectSingleNode("//REQUEST")
        loanDataElement.SetAttribute("action_type", "UPDATE")
        log.Info("XA: Preparing to Update loan data: " + CSecureStringFormatter.MaskSensitiveXMLData(origRequestXMLDoc.InnerXml))

        ' Now send the loan request back with the updated "APPROVED_ACCOUNTS"

        url = loanSubmitURL
        req = WebRequest.Create(url)
        req.Method = "POST"
        req.ContentType = "text/xml"
        writer = New StreamWriter(req.GetRequestStream())
        writer.Write(origRequestXMLDoc.InnerXml)
        writer.Close()

        res = req.GetResponse()

        reader = New StreamReader(res.GetResponseStream())
        sXml = reader.ReadToEnd()

        log.Info("XA: Update loan response: " + sXml)

    End Sub

    Protected Function hasPassedAuthentication(ByVal authMethod As String, ByVal responseXMLStr As String) As Boolean
        ' Return true if we passed the authentication 
        ' We have to know which authentication method was used.
        Dim responseXMLDoc As XmlDocument = New XmlDocument()
        responseXMLDoc.LoadXml(responseXMLStr)

        ' Get the result depending on the authentication method
        Dim successAttribute As String = ""
        Dim successValue As String = ""
        Select Case authMethod.ToUpper()
            Case "RSA"
                successAttribute = "transaction_result"
                successValue = "PASSED"
            Case "PID"
                successAttribute = "decision"
                successValue = "ACCEPT"
            Case "FIS"
                successAttribute = "decision"
                successValue = "PASS"
            Case "DID"
                successAttribute = "decision"
                successValue = "PASSED"
                'equifax 
            Case "EID"
                successAttribute = "decision_code"
                successValue = "Y"
                ''transunion
            Case "TID"
                successAttribute = "decision"
                successValue = "ACCEPT"
            Case Else
                successAttribute = "transaction_result"
                successValue = "ACCEPT"
        End Select

        ' Get the attribute from the XML response
        Dim responseAttribute As String = ""
        Dim responseAttributeNode As XmlNode = responseXMLDoc.SelectSingleNode("//@" + successAttribute)
        'equifax - to avoid the exception error in case the node has nothing
        If responseAttributeNode IsNot Nothing Then
            responseAttribute = responseAttributeNode.InnerXml
        End If
        ' Compare the result with the proper success result
        If responseAttribute.ToUpper() = successValue.ToUpper() Then
            Return True
        Else
            Return False
        End If

    End Function

    'TODO: this is only for DID
    Protected Function hasFollowOnQuestion(ByVal authMethod As String, ByVal responseXMLStr As String) As Boolean
        ' Return true if there is follow on question
        ' We have to know which authentication method was used.
        Dim responseXMLDoc As XmlDocument = New XmlDocument()
        responseXMLDoc.LoadXml(responseXMLStr)

        ' Get the result depending on the authentication method
        Dim successAttribute As String = ""
        Dim successValue As String = ""
        Select Case authMethod.ToUpper()
            'Case "RSA"
            '	successAttribute = "transaction_result"
            '	successValue = "PASSED"
            'Case "PID"
            '	successAttribute = "decision"
            '	successValue = "ACCEPT"
            'Case "FIS"
            '	successAttribute = "decision"
            '	successValue = "PASS"
            Case "DID"
                successAttribute = "decision"
                successValue = "MORE QUESTIONS"
                'equifax 
                'Case "EID"
                '	successAttribute = "decision_code"
                '	successValue = "Y"
                '	''transunion
                'Case "TID"
                '	successAttribute = "decision"
                '	successValue = "ACCEPT"
            Case Else
                successAttribute = "transaction_result"
                successValue = "bogus blah so this method return a fail when not DID"
        End Select

        ' Get the attribute from the XML response
        Dim responseAttribute As String = ""
        Dim responseAttributeNode As XmlNode = responseXMLDoc.SelectSingleNode("//@" + successAttribute)
        'equifax - to avoid the exception error in case the node has nothing
        If responseAttributeNode IsNot Nothing Then
            responseAttribute = responseAttributeNode.InnerXml
        End If
        ' Compare the result with the proper success result
        If responseAttribute.ToUpper() = successValue.ToUpper() Then
            Return True
        Else
            Return False
        End If

    End Function


    Protected Function isXAProductQualified(ByVal responseXMLStr As String) As Boolean
        Try

            Dim responseXMLDoc As XmlDocument = New XmlDocument()
            responseXMLDoc.LoadXml(responseXMLStr)
            ' Get the attribute from the XML response
            Dim responseAttribute As String = ""
            Dim responseNode As XmlNode = responseXMLDoc.SelectSingleNode("//DECISION")
            If responseNode Is Nothing Then Return False
            responseAttribute = Common.SafeString(responseNode.Attributes("status").Value)
            ' Compare the result with the proper success result
            If responseAttribute.ToUpper() = "QUALIFIED" Then
                Return True
            Else
                Return False
            End If
        Catch ex As Exception
            log.Error("Error processing decisionXA response: " & ex.Message, ex)
        End Try
        Return False
    End Function


    ''' <summary>
    ''' True=accept  else = refer
    ''' </summary>
    ''' <param name="authMethod"></param>
    ''' <param name="responseXMLStr"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Protected Function isEFundsSuccessful(ByVal responseXMLStr As String) As Boolean
        ' Return true if eFunds was successful
        Dim responseXMLDoc As XmlDocument = New XmlDocument()
        responseXMLDoc.LoadXml(responseXMLStr)
        ' Get the attribute from the XML response
        Dim responseAttribute As String = ""
        Dim passedIdentityManager As String = ""
        Dim passedOFAC As String = ""

        Dim responseAttributeNode As XmlNode = responseXMLDoc.SelectSingleNode("//@qualifile_decision")
        Dim resIdentityManagerNode As XmlNode = responseXMLDoc.SelectSingleNode("//@passed_identity_manager")
        Dim resOFACNode As XmlNode = responseXMLDoc.SelectSingleNode("//@passed_ofac")

        '' check qualify_decision
        If responseAttributeNode Is Nothing Then Return False

        responseAttribute = responseAttributeNode.InnerXml.ToUpper
        Dim qualifileDecision As Boolean = If(responseAttribute = "ACCEPT" Or responseAttribute = "APPROVE" Or responseAttribute = "APPROVED", True, False)

        ''exist quailifeDecision, identiyManagerNode and OFACNode 
        If resIdentityManagerNode IsNot Nothing And resOFACNode IsNot Nothing Then
            passedIdentityManager = resIdentityManagerNode.InnerXml.ToUpper
            passedOFAC = resOFACNode.InnerXml.ToUpper
            If qualifileDecision And passedIdentityManager = "Y" And passedOFAC = "Y" Then
                Return True ''success result
            Else
                Return False
            End If
        ElseIf resIdentityManagerNode IsNot Nothing And resOFACNode Is Nothing Then ''exist qualifileDecision and IdentityMangager
            passedIdentityManager = resIdentityManagerNode.InnerXml.ToUpper
            If qualifileDecision And passedIdentityManager = "Y" Then
                Return True ''success result
            Else
                Return False
            End If
        ElseIf resIdentityManagerNode Is Nothing And resOFACNode IsNot Nothing Then ''exist qualifile decision and OFAC
            passedOFAC = resOFACNode.InnerXml.ToUpper
            If qualifileDecision And passedOFAC = "Y" Then
                Return True ''success result
            Else
                Return False
            End If
        End If

        ''by default exist only qualifile decision
        If qualifileDecision Then
            Return True
        Else
            Return False
        End If
    End Function

    Protected Function isRBankingSuccessful(ByVal responseXMLStr As String) As Boolean
        Dim responseXMLDoc As XmlDocument = New XmlDocument()
        responseXMLDoc.LoadXml(responseXMLStr)
        Dim DebitType As String = _CurrentWebsiteConfig.DebitType
        ''enable the retailbanking when DebitType = "EID"
        If (DebitType.ToUpper = "EID") Then
            ''get the attribute from the xml response
            Dim responseAttribute As String = ""
            Dim responseAttributeNode As XmlNode = responseXMLDoc.SelectSingleNode("//@decision_code")
            If responseAttributeNode Is Nothing Then Return False
            responseAttribute = responseAttributeNode.InnerXml
            ' Compare the result with the proper success result
            If responseAttribute.ToUpper() = "APPROVED" Then
                Return True
            Else
                Return False
            End If
        Else
            Return False
        End If
    End Function
    Protected Function executeUpLoadDocument() As Boolean
        Dim SubmitUploadDocumentUrl As String = ""
        Dim uploadDocument As String = BuildUploadDocumentsRequest(SubmitUploadDocumentUrl)
        If String.IsNullOrEmpty(SubmitUploadDocumentUrl) Then ''if URL is empty return false and exit
            Return False
        End If
        Dim req As WebRequest = WebRequest.Create(SubmitUploadDocumentUrl)
        req.Method = "POST"
        req.ContentType = "text/xml"

        log.Info("Preparing to POST upload document: " & Left(CSecureStringFormatter.MaskSensitiveXMLData(uploadDocument), 100))

        Dim writer As New StreamWriter(req.GetRequestStream())
        writer.Write(uploadDocument)
        writer.Close()

        Dim res As WebResponse = req.GetResponse()

        Dim reader As New StreamReader(res.GetResponseStream())
        Dim sXml As String = reader.ReadToEnd()
        log.Info("XA: Response from server(upload document): " + sXml)
        ''need to make sure submit document to the system success
        Dim xmlDoc As New XmlDocument()
        xmlDoc.LoadXml(sXml)
        Dim responseNode As XmlElement = xmlDoc.SelectSingleNode("OUTPUT/RESPONSE")
        If responseNode.GetAttribute("status") = "FAILURE" Then ''May be there is not enough space on the disk ....
            Return False
        End If
        reader.Close()
        res.GetResponseStream().Close()
        req.GetRequestStream().Close()

        If Not IsSubmitted(sXml) Then
            Return False
        End If
        Return True
    End Function
    Protected Function BuildUploadDocumentsRequest(ByRef UpLoadDocumentURL As String) As String
        Dim oDocCleaned As New Dictionary(Of String, String) 'preprocess or format document type
        Dim oLoanApp As LoanAppInfo = CreateLoanApp()
        Dim sb As New StringBuilder()
        Dim writer As XmlWriter = XmlTextWriter.Create(sb)
        If (oLoanApp.DocBase64Dic.Count > 0) Then ''make sure  docbase64dic is not null
            ' Prepare the various URLs
            Dim SubmitURL = _CurrentWebsiteConfig.BaseSubmitLoanUrl
            UpLoadDocumentURL = SubmitURL.Substring(0, SubmitURL.IndexOf("services")) + "Services/ipad/iPadDocumentSubmit.aspx"
            For Each doc As KeyValuePair(Of String, String) In oLoanApp.DocBase64Dic
                Dim fileExtension As String = ""
                If doc.Key.LastIndexOf(".") > 0 Then  ''data:application/pdf;base64,
                    fileExtension = doc.Key.Substring(doc.Key.LastIndexOf(".") + 1)
                End If
                If fileExtension = "" Then
                    log.Info("Invalid file format: missing file extension")
                    Continue For
                End If

                If fileExtension.ToUpper = "PDF" Then
                    Dim sPDF As String = doc.Value.Replace("data:application/pdf;base64,", "")
                    sPDF = sPDF.Replace(" ", "") ''remove white space if it exist
                    ''make sure input string is base64string 
                    If (sPDF.Length Mod 4 = 0) Then
                        Dim sPDFByte As Byte() = Convert.FromBase64String(sPDF)
                        Dim sPDFString As String = Convert.ToBase64String(sPDFByte)
                        oDocCleaned.Add(doc.Key, sPDFString)
                    Else
                        log.Info("The input string '" & doc.Key & "' is not base64string")
                    End If
                ElseIf fileExtension.ToUpper = "PNG" Or fileExtension.ToUpper = "JPG" Or fileExtension.ToUpper = "GIF" Then 'need to convert to pdf
                    Dim sImage As String = doc.Value.Replace("data:image/png;base64,", "")
                    ''need to replace for all cases jpeg, png, jpg, and gif
                    sImage = sImage.Replace("data:image/jpeg;base64,", "")
                    sImage = sImage.Replace("data:image/jpg;base64,", "")
                    sImage = sImage.Replace("data:image/gif;base64,", "")
                    'Dim sImage = StreamFile2Base64("front_license3.jpg")
                    sImage = sImage.Replace(" ", "") ''remove white space if it exist
                    ''make sure input string is base64string
                    If (sImage.Length Mod 4 = 0) Then
                        Dim base64PDF = convertBase64Image2Base64PDF(sImage, doc.Key)
                        oDocCleaned.Add(doc.Key, base64PDF)
                    Else
                        log.Info("The input string of '" & doc.Key & "' is not base64string")
                    End If
                Else
                    log.Info("Invalid file extension: " & fileExtension)
                End If
            Next

            If oDocCleaned.Count = 0 Then
                Return Nothing
            End If

            ' Now construct the XML response

            writer.WriteStartDocument()

            ' begin input
            writer.WriteStartElement("INPUT")
            writer.WriteStartElement("LOGIN")
            writer.WriteAttributeString("api_user_id", _CurrentWebsiteConfig.APIUser)
            writer.WriteAttributeString("api_password", _CurrentWebsiteConfig.APIPassword)
            writer.WriteEndElement()

            writer.WriteStartElement("REQUEST")
            writer.WriteAttributeString("app_num", loanNumber)
            writer.WriteAttributeString("app_type", "DEPOSIT")
            writer.WriteEndElement() ''request
            writer.WriteStartElement("DOCUMENTS")
            ''for loop for each document
            For Each item As KeyValuePair(Of String, String) In oDocCleaned
                ''get DocCode and DocGroup user input
                Dim docGroup As String = ""
                Dim docCode As String = ""
                Dim docTitle As String = ""
                If oLoanApp.oUploadDocumentInfoList.Count > 0 Then
                    For Each oUploadDocInfo As uploadDocumentInfo In oLoanApp.oUploadDocumentInfoList
                        If item.Key = oUploadDocInfo.docFileName Then
                            docGroup = oUploadDocInfo.docGroup
                            docCode = oUploadDocInfo.docCode
                            docTitle = oUploadDocInfo.docTitle
                            Exit For
                        End If
                    Next
                End If
                writer.WriteStartElement("DOCUMENT")
                If String.IsNullOrEmpty(docTitle) Then

                    docTitle = item.Key
                End If
                writer.WriteAttributeString("title", docTitle)
                writer.WriteAttributeString("DocID", System.Guid.NewGuid().ToString)
                If Not String.IsNullOrEmpty(docCode) Then
                    writer.WriteAttributeString("DocCode", docCode)
                End If
                If Not String.IsNullOrEmpty(docGroup) Then
                    writer.WriteAttributeString("DocGroup", docGroup)
                End If
                writer.WriteValue(item.Value)
                writer.WriteEndElement() 'end documents
            Next
            writer.WriteEndElement() ''end documents


            writer.WriteEndElement() ''end input
            writer.Flush()
            writer.Close()
        End If
        Dim returnStr = sb.ToString()
        Return returnStr
    End Function

    Protected Function BuildCreditPullXMLRequest() As String
        ' Now construct the XML response
        Dim sb As New StringBuilder()
        Dim writer As XmlWriter = XmlTextWriter.Create(sb)
        writer.WriteStartDocument()

        ' begin input
        writer.WriteStartElement("INPUT")

        writer.WriteStartElement("LOGIN")
        writer.WriteAttributeString("api_user_id", _CurrentWebsiteConfig.APIUser)
        writer.WriteAttributeString("api_password", _CurrentWebsiteConfig.APIPassword)
        writer.WriteEndElement()

        writer.WriteStartElement("REQUEST")
        writer.WriteAttributeString("app_id", loanID)
        writer.WriteEndElement()

        writer.WriteEndElement() ' end input
        writer.Flush()
        writer.Close()

        Dim returnStr = sb.ToString()
        Return returnStr
    End Function

    Protected Function BuildDecisionXAXMLRequest() As String
        ' Now construct the XML response
        Dim sb As New StringBuilder()
        Dim writer As XmlWriter = XmlTextWriter.Create(sb)
        writer.WriteStartDocument()

        ' begin input
        writer.WriteStartElement("INPUT")
        writer.WriteStartElement("LOGIN")
        writer.WriteAttributeString("api_user_id", _CurrentWebsiteConfig.APIUser)
        writer.WriteAttributeString("api_password", _CurrentWebsiteConfig.APIPassword)
        writer.WriteEndElement() 'end LOGIN

        writer.WriteStartElement("REQUEST")
        writer.WriteAttributeString("xpress_app_id", loanID)
        writer.WriteAttributeString("action_type", "UPDATE")
        writer.WriteEndElement() 'end REQUEST

        writer.WriteEndElement() ' end input
        writer.Flush()
        writer.Close()

        Dim returnStr = sb.ToString()
        Return returnStr
    End Function
    Protected Function BuildRetailBankingXMLRequest(Optional ByVal pbIsJoint As Boolean = False) As String
        ''construct the xml response
        Dim sb As New StringBuilder()
        Dim writer As XmlWriter = XmlWriter.Create(sb)
        writer.WriteStartDocument()
        ''begin input
        writer.WriteStartElement("INPUT")
        writer.WriteStartElement("LOGIN")
        writer.WriteAttributeString("api_user_id", _CurrentWebsiteConfig.APIUser)
        writer.WriteAttributeString("api_password", _CurrentWebsiteConfig.APIPassword)
        writer.WriteEndElement()
        writer.WriteStartElement("REQUEST")
        writer.WriteAttributeString("xpressappid", loanID)
        If pbIsJoint = False Then
            writer.WriteAttributeString("applicant_index", "0")
            writer.WriteAttributeString("applicant_type", "PRIMARY")
        Else
            writer.WriteAttributeString("applicant_index", "0")
            writer.WriteAttributeString("applicant_type", "JOINT")
        End If
        'writer.WriteAttributeString("applicant_type", "PRIMARY")
        writer.WriteEndElement()
        'end input
        writer.WriteEndElement()
        writer.Flush()
        writer.Close()
        Dim returnStr = sb.ToString()
        Return returnStr
    End Function
    Protected Function BuildeFundsXMLRequest(Optional ByVal pbIsJoint As Boolean = False) As String

        ' Now construct the XML response
        Dim sb As New StringBuilder()
        Dim writer As XmlWriter = XmlTextWriter.Create(sb)
        writer.WriteStartDocument()

        ' begin input
        writer.WriteStartElement("INPUT")

        writer.WriteStartElement("LOGIN")
        'writer.WriteAttributeString("login", "quanntest")
        'writer.WriteAttributeString("password", "lpq123")
        writer.WriteAttributeString("api_user_id", _CurrentWebsiteConfig.APIUser)
        writer.WriteAttributeString("api_password", _CurrentWebsiteConfig.APIPassword)
        writer.WriteEndElement()

        writer.WriteStartElement("REQUEST")
        'writer.WriteAttributeString("app_id", loanID)
        writer.WriteAttributeString("xpressappid", loanID)
        If pbIsJoint = False Then
            writer.WriteAttributeString("applicant_index", "0")
            writer.WriteAttributeString("applicant_type", "PRIMARY")
        Else
            writer.WriteAttributeString("applicant_index", "0")
            writer.WriteAttributeString("applicant_type", "JOINT")
        End If
        'writer.WriteAttributeString("applicant_type", "PRIMARY")
        writer.WriteEndElement()

        ' end input
        writer.WriteEndElement()
        writer.Flush()
        writer.Close()
        Dim returnStr = sb.ToString()
        Return returnStr
    End Function

    Protected Function buildWalletAnswerXML(ByVal questionIDList As List(Of String), ByVal answerIDList As List(Of String), _
     ByVal authenticationMethod As String) As String
        Select Case authenticationMethod.ToUpper()
            Case "RSA"
                Return buildWalletAnswerXMLRSA(questionIDList, answerIDList)
            Case "PID"
                Return buildWalletAnswerXMLPID(questionIDList, answerIDList)
            Case "FIS"
                Return buildWalletAnswerXMLFIS(questionIDList, answerIDList)
            Case "DID"
                Return buildWalletAnswerXMLDID(questionIDList, answerIDList)
                ' Equifax 
            Case "EID"
                Return buildWalletAnswerXMLEquifax(questionIDList, answerIDList)
            Case "TID"
                Return buildWalletAnswerXMLTID(questionIDList, answerIDList)
        End Select

        Return Nothing

    End Function

    Protected Function manuallySelectSingleNode(ByRef xmlDoc As XmlDocument, ByVal nodeName As String) As XmlNode
        Dim foundNode As XmlNode = Nothing
        Dim matchingNodes As XmlNodeList = xmlDoc.GetElementsByTagName(nodeName)

        For Each aNode As XmlNode In matchingNodes
            If aNode.Name.ToUpper() = nodeName.ToUpper() Then
                foundNode = aNode
                Return foundNode
            End If
        Next

        Return Nothing
    End Function

    'use the one fom ccommon
    'Private Function getLoanNumber(ByVal pXMLResponse As String) As String
    '    Try
    '        Dim returnXmlDoc As New XmlDocument
    '        returnXmlDoc.LoadXml(pXMLResponse)

    '        Dim responseNode As XmlElement = returnXmlDoc.SelectSingleNode("OUTPUT/RESPONSE")

    '        If responseNode IsNot Nothing Then
    '            log.Info("Loan submit successful. Loan number: " & responseNode.GetAttribute("loan_number") & "    " & Now.ToString)
    '            Return responseNode.GetAttribute("loan_number")
    '        Else ' Error NODES
    '            Dim errorNode As XmlElement = returnXmlDoc.SelectSingleNode("OUTPUT/ERROR")
    '            If errorNode IsNot Nothing Then
    '                Select Case errorNode.GetAttribute("type")
    '                    'Case "INTERNAL_ERROR"
    '                    '    Log.Warn("Loan Submitted with errors: " & errorNode.InnerText & "   " & Now.ToString)
    '                    '    Return Nothing
    '                    Case "INTERNAL_ERROR" 'fail realtime credit check
    '                        log.Warn("Loan Submitted with errors: " & errorNode.InnerText & "   " & Now.ToString)
    '                        Return errorNode.GetAttribute("loan_number")
    '                    Case "VALIDATION_FAILED"
    '                        log.Warn("Loan Submit Failed with Schema Validation Problems: " & errorNode.InnerText & "   " & Now.ToString)
    '                        Return Nothing
    '                    Case Else
    '                        log.Warn("Loan Submit with total failure: " & errorNode.InnerText & "   " & Now.ToString)
    '                        Return Nothing
    '                End Select
    '            End If
    '        End If
    '        Return Nothing
    '    Catch ex As Exception
    '        log.Info("Parsing Error: " & ex.ToString())
    '        Return Nothing
    '    End Try
    'End Function
    'equifax
    Protected Function buildWalletAnswerXMLEquifax(ByVal questionIDList As List(Of String), ByVal answerIDList As List(Of String)) As String
        Dim sb As New StringBuilder()
        Dim writer As XmlWriter = XmlTextWriter.Create(sb)
        writer.WriteStartDocument()

        ' begin input
        writer.WriteStartElement("INPUT")

        writer.WriteStartElement("LOGIN")
        '      writer.WriteAttributeString("login", "quanntest")
        'writer.WriteAttributeString("password", "lpq123")
        writer.WriteAttributeString("api_user_id", _CurrentWebsiteConfig.APIUser)
        writer.WriteAttributeString("api_password", _CurrentWebsiteConfig.APIPassword)
        writer.WriteEndElement()
        writer.WriteStartElement("REQUEST")
        writer.WriteAttributeString("transaction_id", transaction_ID)
        writer.WriteAttributeString("app_id", loanID)

        If "False" = SafeString(Session("isJoinQuestionDone")) And IsJointApplication Then
            writer.WriteAttributeString("applicant_index", "0")
            writer.WriteAttributeString("is_joint", "true")
            Session("isJoinQuestionDone") = "Done"
        Else
            writer.WriteAttributeString("applicant_index", "0")
            writer.WriteAttributeString("is_joint", "false")
        End If

        'writer.WriteAttributeString("is_joint", _IsJointApplication)
        'writer.WriteAttributeString("is_joint", "false")  'join is only used for spouse not-coborrower

        Dim numQuestions As Integer = questionIDList.Count
        writer.WriteStartElement("ANSWERS")
        For i As Integer = 0 To numQuestions - 1
            writer.WriteStartElement("ANSWER")
            writer.WriteAttributeString("question_number", questionIDList(i))
            writer.WriteAttributeString("answer_number", answerIDList(i))
            writer.WriteEndElement()
        Next
        ' END </ANSWERS>
        writer.WriteEndElement()

        ' END </REQUEST>
        writer.WriteEndElement()

        ' END </INPUT>
        writer.WriteEndElement()
        writer.Flush()
        writer.Close()

        Dim returnStr As String = sb.ToString()

        Return returnStr
    End Function
    '
    Protected Function buildWalletAnswerXMLRSA(ByVal questionIDList As List(Of String), ByVal answerIDList As List(Of String)) As String
        Dim sb As New StringBuilder()
        Dim writer As XmlWriter = XmlTextWriter.Create(sb)
        writer.WriteStartDocument()

        ' begin input
        writer.WriteStartElement("INPUT")

        writer.WriteStartElement("LOGIN")
        '      writer.WriteAttributeString("login", "quanntest")
        'writer.WriteAttributeString("password", "lpq123")
        writer.WriteAttributeString("api_user_id", _CurrentWebsiteConfig.APIUser)
        writer.WriteAttributeString("api_password", _CurrentWebsiteConfig.APIPassword)
        writer.WriteEndElement()


        writer.WriteStartElement("REQUEST")
        writer.WriteAttributeString("transaction_id", transaction_ID)
        writer.WriteAttributeString("question_set_id", question_set_ID)
        writer.WriteAttributeString("app_id", loanID)
        'writer.WriteAttributeString("applicant_index", "0")
        If "False" = SafeString(Session("isJoinQuestionDone")) And IsJointApplication Then
            writer.WriteAttributeString("applicant_index", "0")
            writer.WriteAttributeString("is_joint", "true")
            Session("isJoinQuestionDone") = "Done"
        Else
            writer.WriteAttributeString("applicant_index", "0")
            writer.WriteAttributeString("is_joint", "false")
        End If
        'writer.WriteAttributeString("is_joint", _IsJointApplication)
        'writer.WriteAttributeString("is_joint", "false")  'join is only used for spouse not-coborrower

        Dim numQuestions As Integer = questionIDList.Count
        writer.WriteStartElement("ANSWERS")
        For i As Integer = 0 To numQuestions - 1
            writer.WriteStartElement("ANSWER")
            writer.WriteAttributeString("question_id", questionIDList(i))
            writer.WriteAttributeString("choice_id", answerIDList(i))
            writer.WriteEndElement()
        Next
        ' END </ANSWERS>
        writer.WriteEndElement()

        ' END </REQUEST>
        writer.WriteEndElement()

        ' END </INPUT>
        writer.WriteEndElement()
        writer.Flush()
        writer.Close()

        Dim returnStr As String = sb.ToString()

        Return returnStr
    End Function

    Protected Function buildWalletAnswerXMLPID(ByVal questionIDList As List(Of String), ByVal answerIDList As List(Of String)) As String
        Dim sb As New StringBuilder()
        Dim writer As XmlWriter = XmlTextWriter.Create(sb)
        writer.WriteStartDocument()

        ' begin input
        writer.WriteStartElement("INPUT")

        writer.WriteStartElement("LOGIN")
        writer.WriteAttributeString("api_user_id", _CurrentWebsiteConfig.APIUser)
        writer.WriteAttributeString("api_password", _CurrentWebsiteConfig.APIPassword)
        writer.WriteEndElement()


        writer.WriteStartElement("REQUEST")
        writer.WriteAttributeString("app_id", loanID)

        'writer.WriteAttributeString("is_joint", _IsJointApplication)
        'writer.WriteAttributeString("is_joint", "false")  'join is only used for spouse not-coborrower
        'writer.WriteAttributeString("applicant_index", "0")
        If "False" = SafeString(Session("isJoinQuestionDone")) And IsJointApplication Then
            writer.WriteAttributeString("applicant_index", "0")
            writer.WriteAttributeString("is_joint", "true")
            Session("isJoinQuestionDone") = "Done"
        Else
            writer.WriteAttributeString("applicant_index", "0")
            writer.WriteAttributeString("is_joint", "false")
        End If

        writer.WriteAttributeString("session_id", transaction_ID)

        Dim numQuestions As Integer = questionIDList.Count
        writer.WriteStartElement("ANSWERS")
        For i As Integer = 0 To numQuestions - 1
            writer.WriteStartElement("ANSWER")
            writer.WriteAttributeString("answer_text", answerIDList(i))
            writer.WriteEndElement()
        Next
        ' END </ANSWERS>
        writer.WriteEndElement()

        ' END </REQUEST>
        writer.WriteEndElement()

        ' END </INPUT>
        writer.WriteEndElement()
        writer.Flush()
        writer.Close()

        Dim returnStr As String = sb.ToString()

        Return returnStr
    End Function

    Protected Function buildWalletAnswerXMLFIS(ByVal questionIDList As List(Of String), ByVal answerIDList As List(Of String)) As String
        Dim sb As New StringBuilder()
        Dim writer As XmlWriter = XmlTextWriter.Create(sb)
        writer.WriteStartDocument()

        ' begin input
        writer.WriteStartElement("INPUT")

        writer.WriteStartElement("LOGIN")

        'FIS IDA ONLY work with lender level user
        If _CurrentWebsiteConfig.APIUserLender <> "" Then
            writer.WriteAttributeString("api_user_id", _CurrentWebsiteConfig.APIUserLender)
            writer.WriteAttributeString("api_password", _CurrentWebsiteConfig.APIPasswordLender)
        Else
            writer.WriteAttributeString("api_user_id", _CurrentWebsiteConfig.APIUser)
            writer.WriteAttributeString("api_password", _CurrentWebsiteConfig.APIPassword)
        End If

        writer.WriteEndElement()


        writer.WriteStartElement("REQUEST")
        writer.WriteAttributeString("xpressappid", loanID)

        If SafeString(Session("isJoinQuestionDone")) = "False" And IsJointApplication Then
            writer.WriteAttributeString("applicant_index", "0")
            writer.WriteAttributeString("applicant_type", "JOINT")  'for both prmary and co, there is no spouse
            Session("isJoinQuestionDone") = "Done"
        Else
            writer.WriteAttributeString("applicant_index", "0")
            writer.WriteAttributeString("applicant_type", "PRIMARY")
        End If

        writer.WriteAttributeString("quiz_id", question_set_ID)
        writer.WriteAttributeString("transaction_id", transaction_ID)

        Dim numQuestions As Integer = questionIDList.Count
        writer.WriteStartElement("ANSWERS")
        For i As Integer = 0 To numQuestions - 1
            writer.WriteStartElement("ANSWER")
            writer.WriteAttributeString("question_id", questionIDList(i))
            writer.WriteAttributeString("answer_id", answerIDList(i))
            writer.WriteEndElement()
        Next
        ' END </ANSWERS>
        writer.WriteEndElement()

        ' END </REQUEST>
        writer.WriteEndElement()

        ' END </INPUT>
        writer.WriteEndElement()
        writer.Flush()
        writer.Close()

        Dim returnStr As String = sb.ToString()

        Return returnStr
    End Function

    Protected Function buildWalletAnswerXMLDID(ByVal questionIDList As List(Of String), ByVal answerIDList As List(Of String)) As String
        Dim sb As New StringBuilder()
        Dim writer As XmlWriter = XmlTextWriter.Create(sb)
        writer.WriteStartDocument()

        ' begin input
        writer.WriteStartElement("INPUT")

        writer.WriteStartElement("LOGIN")
        writer.WriteAttributeString("api_user_id", _CurrentWebsiteConfig.APIUser)
        writer.WriteAttributeString("api_password", _CurrentWebsiteConfig.APIPassword)
        writer.WriteEndElement()

        writer.WriteStartElement("REQUEST")
        writer.WriteAttributeString("xpress_app_id", loanID)

        If SafeString(Session("isJoinQuestionDone")) = "False" And IsJointApplication Then
            writer.WriteAttributeString("applicant_index", "0")
            writer.WriteAttributeString("is_joint", "true")
            Session("isJoinQuestionDone") = "Done"
        Else
            writer.WriteAttributeString("applicant_index", "0")
            writer.WriteAttributeString("is_joint", "false")
        End If

        writer.WriteAttributeString("session_id", transaction_ID)
        writer.WriteAttributeString("request_app_id", request_app_ID)
        writer.WriteAttributeString("request_client_id", request_client_ID)
        writer.WriteAttributeString("request_sequence_id", request_sequence_ID)

        Dim numQuestions As Integer = questionIDList.Count
        writer.WriteStartElement("ANSWERS")
        For i As Integer = 0 To numQuestions - 1
            writer.WriteStartElement("ANSWER")
            writer.WriteAttributeString("question_number", questionIDList(i))
            writer.WriteAttributeString("answer_text", answerIDList(i))
            writer.WriteEndElement()
        Next
        ' END </ANSWERS>
        writer.WriteEndElement()

        ' END </REQUEST>
        writer.WriteEndElement()

        ' END </INPUT>
        writer.WriteEndElement()
        writer.Flush()
        writer.Close()

        Dim returnStr As String = sb.ToString()

        Return returnStr
    End Function

    Protected Function parseAnswersForID() As List(Of String)
        ' Parse the answer ids that were posted to this form
        ' The answer ids are concatenated into a string separated by semicolons

        Dim combinedAnswerStr As String

        combinedAnswerStr = Request.Form("WalletAnswers")

        ' Read the string up to but not including a semicolon and write out what was read
        ' repeat until end of string
        Dim length As Integer = combinedAnswerStr.Length
        Dim answerIDList As New List(Of String)
        Dim answerIDStr As String = ""

        For I As Integer = 0 To length - 1
            If (combinedAnswerStr.Chars(I) = ";") Then
                ' Add the current string to the list
                answerIDList.Add(answerIDStr)
                answerIDStr = ""
            Else
                answerIDStr += combinedAnswerStr.Chars(I)
            End If
        Next

        ' Add the last one to the list
        answerIDList.Add(answerIDStr)

        Return answerIDList

    End Function

    Protected Function getXMLNodes(ByRef xmlDoc As XmlDocument, ByVal elementName As String) As XmlNodeList
        ' Use this function for some special cases where SelectSingleNode does not work
        Dim targetNodes As XmlNodeList = xmlDoc.GetElementsByTagName(elementName)

        Return targetNodes
    End Function
    Protected Function CreateLoanApp() As LoanAppInfo
        Dim oLoanApp As LoanAppInfo = New LoanAppInfo(_CurrentWebsiteConfig)
        '' oLoanApp.IsSSO = IsSSO

        ''--disclosure
        If Not String.IsNullOrEmpty(Request.Form("Disclosure")) Then
            oLoanApp.Disclosure = Request.Form("Disclosure")
        End If
        ''--Minor Account Code
        If Not String.IsNullOrEmpty(Common.SafeString(Request.Form("MinorAccountCode"))) Then
            oLoanApp.MinorAccountCode = Common.SafeString(Request.Form("MinorAccountCode"))
        End If

        If Not String.IsNullOrEmpty(Common.SafeString(Request.Form("MinorRoleType"))) Then
            oLoanApp.MinorRoleType = Common.SafeString(Request.Form("MinorRoleType"))
        End If

        If Not String.IsNullOrEmpty(Common.SafeString(Request.Form("SecondRoleType"))) Then
            oLoanApp.SecondRoleType = Common.SafeString(Request.Form("SecondRoleType"))
        End If
        If Not String.IsNullOrEmpty(Common.SafeString(Request.Form("eSignature"))) Then
            oLoanApp.eSignature = Common.SafeString(Request.Form("eSignature"))
        End If
        oLoanApp.oAvailability = Common.SafeString(Request.Form("Availability"))
        ''Medishare number
        oLoanApp.oMediShareNumber = Common.SafeString(Request.Form("MediShareNumber"))
        ''house hold number 
        oLoanApp.oHouseHoldNumber = Common.SafeString(Request.Form("HouseHoldNumber"))
        ''get isLive for second callback
        isLive = Common.SafeString(Request.Form("isLive"))
        oLoanApp.oIsLive = isLive
        '' load personal info
        Dim _personalInfo As New PersonalInfo
        _personalInfo.EmployeeOfLender = Request.Form("EmployeeOfLender")
        _personalInfo.MemberNumber = Request.Form("MemberNumber")
        _personalInfo.FirstName = Request.Form("FirstName")
        _personalInfo.LastName = Request.Form("LastName")
        _personalInfo.MiddleName = Request.Form("MiddleName")
        _personalInfo.Gender = Request.Form("Gender")
        _personalInfo.Suffix = Request.Form("NameSuffix")
        _personalInfo.DateOfBirth = Request.Form("DOB")
        _personalInfo.HasMinor = SafeString(Request.Form("HasMinorApplicant"))
        'If selectChoiceYear.Value <> "0" AndAlso selectChoiceMonth.Value <> "0" AndAlso selectChoicDay.Value <> "0" Then
        '    _personalInfo.DateOfBirth = selectChoiceYear.Value & "-" & selectChoiceMonth.Value & "-" & selectChoicDay.Value
        'End If

        If Request.Form("EmploymentStatus") IsNot Nothing Then
            _personalInfo.EmploymentSectionEnabled = True
            _personalInfo.EmploymentStatus = Common.SafeString(Request.Form("EmploymentStatus"))
            If _personalInfo.EmploymentStatus = "STUDENT" Or _personalInfo.EmploymentStatus = "HOMEMAKER" Then
                _personalInfo.txtJobTitle = _personalInfo.EmploymentStatus
            ElseIf _personalInfo.EmploymentStatus = "RETIRED" Or _personalInfo.EmploymentStatus = "UNEMPLOYED" Then
                _personalInfo.txtJobTitle = _personalInfo.EmploymentStatus
                _personalInfo.txtEmployedDuration_month = Common.SafeString(Request.Form("txtEmployedDuration_month"))
                _personalInfo.txtEmployedDuration_year = Common.SafeString(Request.Form("txtEmployedDuration_year"))
            Else
                _personalInfo.txtJobTitle = Common.SafeString(Request.Form("txtJobTitle"))
                _personalInfo.txtEmployedDuration_month = Common.SafeString(Request.Form("txtEmployedDuration_month"))
                _personalInfo.txtEmployedDuration_year = Common.SafeString(Request.Form("txtEmployedDuration_year"))
                _personalInfo.txtEmployer = Common.SafeString(Request.Form("txtEmployer"))
            End If
            ' new employment logic
            _personalInfo.txtBusinessType = Common.SafeString(Request.Form("txtBusinessType"))
            ''enlistment date for active military
            Dim employmentStatus = CEnum.MapEmploymentStatusToValue(_personalInfo.EmploymentStatus)
            Dim enlistmentDate As String = Request.Form("EnlistmentDate")
            If (employmentStatus = "MI" And enlistmentDate <> "") Then
                _personalInfo.txtEmploymentStartDate = Common.SafeString(enlistmentDate)
            Else
                _personalInfo.txtEmploymentStartDate = Common.SafeString(Request.Form("txtEmploymentStartDate"))
            End If
            _personalInfo.txtETS = Common.SafeString(Request.Form("txtETS"))
            _personalInfo.txtProfessionDuration_month = Common.SafeString(Request.Form("txtProfessionDuration_month"))
            _personalInfo.txtProfessionDuration_year = Common.SafeString(Request.Form("txtProfessionDuration_year"))
            _personalInfo.txtSupervisorName = Common.SafeString(Request.Form("txtSupervisorName"))
            _personalInfo.ddlBranchOfService = Common.SafeString(Request.Form("ddlBranchOfService"))
            _personalInfo.ddlPayGrade = Common.SafeString(Request.Form("ddlPayGrade"))
            _personalInfo.txtGrossMonthlyIncome = Common.SafeDouble(Request.Form("txtGrossMonthlyIncome"))
            ' end new employment logic

            'prev employment
            _personalInfo.hasPrevEmployment = Common.SafeString(Request.Form("hasPreviousEmployment"))
            If (_personalInfo.hasPrevEmployment = "Y") Then
                _personalInfo.prev_EmploymentStatus = Common.SafeString(Request.Form("prev_EmploymentStatus"))
                _personalInfo.prev_GrossMonthlyIncome = Common.SafeDouble(Request.Form("prev_grossMonthlyIncome"))
                If _personalInfo.prev_EmploymentStatus = "STUDENT" Or _personalInfo.prev_EmploymentStatus = "HOMEMAKER" Then
                    _personalInfo.prev_txtJobTitle = _personalInfo.prev_EmploymentStatus
                ElseIf _personalInfo.prev_EmploymentStatus = "RETIRED" Or _personalInfo.prev_EmploymentStatus = "UNEMPLOYED" Then
                    _personalInfo.prev_txtJobTitle = _personalInfo.prev_EmploymentStatus
                    _personalInfo.prev_txtEmployedDuration_month = Common.SafeString(Request.Form("prev_txtEmployedDuration_month"))
                    _personalInfo.prev_txtEmployedDuration_year = Common.SafeString(Request.Form("prev_txtEmployedDuration_year"))
                Else
                    _personalInfo.prev_txtJobTitle = Common.SafeString(Request.Form("prev_txtJobTitle"))
                    _personalInfo.prev_txtEmployedDuration_month = Common.SafeString(Request.Form("prev_txtEmployedDuration_month"))
                    _personalInfo.prev_txtEmployedDuration_year = Common.SafeString(Request.Form("prev_txtEmployedDuration_year"))
                    _personalInfo.prev_txtEmployer = Common.SafeString(Request.Form("prev_txtEmployer"))
                End If


                _personalInfo.prev_txtBusinessType = Common.SafeString(Request.Form("prev_txtBusinessType"))
                ''enlistment date for active military
                Dim prev_EmploymentStatus = CEnum.MapEmploymentStatusToValue(_personalInfo.prev_EmploymentStatus)
                Dim prev_EnlistmentDate As String = Request.Form("prev_EnlistmentDate")
                If (prev_EmploymentStatus = "MI" And prev_EnlistmentDate <> "") Then
                    _personalInfo.prev_txtEmploymentStartDate = Common.SafeString(prev_EnlistmentDate)
                Else
                    _personalInfo.prev_txtEmploymentStartDate = Common.SafeString(Request.Form("prev_txtEmploymentStartDate"))
                End If
                _personalInfo.prev_txtETS = Common.SafeString(Request.Form("prev_txtETS"))
                _personalInfo.prev_ddlBranchOfService = Common.SafeString(Request.Form("prev_ddlBranchOfService"))
                _personalInfo.prev_ddlPayGrade = Common.SafeString(Request.Form("prev_ddlPayGrade"))
            End If
            'end prev employment
        End If


        _personalInfo.Citizenship = Request.Form("Citizenship")
        _personalInfo.MotherMaidenName = Request.Form("MotherMaidenName")
        _personalInfo.SSN = Request.Form("SSN")
        _personalInfo.MaritalStatus = Common.SafeString(Request.Form("MaritalStatus"))
        oLoanApp.oPersonal = _personalInfo



        ' load contact info
        Dim _contact As New ContactInfo
        _contact.CellPhone = SafeString(Request.Form("MobilePhone"))
        _contact.CellPhoneCountry = SafeString(Request.Form("MobilePhoneCountry"))
        _contact.Email = SafeString(Request.Form("EmailAddr"))
        _contact.HomePhone = SafeString(Request.Form("HomePhone"))
        _contact.HomePhoneCountry = SafeString(Request.Form("HomePhoneCountry"))
        _contact.PreferredContactMethod = SafeString(Request.Form("ContactMethod"))
        _contact.WorkPhone = SafeString(Request.Form("WorkPhone"))
        _contact.WorkPhoneCountry = SafeString(Request.Form("WorkPhoneCountry"))
        _contact.WorkPhoneExt = SafeString(Request.Form("WorkPhoneEXT"))
        oLoanApp.oContact = _contact

        Dim _CoContact As New ContactInfo

        ' load identification info
        Dim _id As New IdentificationInfo
        _id.CardNumber = Common.SafeString(Request.Form("IDCardNumber"))
        _id.CardType = Common.SafeString(Request.Form("IDCardType"))
        _id.Country = Common.SafeString(Request.Form("IDCountry"))
        _id.DateIssued = Common.SafeString(Request.Form("IDDateIssued"))
        _id.ExpirationDate = Common.SafeString(Request.Form("IDDateExpire"))
        _id.State = Common.SafeString(Request.Form("IDState"))
        oLoanApp.oIDCard = _id

        Dim _CoId As New IdentificationInfo

        '' load address info
        Dim _address As New AddressInfo
        _address.Address = Request.Form("AddressStreet")
        _address.City = Request.Form("AddressCity")
        _address.State = Request.Form("AddressState")
        _address.Zip = Request.Form("AddressZip")
        _address.Country = Request.Form("Country")
        ''---make sure occupancy status is existing
        '' previous address
        Dim _PreviousAddress As New AddressInfo
        If Not String.IsNullOrEmpty(Request.Form("OccupyingLocation")) Then
            _address.OccupancyDuration = SafeInteger(Request.Form("LiveMonths"))
            _address.OccupancyStatus = Request.Form("OccupyingLocation")
            Dim hasPreviousAddress As String = Request.Form("hasPreviousAddress")
            If Not String.IsNullOrEmpty(hasPreviousAddress) Then
                If hasPreviousAddress = "Y" Then
                    _PreviousAddress.Address = Request.Form("PreviousAddressStreet")
                    _PreviousAddress.City = Request.Form("PreviousAddressCity")
                    _PreviousAddress.State = Request.Form("PreviousAddressState")
                    _PreviousAddress.Zip = Request.Form("PreviousAddressZip")
                Else
                    _PreviousAddress.Address = ""
                    _PreviousAddress.City = ""
                    _PreviousAddress.State = ""
                    _PreviousAddress.Zip = ""
                End If
            End If
        End If
        oLoanApp.oPreviousAddress = _PreviousAddress
        oLoanApp.oAddress = _address
        ''mailing address
        Dim _MailingAddress As New AddressInfo
        Dim hasMailingAddress As String = Request.Form("hasMailingAddress")
        If Not String.IsNullOrEmpty(hasMailingAddress) Then
            If hasMailingAddress = "Y" Then
                _MailingAddress.Address = Request.Form("MailingAddressStreet")
                _MailingAddress.City = Request.Form("MailingAddressCity")
                _MailingAddress.State = Request.Form("MailingAddressState")
                _MailingAddress.Zip = Request.Form("MailingAddressZip")
                _MailingAddress.Country = Request.Form("MailingAddressCountry")
            Else
                _MailingAddress.Address = ""
                _MailingAddress.City = ""
                _MailingAddress.State = ""
                _MailingAddress.Zip = ""
                _MailingAddress.Country = ""
            End If
        End If
        oLoanApp.oMailingAddress = _MailingAddress
        ''applicant questions
        Dim oAQSerializer As New JavaScriptSerializer()
        Dim oAQAnswerList As New List(Of XAQuestionAnswer)
        If Not String.IsNullOrEmpty(SafeString(Request.Form("ApplicantQuestionAnswers"))) Then
            oAQAnswerList = oAQSerializer.Deserialize(Of List(Of XAQuestionAnswer))(Request.Form("ApplicantQuestionAnswers"))
        End If
        oLoanApp.oApplicantQuestionList = oAQAnswerList
        Dim _CoAddress As New AddressInfo
        Dim _CoAppPersonalInfo As New PersonalInfo
        If (True = IsJointApplication) Then
            _CoAppPersonalInfo.EmployeeOfLender = Request.Form("co_EmployeeOfLender")
            _CoAppPersonalInfo.MemberNumber = Request.Form("co_MemberNumber")
            _CoAppPersonalInfo.FirstName = Request.Form("co_FirstName")
            Co_FName = SafeString(Request.Form("co_FirstName")) 'persis to session
            _CoAppPersonalInfo.LastName = Request.Form("co_LastName")
            _CoAppPersonalInfo.MiddleName = Request.Form("co_MiddleName")
            _CoAppPersonalInfo.Gender = Request.Form("co_Gender")
            _CoAppPersonalInfo.Suffix = Request.Form("co_NameSuffix")
            _CoAppPersonalInfo.DateOfBirth = Request.Form("co_DOB")

            If Request.Form("co_EmploymentStatus") IsNot Nothing Then
                _CoAppPersonalInfo.EmploymentSectionEnabled = True
                _CoAppPersonalInfo.EmploymentStatus = Common.SafeString(Request.Form("co_EmploymentStatus"))
                If _CoAppPersonalInfo.EmploymentStatus = "STUDENT" Or _CoAppPersonalInfo.EmploymentStatus = "HOMEMAKER" Then
                    _CoAppPersonalInfo.txtJobTitle = _CoAppPersonalInfo.EmploymentStatus
                ElseIf _CoAppPersonalInfo.EmploymentStatus = "RETIRED" Or _CoAppPersonalInfo.EmploymentStatus = "UNEMPLOYED" Then
                    _CoAppPersonalInfo.txtJobTitle = _CoAppPersonalInfo.EmploymentStatus
                    _CoAppPersonalInfo.txtEmployedDuration_month = Common.SafeString(Request.Form("co_txtEmployedDuration_month"))
                    _CoAppPersonalInfo.txtEmployedDuration_year = Common.SafeString(Request.Form("co_txtEmployedDuration_year"))
                Else
                    _CoAppPersonalInfo.txtJobTitle = Common.SafeString(Request.Form("co_txtJobTitle"))
                    _CoAppPersonalInfo.txtEmployedDuration_month = Common.SafeString(Request.Form("co_txtEmployedDuration_month"))
                    _CoAppPersonalInfo.txtEmployedDuration_year = Common.SafeString(Request.Form("co_txtEmployedDuration_year"))
                    _CoAppPersonalInfo.txtEmployer = Common.SafeString(Request.Form("co_txtEmployer"))
                End If
                ' new employment logic
                _CoAppPersonalInfo.txtBusinessType = Common.SafeString(Request.Form("co_txtBusinessType"))

                ''enlistment date for active military
                Dim co_employmentStatus = CEnum.MapEmploymentStatusToValue(_CoAppPersonalInfo.EmploymentStatus)
                Dim co_enlistmentDate As String = Request.Form("co_EnlistmentDate")
                If (co_employmentStatus = "MI" And co_enlistmentDate <> "") Then
                    _CoAppPersonalInfo.txtEmploymentStartDate = Common.SafeString(co_enlistmentDate)
                Else
                    _CoAppPersonalInfo.txtEmploymentStartDate = Common.SafeString(Request.Form("co_txtEmploymentStartDate"))
                End If
                _CoAppPersonalInfo.txtETS = Common.SafeString(Request.Form("co_txtETS"))
                _CoAppPersonalInfo.txtProfessionDuration_month = Common.SafeString(Request.Form("co_txtProfessionDuration_month"))
                _CoAppPersonalInfo.txtProfessionDuration_year = Common.SafeString(Request.Form("co_txtProfessionDuration_year"))
                _CoAppPersonalInfo.txtSupervisorName = Common.SafeString(Request.Form("co_txtSupervisorName"))
                _CoAppPersonalInfo.ddlBranchOfService = Common.SafeString(Request.Form("co_ddlBranchOfService"))
                _CoAppPersonalInfo.ddlPayGrade = Common.SafeString(Request.Form("co_ddlPayGrade"))
                _CoAppPersonalInfo.txtGrossMonthlyIncome = Common.SafeDouble(Request.Form("co_txtGrossMonthlyIncome"))
                ' end new employment logic


                'prev employment
                _CoAppPersonalInfo.hasPrevEmployment = Common.SafeString(Request.Form("co_hasPreviousEmployment"))
                If (_CoAppPersonalInfo.hasPrevEmployment = "Y") Then
                    _CoAppPersonalInfo.prev_EmploymentStatus = Common.SafeString(Request.Form("co_prev_EmploymentStatus"))
                    _CoAppPersonalInfo.prev_GrossMonthlyIncome = Common.SafeDouble(Request.Form("co_prev_grossMonthlyIncome"))
                    If _CoAppPersonalInfo.prev_EmploymentStatus = "STUDENT" Or _CoAppPersonalInfo.prev_EmploymentStatus = "HOMEMAKER" Then
                        _CoAppPersonalInfo.prev_txtJobTitle = _CoAppPersonalInfo.prev_EmploymentStatus
                    ElseIf _CoAppPersonalInfo.prev_EmploymentStatus = "RETIRED" Or _CoAppPersonalInfo.prev_EmploymentStatus = "UNEMPLOYED" Then
                        _CoAppPersonalInfo.prev_txtJobTitle = _CoAppPersonalInfo.prev_EmploymentStatus
                        _CoAppPersonalInfo.prev_txtEmployedDuration_month = Common.SafeString(Request.Form("co_prev_txtEmployedDuration_month"))
                        _CoAppPersonalInfo.prev_txtEmployedDuration_year = Common.SafeString(Request.Form("co_prev_txtEmployedDuration_year"))
                    Else
                        _CoAppPersonalInfo.prev_txtJobTitle = Common.SafeString(Request.Form("co_prev_txtJobTitle"))
                        _CoAppPersonalInfo.prev_txtEmployedDuration_month = Common.SafeString(Request.Form("co_prev_txtEmployedDuration_month"))
                        _CoAppPersonalInfo.prev_txtEmployedDuration_year = Common.SafeString(Request.Form("co_prev_txtEmployedDuration_year"))
                        _CoAppPersonalInfo.prev_txtEmployer = Common.SafeString(Request.Form("co_prev_txtEmployer"))
                    End If


                    _CoAppPersonalInfo.prev_txtBusinessType = Common.SafeString(Request.Form("co_prev_txtBusinessType"))
                    ''enlistment date for active military
                    Dim co_prev_EmploymentStatus = CEnum.MapEmploymentStatusToValue(_CoAppPersonalInfo.prev_EmploymentStatus)
                    Dim co_prev_EnlistmentDate As String = Request.Form("co_prev_EnlistmentDate")
                    If (co_prev_EmploymentStatus = "MI" And co_prev_EnlistmentDate <> "") Then
                        _CoAppPersonalInfo.prev_txtEmploymentStartDate = Common.SafeString(co_prev_EnlistmentDate)
                    Else
                        _CoAppPersonalInfo.prev_txtEmploymentStartDate = Common.SafeString(Request.Form("co_prev_txtEmploymentStartDate"))
                    End If
                    _CoAppPersonalInfo.prev_txtETS = Common.SafeString(Request.Form("co_prev_txtETS"))
                    _CoAppPersonalInfo.prev_ddlBranchOfService = Common.SafeString(Request.Form("co_prev_ddlBranchOfService"))
                    _CoAppPersonalInfo.prev_ddlPayGrade = Common.SafeString(Request.Form("co_prev_ddlPayGrade"))
                End If
                'end prev employment
            End If


            _CoAppPersonalInfo.Citizenship = Common.SafeString(Request.Form("co_Citizenship"))
            _CoAppPersonalInfo.MotherMaidenName = Common.SafeString(Request.Form("co_MotherMaidenName"))
            _CoAppPersonalInfo.SSN = Common.SafeString(Request.Form("co_SSN"))
            _CoAppPersonalInfo.MaritalStatus = Common.SafeString(Request.Form("co_MaritalStatus"))
            oLoanApp.oCoAppPersonal = _CoAppPersonalInfo

            _CoId.CardNumber = Common.SafeString(Request.Form("co_IDCardNumber"))
            _CoId.CardType = Common.SafeString(Request.Form("co_IDCardType"))
            _CoId.Country = Common.SafeString(Request.Form("co_IDCountry"))
            _CoId.DateIssued = Common.SafeString(Request.Form("co_IDDateIssued"))
            _CoId.ExpirationDate = Common.SafeString(Request.Form("co_IDDateExpire"))
            _CoId.State = Common.SafeString(Request.Form("co_IDState"))
            oLoanApp.oCoAppIDCard = _CoId

            _CoAddress.Address = Request.Form("co_AddressStreet")
            _CoAddress.City = Request.Form("co_AddressCity")
            _CoAddress.State = Request.Form("co_AddressState")
            _CoAddress.Zip = Request.Form("co_AddressZip")
            _CoAddress.Country = Request.Form("co_Country")
            ''---make sure occupancy status is existing
            '' previous address
            Dim _CoPreviousAddress As New AddressInfo
            If Not String.IsNullOrEmpty(Request.Form("co_OccupyingLocation")) Then
                _CoAddress.OccupancyDuration = SafeInteger(Request.Form("co_LiveMonths"))
                _CoAddress.OccupancyStatus = Request.Form("co_OccupyingLocation")
                Dim hasCoPreviousAddress As String = Request.Form("co_hasPreviousAddress")
                If Not String.IsNullOrEmpty(hasCoPreviousAddress) Then
                    If hasCoPreviousAddress = "Y" Then
                        _CoPreviousAddress.Address = Request.Form("co_PreviousAddressStreet")
                        _CoPreviousAddress.City = Request.Form("co_PreviousAddressCity")
                        _CoPreviousAddress.State = Request.Form("co_PreviousAddressState")
                        _CoPreviousAddress.Zip = Request.Form("co_PreviousAddressZip")
                    Else
                        _CoPreviousAddress.Address = ""
                        _CoPreviousAddress.City = ""
                        _CoPreviousAddress.State = ""
                        _CoPreviousAddress.Zip = ""
                    End If
                End If
            End If
            oLoanApp.oCoPreviousAddress = _CoPreviousAddress
            oLoanApp.oCoAppAddress = _CoAddress
            ''co mailing address
            Dim _CoMailingAddress As New AddressInfo
            Dim hasCoMailingAddress As String = Request.Form("co_hasMailingAddress")
            If Not String.IsNullOrEmpty(hasCoMailingAddress) Then
                If hasCoMailingAddress = "Y" Then
                    _CoMailingAddress.Address = Request.Form("co_MailingAddressStreet")
                    _CoMailingAddress.City = Request.Form("co_MailingAddressCity")
                    _CoMailingAddress.State = Request.Form("co_MailingAddressState")
                    _CoMailingAddress.Zip = Request.Form("co_MailingAddressZip")
                    _CoMailingAddress.Country = Request.Form("co_MailingAddressCountry")
                Else
                    _CoMailingAddress.Address = ""
                    _CoMailingAddress.City = ""
                    _CoMailingAddress.State = ""
                    _CoMailingAddress.Zip = ""
                    _CoMailingAddress.Country = ""
                End If
            End If

            oLoanApp.oCoMailingAddress = _CoMailingAddress
            _CoContact.CellPhone = SafeString(Request.Form("co_MobilePhone"))
            _CoContact.CellPhoneCountry = SafeString(Request.Form("co_MobilePhoneCountry"))
            _CoContact.Email = SafeString(Request.Form("co_EmailAddr"))
            _CoContact.HomePhone = SafeString(Request.Form("co_HomePhone"))
            _CoContact.HomePhoneCountry = SafeString(Request.Form("co_HomePhoneCountry"))
            _CoContact.PreferredContactMethod = SafeString(Request.Form("co_ContactMethod"))
            _CoContact.WorkPhone = SafeString(Request.Form("co_WorkPhone"))
            _CoContact.WorkPhoneCountry = SafeString(Request.Form("co_WorkPhoneCountry"))
            _CoContact.WorkPhoneExt = SafeString(Request.Form("co_WorkPhoneEXT"))
            oLoanApp.oCoAppContact = _CoContact
            '' co applicant questions
            Dim oCoAQSerializer As New JavaScriptSerializer()
            Dim oCoAQAnswerList As New List(Of XAQuestionAnswer)
            If Not String.IsNullOrEmpty(SafeString(Request.Form("co_ApplicantQuestionAnswers"))) Then
                oCoAQAnswerList = oCoAQSerializer.Deserialize(Of List(Of XAQuestionAnswer))(Request.Form("co_ApplicantQuestionAnswers"))
            End If

            oLoanApp.oCoApplicantQuestionList = oCoAQAnswerList
        End If

        ''just get all accounts in the accountslist instead
        Dim oSerializer As New JavaScriptSerializer()
        Dim oProductsList As New List(Of SelectProducts)
        Dim oAccountsList As New List(Of AccountInfo)
        If Not String.IsNullOrEmpty(Request.Params("SelectedProducts")) Then
            oProductsList = oSerializer.Deserialize(Of List(Of SelectProducts))(Request.Params("SelectedProducts"))
        End If

        Dim i As Integer
        ''make sure oProductsList has at least one products
        'Media-sharechecking   CHECKING_180
        If oProductsList.Count > 0 Then
            For i = 0 To oProductsList.Count - 1
                Dim acountInfor As New AccountInfo(_CurrentWebsiteConfig)
                acountInfor.Amount = SafeDouble(oProductsList(i).depositAmount)
                acountInfor.CustomQuestions = oProductsList(i).productQuestions
                acountInfor.Product = oProductsList(i).productName
                acountInfor.ProductServices = oProductsList(i).productServices
                oAccountsList.Add(acountInfor)
            Next
        End If
        oLoanApp.oAccountsList = oAccountsList

        '===all upload document
        Dim oSelectDocsList As New List(Of selectDocuments)
        Dim oSerializer_doc As New JavaScriptSerializer()
        Dim oDocsList As New Dictionary(Of String, String)
        ''by default MaxJsonLenth = 102400, set it =int32.maxvalue = 2147483647 
        oSerializer_doc.MaxJsonLength = Int32.MaxValue
        If Not String.IsNullOrEmpty(Request.Form("Image")) Then
            'TODO
            '' oDocsList = oSerializer.Deserialize(Of Dictionary(Of String, String))(Request.Params("Image"))
            'need to work on serialization
            ''oDocsList.Add("test title.png", Request.Params("Image"))
            oSelectDocsList = oSerializer_doc.Deserialize(Of List(Of selectDocuments))(Request.Form("Image"))
            If oSelectDocsList.Count > 0 Then
                Dim j As Integer
                For j = 0 To oSelectDocsList.Count - 1
                    Dim oTitle As String = oSelectDocsList(j).title
                    Dim obase64data As String = oSelectDocsList(j).base64data
                    oDocsList.Add(oTitle, obase64data)
                Next
                If oDocsList.Count = 0 Then
                    log.Info("Can't de-serialize image ")
                End If
            End If
            oLoanApp.DocBase64Dic = oDocsList
        End If
        ''get upload document info if it exist
        Dim oSerializerDocInfo As New JavaScriptSerializer()
        Dim oUploadDocList As New List(Of uploadDocumentInfo)
        If Not String.IsNullOrEmpty(Request.Form("Image")) Then
            oUploadDocList = oSerializerDocInfo.Deserialize(Of List(Of uploadDocumentInfo))(Request.Form("UploadDocInfor"))
        End If
        oLoanApp.oUploadDocumentInfoList = oUploadDocList
        'LenderID = Request.Form("LenderId")
        'BranchID = Request.Form("BranchID")
        ''remove 3 account
        ''end remove 3 account
        'Loan_Info
        Dim _LoanInfo As New LoanInfo
        _LoanInfo.account_position = Request.Form("TypeAccount")
        _LoanInfo.referral_source = Request.Form("ReferralSource")


        oLoanApp.oLoanInfo = _LoanInfo
        ''--Minor Applicant
        Dim hasMinorApplicant = SafeString(Request.Form("HasMinorApplicant"))
        If hasMinorApplicant = "Y" Then
            CreateMinorLoanApp(oLoanApp)
        End If

        Return oLoanApp
    End Function
    Protected Sub CreateMinorLoanApp(ByRef oLoanApp As LoanAppInfo)
        Dim _MinorAppPersonalInfo As New PersonalInfo()
        '' account info
        _MinorAppPersonalInfo.EmployeeOfLender = SafeString(Request.Form("m_EmployeeOfLender"))
        _MinorAppPersonalInfo.MemberNumber = SafeString(Request.Form("m_MemberNumber"))
        _MinorAppPersonalInfo.FirstName = SafeString(Request.Form("m_FirstName"))
        _MinorAppPersonalInfo.LastName = SafeString(Request.Form("m_LastName"))
        _MinorAppPersonalInfo.MiddleName = SafeString(Request.Form("m_MiddleName"))
        _MinorAppPersonalInfo.Gender = SafeString(Request.Form("m_Gender"))
        _MinorAppPersonalInfo.Suffix = SafeString(Request.Form("m_NameSuffix"))
        _MinorAppPersonalInfo.DateOfBirth = SafeString(Request.Form("m_DOB"))

        If Request.Form("m_EmploymentStatus") IsNot Nothing Then
            _MinorAppPersonalInfo.EmploymentSectionEnabled = True
            _MinorAppPersonalInfo.EmploymentStatus = SafeString(Request.Form("m_EmploymentStatus"))
            If _MinorAppPersonalInfo.EmploymentStatus = "STUDENT" Or _MinorAppPersonalInfo.EmploymentStatus = "HOMEMAKER" Then
                _MinorAppPersonalInfo.txtJobTitle = _MinorAppPersonalInfo.EmploymentStatus
            ElseIf _MinorAppPersonalInfo.EmploymentStatus = "RETIRED" Or _MinorAppPersonalInfo.EmploymentStatus = "UNEMPLOYED" Then
                _MinorAppPersonalInfo.txtJobTitle = _MinorAppPersonalInfo.EmploymentStatus
                _MinorAppPersonalInfo.txtEmployedDuration_month = SafeString(Request.Form("m_txtEmployedDuration_month"))
                _MinorAppPersonalInfo.txtEmployedDuration_year = SafeString(Request.Form("m_txtEmployedDuration_year"))
            Else
                _MinorAppPersonalInfo.txtJobTitle = SafeString(Request.Form("m_txtJobTitle"))
                _MinorAppPersonalInfo.txtEmployedDuration_month = SafeString(Request.Form("m_txtEmployedDuration_month"))
                _MinorAppPersonalInfo.txtEmployedDuration_year = SafeString(Request.Form("m_txtEmployedDuration_year"))
                _MinorAppPersonalInfo.txtEmployer = SafeString(Request.Form("m_txtEmployer"))
            End If
            ' new employment logic
            _MinorAppPersonalInfo.txtBusinessType = SafeString(Request.Form("m_txtBusinessType"))

            ''enlistment date for active military
            Dim m_employmentStatus = CEnum.MapEmploymentStatusToValue(_MinorAppPersonalInfo.EmploymentStatus)
            Dim m_enlistmentDate As String = SafeString(Request.Form("m_EnlistmentDate"))
            If (m_employmentStatus = "MI" And m_enlistmentDate <> "") Then
                _MinorAppPersonalInfo.txtEmploymentStartDate = SafeString(m_enlistmentDate)
            Else
                _MinorAppPersonalInfo.txtEmploymentStartDate = SafeString(Request.Form("m_txtEmploymentStartDate"))
            End If
            _MinorAppPersonalInfo.txtETS = SafeString(Request.Form("m_txtETS"))
            _MinorAppPersonalInfo.txtProfessionDuration_month = SafeString(Request.Form("m_txtProfessionDuration_month"))
            _MinorAppPersonalInfo.txtProfessionDuration_year = SafeString(Request.Form("m_txtProfessionDuration_year"))
            _MinorAppPersonalInfo.txtSupervisorName = SafeString(Request.Form("m_txtSupervisorName"))
            _MinorAppPersonalInfo.ddlBranchOfService = SafeString(Request.Form("m_ddlBranchOfService"))
            _MinorAppPersonalInfo.ddlPayGrade = SafeString(Request.Form("m_ddlPayGrade"))
            _MinorAppPersonalInfo.txtGrossMonthlyIncome = SafeDouble(Request.Form("m_txtGrossMonthlyIncome"))
            ' end new employment logic

            'prev employment
            _MinorAppPersonalInfo.hasPrevEmployment = Common.SafeString(Request.Form("m_hasPreviousEmployment"))
            If (_MinorAppPersonalInfo.hasPrevEmployment = "Y") Then
                _MinorAppPersonalInfo.prev_EmploymentStatus = Common.SafeString(Request.Form("m_prev_EmploymentStatus"))
                _MinorAppPersonalInfo.prev_GrossMonthlyIncome = Common.SafeDouble(Request.Form("m_prev_grossMonthlyIncome"))
                If _MinorAppPersonalInfo.prev_EmploymentStatus = "STUDENT" Or _MinorAppPersonalInfo.prev_EmploymentStatus = "HOMEMAKER" Then
                    _MinorAppPersonalInfo.prev_txtJobTitle = _MinorAppPersonalInfo.prev_EmploymentStatus
                ElseIf _MinorAppPersonalInfo.prev_EmploymentStatus = "RETIRED" Or _MinorAppPersonalInfo.prev_EmploymentStatus = "UNEMPLOYED" Then
                    _MinorAppPersonalInfo.prev_txtJobTitle = _MinorAppPersonalInfo.prev_EmploymentStatus
                    _MinorAppPersonalInfo.prev_txtEmployedDuration_month = Common.SafeString(Request.Form("m_prev_txtEmployedDuration_month"))
                    _MinorAppPersonalInfo.prev_txtEmployedDuration_year = Common.SafeString(Request.Form("m_prev_txtEmployedDuration_year"))
                Else
                    _MinorAppPersonalInfo.prev_txtJobTitle = Common.SafeString(Request.Form("m_prev_txtJobTitle"))
                    _MinorAppPersonalInfo.prev_txtEmployedDuration_month = Common.SafeString(Request.Form("m_prev_txtEmployedDuration_month"))
                    _MinorAppPersonalInfo.prev_txtEmployedDuration_year = Common.SafeString(Request.Form("m_prev_txtEmployedDuration_year"))
                    _MinorAppPersonalInfo.prev_txtEmployer = Common.SafeString(Request.Form("m_prev_txtEmployer"))
                End If


                _MinorAppPersonalInfo.prev_txtBusinessType = Common.SafeString(Request.Form("m_prev_txtBusinessType"))
                ''enlistment date for active military
                Dim m_prev_EmploymentStatus = CEnum.MapEmploymentStatusToValue(_MinorAppPersonalInfo.prev_EmploymentStatus)
                Dim m_prev_EnlistmentDate As String = Request.Form("m_prev_EnlistmentDate")
                If (m_prev_EmploymentStatus = "MI" And m_prev_EnlistmentDate <> "") Then
                    _MinorAppPersonalInfo.prev_txtEmploymentStartDate = Common.SafeString(m_prev_EnlistmentDate)
                Else
                    _MinorAppPersonalInfo.prev_txtEmploymentStartDate = Common.SafeString(Request.Form("m_prev_txtEmploymentStartDate"))
                End If
                _MinorAppPersonalInfo.prev_txtETS = Common.SafeString(Request.Form("m_prev_txtETS"))
                _MinorAppPersonalInfo.prev_ddlBranchOfService = Common.SafeString(Request.Form("m_prev_ddlBranchOfService"))
                _MinorAppPersonalInfo.prev_ddlPayGrade = Common.SafeString(Request.Form("m_prev_ddlPayGrade"))
            End If
            'end prev employment
        End If

        _MinorAppPersonalInfo.Citizenship = SafeString(Request.Form("m_Citizenship"))
        _MinorAppPersonalInfo.MotherMaidenName = SafeString(Request.Form("m_MotherMaidenName"))
        _MinorAppPersonalInfo.SSN = SafeString(Request.Form("m_SSN"))
        oLoanApp.oMinorAppPersonal = _MinorAppPersonalInfo
        ''end minor App personal info
        ''minor contact info
        Dim _MinorContact As New ContactInfo()
        _MinorContact.CellPhone = SafeString(Request.Form("m_MobilePhone"))
        _MinorContact.CellPhoneCountry = SafeString(Request.Form("m_MobilePhoneCountry"))
        _MinorContact.Email = SafeString(Request.Form("m_EmailAddr"))
        _MinorContact.HomePhone = SafeString(Request.Form("m_HomePhone"))
        _MinorContact.HomePhoneCountry = SafeString(Request.Form("m_HomePhoneCountry"))
        _MinorContact.PreferredContactMethod = SafeString(Request.Form("m_ContactMethod"))
        _MinorContact.WorkPhone = SafeString(Request.Form("m_WorkPhone"))
        _MinorContact.WorkPhoneCountry = SafeString(Request.Form("m_WorkPhoneCountry"))
        _MinorContact.WorkPhoneExt = SafeString(Request.Form("m_WorkPhoneEXT"))
        oLoanApp.oMinorAppContact = _MinorContact
        ''minor Identification
        Dim _MinorId As New IdentificationInfo()
        _MinorId.CardNumber = SafeString(Request.Form("m_IDCardNumber"))
        _MinorId.CardType = SafeString(Request.Form("m_IDCardType"))
        _MinorId.Country = SafeString(Request.Form("m_IDCountry"))
        _MinorId.DateIssued = SafeString(Request.Form("m_IDDateIssued"))
        _MinorId.ExpirationDate = SafeString(Request.Form("m_IDDateExpire"))
        _MinorId.State = SafeString(Request.Form("m_IDState"))
        oLoanApp.oMinorAppIDCard = _MinorId
        ''minor current address
        Dim _MinorAddress As New AddressInfo()
        _MinorAddress.Address = SafeString(Request.Form("m_AddressStreet"))
        _MinorAddress.City = SafeString(Request.Form("m_AddressCity"))
        _MinorAddress.State = SafeString(Request.Form("m_AddressState"))
        _MinorAddress.Zip = SafeString(Request.Form("m_AddressZip"))
        _MinorAddress.Country = SafeString(Request.Form("m_Country"))
        If Not String.IsNullOrEmpty(SafeString(Request.Form("m_OccupyingLocation"))) Then
            _MinorAddress.OccupancyDuration = SafeInteger(Request.Form("m_LiveMonths"))
            _MinorAddress.OccupancyStatus = Request.Form("m_OccupyingLocation")
        End If
        oLoanApp.oMinorAppAddress = _MinorAddress
        ''Minor mailing address
        Dim _MinorMailingAddress As New AddressInfo()
        _MinorMailingAddress.Address = SafeString(Request.Form("m_MailingAddressStreet"))
        _MinorMailingAddress.City = SafeString(Request.Form("m_MailingAddressCity"))
        _MinorMailingAddress.State = SafeString(Request.Form("m_MailingAddressState"))
        _MinorMailingAddress.Zip = SafeString(Request.Form("m_MailingAddressZip"))
        oLoanApp.oMinorMailingAddress = _MinorMailingAddress
        ' minor applicant question
        Dim oMinorAQSerializer As New JavaScriptSerializer()
        Dim oMinorAQAnswerList As New List(Of XAQuestionAnswer)
        If Not String.IsNullOrEmpty(SafeString(Request.Form("m_ApplicantQuestionAnswers"))) Then
            oMinorAQAnswerList = oMinorAQSerializer.Deserialize(Of List(Of XAQuestionAnswer))(Request.Form("m_ApplicantQuestionAnswers"))
        End If
        oLoanApp.oMinorApplicantQuestionList = oMinorAQAnswerList
        '' --end minor applicant
    End Sub

    Protected Sub setLoanID(ByVal responseStr As String)
        Dim myDocument As New XmlDocument
        myDocument.LoadXml(responseStr)

        Dim tempNodes As XmlNodeList
        loanID = ""
        Dim loanIDXMLStr As String
        tempNodes = myDocument.GetElementsByTagName("RESPONSE")
        For Each childNode As XmlNode In tempNodes
            loanIDXMLStr = childNode.Attributes("loan_id").InnerXml
            If (Not String.IsNullOrEmpty(loanIDXMLStr)) Then
                loanID = loanIDXMLStr
            End If
        Next
    End Sub

    Protected Function IsSubmitted(ByVal responseXml As String, Optional ByRef pIsFailed As Boolean = False) As Boolean
        Dim xmlDoc As New XmlDocument()

        xmlDoc.LoadXml(responseXml)
        'check the response is nothing
        If (xmlDoc.GetElementsByTagName("RESPONSE").Count = 0) Then
            pIsFailed = True
        End If
        If (xmlDoc.GetElementsByTagName("RESPONSE").Count = 0 And responseXml.IndexOf("VALIDATION_FAILED")) Then   '<ERROR type="VALIDATION_FAILED">
            pIsFailed = True
            Return False
        End If

        If xmlDoc.GetElementsByTagName("ERROR").Count > 0 Then
            If Not xmlDoc.GetElementsByTagName("ERROR")(0).InnerText.ToUpper.Contains("EXPIRED") Then 'expired  pw
                Return False
            End If
        End If

        If xmlDoc.GetElementsByTagName("FAILED").Count > 0 Then
            Return False
        End If

        If (responseXml.IndexOf("faileddetails") > -1) Or (responseXml.IndexOf("errordetails") > -1) Then 'Failed: Individual Not Located, invalid ssn
            pIsFailed = True
            Return True
        End If

        'for preciseID, RAS,Equifax EID
        If xmlDoc.GetElementsByTagName("QUESTIONS").Count = 0 Then  'no question is found
            pIsFailed = True   '
            Return True
        End If

        Return True
    End Function

    Private Function SafeString(ByVal obj As Object) As String
        If obj Is Nothing Then
            Return ""
        End If
        Return obj.ToString()
    End Function

    Private Function SafeInt(ByVal obj As Object) As Integer
        Dim res As Integer = 0
        If obj IsNot Nothing Then
            Integer.TryParse(obj.ToString(), res)
        End If
        Return res
    End Function

    Private Function SafeDouble(ByVal obj As Object) As Double
        Dim res As Double = 0
        If obj IsNot Nothing Then
            Double.TryParse(obj.ToString(), res)
        End If
        Return res
    End Function

    Public Class LoanAppInfo
        'Inherits CBasePage
        ''Public IsSSO As Boolean = False
        Public oPersonal As PersonalInfo
        Public oContact As ContactInfo
        Public oIDCard As IdentificationInfo
        Public oAddress As AddressInfo
        ''applicant question list
        Public oApplicantQuestionList As List(Of XAQuestionAnswer)
        ''co applicant question list
        Public oCoApplicantQuestionList As List(Of XAQuestionAnswer)

        ''Public oAccount1 As AccountInfo
        ''Public oAccount2 As AccountInfo
        ''Public oAccount3 As AccountInfo
        ''remove 3 account ->create a oAccountsList to store all accounts
        Public oAccountsList As List(Of AccountInfo)
        Public DocBase64Dic As New Dictionary(Of String, String) 'title, dada
        Public oUploadDocumentInfoList As List(Of uploadDocumentInfo)
        '' previous address
        Public oPreviousAddress As AddressInfo
        Public oCoPreviousAddress As AddressInfo
        '' add mailing address
        Public oMailingAddress As AddressInfo
        Public oCoMailingAddress As AddressInfo

        Public oCoAppPersonal As PersonalInfo
        Public oCoAppContact As ContactInfo
        Public oCoAppIDCard As IdentificationInfo
        Public oCoAppAddress As AddressInfo
        ''----disclosure
        Public Disclosure As String
        Public oLoanInfo As LoanInfo
        Public oConfig As CWebsiteConfig
        ''-------minor Applicant
        Public oMinorAppPersonal As PersonalInfo
        Public oMinorAppContact As ContactInfo
        Public oMinorAppIDCard As IdentificationInfo
        Public oMinorAppAddress As AddressInfo
        Public oMinorMailingAddress As AddressInfo
        ''minor applicant question list 
        Public oMinorApplicantQuestionList As List(Of XAQuestionAnswer)
        Public MinorAccountCode As String
        Public MinorRoleType As String
        Public SecondRoleType As String
        Public oAvailability As String
        Public eSignature As String
        Public oHouseHoldNumber As String
        Public oMediShareNumber As String
        Public oIsLive As String
        Private ReadOnly _log As log4net.ILog = log4net.LogManager.GetLogger(Me.GetType())


        Public Sub New(ByVal poConfig As CWebsiteConfig)
            Me.oConfig = poConfig
        End Sub

        Private Function FormatPhone(ByVal src As String) As String
            Dim sb As New StringBuilder
            Dim count As Integer = 0
            For Each c As Char In src
                If Char.IsDigit(c) Then
                    sb.Append(c)
                    count += 1
                End If

                If count = 10 Then
                    Exit For
                End If
            Next
            If count < 10 Then
                Return ""
            End If
            Return sb.ToString()
        End Function
        ''phone work ext is not exactly 4 digits(could be 2, 3, ... depend on lenders) -> need to update the phone work ext format
        Private Function FormatPhoneExt(ByVal src As String) As String
            Dim sb As New StringBuilder
            Dim count As Integer = 0
            For Each c As Char In src
                If Char.IsDigit(c) Then
                    sb.Append(c)
                    count += 1
                End If

                If count = 5 Then '' in loanspq ext maxlength =5 
                    Exit For
                End If
            Next

            If count < 1 Then ''no ext just return empty string
                Return ""
            End If

            Return sb.ToString()
        End Function
        Public Sub ApplicantQuestionAnswer(ByVal appPrefix As String, ByVal writer As XmlWriter)
            Dim oAQAnswerList As New List(Of XAQuestionAnswer)
            If appPrefix = "co_" Then
                oAQAnswerList = oCoApplicantQuestionList
            ElseIf appPrefix = "m_" Then
                oAQAnswerList = oMinorApplicantQuestionList
            Else
                oAQAnswerList = oApplicantQuestionList
            End If
            If oAQAnswerList.Count > 0 Then  ''make sure applicant question answer is not empty
                writer.WriteStartElement("APPLICANT_QUESTIONS")
                ' deserialize array string from json
                For Each oAQ As CCustomQuestionXA In CCustomQuestionXA.CurrentXAApplicantQuestions(oConfig)
                    'only work on applicant questions that are available to LPQMobile
                    Dim oAQName As String = ""
                    If Not String.IsNullOrEmpty(oAQ.Name) Then
                        oAQName = oAQ.Name
                    End If

                    Dim oAQIndex As Integer = -1
                    For i = 0 To oAQAnswerList.Count - 1
                        Dim oAQAnswerName As String = ""
                        If Not String.IsNullOrEmpty(oAQAnswerList(i).q) Then
                            oAQAnswerName = oAQAnswerList(i).q
                        End If
                        If oAQAnswerName.ToLower = oAQName.ToLower Then
                            oAQIndex = i
                            Exit For
                        End If
                    Next
                    If oAQIndex = -1 Then Continue For
                    writer.WriteStartElement("APPLICANT_QUESTION")
                    writer.WriteAttributeString("question_name", oAQ.Name)
                    writer.WriteAttributeString("question_type", oAQ.AnswerType)
                    ' swap for correct answer
                    ' find correct answer
                    If oAQ.AnswerType.ToLower = "checkbox" Then
                        'get all check boxes
                        For Each oItem As XAQuestionAnswer In oAQAnswerList
                            If Not String.IsNullOrEmpty(oItem.q) Then
                                If Not oItem.q.ToLower.Equals(oAQ.Name.ToLower) Then Continue For
                                For Each oOpt As CCustomQuestionOption In oAQ.CustomQuestionOptions
                                    If oOpt.Value.ToLower.Equals(oItem.a.ToLower) Or oOpt.Text.ToLower.Equals(oItem.a.ToLower) Then
                                        writer.WriteStartElement("APPLICANT_QUESTION_ANSWER")
                                        writer.WriteAttributeString("answer_text", oOpt.Text)
                                        writer.WriteAttributeString("answer_value", oOpt.Value)
                                        writer.WriteEndElement()
                                    End If
                                Next
                            End If
                        Next
                    Else
                        Dim realAnswerVal As String = ""
                        If Not String.IsNullOrEmpty(oAQAnswerList(oAQIndex).a) Then
                            realAnswerVal = oAQAnswerList(oAQIndex).a
                        End If
                        Dim realAnswerText As String = String.Empty
                        'case dropdown and textbox
                        If oAQ.AnswerType.ToLower = "dropdown" Then
                            For Each oOpt As CCustomQuestionOption In oAQ.CustomQuestionOptions
                                If oOpt.Value.Equals(realAnswerVal) Or oOpt.Text.Equals(realAnswerVal) Then
                                    realAnswerText = oOpt.Text
                                    realAnswerVal = oOpt.Value
                                    Exit For
                                End If
                            Next
                        Else 'textbox
                            realAnswerText = realAnswerVal
                        End If
                        writer.WriteStartElement("APPLICANT_QUESTION_ANSWER")
                        writer.WriteAttributeString("answer_text", realAnswerText)
                        writer.WriteAttributeString("answer_value", realAnswerVal)
                        writer.WriteEndElement()
                    End If
                    writer.WriteEndElement()
                Next
                writer.WriteEndElement()  ''end applicant question answer
            End If  ''end AQAnswersList
        End Sub
        Public Function DriverLiencseComments(ByVal oAppIDCard As IdentificationInfo, ByVal oAppPersonal As PersonalInfo) As String
            Dim strComment As String = ""
            Dim IDType As String = oAppIDCard.CardType
            Dim maxExpirationDate As New DateTime(2078, 12, 31) ''  "12/31/2078"
            If IDType.ToUpper = "DRIVERS_LICENSE" Then
                If Not String.IsNullOrEmpty(oAppIDCard.ExpirationDate) Then
                    Dim ExpirationDate As DateTime = Date.Parse(oAppIDCard.ExpirationDate)
                    If DateTime.Compare(ExpirationDate, maxExpirationDate) > 0 Then
                        strComment += "Status=REFERRED because " & oAppPersonal.FirstName & " " & oAppPersonal.LastName & " Drivers License expiration date: " & ExpirationDate & " > " & maxExpirationDate
                    End If
                End If
            End If
            Return strComment
        End Function
        Public Function IRSWithholdingCommnents() As String
            ' deserialize array string from json
            Dim listCQJsonQuestionAnswers As New List(Of XAQuestionAnswer)
            listCQJsonQuestionAnswers = (New JavaScriptSerializer()).Deserialize(Of List(Of XAQuestionAnswer))(HttpContext.Current.Request.Params("XAQuestionAnswers"))
            Dim strComment As String = ""
            If listCQJsonQuestionAnswers.Count > 0 Then
                For i = 0 To listCQJsonQuestionAnswers.Count - 1
                    If (listCQJsonQuestionAnswers(i).q.Length > 14) Then ''make sure the length of string at least the same as "IRS_WITHHOLDING" 
                        Dim questionName As String = listCQJsonQuestionAnswers(i).q.Substring(0, 15)
                        Dim questionAnswer As String = listCQJsonQuestionAnswers(i).a
                        If questionName.ToUpper = "IRS_WITHHOLDING" Then
                            If questionAnswer.ToUpper = "YES" Then
                                strComment += "Status=REFERRED because IRS Withholding = Yes."
                                Exit For
                            End If
                        End If
                    End If
                Next
            End If
            Return strComment
        End Function
        Public Function ToXmlString(Optional ByVal pbIncludeJoint As Boolean = False) As String
            Dim sb As New StringBuilder
            Dim writer As XmlWriter = XmlTextWriter.Create(sb)
            Dim hasMinorApp As String = oPersonal.HasMinor
            writer.WriteStartDocument()
            writer.WriteStartElement("XPRESS_LOAN", "http://www.meridianlink.com/CLF")

            writer.WriteAttributeString("xmlns", "xsi", Nothing, "http://www.w3.org/2001/XMLSchema-instance")
            writer.WriteAttributeString("xsi", "schemaLocation", Nothing, "http://www.meridianlink.com/CLF C:\LoansPQ2\CLF\xpress_loan.xsd http://www.meridianlink.com/CLF C:\LoansPQ2\CLF\Integration\SSFCU.xsd")
            writer.WriteAttributeString("version", "5.069")


            '' begin applicants
            writer.WriteStartElement("APPLICANTS")
            ''----minor applicant---------
            If (hasMinorApp = "Y") Then
                ToMinorXMLString(writer)
            End If
            '' begin applicant
            writer.WriteStartElement("APPLICANT")
            writer.WriteAttributeString("first_name", oPersonal.FirstName)
            writer.WriteAttributeString("middle_name", oPersonal.MiddleName)
            writer.WriteAttributeString("last_name", oPersonal.LastName)
            writer.WriteAttributeString("suffix", oPersonal.Suffix)
            writer.WriteAttributeString("gender", oPersonal.Gender)
            ' foreign address
            'writer.WriteAttributeString("home_phone", Common.SafeString(oContact.HomePhone).Replace("-", "").Replace("(", "").Replace(")", "").Replace(".", "").Replace(" ", ""))
            'writer.WriteAttributeString("is_home_phone_foreign", "Y")
            'writer.WriteAttributeString("work_phone", Common.SafeString(oContact.CellPhone).Replace("-", "").Replace("(", "").Replace(")", "").Replace(".", "").Replace(" ", ""))
            'writer.WriteAttributeString("is_work_phone_foreign", "Y")
            'writer.WriteAttributeString("cell_phone", Common.SafeString(oContact.WorkPhone).Replace("-", "").Replace("(", "").Replace(")", "").Replace(".", "").Replace(" ", ""))
            'writer.WriteAttributeString("is_cell_phone_foreign", "Y")
            If (Not String.IsNullOrEmpty(SecondRoleType)) Then
                writer.WriteAttributeString("role_type", SecondRoleType)
            End If
            If Not String.IsNullOrEmpty(oPersonal.DateOfBirth) Then
                writer.WriteAttributeString("dob", SafeDateXml(oPersonal.DateOfBirth))
            End If
            writer.WriteAttributeString("mother_maiden_name", oPersonal.MotherMaidenName)
            writer.WriteAttributeString("citizenship", CEnum.MapCitizenshipStatusToValue(oPersonal.Citizenship))
            If (Not String.IsNullOrEmpty(oPersonal.MaritalStatus)) Then
                writer.WriteAttributeString("marital_status", CEnum.MapMaritalStatusToValue(oPersonal.MaritalStatus))
            End If
            writer.WriteAttributeString("ssn", oPersonal.SSN)
            writer.WriteAttributeString("email", oContact.Email)
            writer.WriteAttributeString("member_number", oPersonal.MemberNumber)

            If oContact.HomePhoneCountry <> "" And Common.SafeString(oContact.HomePhoneCountry).ToUpper <> "US" Then
                writer.WriteAttributeString("home_phone_country", oContact.HomePhoneCountry.ToUpper)
                'writer.WriteAttributeString("is_home_phone_foreign", "Y")
                writer.WriteAttributeString("home_phone", oContact.HomePhone)
            Else
                writer.WriteAttributeString("home_phone", FormatPhone(oContact.HomePhone))
            End If


            If oContact.CellPhoneCountry <> "" And Common.SafeString(oContact.CellPhoneCountry).ToUpper <> "US" Then
                writer.WriteAttributeString("cell_phone_country", oContact.CellPhoneCountry.ToUpper)
                'writer.WriteAttributeString("is_cell_phone_foreign", "Y")
                writer.WriteAttributeString("cell_phone", oContact.CellPhone)
            Else
                writer.WriteAttributeString("cell_phone", FormatPhone(oContact.CellPhone))
            End If
            If oContact.WorkPhoneCountry <> "" And Common.SafeString(oContact.WorkPhoneCountry).ToUpper <> "US" Then
                writer.WriteAttributeString("work_phone_country", oContact.WorkPhoneCountry.ToUpper)
                'writer.WriteAttributeString("is_work_phone_foreign", "Y")
                writer.WriteAttributeString("work_phone", oContact.WorkPhone)
            Else
                writer.WriteAttributeString("work_phone", FormatPhone(oContact.WorkPhone))
            End If
            writer.WriteAttributeString("work_phone_extension", FormatPhoneExt(oContact.WorkPhoneExt))

            If oContact.PreferredContactMethod <> "" Then writer.WriteAttributeString("preferred_contact_method", oContact.PreferredContactMethod)

            '' current address
            writer.WriteStartElement("CURRENT_ADDRESS")
            ''---mapping the occupancy status   
            If Not String.IsNullOrEmpty(oAddress.OccupancyStatus) Then
                Dim OStatus As String = ""
                OStatus = CEnum.MapOccupancyTypeToValue(oAddress.OccupancyStatus)
                'TODO:occupancy_status
                writer.WriteAttributeString("occupancy_status", OStatus)
                writer.WriteAttributeString("occupancy_duration", oAddress.OccupancyDuration)
            End If
            ''select either threeline or loose format
            If oAddress.Country <> "" And oAddress.Country <> "USA" Then
                ' three line address, foreign
                writer.WriteStartElement("THREE_LINE_ADDRESS")
                writer.WriteAttributeString("street_address_1", oAddress.Address)
                writer.WriteAttributeString("city", oAddress.City)
                writer.WriteAttributeString("country", oAddress.Country)
                writer.WriteAttributeString("zip", oAddress.Zip)
                writer.WriteEndElement() ' end three line address
            Else
                'domestic
                writer.WriteStartElement("LOOSE_ADDRESS")
                writer.WriteAttributeString("street_address_1", oAddress.Address)
                writer.WriteAttributeString("street_address_2", "")
                writer.WriteAttributeString("city", oAddress.City)
                writer.WriteAttributeString("state", oAddress.State)
                writer.WriteAttributeString("zip", oAddress.Zip)
                writer.WriteEndElement() 'end LOOSE_ADDRESS
            End If
            writer.WriteEndElement() '' end current address



            Dim previousAddress As String = oPreviousAddress.Address
            If Not String.IsNullOrEmpty(previousAddress) Then
                writer.WriteStartElement("PREVIOUS_ADDRESS")
                writer.WriteAttributeString("street_address_1", oPreviousAddress.Address)
                writer.WriteAttributeString("street_address_2", "")
                writer.WriteAttributeString("city", oPreviousAddress.City)
                writer.WriteAttributeString("state", oPreviousAddress.State)
                writer.WriteAttributeString("zip", oPreviousAddress.Zip)
                writer.WriteEndElement()
            Else
                writer.WriteStartElement("PREVIOUS_ADDRESS")
                writer.WriteEndElement()
            End If


            Dim mailingAddress As String = oMailingAddress.Address
            If Not String.IsNullOrEmpty(mailingAddress) Then
                writer.WriteStartElement("MAILING_ADDRESS")
                writer.WriteAttributeString("street_address_1", oMailingAddress.Address)
                writer.WriteAttributeString("street_address_2", "")
                writer.WriteAttributeString("city", oMailingAddress.City)
                writer.WriteAttributeString("state", oMailingAddress.State)
                writer.WriteAttributeString("zip", oMailingAddress.Zip)
                writer.WriteAttributeString("country", oMailingAddress.Country)
                writer.WriteAttributeString("is_current", "N")
                writer.WriteEndElement()
            Else
                writer.WriteStartElement("MAILING_ADDRESS")
                writer.WriteAttributeString("is_current", "Y")
                writer.WriteEndElement()
            End If
            writer.WriteStartElement("FINANCIAL_INFO")
            If Not String.IsNullOrEmpty(oPersonal.EmployeeOfLender) And oPersonal.EmployeeOfLender <> "NONE" Then
                writer.WriteAttributeString("employee_of_lender_type", oPersonal.EmployeeOfLender)
            End If
            If oPersonal.EmploymentSectionEnabled Then
                Dim employment_status = CEnum.MapEmploymentStatusToValue(oPersonal.EmploymentStatus)
                If Not String.IsNullOrEmpty(employment_status) Then
                    writer.WriteAttributeString("employment_status", employment_status)
                    ' new employment logic
                    If oPersonal.EmploymentStatus.Contains("MILITARY") Then
                        writer.WriteAttributeString("employer", CEnum.MapBranchOfServiceToValue(oPersonal.ddlBranchOfService))
                    Else
                        writer.WriteAttributeString("employer", oPersonal.txtEmployer)
                    End If
                    Dim employed_months As Integer
                    employed_months = SafeInteger(oPersonal.txtEmployedDuration_year) * 12 + SafeInteger(oPersonal.txtEmployedDuration_month)
                    Dim profession_months As Integer
                    profession_months = SafeInteger(oPersonal.txtProfessionDuration_year) * 12 + SafeInteger(oPersonal.txtProfessionDuration_month)
                    writer.WriteAttributeString("employed_months", employed_months.ToString())
                    If profession_months = 0 Then
                        writer.WriteAttributeString("profession_months", employed_months.ToString())
                    Else
                        writer.WriteAttributeString("profession_months", profession_months.ToString())
                    End If
                    writer.WriteAttributeString("occupation", oPersonal.txtJobTitle)
                    writer.WriteAttributeString("employment_business_type", oPersonal.txtBusinessType)
                    writer.WriteAttributeString("supervisor_name", oPersonal.txtSupervisorName)
                    writer.WriteAttributeString("pay_grade", oPersonal.ddlPayGrade)
                    writer.WriteAttributeString("monthly_income_base_salary", oPersonal.txtGrossMonthlyIncome)
                    If Not String.IsNullOrEmpty(SafeDateXml(oPersonal.txtEmploymentStartDate)) Then
                        writer.WriteAttributeString("employment_start_date", SafeDateXml(oPersonal.txtEmploymentStartDate))
                    End If
                    If Not String.IsNullOrEmpty(oPersonal.txtETS) Then
                        writer.WriteAttributeString("ets", SafeDateXml(oPersonal.txtETS))
                    End If
                    ' end new employment logic
                Else
                    writer.WriteAttributeString("employment_status", "OT")  'this is required for decisionXA
                End If
            End If

            writer.WriteEndElement() ' END </FINANCIAL_INFO>

            writer.WriteStartElement("ID_CARD")
            writer.WriteAttributeString("card_type", oIDCard.CardType)
            writer.WriteAttributeString("card_number", oIDCard.CardNumber)
            writer.WriteAttributeString("state", oIDCard.State)
            writer.WriteAttributeString("country", oIDCard.Country)

            If Not String.IsNullOrEmpty(SafeDateXml(oIDCard.DateIssued)) Then
                writer.WriteAttributeString("date_issued", SafeDateXml(oIDCard.DateIssued))
            End If

            If Not String.IsNullOrEmpty(oIDCard.ExpirationDate) Then
                writer.WriteAttributeString("exp_date", SafeDateXml(oIDCard.ExpirationDate))
            End If
            writer.WriteEndElement() 'end ID card 
            ''Applicant Question Answers
            '''''''''''
            ApplicantQuestionAnswer("", writer)
            ' BEGIN JOint-APPLICANT
            '' begin applicant
            If (True = pbIncludeJoint) Then
                writer.WriteStartElement("SPOUSE")
                writer.WriteAttributeString("first_name", oCoAppPersonal.FirstName)
                writer.WriteAttributeString("middle_name", oCoAppPersonal.MiddleName)
                writer.WriteAttributeString("last_name", oCoAppPersonal.LastName)
                writer.WriteAttributeString("suffix", oCoAppPersonal.Suffix)
                writer.WriteAttributeString("gender", oCoAppPersonal.Gender)
                If (Not String.IsNullOrEmpty(SecondRoleType)) Then
                    writer.WriteAttributeString("role_type", SecondRoleType)
                End If
                If Not String.IsNullOrEmpty(oCoAppPersonal.DateOfBirth) Then
                    writer.WriteAttributeString("dob", SafeDateXml(oCoAppPersonal.DateOfBirth))
                End If

                writer.WriteAttributeString("mother_maiden_name", oCoAppPersonal.MotherMaidenName)
                writer.WriteAttributeString("citizenship", CEnum.MapCitizenshipStatusToValue(oCoAppPersonal.Citizenship))
                If (Not String.IsNullOrEmpty(oCoAppPersonal.MaritalStatus)) Then
                    writer.WriteAttributeString("marital_status", CEnum.MapMaritalStatusToValue(oCoAppPersonal.MaritalStatus))
                End If
                writer.WriteAttributeString("ssn", oCoAppPersonal.SSN)
                writer.WriteAttributeString("email", oCoAppContact.Email)
                writer.WriteAttributeString("member_number", oCoAppPersonal.MemberNumber)

                If oCoAppContact.HomePhoneCountry <> "" And Common.SafeString(oCoAppContact.HomePhoneCountry).ToUpper <> "US" Then
                    writer.WriteAttributeString("home_phone_country", oCoAppContact.HomePhoneCountry.ToUpper)
                    'writer.WriteAttributeString("is_home_phone_foreign", "Y")
                    writer.WriteAttributeString("home_phone", oCoAppContact.HomePhone)
                Else
                    writer.WriteAttributeString("home_phone", FormatPhone(oCoAppContact.HomePhone))
                End If


                If oCoAppContact.CellPhoneCountry <> "" And Common.SafeString(oCoAppContact.CellPhoneCountry).ToUpper <> "US" Then
                    writer.WriteAttributeString("cell_phone_country", oCoAppContact.CellPhoneCountry.ToUpper)
                    'writer.WriteAttributeString("is_cell_phone_foreign", "Y")
                    writer.WriteAttributeString("cell_phone", oCoAppContact.CellPhone)
                Else
                    writer.WriteAttributeString("cell_phone", FormatPhone(oCoAppContact.CellPhone))
                End If
                If oCoAppContact.WorkPhoneCountry.ToUpper <> "" And Common.SafeString(oCoAppContact.WorkPhoneCountry).ToUpper <> "US" Then
                    writer.WriteAttributeString("work_phone_country", oCoAppContact.WorkPhoneCountry.ToUpper)
                    'writer.WriteAttributeString("is_work_phone_foreign", "Y")
                    writer.WriteAttributeString("work_phone", oCoAppContact.WorkPhone)
                Else
                    writer.WriteAttributeString("work_phone", FormatPhone(oCoAppContact.WorkPhone))
                End If
                writer.WriteAttributeString("work_phone_extension", FormatPhoneExt(oCoAppContact.WorkPhoneExt))

                If oCoAppContact.PreferredContactMethod <> "" Then writer.WriteAttributeString("preferred_contact_method", oCoAppContact.PreferredContactMethod)
                '' current address
                writer.WriteStartElement("CURRENT_ADDRESS")
                '---mapping the occupancy status   
                If Not String.IsNullOrEmpty(oCoAppAddress.OccupancyStatus) Then
                    Dim co_OStatus As String = ""
                    co_OStatus = CEnum.MapOccupancyTypeToValue(oCoAppAddress.OccupancyStatus)
                    writer.WriteAttributeString("occupancy_status", co_OStatus)
                    writer.WriteAttributeString("occupancy_duration", oCoAppAddress.OccupancyDuration)
                End If
                writer.WriteStartElement("LOOSE_ADDRESS")
                writer.WriteAttributeString("street_address_1", oCoAppAddress.Address)
                writer.WriteAttributeString("street_address_2", "")
                writer.WriteAttributeString("city", oCoAppAddress.City)
                writer.WriteAttributeString("state", oCoAppAddress.State)
                writer.WriteAttributeString("zip", oCoAppAddress.Zip)
                writer.WriteEndElement()
                writer.WriteEndElement()
                '' end current address
                Dim co_previousAddress As String = oCoPreviousAddress.Address
                If Not String.IsNullOrEmpty(co_previousAddress) Then
                    writer.WriteStartElement("PREVIOUS_ADDRESS")
                    writer.WriteAttributeString("street_address_1", oCoPreviousAddress.Address)
                    writer.WriteAttributeString("street_address_2", "")
                    writer.WriteAttributeString("city", oCoPreviousAddress.City)
                    writer.WriteAttributeString("state", oCoPreviousAddress.State)
                    writer.WriteAttributeString("zip", oCoPreviousAddress.Zip)
                    writer.WriteEndElement()
                Else
                    writer.WriteStartElement("PREVIOUS_ADDRESS")
                    writer.WriteEndElement()
                End If
                Dim coMailingAddress As String = oCoMailingAddress.Address
                If Not String.IsNullOrEmpty(coMailingAddress) Then
                    writer.WriteStartElement("MAILING_ADDRESS")
                    writer.WriteAttributeString("street_address_1", oCoMailingAddress.Address)
                    writer.WriteAttributeString("street_address_2", "")
                    writer.WriteAttributeString("city", oCoMailingAddress.City)
                    writer.WriteAttributeString("state", oCoMailingAddress.State)
                    writer.WriteAttributeString("zip", oCoMailingAddress.Zip)
                    writer.WriteAttributeString("country", oCoMailingAddress.Country)
                    writer.WriteAttributeString("is_current", "N")
                    writer.WriteEndElement()
                Else
                    writer.WriteStartElement("MAILING_ADDRESS")
                    writer.WriteAttributeString("is_current", "Y")
                    writer.WriteEndElement()
                End If
                writer.WriteStartElement("FINANCIAL_INFO")
                If Not String.IsNullOrEmpty(oCoAppPersonal.EmployeeOfLender) And oCoAppPersonal.EmployeeOfLender <> "NONE" Then
                    writer.WriteAttributeString("employee_of_lender_type", oCoAppPersonal.EmployeeOfLender)
                End If
                If oCoAppPersonal.EmploymentSectionEnabled = True Then

                    Dim co_employment_status = CEnum.MapEmploymentStatusToValue(oCoAppPersonal.EmploymentStatus)

                    If Not String.IsNullOrEmpty(co_employment_status) Then
                        writer.WriteAttributeString("employment_status", co_employment_status)
                        ' new employment logic
                        If oCoAppPersonal.EmploymentStatus.Contains("MILITARY") Then
                            writer.WriteAttributeString("employer", CEnum.MapBranchOfServiceToValue(oCoAppPersonal.ddlBranchOfService))
                        Else
                            writer.WriteAttributeString("employer", oCoAppPersonal.txtEmployer)
                        End If
                        'Dim employed_months As Integer
                        Dim employed_months = SafeInteger(oCoAppPersonal.txtEmployedDuration_year) * 12 + SafeInteger(oCoAppPersonal.txtEmployedDuration_month)
                        'Dim profession_months As Integer
                        Dim profession_months = SafeInteger(oCoAppPersonal.txtProfessionDuration_year) * 12 + SafeInteger(oCoAppPersonal.txtProfessionDuration_month)
                        writer.WriteAttributeString("employed_months", employed_months.ToString())
                        If profession_months = 0 Then
                            writer.WriteAttributeString("profession_months", employed_months.ToString())
                        Else
                            writer.WriteAttributeString("profession_months", profession_months.ToString())
                        End If
                        writer.WriteAttributeString("occupation", oCoAppPersonal.txtJobTitle)
                        writer.WriteAttributeString("employment_business_type", oCoAppPersonal.txtBusinessType)
                        writer.WriteAttributeString("supervisor_name", oCoAppPersonal.txtSupervisorName)
                        writer.WriteAttributeString("pay_grade", oCoAppPersonal.ddlPayGrade)
                        writer.WriteAttributeString("monthly_income_base_salary", oCoAppPersonal.txtGrossMonthlyIncome)
                        If Not String.IsNullOrEmpty(SafeDateXml(oCoAppPersonal.txtEmploymentStartDate)) Then
                            writer.WriteAttributeString("employment_start_date", SafeDateXml(oCoAppPersonal.txtEmploymentStartDate))
                        End If
                        If Not String.IsNullOrEmpty(oCoAppPersonal.txtETS) Then
                            writer.WriteAttributeString("ets", SafeDateXml(oCoAppPersonal.txtETS))
                        End If
                    Else
                        writer.WriteAttributeString("employment_status", "OT")
                        ' end new employment logic
                    End If
                End If

                ' END </FINANCIAL_INFO>
                writer.WriteEndElement()

                writer.WriteStartElement("ID_CARD")
                writer.WriteAttributeString("card_type", oCoAppIDCard.CardType)
                writer.WriteAttributeString("card_number", oCoAppIDCard.CardNumber)
                writer.WriteAttributeString("state", oCoAppIDCard.State)
                writer.WriteAttributeString("country", oCoAppIDCard.Country)

                If Not String.IsNullOrEmpty(SafeDateXml(oCoAppIDCard.DateIssued)) Then
                    writer.WriteAttributeString("date_issued", SafeDateXml(oCoAppIDCard.DateIssued))
                End If

                If Not String.IsNullOrEmpty(oCoAppIDCard.ExpirationDate) Then
                    writer.WriteAttributeString("exp_date", SafeDateXml(oCoAppIDCard.ExpirationDate))
                End If
                writer.WriteEndElement() ''end ID card
                ''Co Applicant Question Answers
                ApplicantQuestionAnswer("co_", writer)
                writer.WriteEndElement() '' end applicant FOR JOINT-APPLICANT
            End If  'End joint

            writer.WriteEndElement()  '' end applicant



            '_CurrentUserAgent = Request.ServerVariables("HTTP_USER_AGENT")
            'If _CurrentUserAgent.ToLower.IndexOf("android") > -1 Then
            '	_textAndroidTel = "tel"
            'End If

            'Dim sIP As String = Request.UserHostAddress
            '_CurrentHost = Request.Url.Host



            writer.WriteEndElement()
            '' end applicants

            '' begin new xml writer
            writer.WriteStartElement("COMMENTS")
            ''''write internal comment element: get disclosures and originating ip address
            writer.WriteStartElement("INTERNAL_COMMENTS")
            '' only for lender start with "BCU"
            Dim bFailPreUnderwrite As Boolean = False
            If oConfig.LenderRef.StartsWith("bcu") Then
                If Not String.IsNullOrEmpty(IRSWithholdingCommnents()) Then
                    writer.WriteValue("SYSTEM (" + DateTime.Now + "): " + IRSWithholdingCommnents())
                    writer.WriteValue(Environment.NewLine)
                    bFailPreUnderwrite = True
                End If
                If oIDCard IsNot Nothing AndAlso Not String.IsNullOrEmpty(DriverLiencseComments(oIDCard, oPersonal)) Then
                    writer.WriteValue("SYSTEM (" + DateTime.Now + "): " + DriverLiencseComments(oIDCard, oPersonal))
                    writer.WriteValue(Environment.NewLine)
                    bFailPreUnderwrite = True
                End If
                If pbIncludeJoint Then
                    If oCoAppIDCard IsNot Nothing AndAlso String.IsNullOrEmpty(DriverLiencseComments(oCoAppIDCard, oCoAppPersonal)) Then
                        writer.WriteValue("SYSTEM (" + DateTime.Now + "): " + DriverLiencseComments(oCoAppIDCard, oCoAppPersonal))
                        writer.WriteValue(Environment.NewLine)
                        bFailPreUnderwrite = True
                    End If
                End If
            End If

            writer.WriteValue(oContact.Email + " (" + DateTime.Now + "): Disclosure(s) checked: " + Disclosure)
            If Not String.IsNullOrEmpty(eSignature) Then
                Dim index As Integer = eSignature.IndexOf("eSignature:")
                writer.WriteValue(Environment.NewLine)
                writer.WriteValue(eSignature.Substring(0, index))
                writer.WriteValue(Environment.NewLine)
                writer.WriteValue(Environment.NewLine)
                writer.WriteValue(eSignature.Substring(index))
            End If
            writer.WriteValue(Environment.NewLine)
            writer.WriteValue("SYSTEM (" + DateTime.Now + "): Originating IP Address: " + oConfig.RequestedIP)
            writer.WriteValue(Environment.NewLine)
            writer.WriteValue("SYSTEM (" + DateTime.Now + "): User Agent: " + oConfig.RequestedUserAgent)

            writer.WriteValue(Environment.NewLine)
            writer.WriteValue("SYSTEM (" + DateTime.Now + "): MediShareNumber:[" + oMediShareNumber + "]")

            writer.WriteValue(Environment.NewLine)
            writer.WriteValue("SYSTEM (" + DateTime.Now + "): HouseHoldNumber:[" + oHouseHoldNumber + "]")
            writer.WriteEndElement()
            writer.WriteEndElement()

            writer.WriteStartElement("LOAN_INFO")
            If oConfig.LoanStatusEnum <> "" Then 'set default status for XA that had underwritting
                If bFailPreUnderwrite Then
                    writer.WriteAttributeString("status", "REFERRED")  'fail preUnderwrite
                Else
                    writer.WriteAttributeString("status", "PENDING")
                End If
            Else
                writer.WriteAttributeString("status", "NEW")
            End If

            If oIsLive = "Y" Then
                writer.WriteAttributeString("instant_membership_requirement_id", "3db6c43a106d426fbb11e06a198e57e2")
            Else
                writer.WriteAttributeString("instant_membership_requirement_id", "581d03add4b341a4acb8aad52a65a7ce")
            End If


            writer.WriteStartAttribute("create_date")
            writer.WriteValue(DateTime.Now)
            writer.WriteEndAttribute()
            writer.WriteAttributeString("account_position", oLoanInfo.account_position)
            ''minor Applicant(special account)
            If oPersonal.HasMinor = "Y" Then
                writer.WriteAttributeString("entity_type", "S")
            End If
            writer.WriteEndElement()

            writer.WriteStartElement("CUSTOM_QUESTIONS")
            ' deserialize array string from json
            Dim listCQJsonQuestionAnswers As List(Of XAQuestionAnswer) = (New JavaScriptSerializer()).Deserialize(Of List(Of XAQuestionAnswer))(HttpContext.Current.Request.Params("XAQuestionAnswers"))
            For Each oCQ As CCustomQuestionXA In CCustomQuestionXA.CurrentXACustomQuestions(oConfig)

                'only work on questions that are available to LPQMobile
                Dim indexCQJsonQuestionAnswer As Integer = -1
                For i = 0 To listCQJsonQuestionAnswers.Count - 1
                    If Not String.IsNullOrEmpty(listCQJsonQuestionAnswers(i).q) Then
                        If listCQJsonQuestionAnswers(i).q.ToLower = oCQ.Name.ToLower Then
                            indexCQJsonQuestionAnswer = i
                            Exit For
                        End If
                    End If
                Next
                If indexCQJsonQuestionAnswer = -1 Then Continue For

                writer.WriteStartElement("CUSTOM_QUESTION")
                writer.WriteAttributeString("question_name", oCQ.Name)
                writer.WriteAttributeString("question_type", oCQ.AnswerType)

                ' swap for correct answer
                ' find correct answer
                If oCQ.AnswerType.ToLower = "checkbox" Then

                    'get all check boxes
                    For Each oItem As XAQuestionAnswer In listCQJsonQuestionAnswers
                        If String.IsNullOrEmpty(oItem.q) Then Continue For
                        If Not oItem.q.ToLower.Equals(oCQ.Name.ToLower) Then Continue For
                        For Each oOpt As CCustomQuestionOption In oCQ.CustomQuestionOptions
                            If oOpt.Value.ToLower.Equals(oItem.a.ToLower) Or oOpt.Text.ToLower.Equals(oItem.a.ToLower) Then
                                writer.WriteStartElement("CUSTOM_QUESTION_ANSWER")
                                writer.WriteAttributeString("answer_text", oOpt.Text)
                                writer.WriteAttributeString("answer_value", oOpt.Value)
                                writer.WriteEndElement()
                            End If
                        Next
                    Next
                Else
                    Dim realAnswerVal As String = listCQJsonQuestionAnswers(indexCQJsonQuestionAnswer).a
                    Dim realAnswerText As String = String.Empty

                    'case dropdown and textbox
                    If oCQ.AnswerType.ToLower = "dropdown" Then
                        For Each oOpt As CCustomQuestionOption In oCQ.CustomQuestionOptions
                            If oOpt.Value.Equals(realAnswerVal) Or oOpt.Text.Equals(realAnswerVal) Then
                                realAnswerText = oOpt.Text
                                realAnswerVal = oOpt.Value
                                Exit For
                            End If
                        Next
                    Else 'textbox
                        realAnswerText = realAnswerVal
                    End If
                    writer.WriteStartElement("CUSTOM_QUESTION_ANSWER")
                    writer.WriteAttributeString("answer_text", realAnswerText)
                    writer.WriteAttributeString("answer_value", realAnswerVal)
                    writer.WriteEndElement()
                End If
                writer.WriteEndElement()
            Next
            writer.WriteEndElement()

            writer.WriteStartElement("SYSTEM")
            writer.WriteAttributeString("source", "GATEWAY")
            writer.WriteAttributeString("type", "LPQ")

            'TODO: enable this for all lender in mid 2016 when all lpq code is updated
            If oConfig.LenderRef.StartsWith("bcu_test") Then
                writer.WriteAttributeString("origination_ip", oConfig.RequestedIP)
            End If
            '' don't use sso and refferal_source
            ''If oLoanInfo.referral_source <> "" Then
            ''    writer.WriteAttributeString("external_source", Right(oLoanInfo.referral_source, 20))
            ''ElseIf IsSSO Then
            ''    writer.WriteAttributeString("external_source", "MOBILE WEBSITE SSO")
            ''Else
            ''    writer.WriteAttributeString("external_source", "MOBILE WEBSITE")
            ''End If

            writer.WriteAttributeString("external_source", "MediShare")

            'writer.WriteAttributeString("loan_number", "")
            'writer.WriteAttributeString("loan_id", "")
            If oConfig.sDefaultXABranchName <> "" And oConfig.sDefaultXABranchID <> "" Then 'this has higher priority than CurrentBranchID bc CurrentBranchID can be derived from DEFAULT_LOAN_BRANCH or url parameter
                writer.WriteStartElement("BRANCH")
                writer.WriteAttributeString("id", oConfig.sDefaultXABranchID)
                writer.WriteEndElement()
            ElseIf oConfig.CurrentBranchID <> "" Then
                writer.WriteStartElement("BRANCH")
                writer.WriteAttributeString("id", oConfig.CurrentBranchID)
                writer.WriteEndElement()
            End If

            writer.WriteStartElement("LENDER")
            'writer.WriteAttributeString("id", "e4e6624600b7441e8eee00eac47f11d4")
            writer.WriteAttributeString("id", oConfig.LenderId)
            writer.WriteEndElement()
            writer.WriteStartElement("ORGANIZATION")
            'writer.WriteAttributeString("id", "11842847ef2c47c3a8a029fa20d3ff19")
            writer.WriteAttributeString("id", oConfig.OrganizationId)
            writer.WriteEndElement()
            writer.WriteEndElement()

            Dim funding As CFundingSourceInfo = (New JavaScriptSerializer()).Deserialize(Of CFundingSourceInfo)(HttpContext.Current.Request.Params("rawFundingSource"))
            Dim bIsFunded As Boolean = funding IsNot Nothing
            Dim sFundingType As String = ""
            If funding IsNot Nothing Then
                sFundingType = funding.ValueFromType(funding.fsFundingType)
            End If
            ''if has coApp get coAppSSN
            Dim coAppSSN As String = String.Empty
            If (True = pbIncludeJoint) Then
                coAppSSN = oCoAppPersonal.SSN
            End If

            'add approved account 
            Dim productList As List(Of CProduct) = CProduct.GetProducts(oConfig, oAvailability) '' TODO: NEED TO PASS IN ACCOUNT TYPE

            'XA feature attribute' deprecated, doesn't work out for kscfcu
            '         Dim oFeatureNodes As XmlNodeList = oConfig._webConfigXML.SelectNodes("XA_LOAN/FEATURE")
            'Dim isDisableAutoApprovedAccount As Boolean = False
            '         If oFeatureNodes.Count > 0 Then
            '             If oFeatureNodes(0).Attributes("disable_auto_approved_account").InnerText = "Y" Then
            '                 isDisableAutoApprovedAccount = True
            '             End If
            'End If

            'If Not isDisableAutoApprovedAccount And Not oConfig.IsDecisionXAEnable Then
            If Not oConfig.IsDecisionXAEnable Then
                'add approved account
                writer.WriteStartElement("APPROVED_ACCOUNTS")
                Dim approved_account_index As Integer
                If oAccountsList.Count > 0 Then
                    For approved_account_index = 0 To oAccountsList.Count - 1
                        writer.WriteRaw(oAccountsList(approved_account_index).ToXmlString(productList, True, sFundingType, coAppSSN))
                    Next
                End If
                writer.WriteEndElement()
            End If

            ''adding account here
            writer.WriteStartElement("INTERESTED_ACCOUNTS")
            Dim interested_account_index As Integer
            If oAccountsList.Count > 0 Then
                For interested_account_index = 0 To oAccountsList.Count - 1
                    writer.WriteRaw(oAccountsList(interested_account_index).ToXmlString(productList, False, sFundingType, coAppSSN))
                Next
            End If
            writer.WriteEndElement()


            'move to before Approved account so we can check for funding status
            'Dim funding As CFundingSourceInfo = (New JavaScriptSerializer()).Deserialize(Of CFundingSourceInfo)(HttpContext.Current.Request.Params("rawFundingSource"))
            writer.WriteStartElement("FUNDING_SOURCES")
            If funding IsNot Nothing Then
                writer.WriteStartElement("FUNDING_SOURCE")
                If funding.ValueFromType(funding.fsFundingType) = "CASH" Then
                    writer.WriteAttributeString("clf_funding_source_id", "0")
                ElseIf funding.ValueFromType(funding.fsFundingType) = "MAIL" Then
                    writer.WriteAttributeString("clf_funding_source_id", "1")
                ElseIf funding.ValueFromType(funding.fsFundingType) = "NOT_FUNDING" Then
                    writer.WriteAttributeString("clf_funding_source_id", "2")
                ElseIf funding.ValueFromType(funding.fsFundingType) = "PAYPAL" Then
                    writer.WriteAttributeString("clf_funding_source_id", "3")
                Else
                    writer.WriteAttributeString("clf_funding_source_id", "10")
                End If

                writer.WriteAttributeString("funding_type", funding.ValueFromType(funding.fsFundingType))


                Select Case funding.fsFundingType
                    Case "CREDIT CARD"
                        writer.WriteAttributeString("name_on_card", funding.cNameOnCard)
                        writer.WriteAttributeString("cc_card_type", funding.cCardType)
                        writer.WriteAttributeString("cc_card_number", funding.cCreditCardNumber)
                        writer.WriteAttributeString("cc_card_exp_date", String.Format("{0}-{1}-01", funding.cExpirationDate.Substring(3), funding.cExpirationDate.Substring(0, 2)))
                        writer.WriteAttributeString("cc_cvn_number", funding.cCVNNumber)
                        writer.WriteAttributeString("billing_street_address", funding.cBillingAddress)
                        writer.WriteAttributeString("billing_city", funding.cBillingCity)
                        writer.WriteAttributeString("billing_state", funding.cBillingState)
                        writer.WriteAttributeString("billing_zip", funding.cBillingZip)
                    Case "TRANSFER FROM ANOTHER FINANCIAL INSTITUTION"
                        writer.WriteAttributeString("routing_number", funding.bRoutingNumber)
                        writer.WriteAttributeString("bank_name_on_card", funding.bNameOnCard)
                        writer.WriteAttributeString("bank_account_number", funding.bAccountNumber)
                        writer.WriteAttributeString("bank_bankstate", funding.bBankState)
                        writer.WriteAttributeString("bank_bankname", funding.bBankName)
                        writer.WriteAttributeString("bank_account_type", funding.bAccountType)

                    Case "INTERNAL TRANSFER"
                        writer.WriteAttributeString("transfer_account_number", funding.tAccountNumber)
                        writer.WriteAttributeString("transfer_account_type", funding.tAccountType)

                End Select

                writer.WriteEndElement()
            End If

            writer.WriteEndElement()

            If hasMinorApp = "Y" Then
                ''minor applicant special info
                writer.WriteStartElement("SPECIAL_INFO")
                If Not String.IsNullOrEmpty(MinorAccountCode) Then
                    writer.WriteAttributeString("special_account_type_code", MinorAccountCode)
                Else
                    _log.Info("Unable to get minor code")
                End If
                writer.WriteEndElement() ''end special info
            End If
            'Start fom section
            ' this list may be empty

            Dim listFomAnswers As New List(Of String)
            If Not String.IsNullOrEmpty(Common.SafeString(HttpContext.Current.Request.Params("FOMAnswers"))) Then
                listFomAnswers = (New JavaScriptSerializer()).Deserialize(Of List(Of String))(HttpContext.Current.Request.Params("FOMAnswers"))
            End If

            If listFomAnswers.Count > 0 Then
                '' begin fom question section
                writer.WriteStartElement("FOM_ANSWERED_QUESTIONS")
                ' check the index
                Dim selectedFOMIdx As Integer = -1
                Integer.TryParse(HttpContext.Current.Request.Params("FOMIdx"), selectedFOMIdx)
                If selectedFOMIdx >= 0 And Not (HttpContext.Current.Request.Params("FOMIdx") = Nothing) Then
                    Dim oFQ As CFOMQuestion = CFOMQuestion.CurrentFOMQuestions(oConfig)(selectedFOMIdx)
                    writer.WriteStartElement("FOM_ANSWERED_QUESTION")
                    writer.WriteAttributeString("name", oFQ.Name)
                    writer.WriteStartElement("text_template")
                    writer.WriteString(oFQ.TextTemplate)
                    writer.WriteEndElement()

                    writer.WriteStartElement("FOM_ANSWERS")

                    Dim fieldIdx As Integer = 0
                    For Each _field As CFOMQuestionField In oFQ.Fields
                        writer.WriteStartElement("FOM_ANSWER")

                        writer.WriteAttributeString("field_type", _field.Type.ToUpper())

                        Dim _val As String = IIf(listFomAnswers.Count > fieldIdx, listFomAnswers(fieldIdx), "")
                        Dim _text As String = _val

                        writer.WriteStartElement("value")
                        writer.WriteString(_val)
                        writer.WriteEndElement()

                        For Each _ans As CFOMQuestionAnswer In _field.Answers
                            If _ans.Value.Equals(_val) Then
                                _text = _ans.Text
                                Exit For
                            End If
                        Next

                        writer.WriteStartElement("text")
                        writer.WriteString(_text)
                        writer.WriteEndElement()

                        writer.WriteEndElement()
                        fieldIdx += 1
                    Next
                    writer.WriteEndElement()
                    writer.WriteEndElement()
                End If
                ''get nextquestion and nextquestion item
                Dim NextQuestionList As List(Of String) = (New JavaScriptSerializer()).Deserialize(Of List(Of String))(HttpContext.Current.Request.Params("FOMNQAnswers"))
                Dim NQItemList As List(Of String) = (New JavaScriptSerializer()).Deserialize(Of List(Of String))(HttpContext.Current.Request.Params("FOMNQItems"))
                If NextQuestionList.Count > 0 Then
                    Dim NQId As Integer = Integer.Parse(NextQuestionList(0))
                    '' make sure the NextQuestion belong to the starting question
                    If selectedFOMIdx = NQId Then
                        ToFOMNextQuestionXMLString(writer, NextQuestionList, NQItemList)
                    End If
                End If

                ''get innerNextQuestion and nextquestion list
                Dim innerNextQuestionList As List(Of String) = (New JavaScriptSerializer()).Deserialize(Of List(Of String))(HttpContext.Current.Request.Params("FOMInnerNQAnswers"))
                Dim innerNQItemList As List(Of String) = (New JavaScriptSerializer()).Deserialize(Of List(Of String))(HttpContext.Current.Request.Params("FOMInnerNQItems"))
                If innerNextQuestionList.Count > 0 Then
                    ToFOMNextQuestionXMLString(writer, innerNextQuestionList, innerNQItemList)
                End If
                writer.WriteEndElement()
                '' end fom
            End If
            ' end xpress loan
            writer.WriteEndElement()

            ' end document
            writer.WriteEndDocument()
            writer.Close()

            Return sb.ToString()
        End Function
        Public Sub ToFOMNextQuestionXMLString(ByVal writer As XmlWriter, ByVal NextQuestionList As List(Of String), ByVal NQItemList As List(Of String))
            Dim NQfieldCount As Integer = Integer.Parse(NextQuestionList(1))
            Dim NQName As String = NextQuestionList(2)
            Dim NQTextTemplate As String = NextQuestionList(3)
            Dim oFNQ As New List(Of CFOMQuestion)
            oFNQ = CFOMQuestion.DownloadFomNextQuestions(oConfig)
            Dim text_template As String = ""
            For Each oItem In oFNQ
                If oItem.Name = NQName Then
                    text_template = oItem.TextTemplate
                    Exit For
                End If
            Next
            Dim NQTypeList As New ArrayList()
            Dim NQValueList As New ArrayList()
            If NQfieldCount > 0 And NQItemList.Count > 0 Then
                For i = 0 To NQItemList.Count - 1
                    If (i + 1) Mod 2 = 0 Then
                        NQValueList.Add(NQItemList(i))
                    Else
                        NQTypeList.Add(NQItemList(i))
                    End If
                Next
            End If '' end nqfieldcount
            writer.WriteStartElement("FOM_ANSWERED_QUESTION")
            writer.WriteAttributeString("name", NQName)
            writer.WriteStartElement("text_template")
            writer.WriteString(text_template)
            writer.WriteEndElement()
            writer.WriteStartElement("FOM_ANSWERS")
            If NQfieldCount > 0 Then
                For i = 0 To NQfieldCount - 1
                    Dim nqValue = NQValueList(i)
                    Dim nqType = NQTypeList(i)
                    If nqType = "text" Then
                        nqType = "TEXTBOX"
                    End If
                    ''question text is not the same value
                    Dim nqText As String = getAnswerQuestionText(nqValue, oFNQ)
                    If String.IsNullOrEmpty(nqText) Then
                        nqText = nqValue
                    End If
                    writer.WriteStartElement("FOM_ANSWER")
                    writer.WriteAttributeString("field_type", nqType.ToUpper())
                    writer.WriteStartElement("value")
                    writer.WriteString(nqValue)
                    writer.WriteEndElement()
                    writer.WriteStartElement("text")
                    writer.WriteString(nqText)
                    writer.WriteEndElement()
                    writer.WriteEndElement()
                Next ''end for
            End If ''end NQfieldCount
            writer.WriteEndElement()

            writer.WriteEndElement() '' end FOM_Answer_Question
        End Sub
        Public Function getAnswerQuestionText(ByVal oValue As String, ByVal oCFOMNextQuestion As List(Of CFOMQuestion)) As String
            For Each oItem In oCFOMNextQuestion
                For Each oField As CFOMQuestionField In oItem.Fields
                    For Each oAnswer As CFOMQuestionAnswer In oField.Answers
                        If oValue.ToUpper = oAnswer.Value.ToUpper And Not String.IsNullOrEmpty(oAnswer.Text) Then
                            Return oAnswer.Text
                        End If
                    Next
                Next
            Next
            Return ""
        End Function

        Public Sub ToMinorXMLString(ByVal writer As XmlWriter)
            Dim strNodeName As String = "XA_LOAN/VISIBLE"
            ''start minor applicant
            writer.WriteStartElement("APPLICANT")
            writer.WriteAttributeString("first_name", oMinorAppPersonal.FirstName)
            writer.WriteAttributeString("middle_name", oMinorAppPersonal.MiddleName)
            writer.WriteAttributeString("last_name", oMinorAppPersonal.LastName)
            writer.WriteAttributeString("suffix", oMinorAppPersonal.Suffix)
            writer.WriteAttributeString("gender", oMinorAppPersonal.Gender)

            If Not String.IsNullOrEmpty(oMinorAppPersonal.DateOfBirth) Then
                writer.WriteAttributeString("dob", SafeDateXml(oMinorAppPersonal.DateOfBirth))
            End If
            writer.WriteAttributeString("mother_maiden_name", oMinorAppPersonal.MotherMaidenName)
            writer.WriteAttributeString("citizenship", CEnum.MapCitizenshipStatusToValue(oMinorAppPersonal.Citizenship))
            writer.WriteAttributeString("ssn", oMinorAppPersonal.SSN)
            writer.WriteAttributeString("member_number", oMinorAppPersonal.MemberNumber)
            If (Not String.IsNullOrEmpty(MinorRoleType)) Then
                writer.WriteAttributeString("role_type", MinorRoleType)
            Else
                _log.Info("Unable to get role type for minor account")
            End If

            ''minor contact infor
            writer.WriteAttributeString("email", oMinorAppContact.Email)
            If oMinorAppContact.HomePhoneCountry <> "" And Common.SafeString(oMinorAppContact.HomePhoneCountry).ToUpper <> "US" Then
                writer.WriteAttributeString("home_phone_country", oMinorAppContact.HomePhoneCountry.ToUpper)
                'writer.WriteAttributeString("is_home_phone_foreign", "Y")
                writer.WriteAttributeString("home_phone", oMinorAppContact.HomePhone)
            Else
                writer.WriteAttributeString("home_phone", FormatPhone(oMinorAppContact.HomePhone))
            End If


            If oMinorAppContact.CellPhoneCountry <> "" And Common.SafeString(oMinorAppContact.CellPhoneCountry).ToUpper <> "US" Then
                writer.WriteAttributeString("cell_phone_country", oMinorAppContact.CellPhoneCountry.ToUpper)
                'writer.WriteAttributeString("is_cell_phone_foreign", "Y")
                writer.WriteAttributeString("cell_phone", oMinorAppContact.CellPhone)
            Else
                writer.WriteAttributeString("cell_phone", FormatPhone(oMinorAppContact.CellPhone))
            End If


            If oMinorAppContact.WorkPhoneCountry.ToUpper <> "" And Common.SafeString(oMinorAppContact.WorkPhoneCountry).ToUpper <> "US" Then
                writer.WriteAttributeString("work_phone_country", oMinorAppContact.WorkPhoneCountry.ToUpper)
                'writer.WriteAttributeString("is_work_phone_foreign", "Y")
                writer.WriteAttributeString("work_phone", oMinorAppContact.WorkPhone)
            Else
                writer.WriteAttributeString("work_phone", FormatPhone(oMinorAppContact.WorkPhone))
            End If
            writer.WriteAttributeString("work_phone_extension", FormatPhoneExt(oMinorAppContact.WorkPhoneExt))

            If oMinorAppContact.PreferredContactMethod <> "" Then writer.WriteAttributeString("preferred_contact_method", oMinorAppContact.PreferredContactMethod)

            ''minor current address
            writer.WriteStartElement("CURRENT_ADDRESS")
            ''---mapping the occupancy status   
            If Not String.IsNullOrEmpty(oMinorAppAddress.OccupancyStatus) Then
                writer.WriteAttributeString("occupancy_status", CEnum.MapOccupancyTypeToValue(oMinorAppAddress.OccupancyStatus))
                writer.WriteAttributeString("occupancy_duration", oMinorAppAddress.OccupancyDuration)
            End If
            ''select either threeline or loose format
            If oMinorAppAddress.Country <> "" And oMinorAppAddress.Country <> "USA" Then
                ' three line address, foreign
                writer.WriteStartElement("THREE_LINE_ADDRESS")
                writer.WriteAttributeString("street_address_1", oMinorAppAddress.Address)
                writer.WriteAttributeString("city", oMinorAppAddress.City)
                writer.WriteAttributeString("country", oMinorAppAddress.Country)
                writer.WriteAttributeString("zip", oMinorAppAddress.Zip)
                writer.WriteEndElement() ' end three line address
            Else
                'domestic
                writer.WriteStartElement("LOOSE_ADDRESS")
                writer.WriteAttributeString("street_address_1", oMinorAppAddress.Address)
                writer.WriteAttributeString("street_address_2", "")
                writer.WriteAttributeString("city", oMinorAppAddress.City)
                writer.WriteAttributeString("state", oMinorAppAddress.State)
                writer.WriteAttributeString("zip", oMinorAppAddress.Zip)
                writer.WriteEndElement() 'end LOOSE_ADDRESS
            End If
            writer.WriteEndElement() '' end current address
            ''minor mailing address
            Dim mailingAddress As String = oMinorMailingAddress.Address
            If Not String.IsNullOrEmpty(mailingAddress) Then
                writer.WriteStartElement("MAILING_ADDRESS")
                writer.WriteAttributeString("street_address_1", oMinorMailingAddress.Address)
                writer.WriteAttributeString("street_address_2", "")
                writer.WriteAttributeString("city", oMinorMailingAddress.City)
                writer.WriteAttributeString("state", oMinorMailingAddress.State)
                writer.WriteAttributeString("zip", oMinorMailingAddress.Zip)
                writer.WriteAttributeString("is_current", "N")
                writer.WriteEndElement()
            Else
                writer.WriteStartElement("MAILING_ADDRESS")
                writer.WriteAttributeString("is_current", "Y")
                writer.WriteEndElement()
            End If
            writer.WriteStartElement("FINANCIAL_INFO")
            If Not String.IsNullOrEmpty(oMinorAppPersonal.EmployeeOfLender) And oMinorAppPersonal.EmployeeOfLender <> "NONE" Then
                writer.WriteAttributeString("employee_of_lender_type", oMinorAppPersonal.EmployeeOfLender)
            End If
            If oMinorAppPersonal.EmploymentSectionEnabled = True Then
                Dim employment_status = CEnum.MapEmploymentStatusToValue(oMinorAppPersonal.EmploymentStatus)
                If Not String.IsNullOrEmpty(employment_status) Then
                    writer.WriteAttributeString("employment_status", employment_status)
                    ' new employment logic
                    If oMinorAppPersonal.EmploymentStatus.Contains("MILITARY") Then
                        writer.WriteAttributeString("employer", CEnum.MapBranchOfServiceToValue(oMinorAppPersonal.ddlBranchOfService))
                    Else
                        writer.WriteAttributeString("employer", oMinorAppPersonal.txtEmployer)
                    End If
                    Dim employed_months As Integer
                    employed_months = SafeInteger(oMinorAppPersonal.txtEmployedDuration_year) * 12 + SafeInteger(oMinorAppPersonal.txtEmployedDuration_month)
                    Dim profession_months As Integer
                    profession_months = SafeInteger(oMinorAppPersonal.txtProfessionDuration_year) * 12 + SafeInteger(oMinorAppPersonal.txtProfessionDuration_month)
                    writer.WriteAttributeString("employed_months", employed_months.ToString())
                    If profession_months = 0 Then
                        writer.WriteAttributeString("profession_months", employed_months.ToString())
                    Else
                        writer.WriteAttributeString("profession_months", profession_months.ToString())
                    End If
                    writer.WriteAttributeString("occupation", oMinorAppPersonal.txtJobTitle)
                    writer.WriteAttributeString("employment_business_type", oMinorAppPersonal.txtBusinessType)
                    writer.WriteAttributeString("supervisor_name", oMinorAppPersonal.txtSupervisorName)
                    writer.WriteAttributeString("pay_grade", oMinorAppPersonal.ddlPayGrade)
                    writer.WriteAttributeString("monthly_income_base_salary", oMinorAppPersonal.txtGrossMonthlyIncome)
                    If Not String.IsNullOrEmpty(SafeDateXml(oMinorAppPersonal.txtEmploymentStartDate)) Then
                        writer.WriteAttributeString("employment_start_date", SafeDateXml(oMinorAppPersonal.txtEmploymentStartDate))
                    End If
                    If Not String.IsNullOrEmpty(oMinorAppPersonal.txtETS) Then
                        writer.WriteAttributeString("ets", SafeDateXml(oMinorAppPersonal.txtETS))
                    End If
                Else
                    writer.WriteAttributeString("employment_status", "OT")
                    ' end new employment logic
                End If
            End If   ''end hasMinor
            writer.WriteEndElement() ' END </FINANCIAL_INFO>
            ''minor identification
            writer.WriteStartElement("ID_CARD")
            writer.WriteAttributeString("card_type", oMinorAppIDCard.CardType)
            writer.WriteAttributeString("card_number", oMinorAppIDCard.CardNumber)
            writer.WriteAttributeString("state", oMinorAppIDCard.State)
            writer.WriteAttributeString("country", oMinorAppIDCard.Country)

            If Not String.IsNullOrEmpty(SafeDateXml(oMinorAppIDCard.DateIssued)) Then
                writer.WriteAttributeString("date_issued", SafeDateXml(oMinorAppIDCard.DateIssued))
            End If

            If Not String.IsNullOrEmpty(oMinorAppIDCard.ExpirationDate) Then
                writer.WriteAttributeString("exp_date", SafeDateXml(oMinorAppIDCard.ExpirationDate))
            End If
            writer.WriteEndElement() 'end ID card 

            '' minor Applicant Question Answers
            ApplicantQuestionAnswer("m_", writer)
            writer.WriteEndElement() 'end minor applicant 
        End Sub

        Protected Function GetAllowedProductQuestions() As List(Of String)
            Dim ProductQuestions As New List(Of String)
            Dim oNodes As XmlNodeList = oConfig._webConfigXML.SelectNodes("XA_LOAN/ACCOUNT_QUESTIONS/QUESTION")

            If oNodes.Count < 1 Then
                Return ProductQuestions
            End If

            For Each child As XmlNode In oNodes
                If child.Attributes("question_name") Is Nothing Then
                    _log.Error("question_name is not correct.", Nothing)
                End If
                If child.Attributes("question_name").InnerXml <> "" Then
                    ProductQuestions.Add(child.Attributes("question_name").InnerXml)
                End If
            Next

            Return ProductQuestions

        End Function
    End Class
    ''upload documents
    Public Class selectDocuments
        Public title As String
        Public base64data As String
    End Class
    Public Class XAQuestionAnswer
        Public q As String 'question
        Public a As String   'answer
    End Class


    Public Class PersonalInfo
        Public EmployeeOfLender As String
        Public MemberNumber As String
        Public FirstName As String
        Public MiddleName As String
        Public LastName As String
        Public Suffix As String
        Public Gender As String
        Public DateOfBirth As String
        Public MotherMaidenName As String
        Public Citizenship As String
        Public MaritalStatus As String
        Public SSN As String
        Public EmploymentStatus As String

        Public txtJobTitle As String
        Public ddlBranchOfService As String
        Public txtEmployer As String
        Public ddlPayGrade As String
        Public txtSupervisorName As String
        Public txtBusinessType As String
        Public txtEmploymentStartDate As String
        Public txtEmployedDuration_year As String
        Public txtEmployedDuration_month As String
        Public txtProfessionDuration_year As String
        Public txtProfessionDuration_month As String
        Public txtETS As String
        Public txtGrossMonthlyIncome As Double

        ''previous employment information
        Public hasPrevEmployment As String
        Public prev_EmploymentStatus As String
        Public prev_txtJobTitle As String
        Public prev_ddlBranchOfService As String
        Public prev_txtEmployer As String
        Public prev_ddlPayGrade As String
        Public prev_txtBusinessType As String
        Public prev_txtEmploymentStartDate As String
        Public prev_txtEmployedDuration_year As String
        Public prev_txtEmployedDuration_month As String
        Public prev_txtETS As String
        Public prev_GrossMonthlyIncome As Double
        Public EmploymentSectionEnabled As Boolean = False
        ''hasMinor
        Public HasMinor As String

    End Class

    Public Class ContactInfo
        Public Email As String
        Public HomePhone As String
        Public HomePhoneCountry As String
        Public CellPhone As String
        Public CellPhoneCountry As String
        Public WorkPhone As String
        Public WorkPhoneCountry As String
        Public WorkPhoneExt As String
        Public PreferredContactMethod As String
    End Class

    Public Class IdentificationInfo
        Public CardType As String
        Public CardNumber As String
        Public DateIssued As String
        Public ExpirationDate As String
        Public State As String
        Public Country As String
    End Class

    Public Class AddressInfo
        Public Address As String
        Public Zip As String
        Public City As String
        Public State As String
        Public Country As String
        Public OccupancyDuration As Integer
        Public OccupancyStatus As String
    End Class

    Public Class LoanInfo
        Public account_position As String '1=primary, 2= secondary
        Public referral_source As String = ""
    End Class

    Public Class AccountInfo

        Private ReadOnly _log As log4net.ILog = log4net.LogManager.GetLogger(Me.GetType())

        Private oConfig As CWebsiteConfig
        Public Sub New(ByVal poConfig As CWebsiteConfig)
            Me.oConfig = poConfig
        End Sub

        Public Amount As Double
        Public Product As String    ' saving product code

        Private Class AccountTypeInfo
            Public AccountType As String
            Public AccountName As String
            Public AccountMinDeposit As String
            Public AccountIsRequired As String
        End Class

        Public ProductServices As List(Of String)

        Public CustomQuestions As List(Of CProductCustomQuestion)
        '' update toxmlString function by adding coSSN parameter to the function
        Public Function ToXmlString(ByVal listProducts As List(Of CProduct), Optional ByVal pbIsApproved As Boolean = False, Optional ByVal psFundType As String = "", Optional ByVal psCoSSN As String = "") As String
            ' This function will match the selected account product with the one in the XML config file.
            ' It will then fill in the initial deposit amount and construct the XML
            Dim sb As New StringBuilder
            Dim settings As New XmlWriterSettings()
            settings.Indent = True
            settings.OmitXmlDeclaration = True
            Dim writer As XmlWriter = XmlTextWriter.Create(sb, settings)

            Dim valueStr As String = ""
            Dim AccountTypesList As New List(Of AccountTypeInfo)
            Dim matchingProduct As CProduct = Nothing
            Dim hasMatchingNode As Boolean
            hasMatchingNode = False

            Dim tempAccountInfo As AccountTypeInfo
            For Each prod As CProduct In listProducts
                ' Grab the names of the accounts
                tempAccountInfo = New AccountTypeInfo
                tempAccountInfo.AccountName = prod.AccountName
                tempAccountInfo.AccountType = prod.AccountType
                tempAccountInfo.AccountMinDeposit = prod.MinimumDeposit.ToString()
                tempAccountInfo.AccountIsRequired = If(prod.IsRequired, "Y", "N")
                AccountTypesList.Add(tempAccountInfo)

                If (prod.ProductCode = Product) Then
                    'prod.Services = prod.Services.Where(Function(x) Me.ProductServices.Contains(x.ServiceCode)).ToList()
                    'matchingProduct = prod
                    matchingProduct = prod.Clone() 'do a clone so we don't modify the original cached copy
                    matchingProduct.Services = matchingProduct.Services.Where(Function(x) Me.ProductServices.Contains(x.ServiceCode)).ToList()
                    hasMatchingNode = True
                End If
            Next

            If (Not hasMatchingNode) Then
                Return ""
            End If

            ' Write out the XML 
            writer.WriteStartElement("ACCOUNT_TYPE")
            writer.WriteAttributeString("account_name", matchingProduct.AccountName)
            writer.WriteAttributeString("product_code", matchingProduct.ProductCode)
            writer.WriteAttributeString("account_type", matchingProduct.AccountType)
            writer.WriteAttributeString("amount_deposit", Amount)
            If psFundType <> "" And pbIsApproved Then
                If psFundType = "CASH" Then
                    writer.WriteAttributeString("clf_funding_source_id", "0")
                ElseIf psFundType = "MAIL" Then
                    writer.WriteAttributeString("clf_funding_source_id", "1")
                ElseIf psFundType = "NOT_FUNDING" Then
                    writer.WriteAttributeString("clf_funding_source_id", "2")
                ElseIf psFundType = "PAYPAL" Then
                    writer.WriteAttributeString("clf_funding_source_id", "3")
                Else
                    writer.WriteAttributeString("clf_funding_source_id", "10")
                End If
            End If

            'writer.WriteAttributeString("IS_REQUIRED", matchingAccountNode.Attributes("is_required").InnerXml)
            Dim rate As String
            Dim apy As String
            Dim term As String

            rate = matchingProduct.Rate
            If String.IsNullOrEmpty(rate) Then
                rate = "-999999"
            ElseIf rate.Length > 7 Then
                rate = "-999999"
            End If
            writer.WriteAttributeString("rate", rate)

            apy = matchingProduct.Apy
            If String.IsNullOrEmpty(apy) Then
                apy = "-999999"
            ElseIf apy.Length > 7 Then
                apy = "-999999"
            ElseIf apy <> "-999999" Then
                apy = FormatNumber(apy, 2)
            End If

            If String.IsNullOrEmpty(rate) Then
                apy = "-999999"
            End If
            writer.WriteAttributeString("apy", apy)

            term = matchingProduct.Term
            If String.IsNullOrEmpty(term) Then
                term = "-16959"
            End If
            writer.WriteAttributeString("term", term)
            'begin add ATM ' auto checked for cheking account, requested by baxter and Targget, 
            'TODO:  ATM service code need to come from LPQ and user selectable
            '' write selected SERVICES node if available from LPQ as TODO required
            If matchingProduct.Services IsNot Nothing AndAlso matchingProduct.Services.Count > 0 Then
                writer.WriteStartElement("SERVICES")
                For Each serv In matchingProduct.Services
                    writer.WriteStartElement("SERVICE")
                    writer.WriteAttributeString("description", serv.Description)
                    writer.WriteAttributeString("service_code", serv.ServiceCode)
                    writer.WriteAttributeString("service_type", serv.ServiceType)
                    writer.WriteEndElement()
                Next
                writer.WriteEndElement()
            End If


            '' if has Joint Applicant and product level ownership-> add acount_type_relations in account type
            Dim hasOnwershipLevel As String = Common.hasOwnerShipLevel(oConfig)
            If psCoSSN <> "" And pbIsApproved And hasOnwershipLevel = "Y" Then
                writer.WriteStartElement("ACCOUNT_TYPE_RELATIONS")
                writer.WriteStartElement("RELATION")
                writer.WriteAttributeString("ssn", psCoSSN)
                'If hasOnwershipLevel = "Y" Then
                writer.WriteAttributeString("benefactor_type", "J")
                'End If
                writer.WriteEndElement()  'end  RELATION
                writer.WriteEndElement()  'ACCOUNT_TYPE_RELATIONS
            End If

            ' begin add product custom question '
            If Me.CustomQuestions IsNot Nothing AndAlso Me.CustomQuestions.Count > 0 Then
                If pbIsApproved Then
                    writer.WriteStartElement("PRODUCT_CUSTOM_QUESTIONS_APPROVED")
                Else
                    writer.WriteStartElement("PRODUCT_CUSTOM_QUESTIONS_INTERESTED")
                End If

                For Each question As CProductCustomQuestion In Me.CustomQuestions
                    writer.WriteStartElement("CUSTOM_QUESTION")
                    writer.WriteAttributeString("question_name", question.QuestionName)
                    writer.WriteAttributeString("question_type", question.AnswerType)
                    If String.Equals(question.AnswerType, "CHECKBOX") OrElse String.Equals(question.AnswerType, "DROPDOWN") Then
                        For Each ans As CProductRestrictedAnswer In question.RestrictedAnswers
                            If ans.isSelected Then
                                writer.WriteStartElement("CUSTOM_QUESTION_ANSWER")
                                writer.WriteAttributeString("answer_text", ans.Text)
                                writer.WriteAttributeString("answer_value", ans.Value)
                                writer.WriteEndElement() 'end CUSTOM_QUESTION_ANSWER
                                '' check box can have one or more answer -> need to check all the answers
                                '' Exit For  ->correct only for dropdown (not for checkbox)
                            End If
                        Next
                    Else
                        writer.WriteStartElement("CUSTOM_QUESTION_ANSWER")
                        writer.WriteAttributeString("answer_text", question.productAnswerText)
                        writer.WriteAttributeString("answer_value", question.productAnswerText)
                        writer.WriteEndElement() 'end CUSTOM_QUESTION_ANSWER
                    End If
                    writer.WriteEndElement()  'end CUSTOM_QUESTION
                Next
                writer.WriteEndElement() 'end PRODUCT_CUSTOM_QUESTIONS_APPROVED
            End If
            ' end add product custom question '

            'writer.WriteStartAttribute("create_date")
            'writer.WriteValue(DateTime.Now)
            'writer.WriteEndAttribute()
            '' get the joint CoApp
            writer.WriteEndElement() 'end ACCOUNT_TYPE
            writer.Flush()
            writer.Close()

            Console.WriteLine(sb.ToString())

            Dim xmlStr As String
            xmlStr = sb.ToString()

            Console.WriteLine(xmlStr)

            '_log.Info("AccountInfoXML: " + xmlStr)

            Return xmlStr



        End Function
    End Class

    <Serializable()> _
    Public Class WalletQuestionRendering
        'Inherits CBasePage

        Public _CurrentWebsiteConfig As CWebsiteConfig

        Dim walletXMLDoc As XmlDocument
        Public questionsList As List(Of questionDataClass)
        Dim isPopulated As Boolean
        'Dim authenticationType As String = "RSA"
        'equifax
        Dim authenticationType As String

        <Serializable()> _
        Public Class questionDataClass
            Public questionText As String
            Public questionID As String
            Public choicesList As List(Of choiceClass)
        End Class
        <Serializable()> _
        Public Class choiceClass
            Public choiceText As String
            Public choiceID As String
        End Class

        Sub New(ByVal walletXML As String, ByVal poConfig As CWebsiteConfig)
            ' Get the wallet questions XML
            walletXMLDoc = New XmlDocument()
            walletXMLDoc.LoadXml(walletXML)
            questionsList = New List(Of questionDataClass)
            isPopulated = False
            ' authenticationType = "RSA"
            'equifax
            _CurrentWebsiteConfig = poConfig
            authenticationType = _CurrentWebsiteConfig.AuthenticationType
        End Sub

        Public ReadOnly Property NumQuestions() As Integer
            Get
                Return questionsList.Count
            End Get
        End Property
        ' Read the XML data and strip out the relevant pieces of data
        Public Sub populateQuestions(ByRef transaction_ID As String, ByRef question_set_ID As String, ByRef request_app_ID As String, _
                                     ByRef request_client_ID As String, ByRef request_sequence_ID As String, ByRef reference_number As String)
            ' We need to figure out which type of authentication to use
            Select Case authenticationType.ToUpper()
                Case "RSA"
                    'easier to have all the parameters)
                    populateQuestionsRSA(transaction_ID, question_set_ID, request_app_ID, request_client_ID, request_sequence_ID)
                Case "PID"
                    populateQuestionsPID(transaction_ID, question_set_ID, request_app_ID, request_client_ID, request_sequence_ID)
                Case "FIS"
                    populateQuestionsFIS(transaction_ID, question_set_ID, request_app_ID, request_client_ID, request_sequence_ID)
                Case "DID"
                    populateQuestionsDID(transaction_ID, question_set_ID, request_app_ID, request_client_ID, request_sequence_ID)
                    'equifax
                Case "EID"
                    populateQuestionsEquifax(transaction_ID, question_set_ID, request_app_ID, request_client_ID, request_sequence_ID)
                Case "TID"
                    populateQuestionsTID(transaction_ID, question_set_ID, request_app_ID, request_client_ID, request_sequence_ID, reference_number)

            End Select
        End Sub

        Public Sub setAuthenticationType(ByVal authType As String)
            authenticationType = authType.ToUpper()
        End Sub

        'Equifax
        Protected Sub populateQuestionsEquifax(ByRef transaction_ID As String, ByRef question_set_ID As String, ByRef request_app_ID As String, _
                                     ByRef request_client_ID As String, ByRef request_sequence_ID As String)
            'this is used for the Equifax authentication method
            'also need to get the transaction_id and 
            Dim request_node As XmlNode = walletXMLDoc.SelectSingleNode("/OUTPUT/RESPONSE")
            If request_node IsNot Nothing Then
                transaction_ID = request_node.Attributes("transaction_id").InnerXml
            End If
            Dim child_nodes As XmlNodeList = walletXMLDoc.GetElementsByTagName("QUESTION")
            questionsList.Clear()

            If (child_nodes.Count < 1) Then
                'raised an exception and log the error
            End If
            'number of question
            Dim numQuestions As Integer = child_nodes.Count
            Dim valuesStr As String = ""
            Dim tempQuestion As questionDataClass
            Dim tempChoice As choiceClass
            For Each child As XmlNode In child_nodes
                'we need to grab each question along with the choices, and also we need to save 
                'the question_number and the choice_number
                tempQuestion = New questionDataClass
                tempQuestion.questionText = child.Attributes("question_text").InnerXml
                tempQuestion.questionID = child.Attributes("question_number").InnerXml
                tempQuestion.choicesList = New List(Of choiceClass)
                'for each question grap all the choices
                Dim choicesXML As New XmlDocument
                choicesXML.LoadXml(child.OuterXml)
                Dim choiceNodes As XmlNodeList = choicesXML.GetElementsByTagName("CHOICE")
                ' Loop through all the elements with the "CHOICE" tag in them and get the choice_id and text
                For Each choice As XmlNode In choiceNodes
                    tempChoice = New choiceClass
                    tempChoice.choiceText = choice.Attributes("choice_text").InnerXml
                    tempChoice.choiceID = choice.Attributes("choice_number").InnerXml
                    ' Add the choice to the list of choices
                    tempQuestion.choicesList.Add(tempChoice)
                Next
                ' Now that the question and choices are grouped together, add them to the main questions list
                questionsList.Add(tempQuestion)
            Next
            ' So we now have all the questions and their choices.
            isPopulated = True
        End Sub

        Protected Sub populateQuestionsRSA(ByRef transaction_ID As String, ByRef question_set_ID As String, ByRef request_app_ID As String, _
                                     ByRef request_client_ID As String, ByRef request_sequence_ID As String)
            ' This is used for the RSA authentication method.  
            ' Also need to get the transaction_id and the question_set_id
            Dim request_node As XmlNode = walletXMLDoc.SelectSingleNode("/OUTPUT/RESPONSE")
            transaction_ID = request_node.Attributes("transaction_id").InnerXml
            question_set_ID = request_node.Attributes("question_set_id").InnerXml

            Dim child_nodes As XmlNodeList = walletXMLDoc.GetElementsByTagName("QUESTION")
            questionsList.Clear()

            If child_nodes.Count < 1 Then
                ' Raise an exception and log the error
            End If

            ' Now we know how many questions there are
            Dim numQuestions As Integer = child_nodes.Count
            Dim valueStr As String = ""

            Dim tempQuestion As questionDataClass
            Dim tempChoice As choiceClass
            For Each child As XmlNode In child_nodes
                ' What we need to do is grab the text for each question along with the choices
                ' We also need to save the question_id for each question and the choice_id for each choice
                tempQuestion = New questionDataClass
                tempQuestion.questionText = child.Attributes("text").InnerXml
                tempQuestion.questionID = child.Attributes("question_id").InnerXml
                tempQuestion.choicesList = New List(Of choiceClass)

                ' Now we have to grab all the choices
                Dim choicesXML As New XmlDocument
                choicesXML.LoadXml(child.OuterXml)
                Dim choiceNodes As XmlNodeList = choicesXML.GetElementsByTagName("CHOICE")
                ' Loop through all the elements with the "CHOICE" tag in them and get the choice_id and text
                For Each choice As XmlNode In choiceNodes
                    tempChoice = New choiceClass
                    tempChoice.choiceText = choice.Attributes("text").InnerXml
                    tempChoice.choiceID = choice.Attributes("choice_id").InnerXml
                    ' Add the choice to the list of choices
                    tempQuestion.choicesList.Add(tempChoice)
                Next
                ' Now that the question and choices are grouped together, add them to the main questions list
                questionsList.Add(tempQuestion)
            Next

            ' So we now have all the questions and their choices.
            isPopulated = True
        End Sub

        Protected Sub populateQuestionsPID(ByRef transaction_ID As String, ByRef question_set_ID As String, ByRef request_app_ID As String, _
                                     ByRef request_client_ID As String, ByRef request_sequence_ID As String)
            ' This is used for the PID authentication method.  
            ' Also need to get the transaction_id and the question_set_id
            Dim request_node As XmlNode = walletXMLDoc.SelectSingleNode("/OUTPUT/RESPONSE")
            transaction_ID = request_node.Attributes("session_id").InnerXml

            ' Get all the nodes that have the QUESTION element
            Dim child_nodes As XmlNodeList = walletXMLDoc.GetElementsByTagName("QUESTION")
            questionsList.Clear()

            If child_nodes.Count < 1 Then
                ' Raise an exception and log the error
            End If

            ' Now we know how many questions there are
            Dim numQuestions As Integer = child_nodes.Count
            Dim valueStr As String = ""

            Dim tempQuestion As questionDataClass
            Dim tempChoice As choiceClass
            Dim questionIndex As Integer = 1
            Dim answerIndex As Integer = 1
            For Each child As XmlNode In child_nodes
                ' What we need to do is grab the text for each question along with the "answers"
                ' We also need to save the question_id for each question and the choice_id for each choice
                tempQuestion = New questionDataClass
                tempQuestion.questionText = child.Attributes("question_text").InnerXml
                ' PID doesn't use question ID's, so we'll set this to zero
                tempQuestion.questionID = questionIndex
                tempQuestion.choicesList = New List(Of choiceClass)

                questionIndex = questionIndex + 1

                ' Now we have to grab all the choices
                Dim choicesXML As New XmlDocument
                choicesXML.LoadXml(child.OuterXml)
                Dim choiceNodes As XmlNodeList = choicesXML.GetElementsByTagName("ANSWER")
                ' Loop through all the elements with the "CHOICE" tag in them and get the choice_id and text
                For Each choice As XmlNode In choiceNodes
                    tempChoice = New choiceClass
                    tempChoice.choiceText = choice.Attributes("answer_text").InnerXml
                    tempChoice.choiceID = answerIndex
                    ' Add the choice to the list of choices
                    tempQuestion.choicesList.Add(tempChoice)

                    answerIndex = answerIndex + 1
                Next
                ' Now that the question and choices are grouped together, add them to the main questions list
                questionsList.Add(tempQuestion)
                ' Reset the answerIndex for the next set of answers
                answerIndex = 1
            Next

            ' So we now have all the questions and their choices.
            isPopulated = True
        End Sub

        Protected Sub populateQuestionsFIS(ByRef transaction_ID As String, ByRef question_set_ID As String, ByRef request_app_ID As String, _
                                     ByRef request_client_ID As String, ByRef request_sequence_ID As String)
            ' This is used for the PID authentication method.  
            ' Also need to get the transaction_id and the question_set_id
            Dim request_node As XmlNode = walletXMLDoc.SelectSingleNode("/OUTPUT/RESPONSE")
            question_set_ID = request_node.Attributes("quiz_id").InnerXml
            transaction_ID = request_node.Attributes("transaction_id").InnerXml

            ' Get all the nodes that have the QUESTION element
            Dim child_nodes As XmlNodeList = walletXMLDoc.GetElementsByTagName("QUESTION")
            questionsList.Clear()

            If child_nodes.Count < 1 Then
                ' Raise an exception and log the error
            End If

            ' Now we know how many questions there are
            Dim numQuestions As Integer = child_nodes.Count
            Dim valueStr As String = ""

            Dim tempQuestion As questionDataClass
            Dim tempChoice As choiceClass
            Dim questionIndex As Integer = 1
            Dim answerIndex As Integer = 1
            For Each child As XmlNode In child_nodes
                ' What we need to do is grab the text for each question along with the "answers"
                ' We also need to save the question_id for each question and the choice_id for each choice
                tempQuestion = New questionDataClass
                tempQuestion.questionID = child.Attributes("id").InnerXml
                tempQuestion.questionText = child.Attributes("question_text").InnerXml
                tempQuestion.choicesList = New List(Of choiceClass)

                questionIndex = questionIndex + 1

                ' Now we have to grab all the choices
                Dim choicesXML As New XmlDocument
                choicesXML.LoadXml(child.OuterXml)
                Dim choiceNodes As XmlNodeList = choicesXML.GetElementsByTagName("ANSWER")
                ' Loop through all the elements with the "CHOICE" tag in them and get the choice_id and text
                For Each choice As XmlNode In choiceNodes
                    tempChoice = New choiceClass
                    tempChoice.choiceID = choice.Attributes("id").InnerXml
                    tempChoice.choiceText = choice.Attributes("text").InnerXml
                    ' Add the choice to the list of choices
                    tempQuestion.choicesList.Add(tempChoice)

                    answerIndex = answerIndex + 1
                Next
                ' Now that the question and choices are grouped together, add them to the main questions list
                questionsList.Add(tempQuestion)
                ' Reset the answerIndex for the next set of answers
                answerIndex = 1
            Next

            ' So we now have all the questions and their choices.
            isPopulated = True
        End Sub

        Protected Sub populateQuestionsDID(ByRef transaction_ID As String, ByRef question_set_ID As String, ByRef request_app_ID As String, _
                                     ByRef request_client_ID As String, ByRef request_sequence_ID As String)
            ' This is used for the PID authentication method.  
            ' Also need to get the transaction_id and the question_set_id
            Dim request_node As XmlNode = walletXMLDoc.SelectSingleNode("/OUTPUT/RESPONSE")
            transaction_ID = request_node.Attributes("session_id").InnerXml
            request_app_ID = request_node.Attributes("request_app_id").InnerXml
            request_client_ID = request_node.Attributes("request_client_id").InnerXml
            request_sequence_ID = request_node.Attributes("request_sequence_id").InnerXml

            ' Get all the nodes that have the QUESTION element
            Dim child_nodes As XmlNodeList = walletXMLDoc.GetElementsByTagName("QUESTION")
            questionsList.Clear()

            If child_nodes.Count < 1 Then
                ' Raise an exception and log the error
            End If

            ' Now we know how many questions there are
            Dim numQuestions As Integer = child_nodes.Count
            Dim valueStr As String = ""

            Dim tempQuestion As questionDataClass
            Dim tempChoice As choiceClass
            Dim questionIndex As Integer = 1
            Dim answerIndex As Integer = 1
            For Each child As XmlNode In child_nodes
                ' What we need to do is grab the text for each question along with the "answers"
                ' We also need to save the question_id for each question and the choice_id for each choice
                tempQuestion = New questionDataClass
                tempQuestion.questionID = child.Attributes("question_number").InnerXml
                tempQuestion.questionText = child.Attributes("question_text").InnerXml
                tempQuestion.choicesList = New List(Of choiceClass)

                questionIndex = questionIndex + 1

                ' Now we have to grab all the choices
                Dim choicesXML As New XmlDocument
                choicesXML.LoadXml(child.OuterXml)
                Dim choiceNodes As XmlNodeList = choicesXML.GetElementsByTagName("ANSWER")
                ' Loop through all the elements with the "CHOICE" tag in them and get the choice_id and text
                For Each choice As XmlNode In choiceNodes
                    tempChoice = New choiceClass
                    tempChoice.choiceID = answerIndex
                    tempChoice.choiceText = choice.Attributes("answer_text").InnerXml
                    ' Add the choice to the list of choices
                    tempQuestion.choicesList.Add(tempChoice)

                    answerIndex = answerIndex + 1
                Next
                ' Now that the question and choices are grouped together, add them to the main questions list
                questionsList.Add(tempQuestion)
                ' Reset the answerIndex for the next set of answers
                answerIndex = 1
            Next

            ' So we now have all the questions and their choices.
            isPopulated = True
        End Sub
        'TransUnion IDMA
        Protected Sub populateQuestionsTID(ByRef transaction_ID As String, ByRef question_set_ID As String, ByRef request_app_ID As String, _
                                     ByRef request_client_ID As String, ByRef request_sequence_ID As String, ByRef reference_number As String)
            'this is used for the Equifax authentication method
            'also need to get the transaction_id and 
            Dim request_node As XmlNode = walletXMLDoc.SelectSingleNode("/OUTPUT/RESPONSE")
            If request_node IsNot Nothing Then
                reference_number = request_node.Attributes("reference_number").InnerXml
            End If
            Dim child_nodes As XmlNodeList = walletXMLDoc.GetElementsByTagName("QUESTION")
            questionsList.Clear()

            If (child_nodes.Count < 1) Then
                'raised an exception and log the error
            End If
            'number of question
            Dim numQuestions As Integer = child_nodes.Count
            Dim valuesStr As String = ""
            Dim tempQuestion As questionDataClass
            Dim tempChoice As choiceClass
            For Each child As XmlNode In child_nodes
                'we need to grab each question along with the choices, and also we need to save 
                'the display_name and the name
                tempQuestion = New questionDataClass
                tempQuestion.questionText = child.Attributes("display_name").InnerXml
                tempQuestion.questionID = child.Attributes("name").InnerXml
                tempQuestion.choicesList = New List(Of choiceClass)
                'for each question grap all the choices
                Dim choicesXML As New XmlDocument
                choicesXML.LoadXml(child.OuterXml)
                Dim choiceNodes As XmlNodeList = choicesXML.GetElementsByTagName("CHOICE")
                ' Loop through all the elements with the "CHOICE" tag in them and get the choice_id and text
                For Each choice As XmlNode In choiceNodes
                    tempChoice = New choiceClass
                    tempChoice.choiceText = choice.Attributes("display").InnerXml
                    tempChoice.choiceID = choice.Attributes("key").InnerXml
                    ' Add the choice to the list of choices
                    tempQuestion.choicesList.Add(tempChoice)
                Next
                ' Now that the question and choices are grouped together, add them to the main questions list
                questionsList.Add(tempQuestion)
            Next
            ' So we now have all the questions and their choices.
            isPopulated = True
        End Sub

        Public Function RenderWalletQuestions(ByVal psFirstName As String, ByRef transaction_ID As String, ByRef question_set_ID As String, ByRef request_app_ID As String, _
                                     ByRef request_client_ID As String, ByRef request_sequence_ID As String, ByRef reference_number As String) As String
            ' If the questions have not been populated, then go and populate them first
            If Not isPopulated Then
                populateQuestions(transaction_ID, question_set_ID, request_app_ID, request_client_ID, request_sequence_ID, reference_number)
            End If

            ' The questions and choices are eseentially going to be rendered as radio buttons
            Dim I As Integer
            Dim numQuestions As Integer = questionsList.Count
            'Dim returnStr As String = ""
            'returnStr += psFirstName & ", please answer the following questions to help us verify your identity:<br/><br/>"
            Dim sb As New StringBuilder
            sb.AppendLine(HeaderUtils.RenderPageTitle(0, psFirstName & ", please answer the following questions to help us verify your identity:", True))

            For I = 0 To numQuestions - 1
                'returnStr += "<fieldset data-role='controlgroup'>"

                ' Render the question text
                sb.AppendLine("<div>")
                sb.Append("<b style='margin-right:2px;'>" & I + 1 & ".</b>")
                sb.AppendLine(questionsList(I).questionText)
                sb.AppendLine("<span style='display: inline' class='require-span'>*</span>")
                sb.AppendLine("</div>")
                ' Render the choices
                Dim J As Integer
                Dim numChoices As Integer = questionsList(I).choicesList.Count
                For J = 0 To numChoices - 1
                    sb.AppendLine("<input type='radio' name='walletquestion-radio-" + (I + 1).ToString() + "' class='walletquestion-radio-" + (I + 1).ToString())
                    sb.Append("' id='rq" + (I + 1).ToString() + "_" + questionsList(I).choicesList(J).choiceID + "' />")
                    sb.Append("<label for='rq" + (I + 1).ToString() + "_" + questionsList(I).choicesList(J).choiceID + "'>")
                    sb.Append(questionsList(I).choicesList(J).choiceText)
                    sb.Append("</label>")
                Next
            Next

            Return sb.ToString()
        End Function

    End Class

End Class


<Serializable()> _
Public Class CIncompleteAppInfo
    Public sIncAppLoanRequestXML As String
    Public sIncAppLoanID As String
    Public sIncAppGetLoanResponse As String
End Class


