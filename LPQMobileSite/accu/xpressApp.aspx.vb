﻿Imports System.Net
Imports System.Xml
Imports System.IO
Imports LPQMobile.BO
Imports LPQMobile.Utils
Imports System.Web.Script.Serialization
Imports System.Web.UI.Control
Imports Newtonsoft.Json
Imports System.Linq

''' <summary>
''' This is a custom app that for Medishare to pass over applicant info via SSO.  
''' Only disclosure and IDA sections are shown, all other sections are hidden. 
''' This mean we dont need to modify this folder that often.
''' </summary>
Partial Public Class CXpressApp
    Inherits CBasePage

    'Shared _Lender_ID As String	 ''?what is the use: TODO
    'Shared _Org_ID As String
    Private _SubmitURL As String
    Protected _LoanPurposeDropdown As String
    Protected _LineOfCreditPurposeDropdown As String
    Protected _IDCardTypeDropDown As String
    'Protected _RenderedProductRadio As String
    Protected _RenderedProductDepositInputs As String
    'Protected _RenderedProductCollapsibles As String
    Protected _RenderedWalletQuestions As String
    Protected _FirstName As String
    Protected _MinDepositAmount As Integer
    Protected _HiddenMinAmounts As String
    Protected _HiddenNumProducts As String
    Protected _HiddenProducts As String
    Protected _HiddenRequiredProducts As String
    Protected _ProductRendering As CProductRendering

    Protected _ProductPackageJsonString As String = "[]"
    Protected _FundingSourcePackageJsonString As String = "[]"
	'Protected _OccupancyStatus As String = ""
    'Protected _previous_address_thresshold As String = ""
    Protected _address_key As String = ""
	'Protected _MaxFunding As String = ""
    Protected _CheckMailAddress As String = ""
    'Protected _PageFOMHeader As String = ""
    'Protected _PageFOMFooter As String = ""
    'Protected _HasNavLeft As String = ""
    ''******
    Protected _XALogo As String = ""
    'Protected _XAOtherLogo As String = ""
    Protected _RenderInitialDepositProducts As String = ""
    'Protected _RenderSelectProducts As String = ""
    Protected _RenderProductQuestions As String = ""
    Protected _HasEnableSecondaryUpLoadDocs As String = ""
    Protected _RenderApplicantQuestion As String = ""
    Protected _RenderCoApplicantQuestion As String = ""
    ''minor applicant
    Protected _HasMinorApplicant As String = ""
    Protected _RenderMinorApplicantQuestion As String = ""
    Protected _visibleMinorApp As String = ""
	'Protected _visibleMinorId As String = ""
	'Protected _HasMinorId As String = ""
	'Protected _visibleMinorEmployment As String = ""
	'Protected _HasMinorEmployment As String = ""
	'Protected _bgTheme As String
    Protected _Availability As String = "" 'PRIMARY/SECONDARY
    Protected _minorAccountList As String = "[]"
    Protected _minoProductList As String = "[]"
    Protected _filterProductCodes As String = "[]"
    ''credit pull message
    Protected _CreditPullMessage As String = ""
    Protected _hasCreditPull As String = ""
    Private _accountType As String = ""
    Protected _RenderSpecialAccountType As String = ""
	'Protected _ShowEmployment As String = ""
    'Protected _ShowMinorEmployment As String
    Protected _enableJoint As Boolean = True
	'Protected _maritalStatus As String = ""
    ''product code lists
    Protected _productCodes As String = ""
#Region "SSO"
    'Ex:DI, 
    'Protected ReadOnly Property PlatformSource() As String
    '	Get
    '		Return Common.SafeString(Request.Params("platform_source"))
    '	End Get
    'End Property



#End Region
    Protected _visibleCustomQuestion As String
    Protected _xaStopInvalidRoutingNumber As String = ""
	'Protected _xaVisibleBranchName As String
    Protected _isLive As String = ""
#Region "Field"

    Protected ReadOnly Property MediShareNumber() As String
        Get
            Return Common.SafeString(HttpUtility.UrlDecode(Request.Params("MediShareNumber")))
        End Get
    End Property
    Protected ReadOnly Property applicationQuestionName() As String
        Get
            Return Common.SafeString(HttpUtility.UrlDecode(Request.Params("applicationQuestionName")))
        End Get
    End Property
    Protected ReadOnly Property applicationQuestionAnswer() As String
        Get
            Return Common.SafeString(HttpUtility.UrlDecode(Request.Params("applicationQuestionAnswer")))
        End Get
    End Property
    Protected ReadOnly Property houseHoldNumber() As String
        Get
            Return Common.SafeString(HttpUtility.UrlDecode(Request.Params("houseHoldNumber")))
        End Get
    End Property
    ''--------address--------

    Protected ReadOnly Property Address() As String
        Get
            Return Common.SafeString(HttpUtility.UrlDecode(Request.Params("Address")))
        End Get
    End Property
    Protected ReadOnly Property Zip() As String
        Get
            Return Common.SafeString(HttpUtility.UrlDecode(Request.Params("Zip")))
        End Get
    End Property
    Protected ReadOnly Property City() As String
        Get
            Return Common.SafeString(HttpUtility.UrlDecode(Request.Params("City")))
        End Get
    End Property
    Protected ReadOnly Property State() As String
        Get
            Return Common.SafeString(HttpUtility.UrlDecode(Request.Params("State")))
        End Get
    End Property
    Protected ReadOnly Property occupancyStatus() As String
        Get
            Return Common.SafeString(HttpUtility.UrlDecode(Request.Params("occupancyStatus")))
        End Get
    End Property
    Protected ReadOnly Property occupancyDurationMonth() As String
        Get
            Return Common.SafeString(HttpUtility.UrlDecode(Request.Params("occupancyDurationMonth")))
        End Get
    End Property
    Protected ReadOnly Property occupancyDurationYear() As String
        Get
            Return Common.SafeString(HttpUtility.UrlDecode(Request.Params("occupancyDurationYear")))
        End Get
    End Property
    ''----address for coApp
    Protected ReadOnly Property co_Address() As String
        Get
            Return Common.SafeString(HttpUtility.UrlDecode(Request.Params("co_Address")))
        End Get
    End Property
    Protected ReadOnly Property co_Zip() As String
        Get
            Return Common.SafeString(HttpUtility.UrlDecode(Request.Params("co_Zip")))
        End Get
    End Property
    Protected ReadOnly Property co_City() As String
        Get
            Return Common.SafeString(HttpUtility.UrlDecode(Request.Params("co_City")))
        End Get
    End Property
    Protected ReadOnly Property co_State() As String
        Get
            Return Common.SafeString(HttpUtility.UrlDecode(Request.Params("co_State")))
        End Get
    End Property
    Protected ReadOnly Property co_occupancyStatus() As String
        Get
            Return Common.SafeString(HttpUtility.UrlDecode(Request.Params("co_occupancyStatus")))
        End Get
    End Property
    Protected ReadOnly Property co_occupancyDurationMonth() As String
        Get
            Return Common.SafeString(HttpUtility.UrlDecode(Request.Params("co_occupancyDurationMonth")))
        End Get
    End Property
    Protected ReadOnly Property co_occupancyDurationYear() As String
        Get
            Return Common.SafeString(HttpUtility.UrlDecode(Request.Params("co_occupancyDurationYear")))
        End Get
    End Property
    ''-------end address-----
    ''--------Contact Info -------
    Protected ReadOnly Property HomePhone() As String
        Get
            Return Common.SafeString(HttpUtility.UrlDecode(Request.Params("HomePhone")))
        End Get
    End Property
    Protected ReadOnly Property WorkPhone() As String
        Get
            Return Common.SafeString(HttpUtility.UrlDecode(Request.Params("WorkPhone")))
        End Get
    End Property

    Protected ReadOnly Property WorkPhoneExt() As String
        Get
            Return Common.SafeString(HttpUtility.UrlDecode(Request.Params("WorkPhoneEXT")))
        End Get
    End Property
    Protected ReadOnly Property MobilePhone() As String
        Get
            Return Common.SafeString(HttpUtility.UrlDecode(Request.Params("MobilePhone"))).Replace("-", "")
        End Get
    End Property
    Protected ReadOnly Property Email() As String
        Get
            Return Common.SafeString(HttpUtility.UrlDecode(Request.Params("Email")))
        End Get
    End Property
    Protected ReadOnly Property ContactMethod() As String
        Get
            Return Common.SafeString(HttpUtility.UrlDecode(Request.Params("ContactMethod")))
        End Get
    End Property
    ''---co App
    ''-------for coApp accu------------------

    Protected ReadOnly Property co_HomePhone() As String
        Get
            Return Common.SafeString(HttpUtility.UrlDecode(Request.Params("co_HomePhone")))
        End Get
    End Property
    Protected ReadOnly Property co_WorkPhone() As String
        Get
            Return Common.SafeString(HttpUtility.UrlDecode(Request.Params("co_WorkPhone")))
        End Get
    End Property

    Protected ReadOnly Property co_WorkPhoneExt() As String
        Get
            Return Common.SafeString(HttpUtility.UrlDecode(Request.Params("co_WorkPhoneEXT")))
        End Get
    End Property
    Protected ReadOnly Property co_MobilePhone() As String
        Get
            Return Common.SafeString(HttpUtility.UrlDecode(Request.Params("co_MobilePhone"))).Replace("-", "")
        End Get
    End Property
    Protected ReadOnly Property co_Email() As String
        Get
            Return Common.SafeString(HttpUtility.UrlDecode(Request.Params("co_Email")))
        End Get
    End Property
    Protected ReadOnly Property co_ContactMethod() As String
        Get
            Return Common.SafeString(HttpUtility.UrlDecode(Request.Params("co_ContactMethod")))
        End Get
    End Property
    ''--------end Contact Info --------------
    ''--------Employment info----------------
    ''-------for coApp accu------------------
    Protected ReadOnly Property employmentStatus() As String
        Get
            Return Common.SafeString(HttpUtility.UrlDecode(Request.Params("employmentStatus")))
        End Get
    End Property
    Protected ReadOnly Property employer() As String
        Get
            Return Common.SafeString(HttpUtility.UrlDecode(Request.Params("employer")))
        End Get
    End Property
    Protected ReadOnly Property occupation() As String
        Get
            Return Common.SafeString(HttpUtility.UrlDecode(Request.Params("occupation")))
        End Get
    End Property
    Protected ReadOnly Property employmentDurationMonth() As String
        Get
            Return Common.SafeString(HttpUtility.UrlDecode(Request.Params("employmentDurationMonth")))
        End Get
    End Property
    Protected ReadOnly Property employmentDurationYear() As String
        Get
            Return Common.SafeString(HttpUtility.UrlDecode(Request.Params("employmentDurationYear")))
        End Get
    End Property

    ''--------coApp------

    Protected ReadOnly Property co_employmentStatus() As String
        Get
            Return Common.SafeString(HttpUtility.UrlDecode(Request.Params("co_employmentStatus")))
        End Get
    End Property
    Protected ReadOnly Property co_employer() As String
        Get
            Return Common.SafeString(HttpUtility.UrlDecode(Request.Params("co_employer")))
        End Get
    End Property
    Protected ReadOnly Property co_occupation() As String
        Get
            Return Common.SafeString(HttpUtility.UrlDecode(Request.Params("co_occupation")))
        End Get
    End Property
    Protected ReadOnly Property co_employmentDurationMonth() As String
        Get
            Return Common.SafeString(HttpUtility.UrlDecode(Request.Params("co_employmentDurationMonth")))
        End Get
    End Property
    Protected ReadOnly Property co_employmentDurationYear() As String
        Get
            Return Common.SafeString(HttpUtility.UrlDecode(Request.Params("co_employmentDurationYear")))
        End Get
    End Property

    ''--------end Employment info-----------
    ''--------IDentification-------------
    ''-------for coApp accu------------------
    Protected ReadOnly Property IDCardType() As String
        Get
            Return Common.SafeString(HttpUtility.UrlDecode(Request.Params("IDCardType")))
        End Get
    End Property
    Protected ReadOnly Property IDCardNumber() As String
        Get
            Return Common.SafeString(HttpUtility.UrlDecode(Request.Params("IDCardNumber")))
        End Get
    End Property
    Protected ReadOnly Property IDState() As String
        Get
            Return Common.SafeString(HttpUtility.UrlDecode(Request.Params("IDState")))
        End Get
    End Property
    Protected ReadOnly Property IDDateIssued() As String
        Get
            Return convertDate(Common.SafeString(HttpUtility.UrlDecode(Request.Params("IDDateIssued"))))
        End Get
    End Property
    Protected ReadOnly Property IDDateExpired() As String
        Get
            Return convertDate(Common.SafeString(HttpUtility.UrlDecode(Request.Params("IDDateExpired"))))
        End Get
    End Property
    ''-------coApp

    Protected ReadOnly Property co_IDCardType() As String
        Get
            Return Common.SafeString(HttpUtility.UrlDecode(Request.Params("co_IDCardType")))
        End Get
    End Property
    Protected ReadOnly Property co_IDCardNumber() As String
        Get
            Return Common.SafeString(HttpUtility.UrlDecode(Request.Params("co_IDCardNumber")))
        End Get
    End Property
    Protected ReadOnly Property co_IDState() As String
        Get
            Return Common.SafeString(HttpUtility.UrlDecode(Request.Params("co_IDState")))
        End Get
    End Property
    Protected ReadOnly Property co_IDDateIssued() As String
        Get
            Return convertDate(Common.SafeString(HttpUtility.UrlDecode(Request.Params("co_IDDateIssued"))))
        End Get
    End Property
    Protected ReadOnly Property co_IDDateExpired() As String
        Get
            Return convertDate(Common.SafeString(HttpUtility.UrlDecode(Request.Params("co_IDDateExpired"))))
        End Get
    End Property
    ''--------end Identification---------
    ''---------applicant info------------
    ''------- for coApp accu------------------

    Protected ReadOnly Property FName() As String
        Get
            Return Common.SafeString(HttpUtility.UrlDecode(Request.Params("FName")))
        End Get
    End Property

    Protected ReadOnly Property MName() As String
        Get
            Return Common.SafeString(HttpUtility.UrlDecode(Request.Params("MName")))
        End Get
    End Property
    Protected ReadOnly Property LName() As String
        Get
            Return Common.SafeString(HttpUtility.UrlDecode(Request.Params("LName")))
        End Get
    End Property
    Protected ReadOnly Property NameSuffix() As String
        Get
            Return Common.SafeString(HttpUtility.UrlDecode(Request.Params("NameSuffix")))
        End Get
    End Property
    Protected ReadOnly Property SSN() As String
        Get
            Return Common.SafeString(HttpUtility.UrlDecode(Request.Params("SSN"))).Replace("/", "").Replace("\", "").Replace("-", "")
        End Get
    End Property
    Protected ReadOnly Property Citizenship() As String
        Get
            Return Common.SafeString(HttpUtility.UrlDecode(Request.Params("Citizenship")))
        End Get
    End Property
    Protected ReadOnly Property Gender() As String
        Get
            Return Common.SafeString(HttpUtility.UrlDecode(Request.Params("Gender")))
        End Get
    End Property
    Protected ReadOnly Property MaritalStatus() As String
        Get
            Return Common.SafeString(HttpUtility.UrlDecode(Request.Params("MaritalStatus")))
        End Get
    End Property
    Protected ReadOnly Property DOB() As String
        Get
            Return convertDate(Common.SafeString(HttpUtility.UrlDecode(Request.Params("DOB"))))
        End Get
    End Property
    ''------- end for coApp accu------------------

    ''---------coApp
    Protected ReadOnly Property co_FName() As String
        Get
            Return Common.SafeString(HttpUtility.UrlDecode(Request.Params("co_FName")))
        End Get
    End Property

    Protected ReadOnly Property co_MName() As String
        Get
            Return Common.SafeString(HttpUtility.UrlDecode(Request.Params("co_MName")))
        End Get
    End Property
    Protected ReadOnly Property co_LName() As String
        Get
            Return Common.SafeString(HttpUtility.UrlDecode(Request.Params("co_LName")))
        End Get
    End Property
    Protected ReadOnly Property co_NameSuffix() As String
        Get
            Return Common.SafeString(HttpUtility.UrlDecode(Request.Params("co_NameSuffix")))
        End Get
    End Property
    Protected ReadOnly Property co_SSN() As String
        Get
            Return Common.SafeString(HttpUtility.UrlDecode(Request.Params("co_SSN"))).Replace("/", "").Replace("\", "").Replace("-", "")
        End Get
    End Property

    Protected ReadOnly Property co_Citizenship() As String
        Get
            Return Common.SafeString(HttpUtility.UrlDecode(Request.Params("co_Citizenship")))
        End Get
    End Property
    Protected ReadOnly Property co_Gender() As String
        Get
            Return Common.SafeString(HttpUtility.UrlDecode(Request.Params("co_Gender")))
        End Get
    End Property
    Protected ReadOnly Property co_MaritalStatus() As String
        Get
            Return Common.SafeString(HttpUtility.UrlDecode(Request.Params("co_MaritalStatus")))
        End Get
    End Property
    Protected ReadOnly Property co_DOB() As String
        Get
            Return convertDate(Common.SafeString(HttpUtility.UrlDecode(Request.Params("co_DOB"))))
        End Get
    End Property

    ''---------end applicant infor--------
#End Region
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
		'log.Info("XA: Page loading")
        '==is url and credential for live
        Dim sHost = SafeString(HttpContext.Current.Request.Url.Host).ToUpper
        If sHost.Contains("APP.LOANSPQ") Then
            _isLive = "Y"
        End If

        ucPageHeader._currentConfig = _CurrentWebsiteConfig
		'ucUploadDocDialog.CurrentWebsiteConfig = _CurrentWebsiteConfig
		'ucUploadDocDialog.UploadLoandocPath = "ENUMS/XA_DOC_TITLE/ITEM"
		'ucCoUploadDocDialog.CurrentWebsiteConfig = _CurrentWebsiteConfig
		'ucCoUploadDocDialog.UploadLoandocPath = "ENUMS/XA_DOC_TITLE/ITEM"
		xaDriverlicenseScan.CurrentConfig = _CurrentWebsiteConfig
		xaCoDriverlicenseScan.CurrentConfig = _CurrentWebsiteConfig
        'xaApplicantAddress.FooterDataTheme = _FooterDataTheme
        'xaCoApplicantAddress.FooterDataTheme = _FooterDataTheme
        '' clear any session related to PAYPAL stuff, this is different from  session("APPROVED_MESSAGE")
        Session("PAYPAL_PAYMENT_MESSAGE") = Nothing

		''token use for session validation 
        If (Session("token") IsNot Nothing) And Session("token_count") = Nothing Then 'reuse the same token if user hasn't submit yet, occur when user open multiple windows
            _token = Common.SafeString(Session("token"))
        Else
            _token = GetToken()
        End If

		If _CurrentWebsiteConfig.authKey <> "" AndAlso Not IsValidToken(Common.SafeString(Request.Params("authkey")), _CurrentWebsiteConfig) Then
			log.Info("Invalid access key")
			Response.Write("Your access key is invalid")
			Return
		End If

		Dim sAvailability As String = SafeString(Request.Params("type")) 'primary/secondary     

        If sAvailability = "1a" Or sAvailability = "2a" Then
            _HasMinorApplicant = "Y"
            _visibleMinorApp = "block;"
        Else
            _visibleMinorApp = "none;"
            _HasMinorApplicant = "N"
        End If
        Select Case sAvailability
            Case "1"
                _Availability = "1"
            Case "2"
                _Availability = "2"
            Case "1a"
                _Availability = "1a"
            Case "2a"
                _Availability = "2a"
            Case "1b"
                _Availability = "1b"
            Case "2b"
                _Availability = "2b"
            Case Else
                _Availability = "1"
        End Select
       
		'' ''get the previous_address_threshold if existing occupancy status
		''_previous_address_thresshold = Common.getPreviousAddressThresHold(_CurrentWebsiteConfig)
		Dim liveMonthsDropdown As String = Common.GetMonths()
		''add ddlyears and months
		Dim liveYearsDropdown As String = Common.GetYears()
		Dim occupyingLocationDropdown As String = Common.RenderDropdownlistWithEmpty(CEnum.OCCUPY_LOCATIONS, " ", "--Please Select--")
		''applicant

		If CheckShowField("divOccupancyStatusSection", "", True) Then
			xaApplicantAddress.LiveMonthsDropdown = liveMonthsDropdown
			xaApplicantAddress.LiveYearsDropdown = liveYearsDropdown
			xaApplicantAddress.OccupyingLocationDropdown = occupyingLocationDropdown
			xaApplicantAddress.EnableOccupancyStatus = True
		Else
			xaApplicantAddress.EnableOccupancyStatus = False
		End If
		If CheckShowField("divOccupancyStatusSection", "co_", True) Then
			''coApplicant
			xaCoApplicantAddress.LiveMonthsDropdown = liveMonthsDropdown
			xaCoApplicantAddress.LiveYearsDropdown = liveYearsDropdown
			xaCoApplicantAddress.OccupyingLocationDropdown = occupyingLocationDropdown
			xaCoApplicantAddress.EnableOccupancyStatus = True
		Else
			xaCoApplicantAddress.EnableOccupancyStatus = False
		End If
		If CheckShowField("divOccupancyStatusSection", "m_", True) Then
			'minor
			xaMinorApplicantAddress.LiveMonthsDropdown = liveMonthsDropdown
			xaMinorApplicantAddress.LiveYearsDropdown = liveYearsDropdown
			xaMinorApplicantAddress.OccupyingLocationDropdown = occupyingLocationDropdown
			xaMinorApplicantAddress.EnableOccupancyStatus = True
		Else
			xaMinorApplicantAddress.EnableOccupancyStatus = False
		End If

		

        Dim ddlIDCardType As Dictionary(Of String, String)
        If _CurrentWebsiteConfig.GetEnumItems("ID_CARD_TYPES").Count = 0 Then
			ddlIDCardType = CFOMQuestion.DownLoadIDCardTypes(_CurrentWebsiteConfig)
            If (ddlIDCardType.Count = 0) Then  'there is no id_card_type from retrieval list
                xaApplicantID.isExistIDCardType = False
                xaCoApplicantID.isExistIDCardType = False
                ''minor ID
                xaMinorApplicantID.isExistIDCardType = False
            Else
                'ID_Card_type dropdown, default driver license
                xaApplicantID.isExistIDCardType = True
                xaCoApplicantID.isExistIDCardType = True
                _IDCardTypeDropDown = Common.RenderDropdownlist(ddlIDCardType, " ", "DRIVERS_LICENSE")
                xaApplicantID.IDCardTypeList = _IDCardTypeDropDown
                xaCoApplicantID.IDCardTypeList = _IDCardTypeDropDown
                ''minor ID
                xaMinorApplicantID.isExistIDCardType = True
                xaMinorApplicantID.IDCardTypeList = _IDCardTypeDropDown
            End If
        Else
            'ID_Card_type dropdown, default driver license
            xaApplicantID.isExistIDCardType = True
            xaCoApplicantID.isExistIDCardType = True
            ''minor ID
            xaMinorApplicantID.isExistIDCardType = True

            ddlIDCardType = _CurrentWebsiteConfig.GetEnumItems("ID_CARD_TYPES")
            _IDCardTypeDropDown = Common.RenderDropdownlist(ddlIDCardType, " ", "DRIVERS_LICENSE")
            xaApplicantID.IDCardTypeList = _IDCardTypeDropDown
            xaCoApplicantID.IDCardTypeList = _IDCardTypeDropDown
            'minor id
            xaMinorApplicantID.isExistIDCardType = True
            xaMinorApplicantID.IDCardTypeList = _IDCardTypeDropDown
        End If

        'override default so it cann be called from design page
        If _RedirectXAURL <> "" Then _RedirectURL = _RedirectXAURL

        Try
			_ProductPackageJsonString = CProduct.GetJsonProductsPackage(_CurrentWebsiteConfig, _Availability)

            Dim fundingSource As CFundingSourceInfo
            If isCheckFundingPage() = True Then  'funding page is enabled
				fundingSource = CFundingSourceInfo.CurrentJsonFundingPackage(_CurrentWebsiteConfig)
            Else
                fundingSource = New CFundingSourceInfo(New CLenderServiceConfigInfo())
            End If

            '' try to append PAYPAL funding option into _FundingSourcePackageJsonString since its config stay in WEBSITE node
            If fundingSource.ListTypes IsNot Nothing AndAlso Not String.IsNullOrEmpty(_CurrentWebsiteConfig.PayPalEmail) Then
                '' make sure PAYPAL option stay on top(below not funding option)
                fundingSource.ListTypes.Insert(1, "PAYPAL")
            End If
            '' serialize fundingSource into JSON then .aspx can access it in Page_PreRender 
            _FundingSourcePackageJsonString = JsonConvert.SerializeObject(fundingSource)
            xaFundingOptions.FundingTypeList = fundingSource.ListTypes
        Catch ex As Exception  'catch connection issue
            logError("Check connection, etc..", ex)
            Return
        End Try

        If (IsPostBack = False) Then
            FirstTimeInit()
            SetVisibleAttribute()  'set HideFOMPage.Value based on config or url paramater type

			If Not CFOMQuestion.CurrentFOMQuestions(_CurrentWebsiteConfig).Count > 0 Then
				HideFOMPage.Value = "Y"
			End If

            '' If Not HideFOMPage.Value = "Y" Then
			ucFomQuestion.FOMQuestions = CFOMQuestion.CurrentFOMQuestions(_CurrentWebsiteConfig)
			ucFomQuestion._CFOMQuestionList = CFOMQuestion.DownloadFomNextQuestions(_CurrentWebsiteConfig)
            ucFomQuestion._lenderRef = _CurrentLenderRef
            ''End If
            'show the question fom
            If HideFOMPage.Value = "Y" Then
                '_PageFOMHeader = ""
                '_PageFOMFooter = ""
                _XALogo = _Logo
                '_XAOtherLogo = "" ''already has _XALogo
                'divShowFOM.Visible = False
                ucFomQuestion.Visible = False
                'disabled via change request #B7 (Sep 22,2016)
                '_HasNavLeft = ""
                ''hide and show document scan page based on the scanDocumentKey from config or SSO mode
                If String.IsNullOrEmpty(Common.SafeString(_CurrentWebsiteConfig.ScanDocumentKey)) Or IsSSO Then
                    scandocs.Visible = False
                    'Else
                    ''_HasNavLeft = "<a href='' data-icon='arrow-l' data-iconpos='left' class='NavIconLeft'  data-rel='back' >Back</a>"
                    '_HasNavLeft = "Or <a href='#' data-rel='back' class ='div-goback' data-corners='false' data-shadow='false' data-theme='reset'><span class='hover-goback'> Go Back</span></a>"
                End If
            Else
                ucFomQuestion.Visible = True
                'divShowFOM.Visible = True
                ''_HasNavLeft = "<a href='' data-icon='arrow-l' data-iconpos='left' class='NavIconLeft'  data-rel='back' >Back</a>"
                '_HasNavLeft = "Or <a href='#' data-rel='back' class ='div-goback' data-corners='false' data-shadow='false' data-theme='reset'><span class='hover-goback'> Go Back</span></a>"
                _XALogo = "" ''already has logo in QuestionFom
                '_XAOtherLogo = _OtherLogo
                '_PageFOMHeader = RenderingPageFOMHeader()
                ucFomQuestion.FormHeader = RenderingPageFOMHeader()
                '_PageFOMFooter = RenderingPageFOMFooter()
            End If
            ''enable attribute
            'hdEnableJoint.Value = hasEnableJointXA(_CurrentWebsiteConfig)
            _enableJoint = Not hasEnableJointXA(_CurrentWebsiteConfig).ToUpper().Equals("N")
            hdForeignContact.Value = isForeignContact(_CurrentWebsiteConfig)


            Dim nAvailability As String = "1"
            If Not String.IsNullOrEmpty(Request.QueryString("type")) Then
                nAvailability = Common.SafeString(Request.QueryString("type"))
            End If
            xaApplicantQuestion.nAvailability = nAvailability
            xaCoApplicantQuestion.nAvailability = nAvailability
            xaMinorApplicantQuestion.nAvailability = nAvailability

            xaApplicantInfo.ContentDataTheme = _ContentDataTheme
            xaMinorApplicantInfo.ContentDataTheme = _ContentDataTheme
            xaCoApplicantInfo.ContentDataTheme = _ContentDataTheme
        Else
            'HandleFormSubmit()
        End If
        'asign config theme for PageHeader UC, avoid reading config file unnescessary
        ucPageHeader._headerDataTheme = _HeaderDataTheme
        ucPageHeader._footerDataTheme = _FooterDataTheme
        ucPageHeader._contentDataTheme = _ContentDataTheme
		ucPageHeader._headerDataTheme2 = _HeaderDataTheme2
        ucPageHeader._backgroundDataTheme = _BackgroundTheme
		ucPageHeader._buttonDataTheme = _ButtonTheme
		ucPageHeader._buttonFontColor = _ButtonFontTheme

		_productCodes = parsedProductCodes(_CurrentWebsiteConfig)
    End Sub

    ''get product_code list from config
    Private Function parsedProductCodes(ByVal poCurrentConfig As CWebsiteConfig) As String
        Dim sProductCodes As New List(Of String)
        Dim oNodes As XmlNodeList = poCurrentConfig._webConfigXML.SelectNodes("XA_LOAN/ACCOUNTS/ACCOUNT")
        If oNodes.Count = 0 Then
            Return ""
        End If
        For Each childNode As XmlNode In oNodes
            Try
                If Not String.IsNullOrEmpty(childNode.Attributes("product_code").InnerXml) Then
                    sProductCodes.Add(childNode.Attributes("product_code").InnerXml)
                End If
            Catch ex As Exception
                Return ""
            End Try
        Next
        Return New JavaScriptSerializer().Serialize(sProductCodes)
    End Function
    ''end get product_code list

    Private Function hasEnableJointXA(ByVal poCurrentConfig As CWebsiteConfig) As String
        Dim isEnableJoint As String = ""
        Dim oNodes As XmlNodeList = poCurrentConfig._webConfigXML.SelectNodes("XA_LOAN/VISIBLE")
        If oNodes.Count = 0 Then
            Return "" ''return by default
        End If
        For Each childNode As XmlNode In oNodes
            Try
                isEnableJoint = childNode.Attributes("joint_enable").InnerXml
            Catch ex As Exception
                Return ""
            End Try
        Next
        Return isEnableJoint
    End Function

    Private Function isForeignContact(ByVal poCurrentConfig As CWebsiteConfig) As String
        Dim isForeign As String = ""
        Dim oNodes As XmlNodeList = poCurrentConfig._webConfigXML.SelectNodes("XA_LOAN/VISIBLE")
        If oNodes.Count = 0 Then
            Return "" ''return by default
        End If
        For Each childNode As XmlNode In oNodes
            Try
                isForeign = childNode.Attributes("foreign_contact").InnerXml
            Catch ex As Exception
                Return ""
            End Try
        Next
        Return isForeign
    End Function

    Private Function isCheckFundingPage() As Boolean
        Dim result As Boolean = True
        Dim oNodes As XmlNodeList = _CurrentWebsiteConfig._webConfigXML.SelectNodes("XA_LOAN/VISIBLE")
        If oNodes.Count < 1 Then
            Return True
        End If

        For Each child As XmlNode In oNodes
            If child.Attributes("funding") Is Nothing Then Continue For
            If Common.SafeString(child.Attributes("funding").InnerText) = "N" Then
                result = False
            End If
        Next

        Return result
    End Function



    Private Function EligibilityMessage() As String
        Dim elMessage As String = ""
		Dim strDefaultElMessage As String = Common.GenerateDefaultEligibilityMessage()	 ''default Eligibilty message
        
        Dim oElMessagedNode As XmlNode = _CurrentWebsiteConfig._webConfigXML.SelectSingleNode("XA_LOAN/ELIGIBILITY_MESSAGE")
        If oElMessagedNode IsNot Nothing Then
            If (Not String.IsNullOrEmpty(oElMessagedNode.InnerText)) Then
                elMessage = Server.HtmlDecode(oElMessagedNode.InnerText)
            Else
                elMessage = strDefaultElMessage
            End If

        Else
            elMessage = strDefaultElMessage '' get the defaut message if there is no eli message in config
        End If
        Return elMessage
    End Function

    Private Sub FirstTimeInit()
        '_Lender_ID = Request.Params("lenderid")	 '?what is the use: TODO
        '_Org_ID = Request.Form("organization_id")

        ''new format no use
        'If sAccountType <> "" Then
        '    divPrimarySecondary.Visible = True
        'End If

		Dim currentProdList As List(Of CProduct) = CProduct.GetProducts(_CurrentWebsiteConfig, _Availability)
        'TEMPORARY ADD THESE LINES OF CODE TO AVOID BUG COME FROM THE SERVICE TO GET LIST OF PRODUCT
        'THAT SERVICE RETURN NOT WELL-FORMED HTML (HTML CONTENT WITH MISSING CLOSING TAG)
        'If currentProdList IsNot Nothing Then
        '    For Each cProduct As CProduct In currentProdList
        '        If cProduct.Services.Any() Then
        '            For Each cProductService As CProductService In cProduct.Services
        '                If cProductService.Description.Contains("<") Then
        '                    cProduct.Services.Remove(cProductService)
        '                End If
        '            Next
        '        End If
        '    Next
        'End If
        'END TEMP

        '' render other product screen
        'Try
        '    If _CurrentWebsiteConfig._webConfigXML.SelectSingleNode("XA_LOAN/VISIBLE").Attributes("other_product").InnerXml = "Y" Then
        '        '' get other product
        '        Dim otherProds = currentProdList.Where(Function(x) x.AccountType = "OTHER").ToList()
        '        ucOtherProduct.Products = otherProds
        '    End If
        'Catch ex As Exception
        'End Try

        '' init CProductRendering object which will take care of rendering controls
        _ProductRendering = New CProductRendering(currentProdList)
        'If _ProductRendering.getNumProducts > 10 Then ' User collasible if the list get relly long  :TODO: change it back to 6
        '    _RenderedProductCollapsibles = _ProductRendering.RenderProductsGroup()
        'Else
        '    _RenderedProductRadio = _ProductRendering.renderProductsRadio()
        'End If

        _HiddenMinAmounts = _ProductRendering.RenderHiddenMinDeposits()
        _HiddenNumProducts = _ProductRendering.RenderHiddenNumProducts()
        _HiddenProducts = _ProductRendering.RenderHiddenProducts()
        _HiddenRequiredProducts = _ProductRendering.RenderHiddenRequiredProducts()

        'FOR TESTING ONLY
        'For Each cProduct As CProduct In currentProdList
        '    If cProduct.CustomQuestions.Any() Then
        '        For Each q As CProductCustomQuestion In cProduct.CustomQuestions
        '            q.IsRequired = True
        '        Next
        '    End If
        '    If cProduct.ProductCode = "CD6" Then
        '        cProduct.IsPreSelection = True
        '    End If
        '    cProduct.MinorAccountCode = "a|b|c"
        'Next
        'END FOR TESTING

        ''*****display all the products
        '_RenderSelectProducts = Server.HtmlDecode(_ProductRendering.RenderAllProductsGroup(_CurrentLenderRef))
        ucProductSelection.CurrentLenderRef = _CurrentLenderRef
        ucProductSelection.CurrentProductList = currentProdList
        ucProductSelection.ContentDataTheme = _ContentDataTheme
        ucProductSelection.FooterDataTheme = _FooterDataTheme
        ucProductSelection.HeaderDataTheme = _HeaderDataTheme
        ucProductSelection.HeaderDataTheme2 = _HeaderDataTheme2
        _RenderInitialDepositProducts = Server.HtmlDecode(_ProductRendering.RenderInitialDepositProduct(_textAndroidTel))
        Dim minorProductList As List(Of MinorAccountProduct) = Common.ParsedMinorAccountProduct(_CurrentWebsiteConfig)
        Dim minorAccountList As List(Of CMinorAccount) = Common.ParsedMinorAccountType(_CurrentWebsiteConfig)
        Dim mProductListSerializer As New JavaScriptSerializer()
        Dim mAccountListSerializer As New JavaScriptSerializer()
        _minoProductList = mProductListSerializer.Serialize(minorProductList)
        _minorAccountList = mAccountListSerializer.Serialize(minorAccountList)
        _filterProductCodes = New JavaScriptSerializer().Serialize(GetAllowedProductCodes("XA_LOAN"))
        '' RENDER Product Service(s) section
        'Dim bIsFirstRequiredProductFound As Boolean = False
        'For Each prod In currentProdList
        '    Dim ctrl As ASP.xa_inc_productservices_ascx = CType(LoadControl("~/xa/inc/ProductServices.ascx"), ASP.xa_inc_productservices_ascx)
        '    Dim tempProduct As CProduct = prod

        '    ' only allow service from the first required product to be selected during initialization
        '    If bIsFirstRequiredProductFound Then
        '        tempProduct.IsRequired = False
        '    ElseIf tempProduct.IsRequired Then
        '        bIsFirstRequiredProductFound = True
        '    End If

        '    ctrl.Product = tempProduct
        '    phProductServices.Controls.Add(ctrl)

        'Next
        'div_product_services_wrapper.Visible = phProductServices.Controls.Count > 0

        '_RenderProductQuestions = _ProductRendering.BuildProductQuestions(currentProdList, _textAndroidTel)


        ucProductSelection.ProductQuestionsHtml = _ProductRendering.BuildProductQuestions(currentProdList, _textAndroidTel)

        ''chechmailaddress
        If Not String.IsNullOrEmpty(Common.getCheckMailAddress(_CurrentWebsiteConfig)) Then
            _CheckMailAddress = Common.getCheckMailAddress(_CurrentWebsiteConfig)
        End If
        '' address_key
        If Not String.IsNullOrEmpty(_CurrentWebsiteConfig.AddressKey) Then
            _address_key = _CurrentWebsiteConfig.AddressKey
        End If
        '' maxfunding

        ' Set the initial state of the accounts and deposit amounts
        ' If there are no required products, then they should all be NOT SET.
        ' If there are required products, then set them and set the minimum deposit amount
		'Dim iNumProducts As Integer = _ProductRendering.getNumProducts()
		'_ProductRendering.SetInitialAccountStates(0, hdAccount1, hdAmount1)
		'_ProductRendering.SetInitialAccountStates(1, hdAccount2, hdAmount2)
		'_ProductRendering.SetInitialAccountStates(2, hdAccount3, hdAmount3)
        ''new format no use
        ''If iNumProducts < 3 Then
        ''    account3Div.Visible = False
        ''End If
        ''If iNumProducts < 2 Then
        ''    account2Div.Visible = False
        ''End If
        ''Citizenship default
		''end citizen ship
		'_maritalStatus = Common.getNodeAttributes(_CurrentWebsiteConfig, "XA_LOAN/VISIBLE", "marital_status")
		Dim maritalStatusDropdown As String = Common.RenderDropdownlistWithEmpty(CEnum.MARITAL_STATUS, "", "--Please Select--")
		_xaStopInvalidRoutingNumber = Common.getNodeAttributes(_CurrentWebsiteConfig, "XA_LOAN/BEHAVIOR", "stop_invalid_routing_number")

        ''check the citizenship from download
        Dim citizenshipStatusDropdown As String = ""
        Dim hasBlankDefaultCitizenship As Boolean = False
        Dim oCitizenshipNode As XmlNode = _CurrentWebsiteConfig._webConfigXML.SelectSingleNode("XA_LOAN/BLANK_DEFAULT_CITIZENSHIP")
        If oCitizenshipNode IsNot Nothing Then
            hasBlankDefaultCitizenship = True
        End If
        'change the default US Citizen 
        If _CurrentWebsiteConfig.GetEnumItems("CITIZENSHIP_TYPES").Count > 0 Then  'available in  LPQMobileWebsiteConfig
            Dim Citizenships As New Dictionary(Of String, String)
            Citizenships = _CurrentWebsiteConfig.GetEnumItems("CITIZENSHIP_TYPES")
            citizenshipStatusDropdown = Common.DefaultCitizenShipFromConfig(Citizenships, _DefaultCitizenship, _CurrentLenderRef, hasBlankDefaultCitizenship)
        Else
            citizenshipStatusDropdown = Common.DefaultCitizenShip(_DefaultCitizenship, _CurrentLenderRef, hasBlankDefaultCitizenship) ''from hard code
        End If

        Dim stateDropdown As String = Common.RenderStateDropdownlistWithEmpty(CEnum.STATES, "", "--Please Select--")
        Dim employmentStatusDropdown As String = Common.RenderDropdownlistWithEmpty(CEnum.EMPLOYMENT_STATUS, "", "--Please Select--")
        Dim liveMonthsDropdown As String = Common.GetMonths()
        xaApplicantInfo.LiveMonthsDropdown = liveMonthsDropdown
        xaApplicantInfo.MaritalStatusDropdown = maritalStatusDropdown
        xaApplicantInfo.CitizenshipStatusDropdown = citizenshipStatusDropdown

        xaCoApplicantInfo.LiveMonthsDropdown = liveMonthsDropdown
        xaCoApplicantInfo.MaritalStatusDropdown = maritalStatusDropdown
        xaCoApplicantInfo.CitizenshipStatusDropdown = citizenshipStatusDropdown
        ''minor information
        xaMinorApplicantInfo.LiveMonthsDropdown = liveMonthsDropdown
        xaMinorApplicantInfo.MaritalStatusDropdown = maritalStatusDropdown
        xaMinorApplicantInfo.CitizenshipStatusDropdown = citizenshipStatusDropdown

        xaMinorApplicantEmployment.EmploymentStatusDropdown = employmentStatusDropdown
        xaApplicantEmployment.EmploymentStatusDropdown = employmentStatusDropdown
        xaCoApplicantEmployment.EmploymentStatusDropdown = employmentStatusDropdown
        '' employee of lender
        Dim employeeOfLenderDropdown As String = ""
        If _CurrentWebsiteConfig.GetEnumItems("EMPLOYEE_OF_LENDER_TYPE").Count > 0 Then  'available in  LPQMobileWebsiteConfig
            ''get employee of lender type from config
            Dim employeeOfLenderType As New Dictionary(Of String, String)
            employeeOfLenderType = _CurrentWebsiteConfig.GetEnumItems("EMPLOYEE_OF_LENDER_TYPE")
            employeeOfLenderDropdown = Common.RenderDropdownlist(employeeOfLenderType, "")

            xaApplicantInfo.EmployeeOfLenderDropdown = employeeOfLenderDropdown
            xaCoApplicantInfo.EmployeeOfLenderDropdown = employeeOfLenderDropdown
			Dim sLenderName As String = CDownloadedSettings.DownloadLenderServiceConfigInfo(_CurrentWebsiteConfig).LenderName
            xaApplicantInfo.LenderName = sLenderName
            xaCoApplicantInfo.LenderName = sLenderName
        End If

        ''add state dropdown
        xaApplicantAddress.StateDropdown = stateDropdown
        xaCoApplicantAddress.StateDropdown = stateDropdown
        ''minor 
		xaMinorApplicantAddress.StateDropdown = stateDropdown

		xaApplicantID.StateDropdown = stateDropdown
		xaCoApplicantID.StateDropdown = stateDropdown
		xaMinorApplicantID.StateDropdown = stateDropdown

        '' Set custom questions
        Dim oAllowedCustomQuestionNames As List(Of String) = GetAllowedCustomQuestionNames()
		Dim questions As New List(Of CCustomQuestionXA)
		If oAllowedCustomQuestionNames.Count > 0 Then
			Dim questionsTemp As List(Of CCustomQuestionXA) = CCustomQuestionXA.CurrentXACustomQuestions(_CurrentWebsiteConfig)
			For Each oItem As CCustomQuestionXA In questionsTemp
				If Not oAllowedCustomQuestionNames.Contains(oItem.Name.ToLower) Then
					Continue For
				End If
				questions.Add(oItem)
			Next
		Else
			questions = CCustomQuestionXA.CurrentXACustomQuestions(_CurrentWebsiteConfig)
		End If
        ucCustomQuestions.CustomQuestionXAs = questions
        ucCustomQuestions.ConditionedQuestions = getConditionQuestionXAs(_CurrentLenderId)
        'TODO:may need to 
        If questions IsNot Nothing AndAlso questions.Count > 0 Then
            _visibleCustomQuestion = "block"
        Else
            _visibleCustomQuestion = "none"
        End If
		If CheckShowField("divDisclosureSection", "", True) Then
			''TODO: refactor to disclosure control
			'' Set disclosures
			Dim disclosures As List(Of String) = Common.GetDisclosures(_CurrentWebsiteConfig, "XA", False, _Availability)
			ucDisclosures.Disclosures = disclosures
			ucDisclosures.MappedShowFieldLoanType = mappedShowFieldLoanType

			' ''verify code
			''  Dim reasonableDemoCode As String = Common.getNodeAttributes(_CurrentWebsiteConfig, "XA_LOAN/VISIBLE", "verify_code_enable")
			''  ucDisclosures.strRenderingVerifyCode = Common.RenderingVerifyCodeField(reasonableDemoCode, _CurrentWebsiteConfig)
		End If

		_SubmitURL = _CurrentWebsiteConfig.BaseSubmitLoanUrl & "/SubmitLoan/SubmitLoan.aspx"

		'set/hide fields in Applicant - custom for GETCU
		xaApplicantInfo.Config = _CurrentWebsiteConfig.GetWebConfigElement
		xaCoApplicantInfo.Config = _CurrentWebsiteConfig.GetWebConfigElement
		xaMinorApplicantInfo.Config = _CurrentWebsiteConfig.GetWebConfigElement
		xaApplicantEmployment.Config = _CurrentWebsiteConfig.GetWebConfigElement
		xaCoApplicantEmployment.Config = _CurrentWebsiteConfig.GetWebConfigElement
		xaMinorApplicantEmployment.Config = _CurrentWebsiteConfig.GetWebConfigElement
		' Fill out the form fields if test mode is set
		'Dim sTestMode As String = Request.Params("testmode")
		'If sTestMode <> Nothing Then
		'	If sTestMode.ToUpper() = "ON" Then
		'		autoFillForm()
		'	End If
		'End If

		'' Test the submission of test data
		'testApplicationSubmission()

		'Dim bIsCameraAllow As Boolean = True
		'If PlatformSource = "DI" And _textAndroidTel = "tel" Then	'don't allow camera when running inside DI android app
		'	bIsCameraAllow = False
		'End If

		' ''upload document message
		'Dim uploadDocs As String = Common.getXAUploadDocumentMessage(_CurrentWebsiteConfig)
		'If Common.getXAUploadDocumentEnable(_CurrentWebsiteConfig) AndAlso Not String.IsNullOrEmpty(uploadDocs) And bIsCameraAllow Then
		'	Dim docTitleRequired = LPQMobile.Utils.Common.getNodeAttributes(_CurrentWebsiteConfig, "XA_LOAN/BEHAVIOR", "required_doc_title").Equals("Y", StringComparison.OrdinalIgnoreCase)
		'	Dim docTitleOptions = Common.GetDocumentTitleInfoList(_CurrentWebsiteConfig._webConfigXML.SelectSingleNode("ENUMS/XA_DOC_TITLE"))
		'	ucDocUpload.Title = Server.HtmlDecode(uploadDocs)
		'	ucDocUpload.RequireDocTitle = docTitleRequired
		'	ucDocUpload.DocTitleOptions = docTitleOptions
		'	ucDocUpload.DocUploadRequired = _requiredUpLoadDocsXA.Equals("Y", StringComparison.OrdinalIgnoreCase)

		'	ucCoDocUpload.Title = Server.HtmlDecode(uploadDocs)
		'	ucCoDocUpload.RequireDocTitle = docTitleRequired
		'	ucCoDocUpload.DocTitleOptions = docTitleOptions
		'	ucCoDocUpload.DocUploadRequired = _requiredUpLoadDocsXA.Equals("Y", StringComparison.OrdinalIgnoreCase)
		'	ucDocUpload.Visible = True
		'	ucCoDocUpload.Visible = True
		'	ucDocUploadSrcSelector.Visible = True
		'	ucCoDocUploadSrcSelector.Visible = True
		'Else
		'	ucDocUpload.Visible = False
		'	ucCoDocUpload.Visible = False
		'	ucDocUploadSrcSelector.Visible = False
		'	ucCoDocUploadSrcSelector.Visible = False
		'End If
		'ucCoUploadDocDialog.Title = _UpLoadDocument
		'ucUploadDocDialog.Title = _UpLoadDocument
		_HasEnableSecondaryUpLoadDocs = Common.getNodeAttributes(_CurrentWebsiteConfig, "XA_LOAN/VISIBLE", "camera_xa_secondary_enable")
		''end upload
		'' app_type ='FOREIGN'
		Dim appType As String = _CurrentWebsiteConfig.AppType
		hdForeignAppType.Value = ""	'' initial hidden value foreign
		''Make sure appType is not nothing
		If Not String.IsNullOrEmpty(appType) Then
			If appType.ToUpper() = "FOREIGN" Then
				hdForeignAppType.Value = "Y"
			End If
		End If
		''backgroud theme
		'If String.IsNullOrEmpty(_BackgroundTheme) Then
		'    _bgTheme = _FooterDataTheme
		'Else
		'    _bgTheme = _BackgroundTheme
		'End If
		'xaDriverlicenseScan._sThemeFileName = Common.Theme_File(_CurrentWebsiteConfig)
		'xaDriverlicenseScan._currentConfig = _CurrentWebsiteConfig
		''hide the scan docs page if SSO via MobileApp
		Dim scanDocsKey As String = ""
		If Common.SafeString(Request.Params("FName")) = "" Then
			scanDocsKey = _CurrentWebsiteConfig.ScanDocumentKey
		End If
		If Not String.IsNullOrEmpty(scanDocsKey) Then
			'xaDriverlicenseScan._sDocScanMessage = Server.HtmlDecode(Common.getDocumentScanMessage(_CurrentWebsiteConfig))
			hdScanDocumentKey.Value = _CurrentWebsiteConfig.ScanDocumentKey
			' xaDriverlicenseScan._sStateDropdown = stateDropdown
			'xaDriverlicenseScan._sHeaderDataTheme = _HeaderDataTheme
			'xaDriverlicenseScan._sFooterDataTheme = _FooterDataTheme
			'xaDriverlicenseScan._sContentDataTheme = _ContentDataTheme
			''for joint
			'xaCoDriverlicenseScan._sStateDropdown = stateDropdown
			'xaCoDriverlicenseScan._sHeaderDataTheme = _HeaderDataTheme
			'xaCoDriverlicenseScan._sFooterDataTheme = _FooterDataTheme
			'xaCoDriverlicenseScan._sContentDataTheme = _ContentDataTheme
		Else
			hdScanDocumentKey.Value = ""
		End If
		''credit pull messagein disclosure section
		Dim oNode As XmlNode = _CurrentWebsiteConfig._webConfigXML.SelectSingleNode("CREDIT_PULL_MESSAGE")
		If oNode IsNot Nothing Then
			_CreditPullMessage = oNode.InnerText
		End If
		''override credit pull message if it exist in xa_loan
		Dim oXANode As XmlNode = _CurrentWebsiteConfig._webConfigXML.SelectSingleNode("XA_LOAN/CREDIT_PULL_MESSAGE")
		If oXANode IsNot Nothing Then
			If Not String.IsNullOrEmpty(oXANode.InnerText) Then
				_CreditPullMessage = oXANode.InnerText
			End If
		End If

		'TODO: should be and with XA underwrite too
		_hasCreditPull = _CurrentWebsiteConfig.CreditPull

		''get minor_employment and minor_id attribute from config
		''minor applicant special info
		Dim strVisbileNode As String = "XA_LOAN/VISIBLE"
		'_HasMinorId = Common.getNodeAttributes(_CurrentWebsiteConfig, strVisbileNode, "minor_id")
		'_HasMinorEmployment = Common.getNodeAttributes(_CurrentWebsiteConfig, strVisbileNode, "minor_employment")
		hdSpecialJoint.Value = Common.getNodeAttributes(_CurrentWebsiteConfig, strVisbileNode, "special_joint")
		''minor id
		'If _HasMinorId = "Y" Then
		'	_visibleMinorId = "block"
		'Else
		'	_visibleMinorId = "none"
		'End If
		' '' minor employment
		'If _HasMinorEmployment = "Y" Then
		'	_visibleMinorEmployment = "block"
		'Else
		'	_visibleMinorEmployment = "none"
		'End If
		''render special account type 
		If _Availability = "1a" Or _Availability = "2a" Or _Availability = "1b" Or _Availability = "2b" Then
			Dim oNodeName As String = "XA_LOAN/SPECIAL_ACCOUNT_TYPE/ITEM"
			Dim parseSpecialAccountType As Dictionary(Of String, String) = Common.getItemsFromConfig(_CurrentWebsiteConfig, oNodeName, "account_code", "title")
			Dim ddlSpecialAccountType As Dictionary(Of String, String) = filterSpecialAccountType(currentProdList, parseSpecialAccountType)
			Dim btnClass = "class='btn-header-theme active'"
			If _Availability = "1b" Or _Availability = "2b" Then
				btnClass = "class='btn-header-theme'"
			End If
			If ddlSpecialAccountType.Count > 1 Then	'' rendering dropdown when it has at least 2 account types
				Dim strDefaultOption As String = "<option value=''>Choose an account type</option>"
				_RenderSpecialAccountType = "<div id='divSpecialAccountType' class='header-theme-dropdown-btn' style='padding-top:3px'><select id='ddlSpecialAccountType'>" & strDefaultOption & Common.RenderDropdownlist(ddlSpecialAccountType, "") & "</select></div>"
			ElseIf ddlSpecialAccountType.Count = 1 Then
				hdMinorAccountCode.Value = ddlSpecialAccountType.ElementAt(0).Key
				_RenderSpecialAccountType = "<div id='divSpecialAccountType' class='text-align-center' ><a data-role='button'" & btnClass & " id='ddlSpecialAccountType' onclick='toggleSpecialAccountTypeButton(this);' >" & ddlSpecialAccountType.ElementAt(0).Value & "</a></div>"
			Else
				_RenderSpecialAccountType = "<div id='divSpecialAccountType' class='text-align-center' ><a data-role='button' class='btn-header-theme ui-disabled' id='ddlSpecialAccountType' onclick='toggleSpecialAccountTypeButton(this);' >Minor Account is not available.</a></div>"
			End If
		End If
		
		xaApplicantEmployment.EnableGrossMonthlyIncome = CheckShowField("divGrossMonthlyIncome", "", True)
		xaCoApplicantEmployment.EnableGrossMonthlyIncome = CheckShowField("divGrossMonthlyIncome", "co_", True)
		xaMinorApplicantEmployment.EnableGrossMonthlyIncome = CheckShowField("divGrossMonthlyIncome", "m_", True)

		xaApplicantInfo.EnableMotherMaidenName = CheckShowField("divMotherMaidenName", "", True)
		xaCoApplicantInfo.EnableMotherMaidenName = CheckShowField("divMotherMaidenName", "co_", True)
		xaMinorApplicantInfo.EnableMotherMaidenName = CheckShowField("divMotherMaidenName", "m_", True)

		xaApplicantInfo.EnableCitizenshipStatus = CheckShowField("divCitizenshipStatus", "", True)
		xaCoApplicantInfo.EnableCitizenshipStatus = CheckShowField("divCitizenshipStatus", "co_", True)
		xaMinorApplicantInfo.EnableCitizenshipStatus = CheckShowField("divCitizenshipStatus", "m_", True)

		If _Availability = "1" Then
			xaApplicantInfo.EnableGender = CheckShowField("divGender", "", True)
			xaCoApplicantInfo.EnableGender = CheckShowField("divGender", "co_", True)
			xaMinorApplicantInfo.EnableGender = CheckShowField("divGender", "m_", True)
		Else
			xaApplicantInfo.EnableGender = False
			xaCoApplicantInfo.EnableGender = False
			xaMinorApplicantInfo.EnableGender = False
		End If
		xaApplicantInfo.MappedShowFieldLoanType = mappedShowFieldLoanType
		xaCoApplicantInfo.MappedShowFieldLoanType = mappedShowFieldLoanType
		xaMinorApplicantInfo.MappedShowFieldLoanType = mappedShowFieldLoanType
		xaApplicantAddress.MappedShowFieldLoanType = mappedShowFieldLoanType
		xaCoApplicantAddress.MappedShowFieldLoanType = mappedShowFieldLoanType
		xaMinorApplicantAddress.MappedShowFieldLoanType = mappedShowFieldLoanType

		xaApplicantEmployment.MappedShowFieldLoanType = mappedShowFieldLoanType
		xaCoApplicantEmployment.MappedShowFieldLoanType = mappedShowFieldLoanType
		xaMinorApplicantEmployment.MappedShowFieldLoanType = mappedShowFieldLoanType

		xaApplicantID.MappedShowFieldLoanType = mappedShowFieldLoanType
		xaCoApplicantID.MappedShowFieldLoanType = mappedShowFieldLoanType
		xaMinorApplicantID.MappedShowFieldLoanType = mappedShowFieldLoanType
		'get configuration of employment
		'_ShowEmployment = Common.getNodeAttributes(_CurrentWebsiteConfig, strVisbileNode, "employment")

    End Sub
    ''convert date to date format
    Protected Function convertDate(ByVal sInputDate As String) As String
        If IsNumeric(sInputDate) And sInputDate.Length = 8 Then
            Return sInputDate.Substring(0, 2) + "/" + sInputDate.Substring(2, 2) + "/" + sInputDate.Substring(4, 4)
        End If

        If Not IsDate(sInputDate) Then
            Return ""
        End If

        Dim sDate As DateTime = DateTime.Parse(sInputDate)
        Dim strDay = sDate.Day.ToString
        Dim strMonth = sDate.Month.ToString
        Dim strYear = sDate.Year.ToString
        If strDay.Length = 1 Then
            strDay = "0" + strDay
        End If
        If strMonth.Length = 1 Then
            strMonth = "0" + strMonth
        End If
        Return strMonth + "/" + strDay + "/" + strYear
    End Function
    Public Function hasSpecialAccountCode(ByVal accountCodeList As List(Of String), ByVal accountCode As String) As Boolean
        Dim sIndex As Integer
        For sIndex = 0 To accountCodeList.Count - 1
            If accountCode = accountCodeList(sIndex) Then
                Return True
            End If
        Next
        Return False
    End Function
    Public Function filterSpecialAccountType(ByVal oProductList As List(Of CProduct), ByVal oSpecialAcountType As Dictionary(Of String, String)) As Dictionary(Of String, String)
        Dim currSpecialAccountType As New Dictionary(Of String, String)
        Dim currSpecialAccountCode As New List(Of String)
        For Each oProdNode As CProduct In oProductList
			If oProdNode.MinorAccountCode IsNot Nothing AndAlso oProdNode.MinorAccountCode.Contains("|") Then ''take care the product has more than one minor account codes
				Dim currentList As String() = oProdNode.MinorAccountCode.Split("|"c)
				Dim sIndex As Integer
				For sIndex = 0 To currentList.Count - 1
					Dim hasAccountCode = hasSpecialAccountCode(currSpecialAccountCode, currentList(sIndex))
					If Not hasAccountCode Then
						currSpecialAccountCode.Add(currentList(sIndex))
					End If
				Next
			Else
				Dim hasAccountCode = hasSpecialAccountCode(currSpecialAccountCode, oProdNode.MinorAccountCode)
				If Not hasAccountCode Then
					If Not String.IsNullOrEmpty(oProdNode.MinorAccountCode) Then
						currSpecialAccountCode.Add(oProdNode.MinorAccountCode)
					End If
				End If
			End If
        Next
        If _Availability = "1b" Or _Availability = "2b" Then
            If oSpecialAcountType.Count > 0 Then
                If Not currSpecialAccountType.ContainsKey("REGULAR") Then
                    currSpecialAccountType.Add("REGULAR", "Apply for an adult account")
                End If
            End If
        End If
        Dim i As Integer = 0
        For i = 0 To currSpecialAccountCode.Count - 1
            If oSpecialAcountType.ContainsKey(currSpecialAccountCode(i)) Then
                Dim value As String = oSpecialAcountType.Item(currSpecialAccountCode(i))
                If Not currSpecialAccountType.ContainsKey(currSpecialAccountCode(i)) Then
                    currSpecialAccountType.Add(currSpecialAccountCode(i), value)
                End If
            End If
        Next
        Return currSpecialAccountType
    End Function
    Protected Function getConditionQuestionXAs(ByVal sCurrentlenderID As String) As List(Of CCustomQuestion)
        Dim xaConQuestions As New List(Of CCustomQuestion)
        Dim conQuestions As List(Of CCustomQuestion) = _CurrentWebsiteConfig.GetConditionedQuestions("XA_LOAN")
        For Each conCQ As CCustomQuestion In conQuestions
			Dim cqList As List(Of CCustomQuestionXA) = CCustomQuestionXA.CurrentXACustomQuestions(_CurrentWebsiteConfig)
            For Each cq As CCustomQuestionXA In cqList
                If (conCQ.Name = cq.Name) Then
                    Select Case cq.AnswerType.ToLower()
                        Case "text"
                            conCQ.UIType = QuestionUIType.TEXT
                        Case "dropdown"
                            conCQ.UIType = QuestionUIType.DROPDOWN
                        Case "checkbox"
                            conCQ.UIType = QuestionUIType.CHECK
                        Case "radio"
                            conCQ.UIType = QuestionUIType.RADIO
                        Case "hidden"
                            conCQ.UIType = QuestionUIType.HIDDEN
                    End Select
                    conCQ.CustomQuestionOptions = cq.CustomQuestionOptions
                    conCQ.Text = cq.Text
                    conCQ.RegExpression = cq.reg_expression
                    If String.IsNullOrEmpty(SafeString(conCQ.isRequired)) Then
                        If (cq.IsRequired) Then
                            conCQ.isRequired = "Y"
                        Else
                            conCQ.isRequired = "N"
                        End If

                    End If

                    xaConQuestions.Add(conCQ)
                End If
            Next
        Next
        Return xaConQuestions
    End Function
    Protected Sub autoFillForm()
        ' Fill the form with test values

    End Sub

    Protected Sub testApplicationSubmission()
        ' Define the name and type of the client scripts on the page. 
        Dim csname1 As [String] = "testApplicationScript"
        Dim cstype As Type = Me.[GetType]()

        ' Get a ClientScriptManager reference from the Page class. 
        Dim cs As ClientScriptManager = Page.ClientScript

        ' Check to see if the startup script is already registered. 
        If Not cs.IsStartupScriptRegistered(cstype, csname1) Then
            Dim cstext1 As New StringBuilder()
            cstext1.Append("<script type=text/javascript> submitTestData() </script>")
            cs.RegisterStartupScript(cstype, csname1, cstext1.ToString())
        End If
    End Sub

    ''Protected Function GetAllowedProductCodes() As List(Of String)
    ''    Dim ProductCodes As New List(Of String)
    ''    Dim oNodes As XmlNodeList = _CurrentWebsiteConfig._webConfigXML.SelectNodes("XA_LOAN/ACCOUNTS/ACCOUNT")

    ''    If oNodes.Count < 1 Then
    ''        Return ProductCodes
    ''    End If

    ''    For Each child As XmlNode In oNodes
    ''        If child.Attributes("product_code") Is Nothing Then
    ''            logError("product_code is not correct.", Nothing)
    ''        End If
    ''        If child.Attributes("product_code").InnerXml <> "" Then
    ''            ProductCodes.Add(child.Attributes("product_code").InnerXml)
    ''        End If
    ''    Next

    ''    Return ProductCodes

    ''End Function

    Protected Function GetAllowedProductQuestions() As List(Of String)
        Dim ProductQuestions As New List(Of String)
        Dim oNodes As XmlNodeList = _CurrentWebsiteConfig._webConfigXML.SelectNodes("XA_LOAN/ACCOUNT_QUESTIONS/QUESTION")

        If oNodes.Count < 1 Then
            Return ProductQuestions
        End If

        For Each child As XmlNode In oNodes
            If child.Attributes("question_name") Is Nothing Then
                logError("question_name is not correct.", Nothing)
            End If
            If child.Attributes("question_name").InnerXml <> "" Then
                ProductQuestions.Add(child.Attributes("question_name").InnerXml)
            End If
        Next

        Return ProductQuestions

    End Function

    Protected Function GetAllowedCustomQuestionNames() As List(Of String)
        Dim ProductNames As New List(Of String)
        Dim oNodes As XmlNodeList = _CurrentWebsiteConfig._webConfigXML.SelectNodes("XA_LOAN/CUSTOM_QUESTIONS/QUESTION")

        If oNodes.Count < 1 Then
            Return ProductNames
        End If

        For Each child As XmlNode In oNodes
            If child.Attributes("name") Is Nothing Then
                logError("QUESTION name is not correct.", Nothing)
            End If
            If child.Attributes("name").InnerXml <> "" Then
                ProductNames.Add(child.Attributes("name").InnerXml.ToLower)
            End If
        Next
        Return ProductNames

    End Function

    Protected Sub SetVisibleAttribute()
        Dim sAvailbility As String = SafeString(Request.QueryString("type"))
        If sAvailbility = "2" Or sAvailbility = "2a" Or sAvailbility = "2b" Then
            HideFOMPage.Value = "Y"  'existing member
        Else
            HideFOMPage.Value = "N"  'new member so show fom
        End If

        Dim oNodes As XmlNodeList = _CurrentWebsiteConfig._webConfigXML.SelectNodes("XA_LOAN/VISIBLE")
        If oNodes.Count < 1 Then
            Return
        End If

        For Each child As XmlNode In oNodes
            If child.Attributes("fom") Is Nothing Then Return
            If SafeString(child.Attributes("fom").InnerText) = "N" Then
                HideFOMPage.Value = "Y"
            End If
        Next
    End Sub

    Protected Function RenderingPageFOMHeader() As String
        Dim strFomQuestion As String = ""
        'strFomQuestion += "<div data-role='page' id='pageFom' runat ='server' >"
        'strFomQuestion += "<div data-role='header'  data-theme='" & _HeaderDataTheme & "' data-position=''>"
        'strFomQuestion += "     <h1>Membership Eligibility</h1>"
        'strFomQuestion += "</div>"
        'strFomQuestion += "<div data-role='content' data-theme='" & _ContentDataTheme & "'>"
        strFomQuestion += "" & EligibilityMessage()
        strFomQuestion += HeaderUtils.RenderPageTitle(0, "Select your eligibility<span style='display: inline' class='require-span'>*</span>", True)
        'strFomQuestion += "<div><span class='ProductCQ-Font'><br/>Please select your eligibility</span><span style='color:red'>*</span></div>"
        Return strFomQuestion
    End Function

    Protected Function RenderingPageFOMFooter() As String
        Dim strFomQuestion As String = ""
        ''strFomQuestion += "</div><div data-role='footer' data-theme='" & _FooterDataTheme & "'>"
        '' strFomQuestion += "<a href='' style='visibility: hidden;'></a>"
        '' strFomQuestion += "<a href='#' data-icon='arrow-r' data-iconpos='right' data-transition='slide' onclick='validateActivePage(this, true);' "
        ''strFomQuestion += "class='NavIconRight'>Next</a>"
        ''strFomQuestion += "<a href='#divErrorDialog' style='display: none;'></a></div></div>"
        ''new format
        strFomQuestion += "</div><div class='div-continue' data-role='footer' data-theme='" & _ContentDataTheme & "'>"
        strFomQuestion += "<a href='#'  data-transition='slide' onclick='validateActivePage(this, true);' data-theme='" & _FooterDataTheme & "' type='button' class='div-continue-button'>Continue</a>"
        strFomQuestion += "<a href='#divErrorDialog' style='display: none;'></a></div></div>"
        Return strFomQuestion
    End Function

    ''Deprecated
    '  Protected Function RenderServiceNames() As String
    '' Get the children that have the "PRODUCT" tag

    '      Dim accountsXML As New XmlDocument
    '      accountsXML = Common.GetConfigXML()

    '      Dim child_nodes As XmlNodeList = accountsXML.GetElementsByTagName("PRODUCT")

    '      If child_nodes.Count < 1 Then
    '          ' Raise an exception and log the error
    'End If


    '      ' Now we know how many PRODUCTs there are
    '      Dim numProducts As Integer = child_nodes.Count
    '      Dim valueStr As String = ""
    '      Dim serviceNameArray As New List(Of String)
    '      Dim serviceTypeArray As New List(Of String)
    '      Dim serviceCheckedArray As New List(Of String)

    '      For Each child As XmlNode In child_nodes
    '          ' Grab the names of the products
    '          serviceNameArray.Add(child.Attributes("service_name").InnerXml)
    '          serviceTypeArray.Add(child.Attributes("service_type").InnerXml)
    '          serviceCheckedArray.Add(child.Attributes("default").InnerXml)
    '      Next

    '      Dim returnStr As String

    '      ' Now process the rest of the products
    '      'Dim i As Integer
    '      'For i = 0 To numProducts - 1
    '      '    returnStr += "<input type='checkbox' name='services' id='checkbox"
    '      '    returnStr += (i + 1).ToString() + "' value='"
    '      '    returnStr += serviceTypeArray(i)
    '      '    returnStr += RenderServiceXMLCheckState(serviceCheckedArray(i))
    '      '    returnStr += "/>"
    '      '    returnStr += "<label for='checkbox" + (i + 1).ToString() + "'>"
    '      '    returnStr += 
    '      '                        (ODP) OverDraft Protection</label>
    '      'Next

    '      Return returnStr
    '  End Function

    ''Deprecated
    'Protected Function RenderServiceXMLCheckState(ByVal checkedState As String) As String
    '    If (checkedState.ToUpper() = "Y") Then
    '        Return "checked='checked' "
    '    Else
    '        Return ""
    '    End If
    'End Function
    ''Deprecated
    'Protected Function GetMinAmountDollarStr() As String
    '    ' Check the XML config document to see if the selected account has a minimum 
    '    ' deposit requirement.
    '    Return FormatCurrency(_MinDepositAmount.ToString())
    'End Function
    ''Deprecated
    'Public Function GetMinDepositValue() As Integer
    '    Return _MinDepositAmount
    'End Function
    ''Deprecated
    'Protected Function LoadTemplateXML() As String
    '    Dim sb As New StringBuilder()

    '    Dim writer As XmlWriter = XmlTextWriter.Create(sb)
    '    writer.WriteStartDocument()
    '    ' begin input
    '    writer.WriteStartElement("INPUT")

    '    ' begin login
    '    writer.WriteStartElement("LOGIN")
    '    writer.WriteAttributeString("login", "andtester")
    '    writer.WriteAttributeString("password", "an@test498")
    '    writer.WriteEndElement()
    '    ' end login

    '    ' begin request
    '    writer.WriteStartElement("REQUEST")
    '    writer.WriteAttributeString("action_type", "NEW")

    '    writer.WriteStartElement("LOAN_DATA")
    '    writer.WriteAttributeString("loan_type", "XA")
    '    Dim oLoanApp As LoanAppInfo = CreateLoanApp()
    '    writer.WriteCData(oLoanApp.ToXmlString())
    '    writer.WriteEndElement()
    '    ' end request
    '    writer.WriteEndElement()
    '    ' end input
    '    writer.WriteEndElement()
    '    writer.WriteEndDocument()
    '    writer.Close()
    '    Return sb.ToString()
    'End Function

    Protected Function IsSubmitted(ByVal responseXml As String) As Boolean
        Dim xmlDoc As New XmlDocument()

        xmlDoc.LoadXml(responseXml)

        If xmlDoc.GetElementsByTagName("ERROR").Count > 0 Then
            Return False
        End If
        Return True
    End Function

	'deprecate
	''' <summary>
	''' create loan app from posting data
	''' </summary>
	''' <returns></returns>
	''' <remarks></remarks>
	'  Protected Function CreateLoanApp() As LoanAppInfo
	'      Dim oLoanApp As New LoanAppInfo

	'      '' load personal info
	'      Dim _personalInfo As New PersonalInfo
	'      _personalInfo.FirstName = Request.Form("FirstName")
	'      _personalInfo.LastName = Request.Form("LastName")
	'      _personalInfo.MiddleName = Request.Form("MiddleName")
	'      _personalInfo.Gender = Request.Form("Gender")
	'      _personalInfo.Suffix = Request.Form("NameSuffix")
	'      _personalInfo.DateOfBirth = Request.Form("DOB")
	'      'If selectChoiceYear.Value <> "0" AndAlso selectChoiceMonth.Value <> "0" AndAlso selectChoicDay.Value <> "0" Then
	'      '    _personalInfo.DateOfBirth = selectChoiceYear.Value & "-" & selectChoiceMonth.Value & "-" & selectChoicDay.Value
	'      'End If

	'      _personalInfo.JobTitle = Request.Form("Profession")
	'      _personalInfo.Citizenship = Request.Form("Citizenship")
	'      _personalInfo.SSN = Request.Form("SSN")

	'      oLoanApp.oPersonal = _personalInfo

	'      ' load contact info
	'      Dim _contact As New ContactInfo
	'      _contact.CellPhone = Request.Form("MobilePhone")
	'      _contact.Email = Request.Form("EmailAddr")
	'      _contact.HomePhone = Request.Form("HomePhone")
	'      _contact.PreferredContactMethod = Request.Form("ContactMethod")
	'      _contact.WorkPhone = Request.Form("WorkPhone")
	'      _contact.WorkPhoneExt = Request.Form("WorkPhoneEXT")

	'      oLoanApp.oContact = _contact

	'      ' load identification info
	'      Dim _id As New IdentificationInfo
	'      _id.CardNumber = Request.Form("IDCardNumber")
	'      _id.CardType = Request.Form("IDCardType")
	'      _id.Country = Request.Form("IDCountry")
	'      _id.DateIssued = Request.Form("IDDateIssued")
	'      _id.ExpirationDate = Request.Form("IDDateExpire")
	'      _id.State = Request.Form("IDState")
	'      oLoanApp.oIDCard = _id

	'      '' load address info
	'      Dim _address As New AddressInfo
	'      _address.Address = Request.Form("AdressStreet")
	'      _address.City = Request.Form("AddressCity")
	'      _address.State = Request.Form("AddressState")
	'      _address.Zip = Request.Form("AddressZip")

	'      oLoanApp.oAddress = _address

	''loadn Product info
	'Dim acc1 As New ProductInfo()
	'Dim acc2 As New ProductInfo()
	'Dim acc3 As New ProductInfo()

	'      'acc1.Amount = SafeDouble(hdAmount1.Value)
	'      'acc2.Amount = SafeDouble(hdAmount2.Value)
	'      'acc3.Amount = SafeDouble(hdAmount3.Value)

	'      acc1.Amount = SafeDouble(Request.Form("Account1Deposit"))
	'      acc2.Amount = SafeDouble(Request.Form("Account2Deposit"))
	'      acc3.Amount = SafeDouble(Request.Form("Account3Deposit"))

	'      acc1.Product = Request.Form("Account1Name")
	'      acc2.Product = Request.Form("Account2Name")
	'      acc3.Product = Request.Form("Account3Name")

	'oLoanApp.oProduct1 = acc1
	'oLoanApp.oProduct2 = acc2
	'oLoanApp.oProduct3 = acc3

	'      Return oLoanApp
	'  End Function

	Private Function SafeString(ByVal obj As Object) As String
		If obj Is Nothing Then
			Return ""
		End If
		Return obj.ToString()
	End Function

#Region "security"
	''' <summary>
	''' Checks to make sure that the incoming hashed token is equal to hash(WEBSITE@authKey + Date + Hour)
	''' </summary>
	''' <remarks>
	''' Originally copied from Common.vb
	''' </remarks>
	''' <param name="psHasedToken"></param>
	''' <param name="poConfig"></param>
	''' <returns></returns>
	Public Function IsValidToken(ByVal psHasedToken As String, ByVal poConfig As CWebsiteConfig) As Boolean
		If poConfig.authKey = "" Or psHasedToken = "" Then Return False

		Dim sKey As String = poConfig.authKey & Now.Day & Now.Hour

		' Note: AP-1132 MD5 is in the process of being deprecated. The code here will be changed to allow both MD5 and SHA1.
		' We will notify ACCU to switch to SHA1 after the 1.6.0 release. In 1.7.0, we will remove the MD5 algorithm here.
		For Each hashAlgorithm In New System.Security.Cryptography.HashAlgorithm() {New System.Security.Cryptography.MD5CryptoServiceProvider(), New System.Security.Cryptography.SHA1CryptoServiceProvider()}
			If getOneWayHashValue(sKey, hashAlgorithm) = psHasedToken Then Return True

			'account for timezone different +3 or -3
			For i As Integer = -3 To 3 Step 1
				Dim sKeyZone As String = poConfig.authKey & Now.Day & Now.Hour + i
				If getOneWayHashValue(sKeyZone, hashAlgorithm) = psHasedToken Then Return True
			Next
		Next

		log.WarnFormat("Fail authkey.  Expected {0}.  Received {1}", getOneWayHashValue(sKey, New System.Security.Cryptography.SHA1CryptoServiceProvider()), psHasedToken)
		Return False

	End Function
	Public Function getOneWayHashValue(ByVal psKey As String, ByVal hashAlgorithm As System.Security.Cryptography.HashAlgorithm) As String
		Dim sHashed As String = Nothing

		Dim oBytes As Byte() = System.Text.Encoding.ASCII.GetBytes(psKey.ToUpper())
		Dim oHashed As Byte() = hashAlgorithm.ComputeHash(oBytes)
		Dim sBase64 As String = Replace(Convert.ToBase64String(oHashed), "+", ";") 'the plus sign chokes when user clicks on the URL			

		sHashed = Left(sBase64, 22)  'chop off padding chars	
		Return sHashed
	End Function
#End Region

End Class

'deprecated
'Public Class LoanAppInfo
'	'Inherits CBasePage

'	'Public oPersonal As PersonalInfo
'	'Public oContact As ContactInfo
'	'Public oIDCard As IdentificationInfo
'	'Public oAddress As AddressInfo
'	'Public oProduct1 As ProductInfo
'	'Public oProduct2 As ProductInfo
'	'Public oProduct3 As ProductInfo

'	'Private Function FormatPhone(ByVal src As String) As String
'	'	Dim sb As New StringBuilder
'	'	Dim count As Integer = 0
'	'	For Each c As Char In src
'	'		If Char.IsDigit(c) Then
'	'			sb.Append(c)
'	'			count += 1
'	'		End If

'	'		If count = 10 Then
'	'			Exit For
'	'		End If
'	'	Next
'	'	If count < 10 Then
'	'		Return ""
'	'	End If
'	'	Return sb.ToString()
'	'End Function

'	'Private Function FormatPhoneExt(ByVal src As String) As String
'	'	Dim sb As New StringBuilder
'	'	Dim count As Integer = 0
'	'	For Each c As Char In src
'	'		If Char.IsDigit(c) Then
'	'			sb.Append(c)
'	'			count += 1
'	'		End If

'	'		If count = 4 Then
'	'			Exit For
'	'		End If
'	'	Next

'	'	If count < 4 Then
'	'		Return ""
'	'	End If

'	'	Return sb.ToString()
'	'End Function

'	'depreacted
'	'Public Function ToXmlString() As String
'	'	Dim sb As New StringBuilder
'	'	Dim writer As XmlWriter = XmlTextWriter.Create(sb)

'	'	writer.WriteStartDocument()
'	'	writer.WriteStartElement("XPRESS_LOAN", "http://www.meridianlink.com/CLF")

'	'	writer.WriteAttributeString("xmlns", "xsi", Nothing, "http://www.w3.org/2001/XMLSchema-instance")
'	'	writer.WriteAttributeString("xsi", "schemaLocation", Nothing, "http://www.meridianlink.com/CLF C:\LoansPQ2\CLF\xpress_loan.xsd http://www.meridianlink.com/CLF C:\LoansPQ2\CLF\Integration\SSFCU.xsd")
'	'	writer.WriteAttributeString("version", "5.069")


'	'	'' begin applicants
'	'	writer.WriteStartElement("APPLICANTS")
'	'	'' begin applicant
'	'	writer.WriteStartElement("APPLICANT")

'	'	writer.WriteAttributeString("first_name", oPersonal.FirstName)
'	'	writer.WriteAttributeString("middle_name", oPersonal.MiddleName)
'	'	writer.WriteAttributeString("last_name", oPersonal.LastName)
'	'	writer.WriteAttributeString("suffix", oPersonal.Suffix)
'	'	writer.WriteAttributeString("gender", oPersonal.Gender)
'	'	If Not String.IsNullOrEmpty(oPersonal.DateOfBirth) Then
'	'		writer.WriteAttributeString("dob", oPersonal.DateOfBirth)
'	'	End If

'	'	writer.WriteAttributeString("mother_maiden_name", oPersonal.MotherMaidenName)
'	'	writer.WriteAttributeString("ssn", oPersonal.SSN)
'	'	writer.WriteAttributeString("email", oContact.Email)

'	'	writer.WriteAttributeString("home_phone", FormatPhone(oContact.HomePhone))
'	'	writer.WriteAttributeString("cell_phone", FormatPhone(oContact.CellPhone))
'	'	writer.WriteAttributeString("work_phone", FormatPhone(oContact.WorkPhone))
'	'	writer.WriteAttributeString("work_phone_extension", FormatPhoneExt(oContact.WorkPhoneExt))

'	'	writer.WriteAttributeString("preferred_contact_method", oContact.PreferredContactMethod)

'	'	'' current address
'	'	writer.WriteStartElement("CURRENT_ADDRESS")
'	'	writer.WriteAttributeString("occupancy_status", "")
'	'	writer.WriteAttributeString("occupancy_duration", "0")
'	'	writer.WriteStartElement("LOOSE_ADDRESS")
'	'	writer.WriteAttributeString("street_address_1", oAddress.Address)
'	'	writer.WriteAttributeString("street_address_2", "")
'	'	writer.WriteAttributeString("city", oAddress.City)
'	'	writer.WriteAttributeString("state", oAddress.State)
'	'	writer.WriteAttributeString("zip", oAddress.Zip)
'	'	writer.WriteEndElement()
'	'	writer.WriteEndElement()
'	'	'' end current address

'	'	writer.WriteStartElement("PREVIOUS_ADDRESS")
'	'	writer.WriteEndElement()
'	'	writer.WriteStartElement("MAILING_ADDRESS")
'	'	writer.WriteEndElement()

'	'	writer.WriteStartElement("FINANCIAL_INFO")
'	'	writer.WriteAttributeString("occupation", oPersonal.JobTitle)
'	'	writer.WriteEndElement()

'	'	writer.WriteStartElement("ID_CARD")
'	'	writer.WriteAttributeString("card_type", oIDCard.CardType)
'	'	writer.WriteAttributeString("card_number", oIDCard.CardNumber)
'	'	writer.WriteAttributeString("state", oIDCard.State)
'	'	writer.WriteAttributeString("country", oIDCard.Country)

'	'	If Not String.IsNullOrEmpty(oIDCard.DateIssued) Then
'	'		writer.WriteAttributeString("date_issued", oIDCard.DateIssued)
'	'	End If

'	'	If Not String.IsNullOrEmpty(oIDCard.ExpirationDate) Then
'	'		writer.WriteAttributeString("exp_date", oIDCard.ExpirationDate)
'	'	End If
'	'	writer.WriteEndElement()
'	'	writer.WriteEndElement()
'	'	'' end applicant

'	'	' TODO: add spouse xml here
'	'	writer.WriteEndElement()
'	'	'' end applicants

'	'	'' begin new xml writer
'	'	writer.WriteStartElement("COMMENTS")
'	'	writer.WriteEndElement()

'	'	writer.WriteStartElement("LOAN_INFO")
'	'	writer.WriteAttributeString("status", "NEW")
'	'	writer.WriteStartAttribute("create_date")
'	'	writer.WriteValue(DateTime.Now)
'	'	writer.WriteEndAttribute()
'	'	writer.WriteEndElement()

'	'	writer.WriteStartElement("CUSTOM_QUESTIONS")
'	'	writer.WriteEndElement()

'	'	writer.WriteStartElement("SYSTEM")
'	'	writer.WriteAttributeString("source", "GATEWAY")
'	'	writer.WriteAttributeString("type", "LPQ")
'	'	writer.WriteAttributeString("external_source", "TBL")
'	'	writer.WriteAttributeString("loan_number", "")
'	'	writer.WriteAttributeString("loan_id", "")
'	'	writer.WriteStartElement("LENDER")

'	'	'writer.WriteAttributeString("id", "e4e6624600b7441e8eee00eac47f11d4")
'	'	writer.WriteAttributeString("id", _CurrentLenderId)
'	'	writer.WriteEndElement()
'	'	writer.WriteStartElement("ORGANIZATION")
'	'	'writer.WriteAttributeString("id", "11842847ef2c47c3a8a029fa20d3ff19")
'	'	writer.WriteAttributeString("id", _CurrentOrgId)
'	'	writer.WriteEndElement()
'	'	writer.WriteEndElement()

'	'	''adding Product here
'	'	writer.WriteStartElement("INTERESTED_ACCOUNTS")
'	'	writer.WriteRaw(oProduct1.ToXmlString())
'	'	writer.WriteRaw(oProduct2.ToXmlString())
'	'	writer.WriteRaw(oProduct3.ToXmlString())
'	'	writer.WriteEndElement()

'	'	writer.WriteStartElement("FUNDING_SOURCES")
'	'	writer.WriteEndElement()

'	'	writer.WriteStartElement("FOM_ANSWERED_QUESTIONS")
'	'	writer.WriteEndElement()

'	'	' end xpress loan
'	'	writer.WriteEndElement()

'	'	' end document
'	'	writer.WriteEndDocument()
'	'	writer.Close()

'	'	Return sb.ToString()
'	'End Function
'End Class

'deprecated, move to callback
'Public Class PersonalInfo
'	Public FirstName As String
'	Public MiddleName As String
'	Public LastName As String
'	Public Suffix As String
'	Public Gender As String
'	Public DateOfBirth As String
'	Public MotherMaidenName As String
'	Public JobTitle As String
'	Public Citizenship As String
'	Public SSN As String

'End Class

'Public Class ContactInfo
'	Public Email As String
'	Public HomePhone As String
'	Public CellPhone As String
'	Public WorkPhone As String
'	Public WorkPhoneExt As String
'	Public PreferredContactMethod As String
'End Class

'Public Class IdentificationInfo
'	Public CardType As String
'	Public CardNumber As String
'	Public DateIssued As String
'	Public ExpirationDate As String
'	Public State As String
'	Public Country As String
'End Class

'Public Class AddressInfo
'	Public Address As String
'	Public Zip As String
'	Public City As String
'	Public State As String
'End Class


'Public Class ProductInfo
'	Public Amount As Double
'	Public Product As String

'	'deprecate
'	'Public Function ToXmlString() As String
'	'	Select Case Product
'	'		Case "not_set"
'	'			Return ""
'	'		Case "12_month"
'	'			Return "<ACCOUNT_TYPE account_name='12 MONTH CD' product_code='C12M' account_type='CD' amount_deposit='1000' rate='3.5' apy='1' term='12'/>"
'	'		Case "checking"
'	'			Return "<ACCOUNT_TYPE account_name='Checking' product_code='DREG01' account_type='CHECKING' amount_deposit='0' rate='3.5' apy='-999999' term='-16959'>" & _
'	'			"<SERVICES> " & _
'	'			 "<SERVICE description='OverDraft Protection' service_type='OD_PROT' service_code='ODP'/> " & _
'	'			 "<SERVICE description='Debit Card' service_type='ATMDEBIT' service_code='DC'/> " & _
'	'			 "<SERVICE description='eStatements' service_type='' service_code='EST'/> " & _
'	'			"</SERVICES> </ACCOUNT_TYPE>"

'	'		Case "hsa"
'	'			Return "<ACCOUNT_TYPE account_name='HSA' product_code='HSAC01' account_type='HSA' amount_deposit='1234' rate='-999999' apy='-999999' term='-16959'/>"

'	'		Case "money_market"
'	'			Return "<ACCOUNT_TYPE account_name='High Interest Money Market' product_code='MM12' account_type='MONEY_MARKET' amount_deposit='500' rate='3.2' apy='3' term='-16959'/>"
'	'		Case "ira_cd"
'	'			Return "<ACCOUNT_TYPE account_name='IRA CD' product_code='IRA12' account_type='IRACD' amount_deposit='600' rate='-999999' apy='-999999' term='-16959'/>"

'	'		Case "personal_saving"
'	'			Return "<ACCOUNT_TYPE account_name='PERSONAL SAVINGS' product_code='SPER01' account_type='SAVINGS' amount_deposit='2133' rate='-999999' apy='-999999' term='-16959'/>"
'	'		Case "senior_checking"
'	'			Return "<ACCOUNT_TYPE account_name='SENIOR CHECKING' product_code='DSEN01' account_type='CHECKING' amount_deposit='4444' rate='-999999' apy='-999999' term='-16959'/>"
'	'		Case Else
'	'			Return ""
'	'	End Select

'	'End Function
'End Class

