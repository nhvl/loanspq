﻿var isSubmitWalletAnswer = false;
var isSubmittingXA = false;
var acc1 = {};  //store products per account
var acc2 = {};
var acc3 = {};
//var selectedAccountsArray = new Array(); //store 3 account
//var selectedAccountsCount = 0;
var currentAccount = 1; //was
var currentlySelectedAccount = 1;  //is
//var numAccounts = -1; //TODO?  not used
// accountsArray will be an array of accountInfo objects
var accountsArray = new Array();  //store 3 account
var productsArray = new Array(); //ex:checking, saving
var numWalletQuestions = 0;
var isApplicationCompleted = false;
var EMPLogicPI = {};
var EMPLogicJN = {};
var EMPLogicMR = {}; //for Minor App
var EMPLogicPI_PREV = {};
var EMPLogicJN_PREV = {};
var EMPLogicMR_PREV = {}; //for Minor App
var docScanObj, co_docScanObj;
// This function is used to create an AccountInfo object
function accountInfo(accountNum, productName, minDepositAmt, userDepositAmt) {
    this.accountNum = accountNum;
    this.productName = productName;
    this.minDepositAmt = minDepositAmt;
    this.userDepositAmt = userDepositAmt;
}

// This is used to create a productInfo object that stores information about each product 
function productInfo(productName, minDepositAmt, isRequired) {
    this.productName = productName;
    this.minDepositAmt = minDepositAmt;
    this.isRequired = isRequired;
}

function getParameterByName(name) {
    //name = name.replace(/[\[]/, "\\\[").replace(/[\]]/, "\\\]");
    //var regexS = "[\\?&]" + name + "=([^&#]*)";
    //var regex = new RegExp(regexS);
    //var results = regex.exec(window.location.search);
    //if (results == null)
    //    return "";
    //else
	//    return decodeURIComponent(results[1].replace(/\+/g, " "));
	if (qDATA != null && typeof qDATA[name] !== "undefined") {
		return qDATA[name];
	}
	return "";
}


function loadFromAccount() {
    var acc = acc1;
    if (currentAccount == 2) {
        acc = acc2;
    }
    else if (currentAccount == 3) {
        acc = acc3;
    }

    $("input[name='product']").each(function () {
        if (acc.product == $(this).val())
            $(this).attr('checked', true).checkboxradio("refresh");
        else
            $(this).attr('checked', false).checkboxradio("refresh");
    });

    if (acc.product == 'checking') {
        $('#divServices').show();
        $("input[name='services']").each(function () {
            if (acc.services.indexOf($(this).val() + '|') > 0)
                $(this).attr('checked', true).checkboxradio("refresh");
            else
                $(this).attr('checked', false).checkboxradio("refresh");
        });
    }
    else {
        $('#divServices').hide();
    }
}
/* remove old format
function saveToHidden() {
    //noneed
   if (currentAccount == 1)
        acc1.amount = $('#depositamount').val();
    if (currentAccount == 2)
        acc2.amount = $('#depositamount').val();
    if (currentAccount == 3)
        acc3.amount = $('#depositamount').val();

    $('#hdAccount1').val(acc1.product);
    $('#hdAmount1').val(acc1.amount);

    $('#hdAccount2').val(acc2.product);
    $('#hdAmount2').val(acc2.amount);

    $('#hdAccount3').val(acc3.product);
    $('#hdAmount3').val(acc3.amount);
   
} */
///////////////////             TURNED OFF FOR DEBUGGING
//deprecated not use any where
function handlePageRefresh() {
    if (hasFOM) {
        nextpage = $('#pageFom');
    } else {
        nextpage = $('#page1');
    }
    $.mobile.changePage(nextpage, {
        transition: "slide",
        reverse: false,
        changeHash: true
    });
}
//may have been deprecated
//1=primary, 2=secondary
function setAvailability() {
    //set the account type based on query string "type"
    var availability = getParameterByName("type");

    //by default value of hdAvailability already set to '1', so only check for secondary 
    if (availability == '2' || availability == '2a' || availability == "2b") {
        $('#hdAvailability').val('2');
    }
}
//execute after DOM has been initialized, but before fully rendered
$(function () {
   //need to initial products in productArray
    readInitialAccountInfo();
 
    //minor employment status
    EMPLogicMR = $.extend(true, {}, EMPLogic);
    EMPLogicMR.init('m_ddlEmploymentStatus', 'm_', 'pageMinorInfo', 'm_divEmploymentStatusRest');
    $('#pageMinorInfo').on("pageshow", function () {
        EMPLogicMR.refreshHtml();
    });
    EMPLogicMR_PREV = $.extend(true, {}, EMPLogic);
    EMPLogicMR_PREV.init('m_prev_ddlEmploymentStatus', 'm_prev_', 'pageMinorInfo', 'm_prev_divEmploymentStatusRest');
    $('#pageMinorInfo').on("pageshow", function () {
    	EMPLogicMR_PREV.refreshHtml();
    });


    EMPLogicPI = $.extend(true, {}, EMPLogic);
    EMPLogicPI.init('ddlEmploymentStatus', '', 'page2', 'divEmploymentStatusRest');
    $('#page2').on("pageshow", function () {
        EMPLogicPI.refreshHtml();
    });
    EMPLogicPI_PREV = $.extend(true, {}, EMPLogic);
    EMPLogicPI_PREV.init('prev_ddlEmploymentStatus', 'prev_', 'page2', 'prev_divEmploymentStatusRest');
    $('#page2').on("pageshow", function () {
    	EMPLogicPI_PREV.refreshHtml();
    });

    
    EMPLogicJN = $.extend(true, {}, EMPLogic);
    EMPLogicJN.init('co_ddlEmploymentStatus', 'co_', 'page6', 'co_divEmploymentStatusRest');
    $('#page6').on("pageshow", function () {
        EMPLogicJN.refreshHtml();
    });
    EMPLogicJN_PREV = $.extend(true, {}, EMPLogic);
    EMPLogicJN_PREV.init('co_prev_ddlEmploymentStatus', 'co_prev_', 'page6', 'co_prev_divEmploymentStatusRest');
    $('#page6').on("pageshow", function () {
    	EMPLogicJN_PREV.refreshHtml();
    });
    
    //ShowProductCQ(); //default
    //displayProductAmountTitle();
    //show and hide deposit text field
    //ShowAndHideDepositTextFields();
    //xaProductFactory.toggleDepositTextFields();
    $('#txtTotalDeposit').val(Common.FormatCurrency(getTotalDeposit(), true));
    //refresh check box
    //handleEnableJointXA();
    //***start --handle arrow up and down inside checkbox button ->show description of the product
    //$('.ArrowUp').click(function () {
    //    var DescriptionId = 'Description' + $(this).attr('id').replace("ArrowUp", "");
    //    var ArrowDownId = $(this).attr('id').replace("ArrowUp", "ArrowDown");
    //    $('#' + $(this).attr('id')).hide();
    //    $('#' + ArrowDownId).show();
    //    $('#' + DescriptionId).hide();
      
    //});
    //$('.ArrowDown').click(function () {
    //    var DescriptionId = 'Description' + $(this).attr('id').replace("ArrowDown", "");
    //    var ArrowUpId = $(this).attr('id').replace('ArrowDown', 'ArrowUp');
    //    var firsChildElement = $('#' + ArrowUpId).children().first();
    //    var cssClass = firsChildElement.attr('class');
    //    if (cssClass != undefined) {
    //        if (cssClass.indexOf("ui-corner-bottom") != -1) {
    //          firsChildElement.removeClass("ui-corner-bottom");
    //        }
    //    }
    //    $('#' + $(this).attr('id')).hide();
    //    $('#' + ArrowUpId).show();
    //    //make sure select product has description
    //    if ($('#' + DescriptionId).text() != "" && $('#' + DescriptionId).text() != undefined) {
    //        $('#' + DescriptionId).show();
    //        if ($('#Compare').length != 0) {
    //            $('#Compare').hide(); //''this for lenderref =wscu product description
    //        }
    //    } else {
    //        $('#' + DescriptionId).hide();
    //    }
    //});
    //***end -- handle arrow up and down inside checkbox button ->show description of the product
    //need to add product questions of pre-selected products(required products) to ProductCQ.Questions
    //$('.selectProduct').each(function () {
    //    var element =$(this);
    //    var productID = element.attr('id');
    //    var currentKey = productID.replace("chk", "");
    //    var NumOfQuestions = Number($('#divProductQuestion_' + productID).attr('question_count'));
    //    var isChecked = $('#' + productID).is(':checked');
    //    var isPreSelected = element.attr('pre_select');
    //    if (isPreSelected == 'Y' && isChecked) {
    //        if (NumOfQuestions > 0) {
    //            ProductCQ.Questions.push(new ProductCQ.buildDataForProduct(currentKey, _ProductQuestionPackage));
    //        }
    //    }
    //});
    ////display a link inside check box label
    //$('.selectProduct').click(function () {
    //    var productID = $(this).attr('id');
    //    $("#Description-" + productID + " a").click(function () {
    //        var productURL = $(this).attr('href'); // will display a link if this product has a link 
    //        if (productURL != null && productURL != undefined) {
    //            window.open(productURL);
    //        }
    //    });
    //});

    //$('.selectProduct').on('change', function (event, ui) {
    //    var productID = event.target.id;
    //    var currentKey = productID.replace("chk", "");
    //    var isChecked = $('#' + productID).is(':checked');

    //    toogleDisplayProductService(currentKey, isChecked);

    //    //show and hide questions when check and uncheck product
    //    var NumOfQuestions = Number($('#divProductQuestion_' + productID).attr('question_count'));
    //    if (isChecked) {
    //        //required products ->preselected and disabled only one product
    //       handleRequiredProductChange(productID);
    //        var isRequired = "";
    //        //hide product question
    //        if (NumOfQuestions == 0) { //there is no product questions
    //            $('#divProductQuestion_' + productID).hide();
    //        } else {
    //            //has custom question
    //            hasPCQ = true;
    //            //show product quesion
    //            $('#divProductQuestion_' + productID).show();
    //            if (NumOfQuestions == 1) {
    //                isRequired = $('#question_1_' + productID).attr('question_required');
    //                if (isRequired == 'Y') { //display the red star for required product
    //                    $('#reqQuestion_1_' + productID).show();
    //                } else {
    //                    $('#reqQuestion_1_' + productID).hide();
    //                }
    //            }
    //            else if (NumOfQuestions > 1) //multiple questions in each products
    //            {
    //                for (var i = 1 ; i <= NumOfQuestions; i++) {
    //                    isRequired = $('#question_' + i + '_' + productID).attr('question_required');
    //                    if (isRequired == 'Y') { //display the red star for required product
    //                        $('#reqQuestion_' + i + '_' + productID).show();
    //                    } else {
    //                        $('#reqQuestion_' + i + '_' + productID).hide();
    //                    }
    //                }
    //            }
    //        }
    //        /*new format user can select more than 3 products
    //        // maximum three products
    //        if ($('.selectProduct:checked').length <= 3 && $('.selectProduct:checked').length >= 0) {
    //            //add the object to the ProductCQ.Product array
    //            var userDepositAmount = $('#txtDepositAmount_' + productID).val();
    //            if (NumOfQuestions > 0) {
    //                ProductCQ.Questions.push(new ProductCQ.buildDataForProduct(currentKey, _ProductQuestionPackage));
    //            }
    //        }*/
            
    //        if (NumOfQuestions > 0) {
    //            ProductCQ.Questions.push(new ProductCQ.buildDataForProduct(currentKey, _ProductQuestionPackage));
    //        }
    //    } else { //uncheck product, just hide question
    //        $('#divProductQuestion_' + productID).hide();
    //        //unchecked product custom question
    //        for (var j = 0; j < ProductCQ.Questions.length; j++) {
    //            if (ProductCQ.Questions[j].getProductName == currentKey) {
    //                if (NumOfQuestions > 0) {
    //                    ProductCQ.Questions.splice(j, 1);
    //                    break;
    //                }
    //            }
    //        }
    //        $('#divProductQuestionTitle').hide();
    //    } //end show and hide ....
    //    var hasPCQ = false;
    //    $('.selectProduct').each(function () {
    //        if ($(this).is(':checked')) {
    //            var productID = $(this).attr('id');
    //            if (Number($('#divProductQuestion_' + productID).attr('question_count')) > 0) {
    //                hasPCQ = true;
    //                return false; //break the loop
    //            }
    //        }
    //    });
    //    //show and hide product question titles
    //    if (hasPCQ) {
    //        $('#divProductQuestionTitle').show();
    //    } else {
    //        $('#divProductQuestionTitle').hide();
    //    }
    //});

    $('#txtTotalDeposit').click(function () {
        $('#txtTotalDeposit').val(Common.FormatCurrency(getTotalDeposit(), true));
    });
    //update total deposit amount when required intial product amount changes
   /* $('#DivDepositInput').on('change', function (event, ui) {
         var selectedID = event.target.id;
        $('#' + selectedID).blur(function () {
            $('#txtTotalDeposit').val(Common.FormatCurrency(getTotalDeposit(), true));
            FS.refreshDivContent();
    });*/
    
   
    //$('.OccupancyDuration').change(function () {
    //    ShowPreviousAddress();
    //});
    //$('.co_OccupancyDuration').change(function () {
    //    //show the previous address based on the occupancy duration 
    //    co_ShowPreviousAddress();
    //});
  
    //show and hide mailing address fom
    //handledShowAndHideMailingAddress(""); //mailing address for primary
    //handledShowAndHideMailingAddress("co_"); //mailing address for CoApplicant
    //handledShowAndHideMailingAddress("m_"); //mailing address for Minor Applicant
    //hide previous address for minor
    //m_ShowPreviousAddress();
    // Comment out this because of double event handler regist
    //$(".label-fom-question a").click(
    //      function () {
    //          var formQuestionURL = $(this).attr('href');
    //          if (formQuestionURL != null && formQuestionURL != undefined) {
    //              window.open(formQuestionURL); // will display a link if this text has a href attrtibute  
    //          }
    //      });

    //end new format///////////////////////////////////
    setAvailability(); 
    acc1.amount = 0;
    acc1.product = 'not_set';
    acc1.services = "|ODP|EST|";
    acc2.amount = 0;
    acc2.product = 'not_set';
    acc2.services = "|ODP|EST|";
    acc3.amount = 0;
    acc3.product = 'not_set';
    acc3.services = "|ODP|EST|";
    currentAccount = 1;
    loadFromAccount();
    ///////////////////             TURNED OFF FOR DEBUGGING
    // window.onbeforeunload = handlePageRefresh;
    //if the Membership Eligibility is the first page, when clicking to the next page(open membership page) some functions 
    // Unbind the events for things that are locked (such as required accounts)
    $('.requiredAccountCollapsible').bind('expand', function () {
        //                alert('Expanded');
    }).bind('collapse', function () {
        //                alert('Collapsed');
        $(this).trigger('expand');
    });
    // =================================================================
    // Bind to the product selection radio buttons, don't want to execute account selection(execute after handleAccountChange cause conflict)
    // =================================================================F

    //need to update this function
    $("input[name='AccountRadio']").on('change',
            function (event, ui) {
                //            	alert(event.target.id);

                var sourceObjectID = event.target.id;
                // We want to check the minimum balance and set that in the deposit input box
                // First, check to see if the radio has selected a valid product code
                var numProducts = $('#hdNumProducts').val();
                numProducts = parseInt(numProducts, 10);
                for (var i = 1; i <= numProducts; i++) {
                    accountsArray[currentlySelectedAccount].productName = $('input[name=AccountRadio]:checked').attr('id');

                    //noneed
                    /*
                        //reset prodcut select
                        if (sourceObjectID == "NOT SET") {
                            accountsArray[currentlySelectedAccount].productName = $('input[name=AccountRadio]:checked').attr('id');
                            break;
                        }
    
                     if (sourceObjectID == productsArray[i].productName) {
                            // Found the matching product code
                            // If the deposit amount is less than the minimum, then set the minimum
                            var depositAmt = $('#depositAmount').val();
                            var minDepositAmt = productsArray[i].minDepositAmt;
                            minDepositAmt = parseInt(minDepositAmt, 10);
                            depositAmt = String(depositAmt).replace(/[^\d.]/g, "");
                            depositAmt = parseInt(depositAmt, 10);
                            if (depositAmt < minDepositAmt) {
                               $('#depositAmount').prop('value', "$" + Common.FormatCurrency(minDepositAmt));
                            }
                            accountsArray[currentlySelectedAccount].userDepositAmt = $('#depositAmount').val();
                            // Get the id of the selected product
                            var selectedProduct = $('input[name=AccountRadio]:checked').attr('id'); 
                            accountsArray[currentlySelectedAccount].productName = selectedProduct;
                            $('#hdAmount' + currentlySelectedAccount).val(accountsArray[currentlySelectedAccount].userDepositAmt);
                            $('#hdAccount' + currentlySelectedAccount).val(accountsArray[currentlySelectedAccount].productName);
                            //keep track min Deposit for each product.
                            // Therefore, we need to update deposit amount input when user selects another product
                            minDepositAmt = productsArray[i].minDepositAmt;
                            minDepositAmt = parseInt(minDepositAmt, 10);
                            if (minDepositAmt < 0) {
                                minDepositAmt = 0.0; // there is no min deposit -> just reset to 0;
                            }
                            $('#depositAmount').prop('value', Common.FormatCurrency(minDepositAmt));
                            break;
                        }*/
                }
            });



    //Bind to deposit text field
    //This is needed bc user may enter a deposit value then hit the next page button
    //If this text box is executed then the user will be required to hit the other button twice, once to defocus and another to execute event
    //replace with handler deposit inoput
    //	$("input[name='depositInput23']").change(function() {
    //		// We want to check the minimum balance and set that in the deposit input box
    //		// First, check to see if the radio has selected a valid product code
    //		var numProducts = $('#hdNumProducts').val();
    //		numProducts = parseInt(numProducts, 10);
    //		for (var i = 1; i <= numProducts; i++) {
    //			if (accountsArray[currentlySelectedAccount].productName == productsArray[i].productName) {
    //				// Found the matching product code
    //				// If the deposit amount is less than the minimum, then set the minimum
    //				var depositAmt = $('#depositAmount').val();
    //				var minDepositAmt = productsArray[i].minDepositAmt;
    //				minDepositAmt = parseInt(minDepositAmt, 10);
    //				depositAmt = String(depositAmt).replace(/[^\d.]/g, "");
    //				depositAmt = parseInt(depositAmt, 10);

    //				if (depositAmt < minDepositAmt) {
    //					$('#depositAmount').prop('value', "$" + Common.FormatCurrency(minDepositAmt));
    //				}

    //				accountsArray[currentlySelectedAccount].userDepositAmt = $('#depositAmount').val();
    //				// Get the id of the selected product

    //				$('#hdAmount' + currentlySelectedAccount).val(accountsArray[currentlySelectedAccount].userDepositAmt);
    //				break;
    //			}
    //		}
    //	});


    // Do this each time the PDF display "page" loads
    //            $('#page1.5').on('pageinit', function(event) {
    //                alert('This page was just enhanced by jQuery Mobile!');
    //            });


    // Apparently, we can pass an array of bytes to the PDFJS class
    // For our purposes, we need to get the data which will be in base64
    // then we convert it to an array of bytes before we pass it to PDFJS.
    //	$('#page1-5').on('pageshow', function(event, ui) {
    //		//                var url = 'http://cdn.mozilla.net/pdfjs/tracemonkey.pdf';
    //		var url = 'PDF_viewer.pdf';
    //		//
    //		// Disable workers to avoid yet another cross-origin issue (workers need the URL of
    //		// the script to be loaded, and dynamically loading a cross-origin script does
    //		// not work)
    //		//
    //		PDFJS.disableWorker = true;

    //		//
    //		// Asynchronous download PDF as an ArrayBuffer
    //		//
    //		PDFJS.getDocument(url).then(function getPdfHelloWorld(pdf) {
    //			//
    //			// Fetch the first page
    //			//
    //			pdf.getPage(1).then(function getPageHelloWorld(page) {
    //				var scale = 1.0;
    //				var viewport = page.getViewport(scale);

    //				//
    //				// Prepare canvas using PDF page dimensions
    //				//
    //				var canvas = document.getElementById('the-canvas');
    //				var context = canvas.getContext('2d');
    //				canvas.height = viewport.height;
    //				canvas.width = viewport.width;

    //				//
    //				// Render PDF page into canvas context
    //				//
    //				page.render({ canvasContext: context, viewport: viewport });
    //			});
    //		});
    //	});		 
    // Fill in the fields with test data
    if (getParameterByName("autofill") == "true") {
        // Fill in the stuff
        autoFillData();
    }

    // Get the initial values for the accounts
    // Record the minimum deposit amounts too.
    //

    // readInitialAccountInfo(); ->> move this function to the top

    // Set the initial values onto the page 

    //setInitialAccountInfo();  remove old format

    /*          // This section implements the mechanism to handle swipe gestures to change pages.
    // It's been disabled for now to prevent confusion.
    $('div.ui-page').on("swipeleft", function() {
    var nextpage = $(this).next('div[data-role="page"]');
    // swipe using id of next page if exists
    // We should validate this page first

	///////////////////             TURNED OFF FOR DEBUGGING
    //////////////                if (validateActivePage(this, false) != "") {
    //////////////                    // An error occurred, so we can't let the user go to the next page.
    //////////////                    // We need to bring up the error dialog.
    //////////////                    nextpage = $('#divErrorDialog');
    //////////////                }

	if (nextpage.length > 0) {
    $.mobile.changePage(nextpage, {
    transition: "slide",
    reverse: false,
    changeHash: true
    });
    }
    });
    $('div.ui-page').on("swiperight", function() {
    // We're not going to do validation checks when the user wants to go back to the previous page.
    // 
    var prevpage = $(this).prev('div[data-role="page"]');
    // swipe using id of next page if exists
    if (prevpage.length > 0) {
    $.mobile.changePage(prevpage, {
    transition: "slide",
    reverse: true,
    changeHash: true
    });
    }
    });
    */

    //bind to the 3 account button , TODO Why redundant
    //$("input[name='account']:checked").val()
    //duplicate of handleAccountChange
    //	$("input[name='account']").change(function() {
    //		if (currentAccount == 1)					//save previous
    //			acc1.amount = $('#depositAmount').val();
    //		if (currentAccount == 2)
    //			acc2.amount = $('#depositAmount').val();
    //		if (currentAccount == 3)
    //			acc3.amount = $('#depositAmount').val();

    //		if ($("input[name='account']:checked").val().indexOf('1') >= 0)  //load current
    //			currentAccount = 1;
    //		if ($("input[name='account']:checked").val().indexOf('2') >= 0)
    //			currentAccount = 2;
    //		if ($("input[name='account']:checked").val().indexOf('3') >= 0)
    //			currentAccount = 3;
    //		loadFromAccount();  //load product for current account
    //	});

    //bind to product button 
    //old code       
    //	$("input[name='product']").change(function() {
    //		if ($("input[name='account']:checked").val().indexOf('1') >= 0)
    //			acc1.product = $("input[name='product']:checked").val();
    //		if ($("input[name='account']:checked").val().indexOf('2') >= 0)
    //			acc2.product = $("input[name='product']:checked").val();
    //		if ($("input[name='account']:checked").val().indexOf('3') >= 0)
    //			acc3.product = $("input[name='product']:checked").val();

    //		if ($(this).val() == 'Checking') {
    //			$('#divServices').show();
    //			$("input[name='services']").each(function() {

    //				var _acc = acc1;
    //				if (currentAccount == 2)
    //					_acc = acc2;
    //				if (currentAccount == 3)
    //					_acc = acc3;
    //				if (_acc.services.indexOf($(this).val() + '|') > 0)
    //					$(this).attr('checked', true).checkboxradio("refresh");
    //				else
    //					$(this).attr('checked', false).checkboxradio("refresh");
    //			});
    //		}
    //		else {
    //			$('#divServices').hide();
    //		}

    //		if ($(this).val() == '12_month' && $(this).is(':checked')
    //                     && $('#depositamount').val() < 1000) {
    //			$('#depositamount').val(1000);
    //		}

    //		if ($(this).val() == 'money_market' && $(this).is(':checked')
    //                     && $('#depositamount').val() < 500) {
    //			$('#depositamount').val(500);
    //		}
    //	});




    $("input[name='services']").change(function () {
        var commbine = "|";
        $("input[name='services']").each(function () {
            if ($(this).is(':checked'))
                commbine += $(this).val() + '|';
        });
        if ($("input[name='account']:checked").val().indexOf('1') >= 0)
            acc1.services = commbine;
        if ($("input[name='account']:checked").val().indexOf('2') >= 0)
            acc2.services = commbine;
        if ($("input[name='account']:checked").val().indexOf('3') >= 0)
            acc3.services = commbine;
    });

    toogleDisplayProductService();

    //hide Restart button on first page. Only happen in XA module
    $("#GettingStarted").find("div.restartbtn-placeholder").hide();
    docScanObj = new LPQDocScan("");
    co_docScanObj = new LPQDocScan("co_");
});
function ScanAccept(prefix) {
	if (prefix == "") {
		docScanObj.scanAccept();
		goToNextPage("#pl2");
	} else {
		co_docScanObj.scanAccept();
		goToNextPage("#pl6");
	}
}

//function handleEnableJointXA() {
//    var isEnableJoint = $('#hdEnableJoint').val();
//    if (isEnableJoint != undefined) {
//        isEnableJoint = isEnableJoint.toUpperCase();
//    }
//    if (isEnableJoint == 'N') {
//        $('#divHasCoApplicant').hide();
//        //also hide all pages for joint   
//        $('#page6').hide();
//        $('#page7').hide();
//        $('#page8').hide();
//        $('#page9').hide();
//        $('#finalreviewpage').hide();
//    }
//}

function handleRequiredProductChange(productID) {
    var isRequiredProduct = $('#' + productID).attr('req_product');
    var isDisabled = document.getElementById(productID).disabled;
    if (isRequiredProduct == "Y") {
        if (!isDisabled) {
            $('.selectProduct').each(function () {
                var element = $(this);
                var isRequired = element.attr('req_product');
                var disabledProduct = document.getElementById(element.attr('id')).disabled;
                if (isRequired == 'Y' && disabledProduct) {
                    //update ProductCQ.Questions array, uncheck and enable required product checkbox 
                    var productKey = element.attr('id').replace("chk", "");
                    for (var j = 0; j < ProductCQ.Questions.length; j++) {
                        if (ProductCQ.Questions[j].getProductName == productKey) {
                            var NumOfQuestions = Number($('#divProductQuestion_' + element.attr('id')).attr('question_count'));
                            if (NumOfQuestions > 0) {
                                ProductCQ.Questions.splice(j, 1);
                            }
                        }
                    }
                    element.attr('checked', false);
                    element.attr('disabled', false).checkboxradio('refresh');
                    $('#divProductQuestion_' + element.attr('id')).hide();

                    toogleDisplayProductService(productKey, false);

                }

            });
            //check and disable required checkbox when user select it
            $('#' + productID).attr('checked', true);
            $('#' + productID).attr('disabled', true).checkboxradio('refresh');
        }
    }
}
function isNumber(n) {
    return !isNaN(parseFloat(n)) && isFinite(n);

}
//need to update this function
function readInitialAccountInfo() {
    var account1SelectedProduct = $('#hdAccount1').val();
    var account2SelectedProduct = $('#hdAccount2').val();
    var account3SelectedProduct = $('#hdAccount3').val();
    var account1Amount = $('#hdAmount1').val();
    var account2Amount = $('#hdAmount2').val();
    var account3Amount = $('#hdAmount3').val();
    // The initial deposit amounts for each account has been set to the minimum if the account is required
    // If the account is not required, then the initial amount does not reflect the minimum amounts
    //            var account1MinDeposit = $('#hdMinAmount1').val();
    //            var account2MinDeposit = $('#hdMinAmount2').val();
    //            var account3MinDeposit = $('#hdMinAmount3').val();

    accountsArray[1] = new accountInfo(1, account1SelectedProduct, account1Amount, account1Amount);
    accountsArray[2] = new accountInfo(2, account2SelectedProduct, account2Amount, account2Amount);
    accountsArray[3] = new accountInfo(3, account3SelectedProduct, account3Amount, account3Amount);

    // Now fill out the information about each product
    var numProducts = $('#hdNumProducts').val();
    numProducts = parseInt(numProducts, 10);
    var minDepositAmount;
    for (var i = 1; i <= numProducts; i++) {
        //make sure deposit amount >=0;
        minDepositAmount = $('#hdMinAmount' + i.toString()).val();
        if (minDepositAmount == '' || minDepositAmount == undefined) {
            minDepositAmount = 0;
        }
        else {
            minDepositAmount = parseFloat(minDepositAmount, 10);
            if (minDepositAmount < 0) {
                minDepositAmount = 0;
            }
        }
        productsArray[i] = new productInfo($('#hdProduct' + i.toString()).val(), minDepositAmount, $('#hdRequiredProduct' + i.toString()).val());
    }

}
//need to update this function
function setInitialAccountInfo() {
    // Set the initial state for the first page
    // If the first account is required, then select it
    // if there is a minimum balance, then display that in the deposit field
    if (accountsArray[1].productName.toUpperCase() != "NOT SET") {
        // This product is required.  Select it in the checkbox

        // Clear radio button that is set
        var element = $('input[name=AccountRadio]:checked')[0];
        if (element != undefined) {
            element.checked = false;
        }

        $('input:radio[name=AccountRadio]')[1].checked = true;
        //               
        //noneed  $('#depositAmount').val("$" + Common.FormatCurrency(accountsArray[1].minDepositAmt));
    }
    else {
        // Product not required, but we need to make sure the NOT SET is checked
        var radioButtons = $('input[name=AccountRadio]');
        $(radioButtons[0]).prop('checked', true).checkboxradio("refresh");
    }
}

function handleAvailabilityChange(element) {
    // Decide what to do when the user clicks on an Type account button.
    // Did the Tyep account change?
    //Yes=reload everything	and set query string "type"

    var elementId = element.id;
    elementId = String(elementId);
    var IdLength = elementId.length;
    var Availability = elementId.charAt(IdLength - 1);


    //don't do anything if the same button
    if (Availability == $('#hdAvailability').val()) {
        return true;
    }

    //clear out type to avoid duplicate
    var oldLocation = window.location.href;
    oldLocation = oldLocation.replace("&type=2", "").replace("&type=1", "");

    var newTypeQueryString = '&type=' + Availability;
    window.location = oldLocation + newTypeQueryString;


    //	//window.location.reload(true);
    //	
    //	$('#hdTypeAccount').val("2");
    //	
}
/*no need to use this function
function handleDepositChange(element) {
	// We want to check the minimum balance and set that in the deposit input box
	// First, check to see if the radio has selected a valid product code
	var numProducts = $('#hdNumProducts').val();
	numProducts = parseInt(numProducts, 10);
	for (var i = 1; i <= numProducts; i++) {
		if (accountsArray[currentlySelectedAccount].productName == productsArray[i].productName) {
			// Found the matching product code
			// If the deposit amount is less than the minimum, then set the minimum
			var depositAmt = $('#depositAmount').val();
			var minDepositAmt = productsArray[i].minDepositAmt;
			minDepositAmt = parseInt(minDepositAmt, 10);
			depositAmt = String(depositAmt).replace(/[^\d.]/g, "");
			depositAmt = parseInt(depositAmt, 10);
			/*if (depositAmt < minDepositAmt) {
				$('#depositAmount').prop('value', "$" + Common.FormatCurrency(minDepositAmt));
			}* /
           // just get deposit amount 
			accountsArray[currentlySelectedAccount].userDepositAmt = $('#depositAmount').val();
			// Get the id of the selected product
			$('#hdAmount' + currentlySelectedAccount).val(accountsArray[currentlySelectedAccount].userDepositAmt);
			break;
		}
	}

}*/

function handleAccountChange(element) {
    // Decide what to do when the user clicks on an account button.
    // Did the account change?
    // Is there a required product?
    // Is there a minimum deposit?  
    // Should we gray out all the products that have already been selected for other accounts?

    // Also need to save the current info before we show the info for the selected account
    // Since this function is only triggered when the user clicks on a different account,
    // We won't know the number of the previous account unless we saved it.
    /*no need this code
	accountsArray[currentlySelectedAccount].userDepositAmt = $('#depositAmount').val();
	*/
    // We want to check the minimum balance and overwrite vlue from in the deposit input box
    var numProducts = $('#hdNumProducts').val();
    numProducts = parseInt(numProducts, 10);
    /*move this validation to funding page
	var errorMsg = "";
	for (var i = 1; i <= numProducts; i++) {
		if (accountsArray[currentlySelectedAccount].productName == productsArray[i].productName) {
			// Found the matching product code
			// If the deposit amount is less than the minimum, then set the minimum
			var depositAmt = $('#depositAmount').val();
			var minDepositAmt = productsArray[i].minDepositAmt;
			minDepositAmt = parseInt(minDepositAmt, 10);
			depositAmt = String(depositAmt).replace(/[^\d.]/g, "");
			depositAmt = parseInt(depositAmt, 10);
			if (depositAmt < minDepositAmt) {
				$('#depositAmount').prop('value', "$" + Common.FormatCurrency(minDepositAmt));
			   	accountsArray[currentlySelectedAccount].userDepositAmt = $('#depositAmount').val();
			    errorMsg ="Account"+currentlySelectedAccount+": the minimum deposit amount must be $" + minDepositAmt + ".00<br/>";  
			}
			break;
		}
	}
	if (errorMsg != "") {
	    // An error occurred, we need to bring up the error dialog.
	    var nextpage = $('#divErrorDialog');
	    $('#txtErrorMessage').html(errorMsg);
	    if (nextpage.length > 0){
	        $.mobile.changePage(nextpage, {
	            transition: "slide",
	            reverse: false,
	            changeHash: true
	        });
	    }
	}*/

    // Get the id of the selected product
    var selectedProduct = $('input[name=AccountRadio]:checked').attr('id');
    accountsArray[currentlySelectedAccount].productName = selectedProduct;
    /*no use this
    $('#hdAmount' + currentlySelectedAccount).val(accountsArray[currentlySelectedAccount].userDepositAmt);
    $('#hdAccount' + currentlySelectedAccount).val(accountsArray[currentlySelectedAccount].productName);
    */



    // Show the selected account info for newly select
    var elementId = element.id;
    elementId = String(elementId);
    var IdLength = elementId.length;
    var accountNumber = elementId.charAt(IdLength - 1);
    currentlySelectedAccount = accountNumber;
    // Now load the info for the account with the number = accountNumber
    var selectedAccount = accountsArray[accountNumber];

    // If the account has a required product, disable all the products except for the one that is required
    // So, we can clear all the buttons, then select only the required one.
    // Then we disable the entire group

    // Uncheck all except for selected one
    var radioButtons = $('input[name=AccountRadio]');
    var numRadioButtons = radioButtons.length;
    for (var i = 0; i < numRadioButtons; i++) {
        $(radioButtons[i]).prop('checked', false).checkboxradio("refresh");
        $(radioButtons[i]).prop('disabled', true).checkboxradio("refresh");
        if (radioButtons[i].id == selectedAccount.productName) {
            $(radioButtons[i]).attr('checked', true).checkboxradio("refresh");
            $(radioButtons[i]).prop('disabled', false).checkboxradio("refresh");
        }
    }

    // If no required product is forced on this account, then enable all the radio buttons
    // This could have been combined with the top loop, but I split it for clarity
    var hasRequiredProduct = $('#hdRequiredProduct' + accountNumber).val();
    if ((String(hasRequiredProduct)).toUpperCase() == "N") {
        for (var i = 0; i < numRadioButtons; i++) {
            $(radioButtons[i]).prop('disabled', false).checkboxradio("refresh");
        }
    }

    // Now go back and disable the products that have been selected in another account
    for (var i = 0; i < numRadioButtons; i++) {
        for (var j = 1; j <= accountsArray.length - 1; j++) {
            if (j != currentlySelectedAccount) {
                var currentRadioValue = $(radioButtons[i]).attr('id');
                if (currentRadioValue.toUpperCase() == "NOT SET") {
                    continue;
                }
                if (currentRadioValue == accountsArray[j].productName && j != currentlySelectedAccount) {
                    $(radioButtons[i]).prop('disabled', true).checkboxradio("refresh");
                    continue;
                }
            }
        }
    }


    /* remove old format
    $('#depositAmount').prop('value', "$" + Common.FormatCurrency(selectedAccount.userDepositAmt));
   
    if (PCQ && PCQ.showQuestions)
        PCQ.showQuestions(); */
}

function validateForm(element) {
    
    //this lines of code is to prevent user click submit multiple times accidently
    if (isSubmittingXA) {
        return false;
    }
    isSubmittingXA = true;

    //if ($("#chkICertified").is(":checked") == false) {
    //    return false;
    //}
    //            if ($.trim($('#firstname').val()).length <= 0)
    //               {
    //               alert("First Name is required!");
    //               return false;
    //               }
    //            if ($.trim($('#lastname').val()).length <= 0)
    //                {alert("Last Name is required!");
    //                return false;
    //                }
    //            if ($.trim($('#ssn').val()).length <= 0)
    //                {
    //                alert("Social Security Number is required!");
    //                return false;
    //                }
    //            if ($('#ssn').val().length > 9 || !isNumber($('#ssn').val()))
    //                {alert("Invalid Social Security Number.")
    //                    return false;
    //                }
  
    //saveToHidden();  //remove old format
	return validateAll(element);
}

//function co_validateForm(element) {
//    //if ($("#co_chkICertified").is(":checked") == false) {
//    //    return false;
//    //}
//    //saveToHidden(); // remove old format
//    if (co_validateAll(element) != "") {
//        return false;
//    }

//    return false;
//}

function validateWalletQuestions(element) {
    

    //this lines of code is to prevent user click submit multiple times accidently
    if (isSubmitWalletAnswer) {
        return false;
    }
    isSubmitWalletAnswer = true;

    // Make sure that there is a selected answer for each question.
    // After validating the questions, submit them
    var index = 0;
    var numSelected = 0;
    var errorStr = "";

    var numOfQuestions = $('#hdNumWalletQuestions').val();
    numOfQuestions = parseInt(numOfQuestions, 10);

    for (index = 1; index <= numOfQuestions; index++) {
        // Check to see if a radio button was selected
        var radioButtons = $('input[name=walletquestion-radio-' + index.toString() + ']');
        var numRadioButtons = radioButtons.length;
        for (var i = 0; i < numRadioButtons; i++) {
            if ($(radioButtons[i]).prop('checked') == true) {
                numSelected++;
            }
        }
        if (numSelected == 0) {
            errorStr += "Please answer all the questions";
            break;
        }
        numSelected = 0;
    }
   
    if (errorStr != "") {
    	// An error occurred, we need to bring up the error dialog.
    	if ($("#divErrorPopup").length > 0) {
    		$('#txtErrorPopupMessage').html(errorStr);
    		$("#divErrorPopup").popup("open", { "positionTo": "window" });
    	} else {
    		var nextpage = $('#divErrorDialog');
    		$('#txtErrorMessage').html(errorStr);
    		if (nextpage.length > 0) {
    			$.mobile.changePage(nextpage, {
    				transition: "slide",
    				reverse: false,
    				changeHash: true
    			});
    		}
    	}
        //return errorStr;
        return false;
    }
   
    // No errors, so we can proceed
    submitWalletAnswers();
   
    //disable submit button 
    $('#walletQuestions .btnSubmitAnswer').addClass('ui-disabled');

    //isSubmitAnswer = true;
   
    return false;
    //return true;
}


function handleAccountDDLChange(clicked_obj) {
    //            var control = context.getEventSource();
    var curElement = $(clicked_obj);
    var selectedObjIDStr = clicked_obj;
    var selectedNum = selectedObjIDStr.charAt(selectedObjIDStr.length - 1);
    var selectedAccountName = document.getElementById(selectedObjIDStr).value;

    // Scan the other drop down lists to see which ones are selected.

    // Change the visibilty of the input field if this account ddl has a valid option selected
    // We also need to update the minimum balance
    if (selectedAccountName != "Please select an account") {
        $('#depositInputDiv' + selectedNum).show();
        //var minAmount = document.getElementById('hdMinAmount' + selectedAccountName).value;
        //noneed document.getElementById('depositAmount' + selectedNum).value = "$" + Common.FormatCurrency(minAmount);
    }
    else {
        $('#depositInput' + selectedNum).hide();
    }
}

function valButton(btn) {
    for (var i = btn.length - 1; i > -1; i--) {
        if (btn[i].checked) return btn[i].value;
    }
    return null;
}
/* remove old format
function validateDepositAmounts(numAccountsInt, numSelected) {
    // Validate the deposit amounts for all accounts that are not "NOT SET"
    var errorStr = "";

    for (var i = 1; i <= accountsArray.length; i++) {
        if (accountsArray[i] == undefined) {
            continue;
        }
        // Check the min deposit amount against the user deposit amount
        var userdepositAmount = String(accountsArray[i].userDepositAmt).replace(/[^\d.]/g, "");
        var minDepositAmount = String(accountsArray[i].minDepositAmt).replace(/[^\d.]/g, "");
        userdepositAmount = parseFloat(userdepositAmount, 10);
        minDepositAmount = parseFloat(minDepositAmount, 10);
        if (userdepositAmount < minDepositAmount) {
            errorStr += "The minimum deposit for " + accountsArray[i].productName;
            errorStr += " is $" + Common.FormatCurrency(accountsArray[i].minDepositAmt);
            errorStr += "</br>";
        }
    }

    return errorStr;


    //////            // Validate the initial deposit amounts for each Account that has been selected.
    //////            // Enforce the minimum deposit amount too.
    //////            var errorStr = "";
    //////            numSelected.value = 0;
    //////            
    //////            // Check the minimum deposit amount for each of the accounts
    //////            for (var i = 1; i <= numAccountsInt; i++) {
    //////                // Only enforce the minimum for the accounts that have been selected.
    //////                if (isAccountSelected(i)) {
    //////                    numSelected.value++;
    //////                
    //////                    // If the account has been selected, then check the minimum deposit
    //////                    var minDeposit = $('#hdMinAmount' + i.toString()).val();
    //////                    // Strip all non-digits from depositAmount except for "."
    //////                    var depositAmount = $('#depositAmount' + i.toString()).val().replace(/[^\d.]/g, "");
    //////                    
    //////                    // Check to see if the depositAmount was filled in
    //////                    if (!Common.ValidateText(depositAmount)) {
    //////                        strMessage += '* The Deposit Amount is required<br />';
    //////                    }
    //////                    else if (!isNumber(depositAmount)) {
    //////                        strMessage += '* The Deposit Amount is invalid<br />';
    //////                    }
    //////                    else {
    //////                        // If we get here, it means the deposit amount is not empty
    //////                        // and is a number.  There is one more check.
    //////                        // Is the amount within a valid range?

    //////                        minDeposit = parseFloat(minDeposit, 10);
    //////                        depositAmount = parseFloat(depositAmount, 10);
    //////                        if (parseFloat(depositAmount, 10) < minDeposit) {
    //////                            // The deposit amount is less than the minimum required
    //////                            errorStr += "* The minimum deposit for ";
    //////                            errorStr += $('#accountCollapsible' + i.toString()).attr("name");
    //////                            errorStr += " is $" + Common.FormatCurrency(minDeposit.toString());
    //////                            errorStr += "</br>";
    //////                        }
    //////                    }
    //////                }
    //////            }
    //////            if (numAccounts < 0) {
    //////                numAccounts = numSelected.value;
    //////            }
    //////            return errorStr;
}
//noneed

function isAccountSelected(index) {
    // Another way is to check if the deposit input is visible.
    if (!$('#depositAmount' + index.toString()).is(':hidden')) {
        return true;
    }
    else {
        return false;
    }
}
*/


//******create some new functions for new xa format
//show and hide product custom question
//function ShowProductCQ() {
//    var hasPCQ = false;
//    $('.selectProduct').each(function () {
//        var isChecked = $(this).is(':checked');
//        var productID = $(this).attr('id');
//        var NumOfQuestions = Number($('#divProductQuestion_' + productID).attr('question_count'));
//        if (isChecked) {
//            var isRequired = "";
//            //hide product question
//            if (NumOfQuestions == 0) { //there is no product questions
//                $('#divProductQuestion_' + productID).hide();
//            } else {
//                //show product quesion
//                hasPCQ = true;
//                $('#divProductQuestion_' + productID).show();
//                if (NumOfQuestions == 1) {
//                    isRequired = $('#question_1_' + productID).attr('question_required');
//                    if (isRequired == 'Y') { //display the red star for required product
//                        $('#reqQuestion_1_' + productID).show();
//                    } else {
//                        $('#reqQuestion_1_' + productID).hide();
//                    }
//                }
//                else if (NumOfQuestions > 1) //multiple questions in each products
//                {
//                    for (var i = 1 ; i <= NumOfQuestions; i++) {
//                        isRequired = $('#question_' + i + '_' + productID).attr('question_required');
//                        if (isRequired == 'Y') { //display the red star for required product
//                            $('#reqQuestion_' + i + '_' + productID).show();
//                        } else {
//                            $('#reqQuestion_' + i + '_' + productID).hide();
//                        }
//                    }
//                }
//            }

//        }
//        else {
//            $('#divProductQuestion_' + productID).hide();
//        }
//    });
//    if (hasPCQ) {
//        $('#divProductQuestionTitle').show();
//    } else {
//        $('#divProductQuestionTitle').hide();
//    }
//}
//function selectedProductIndex() {
//    var numProductsStr = $('#hdNumProducts').val();
//    var numProductsInt = parseInt(numProductsStr, 10);
//    var nProductArray = new Array();
//    var productIndex = 1;
//    $('.selectProduct').each(function () {
//        if ($(this).is(':checked') && $(this).is(':visible')) {
//            nProductArray.push(productIndex);
//        }
//        productIndex++;
//    });
//    return nProductArray;
//}
//function displayProductAmountTitle() {
//    var maxDepositAmount = $('#hdMaxFunding').val();
//    var strDepositAmountTitle = "";//"<span class='ProductCQ-Font'>How much do you want to deposit?</span>";
//    var fsFundingType = $('#fsFundingType option:selected').val();
//    if (maxDepositAmount != undefined && maxDepositAmount != "" && Number(maxDepositAmount) != 0 && fsFundingType != "INTERNAL TRANSFER") {
//        strDepositAmountTitle += "<i>The maximum total amount allowed across all products is " + Common.FormatCurrency(maxDepositAmount, true) + ".</i>";
//    }
//    $('#spProductAmount').html(strDepositAmountTitle);
//}
//function ShowAndHideDepositTextFields() {
//    $('.selectProduct').each(function () {
//        var productID = $(this).attr('id');
//        var productType = $(this).attr('product_type') ? $(this).attr('product_type').toLowerCase() : '';
//        if ($(this).is(':checked') && productType !== 'other') {
//            //show deposit amount and required Deposit amount label
//            $('#spDepositAmount_' + productID).show();
//            $('#divDepositAmount_' + productID).show();
//            //selected products for minor accounts
//            if (isSpecialAccount()) {
//                var currElement = $(this);
//                var currElementStyle = currElement.parent().parent().parent().attr('style');
//                if (!currElement.parent().hasClass('ui-checkbox')) {
//                    currElementStyle = currElement.parent().parent().attr('style');
//                }
//                if (currElementStyle != undefined && currElementStyle.indexOf('block') == -1) {
//                    //hide required Deposit amount product
//                    $('#spDepositAmount_' + productID).hide(); //hide required products label
//                    $('#divDepositAmount_' + productID).hide(); //hide no select product
//                }
//            }

//        } else {
//            //hide required Deposit amount product
//            $('#spDepositAmount_' + productID).hide(); //hide required products label
//            $('#divDepositAmount_' + productID).hide(); //hide no select product
//        }
//    });

//}
function validateInitialDepositProducts() {
    //var strError = "";
    //var userDepositAmt = 0;
    //var totalDeposit = 0.0;
    //var numProducts = $('#hdNumProducts').val();
    //numProducts = parseInt(numProducts, 10);
    //var maxTotalDeposit = $('#hdMaxFunding').val();
    //$('.selectProduct').each(function () {
    //    var currElement = $(this);
    //    if (currElement.is(':checked')) {
    //        var selectProductID = currElement.attr('id').replace('chk', "");
    //        for (var i = 1; i <= numProducts; i++) {
    //            if (selectProductID == productsArray[i].productName) {
    //                if (isSpecialAccount()) {
    //                    var currElementStyle = currElement.parent().parent().parent().attr('style');
    //                    if (!currElement.parent().hasClass('ui-checkbox')) {
    //                        currElementStyle = currElement.parent().parent().attr('style');
    //                    }
    //                    if (currElementStyle != undefined && currElementStyle.indexOf('block') == -1) { 
    //                        continue; //no validate hidden products
    //                    }
    //                }
    //                var minDepositAmt = productsArray[i].minDepositAmt;
    //                var depositAmt = $("#txtDepositAmount_chk" + selectProductID).val();
    //                minDepositAmt = parseFloat(minDepositAmt, 10);
    //                if (minDepositAmt < 0) { //make sure initial deposit is positive number
    //                    minDepositAmt = 0;
    //                }
    //                depositAmt = String(depositAmt).replace(/[^\d.]/g, "");
    //                depositAmt = parseFloat(depositAmt, 10);
    //                if (depositAmt < minDepositAmt) {
    //                    strError += "The minimum deposit amount of " + $("#txtDepositAmount_chk" + selectProductID).attr('text') + " must be " + Common.FormatCurrency(minDepositAmt, true) + ".<br/>";
    //                }
    //            }
    //        } //end for loop
    //    } //end checked
    //});
    ////Funding limits - there should not be a limit for the Internal Transfers as it is setup for online ->skip validation
    //var fundingType = $('#fsFundingType option:selected').val();
    //if (maxTotalDeposit != undefined && maxTotalDeposit != "" && Number(maxTotalDeposit) != 0 && fundingType != "INTERNAL TRANSFER") {
    //    totalDeposit = getTotalDeposit();
    //    maxTotalDeposit = parseFloat(maxTotalDeposit, 10);
    //    if (totalDeposit > maxTotalDeposit) {
    //        strError += "The deposit amount " + Common.FormatCurrency(totalDeposit, true) + " is over the maximum total amount allowed across all products.<br/>";
    //    }
    //}
    return xaProductFactory.validateInitialDepositProducts();
}
function getSelectProductArray() {
    //var selectProductArray = new Array();
    ////this function is used to create an selectedProduct object
    //var selectProduct = function (productName, userDepositAmt) {
    //    this.productName = productName;
    //    this.userDepositAmt = userDepositAmt;
    //}
    //$('.selectProduct').each(function () {
    //    var currElement =$(this);
    //    if (currElement.is(':checked')) {
    //        var productID = currElement.attr('id');
    //        //selected products for minor accounts
    //        if (isSpecialAccount()) {
    //            var currElementStyle = currElement.parent().parent().parent().attr('style');
    //            if (!currElement.parent().hasClass('ui-checkbox')) {
    //                currElementStyle = currElement.parent().parent().attr('style');
    //            }
    //            if (currElementStyle != undefined && currElementStyle.indexOf('block')==-1) {
    //                return true; //continue the loop
    //            }
    //        }
    //        var txtDepositAmount = $('#txtDepositAmount_' + productID).val();
    //        var ProductName = productID.replace("chk", "");
    //        selectProductArray.push(new selectProduct(ProductName, txtDepositAmount));
    //    }
    //});
    //return selectProductArray;
    return xaProductFactory.getProductSelectionList();
}
function getTotalDeposit() {
    var totalDeposit = 0.0;
    //$('.selectProduct').each(function () {
    //    if ($(this).is(':checked')) {
    //        var depositAmt = $("#txtDepositAmount_" + $(this).attr('id')).val();
    //        depositAmt = String(depositAmt).replace(/[^\d.]/g, "");
    //        if (depositAmt === "") depositAmt = 0;
    //        depositAmt = parseFloat(depositAmt, 10);
    //        totalDeposit += depositAmt;
    //    }
    //});
    //totalDeposit = xaProductFactory.getTotalDeposit();
    return totalDeposit;
}

//function ViewProductSelection() {
//    var strHtml = "";
//    var strStack1 = []; //product has services
//    var strStack2 = []; //product only
//    $('.selectProduct:checked').each(function () {
//        var strTemp = '';
//        if (hasHiddenMinorProduct($(this))) {
//            return true; //continue the loop
//        }
//        if ($(this).attr('product_type') && $(this).attr('product_type').toLowerCase() !== 'other') {
//            var productID = $(this).attr('id');
//            strTemp += '<div class="row"><div><span class="glyphicon glyphicon-check" aria-hidden="true"></span><span>' + $('#' + productID).val() + '</span></div>';

//            // Render Services
//            var prodServices = getServiceNameAndAccountNameArray(productID.replace('chk', ''));
//            if (prodServices.length > 0) {
//                for (var idx = 0; idx < prodServices.length; idx++) {
//                    strTemp += '<div style="padding-left:20px;"><span class="glyphicon glyphicon-check" aria-hidden="true"></span><span>' + prodServices[idx].ServiceName + '</span></div>';
//                }
//            }
//            strTemp += '</div>';
//            if (prodServices.length > 0) {
//                strStack1.push(strTemp);
//            } else {
//                strStack2.push(strTemp);
//            }
//        }
//    });
//    if (strStack1.length === 0 && strStack2.length === 0) {
//        return "";
//    }
//    else if (strStack1.length > 0 && strStack2.length > 0) {
//        strHtml += '<div class="col-sm-6 col-xs-12">';
//        for (var i = 0; i < strStack1.length; i++) {
//            strHtml += strStack1[i];
//        }
//        strHtml += '</div>';
//        strHtml += '<div class="col-sm-6 col-xs-12">';
//        for (var i = 0; i < strStack2.length; i++) {
//            strHtml += strStack2[i];
//        }
//        strHtml += '</div>';
//    } else {
//        if (strStack1.length === 0) {
//            for (var i = 0; i < strStack2.length; i++) {
//                strHtml += '<div class="col-sm-6 col-xs-12">' + strStack2[i] + '</div>';
//            }
//        } else {
//            for (var i = 0; i < strStack1.length; i++) {
//                strHtml += '<div class="col-sm-6 col-xs-12">'+ strStack1[i]+ '</div>';
//            }
//        }
//    }
//    return strHtml;
//}

function ViewOtherProductInterest() {
    var strHtml = "";
    $('.selectProduct:checked').each(function () {
        if (hasHiddenMinorProduct($(this))) {
            return true; //continue the loop
        }
        if ($(this).attr('product_type') && $(this).attr('product_type').toLowerCase() === 'other') {
            var productID = $(this).attr('id');
            strHtml += '<div class="col-sm-6 col-xs-12 group-heading" style="padding-left: 100px;"><div><span class="glyphicon glyphicon-check" aria-hidden="true"></span><span>' + $('#' + productID).val() + '</span></div>';

            // Render Services
            var prodServices = getServiceNameAndAccountNameArray(productID.replace('chk', ''));
            if (prodServices.length > 0) {
                for (var idx = 0; idx < prodServices.length; idx++) {
                    strHtml += '<div style="padding-left:20px;"><span class="glyphicon glyphicon-check" aria-hidden="true"></span><span>' + prodServices[idx].ServiceName + '</span></div>';
                }
            }
            strHtml += '</div>';
        }
    });
    $(this).prev("div.row").show();
    if (strHtml == "") {
        $(this).prev("div.row").hide();
    }
    return strHtml;
}
function ViewProductCustomQuestion() {
    var strHtml = "";
    $('.selectProduct:checked').each(function () {
        if (hasHiddenMinorProduct($(this))) {
            return true; //continue the loop
        }
        //'<div><p class="group-heading">12 Monthly CDs</p><p>Please select card </p><p style="font-style: italic">blah blah</p></div>'

        // Render Questions
        var productID = $(this).attr('id');
        var NumOfQuestions = Number($('#divProductQuestion_' + productID).attr('question_count'));
        if (Number(NumOfQuestions) > 0) {
            //render group title

            strHtml += '<div><p class="group-heading">' + $('#' + productID).val() + '</p>';

            for (var i = 1; i <= Number(NumOfQuestions) ; i++) {
                var productQuestionID = 'question_' + i + '_' + productID;
                var chkLength = $('#' + productQuestionID).attr('chknum');

                if (chkLength != undefined) // checkbox question;
                {
                    strHtml += '<p>' + $('#questionName_' + i + '_' + productID).text() + '</p>';
                    if (Number(chkLength) > 0) {
                        for (var j = 0; j < Number(chkLength) ; j++) {
                            if ($('#' + productQuestionID + '_' + j).is(':checked')) {
                                strHtml += '<p style="font-style: italic">' + $('#chkName_' + i + '_' + j).text() + '</p>';
                            }
                        }
                    }

                } else { //other question type
                    if ($('#question_' + i + '_' + productID).val() != undefined) {
                        strHtml += '<p>' + $('#questionName_' + i + '_' + productID).text() + '</p>';
                        var selectType = document.getElementById('question_' + i + '_' + productID).tagName;
                        if (selectType == "SELECT") { //for dropdown questions -->display text not value
                            strHtml += '<p style="font-style: italic">' + $('#question_' + i + '_' + productID + ' option:selected').text() + '</p>';
                        } else {
                            strHtml += '<p style="font-style: italic">' + $('#question_' + i + '_' + productID).val() + '</p>';
                        }
                    }
                }
            } //end for
            strHtml += '</div>';
        } //end numofquest..
    });
    $(this).prev("div.row").show();
    if (strHtml == "") {
        $(this).prev("div.row").hide();
    }
    return strHtml;
}
/*
function ViewSelectProduct() {
    var strViewSelectProduct = "";    
    var strViewProductServices = "";
    var strViewProductQuestion = "";
    var hasProductQuestion = false;
    var startIndex = 0;
    var otherProdTitle = "Other Product Interest(s)";
    $('.selectProduct').each(function () { //display all selected products
        var productID = $(this).attr('id');
        var isChecked = $('#' + productID).is(':checked');
      
        if (isChecked) {
            if (hasHiddenMinorProduct($(this))) {
                return true; //continue the loop
            }
            // only render otherProdTitle once
            if ($(this).attr('product_type') && $(this).attr('product_type').toLowerCase() === 'other' && otherProdTitle !== '') {
                strViewSelectProduct += "<br /><br /><span class='panel-title-font'>" + otherProdTitle + "</span>&nbsp;<a href='#pageOP' class='ui-link'>edit</a>";
                otherProdTitle = '';
            }

            // Render Products and Other Products
            strViewSelectProduct += "<br/><span class='CheckBoxIcon'></span><b>" + $('#' + productID).val() + "</b>";

            // Render Services
            var prodServices = getServiceNameAndAccountNameArray(productID.replace('chk', ''));
            if (prodServices.length > 0) {
                var currentAccName = prodServices[0].AccountName;
                strViewProductServices += "<br /><span class='labeltext-bold'>" + currentAccName + "</span>";
                for (var idx = 0; idx < prodServices.length; idx++) {
                    // group ServiceName by AccountName
                    if (currentAccName !== prodServices[idx].AccountName) {
                        currentAccName = prodServices[idx].AccountName;
                        strViewProductServices += "<br /><span class='labeltext-bold'>" + currentAccName + "</span>";
                    }
                    strViewProductServices += "<br/>&nbsp;&nbsp;&nbsp;<span class='CheckBoxIcon'></span><b>" + prodServices[idx].ServiceName + "</b>";
                }
            }

            // Render Questions
            var NumOfQuestions = Number($('#divProductQuestion_' + productID).attr('question_count')); 
            if (Number(NumOfQuestions) > 0) {
                startIndex++;
                if (startIndex == 1) {
                    strViewProductQuestion = "<br/><span class='labeltext-bold'>" + $('#' + productID).val() + "</span>";
                } else {
                    strViewProductQuestion += "<br/><br/><span class='labeltext-bold'>" + $('#' + productID).val() + "</span>";
                }

                hasProductQuestion = true;
                for (var i = 1; i <= Number(NumOfQuestions) ; i++) {
                    var productQuestionID = 'question_' + i + '_' + productID;
                    var chkLength = $('#' + productQuestionID).attr('chknum');

                    if (chkLength != undefined) // checkbox question;
                    {
                        strViewProductQuestion += "<br/><b>" + $('#questionName_' + i + '_' + productID).text() + "</b>";
                        if (Number(chkLength) > 0) {
                            for (var j = 0; j < Number(chkLength) ; j++) {
                                if ($('#' + productQuestionID + '_' + j).is(':checked')) {
                                    strViewProductQuestion += "<br/>" + $('#chkName_' + i + '_' + j).text();
                                }
                            }
                        }

                    } else { //other question type
                        // if ($('#question_' + i + '_' + productID).val() != "" && $('#question_' + i + '_' + productID).val() != undefined) {
                        //display all selected product questions(including questions are not answer) 
                        if ($('#question_' + i + '_' + productID).val() != undefined) {
                            strViewProductQuestion += "<br/><b>" + $('#questionName_' + i + '_' + productID).text() + " </b><br/>";
                            var selectType = document.getElementById('question_' + i + '_' + productID).tagName;
                            if (selectType == "SELECT") { //for dropdown questions -->display text not value
                                strViewProductQuestion += "<span>" + $('#question_' + i + '_' + productID + ' option:selected').text() + "</span>";
                            } else {
                                strViewProductQuestion += "<span>" + $('#question_' + i + '_' + productID).val() + "</span>";
                            }
                        }
                    }
                } //end for
            } //end numofquest...            
        }
    });

    if (strViewProductServices !== "") {
        strViewProductServices = "<br/><br/><span class='panel-title-font'>Product Services</span>" + strViewProductServices;
        strViewSelectProduct += strViewProductServices;
    }

    if (hasProductQuestion) {
        strViewProductQuestion = "<br/><br/><span class='panel-title-font'>Product Custom Questions</span>" + strViewProductQuestion;
        strViewSelectProduct = strViewSelectProduct + strViewProductQuestion;
    }
    return strViewSelectProduct;
}
*/
function hasHiddenMinorProduct(element) {
    if (isSpecialAccount()) {
        var currElement = element.parent().parent().parent().attr('style');
        if (!element.parent().hasClass('ui-checkbox')) {
            currElement = element.parent().parent().attr('style');
        }
        if (currElement != undefined && currElement.indexOf('block') == -1) {
            return true; 
        }
    }
    return false;
}
function ViewSelectedProducts() {
    var strViewSelectedProduct = '<div class="col-sm-6 col-xs-12">';

    $('.selectProduct').each(function () {
        var productType = $(this).attr('product_type') ? $(this).attr('product_type').toLowerCase() : '';
        if ($(this).is(':checked') && productType !== 'other') {
            if (hasHiddenMinorProduct($(this))) {
                return true; //continue the loop
            }
            var txtDepositAmountName = $('#txtDepositAmount_' + $(this).attr('id')).attr('text');
            var txtDepositAmount = $('#txtDepositAmount_' + $(this).attr('id')).val();
            strViewSelectedProduct += '<div class="row"><div class="col-xs-6 text-right row-title"><b>' + txtDepositAmountName + ':</b></div><div class="col-xs-6 text-left row-data"><span>' + Common.FormatCurrency(txtDepositAmount, true) + '</span></div></div>';
        }
    });
    strViewSelectedProduct += '<div class="row"><div class="col-xs-6 text-right row-title"><b>Total Deposit:</b></div><div class="col-xs-6 text-left row-data"><span>' + Common.FormatCurrency($('#txtTotalDeposit').val(), true) + '</span></div></div></div>';

    return strViewSelectedProduct;
}


//******end add new functions
/*deprecated*/
function validateScreen1(element, normalClick) {
    var currElement = $(element);
    var numProductsStr = $('#hdNumProducts').val();
    var numProductsInt = parseFloat(numProductsStr, 10);
    var strMessage = '';
    var numSelected = new Object();
    // Check the deposit amounts  TODO:numProductsInt, numSelected in para not use
    //---> validateDepositAmounts() is not correct so just don't use it
    //--->strMessage += validateDepositAmounts(numProductsInt, numSelected);
    // Make sure that at least one account is selected TODO:?what for?
    //comment out b/c numSelected is alway nothing , numAccounts is alway -1
    //    if (numAccounts < 0 && numSelected.value < 1) {
    //        strMessage += "Please select an account</br>";
    //    }



    /* remove old format
    var errorMessage = PCQ.getValidateError();
    if (errorMessage != null && errorMessage.length > 0) {
        strMessage += errorMessage;
    }
    //make sure at least one product is selected
    var isProductSelected = false;
    for (var i = 1; i <= 3; i++) {
        var selectedProduct = accountsArray[i].productName;
        if (selectedProduct != "NOT SET") {
            isProductSelected = true;
            break;
        }       
    }*/
    //var errorMessage = ProductCQ.getValidateError();
    //if (errorMessage != null && errorMessage.length > 0) {
    //    strMessage += errorMessage;
    //}
    var activePage = $.mobile.pageContainer.pagecontainer('getActivePage').attr("id");
    //ShowAndHideDepositTextFields();
    //xaProductFactory.toggleDepositTextFields();
    var productSelectionMsg = xaProductFactory.validateProductSelection();
    if ($.trim(productSelectionMsg) != "") {
        strMessage += productSelectionMsg;
    }
    //var NumOfSelectedProduct = selectedProductIndex().length;
    //if (NumOfSelectedProduct < 1 && activePage=='GettingStarted') {
    //    strMessage += "Please select at least 1 product. <br/>";
    //}
        /*new format user can select more than 3 products
        if (NumOfSelectedProduct >3) {
                strMessage += "The products was selected no more than 3 products.<br/>";    
        }
        if (NumOfSelectedProduct <= 3 && NumOfSelectedProduct > 0) {
            var totalDeposit = getTotalDeposit();
                $('#txtTotalDeposit').val(Common.FormatCurrency(totalDeposit, true));
        }*/
    else {
        var totalDeposit = getTotalDeposit();
        $('#txtTotalDeposit').val(Common.FormatCurrency(totalDeposit, true));
    }
    /* remove old format - now we modify and move the initial deposit field to the funding page
    therefore this codes should be used in the funding page(not product page) ****************
    //make sure the input amount meet the minimum requirement amount
    for (var i = 1; i <= numProductsInt; i++) {
        if (accountsArray[currentlySelectedAccount].productName == productsArray[i].productName) {
            // Found the matching product code
            // If the deposit amount is less than the minimum, then display error message
            //need to display the red star for selected product
            var depositAmt = $('#depositAmount').val();
            var minDepositAmt = productsArray[i].minDepositAmt;
            minDepositAmt = parseInt(minDepositAmt, 10);
            depositAmt = String(depositAmt).replace(/[^\d.]/g, "");
            depositAmt = parseInt(depositAmt, 10);
            if (depositAmt < minDepositAmt) {
                strMessage += "The minimum deposit amount must be $" + minDepositAmt + ".00 <br/>";
            }
            //break;
        } 
    }
    //limit the deposit amount -- for example total amount of three account <= 2000
   var TotalDepositAmt = 0;
    var MaxFunding = $('#hdMaxFunding').val();
    var userDepositAmt = 0;
    for (var i = 1; i <= 3; i++) {
            userDepositAmt = $('#hdAmount' + i).val();
            userDepositAmt = String(userDepositAmt).replace(/[^\d.]/g, "");
            userDepositAmt = parseInt(userDepositAmt, 10);
            if (!isNaN(userDepositAmt)) { 
                TotalDepositAmt += userDepositAmt;
            }
    }
    if (MaxFunding != "") {
        MaxFunding =parseInt(MaxFunding,10);
        if(!isNaN(MaxFunding))
        {
            if (TotalDepositAmt > MaxFunding) {
                strMessage += "$"+Common.FormatCurrency(TotalDepositAmt) +" is invalid, the maximum deposit amount is $"+Common.FormatCurrency(MaxFunding)+". Please enter deposit amount again.<br/>"
            }
        }
    }*/
    //validate verify code for home loan
    //strMessage += validateVerifyCode();
    // Make sure that the user has agreed to the disclosures
    //if (!Common.ValidateDisclosures('disclosures')) {
    //    strMessage += 'Please review and check all disclosures.<br />';
    //}
   
    //            // If there were errors, then programmatically click on the error dialog
    //            // otherwise, click on the link to the next screen
    //            if (normalClick == true) {
    //                if (strMessage != '') {
    //                    $('#txtErrorMessage').html(strMessage);
    //                    currElement.next().trigger('click');
    //                } else
    //                    currElement.next().next().trigger('click');
    //            }
    //            else {
    //                $('#txtErrorMessage').html(strMessage);
    //                return strMessage;
    //            }

    // Update the account info with the most current selections
    // Trigger the code for handling the user account change clicks
    // It will update the saved data for what is displayed on the screen
    // $('#account_choice_1').trigger('click');
    if (validateSpecialAccountType() != "") {
        strMessage = validateSpecialAccountType();
    }
    return strMessage;
}
//deprecated
//function validateMinorApplicantInfo() {
//    var strMessage = "";
//    //var strErrorMessage = "All required fields of ";
//    var hasMinorApplicant = $('#hdHasMinorApplicant').val(); 
//    if (hasMinorApplicant == 'Y') {
//    	var hasMinorId = $('#hdHasMinorId').val();
//    	var validator = true;
//        //if (m_ValidateApplicantInfoXA() != "") {
//        //    strMessage += strErrorMessage + "Minor Information are not completed.<br/>";
//        //    strMessage += "<div class='minor-margin-left'>" + m_ValidateApplicantInfoXA() + "</div>";
//    	//}
//    	if (m_ValidateApplicantInfoXA() === false) {
//		    validator = false;
//	    }
//        //if (m_ValidateApplicantContactInfoXA() != "") {
//        //    strMessage += strErrorMessage + "Contact Information are not completed.<br/>";
//        //    strMessage += "<div class='minor-margin-left'>" + m_ValidateApplicantContactInfoXA() + "</div>";
//    	//}
//    	if (m_ValidateApplicantContactInfoXA() === false) {
//    		validator = false;
//    	}
//        if (hasMinorId == 'Y') { 
//            //if (m_ValidateApplicantIdXA() != "") {
//            //    strMessage += strErrorMessage + "Identification Information are not completed.<br/>";
//            //    strMessage += "<div class='minor-margin-left'>" + m_ValidateApplicantIdXA() + "</div>";
//        	//}
//        	if (m_ValidateApplicantIdXA() === false) {
//        		validator = false;
//        	}
//        }
//        //if( m_ValidateApplicantAddress()!=""){
//        //    strMessage += strErrorMessage + "Current Physical Address are not completed.<br/>";
//        //    strMessage += "<div class='minor-margin-left'>" + m_ValidateApplicantAddress() + "</div>";
//        //}
//        //if(m_ValidateApplicantMailingAddress()!=""){
//        //    strMessage += m_ValidateApplicantMailingAddress();
//    	//}
//        if (m_ValidateApplicantAddress() === false) {
//        	validator = false;
//        }
//        if (m_ValidateApplicantMailingAddress() === false) {
//        	validator = false;
//        }
//        if (m_ValidateApplicantPreviousAddress() === false) {
//        	validator = false;
//        }
//        if ($("#hdShowMinorEmployment").val().toUpperCase() === "Y") {
//            //if (m_ValidateApplicantEmploymentXA() != "") {
//            //    strMessage += m_ValidateApplicantEmploymentXA();
//        	//}
//        	if (m_ValidateApplicantEmploymentXA() === false) {
//        		validator = false;
//        	}
//        }
//        if ($.lpqValidate("m_ValidateApplicantQuestionsXA") === false) {
//        	validator = false;
//        } else {
//        	//don't know whether the Condition Question is in use or not. So temporary put it here to make sure nothing will be missed
//        	//if it's not in use. Then, we can remove these lines of code
//        	var appConQuestionMsg = m_validateApplicantConditionedQuestion();
//        	if (appConQuestionMsg !== "") {
//        		strMessage += appConQuestionMsg;
//        		validator = false;
//        	}
//        }

//		if (validator === false) {
//			strMessage += "Please complete all of the required field(s)<br/>";
//		}
        
//    }
//    return strMessage;
//}

//not use
//function validateScreen2(element, normalClick) {
//    // Not implemented yet
//    var currElement = $(element);
//    var strMessage = "";
//    // Check the first name
//    var firstName = $('#txtFName').val();
//    if (firstName == null || firstName == "") {
//        strMessage += "First Name is required <br />";
//    }

//    // Check the last name
//    var lastName = $('#txtLName').val();
//    if (lastName == null || lastName == "") {
//        strMessage += "Last Name is required <br />";
//    }

//    // Check the Birthday by checking the Month, Day, and Year
//    /*var selectedMonth = $('#selectChoiceMonth option:selected').val();
//    if (null == selectedMonth || String(selectedMonth) == "0") {
//        strMessage += 'Month of birth is required<br />';
//    }
//    var selectedDay = $('#selectChoicDay option:selected').val();
//    if (null == selectedDay || String(selectedDay) == "0") {
//        strMessage += 'Day of birth is required<br />';
//    }
//    var selectedYear = $('#selectChoiceYear option:selected').val();
//    if (null == selectedYear || String(selectedYear) == "0") {
//        strMessage += 'Year of birth is required<br />';
//    }*/

//    // Check the Mother's Maiden Name field
//    var motherMaidenName = $('#mothermaidenname').val();
//    if (motherMaidenName == null || motherMaidenName == "") {
//        strMessage += "Mother's Maiden Name is required <br />";
//    }

//    //            // If there were errors, then programmatically click on the error dialog
//    //            // otherwise, click on the link to the next screen
//    //            if (normalClick == true) {
//    //                if (strMessage != '') {
//    //                    $('#txtErrorMessage').html(strMessage);
//    //                    currElement.next().trigger('click');
//    //                } else
//    //                    currElement.next().next().trigger('click');
//    //            }
//    //            else {
//    //                $('#txtErrorMessage').html(strMessage);
//    //                return strMessage;
//    //            }
//    return strMessage;
//}



function hasFOM() {
    return $('#HideFOMPage').val() != "Y";
}
function hasForeignTypeApp() {
    return $('#hdForeignAppType').val() == "Y";
}

//function validateUploadDocs(element, isCoApp) {
//    var currElement = $(element);
//    var strMessage = "";
//    if (typeof isCoApp == "undefined" || isCoApp == false) {
//        strMessage += validateUploadDocument();
//    } else {
//        strMessage += co_validateUploadDocument();
//    }

//    if (strMessage != '') {
//        $('#txtErrorMessage').html(strMessage);
//        currElement.next().trigger('click');
//    } else {
//        currElement.next().next().trigger('click');
//    }
//}


//modify validateActivePage based on the changing format
function validateActivePage(element, normalClick) {
    // When the validation function is not called directly, we need a way to figure out which page
    // to perform the validation, so we call this function and it will sort things out for us.
    // First, we need to figure out the ID of the active page
    var currElement = $(element);
    var activePage = $.mobile.pageContainer.pagecontainer('getActivePage').attr("id");
    var errorStr = "";
	var validator = true;
    var nextPage = "#pageFom";
    var hasCoApp = $('#hdHasCoApplicant').val();
    var scanDocument = $('#hdScanDocumentKey').val();
    var hasMinorApplicant = $('#hdHasMinorApplicant').val();
    switch (String(activePage).toUpperCase()) {
       
    	case "ACCUWELCOME":
    		validator = true;
    		errorStr = "";
    		if ($.lpqValidate("ValidateDisclosure_disclosures") == false) {
    			validator = false;
    		}
            nextPage = "#page2";
            break;
    	case "GETTINGSTARTED":
    		validator = true;
		    errorStr = "";
            if (hasFOM()) {
            	if ($.lpqValidate("ValidateFOM") === false) {
            		validator = false;
            	}
            }
            //xaProductFactory.toggleDepositTextFields();
            if ($.lpqValidate("ValidateProductSelection") == false) {
	            validator = false;
            } else {
            	var totalDeposit = getTotalDeposit();
            	$('#txtTotalDeposit').val(Common.FormatCurrency(totalDeposit, true));
            }
            errorStr += validateSpecialAccountType();
            if (errorStr != "") {
            	validator = false;
            }
            
            //if ($('div#pageOP').length > 0) {
            //    nextPage = '#pageOP';
            //} else {
            //    if (hasMinorApplicant == 'Y') {
            //        nextPage = "#pageMinorInfo";
            //    } else {
            //        nextPage = "#page2"; //original
            //    }
            //}
            if (hasMinorApplicant == 'Y') {
                nextPage = "#pageMinorInfo";
            } else {
                nextPage = "#page2"; //original
            }
            break;
    	//case "SCANDOCS":
    	//	validator = true;
    	//	errorStr = "";
        //  //  errorStr = validateRequiredDLScan($('#hdRequiredDLScan').val(),false);
        //   /* if ($('div#pageOP').length > 0) {
        //        nextPage = '#pageOP';
        //    } else {
        //        if (hasMinorApplicant == 'Y') {
        //            nextPage = "#pageMinorInfo";
        //        } else {
        //            nextPage = "#page2"; //original
        //        }
        //    }*/

        //    //New UI --> go to page2 when clicking continue/goback button
        //    nextPage = "#page2";
        //    break;
          
        /*case "PAGEOP":
            if (hasMinorApplicant=='Y') {
                nextPage = "#pageMinorInfo";
            } else { 
                nextPage = '#page2';
            }
            break;*/
    	case "PAGEMINORINFO":
    		validator = true;
    		errorStr = "";
    		var hasMinorApplicant = $('#hdHasMinorApplicant').val();
    		if (hasMinorApplicant == 'Y') {
    			//var hasMinorId = $('#hdHasMinorId').val();
    			if (m_ValidateApplicantInfoXA() === false) {
    				validator = false;
    			}
    			if (m_ValidateApplicantContactInfoXA() === false) {
    				validator = false;
    			}
    			if ($('#hdEnableIDSectionForMinor').val() == 'Y') {
    				if (m_ValidateApplicantIdXA() === false) {
    					validator = false;
    				}
    			}
    			if (m_ValidateApplicantAddress() === false) {
    				validator = false;
    			}
    			if (m_ValidateApplicantMailingAddress() === false) {
    				validator = false;
    			}
			    if ($('#hdEnableOccupancyStatusSectionForMinor').val() == 'Y') {
				    if (m_ValidateApplicantPreviousAddress() === false) {
					    validator = false;
				    }
			    }
			    if ($("#hdEnableEmploymentSectionForMinor").val().toUpperCase() === "Y") {
    				if (m_ValidateApplicantEmploymentXA() === false) {
    					validator = false;
    				}
    			}

    			if ($.lpqValidate("m_ValidateApplicantQuestionsXA") === false) {
    				validator = false;
    			} else {
    				//don't know whether the Condition Question is in use or not. So temporary put it here to make sure nothing will be missed
    				//if it's not in use. Then, we can remove these lines of code
    				var m_appConQuestionMsg = m_validateApplicantConditionedQuestion();
    				if (m_appConQuestionMsg !== "") {
    					errorStr += m_appConQuestionMsg;
    					validator = false;
    				}
    			}
    		}
            //errorStr += validateMinorApplicantInfo();
            nextPage = '#page2';
            break;
    	case "PAGEFS":
    		validator = true;
            //enabledSubmitBtn(hasCoApp);
            /*if (hasUpLoadDocument()) { //go to the upload document page
                errorStr += validateInitialDepositProducts();
                //errorStr += validateFS(element, normalClick);
                errorStr += validateFS1(element, normalClick);
                nextPage = "#UploadDocs";
            } else {*/
                //need to validate all information before display review information
                //if (hasFOM()) {
                //    errorStr = validateFOM();
                //}
                //errorStr += validateScreen1(element, normalClick);
                //errorStr += ValidateApplicantInfoXA();
                //errorStr += ValidateApplicantEmploymentXA();
                //errorStr += ValidateApplicantContactInfoXA();
                //errorStr += ValidateApplicantIdXA();
                //errorStr += ValidateApplicantAddress();
                //errorStr += ValidateApplicantPreviousAddress();
                //errorStr += ValidateApplicantMailingAddress();
                //errorStr += ValidateCustomQuestions();
                //errorStr += validateInitialDepositProducts();//move to validateFS1
                //errorStr += validateFS(element, normalClick);
    		if (validateFS1() == false) {
    			validator = false;
    		}
                //if (hasCoApp == 'Y') {
                //    errorStr += co_ValidateApplicantInfoXA();
                //    errorStr += co_ValidateApplicantEmploymentXA();
                //    errorStr += co_ValidateApplicantContactInfoXA();
                //    errorStr += co_ValidateApplicantIdXA();
                //    errorStr += co_ValidateApplicantAddress();
                //    errorStr += co_ValidateApplicantPreviousAddress();
                //    errorStr += co_ValidateApplicantMailingAddress();
                //}
                nextPage = "#reviewpage";
            /*}*/
            break;
        
    	case "PAGE2":
    		validator = true;
    		errorStr = "";
        	if(ValidateApplicantInfoXA() === false) {
		        validator = false;
        	}
        	if(ValidateApplicantAddress() === false) {
        		validator = false;
        	}
		    if ($('#hdEnableOccupancyStatusSection').val() == 'Y') {
			    if (ValidateApplicantPreviousAddress() === false) {
				    validator = false;
			    }
		    }
		    if(ValidateApplicantMailingAddress() === false) {
        		validator = false;
        	}
        	if(ValidateApplicantContactInfoXA() === false) {
        		validator = false;
        	}
		    if ($('#hdEnableIDSection').val() == 'Y') {
			    if (ValidateApplicantIdXA() === false) {
				    validator = false;
			    }
		    }
		    if ($("#hdEnableEmploymentSection").val().toUpperCase() !== "N") {
            	if (ValidateApplicantEmploymentXA() === false) {
            		validator = false;
            	}
            }
		    //if (typeof docUploadObj != "undefined" && docUploadObj != null && docUploadObj.validateUploadDocument() == false) {
			//	validator = false;
			//}

			if ($.lpqValidate("ValidateApplicantQuestionsXA") === false) {
				validator = false;
			} else {
				//don't know whether the Condition Question is in use or not. So temporary put it here to make sure nothing will be missed
				//if it's not in use. Then, we can remove these lines of code
				var appConQuestionMsg = validateApplicantConditionedQuestion();
				if (appConQuestionMsg !== "") {
					errorStr += appConQuestionMsg;
					validator = false;
				}
			}

			var eSignatureMsg = validateESignature();
			if (eSignatureMsg !== "") {
				validator = false;
				errorStr += eSignatureMsg;
			}
            
            var requiredDLMessage = validateRequiredDLScan($('#hdRequiredDLScan').val(), false);
            if (requiredDLMessage != "") {
            	errorStr += requiredDLMessage;
            	validator = false;
            }
            if (hasCoApp == 'Y') {
                nextPage = "#page6";
            } else {
                //make sure funding options are available --> skip funding page
                if ($("#divFundingTypeList a.btn-header-theme").length > 0) {
                    nextPage = "#pageFS";
                } else {
                    nextPage = "#reviewpage";
                }
            }
            break;
        /*
        This portion of code has been commented out due to the logic changed
        We combine and move Applicant Info, Applicant Address, Contact Info, Identification, Employment Status to page #2
        So we can bypass these steps through #page5
        */
        /*case "PAGE3":
            errorStr = ValidateApplicantContactInfoXA();
            if (isHiddenSecIdPage()) {
              nextPage = "#page5";
             } else {    
              nextPage = "#page4";
             }
            break;
        case "PAGE4":
            errorStr = ValidateApplicantIdXA();
            nextPage = "#page5";
            break;
        case "PAGE5":
            if(errorStr.indexOf(ValidateCustomQuestions()) ==-1){
                errorStr += ValidateCustomQuestions();
            }
            if (hasCoApp == 'Y') {
                if (scanDocument != "") {
                  
                    nextPage = "#co_scandocs";
                } else {
                    nextPage = "#page6";
                }
            } else {
                nextPage = "#pageFS";
            }
            break;*/
    	//case "CO_SCANDOCS":
    	//	validator = true;
    	//	errorStr = "";
        //   // errorStr = validateRequiredDLScan($('#hdRequiredDLScan').val(), true);
        //    nextPage = "#page6";
        //    break;

    	case "PAGE6":
    		validator = true;
    		errorStr = "";
    		if (co_ValidateApplicantInfoXA() === false) {
			    validator = false;
    		}
    		if (co_ValidateApplicantAddress() === false) {
    			validator = false;
    		}
		    if ($('#hdEnableOccupancyStatusSectionForCoApp').val() == 'Y') {
			    if (co_ValidateApplicantPreviousAddress() === false) {
				    validator = false;
			    }
		    }
		    if (co_ValidateApplicantMailingAddress() === false) {
    			validator = false;
    		}
    		if (co_ValidateApplicantContactInfoXA() === false) {
    			validator = false;
    		}
		    if ($('#hdEnableIDSectionForCoApp').val() == 'Y') {
			    if (co_ValidateApplicantIdXA() === false) {
				    validator = false;
			    }
		    }
		    if ($("#hdEnableEmploymentSectionForCoApp").val().toUpperCase() === "Y") {
            	if (co_ValidateApplicantEmploymentXA() === false) {
            		validator = false;
            	}
            }
		    //if (typeof co_docUploadObj != "undefined" && co_docUploadObj != null && co_docUploadObj.validateUploadDocument() == false) {
            //	validator = false;
            //}

            if ($.lpqValidate("co_ValidateApplicantQuestionsXA") === false) {
            	validator = false;
            } else {
            	//don't know whether the Condition Question is in use or not. So temporary put it here to make sure nothing will be missed
            	//if it's not in use. Then, we can remove these lines of code
            	var co_appConQuestionMsg = co_validateApplicantConditionedQuestion();
            	if (co_appConQuestionMsg !== "") {
            		errorStr += co_appConQuestionMsg;
            		validator = false;
            	}
            }

			var requiredDLMessage = validateRequiredDLScan($('#hdRequiredDLScan').val(), true);
            if (requiredDLMessage != "") {
            	errorStr += requiredDLMessage;
	            validator = false;
            }
            //make sure funding options are available --> skip funding page
            if ($("#divFundingTypeList a.btn-header-theme").length > 0) {
                nextPage = "#pageFS";
            } else {
                nextPage = "#reviewpage";
            }
            break;
        /*case "PAGE7":
            errorStr = co_ValidateApplicantContactInfoXA();
            if (isHiddenSecIdPage()) {
                nextPage = "#page9";
            } else {
                nextPage = "#page8";
            }
            break;
        case "PAGE8":
            errorStr = co_ValidateApplicantIdXA();
            nextPage = "#page9";
            break;
        case "PAGE9":

            errorStr += co_ValidateApplicantAddress();
            errorStr += co_ValidateApplicantPreviousAddress();
            errorStr += co_ValidateApplicantMailingAddress();
            if (hasCoApp == 'Y') {      
                if (errorStr.indexOf(validateApplicantQuestions('co_')) == -1) {
                    errorStr += validateApplicantQuestions('co_');
                }
            }
            nextPage = "#pageFS";
            break;*/
        default:
            // This case should never happen.
            // If it does, then we should start the application process over
            $('#txtErrorMessage').html("An error occurred. Please start again from the beginning.");
            //nextpage = $('#page1');
            nextpage = $('#pageFom');
            $.mobile.changePage(nextpage, {
                transition: "slide",
                reverse: false,
                changeHash: true
            });

            return "Please start over";

            break;
    }

    // If there were errors, then programmatically click on the error dialog
    // otherwise, click on the link to the next screen

    if (normalClick == true) {
    	if (validator == false) {
			if (errorStr !== "") {
				if ($("#divErrorPopup").length > 0) {
					$('#txtErrorPopupMessage').html(errorStr);
					$("#divErrorPopup").popup("open", { "positionTo": "window" });
				} else {
					$('#txtErrorMessage').html(errorStr);
					currElement.next().trigger('click');
				}
			} else {
				Common.ScrollToError();
			}
	    } else {
    		//if (nextPage == "#reviewpage" || nextPage == "#finalreviewpage") {
    		if (nextPage == "#reviewpage") {
    			ViewInformationPage(hasCoApp);
    			if (hasMinorApplicant == 'Y') {  
    				ViewMinorInformation();
    			}
    		}
    		nextpage = $(nextPage);
    		$.mobile.changePage(nextpage, {
    			transition: "slide",
    			reverse: false,
    			changeHash: true
    		});
	    }
    }
    else {
        // If the validation was triggered by a swipe,
        // simply return the error message.  The swipe feature will
        // decide what to do with it.  It should bring up the error dialog.
        $('#txtErrorMessage').html(errorStr);
        return errorStr;
    }

}
//review Page section
function ViewInformationPage(hasCoApp) {
    //$(".ViewSelectProduct").html(ViewSelectProduct());
    //$(".ViewProductSelection").html(ViewProductSelection());
    $(".ViewProductSelection").html(xaProductFactory.viewProductSelection());
    $(".ViewOtherProductInterest").html(ViewOtherProductInterest.call($(".ViewOtherProductInterest")));
    //$(".ViewProductCustomQuestion").html(ViewProductCustomQuestion.call($(".ViewProductCustomQuestion")));
    $(".ViewAcountInfo").html(ViewAccountInfo());
    $(".ViewContactInfo").html(ViewContactInfo());
	if ($('#hdEnableIDSection').val() == 'Y') {
		$(".ViewIdentification").html(ViewPrimaryIdentification.call($(".ViewIdentification")));
	}
	$(".ViewAddress").html(ViewAddress());
	if ($("#hdEnableEmploymentSection").val() === "Y") {
		$(".ViewEmploymentInfo").html(ViewEmploymentInfo.call($(".ViewEmploymentInfo")));
		$(".ViewPrevEmploymentInfo").html(ViewPrevEmploymentInfo.call($(".ViewPrevEmploymentInfo")));
	}
    //$(".ViewApplicantQuestion").html(ViewApplicantQuestion(""));
    //$(".ViewCustomQuestion").html(ViewCustomQuestion());
    $(".ViewFundingSource").html(ViewSelectedProducts() + ViewFundingSource());
    //if (reviewPage = "#finalreviewpage") {
    if (hasCoApp === "Y") {
        $('div[data-section-name="reviewJointApplicant"]').show();
        $(".co_ViewAcountInfo").html(co_ViewAccountInfo());
        $(".co_ViewContactInfo").html(co_ViewContactInfo());
	    if ($('#hdEnableIDSectionForCoApp').val() == 'Y') {
	    	$(".co_ViewIdentification").html(co_ViewPrimaryIdentification.call($('.co_ViewIdentification')));
	    }
	    //$("#co_ViewApplicantQuestion").html(co_ViewApplicantQuestion());
	    $(".co_ViewAddress").html(co_ViewAddress());
	    if ($("#hdEnableEmploymentSectionForCoApp").val() === "Y") {
	    	$(".co_ViewEmploymentInfo").html(co_ViewEmploymentInfo.call($(".co_ViewEmploymentInfo")));
	    	$(".ViewJointApplicantPrevEmploymentInfo").html(co_ViewPrevEmploymentInfo.call($(".ViewJointApplicantPrevEmploymentInfo")));
	    }
    } else {
        $('div[data-section-name="reviewJointApplicant"]').hide();
    }
    $("div.review-container div.row-title b").each(function (idx, ele) {
    	if (typeof $(ele).data("renameid") == "undefined") {
    		var dataId = getDataId();
    		$(ele).attr("data-renameid", dataId);
    		RENAME_REPOSITORY[dataId] = htmlEncode($(ele).html());
    	}
    });
  
}
function ViewMinorInformation() {
    $(".ViewMinorAcountInfo").html(m_ViewAccountInfo());
    $(".ViewMinorContactInfo").html(m_ViewContactInfo());
	if ($('#hdEnableIDSectionForMinor').val() == 'Y') {
		$(".ViewMinorIdentification").html(m_ViewPrimaryIdentification.call($(".ViewMinorIdentification")));
	}
	$(".ViewMinorAddress").html(m_ViewAddress());
	if ($("#hdEnableEmploymentSectionForMinor").val() === "Y") {
		$(".ViewMinorEmploymentInfo").html(m_ViewEmploymentInfo.call($(".ViewMinorEmploymentInfo")));
		$(".ViewMinorPrevEmploymentInfo").html(m_ViewPrevEmploymentInfo.call($(".ViewMinorPrevEmploymentInfo")));
	}
    //$(".ViewMinorApplicantQuestion").html(m_ViewApplicantQuestion());
}
function ClearReviewInfoXA(){
    if ($('#reviewPanels').length > 0) {
        //clear the Review Information 
        $(".ViewProductSelection").html('');
        $(".ViewOtherProductInterest").html('');
        $(".ViewProductCustomQuestion").html('');
        $(".ViewAcountInfo").html('');
        $(".ViewContactInfo").html('');
        $(".ViewIdentification").html('');
        $(".ViewAddress").html('');
        //$(".ViewApplicantQuestion").html('');
        //$(".ViewCustomQuestion").html('');
        $(".ViewFundingSource").html('');
        $(".ViewAddress").html('');
        $(".co_ViewAcountInfo").html('');
        $(".co_ViewContactInfo").html('');
        $(".co_ViewIdentification").html('');
        $(".co_ViewAddress").html('');
        //$("#co_ViewApplicantQuestion").html('');
        //clear minor applicant
        $(".ViewMinorAcountInfo").html('');
        $(".ViewMinorContactInfo").html('');
        if( $(".ViewMinorIdentification").length>0){
            $(".ViewMinorIdentification").html('');
        }
        $(".ViewMinorAddress").html('');
        if ($(".ViewMinorEmploymentInfo").length > 0) {
            $(".ViewMinorEmploymentInfo").html('');
        }
        if ($(".ViewEmploymentInfo").length > 0) {
            $(".ViewEmploymentInfo").html('');
        }
        if ($(".co_ViewEmploymentInfo").length > 0) {
            $(".co_ViewEmploymentInfo").html('');
        }

        if ($(".ViewPrevEmploymentInfo").length > 0) {
        	$(".ViewPrevEmploymentInfo").html('');
        }
        if ($(".ViewJointApplicantPrevEmploymentInfo").length > 0) {
        	$(".ViewJointApplicantPrevEmploymentInfo").html('');
        }
        if ($(".ViewMinorPrevEmploymentInfo").length > 0) {
        	$(".ViewMinorPrevEmploymentInfo").html('');
        }

        if ($(".ViewMinorApplicantQuestion").length > 0) {
            $(".ViewMinorApplicantQuestion").html('');
        }
    }
}
//end review page section
function validateAll(element) {
    // Validate all the screens again before submitting.
    var errorStr = "";
    var validator = true;
    //if (hasFOM()) {
    //    errorStr = validateFOM();
    //}
    //errorStr += validateScreen1(element, false);
    //var hasMinorApplicant = $('#hdHasMinorApplicant').val();
    //if (hasMinorApplicant == 'Y') {
    //	var hasMinorId = $('#hdHasMinorId').val();
    //	if (m_ValidateApplicantInfoXA() === false) {
    //		validator = false;
    //	}
    //	if (m_ValidateApplicantContactInfoXA() === false) {
    //		validator = false;
    //	}
    //	if (hasMinorId == 'Y') {
    //		if (m_ValidateApplicantIdXA() === false) {
    //			validator = false;
    //		}
    //	}
    //	if (m_ValidateApplicantAddress() === false) {
    //		validator = false;
    //	}
    //	if (m_ValidateApplicantMailingAddress() === false) {
    //		validator = false;
    //	}
    //	if (m_ValidateApplicantPreviousAddress() === false) {
    //		validator = false;
    //	}
    //	if ($("#hdShowMinorEmployment").val().toUpperCase() === "Y") {
    //		if (m_ValidateApplicantEmploymentXA() === false) {
    //			validator = false;
    //		}
    //	}
    //	errorStr += m_validateApplicantQuestions();
    //}
	//page2
    //if (ValidateApplicantInfoXA() === false) {
    //	validator = false;
    //}
    //if (ValidateApplicantAddress() === false) {
    //	validator = false;
    //}
    //if (ValidateApplicantPreviousAddress() === false) {
    //	validator = false;
    //}
    //if (ValidateApplicantMailingAddress() === false) {
    //	validator = false;
    //}
    //if (ValidateApplicantContactInfoXA() === false) {
    //	validator = false;
    //}

    //if (ValidateApplicantIdXA() === false) {
    //	validator = false;
    //}
    //if ($("#hdShowEmployment").val().toUpperCase() === "Y") {
    //	if (ValidateApplicantEmploymentXA() === false) {
    //		validator = false;
    //	}
    //}
    //errorStr += validateESignature();
    //errorStr += validateApplicantQuestions();
    //errorStr += validateUploadDocument();
    //var requiredDLMessage = validateRequiredDLScan($('#hdRequiredDLScan').val(), false);
    //if (requiredDLMessage != "") {
    //	errorStr += requiredDLMessage;
    //}
	//page6
    //if($("#hdHasCoApplicant").val() === "Y") {
    //	if (co_ValidateApplicantInfoXA() === false) {
    //		validator = false;
    //	}
    //	if (co_ValidateApplicantAddress() === false) {
    //		validator = false;
    //	}
    //	if (co_ValidateApplicantPreviousAddress() === false) {
    //		validator = false;
    //	}
    //	if (co_ValidateApplicantMailingAddress() === false) {
    //		validator = false;
    //	}
    //	if (co_ValidateApplicantContactInfoXA() === false) {
    //		validator = false;
    //	}
    //	if (co_ValidateApplicantIdXA() === false) {
    //		validator = false;
    //	}
    //	if ($("#hdShowEmployment").val().toUpperCase() === "Y") {
    //		if (co_ValidateApplicantEmploymentXA() === false) {
    //			validator = false;
    //		}
    //	}
    //	errorStr += co_validateApplicantQuestions();
    //	errorStr += co_validateUploadDocument();
    //	//var requiredDLMessage = validateRequiredDLScan($('#hdRequiredDLScan').val(), true);
    //	//if (requiredDLMessage != "") {
    //	//	errorStr += requiredDLMessage;
    //	//}
    //}
   // errorStr += ValidateCustomQuestions();

    //errorStr += validateInitialDepositProducts(); //move to validateFS1
    //errorStr += validateFS(element, false);
    //errorStr += validateFS1(element, false);

   
    if ($("#hdEnableDisclosureSection").val() === "Y") {
    	//disclosure field does not exist  
    	if ($('div[section-name="disclosure"]').length == 0) {
    		return false;
    	}
		if ($.lpqValidate("ValidateDisclosure_disclosures") == false) {
			validator = false;
		}
	}

	//if (validator === false && errorStr.indexOf("Please complete all of the required field(s)") < 0) {
    //	errorStr = "Please complete all of the required field(s)<br/>" + errorStr;
    //}
    if (validator === false) {
        isSubmittingXA = false;  //allow resubmit when there is error
    	// An error occurred, we need to bring up the error dialog.
	    Common.ScrollToError(function () {
	    	isSubmittingXA = false;
	    });
	    return false;
    }
  
    // No errors, so we can proceed
    saveUserInfo();
    //disable submit button to prevent user click submit multiple times 
    $('#accuWelcome .btnSubmit').addClass('ui-disabled');

    return true;
}
//COMBINE reviewpage and finalreviewpage, THIS FUNCTION IS OBSOLETED
//function co_validateAll(element) {
//    // Validate all the screens again before submitting.
//    var errorStr = "";
//    if (hasFOM()) {
//        errorStr = validateFOM();
//    }

//    errorStr += validateScreen1(element, false);
//    errorStr += ValidateApplicantInfoXA();
//    errorStr += ValidateApplicantEmploymentXA();
//    errorStr += ValidateApplicantContactInfoXA();
//    errorStr += ValidateApplicantIdXA();
//    errorStr += ValidateApplicantAddress();
//    errorStr += ValidateApplicantPreviousAddress();
//    errorStr += ValidateApplicantMailingAddress();
//    //errorStr += ValidateCustomQuestions();
//    errorStr += co_ValidateApplicantInfoXA();
//    errorStr += co_ValidateApplicantEmploymentXA();
//    errorStr += co_ValidateApplicantContactInfoXA();
//    errorStr += co_ValidateApplicantIdXA();
//    errorStr += co_ValidateApplicantAddress();
//    errorStr += co_ValidateApplicantPreviousAddress();
//    errorStr += co_ValidateApplicantMailingAddress();
//    errorStr += validateInitialDepositProducts();
//    //errorStr += validateFS(element, false);
//    errorStr += validateFS1(element, false);
//    if (!Common.ValidateDisclosures('disclosures1')) {
//        errorStr += 'Please review and check all disclosures.<br />';
//    }
//    if (errorStr != "") {
//        // An error occurred, we need to bring up the error dialog.
//        var nextpage = $('#divErrorDialog');

//        $('#txtErrorMessage').html(errorStr);
//        if (nextpage.length > 0) {
//            $.mobile.changePage(nextpage, {
//                transition: "slide",
//                reverse: false,
//                changeHash: true
//            });
//        }
//        return errorStr;
//    }

//    // No errors, so we can proceed
//    saveUserInfo();
//    return "";
//}



function handleAccountCollapsibleClick(clicked_obj) {
    return;
    var curElement = $(clicked_obj);
    var selectedObjIDStr = clicked_obj;
    var selectedElement = document.getElementById(selectedObjIDStr);
    var idStrPos = selectedObjIDStr.search("requiredAccount");
    if (idStrPos >= 0) {
        // This is a required account, so don't let the user close it
        //                selectedElement..collapsible({collapsed: false});
        $(selectedObjIDStr).trigger('expand');
        $(selectedObjIDStr).trigger('expand');
    }
}

function saveUserInfo() {
    if (g_submit_button_clicked) return false;
    //var fundingType = $('#fsFundingType').val();
    //if (fundingType === "PAYPAL") {
    //    saveUserInfoWithPayPal();
    //} else {
        //$('#txtErrorMessage').html('');//useless
        var loanInfo = getApplicantInfo();
        loanInfo.Task = "SubmitLoan";
        loanInfo = attachGlobalVarialble(loanInfo);
        //var fundingType = $('#fsFundingType').val();
        var fundingType = $('#fsSelectedFundingType').val();
        if (fundingType === "PAYPAL") {
            loanInfo.SelectedFundingSource = 'PAYPAL';
        } 
       
        $.ajax({
            type: 'POST',
            url: 'Callback.aspx',
            data: loanInfo,
            dataType: 'html',
            success: function (response) {

                //enable submit button
                $('#accuWelcome .btnSubmit').removeClass('ui-disabled');

                if (response.toUpperCase().indexOf('MLERRORMESSAGE') > -1) {
                    var decoded_message = $('<div/>').html(response).text(); //message maybe encoded in the config xml so need to decode
                    //$('#txtErrorMessage').html(decoded_message);
                    goToDialog(decoded_message);
                } else {
                    if (response.toUpperCase().indexOf('MLNOIDAUTHENTICATION') > -1) {
                        isApplicationCompleted = true;
                        goToLastDialog(response); // submit success message
                        //need to display response first before clear all inputs
                        //submit sucessfull so clear
                        // clearForms();

                        //disable submit button to prevent user click submit multiple times 
                        $('#accuWelcome .btnSubmit').addClass('ui-disabled');

                    } else if (response.toUpperCase().indexOf('WALLETQUESTION') > -1) {
                      
                        displayWalletQuestions(response);

                        //disable submit button to prevent user click submit multiple times 
                        $('#accuWelcome .btnSubmit').addClass('ui-disabled');
                    } else if (response.toUpperCase().indexOf('MLPAYPAL_CREATED') > -1) {
                        window.location.href = JSON.parse(response).Info.URL;
                    } else if (response.toUpperCase().indexOf('MLPAYPAL_FAILED') > -1) {
                        goToLastDialog(JSON.parse(response).Info.APPROVED_MESSAGE); //preapproved message

                        //disable submit button to prevent user click submit multiple times 
                        $('#accuWelcome .btnSubmit').addClass('ui-disabled');
                    } else {
                        isApplicationCompleted = true;
                        goToLastDialog(response); //preapproved message
                        //need to display response first before clear all inputs
                        //submit sucessfull so clear
                        //clearForms();

                        //disable submit button to prevent user click submit multiple times 
                        $('#accuWelcome .btnSubmit').addClass('ui-disabled');
                    }
                }
            },
            error: function (err) {

                //enable submit button
                $('#accuWelcome .btnSubmit').removeClass('ui-disabled');

                //$('#txtErrorMessage').html('We apologize, but unfortunately there was an error when submitting the application. Please try again later.');
                goToDialog('We apologize, but unfortunately there was an error when submitting the application. Please try again later.');
            }
        });
    //}
        //hasClickedSubmitBtn(window.bHasCoApp);
    return false;
}


// Use this function to submit data for an application without having to type in all the data
function submitTestData() {
    $('#txtErrorMessage').html('');
    var loanInfo = getTestLoanInfo();
    loanInfo = attachGlobalVarialble(loanInfo);
    $.ajax({
        type: 'POST',
        url: 'Callback.aspx',
        data: loanInfo,
        dataType: 'html',
        success: function (response) {
            if (response.toUpperCase().indexOf('MLERRORMESSAGE') > -1) {
                //$('#txtErrorMessage').html(response);
                goToDialog(response);
            } else {
                if (response.toUpperCase().indexOf('MLNOIDAUTHENTICATION') > -1) {
                    isApplicationCompleted = true;
                    goToLastDialog(response);
                }
                else {
                    displayWalletQuestions(response);
                }
            }
        },
        error: function (err) {
            //$('#txtErrorMessage').html('We apologize, but unfortunately there was an error when submitting the application. Please try again later.');
            goToDialog('We apologize, but unfortunately there was an error when submitting the application. Please try again later.');
        }
    });
    return false;
}

function submitWalletAnswers() {
    //$('#txtErrorMessage').html('');//useless
    var answersObj = getWalletAnswers();
    answersObj = attachGlobalVarialble(answersObj);
    //var fundingType = $('#fsFundingType').val();
    //var fundingType = $('#fsSelectedFundingType').val();
    //if (fundingType === "PAYPAL") {
    //    answersObj.SelectedFundingSource = 'PAYPAL';
    //}

    $.ajax({
        type: 'POST',
        url: 'Callback.aspx',
        data: answersObj,
        dataType: 'html',
        success: function (response) {
            //enable submit button 
            $('#walletQuestions .btnSubmitAnswer').removeClass('ui-disabled');

            if (response.toUpperCase().indexOf('MLERRORMESSAGE') > -1) {
                var decoded_message = $('<div/>').html(response).text(); //message maybe encoded in the config xml so need to decode
                //$('#txtErrorMessage').html(decoded_message);
                goToDialog(decoded_message);
            }
            else {
                if (response.toUpperCase().indexOf('MLNOIDAUTHENTICATION') > -1) {
                    isApplicationCompleted = true;
                    goToLastDialog(response);
                    //need to display response first before clear all inputs
                    // clearForms();

                    //disable submit answer button 
                    $('#walletQuestions .btnSubmitAnswer').addClass('ui-disabled');
                }
              
                else if (response.toUpperCase().indexOf('WALLETQUESTION') > -1) {
                    $('input.btnSubmitAnswer').attr('disabled', false);  //need to enable for join or followon
                    isSubmitWalletAnswer = false; //allow second joint applicant to submit

                    displayWalletQuestions(response);  //TODO: testing to use the same page for both join and follow on question

                    //co_displayWalletQuestions(response);
                   
                }
                else if (response.toUpperCase().indexOf('MLPAYPAL_CREATED') > -1) {
                    window.location.href = JSON.parse(response).Info.URL;
                }
                else if (response.toUpperCase().indexOf('MLPAYPAL_FAILED') > -1) {  //fail paypal
                    goToLastDialog(JSON.parse(response).Info.APPROVED_MESSAGE); //preapproved message

                    //disable submit answer button 
                    $('#walletQuestions .btnSubmitAnswer').addClass('ui-disabled');
                } else if (response.toUpperCase().indexOf('EXPIREDMESSAGE') > -1) {
                    goToLastDialog(response);
                    //disable submit answer button 
                    $('#walletQuestions .btnSubmitAnswer').addClass('ui-disabled');
                }
                else {
                    isApplicationCompleted = true;
                    goToLastDialog(response);
                    //need to display response first before clear all inputs
                    // clearForms();

                    //disable submit answer button 
                    $('#walletQuestions .btnSubmitAnswer').addClass('ui-disabled');
                }
            }
        },
        error: function (err) {

            //enable submit button 
            $('#walletQuestions .btnSubmitAnswer').removeClass('ui-disabled');

        	goToDialog('We apologize, but unfortunately there was an error when submitting the application. Please try again later.');
        }
    });
    return false;
}





function getWalletAnswers() {
    // Grab the answer choices that have been selected by the user
    var appInfo = new Object();
    //            var selectedAnswer = $('input[name=AccountRadio]:checked').attr('id');

    var index = 0;
    var numSelected = 0;

    var numOfQuestions = $('#hdNumWalletQuestions').val();
    numOfQuestions = numWalletQuestions;
    numOfQuestions = parseInt(numOfQuestions, 10);


    var answersArray = new Array();

    for (index = 1; index <= numOfQuestions; index++) {
        // Check to see if a radio button was selected
        //                var radioButtons = $('input[name=walletquestion-radio-' + index.toString() + ']');
        var selectedAnswer = $('input[name=walletquestion-radio-' + index.toString() + ']:checked').attr('id');
        //                var numRadioButtons = radioButtons.length;
        //                for (var i = 0; i < numRadioButtons; i++) {
        //                    if ($(radioButtons[i]).prop('checked') == true) {
        //                        answersArray[i - 1] = radioButtons[i].attr('id');
        //                    }
        //                }
        if (selectedAnswer != "" && selectedAnswer != null) {
            //exclude "rq"+i+"_" prefix from selectectAnswer
            var rqPrefixLength = 4;
            if (i >= 10) {
                rqPrefixLength = 5;
            }
            answersArray[index - 1] = selectedAnswer.substring(rqPrefixLength,selectedAnswer.length);
         }
    }

    // Put the answers into a semicolon separated string
    // Only the ids of the answers will be passed to the callback code
    var walletAnswersConcatStr = "";

    for (var i = 0; i < numOfQuestions; i++) {
        walletAnswersConcatStr += answersArray[i] + ";";
    }
    // Get rid of the last semicolon
    walletAnswersConcatStr = walletAnswersConcatStr.substr(0, walletAnswersConcatStr.length - 1);
    appInfo.WalletAnswers = walletAnswersConcatStr;
    appInfo.CSRF = $('#hfCSRF').val();
    appInfo.LenderRef = $('#hfLenderRef').val();
    appInfo.Task = "WalletQuestions";
 
    if ($('#hdSessionID').length > 0) {
        appInfo.CurrentSessionID = $('#hdSessionID').val();
    }

    if ($('#co_hdSessionID').length > 0) {
        appInfo.co_CurrentSessionID = $('#co_hdSessionID').val();
    }

    appInfo.FirstName = $('#txtFName').val();
    appInfo.co_FirstName = $('#co_txtFName').val();

    appInfo.EmailAddr = $('#txtEmail').val().trim();
   
   

    return appInfo;
}

//getCustom Question
function getXACustomQuestions() {
    var XAQuestionAnswer = new Object;
    var XAQuestionAnswers = new Array();
    if ($('.CustomQuestion').length) {
        $('.CustomQuestion:not(span)').each(function () {
            var element = $(this);
            var ui_type = element.attr('type');
            if (ui_type == "checkbox") {
                if (element.prev('.ui-checkbox-on').length > 0) {

                    XAQuestionAnswer = { q: $(this).attr('iname'), a: element.attr('answer') };
                    XAQuestionAnswers.push(XAQuestionAnswer);
                }
            }
            else {
                XAQuestionAnswer = { q: $(this).attr('iname'), a: element.val() };
                XAQuestionAnswers.push(XAQuestionAnswer);
            }

        });
    }
    //condition custom question
    if ($('.ConditionedQuestion').length) {
        $('.ConditionedQuestion:not(span)').each(function () {
            var element = $(this);
            var strDisplayedCQ = getDivConditionQuestionStyle(element);
            var ui_type = element.attr('type');
            if (ui_type == "checkbox") {
                if (strDisplayedCQ.indexOf('none') == -1) {
                    if (element.prev('.ui-checkbox-on').length > 0) {
                        XAQuestionAnswer = { q: $(this).attr('iname'), a: element.attr('answer') };
                        XAQuestionAnswers.push(XAQuestionAnswer);
                    }
                }
            } else {
                if (strDisplayedCQ.indexOf('none') == -1) {
                    XAQuestionAnswer = { q: $(this).attr('iname'), a: element.val() };
                    XAQuestionAnswers.push(XAQuestionAnswer);
                }
            }
        });
    }
    return XAQuestionAnswers;
}

// This function fills out an object with preset application info
function getTestLoanInfo() {
    // Bundle up the information so that we can pass it along to the submission function.
    var appInfo = new Object();

    // Accounts (Interested Products)
    appInfo.Account1Name = "PS";
    appInfo.Account1Deposit = 10.0;
    appInfo.Account2Name = "NOT SET";
    appInfo.Account2Deposit = 0;
    appInfo.Account3Name = "NOT SET";
    appInfo.Account3Deposit = 0;

    // Applicant Info
    var txtMemberNumber = "1234567";
    var txtFName = "Marisol";
    var txtMName = "";
    var txtLName = "Testcase";
    var txtSuffix = "";
    var txtGender = "FEMALE";
    var txtDOB;
    var txtMonth = "March";
    var txtDay = "21";
    var txtYear = "1992";
    var txtMotherMaidenName = "Smith";
    var txtProfession = "Governor";
    var ddlCitizenshipStatus = "US CITIZEN";
    var txtSSN = "000000001";

    appInfo.LenderRef = $('#hfLenderRef').val();
    appInfo.FirstName = txtFName;
    appInfo.MiddleName = txtMName;
    appInfo.LastName = txtLName;
    appInfo.NameSuffix = txtSuffix;
    appInfo.Gender = txtGender;
    txtDOB = txtYear + "-" + convertMonthWordToNumber(txtMonth) + "-" + txtDay;
    appInfo.DOB = txtDOB;
    appInfo.MotherMaidenName = txtMotherMaidenName;
    appInfo.Profession = txtProfession;
    appInfo.EmploymentStatus = "EMPLOYED";
    appInfo.EmployedMonths = "24";
    appInfo.Employer = "US Government";
    appInfo.Citizenship = ddlCitizenshipStatus;
    appInfo.SSN = txtSSN;
    appInfo.MemberNumber = txtMemberNumber;
    // CO-APPLICANT
    appInfo.co_FirstName = "David";
    appInfo.co_MiddleName = "";
    appInfo.co_LastName = "Testcase";
    appInfo.co_NameSuffix = "";
    appInfo.co_Gender = "MALE";
    txtDOB = txtYear + "-" + convertMonthWordToNumber(txtMonth) + "-" + txtDay;
    appInfo.co_DOB = "1980-08-22";
    appInfo.co_MotherMaidenName = "Johnson";
    appInfo.co_Profession = "Gunner";
    appInfo.co_EmploymentStatus = "EMPLOYED";
    appInfo.co_EmployedMonths = "24";
    appInfo.co_Employer = "US Government";
    appInfo.co_Citizenship = "US CITIZEN";
    appInfo.co_SSN = "000000002";

    // Applicant Contact Info
    var txtEmail = "marisol@testing.org";
    var txtHomePhone = "0000000000";
    var txtMobilePhone = "";
    var txtWorkPhone = "";
    var txtWorkPhoneEXT = "";
    var ddlContactMethod = "HOME";
    appInfo.EmailAddr = txtEmail;
    appInfo.HomePhone = txtHomePhone;
    appInfo.MobilePhone = txtMobilePhone;
    appInfo.WorkPhone = txtWorkPhone;
    appInfo.WorkPhoneEXT = txtWorkPhoneEXT;
    appInfo.ContactMethod = ddlContactMethod;
    // co app
    appInfo.co_EmailAddr = "david@testing.org";;
    appInfo.co_HomePhone = txtHomePhone;
    appInfo.co_MobilePhone = txtMobilePhone;
    appInfo.co_WorkPhone = txtWorkPhone;
    appInfo.co_WorkPhoneEXT = txtWorkPhoneEXT;
    appInfo.co_ContactMethod = ddlContactMethod;

    // Applicant ID
    var ddlIDCardType = "BIRTH_CERT";
    var txtIDCardNumber = "123123";
    var txtIDDateIssued = "1980-01-01";
    var txtIDDateExpire = "2013-12-12";
    var ddlIDState = "";
    var ddlIDCountry = "";
    appInfo.IDCardType = ddlIDCardType;
    appInfo.IDCardNumber = txtIDCardNumber;
    appInfo.IDDateIssued = txtIDDateIssued;
    appInfo.IDDateExpire = txtIDDateExpire;
    appInfo.IDState = ddlIDState;
    appInfo.IDCountry = ddlIDCountry;
    // Co app
    appInfo.co_IDCardType = ddlIDCardType;
    appInfo.co_IDCardNumber = "99999";
    appInfo.co_IDDateIssued = txtIDDateIssued;
    appInfo.co_IDDateExpire = txtIDDateExpire;
    appInfo.co_IDState = ddlIDState;
    appInfo.co_IDCountry = ddlIDCountry;

    // Applicant Address
    var txtAddress = "1234 Nowhere Lane";
    var txtZip = "92660";
    var txtCity = "Newport Beach";
    var ddlState = "CA";
    appInfo.AddressStreet = txtAddress;
    appInfo.AddressZip = txtZip;
    appInfo.AddressCity = txtCity;
    appInfo.AddressState = ddlState;
    // co app
    appInfo.co_AddressStreet = txtAddress;
    appInfo.co_AddressZip = txtZip;
    appInfo.co_AddressCity = txtCity;
    appInfo.co_AddressState = ddlState;

    return appInfo;
}

//this create selectProducts object
function selectProducts(productName, depositAmount, productQuestions, productServices) {
    this.productName = productName;
    this.depositAmount = depositAmount;
    this.productQuestions = productQuestions;
    this.productServices = productServices ? productServices : [];
}
function getHiddenProductData() {
    var currProductData = [];
    var currProductCodes = [];
    var currProductQuestions = {};
    var currDepositAmount = 0;
    var currProductServices = [];  
    if (PRODUCTCODES != "") {
        currProductCodes = JSON.parse(PRODUCTCODES);
    }
    for (var i = 0; i<currProductCodes.length; i++) {
        currProductData.push(new selectProducts(currProductCodes[i],currDepositAmount,currProductQuestions,currProductServices));
    }
    return currProductData;
}
//For main Applicant only + call other for coapp
function getApplicantInfo() {
    // Bundle up the information that the user has entered so that we can pass it along to the submission function.
    var appInfo = new Object();

    //add product to product array
    appInfo.SelectedProducts = JSON.stringify(getHiddenProductData());
 
    appInfo.TypeAccount = $('#hdAvailability').val();
    //get all disclosures 
    var disclosures = "";
    var temp = "";
    /* if ($('.checkDisclosure').val() != undefined) {
         $('.checkDisclosure').each(function () {
             temp = $(this).text().trim().replace(/\s+/g, " ");
             disclosures += temp + "\n";
         });
         appInfo.Disclosure = disclosures;
     }*/
	if ($("#hdEnableDisclosureSection").val() === "Y") {
		if (parseDisclosures() != "") {
			appInfo.Disclosure = parseDisclosures();
		}
	}
	appInfo.isLive = ISLIVE;
    appInfo.MediShareNumber = MEDISHARENUMBER;
    appInfo.HouseHoldNumber = HOUSEHOLDNUMBER;
    appInfo.PlatformSource = $('#hdPlatformSource').val();
    appInfo.LenderRef = $('#hfLenderRef').val();
    appInfo.BranchID = $('#hfBranchId').val();
    appInfo.CSRF = $('#hfCSRF').val();
    appInfo.ReferralSource = $('#hfReferralSource').val();
   
    SetHiddenApplicantAddress(appInfo);
    
    SetHiddenContactInfo(appInfo);

    setHiddenApplicantEmployment(appInfo);

    SetHiddenApplicantID(appInfo);
  
    SetHiddenApplicantInfo(appInfo);
   
    appInfo.IsJoint = "false";
    appInfo.Availability = getParameterByName("type");
    if (HASCOAPP == "Y") {
        appInfo.IsJoint = "true";
    }

    appInfo.XAQuestionAnswers = JSON.stringify(HIDDENQUESTIONS);
 
    appInfo.rawFundingSource = '';
   
    return appInfo;
}
//For main Applicant only + call other for coapp
function getApplicantInfo_original() {
    // Bundle up the information that the user has entered so that we can pass it along to the submission function.
        var appInfo = new Object();
        var sProductInfoArray = new Array();
        
        appInfo.TypeAccount = $('#hdAvailability').val();
        //set special account type
        setSpecialAccountType(appInfo);
        //get all disclosures 
        var disclosures = "";
        var temp = "";
        /* if ($('.checkDisclosure').val() != undefined) {
             $('.checkDisclosure').each(function () {
                 temp = $(this).text().trim().replace(/\s+/g, " ");
                 disclosures += temp + "\n";
             });
             appInfo.Disclosure = disclosures;
         }*/
	if ($("#hdEnableDisclosureSection").val() === "Y") {
		if (parseDisclosures() != "") {
			appInfo.Disclosure = parseDisclosures();
		}
	}
	appInfo.HouseHoldNumber = HOUSEHOLDNUMBER;
        appInfo.PlatformSource = $('#hdPlatformSource').val();
        appInfo.LenderRef = $('#hfLenderRef').val();
        appInfo.BranchID = $('#hfBranchId').val();
        appInfo.CSRF = $('#hfCSRF').val();
        appInfo.ReferralSource = $('#hfReferralSource').val();
   
        //FIX   FORMAT FOR dob
        var $DOB = $('#txtDOB');
        var txtDOB = $DOB.val();
        if (!Common.ValidateDate(txtDOB)) {
        } else {
            ValidateDateOfBirth(false, txtDOB, $DOB);
        }
        SetApplicantInfo(appInfo);
	if ($("#hdEnableEmploymentSection").val() === "Y") {
		SetApplicantEmployment(appInfo);
	}
	SetContactInfo(appInfo);
	if ($("#hdEnableIDSection").val() === "Y") {
		SetApplicantID(appInfo);
	}

	SetApplicantAddress(appInfo);
	if ($('#hdEnableOccupancyStatusSection').val() == 'Y') {
		SetApplicantPreviousAddress(appInfo);
	}
	SetApplicantMailingAddress(appInfo);
        
        EMPLogicPI.setValueToAppObject(appInfo);
        appInfo.ApplicantQuestionAnswers = JSON.stringify(getXAApplicantQuestions());
        appInfo.IsJoint = "false";
        appInfo.Availability = getParameterByName("type");
        if (HASCOAPP == "Y") {
            appInfo.IsJoint = "true";
        }

        if ($("#hdHasCoApplicant").val() === "Y") {
            //FIX   FORMAT FOR dob
            var $DOB = $('#co_txtDOB');
            var txtDOB = $DOB.val();
            if (!Common.ValidateDate(txtDOB)) {
            } else {
                ValidateDateOfBirth(false, txtDOB, $DOB);
            }
            co_SetApplicantInfo(appInfo);
	        if ($("#hdEnableEmploymentSectionForCoApp").val() === "Y") {
		        co_SetApplicantEmployment(appInfo);
	        }
	        co_SetContactInfo(appInfo);
	        if ($("#hdEnableIDSectionForCoApp").val() === "Y") {
		        co_SetApplicantID(appInfo);
	        }
	        co_SetApplicantAddress(appInfo);
	        if ($('#hdEnableOccupancyStatusSectionForCoApp').val() == 'Y') {
		        co_SetApplicantPreviousAddress(appInfo);
	        }
	        co_SetApplicantMailingAddress(appInfo);
            appInfo.co_ApplicantQuestionAnswers = JSON.stringify(co_getXAApplicantQuestions());
            appInfo.IsJoint = "true";
        }
        //minor Applicant
        appInfo.HasMinorApplicant = $('#hdHasMinorApplicant').val();
        if (appInfo.HasMinorApplicant == 'Y') {
        	m_SetApplicantInfo(appInfo);
	        if ($("#hdEnableEmploymentSectionForMinor").val() === "Y") {
		        m_SetApplicantEmployment(appInfo);
	        }
	        m_SetContactInfo(appInfo);
	        if ($("#hdEnableIDSectionForMinor").val() === "Y") {
		        m_SetApplicantID(appInfo);
	        }
	        m_SetApplicantAddress(appInfo);
            m_SetApplicantMailingAddress(appInfo);
            //get minor applicant questions
            appInfo.m_ApplicantQuestionAnswers = JSON.stringify(m_getXAApplicantQuestions());
        }
        //setESignature(appInfo);
        //end minor applicant
        //get the custom_question_answer
    //appInfo.XAQuestionAnswers = JSON.stringify(getXACustomQuestions());
        appInfo.XAQuestionAnswers = JSON.stringify(HIDDENQUESTIONS);
        if (hasFOM()) {
            setFOM(appInfo);
        }
    
        //FS.loadValueToData();
        //FS1.loadValueToData();
        //appInfo.rawFundingSource = JSON.stringify(FS1.Data);
        appInfo.rawFundingSource = '';
        //set UploadDocument
        //xaSetUploadDocument(appInfo);
        //SetUploadDocument(appInfo, $("#hdHasCoApplicant").val());

        return appInfo;
    }

function goToDialog(message) {
	if ($("#divErrorPopup").length > 0) {
		$('#txtErrorPopupMessage').html(message);  //error message are not encoded, endoding will remove html tag
		$("#divErrorPopup").popup("open", { "positionTo": "window" });
	} else {
		$('#txtErrorMessage').html(message);  //error message are not encoded, endoding will remove html tag
		$('#href_show_dialog_1').trigger('click');
	}
}
function goToLastDialog(message) {
    //already decode message in xa callback code behind
    //var decoded_message = $('<div/>').html(message).text(); //message maybe encoded in the config xml so need to decode
    //get the dropdown funding source type
    var ddlFundingSource = $('#divFundingSource option:selected').val();
  
    if (isApplicationCompleted) {
        if (ddlFundingSource == "MAIL A CHECK" && $('#hdCheckMailAddress').val() != "") {
            document.getElementById("divCheckMailAddress").style.display = "block";
        }
        //$('#txtTitleMessage').html("Application Completed");
        //$('#strResponseMessage').html("Application Completed");
        //clear all user inputs after application is completed
        clearForms();
    } else {
        //$('#txtTitleMessage').html("System Message");
        //$('#strResponseMessage').html("System Message");
    }
    //$('#div_system_message').html(decoded_message);
    $('#div_system_message').html(message);
    $('#href_show_last_dialog').trigger('click');

}

function displayWalletQuestions(message) {
    // The first two characters of the message contains the number of questions
    var numQuestions = parseInt(message.substr(0, 2), 10);
    var questionsHTML = message.substr(2, message.length - 2);

    // Put the number of questions into the hidden input field
    $('#hdNumWalletQuestions').val(numQuestions);
    numWalletQuestions = numQuestions;

    // Put the HTML for the questions into the proper location in our page
    $('#walletQuestionsDiv').html('');
    $('#walletQuestionsDiv').html(questionsHTML);
    $('#walletQuestionsDiv').trigger("create");  //need to have this for 2nd time usage so jQM can render the style
    var nextpage = $('#walletQuestions');
    $.mobile.changePage(nextpage, {
        transition: "slide",
        reverse: false,
        changeHash: true
    });
}

function autoFillData() {
    //fom
    
    //funding
    $('#fsFundingType').val("PAYPAL");

    // Applicant Info
    $('#txtMemberNumber').val("123456");
    $('#txtFName').val("Marisol");
    $('#txtMName').val("");
    $('#txtLName').val("Testcase");
    //	$('#suffix option:selected').val();
    $('#gender').val("FEMALE");
    $('#txtDOB1').val("03");
    $('#txtDOB2').val("14");
    $('#txtDOB3').val("1980");
    $('#txtDOB').val("03/04/1980");
    //    'vivisble
    $('#txtMotherMaidenName').val("Smith");

    //  $('#ddlCitizenshipStatus').val("USCITIZEN");
   // $('#ddlEmploymentStatus').val("STUDENT");
    $('#txtJobTitle').val("Butcher");
    $('#txtSSN1').val("000");
    $('#txtSSN2').val("00");
    $('#txtSSN3').val("0001");
    $('#txtSSN').val("000-00-0001");
    $("#txtEmployer").val("Albertsons");
    $('#txtProfessionDuration_year').val("12");
    $('#txtJobTitle').val("stock trader");
    //		// Applicant Contact Info
    $('#txtEmail').val("marisol@test.com");
    $('#txtHomePhone').val("999-999-9999");
    //$('#preferredContactMethod').val("Email");
    /*$('#txtIDDateIssued').val("11/11/2009");
    $('#txtIDDateExpire').val("11/11/2017");*/

    //		var txtMobilePhone = $('#txtWorkPhone').val();
    //		var txtWorkPhone = $('#txtWorkPhone').val();
    //		var txtWorkPhoneEXT = $('#txtWorkPhone').val();
    //		var ddlContactMethod = $('#preferredContactMethod option:selected').val();
    //		appInfo.EmailAddr = txtEmail;
    //		appInfo.HomePhone = txtHomePhone;
    //		appInfo.MobilePhone = txtMobilePhone;
    //		appInfo.WorkPhone = txtWorkPhone;
    //		appInfo.WorkPhoneEXT = txtWorkPhoneEXT;
    //		appInfo.ContactMethod = ddlContactMethod;
    //$('#preferredContactMethod').val("Email");

    //		// Applicant ID
    //		var ddlIDCardType = $('#ddlIDCardType option:selected').val();
    $('#txtIDCardNumber').val("1111111111");
    $('#txtIDDateIssued').val("12/12/1999");
    $('#txtIDDateIssued1').val("12");
    $('#txtIDDateIssued2').val("12");
    $('#txtIDDateIssued3').val("1999");
    $('#txtIDDateExpire').val("12/12/2020");
    $('#txtIDDateExpire1').val("12");
    $('#txtIDDateExpire2').val("12");
    $('#txtIDDateExpire3').val("2020");
    $('#ddlIDState').val("CA");
    //$('#ddlIDState').selectmenu('refresh'); //testing purpose
    //		var ddlIDCountry = $('#ddlIDCountry option:selected').val();
    //		appInfo.IDCardType = ddlIDCardType;
    //		appInfo.IDCardNumber = txtIDCardNumber;
    //		appInfo.IDDateIssued = txtIDDateIssued;
    //		appInfo.IDDateExpire = txtIDDateExpire;
    //		appInfo.IDState = ddlIDState;
    //		appInfo.IDCountry = ddlIDCountry;

    //		// Applicant Address
    $('#txtAddress').val("1234 Square Circle");
    $('#txtZip').val("92660");
    $('#txtCity').val("Newport Beach");
    $('#ddlState').val("CA");
    $('#ddlOccupyingStatus').val("RENT");
    $('#ddlOccupancyDurationYear').val("4");
    //		appInfo.AddressStreet = txtAddress;
    //		appInfo.AddressZip = txtZip;
    //		appInfo.AddressCity = txtCity;
    //		appInfo.AddressState = ddlState;



    //		var year = $('#txtLengthOfEmploymentYear').val();
    //		var month = $('#ddlLengthOfEmploymentMonth option:selected').val();
    //		var x = year == '' ? 0 : parseInt(year);
    //		var y = $.trim(month) == '' ? 0 : parseInt(month);
    //		appInfo.EmployedMonths = x * 12 + y;

    //		//	year = $('#<%=IDPrefix%>txtLengthOfEmploymentYear').val();
    //		//	month = $('#<%=IDPrefix%>ddlLengthOfEmploymentMonth option:selected').val();
    //		//	x = year == '' ? 0 : parseInt(year);
    //		//	y = $.trim(month) == '' ? 0 : parseInt(month);
    //		//	appInfo.co_EmployedMonths = x * 12 + y;

    //		//            SetApplicantFinancialInfo(appInfo);

    //		appInfo.IsJoint = false;
    //		if (Common.bHasCoApp) {
    //			co_SetApplicantInfo(appInfo);
    //			co_SetContactInfo(appInfo);
    //			co_SetApplicantID(appInfo);
    //			co_SetApplicantAddress(appInfo);
    //			appInfo.IsJoint = true;
    //		}

    // CO APP DATA
    $('#co_txtMemberNumber').val("654321");
    $('#co_txtFName').val("David");
    $('#co_txtMName').val("");
    $('#co_txtLName').val("Testcase");
    //	$('#suffix option:selected').val();
    $('#co_gender').val("MALE");
    /*$('#co_selectChoiceMonth').val("03");
    $('#co_selectChoiceDay').val("14");
    $('#co_selectChoiceYear').val("1975");*/

    $('#co_txtDOB').val("03/14/1975");
    $('#co_txtDOB1').val("03");
    $('#co_txtDOB2').val("14");
    $('#co_txtDOB3').val("1975");
    $('#co_txtMotherMaidenName').val("Smith");
    $('#co_txtProfession').val("Sniper");

    $('#co_ddlCitizenshipStatus').val("US CITIZEN");
  //  $('#co_ddlEmploymentStatus').val("EMPLOYED");

    $('#co_txtSSN').val("000-00-0002");
    $('#co_txtSSN1').val("000");
    $('#co_txtSSN2').val("00");
    $('#co_txtSSN3').val("0002");


    $("#co_txtEmployer").val("Army");
    $('#co_txtLengthOfEmploymentYear').val("12");

    //		// Applicant Contact Info
    $('#co_txtEmail').val("david@test.com");
    $('#co_txtHomePhone').val("999-999-9999");

    $('#co_txtIDCardNumber').val("222222222");
    $('#co_txtIDDateIssued').val("10/12/1999");
    $('#co_txtIDDateIssued1').val("10");
    $('#co_txtIDDateIssued2').val("12");
    $('#co_txtIDDateIssued3').val("1999");
    $('#co_txtIDDateExpire').val("10/12/2020");
    $('#co_txtIDDateExpire1').val("10");
    $('#co_txtIDDateExpire2').val("12");
    $('#co_txtIDDateExpire3').val("2020");
    $('#co_ddlIDState').val("CA");

    //		// Applicant Address
    $('#co_txtAddress').val("1234 Square Circle");
    $('#co_txtZip').val("92660");
    $('#co_txtCity').val("Newport Beach");
    $('#co_ddlState').val("CA");

    // prefill data for Name On Account of Fund Your Account screen in case TFAFI is default selected
    $('#bNameOnCard').val("Marisol Testcase");

    // MINOR APP DATA
    $('#m_txtFName').val("minorDavid");
    $('#m_txtMName').val("");
    $('#m_txtLName').val("minorTestcase");
    //	$('#suffix option:selected').val();
    $('#co_gender').val("MALE");
    /*$('#co_selectChoiceMonth').val("03");
    $('#co_selectChoiceDay').val("14");
    $('#co_selectChoiceYear').val("1975");*/

    $('#m_txtDOB').val("03/14/1999");
    $('#m_txtDOB1').val("03");
    $('#m_txtDOB2').val("14");
    $('#m_txtDOB3').val("1999");
    $('#m_txtMotherMaidenName').val("cindy");
    $('#m_txtProfession').val("eat");

    $('#m_ddlCitizenshipStatus').val("US CITIZEN");

    $('#m_txtSSN').val("000-00-0003");
    $('#m_txtSSN1').val("000");
    $('#m_txtSSN2').val("00");
    $('#m_txtSSN3').val("0003");


    $("#m_txtEmployer").val("Army");
    $('#m_txtLengthOfEmploymentYear').val("1");

    //minor Applicant Contact Info
    $('#m_txtEmail').val("minordavid@test.com");
    $('#m_txtHomePhone').val("555-999-9999");

    $('#m_txtIDCardNumber').val("1222224567");
    $('#m_txtIDDateIssued').val("10/12/1997");
    $('#m_txtIDDateIssued1').val("10");
    $('#m_txtIDDateIssued2').val("12");
    $('#m_txtIDDateIssued3').val("1997");
    $('#m_txtIDDateExpire').val("10/12/2019");
    $('#m_txtIDDateExpire1').val("10");
    $('#m_txtIDDateExpire2').val("12");
    $('#m_txtIDDateExpire3').val("2019");

    // minor Applicant Address
    $('#m_txtAddress').val("123 west");
    $('#m_txtZip').val("92841");
    $('#m_txtCity').val("Garden grove");
    $('#m_ddlState').val("CA");
}

var BASE64_MARKER = ';base64,';

function convertDataURIToBinary(dataURI) {
    var base64Index = dataURI.indexOf(BASE64_MARKER) + BASE64_MARKER.length;
    var base64 = dataURI.substring(base64Index);
    var raw = window.atob(base64);
    var rawLength = raw.length;
    var array = new Uint8Array(new ArrayBuffer(rawLength));

    for (i = 0; i < rawLength; i++) {
        array[i] = raw.charCodeAt(i);
    }
    return array;
}

//check date string from this format:
//		MM-YYYY
function checkMonthYear(date) {
    var minYear = 1902;
    var maxYear = (new Date()).getFullYear() + 20;
    if ($.trim(date) == '') return false;

    // regular expression to match required date format
    re = /^(\d{1,2})-(\d{4})$/;

    if (date != '') {
        if (regs = date.match(re)) {
            if (regs[1] < 1 || regs[1] > 12) {
                return false;
            } else if (regs[2] < minYear || regs[2] > maxYear) {
                return false;
            }
        } else {
            return false;
        }
    }
    return true;
}
function getUserName() {
    var txtFName = $('#txtFName').val();
    var txtMName = $('#txtMName').val();
    var txtLName = $('#txtLName').val();
    //capitalize the first character
    txtFName = txtFName.replace(txtFName.charAt(0), txtFName.charAt(0).toUpperCase());
    txtMName = txtMName.replace(txtMName.charAt(0), txtMName.charAt(0).toUpperCase());
    txtLName = txtLName.replace(txtLName.charAt(0), txtLName.charAt(0).toUpperCase());
    var userName = "";
    if (txtMName != "")
        userName = txtFName + " " + txtMName + " " + txtLName;
    else
        userName = txtFName + " " + txtLName;
    return userName;
}



function convertMonthWordToNumber(monthWord) {
    // Make sure that month is a string
    monthWord = String(monthWord);

    switch (monthWord.toUpperCase()) {
        case "JANUARY":
            return "01";
        case "FEBRUARY":
            return "02";
        case "MARCH":
            return "03";
        case "APRIL":
            return "04";
        case "MAY":
            return "05";
        case "JUNE":
            return "06";
        case "JULY":
            return "07";
        case "AUGUST":
            return "08";
        case "SEPTEMBER":
            return "09";
        case "OCTOBER":
            return "10";
        case "NOVEMBER":
            return "11";
        case "DECEMBER":
            return "12";
        default:
            // It's always January if we can't figure out what it is
            return "01";
    }





}
//

///////////////////
// FUNDING SOURE //
///////////////////
function handleFSFooter() {
    var divLogoHeight = $('.divOtherLogo').height();
    var divDepositInputHeight = $('#DivDepositInput').height();
    var element = $('#pageFS');
    var screenContentHeight = $(window).height() - 82;
    $('#divFundingSource').on('change', function (e) {
        var elementID = e.target.id;
        if (elementID == 'fsFundingType') {
            var divFSHeight = $(this).height();
            var fsContentHeight = divLogoHeight + divDepositInputHeight + divFSHeight;
            if (element.attr('id') != "divErrorDialog") {
                if (fsContentHeight < screenContentHeight) {
                    element.find('div[data-role="content"]').height(screenContentHeight);
                } else {
                    element.find('div[data-role="content"]').height(fsContentHeight + 82);
                }
                return false;
            }
        }
    });
}
//var FS = {};
//FS.DivID = '#divFundingSource';
//FS.Data = _FundingSourcePackage;


//FS.buildHtmlForType = function (type) {
//    if (type == 'CREDIT CARD') {
//        return FS.buildCCHtml();
//    }
//    if (type == 'TRANSFER FROM ANOTHER FINANCIAL INSTITUTION') {
//        return FS.buildBankHtml();
//    }
//    if (type == 'INTERNAL TRANSFER') {
//        return FS.buildTransferHtml();
//    }
//    return '';
//}

//FS.SafeValue = function (id) {
//    return $(id).length > 0 ? $(id).val() : '';
//}

//FS.loadValueToData = function () {
//    this.Data.cNameOnCard = this.SafeValue('#cNameOnCard');
//    this.Data.cCardType = this.SafeValue('#cCardType');
//    this.Data.cCreditCardNumber = this.SafeValue('#cCreditCardNumber');
//    this.Data.cExpirationDate = this.SafeValue('#cExpirationDate');
//    this.Data.cCVNNumber = this.SafeValue('#cCVNNumber');
//    this.Data.cBillingAddress = this.SafeValue('#cBillingAddress');

//    this.Data.cBillingZip = this.SafeValue('#cBillingZip');
//    this.Data.cBillingCity = this.SafeValue('#cBillingCity');
//    this.Data.cBillingState = this.SafeValue('#cBillingState');

//    this.Data.bAccountType = this.SafeValue('#bAccountType');
//    this.Data.bNameOnCard = this.SafeValue('#bNameOnCard');
//    this.Data.bAccountNumber = this.SafeValue('#bAccountNumber');
//    this.Data.bRoutingNumber = this.SafeValue('#bRoutingNumber');
//    this.Data.bBankName = this.SafeValue('#bBankName');
//    this.Data.bBankState = this.SafeValue('#bBankState');

//    this.Data.tAccountNumber = this.SafeValue('#tAccountNumber');
//    this.Data.tAccountType = this.SafeValue('#tAccountType');
//    //missing fundingtype-> need to load fsfunding type
//    this.Data.fsFundingType = this.SafeValue('#fsFundingType');
//}

//FS.buildTransferHtml = function () {

//    var html = '';
//    html += "<div data-role='fieldcontain'><label>Account Number <span style='color:red'>*</span></label>";
//    html += "<input type='tel' id='tAccountNumber' class='required numeric' maxlength ='20'/>";
//    html += "</div>";

//    html += "<div data-role='fieldcontain'><label>Account Type</label>";
//    html += "<select id='tAccountType'>";
//    html += "<option value='CHECKING'>CHECKING</option>";
//    html += "<option value='MONEY_MARKET'>MONEY MARKET</option>";
//    html += "<option value='SAVINGS'>SAVINGS</option>";
//    html += "</select>";
//    html += "</div>";

//    return html;
//}

//FS.buildCCHtml = function () {
//    var html = '';
//    html += "<div data-role='fieldcontain'><label>Card Type<span style='color:red'>*</span></label>";
//    html += "<select id='cCardType' >";
//    html += "<option value=''>--Please Select--</option>";
//    html += "<option value='VISA'>VISA</option>";
//    html += "<option value='MASTERCARD'>MasterCard</option>";
//    html += "</select>";
//    html += "</div>";

//    html += "<div data-role='fieldcontain'><label>Credit Card Number(xxxx-xxxx-xxxx-xxxx) <span style='color:red'>*</span></label>";
//    if (isMobile.Android()) {
//        html += "<input type='tel'   id='cCreditCardNumber' maxlength ='19' class='required inCCardNumber'/>";
//    }
//    else {
//        html += "<input type='text'  pattern='[0-9]*' id='cCreditCardNumber' maxlength ='19' class='required inCCardNumber'/>";

//    }
//    html += "</div>";
    
//    html += "<div data-role='fieldcontain'><label>Expiration Date<i>(mm-yyyy)</i> <span style='color:red'>*</span></label>";
//    if (isMobile.Android()) {
//        html += "<input type='tel' id='cExpirationDate' maxlength ='7' class='required inCardExpiredDate'/>";

//    }
//    else {
//        html += "<input type='text' pattern='[0-9]*' id='cExpirationDate' maxlength ='7' class='required inCardExpiredDate'/>";
//    }
//    html += "</div>";

//    html += "<div data-role='fieldcontain'><label>CVN Number (xxx) <span style='color:red'>*</span></label>";
//    html += "<input type='tel'  id='cCVNNumber' class='required numeric' maxlength='3'/>";
//    html += "</div>";
//    //add a button to copy address
//    html += "<div class ='divCopyAddress' id='copyAddress' style='width:100%; text-align:right'>";
//    //html += "<input type='button' id='cBtnCopyAddress' data-theme='" + _FooterThemeData + "' value ='Same Name and Address' />"
//    html += "<a style='cursor: pointer' name='cBtnCopyAddress' id='cBtnCopyAddress'><i>Same Name and Address</i></a>";
//    html += "</div>";
//    //add name on card over here
//    html += "<div data-role='fieldcontain'><label>Name On Card <span style='color:red'>*</span></label>";
//    html += "<input type='text' id='cNameOnCard' class='required' maxlength ='50'/>";
//    html += "</div>";
//    html += "<div data-role='fieldcontain'><label>Billing Address<span style='color:red'>*</span></label>";
//    html += "<input type='text' id='cBillingAddress' class='required' maxlength='100'/>";
//    html += "</div>";
    
//    html += "<div data-role='fieldcontain'><label>Billing Zip<span style='color:red'>*</span></label>";
//    html += "<input type='tel' id='cBillingZip' class='numeric required' maxlength ='5'/>";
//    html += "</div>";
    
//    html += "<div data-role='fieldcontain'><label>Billing City<span style='color:red'>*</span></label>";
//    html += "<input type='text' id='cBillingCity' class ='required' maxlength ='50'/>";
//    html += "</div>";
    
//    html += "<div data-role='fieldcontain'><label>Billing State <span style='color:red'>*</span></label>";
//    html += "<select id='cBillingState'>";
//    html += this.StateOptionHtml();
//    html += '</select>';
//    html += "</div>";

//    return html;
//}

//FS.StateOptionHtml = function () {
//    html = '';
//    html += '<option value="">--Please Select--</option>';
   
//    html += '<option value="AL">Alabama</option>';
//    html += '<option value="AK">Alaska</option>';    
//    html += '<option value="AS">American Samoa</option>';
//    html += '<option value="AZ">Arizona</option>';
//    html += '<option value="AR">Arkansas</option>';
//    html += '<option value="AA">Armed Forces - Americas</option>';
//    html += '<option value="AE">Armed Forces – Europe</option>';
//    html += '<option value="AP">Armed Forces – Pacific</option>';
//    html += '<option value="CA">California</option>';
//    html += '<option value="CO">Colorado</option>';
//    html += '<option value="CT">Connecticut</option>';
//    html += '<option value="DE">Delaware</option>';
//    html += '<option value="DC">District Of Columbia</option>';
//    html += '<option value="FL">Florida</option>';
//    html += '<option value="GA">Georgia</option>';
//    html += '<option value="GU">Guam</option>';
//    html += '<option value="HI">Hawaii</option>';
//    html += '<option value="ID">Idaho</option>';
//    html += '<option value="IL">Illinois</option>';
//    html += '<option value="IN">Indiana</option>';
//    html += '<option value="IA">Iowa</option>';
//    html += '<option value="KS">Kansas</option>';
//    html += '<option value="KY">Kentucky</option>';
//    html += '<option value="LA">Louisiana</option>';
//    html += '<option value="ME">Maine</option>';
//    html += '<option value="MD">Maryland</option>';
//    html += '<option value="MA">Massachusetts</option>';
//    html += '<option value="MI">Michigan</option>';
//    html += '<option value="MN">Minnesota</option>';
//    html += '<option value="MS">Mississippi</option>';
//    html += '<option value="MO">Missouri</option>';
//    html += '<option value="MT">Montana</option>';
//    html += '<option value="NE">Nebraska</option>';
//    html += '<option value="NV">Nevada</option>';
//    html += '<option value="NH">New Hampshire</option>';
//    html += '<option value="NJ">New Jersey</option>';
//    html += '<option value="NM">New Mexico</option>';
//    html += '<option value="NY">New York</option>';
//    html += '<option value="NC">North Carolina</option>';
//    html += '<option value="ND">North Dakota</option>';
//    html += '<option value="OH">Ohio</option>';
//    html += '<option value="OK">Oklahoma</option>';
//    html += '<option value="OR">Oregon</option>';
//    html += '<option value="PA">Pennsylvania</option>';
//    html += '<option value="PR">Puerto Rico</option>';
//    html += '<option value="RI">Rhode Island</option>';
//    html += '<option value="SC">South Carolina</option>';
//    html += '<option value="SD">South Dakota</option>';
//    html += '<option value="TN">Tennessee</option>';
//    html += '<option value="TX">Texas</option>';
//    html += '<option value="UT">Utah</option>';   
//    html += '<option value="VT">Vermont</option>';
//    html += '<option value="VI">Virgin Islands</option>';
//    html += '<option value="VA">Virginia</option>';
//    html += '<option value="WA">Washington</option>';
//    html += '<option value="WV">West Virginia</option>';
//    html += '<option value="WI">Wisconsin</option>';
//    html += '<option value="WY">Wyoming</option>';

//    return html;
//}

//FS.buildBankHtml = function () {
//    // prefill Name On Account when BankHtml is rendered
//    var fName = $('#txtFName').val();
//    var mName = $('#txtMName').val();
//    var lName = $('#txtLName').val();
//    var fullName = (fName ? fName : '') + (mName ? ' ' + mName + ' ' : ' ') + (lName ? lName : '');
//    // encode fullName html using jquery
//    var name = $('<div/>').text(fullName).html().replace(/'/g, "&apos;");
    
//    var html = '';

//    html += "<div data-role='fieldcontain'><label>Account Type</label>";
//    html += "<select id='bAccountType'>";
//    html += "<option value='CHECKING'>CHECKING</option>";
//    html += "<option value='SAVINGS'>SAVINGS</option>";
//    html += "</select>";
//    html += "</div>";

//    html += "<div data-role='fieldcontain'><label>Name On Account <span style='color:red'>*</span></label>";
//    html += "<input type='text' id='bNameOnCard' class='required' maxlength ='50' value='" + name + "' />";
//    html += "</div>";

//    html += "<div data-role='fieldcontain'><label>Account Number <span style='color:red'>*</span>&nbsp;<a href='#popupQuestionMark' data-rel='popup' data-transition='pop' data-position-to='window'><div class='questionMark'></div></a></label>";
//    html += "<input type='tel' id='bAccountNumber' class='required numeric' maxlength ='20'/>";
//    html += "</div>";

//    html += "<div data-role='fieldcontain'><label>Routing Number <span style='color:red'>*</span>&nbsp;<a href='#popupQuestionMark' data-rel='popup' data-transition='pop' data-position-to='window'><div class='questionMark'></div></a></label>";
//    html += "<input type='tel'  id='bRoutingNumber' class='required numeric' maxlength ='9'/>";
//    html += "</div>";


//    html += "<div data-role='fieldcontain'><label>Financial Institution <span style='color:red'>*</span></label>";
//    html += "<input type='text' id='bBankName' class='required' maxlength ='50'/>";
//    html += "</div>";

//    html += "<div data-role='fieldcontain'><label>Financial Institution State<span style='color:red'>*</span></label>";
//    html += "<select id='bBankState'>";
//    html += this.StateOptionHtml();
//    html += '</select>';
//    html += "</div>";

//    return html;
//}

//FS.refreshDivContent = function () {
//    var html = '';
//    html += "<div data-role='fieldcontain'><label><b>Please select your funding option:</b></label>";
//    html += "<select id='fsFundingType'>";
//    var totalDepositAmount = Common.GetFloatFromMoney($('#txtTotalDeposit').val());
//    var ddlFundingType = "";
    
//    for (var i = 0; i < this.Data.ListTypes.length; i++) {
//        ddlFundingType = this.Data.ListTypes[i];
//        if (ddlFundingType.indexOf("NOT FUNDING") > -1 && totalDepositAmount > 0) {
//            continue;
//        }
//        html += "<option value='" + this.Data.ListTypes[i] + "'>" + this.Data.ListTypes[i] + "</option>";
//    }
//    html += '</select>';
//    html += "</div>";
//    //fixed the missing default case -> funding source page showed nothing
//   // if (this.Data.fsFundingType == null && this.Data.ListTypes[0] != "")
//     //   html += this.buildHtmlForType(this.Data.ListTypes[0]); //default case
//    //else {
//      //  html += this.buildHtmlForType(this.Data.fsFundingType);
//    //}
//    var selectedFundingType = $('#fsFundingType option:selected').val();
//    if (selectedFundingType != undefined) {
//        html += this.buildHtmlForType(selectedFundingType);
//    } else {
//        if (this.Data.ListTypes[0] != "") {
//            html += this.buildHtmlForType(this.Data.ListTypes[0]);
//        }
//    }
//    $(this.DivID).html(html);

//    bindLooupRoutingNumberEvent();

//    $('#fsFundingType').val(this.Data.fsFundingType);
//    $('#fsFundingType').change(function () {
//        FS.Data.fsFundingType = $(this).val();
//        displayProductAmountTitle();
//        FS.refreshDivContent();
//      //  handleFSFooter();        
//    });
 
//    $('.numeric').numeric();
//    $(this.DivID).trigger('create');
//    if (!isMobile.Android()) {
//        $('input.inCardExpiredDate').mask('99-9999');
//        $('input.inCCardNumber').mask('9999-9999-9999-9999');

//    }
//    else {
//        $('input.inCardExpiredDate').blur(function () {
//            var element = $(this);
//            element.attr('maxlength', '7'); // mm-yyyy ->maxlength =7
//            element.val(Common.FormatExpDate(element.val()));
//        });
//        //expired date format 
//        $('input.inCardExpiredDate').keyup(function () {
//            var element = $(this);
//            var date = element.val();
//            if (!isNaN(date)) {
//                element.attr('maxlength', '6');
//            } else {
//                element.attr('maxlength', '7');
//            }
//        });
//        $('input.inCardExpiredDate').keydown(function (event) {
//            var key = event.keyCode;
//            if (key == 32) {//disable spacing bar for expired date
//                event.preventDefault();
//            }
//        });
//        //do the FormatCCard in the common.js for global use
//        // $('input.inCCardNumber').blur(function () {
//        //    $(this).val(Common.FormatCCard($(this).val()));
//        //});
//        creditCardFormat();
//    }
//    $('input#bAccountNumber').blur(function () {
//        $(this).val(Common.GetRtNumber($(this).val()));
//    });
//    $('input#bRoutingNumber').blur(function () {
//        $(this).val(Common.GetRtNumber($(this).val()));
//    });
//    $('input#cCVNNumber').blur(function () {
//        $(this).val(Common.GetPosNumber($(this).val(), 3));
//    });
//    $('input#cBillingZip').blur(function () {
//        $(this).val(Common.GetPosNumber($(this).val(), 5));
//    });

//    //clear or copy address to billing address
//    var btnCopyAddressClick = 0;
//    $('.divCopyAddress').click(function () {
//        if (btnCopyAddressClick % 2 == 0) {
//            CopyMailingAddress();
//            $('#cNameOnCard').val(getUserName());
//        } else {
//            ClearCopyMailingAddress();
//            $('#cNameOnCard').val("");
//        }
//        btnCopyAddressClick++;
//    });
//};
//function isVisibleFundingSourceField() {
//    if ($('#divFundingSource .ui-select').length > 1) {
//        return true;
//    }
//    return false;
//}
//function getNotFundingOptionIndex(fundingTypeElement) {
//    var sLength=fundingTypeElement.length;
//    for (var i = 0; i < sLength; i++) {
//        if (fundingTypeElement.options[i].value.toUpperCase().indexOf("NOT FUNDING") > -1) {
//            return i;
//        }
//    }
//    return -1;
//}
//function addNotFundingOption(fundingTypeElement) {
//    strOption = document.createElement("option");
//    strOption.text = "NOT FUNDING AT THIS TIME";
//    strOption.value = "NOT FUNDING AT THIS TIME";
//    fundingTypeElement.add(strOption,fundingTypeElement[0]);
//}
//function handledFundingSourceType(hasChangeDepositInput) {
//    if (hasChangeDepositInput == undefined) {
//        hasChangeDepositInput = false;
//    }
//    var fundingTypeElement = document.getElementById("fsFundingType");
//    var selectedValue = '';
//    var selectedIndex = fundingTypeElement.selectedIndex;
    
//    if (selectedIndex > 0) {
//        var opt = fundingTypeElement.options[selectedIndex];
//        selectedValue = opt.value;
//    }
    
//    $('#txtTotalDeposit').val(Common.FormatCurrency(getTotalDeposit(), true));
//    var fsFundingType = $('#fsFundingType option:selected').val();
//    var totalDepositAmount = Common.GetFloatFromMoney($('#txtTotalDeposit').val());
//    var sNotFundingIndex = getNotFundingOptionIndex(fundingTypeElement);
//    if (totalDepositAmount > 0) {
//        if (sNotFundingIndex > -1) {
//            fundingTypeElement.remove(sNotFundingIndex);
//            $('#fsFundingType').selectmenu('refresh');
//        }
//        if (!isVisibleFundingSourceField()) {//to make sure all the fields of funding source are rendering
//            FS.refreshDivContent();
//        }
//    } else {
//        if (sNotFundingIndex == -1) { //add NOT FUNDING option if it 's not exist
//            addNotFundingOption(fundingTypeElement);
//            $('#fsFundingType').selectmenu('refresh');
//        }
//        if (isVisibleFundingSourceField()) {           
//            if (!hasChangeDepositInput) { 
//                if (selectedValue.toUpperCase().indexOf("NOT FUNDING") > -1) {
//                    FS.refreshDivContent();
//                }
//            } 
//        }    
//    }
//}
/////////////////////////////////
///  PRODUCT QUESTION REGION  ///
/////////////////////////////////
/* remove old format
var PCQ = {};
PCQ.DivIDPrefix = '#divProductCustomQuestionContent';
PCQ.Account1 = {};
PCQ.Account2 = {};
PCQ.Account3 = {};
function isUndefined(object) {
    return typeof object === "undefined";
}

PCQ.getValidateError = function() {
    return PCQ.Account1.getValidateError() +
        PCQ.Account2.getValidateError() +
        PCQ.Account3.getValidateError();
}*/
/* remove old format
PCQ.buildDataForAccount = function(key, dataPackage) {
    var result = {};
    result.NeedRebuildHtml = true;
    result.Questions = [];
    for (var i = 0; i < dataPackage.length; i++) {
        if (!isUndefined(dataPackage[i].ProductCode) &&
            !isUndefined(key) &&
            dataPackage[i].ProductCode.toUpperCase() == key.toUpperCase() &&
            !isUndefined(dataPackage[i].CustomQuestions)) {
            result.Questions = dataPackage[i].CustomQuestions.slice(0);
            break;
        }
    }

    / *
    #define PQ_TYPE_PASSWORD @"PASSWORD"
    #define PQ_TYPE_TEXTBOX @"TEXTBOX"
    #define PQ_TYPE_DROPDOWN @"DROPDOWN"
    #define PQ_TYPE_HEADER @"HEADER"
    #define PQ_TYPE_DATE @"DATE"
    #define PQ_TYPE_CHECKBOX @"CHECKBOX"
    * /

    result.getValidateError = function() {
        if (this.Questions.length <= 0)
            return '';
        var errorMsg = '';
        for (var i = 0; i < this.Questions.length; i++) {
            var que = this.Questions[i];

            // set value first
            switch (que.AnswerType) {
                case "HEADER":
                    continue;
                case "PASSWORD":
                case "TEXTBOX":
                case "DATE":
                    que.productAnswerText = $('#' + que.HtmlID).val();
                    break;
                case "DROPDOWN":
                    var ra_val = $('#' + que.HtmlID).val();
                    for (var j = 0; j < que.RestrictedAnswers.length; j++) {
                        var ra = que.RestrictedAnswers[j];
                        ra.isSelected = false;
                        if (ra_val == ra.Value) {
                            ra.isSelected = true;
                        }
                    }
                    break;
                case "CHECKBOX":
                    for (var j = 0; j < que.RestrictedAnswers.length; j++) {
                        var ra = que.RestrictedAnswers[j];
                        var ra_id = que.HtmlID + "_" + j;
                        ra.isSelected = $('#' + ra_id).is(':checked');
                    }
                    break;
            }

            if (que.IsRequired) {
                if (que.AnswerType != "CHECKBOX" && que.AnswerType != "DROPDOWN" && que.productAnswerText.length <= 0) {
                    errorMsg += 'Missing answer - ' + que.QuestionText + '<br/>';
                }
                else if (que.AnswerType == "CHECKBOX") {
                    var ra_answered = false;
                    for (var j = 0; j < que.RestrictedAnswers.length; j++) {
                        var ra = que.RestrictedAnswers[j];
                        var ra_id = que.HtmlID + "_" + j;
                        if ($('#' + ra_id).is(':checked')) {
                            ra_answered = true;
                            break;
                        }
                    }

                    if (!ra_answered)
                        errorMsg += 'Missing answer - ' + que.QuestionText + '<br/>';
                }
                // validate required dropdown list
                else if (que.AnswerType == "DROPDOWN")
                 {
                    var ra_val = $('#' + que.HtmlID).val();
                    for (var j = 0; j < que.RestrictedAnswers.length; j++) 
                    {
                        var ra = que.RestrictedAnswers[j];
                        ra.isSelected = false;
                        if (ra_val == ra.Value && ra_val != '') 
                        {
                            ra.isSelected = true;
                            break;
                        }

                    }
                    if (!(ra.isSelected))
                        errorMsg += 'Missing answer -' + que.QuestionText + '<br/>';
                }
            }
        }

        return errorMsg;
    }

    return result;
}*/
/* remove old format
PCQ.reloadQuestionForAccountWhenChange = function() {
    var curKey = $('input[type="radio"][name="AccountRadio"]:checked').attr('id');
    var curAccountStr = $('input[type="radio"][name="account"]:checked').val();
    switch (curAccountStr) {
        case "account1":
            PCQ.Account1 = PCQ.buildDataForAccount(curKey, _ProductQuestionPackage);
            break;
        case "account2":
            PCQ.Account2 = PCQ.buildDataForAccount(curKey, _ProductQuestionPackage);
            break;
        case "account3":
            PCQ.Account3 = PCQ.buildDataForAccount(curKey, _ProductQuestionPackage);
            break;
    }
}
function redStar(que) {
    var redstar='';
    if (que.IsRequired)
     {
         if (que.productAnswerText.length <= 0) {

             redstar = '<span style ="color:red">*<span>';
        }
        
    }
   
    return redstar;
}


PCQ.showQuestions = function() {
    // .trigger('create');
    // $('input[type="radio"][name="AccountRadio"]:checked')
    // $('input[type="radio"][name="account"]:checked')

    var curAccountStr = $('input[type="radio"][name="account"]:checked').val();

    var obj = this.Account1;
    var divHtmlId = "";
    var prefix = "";
    switch (curAccountStr) {
        case "account1":
            obj = this.Account1;
            prefix = "question_account1";
            divHtmlId = this.DivIDPrefix + "1";
            break;
        case "account2":
            obj = this.Account2;
            prefix = "question_account2";
            divHtmlId = this.DivIDPrefix + "2";
            break;
        case "account3":
            obj = this.Account3;
            prefix = "question_account3";
            divHtmlId = this.DivIDPrefix + "3";
            break;
    }

    if (!obj.NeedRebuildHtml) {
        $(PCQ.DivIDPrefix + "1").hide();
        $(PCQ.DivIDPrefix + "2").hide();
        $(PCQ.DivIDPrefix + "3").hide();
        $(divHtmlId).show();
        return;
    }
    var qContentHtml = "";
    var qHeaderHtml = "";
    var preHeaderCount = 0;
    for (var i = 0; i < obj.Questions.length; i++) {
        var qid = prefix + "_" + i;
        var qname = qid;
        var que = obj.Questions[i];
        que.HtmlID = qid;

        var qHtml = i == 0 ? "" : "<br/>";
        qHtml += "<b>" + (i + 1 - preHeaderCount) + ".</b>&nbsp;";
        switch (que.AnswerType) {
            case "PASSWORD":
                qHtml += "<label>" + que.QuestionText + redStar(que) + "</label> <br/>" +
                    "<input type='password' id='" + qid + "' name='" + qname + "' />";

                break;
            case "TEXTBOX":
                qHtml += "<label>" + que.QuestionText + redStar(que) + "</label> <br/>" +
                    "<input type='text' maxlength ='100' id='" + qid + "' name='" + qname + "'/>";
                break;
            case "DROPDOWN":
                qHtml += "<label>" + que.QuestionText + redStar(que) + "</label> <br/>" +
                    "<select id='" + qid + "' name='" + qname + "'>";
                for (var j = 0; j < que.RestrictedAnswers.length; j++) {
                    var ra = que.RestrictedAnswers[j];
                    qHtml += "<option value='" + ra.Value + "'>" + ra.Text + "</option>";
                }
                qHtml += "</select>";
                break;
            case "HEADER":
                preHeaderCount++;
                qHtml = "<br/><label><b>" + que.QuestionText + "</b></label>";
                break;
            case "DATE":
                if(isMobile.Android()){
                        qHtml += "<label>" + que.QuestionText + redStar(que) + "</label> <br/>" +
                            "<input type='tel' id='" + qid + "' name='" + qname + "' class='indate' maxlength ='10'/>";
                        }
                 else{
                        qHtml += "<label>" + que.QuestionText + redStar(que) + "</label> <br/>" +
                        "<input type='text' pattern='[0 - 9]*' id='" + qid + "' name='" + qname + "' class='indate'/>";
                  
                        }
                break;
            case "CHECKBOX":
                qHtml += "<label>" + que.QuestionText + redStar(que) + "</label> <br/>";
                qHtml += "<fieldset data-role='controlgroup'>";
                for (var j = 0; j < que.RestrictedAnswers.length; j++) {
                    var ra = que.RestrictedAnswers[j];
                    qHtml += "<input type='checkbox' name='" + qname + "' id='" + qid + "_" + j + "' value='" + ra.Value + "'/>";
                    qHtml += "<label for='" + qid + "_" + j + "'>" + ra.Text + "</label>";
                }
                qHtml += "</fieldset>";
                break;
        }

        qContentHtml += qHtml;

    }

    if (qContentHtml.length > 0) {
        qHeaderHtml = '<h3>Product Questions:</h3>'
    }

    $(PCQ.DivIDPrefix + "1").hide();
    $(PCQ.DivIDPrefix + "2").hide();
    $(PCQ.DivIDPrefix + "3").hide();
    $(divHtmlId).show();

    obj.NeedRebuildHtml = false;
    qContentHtml = qHeaderHtml + qContentHtml;
    $(divHtmlId).html(qContentHtml);
    $(divHtmlId).trigger('create');
    
    if (!isMobile.Android()) {
        $(divHtmlId + ' .indate').mask('99/99/9999');
    }
    else {
        $(divHtmlId + ' .indate').blur(function() {
            $(this).val(Common.FormatDate($(this).val()));
        });
    }
} */

//**********build new product custom question for 
var ProductCQ = {};
ProductCQ.Questions = new Array();
function isUndefined(object) {
    return typeof object === "undefined";
}

ProductCQ.getValidateError = function () {
    var errorMessage = '';
    for (var i = 0; i < ProductCQ.Questions.length; i++) {
        errorMessage += ProductCQ.Questions[i].getValidateError();
    }
    return errorMessage;
}

ProductCQ.buildDataForProduct = function (key, dataPackage) {
    var result = {};
    result.Questions = [];
    for (var i = 0; i < dataPackage.length; i++) {
        if (!isUndefined(dataPackage[i].ProductCode) &&
            !isUndefined(key) &&
            dataPackage[i].ProductCode.toUpperCase() == key.toUpperCase() &&
            !isUndefined(dataPackage[i].CustomQuestions)) {
            result.Questions = dataPackage[i].CustomQuestions.slice(0);
            break;
        }
    }
    var chkProductID = $('#chk' + key);
    if(isHiddenSelectedMinorProduct(chkProductID)) {
        return result; //return empty object if hidden product
    }
    result.getProductName = key;
    result.getValidateError = function () {
        if (this.Questions.length <= 0)
            return '';
        var errorMsg = '';
        if (isHiddenSelectedMinorProduct(chkProductID)) { //skip validate hidden product
            return errorMsg;
        }
        for (var i = 0; i < this.Questions.length; i++) {
            var que = this.Questions[i];
            var qIndex = i + 1;
            var questionID = "question_" + qIndex + "_chk" + key;
            // set value first
            switch (que.AnswerType) {
                case "HEADER":
                    continue;
                case "PASSWORD":
                case "TEXTBOX":
                case "DATE":
                    que.productAnswerText = $('#' + questionID).val();
                    break;
                case "DROPDOWN":
                    var ra_val = $('#' + questionID).val();
                    for (var j = 0; j < que.RestrictedAnswers.length; j++) {
                        var ra = que.RestrictedAnswers[j];
                        ra.isSelected = false;
                        if (ra_val == ra.Value) {
                            ra.isSelected = true;
                        }
                    }
                    break;
                case "CHECKBOX":
                    for (var j = 0; j < que.RestrictedAnswers.length; j++) {
                        var ra = que.RestrictedAnswers[j];
                        var ra_id = questionID + "_" + j;
                        ra.isSelected = $('#' + ra_id).is(':checked');
                    }
                    break;
            }

            if (que.IsRequired) {
                if (que.AnswerType != "CHECKBOX" && que.AnswerType != "DROPDOWN" && que.AnswerType != "HEADER") {
                    if (que.productAnswerText.length <= 0) {
                        errorMsg += 'Missing answer - ' + que.QuestionText + '<br/>';
                    } else {
                        if (que.productAnswerText.trim() == "") {
                            errorMsg += 'Missing answer - ' + que.QuestionText + '<br/>';
                        }
                    }
                }
                else if (que.AnswerType == "CHECKBOX") {
                    var ra_answered = false;
                    for (var j = 0; j < que.RestrictedAnswers.length; j++) {
                        var ra = que.RestrictedAnswers[j];
                        var ra_id = questionID + "_" + j;
                        if ($('#' + ra_id).is(':checked')) {
                            ra_answered = true;
                            break;
                        }
                    }

                    if (!ra_answered)
                        errorMsg += 'Missing answer - ' + que.QuestionText + '<br/>';
                }
                    // validate required dropdown list
                else if (que.AnswerType == "DROPDOWN") {
                    var ra_val = $('#' + questionID).val();
                    for (var j = 0; j < que.RestrictedAnswers.length; j++) {
                        var ra = que.RestrictedAnswers[j];
                        ra.isSelected = false;
                        if (ra_val == ra.Value && ra_val != '') {
                            ra.isSelected = true;
                            break;
                        }

                    }
                    if (!(ra.isSelected))
                        errorMsg += 'Missing answer -' + que.QuestionText + '<br/>';
                }
            }
            //validate text field using regular expression
            if (que.AnswerType != "CHECKBOX" && que.AnswerType != "DROPDOWN" && que.AnswerType != "HEADER") {
                if (que.productAnswerText.length > 0) {
                    if (que.productAnswerText.trim() != "") {
                        var reg_expr = que.reg_expression;
                        if (reg_expr != undefined && reg_expr != "") {
                            var reg = new RegExp(reg_expr);
                            var strAnswerText = que.productAnswerText;
                            var validAnswerText = strAnswerText.match(reg);
                            if (validAnswerText == null) {
                                errorMsg += "Invalid answer- " + que.QuestionText + "<br/>";
                            } else {
                                if (validAnswerText != strAnswerText.trim()) {
                                    errorMsg += "Invalid answer- " + que.QuestionText + "<br/>";
                                }
                            }
                        }
                    }
                }
            }   //end validate regular expression   
        }
        return errorMsg;
    }

    return result;
}

$(function () {
    //disable submit button after clicking submit to avoid click submit multiple times
    //$('input.btnSubmit').click(function () {
    //    if (isSubmitQuestion) {
    //        $('input.btnSubmit').attr('disabled', true);
    //        //the btnSubmit is disabled in 20 seconds, and it is enabled back after 20 seconds
    //        // setTimeout(function() { $('input.btnSubmit').attr('disabled', false); }, 60000);
    //    }

    //});
    //$('input.co_btnSubmit').click(function () {
    //    if (isCoSubmitQuestion) {
    //        $('input.co_btnSubmit').attr('disabled', true);
    //    }
    //});
    //if (!isSubmitQuestion) {
    //    $('input.btnSubmit').attr('disabled', false);
    //}
    //if (!isCoSubmitQuestion) {
    //    $('input.co_btnSubmit').attr('disabled', false);
    //}
    //disable Submit Answers button after clicking submit to avoid click submit multiple times
    // MODIFIED AND MOVED TO validateWalletQuestions
    //$('input.btnSubmitAnswer').click(function () {
    //    if (isSubmitAnswer) {
    //        $('input.btnSubmitAnswer').attr('disabled', true);
    //        //will be enabled if there is follow on questions
    //    }
    //});
        
    //if (!isSubmitAnswer) { //not success -> keep submit button enable
    //    $('input.btnSubmitAnswer').attr('disabled', false);
    //}
  
});
//function enabledSubmitBtn(hasCoApp) {
//    if (hasCoApp=='Y') {
//        isCoSubmitQuestion = false;
//        $('input.co_btnSubmit').attr('disabled', false);
//    } else {    
//        isSubmitQuestion = false;
//        $('input.btnSubmit').attr('disabled', false);
//    }
//}
//function hasClickedSubmitBtn(isCoApp) {
//    if (isCoApp) {
//        isCoSubmitQuestion = true;
//    } else {
//        isSubmitQuestion = true;
//    }
//}
/*
$(document).ready(function() {
     remove old format
    PCQ.Account1 = PCQ.buildDataForAccount($('#hdAccount1').val(), _ProductQuestionPackage);
    PCQ.Account2 = PCQ.buildDataForAccount($('#hdAccount2').val(), _ProductQuestionPackage);
    PCQ.Account3 = PCQ.buildDataForAccount($('#hdAccount3').val(), _ProductQuestionPackage);
   
  
    $('input[type="radio"][name="AccountRadio"]').change(function() {
        PCQ.reloadQuestionForAccountWhenChange();
        PCQ.showQuestions();  
    });
  
}); */

/////////////////////////////////
///   END PRODUCT QUESTION    ///
/////////////////////////////////

//***RoutingNumber Handler***//
//function bindLooupRoutingNumberEvent() {
//    // bind lookup routing number event
//    if ($('#bRoutingNumber')) {
//        $('#bRoutingNumber').keyup(function(e) {
//            var txt = $(this).val();
//            if (txt.length === 9) {
//                LookupRoutingNumber(txt, function(bankName, state) {
//                    $('#bBankName').val(bankName);
//                    $('#bBankState').val(state).selectmenu('refresh');
//                });
//            }
//        });
//    }
//}

//not use for accu but keep it here to avoid reference js error
    function LookupRoutingNumber(num, callback, failedCallback) {
        $.ajax({
            type: 'POST',
            url: '/Handler/Handler.aspx',
            data: { command: 'LookupRoutingNumber', routing_num: num,previewCode:$("#hdPreviewCode").val() },
            dataType: 'json',
            async: true,
            success: function (response) {
                if (response.IsSuccess) {
                    if ($.isFunction(callback)) {
                        callback(response.Info.BankName, response.Info.State);
                    }
                } else {
                    //$('#txtErrorMessage').html(response.Message);
                    //goToNextPage("#divErrorDialog");
                    goToDialog(response.Message);
                    if ($.isFunction(failedCallback)) {
                        failedCallback();
                    }
                    //alert(response.Message);
                }
            }
        });
    }
    //***Product Service Handler***//
    function toogleDisplayProductService(productCode, isShow) {
        // hide service section if there's no selected product
        var wrapper = $('#div_product_services_wrapper');
        if (wrapper) {
            if ($('#divSelectProduct input[type=checkbox]:checked').length === 0) {
                wrapper.hide();
            } else {
                var totalProd = 0;
                $('#divSelectProduct input[type=checkbox]:checked').each(function() {
                    totalProd += parseInt($(this).attr('total_service'));
                });
                totalProd === 0 ? wrapper.hide() : wrapper.show();
            }
        }
        // toogle display service
        var element = $('div[product-code="' + productCode + '"]');
        if (element) {        
            isShow ? element.show() : element.hide();
        }
    }

    /// Get all services belong to Product Code
    function getProductServices(productCode) {
        var serviceArray = [];
        // build the id prefix
        var idPrefix = 'chk_' + productCode + '_';
        $('#div_product_services_wrapper input[type=checkbox]:checked').each(function () {
            var id = $(this).attr('id');
            if (id.indexOf(idPrefix) === 0) {
                serviceArray.push(id.replace(idPrefix, ''));
            }
        });
        return serviceArray;
    }

    function getServiceNameAndAccountNameArray(productCode) {
        var serviceArray = [];
        // build the id prefix
        var idPrefix = 'chk_' + productCode + '_';
        $('#div_product_services_wrapper input[type=checkbox]:checked').each(function () {
            var id = $(this).attr('id');
            if (id.indexOf(idPrefix) === 0) {
                var obj = new Object();
                obj.AccountName = $(this).attr('account_name');
                obj.ServiceName = $(this).attr('service_name');
                serviceArray.push(obj);
            }
        });
        return serviceArray;
    }

    function fixCSSCorner() {
        // not sure why we broke CSS of checkbox group, the last item will miss border-bottom
        // lets fix it by adding two class for the checkbox label
        if ($('#page1').length > 0) {
            $('#page1').on('pageinit', function() {
                $('#divSelectProduct div.ui-block-a.ProductLabel:first div.ui-checkbox label[for]').removeClass('ui-corner-top');
                $('#divSelectProduct div.ui-block-a.ProductLabel:last div.ui-checkbox label[for]').removeClass('ui-corner-bottom').addClass('ui-controlgroup-last');
                $('#divSelectOtherProduct div.ui-block-a.ProductLabel:first div.ui-checkbox label[for]').removeClass('ui-corner-top');
                $('#divSelectOtherProduct div.ui-block-a.ProductLabel:last div.ui-checkbox label[for]').removeClass('ui-corner-bottom').addClass('ui-controlgroup-last');
            });
        }

        /*if ($('#pageOP')) {
            $('#pageOP').on('pageinit', function () {
                $('#divSelectProduct div.ui-block-a.ProductLabel:first div.ui-checkbox label[for]').removeClass('ui-corner-top');
                $('#divSelectProduct div.ui-block-a.ProductLabel:last div.ui-checkbox label[for]').removeClass('ui-corner-bottom').addClass('ui-controlgroup-last');
                $('#divSelectOtherProduct div.ui-block-a.ProductLabel:first div.ui-checkbox label[for]').removeClass('ui-corner-top');
                $('#divSelectOtherProduct div.ui-block-a.ProductLabel:last div.ui-checkbox label[for]').removeClass('ui-corner-bottom').addClass('ui-controlgroup-last');
            });
        }*/
    }
    fixCSSCorner();
//------special account type ---------
    function validateSpecialAccountType() {
        var strMessage = "";
        var mAccountTypeObj = getMinorAccountTypeObj();
        if (mAccountTypeObj.isMinor == "MINOR") {
            if (mAccountTypeObj.mAccountType == "") {
                strMessage = "Please select a youth or special account.<br/>";
            }
        } 
        return strMessage;
    }
    function setSpecialAccountType(appInfo) {
        var mAccountTypeObj = getMinorAccountTypeObj();
        var minorAccountList = MINORACCOUNTLIST;
        var minorRoleType = "";
        var secondRoleType = "";
        if ($('#hdHasMinorApplicant').val() == "Y") {
            for (var i = 0; i < minorAccountList.length; i++) {
                if (minorAccountList[i].minorAccountCode == mAccountTypeObj.mAccountType) {
                    minorRoleType = minorAccountList[i].minorRoleType;
                    secondRoleType = minorAccountList[i].secondRoleType;
                    break;
                }
            }
            appInfo.MinorRoleType = minorRoleType;
            appInfo.SecondRoleType = secondRoleType;
            appInfo.MinorAccountCode = mAccountTypeObj.mAccountType;
        }
    }
    function isSpecialAccount() {
        var availability = getParameterByName("type");
        if (availability == "1a" || availability == "2a") {
            return true;
        }
        return false;
    }
    function isSpecialAccountOptional() {
        var availability = getParameterByName("type");
        if (availability == "1b" || availability == "2b") {
            return true;
        }
        return false;
    }
    function hasSecRoleEmployment() {    
        var mAccountTypeObj = getMinorAccountTypeObj();
        var hasEmployment = "";
        for (var i = 0; i < MINORACCOUNTLIST.length; i++) {
            if (mAccountTypeObj.mAccountType == MINORACCOUNTLIST[i].minorAccountCode) {
                hasEmployment = MINORACCOUNTLIST[i].employment;
                break;
            }
        }
        return hasEmployment;
    }
    function getMinorAccountTypeObj() {
        var accountTypeElem = $('#ddlSpecialAccountType');
        var selectedAccountType = "";
        var strMinor = "";
        function minorObj(minorAccountType, isMinor) {
            this.mAccountType = minorAccountType;
            this.isMinor = isMinor;
        }
        if (accountTypeElem.find('option').length > 0) {
            selectedAccountType = accountTypeElem.find('option:selected').val();
        } else {
            if ($('#hdMinorAccountCode').length > 0 && accountTypeElem.hasClass('active')) {
                selectedAccountType = $('#hdMinorAccountCode').val();
            }
        }
        if (isSpecialAccount()) {
            strMinor = "MINOR";
        } else if (isSpecialAccountOptional()) {
            strMinor = "OPTIONAL";
        }

        return new minorObj(selectedAccountType, strMinor);
    }
    function isHiddenSelectedMinorProduct(chkProductID) {
        if (isSpecialAccount()) {
            if(chkProductID.prev().hasClass('ui-checkbox-on')){
                var divChkElement = $(chkProductID).parent().parent().parent().attr('style');
                if (!$(chkProductID).parent().hasClass('ui-checkbox')) {
                    divChkElement = $(chkProductID).parent().parent().attr('style');
                }
                if (divChkElement !=undefined && divChkElement.indexOf('block')==-1) {
                   return true;
               }
            }
        }
        return false;
    }

//-------end special account type ------
//   function hideSecondaryFields(coPrefix) {
//        if (coPrefix == undefined) {
//            coPrefix = "";
//        }       
//        if (isHiddenSecMotherMaiden()) {
//            $('#'+coPrefix+'txtMotherMaidenName').parent().hide(); //hide mother maiden name field 
//        }
//        if (isHiddenSecCitizenship()) {
//            $('#'+coPrefix+'ddlCitizenshipStatus').parent().hide(); //hide citizenship field
//        }
//        if (isHiddenSecEmployment()) {
//            $('#' + coPrefix + 'divEmploymentStatus').hide(); //hide employment fields
//            $('.ViewEmploymentInfo').hide();
//            $('.ViewEmploymentInfo').prev().hide();
//            $('.co_ViewEmploymentInfo').hide();
//            $('.co_ViewEmploymentInfo').prev().hide();

//        }
//        if (isHiddenSecOccupancy()) {
//            $('#' + coPrefix + 'divOccupancyStatus').hide(); //hide occupancy status fields
//        }
//        if (isHiddenSecIdPage()) {
//            $('#' + coPrefix + 'divApplicantID').hide(); //hide Identification fields 
//            $('.ViewIdentification').hide();
//            $('.ViewIdentification').prev().hide();
//            $(".co_ViewIdentification").hide();
//            $(".co_ViewIdentification").prev().hide();     
//        }
//}
//    function isHiddenSecMotherMaiden() {
//        if( getParameterByName("type")=='2' && $('#hdSecondaryMotherMaidenName').val() =='N'){
//            return true;
//        }
//        return false;
//    }
//    function isHiddenSecCitizenship() {
//        if (getParameterByName("type") == '2' && $('#hdSecondaryCitizenship').val() == 'N') {
//            return true;
//        }
//        return false;
//    }
//    function isHiddenSecEmployment() {
//        if (getParameterByName("type") == '2' && $('#hdSecondaryEmployment').val() == 'N') {
//            return true;
//        }
//        return false;
//    }
//    function isHiddenSecOccupancy() {
//        if (getParameterByName("type") == '2' && $('#hdSecondaryOccupancy').val() == 'N') {
//            return true;
//        }
//        return false;
//    }
//    function isHiddenSecIdPage() {
//        if (getParameterByName("type") == '2' && $('#hdSecondaryIdPage').val() == 'N') {
//            return true;
//        }
//        return false;
//    }
//    function isHiddenSecGrossIncome() {
//        if (getParameterByName("type") == '2' && $('#hdSecondaryGrossIncome').val() == 'N') {
//            return true;
//        }
//        return false;
//    }
    $(function () {
      //  handledSpecialAccountType();
        //handle disable disclosures 
        $("div[name='disclosures'] label").click(function () {
            handleDisableDisclosures(this);
        });
        //hideSecondaryFields(); //primary
        //hideSecondaryFields("co_"); //co app
        
        //show special account button if it exist
        if (isSpecialAccount() || isSpecialAccountOptional()) {
            $('#divAccountTypeCol').addClass('col-sm-6');
            $('#divBtnJointCol').addClass('col-sm-6');
            $('#divAccountTypeCol').show();
        }
       
        // handle selected account type dropdown: hover, selected/unselected
        $('#ddlAccountType-button').on('mouseenter', function () {
            var element = $(this);
            element.removeClass('btnActive_off');
            element.addClass('btnHover');
        });
        
        $('#ddlAccountType-button').on('mouseleave', function () {
            var element = $(this);
            element.removeClass('btnHover');
        });
      
        $('select#ddlAccountType').on('change', function () {
            var element = $(this);
            if (element.val() == '') {
                element.parent().addClass('btnActive_off');
                element.parent().removeClass('btnActive_on');
            } else {
                element.parent().addClass('btnActive_on');
                element.parent().removeClass('btnActive_off');
            }  
        });
        //end handle selected account type dropdown: hover, selected/unselected
        initPage1();
        $(document).on("pageshow", function () {
            var curPageId = $.mobile.pageContainer.pagecontainer('getActivePage').attr('id');
            if (curPageId === "accuWelcome") {
                isSubmittingXA = false;

                if ($('#walletQuestions .btnSubmitAnswer').hasClass('ui-disabled')) {
                    //enable submitAnswer 
                    $('#walletQuestions .btnSubmitAnswer').removeClass('ui-disabled');
                }           
            }

            if (curPageId === "GettingStarted") {
                initPage1();
            }
            
            if (curPageId === "reviewpage") {
                isSubmittingXA = false;
            }
            $("#divErrorPopup").on("popupafterclose", function () {
            	isSubmittingXA = false;
            	isSubmitWalletAnswer = false;
            });
        });
    });
function initPage1() {
    //handle selected Joint Applicant button
    $('#btnHasCoApplicant').off("click").on('click', function () {
        var $self = $(this);
        if ($("#hdHasCoApplicant").val() === "N") {
            $("#hdHasCoApplicant").val("Y");
        } else {
            $("#hdHasCoApplicant").val("N");
        }
        $self.toggleClass("active");
        handledBtnHeaderTheme($(this));
    });
}
function goBack(ele) {
	var $ele = $(ele);
	var pageId = $ele.closest("div[data-role='page']").attr("id");
	switch (pageId) {
		case "pageMinorInfo":
			goToNextPage("#GettingStarted");
			break;
		case "page2":
			if ($('#hdHasMinorApplicant').val() == 'Y') {
				goToNextPage("#pageMinorInfo");
			} else {
				goToNextPage("#GettingStarted");
			}
			break;
		case "page6":
			goToNextPage("#page2");
			break;
		case "pageFS":
			if ($('#hdHasCoApplicant').val() == 'Y') {
				goToNextPage("#page6");
			} else {
				goToNextPage("#page2");
			}
			break;
	    case "reviewpage":
	        if ($("#divFundingTypeList a.btn-header-theme").length > 0) {
	            goToNextPage("#pageFS");
	        } else {
	            if ($('#hdHasCoApplicant').val() == 'Y') {
	                goToNextPage("#page6");
	            } else {
	                goToNextPage("#page2");
	            }
	        }
			break;
		default:
	}
}