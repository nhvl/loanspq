﻿
Imports LPQMobile.Utils

Partial Class PageLoading
    Inherits System.Web.UI.Page

    Protected Url As String
    Protected ServerRoot As String

    Private Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Url = Common.SafeString(Request.Params("target"))

        Dim port As String
        If (Request.Url.Port = 80) Then
            port = ""
        Else
            port = ":" + Request.Url.Port.ToString()
        End If

        ServerRoot = Request.Url.Scheme + "://" + Request.Url.Host + port
    End Sub
End Class
