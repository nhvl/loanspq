﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="ViewerTester.aspx.vb" Inherits="ViewerTest" %>
<%@ Register TagPrefix="uc" TagName="pageFooter" Src="~/Inc/PageFooter.ascx" %>
<%@ Register Src="~/Inc/PageHeader.ascx" TagPrefix="uc" TagName="pageHeader" %>
<%@ Import Namespace="LPQMobile.Utils" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
<meta charset="UTF-8"/>	
  <title>Viewer Tester</title>
	<meta name="viewport" content="width=device-width, initial-scale=1" />
	 <uc:pageHeader ID="ucPageHeader" runat="server" />
	<%--<link rel="stylesheet" href="/css/themes/default/jquery.mobile-1.1.0.css" />
    <script src="/js/jquery.js" type="text/javascript"></script>
    <script src="/js/jquery.mobile-1.1.0.min.js" type="text/javascript"></script>--%>
	 <%--<style>
        
        div.ui-body-c{
            background-color :#5E87B0;
            color: #00457c;
        }
       
		.lpq_container {
		background-color: #ffffff;
		box-sizing: border-box;
		margin: 2% auto;   /*outside_top, left&right*/
		max-width: 960px;
		padding: 15px 10px 5%;
		position: relative;
		width: 100%;	
		}
	</style>--%>
	<script type="text/javascript">
		var currentPage = "page1";
		function callOpenFramedViewer() {
			var hrefUrl = 'https://iqcollection.loanspq.com/pdfviewer/pdf/7e26bd37d43_AccountAgreement.pdf.pdf';
			//var hrefUrl = 'https://iqcollection.loanspq.com:443/pdfviewer/pdf/test_live_6xlOSWF-Vi2bz5q_0g.pdf'; //fail due to pdf format or text, first common wealth
			//var hrefURL = 'https://cs.loanspq.com/Information/DocViewer.aspx?enc2=YwDHIC8iE78fiH8g__kqAhXBJLwlLigyTc9mLzmWwD4s8w0YnlWi3ijgM_FO-625ZGj6i_EvU7-bv0mptrQqssB_03DdYZyWRgyJ81DNyv4NCKRSndjHP3pSiaNV5-jIk04Hh_vA_bZcAK3fql1oxrSPWWF46yTD0soCdW_1QPZRb1gdaJjlsu68V8vcvKUHy34Xk7D2nC2g8mAkO7IC7f8CQ5rv0zg-3M8um71rUZz-ovuE0je0_PRqZthclKo2RDC289IRjMPf5im7acI0UQ';

			//var fileUrl = 'https://mozilla.github.io/pdf.js/web/viewer.html?file=' + hrefUrl; //15.5sec(728KB), CTRL +F5 13.2(681KB), 2.87s to 1.9sec (10KB)
			//var fileUrl = 'http://lpqmobile.localhost/pdfviewer/web/viewer.aspx?file=' + hrefUrl;  //CTRL+f5 14s(696KB),  1.85s(19KB)  clear cache more often, no cache control=756KB

			//after enable http compression 12sec(586KB), 2.8sec(26KB)
			var fileUrl = 'https://apptest.loanspq.com/pdfviewer/web/viewer.aspx?file=' + hrefUrl; //28.4sec(2.1MB),,24sec, 11sec(570KB) to 2.8sec(26KB)  why 2.1MB

			//after enable http compression 11.4sec(578KB),
			//var fileUrl = 'https://app.loanspq.com/pdfviewer/web/viewer.aspx?file=' + hrefUrl; //36sec all the time due to no cache
			openFramedViewer('', fileUrl, '', true)
		}
		function openFramedViewer(obj, fileUrl, lenderref, isPdfLink) {
			var height = window.innerHeight ||
						 document.documentElement.clientHeight ||
						 document.body.clientHeight;

			height = height - 90; // height of one close button


			var sandboxOption = isPdfLink ? "" : "sandbox=\"allow-forms allow-pointer-lock allow-same-origin\"";


			$('#div_iframe').html("<iframe onload='iframeOnloaded()' src='" + fileUrl + "' scrolling='auto' frameborder='0' width='100%' height='" + height + "px' " + sandboxOption + "></iframe>");


			$.mobile.changePage($("#popupPDFViewer"), {});
			$.mobile.loading("a", "Loading ... please wait", true);
		} //end openFramedViewer

		function iframeOnloaded() {
			$.mobile.loading('hide');
		}
		

		function gotoPreviousPage() {
			$('#div_iframe').html("");
			$.mobile.changePage($("#" + currentPage), {});
		}


		$(function () {
			//test on good 2G(450kb/s)
			//var hrefUrl = 'https://iqcollection.loanspq.com/pdfviewer/pdf/7e26bd37d43_AccountAgreement.pdf.pdf';
			//var fileUrl = 'http://mozilla.github.io/pdf.js/web/viewer.html?file=' + hrefUrl; //15.5sec(728KB), CTRL +F5 13.2(681KB), 2.87s to 1.9sec (10KB)
			//var fileUrl = 'http://lpqmobile.localhost/pdfviewer/web/viewer.aspx?file=' + hrefUrl;  //CTRL+f5 14s(696KB),  1.85s(19KB)  clear cache more often, no cache control=756KB
								
			//after enable http compression 12sec(586KB), 2.8sec(26KB)
			//var fileUrl = 'https://apptest.loanspq.com/pdfviewer/web/viewer.aspx?file=' + hrefUrl; //28.4sec(2.1MB),,24sec, 11sec(570KB) to 2.8sec(26KB)  why 2.1MB

			//after enable http compression 11.4sec(578KB),
		   //var fileUrl = 'https://app.loanspq.com/pdfviewer/web/viewer.aspx?file=' + hrefUrl; //36sec all the time due to no cache
			//openFramedViewer('', fileUrl, '', true)
		});
	</script>
</head>
<body class = "lpq_container">
	<form id="form1" runat="server">
     <div id="page1" data-role="page">
		 <div data-role="header" data-position="">
            <h1>
                Credit Card Information</h1>
        </div>

		 <div data-role="content" style="padding: 0 !important">
			 <div style="text-align: center;">
				<select id="Select1">
                    <option value="1">1</option>
					 <option value="2">2</option>
					 <option value="3">3</option>
                </select> 
				    
			</div>
			<div style="text-align: center;">
				<a href="#" data-transition="slide" onclick="callOpenFramedViewer();" data-theme="s" type="button" 
					class="div-continue-button ui-btn ui-btn-up-s ui-shadow ui-btn-corner-all" data-corners="true" data-shadow="true" data-iconshadow="true" data-wrapperels="span">
					<span class="ui-btn-inner ui-btn-corner-all"><span class="ui-btn-text">Continue</span></span></a>
			</div>
			 <div class="Title">
                Loan Amount   <span style="color:red">*</span></div>
            <div>
                <input type="<%=_textAndroidTel%>" pattern="[0-9]*" id="txtLoanAmount" class="money" maxlength ="12"   />
            </div>
            <div id="divLoanTerm">
                <div class="Title">
                    Loan Term (months)<span style="color:red">*</span> </div>
                <div>
                    <input type="<%=_textAndroidTel%>" pattern="[0-9]*" id="txtLoanTerm" class="inmonth" maxlength="3" oninput ="CheckMaxLength(this);" />
                </div>
            </div>
		 </div> <%--//content--%>
		 <div class ="div-continue"  data-role="footer">
                <a href="#"  data-transition="slide" onclick="validateScreen1(this);" type="button" class="div-continue-button">Continue</a> 
                <a href="#divErrorDialog" style="display: none;"></a>
                <a href='#pl2' style="display: none;"></a>
         </div>
	</div>	<%--end first page--%>


	<div id="popupPDFViewer" data-role="page">
        <div data-role="content" style="padding: 0 !important">
			<%--onclick="gotoPreviousPage()"--%>
			<div style="text-align: center;"><a data-role="button" data-inline="true" data-icon="arrow-l" data-theme="b" onclick="gotoPreviousPage()">Close</a></div>
            <div style="text-align: center; margin: 10px 0;" id="div_iframe" ></div>
			<div style="text-align: center;">
				<select id="ddlCreditCardType">
                    <option value="1">1</option>
					 <option value="2">2</option>
					 <option value="3">3</option>
                </select>    
			</div>
           
        </div>
    </div> <%--end last page--%>

		        <!--footer logo -->   
  <div class = "divfooter"> 
           <img  id="hdLogoImage" src="<%=_strLogoUrl%>" alt ="" style="display :none;"/>
            <div class="divfooterleft"><div class="divfooterleft-content"><%=_strFooterLeft  %></div></div>          
            <div class="divfooterright">
                <% If String.IsNullOrEmpty(_hasFooterRight) Then%>
                    <div class="divfooterright-content">
                        <div class="divfooterright-content-style"><a href="<%=_strNCUAUrl%>"  class="divfooterright-content-color ui-link" ><%=_strFooterRight%></a></div></div>
                    <div class="divfooterright-logo">
                         <div class="divfooterright-logo-style"><a href="<%=_strHUDUrl%>" class="ui-link"> <img src="<%=_strLogoUrl%>" alt ="" id="logoImage" class="footerimage-style" /></a></div></div> 
                    <div style ="clear:both"></div> 
                <%Else%>
                    <%=_strFooterRight%>
                    <div style ="clear:both"></div> 
                <%End If%>
            </div>
          <div style ="clear:both"></div>            
  </div>   <%--end footer --%> 
    </form>
	<uc:pageFooter ID="ucPageFooter" runat="server" />
</body>
</html>
