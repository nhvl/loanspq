﻿
Partial Class Admin_XmlEditor_editor
    Inherits System.Web.UI.Page
	Protected _loadBogusFile As Boolean = True
	Protected _allowSaveChanges As Boolean = True
	Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
		If Request.QueryString("e") = "1" Then
			_loadBogusFile = False
		End If
		If Request.QueryString("c") = "false" Then
			_allowSaveChanges = False
		End If
	End Sub
End Class
