﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="editor.aspx.vb" Inherits="Admin_XmlEditor_editor" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>AP Legacy Editor</title>
	<link rel="stylesheet" href="css/reset.css" type="text/css" />
	<link rel="stylesheet" href="lib/colorpicker/css/colorpicker.css" type="text/css" />
	<link rel="stylesheet" href="css/themes/smoothness/jquery-ui.min.css" type="text/css" />
	<link rel="stylesheet" href="css/demo.css" type="text/css" />
	<link rel="stylesheet" href="css/jquery.xmleditor.css" type="text/css" />
	<script src="lib/ace/src-min/ace.js"></script>
	<script src="lib/jquery.min.js"></script>
	<script src="lib/jquery-ui.min.js"></script>
	<script src="lib/json2.js"></script>
	<script src="lib/cycle.js"></script>
	<script src="lib/jquery.autosize-min.js"></script>
	<script src="jquery.xmleditor.js"></script>
	<script src="xsd/xsd2json.js"></script>
	<script src="../../sm/js/ckeditor/ckeditor.js"></script>
	<script src="lib/colorpicker/js/colorpicker.js"></script>
	<style type="text/css">
			.noTitleBar .ui-dialog-titlebar {
				display: none;
			}
	</style>
</head>
<body>
    <div class="wrapper">
		<header><h1>AP Legacy Editor</h1></header>
	</div>
	<textarea id="xml_editor"></textarea>
	<div id="dlgStringEditor">
		<textarea id="ckEditor1" name="ckEditor1"></textarea>
	</div>
	<script>
		$(function () {

			//to fix bug: does not allow edit text in hyperlink dialog (jquery dialog conflict with ckedtior
			$.widget("ui.dialog", $.ui.dialog, {
				/*! jQuery UI - v1.10.2 - 2013-12-12
				 *  http://bugs.jqueryui.com/ticket/9087#comment:27 - bugfix
				 *  http://bugs.jqueryui.com/ticket/4727#comment:23 - bugfix
				 *  allowInteraction fix to accommodate windowed editors
				 */
				_allowInteraction: function (event) {
					if (this._super(event)) {
						return true;
					}

					// address interaction issues with general iframes with the dialog
					if (event.target.ownerDocument != this.document[0]) {
						return true;
					}

					// address interaction issues with dialog window
					if ($(event.target).closest(".cke_dialog").length) {
						return true;
					}

					// address interaction issues with iframe based drop downs in IE
					if ($(event.target).closest(".cke").length) {
						return true;
					}
				},
				/*! jQuery UI - v1.10.2 - 2013-10-28
				 *  http://dev.ckeditor.com/ticket/10269 - bugfix
				 *  moveToTop fix to accommodate windowed editors
				 */
				_moveToTop: function (event, silent) {
					if (!event || !this.options.modal) {
						this._super(event, silent);
					}
				}
			});


			$("#dlgStringEditor").dialog({
				autoOpen: false, modal: true, dialogClass: "noTitleBar", width: 500, buttons: {
					Close: function () { $(this).dialog("close"); },
					Save: function () {
						var content = CKEDITOR.instances["ckEditor1"].getData();
						$(this).data("store-data-element").val(content);
						$(this).data("store-data-element").trigger("change");
						$("#xml_editor").xmlEditor("updateDocumentState");
						$(this).dialog("close");
					}
				}
			});

			CKEDITOR.replace('ckEditor1', { autoParagraph: false, format_tags: 'p;h1;h2;pre;div' });

			CKEDITOR.config.autoParagraph = false;
			CKEDITOR.config.extraAllowedContent = '*(*);*{*}';
			var extractor = new Xsd2Json("config.xsd", { "schemaURI": "../../migrateConfig/" });
			//var extractor = new Xsd2Json("mods-3-4.xsd", { "schemaURI": "examples/mods-3-4/" });
			//var extractor = new Xsd2Json("dcterms.xsd", { "schemaURI": "examples/dcterms/" });
			<%If _loadBogusFile = true %>
			$("#xml_editor").xmlEditor({
				documentTitle: "Loaded sample bogus configure file",
				ajaxOptions: {
					xmlRetrievalPath: "config_temp.xml"
				},
				schema: extractor.getSchema(),
				editorDialog: $("#dlgStringEditor"),
				editorName: "ckEditor1",
				//TODO: move to external folder, outside web folder.
				accessConfigPath: "/migrateConfig/access_configuration.json?" + new Date().getTime()
				//schema : "examples/mods.json",
				//libPath : '../lib/'
			});
			<%else%>
			$("#xml_editor").text(sessionStorage.getItem("configData"));
			if ($.trim(sessionStorage.getItem("configData")) === "") {
				$("#xml_editor").text("<WEBSITE></WEBSITE>");
			}
			var btnConfig = null;
			<%If _allowSaveChanges Then%>
			btnConfig = [
				{
					label: "Save changes",
					onSubmit: function() {
						var strXml = $("#xml_editor").xmlEditor("toXMLString");

						if (strXml !== null) {
							$(window.opener.document.getElementById('txtConfigData')).val(strXml);
							$("#xml_editor").data("xmlXmlEditor").xmlState.changeState = 1;
							window.close();
						}
					}
				}
			];
			<%End If%>
			$("#xml_editor").xmlEditor({
				documentTitle: "Config Data",
				//ajaxOptions: {
				//	xmlRetrievalPath: "config_temp.xml"
				//},
				schema: extractor.getSchema(),
				editorDialog: $("#dlgStringEditor"),
				editorName: "ckEditor1",
				//TODO: move to external folder, outside web folder.
				accessConfigPath: "/migrateConfig/access_configuration.json?" + new Date().getTime(),
				submitButtonConfigs: btnConfig
				//schema : "examples/mods.json",
				//libPath : '../lib/'
			});
			<%End If%>
			window.onerror=function(msg, url, lineNo, columnNo, error){
				var string = msg.toLowerCase();
				var substring = "script error";
				if (string.indexOf(substring) > -1){
				    alert('Script Error: See Browser Console for Detail');
				} else {
				    var message = [
				      'Message: ' + msg,
				      'URL: ' + url,
				      'Line: ' + lineNo,
				      'Column: ' + columnNo,
				      'Error object: ' + JSON.stringify(error)
				    ].join(' - ');

				    alert(message);
				}

				return false;
			};
		});
	</script>
	<footer><p></p></footer>
</body>
</html>
