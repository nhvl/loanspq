﻿
Imports Newtonsoft.Json
Imports LPQMobile.Utils
Imports System.IO

Partial Class Sm_SmAuthHandler
	Inherits System.Web.UI.Page

	Protected _log As log4net.ILog = log4net.LogManager.GetLogger(Me.GetType)
	Protected _serverRoot As String

	Public ReadOnly Property ServerRoot() As String
		Get
			Dim port As String = ""
			If (Request.Url.Scheme = "http" AndAlso Request.Url.Port <> 80) OrElse (Request.Url.Scheme = "https" AndAlso Request.Url.Port <> 443) Then
				port = ":" + Request.Url.Port.ToString()
			End If
			Return Request.Url.Scheme + "://" + Request.Url.Host + port
		End Get
	End Property

	Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
		_serverRoot = ServerRoot
		Dim cmd As String = Common.SafeString(Request.Form("command"))
		Select Case cmd
			Case "activateAccount"
				ActivateAccount()
				'Case "accountRequest"
				'	AccountRequest()
			Case "forgotPassword"
				ForgotPassword()
			Case "resetPassword"
				ResetPassword()
			Case "uploadAvatar"
				UploadAvatar()
			Case "deleteAvatar"
				DeleteAvatar()
		End Select
	End Sub

	Private Sub ActivateAccount()
		Dim activateAccountObj = JsonConvert.DeserializeObject(Of SmActivateAccountModel)(Common.SafeString(Request.Form("user_data")), New JsonSerializerSettings() With {.NullValueHandling = NullValueHandling.Ignore}).MakeItSafe()
		Dim res As CJsonResponse
		If String.IsNullOrEmpty(activateAccountObj.Token) OrElse String.IsNullOrEmpty(activateAccountObj.Password) OrElse activateAccountObj.Password <> activateAccountObj.ConfirmPassword Then
			res = New CJsonResponse(False, "Bad data", "FAILED")
		Else
			Try
				Dim smAuthBL As New SmAuthBL()
				Dim tokenObj As SmUserActivateToken = smAuthBL.GetActivationTokenInfo(activateAccountObj.Token)
				Dim expireExceed As Integer = Common.SafeInteger(ConfigurationManager.AppSettings("UserActivationTokenExpireAfter"))
				If tokenObj Is Nothing OrElse tokenObj.Status <> SmSettings.TokenStatus.Waiting OrElse tokenObj.CreatedDate.AddDays(expireExceed) < Now Then
					res = New CJsonResponse(False, "Invalid token", "FAILED")
				Else
					Dim usr = smAuthBL.GetUserByID(tokenObj.UserID)
					If usr IsNot Nothing Then
						Dim checkValidPasswordResult = smAuthBL.CheckValidPassword(usr, activateAccountObj.Password)
						If checkValidPasswordResult = "" Then
							Dim ipAddress As String = Request.ServerVariables("REMOTE_ADDR")
							Dim result = smAuthBL.ActivateUser(tokenObj, activateAccountObj, ipAddress)
							If result = "ok" Then
								If HttpContext.Current.User.Identity.IsAuthenticated Then
									''clear current session
									FormsAuthentication.SignOut()
									Session.Clear()
									Session.Abandon()
								End If
								res = New CJsonResponse(True, "", "OK")
							Else
								res = New CJsonResponse(False, "", "FAILED")
							End If
						Else
							res = New CJsonResponse(False, checkValidPasswordResult, "INVALID")
						End If
					Else
						res = New CJsonResponse(False, "", "FAILED")
					End If

				End If
			Catch ex As Exception
				_log.Error("Could not ActivateAccount.", ex)
				res = New CJsonResponse(False, "Could not activate account. Please try again", "FAILED")
			End Try
		End If
		Response.Write(JsonConvert.SerializeObject(res))
		Response.End()
	End Sub

	Private Sub ForgotPassword()
		Dim userName As String = Common.SafeStripHtmlString(Request.Form("userName"))
		Dim res As CJsonResponse
		If String.IsNullOrEmpty(userName) Then
			res = New CJsonResponse(False, "Bad data", "FAILED")
		Else
			Try
				Dim smAuthBL As New SmAuthBL()
				Dim result As String = smAuthBL.ForgotPassword(HttpContext.Current, userName, Request.ServerVariables("REMOTE_ADDR"), _serverRoot)
				If result = "ok" Then
					res = New CJsonResponse(True, "Your request has been processed and an email has been sent to you.  The email has instructions that may be time sensitive.", "OK")
				ElseIf result = "notFound" Then
					'res = New CJsonResponse(True, "Account corresponding with this email is not found. Please check and try again", "FAILED")
					'use same message to mitigate E-Mail Account Enumeration
					_log.Debug("Account corresponding with this email is not found. Please check and try again")
					res = New CJsonResponse(True, "Your request has been processed and an email has been sent to you.  The email has instructions that may be time sensitive.", "OK")
				Else
					res = New CJsonResponse(True, "Error!", "FAILED")
				End If

			Catch ex As Exception
				_log.Error("Could not process forgot password request.", ex)
				res = New CJsonResponse(False, "Could not execute request password request. Please try again", "FAILED")
			End Try
		End If
		Response.Write(JsonConvert.SerializeObject(res))
		Response.End()
	End Sub

	''deprecated, no longer nee
	'Private Sub AccountRequest()
	'	Dim userName As String = Common.SafeStripHtmlString(Request.Form("userName"))
	'	Dim res As CJsonResponse
	'	If String.IsNullOrEmpty(userName) Then
	'		res = New CJsonResponse(False, "Bad data", "FAILED")
	'	Else
	'		Try
	'			Dim smAuthBL As New SmAuthBL()
	'			Dim result As String = smAuthBL.AccountRequest(HttpContext.Current, userName, Request.ServerVariables("REMOTE_ADDR"), _serverRoot)
	'			If result = "ok" Then
	'				res = New CJsonResponse(True, "Thank you, your request has been received.  You will receive an email from the system for account activation soon.", "OK")
	'			Else
	'				res = New CJsonResponse(True, "Error!", "FAILED")
	'			End If

	'		Catch ex As Exception
	'			_log.Error("Could not process AccountRequest.", ex)
	'			res = New CJsonResponse(False, "Could not execute account request action. Please try again", "FAILED")
	'		End Try
	'	End If
	'	Response.Write(JsonConvert.SerializeObject(res))
	'	Response.End()
	'End Sub

	Private Sub ResetPassword()
		Dim token As String = Common.SafeStripHtmlString(Request.Form("token"))
		Dim password As String = Common.SafeStripHtmlString(Request.Form("password"))
		Dim confirmPassword As String = Common.SafeStripHtmlString(Request.Form("confirmPassword"))
		Dim res As CJsonResponse
		If String.IsNullOrEmpty(token) OrElse String.IsNullOrEmpty(password) OrElse password <> confirmPassword Then
			res = New CJsonResponse(False, "Bad data", "FAILED")
		Else
			Try
				Dim smAuthBL As New SmAuthBL()
				Dim tokenObj As SmUserActivateToken = smAuthBL.GetActivationTokenInfo(token)
				Dim expireExceed As Integer = Common.SafeInteger(ConfigurationManager.AppSettings("UserActivationTokenExpireAfter"))
				If tokenObj Is Nothing OrElse tokenObj.Status <> SmSettings.TokenStatus.Waiting OrElse tokenObj.CreatedDate.AddDays(expireExceed) < Now Then
					res = New CJsonResponse(False, "Invalid token", "FAILED")
				Else
					Dim usr = smAuthBL.GetUserByID(tokenObj.UserID)
					If usr IsNot Nothing Then
						Dim checkValidPasswordResult = smAuthBL.CheckValidPassword(usr, password)
						If checkValidPasswordResult = "" Then
							Dim ipAddress As String = Request.ServerVariables("REMOTE_ADDR")
							Dim result = smAuthBL.ResetPassword(tokenObj, password, ipAddress)
							If result = "ok" Then
								If HttpContext.Current.User.Identity.IsAuthenticated Then
									''clear current session
									FormsAuthentication.SignOut()
									Session.Clear()
									Session.Abandon()
								End If
								res = New CJsonResponse(True, "", "OK")
							Else
								res = New CJsonResponse(False, "", "FAILED")
							End If
						Else
							res = New CJsonResponse(False, checkValidPasswordResult, "INVALID")
						End If
					Else
						res = New CJsonResponse(False, "", "FAILED")
					End If
				End If
			Catch ex As Exception
				_log.Error("Could not ResetPassword.", ex)
				res = New CJsonResponse(False, "Could not reset password. Please try again", "FAILED")
			End Try
		End If
		Response.Write(JsonConvert.SerializeObject(res))
		Response.End()
	End Sub
	Private Sub UploadAvatar()
		Dim fileUrl = ""
		Try
			If Request.Files.Count > 0 Then
				Dim file As HttpPostedFile = Request.Files(0)
				If file IsNot Nothing AndAlso file.ContentLength > 0 AndAlso (file.ContentType = "image/png" Or file.ContentType = "image/jpeg") Then
					Dim fileExt As String = New FileInfo(file.FileName).Extension
					Dim fileName As String = Guid.NewGuid().ToString().Replace("-", "").Substring(0, 10) & fileExt
					If Not String.IsNullOrWhiteSpace(ConfigurationManager.AppSettings.Get("AZURE_MEDIA_ACCOUNT_NAME")) Then
						Using reader = New BinaryReader(file.InputStream)
							Dim fileContent As Byte() = reader.ReadBytes(file.ContentLength)
							Dim azureSrv As New CAzureStorageService(ConfigurationManager.AppSettings.Get("AZURE_MEDIA_ACCOUNT_NAME"), ConfigurationManager.AppSettings.Get("AZURE_MEDIA_ACCOUNT_KEY"))
							Dim avatarContainerName As String = ConfigurationManager.AppSettings.Get("AZURE_AVATAR_CONTAINER")
							If String.IsNullOrWhiteSpace(avatarContainerName) Then avatarContainerName = "avatars"
							fileUrl = azureSrv.UploadBlob(avatarContainerName, fileName, fileContent, file.ContentType)
						End Using
					Else
						Dim avatarFolderPath As String = IO.Path.Combine(Server.MapPath("~/images"), "avatars")
						If Not Directory.Exists(avatarFolderPath) Then
							Directory.CreateDirectory(avatarFolderPath)
						End If
						file.SaveAs(Path.Combine(avatarFolderPath, fileName))
						fileUrl = "/images/avatars/" & fileName
					End If
				End If
			End If
		Catch ex As Exception
			_log.Error("Error while saving avatar", ex)
		End Try
		Response.Write(fileUrl)
		Response.End()
	End Sub
	Private Sub DeleteAvatar()
		Dim result As String = ""
		Try
			'https://lpqhieplam.blob.core.windows.net/avatars/8915a919bc.png
			If Not String.IsNullOrWhiteSpace(ConfigurationManager.AppSettings.Get("AZURE_MEDIA_ACCOUNT_NAME")) Then
				Dim avatarContainerName As String = ConfigurationManager.AppSettings.Get("AZURE_AVATAR_CONTAINER")
				If String.IsNullOrWhiteSpace(avatarContainerName) Then avatarContainerName = "avatars"
				Dim m As Match = Regex.Match(Common.SafeStripHtmlString(Request.Form("fileurl")), String.Format("{0}/(?<filename>\w+.(?:png|jpg|jpeg))$", avatarContainerName))
				If m.Success Then
					Dim azureSrv As New CAzureStorageService(ConfigurationManager.AppSettings.Get("AZURE_MEDIA_ACCOUNT_NAME"), ConfigurationManager.AppSettings.Get("AZURE_MEDIA_ACCOUNT_KEY"))
					azureSrv.DeleteBlob(avatarContainerName, m.Groups("filename").Value)
				End If
			Else
				Dim m As Match = Regex.Match(Common.SafeStripHtmlString(Request.Form("fileurl")), "images/avatars/(?<filename>\w+.(?:png|jpg|jpeg))$")
				If m.Success Then
					Dim avatarFilePath As String = IO.Path.Combine(Server.MapPath("~/images"), "avatars", m.Groups("filename").Value)
					If IO.File.Exists(avatarFilePath) Then
						IO.File.Delete(avatarFilePath)
					End If
				End If
			End If
			result = "done"
		Catch ex As Exception
			_log.Error("Error while deleting avatar", ex)
			result = "error"
		End Try
		Response.Write(result)
		Response.End()
	End Sub
End Class
