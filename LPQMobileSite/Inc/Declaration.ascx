﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="Declaration.ascx.vb"
    Inherits="Inc_Declaration" %>
<%@ Import Namespace="LPQMobile.Utils" %>
<div id="<%=IDPrefix%>divDeclarationSection" class="declaration-wrapper">
	<div style="height: 20px;">
		<span id="<%=IDPrefix%>spValidateDeclarationMessage"></span>
	</div>
	<%If DeclarationList IsNot Nothing AndAlso DeclarationList.Count > 0 Then%>
	<%	 
		Dim index As Integer = 0
		For Each item In DeclarationList
			index += 1
			Dim additionalQuestions = GetAdditionalQuestions(item.Key)
		 %>
	<div class="Title">
		<div class="col-sm-7 col-md-8 col-xs-12 no-padding">
		   <span class="declaration-font RequiredIcon rename-able"> <%=String.Format("{0}. {1}", IIf(LoanType = "HE", Common.GenerateAlphabetBulletName(index), index), item.Value)%></span>
		</div>
		<div class="col-sm-5 col-md-4 col-xs-12 no-padding">
			<fieldset data-role="controlgroup" data-type="horizontal">
				<input type="radio" name="rad_dec_<%=IDPrefix & item.Key%>" id="rad_dec_<%=IDPrefix & item.Key%>_no"
					value="N" data-mini="true" />
				<label for="rad_dec_<%=IDPrefix & item.Key%>_no">
					No</label>
				<input type="radio" name="rad_dec_<%=IDPrefix & item.Key%>" id="rad_dec_<%=IDPrefix & item.Key%>_yes"
					value="Y" data-mini="true" />
				<label for="rad_dec_<%=IDPrefix & item.Key%>_yes">
					Yes</label>
			</fieldset>
		</div>
		<div style="clear: both">
		</div>
	</div>
	<%If additionalQuestions IsNot Nothing AndAlso additionalQuestions.Count > 0 Then%>
	<div class="add-questions">
			<%For Each aq As Tuple(Of String, String, String, Boolean) In additionalQuestions
			%>
		<div data-role="fieldcontain">
			<label id="lbl_dec_<%=IDPrefix & aq.Item1%>" for="txt_dec_<%=IDPrefix & aq.Item1%>" <%=IIf(aq.Item4, "class=""RequiredIcon""", "")%> ><%=aq.Item2%></label>
			<input type="text" <%=IIf(aq.Item4, "data-required=""true""", "")%> class="<%=aq.Item3%>" name="txt_dec_<%=IDPrefix & aq.Item1%>" id="txt_dec_<%=IDPrefix & aq.Item1%>"/>
		</div>
	 <%Next%>
	</div>
	 <%End If
Next%>
	<%End If%>
</div>
<script type="text/javascript">

	$(function() {
		$("input:radio", $("#<%=IDPrefix%>divDeclarationSection")).on("change", function (e) {
			var $self = $(e.target);
			if ($self.val() == "Y") {
				$self.closest("div.Title").addClass("say-yes");
			} else {
				$self.closest("div.Title").removeClass("say-yes");
			}
			$("#<%=IDPrefix%>divDeclarationSection").trigger("change");
		});
		$("#<%=IDPrefix%>divDeclarationSection").observer({
			validators: [
				function (partial) {
					var $element = $(this);
					var validator = true;
					var msg = "Please answer all of declaration questions";
					var checkedCount = $('input:radio:checked', $element).length;
					var checkboxCount = $('input:radio', $element).length;
					validator = ((checkboxCount / 2) == checkedCount);
					if (validator === false) return msg;
					return "";
				}
			],
			validateOnBlur: true,
			group: "<%=IDPrefix%>ValidateDeclaration",
			groupType: "complexUI",
			placeHolder: "#<%=IDPrefix%>spValidateDeclarationMessage"
		});
		$("[data-required='true']", "#<%=IDPrefix%>divDeclarationSection").each(function (idx, ele) {
			$(ele).observer({
				validators: [
					function (partial) {
						var $self = $(this);
						if (!$self.is(":visible")) return "";
						if (!Common.ValidateText($self.val())) return "This field is required";
						return "";
					}
				],
				validateOnBlur: true,
				group: "<%=IDPrefix%>ValidateDeclaration",
			});
		});
	});
    
	function <%=IDPrefix%>ViewDeclaration() {
		var strHtml = "";
		<%If DeclarationList isnot nothing andalso DeclarationList.Count > 0 then %>
		<%
	Dim index As Integer = 0
	For Each item In DeclarationList
		index += 1
		Dim additionalQuestions = GetAdditionalQuestions(item.Key)
		%>
		var chk_<%=item.Key%> = $('input[name="rad_dec_<%=IDPrefix & item.Key%>"]:checked').val();
		strHtml += '<div class="col-xs-12 row-title"><span class="bold"><%=String.Format("{0}. {1}", index, HttpUtility.HtmlEncode(item.Value))%></span></div><div class="col-xs-12 row-data"><span>' + (chk_<%=item.Key%> == "Y" ? "Yes" : "No") + '</span></div>';

		<%If additionalQuestions IsNot Nothing AndAlso additionalQuestions.Count > 0 Then%>
		if (chk_<%=item.Key%> == "Y") {
			<%For Each aq In additionalQuestions%>
			if (Common.ValidateText($('#txt_dec_<%=IDPrefix & aq.Item1%>').val())) {
				strHtml += '<div class="col-xs-12 row-title"><span class="bold"><%=HttpUtility.HtmlEncode(aq.Item2)%></span></div><div class="col-xs-12 row-data"><span>' + htmlEncode($('#txt_dec_<%=IDPrefix & aq.Item1%>').val()) + '</span></div>';
			}
			<%Next%>
		}
		<%End If%>
		<%Next%>
		<%End If%>
		$(this).show();
		$(this).prev("div.row").show();
		return strHtml;
	}

	function <%=IDPrefix%>SetDeclarations(appInfo) {
		var declarations = [];
		var additionalDeclarations = {};
		<%If DeclarationList isnot nothing andalso DeclarationList.Count > 0 then %>
		<%	For Each item In DeclarationList
		Dim additionalQuestions = GetAdditionalQuestions(item.Key)
		%>
		declarations.push({ Key: '<%=item.Key%>', Value: $('input[name= "rad_dec_<%=IDPrefix & item.Key%>"]:checked').val() == "Y" });
		<%If additionalQuestions IsNot Nothing AndAlso additionalQuestions.Count > 0 Then%>
		if ($('input[name= "rad_dec_<%=IDPrefix & item.Key%>"]:checked').val() == "Y") {
		<%For Each aq In additionalQuestions%>
			additionalDeclarations['<%=aq.Item1%>'] = $('#txt_dec_<%=IDPrefix & aq.Item1%>').val();
			<%Next%>
		}
		<%End If%>
		<%Next%>
		<%End If%>
		appInfo.<%=IDPrefix%>Declarations = declarations;
		appInfo.<%=IDPrefix%>AdditionalDeclarations = additionalDeclarations;
		return appInfo;
	}
	function <%=IDPrefix%>autofill_Declarations() {
		$("#<%=IDPrefix%>divDeclarationSection input:radio[value='N']").each(function(idx, rad) {
			$(rad).prop("checked", true).checkboxradio().checkboxradio("refresh");
		});
	}
</script>

