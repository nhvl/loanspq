﻿Partial Class Inc_NewDocCapture
	Inherits CBaseUserControl

	Public Property DocTitleOptions As List(Of CDocumentTitleInfo)
	Public Property RequireDocTitle As Boolean
	Public Property DocUploadRequired As Boolean
	Private _oneColumnLayout As String
	''' <summary>
	''' Show uploaded file in single column. Default is false
	''' </summary>
	Property OneColumnLayout() As String
		Get
			If String.IsNullOrWhiteSpace(_oneColumnLayout) Then
				Return "N"
			End If
			Return _oneColumnLayout
		End Get
		Set(value As String)
			_oneColumnLayout = value
		End Set
	End Property

	Property Title As String
End Class

