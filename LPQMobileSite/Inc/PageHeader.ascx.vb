﻿
Imports System.Activities.Expressions
Imports System.Web.Security.AntiXss
Imports LPQMobile.Utils

Partial Class Inc_PageHeader
	Inherits CBaseUserControl

	Public _currentConfig As LPQMobile.Utils.CWebsiteConfig
	Public TimeOutUrl As String	'parent page can use this to override 
	Protected ServerRoot As String

	Public sThemeFileName As String
	Public restart_button As String
	Public IsHideGMI As Boolean
	Public IsHideGMI_HELOC As Boolean
	Public _headerDataTheme As String
	Public _headerDataTheme2 As String
	Public _contentDataTheme As String
	'Public _reviewDataTheme As String
	Public _footerDataTheme As String
	Public _buttonDataTheme As String
	Public _backgroundDataTheme As String
	Public _buttonFontColor As String
	Public HasFooterRight As String = ""
	Private _scriptFolder As String = ""
	Protected _queryStringDic As Dictionary(Of String, String)
	Protected ForeignPhone As Boolean
	Protected ForeignAddress As Boolean
	Public Property ScriptFolder As String
		Get
			Return _scriptFolder
		End Get
		Set(value As String)
			_scriptFolder = value
		End Set
	End Property


	Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
		sThemeFileName = LPQMobile.Utils.Common.Theme_File(_currentConfig)
		restart_button = LPQMobile.Utils.Common.getVisibleAttribute(_currentConfig, "restart_button")

		IsHideGMI = LPQMobile.Utils.Common.getVisibleAttribute(_currentConfig, "hide_government_monitoring").ToUpper() = "Y"
		IsHideGMI_HELOC = LPQMobile.Utils.Common.getVisibleAttribute(_currentConfig, "hide_government_monitoring_heloc").ToUpper() = "Y"
		ForeignPhone = Common.SafeString(Common.getVisibleAttribute(_currentConfig, "foreign_phone")).ToUpper().Equals("Y")
		ForeignAddress = Common.SafeString(Common.getVisibleAttribute(_currentConfig, "foreign_address")).ToUpper().Equals("Y")

		'assign these theme data in referrer page. avoid accessing and reading config file too many times
		'LPQMobile.Utils.Common.ProcessColorScheme(_currentConfig, _headerDataTheme, _footerDataTheme, _contentDataTheme, _headerDataTheme2, _reviewDataTheme)
		Dim port As String
		If (Request.Url.Port = 80) Then
			port = ""
		Else
			port = ":" + Request.Url.Port.ToString()
		End If

		ServerRoot = Request.Url.Scheme + "://" + Request.Url.Host + port
		If Not String.IsNullOrEmpty(LPQMobile.Utils.Common.getInnerTextMessage(_currentConfig, "FOOTER_RIGHT")) Then
			HasFooterRight = "Y"
		End If
        _queryStringDic = New Dictionary(Of String, String)()
        If Common.SafeString(Request.Form("acct_num0")) <> "" Then
            For Each key As String In Request.Form.AllKeys
                If key Is Nothing Or key = "" Then Continue For
                _queryStringDic.Add(AntiXssEncoder.HtmlEncode(key, True), AntiXssEncoder.HtmlEncode(Request.Form(key), True))
            Next
        Else
            For Each key As String In Request.QueryString.AllKeys
                If key Is Nothing Or key = "" Then Continue For
                _queryStringDic.Add(AntiXssEncoder.HtmlEncode(key, True), AntiXssEncoder.HtmlEncode(Request.QueryString(key), True))
            Next
        End If
    End Sub

    Private _lenderrefWhitelist() As String = {"redwoodcu", "mcu", "ecu", "floridastateucu", "floridascu", "ccfcu", "solaritycu", "alliancecu", "cusocal", "sdfcu", "fedchoicefcu", "fcfcu", "fuscu", "mfcu", "floridacu", "sdccu", "cacu", "neighborscu", "pslfcu", "cefcu", "keeslerfcu", "gbfcu"}
    'return framebreaker code if cu is not in whitelist
    Public Function getFrameBreakerCode() As String
		If IsInMode("777") Then Return ""
		Dim s As String = "<style id='antiClickjack'>body{display:none !important;}</style>" & _
			"<script type='text/javascript'>" & _
	  "if (self === top){var antiClickjack = document.getElementById('antiClickjack'); antiClickjack.parentNode.removeChild(antiClickjack);} else {top.location = self.location;}" & _
		"</script>"
		Dim sLenderRef As String = Request.Params("lenderref")
        If sLenderRef = "" Then Return s
        Dim disableFrameBreaker = Common.getNodeAttributes(_currentConfig, "BEHAVIOR", "disable_frame_breaker").ToUpper = "Y"
        If disableFrameBreaker Then Return "" ''via xml config
        For Each lender As String In _lenderrefWhitelist
			If sLenderRef.ToLower.Contains(lender) Then
				Return Nothing
			End If
		Next

		Return s 'frame breaker code
	End Function

	'return crossdomain code if cu is in the whitelist
	'ref:http://benalman.com/code/projects/jquery-postmessage/examples/iframe/
	Public Function getCrossDomainCode() As String
		Dim s As String = "<script src='/js/jquery.ba-postmessage.min.js?ver=<%=Common.GetBuildVersion()%>' type='text/javascript'></script>" & _
		"<script src='/js/CrossDomainRedwood.js?ver=<%=Common.GetBuildVersion()%>' type='text/javascript'></script>"
		Dim sLenderRef As String = Request.Params("lenderref")
		If sLenderRef = "" Then Return Nothing
		For Each lender As String In _lenderrefWhitelist
			If sLenderRef.Contains(lender) Then
				Return s
			End If
		Next
		Return Nothing
	End Function

	'Private LINKEDIN_API_KEY As String = "75bnra4rg9z0lc"  'Linh
	Private LINKEDIN_API_KEY As String = "757owcm36wb7lh"
	Public Function getLinkedInCode() As String
		Dim sb As New System.Text.StringBuilder
		With sb
			sb.AppendLine("<!-- LinkedIn Integration -->")
			sb.AppendLine("<script type='text/javascript' src='//platform.linkedin.com/in.js'>")
			sb.AppendLine("api_key:" & LINKEDIN_API_KEY)
			sb.AppendLine("authorize:true")
			'sb.AppendLine("scope:  r_basicprofile, r_emailaddress")
			sb.AppendLine("onLoad: onLinkedInLoad")
			sb.AppendLine("</script>")
		End With

		Dim bIsLinkedInEnabled As Boolean = LPQMobile.Utils.Common.getVisibleAttribute(_currentConfig, "linkedin_enable") = "Y"
		Dim bIsSSO As Boolean = LPQMobile.Utils.Common.SafeString(Request.Params("FName")) <> ""
		If bIsLinkedInEnabled And Not bIsSSO Then
			Return sb.ToString
		Else
			Return Nothing
		End If
	End Function

End Class
