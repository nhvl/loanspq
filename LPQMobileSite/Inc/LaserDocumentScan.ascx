﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="LaserDocumentScan.ascx.vb" Inherits="Inc_LaserDocumentScan" %>
<div class="divScanDocs laser-scan">
	<div id="<%=IDPrefix%>scandit-barcode-picker" style="max-width: 1280px; max-height: 530px; margin: 10px 0;"></div>
	<div class="scan-guide-text hidden"><p>Center align blue bar with driver's license barcode</p></div>
	<div class="scan-result-wrapper">
		<div class="scan-result-title">
			<img src="/images/list-icon.png" class="img-responsive"/>
			<p>Scan Result</p>
		</div>
		<div class="scan-result-list">
			<div>
				<div class="scan-result-name">Name</div>
				<div class="scan-result-value js-prop-name"></div>
			</div>
			<div>
				<div class="scan-result-name">DOB</div>
				<div class="scan-result-value js-prop-dob"></div>
			</div>
			<div>
				<div class="scan-result-name">Sex</div>
				<div class="scan-result-value js-prop-sex"></div>
			</div>
			<div>
				<div class="scan-result-name">Address</div>
				<div class="scan-result-value js-prop-address"></div>
			</div>
			<div>
				<div class="scan-result-name">License Number</div>
				<div class="scan-result-value js-prop-license-number"></div>
			</div>
			<div>
				<div class="scan-result-name">Issued Date</div>
				<div class="scan-result-value js-prop-issued-date"></div>
			</div>
			<div>
				<div class="scan-result-name">Expired Date</div>
				<div class="scan-result-value js-prop-expired-date"></div>
			</div>
		</div>
		<div class="scan-result-btn">
			<button onclick="acceptLaserScanResult('<%=IDPrefix%>', this)">Accept</button>
			<button onclick="hideLaserScanResult(this)">Rescan</button>
		</div>
	</div>
</div>