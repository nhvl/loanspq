﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="PiwikTracking.ascx.vb" Inherits="PiwikTracking" %>
<asp:Literal runat="server" ID="tracker" EnableViewState="false" />
<!-- Start Tracking Tag -->
<%If Not String.IsNullOrEmpty(_sGoogleAnalyticCode) Then%>
	<%=_sGoogleAnalyticCode%>
<%End If%>

<script type="text/javascript">
	var currentURL = function (titlePath) {
		if (titlePath == null) {
			 titlePath = '.ui-page-active .ui-title';
		}
		var title = jQuery(titlePath), currentStage = null, trackedURL = null;
		if (title.length > 0 && title[0].innerHTML.length > 0) {
			 currentStage = title[0].innerHTML.replace(/^\s+|\s+$/g, '');
		}
		else if ((title = jQuery('.ui-page-active .ui-content div[id]')).length > 0) {
			currentStage = title[0]['id'].replace(/^\s+|\s+$/g, ''); 
			
		}
		<%--Pass unencrypted URL to Analytic(Google, Piwik)--%>
		if (currentStage != null && typeof (currentStage) != 'undefined') {
			trackedURL = window.location.protocol + '//' + window.location.host + window.location.pathname + '/' + currentStage.toLowerCase().replace(/\s|[\/]/g, '-');
			//var params = (window.location.search || window.location.hash);
			//if (params != null && params != '') {
			//	trackedURL += params;
			//}
			trackedURL += '<%=System.Web.Security.AntiXss.AntiXssEncoder.HtmlEncode( Request.Url.Query,  True)%>';
			var hash = window.location.hash;
			if (hash !== null) trackedURL += hash;
		} else {
			trackedURL = '<%=System.Web.Security.AntiXss.AntiXssEncoder.HtmlEncode(Request.Url.AbsoluteUri, True)%>'; 
			
		}
		return { stage: currentStage, trackedURL: trackedURL };
	};
	var _paq = _paq || [];
	jQuery(document).ready(function () {
		var currURL = currentURL('.ui-page .ui-title');
		_paq.push(['setCustomUrl', currURL.trackedURL]);
		_paq.push(['trackPageView', currURL.stage]);
		_paq.push(['enableLinkTracking']);
		(function () {
			var u = 'https://analytics.loanspq.com/';
			_paq.push(['setTrackerUrl', u + 'piwik.php']);
			_paq.push(['setSiteId', <%=nPiWikID%>]);
			var d = document, g = d.createElement('script'), s = d.getElementsByTagName('script')[0];
			g.type = 'text/javascript'; 
			g.async = true; g.defer = true;
			g.src = '<%=PiwikURL%>';
			s.parentNode.insertBefore(g, s);
		})();
		//'trigger piwik per inner page load 
        var piwikFirstLoad = true;
		setTimeout(function () {
            jQuery(window).on('pageshow', function (event) {
                if (piwikFirstLoad != true) {
                    var currURL = currentURL();
                    _paq.push(['setCustomUrl', currURL.trackedURL]);
                    _paq.push(['trackPageView', currURL.stage]);
                }
                piwikFirstLoad = false;
			});
		}, 800);
		<%--trigger ga per inner page load --%>
		<%If _sGoogleAnalyticCode <> "" Then%>
			// Start GA Tag
			setTimeout(function () {
				jQuery(window).on('pageshow', function (event) {
					var currURL = currentURL();
			        //check function ga exist before calling
					if(typeof ga == 'function'){
						ga('set', 'page', currURL.trackedURL );
						ga('send', 'pageview' );
			        } //end check function ga
				});
			}, 800);
			//End GA Tag
		<%End If%>
	});
</script>

<% If Not String.IsNullOrEmpty(GOOGLE_TAG_MANAGER_ID) Then%>
<!-- Google Tag Manager -->
<noscript role='complementary' aria-label='Google Tag Manager'><iframe src='https://www.googletagmanager.com/ns.html?id=<%=GOOGLE_TAG_MANAGER_ID%>' height='0' width='0' style='display:none;visibility:hidden'></iframe></noscript>
<script type="text/javascript">
	var pageFirstLoad = true;
	var googleTagManager = function(w, d, s, l, i) {
		w[l] = w[l] || [];
		w[l].push({ 'gtm.start': new Date().getTime(), event: 'gtm.js' });
		var f = d.getElementsByTagName(s)[0], j = d.createElement(s), dl = l !== 'dataLayer' ? '&l=' + l : '';
		j.async = true;
		j.src = 'https://www.googletagmanager.com/gtm.js?id=' + i + dl;
		f.parentNode.insertBefore(j, f);
	};
	googleTagManager(window, document, 'script', 'dataLayer', '<%=GOOGLE_TAG_MANAGER_ID%>');
	<%--call googleTagManager every pageload init(only cause one ga trigger per init - don't use pageshow  because it cause mulitple trigger per page(error popup, redraw page)
	$(document).on('pageinit', function(event, ui) {--%>
        jQuery(window).on('pageshow', function (event) {
			<%--Fix GTM fired 2 times when page first load--%>
            if (pageFirstLoad != true) {
                //console.log('GTM fired.');
				var currURL = currentURL();
				dataLayer.push({
					'event': 'VirtualPageview',
					'virtualPageURL': currURL.trackedURL,
					'virtualPageTitle': currURL.stage,
					//This is still need for backward compatible and doesn't require client to add custom variable
					'Page': currURL.trackedURL,
					'Page URL': currURL.trackedURL
				});
			}
			pageFirstLoad = false;
		}); <%--end check function ga-->
	<%--});--%>
</script>
<!-- End Google Tag Manager -->
<%End If %>
<%--image for browser that doesn't support js--%>
<noscript role="complementary" aria-label="No Script Browser Image"><p><img src="https://analytics.loanspq.com/piwik.php?idsite=<%=nPiWikID.ToString%>" style="border:0" alt="" /></p></noscript>

<!-- Facebook Pixel tracking -->
<% If Not String.IsNullOrEmpty(FACEBOOK_PIXEL_ID) Then%>
    <!-- Facebook Pixel Code -->
    <script>
      var fbFirstPageLoad = true;

      !function(f,b,e,v,n,t,s)
      {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
      n.callMethod.apply(n,arguments):n.queue.push(arguments)};
      if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
      n.queue=[];t=b.createElement(e);t.async=!0;
      t.src=v;s=b.getElementsByTagName(e)[0];
      s.parentNode.insertBefore(t,s)}(window, document,'script',
      'https://connect.facebook.net/en_US/fbevents.js');
      fbq('init', '<%=FACEBOOK_PIXEL_ID%>');
      //fbq('track', 'PageView');

        jQuery(window).on('pageshow', function (event) {
            if (fbFirstPageLoad != true) {
                //console.log('facebook pixel fired.');
                var currURL = currentURL();

                // FB doesn't support multiple 'PageView' event on a page, use 'ViewContent' instead
			    fbq('track', 'ViewContent', {
			        content_name: currURL.stage
			    });

			    //fbq('track', currURL.stage); // Enable this for 'non-standard FB events'
		    }
            fbFirstPageLoad = false;
        });

    </script>
    <noscript role='complementary' aria-label='Facebook Pixel'><img height="1" width="1" style="display:none" src="https://www.facebook.com/tr?id=<%=FACEBOOK_PIXEL_ID%>&ev=PageView&noscript=1" /></noscript>
<!-- End Facebook Pixel Code -->
<% End If %>

<!-- End Tracking Tag -->