﻿Option Strict On

Imports LPQMobile.Utils


''' <summary>
''' 1) Track pageview via piwik when there is a piwik_site_id attribute in Website node - still track using default piwkisite_id if there is no piwik_site_id in config.
''' 2) Track pageview via Google Tag Manager(Google Analytic) when there is a google_tag_manager_id attribute in Website node.
''' 2) also track pagview via Google analytic when GOOGLE_ANALYTIC_CODE node is not empty(old method)
''' </summary>
''' <remarks>realtime ok on IE , analytics : display realtime ONLY for "vististor in REal-time" in dash board, other including detail is update every hour</remarks>
Partial Class PiwikTracking
	Inherits CBaseUserControl

	' If any Org doesn't have their own piwik site ID ( means they don't track their activities)
	' we will use our default one to track their activity.  They won't know about it
	' Our default is the master tracking for all organizations

	'TODO need to change to the correct one
	Protected nPiWikID As Integer = 132	' 132 default piwik site for mobile website, 1 for lpq demo, 69 for all or

	Protected GOOGLE_TAG_MANAGER_ID As String
    Protected FACEBOOK_PIXEL_ID As String

	Protected _sGoogleAnalyticCode As String = ""

	Protected PiwikURL As String = ""

	Private Sub InitPiwikTracking()
		'' parent page should be CBasePage
		Try
			Dim parent As CBasePage = TryCast(Me.Page, CBasePage)
			If parent IsNot Nothing AndAlso parent._CurrentWebsiteConfig IsNot Nothing Then
				Dim oUpLoadNode As System.Xml.XmlNode = parent._CurrentWebsiteConfig._webConfigXML.SelectSingleNode("GOOGLE_ANALYTIC_CODE")
				If oUpLoadNode IsNot Nothing Then
					If oUpLoadNode.InnerText <> "" Then
						_sGoogleAnalyticCode = oUpLoadNode.InnerText
					End If
				End If

				'user defined takes precendence over all
				If Common.SafeInteger(parent._CurrentWebsiteConfig.PiWikSiteID) <> 0 Then
					nPiWikID = Common.SafeInteger(parent._CurrentWebsiteConfig.PiWikSiteID)
				End If
				PiwikURL = Request.Url.GetLeftPart(UriPartial.Authority) & "/inc/Piwik/piwikHandler.ashx"
				GOOGLE_TAG_MANAGER_ID = parent._CurrentWebsiteConfig.GoogleTagManagerID
                FACEBOOK_PIXEL_ID = parent._CurrentWebsiteConfig.FacebookPixelID
			End If
		Catch ex As Exception
			'if page can't be type case to cbasepage tehn track it via mobile external dev
			nPiWikID = 232
			PiwikURL = Request.Url.GetLeftPart(UriPartial.Authority) & "/inc/Piwik/piwikHandler.ashx"
		End Try
	End Sub


	Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
		InitPiwikTracking()
		'applyPiWikTracking()
	End Sub

	

End Class
