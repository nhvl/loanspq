﻿<%@ WebHandler Language="VB" Class="piwikHandler" %>

Imports System
Imports System.Web
Imports System.Net
Imports System.Reflection

''' <summary>
''' This class exist to provide quick cached content of external resource.  If external resource are slow
''' to retrieve, it can have negative impact on cient side page rendering.
''' </summary>
Public Class piwikHandler : Implements IHttpHandler
    
    Private Shared log As log4net.ILog = log4net.LogManager.GetLogger(GetType(piwikHandler))
    
    Public Sub ProcessRequest(ByVal context As HttpContext) Implements IHttpHandler.ProcessRequest
        Dim content As String = GetPiwikResource(context)
        
        If content = "" Then
            Return
        End If
        
		'new design doesn't need this bc js is called base on page show event
		'content += ";piwik_init();"
		content += ";"
        context.Response.ContentType = "text/javascript"
        context.Response.Write(content)
    End Sub
 
    
    Private Function GetPiwikResource(ByVal pContext As HttpContext) As String
        Dim cacheKey As String = "PIWIK_SCRIPT"
        Dim content As String
     

        content = DirectCast(pContext.Cache.Get(cacheKey), String)
        If Not String.IsNullOrWhiteSpace(content) Then
            Return content
        End If

        SyncLock _CacheLock
            content = DirectCast(pContext.Cache.Get(cacheKey), String) ' second check to see if we can avoid I/O
            If Not String.IsNullOrWhiteSpace(content) Then
                Return content
            End If
            
            log.Info("StaticResource piwik not found in cache")
            
            Dim url As String = "https://analytics.loanspq.com/piwik.js"
            
			Try
				'old code
				'content = CWebRequest.GetDataFrom(url)
				
				'new code without websitelib
				Dim responseText As String = LPQMobile.Utils.Common.webGet(url)
				content = responseText
				
			Catch ex As Exception
				log.InfoFormat("We encounter exception when trying to retrieve the piwik resource : {0}", ex.Message)
				log.Error("We encounter exception when trying to retrieve the piwik resource :", ex)
				Return ""
			End Try
                             
            If content = "" Then
                Return content    ' empty string
            End If
                        
            pContext.Cache.Insert(cacheKey, content, Nothing, Now.AddHours(23), System.Web.Caching.Cache.NoSlidingExpiration)
            
        End SyncLock
        Return content
    End Function
    
    Private Shared _CacheLock As New Object
    
    Public ReadOnly Property IsReusable() As Boolean Implements IHttpHandler.IsReusable
        Get
            Return False
        End Get
    End Property

End Class