﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="PageFooter.ascx.vb" Inherits="Inc_PageFooter" %>
<%@ Import Namespace="LPQMobile.Utils" %>
<%@ Import Namespace="System.Web.Optimization" %>
<%@ Import Namespace="System.Activities.Statements" %>
<input type="hidden" id="hdPreviewCode" value="<%=PreviewCode %>"/>
<input type="hidden" id="hdEmode" value="<%=IIf(IsInMode("777"), "true", "")%>"/>
<div id="divThemeController" style="display: none">
	<div data-theme-role="footer" <%=IIf(Regex.IsMatch(FooterDataTheme, "#[A-Za-z0-9]{6}"), "style='background-color: " & FooterDataTheme & ";'", "class='ui-bar-" & FooterDataTheme & "'")%>></div>
	<div data-theme-role="footerFont" <%=IIf(Regex.IsMatch(FooterFontDataTheme, "#[A-Za-z0-9]{6}"), "style='color: " & FooterFontDataTheme & ";'", "")%>></div>
	<div data-theme-role="background" <%=IIf(Regex.IsMatch(BackgroundDataTheme, "#[A-Za-z0-9]{6}"), "style='background-color: " & BackgroundDataTheme & ";'", "class='ui-bar-" & BackgroundDataTheme & "'")%>></div>
	<div data-theme-role="header" <%=IIf(Regex.IsMatch(HeaderDataTheme, "#[A-Za-z0-9]{6}"), "style='background-color: " & HeaderDataTheme & ";'", "class='ui-bar-" & HeaderDataTheme & "'")%>></div>
	<div data-theme-role="header2" <%=IIf(Regex.IsMatch(HeaderDataTheme2, "#[A-Za-z0-9]{6}"), "style='background-color: " & HeaderDataTheme2 & ";'", "class='ui-bar-" & HeaderDataTheme2 & "'")%>></div>
	<div data-theme-role="button" <%=IIf(Regex.IsMatch(ButtonDataTheme, "#[A-Za-z0-9]{6}"), "style='background-color: " & ButtonDataTheme & ";'", "class='ui-btn ui-btn-" & ButtonDataTheme & "'")%>></div>
	<div data-theme-role="content" <%=IIf(Regex.IsMatch(ContentDataTheme, "#[A-Za-z0-9]{6}"), "style='background-color: " & ContentDataTheme & ";'", "class='ui-body-" & ContentDataTheme & "'")%>></div>
</div>
<%:Scripts.Render("~/js/newdocumentscan")%>
<%:Scripts.Render("~/js/newdoccapture")%>
<script type="text/javascript">
	
	var gl_background_data_theme = "<%=BackgroundDataTheme%>";
	var EXTRACT_BUTTONLABEL_REGEX = /^[_\w -:\$\?\.\(\)]+/i;
	var BUTTONLABELLIST = null;
	<%If _buttonLabelListStr <> "[]" And _buttonLabelListStr <> "" Then%>
	BUTTONLABELLIST = <%=_buttonLabelListStr%>;
	<%End If%>

	var HIDEFIELDLIST = null;
	<%If _hideFieldListStr <> "[]" And _hideFieldListStr <> "" Then%>
	HIDEFIELDLIST = <%=_hideFieldListStr%>;
	<%End If%>
	
	<%--var SHOWFIELDLIST = null;
    <%If _showFieldListStr <> "[]" And _showFieldListStr <> "" Then%>
	SHOWFIELDLIST = <%=_showFieldListStr%>;
    <%End If%>--%>

	var RELOCATELIST = null;
	<%If _relocateListStr <> "[]" And _relocateListStr <> "" Then%>
	RELOCATELIST = <%=_relocateListStr%>;
	<%End If%>

	var INJECTIONLIST = null;
	<%If _injectionListStr <> "[]" And _injectionListStr <> "" Then%>
	INJECTIONLIST = <%=_injectionListStr%>;
	<%End If%>
	var ADVANCEDLOGICLIST = null;
	<%If _advancedLogicListStr <> "[]" And _advancedLogicListStr <> "" Then%>
	ADVANCEDLOGICLIST = <%=_advancedLogicListStr%>;
	<%End If%>
	var laserScanEnabled = false;
	<%If LaserBarcodeScanEnabled then %>
	laserScanEnabled = true;
	<%End If%>
	<%If Not String.IsNullOrEmpty(Common.SafeString(Request.Params("previewcode"))) Then%>
		$(function() {
			$("body").prepend('<div id="divPreviewNoty" class="preview-panel">Preview mode</div>');
		});
	<%End If%>
</script>

<%:Scripts.Render("~/js/pagefooter")%>
<%If IsInMode("777") Then%>
<script type="text/javascript" src="/js/ThirdParty/easyxdm/easyXDM.debug.js"></script>
<script type="text/javascript" src="/sm/js/admin.js?<%=Now.Ticks%>"></script>
<%End If%>

