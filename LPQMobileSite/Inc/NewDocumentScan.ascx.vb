﻿
Imports LPQMobile.Utils

Partial Class Inc_NewDocumentScan
	Inherits CBaseUserControl
	Public CurrentConfig As LPQMobile.Utils.CWebsiteConfig
	Protected DocScanMessage As String = ""
	Protected DLScanImage As String
	Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
		If Not String.IsNullOrEmpty(CurrentConfig.ScanDocumentKey) Then
			DocScanMessage = Server.HtmlDecode(Common.getDocumentScanMessage(CurrentConfig))
		End If
		If String.IsNullOrEmpty(DocScanMessage) Then
            DocScanMessage = "<div id='" & IDPrefix & "docScanMessage'><div class='title-style'>Speed up your application process by taking the BACK image of your US driver's license.</div><div class='scanDocsContentLeft-style'>If you do not wish to use this feature, simply close this popup window and proceed.</div></div>"
        End If
		DLScanImage = "../images/dl_back_sample.jpg" '' by default
		If Not String.IsNullOrEmpty(CurrentConfig.DLScanImage) Then
			''Dim path As String = HttpContext.Current.Server.MapPath("../images/custom/" & _currentConfig.DLScanImage)
			''If File.Exists(path) Then
			''    _oDLScanImage = "../images/custom/" & _currentConfig.DLScanImage
			''End If
			DLScanImage = CurrentConfig.DLScanImage
		End If
	End Sub
End Class
