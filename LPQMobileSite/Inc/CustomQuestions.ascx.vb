﻿Imports LPQMobile.BO
Imports LPQMobile.Utils
Imports System.Xml
Imports System.Web.Script.Serialization
Partial Class Inc_CustomQuestions
    Inherits CBaseUserControl
    Public CustomQuestions As List(Of CCustomQuestion)
    Public ConditionedQuestions As List(Of CCustomQuestion)
    Public CustomQuestionXAs As List(Of CCustomQuestionXA)
    Public CustomQuestionBLs As List(Of CCustomQuestionXA)
    'Public LoanType As String '"CC,VL,PL,HE, HELOC, XA
    Protected _CustomQuestionsHML As String    
    Protected _CustomQuestionHiddenInputs As String
    Protected _divCustomQuestionId As String = "div_custom_questions"
    Protected _strCQHtmlHE As String
    Protected _strCQHtmlHELOC As String
	Protected isCQNewAPI As Boolean = False
    Protected _minorAccountTypes As New Dictionary(Of String, List(Of String))
    Protected _URLParaCustomQuestions As New List(Of CURLParaCustomQuestionAndAnswer) ''contain list of  question name, text,  answer, and type
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
		Dim availability As Integer = 1 ''default PRIMARY
        If Not String.IsNullOrEmpty(Request.QueryString("type")) Then
            availability = Common.SafeInteger(Request.QueryString("type").Substring(0, 1))
        End If

        '' parent page should be CBasePage
        Dim parent As CBasePage = TryCast(Me.Page, CBasePage)
        If parent Is Nothing Or parent._CurrentWebsiteConfig Is Nothing Then
            Return
        End If


        'Merchant stuff
        Dim sMerchantID As String = Request.QueryString("merchantid")
        Dim sXPathVisible As String = String.Format("MERCHANTS/MERCHANT[@merchant_id='{0}']/VISIBLE", sMerchantID)
        Dim oMerchantVisibleNode As XmlNode = parent._CurrentWebsiteConfig._webConfigXML.SelectSingleNode(sXPathVisible)
        Dim bIsVisibleCustomQuestion As Boolean = False
        If oMerchantVisibleNode IsNot Nothing Then
            bIsVisibleCustomQuestion = If(CType(oMerchantVisibleNode, XmlElement).GetAttribute("custom_question") = "N", False, True)
        End If
        If Request.QueryString("merchantid") <> "" And Not bIsVisibleCustomQuestion Then    'clinic has no question
            Return
        End If

        isCQNewAPI = Common.getVisibleAttribute(parent._CurrentWebsiteConfig, "custom_question_new_api") = "Y"
        initCustomQuestion()

        If CustomQuestions IsNot Nothing AndAlso CustomQuestions.Count > 0 Then   'this is for loan
            Dim ParseUrlParaCustomQuestionsAndAnswers = Common.parseUrlParaCustomQuestionNamesAndAnswers(HttpContext.Current.Request.Url.Query.safeQueryString)
            If ParseUrlParaCustomQuestionsAndAnswers IsNot Nothing AndAlso ParseUrlParaCustomQuestionsAndAnswers.Any() Then
                _URLParaCustomQuestions = Common.getUrlParaCustomQuestionsAndAnswers(parent._CurrentWebsiteConfig, LoanType, False, CustomQuestions, ParseUrlParaCustomQuestionsAndAnswers)
            End If
            ''filter custom questions if there exist url parameter CustomQuestions
            ''no render custom questions if the questions already in the _UrlParaCustom Questions,         
            If _URLParaCustomQuestions IsNot Nothing AndAlso _URLParaCustomQuestions.Any() Then
                Dim oFilteredCQs As New List(Of CCustomQuestion)
                For Each oCQ In CustomQuestions
                    Dim oUrlParaCQ = _URLParaCustomQuestions.FirstOrDefault(Function(cq) cq.Name = oCQ.Name)
                    If oUrlParaCQ IsNot Nothing Then Continue For '' go to the next question
                    oFilteredCQs.Add(oCQ)
                Next
                CustomQuestions = oFilteredCQs
            End If
            ''filter url para custom questions in the _URLParaCustomQuestions: make sure the answer of each question is not empty
            _URLParaCustomQuestions = _URLParaCustomQuestions.Where(Function(cq) Not String.IsNullOrWhiteSpace(cq.Answer)).ToList()

            _CustomQuestionsHML = Server.HtmlDecode(Common.RenderCustomQuestions(CustomQuestions, ConditionedQuestions))
            _CustomQuestionHiddenInputs = Common.RenderCustomQuestionHiddenInputs(CustomQuestions)
            If LoanType = "HE" Then
                _strCQHtmlHE = New JavaScriptSerializer().Serialize(_CustomQuestionHiddenInputs + _CustomQuestionsHML)
            End If
            If LoanType = "HELOC" Then
                _strCQHtmlHELOC = New JavaScriptSerializer().Serialize(_CustomQuestionHiddenInputs + _CustomQuestionsHML)
            End If

        ElseIf CustomQuestionXAs IsNot Nothing AndAlso CustomQuestionXAs.Count > 0 Then
            _CustomQuestionsHML = Server.HtmlDecode(Common.RenderCustomQuestionXAs(CustomQuestionXAs, ConditionedQuestions, availability))
            _CustomQuestionHiddenInputs = Common.RenderCustomQuestionHiddenInputXAs(CustomQuestionXAs, availability)
            ''get special account type
            If Common.SafeString(Request.QueryString("type")) = "1a" Or Common.SafeString(Request.QueryString("type")) = "2a" Then
                If CustomQuestionXAs.Count > 0 Then
                    For Each oCQ In CustomQuestionXAs
                        If oCQ.Name <> "" And Not _minorAccountTypes.ContainsKey(oCQ.Name) Then
                            _minorAccountTypes.Add(oCQ.Name, oCQ.specialAccountTypes)
                        End If
                    Next
                End If
            End If
        ElseIf CustomQuestionBLs IsNot Nothing AndAlso CustomQuestionBLs.Count > 0 Then
            ConditionedQuestions = New List(Of CCustomQuestion) ''using advance logic instead of ConditionedQuestions
            _CustomQuestionsHML = Server.HtmlDecode(Common.RenderCustomQuestionXAs(CustomQuestionBLs, ConditionedQuestions, availability))
            _CustomQuestionHiddenInputs = Common.RenderCustomQuestionHiddenInputXAs(CustomQuestionBLs, availability)
        End If

    End Sub
    Private Sub initCustomQuestion()
        '' parent page should be CBasePage
        Dim parent As CBasePage = TryCast(Me.Page, CBasePage)
        If parent Is Nothing Or parent._CurrentWebsiteConfig Is Nothing Then
            Return
        End If
        If LoanType = "BL" Then
            Return 'Cus passsed from parent
        End If
        If LoanType = "XA" Then
            ConditionedQuestions = Common.GetConditionQuestionXAs(parent._CurrentWebsiteConfig, isCQNewAPI, Common.SafeString(Request.QueryString("type")))
            Return 'Cus passsed from parent
        End If
        If LoanType = "HELOC" Or LoanType = "HE" Then
            _divCustomQuestionId = _divCustomQuestionId & "_" & LoanType
        End If
        ''set custom question from new api
        Dim CCQuestions As List(Of CCustomQuestion) = parent._CurrentWebsiteConfig.GetCustomQuestions(Common.GetConfigLoanTypeFromShort(LoanType))
        Dim conCCQuestions As List(Of CCustomQuestion) = parent._CurrentWebsiteConfig.GetConditionedQuestions(Common.GetConfigLoanTypeFromShort(LoanType))

        ''allow questions with new api
        If isCQNewAPI Then
            CCQuestions = CCustomQuestionNewAPI.getAllowedCustomQuestions(parent._CurrentWebsiteConfig, LoanType, False)
            conCCQuestions = CCustomQuestionNewAPI.getConditionedQuestions(parent._CurrentWebsiteConfig, LoanType, False, conCCQuestions)
        End If

        Dim questions As New List(Of CCustomQuestion)
        Dim conQuestions As New List(Of CCustomQuestion)
        questions = CCQuestions
        conQuestions = conCCQuestions
        'Since consumer will be applying for both loan and membership(XA) at the same time, the system needs to show questions and disclosures for both loan and XA
        If parent.IsComboMode Then
            'get XA customquestions
            Dim oAllowedXACustomQuestionNames As List(Of String) = Common.GetXAAllowedCustomQuestionNames(parent._CurrentWebsiteConfig)
            Dim XAQuestions As New List(Of CCustomQuestionXA)
            Dim conXAQuestions As New List(Of CCustomQuestion)
            Dim currentDownLoadedXAQuestions As New List(Of CCustomQuestionXA)
            If isCQNewAPI Then
                currentDownLoadedXAQuestions = CCustomQuestionNewAPI.getDownloadedXACustomQuestions(parent._CurrentWebsiteConfig, False, "XA", Common.SafeString(Request.QueryString("type")))
            Else
                currentDownLoadedXAQuestions = CCustomQuestionXA.CurrentXACustomQuestions(parent._CurrentWebsiteConfig)
            End If

            If oAllowedXACustomQuestionNames.Count > 0 Then
                For Each oItem As CCustomQuestionXA In currentDownLoadedXAQuestions
                    If Not oAllowedXACustomQuestionNames.Contains(oItem.Name.ToLower) Then
                        Continue For
                    End If
                    XAQuestions.Add(oItem)
                Next
            Else
                XAQuestions = currentDownLoadedXAQuestions
            End If
            conXAQuestions = Common.GetConditionQuestionXAs(parent._CurrentWebsiteConfig, isCQNewAPI, Common.SafeString(Request.QueryString("type")))
            'end get XA customquestions
            'do merge question
            For Each xaQ As CCustomQuestionXA In XAQuestions
                If xaQ.IsHeader Or xaQ.AccountPositionScope = "S" Then
                    Continue For
                End If
                If CCQuestions.Any(Function(ccQ As CCustomQuestion) ccQ.Name = xaQ.Name) Then
                    Continue For
                End If
                Dim cqItem As New CCustomQuestion()
                With cqItem
                    .Name = xaQ.Name
                    .Text = xaQ.Text
                    .Value = String.Empty
                    Select Case xaQ.AnswerType
                        Case "TEXTBOX"
                            .UIType = QuestionUIType.TEXT
                            .DataType = xaQ.DataType
                            Select Case xaQ.DataType
                                Case "D", "N"
                                    .maxLength = "10"
                                Case "C"
                                    .maxLength = String.Empty
                                Case "P"

                                    If Not String.IsNullOrEmpty(xaQ.maxLength) Then
                                        .maxLength = xaQ.maxLength
                                    Else
                                        .maxLength = "20"
                                    End If
                                Case Else
                                    .maxLength = "150"
                            End Select
                        Case "DROPDOWN"
                            .UIType = QuestionUIType.DROPDOWN
                        Case "CHECKBOX"
                            .UIType = QuestionUIType.CHECK
                    End Select
                    If xaQ.IsRequired Then
                        .isRequired = "Y"
                    Else
                        .isRequired = "N"
                    End If
                    .Conditioning = String.Empty
                    .RegExpression = xaQ.reg_expression
                    .maxValue = String.Empty
                    .minValue = String.Empty
                    .minLength = xaQ.minLength
                    .errorMessage = xaQ.ErrorMessage
                    .ConfirmationText = xaQ.ConfirmationText
                    .CustomQuestionOptions = xaQ.CustomQuestionOptions
                End With
                questions.Add(cqItem)
            Next
            For Each conXAQ As CCustomQuestion In conXAQuestions
                If conCCQuestions.Any(Function(conCCQ As CCustomQuestion) conCCQ.Name = conXAQ.Name) Then
                    Continue For
                End If
                conQuestions.Add(conXAQ)
            Next
            'end do merge questions
        End If
        CustomQuestions = CCQuestions
        ConditionedQuestions = conCCQuestions
    End Sub

End Class
