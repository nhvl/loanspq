﻿
Imports LPQMobile.BO
Imports Newtonsoft.Json
Imports LPQMobile.Utils
Imports System.Xml
Partial Class Inc_PageFooter
	Inherits CBaseUserControl

    Protected ButtonLabelList As Dictionary(Of String, String)
	Protected InjectionList As List(Of KeyValuePair(Of String, String))
	Protected HideFieldList As Dictionary(Of String, String)
	'Protected ShowFieldList As List(Of String)
	Protected RelocateList As Dictionary(Of String, String)
	Protected AdvancedLogicList As List(Of CAdvancedLogicItem)
	Protected _buttonLabelListStr As String = "[]"
	Protected _hideFieldListStr As String = "[]"
	'Protected _showFieldListStr As String = "[]"
	Protected _relocateListStr As String = "[]"
	Protected _injectionListStr As String = "[]"
	Protected _advancedLogicListStr As String = "[]"
	Protected LaserBarcodeScanEnabled As Boolean = False
	Private Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load

		If Not Page.IsPostBack Then
			InitPageFooter()

			If ButtonLabelList IsNot Nothing And ButtonLabelList.Any() Then
				_buttonLabelListStr = JsonConvert.SerializeObject(ButtonLabelList)
			End If
			If HideFieldList IsNot Nothing AndAlso HideFieldList.Any() Then
				_hideFieldListStr = JsonConvert.SerializeObject(HideFieldList)
			End If
			'If ShowFieldList IsNot Nothing AndAlso ShowFieldList.Any() Then
			'	_showFieldListStr = JsonConvert.SerializeObject(ShowFieldList)
			'End If
			If RelocateList IsNot Nothing AndAlso RelocateList.Any() Then
				_relocateListStr = JsonConvert.SerializeObject(RelocateList)
			End If
			If InjectionList IsNot Nothing AndAlso InjectionList.Any() Then
				_injectionListStr = JsonConvert.SerializeObject(InjectionList)
			End If
			If AdvancedLogicList IsNot Nothing AndAlso AdvancedLogicList.Any() Then
				_advancedLogicListStr = JsonConvert.SerializeObject(AdvancedLogicList)
			End If
		End If
	End Sub


	Private Sub InitPageFooter()
		'' parent page should be CBasePage
		Dim parent As CBasePage = TryCast(Me.Page, CBasePage)
		If parent IsNot Nothing AndAlso parent._CurrentWebsiteConfig IsNot Nothing Then
			HeaderDataTheme = parent._HeaderDataTheme
			FooterDataTheme = parent._FooterDataTheme
			FooterFontDataTheme = parent._FooterFontDataTheme
			ContentDataTheme = parent._ContentDataTheme
			HeaderDataTheme2 = parent._HeaderDataTheme2
			BackgroundDataTheme = parent._BackgroundTheme
			ButtonDataTheme = parent._ButtonTheme


			Dim sLoanType As String = parent._LoanType.ToLower
			Dim sLoanTypeOther As String = parent._LoanTypeOther.NullSafeToLower_
			Dim sLoanNodeName As String = parent._LoanNodeName
			If parent.IsComboMode Then
				sLoanType = sLoanType & "_combo"
			End If
			ButtonLabelList = getRenameButtonLabelList(parent._CurrentWebsiteConfig, sLoanType, sLoanTypeOther)
			HideFieldList = GetHideFieldList(parent._CurrentWebsiteConfig, sLoanType, sLoanTypeOther)
			'ShowFieldList = GetShowFieldList(parent._CurrentWebsiteConfig, sLoanType, sLoanTypeOther)
			RelocateList = GetRelocateList(parent._CurrentWebsiteConfig, sLoanType, sLoanTypeOther)
			InjectionList = GetInjectionList(parent._CurrentWebsiteConfig, sLoanType, sLoanTypeOther)
			AdvancedLogicList = GetAdvancedLogics(parent._CurrentWebsiteConfig, sLoanType)
			AdvancedLogicList.AddRange(GetAdvancedLogicListsFromConditionedQuestions(parent._CurrentWebsiteConfig, parent._LoanType, sLoanNodeName))
			If parent._CurrentWebsiteConfig.DLBarcodeScan = "Y" Then
				LaserBarcodeScanEnabled = True
			End If
		End If
	End Sub

	Private Function getRenameButtonLabelList(ByVal poCurrentConfig As CWebsiteConfig, ByVal loanTypeName As String, ByVal loanTypeOther As String) As Dictionary(Of String, String)
		Dim dic As New Dictionary(Of String, String)
		Dim xpath As String = "CUSTOM_LIST/RENAME_BUTTON_LABEL/ITEM"
		Dim oEnumsNodes As XmlNodeList = poCurrentConfig._webConfigXML.SelectNodes(xpath)
		For Each node As XmlElement In oEnumsNodes
			Dim sLoanType = Common.SafeString(node.GetAttribute("loan_type")) 'loan_type may be null
			If sLoanType <> "" Then
				Dim arrayLoanType() As String = sLoanType.ToLower().Split("|"c)

				'For backward compatibility and avoid the need for migration script, use values “xa_special” and “sa_special” for loan_type attribute for both XA Special and XA Minor - don't use values “xa_minor” or “sa_minor”
				'This may have the side effect that a change in XA Special may leak to XA Minor. We would have to ask client to use as-is.
				'Dim bIsXAMinorApp_APM = (loanTypeOther <> "" AndAlso arrayLoanType.Contains("xa_minor") And Regex.IsMatch(loanTypeOther.Trim().ToLower(), "^(1a)$"))
				'Dim bIsSAMinorApp_APM = (loanTypeOther <> "" AndAlso arrayLoanType.Contains("sa_minor") And Regex.IsMatch(loanTypeOther.Trim().ToLower(), "^(2a)$"))
				Dim bIsXASpecialApp_APM = (loanTypeOther <> "" AndAlso arrayLoanType.Contains("xa_special") And Regex.IsMatch(loanTypeOther.Trim().ToLower(), "^(1s|1a)$"))
				Dim bIsSASpecialApp_APM = (loanTypeOther <> "" AndAlso arrayLoanType.Contains("sa_special") And Regex.IsMatch(loanTypeOther.Trim().ToLower(), "^(2s|2a)$"))
				Dim bIsXABusinessApp_APM = (loanTypeOther <> "" AndAlso arrayLoanType.Contains("xa_business") And Regex.IsMatch(loanTypeOther.Trim().ToLower(), "^(1b)$"))
				Dim bIsSABusinessApp_APM = (loanTypeOther <> "" AndAlso arrayLoanType.Contains("sa_business") And Regex.IsMatch(loanTypeOther.Trim().ToLower(), "^(2b)$"))
				Dim bIsSpecialApp_Legacy = (loanTypeOther <> "" AndAlso arrayLoanType.Contains(loanTypeOther))	'(loanTypeOther=1a,1b,2a,2b and loan_type=1a,1b,2a,2b)   or (loanTypeOther=MERCHANT and loan_type="MERCHANT")
				Dim bIsNormalorComboLoan = arrayLoanType.Contains(loanTypeName) AndAlso (loanTypeName.ToLower() <> "xa" OrElse Not Regex.IsMatch(loanTypeOther.Trim().ToLower(), "^(1a|2a|1b|2b|1s|2s)$"))	   ' ex: VL, VL_COMBO

				If bIsSpecialApp_Legacy OrElse bIsXASpecialApp_APM OrElse bIsSASpecialApp_APM OrElse bIsNormalorComboLoan OrElse bIsXABusinessApp_APM OrElse bIsSABusinessApp_APM Then
					Dim key As String = Common.SafeString(node.GetAttribute("original_text")).ToLower
					If Not dic.ContainsKey(key) Then
						dic.Add(key, node.GetAttribute("new_text"))
					End If
				End If
			Else 'apply to all loan
				Dim key As String = Common.SafeString(node.GetAttribute("original_text")).ToLower
				If Not dic.ContainsKey(key) Then
					dic.Add(key, node.GetAttribute("new_text"))
				End If
			End If
		Next
		Return dic
	End Function

	Private Function GetHideFieldList(ByVal poCurrentConfig As CWebsiteConfig, ByVal loanTypeName As String, ByVal loanTypeOther As String) As Dictionary(Of String, String)
		Dim oHideFieldNodes As XmlNodeList = poCurrentConfig._webConfigXML.SelectNodes("CUSTOM_LIST/HIDE_FIELDS/ITEM")
		Dim hideFieldList As New Dictionary(Of String, String)
		For Each node As XmlElement In oHideFieldNodes
			Dim sLoanType = Common.SafeString(node.GetAttribute("loan_type")) 'loan_type may be null
			If sLoanType <> "" Then
				Dim arrayLoanType() As String = Common.SafeString(node.GetAttribute("loan_type")).ToLower().Split("|"c)
				If arrayLoanType.Contains(loanTypeName) Or (loanTypeOther <> "" AndAlso arrayLoanType.Contains(loanTypeOther)) Then
					Dim key As String = Common.SafeString(node.GetAttribute("controller_id"))
					If Not hideFieldList.ContainsKey(key) Then
						hideFieldList.Add(key, Common.SafeString(node.GetAttribute("force_value")))
					End If
				End If
			Else
				Dim key As String = Common.SafeString(node.GetAttribute("controller_id"))
				If Not hideFieldList.ContainsKey(key) Then
					hideFieldList.Add(key, Common.SafeString(node.GetAttribute("force_value")))
				End If
			End If

		Next
		Return hideFieldList
	End Function

	'Private Function GetShowFieldList(ByVal poCurrentConfig As CWebsiteConfig, ByVal loanType As String, ByVal loanTypeOther As String) As List(Of String)
	'	Dim oShowFieldNodes As XmlNodeList = poCurrentConfig._webConfigXML.SelectNodes("CUSTOM_LIST/SHOW_FIELDS/ITEM")
	'	Dim showFieldList As New List(Of String)
	'	For Each node As XmlElement In oShowFieldNodes
	'		Dim sLoanType = Common.SafeString(node.GetAttribute("loan_type")) 'loan_type may be null
	'		If sLoanType <> "" Then
	'			Dim arrayLoanType() As String = Common.SafeString(node.GetAttribute("loan_type")).ToLower().Split("|"c)
	'			If arrayLoanType.Contains(loanType) Or (loanTypeOther <> "" AndAlso arrayLoanType.Contains(loanTypeOther)) Then
	'				Dim key As String = Common.SafeString(node.GetAttribute("controller_id"))
	'				showFieldList.Add(key)
	'			End If
	'		Else
	'			Dim key As String = Common.SafeString(node.GetAttribute("controller_id"))
	'			showFieldList.Add(key)
	'		End If

	'	Next
	'	Return showFieldList
	'End Function

	Private Function GetRelocateList(ByVal poCurrentConfig As CWebsiteConfig, ByVal loanTypeName As String, ByVal loanTypeOther As String) As Dictionary(Of String, String)
		Dim oRelocateNodes As XmlNodeList = poCurrentConfig._webConfigXML.SelectNodes("CUSTOM_LIST/RELOCATE/ITEM")
		Dim relocateList As New Dictionary(Of String, String)
		For Each node As XmlElement In oRelocateNodes
			Dim sLoanType = Common.SafeString(node.GetAttribute("loan_type")) 'loan_type may be null
			If sLoanType <> "" Then
				Dim arrayLoanType() As String = Common.SafeString(node.GetAttribute("loan_type")).ToLower().Split("|"c)
				If arrayLoanType.Contains(loanTypeName) Or (loanTypeOther <> "" AndAlso arrayLoanType.Contains(loanTypeOther)) Then
					Dim key As String = Common.SafeString(node.GetAttribute("target_controller_id"))
					Dim value As String = Common.SafeString(node.GetAttribute("top_anchor_controller_id"))
					If Not relocateList.ContainsKey(key) Then
						relocateList.Add(key, value)
					End If
				End If
			Else
				Dim key As String = Common.SafeString(node.GetAttribute("target_controller_id"))
				Dim value As String = Common.SafeString(node.GetAttribute("top_anchor_controller_id"))
				If Not relocateList.ContainsKey(key) Then
					relocateList.Add(key, value)
				End If
			End If
		Next
		Return relocateList
	End Function


	Private Function GetInjectionList(ByVal poCurrentConfig As CWebsiteConfig, ByVal loanTypeName As String, ByVal loanTypeOther As String) As List(Of KeyValuePair(Of String, String))
		Dim oInjectionNodes As XmlNodeList = poCurrentConfig._webConfigXML.SelectNodes("CUSTOM_LIST/INJECTIONS/INJECT_ITEM")
		Dim itemList As New List(Of KeyValuePair(Of String, String))
		For Each node As XmlElement In oInjectionNodes
			Dim sLoanType = Common.SafeString(node.GetAttribute("loan_type")) 'loan_type may be null
			If sLoanType <> "" Then
				Dim arrayLoanType() As String = Common.SafeString(node.GetAttribute("loan_type")).ToLower().Split("|"c)
				If arrayLoanType.Contains(loanTypeName) Or (loanTypeOther <> "" AndAlso arrayLoanType.Contains(loanTypeOther)) Then
					Dim target As String = Common.SafeString(node.GetAttribute("target_controller_id"))
					Dim anchor As String = Common.SafeString(node.GetAttribute("top_anchor_controller_id"))
					Dim content As String = Common.SafeString(node.InnerText)
					itemList.Add(New KeyValuePair(Of String, String)(anchor, content))
				End If
			Else
				Dim target As String = Common.SafeString(node.GetAttribute("target_controller_id"))
				Dim anchor As String = Common.SafeString(node.GetAttribute("top_anchor_controller_id"))
				Dim content As String = Common.SafeString(node.InnerText.Trim())
				itemList.Add(New KeyValuePair(Of String, String)(anchor, content))
			End If

		Next
		Return itemList
	End Function

	Private Function GetAdvancedLogics(ByVal poCurrentConfig As CWebsiteConfig, ByVal loanTypeName As String) As List(Of CAdvancedLogicItem)
		If Regex.IsMatch(loanTypeName, "^(xa|sa|xa_special|sa_special|xa_business|sa_business)$", RegexOptions.IgnoreCase) Then
			loanTypeName = "xa"
		End If
		Dim oAdvancedLogicItemNodes As XmlNodeList = poCurrentConfig._webConfigXML.SelectNodes(String.Format("ADVANCED_LOGICS/ADVANCED_LOGIC_ITEM[@loan_type='{0}']", loanTypeName))
		Dim itemList As New List(Of CAdvancedLogicItem)
		For Each node As XmlElement In oAdvancedLogicItemNodes
			Dim targetItem As String = Common.SafeString(node.GetAttribute("target_item")) 'for now, it's custom question name
			Dim expression As String = Common.SafeString(node.GetAttribute("expression"))
			Dim targetType As String = Common.SafeString(node.GetAttribute("target_type"))
			targetType = IIf(targetType = "CQ", "AQ", targetType)
			Dim conditionNodes As XmlNodeList = node.SelectNodes("CONDITIONS/CONDITION_ITEM")
			Dim conditionList As New List(Of CConditionItem)
			For Each conditionNode As XmlElement In conditionNodes
				Dim conditionItem As New CConditionItem()
				conditionItem.ID = Guid.NewGuid().ToString().Replace("-", "").Substring(0, 10)
				conditionItem.Type = Common.SafeString(conditionNode.GetAttribute("type"))
				conditionItem.Name = Common.SafeString(conditionNode.GetAttribute("name"))
				conditionItem.Comparer = Common.SafeString(conditionNode.GetAttribute("operator"))
				conditionItem.Value = Common.SafeString(conditionNode.GetAttribute("value"))
				'conditionItem.LogicalOperator = ""
				'If conditionNode.HasAttribute("logical_operator") Then
				'	conditionItem.LogicalOperator = Common.SafeString(conditionNode.GetAttribute("logical_operator"))
				'End If
				'conditionItem.LogicalIndent = 0
				'If conditionNode.HasAttribute("indent") Then
				'	conditionItem.LogicalIndent = Common.SafeInteger(conditionNode.GetAttribute("indent"))
				'End If
				conditionList.Add(conditionItem)
			Next
			itemList.Add(New CAdvancedLogicItem() With {.TargetItem = targetItem, .TargetType = targetType, .LoanType = loanTypeName, .Conditions = conditionList, .Expression = expression})
		Next
		Return itemList
	End Function

	Private Function GetAdvancedLogicListsFromConditionedQuestions(poCurrentConfig As CWebsiteConfig, sLoanType As String, sLoanNodeName As String) As List(Of CAdvancedLogicItem)
		Dim itemList As New List(Of CAdvancedLogicItem)
		If sLoanType = "SA" Then sLoanType = "XA"

		' AP-2349 - Read in legacy conditioned questions as advanced logic items
		For Each node As XmlElement In poCurrentConfig._webConfigXML.SelectNodes(String.Format("/WEBSITE/{0}/CUSTOM_CONDITIONED_QUESTIONS/QUESTION", sLoanNodeName))
			itemList.Add(ConvertConditionedQuestionToAdvancedLogicItem(node, sLoanType, QuestionRole.Application, CCustomQuestionNewAPI.getDownloadedXACustomQuestions(poCurrentConfig, False, sLoanType)))
		Next
		For Each node As XmlElement In poCurrentConfig._webConfigXML.SelectNodes(String.Format("/WEBSITE/{0}/CUSTOM_APPLICANT_CONDITIONED_QUESTIONS/QUESTION", sLoanNodeName))
			itemList.Add(ConvertConditionedQuestionToAdvancedLogicItem(node, sLoanType, QuestionRole.Applicant, CCustomQuestionNewAPI.getDownloadedXACustomQuestions(poCurrentConfig, True, sLoanType)))
		Next
		If isComboMode AndAlso Not sLoanType.Equals("XA", StringComparison.OrdinalIgnoreCase) Then
			For Each node As XmlElement In poCurrentConfig._webConfigXML.SelectNodes("/WEBSITE/XA_LOAN/CUSTOM_CONDITIONED_QUESTIONS/QUESTION")
				itemList.Add(ConvertConditionedQuestionToAdvancedLogicItem(node, sLoanType, QuestionRole.Application, CCustomQuestionNewAPI.getDownloadedXACustomQuestions(poCurrentConfig, False, "XA")))
			Next
			For Each node As XmlElement In poCurrentConfig._webConfigXML.SelectNodes("/WEBSITE/XA_LOAN/CUSTOM_APPLICANT_CONDITIONED_QUESTIONS/QUESTION")
				itemList.Add(ConvertConditionedQuestionToAdvancedLogicItem(node, sLoanType, QuestionRole.Applicant, CCustomQuestionNewAPI.getDownloadedXACustomQuestions(poCurrentConfig, True, "XA")))
			Next
		End If

		Return itemList.Where(Function(item) item IsNot Nothing).ToList()
	End Function

	Private Function ConvertConditionedQuestionToAdvancedLogicItem(node As XmlElement, sLoanTypeName As String, eCqRole As QuestionRole, lstCQs As List(Of CCustomQuestionXA)) As CAdvancedLogicItem
		' Todo: Parse manually once CCustomQuestion is deprecated
		Dim oCq = New CCustomQuestion(node)
		If oCq Is Nothing OrElse String.IsNullOrEmpty(oCq.Conditioning) Then Return Nothing

		Dim oCqXa = lstCQs.FirstOrDefault(Function(cq) cq.Identifer = New QuestionIdentifier(oCq.Name, eCqRole))
		If oCqXa Is Nothing Then Return Nothing

		Dim conditions = New List(Of CConditionItem)
		Dim conditionParts() As String = oCq.Conditioning.Split("="c)
		If Not conditionParts.Length = 2 Then Return Nothing

		Dim sConditionCQName As String = conditionParts(0)
		Dim sConditionValues As String = conditionParts(1)
		Dim lstConditionValues() As String = sConditionValues.Split("|"c)
		If lstConditionValues.Length = 0 Then Return Nothing

		Dim lstConditions As New List(Of CConditionItem)
		For Each conditionValue As String In lstConditionValues
			lstConditions.Add(New CConditionItem With {
							  .ID = Guid.NewGuid().ToString().Replace("-", "").Substring(0, 10),
							  .Type = "AQ",
							  .Name = sConditionCQName,
							  .Comparer = "equal",
							  .Value = conditionValue
							  })
		Next

		Return New CAdvancedLogicItem With {
			.TargetItem = oCq.Name,
			.TargetType = "AQ",
			.LoanType = sLoanTypeName,
			.Conditions = lstConditions,
			.Expression = String.Join(" OR ", lstConditions.Select(Function(cond, index) "$exp" + (index + 1).ToString()))
		}
	End Function
End Class
