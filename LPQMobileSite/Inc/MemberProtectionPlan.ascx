﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="MemberProtectionPlan.ascx.vb"
    Inherits="Inc_MemberProtectionPlan" %>
<div class="RoundedSection">
    <div class="SectionTitle">
        <span style="font-weight: bold;">Member Protection Plan</span>
    </div>
    <div style="margin: 10px 0;">
        <div style="float: left; width: 60%;">
            Are you interested in
            <%=LenderName%>
            Payment Protection for your loan?
        </div>
        <div style="float: right; width: 35%;">
            <fieldset data-role="controlgroup" data-mini="true">
                <input type="radio" name="rdProtectionForYourLoan" id="rdProtectionForYourLoanYes"
                    value="N" />
                <label for="rdProtectionForYourLoanYes">
                    No</label>
                <input type="radio" name="rdProtectionForYourLoan" id="rdProtectionForYourLoanNo"
                    value="Y" />
                <label for="rdProtectionForYourLoanNo">
                    Yes</label>
            </fieldset>
        </div>
        <div style="clear: both">
        </div>
    </div>
</div>

<script type="text/javascript">
    function ValidateMemberProtectionPlan(strMessage) {
        if (!Common.ValidateRadioGroup('rdProtectionForYourLoan'))
            strMessage += 'Select a Member Protection Plan option<br />';
    }

    function SetMemberProtectionPlan(appInfo) {
        appInfo.MemberProtectionPlan = '';
        if ($("input[name='rdProtectionForYourLoan']:checked").length > 0) {
            appInfo.MemberProtectionPlan = $("input[name='rdProtectionForYourLoan']:checked").val();
        }
    }
</script>

