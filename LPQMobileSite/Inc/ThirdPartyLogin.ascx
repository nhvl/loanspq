﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="ThirdPartyLogin.ascx.vb" Inherits="Inc_ThirdPartyLogin" %>

<!-- Facebook Integration -->
<script>
    var coPrefix = '<%=IDPrefix%>';

    function statusChangeCallback(response) {
        if (response.status === 'connected') {
            getLoggedInUserData();
        }
    }

    function checkLoginState() {
        FB.getLoginStatus(function (response) {
            statusChangeCallback(response);
        });
    }

	//disable facebook bc it only providfe email and nothing else
    //window.fbAsyncInit = function () {
    //    FB.init({
    //        appId: '290722327608457',
    //        cookie: true,
    //        xfbml: true,
    //        version: 'v2.2'
    //    });

    //    FB.getLoginStatus(function (response) {
    //        statusChangeCallback(response);
    //    });
    //};

    //(function (d, s, id) {
    //    var js, fjs = d.getElementsByTagName(s)[0];
    //    if (d.getElementById(id)) return;
    //    js = d.createElement(s); js.id = id;
    //    js.src = "//connect.facebook.net/en_US/sdk.js";
    //    fjs.parentNode.insertBefore(js, fjs);
    //}(document, 'script', 'facebook-jssdk'));

    function getLoggedInUserData() {
        FB.api('/me', function (response) {
            document.getElementById('hd_fb_first_name').value = response.first_name;
            document.getElementById('hd_fb_last_name').value = response.last_name;
            document.getElementById('hd_fb_full_name').value = response.name;
            document.getElementById('hd_fb_gender').value = response.gender;
            document.getElementById('hd_fb_email').value = response.email;

            //BindLoggedInFbData(response);
        });
    }
    
    function CheckFbLoginStatus() {
        FB.getLoginStatus(function (response) {
            statusChangeCallback(response);
        });
    }
    
    function logoutFacebook() {
        FB.logout(function (response) {
            statusChangeCallback(response);
        });
    }
</script>

<div>
    <div style="float: left; margin: 5px"><script type="in/Login"></script></div>
    <div id="btn_fb_login" style="float: left; margin: 5px 5px 5px 10px; display: none" class="fb-login-button" scope="public_profile,email,user_friends" data-max-rows="1" data-size="small" data-show-faces="false" data-auto-logout-link="false" onlogin="CheckFbLoginStatus()"></div>
    <div style="clear: both"></div>
</div>
<input type="hidden" id="hd_fb_first_name" />
<input type="hidden" id="hd_fb_last_name" />
<input type="hidden" id="hd_fb_full_name" />
<input type="hidden" id="hd_fb_gender" />
<input type="hidden" id="hd_fb_email" />

<input type="hidden" id="hd_linkedin_firstname" />
<input type="hidden" id="hd_linkedin_lastname" />
<input type="hidden" id="hd_linkedin_formattedname" />
<input type="hidden" id="hd_linkedin_email" />
<input type="hidden" id="hd_linkedin_title" />
<input type="hidden" id="hd_linkedin_companyname" />
<input type="hidden" id="hd_linkedin_country" />