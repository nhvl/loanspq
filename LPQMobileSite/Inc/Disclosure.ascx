﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="Disclosure.ascx.vb" Inherits="Inc_Disclosure" %>
<style>
    /*verify code*/
.divVerifyCode
{
    width:100%;
    max-width :400px;
    position:relative;
    margin:auto 0px;
}
.divVerifyCodeRight
{
    width:40%;
    float:right;
}
.divVerifyCodeLeft
{
    width:60%;
    float:left;
}
.divVerifyCodeRight_style
{
    width:100%;
    margin-left:0px;
    margin-top:2px;
}
.divVerifyCodeLeft_style
{
    margin-right:0px;
    width:100%;
}
.divVerifyCodeRight_style .ui-btn
{
    height :45px;
    border-radius :4px;
    padding: 8px;
    margin:8px auto;

}
.divVerifyCodeRight_style .ui-btn:hover, .divVerifyCodeRight_style .ui-btn:focus
{   
    color :black;   
}
.VerifyCodeMessage
{
  background-color:lightcyan;
  font-weight:normal;
  padding:6px 6px;
  margin:0px 4px 4px 4px;
  border-radius :5px;
  max-width:400px;
}              
/*end verify code*/
</style>
 
<%=strRenderingVerifyCode%>

<%If EnableEmailMeButton AndAlso DisclosureLinks IsNot Nothing AndAlso DisclosureLinks.Any() Then%>
	<div id="divEmailMeButton<%=IIf(LoanType = "HELOC", Name, "") %>" <%=IIf(IsInMode("777") AndAlso IsInFeature("visibility"), "class=""showfield-section"" data-show-field-section-id=""" & BuildShowFieldSectionID("divEmailMeButton" & IIf(LoanType = "HELOC", Name, "") & "") & """ data-default-state=""off"" data-section-name='Email Me Link Button " & IIf(LoanType = "HELOC", "(LOC)", "") & "' ", "")%>>
		<div style="clear: both; padding: 0;" id="divEmailMeLink<%=IIf(LoanType = "HELOC", Name, "") %>" >
			<a href="javascript:void(0);" class="ui-link header_theme2 shadow-btn chevron-circle-right-before" tabindex="0" style="font-size: 18px; font-weight :bold;" data-mode="self-handle-event" id="btnEmailMe<%=Name%>">Email me all below disclosure link(s)</a>
		</div>
	</div>
<%End If%>

<div style="margin-top: 20px;" id="divDisclosureContent" section-name="disclosure" class="disclosure-section" runat="server">
    
	<div class="row">
		<asp:Repeater runat="server" ID="rptDisclosureByButton">
			<ItemTemplate>
				<div class="col-md-4 col-sm-6 col-xs-12">
				<a <%#Eval("Value")%> data-role="button" class="btn-header-theme" style="padding: 0px 10px 0px 10px; text-align: center; height: 80px; line-height: 80px; white-space: normal;" ><%#Eval("Key")%></a>
				</div>
			</ItemTemplate>    
		</asp:Repeater>			
    </div>
    <asp:Repeater runat="server" ID="rptDisclosureByCheck">

        <ItemTemplate>
            <label>
                <input title="disclosure" type="checkbox" data-disabled="<%#disabledClass(Container.ItemIndex)%>"><%#Container.DataItem%>
            </label>
        </ItemTemplate>
    </asp:Repeater>
    <div class="error-placeholder"></div>
</div>
<div id="popupEmailMe<%=Name%>" data-role="popup" data-position-to="window" data-dismissible="true" data-history="false" style="max-width: 500px;">
	<div data-role="header">
        <button class="header-hidden-btn">.</button>
        <div data-place-holder="heading" style="padding: 20px 0px 20px 0px;font-size: 22px; color: #fff; font-weight: 400; margin: 0 80px 0 20px">Email Agreements & Disclosures</div>
        <div tabindex="0" data-command="close" class="header_theme2 ui-btn-right" data-role="none">
            <%=HeaderUtils.IconClose %><span style="display: none;">close</span>
        </div>
    </div>
	<div data-role="content">
		<div class="row" style="padding: 15px 0;">
			<div class="col-sm-12">
				<div data-role="fieldcontain">
                    <label for="txtEmailMe<%=Name%>">Your E-mail Address</label>
                    <input id="txtEmailMe<%=Name%>" type="text"/>
                </div>
			</div>
			<div class="col-sm-12" style="margin-top: 20px;">The links to Agreements & Disclosures will be sent to your e-mail account so you may view/print later.</div>
		</div>
		<div class="row text-center">
			<div class="col-xs-12 text-center" style="margin-bottom: 20px;">
				<a onclick="sendEmailDisclosures<%=Name%>()" data-role="button" type="button">Send</a>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
	function DisclosureItem(name, link) {
		this.Name = name;
		this.Link = link;
	}
    $(function () {

        $('#btnVerifyCode').click(function () {
            verifyCode();
        });

        $("#btnEmailMe<%=Name%>").on("click", function (evt) {
            //prevent email popup when clicking edit label field
            if ($(this).attr("contenteditable") == "true") return false;
	    	openPopup("#popupEmailMe<%=Name%>");
		    evt.stopPropagation();
	    });
	    
    	$("#popupEmailMe<%=Name%> div[data-command='close']").on("click", function () {
    		$("#popupEmailMe<%=Name%>").popup("close");
	    });
        $('#btnVerifyCodeHELOC').click(function () {      
            verifyCode("HELOC"); //for home equity line of credit
        });
        
        $('#btnVerifyCode').css('color', 'white');
        //handleDisclosureClick();
        $("div[name='<%=Name%>']").observer({
            validators: [
                function (partial) {
                    if (ValidateDisclosures("<%=Name%>") === false) {
                        return "Please review and check all disclosures.";
                    }
                    return "";
                }
            ],
            validateOnBlur: false,
            group: "ValidateDisclosure_<%=Name%>",
            groupType: "complexUI",
            placeHolder: $("div[name='<%=Name%>']").find("div.error-placeholder")
        });
    	$(document).on("change blur disclosureChange", $("div[name='<%=Name%>']"), function (evt) {
    		if (ValidateDisclosures("<%=Name%>") === true) {
			    $.lpqValidate.hideValidation($("div[name='<%=Name%>']").find("div.error-placeholder"));
    		} else if (evt.type == "disclosureChange") {
    			$.lpqValidate.showValidation($("div[name='<%=Name%>']").find("div.error-placeholder"), "Please review and check all disclosures.");
		    }
    	});

    	$("div[name='<%=Name%>'] input:checkbox[data-disabled='true']").each(function (idx, ele) {
    		$(ele).closest("label").on("click", function() {
			    var $nextChk = $(this).next("input:checkbox");
			    if ($nextChk.data("disabled") == true && $nextChk.is(":checked") == false) {
			    	$.lpqValidate.showValidation($("div[name='<%=Name%>']").find("div.error-placeholder"), "Please open and review any links before checking the disclosure");
			    }
		    });
	    });
    	
    	$("#popupEmailMe<%=Name%>").on("popupafteropen", function (event, ui) {
    		$("#" + this.id + "-screen").height("");
    		if ($("#txtEmail").length === 1 && $("#txtEmailMe<%=Name%>").val() === "") {
    			$("#txtEmailMe<%=Name%>").val($("#txtEmail").val());
			}
    	});
    	$("#popupEmailMe<%=Name%>").on("popupafterclose", function (event, ui) {
    		if ($("#txtEmailMe<%=Name%>").val() === $("#txtEmail").val()) {
    			$("#txtEmailMe<%=Name%>").val("");
		    }
		});
    	$("#txtEmailMe<%=Name%>").observer({
    		validators: [
				function (partial) {
					if (Common.ValidateEmail($(this).val()) == false) {
						return "Please enter valid email";
					}
					return "";
				}
    		],
    		validateOnBlur: true,
    		group: "ValidateEmailDisclosures<%=Name%>"
        });
    });
	<%--function handleDisclosureClick() {
        $("div[name='<%=Name%>'] .ui-checkbox, div[name='<%=Name%>'] .btn-header-theme").each(function (idx, ele) {
            $(ele).click(function() {
                $(ele).closest("div[section-name='disclosure']").trigger("change");
            });
        });
    }--%>

	function sendEmailDisclosures<%=Name%>() {
		if ($.lpqValidate("ValidateEmailDisclosures<%=Name%>") === false) return;
		<%--var disclosureArr = [];

		//links from btn an non-btn
	    $("a", $("div[section-name='disclosure'][name='<%=Name%>']")).each(function(idx, ele) {
	    	var $ele = $(ele);

			//assume only disclosure link has an actual reference
	    	if ($ele.attr("href").length > 30) {
	    		var item = new DisclosureItem($ele.text(), $ele.attr("href"));
	    		disclosureArr.push(item);
	    	}
	    });--%>
        $.ajax({
            url: '/handler/chandler.aspx?lenderref='+ encodeURIComponent(Common.GetParameterByName("lenderref"))+'&type=' + encodeURIComponent(Common.GetParameterByName('type')),
            async: true,
		    cache: false,
		    type: 'POST',
		    dataType: 'html',
		    data: {
			    command: 'SendEmailDisclosures',
			    email: $("#txtEmailMe<%=Name%>").val(),
                loantype: '<%=LoanType%>'
			    //data: JSON.stringify(disclosureArr)
		    },
		    success: function (responseText) {
			    var response = $.parseJSON(responseText);
			    if (response.IsSuccess) {
			    	$("#popupEmailMe<%=Name%>").popup("close");
			    } else {
			    	$.lpqValidate.showValidation("#txtEmailMe<%=Name%>", response.Message);
			    }

		    }
	    });
    }

    function handleDisableDisclosures(element) {
            var currElement = $(element);
            var isDataDisabled = (currElement.next('input').attr('data-disabled') == 'true');
            if (!isDataDisabled && currElement.find('a').hasClass('DisclosureDisable')) {
                toggleCheckBox(currElement);
            }     
    }
    function verifyCode(hePrefix) {
        if (hePrefix == undefined) {
            hePrefix = '';
        }
        if ($('#divVerifyCode'+hePrefix).length == 0) {
            return strErrorMessage;
        }
        var verifyCode = $('#hdVerifyCode' + hePrefix).val();
        if (verifyCode != "") {
            var txtVerifyCode = $('#txtVerifyCode' + hePrefix);
            var verifyCodeMessage = $('#verifyCodeMessage' + hePrefix);
            var txtInputValue = txtVerifyCode.val();
            verifyCodeMessage.addClass('VerifyCodeMessage');
            $('.VerifyCodeMessage').show();
            var txtVerifyCodeMessage = "";
            if (verifyCode == txtInputValue) {
                txtVerifyCodeMessage = "The code you entered is correct. Thank you.";
                txtVerifyCode.removeClass('verify_code_n');
            } else {
                if (txtInputValue.trim() == "") {
                    txtVerifyCodeMessage = "Please enter the code.";
                } else {
                    txtVerifyCodeMessage = "The code you entered is not correct. Please try again.";
                }
                txtVerifyCode.addClass('verify_code_n');
            }
            verifyCodeMessage.text(txtVerifyCodeMessage);
        }
    }
    function validateVerifyCode(hePrefix) {
        if (hePrefix == undefined) {
            hePrefix = '';
        }
        var strErrorMessage = '';
        if ($('#divVerifyCode' + hePrefix).length == 0) {
            return strErrorMessage;
        }
        var verifyCodeElement = $('#txtVerifyCode' + hePrefix);
        //validate visible reasonable demo code pdf view
         if(verifyCodeElement.is(':visible')){
            if (verifyCodeElement.hasClass('verify_code_n')) {
                strErrorMessage = 'Please complete verify code field.<br/>';
            }
        }
        return strErrorMessage;
    }
    function toggleCheckBox(element) {
        if (element.hasClass('ui-checkbox-off')) {
            element.removeClass('ui-checkbox-off').addClass('ui-checkbox-on');
        } else {
            element.removeClass('ui-checkbox-on').addClass('ui-checkbox-off');
        }
    }
	function autofill_Disclosure() {
		$(".disclosure-section input:checkbox").each(function(idx, chk) {
			$(chk).prop("checked", true).checkboxradio().checkboxradio("refresh");
		});
		$(".disclosure-section a[data-role='button'].btn-header-theme").each(function (idx, btn) {
			$(btn).addClass("active").addClass("btnActive_on");
		});
	}
	function ValidateDisclosures(containerName) {
		var isCheckedAll = true;
		$("div[name='" + containerName + "'] .ui-checkbox").each(function () {
			if ($(this).children('input:checkbox').is(":checked") == false) {
				isCheckedAll = false;
				return false; //break the loop
			}
		});
		$("div[name='" + containerName + "'] .btn-header-theme").each(function() {
			if ($(this).hasClass("active") == false) {
				isCheckedAll = false;
				return false;
			}
		});
		return isCheckedAll;
	}
</script>