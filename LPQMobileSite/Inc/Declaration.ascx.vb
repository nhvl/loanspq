﻿Imports LPQMobile.Utils
Partial Class Inc_Declaration
	Inherits CBaseUserControl

	Public Property DeclarationList As Dictionary(Of String, String)

	Protected Function GetAdditionalQuestions(declarationName As String) As List(Of Tuple(Of String, String, String, Boolean))
		Dim result As New List(Of Tuple(Of String, String, String, Boolean))
		If declarationName = "has_co_maker" Then
			result.Add(New Tuple(Of String, String, String, Boolean)("co_maker_primary_name", "Primary Name", "", True))
			result.Add(New Tuple(Of String, String, String, Boolean)("co_maker_creditor", "Creditor Name", "", True))
			result.Add(New Tuple(Of String, String, String, Boolean)("co_maker_amount", "Amount", "money", False))
		ElseIf declarationName = "has_alias" Then
			result.Add(New Tuple(Of String, String, String, Boolean)("alias", "Other Name", "", True))
		ElseIf declarationName = "has_alimony" Then
			result.Add(New Tuple(Of String, String, String, Boolean)("alimony_recipient", "Recipient Name", "", True))
			result.Add(New Tuple(Of String, String, String, Boolean)("alimony_recipient_address", "Recipient Address", "", True))
			result.Add(New Tuple(Of String, String, String, Boolean)("alimony", "Alimony", "money", False))
			result.Add(New Tuple(Of String, String, String, Boolean)("separate_maintenance_expense", "Separate Maintenance", "money", False))
			result.Add(New Tuple(Of String, String, String, Boolean)("child_support", "Child Support", "money", False))
		End If
		Return result
	End Function
End Class
