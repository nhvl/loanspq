﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="NewDocumentScan.ascx.vb" Inherits="Inc_NewDocumentScan" %>

<%@ Import Namespace="System.Web.Optimization" %>
<%@ Register TagPrefix="uc" TagName="ThirdPartyLogin" Src="~/Inc/ThirdPartyLogin.ascx" %>
   
<div class="divScanDocs ui-grid-a legacy-scan">  
    <div class="ui-block-a">
        <div class="scanDocsContentLeft" id="<%=IDPrefix%>DcFile">
         <%=DocScanMessage%>
         <div class ="labeltext-bold" > 
			 <br />               
            <p>Recommendation for best recognition:</p>
             <div class="scanDocsContentLeft-style">
             <p>* Use a dark background</p>
             <p>* Make sure all four corners are visible</p>
             <p>* Avoid glare</p>
              <p>* Make sure image is in focus</p>
           </div>
        </div>  
        </div>     
    </div>  
    <div class="ui-block-b">
        <!--start divScanDriver license -->
        <div id="<%=IDPrefix%>divScanDriverLicense" class="scanDocsContentRight">
            <!--back driver license image -->
            <div id="<%=IDPrefix%>divBackImage"> 
                <div class="fileupload fileupload-new" id="<%=IDPrefix%>fileupload-backImage" data-provides="fileupload">
                    <input type="hidden" value="">
                            <%--<div class="title-style">Back of your driver license</div>--%>
                    <!--1st step , image to overlay and invoke id="input-image" for capture or upload file-->
                    <div class="fileupload-new img-responsive" style='max-height:282px'>
						<input type="image" class="img-responsive img-thumbnail" id="<%=IDPrefix%>backImagePlaceHolder" alt="thumbnail" src="<%=DLScanImage%>" style="max-height :210px"/>
							<div id="<%=IDPrefix%>backImage-thumbnail" class="fileupload-preview fileupload-exists thumbnail"></div>        
						<div style="display: none;">
							<!--display browse button for upload or capture image-->                    
						<input aria-labelledby="<%=IDPrefix%>DcFile" name="driversLicenseImage" id="<%=IDPrefix%>inputBackImage" type="file" capture="camera" accept="image/*"></div>    
					</div>
					<div class="fileupload-new img-responsive desktop-capture hidden" style='max-height:282px'>
						<img class="img-responsive img-thumbnail" alt="thumbnail" src="<%=DLScanImage%>" style="max-height :210px"/>
						<div class="fileupload-preview fileupload-exists thumbnail"></div>
						<div class="capture-stream">
							<video autoplay=""></video>
							<canvas class="hidden"></canvas>
							<img/>
							<button class="close-btn">Back</button>
							<div class="control-btns">								
								<button class="cancel-btn">Cancel</button>
								<button class="continue-btn">Continue</button>
							</div>
						</div>
					</div>     
                <div id="<%=IDPrefix%>divBackMessage" style="color:blue"></div> 
            </div> <!-- end back driver licence -->
			            
			<div id="<%=IDPrefix%>scanResult" style="display: none;"></div>
        </div> <!--end divScanDriver license -->
    </div>
</div>  <!--end divDocsContents -->
<div style="float: left; margin-bottom: 20px;">
    <div id="div_linkedin_login" style="display:none">
		<br />
        <span class="bold">Choose LinkedIn profile to prefill employment information. Click button below</span>
        <uc:ThirdPartyLogin runat="server" />
    </div>
    <div id="div_linkedin_name" style="text-align: left;display:none">
        <img src="/images/linkedin_logo.png" alt="LinkedIn Logo" style="width: 93px; height: 21px;"/>
        You're logged in as <a id="anchor_linkedin_name" href="#">.</a>
    </div>
</div>
</div>