﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="CustomQuestions.ascx.vb"
    Inherits="Inc_CustomQuestions" %>
<%@ Import Namespace="Newtonsoft.Json" %>
<div section-name="custom-questions" id="<%=_divCustomQuestionId%>">
    <% =_CustomQuestionHiddenInputs%>
    <%=_CustomQuestionsHML%>
    <%If LoanType = "HE" Then%>
        <input type="hidden" id="hdCQHtmlHE" value='<%=_strCQHtmlHE%>' />
    <%ElseIf LoanType = "HELOC" Then%>
        <input type="hidden" id="hdCQHtmlHELOC" value ='<%=_strCQHtmlHELOC%>' />
    <%End If %>
</div>

<script type="text/javascript">
      if (typeof isSpecialAccount == "function" && isSpecialAccount()){ //special account
        ///show and hide minor custom questions
        var minorAccountTypes =<%=Newtonsoft.Json.JsonConvert.SerializeObject(_minorAccountTypes)%>;
        var xaMinorCQ={};
        xaMinorCQ={
            getCustomQuestionNames: function(mAccount){
                var mQuestionNames =[];
                if (mAccount==undefined || mAccount==""){
                    return mQuestionNames;
                }
                for (var key in minorAccountTypes){
                    //convert minor account types to upper case
                    var mAccountTypes = minorAccountTypes[key].map(function(type){return type.toUpperCase()});
                    if($.inArray(mAccount.toUpperCase(),mAccountTypes)>-1){
                        mQuestionNames.push(key);
                    }
                }
                return mQuestionNames;
            },
            showSelectedCustomQuestions:function(){
                //hide all custom questions
                var $divCQ = $("#div_custom_questions > div[qname]");
                $divCQ.each(function (){
                    xaMinorCQ.hideCustomQuestion($(this).attr('qname'));            
                });

                var $btnAccountType = $('a#ddlSpecialAccountType');
                var mQuestionNames=[];
                var mAccountType ="";
                if($btnAccountType.length>0){
                    if($btnAccountType.hasClass('active')){
                        mAccountType =$btnAccountType.text();
                        mQuestionNames =  xaMinorCQ.getCustomQuestionNames(mAccountType);
                        xaMinorCQ.showAndHideCustomQuestions(mQuestionNames);
                    }
                }else{
                    //dropdown special account type
                    $('#btnSelectionContainer').on('change', 'select#ddlSpecialAccountType', function () {
                        mAccountType = $(this).find('option:selected').text();  
                        mQuestionNames = xaMinorCQ.getCustomQuestionNames(mAccountType);
                        xaMinorCQ.showAndHideCustomQuestions(mQuestionNames);
                    });
                }    
            },
            showAndHideCustomQuestions:function(QuestionNames){            
                var $divCQ = $("#div_custom_questions > div[qname]");  
                $divCQ.each(function(){
                    var $currentDiv =$(this);         
                    var cqName= $currentDiv.attr('qname');
                    var $cqText=$currentDiv.text();     
                    if($.inArray(cqName,QuestionNames)>-1){
                        //update index question 
                       // var sText="<span class="bold">"+(QuestionNames.indexOf(cqName)+1).toString()+ $currentDiv.html().substring($currentDiv.html().indexOf('.'));
                       // $currentDiv.html(sText);
                        xaMinorCQ.showCustomQuestion(cqName);                         
                    }else{
                        xaMinorCQ.hideCustomQuestion(cqName);  
                    }
                });       
            },
            hideCustomQuestion:function(questionName){
                var inputElement = $("#div_custom_questions input[iname='"+questionName+"']");
                var selectElement= $("#div_custom_questions select[iname='"+questionName+"']");
                //hide title
                $("#div_custom_questions div[qname='"+questionName+"']").addClass('hidden');         
                if (inputElement){
                    if(inputElement.attr("type")=="checkbox"){
                        inputElement.closest("div[class~='chkgrp']").addClass("hidden");
                    }else{
                        inputElement.addClass("hidden");
                        //re-enter password field
                        var psElement = $("#div_custom_questions input[iname='re_"+questionName+"']");
                        if(inputElement.attr("type")=="password" && psElement){          
                            $("#"+psElement.attr("aria-labelledby")).addClass('hidden'); 
                            psElement .addClass("hidden");
                        }
                        
                        $('#reviewpage').on('pageshow',function(){
                            inputElement.parent("div[class~='ui-input-text']").addClass("hidden");
                            psElement.parent("div[class~='ui-input-text']").addClass("hidden");
                        });
                    }
                }
                if(selectElement){
                    selectElement.addClass("hidden");
                }
            },
            showCustomQuestion:function(questionName){
                var inputElement = $("#div_custom_questions input[iname='"+questionName+"']");
                var selectElement= $("#div_custom_questions select[iname='"+questionName+"']");
               $("#div_custom_questions div[qname='"+questionName+"']").removeClass('hidden');
                if (inputElement){
                    if(inputElement.attr("type")=="checkbox"){
                        inputElement.closest("div[class~='chkgrp']").removeClass("hidden");
                    }else{
                            
                        inputElement.removeClass("hidden");
                        //re-enter password field
                        var psElement = $("#div_custom_questions input[iname='re_"+questionName+"']");
                        if(inputElement.attr("type")=="password"){          
                            $("#"+psElement.attr("aria-labelledby")).removeClass('hidden'); 
                            psElement.removeClass("hidden");
                        }
                        $('#reviewpage').on('pageshow',function(){
                            inputElement.parent("div[class~='ui-input-text']").removeClass("hidden");
                            psElement.parent("div[class~='ui-input-text']").removeClass("hidden");
                        }); 
                    }
                }
                if(selectElement){
                    selectElement.removeClass("hidden");
                }
            }
        };
        $(function(){ 
            xaMinorCQ.showSelectedCustomQuestions();     
        });
    }//end special account
   
    $(document).ready(function() {
        loadConditionedQuestions();   
    });

        function loadConditionedQuestions() {
            $('input.CustomQuestion').each(function() {
                checkConditionedQuestions(this);
            });
            $('select.CustomQuestion').each(function() {
                checkConditionedQuestions(this);
            });
            //primary,joint,and minor applicant conditioned questions
            var apPrefix = "ap_";
            var co_apPrefix = "co_ap_";
            var minor_apPrefix = "m_ap_";

            // primary applicant questions
            $('input.ApplicantQuestion').each(function () {
                checkApplicantConditionedQuestionXAs(this, apPrefix);
            });
            $('select.ApplicantQuestion').each(function () {
                checkApplicantConditionedQuestionXAs(this, apPrefix);
            }); //end primary

            //joint applicant question
            $('input.co_ApplicantQuestion').each(function () {
                checkApplicantConditionedQuestionXAs(this, co_apPrefix);
            });
            $('select.co_ApplicantQuestion').each(function () {
                checkApplicantConditionedQuestionXAs(this, co_apPrefix);
            }); //end joint 

            //minor applicant question
            $('input.m_ApplicantQuestion').each(function () {
                checkApplicantConditionedQuestionXAs(this, minor_apPrefix);
            });
            $('select.m_ApplicantQuestion').each(function () {
                checkApplicantConditionedQuestionXAs(this, minor_apPrefix);
            }); // end minor
        }

        function checkApplicantConditionedQuestionXAs(obj, aqPrefix)
        {
            var custom_iname = $(obj).attr('iname');
            var custom_value = $(obj).val();
            //custom_value is not correct value for checkbox(value of checkbox is "on" or "off")
            if ($(obj).attr('type') == 'checkbox' && $(obj).is(':checked')) {
                custom_value = $(obj).attr('answer'); 
            }
            if (aqPrefix == "ap_") {//for primary
                // Find for input (textbox, checkbox, radio)
                $('#divApplicantQuestion input').each(function () {
                    checkConditonedQuestionForELement(this, custom_iname, custom_value, aqPrefix);
                });
                // Find for dropdownlist
                $('#divApplicantQuestion select').each(function () {
                    checkConditonedQuestionForELement(this, custom_iname, custom_value, aqPrefix);
                });
            } else if (aqPrefix == "co_ap_") { //for joint
                // Find for input (textbox, checkbox, radio)
                $('#co_divApplicantQuestion input').each(function () {
                    checkConditonedQuestionForELement(this, custom_iname, custom_value, aqPrefix);
                });
                // Find for dropdownlist
                $('#co_divApplicantQuestion select').each(function () {
                    checkConditonedQuestionForELement(this, custom_iname, custom_value, aqPrefix);
                });
            } else if (aqPrefix == "m_ap_") { //for minor
                // Find for input (textbox, checkbox, radio)
                $('#divMinorApplicantQuestion input').each(function () {
                    checkConditonedQuestionForELement(this, custom_iname, custom_value, aqPrefix);
                });
                // Find for dropdownlist
                $('#divMinorApplicantQuestion select').each(function () {
                    checkConditonedQuestionForELement(this, custom_iname, custom_value, aqPrefix);
                });
            }
        }
        function checkConditionedQuestions(obj) {
            var custom_iname = $(obj).attr('iname');
            var custom_value = $(obj).val();
            //custom_value is not correct value for checkbox(value of checkbox is "on" or "off")
            if ($(obj).attr('type') == 'checkbox' && $(obj).is(':checked')) {
                custom_value = $(obj).attr('answer');
            }
            // Find for input (textbox, checkbox, radio)
            $('#div_custom_questions input').each(function () {
                checkConditonedQuestionForELement(this, custom_iname, custom_value);
            });
            // Find for dropdownlist
            $('#div_custom_questions select').each(function() {
                checkConditonedQuestionForELement(this, custom_iname, custom_value);
            });
            // Find for input (textbox, checkbox, radio)
            $('#div_custom_questions_HE input').each(function () {
                checkConditonedQuestionForELement(this, custom_iname, custom_value);
            });
            // Find for dropdownlist
            $('#div_custom_questions_HE select').each(function () {
                checkConditonedQuestionForELement(this, custom_iname, custom_value);
            });
            // Find for input (textbox, checkbox, radio)
            $('#div_custom_questions_HELOC input').each(function () {
                checkConditonedQuestionForELement(this, custom_iname, custom_value);
            });
            // Find for dropdownlist
            $('#div_custom_questions_HELOC select').each(function () {
                checkConditonedQuestionForELement(this, custom_iname, custom_value);
            });
        }

        function checkConditonedQuestionForELement(element, custom_iname, custom_value, aqPrefix) {
            aqPrefix = typeof aqPrefix !== 'undefined' ? aqPrefix : "";
            var conditioning = $(element).attr('conditioning');
      
            if (conditioning != undefined) {
                //custom_value from user input could be case incensitive. 
                //then convert it back to exact value from conditioning attribute
                if (custom_value != undefined) {
                    var conEleValue = conditioning.split('=')[1];
                    if (conEleValue != undefined && conEleValue.indexOf('|') == -1 &&conEleValue.toUpperCase() == custom_value.toUpperCase()){       
                        custom_value = conEleValue;                                                             
                    }
                }

                var combine_to_conditioning = custom_iname + '=' + custom_value;
                var con_iname = conditioning.split('=')[0];
                var con_value = $(element).val();
                var parent_id = aqPrefix + 'div_' + $(element).attr('con_iname');
                var is_show_children = false;
                if (con_iname == custom_iname) {
                    if (conditioning == combine_to_conditioning) {
                        $('#' + parent_id).show();
                        is_show_children = true;
                    } else {
                        var strConValue = conditioning.split('=')[1];
                        if (conditioning.indexOf("|") != -1) { //multiple selections has the same condition question 
                            var conValueArray = strConValue.split('|');
                            var strConditioning = "";
                            for (var i = 0; i < conValueArray.length; i++) {
                                strConditioning =con_iname+'='+conValueArray[i];
                                if (combine_to_conditioning == strConditioning) {
                                    $('#' + parent_id).show();
                                    is_show_children = true;
                                    break;
                                } else {
                                    $('#' + parent_id).hide();
                                    is_show_children = false;
                                }
                            }
                        } else {
                       
                            if (!isCheckedConditionQuestion(parent_id)) {
                                $('#' + parent_id).hide();
                                is_show_children = false;
                            } else {
                                $('#' + parent_id).show();
                                is_show_children = true;
                            }
                        }
                    }
                    var parent_value = $(element).val();
                    if ($(element).attr('type') == 'checkbox' && $(element).is(':checked')) {
                        parent_value = $(element).attr('answer');
                    }
                    checkConditionedQuestionChildren($(element).attr('iname'),parent_value, is_show_children, aqPrefix);
                }
            } 
        }
        function checkConditionedQuestionChildren(parent_iname, parent_value, is_show_children, aqPrefix) {
            $('.' + aqPrefix + 'ConditionedQuestion:not(span)').each(function () {
                var child_conditioning = $(this).attr('conditioning');
                var child_con_iname = child_conditioning.split('=')[0];
                var child_con_value = child_conditioning.split('=')[1];
                var parent_id = aqPrefix + 'div_' + $(this).attr('con_iname');

                if ($.trim(child_con_iname) == $.trim(parent_iname)) {
                    if (child_con_value != undefined && child_con_value.indexOf('|') != -1) {//multiple selections has the same condition question 
                        if (is_show_children == false || child_con_value.indexOf(parent_value)==-1) {
                            $('#' + parent_id).hide();
                        }else {
                            $('#' + parent_id).show();
                        }
                    } else {
                        if (is_show_children == false || parent_value != child_con_value) {
                            $('#' + parent_id).hide();        
                        }
                        else {
                            $('#' + parent_id).show();
                        }
                    }
                }
            });
        }
        function isCheckedConditionQuestion(parent_id) {
            var isChecked = false;
            var conditioning = $('#' + parent_id + ' input').attr('conditioning');
            if (conditioning == undefined && $('#' + parent_id + ' select').length > 0) {
                conditioning = $('#' + parent_id + ' select').attr('conditioning');
            }  
            var divCustomElement = $('#'+getDivCustomQuestionID()+' input');
            divCustomElement.each(function () {
                var element = $(this);
                if (element.attr('type') == 'checkbox') {
                    var chk_value = element.attr('answer');
                    var chkConditioning = element.attr('iname')+"="+chk_value;
                    if (element.is(':checked') && conditioning != undefined && conditioning==chkConditioning) {
                        var con_value = conditioning.split('=')[1];
                        if (con_value == chk_value) {
                            isChecked = true;
                            return false; // break the loop
                        }
                    }                
                }
            });

            return isChecked;
        }
        function getDivCustomQuestionID() {
            var divCustomQuestionName = "div_custom_questions";
            var divCQElem_HE = $('#div_custom_questions_HE');
            var divCQElem_HELOC = $('#div_custom_questions_HELOC');
            //HE and HELOC case
            if (divCQElem_HE.length > 0 && divCQElem_HELOC.length > 0) {
                if ($('#div_custom_questions_HE').hasClass('HiddenElement')) {
                    divCustomQuestionName="div_custom_questions_HELOC";
                } else {
                    divCustomQuestionName = "div_custom_questions_HE";
                }
            }
            return divCustomQuestionName;
        }
</script>

<script type="text/javascript">
 
    function validateRegExpCustomQuestion()
    {
        var errorMsg = "";
        var customQuestionElem = $('#' + getDivCustomQuestionID() + ' input.CustomQuestion');
        customQuestionElem.each(function () {
            var element = $(this);
            var questionName = "";
            if (element.attr('qindex') != undefined) { //for xa custom questions
                questionName = $('#CustomQuestion_' + element.attr('qindex')).text();
            } else { //for loans custom questions     
                questionName = element.parent().prev().text();
            }
            errorMsg += validateRegExpression(element, questionName);
        });

        //validate conditioned question
        var conditionQuestionElem = $('#' + getDivCustomQuestionID() + ' input.ConditionedQuestion');
        
        conditionQuestionElem.each(function () {
            var element = $(this);
            var conditionErrorMsg = "";
            var questionName = $('#'+element.attr('aria-labelledby')).text();
             conditionErrorMsg = validateRegExpression(element, questionName);
            
            if (element.attr('type') == 'password') {
                //validate conditioned re-enter password field
                var isRequired = element.attr('is_required');
                if (isRequired == 'Y') {
                    var reEnterPasswordElem = $('input[aria-labelledby="' + element.attr('aria-labelledby') + '_confirm"]');
                    if (element.val().trim() != "" && conditionErrorMsg == "") {
                        if (element.val() != reEnterPasswordElem.val()) {
                            conditionErrorMsg += "<br/>" + reEnterPasswordElem.text() + " -- Confirmation codes do not match.<br/>";
                        }
                    }           
                }//end validate conditioned re-enter password field
            }
            errorMsg += conditionErrorMsg;
        });

        return errorMsg;
    }   //end validate regular expression   

    function getChkCustomQuestionList(cqClass, cqType) {
        var chkCQList = new Array();
        var chkCQ = new Object();
        var chkCQArray = new Array();
        var chkCQObject = {};
        $('#' + getDivCustomQuestionID() + ' input.' + cqClass).each(function () {
            var currElement = $(this);
            if (currElement.attr('type').toLowerCase() == cqType) {
                chkCQArray.push(currElement.attr('name')); //get all chk questions 
            }
        });
        if (chkCQArray.length > 0) { //group chk questions and get number of chk for each custom question
            for (chkCQ in chkCQArray) {
                if (chkCQObject[chkCQArray[chkCQ]]) {
                    chkCQObject[chkCQArray[chkCQ]]++;
                } else {
                    chkCQObject[chkCQArray[chkCQ]] = 1;
                }
            }
        }
        for (var key in chkCQObject) {
            chkCQ = { chkName: key, chkNum: chkCQObject[key] };
            chkCQList.push(chkCQ); //put chk name and num of chk in array list
        }
        return chkCQList;
    }
    function validatePasswordField(element,isRequired) {
        var strMessage = "";
        if (element.attr('type') == 'password') {
            var reEnterPasswordElem = $("input[iname='re_" + element.attr('iname') + "']");
            if (isRequired == 'Y') {
                if (reEnterPasswordElem.val() == "") {
                	strMessage = "<br/>" + element.parent().prev().children().first().text() + " -- Confirmation codes do not match.<br/>";
                } else if (element.val() != reEnterPasswordElem.val()) {
                	strMessage = "<br/>" + element.parent().prev().children().first().text() + " -- Confirmation codes do not match.<br/>";
                }
            } else {
                if (element.val() != "" || reEnterPasswordElem.val() != "") {// validate password if the input password is not empty
                    if (element.val() != reEnterPasswordElem.val()) {
                    	strMessage = "<br/>" + element.parent().prev().children().first().text() + " -- Confirmation codes do not match.<br/>";
                    }
                }
            }
        }
        return strMessage;
    }

    //this function for checkbox custom questions 
    function chkCustomQuestionArray() {
    	//push all the check box question index in chkquestionArray
    	var chkQuestionArray = new Array();
    	$('#' + getDivCustomQuestionID() + ' .CustomQuestion:not(span)').each(function () {
    		var element = $(this);
    		var chkAnswers = "";
    		var qNum = element.attr('qindex');
    		var questionName = $('#CustomQuestion_' + qNum).text();
    		var chkLength = element.attr('chk_cq_length');
    		var cqId = "CustomQuestion_" + qNum;
    		var isRequired = $('#' + cqId).attr('cq_required');
    		var ui_type = element.attr('type');
    		if (ui_type == 'checkbox') {
    			if (element.prev('.ui-checkbox-on').length > 0) {
    				chkAnswers = element.prev("label").text();
    				chkQuestionArray.push(new checkboxQuestion(chkAnswers, questionName, qNum, chkLength, isRequired));
    			}
    		}
    	});
    	return chkQuestionArray;
    }

    function ReviewCheckboxCustomQuestion() {
    	var chkQuestionArray = [];
    	//push all the check box question index in chkquestionArray
    	chkQuestionArray = chkCustomQuestionArray();
    	var chkQuestionNameArray = getChkQuestionName(chkQuestionArray);
    	var strCheckBox = "";
    	if (chkQuestionNameArray.length > 0) {
    		for (var i = 0; i < chkQuestionNameArray.length; i++) {
    			var questionName = chkQuestionNameArray[i];
    			var strCheckboxHtml = "";
    			for (var j = 0; j < chkQuestionArray.length; j++) {
    				if (chkQuestionArray[j].chkQuestionName == questionName && chkQuestionArray[j].chkAnswers != "") {
    					strCheckboxHtml += "<br/><span style='margin-left:7px'>" + chkQuestionArray[j].chkAnswers;
    				}
    			}
    			strCheckBox += "<br/><span class='bold'>" + questionName + "</span>" + strCheckboxHtml;
    		}
    	}
    	return strCheckBox;
    }
   
    function GetCustomAnswers() {
        var array = new Array();
        var cutomQuestionObj;

        $('#' + getDivCustomQuestionID() + ' input.CustomQuestion').each(function () {
        	var element = $(this);
	        if (element.is(":hidden")) {
		        return; // abort current element and move to next
	        }
            var ui_type = element.attr('type');
            switch (ui_type.toLowerCase()) {
                case 'text':
                case 'password':
                    cutomQuestionObj = new Object();
                    cutomQuestionObj.Name = element.attr('iname');
                    cutomQuestionObj.Answer = element.val();
                    cutomQuestionObj.AnswerText = element.val();
                    //add answer type
                    cutomQuestionObj.AnswerType = element.attr('type');
                    array.push(cutomQuestionObj);
                    break;
                case 'checkbox':
                    if (element.prev('.ui-checkbox-on').length > 0) {
                        cutomQuestionObj = new Object();
                        cutomQuestionObj.Name = element.attr('iname');
                        cutomQuestionObj.Answer = element.attr('answer');
                        cutomQuestionObj.AnswerText = element.prev('.ui-checkbox-on').text();
                        //add answer type
                        cutomQuestionObj.AnswerType = element.attr('type');
                        array.push(cutomQuestionObj);
                    }
                    break;
                case 'radio':
                    if (element.prev('.ui-radio-on').length > 0) {
                        cutomQuestionObj = new Object();
                        cutomQuestionObj.Name = element.attr('iname');
                        cutomQuestionObj.Answer = element.attr('answer');
                        cutomQuestionObj.AnswerText = element.prev('.ui-radio-on').text();
                        //add answer type
                        cutomQuestionObj.AnswerType = element.attr('type');
                        array.push(cutomQuestionObj);
                    }
                    break;
                case 'hidden':
                    cutomQuestionObj = new Object();
                    cutomQuestionObj.Name = element.attr('iname');
                    cutomQuestionObj.Answer = element.val();
                    cutomQuestionObj.AnswerText = element.val();
                    //add answer type
                    cutomQuestionObj.AnswerType = element.attr('type');
                    array.push(cutomQuestionObj);
                    break;
                default:
                    break;
            }
        });

        $('#' + getDivCustomQuestionID() + ' select.CustomQuestion').each(function () {
        	var element = $(this);
        	if (element.is(":hidden")) {
        		return; // abort current element and move to next
        	}
            cutomQuestionObj = new Object();
            cutomQuestionObj.Name = element.attr('iname');
            cutomQuestionObj.Answer = element.find('option:selected').val();
            //''''missing dropdown text, so just add answer text
            cutomQuestionObj.AnswerText = element.find('option:selected').text();
            //add answer type
            cutomQuestionObj.AnswerType = element.attr('type');
            array.push(cutomQuestionObj);
        });

        $('#' + getDivCustomQuestionID() + ' input.ConditionedQuestion').each(function () {
        	var element = $(this);
        	if (element.is(":hidden")) {
        		return; // abort current element and move to next
        	}
            var ui_type = element.attr('type');  
            switch (ui_type.toLowerCase()) {
                case 'text':
                case 'password':
                    cutomQuestionObj = new Object();
                    cutomQuestionObj.Name = element.attr('iname');
                    cutomQuestionObj.Answer = element.val();
                    cutomQuestionObj.AnswerText = element.val();
                    //add answer type
                    cutomQuestionObj.AnswerType = element.attr('type');
                    array.push(cutomQuestionObj);
                    break;
                case 'checkbox':
                    if (element.prev('.ui-checkbox-on').length > 0) {
                        cutomQuestionObj = new Object();
                        cutomQuestionObj.Name = element.attr('iname');
                        cutomQuestionObj.Answer = element.attr('answer');
                        cutomQuestionObj.AnswerText = element.prev('.ui-checkbox-on').text();
                        //add answer type
                        cutomQuestionObj.AnswerType = element.attr('type');
                        array.push(cutomQuestionObj);
                    }
                    break;
                case 'radio':
                    if (element.prev('.ui-radio-on').length > 0) {
                        cutomQuestionObj = new Object();
                        cutomQuestionObj.Name = element.attr('iname');
                        cutomQuestionObj.Answer = element.attr('answer');
                        cutomQuestionObj.AnswerText = element.prev('.ui-radio-on').text();
                        //add answer type
                        cutomQuestionObj.AnswerType = element.attr('type');
                        array.push(cutomQuestionObj);
                    }
                    break;
                case 'hidden':
                    cutomQuestionObj = new Object();
                    cutomQuestionObj.Name = element.attr('iname');
                    cutomQuestionObj.Answer = element.val();
                    cutomQuestionObj.AnswerText = element.val();
                    //add answer type
                    cutomQuestionObj.AnswerType = element.attr('type');
                    array.push(cutomQuestionObj);
                    break;
                default:
                    break;
            }         
        });

        $('#' + getDivCustomQuestionID() + ' select.ConditionedQuestion').each(function () {
        	var element = $(this);
        	if (element.is(":hidden")) {
        		return; // abort current element and move to next
        	}          
            cutomQuestionObj = new Object();
            cutomQuestionObj.Name = element.attr('iname');
            cutomQuestionObj.Answer = element.find('option:selected').val();
            //''''missing dropdown text, so just add answer text
            cutomQuestionObj.AnswerText = element.find('option:selected').text();
            //add answer type
            cutomQuestionObj.AnswerType = element.attr('type');
            array.push(cutomQuestionObj);
            
        });
      
       return array;
    }
    function ViewCustomQuestion() {
    	var strCustomQuestion = "";
    	var hasChk = true;
    	$('#' + getDivCustomQuestionID() + ' .CustomQuestion:not(span)').each(function () {
    		var element = $(this);
    		if (element.is(":hidden")) {
    			return; // abort current element and move to next
    		}
    		var txtValue = element.val();
    		var labelText = element.prev("label").text();
    		var qNum = element.attr('qindex');
    		var questionName = $('#CustomQuestion_' + qNum).text();		
    		var ui_type = element.attr('type');
    		if (ui_type == 'checkbox') {
    			if (hasChk) {
    				strCustomQuestion += ReviewCheckboxCustomQuestion(); //no applicant question, no co app
    				hasChk = false;
    			}
    		}
    		if (ui_type == 'radio') {
    			if (element.prev('.ui-radio-on').length > 0) {
    				strCustomQuestion += "<br/><span class='bold'>" + questionName + "</span><br/><span style='margin-left:7px'> " + labelText + "</span>";
    			}
    		}
    		if (ui_type == 'text') {
    			if (txtValue != '' && txtValue != undefined)
    				strCustomQuestion += "<br/><span class='bold'>" + questionName + "</span><br/><span style='margin-left:7px'> " + txtValue + "</span>";
    		}
    		if (ui_type == 'password') {
    			if (txtValue != undefined && txtValue.trim() != '') {
    				var pwValue = "";
    				for (var i = 0; i < txtValue.length; i++) {
    					pwValue += "*";
    				}
    				strCustomQuestion += "<br/><span class='bold'>" + questionName + "</span><br/><span style='margin-left:7px'> " + pwValue + "</span>";
    			}
    		}
    	});
    	//using select.customquestion to get drop down question type
    	$('#' + getDivCustomQuestionID() + ' select.CustomQuestion').each(function () {
    		var element = $(this);
    		if (element.is(":hidden")) {
    			return; // abort current element and move to next
    		}
    		var ddlText = element.find('option:selected').text();
    		var qNum = element.attr('qindex');
    		var questionName = $('#CustomQuestion_' + qNum).text();   		
    		var ddlValue = element.find('option:selected').val().toUpperCase();
    		if (ddlValue != "" && ddlValue != undefined) { //"None could be a user answer,
    			strCustomQuestion += "<br/><span class='bold'>" + questionName + "</span><br/> <span style='margin-left:7px'>" + ddlText + "</span>";
    		}
    	});
    	strCustomQuestion += ViewConditionQuestions("");
    	return strCustomQuestion;
    }
    $(function () {
    //remove horizontal line from download
        $('.Title hr').remove();
        HideAndShowCustomQuestionTitle();
        registerCQValidator<%=LoanType%>();
    });
    function HideAndShowCustomQuestionTitle() {
        var divCustomQuestionElem = $('#' + getDivCustomQuestionID());
        if (divCustomQuestionElem.children().length == 0) {
            divCustomQuestionElem.parent().addClass('HiddenElement');
        } else {
            divCustomQuestionElem.parent().removeClass('HiddenElement');
        }
    }

    function registerCQValidator<%=LoanType%>() {
        var $container = $("#<%=_divCustomQuestionId%>");
        $('input.CustomQuestion, input.ConditionedQuestion', $container).each(function (idx, ele) {
            var $ele = $(ele);
            if ($ele.is(":checkbox")) {
                $(ele).closest("div.chkgrp").observer({
                    validators: [
						function (partial) {
						    if ($(this).is(":hidden")) return "";
						    var $con = $(this);
						    if ($con.attr("is_required") === "Y") {
						        if ($("input:checkbox:checked", $con).length === 0) {
						            return "Please answer this question";
						        }    
						    }
						    return "";
						}
					],
                    validateOnBlur: true,
                    groupType: "checkboxGroup",
                    group: "ValidateCustomQuestions<%=LoanType%>"
                });
            }else if ($ele.is(":radio")) {
                $(ele).closest("div.radgrp").observer({
                    validators: [
						function (partial) {
						    if ($(this).is(":hidden")) return "";
						    var $con = $(this);
						    if ($con.attr("is_required") === "Y") {
						        if ($("input:radio:checked", $con).length === 0) {
						            return "Please answer this question";
						        }
						    }
						    return "";
						}
                    ],
                    validateOnBlur: true,
                    groupType: "checkboxGroup",
                    group: "ValidateCustomQuestions<%=LoanType%>"
                });
            }else if ($ele.is(":text")) {
                $ele.observer({
                    validators: [
                        function (partial) {
                            if ($(this).is(":hidden")) return "";
                            var msg = "";
                            if ($(this).attr("is_required") === "Y" && $.trim($(this).val()) === "") {
                                msg = "Please answer this question";
                            }
                            if (msg === "") {
                                var questionName = "";
                                if ($(this).attr('qindex') != undefined) { //for xa custom questions
                                    questionName = $('#CustomQuestion_' + $(this).attr('qindex')).text();                                 
                                } else { //for loans custom questions     
                                    questionName = $(this).parent().prev().text();
                                }
                                msg = validateRegExpression($(this), questionName).replace(/<br\/>/g, "");
                            }
                            return msg;
                        }
                    ],
                    validateOnBlur: true,
                    group: "ValidateCustomQuestions<%=LoanType%>"
                });
            } else if ($ele.is(":password")) {
                $ele.observer({
                    validators: [
                        function (partial) {
                            if ($(this).is(":hidden")) return "";
                            var msg = "";
                            if ($(this).attr("is_required") === "Y" && $.trim($(this).val()) === "") {
                                msg = "Please answer this question";
                            }
                            if (msg === "") {
                                var questionName = "";
                                if ($(this).attr('qindex') != undefined) { //for xa custom questions
                                    questionName = $('#CustomQuestion_' + $(this).attr('qindex')).text();                               
                                } else { //for loans custom questions     
                                    questionName = $(this).parent().prev().text();
                                }
                                msg = validateRegExpression($(this), questionName).replace(/<br\/>/g, "");
                            }
                            if (msg === "") {
                                var $mField = $("input[iname='re_" + $(this).attr("iname") + "']");
                                if ($mField.length === 1 && $mField.val() !== $(this).val()) {
                                    $.lpqValidate.showValidation($mField, "Confirmation codes do not match");
                                } else {
                                    $.lpqValidate.hideValidation($mField);
                                }
                            }
                            return msg;
                        }
                    ],
                    validateOnBlur: true,
                  group: "ValidateCustomQuestions<%=LoanType%>"
                });
                var iName = $ele.attr("iname");
                var $matchField = $("input[iname='re_" + iName + "']");
                $matchField.observer({
                    validators: [
                        function (partial) {
                            if ($(this).is(":hidden")) return "";
                            var msg = "";
                            var $mainField = $("input[iname='" + $(this).attr("iname").replace("re_", "") + "']");
                            if ($(this).val() !== $mainField.val()) {
                                msg = "Confirmation codes do not match";
                            }
                            return msg;
                        }
                    ],
                    validateOnBlur: true,
                    group: "ValidateCustomQuestions<%=LoanType%>"
                });
            }
        });
        $('select.CustomQuestion, select.ConditionedQuestion', $container).each(function (idx, ele) {
            var $ele = $(ele);
            $ele.observer({
                validators: [
                    function (partial) {
                        if ($(this).is(":hidden")) return "";
                        var msg = "";
                        if ($(this).attr("is_required") === "Y") {
                            var selectedVal = $.trim($(this).val());
                            var selectedText = $.trim($(this).find("option:selected").text());
                            if (selectedVal === "" || selectedText === "") {
                                msg = "Please answer this question";
                            }
                        }
                        return msg;
                    }
                ],
                validateOnBlur: true,
                group: "ValidateCustomQuestions<%=LoanType%>"
            });
        });
    }
    function getUrlParaCustomQuestions() {
         var oCustomQuestions = [];
         var oCustomQuestion;
         //add all url parameter custom questions to the array if it exist       
        <%If _URLParaCustomQuestions IsNot Nothing AndAlso _URLParaCustomQuestions.Any() Then %>
         var urlParaCustomQuestions = <%= JsonConvert.SerializeObject(_URLParaCustomQuestions) %>;
            if (urlParaCustomQuestions.length > 0) {
                for (var i = 0; i < urlParaCustomQuestions.length; i++) {              
                    var oUrlParaCQ = urlParaCustomQuestions[i];                                  
                    oCustomQuestion = {};
                    var answerText = oUrlParaCQ.AnswerText;
                    //for textbox field: answer value and answer text would be the same
                    if (oUrlParaCQ.AnswerType.toUpperCase() == "TEXT") {
                        answerText = oUrlParaCQ.Answer;
                    }
                    oCustomQuestion.Name = oUrlParaCQ.Name;  
                    oCustomQuestion.Answer = oUrlParaCQ.Answer;
                    oCustomQuestion.AnswerText = answerText;                 
                    oCustomQuestion.AnswerType = oUrlParaCQ.AnswerType;            
                    oCustomQuestions.push(oCustomQuestion);
                } 
            }
        <%End If %>     
      return oCustomQuestions;
    }      
</script>

