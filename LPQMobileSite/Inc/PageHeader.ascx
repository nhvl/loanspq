﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="PageHeader.ascx.vb" Inherits="Inc_PageHeader" %>
<%@ Import Namespace="Newtonsoft.Json" %>
<%@ Import Namespace="LPQMobile.Utils" %>
<%@ Import Namespace="System.Web.Optimization" %>
<meta http-equiv="Cache-control" content="no-cache, no-store">

<!--[if lte IE 8]>
	<p class="browsehappy">
	You are using an <strong>outdated</strong> browser.
	Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
	<script type="text/jscript">
		document.execCommand('Stop');
	</script>
<![endif]-->

<noscript>
	For full functionality of this site it is necessary to enable JavaScript.
	Here are the <a href="http://www.enable-javascript.com/" target="_blank">
	instructions how to enable JavaScript in your web browser</a>.
</noscript>

<%=getFrameBreakerCode()%>
<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=yes" />

<!-- get the required files from 3rd party sources -->
<link href='https://fonts.googleapis.com/css?family=Roboto:Light,Regular,Bold' rel='stylesheet' type='text/css'>

<script type="text/javascript">
    var server_root = '<%=ServerRoot%>';
    var g_IsHideGMI = '<%=IsHideGMI%>' == 'True';
    var g_IsHideGMI_HELOC = '<%=IsHideGMI_HELOC%>' == 'True';
    var gl_BaseSubmitLoanUrl = "<%=_currentConfig.BaseSubmitLoanUrl.Replace("/services", "")%>";
    var gl_content_data_theme = '<%=_contentDataTheme%>';
    var gl_has_footer_right = '<%=HasFooterRight%>';
    var hasRestartButton = '<%=restart_button%>';
    var finishedUrl = '<%=_currentConfig.FinishedUrl%>';
    var gl_button_font_color = '<%=_buttonFontColor%>';
    var gl_icon_close = '<%=HeaderUtils.IconClose.Replace(vbCrLf, "").Replace(vbLf, "").Replace(vbCr, "")%>';
    var gl_timeout_url = '<%=TimeOutUrl%>';
    var is_foreign_phone = <%=IIf(ForeignPhone, "true", "false")%>;
    var is_foreign_address = <%=IIf(ForeignAddress, "true", "false")%>;
    var qDATA = null;
    <%if _queryStringDic isnot nothing andalso _queryStringDic.Any() then %>
    qDATA = <%=JsonConvert.SerializeObject(_queryStringDic)%>;
    <%End If%>
</script>
<%=getLinkedInCode()%>

<%--TODO:convert to min--%>
<link rel="Stylesheet" href="/css/themes/default/jquery.mobile-1.4.5.min.css?ver=<%=Common.GetBuildVersion()%>" />
<%If IsInMode("777") Then%>
<link rel="Stylesheet" href="../Sm/content/font-awesome/css/font-awesome.css" />
<%End If%>
<link rel="Stylesheet" href="/css/themes/default/<%=sThemeFileName%>?ver=<%=Common.GetBuildVersion()%>" />
<%:Styles.Render("~/css/thirdparty/misc")%>
<%:Styles.Render("~/css/default")%>
<%:Scripts.Render("~/js/lib")%>
<%:Scripts.Render("~/js/thirdparty/intltelinput/script")%>
<%If String.IsNullOrEmpty(ScriptFolder) = False Then%>
<%:Scripts.Render("~/" & ScriptFolder & "/script")%>
<%End If%>
<%If String.IsNullOrEmpty(_currentConfig.Favico) Then%>
<link rel="shortcut icon" href="https://app.loanspq.com/logo/lpq_favicon.ico"  type="image/ico"  />
<link rel="icon" href="https://app.loanspq.com/logo/lpq_favicon.ico"  type="image/ico"  />
<%Else%>
<link rel="shortcut icon" href="<%=_currentConfig.Favico%>"/>
<link rel="icon" href="<%=_currentConfig.Favico%>"/>
<%End If%>

<%--Custom cross domain code--%>
<%=getCrossDomainCode()%>
<script type="text/javascript">
    //goback to final decision message after docusign is closed
    window.addEventListener("message", function (e) {
        var currentUrl = window.location.protocol + "//" + window.location.host;           
        if (currentUrl === e.origin && e.data === "DocuSignTrackerLoaded") {            
            $('#href_show_last_dialog').trigger('click');
        } else if (currentUrl === e.origin && typeof e.data === "string" && e.data.indexOf("DocuSignTrackerLoaded_verifyID_") > -1) { //this is for custom merchant call
            $('#hdDocusignVeriStatus').val(e.data.substring(e.data.indexOf("verifyID_")+9));
            $('#submit_verify_identity').trigger('click');                     
        }
    }, false);
</script>
<%If IsInMode("777") Then%>
<link rel="stylesheet" type="text/css" href="/css/adminStyle.css"/>
<%End If%>
