﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="Agreement.ascx.vb" Inherits="Inc_Agreement" %>
<div id="<%=IDPrefix%>divAgreement" class="RoundedSection" style="margin-top: 20px;
    background-color: #ffddd5; padding: 5px;">
    <!-- #FDC3B6 -->
    <p style="font-size: 90%;">
        <strong>YOUR APPLICATION IS NOT COMPLETE UNTIL YOU READ THE DISCLOSURE BELOW AND CLICK THE
            “AGREE” BUTTON IN ORDER TO SUBMIT YOUR APPLICATION.</strong></p>
    <p style="font-size: 90%; text-align: justify;">
        You are now ready to submit your application! By clicking on "I agree", you authorize
        us to verify the information you submitted and to obtain your credit report.  
		Upon your request, we will tell you if a credit report was obtained and give
        you the name and address of the credit reporting agency that provided the report.
        You warrant to us that the information you are submitting is true and correct. By
        submitting this application, you agree to allow us to receive the information contained
        in your application, as well as the status of your application.</p>
    <div>
        <fieldset data-role="controlgroup" data-type="horizontal">
            <input type="radio" name="<%=IDPrefix%>rdAgreement" id="<%=IDPrefix%>rdDisagree"
                class="custom btn-redirect" data-mini="true" url='<%=RedirectURL%>' />
            <label for="<%=IDPrefix%>rdDisagree">
                I Disagree</label>
            <input type="radio" name="<%=IDPrefix%>rdAgreement" id="<%=IDPrefix%>rdAgree" class="custom"
                data-mini="true" onclick="finishMainApplicant(this);" />
            <label for="<%=IDPrefix%>rdAgree">
                I Agree</label>
        </fieldset>
    </div>
    <a href="#divErrorDialog" style="display: none;"></a><a id="<%=NextScreen%>nav_screen" href="<%=NextScreen%>" style="display: none;">
    </a>
</div>

<script type="text/javascript">
    function <%=IDPrefix%>ShowAgreement() {
        $('#<%=IDPrefix%>divAgreement').show();
    }

    function <%=IDPrefix%>HideAgreement() {
        $('#<%=IDPrefix%>divAgreement').hide();
    }
    
    function finishMainApplicant() {       
        var hasCoApp = $('#ddlHasCoApplicant option:selected').val();

        if (hasCoApp == "N") {
            return saveLoanInfo(document.getElementById('href_save'));
        }
        else {
            currElement.next().next().trigger('click');
            return true;
        }
    }
</script>

