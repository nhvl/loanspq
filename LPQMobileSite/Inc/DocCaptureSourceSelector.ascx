﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="DocCaptureSourceSelector.ascx.vb" Inherits="Inc_DocCaptureSourceSelector" %>
<div data-role="dialog" id="<%=IDPrefix%>DocUploadSrcSelector" role="region" aria-label="Do Upload Src Selector">
    <div data-role="header"  style="display:none" >
        <h1>Document Upload Source Selector</h1>
    </div>
    <div data-role="content">
		<a data-rel="back" href="#" class="pull-right svg-btn"><%=HeaderUtils.IconClose %><span style="display: none;">close</span></a>  
		 <div class="du-selector-wrapper">
			 <div class="img-responsive" data-command="upload">
				<img src="../../images/dc_upload.jpg" alt="Document Upload Button"/>
			</div> 
			<div class="img-responsive" data-command="capture">
				<img src="../../images/dc_capture.jpg" alt="Document Capture Button"/>
			</div>
			 <div class="capture-stream">
				<video autoplay=""></video>
				<canvas class="hidden"></canvas>
				<img/>
				<button class="close-btn">Back</button>
				<div class="control-btns">
					<span class="capture-btn"><button></button></span>
					<button class="cancel-btn">Cancel</button>
					<button class="continue-btn">Continue</button>
				</div>
			</div>
		 </div>
		  
    </div>
</div>