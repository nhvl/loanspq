﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="NewDocCapture.ascx.vb" Inherits="Inc_NewDocCapture" %>
<%@ Import Namespace="Newtonsoft.Json" %>
<div id="<%=IDPrefix%>divDocCaptureWrapper">
	<%If Not String.IsNullOrWhiteSpace(Title) Then%>
	<%=HeaderUtils.RenderPageTitle(0, Title, True)%>
	<%End If%>
	<br/>
	<div id="<%=IDPrefix%>divDocCaptureMessage"></div>
	<div>         
		<div id ="<%=IDPrefix%>divImage" class="divImages row"></div>   
		<div class="fileupload fileupload-new" id="<%=IDPrefix%>fileupload-container" data-provides="fileupload" >
		<div>
			<a style="display: inline-block;" href="#" class="img-btn">
			<div section-name="docupload" class="uploaddocs-box svg-img" id="<%=IDPrefix%>imagePlaceHolder">
				<p class="pull-left caption caption-text bold">Click or tap here to capture or upload an image or document.</p>
				<div class="pull-right avatar"><%=HeaderUtils.UploadThumb %></div>
			</div>
			</a>
			<div class="clearfix"></div>
		</div>
		<div id="<%=IDPrefix%>image-thumbnail" class="fileupload-preview fileupload-exists thumbnail"></div>
		<div style="display: none;">
			<!--display browse button for upload or capture image-->                    
			<input aria-labelledby="<%=IDPrefix%>imagePlaceHolder" name="driversLicenseImage" id="<%=IDPrefix%>btnFileCamera" type="file" capture="camera" accept="image/*, application/pdf">
		</div>
		</div>
	</div>
	<input type="hidden" id="<%=IDPrefix%>hdOneColumnLayout" value="<%=OneColumnLayout%>"/>
</div>
 <script type="text/javascript">
 	var <%=IDPrefix%>docUploadObj;
 	$(function() {
 		var <%=IDPrefix%>DocUploadSettings = {
 			prefix: "<%=IDPrefix%>",
 			requireDocTitle: <%=IIf(RequireDocTitle, "true", "false")%>,
 			docTitleOptions: <%=JsonConvert.SerializeObject(DocTitleOptions)%>,
 			requireDocUpload: <%=IIf(DocUploadRequired, "true", "false")%>,
 		};

 		<%=IDPrefix%>docUploadObj = new LPQDocCapture(<%=IDPrefix%>DocUploadSettings);
 	});
 </script>