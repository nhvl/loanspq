﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="VinNumberScan.ascx.vb" Inherits="Inc_VinNumberScan" %>
<div class="divScanDocs vin-laser-scan">
	<div id="scandit-vin-barcode-picker" style="max-width: 1280px; max-height: 530px; margin: 10px 0;"></div>
	<div class="no-camera hidden">
		<img src="/images/no-camera.png" class="img-responsive"/>
		<p>There is no connected camera</p>
	</div>
	<div class="scan-guide-text hidden"><p>Center align blue bar with VIN number barcode</p></div>
</div>