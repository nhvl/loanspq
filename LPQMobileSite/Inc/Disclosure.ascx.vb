﻿Imports LPQMobile.BO
Imports LPQMobile.Utils
Imports System.Xml
Imports System.Xml.Serialization
Imports System.IO

Partial Class Inc_Disclosure
    'Inherits System.Web.UI.UserControl
    Inherits CBaseUserControl
    Private _name As String
    'Public LoanType As String '"CC,VL,PL,HE, HELOC, XA

    Public Property EnableEmailMeButton As Boolean
    Private _DisclosureLinks As New Dictionary(Of String, String)
    Public Property DisclosureLinks() As Dictionary(Of String, String)
        Get
            Return _DisclosureLinks
        End Get
        Set(ByVal value As Dictionary(Of String, String))
            _DisclosureLinks = value
        End Set
    End Property


    Public Property Name() As String
        Get
            Return _name
        End Get
        Set(ByVal value As String)
            _name = value
        End Set
    End Property

    'self load or passed from parent
    Private _Disclosures As New List(Of String)
    Public Property Disclosures() As List(Of String)
        Get
            Return _Disclosures
        End Get
        Set(ByVal value As List(Of String))
            _Disclosures = value
        End Set
    End Property
    Private _strRenderingVerifyCode As String
    Public Property strRenderingVerifyCode() As String
        Get
            Return _strRenderingVerifyCode
        End Get
        Set(value As String)
            _strRenderingVerifyCode = value
        End Set
    End Property

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            InitDiclosure()

            divDisclosureContent.Attributes("name") = Name
            Dim lstA As New List(Of String)
            Dim lstB As New Dictionary(Of String, String)

            '        For Each disclosure As String In _Disclosures
            '            Dim regEx As New Regex("^<a(.+)>(.+)</a>$", RegexOptions.IgnoreCase)
            '            Dim m As Match = regEx.Match(disclosure.Trim())
            '            If m.Success Then
            '                Dim sValue As String = m.Groups(1).Value
            '                If sValue.Contains("DisclosureDisable") Then
            '                    sValue = sValue.Replace("DisclosureDisable", "btn-header-theme")
            '                End If
            '                If Not lstB.ContainsKey(m.Groups(2).Value) Then '' don't add item if key exist
            '                    lstB.Add(m.Groups(2).Value, sValue)
            '                End If
            '            Else
            '                lstA.Add(disclosure)
            '            End If

            '            'get links from link button and/or normal disclosure
            '            Dim regEx2 As New Regex("<a(.+)>(.+)</a>", RegexOptions.IgnoreCase)
            '            Dim m2 As Match = regEx2.Match(disclosure.Trim())
            'If m2.Success And DisclosureLinks.ContainsKey(m2.Groups(2).Value) = False Then
            '	DisclosureLinks.Add(m2.Groups(2).Value, m2.Groups(1).Value)
            'End If
            '        Next
            DisclosureLinks = Common.CompileDisclosures(_Disclosures, lstB, lstA)
            'TODO: remove lender restriction when ready for deploy
            '         Dim bIsVisibleLender As Boolean = If(Request.Params("lenderref").NullSafeToLower_.StartsWith("lpquniversity") = True Or Request.Params("lenderref").NullSafeToLower_.StartsWith("11111153eed453b") = True, True, False)
            'If lstAllLinks IsNot Nothing AndAlso lstAllLinks.Any() AndAlso bIsVisibleLender Then
            '	showEmailMeButton = True
            'End If
            rptDisclosureByButton.DataSource = lstB
            rptDisclosureByButton.DataBind()
            rptDisclosureByCheck.DataSource = lstA
            rptDisclosureByCheck.DataBind()
            divDisclosureContent.Visible = Disclosures.Count > 0
        End If
    End Sub

    Private Sub InitDiclosure()

        If LoanType = "XA" Or LoanType = "BL" Then Return 'Disclosures passsed from parent

        '' parent page should be CBasePage
        Dim parent As CBasePage = TryCast(Me.Page, CBasePage)
        If parent Is Nothing Or parent._CurrentWebsiteConfig Is Nothing Then
            Return
        End If
        Disclosures = Common.GetDisclosures(parent._CurrentWebsiteConfig, LoanType, parent.IsComboMode)

        ''Dim CCDisclosures As List(Of String) = _CurrentWebsiteConfig.GetLoanDisclosures("CREDIT_CARD_LOAN")
        'Dim CCDisclosures As List(Of String)
        'If LoanType = "HELOC" Then
        '	'use DISCLOSURES_LOC when purpose contain keyword "LOC" and <HOME_EQUITY_LOAN><DISCLOSURES_LOC> is not null.
        '	CCDisclosures = parent._CurrentWebsiteConfig.GetLoanDisclosuresLoc(Common.GetConfigLoanTypeFromShort(LoanType))
        '	If CCDisclosures Is Nothing Or Not CCDisclosures.Any() Then
        '		CCDisclosures = parent._CurrentWebsiteConfig.GetLoanDisclosures(Common.GetConfigLoanTypeFromShort(LoanType))
        '	End If
        'Else
        '	CCDisclosures = parent._CurrentWebsiteConfig.GetLoanDisclosures(Common.GetConfigLoanTypeFromShort(LoanType))
        'End If
        '      Dim IsComboMode As Boolean = parent.IsComboMode

        '      Disclosures = CCDisclosures

        '      'Since consumer will be applying for both loan and membership(XA) at the same time, the system needs to show questions and disclosures for both loan and XA.
        '      If IsComboMode Then
        '          Dim XADisclosures As List(Of String) = parent._CurrentWebsiteConfig.GetLoanDisclosures("XA_LOAN")
        '          ''Dim CCDisclosures_Link As New Dictionary(Of String, String)
        '          ''Dim CCDisclosures_Text As New Dictionary(Of String, String)

        '          ''Dim regExLinkButton As New Regex("^<a.*\shref=['""](.[^""]{3,})['""](\s.*)?>(.+)</a>$", RegexOptions.IgnoreCase)
        '          ''Dim regExContainHtmlTag As New Regex("<.+>", RegexOptions.IgnoreCase)
        '          ''For Each ccDis As String In CCDisclosures
        '          ''    Dim m As Match = regExLinkButton.Match(ccDis.Trim())
        '          ''    If m.Success Then
        '          ''        If CCDisclosures_Link.ContainsKey(m.Groups(1).Value) Then Continue For
        '          ''        CCDisclosures_Link.Add(m.Groups(1).Value, ccDis)
        '          ''    ElseIf regExContainHtmlTag.IsMatch(ccDis) Then
        '          ''        'TODO: don't know what should I do with this - a disclosures mixed between text and link. For exmple: I agree<a href="abc.com">condition</a>
        '          ''    Else 'disclosures with text only
        '          ''        Dim maxLength = ccDis.Trim().Length
        '          ''        If maxLength > 100 Then
        '          ''            maxLength = 100
        '          ''        End If
        '          ''        CCDisclosures_Text.Add(ccDis.Trim().Substring(0, maxLength), ccDis)
        '          ''    End If
        '          ''Next
        '          ''For Each xaDis As String In XADisclosures
        '          ''    Dim m As Match = regExLinkButton.Match(xaDis.Trim())
        '          ''    If m.Success Then
        '          ''        If CCDisclosures_Link.ContainsKey(m.Groups(1).Value) Then
        '          ''            Continue For ' this disclosures existed, abort it
        '          ''        End If
        '          ''    ElseIf regExContainHtmlTag.IsMatch(xaDis) Then
        '          ''    Else
        '          ''        Dim maxLength = xaDis.Trim().Length
        '          ''        If maxLength > 100 Then
        '          ''            maxLength = 100
        '          ''        End If
        '          ''        If CCDisclosures_Text.ContainsKey(xaDis.Trim().Substring(0, maxLength)) Then
        '          ''            Continue For ' this disclosures existed, abort it
        '          ''        End If
        '          ''    End If
        '          ''    Disclosures.Add(xaDis)
        '          ''Next
        '          Dim comboDisclosuresDic As New Dictionary(Of String, String)
        '          Dim comboDisclosures As New List(Of String)
        '          ''get loans disclosures
        '          Dim strCCDic As String = ""
        '          For Each loanCCDic In CCDisclosures
        '              strCCDic = Server.HtmlEncode(loanCCDic.Trim())
        '              If Not comboDisclosuresDic.ContainsKey(strCCDic) Then
        '                  comboDisclosuresDic.Add(strCCDic, strCCDic)
        '              End If
        '          Next
        '          '' get xa disclosures
        '          Dim strXADic As String = ""
        '          For Each xaDic In XADisclosures
        '              strXADic = Server.HtmlEncode(xaDic.Trim())
        '              If Not comboDisclosuresDic.ContainsKey(strXADic) Then
        '                  comboDisclosuresDic.Add(strXADic, strXADic)
        '              End If
        '          Next
        '          ''get combo disclosures
        '          For Each comboDic In comboDisclosuresDic
        '              comboDisclosures.Add(Server.HtmlDecode(comboDic.Value))
        '          Next
        '          Disclosures = comboDisclosures
        '      End If
    End Sub

    Function disabledClass(ByVal itemIndex As Integer) As String
        Dim strSetAgreement As String = Disclosures(itemIndex)
        Dim strDisabledClass As String = ""
        If strSetAgreement.ToUpper.Contains("DISCLOSUREDISABLE") Then
            strDisabledClass = "true"
        End If
        Return strDisabledClass
    End Function
End Class