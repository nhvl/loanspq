﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="ApplicationBlockRules.ascx.vb" Inherits="Inc_MainApp_ApplicationBlockRules" %>
<div id="applicationBlockRulesPage" data-role="dialog" data-history="false" data-close-btn="none" style="width: 100%;">
	<div data-role="header">
		<button class="header-hidden-btn">.</button>
		<div class="page-header-title"><%=CustomListItem.DialogTitle%></div>
		<div tabindex="0" onclick="ABR.FACTORY.closeApplicationBlockRulesDialog()" class="header_theme2 ui-btn-right" data-role="none">
			<%=HeaderUtils.IconClose %><span style="display: none;">close</span>
		</div>
	</div>
	<div data-role="content">
		<div class="rename-able dialog-content"><%=CustomListItem.DialogMessage%></div>
		<ul data-place-holder="reason-list" style="font-size: 1.1em;">
			
		</ul>
		<%--<p>Reason 1</p>
		<p>Reason 2</p>
		<p>Reason 3</p>--%>
		<div class="div-continue" style="width: 90%;">
			<a href="#" data-role="button" type="button" onclick="ABR.FACTORY.closeApplicationBlockRulesDialog()" class="button-style div-continue-button">Return to Application</a>
			<a href="#" data-role="button" type="button" onclick="ABR.FACTORY.cancelApplication(event)" class="button-style div-continue-button-reverse hover-goback">Cancel Application</a>
		</div>    
	</div>
</div>
<script type="text/javascript">
	
	(function (ABR, $, undefined) {
		ABR.FACTORY = {};
		ABR.DATA = {};
		<%If Not String.IsNullOrWhiteSpace(rulesJson) Then%>
		ABR.DATA.rules = <%=rulesJson%>;
		<%Else%>
		ABR.DATA.rules = [];
		<%End If%>

		function IS_ARRAY() {
			if (arguments && arguments.length > 0 && typeof arguments[0] != "undefined") {
				var value = arguments[0];
				var params = _.takeRight(arguments, arguments.length - 1);
				return Array.isArray(value) && Array.isArray(params) &&  _.isEqual(_.sortBy(value), _.sortBy(params));
			}
			return false;
		}
		function INTERSECTS() {
			if (arguments && arguments.length > 0 && typeof arguments[0] != "undefined") {
				var value = arguments[0];
				var params = _.takeRight(arguments, arguments.length - 1);
				return Array.isArray(value) && Array.isArray(params) &&  _.intersection(_.sortBy(value), _.sortBy(params)).length > 0;
			}
			return false;
		}
		function IS_A_SUPERSET_OF() {
			if (arguments && arguments.length > 0 && typeof arguments[0] != "undefined") {
				var value = arguments[0];
				var params = _.takeRight(arguments, arguments.length - 1);
				if (!Array.isArray(value) || !Array.isArray(params) || value.length == 0 || value.length < params.length) return false;
				return _.difference(value, params).length >= 0 && _.difference(params, value).length == 0;
			}
			return false;
		}
		function IS_A_SUBSET_OF() {
			if (arguments && arguments.length > 0 && typeof arguments[0] != "undefined") {
				var value = arguments[0];
				var params = _.takeRight(arguments, arguments.length - 1);
				if (!Array.isArray(value) || !Array.isArray(params) || value.length == 0 || value.length > params.length) return false;
				return _.difference(value, params).length == 0 && _.difference(params, value).length >= 0;
			}
			return false;
		}
		function X_DAYS_BEFORE_TODAY() {
			if (arguments && arguments.length == 2) {
				var d = moment(arguments[0], "MM/DD/YYYY");
				var x = arguments[1] || 0;

				return d.isSameOrBefore(moment().startOf("days")) && moment.duration(moment().startOf("days").diff(d)).asDays() == x;
			}
			return false;
		}
		function WITHIN_X_DAYS_BEFORE_TODAY() {
			if (arguments && arguments.length == 2 && typeof arguments[0] != "undefined") {
				var d = moment(arguments[0], "MM/DD/YYYY");
				var x = arguments[1] || 0;

				return d.isSameOrBefore(moment().startOf("days")) && moment.duration(moment().startOf("days").diff(d)).asDays() <= x;
			}
			return false;
		}
		function OLDER_THAN_X_DAYS_BEFORE_TODAY() {
			if (arguments && arguments.length == 2 && typeof arguments[0] != "undefined") {
				var d = moment(arguments[0], "MM/DD/YYYY");
				var x = arguments[1] || 0;

				return d.isSameOrBefore(moment().startOf("days")) && moment.duration(moment().startOf("days").diff(d)).asDays() > x;
			}
			return false;
		}
		function IS_DEFINED() {
			if (arguments && arguments.length == 1) {
				return typeof arguments[0] !== "undefined" && arguments[0] !== null && (typeof arguments[0] === "string" && arguments[0].trim() != "");
			}
			return false;
		}
		function ENDS_WITH() {
			if (arguments && arguments.length == 2 && typeof arguments[0] != "undefined") {
				var value = arguments[0] || "";
				return value.endsWith(arguments[1]);
			}
			return false;
		}
		function CONTAINS() {
			if (arguments && arguments.length == 2 && typeof arguments[0] != "undefined") {
				var value = arguments[0] || "";
				return value.includes(arguments[1]);
			}
			return false;
		}
		function BEGINS_WITH() {
			if (arguments && arguments.length == 2 && typeof arguments[0] != "undefined") {
				var value = arguments[0] || "";
				return value.startsWith(arguments[1]);
			}
			return false;
		}
		function BETWEEN() {
			if (arguments && arguments.length == 3 && typeof arguments[0] != "undefined") {
				var value = parseFloat(arguments[0]);
				var lower = parseFloat(arguments[1]);
				var upper = parseFloat(arguments[2]);
				return !isNaN(value) && !isNaN(upper) && !isNaN(lower) && value <= upper && value >= lower;
			}
			return false;
		}
		function IN() {
			if (arguments && arguments.length > 0 && typeof arguments[0] != "undefined") {
				var value = arguments[0];
				var params = _.takeRight(arguments, arguments.length - 1) || [];
				return params.length > 0 && _.indexOf(params, value) != -1;
			}
			return false;
		}
		function IS_ONE_OF() {
			if (arguments && arguments.length > 0 && typeof arguments[0] != "undefined") {
				var value = arguments[0];
				var params = _.takeRight(arguments, arguments.length - 1) || [];
				return params.length > 0 && _.indexOf(params, value) != -1;
			}
			return false;
		}

		ABR.FACTORY.applicationBlockRulesDialog = function (currentPage, result) {
			var $reasonListWrapper =$("ul[data-place-holder='reason-list']", "#applicationBlockRulesPage");
			$reasonListWrapper.html("");
			_.forEach(result, function(reason) {
				$reasonListWrapper.append($("<li/>").text(reason));
			});
			ABR.DATA.currentPage = currentPage;
			$.mobile.changePage("#applicationBlockRulesPage", {
				transition: "fade",
				reverse: false,
				changeHash: false
			});
		};
		ABR.FACTORY.closeApplicationBlockRulesDialog = function () {
			$.mobile.changePage("#" + ABR.DATA.currentPage, {
				transition: "fade",
				reverse: false,
				changeHash: false
			});
		}
		ABR.FACTORY.cancelApplication = function (e) {
			$.mobile.changePage("#pageApplicationCancelled", {
				transition: "fade",
				reverse: false,
				changeHash: false
			});
			e.preventDefault();
		}
		ABR.FACTORY.evaluateRules = function(currentPage, formValues) {
			var result = [];
			if (ABR.DATA.rules.length > 0) {
				var regExFormValue = /formValues\['([a-zA-Z0-9_-]*)'\]/g;
				for (var i = 0; i < ABR.DATA.rules.length; i++) {
					var rule = ABR.DATA.rules[i];
					if (/formValues\['([a-zA-Z0-9_-]*)'\]/.test(rule.expression)) {
						var m;
						do {
							m = regExFormValue.exec(rule.expression);
							if (m) {
								if (!formValues.hasOwnProperty(m[1])) {
									m = false;
								}
							}
						} while (m);
						if (m != false) {
							var ruleValues = rule.values;
							if (eval(rule.expression) == true)
								result.push(rule.name);		
						}
					}
				}
			}
			if (result.length > 0) {
				ABR.FACTORY.applicationBlockRulesDialog(currentPage, _.uniq(result));
				return true;
			}
			return false;
		}
	}(window.ABR = window.ABR || {}, jQuery));
</script>