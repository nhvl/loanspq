﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="ApplicantInfo.ascx.vb" Inherits="Inc_ApplicantInfo" %>
<%@ Import Namespace="System.Web.Optimization" %>
<%@ Import Namespace="LPQMobile.Utils" %>

<iframe name="<%=IDPrefix%>if_ApplicantInfo" style="display: none;" src="about:blank" ></iframe>
<form target="<%=IDPrefix%>if_ApplicantInfo" action="/handler/AutoFillHandler.aspx" id="<%=IDPrefix%>frm_ApplicantInfo">
<%If LaserScandocAvailable = True Then%>
<div id="<%=IDPrefix%>divLaserScanDocument" style="margin-top: 2px;padding: 10px 0px;">
	<%--<add A tag to make the div focusable (for WCAG/ADA) and set inline-block to make heigh of A 100%--%>
	<a href="#<%=IDPrefix%>scandocs" data-rel="dialog" data-transition="pop" style="display: inline-block;" class="img-btn">
    <div class="scandocs-box laser-scan">
	    <div class="caption-text">
            <p class="caption">Click or tap here to pre-fill information with your driver's license</p>
            <div class="avatar">
                <img src="/images/laser_scan.png" alt="avatar" class="img-responsive"/>
            </div>
			</div>
    </div>
	</a>
</div>
<%ElseIf LegacyScandocAvailable Then%>
<div id="<%=IDPrefix%>divScanDocument" style="margin-top: 2px;padding: 10px 0px;">
	<%--<add A tag to make the div focusable (for WCAG/ADA) and set inline-block to make heigh of A 100%--%>
	<a href="#<%=IDPrefix%>scandocs" data-rel="dialog" data-transition="pop" style="display: inline-block;" class="img-btn">
    <div class="scandocs-box">
	    <div class="caption-text">
            <p class="caption">Click or tap here to pre-fill information with your driver's license</p>
            <div class="pull-right">
                <img src="/images/avatar.jpg" alt="avatar" class="img-responsive"/>
            </div>
			</div>
    </div>
	</a>
</div>
<%End If%>
<div id="<%=IDPrefix%>divEmployeeOfLender">
<%If Not String.IsNullOrEmpty(EmployeeOfLenderDropdown) Then%>
    <div data-role="fieldcontain">
        <label for="<%=IDPrefix%>ddlEmployeeOfLender">Are you an employee of <span class="bold"><%=LenderName%>?</span></label>
        <select id="<%=IDPrefix%>ddlEmployeeOfLender">
	        <%=EmployeeOfLenderDropdown%>
	    </select>
    </div>
<%End If%>
</div>
<div data-role="fieldcontain">
    <label for="<%=IDPrefix%>txtFName" class="RequiredIcon">First Name</label>
    <input type="text" id="<%=IDPrefix%>txtFName" name="fname" autocomplete="given-name" class ="FirstName" maxlength ="50" value="<%=FName %>" <%=FNameDisabledString%> />
</div>
<div data-role="fieldcontain">
    <label for="<%=IDPrefix%>txtMName">Middle Name</label>
    <input type="text" id="<%=IDPrefix%>txtMName" maxlength ="50" name="mname" autocomplete="additional-name" value="<%=MName %>" <%=MNameDisabledString%> />
</div>
<div data-role="fieldcontain">
    <label for="<%=IDPrefix%>txtLName" class="RequiredIcon">Last Name</label>
    <input type="text" id="<%=IDPrefix%>txtLName" maxlength ="50" name="lname" autocomplete="family-name" value="<%=LName %>"  <%=LNameDisabledString%> />
</div>
</form>

<div data-role="fieldcontain">
	<label for="<%=IDPrefix%>suffix">Suffix (Jr., Sr., etc.)</label>
	<select name="suffix" id="<%=IDPrefix%>suffix">
		<%=Common.RenderDropdownlist(GetSuffixList(), "")%>
	</select>
</div>
<div data-role="fieldcontain">
	<div class="row">
		<div class="col-xs-6 no-padding"><label id="<%=IDPrefix%>lblSSN" for="<%=IDPrefix%>txtSSN1">SSN</label></div>
		<div class="col-xs-6 no-padding ui-label-ssn">
			<span class="pull-right header_theme2 ssn-ico" style="padding-left: 4px; padding-right: 2px;display: inline;"><%=HeaderUtils.IconLock16%><span class="sr-only">SSN IconLock</span></span>
			<span id="<%=IDPrefix%>spOpenSsnSastisfy" onclick='openPopup("#<%=IDPrefix%>popSSNSastisfy")' class="pull-right focus-able" style="display: inline;" data-rel='popup' data-position-to="window" data-transition='pop'><span class="pull-right ssn-ico header_theme2"><%=HeaderUtils.IconQuestion16%></span><span class="sr-only">SSN IconQuestion</span></span>
			<%If SSNDisabledString = "" Then%>
			<div style="display: inline;" class="hidden"><label is_show="1"  class="pull-right header_theme2 rename-able shadow-btn focus-able" style="margin-right: 10px; cursor: pointer;outline: none;" onclick="<%=IDPrefix%>toggleSSNText(this);">Hide SSN</label></div>
			<div style="display: inline;"><label is_show="0"  class="pull-right header_theme2 rename-able shadow-btn focus-able" style="margin-right: 10px; cursor: pointer;outline: none;" onclick="<%=IDPrefix%>toggleSSNText(this);">Show SSN</label></div>
        <%End If%>
		</div>
	</div>
    <%--<input type="<%=TextAndroidTel%>" pattern="[0-9]*" id="<%=IDPrefix%>txtSSN" class="inssn"  maxlength ='11' value="<%=SSN %>" <%=SSNDisabledString%> />--%>
    <div id="<%=IDPrefix%>divSSN" class="ui-input-ssn row">
        <div class="col-xs-4">
            <input aria-labelledby="<%=IDPrefix%>lblSSN" type="password" autocomplete="new-password" pattern="[0-9]*" placeholder="---" id="<%=IDPrefix%>txtSSN1" maxlength ='3' next-input="#<%=IDPrefix%>txtSSN2" onkeydown="limitToNumeric(event);" onkeyup="onKeyUp(event,'#<%=IDPrefix%>txtSSN1','#<%=IDPrefix%>txtSSN2', '3');" value="<%=SSN1%>" <%=SSNDisabledString%> />
            <input type="hidden" id="<%=IDPrefix%>hdSSN1" value=""/>
        </div>
        <div class="col-xs-4">
            <input aria-labelledby="<%=IDPrefix%>lblSSN" type="password" autocomplete="new-password" pattern="[0-9]*" placeholder="--" id="<%=IDPrefix%>txtSSN2" maxlength ='2' next-input="#<%=IDPrefix%>txtSSN3" onkeydown="limitToNumeric(event);" onkeyup="onKeyUp(event,'#<%=IDPrefix%>txtSSN2','#<%=IDPrefix%>txtSSN3', '2');" value="<%=SSN2%>" <%=SSNDisabledString%> />
            <input type="hidden" id="<%=IDPrefix%>hdSSN2" value=""/>
        </div>
        <div class="col-xs-4" style="padding-right: 0px;">
            <input aria-labelledby="<%=IDPrefix%>lblSSN" type="<%=TextAndroidTel%>" pattern="[0-9]*" placeholder="----" id="<%=IDPrefix%>txtSSN3" maxlength ='4' onkeydown="limitToNumeric(event);" value="<%=SSN3%>" <%=SSNDisabledString%> />
            <input type="hidden" id="<%=IDPrefix%>txtSSN" class="combine-field-value" value="<%=SSN%>" <%=SSNDisabledString%>  />
        </div>
       
    </div>
</div>
<div id="<%=IDPrefix%>popSSNSastisfy" data-role="popup" style="max-width: 400px;">
    <div data-role="content">
        <div class="row">
            <div class="col-xs-12 header_theme2">
                <a data-rel="back" class="pull-right svg-btn" href="#"><%=HeaderUtils.IconClose %><span style="display: none;">close</span></a>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12">
                <div style="margin: 10px 0; font-weight: normal;">
                    Your Social Security Number (SSN) is used for identification purposes and to determine your account opening eligibility.
                </div>     
            </div>    
        </div>
    </div>
</div>
<div data-role="fieldcontain" id="<%=IDPrefix%>divDOBStuff">
	<div class="row">
		<div class="col-xs-8 no-padding"><label id="<%=IDPrefix%>lblDOB" for="<%=IDPrefix%>txtDOB" class="dob-label RequiredIcon">Date of Birth</label></div>
		<div class="col-xs-4 no-padding"><input class="pull-right" type="hidden" id="<%=IDPrefix%>txtDOB" class="combine-field-value" value="<%=DOB %>" <%=DOBDisabledString%>  /></div>
	</div>
    <%--<input type="<%=TextAndroidTel%>" pattern="[0-9]*" id="<%=IDPrefix%>txtDOB" class="indate" maxlength ='10' value="<%=DOB %>"  <%=DOBDisabledString%> />--%>
    <div id="<%=IDPrefix%>divDOB" class="ui-input-date row">
        <div class="col-xs-4">
            <input aria-labelledby="<%=IDPrefix%>lblDOB" type="<%=TextAndroidTel%>" pattern="[0-9]*" placeholder="mm" id="<%=IDPrefix%>txtDOB1" maxlength ='2' onkeydown="limitToNumeric(event);" onkeyup="onKeyUp(event,'#<%=IDPrefix%>txtDOB1','#<%=IDPrefix%>txtDOB2', '2');" value="<%=DOBMonth%>" <%=DOBDisabledString%> />    
        </div>
        <div class="col-xs-4">
            <input aria-labelledby="<%=IDPrefix%>lblDOB" type="<%=TextAndroidTel%>" pattern="[0-9]*" placeholder="dd" id="<%=IDPrefix%>txtDOB2" maxlength ='2' onkeydown="limitToNumeric(event);" onkeyup="onKeyUp(event,'#<%=IDPrefix%>txtDOB2','#<%=IDPrefix%>txtDOB3', '2');" value="<%=DOBDay%>" <%=DOBDisabledString%> />
        </div>
        <div class="col-xs-4" style="padding-right: 0px;">
            <input aria-labelledby="<%=IDPrefix%>lblDOB" type="<%=TextAndroidTel%>" pattern="[0-9]*" placeholder="yyyy" id="<%=IDPrefix%>txtDOB3" maxlength ='4' onkeydown="limitToNumeric(event);" value="<%=DOBYear%>" <%=DOBDisabledString%> />
        </div>
    </div>
</div>
<%--
	Note: The Member Number field is not shown on Combo apps, because if it's a Combo then the consumer is signing up for
	a new membership, and thus would not have an existing member number to input.
	
	The Member Number field is not shown for Banks, because bank customers do not have one central account/identifier. Instead,
	they may have several account numbers (one for checking, one for savings, etc) and would be confused as to which number to put
	in. For this reason, we do not show it.
	--%>
<%If Not isComboMode And InstitutionType <> CEnum.InstitutionType.BANK Then%>
    <%If EnableMemberNumber Then%> 
        <div id="<%=IDPrefix%>divMemberNumber" <%=IIf(IsInMode("777") AndAlso IsInFeature("visibility"), "class=""showfield-section"" data-show-field-section-id=""" & BuildShowFieldSectionID("divMemberNumber") & """ data-default-state=""on"" data-section-name= '" & IIf(String.IsNullOrEmpty(IDPrefix), "", "Co-App") & IIf(InstitutionType = CEnum.InstitutionType.BANK, " Account Number'", " Member Number'"), "")%>>
	        <div data-role="fieldcontain">
		        <label for="<%=IDPrefix%>txtMemberNumber" <%=IIf(MemberNumberRequired = "Y", " class='RequiredIcon'", "")%>><%:IIf(InstitutionType = CEnum.InstitutionType.BANK, "Account Number", "Member Number")%></label>
		        <input type="text"  id="<%=IDPrefix%>txtMemberNumber" maxlength ="50" name="membernumber" autocomplete="on" value="<%=MemberNumber %>" <%=MemberNumberDisabledString%>/>
	        </div>
        </div>
    <%End If %>
<% End If%>
<%If IDPrefix = "co_" AndAlso Not String.IsNullOrEmpty(RelationshipToPrimary) Then%>
<div data-role="fieldcontain">
    <label for="<%=IDPrefix%>ddlRelationshipToPrimary" class="RequiredIcon">Relationship to Primary Applicant</label>
    <select id="<%=IDPrefix%>ddlRelationshipToPrimary">
	    <%=RelationshipToPrimary%>
	</select>
</div>
<%End If%>
<%If EnableMembershipLength Then%>
<div id="<%=IDPrefix%>divMembershipLength" <%=IIf(IsInMode("777") AndAlso IsInFeature("visibility"), "class=""showfield-section"" data-show-field-section-id=""" & BuildShowFieldSectionID("divMembershipLength") & """ data-default-state=""off"" data-section-name= '" & IIf(String.IsNullOrEmpty(IDPrefix), "", "Co-App") & IIf(InstitutionType = CEnum.InstitutionType.BANK, " Account Length'", " Membership Length'"), "")%>>
<div data-role="fieldcontain">
        <label id="<%=IDPrefix%>lblMembershipLength"><%:IIf(InstitutionType = CEnum.InstitutionType.BANK, "Account Length", "Membership Length")%></label>
        <div class="ui-input-horizal row">
            <div class="col-xs-6">
                <select id="<%=IDPrefix%>ddlMembershipLengthYear" aria-labelledby="<%=IDPrefix%>lblMembershipLength">
                    <option value=" ">Years</option>
                    <%=Common.GetYears()%>
                </select>
            </div>
            <div class="col-xs-6">
                <select id="<%=IDPrefix%>ddlMembershipLengthMonth" aria-labelledby="<%=IDPrefix%>lblMembershipLength">
                    <option value=" ">Months</option>
                    <%=Common.GetMonths()%>
                </select>
            </div>
        </div>
    </div>
</div>
<%End If%>
<%If isComboMode And HasMotherName Then%>
<div data-role="fieldcontain" id="div_TxtMotherMaidenName">
    <label for="<%=IDPrefix%>txtMotherMaidenName" class="RequiredIcon">Mother's Maiden Name</label>
	<input id="<%=IDPrefix%>txtMotherMaidenName" name="mothermaidenname" autocomplete="name" type="text" maxlength ="20"/>
</div>
<%End If%>
<%If EnableCitizenshipStatus Then%>
<div id="<%=IDPrefix%>divCitizenshipStatus" <%=IIf(IsInMode("777") AndAlso IsInFeature("visibility"), "class=""showfield-section"" data-show-field-section-id=""" & BuildShowFieldSectionID("divCitizenshipStatus") & """ data-section-name= '" & IIf(String.IsNullOrEmpty(IDPrefix), "", IIf(IDPrefix = "co_", "Co-App ", "Minor ")) & " Citizenship Status'", "")%>>
<div data-role="fieldcontain">
    <label for="<%=IDPrefix%>ddlCitizenshipStatus" class="RequiredIcon">Citizenship Status</label>
    <select id="<%=IDPrefix%>ddlCitizenshipStatus">
        <%=CitizenshipStatusDropdown %>
    </select>
</div>
</div>
<%End If%>
<%--Marital status --%>   
<%If EnableMaritalStatus Then%>
<div id="<%=IDPrefix%>divMaritalStatus" <%=IIf(IsInMode("777") AndAlso IsInFeature("visibility"), "class=""showfield-section"" data-show-field-section-id=""" & BuildShowFieldSectionID("divMaritalStatus") & """ data-default-state=""off"" data-section-name= '" & IIf(String.IsNullOrEmpty(IDPrefix), "", "Co-App") & " Marital Status'", "")%>>
<div data-role="fieldcontain">
    <label for="<%=IDPrefix%>ddlMaritalStatus">Marital Status</label>
    <select id="<%=IDPrefix%>ddlMaritalStatus">
        <%=MaritalStatusDropdown%>
    </select>
</div>
</div>
<%End If%>

<input type="hidden" id="<%=IDPrefix%>hdDLNumber" />
<input type="hidden" id="<%=IDPrefix%>hdDLIssueDate" />
<input type="hidden" id="<%=IDPrefix%>hdDLExpirationDate" />
<input type="hidden" id="<%=IDPrefix%>hdDLState" />
<input type="hidden" id="<%=IDPrefix%>hdDLName" />
<input type="hidden" id="<%=IDPrefix%>hdDLDOB" />

<script type="text/javascript">
	var <%=IDPrefix%>xbarcodePicker;
	$(function () {
		if (isMobile.any() && !isMobile.Windows()) {//moz-text-security does not works on window devices
			$("#<%=IDPrefix%>txtSSN1,#<%=IDPrefix%>txtSSN2")
				.attr("type", "tel")
				.addClass("mask-password");
		} else {
			$("#<%=IDPrefix%>txtSSN1,#<%=IDPrefix%>txtSSN2")
				.attr("type", "password");
		}

        if ($('#hdForeignAppType').val()=='Y') {
        	$("#<%=IDPrefix%>lblSSN").removeClass("RequiredIcon"); // no required ->remove red stard
        } else {
        	$("#<%=IDPrefix%>lblSSN").addClass("RequiredIcon"); // required -> add restar
        }
		<%--if (is_foreign_phone) {

			loadJS('<%:Scripts.Url("~/js/thirdparty/intlTelInput/script")%>', function () {
        		// international country code
        		$('#<%=IDPrefix%>txtHomePhone').intlTelInput("setCountry", "us");
				$('#<%=IDPrefix%>txtMobilePhone').intlTelInput("setCountry", "us");
				$('#<%=IDPrefix%>txtWorkPhone').intlTelInput("setCountry", "us");
            });

            handleForeignContact();
        }--%>
		$("#<%=IDPrefix%>txtDOB").datepicker({
			<%=IIf(DOBDisabledString = "", "showOn: 'button',", "showOn: '',")%>
            changeMonth: true,
            changeYear: true,
            yearRange: "-100:+0",
            buttonImage: "../images/calendar.png",
            buttonImageOnly: true,
            onSelect: function (value, inst) {
                var date = $(this).datepicker('getDate');

                $("#<%=IDPrefix%>txtDOB2").val(padLeft(date.getDate(), 2));
                $("#<%=IDPrefix%>txtDOB1").val(padLeft(date.getMonth() + 1, 2));
                $("#<%=IDPrefix%>txtDOB3").val(padLeft(date.getFullYear(), 4));

            	$("#<%=IDPrefix%>txtDOB").datepicker("hide");
            	$("#<%=IDPrefix%>txtDOB1").trigger("focus").trigger("blur");
            }
        });
        $("#<%=IDPrefix%>popSSNSastisfy").on("popupafteropen", function (event, ui) {
            //prevent scrollbar by popup overlay
            $("#" + this.id + "-screen").height("");
        });
		$("#<%=IDPrefix%>popSSNSastisfy").on("popupafterclose", function (event, ui) {
			setTimeout(function () {
				$("#<%=IDPrefix%>spOpenSsnSastisfy").focus();
			}, 100);
		});
        $('#<%=IDPrefix%>divDOBStuff div.ui-input-date input').on('focusout', function () {
            var maxlength = parseInt($(this).attr('maxlength'));
            // add 0 if value < 10
            $(this).val(padLeft($(this).val(), maxlength));
           <%=IDPrefix%>updateHiddenDOB();         
        });
       
		<%--//Hide divScanDocument if it 's not available
        if ('<%=ScandocAvaiable%>' !='True') {
            $('#<%=IDPrefix%>divScanDocument').hide();
        }--%>		
		$("#<%=IDPrefix%>scandocs").on("pageshow", function () {
			if ($("#<%=IDPrefix%>scandit-barcode-picker").length > 0 && $("#<%=IDPrefix%>scandocs").data("no-camera") != true ) {
				ScanditSDK.CameraAccess.getCameras().then(function (cameraList) {
					if (cameraList.length > 0) {
						<%=IDPrefix%>initLaserScan();
					} else {
						<%=IDPrefix%>loadLegacyScan();
					}
					return null;
				}).catch(function (ex) {
					//console.log(ex);
					if (ex.name == "NotAllowedError") {
						$("#<%=IDPrefix%>scandocs .js-laser-scan-container").find("a[data-rel='back']").trigger("click");
					} else {
						$("#<%=IDPrefix%>scandocs").data("no-camera", true);
						<%=IDPrefix%>loadLegacyScan();	
					}
					return null;
				});
			} else {
				<%=IDPrefix%>loadLegacyScan();
			}
			$("#<%=IDPrefix%>scandocs a[data-rel='back']").focus();
		});
		$("#<%=IDPrefix%>scandocs").on("pagehide", function () {
			if ($("#<%=IDPrefix%>scandit-barcode-picker").length > 0 && <%=IDPrefix%>xbarcodePicker) {
				$(".divScanDocs.laser-scan", "#<%=IDPrefix%>scandocs").removeClass("open");
				<%=IDPrefix%>xbarcodePicker.destroy();

			}
			$('a[href="#<%=IDPrefix%>scandocs"]').focus();
        });
      
	});
	function <%=IDPrefix%>initLaserScan() {
		ScanditSDK.BarcodePicker.create(document.getElementById("<%=IDPrefix%>scandit-barcode-picker"), {
			playSoundOnScan: true,
			vibrateOnScan: true
		}).then(function (barcodePicker) {
			$("#<%=IDPrefix%>scandocs").find(".scan-guide-text").removeClass("hidden");
			<%=IDPrefix%>xbarcodePicker = barcodePicker;
			// barcodePicker is ready here to be used
			var scanSettings = new ScanditSDK.ScanSettings({
				enabledSymbologies: ["pdf417"],
				codeDuplicateFilter: 1000
			});
			barcodePicker.applyScanSettings(scanSettings);
			$(".scandit-camera-switcher, .scandit-flash-white, .scandit-flash-color", ".scandit-barcode-picker ").addClass("hidden");
			barcodePicker.onScan(function (scanResult) {
				scanResult.barcodes.reduce(function (string, barcode) {
					var result = analyzeData(barcode.data);
					var $scanResultList = $(".scan-result-list", "#<%=IDPrefix%>scandocs");
					if (result.FirstName) {
						$scanResultList.find(".js-prop-name").text(result.FirstName + ($.trim(result.MiddleName).length > 0 ? " " + result.MiddleName : "") + " " + result.LastName);
					}
					$scanResultList.find(".js-prop-dob").text(result.DOB);

					$scanResultList.find(".js-prop-issued-date").text(result.LicenseIssuedDate);

					$scanResultList.find(".js-prop-expired-date").text(result.LicenseExpiredDate);
					var sex = "Unknown";
					if (result.Sex == "1") {
						sex = "Male";
					} else if (result.Sex == "2") {
						sex = "Female";
					}
					$scanResultList.find(".js-prop-sex").text(sex);
					$scanResultList.find(".js-prop-license-number").text(result.LicenseNumber);
					var address = "";
					address += "<p>" + result.MailingAddress1 + "</p>";
					address += "<p>" + result.MailingCity + " " + result.MailingState + ", " + result.MailingZip + "</p>";
					$scanResultList.find(".js-prop-address").html(address);
					$(".divScanDocs.laser-scan", "#<%=IDPrefix%>scandocs").addClass("open");
					$(".scan-result-wrapper", "#<%=IDPrefix%>scandocs").data("scan-result", result);
				}, "");
			});
			return null;
		});
	}
	function <%=IDPrefix%>loadLegacyScan() {
		$("#<%=IDPrefix%>scandocs").find(".js-laser-scan-container").addClass("hidden");
		$("#<%=IDPrefix%>scandocs").find(".js-legacy-scan-container").removeClass("hidden");
	}
	function <%=IDPrefix%>updateHiddenSSN() {
		if ($("#<%=IDPrefix%>txtSSN1").val().length == 3) {
    		var ssn = $("#<%=IDPrefix%>txtSSN1").val() + '-' + $("#<%=IDPrefix%>txtSSN2").val() + '-' + $("#<%=IDPrefix%>txtSSN3").val();
		  	$("#<%=IDPrefix%>txtSSN").val(ssn);
    	}
    }
    function <%=IDPrefix%>updateHiddenDOB() {
        var month = padLeft($("#<%=IDPrefix%>txtDOB1").val(), 2);
        var day = padLeft($("#<%=IDPrefix%>txtDOB2").val(), 2);
    	var year = padLeft($("#<%=IDPrefix%>txtDOB3").val(), 4);
		if (month != "" && day != "" && year != "") {
			$("#<%=IDPrefix%>txtDOB").val(month + '/' + day + '/' + year);
		} else {
			$("#<%=IDPrefix%>txtDOB").val("");
		}
    }

	function <%=IDPrefix%>toggleSSNText(btn) {
        return Common.ToggleSSNText(btn, "<%=IDPrefix%>");
    }
	//obsoleted
    //function handleForeignContact() {
    //    // change the text
    //    $('.phone_format').html("");

    //    // remove mask
    //    if (!isMobile.Android()) {
    //        $('.inphone').inputmask('remove');
    //    } else {
    //        $('input.inphone').off('blur').off('keyup').off('keydown');
    //    }
    //    $('input.inphone').numeric();
    //}
     
    function <%=IDPrefix%>hasHiddenDriverLicense() {
        var hdName = $('#<%=IDPrefix%>hdDLName').val().trim();
        var hdDOB = $('#<%=IDPrefix%>hdDLDOB').val().trim();
        var Name =$('#<%=IDPrefix%>txtFName').val().trim()+$('#<%=IDPrefix%>txtLName').val().trim();
        var DOB = $('#<%=IDPrefix%>txtDOB').val().trim();
        if (hdName.toUpperCase() == Name.toUpperCase() && hdDOB==DOB) {
            return true;
        }
        return false;
    }
    
    function <%=IDPrefix%>ValidateApplicantInfo() {
       if ($.lpqValidate("<%=IDPrefix%>ValidateApplicantInfo")) {
    		document.getElementById("<%=IDPrefix%>frm_ApplicantInfo").submit();
			return true;
		}
		return false;
    }
	function <%=IDPrefix%>registerApplicantInfoValidator() {
		$('#<%=IDPrefix%>txtFName').observer({
			validators: [
				function (partial) {
					var text = $(this).val();
					if (!Common.ValidateText(text)) {
						return 'First Name is required';
					} else if (/^[\sa-zA-Z\'-]+$/.test(text) == false) {
						return 'Enter a valid first name. Allowed characters: letters "A-Z", dashes, apostrophes and spaces.';
					}
					return "";
				}
			],
			validateOnBlur: true,
			group: "<%=IDPrefix%>ValidateApplicantInfo"
		});
		$('#<%=IDPrefix%>txtLName').observer({
			validators: [
				function (partial) {
					var text = $(this).val();
					if (!Common.ValidateText(text)) {
						return 'Last Name is required';
					} else if (/^[\sa-zA-Z\'-]+$/.test(text) == false) {
						return 'Enter a valid last name. Allowed characters: letters "A-Z", dashes, apostrophes and spaces.';
					}
					return "";
				}
			],
			validateOnBlur: true,
			group: "<%=IDPrefix%>ValidateApplicantInfo"
		});
		$('#<%=IDPrefix%>txtMName').observer({
			validators: [
				function (partial) {
					var text = $(this).val();
					if (/^[\sa-zA-Z\'-]*$/.test(text) == false) {
						return 'Enter a valid middle name. Allowed characters: letters "A-Z", dashes, apostrophes and spaces.';
					}
					return "";
				}
			],
			validateOnBlur: true,
			group: "<%=IDPrefix%>ValidateApplicantInfo"
		});
		$('#<%=IDPrefix%>divSSN').observer({
			validators: [
				function (partial, evt) {
                     <%=IDPrefix%>updateHiddenSSN();
					//var ssn = Common.GetSSN($('#<%=IDPrefix%>txtSSN').val());
					var ssn = $('#<%=IDPrefix%>txtSSN1').val() + $('#<%=IDPrefix%>txtSSN2').val() + $('#<%=IDPrefix%>txtSSN3').val();
					
					if ($('#hdForeignAppType').val() == 'Y' && ssn === "") { //make ssn is not required if hasForeignAppType 
						return "";
					}
					if (/^[0-9]{9}$/.test(ssn) == false) {
						return 'Valid SSN is required';
					}
					<%If IDPrefix = "" Then%>
					if (ssn == Common.GetSSN($('#co_txtSSN').val())) {
						return "SSN shouldn't be the same as Joint Applicant";
					}
					<%Else%>
					if (ssn == Common.GetSSN($('#txtSSN').val())) {
						return "SSN shouldn't be the same as Primary Applicant";
					}
					<%End If%>
					return "";
				}
			],
			validateOnBlur: true,
			group: "<%=IDPrefix%>ValidateApplicantInfo"
		});
		
		$('#<%=IDPrefix%>divDOB').observer({
			validators: [
				function (partial) {
                    <%=IDPrefix%>updateHiddenDOB();
					var $DOB = $('#<%=IDPrefix%>txtDOB');

					if ($DOB.val().trim() == "") {
						return "Date of Birth is required";
					}

					var dob = moment($DOB.val(), "MM-DD-YYYY");
					if (!Common.IsValidDate($DOB.val())) {
						return "Valid Date of Birth is required";
					}
					if (dob.year() < 1900) {
						return "Date of Birth is too old";
					}
					if (dob.isAfter(moment())) {
						return "Date of Birth must be equal or less than current date";
					}
					var blockUnderAge = <%=_blockApplicantAgeUnder%>;
					<%If LoanType = "HE" then %>
					var loanPurpose = $('#hdLoanPurpose').val();
					if (loanPurpose != undefined && (loanPurpose.toUpperCase().indexOf("LINE OF CREDIT") != -1 || loanPurpose.toUpperCase().indexOf("HELOC")) != -1) {
						blockUnderAge = <%=_blockApplicantAgeUnderHELOC%>;
					}
					<%End If%>
					var age = moment().diff(dob, "years");
					if (blockUnderAge > 0) {
						if (age < blockUnderAge) {
							return "The applicant age must be at least " + blockUnderAge;
						}
					} else {
						<%If IDPrefix = "" Then%>
						if (age < 18) {
					    	return "The applicant age must be at least 18";
					    }
					    <%End If%>
					}

				    return "";
				}
			],
			validateOnBlur: true,
			container: '#<%=IDPrefix%>divDOB',
			group: "<%=IDPrefix%>ValidateApplicantInfo"
		});
		$('#<%=IDPrefix%>ddlCitizenshipStatus').observer({
			validators: [
				function (partial) {
					if ($(this).closest("div.showfield-section").length > 0 && $(this).closest("div.showfield-section").hasClass("hidden")) return "";
					if (!Common.ValidateText($(this).val())) {
						return 'Citizenship Status is required';
					}
					return "";
				}
			],
			validateOnBlur: true,
			group: "<%=IDPrefix%>ValidateApplicantInfo"
		});
		<%If IDPrefix = "co_" AndAlso Not String.IsNullOrEmpty(RelationshipToPrimary) Then%>
		$('#<%=IDPrefix%>ddlRelationshipToPrimary').observer({
			validators: [
				function (partial) {
					if (!Common.ValidateText($(this).val())) {
						return 'Relationship to Primary Applicant is required';
					}
					return "";
				}
			],
			validateOnBlur: true,
			group: "<%=IDPrefix%>ValidateApplicantInfo"
		});
		<%End If%>
		
		<%If isComboMode And HasMotherName Then%>
		$('#<%=IDPrefix%>txtMotherMaidenName').observer({
			validators: [
                function (partial) {
                    var text = $(this).val();
                    if (!Common.ValidateText(text)) {
                        return 'Mother Maiden Name is required';
                    }else if (/^[\sa-zA-Z\'-]*$/.test(text) == false) {
						return 'Enter a valid mother maiden name. Allowed characters: letters "A-Z", dashes, apostrophes and spaces.'			
                    }
					return "";
				}
			],
			validateOnBlur: true,
			group: "<%=IDPrefix%>ValidateApplicantInfo"
		});
		<%End If%>
		<%If Not isComboMode And MemberNumberRequired = "Y" And InstitutionType <> CEnum.InstitutionType.BANK Then%>
             <% If EnableMemberNumber Then %>
		        $('#<%=IDPrefix%>txtMemberNumber').observer({
			        validators: [
				        function (partial) {
					        if (!Common.ValidateText($(this).val())) {
						        return '<%:IIf(InstitutionType = CEnum.InstitutionType.BANK, "Account Number", "Member Number")%> is required';
					        }
					        return "";
				        }
			        ],
			        validateOnBlur: true,
			        group: "<%=IDPrefix%>ValidateApplicantInfo"
                });
            <%End If%>
		<%End If%>
		
	}
	<%=IDPrefix%>registerApplicantInfoValidator();



	function <%=IDPrefix%>SetApplicantInfo(appInfo) {
        appInfo.<%=IDPrefix%>FirstName = $("#<%=IDPrefix%>txtFName").val();
        appInfo.<%=IDPrefix%>MiddleName = $("#<%=IDPrefix%>txtMName").val();
        appInfo.<%=IDPrefix%>LastName = $("#<%=IDPrefix%>txtLName").val();
    	appInfo.<%=IDPrefix%>NameSuffix = $('#<%=IDPrefix%>suffix option:selected').val();

        if ($('#hdForeignAppType').val() == 'Y') {
            var txtSSN = Common.GetSSN($("#<%=IDPrefix%>txtSSN").val());
            if (txtSSN == ""){
                var ssn = Common.GetSSN("999-99-9999");
                if("<%=IDPrefix%>" =='co_'){
                    ssn = Common.GetSSN("999-99-9998");
                }
                appInfo.<%=IDPrefix%>SSN = ssn;

            } else {
                appInfo.<%=IDPrefix%>SSN = Common.GetSSN($("#<%=IDPrefix%>txtSSN").val());
            }

        } else {
        	appInfo.<%=IDPrefix%>SSN = Common.GetSSN($("#<%=IDPrefix%>txtSSN").val());

			//debug pupose
        	<%--if (appInfo.<%=IDPrefix%>SSN != undefined && appInfo.<%=IDPrefix%>SSN.length != 9) {
        		logInfo("SSN extract" + appInfo.<%=IDPrefix%>SSN)
        		logInfo("SSN org" + $("#<%=IDPrefix%>txtSSN").val())
        		logInfo("SSN1" + $("#<%=IDPrefix%>txtSSN1").val())
        		logInfo("SSN2" + $("#<%=IDPrefix%>txtSSN2").val())
        		logInfo("SSN3" + $("#<%=IDPrefix%>txtSSN3").val())
        	}--%>

        }
    	appInfo.<%=IDPrefix%>DOB = $("#<%=IDPrefix%>txtDOB").val(); 	
        <%If EnableMemberNumber Then%>
            if ($('#<%=IDPrefix%>txtMemberNumber').length === 1) {
                if ($('#<%=IDPrefix%>txtMemberNumber').closest("div.showfield-section").length == 0 || $('#<%=IDPrefix%>txtMemberNumber').closest("div.showfield-section").hasClass("hidden") == false) {
                    appInfo.<%=IDPrefix%>MemberNumber = $('#<%=IDPrefix%>txtMemberNumber').val();
                }
            }
		<%End If%>
		//appInfo.<%=IDPrefix%>MaritalStatus = $('#<%=IDPrefix%>ddlMaritalStatus option:selected').val();

		<%If EnableCitizenshipStatus Then%>
		if ($('#<%=IDPrefix%>ddlCitizenshipStatus').closest("div.showfield-section").length == 0 || $('#<%=IDPrefix%>ddlCitizenshipStatus').closest("div.showfield-section").hasClass("hidden") == false) {
			appInfo.<%=IDPrefix%>CitizenshipStatus = $('#<%=IDPrefix%>ddlCitizenshipStatus option:selected').val();
		}
		<%End If%>
		
        //get hidden driver license if it exist
        var hasDriverLicense = <%=IDPrefix%>hasHiddenDriverLicense();
        if (hasDriverLicense) {
            appInfo.<%=IDPrefix%>dlNumber = $('#<%=IDPrefix%>hdDLNumber').val();
            appInfo.<%=IDPrefix%>dlIssuedDate = $('#<%=IDPrefix%>hdDLIssueDate').val();
            appInfo.<%=IDPrefix%>dlExpirationDate = $('#<%=IDPrefix%>hdDLExpirationDate').val();
            appInfo.<%=IDPrefix%>dlState = $('#<%=IDPrefix%>hdDLState').val();
        }
        //get employee of lender
        var emp_lender="NONE"; 
        var ddlEmployeeOfLender = $('#<%=IDPrefix%>ddlEmployeeOfLender option:selected').val();
        if (ddlEmployeeOfLender != undefined) //make sure it has employee of lender dropdown list
        {
            if (ddlEmployeeOfLender != "" && ddlEmployeeOfLender != "NONE") {
                emp_lender = ddlEmployeeOfLender;
            }
        }
        appInfo.<%=IDPrefix%>MotherMaidenName = "";
		<%If isComboMode And HasMotherName then %>
		appInfo.<%=IDPrefix%>MotherMaidenName = $("#<%=IDPrefix%>txtMotherMaidenName").val();
		<%End If %>
		appInfo.<%=IDPrefix%>EmployeeOfLender = emp_lender;

		<%If EnableMaritalStatus Then%>
		if ($('#<%=IDPrefix%>ddlMaritalStatus').closest("div.showfield-section").length == 0 || $('#<%=IDPrefix%>ddlMaritalStatus').closest("div.showfield-section").hasClass("hidden") == false) {
			appInfo.<%=IDPrefix%>MaritalStatus = $('#<%=IDPrefix%>ddlMaritalStatus option:selected').val();
    	}
		<%End If%>

		<%If EnableMemberShipLength Then%>
		if ($('#<%=IDPrefix%>ddlMembershipLengthYear').closest("div.showfield-section").length == 0 || $('#<%=IDPrefix%>ddlOccupyingStatus').closest("div.showfield-section").hasClass("hidden") == false) {
			var year = $("#<%=IDPrefix%>ddlMembershipLengthYear").val();
			var month = $("#<%=IDPrefix%>ddlMembershipLengthMonth").val();
			var x = $.trim(year) == '' ? 0 : parseInt(year);
			var y = $.trim(month) == '' ? 0 : parseInt(month);
			appInfo.<%=IDPrefix%>MembershipLengthMonths = x * 12 + y;
		}
		<%End If%>
		<%If IDPrefix = "co_" AndAlso Not String.IsNullOrEmpty(RelationshipToPrimary) Then%>
		appInfo.<%=IDPrefix%>RelationshipToPrimary = $('#<%=IDPrefix%>ddlRelationshipToPrimary option:selected').val();
		<%End If%>
    }
    function <%=IDPrefix%>autoFillData_PersonalInfor()
    {
        $('#<%=IDPrefix%>txtFName').val("Marisol");
        $('#<%=IDPrefix%>txtLName').val("Testcase");
    	$('#<%=IDPrefix%>txtDOB').val("11/11/1990");
    	$('#<%=IDPrefix%>txtDOB1').val("11");
    	$('#<%=IDPrefix%>txtDOB2').val("11");
        $('#<%=IDPrefix%>txtDOB3').val("1990");
        <% If EnableMemberNumber Then %>
            if ($('#<%=IDPrefix%>txtMemberNumber').length === 1) {
                $('#<%=IDPrefix%>txtMemberNumber').val("testing1111");
            }
        <%End If%>
      
    	$('#<%=IDPrefix%>txtSSN').val("000-00-0001");
    	$('#<%=IDPrefix%>txtSSN1').val("000");
    	$('#<%=IDPrefix%>txtSSN2').val("00");
    	$('#<%=IDPrefix%>txtSSN3').val("0001");
        if('<%=IDPrefix%>' =="co_")
        {
        	$('#<%=IDPrefix%>txtSSN').val("000-00-0002");
        	$('#<%=IDPrefix%>txtSSN1').val("000");
        	$('#<%=IDPrefix%>txtSSN2').val("00");
        	$('#<%=IDPrefix%>txtSSN3').val("0002");
        	$('#<%=IDPrefix%>txtDOB').val("08/22/1980");
        	$('#<%=IDPrefix%>txtDOB1').val("08");
        	$('#<%=IDPrefix%>txtDOB2').val("22");
        	$('#<%=IDPrefix%>txtDOB3').val("1980");
            $('#<%=IDPrefix%>txtFName').val("David");
        }
    	$("#<%=IDPrefix%>txtMotherMaidenName").val("cindy");
    	$('#<%=IDPrefix%>ddlMaritalStatus option').eq(1).prop('selected', true);
    	<%If IDPrefix = "co_" AndAlso Not String.IsNullOrEmpty(RelationshipToPrimary) Then%>
    	$('#<%=IDPrefix%>ddlRelationshipToPrimary option').eq(1).prop('selected', true);
		<%End If%>
    }
    

    function <%=IDPrefix%>ViewAccountInfo() {
        var strHtml = "";
        var txtMemberNumber = htmlEncode($('#<%=IDPrefix%>txtMemberNumber').val());
        var txtFName = htmlEncode($('#<%=IDPrefix%>txtFName').val());
        var txtMName = htmlEncode($('#<%=IDPrefix%>txtMName').val());
        var txtLName = htmlEncode($('#<%=IDPrefix%>txtLName').val());
        var ddlSuffix = htmlEncode($('#<%=IDPrefix%>suffix option:selected').val());
        var txtSSN = Common.GetSSN($('#<%=IDPrefix%>txtSSN').val());
        var txtDOB = $('#<%=IDPrefix%>txtDOB').val();
       
    	var ddlEmployeeOfLender = $('#<%=IDPrefix%>ddlEmployeeOfLender option:selected').val();
        if (ddlEmployeeOfLender != undefined) {
            if (ddlEmployeeOfLender != "") {
                strHtml += '<div class="col-sm-6 col-xs-12"><div class="row"><div class="col-xs-6 text-right row-title"><span class="bold">Employee of Lender</span></div><div class="col-xs-6 text-left row-data"><span>' + $('#<%=IDPrefix%>ddlEmployeeOfLender option:selected').text() + '</span></div></div></div>';
            }
        }
    	var strFullName = txtFName.trim() + (txtMName.trim() != "" ? " " + txtMName + " " : " ") + txtLName.trim();
		if ($.trim(strFullName) !== "") {
			strHtml += '<div class="col-sm-6 col-xs-12"><div class="row"><div class="col-xs-6 text-right row-title"><span class="bold">Full Name</span></div><div class="col-xs-6 text-left row-data"><span>' + strFullName + '</span></div></div></div>';
		}
        
        if (ddlSuffix != '') {
            strHtml += '<div class="col-sm-6 col-xs-12"><div class="row"><div class="col-xs-6 text-right row-title"><span class="bold">Suffix</span></div><div class="col-xs-6 text-left row-data"><span>' + ddlSuffix + '</span></div></div></div>';
        }
        if (txtSSN != "") {
            strHtml += '<div class="col-sm-6 col-xs-12"><div class="row"><div class="col-xs-6 text-right row-title"><span class="bold">SSN</span></div><div class="col-xs-6 text-left row-data"><span>' + txtSSN.replace(/^\d{5}/, "*****") + '</span></div></div></div>';
        }
        <% If EnableMemberNumber Then %>
            if ($('#<%=IDPrefix%>txtMemberNumber').length === 1) {
                if ($('#<%=IDPrefix%>txtMemberNumber').closest("div.showfield-section").length == 0 || $('#<%=IDPrefix%>txtMemberNumber').closest("div.showfield-section").hasClass("hidden") == false) {
                    if ($('#<%=IDPrefix%>txtMemberNumber').length === 1 && txtMemberNumber != "") {
                        strHtml += '<div class="col-sm-6 col-xs-12"><div class="row"><div class="col-xs-6 text-right row-title"><span class="bold"><%:IIf(InstitutionType = CEnum.InstitutionType.BANK, "Account Number", "Member Number")%></span></div><div class="col-xs-6 text-left row-data"><span>' + txtMemberNumber + '</span></div></div></div>';
                    }
                }
            }
        <%End If %>
    	<%If IDPrefix = "co_" AndAlso Not String.IsNullOrEmpty(RelationshipToPrimary) Then%>
    	strHtml += '<div class="col-sm-6 col-xs-12"><div class="row"><div class="col-xs-6 text-right row-title"><span class="bold">Relationship to Primary Applicant</span></div><div class="col-xs-6 text-left row-data"><span>' + $('#<%=IDPrefix%>ddlRelationshipToPrimary option:selected').text() + '</span></div></div></div>';
		<%End If%>
		if ($.trim(txtDOB) !== "") {
			strHtml += '<div class="col-sm-6 col-xs-12"><div class="row"><div class="col-xs-6 text-right row-title"><span class="bold">Date of Birth</span></div><div class="col-xs-6 text-left row-data"><span>' + txtDOB + '</span></div></div></div>';
		}
        
    	<%If isComboMode And HasMotherName Then %>
    	var txtMotherMaidenName = htmlEncode($('#<%=IDPrefix%>txtMotherMaidenName').val());
    	if (txtMotherMaidenName != "") {
    		strHtml += '<div class="col-sm-6 col-xs-12"><div class="row"><div class="col-xs-6 text-right row-title"><span class="bold">MMN</span></div><div class="col-xs-6 text-left row-data masking-text"><span>' + txtMotherMaidenName + '</span><span onclick="toggleReveal(this)"></span></div></div></div>';
    	}
		<%End If%>            

		<%If EnableCitizenshipStatus Then%>
    	if ($('#<%=IDPrefix%>ddlCitizenshipStatus').closest("div.showfield-section").length == 0 || $('#<%=IDPrefix%>ddlCitizenshipStatus').closest("div.showfield-section").hasClass("hidden") == false) {
    		if ($.trim($('#<%=IDPrefix%>ddlCitizenshipStatus').val()) != "" && $('#<%=IDPrefix%>ddlCitizenshipStatus option:selected').text() != "") {
   			strHtml += '<div class="col-sm-6 col-xs-12"><div class="row"><div class="col-xs-6 text-right row-title"><span class="bold">Citizenship Status</span></div><div class="col-xs-6 text-left row-data"><span>' + $('#<%=IDPrefix%>ddlCitizenshipStatus option:selected').text() + '</span></div></div></div>';
   		}
	   }

		<%End If%>

		<%If EnableMaritalStatus Then%>
    	if ($('#<%=IDPrefix%>ddlMaritalStatus').closest("div.showfield-section").length == 0 || $('#<%=IDPrefix%>ddlMaritalStatus').closest("div.showfield-section").hasClass("hidden") == false) {
    		if ($('#<%=IDPrefix%>ddlMaritalStatus option:selected').val() != "") {
   			strHtml += '<div class="col-sm-6 col-xs-12"><div class="row"><div class="col-xs-6 text-right row-title"><span class="bold">Marital Status</span></div><div class="col-xs-6 text-left row-data"><span>' + $('#<%=IDPrefix%>ddlMaritalStatus option:selected').val() + '</span></div></div></div>';
   		}
	   }

    	<%End If%>
		<%If EnableMemberShipLength Then%>
    	if ($('#<%=IDPrefix%>ddlMembershipLengthYear').closest("div.showfield-section").length == 0 || $('#<%=IDPrefix%>ddlMaritalStatus').closest("div.showfield-section").hasClass("hidden") == false) {
    		var membershipLengthYear = $('#<%=IDPrefix%>ddlMembershipLengthYear').val();
    		var membershipLengthMonth = $('#<%=IDPrefix%>ddlMembershipLengthMonth').val();
    		var strMembershipLengthMonths = (Number(membershipLengthYear) > 0 ? membershipLengthYear + ' yrs' : "") + " " + (Number(membershipLengthMonth) > 0 ? membershipLengthMonth + ' mos ' : "");
    		if ($.trim(strMembershipLengthMonths) !== "") {
    			strHtml += '<div class="col-sm-6 col-xs-12"><div class="row"><div class="col-xs-6 text-right row-title"><span class="bold"><%:IIf(InstitutionType = CEnum.InstitutionType.BANK, "Account Length", "Membership Length")%></span></div><div class="col-xs-6 text-left row-data"><span>' + strMembershipLengthMonths + '</span></div></div></div>';
    		}
		}

    	<%End If%>

        $(this).hide();
        $(this).prev("div.row").hide();
        if (strHtml !== "") {
            $(this).show();
            $(this).prev("div.row").show();
        }
        return strHtml;
    }
</script>

