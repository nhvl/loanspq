﻿Imports LPQMobile.Utils
Imports System.Xml
Imports System.Web.Security.AntiXss

Partial Class Inc_ApplicantInfo
    Inherits CBaseUserControl

#Region "Fields"
	Public HasMotherName As Boolean = True
	Public MaritalStatusDropdown As String
    Public Property EnableMaritalStatus As Boolean
    Public Property EnableMemberShipLength As Boolean
    Public Property EnableMemberNumber As Boolean = True ''by default it is visible
    'These fields are used for passing values in single sign on env
    Protected ReadOnly Property FName() As String
		Get
			'test case for XSS
			'http://lpqmobile.localhost/cc/CreditCard.aspx?lenderref=sdfcu_test&fName=%22%20onEvent%3D%22X155532828Y2Z%20
			Return AntiXssEncoder.HtmlEncode(Request.Params(Left(IDPrefix, 2) & "FName"), True)
		End Get
	End Property
	Protected ReadOnly Property FNameDisabledString As String
		Get
			Return If(Common.SafeString(HttpUtility.UrlDecode(Request.Params(Left(IDPrefix, 2) & "FName"))) <> "", "disabled", "")
		End Get
	End Property
	Protected ReadOnly Property LNameDisabledString As String
		Get
			Return If(Common.SafeString(HttpUtility.UrlDecode(Request.Params(Left(IDPrefix, 2) & "LName"))) <> "", "disabled", "")
		End Get
	End Property

	Protected ReadOnly Property MNameDisabledString As String
		Get
			Return If(Common.SafeString(HttpUtility.UrlDecode(Request.Params(Left(IDPrefix, 2) & "MName"))) <> "", "disabled", "")
		End Get
	End Property

	Protected ReadOnly Property SSNDisabledString As String
		Get
			Return If(Common.SafeString(HttpUtility.UrlDecode(Request.Params(Left(IDPrefix, 2) & "SSN"))) <> "", "disabled", "")
		End Get
	End Property
	Protected ReadOnly Property DOBDisabledString As String
		Get
			Return If(Common.SafeString(HttpUtility.UrlDecode(Request.Params(Left(IDPrefix, 2) & "DOB"))) <> "", "disabled", "")
		End Get
	End Property

	Protected ReadOnly Property MemberNumberDisabledString As String
		Get
			Return If(Common.SafeString(HttpUtility.UrlDecode(Request.Params(Left(IDPrefix, 2) & "MemberNumber"))) <> "", "disabled", "")
		End Get
	End Property

	Protected ReadOnly Property MName() As String
		Get
			Return Common.SafeString(AntiXssEncoder.HtmlEncode(Request.Params(Left(IDPrefix, 2) & "MName"), True))
		End Get
	End Property
	Protected ReadOnly Property LName() As String
		Get
			Return Common.SafeString(AntiXssEncoder.HtmlEncode(Request.Params(Left(IDPrefix, 2) & "LName"), True))
		End Get
	End Property
	Protected ReadOnly Property SSN() As String
		Get
			Return Common.SafeString(AntiXssEncoder.HtmlEncode(Request.Params(Left(IDPrefix, 2) & "SSN"), True)).Replace("/", "").Replace("\", "").Replace("-", "")
		End Get
	End Property
	Protected ReadOnly Property SSN1() As String
		Get
			If SSN.Length > 2 Then
				Return SSN.Substring(0, 3)
			Else
				Return ""
			End If
		End Get
	End Property
	Protected ReadOnly Property SSN2() As String
		Get
			If SSN.Length > 4 Then
				Return SSN.Substring(3, 2)
			Else
				Return ""
			End If
		End Get
	End Property
	Protected ReadOnly Property SSN3() As String
		Get
			If SSN.Length > 8 Then
				Return SSN.Substring(5, 4)
			Else
				Return ""
			End If
		End Get
	End Property
	Protected ReadOnly Property DOB() As String
		Get
			Return Common.SafeString(AntiXssEncoder.HtmlEncode(Request.Params(Left(IDPrefix, 2) & "DOB"), True)).Replace("/", "").Replace("\", "").Replace("-", "")
		End Get
	End Property
	Protected ReadOnly Property DOBMonth() As String
		Get
			If DOB.Length > 1 Then
				Return DOB.Substring(0, 2)
			Else
				Return ""
			End If
		End Get
	End Property
	Protected ReadOnly Property DOBDay() As String
		Get
			If DOB.Length > 3 Then
				Return DOB.Substring(2, 2)
			Else
				Return ""
			End If
		End Get
	End Property
	Protected ReadOnly Property DOBYear() As String
		Get
			If DOB.Length > 7 Then
				Return DOB.Substring(4, 4)
			Else
				Return ""
			End If
		End Get
	End Property
	Protected ReadOnly Property MemberNumber() As String
		Get
			Return AntiXssEncoder.HtmlEncode(Request.Params(Left(IDPrefix, 2) & "MemberNumber"), True)
		End Get
	End Property
	Protected ReadOnly Property HomePhone() As String
		Get
			Return AntiXssEncoder.HtmlEncode(Request.Params(Left(IDPrefix, 2) & "HomePhone"), True).Replace("-", "")
		End Get
	End Property
	Protected ReadOnly Property CellPhone() As String
		Get
			Return AntiXssEncoder.HtmlEncode(Request.Params(Left(IDPrefix, 2) & "CellPhone"), True).Replace("-", "")
		End Get
	End Property
	Protected ReadOnly Property WorkPhone() As String
		Get
			Return AntiXssEncoder.HtmlEncode(Request.Params(Left(IDPrefix, 2) & "WorkPhone"), True).Replace("-", "")
		End Get
	End Property
	Protected ReadOnly Property Email() As String
		Get
			Return AntiXssEncoder.HtmlEncode(Request.Params(Left(IDPrefix, 2) & "Email"), True)
		End Get
	End Property

	Public Property LegacyScandocAvailable As Boolean = False
	Public Property LaserScandocAvailable As Boolean = False

	Private _memberNumberRequired As String
	Public Property MemberNumberRequired() As String
		Get
			Return _memberNumberRequired
		End Get
		Set(value As String)
			_memberNumberRequired = value
		End Set
	End Property


	'Public MaritalStatusDropdown As String
	Public CitizenshipStatusDropdown As String
	Public EmployeeOfLenderDropdown As String
	Public LenderName As String = ""
	''Public IsComboMode As Boolean = False
	Protected _blockApplicantAgeUnder As Integer = 0
	Protected _blockApplicantAgeUnderHELOC As Integer = 0
	Public InstitutionType As CEnum.InstitutionType = CEnum.InstitutionType.CU
	Public Property EnableCitizenshipStatus As Boolean
	Protected Property RelationshipToPrimary As String = ""
#End Region
	Protected Sub Page_Load1(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
		CPBLogger.PBLogger.Component = Me.GetType.ToString
		CPBLogger.PBLogger.Context = Request.UserHostAddress
		If (IsPostBack = False) Then
			SetHideField()
			Dim parent As CBasePage = TryCast(Me.Page, CBasePage)
			If parent._CurrentWebsiteConfig IsNot Nothing Then
				If LoanType Is Nothing Then
					Return
				End If
				Dim nodeName As String = Common.GetConfigLoanTypeFromShort(LoanType) & "/BEHAVIOR"
				Integer.TryParse(Common.getNodeAttributes(parent._CurrentWebsiteConfig, nodeName, "block_applicant_age_under"), _blockApplicantAgeUnder)
				If LoanType = "HE" Then
					Integer.TryParse(Common.getNodeAttributes(parent._CurrentWebsiteConfig, "HOME_EQUITY_LOAN/BEHAVIOR", "block_applicant_age_under"), _blockApplicantAgeUnderHELOC)
				End If
			End If
			Dim relationshipToPrimaryList As Dictionary(Of String, String) = parent._CurrentWebsiteConfig.GetEnumItems("RELATIONSHIP_PRIMARY")
			If relationshipToPrimaryList IsNot Nothing AndAlso parent._CurrentWebsiteConfig.GetEnumItems("RELATIONSHIP_PRIMARY").Count > 0 Then
				RelationshipToPrimary = Common.RenderDropdownlistWithEmpty(relationshipToPrimaryList, "", "--Please Select--")
			End If
		End If
	End Sub

	Private Sub SetHideField()
		'' parent page should be CBasePage
		Dim parent As CBasePage = TryCast(Me.Page, CBasePage)
		If parent IsNot Nothing AndAlso parent._CurrentWebsiteConfig IsNot Nothing Then
		Else
			Return
		End If
		Try
			If isComboMode Then
				HasMotherName = GetXaShowFieldConfig(parent._CurrentWebsiteConfig, "divMotherMaidenName").Equals("Y")
				'Dim oXAVisibility = CType(parent._CurrentWebsiteConfig.GetWebConfigElement.SelectSingleNode("XA_LOAN/VISIBLE"), XmlElement)
				'If oXAVisibility IsNot Nothing AndAlso oXAVisibility.HasAttributes AndAlso Common.SafeString(oXAVisibility.GetAttribute("mother_maiden_name")) = "N" Then
				'	HasMotherName = False
				'End If
			End If
		Catch ex As Exception
			CPBLogger.logInfo("Error SetHideField: " & ex.Message)
			Return
		End Try
	End Sub

	Private Function GetXaShowFieldConfig(websiteConfig As CWebsiteConfig, controllerId As String) As String
		Dim showFieldNode = websiteConfig.GetWebConfigElement.SelectSingleNode(String.Format("CUSTOM_LIST/SHOW_FIELDS/ITEM[@loan_type='XA' and @controller_id='{0}{1}']", IDPrefix, controllerId))
		If showFieldNode IsNot Nothing AndAlso showFieldNode.Attributes("is_visible") IsNot Nothing Then
			Return Common.SafeString(showFieldNode.Attributes("is_visible").InnerXml).ToUpper()
		End If
		Return "Y"
	End Function

End Class
