﻿
Imports System
Imports System.Security.Permissions
Imports LPQMobile.BO
Imports LPQMobile.Utils
Imports LPQMobile.Utils.Common
Imports System.Web.Security.AntiXss
Partial Class Inc_MainApp_xaApplicantID
    Inherits CBaseUserControl

    Public StateDropdown As String
	'Public MaritalStatusDropdown As String
	'Public CitizenshipStatusDropdown As String
    Public IDCardTypeList As String
	Public isExistIDCardType As Boolean
	Public Title As String = ""
	Public Property Header As String = ""
	Public Property isBusinessLoan As Boolean = False
	Public Property ShowFieldDefaultStateOn As Boolean = True
	Public Property Required As Boolean = True
	Protected ReadOnly Property IsRequired() As Boolean
		Get
			If IDPrefix = "m_" Then
				' Minor is optional
				Return False
			ElseIf isBusinessLoan Then
				' Business loans is optional
				Return False
			ElseIf Not Required Then
				' Otherwise, if we're told to be optional, then respect it
				Return False
			Else
				' Otherwise, be required by default
				Return True
			End If
		End Get
	End Property
#Region "NetTellerSSO"
	Protected ReadOnly Property IDCardType() As String
        Get
            Return Common.SafeString(AntiXssEncoder.HtmlEncode(Request.Params(Left(IDPrefix, 2) & "IDCardType"), True))
        End Get
    End Property
    Protected ReadOnly Property IDCountry() As String
        Get
            Return Common.SafeString(AntiXssEncoder.HtmlEncode(Request.Params(Left(IDPrefix, 2) & "IDCountry"), True))
        End Get
    End Property
    Protected ReadOnly Property IDCardNumber() As String
        Get
            Return Common.SafeString(AntiXssEncoder.HtmlEncode(Request.Params(Left(IDPrefix, 2) & "IDCardNumber"), True))
        End Get
    End Property
    Protected ReadOnly Property IDExpiredDate() As String
        Get
            Return Common.SafeString(AntiXssEncoder.HtmlEncode(Request.Params(Left(IDPrefix, 2) & "IDExpiredDate"), True))
        End Get
    End Property
    Protected ReadOnly Property IDExpiredMonth() As String
        Get
            If IDExpiredDate.Length > 1 Then
                Return IDExpiredDate.Substring(0, 2)
            Else
                Return ""
            End If
        End Get
    End Property
    Protected ReadOnly Property IDExpiredDay() As String
        Get
            If IDExpiredDate.Length > 3 Then
                Return IDExpiredDate.Substring(2, 2)
            Else
                Return ""
            End If
        End Get
    End Property
    Protected ReadOnly Property IDExpiredYear() As String
        Get
            If IDExpiredDate.Length > 7 Then
                Return IDExpiredDate.Substring(4, 4)
            Else
                Return ""
            End If
        End Get
    End Property
    Protected ReadOnly Property IDIssuedDate() As String
        Get
            Return Common.SafeString(AntiXssEncoder.HtmlEncode(Request.Params(Left(IDPrefix, 2) & "IDIssuedDate"), True))
        End Get
    End Property
    Protected ReadOnly Property IDIssuedMonth() As String
        Get
            If IDIssuedDate.Length > 1 Then
                Return IDIssuedDate.Substring(0, 2)
            Else
                Return ""
            End If
        End Get
    End Property
    Protected ReadOnly Property IDIssuedDay() As String
        Get
            If IDIssuedDate.Length > 3 Then
                Return IDIssuedDate.Substring(2, 2)
            Else
                Return ""
            End If
        End Get
    End Property
    Protected ReadOnly Property IDIssuedYear() As String
        Get
            If IDIssuedDate.Length > 7 Then
                Return IDIssuedDate.Substring(4, 4)
            Else
                Return ""
            End If
        End Get
    End Property
    Protected ReadOnly Property IDState() As String
        Get
            Return Common.SafeString(AntiXssEncoder.HtmlEncode(Request.Params(Left(IDPrefix, 2) & "IDState"), True))
        End Get
    End Property
#End Region

    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
		If Not IsPostBack Then
			If String.IsNullOrEmpty(Header) Then
				Header = HeaderUtils.RenderPageTitle(16, IIf(String.IsNullOrEmpty(Title), "Identification", Title).ToString(), True)
			End If
		End If
	End Sub
End Class
