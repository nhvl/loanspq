﻿Imports LPQMobile.Utils
Imports System.Web.Security.AntiXss

Partial Class Inc_MainApp_xaApplicantAddress
    Inherits CBaseUserControl
	Public StateDropdown As String
	Protected StateDropdownWithSelectedItem As String
    Public MaritalStatusDropdown As String
    Public CitizenshipStatusDropdown As String
    ''declare varibles for occupancy status
    Public OccupyingLocationDropdown As String
    Public LiveMonthsDropdown As String
	Public LiveYearsDropdown As String
    'Public FooterDataTheme As String

	'Public LoanType As String

	Protected _EnabledAddressVerification As Boolean

	Public Property EnableOccupancyStatus As Boolean = True
	Public Property EnableMailingAddress As Boolean = True
    Public Property EnableHousingPayment As Boolean = False
    Private _collectDescriptionIfOccupancyStatusIsOther As String = ""
	Public Property CollectDescriptionIfOccupancyStatusIsOther As String
		Get
			Return _collectDescriptionIfOccupancyStatusIsOther
		End Get
		Set(value As String)
			_collectDescriptionIfOccupancyStatusIsOther = value
		End Set
	End Property
	Public Property OccupancyStatusesForRequiredHousingPayment As List(Of String) = New List(Of String)({"RENT"})

	Protected ReadOnly Property Address() As String
		Get
			Return Common.SafeString(AntiXssEncoder.HtmlEncode(Request.Params(Left(IDPrefix, 2) & "Address"), True))
		End Get
	End Property
	Protected ReadOnly Property Zip() As String
		Get
			Return Common.SafeString(AntiXssEncoder.HtmlEncode(Request.Params(Left(IDPrefix, 2) & "Zip"), True))
		End Get
	End Property
	Protected ReadOnly Property City() As String
		Get
			Return Common.SafeString(AntiXssEncoder.HtmlEncode(Request.Params(Left(IDPrefix, 2) & "City"), True))
		End Get
	End Property
	Protected ReadOnly Property State() As String
		Get
			Return Common.SafeString(AntiXssEncoder.HtmlEncode(Request.Params(Left(IDPrefix, 2) & "State"), True))
		End Get
	End Property

	Private Sub Inc_MainApp_xaApplicantAddress_Load(sender As Object, e As EventArgs) Handles Me.Load
		'' check if parent page has enabled address verification feature
		'' parent page should be CBasePage
		Dim parent As CBasePage = TryCast(Me.Page, CBasePage)
		If parent IsNot Nothing AndAlso parent._CurrentWebsiteConfig IsNot Nothing Then
			_EnabledAddressVerification = parent._CurrentWebsiteConfig.EnabledAddressVerification
		End If
		StateDropdownWithSelectedItem = Common.RenderStateDropdownlistWithEmpty(CEnum.STATES, "", "--Please Select--", State)
	End Sub

	''' <summary>
	''' Call javascript ""verifyAddress"" function or ""lookupZipcode"" function base on site config
	''' </summary>
	''' <param name="isZipcodeTextbox"></param>
	''' <returns></returns>
	Protected Function callVerifyAddressJSFunction(isZipcodeTextbox As Boolean, Optional ByVal prefixAddress As String = "") As String
		'' verifyAddress js function needs to attach to address + zip + city + state html controls
		If _EnabledAddressVerification Then
			Return String.Format("{0}verifyAddress('{1}');", IDPrefix, prefixAddress)
		Else
			'' lookupZipcode js function only needs to attach to zip html control
			If isZipcodeTextbox Then
				Return String.Format("{0}lookupZipcode(this.value,'{1}');", IDPrefix, prefixAddress)
			End If
		End If
		Return String.Empty
	End Function

End Class
