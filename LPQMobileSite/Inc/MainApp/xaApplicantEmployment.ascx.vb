﻿Imports System.Xml
Imports LPQMobile.Utils
Imports LPQMobile.Utils.Common
Partial Class Inc_MainApp_xaApplicantEmployment
    Inherits CBaseUserControl

    Private _employmentStatusDropdown As String
	'Public HasEmployment As String = ""
	'Public HasMinorEmployment As String = ""
	'Public VisibleEmployment As String = ""
    Private _Config As XmlElement
	Public Property Title As String = ""

	Public Property Header As String = ""

	Public Property EnableGrossMonthlyIncome As Boolean = True
	Public Property CollectJobTitleOnly As Boolean = False

    Public Property EmploymentStatusDropdown() As String
        Get
            Return _employmentStatusDropdown
        End Get
        Set(value As String)
            _employmentStatusDropdown = value
        End Set
    End Property

    Public Property Config() As XmlElement
        Get
            Return _Config
        End Get
        Set(ByVal value As XmlElement)
            _Config = value
        End Set
	End Property

	Public Property ShowFieldDefaultStateOn As Boolean = True

	Private _collectDescriptionIfEmploymentStatusIsOther As String = ""
	Public Property CollectDescriptionIfEmploymentStatusIsOther As String
		Get
			Return _collectDescriptionIfEmploymentStatusIsOther
		End Get
		Set(value As String)
			_collectDescriptionIfEmploymentStatusIsOther = value
		End Set
	End Property
	
    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        CPBLogger.PBLogger.Component = Me.GetType.ToString
        CPBLogger.PBLogger.Context = Request.UserHostAddress
        If (IsPostBack = False) Then
			'SetHideField(_Config)
			If String.IsNullOrEmpty(Header) Then
				Header = HeaderUtils.RenderPageTitle(17, IIf(String.IsNullOrEmpty(Title), "About Your Employment", Title).ToString(), True)
			End If
        End If
    End Sub
	'Private Sub SetHideField(ByVal poConfig As XmlElement)
	'	Dim sXABranchXpath As String = "XA_LOAN/VISIBLE"
	'	Dim oXAVisibility As XmlElement
	'	Try
	'		oXAVisibility = CType(poConfig.SelectSingleNode(sXABranchXpath), XmlElement)
	'		If oXAVisibility.HasAttributes Then
	'			If Not String.IsNullOrEmpty(SafeString(oXAVisibility.GetAttribute("minor_employment"))) Then
	'				HasMinorEmployment = oXAVisibility.GetAttribute("minor_employment")
	'			End If
	'			If SafeString(oXAVisibility.GetAttribute("employment")) <> "N" Then	'default = "Y"
	'				If IDPrefix = "m_" Then ''hide and show minor employment based on the minor_employment attribute
	'					If HasMinorEmployment = "Y" Then
	'						VisibleEmployment = "block"
	'					Else
	'						VisibleEmployment = "none" ''default hide minor employment
	'					End If
	'				Else
	'					VisibleEmployment = "block"	'default = "Y"
	'					HasEmployment = "Y"
	'				End If
	'			Else 'employment = "N"
	'				VisibleEmployment = "none"
	'				HasEmployment = "N"
	'			End If
	'		Else
	'			If IDPrefix = "m_" Then ''hide and show minor employment based on the minor_employment attribute
	'				VisibleEmployment = "none" ''default hide minor employment
	'			Else
	'				VisibleEmployment = "block"	'default show employment for non minor
	'			End If
	'		End If
	'	Catch ex As Exception
	'		CPBLogger.logInfo("XA: Error SetHideField: " & ex.Message)
	'		Return
	'	End Try
	'End Sub
End Class
