﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="ApplicantAdditionalInfo.ascx.vb" Inherits="Inc_MainApp_ApplicantAdditionalInfo"  %>
<%@ Import Namespace="Newtonsoft.Json" %>
<%@ Import Namespace="LPQMobile.Utils" %>
<div data-role="page" id="page<%=IDPrefix%>">
    <div data-role="header" style="display: none">
         <h1>Applicant Additional Information</h1>
    </div>
    <div data-role="content">
	    <%=HeaderUtils.RenderLogoAndSteps(LogoUrl, 1, "APPLY_CC")%> 
        <%=HeaderUtils.RenderPageTitle(21, "Applicant Additional Information", False)%>
		<%=HeaderUtils.RenderPageTitle(0, "State law requires us to collect spousal information for residents applying for credit.", True)%>
		<br/>
		<div data-role="fieldcontain">
			<label for="<%=IDPrefix%>ddlIsMarried" class="RequiredIcon">Are you married?</label>
			<select id="<%=IDPrefix%>ddlIsMarried">
				<option value=""></option>
				<option value="true">Yes</option>
				<option value="false">No</option>
			</select>
		</div>
		<div id="<%=IDPrefix%>divFullPageContainer" class="hidden">
			<%=HeaderUtils.RenderPageTitle(0, "Please provide the following information about your spouse.", True)%>
			<%=HeaderUtils.RenderPageTitle(18, "Spouse Information", True)%>
			<iframe name="<%=IDPrefix%>if_ApplicantInfo" style="display: none;" src="about:blank" ></iframe>
			<form target="<%=IDPrefix%>if_ApplicantInfo" action="/handler/AutoFillHandler.aspx" id="<%=IDPrefix%>frm_ApplicantInfo">
				<div data-role="fieldcontain">
					<label for="<%=IDPrefix%>txtFName" class="RequiredIcon">First Name</label>
					<input type="text" id="<%=IDPrefix%>txtFName" name="fname" autocomplete="given-name" maxlength ="50"/>
				</div>
				<div data-role="fieldcontain">
					<label for="<%=IDPrefix%>txtMName">Middle Name</label>
					<input type="text" id="<%=IDPrefix%>txtMName" maxlength ="50" name="mname" autocomplete="additional-name" />
				</div>
				<div data-role="fieldcontain">
					<label for="<%=IDPrefix%>txtLName" class="RequiredIcon">Last Name</label>
					<input type="text" id="<%=IDPrefix%>txtLName" maxlength ="50" name="lname" autocomplete="family-name"/>
				</div>
			</form>
			<div data-role="fieldcontain">
				<label for="<%=IDPrefix%>suffix">Suffix (Jr., Sr., etc.)</label>
				<select name="suffix" id="<%=IDPrefix%>suffix">
					<%=Common.RenderDropdownlist(GetSuffixList(), "")%>
				</select>
			</div>
			<div data-role="fieldcontain">
				<div class="row">
					<div class="col-xs-6 no-padding"><label id="<%=IDPrefix%>lblSSN" for="<%=IDPrefix%>txtSSN1">SSN</label></div>
					<div class="col-xs-6 no-padding ui-label-ssn">
						<span class="pull-right header_theme2 ssn-ico" style="padding-left: 4px; padding-right: 2px;display: inline;"><%=HeaderUtils.IconLock16%><span class="sr-only">SSN IconLock</span></span>
						<span id="<%=IDPrefix%>spOpenSsnSastisfy" onclick='openPopup("#<%=IDPrefix%>popSSNSastisfy")' class="pull-right focus-able" style="display: inline;" data-rel='popup' data-position-to="window" data-transition='pop'><span class="pull-right ssn-ico header_theme2"><%=HeaderUtils.IconQuestion16%></span><span class="sr-only">SSN IconQuestion</span></span>
						<div style="display: inline;" class="hidden"><label is_show="1"  class="pull-right header_theme2 rename-able shadow-btn focus-able" style="margin-right: 10px; cursor: pointer;outline: none;" onclick="ASI.FACTORY.<%=IDPrefix%>toggleSSNText(this);">Hide SSN</label></div>
						<div style="display: inline;"><label is_show="0"  class="pull-right header_theme2 rename-able shadow-btn focus-able" style="margin-right: 10px; cursor: pointer;outline: none;" onclick="ASI.FACTORY.<%=IDPrefix%>toggleSSNText(this);">Show SSN</label></div>
					</div>
				</div>
				<div id="<%=IDPrefix%>divSSN" class="ui-input-ssn row">
					<div class="col-xs-4">
						<input aria-labelledby="<%=IDPrefix%>lblSSN" type="password" autocomplete="new-password" pattern="[0-9]*" placeholder="---" id="<%=IDPrefix%>txtSSN1" maxlength ='3' next-input="#<%=IDPrefix%>txtSSN2" onkeydown="limitToNumeric(event);" onkeyup="onKeyUp(event,'#<%=IDPrefix%>txtSSN1','#<%=IDPrefix%>txtSSN2', '3');"/>
						<input type="hidden" id="<%=IDPrefix%>hdSSN1" value=""/>
					</div>
					<div class="col-xs-4">
						<input aria-labelledby="<%=IDPrefix%>lblSSN" type="password" autocomplete="new-password" pattern="[0-9]*" placeholder="--" id="<%=IDPrefix%>txtSSN2" maxlength ='2' next-input="#<%=IDPrefix%>txtSSN3" onkeydown="limitToNumeric(event);" onkeyup="onKeyUp(event,'#<%=IDPrefix%>txtSSN2','#<%=IDPrefix%>txtSSN3', '2');"/>
						<input type="hidden" id="<%=IDPrefix%>hdSSN2" value=""/>
					</div>
					<div class="col-xs-4" style="padding-right: 0;">
						<input aria-labelledby="<%=IDPrefix%>lblSSN" type="<%=TextAndroidTel%>" pattern="[0-9]*" placeholder="----" id="<%=IDPrefix%>txtSSN3" maxlength ='4' onkeydown="limitToNumeric(event);"/>
						<input type="hidden" id="<%=IDPrefix%>txtSSN" class="combine-field-value"/>
					</div>
				</div>
			</div>
			<div id="<%=IDPrefix%>popSSNSastisfy" data-role="popup" style="max-width: 400px;">
				<div data-role="content">
					<div class="row">
						<div class="col-xs-12 header_theme2">
							<a data-rel="back" class="pull-right svg-btn" href="#"><%=HeaderUtils.IconClose %><span style="display: none;">close</span></a>
						</div>
					</div>
					<div class="row">
						<div class="col-sm-12">
							<div style="margin: 10px 0; font-weight: normal;">
								Your Social Security Number (SSN) is used for identification purposes and to determine your account opening eligibility.
							</div>     
						</div>    
					</div>
				</div>
			</div>
			<%=HeaderUtils.RenderPageTitle(14, "Spouse Address", True)%>
			<iframe name="<%=IDPrefix%>if_Address"  style="display: none;" src="about:blank" ></iframe>
			<form target="<%=IDPrefix%>if_Address" action="/handler/AutoFillHandler.aspx" id="<%=IDPrefix%>frm_Address">
				<div style="margin-top: 10px;">
					<a href="#" class="header_theme2 shadow-btn chevron-circle-right-before" id="<%=IDPrefix%>btnCopyAddress" onclick="ASI.FACTORY.<%=IDPrefix%>CopyPrimaryAddress(event);">Copy the Primary Applicant's Address</a>
				</div>
				<div id="<%=IDPrefix%>div_ddl_country" class="hidden">
					<div data-role="fieldcontain">
						<label for="<%=IDPrefix%>ddlCountry" class="RequiredIcon">Country</label>
						<select id="<%=IDPrefix%>ddlCountry" name="country" autocomplete="country" onchange="ASI.FACTORY.<%=IDPrefix%>showHideState()">
							<option value="USA" selected="selected">USA</option>
							<option value="FRANCE">France</option>
							<option value="GERMANY">Germany</option>
							<option value="ITALY">Italy</option>
							<option value="SPAIN">Spain</option>
							<option value="UNITED KINGDOM">United Kingdom</option>
							<option value="AALAND">Aaland</option>
							<option value="ABKHAZIA">Abkhazia</option>
							<option value="AFGHANISTAN">Afghanistan</option>
							<option value="ALBANIA">Albania</option>
							<option value="ALGERIA">Algeria</option>
							<option value="AMERICAN SAMOA">American Samoa</option>
							<option value="ANDORRA">Andorra</option>
							<option value="ANGOLA">Angola</option>
							<option value="ANGUILLA">Anguilla</option>
							<option value="ANTARCTICA">Antarctica</option>
							<option value="ANTIGUA_BARBUDA">Antigua And Barbuda</option>
							<option value="ARGENTINA">Argentina</option>
							<option value="ARMENIA">Armenia</option>
							<option value="ARUBA">Aruba</option>
							<option value="AUSTRALIA">Australia</option>
							<option value="AUSTRIA">Austria</option>
							<option value="AZERBAIJAN">Azerbaijan</option>
							<option value="BAHAMAS">Bahamas</option>
							<option value="BAHRAIN">Bahrain</option>
							<option value="BANGLADESH">Bangladesh</option>
							<option value="BARBADOS">Barbados</option>
							<option value="BELARUS">Belarus</option>
							<option value="BELGIUM">Belgium</option>
							<option value="BELIZE">Belize</option>
							<option value="BENIN">Benin</option>
							<option value="BERMUDA">Bermuda</option>
							<option value="BHUTAN">Bhutan</option>
							<option value="BOLIVIA">Bolivia</option>
							<option value="BOSNIA_HERZEGOVINA">Bosnia And Herzegovina</option>
							<option value="BOTSWANA">Botswana</option>
							<option value="BOUVET ISLAND">Bouvet Island</option>
							<option value="BRAZIL">Brazil</option>
							<option value="BRITISH INDIAN OCEAN">British Indian Ocean Territory</option>
							<option value="BRITISH_VIRGIN_ILND">British Virgin Islands</option>
							<option value="BRUNEI">Brunei</option>
							<option value="BULGARIA">Bulgaria</option>
							<option value="BURKINA FASO">Burkina Faso</option>
							<option value="BURUNDI">Burundi</option>
							<option value="CAMBODIA">Cambodia</option>
							<option value="CAMEROON">Cameroon</option>
							<option value="CANADA">Canada</option>
							<option value="CAPE VERDE">Cape Verde</option>
							<option value="CAYMAN ISLANDS">Cayman Islands</option>
							<option value="CENTRAL AFRICAN REP">Central African Republic</option>
							<option value="CHAD">Chad</option>
							<option value="CHILE">Chile</option>
							<option value="CHINA">China</option>
							<option value="CHRISTMAS ISLAND">Christmas Island</option>
							<option value="COCOS ISLANDS">Cocos Islands</option>
							<option value="COLOMBIA">Colombia</option>
							<option value="COMOROS">Comoros</option>
							<option value="COOK ISLANDS">Cook Islands</option>
							<option value="COSTA RICA">Costa Rica</option>
							<option value="COTE D&#39;IVOIRE">Cote D&#39;Ivoire</option>
							<option value="CROATIA">Croatia</option>
							<option value="CUBA">Cuba</option>
							<option value="CYPRUS">Cyprus</option>
							<option value="CZECH REPUBLIC">Czech Republic</option>
							<option value="DEMOCRATIC_REP_CONGO">Democratic Republic Of The Congo</option>
							<option value="DENMARK">Denmark</option>
							<option value="DJIBOUTI">Djibouti</option>
							<option value="DOMINICA">Dominica</option>
							<option value="DOMINICAN REPUBLIC">Dominican Republic</option>
							<option value="TIMOR-LESTE">East Timor</option>
							<option value="ECUADOR">Ecuador</option>
							<option value="EGYPT">Egypt</option>
							<option value="EL SALVADOR">El Salvador</option>
							<option value="EQUATORIAL GUINEA">Equatorial Guinea</option>
							<option value="ERITREA">Eritrea</option>
							<option value="ESTONIA">Estonia</option>
							<option value="ETHIOPIA">Ethiopia</option>
							<option value="FALKLAND ISLANDS">Falkland Islands</option>
							<option value="FAROE ISLANDS">Faroe Islands</option>
							<option value="FIJI">Fiji</option>
							<option value="FINLAND">Finland</option>
							<option value="FRENCH GUIANA">French Guiana</option>
							<option value="FRENCH POLYNESIA">French Polynesia</option>
							<option value="FRENCH_SOUTHERN_TERR">French Southern Territories</option>
							<option value="GABON">Gabon</option>
							<option value="GAMBIA">Gambia</option>
							<option value="GEORGIA">Georgia</option>
							<option value="GHANA">Ghana</option>
							<option value="GIBRALTAR">Gibraltar</option>
							<option value="GREECE">Greece</option>
							<option value="GREENLAND">Greenland</option>
							<option value="GRENADA">Grenada</option>
							<option value="GUADELOUPE">Guadeloupe</option>
							<option value="GUAM">Guam</option>
							<option value="GUATEMALA">Guatemala</option>
							<option value="GUERNSEY">Guernsey</option>
							<option value="GUINEA">Guinea</option>
							<option value="GUINEA BISSAU">Guinea Bissau</option>
							<option value="GUYANA">Guyana</option>
							<option value="HAITI">Haiti</option>
							<option value="HEARD_MCDONALD_ILND">Heard Island And Mcdonald Islands</option>
							<option value="HONDURAS">Honduras</option>
							<option value="HONG KONG">Hong Kong</option>
							<option value="HUNGARY">Hungary</option>
							<option value="ICELAND">Iceland</option>
							<option value="INDIA">India</option>
							<option value="INDONESIA">Indonesia</option>
							<option value="IRAN">Iran</option>
							<option value="IRAQ">Iraq</option>
							<option value="IRELAND">Ireland</option>
							<option value="ISLE OF MAN">Isle Of Man</option>
							<option value="ISRAEL">Israel</option>
							<option value="JAMAICA">Jamaica</option>
							<option value="JAPAN">Japan</option>
							<option value="JERSEY">Jersey</option>
							<option value="JORDAN">Jordan</option>
							<option value="KAZAKHSTAN">Kazakhstan</option>
							<option value="KENYA">Kenya</option>
							<option value="KIRIBATI">Kiribati</option>
							<option value="KOSOVO">Kosovo</option>
							<option value="KUWAIT">Kuwait</option>
							<option value="KYRGYZSTAN">Kyrgyzstan</option>
							<option value="LAOS">Laos</option>
							<option value="LATVIA">Latvia</option>
							<option value="LEBANON">Lebanon</option>
							<option value="LESOTHO">Lesotho</option>
							<option value="LIBERIA">Liberia</option>
							<option value="LIBYA">Libya</option>
							<option value="LIECHTENSTEIN">Liechtenstein</option>
							<option value="LITHUANIA">Lithuania</option>
							<option value="LUXEMBOURG">Luxembourg</option>
							<option value="MACAU">Macau</option>
							<option value="MACEDONIA">Macedonia</option>
							<option value="MADAGASCAR">Madagascar</option>
							<option value="MALAWI">Malawi</option>
							<option value="MALAYSIA">Malaysia</option>
							<option value="MALDIVES">Maldives</option>
							<option value="MALI">Mali</option>
							<option value="MALTA">Malta</option>
							<option value="MARSHALL ISLANDS">Marshall Islands</option>
							<option value="MARTINIQUE">Martinique</option>
							<option value="MAURITANIA">Mauritania</option>
							<option value="MAURITIUS">Mauritius</option>
							<option value="MAYOTTE">Mayotte</option>
							<option value="MEXICO">Mexico</option>
							<option value="MICRONESIA">Micronesia</option>
							<option value="MOLDOVA">Moldova</option>
							<option value="MONACO">Monaco</option>
							<option value="MONGOLIA">Mongolia</option>
							<option value="MONTENEGRO">Montenegro</option>
							<option value="MONTSERRAT">Montserrat</option>
							<option value="MOROCCO">Morocco</option>
							<option value="MOZAMBIQUE">Mozambique</option>
							<option value="MYANMAR">Myanmar</option>
							<option value="NAGORNO-KARABAKH">Nagorno-Karabakh</option>
							<option value="NAMIBIA">Namibia</option>
							<option value="NAURU">Nauru</option>
							<option value="NEPAL">Nepal</option>
							<option value="NETHERLANDS">Netherlands</option>
							<option value="NETHERLANDS ANTILLES">Netherlands Antilles</option>
							<option value="NEW CALEDONIA">New Caledonia</option>
							<option value="NEW ZEALAND">New Zealand</option>
							<option value="NICARAGUA">Nicaragua</option>
							<option value="NIGER">Niger</option>
							<option value="NIGERIA">Nigeria</option>
							<option value="NIUE">Niue</option>
							<option value="NORFOLK ISLAND">Norfolk Island</option>
							<option value="NORTH KOREA">North Korea</option>
							<option value="NORTHERN CYPRUS">Northern Cyprus</option>
							<option value="NORTHERN_MARIANA_ILN">Northern Mariana Islands</option>
							<option value="NORWAY">Norway</option>
							<option value="OMAN">Oman</option>
							<option value="PAKISTAN">Pakistan</option>
							<option value="PALAU">Palau</option>
							<option value="PALESTINE">Palestine</option>
							<option value="PANAMA">Panama</option>
							<option value="PAPUA NEW GUINEA">Papua New Guinea</option>
							<option value="PARAGUAY">Paraguay</option>
							<option value="PERU">Peru</option>
							<option value="PHILIPPINES">Philippines</option>
							<option value="PITCAIRN">Pitcairn</option>
							<option value="POLAND">Poland</option>
							<option value="PORTUGAL">Portugal</option>
							<option value="PUERTO RICO">Puerto Rico</option>
							<option value="QATAR">Qatar</option>
							<option value="CONGO">Republic Of The Congo</option>
							<option value="REUNION">Reunion</option>
							<option value="ROMANIA">Romania</option>
							<option value="RUSSIA">Russia</option>
							<option value="RWANDA">Rwanda</option>
							<option value="SAINT BARTHELEMY">Saint Barthelemy</option>
							<option value="SAINT HELENA">Saint Helena</option>
							<option value="SAINT_KITTS_NEVIS">Saint Kitts And Nevis</option>
							<option value="SAINT LUCIA">Saint Lucia</option>
							<option value="SAINT_VINCENT_GREN">Saint Vincent And The Grenadines</option>
							<option value="SAINT-MARTIN">Saint-Martin</option>
							<option value="SAINTPIERRE_MIQUELON">Saint-Pierre And Miquelon</option>
							<option value="SAMOA">Samoa</option>
							<option value="SAN MARINO">San Marino</option>
							<option value="SAO_TOME_PRINCIPE">Sao Tome And Principe</option>
							<option value="SAUDI ARABIA">Saudi Arabia</option>
							<option value="SENEGAL">Senegal</option>
							<option value="SERBIA">Serbia</option>
							<option value="SEYCHELLES">Seychelles</option>
							<option value="SIERRA LEONE">Sierra Leone</option>
							<option value="SINGAPORE">Singapore</option>
							<option value="SLOVAKIA">Slovakia</option>
							<option value="SLOVENIA">Slovenia</option>
							<option value="SOLOMON ISLANDS">Solomon Islands</option>
							<option value="SOMALIA">Somalia</option>
							<option value="SOMALILAND">Somaliland</option>
							<option value="SOUTH AFRICA">South Africa</option>
							<option value="S_GEORGIA_SANDWICH">South Georgia And The South Sandwich Islands</option>
							<option value="SOUTH KOREA">South Korea</option>
							<option value="SOUTH OSSETIA">South Ossetia</option>
							<option value="SRI LANKA">Sri Lanka</option>
							<option value="SUDAN">Sudan</option>
							<option value="SURINAME">Suriname</option>
							<option value="SVALBARD_JAN_MAYEN">Svalbard And Jan Mayen</option>
							<option value="SWAZILAND">Swaziland</option>
							<option value="SWEDEN">Sweden</option>
							<option value="SWITZERLAND">Switzerland</option>
							<option value="SYRIA">Syria</option>
							<option value="TAIWAN">Taiwan</option>
							<option value="TAJIKISTAN">Tajikistan</option>
							<option value="TANZANIA">Tanzania</option>
							<option value="THAILAND">Thailand</option>
							<option value="TOGO">Togo</option>
							<option value="TOKELAU">Tokelau</option>
							<option value="TONGA">Tonga</option>
							<option value="TRANSNISTRIA">Transnistria</option>
							<option value="TRINIDAD AND TOBAGO">Trinidad And Tobago</option>
							<option value="TRISTAN DA CUNHA">Tristan Da Cunha</option>
							<option value="TUNISIA">Tunisia</option>
							<option value="TURKEY">Turkey</option>
							<option value="TURKMENISTAN">Turkmenistan</option>
							<option value="TURKS_CAICOS_ISLANDS">Turks And Caicos Islands</option>
							<option value="TUVALU">Tuvalu</option>
							<option value="UGANDA">Uganda</option>
							<option value="UKRAINE">Ukraine</option>
							<option value="UNITED ARAB EMIRATES">United Arab Emirates</option>
							<option value="US_MINOR_ISLANDS">United States Minor Outlying Islands</option>
							<option value="US_VIRGIN_ISLANDS">United States Virgin Islands</option>
							<option value="URUGUAY">Uruguay</option>
							<option value="UZBEKISTAN">Uzbekistan</option>
							<option value="VANUATU">Vanuatu</option>
							<option value="VATICAN CITY">Vatican City</option>
							<option value="VENEZUELA">Venezuela</option>
							<option value="VIETNAM">Vietnam</option>
							<option value="WALLIS AND FUTUNA">Wallis And Futuna</option>
							<option value="WESTERN SAHARA">Western Sahara</option>
							<option value="YEMEN">Yemen</option>
							<option value="ZAMBIA">Zambia</option>
							<option value="ZIMBABWE">Zimbabwe</option>
						</select>
					</div>
				</div>
				<div data-role="fieldcontain">
					<label for="<%=IDPrefix%>txtAddress" class="RequiredIcon">Address</label>
					<input type="text" id="<%=IDPrefix%>txtAddress" maxlength="100" name="address1" autocomplete="street-address"/>
				</div>
				<div data-role="fieldcontain" id="<%=IDPrefix%>divAddress2" style="display: none;">
					<label for="<%=IDPrefix%>txtAddress2">Address Line 2</label>
					<input type="text" id="<%=IDPrefix%>txtAddress2" maxlength="100" name="address2" autocomplete="address-line2"/>
				</div>
				<div data-role="fieldcontain">
					<label for="<%=IDPrefix%>txtZip" id="<%=IDPrefix%>lblZipTitle" class="RequiredIcon">Zip</label>
					<input type="tel" id="<%=IDPrefix%>txtZip" maxlength="10" pattern="[0-9]*" name="zip" autocomplete="postal-code"/>
				</div>

				<div data-role="fieldcontain">
					<label for="<%=IDPrefix%>txtCity" id="<%=IDPrefix%>lblCityTitle" class="RequiredIcon">City</label>
					<input type="text" id="<%=IDPrefix%>txtCity" maxlength="50" name="city" autocomplete="address-level2"/>
				</div>
				<div class="<%=IDPrefix%>div_ddl_state">
					<div data-role="fieldcontain">
						<label for="<%=IDPrefix%>ddlState" class="RequiredIcon">State</label>
						<select id="<%=IDPrefix%>ddlState" name="state" autocomplete="address-level1">
							<%=Common.RenderStateDropdownlistWithEmpty(CEnum.STATES, "", "--Please Select--")%>
						</select>
					</div>
				</div>
				<div><span id="<%=IDPrefix%>spVerifyMessage" class="require-span" style="display: none;">Unable to validate address</span> </div>
			</form>
			<%If EnablePrimaryAppSpouseContactInfo Then%>
			<div id="<%=IDPrefix%>divNSSContactInfo" <%=IIf(IsInMode("777") AndAlso IsInFeature("visibility"), "class=""showfield-section"" data-show-field-section-id=""" & BuildShowFieldSectionID("divNSSContactInfo") & """ data-default-state=""off"" data-section-name= 'Applicant&#39;s Spouse Contact'", "")%>>
				<%=HeaderUtils.RenderPageTitle(22, "Spouse Contact", True)%>
				<iframe name="<%=IDPrefix%>if_ContactInfo" style="display: none;" src="about:blank" ></iframe>
				<form target="<%=IDPrefix%>if_ContactInfo" action="/handler/AutoFillHandler.aspx" id="<%=IDPrefix%>frm_ContactInfo">
					<div data-role="fieldcontain">
						<label id="<%=IDPrefix%>lblEmail" for="<%=IDPrefix%>txtEmail" class="RequiredIcon">Email</label>
						<input type="email" name="email" autocomplete="email" id="<%=IDPrefix%>txtEmail" maxlength ="50"/>
					</div>
					<div data-role="fieldcontain" id="<%=IDPrefix%>divHomePhone">
						<div>
							<div style="display: inline-block;"><label id="<%=IDPrefix%>lblHomePhone" for="<%=IDPrefix%>txtHomePhone" style="white-space: nowrap;">Home Phone</label></div>
							<div style="display: inline-block;"><label class="phone_format rename-able" style="white-space: nowrap; font-style: italic;">(xxx) xxx-xxxx</label></div>
						</div>
						<input type="<%=TextAndroidTel%>" pattern="[0-9]*" autocomplete="tel" name="phone" id="<%=IDPrefix%>txtHomePhone" class="inphone" maxlength='14'/>
					</div>
					<div data-role="fieldcontain" id="<%=IDPrefix%>divMobilePhone">
						<div>
							<div style="display: inline-block;"><label id="<%=IDPrefix%>lblMobilePhone" for="<%=IDPrefix%>txtMobilePhone"  style="white-space: nowrap;">Cell Phone</label></div>
							<div style="display: inline-block;"><label class="phone_format rename-able" style="white-space: nowrap; font-style: italic;">(xxx) xxx-xxxx</label></div>
						</div>
						<input type= "<%=TextAndroidTel%>" pattern="[0-9]*" autocomplete="tel" name="mobile" id="<%=IDPrefix%>txtMobilePhone" class="inphone" maxlength='14'/>
					</div>
				</form>
			</div>
			<%End If%>
			<br/>
			<div id="div<%=IDPrefix%>DisclosureContainer" class="header_theme">
			</div>
		</div>
	 </div>
    <div class ="div-continue"  data-role="footer">
        <a href="#" data-transition="slide" type="button" onclick="ASI.FACTORY.validatePage<%=IDPrefix%>()" class="div-continue-button">Continue</a> 
        <a href="#divErrorDialog" style="display: none;">no text</a>
        <span> Or</span>  <a href="#" onclick="ASI.FACTORY.goToPreviousPage();" class ="div-goback" data-corners="false" data-shadow="false" data-theme="reset"><span class="hover-goback"> Go Back</span></a>
    </div>
</div>
<div data-role="page" id="pageco_<%=IDPrefix%>">
    <div data-role="header" style="display: none">
         <h1>Co-Applicant Additional Information</h1>
    </div>
    <div data-role="content">
	    <%=HeaderUtils.RenderLogoAndSteps(LogoUrl, 1, "APPLY_CC")%> 
        <%=HeaderUtils.RenderPageTitle(21, "Co-Applicant Additional Information", False)%>
		<%=HeaderUtils.RenderPageTitle(0, "State law requires us to collect spousal information for residents applying for credit.", True)%>
		<br/>
		<div data-role="fieldcontain">
			<label for="<%=IDPrefix%>co_ddlIsMarried" class="RequiredIcon">Is the co-applicant married?</label>
			<select id="<%=IDPrefix%>co_ddlIsMarried">
				<option value=""></option>
				<option value="true">Yes</option>
				<option value="false">No</option>
			</select>
		</div>
		<div id="<%=IDPrefix%>co_divFullPageContainer" class="hidden">
			<%=HeaderUtils.RenderPageTitle(0, "Please provide the following information about the co-applicant's spouse.", True)%>
			<%=HeaderUtils.RenderPageTitle(18, "Personal Information", true)%>
			<iframe name="<%=IDPrefix%>co_if_ApplicantInfo" style="display: none;" src="about:blank" ></iframe>
			<form target="<%=IDPrefix%>co_if_ApplicantInfo" action="/handler/AutoFillHandler.aspx" id="<%=IDPrefix%>co_frm_ApplicantInfo">
				<div data-role="fieldcontain">
					<label for="<%=IDPrefix%>co_txtFName" class="RequiredIcon">First Name</label>
					<input type="text" id="<%=IDPrefix%>co_txtFName" name="fname" autocomplete="given-name" maxlength ="50"/>
				</div>
				<div data-role="fieldcontain">
					<label for="<%=IDPrefix%>co_txtMName">Middle Name</label>
					<input type="text" id="<%=IDPrefix%>co_txtMName" maxlength ="50" name="mname" autocomplete="additional-name" />
				</div>
				<div data-role="fieldcontain">
					<label for="<%=IDPrefix%>co_txtLName" class="RequiredIcon">Last Name</label>
					<input type="text" id="<%=IDPrefix%>co_txtLName" maxlength ="50" name="lname" autocomplete="family-name"/>
				</div>
			</form>
			<div data-role="fieldcontain">
				<label for="<%=IDPrefix%>co_suffix">Suffix (Jr., Sr., etc.)</label>
				<select name="suffix" id="<%=IDPrefix%>co_suffix">
					<%=Common.RenderDropdownlist(GetSuffixList(), "")%>
				</select>
			</div>
			<div data-role="fieldcontain">
				<div class="row">
					<div class="col-xs-6 no-padding"><label id="<%=IDPrefix%>co_lblSSN" for="<%=IDPrefix%>co_txtSSN1">SSN</label></div>
					<div class="col-xs-6 no-padding ui-label-ssn">
						<span class="pull-right header_theme2 ssn-ico" style="padding-left: 4px; padding-right: 2px;display: inline;"><%=HeaderUtils.IconLock16%><span class="sr-only">SSN IconLock</span></span>
						<span id="<%=IDPrefix%>co_spOpenSsnSastisfy" onclick='openPopup("#<%=IDPrefix%>co_popSSNSastisfy")' class="pull-right focus-able" style="display: inline;" data-rel='popup' data-position-to="window" data-transition='pop'><span class="pull-right ssn-ico header_theme2"><%=HeaderUtils.IconQuestion16%></span><span class="sr-only">SSN IconQuestion</span></span>
						<div style="display: inline;" class="hidden"><label is_show="1"  class="pull-right header_theme2 rename-able shadow-btn focus-able" style="margin-right: 10px; cursor: pointer;outline: none;" onclick="ASI.FACTORY.<%=IDPrefix%>toggleSSNText(this, 'co_');">Hide SSN</label></div>
						<div style="display: inline;"><label is_show="0"  class="pull-right header_theme2 rename-able shadow-btn focus-able" style="margin-right: 10px; cursor: pointer;outline: none;" onclick="ASI.FACTORY.<%=IDPrefix%>toggleSSNText(this, 'co_');">Show SSN</label></div>
					</div>
				</div>
				<div id="<%=IDPrefix%>co_divSSN" class="ui-input-ssn row">
					<div class="col-xs-4">
						<input aria-labelledby="<%=IDPrefix%>co_lblSSN" type="password" autocomplete="new-password" pattern="[0-9]*" placeholder="---" id="<%=IDPrefix%>co_txtSSN1" maxlength ='3' next-input="#<%=IDPrefix%>co_txtSSN2" onkeydown="limitToNumeric(event);" onkeyup="onKeyUp(event,'#<%=IDPrefix%>co_txtSSN1','#<%=IDPrefix%>co_txtSSN2', '3');"/>
						<input type="hidden" id="<%=IDPrefix%>co_hdSSN1" value=""/>
					</div>
					<div class="col-xs-4">
						<input aria-labelledby="<%=IDPrefix%>co_lblSSN" type="password" autocomplete="new-password" pattern="[0-9]*" placeholder="--" id="<%=IDPrefix%>co_txtSSN2" maxlength ='2' next-input="#<%=IDPrefix%>co_txtSSN3" onkeydown="limitToNumeric(event);" onkeyup="onKeyUp(event,'#<%=IDPrefix%>co_txtSSN2','#<%=IDPrefix%>co_txtSSN3', '2');"/>
						<input type="hidden" id="<%=IDPrefix%>co_hdSSN2" value=""/>
					</div>
					<div class="col-xs-4" style="padding-right: 0;">
						<input aria-labelledby="<%=IDPrefix%>co_lblSSN" type="<%=TextAndroidTel%>" pattern="[0-9]*" placeholder="----" id="<%=IDPrefix%>co_txtSSN3" maxlength ='4' onkeydown="limitToNumeric(event);"/>
						<input type="hidden" id="<%=IDPrefix%>co_txtSSN" class="combine-field-value"/>
					</div>
				</div>
			</div>
			<div id="<%=IDPrefix%>co_popSSNSastisfy" data-role="popup" style="max-width: 400px;">
				<div data-role="content">
					<div class="row">
						<div class="col-xs-12 header_theme2">
							<a data-rel="back" class="pull-right svg-btn" href="#"><%=HeaderUtils.IconClose %><span style="display: none;">close</span></a>
						</div>
					</div>
					<div class="row">
						<div class="col-sm-12">
							<div style="margin: 10px 0; font-weight: normal;">
								Your Social Security Number (SSN) is used for identification purposes and to determine your account opening eligibility.
							</div>     
						</div>    
					</div>
				</div>
			</div>
			<%=HeaderUtils.RenderPageTitle(14, "Current Address", True)%>
			<iframe name="<%=IDPrefix%>co_if_Address"  style="display: none;" src="about:blank" ></iframe>
			<form target="<%=IDPrefix%>co_if_Address" action="/handler/AutoFillHandler.aspx" id="<%=IDPrefix%>co_frm_Address">
				<div style="margin-top: 10px;">
					<a href="#" class="header_theme2 shadow-btn chevron-circle-right-before" id="<%=IDPrefix%>co_btnCopyAddress" onclick="ASI.FACTORY.<%=IDPrefix%>CopyPrimaryAddress(event, 'co_');">Copy the Co-Applicant's Address</a>
				</div>
				<div id="<%=IDPrefix%>co_div_ddl_country" class="hidden">
					<div data-role="fieldcontain">
						<label for="<%=IDPrefix%>co_ddlCountry" class="RequiredIcon">Country</label>
						<select id="<%=IDPrefix%>co_ddlCountry" name="country" autocomplete="country" onchange="ASI.FACTORY.<%=IDPrefix%>showHideState('co_')">
							<option value="USA" selected="selected">USA</option>
							<option value="FRANCE">France</option>
							<option value="GERMANY">Germany</option>
							<option value="ITALY">Italy</option>
							<option value="SPAIN">Spain</option>
							<option value="UNITED KINGDOM">United Kingdom</option>
							<option value="AALAND">Aaland</option>
							<option value="ABKHAZIA">Abkhazia</option>
							<option value="AFGHANISTAN">Afghanistan</option>
							<option value="ALBANIA">Albania</option>
							<option value="ALGERIA">Algeria</option>
							<option value="AMERICAN SAMOA">American Samoa</option>
							<option value="ANDORRA">Andorra</option>
							<option value="ANGOLA">Angola</option>
							<option value="ANGUILLA">Anguilla</option>
							<option value="ANTARCTICA">Antarctica</option>
							<option value="ANTIGUA_BARBUDA">Antigua And Barbuda</option>
							<option value="ARGENTINA">Argentina</option>
							<option value="ARMENIA">Armenia</option>
							<option value="ARUBA">Aruba</option>
							<option value="AUSTRALIA">Australia</option>
							<option value="AUSTRIA">Austria</option>
							<option value="AZERBAIJAN">Azerbaijan</option>
							<option value="BAHAMAS">Bahamas</option>
							<option value="BAHRAIN">Bahrain</option>
							<option value="BANGLADESH">Bangladesh</option>
							<option value="BARBADOS">Barbados</option>
							<option value="BELARUS">Belarus</option>
							<option value="BELGIUM">Belgium</option>
							<option value="BELIZE">Belize</option>
							<option value="BENIN">Benin</option>
							<option value="BERMUDA">Bermuda</option>
							<option value="BHUTAN">Bhutan</option>
							<option value="BOLIVIA">Bolivia</option>
							<option value="BOSNIA_HERZEGOVINA">Bosnia And Herzegovina</option>
							<option value="BOTSWANA">Botswana</option>
							<option value="BOUVET ISLAND">Bouvet Island</option>
							<option value="BRAZIL">Brazil</option>
							<option value="BRITISH INDIAN OCEAN">British Indian Ocean Territory</option>
							<option value="BRITISH_VIRGIN_ILND">British Virgin Islands</option>
							<option value="BRUNEI">Brunei</option>
							<option value="BULGARIA">Bulgaria</option>
							<option value="BURKINA FASO">Burkina Faso</option>
							<option value="BURUNDI">Burundi</option>
							<option value="CAMBODIA">Cambodia</option>
							<option value="CAMEROON">Cameroon</option>
							<option value="CANADA">Canada</option>
							<option value="CAPE VERDE">Cape Verde</option>
							<option value="CAYMAN ISLANDS">Cayman Islands</option>
							<option value="CENTRAL AFRICAN REP">Central African Republic</option>
							<option value="CHAD">Chad</option>
							<option value="CHILE">Chile</option>
							<option value="CHINA">China</option>
							<option value="CHRISTMAS ISLAND">Christmas Island</option>
							<option value="COCOS ISLANDS">Cocos Islands</option>
							<option value="COLOMBIA">Colombia</option>
							<option value="COMOROS">Comoros</option>
							<option value="COOK ISLANDS">Cook Islands</option>
							<option value="COSTA RICA">Costa Rica</option>
							<option value="COTE D&#39;IVOIRE">Cote D&#39;Ivoire</option>
							<option value="CROATIA">Croatia</option>
							<option value="CUBA">Cuba</option>
							<option value="CYPRUS">Cyprus</option>
							<option value="CZECH REPUBLIC">Czech Republic</option>
							<option value="DEMOCRATIC_REP_CONGO">Democratic Republic Of The Congo</option>
							<option value="DENMARK">Denmark</option>
							<option value="DJIBOUTI">Djibouti</option>
							<option value="DOMINICA">Dominica</option>
							<option value="DOMINICAN REPUBLIC">Dominican Republic</option>
							<option value="TIMOR-LESTE">East Timor</option>
							<option value="ECUADOR">Ecuador</option>
							<option value="EGYPT">Egypt</option>
							<option value="EL SALVADOR">El Salvador</option>
							<option value="EQUATORIAL GUINEA">Equatorial Guinea</option>
							<option value="ERITREA">Eritrea</option>
							<option value="ESTONIA">Estonia</option>
							<option value="ETHIOPIA">Ethiopia</option>
							<option value="FALKLAND ISLANDS">Falkland Islands</option>
							<option value="FAROE ISLANDS">Faroe Islands</option>
							<option value="FIJI">Fiji</option>
							<option value="FINLAND">Finland</option>
							<option value="FRENCH GUIANA">French Guiana</option>
							<option value="FRENCH POLYNESIA">French Polynesia</option>
							<option value="FRENCH_SOUTHERN_TERR">French Southern Territories</option>
							<option value="GABON">Gabon</option>
							<option value="GAMBIA">Gambia</option>
							<option value="GEORGIA">Georgia</option>
							<option value="GHANA">Ghana</option>
							<option value="GIBRALTAR">Gibraltar</option>
							<option value="GREECE">Greece</option>
							<option value="GREENLAND">Greenland</option>
							<option value="GRENADA">Grenada</option>
							<option value="GUADELOUPE">Guadeloupe</option>
							<option value="GUAM">Guam</option>
							<option value="GUATEMALA">Guatemala</option>
							<option value="GUERNSEY">Guernsey</option>
							<option value="GUINEA">Guinea</option>
							<option value="GUINEA BISSAU">Guinea Bissau</option>
							<option value="GUYANA">Guyana</option>
							<option value="HAITI">Haiti</option>
							<option value="HEARD_MCDONALD_ILND">Heard Island And Mcdonald Islands</option>
							<option value="HONDURAS">Honduras</option>
							<option value="HONG KONG">Hong Kong</option>
							<option value="HUNGARY">Hungary</option>
							<option value="ICELAND">Iceland</option>
							<option value="INDIA">India</option>
							<option value="INDONESIA">Indonesia</option>
							<option value="IRAN">Iran</option>
							<option value="IRAQ">Iraq</option>
							<option value="IRELAND">Ireland</option>
							<option value="ISLE OF MAN">Isle Of Man</option>
							<option value="ISRAEL">Israel</option>
							<option value="JAMAICA">Jamaica</option>
							<option value="JAPAN">Japan</option>
							<option value="JERSEY">Jersey</option>
							<option value="JORDAN">Jordan</option>
							<option value="KAZAKHSTAN">Kazakhstan</option>
							<option value="KENYA">Kenya</option>
							<option value="KIRIBATI">Kiribati</option>
							<option value="KOSOVO">Kosovo</option>
							<option value="KUWAIT">Kuwait</option>
							<option value="KYRGYZSTAN">Kyrgyzstan</option>
							<option value="LAOS">Laos</option>
							<option value="LATVIA">Latvia</option>
							<option value="LEBANON">Lebanon</option>
							<option value="LESOTHO">Lesotho</option>
							<option value="LIBERIA">Liberia</option>
							<option value="LIBYA">Libya</option>
							<option value="LIECHTENSTEIN">Liechtenstein</option>
							<option value="LITHUANIA">Lithuania</option>
							<option value="LUXEMBOURG">Luxembourg</option>
							<option value="MACAU">Macau</option>
							<option value="MACEDONIA">Macedonia</option>
							<option value="MADAGASCAR">Madagascar</option>
							<option value="MALAWI">Malawi</option>
							<option value="MALAYSIA">Malaysia</option>
							<option value="MALDIVES">Maldives</option>
							<option value="MALI">Mali</option>
							<option value="MALTA">Malta</option>
							<option value="MARSHALL ISLANDS">Marshall Islands</option>
							<option value="MARTINIQUE">Martinique</option>
							<option value="MAURITANIA">Mauritania</option>
							<option value="MAURITIUS">Mauritius</option>
							<option value="MAYOTTE">Mayotte</option>
							<option value="MEXICO">Mexico</option>
							<option value="MICRONESIA">Micronesia</option>
							<option value="MOLDOVA">Moldova</option>
							<option value="MONACO">Monaco</option>
							<option value="MONGOLIA">Mongolia</option>
							<option value="MONTENEGRO">Montenegro</option>
							<option value="MONTSERRAT">Montserrat</option>
							<option value="MOROCCO">Morocco</option>
							<option value="MOZAMBIQUE">Mozambique</option>
							<option value="MYANMAR">Myanmar</option>
							<option value="NAGORNO-KARABAKH">Nagorno-Karabakh</option>
							<option value="NAMIBIA">Namibia</option>
							<option value="NAURU">Nauru</option>
							<option value="NEPAL">Nepal</option>
							<option value="NETHERLANDS">Netherlands</option>
							<option value="NETHERLANDS ANTILLES">Netherlands Antilles</option>
							<option value="NEW CALEDONIA">New Caledonia</option>
							<option value="NEW ZEALAND">New Zealand</option>
							<option value="NICARAGUA">Nicaragua</option>
							<option value="NIGER">Niger</option>
							<option value="NIGERIA">Nigeria</option>
							<option value="NIUE">Niue</option>
							<option value="NORFOLK ISLAND">Norfolk Island</option>
							<option value="NORTH KOREA">North Korea</option>
							<option value="NORTHERN CYPRUS">Northern Cyprus</option>
							<option value="NORTHERN_MARIANA_ILN">Northern Mariana Islands</option>
							<option value="NORWAY">Norway</option>
							<option value="OMAN">Oman</option>
							<option value="PAKISTAN">Pakistan</option>
							<option value="PALAU">Palau</option>
							<option value="PALESTINE">Palestine</option>
							<option value="PANAMA">Panama</option>
							<option value="PAPUA NEW GUINEA">Papua New Guinea</option>
							<option value="PARAGUAY">Paraguay</option>
							<option value="PERU">Peru</option>
							<option value="PHILIPPINES">Philippines</option>
							<option value="PITCAIRN">Pitcairn</option>
							<option value="POLAND">Poland</option>
							<option value="PORTUGAL">Portugal</option>
							<option value="PUERTO RICO">Puerto Rico</option>
							<option value="QATAR">Qatar</option>
							<option value="CONGO">Republic Of The Congo</option>
							<option value="REUNION">Reunion</option>
							<option value="ROMANIA">Romania</option>
							<option value="RUSSIA">Russia</option>
							<option value="RWANDA">Rwanda</option>
							<option value="SAINT BARTHELEMY">Saint Barthelemy</option>
							<option value="SAINT HELENA">Saint Helena</option>
							<option value="SAINT_KITTS_NEVIS">Saint Kitts And Nevis</option>
							<option value="SAINT LUCIA">Saint Lucia</option>
							<option value="SAINT_VINCENT_GREN">Saint Vincent And The Grenadines</option>
							<option value="SAINT-MARTIN">Saint-Martin</option>
							<option value="SAINTPIERRE_MIQUELON">Saint-Pierre And Miquelon</option>
							<option value="SAMOA">Samoa</option>
							<option value="SAN MARINO">San Marino</option>
							<option value="SAO_TOME_PRINCIPE">Sao Tome And Principe</option>
							<option value="SAUDI ARABIA">Saudi Arabia</option>
							<option value="SENEGAL">Senegal</option>
							<option value="SERBIA">Serbia</option>
							<option value="SEYCHELLES">Seychelles</option>
							<option value="SIERRA LEONE">Sierra Leone</option>
							<option value="SINGAPORE">Singapore</option>
							<option value="SLOVAKIA">Slovakia</option>
							<option value="SLOVENIA">Slovenia</option>
							<option value="SOLOMON ISLANDS">Solomon Islands</option>
							<option value="SOMALIA">Somalia</option>
							<option value="SOMALILAND">Somaliland</option>
							<option value="SOUTH AFRICA">South Africa</option>
							<option value="S_GEORGIA_SANDWICH">South Georgia And The South Sandwich Islands</option>
							<option value="SOUTH KOREA">South Korea</option>
							<option value="SOUTH OSSETIA">South Ossetia</option>
							<option value="SRI LANKA">Sri Lanka</option>
							<option value="SUDAN">Sudan</option>
							<option value="SURINAME">Suriname</option>
							<option value="SVALBARD_JAN_MAYEN">Svalbard And Jan Mayen</option>
							<option value="SWAZILAND">Swaziland</option>
							<option value="SWEDEN">Sweden</option>
							<option value="SWITZERLAND">Switzerland</option>
							<option value="SYRIA">Syria</option>
							<option value="TAIWAN">Taiwan</option>
							<option value="TAJIKISTAN">Tajikistan</option>
							<option value="TANZANIA">Tanzania</option>
							<option value="THAILAND">Thailand</option>
							<option value="TOGO">Togo</option>
							<option value="TOKELAU">Tokelau</option>
							<option value="TONGA">Tonga</option>
							<option value="TRANSNISTRIA">Transnistria</option>
							<option value="TRINIDAD AND TOBAGO">Trinidad And Tobago</option>
							<option value="TRISTAN DA CUNHA">Tristan Da Cunha</option>
							<option value="TUNISIA">Tunisia</option>
							<option value="TURKEY">Turkey</option>
							<option value="TURKMENISTAN">Turkmenistan</option>
							<option value="TURKS_CAICOS_ISLANDS">Turks And Caicos Islands</option>
							<option value="TUVALU">Tuvalu</option>
							<option value="UGANDA">Uganda</option>
							<option value="UKRAINE">Ukraine</option>
							<option value="UNITED ARAB EMIRATES">United Arab Emirates</option>
							<option value="US_MINOR_ISLANDS">United States Minor Outlying Islands</option>
							<option value="US_VIRGIN_ISLANDS">United States Virgin Islands</option>
							<option value="URUGUAY">Uruguay</option>
							<option value="UZBEKISTAN">Uzbekistan</option>
							<option value="VANUATU">Vanuatu</option>
							<option value="VATICAN CITY">Vatican City</option>
							<option value="VENEZUELA">Venezuela</option>
							<option value="VIETNAM">Vietnam</option>
							<option value="WALLIS AND FUTUNA">Wallis And Futuna</option>
							<option value="WESTERN SAHARA">Western Sahara</option>
							<option value="YEMEN">Yemen</option>
							<option value="ZAMBIA">Zambia</option>
							<option value="ZIMBABWE">Zimbabwe</option>
						</select>
					</div>
				</div>
				<div data-role="fieldcontain">
					<label for="<%=IDPrefix%>co_txtAddress" class="RequiredIcon">Address</label>
					<input type="text" id="<%=IDPrefix%>co_txtAddress" maxlength="100" name="address1" autocomplete="street-address"/>
				</div>
				<div data-role="fieldcontain" id="<%=IDPrefix%>co_divAddress2" style="display: none;">
					<label for="<%=IDPrefix%>co_txtAddress2">Address Line 2</label>
					<input type="text" id="<%=IDPrefix%>co_txtAddress2" maxlength="100" name="address2" autocomplete="address-line2"/>
				</div>
				<div data-role="fieldcontain">
					<label for="<%=IDPrefix%>co_txtZip" id="<%=IDPrefix%>co_lblZipTitle" class="RequiredIcon">Zip</label>
					<input type="tel" id="<%=IDPrefix%>co_txtZip" maxlength="10" pattern="[0-9]*" name="zip" autocomplete="postal-code"/>
				</div>

				<div data-role="fieldcontain">
					<label for="<%=IDPrefix%>co_txtCity" id="<%=IDPrefix%>co_lblCityTitle" class="RequiredIcon">City</label>
					<input type="text" id="<%=IDPrefix%>co_txtCity" maxlength="50" name="city" autocomplete="address-level2"/>
				</div>
				<div class="<%=IDPrefix%>co_div_ddl_state">
					<div data-role="fieldcontain">
						<label for="<%=IDPrefix%>co_ddlState" class="RequiredIcon">State</label>
						<select id="<%=IDPrefix%>co_ddlState" name="state" autocomplete="address-level1">
							<%=Common.RenderStateDropdownlistWithEmpty(CEnum.STATES, "", "--Please Select--")%>
						</select>
					</div>
				</div>
				<div><span id="<%=IDPrefix%>co_spVerifyMessage" class="require-span" style="display: none;">Unable to validate address</span> </div>
			</form>
			<%If EnableCoAppSpouseContactInfo Then%>
			<div id="<%=IDPrefix%>co_divNSSContactInfo" <%=IIf(IsInMode("777") AndAlso IsInFeature("visibility"), "class=""showfield-section"" data-show-field-section-id=""" & BuildShowFieldSectionID("co_divNSSContactInfo") & """ data-default-state=""off"" data-section-name= 'Co-Applicant&#39;s Spouse Contact'", "")%>>
				<%=HeaderUtils.RenderPageTitle(22, "Contact Information", True)%>
				<iframe name="<%=IDPrefix%>co_if_ContactInfo" style="display: none;" src="about:blank" ></iframe>
				<form target="<%=IDPrefix%>co_if_ContactInfo" action="/handler/AutoFillHandler.aspx" id="<%=IDPrefix%>co_frm_ContactInfo">
					<div data-role="fieldcontain">
						<label id="<%=IDPrefix%>co_lblEmail" for="<%=IDPrefix%>co_txtEmail" class="RequiredIcon">Email</label>
						<input type="email" name="email" autocomplete="email" id="<%=IDPrefix%>co_txtEmail" maxlength ="50"/>
					</div>
					<div data-role="fieldcontain" id="<%=IDPrefix%>co_divHomePhone">
						<div>
							<div style="display: inline-block;"><label id="<%=IDPrefix%>co_lblHomePhone" for="<%=IDPrefix%>co_txtHomePhone" style="white-space: nowrap;">Home Phone</label></div>
							<div style="display: inline-block;"><label class="phone_format rename-able" style="white-space: nowrap; font-style: italic;">(xxx) xxx-xxxx</label></div>
						</div>
						<input type="<%=TextAndroidTel%>" pattern="[0-9]*" autocomplete="tel" name="phone" id="<%=IDPrefix%>co_txtHomePhone" class="inphone" maxlength='14'/>
					</div>
					<div data-role="fieldcontain" id="<%=IDPrefix%>co_divMobilePhone">
						<div>
							<div style="display: inline-block;"><label id="<%=IDPrefix%>co_lblMobilePhone" for="<%=IDPrefix%>co_txtMobilePhone"  style="white-space: nowrap;">Cell Phone</label></div>
							<div style="display: inline-block;"><label class="phone_format rename-able" style="white-space: nowrap; font-style: italic;">(xxx) xxx-xxxx</label></div>
						</div>
						<input type= "<%=TextAndroidTel%>" pattern="[0-9]*" autocomplete="tel" name="mobile" id="<%=IDPrefix%>co_txtMobilePhone" class="inphone" maxlength='14'/>
					</div>
				</form>
			</div>
			<%End If %>
			
			<br/>
			<div id="div<%=IDPrefix%>co_DisclosureContainer" class="header_theme">
			</div>
		</div>
	 </div>
    <div class ="div-continue"  data-role="footer">
        <a href="#" data-transition="slide" type="button" onclick="ASI.FACTORY.validatePage<%=IDPrefix%>('co_')" class="div-continue-button">Continue</a> 
        <a href="#divErrorDialog" style="display: none;">no text</a>
        <span> Or</span>  <a href="#" onclick="ASI.FACTORY.goToPreviousPage('co_');" class ="div-goback" data-corners="false" data-shadow="false" data-theme="reset"><span class="hover-goback"> Go Back</span></a>
    </div>
</div>
<script type="text/javascript">
	
	(function (ASI, $, undefined) {
		ASI.DATA = {currentFullAddress: "", co_currentFullAddress: "", isUsaAddress: true, co_isUsaAddress: true};
		ASI.FACTORY = {};
		ASI.DATA.nssList = <%=JsonConvert.SerializeObject(NSSList)%>;

		ASI.FACTORY.runNssWf1= function(stateCode, maritalStatus, goToPageSubmitCallback) {
			//TODO: clean review for Primary App's Spouse
			cleanNssReview("");
			cleanNssReview("co_");
			if (ASI.DATA.nssList[stateCode.toUpperCase()]) {
				var $ddlIsMarried = $("#<%=IDPrefix%>ddlIsMarried");
				$ddlIsMarried.selectmenu().selectmenu("enable");
				$("#div<%=IDPrefix%>DisclosureContainer").html("").append(
					_.map(ASI.DATA.nssList[stateCode.toUpperCase()], function(disclosure) {return $("<div/>").html(disclosure);})
				);
				ASI.DATA["hasCoApp"] = false;
				ASI.DATA["maritalStatus"] = maritalStatus;
				ASI.DATA["stateCode"] = stateCode;
				ASI.DATA.goToPageSubmitCallback = goToPageSubmitCallback;
				if (maritalStatus == "MARRIED") {
					$ddlIsMarried.val("true").selectmenu("disable").selectmenu("refresh");
					$("#<%=IDPrefix%>divFullPageContainer").removeClass("hidden");
					goToNextPage("#page<%=IDPrefix%>");
				} else if (typeof maritalStatus != "undefined" && maritalStatus != "") {
					// None/Separated/Registered Domestic Partner/Unmarried
					ASI.DATA["pageBeforePageSubmit"] = "<%=PreviousPage%>";
					goToPageSubmitCallback();
				} else {
					if ($ddlIsMarried.val() != "true" || typeof maritalStatus != "undefined") {
						$ddlIsMarried.val("false").selectmenu("refresh");
						$("#<%=IDPrefix%>divFullPageContainer").addClass("hidden");
					}
					goToNextPage("#page<%=IDPrefix%>");
				}
			} else {
				ASI.DATA["pageBeforePageSubmit"] = "<%=PreviousPage%>";
				goToPageSubmitCallback();
			}
		}
		ASI.FACTORY.goBackFromPageSubmit = function() {
			goToNextPage("#" + ASI.DATA["pageBeforePageSubmit"]);
		}
		ASI.FACTORY.runNssWf2= function(stateCode, maritalStatus, relationshipToPrimaryApp, coAppStateCode, coAppMaritalStatus, goToPageSubmitCallback) {
			//TODO: clean review for Primary App's Spouse and Co-App's Spouse
			cleanNssReview("");
			cleanNssReview("co_");
			ASI.DATA.primaryAppSpouseAvaialble = false;
			if (relationshipToPrimaryApp.toUpperCase() == "SPOUSE") {
				ASI.DATA["pageBeforePageSubmit"] = "<%=PreviousCoAppPage%>";
				goToPageSubmitCallback();
			} else {
				ASI.DATA["hasCoApp"] = true;
				ASI.DATA["maritalStatus"] = maritalStatus;
				ASI.DATA["stateCode"] = stateCode;
				ASI.DATA["relationshipToPrimaryApp"] = relationshipToPrimaryApp;
				ASI.DATA["coAppStateCode"] = coAppStateCode;
				ASI.DATA["coAppMaritalStatus"] = coAppMaritalStatus;
				ASI.DATA.goToPageSubmitCallback = goToPageSubmitCallback;
				if (ASI.DATA.nssList[stateCode.toUpperCase()]) {
					var $ddlIsMarried = $("#<%=IDPrefix%>ddlIsMarried");
					$ddlIsMarried.selectmenu().selectmenu("enable");
					$("#div<%=IDPrefix%>DisclosureContainer").html("").append(
						_.map(ASI.DATA.nssList[stateCode.toUpperCase()], function(disclosure) {
							return $("<div/>").html(disclosure);
						})
					);
					if (maritalStatus == "MARRIED") {
						$ddlIsMarried.val("true").selectmenu("disable").selectmenu("refresh");
						$("#<%=IDPrefix%>divFullPageContainer").removeClass("hidden");
						goToNextPage("#page<%=IDPrefix%>");
					} else if (typeof maritalStatus != "undefined" && maritalStatus != "") {
						// None/Separated/Registered Domestic Partner/Unmarried
						if(ASI.DATA.nssList[coAppStateCode.toUpperCase()]) {
							runNssWfForCoApp(coAppMaritalStatus, coAppStateCode);
						} else {
							ASI.DATA["pageBeforePageSubmit"] = "<%=PreviousCoAppPage%>";
							goToPageSubmitCallback();
						}
					} else {
						if ($ddlIsMarried.val() != "true" || typeof maritalStatus != "undefined") {
							$ddlIsMarried.val("false").selectmenu("refresh");
							$("#<%=IDPrefix%>divFullPageContainer").addClass("hidden");
						}
						goToNextPage("#page<%=IDPrefix%>");
					}
				} else if (ASI.DATA.nssList[coAppStateCode.toUpperCase()]) {
					runNssWfForCoApp(coAppMaritalStatus, coAppStateCode);
				} else {
					ASI.DATA["pageBeforePageSubmit"] = "<%=PreviousCoAppPage%>";
					goToPageSubmitCallback();
				} 
			}
		}
		function runNssWfForCoApp(maritalStatus, stateCode) {
			var $ddlCoIsMarried = $("#<%=IDPrefix%>co_ddlIsMarried");
			$ddlCoIsMarried.selectmenu().selectmenu("enable");
			$("#div<%=IDPrefix%>co_DisclosureContainer").html("").append(
				_.map(ASI.DATA.nssList[stateCode.toUpperCase()], function(disclosure) {
					return $("<div/>").html(disclosure);
				})
			);
			if (maritalStatus == "MARRIED") {
				$ddlCoIsMarried.val("true").selectmenu("disable").selectmenu("refresh");
				$("#<%=IDPrefix%>co_divFullPageContainer").removeClass("hidden");
				goToNextPage("#pageco_<%=IDPrefix%>");
			} else if (typeof maritalStatus != "undefined" && maritalStatus != "") {
				// None/Separated/Registered Domestic Partner/Unmarried
				ASI.DATA["pageBeforePageSubmit"] = "<%=PreviousCoAppPage%>";
				ASI.DATA.goToPageSubmitCallback();
			} else {
				if ($ddlCoIsMarried.val() != "true" || typeof maritalStatus != "undefined") {
					$ddlCoIsMarried.val("false").selectmenu("refresh");
					$("#<%=IDPrefix%>co_divFullPageContainer").addClass("hidden");	
				}
				goToNextPage("#pageco_<%=IDPrefix%>");
			}
		}
		ASI.FACTORY.goToPreviousPage = function(coAppPrefix) {
			coAppPrefix = coAppPrefix || "";
			if (coAppPrefix == "co_") {
				if (ASI.DATA.primaryAppSpouseAvaialble == true) {
					goToNextPage("#page<%=IDPrefix%>");
				} else {
					goToNextPage("#<%=PreviousCoAppPage%>");
				}
			} else if (ASI.DATA["hasCoApp"] == true) {
				goToNextPage("#<%=PreviousCoAppPage%>");
			} else {
				goToNextPage("#<%=PreviousPage%>");
			}
		}
		ASI.FACTORY.validatePage<%=IDPrefix%> = function(coAppPrefix) {
			coAppPrefix = coAppPrefix || "";
			if (!$.lpqValidate("<%=IDPrefix%>" + coAppPrefix + "Validate")) {
				Common.ScrollToError();
				return false;
			}
			if ($("#<%=IDPrefix%>" + coAppPrefix + "ddlIsMarried").val() == "true"){
				var infoValidator = $.lpqValidate("<%=IDPrefix%>" + coAppPrefix + "ValidateApplicantAdditionalInfo");
				var addressValidator = $.lpqValidate("<%=IDPrefix%>" + coAppPrefix + "ValidateApplicantAdditionalAddress");
				var contactValidator = $.lpqValidate("<%=IDPrefix%>" + coAppPrefix + "ValidateApplicantAdditionalContactInfo");
				
				if (infoValidator == false || addressValidator == false || contactValidator == false) {
					Common.ScrollToError();
					return false;
				}

			}
			document.getElementById("<%=IDPrefix%>" + coAppPrefix + "frm_ApplicantInfo").submit();
			document.getElementById("<%=IDPrefix%>" + coAppPrefix + "frm_Address").submit();
			if ($("#<%=IDPrefix%>" + coAppPrefix + "frm_ContactInfo").length > 0) {
				document.getElementById("<%=IDPrefix%>" + coAppPrefix + "frm_ContactInfo").submit();
			}
			if (coAppPrefix == "co_") {
				renderNssReview(coAppPrefix);
				ASI.DATA["pageBeforePageSubmit"] = "pageco_<%=IDPrefix%>";
				ASI.DATA.goToPageSubmitCallback();
			} else {
				cleanNssReview("co_");
				if (ASI.DATA["hasCoApp"]) {
					renderNssReview(coAppPrefix);
					ASI.DATA.primaryAppSpouseAvaialble = true;
					if (ASI.DATA.nssList[ASI.DATA["coAppStateCode"].toUpperCase()]) {
						runNssWfForCoApp(ASI.DATA["coAppMaritalStatus"], ASI.DATA["coAppStateCode"]);
					} else {
						renderNssReview(coAppPrefix);
						ASI.DATA["pageBeforePageSubmit"] = "page<%=IDPrefix%>";
						ASI.DATA.goToPageSubmitCallback();
					}
				} else {
					renderNssReview(coAppPrefix);
					ASI.DATA["pageBeforePageSubmit"] = "page<%=IDPrefix%>";
					ASI.DATA.goToPageSubmitCallback();
				}
			}
		}
		function renderNssReview(coAppPrefix) {
			coAppPrefix = coAppPrefix || "";
			var $targetDiv = coAppPrefix == "co_" ? $(".ViewCoAppSpouseInfo") : $(".ViewPrimaryAppSpouseInfo");
			var $ddlIsMarried = $("#<%=IDPrefix%>" + coAppPrefix + "ddlIsMarried");
			var hasData = $ddlIsMarried.val() == "true";
			$targetDiv.html("");
			if (hasData) {
				var firstName = $("#<%=IDPrefix%>" + coAppPrefix + "txtFName").val();
				var middleName = $("#<%=IDPrefix%>" + coAppPrefix + "txtMName").val();
				var lastName = $("#<%=IDPrefix%>" + coAppPrefix + "txtLName").val();
				$targetDiv.append(Common.buildReviewBlock("Full Name", Common.concatenateFullName(firstName, middleName, lastName)));
				var $ssn = $("#<%=IDPrefix%>" + coAppPrefix + "txtSSN");
				$targetDiv.append(Common.buildReviewBlock("SSN", Common.GetSSN($ssn.val()).replace(/^\d{5}/, "*****")));
				var address = $("#<%=IDPrefix%>" + coAppPrefix + "txtAddress").val();
				var address2 = $("#<%=IDPrefix%>" + coAppPrefix + "txtAddress2").val();
				var city = $("#<%=IDPrefix%>" + coAppPrefix + "txtCity").val();
				var zip = $("#<%=IDPrefix%>" + coAppPrefix + "txtZip").val();
				var state = $("#<%=IDPrefix%>" + coAppPrefix + "ddlState").val();
				var country = $("#<%=IDPrefix%>" + coAppPrefix + "ddlCountry").val();
				$targetDiv.append(Common.buildReviewBlock("Address", Common.concatenateAddress(address, address2, city, zip, state, country)));

				if ($("#<%=IDPrefix%>" + coAppPrefix + "frm_ContactInfo").length > 0 && ($("#<%=IDPrefix%>" + coAppPrefix + "frm_ContactInfo").closest("div.showfield-section").length == 0 || $("#<%=IDPrefix%>" + coAppPrefix + "frm_ContactInfo").closest("div.showfield-section").hasClass("hidden") == false)) {
					var email = htmlEncode($("#<%=IDPrefix%>" + coAppPrefix + "txtEmail").val().trim());
					var homePhone = $("#<%=IDPrefix%>" + coAppPrefix + "txtHomePhone").val();
					var mobilePhone = $("#<%=IDPrefix%>" + coAppPrefix + "txtMobilePhone").val();
					$targetDiv.append(Common.buildReviewBlock("Email", email));
					if (homePhone != "") {
						$targetDiv.append(Common.buildReviewBlock("Home Phone", homePhone));
					}
					if (mobilePhone != "") {
						$targetDiv.append(Common.buildReviewBlock("Cell Phone", mobilePhone));
					}
				}

				$targetDiv.show();
				$targetDiv.prev("div.row").show();
				ASI.DATA[coAppPrefix + "hasSpouseInfo"] = true;
			} else {
				$targetDiv.hide();
				$targetDiv.prev("div.row").hide();
				ASI.DATA[coAppPrefix + "hasSpouseInfo"] = false;
			}
		}
		function cleanNssReview(coAppPrefix) {
			coAppPrefix = coAppPrefix || "";
			var $targetDiv = coAppPrefix == "co_" ? $(".ViewCoAppSpouseInfo") : $(".ViewPrimaryAppSpouseInfo");
			if ($targetDiv.length > 0) {
				$targetDiv.hide();
				$targetDiv.prev("div.row").hide();
				$targetDiv.html("");
				ASI.DATA[coAppPrefix + "hasSpouseInfo"] = false;
			}
		}
		function <%=IDPrefix%>updateHiddenSSN(coAppPrefix) {
			coAppPrefix = coAppPrefix || "";
			if ($("#<%=IDPrefix%>" + coAppPrefix + "txtSSN1").val().length == 3) {
				var ssn = $("#<%=IDPrefix%>" + coAppPrefix + "txtSSN1").val() + '-' + $("#<%=IDPrefix%>" + coAppPrefix + "txtSSN2").val() + '-' + $("#<%=IDPrefix%>" + coAppPrefix + "txtSSN3").val();
				$("#<%=IDPrefix%>" + coAppPrefix + "txtSSN").val(ssn);
			}
		}
		ASI.FACTORY.<%=IDPrefix%>toggleSSNText = function(btn, coAppPrefix) {
            coAppPrefix = coAppPrefix || "";
            Common.ToggleSSNText(btn, "<%=IDPrefix%>" + coAppPrefix);
		}
		ASI.FACTORY.setApplicantAdditionalInfo = function(appInfo) {
			if (ASI.DATA["hasSpouseInfo"]) {
				appInfo.AppSpouseInfo = JSON.stringify(collectSpouseInfo());
			}
			if (ASI.DATA["co_hasSpouseInfo"]) {
				appInfo.co_AppSpouseInfo =  JSON.stringify(collectSpouseInfo("co_"));
			}
		}
		function collectSpouseInfo(coAppPrefix) {
			var result = {};
			coAppPrefix = coAppPrefix || "";
			result["FirstName"] = $("#<%=IDPrefix%>" + coAppPrefix + "txtFName").val();
			result["MiddleName"] = $("#<%=IDPrefix%>" + coAppPrefix + "txtMName").val();
			result["LastName"] = $("#<%=IDPrefix%>" + coAppPrefix + "txtLName").val();
			result["Suffix"] = $("#<%=IDPrefix%>" + coAppPrefix + "suffix").val();
			result["SSN"] = Common.GetSSN($("#<%=IDPrefix%>" + coAppPrefix + "txtSSN").val());

			result["Address"] = $("#<%=IDPrefix%>" + coAppPrefix + "txtAddress").val();
			result["Zip"] = $("#<%=IDPrefix%>" + coAppPrefix + "txtZip").val();
			result["City"] = $("#<%=IDPrefix%>" + coAppPrefix + "txtCity").val();
    		if ($("#<%=IDPrefix%>" + coAppPrefix + "ddlCountry").val() == "USA") {
    			result["State"] = $("#<%=IDPrefix%>" + coAppPrefix + "ddlState").val();
			} else {
        		result["State"] = "";
        		result["Address2"] = $("#<%=IDPrefix%>" + coAppPrefix + "txtAddress2").val();
			}
			result["Country"] = $("#<%=IDPrefix%>" + coAppPrefix + "ddlCountry").val();

			if ($("#<%=IDPrefix%>" + coAppPrefix + "frm_ContactInfo").length > 0 && ($("#<%=IDPrefix%>" + coAppPrefix + "frm_ContactInfo").closest("div.showfield-section").length == 0 || $("#<%=IDPrefix%>" + coAppPrefix + "frm_ContactInfo").closest("div.showfield-section").hasClass("hidden") == false)) {
				result["Email"] = $("#<%=IDPrefix%>" + coAppPrefix + "txtEmail").val();
				result["HomePhone"] = $("#<%=IDPrefix%>" + coAppPrefix + "txtHomePhone").val();
				result["CellPhone"] = $("#<%=IDPrefix%>" + coAppPrefix + "txtMobilePhone").val();
			}

			return result;
		}
		ASI.FACTORY.init = function(coAppPrefix) {
			coAppPrefix = coAppPrefix || "";
			registerValidatorPage<%=IDPrefix%>(coAppPrefix);
			$("#<%=IDPrefix%>" + coAppPrefix + "ddlIsMarried").on("change", function() {
				var $self = $(this);
				var $ddlMaritalStatus = $("#" + coAppPrefix + "ddlMaritalStatus");
				if ($self.val() == "true") {
					$("#<%=IDPrefix%>" + coAppPrefix + "divFullPageContainer").removeClass("hidden");
					if ($ddlMaritalStatus.length > 0) {
						$ddlMaritalStatus.val("MARRIED").selectmenu("refresh");
					}
				} else {
					$("#<%=IDPrefix%>" + coAppPrefix + "divFullPageContainer").addClass("hidden");
					if ($ddlMaritalStatus.length > 0) {
						$ddlMaritalStatus.val("").selectmenu("refresh");
					}
				}
			});
			if (isMobile.any() && !isMobile.Windows()) {//moz-text-security does not works on window devices
				$("#<%=IDPrefix%>" + coAppPrefix + "txtSSN1,#<%=IDPrefix%>" + coAppPrefix + "txtSSN2").attr("type", "tel").addClass("mask-password");
			} else {
				$("#<%=IDPrefix%>" + coAppPrefix + "txtSSN1,#<%=IDPrefix%>" + coAppPrefix + "txtSSN2").attr("type", "password");
			}

			if ($('#hdForeignAppType').val()=='Y') {
				$("#<%=IDPrefix%>" + coAppPrefix + "lblSSN").removeClass("RequiredIcon"); // no required ->remove red stard
			} else {
				$("#<%=IDPrefix%>" + coAppPrefix + "lblSSN").addClass("RequiredIcon"); // required -> add restar
			}
			$("#<%=IDPrefix%>" + coAppPrefix + "popSSNSastisfy").on("popupafteropen", function (event, ui) {
				//prevent scrollbar by popup overlay
				$("#" + this.id + "-screen").height("");
			});
			$("#<%=IDPrefix%>" + coAppPrefix + "popSSNSastisfy").on("popupafterclose", function (event, ui) {
				setTimeout(function () {
					$("#<%=IDPrefix%>" + coAppPrefix + "spOpenSsnSastisfy").focus();
				}, 100);
			});
			<%=IDPrefix%>InitAddressSection(coAppPrefix);
		}

		function <%=IDPrefix%>InitAddressSection(coAppPrefix) {
			coAppPrefix = coAppPrefix || "";
			ASI.DATA[coAppPrefix +"currentFullAddress"] = <%=IDPrefix%>getAddress(coAppPrefix);
			<%--LPQ side does not support foreign address for NSS so we disable it--%>
			<%-- if (is_foreign_address) {
				$("#<%=IDPrefix%>" + coAppPrefix + "div_ddl_country").show();
			} else {
				$("#<%=IDPrefix%>" + coAppPrefix + "div_ddl_country").hide();
			}--%>
			if ($("#<%=IDPrefix%>" + coAppPrefix + "ddlCountry").val() == 'USA') {
				$("#<%=IDPrefix%>" + coAppPrefix + "divAddress2").hide();
				$("#<%=IDPrefix%>" + coAppPrefix + "txtZip").off('.alphanum');
				$("#<%=IDPrefix%>" + coAppPrefix + "txtZip").attr("maxlength", "10");
				$("#<%=IDPrefix%>" + coAppPrefix + "txtZip").attr("pattern", "[0-9]*");
    			if (isMobile.Android()) {
    				$("#<%=IDPrefix%>" + coAppPrefix + "txtZip").attr("type", "tel");
        		} else {
    				$("#<%=IDPrefix%>" + coAppPrefix + "txtZip").attr("type", "text");
        		}
			} else {
				$("#<%=IDPrefix%>" + coAppPrefix + "divAddress2").show();
				$("#<%=IDPrefix%>" + coAppPrefix + "txtZip").attr("maxlength", "10");
				$("#<%=IDPrefix%>" + coAppPrefix + "txtZip").attr("type", "text");
				$("#<%=IDPrefix%>" + coAppPrefix + "txtZip").alphanum({ allowSpace: true });
				$("#<%=IDPrefix%>" + coAppPrefix + "txtZip").removeAttr("pattern");
			}
			$("#<%=IDPrefix%>" + coAppPrefix + "frm_Address").find("input, select").on("change", function() {
				<%=IDPrefix%>verifyAddress(coAppPrefix);
			});
		}
		ASI.FACTORY.<%=IDPrefix%>showHideState = function(coAppPrefix) {
			coAppPrefix = coAppPrefix || "";
			ASI.DATA[coAppPrefix +"isUsaAddress"] = $("#<%=IDPrefix%>" + coAppPrefix + "ddlCountry").val() == "USA";
			if (ASI.DATA[coAppPrefix +"isUsaAddress"]) {
				$(".<%=IDPrefix%>" + coAppPrefix + "div_ddl_state").show();
				$(".<%=IDPrefix%>" + coAppPrefix + "lblZipTitle").addClass("RequiredIcon");

				ReplaceButtonLabel($("#<%=IDPrefix%>" + coAppPrefix + "lblZipTitle"), "Zip");
				ReplaceButtonLabel($("#<%=IDPrefix%>" + coAppPrefix + "lblCityTitle"), "City");

				$("#<%=IDPrefix%>" + coAppPrefix + "lblCityTitle").addClass("RequiredIcon");
				$("#<%=IDPrefix%>" + coAppPrefix + "divAddress2").hide();
				$("#<%=IDPrefix%>" + coAppPrefix + "txtZip").off('.alphanum');
				$("#<%=IDPrefix%>" + coAppPrefix + "txtZip").attr("maxlength", "10");
				$("#<%=IDPrefix%>" + coAppPrefix + "txtZip").attr("pattern", "[0-9]*");     
				var $zipCode = $("#<%=IDPrefix%>" + coAppPrefix + "txtZip");
    			if ($zipCode.val().length == 5) { //5 digit(xxxxx)
    				if (/^[0-9]{1,5}$/.test($zipCode.val()) === false) {
    					$zipCode.val("");
    				}
    			} else { //zipcode + 4 ext digits(xxxxx-xxxx)
    				var zipCode = $zipCode.val().substring(0, 5);
    				var zipCodeExt = $zipCode.val().substring(6, 10);  
    				if (/^[0-9]{1,5}$/.test(zipCode) === false || /^[0-9]{1,4}$/.test(zipCodeExt) === false) {       
    					$zipCode.val("");
    				}      
    			}
    			if (isMobile.Android()) {
    				$("#<%=IDPrefix%>" + coAppPrefix + "txtZip").attr("type", "tel");
				} else {
					$("#<%=IDPrefix%>" + coAppPrefix + "txtZip").attr("type", "text");
				}
			} else {
				$(".<%=IDPrefix%>" + coAppPrefix + "div_ddl_state").hide();
    			$(".<%=IDPrefix%>" + coAppPrefix + "lblZipTitle").removeClass("RequiredIcon");

    			ReplaceButtonLabel($("#<%=IDPrefix%>" + coAppPrefix + "lblZipTitle"), "Postal Code");
    			ReplaceButtonLabel($("#<%=IDPrefix%>" + coAppPrefix + "lblCityTitle"), "City/Province");

    			$("#<%=IDPrefix%>" + coAppPrefix + "lblCityTitle").removeClass("RequiredIcon");
    			$("#<%=IDPrefix%>" + coAppPrefix + "txtZip").removeAttr("pattern");
    			$("#<%=IDPrefix%>" + coAppPrefix + "txtZip").attr("maxlength", "10");
    			$("#<%=IDPrefix%>" + coAppPrefix + "txtZip").alphanum({ allowSpace: true });
    			$("#<%=IDPrefix%>" + coAppPrefix + "txtZip").attr("type", "text");
    			$("#<%=IDPrefix%>" + coAppPrefix + "divAddress2").show();
    		}
			$.lpqValidate.hideValidation($("#<%=IDPrefix%>" + coAppPrefix + "txtZip"));
		}
		function <%=IDPrefix%>validateAddress(coAppPrefix) {
			coAppPrefix = coAppPrefix || "";
			return (($("#<%=IDPrefix%>" + coAppPrefix + "txtAddress").val() != "" &&
                $("#<%=IDPrefix%>" + coAppPrefix + "txtZip").val() != "") ||
              ($("#<%=IDPrefix%>" + coAppPrefix + "txtAddress").val() != "" &&
              $("#<%=IDPrefix%>" + coAppPrefix + "txtCity").val() != "" &&
               $("#<%=IDPrefix%>" + coAppPrefix + "ddlState").val() != ""));
		}
		function <%=IDPrefix%>verifyAddress(coAppPrefix) {
			coAppPrefix = coAppPrefix || "";
			if (!ASI.DATA[coAppPrefix +"isUsaAddress"]) return;
			if (Common.ValidateZipCode($("#<%=IDPrefix%>" + coAppPrefix + "txtZip").val(), $("#<%=IDPrefix%>" + coAppPrefix + "txtZip")) == false) return;
			var address = <%=IDPrefix%>getAddress(coAppPrefix);
			if (<%=IDPrefix%>validateAddress(coAppPrefix) && ASI.DATA[coAppPrefix +"currentFullAddress"] != address) {
				var url = '/handler/Handler.aspx';
				$.ajax({
					url: url,
					async: true,
					cache: false,
					type: 'POST',
					dataType: 'html',
					data: {
						command: 'verifyAddress',
						address: address
					},
					success: function (responseText) {
						ASI.DATA[coAppPrefix +"currentFullAddress"] = <%=IDPrefix%>getAddress(coAppPrefix);
						if (responseText.indexOf('res="OK"') < 0) {
							//address service is down or configuration is bad, dont do anything
							//TODO: log the error or email admin   
						} else if (responseText.indexOf('FoundSingleAddress="False"') > -1) { //not found single address                      
							$("#<%=IDPrefix%>" + coAppPrefix + "spVerifyMessage").show();
    						var shortDesc = /ShortDescriptive="(.*?)"/g.exec(responseText);
    						if (shortDesc[1] == 'Not Found') {
    							shortDesc[1] = "Unable to Validate Address";
    						}
    						$("#<%=IDPrefix%>" + coAppPrefix + "spVerifyMessage").text(shortDesc[1]);
						} else if (responseText.indexOf('FoundSingleAddress="True"') > -1) { //found single address                      
							$("#<%=IDPrefix%>" + coAppPrefix + "spVerifyMessage").hide();
							<%=IDPrefix%>updateAddress(responseText, coAppPrefix);
						} else { //unknow case                      
							$("#<%=IDPrefix%>" + coAppPrefix + "spVerifyMessage").hide();
						}
    				}
    			});
			}
		}
		function registerValidatorPage<%=IDPrefix%>(coAppPrefix) {
			coAppPrefix = coAppPrefix || "";
			$("#<%=IDPrefix%>" + coAppPrefix + "ddlIsMarried").observer({
				validators: [
					function (partial) {
						if ($(this).val() == "") {
							return 'Please answer this question';
						}
						return "";
					}
    			],
				validateOnBlur: true,
				group: "<%=IDPrefix%>" + coAppPrefix + "Validate"
			});
			$("#<%=IDPrefix%>" + coAppPrefix + "txtFName").observer({
				validators: [
					function (partial) {
						var text = $(this).val();
						if (!Common.ValidateText(text)) {
							return 'First Name is required';
						} else if (/^[\sa-zA-Z\'-]+$/.test(text) == false) {
							return 'Enter a valid first name. Allowed characters: letters "A-Z", dashes, apostrophes and spaces.';
						}
						return "";
					}
				],
				validateOnBlur: true,
				group: "<%=IDPrefix%>" + coAppPrefix + "ValidateApplicantAdditionalInfo"
			});
			$("#<%=IDPrefix%>" + coAppPrefix + "txtLName").observer({
					validators: [
						function (partial) {
							var text = $(this).val();
							if (!Common.ValidateText(text)) {
								return 'Last Name is required';
							} else if (/^[\sa-zA-Z\'-]+$/.test(text) == false) {
								return 'Enter a valid last name. Allowed characters: letters "A-Z", dashes, apostrophes and spaces.';
							}
							return "";
						}
					],
					validateOnBlur: true,
					group: "<%=IDPrefix%>" + coAppPrefix + "ValidateApplicantAdditionalInfo"
			});
			$("#<%=IDPrefix%>" + coAppPrefix + "txtMName").observer({
					validators: [
						function (partial) {
							var text = $(this).val();
							if (/^[\sa-zA-Z\'-]*$/.test(text) == false) {
								return 'Enter a valid middle name. Allowed characters: letters "A-Z", dashes, apostrophes and spaces.';
							}
							return "";
						}
					],
					validateOnBlur: true,
					group: "<%=IDPrefix%>" + coAppPrefix + "ValidateApplicantAdditionalInfo"
			});
			$("#<%=IDPrefix%>" + coAppPrefix + "divSSN").observer({
				validators: [
					function (partial, evt) {
						<%=IDPrefix%>updateHiddenSSN(coAppPrefix);
						var ssn = $("#<%=IDPrefix%>" + coAppPrefix + "txtSSN1").val() + $("#<%=IDPrefix%>" + coAppPrefix + "txtSSN2").val() + $("#<%=IDPrefix%>" + coAppPrefix + "txtSSN3").val();
					
						if ($('#hdForeignAppType').val() == 'Y' && ssn === "") { //make ssn is not required if hasForeignAppType 
							return "";
						}
						if (/^[0-9]{9}$/.test(ssn) == false) {
							return 'Valid SSN is required';
						}
						if (ssn == Common.GetSSN($('#txtSSN').val())) {
							return "SSN shouldn't be the same as Primary Applicant";
						}
						if (ssn == Common.GetSSN($('#co_txtSSN').val())) {
							return "SSN shouldn't be the same as Co-Applicant";
						}
						if (coAppPrefix == "") {
							if (ssn == Common.GetSSN($("#<%=IDPrefix%>co_txtSSN").val())) {
								return "SSN shouldn't be the same as Co-Applicant's Spouse";
							}
						} else {
							if (ssn == Common.GetSSN($("#<%=IDPrefix%>txtSSN").val())) {
								return "SSN shouldn't be the same as Primary Applicant's Spouse";
							}
						}
						return "";
					}
				],
				validateOnBlur: true,
				group: "<%=IDPrefix%>" + coAppPrefix + "ValidateApplicantAdditionalInfo"
			});

			$("#<%=IDPrefix%>" + coAppPrefix + "txtAddress").observer({
				validators: [
					function (partial) {
						var address = $(this).val();
						if (!Common.ValidateText(address)) {
							return 'Address is required';
						} else if (ASI.DATA[coAppPrefix +"isUsaAddress"] && /^[\sa-zA-Z0-9\.'#/-]+$/.test(address) == false) {
                			return 'Invalid Address was entered.  Allowed characters: letters A-Z, numbers, hyphens, periods, forwardslashes, dashes, number signs, and apostrophes.';
                		}
						else if (ASI.DATA[coAppPrefix +"isUsaAddress"] && isPOBox(address)) {
							return address + " is not accepted. Please enter a different address";
						}
						return "";
					}
    			],
				validateOnBlur: true,
				group: "<%=IDPrefix%>" + coAppPrefix + "ValidateApplicantAdditionalAddress"
			});
			$("#<%=IDPrefix%>" + coAppPrefix + "txtZip").observer({
				validators: [
					function (partial) {
						if (ASI.DATA[coAppPrefix +"isUsaAddress"] && Common.ValidateZipCode($(this).val(), this) == false) {
                		return 'Valid zip is required.';
                		}
                		if (!Common.ValidateText($(this).val())) {
                			return "Zip is required";
                		}
                		return "";
					}
    			],
				validateOnBlur: true,
				group: "<%=IDPrefix%>" + coAppPrefix + "ValidateApplicantAdditionalAddress"
			});
			$("#<%=IDPrefix%>" + coAppPrefix + "txtCity").observer({
				validators: [
					function (partial) {
						var city = $(this).val();
						if (ASI.DATA[coAppPrefix +"isUsaAddress"] && !Common.ValidateText(city)) {
                			return 'City is required';
						} else if (ASI.DATA[coAppPrefix +"isUsaAddress"] && /^[\sa-zA-Z0-9\.'#/-]+$/.test(city) == false) {
                			return 'Invalid City was entered.  Allowed characters: letters A-Z, numbers, hyphens, periods, forwardslashes, dashes, number signs, and apostrophes.';
                		}
                		return "";
					}
    			],
				validateOnBlur: true,
				group: "<%=IDPrefix%>" + coAppPrefix + "ValidateApplicantAdditionalAddress"
			});
			$("#<%=IDPrefix%>" + coAppPrefix + "ddlState").observer({
				validators: [
					function (partial) {
						var state = $(this).val();
						if (ASI.DATA[coAppPrefix +"isUsaAddress"] && !Common.ValidateText(state)) {
                			return 'State is required';
                		}
                		return "";
					}
    			],
				validateOnBlur: true,
				group: "<%=IDPrefix%>" + coAppPrefix + "ValidateApplicantAdditionalAddress"
			});

			$("#<%=IDPrefix%>" + coAppPrefix + "txtEmail").observer({
				validators: [
					function (partial) {
						if ($(this).closest("div.showfield-section").length > 0 && $(this).closest("div.showfield-section").hasClass("hidden")) return "";
						var email = $(this).val();
						if (Common.ValidateEmail(email) == false) {
							return "Valid Email is required";
						}
						return "";
					}
				],
				validateOnBlur: true,
				group: "<%=IDPrefix%>" + coAppPrefix + "ValidateApplicantAdditionalContactInfo"
			});
			$("#<%=IDPrefix%>" + coAppPrefix + "txtHomePhone").observer({
				validators: [
					function (partial) {
						if ($(this).closest("div.showfield-section").length > 0 && $(this).closest("div.showfield-section").hasClass("hidden")) return "";
						var homephone = $(this).val();
						if ($.trim(homephone) != "") {
							if (!Common.ValidatePhone(homephone)) {
								return "Valid phone number is required";
							} else {
								//validate first digit of home phone : 0 and 1 is invalid
								var firstDigit = homephone.replace("(", "").substring(0, 1);
								if (firstDigit == 0 || firstDigit == 1) {
									return 'Phone number and area code can not begin with 0 or 1 and must be a valid phone number';
								}
							}
						}
						return "";
					}
				],
				validateOnBlur: true,
				group: "<%=IDPrefix%>" + coAppPrefix + "ValidateApplicantAdditionalContactInfo"
			});
			$("#<%=IDPrefix%>" + coAppPrefix + "txtMobilePhone").observer({
				validators: [
					function (partial) {
						if ($(this).closest("div.showfield-section").length > 0 && $(this).closest("div.showfield-section").hasClass("hidden")) return "";
						var mobile = $(this).val();
						if ($.trim(mobile) != "") {
							if (!Common.ValidatePhone(mobile)) {
								return "Valid phone number is required";
							} else {
								//validate first digit of home phone : 0 and 1 is invalid
								var firstDigit = mobile.replace("(", "").substring(0, 1);
								if (firstDigit == 0 || firstDigit == 1) {
									return 'Phone number and area code can not begin with 0 or 1 and must be a valid phone number';
								}
							}
						}
						return "";
					}
				],
				validateOnBlur: true,
				group: "<%=IDPrefix%>" + coAppPrefix + "ValidateApplicantAdditionalContactInfo"
			});
		}
		function <%=IDPrefix%>getAddress(coAppPrefix) {
			coAppPrefix = coAppPrefix || "";
			var zip = "";
			if ($("#<%=IDPrefix%>" + coAppPrefix + "ddlCountry").val() === "USA" && $("#<%=IDPrefix%>" + coAppPrefix + "txtZip").length > 0 && Common.ValidateZipCode($("#<%=IDPrefix%>" + coAppPrefix + "txtZip").val()) == true) {
				zip = $("#<%=IDPrefix%>" + coAppPrefix + "txtZip").val().replace(/-/g, "").substring(0, 5);
				}
			return "street=" + $("#<%=IDPrefix%>" + coAppPrefix + "txtAddress").val() + ($("#<%=IDPrefix%>" + coAppPrefix + "ddlCountry").val() === "USA" ? "" : " " + $("#<%=IDPrefix%>" + coAppPrefix + "txtAddress2").val()) + "&city=" + $("#<%=IDPrefix%>" + coAppPrefix + "txtCity").val() + "&state=" + $("#<%=IDPrefix%>" + coAppPrefix + "ddlState").val() + "&zip=" + zip;
			
		}
		function <%=IDPrefix%>updateAddress(responseText, coAppPrefix) {
			var $txtAddress = $("#<%=IDPrefix%>" + coAppPrefix + "txtAddress");
			var $txtCity = $("#<%=IDPrefix%>" + coAppPrefix + "txtCity");
			var $txtZip = $("#<%=IDPrefix%>" + coAppPrefix + "txtZip");
			var $ddlState = $("#<%=IDPrefix%>" + coAppPrefix + "ddlState");
			$txtAddress.val(/street="(.*?)"/g.exec(responseText)[1]);
			$.lpqValidate.hideValidation($txtAddress);
			$txtCity.val(/city="(.*?)"/g.exec(responseText)[1]);
			$.lpqValidate.hideValidation($txtCity);
			$ddlState.val(/state="(.*?)"/g.exec(responseText)[1]).selectmenu().selectmenu('refresh');
			$.lpqValidate.hideValidation($ddlState);
			$txtZip.val(/zip="(.*?)["]/g.exec(responseText)[1]);  //zip code may be 5 or 10 digit
			$.lpqValidate.hideValidation($txtZip);
		}
		ASI.FACTORY.<%=IDPrefix%>CopyPrimaryAddress = function(e, coAppPrefix) {
			coAppPrefix = coAppPrefix || "";
			ASI.FACTORY.<%=IDPrefix%>showHideState();
			$("#<%=IDPrefix%>" + coAppPrefix + "ddlState").val($("#" + coAppPrefix + "ddlState option:selected").val());
			$("#<%=IDPrefix%>" + coAppPrefix + "ddlState").selectmenu().selectmenu('refresh');
			if ($.trim($("#<%=IDPrefix%>" + coAppPrefix + "ddlState").val()) !== "") {
				$.lpqValidate.hideValidation($("#<%=IDPrefix%>" + coAppPrefix + "ddlState"));
    		}
			<%--if (is_foreign_address) {
				$("#<%=IDPrefix%>" + coAppPrefix + "ddlCountry").val($("#" + coAppPrefix + "ddlCountry option:selected").val());
				$("#<%=IDPrefix%>" + coAppPrefix + "ddlCountry").selectmenu().selectmenu('refresh');
				if ($.trim($("#<%=IDPrefix%>" + coAppPrefix + "ddlCountry").val()) !== "") {
					$.lpqValidate.hideValidation($("#<%=IDPrefix%>" + coAppPrefix + "ddlCountry"));
        		}
				$("#<%=IDPrefix%>" + coAppPrefix + "txtAddress2").val($("#" + coAppPrefix + "txtAddress2").val());
    		}--%>
			$("#<%=IDPrefix%>" + coAppPrefix + "txtAddress").val($("#" + coAppPrefix + "txtAddress").val());
			if ($.trim($("#<%=IDPrefix%>" + coAppPrefix + "txtAddress").val()) !== "") {
				$.lpqValidate.hideValidation($("#<%=IDPrefix%>txtAddress"));
			}
			$("#<%=IDPrefix%>" + coAppPrefix + "txtZip").val($("#" + coAppPrefix + "txtZip").val());
			if ($.trim($("#<%=IDPrefix%>" + coAppPrefix + "txtZip").val()) !== "") {
				$.lpqValidate.hideValidation($("#<%=IDPrefix%>" + coAppPrefix + "txtZip"));
    		}
			$("#<%=IDPrefix%>" + coAppPrefix + "txtCity").val($("#" + coAppPrefix + "txtCity").val());
			if ($.trim($("#<%=IDPrefix%>" + coAppPrefix + "txtCity").val()) !== "") {
				$.lpqValidate.hideValidation($("#<%=IDPrefix%>" + coAppPrefix + "txtCity"));
			}
			$("#<%=IDPrefix%>" + coAppPrefix + "txtCity").trigger("change");
			e.preventDefault();
		}
	}(window.ASI = window.ASI || {}, jQuery));
	$(function() {
		ASI.FACTORY.init();
		ASI.FACTORY.init("co_");
	});
</script>