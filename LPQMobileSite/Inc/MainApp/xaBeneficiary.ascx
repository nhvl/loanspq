﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="xaBeneficiary.ascx.vb" Inherits="Inc_MainApp_xaBeneficiary" %>
<%@ Import Namespace="LPQMobile.Utils" %>
<style>
    #<%=IDPrefix%>divSetPercentShare {
		/*max-width: 60%;*/
		position: relative;
		padding: 20px 0 30px;
	}
	#<%=IDPrefix%>divSetPercentShare>label {
		padding: 20px 0 10px;
		display: inline;
	}
	#<%=IDPrefix%>divSetPercentShare .calculator-btn{
		border-radius: 4px;
		display: inline;
		position: absolute;
		margin-left: 30px;
		top: 11px;
		padding: 6px 10px;
		/*padding: 10px 10px 8px;
		margin-left: 20px;*/
	}
	
	#<%=IDPrefix%>divSetPercentShare .calculator-btn em.fa {
		font-size: 1.2em;
		line-height: 1.2em;
	}
	#<%=IDPrefix%>divSetPercentShare .calculator-btn em.fa:after{
		content: '\f0d7';
		font-family: FontAwesome;
		margin-left: 10px;
	}

	#<%=IDPrefix%>popupDistributeEvenly .beneficiary-total-row {
		margin-top: 8px;
	}
	#<%=IDPrefix%>popupDistributeEvenly .beneficiary-row > div:first-child {
		width: calc(100% - 140px);
		
	}
	#<%=IDPrefix%>popupDistributeEvenly .beneficiary-row > div:first-child label{
		margin: 0;
		line-height: 16px;
		white-space: nowrap;
		text-overflow: ellipsis;
		overflow: hidden;
	}
	#<%=IDPrefix%>popupDistributeEvenly .beneficiary-row >div, #<%=IDPrefix%>popupDistributeEvenly .beneficiary-total-row >div {
		display: inline-block;
	}
	#<%=IDPrefix%>popupDistributeEvenly .beneficiary-amount input {
		text-align: right;
	}
	#<%=IDPrefix%>popupDistributeEvenly .beneficiary-row > div + div, #<%=IDPrefix%>popupDistributeEvenly .beneficiary-total-row > div + div {
		width: 120px;
		margin-left: 10px;
	}
	@media(max-width: 768px) {
		#<%=IDPrefix%>divSetPercentShare .calculator-btn{
			right: 0;
			margin-left: 0;
		}
	}
	/*@media(max-width: 440px) {
		#divSetPercentShare {
			padding: 20px 0;
		}
		#divSetPercentShare > label {
			padding: 10px 0;
		}
		#divSetPercentShare .calculator-btn{
			display: inline-block;
			position: inherit;
			margin: 4px 0;

		}
	}*/
	@media(max-width: 440px) {
		#<%=IDPrefix%>divSetPercentShare .calculator-btn{
			display: block;
			width: 62px;
			position: relative;
		}

		#<%=IDPrefix%>popupDistributeEvenly .beneficiary-row >div {
			margin: 0;
			padding: 0;
			/*text-align: left;
			display: block;
			text-align: left;*/
		}
		#<%=IDPrefix%>popupDistributeEvenly .beneficiary-row > div + div{
			/*text-align: right;
			display: block;
			width: 100%;
			margin: 0;*/
			padding: 0;
		}

		#<%=IDPrefix%>popupDistributeEvenly .beneficiary-total-row .ui-field-contain {
			padding: 0;
			margin: 0;
		}
	}
	#<%=IDPrefix%>popupDistributeEvenly, #<%=IDPrefix%>popupDistributeEvenlyNote {
		max-width: 450px;
	}
	#<%=IDPrefix%>popupDistributeEvenly label.beneficiary-total-amount.invalid {
		color: red;
	}
	/*#popupDistributeEvenly label.beneficiary-name, #popupDistributeEvenly label.beneficiary-total, #popupDistributeEvenly label.beneficiary-total-amount{
		margin-top: 10px;
	}*/
	#<%=IDPrefix%>showBeneficiary:before {
		content: "\f055";
		font-family: FontAwesome;
		padding-right: 4px;
	}
	#<%=IDPrefix%>showBeneficiary[status="Y"]:before {
		content:"\f056";
		font-family: FontAwesome;
		padding-right: 4px;
	}
</style>

<div id="<%=IDPrefix%>divBeneficiary" <%=IIf(IsInMode("777") AndAlso IsInFeature("visibility"), "class=""showfield-section"" data-show-field-section-id=""" & BuildShowFieldSectionID("divBeneficiary") & """ data-section-name= '" & "Beneficiary Information'", "")%>>

	<div id="<%=IDPrefix%>divBeneficiaryLink" style="margin-top: 10px;" class="hide-able" >
	<a id="<%=IDPrefix%>showBeneficiary" href="#" data-mode="self-handle-event" status="N" style="cursor: pointer;font-weight :bold;" class="header_theme2 shadow-btn"><%--<i class="fa fa-plus-circle"></i>--%>Add a beneficiary</a>
	</div>
                   
	<div id="<%=IDPrefix%>divBeneficiaryPanel" style="display:none;" >
		<div id="<%=IDPrefix%>beneficiaryContainer"></div>
		<div id="<%=IDPrefix%>divSetPercentShare">
			<label id="<%=IDPrefix%>spTotalSharedTitle">Set beneficiary percent share: </label><label id="<%=IDPrefix%>spTotalShared">0%</label>
			<div class="calculator-btn btn-header-theme" onclick='<%=IDPrefix%>openDistributeEvenlyPopup()' data-rel='popup' data-transition='pop'><em class="fa fa-calculator" aria-hidden="true"></em></div>
		</div>
		<div style ="clear:both"></div>
		<div id="<%=IDPrefix%>divBtn">
			<div class ="addDivBtn" ><a href ="#"  data-role="button" class="ui-btn btn-header-theme ui-shadow ui-corner-all" >Add More</a></div>
			<div class="removeDivBtn"><a href="#"  data-role="button" class="ui-btn btn-header-theme ui-shadow ui-corner-all">Remove</a></div>
		</div>
		<div style ="clear:both"></div>
	</div>
</div>
<div id="<%=IDPrefix%>popupDistributeEvenly" data-role="popup" data-position-to="window" data-dismissible="false">
	<div data-role="content">
		<div class="row">
			<div class="col-sm-12 header_theme2">
				<a href="#" data-rel="back" class="pull-right svg-btn"><%=HeaderUtils.IconClose %><span style="display: none;">close</span></a>
			</div>
		</div>
		<div class="row">
			<div class="col-sm-12">
				<br/><div data-role="fieldcontain"><p>Please indicate the percentage of the payout each beneficiary will receive:</p></div><br/>
			</div>    
		</div>
		<div id="<%=IDPrefix%>divBeneficiaryPSContainer">
		</div>
		<%--<div class="beneficiary-row row text-right">
			<div data-role="fieldcontain" class="text-right"><label class="beneficiary-name">Loan Amount</label></div>
			<div data-role="fieldcontain" class="beneficiary-amount"><input type="text" pattern="[0-9\.,]*" class="percent" maxlength ="6"/></div>   
		</div>--%>
		<div class="beneficiary-total-row row text-right">
			<div data-role="fieldcontain" class="text-right"><label class="beneficiary-total">Total</label></div>
			<div data-role="fieldcontain"><label class="beneficiary-total-amount">0%</label></div>
		</div>
		<div class="row distribute-btn-wrapper">
			<a href="#" onclick="<%=IDPrefix%>distributeEvenly()"  data-role="button" class="ui-btn distribute-btn btn-header-theme ui-shadow ui-corner-all">Distribute Evenly</a>
			<a  href="#" onclick='<%=IDPrefix%>openDistributeEvenlyNotePopup()' style="margin-left: 8px; font-size: 1.2em;" data-rel='popup' data-transition='pop'><em class="fa fa-question-circle header_theme2"></em></a>
		</div>
	</div>
</div>
<div id="<%=IDPrefix%>popupDistributeEvenlyNote" data-role="popup" data-position-to="window" data-dismissible="false">
	<div data-role="content">
		<div class="row">
			<div class="col-sm-12 header_theme2">
				<a href="#" data-rel="back" class="pull-right svg-btn"><%=HeaderUtils.IconClose %><span style="display: none;">close</span></a>
			</div>
		</div>
		<div class="row">
			<div class="col-sm-12">
				<br/>
				<div data-role="fieldcontain">
					<p>Press this button if you want to assign an equal value to all beneficiaries.</p>
					<p>In some cases, 100 cannot be divided evenly by the number of beneficiaries When this occurs, the amount of the first beneficiary will be increased to make the total equal 100%.</p>
				</div>   
			</div>    
		</div>
	</div>
</div>
<div class="hidden">
	<div id="<%=IDPrefix%>divIconQuestion16"><%=HeaderUtils.IconQuestion16%></div>
	<div id="<%=IDPrefix%>divIconLock16"><%=HeaderUtils.IconLock16%></div>
</div>
<script>
	function <%=IDPrefix%>openDistributeEvenlyNotePopup() {
		$.mobile.switchPopup($("#<%=IDPrefix%>popupDistributeEvenly"), $("#<%=IDPrefix%>popupDistributeEvenlyNote"), function () { });
	}
	function <%=IDPrefix%>openDistributeEvenlyPopup() {
		var $container = $("#<%=IDPrefix%>divBeneficiaryPSContainer");
		$container.html("");
		$('#<%=IDPrefix%>beneficiaryContainer .Beneficiary').each(function (idx, ele) {
			var obj = {};
			obj.idx = idx + 1;
			obj.name = $('#<%=IDPrefix%>first_name' + obj.idx).val() + " " + $('#<%=IDPrefix%>last_name' + obj.idx).val();
			obj.value = Common.FormatPercent($('#<%=IDPrefix%>percent_share' + obj.idx).val(), true, 2);
			$container.append(<%=IDPrefix%>buildPercentShareField(obj));
		});
		$("input.percent", $container).on("blur", function () {
			$(this).val(Common.FormatPercent($(this).val(), true, 2));
			$("#<%=IDPrefix%>popupDistributeEvenly label.beneficiary-total-amount").trigger("change");
		});
		$("#<%=IDPrefix%>popupDistributeEvenly label.beneficiary-total-amount").trigger("change");
		$container.trigger("create");
		$("#<%=IDPrefix%>popupDistributeEvenly").popup("open");
	}
	function <%=IDPrefix%>distributeEvenly() {
		var cnt = $("input.percent", "#<%=IDPrefix%>divBeneficiaryPSContainer").length;
		var disVal = Common.FormatPercent(100 / cnt, true, 2);
		if ((100 - Common.GetPercentFromPercentString(disVal) * (cnt - 1)) < Common.GetPercentFromPercentString(disVal)) {
			disVal = Common.FormatPercent(Math.floor(100 / cnt * 100) / 100, true, 2);
		}
		$("input.percent", "#<%=IDPrefix%>divBeneficiaryPSContainer").each(function (idx, ele) {
			if (idx == 0) {
				$(ele).val(Common.FormatPercent(100 - Common.GetPercentFromPercentString(disVal)*(cnt-1), true, 2));
			} else {
				$(ele).val(disVal);
			}
		});
		$("#<%=IDPrefix%>popupDistributeEvenly label.beneficiary-total-amount").text("100.00%").removeClass("invalid");
	}
	function <%=IDPrefix%>Obj(id, type, label, isRequired, className, maxLength, index) {
        this.id = id;
        this.type = type;
        this.label= label;
        this.isRequired = isRequired;
        this.className = className;
        this.maxLength = maxLength;
	    this.index = index;
    }

	function <%=IDPrefix%>beneficiaryInfo(index) {
    	var beneficiary = [];
		<%If EnableBeneficiaryTrust then %>
		beneficiary.push(new <%=IDPrefix%>Obj("chkTrust" + index, "checkbox", "Is Beneficiary a Trust?", "false", "", "", index));
		<%End If%>
		beneficiary.push(new <%=IDPrefix%>Obj("first_name" + index, "text", "First Name", true, "", "50", index));
		beneficiary.push(new <%=IDPrefix%>Obj("last_name" + index, "text", "Last Name", true, "", "50", index));
		beneficiary.push(new <%=IDPrefix%>Obj("dob" + index, "text", "Date of Birth", true, "", "10", index));
		<%If EnableBeneficiarySSN then %>
		beneficiary.push(new <%=IDPrefix%>Obj("ssn" + index, "text", "SSN", true, "", "9", index));
        <%End If%>
		
		<%If EnableBeneficiaryAddress then %>
		beneficiary.push(new <%=IDPrefix%>Obj("address" + index, "text", "Address", false, "", "", index));
		<%End If%>
		beneficiary.push(new <%=IDPrefix%>Obj("phone" + index, "text", "Phone", false, "", "", index));
		beneficiary.push(new <%=IDPrefix%>Obj("relationship" + index, "dropdown", "Relationship", false, "", "", index));
		beneficiary.push(new <%=IDPrefix%>Obj("percent_share" + index, "tel", "Percent Share(%)", false, "numeric", "5", index));    

        return beneficiary;
    }
  
	function <%=IDPrefix%>renderingBeneficiaries(index) {
		var beneficiaryArr = <%=IDPrefix%>beneficiaryInfo(index);
        var beneficiaryHtml = "<div class='Beneficiary' id='<%=IDPrefix%>beneficiary" + index + "'>";
        beneficiaryHtml += "<div class='beneficiaryLabel'>Beneficiary #" + index + "</div>";
        for (var i = 0; i < beneficiaryArr.length; i++) {
        	beneficiaryHtml += <%=IDPrefix%>htmlBeneficiaryField(beneficiaryArr[i]);
        }
        beneficiaryHtml += "</div>";
        return beneficiaryHtml;
    }
	function <%=IDPrefix%>buildPercentShareField(obj) {
	    if ($.trim(obj.name) == "") obj.name = "<Beneficiary #" + obj.idx + ">";
		var $item = $("<div/>", { "class": "beneficiary-row row text-right" });
		var $label = $("<label/>", { "class": "beneficiary-name" }).text(obj.name);
		$("<div/>", { "data-role": "fieldcontain", "class": "text-right" }).append($label).appendTo($item);
		var $input = $("<input/>", { "class": "percent numeric", "maxlength": 6, "type": "text", "data-index": obj.idx}).val(obj.value);
		$("<div/>", { "data-role": "fieldcontain", "class": "beneficiary-amount" }).append($input).appendTo($item);
		return $item;
	}
	function <%=IDPrefix%>htmlBeneficiaryField(obj) {
		var strHtml = "";
        if (obj.id.indexOf("dob") > -1) {
        	var txtDOB1 = "txt" + obj.id + "_1";
        	var txtDOB2 = "txt" + obj.id + "_2";
        	var txtDOB3 = "txt" + obj.id + "_3";

        	strHtml += "<div data-role='fieldcontain' id='<%=IDPrefix%>div" + obj.id + "Stuff'>";
        	strHtml += '<div class="row">';
        	strHtml += '<div class="col-xs-12 no-padding ui-label-dob"><label for="<%=IDPrefix%>' + txtDOB1 + '" id="<%=IDPrefix%>lbldob' + obj.index + '" class="dob-label ' + (obj.isRequired ? " RequiredIcon" : "") + '">Date of Birth</label></div>';
        	strHtml += '<div class="col-xs-12 no-padding hidden ui-label-date-established"><label for="<%=IDPrefix%>' + txtDOB1 + '" id="<%=IDPrefix%>lblde' + obj.index + '" class="dob-label ' + (obj.isRequired ? " RequiredIcon" : "") + '">Date Established</label><input type="hidden" id="<%=IDPrefix%>txt' + obj.id + '" class="combine-field-value"/></div>';
        	strHtml += '</div>';

	        strHtml += "<div id='<%=IDPrefix%>div" + obj.id + "' class='ui-input-date row'>";
        	
        	strHtml += "<div class='col-xs-4'><input aria-labelledby='<%=IDPrefix%>lbl" + obj.id + "' type='" + '<%=TextAndroidTel%>' + "' pattern='[0-9]*' placeholder='mm' id='<%=IDPrefix%>" + txtDOB1 + "' maxlength='2' onkeydown='limitToNumeric(event);' onkeyup='<%=IDPrefix%>onKeyUp_DOB(event,this)'/></div>";
        	strHtml += "<div class='col-xs-4'><input aria-labelledby='<%=IDPrefix%>lbl" + obj.id + "' type='" + '<%=TextAndroidTel%>' + "' pattern='[0-9]*' placeholder='dd' id='<%=IDPrefix%>" + txtDOB2 + "' maxlength='2' onkeydown='limitToNumeric(event);' onkeyup='<%=IDPrefix%>onKeyUp_DOB(event,this)'/></div>";
        	strHtml += "<div class='col-xs-4'><input aria-labelledby='<%=IDPrefix%>lbl" + obj.id + "' type='" + '<%=TextAndroidTel%>' + "' pattern='[0-9]*' placeholder='yyyy' id='<%=IDPrefix%>" + txtDOB3 + "' maxlength='4' onkeydown='limitToNumeric(event);' /></div></div></div>";
        } else if (obj.id.indexOf("ssn") > -1) {
        	var txtSSN1 = "txt" + obj.id + "_1";
        	var txtSSN2 = "txt" + obj.id + "_2";
        	var txtSSN3 = "txt" + obj.id + "_3";
        	var showLabel = 'Show ' + obj.label;
        	var hideLabel = 'Hide ' + obj.label;
			<%--for backward compability, still keep using divBeneficiarySSN1 instead of divBeneficiarySSN --%>
        	strHtml += "<div id='<%=IDPrefix%>divBeneficiarySSN" + obj.index + "' <%=IIf(IsInMode("777") AndAlso IsInFeature("visibility"), "class='showfield-section ssn-block' data-show-field-section-id='" & BuildShowFieldSectionID("divBeneficiarySSN1") & "' data-section-name='Beneficiary SSN' data-default-state='off'", "")%>>";
        	strHtml += "<div data-role='fieldcontain' id=<%=IDPrefix%>'div" + obj.id + "Stuff'>";

        	strHtml += '<div class="row">';
        	strHtml += '<div class="col-xs-6 no-padding"><label for="<%=IDPrefix%>' + txtSSN1 + '" id="<%=IDPrefix%>lbl' + obj.id + '" class="' + (obj.isRequired ? ' RequiredIcon"' : "") + '>' + obj.label + '</label></div>';
			strHtml += '<div class="col-xs-6 no-padding ui-label-ssn">';
			strHtml += '<label class="pull-right header_theme2 ssn-ico lock-ico" style="padding-left: 4px; padding-right: 2px; display: inline;"></label>';
			strHtml += '<label id="<%=IDPrefix%>spOpenSsnSastisfy" onclick="openPopup(\'#<%=IDPrefix%>popSSNSastisfy\')" class="pull-right focus-able header_theme2 ssn-ico question-ico abort-renameable" tabindex="0" style="display: inline;"></label>';
			strHtml += '<div style="display: inline;" class="hidden"><label is_show="1" tabindex="0" class="pull-right header_theme2 rename-able shadow-btn focus-able" style="margin-right: 10px; cursor: pointer;" onclick="<%=IDPrefix%>toggleBeneficiarySSNText(this,\'' + obj.id + '\');" onkeypress="<%=IDPrefix%>toggleBeneficiarySSNText(this, \'' + obj.id + '\');">' + hideLabel + '</label></div>';
        	strHtml += '<div style="display: inline;"><label is_show="0" tabindex="0" class="pull-right header_theme2 rename-able shadow-btn focus-able" style="margin-right: 10px; cursor: pointer;" onclick="<%=IDPrefix%>toggleBeneficiarySSNText(this,\'' + obj.id + '\');" onkeypress="<%=IDPrefix%>toggleBeneficiarySSNText(this, \'' + obj.id + '\');">' + showLabel + '</label></div>';
        	strHtml += '<div class="clearfix"></div>';
        	strHtml += '</div>';
        	strHtml += '</div>';
			
        	strHtml += "<div id='<%=IDPrefix%>div" + obj.id + "' class='ui-input-ssn row'>";
        	strHtml += "<div class='col-xs-4'><input type='password' aria-labelledby='<%=IDPrefix%>lbl" + obj.id + "' pattern='[0-9]*' placeholder='---' id='<%=IDPrefix%>" + txtSSN1 + "'  maxlength='3' next-input='<%=IDPrefix%>" + txtSSN2 + "' onkeydown='limitToNumeric(event);' onkeyup='onKeyUp(event,\"#<%=IDPrefix%>" + txtSSN1 + "\",\"#<%=IDPrefix%>" + txtSSN2 + "\", \"3\");' value=''/><input type='hidden' id='<%=IDPrefix%>hd" + obj.id + "1' value=''/></div>";
        	strHtml += "<div class='col-xs-4'><input type='password' aria-labelledby='lbl" + obj.id + "' pattern='[0-9]*' placeholder='--' id='<%=IDPrefix%>" + txtSSN2 + "' maxlength ='2' next-input='<%=IDPrefix%>" + txtSSN3 + "' onkeydown='limitToNumeric(event);' onkeyup='onKeyUp(event,\"#<%=IDPrefix%>" + txtSSN2 + "\",\"#<%=IDPrefix%>" + txtSSN3 + "\", \"2\");'  value='' /><input type='hidden' id='<%=IDPrefix%>hd" + obj.id + "2' value=''/></div>";
        	strHtml += "<div class='col-xs-4' style='padding-right: 0px;'><input aria-labelledby='<%=IDPrefix%>lbl" + obj.id + "' type='<%=TextAndroidTel%>' pattern='[0-9]*' placeholder='----' id='<%=IDPrefix%>" + txtSSN3 + "' maxlength ='4' onkeydown='limitToNumeric(event);' value=''/><input type='hidden' id='<%=IDPrefix%>txt" + obj.id + "' value='' class='combine-field-value'/></div></div></div>";
        	strHtml += "</div>";
        } else if (obj.id.indexOf("chkTrust") > -1) {
			<%--for backward compability, still keep using divBeneficiaryTrust1 instead of divBeneficiaryTrust --%>
        	strHtml += "<div id='<%=IDPrefix%>divBeneficiaryTrust" + obj.index + "' <%=IIf(IsInMode("777") AndAlso IsInFeature("visibility"), "class='showfield-section trust-block' data-show-field-section-id='" & BuildShowFieldSectionID("divBeneficiaryTrust1") & "' data-section-name= '" & "Beneficiary Trust'", "")%>>";
        	strHtml += "<div data-role='fieldcontain'>";
        	strHtml += "<label class='rename-able'>" + obj.label + "</label>";
        	strHtml += "<div class='row'><div class='beneficiaryCheckbox'>";
        	strHtml += "<label for='<%=IDPrefix%>" + obj.id + "_yes' class='ui-btn ui-corner-all ui-btn-inherit ui-btn-icon-left ui-checkbox-off chkLabel abort-renameable'>Yes</label>";
        	strHtml += "<input type='" + obj.type + "' id='<%=IDPrefix%>" + obj.id + "_yes' /></div>";
        	strHtml += "<div class='beneficiaryCheckbox'><label for='<%=IDPrefix%>" + obj.id + "_no' class='ui-btn ui-corner-all ui-btn-inherit ui-btn-icon-left ui-checkbox-off chkLabel abort-renameable'>No</label>";
        	strHtml += "<input type='" + obj.type + "' id='<%=IDPrefix%>" + obj.id + "_no' checked='checked'/></div></div>";
        	strHtml += "</div>";
        	strHtml += "</div>";
        } else if (obj.id.indexOf("phone") > -1) {
        	strHtml += "<div data-role='fieldcontain'>";
        	strHtml += "<div>";
        	strHtml += "<div style='display: inline-block;'><label id='<%=IDPrefix%>lblContactHomePhone' for='<%=IDPrefix%>" + obj.id + "' class='' style='white-space: nowrap;'>" + obj.label + "</label></div>";
        	strHtml += "<div style='display: inline-block;'> <label class='phone_format rename-able' style='white-space: nowrap; font-style: italic;'>(xxx) xxx-xxxx</label></div>";
        	strHtml += "</div>";
            strHtml += "<input type='<%=TextAndroidTel%>' id='<%=IDPrefix%>" + obj.id + "'";
	        strHtml += "maxlength='14' pattern='[0-9]*' class='inphone' autocomplete='tel'";
            strHtml +="/></div>";
        } else if (obj.id.indexOf("first_name") > -1) {
            strHtml += "<div data-role='fieldcontain'>";

        	strHtml += "<div>";
        	strHtml += "<label for='<%=IDPrefix%>" + obj.id + "' id='<%=IDPrefix%>lblFirstName" + obj.index + "' class='" + (obj.isRequired ? " RequiredIcon'" : "") + ">First Name</label>";
        	strHtml += "</div>";
        	strHtml += "<div class='hidden'>";
        	strHtml += "<label for='<%=IDPrefix%>" + obj.id + "' id='<%=IDPrefix%>lblTrustName" + obj.index + "' class='" + (obj.isRequired ? " RequiredIcon'" : "") + ">Trust Name</label>";
            strHtml += "</div>";

        	strHtml += "<input type='" + obj.type + "' id='<%=IDPrefix%>" + obj.id + "'";
        	if (obj.className != "") {
        		strHtml += " class='" + obj.className + "'";
        	}
        	if (obj.maxLength != "") {
        		strHtml += "maxlength='" + obj.maxLength + "'";
        	}
        	strHtml += "/>";
            strHtml += "</div>";
        } else if (obj.id.indexOf("last_name") > -1) {
            strHtml += "<div data-role='fieldcontain'>";
            strHtml += "<div>";
            strHtml += "<label for='<%=IDPrefix%>" + obj.id + "' id='<%=IDPrefix%>lblLastName" + obj.index + "' class='" + (obj.isRequired ? " RequiredIcon'" : "") + ">Last Name</label>";
            strHtml += "</div>";
           
            strHtml += "<input type='" + obj.type + "' id='<%=IDPrefix%>" + obj.id + "'";
            if (obj.className != "") {
                strHtml += " class='" + obj.className + "'";
            }
            if (obj.maxLength != "") {
                strHtml += "maxlength='" + obj.maxLength + "'";
            }
            strHtml += "/>";
            strHtml += "</div>";
        } else if (obj.id.indexOf("address") > -1) {
        	strHtml += "<div id='<%=IDPrefix%>divBeneficiaryAddress" + obj.index + "' <%=IIf(IsInMode("777") AndAlso IsInFeature("visibility"), "class='showfield-section' data-show-field-section-id='" & BuildShowFieldSectionID("divBeneficiaryAddress") & "' data-default-state='off' data-section-name= '" & "Beneficiary Address'", "")%>>";
        	strHtml += "<div data-role='fieldcontain'>";
        	strHtml += "<label for='<%=IDPrefix%>address" + obj.index + "' class='" + (obj.isRequired ? " RequiredIcon'" : "'") + ">Address</label>";
        	strHtml += "<input type='text' id='<%=IDPrefix%>address" + obj.index + "'  maxlength='100' name='address' autocomplete='street-address' onchange='<%=IDPrefix%>verifyBeneficiaryAddress(" + obj.index + ")'/>";
			strHtml += "</div>";

			strHtml += "<div data-role='fieldcontain'>";
			strHtml += "<label for='<%=IDPrefix%>zip" + obj.index + "' class='" + (obj.isRequired ? " RequiredIcon'" : "'") + ">Zip</label>";
        	strHtml += "<input type='text' id='<%=IDPrefix%>zip" + obj.index + "' maxlength='5' maxlength='100' class='numeric' name='zip' autocomplete='postal-code' onchange='<%=IDPrefix%>verifyBeneficiaryAddress(" + obj.index + ")'/>";
        	strHtml += "</div>";

        	strHtml += "<div data-role='fieldcontain'>";
        	strHtml += "<label for='<%=IDPrefix%>city" + obj.index + "' class='" + (obj.isRequired ? " RequiredIcon'" : "'") + ">City</label>";
        	strHtml += "<input type='text' id='<%=IDPrefix%>city" + obj.index + "'  maxlength='50' name='city' autocomplete='address-level2' onchange='<%=IDPrefix%>verifyBeneficiaryAddress(" + obj.index + ")'/>";
        	strHtml += "</div>";

        	strHtml += "<div data-role='fieldcontain'>";
        	strHtml += "<label for='<%=IDPrefix%>state" + obj.index + "' class='" + (obj.isRequired ? " RequiredIcon'" : "'") + ">State</label>";
        	strHtml += "<select id='<%=IDPrefix%>state" + obj.index + "' name='state' autocomplete='address-level1' onchange='<%=IDPrefix%>verifyBeneficiaryAddress(" + obj.index + ")'>";
        	strHtml += "<%=Common.RenderStateDropdownlistWithEmpty(CEnum.STATES, "", "--Please Select--", "")%>";
        	strHtml += "</select>";
        	strHtml += "</div>";
	        strHtml += "<div><span id='<%=IDPrefix%>spVerifyBeneficiaryAddressMessage" + obj.index + "' class='require-span' style='display: none;'>Unable to validate address</span> </div>";
        	strHtml += "</div>";
        } else {
            strHtml += "<div data-role='fieldcontain'>";
            strHtml += "<label for='<%=IDPrefix%>" + obj.id + "' class='" + (obj.isRequired ? "RequiredIcon'" : "'") + ">" + obj.label + "</label>";
            if (obj.type == "text" || obj.type == "tel") {
            	strHtml += "<input type='" + obj.type + "' id='<%=IDPrefix%>" + obj.id + "'";
                if (obj.className != "") {
                	strHtml += " class='" + obj.className + "'";
                }
                if (obj.maxLength != "") {
                	strHtml += "maxlength='" + obj.maxLength + "'";
                }
                if (obj.id.indexOf("percent_share") > -1) {
                	strHtml += "value='0'";
                }
                strHtml += "/>";
            } else if (obj.type == "dropdown") {
            	strHtml += "<select id='<%=IDPrefix%>" + obj.id + "'>";
            	strHtml += "<option value=''></option>";
            	strHtml += "<option value='Child'>Child</option>";
            	strHtml += "<option value='Friend'>Friend</option>";
            	strHtml += "<option value='Grandchild'>Grandchild</option>";
            	strHtml += "<option value='Other'>Other</option>";
            	strHtml += "<option value='Parent'>Parent</option>";
            	strHtml += "<option value='Sibling'>Sibling</option>";
            	strHtml += "<option value='Spouse'>Spouse</option>";
                strHtml += "</select>";
            } else if (obj.type == "checkbox") {
	            strHtml += "<div class='row'><div class='beneficiaryCheckbox'>";
	            strHtml += "<label for='<%=IDPrefix%>" + obj.id + "_yes' class='ui-btn ui-corner-all ui-btn-inherit ui-btn-icon-left ui-checkbox-off chkLabel'>Yes</label>";
            	strHtml += "<input type='" + obj.type + "' id='<%=IDPrefix%>" + obj.id + "_yes' /></div>";
            	strHtml += "<div class='beneficiaryCheckbox'><label for='<%=IDPrefix%>" + obj.id + "_no' class='ui-btn ui-corner-all ui-btn-inherit ui-btn-icon-left ui-checkbox-off chkLabel'>No</label>";
            	strHtml += "<input type='" + obj.type + "' id='<%=IDPrefix%>" + obj.id + "_no' checked='checked'/></div></div>";
            }
            strHtml += "</div>";
        }
        return strHtml;
    }

	function <%=IDPrefix%>showBeneficiary(obj) {
		if ($(obj).attr("contenteditable") == "true") return false;
        var selectedValue = $(obj).attr('status');
		if (selectedValue == "N") {
			$(obj).html(translateRename('Remove beneficiary'));
			$(obj).attr('status', 'Y');
			$('#<%=IDPrefix%>divBeneficiaryPanel').show();
		} else {
			$(obj).html(translateRename('Add a beneficiary'));
			$(obj).attr('status', 'N');
			$('#<%=IDPrefix%>divBeneficiaryPanel').hide();
		}

	}
	function <%=IDPrefix%>toggleBeneficiarySSNText(btn, id) {
		var $self = $(btn);
		if ($self.attr("contenteditable") == "true") return false;
	    var $container = $self.closest("div.Beneficiary");
	    if ($self.attr('is_show') == '0') {
	    	$self.parent().addClass("hidden");
	    	$self.closest("div.ui-label-ssn").find("label[is_show='1']").parent().removeClass("hidden");
    		if (isMobile.any() && !isMobile.Windows()) { //moz-text-security does not works on window devices
    			var ssn1 = "";
    			var ssn2 = "";
    			var matchs = /^(\w*)-(\w*)-(\w*)$/i.exec($("#txt" + id, $container).val());
				if (typeof matchs != "undefined" && matchs != null) {
					ssn1 = matchs[1];
					ssn2 = matchs[2];
				}
				$("#<%=IDPrefix%>txt" + id + "_1", $container).removeClass("mask-password").val("").val(ssn1); //keep .val("") to prevent bug on mobile devices
    			$("#<%=IDPrefix%>txt" + id + "_2", $container).removeClass("mask-password").val("").val(ssn2);
			} else {
    			$("#<%=IDPrefix%>txt" + id + "_1", $container).attr('type', 'text');
    			$("#<%=IDPrefix%>txt" + id + "_2", $container).attr('type', 'text');
			}
		} else {
	    	$self.parent().addClass("hidden");
	    	$self.closest("div.ui-label-ssn").find("label[is_show='0']").parent().removeClass("hidden");
			if (isMobile.any() && !isMobile.Windows()) { //moz-text-security does not works on window devices
				$("#<%=IDPrefix%>txt" + id + "_1", $container).addClass("mask-password");
				$("#<%=IDPrefix%>txt" + id + "_2", $container).addClass("mask-password");
			} else {
				$("#<%=IDPrefix%>txt" + id + "_1", $container).attr('type', 'password');
				$("#<%=IDPrefix%>txt" + id + "_2", $container).attr('type', 'password');
			}
		}
	}
	function <%=IDPrefix%>validateBeneficiary() {
		if ($('#<%=IDPrefix%>showBeneficiary').attr('status') == 'Y') {
			return $.lpqValidate("<%=IDPrefix%>ValidateBeneficiary");
	    }
	    return true;
	}

	function <%=IDPrefix%>registerBeneficiaryValidator(index) {
       
		$('#<%=IDPrefix%>first_name' + index).observer({
            validators: [
                function (partial) {
                	if ($(this).closest("div.showfield-section").length > 0 && $(this).closest("div.showfield-section").hasClass("hidden")) return "";
                    if (!Common.ValidateText($(this).val())) {
                    	if (<%=IDPrefix%>isTrust(index)) {
                            return "Trust Name is required";
                        } else {
                            return "First Name is required";
                        }
                    }
                    return '';
                }
            ],
            validateOnBlur: true,
            group: "<%=IDPrefix%>ValidateBeneficiary"
        });
  
		$('#<%=IDPrefix%>last_name' + index).observer({
                validators: [
                     function (partial) {
                     	if ($(this).closest("div.showfield-section").length > 0 && $(this).closest("div.showfield-section").hasClass("hidden")) return "";
                     	if (<%=IDPrefix%>isTrust(index)) {
                             return '';
                         }

                         if (!Common.ValidateText($(this).val())) {
                             return "Last Name is required";
                         }
                         return '';
                     }
                ],
                validateOnBlur: true,
                group: "<%=IDPrefix%>ValidateBeneficiary"
            });
     
		$('#<%=IDPrefix%>percent_share' + index).observer({
            validators: [
                 function (partial) {
                 	if ($(this).closest("div.showfield-section").length > 0 && $(this).closest("div.showfield-section").hasClass("hidden")) return "";
                     var element = $(this);
                     if (!Common.ValidateText(element.val())) {
                         //percent share is not a required field
                         //  return "Percent Share is required";
                     } else {
                         if (isNaN(element.val())) {
                             return "Percent Share is not valid";
                         } else {
                             if (Number(element.val()) < 0 || Number(element.val()) > 100) {
                                 return "Percent Share is not valid";
                             }
                         }
                     }
                     return '';
                 }
            ],
            validateOnBlur: true,
            group: "<%=IDPrefix%>ValidateBeneficiary"
        });
		$('#<%=IDPrefix%>divdob' + index).observer({
            validators: [
				function (partial) {
					if ($(this).closest("div.showfield-section").length > 0 && $(this).closest("div.showfield-section").hasClass("hidden")) return "";
					<%=IDPrefix%>updateHiddenBeneficiaryDOB("#<%=IDPrefix%>beneficiary" + index, index);
					var $DOB = $('#<%=IDPrefix%>txtdob' + index);
				    var fieldName = "Date of Birth";
				    if (<%=IDPrefix%>isTrust(index)) {
				    	fieldName = "Date Established";
				    }
                    var dob = moment($DOB.val(), "MM-DD-YYYY");                     
					if (!Common.ValidateText($DOB.val())) {
                        return fieldName + " is required";
                    } else if (!Common.IsValidDate($DOB.val())) {
						return "Valid " + fieldName + " is required";
					} else if (dob.year() < 1900) {
						return fieldName + " is too old";
					} else if (dob.isAfter(moment())) {
						//beneficiary birthday range - from the past to current date	       
						return fieldName + " must be equal or less than current date";
					}

				    return "";
				}
            ],
            validateOnBlur: true,
            group: "<%=IDPrefix%>ValidateBeneficiary"
        });
		$('#<%=IDPrefix%>divssn' + index).observer({
        	validators: [
				function (partial, evt) {
					if ($(this).closest("div.showfield-section").length > 0 && $(this).closest("div.showfield-section").hasClass("hidden")) return "";
					if ($(this).closest(".ssn-block").length > 0 && $(this).closest(".ssn-block").hasClass("hidden")) {
						return "";
					}
					<%=IDPrefix%>updateHiddenBeneficiarySSN("#<%=IDPrefix%>beneficiary" + index, index);
					<%If ForceSSNShown = false then %>
					if (<%=IDPrefix%>isTrust(index)) {
						return "";
					}
					<%End If%>
					var ssn = $("#<%=IDPrefix%>txtssn" + index + "_1", "#<%=IDPrefix%>beneficiary" + index).val() + $("#<%=IDPrefix%>txtssn" + index + "_2", "#<%=IDPrefix%>beneficiary" + index).val() + $("#<%=IDPrefix%>txtssn" + index + "_3", "#<%=IDPrefix%>beneficiary" + index).val();
					if (/^[0-9]{9}$/.test(ssn) == false) {
						return 'Valid SSN is required';
					}
					return "";
				}
			],
			validateOnBlur: true,
			group: "<%=IDPrefix%>ValidateBeneficiary"
        });
		$('#<%=IDPrefix%>phone' + index).observer({
		    validators: [
			    function (partial) {
			    	if ($(this).closest("div.showfield-section").length > 0 && $(this).closest("div.showfield-section").hasClass("hidden")) return "";
				    var $phone = $(this);
				    if (Common.ValidateText($phone.val())) {
					    if (!Common.ValidatePhone($phone.val())) {
						    return 'Phone number is invalid.';
					    }
					    //validate first digit of home phone : 0 and 1 is invalid
					    var firstDigit = $phone.val().replace("(", "").substring(0, 1);
					    if (firstDigit == 0 || firstDigit == 1) {
						    return 'Phone number and area code can not begin with 0 or 1 and must be a valid phone number';
					    }

				    }
				    return '';
			    }
		    ],
		    validateOnBlur: true,
		    group: "<%=IDPrefix%>ValidateBeneficiary"
	    });
		$("#<%=IDPrefix%>divSetPercentShare").observer({
	    	validators: [
                 function (partial) {
                 	if ($(this).closest("div.showfield-section").length > 0 && $(this).closest("div.showfield-section").hasClass("hidden")) return "";
                 	if (Common.GetPercentFromPercentString($("#<%=IDPrefix%>spTotalShared").text()) == 100) {
		                 return "";
                 	}
                 	return "Total percent share value must be 100%";
                 }
	    	],
	    	validateOnBlur: true,
	    	group: "<%=IDPrefix%>ValidateBeneficiary",
	    	groupType: "complexUI",
	    	placeHolder: "#<%=IDPrefix%>spTotalSharedTitle"
		});
		$('#<%=IDPrefix%>address' + index).observer({
			validators: [
                function (partial) {
                    if (!$("label[for='<%=IDPrefix%>address" + index+"']").hasClass('RequiredIcon')) return ""; //no validate if it is optional
                	if ($(this).closest("div.showfield-section").length > 0 && $(this).closest("div.showfield-section").hasClass("hidden")) return "";
                	if (!Common.ValidateText($(this).val())) {
                		return "Address is required";
                    }
                	return '';
                }
            ],
			validateOnBlur: true,
			group: "<%=IDPrefix%>ValidateBeneficiary"
		});
		$('#<%=IDPrefix%>zip' + index).observer({
			validators: [
                function (partial) {
                    if (!$("label[for='<%=IDPrefix%>zip" + index+"']").hasClass('RequiredIcon')) return ""; //no validate if it is optional
                	if ($(this).closest("div.showfield-section").length > 0 && $(this).closest("div.showfield-section").hasClass("hidden")) return "";
                	if(!Common.ValidateZipCode($(this).val())){
                	//if (!Common.ValidateText($(this).val()) || !/^[0-9]{5}$/.test($(this).val())) {
                		return "ZipCode is required";
                	}
                	return '';
                }
			],
			validateOnBlur: true,
			group: "<%=IDPrefix%>ValidateBeneficiary"
		});
		$('#<%=IDPrefix%>city' + index).observer({
			validators: [
                function (partial) {
                      if (!$("label[for='<%=IDPrefix%>city" + index+"']").hasClass('RequiredIcon')) return ""; //no validate if it is optional
                	if ($(this).closest("div.showfield-section").length > 0 && $(this).closest("div.showfield-section").hasClass("hidden")) return "";
                	if (!Common.ValidateText($(this).val())) {
                		return "City is required";
                	}
                	return '';
                }
			],
			validateOnBlur: true,
			group: "<%=IDPrefix%>ValidateBeneficiary"
		});
		$('#<%=IDPrefix%>state' + index).observer({
			validators: [
                function (partial) {
                    if (!$("label[for='<%=IDPrefix%>state" + index+"']").hasClass('RequiredIcon')) return ""; //no validate if it is optional
                	if ($(this).closest("div.showfield-section").length > 0 && $(this).closest("div.showfield-section").hasClass("hidden")) return "";
                	if (!Common.ValidateText($(this).val())) {
                		return "State is required";
                	}
                	return '';
                }
			],
			validateOnBlur: true,
			group: "<%=IDPrefix%>ValidateBeneficiary"
		});
    }

	function <%=IDPrefix%>registerBeneficiariesValidator() {
		var beneficiaryEle = $('#<%=IDPrefix%>beneficiaryContainer .Beneficiary');
        var count = 0;
        beneficiaryEle.each(function () {
            count += 1;
            <%=IDPrefix%>registerBeneficiaryValidator(count);
        });
    }
	function <%=IDPrefix%>getBenficiaryInfo() {
        var beneficiaryObj = function (firstName, lastName, relationship, percentShare,isTrust,dob, ssn, phone, address, zip, city, state) {
            this.firstName = firstName;
            this.lastName = lastName;
            this.relationship = relationship;
            this.percentShare = percentShare;
            this.isTrust = isTrust;
            this.dob = dob;
            this.ssn = ssn;
            this.phone = phone;
            this.address = address;
            this.zip = zip;
            this.city = city;
	        this.state = state;
        }
        var beneficiaryArr = [];
        var beneficiaryEle = $('#<%=IDPrefix%>beneficiaryContainer .Beneficiary');
		if ($('#<%=IDPrefix%>showBeneficiary').attr('status') == 'Y') {
            var count = 0;
            var firstName="", lastName="", relationship="", percentShare="", isTrust="", dob="", ssn="",phone="", address="", zip="",city="",state="";
  
            beneficiaryEle.each(function () {
                count += 1;
                firstName = $('#<%=IDPrefix%>first_name' + count).val();
            	lastName = $('#<%=IDPrefix%>last_name' + count).val();
            	relationship = $('#<%=IDPrefix%>relationship' + count).val();
            	phone = $('#<%=IDPrefix%>phone' + count).val();
				<%If EnableBeneficiarySSN then %>
            	ssn = Common.GetSSN($('#<%=IDPrefix%>txtssn' + count).val());
				<%End If%>
            	percentShare = $('#<%=IDPrefix%>percent_share' + count).val();
            	var chkYesEle = $('#<%=IDPrefix%>chkTrust' + count + "_yes");
                isTrust = "N"; //by default
				<%If EnableBeneficiaryTrust then %>
                if (chkYesEle.is(':checked')) {
                    isTrust = "Y";
                    lastName = "";
					<%If ForceSSNShown = false then %>
	                ssn = "";
					<%End If%>
                }
            	<%End If%>
				<%If EnableBeneficiaryAddress Then%>
            	address = $('#<%=IDPrefix%>address' + count).val();
            	zip = $('#<%=IDPrefix%>zip' + count).val();
            	city = $('#<%=IDPrefix%>city' + count).val();
            	state = $('#<%=IDPrefix%>state' + count).val();
            	<%End If%>
            	dob = $('#<%=IDPrefix%>txtdob' + count).val();
                beneficiaryArr.push(new beneficiaryObj(firstName, lastName, relationship, percentShare,isTrust,dob, ssn,phone, address, zip, city, state));
            });
        }
        return beneficiaryArr;
    }

	function <%=IDPrefix%>showAndHideBeneficiaryBtn(numCount, beneficiaryText) {
		$.lpqValidate.hideValidation("#<%=IDPrefix%>divSetPercentShare");
		if ($('#<%=IDPrefix%>showBeneficiary').attr("contenteditable") == "true") return false;
        if (numCount > 0) {
        	$('#<%=IDPrefix%>divBtn').show();
        	$("#<%=IDPrefix%>divSetPercentShare").removeClass("hidden");
        } else {
        	$('#<%=IDPrefix%>divBtn').hide();
        	$("#<%=IDPrefix%>divSetPercentShare").addClass("hidden");
        	$('#<%=IDPrefix%>showBeneficiary').html(translateRename('Add a beneficiary'));
        	var value = BUTTONLABELLIST[$.trim($('#<%=IDPrefix%>showBeneficiary').html()).toLowerCase()];
        	if (typeof value == "string" && $.trim(value) !== "") {
        		$('#<%=IDPrefix%>showBeneficiary').html(value);
        	}
        	$('#<%=IDPrefix%>showBeneficiary').attr('status', 'N');
        }
   }

	function <%=IDPrefix%>beneficiaryCounter() {
        var count = 0;
        function changeCounter(val) {
            count += val;
        }
        return {
            increment: function () {
                changeCounter(1);
            },
            decrement: function () {
                changeCounter(-1);
            },
            getCounter: function () {          
                return count;
            }
        }           
   }

	function <%=IDPrefix%>viewBeneficiaries() {
		if ($('#<%=IDPrefix%>divBeneficiary').hasClass("hidden")) return "";
		var beneficiaryInfo = <%=IDPrefix%>getBenficiaryInfo();
       var strHtml = "";
       for (var i = 0; i < beneficiaryInfo.length; i++) {
          strHtml += '<div class="row">';
          if (<%=IDPrefix%>isTrust(i + 1)) {
          	<%If EnableBeneficiaryTrust Then%>
	          if ($('#<%=IDPrefix%>divBeneficiaryTrust' + (i + 1)).hasClass("hidden") == false) {
		          strHtml += '<div class="col-sm-6 col-xs-12"><div class="row"><div class="col-xs-6 text-right row-title"><span class="bold">is Beneficiary a Trust?</span></div><div class="col-xs-6 text-left row-data"><span>Yes</span></div></div></div>';
	          }
	          <%End If%>
               strHtml += '<div class="col-sm-6 col-xs-12"><div class="row"><div class="col-xs-6 text-right row-title"><span class="bold">Trust Name</span></div><div class="col-xs-6 text-left row-data"><span>' + beneficiaryInfo[i].firstName + '</span></div></div></div>';
               strHtml += '<div class="col-sm-6 col-xs-12"><div class="row"><div class="col-xs-6 text-right row-title"><span class="bold">Date Established</span></div><div class="col-xs-6 text-left row-data"><span>' + beneficiaryInfo[i].dob + '</span></div></div></div>';
			<%If ForceSSNShown then %>
          	if ($('#<%=IDPrefix%>divBeneficiarySSN' + (i + 1)).hasClass("hidden") == false) {
          		strHtml += '<div class="col-sm-6 col-xs-12"><div class="row"><div class="col-xs-6 text-right row-title"><span class="bold">SSN</span></div><div class="col-xs-6 text-left row-data"><span>' + beneficiaryInfo[i].ssn.replace(/^\d{5}/, "*****") + '</span></div></div></div>';
          	}
	          <%End If%>
       	} else {
          	<%If EnableBeneficiaryTrust Then%>
	          if ($('#<%=IDPrefix%>divBeneficiaryTrust' + (i + 1)).hasClass("hidden") == false) {
		          strHtml += '<div class="col-sm-6 col-xs-12"><div class="row"><div class="col-xs-6 text-right row-title"><span class="bold">is Beneficiary a Trust?</span></div><div class="col-xs-6 text-left row-data"><span>No</span></div></div></div>';
	          }
	          <%End If%>
               strHtml += '<div class="col-sm-6 col-xs-12"><div class="row"><div class="col-xs-6 text-right row-title"><span class="bold">First Name</span></div><div class="col-xs-6 text-left row-data"><span>' + beneficiaryInfo[i].firstName + '</span></div></div></div>';
               strHtml += '<div class="col-sm-6 col-xs-12"><div class="row"><div class="col-xs-6 text-right row-title"><span class="bold">Last Name</span></div><div class="col-xs-6 text-left row-data"><span>' + beneficiaryInfo[i].lastName + '</span></div></div></div>';
               strHtml += '<div class="col-sm-6 col-xs-12"><div class="row"><div class="col-xs-6 text-right row-title"><span class="bold">Date of Birth</span></div><div class="col-xs-6 text-left row-data"><span>' + beneficiaryInfo[i].dob + '</span></div></div></div>';
          	<%If EnableBeneficiarySSN then %>
	          if ($('#<%=IDPrefix%>divBeneficiarySSN' + (i + 1)).hasClass("hidden") == false) {
		          strHtml += '<div class="col-sm-6 col-xs-12"><div class="row"><div class="col-xs-6 text-right row-title"><span class="bold">SSN</span></div><div class="col-xs-6 text-left row-data"><span>' + beneficiaryInfo[i].ssn.replace(/^\d{5}/, "*****") + '</span></div></div></div>';
	          }
	          <%End If%>
        }
        if (beneficiaryInfo[i].phone != "") {
            strHtml += '<div class="col-sm-6 col-xs-12"><div class="row"><div class="col-xs-6 text-right row-title"><span class="bold">Phone</span></div><div class="col-xs-6 text-left row-data"><span>' + beneficiaryInfo[i].phone + '</span></div></div></div>';
        }
        if (beneficiaryInfo[i].relationship != "") {
            strHtml += '<div class="col-sm-6 col-xs-12"><div class="row"><div class="col-xs-6 text-right row-title"><span class="bold">Relationship</span></div><div class="col-xs-6 text-left row-data"><span>' + beneficiaryInfo[i].relationship + '</span></div></div></div>';
        }
        if (beneficiaryInfo[i].percentShare != "") {
            strHtml += '<div class="col-sm-6 col-xs-12"><div class="row"><div class="col-xs-6 text-right row-title"><span class="bold">Percent Share</span></div><div class="col-xs-6 text-left row-data"><span>' + beneficiaryInfo[i].percentShare + '%</span></div></div></div>';
        }
       	<%If EnableBeneficiaryAddress then %>
	       if ($('#<%=IDPrefix%>divBeneficiaryAddress' + (i+1)).hasClass("hidden") == false) {
		       var addr = beneficiaryInfo[i].address + " " + beneficiaryInfo[i].city + ", " + beneficiaryInfo[i].state + " " + beneficiaryInfo[i].zip;
		       strHtml += '<div class="col-sm-6 col-xs-12"><div class="row"><div class="col-xs-6 text-right row-title"><span class="bold">Address</span></div><div class="col-xs-6 text-left row-data"><span>' + addr + '</span></div></div></div>';
	       }
	       <%End If%>
            strHtml += "</div><br/>";
       }
       return strHtml;
   }
	function <%=IDPrefix%>updateHiddenBeneficiarySSN(container, counter) {
		if ($("#<%=IDPrefix%>txtssn" + counter + "_1", $(container)).val().length == 3) {
			var ssn = $("#<%=IDPrefix%>txtssn" + counter + "_1", $(container)).val() + '-' + $("#<%=IDPrefix%>txtssn" + counter + "_2", $(container)).val() + '-' + $("#<%=IDPrefix%>txtssn" + counter + "_3", $(container)).val();
			$("#<%=IDPrefix%>txtssn" + counter, $(container)).val(ssn);
		}
	}
	function <%=IDPrefix%>updateHiddenBeneficiaryDOB(container, counter) {
		var month = padLeft($("#<%=IDPrefix%>txtdob" + counter + "_1").val(), 2);
		var day = padLeft($("#<%=IDPrefix%>txtdob" + counter + "_2").val(), 2);
		var year = padLeft($("#<%=IDPrefix%>txtdob" + counter + "_3").val(), 4);
		if (month != "" && day != "" && year != "") {
			$("#<%=IDPrefix%>txtdob").val(month + '/' + day + '/' + year);
		} else {
			$("#<%=IDPrefix%>txtdob").val("");
		}
	}
	function <%=IDPrefix%>addBeneficiaryBlock(cnt) {
		$('#<%=IDPrefix%>beneficiaryContainer').append(<%=IDPrefix%>renderingBeneficiaries(cnt));
		$('#<%=IDPrefix%>divBeneficiaryPanel').trigger('create');
		indexRenameItems("#<%=IDPrefix%>beneficiary" + cnt);
		<%If IsInMode("777") Andalso IsInFeature("rename")%>
		attachEditButton("#<%=IDPrefix%>beneficiary" + cnt);
		<%End If%>
		performRename("#<%=IDPrefix%>beneficiary" + cnt);
		$("#<%=IDPrefix%>beneficiary" + cnt + " select").each(function (idx, ele) {
			$(ele).on("change", function () {
				setTimeout(function () {
					$(ele).focus();
				}, 100);
			});
		});
		if ($("#<%=IDPrefix%>beneficiary" + cnt + " input:checkbox").length > 0) {
			$("#<%=IDPrefix%>beneficiary" + cnt + " input:checkbox").first().focus();
		}
		$("span.lock-ico", $("#<%=IDPrefix%>beneficiary" + cnt)).html($("#<%=IDPrefix%>divIconLock16").html());
		$("span.question-ico", $("#<%=IDPrefix%>beneficiary" + cnt)).html($("#<%=IDPrefix%>divIconQuestion16").html());
		if (isMobile.any() && !isMobile.Windows()) {//moz-text-security does not works on window devices
			$("#<%=IDPrefix%>txtssn" + cnt + "_1,#<%=IDPrefix%>txtssn" + cnt + "_2", "#<%=IDPrefix%>beneficiary" + cnt)
			.attr("type", "tel")
			.addClass("mask-password");
		} else {
			$("#<%=IDPrefix%>txtssn" + cnt + "_1,#<%=IDPrefix%>txtssn" + cnt + "_2", "#<%=IDPrefix%>beneficiary" + cnt)
			.attr("type", "password");
		}
		$('.ui-input-ssn input', "#<%=IDPrefix%>beneficiary" + cnt).on('focusout', function () {
			<%=IDPrefix%>updateHiddenBeneficiarySSN("#<%=IDPrefix%>beneficiary" + cnt, cnt);
		});
		$('.ui-input-date input', "#<%=IDPrefix%>beneficiary" + cnt).on('focusout', function () {
			<%=IDPrefix%>updateHiddenBeneficiaryDOB("#<%=IDPrefix%>beneficiary" + cnt, cnt);
		});
		<%--<%If IsInMode("777") AndAlso IsInFeature("visibility") then%>
		var $beneficiarySSNBlock = $("#<%=IDPrefix%>divBeneficiarySSN" + cnt);
		var $beneficiaryTrustBlock = $("#<%=IDPrefix%>divBeneficiaryTrust" + cnt);
		if (typeof ShowFieldButton == "function") {
			ShowFieldButton($beneficiarySSNBlock);
			ShowFieldButton($beneficiaryTrustBlock);
			<%=IDPrefix%>updateBeneficiaryBlockState($beneficiarySSNBlock, $("#<%=IDPrefix%>beneficiaryContainer").data("ssn-initial-state"));
			<%=IDPrefix%>updateBeneficiaryBlockState($beneficiaryTrustBlock, $("#<%=IDPrefix%>beneficiaryContainer").data("trust-initial-state"));
			$beneficiarySSNBlock.on("change", function (evt, isVisible, srcEle, state) {
				$("#<%=IDPrefix%>beneficiaryContainer").data("ssn-initial-state", state);
				$(".showfield-section.ssn-block", "#<%=IDPrefix%>beneficiaryContainer").not($beneficiarySSNBlock).each(function (idx, ele) {
					<%=IDPrefix%>updateBeneficiaryBlockState(ele, state);
				});
			});
			$beneficiaryTrustBlock.on("change", function (evt, isVisible, srcEle, state) {
				$("#<%=IDPrefix%>beneficiaryContainer").data("trust-initial-state", state);
				$(".showfield-section.trust-block", "#<%=IDPrefix%>beneficiaryContainer").not($beneficiaryTrustBlock).each(function (idx, ele) {
					<%=IDPrefix%>updateBeneficiaryBlockState(ele, state);
				});
			});
		}
	    <%End If%>--%>
	    <%=IDPrefix%>beneficiaryPhoneFormat();
		<%If IsInMode("777") AndAlso IsInFeature("visibility") Then%>
		<%If EnableBeneficiaryTrust then%>
		ShowFieldButton($("#<%=IDPrefix%>divBeneficiaryTrust" + cnt));
		triggerUpdateShowField({controllerId: '<%=BuildShowFieldSectionID("divBeneficiaryTrust1")%>'});
		<%End If%>
		<%If EnableBeneficiarySSN Then%>
		ShowFieldButton($("#<%=IDPrefix%>divBeneficiarySSN" + cnt));
		triggerUpdateShowField({ controllerId: '<%=BuildShowFieldSectionID("divBeneficiarySSN1")%>' });
		<%End If%>
		<%If EnableBeneficiaryAddress then %>
		ShowFieldButton($("#<%=IDPrefix%>divBeneficiaryAddress" + cnt));
		triggerUpdateShowField({ controllerId: '<%=BuildShowFieldSectionID("divBeneficiaryAddress")%>' });
		<%End If%>
		<%End If%>
	}
	<%--<%If IsInMode("777") AndAlso IsInFeature("visibility") then%>
	function <%=IDPrefix%>updateBeneficiaryBlockState(block, state) {
		var $controller = $(block);
		var $btnShowField = $controller.prev("div.btn-showfield");
		$btnShowField.find("div[data-command]").removeClass("active");
		if (state == null) { //default state
			$btnShowField.find("div.default-state").removeClass("hidden");
			if ($controller.data("default-state") == "off") {
				$controller.addClass("hidden");
				$btnShowField.addClass("section-off");
			} else {
				$controller.removeClass("hidden");
				$btnShowField.removeClass("section-off");
			}
		} else {
			$btnShowField.find("div.default-state").addClass("hidden");
			if (state == true) {
				$controller.removeClass("hidden");
				$btnShowField.removeClass("section-off");
				$btnShowField.find("div[data-command='visible']").addClass("active");
			} else {
				$controller.addClass("hidden");
				$btnShowField.addClass("section-off");
				$btnShowField.find("div[data-command='hidden']").addClass("active");
			}
		}
	}
	function <%=IDPrefix%>setBeneficiarySsnInitialState(state) {
		$("#<%=IDPrefix%>beneficiaryContainer").data("ssn-initial-state", state);
	}
	function <%=IDPrefix%>setBeneficiaryTrustInitialState(state) {
		$("#<%=IDPrefix%>beneficiaryContainer").data("trust-initial-state", state);
	}
	<%End If%>--%>
    $(function () {
        //beneficiary
    	var counter = <%=IDPrefix%>beneficiaryCounter();
        var beneficiaryText = "Add beneficiary"; //by default
        $(document).on('pageshow', function () {
        	beneficiaryText = $('#<%=IDPrefix%>showBeneficiary').text(); //override beneficiary text
        });
        
    	$('#<%=IDPrefix%>showBeneficiary').on('click', function (e) {
    		if ($('#<%=IDPrefix%>beneficiaryContainer').children().length == 0) {
            	counter.increment();
            	var cnt = counter.getCounter();
	            <%=IDPrefix%>addBeneficiaryBlock(cnt);
            }
            <%=IDPrefix%>showBeneficiary(this, beneficiaryText);
           <%=IDPrefix%>showAndHideBeneficiaryBtn(counter.getCounter(), beneficiaryText);
           if ($(this).attr('status') == 'Y') {         
				delete OBSERVERDB["<%=IDPrefix%>ValidateBeneficiary"];
				<%=IDPrefix%>registerBeneficiariesValidator();
           }
	        e.preventDefault();
        });
    	$('.addDivBtn', "#<%=IDPrefix%>divBeneficiary").on('click', function () {
        	counter.increment();
        	var cnt = counter.getCounter();
        	<%=IDPrefix%>addBeneficiaryBlock(cnt);
           
        });
    	$('.removeDivBtn', "#<%=IDPrefix%>divBeneficiary").on('click', function () {
			
        	if (counter.getCounter() > 0) {
        		$('#<%=IDPrefix%>beneficiary' + counter.getCounter()).remove();
		        counter.decrement();
		        <%=IDPrefix%>showAndHideBeneficiaryBtn(counter.getCounter());
        		var totalShare = <%=IDPrefix%>calculateTotalShared();
        		$("#<%=IDPrefix%>spTotalShared").text(Common.FormatPercent(totalShare, true, 2));
	        }
        	if ($('#<%=IDPrefix%>showBeneficiary').attr('status') == 'Y') {
        		$("#<%=IDPrefix%>divSetPercentShare").trigger("change");
        		$('html, body').stop().animate({ scrollTop: $("#<%=IDPrefix%>divSetPercentShare").offset().top - 100 }, '500', 'swing');
	        } else {
        		$('html, body').stop().animate({ scrollTop: $("#<%=IDPrefix%>showBeneficiary").offset().top - 100 }, '500', 'swing');
	        }
        });
      
    	$('#<%=IDPrefix%>divBtn').on('click', function () {
    		delete OBSERVERDB["<%=IDPrefix%>ValidateBeneficiary"];
          <%=IDPrefix%>registerBeneficiariesValidator();
        });
             
        //handle toggle Yes/No checkbox     
    	$('#<%=IDPrefix%>beneficiaryContainer').on('change', 'input[type="checkbox"]', function (e) {
            var currentElem = $(this);
            var chkEleID = currentElem.attr('id');
            var bIndex = chkEleID.match(/\d+/g).pop(); //get last digit for index
            var isTrust = false;
            $.lpqValidate.hideValidation("#<%=IDPrefix%>divssn" + bIndex);
            if (currentElem.is(':checked')) {
                if (chkEleID.indexOf("no") > -1) {
                    var chkYesID = chkEleID.replace("no", "yes");
                    $('#' + chkYesID).prop("checked", false).checkboxradio("refresh");

                } else {
                    var chkNoID = chkEleID.replace("yes", "no");
                    $('#' + chkNoID).prop("checked", false).checkboxradio("refresh");
                    isTrust = true;
                }

                <%=IDPrefix%>handleShowAndHideBeneficiaryFields(bIndex, isTrust);
            } else {
                //prevent unchecked
                currentElem.prop("checked", true).checkboxradio("refresh");        
            }
        });
    	$('#<%=IDPrefix%>beneficiaryContainer').on('blur', 'input.numeric', function (e) {
	        if ($.trim($(this).val()) == "") $(this).val(0);
	        var totalShare = <%=IDPrefix%>calculateTotalShared();
    		$("#<%=IDPrefix%>spTotalShared").text(Common.FormatPercent(totalShare, true, 2));
    		$("#<%=IDPrefix%>divSetPercentShare").trigger("change");
        });
    	$('#<%=IDPrefix%>beneficiaryContainer').on('focusout', 'div.ui-input-date input', function () {
          var currentEle = $(this);
            var maxlength = parseInt(currentEle.attr('maxlength'),10);
            // add 0 if value < 10
            currentEle.val(padLeft(currentEle.val(), maxlength));
            var currentID = currentEle.attr('id');
           var prefixID = currentID.substring(0, currentID.length - 2);
           var month = padLeft($("#" + prefixID + "_1").val(), 2);
           var day = padLeft($("#" + prefixID + "_2").val(), 2);
           var year = padLeft($("#" + prefixID + "_3").val(), 4);

           var date = month + '/' + day + '/' + year;
            //add birthday to hidden txtDOBID
           var txtDOBID = prefixID.replace("div", "txt");
           $('#' + txtDOBID).val(date == "//" ? "" : date);       
        });
    	$("#<%=IDPrefix%>popupDistributeEvenlyNote, #<%=IDPrefix%>popupDistributeEvenly").on("popupafteropen", function (event, ui) {
    		$("#" + this.id + "-screen").height("");
        });
    	$("#<%=IDPrefix%>popupDistributeEvenlyNote").on("popupafterclose", function (event, ui) {
    		$("#<%=IDPrefix%>popupDistributeEvenly").popup("open");
        });

    	$("#<%=IDPrefix%>popupDistributeEvenly").on("popupafterclose", function (event, ui) {
        	//fill value back to main field
    		$(".beneficiary-amount input.percent", "#<%=IDPrefix%>popupDistributeEvenly").each(function (idx, ele) {
        		var $ele = $(ele);
        		$("#<%=IDPrefix%>percent_share" + $ele.data("index")).val(Common.GetPercentFromPercentString($ele.val()));
        	});
    		var totalShare = <%=IDPrefix%>calculateTotalShared();
    		$("#<%=IDPrefix%>spTotalShared").text(Common.FormatPercent(totalShare, true, 2));
    		$("#<%=IDPrefix%>divSetPercentShare").trigger("change");
        });
	    
    	$("#<%=IDPrefix%>popupDistributeEvenly label.beneficiary-total-amount").on("change", function () {
    		var totalShare = <%=IDPrefix%>calculatePopupTotalShared();
		    $(this).text(Common.FormatPercent(totalShare, true, 2));
			if (totalShare == 100) {
				$(this).removeClass("invalid");
			} else {
				$(this).addClass("invalid");
			}
	    });
    });
	function <%=IDPrefix%>calculatePopupTotalShared() {
		var totalValue = 0;
		$(".beneficiary-amount input.percent", "#<%=IDPrefix%>popupDistributeEvenly").each(function (idx, ele) {
			var $ele = $(ele);
			totalValue += Common.GetPercentFromPercentString($ele.val());
		});
		return totalValue;
	}
	function <%=IDPrefix%>calculateTotalShared() {
		var totalValue = 0;
		$("input[id*=percent_share]", "#<%=IDPrefix%>beneficiaryContainer").each(function (idx, ele) {
			var $ele = $(ele);
			totalValue += Common.GetPercentFromPercentString($ele.val());
		});
		return totalValue;
	}

    // Handles moving focus across SSN field DOB fields
	function <%=IDPrefix%>onKeyUp_DOB(evt, element) {
        //console.log(evt.target, evt.currentTarget, evt.keyCode);
        var elementID = $(element).attr('id');
        var txt1Len = $(element).attr('maxlength');
        var txt1Value = $(element).val();
        if ($(element)[0].hasAttribute("mask-pattern")) {
            //use for SSN text input
            txt1Value = txt1Value.replace(/_/g, "");
        }
        if (txt1Len == txt1Value.length && $(element).data("disable-move-next") != true) {
            $(element).data("disable-move-next", true);
            if (elementID.indexOf("_1") > -1) {
                var dob2ID = elementID.replace("_1", "_2");
                $('#' + dob2ID).focus();
            } else if (elementID.indexOf("_2") > -1) {
                var dob3ID = elementID.replace("_2", "_3");
                $('#' + dob3ID).focus();
            }            
        }
    }
    //phone format
	function <%=IDPrefix%>beneficiaryPhoneFormat() {
        if (!isMobile.Android()) {
        	$('#<%=IDPrefix%>beneficiaryContainer input.inphone').inputmask('(999) 999-9999');
        } else {
            // phone format
        	$('#<%=IDPrefix%>beneficiaryContainer').on('blur', 'input.inphone', function () {
                var element = $(this);
                element.attr('maxlength', '14'); // (xxx) xxx-xxxx ->maxlength =14
                element.val(Common.FormatPhone(element.val()));
            });
        	$('#<%=IDPrefix%>beneficiaryContainer').on('keyup', 'input.inphone', function () {
                var element = $(this);
                var phoneNumber = element.val();
                if (!isNaN(phoneNumber)) {
                    element.attr('maxlength', '10');
                } else {
                    if (phoneNumber.indexOf(" ") == -1) {
                        element.attr('maxlength', '13');
                    } else {
                        element.attr('maxlength', '14');
                    }
                }
            });
        	$('#<%=IDPrefix%>beneficiaryContainer').on('keydown', 'input.inphone', function (event) {
                var key = event.keyCode;
                if (key == 32) {//disable spacing bar for phone
                    event.preventDefault();
                }
            });
        }
    }
    
	function <%=IDPrefix%>handleShowAndHideBeneficiaryFields(bIndex, isTrust) {
		var divlastNameEle = $('#<%=IDPrefix%>last_name' + bIndex).closest('div[data-role="fieldcontain"]');
		var divssnEle = $('#<%=IDPrefix%>txtssn' + bIndex).closest('div[data-role="fieldcontain"]');
		//var labelFirstNameEle = $('#<%=IDPrefix%>first_name' + bIndex).closest('div').prev('label');
		//var labelDeEle = $('#<%=IDPrefix%>lblde' + bIndex);
		//var labelDobEle = $('#<%=IDPrefix%>lbldob' + bIndex);
        //var labelDobText = labelDobEle.html();
        //var firstNameText = labelFirstNameEle.html();
        if (isTrust) {
            //labelDobText = labelDobText.replace("Date of Birth", "Date Established");
        	//labelDobEle.html(labelDobText);
        	$('#<%=IDPrefix%>lblde' + bIndex).closest(".ui-label-date-established").removeClass("hidden");
        	$('#<%=IDPrefix%>lbldob' + bIndex).closest(".ui-label-dob").addClass("hidden");
        	divlastNameEle.hide();
			<%If ForceSSNShown = false then%>
	        divssnEle.hide();
			<%End If%>
            //firstNameText = firstNameText.replace("First Name", "Trust Name");
        	//labelFirstNameEle.html(firstNameText);
	        $('#<%=IDPrefix%>lblFirstName' + bIndex).closest("div").addClass("hidden");
        	$('#<%=IDPrefix%>lblTrustName' + bIndex).closest("div").removeClass("hidden");
        } else {
            //labelDobText = labelDobText.replace("Date Established","Date of Birth");
        	//labelDobEle.html(labelDobText);
        	$('#<%=IDPrefix%>lblde' + bIndex).closest(".ui-label-date-established").addClass("hidden");
        	$('#<%=IDPrefix%>lbldob' + bIndex).closest(".ui-label-dob").removeClass("hidden");
            divlastNameEle.show();
            divssnEle.show();
            //firstNameText = firstNameText.replace("Trust Name", "First Name");
        	//labelFirstNameEle.html(firstNameText);
            $('#<%=IDPrefix%>lblFirstName' + bIndex).closest("div").removeClass("hidden");
            $('#<%=IDPrefix%>lblTrustName' + bIndex).closest("div").addClass("hidden");
        }
		$.lpqValidate.hideValidation($("#<%=IDPrefix%>divdob" + bIndex));
	}
  
	function <%=IDPrefix%>isTrust(index) {
		<%If EnableBeneficiaryTrust Then%>
		if ($('#<%=IDPrefix%>chkTrust' + index + "_yes").is(':checked')) {
            return true;
        }
		<%End If%>
        return false;
	}
	

	function <%=IDPrefix%>verifyBeneficiaryAddress(index) {
		if (Common.ValidateZipCode($('#<%=IDPrefix%>zip' + index).val(), $('#<%=IDPrefix%>zip' + index)) == false) return;
		if ($('#hdAddressKey').val() != "") {
			var address = <%=IDPrefix%>getBeneficiaryAddress(index);
			if (<%=IDPrefix%>validateBeneficiaryAddress(index)) {
    			var url = '/handler/Handler.aspx';
    			$.ajax({
    				url: url,
    				async: true,
    				cache: false,
    				type: 'POST',
    				dataType: 'html',
    				data: {
    					command: 'verifyAddress',
    					address: address
    				},
    				success: function (responseText) {
    					if (responseText.indexOf('res="OK"') < 0) {
    						//address service is down or configuration is bad, dont do anything
    						//TODO: log the error or email admin   

    					} else if (responseText.indexOf('FoundSingleAddress="False"') > -1) { //not found single address                      
    						$('#<%=IDPrefix%>spVerifyBeneficiaryAddressMessage' + index).show();
        					var shortDesc = /ShortDescriptive="(.*?)"/g.exec(responseText);
        					if (shortDesc[1] == 'Not Found') {
        						shortDesc[1] = "Unable to Validate Address";
        					}
        					$('#<%=IDPrefix%>spVerifyBeneficiaryAddressMessage' + index).text(shortDesc[1]);
						} else if (responseText.indexOf('FoundSingleAddress="True"') > -1) { //found single address                      
							$('#<%=IDPrefix%>spVerifyBeneficiaryAddressMessage' + index).hide();
								<%=IDPrefix%>updateBeneficiaryAddress(responseText, index);
							} else { //unknow case                      
								$('#<%=IDPrefix%>spVerifyBeneficiaryAddressMessage' + index).hide();
							}
        			}
        		});
			}
		}
	}
	function <%=IDPrefix%>validateBeneficiaryAddress(index) {
		return (($('#<%=IDPrefix%>address' + index).val() != '' && $('#<%=IDPrefix%>zip' + index).val() != '') || ($('#<%=IDPrefix%>address' + index).val() != '' && $('#<%=IDPrefix%>city' + index).val() != '' && $('#<%=IDPrefix%>state' + index).val() != ''));
	}
	function <%=IDPrefix%>getBeneficiaryAddress(index) {
		var zip = "";
		if ($('#<%=IDPrefix%>zip' + index).length > 0 && Common.ValidateZipCode($('#<%=IDPrefix%>zip' + index).val()) == true) {
			zip = $('#<%=IDPrefix%>zip' + index).val().replace(/-/g, "").substring(0, 5);
		}
		var address = 'street=' + $('#<%=IDPrefix%>address' + index).val() + '&city=' + $('#<%=IDPrefix%>city' + index).val() + '&state=' + $('#<%=IDPrefix%>state' + index).val() + '&zip=' + zip;
		return address;
	}

	function <%=IDPrefix%>updateBeneficiaryAddress(responseText, index) {
		var txtAddress = $('#<%=IDPrefix%>address' + index);
		var txtCity = $('#<%=IDPrefix%>city' + index);
		var txtZip = $('#<%=IDPrefix%>zip' + index);
		var ddlState = $('#<%=IDPrefix%>state' + index);
		txtAddress.val(/street="(.*?)"/g.exec(responseText)[1]);
		$.lpqValidate.hideValidation(txtAddress);
		txtCity.val(/city="(.*?)"/g.exec(responseText)[1]);
		$.lpqValidate.hideValidation(txtCity);
		ddlState.val(/state="(.*?)"/g.exec(responseText)[1]).selectmenu().selectmenu('refresh');
		$.lpqValidate.hideValidation(ddlState);
		if (txtZip.is(":disabled")) {
			<%--When zip field (primary applicent) is locked due to zip pool enabled, do not allow override it by address verify task--%>
			var match = /zip="([0-9]{5}).*?["]/g.exec(responseText);
			if (match != null && match.length > 0 && match[1] == txtZip.val()) {
				$('#<%=IDPrefix%>spVerifyBeneficiaryAddressMessage' + index).hide();
			} else {
				$('#<%=IDPrefix%>spVerifyBeneficiaryAddressMessage' + index).text("Unable to Validate Address");
				$('#<%=IDPrefix%>spVerifyBeneficiaryAddressMessage' + index).show();
			}
		} else {
			txtZip.val(/zip="(.*?)["]/g.exec(responseText)[1]);  //zip code may be 5 or 10 digit
		}
		$.lpqValidate.hideValidation(txtZip);
	}
</script>



