﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="FinancialInfo.ascx.vb"
    Inherits="Inc_FinancialInfo" %>
<%@ Import Namespace="Newtonsoft.Json" %>
<% If IsBusinessLoan Then %>
    <div>
     <%=HeaderUtils.RenderPageTitle(0, "Financial Information", True)%>
    </div>
<%End If %>
<div>
 <br /><label><em><sup>§</sup>Alimony, child support and separate maintenance income need not be revealed if you do not wish to have them considered as a basis for repaying this obligation.</em></label>
</div>
<div data-role="fieldcontain">
    <label for="<%=IDPrefix%>txtGrossMonthlyIncome" id="<%=IDPrefix%>lblGrossMonthlyIncome">Gross Monthly Income (before taxes)</label>
    <input type="<%=TextAndroidTel%>" pattern="[0-9]*" id="<%=IDPrefix%>txtGrossMonthlyIncome" class="money" maxlength ="10"/>
</div>

<!--gross Monthly income tax exempt -->
<div data-role="fieldcontain" style="display:none">
    <label for="<%=IDPrefix%>grossMonthlyIncomeTaxExempt">Gross Monthly Income Tax Exempt?</label>
    <input type="checkbox" id="<%=IDPrefix%>grossMonthlyIncomeTaxExempt" />
</div>

<!--other monthly income -->
 <div id="<%=IDPrefix%>divOtherMonthlyIncome">
     <div style="margin-top: 10px;">
        <a id="<%=IDPrefix%>lnkHasOtherMonthlyIncome" href="#" data-mode="self-handle-event" status="N" style="cursor: pointer;font-weight :bold" class="header_theme2 shadow-btn plus-circle-before">Add other income</a>
    </div>
</div>
<div id="<%=IDPrefix%>divOtherMonthlyIncomeForm" style="display :none">
    <div data-role="fieldcontain">
        <label for="<%=IDPrefix%>txtOtherMonthlyIncome">Other Monthly Income</label>
        <input type="<%=TextAndroidTel%>" pattern="[0-9]*" class="money" maxlength="10" id="<%=IDPrefix%>txtOtherMonthlyIncome" />
    </div>
    <!--other Monthly income tax exempt -->
   <div data-role="fieldcontain">
    <label for="<%=IDPrefix%>otherMonthlyIncomeTaxExempt">Other Monthly Income Tax Exempt?</label>
    <input type="checkbox" id="<%=IDPrefix%>otherMonthlyIncomeTaxExempt" />
   </div>
    <div data-role="fieldcontain">
        <label for="<%=IDPrefix%>txtOtherMonthlyIncomeDesc">Other Monthly Income Description</label>
        <input type="text" maxlength="150" id="<%=IDPrefix%>txtOtherMonthlyIncomeDesc" />
    </div>
	
</div>
<br />
<!--end other monthly income -->
<div data-role="fieldcontain">
    <label for="<%=IDPrefix%>txtTotalMonthlyHousingExpense" id="<%=IDPrefix%>lblTotalMonthlyHousingExpense">Monthly Mortgage/Rent Payment</label>
    <input type="<%=TextAndroidTel%>" pattern="[0-9]*" id="<%=IDPrefix%>txtTotalMonthlyHousingExpense" class="money" maxlength ="10" />
</div>
     

<div data-role="fieldcontain">
    <label for="<%=IDPrefix%>ddlEmploymentStatus" class="RequiredIcon">Employment Status</label>
    <select id='<%=IDPrefix%>ddlEmploymentStatus' onchange="<%=IDPrefix%>HandleEmploymentStatusChange(this)">
         <!--add a blank field for the employment status so it won’t defaulted to Employed -->
        <%=EmploymentStatusDropdown%>
    </select>
</div>
<%If CollectDescriptionIfEmploymentStatusIsOther.ToUpper() = "Y" Then%>
    <div id="<%=IDPrefix%>divCollectDescriptionIfEmploymentStatusIsOther" style="display: none;">
        <div data-role="fieldcontain">
            <label for="<%=IDPrefix%>txtEmploymentDescription" class="RequiredIcon">Other Employment Description</label>
            <input type="text" id="<%=IDPrefix%>txtEmploymentDescription" maxlength="90"/>
        </div>  
    </div>
<%End If%>
<div id='<%=IDPrefix%>divEmploymentStatusRest'>
</div>
<!--Previous Employment -->
<div id="<%=IDPrefix%>divPreviousEmploymentInfo" style= "display:none;">
	<br />
    <div style="font-weight :bold;font-size:17px">Previous Employment Information</div>
    <div data-role="fieldcontain">
        <label for="<%=IDPrefix%>prev_txtGrossMonthlyIncome" id="<%=IDPrefix%>prev_lblGrossMonthlyIncome">Gross Monthly Income (before taxes)</label>
        <input type="<%=TextAndroidTel%>" pattern="[0-9]*" id="<%=IDPrefix%>prev_txtGrossMonthlyIncome" class="money" maxlength ="10"/>
    </div>
    <div data-role="fieldcontain">
        <label for="<%=IDPrefix%>prev_ddlEmploymentStatus" >Employment Status</label>
        <select id='<%=IDPrefix%>prev_ddlEmploymentStatus' onchange="<%=IDPrefix%>prev_HandleEmploymentStatusChange(this)">
             <!--add a blank field for the employment status so it won’t defaulted to Employed -->
            <%=EmploymentStatusDropdown%>
        </select>
    </div>
    <div id='<%=IDPrefix%>prev_divEmploymentStatusRest'>
  </div>
</div>
<div id="<%=IDPrefix%>popGrossMonthlyIncomWarning" data-role="popup" style="max-width: 500px;">
	<div data-role="content">
		<div class="row" style="padding: 20px 0 0;">
			<div class="col-sm-12">
				<div style="margin: 10px 0; font-weight: normal;" data-place-holder="content"></div>     
			</div>    
		</div>
	</div>
	<div class="div-continue js-no-required-field" style="text-align: center;"><a href="#" data-transition="slide" type="button" onclick="closePopup('#<%=IDPrefix%>popGrossMonthlyIncomWarning')" data-role="button" class="div-continue-button">OK</a></div>
</div>
<input type="hidden" id="<%=IDPrefix%>hdShowPreviousEmployment" value="N" />
<script type="text/javascript">
  
	function <%=IDPrefix%>HandleEmploymentStatusChange(ele) {
		<%If CollectDescriptionIfEmploymentStatusIsOther.ToUpper() = "Y" Then%>
		var sEmpStatus = $(ele).val();
		if (sEmpStatus.toUpperCase() == "OTHER") {
			$("#<%=IDPrefix%>divCollectDescriptionIfEmploymentStatusIsOther").show();
		} else {
			$("#<%=IDPrefix%>divCollectDescriptionIfEmploymentStatusIsOther").hide();
			$.lpqValidate.hideValidation($('#<%=IDPrefix%>txtEmploymentDescription'));
		}
		<%End If%>
		<%=IDPrefix%>updateIncomeTitle();
	}

	function <%=IDPrefix%>prev_HandleEmploymentStatusChange(ele) {
		<%=IDPrefix%>prev_updateIncomeTitle();
	}

	function <%=IDPrefix%>employmentStatusRequiresMonthlyIncome(sEmploymentStatus) {
		if (sEmploymentStatus == 'RETIRED' || sEmploymentStatus == 'RETIRED MILITARY' || sEmploymentStatus == 'STUDENT' || sEmploymentStatus == 'UNEMPLOYED'
            || sEmploymentStatus == 'GOVERNMENT/DOD' || sEmploymentStatus == 'HOMEMAKER' || sEmploymentStatus == 'OTHER') {
			return true;
        } else {
			return false;
        }
	}

	function <%=IDPrefix%>updateIncomeTitle() {
        var sEmploymentStatus = "";
        sEmploymentStatus = $('#<%=IDPrefix%>ddlEmploymentStatus option:selected').val();
        if (<%=IDPrefix%>employmentStatusRequiresMonthlyIncome(sEmploymentStatus)) {
        	$('#<%=IDPrefix%>lblGrossMonthlyIncome').removeClass("RequiredIcon"); // not required
        	$.lpqValidate.hideValidation($('#<%=IDPrefix%>txtGrossMonthlyIncome'));
        } else {
        	$('#<%=IDPrefix%>lblGrossMonthlyIncome').addClass("RequiredIcon"); // required
        }
	}

	function <%=IDPrefix%>prev_updateIncomeTitle() {
		var sPrevEmploymentStatus = "";
		sPrevEmploymentStatus = $('#<%=IDPrefix%>prev_ddlEmploymentStatus option:selected').val();
		if (<%=IDPrefix%>employmentStatusRequiresMonthlyIncome(sPrevEmploymentStatus)) {
			$('#<%=IDPrefix%>prev_lblGrossMonthlyIncome').removeClass("RequiredIcon"); //not required
			$.lpqValidate.hideValidation($('#<%=IDPrefix%>prev_txtGrossMonthlyIncome'));
        } else {
			$('#<%=IDPrefix%>prev_lblGrossMonthlyIncome').addClass("RequiredIcon"); // required   
        }
	}

	function <%=IDPrefix%>ValidateApplicantFinancialInfo() {

		<%--var txtJobTitle = $('#<%=IDPrefix%>txtJobTitle').val();
        var txtEmployer = $('#<%=IDPrefix%>txtEmployer').val();
        var txtLengthOfEmploymentYear = $('#<%=IDPrefix%>txtEmployedDuration_year option:selected').val();
        var ddlLengthOfEmploymentMonth = $('#<%=IDPrefix%>txtEmployedDuration_month option:selected').val();--%>
		// If the the <coprefix> == "" then we are checking the main applicant
		// if <coprefix> != "" then we are checking the coapplicant
		// so when the two text fields match, that means that the <coprefix> wasn't set and it's the main applicant
		<%--var isCoAppField = ($('#<%=IDPrefix%>txtGrossMonthlyIncome').val() != $('#txtGrossMonthlyIncome').val()); --%>
		<%--var ddlEmploymentStatus = $('#<%=IDPrefix%>ddlEmploymentStatus').val();
    	var txtGrossMonthlyIncome = $('#<%=IDPrefix%>txtGrossMonthlyIncome').val();
        var txtTotalMonthlyHousingExpense = $('#<%=IDPrefix%>txtTotalMonthlyHousingExpense').val();
        var ddlOccupyingStatus = $('#<%=IDPrefix%>ddlOccupyingStatus').val();
		var strMessage = '';
        
    	//validate monthly income -->always required
        if (!Common.ValidateTextNonZero(txtGrossMonthlyIncome)) {
        	strMessage += 'Please complete the gross monthly income field. <br/>';
        }
        //validate other monthly income
        strMessage += <%=IDPrefix%>ValidateOtherMonthlyIncome();

        if (!Common.ValidateText(ddlEmploymentStatus)) {
            strMessage += 'Employment Status is required<br />';
        }
        
        strMessage += '<%=IDPrefix%>'.length > 0 ? EMPLogicJN.validate() : EMPLogicPI.validate();
        //validate previous employment if it exist
        var previousEmploymentMessage = "";
        if ($('#<%=IDPrefix%>hdShowPreviousEmployment').val() == 'Y') {
            var ddlPrevEmploymentStatus = $('#<%=IDPrefix%>prev_ddlEmploymentStatus option:selected').val();
            var prev_txtGrossMonthlyIncome = $('#<%=IDPrefix%>prev_txtGrossMonthlyIncome').val();
            //validate monthly income -->always required
            if (!Common.ValidateTextAllowZero(prev_txtGrossMonthlyIncome)) {
                previousEmploymentMessage += 'Please complete the gross monthly income field. <br/>';
            }
            if (!Common.ValidateText(ddlPrevEmploymentStatus)) {
                previousEmploymentMessage += 'Employment Status is required<br />';
            }
            previousEmploymentMessage += '<%=IDPrefix%>'.length > 0 ? EMPLogicJN_PREV.validate() : EMPLogicPI_PREV.validate();
        }
        if (previousEmploymentMessage != "") {
            strMessage += "Please complete the previous employment information: <br/><div style='padding:0px 0px 0px 20px'>" + previousEmploymentMessage +"</div>";
        }

        //morgate/rent payment field is required when user select Rent 
        if (ddlOccupyingStatus == "RENT") {
                if (!Common.ValidateTextAllowZero(txtTotalMonthlyHousingExpense)) {
                	strMessage += 'Please complete the monthly mortgage/rent payment field.<br />';
                }
        }     
        return strMessage;--%>

		var validate1 = $.lpqValidate("<%=IDPrefix%>ValidateFinancialInfo");
        var validate2 = true;
       	<%If IDPrefix = "co_" Then%>
    	  validate2 = EMPLogicJN.validate2();
    	<%ElseIf IDPrefix = "" Then%>
    	    validate2 = EMPLogicPI.validate2();
		<%Else%>
    	  validate2 = EMPLogic<%=IDPrefix%>.validate2();
    	<%End If%>
	
		var validate3 = true;
		var validate4 = true;
		//validate previous employment if it exist
		if ($('#<%=IDPrefix%>hdShowPreviousEmployment').val() == 'Y') {
			validate3 = $.lpqValidate("<%=IDPrefix%>prev_ValidateFinancialInfo");
			<%If IDPrefix = "co_" Then%>
    		    validate4 = EMPLogicJN_PREV.validate2();
    		<%ElseIf IDPrefix = "" Then%>
    		    validate4 = EMPLogicPI_PREV.validate2();
			<%Else%>
    		   validate4 = EMPLogic<%=IDPrefix%>PREV.validate2();
    		<%End If%>
		}
		return validate1 && validate2 && validate3 && validate4;
	}

    
	<%--
	NO USE ANYMORE
	function <%=IDPrefix%>ValidateOtherMonthlyIncome() {
		var otherMonthlyIncome = $('#<%=IDPrefix%>txtOtherMonthlyIncome').val();
		var otherMonthlyIncomeDesc = $('#<%=IDPrefix%>txtOtherMonthlyIncomeDesc').val();
		var hasOtherMonthlyIncome = $('#<%=IDPrefix%>lnkHasOtherMonthlyIncome').attr('status');
		var strMessage = "";
		if (hasOtherMonthlyIncome == 'Y') {
			if (otherMonthlyIncome != '') {
				if (otherMonthlyIncomeDesc == '') {
					strMessage += "Please complete other monthly income description field.<br/>";
				}
			}     
		}
		return strMessage;
	}--%>


	function <%=IDPrefix%>registerFinancialInfoValidator() {
		//validate monthly income -->always required
		$('#<%=IDPrefix%>txtGrossMonthlyIncome').observer({
			validators: [
				function (partial) {
					if ($("#<%=IDPrefix%>divGrossMonthlyIncome.showfield-section").length > 0 && $("#<%=IDPrefix%>divGrossMonthlyIncome.showfield-section").hasClass("hidden")) return "";//skip validate if Gross Income field is hidden
					//validate monthly income
					var sEmploymentStatus = "";
					var txtGrossMonthlyIncome = $(this).val();
					sEmploymentStatus = $('#<%=IDPrefix%>ddlEmploymentStatus option:selected').val();
						if (!<%=IDPrefix%>employmentStatusRequiresMonthlyIncome(sEmploymentStatus)) {
							if (!Common.ValidateTextNonZero(txtGrossMonthlyIncome)) {
								return 'Gross Monthly Income is required';
							}
						}
					return "";
				}
			],
			validateOnBlur: true,
			group: "<%=IDPrefix%>ValidateFinancialInfo"
		});
		$('#<%=IDPrefix%>txtOtherMonthlyIncomeDesc').observer({
			validators: [
				function (partial) {
					var hasOtherMonthlyIncome = $('#<%=IDPrefix%>lnkHasOtherMonthlyIncome').attr('status');
					var otherMonthlyIncome = $('#<%=IDPrefix%>txtOtherMonthlyIncome').val();
					var desc = $(this).val();
					if (hasOtherMonthlyIncome == 'Y' && Common.ValidateTextNonZero(otherMonthlyIncome) && Common.ValidateText(desc) == false) {
						return "Other Monthly Income Description is required";
					}
					return "";
				}
			],
			validateOnBlur: true,
			group: "<%=IDPrefix%>ValidateFinancialInfo"
		});
		$('#<%=IDPrefix%>ddlEmploymentStatus').observer({
			validators: [
				function (partial) {
					var status = $(this).val();
					if (Common.ValidateText(status) == false) {
						return "Employment Status is required";
					}
					return "";
				}
			],
			validateOnBlur: true,
			group: "<%=IDPrefix%>ValidateFinancialInfo"
		});


		<%If CollectDescriptionIfEmploymentStatusIsOther.ToUpper() = "Y" Then%>
		$('#<%=IDPrefix%>txtEmploymentDescription').observer({
			validators: [
                function (partial) {
                	var employmentDesc = $(this).val();
                	if ($('#<%=IDPrefix%>ddlEmploymentStatus').val().toUpperCase() == "OTHER" && !Common.ValidateText(employmentDesc)) {
                		return 'Other Employment Description is required';
                	}
                	return "";
                }
    		],
    		validateOnBlur: true,
    		group: "<%=IDPrefix%>ValidateFinancialInfo"
    	});
    	<%End If%>

        <% If Not IsBLJoint Then%>           
		        $('#<%=IDPrefix%>txtTotalMonthlyHousingExpense').observer({
			        validators: [
				        function (partial) {
                            if ($('#<%=IDPrefix%>ddlOccupyingStatus').length == 0) return "";
                            var housingExpense = $(this).val();
                            var occupyingStatus = $('#<%=IDPrefix%>ddlOccupyingStatus').val();                         
					        // Mortgage/rent payment field is required when user select Rent or overridden behavior
							const housingStatusForRequiredPayment = <%=JsonConvert.SerializeObject(OccupancyStatusesForRequiredHousingPayment)%>;
                            if (_.indexOf(housingStatusForRequiredPayment, occupyingStatus.toUpperCase()) >= 0 && Common.ValidateTextNonZero(housingExpense) == false) {
                                 //make monthly mortgate/Rent payment is optional field for joint
                                if ('<%=IDPrefix %>' == 'co_') return "";              
						        return "Monthly Mortgage/Rent Payment is required";
					        }
					        return "";
				        }
			        ],
			        validateOnBlur: true,
			        group: "<%=IDPrefix%>ValidateFinancialInfo"
                });      
        <%End If %>
		//validate previous employment if it exist
		$('#<%=IDPrefix%>prev_ddlEmploymentStatus').observer({
			validators: [
				function (partial) {
					var status = $(this).val();
					if (Common.ValidateText(status) == false) {
						return "Employment Status is required";
					}
					return "";
				}
			],
			validateOnBlur: true,
			group: "<%=IDPrefix%>prev_ValidateFinancialInfo"
		});
		$('#<%=IDPrefix%>prev_txtGrossMonthlyIncome').observer({
			validators: [
				function (partial) {
					if ($("#<%=IDPrefix%>divGrossMonthlyIncome.showfield-section").length > 0 && $("#<%=IDPrefix%>divGrossMonthlyIncome.showfield-section").hasClass("hidden")) return "";//skip validate if Gross Income field is hidden 
					//validate monthly income
					var sEmploymentStatus = "";
					var txtGrossMonthlyIncome = $(this).val();
					sEmploymentStatus = $('#<%=IDPrefix%>prev_ddlEmploymentStatus option:selected').val();
						if (!<%=IDPrefix%>employmentStatusRequiresMonthlyIncome(sEmploymentStatus)) {
							if (!Common.ValidateTextNonZero(txtGrossMonthlyIncome)) {
								return 'Gross Monthly Income is required';
							}
						}
					return "";
				}
			],
			validateOnBlur: true,
			group: "<%=IDPrefix%>prev_ValidateFinancialInfo"
		});
	}

	<%=IDPrefix%>registerFinancialInfoValidator();

	function getTaxExempt(chkElement) {
		var taxExempt = "N";
		if (chkElement.prev().hasClass('ui-checkbox-on')) {
			taxExempt="Y";
		}
		return taxExempt;
	}

	function <%=IDPrefix%>SetApplicantFinancialInfo(appInfo) {
		var ddlEmloymentStatus = $("#<%=IDPrefix%>ddlEmploymentStatus option:selected").val();
		appInfo.<%=IDPrefix%>EmploymentStatus = ddlEmloymentStatus;
		appInfo.<%=IDPrefix%>EmploymentDescription = "";
		<%If CollectDescriptionIfEmploymentStatusIsOther.ToUpper() = "Y" Then%>
		if (appInfo.<%=IDPrefix%>EmploymentStatus.toUpperCase() == "OTHER") {
			appInfo.<%=IDPrefix%>EmploymentDescription = $("#<%=IDPrefix%>txtEmploymentDescription").val();
        }
        <%End If%>
      
        <%If String.IsNullOrEmpty(IDPrefix) Then%>     
            EMPLogicPI.setValueToAppObject(appInfo); // primary          
        <%ElseIf IDPrefix = "co_" Then%>
            EMPLogicJN.setValueToAppObject(appInfo); // for joint    
        <%Else %>
            EMPLogic<%=IDPrefix%>.setValueToAppObject(appInfo);
        <%End If %>
		//enlistment date
		var durationYear = $('#<%=IDPrefix%>txtEmployedDuration_year option:selected').val();
		var durationMonth = $('#<%=IDPrefix%>txtEmployedDuration_month option:selected').val();
		var grossMonthlyIncome = $("#<%=IDPrefix%>txtGrossMonthlyIncome").val();
		var totalMonthlyHousingExpense = $("#<%=IDPrefix%>txtTotalMonthlyHousingExpense").val();
		var otherMonthlyIncome = $('#<%=IDPrefix%>txtOtherMonthlyIncome').val();
		var otherMonthlyIncomeDesc = $('#<%=IDPrefix%>txtOtherMonthlyIncomeDesc').val();
		var hasOtherMonthlyIncome = $('#<%=IDPrefix%>lnkHasOtherMonthlyIncome').attr('status');
		if (grossMonthlyIncome == "") {
			grossMonthlyIncome = 0.0;
        }
        if (totalMonthlyHousingExpense == "") {
			totalMonthlyHousingExpense = 0.0;
		}
		if (hasOtherMonthlyIncome != 'Y' || otherMonthlyIncome == "") {
			otherMonthlyIncome = 0;
			otherMonthlyIncomeDesc = "";
		}
		//get enlistment date for active miltary if it selected
		var enlistmentDate = "";
		if (ddlEmloymentStatus == "ACTIVE MILITARY") {
			enlistmentDate = getEnlistmentDate(durationYear, durationMonth);
		}
		appInfo.<%=IDPrefix%>txtEmploymentStartDate = enlistmentDate;
		appInfo.<%=IDPrefix%>GrossMonthlyIncome = Common.GetFloatFromMoney(grossMonthlyIncome);
		appInfo.<%=IDPrefix%>TotalMonthlyHousingExpense = Common.GetFloatFromMoney(totalMonthlyHousingExpense);  
		appInfo.<%=IDPrefix%>OtherMonthlyIncome = Common.GetFloatFromMoney(otherMonthlyIncome);
		appInfo.<%=IDPrefix%>OtherMonthlyIncomeDesc = otherMonthlyIncomeDesc;
		appInfo.<%=IDPrefix%>GrossMonthlyInComeTaxExempt = getTaxExempt($('#<%=IDPrefix%>grossMonthlyIncomeTaxExempt'));
        appInfo.<%=IDPrefix%>OtherMonthlyIncomeTaxExempt = getTaxExempt($('#<%=IDPrefix%>otherMonthlyIncomeTaxExempt'));     
		//set previous employment infor
		var hasPreviousEmployment = $('#<%=IDPrefix%>hdShowPreviousEmployment').val();
		appInfo.<%=IDPrefix%>HasPreviousEmployment = hasPreviousEmployment;
		if (hasPreviousEmployment == 'Y') {
			var prev_grossMonthlyIncome = $("#<%=IDPrefix%>prev_txtGrossMonthlyIncome").val();
			var prev_ddlEmloymentStatus = $("#<%=IDPrefix%>prev_ddlEmploymentStatus option:selected").val();
			var prev_durationYear = $('#<%=IDPrefix%>prev_txtEmployedDuration_year option:selected').val();
			var prev_durationMonth = $('#<%=IDPrefix%>prev_txtEmployedDuration_month option:selected').val();
            <%If String.IsNullOrEmpty(IDPrefix) Then%>
    		    EMPLogicPI_PREV.setValueToAppObject(appInfo); // primary
    		<%ElseIf IDPrefix = "co_" Then%>
    		    EMPLogicJN_PREV.setValueToAppObject(appInfo); // for joint
			<%Else%>
                EMPLogic<%=IDPrefix%>PREV.setValueToAppObject(appInfo);            
			<%End If%>
			var prev_enlistmentDate = "";
			if (prev_ddlEmloymentStatus == "ACTIVE MILITARY") {
				prev_enlistmentDate = getEnlistmentDate(prev_durationYear, prev_durationMonth);
			}
			appInfo.<%=IDPrefix%>prev_grossMonthlyIncome=Common.GetFloatFromMoney(prev_grossMonthlyIncome);
			appInfo.<%=IDPrefix%>prev_txtEmploymentStartDate = prev_enlistmentDate;
			appInfo.<%=IDPrefix%>prev_EmploymentStatus = prev_ddlEmloymentStatus;
        }
	}

	function <%=IDPrefix%>ViewFinancialInfo() {
		var txtGrossMonthlyIncome = htmlEncode($('#<%=IDPrefix%>txtGrossMonthlyIncome').val());
		var txtTotalMonthlyHousingExpense = htmlEncode($('#<%=IDPrefix%>txtTotalMonthlyHousingExpense').val());
		var ddlEmploymentStatus = $('#<%=IDPrefix%>ddlEmploymentStatus option:selected').val();
		var ddlBranchService = $('#<%=IDPrefix%>ddlBranchOfService option:selected').val();
		var ddlPayGrade = $('#<%=IDPrefix%>ddlPayGrade option:selected').val();
		var txtBusinessType = htmlEncode($('#<%=IDPrefix%>txtBusinessType').val());
		var txtEmployer = htmlEncode($('#<%=IDPrefix%>txtEmployer').val());
		var txtEmploymentDescription = htmlEncode($('#<%=IDPrefix%>txtEmploymentDescription').val());
		var ddlLengthOfEmploymentYear = $('#<%=IDPrefix%>txtEmployedDuration_year option:selected').val();
		var ddlLengthOfEmploymentMonth = $('#<%=IDPrefix%>txtEmployedDuration_month option:selected').val();
		var jobTitleElement = $('#<%=IDPrefix%>txtJobTitle');
		var otherMonthlyIncome = $('#<%=IDPrefix%>txtOtherMonthlyIncome').val();
		var otherMonthlyIncomeDesc = htmlEncode($('#<%=IDPrefix%>txtOtherMonthlyIncomeDesc').val());
		var hasOtherMonthlyIncome = $('#<%=IDPrefix%>lnkHasOtherMonthlyIncome').attr('status');
		var txtJobTitle = "";
		if (jobTitleElement.length > 0) {
			txtJobTitle = htmlEncode(jobTitleElement.val());
			if (jobTitleElement.attr('type') != 'text') {
				txtJobTitle = jobTitleElement.find('option:selected').text();
			}
		}
        
		var strEmploymentInfo = "";
		strEmploymentInfo += '<div class="col-sm-6 col-xs-12"><div class="row"><div class="col-xs-6 text-right row-title"><span class="bold">Employment Status</span></div><div class="col-xs-6 text-left row-data"><span>' + ddlEmploymentStatus + '</span></div></div></div>';
		<%If CollectDescriptionIfEmploymentStatusIsOther.ToUpper() = "Y" Then%>
		if (ddlEmploymentStatus == "OTHER" && txtEmploymentDescription != '') {
			strEmploymentInfo += '<div class="col-sm-6 col-xs-12"><div class="row"><div class="col-xs-6 text-right row-title"><span class="bold">Other Employment Description</span></div><div class="col-xs-6 text-left row-data"><span>' + txtEmploymentDescription + '</span></div></div></div>';
		}
		<%End If%>
		if (ddlEmploymentStatus == "RETIRED MILITARY" || ddlEmploymentStatus == "ACTIVE MILITARY") {
			if (txtJobTitle != '' && txtJobTitle != undefined) {
				strEmploymentInfo += '<div class="col-sm-6 col-xs-12"><div class="row"><div class="col-xs-6 text-right row-title"><span class="bold">Profession/MOS</span></div><div class="col-xs-6 text-left row-data"><span>' + txtJobTitle + '</span></div></div></div>';
			}
			if (ddlBranchService != '') {
				strEmploymentInfo += '<div class="col-sm-6 col-xs-12"><div class="row"><div class="col-xs-6 text-right row-title"><span class="bold">Branch of Service</span></div><div class="col-xs-6 text-left row-data"><span>' + ddlBranchService + '</span></div></div></div>';
			}
			if (ddlPayGrade != '') {
				strEmploymentInfo += '<div class="col-sm-6 col-xs-12"><div class="row"><div class="col-xs-6 text-right row-title"><span class="bold">Pay Grade</span></div><div class="col-xs-6 text-left row-data"><span>' + ddlPayGrade + '</span></div></div></div>';
			}
		}
		else {
			if (txtJobTitle != '' && txtJobTitle != undefined) {
				strEmploymentInfo += '<div class="col-sm-6 col-xs-12"><div class="row"><div class="col-xs-6 text-right row-title"><span class="bold">Profession/Job Title</span></div><div class="col-xs-6 text-left row-data"><span>' + txtJobTitle + '</span></div></div></div>';
			}
			if (txtEmployer != '' && txtEmployer != undefined) {
				strEmploymentInfo += '<div class="col-sm-6 col-xs-12"><div class="row"><div class="col-xs-6 text-right row-title"><span class="bold">Employer</span></div><div class="col-xs-6 text-left row-data"><span>' + txtEmployer + '</span></div></div></div>';
			}
		}
		if (ddlEmploymentStatus == "OWNER" || ddlEmploymentStatus == "SELF EMPLOYED") {
			if (txtBusinessType != '') {
				strEmploymentInfo += '<div class="col-sm-6 col-xs-12"><div class="row"><div class="col-xs-6 text-right row-title"><span class="bold">Business Type</span></div><div class="col-xs-6 text-left row-data"><span>' + txtBusinessType + '</span></div></div></div>';
			}
		}
		if (ddlEmploymentStatus == "GOVERNMENT/DOD") {
			if (ddlPayGrade != '') {
				strEmploymentInfo += '<div class="col-sm-6 col-xs-12"><div class="row"><div class="col-xs-6 text-right row-title"><span class="bold">Pay Grade</span></div><div class="col-xs-6 text-left row-data"><span>' + ddlPayGrade + '</span></div></div></div>';
			}
		}
		if (Number(ddlLengthOfEmploymentYear) > 0 || Number(ddlLengthOfEmploymentMonth) > 0) {
			var strEmpStatus = "";
			//if (ddlEmploymentStatus == "RETIRED MILITARY" || ddlEmploymentStatus == "ACTIVE MILITARY") {
			if (ddlEmploymentStatus == "ACTIVE MILITARY") {//only ACTIVE MILITARY has Time In Service <-- based on employment.logic.js
				strEmpStatus = "Time In Service";
			} else {
				strEmpStatus = "Employment Duration";
			}
			var strEmpDuration = "";
			if (Number(ddlLengthOfEmploymentYear) > 0) {
				strEmpDuration += ddlLengthOfEmploymentYear + " yrs";
			}
			if (Number(ddlLengthOfEmploymentMonth) > 0) {
				strEmpDuration += " " + ddlLengthOfEmploymentMonth + " mos";
			}
			strEmploymentInfo += '<div class="col-sm-6 col-xs-12"><div class="row"><div class="col-xs-6 text-right row-title"><span class="bold">' + strEmpStatus + '</span></div><div class="col-xs-6 text-left row-data"><span>' + strEmpDuration + '</span></div></div></div>';
			var strPayGrades = "E1,E2,E3,E4,E5,E6,E7,E8,E9";
			if (strPayGrades.indexOf(ddlPayGrade) != -1 && ddlEmploymentStatus == "ACTIVE MILITARY") {//only ACTIVE MILITARY has Time In Service <-- based on employment.logic.js
				strEmploymentInfo += '<div class="col-sm-6 col-xs-12"><div class="row"><div class="col-xs-6 text-right row-title"><span class="bold">ETS/Expiration Date</span></div><div class="col-xs-6 text-left row-data"><span>' + $('#<%=IDPrefix%>txtETS').val() + '</span></div></div></div>';
			}
		}
        
		if (Common.ValidateText(txtGrossMonthlyIncome)) {
			txtGrossMonthlyIncome = Common.FormatCurrency(txtGrossMonthlyIncome, true);
			strEmploymentInfo += '<div class="col-sm-6 col-xs-12"><div class="row"><div class="col-xs-6 text-right row-title"><span class="bold">Gross Monthly Income (before taxes)</span></div><div class="col-xs-6 text-left row-data"><span>' + txtGrossMonthlyIncome + '</span></div></div></div>';
			//tax exempt 
			var grossMonthlyIncomeTaxExempt= getTaxExempt($('#<%=IDPrefix%>grossMonthlyIncomeTaxExempt'));
			if (grossMonthlyIncomeTaxExempt == 'Y') {
				strEmploymentInfo += '<div class="col-sm-6 col-xs-12"><div class="row"><div class="col-xs-6 text-right row-title"><span class="bold">Gross Monthly Income Tax Exempt</span></div><div class="col-xs-6 text-left row-data"><span>Yes</span></div></div></div>';
			}
		}
		//other monthly income
		if (hasOtherMonthlyIncome == 'Y') {
			if (Common.ValidateText(otherMonthlyIncome)) {
				otherMonthlyIncome = Common.FormatCurrency(otherMonthlyIncome, true);
				strEmploymentInfo += '<div class="col-sm-6 col-xs-12"><div class="row"><div class="col-xs-6 text-right row-title"><span class="bold">Other Monthly Income</span></div><div class="col-xs-6 text-left row-data"><span>' + otherMonthlyIncome + '</span></div></div></div>';
				//tax exempt 
				var otherMonthlyIncomeTaxExempt = getTaxExempt($('#<%=IDPrefix%>otherMonthlyIncomeTaxExempt'));
				if (otherMonthlyIncomeTaxExempt == 'Y') {
					strEmploymentInfo += '<div class="col-sm-6 col-xs-12"><div class="row"><div class="col-xs-6 text-right row-title"><span class="bold">Other Monthly Income Tax Exempt</span></div><div class="col-xs-6 text-left row-data"><span>Yes</span></div></div></div>';
				}
			}
			if (Common.ValidateText(otherMonthlyIncomeDesc)) {
				strEmploymentInfo += '<div class="col-sm-6 col-xs-12"><div class="row"><div class="col-xs-6 text-right row-title"><span class="bold">Other Monthly Income Description</span></div><div class="col-xs-6 text-left row-data"><span>' + otherMonthlyIncomeDesc + '</span></div></div></div>';
			}
        }
       
		if (Common.ValidateText(txtTotalMonthlyHousingExpense)) {
			txtTotalMonthlyHousingExpense = Common.FormatCurrency(txtTotalMonthlyHousingExpense, true);
			strEmploymentInfo += '<div class="col-sm-6 col-xs-12"><div class="row"><div class="col-xs-6 text-right row-title"><span class="bold">Monthly Mortgage/Rent Payment</span></div><div class="col-xs-6 text-left row-data"><span>' + txtTotalMonthlyHousingExpense + '</span></div></div></div>';
        }
       
		$(this).hide();
		$(this).prev("div.row").hide();
		if (strEmploymentInfo !== "") {
			$(this).show();
			$(this).prev("div.row").show();
		}
		return strEmploymentInfo;
	}

	function <%=IDPrefix%>ViewPrevEmploymentInfo() {
		if ($('#<%=IDPrefix%>hdShowPreviousEmployment').val() != "Y") {
			$(this).hide();
			$(this).prev("div.row").hide();
			return "";
		}
		var txtGrossMonthlyIncome = htmlEncode($('#<%=IDPrefix%>prev_txtGrossMonthlyIncome').val());
		var ddlEmploymentStatus = $('#<%=IDPrefix%>prev_ddlEmploymentStatus option:selected').val();
		var ddlBranchService = $('#<%=IDPrefix%>prev_ddlBranchOfService option:selected').val();
		var ddlPayGrade = $('#<%=IDPrefix%>prev_ddlPayGrade option:selected').val();
		var txtBusinessType = htmlEncode($('#<%=IDPrefix%>prev_txtBusinessType').val());
		var txtEmployer = htmlEncode($('#<%=IDPrefix%>prev_txtEmployer').val());
		var ddlLengthOfEmploymentYear = $('#<%=IDPrefix%>prev_txtEmployedDuration_year option:selected').val();
		var ddlLengthOfEmploymentMonth = $('#<%=IDPrefix%>prev_txtEmployedDuration_month option:selected').val();
		var jobTitleElement = $('#<%=IDPrefix%>prev_txtJobTitle');
		var txtJobTitle = "";
		if (jobTitleElement.length > 0) {
			txtJobTitle = htmlEncode(jobTitleElement.val());
			if (jobTitleElement.attr('type') != 'text') {
				txtJobTitle = jobTitleElement.find('option:selected').text();
			}
		}
		var strPrevEmploymentInfo = "";
		strPrevEmploymentInfo += '<div class="col-sm-6 col-xs-12"><div class="row"><div class="col-xs-6 text-right row-title"><span class="bold">Employment Status</span></div><div class="col-xs-6 text-left row-data"><span>' + ddlEmploymentStatus + '</span></div></div></div>';
		if (ddlEmploymentStatus == "RETIRED MILITARY" || ddlEmploymentStatus == "ACTIVE MILITARY") {
			if (txtJobTitle != '' && txtJobTitle != undefined) {
				strPrevEmploymentInfo += '<div class="col-sm-6 col-xs-12"><div class="row"><div class="col-xs-6 text-right row-title"><span class="bold">Profession/MOS</span></div><div class="col-xs-6 text-left row-data"><span>' + txtJobTitle + '</span></div></div></div>';
			}
			if (ddlBranchService != '') {
				strPrevEmploymentInfo += '<div class="col-sm-6 col-xs-12"><div class="row"><div class="col-xs-6 text-right row-title"><span class="bold">Branch of Service</span></div><div class="col-xs-6 text-left row-data"><span>' + ddlBranchService + '</span></div></div></div>';
			}
			if (ddlPayGrade != '') {
				strPrevEmploymentInfo += '<div class="col-sm-6 col-xs-12"><div class="row"><div class="col-xs-6 text-right row-title"><span class="bold">Pay Grade</span></div><div class="col-xs-6 text-left row-data"><span>' + ddlPayGrade + '</span></div></div></div>';
			}
		}
		else {
			if (txtJobTitle != '' && txtJobTitle != undefined) {
				strPrevEmploymentInfo += '<div class="col-sm-6 col-xs-12"><div class="row"><div class="col-xs-6 text-right row-title"><span class="bold">Profession/Job Title</span></div><div class="col-xs-6 text-left row-data"><span>' + txtJobTitle + '</span></div></div></div>';
			}
			if (txtEmployer != '' && txtEmployer != undefined) {
				strPrevEmploymentInfo += '<div class="col-sm-6 col-xs-12"><div class="row"><div class="col-xs-6 text-right row-title"><span class="bold">Employer</span></div><div class="col-xs-6 text-left row-data"><span>' + txtEmployer + '</span></div></div></div>';
			}
		}
		if (ddlEmploymentStatus == "OWNER" || ddlEmploymentStatus == "SELF EMPLOYED") {
			if (txtBusinessType != '') {
				strPrevEmploymentInfo += '<div class="col-sm-6 col-xs-12"><div class="row"><div class="col-xs-6 text-right row-title"><span class="bold">Business Type</span></div><div class="col-xs-6 text-left row-data"><span>' + txtBusinessType + '</span></div></div></div>';
			}
		}
		if (ddlEmploymentStatus == "GOVERNMENT/DOD") {
			if (ddlPayGrade != '') {
				strPrevEmploymentInfo += '<div class="col-sm-6 col-xs-12"><div class="row"><div class="col-xs-6 text-right row-title"><span class="bold">Pay Grade</span></div><div class="col-xs-6 text-left row-data"><span>' + ddlPayGrade + '</span></div></div></div>';
			}
		}
		if (Number(ddlLengthOfEmploymentYear) > 0 || Number(ddlLengthOfEmploymentMonth) > 0) {
			var strEmpStatus = "";
			//if (ddlEmploymentStatus == "RETIRED MILITARY" || ddlEmploymentStatus == "ACTIVE MILITARY") {
			if (ddlEmploymentStatus == "ACTIVE MILITARY") {//only ACTIVE MILITARY has Time In Service <-- based on employment.logic.js
				strEmpStatus = "Time In Service";
			} else {
				strEmpStatus = "Employment Duration";
			}
			var strEmpDuration = "";
			if (Number(ddlLengthOfEmploymentYear) > 0) {
				strEmpDuration += ddlLengthOfEmploymentYear + " yrs";
			}
			if (Number(ddlLengthOfEmploymentMonth) > 0) {
				strEmpDuration += " " + ddlLengthOfEmploymentMonth + " mos";
			}
			strPrevEmploymentInfo += '<div class="col-sm-6 col-xs-12"><div class="row"><div class="col-xs-6 text-right row-title"><span class="bold">' + strEmpStatus + '</span></div><div class="col-xs-6 text-left row-data"><span>' + strEmpDuration + '</span></div></div></div>';
			var strPayGrades = "E1,E2,E3,E4,E5,E6,E7,E8,E9";
			if (strPayGrades.indexOf(ddlPayGrade) != -1 && ddlEmploymentStatus == "ACTIVE MILITARY") {//only ACTIVE MILITARY has Time In Service <-- based on employment.logic.js
				strPrevEmploymentInfo += '<div class="col-sm-6 col-xs-12"><div class="row"><div class="col-xs-6 text-right row-title"><span class="bold">ETS/Expiration Date</span></div><div class="col-xs-6 text-left row-data"><span>' + $('#<%=IDPrefix%>prev_txtETS').val() + '</span></div></div></div>';
			}
		}

		if (Common.ValidateText(txtGrossMonthlyIncome)) {
			txtGrossMonthlyIncome = Common.FormatCurrency(txtGrossMonthlyIncome, true);
			strPrevEmploymentInfo += '<div class="col-sm-6 col-xs-12"><div class="row"><div class="col-xs-6 text-right row-title"><span class="bold">Gross Monthly Income (before taxes)</span></div><div class="col-xs-6 text-left row-data"><span>' + txtGrossMonthlyIncome + '</span></div></div></div>';
		}
		$(this).hide();
		$(this).prev("div.row").hide();
		if (strPrevEmploymentInfo !== "") {
			$(this).show();
			$(this).prev("div.row").show();
		}
		return strPrevEmploymentInfo;
	}

	function  <%=IDPrefix%>autoFillData_FinancialInFor()
	{   
        $('#<%=IDPrefix%>ddlEmploymentStatus').val("HOMEMAKER");    
		$("#<%=IDPrefix%>txtJobTitle").val("housekeeper");
        $('#<%=IDPrefix%>txtGrossMonthlyIncome').val("8000");
        $('#<%=IDPrefix%>txtTotalMonthlyHousingExpense').val("1500");
        
	}
	function <%=IDPrefix%>ShowPreviousEmploymentInfo() {
		var oDurationTotalInMonth = 0;
		var oDurationMonth = $('#<%=IDPrefix%>txtEmployedDuration_month option:selected').val();
		var oDurationYear = $('#<%=IDPrefix%>txtEmployedDuration_year option:selected').val();
        var selectedMonth = false, selectedYear = false;
		if (oDurationMonth == undefined || (oDurationMonth != undefined && oDurationMonth.trim() == "")) {
			oDurationMonth = 0;
		} else {
			oDurationMonth = parseInt(oDurationMonth);
			selectedMonth = true;
		}
		//duration year
		if (oDurationYear == undefined || (oDurationYear != undefined && oDurationYear.trim() == "")) {
			oDurationYear = 0;
		} else {
			oDurationYear = parseInt(oDurationYear) * 12;//convert year to month
			selectedYear = true;
		}
		//calculate total duration in months
		oDurationTotalInMonth = oDurationMonth + oDurationYear;
		$('#<%=IDPrefix%>hdShowPreviousEmployment').val("N");
		$('#<%=IDPrefix%>divPreviousEmploymentInfo').hide();
		if (selectedYear || selectedMonth) {
			var previous_employment_threshold = $('#hdPrevEmploymentThreshold').val(); 
			if (previous_employment_threshold != "") { 
				if (!isNaN(previous_employment_threshold)) {
					if (oDurationTotalInMonth < parseInt(previous_employment_threshold)) {
						$('#<%=IDPrefix%>divPreviousEmploymentInfo').show();
						$('#<%=IDPrefix%>hdShowPreviousEmployment').val("Y");
					} 
				}
			}
		} //end selectedYear ...
		<%--//Temp for testing
        $('#<%=IDPrefix%>divPreviousEmploymentInfo').show();
        $('#<%=IDPrefix%>hdShowPreviousEmployment').val("Y");--%>
	}//end showPreviousEmploymentInfo
    
	$(function () {

		var employmentStatus = $('#<%=IDPrefix%>ddlEmploymentStatus option:selected').val();
        if (employmentStatus != null || employmentStatus != "") {
            <%=IDPrefix%>updateIncomeTitle();
        }
    	var prevEmploymentStatus = $('#<%=IDPrefix%>prev_ddlEmploymentStatus option:selected').val();
    	if (prevEmploymentStatus != null || prevEmploymentStatus != "") {
    		<%=IDPrefix%>prev_updateIncomeTitle();
        }
	   
		$('#<%=IDPrefix%>divEmploymentStatusRest').on('change', 'select[id*="txtEmployedDuration"]', function () {
			<%=IDPrefix%>ShowPreviousEmploymentInfo();     
		}); 
		$('#<%=IDPrefix%>lnkHasOtherMonthlyIncome').on('click', function (e) {
			var $self = $(this);
			var selectedValue = $self.attr('status');
			if (selectedValue == "N") {
				$self.html("Remove other income");
				$self.addClass("minus-circle-before").removeClass("plus-circle-before");
				$self.attr('status', 'Y');
				$('#<%=IDPrefix%>divOtherMonthlyIncomeForm').show();
			} else {
				$self.html("Add other income");
				$self.attr('status', 'N');
				$self.addClass("plus-circle-before").removeClass("minus-circle-before");
				$('#<%=IDPrefix%>divOtherMonthlyIncomeForm').hide();
			}
			if (BUTTONLABELLIST != null) {
				var value = BUTTONLABELLIST[$.trim($self.html()).toLowerCase()];
				if (typeof value == "string" && $.trim(value) !== "") {
					$self.html(value);
				}
			}
			e.preventDefault();
		});
		$("#<%=IDPrefix%>txtGrossMonthlyIncome, #<%=IDPrefix%>prev_txtGrossMonthlyIncome").on("blur", function() {
			var $self = $(this);
			if (Common.GetFloatFromMoney($self.val()) > 20000) {
				$("#<%=IDPrefix%>popGrossMonthlyIncomWarning [data-place-holder='content']").html("Your estimated gross annual salary is " + Common.FormatCurrency(Common.GetFloatFromMoney($self.val()) * 12, true));
				$("#<%=IDPrefix%>popGrossMonthlyIncomWarning").popup("open", { "positionTo": "window" });
			}
		});
		$("#<%=IDPrefix%>popGrossMonthlyIncomWarning").on("popupafteropen", function (event, ui) {
			//prevent scrollbar by popup overlay
			$("#" + this.id + "-screen").height("");
		});
	});

</script>

