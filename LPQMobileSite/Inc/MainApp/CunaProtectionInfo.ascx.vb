﻿Imports LPQMobile.BO
Imports LPQMobile.Utils
Partial Class Inc_MainApp_CunaProtectionInfo
    Inherits CBaseUserControl
    Protected _cunaProtectionAll As String = ""
    Protected _cunaCreditLife As String = ""
    Protected _cunaDebt As String = ""
    Protected _cunaGAP As String = ""
    Protected _cunaMRC As String = ""
    Protected _helocPrefix As String = ""
    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        '' parent page should be CBasePage
        Dim parent As CBasePage = TryCast(Me.Page, CBasePage)
        Dim oConfig = parent._CurrentWebsiteConfig
        If oConfig IsNot Nothing Then
            If LoanType = "HELOC" Then
                _helocPrefix = "heloc_"
            End If
			Dim currentNode As String = Common.GetConfigLoanTypeFromShort(LoanType) & "/VISIBLE"
            _cunaProtectionAll = Common.getVisibleAttribute(oConfig, "cuna_protection_all")
            _cunaCreditLife = Common.getNodeAttributes(oConfig, currentNode, "cuna_credit_life")
            _cunaDebt = Common.getNodeAttributes(oConfig, currentNode, "cuna_debt")
            If LoanType = "VL" Then
                _cunaGAP = Common.getNodeAttributes(oConfig, currentNode, "cuna_gap")
                _cunaMRC = Common.getNodeAttributes(oConfig, currentNode, "cuna_mrc")
            End If        
        End If

    End Sub
End Class
