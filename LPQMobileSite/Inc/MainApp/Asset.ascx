﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="Asset.ascx.vb" Inherits="Inc_MainApp_Asset" %>
<div id="<%=IDPrefix%>divAssetSection" <%=IIf(IsInMode("777") AndAlso IsInFeature("visibility"), "class=""showfield-section"" data-show-field-section-id=""" & BuildShowFieldSectionID("divAssetSection") & """ data-default-state=""off"" data-section-name= '" & IIf(String.IsNullOrEmpty(IDPrefix), "", "Co-App ") & "Asset'", "")%>>
<%If EnableAssetSection Or (IsInMode("777") Andalso IsInFeature("rename")) Then %>
    <div class="rename-able">
        <%=HeaderUtils.RenderPageTitle(0, IIf(String.IsNullOrEmpty(IDPrefix), "", "Co-App ") & "Assets", True)%>
    </div>
    <div data-role="fieldcontain">
        <p class="rename-able">Specify up to 10 assets that you would like to be used as collateral</p>
        <div id="<%=IDPrefix%>assetTemplate" class="hidden">
            <label name="asset_index" class="bold"></label>
            <div data-role="fieldcontain">
                <label class="rename-able RequiredIcon">Asset Type</label>
                <select name="asset_type" id="asset_type_t" onchange="<%=IDPrefix%>assetTypeChange(event)"><%=AssetTypesString%></select>
            </div>
            <div data-role="fieldcontain">
                <label class="rename-able RequiredIcon" name="asset_value_label">Asset Value</label>
                <input type="text" pattern="[0-9]*" id="asset_value_t" class="money" maxlength="10" value="" name="asset_value"/>
            </div>
            <div data-role="fieldcontain" name="rate_section">
                <label class="rename-able RequiredIcon" name="rate_label">Rate</label>
                <input type="tel" class="numeric" pattern="[0-9]*" id="rate_t" value="" name="rate" maxlength="10" oninput="this.value=this.value.slice(0,this.maxLength)"/>
            </div>
            <div data-role="fieldcontain">
                <label class="rename-able" name="description_label">Description</label>             
                <input type="text" value="" name="description" maxlength="300" />
            </div>
            <a class="rename-able header_theme2 shadow-btn minus-circle-before bold" href="javascript:void(0);" name="remove_asset" onclick="<%=IDPrefix%>removeAsset(event)">Remove asset</a>
        </div>
        <div id="<%=IDPrefix%>assetContainter" class="assetContainer"></div>
        <a id="<%=IDPrefix%>btnAddAsset"  href="javascript:void(0);" onclick="<%=IDPrefix%>addAsset()" class="rename-able header_theme2 shadow-btn plus-circle-before bold">Add an asset</a>
    </div>
<%End If%>
</div>
<script type="text/javascript">
    _COMMON_MAX_ASSETS = 10;
    var <%=IDPrefix%>assetList = [];
    <%=IDPrefix%>registerAssetValidator($('#<%=IDPrefix%>assetTemplate'));
    function isEnableAssetSection() {
        return ("<%=EnableAssetSection%>" == "True");
    }
    function isVerbiageMode(){
        return ("<%=IsInMode("777") AndAlso IsInFeature("rename")%>" == "True");
    }
    function hasRate(assetType) {
        //LPQ CLF import doesn't support rate for IRA & MONEY_MARKET
        //return (assetType == "CERTIFICATE" || assetType == "IRA" || assetType == "MONEY_MARKET" || assetType == "SHARE_BACK");
        return (assetType == "CERTIFICATE" || assetType == "SHARE_BACK");
    }
    function <%=IDPrefix%>addAsset(){
        $('#<%=IDPrefix%>assetTemplate').toggleClass("hidden", !isVerbiageMode());
        if(<%=IDPrefix%>assetList.length >= _COMMON_MAX_ASSETS || isVerbiageMode()) return;
        <%=IDPrefix%>assetList.push({
            asset_type:"",
            asset_value:"",
            rate:"",
            description:"",
        });
        <%=IDPrefix%>updateAssetContainter();
    }
    function <%=IDPrefix%>removeAsset(event){
        let index = $(event.target).attr("index");
        $('#<%=IDPrefix%>assetTemplate').toggleClass("hidden", true);
        if(isVerbiageMode() || index == null) return;
        <%=IDPrefix%>assetList.splice(index, 1);
        <%=IDPrefix%>updateAssetContainter();    
    }
    function <%=IDPrefix%>updateAssetContainter(){
        var assetContainter = $("#<%=IDPrefix%>assetContainter");
        assetContainter.empty();
        for(var i = 0; i < <%=IDPrefix%>assetList.length; i++){
            var asset = <%=IDPrefix%>assetItemTemplate(<%=IDPrefix%>assetList[i], i);
            assetContainter.append(asset);
            <%=IDPrefix%>registerAssetValidator(asset);
        }
        if (<%=IDPrefix%>assetList.length >= 1) {
            $("#<%=IDPrefix%>btnAddAsset").text("Add an additional asset");
        } else { 
           $("#<%=IDPrefix%>btnAddAsset").text("Add an asset");
        }
    }
    function <%=IDPrefix%>assetTypeChange(event){
        if(isVerbiageMode()){
            $('#<%=IDPrefix%>assetTemplate div[name=rate_section]').toggle(hasRate(event.target.value));
        }
    }
    function <%=IDPrefix%>assetItemTemplate(asset, index){
        var item = $('<div />').append($('#<%=IDPrefix%>assetTemplate').children().clone(true) );
        $('label[name="asset_index"]', item).text("Asset #" + (index + 1));
        $('select[name="asset_type"]', item).change(function (event) {
                    $(event.target).siblings('span.rename-able').text($("option:selected",event.target).text());
                    <%=IDPrefix%>onAssetChange(event.target.value, "asset_type", index);
                    $('div[name=rate_section]', item).toggle(hasRate(event.target.value));
                });
        $('select[name="asset_type"]', item).prop("id","asset_type_" + index).val(asset.asset_type).change();
        $('input[name="rate"]',item).prop("id","rate_" + index).val(asset.rate)
                .change(function(event){
                    <%=IDPrefix%>onAssetChange(event.target.value,"rate", index);
                });
        $('input[name="asset_value"]',item).prop("id","asset_value_" + index).val(asset.asset_value)
                .change(function(event){
                    <%=IDPrefix%>onAssetChange(event.target.value,"asset_value", index);
                })
                .blur(function() {
                    $(this).val(Common.FormatCurrency($(this).val(), true));
                }).click(function () {
                    if (isMobile.any()) {
                        this.focus();
                        this.setSelectionRange(0, 9999);
                    } else {
                        $(this).select();
                    }
                });
        $('input[name="description"]',item).prop("id","description_" + index).val(asset.description)
                .change(function(event){
                    <%=IDPrefix%>onAssetChange(event.target.value,"description", index);
                });
        $('a[name="remove_asset"]',item).attr("index", index);
        return item;
    }
    function <%=IDPrefix%>onAssetChange(value, type, index){
        for(var i = 0; i < <%=IDPrefix%>assetList.length; i++){
            if(i == index){
                <%=IDPrefix%>assetList[index][type] = value;
                return;
            }
        }
    }
    function <%=IDPrefix%>registerAssetValidator(container){
        $('select[name="asset_type"]', container).observer({
            validators: [
                function (partial) {
                    if(!$(this).is(":visible")) return "";
                    var text = $(this).val();
                    if (!Common.ValidateText(text)) {
                        return 'Asset Type is required';
                    }
                    return "";
                }
            ],
            validateOnBlur: true,
            group: "<%=IDPrefix%>Assets"
        });
        $('input[name="asset_value"]', container).observer({
            validators: [
                function (partial) {
                    if(!$(this).is(":visible")) return "";
                    var text = $(this).val();
                    if (!Common.ValidateText(text)) {
                        return 'Asset Value is required';
                    }
                    return "";
                }
            ],
            validateOnBlur: true,
            group: "<%=IDPrefix%>Assets"
        });
        $('input[name="rate"]', container).observer({
            validators: [
                function (partial) {
                    if(!$(this).is(":visible")) return "";
                    var text = $(this).val();
                    if (!Common.ValidateText(text)) {
                        return 'Rate is required';
                    }
                    return "";
                }
            ],
            validateOnBlur: true,
            group: "<%=IDPrefix%>Assets"
        });
    }
    function <%=IDPrefix%>ValidateAssets(){
        return $.lpqValidate("<%=IDPrefix%>Assets");
    }
    function <%=IDPrefix%>SetAssets(appInfo){
        appInfo.<%=IDPrefix%>Assets = isEnableAssetSection() ? JSON.stringify(<%=IDPrefix%>assetList) : "";
    }
</script>

