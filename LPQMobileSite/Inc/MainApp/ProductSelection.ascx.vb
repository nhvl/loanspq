﻿Imports LPQMobile.Utils
Imports System.Globalization
Imports Newtonsoft.Json
Imports System.Xml

Partial Class Inc_MainApp_ProductSelection
	Inherits CBaseUserControl

	Private _currentLenderRef As String
	Private _currentProductList As List(Of CProduct)
	Private _productQuestionsHtml As String
	Private _zipCodePools As Dictionary(Of String, List(Of String))

	Protected ProductTypeDictionary As Dictionary(Of String, List(Of ProductInfo))
	Protected NormalProductList As List(Of ProductInfo)
	Protected NormalProductListStr As String
	Protected OrderedAccountTypes As List(Of String)
	Protected InstitutionType As CEnum.InstitutionType
	Protected RatePrecision As Integer
	Protected APYPrecision As Integer
	Protected ZipCodePools As Dictionary(Of String, List(Of String))
	Protected _NotFundedProductCodes As New List(Of String)
	Protected AccountNumberLimitsStr As String
	Protected InstanceCountLimitsStr As String
	Public Property ZipCodePoolName As Dictionary(Of String, String)
	Protected RequiredProductList As List(Of ProductInfo)
	Property CurrentLenderRef() As String
		Get
			Return _currentLenderRef
		End Get
		Set(value As String)
			_currentLenderRef = value
		End Set
	End Property


	Property CurrentProductList() As List(Of CProduct)
		Get
			Return _currentProductList
		End Get
		Set(value As List(Of CProduct))
			_currentProductList = value
		End Set
	End Property

	Property ZipCodePoolList() As Dictionary(Of String, List(Of String))
		Get
			Return _zipCodePools
		End Get
		Set(value As Dictionary(Of String, List(Of String)))
			_zipCodePools = value
		End Set
	End Property

	Property ProductQuestionsHtml() As String
		Set(value As String)
			_productQuestionsHtml = value
		End Set
		Get
			Return _productQuestionsHtml
		End Get
	End Property

	Private _config As CWebsiteConfig
	Public Property WebsiteConfig As CWebsiteConfig
		Get
			Return _config
		End Get
		Set(value As CWebsiteConfig)
			_config = value
		End Set
	End Property

#Region "Methods"
	Private Function populateProducts(ByVal listProducts As List(Of CProduct)) As List(Of ProductInfo)
		Dim productList As New List(Of ProductInfo)
		Dim tempAccountInfo As ProductInfo
		For Each prod As CProduct In listProducts
			tempAccountInfo = New ProductInfo
			tempAccountInfo.ZipCodeFilterType = prod.ZipCodeFilterType
			tempAccountInfo.ZipCodePoolId = prod.ZipCodePoolId
			tempAccountInfo.ProductName = prod.AccountName
			'TODO: if tier rate is used then get the min from all rates
			tempAccountInfo.ProductMinDeposit = prod.MinimumDeposit.ToString("C2", New CultureInfo("en-US"))
			If Convert.ToDouble(prod.MaxDeposit) > 0 Then
				tempAccountInfo.ProductMaxDeposit = prod.MaxDeposit.ToString("C2", New CultureInfo("en-US"))
			End If
			'TODO: if tier rate is used then get the max APY from all rates
			tempAccountInfo.ProductAPY = prod.Apy
			tempAccountInfo.ProductIsRequired = If(prod.IsRequired, "Y", "N")
			tempAccountInfo.ProductIsPreselected = If(prod.IsPreSelection, "Y", "N")
			tempAccountInfo.ProductCode = prod.ProductCode
			tempAccountInfo.AccountType = prod.AccountType
			tempAccountInfo.OriginalAccountType = prod.AccountType
			If prod.strPreSelectedValue.ToUpper() = "C" Then
				tempAccountInfo.AccountType = "RECOMMENDED"
			End If


			tempAccountInfo.IsAutoCalculateTier = prod.IsAutoCalculateTier
			tempAccountInfo.IsApyAutoCalculated = prod.IsApyAutoCalculated
			tempAccountInfo.TermType = prod.TermType
			tempAccountInfo.Position = prod.Position
			tempAccountInfo.ProductDescription = prod.ProductDescription
			''sorted product services
			If prod.Services.Count > 0 Then
				prod.Services = SortedProductServices(prod.Services)
			End If
			'' set product services from outer object CProduct
			tempAccountInfo.ProductServices = prod.Services
			tempAccountInfo.MinorAccountCode = prod.MinorAccountCode
			tempAccountInfo.CustomQuestions = prod.CustomQuestions
			tempAccountInfo.PreSelectedValue = prod.strPreSelectedValue
			tempAccountInfo.EntityType = prod.EntityType
			tempAccountInfo.ProductRates = prod.AvailableRates
			tempAccountInfo.NoFund = prod.cannotBeFunded.Equals("Y")
			productList.Add(tempAccountInfo)
			If prod.cannotBeFunded = "Y" Then
				_NotFundedProductCodes.Add(prod.ProductCode)
			End If

		Next
		Return productList
	End Function

	''' <summary>
	''' Transform a list of products with their Position attributes into an abbreviated list
	''' of the Account Type of each prodct. This reflects the order of the products from in-branch
	''' in the control properly.
	''' </summary>
	''' <param name="listProducts"></param>
	''' <returns></returns>
	Private Function CalculateAccountTypeOrder(ByVal listProducts As List(Of ProductInfo)) As List(Of String)
		Dim lstOrderedAccountTypes = New List(Of String)
		If listProducts Is Nothing OrElse listProducts.Count = 0 Then Return lstOrderedAccountTypes
		' For consistency, if there are conflicting Position values, the LINQ OrderBy method is a stable sort, so that
		' any conflicts will keep the original order that came in to this function.
		Dim accountTypesOriginal = listProducts.OrderBy(Function(p) p.Position).Select(Function(p) p.AccountType.ToUpper())
		Dim hshAccountTypes = New HashSet(Of String)
		'check this to make sure the RECOMMENDED category is not empty. When there are no products belong to it, do not render it in UI
		If accountTypesOriginal.Contains("RECOMMENDED") Then
			lstOrderedAccountTypes.Add("RECOMMENDED") ' Recommended always appears first.
			hshAccountTypes.Add("RECOMMENDED")
		End If
		For Each acctType In accountTypesOriginal
			If Not hshAccountTypes.Contains(acctType) Then
				hshAccountTypes.Add(acctType)
				lstOrderedAccountTypes.Add(acctType)
			End If
		Next
		Return lstOrderedAccountTypes
	End Function

	Protected Function SafeHTML(obj As Object) As String
		Dim str As String = If(obj IsNot Nothing, obj.ToString(), "")
		Return str.Replace("'", "&apos;").Replace("""", "&quot;")
	End Function
	Protected Function getProductServiceName(ByVal sDescription As String, ByVal sDescriptionURL As String) As String
		If Not String.IsNullOrEmpty(sDescriptionURL) Then
			sDescription = "<a href=" & SafeHTML(sDescriptionURL) & ">" & SafeHTML(sDescription) & "</a>"
		End If
		Return sDescription
	End Function
#End Region


	Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
		If Not Page.IsPostBack Then
			InitProductSelection()
			ProductTypeDictionary = New Dictionary(Of String, System.Collections.Generic.List(Of ProductInfo)) 'SAVING, CHECKING...

			''check _currProductList is null
			If _currentProductList Is Nothing Then
				_currentProductList = New List(Of CProduct)
			End If

			'for testing only
			'_currentProductList = _currentProductList.Where(Function(p) p.IsPreSelection = False).ToList()


			'' IMPORTANT! should not render OTHER product here, we will render it in a dedicated screen
			NormalProductList = populateProducts(_currentProductList) '.Where(Function(x) x.AccountType <> "OTHER").ToList()
			'FOR TESTING ONLY
			'NormalProductList(10).ProductDescription = NormalProductList(10).ProductDescription + "<span id='spImg'>text to hide</span><label for='chkDetail'>More detail</label><input id='chkDetail' type='checkbox' onchange='aaa(""spImg"");'/><script>function aaa(a){var el = document.getElementById(a); if(el.style.visibility == 'visible') {el.style.visibility='hidden';} else{el.style.visibility='visible';} }</script>"
			NormalProductListStr = JsonConvert.SerializeObject(NormalProductList).Replace("</script>", "</scripttag>")
			OrderedAccountTypes = CalculateAccountTypeOrder(NormalProductList)

			Dim accountNumberLimits As New Dictionary(Of String, Integer)
			Dim instanceCountLimits As New Dictionary(Of String, Integer)
			Dim xaPreferencesDoc As XmlDocument
			Dim xaProductDoc As XmlDocument
			If _config IsNot Nothing Then
				xaPreferencesDoc = CDownloadedSettings.GetXAPreferences(_config)
				xaProductDoc = CDownloadedSettings.GetXAProducts(_config)
			End If
			If _currentProductList IsNot Nothing AndAlso _currentProductList.Any() Then
				If xaPreferencesDoc IsNot Nothing Then
					'log.Debug(xaPreferencesDoc.OuterXml)
					For Each prod As CProduct In _currentProductList
						Dim accountType As String = prod.AccountType
						If prod.AccountType = "IRACD" Then accountType = "IRA_CD"
						If xaPreferencesDoc IsNot Nothing AndAlso Not accountNumberLimits.ContainsKey(accountType) Then
							Dim accountNumberLimitNode As XmlNode = xaPreferencesDoc.SelectSingleNode(String.Format("PREFERENCES/CONSUMER_WORKFLOW/{0}_LIMIT", accountType))
							If accountNumberLimitNode IsNot Nothing AndAlso Not String.IsNullOrEmpty(accountNumberLimitNode.InnerText) Then
								If New Regex("[0-9]+").IsMatch(accountNumberLimitNode.InnerText.Trim()) Then
									accountNumberLimits.Add(accountType, CInt(accountNumberLimitNode.InnerText.Trim()))
								Else
									accountNumberLimits.Add(accountType, -1)
								End If
							End If
						End If
						If xaProductDoc IsNot Nothing AndAlso Not instanceCountLimits.ContainsKey(accountType) Then
							Dim generalInfoNode As XmlNode = xaProductDoc.SelectSingleNode(String.Format("PRODUCTS/PRODUCT/GENERAL_INFORMATION[PRODUCT_CODE='{0}']", prod.ProductCode))
							If generalInfoNode IsNot Nothing Then
								Dim maxCountConsumerNode = generalInfoNode.SelectSingleNode("MAX_COUNT_CONSUMER")
								Dim instanceCountLimit As Integer = 0
								If maxCountConsumerNode Is Nothing OrElse Not Integer.TryParse(maxCountConsumerNode.InnerText, instanceCountLimit) Then
									instanceCountLimit = 0
								End If
								instanceCountLimits.Add(prod.ProductCode, instanceCountLimit)
							End If
						End If
					Next
				End If
			End If

			AccountNumberLimitsStr = JsonConvert.SerializeObject(accountNumberLimits).Replace("</script>", "</scripttag>")
			InstanceCountLimitsStr = JsonConvert.SerializeObject(instanceCountLimits)
			For Each product As ProductInfo In NormalProductList
				Dim sKey As String = product.AccountType 'ex:check,saving
				If product.ProductIsRequired = "Y" Then
					If RequiredProductList Is Nothing Then RequiredProductList = New List(Of ProductInfo)
					RequiredProductList.Add(product)
				Else
					If Not ProductTypeDictionary.ContainsKey(sKey) Then
						Dim childProduct As New Generic.List(Of ProductInfo)
						childProduct.Add(product)
						ProductTypeDictionary.Add(sKey, childProduct)
					Else
						Dim existedChildProductList As Generic.List(Of ProductInfo)
						existedChildProductList = ProductTypeDictionary(sKey)
						'add new child to existing child product list
						existedChildProductList.Add(product)
						ProductTypeDictionary(sKey) = existedChildProductList
					End If
				End If
			Next
		End If
	End Sub

	Private Sub InitProductSelection()
		'' parent page should be CBasePage
		Dim parent As CBasePage = TryCast(Me.Page, CBasePage)
		If parent Is Nothing Or parent._CurrentWebsiteConfig Is Nothing Then Return

		InstitutionType = parent._CurrentWebsiteConfig.InstitutionType

		Dim oXA_BehaviorNode As System.Xml.XmlNode = parent._CurrentWebsiteConfig._webConfigXML.SelectSingleNode("XA_LOAN/BEHAVIOR")

		RatePrecision = 2 'default
		APYPrecision = 2
		If oXA_BehaviorNode IsNot Nothing Then
			Dim sRatePrecision = Common.SafeString(CType(oXA_BehaviorNode, System.Xml.XmlElement).GetAttribute("rate_precision"))
			Dim sApyPrecision = Common.SafeString(CType(oXA_BehaviorNode, System.Xml.XmlElement).GetAttribute("apy_precision"))

			'override default precision
			If sRatePrecision <> "" Then RatePrecision = Common.SafeInteger(sRatePrecision)
			If sApyPrecision <> "" Then APYPrecision = Common.SafeInteger(sApyPrecision)
		End If
	End Sub
	''Services, for each product, are arranged into three groups in the following order: Required services, Preselected (checked) services, and Optional (unchecked) services
	''Each group Is sorted alphabetically. 
	Private Function SortedProductServices(ByVal oProductServices As List(Of CProductService)) As List(Of CProductService)
		Dim sortedServices As New List(Of CProductService)
		Dim requiredServices As New List(Of CProductService)
		Dim preselectServices As New List(Of CProductService)
		Dim optionalServices As New List(Of CProductService)
		requiredServices = oProductServices.Where(Function(req) req.IsRequired).ToList()
		preselectServices = oProductServices.Where(Function(pre) pre.IsPreSelection).ToList()
		optionalServices = oProductServices.Where(Function(opt) Not opt.IsRequired And Not opt.IsPreSelection).ToList()
		If requiredServices IsNot Nothing AndAlso requiredServices.Count > 0 Then
			requiredServices = requiredServices.OrderBy(Function(req) req.Description.ToLower).ToList()
			sortedServices.AddRange(requiredServices)
		End If
		If preselectServices IsNot Nothing AndAlso preselectServices.Count > 0 Then
			preselectServices = preselectServices.OrderBy(Function(req) req.Description.ToLower).ToList()
			sortedServices.AddRange(preselectServices)
		End If
		If optionalServices IsNot Nothing AndAlso optionalServices.Count > 0 Then
			optionalServices = optionalServices.OrderBy(Function(req) req.Description.ToLower).ToList()
			sortedServices.AddRange(optionalServices)
		End If
		Return sortedServices
	End Function

	Protected Function BindingTerm(minTerm As Integer, maxTerm As Integer) As String
		If minTerm >= 0 AndAlso maxTerm >= 0 AndAlso minTerm < maxTerm Then
			Return String.Format("{0} - {1}", minTerm, maxTerm)
		ElseIf minTerm >= 0 Then
			Return String.Format("{0} (min)", minTerm)
		ElseIf maxTerm >= 0 Then
			Return String.Format("{0} (max)", maxTerm)
		Else
			Return "N/A"
		End If
	End Function
End Class