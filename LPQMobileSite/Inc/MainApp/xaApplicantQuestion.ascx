﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="xaApplicantQuestion.ascx.vb" Inherits="Inc_MainApp_xaApplicantQuestion" %>
<%@ Import Namespace="Newtonsoft.Json" %>

<div class="aq-section">
<%="" %> <%-- This gets rid of "__o" errors on this control --%>
<% If _renderedApplicantQuestion.Length > 0 Then%>    
	<%=Header %>
	<%--<%=HeaderUtils.RenderPageTitle(0, IIf(LoanType = "HELOC", "", "Additional Information"), True)%>--%>
	<div id="<%=_divApplicantQuestionID%>">
		<%=_renderedApplicantQuestion%>   
	</div>
     <%If LoanType = "HE" Then%>
          <input type="hidden" id="<%=IDPrefix%>hdAQHtmlHE" value='<%=_strAQHtmlHE%>' />
     <%ElseIf LoanType = "HELOC" Then%>
          <input type="hidden" id="<%=IDPrefix%>hdAQHtmlHELOC" value ='<%=_strAQHtmlHELOC%>' />
     <%End If %>
<% End If%>
</div>
<%If LoanType <> "HELOC" Then%>
    <script type="text/javascript" >
        <%--function <%=IDPrefix%>validateApplicantQuestions() {
		    var aqClass = "";
		    var aqIdPrefix = "";
		    var coPrefix = '<%=IDPrefix%>';
		    aqClass = '.' + coPrefix + 'ApplicantQuestion';
		    aqIdPrefix = '#' + coPrefix + 'ApplicantQuestion_';
		    var strMessage = "";
		    if (!$(aqClass).length) {
			    return strMessage;
		    }
		    var selectedChkQuestionNameList = getChkQuestionName(<%=IDPrefix%>chkApplicantQuestionArray());
		    $('input' + aqClass).each(function () {
			    var element = $(this);
			    var aqIndex = element.attr('aqindex');
			    var aqType = "";
			    if (element.attr('type') != undefined) {
				    aqType = element.attr('type').toUpperCase();
			    }
			    var aqId = aqIdPrefix + aqIndex;
			    var isRequired = "";
			    if ($(aqId).attr('is_required') != undefined) {
				    isRequired = $(aqId).attr('is_required');
			    }
			    var chkAnswers = "";
			    var questionName = $(aqId).text();
			
			    if (aqType == 'CHECKBOX') { //one question can have one or multiple answers ->more complicated
				    if (isRequired == 'Y') {
					    var isChecked = false;
					    questionName = questionName.replace(questionName.substring(0, 3), "");
					    for (var i = 0; i < selectedChkQuestionNameList.length; i++) {
						    if (questionName == selectedChkQuestionNameList[i]) {
							    isChecked = true;
						    }
					    }
					    if (!isChecked) {
						    strMessage = 'Please complete Additional Information section<br/>';
						    return true; //get out of the loop
					    }
				    }
			    } else { //one question -> one answer
				    if (isRequired == 'Y') {
					    var aqAnswer = element.val();
					    if (!Common.ValidateText(aqAnswer)) {
						    strMessage = 'Please complete Additional Information section<br/>';
						    return true; //get out of the loop
					    }
				    }
				    //validate regular expression- text and password if the field is not empty
				    strMessage += validateRegExpression(element, questionName);
		
			        //validate passsword
				    strMessage += validateApplicantQuestionPasswordField(element, coPrefix, isRequired);
			
			    }

		    });
		    $('select' + aqClass).each(function () {
			    var element = $(this);
			    var aqIndex = element.attr('aqindex');
			    var aqId = aqIdPrefix + aqIndex;
			    var isRequired = $(aqId).attr('is_required');
			    if (isRequired == 'Y') {
				    var aqAnswer = element.val();
				    if (!Common.ValidateText(aqAnswer)) {
					    strMessage = 'Please complete Additional Information section<br/>';
					    return true; //get out of the loop
				    }
			    }

		    });

		    if ($.lpqValidate("<%=IDPrefix%>ValidateApplicantQuestionsXA")) {
			    //validate xa applicant condition questions
			    return <%=IDPrefix%>validateApplicantConditionedQuestion();
		    }
		    return "Please complete all of the required field(s)<br/>";
		
	    }--%>
     
        $(function () {
            $('#<%=IDPrefix%>divApplicantQuestionHELOC').remove();    
        });
	    function <%=IDPrefix%>registerXAApplicantQuestions() {
		    var aqClass = "";
		    var aqIdPrefix = "";
		    var coPrefix = '<%=IDPrefix%>';
		    aqClass = '.' + coPrefix + 'ApplicantQuestion';
		    aqIdPrefix = '#' + coPrefix + 'ApplicantQuestion_';
		    $('input' + aqClass + ",input[data-confirmation-msg]", "#<%=IDPrefix%>divApplicantQuestion").each(function (idx, ele) {
		        if(<%=IDPrefix%>isHiddenAQ(ele)){
		            return true; //continue next item
		        }
		        var ctrlType = "";
			    if ($(ele).attr("type") != undefined) {
				    ctrlType = $(ele).attr("type").toUpperCase();
			    }
			    if (ctrlType == "CHECKBOX") {//one question can have one or multiple answers ->more complicated
				    $(ele).closest("div.chk-grp-wrapper").observer({
					    validators: [
						    function (partial) {
							    var $container = $(this);
							    var element = $(this.context);
							    if (element.closest(".chk-grp-wrapper.aq-item-wrapper").hasClass("hidden")) {
							    	return ""; //don't validate hidden fields
							    }
							    var aqIndex = element.attr('aqindex');
							    var aqId = aqIdPrefix + aqIndex;
							    var isRequired = "";
							    if ($(aqId).attr('is_required') != undefined) {
								    isRequired = $(aqId).attr('is_required');
							    }
							    var questionName = $(aqId).text();
							    if (partial == false) {
								    var selectedChkQuestionNameList = getChkQuestionName(<%=IDPrefix%>chkApplicantQuestionArray());
								    if (isRequired == 'Y') {
									    var isChecked = false;
									   // questionName = questionName.replace(questionName.substring(0, 3), "");
									    for (var i = 0; i < selectedChkQuestionNameList.length; i++) {
										    if (questionName == selectedChkQuestionNameList[i]) {
											    isChecked = true;
										    }
									    }
									    if (!isChecked) {
										    //return $(aqId).data("question-text") + " is required";
										    var decoded = $('<div/>').html($(aqId).data("question-text")).text();
										    return decoded + " is required";
									    }
								    }
								} else {
									if (isRequired == 'Y') {
										if ($("input:checked", $container).length === 0) {
											//return $(aqId).data("question-text") + " is required";
											var decoded = $('<div/>').html($(aqId).data("question-text")).text();
											return decoded + " is required";
										}
									}
							    }
							
							    return "";
						    }
					    ],
					    validateOnBlur: true,
					    groupType: "checkboxGroup",
					    group: "<%=IDPrefix%>ValidateApplicantQuestionsXA"
				    });
			    } else if (ctrlType === "PASSWORD") {
			    
				    $(ele).observer({
					    validators: [
						    function (partial) {
						    	var element = $(this);
						    	if (element.closest(".chk-grp-wrapper.aq-item-wrapper").hasClass("hidden")) {
						    		return ""; //don't validate hidden fields
						    	}
							    if (element.data("confirmation-msg") !== undefined) {
							        var $pairCtrl = $("#" + element.data("pair-control"));
							   
								    if ($pairCtrl.val() !== element.val()) {
									    //return element.data("confirmation-msg");
									    return "Confirmation codes do not match.";
								    }
							    } else {
								    var aqIndex = element.attr('aqindex');
								    var aqId = aqIdPrefix + aqIndex;
								    var isRequired = "";
								    if ($(aqId).attr('is_required') != undefined) {
									    isRequired = $(aqId).attr('is_required');
								    }
								    var questionName = $(aqId).text();
								    if (isRequired == 'Y') {
									    var aqAnswer = element.val();
									    if (!Common.ValidateText(aqAnswer)) {
									        if (element.data("error-msg") != undefined && element.data("error-msg") != "") {
									            return element.data("error-msg");
									        } else {
									            return questionName.replace(questionName.substring(0, 3), "");
									        }
									    }
								    }
								    var strMessage = "";
								    //validate regular expression- text and password if the field is not empty
								    strMessage += validateRegExpression(element, questionName);
								
								    if ($.trim(strMessage) !== "") {
								        //return strMessage;
								        if (element.data("error-msg") != undefined && element != "") {
								            return element.data("error-msg");
								        } else {
								            return strMessage;
								        }
								    }
								    var $confirmCtrl = $("input[data-pair-control='" + element.attr("id") + "']");
								    if ($confirmCtrl.val() !== element.val() && $confirmCtrl.val() !== "") {
									    //$.lpqValidate.showValidation($confirmCtrl, $confirmCtrl.data("confirmation-msg"));
									    return "Confirmation codes do not match.";
									    //return $confirmCtrl.data("confirmation-msg");
								    } else {
									    $.lpqValidate.showValidation($confirmCtrl);
								    }



							    }
							
							    return "";
						    }
					    ],
					    validateOnBlur: true,
					    group: "<%=IDPrefix%>ValidateApplicantQuestionsXA"
				    });
			    }
			    else {
				    $(ele).observer({
					    validators: [
						    function (partial) {
						    	var element = $(this);
						    	if (element.closest(".chk-grp-wrapper.aq-item-wrapper").hasClass("hidden")) {
								    return ""; //don't validate hidden fields
							    }
							    var aqIndex = element.attr('aqindex');
							
							    var aqId = aqIdPrefix + aqIndex;
							    var isRequired = "";
							    if ($(aqId).attr('is_required') != undefined) {
								    isRequired = $(aqId).attr('is_required');
							    }
							    var questionName = $(aqId).text();
							    if (isRequired == 'Y') {
								    var aqAnswer = element.val();
								    if (!Common.ValidateText(aqAnswer)) {
									    var decoded = $('<div/>').html($(aqId).data("question-text")).text();									
									    return decoded + " is required";								
								    }
							    }
							    var strMessage = "";
							    //validate regular expression- text and password if the field is not empty
							    strMessage += validateRegExpression(element, questionName);
							    if ($.trim(strMessage) !== "") {
								    return strMessage;
							    }
							    return "";
						    }
					    ],
					    validateOnBlur: true,
					    group: "<%=IDPrefix%>ValidateApplicantQuestionsXA"
				    });
			    }
		    });
		    $('select' + aqClass, "#<%=IDPrefix%>divApplicantQuestion").each(function (idx, ele) {
		        if(<%=IDPrefix%>isHiddenAQ(ele)){
		            return true; //continue next item
		        }
		        $(ele).observer({
				    validators: [
					    function (partial) {
					    	var element = $(this);
					    	if (element.closest(".chk-grp-wrapper.aq-item-wrapper").hasClass("hidden")) {
					    		return ""; //don't validate hidden fields
					    	}
						    var aqIndex = element.attr('aqindex');
						    var aqId = aqIdPrefix + aqIndex;
						    var isRequired = $(aqId).attr('is_required');
						    if (isRequired == 'Y') {
							    var aqAnswer = element.val();
							    if (!Common.ValidateText(aqAnswer)) {
								    var questionName = $(aqId).text();
								    var decoded = $('<div/>').html($(aqId).data("question-text")).text();
								    return decoded + ' is required';								
							    }
						    }
						    return "";
					    }
				    ],
				    validateOnBlur: true,
				    group: "<%=IDPrefix%>ValidateApplicantQuestionsXA"
			    });
		    });
	    }

	    <%=IDPrefix%>registerXAApplicantQuestions();

	    //deprecated
	    //function validateApplicantQuestionPasswordField(element,coPrefix,isRequired) {
        //    var strMessage = "";
        //    if (element.attr('type') == 'password') {
        //       var reEnterPasswordElem = $("#" + coPrefix + "divApplicantQuestion input[iname='re_" + element.attr('iname') + "']");
        //       if (isRequired == 'Y') {
        //            if (reEnterPasswordElem.val() == "") {
        //        	    strMessage = "<br/>- " + element.parent().prev().text() + " -- Confirmation does not match password test<br/>";
        //            } else if (element.val() != reEnterPasswordElem.val()) {
        //        	    strMessage = "<br/>- " + element.parent().prev().text() + " -- Confirmation does not match password test<br/>";
        //            }
        //        } else { 
        //           if (element.val() !="" || reEnterPasswordElem.val()!="") {// validate password if the input password is not empty
        //               if (element.val() != reEnterPasswordElem.val()) {
        //           	    strMessage = "<br/>- " + element.parent().prev().text() + " -- Confirmation does not match password test<br/>";
        //               }
        //            }
        //        }
        //    }

        //    return strMessage;
        //}

	    //?deprecate?
        //validate applicant condition questions
	    function <%=IDPrefix%>validateApplicantConditionedQuestion() {
    	    var strMessage = "";
    	    var conAQClass = '<%=IDPrefix%>ap_ConditionedQuestion';
    	    if (!$('.' + conAQClass).length) {
    		    return strMessage;
    	    }
    	    var chk_con_list = <%=IDPrefix%>getChkApplicantQuestionList(conAQClass, 'checkbox');
    	    var chk_con_num = 0;
    	    var isChecked = false;
    	    isRequiredFailed = false;
    	    $('input.' + conAQClass).each(function () {
    		    if (isRequiredFailed) {
    			    // return false to stop iterating through the customquestion inputs
    			    return false;
    		    }
    		    var element = $(this);
    		    var ui_type = element.attr('type');
    		    var is_required = element.attr('is_required') == 'Y';
    		    var questionName = element.prev().text();
    		    var answerText = "";
    		    if (element.val() != undefined) {
    			    answerText = element.val().trim();
    		    }
    		    //use is(':checked') to determine check or uncheck of checkbox 
    		    if (ui_type == 'checkbox') {
    			    if (!is_required) {
    				    return true; // no validate, go to the next checkbox
    			    }
    			    //only check at least one(no need to check all the checkbox) for a question
    			    if (element.is(':checked')) {
    				    isChecked = true;
    			    }
    			    for (var i = 0; i < chk_con_list.length; i++) {
    				    if (element.attr('name') == chk_con_list[i].chkName) {
    					    chk_con_num++;
    					    if (chk_con_num < chk_con_list[i].chkNum && isChecked) {
    						    return true; //go to the next input
    					    } else if (chk_con_num == chk_con_list[i].chkNum) {
    						    //reset chk_con_num
    						    chk_con_num = 0;
    						    if (!isChecked) {
    							    if (element.parent().parent().is(':visible') && is_required == true) {
    								    strMessage = "Please complete Additional Information section<br/>";
    								    isRequiredFailed = true;
    								    break;
    							    }
    						    } else {
    							    isChecked = false;//reset isChecked for next input
    							    break;
    						    }
    					    }
    				    }//end for
    			    }
    		    } else {
    		        if (element.parent().is(':visible')) {
    		            if (is_required == true && answerText == "") {
    		                strMessage = "Please complete Additional Information section<br/>";
    		                isRequiredFailed = true;
    		                return false;
    		            }
    		            strMessage += validateRegExpression(element, questionName);
    		            //validate re-enter password field for conditioned question
    		            if (ui_type == "password" && strMessage == "") {
    		                var rePWEle = $('#re_txt_' + element.attr('aria-labelledby'));
    		                if (rePWEle.length > 0) {
    		                    if (element.val() != rePWEle.val()) {
    		                        strMessage += "Confirmation codes do not match.";
    		                        return false;
    		                    }
    		                }
    		            }
    			    }	
    		    }
    	    });
    	    isRequiredFailed = false;
    	    $('select.' + conAQClass).each(function () {
    		    if (isRequiredFailed) {
    			    // return false to stop iterating through the customquestion inputs
    			    return false;
    		    }
    		    var element = $(this);
    		    var ui_type = element.attr('type');
    		    var is_required = element.attr('is_required') == 'Y';
    		    if (!is_required) {
    			    return true;  //no validate, go to next dropdown
    		    }
    		    if (element.parent().parent().is(':visible') && is_required && element.val() == "") {
    			    strMessage = "Please complete Additional Information section<br/>";
    			    isRequiredFailed = true;
    		    }
    	    });
    	    return strMessage;
	    } //end applicant conditioned questions

	    function <%=IDPrefix%>getChkApplicantQuestionList(cqClass, cqType) {
    	    var chkCQList = new Array();
    	    var chkCQ = new Object();
    	    var chkCQArray = new Array();
    	    var chkCQObject = {};
    	    $('input.' + cqClass).each(function () {
    		    var currElement = $(this);
    		    if (currElement.attr('type').toLowerCase() == cqType) {
    			    chkCQArray.push(currElement.attr('name')); //get all chk questions 
    		    }
    	    });
    	    if (chkCQArray.length > 0) { //group chk questions and get number of chk for each custom question
    		    for (chkCQ in chkCQArray) {
    			    if (chkCQObject[chkCQArray[chkCQ]]) {
    				    chkCQObject[chkCQArray[chkCQ]]++;
    			    } else {
    				    chkCQObject[chkCQArray[chkCQ]] = 1;
    			    }
    		    }
    	    }
    	    for (var key in chkCQObject) {
    		    chkCQ = { chkName: key, chkNum: chkCQObject[key] };
    		    chkCQList.push(chkCQ); //put chk name and num of chk in array list
    	    }
    	    return chkCQList;
        }
    	function <%=IDPrefix%>ViewApplicantQuestion() {
    		var coPrefix = '<%=IDPrefix%>';
    	    var aqClass = "." + coPrefix + "ApplicantQuestion";
    	    var aqIdPrefix = "#" + coPrefix + "ApplicantQuestion_";
    	    var chkAQs =[];
    	    var strCustomQuestion = "";

		    if ($(aqClass).length > 0) {
			    $('#<%=IDPrefix%>divApplicantQuestion .aq-item-wrapper').each(function() {
				    var element = $(this);
				    if (<%=IDPrefix%>isHiddenAQ(this)) {
					    return true; //go to next item
				    }
				    if (element.find('input[type="checkbox"]').length > 0) {
					    chkAQs.push(element.attr('qname'));
				    }
			    });
			    // var hasChk = true;
			    if ($(".ViewMinorApplicantQuestion").length > 0) {
				    $(".ViewMinorApplicantQuestion").html('');
			    }
			    $(aqClass).each(function() {
				    if (<%=IDPrefix%>isHiddenAQ(this)) {
					    return true; //go to next item
				    }
				    var element = $(this);
				    var txtValue = element.val();
				    var labelText = element.prev("label").text();
				    var qNum = element.attr('aqindex');
				    var questionName = $(aqIdPrefix + qNum).text();
				    //questionName = questionName.replace(questionName.substring(0, 3), "");
				    var ui_type = element.attr('type');
				    if (ui_type == 'checkbox') {
					    //if (hasChk) {
					    //    strCustomQuestion += <%=IDPrefix%>ReviewCheckboxApplicantQuestion();
					    //    hasChk = false;
					    //}    		       
					    var chkAQName = element.attr('iname');
					    if ($.inArray(chkAQName, chkAQs) > -1) {
						    var strCheckboxHtml = "";
							$("#<%=IDPrefix%>divApplicantQuestion div[qname]").filter(function (idx, el) { return $(el).attr("qname") == chkAQName }).find("input[type='checkbox']").each(function() {
							    var $chk = $(this);
							    if ($chk.prev('label').hasClass('ui-checkbox-on')) {
								    strCheckboxHtml += $chk.prev('label').text() + "<br/>";
							    }
						    });
						    if (strCheckboxHtml != "") {
							    var chkAQText = $("#<%=IDPrefix%>divApplicantQuestion div[qname]").filter(function (idx, el) { return $(el).attr("qname") == chkAQName }).children().first().text();
							    strCustomQuestion += '<div class="col-xs-12 row-title"><span class="bold">' + chkAQText + '</span></div><div class="col-xs-12 row-data" style="padding-left:10px"><span>' + strCheckboxHtml + '</span></div>';
						    }
						    chkAQs[chkAQs.indexOf(chkAQName)] = ""; //reset chkAQName
					    }
				    } else if (ui_type == 'radio') {
					    if (element.prev('.ui-radio-on').length > 0) {
						    strCustomQuestion += '<div class="col-xs-12 row-title"><span class="bold">' + questionName + '</span></div><div class="col-xs-12 row-data" style="padding-left:10px"><span>' + labelText + '</span></div>';
					    }
				    } else if (element.children('option').length > 0) {

					    var optionElem = element.find('option:selected');
					    if (optionElem.val() != undefined && optionElem.val() != "") { //"None could be a user answer,
						    strCustomQuestion += '<div class="col-xs-12 row-title"><span class="bold">' + questionName + '</span></div><div class="col-xs-12 row-data" style="padding-left:10px"><span>' + optionElem.text() + '</span></div>';
					    }
				    } else {
					    if (txtValue != '' && txtValue != undefined) {
						    if (ui_type == 'password') {
							    var pwValue = "";
							    for (var i = 0; i < txtValue.length; i++) {
								    if (i < txtValue.length - 2) {
									    pwValue += "*"; //don't show the real value
								    } else { //only show last two characters
									    pwValue += txtValue.substring(i);
									    break;
								    }
							    }
							    txtValue = pwValue;
						    }
						    strCustomQuestion += '<div class="col-xs-12 row-title"><span class="bold">' + questionName + '</span></div><div class="col-xs-12 row-data" style="padding-left:10px"><span>' + txtValue + '</span></div>';
					    }
				    }

			    });
			    /*
    			//using select.customquestion to get drop down question type
    			$('select' + aqClass).each(function () {
    				var element = $(this);
    				var ddlText = element.find('option:selected').text();
    				var qNum = element.attr('aqindex');
    				var questionName = $(aqIdPrefix + qNum).text();
    			  //  questionName = questionName.replace(questionName.substring(0, 3), "");
    				var ddlValue = element.find('option:selected').val();
    				if (ddlValue != "" && ddlValue != undefined) { //"None could be a user answer,
    					strCustomQuestion += '<div class="col-xs-12 row-title"><span class="bold">' + questionName + '</span></div><div class="col-xs-12 row-data" style="padding-left:10px"><span>' + ddlText + '</span></div>';
    				}
    			});*/
			    strCustomQuestion += ViewConditionQuestions(coPrefix + "ap_");
		    }
		    $(this).hide();
    	    $(this).prev("div.row").hide();
    	    if (strCustomQuestion !== "") {
    	    	$(this).show();
    	    	$(this).prev("div.row").show();
    	    }
    	    return strCustomQuestion;
        }
        //this function for checkbox applicant questions
	    function <%=IDPrefix%>chkApplicantQuestionArray() {
    	    //push all the check box question index in chkquestionArray
		    var aqClass = ".<%=IDPrefix%>ApplicantQuestion";
		    var aqIdPrefix = "#<%=IDPrefix%>ApplicantQuestion_";
    	    var chkQuestionArray = new Array();
    	    if (!$(aqClass).length) {
    		    return chkQuestionArray;
    	    }
    	    $(aqClass).each(function () {
    	        if(<%=IDPrefix%>isHiddenAQ(this)){
    	            return true; //continue next item
    	        }
    		    var element = $(this);
    		    var chkAnswers = "";
    		    var qNum = element.attr('aqindex');
    		    var aqId = aqIdPrefix + qNum;
    		    var questionName = $(aqId).text();
    		   // questionName = questionName.replace(questionName.substring(0, 3), "");
    		    var chkLength = element.attr('chk_aq_length');
    		    var isRequired = $(aqId).attr('is_required');
    		    var ui_type = element.attr('type');
    		    if (ui_type == 'checkbox') {
    			    if (element.prev('.ui-checkbox-on').length > 0) {
    				    chkAnswers = element.prev("label").text();
    				    chkQuestionArray.push(new checkboxQuestion(chkAnswers, questionName, qNum, chkLength, isRequired));
    			    }
    		    }
    	    });
    	    return chkQuestionArray;
        }
        function <%=IDPrefix%>ReviewCheckboxApplicantQuestion() {
    	  var chkQuestionArray = [];
    	    //push all the check box question index in chkquestionArray
    	    chkQuestionArray = <%=IDPrefix%>chkApplicantQuestionArray();
    	  var chkQuestionNameArray = getChkQuestionName(chkQuestionArray);
    	    var strCheckBox = "";
    	    if (chkQuestionNameArray.length > 0) {
    		    for (var i = 0; i < chkQuestionNameArray.length; i++) {
    			    var questionName = chkQuestionNameArray[i];
    			    var strCheckboxHtml = "";
    			    for (var j = 0; j < chkQuestionArray.length; j++) {
    				    if (chkQuestionArray[j].chkQuestionName == questionName && chkQuestionArray[j].chkAnswers != "") {
    				        strCheckboxHtml += chkQuestionArray[j].chkAnswers+"<br/>";
    				    }
    			    }
    			    strCheckBox += '<div class="col-xs-12 row-title"><span class="bold">' + questionName + '</span></div><div class="col-xs-12 row-data" style="padding-left:10px"><span>' + strCheckboxHtml + '</span></div>';
    		    }
    	    }
    	    return strCheckBox;
		}
		function <%=IDPrefix%>getAllCustomQuestionAnswers() {
			//get the applicant question answer
		    var aqAnswer = new Object;
		    var aqAnswerArray = new Array();
			var aqClass = '.<%=IDPrefix%>ApplicantQuestion:not(span)';
			var checkboxQuestions = [];
		    if ($(aqClass).length) {
		        $(aqClass).each(function () {
		            if(<%=IDPrefix%>isHiddenAQ(this)){
		                return true; //continue next item
		            }
			        var $element = $(this);
				    var ui_type = $element.attr('cq_type');
					if (ui_type == "CHECKBOX") {
						aqAnswer = { CQName: $element.attr('iname'), CQAnswer: $element.attr('answer'), CQRole: $element.attr("cq_role"), CQApplicantPrefix: "<%=IDPrefix%>", CQLocation: $element.attr("cq_location"), CQType: ui_type };
						if ($element.is(":checked")) {
							aqAnswerArray.push(aqAnswer);
						} else {
							checkboxQuestions.push(aqAnswer);
						}
				    }
				    else {
						aqAnswer = { CQName: $element.attr('iname'), CQAnswer: $element.val(), CQRole: $element.attr("cq_role"), CQApplicantPrefix: "<%=IDPrefix%>", CQLocation: $element.attr("cq_location"), CQType: ui_type };
						aqAnswerArray.push(aqAnswer);
				    }

			    });
			}

			// This function should return a single entry with an answer value of "" for Checkboxes.
			// Add such an entry if no other checkbox answers are present for the CQ.
			var uniqCheckboxes = _.uniqBy(checkboxQuestions, "CQName");
			for (var i = 0; i < uniqCheckboxes.length; i++) {
				uniqCheckboxes[i].CQAnswer = "";
				if (!_.some(aqAnswerArray, { CQName: uniqCheckboxes[i].CQName })) {
					aqAnswerArray.push(uniqCheckboxes[i]);
				}
			}

			return aqAnswerArray;
		}
	    //getApplicantQuestions()
	    function <%=IDPrefix%>getXAApplicantQuestions() {
		    //get the applicant question answer
		    var aqAnswer = new Object;
		    var aqAnswerArray = new Array();
		    var aqClass = '.<%=IDPrefix%>ApplicantQuestion:not(span)';
		    if ($(aqClass).length) {
		        $(aqClass).each(function () {
		            if(<%=IDPrefix%>isHiddenAQ(this)){
		                return true; //continue next item
		            }
			        var element = $(this);
				    var ui_type = element.attr('type');
				    if (ui_type == "checkbox") {
					    if (element.prev('.ui-checkbox-on').length > 0) {
						    aqAnswer = { q: element.attr('iname'), a: element.attr('answer') };
						    aqAnswerArray.push(aqAnswer);
					    }
				    }
				    else {
					    if (element.val() != "" && element.val() != undefined) //make sure answer is not empty or undefined
					    {
						    aqAnswer = { q: element.attr('iname'), a: element.val() };
						    aqAnswerArray.push(aqAnswer);
					    }
				    }

			    });
		    }
		    //get applicant condition questions
		    var conAQClass = '.<%=IDPrefix%>ap_ConditionedQuestion:not(span)';
		    if ($(conAQClass).length) {
			    $(conAQClass).each(function () {
				    var element = $(this);
				    var strDisplayedCQ = getDivConditionQuestionStyle(element);
				    var ui_type = element.attr('type');
				    if (ui_type == "checkbox") {
					    if (strDisplayedCQ.indexOf('none') == -1) {
						    if (element.prev('.ui-checkbox-on').length > 0) {
							    aqAnswer = { q: element.attr('iname'), a: element.attr('answer') };
							    aqAnswerArray.push(aqAnswer);
						    }
					    }
				    } else {
					    if (strDisplayedCQ.indexOf('none') == -1) {
						    aqAnswer = { q: element.attr('iname'), a: element.val() };
						    aqAnswerArray.push(aqAnswer);
					    }
				    }
			    });
		    }
		    return aqAnswerArray;
	    }
        //handle show and hide minor applicant questions
        function <%=IDPrefix%>isHiddenAQ(element){
            if('<%=IDPrefix%>'=='m_'){
                //skip question if it has hidden class
				if ($('div[qname]').filter(function (idx, el) { return $(el).attr("qname") == $(element).attr("iname") }).hasClass('hidden')) {
                    return true;
                }
            }
            return false;
		}
		function <%=IDPrefix%>getAllUrlParaCustomQuestionAnswers() {
			<%If _URLParaCustomQuestions IsNot Nothing AndAlso _URLParaCustomQuestions.Any() Then %>
			var urlParaCustomQuestions = <%=JsonConvert.SerializeObject(_URLParaCustomQuestions) %>;
			return urlParaCustomQuestions;
			<% Else %>
			return [];
			<%End If %>
		}
        if('<%=IDPrefix%>' =='m_'){
            var specialAccountTypes =<%=Newtonsoft.Json.JsonConvert.SerializeObject(_specialAccountTypes)%>;
            var xaMinorAQ={};
            xaMinorAQ={
                getApplicantQuestionNames: function(mAccount){
                    var mQuestionNames =[];
                    if (mAccount==undefined || mAccount==""){
                        return mQuestionNames;
                    }
                    for (var key in specialAccountTypes){
                        //convert minor account types to upper case
                        var mAccountTypes = specialAccountTypes[key].map(function(type){return type.toUpperCase()});
                        if($.inArray(mAccount.toUpperCase(),mAccountTypes)>-1){
                            mQuestionNames.push(key);
                        }
                    }
                    return mQuestionNames;
                },
                showSelectedApplicantQuestions:function(){
                    var $btnAccountType =$('a#ddlSpecialAccountType');
                    var mQuestionNames=[];
                    var mAccountType ="";
                    if($btnAccountType.length>0){
                        if($btnAccountType.hasClass('active')){
                            mAccountType =$btnAccountType.text();
                            mQuestionNames =  xaMinorAQ.getApplicantQuestionNames(mAccountType);
                            xaMinorAQ.showAndHideApplicantQuestions(mQuestionNames);
                        }
                    }else{
                        //dropdown special account type
                        $('#btnSelectionContainer').on('change', 'select#ddlSpecialAccountType', function () {
                            mAccountType =$(this).find('option:selected').text();  
                            mQuestionNames =  xaMinorAQ.getApplicantQuestionNames(mAccountType);
                            xaMinorAQ.showAndHideApplicantQuestions(mQuestionNames);
                        });
                    }    
                },
                showAndHideApplicantQuestions:function(QuestionNames){
                    var $divAQ = $('#m_divApplicantQuestion div[class~="aq-item-wrapper"]');    
                    var isHiddenAQ =true;
                    $divAQ.each(function(){
                        var $currentAQ =$(this);
                        var aqName= $currentAQ.attr('qname');
                        var $aqText=$currentAQ.children().first();                  
                        if($.inArray(aqName,QuestionNames)>-1){
                            $currentAQ.removeClass('hidden');
                            isHiddenAQ = false;
                            //no using question index
                           // var sText="<span class="bold">"+(QuestionNames.indexOf(aqName)+1).toString()+ $aqText.html().substring($aqText.html().indexOf('.'));
                            //$aqText.html(sText);
                        }else{
                            $currentAQ.addClass('hidden');
                        }
                    });
                    if(isHiddenAQ){ //no applicant questions --> hide "Additional Information" 
                      $('#m_divApplicantQuestion').prev().addClass('hidden');
                    }else{
                      $('#m_divApplicantQuestion').prev().removeClass('hidden');
                    }
                }      
            };
             
            $(function(){  
                xaMinorAQ.showSelectedApplicantQuestions();
             });
        } 
    </script>
<%End If%>