﻿
Partial Class Inc_MainApp_ApplicantAdditionalInfo
	Inherits CBaseUserControl

	Public Property LogoUrl As String
	Public Property PreviousPage As String = ""
	Public Property PreviousCoAppPage As String = ""
	Public Property NextPage As String = ""
	Public Property NSSList As Dictionary(Of String, List(Of String))
	Protected Property EnabledAddressVerification As Boolean = False
	Public Property EnablePrimaryAppSpouseContactInfo As Boolean = False
	Public Property EnableCoAppSpouseContactInfo As Boolean = False

	Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
		IDPrefix = "AppSpouseInfo"
		If Page._CurrentWebsiteConfig IsNot Nothing Then
			EnabledAddressVerification = Page._CurrentWebsiteConfig.EnabledAddressVerification
		End If
	End Sub
End Class
