﻿Imports LPQMobile.Utils
Imports System.Web.Script.Serialization
Partial Class Inc_MainApp_xaFOMQuestion
    Inherits CBaseUserControl

    Public _FOMQuestions As List(Of CFOMQuestion)
    Public _CFOMQuestionList As List(Of CFOMQuestion)
    Public _lenderRef As String
    Protected _NextQuestionList As String

    Protected _hasInnerNextQuestion As String = ""
    Protected _strNextQuestionDic As String
    Protected _innerNQNames As String
    Protected _specialFOMQNames As String = ""
    Dim _formHeader As String
    Public Property SelectedSpecialAccountType As String = ""
    Public Property FOMQuestions() As List(Of CFOMQuestion)
        Get
            If _FOMQuestions Is Nothing Then
                _FOMQuestions = New List(Of CFOMQuestion)
            End If
            Return _FOMQuestions
        End Get
        Set(ByVal value As List(Of CFOMQuestion))
            _FOMQuestions = value
        End Set
    End Property

    Public Property FormHeader() As String
        Get
            Return _formHeader
        End Get
        Set(value As String)
            _formHeader = value
        End Set
    End Property

    Private _hasFOM_V2 As Boolean
    Public Property hasFOM_V2() As Boolean
        Get
            Return _hasFOM_V2
        End Get
        Set(value As Boolean)
            _hasFOM_V2 = value
        End Set
    End Property

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Me.IsPostBack Then

            If _FOMQuestions Is Nothing Then
                _FOMQuestions = New List(Of CFOMQuestion)
            End If

            If _CFOMQuestionList Is Nothing Then
                _CFOMQuestionList = New List(Of CFOMQuestion)
            End If
            Dim sAvailability As String = Common.SafeString(Request.Params("type")).ToLower
            filterFOMQuestions(sAvailability)
            rptFOMQuestions.DataSource = _FOMQuestions
            rptFOMQuestions.DataBind()
            Dim oSerialize As New JavaScriptSerializer()
            _NextQuestionList = oSerialize.Serialize(getNextQuestions())
            If (getNextQuestionList2().Count > 0) Then
                _hasInnerNextQuestion = "Y"
            End If
            _strNextQuestionDic = oSerialize.Serialize(getInnerNextQuestionDic())
            Dim nqList1 As New List(Of String)
            For Each nq1 In getNextQuestionList1()
                nqList1.Add(nq1.Name)
            Next
            _innerNQNames = oSerialize.Serialize(nqList1)
        End If

    End Sub
   
    Protected Sub filterFOMQuestions(psAvailability As String)
        Dim personalFOMQuestions As New List(Of CFOMQuestion)
        Dim businessFOMQuestions As New List(Of CFOMQuestion) ''for future use
        Dim specialFOMQuestions As New List(Of CFOMQuestion)
        Dim cFomQuestions As New List(Of CFOMQuestion)
        Dim specialFOMNameDic As New Dictionary(Of String, List(Of String))
        For Each oFOMQuestion In _FOMQuestions
            If oFOMQuestion.for_personal <> "false" Then
                personalFOMQuestions.Add(oFOMQuestion)
            End If
            If oFOMQuestion.for_business <> "false" Then
                businessFOMQuestions.Add(oFOMQuestion)
            End If
            If oFOMQuestion.for_special <> "false" Then
                specialFOMQuestions.Add(oFOMQuestion)
                If Not specialFOMNameDic.ContainsKey(oFOMQuestion.Name) Then
                    specialFOMNameDic.Add(oFOMQuestion.Name, oFOMQuestion.supportedSpecialAccountTypes)
                End If
            End If
        Next
        _specialFOMQNames = New JavaScriptSerializer().Serialize(specialFOMNameDic)   
        Select Case psAvailability.ToLower
            Case "1s", "2s"
                ''filter Fom question based on selected special account type
                Dim filterSpecialFOMQuestions As New List(Of CFOMQuestion)
                If SelectedSpecialAccountType <> "" Then
                    For Each oSpecialFOMQuestion As CFOMQuestion In specialFOMQuestions
                        Dim sAccType As String = ""
                        sAccType = oSpecialFOMQuestion.supportedSpecialAccountTypes.FirstOrDefault(Function(accType) accType.ToUpper = SelectedSpecialAccountType.ToUpper)
                        If Not String.IsNullOrEmpty(sAccType) Then
                            filterSpecialFOMQuestions.Add(oSpecialFOMQuestion)
                        End If
                    Next
                End If
                _FOMQuestions = filterSpecialFOMQuestions
            Case "1b", "2b"
                _FOMQuestions = businessFOMQuestions
            Case Else
                _FOMQuestions = personalFOMQuestions
        End Select
    End Sub
    Protected Function GenerateExtraData(ByVal obj As Object, ByVal questionIndex As Integer) As String

        Dim cq As CFOMQuestion = obj
        Dim textTemplate As String = cq.TextTemplate

        ''FOM_V2 -------
        If hasFOM_V2 Then
            Dim LabelTextArray() As String = getTextTemplate(cq)
            Dim sIndex As Integer = 0
            Dim inputHtml As String = ""
            For Each oField As CFOMQuestionField In cq.Fields
                Dim labelText As String = ""
                If sIndex < LabelTextArray.Length Then
                    labelText = "<span class='bold'>" & LabelTextArray(sIndex) & "</span>"
                End If
                inputHtml += labelText + GenerateInputForField(oField, questionIndex)
                sIndex += 1
            Next

            Return inputHtml
        End If ''End FOM_V2 -------


        '' '' remove first title
        Dim firstIndex As Integer = textTemplate.IndexOf("{")
        If (firstIndex < 0) OrElse cq.Fields.Count = 0 Then
            Return ""
        End If
        Dim strTitle As String = textTemplate.Substring(0, firstIndex)

        If (textTemplate.IndexOf("<table>") > 0) Then
            textTemplate = textTemplate.Substring(textTemplate.IndexOf("<table>"))
            textTemplate = Regex.Replace(textTemplate, "<.*?>", String.Empty) ' remove html tag :D
        ElseIf (strTitle.LastIndexOf(":") > -1 And strTitle.LastIndexOf(":") < firstIndex) Then
            ''check no character between the last ":" and "{"
            Dim strChar As String = ""
            Dim strLength As Integer = firstIndex - strTitle.LastIndexOf(":")
            If strLength > 0 Then
                strTitle.Substring(strTitle.LastIndexOf(":"), strLength).Trim()
            End If

            If Not String.IsNullOrEmpty(strChar) Then
                ''case new line for member name(example lenderef=fuscu_test) 
                textTemplate = textTemplate.Substring(textTemplate.LastIndexOf(":") + 1)
            Else
                textTemplate = textTemplate.Substring(firstIndex)
            End If

            ''case new line for member name(example lenderef=fccu_test) 
            If (strTitle.LastIndexOf(".") > -1 And strTitle.LastIndexOf(".") < strTitle.LastIndexOf(":")) Then
                Dim length As Integer = strTitle.LastIndexOf(":") - strTitle.LastIndexOf(".")
                Dim strFirstInput As String = strTitle.Substring(strTitle.LastIndexOf("."), length)
                If strFirstInput.ToUpper.IndexOf("NAME") > -1 Or strFirstInput.ToUpper.IndexOf("MEMBER") > -1 Or strFirstInput.ToUpper.IndexOf("RELATIONSHIP") > -1 Then
                    textTemplate = cq.TextTemplate.Substring(strTitle.LastIndexOf(".") + 1)
                End If
            End If
        Else
            textTemplate = textTemplate.Substring(firstIndex) ' remove first title
        End If
        'textTemplate = Regex.Replace(textTemplate, "<.*?>", String.Empty) ' remove html tag :D
        textTemplate = Replace(textTemplate, "<br/>", "")
        ' continue generate rest field by replacing {idx} :D
        ' i prefer for each :D
        Dim idx As Integer = 0
        For Each oField As CFOMQuestionField In cq.Fields
            Dim pattern As String = "{" & idx.ToString() & "}"
            Dim inputHtml As String = GenerateInputForField(oField, questionIndex)
            textTemplate = textTemplate.Replace(pattern, inputHtml)
            idx += 1
        Next
        Return textTemplate


    End Function

    Protected Function GenerateInputForField(ByVal field As CFOMQuestionField, ByVal questionIndex As Integer) As String
        Dim sb As StringBuilder = New StringBuilder()
        Dim strHiddenInputField As String = ""
        ' we only have 2 case: dropdown and textbox (lowercase)
        If field.Type.Equals("dropdown") AndAlso field.Answers.Count > 0 Then
            sb.AppendFormat("<select aria-labelledby='fom-question-" & questionIndex & "' id='ddl-answer-{0}' class='fom-answer-{0}' onchange='AutoSelectItem(""fom-question-{0}"");'>", questionIndex)
            Dim ddlIndex As Integer = 0
            For Each anw As CFOMQuestionAnswer In field.Answers
                sb.AppendFormat("<option value=""{0}"">{1}</option>", Server.HtmlEncode(anw.Value), Server.HtmlEncode(anw.Text))
                If Not String.IsNullOrEmpty(anw.NextQuestion) Then
					strHiddenInputField += "<input type='hidden' tabindex='-1' id='hdNextQuestion" & questionIndex & "_" & ddlIndex & "' value='" & anw.NextQuestion & "' />"
                End If
                ddlIndex += 1
            Next
            sb.Append("</select>")
        Else ' we treat this one as textbox
            sb.AppendFormat("<input aria-labelledby='fom-question-" & questionIndex & "' type='text' maxlength='100' class='fom-answer-{0}' data-customValidation='{1}' onchange='AutoSelectItem(""fom-question-{0}"");' />", questionIndex, field.CustomValidations)
        End If
        Return sb.ToString() + strHiddenInputField
    End Function
    Function getTextTemplate(ByVal nq As CFOMQuestion) As String()

        If hasFOM_V2 Then
            Return getTextTemplate_V2(nq)
        End If

        Dim textTemplate As String() = Nothing
        Dim fieldCount As Integer = 0
        ''count number of fields in each question
        For Each oField As CFOMQuestionField In nq.Fields
            fieldCount += 1
        Next
        Dim txtTemplate As String = nq.TextTemplate
        'txtTemplate = Regex.Replace(txtTemplate, "<.*?>", String.Empty)
        txtTemplate = Regex.Replace(txtTemplate, "<(?!a|/a).*?>", String.Empty)
        Dim pattern As String = ""
        For i = 0 To fieldCount - 1
            pattern = "{" & i & "}"
            txtTemplate = Replace(txtTemplate, pattern.ToString, ";")
        Next
        textTemplate = txtTemplate.Split(";")
        Return textTemplate

    End Function

    Function getTextTemplate_V2(ByVal cq As CFOMQuestion) As String()
        Dim LabelTextArray() As String = {}
        Dim textTemplate As String = cq.TextTemplate
        Dim startIndex As Integer = textTemplate.IndexOf("{")
        Dim endIndex As Integer = textTemplate.IndexOf("}")
        If startIndex > -1 And endIndex > startIndex Then
            Dim labelTextParas As String = textTemplate.Substring(startIndex + 1, endIndex - startIndex - 1)
            If Not IsNumeric(labelTextParas) Then
                ''get label text from parameters {...}
                Return labelTextParas.Split(";")
            Else ''handled case parameter is number
                ''remove first Title
                textTemplate = textTemplate.Substring(startIndex)

                Dim fieldCount As Integer = textTemplate.Split("{").Length
                Dim pattern As String = ""
                For i = 0 To fieldCount - 1
                    pattern = "{" & i & "}"
                    textTemplate = Replace(textTemplate, pattern.ToString, ";")
                Next

                Return textTemplate.Split(";")
            End If
        End If
        ReDim LabelTextArray(cq.Fields.Count)
        Dim sIndex As Integer = 0
        ''get label text of each field
        For Each oField As CFOMQuestionField In cq.Fields
            If sIndex <cq.Fields.Count Then
                LabelTextArray(sIndex) = oField.labelText
            End If
            sIndex += 1
        Next
        Return LabelTextArray
    End Function
    Function getFieldCount(ByVal nq As CFOMQuestion) As Integer
        Dim fieldCount As Integer = 0
        For Each ofield As CFOMQuestionField In nq.Fields
            fieldCount += 1
        Next
        Return fieldCount
    End Function
    Function RenderingNextQuestionFields(ByVal obj As Object, ByVal cqIndex As Integer) As String
        Dim cq As CFOMQuestion = obj
        Dim strHtml As String = ""
        Dim hasNQName As Boolean = False
        Dim nqIndex As Integer = 0
        Dim strTitle As String()
        Dim fieldCount As Integer
        Dim tIndex As Integer
        '' need to get all the value and next question attribute using dictionary
        Dim dicNextQuestions As New Dictionary(Of String, String)
        ''add next question attribute if it exist in question node
        If Not String.IsNullOrEmpty(cq.NextQuestion) Then
            If Not dicNextQuestions.ContainsKey(cq.NextQuestion) Then
                dicNextQuestions.Add(cq.NextQuestion, cq.NextQuestion)
            End If

        End If
        '' add all next question attribute in answers node 
        For Each ofield As CFOMQuestionField In cq.Fields
            For Each oAns As CFOMQuestionAnswer In ofield.Answers
                If (Not String.IsNullOrEmpty(oAns.NextQuestion)) Then
                    If Not dicNextQuestions.ContainsKey(oAns.NextQuestion) Then
                        dicNextQuestions.Add(oAns.NextQuestion, oAns.Value)
                    End If
                End If
            Next
        Next
        ''rendering input fields for all next question in dicNextQuestion
        For Each nq As CFOMQuestion In _CFOMQuestionList
            If (dicNextQuestions.ContainsKey(nq.Name)) Then
                Dim divNQPrefix As String = nq.Name.Replace(" ", "_")
                strTitle = getTextTemplate(nq)
                fieldCount = getFieldCount(nq)
                Dim textTemplate As String = nq.TextTemplate
                '' textTemplate = Regex.Replace(textTemplate, "<.*?>", String.Empty)
                textTemplate = Regex.Replace(textTemplate, "<(?!a|/a).*?>", String.Empty)
                Dim isFound As Boolean = False
                Dim Items As Integer = 0
                Dim sb As New StringBuilder()
                sb.AppendFormat("<div id ='divNextQuestion" & cqIndex.ToString() & divNQPrefix & "' style='display:none' class='NextQuestion' name=""{1}"" fieldCount='{2}' text_template =""{3}"" selectedValue=""{4}"">", cqIndex, nq.Name, fieldCount, textTemplate, divNQPrefix)
                strHtml += sb.ToString()
                tIndex = 0
                ''no parameter("{0},{1}, or {text ...}) in text template and also all labelText of each field is empty
                'example lenderref =LAPFCU_BETA
                ''<question name="TIPS" text_template="Please specify which law enforcement agency you are affiliated with" header_text="">
                ''<fields>
                ''	<field type = "dropdown" >
                ''       <answers>
                ''            <answer text="Alhambra Civilian" value="02-ALHAMBRA CIVILIAN"/>
                ''         ...</answers></field></fields></question>
                Dim oQuestionField = nq.Fields.FirstOrDefault(Function(fd) Not String.IsNullOrEmpty(fd.labelText))
                If (nq.TextTemplate.IndexOf("}") = -1 And nq.TextTemplate.IndexOf("{") = -1 And oQuestionField Is Nothing) Then
                    strHtml += "<span class='bold'>" + nq.TextTemplate + "</span>"
                End If
                Dim strRenderField As String = ""
                For Each ofield As CFOMQuestionField In nq.Fields
                    Dim ItemIndex As String = cqIndex.ToString() & divNQPrefix & "_" & tIndex
                    Dim labelText As String = ""
                    If tIndex < strTitle.Length Then
                        labelText = strTitle(tIndex)
                    End If
                    strRenderField += "<div id='txtItem" & ItemIndex & "' >" + IIf(labelText <> "", "<span class='bold'>" + labelText + "</span><span class='require-span'>*</span>", "") + "</div>" + generateNextQuestionFields(ofield, cqIndex, ItemIndex, nq.Name)
                    tIndex += 1
                Next
                strHtml += strRenderField + "</div>"
            End If
            nqIndex += 1
        Next
        Return strHtml
    End Function
    Function generateNextQuestionFields(ByVal field As CFOMQuestionField, ByVal cqIndex As Integer, ByVal itemIndex As String, ByVal nqName As String) As String
        '' get next question name
        Dim sb As StringBuilder = New StringBuilder()
        Dim strAnswerHiddenInput As String = ""

        ' we only have 2 case: dropdown and textbox (lowercase)
        If field.Type.Equals("dropdown") AndAlso field.Answers.Count > 0 Then
            sb.AppendFormat("<select aria-labelledby='txtItem" & itemIndex & "' tabindex='0' class='nq-fom-answer-{0}' id ='NQItem{1}' type ='dropdown' >", cqIndex, itemIndex)
            Dim selectedIndex As Integer = 0
            For Each anw As CFOMQuestionAnswer In field.Answers
                sb.AppendFormat("<option value=""{0}"">{1}</option>", Server.HtmlEncode(anw.Value), Server.HtmlEncode(anw.Text))
                If Not String.IsNullOrEmpty(anw.NextQuestion) Then
					strAnswerHiddenInput += "<input type='hidden' tabindex='-1' id='hdInnerNQ" & itemIndex & "index" & selectedIndex & "' value='" & anw.NextQuestion & "' />"
                End If
                selectedIndex += 1
            Next
            sb.Append("</select>")

        ElseIf field.Type.Equals("textbox") Then
			sb.AppendFormat("<input aria-labelledby='txtItem" & itemIndex & "' tabindex='0' type='text' class='nq-fom-answer-{0}' id='NQItem{1}' type='text' maxlength='100' data-customValidation='{2}'/>", cqIndex, itemIndex, field.CustomValidations)
            Dim sNQAnswer As New List(Of String)
            sNQAnswer = getNQAnswers(nqName)
            If (sNQAnswer.Count > 0) Then
                Dim strNQAnswer As String = ""
                Dim oSerialize As New JavaScriptSerializer()
                strNQAnswer = oSerialize.Serialize(sNQAnswer)
                strAnswerHiddenInput += "<input type='hidden' id='hdAnswer_NQItem" & itemIndex & "' value='" & strNQAnswer & "' />"
            End If
        End If
        Return sb.ToString() + strAnswerHiddenInput
    End Function
    ''next question for radio button type
    Protected Function NextQuestionHiddenInputForCheckBoxField(ByVal obj As Object, ByVal cqIndex As Integer) As String
        Dim cq As CFOMQuestion = obj
        Dim strHtml As String = ""
        Dim sb As StringBuilder = New StringBuilder()
        Dim strNextQuestion = cq.NextQuestion
        If String.IsNullOrEmpty(strNextQuestion) Then
            strNextQuestion = ""
        End If
		sb.AppendFormat("<input type='hidden' tabindex='-1' id='hdNextQuestion{1}' value='{0}' />", strNextQuestion, cqIndex)
        Return sb.ToString()
    End Function
    Function getNextQuestions() As List(Of String)
        Dim nqList As New List(Of String)
        For Each oCQ As CFOMQuestion In _FOMQuestions
            If Not String.IsNullOrEmpty(oCQ.NextQuestion) Then
                If Not nqList.Contains(oCQ.NextQuestion) Then
                    nqList.Add(oCQ.NextQuestion)
                End If

            End If
            For Each ofield As CFOMQuestionField In oCQ.Fields
                For Each ans As CFOMQuestionAnswer In ofield.Answers
                    If Not String.IsNullOrEmpty(ans.NextQuestion) Then
                        If Not nqList.Contains(ans.NextQuestion) Then
                            nqList.Add(ans.NextQuestion)
                        End If

                    End If
                Next
            Next
        Next
        Return nqList
    End Function

    Function getNQAnswers(ByVal oNextQuestion As String) As List(Of String)
        Dim nqAnsList As New List(Of String)
        For Each oNQ As CFOMQuestion In _CFOMQuestionList
            If oNQ.Name = oNextQuestion Then
                For Each oField As CFOMQuestionField In oNQ.Fields
                    If oField.Type.ToLower = "textbox" And oField.Answers.Count > 0 Then
                        For Each ans As CFOMQuestionAnswer In oField.Answers
                            nqAnsList.Add(ans.Text)
                        Next
                    End If
                Next
            End If
        Next
        Return nqAnsList
    End Function
    ''-----------implement inner next question---------------
    Function getNextQuestionList1() As List(Of CFOMQuestion)
        ''the getNetQuestionList1() contains all the  FOM questions in the <questions> xml node at second level (exclude the next question that has next_question attribute in the answer fields).
        ''example downloading xml FOM questions (lenderref=SFPCU_DEMO) 
        ''<starting_questions>
        ''      <question name = "EligibilityLawEnforcement" text_template="I belong to a law enforcement agency/association in the State of California" next_question="Agency"> </question>
        ''</starting_questions>
        ''<questions>
        ''      <question name = "Agency" text_template="I'm a(n) {0} {1} Please enter your employer's or association's name {2}">   <fields> </question>
        ''<questions>
        Dim currentNQList As New List(Of CFOMQuestion)
        For Each nq In _CFOMQuestionList
            Dim oNQ = getNextQuestionList2().FirstOrDefault(Function(q) q.Name = nq.Name)
            If oNQ IsNot Nothing Then Continue For
            If Not currentNQList.Contains(nq) Then
                currentNQList.Add(nq)
            End If
        Next
        Return currentNQList
    End Function
    Function getNextQuestionList2() As List(Of CFOMQuestion)
        ''the getNetQuestionList2() contains all the  FOM questions in the <questions> xml node at third level (exclude the next questions that are in the getNextQuestionList1()).
        ''example downloading xml FOM questions (lenderref=SFPCU_DEMO) 
        ''<starting_questions>
        ''       <question name="EligibilityEMT" text_template="I'm an employee/volunteer of an eligible EMT company or EMT department in the Bay Area" next_question="EMTCounty"> </question>
        ''</starting_questions>
        ''<questions>
        ''<question name = "EMTCounty" text_template="In which county: {0}">
        ''      <fields>
        ''      <field type = "dropdown" label_text="StatusType">
        ''            <answers>
        ''            <answer text = "Please select" value="" />
        ''            <answer text = "Alameda" value="Alameda" next_question="AlamedaEMTSEG" />
        ''            <answer text = "Contra Costa" value="Contra Costa" next_question="ContraCostaEMTSEG" />
        ''            <answer text = "Marin" value="Marin" next_question="MarinEMTSEG" />
        ''            <answer text = "Napa" value="Napa" next_question="NapaEMTSEG" />
        ''            <answer text = "San Francisco" value="San Francisco" next_question="SanFranciscoEMTSEG" />
        ''            <answer text = "San Mateo" value="San Mateo" next_question="SanMateoEMTSEG" />
        ''            <answer text = "Santa Clara" value="Santa Clara" next_question="SantaClaraEMTSEG" />
        ''            <answer text = "Solano" value="Solano" next_question="SolanoEMTSEG" />
        ''            <answer text = "Sonoma" value="Sonoma" next_question="SonomaEMTSEG" />
        ''          </answers>
        ''        </field>
        ''      </fields>
        ''    </question>
        ''<questions>
        Dim currentNQList As New List(Of CFOMQuestion)
        For Each nq In _CFOMQuestionList
            Dim hasNextQuestion As Boolean = False
            For Each field In nq.Fields
                Dim sNQAns = field.Answers.FirstOrDefault(Function(ans) Not String.IsNullOrWhiteSpace(ans.NextQuestion))
                If sNQAns IsNot Nothing Then
                    hasNextQuestion = True
                    Exit For
                End If
            Next
            If (hasNextQuestion) Then
                If Not currentNQList.Contains(nq) Then
                    currentNQList.Add(nq)
                End If
            End If
        Next
        Return currentNQList
    End Function
    Function getInnerNextQuestionDic() As Dictionary(Of String, String)
        Dim currentNQDic As New Dictionary(Of String, String)
        Dim cqIndex As Integer = 0
        For Each cq In _FOMQuestions
            If cq.Fields.Count > 0 Then
                '' the FOM question has field node, and it has next_question attribute in the answer nodes        
                '' example downloading xml FOM questions (lenderref=AAFCU_DEMO) 
                '' <starting_questions>
                ''      <question name = "0" text_template="Employee of American Airlines {0}">
                ''      <fields>
                ''          <field type = "dropdown" >
                ''              <answers>
                ''                  <answer text="-- Select --" value=""/>
                ''                  <answer text="American Airlines" value="American Airlines" next_question="AEOther1"/>
                ''                  <answer text="Envoy/American Eagle" value="Envoy/American Eagle" next_question="AEOther1"/>
                ''                  <answer text="Piedmont" value="Piedmont" next_question="AEOther1"/>
                ''                  <answer text="PSA Airlines" value="PSA Airlines" next_question="AEOther1"/>
                ''              </answers>
                ''            </field>
                ''  </fields>
                ''</starting_questions>
                For Each cqField In cq.Fields
                    Dim selectedIndex As Integer = 0
                    For Each ans In cqField.Answers
                        If Not String.IsNullOrEmpty(ans.NextQuestion) Then
                            Dim oNQ = _CFOMQuestionList.FirstOrDefault(Function(q) q.Name = ans.NextQuestion)
                            If oNQ IsNot Nothing Then
                                Dim sNQKey = cqIndex & "_" & selectedIndex
                                If Not currentNQDic.ContainsKey(sNQKey) Then
                                    currentNQDic.Add(sNQKey, oNQ.Name)
                                End If
                            End If
                        End If
                        selectedIndex += 1
                    Next
                Next
            Else
                '' the FOM question only has next_question attribute in the <question> node         
                '' example downloading xml FOM questions (lenderref=SFPCU_DEMO) 
                '' <starting_questions>
                ''      <question name = "EligibilityLawEnforcement" text_template="I belong to a law enforcement agency/association in the State of California" next_question="Agency">
                ''      <supported_special_account_types>
                ''      <supported_special_account_type>Minor</supported_special_account_type>
                ''      </supported_special_account_types>
                ''      </question>
                ''</starting_questions>
                Dim oNQ = _CFOMQuestionList.FirstOrDefault(Function(q) q.Name = cq.NextQuestion)
                If oNQ IsNot Nothing Then
                    Dim sNQKey = cqIndex & "_"
                    If Not currentNQDic.ContainsKey(sNQKey) Then
                        currentNQDic.Add(sNQKey, oNQ.Name)
                    End If
                End If
            End If
            cqIndex += 1
        Next
        Return currentNQDic
    End Function
    Function RenderingInnerNextQuestionFields(ByVal obj As Object, ByVal cqIndex As Integer) As String
        Dim cq As CFOMQuestion = obj
        Dim strHtml As String = ""
        Dim hasNQName As Boolean = False
        Dim nqIndex As Integer = 0
        Dim strTitle As String()
        Dim fieldCount As Integer
        Dim tIndex As Integer
        '' need to get all the value and next question attribute using dictionary
        Dim dicNextQuestions As New Dictionary(Of String, String)
        ''add next question attribute if it exist in question node
        If Not String.IsNullOrEmpty(cq.NextQuestion) Then
            If Not dicNextQuestions.ContainsKey(cq.NextQuestion) Then
                dicNextQuestions.Add(cq.NextQuestion, cq.NextQuestion)
            End If
        End If
        '' add all next question attribute in answers node 
        For Each ofield As CFOMQuestionField In cq.Fields
            For Each oAns As CFOMQuestionAnswer In ofield.Answers
                If (Not String.IsNullOrEmpty(oAns.NextQuestion)) Then
                    If Not dicNextQuestions.ContainsKey(oAns.NextQuestion) Then
                        dicNextQuestions.Add(oAns.NextQuestion, oAns.Value)
                    End If
                End If
            Next
        Next
        ''rendering input fields for all next question in dicNextQuestion
        For Each nq2 As CFOMQuestion In getNextQuestionList2()
            If (dicNextQuestions.ContainsKey(nq2.Name)) Then
                ''now get inner next question
                Dim index As Integer = 0
                For Each nqField In nq2.Fields
                    For Each ans In nqField.Answers
                        If Not String.IsNullOrEmpty(ans.NextQuestion) Then
                            For Each nq In getNextQuestionList1()
                                If (ans.NextQuestion = nq.Name) Then
                                    ''render html for inner next question
                                    Dim divNQPrefix As String = nq2.Name.Replace(" ", "_")
                                    strTitle = getTextTemplate(nq)
                                    fieldCount = getFieldCount(nq)
                                    Dim textTemplate As String = nq.TextTemplate
                                    '' textTemplate = Regex.Replace(textTemplate, "<.*?>", String.Empty)
                                    textTemplate = Regex.Replace(textTemplate, "<(?!a|/a).*?>", String.Empty)
                                    Dim isFound As Boolean = False
                                    Dim Items As Integer = 0
                                    Dim sb As New StringBuilder()
                                    sb.AppendFormat("<div id ='divNextQuestion" & cqIndex.ToString() & divNQPrefix & "_" & nq.Name & "' style='display:none' class='NextQuestion' name=""{1}"" fieldCount='{2}' text_template =""{3}"" selectedValue=""{4}"">", cqIndex, nq.Name, fieldCount, textTemplate, divNQPrefix)
                                    strHtml += sb.ToString()
                                    tIndex = 0
                                    If (nq.Fields.Count = 0) Then ''question only has text template
                                        strHtml += textTemplate
                                    End If
                                    Dim strRenderField As String = ""
                                    For Each ofield As CFOMQuestionField In nq.Fields

                                        Dim labelText As String = ""
                                        If tIndex < strTitle.Length Then
                                            labelText = strTitle(tIndex)
                                        End If

                                        Dim ItemIndex As String = cqIndex.ToString() & divNQPrefix & "_" & tIndex & "_" & nq.Name
                                        strRenderField += "<div id='txtItem" & ItemIndex & "' ><span class='bold'>" + labelText + "</span><span class='require-span'>*</span></div>" + generateNextQuestionFields(ofield, cqIndex, ItemIndex, nq.Name)
                                        tIndex += 1
                                    Next
                                    strHtml += strRenderField + "</div>"
                                    Exit For
                                End If
                            Next
                        End If
                    Next
                Next
            End If
            nqIndex += 1
        Next
        Return strHtml
    End Function
    ''-----------implement inner next question---------------
End Class
