﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="ReferenceInfo.ascx.vb" Inherits="Inc_MainApp_ReferenceInfo" %>
<div id="<%=IDPrefix%>divReferenceInformation" <%=IIf(IsInMode("777") AndAlso IsInFeature("visibility"), "class=""showfield-section"" data-show-field-section-id=""" & BuildShowFieldSectionID("divReferenceInformation") & """ data-section-name= '" & IIf(String.IsNullOrEmpty(IDPrefix), "", "Co-App ") & " Reference Information'", "")%>>
	<div style="margin-top: 10px;" class="hide-able" id="<%=IDPrefix%>divShowReferenceInfoLink">
		<a id="<%=IDPrefix%>showReferenceInfo" href="#" data-mode="self-handle-event" status="N" style="cursor: pointer; font-weight: bold;" class="header_theme2 shadow-btn plus-circle-before">Add a reference</a>
	</div>

	<div id="<%=IDPrefix%>divReferenceInfoPanel" style="display: none;">
		<div style="font-size: 17px; font-weight: bold; padding-top: 15px">Reference Information</div>
		<div data-role="fieldcontain">
			<label for="<%=IDPrefix%>refFirstName">First Name</label>
			<input type="text" id="<%=IDPrefix%>refFirstName" />
		</div>
		<div data-role="fieldcontain">
			<label for="<%=IDPrefix%>refLastName">Last Name</label>
			<input type="text" id="<%=IDPrefix%>refLastName" />
		</div>
		<div data-role="fieldcontain">
			<label for="<%=IDPrefix%>refEmail">Email</label>
			<input type="email" id="<%=IDPrefix%>refEmail" maxlength="50" />
		</div>
		<div data-role="fieldcontain">
			<div>
				<div style="display: inline-block;"><label for="<%=IDPrefix%>refPhone" style="white-space: nowrap;">Phone</label></div>
				<div style="display: inline-block;"><label class="phone_format rename-able" style="white-space: nowrap; font-style: italic;">(xxx) xxx-xxxx</label></div>
			</div>
			<input type="<%=TextAndroidTel%>" pattern="[0-9]*" name="phone" id="<%=IDPrefix%>refPhone" class="inphone" maxlength='14' />
		</div>
		<div data-role="fieldcontain">
			<label for="<%=IDPrefix%>refRelationship">Relationship</label>
			<input type="text" id="<%=IDPrefix%>refRelationship" maxlength="20" />
		</div>
	</div>
</div>
<script>
	function <%=IDPrefix%>ValidateReferenceInfo() {
		if ($("#<%=IDPrefix%>divReferenceInformation.showfield-section").length > 0 && $("#<%=IDPrefix%>divReferenceInformation.showfield-section").hasClass("hidden")) return true;
		return $.lpqValidate("<%=IDPrefix%>ValidateReferenceInfo");
	}

    function <%=IDPrefix%>registerReferenceInfoValidator() {

		$('#<%=IDPrefix%>refEmail').observer({
    		validators: [
				function (partial) {
					if ($("#<%=IDPrefix%>showReferenceInfo").attr('status') != "Y") {
                    		return ""; //skip validate reference info
                    	}
                    	var email = $(this).val();
                    	if (email != "") {
                    		if (Common.ValidateEmail(email) == false) {
                    			return "Valid Email is required";
                    		}
                    	}
                    	return "";
                    }
                ],
            	validateOnBlur: true,
            	group: "<%=IDPrefix%>ValidateReferenceInfo"
            });

    	//validate phone
			$('#<%=IDPrefix%>refPhone').observer({
    		validators: [
				function (partial) {
					if ($("#<%=IDPrefix%>showReferenceInfo").attr('status') != "Y") {
                    		return ""; //skip validate reference info
                    	}
                    	var refPhone = $(this).val();
                    	if (refPhone != "") {
                    		if (!is_foreign_phone) {
                    			if (Common.ValidatePhone(refPhone)) {
                    				//validate first digit of home phone : 0 and 1 is invalid
                    				var firstDigit = refPhone.replace("(", "").substring(0, 1);
                    				if (firstDigit == 0 || firstDigit == 1) {
                    					return 'The area code can not begin with 0 or 1.';
                    				}
                    			} else {
                    				return 'Valid phone number is required';
                    			}
                    		}
                    	}
                    	return "";
                    }
                ],
            	validateOnBlur: true,
            	group: "<%=IDPrefix%>ValidateApplicantContactInfoXA"
            });

		}
		<%=IDPrefix%>registerReferenceInfoValidator();

	function  <%=IDPrefix%>setReferenceInfo(appInfo) {
		if ($("#<%=IDPrefix%>divReferenceInformation.showfield-section").length > 0 && $("#<%=IDPrefix%>divReferenceInformation.showfield-section").hasClass("hidden")) {
			return appInfo;
		}
		if ($('#<%=IDPrefix%>showReferenceInfo').attr('status') == 'Y') {
    		var referenceInfo = [];
    		referenceInfo.push('ref_first_name|' + $('#<%=IDPrefix%>refFirstName').val());
            referenceInfo.push('ref_last_name|' + $('#<%=IDPrefix%>refLastName').val());
        	referenceInfo.push('ref_email|' + $('#<%=IDPrefix%>refEmail').val());
        	referenceInfo.push('ref_phone|' + $('#<%=IDPrefix%>refPhone').val());
        	referenceInfo.push('ref_relationship|' + $('#<%=IDPrefix%>refRelationship').val());
        	appInfo.<%=IDPrefix%>referenceInfo = JSON.stringify(referenceInfo);
        }
	}
	function <%=IDPrefix%>ViewReferenceInfo() {
		var strHtml = "";
		$(this).hide();
		$(this).prev("div.row").hide();
		if ($("#<%=IDPrefix%>divReferenceInformation.showfield-section").length > 0 && $("#<%=IDPrefix%>divReferenceInformation.showfield-section").hasClass("hidden")) {
			return "";
		}
		if ($('#<%=IDPrefix%>showReferenceInfo').attr('status') == 'Y') {
        	var firstName = $('#<%=IDPrefix%>refFirstName').val();
        	var lastName = $('#<%=IDPrefix%>refLastName').val();
        	var email = $('#<%=IDPrefix%>refEmail').val();
        	var phone = $('#<%=IDPrefix%>refPhone').val();
        	var relationship = $('#<%=IDPrefix%>refRelationship').val();
        	if (firstName != "") {
        		strHtml += '<div class="col-sm-6 col-xs-12"><div class="row"><div class="col-xs-6 text-right row-title"><span class="bold">First Name</span></div><div class="col-xs-6 text-left row-data"><span>' + firstName + '</span></div></div></div>';
        	}
        	if (lastName != "") {
        		strHtml += '<div class="col-sm-6 col-xs-12"><div class="row"><div class="col-xs-6 text-right row-title"><span class="bold">Last Name</span></div><div class="col-xs-6 text-left row-data"><span>' + lastName + '</span></div></div></div>';
        	}
        	if (email != "") {
        		strHtml += '<div class="col-sm-6 col-xs-12"><div class="row"><div class="col-xs-6 text-right row-title"><span class="bold">Email</span></div><div class="col-xs-6 text-left row-data"><span>' + email + '</span></div></div></div>';
        	}
        	if (phone != "") {
        		strHtml += '<div class="col-sm-6 col-xs-12"><div class="row"><div class="col-xs-6 text-right row-title"><span class="bold">Phone</span></div><div class="col-xs-6 text-left row-data"><span>' + phone + '</span></div></div></div>';
        	}
        	if (relationship != "") {
        		strHtml += '<div class="col-sm-6 col-xs-12"><div class="row"><div class="col-xs-6 text-right row-title"><span class="bold">Relationship</span></div><div class="col-xs-6 text-left row-data"><span>' + relationship + '</span></div></div></div>';
        	}
        	if (strHtml != "") {
        		$(this).show();
        		$(this).prev("div.row").show();
        	}
        }

		return strHtml;
	}
	$(function () {
		$('#<%=IDPrefix%>showReferenceInfo').on('click', function (e) {
			var $self = $(this);
			var selectedValue = $self.attr('status');
			var RefElem = $('#<%=IDPrefix%>divReferenceInfoPanel');
			if (selectedValue == "N") {
				$self.html('Remove reference');
				$self.addClass("minus-circle-before").removeClass("plus-circle-before");
				$self.attr('status', 'Y');
				RefElem.show();
			} else {
				$self.html("Add a reference");
				$self.addClass("plus-circle-before").removeClass("minus-circle-before");
				$self.attr('status', 'N');
				RefElem.hide();
			}
			if (BUTTONLABELLIST != null) {
				var value = BUTTONLABELLIST[$.trim($self.html()).toLowerCase()];
				if (typeof value == "string" && $.trim(value) !== "") {
					$self.html(value);
				}
			}
		    e.preventDefault();

	    });
    });

</script>
