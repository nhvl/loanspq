﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="CunaProtectionInfo.ascx.vb" Inherits="Inc_MainApp_CunaProtectionInfo" %>
<style>
    
    .chkProtectionInfo
    {
       margin:0px 1% 0px 0px;
       padding-left: 0px;
       padding-right: 0px;
       float: left;  
       width:49%;
    }
    .chkLabel
    {
        padding: 10px 14px 10px 40px;
    }
   
    #divProtectionInfo .Title, #heloc_divProtectionInfo .Title
    {
       margin-top :30px;
       padding-bottom :0px;
       margin-bottom:0px;	   
    }
</style>
<div id="<%=_helocPrefix%>divProtectionInfo">
       <%=HeaderUtils.RenderPageTitle(24, "Protection that Matters", True)%>  
    <div data-role="fieldcontain">
        <div id="<%=_helocPrefix%>divCreditLife" class="ProtectionInfo"> 
            <div class ="Title">
                <p><span class="bold">Credit Life and Credit Disability Insurance</span>, underwritten by CMFG Life Insurance Company, is available for most loan products</p>        
               <span> Would you like more information about protecting your loan payment or balance with <span class="bold">Credit Life and Credit Disability Insurance</span> from your lender? </span>	               
            </div>   
            <div class="container">
                <div class="row" >
                    <div class="chkProtectionInfo">
                        <div class="ui-checkbox">
						    <label for="<%=_helocPrefix%>chkInterest_CreditLife" class="ui-btn ui-corner-all ui-btn-inherit ui-btn-icon-left ui-checkbox-off chkLabel">Interested</label>
		                    <input type="checkbox" name="checkbox-enhanced" id="<%=_helocPrefix%>chkInterest_CreditLife" data-enhanced="true"/>
				     </div></div>
                    <div class="chkProtectionInfo">
                        <div class="ui-checkbox">
						    <label for="<%=_helocPrefix%>chkNotNow_CreditLife" class="ui-btn ui-corner-all ui-btn-inherit ui-btn-icon-left ui-checkbox-off chkLabel" >Not Now</label>
		                    <input type="checkbox" name="checkbox-enhanced" id="<%=_helocPrefix%>chkNotNow_CreditLife" data-enhanced="true"/>
				     </div></div>
           
                 </div>
            </div>
        </div>
        <!--Guaranteed asset -->
        <div id="<%=_helocPrefix%>divDebt" class="ProtectionInfo">
            <div class="Title">
                <p><span class="bold">Debt Protection</span> is available with most loans by the credit union.  It is designed to cancel your loan payment or balance, up to the contract maximums, if a protected life event occurs.  Available package options will include one or more of the following life events: Life, Disability or involuntary unemployment. </p>
               <span> Would you like more information about protecting your loan payment or balance with <span class="bold">Debt Protection</span> from your lender?</span>
            </div>    
            <div class="container">
                <div class="row" >
                    <div class="chkProtectionInfo">
                        <div class="ui-checkbox">
						    <label for="<%=_helocPrefix%>chkInterest_Debt" class="ui-btn ui-corner-all ui-btn-inherit ui-btn-icon-left ui-checkbox-off chkLabel" >Interested</label>
		                    <input type="checkbox" name="checkbox-enhanced" id="<%=_helocPrefix%>chkInterest_Debt" data-enhanced="true" />
				     </div></div>
                    <div class="chkProtectionInfo" >
                        <div class="ui-checkbox">
						    <label for="<%=_helocPrefix%>chkNotNow_Debt" class="ui-btn ui-corner-all ui-btn-inherit ui-btn-icon-left ui-checkbox-off chkLabel">Not Now</label>
		                    <input type="checkbox" name="checkbox-enhanced" id="<%=_helocPrefix%>chkNotNow_Debt" data-enhanced="true"/>
				     </div></div>
                  </div>
           </div>
        </div>
        <!--Guaranteed Asset Protection -->
        <div id="<%=_helocPrefix%>divGAP" class="ProtectionInfo">
            <div class="Title">
                <p><span class="bold">MEMBER’S CHOICE™ Guaranteed Asset Protection</span> (GAP) is an optional protection product made available by the credit union. In the event of a total loss, GAP is designed to cancel the difference between your primary insurance settlement and the outstanding loan balance, up to the contract maximums. For more information, call or visit the credit union.</p>
               <span> Would you like more information about protecting your loan with <span class="bold">GAP</span> from your lender?</span>
            </div>
            <div class="container">
                <div class="row" >
                    <div class="chkProtectionInfo">
                        <div class="ui-checkbox">
						    <label for="<%=_helocPrefix%>chkInterest_GAP" class="ui-btn ui-corner-all ui-btn-inherit ui-btn-icon-left ui-checkbox-off chkLabel" >Interested</label>
		                    <input type="checkbox" name="checkbox-enhanced" id="<%=_helocPrefix%>chkInterest_GAP" data-enhanced="true"/>
				     </div></div>
                    <div class="chkProtectionInfo" >
                        <div class="ui-checkbox">
						    <label for="<%=_helocPrefix%>chkNotNow_GAP" class="ui-btn ui-corner-all ui-btn-inherit ui-btn-icon-left ui-checkbox-off chkLabel">Not Now</label>
		                    <input type="checkbox" name="checkbox-enhanced" id="<%=_helocPrefix%>chkNotNow_GAP" data-enhanced="true"/>
				     </div></div>
                </div>
            </div>
        </div>
        <!--Mechanical Repair -->
        <div id="<%=_helocPrefix%>divMRC" class="ProtectionInfo">
             <div class="Title">
                 <p><span class="bold">Mechanical Repair Coverage</span> can help you limit the cost of covered vehicle breakdowns. </p>
                <span> Would you like more information about <span class="bold">Mechanical Repair Coverage</span> from your lender?</span>
             </div>
             <div  class="container">
                <div class="row" >
                    <div class="chkProtectionInfo">
                        <div class="ui-checkbox">
						    <label for="<%=_helocPrefix%>chkInterest_MRC" class="ui-btn ui-corner-all ui-btn-inherit ui-btn-icon-left ui-checkbox-off chkLabel" >Interested</label>
		                    <input type="checkbox" name="checkbox-enhanced" id="<%=_helocPrefix%>chkInterest_MRC" data-enhanced="true"/>
				     </div></div>
                    <div  class="chkProtectionInfo">
                        <div class="ui-checkbox">
						    <label for="<%=_helocPrefix%>chkNotNow_MRC" class="ui-btn ui-corner-all ui-btn-inherit ui-btn-icon-left ui-checkbox-off chkLabel" >Not Now</label>
		                    <input type="checkbox" name="checkbox-enhanced" id="<%=_helocPrefix%>chkNotNow_MRC" data-enhanced="true"/>
				     </div></div>
                 </div>
            </div>
         </div>
   </div>     
</div>
<input type="hidden" id="<%=_helocPrefix%>hdCunaProtectionAll" value="<%=_cunaProtectionAll%>" />
<input type="hidden" id="<%=_helocPrefix%>hdCunaCreditLife" value="<%=_cunaCreditLife%>" />
<input type="hidden" id="<%=_helocPrefix%>hdCunaDebt" value="<%=_cunaDebt%>" />
<input type="hidden" id="<%=_helocPrefix%>hdCunaGAP" value="<%=_cunaGAP%>" />
<input type="hidden" id="<%=_helocPrefix%>hdCunaMRC" value="<%=_cunaMRC%>" />    
  

<script>
    $(function () {
        <%=_helocPrefix%>handledShowAndHideProtectionInfo();
     
        $("#<%=_helocPrefix%>divCreditLife input[type='checkbox']").on('click', function () {
            <%=_helocPrefix%>handleToggleCheckBox("<%=_helocPrefix%>divCreditLife", $(this));
        });

        $("#<%=_helocPrefix%>divDebt input[type='checkbox']").on('click', function () {
            <%=_helocPrefix%>handleToggleCheckBox("<%=_helocPrefix%>divDebt", $(this));
        });
  
        $("#<%=_helocPrefix%>divGAP input[type='checkbox']").on('change', function () {      
            <%=_helocPrefix%>handleToggleCheckBox("<%=_helocPrefix%>divGAP", $(this));    
        });

        $("#<%=_helocPrefix%>divMRC input[type='checkbox']").on('click', function () {
            <%=_helocPrefix%>handleToggleCheckBox("<%=_helocPrefix%>divMRC", $(this));
        });
        
    });

    function <%=_helocPrefix%>handleToggleCheckBox(id, chkElement) {
        var chkIDs = [];
        $('#'+id+ ' input[type="checkbox"]').each(function () {
            chkIDs.push($(this).attr('id'));
        });

        if (chkElement.is(':checked')) {
            if (chkElement.attr('id') == chkIDs[0]) {
                $("#" + chkIDs[1]).prop("checked", false).checkboxradio("refresh");
            } else {
                $("#" + chkIDs[0]).prop("checked", false).checkboxradio("refresh");
            }
        } 
    }
  
    function <%=_helocPrefix%>handledShowAndHideProtectionInfo() {
        var creditLifeEle = $('#<%=_helocPrefix%>divCreditLife');
        var debtEle = $('#<%=_helocPrefix%>divDebt');
        var gapEle = $('#<%=_helocPrefix%>divGAP');
        var mrcEle = $('#<%=_helocPrefix%>divMRC');
        var visibleProtectionInfo = false;
        var showCunaCreditLife = $('#<%=_helocPrefix%>hdCunaCreditLife').val();
        var showCunaDebt = $('#<%=_helocPrefix%>hdCunaDebt').val();
        var showCunaGap = $('#<%=_helocPrefix%>hdCunaGAP').val();
        var showMRC = $('#<%=_helocPrefix%>hdCunaMRC').val();
        //hide all Protection infor
        creditLifeEle.hide();
        debtEle.hide();
        gapEle.hide();
        mrcEle.hide();
        if (<%=_helocPrefix%>overrideProtection()) {
            if (showCunaCreditLife == 'Y') {
                creditLifeEle.show();
                visibleProtectionInfo = true;
            }
            if (showCunaDebt == 'Y') {
                debtEle.show();
                visibleProtectionInfo = true;
            }
            if (showCunaGap == 'Y') {
                gapEle.show();
                visibleProtectionInfo = true;
            }
            if (showMRC == 'Y') {
                mrcEle.show();
                visibleProtectionInfo = true;
            }
        } else {
            if ($('#<%=_helocPrefix%>hdCunaProtectionAll').val() == "Y") {     
                if (showCunaCreditLife != "N") {
                    creditLifeEle.show();
                    visibleProtectionInfo = true;
                }
                if (showCunaDebt != "N") {
                    debtEle.show();
                    visibleProtectionInfo = true;
                }             
                if ('<%=LoanType %>' == 'VL') {
                    if (showCunaGap != 'N') {
                        gapEle.show();
                        visibleProtectionInfo = true;
                    }
                    if (showMRC != 'N') {
                        mrcEle.show();
                        visibleProtectionInfo = true;
                    }
                 }
            }
        }
        if (!visibleProtectionInfo) {//hide protectionfor header
            $('#<%=_helocPrefix%>divProtectionInfo .sub_section_header').hide();
        }
    }
    function <%=_helocPrefix%>overrideProtection() { 
        return ($('#<%=_helocPrefix%>hdCunaCreditLife').val() == 'Y' || $('#<%=_helocPrefix%>hdCunaDebt').val() == 'Y' || $('#<%=_helocPrefix%>hdCunaGAP').val() == 'Y' || $('#<%=_helocPrefix%>hdCunaMRC').val() == 'Y');
    }
    function setProtectionInfo(appInfor, isHELOC) {
        var ProtectionAnswers = [];
             var element = $('#divProtectionInfo .ProtectionInfo');

            if (isHELOC) {
                element = $('#heloc_divProtectionInfo .ProtectionInfo');
            }

            element.each(function () {
                var currElem = $(this);            
                if (currElem.attr('style') != undefined && currElem.attr('style').indexOf('none') == -1) {
                    var divChkElem = currElem.find('div[class~="chkProtectionInfo"]');
                    var divQuestionElem = currElem.find('div[class~="Title"]');
                    var selectedChkText = "";

                    divChkElem.each(function () {
                        var chkElem = $(this).find('input[type="checkbox"]');
                        if (chkElem.is(':checked')) {
                            selectedChkText = chkElem.prev('label').text();
                            return false;
                        }
                    });
               
                    if (selectedChkText != "") {
                        ProtectionAnswers.push(htmlEncode(divQuestionElem.find('span').text() + ": " + selectedChkText));
                    }             
                }
            });
        appInfor.cunaQuestionAnswers = JSON.stringify(ProtectionAnswers);
    }
  
    /*
    function hiddenAllProtection() { //some loans do not want to have any protection info
        return ($('#hdCunaCreditLife').val() == 'N' && $('#hdCunaDebt').val() == 'N' && $('#hdCunaGAP').val() == 'N' && $('#hdCunaMRC').val() == 'N');
    }*/
</script>