﻿
Imports System.Activities.Expressions

Partial Class Inc_MainApp_xaFundingOptions
    Inherits CBaseUserControl
    Private _fundingTypeList As List(Of String)
	Private _CCMaxFunding As String
	Private _ACHMaxFunding As String
	Private _PaypalMaxFunding As String
    Private _FundingDisclosure As String
    Public Property FundingTypeList() As List(Of String)
        Get
            Return _fundingTypeList
        End Get
        Set(value As List(Of String))
            _fundingTypeList = value
        End Set
    End Property

	Public Property CCMaxFunding() As String
		Get
			Return _CCMaxFunding
		End Get
		Set(value As String)
			_CCMaxFunding = value
		End Set
	End Property

	Public Property ACHMaxFunding() As String
		Get
			Return _ACHMaxFunding
		End Get
		Set(value As String)
			_ACHMaxFunding = value
		End Set
	End Property

	Public Property PaypalMaxFunding() As String
		Get
			Return _PaypalMaxFunding
		End Get
		Set(value As String)
			_PaypalMaxFunding = value
		End Set
	End Property
    Public Property FundingDisclosure() As String
        Get
            Return _FundingDisclosure
        End Get
        Set(value As String)
            _FundingDisclosure = value
        End Set
    End Property
	Protected Function DialogPage(fundType As String) As String
		Select Case fundType
			Case "CREDIT CARD"
				Return "dialog-page-id='fundingCreditCard'"
			Case "INTERNAL TRANSFER"
				Return "dialog-page-id='fundingInternalTransfer'"
			Case "TRANSFER FROM ANOTHER FINANCIAL INSTITUTION"
				Return "dialog-page-id='fundingFinancialIns'"
			Case Else
				Return ""
		End Select
	End Function

End Class
