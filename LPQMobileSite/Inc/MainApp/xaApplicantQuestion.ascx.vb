﻿Imports LPQMobile.BO
Imports System.Xml
Imports LPQMobile.Utils
Imports System.Web.Script.Serialization
Partial Class Inc_MainApp_xaApplicantQuestion
	Inherits CBaseUserControl
	Public Property Header As String
	Public Property CQLocation As CustomQuestionLocation
	Protected _renderedApplicantQuestion As String = ""
	Protected _divApplicantQuestionID As String = ""
	Protected _strAQHtmlHE As String = ""
	Protected _strAQHtmlHELOC As String = ""
	Private _log As log4net.ILog = log4net.LogManager.GetLogger(Me.GetType)

    Public nAvailability As String = ""
    Private _parent As CBasePage
	Protected _specialAccountTypes As New Dictionary(Of String, List(Of String))
	Public Property isBusinessLoan As Boolean = False
	Public Property SpecialAccountName As String = ""
	Protected Property _URLParaCustomQuestions As List(Of CApplicantQA)
	Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
		'If Not IsPostBack Then
		_parent = TryCast(Me.Page, CBasePage)

        Dim currentApplicantQuestions As New List(Of CCustomQuestionXA)
        Dim sUrlParaType = Common.SafeString(Request.QueryString("type"))
        ''nAvailability is only for XA app
        If LoanType = "XA" Then
            nAvailability = IIf(String.IsNullOrWhiteSpace(sUrlParaType), "1", sUrlParaType)
        End If
        ''check combo mode: combo app condition: if type=1 and also loantype is not XA 
        isComboMode = sUrlParaType = "1" AndAlso LoanType <> "XA"
        ' ''allow questions with new api
        _divApplicantQuestionID = IDPrefix & "divApplicantQuestion"
		If LoanType = "HELOC" Then
			_divApplicantQuestionID = _divApplicantQuestionID & "HELOC"
		End If

        currentApplicantQuestions = filteredCustomQuestions(isComboMode, LoanType, CQLocation)

        _renderedApplicantQuestion = Server.HtmlDecode(BuildApplicantQuestionXAs(IDPrefix, currentApplicantQuestions))
        If LoanType = "HE" Then
			_strAQHtmlHE = New JavaScriptSerializer().Serialize(_renderedApplicantQuestion)
		ElseIf LoanType = "HELOC" Then
			_strAQHtmlHELOC = New JavaScriptSerializer().Serialize(_renderedApplicantQuestion)
		End If
		If String.IsNullOrEmpty(Header) AndAlso Me.CQLocation = CustomQuestionLocation.ApplicantPage Then
			Header = HeaderUtils.RenderPageTitle(0, IIf(LoanType = "HELOC", "", "Additional Information").ToString(), True)
		End If
		'End If
	End Sub

    ''' <summary>
    ''' Renders the given <paramref name="oApplicantQuestions"/> into an HTML string to be rendered in this control.
    ''' </summary>
    ''' <param name="coPrefix"></param>
    ''' <param name="oApplicantQuestions"></param>	
    ''' <returns></returns>
    Public Function BuildApplicantQuestionXAs(ByVal coPrefix As String, ByVal oApplicantQuestions As List(Of CCustomQuestionXA)) As String
        Dim htmlControl As New StringBuilder()
        '' check the oApplicantQuestions exist or not
        If oApplicantQuestions.Count = 0 Then
            Return ""  ''return empty string -->there is no applicant questions
        End If

        Dim sTitle As String = "<div id=""" & coPrefix & "ApplicantQuestion_{1}"" is_required =""{2}"" class=""Title labeltext-bold"" data-question-text=""{3}"">{0}</div>"

        Dim sTexttemp As String = "" ' Set below

        Dim sDropdown As String = "" ' Set below

        Dim sOption As String = "<option value=""{0}"">{1}</option>"
        Dim sCheck As String =
         "<input type=""checkbox"" container=""" & coPrefix & "ApplicantQuestion_{5}"" aqindex=""{5}"" chk_aq_length=""{6}"" iname=""{0}"" id=""" & coPrefix & "chk_aq_{1}"" name=""chk_aq_grp_{2}""  answer=""{3}"" class=""" & coPrefix & "ApplicantQuestion"" cq_role=""{7}"" cq_location=""{8}"" cq_type=""{9}"" />" &
         "<label for=""" & coPrefix & "chk_aq_{1}"">{4}</label>"

        Dim controlIndex As Integer = 0
        Dim groupIndex As Integer = 0
        Dim questionIndex As Integer = 1
        Dim isRequired As String = "N"
        'Dim aqTitle As String = "<div class='cqTitle'>Applicant Question(s):</div>"
        'If coPrefix = "co_" Then
        '    aqTitle = "<div class='cqTitle'>Joint Applicant Question(s):</div>"
        'End If

        For Each oItem As CCustomQuestionXA In oApplicantQuestions
            ''removed applicant question index
            Dim preTitle As String = "" ' String.Format("<b>{0}. &nbsp;</b>", questionIndex)
            Dim subTitle As String = ""
            If oItem.IsRequired Then
                subTitle = String.Format("<span class=""require-span"">*</span>")
                isRequired = "Y"
            Else
                isRequired = "N"
            End If
            Dim minLength As String = ""
            Dim maxLength As String = ""
            Dim sGenElementID As String = Common.generateConditionedQuestionElementID(oItem.Name)  ''oItem.Name.Replace(" ", "").Replace("'", "").Replace("/", "")  ''replace single quote, empty string, and forward slash

            ''encode the single quote if it exist in oItem.text 
            oItem.Text = oItem.Text.Replace("'", "&#39;")

            Select Case oItem.AnswerType
                Case "TEXTBOX"
                    Dim extraAttr As String = "class=""" & coPrefix & "ApplicantQuestion"" type=""text"" maxlength=""150""" '' change maxlength to 150

                    If Not String.IsNullOrEmpty(oItem.maxLength) Or Not String.IsNullOrEmpty(oItem.minLength) Then
                        Dim strLengthAttrs As String = "maxlength=""150"""
                        If Not String.IsNullOrEmpty(oItem.maxLength) Then
                            strLengthAttrs = " maxlength=""" & HttpUtility.HtmlAttributeEncode(oItem.maxLength) & """ "
                        End If
                        If Not String.IsNullOrEmpty(oItem.minLength) Then
                            strLengthAttrs += " min_length=""" & HttpUtility.HtmlAttributeEncode(oItem.minLength) & """ "
                        End If
                        extraAttr = "class=""" & coPrefix & "ApplicantQuestion"" type=""text""" & strLengthAttrs
                    End If

                    Select Case oItem.DataType
                        Case "D"
                            extraAttr = "class=""indate " & coPrefix & "ApplicantQuestion"" type=""text""maxlength=""10"""
                        Case "N"
                            extraAttr = "class=""numeric " & coPrefix & "ApplicantQuestion"" type=""text"" maxlength=""10"""
                        Case "C"
                            extraAttr = "class=""numeric " & coPrefix & "ApplicantQuestion"" type=""text"""
                        Case "P"
                            If String.IsNullOrEmpty(oItem.reg_expression) Then
                                If Not String.IsNullOrEmpty(oItem.minLength) Then
                                    minLength = " min_length =""" & HttpUtility.HtmlAttributeEncode(oItem.minLength) & """ "
                                End If
                                If Not String.IsNullOrEmpty(oItem.maxLength) Then
                                    maxLength = " maxlength =""" & HttpUtility.HtmlAttributeEncode(oItem.maxLength) & """ "
                                Else
                                    maxLength = " maxlength =""20"" " ''default maxlength
                                End If
                            End If

                            extraAttr = "class=""" & coPrefix & "ApplicantQuestion"" type=""password""" & minLength & maxLength & " data-error-msg=""" & HttpUtility.HtmlAttributeEncode(oItem.ErrorMessage) & """"

                    End Select
                    ''need to add reg_expression to the textbox if it exist
                    If Not String.IsNullOrEmpty(oItem.reg_expression) Then
                        extraAttr = extraAttr & " reg_expression=""" & HttpUtility.HtmlAttributeEncode(oItem.reg_expression) & """"
                    End If

                    Dim aqPrefix As String = "ap_"
                    If Not String.IsNullOrEmpty(coPrefix) Then
                        aqPrefix = coPrefix + "ap_"
                    End If

                    sTexttemp = "<input aria-labelledby=""" & coPrefix & "ApplicantQuestion_{2}"" aqindex=""{2}"" id=""" & coPrefix & "txt_aq_{3}""" & " iname=""{0}"" {1} cq_role=""{4}"" cq_location=""{5}"" cq_type=""{6}""/>"  'this is a temporary fix for mcu  to remove "/" from id name, doesn't impact backend post
                    htmlControl.AppendFormat("<div class=""chk-grp-wrapper aq-item-wrapper"" qname=""{1}"" id=""" & coPrefix & "ApplicantQuestion_{0}_wrapper"">", questionIndex, HttpUtility.HtmlAttributeEncode(oItem.Name))
                    htmlControl.AppendFormat(sTitle, preTitle + oItem.Text + subTitle, questionIndex, isRequired, HttpUtility.HtmlAttributeEncode(oItem.Text))
                    'htmlControl.AppendFormat(sText, oItem.Name, extraAttr, questionIndex)
                    htmlControl.AppendFormat(
                        sTexttemp,
                        HttpUtility.HtmlAttributeEncode(oItem.Name),
                        extraAttr,
                        questionIndex,
                        HttpUtility.HtmlAttributeEncode(sGenElementID),
                        HttpUtility.HtmlAttributeEncode(oItem.Identifer.Role.ToString()),
                        HttpUtility.HtmlAttributeEncode(Me.CQLocation.ToString()),
                        HttpUtility.HtmlAttributeEncode(oItem.AnswerType))
                    ''add re-enter Password input field, if type is password
                    If oItem.DataType = "P" Then
                        If Not String.IsNullOrEmpty(oItem.ConfirmationText) Then
                            htmlControl.AppendFormat("<div class=""Title"">{0}</div>", oItem.ConfirmationText)
                        Else
                            htmlControl.AppendFormat("<div class=""Title"">Re-enter: {0}</div>", oItem.Text)
                        End If
                        htmlControl.AppendFormat("<input aria-labelledby=""" & coPrefix & "ApplicantQuestion_{3}"" type=""password"" id=""re_" & coPrefix & "txt_aq_{4}"" iname=""re_{0}"" {1} data-confirmation-msg=""{2}"" data-pair-control=""" & coPrefix & "txt_aq_{4}"" />", oItem.Name, maxLength + minLength, HttpUtility.HtmlEncode(oItem.ConfirmationText), questionIndex, sGenElementID)
                    End If
                    htmlControl.AppendLine("</div>")
                Case "DROPDOWN"
                    htmlControl.AppendFormat("<div class=""chk-grp-wrapper aq-item-wrapper"" qname=""{1}"" id=""" & coPrefix & "ApplicantQuestion_{0}_wrapper"">", questionIndex, HttpUtility.HtmlAttributeEncode(oItem.Name))
                    htmlControl.AppendFormat(sTitle, preTitle + HttpUtility.HtmlDecode(oItem.Text) + subTitle, questionIndex, isRequired, HttpUtility.HtmlAttributeEncode(oItem.Text))
                    Dim htmlOption As New StringBuilder()
                    For Each opt As CCustomQuestionOption In oItem.CustomQuestionOptions
                        ' While the display text may have HTML, the value mustb e attribute encoded.
                        htmlOption.AppendFormat(sOption, HttpUtility.HtmlAttributeEncode(opt.Value), HttpUtility.HtmlDecode(opt.Text))
                    Next
                    Dim aqPrefix As String = "ap_"
                    If Not String.IsNullOrEmpty(coPrefix) Then
                        aqPrefix = coPrefix + "ap_"
                    End If

                    sDropdown = "<select aria-labelledby=""" & coPrefix & "ApplicantQuestion_{2}"" aqindex =""{2}"" id=""" & coPrefix & "ddl_aq_{3}"" iname=""{0}""" & " class=""" & coPrefix & "ApplicantQuestion"" cq_role=""{4}"" cq_location=""{5}"" cq_type=""{6}"">{1}</select>"
                    htmlControl.AppendFormat(
                        sDropdown,
                        HttpUtility.HtmlAttributeEncode(oItem.Name),
                        htmlOption.ToString(),
                        questionIndex,
                        HttpUtility.HtmlAttributeEncode(sGenElementID),
                        HttpUtility.HtmlAttributeEncode(oItem.Identifer.Role.ToString()),
                        HttpUtility.HtmlAttributeEncode(Me.CQLocation.ToString()),
                        HttpUtility.HtmlAttributeEncode(oItem.AnswerType))
                    htmlControl.AppendLine("</div>")
                Case "CHECKBOX"
                    htmlControl.AppendFormat("<div class=""chk-grp-wrapper aq-item-wrapper"" qname=""{1}"" id=""" & coPrefix & "ApplicantQuestion_{0}_wrapper"">", questionIndex, HttpUtility.HtmlAttributeEncode(oItem.Name))
                    htmlControl.AppendFormat(sTitle, preTitle + oItem.Text + subTitle, questionIndex, isRequired, HttpUtility.HtmlAttributeEncode(oItem.Text))
                    For Each opt As CCustomQuestionOption In oItem.CustomQuestionOptions
                        ''----replace opt.text to opt.value
                        htmlControl.AppendFormat(
                            sCheck,
                            HttpUtility.HtmlAttributeEncode(oItem.Name),
                            controlIndex,
                            groupIndex,
                            HttpUtility.HtmlAttributeEncode(opt.Value),
                            opt.Text,
                            questionIndex,
                            oItem.CustomQuestionOptions.Count,
                            HttpUtility.HtmlAttributeEncode(oItem.Identifer.Role.ToString()),
                            HttpUtility.HtmlAttributeEncode(Me.CQLocation.ToString()),
                            HttpUtility.HtmlAttributeEncode(oItem.AnswerType))
                        controlIndex += 1
                    Next
                    htmlControl.AppendLine("</div>")
                    groupIndex += 1
            End Select
            questionIndex += 1

        Next
        'If String.IsNullOrEmpty(htmlControl.ToString()) Then
        '    aqTitle = "" ''no applicant questions
        'End If
        'Return aqTitle + htmlControl.ToString()
        Return htmlControl.ToString()

    End Function
    Protected Function getAllowedApplicantQuestions(ByVal sLoanType As String, Optional ByVal isMinor As Boolean = False) As Dictionary(Of QuestionIdentifier, CQInformation)
        Dim oApplicantQuestion As New Dictionary(Of String, String)

        sLoanType = Common.GetConfigLoanTypeFromShort(sLoanType)
        Dim xPath As String = sLoanType & "/CUSTOM_APPLICANT_QUESTIONS/QUESTION"

        If isMinor Then
            xPath = "XA_LOAN/MINOR_APPLICANT_QUESTIONS/QUESTION"
        End If

        Return getAllowedQuestions(xPath, QuestionRole.Applicant)
    End Function

    Protected Function getAllowedApplicationQuestions(ByVal sLoanType As String) As Dictionary(Of QuestionIdentifier, CQInformation)
		Dim oApplicantQuestion As New Dictionary(Of String, String)

		sLoanType = Common.GetConfigLoanTypeFromShort(sLoanType)
		Dim xPath As String = sLoanType & "/CUSTOM_QUESTIONS/QUESTION"

		Return getAllowedQuestions(xPath, QuestionRole.Application)
	End Function

	Protected Function getAllowedQuestions(ByVal xPath As String, ByVal eCqRole As QuestionRole) As Dictionary(Of QuestionIdentifier, CQInformation)
		Dim oApplicantQuestion As New Dictionary(Of QuestionIdentifier, CQInformation)

		Dim lstCQInfo = CQInformation.ReadCQInfoFromConfig(_parent._CurrentWebsiteConfig, xPath, eCqRole)

		For Each cqInfo In lstCQInfo
			Dim qId As New QuestionIdentifier(cqInfo.QuestionName, eCqRole)
			If Not oApplicantQuestion.ContainsKey(qId) Then
				oApplicantQuestion.Add(qId, cqInfo)
			End If
		Next

		Return oApplicantQuestion
	End Function

	''' <summary>
	''' From all custom questions available, filter down to the questions this control should show.
	''' </summary>
	''' <param name="isComboMode">Whether the loan is in combo mode or not</param>
	''' <param name="sLoanType">Loan Type this control is being used in</param>
	''' <param name="eCqLocation">The location of this control</param>
	''' <returns>The correct questions to show</returns>
	Protected Function filteredCustomQuestions(isComboMode As Boolean, sLoanType As String, eCqLocation As CustomQuestionLocation) As List(Of CCustomQuestionXA)
		If sLoanType = "HELOC" Then sLoanType = "HE"

		Dim lstCQs As New List(Of CCustomQuestionXA)
		Dim isNewAPI As Boolean = Common.getVisibleAttribute(_parent._CurrentWebsiteConfig, "custom_question_new_api") = "Y"

		' 1.a. Retrieval
		' Get the regular questions
		If isNewAPI Then
			lstCQs.AddRange(CCustomQuestionNewAPI.
							getAllDownloadedXACustomQuestions(_parent._CurrentWebsiteConfig, sLoanType, nAvailability, bIsUrlParaCustomQuestion:=False))
		Else
			' Application
			' In the "old API", regular custom questions seemed to only come from the XML Config for the portal, and not any API. This is 
			' in accordance with CustomQuestions.ascx.vb, which this control is replacing.
			lstCQs.AddRange(CCustomQuestionNewAPI.convertToCCustomQuestionXA(_parent._CurrentWebsiteConfig.GetCustomQuestions(Common.GetConfigLoanTypeFromShort(sLoanType))))
			' Applicant
			' Note that this only pulls XA questions. This will be filtered out below.
			lstCQs.AddRange(CCustomQuestionXA.CurrentXAApplicantQuestions(_parent._CurrentWebsiteConfig, nAvailability))
		End If

        ' 1.b. Retrieval 
        ' If this is combo mode For a loan, add in the XA CQs
        If isComboMode Then
            Dim lstComboCQs As New List(Of CCustomQuestionXA)
            Dim sAvailability = "1" ''get the xa primary custom questions
			If isNewAPI Then
				lstComboCQs.AddRange(CCustomQuestionNewAPI.
									 getAllDownloadedXACustomQuestions(_parent._CurrentWebsiteConfig, "XA", sAvailability, bIsUrlParaCustomQuestion:=False))
			Else
				' Application
				' In the "old API", regular custom questions seemed to only come from the XML Config for the portal, and not any API. This is 
				' in accordance with CustomQuestions.ascx.vb, which this control is replacing.
				lstComboCQs.AddRange(CCustomQuestionNewAPI.convertToCCustomQuestionXA(_parent._CurrentWebsiteConfig.GetCustomQuestions(Common.GetConfigLoanTypeFromShort("XA"))))
				' Applicant
				' Note that this only pulls XA questions. This will be filtered out below.
				lstComboCQs.AddRange(CCustomQuestionXA.CurrentXAApplicantQuestions(_parent._CurrentWebsiteConfig, sAvailability))
			End If

			' 1.c. Retrieval sub-filter
			' Remove Secondary-only XA questions from combo apps. 
			' If a question appears in both a Loan and an XA, but it's set to Secondary-only for XA,
			' then it will already be in lstCQs from above, but shouldn't be attempted to be added here.
			' If it's XA-only, then it is caught here. But we can't filter it out below, because it'll
			' be a merged question, and we won't know the Loan and/or XA assignment.
			' P = only Primary, and S = only Secondary, B = both.
			lstComboCQs = lstComboCQs.Where(Function(cq) cq.AccountPositionScope <> "S").ToList()

			' Make sure to remove duplicates based off of the identifier, since a question could have come from the regular loan as well as the XA list.
			lstCQs.AddRange(lstComboCQs.Where(Function(cq) Not lstCQs.Any(Function(cq2) cq2.Identifer = cq.Identifer)))
        End If

        ' 1.c. Retrieval
        ' Load URL Parameter answers
        Dim ParseUrlParaCustomQuestionsAndAnswers = Common.parseUrlParaCustomQuestionNamesAndAnswers(HttpContext.Current.Request.Url.Query.safeQueryString)
		If ParseUrlParaCustomQuestionsAndAnswers IsNot Nothing AndAlso ParseUrlParaCustomQuestionsAndAnswers.Any() Then
			_URLParaCustomQuestions = Common.getUrlParaXACustomQuestionsAndAnswers(_parent._CurrentWebsiteConfig, lstCQs, ParseUrlParaCustomQuestionsAndAnswers)
		End If

        ' 2.a. Filtering
        ' Only show active questions
        ' questions do not contain answerType ="HEADER" or IsHeader = True
        lstCQs = lstCQs.Where(Function(cq) cq.IsActive AndAlso cq.AnswerType <> "HEADER" AndAlso Not cq.IsHeader).ToList()

        ' 2.b. Filtering
        ' Hide questions that are answered in the URL parameters
        Dim hshUrlQuestionNames = New HashSet(Of String)(ParseUrlParaCustomQuestionsAndAnswers.Select(Function(qa) qa.q))
		lstCQs = lstCQs.Where(Function(cq) Not hshUrlQuestionNames.Contains(cq.Name)).ToList()

		' 2.c. Filtering
		' Filter for Special XA, if needed
		If nAvailability = "1s" OrElse nAvailability = "2s" Then
			If String.IsNullOrEmpty(SpecialAccountName) Then
				_log.Warn("The SpecialAccountName on xaApplicantQuestion.ascx is not set for the Special XA page. This may affect which Custom Questions appear.")
			Else
				''filter applicant questions base on selected account type
				lstCQs = lstCQs.Where(Function(cq) cq.specialAccountTypes.Contains(SpecialAccountName)).ToList()
			End If
		End If

		' 2.d. Filtering
		' Filter primary/secondary XA
		' Primary/secondary filter is based on AccountPositionScope, B= available for both primary and secondary
		' P = only Primary, and S = only Secondary
		' Note: Don't do anything for combo. See 1.c. above.
		If LoanType = "XA" Then
			If nAvailability.StartsWith("2") Then ''Secondary XA: 2, 2s, 2a, or 2b
				lstCQs = lstCQs.Where(Function(cq) cq.AccountPositionScope <> "P").ToList()
			Else ''Primary XA: 1, 1s, 1b, 1a, or empty
				lstCQs = lstCQs.Where(Function(cq) cq.AccountPositionScope <> "S").ToList()
			End If
		End If

		' 2.e. Filtering
		' Only show the custom questions as designated by APM configuration.
		Dim dctAllowedQuestions = getAllowedApplicationQuestions(sLoanType)
		For Each allowedApplicant In getAllowedApplicantQuestions(sLoanType)
			dctAllowedQuestions.Add(allowedApplicant.Key, allowedApplicant.Value)
		Next
        ' Add in Combo (XA) items as well, if in combo mode
        If isComboMode Then
            ' First, gather all of the XA items here, for both application and applicant
            Dim dctAllowedXAQuestions = getAllowedApplicationQuestions("XA")
            For Each allowedApplicant In getAllowedApplicantQuestions("XA")
                dctAllowedXAQuestions.Add(allowedApplicant.Key, allowedApplicant.Value)
            Next

            ' Next, merge them into dctAllowedQuestions. We need to be careful here, since
            ' the same CQ can be configured for both the Loan and for XA. If they have different display positions,
            ' then prefer the display position that is set to the first page if they're different.
            ' If a CQ is enabled on one but not the other, then still include it.
            For Each allowedXAQuestion In dctAllowedXAQuestions
                If Not dctAllowedQuestions.ContainsKey(allowedXAQuestion.Key) Then
                    ' If no merge conflict, just add
                    dctAllowedQuestions.Add(allowedXAQuestion.Key, allowedXAQuestion.Value)
                Else
                    ' If there is a merge conflict, use the logic described above. Here, just modify the existing .Value
                    Dim allowedQuestion = dctAllowedQuestions.Item(allowedXAQuestion.Key)
                    If allowedQuestion.DisplayPage = CustomQuestionLocation.LoanPage OrElse allowedXAQuestion.Value.DisplayPage = CustomQuestionLocation.LoanPage Then
                        allowedQuestion.DisplayPage = CustomQuestionLocation.LoanPage
                    End If
                End If
            Next
        End If
        Dim bAnyAllowedApplication As Boolean = dctAllowedQuestions.Any(Function(allowed) allowed.Value.IsEnable AndAlso allowed.Key.Role = QuestionRole.Application)
		Dim bAnyAllowedApplicant As Boolean = dctAllowedQuestions.Any(Function(allowed) allowed.Value.IsEnable AndAlso allowed.Key.Role = QuestionRole.Applicant)
		lstCQs = lstCQs.Where(Function(cq)
								  ' Empty allowed list => Show All
								  ' Filled allowed list => Show only from list
								  If cq.IsApplicant AndAlso Not bAnyAllowedApplicant Then Return True
								  If Not cq.IsApplicant AndAlso Not bAnyAllowedApplication Then Return True
								  Return dctAllowedQuestions.ContainsKey(cq.Identifer) AndAlso dctAllowedQuestions.Item(cq.Identifer).IsEnable
							  End Function).ToList()

		' 2.f. Filtering
		' Based on the location of this control, choose the correct CQs based on APM Config
		lstCQs = lstCQs.
			Where(Function(cq)
					  If dctAllowedQuestions.ContainsKey(cq.Identifer) Then
						  ' If there is an override, use that
						  If dctAllowedQuestions.Item(cq.Identifer).DisplayPage = eCqLocation Then
							  Return True
						  End If
					  Else
						  ' Else, use the default location for a question based on its settings
						  Select Case eCqLocation
							  Case CustomQuestionLocation.LoanPage
								  Return False
							  Case CustomQuestionLocation.ApplicantPage
								  Return cq.IsApplicant = True
							  Case CustomQuestionLocation.ReviewPage
								  Return cq.IsApplicant = False
						  End Select
					  End If
					  Return False
				  End Function).
			ToList()

		' 3.a. Post Processing
		' Perform post-processing on downloaded questions based on APM overrides.
		' If APM is overriding the required attribute, change it on the CQs here.
		For Each cq In lstCQs
			If dctAllowedQuestions.ContainsKey(cq.Identifer) Then
				Dim bOptionalIsRequired As Boolean? = dctAllowedQuestions.Item(cq.Identifer).IsRequired
				If bOptionalIsRequired.HasValue Then cq.IsRequired = bOptionalIsRequired.Value
			End If
		Next

		' 3.b. Post Processing
		' Sort CQs
		lstCQs = lstCQs.OrderBy(Function(cq) cq.Identifer.Role).ThenBy(Function(cq) cq.Position).ToList()

		Return lstCQs

	End Function

End Class
