﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="xaApplicantAddress.ascx.vb"
    Inherits="Inc_MainApp_xaApplicantAddress" %>
<%@ Import Namespace="Newtonsoft.Json" %>
<iframe name="<%=IDPrefix%>if_Address"  style="display: none;" src="about:blank" ></iframe>
<form target="<%=IDPrefix%>if_Address" action="/handler/AutoFillHandler.aspx" id="<%=IDPrefix%>frm_Address">
<!--move copy primary address for joint-->
<div style="display: none; " id="<%=IDPrefix%>divCopyAddress">
    <div style="margin-top: 10px;">
            <a href="#" class="header_theme2 shadow-btn chevron-circle-right-before" name="btnCopyAddress" id="<%=IDPrefix%>btnCopyAddress" onclick="<%=IDPrefix%>CopyPrimaryAddress(event);<%=IDPrefix%>showHideState();">Copy the Primary Applicant's Address</a>
    </div>
</div>
<div id="<%=IDPrefix%>div_ddl_country">
    <div data-role="fieldcontain">
        <label for="<%=IDPrefix%>ddlCountry" class="RequiredIcon">Country</label>
        <select id="<%=IDPrefix%>ddlCountry" name="country" autocomplete="country" onchange="<%=IDPrefix%>showHideState()">
            <option value="USA" selected="selected">USA</option>
            <option value="FRANCE">France</option>
            <option value="GERMANY">Germany</option>
            <option value="ITALY">Italy</option>
            <option value="SPAIN">Spain</option>
            <option value="UNITED KINGDOM">United Kingdom</option>
            <option value="AALAND">Aaland</option>
            <option value="ABKHAZIA">Abkhazia</option>
            <option value="AFGHANISTAN">Afghanistan</option>
            <option value="ALBANIA">Albania</option>
            <option value="ALGERIA">Algeria</option>
            <option value="AMERICAN SAMOA">American Samoa</option>
            <option value="ANDORRA">Andorra</option>
            <option value="ANGOLA">Angola</option>
            <option value="ANGUILLA">Anguilla</option>
            <option value="ANTARCTICA">Antarctica</option>
            <option value="ANTIGUA_BARBUDA">Antigua And Barbuda</option>
            <option value="ARGENTINA">Argentina</option>
            <option value="ARMENIA">Armenia</option>
            <option value="ARUBA">Aruba</option>
            <option value="AUSTRALIA">Australia</option>
            <option value="AUSTRIA">Austria</option>
            <option value="AZERBAIJAN">Azerbaijan</option>
            <option value="BAHAMAS">Bahamas</option>
            <option value="BAHRAIN">Bahrain</option>
            <option value="BANGLADESH">Bangladesh</option>
            <option value="BARBADOS">Barbados</option>
            <option value="BELARUS">Belarus</option>
            <option value="BELGIUM">Belgium</option>
            <option value="BELIZE">Belize</option>
            <option value="BENIN">Benin</option>
            <option value="BERMUDA">Bermuda</option>
            <option value="BHUTAN">Bhutan</option>
            <option value="BOLIVIA">Bolivia</option>
            <option value="BOSNIA_HERZEGOVINA">Bosnia And Herzegovina</option>
            <option value="BOTSWANA">Botswana</option>
            <option value="BOUVET ISLAND">Bouvet Island</option>
            <option value="BRAZIL">Brazil</option>
            <option value="BRITISH INDIAN OCEAN">British Indian Ocean Territory</option>
            <option value="BRITISH_VIRGIN_ILND">British Virgin Islands</option>
            <option value="BRUNEI">Brunei</option>
            <option value="BULGARIA">Bulgaria</option>
            <option value="BURKINA FASO">Burkina Faso</option>
            <option value="BURUNDI">Burundi</option>
            <option value="CAMBODIA">Cambodia</option>
            <option value="CAMEROON">Cameroon</option>
            <option value="CANADA">Canada</option>
            <option value="CAPE VERDE">Cape Verde</option>
            <option value="CAYMAN ISLANDS">Cayman Islands</option>
            <option value="CENTRAL AFRICAN REP">Central African Republic</option>
            <option value="CHAD">Chad</option>
            <option value="CHILE">Chile</option>
            <option value="CHINA">China</option>
            <option value="CHRISTMAS ISLAND">Christmas Island</option>
            <option value="COCOS ISLANDS">Cocos Islands</option>
            <option value="COLOMBIA">Colombia</option>
            <option value="COMOROS">Comoros</option>
            <option value="COOK ISLANDS">Cook Islands</option>
            <option value="COSTA RICA">Costa Rica</option>
            <option value="COTE D&#39;IVOIRE">Cote D&#39;Ivoire</option>
            <option value="CROATIA">Croatia</option>
            <option value="CUBA">Cuba</option>
            <option value="CYPRUS">Cyprus</option>
            <option value="CZECH REPUBLIC">Czech Republic</option>
            <option value="DEMOCRATIC_REP_CONGO">Democratic Republic Of The Congo</option>
            <option value="DENMARK">Denmark</option>
            <option value="DJIBOUTI">Djibouti</option>
            <option value="DOMINICA">Dominica</option>
            <option value="DOMINICAN REPUBLIC">Dominican Republic</option>
            <option value="TIMOR-LESTE">East Timor</option>
            <option value="ECUADOR">Ecuador</option>
            <option value="EGYPT">Egypt</option>
            <option value="EL SALVADOR">El Salvador</option>
            <option value="EQUATORIAL GUINEA">Equatorial Guinea</option>
            <option value="ERITREA">Eritrea</option>
            <option value="ESTONIA">Estonia</option>
            <option value="ETHIOPIA">Ethiopia</option>
            <option value="FALKLAND ISLANDS">Falkland Islands</option>
            <option value="FAROE ISLANDS">Faroe Islands</option>
            <option value="FIJI">Fiji</option>
            <option value="FINLAND">Finland</option>
            <option value="FRENCH GUIANA">French Guiana</option>
            <option value="FRENCH POLYNESIA">French Polynesia</option>
            <option value="FRENCH_SOUTHERN_TERR">French Southern Territories</option>
            <option value="GABON">Gabon</option>
            <option value="GAMBIA">Gambia</option>
            <option value="GEORGIA">Georgia</option>
            <option value="GHANA">Ghana</option>
            <option value="GIBRALTAR">Gibraltar</option>
            <option value="GREECE">Greece</option>
            <option value="GREENLAND">Greenland</option>
            <option value="GRENADA">Grenada</option>
            <option value="GUADELOUPE">Guadeloupe</option>
            <option value="GUAM">Guam</option>
            <option value="GUATEMALA">Guatemala</option>
            <option value="GUERNSEY">Guernsey</option>
            <option value="GUINEA">Guinea</option>
            <option value="GUINEA BISSAU">Guinea Bissau</option>
            <option value="GUYANA">Guyana</option>
            <option value="HAITI">Haiti</option>
            <option value="HEARD_MCDONALD_ILND">Heard Island And Mcdonald Islands</option>
            <option value="HONDURAS">Honduras</option>
            <option value="HONG KONG">Hong Kong</option>
            <option value="HUNGARY">Hungary</option>
            <option value="ICELAND">Iceland</option>
            <option value="INDIA">India</option>
            <option value="INDONESIA">Indonesia</option>
            <option value="IRAN">Iran</option>
            <option value="IRAQ">Iraq</option>
            <option value="IRELAND">Ireland</option>
            <option value="ISLE OF MAN">Isle Of Man</option>
            <option value="ISRAEL">Israel</option>
            <option value="JAMAICA">Jamaica</option>
            <option value="JAPAN">Japan</option>
            <option value="JERSEY">Jersey</option>
            <option value="JORDAN">Jordan</option>
            <option value="KAZAKHSTAN">Kazakhstan</option>
            <option value="KENYA">Kenya</option>
            <option value="KIRIBATI">Kiribati</option>
            <option value="KOSOVO">Kosovo</option>
            <option value="KUWAIT">Kuwait</option>
            <option value="KYRGYZSTAN">Kyrgyzstan</option>
            <option value="LAOS">Laos</option>
            <option value="LATVIA">Latvia</option>
            <option value="LEBANON">Lebanon</option>
            <option value="LESOTHO">Lesotho</option>
            <option value="LIBERIA">Liberia</option>
            <option value="LIBYA">Libya</option>
            <option value="LIECHTENSTEIN">Liechtenstein</option>
            <option value="LITHUANIA">Lithuania</option>
            <option value="LUXEMBOURG">Luxembourg</option>
            <option value="MACAU">Macau</option>
            <option value="MACEDONIA">Macedonia</option>
            <option value="MADAGASCAR">Madagascar</option>
            <option value="MALAWI">Malawi</option>
            <option value="MALAYSIA">Malaysia</option>
            <option value="MALDIVES">Maldives</option>
            <option value="MALI">Mali</option>
            <option value="MALTA">Malta</option>
            <option value="MARSHALL ISLANDS">Marshall Islands</option>
            <option value="MARTINIQUE">Martinique</option>
            <option value="MAURITANIA">Mauritania</option>
            <option value="MAURITIUS">Mauritius</option>
            <option value="MAYOTTE">Mayotte</option>
            <option value="MEXICO">Mexico</option>
            <option value="MICRONESIA">Micronesia</option>
            <option value="MOLDOVA">Moldova</option>
            <option value="MONACO">Monaco</option>
            <option value="MONGOLIA">Mongolia</option>
            <option value="MONTENEGRO">Montenegro</option>
            <option value="MONTSERRAT">Montserrat</option>
            <option value="MOROCCO">Morocco</option>
            <option value="MOZAMBIQUE">Mozambique</option>
            <option value="MYANMAR">Myanmar</option>
            <option value="NAGORNO-KARABAKH">Nagorno-Karabakh</option>
            <option value="NAMIBIA">Namibia</option>
            <option value="NAURU">Nauru</option>
            <option value="NEPAL">Nepal</option>
            <option value="NETHERLANDS">Netherlands</option>
            <option value="NETHERLANDS ANTILLES">Netherlands Antilles</option>
            <option value="NEW CALEDONIA">New Caledonia</option>
            <option value="NEW ZEALAND">New Zealand</option>
            <option value="NICARAGUA">Nicaragua</option>
            <option value="NIGER">Niger</option>
            <option value="NIGERIA">Nigeria</option>
            <option value="NIUE">Niue</option>
            <option value="NORFOLK ISLAND">Norfolk Island</option>
            <option value="NORTH KOREA">North Korea</option>
            <option value="NORTHERN CYPRUS">Northern Cyprus</option>
            <option value="NORTHERN_MARIANA_ILN">Northern Mariana Islands</option>
            <option value="NORWAY">Norway</option>
            <option value="OMAN">Oman</option>
            <option value="PAKISTAN">Pakistan</option>
            <option value="PALAU">Palau</option>
            <option value="PALESTINE">Palestine</option>
            <option value="PANAMA">Panama</option>
            <option value="PAPUA NEW GUINEA">Papua New Guinea</option>
            <option value="PARAGUAY">Paraguay</option>
            <option value="PERU">Peru</option>
            <option value="PHILIPPINES">Philippines</option>
            <option value="PITCAIRN">Pitcairn</option>
            <option value="POLAND">Poland</option>
            <option value="PORTUGAL">Portugal</option>
            <option value="PUERTO RICO">Puerto Rico</option>
            <option value="QATAR">Qatar</option>
            <option value="CONGO">Republic Of The Congo</option>
            <option value="REUNION">Reunion</option>
            <option value="ROMANIA">Romania</option>
            <option value="RUSSIA">Russia</option>
            <option value="RWANDA">Rwanda</option>
            <option value="SAINT BARTHELEMY">Saint Barthelemy</option>
            <option value="SAINT HELENA">Saint Helena</option>
            <option value="SAINT_KITTS_NEVIS">Saint Kitts And Nevis</option>
            <option value="SAINT LUCIA">Saint Lucia</option>
            <option value="SAINT_VINCENT_GREN">Saint Vincent And The Grenadines</option>
            <option value="SAINT-MARTIN">Saint-Martin</option>
            <option value="SAINTPIERRE_MIQUELON">Saint-Pierre And Miquelon</option>
            <option value="SAMOA">Samoa</option>
            <option value="SAN MARINO">San Marino</option>
            <option value="SAO_TOME_PRINCIPE">Sao Tome And Principe</option>
            <option value="SAUDI ARABIA">Saudi Arabia</option>
            <option value="SENEGAL">Senegal</option>
            <option value="SERBIA">Serbia</option>
            <option value="SEYCHELLES">Seychelles</option>
            <option value="SIERRA LEONE">Sierra Leone</option>
            <option value="SINGAPORE">Singapore</option>
            <option value="SLOVAKIA">Slovakia</option>
            <option value="SLOVENIA">Slovenia</option>
            <option value="SOLOMON ISLANDS">Solomon Islands</option>
            <option value="SOMALIA">Somalia</option>
            <option value="SOMALILAND">Somaliland</option>
            <option value="SOUTH AFRICA">South Africa</option>
            <option value="S_GEORGIA_SANDWICH">South Georgia And The South Sandwich Islands</option>
            <option value="SOUTH KOREA">South Korea</option>
            <option value="SOUTH OSSETIA">South Ossetia</option>
            <option value="SRI LANKA">Sri Lanka</option>
            <option value="SUDAN">Sudan</option>
            <option value="SURINAME">Suriname</option>
            <option value="SVALBARD_JAN_MAYEN">Svalbard And Jan Mayen</option>
            <option value="SWAZILAND">Swaziland</option>
            <option value="SWEDEN">Sweden</option>
            <option value="SWITZERLAND">Switzerland</option>
            <option value="SYRIA">Syria</option>
            <option value="TAIWAN">Taiwan</option>
            <option value="TAJIKISTAN">Tajikistan</option>
            <option value="TANZANIA">Tanzania</option>
            <option value="THAILAND">Thailand</option>
            <option value="TOGO">Togo</option>
            <option value="TOKELAU">Tokelau</option>
            <option value="TONGA">Tonga</option>
            <option value="TRANSNISTRIA">Transnistria</option>
            <option value="TRINIDAD AND TOBAGO">Trinidad And Tobago</option>
            <option value="TRISTAN DA CUNHA">Tristan Da Cunha</option>
            <option value="TUNISIA">Tunisia</option>
            <option value="TURKEY">Turkey</option>
            <option value="TURKMENISTAN">Turkmenistan</option>
            <option value="TURKS_CAICOS_ISLANDS">Turks And Caicos Islands</option>
            <option value="TUVALU">Tuvalu</option>
            <option value="UGANDA">Uganda</option>
            <option value="UKRAINE">Ukraine</option>
            <option value="UNITED ARAB EMIRATES">United Arab Emirates</option>
            <option value="US_MINOR_ISLANDS">United States Minor Outlying Islands</option>
            <option value="US_VIRGIN_ISLANDS">United States Virgin Islands</option>
            <option value="URUGUAY">Uruguay</option>
            <option value="UZBEKISTAN">Uzbekistan</option>
            <option value="VANUATU">Vanuatu</option>
            <option value="VATICAN CITY">Vatican City</option>
            <option value="VENEZUELA">Venezuela</option>
            <option value="VIETNAM">Vietnam</option>
            <option value="WALLIS AND FUTUNA">Wallis And Futuna</option>
            <option value="WESTERN SAHARA">Western Sahara</option>
            <option value="YEMEN">Yemen</option>
            <option value="ZAMBIA">Zambia</option>
            <option value="ZIMBABWE">Zimbabwe</option>
        </select>
    </div>
</div>
<div data-role="fieldcontain">
    <label for="<%=IDPrefix%>txtAddress" class="RequiredIcon">Address</label>
    <input type="text" id="<%=IDPrefix%>txtAddress" maxlength="100" name="address1" autocomplete="street-address" value="<%=Address %>" onchange="<%=callVerifyAddressJSFunction(False)%>" />
</div>
<div data-role="fieldcontain" id="<%=IDPrefix%>divAddress2" style="display: none;">
    <label for="<%=IDPrefix%>txtAddress2">Address Line 2</label>
    <input type="text" id="<%=IDPrefix%>txtAddress2" maxlength="100" name="address2" autocomplete="address-line2" value="" onchange="<%=callVerifyAddressJSFunction(False)%>" />
</div>
<div data-role="fieldcontain">
    <label for="<%=IDPrefix%>txtZip" id="<%=IDPrefix%>lblZipTitle" class="RequiredIcon">Zip</label>
    <input type="tel" id="<%=IDPrefix%>txtZip" maxlength="10" pattern="[0-9]*" name="zip" autocomplete="postal-code" value="<%=Zip %>" onchange="<%=callVerifyAddressJSFunction(True)%>" />
</div>

<div data-role="fieldcontain">
    <label for="<%=IDPrefix%>txtCity" id="<%=IDPrefix%>lblCityTitle" class="RequiredIcon">City</label>
    <input type="text" id="<%=IDPrefix%>txtCity" maxlength="50" name="city" autocomplete="address-level2" value="<%=City %>" onchange="<%=callVerifyAddressJSFunction(False)%>" />
</div>
<div class="<%=IDPrefix%>div_ddl_state">
    <div data-role="fieldcontain">
        <label for="<%=IDPrefix%>ddlState" class="RequiredIcon">State</label>
        <select id="<%=IDPrefix%>ddlState" name="state" autocomplete="address-level1" onchange="<%=callVerifyAddressJSFunction(False)%>">
            <%=StateDropdownWithSelectedItem%>
        </select>
    </div>
</div>
<div><span id="<%=IDPrefix%>spVerifyMessage" class="require-span" style="display: none;">Unable to validate address</span> </div>
<%If EnableOccupancyStatus Then%>
<div id="<%=IDPrefix%>divOccupancyStatusSection" <%=IIf(IsInMode("777") AndAlso IsInFeature("visibility") AndAlso (LoanType = "XA" Or LoanType = "SA"), "class=""showfield-section"" data-show-field-section-id=""" & BuildShowFieldSectionID("divOccupancyStatusSection") & """ " & IIf(LoanType = "XA" Or LoanType = "SA", "data-default-state=""off""", "") & " data-section-name= '" & IIf(String.IsNullOrEmpty(IDPrefix), "", IIf(IDPrefix = "co_", "Co-App", "Minor")) & " Occupancy Status'", "")%>>
	<div data-role="fieldcontain">
        <label for="<%=IDPrefix%>ddlOccupyingStatus" class="RequiredIcon">Occupancy Status</label>
        <select id="<%=IDPrefix%>ddlOccupyingStatus" onchange="<%=IDPrefix%>HandleOccupancyStatusChange()">
            <%=OccupyingLocationDropdown%>
        </select>
    </div>
    <%If (IDPrefix = "" Or IDPrefix = "co_") And CollectDescriptionIfOccupancyStatusIsOther.ToUpper() = "Y" Then%>
    <div id="<%=IDPrefix%>divCollectDescriptionIfOccupancyStatusIsOther" style="display: none;">
        <div data-role="fieldcontain">
            <label for="<%=IDPrefix%>txtOccupancyDescription" class="RequiredIcon">Other Occupancy Description</label>
            <input type="text" id="<%=IDPrefix%>txtOccupancyDescription" maxlength="90"/>
        </div>  
    </div>
    <%End If%>
    <div data-role="fieldcontain">
        <label id="<%=IDPrefix%>lblOccupancyDuration" class="rename-able RequiredIcon">Occupancy Duration</label>
        <div id="<%=IDPrefix%>divOccupancyDuration" class="ui-input-horizal row">
            <div class="col-xs-6">
                <!--input type="text"  pattern="[0-9]*" id="<%=IDPrefix%>txtOccupancyDurationYear" maxlength="2" class="numeric inyear" /-->
                <select id="<%=IDPrefix%>ddlOccupancyDurationYear" class="<%=IDPrefix%>OccupancyDuration" aria-labelledby="<%=IDPrefix%>lblOccupancyDuration">
                    <option value=" " class="rename-able">Years</option>
                    <%=LiveYearsDropdown%>
                </select>
            </div>
            <div class="col-xs-6">
                <select id="<%=IDPrefix%>ddlOccupancyDurationMonth" class="<%=IDPrefix%>OccupancyDuration" aria-labelledby="<%=IDPrefix%>lblOccupancyDuration">
                    <option value=" " class="rename-able">Months</option>
                    <%=LiveMonthsDropdown%>
                </select>
            </div>
        </div>
    </div>
</div>   <%--end ocupancy div--%>
<%End If%>
    <%If EnableHousingPayment %>
       <div id="<%=IDPrefix%>divHousingPayment" <%=IIf(IsInMode("777") AndAlso IsInFeature("visibility") AndAlso (LoanType = "XA" Or LoanType = "SA"), "class=""showfield-section"" data-show-field-section-id=""" & BuildShowFieldSectionID("divHousingPayment") & """ data-default-state=""off"" data-section-name= '" & IIf(String.IsNullOrEmpty(IDPrefix), "", IIf(IDPrefix = "co_", "Co-App", "Minor")) & " Housing Payment'", "")%>>
		<div data-role="fieldcontain">
			<label for="<%=IDPrefix%>txtHousingPayment">Housing Payment</label>
			<input type="<%=TextAndroidTel%>" pattern="[0-9]*" id="<%=IDPrefix%>txtHousingPayment" class="money" maxlength ="10" />
		</div> 
      </div>
    <%End If%>
</form>

<%If EnableOccupancyStatus AndAlso IDPrefix <> "m_" Then%>
<form target="<%=IDPrefix%>if_Address" action="/handler/AutoFillHandler.aspx" id="<%=IDPrefix%>frm_PreviousAdress">
<div id="<%=IDPrefix%>divPreviousAddress" style="display:none">
    <br />
    <div style="font-size:17px;font-weight :bold">Previous Address</div>
    <%--<div id="<%=IDPrefix%>div_ddl_previous_country" style="display: <%=IIf(LoanType = "XA", "none", "block")%>;" data-role="fieldcontain">--%>
        <div id="<%=IDPrefix%>div_ddl_previous_country" style="display:none" data-role="fieldcontain">  <%--TODO:internatioanl is not yet support on lenderside--%>
        <label for="<%=IDPrefix%>ddlPreviousCountry" class="RequiredIcon">Country</label>
        <select id="<%=IDPrefix%>ddlPreviousCountry" name="country" autocomplete="country" onchange="<%=IDPrefix%>showHidePreviousState()">
            <option value="USA" selected="selected">USA</option>            
        </select>
    </div>
    <div data-role="fieldcontain">
        <label for="<%=IDPrefix%>txtPreviousAddress" class="RequiredIcon">Address</label>
        <input type="text" id="<%=IDPrefix%>txtPreviousAddress" maxlength="100" name="address1" autocomplete="street-address" onchange="<%=callVerifyAddressJSFunction(False, "Previous")%>"/>
    </div>
    <div data-role="fieldcontain" id="<%=IDPrefix%>divPreviousAddress2" style="display: none;">
        <label for="<%=IDPrefix%>txtPreviousAddress2">Address Line 2</label>
        <input type="text" id="<%=IDPrefix%>txtPreviousAddress2" maxlength="100" name="address2" autocomplete="address-line2" onchange="<%=callVerifyAddressJSFunction(False, "Previous")%>" />
    </div>
    <div data-role="fieldcontain">
        <label for="<%=IDPrefix%>txtPreviousZip" id="<%=IDPrefix%>lblPreviousZipTitle" class="RequiredIcon">Zip</label>
        <input type="tel" id="<%=IDPrefix%>txtPreviousZip" pattern="[0-9]*" maxlength="10" name="zip" autocomplete="postal-code" onchange="<%=callVerifyAddressJSFunction(True, "Previous")%>"/>
    </div>
    <div data-role="fieldcontain">
        <label for="<%=IDPrefix%>txtPreviousCity" id="<%=IDPrefix%>lblPreviousCityTitle" class="RequiredIcon">City</label>
        <input type="text" id="<%=IDPrefix%>txtPreviousCity" maxlength="50" name="city" autocomplete="address-level2" onchange="<%=callVerifyAddressJSFunction(False, "Previous")%>"/>
    </div>
    
    <div class="<%=IDPrefix%>div_ddl_previous_state">
        <div data-role="fieldcontain">
            <label for="<%=IDPrefix%>ddlPreviousState" class="RequiredIcon">State</label>
            <select id="<%=IDPrefix%>ddlPreviousState" name="state" autocomplete="address-level1" onchange="<%=callVerifyAddressJSFunction(False, "Previous")%>">
                <%--<option value="">--Please Select--</option>--%>
                <%=StateDropdown %>
            </select>
        </div>
    </div>
    <div><span id="<%=IDPrefix%>spVerifyPreviousMessage" class="require-span" style="display: none;"> Unable to validate address</span></div>

</div>
</form>
<input type="hidden" id="<%=IDPrefix%>hdShowPreviousAddress" value="N" />
<%End If%>
<%If EnableMailingAddress Then%>
<div id="<%=IDPrefix%>divMailingAddressSection" <%=IIf(IsInMode("777") AndAlso IsInFeature("visibility"), "class=""showfield-section"" data-show-field-section-id=""" & BuildShowFieldSectionID("divMailingAddressSection") & """ data-section-name= '" & IIf(IDPrefix = "co_", "Co-App", IIf(IDPrefix = "m_", "Minor", "")) & " Mailing Address'", "")%>>

<form target="<%=IDPrefix%>if_Address" action="/handler/AutoFillHandler.aspx" id="<%=IDPrefix%>frm_mailingAddress">
<div id="<%=IDPrefix%>divMailingAddress">
     <div style="margin-top: 10px;">
        <a id="<%=IDPrefix%>lnkHasMaillingAddress" href="#" data-mode="self-handle-event" status="N" style="cursor: pointer; font-weight: bold;" class="header_theme2 shadow-btn ui-link chevron-circle-right-before">Use different address for mailing</a>
    </div>
</div>
<div id="<%=IDPrefix%>divMailingAddressForm" style="display :none">
    <br />
    <div style="font-size:17px;font-weight :bold">Mailing Address</div>
    <div id="<%=IDPrefix%>div_ddl_mailing_country" data-role="fieldcontain">
        <label for="<%=IDPrefix%>ddlMailingCountry" class="RequiredIcon">Country</label>
        <select id="<%=IDPrefix%>ddlMailingCountry" name="country" autocomplete="country" onchange="<%=IDPrefix%>showHideMailingState()">
            <option value="USA" selected="selected">USA</option>
            <option value="FRANCE">France</option>
            <option value="GERMANY">Germany</option>
            <option value="ITALY">Italy</option>
            <option value="SPAIN">Spain</option>
            <option value="UNITED KINGDOM">United Kingdom</option>
            <option value="AALAND">Aaland</option>
            <option value="ABKHAZIA">Abkhazia</option>
            <option value="AFGHANISTAN">Afghanistan</option>
            <option value="ALBANIA">Albania</option>
            <option value="ALGERIA">Algeria</option>
            <option value="AMERICAN SAMOA">American Samoa</option>
            <option value="ANDORRA">Andorra</option>
            <option value="ANGOLA">Angola</option>
            <option value="ANGUILLA">Anguilla</option>
            <option value="ANTARCTICA">Antarctica</option>
            <option value="ANTIGUA_BARBUDA">Antigua And Barbuda</option>
            <option value="ARGENTINA">Argentina</option>
            <option value="ARMENIA">Armenia</option>
            <option value="ARUBA">Aruba</option>
            <option value="AUSTRALIA">Australia</option>
            <option value="AUSTRIA">Austria</option>
            <option value="AZERBAIJAN">Azerbaijan</option>
            <option value="BAHAMAS">Bahamas</option>
            <option value="BAHRAIN">Bahrain</option>
            <option value="BANGLADESH">Bangladesh</option>
            <option value="BARBADOS">Barbados</option>
            <option value="BELARUS">Belarus</option>
            <option value="BELGIUM">Belgium</option>
            <option value="BELIZE">Belize</option>
            <option value="BENIN">Benin</option>
            <option value="BERMUDA">Bermuda</option>
            <option value="BHUTAN">Bhutan</option>
            <option value="BOLIVIA">Bolivia</option>
            <option value="BOSNIA_HERZEGOVINA">Bosnia And Herzegovina</option>
            <option value="BOTSWANA">Botswana</option>
            <option value="BOUVET ISLAND">Bouvet Island</option>
            <option value="BRAZIL">Brazil</option>
            <option value="BRITISH INDIAN OCEAN">British Indian Ocean Territory</option>
            <option value="BRITISH_VIRGIN_ILND">British Virgin Islands</option>
            <option value="BRUNEI">Brunei</option>
            <option value="BULGARIA">Bulgaria</option>
            <option value="BURKINA FASO">Burkina Faso</option>
            <option value="BURUNDI">Burundi</option>
            <option value="CAMBODIA">Cambodia</option>
            <option value="CAMEROON">Cameroon</option>
            <option value="CANADA">Canada</option>
            <option value="CAPE VERDE">Cape Verde</option>
            <option value="CAYMAN ISLANDS">Cayman Islands</option>
            <option value="CENTRAL AFRICAN REP">Central African Republic</option>
            <option value="CHAD">Chad</option>
            <option value="CHILE">Chile</option>
            <option value="CHINA">China</option>
            <option value="CHRISTMAS ISLAND">Christmas Island</option>
            <option value="COCOS ISLANDS">Cocos Islands</option>
            <option value="COLOMBIA">Colombia</option>
            <option value="COMOROS">Comoros</option>
            <option value="COOK ISLANDS">Cook Islands</option>
            <option value="COSTA RICA">Costa Rica</option>
            <option value="COTE D&#39;IVOIRE">Cote D&#39;Ivoire</option>
            <option value="CROATIA">Croatia</option>
            <option value="CUBA">Cuba</option>
            <option value="CYPRUS">Cyprus</option>
            <option value="CZECH REPUBLIC">Czech Republic</option>
            <option value="DEMOCRATIC_REP_CONGO">Democratic Republic Of The Congo</option>
            <option value="DENMARK">Denmark</option>
            <option value="DJIBOUTI">Djibouti</option>
            <option value="DOMINICA">Dominica</option>
            <option value="DOMINICAN REPUBLIC">Dominican Republic</option>
            <option value="TIMOR-LESTE">East Timor</option>
            <option value="ECUADOR">Ecuador</option>
            <option value="EGYPT">Egypt</option>
            <option value="EL SALVADOR">El Salvador</option>
            <option value="EQUATORIAL GUINEA">Equatorial Guinea</option>
            <option value="ERITREA">Eritrea</option>
            <option value="ESTONIA">Estonia</option>
            <option value="ETHIOPIA">Ethiopia</option>
            <option value="FALKLAND ISLANDS">Falkland Islands</option>
            <option value="FAROE ISLANDS">Faroe Islands</option>
            <option value="FIJI">Fiji</option>
            <option value="FINLAND">Finland</option>
            <option value="FRENCH GUIANA">French Guiana</option>
            <option value="FRENCH POLYNESIA">French Polynesia</option>
            <option value="FRENCH_SOUTHERN_TERR">French Southern Territories</option>
            <option value="GABON">Gabon</option>
            <option value="GAMBIA">Gambia</option>
            <option value="GEORGIA">Georgia</option>
            <option value="GHANA">Ghana</option>
            <option value="GIBRALTAR">Gibraltar</option>
            <option value="GREECE">Greece</option>
            <option value="GREENLAND">Greenland</option>
            <option value="GRENADA">Grenada</option>
            <option value="GUADELOUPE">Guadeloupe</option>
            <option value="GUAM">Guam</option>
            <option value="GUATEMALA">Guatemala</option>
            <option value="GUERNSEY">Guernsey</option>
            <option value="GUINEA">Guinea</option>
            <option value="GUINEA BISSAU">Guinea Bissau</option>
            <option value="GUYANA">Guyana</option>
            <option value="HAITI">Haiti</option>
            <option value="HEARD_MCDONALD_ILND">Heard Island And Mcdonald Islands</option>
            <option value="HONDURAS">Honduras</option>
            <option value="HONG KONG">Hong Kong</option>
            <option value="HUNGARY">Hungary</option>
            <option value="ICELAND">Iceland</option>
            <option value="INDIA">India</option>
            <option value="INDONESIA">Indonesia</option>
            <option value="IRAN">Iran</option>
            <option value="IRAQ">Iraq</option>
            <option value="IRELAND">Ireland</option>
            <option value="ISLE OF MAN">Isle Of Man</option>
            <option value="ISRAEL">Israel</option>
            <option value="JAMAICA">Jamaica</option>
            <option value="JAPAN">Japan</option>
            <option value="JERSEY">Jersey</option>
            <option value="JORDAN">Jordan</option>
            <option value="KAZAKHSTAN">Kazakhstan</option>
            <option value="KENYA">Kenya</option>
            <option value="KIRIBATI">Kiribati</option>
            <option value="KOSOVO">Kosovo</option>
            <option value="KUWAIT">Kuwait</option>
            <option value="KYRGYZSTAN">Kyrgyzstan</option>
            <option value="LAOS">Laos</option>
            <option value="LATVIA">Latvia</option>
            <option value="LEBANON">Lebanon</option>
            <option value="LESOTHO">Lesotho</option>
            <option value="LIBERIA">Liberia</option>
            <option value="LIBYA">Libya</option>
            <option value="LIECHTENSTEIN">Liechtenstein</option>
            <option value="LITHUANIA">Lithuania</option>
            <option value="LUXEMBOURG">Luxembourg</option>
            <option value="MACAU">Macau</option>
            <option value="MACEDONIA">Macedonia</option>
            <option value="MADAGASCAR">Madagascar</option>
            <option value="MALAWI">Malawi</option>
            <option value="MALAYSIA">Malaysia</option>
            <option value="MALDIVES">Maldives</option>
            <option value="MALI">Mali</option>
            <option value="MALTA">Malta</option>
            <option value="MARSHALL ISLANDS">Marshall Islands</option>
            <option value="MARTINIQUE">Martinique</option>
            <option value="MAURITANIA">Mauritania</option>
            <option value="MAURITIUS">Mauritius</option>
            <option value="MAYOTTE">Mayotte</option>
            <option value="MEXICO">Mexico</option>
            <option value="MICRONESIA">Micronesia</option>
            <option value="MOLDOVA">Moldova</option>
            <option value="MONACO">Monaco</option>
            <option value="MONGOLIA">Mongolia</option>
            <option value="MONTENEGRO">Montenegro</option>
            <option value="MONTSERRAT">Montserrat</option>
            <option value="MOROCCO">Morocco</option>
            <option value="MOZAMBIQUE">Mozambique</option>
            <option value="MYANMAR">Myanmar</option>
            <option value="NAGORNO-KARABAKH">Nagorno-Karabakh</option>
            <option value="NAMIBIA">Namibia</option>
            <option value="NAURU">Nauru</option>
            <option value="NEPAL">Nepal</option>
            <option value="NETHERLANDS">Netherlands</option>
            <option value="NETHERLANDS ANTILLES">Netherlands Antilles</option>
            <option value="NEW CALEDONIA">New Caledonia</option>
            <option value="NEW ZEALAND">New Zealand</option>
            <option value="NICARAGUA">Nicaragua</option>
            <option value="NIGER">Niger</option>
            <option value="NIGERIA">Nigeria</option>
            <option value="NIUE">Niue</option>
            <option value="NORFOLK ISLAND">Norfolk Island</option>
            <option value="NORTH KOREA">North Korea</option>
            <option value="NORTHERN CYPRUS">Northern Cyprus</option>
            <option value="NORTHERN_MARIANA_ILN">Northern Mariana Islands</option>
            <option value="NORWAY">Norway</option>
            <option value="OMAN">Oman</option>
            <option value="PAKISTAN">Pakistan</option>
            <option value="PALAU">Palau</option>
            <option value="PALESTINE">Palestine</option>
            <option value="PANAMA">Panama</option>
            <option value="PAPUA NEW GUINEA">Papua New Guinea</option>
            <option value="PARAGUAY">Paraguay</option>
            <option value="PERU">Peru</option>
            <option value="PHILIPPINES">Philippines</option>
            <option value="PITCAIRN">Pitcairn</option>
            <option value="POLAND">Poland</option>
            <option value="PORTUGAL">Portugal</option>
            <option value="PUERTO RICO">Puerto Rico</option>
            <option value="QATAR">Qatar</option>
            <option value="CONGO">Republic Of The Congo</option>
            <option value="REUNION">Reunion</option>
            <option value="ROMANIA">Romania</option>
            <option value="RUSSIA">Russia</option>
            <option value="RWANDA">Rwanda</option>
            <option value="SAINT BARTHELEMY">Saint Barthelemy</option>
            <option value="SAINT HELENA">Saint Helena</option>
            <option value="SAINT_KITTS_NEVIS">Saint Kitts And Nevis</option>
            <option value="SAINT LUCIA">Saint Lucia</option>
            <option value="SAINT_VINCENT_GREN">Saint Vincent And The Grenadines</option>
            <option value="SAINT-MARTIN">Saint-Martin</option>
            <option value="SAINTPIERRE_MIQUELON">Saint-Pierre And Miquelon</option>
            <option value="SAMOA">Samoa</option>
            <option value="SAN MARINO">San Marino</option>
            <option value="SAO_TOME_PRINCIPE">Sao Tome And Principe</option>
            <option value="SAUDI ARABIA">Saudi Arabia</option>
            <option value="SENEGAL">Senegal</option>
            <option value="SERBIA">Serbia</option>
            <option value="SEYCHELLES">Seychelles</option>
            <option value="SIERRA LEONE">Sierra Leone</option>
            <option value="SINGAPORE">Singapore</option>
            <option value="SLOVAKIA">Slovakia</option>
            <option value="SLOVENIA">Slovenia</option>
            <option value="SOLOMON ISLANDS">Solomon Islands</option>
            <option value="SOMALIA">Somalia</option>
            <option value="SOMALILAND">Somaliland</option>
            <option value="SOUTH AFRICA">South Africa</option>
            <option value="S_GEORGIA_SANDWICH">South Georgia And The South Sandwich Islands</option>
            <option value="SOUTH KOREA">South Korea</option>
            <option value="SOUTH OSSETIA">South Ossetia</option>
            <option value="SRI LANKA">Sri Lanka</option>
            <option value="SUDAN">Sudan</option>
            <option value="SURINAME">Suriname</option>
            <option value="SVALBARD_JAN_MAYEN">Svalbard And Jan Mayen</option>
            <option value="SWAZILAND">Swaziland</option>
            <option value="SWEDEN">Sweden</option>
            <option value="SWITZERLAND">Switzerland</option>
            <option value="SYRIA">Syria</option>
            <option value="TAIWAN">Taiwan</option>
            <option value="TAJIKISTAN">Tajikistan</option>
            <option value="TANZANIA">Tanzania</option>
            <option value="THAILAND">Thailand</option>
            <option value="TOGO">Togo</option>
            <option value="TOKELAU">Tokelau</option>
            <option value="TONGA">Tonga</option>
            <option value="TRANSNISTRIA">Transnistria</option>
            <option value="TRINIDAD AND TOBAGO">Trinidad And Tobago</option>
            <option value="TRISTAN DA CUNHA">Tristan Da Cunha</option>
            <option value="TUNISIA">Tunisia</option>
            <option value="TURKEY">Turkey</option>
            <option value="TURKMENISTAN">Turkmenistan</option>
            <option value="TURKS_CAICOS_ISLANDS">Turks And Caicos Islands</option>
            <option value="TUVALU">Tuvalu</option>
            <option value="UGANDA">Uganda</option>
            <option value="UKRAINE">Ukraine</option>
            <option value="UNITED ARAB EMIRATES">United Arab Emirates</option>
            <option value="US_MINOR_ISLANDS">United States Minor Outlying Islands</option>
            <option value="US_VIRGIN_ISLANDS">United States Virgin Islands</option>
            <option value="URUGUAY">Uruguay</option>
            <option value="UZBEKISTAN">Uzbekistan</option>
            <option value="VANUATU">Vanuatu</option>
            <option value="VATICAN CITY">Vatican City</option>
            <option value="VENEZUELA">Venezuela</option>
            <option value="VIETNAM">Vietnam</option>
            <option value="WALLIS AND FUTUNA">Wallis And Futuna</option>
            <option value="WESTERN SAHARA">Western Sahara</option>
            <option value="YEMEN">Yemen</option>
            <option value="ZAMBIA">Zambia</option>
            <option value="ZIMBABWE">Zimbabwe</option>
        </select>
    </div>
    <div data-role="fieldcontain">
        <label for="<%=IDPrefix%>txtMailingAddress" class="RequiredIcon">Address</label>
        <input type="text" id="<%=IDPrefix%>txtMailingAddress" maxlength="100" name="address" autocomplete="street-address" onchange="<%=callVerifyAddressJSFunction(False, "Mailing")%>"/>
    </div>
    <div data-role="fieldcontain" id="<%=IDPrefix%>divMailingAddress2" style="display: none;">
        <label for="<%=IDPrefix%>txtMailingAddress2">Address Line 2</label>
        <input type="text" id="<%=IDPrefix%>txtMailingAddress2" maxlength="100" name="address2" autocomplete="address-line2" onchange="<%=callVerifyAddressJSFunction(False, "Mailing")%>" />
    </div>
    <div data-role="fieldcontain">
        <label for="<%=IDPrefix%>txtMailingZip" id="<%=IDPrefix%>lblMailingZipTitle" class="RequiredIcon">Zip</label>
        <input type="tel" id="<%=IDPrefix%>txtMailingZip" pattern="[0-9]*" maxlength="10" name="zip" autocomplete="postal-code" onchange="<%=callVerifyAddressJSFunction(True, "Mailing")%>" />
    </div>
    <div data-role="fieldcontain">
        <label for="<%=IDPrefix%>txtMailingCity" id="<%=IDPrefix%>lblMailingCityTitle" class="RequiredIcon">City</label>
        <input type="text" id="<%=IDPrefix%>txtMailingCity" name="city" autocomplete="address-level2" maxlength="50" onchange="<%=callVerifyAddressJSFunction(False, "Mailing")%>" />
    </div>
    
    <div class="<%=IDPrefix%>div_ddl_mailing_state">
        <div data-role="fieldcontain">
            <label for="<%=IDPrefix%>ddlMailingState" class="RequiredIcon">State</label>
            <select id="<%=IDPrefix%>ddlMailingState" name="state" autocomplete="address-level1" onchange="<%=callVerifyAddressJSFunction(False, "Mailing")%>">
                <%--<option value="">--Please Select--</option>--%>
                <%=StateDropdown %>
            </select>
        </div>
    </div>
    <div><span id="<%=IDPrefix%>spVerifyMailingMessage" style="color: Red; display: none;"> Unable to validate address</span></div>
</div>
</form>
</div><%--end mailing div--%>
<%End If%>



<%--Zipcode Lookup And verify address--%>

<script type="text/javascript">
	var <%=IDPrefix%>g_current_full_address = '';
	var <%=IDPrefix%>g_current_full_mailing_address = '';
	var <%=IDPrefix%>g_current_full_previous_address = '';
	var <%=IDPrefix%>g_is_usa_address = true;
	var <%=IDPrefix%>g_is_usa_mailing_address = true;
	var <%=IDPrefix%>g_is_usa_previous_address = true;
    $(document).ready(function () {      
		//current address
		<%=IDPrefix%>g_current_full_address = <%=IDPrefix%>getAddress();
    	//mailing address
    	<%=IDPrefix%>g_current_full_mailing_address =<%=IDPrefix%>getAddress('Mailing');
    	//previous address
    	<%=IDPrefix%>g_current_full_previous_address =<%=IDPrefix%>getAddress('Previous');
    	if (is_foreign_address) {
    		$('#<%=IDPrefix%>div_ddl_country').show();
        	$('#<%=IDPrefix%>div_ddl_mailing_country').show();
        } else {
        	$('#<%=IDPrefix%>div_ddl_country').hide();
        	$('#<%=IDPrefix%>div_ddl_mailing_country').hide();
        }
    	if ($('#<%=IDPrefix%>ddlCountry').val() == 'USA') {
    		$('#<%=IDPrefix%>divAddress2').hide();
        	$("#<%=IDPrefix%>txtZip").off('.alphanum');
		    $("#<%=IDPrefix%>txtZip").attr("maxlength", "10");
        	$("#<%=IDPrefix%>txtZip").attr("pattern", "[0-9]*");
        	if (isMobile.Android()) {
        		$("#<%=IDPrefix%>txtZip").attr("type", "tel");
            } else {
            	$("#<%=IDPrefix%>txtZip").attr("type", "text");
            }
		} else {
			$('#<%=IDPrefix%>divAddress2').show();
        	$("#<%=IDPrefix%>txtZip").attr("maxlength", "10");
        	$("#<%=IDPrefix%>txtZip").attr("type", "text");
        	$("#<%=IDPrefix%>txtZip").alphanum({ allowSpace: true });
        	$("#<%=IDPrefix%>txtZip").removeAttr("pattern");
        }
		<%If EnableMailingAddress then%>
    	if ($('#<%=IDPrefix%>ddlMailingCountry').val() == 'USA') {
    		$('#<%=IDPrefix%>divMailingAddress2').hide();
        	$("#<%=IDPrefix%>txtMailingZip").off('.alphanum');
        	$("#<%=IDPrefix%>txtMailingZip").attr("maxlength", "10");
        	$("#<%=IDPrefix%>txtMailingZip").attr("pattern", "[0-9]*");
        	if (isMobile.Android()) {
        		$("#<%=IDPrefix%>txtMailingZip").attr("type", "tel");
            } else {
            	$("#<%=IDPrefix%>txtMailingZip").attr("type", "text");
            }
		} else {
			$('#<%=IDPrefix%>divMailingAddress2').show();
        	$("#<%=IDPrefix%>txtMailingZip").attr("maxlength", "10");
        	$("#<%=IDPrefix%>txtMailingZip").attr("type", "text");
        	$("#<%=IDPrefix%>txtMailingZip").alphanum({ allowSpace: true });
        	$("#<%=IDPrefix%>txtMailingZip").removeAttr("pattern");
    	}
		<%End If%>
		<%If EnableOccupancyStatus AndAlso IDPrefix <> "m_" Then%>
    	if ($('#<%=IDPrefix%>ddlPreviousCountry').val() == 'USA') {
    		$('#<%=IDPrefix%>divPreviousAddress2').hide();
        	$("#<%=IDPrefix%>txtPreviousZip").off('.alphanum');
        	$("#<%=IDPrefix%>txtPreviousZip").attr("maxlength", "10");
        	$("#<%=IDPrefix%>txtPreviousZip").attr("pattern", "[0-9]*");
        	if (isMobile.Android()) {
        		$("#<%=IDPrefix%>txtPreviousZip").attr("type", "tel");
            } else {
            	$("#<%=IDPrefix%>txtPreviousZip").attr("type", "text");
            }
		} else {
			$('#<%=IDPrefix%>divPreviousAddress2').show();
        	$("#<%=IDPrefix%>txtPreviousZip").attr("maxlength", "10");
        	$("#<%=IDPrefix%>txtPreviousZip").attr("type", "text");
        	$("#<%=IDPrefix%>txtPreviousZip").alphanum({ allowSpace: true });
        	$("#<%=IDPrefix%>txtPreviousZip").removeAttr("pattern");
        }
		<%End If%>
        
    });

	$(function () {
		<%If EnableOccupancyStatus Then %>
			<%If IDPrefix <> "m_" Then%>
			//show and hide previous address for xa and loans
        $('#<%=IDPrefix%>divOccupancyStatusSection').on('change', 'select[id*="ddlOccupancyDuration"]', function () {      
    			<%=IDPrefix%>ShowPreviousAddress();
			});
			<%--$('input#<%=IDPrefix%>txtPreviousZip').blur(function () {
				if ($('#<%=IDPrefix%>ddlPreviousCountry').val() == 'USA') {
					$(this).val(Common.GetPosNumber($(this).val(), 5));
				}
			});--%>
			<%End If%>
			<%=IDPrefix%>updatePaymentTitle();
			$('#<%=IDPrefix%>ddlOccupyingStatus').closest("div.showfield-section").on("change", function(evt, state) {
				if (state == "hide") {
					//reset prev address
					$('#<%=IDPrefix%>ddlOccupancyDurationMonth').val(" ");
					$('#<%=IDPrefix%>ddlOccupancyDurationMonth').selectmenu("refresh");
					$('#<%=IDPrefix%>ddlOccupancyDurationYear').val(" ");
					$('#<%=IDPrefix%>ddlOccupancyDurationYear').selectmenu("refresh");
					<%If IDPrefix <> "m_" Then%>
					<%=IDPrefix%>ShowPreviousAddress();
					<%End If%>
					//reset monthly payment require state
					$('#<%=IDPrefix%>ddlOccupyingStatus').val(" ");
					$('#<%=IDPrefix%>ddlOccupyingStatus').selectmenu("refresh");
					$('#<%=IDPrefix%>ddlOccupyingStatus').trigger("change");
				}
			});
		<%End If%>

		<%--$('input#<%=IDPrefix%>txtZip').blur(function () {
			if ($('#<%=IDPrefix%>ddlCountry').val() == 'USA') {
				$(this).val(Common.GetPosNumber($(this).val(), 5));
		    }

        });
    	$('input#<%=IDPrefix%>txtMailingZip').blur(function () {
    		if ($('#<%=IDPrefix%>ddlMailingCountry').val() == 'USA') {
        		$(this).val(Common.GetPosNumber($(this).val(), 5));
        	}

        });--%>
    	<%If EnableMailingAddress then%>
	    $('#<%=IDPrefix%>lnkHasMaillingAddress').on('click', function (e) {
			var $self = $(this);
	    	var selectedValue = $self.attr('status');
	    	if (selectedValue == "N") {
	    		$self.html('Use same address for mailing');
	    		$self.addClass("chevron-circle-down-before").removeClass("chevron-circle-right-before");
	    		$self.attr('status', 'Y');
	    		$('#<%=IDPrefix%>divMailingAddressForm').show();
			} else {
	    		$self.html("Use different address for mailing");
	    		$self.addClass("chevron-circle-right-before").removeClass("chevron-circle-down-before");
				$self.attr('status', 'N');
				$('#<%=IDPrefix%>divMailingAddressForm').hide();
			}
	    	if (BUTTONLABELLIST != null) {
	    		var value = BUTTONLABELLIST[$.trim($self.html()).toLowerCase()];
	    		if (typeof value == "string" && $.trim(value) !== "") {
	    			$self.html(value);
	    		}
	    	}
		    e.preventDefault();
	    });
		<%End If%>
    });
   
    //deprecated, use verifyAddress
    //this function is redundant with addressverify service
    //leave this  here since addressverify service only work for valid address, may need  to use this later
    function <%=IDPrefix%>lookupZipcode(zip, prefixAddress) {
		prefixAddress = typeof prefixAddress !== 'undefined' ? prefixAddress : "";
		if (prefixAddress == "" && !<%=IDPrefix%>g_is_usa_address) return;
        if (prefixAddress == "Mailing" && !<%=IDPrefix%>g_is_usa_mailing_address) return;
    	if (prefixAddress == "Previous" && !<%=IDPrefix%>g_is_usa_previous_address) return;
    	if (Common.ValidateZipCode($('#<%=IDPrefix%>txt' + prefixAddress + 'Zip').val(), $('#<%=IDPrefix%>txt' + prefixAddress + 'Zip')) == false) return;
		if (typeof zip == "string") {
			zip = zip.replace(/-/g, "");
		}
    	//var address_key = $('#hdAddressKey').val();
    	//if (address_key != "") {
    	var url = '/handler/Handler.aspx';
    	$.ajax({
    		url: url,
    		async: true,
    		cache: false,
    		type: 'POST',
    		dataType: 'html',
    		data: {
    			command: 'lookupZipcode',
    			zip: zip
    		},
    		success: function (responseText) {
    			var result = responseText.split('|');
    			var zip = result[0];
    			var city = result[1];
    			var state = result[2];

    			//$('#<%=IDPrefix%>txtZip').val(zip);
    			$('#<%=IDPrefix%>txt' + prefixAddress + 'City').val(city);
				if (city != "") {
					$.lpqValidate.hideValidation('#<%=IDPrefix%>txt' + prefixAddress + 'City');
				}
    			$('#<%=IDPrefix%>ddl' + prefixAddress + 'State').val(state);
    			if (city != "") {
    				$.lpqValidate.hideValidation('#<%=IDPrefix%>ddl' + prefixAddress + 'State');
				}
    			$('#<%=IDPrefix%>ddl' + prefixAddress + 'State').selectmenu().selectmenu('refresh');
				<%--<%=IDPrefix%>verifyAddress();--%>
            }
        });
    	//}
    }

    function <%=IDPrefix%>showHideState() {
        var $country = $("#<%=IDPrefix%>ddlCountry");
        if (is_foreign_address) {  //update  g_is_usa_address when foreign address is enabled      
          <%=IDPrefix%>g_is_usa_address = $country.val() == 'USA';
        } else { //reset ddlCountry value to "USA" if the foreign_address is disable in xml config and the selected ddlCountry is not "USA"            
            if ($country.val() != "USA") {
               $country.val("USA");
               $country.selectmenu().selectmenu('refresh');
            }            
        }
    	if (<%=IDPrefix%>g_is_usa_address) {
    		$('.<%=IDPrefix%>div_ddl_state').show();
    		$('.<%=IDPrefix%>lblZipTitle').addClass("RequiredIcon");

        	ReplaceButtonLabel($('#<%=IDPrefix%>lblZipTitle'), "Zip");
        	ReplaceButtonLabel($('#<%=IDPrefix%>lblCityTitle'), "City");

    		$('#<%=IDPrefix%>lblCityTitle').addClass("RequiredIcon");
        	$('#<%=IDPrefix%>divAddress2').hide();
        	$("#<%=IDPrefix%>txtZip").off('.alphanum');
        	$("#<%=IDPrefix%>txtZip").attr("maxlength", "10");
            $("#<%=IDPrefix%>txtZip").attr("pattern", "[0-9]*");     
        	//fixbug for CopyPrimaryAddress
            <%--
            if (/^[0-9]{1,5}$/.test($("#<%=IDPrefix%>txtZip").val()) === false) {
        		$("#<%=IDPrefix%>txtZip").val("");
            }
            --%>
            var $zipCode = $("#<%=IDPrefix%>txtZip");
            if ($zipCode.val().length == 5) { //5 digit(xxxxx)
                if (/^[0-9]{1,5}$/.test($zipCode.val()) === false) {
        		    $zipCode.val("");
                }
            } else { //zipcode + 4 ext digits(xxxxx-xxxx)
                var zipCode = $zipCode.val().substring(0, 5);
                var zipCodeExt = $zipCode.val().substring(6, 10);  
                 if (/^[0-9]{1,5}$/.test(zipCode) === false || /^[0-9]{1,4}$/.test(zipCodeExt) === false) {       
        		     $zipCode.val("");
                  }      
            }
			if (isMobile.Android()) {
				$("#<%=IDPrefix%>txtZip").attr("type", "tel");
            } else {
            	$("#<%=IDPrefix%>txtZip").attr("type", "text");
            }
		} else {
			$('.<%=IDPrefix%>div_ddl_state').hide();
    		$('.<%=IDPrefix%>lblZipTitle').removeClass("RequiredIcon");

        	ReplaceButtonLabel($('#<%=IDPrefix%>lblZipTitle'), "Postal Code");
        	ReplaceButtonLabel($('#<%=IDPrefix%>lblCityTitle'), "City/Province");

    		$('#<%=IDPrefix%>lblCityTitle').removeClass("RequiredIcon");
        	$("#<%=IDPrefix%>txtZip").removeAttr("pattern");
        	$("#<%=IDPrefix%>txtZip").attr("maxlength", "10");
        	$("#<%=IDPrefix%>txtZip").alphanum({ allowSpace: true });
        	$("#<%=IDPrefix%>txtZip").attr("type", "text");
        	$('#<%=IDPrefix%>divAddress2').show();
		}
		$.lpqValidate.hideValidation($("#<%=IDPrefix%>txtZip"));
	}
	<%If EnableMailingAddress then%>
    function <%=IDPrefix%>showHideMailingState() {
       var $country = $('#<%=IDPrefix%>ddlMailingCountry');
        if (is_foreign_address) {  //update  g_is_usa_address when foreign address is enabled      
          <%=IDPrefix%>g_is_usa_address = $country.val() == 'USA';
        } else { //reset ddlCountry value to "USA" if the foreign_address is disable in xml config and the selected ddlCountry is not "USA"            
            if ($country.val() != "USA") {
               $country.val("USA");
               $country.selectmenu().selectmenu('refresh');
            }            
        }
		<%=IDPrefix%>g_is_usa_mailing_address = $country.val() == 'USA';
    	if (<%=IDPrefix%>g_is_usa_mailing_address) {
    		$('.<%=IDPrefix%>div_ddl_mailing_state').show();
    		$('.<%=IDPrefix%>lblMailingZipTitle').addClass("RequiredIcon");

        	ReplaceButtonLabel($('#<%=IDPrefix%>lblMailingZipTitle'), "Zip");
        	ReplaceButtonLabel($('#<%=IDPrefix%>lblMailingCityTitle'), "City");

    		$('.<%=IDPrefix%>lblMailingCityTitle').addClass("RequiredIcon");
        	$('#<%=IDPrefix%>divMailingAddress2').hide();
        	$("#<%=IDPrefix%>txtMailingZip").off('.alphanum');
        	$("#<%=IDPrefix%>txtMailingZip").attr("maxlength", "10");
        	$("#<%=IDPrefix%>txtMailingZip").attr("pattern", "[0-9]*").val("");
        	if (isMobile.Android()) {
        		$("#<%=IDPrefix%>txtMailingZip").attr("type", "tel");
            } else {
            	$("#<%=IDPrefix%>txtMailingZip").attr("type", "text");
            }
		} else {
			$('.<%=IDPrefix%>div_ddl_mailing_state').hide();
    		$('.<%=IDPrefix%>lblMailingZipTitle').removeClass("RequiredIcon");

        	ReplaceButtonLabel($('#<%=IDPrefix%>lblMailingZipTitle'), "Postal Code");
        	ReplaceButtonLabel($('#<%=IDPrefix%>lblMailingCityTitle'), "City/Province");

    		$('.<%=IDPrefix%>lblMailingCityTitle').removeClass("RequiredIcon");
        	$("#<%=IDPrefix%>txtMailingZip").removeAttr("pattern");
        	$("#<%=IDPrefix%>txtMailingZip").attr("maxlength", "10");
        	$("#<%=IDPrefix%>txtMailingZip").alphanum({ allowSpace: true });
        	$("#<%=IDPrefix%>txtMailingZip").attr("type", "text");
        	$('#<%=IDPrefix%>divMailingAddress2').show();
        }
	}
	<%End If%>
	<%If EnableOccupancyStatus AndAlso IDPrefix <> "m_" Then%>
    function <%=IDPrefix%>showHidePreviousState() {
        
        var $country = $('#<%=IDPrefix%>ddlPreviousCountry');
        if (is_foreign_address) {  //update  g_is_usa_address when foreign address is enabled      
          <%=IDPrefix%>g_is_usa_previous_address = $country.val() == 'USA';
        } else { //reset ddlCountry value to "USA" if the foreign_address is disable in xml config and the selected ddlCountry is not "USA"            
            if ($country.val() != "USA") {
                $country.val("USA");
                $country.selectmenu().selectmenu('refresh');
            }            
        }             		
    	if (<%=IDPrefix%>g_is_usa_previous_address) {
    		$('.<%=IDPrefix%>div_ddl_previous_state').show();
    		$('.<%=IDPrefix%>lblPreviousZipTitle').addClass("RequiredIcon");

        	ReplaceButtonLabel($('#<%=IDPrefix%>lblPreviousZipTitle'), "Zip");
        	ReplaceButtonLabel($('#<%=IDPrefix%>lblPreviousCityTitle'), "City");

    		$('.<%=IDPrefix%>lblPreviousCityTitle').addClass("RequiredIcon");
        	$('#<%=IDPrefix%>divPreviousAddress2').hide();
        	$("#<%=IDPrefix%>txtPreviousZip").off('.alphanum');
        	$("#<%=IDPrefix%>txtPreviousZip").attr("maxlength", "10");
        	$("#<%=IDPrefix%>txtPreviousZip").attr("pattern", "[0-9]*").val("");
        	if (isMobile.Android()) {
        		$("#<%=IDPrefix%>txtPreviousZip").attr("type", "tel");
            } else {
            	$("#<%=IDPrefix%>txtPreviousZip").attr("type", "text");
            }
		} else {
			$('.<%=IDPrefix%>div_ddl_previous_state').hide();
    		$('.<%=IDPrefix%>lblPreviousZipTitle').removeClass("RequiredIcon");

        	ReplaceButtonLabel($('#<%=IDPrefix%>lblPreviousZipTitle'), "Postal Code");
        	ReplaceButtonLabel($('#<%=IDPrefix%>lblPreviousCityTitle'), "City/Province");

    		$('.<%=IDPrefix%>lblPreviousCityTitle').removeClass("RequiredIcon");
        	$("#<%=IDPrefix%>txtPreviousZip").removeAttr("pattern");
        	$("#<%=IDPrefix%>txtPreviousZip").attr("maxlength", "10");
        	$("#<%=IDPrefix%>txtPreviousZip").alphanum({ allowSpace: true });
        	$("#<%=IDPrefix%>txtPreviousZip").attr("type", "text");
        	$('#<%=IDPrefix%>divPreviousAddress2').show();
        }
	}

	<%End If%>
	function <%=IDPrefix%>verifyAddress(prefixAddress) {
		prefixAddress = typeof prefixAddress !== 'undefined' ? prefixAddress : "";
		if (prefixAddress == "" && !<%=IDPrefix%>g_is_usa_address) return;
        if (prefixAddress == "Mailing" && !<%=IDPrefix%>g_is_usa_mailing_address) return;
    	if (prefixAddress == "Previous" && !<%=IDPrefix%>g_is_usa_previous_address) return;
		if (Common.ValidateZipCode($('#<%=IDPrefix%>txt' + prefixAddress + 'Zip').val(), $('#<%=IDPrefix%>txt' + prefixAddress + 'Zip')) == false) return;
    	var address_key = $('#hdAddressKey').val();

    	if (address_key != "") {
    		var gCurrentAddress =<%=IDPrefix%>g_current_full_address;
        	if (prefixAddress == "Mailing") {
        		gCurrentAddress =<%=IDPrefix%>g_current_full_mailing_address;
            } else if (prefixAddress == "Previous") {
            	gCurrentAddress =<%=IDPrefix%>g_current_full_previous_address;
            }
			var address = <%=IDPrefix%>getAddress(prefixAddress);
        	if (<%=IDPrefix%>validateAddress(prefixAddress) && gCurrentAddress != address) {
        		var url = '/handler/Handler.aspx';
        		$.ajax({
        			url: url,
        			async: true,
        			cache: false,
        			type: 'POST',
        			dataType: 'html',
        			data: {
        				command: 'verifyAddress',
        				address: address
        			},
        			success: function (responseText) {
        				if (responseText.indexOf('res="OK"') < 0) {
        					//address service is down or configuration is bad, dont do anything
        					//TODO: log the error or email admin   

        				} else if (responseText.indexOf('FoundSingleAddress="False"') > -1) { //not found single address                      
        					$('#<%=IDPrefix%>spVerify' + prefixAddress + 'Message').show();
        					var shortDesc = /ShortDescriptive="(.*?)"/g.exec(responseText);
        					if (shortDesc[1] == 'Not Found') {
        						shortDesc[1] = "Unable to Validate Address";
        					}
                        	$('#<%=IDPrefix%>spVerify' + prefixAddress + 'Message').text(shortDesc[1]);
							} else if (responseText.indexOf('FoundSingleAddress="True"') > -1) { //found single address                      
								$('#<%=IDPrefix%>spVerify' + prefixAddress + 'Message').hide();
                            	<%=IDPrefix%>updateAddress(responseText, prefixAddress);
                            } else { //unknow case                      
                            	$('#<%=IDPrefix%>spVerify' + prefixAddress + 'Message').hide();
                            }
                    }
                });
		}
	}
}

function <%=IDPrefix%>getAddress(prefixAddress) {
	prefixAddress = typeof prefixAddress !== 'undefined' ? prefixAddress : "";
	var zip = "";
	if ($('#<%=IDPrefix%>ddl' + prefixAddress + 'Country').val() === 'USA' && $('#<%=IDPrefix%>txt' + prefixAddress + 'Zip').length > 0 && Common.ValidateZipCode($('#<%=IDPrefix%>txt' + prefixAddress + 'Zip').val()) == true) {
		zip = $('#<%=IDPrefix%>txt' + prefixAddress + 'Zip').val().replace(/-/g, "").substring(0, 5);
	}
	var address = 'street=' + $('#<%=IDPrefix%>txt' + prefixAddress + 'Address').val() +
                ($('#<%=IDPrefix%>ddl' + prefixAddress + 'Country').val() === 'USA' ? "" : " " + $('#<%=IDPrefix%>txt' + prefixAddress + 'Address2').val()) +
                    '&city=' + $('#<%=IDPrefix%>txt' + prefixAddress + 'City').val() +
                    '&state=' + $('#<%=IDPrefix%>ddl' + prefixAddress + 'State').val() +
                    '&zip=' + zip;
	return address;
}

	function <%=IDPrefix%>updateAddress(responseText, prefixAddress) {
		prefixAddress = typeof prefixAddress !== 'undefined' ? prefixAddress : "";
		var txtAddress = $('#<%=IDPrefix%>txt' + prefixAddress + 'Address');
        var txtCity = $('#<%=IDPrefix%>txt' + prefixAddress + 'City');
    	var txtZip = $('#<%=IDPrefix%>txt' + prefixAddress + 'Zip');
    	var ddlState = $('#<%=IDPrefix%>ddl' + prefixAddress + 'State');
    	txtAddress.val(/street="(.*?)"/g.exec(responseText)[1]);
    	$.lpqValidate.hideValidation(txtAddress);
    	txtCity.val(/city="(.*?)"/g.exec(responseText)[1]);
    	$.lpqValidate.hideValidation(txtCity);
    	ddlState.val(/state="(.*?)"/g.exec(responseText)[1]).selectmenu().selectmenu('refresh');
    	$.lpqValidate.hideValidation(ddlState);
		if (txtZip.is(":disabled")) {
			<%--When zip field (primary applicent) is locked due to zip pool enabled, do not allow override it by address verify task--%>
			var match = /zip="([0-9]{5}).*?["]/g.exec(responseText);
			if (match != null && match.length > 0 && match[1] == txtZip.val()) {
				$('#<%=IDPrefix%>spVerify' + prefixAddress + 'Message').hide();
			} else {
				$('#<%=IDPrefix%>spVerify' + prefixAddress + 'Message').text("Unable to Validate Address");
				$('#<%=IDPrefix%>spVerify' + prefixAddress + 'Message').show();
			}
		} else {
			txtZip.val(/zip="(.*?)["]/g.exec(responseText)[1]);  //zip code may be 5 or 10 digit
		}
    	$.lpqValidate.hideValidation(txtZip);
    }

    //ok to have missing city and state
    //ok to have missing zip
    function <%=IDPrefix%>validateAddress(prefixAddress) {
		prefixAddress = typeof prefixAddress !== 'undefined' ? prefixAddress : "";
		return (($('#<%=IDPrefix%>txt' + prefixAddress + 'Address').val() != '' &&
                $('#<%=IDPrefix%>txt' + prefixAddress + 'Zip').val() != '') ||
              ($('#<%=IDPrefix%>txt' + prefixAddress + 'Address').val() != '' &&
              $('#<%=IDPrefix%>txt' + prefixAddress + 'City').val() != '' &&
               $('#<%=IDPrefix%>ddl' + prefixAddress + 'State').val() != ''));
	}
</script>

<script type="text/javascript">
	<%--//isOccupancyStatus is only for xa
	//make it show by default
	var isOccupancyStatus = "Y";
	var hdOCCElement = $('#hdOccupancyStatus');
	if (hdOCCElement.length > 0) { //for xa
		isOccupancyStatus = hdOCCElement.val();
		if (isOccupancyStatus == "Y") {
			$('#<%=IDPrefix%>divOccupancyStatus').show();
        } else {
        	$('#<%=IDPrefix%>divOccupancyStatus').hide();
        }
	}
	//isHiddenSecOccupancy() is only for xa
	var hiddenSecOccupancy = false;
	if (typeof isHiddenSecOccupancy == "function") {
		hiddenSecOccupancy = isHiddenSecOccupancy();
	}--%>
	//*****
	if ("<%=IDPrefix%>" == "co_") {
    	document.getElementById("<%=IDPrefix%>divCopyAddress").style.display = "block";
    }

    function <%=IDPrefix%>CopyPrimaryAddress(e) {
		<%=IDPrefix%>showHideState();
    	$("#<%=IDPrefix%>ddlState").val($("#ddlState option:selected").val());
    	$("#<%=IDPrefix%>ddlState").selectmenu().selectmenu('refresh');
    	if ($.trim($("#<%=IDPrefix%>ddlState").val()) !== "") {
    		$.lpqValidate.hideValidation($("#<%=IDPrefix%>ddlState"));
        }
		if (is_foreign_address) {
			$("#<%=IDPrefix%>ddlCountry").val($("#ddlCountry option:selected").val());
			$("#<%=IDPrefix%>ddlCountry").selectmenu().selectmenu('refresh');
        	if ($.trim($("#<%=IDPrefix%>ddlCountry").val()) !== "") {
        		$.lpqValidate.hideValidation($("#<%=IDPrefix%>ddlCountry"));
            }
		}
		$("#<%=IDPrefix%>txtAddress").val($("#txtAddress").val());
    	if (is_foreign_address) {
    		$("#<%=IDPrefix%>txtAddress2").val($("#txtAddress2").val());
        }
		if ($.trim($("#<%=IDPrefix%>txtAddress").val()) !== "") {
    		$.lpqValidate.hideValidation($("#<%=IDPrefix%>txtAddress"));
        }
		$("#<%=IDPrefix%>txtZip").val($("#txtZip").val());
    	if ($.trim($("#<%=IDPrefix%>txtZip").val()) !== "") {
    		$.lpqValidate.hideValidation($("#<%=IDPrefix%>txtZip"));
        }
		$("#<%=IDPrefix%>txtCity").val($("#txtCity").val());
    	if ($.trim($("#<%=IDPrefix%>txtCity").val()) !== "") {
    		$.lpqValidate.hideValidation($("#<%=IDPrefix%>txtCity"));
    	}
	    e.preventDefault();
    }
	//validate applicant address
	function <%=IDPrefix%>ValidateApplicantAddress() {
        <%--var txtAddress = $('#<%=IDPrefix%>txtAddress').val();
        var txtZip = $('#<%=IDPrefix%>txtZip').val();
        var txtCity = $('#<%=IDPrefix%>txtCity').val();
        var ddlState = $('#<%=IDPrefix%>ddlState').val();
        //occupancy status
        var oStatus = $('#<%=IDPrefix%>ddlOccupyingStatus option:selected').val();
        var oDurationYear = $('#<%=IDPrefix%>ddlOccupancyDurationYear').val();
        var oDurationMonth = $('#<%=IDPrefix%>ddlOccupancyDurationMonth').val();
        var strMessage = '';
        if (!Common.ValidateText(txtAddress)) {
            strMessage += 'Please complete the Address field.<br />';
        }
        if (<%=IDPrefix%>g_is_usa_address && isPOBox(txtAddress)) {
            strMessage += txtAddress + " is not accepted. Please enter a different address <br/>";

        }
        if (<%=IDPrefix%>g_is_usa_address && !Common.ValidateText(txtCity)) {
            strMessage += 'Please complete the City field.<br />';
        }
        if (<%=IDPrefix%>g_is_usa_address && !Common.ValidateText(ddlState)) {
            strMessage += 'Please complete the State field.<br />';
        }
        if (<%=IDPrefix%>g_is_usa_address && !Common.ValidateText(txtZip)) {
            strMessage += 'Please complete the ZIP field.<br />';
        }
        //do the validation if occupancy status is visible
        if (!hiddenSecOccupancy) { //skip validate if secondary occupancy is hidden
            if (isOccupancyStatus == "Y") {
                if (!Common.ValidateText(oStatus)) {
                    strMessage += 'Please complete the occupancy status field.<br/>';
                }
                if (!Common.ValidateText(oDurationYear) && !Common.ValidateText(oDurationMonth)
                  || oDurationYear * 12 + oDurationMonth * 1 > 9999) {
                    strMessage += 'Please complete the occupancy duration field.<br />';
                }
            }
        }
        if ($.trim(strMessage) === "") {
            document.getElementById("<%=IDPrefix%>frm_Address").submit();
        }
        return strMessage;--%>
    	if ($.lpqValidate("<%=IDPrefix%>ValidateApplicantAddress")) {
    		document.getElementById("<%=IDPrefix%>frm_Address").submit();
        	return true;
        }
		return false;
	} //end validate applicant address

	function <%=IDPrefix%>registerXAApplicantAddressValidator() {
		$('#<%=IDPrefix%>txtAddress').observer({
    		validators: [
                function (partial) {
                	var address = $(this).val();
                	if (!Common.ValidateText(address)) {
                		return 'Address is required';
                	} else if (<%=IDPrefix%>g_is_usa_address && /^[\sa-zA-Z0-9\.'#/-]+$/.test(address) == false) {
                		//alert("Invalid Address was entered. Allowed charaters: letters A-Z, numbers, hyphens, periods,forwardslashes, dashes, number signs and apostrophes");
                		return 'Invalid Address was entered.  Allowed characters: letters A-Z, numbers, hyphens, periods, forwardslashes, dashes, number signs, and apostrophes.';
					}
                	else {
                		if (<%=IDPrefix%>g_is_usa_address && isPOBox(address)) {
                    		return address + " is not accepted. Please enter a different address";
                    	}
                    	return "";
                    }
                }
            ],
        	validateOnBlur: true,
        	group: "<%=IDPrefix%>ValidateApplicantAddress"
        });
		$('#<%=IDPrefix%>txtZip').observer({
    		validators: [
                function (partial) {
                	if (<%=IDPrefix%>g_is_usa_address && Common.ValidateZipCode($(this).val(), this) == false) {
						return 'Valid zip is required.';
					}
					if (!Common.ValidateText($(this).val())) {
						return "Zip is required";
					}
                    return "";
                }
            ],
        	validateOnBlur: true,
        	group: "<%=IDPrefix%>ValidateApplicantAddress"
        });
		$('#<%=IDPrefix%>txtCity').observer({
    		validators: [
                function (partial) {
                	var city = $(this).val();
                	if (<%=IDPrefix%>g_is_usa_address && !Common.ValidateText(city)) {
                    	return 'City is required';
                	} else if (<%=IDPrefix%>g_is_usa_address && /^[\sa-zA-Z0-9\.'#/-]+$/.test(city) == false) {
                		//alert("Invalid City was entered. Allowed charaters: letters A-Z, numbers, hyphens, periods,forwardslashes, dashes, number signs and apostrophes");
                		return 'Invalid City was entered.  Allowed characters: letters A-Z, numbers, hyphens, periods, forwardslashes, dashes, number signs, and apostrophes.';
                	}
                    return "";
                }
            ],
        	validateOnBlur: true,
        	group: "<%=IDPrefix%>ValidateApplicantAddress"
        });
		$('#<%=IDPrefix%>ddlState').observer({
    		validators: [
                function (partial) {
                	var state = $(this).val();
                	if (<%=IDPrefix%>g_is_usa_address && !Common.ValidateText(state)) {
                    	return 'State is required';
                    }
                    return "";
                }
            ],
        	validateOnBlur: true,
        	group: "<%=IDPrefix%>ValidateApplicantAddress"
        });
		<%If EnableOccupancyStatus Then%>
		$('#<%=IDPrefix%>ddlOccupyingStatus').observer({
    		validators: [
                function (partial) {
                	if ($(this).closest("div.showfield-section").length > 0 && $(this).closest("div.showfield-section").hasClass("hidden")) return "";
                	var oStatus = $(this).val();
                	if (!Common.ValidateText(oStatus)) {
                		return 'Occupancy Status is required';
                	}
                	return "";
                }
    		],
    		validateOnBlur: true,
    		group: "<%=IDPrefix%>ValidateApplicantAddress"
        });
		$('#<%=IDPrefix%>divOccupancyDuration').observer({
    		validators: [
                function (partial) {
                	if ($(this).closest("div.showfield-section").length > 0 && $(this).closest("div.showfield-section").hasClass("hidden")) return "";
                	var oDurationYear = $('#<%=IDPrefix%>ddlOccupancyDurationYear').val();
                	var oDurationMonth = $('#<%=IDPrefix%>ddlOccupancyDurationMonth').val();
                	if (!Common.ValidateText(oDurationYear) && !Common.ValidateText(oDurationMonth)
                              || oDurationYear * 12 + oDurationMonth * 1 > 9999) {
                		return "Valid Occupancy Duration is required";
                	}
                	return "";
                }
            ],
        	validateOnBlur: true,
        	group: "<%=IDPrefix%>ValidateApplicantAddress"
        });

        <%If(IDPrefix = "" Or IDPrefix = "co_") and CollectDescriptionIfOccupancyStatusIsOther.ToUpper() = "Y" then%>
    	$('#<%=IDPrefix%>txtOccupancyDescription').observer({
    		validators: [
                function (partial) {
                	if ($(this).closest("div.showfield-section").length > 0 && $(this).closest("div.showfield-section").hasClass("hidden")) return "";
                	var occupancyDesc = $(this).val();
                	if ($('#<%=IDPrefix%>ddlOccupyingStatus').val().toUpperCase() == "OTHER" && !Common.ValidateText(occupancyDesc)) {
                		return 'Other Occupancy Description is required';
                	}
                	return "";
                }
            ],
        	validateOnBlur: true,
        	group: "<%=IDPrefix%>ValidateApplicantAddress"
        });
		<%End If%>
		<%End If%>
    }

    <%=IDPrefix%>registerXAApplicantAddressValidator();
	//validate applicant mailing address
	function <%=IDPrefix%>ValidateApplicantMailingAddress() {
		<%If EnableMailingAddress Then%>
		if ($('#<%=IDPrefix%>lnkHasMaillingAddress').closest("div.showfield-section").length == 0 || !$('#<%=IDPrefix%>lnkHasMaillingAddress').closest("div.showfield-section").hasClass("hidden")) {
			if ($('#<%=IDPrefix%>lnkHasMaillingAddress').attr('status') == "Y") {
				return $.lpqValidate("<%=IDPrefix%>ValidateApplicantMailingAddress");
			}
		}
		<%End If%>
		return true;
	}//end validate applicant mailing address

	<%If EnableMailingAddress then%>
	function <%=IDPrefix%>registerXAApplicantMailingAddressValidator() {
		$('#<%=IDPrefix%>txtMailingAddress').observer({
    		validators: [
                function (partial) {
                	if ($(this).closest("div.showfield-section").length > 0 && $(this).closest("div.showfield-section").hasClass("hidden")) return "";
                	var address = $(this).val();
                	if (!Common.ValidateText(address)) {
                		return 'Address is required';
                	} else if (<%=IDPrefix%>g_is_usa_mailing_address && /^[\sa-zA-Z0-9\.'#/-]+$/.test(address) == false) {
                		//alert("Invalid Address was entered. Allowed charaters: letters A-Z, numbers, hyphens, periods,forwardslashes, dashes, number signs and apostrophes");
                		return 'Invalid Mailing Address was entered.  Allowed characters: letters A-Z, numbers, hyphens, periods, forwardslashes, dashes, number signs, and apostrophes.';
                	}
                	return "";
                }
    		],
    		validateOnBlur: true,
    		group: "<%=IDPrefix%>ValidateApplicantMailingAddress"
        });
		$('#<%=IDPrefix%>txtMailingZip').observer({
    		validators: [
                function (partial) {
                	if ($(this).closest("div.showfield-section").length > 0 && $(this).closest("div.showfield-section").hasClass("hidden")) return "";
                	if (<%=IDPrefix%>g_is_usa_mailing_address && Common.ValidateZipCode($(this).val(), this) == false) {
                    	return 'Valid zip is required';
                    }
                    return "";
                }
            ],
        	validateOnBlur: true,
        	group: "<%=IDPrefix%>ValidateApplicantMailingAddress"
        });
		$('#<%=IDPrefix%>txtMailingCity').observer({
    		validators: [
                function (partial) {
                	if ($(this).closest("div.showfield-section").length > 0 && $(this).closest("div.showfield-section").hasClass("hidden")) return "";
                	var city = $(this).val();
                	if (<%=IDPrefix%>g_is_usa_mailing_address && !Common.ValidateText(city)) {
                    	return 'City is required';
                	} else if (<%=IDPrefix%>g_is_usa_mailing_address && /^[\sa-zA-Z0-9\.'#/-]+$/.test(city) == false) {
                		//alert("Invalid City was entered. Allowed charaters: letters A-Z, numbers, hyphens, periods,forwardslashes, dashes, number signs and apostrophes");
                		return 'Invalid Mailing City was entered.  Allowed characters: letters A-Z, numbers, hyphens, periods, forwardslashes, dashes, number signs, and apostrophes.';
                	}
                    return "";
                }
            ],
        	validateOnBlur: true,
        	group: "<%=IDPrefix%>ValidateApplicantMailingAddress"
        });
		$('#<%=IDPrefix%>ddlMailingState').observer({
    		validators: [
                function (partial) {
                	if ($(this).closest("div.showfield-section").length > 0 && $(this).closest("div.showfield-section").hasClass("hidden")) return "";
                	var state = $(this).val();
                	if (<%=IDPrefix%>g_is_usa_mailing_address && !Common.ValidateText(state)) {
                    	return 'State is required';
                    }
                    return "";
                }
            ],
        	validateOnBlur: true,
        	group: "<%=IDPrefix%>ValidateApplicantMailingAddress"
        });
	}

	<%=IDPrefix%>registerXAApplicantMailingAddressValidator();
	<%End If%>

	<%If EnableOccupancyStatus Then%>
		//validate previous address
		function <%=IDPrefix%>ValidateApplicantPreviousAddress() {
			var ShowPreviousAddress = $('#<%=IDPrefix%>hdShowPreviousAddress').val();
			<%--var strMessage = '';
			if ('<%=IDPrefix%>' == 'm_') {
				return ''; //no previous address for minor
			}
			if (ShowPreviousAddress == "Y") {
				var txtAddress = $('#<%=IDPrefix%>txtPreviousAddress').val();
				var txtZip = $('#<%=IDPrefix%>txtPreviousZip').val();
				var txtCity = $('#<%=IDPrefix%>txtPreviousCity').val();
				var ddlState = $('#<%=IDPrefix%>ddlPreviousState').val();
				if (!Common.ValidateText(txtAddress)) {
					strMessage += "&nbsp&nbsp  Incomplete Previous Address field. <br/>";
				}
				if (<%=IDPrefix%>g_is_usa_previous_address && isPOBox(txtAddress)) {
					strMessage += txtAddress + " is not accepted. Please enter a different previous address <br/>";
				}
				if (<%=IDPrefix%>g_is_usa_previous_address && !Common.ValidateText(txtCity)) {
					strMessage += "&nbsp&nbsp Incomplete Previous City field. <br/>";
				}
				if (<%=IDPrefix%>g_is_usa_previous_address && !Common.ValidateText(ddlState)) {
					strMessage += "&nbsp&nbsp Incomplete Previous State field.<br />";
				}
				if (<%=IDPrefix%>g_is_usa_previous_address && !Common.ValidateText(txtZip)) {
					strMessage += "&nbsp&nbsp Incomplete Previous ZIP field.<br />";
				}
			}//end ShowPreviousAddress ...
			if (strMessage != "") {
				strMessage = "The previous address is incompleted <br/>" + strMessage;
			}
			return strMessage;--%>
			<%If IDPrefix <> "m_" Then%>
			if (ShowPreviousAddress == "Y") {
				return $.lpqValidate("<%=IDPrefix%>ValidateApplicantPreviousAddress");
    		}
			<%End If%>
			return true;
		} // end validate previous address

		function <%=IDPrefix%>registerXAApplicantPreviousAddressValidator() {
			$('#<%=IDPrefix%>txtPreviousAddress').observer({
    			validators: [
					function (partial) {
                		var address = $(this).val();
                		if (!Common.ValidateText(address)) {
                			return 'Address is required';
                		} else if (<%=IDPrefix%>g_is_usa_previous_address && /^[\sa-zA-Z0-9\.'#/-]+$/.test(address) == false) {
                			//alert("Invalid Address was entered. Allowed charaters: letters A-Z, numbers, hyphens, periods,forwardslashes, dashes, number signs and apostrophes");
                			return 'Invalid Previous Address was entered.  Allowed characters: letters A-Z, numbers, hyphens, periods, forwardslashes, dashes, number signs, and apostrophes.';
                		}
                		else {
                			if (<%=IDPrefix%>g_is_usa_previous_address && isPOBox(address)) {
                				return address + " is not accepted. Please enter a different address";
                			}
                			return "";
                		}
					}
    			],
				validateOnBlur: true,
				group: "<%=IDPrefix%>ValidateApplicantPreviousAddress"
			});
			$('#<%=IDPrefix%>txtPreviousZip').observer({
    			validators: [
					function (partial) {
						if (<%=IDPrefix%>g_is_usa_previous_address && Common.ValidateZipCode($(this).val(), this) == false) {
                			return 'Zip is required';
                		}
                		return "";
					}
    			],
				validateOnBlur: true,
				group: "<%=IDPrefix%>ValidateApplicantPreviousAddress"
			});
			$('#<%=IDPrefix%>txtPreviousCity').observer({
    			validators: [
					function (partial) {
                		var city = $(this).val();
                		if (<%=IDPrefix%>g_is_usa_previous_address && !Common.ValidateText(city)) {
                			return 'City is required';
                		} else if (<%=IDPrefix%>g_is_usa_previous_address && /^[\sa-zA-Z0-9\.'#/-]+$/.test(city) == false) {
                			//alert("Invalid City was entered. Allowed charaters: letters A-Z, numbers, hyphens, periods,forwardslashes, dashes, number signs and apostrophes");
                			return 'Invalid Previous City was entered.  Allowed characters: letters A-Z, numbers, hyphens, periods, forwardslashes, dashes, number signs, and apostrophes.';
                		}
                		return "";
					}
    			],
				validateOnBlur: true,
				group: "<%=IDPrefix%>ValidateApplicantPreviousAddress"
			});
			$('#<%=IDPrefix%>ddlPreviousState').observer({
    			validators: [
					function (partial) {
                		var state = $(this).val();
                		if (<%=IDPrefix%>g_is_usa_previous_address && !Common.ValidateText(state)) {
                			return 'State is required';
                		}
                		return "";
					}
    			],
				validateOnBlur: true,
				group: "<%=IDPrefix%>ValidateApplicantPreviousAddress"
			});
		}

		<%=IDPrefix%>registerXAApplicantPreviousAddressValidator();

		function <%=IDPrefix%>updatePaymentTitle() {
			if ($('#<%=IDPrefix%>ddlOccupyingStatus').length == 0) return;
			var sHousingStatus = "";
            sHousingStatus = $('#<%=IDPrefix%>ddlOccupyingStatus option:selected').val();
            //make monthly mortgate/Rent payment is optional field for joint
			// Make housing expense required conditionally based on occupancy/housing status
			const housingStatusForRequiredPayment = <%=JsonConvert.SerializeObject(OccupancyStatusesForRequiredHousingPayment)%>;
    		if (_.indexOf(housingStatusForRequiredPayment, sHousingStatus) >= 0 && '<%=IDPrefix%>' !='co_') {
    			$('#<%=IDPrefix%>lblTotalMonthlyHousingExpense').addClass("RequiredIcon"); // required
			} else {
    			$('#<%=IDPrefix%>lblTotalMonthlyHousingExpense').removeClass("RequiredIcon"); // not required  
        		$.lpqValidate.hideValidation($('#<%=IDPrefix%>txtTotalMonthlyHousingExpense'));
			}
			<%If (IDPrefix = "" Or IDPrefix = "co_") And CollectDescriptionIfOccupancyStatusIsOther.ToUpper() = "Y" Then%>
			if (sHousingStatus.toUpperCase() == "OTHER") {
				$("#<%=IDPrefix%>divCollectDescriptionIfOccupancyStatusIsOther").show();
    		} else {
    			$("#<%=IDPrefix%>divCollectDescriptionIfOccupancyStatusIsOther").hide();
    			$.lpqValidate.hideValidation($('#<%=IDPrefix%>txtOccupancyDescription'));
    		}
			<%End If%>
		}

		function <%=IDPrefix%>HandleOccupancyStatusChange() {
			<%If (IDPrefix = "" Or IDPrefix = "co_") And CollectDescriptionIfOccupancyStatusIsOther.ToUpper() = "Y" Then%>
			var sHousingStatus = "";
			sHousingStatus = $('#<%=IDPrefix%>ddlOccupyingStatus option:selected').val();
			if (sHousingStatus.toUpperCase() == "OTHER") {
				$("#<%=IDPrefix%>divCollectDescriptionIfOccupancyStatusIsOther").show();
			} else {
				$("#<%=IDPrefix%>divCollectDescriptionIfOccupancyStatusIsOther").hide();
				$.lpqValidate.hideValidation($('#<%=IDPrefix%>txtOccupancyDescription'));
			}
			<%End If%>
			<%=IDPrefix%>updatePaymentTitle();
		}
		<%If IDPrefix <> "m_" Then%>
		function <%=IDPrefix%>SetApplicantPreviousAddress(appInfo) {
			appInfo.<%=IDPrefix%>HasPreviousAddress = $('#<%=IDPrefix%>hdShowPreviousAddress').val();
			if (appInfo.<%=IDPrefix%>HasPreviousAddress == "Y") {
				appInfo.<%=IDPrefix%>PreviousAddressStreet = $("#<%=IDPrefix%>txtPreviousAddress").val();
    			appInfo.<%=IDPrefix%>PreviousAddressZip = $("#<%=IDPrefix%>txtPreviousZip").val();
    			appInfo.<%=IDPrefix%>PreviousAddressCity = $("#<%=IDPrefix%>txtPreviousCity").val();
    			if ($("#<%=IDPrefix%>ddlPreviousCountry").val() == "USA") {  //not support on backend so not visible here anyway
    				appInfo.<%=IDPrefix%>PreviousAddressState = $('#<%=IDPrefix%>ddlPreviousState').val();
        		} else {
        			appInfo.<%=IDPrefix%>PreviousAddressState = "";
        			appInfo.<%=IDPrefix%>PreviousAddressStreet2 = $("#<%=IDPrefix%>txtPreviousAddress2").val(); //not support on backend
        		}
				appInfo.<%=IDPrefix%>PreviousAddressCountry = $('#<%=IDPrefix%>ddlPreviousCountry').val();
    		}
			return appInfo;
		}
		<%End If%>
		function <%=IDPrefix%>ShowPreviousAddress() {
			var oDurationTotalInMonth = 0;
			var oDurationMonth = $('#<%=IDPrefix%>ddlOccupancyDurationMonth').val();
			var oDurationYear = $('#<%=IDPrefix%>ddlOccupancyDurationYear').val();
			var selectedMonth = false, selectedYear = false;

			if (oDurationMonth.trim() == "") {
				oDurationMonth = 0;
			} else {
				oDurationMonth = parseInt(oDurationMonth);
				selectedMonth = true;
			}
			//duration year
			if (oDurationYear.trim() == "") {
				oDurationYear = 0;
			} else {
				oDurationYear = parseInt(oDurationYear) * 12;//convert year to month
				selectedYear = true;
			}
			//calculate total duration in months
			oDurationTotalInMonth = oDurationMonth + oDurationYear;
			$('#<%=IDPrefix%>hdShowPreviousAddress').val("N");
    		$('#<%=IDPrefix%>divPreviousAddress').hide();
			if (selectedYear || selectedMonth) {
				var previous_address_threshold = $('#hdPrevAddressThreshold').val();
				if (previous_address_threshold != "") {
					if (!isNaN(previous_address_threshold)) {
						if (oDurationTotalInMonth < parseInt(previous_address_threshold)) {
							$('#<%=IDPrefix%>divPreviousAddress').show();
    						$('#<%=IDPrefix%>hdShowPreviousAddress').val("Y");
    					}
					}
				}
			} //end selectedYear ...
		}//end showPreviousAddress
	<%End If%>

	//set Applicant address function
    function <%=IDPrefix%>SetApplicantAddress(appInfo) {       
		appInfo.<%=IDPrefix%>AddressStreet = $("#<%=IDPrefix%>txtAddress").val();
	    appInfo.<%=IDPrefix%>AddressZip = $("#<%=IDPrefix%>txtZip").val();
        appInfo.<%=IDPrefix%>AddressCity = $("#<%=IDPrefix%>txtCity").val();    
    	if ($("#<%=IDPrefix%>ddlCountry").val() == "USA") {
    		appInfo.<%=IDPrefix%>AddressState = $('#<%=IDPrefix%>ddlState').val();
        } else {
        	appInfo.<%=IDPrefix%>AddressState = "";
        	appInfo.<%=IDPrefix%>AddressStreet2 = $("#<%=IDPrefix%>txtAddress2").val();
        }
		appInfo.<%=IDPrefix%>Country = $("#<%=IDPrefix%>ddlCountry").val();


		<%If EnableOccupancyStatus Then%>
		if ($('#<%=IDPrefix%>ddlOccupyingStatus').closest("div.showfield-section").length == 0 || $('#<%=IDPrefix%>ddlOccupyingStatus').closest("div.showfield-section").hasClass("hidden") == false) {
			var occupancyStatus = $("#<%=IDPrefix%>ddlOccupyingStatus option:selected").val();
			appInfo.<%=IDPrefix%>OccupyingLocation = occupancyStatus;
			var year = $("#<%=IDPrefix%>ddlOccupancyDurationYear").val();
			var month = $("#<%=IDPrefix%>ddlOccupancyDurationMonth").val();
			var x = $.trim(year) == '' ? 0 : parseInt(year);
			var y = $.trim(month) == '' ? 0 : parseInt(month);
			appInfo.<%=IDPrefix%>LiveMonths = x * 12 + y;
            	appInfo.<%=IDPrefix%>OccupancyDescription = "";
                <%If (IDPrefix = "" Or IDPrefix = "co_") And CollectDescriptionIfOccupancyStatusIsOther.ToUpper() = "Y" Then%>
			if (appInfo.<%=IDPrefix%>OccupyingLocation.toUpperCase() == "OTHER") {
				appInfo.<%=IDPrefix%>OccupancyDescription = $("#<%=IDPrefix%>txtOccupancyDescription").val();
            	}
                <%End If%>
          
    	}
		<%End If%>
        <% If EnableHousingPayment Then %>
                appInfo.<%=IDPrefix%>HousingPayment = Common.GetFloatFromMoney($("#<%=IDPrefix%>txtHousingPayment").val());  
        <%End If %>
		return appInfo;
    } //end set applicant address function

	<%--function <%=IDPrefix%>CopyMailingAddress() {
		if ($('#<%=IDPrefix%>lnkHasMaillingAddress').attr('status') == 'Y') {
    		$('#cBillingAddress').val($("#<%=IDPrefix%>txtMailingAddress").val());
    		$('#cBillingZip').val($("#<%=IDPrefix%>txtMailingZip").val());
    		$('#cBillingCity').val($("#<%=IDPrefix%>txtMailingCity").val());
    		$('#cBillingState').val($('#<%=IDPrefix%>ddlMailingState').val());
    		$('#cBillingState').selectmenu();
    		$('#cBillingState').selectmenu('refresh');
    	} else {
    		$('#cBillingAddress').val($("#<%=IDPrefix%>txtAddress").val());
    		$('#cBillingZip').val($("#<%=IDPrefix%>txtZip").val());
    		$('#cBillingCity').val($("#<%=IDPrefix%>txtCity").val());
    		$('#cBillingState').val($('#<%=IDPrefix%>ddlState').val());
		    $('#cBillingState').selectmenu();
    		$('#cBillingState').selectmenu('refresh');
    	}
    }

	function <%=IDPrefix%>ClearCopyMailingAddress() {
    	$('#cBillingAddress').val("");
    	$('#cBillingZip').val("");
    	$('#cBillingCity').val("");
    	$('#cBillingState').val("");
    	$('#cBillingState').selectmenu('refresh');
    }--%>

    

	function <%=IDPrefix%>SetApplicantMailingAddress(appInfo) {
		<%If EnableMailingAddress then%>
		if ($('#<%=IDPrefix%>lnkHasMaillingAddress').closest("div.showfield-section").length == 0 || !$('#<%=IDPrefix%>lnkHasMaillingAddress').closest("div.showfield-section").hasClass("hidden")) {
			appInfo.<%=IDPrefix%>hasMailingAddress = $('#<%=IDPrefix%>lnkHasMaillingAddress').attr('status');
			if (appInfo.<%=IDPrefix%>hasMailingAddress == "Y") {
				appInfo.<%=IDPrefix%>MailingAddressStreet = $("#<%=IDPrefix%>txtMailingAddress").val();
    			appInfo.<%=IDPrefix%>MailingAddressZip = $("#<%=IDPrefix%>txtMailingZip").val();
    			appInfo.<%=IDPrefix%>MailingAddressCity = $("#<%=IDPrefix%>txtMailingCity").val();
    			if ($("#<%=IDPrefix%>ddlMailingCountry").val() == "USA") {
    				appInfo.<%=IDPrefix%>MailingAddressState = $('#<%=IDPrefix%>ddlMailingState').val();
        		} else {
        			appInfo.<%=IDPrefix%>MailingAddressState = "";
        			appInfo.<%=IDPrefix%>MailingAddressStreet2 = $("#<%=IDPrefix%>txtMailingAddress2").val();
        		}
				appInfo.<%=IDPrefix%>MailingAddressCountry = $("#<%=IDPrefix%>ddlMailingCountry").val();
    		}
		}
		<%End If%>
		return appInfo;
	}


	

	function <%=IDPrefix%>ViewAddress() {
		var txtAddress = htmlEncode($('#<%=IDPrefix%>txtAddress').val());
    	var txtAddress2 = htmlEncode($('#<%=IDPrefix%>txtAddress2').val());
    	var txtZip = htmlEncode($('#<%=IDPrefix%>txtZip').val());
    	var txtCity = htmlEncode($('#<%=IDPrefix%>txtCity').val());
    	var ddlState = $('#<%=IDPrefix%>ddlState').val();
    	var ddlCountry = $("#<%=IDPrefix%>ddlCountry option:selected");
		
    	
    	var strViewAddress = "";

    	var strCurrentAddress = "";

    	strCurrentAddress += txtAddress;
    	if (<%=IDPrefix%>g_is_usa_address == false) {
        	strCurrentAddress += ", " + txtAddress2;
        }
        if (txtCity.trim() != "") {
        	strCurrentAddress += ", " + txtCity.trim();
        }
        if (<%=IDPrefix%>g_is_usa_address) {
        	strCurrentAddress += ", " + ddlState;
        }
        if (txtZip != "") {
        	strCurrentAddress += " " + txtZip.trim();
        }
        if (ddlCountry.val() != "USA") {
        	strCurrentAddress += " " + ddlCountry.val();
        }
		if ($.trim(strCurrentAddress.replace(",", "")) !== "") {
			strViewAddress += '<div class="col-sm-6 col-xs-12"><div class="row"><div class="col-xs-6 text-right row-title"><span class="bold">Current Physical Address</span></div><div class="col-xs-6 text-left row-data"><span>' + strCurrentAddress + '</span></div></div></div>';
		}
        
		<%If EnableOccupancyStatus Then %>
		//occupancy status
		var oStatus = $('#<%=IDPrefix%>ddlOccupyingStatus option:selected').val();
		var oDurationYear = $('#<%=IDPrefix%>ddlOccupancyDurationYear').val();
		var oDurationMonth = $('#<%=IDPrefix%>ddlOccupancyDurationMonth').val();
		var ShowPreviousAddress = $('#<%=IDPrefix%>hdShowPreviousAddress').val();
		
		if ($('#<%=IDPrefix%>ddlOccupyingStatus').closest("div.showfield-section").length == 0 || $('#<%=IDPrefix%>ddlOccupyingStatus').closest("div.showfield-section").hasClass("hidden") == false) {
			if ($.trim(oStatus) !== "") {
				strViewAddress += '<div class="col-sm-6 col-xs-12"><div class="row"><div class="col-xs-6 text-right row-title"><span class="bold">Occupancy Status</span></div><div class="col-xs-6 text-left row-data"><span>' + oStatus + '</span></div></div></div>';
			}


                <%If (IDPrefix = "" Or IDPrefix = "co_") And CollectDescriptionIfOccupancyStatusIsOther.ToUpper() = "Y" Then%>
			if (oStatus.toUpperCase() == "OTHER") {
				strViewAddress += '<div class="col-sm-6 col-xs-12"><div class="row"><div class="col-xs-6 text-right row-title"><span class="bold">Other Occupancy Description</span></div><div class="col-xs-6 text-left row-data"><span>' + $("#<%=IDPrefix%>txtOccupancyDescription").val() + '</span></div></div></div>';
            	}
        		<%End If%>
			var strOccupancyDur = (Number(oDurationYear) > 0 ? oDurationYear + ' yrs' : "") + " " + (Number(oDurationMonth) > 0 ? oDurationMonth + ' mos ' : "");
			if ($.trim(strOccupancyDur) !== "") {
				strViewAddress += '<div class="col-sm-6 col-xs-12"><div class="row"><div class="col-xs-6 text-right row-title"><span class="bold">Occupancy Duration</span></div><div class="col-xs-6 text-left row-data"><span>' + strOccupancyDur + '</span></div></div></div>';
			}


			if ('<%=IDPrefix%>' != 'm_') {
				//previous address
				var txtPrevAddress = htmlEncode($('#<%=IDPrefix%>txtPreviousAddress').val());
				var txtPrevAddress2 = htmlEncode($('#<%=IDPrefix%>txtPreviousAddress2').val());
				var txtPrevZip = htmlEncode($('#<%=IDPrefix%>txtPreviousZip').val());
				var txtPrevCity = htmlEncode($('#<%=IDPrefix%>txtPreviousCity').val());
				var ddlPrevState = $('#<%=IDPrefix%>ddlPreviousState').val();
				var ddlPrevCountry = $("#<%=IDPrefix%>ddlPreviousCountry option:selected");
		        if (ShowPreviousAddress == 'Y') {
		        	var strPreviousAddress = "";
		        	strPreviousAddress += txtPrevAddress;
		        	if (<%=IDPrefix%>g_is_usa_previous_address == false) {
                		strPreviousAddress += ", " + txtPrevAddress2;
                	}
                	if (txtPrevCity != "") {
                		strPreviousAddress += ", " + txtPrevCity;
                	}
                	if (<%=IDPrefix%>g_is_usa_previous_address) {
                        strPreviousAddress += ", " + ddlPrevState;
                    }
                    if (txtPrevZip != "") {
                        strPreviousAddress += " " + txtPrevZip;
                    }
                    if (ddlPrevCountry.val() != "USA") {
                        strPreviousAddress += " " + ddlPrevCountry.val();
                    }
                    strViewAddress += '<div class="col-sm-6 col-xs-12"><div class="row"><div class="col-xs-6 text-right row-title"><span class="bold">Previous Address</span></div><div class="col-xs-6 text-left row-data"><span>' + strPreviousAddress + '</span></div></div></div>';
				}
            }
            
		}
		<%End If%>
        //monthlyhousing payment
          <% If EnableHousingPayment Then %>  
            if ($('#<%=IDPrefix%>divHousingPayment').hasClass('hidden')) {
               $('#<%=IDPrefix%>txtHousingPayment').val(""); //clear txtHousingPayment if it is hidden
            } else {
                var txtHousingPayment = $('#<%=IDPrefix%>txtHousingPayment').val();
                if (Common.ValidateText(txtHousingPayment)) {
			        txtHousingPayment = Common.FormatCurrency(txtHousingPayment, true);
			        strViewAddress += '<div class="col-sm-6 col-xs-12"><div class="row"><div class="col-xs-6 text-right row-title"><span class="bold">Housing Payment</span></div><div class="col-xs-6 text-left row-data"><span>' + txtHousingPayment + '</span></div></div></div>';
		        }
            }          
         <%End If %>
		<%If EnableMailingAddress then%>
		if ($('#<%=IDPrefix%>lnkHasMaillingAddress').closest("div.showfield-section").length == 0 || !$('#<%=IDPrefix%>lnkHasMaillingAddress').closest("div.showfield-section").hasClass("hidden")) {
			//mailing address
			var ShowMailingAddress = $('#<%=IDPrefix%>lnkHasMaillingAddress').attr('status');
			var txtMailAddress = htmlEncode($('#<%=IDPrefix%>txtMailingAddress').val());
			var txtMailAddress2 = htmlEncode($('#<%=IDPrefix%>txtMailingAddress2').val());
			var txtMailZip = htmlEncode($('#<%=IDPrefix%>txtMailingZip').val());
			var txtMailCity = htmlEncode($('#<%=IDPrefix%>txtMailingCity').val());
			var ddlMailState = $('#<%=IDPrefix%>ddlMailingState').val();
			var ddlMailingCountry = $("#<%=IDPrefix%>ddlMailingCountry option:selected");
			if (ShowMailingAddress == "Y") {
				var strMailingAddress = "";

				strMailingAddress += txtMailAddress;
				if (<%=IDPrefix%>g_is_usa_mailing_address == false) {
					strMailingAddress += ", " + txtMailAddress2;
				}
				if (txtMailCity != "") {
					strMailingAddress += ", " + txtMailCity;
				}
				if (<%=IDPrefix%>g_is_usa_mailing_address) {
            		strMailingAddress += ", " + ddlMailState;
				}
				if (txtMailZip != "") {
            		strMailingAddress += " " + txtMailZip;
				}
				if (ddlMailingCountry.val() != "USA") {
            		strMailingAddress += " " + ddlMailingCountry.val();
				}
				strViewAddress += '<div class="col-sm-6 col-xs-12"><div class="row"><div class="col-xs-6 text-right row-title"><span class="bold">Mailing Address</span></div><div class="col-xs-6 text-left row-data"><span>' + strMailingAddress + '</span></div></div></div>';
			} else {
				if ($.trim(strCurrentAddress.replace(",", "")) !== "") {
					strViewAddress += '<div class="col-sm-6 col-xs-12 row-title"><span>Mailing address is the same as current physical address</span></div>';
				}
			}
		}
		
		<%End If%>
		$(this).hide();
		$(this).prev("div.row").hide();
		if (strViewAddress !== "") {
			$(this).show();
			$(this).prev("div.row").show();
		}
		return strViewAddress;
	}//end viewAddress()

	//handle hide and show mailing address form
	//function handledShowAndHideMailingAddress(coPrefix) {
	//    $('#' + coPrefix + 'lnkHasMaillingAddress').on('click', function () {
	//        ShowMailingAddress(this, coPrefix);
	//    });
	//}

    function <%=IDPrefix%>autoFillData_Address() {   
    	$('#<%=IDPrefix%>txtAddress').val("1234 Square Circle");
    	$('#<%=IDPrefix%>txtZip').val("92660");
        $('#<%=IDPrefix%>txtCity').val("Newport Beach");
        $('#<%=IDPrefix%>ddlState').val("CA").selectmenu().selectmenu("refresh");
    	$('#<%=IDPrefix%>ddlOccupyingStatus').val("RENT").selectmenu().selectmenu("refresh");
    	$('#<%=IDPrefix%>ddlOccupancyDurationMonth').val("11").selectmenu().selectmenu("refresh");
        $('#<%=IDPrefix%>ddlOccupancyDurationYear').val("11").selectmenu().selectmenu("refresh");
    }


</script>

