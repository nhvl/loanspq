﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="InstaTouchStuff.ascx.vb" Inherits="Inc_MainApp_InstaTouchStuff" %>
<div id="istPrefillUnAvailablePage" data-role="dialog" data-history="false" data-close-btn="none" style="width: 100%;">
	<div data-role="header">
		<button class="header-hidden-btn">.</button>
		<div class="page-header-title">Information</div>
		<div tabindex="0" onclick="IST.FACTORY.goToPersonalInfoPage()" class="header_theme2 ui-btn-right" data-role="none">
			<%=HeaderUtils.IconClose %><span style="display: none;">close</span>
		</div>
	</div>
	<div data-role="content">
		<div class="rename-able dialog-content">We are sorry, but the identify prefill feature is currently unavailable.</div>
		<br/>
		<div class ="div-continue" style="width: 100%;">
			<a href="#" data-role="button" type="button" onclick="IST.FACTORY.goToPersonalInfoPage()" class="button-style div-continue-button">Continue</a>
		</div>    
	</div>
</div>
<div id="istWifiPrefillApplicationOptionPage" data-role="dialog" data-history="false" data-close-btn="none" style="width: 100%;">
	<div data-role="header">
		<button class="header-hidden-btn">.</button>
		<div class="page-header-title">Prefill Application Option</div>
		<div tabindex="0" onclick="IST.FACTORY.goToPersonalInfoPage()" class="header_theme2 ui-btn-right" data-role="none">
			<%=HeaderUtils.IconClose %><span style="display: none;">close</span>
		</div>
	</div>
	<div data-role="content">
		<div class="rename-able dialog-content" style="margin: 20px 10px;">
			<p>Applying just got easier!</p>
			<p>Verifying your identity now will enable us to prefill your application. Enter your personal mobile phone number, and we will send a validation code.</p>
		</div>
		<div data-role="fieldcontain">
            <label for="txtISTPhoneNumber" class="RequiredIcon">Phone Number</label>
            <input type="tel" id="txtISTPhoneNumber" data-required="true" data-format="phone" data-message-require="Phone Number is required" maxlength="10" />
        </div>
		<div class ="div-continue" style="width: 100%;">
			<a href="#" data-role="button" type="button" onclick="IST.FACTORY.processWifiConsent()" class="button-style div-continue-button">Prefill My Data</a>
			<a href="#" data-role="button" type="button" onclick="IST.FACTORY.goToPersonalInfoPage()" class="button-style div-continue-button-reverse hover-goback">No, thanks</a>
		</div>        
        <!-- #include file="./InstaTouchStuffConsent.html" -->
	</div>
</div>
<div id="istEnterCodePage" data-role="dialog" data-history="false" data-close-btn="none" style="width: 100%;">
	<div data-role="header">
        <button class="header-hidden-btn">.</button>
		<div class="page-header-title">Enter Code</div>
		<div tabindex="0" onclick="IST.FACTORY.goToPersonalInfoPage()" Class="header_theme2 ui-btn-right" data-role="none">
			<%=HeaderUtils.IconClose %><span style="display: none;">close</span>
		</div>
	</div>
	<div data-role="content">
		<div class="rename-able dialog-content" style="margin: 20px 10px;">
			<p>Please enter the validation code that was sent to your mobile phone.</p>
		</div>
		<div data-role="fieldcontain">
            <label for="txtISTValidationCode" class="RequiredIcon">Validation Code</label>
            <input type="text" id="txtISTValidationCode" data-required="true" data-message-require="Validation Code is required" maxlength="20" autocomplete="off" autocorrect="off" autocapitalize="none" />
        </div>
		<br/>
		<div class ="div-continue" style="width: 100%;">
			<a href="#" data-role="button" type="button" onclick="IST.FACTORY.validateOtp()" class="button-style div-continue-button">Confirm Code</a>
			<a href="#" data-role="button" type="button" onclick="IST.FACTORY.goToPersonalInfoPage()" class="button-style div-continue-button-reverse hover-goback">Cancel</a>
		</div>
	</div>
</div>
<div id="istAuthenticatePage" data-role="dialog" data-history="false" data-close-btn="none" style="width: 100%;">
	<div data-role="header">
		<button class="header-hidden-btn">.</button>
		<div class="page-header-title">Authenticate</div>
		<div tabindex="0" onclick="IST.FACTORY.goToPersonalInfoPage()" class="header_theme2 ui-btn-right" data-role="none">
			<%=HeaderUtils.IconClose %><span style="display: none;">close</span>
		</div>
	</div>
	<div data-role="content">
		<div class="rename-able dialog-content" style="margin: 20px 10px;">
			<p>Almost there! We just need your Zip Code and last 4 digits of your Social Security Number.</p>
		</div>
		<div data-role="fieldcontain">
            <label for="txtISTWifiZipCode" class="RequiredIcon">Zip Code</label>
            <input type="tel" id="txtISTWifiZipCode" data-required="true" data-format="zip" data-message-invalid-data="Invalid Zip Code" data-message-require="Zip Code is required" maxlength="5" />
        </div>
		<div data-role="fieldcontain">
            <label for="txtISTWifiLast4SSNDigits" class="RequiredIcon">Social Security Number (Last 4 Digits)</label>
            <input type="tel" id="txtISTWifiLast4SSNDigits" data-required="true" data-pattern="^[0-9]{4}$" data-message-invalid-data="Invalid SSN" data-message-require="Last 4 digits of SSN is required" maxlength="4" />
        </div>
		<br/>
		<div class ="div-continue" style="width: 100%;">
			<a href="#" data-role="button" type="button" onclick="IST.FACTORY.authenticate()" class="button-style div-continue-button">Prefill My Data</a>
			<a href="#" data-role="button" type="button" onclick="IST.FACTORY.goToPersonalInfoPage()" class="button-style div-continue-button-reverse hover-goback">No, Thanks</a>
		</div>
	</div>
</div>
<div id="istIncorrectCodePage" data-role="dialog" data-history="false" data-close-btn="none" style="width: 100%;">
	<div data-role="header">
		<button class="header-hidden-btn">.</button>
		<div class="page-header-title">Incorrect Code</div>
		<div tabindex="0" onclick="IST.FACTORY.goToAuthenticatePage()" class="header_theme2 ui-btn-right" data-role="none">
			<%=HeaderUtils.IconClose %><span style="display: none;">close</span>
		</div>
	</div>
	<div data-role="content">
		<div class="rename-able dialog-content text-center">Incorrect validation code entered.</div>
		<br/>
		<div class ="div-continue" style="width: 100%;">
			<a href="#" data-role="button" type="button" onclick="IST.FACTORY.goToEnterCodePage()" class="button-style div-continue-button">Try Again</a>
		</div>    
	</div>
</div>


<div id="istUnableToVerifyPage" data-role="dialog" data-history="false" data-close-btn="none" style="width: 100%;">
	<div data-role="header">
		<button class="header-hidden-btn">.</button>
		<div class="page-header-title">Unable to Verify</div>
		<div tabindex="0" onclick="IST.FACTORY.goToAuthenticatePage()" class="header_theme2 ui-btn-right" data-role="none">
			<%=HeaderUtils.IconClose %><span style="display: none;">close</span>
		</div>
	</div>
	<div data-role="content">
		<div class="dialog-content centered-table-block two-col">
			<div>
				<span>Zip Code:</span>	
				<span id="spISTZip"></span>
			</div>
			<div>
				<span>SSN:</span>	
				<span id="spISTSsn"></span>
			</div>
		</div>
		<div class="rename-able dialog-content text-center">
			<p>You may go back and correct any typos and re-verify, or continue to the application.</p>
		</div>
		<br/>
		<div class ="div-continue" style="width: 100%;">
			<a href="#" data-role="button" type="button" onclick="IST.FACTORY.goToAuthenticatePage()" class="button-style div-continue-button">Try Again</a>
		</div>    
	</div>
</div>

<div id="istMobilePrefillApplicationOptionPage" data-role="dialog" data-history="false" data-close-btn="none" style="width: 100%;">
	<div data-role="header">
		<button class="header-hidden-btn">.</button>
		<div class="page-header-title">Prefill Application Option</div>
		<div tabindex="0" onclick="IST.FACTORY.goToPersonalInfoPage()" class="header_theme2 ui-btn-right" data-role="none">
			<%=HeaderUtils.IconClose %><span style="display: none;">close</span>
		</div>
	</div>
	<div data-role="content">
		<div class="rename-able" style="font-size: 1.1em; margin: 20px 10px;">
			<p>Applying just got easier! We noticed you are applying from a mobile device.</p>
			<p>Verifying your identity now will enable us to prefill your application. Enter your Zip Code and last 4 digits of your Social Security Number.</p>
		</div>
		<div data-role="fieldcontain">
            <label for="txtISTMobileZipCode" class="RequiredIcon">Zip Code</label>
            <input type="tel" id="txtISTMobileZipCode" data-required="true" data-format="zip" data-message-invalid-data="Invalid Zip Code" data-message-require="Zip Code is required" maxlength="5" />
        </div>
		<div data-role="fieldcontain">
            <label for="txtISTMobileLast4SSNDigits" class="RequiredIcon">Social Security Number (Last 4 Digits)</label>
            <input type="tel" id="txtISTMobileLast4SSNDigits" data-required="true" data-message-invalid-data="Invalid SSN" data-pattern="^[0-9]{4}$" data-message-require="Last 4 digits of SSN is required" maxlength="4" />
        </div>
		<br/>
		<div class ="div-continue" style="width: 100%;">
			<a href="#" data-role="button" type="button" onclick="IST.FACTORY.authenticate()" class="button-style div-continue-button">Prefill My Data</a>
			<a href="#" data-role="button" type="button" onclick="IST.FACTORY.goToPersonalInfoPage()" class="button-style div-continue-button-reverse hover-goback">No, thanks</a>
		</div>         
        <!-- #include file="./InstaTouchStuffConsent.html" -->
	</div>
</div>
<img id="hiddenImage" name="hiddenImage" style="visibility:hidden; position: fixed; width: 0; height:0;" />
<input type="hidden" id="hdInstaTouchPrefillComment" value="" />
<script type="text/javascript">
	(function (IST, $, undefined) {
		IST.FACTORY = {};
		IST.DATA = {};
        IST.FACTORY.goToPersonalInfoPage = function (identityResponse) {      
            //clear hdInstaTouchPrefillComment
            $('#hdInstaTouchPrefillComment').val("");
            if (typeof identityResponse != "undefined" && identityResponse != null) {
	            var hideDLScan = false;
                if (typeof identityResponse.identity != "undefined" && identityResponse.identity != null) {
                    var prefillData = identityResponse.identity;
                    //update hdInstaTouchPrefillComment
                    $('#hdInstaTouchPrefillComment').val("Application Prefilled with Equifax InstaTouch.");
                    if (prefillData.name.firstName != null && prefillData.name.firstName != "") {
                        $("#txtFName").val(prefillData.name.firstName);
                    }
                    if (prefillData.name.lastName != null && prefillData.name.lastName != "") {
                        $("#txtLName").val(prefillData.name.lastName);
                    }
                    if (prefillData.contact.email != null && prefillData.contact.email != "") {
                        $("#txtEmail").val(prefillData.contact.email);
                    }
                    if (prefillData.contact.homePhone != null && prefillData.contact.homePhone != "") {
                        $("#txtHomePhone").val(prefillData.contact.homePhone);
                    }
                    if (prefillData.contact.mobile != null && prefillData.contact.mobile != "") {
                        $("#txtMobilePhone").val(prefillData.contact.mobile);
                    }            
                    //prefill address    
                    if (!$.isEmptyObject(prefillData.addresses)) {
                        var address = prefillData.addresses[0];
                        $("#txtAddress").val(address.streetAddress);
                        $("#txtCity").val(address.city);
                        $("#ddlState").val(address.state);
                        $("#txtZip").val(address.zipCode);
                    }
                    hideDLScan = true;
                }
                //prefill ssn and dob fields
                if (typeof identityResponse.identification != "undefined" && identityResponse.identification != null) {
                    var sSSN = identityResponse.identification.ssn;
                    var sDOB = identityResponse.identification.dob;
                    if (sSSN != null && sSSN != "") {
                        $('#txtSSN').val(sSSN);
                        $('#txtSSN1').val(sSSN.substring(0, 3));
                        $('#txtSSN2').val(sSSN.substring(3, 5));
                        $('#txtSSN3').val(sSSN.substring(5, 9));
                    }
                    if (sDOB != null && sDOB != "") {
                        var oDOB = sDOB.split("/");
                        $('#txtDOB').val(sDOB);
                        $('#txtDOB1').val(oDOB[0]);
                        $('#txtDOB2').val(oDOB[1]);
                        $('#txtDOB3').val(oDOB[2]);
                    }
                    hideDLScan = true;
                }
				if (hideDLScan) {
					$("#divLaserScanDocument").addClass("hidden");
					$("#divScanDocument").addClass("hidden");
				}
            }
			if (typeof IST.DATA.runBeforeGoToInfoPageCallback == "function") {
				IST.DATA.runBeforeGoToInfoPageCallback();
            }       
			goToNextPage("#<%=PersonalInfoPage%>");
		};
		IST.FACTORY.goToAuthenticatePage = function () {
			if (IST.DATA.mobileFlow) {
				goToNextPage("#istMobilePrefillApplicationOptionPage");
			} else {
				goToNextPage("#istAuthenticatePage");
			}
		}
		IST.FACTORY.init = function () {
			IST.DATA.mobileFlow = false;
			registerDataValidator("#istWifiPrefillApplicationOptionPage", "ValidateISTWifiPrefillApplicationOption");
			registerDataValidator("#istEnterCodePage", "ValidateISTEnterCode");
			registerDataValidator("#istAuthenticatePage", "ValidateISTAuthenticate");
			registerDataValidator("#istMobilePrefillApplicationOptionPage", "ValidateISTMobilePrefillApplicationOption");
			$(document).on("pageshow", function () {
				var curPageId = $.mobile.pageContainer.pagecontainer('getActivePage').attr('id');
				switch (curPageId) {
					case "istWifiPrefillApplicationOptionPage":
						$.lpqValidate.hideAllValidation("ValidateISTWifiPrefillApplicationOption");
						break;
					case "istEnterCodePage":
						$.lpqValidate.hideAllValidation("ValidateISTEnterCode");
						$("#txtISTValidationCode").val("");
						break;
					case "istAuthenticatePage":
						$.lpqValidate.hideAllValidation("ValidateISTAuthenticate");
						//$("#txtISTWifiZipCode").val("");
						//$("#txtISTWifiLast4SSNDigits").val("");
						break;
					case "istMobilePrefillApplicationOptionPage":
						$.lpqValidate.hideAllValidation("ValidateISTMobilePrefillApplicationOption");
						//$("#txtISTMobileZipCode").val("");
						//$("#txtISTMobileLast4SSNDigits").val("");
						break;
				}
			});
			var img = document.getElementById("hiddenImage");
			img.addEventListener('load', function () {
				callUpdateMdn();
			});
			img.addEventListener('error', function () {
				callUpdateMdn();
			});

		};
		IST.FACTORY.goToEnterCodePage = function() {
			goToNextPage("#istEnterCodePage");
        }
        IST.FACTORY.loadCarrierIdentification = function (finishedCallback) {
            $.ajax({
				url: "/handler/instahandler.aspx",
				async: true,
				cache: false,
				type: 'POST',
				dataType: 'html',
				data: {
                    command: 'carrierIdentification',
					InstaTouchMerchantId: '<%=InstaTouchMerchantId%>',
					lenderRef: '<%=LenderRef%>',
				},
                success: function (responseText) {
					IST.DATA.carrierIdentificationResponse = $.parseJSON(responseText);
					if (typeof finishedCallback == "function") {
						finishedCallback();
					}
				}
			});
        }

		function checkRestriction(callbackSuccess, callbackFailed) {
			$.ajax({
				url: "/handler/instahandler.aspx",
				async: true,
				cache: false,
				type: 'POST',
				dataType: 'json',
				data: {
					command: 'checkRestriction',
					lenderId: '<%=LenderID%>',
					lenderRef: '<%=LenderRef%>'
				},
				success: function (response) {
					if (response.IsSuccess) {
						if (typeof callbackSuccess == "function") {
							callbackSuccess();
						}
					} else {
						if (typeof callbackFailed == "function") {
							callbackFailed();
						}
					}
				},
				error: function() {
					if (typeof callbackFailed == "function") {
						callbackFailed();
					} 
				}
			});
		}
		IST.FACTORY.carrierIdentification = function (runBeforeGoToInfoPageCallback) {
			checkRestriction(function() {
				//success
				if (typeof runBeforeGoToInfoPageCallback != "undefined") {
					IST.DATA.runBeforeGoToInfoPageCallback = runBeforeGoToInfoPageCallback;
				}
				var response = IST.DATA.carrierIdentificationResponse != undefined ? IST.DATA.carrierIdentificationResponse : {};
				if (response.IsSuccess) {
					if (response.Info.mobile == true) {
						IST.DATA.mobileFlow = true;
						var img = document.getElementById("hiddenImage");
						img.src = response.Info.instaUrl;
					} else {
						IST.DATA.mobileFlow = false;
						goToNextPage("#istWifiPrefillApplicationOptionPage");
					}
				} else {
					IST.FACTORY.goToPersonalInfoPage();
				}
			}, function() {
				//failed
				goToNextPage("#<%=PersonalInfoPage%>");
			});
			
		}
        
		IST.FACTORY.processWifiConsent = function() {
			if ($.lpqValidate("ValidateISTWifiPrefillApplicationOption") == false) return;
			$.ajax({
				url: "/handler/instahandler.aspx",
				async: true,
				cache: false,
				type: 'POST',
				dataType: 'html',
				data: {
					command: 'processWifiConsent',
					phone: $("#txtISTPhoneNumber").val(),
					InstaTouchMerchantId: '<%=InstaTouchMerchantId%>',
					lenderId: '<%=LenderID%>',
					lenderRef: '<%=LenderRef%>',
                    loanType: '<%=LoanType%>'
                    
				},
				success: function (responseText) {
					var response = $.parseJSON(responseText);
					if (response.IsSuccess) {
						$("#txtISTPhoneNumber").val(response.Info.phoneNumber);
						goToNextPage("#istEnterCodePage");
					} else {
						goToNextPage("#istPrefillUnAvailablePage");
					}
				}
			});
		}
		IST.FACTORY.validateOtp = function() {
			if ($.lpqValidate("ValidateISTEnterCode") == false) return;
			$.ajax({
				url: "/handler/instahandler.aspx",
				async: true,
				cache: false,
				type: 'POST',
				dataType: 'html',
				data: {
					command: 'validateOtp',
					phone: $("#txtISTPhoneNumber").val(),
                    otp: $("#txtISTValidationCode").val(),
					InstaTouchMerchantId: '<%=InstaTouchMerchantId%>',
					lenderRef: '<%=LenderRef%>',
				},
				success: function (responseText) {
					var response = $.parseJSON(responseText);
					if (response.IsSuccess) {
						goToNextPage("#istAuthenticatePage");
					} else {
						goToNextPage("#istIncorrectCodePage");
					}
				}
			});
		}
		IST.FACTORY.authenticate = function () {
			var validationGroup = "";
			if (IST.DATA.mobileFlow == true) {
				validationGroup = "ValidateISTMobilePrefillApplicationOption";
			} else {
				validationGroup = "ValidateISTAuthenticate";
			}
			if ($.lpqValidate(validationGroup) == false) return;
			var data = {
				command: 'authenticate',
				InstaTouchMerchantId: '<%=InstaTouchMerchantId%>',
				lenderId: '<%=LenderID%>',
				lenderRef: '<%=LenderRef%>',
                loanType: '<%=LoanType%>'
			};
			if (IST.DATA.mobileFlow == true) {
				data.zipCode = $("#txtISTMobileZipCode").val();
				data.ssn = $("#txtISTMobileLast4SSNDigits").val();
			} else {
				data.zipCode = $("#txtISTWifiZipCode").val();
				data.ssn = $("#txtISTWifiLast4SSNDigits").val();
			}
			$.ajax({
				url: "/handler/instahandler.aspx",
				async: true,
				cache: false,
				type: 'POST',
				dataType: 'html',
				data: data,
				success: function (responseText) {
                    var response = $.parseJSON(responseText);
                    if (response.IsSuccess) {
                        IST.FACTORY.goToPersonalInfoPage(response.Info);
					} else {
						goToUnableToVerifyPage(response.Info);
					}
				}
			});
		}
		function callUpdateMdn() {
			$.ajax({
				url: "/handler/instahandler.aspx",
				async: true,
				cache: false,
				type: 'POST',
				dataType: 'html',
				data: {
                    command: 'updateMdn',
					InstaTouchMerchantId: '<%=InstaTouchMerchantId%>',
					lenderRef: '<%=LenderRef%>',
				},
				success: function (responseText) {
					var response = $.parseJSON(responseText);
					if (response.IsSuccess) {
						IST.FACTORY.goToAuthenticatePage();
					} else {
						//proceed wifi workflow
						IST.DATA.mobileFlow = false;
						goToNextPage("#istWifiPrefillApplicationOptionPage");
					}
				}
			});
		}
		function goToUnableToVerifyPage(data) {
			if (typeof data != "undefined" && data != null) {
				$("#spISTZip").text(data.zipCode);
				$("#spISTSsn").text(data.ssn);
			}
			goToNextPage("#istUnableToVerifyPage");
		}
		function registerDataValidator(container, groupName) {
			var $container = $(container);
			$.lpqValidate.removeValidationGroup(groupName);
			$("[data-required='true'],[data-format]", $container).each(function(idx, ele) {
				if ($(ele).is(":radio")) {
					var $placeHolder = $(ele).closest("div");
					$(ele).observer({
						validators: [
							function(partial) {
								var $self = $(this);
								if ($self.is(":visible") == false) return "";
								if ($("input[name='" + $self.attr("name") + "']:checked", $(container)).length == 0) {
									return "Please select an option";
								}
								return "";
							}
						],
						validateOnBlur: true,
						placeHolder: $placeHolder,
						group: groupName
					});
				} else {
					$(ele).observer({
						validators: [
							function(partial) {
								var $self = $(this);
								if ($self.is(":visible") == false) return "";
								if ($self.data("required") && $.trim($self.val()) === "") {
									if ($self.data("message-require")) {
										return $self.data("message-require");
									} else {
										return "This field is required";
									}
								}
								var format = $self.data("format");
								if (format) {
									var msg = "Invalid format";
									if ($self.data("message-invalid-data")) {
										msg = $self.data("message-invalid-data");
									}
									if (format === "email") {
										if (Common.ValidateEmail($self.val()) === false) {
											return msg;
										}
									} else if (format === "phone") {
                                        if (Common.ValidatePhone($self.val()) === false) {
                                            return msg;
                                        } else {
                                            //validate first digit of the area code
                                            var firstDigit = $self.val().substring(0, 1);
                                            if (firstDigit == "0" || firstDigit == "1") {
                                               return "Invalid format. Area code can not begin with 0 or 1";
                                            }
                                        }
									} else if (format === "zip") {
										if (Common.ValidateZipCode($self.val(), $self) === false) {
											return msg;
										}
									}
								} else {
									var pattern = $self.data("pattern");
									if (pattern) {
										var regex = new RegExp(pattern);
										if (!regex.test($self.val())) {
											var patternMsg = "Invalid format";
											if ($self.data("message-invalid-data")) {
												patternMsg = $self.data("message-invalid-data");
											}
											return patternMsg;
										}
									}
								}
								return "";
							}
						],
						validateOnBlur: true,
						group: groupName
					});
				}
			});
		}

	}(window.IST = window.IST || {}, jQuery));
	$(function () {
		IST.FACTORY.init();
	});

</script>