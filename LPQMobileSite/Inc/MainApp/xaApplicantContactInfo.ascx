﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="xaApplicantContactInfo.ascx.vb"
    Inherits="Inc_MainApp_xaApplicantContactInfo" %>
<%@ Import Namespace="System.Web.Optimization" %>
<iframe name="<%=IDPrefix%>if_ContactInfo" style="display: none;" src="about:blank" ></iframe>
<form target="<%=IDPrefix%>if_ContactInfo" action="/handler/AutoFillHandler.aspx" id="<%=IDPrefix%>frm_ContactInfo">
<div data-role="fieldcontain">
    <label id="<%=IDPrefix%>lblPreferedMethod" for="<%=IDPrefix%>preferredContactMethod">Preferred Contact Method</label>
    <select name="preferredContactMethod" id="<%=IDPrefix%>preferredContactMethod" data-prev-value="" onchange =" <%=IDPrefix%>updateContactInfo(this)">
        <option value="">--Please Select--</option>
	    <%If String.IsNullOrEmpty(PreferContactMethodDropdown) Then%> 
	    <option value="EMAIL">Email</option>
        <option value="HOME">Home Phone</option>
        <option value="CELL">Cell Phone</option>
        <option value="WORK">Work Phone</option>
        <%Else%>
            <%=PreferContactMethodDropdown%>
        <%End If%>
    </select>
</div>
<div data-role="fieldcontain">
    <label id="<%=IDPrefix%>lblEmail" for="<%=IDPrefix%>txtEmail" class="RequiredIcon">Email</label>
    <input type="email" name="email" autocomplete="email" id="<%=IDPrefix%>txtEmail" maxlength ="50" value="<%=Email %>"/>
</div>
<div data-role="fieldcontain" id="<%=IDPrefix%>divHomePhone">
	<div>
		<div style="display: inline-block;"><label id="<%=IDPrefix%>lblHomePhone" for="<%=IDPrefix%>txtHomePhone" class="RequiredIcon" style="white-space: nowrap;">Home Phone</label></div>
		<div style="display: inline-block;"><label class="phone_format rename-able" style="white-space: nowrap; font-style: italic;">(xxx) xxx-xxxx</label></div>
	</div>
    <input type="<%=TextAndroidTel%>" pattern="[0-9]*" autocomplete="tel" name="phone" id="<%=IDPrefix%>txtHomePhone" class="inphone" maxlength='14'  value="<%=HomePhone %>" />
</div>
<div data-role="fieldcontain" id="<%=IDPrefix%>divMobilePhone">
	<div>
		<div style="display: inline-block;"><label id="<%=IDPrefix%>lblMobilePhone" for="<%=IDPrefix%>txtMobilePhone"  style="white-space: nowrap;">Cell Phone</label></div>
		<div style="display: inline-block;"><label class="phone_format rename-able" style="white-space: nowrap; font-style: italic;">(xxx) xxx-xxxx</label></div>
	</div>
    <input type= "<%=TextAndroidTel%>" pattern="[0-9]*" autocomplete="tel" name="mobile" id="<%=IDPrefix%>txtMobilePhone" class="inphone" maxlength='14' value="<%=MobilePhone %>"/>
</div>
<div data-role="fieldcontain" id="<%=IDPrefix%>divWorkPhone">
	<div>
		<div style="display: inline-block;"><label id="<%=IDPrefix%>lblWorkPhone" for="<%=IDPrefix%>txtWorkPhone" style="white-space: nowrap;">Work Phone</label></div>
		<div style="display: inline-block;"><label class="phone_format rename-able" style="white-space: nowrap; font-style: italic;">(xxx) xxx-xxxx</label></div>
	</div>
    <input type="<%=TextAndroidTel%>" pattern="[0-9]*" autocomplete="off" name="workphone" id="<%=IDPrefix%>txtWorkPhone" class="inphone" maxlength='14' value="<%=WorkPhone %>" />
</div>
<div data-role="fieldcontain" class="hidden">
    <label for="<%=IDPrefix%>txtWorkPhoneExt">Work Phone Extension</label>
    <input type="<%=TextAndroidTel%>" pattern="[0-9]*" id="<%=IDPrefix%>txtWorkPhoneExt" name="ext" autocomplete="off"  maxlength="5" class="numeric" />
</div>
</form>
<input type="hidden" id="<%=IDPrefix%>hdHomePhoneCountry" />
<input type="hidden" id="<%=IDPrefix%>hdMobilePhoneCountry" />
<input type="hidden" id="<%=IDPrefix%>hdWorkPhoneCountry" />
<script type="text/javascript">
 $(function () {
 	if (is_foreign_phone) {
 	    // international country code
 	    //by default it display separateDialCode dropdown
 	    //don't want to display separateDialCode dropdown, just set separateDialCode: false
 	    $("#<%=IDPrefix%>txtHomePhone").intlTelInput({
 	        separateDialCode: false
 	    }); // don't know why it's not set default for this one
 	    $('#<%=IDPrefix%>txtMobilePhone').intlTelInput({
 	        separateDialCode: false
 	    });
 	    $('#<%=IDPrefix%>txtWorkPhone').intlTelInput({
 	        separateDialCode: false
 	    });

        // change the text
 		$('.phone_format', "#<%=IDPrefix%>frm_ContactInfo").html("");

        // remove mask
        if (!isMobile.Android()) {
        	$('.inphone', "#<%=IDPrefix%>frm_ContactInfo").inputmask('remove');
        } else {
        	$('input.inphone', "#<%=IDPrefix%>frm_ContactInfo").off('blur').off('keyup').off('keydown');
        }
 		$('input.inphone', "#<%=IDPrefix%>frm_ContactInfo").numeric();

        //set hidden phone country to "us" by default
        $("#<%=IDPrefix%>hdHomePhoneCountry").val("us");
        $("#<%=IDPrefix%>hdMobilePhoneCountry").val("us");
        $("#<%=IDPrefix%>hdWorkPhoneCountry").val("us");
     }
 	if ($("#hdHomePhoneRequired").length == 1 && $("#hdHomePhoneRequired").val().toUpperCase() === "N") {
        $('#<%=IDPrefix%>lblPreferedMethod').addClass('RequiredIcon');           
        if (($('#<%=IDPrefix%>preferredContactMethod option:selected').val() || "").toUpperCase() !== "HOME") {
 			$('#<%=IDPrefix%>lblHomePhone').removeClass('RequiredIcon');
		}
      }    
      if ($("#hdHomePhoneRequired").length == 1 && $("#hdHomePhoneRequired").val().toUpperCase() === "N" && ($('#<%=IDPrefix%>preferredContactMethod option:selected').val() || "").toUpperCase() !== "HOME") {
 		$('#<%=IDPrefix%>lblHomePhone').removeClass('RequiredIcon');
	}
        //update hidden Phone Country
 		$('#<%=IDPrefix%>divHomePhone').on('click', '.flag-container', function () {
 			<%=IDPrefix%>updateHiddenPhoneCountry($(this), $('#<%=IDPrefix%>hdHomePhoneCountry'));
        });
        //$('#co_divHomePhone').on('click', '.flag-container', function () {
        //    updateHiddenPhoneCountry($(this), $('#co_hdHomePhoneCountry'));
        //});
 		$('#<%=IDPrefix%>divMobilePhone').on('click', '.flag-container', function () {
 			<%=IDPrefix%>updateHiddenPhoneCountry($(this), $('#<%=IDPrefix%>hdMobilePhoneCountry'));
        });
        //$('#co_divMobilePhone').on('click', '.flag-container', function () {
        //    updateHiddenPhoneCountry($(this), $('#co_hdMobilePhoneCountry'));
        //});
 		$('#<%=IDPrefix%>divWorkPhone').on('click', '.flag-container', function () {
 			<%=IDPrefix%>updateHiddenPhoneCountry($(this), $('#<%=IDPrefix%>hdWorkPhoneCountry'));
        });
        //$('#co_divWorkPhone').on('click', '.flag-container', function () {
        //    updateHiddenPhoneCountry($(this), $('#co_hdWorkPhoneCountry'));
        //});
 	//end update hidden Phone country
	 $("#<%=IDPrefix%>txtWorkPhone").on("keyup blur", function() {
		if ($.trim($(this).val()) == "") {
			$("#<%=IDPrefix%>txtWorkPhoneExt").closest("div[data-role='fieldcontain']").addClass("hidden");
		} else {
			$("#<%=IDPrefix%>txtWorkPhoneExt").closest("div[data-role='fieldcontain']").removeClass("hidden");
		}
	 });
 });

    function <%=IDPrefix%>updateContactInfo(element)
    {
        var ddlContactMethod = ($('#<%=IDPrefix%>preferredContactMethod option:selected').val() || "").toUpperCase();
        var labelMobilePhoneElem = $('#<%=IDPrefix%>lblMobilePhone');
    	var labelWorkPhoneElem = $('#<%=IDPrefix%>lblWorkPhone');
    	var labelHomePhoneElem = $('#<%=IDPrefix%>lblHomePhone');
        if (ddlContactMethod == "CELL") {
            labelMobilePhoneElem.addClass('RequiredIcon');
            labelWorkPhoneElem.removeClass('RequiredIcon');
            $.lpqValidate.hideValidation($('#<%=IDPrefix%>txtWorkPhone'));
        } else if (ddlContactMethod == "WORK") {
            labelWorkPhoneElem.addClass('RequiredIcon');
            labelMobilePhoneElem.removeClass('RequiredIcon');
	        $.lpqValidate.hideValidation($('#<%=IDPrefix%>txtMobilePhone'));
        } else {
            labelMobilePhoneElem.removeClass('RequiredIcon');
            labelWorkPhoneElem.removeClass('RequiredIcon');
        }
    	if ($("#hdHomePhoneRequired").length == 1 && $("#hdHomePhoneRequired").val().toUpperCase() === "N" && ddlContactMethod !== "HOME") {
		    labelHomePhoneElem.removeClass('RequiredIcon');
	    } else {
    		labelHomePhoneElem.addClass('RequiredIcon');
    	}
    	
    	switch ($(element).data("prev-value")) {
	    	case "HOME":
	    		$.lpqValidate.validate("<%=IDPrefix%>ValidateApplicantContactInfoXA", '#<%=IDPrefix%>txtHomePhone');
    		case "CELL":
    			$.lpqValidate.validate("<%=IDPrefix%>ValidateApplicantContactInfoXA", '#<%=IDPrefix%>txtMobilePhone');
				break;
    		case "WORK":
    			$.lpqValidate.validate("<%=IDPrefix%>ValidateApplicantContactInfoXA", '#<%=IDPrefix%>txtWorkPhone');
    			$.lpqValidate.validate("<%=IDPrefix%>ValidateApplicantContactInfoXA", '#<%=IDPrefix%>txtWorkPhoneExt');
				break;
			default:
    	}
        $(element).data("prev-value", ($(element).val() || "").toUpperCase());
    }
  /*  function getSelectedCountryCode(currElem) {    
        var selectedDialCode = currElem.text().replace("+", "");
        var selectedCountryCode = "";
        $('.country-list .country').each(function () {
            var element = $(this);        
            if (selectedDialCode == element.attr('data-dial-code')) {
                selectedCountryCode = element.attr('data-country-code');
                return false; //exit the loop
            }
        });
        return selectedCountryCode;
    }*/
	function <%=IDPrefix%>updateHiddenPhoneCountry(currElem, hdPhoneCountryElem) {
         //var selectedElem = currElem.children().first().children().next();
        //hdPhoneCountryElem.val(getSelectedCountryCode(selectedElem));  
        if (currElem.find('li[class~="active"]')) {
            hdPhoneCountryElem.val(currElem.find('li[class~="active"]').attr('data-country-code'));
        }
	}
	//OBSOLETE
    //function getPhoneCountry(txtPhoneID) {
    //    var currElement = $('#' + txtPhoneID);
    //    var selectedDialCodeElement = currElement.prev().children().first().children().next();
    //    var selectedDialCode = "";
    //    var selectedCountryCode = "";
    //    var countryList = currElement.prev().children().first().next().children();
    //    if (selectedDialCodeElement.length > 0) {
    //        if (selectedDialCodeElement.attr('class') == 'selected-dial-code') {
    //            selectedDialCode = selectedDialCodeElement.text().replace("+", "");
    //        }
    //    }     
    //    $('.country-list .country').each(function () {
    //        var element = $(this);
    //        if (selectedDialCode ==  element.attr('data-dial-code')) {
    //            selectedCountryCode = element.attr('data-country-code');
    //            return false; //exit the loop
    //        }
    //    });
    
    //    return selectedCountryCode;
    //}
    
    function <%=IDPrefix%>ValidateApplicantContactInfoXA() {
    	
    	if ($.lpqValidate("<%=IDPrefix%>ValidateApplicantContactInfoXA")) {
    		document.getElementById("<%=IDPrefix%>frm_ContactInfo").submit();
			return true;
		}
		return false;
    }

	function <%=IDPrefix%>registerXAApplicantContactInfoValidator() {
		$('#<%=IDPrefix%>preferredContactMethod').observer({
			validators: [
				function (partial) {
					var preferredMethod = $(this).val();
					if ($("#hdHomePhoneRequired").length == 1 && $("#hdHomePhoneRequired").val().toUpperCase() === "N") {
						if (preferredMethod == "") {
							return "Preferred Contact Method is required";
						}
					}
					
					return "";
				}
			],
			validateOnBlur: true,
			group: "<%=IDPrefix%>ValidateApplicantContactInfoXA"
		});
		$('#<%=IDPrefix%>txtEmail').observer({
			validators: [
				function (partial) {
					var email = $(this).val();
					if (Common.ValidateEmail(email) == false) {
						return "Valid Email is required";
					}
					return "";
				}
    		],
			validateOnBlur: true,
			group: "<%=IDPrefix%>ValidateApplicantContactInfoXA"
		});
		$('#<%=IDPrefix%>txtHomePhone').observer({
			validators: [
				function (partial) {
					var homephone = $(this).val();
					//primary phone field is required
					var homephoneIsRequired = true;
                    var preferMethod = ($('#<%=IDPrefix%>preferredContactMethod').val() || "").toUpperCase();
					if ($("#hdHomePhoneRequired").length == 1 && $("#hdHomePhoneRequired").val().toUpperCase() === "N" && preferMethod !== "HOME") {
						homephoneIsRequired = false;
					}
					if (homephoneIsRequired == true) {
						if (!Common.ValidateText(homephone)) {
							return "Valid phone number is required";
						}
					} else { //not required
					     if ($.trim(homephone) === "") {
					        return ""; //no validate if homephone is empty
					     }
					}

					if (is_foreign_phone) {
						return validateForeignPhone('<%=IDPrefix%>txtHomePhone');
					} else {
						if (!Common.ValidatePhone(homephone)) {
							return "Valid phone number is required";
            			} else {
            				//validate first digit of home phone : 0 and 1 is invalid
							var firstDigit = homephone.replace("(", "").substring(0, 1);
            				if (firstDigit == 0 || firstDigit == 1) {
            					return 'Phone number and area code can not begin with 0 or 1 and must be a valid phone number';
            				}
            			}
					}
					return "";
				}
			],
			validateOnBlur: true,
			group: "<%=IDPrefix%>ValidateApplicantContactInfoXA"
		});
		$('#<%=IDPrefix%>txtMobilePhone').observer({
			validators: [
				function (partial) {
					var mobile = $(this).val();
					if (Common.ValidateText(mobile)) {
					    if (is_foreign_phone) {
					        return validateForeignPhone('<%=IDPrefix%>txtMobilePhone');
					    } else {
					        if (!Common.ValidatePhone(mobile)) {
					            return "Valid phone number is required";
					        } else {
					            //validate first digit of home phone : 0 and 1 is invalid
					            var firstDigit = mobile.replace("(", "").substring(0, 1);
					            if (firstDigit == 0 || firstDigit == 1) {
					                return 'Phone number and area code can not begin with 0 or 1 and must be a valid phone number';
					            }
					        }
					    }
					} else { //empty mobile phone input
                        if (($('#<%=IDPrefix%>preferredContactMethod').val() || "").toUpperCase() === "CELL") {		   
					            return "Valid phone number is required";
					    }
					}
					return "";
				}
			],
			validateOnBlur: true,
			group: "<%=IDPrefix%>ValidateApplicantContactInfoXA"
		});
		$('#<%=IDPrefix%>txtWorkPhone').observer({
			validators: [
				function (partial) {
					var workphone = $(this).val();
					if (Common.ValidateText(workphone)) {
						if (is_foreign_phone) {
							return validateForeignPhone('<%=IDPrefix%>txtWorkPhone');
						} else {
							if (!Common.ValidatePhone(workphone)) {
								return "Valid phone number is required";
							} else {
								//validate first digit of home phone : 0 and 1 is invalid
								var firstDigit = workphone.replace("(", "").substring(0, 1);
								if (firstDigit == 0 || firstDigit == 1) {
									return 'Phone number and area code can not begin with 0 or 1 and must be a valid phone number';
								}
							}
						}
					} else { //empty workphone input
                        if (($('#<%=IDPrefix%>preferredContactMethod').val() || "").toUpperCase() === "WORK") {
					            return "Valid phone number is required";
					    }
					}
					var ext = $('#<%=IDPrefix%>txtWorkPhoneExt').val();
					if ($.trim(workphone) === "" && $.trim(ext) !== "") {
						return "Valid phone number is required";
					}
					return "";
				}
			],
			validateOnBlur: true,
			group: "<%=IDPrefix%>ValidateApplicantContactInfoXA"
		});
		$('#<%=IDPrefix%>txtWorkPhoneExt').observer({
			validators: [
				function (partial) {
					if ($(this.closest("div[data-role='fieldcontain']").hasClass("hidden"))) {
						return "";
					}
					var ext = $(this).val();
					var workphone = $('#<%=IDPrefix%>txtWorkPhone').val();
					if (Common.ValidateText(ext)) {
						if (/^[0-9]{1,5}$/.test(ext) == false) {
							return "Valid Extension is required";
						}
						if (partial === true) {
							if (is_foreign_phone) {
								if (Common.ValidateText(workphone)) {
									$.lpqValidate.showValidation($('#<%=IDPrefix%>txtWorkPhone'), validateForeignPhone('<%=IDPrefix%>txtWorkPhone'));
								} else {
									$.lpqValidate.showValidation($('#<%=IDPrefix%>txtWorkPhone'), "Valid phone number is required");
								}
							} else {
								if (!Common.ValidatePhone(workphone)) {
									$.lpqValidate.showValidation($('#<%=IDPrefix%>txtWorkPhone'), "Valid phone number is required");
								} else {
									//validate first digit of home phone : 0 and 1 is invalid
									var firstDigit = workphone.replace("(", "").substring(0, 1);
									if (firstDigit == 0 || firstDigit == 1) {
										$.lpqValidate.showValidation($('#<%=IDPrefix%>txtWorkPhone'), 'Phone number and area code can not begin with 0 or 1 and must be a valid phone number');
									}
								}
							}
						}
					}
					return "";
				}
			],
			validateOnBlur: true,
			group: "<%=IDPrefix%>ValidateApplicantContactInfoXA"
		});
	}

    <%=IDPrefix%>registerXAApplicantContactInfoValidator();
   
    function <%=IDPrefix%>SetContactInfo(appInfo) {
        var txtEmail = $('#<%=IDPrefix%>txtEmail').val().trim();
        var txtHomePhone = $('#<%=IDPrefix%>txtHomePhone').val();
        var txtMobilePhone = $('#<%=IDPrefix%>txtMobilePhone').val();
    	var txtWorkPhone = $('#<%=IDPrefix%>txtWorkPhone').val();
	    var txtWorkPhoneEXT = "";
    	if ($.trim(txtWorkPhone).length > 0) {
    		txtWorkPhoneEXT = $('#<%=IDPrefix%>txtWorkPhoneExt').val();
	    }
        var ddlContactMethod = ($('#<%=IDPrefix%>preferredContactMethod option:selected').val() || "").toUpperCase();
        var homePhoneCountry = $('#<%=IDPrefix%>hdHomePhoneCountry').val();
        var workPhoneCountry = $('#<%=IDPrefix%>hdWorkPhoneCountry').val();
        var cellPhoneCountry = $('#<%=IDPrefix%>hdMobilePhoneCountry').val();
        //var homePhoneCountry = getPhoneCountry('<%=IDPrefix%>txtHomePhone');
        //var workPhoneCountry = getPhoneCountry('<%=IDPrefix%>txtWorkPhone');
        //var cellPhoneCountry = getPhoneCountry('<%=IDPrefix%>txtMobilePhone');
        //unable to get value from .intlTelInput("getSelectedCountryData").iso2(always get undefined)
        //if(g_is_foreign_contact) {         
        <%--    //var homePhoneCountry = $("#<%=IDPrefix%>txtHomePhone").intlTelInput("getSelectedCountryData").iso2;
    		//var workPhoneCountry = $("#<%=IDPrefix%>txtWorkPhone").intlTelInput("getSelectedCountryData").iso2;
    	    //var cellPhoneCountry = $("#<%=IDPrefix%>txtMobilePhone").intlTelInput("getSelectedCountryData").iso2;
        //} --%>
        
        if (!is_foreign_phone) {//reset phone country to emptry string 
            homePhoneCountry = "";
            workPhoneCountry = "";
            cellPhoneCountry = "";
        }
  
        txtHomePhone = mappingHomePhone(txtHomePhone, txtWorkPhone, txtMobilePhone, ddlContactMethod);
        appInfo.<%=IDPrefix%>EmailAddr = txtEmail;
        appInfo.<%=IDPrefix%>HomePhone = txtHomePhone; 
        appInfo.<%=IDPrefix%>HomePhoneCountry = homePhoneCountry;
        // appInfo.<%=IDPrefix%>HomePhoneCountry = homePhoneCountry == undefined ? "" : homePhoneCountry;
        appInfo.<%=IDPrefix%>MobilePhone = txtMobilePhone;  
        appInfo.<%=IDPrefix%>MobilePhoneCountry = cellPhoneCountry;
        //  appInfo.<%=IDPrefix%>MobilePhoneCountry = cellPhoneCountry == undefined ? "" : cellPhoneCountry;
        appInfo.<%=IDPrefix%>WorkPhone = txtWorkPhone;
        appInfo.<%=IDPrefix%>WorkPhoneCountry = workPhoneCountry;   
        // appInfo.<%=IDPrefix%>WorkPhoneCountry = workPhoneCountry == undefined ? "" : workPhoneCountry;  
        appInfo.<%=IDPrefix%>WorkPhoneEXT = txtWorkPhoneEXT;
        //foreign phone number        
        appInfo.<%=IDPrefix%>ContactMethod = ddlContactMethod;
    }
    $('input#<%=IDPrefix%>txtWorkPhoneExt').blur(function() {
        $(this).val(Common.GetRtNumber($(this).val()));
    });
    function <%=IDPrefix%>ViewContactInfo() {
        var applicantContactInfo = "";
        var txtEmail = htmlEncode($('#<%=IDPrefix%>txtEmail').val().trim());
        var txtHomePhone = $('#<%=IDPrefix%>txtHomePhone').val();
        var txtMobilePhone = $('#<%=IDPrefix%>txtMobilePhone').val();
        var txtWorkPhone = $('#<%=IDPrefix%>txtWorkPhone').val();
        var txtWorkPhoneEXT = $('#<%=IDPrefix%>txtWorkPhoneExt').val();
        var ddlContactMethod = $('#<%=IDPrefix%>preferredContactMethod option:selected').text();

    	if (txtEmail != "" && $('#<%=IDPrefix%>txtEmail').IsHideField() === false) {
            applicantContactInfo += '<div class="col-sm-6 col-xs-12"><div class="row"><div class="col-xs-6 text-right row-title"><span class="bold">Email</span></div><div class="col-xs-6 text-left row-data"><span>' + txtEmail + '</span></div></div></div>';
        }
    	if (txtHomePhone != '' && $('#<%=IDPrefix%>txtHomePhone').IsHideField() === false) {
        	applicantContactInfo += '<div class="col-sm-6 col-xs-12"><div class="row"><div class="col-xs-6 text-right row-title"><span class="bold">Home Phone</span></div><div class="col-xs-6 text-left row-data"><span>' + txtHomePhone + '</span></div></div></div>';
        }
    	if (txtMobilePhone != '' && $('#<%=IDPrefix%>txtMobilePhone').IsHideField() === false) {
        	applicantContactInfo += '<div class="col-sm-6 col-xs-12"><div class="row"><div class="col-xs-6 text-right row-title"><span class="bold">Cell Phone</span></div><div class="col-xs-6 text-left row-data"><span>' + txtMobilePhone + '</span></div></div></div>';
        }
    	if (txtWorkPhone != '' && $('#<%=IDPrefix%>txtWorkPhone').IsHideField() === false) {
    		applicantContactInfo += '<div class="col-sm-6 col-xs-12"><div class="row"><div class="col-xs-6 text-right row-title"><span class="bold">Work Phone</span></div><div class="col-xs-6 text-left row-data"><span>' + txtWorkPhone + (txtWorkPhoneEXT != "" && $('#<%=IDPrefix%>txtWorkPhoneExt').IsHideField() === false ? " Ext. " + txtWorkPhoneEXT : "") + '</span></div></div></div>';
        }
    	if ($.trim($('#<%=IDPrefix%>preferredContactMethod').val()) !== "" && $('#<%=IDPrefix%>preferredContactMethod').IsHideField() === false) {
        	applicantContactInfo += '<div class="col-sm-6 col-xs-12"><div class="row"><div class="col-xs-6 text-right row-title"><span class="bold">Preferred Contact Method</span></div><div class="col-xs-6 text-left row-data"><span>' + ddlContactMethod + '</span></div></div></div>';
        }
        
        $(this).show();
        $(this).prev("div.row").show();
        return applicantContactInfo;
    }

    function <%=IDPrefix%>autoFillData_ContactInfor() {
      
		$('#<%=IDPrefix%>txtEmail').val("marisol@test.com");
		$('#<%=IDPrefix%>txtHomePhone').val("2012512145");
		$('#<%=IDPrefix%>txtWorkPhone').val("2014523698");
        $('#<%=IDPrefix%>txtMobilePhone').val("2013625478");
      
	}
    function <%=IDPrefix%>mappingPreferredContactValue(value) {
        var ddlContactMethodEle = $('#<%=IDPrefix%>preferredContactMethod option');
        var selectedValue = "";
        ddlContactMethodEle.each(function () {
            if ($(this).val() != undefined) {
                if($(this).val().toUpperCase() == value.toUpperCase()){
                    selectedValue = $(this).val();
                    return false; //exit the loop
                }
            }
        });
        return selectedValue;
    }
   
</script>

