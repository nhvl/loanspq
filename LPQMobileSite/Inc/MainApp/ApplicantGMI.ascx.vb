﻿
Partial Class Inc_MainApp_ApplicantGMI
    Inherits CBaseUserControl

    Public Property IsRequired As Boolean
	Public Property DemographicDescription As String
	Private _demographicDisclosure As String
	Public Property DemographicDisclosure As String
		Get
			Return _demographicDisclosure
		End Get
		Set(value As String)
			_demographicDisclosure = New Regex("<h([1-6])>").Replace(value,
																	 Function(match)
																		 Return "<h" & match.Groups(1).Value & " class=""header_theme"">"
																	 End Function)
		End Set
	End Property

End Class
