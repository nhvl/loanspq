﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="xaApplicantEmployment.ascx.vb" Inherits="Inc_MainApp_xaApplicantEmployment" %>
<div id="<%=IDPrefix%>divEmploymentSection" <%=IIf(IsInMode("777") AndAlso IsInFeature("visibility"), "class=""showfield-section"" data-show-field-section-id=""" & BuildShowFieldSectionID("divEmploymentSection") & """  " & IIf(IDPrefix = "m_", "data-default-state=""off""", IIf(ShowFieldDefaultStateOn, "", "data-default-state=""off""")) & " data-section-name= '" & GetApmVisibilitySectionNamePrefix() & " Employment'", "")%>>
	<%=Header%>
	<%--<%=HeaderUtils.RenderPageTitle(17, IIf(String.IsNullOrEmpty(Title), "About Your Employment", Title), True)%>--%>
	<div data-role="fieldcontain">
		<label for='<%=IDPrefix%>ddlEmploymentStatus' class="RequiredIcon">Employment Status</label>
		<select id='<%=IDPrefix%>ddlEmploymentStatus' onchange="<%=IDPrefix%>HandleEmploymentStatusChange(this)">
			<%=EmploymentStatusDropdown%>
		</select>
	</div>
	<%If CollectDescriptionIfEmploymentStatusIsOther.ToUpper() = "Y" Then%>
    <div id="<%=IDPrefix%>divCollectDescriptionIfEmploymentStatusIsOther" style="display: none;">
        <div data-role="fieldcontain">
            <label for="<%=IDPrefix%>txtEmploymentDescription" class="RequiredIcon">Other Employment Description</label>
            <input type="text" id="<%=IDPrefix%>txtEmploymentDescription" maxlength="90"/>
        </div>  
    </div>
	<%End If%>
	<div id='<%=IDPrefix%>divEmploymentStatusRest' onchange="<%=IDPrefix%>updateIncomeTitle()">
	</div>
	<%If EnableGrossMonthlyIncome AndAlso Not CollectJobTitleOnly Then%>
	 <div id="<%=IDPrefix%>divGrossMonthlyIncome" <%=IIf(IsInMode("777") AndAlso IsInFeature("visibility"), "class=""showfield-section"" data-show-field-section-id=""" & BuildShowFieldSectionID("divGrossMonthlyIncome") & """ data-section-name= '" & IIf(String.IsNullOrEmpty(IDPrefix), "", GetApmVisibilitySectionNamePrefix()) & " Gross Monthly Income'", "")%>>
        <div data-role="fieldcontain">
            <label for="<%=IDPrefix%>txtGrossMonthlyIncome" id='<%=IDPrefix%>lblGrossMonthlyIncome'>Gross Monthly Income (before taxes)</label>
            <input type="<%=TextAndroidTel%>" pattern="[0-9]*" id="<%=IDPrefix%>txtGrossMonthlyIncome" class="money" maxlength ="10"/>
       </div>
	</div>
	<%End If%>
	<!--Previous Employment -->
	<div id="<%=IDPrefix%>divPreviousEmploymentInfo" style= "display:none;">
		<br />
		<div style="font-weight :bold;font-size:17px">Previous Employment Information</div>
		<div data-role="fieldcontain">
			<label for="<%=IDPrefix%>prev_ddlEmploymentStatus" class="RequiredIcon">Employment Status</label>
			<select id='<%=IDPrefix%>prev_ddlEmploymentStatus' onchange="<%=IDPrefix%>prev_updateIncomeTitle()">
				 <!--add a blank field for the employment status so it won’t defaulted to Employed -->
				<%=EmploymentStatusDropdown%>
			</select>
		</div>
		<div id='<%=IDPrefix%>prev_divEmploymentStatusRest' onchange="<%=IDPrefix%>prev_updateIncomeTitle()"></div>
		<%If EnableGrossMonthlyIncome Then%>
		<div data-role="fieldcontain">
			<label for="<%=IDPrefix%>prev_txtGrossMonthlyIncome" id="<%=IDPrefix%>prev_lblGrossMonthlyIncome">Gross Monthly Income (before taxes)</label>
			<input type="<%=TextAndroidTel%>" pattern="[0-9]*" id="<%=IDPrefix%>prev_txtGrossMonthlyIncome" class="money" maxlength ="10"/>
		</div>
		<%End If%>
	</div>
	<!--EndPrevious Employment -->
</div>
<div id="<%=IDPrefix%>popGrossMonthlyIncomWarning" data-role="popup" style="max-width: 500px;">
	<div data-role="content">
		<div class="row" style="padding: 20px 0 0;">
			<div class="col-sm-12">
				<div style="margin: 10px 0; font-weight: normal;" data-place-holder="content"></div>     
			</div>    
		</div>
	</div>
	<div class="div-continue js-no-required-field" style="text-align: center;"><a href="#" data-transition="slide" type="button" onclick="closePopup('#<%=IDPrefix%>popGrossMonthlyIncomWarning')" data-role="button" class="div-continue-button">OK</a></div>
</div>
<input type="hidden" id="<%=IDPrefix%>hdShowPreviousEmployment" value="N" />
<script type="text/javascript">
    $(function() {
        var employmentStatus = $('#<%=IDPrefix%>ddlEmploymentStatus option:selected').val();
        if (employmentStatus != null || employmentStatus != "") {
            <%=IDPrefix%>updateIncomeTitle();
        }
    	var prevEmploymentStatus = $('#<%=IDPrefix%>prev_ddlEmploymentStatus option:selected').val();
    	if (prevEmploymentStatus != null || prevEmploymentStatus != "") {
    		<%=IDPrefix%>prev_updateIncomeTitle();
        }
    	$('#<%=IDPrefix%>divEmploymentStatusRest').on('change', 'select[id*="txtEmployedDuration"]', function () {
    		<%=IDPrefix%>ShowPreviousEmploymentInfo();
	    });
		<%If EnableGrossMonthlyIncome Then %>
    	$('#<%=IDPrefix%>divGrossMonthlyIncome').on("change", function (evt, state) {
    		if (state == "hide") {
    			$("#<%=IDPrefix%>prev_txtGrossMonthlyIncome").closest("div[data-role='fieldcontain']").hide();
		    } else {
    			$("#<%=IDPrefix%>prev_txtGrossMonthlyIncome").closest("div[data-role='fieldcontain']").show();
    		}
    		$.lpqValidate.hideValidation($('#<%=IDPrefix%>prev_txtGrossMonthlyIncome'));
			<%=IDPrefix%>ShowPreviousEmploymentInfo();
		});
    	<%End If%>
    	$("#<%=IDPrefix%>txtGrossMonthlyIncome, #<%=IDPrefix%>prev_txtGrossMonthlyIncome").on("blur", function () {
    		var $self = $(this);
    		if (Common.GetFloatFromMoney($self.val()) > 20000) {
    			$("#<%=IDPrefix%>popGrossMonthlyIncomWarning [data-place-holder='content']").html("Your estimated gross annual salary is " + Common.FormatCurrency(Common.GetFloatFromMoney($self.val()) * 12, true));
				$("#<%=IDPrefix%>popGrossMonthlyIncomWarning").popup("open", { "positionTo": "window" });
			}
		});
    	$("#<%=IDPrefix%>popGrossMonthlyIncomWarning").on("popupafteropen", function (event, ui) {
    		//prevent scrollbar by popup overlay
    		$("#" + this.id + "-screen").height("");
    	});
    });
    function <%=IDPrefix%>updateIncomeTitle() {
        var sEmploymentStatus = "";
        sEmploymentStatus = $('#<%=IDPrefix%>ddlEmploymentStatus option:selected').val();
        if (sEmploymentStatus == 'RETIRED' || sEmploymentStatus == 'RETIRED MILITARY' || sEmploymentStatus == 'STUDENT' || sEmploymentStatus == 'UNEMPLOYED'
            || sEmploymentStatus == 'GOVERNMENT/DOD' || sEmploymentStatus == 'HOMEMAKER' || sEmploymentStatus == 'OTHER') {
        	$('#<%=IDPrefix%>lblGrossMonthlyIncome').removeClass("RequiredIcon"); // not required
        	$.lpqValidate.hideValidation($('#<%=IDPrefix%>txtGrossMonthlyIncome'));
        } else {
        	$('#<%=IDPrefix%>lblGrossMonthlyIncome').addClass("RequiredIcon"); // required
        }
    }

	function <%=IDPrefix%>HandleEmploymentStatusChange(ele) {
		<%If CollectDescriptionIfEmploymentStatusIsOther.ToUpper() = "Y" Then%>
		var sEmpStatus = $(ele).val();
		if (sEmpStatus.toUpperCase() == "OTHER") {
			$("#<%=IDPrefix%>divCollectDescriptionIfEmploymentStatusIsOther").show();
		} else {
			$("#<%=IDPrefix%>divCollectDescriptionIfEmploymentStatusIsOther").hide();
			$.lpqValidate.hideValidation($('#<%=IDPrefix%>txtEmploymentDescription'));
		}
		<%End If%>
		<%=IDPrefix%>updateIncomeTitle();
	}

	function <%=IDPrefix%>prev_updateIncomeTitle() {
		var sPrevEmploymentStatus = "";
		sPrevEmploymentStatus = $('#<%=IDPrefix%>prev_ddlEmploymentStatus option:selected').val();
		if (sPrevEmploymentStatus == 'RETIRED' || sPrevEmploymentStatus == 'RETIRED MILITARY' || sPrevEmploymentStatus == 'STUDENT' || sPrevEmploymentStatus == 'UNEMPLOYED'
            || sPrevEmploymentStatus == 'GOVERNMENT/DOD' || sPrevEmploymentStatus == 'HOMEMAKER' || sPrevEmploymentStatus == 'OTHER') {
			$('#<%=IDPrefix%>prev_lblGrossMonthlyIncome').removeClass("RequiredIcon"); // not required
			$.lpqValidate.hideValidation($('#<%=IDPrefix%>prev_txtGrossMonthlyIncome'));
        } else {
			$('#<%=IDPrefix%>prev_lblGrossMonthlyIncome').addClass("RequiredIcon"); // required   
        }
	}

    function <%=IDPrefix%>ValidateApplicantEmploymentXA() {
        <%--var ddlEmploymentStatus = $('#<%=IDPrefix%>ddlEmploymentStatus option:selected').val();
        var txtGrossMonthlyIncome = $('#<%=IDPrefix%>txtGrossMonthlyIncome').val();
    	var strMessage = '';--%>
       // var mAccountType = getMinorAccountTypeObj();
       // if (mAccountType.mAccountType != "") {
           // if (hasSecRoleEmployment() != "Y") {
          //      return ""; //skip validate because employment is hidden
          //  }
       // } else {
            //if (isHiddenSecEmployment()) { //skip validate if secondary employment is hidden
            //    return "";
            //}
    	// }

        if ($("#<%=IDPrefix%>divEmploymentSection.showfield-section").length > 0 && $("#<%=IDPrefix%>divEmploymentSection.showfield-section").hasClass("hidden")) return true;
        <%--var isVisibleEmployment = '<%=HasEmployment%>' != 'N';
        if (!isVisibleEmployment) {
            return ""; //skip validate because employment is hidden
        }--%>

      	var validate1 = $.lpqValidate("<%=IDPrefix%>ValidateFinancialInfoXA");
    	var validate2 = true;
    	<%If IDPrefix = "m_" Then%>
	    validate2 = EMPLogicMR.validate2();
    	<%ElseIf IDPrefix = "co_" Then%>
    	validate2 = EMPLogicJN.validate2();
    	<%ElseIf IDPrefix = "" Then%>
    	validate2 = EMPLogicPI.validate2();
		<%Else%>
    	validate2 = EMPLogic<%=IDPrefix%>.validate2();
    	<%End If%>
    	var validate3 = true;
	    var validate4 = true;
    	if ($('#<%=IDPrefix%>hdShowPreviousEmployment').val() == 'Y') {
    		validate3 = $.lpqValidate("<%=IDPrefix%>prev_ValidateFinancialInfoXA");
			<%If IDPrefix = "m_" Then%>
    		validate4 = EMPLogicMR_PREV.validate2();
    		<%ElseIf IDPrefix = "co_" Then%>
    		validate4 = EMPLogicJN_PREV.validate2();
    		<%ElseIf IDPrefix = "" Then%>
    		validate4 = EMPLogicPI_PREV.validate2();
			<%Else%>
    		validate4 = EMPLogic<%=IDPrefix%>PREV.validate2();
    		<%End If%>
	    }
    	return validate1 && validate2 && validate3 && validate4;
    }

	function <%=IDPrefix%>registerFinancialInfoValidatorXA() {
		$('#<%=IDPrefix%>ddlEmploymentStatus').observer({
			validators: [
				function (partial) {
					var status = $(this).val();
					if (Common.ValidateText(status) == false) {
						return "Employment Status is required";
					}
					return "";
				}
			],
			validateOnBlur: true,
			group: "<%=IDPrefix%>ValidateFinancialInfoXA"
		});

		<%If CollectDescriptionIfEmploymentStatusIsOther.ToUpper() = "Y" Then%>
		$('#<%=IDPrefix%>txtEmploymentDescription').observer({
			validators: [
                function (partial) {
                	var employmentDesc = $(this).val();
                	if ($('#<%=IDPrefix%>ddlEmploymentStatus').val().toUpperCase() == "OTHER" && !Common.ValidateText(employmentDesc)) {
                		return 'Other Employment Description is required';
                	}
                	return "";
                }
			],
			validateOnBlur: true,
			group: "<%=IDPrefix%>ValidateFinancialInfoXA"
		});
		<%End If%>


		$('#<%=IDPrefix%>txtGrossMonthlyIncome').observer({
			validators: [
				function (partial) {
					if ($("#<%=IDPrefix%>divGrossMonthlyIncome.showfield-section").length > 0 && $("#<%=IDPrefix%>divGrossMonthlyIncome.showfield-section").hasClass("hidden")) return "";//skip validate if Gross Income field is hidden
					//validate monthly income
					var sEmploymentStatus = "";
					var txtGrossMonthlyIncome = $(this).val();
					sEmploymentStatus = $('#<%=IDPrefix%>ddlEmploymentStatus option:selected').val();
						if (!(sEmploymentStatus == 'RETIRED' || sEmploymentStatus == 'RETIRED MILITARY' || sEmploymentStatus == 'STUDENT' || sEmploymentStatus == 'UNEMPLOYED' || sEmploymentStatus == 'GOVERNMENT/DOD' || sEmploymentStatus == 'HOMEMAKER' || sEmploymentStatus == 'OTHER')) {
							if (!Common.ValidateTextNonZero(txtGrossMonthlyIncome)) {
								return 'Gross Monthly Income is required';
							}
						}
					return "";
				}
			],
			validateOnBlur: true,
			group: "<%=IDPrefix%>ValidateFinancialInfoXA"
		});
		$('#<%=IDPrefix%>prev_ddlEmploymentStatus').observer({
			validators: [
				function (partial) {
					var status = $(this).val();
					if (Common.ValidateText(status) == false) {
						return "Employment Status is required";
					}
					return "";
				}
			],
			validateOnBlur: true,
			group: "<%=IDPrefix%>prev_ValidateFinancialInfoXA"
		});
		$('#<%=IDPrefix%>prev_txtGrossMonthlyIncome').observer({
			validators: [
				function (partial) {
					if ($("#<%=IDPrefix%>divGrossMonthlyIncome.showfield-section").length > 0 && $("#<%=IDPrefix%>divGrossMonthlyIncome.showfield-section").hasClass("hidden")) return "";//skip validate if Gross Income field is hidden 
					//validate monthly income
					var sEmploymentStatus = "";
					var txtGrossMonthlyIncome = $(this).val();
					sEmploymentStatus = $('#<%=IDPrefix%>prev_ddlEmploymentStatus option:selected').val();
						if (!(sEmploymentStatus == 'RETIRED' || sEmploymentStatus == 'RETIRED MILITARY' || sEmploymentStatus == 'STUDENT' || sEmploymentStatus == 'UNEMPLOYED' || sEmploymentStatus == 'GOVERNMENT/DOD' || sEmploymentStatus == 'HOMEMAKER' || sEmploymentStatus == 'OTHER')) {
							if (!Common.ValidateTextNonZero(txtGrossMonthlyIncome)) {
								return 'Gross Monthly Income is required';
							}
						}
					return "";
				}
			],
			validateOnBlur: true,
			group: "<%=IDPrefix%>prev_ValidateFinancialInfoXA"
		});
	}
	<%=IDPrefix%>registerFinancialInfoValidatorXA();

 
	function <%=IDPrefix%>SetApplicantEmployment(appInfo) {
		if ($("#<%=IDPrefix%>divEmploymentSection.showfield-section").length > 0 && $("#<%=IDPrefix%>divEmploymentSection.showfield-section").hasClass("hidden")) {
			return appInfo;
		}
		var ddlEmploymentStatus = $('#<%=IDPrefix%>ddlEmploymentStatus option:selected').val();
		appInfo.<%=IDPrefix%>EmploymentStatus = ddlEmploymentStatus;

		<%If String.IsNullOrEmpty(IDPrefix) Then%>
		EMPLogicPI.setValueToAppObject(appInfo); // primary    
    	<%ElseIf IDPrefix = "co_" Then%>
		EMPLogicJN.setValueToAppObject(appInfo); // for joint    
    	<%ElseIf IDPrefix = "m_" Then%>
		EMPLogicMR.setValueToAppObject(appInfo); //set app infor for minor
    	<%Else%>
		EMPLogic<%=IDPrefix%>.setValueToAppObject(appInfo);
		<%End If%>
		appInfo.<%=IDPrefix%>EmploymentDescription = "";
		<%If CollectDescriptionIfEmploymentStatusIsOther.ToUpper() = "Y" Then%>
		if (appInfo.<%=IDPrefix%>EmploymentStatus.toUpperCase() == "OTHER") {
			appInfo.<%=IDPrefix%>EmploymentDescription = $("#<%=IDPrefix%>txtEmploymentDescription").val();
    	}
		<%End If%>

		<%If Not CollectJobTitleOnly Then%>
        var durationYear = $('#<%=IDPrefix%>txtEmployedDuration_year option:selected').val();
        var durationMonth = $('#<%=IDPrefix%>txtEmployedDuration_month option:selected').val();
        <%--var isVisibleEmployment = '<%=HasEmployment%>' != 'N';--%>
        //var mAccountTypeObj = getMinorAccountTypeObj();
        var en_Date = "";
        //get enlistment date for active miltary if it selected
        if (ddlEmploymentStatus == "ACTIVE MILITARY") {
            en_Date = getEnlistmentDate(durationYear, durationMonth);
        }
		<%If EnableGrossMonthlyIncome Then%>
		if ($('#<%=IDPrefix%>divGrossMonthlyIncome').closest("div.showfield-section").length == 0 || $('#<%=IDPrefix%>divGrossMonthlyIncome').closest("div.showfield-section").hasClass("hidden") == false) {
			appInfo.<%=IDPrefix%>txtGrossMonthlyIncome = Common.GetFloatFromMoney($('#<%=IDPrefix%>txtGrossMonthlyIncome').val());
        }
		<%End If%>
        //enlistment date
    	appInfo.<%=IDPrefix%>EnlistmentDate = en_Date;
		<%End If%>
    	
    	//set previous employment infor
		var hasPreviousEmployment = $('#<%=IDPrefix%>hdShowPreviousEmployment').val();
    	appInfo.<%=IDPrefix%>HasPreviousEmployment = hasPreviousEmployment;
    	if (hasPreviousEmployment == 'Y') {
    		var prev_ddlEmloymentStatus = $("#<%=IDPrefix%>prev_ddlEmploymentStatus option:selected").val();
            var prev_durationYear = $('#<%=IDPrefix%>prev_txtEmployedDuration_year option:selected').val();
            var prev_durationMonth = $('#<%=IDPrefix%>prev_txtEmployedDuration_month option:selected').val();

			<%If String.IsNullOrEmpty(IDPrefix) Then%>
    		EMPLogicPI_PREV.setValueToAppObject(appInfo); // primary
    		<%ElseIf IDPrefix = "co_" Then%>
    		EMPLogicJN_PREV.setValueToAppObject(appInfo); // for joint
    		<%ElseIf IDPrefix = "m_" Then%>
    		EMPLogicMR_PREV.setValueToAppObject(appInfo); //set app infor for minor
			<%Else%>
    		EMPLogic<%=IDPrefix%>PREV.setValueToAppObject(appInfo); //set app infor for minor
			<%End If%>

			var prev_enlistmentDate = "";
			if (prev_ddlEmloymentStatus == "ACTIVE MILITARY") {
				prev_enlistmentDate = getEnlistmentDate(prev_durationYear, prev_durationMonth);
			}
			appInfo.<%=IDPrefix%>prev_txtEmploymentStartDate = prev_enlistmentDate;
    		appInfo.<%=IDPrefix%>prev_EmploymentStatus = prev_ddlEmloymentStatus;

			<%If EnableGrossMonthlyIncome Then%>
    		if ($('#<%=IDPrefix%>divGrossMonthlyIncome').closest("div.showfield-section").length == 0 || $('#<%=IDPrefix%>divGrossMonthlyIncome').closest("div.showfield-section").hasClass("hidden") == false)
    		{
    			appInfo.<%=IDPrefix%>prev_grossMonthlyIncome = Common.GetFloatFromMoney($("#<%=IDPrefix%>prev_txtGrossMonthlyIncome").val());

			}
			<%End If%>
        }
	}

	function <%=IDPrefix%>ViewEmploymentInfo() {
		if ($("#<%=IDPrefix%>divEmploymentSection.showfield-section").length > 0 && $("#<%=IDPrefix%>divEmploymentSection.showfield-section").hasClass("hidden")) {
			$(this).hide();
			$(this).prev("div.row").hide();
			return "";
		}
		var ddlEmploymentStatus = $('#<%=IDPrefix%>ddlEmploymentStatus option:selected').val();

        var jobTitleElement = $('#<%=IDPrefix%>txtJobTitle');
	    var txtJobTitle = "";
	    if (jobTitleElement.length > 0) {
	    	txtJobTitle = htmlEncode(jobTitleElement.val());
	    	if (jobTitleElement.attr('type') != 'text') {
	    		txtJobTitle = jobTitleElement.find('option:selected').text();
	    	}
	    }
        var strEmploymentInfo = "";
        //employment infor based on the employment status
        if ($.trim(ddlEmploymentStatus) !== "") {
			strEmploymentInfo += '<div class="col-sm-6 col-xs-12"><div class="row"><div class="col-xs-6 text-right row-title"><span class="bold">Employment Status</span></div><div class="col-xs-6 text-left row-data"><span>' + ddlEmploymentStatus + '</span></div></div></div>';
        }

        var ddlBranchService = $('#<%=IDPrefix%>ddlBranchOfService option:selected').val();
        var ddlPayGrade = $('#<%=IDPrefix%>ddlPayGrade option:selected').val();
		var txtBusinessType = htmlEncode($('#<%=IDPrefix%>txtBusinessType').val());
		var txtEmployer = htmlEncode($('#<%=IDPrefix%>txtEmployer').val());
		var txtEmploymentDescription = htmlEncode($('#<%=IDPrefix%>txtEmploymentDescription').val());
		var ddlLengthOfEmploymentYear = $('#<%=IDPrefix%>txtEmployedDuration_year option:selected').val();
		var ddlLengthOfEmploymentMonth = $('#<%=IDPrefix%>txtEmployedDuration_month option:selected').val();
    		
		<%If CollectDescriptionIfEmploymentStatusIsOther.ToUpper() = "Y" Then%>
        if (ddlEmploymentStatus == "OTHER" && txtEmploymentDescription != '') {
        	strEmploymentInfo += '<div class="col-sm-6 col-xs-12"><div class="row"><div class="col-xs-6 text-right row-title"><span class="bold">Other Employment Description</span></div><div class="col-xs-6 text-left row-data"><span>' + txtEmploymentDescription + '</span></div></div></div>';
        }
		<%End If%>
		var strJobTitleLabel = "Profession/Job Title";
		if (ddlEmploymentStatus == "ACTIVE MILITARY") {
			strJobTitleLabel = "Profession/MOS";
		}else if (ddlEmploymentStatus == "RETIRED MILITARY") {
			strJobTitleLabel = "Former Profession/MOS";
		}else if (ddlEmploymentStatus == "RETIRED" || ddlEmploymentStatus == "UNEMPLOYED") {
			strJobTitleLabel = "Former Profession/Job Title";
		}

		if (txtJobTitle != '' && txtJobTitle != undefined) {
			strEmploymentInfo += '<div class="col-sm-6 col-xs-12"><div class="row"><div class="col-xs-6 text-right row-title"><span class="bold">' + strJobTitleLabel + '</span></div><div class="col-xs-6 text-left row-data"><span>' + txtJobTitle + '</span></div></div></div>';
		}
		<%If Not CollectJobTitleOnly then %>
		if (ddlEmploymentStatus == "RETIRED MILITARY" || ddlEmploymentStatus == "ACTIVE MILITARY") {
            if (ddlBranchService != '') {
                strEmploymentInfo += '<div class="col-sm-6 col-xs-12"><div class="row"><div class="col-xs-6 text-right row-title"><span class="bold">Branch of Service</span></div><div class="col-xs-6 text-left row-data"><span>' + ddlBranchService + '</span></div></div></div>';
            }
            if (ddlPayGrade != '') {
                strEmploymentInfo += '<div class="col-sm-6 col-xs-12"><div class="row"><div class="col-xs-6 text-right row-title"><span class="bold">Pay Grade</span></div><div class="col-xs-6 text-left row-data"><span>' + ddlPayGrade + '</span></div></div></div>';
            }
        }
        else {
            if (txtEmployer != '' && txtEmployer != undefined) {
                strEmploymentInfo += '<div class="col-sm-6 col-xs-12"><div class="row"><div class="col-xs-6 text-right row-title"><span class="bold">Employer</span></div><div class="col-xs-6 text-left row-data"><span>' + txtEmployer + '</span></div></div></div>';
            }
        }

        if (ddlEmploymentStatus == "OWNER" || ddlEmploymentStatus == "SELF EMPLOYED") {
            if (txtBusinessType != '') {
                strEmploymentInfo += '<div class="col-sm-6 col-xs-12"><div class="row"><div class="col-xs-6 text-right row-title"><span class="bold">Business Type</span></div><div class="col-xs-6 text-left row-data"><span>' + txtBusinessType + '</span></div></div></div>';
            }
        }
        if (ddlEmploymentStatus == "GOVERNMENT/DOD") {
            if (ddlPayGrade != '') {
                strEmploymentInfo += '<div class="col-sm-6 col-xs-12"><div class="row"><div class="col-xs-6 text-right row-title"><span class="bold">Pay Grade</span></div><div class="col-xs-6 text-left row-data"><span>' + ddlPayGrade + '</span></div></div></div>';
            }
        }
        if (Number(ddlLengthOfEmploymentYear) > 0 || Number(ddlLengthOfEmploymentMonth) > 0) {
            var strEmpStatus = "";
            //if (ddlEmploymentStatus == "RETIRED MILITARY" || ddlEmploymentStatus == "ACTIVE MILITARY") {
            if (ddlEmploymentStatus == "ACTIVE MILITARY") {//only ACTIVE MILITARY has Time In Service <-- based on employment.logic.js
                strEmpStatus = "Time In Service";
            } else {
                strEmpStatus = "Employment Duration";
            }
            var strEmpDuration = "";
            if (Number(ddlLengthOfEmploymentYear) > 0) {
                strEmpDuration += ddlLengthOfEmploymentYear + " yrs";
            }
            if (Number(ddlLengthOfEmploymentMonth) > 0) {
                strEmpDuration += " " + ddlLengthOfEmploymentMonth + " mos";
            }
            strEmploymentInfo += '<div class="col-sm-6 col-xs-12"><div class="row"><div class="col-xs-6 text-right row-title"><span class="bold">' + strEmpStatus + '</span></div><div class="col-xs-6 text-left row-data"><span>' + strEmpDuration + '</span></div></div></div>';
            var strPayGrades = "E1,E2,E3,E4,E5,E6,E7,E8,E9";
            if (strPayGrades.indexOf(ddlPayGrade) != -1 && ddlEmploymentStatus == "ACTIVE MILITARY") {//only ACTIVE MILITARY has Time In Service <-- based on employment.logic.js
                strEmploymentInfo += '<div class="col-sm-6 col-xs-12"><div class="row"><div class="col-xs-6 text-right row-title"><span class="bold">ETS/Expiration Date</span></div><div class="col-xs-6 text-left row-data"><span>' + $('#<%=IDPrefix%>txtETS').val() + '</span></div></div></div>';
            }
        }
		<%If EnableGrossMonthlyIncome Then%>//no view if gross income field is hidden
		if ($('#<%=IDPrefix%>divGrossMonthlyIncome').closest("div.showfield-section").length == 0 || $('#<%=IDPrefix%>divGrossMonthlyIncome').closest("div.showfield-section").hasClass("hidden") == false) {
			var txtGrossMonthlyIncome = htmlEncode($('#<%=IDPrefix%>txtGrossMonthlyIncome').val());
			if (Common.ValidateText(txtGrossMonthlyIncome)) {
				txtGrossMonthlyIncome = Common.FormatCurrency(txtGrossMonthlyIncome, true);
				strEmploymentInfo += '<div class="col-sm-6 col-xs-12"><div class="row"><div class="col-xs-6 text-right row-title"><span class="bold">Gross Monthly Income (before taxes)</span></div><div class="col-xs-6 text-left row-data"><span>' + txtGrossMonthlyIncome + '</span></div></div></div>';
			}
		}
		<%End If%>
		<%End If%>
		

        $(this).hide();
        $(this).prev("div.row").hide();
        if (strEmploymentInfo !== "") {
            $(this).show();
            $(this).prev("div.row").show();
        }
        return strEmploymentInfo;
    }


	function <%=IDPrefix%>ViewPrevEmploymentInfo() {
		if ($('#<%=IDPrefix%>hdShowPreviousEmployment').val() != "Y") {
    		$(this).hide();
    		$(this).prev("div.row").hide();
    		return "";
    	}
    	
    	var ddlEmploymentStatus = $('#<%=IDPrefix%>prev_ddlEmploymentStatus option:selected').val();
    	var ddlBranchService = $('#<%=IDPrefix%>prev_ddlBranchOfService option:selected').val();
    	var ddlPayGrade = $('#<%=IDPrefix%>prev_ddlPayGrade option:selected').val();
    	var txtBusinessType = htmlEncode($('#<%=IDPrefix%>prev_txtBusinessType').val());
    	var txtEmployer = htmlEncode($('#<%=IDPrefix%>prev_txtEmployer').val());
    	var ddlLengthOfEmploymentYear = $('#<%=IDPrefix%>prev_txtEmployedDuration_year option:selected').val();
    	var ddlLengthOfEmploymentMonth = $('#<%=IDPrefix%>prev_txtEmployedDuration_month option:selected').val();
    	var jobTitleElement = $('#<%=IDPrefix%>prev_txtJobTitle');
    	var txtJobTitle = "";
    	if (jobTitleElement.length > 0) {
    		txtJobTitle = htmlEncode(jobTitleElement.val());
    		if (jobTitleElement.attr('type') != 'text') {
    			txtJobTitle = jobTitleElement.find('option:selected').text();
    		}
    	}
    	var strPrevEmploymentInfo = "";
    	strPrevEmploymentInfo += '<div class="col-sm-6 col-xs-12"><div class="row"><div class="col-xs-6 text-right row-title"><span class="bold">Employment Status</span></div><div class="col-xs-6 text-left row-data"><span>' + ddlEmploymentStatus + '</span></div></div></div>';
    	if (ddlEmploymentStatus == "RETIRED MILITARY" || ddlEmploymentStatus == "ACTIVE MILITARY") {
    		if (txtJobTitle != '' && txtJobTitle != undefined) {
    			strPrevEmploymentInfo += '<div class="col-sm-6 col-xs-12"><div class="row"><div class="col-xs-6 text-right row-title"><span class="bold">Profession/MOS</span></div><div class="col-xs-6 text-left row-data"><span>' + txtJobTitle + '</span></div></div></div>';
    		}
    		if (ddlBranchService != '') {
    			strPrevEmploymentInfo += '<div class="col-sm-6 col-xs-12"><div class="row"><div class="col-xs-6 text-right row-title"><span class="bold">Branch of Service</span></div><div class="col-xs-6 text-left row-data"><span>' + ddlBranchService + '</span></div></div></div>';
    		}
    		if (ddlPayGrade != '') {
    			strPrevEmploymentInfo += '<div class="col-sm-6 col-xs-12"><div class="row"><div class="col-xs-6 text-right row-title"><span class="bold">Pay Grade</span></div><div class="col-xs-6 text-left row-data"><span>' + ddlPayGrade + '</span></div></div></div>';
    		}
    	}
    	else {
    		if (txtJobTitle != '' && txtJobTitle != undefined) {
    			strPrevEmploymentInfo += '<div class="col-sm-6 col-xs-12"><div class="row"><div class="col-xs-6 text-right row-title"><span class="bold">Profession/Job Title</span></div><div class="col-xs-6 text-left row-data"><span>' + txtJobTitle + '</span></div></div></div>';
    		}
    		if (txtEmployer != '' && txtEmployer != undefined) {
    			strPrevEmploymentInfo += '<div class="col-sm-6 col-xs-12"><div class="row"><div class="col-xs-6 text-right row-title"><span class="bold">Employer</span></div><div class="col-xs-6 text-left row-data"><span>' + txtEmployer + '</span></div></div></div>';
    		}
    	}
    	if (ddlEmploymentStatus == "OWNER" || ddlEmploymentStatus == "SELF EMPLOYED") {
    		if (txtBusinessType != '') {
    			strPrevEmploymentInfo += '<div class="col-sm-6 col-xs-12"><div class="row"><div class="col-xs-6 text-right row-title"><span class="bold">Business Type</span></div><div class="col-xs-6 text-left row-data"><span>' + txtBusinessType + '</span></div></div></div>';
    		}
    	}
    	if (ddlEmploymentStatus == "GOVERNMENT/DOD") {
    		if (ddlPayGrade != '') {
    			strPrevEmploymentInfo += '<div class="col-sm-6 col-xs-12"><div class="row"><div class="col-xs-6 text-right row-title"><span class="bold">Pay Grade</span></div><div class="col-xs-6 text-left row-data"><span>' + ddlPayGrade + '</span></div></div></div>';
    		}
    	}
    	if (Number(ddlLengthOfEmploymentYear) > 0 || Number(ddlLengthOfEmploymentMonth) > 0) {
    		var strEmpStatus = "";
    		//if (ddlEmploymentStatus == "RETIRED MILITARY" || ddlEmploymentStatus == "ACTIVE MILITARY") {
    		if (ddlEmploymentStatus == "ACTIVE MILITARY") {//only ACTIVE MILITARY has Time In Service <-- based on employment.logic.js
    			strEmpStatus = "Time In Service";
    		} else {
    			strEmpStatus = "Employment Duration";
    		}
    		var strEmpDuration = "";
    		if (Number(ddlLengthOfEmploymentYear) > 0) {
    			strEmpDuration += ddlLengthOfEmploymentYear + " yrs";
    		}
    		if (Number(ddlLengthOfEmploymentMonth) > 0) {
    			strEmpDuration += " " + ddlLengthOfEmploymentMonth + " mos";
    		}
    		strPrevEmploymentInfo += '<div class="col-sm-6 col-xs-12"><div class="row"><div class="col-xs-6 text-right row-title"><span class="bold">' + strEmpStatus + '</span></div><div class="col-xs-6 text-left row-data"><span>' + strEmpDuration + '</span></div></div></div>';
    		var strPayGrades = "E1,E2,E3,E4,E5,E6,E7,E8,E9";
    		if (strPayGrades.indexOf(ddlPayGrade) != -1 && ddlEmploymentStatus == "ACTIVE MILITARY") {//only ACTIVE MILITARY has Time In Service <-- based on employment.logic.js
    			strPrevEmploymentInfo += '<div class="col-sm-6 col-xs-12"><div class="row"><div class="col-xs-6 text-right row-title"><span class="bold">ETS/Expiration Date</span></div><div class="col-xs-6 text-left row-data"><span>' + $('#<%=IDPrefix%>prev_txtETS').val() + '</span></div></div></div>';
            }
		}

		

		<%If EnableGrossMonthlyIncome Then%>//no view if gross income field is hidden
		if ($('#<%=IDPrefix%>divGrossMonthlyIncome').closest("div.showfield-section").length == 0 || $('#<%=IDPrefix%>divGrossMonthlyIncome').closest("div.showfield-section").hasClass("hidden") == false) {
			var txtGrossMonthlyIncome = htmlEncode($('#<%=IDPrefix%>prev_txtGrossMonthlyIncome').val());
			if (Common.ValidateText(txtGrossMonthlyIncome)) {
				txtGrossMonthlyIncome = Common.FormatCurrency(txtGrossMonthlyIncome, true);
				strPrevEmploymentInfo += '<div class="col-sm-6 col-xs-12"><div class="row"><div class="col-xs-6 text-right row-title"><span class="bold">Gross Monthly Income (before taxes)</span></div><div class="col-xs-6 text-left row-data"><span>' + txtGrossMonthlyIncome + '</span></div></div></div>';
			}
		}
		<%End If%>

		$(this).hide();
		$(this).prev("div.row").hide();
		if (strPrevEmploymentInfo !== "") {
			$(this).show();
			$(this).prev("div.row").show();
		}
		return strPrevEmploymentInfo;
	}

	function <%=IDPrefix%>ShowPreviousEmploymentInfo() {
		var oDurationTotalInMonth = 0;
		var oDurationMonth = $('#<%=IDPrefix%>txtEmployedDuration_month option:selected').val();
        var oDurationYear = $('#<%=IDPrefix%>txtEmployedDuration_year option:selected').val();
	    var selectedMonth = false, selectedYear = false;
	  
	    if (oDurationMonth == undefined || (oDurationMonth != undefined && oDurationMonth.trim() == "")) {
			oDurationMonth = 0;
		} else {
			oDurationMonth = parseInt(oDurationMonth);
			selectedMonth = true;
		}
		//duration year
	    if (oDurationYear == undefined || (oDurationYear != undefined && oDurationYear.trim() == "")) {
			oDurationYear = 0;
		} else {
			oDurationYear = parseInt(oDurationYear) * 12;//convert year to month
			selectedYear = true;
		}
		//calculate total duration in months
		oDurationTotalInMonth = oDurationMonth + oDurationYear;
		$('#<%=IDPrefix%>hdShowPreviousEmployment').val("N");
        $('#<%=IDPrefix%>divPreviousEmploymentInfo').hide();
		if (selectedYear || selectedMonth) {
			var previous_employment_threshold = $('#hdPrevEmploymentThreshold').val();
			if (previous_employment_threshold != "") {
				if (!isNaN(previous_employment_threshold)) {
					if (oDurationTotalInMonth < parseInt(previous_employment_threshold)) {
						$('#<%=IDPrefix%>divPreviousEmploymentInfo').show();
                        $('#<%=IDPrefix%>hdShowPreviousEmployment').val("Y");
                    }
				}
			}
		} //end selectedYear ...
        <%--//Temp for testing
        $('#<%=IDPrefix%>divPreviousEmploymentInfo').show();
        $('#<%=IDPrefix%>hdShowPreviousEmployment').val("Y");--%>
	}//end showPreviousEmploymentInfo
	function  <%=IDPrefix%>autoFillData_EmploymentInfo() {
        $('#<%=IDPrefix%>ddlEmploymentStatus').val("HOMEMAKER");
        $('#<%=IDPrefix%>txtGrossMonthlyIncome').val("4000").trigger("blur");
        // job title field could be a textbox field or a dropdown selection field
        if ($("select#<%=IDPrefix%>txtJobTitle").length > 0) {
            $("select#<%=IDPrefix%>txtJobTitle option:selected").val("HOME");    
            $("select#<%=IDPrefix%>txtJobTitle").selectmenu().selectmenu('refresh');
        } else {
            $("#<%=IDPrefix%>txtJobTitle").val("housekeeper").trigger('blur');
        }
       
	}
</script>