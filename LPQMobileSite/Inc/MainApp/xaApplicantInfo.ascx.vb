﻿Imports System.Xml
Imports LPQMobile.BO
Imports LPQMobile.Utils
Imports LPQMobile.Utils.Common
Imports System.Web.Security.AntiXss

Partial Class Inc_MainApp_xaApplicantInfo
    Inherits CBaseUserControl


#Region "Fields"
	Public LiveMonthsDropdown As String
	Public MaritalStatusDropdown As String
	Public CitizenshipStatusDropdown As String
    Public EmployeeOfLenderDropdown As String
    Public LenderName As String = ""
    Public HasMotherName As String = ""
    Public HasCitizenship As String = ""
    Public HasGender As String = ""
	'Public HasExistingMember As String = ""
	Private _Config As XmlElement
	Protected _blockApplicantAgeUnder As Integer = 0
	Public Property LegacyScandocAvailable As Boolean = False
	Public Property LaserScandocAvailable As Boolean = False
	Protected Property RelationshipToPrimary As String = ""
    Public Property Config() As XmlElement
        Get
            Return _Config
        End Get
        Set(ByVal value As XmlElement)
            _Config = value
        End Set
    End Property

    Protected ReadOnly Property FName() As String
        Get
			Return Common.SafeString(AntiXssEncoder.HtmlEncode(Request.Params(Left(IDPrefix, 2) & "FName"), True))
		End Get
	End Property

	Protected ReadOnly Property SSNDisabledString As String
		Get
			Return If(Common.SafeString(HttpUtility.UrlDecode(Request.Params(Left(IDPrefix, 2) & "SSN"))) <> "", "disabled", "")
		End Get
	End Property

	Protected ReadOnly Property DOBDisabledString As String
		Get
			Return If(Common.SafeString(HttpUtility.UrlDecode(Request.Params(Left(IDPrefix, 2) & "DOB"))) <> "", "disabled", "")
		End Get
	End Property

	Protected ReadOnly Property FNameDisabledString As String
		Get
			Return If(Common.SafeString(HttpUtility.UrlDecode(Request.Params(Left(IDPrefix, 2) & "FName"))) <> "", "disabled", "")
		End Get
	End Property

	Protected ReadOnly Property LNameDisabledString As String
		Get
			Return If(Common.SafeString(HttpUtility.UrlDecode(Request.Params(Left(IDPrefix, 2) & "LName"))) <> "", "disabled", "")
		End Get
	End Property

	Protected ReadOnly Property MNameDisabledString As String
		Get
			Return If(Common.SafeString(HttpUtility.UrlDecode(Request.Params(Left(IDPrefix, 2) & "MName"))) <> "", "disabled", "")
		End Get
	End Property

	Protected ReadOnly Property MemberNumberDisabledString As String
		Get
			Return If(Common.SafeString(HttpUtility.UrlDecode(Request.Params(Left(IDPrefix, 2) & "MemberNumber"))) <> "", "disabled", "")
		End Get
	End Property

	Protected ReadOnly Property MName() As String
		Get
			Return Common.SafeString(AntiXssEncoder.HtmlEncode(Request.Params(Left(IDPrefix, 2) & "MName"), True))
		End Get
	End Property
	Protected ReadOnly Property LName() As String
		Get
			Return Common.SafeString(AntiXssEncoder.HtmlEncode(Request.Params(Left(IDPrefix, 2) & "LName"), True))
		End Get
	End Property
	Protected ReadOnly Property SSN() As String
		Get
			Return Common.SafeString(AntiXssEncoder.HtmlEncode(Request.Params(Left(IDPrefix, 2) & "SSN"), True)).Replace("/", "").Replace("\", "").Replace("-", "")
		End Get
	End Property
	Protected ReadOnly Property SSN1() As String
		Get
			If SSN.Length > 2 Then
				Return SSN.Substring(0, 3)
			Else
				Return ""
			End If
		End Get
	End Property
	Protected ReadOnly Property SSN2() As String
		Get
			If SSN.Length > 4 Then
				Return SSN.Substring(3, 2)
			Else
				Return ""
			End If
		End Get
	End Property
	Protected ReadOnly Property SSN3() As String
		Get
			If SSN.Length > 8 Then
				Return SSN.Substring(5, 4)
			Else
				Return ""
			End If
		End Get
	End Property
	Protected ReadOnly Property DOB() As String	'DateTime
		Get
			Dim _DOB As String = Common.SafeString(AntiXssEncoder.HtmlEncode(Request.Params(Left(IDPrefix, 2) & "DOB"), True)).Replace("/", "").Replace("\", "").Replace("-", "")
			'Dim _Month As String = ""
			'If _DOB.Length >= 2 Then
			'    _Month = _DOB.Substring(0, 2)
			'Else
			'    _Month = "01"
			'End If

			'Dim _Day As String = ""
			'If _DOB.Length >= 4 Then
			'    _Day = _DOB.Substring(2, 2)
			'Else
			'    _Day = "01"
			'End If

			'Dim _Year As String = ""
			'If _DOB.Length >= 8 Then
			'    _Year = _DOB.Substring(4, 4)
			'Else
			'    _Year = "1900"
			'End If

			'Return Common.SafeDate(_Month & "/" & _Day & "/" & _Year)
			Return _DOB
		End Get
	End Property
	Protected ReadOnly Property DOBMonth() As String
		Get
			If DOB.Length > 1 Then
				Return DOB.Substring(0, 2)
			Else
				Return ""
			End If
		End Get
	End Property
	Protected ReadOnly Property DOBDay() As String
		Get
			If DOB.Length > 3 Then
				Return DOB.Substring(2, 2)
			Else
				Return ""
			End If
		End Get
	End Property
	Protected ReadOnly Property DOBYear() As String
		Get
			If DOB.Length > 7 Then
				Return DOB.Substring(4, 4)
			Else
				Return ""
			End If
		End Get
	End Property

	''Protected ReadOnly Property YearBirth() As Integer
	''    Get
	''        Return DOB.Year
	''    End Get
	''End Property

	''Protected ReadOnly Property MonthBirth() As Integer
	''    Get
	''        Return DOB.Month
	''    End Get
	''End Property

	''Protected ReadOnly Property DayBirth() As Integer
	''    Get
	''        Return DOB.Day
	''    End Get
	''End Property

	Protected _MonthDDL As String = ""
	Protected _DayDDL As String = ""
	Protected _YearDDL As String = ""

	Protected ReadOnly Property MemberNumber() As String
		Get
			Return AntiXssEncoder.HtmlEncode(Request.Params(Left(IDPrefix, 2) & "MemberNumber"), True)
		End Get
	End Property
	Protected ReadOnly Property HomePhone() As String
		Get
			Return AntiXssEncoder.HtmlEncode(Request.Params(Left(IDPrefix, 2) & "HomePhone"), True).Replace("-", "")
		End Get
	End Property
	Protected ReadOnly Property WorkPhone() As String
		Get
			Return AntiXssEncoder.HtmlEncode(Request.Params(Left(IDPrefix, 2) & "WorkPhone"), True).Replace("-", "")
		End Get
	End Property


	Public Property EnableMotherMaidenName As Boolean
	Public Property EnableCitizenshipStatus As Boolean
	Public Property EnableGender As Boolean

	Public Property EnableMaritalStatus As Boolean

	Public Property EnableMemberNumber As Boolean = False	' by default
	Public Property MemberNumberRequired As Boolean = False ' by default

	Public _FieldMaxLength As String = "50"
	Public Property InstitutionType As CEnum.InstitutionType
#End Region

	Protected Sub Page_Load1(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
		CPBLogger.PBLogger.Component = Me.GetType.ToString
		CPBLogger.PBLogger.Context = Request.UserHostAddress
		If (IsPostBack = False) Then

			'BCU customization
			Dim sLenderref As String = Request.QueryString("lenderref")
			If SafeString(sLenderref).ToUpper.StartsWith("BCU") Then _FieldMaxLength = 40

			Dim tempp As String = IDPrefix
			'SetHideField(_Config)
			''generateDayDDL()
			''generateMonthDDL()
			''generateYearDDL()
			Dim parent As CBasePage = TryCast(Me.Page, CBasePage)
			If parent._CurrentWebsiteConfig IsNot Nothing Then
				Integer.TryParse(Common.getNodeAttributes(parent._CurrentWebsiteConfig, "XA_LOAN/BEHAVIOR", "block_applicant_age_under"), _blockApplicantAgeUnder)
			End If
			Dim relationshipToPrimaryList As Dictionary(Of String, String) = parent._CurrentWebsiteConfig.GetEnumItems("RELATIONSHIP_PRIMARY")
			If relationshipToPrimaryList IsNot Nothing AndAlso parent._CurrentWebsiteConfig.GetEnumItems("RELATIONSHIP_PRIMARY").Count > 0 Then
				RelationshipToPrimary = Common.RenderDropdownlistWithEmpty(relationshipToPrimaryList, "", "--Please Select--")
			End If
		End If
	End Sub

	'Private Sub SetHideField(ByVal poConfig As XmlElement)
	'       ''If SafeString(Request.QueryString("type")) = "2" Or SafeString(Request.QueryString("type")) = "2a" Or SafeString(Request.QueryString("type")) = "2b" Then
	'       ''	' divMemberNumber_lb.Visible = True    'existing member
	'       ''	divMemberNumber_txt.Visible = True
	'       ''	HasExistingMember = "Y"

	'       ''Else
	'       ''	'  divMemberNumber_lb.Visible = False   'new member
	'       ''	divMemberNumber_txt.Visible = False
	'       ''	HasExistingMember = "N"
	'       ''End If

	'End Sub

    ''Private Sub generateMonthDDL()
    ''    Dim builder As New StringBuilder()        
    ''    Dim months As String() = {"January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"}
    ''    builder.AppendLine("<option value='0'>MONTH</option>")

    ''    For i As Integer = 1 To 12
    ''        Dim selected As String = IIf(MonthBirth = i, "selected='selected'", "").ToString()
    ''        If i < 10 Then
    ''            builder.AppendLine("<option " & selected & " value='0" & i.ToString() & "'>" & months(i - 1) & "</option>")
    ''        Else
    ''            builder.AppendLine("<option " & selected & " value='" & i.ToString() & "'>" & months(i - 1) & "</option>")
    ''        End If
    ''    Next

    ''    ''_MonthDDL = builder.ToString()
    ''End Sub

    ''Private Sub generateDayDDL()
    ''    Dim builder As New StringBuilder()
    ''    builder.AppendLine("<option value='0'>DAY</option>")

    ''    For i As Integer = 1 To 31
    ''        Dim selected As String = IIf(DayBirth = i, "selected='selected'", "").ToString()
    ''        If i < 10 Then
    ''            builder.AppendLine("<option " & selected & " value='0" & i.ToString() & "'>" & i.ToString() & "</option>")
    ''        Else
    ''            builder.AppendLine("<option " & selected & " value='" & i.ToString() & "'>" & i.ToString() & "</option>")
    ''        End If
    ''    Next

    ''_DayDDL = builder.ToString()
    ''End Sub

    ''Private Sub generateYearDDL()
    ''    Dim builder As New StringBuilder()
    ''    builder.AppendLine("<option value='0'>YEAR</option>")

    ''    For i As Integer = Now.Year To 1900 Step -1
    ''        Dim selected As String = IIf(YearBirth = i, "selected='selected'", "").ToString()
    ''        builder.AppendLine("<option " & selected & " value='" & i.ToString() & "'>" & i.ToString() & "</option>")
    ''    Next

    ''    ''_YearDDL = builder.ToString()
    ''End Sub

End Class
