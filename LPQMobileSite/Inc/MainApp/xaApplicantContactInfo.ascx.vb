﻿Imports LPQMobile.Utils
Imports System.Web.Security.AntiXss


Partial Class Inc_MainApp_xaApplicantContactInfo
    Inherits CBaseUserControl

#Region "Fields"
	Protected ReadOnly Property HomePhone() As String
		Get
			Return Common.SafeString(AntiXssEncoder.HtmlEncode(Request.Params(Left(IDPrefix, 2) & "HomePhone"), True))
		End Get
	End Property
    Protected ReadOnly Property WorkPhone() As String
        Get
            Return Common.SafeString(AntiXssEncoder.HtmlEncode(Request.Params(Left(IDPrefix, 2) & "WorkPhone"), True))
        End Get
    End Property

    Protected ReadOnly Property MobilePhone() As String
        Get
            If Not String.IsNullOrEmpty(Request.Params(Left(IDPrefix, 2) & "MobilePhone")) Then
                Return Common.SafeString(AntiXssEncoder.HtmlEncode(Request.Params(Left(IDPrefix, 2) & "MobilePhone"), True))
            Else
                Return Common.SafeString(AntiXssEncoder.HtmlEncode(Request.Params(Left(IDPrefix, 2) & "CellPhone"), True))
            End If
        End Get
    End Property

    Protected ReadOnly Property Email() As String
		Get
			Return Common.SafeString(AntiXssEncoder.HtmlEncode(Request.Params(Left(IDPrefix, 2) & "Email"), True))
		End Get
	End Property

	'Public MaritalStatusDropdown As String
	'Public CitizenshipStatusDropdown As String
    Public PreferContactMethodDropdown As String
#End Region
    
	Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
		Dim oParent As CBasePage = TryCast(Me.Page, CBasePage)

        Dim ddlPreferContactMethod As Dictionary(Of String, String) = oParent._CurrentWebsiteConfig.GetEnumItems("PREFERRED_CONTACT_METHODS")
        ddlPreferContactMethod = ddlPreferContactMethod.ToDictionary(Function(kv) kv.Key.NullSafeToUpper_, Function(kv) kv.Value)
        Dim PreferContactMethod As String = Common.RenderDropdownlist(ddlPreferContactMethod, "")
		PreferContactMethodDropdown = PreferContactMethod

	End Sub
End Class
