﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="XSellSelection.ascx.vb" Inherits="Inc_MainApp_XSellSelection" %>
<div id="divXsellSection" style="margin-top: 40px;">
	<%=HeaderUtils.RenderPageTitle(23, "More saving, select an option below to start a pre-qualified application", True)%>
	<div class="xsell-selection-panel" style="margin-top: 20px;">
		<div class="xsell-item" data-role="controlgroup">
			<%--//refinance vehicle--%>
			<%If VlRefinanceGrades IsNot Nothing AndAlso VlRefinanceGrades.Any() Then
					For Each grade As CXSellRefinanceGrade In VlRefinanceGrades
			%>
                <%If grade.MonthlySavings > 0 Then %>
			        <a href="/vl/VehicleLoan.aspx?lenderref=<%=LenderRef%>&type=3&sid=<%=SessionId%>" data-mode="self-handle-event" rel="external" tabindex="0" class="btn-header-theme ui-btn ui-corner-all xsell-item-option">
				        Refinance your <%=grade.CreatorName%> Vehicle Loan
				        <p class="desc"><span class="bold">New Monthly Payment:</span> <%=FormatCurrency(grade.NewMonthlyPayment, 2)%>&nbsp;&nbsp; <span class="bold" style="color:green">Estimated Savings:</span> <em style="font-style: normal;color:red;"><%=FormatCurrency(grade.MonthlySavings, 2)%></em></p>
			        </a>
                <%End If%>
			<%
                    Next
                End If%>

			<%--//refinance with he--%>
			<%If HeRefinanceGrades IsNot Nothing AndAlso HeRefinanceGrades.Any() Then
                    For Each grade As CXSellRefinanceGrade In HeRefinanceGrades
			%>
			<%If grade.MonthlySavings > 0 Then %>
                <a href="/he/HomeEquityLoan.aspx?lenderref=<%=LenderRef%>&type=3&sid=<%=SessionId%>" data-mode="self-handle-event" rel="external" tabindex="0" class="btn-header-theme ui-btn ui-corner-all xsell-item-option">
				    Refinance your current loans with a Home Equity
				    <p class="desc"><span class="bold">Proposed Term(m):</span> <%=grade.AssumedTerm%>&nbsp;&nbsp; <span class="bold">Assumed Amount:</span> <%=FormatCurrency(grade.AssumedAmount, 2)%>&nbsp; &nbsp;<span class="bold">New Monthly Payment:</span> <%=FormatCurrency(grade.NewMonthlyPayment, 2)%>&nbsp;&nbsp; <span class="bold" style="color:green">Estimated Savings:</span> <em style="font-style: normal;color:red;"><%=FormatCurrency(grade.MonthlySavings, 2)%></em></p>
			    </a>
            <%End If %>
			<%
                    Next
                End If%>

			<%--//new credit card--%>
			<%If CreditCardGrades IsNot Nothing AndAlso CreditCardGrades.Any() Then
					For Each grade As CXSellNewGrade In CreditCardGrades
			%>
			<a href="/cc/CreditCard.aspx?lenderref=<%=LenderRef%>&type=3&sid=<%=SessionId%>" data-mode="self-handle-event" rel="external" tabindex="0" class="btn-header-theme ui-btn ui-corner-all xsell-item-option" >
				<%=grade.CreditCardName%>
				<p class="desc"><span class="bold">Rate:</span> <%=FormatNumber(grade.Rate, 2)%>&nbsp;&nbsp;<span class="bold">Credit Limit:</span> <em style="font-style: normal;"><%=FormatCurrency(grade.MaxAmountQualified, 2)%></em></p>
			</a>
			<%
			Next
		End If%>

			<%--new vehicle loan--%>
			<%If VlGrade IsNot Nothing Then %>
				<a href="/vl/VehicleLoan.aspx?lenderref=<%=LenderRef%>&type=3&sid=<%=SessionId%>" data-mode="self-handle-event" rel="external" tabindex="0" class="btn-header-theme ui-btn ui-corner-all xsell-item-option" >
					New Vehicle Loan
					<p class="desc"><span class="bold">APR:</span> <%=FormatNumber(VlGrade.Rate, 2)%>%&nbsp;&nbsp; <span class="bold">Max Qualified Amount:</span> <em style="font-style: normal;"><%=FormatCurrency(VlGrade.MaxAmountQualified, 2)%></em></p>
				</a>
			<%End If%>
		</div>
	</div>
</div>

<script>
    $(function () {
        var divXsellElem = $('#divXsellSection');
        if (divXsellElem.find('a[data-mode="self-handle-event"]').length == 0) { 
            divXsellElem.hide();
        } 
    });
</script>