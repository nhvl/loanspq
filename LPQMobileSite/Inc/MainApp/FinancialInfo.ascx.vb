﻿Imports LPQMobile.Utils

Partial Class Inc_FinancialInfo
    Inherits CBaseUserControl

    Public EmploymentStatusDropdown As String
    Public LiveMonthsDropdown As String
    'add ddlyear
	Public LiveYearsDropdown As String
    Public Property IsBusinessLoan As Boolean = False
    Public Property IsBLJoint As Boolean = False
    Private _collectDescriptionIfEmploymentStatusIsOther As String
	Public Property CollectDescriptionIfEmploymentStatusIsOther As String
		Get
			Return _collectDescriptionIfEmploymentStatusIsOther
		End Get
		Set(value As String)
			_collectDescriptionIfEmploymentStatusIsOther = value
		End Set
	End Property
	Public Property OccupancyStatusesForRequiredHousingPayment As List(Of String) = New List(Of String)({"RENT"})

End Class
