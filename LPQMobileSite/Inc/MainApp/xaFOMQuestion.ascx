﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="xaFOMQuestion.ascx.vb" Inherits="Inc_MainApp_xaFOMQuestion" %>
<%=FormHeader%>
<div id="divShowFOM" style="margin-bottom: 15px">
<div id="divShowFOMMessage"><span id="spShowFOMMessage"></span></div>
<asp:Repeater runat="server" ID='rptFOMQuestions'>
    <ItemTemplate>  
           <%-- <legend></legend>--%>            
            <div tabindex="0" data-role="button" class="btn-header-theme ui-btn ui-corner-all" data-fom-question="true" data-fom-name="<%#Eval("Name")%>" id="fom-question-<%#Container.ItemIndex%>" data-fom-id="fom-question-<%#Container.ItemIndex%>" data-fom-value="<%#Container.ItemIndex%>">
                <%--<%#Container.ItemIndex + 1%>.--%>       
                <%If hasFOM_V2 Then%>
                  <%#Eval("title_V2")%> 
                <%Else%>
                  <%#Eval("title")%> 
                <%End If%>    
               
            </div>  
            <div class="divFomQuestions ">
                <%#GenerateExtraData(Container.DataItem, Container.ItemIndex)%> 
             <%#RenderingNextQuestionFields(Container.DataItem, Container.ItemIndex)%> 
             <%#RenderingInnerNextQuestionFields(Container.DataItem, Container.ItemIndex)%> 
            </div>
        <%#NextQuestionHiddenInputForCheckBoxField(Container.DataItem, Container.ItemIndex)%>
    </ItemTemplate> 
</asp:Repeater> 
</div>
<input type ="hidden" id="hdHasInnerNextQuestion"  value="<%=_hasInnerNextQuestion %>" />
<script type="text/javascript">
    var specialFOMQNames = JSON.parse('<%=_specialFOMQNames%>');
    var nqArray = JSON.parse('<%=_NextQuestionList%>');
    var nqDic = JSON.parse('<%=_strNextQuestionDic%>');
    var innerNQNameArray = JSON.parse('<%=_innerNQNames%>');

      //default hide all next question
   hideNextQuestions();
function AutoSelectItem(id) 
{
    //deactive all other option
	$("#divShowFOM div.btn-header-theme[data-fom-question='true']").removeClass("active");
	//mark coresponding option as active
	$("#divShowFOM div.btn-header-theme[data-fom-question='true'][data-fom-id='"+id+"']").addClass("active");
	handledShowAndHideNextQuestion();
}
function hideNextQuestions() {     
        var FOMQuestionElem = $('#divShowFOM .btn-header-theme');
        FOMQuestionElem.each(function () {
            var idx = $(this).attr("data-fom-value");
            var nqLength = nqArray.length;
            if (nqLength > 0) {
                for (var i = 0; i < nqLength; i++) {
                    var sNQName = nqArray[i].replace(/\s/g, "_");
                    $('#divNextQuestion'+idx + sNQName).hide();
                }
            }
        });
        hideInnerNextQuestion();
}

function resetFom() {
	$("#divShowFOM div.btn-header-theme[data-fom-question='true']").removeClass("active");
	$("#divShowFOM div.btn-header-theme[data-fom-question='true']").removeClass("btnActive_on");
	$("#divShowFOM div.divFomQuestions").addClass("hidden");
}

function handledShowAndHideNextQuestion() {
    var idx = $("#divShowFOM div.btn-header-theme.active[data-fom-question='true']").attr("data-fom-value");
    var nqName = $('#hdNextQuestion' + idx).val();
    var divNextQuestion = '#divNextQuestion'+idx;
    hideNextQuestions();
    if (nqName != undefined && nqName != "") { // seclect custom question 
        nqName = nqName.replace(/\s/g, "_");
        $(divNextQuestion + nqName).show();
        handleShowAndHideInnerNextQuestion(idx, nqName);
    } else { // select custom question answer
        var sElement = document.getElementById('ddl-answer-' + idx);
        var hdNextQuestion = '#hdNextQuestion' + idx + '_';
        if (sElement != undefined) {
            var selectedIndex = sElement.selectedIndex;
            var sNQName = $(hdNextQuestion + selectedIndex).val();          
            if (sNQName != undefined && sNQName != "") {
                sNQName = sNQName.replace(/\s/g, "_");
                $('#divNextQuestion' +idx+ sNQName).show();
                //inner nextQuestion
                handleShowAndHideInnerNextQuestion(idx,sNQName);
            } 
        }
    }
}

function validateNQTextBoxField(nqItem,nqItemText) {
    var strMessage = '';
    var nqItemID = nqItem.attr('id');
    var inputValue = nqItem.val();
    var nqAnswers = $('#hdAnswer_' + nqItemID).val();
    if (nqAnswers != undefined) {
        nqAnswers = JSON.parse(nqAnswers);
        if (nqAnswers.indexOf(inputValue) < 0) { //not found
            strMessage += inputValue + " is not valid.\n";
        }
    } else { //no nqAnswer
        if (nqItemText != undefined && nqItemText.indexOf('Zip Code') != -1) {
            var pattern = /\d{5}/;
            if (!nqItem.val().match(pattern)) {
                strMessage += inputValue+ " is not valid.\n";
            } else {
                if (Number(nqItem.val()) <= 0) {
                    strMessage += inputValue + " is not valid.\n";
                }
            }
        }
    }
    return strMessage;
}

function validateNextQuestionFields(sNQName) {
    var strMessage = "";
    sNQName = sNQName.replace(/\s/g, "_"); 
    var idx = $("#divShowFOM div.btn-header-theme.active[data-fom-question='true']").attr("data-fom-value");
     var divNextQuestion = '#divNextQuestion' +idx+sNQName;
    //var divNextQuestion = '.divNextQuestion' + sNQName;
    var fieldCount = $(divNextQuestion).attr('fieldCount');
        if (fieldCount != undefined && fieldCount != null) {
            fieldCount = Number(fieldCount);
            if (fieldCount > 0) {
                for (var i = 0; i < fieldCount; i++) {
                    var nqIndex = idx + sNQName + "_" + i;
                    var nqItem = $('#NQItem' + nqIndex);
                    var nqItemText = $('#txtItem' + nqIndex).text();
                    var nqItemType = nqItem.attr('type');
                    //validate only two types : text and dropdown
                    if (nqItemType == "text") {
                        strMessage += validateInputTextBoxField(nqItem, nqItemText);
                    } else { //dropdown
                        var ddlValue = nqItem.val();
                        var ddlSelectedText = nqItem.find('option:selected').text();
                        if (!Common.ValidateText(ddlValue) || ddlValue == "0000" || !Common.ValidateText(ddlSelectedText)) {
                            strMessage += nqItemText + " field is not completed.\n";
                        } else {
                            //validate inner next question field if it exist
                            strMessage += validateInnerNextQuestionFields();
                        }
                    }

                }
            }
        }
    return strMessage;
}

function validateNextQuestionAndAnswers(idx) {
    var strMessage = "";
    var nqName = $('#hdNextQuestion' + idx).val(); 
    if (nqName != undefined && nqName != "") { // seclected custom question -->radio button
        strMessage += validateNextQuestionFields(nqName);
    } else { //selected custom question answer -->dropdown answer list
        var sElement = document.getElementById('ddl-answer-' + idx);
        if (sElement != undefined) {
            var sIndex = sElement.selectedIndex;
            var sNQName = $('#hdNextQuestion' + idx + "_" + sIndex).val();
            if (sNQName != undefined && sNQName != "") { //make sure sNQName exist
                strMessage +=validateNextQuestionFields(sNQName);
            }
        }
    }
    return strMessage;
}

//format of the fom question is only one dropdown question, when user select an option
function getNQAnswerFields(arrNextQuestions, arrNQItems, FOMIdx,sNQName){
    sNQName = sNQName.replace(/\s/g, "_");
    var element = $('#divNextQuestion' + FOMIdx + sNQName);
    var cqName = element.attr('name');
    var fieldCount = Number(element.attr('fieldcount'));
    //encode text_template in case it has html tags
    var text_template = htmlEncode(element.attr('text_template')); 
    if (fieldCount > 0) { //make sure a question has at least one field
        arrNextQuestions.push(FOMIdx);
        arrNextQuestions.push(fieldCount);
        arrNextQuestions.push(cqName);
        arrNextQuestions.push(text_template);
        //add type and value of one or multiple fields
        for (var i = 0; i < fieldCount; i++) {
            var nqIndex = FOMIdx + sNQName + "_" + i;
            var nqType = $('#NQItem' + nqIndex).attr('type');
            var nqValue = $('#NQItem' + nqIndex).val();
            arrNQItems.push(nqType);
            arrNQItems.push(nqValue);
        }
    }
}
function BindQuestionEvent() {
    //hide all selected answers of Inactive FOM Questions
    var FOMQuestionElem = $('#divShowFOM .btn-header-theme');
    FOMQuestionElem.each(function () {
        var currElem = $(this);
        if (currElem.next().hasClass('divFomQuestions')) {
            if (!currElem.hasClass('active')) {
                currElem.next().addClass('hidden');
            } else {
                currElem.next().removeClass('hidden');
            }
        }
    });
    //FOMquestions-display next question for current seleted option
    $("#divShowFOM div.btn-header-theme[data-fom-question='true']").click(function() {
        //deactive other option
        $("#divShowFOM div.btn-header-theme[data-fom-question='true']").removeClass("active");
        //mark current option as active
        $(this).addClass("active");
        handledShowAndHideNextQuestion();
    	//$("#divShowFOM").trigger("change");
	    $.lpqValidate.hideValidation("#spShowFOMMessage");
        $('#divShowFOMMessage').css('height', '0');
    });
}
function getFOMSSNAnswer() {
    var FOMQuestionElem = $('#divShowFOM .btn-header-theme');
    var sSSNQuestion = "";
    var ssnAnswer = "";
    FOMQuestionElem.each(function () {
        var currElem = $(this);     
        if (currElem.hasClass('active') && currElem.next().hasClass('divFomQuestions')) {
            var inputElem = currElem.next().find('input[type="text"]');
            inputElem.each(function () {
                var element = $(this);
                var customValidation = element.attr("data-customvalidation");
                if (customValidation != undefined && customValidation != "") {
                    var Obj = JSON.parse(customValidation);
                    for (var key in Obj) {
                        if (key == "SSN") {
                            ssnAnswer=element.val();
                            break;
                        }
                    }
                }
                if (ssnAnswer !="") {
                    return false; //exit inputElem loop
                }
            });

            if (ssnAnswer != "") {
                sSSNQuestion = inputElem.val() + "|" + ssnAnswer; 
                return false; //exit FOMQuestion Element loop
            }
        }   
    });
    return sSSNQuestion;
}

function validateFOM() {
    var strMessage = '';
    var numberFOM = $("#divShowFOM div.btn-header-theme[data-fom-question='true']").length;
    var idx = $("#divShowFOM div.btn-header-theme.active[data-fom-question='true']").attr("data-fom-value");
    var sQuestionText = $("#divShowFOM div.btn-header-theme.active[data-fom-question='true']").text();
    // loop each answer
    var isOk = true;
    if (numberFOM > 0) {
        //all FOM Questions are hidden then skip validation
        if (isHiddenFOMQuestions()) {
            return ""; //skip validating
        }

        // no question selected
        //var numSelected = $('input[type="radio"][name="fom-question"]:checked').length;
        var numSelected = $("#divShowFOM div.btn-header-theme.active[data-fom-question='true']").length;
        if (numSelected == 0) {
            return "Please select a Member Eligibility option.\n";
        }
        $('.fom-answer-' + idx + ':not(span)').each(function () {
            var currAnswer = $(this).val();
            currAnswer = typeof currAnswer !== 'undefined' ? currAnswer : "";
            currAnswer = currAnswer !== null ? currAnswer : "";

            //validate special case lenderref Nasafcu
            if (sQuestionText != undefined && sQuestionText.toUpperCase().indexOf("OF A NASA FEDERAL ASSOCIATION") > -1) {
                if (currAnswer != "") {
                    isOk = true;
                    return false; //break for loop
                } 
            } // end validate special case lenderref Nasafcu

            if (currAnswer.trim() == '')
                isOk = false;
        });
        if (!isOk) { //already selected the option, but all the fields of this option is not completed
            return "The selected Member Eligibility option is not completed.\n";
        }
        //validate next question fields
        strMessage = validateNextQuestionAndAnswers(idx);

    }

    return strMessage;
}

function setFOM(appInfo) {	
	appInfo.FOMIdx = -1;
	appInfo.FOMName = "";
	var FOMQuestionsAndAnswers = [];
	var arrFOMAnswers = [];
    //make sure it hasFomPage
	var selectedFomQuestion = $("#divShowFOM div.btn-header-theme.active[data-fom-question='true']");
	if (selectedFomQuestion.length === 1) {
	    var FOMIdx = selectedFomQuestion.attr("data-fom-value");
	    var FOMName = selectedFomQuestion.attr("data-fom-name");
	    var FOMAnswerClass = '.fom-answer-' + FOMIdx;
	    $(FOMAnswerClass + ":not(span" + FOMAnswerClass + ")").each(function () {//exclude the FOMAnswerclass inside span element
	        arrFOMAnswers.push($(this).val());
	    });
	    appInfo.FOMIdx = FOMIdx;
	    appInfo.FOMName = FOMName;
	    //add question name and answers to the FOMQuestionsAndAnswers
	    FOMQuestionsAndAnswers.push(new FOMQuestionAnswers(FOMName, arrFOMAnswers));
	    var FOMNQA = getFOMNextQuestionAnswers(FOMIdx);
	    var FOMInnerNQA = getFOMInnerNextQuestionAnswers(FOMIdx);
	    //add next question and answers if it exist
	    if (!$.isEmptyObject(FOMNQA)) {
	        FOMQuestionsAndAnswers.push(FOMNQA);
        }    
	    //add inner next question and answers if it exist
        if (!$.isEmptyObject(FOMInnerNQA)) {     
	        FOMQuestionsAndAnswers.push(FOMInnerNQA);
	    }
	}
	appInfo.FOMQuestionsAndAnswers = JSON.stringify(FOMQuestionsAndAnswers);
    appInfo.ssnFOMAnswer = getFOMSSNAnswer();
}

//--------- end inner next question for level 3 -----------------
function hasInnerNextQuestion() {
    return $('#hdHasInnerNextQuestion').val() == "Y";
}
function hideInnerNextQuestion() {
    //hide inner next questions
    if (hasInnerNextQuestion()) {     
        for (var key in nqDic) {
            var cqIndex = key.substring(0, key.indexOf("_"));
            for (var i = 0; i < innerNQNameArray.length; i++) {
                var nqPrefix = cqIndex + nqDic[key].replace(/\s/g,"_") + "_" + innerNQNameArray[i];             
                $('#divNextQuestion' + nqPrefix).removeClass("NQActive");
                $('#divNextQuestion' + nqPrefix).hide();
            }
        }
    }
}
function handleShowAndHideInnerNextQuestion(idx, sNQName) {
    var element = 'divNextQuestion' + idx + sNQName;
    //display inner next question   
    var selectElement = element + ' select.nq-fom-answer-' + idx;
    var fieldCount = Number($('#' + element).attr('fieldcount'));
    $('#' + selectElement).on('change', function () {   
	    hideInnerNextQuestion();
        selectedIndex = $(this)[0].selectedIndex;
        if (fieldCount > 0) {
            for (var i = 0; i < fieldCount; i++) {
                var hdNQNameElem = $('#hdInnerNQ' + idx + sNQName + "_" + i + "index" + selectedIndex);
                if (hdNQNameElem.length > 0 && hdNQNameElem.val() != "") {
                    $('#' + element + "_" + hdNQNameElem.val().replace(/\s/g,"_")).show();
                    $('#' + element + "_" + hdNQNameElem.val().replace(/\s/g,"_")).addClass("NQActive");
                }
            }
        }
    });
    //case: next question only have one option
    if ($('#' + selectElement).find('option').length == 1) {
        hideInnerNextQuestion();
        if (fieldCount > 0) {
            for (var i = 0; i < fieldCount; i++) {
                var hdNQNameElem = $('#hdInnerNQ' + idx + sNQName + "_" + i + "index0");
                if (hdNQNameElem.length > 0 && hdNQNameElem.val() != "") {
                    $('#' + element + "_" + hdNQNameElem.val().replace(/\s/g,"_")).show();
                    $('#' + element + "_" + hdNQNameElem.val().replace(/\s/g,"_")).addClass("NQActive");
                }
            }
        }
    }
}

function validateInnerNextQuestionFields() {
    var strMessage = "";
    //validate inner next Question if it exist
    if (hasInnerNextQuestion()) {
        var innerNQElem = $('#divShowFOM .NQActive');
        if (innerNQElem.length > 0) {
            var fieldCount = innerNQElem.attr('fieldCount');
            var nqIdPrefix = innerNQElem.attr('id').replace("divNextQuestion", "");     
            if (fieldCount != undefined && fieldCount != null) {
                fieldCount = Number(fieldCount);
                if (fieldCount > 0) {
                    for (var i = 0; i < fieldCount; i++) {                 
                       var nqNamePrefix = "_" + innerNQElem.attr('name');
                       var nqIndex = nqIdPrefix.replace(nqNamePrefix,"_"+ i + nqNamePrefix);
                        var nqItem = $('#NQItem' + nqIndex);
                        var nqItemText = $('#txtItem' + nqIndex).text();
                        var nqItemType = nqItem.attr('type');
                        //validate only two types : text and dropdown
                        if (nqItemType == "text") {
                            strMessage += validateInputTextBoxField(nqItem, nqItemText);
                        } else {
                            var ddlValue = nqItem.val();
                            var ddlSelectedText = nqItem.find('option:selected').text();                    
                            if (!Common.ValidateText(ddlValue) || ddlValue == "0000" || !Common.ValidateText(ddlSelectedText)) {
                                strMessage += nqItemText + " field is not completed.";
                            }
                        }
                    }
                }
            }
        }
    }

    return strMessage;
}

//create object for FOM question answers
function FOMQuestionAnswers(FOMName, FOMAnswers) {
    this.FOMName = FOMName;
    this.FOMAnswers = JSON.stringify(FOMAnswers);
}

//setNextQuestionAndAnswer
function getFOMNextQuestionAnswers(FOMIdx) {
    var FOMNQObj = {};
    var FOMAnswers = [];
    //case: user only selected the membership option which contains next question
    var sNQName = $('#hdNextQuestion' + FOMIdx).val();
    if (sNQName != undefined && sNQName != "") {
        FOMAnswers = getFOMNQAnswers(FOMIdx, sNQName);
        FOMNQObj = new FOMQuestionAnswers(sNQName, FOMAnswers);
    } else { //selected custom question answer -->dropdown answer list
        var sElement = document.getElementById('ddl-answer-' + FOMIdx);
        if (sElement != undefined) {
            var sIndex = sElement.selectedIndex;
            var sNQName = $('#hdNextQuestion' + FOMIdx + "_" + sIndex).val();
            if (sNQName != undefined && sNQName != "") { //make sure sNQName exist
                FOMAnswers = getFOMNQAnswers(FOMIdx, sNQName);
                FOMNQObj = new FOMQuestionAnswers(sNQName, FOMAnswers);
            }
        }
    }  
    return FOMNQObj;
}

//add all answers to the array
function getFOMNQAnswers(FOMIdx, sNQName) {
    var FOMAnswers = [];
    sNQName = sNQName.replace(/\s/g, "_");
    var fieldCount = Number($('#divNextQuestion' + FOMIdx + sNQName).attr('fieldcount'));
    if (fieldCount > 0) { //make sure a question has at least one field
        for (var i = 0; i < fieldCount; i++) {
            var nqValue = $('#NQItem' + FOMIdx + sNQName + "_" + i).val();
            FOMAnswers.push(nqValue);
        }
    }
    return FOMAnswers;
}
function getFOMInnerNextQuestionAnswers(FOMIdx) {
    var FOMInnerNQ = {};
    var FOMAnswers = [];
    if (hasInnerNextQuestion()) {
        var innerNQElem = $('#divShowFOM .NQActive');
        if (innerNQElem.length > 0) {
            var fieldCount = Number(innerNQElem.attr('fieldCount'));
            var nqIdPrefix = innerNQElem.attr('id').replace("divNextQuestion", "");         
            if (fieldCount > 0) {   
                //add type and value of one or multiple fields
                for (var i = 0; i < fieldCount; i++) {
                    var nqNamePrefix = "_" + innerNQElem.attr('name');
                    var nqIndex = nqIdPrefix.replace(nqNamePrefix, "_" + i + nqNamePrefix);         
                    var nqValue = $('#NQItem' + nqIndex).val();          
                    FOMAnswers.push(nqValue);
                }
            }
            FOMInnerNQ = new FOMQuestionAnswers(innerNQElem.attr('name'), FOMAnswers);        
        }
    }
    return FOMInnerNQ;
}

function validateInputTextBoxField(nqItem, nqItemText) {
    var strMessage = "";
    if (!Common.ValidateText(nqItem.val())) {
        if (nqItemText.indexOf("Zip Code") != -1) {
            strMessage += "Zip Code field is not completed. \n";
        } else {
            strMessage += nqItemText + " field is not completed. \n";
        }
    } else {
        if (nqItemText.indexOf("Social Security") != -1) {
            var pattern = /\d{9}/;
            if (!nqItem.val().match(pattern)) {
                strMessage += "Social security number is not valid.\n";
            } else {
                if (Number(nqItem.val()) <= 0) {
                    strMessage += "Social security number is not valid.\n";
                }
            }
        }
        strMessage += validateNQTextBoxField(nqItem, nqItemText);
    }

    return strMessage;
}

//--------- end inner next question for level 3 -----------------

    // add maxlength for Zipcode and SSN (in case the fom has zipcode and ssn)

function handledShowAndHideFOMQuestions() {
    var FOMElem = $("#divShowFOM div.btn-header-theme[data-fom-question='true']");
    var sMinorAccountType = "";
    var isMinor = false;
    if (typeof getMinorAccountTypeObj !== "undefined") {
        sMinorAccountType = getMinorAccountTypeObj().mAccountType;
        isMinor= getMinorAccountTypeObj().isMinor=="MINOR";
    }
    resetFom();
    if (isMinor){ //display fom questions for minor accounts
        FOMElem.each(function () {
            var currElem = $(this);
            var sFOMQNames = getSpecialFOMQNames(currElem.attr('data-fom-name'));
            if ((sFOMQNames.AccountTypes.length > 0 && $.inArray(sMinorAccountType, sFOMQNames.AccountTypes) > -1) || (sFOMQNames.AccountTypes.length == 0 && sFOMQNames.QuestionName != "")) {
                currElem.removeClass('hidden');
            } else {
                currElem.addClass('hidden');
            }
        });
    }
    //show and hide Eligibility title
    if (isHiddenFOMQuestions()) {
        $('#divShowFOM').prev("div").hide();
    } else {
        $('#divShowFOM').prev("div").show();
    }
}

function getSpecialFOMQNames(FOMQName) {
    var sObj = {
        QuestionName: "",
        AccountTypes: []
    };
    for (var key in specialFOMQNames) {
        if (key == FOMQName) {
            sObj.QuestionName = key;
            sObj.AccountTypes = specialFOMQNames[key];
            break;
        }
    }
    return sObj;
}
function isHiddenFOMQuestions() {
    var FOMElem = $("#divShowFOM div.btn-header-theme[data-fom-question='true']");
    var isHidden = true;
    FOMElem.each(function () {
        var currElem = $(this);
        if (!currElem.hasClass('hidden')) {
            isHidden = false;
            return false;
        }
    });
    return isHidden;
}
$(function () {
    handledShowAndHideFOMQuestions();
    //Handle FOM QUESTIONS for personal and special
    //dropdown minor account type
    $('#btnSelectionContainer').on('change', 'select#ddlSpecialAccountType', function () {
        handledShowAndHideFOMQuestions();
    });
    //button minor account type
    $('#btnSelectionContainer').on('click', 'a#ddlSpecialAccountType', function () {
        handledShowAndHideFOMQuestions();
    });
    $('.NextQuestion').click(function () {
        //get number of items of each question
        var currElement = $(this);
        var idx = $("#divShowFOM div.btn-header-theme.active[data-fom-question='true']").attr("data-fom-value");
        var sNQName = currElement.attr('id').replace("divNextQuestion" + idx, "");
        var fieldCount = Number(currElement.attr('fieldcount'));
       
        //make sure question has at least one field
        if (fieldCount > 0) {
            for (var i = 0; i<fieldCount; i++) {
                var nqIndex = idx + sNQName + "_" + i;
                var nqItem = $('#NQItem' + nqIndex);
                var nqItemText = $('#txtItem' + nqIndex).text();
                var nqItemType = nqItem.attr('type');             
                if (nqItemType == "text") {
                    if (nqItemText.indexOf( "Social Security") != -1) { //max length for ssn
                        nqItem.attr("maxlength", "9");
                    } else if (nqItemText.indexOf("Zip Code") != -1) {
                        nqItem.attr("maxlength", "5"); //for zipcode    
                    } else {
                        nqItem.attr("maxlength", "100"); // for text
                    }
                }
            }
        }
        $("#divShowFOM").trigger("change");
    });
    BindQuestionEvent();
    $(document).on('pageshow', function() {
        var pageId = $.mobile.pageContainer.pagecontainer('getActivePage').attr('id');
        if (pageId == "GettingStarted" || pageId == "pagesubmit") { //utilize FOM Question on pagesubmit of CC module when comboMode = Y
            BindQuestionEvent();
        }
    });
    //remove horizontal line from download
    $('.divFomQuestions hr').remove();

    //handle selected FOMQuestion button
    $('#divShowFOM').on('click', function () {
        var FOMQuestionElem = $('#divShowFOM .btn-header-theme');
        FOMQuestionElem.each(function () {
            var currElem = $(this);
            if (!currElem.hasClass('active')) {
                currElem.removeClass('btnActive_on');
                if (currElem.next().hasClass('divFomQuestions')) {
                    currElem.next().addClass('hidden');
                    //reset selected value to default value of all unselected questions
                    //clear all input fields(exclude hidden input), and set default value to the first option for all select fields                
                    currElem.next().find('input').each(function () {
                        if ($(this).attr('type') !== 'hidden'){
                            $(this).val("");   
                        }      
                    });
                    currElem.next().find('select').each(function () {                     
                        $(this).val($(this).find('option:first').val());
                        $(this).selectmenu().selectmenu('refresh');
                    });
                }
                            
            } else {//display selected question
                if (currElem.next().hasClass('hidden')) {
                    currElem.next().removeClass('hidden');
                }
            }
            
        });
    });
    $('#divShowFOM').observer({
        validators: [
            function (partial) {
                if (validateFOM() != '') {
                    $('#divShowFOMMessage').css('height', '35px');
                } else {
                    $('#divShowFOMMessage').css('height', '0');
                }

                return validateFOM().replace(/\*/g, "");
            }
        ],
        validateOnBlur: false,
        group: "ValidateFOM",
        groupType: "complexUI",
        placeHolder: "#spShowFOMMessage"
    });
});

</script>