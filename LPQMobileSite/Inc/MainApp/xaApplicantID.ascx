﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="xaApplicantID.ascx.vb"
    Inherits="Inc_MainApp_xaApplicantID" %>
<%@ Import Namespace="LPQMobile.Utils" %>

<div id="<%=IDPrefix%>divApplicantIdentification" <%=IIf(IsInMode("777") AndAlso IsInFeature("visibility") AndAlso Not isBusinessLoan, "class=""showfield-section"" data-show-field-section-id=""" & BuildShowFieldSectionID("divApplicantIdentification") & """ " & IIf(((LoanType = "CC" Or LoanType = "HE" Or LoanType = "PL" Or LoanType = "VL") AndAlso Not isComboMode) Or IDPrefix = "m_", "data-default-state=""off""", IIf(ShowFieldDefaultStateOn, "", "data-default-state=""off""")) & " data-section-name= '" & IIf(String.IsNullOrEmpty(IDPrefix), "", GetApmVisibilitySectionNamePrefix()) & " Applicant Identification'", "")%>>
	<%=Header%>
<%--<%=HeaderUtils.RenderPageTitle(16, IIf(String.IsNullOrEmpty(Title), "Identification", Title), True)%>--%>
<div data-role="fieldcontain" onload="refreshIDList();" >
	<%--minor id card type is not required --> remove required red star of minor id--%>
    <label for="<%=IDPrefix%>ddlIDCardType" <%=IIf(IsRequired, "class='RequiredIcon'", "")%>>ID Type</label>
    <select name="<%=IDPrefix%>ddlIDCardType" id="<%=IDPrefix%>ddlIDCardType" onchange="<%=IDPrefix%>refreshIDList();">
	    <% If Not isExistIDCardType Then%>
	    <option value="DRIVERS_LICENSE">Driver's License</option>
        <option value="PASSPORT">Passport</option>
        <option value="MILITARY_ID">Military ID</option>
	    <option value="ID_CARD">State ID</option>
	    <% Else%>
	   	    <%=IDCardTypeList%>
	    <%End If%>
    </select>
</div>
<div data-role="fieldcontain">
    <label for="<%=IDPrefix%>txtIDCardNumber" <%=IIf(IDPrefix <> "m_", "id='" + IDPrefix + "lblIDCardNumber" + "'", "")%>>ID Number</label>
    <input type="text" name="<%=IDPrefix%>txtIDCardNumber" id="<%=IDPrefix%>txtIDCardNumber" value="<%=IDCardNumber%>" maxlength ="50" />
</div>
<input type="hidden" id="<%=IDPrefix%>hfbooleanStateNeeded" />
<div id="<%=IDPrefix%>divState">
    <div data-role="fieldcontain">
        <label for="<%=IDPrefix%>ddlIDState" id="<%=IDPrefix%>lblIDState">ID State</label>
        <select name="<%=IDPrefix%>ddlIDState" id="<%=IDPrefix%>ddlIDState" onchange="<%=IDPrefix%>refreshIDList();">
	        <%=StateDropdown%>            
        </select>
    </div>
</div>
<div id="<%=IDPrefix%>divCountry" style="display: none">
    <div data-role="fieldcontain">
        <label for="<%=IDPrefix%>ddlIDCountry" id="<%=IDPrefix%>lblIDCountry">ID Country</label>
        <select name="<%=IDPrefix%>ddlIDCountry" id="<%=IDPrefix%>ddlIDCountry">
            <option value="">--Please Select--</option>
            <option value="USA">USA</option>
            <option value="FRANCE">France</option>
            <option value="GERMANY">Germany</option>
            <option value="ITALY">Italy</option>
            <option value="SPAIN">Spain</option>
            <option value="UNITED KINGDOM">United Kingdom</option>
            <option value="AALAND">Aaland</option>
            <option value="ABKHAZIA">Abkhazia</option>
            <option value="AFGHANISTAN">Afghanistan</option>
            <option value="ALBANIA">Albania</option>
            <option value="ALGERIA">Algeria</option>
            <option value="AMERICAN SAMOA">American Samoa</option>
            <option value="ANDORRA">Andorra</option>
            <option value="ANGOLA">Angola</option>
            <option value="ANGUILLA">Anguilla</option>
            <option value="ANTARCTICA">Antarctica</option>
            <option value="ANTIGUA_BARBUDA">Antigua And Barbuda</option>
            <option value="ARGENTINA">Argentina</option>
            <option value="ARMENIA">Armenia</option>
            <option value="ARUBA">Aruba</option>
            <option value="AUSTRALIA">Australia</option>
            <option value="AUSTRIA">Austria</option>
            <option value="AZERBAIJAN">Azerbaijan</option>
            <option value="BAHAMAS">Bahamas</option>
            <option value="BAHRAIN">Bahrain</option>
            <option value="BANGLADESH">Bangladesh</option>
            <option value="BARBADOS">Barbados</option>
            <option value="BELARUS">Belarus</option>
            <option value="BELGIUM">Belgium</option>
            <option value="BELIZE">Belize</option>
            <option value="BENIN">Benin</option>
            <option value="BERMUDA">Bermuda</option>
            <option value="BHUTAN">Bhutan</option>
            <option value="BOLIVIA">Bolivia</option>
            <option value="BOSNIA_HERZEGOVINA">Bosnia And Herzegovina</option>
            <option value="BOTSWANA">Botswana</option>
            <option value="BOUVET ISLAND">Bouvet Island</option>
            <option value="BRAZIL">Brazil</option>
            <option value="BRITISH INDIAN OCEAN">British Indian Ocean Territory</option>
            <option value="BRITISH_VIRGIN_ILND">British Virgin Islands</option>
            <option value="BRUNEI">Brunei</option>
            <option value="BULGARIA">Bulgaria</option>
            <option value="BURKINA FASO">Burkina Faso</option>
            <option value="BURUNDI">Burundi</option>
            <option value="CAMBODIA">Cambodia</option>
            <option value="CAMEROON">Cameroon</option>
            <option value="CANADA">Canada</option>
            <option value="CAPE VERDE">Cape Verde</option>
            <option value="CAYMAN ISLANDS">Cayman Islands</option>
            <option value="CENTRAL AFRICAN REP">Central African Republic</option>
            <option value="CHAD">Chad</option>
            <option value="CHILE">Chile</option>
            <option value="CHINA">China</option>
            <option value="CHRISTMAS ISLAND">Christmas Island</option>
            <option value="COCOS ISLANDS">Cocos Islands</option>
            <option value="COLOMBIA">Colombia</option>
            <option value="COMOROS">Comoros</option>
            <option value="COOK ISLANDS">Cook Islands</option>
            <option value="COSTA RICA">Costa Rica</option>
            <option value="COTE D&#39;IVOIRE">Cote D&#39;Ivoire</option>
            <option value="CROATIA">Croatia</option>
            <option value="CUBA">Cuba</option>
            <option value="CYPRUS">Cyprus</option>
            <option value="CZECH REPUBLIC">Czech Republic</option>
            <option value="DEMOCRATIC_REP_CONGO">Democratic Republic Of The Congo</option>
            <option value="DENMARK">Denmark</option>
            <option value="DJIBOUTI">Djibouti</option>
            <option value="DOMINICA">Dominica</option>
            <option value="DOMINICAN REPUBLIC">Dominican Republic</option>
            <option value="TIMOR-LESTE">East Timor</option>
            <option value="ECUADOR">Ecuador</option>
            <option value="EGYPT">Egypt</option>
            <option value="EL SALVADOR">El Salvador</option>
            <option value="EQUATORIAL GUINEA">Equatorial Guinea</option>
            <option value="ERITREA">Eritrea</option>
            <option value="ESTONIA">Estonia</option>
            <option value="ETHIOPIA">Ethiopia</option>
            <option value="FALKLAND ISLANDS">Falkland Islands</option>
            <option value="FAROE ISLANDS">Faroe Islands</option>
            <option value="FIJI">Fiji</option>
            <option value="FINLAND">Finland</option>
            <option value="FRENCH GUIANA">French Guiana</option>
            <option value="FRENCH POLYNESIA">French Polynesia</option>
            <option value="FRENCH_SOUTHERN_TERR">French Southern Territories</option>
            <option value="GABON">Gabon</option>
            <option value="GAMBIA">Gambia</option>
            <option value="GEORGIA">Georgia</option>
            <option value="GHANA">Ghana</option>
            <option value="GIBRALTAR">Gibraltar</option>
            <option value="GREECE">Greece</option>
            <option value="GREENLAND">Greenland</option>
            <option value="GRENADA">Grenada</option>
            <option value="GUADELOUPE">Guadeloupe</option>
            <option value="GUAM">Guam</option>
            <option value="GUATEMALA">Guatemala</option>
            <option value="GUERNSEY">Guernsey</option>
            <option value="GUINEA">Guinea</option>
            <option value="GUINEA BISSAU">Guinea Bissau</option>
            <option value="GUYANA">Guyana</option>
            <option value="HAITI">Haiti</option>
            <option value="HEARD_MCDONALD_ILND">Heard Island And Mcdonald Islands</option>
            <option value="HONDURAS">Honduras</option>
            <option value="HONG KONG">Hong Kong</option>
            <option value="HUNGARY">Hungary</option>
            <option value="ICELAND">Iceland</option>
            <option value="INDIA">India</option>
            <option value="INDONESIA">Indonesia</option>
            <option value="IRAN">Iran</option>
            <option value="IRAQ">Iraq</option>
            <option value="IRELAND">Ireland</option>
            <option value="ISLE OF MAN">Isle Of Man</option>
            <option value="ISRAEL">Israel</option>
            <option value="JAMAICA">Jamaica</option>
            <option value="JAPAN">Japan</option>
            <option value="JERSEY">Jersey</option>
            <option value="JORDAN">Jordan</option>
            <option value="KAZAKHSTAN">Kazakhstan</option>
            <option value="KENYA">Kenya</option>
            <option value="KIRIBATI">Kiribati</option>
            <option value="KOSOVO">Kosovo</option>
            <option value="KUWAIT">Kuwait</option>
            <option value="KYRGYZSTAN">Kyrgyzstan</option>
            <option value="LAOS">Laos</option>
            <option value="LATVIA">Latvia</option>
            <option value="LEBANON">Lebanon</option>
            <option value="LESOTHO">Lesotho</option>
            <option value="LIBERIA">Liberia</option>
            <option value="LIBYA">Libya</option>
            <option value="LIECHTENSTEIN">Liechtenstein</option>
            <option value="LITHUANIA">Lithuania</option>
            <option value="LUXEMBOURG">Luxembourg</option>
            <option value="MACAU">Macau</option>
            <option value="MACEDONIA">Macedonia</option>
            <option value="MADAGASCAR">Madagascar</option>
            <option value="MALAWI">Malawi</option>
            <option value="MALAYSIA">Malaysia</option>
            <option value="MALDIVES">Maldives</option>
            <option value="MALI">Mali</option>
            <option value="MALTA">Malta</option>
            <option value="MARSHALL ISLANDS">Marshall Islands</option>
            <option value="MARTINIQUE">Martinique</option>
            <option value="MAURITANIA">Mauritania</option>
            <option value="MAURITIUS">Mauritius</option>
            <option value="MAYOTTE">Mayotte</option>
            <option value="MEXICO">Mexico</option>
            <option value="MICRONESIA">Micronesia</option>
            <option value="MOLDOVA">Moldova</option>
            <option value="MONACO">Monaco</option>
            <option value="MONGOLIA">Mongolia</option>
            <option value="MONTENEGRO">Montenegro</option>
            <option value="MONTSERRAT">Montserrat</option>
            <option value="MOROCCO">Morocco</option>
            <option value="MOZAMBIQUE">Mozambique</option>
            <option value="MYANMAR">Myanmar</option>
            <option value="NAGORNO-KARABAKH">Nagorno-Karabakh</option>
            <option value="NAMIBIA">Namibia</option>
            <option value="NAURU">Nauru</option>
            <option value="NEPAL">Nepal</option>
            <option value="NETHERLANDS">Netherlands</option>
            <option value="NETHERLANDS ANTILLES">Netherlands Antilles</option>
            <option value="NEW CALEDONIA">New Caledonia</option>
            <option value="NEW ZEALAND">New Zealand</option>
            <option value="NICARAGUA">Nicaragua</option>
            <option value="NIGER">Niger</option>
            <option value="NIGERIA">Nigeria</option>
            <option value="NIUE">Niue</option>
            <option value="NORFOLK ISLAND">Norfolk Island</option>
            <option value="NORTH KOREA">North Korea</option>
            <option value="NORTHERN CYPRUS">Northern Cyprus</option>
            <option value="NORTHERN_MARIANA_ILN">Northern Mariana Islands</option>
            <option value="NORWAY">Norway</option>
            <option value="OMAN">Oman</option>
            <option value="PAKISTAN">Pakistan</option>
            <option value="PALAU">Palau</option>
            <option value="PALESTINE">Palestine</option>
            <option value="PANAMA">Panama</option>
            <option value="PAPUA NEW GUINEA">Papua New Guinea</option>
            <option value="PARAGUAY">Paraguay</option>
            <option value="PERU">Peru</option>
            <option value="PHILIPPINES">Philippines</option>
            <option value="PITCAIRN">Pitcairn</option>
            <option value="POLAND">Poland</option>
            <option value="PORTUGAL">Portugal</option>
            <option value="PUERTO RICO">Puerto Rico</option>
            <option value="QATAR">Qatar</option>
            <option value="CONGO">Republic Of The Congo</option>
            <option value="REUNION">Reunion</option>
            <option value="ROMANIA">Romania</option>
            <option value="RUSSIA">Russia</option>
            <option value="RWANDA">Rwanda</option>
            <option value="SAINT BARTHELEMY">Saint Barthelemy</option>
            <option value="SAINT HELENA">Saint Helena</option>
            <option value="SAINT_KITTS_NEVIS">Saint Kitts And Nevis</option>
            <option value="SAINT LUCIA">Saint Lucia</option>
            <option value="SAINT_VINCENT_GREN">Saint Vincent And The Grenadines</option>
            <option value="SAINT-MARTIN">Saint-Martin</option>
            <option value="SAINTPIERRE_MIQUELON">Saint-Pierre And Miquelon</option>
            <option value="SAMOA">Samoa</option>
            <option value="SAN MARINO">San Marino</option>
            <option value="SAO_TOME_PRINCIPE">Sao Tome And Principe</option>
            <option value="SAUDI ARABIA">Saudi Arabia</option>
            <option value="SENEGAL">Senegal</option>
            <option value="SERBIA">Serbia</option>
            <option value="SEYCHELLES">Seychelles</option>
            <option value="SIERRA LEONE">Sierra Leone</option>
            <option value="SINGAPORE">Singapore</option>
            <option value="SLOVAKIA">Slovakia</option>
            <option value="SLOVENIA">Slovenia</option>
            <option value="SOLOMON ISLANDS">Solomon Islands</option>
            <option value="SOMALIA">Somalia</option>
            <option value="SOMALILAND">Somaliland</option>
            <option value="SOUTH AFRICA">South Africa</option>
            <option value="S_GEORGIA_SANDWICH">South Georgia And The South Sandwich Islands</option>
            <option value="SOUTH KOREA">South Korea</option>
            <option value="SOUTH OSSETIA">South Ossetia</option>
            <option value="SRI LANKA">Sri Lanka</option>
            <option value="SUDAN">Sudan</option>
            <option value="SURINAME">Suriname</option>
            <option value="SVALBARD_JAN_MAYEN">Svalbard And Jan Mayen</option>
            <option value="SWAZILAND">Swaziland</option>
            <option value="SWEDEN">Sweden</option>
            <option value="SWITZERLAND">Switzerland</option>
            <option value="SYRIA">Syria</option>
            <option value="TAIWAN">Taiwan</option>
            <option value="TAJIKISTAN">Tajikistan</option>
            <option value="TANZANIA">Tanzania</option>
            <option value="THAILAND">Thailand</option>
            <option value="TOGO">Togo</option>
            <option value="TOKELAU">Tokelau</option>
            <option value="TONGA">Tonga</option>
            <option value="TRANSNISTRIA">Transnistria</option>
            <option value="TRINIDAD AND TOBAGO">Trinidad And Tobago</option>
            <option value="TRISTAN DA CUNHA">Tristan Da Cunha</option>
            <option value="TUNISIA">Tunisia</option>
            <option value="TURKEY">Turkey</option>
            <option value="TURKMENISTAN">Turkmenistan</option>
            <option value="TURKS_CAICOS_ISLANDS">Turks And Caicos Islands</option>
            <option value="TUVALU">Tuvalu</option>
            <option value="UGANDA">Uganda</option>
            <option value="UKRAINE">Ukraine</option>
            <option value="UNITED ARAB EMIRATES">United Arab Emirates</option>
            <option value="US_MINOR_ISLANDS">United States Minor Outlying Islands</option>
            <option value="US_VIRGIN_ISLANDS">United States Virgin Islands</option>
            <option value="URUGUAY">Uruguay</option>
            <option value="UZBEKISTAN">Uzbekistan</option>
            <option value="VANUATU">Vanuatu</option>
            <option value="VATICAN CITY">Vatican City</option>
            <option value="VENEZUELA">Venezuela</option>
            <option value="VIETNAM">Vietnam</option>
            <option value="WALLIS AND FUTUNA">Wallis And Futuna</option>
            <option value="WESTERN SAHARA">Western Sahara</option>
            <option value="YEMEN">Yemen</option>
            <option value="ZAMBIA">Zambia</option>
            <option value="ZIMBABWE">Zimbabwe</option>
        </select>
    </div>
</div>
<div data-role="fieldcontain">
    <div class="row">
		<div class="col-xs-8 no-padding"><label for="<%=IDPrefix%>txtIDDateIssued" id="<%=IDPrefix%>lblIDDateIssued" class="dob-label">ID Date Issued</label></div>
		<div class="col-xs-4 no-padding"><input type="hidden" id="<%=IDPrefix%>txtIDDateIssued" class="combine-field-value" value="<%=IDIssuedDate%>"/></div>
	</div>
    <%--<input type="<%=TextAndroidTel%>" pattern="[0-9]*" id="<%=IDPrefix%>txtIDDateIssued" class="indate" value="" maxlength = '10'/>--%>
    <div id="<%=IDPrefix%>divIDDateIssued" class="ui-input-date id-date-issued row">
        <div class="col-xs-4">
            <input aria-labelledby="<%=IDPrefix%>lblIDDateIssued" type="<%=TextAndroidTel%>" pattern="[0-9]*" placeholder="mm" id="<%=IDPrefix%>txtIDDateIssued1" maxlength ='2' onkeydown="limitToNumeric(event);" onkeyup="onKeyUp(event, '#<%=IDPrefix%>txtIDDateIssued1','#<%=IDPrefix%>txtIDDateIssued2', '2');" value="<%=IDIssuedMonth%>"/>
        </div>
        <div class="col-xs-4">
            <input aria-labelledby="<%=IDPrefix%>lblIDDateIssued" type="<%=TextAndroidTel%>" pattern="[0-9]*" placeholder="dd" id="<%=IDPrefix%>txtIDDateIssued2" maxlength ='2' onkeydown="limitToNumeric(event);" onkeyup="onKeyUp(event, '#<%=IDPrefix%>txtIDDateIssued2','#<%=IDPrefix%>txtIDDateIssued3', '2');"  value="<%=IDIssuedDay%>"/>
        </div>
        <div class="col-xs-4" style="padding-right: 0px;">
            <input aria-labelledby="<%=IDPrefix%>lblIDDateIssued" type="<%=TextAndroidTel%>" pattern="[0-9]*" placeholder="yyyy" id="<%=IDPrefix%>txtIDDateIssued3" maxlength ='4' onkeydown="limitToNumeric(event);" value="<%=IDIssuedYear  %>"/>
        </div>

    </div>
</div>
<div data-role="fieldcontain">
	<div class="row">
		<div class="col-xs-8 no-padding"><label for="<%=IDPrefix%>txtIDDateExpire" id="<%=IDPrefix%>lblIDDateExpire" class="dob-label">ID Expiration Date</label></div>
		<div class="col-xs-4 no-padding"><input type="hidden" id="<%=IDPrefix%>txtIDDateExpire" class="combine-field-value" value="<%=IDExpiredDate%>"/></div>
	</div>
    <%--<input type ="<%=TextAndroidTel%>"  pattern="[0-9]*" id="<%=IDPrefix%>txtIDDateExpire" class="indate" value="" maxlength = '10' />--%>
    <div id="<%=IDPrefix%>divIDDateExpire" class="ui-input-date id-date-expired row">
        <div class="col-xs-4">
            <input aria-labelledby="<%=IDPrefix%>lblIDDateExpire" type="<%=TextAndroidTel%>" pattern="[0-9]*" placeholder="mm" id="<%=IDPrefix%>txtIDDateExpire1" maxlength ='2' onkeydown="limitToNumeric(event);" onkeyup="onKeyUp(event, '#<%=IDPrefix%>txtIDDateExpire1','#<%=IDPrefix%>txtIDDateExpire2', '2');" value="<%=IDExpiredMonth%>"/>
        </div>
        <div class="col-xs-4">
            <input aria-labelledby="<%=IDPrefix%>lblIDDateExpire" type="<%=TextAndroidTel%>" pattern="[0-9]*" placeholder="dd" id="<%=IDPrefix%>txtIDDateExpire2" maxlength ='2' onkeydown="limitToNumeric(event);" onkeyup="onKeyUp(event, '#<%=IDPrefix%>txtIDDateExpire2','#<%=IDPrefix%>txtIDDateExpire3', '2');" value="<%=IDExpiredDay%>"/>
        </div>
        <div class="col-xs-4" style="padding-right: 0px;">
            <input aria-labelledby="<%=IDPrefix%>lblIDDateExpire" type="<%=TextAndroidTel%>" pattern="[0-9]*" placeholder="yyyy" id="<%=IDPrefix%>txtIDDateExpire3" maxlength ='4' onkeydown="limitToNumeric(event);" value="<%=IDExpiredYear%>"/>
        </div>
    </div>
</div>
</div>
<script type="text/javascript" >
    //check default
    var IDCardType = $('#<%=IDPrefix%>ddlIDCardType').val() == undefined ? '' : $('#<%=IDPrefix%>ddlIDCardType').val().trim();
   if(IDCardType !=null || IDCardType !="")
   {    
       <%=IDPrefix%>refreshIDList();      
   }
    $(function () {
        //minor id card type is not required --> remove required red star of minor id
        //var visibleMinorIDCard = $('#divMinorIdentification').attr('style');
        //if (visibleMinorIDCard != undefined && visibleMinorIDCard.indexOf("block") != -1) {
        //    $('#m_spIDCardType').html("");
        //}  

        //fresh id card type and id state for netTeller SSO if it exist
        if ('<%=IDCardType%>' != "") {
            $('#<%=IDPrefix%>ddlIDCardType').val('<%=IDCardType%>');
            $('#<%=IDPrefix%>ddlIDCardType').trigger('change');//selectmenu().selectmenu("refresh")
            if ('<%=IDCountry %>' != "") {
                $('#<%=IDPrefix%>ddlIDCountry').val('<%=IDCountry %>');
                $('#<%=IDPrefix%>ddlIDCountry').selectmenu().selectmenu("refresh");
            }
        }
        if ('<%=IDState%>' != "") {
            $('#<%=IDPrefix%>ddlIDState').val('<%=IDState%>');
            $('#<%=IDPrefix%>ddlIDState').selectmenu().selectmenu("refresh");
        }

        <%=IDPrefix%>initDatePicker("#<%=IDPrefix%>txtIDDateIssued");
        <%=IDPrefix%>initDatePicker("#<%=IDPrefix%>txtIDDateExpire");

    	$('.id-date-issued input', "#<%=IDPrefix%>divApplicantIdentification").on('focusout', function () {
            var maxlength = parseInt($(this).attr('maxlength'));
            // add 0 if value < 10
            $(this).val(padLeft($(this).val(), maxlength));
            <%=IDPrefix%>updateHiddenIssuedDate();
        });

    	$('.id-date-expired input', "#<%=IDPrefix%>divApplicantIdentification").on('focusout', function () {
            var maxlength = parseInt($(this).attr('maxlength'));
            // add 0 if value < 10
            $(this).val(padLeft($(this).val(), maxlength));
            <%=IDPrefix%>updateHiddenExpirationDate();     
        });
        //show and hide id field
        $('#<%=IDPrefix%>ddlIDCardType').on('change', function () {
            <%=IDPrefix%>toggleIDfields($(this));
        });
    });
    
    function <%=IDPrefix%>updateHiddenIssuedDate() {
        var month = padLeft($("#<%=IDPrefix%>txtIDDateIssued1").val(), 2);
        var day = padLeft($("#<%=IDPrefix%>txtIDDateIssued2").val(), 2);
        var year = padLeft($("#<%=IDPrefix%>txtIDDateIssued3").val(), 4);
	    if (month != "" && day != "" && year != "") {
	    	$("#<%=IDPrefix%>txtIDDateIssued").val(month + '/' + day + '/' + year);
	    } else {
	    	$("#<%=IDPrefix%>txtIDDateIssued").val("");    
	    }
	    
    }
    function <%=IDPrefix%>updateHiddenExpirationDate() {
        var month = padLeft($("#<%=IDPrefix%>txtIDDateExpire1").val(), 2);
        var day = padLeft($("#<%=IDPrefix%>txtIDDateExpire2").val(), 2);
        var year = padLeft($("#<%=IDPrefix%>txtIDDateExpire3").val(), 4);
    	if (month != "" && day != "" && year != "") {
    		$("#<%=IDPrefix%>txtIDDateExpire").val(month + '/' + day + '/' + year);
		} else {
    		$("#<%=IDPrefix%>txtIDDateExpire").val("");
		}
    }


    function <%=IDPrefix%>toggleIDfields(element) {
        var divIssueDateElem = $("#<%=IDPrefix%>divIDDateIssued");
        var divExpDateElem = $("#<%=IDPrefix%>divIDDateExpire");
        var divStateElem = $("#<%=IDPrefix%>divState");
        var cardNumberElem = $("#<%=IDPrefix%>txtIDCardNumber");
     
    	if (element.text() != undefined && element.text().toUpperCase().indexOf("I DO NOT HAVE") > -1) {
    	    var lblCardNumTitle = cardNumberElem.closest('div[data-role="fieldcontain"]').find('>label');
    	    var optionElem = element.find('option:selected');
    	    if (optionElem.val() == '106' || optionElem.val().toUpperCase().indexOf("I DO NOT HAVE") > -1 || optionElem.text().toUpperCase().indexOf("I DO NOT HAVE") > -1) { //don't have id number
                //hide expiration/issue date
                //hide state
                //change "ID number" to ID Description
                divStateElem.children().first().addClass('HiddenElement');
                divExpDateElem.closest('div[data-role="fieldcontain"]').addClass('HiddenElement');
                divIssueDateElem.closest('div[data-role="fieldcontain"]').addClass('HiddenElement');
	            ReplaceButtonLabel(lblCardNumTitle, "ID Description");
            } else {
                divStateElem.children().first().removeClass('HiddenElement');
                divExpDateElem.closest('div[data-role="fieldcontain"]').removeClass('HiddenElement');
                divIssueDateElem.closest('div[data-role="fieldcontain"]').removeClass('HiddenElement');
            	ReplaceButtonLabel(lblCardNumTitle, "ID Number");
            }
        }

    }

	function <%=IDPrefix%>initDatePicker(controlId) {
        $(controlId).datepicker({
            showOn: "button",
            changeMonth: true,
            changeYear: true,
            yearRange: "-50:+50",
            buttonImage: "../images/calendar.png",
            buttonImageOnly: true,
            onSelect: function (value, inst) {
                var date = $(this).datepicker('getDate');

                $(controlId + "2").val(padLeft(date.getDate(), 2));
                $(controlId + "1").val(padLeft(date.getMonth() + 1, 2));
                $(controlId + "3").val(padLeft(date.getFullYear(), 4));

                $(controlId).datepicker("hide");
                $(controlId + "1").trigger("focus").trigger("blur");
            }
        });
    }
    /* insprius: text and value of  ddl ID card
    <option text="BIRTH CERTIFICATE" value="101" />
	<option text="I DO NOT HAVE" value="106" />
	<option text="MILITARY ID" value="006" /> -->require expiration date
	<option text="PERMANENT RESIDENT CARD" value="008" />
	<option text="TRIBAL ID" value="113" />
	<option text="OTHER" value="115" />
    */
    function <%=IDPrefix%>isMatriculaCID() {
        var optionElem = $('#<%=IDPrefix%>ddlIDCardType option:selected');
        if (optionElem.length == 0) {
            return false;
        }
        if (optionElem.val().toUpperCase() == 'MATR_CONS_ID' || optionElem.text().toUpperCase().indexOf('MATRICULA CONSULAR ID') > -1) {
            return true;
        }
        return false;
    }
	function <%=IDPrefix%>refreshIDList() {           
        var selectIDType = $('#<%=IDPrefix%>ddlIDCardType').val();
        if (selectIDType != undefined && selectIDType != null) selectIDType= selectIDType.trim();
        var isMatCID =<%=IDPrefix%>isMatriculaCID();
		$('#<%=IDPrefix%>lblIDCardNumber').addClass('RequiredIcon');
        $('#<%=IDPrefix%>lblIDCountry').addClass('RequiredIcon');
        if (selectIDType == null || selectIDType == "" || selectIDType=="106"){
        		$('#<%=IDPrefix%>lblIDDateExpire').removeClass('RequiredIcon');
                $('#<%=IDPrefix%>lblIDDateIssued').removeClass('RequiredIcon');
        		$('#<%=IDPrefix%>lblIDState').removeClass('RequiredIcon');
        	
        		$('#<%=IDPrefix%>lblIDCardNumber').removeClass('RequiredIcon');
        		$('#<%=IDPrefix%>lblIDCountry').removeClass('RequiredIcon');
        }
                    
        if  (selectIDType == 'BIRTH_CERT' || selectIDType=='101') {
        		$('#<%=IDPrefix%>lblIDDateExpire').removeClass('RequiredIcon');
                $('#<%=IDPrefix%>lblIDDateIssued').removeClass('RequiredIcon');
        		$('#<%=IDPrefix%>lblIDCountry').removeClass('RequiredIcon');
                $('#<%=IDPrefix%>divCountry').show();
                $('#<%=IDPrefix%>divState').hide();
                $('#<%=IDPrefix%>hfbooleanStateNeeded').val('false');
        }
        //display country list - for Foreigners
        else if (selectIDType == 'PASSPORT' || selectIDType == 'FOREIGN_ID' || selectIDType == 'FRGN_DRVRS' || isMatCID) {
                $('#<%=IDPrefix%>divCountry').show();
                $('#<%=IDPrefix%>divState').hide();
                $('#<%=IDPrefix%>hfbooleanStateNeeded').val('false') ;
                $('#<%=IDPrefix%>lblIDDateIssued').removeClass('RequiredIcon');
                          
            if (selectIDType == 'PASSPORT' || isMatCID) {
            	$('#<%=IDPrefix%>lblIDDateExpire').addClass('RequiredIcon');
            	$('#<%=IDPrefix%>lblIDDateIssued').addClass('RequiredIcon');
                if (isMatCID) {
                    //pre-select Mexico country if id type is Matricula Consular ID
                    var selectElem = $('select#<%=IDPrefix%>ddlIDCountry');
                    selectElem[0].selectedIndex = selectElem.find('option[value="MEXICO"]').index();
                    selectElem.selectmenu('refresh');
                }
            }
            else{
            	$('#<%=IDPrefix%>lblIDDateExpire').removeClass('RequiredIcon');
                $('#<%=IDPrefix%>lblIDDateIssued').removeClass('RequiredIcon');
            }
        }
        else{
            $('#<%=IDPrefix%>divCountry').hide();
            $('#<%=IDPrefix%>divState').show();
            $('#<%=IDPrefix%>hfbooleanStateNeeded').val('true') ;
            if(selectIDType =='ID_CARD'){
                $('#<%=IDPrefix%>lblIDDateIssued').removeClass('RequiredIcon');
            	$('#<%=IDPrefix%>lblIDDateExpire').addClass('RequiredIcon');
            	$('#<%=IDPrefix%>lblIDState').addClass('RequiredIcon');
                }
            else if (selectIDType =="ALIEN_REG_CARD" || selectIDType=="008" ||selectIDType=="006"){
                $('#<%=IDPrefix%>lblIDDateIssued').removeClass('RequiredIcon');
            	$('#<%=IDPrefix%>lblIDDateExpire').addClass('RequiredIcon');
            	$('#<%=IDPrefix%>lblIDState').removeClass('RequiredIcon');
                $('#<%=IDPrefix%>divState').hide();
            }
            else if (selectIDType == "MILITARY_ID") {
                $('#<%=IDPrefix%>lblIDDateIssued').removeClass('RequiredIcon');
            	$('#<%=IDPrefix%>lblIDDateExpire').removeClass('RequiredIcon');
            	$('#<%=IDPrefix%>lblIDState').removeClass('RequiredIcon');
                $('#<%=IDPrefix%>divState').hide();
            }
            else if (selectIDType == 'DRIVERS_LICENSE') {
            	$('#<%=IDPrefix%>lblIDDateExpire').addClass('RequiredIcon');
            	$('#<%=IDPrefix%>lblIDState').addClass('RequiredIcon');
                //some states are not required driver license issued date(TX,MO,MN,MI,MA) - all state now require issue date
            	$('#<%=IDPrefix%>lblIDDateIssued').addClass('RequiredIcon');
                 
            }
            else {
                $('#<%=IDPrefix%>lblIDDateIssued').removeClass('RequiredIcon');
            	$('#<%=IDPrefix%>lblIDDateExpire').removeClass('RequiredIcon');
            	$('#<%=IDPrefix%>lblIDState').removeClass('RequiredIcon');
                //ID number is not required for students
                if (selectIDType == "STUDENT_ID") {
                	$('#<%=IDPrefix%>lblIDCardNumber').removeClass('RequiredIcon');
                }
            }
        }
	}
 
    function <%=IDPrefix%>ValidateApplicantIdXA() {
        if ($("#<%=IDPrefix%>divApplicantIdentification.showfield-section").length > 0 && $("#<%=IDPrefix%>divApplicantIdentification.showfield-section").hasClass("hidden")) return true;
	    return $.lpqValidate("<%=IDPrefix%>ValidateApplicantIDXA");
    }
	function <%=IDPrefix%>registerXAApplicantIDValidator() {
		<%If IsRequired Then%>
		$('#<%=IDPrefix%>ddlIDCardType').observer({
			validators: [
				function (partial) {
					var cardType = $(this).val();
					$.lpqValidate.hideValidation($('#<%=IDPrefix%>txtIDCardNumber'));
					if (Common.ValidateText(cardType) == false) {
						return "ID Type is required";
					}
					return "";
				}
			],
			validateOnBlur: true,
			group: "<%=IDPrefix%>ValidateApplicantIDXA"
		});
		<%End If %>
		<%If IDPrefix <> "m_" Then%>//make minor id id number not required
		$('#<%=IDPrefix%>txtIDCardNumber').observer({
			validators: [
				function (partial) {
					var cardType = $('#<%=IDPrefix%>ddlIDCardType').val().toUpperCase();
					if (cardType === "STUDENT_ID" || cardType.trim()==="") {
						return ''; //skip validation
					}
					var cardNumber = $(this).val();
					// Card number can have letters and digits such as Driver's License
					if (Common.ValidateText(cardNumber) == false) {
						return "Valid ID Number is required";
					}
					return "";
				}
			],
			validateOnBlur: true,
			group: "<%=IDPrefix%>ValidateApplicantIDXA"
		});
	    <%End If%>

		$('#<%=IDPrefix%>ddlIDCountry').observer({
			validators: [
				function (partial) {
					var cardType = $('#<%=IDPrefix%>ddlIDCardType').val().toUpperCase();
					var country = $(this).val();
					if (cardType == 'FOREIGN_ID' || cardType == 'FRGN_DRVRS' || cardType == "PASSPORT" || <%=IDPrefix%>isMatriculaCID()) {
					    if (!Common.ValidateText(country)) {
					        return 'ID Country is required';
					    } else {
					        if (<%=IDPrefix%>isMatriculaCID() && country.toUpperCase() != "MEXICO") {
					            return 'Valid ID Country is required';
					        }
					    }
					}
					return "";
				}
			],
			validateOnBlur: true,
			group: "<%=IDPrefix%>ValidateApplicantIDXA"
		});
		$('#<%=IDPrefix%>ddlIDState').observer({
			validators: [
				function (partial) {
					var cardType = $('#<%=IDPrefix%>ddlIDCardType').val().toUpperCase();
					var state = $(this).val();
					$.lpqValidate.hideValidation($('#<%=IDPrefix%>divIDDateIssued'));
					if (cardType == 'DRIVERS_LICENSE' || cardType == 'ID_CARD') {
						if (!Common.ValidateText(state)) {
							return 'ID State is required';
						}
					}
					return "";
				}
			],
			validateOnBlur: true,
			group: "<%=IDPrefix%>ValidateApplicantIDXA"
		});
		$('#<%=IDPrefix%>divIDDateIssued').observer({
			validators: [
				function (partial) {
                    <%=IDPrefix%>updateHiddenIssuedDate();
                    <%=IDPrefix%>updateHiddenExpirationDate();
					var cardType = $('#<%=IDPrefix%>ddlIDCardType').val().toUpperCase();
					var state = $('#<%=IDPrefix%>ddlIDState').val().toUpperCase();                 
                    var sDateExp = $('#<%=IDPrefix%>txtIDDateExpire').val();
                    var sDateIssued = $('#<%=IDPrefix%>txtIDDateIssued').val();
					if (!Common.ValidateText(sDateIssued)) {
					    //some states are not required driver license issued date(TX,MO,MN,MI,MA)  - all state require issue date now
                        // make passport issue date required 
					    if ((cardType == 'DRIVERS_LICENSE')||cardType=='PASSPORT' ||<%=IDPrefix%>isMatriculaCID()) {
					        return 'Date Issued is required';
					    }
					}
					if (Common.ValidateText(sDateIssued)) {
						//if (!Common.ValidateDate(sDateIssued)) {
						//	return "Valid Date Issued is required";
						//}
                        var oDateIssued = moment(sDateIssued, "MM-DD-YYYY");
                        if (!Common.IsValidDate(sDateIssued)) {
							return 'Valid Date Issued is required';
						}
						var now = moment();
						if (oDateIssued.year() < now.year() - 100) {
							return "Date Issued is too old";
						}
						if (oDateIssued.year() > now.year() + 100) {
							return "Date Issued is too far in the future";
						}
						if (now.isBefore(oDateIssued)) {
							return "Issued date must be equal or less than current date";
                        }
                        var oDateExp = moment(sDateExp, "MM-DD-YYYY");
                        if (Common.IsValidDate(sDateExp)) {
							if (oDateExp.isSameOrBefore(oDateIssued)) {
								return "Expired date must be greater than the issued date";
							}
							$.lpqValidate.hideValidation($('#<%=IDPrefix%>divIDDateExpire'));
						}
					}
					return "";
				}
			],
			validateOnBlur: true,
			group: "<%=IDPrefix%>ValidateApplicantIDXA"
		});
		$('#<%=IDPrefix%>divIDDateExpire').observer({
			validators: [
				function (partial) {

                    <%=IDPrefix%>updateHiddenIssuedDate();
				    <%=IDPrefix%>updateHiddenExpirationDate();

					var cardType = $('#<%=IDPrefix%>ddlIDCardType').val().toUpperCase();
                    var sDateExp = $('#<%=IDPrefix%>txtIDDateExpire').val();
                    var sDateIssued = $('#<%=IDPrefix%>txtIDDateIssued').val();
                    if ((cardType == 'DRIVERS_LICENSE' || cardType == 'PASSPORT' || cardType == 'ID_CARD' || cardType == 'ALIEN_REG_CARD' || cardType == "008" || cardType == "006" ||<%=IDPrefix%>isMatriculaCID()) && Common.ValidateText(sDateExp) == false) {
						return 'Date Expiration is required';
					}
					if (Common.ValidateText(sDateExp)) {
						//if (!Common.ValidateDate(sDateExp)) {
						//	return "Valid Date Expiration is required";
						//}
                        var oDateExp = moment(sDateExp, "MM-DD-YYYY");
                        if (!Common.IsValidDate(sDateExp)) {
							return 'Valid Date Expiration is required';
						}
						var now = moment();
						if (oDateExp.year() < now.year() - 100) {
							return "Date Expiration is too old";
						}
						if (oDateExp.year() > now.year() + 100) {
							return "Date Expiration is too far in the future";
						}
						oDateExp.add(1, "d").subtract(1, "s");
						if (oDateExp.isBefore(moment())){
							return "Expired date must be equal or greater than current date";
						}
                        var oDateIssued = moment(sDateIssued, "MM-DD-YYYY");
                        if (Common.IsValidDate(sDateIssued)) {
                            oDateIssued.add(1, "d").subtract(1, "s");
                            if (oDateExp.isSameOrBefore(oDateIssued)) {
								return "Expired date must be greater than the issued date";
							}
							$.lpqValidate.hideValidation($('#<%=IDPrefix%>divIDDateIssued'));
						}
					}
					return "";
				}
			],
			validateOnBlur: true,
			group: "<%=IDPrefix%>ValidateApplicantIDXA"
		});
	}

   <%=IDPrefix%>registerXAApplicantIDValidator();

	function <%=IDPrefix%>SetApplicantID(appInfo) {
		if ($("#<%=IDPrefix%>divApplicantIdentification.showfield-section").length >0 && $("#<%=IDPrefix%>divApplicantIdentification.showfield-section").hasClass("hidden")) {
			return appInfo;
		}
        var ddlIDCardType = $('#<%=IDPrefix%>ddlIDCardType option:selected').val();
        var txtIDCardNumber = $('#<%=IDPrefix%>txtIDCardNumber').val();
        var txtIDDateIssued = convertDate($('#<%=IDPrefix%>txtIDDateIssued').val());
        var txtIDDateExpire = convertDate($('#<%=IDPrefix%>txtIDDateExpire').val());
        var ddlIDState = $('#<%=IDPrefix%>ddlIDState option:selected').val();
        var ddlIDCountry = $('#<%=IDPrefix%>ddlIDCountry option:selected').val();
        
           // Determine if we need the State or Country
        if ($('#<%=IDPrefix%>hfbooleanStateNeeded').val() == 'false')
        {
            appInfo.<%=IDPrefix%>IDState = "";
            appInfo.<%=IDPrefix%>IDCountry = ddlIDCountry;
        } else {
            // No need State for Military id and Permanent resident cards
            if (ddlIDCardType == "MILITARY_ID" || ddlIDCardType == "ALIEN_REG_CARD" ||ddlIDCardType=='008') {
                appInfo.<%=IDPrefix%>IDState = "";
            } else {
                appInfo.<%=IDPrefix%>IDState = ddlIDState;
            }
             appInfo.<%=IDPrefix%>IDCountry = "";
        }
        appInfo.<%=IDPrefix%>IDCardType = ddlIDCardType;
        appInfo.<%=IDPrefix%>IDCardNumber = txtIDCardNumber;
        appInfo.<%=IDPrefix%>IDDateIssued = txtIDDateIssued;
        appInfo.<%=IDPrefix%>IDDateExpire = txtIDDateExpire;
        
        return appInfo;
    }

	function <%=IDPrefix%>ViewPrimaryIdentification() {
		var strIdentification = "";
		if ($("#<%=IDPrefix%>divApplicantIdentification.showfield-section").length > 0 && $("#<%=IDPrefix%>divApplicantIdentification.showfield-section").hasClass("hidden")) {
			strIdentification = "";
			$(this).hide();
			$(this).prev("div.row").hide();
		} else {
			var ddlIDCardType = $('#<%=IDPrefix%>ddlIDCardType option:selected').text();
			var selectIDType = $('#<%=IDPrefix%>ddlIDCardType option:selected').val();
			var txtIDCardNumber = htmlEncode($('#<%=IDPrefix%>txtIDCardNumber').val());
			var txtIDDateIssued = $('#<%=IDPrefix%>txtIDDateIssued').val();
			var txtIDDateExpire = $('#<%=IDPrefix%>txtIDDateExpire').val();
			var ddlIDState = $('#<%=IDPrefix%>ddlIDState option:selected').val();
			var ddlIDCountry = $('#<%=IDPrefix%>ddlIDCountry option:selected').val();
			
			if ($.trim(ddlIDCardType) !== "") {
				strIdentification += '<div class="col-sm-6 col-xs-12"><div class="row"><div class="col-xs-6 text-right row-title"><span class="bold">ID Type</span></div><div class="col-xs-6 text-left row-data"><span>' + ddlIDCardType + '</span></div></div></div>';
			}

			if (txtIDCardNumber != '' && $('#<%=IDPrefix%>txtIDCardNumber').IsHideField() === false) {
				strIdentification += '<div class="col-sm-6 col-xs-12"><div class="row"><div class="col-xs-6 text-right row-title"><span class="bold">ID Number</span></div><div class="col-xs-6 text-left row-data"><span>' + txtIDCardNumber + '</span></div></div></div>';
			}
			if (selectIDType == 'PASSPORT' || selectIDType == 'FOREIGN_ID' || selectIDType == 'FRGN_DRVRS' || selectIDType == 'BIRTH_CERT' || selectIDType == '101') {
				if (ddlIDCountry != '') {
					strIdentification += '<div class="col-sm-6 col-xs-12"><div class="row"><div class="col-xs-6 text-right row-title"><span class="bold">ID Country</span></div><div class="col-xs-6 text-left row-data"><span>' + ddlIDCountry + '</span></div></div></div>';
				}
			} else {
				if (ddlIDState != '') {
					strIdentification += '<div class="col-sm-6 col-xs-12"><div class="row"><div class="col-xs-6 text-right row-title"><span class="bold">ID State</span></div><div class="col-xs-6 text-left row-data"><span>' + ddlIDState + '</span></div></div></div>';
				}
			}
			if (txtIDDateIssued != '' && $('#<%=IDPrefix%>txtIDDateIssued').IsHideField() === false) {
				strIdentification += '<div class="col-sm-6 col-xs-12"><div class="row"><div class="col-xs-6 text-right row-title"><span class="bold">ID Date Issued</span></div><div class="col-xs-6 text-left row-data"><span>' + txtIDDateIssued + '</span></div></div></div>';
			}
			if (txtIDDateExpire != '' && $('#<%=IDPrefix%>txtIDDateExpire').IsHideField() === false) {
				strIdentification += '<div class="col-sm-6 col-xs-12"><div class="row"><div class="col-xs-6 text-right row-title"><span class="bold">ID Expiration Date</span></div><div class="col-xs-6 text-left row-data"><span>' + txtIDDateExpire + '</span></div></div></div>';
			}
		}
		if (typeof $(this)[0].tagName !== "undefined" && $(this)[0].tagName === "DIV") {
            $(this).hide();
            $(this).prev("div.row").hide();
            if (strIdentification !== "") {
                $(this).show();
                $(this).prev("div.row").show();
            }
        }
        return strIdentification;
    }
    function <%=IDPrefix%>autoFillData_ID() {
        var $IDCardType = $('#<%=IDPrefix%>ddlIDCardType');
        $IDCardType.val("DRIVERS_LICENSE");
        if ($IDCardType.val() == undefined || $IDCardType.val() == null) {
            $IDCardType.val($IDCardType.find('option:first').val());
        }
            $('#<%=IDPrefix%>txtIDCardNumber').val("4446464664");
            $('#<%=IDPrefix%>ddlIDState').val("CA");
            $('#<%=IDPrefix%>txtIDDateIssued').val("04/04/2002");
            $('#<%=IDPrefix%>txtIDDateIssued1').val("04");
            $('#<%=IDPrefix%>txtIDDateIssued2').val("04");
            $('#<%=IDPrefix%>txtIDDateIssued3').val("2002");

            $('#<%=IDPrefix%>txtIDDateExpire').val("04/04/2029");
            $('#<%=IDPrefix%>txtIDDateExpire1').val("04");
            $('#<%=IDPrefix%>txtIDDateExpire2').val("04");
            $('#<%=IDPrefix%>txtIDDateExpire3').val("2029");
        }   
</script>

