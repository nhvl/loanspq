﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="xaFundingOptions.ascx.vb" Inherits="Inc_MainApp_xaFundingOptions" %>
<%@ Import Namespace="System.Globalization" %>
<div id="divFundingTypeList" class="funding-option-lst">
    <input type="hidden" id="fsSelectedFundingType" value=""/>
    <%If FundingTypeList IsNot Nothing AndAlso FundingTypeList.Any() Then%>
    <%=HeaderUtils.RenderPageTitle(0, "How do you want to fund your new account?", True, isRequired:=True)%>
    <%--<div><span class='ProductCQ-Font'>How do you want to fund your new account?</span><span style='color:red'>*</span></div>--%>
    <% For Each fundingType As String In FundingTypeList
    		Dim btnText As String = ""
    		Dim maxFunding As Decimal = 0D
    		btnText = StrConv(fundingType, VbStrConv.ProperCase)
    		Select Case fundingType.ToUpper()
    			Case "CREDIT CARD"
    				If Decimal.TryParse(CCMaxFunding, maxFunding) = True Then
    					btnText = String.Format("{0} (max {1})", StrConv(fundingType, VbStrConv.ProperCase), maxFunding.ToString("C2", New CultureInfo("en-US")))
    				End If
    			Case "TRANSFER FROM ANOTHER FINANCIAL INSTITUTION"
    				If Decimal.TryParse(ACHMaxFunding, maxFunding) = True Then
    					btnText = String.Format("{0} (max {1})", StrConv(fundingType, VbStrConv.ProperCase), maxFunding.ToString("C2", New CultureInfo("en-US")))
    				End If
    			Case "PAYPAL"
    				If Decimal.TryParse(PaypalMaxFunding, maxFunding) = True Then
    					btnText = String.Format("{0} (max {1})", StrConv(fundingType, VbStrConv.ProperCase), maxFunding.ToString("C2", New CultureInfo("en-US")))
    				End If
    		End Select
    %>
        <div><a href="#" data-role="button" data-value="<%=fundingType%>" <%=DialogPage(fundingType)%> class="btn-header-theme rename-able funding-option"><%=btnText%></a></div>
    <%Next%>
    <%End If%>
</div>
<% If Not String.IsNullOrWhiteSpace(FundingDisclosure) Then%>
<div id="divfundingDisc" class="pt-20">
    <label>
        <input id="fundingDisclosure" title="disclosure" type="checkbox" onchange="$(this).blur();" /><span class="RequiredIcon"><%=FundingDisclosure %></span>
    </label>
    <div class="error-placeholder"></div>
</div>
<%End If %>