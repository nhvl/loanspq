﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="xaApplicantInfo.ascx.vb"
	Inherits="Inc_MainApp_xaApplicantInfo" %>
<%@ Import Namespace="LPQMobile.Utils" %>

<iframe name="<%=IDPrefix%>if_ApplicantInfo" style="display: none;" src="about:blank" ></iframe>
<form target="<%=IDPrefix%>if_ApplicantInfo" action="/handler/AutoFillHandler.aspx" id="<%=IDPrefix%>frm_ApplicantInfo">
<%--<div style="width: 100%">
    <div class="pull-right">
        <a href="#scandocs" data-rel="dialog" data-transition="pop" data-role="button">Open Scandocs</a>
    </div>
    <div class="clearfix"></div>
    <div class="pull-right"><i style="font-weight: normal">Capture and upload your ID to pre-fill information</i></div>
</div>--%>
<%If LaserScandocAvailable = True Then%>
<div id="<%=IDPrefix%>divLaserScanDocument" style="margin-top: 2px;padding: 10px 0px;">
	<%--<add A tag to make the div focusable (for WCAG/ADA) and set inline-block to make heigh of A 100%--%>
	<a href="#<%=IDPrefix%>scandocs" data-rel="dialog" data-transition="pop" style="display: inline-block;" class="img-btn">
    <div class="scandocs-box laser-scan">
	    <div class="caption-text">
            <p class="caption">Click or tap here to pre-fill information with your driver's license</p>
            <div class="avatar">
                <img src="/images/laser_scan.png" alt="avatar" class="img-responsive"/>
            </div>
			</div>
    </div>
	</a>
</div>
<%ElseIf LegacyScandocAvailable Then%>
<div id="<%=IDPrefix%>divScanDocument" style="margin-top: 2px;padding: 10px 0px;">
	<%--<add A tag to make the div focusable (for WCAG/ADA) and set inline-block to make heigh of A 100%--%>
	<a href="#<%=IDPrefix%>scandocs" data-rel="dialog" data-transition="pop" style="display: inline-block;" class="img-btn">
    <div class="scandocs-box">
	    <div class="caption-text">
            <p class="caption">Click or tap here to pre-fill information with your driver's license</p>
            <div class="avatar">
                <img src="/images/avatar.jpg" alt="avatar" class="img-responsive"/>
            </div>
		</div>
    </div>
   </a>
</div>
<%End If%>
<div id="<%=IDPrefix%>divEmployeeOfLender">
    <%If Not String.IsNullOrEmpty(EmployeeOfLenderDropdown) Then%>
     <div data-role="fieldcontain">
		 <label for="<%=IDPrefix%>ddlEmployeeOfLender">Are you an employee of <span class="bold"><%=LenderName%>?</span></label>
         <select id="<%=IDPrefix%>ddlEmployeeOfLender">
	    <%=EmployeeOfLenderDropdown%>
	    </select>
    </div>
	<%End If%>
</div>
<div data-role="fieldcontain">
	<div class="row">
		<div class="col-xs-6 no-padding">
			<label for="<%=IDPrefix%>txtSSN1" id="<%=IDPrefix%>lblSSN">SSN</label>
		</div>
		<div class="col-xs-6 no-padding ui-label-ssn">
			<span class="pull-right header_theme2 ssn-ico" style="padding-left: 4px; padding-right: 2px; display: inline;"><%=HeaderUtils.IconLock16%> <span class="sr-only">SSN IconLock</span></span>
			<span id="<%=IDPrefix%>spOpenSsnSastisfy" style="display: inline;" onclick='openPopup("#<%=IDPrefix%>popSSNSastisfy")' class="pull-right focus-able"  data-rel='popup' data-position-to="window" data-transition='pop'><span class="pull-right header_theme2  ssn-ico"><%=HeaderUtils.IconQuestion16%></span><span class="sr-only">SSN IconQuestion</span></span>
			<%If SSNDisabledString = "" Then%>
			<div style="display: inline" class="hidden">
				<label is_show="1"  class="pull-right header_theme2 rename-able shadow-btn focus-able" style="margin-right: 10px; cursor: pointer;outline: none; display: inline;" onclick="<%=IDPrefix%>toggleSSNText(this);">Hide SSN</label>
			</div>
			<div style="display: inline">
				<label is_show="0"  class="pull-right header_theme2 rename-able shadow-btn focus-able" style="margin-right: 10px; cursor: pointer;outline: none; display: inline;" onclick="<%=IDPrefix%>toggleSSNText(this);">Show SSN</label>
			</div>
				
			<%End If%>
			<div class="clearfix"></div>	
		</div>
	</div>
	<%--<input pattern="[0-9]*" id="<%=IDPrefix%>txtSSN" class="inssn" type="<%=TextAndroidTel%>" maxlength ='11' value="<%=SSN %>" <%=SSNDisabledString%> />--%>
    <div id="<%=IDPrefix%>divSSN" class="ui-input-ssn row">
        <div class="col-xs-4">
            <input type="password" aria-labelledby="<%=IDPrefix%>lblSSN" pattern="[0-9]*" placeholder="---" id="<%=IDPrefix%>txtSSN1" autocomplete="new-password" maxlength ='3' next-input="#<%=IDPrefix%>txtSSN2" onkeydown="limitToNumeric(event);" onkeyup="onKeyUp(event,'#<%=IDPrefix%>txtSSN1','#<%=IDPrefix%>txtSSN2', '3');" value="<%=SSN1%>" <%=SSNDisabledString%> />
            <input type="hidden" id="<%=IDPrefix%>hdSSN1" value=""/>
        </div>
        <div class="col-xs-4">
            <input type="password" aria-labelledby="<%=IDPrefix%>lblSSN" pattern="[0-9]*" placeholder="--" id="<%=IDPrefix%>txtSSN2" autocomplete="new-password" maxlength ='2' next-input="#<%=IDPrefix%>txtSSN3" onkeydown="limitToNumeric(event);" onkeyup="onKeyUp(event,'#<%=IDPrefix%>txtSSN2','#<%=IDPrefix%>txtSSN3', '2');" value="<%=SSN2%>" <%=SSNDisabledString%> />
            <input type="hidden" id="<%=IDPrefix%>hdSSN2" value=""/>
        </div>
        <div class="col-xs-4" style="padding-right: 0px;">
            <input aria-labelledby="<%=IDPrefix%>lblSSN" type="<%=TextAndroidTel%>" pattern="[0-9]*" placeholder="----" id="<%=IDPrefix%>txtSSN3" maxlength ='4' onkeydown="limitToNumeric(event);" value="<%=SSN3%>" <%=SSNDisabledString%> />
            <input type="hidden" id="<%=IDPrefix%>txtSSN" value="<%=SSN%>" <%=SSNDisabledString%> class="combine-field-value"/>
        </div>
    </div>
</div>
<div id="<%=IDPrefix%>popSSNSastisfy" data-role="popup" style="max-width: 400px;">
    <div data-role="content">
        <div class="row">
            <div class="col-xs-12 header_theme2">
                <a data-rel="back" href="#" class="pull-right svg-btn"><%=HeaderUtils.IconClose %><span style="display: none;">close</span></a>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12">
                <div style="margin: 10px 0;">
                    Your Social Security Number (SSN) is used for identification purposes and to determine your account opening eligibility.
                </div>
            </div>    
        </div>
    </div>
</div>
<%If EnableMemberNumber Then%>
<div id="<%=IDPrefix%>divMemberNumber" <%=IIf(IsInMode("777") AndAlso IsInFeature("visibility"), "class=""showfield-section"" data-show-field-section-id=""" & BuildShowFieldSectionID("divMemberNumber") & """ data-default-state=""off"" data-section-name= '" & IIf(String.IsNullOrEmpty(IDPrefix), "", IIf(IDPrefix = "co_", "Co-App", "Minor")) & IIf(InstitutionType = CEnum.InstitutionType.BANK, " Account Number'", " Member Number'"), "")%>>
<div data-role="fieldcontain"> <%-- id="divMemberNumber_txt" runat="server" visible="false"> --%>
   <label for="<%=IDPrefix%>txtMemberNumber" <%=IIf(MemberNumberRequired, "class='RequiredIcon'", "")%>><%:IIf(InstitutionType = CEnum.InstitutionType.BANK, "Account Number", "Member Number")%></label>
   <input type="text" id="<%=IDPrefix%>txtMemberNumber" name="menbernum" maxlength ="50" value="<%=MemberNumber %>" <%=MemberNumberDisabledString%>/>
</div>
</div>
<%End If%>
<div data-role="fieldcontain">
	<label for="<%=IDPrefix%>txtFName" class="RequiredIcon">First Name</label>
	<input id="<%=IDPrefix%>txtFName" type="text" name="fname" autocomplete="given-name" maxlength = "<%=_FieldMaxLength%>" value="<%=FName %>" <%=FNameDisabledString%> />
</div>
<div data-role="fieldcontain">
	<label for="<%=IDPrefix%>txtMName">Middle Name</label>
	<input id="<%=IDPrefix%>txtMName" type="text" name="mname" autocomplete="additional-name" maxlength = "<%=_FieldMaxLength%>" value="<%=MName %>" <%=MNameDisabledString%> />
</div>
<div data-role="fieldcontain">
	<label for="<%=IDPrefix%>txtLName" class="RequiredIcon">Last Name</label>
	<input id="<%=IDPrefix%>txtLName" type="text" maxlength = "<%=_FieldMaxLength%>" name="lname" autocomplete="family-name" value="<%=LName %>" <%=LNameDisabledString%> />
</div>
</form>


<div data-role="fieldcontain">
	<label for="<%=IDPrefix%>suffix">Suffix (Jr., Sr., etc.)</label>
	<select name="suffix" id="<%=IDPrefix%>suffix">
		<%=Common.RenderDropdownlist(GetSuffixList(), "")%>
	</select>
</div>
<%If EnableGender Then%>
<div id="<%=IDPrefix%>divGender" <%=IIf(IsInMode("777") AndAlso IsInFeature("visibility"), "class=""showfield-section"" data-show-field-section-id=""" & BuildShowFieldSectionID("divGender") & """ data-section-name= '" & IIf(String.IsNullOrEmpty(IDPrefix), "", IIf(IDPrefix = "co_", "Co-App", "Minor")) & " Gender'", "")%>>
<div data-role="fieldcontain">
    <label for="<%=IDPrefix%>gender">Gender</label>
	<select name="gender" id="<%=IDPrefix%>gender">
        <option value=""></option>
		<option value="MALE">Male</option>
		<option value="FEMALE">Female</option>
	</select>
</div>
</div>
<%End If%>
<div data-role="fieldcontain" id="<%=IDPrefix%>divDOBStuff">
	<div class="row">
		<div class="col-xs-8 no-padding">
			<label for="<%=IDPrefix%>txtDOB" id="<%=IDPrefix%>lblDOB" class="dob-label RequiredIcon">Date of Birth</label>
		</div>
		<div class="col-xs-4 no-padding"><input type="hidden" id="<%=IDPrefix%>txtDOB" class="combine-field-value" value="<%=DOB%>" <%=DOBDisabledString%> /></div>
	</div>
    <%--<input type="<%=TextAndroidTel%>" pattern="[0-9]*" id="<%=IDPrefix%>txtDOB" class="indate" maxlength ='10' value="<%=DOB%>" <%=DOBDisabledString%> />--%>
    <div id="<%=IDPrefix%>divDOB" class="ui-input-date row">
        <div class="col-xs-4">
            <input aria-labelledby="<%=IDPrefix%>lblDOB" type="<%=TextAndroidTel%>" pattern="[0-9]*" placeholder="mm" id="<%=IDPrefix%>txtDOB1" maxlength ='2' onkeydown="limitToNumeric(event);" onkeyup="onKeyUp(event,'#<%=IDPrefix%>txtDOB1','#<%=IDPrefix%>txtDOB2', '2');" value="<%=DOBMonth%>" <%=DOBDisabledString%> />
        </div>
        <div class="col-xs-4">
            <input aria-labelledby="<%=IDPrefix%>lblDOB" type="<%=TextAndroidTel%>" pattern="[0-9]*" placeholder="dd" id="<%=IDPrefix%>txtDOB2" maxlength ='2' onkeydown="limitToNumeric(event);" onkeyup="onKeyUp(event,'#<%=IDPrefix%>txtDOB2','#<%=IDPrefix%>txtDOB3', '2');" value="<%=DOBDay%>" <%=DOBDisabledString%> />
        </div>
        <div class="col-xs-4" style="padding-right: 0px;">
            <input aria-labelledby="<%=IDPrefix%>lblDOB" type="<%=TextAndroidTel%>" pattern="[0-9]*" placeholder="yyyy" id="<%=IDPrefix%>txtDOB3" onkeydown="limitToNumeric(event);" maxlength ='4' value="<%=DOBYear%>" <%=DOBDisabledString%> />
        </div>
    </div>
    <%-- 
	<div>
		<select name="selectChoiceMonth" id="<%=IDPrefix%>selectChoiceMonth">
			<%=_MonthDDL%>
		</select>
	</div>
	<br />
	<div>
		<select name="selectChoiceDay" id="<%=IDPrefix%>selectChoiceDay">
			<%=_DayDDL%>
		</select>
	</div>
	<br />
	<div>
		<select name="selectChoiceYear" id="<%=IDPrefix%>selectChoiceYear">
			<%=_YearDDL%>
		</select>
	</div>
        --%>
</div>
<%If EnableMotherMaidenName Then%>
<div id="<%=IDPrefix%>divMotherMaidenName" <%=IIf(IsInMode("777") AndAlso IsInFeature("visibility"), "class=""showfield-section"" data-show-field-section-id=""" & BuildShowFieldSectionID("divMotherMaidenName") & """ data-section-name= '" & IIf(String.IsNullOrEmpty(IDPrefix), "", IIf(IDPrefix = "co_", "Co-App", "Minor")) & " Mother Maiden Name'", "")%>>
	<div data-role="fieldcontain">
		<label for="<%=IDPrefix%>txtMotherMaidenName" class="RequiredIcon">Mother's Maiden Name</label>
		<input id="<%=IDPrefix%>txtMotherMaidenName" name="mothermaidenname" autocomplete="name" type="text" maxlength ="20"/>
	</div>
</div>
<%End If%>
<%If IDPrefix <> "" AndAlso Not String.IsNullOrEmpty(RelationshipToPrimary) Then%>
<div data-role="fieldcontain">
    <label for="<%=IDPrefix%>ddlRelationshipToPrimary" class="RequiredIcon">Relationship to Primary Applicant</label>
    <select id="<%=IDPrefix%>ddlRelationshipToPrimary">
	    <%=RelationshipToPrimary%>
	</select>
</div>
<%End If%>
<%If EnableCitizenshipStatus Then%>
<div id="<%=IDPrefix%>divCitizenshipStatus" <%=IIf(IsInMode("777") AndAlso IsInFeature("visibility"), "class=""showfield-section"" data-show-field-section-id=""" & BuildShowFieldSectionID("divCitizenshipStatus") & """ data-section-name= '" & IIf(String.IsNullOrEmpty(IDPrefix), "", IIf(IDPrefix = "co_", "Co-App", "Minor")) & " Citizenship Status'", "")%>>
<div data-role="fieldcontain">
    <label for="<%=IDPrefix%>ddlCitizenshipStatus" class="RequiredIcon">Citizenship Status</label>
	<select id="<%=IDPrefix%>ddlCitizenshipStatus" name="D1">
		<%=CitizenshipStatusDropdown %>
	</select>
</div>
</div>
<%End If%>
<%--Marital status --%>   
<%If EnableMaritalStatus Then%>
<div id="<%=IDPrefix%>divMaritalStatus" <%=IIf(IsInMode("777") AndAlso IsInFeature("visibility"), "class=""showfield-section"" data-show-field-section-id=""" & BuildShowFieldSectionID("divMaritalStatus") & """ data-default-state=""off"" data-section-name= '" & IIf(String.IsNullOrEmpty(IDPrefix), "", IIf(IDPrefix = "co_", "Co-App", "Minor")) & " Marital Status'", "")%>>
<div data-role="fieldcontain">
    <label for="<%=IDPrefix%>ddlMaritalStatus">Marital Status</label>
    <select id="<%=IDPrefix%>ddlMaritalStatus">
        <%=MaritalStatusDropdown%>
    </select>
</div>
</div>
<%End If%>


<script type="text/javascript">
	var <%=IDPrefix%>xbarcodePicker;
	$(function () {
		if (isMobile.any() && !isMobile.Windows()) {//moz-text-security does not works on window devices
			$("#<%=IDPrefix%>txtSSN1,#<%=IDPrefix%>txtSSN2")
				.attr("type", "tel")
				.addClass("mask-password");
		} else {
			$("#<%=IDPrefix%>txtSSN1,#<%=IDPrefix%>txtSSN2")
				.attr("type", "password");
		}

		if (hasForeignTypeApp()) {
			$("#<%=IDPrefix%>lblSSN").removeClass("RequiredIcon"); // no required ->remove red stard
		} else {
			$("#<%=IDPrefix%>lblSSN").addClass("RequiredIcon"); // required -> add restar
		}

		$("#<%=IDPrefix%>txtDOB").datepicker({
			<%=IIf(DOBDisabledString = "", "showOn: 'button',", "showOn: '',")%>
			changeMonth: true,
			changeYear: true,
			yearRange: "-100:+0",
			buttonImage: "../images/calendar.png",
			buttonImageOnly: true,
			onSelect: function (value, inst) {
				var date = $(this).datepicker('getDate');

				$("#<%=IDPrefix%>txtDOB2").val(padLeft(date.getDate(), 2));
				$("#<%=IDPrefix%>txtDOB1").val(padLeft(date.getMonth() + 1, 2));
				$("#<%=IDPrefix%>txtDOB3").val(padLeft(date.getFullYear(), 4));

				$("#<%=IDPrefix%>txtDOB").datepicker("hide");
				$("#<%=IDPrefix%>txtDOB1").trigger("focus").trigger("blur");
			}
		});

		$("#<%=IDPrefix%>popSSNSastisfy").on("popupafteropen", function (event, ui) {
			$("#" + this.id + "-screen").height("");
		});
		$("#<%=IDPrefix%>popSSNSastisfy").on("popupafterclose", function (event, ui) {
			setTimeout(function() {
				$("#<%=IDPrefix%>spOpenSsnSastisfy").focus();
			}, 100);
		});
		$('#<%=IDPrefix%>divDOBStuff div.ui-input-date input').on('focusout', function () {
			var maxlength = parseInt($(this).attr('maxlength'));
			// add 0 if value < 10
			$(this).val(padLeft($(this).val(), maxlength));
			<%=IDPrefix%>updateHiddenDOB();
        });  
		// BUG: not work on mobile, temporary comment out
		<%--$("input[id='<%=IDPrefix%>txtSSN1'],input[id='<%=IDPrefix%>txtSSN2']").each(function () {
           var $self = $(this);
           var maxlen = $self.attr("maxlength");
           $self[0].onkeypress = function (evt) {
               if (window.timer !== undefined)
                   clearTimeout(window.timer);
               var val = this.value;
               evt = evt || window.event;
               // Ensure we only handle printable keys, excluding enter and space
               var charCode = typeof evt.which == "number" ? evt.which : evt.keyCode;
               if (charCode && (charCode >= 48 && charCode <= 57)) {
                   var keyChar = String.fromCharCode(charCode);

                   var start, end;
                   if (typeof this.selectionStart == "number" && typeof this.selectionEnd == "number") {
                       // Non-IE browsers and IE 9
                       start = this.selectionStart;
                       end = this.selectionEnd;
                       this.value = val.slice(0, start).replace(/\d/g, '*') + keyChar + val.slice(end).replace(/\d/g, '*');
                       var hd = $(this).parent().next()[0];
                       var a = hd.value.slice(0, start) + keyChar + hd.value.slice(end);
                       hd.value = a;
                       // Move the caret
                       this.selectionStart = this.selectionEnd = start + 1;
                       window.timer = setTimeout(function() {
                           $self[0].value = $self[0].value.replace(/\d/g, '*');
                       }, 500);
                   }
                   if (this.value.length >= maxlen) {
                       $(this.getAttribute("next-input")).focus();
                   }
                   return false;
               }
               return false;
           }
           $self[0].onkeydown = function (evt) {
               if (this.value.length >= maxlen && this.selectionStart <= maxlen) {
                   if (evt.keyCode !== 8 && evt.keyCode !== 46 && evt.keyCode != 37 && evt.keyCode != 39) {
                       return false;
                   }
               }
               if (evt.keyCode === 8 || evt.keyCode === 46) {
                   var start, end, a;
                   start = this.selectionStart;
                   end = this.selectionEnd;
                   var hd = $(this).parent().next()[0];
                   if (start === end) {
                       a = hd.value.slice(0, start-1) + hd.value.slice(end);
                   } else {
                       a = hd.value.slice(0, start) + hd.value.slice(end);
                   }
                   hd.value = a;
               }
           }
       });--%>

		//show and hide scandocs field
		if ($('#hdScanDocumentKey').val() != "") {
			if ('<%=IDPrefix%>' == 'm_') {
				$('#<%=IDPrefix%>divScanDocument').hide();
			}
		} else {
			$('#<%=IDPrefix%>divScanDocument').hide();
		}
		//end show and hide scandocs field
		//marital status
		<%--var maritalStatus = $('#hdMaritalStatus').val();
        if (maritalStatus == "Y") {
            if ('<%=IDPrefix%>' != 'm_') {
                $('#<%=IDPrefix%>ddlMaritalStatus').parent().show();
            }
        }--%>
		//show and hide member number 
		<%--<%=IDPrefix%>showAndHideMemberNumber();--%>
		<%If IDPrefix <> "m_" Then%>
		$("#<%=IDPrefix%>scandocs").on("pageshow", function () {
			if ($("#<%=IDPrefix%>scandit-barcode-picker").length > 0 && $("#<%=IDPrefix%>scandocs").data("no-camera") != true) {
				ScanditSDK.CameraAccess.getCameras().then(function (cameraList) {
					if (cameraList.length > 0) {
						<%=IDPrefix%>initLaserScan();
					} else {
						<%=IDPrefix%>loadLegacyScan();
					}
					return null;
				}).catch(function (ex) {
					console.log(ex);
					if (ex.name == "NotAllowedError") {
						$("#<%=IDPrefix%>scandocs .js-laser-scan-container").find("a[data-rel='back']").trigger("click");
					} else {
						$("#<%=IDPrefix%>scandocs").data("no-camera", true);
						<%=IDPrefix%>loadLegacyScan();	
					}
					return null;
				});
			} else {
				<%=IDPrefix%>loadLegacyScan();
			}
			$("#<%=IDPrefix%>scandocs a[data-rel='back']").focus();
		});
		$("#<%=IDPrefix%>scandocs").on("pagehide", function () {
			if ($("#<%=IDPrefix%>scandit-barcode-picker").length > 0 && <%=IDPrefix%>xbarcodePicker) {
				$(".divScanDocs.laser-scan", "#<%=IDPrefix%>scandocs").removeClass("open");
				<%=IDPrefix%>xbarcodePicker.destroy();
			}
			$('a[href="#<%=IDPrefix%>scandocs"]').focus();

		});
		<%End If%>
	});
	function <%=IDPrefix%>initLaserScan() {
		ScanditSDK.BarcodePicker.create(document.getElementById("<%=IDPrefix%>scandit-barcode-picker"), {
			playSoundOnScan: true,
			vibrateOnScan: true
		}).then(function (barcodePicker) {
			$("#<%=IDPrefix%>scandocs").find(".scan-guide-text").removeClass("hidden");
			<%=IDPrefix%>xbarcodePicker = barcodePicker;
			// barcodePicker is ready here to be used
			var scanSettings = new ScanditSDK.ScanSettings({
				enabledSymbologies: ["pdf417"],
				codeDuplicateFilter: 1000
			});
			barcodePicker.applyScanSettings(scanSettings);
			$(".scandit-camera-switcher, .scandit-flash-white, .scandit-flash-color", ".scandit-barcode-picker ").addClass("hidden");
			barcodePicker.onScan(function (scanResult) {
				scanResult.barcodes.reduce(function (string, barcode) {
					var result = analyzeData(barcode.data);
					var $scanResultList = $(".scan-result-list", "#<%=IDPrefix%>scandocs");
					if (result.FirstName) {
						$scanResultList.find(".js-prop-name").text(result.FirstName + ($.trim(result.MiddleName).length > 0 ? " " + result.MiddleName : "") + " " + result.LastName);
					}
					$scanResultList.find(".js-prop-dob").text(result.DOB);

					$scanResultList.find(".js-prop-issued-date").text(result.LicenseIssuedDate);

					$scanResultList.find(".js-prop-expired-date").text(result.LicenseExpiredDate);
					var sex = "Unknown";
					if (result.Sex == "1") {
						sex = "Male";
					} else if (result.Sex == "2") {
						sex = "Female";
					}
					$scanResultList.find(".js-prop-sex").text(sex);
					$scanResultList.find(".js-prop-license-number").text(result.LicenseNumber);
					var address = "";
					address += "<p>" + result.MailingAddress1 + "</p>";
					address += "<p>" + result.MailingCity + " " + result.MailingState + ", " + result.MailingZip + "</p>";
					$scanResultList.find(".js-prop-address").html(address);
					$(".divScanDocs.laser-scan", "#<%=IDPrefix%>scandocs").addClass("open");
					$(".scan-result-wrapper", "#<%=IDPrefix%>scandocs").data("scan-result", result);
				}, "");
			});
			return null;
		});
	}
	function <%=IDPrefix%>loadLegacyScan() {
		$("#<%=IDPrefix%>scandocs").find(".js-laser-scan-container").addClass("hidden");
		$("#<%=IDPrefix%>scandocs").find(".js-legacy-scan-container").removeClass("hidden");
	}
	function <%=IDPrefix%>updateHiddenSSN() {
		if ($("#<%=IDPrefix%>txtSSN1").val().length == 3) {
			var ssn = $("#<%=IDPrefix%>txtSSN1").val() + '-' + $("#<%=IDPrefix%>txtSSN2").val() + '-' + $("#<%=IDPrefix%>txtSSN3").val();
			$("#<%=IDPrefix%>txtSSN").val(ssn);
		}
	}

	function <%=IDPrefix%>updateHiddenDOB() {     
		var month = padLeft($("#<%=IDPrefix%>txtDOB1").val(), 2);
		var day = padLeft($("#<%=IDPrefix%>txtDOB2").val(), 2);
		var year = padLeft($("#<%=IDPrefix%>txtDOB3").val(), 4);
		if (month != "" && day != "" && year != "") {
			$("#<%=IDPrefix%>txtDOB").val(month + '/' + day + '/' + year);
		} else {
			$("#<%=IDPrefix%>txtDOB").val("");
		}
	}

	<%--function <%=IDPrefix%>showAndHideMemberNumber(){
        var availability = Common.GetParameterByName("type");
        var visibleMemberNumber = $('#hdVisibleMemberNumber').val();
        var requiredMemberNumber =$('#hdRequiredMemberNumber').val();
        var memberNumberElem =$('#<%=IDPrefix%>txtMemberNumber');
        if(availability=="2" || availability=="2a" || availability=="2b"){ //secondary
            if(visibleMemberNumber =="Y"){
                memberNumberElem.closest('div[data-role="fieldcontain"]').removeClass('HiddenElement');
                if(requiredMemberNumber =="N"){
                	memberNumberElem.closest('div[data-role="fieldcontain"]').children('label').children('span').addClass('HiddenElement');                    
                }else{
                	memberNumberElem.closest('div[data-role="fieldcontain"]').children('label').children('span').removeClass('HiddenElement');
                }
            }else{//hide member number and reset value to to empty string
                memberNumberElem.closest('div[data-role="fieldcontain"]').addClass('HiddenElement');
                memberNumberElem.val('');
            }

        }else{ //hide member number and reset value to to empty string
            memberNumberElem.closest('div[data-role="fieldcontain"]').addClass('HiddenElement');
            memberNumberElem.val('');
        }
    }--%>
	function <%=IDPrefix%>toggleSSNText(btn) {
        return Common.ToggleSSNText(btn, "<%=IDPrefix%>");
	}

	function <%=IDPrefix%>ValidateApplicantInfoXA() {

		<%--var txtMemberNumber = $('#<%=IDPrefix%>txtMemberNumber').val();
        var txtFName = $('#<%=IDPrefix%>txtFName').val();
        var txtMName = $('#<%=IDPrefix%>txtMName').val();
        var txtLName = $('#<%=IDPrefix%>txtLName').val();
        var txtSuffix = $('#<%=IDPrefix%>suffix option:selected').val();
        var txtGender = $('#<%=IDPrefix%>gender option:selected').val();
        var txtDOBElement = $('#<%=IDPrefix%>txtDOB');
        var txtDOB = txtDOBElement.val();
        var txtMotherMaidenName = $('#<%=IDPrefix%>txtMotherMaidenName').val();
        var txtProfession = $('#<%=IDPrefix%>txtProfession').val();
        var ddlCitizenshipStatus = $('#<%=IDPrefix%>ddlCitizenshipStatus option:selected').val();
        var txtSSN = Common.GetSSN($('#<%=IDPrefix%>txtSSN').val());
        var strMessage = '';
        var IsVisibleMemberNumber = $('#<%=divMemberNumber_txt.ClientID%>').is(":visible");
        // txtMemberNumber is not required
       //  if ((txtMemberNumber == null || txtMemberNumber == "") && IsVisibleMemberNumber) {
         //   strMessage += "Please complete the Member Number field. <br />";
        //}
      
        if (!Common.ValidateText(txtFName)) {
            strMessage += 'Please complete the First Name field.<br />';
        }
        if (!Common.ValidateText(txtLName)) {
            strMessage += 'Please complete the Last Name field.<br />';
        }     
        if (!Common.ValidateText(txtSSN)) {
               if (!hasForeignTypeApp()) { //make ssn is not required if hasForeignAppType 
                 strMessage += 'Please complete the Social Security Number field.<br />';
                }
        } else if (/^[0-9]{9}$/.test(txtSSN) == false) {
                 strMessage += 'Please input valid social security number.<br />';
        }
        
        
        //var txtMonth = $('#<--%=IDPrefix%>selectChoiceMonth option:selected').val();
        //var txtDay = $('#<--%=IDPrefix%>selectChoiceDay option:selected').val();
        //var txtYear = $('#<--%=IDPrefix%>selectChoiceYear option:selected').val();
        //txtDOB = '';
        //txtDOB = txtMonth + "/" + txtDay + "/" + txtYear; 
        //change the date of birth to entry numeric textbox instead dropdownlist
        var txtDOB = '';
        txtDOB = $('#<%=IDPrefix%>txtDOB').val();

        if (!Common.ValidateDate(txtDOB)) { 
            if (txtDOB != '') {
                strMessage += 'Date of Birth is invalid<br />';
            } else {
                strMessage += 'Please complete the Date of Birth field.<br />';
            }
        } else {
            if ('<%=IDPrefix%>' == 'm_') {
                strMessage += ValidateDateOfBirth(true, txtDOB,txtDOBElement);
            } else {
                strMessage += ValidateDateOfBirth(false, txtDOB,txtDOBElement);
            }
        }
       
        if (!isHiddenSecMotherMaiden()) { //skip validate if mother maiden name is hidden
            // Check the Mother's Maiden Name field
            var IsVisibleMotherMaidenName = $('#<%=div_lbMotherMaidenName.ClientID%>').is(":visible");
            if ((txtMotherMaidenName == null || txtMotherMaidenName == "") && IsVisibleMotherMaidenName) {
                strMessage += "Please complete the Mother's Maiden Name field. <br />";
            }
        }
        if (!isHiddenSecCitizenship()) { //skip validate if secondary citizenship is hidden
            var IsVisibleCitizenship = $('#<%=div_ddlCitizenship.ClientID%>').is(":visible");
            if ((!Common.ValidateText(ddlCitizenshipStatus)) && IsVisibleCitizenship) {
                strMessage += 'Please complete the Citizenship Status field.<br />';
            }
        }
        //validate Expiration date for pay grade E1 - E9
        /* validate ETS/Expiration date in the employment.logic.js -> use for both xa and Loans  
        if (sEmploymentStatus == "ACTIVE MILITARY") {
              var payGrade = $('#< %=IDPrefix%>ddlPayGrade option:selected').val();
              for (var i = 1; i <= 9; i++) {
                  var selectedPG = 'E' + i;
                  if (payGrade == selectedPG) {
                      //validate date
                      var txtETSDate = $('#< %=IDPrefix%>txtETS').val();
                      if (Common.ValidateText(txtETSDate)) { //not empty
                          if (Common.ValidateDate(txtETSDate)) {
                              var txtExpireDate = new Date(txtETSDate);
                              var txtCurrentDate = new Date();
                              txtExpireDate.setHours(24);
                              if (txtExpireDate.getTime() < txtCurrentDate.getTime()) {
                                  strMessage += "The ETS/Expiration date is not valid. It must be equal or greater than current date.<br/>";
                                  break;  //exit the loop;
                              }
                          } 
                         
                      } else { 
                          //no enter date - no validation
                          break;
                      }
                  }
              }
          }*/

        if ($.trim(strMessage) === "") {
            document.getElementById("<%=IDPrefix%>frm_ApplicantInfo").submit();
        }--%>
		if ($.lpqValidate("<%=IDPrefix%>ValidateApplicantInfoXA")) {
			document.getElementById("<%=IDPrefix%>frm_ApplicantInfo").submit();
			return true;
		}
		return false;
	}


	function <%=IDPrefix%>registerXAApplicantInfoValidator() {
		$('#<%=IDPrefix%>txtFName').observer({
			validators: [
				function (partial) {
					var text = $(this).val();
					if (!Common.ValidateText(text)) {
						return 'First Name is required';					
					} else if (/^[\sa-zA-Z\'-]+$/.test(text) == false) {
						return 'Enter a valid first name. Allowed characters: letters "A-Z", dashes, apostrophes and spaces.';
					}
					return "";
				}
			],
			validateOnBlur: true,
			group: "<%=IDPrefix%>ValidateApplicantInfoXA"
		});
		$('#<%=IDPrefix%>txtLName').observer({
			validators: [
				function (partial) {
					var text = $(this).val();
					if (!Common.ValidateText(text)) {
						return 'Last Name is required';
					} else if (/^[\sa-zA-Z\'-]+$/.test(text) == false) {
						return 'Enter a valid last name. Allowed characters: letters "A-Z", dashes, apostrophes and spaces.';
					}
					return "";
				}
			],
			validateOnBlur: true,
			group: "<%=IDPrefix%>ValidateApplicantInfoXA"
		});
		$('#<%=IDPrefix%>txtMName').observer({
			validators: [
				function (partial) {
					var text = $(this).val();
					if (/^[\sa-zA-Z\'-]*$/.test(text) == false) {
						return 'Enter a valid middle name. Allowed characters: letters "A-Z", dashes, apostrophes and spaces.';
					}
					return "";
				}
			],
			validateOnBlur: true,
			group: "<%=IDPrefix%>ValidateApplicantInfoXA"
		});
		$('#<%=IDPrefix%>divSSN').observer({
			validators: [
				function (partial, evt) {
					<%=IDPrefix%>updateHiddenSSN();

					//var ssn = Common.GetSSN($('#<%=IDPrefix%>txtSSN').val());
					var ssn = $('#<%=IDPrefix%>txtSSN1').val() + $('#<%=IDPrefix%>txtSSN2').val() + $('#<%=IDPrefix%>txtSSN3').val();
					
					if (hasForeignTypeApp() && ssn === "") { //make ssn is not required if hasForeignAppType 
						return "";
					}
					if (/^[0-9]{9}$/.test(ssn) == false) {
						return 'Valid SSN is required';
					}
					<%If IDPrefix = "co_" Then%>
					if (ssn == Common.GetSSN($('#txtSSN').val())) {
						return "SSN shouldn't be the same as Primary Applicant";
					}
					if (ssn == Common.GetSSN($('#m_txtSSN').val())) {
						return "SSN shouldn't be the same as Minor Applicant";
					}
					<%ElseIf IDPrefix = "m_" Then%>
					if (ssn == Common.GetSSN($('#co_txtSSN').val())) {
						return "SSN shouldn't be the same as Joint Applicant";
					}
					if (ssn == Common.GetSSN($('#txtSSN').val())) {
						return "SSN shouldn't be the same as Primary Applicant";
					}
					<%Else%>
					if (ssn == Common.GetSSN($('#m_txtSSN').val())) {
						return "SSN shouldn't be the same as Minor Applicant";
					}
					if (ssn == Common.GetSSN($('#co_txtSSN').val())) {
						return "SSN shouldn't be the same as Joint Applicant";
					}
					<%End If%>
					return "";
				}
			],
			validateOnBlur: true,
			group: "<%=IDPrefix%>ValidateApplicantInfoXA"
		});
		<%If MemberNumberRequired Then %>
		$('#<%=IDPrefix%>txtMemberNumber').observer({
			validators: [
				function(partial,evt){
					if ($(this).closest("div.showfield-section").length > 0 && $(this).closest("div.showfield-section").hasClass("hidden")) return "";
					if (!Common.ValidateText($(this).val())){
						return "<%:IIf(InstitutionType = CEnum.InstitutionType.BANK, "Account Number", "Member Number")%> is required";
					}
					return "";
				}
			],
			validateOnBlur:true,
			group: "<%=IDPrefix%>ValidateApplicantInfoXA"    
		});
		<%End If%>
		<%If IDPrefix <> "" AndAlso Not String.IsNullOrEmpty(RelationshipToPrimary) Then%>
		$('#<%=IDPrefix%>ddlRelationshipToPrimary').observer({
			validators: [
				function (partial) {
					if (!Common.ValidateText($(this).val())) {
						return 'Relationship to Primary Applicant is required';
					}
					return "";
				}
			],
			validateOnBlur: true,
			group: "<%=IDPrefix%>ValidateApplicantInfoXA"
		});
		<%End If%>
		$('#<%=IDPrefix%>divDOB').observer({
			validators: [
				function (partial) {
					<%=IDPrefix%>updateHiddenDOB();
					var $DOB = $('#<%=IDPrefix%>txtDOB');
					if ($DOB.val().trim() == "") {
						return "Date of Birth is required";
					}
					var dob = moment($DOB.val(), "MM-DD-YYYY");
					if (!Common.IsValidDate($DOB.val())) {
						return "Valid Date of Birth is required";
					}
					if (dob.year() < 1900) {
						return "Date of Birth is too old";
					}
					if (dob.isAfter(moment())) {
						return "Date of Birth must be equal or less than current date";
					}
					var strMsg = "";
					var age = moment().diff(dob, "years");
					<%If IDPrefix = "m_" then %>
					function getMinorAgeRange() {
						var minorAgeRange = [];
						var mAccountTypeObj = getMinorAccountTypeObj();
						for (var i = 0; i < MINORACCOUNTLIST.length; i++) {
							if (MINORACCOUNTLIST[i].minorAccountCode == mAccountTypeObj.mAccountType) {
								minorAgeRange.push(MINORACCOUNTLIST[i].minorMinAge);
								minorAgeRange.push(MINORACCOUNTLIST[i].minorMaxAge);
								break;
							}
						}
						return minorAgeRange;
					}
					function validateMinorDateOfBirth(minorAge) {
						var minorAgeRange = getMinorAgeRange();
						var strMessage = "";
						if (minorAgeRange.length > 0) {
							var minAge = minorAgeRange[0];
							var maxAge = minorAgeRange[1];
							if (minAge != "" && maxAge != "") { //has min and max age   
								if (minAge == maxAge) {
									if (parseInt(maxAge) != minorAge) {
										strMessage = "The applicant age must be " + maxAge;
									}
								} else {
									if (parseInt(minAge) > minorAge || parseInt(maxAge) < minorAge) {
										strMessage = "The applicant age must be between " + minAge + " and " + maxAge;
									}
								}                
							} else if (minAge != "" && maxAge == "") { //only has min_age
								if (parseInt(minAge) > minorAge) {
									strMessage = "The applicant age must be at least " + minAge;
								}
							} else if (minAge == "" && maxAge != "") {//only has max_age
								if (parseInt(maxAge) < minorAge) {
									strMessage = "The applicant age must be equal or less than " + maxAge;
								}
							}
						} else {
							if ($('#ddlSpecialAccountType option:selected').val() == "") {
								strMessage = "Please select account type";
							}
						}
						return strMessage;
					}
					strMsg = validateMinorDateOfBirth(age);
					<%Else%>
					var blockUnderAge = <%=_blockApplicantAgeUnder%>;
					if (blockUnderAge > 0) {
						if (age < blockUnderAge) {
							strMsg = "The applicant age must be at least " + blockUnderAge;
						}
					} else {
						if (age < 18) {
							strMsg = "The applicant age must be at least 18";
						}
					}
					<%End If%>
					return strMsg;
				}
			],
			validateOnBlur: true,
			container: '#<%=IDPrefix%>divDOB',
			group: "<%=IDPrefix%>ValidateApplicantInfoXA"
		});
		$('#<%=IDPrefix%>txtMotherMaidenName').observer({
			validators: [
				function (partial) {
					if ($(this).closest("div.showfield-section").length > 0 && $(this).closest("div.showfield-section").hasClass("hidden")) return "";
					var text = $(this).val();
                    if (!Common.ValidateText(text)) {
                        return 'Mother Maiden Name is required';
                    }else if (/^[\sa-zA-Z\'-]*$/.test(text) == false) {
						return 'Enter a valid mother maiden name. Allowed characters: letters "A-Z", dashes, apostrophes and spaces.'			
                    }
					return "";
				}
			],
			validateOnBlur: true,
			group: "<%=IDPrefix%>ValidateApplicantInfoXA"
		});
		$('#<%=IDPrefix%>ddlCitizenshipStatus').observer({
			validators: [
				function (partial) {
					if ($(this).closest("div.showfield-section").length > 0 && $(this).closest("div.showfield-section").hasClass("hidden")) return "";
					if (!Common.ValidateText($(this).val())) {
						return 'Citizenship Status is required';
					}
					return "";
				}
			],
			validateOnBlur: true,
			group: "<%=IDPrefix%>ValidateApplicantInfoXA"
		});
		
	}
	<%=IDPrefix%>registerXAApplicantInfoValidator();
    
	function <%=IDPrefix%>SetApplicantInfo(appInfo) {
		var txtFName = $('#<%=IDPrefix%>txtFName').val();
		var txtMName = $('#<%=IDPrefix%>txtMName').val();
		var txtLName = $('#<%=IDPrefix%>txtLName').val();
		var txtSuffix = $('#<%=IDPrefix%>suffix option:selected').val();
		var txtDOB = $('#<%=IDPrefix%>txtDOB').val();
		//var txtMonth = $('#<--%=IDPrefix%>selectChoiceMonth option:selected').val();
		//var txtDay = $('#<--%=IDPrefix%>selectChoiceDay option:selected').val();
		// var txtYear = $('#<--%=IDPrefix%>selectChoiceYear option:selected').val();

		var txtSSN = Common.GetSSN($('#<%=IDPrefix%>txtSSN').val());

		if (hasForeignTypeApp()) {
			if (txtSSN == "") {
				txtSSN = Common.GetSSN("999-99-9998");
			}
		}
       
		<%If EnableMemberNumber Then%>
		if ($('#<%=IDPrefix%>txtMemberNumber').closest("div.showfield-section").length == 0 || $('#<%=IDPrefix%>txtMemberNumber').closest("div.showfield-section").hasClass("hidden") == false) {
			appInfo.<%=IDPrefix%>MemberNumber = $('#<%=IDPrefix%>txtMemberNumber').val();	
		}
		<%End If%>

		appInfo.<%=IDPrefix%>FirstName = txtFName;
		appInfo.<%=IDPrefix%>MiddleName = txtMName;
		appInfo.<%=IDPrefix%>LastName = txtLName;
		appInfo.<%=IDPrefix%>NameSuffix = txtSuffix;
		//txtDOB = txtYear + "-" + txtMonth + "-" + txtDay;
		appInfo.<%=IDPrefix%>DOB = txtDOB;     
		<%If EnableMotherMaidenName Then%>
		if ($('#<%=IDPrefix%>txtMotherMaidenName').closest("div.showfield-section").length == 0 || $('#<%=IDPrefix%>txtMotherMaidenName').closest("div.showfield-section").hasClass("hidden") == false) {
			appInfo.<%=IDPrefix%>MotherMaidenName = $('#<%=IDPrefix%>txtMotherMaidenName').val();
		}
		<%End If%>

		<%If EnableCitizenshipStatus Then%>
		if ($('#<%=IDPrefix%>ddlCitizenshipStatus').closest("div.showfield-section").length == 0 || $('#<%=IDPrefix%>ddlCitizenshipStatus').closest("div.showfield-section").hasClass("hidden") == false) {
			appInfo.<%=IDPrefix%>Citizenship = $('#<%=IDPrefix%>ddlCitizenshipStatus').val();
		}
		<%End If%>

		<%If EnableGender Then%>
		if ($('#<%=IDPrefix%>gender').closest("div.showfield-section").length == 0 || $('#<%=IDPrefix%>gender').closest("div.showfield-section").hasClass("hidden") == false) {
			appInfo.<%=IDPrefix%>Gender = $('#<%=IDPrefix%>gender').val();
		}
		<%End If%>
		appInfo.<%=IDPrefix%>SSN = txtSSN;
		<%If EnableMaritalStatus Then%>
		if ($('#<%=IDPrefix%>ddlMaritalStatus').closest("div.showfield-section").length == 0 || $('#<%=IDPrefix%>ddlMaritalStatus').closest("div.showfield-section").hasClass("hidden") == false) {
			appInfo.<%=IDPrefix%>MaritalStatus = $('#<%=IDPrefix%>ddlMaritalStatus option:selected').val();
		}
		<%End If%>
		//get employee of lender
		var emp_lender = "NONE";
		var ddlEmployeeOfLender = $('#<%=IDPrefix%>ddlEmployeeOfLender option:selected').val();
		if (ddlEmployeeOfLender != undefined) //make sure it has employee of lender dropdown list
		{
			if (ddlEmployeeOfLender != "" && ddlEmployeeOfLender != "NONE") {
				emp_lender = ddlEmployeeOfLender;
			}
		}
		appInfo.<%=IDPrefix%>EmployeeOfLender = emp_lender;
		<%If IDPrefix <> "" AndAlso Not String.IsNullOrEmpty(RelationshipToPrimary) Then%>
		appInfo.<%=IDPrefix%>RelationshipToPrimary = $('#<%=IDPrefix%>ddlRelationshipToPrimary option:selected').val();
		<%End If%>
	}

	function <%=IDPrefix%>ViewAccountInfo() {
		var strHtml = "";
		<%--var txtMemberNumber = htmlEncode($('#<%=IDPrefix%>txtMemberNumber').val());--%>
		var txtFName = htmlEncode($('#<%=IDPrefix%>txtFName').val());
		var txtMName = htmlEncode($('#<%=IDPrefix%>txtMName').val());
		var txtLName = htmlEncode($('#<%=IDPrefix%>txtLName').val());
		var ddlSuffix = htmlEncode($('#<%=IDPrefix%>suffix option:selected').val());
		var txtDOB = $('#<%=IDPrefix%>txtDOB').val();
		var txtSSN = Common.GetSSN($('#<%=IDPrefix%>txtSSN').val());
		var ddlEmployeeOfLender = $('#<%=IDPrefix%>ddlEmployeeOfLender option:selected').val();
        
		<%--var hasExistingMember = '<%=HasExistingMember%>';--%>
       
		if (ddlEmployeeOfLender != undefined) {
			if (ddlEmployeeOfLender != "") {
				strHtml += '<div class="col-sm-6 col-xs-12"><div class="row"><div class="col-xs-6 text-right row-title"><span class="bold">Employee of Lender</span></div><div class="col-xs-6 text-left row-data"><span>' + $('#<%=IDPrefix%>ddlEmployeeOfLender option:selected').text() + '</span></div></div></div>';
			}
		}
		var strFullName = txtFName.trim() + (txtMName.trim() != "" ? " " + txtMName + " " : " ") + txtLName.trim();
		if ($.trim(strFullName) !== "") {
			strHtml += '<div class="col-sm-6 col-xs-12"><div class="row"><div class="col-xs-6 text-right row-title"><span class="bold">Full Name</span></div><div class="col-xs-6 text-left row-data"><span>' + strFullName + '</span></div></div></div>';	
		}
        
		if (ddlSuffix != '') {
			strHtml += '<div class="col-sm-6 col-xs-12"><div class="row"><div class="col-xs-6 text-right row-title"><span class="bold">Suffix</span></div><div class="col-xs-6 text-left row-data"><span>' + ddlSuffix + '</span></div></div></div>';
		}
		if (txtSSN != "") {
			strHtml += '<div class="col-sm-6 col-xs-12"><div class="row"><div class="col-xs-6 text-right row-title"><span class="bold">SSN</span></div><div class="col-xs-6 text-left row-data"><span>' + txtSSN.replace(/^\d{5}/, "*****") + '</span></div></div></div>';
		}

		<%--if (hasExistingMember == 'Y') {
            if (txtMemberNumber != "") {
                strHtml += '<div class="col-sm-6 col-xs-12"><div class="row"><div class="col-xs-6 text-right row-title"><span class="bold"><%:IIf(InstitutionType = CEnum.InstitutionType.BANK, "Account Number", "Member Number")%></span></div><div class="col-xs-6 text-left row-data"><span>' + txtMemberNumber + '</span></div></div></div>';
            }
        }--%>
		<%If EnableMemberNumber Then%>
		if ($('#<%=IDPrefix%>txtMemberNumber').closest("div.showfield-section").length == 0 || $('#<%=IDPrefix%>txtMemberNumber').closest("div.showfield-section").hasClass("hidden") == false) {
			if ($('#<%=IDPrefix%>txtMemberNumber').val() != "") {
				strHtml += '<div class="col-sm-6 col-xs-12"><div class="row"><div class="col-xs-6 text-right row-title"><span class="bold"><%:IIf(InstitutionType = CEnum.InstitutionType.BANK, "Account Number", "Member Number")%></span></div><div class="col-xs-6 text-left row-data"><span>' + htmlEncode($('#<%=IDPrefix%>txtMemberNumber').val()) + '</span></div></div></div>';
			}
		}
		<%End If%>
    	<%If IDPrefix <> "" AndAlso Not String.IsNullOrEmpty(RelationshipToPrimary) Then%>
		strHtml += '<div class="col-sm-6 col-xs-12"><div class="row"><div class="col-xs-6 text-right row-title"><span class="bold">Relationship to Primary Applicant</span></div><div class="col-xs-6 text-left row-data"><span>' + $('#<%=IDPrefix%>ddlRelationshipToPrimary option:selected').text() + '</span></div></div></div>';
		<%End If%>
		<%If EnableGender Then%>
		if ($('#<%=IDPrefix%>gender').closest("div.showfield-section").length == 0 || $('#<%=IDPrefix%>gender').closest("div.showfield-section").hasClass("hidden") == false) {
			if ($('#<%=IDPrefix%>gender').val() != "") {
				strHtml += '<div class="col-sm-6 col-xs-12"><div class="row"><div class="col-xs-6 text-right row-title"><span class="bold">Gender</span></div><div class="col-xs-6 text-left row-data"><span>' + $('#<%=IDPrefix%>gender').val() + '</span></div></div></div>';
			}
		}
    	
		<%End If%>
		if ($.trim(txtDOB) !== "") {
			strHtml += '<div class="col-sm-6 col-xs-12"><div class="row"><div class="col-xs-6 text-right row-title"><span class="bold">Date of Birth</span></div><div class="col-xs-6 text-left row-data"><span>' + txtDOB + '</span></div></div></div>';	
		}
        
		<%If EnableMotherMaidenName Then%>
		if ($('#<%=IDPrefix%>txtMotherMaidenName').closest("div.showfield-section").length == 0 || $('#<%=IDPrefix%>txtMotherMaidenName').closest("div.showfield-section").hasClass("hidden") == false) {
			if ($('#<%=IDPrefix%>txtMotherMaidenName').val() != "") {
				strHtml += '<div class="col-sm-6 col-xs-12"><div class="row"><div class="col-xs-6 text-right row-title"><span class="bold">MMN</span></div><div class="col-xs-6 text-left row-data masking-text"><span>' + htmlEncode($('#<%=IDPrefix%>txtMotherMaidenName').val()) + '</span><span onclick="toggleReveal(this)"></span></div></div></div>';
			}
		}
    	
		<%End If%>
		<%If EnableCitizenshipStatus Then%>
		if ($('#<%=IDPrefix%>ddlCitizenshipStatus').closest("div.showfield-section").length == 0 || $('#<%=IDPrefix%>ddlCitizenshipStatus').closest("div.showfield-section").hasClass("hidden") == false) {
			if ($.trim($('#<%=IDPrefix%>ddlCitizenshipStatus').val()) != "" && $('#<%=IDPrefix%>ddlCitizenshipStatus option:selected').text() != "") {
				strHtml += '<div class="col-sm-6 col-xs-12"><div class="row"><div class="col-xs-6 text-right row-title"><span class="bold">Citizenship Status</span></div><div class="col-xs-6 text-left row-data"><span>' + $('#<%=IDPrefix%>ddlCitizenshipStatus option:selected').text() + '</span></div></div></div>';
			}
		}
    	
		<%End If%>
		
		<%If EnableMaritalStatus Then%>
		if ($('#<%=IDPrefix%>ddlMaritalStatus').closest("div.showfield-section").length == 0 || $('#<%=IDPrefix%>ddlMaritalStatus').closest("div.showfield-section").hasClass("hidden") == false) {
			if ($('#<%=IDPrefix%>ddlMaritalStatus option:selected').val() != "") {
				strHtml += '<div class="col-sm-6 col-xs-12"><div class="row"><div class="col-xs-6 text-right row-title"><span class="bold">Marital Status</span></div><div class="col-xs-6 text-left row-data"><span>' + $('#<%=IDPrefix%>ddlMaritalStatus option:selected').val() + '</span></div></div></div>';
			}
		}
    	
		<%End If%>
		return strHtml;
	}
	function <%=IDPrefix%>autoFillData_PersonalInfor()
    {  
		$('#<%=IDPrefix%>txtFName').val("Marisol");
	 	$('#<%=IDPrefix%>txtLName').val("Testcase");
	 	$('#<%=IDPrefix%>txtDOB').val("11/11/1990");
	 	$('#<%=IDPrefix%>txtDOB1').val("11");
	 	$('#<%=IDPrefix%>txtDOB2').val("11");
	 	$('#<%=IDPrefix%>txtDOB3').val("1990");
	 	$('#<%=IDPrefix%>txtMemberNumber').val("testing1111");
      
	 	$('#<%=IDPrefix%>txtSSN').val("000-00-0001");
	 	$('#<%=IDPrefix%>txtSSN1').val("000");
	 	$('#<%=IDPrefix%>txtSSN2').val("00");
	 	$('#<%=IDPrefix%>txtSSN3').val("0001");  
	 	if('<%=IDPrefix%>' =="co_")
	 	{
	 		$('#<%=IDPrefix%>txtSSN').val("000-00-0002");
        	$('#<%=IDPrefix%>txtSSN1').val("000");
        	$('#<%=IDPrefix%>txtSSN2').val("00");
        	$('#<%=IDPrefix%>txtSSN3').val("0002");
        	$('#<%=IDPrefix%>txtDOB').val("08/22/1980");
        	$('#<%=IDPrefix%>txtDOB1').val("08");
        	$('#<%=IDPrefix%>txtDOB2').val("22");
        	$('#<%=IDPrefix%>txtDOB3').val("1980");
        	$('#<%=IDPrefix%>txtFName').val("David");
          }else if('<%=IDPrefix%>' == "m_"){
			$('#<%=IDPrefix%>txtSSN').val("000-00-0003");
	 		$('#<%=IDPrefix%>txtSSN1').val("000");
	 		$('#<%=IDPrefix%>txtSSN2').val("00");
	 		$('#<%=IDPrefix%>txtSSN3').val("0003");
	 		$('#<%=IDPrefix%>txtDOB').val("08/22/1981");
	 		$('#<%=IDPrefix%>txtDOB1').val("08");
	 		$('#<%=IDPrefix%>txtDOB2').val("22");
	 		$('#<%=IDPrefix%>txtDOB3').val("1981");
              $('#<%=IDPrefix%>txtFName').val("David");
	 	}

		$("#<%=IDPrefix%>txtMotherMaidenName").val("cindy");
		$('#<%=IDPrefix%>ddlMaritalStatus option').eq(1).prop('selected', true);
		<%If IDPrefix <> "" AndAlso Not String.IsNullOrEmpty(RelationshipToPrimary) Then%>
		$('#<%=IDPrefix%>ddlRelationshipToPrimary option').eq(1).prop('selected', true);
		<%End If%>
	 }
</script>

