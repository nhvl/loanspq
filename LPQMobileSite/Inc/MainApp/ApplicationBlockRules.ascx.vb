﻿Imports CustomList
Imports Newtonsoft.Json

Partial Class Inc_MainApp_ApplicationBlockRules
	Inherits CBaseUserControl

	Protected rulesJson As String

	Public CustomListItem As CCustomListRuleItem

	Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
		If Not IsPostBack Then
			Dim ruleList = CustomListItem.GetList(Of CApplicantBlockRule)()
			If ruleList Is Nothing Then ruleList = New List(Of CApplicantBlockRule)
			If ruleList.Count > 0 Then
				rulesJson = JsonConvert.SerializeObject(ruleList.Where(Function(r) r.Enable AndAlso r.Expression IsNot Nothing AndAlso r.Rule IsNot Nothing).Select(Function(r) New With {.name = r.Name, .expression = CompileExpression(r.Expression.js), .values = r.Expression.params}))
				'rulesJson = oSerializer.Serialize(RuleList.Where(Function(r) r.Enable).Select(Function(r) New With {.name = r.Name, .expression = CompileExpression(r.Expression.sql), .values = r.Expression.params}))
				'log.Debug(rulesJson)
			End If
		End If
	End Sub

	Private Function CompileExpression(expression As String) As String
		Dim ret As String = ""
		If Not String.IsNullOrWhiteSpace(expression) Then
			ret = Regex.Replace(expression, "\s*@([0-9]+)\s*", " ruleValues[$1-1] ")
			ret = Regex.Replace(ret, "#([a-zA-Z0-9_-]+)", "formValues['$1']")
			ret = Regex.Replace(ret, "%([PCA]Q_[a-zA-Z0-9_-]+)%", "formValues['$1']")
		End If
		Return ret
	End Function
End Class
