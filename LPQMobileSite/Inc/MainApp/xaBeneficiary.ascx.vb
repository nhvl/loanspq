﻿
Partial Class Inc_MainApp_xaBeneficiary
    Inherits CBaseUserControl

	Private _enableBeneficiarySSN As Boolean

	Public Property EnableBeneficiarySSN As Boolean
		Get
			If ForceSSNShown = True Then
				Return True
			End If
			Return _enableBeneficiarySSN
		End Get
		Set(value As Boolean)
			_enableBeneficiarySSN = value
		End Set
	End Property
	Public Property EnableBeneficiaryTrust As Boolean

	Public Property EnableBeneficiaryAddress As Boolean

	Public Property ForceSSNShown As Boolean = False

End Class
