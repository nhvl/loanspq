﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="ApplicantGMI.ascx.vb" Inherits="Inc_MainApp_ApplicantGMI" %>
<div class="div_gmi_section">
	<br />
    <%--<h4>Information For Government Monitoring Purpose <a href='#popUpGMPInfo' data-rel='popup' data-transition='pop'><div class='questionMark'></div></a></h4>--%>
	<%=HeaderUtils.RenderPageTitle(0, "Demographic Information", True)%>
    <div id="<%=IDPrefix%>popUpGMPInfo" data-role="popup" data-position-to="window" style="max-width: 400px;">
        <div data-role="content">
            <div class="header_theme2" style="float: right; position: relative; z-index: 1000;">
                <a href="#" data-rel="back" class="pull-right svg-btn"><%=HeaderUtils.IconClose %><span style="display: none;">close</span></a>
            </div>
            <div class="row">
                <div class="col-sm-12">
                    <div style="margin: 10px 0;">
	                    <%=DemographicDisclosure %>
                    </div>
                </div>
            </div>
			<div class="row">
				<div class="col-sm-12">
					<a href="#" class="div-goback ui-btn" data-rel="back" data-corners="false" data-shadow="false" style="display: block;">Close</a>   
				</div>
			</div>
        </div>
    </div>

	<div class="gmi-input-group">
		<%=DemographicDescription %>
	</div>

	<div class="gmi-input-group">
		<a href="javascript:void(0);" onclick="openPopup('#<%=IDPrefix %>popUpGMPInfo')" class="rename-able" data-rel="popup" data-transition="pop">Read full disclosure</a>
	</div>

    <div class="gmi-input-group" id="<%=IDPrefix%>chkGenderGroup">
        <p><span class="bold <%If IsRequired Then%>RequiredIcon<% End If%>">Sex </span>(select all that apply)</p>
        <div id="<%=IDPrefix%>divShowSexMessage"><span id="<%=IDPrefix%>spShowSexMessage"></span></div>
		<label for="<%=IDPrefix%>chkNotProvidedSex" ><input type="checkbox" id="<%=IDPrefix%>chkNotProvidedSex" onchange="<%=IDPrefix%>GMIGenderClick(this)" value="NOT_PROVIDED_SEX"/>I do not wish to provide this information</label>
		<label for="<%=IDPrefix%>chkMale" ><input type="checkbox" id="<%=IDPrefix%>chkMale" onchange="<%=IDPrefix%>GMIGenderClick(this)" value="MALE"/>Male</label>
		<label for="<%=IDPrefix%>chkFemale" ><input type="checkbox" id="<%=IDPrefix%>chkFemale" onchange="<%=IDPrefix%>GMIGenderClick(this)" value="FEMALE"/>Female</label>
    </div>
    <div class="gmi-input-group" id="<%=IDPrefix%>chkEthnicityGroup">
        <p><span class="bold <%If IsRequired Then%>RequiredIcon<% End If%>">Ethnicity </span>(select all that apply)</p>
        <div id="<%=IDPrefix%>divShowEthnicityMessage"><span id="<%=IDPrefix%>spShowEthnicityMessage"></span></div>
		<label for="<%=IDPrefix%>chkNotProvidedEthnicity"><input type="checkbox" id="<%=IDPrefix%>chkNotProvidedEthnicity" onchange="<%=IDPrefix%>GMINotProvidedEthnicityClick(this)" value="NOT_PROVIDED_HISPANIC_OR_LATINO"/>I do not wish to provide this information</label>
		<label for="<%=IDPrefix%>chkNotHispanicOrLatino"><input type="checkbox" value="NOT_HISPANIC_OR_LATINO" id="<%=IDPrefix%>chkNotHispanicOrLatino" onchange="<%=IDPrefix%>GMINotHispanicOrLatinoClick(this)"/>Not Hispanic or Latino</label>
		<label class="droplist" for="<%=IDPrefix%>chkHispanicOrLatino"><input type="checkbox" value="HISPANIC_OR_LATINO" id="<%=IDPrefix%>chkHispanicOrLatino" onchange="<%=IDPrefix%>GMIHispanicOrLatinoClick(this)"/>Hispanic or Latino</label>
		<div class="ml20 hidden" id="<%=IDPrefix%>divHispanicOrLatino">
			<label for="<%=IDPrefix%>chkMexican"><input type="checkbox" data-group="HISPANIC" value="M" id="<%=IDPrefix%>chkMexican"/>Mexican</label>
			<label for="<%=IDPrefix%>chkPuertoRico"><input type="checkbox" data-group="HISPANIC" value="P" id="<%=IDPrefix%>chkPuertoRico"/>Puerto Rican</label>
			<label for="<%=IDPrefix%>chkCuban"><input type="checkbox" data-group="HISPANIC" value="C" id="<%=IDPrefix%>chkCuban"/>Cuban</label>
			<label for="<%=IDPrefix%>chkOtherHispanicOrLatino"><input type="checkbox" data-group="HISPANIC" id="<%=IDPrefix%>chkOtherHispanicOrLatino" onchange="<%=IDPrefix%>GMIOtherHispanicOrLatinoClick(this)" value="O"/>Other Hispanic or Latino</label>
			<div class="ml40 hidden" id="<%=IDPrefix%>divOtherHispanicOrLatino" data-role="fieldcontain">
				<input type="text" id="<%=IDPrefix%>txtOtherHispanicOrLatino" aria-labelledby="<%=IDPrefix%>divOtherHispanicOrLatino" />
			</div>
		</div>
    </div>
    <div class="gmi-input-group" id="<%=IDPrefix%>chkRaceGroup">
        <p><span class="bold <%If IsRequired Then%>RequiredIcon<% End If%>">Race </span>(select all that apply)</p>
        <div id="<%=IDPrefix%>divShowRaceMessage"><span id="<%=IDPrefix%>spShowRaceMessage"></span></div>
		<label for="<%=IDPrefix%>chkNotProvidedRace"><input type="checkbox" id="<%=IDPrefix%>chkNotProvidedRace" onchange="<%=IDPrefix%>GMIRaceClick(this)" value="NOT_PROVIDED"/>I do not wish to provide this information</label>
		<label for="<%=IDPrefix%>chkAmericanIndian"><input type="checkbox" data-group="RACE" value="AMERICAN_INDIAN" id="<%=IDPrefix%>chkAmericanIndian" onchange="<%=IDPrefix%>GMIRaceClick(this)"/>American Indian or Alaska Native (principal tribe)</label>
		<div class="ml20 hidden" id="<%=IDPrefix%>divTribeName" data-role="fieldcontain">
				<input type="text" id="<%=IDPrefix%>txtTribeName" aria-labelledby="<%=IDPrefix%>divTribeName" />
			</div>
		<label class="droplist" for="<%=IDPrefix%>chkAsian"><input type="checkbox" data-group="RACE" id="<%=IDPrefix%>chkAsian"  onchange="<%=IDPrefix%>GMIRaceClick(this)" value="ASIAN"/>Asian</label>
		<div class="ml20 hidden" id="<%=IDPrefix%>divAsian">
			<label for="<%=IDPrefix%>chkAsianIndian"><input type="checkbox" data-group="RACEASIAN" value="AI" id="<%=IDPrefix%>chkAsianIndian"/>Asian Indian</label>
			<label for="<%=IDPrefix%>chkChinese"><input type="checkbox" data-group="RACEASIAN" value="AC" id="<%=IDPrefix%>chkChinese"/>Chinese</label>
			<label for="<%=IDPrefix%>chkFillipino"><input type="checkbox" data-group="RACEASIAN" value="AF" id="<%=IDPrefix%>chkFillipino"/>Filipino</label>
			<label for="<%=IDPrefix%>chkJapanese"><input type="checkbox" data-group="RACEASIAN" value="AJ" id="<%=IDPrefix%>chkJapanese"/>Japanese</label>
			<label for="<%=IDPrefix%>chkKorean"><input type="checkbox" data-group="RACEASIAN" value="AK" id="<%=IDPrefix%>chkKorean"/>Korean</label>
			<label for="<%=IDPrefix%>chkVietnamese"><input type="checkbox" data-group="RACEASIAN" value="AV" id="<%=IDPrefix%>chkVietnamese"/>Vietnamese</label>
			<label for="<%=IDPrefix%>chkOtherAsian"><input type="checkbox" data-group="RACEASIAN" value="AO" id="<%=IDPrefix%>chkOtherAsian" onchange="<%=IDPrefix%>GMIOtherAsianClick(this)"/>Other Asian:</label>
			<div class="ml40 hidden" id="<%=IDPrefix%>divOtherAsian" data-role="fieldcontain">
				<input type="text" id="<%=IDPrefix%>txtOtherAsian" aria-labelledby="<%=IDPrefix%>divOtherAsian" />
			</div>
		</div>
		<label for="<%=IDPrefix%>chkBlackAfrican"><input type="checkbox" data-group="RACE" value="BLACK" onchange="<%=IDPrefix%>GMIRaceClick(this)"  id="<%=IDPrefix%>chkBlackAfrican" />Black or African American</label>
		<label class="droplist" for="<%=IDPrefix%>chkPacificIslander"><input type="checkbox" data-group="RACE" value="PACIFIC_ISLANDER" id="<%=IDPrefix%>chkPacificIslander" onchange="<%=IDPrefix%>GMIRaceClick(this)"/>Native Hawaiian or Other Pacific Islander</label>
		<div class="ml20 hidden" id="<%=IDPrefix%>divNativeHawaiian">
			<label for="<%=IDPrefix%>chkNativeHawaiian"><input type="checkbox" data-group="RACEPACIFICISLANDER" value="PH" id="<%=IDPrefix%>chkNativeHawaiian"/>Native Hawaiian</label>
			<label for="<%=IDPrefix%>chkGuamanianChamorro"><input type="checkbox" data-group="RACEPACIFICISLANDER" value="PG" id="<%=IDPrefix%>chkGuamanianChamorro"/>Guamanian or Chamorro</label>
			<label for="<%=IDPrefix%>chkSamoan"><input type="checkbox" data-group="RACEPACIFICISLANDER" value="PS" id="<%=IDPrefix%>chkSamoan"/>Samoan</label>
			<label for="<%=IDPrefix%>chkOtherPacificIslander" ><input type="checkbox" data-group="RACEPACIFICISLANDER" value="PO" id="<%=IDPrefix%>chkOtherPacificIslander" onchange="<%=IDPrefix%>GMIOtherPacificIslanderClick(this)"/>Other Pacific Islander</label>
			<div class="ml40 hidden" data-role="fieldcontain" id="<%=IDPrefix%>divOtherPacificIslander">
				<input type="text" id="<%=IDPrefix%>txtOtherPacificIslander" aria-labelledby="<%=IDPrefix%>divOtherPacificIslander" />
			</div>	
		</div>
		<label  for="<%=IDPrefix%>chkWhite" ><input type="checkbox" data-group="RACE" value="WHITE" onchange="<%=IDPrefix%>GMIRaceClick(this)" id="<%=IDPrefix%>chkWhite"/>White</label>
	</div>
</div>

<script type="text/javascript">
    function <%=IDPrefix%>ValidateGMIInfo() {
        <%If Not IsRequired Then%>return true;<% End If%>

        var validator = true;
        if(!$.lpqValidate("<%=IDPrefix%>ValidateGenderInfo")){
            validator = false;
        }
        if(!$.lpqValidate("<%=IDPrefix%>ValidateEthnicityInfo")){
            validator = false;
        }
        if(!$.lpqValidate("<%=IDPrefix%>ValidateRaceInfo")){
            validator = false;
        }
        return validator;
    }
    function <%=IDPrefix%>registerGMIInfoValidator() {
        $("#<%=IDPrefix%>chkGenderGroup").observer({
            validators: [
                function (partial) {
                    if ($("input[type=checkbox]:checked", this).length == 0) {
                        $('#<%=IDPrefix%>divShowSexMessage').css('height', '35px');
                        return "Please select one of the options below";
                    }
                     $('#<%=IDPrefix%>divShowSexMessage').css('height', '0');
                    return "";
                }
            ],
            validateOnBlur: true,
            group: "<%=IDPrefix%>ValidateGenderInfo",
            groupType: "complexUI",
            placeHolder: "#<%=IDPrefix%>spShowSexMessage"
        });
        $("#<%=IDPrefix%>chkEthnicityGroup").observer({
            validators: [
                function (partial) {
                    if ($("input[type=checkbox]:checked", this).length == 0) {
                        $('#<%=IDPrefix%>divShowEthnicityMessage').css('height', '35px');
                        return "Please select one of the options below";
                    }
                     $('#<%=IDPrefix%>divShowEthnicityMessage').css('height', '0');
                    return "";
                }
            ],
            validateOnBlur: true,
            group: "<%=IDPrefix%>ValidateEthnicityInfo",
            groupType: "complexUI",
            placeHolder: "#<%=IDPrefix%>spShowEthnicityMessage"
        });
        $("#<%=IDPrefix%>chkRaceGroup").observer({
            validators: [
                function (partial) {
                    if ($("input[type=checkbox]:checked", this).length == 0) {
                        $('#<%=IDPrefix%>divShowRaceMessage').css('height', '35px');
                        return "Please select one of the options below";
                    }
                     $('#<%=IDPrefix%>divShowRaceMessage').css('height', '0');
                    return "";
                }
            ],
            validateOnBlur: true,
            group: "<%=IDPrefix%>ValidateRaceInfo",
            groupType: "complexUI",
            placeHolder: "#<%=IDPrefix%>spShowRaceMessage"
        });
    }
    <%If IsRequired Then%>
        <%=IDPrefix%>registerGMIInfoValidator();
    <% End If%>

	function <%=IDPrefix%>SetGMIInfo(appInfo) {
		appInfo.<%=IDPrefix%>HasGMI = (g_IsHideGMI && g_IsHideGMI_HELOC) ? "N" : "Y";
		appInfo.<%=IDPrefix%>SexNotProvided = $("#<%=IDPrefix%>chkNotProvidedSex").is(":checked") ? "Y" : "N";
		appInfo.<%=IDPrefix%>Sex = "";
		if (appInfo.<%=IDPrefix%>SexNotProvided == "N") {
			if ($("#<%=IDPrefix%>chkMale").is(":checked") && $("#<%=IDPrefix%>chkFemale").is(":checked")) {
				appInfo.<%=IDPrefix%>Sex = "OTHER";
			} else if ($("#<%=IDPrefix%>chkMale").is(":checked")) {
				appInfo.<%=IDPrefix%>Sex = "MALE";
			} else if ($("#<%=IDPrefix%>chkFemale").is(":checked")) {
				appInfo.<%=IDPrefix%>Sex = "FEMALE";
			 }
		}
		//Ethnicity
		appInfo.<%=IDPrefix%>EthnicityNotProvided = $("#<%=IDPrefix%>chkNotProvidedEthnicity").is(":checked") ? "Y" : "N";
		appInfo.<%=IDPrefix%>EthnicityHispanicOther = "";
		appInfo.<%=IDPrefix%>EthnicityHispanic = "";
		appInfo.<%=IDPrefix%>EthnicityIsNotHispanic = "";
		appInfo.<%=IDPrefix%>EthnicityIsHispanic = "";
		if (appInfo.<%=IDPrefix%>EthnicityNotProvided == "N") {
			appInfo.<%=IDPrefix%>EthnicityIsNotHispanic = $("#<%=IDPrefix%>chkNotHispanicOrLatino").is(":checked") ? "Y" : "N";
			appInfo.<%=IDPrefix%>EthnicityIsHispanic = $("#<%=IDPrefix%>chkHispanicOrLatino").is(":checked") ? "Y" : "N";
			if (appInfo.<%=IDPrefix%>EthnicityIsHispanic == "Y") {
				appInfo.<%=IDPrefix%>EthnicityHispanic = $.map($("#<%=IDPrefix%>divHispanicOrLatino").find("input[data-group='HISPANIC']:checked"), function (ele) {
					return $(ele).val();
				}).join(",");
			}
			if ($("#<%=IDPrefix%>chkOtherHispanicOrLatino").is(":checked")) {
				appInfo.<%=IDPrefix%>EthnicityHispanicOther = $("#<%=IDPrefix%>txtOtherHispanicOrLatino").val();	
			}
		}
		//Race
		appInfo.<%=IDPrefix%>RaceNotProvided = $("#<%=IDPrefix%>chkNotProvidedRace").is(":checked") ? "Y" : "N";
		appInfo.<%=IDPrefix%>RaceBase = $.map($("#<%=IDPrefix%>chkRaceGroup").find("input[data-group='RACE']:checked"), function (ele) {
			return $(ele).val();
		}).join(",");

		if ($("#<%=IDPrefix%>chkAmericanIndian").is(":checked")) {
			appInfo.<%=IDPrefix%>RaceTribeName = $("#<%=IDPrefix%>txtTribeName").val();
		}
		appInfo.<%=IDPrefix%>RaceAsian = "";
		if ($("#<%=IDPrefix%>chkAsian").is(":checked")) {
			appInfo.<%=IDPrefix%>RaceAsian = $.map($("#<%=IDPrefix%>chkRaceGroup").find("input[data-group='RACEASIAN']:checked"), function (ele) {
				return $(ele).val();
			}).join(",");
		}
		appInfo.<%=IDPrefix%>RaceAsianOther = "";
		if ($("#<%=IDPrefix%>chkOtherAsian").is(":checked")) {
			appInfo.<%=IDPrefix%>RaceAsianOther = $("#<%=IDPrefix%>txtOtherAsian").val();	
		}

		appInfo.<%=IDPrefix%>RacePacificIslander = "";
		if ($("#<%=IDPrefix%>chkPacificIslander").is(":checked")) {
			appInfo.<%=IDPrefix%>RacePacificIslander = $.map($("#<%=IDPrefix%>chkRaceGroup").find("input[data-group='RACEPACIFICISLANDER']:checked"), function (ele) {
				return $(ele).val();
			}).join(",");
		}
		appInfo.<%=IDPrefix%>RacePacificIslanderOther = "";
		if ($("#<%=IDPrefix%>chkOtherPacificIslander").is(":checked")) {
			appInfo.<%=IDPrefix%>RacePacificIslanderOther = $("#<%=IDPrefix%>txtOtherPacificIslander").val();
		}
        return appInfo;
	}
	
	
	$("#<%=IDPrefix%>popUpGMPInfo").on("popupafteropen", function (event, ui) {
        //prevent scrollbar by popup overlay
		$("#" + this.id + "-screen").height("");
    });

	function <%=IDPrefix%>GMIGenderClick(ele) {
		var $self = $(ele);
		var $container = $self.closest("div.gmi-input-group");
		if ($self.is(":checked")) {
			if ($self.val() == "NOT_PROVIDED_SEX") {
				$("input:checkbox", $container).not($self).prop("checked", false).checkboxradio("refresh");
			} else {
				$("input:checkbox[value='NOT_PROVIDED_SEX']", $container).prop("checked", false).checkboxradio("refresh");
			}
		}
        $self.blur();
	}

	function <%=IDPrefix%>GMINotHispanicOrLatinoClick(ele) {
		var $self = $(ele);
        if ($self.is(":checked")) {		
			$("#<%=IDPrefix%>chkNotProvidedEthnicity").prop("checked", false).checkboxradio("refresh");
		}
        $self.blur();
	}

	function <%=IDPrefix%>GMIHispanicOrLatinoClick(ele) {
		var $self = $(ele);
		if ($self.is(":checked")) {
			$("#<%=IDPrefix%>divHispanicOrLatino").removeClass("hidden");		
			$("#<%=IDPrefix%>chkNotProvidedEthnicity").prop("checked", false).checkboxradio("refresh");
		} else {
			$("#<%=IDPrefix%>divHispanicOrLatino").addClass("hidden");
		}
		$("#<%=IDPrefix%>divHispanicOrLatino").find("input:checkbox").each(function(idx, chk) {
			$(chk).prop("checked", false).checkboxradio("refresh");
		});
		$("#<%=IDPrefix%>txtOtherHispanicOrLatino").val("");
		$("#<%=IDPrefix%>divOtherHispanicOrLatino").addClass("hidden");

        $self.blur();
	}
	function <%=IDPrefix%>GMIOtherHispanicOrLatinoClick(ele) {
		var $self = $(ele);
		if ($self.is(":checked")) {
			$("#<%=IDPrefix%>divOtherHispanicOrLatino").removeClass("hidden");
			$("#<%=IDPrefix%>txtOtherHispanicOrLatino").focus();
		} else {
			$("#<%=IDPrefix%>divOtherHispanicOrLatino").addClass("hidden");
		}
		$("#<%=IDPrefix%>txtOtherHispanicOrLatino").val("");

        $self.blur();
	}
	function <%=IDPrefix%>GMIOtherAsianClick(ele) {
		var $self = $(ele);
		if ($self.is(":checked")) {
			$("#<%=IDPrefix%>divOtherAsian").removeClass("hidden");
			$("#<%=IDPrefix%>txtOtherAsian").focus();
		} else {
			$("#<%=IDPrefix%>divOtherAsian").addClass("hidden");
		}
		$("#<%=IDPrefix%>txtOtherAsian").val("");

        $self.blur();
	}
	
	function <%=IDPrefix%>GMIOtherPacificIslanderClick(ele) {
		var $self = $(ele);
		if ($self.is(":checked")) {
			$("#<%=IDPrefix%>divOtherPacificIslander").removeClass("hidden");
			$("#<%=IDPrefix%>txtOtherPacificIslander").focus();
		} else {
			$("#<%=IDPrefix%>divOtherPacificIslander").addClass("hidden");
		}
		$("#<%=IDPrefix%>txtOtherPacificIslander").val("");

        $self.blur();
	}
	function <%=IDPrefix%>GMINotProvidedEthnicityClick(ele) {
		var $self = $(ele);
		if ($self.is(":checked")) {
			$self.closest("div.gmi-input-group").find("input:checkbox:checked").not($self).each(function(idx, chk) {
				$(chk).prop("checked", false).checkboxradio("refresh");
				$(chk).trigger("change");
			});
		}

        $self.blur();
	}
	function <%=IDPrefix%>GMIRaceClick(ele) {
		var $self = $(ele);
		if ($self.is(":checked")) {
			if ($self.val() === "NOT_PROVIDED") {
				$self.closest("div.gmi-input-group").find("input:checkbox:checked").not($self).each(function (idx, chk) {
					$(chk).prop("checked", false).checkboxradio("refresh");
					$(chk).trigger("change");
				});
			} else {
				$("#<%=IDPrefix%>chkNotProvidedRace").prop("checked", false).checkboxradio("refresh");
			}
		}
		if ($self.val() == "ASIAN") {
			if ($self.is(":checked")) {
				$("#<%=IDPrefix%>divAsian").removeClass("hidden");
			} else {
				$("#<%=IDPrefix%>divAsian").addClass("hidden");
			}
			$("#<%=IDPrefix%>divAsian").find("input:checkbox").each(function (idx, chk) {
				$(chk).prop("checked", false).checkboxradio("refresh");
			});
			$("#<%=IDPrefix%>txtOtherAsian").val("");
			$("#<%=IDPrefix%>divOtherAsian").addClass("hidden");
		} else if ($self.val() == "PACIFIC_ISLANDER") {
			if ($self.is(":checked")) {
				$("#<%=IDPrefix%>divNativeHawaiian").removeClass("hidden");
			} else {
				$("#<%=IDPrefix%>divNativeHawaiian").addClass("hidden");
			}
			$("#<%=IDPrefix%>divNativeHawaiian").find("input:checkbox").each(function (idx, chk) {
				$(chk).prop("checked", false).checkboxradio("refresh");
			});
			$("#<%=IDPrefix%>txtOtherPacificIslander").val("");
			$("#<%=IDPrefix%>divOtherPacificIslander").addClass("hidden");
		}else if ($self.val() == "AMERICAN_INDIAN") {
			if ($self.is(":checked")) {
				$("#<%=IDPrefix%>divTribeName").removeClass("hidden");
				$("#<%=IDPrefix%>txtTribeName").focus();
			} else {
				$("#<%=IDPrefix%>divTribeName").addClass("hidden");
			}
			$("#<%=IDPrefix%>txtTribeName").val("");
		}

        $self.blur();
	}
</script>