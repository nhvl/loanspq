﻿/////////////////////////////////
///  BEGIN EMPLOYMENT LOGIC   ///
/////////////////////////////////
var EMPLogic = {};
EMPLogic.StatusID = "";
EMPLogic.PrefixID = "";
EMPLogic.ParentID = "";
EMPLogic.RestID = "";
EMPLogic.Mode = "";

EMPLogic.VisibleIDs = [];
EMPLogic.PreVisibleIDs = [];

EMPLogic.EStatus = {};
EMPLogic.EStatus.EMPLOYED = "EMPLOYED";
EMPLogic.EStatus.ACTIVE_MILITARY = "ACTIVE MILITARY";
EMPLogic.EStatus.HOMEMAKER = "HOMEMAKER";
EMPLogic.EStatus.OWNER = "OWNER";
EMPLogic.EStatus.RETIRED = "RETIRED";
EMPLogic.EStatus.SELF_EMPLOYED = "SELF EMPLOYED";
EMPLogic.EStatus.UNEMPLOYED = "UNEMPLOYED";
EMPLogic.EStatus.STUDENT = "STUDENT";
EMPLogic.EStatus.OTHER = "OTHER";
EMPLogic.EStatus.RETIRED_MILITARY = "RETIRED MILITARY";
EMPLogic.EStatus.GOVERNMENT_DOD = "GOVERNMENT/DOD";
EMPLogic.format = function(s) {
    var s = arguments[0];
    for (var i = 0; i < arguments.length - 1; i++) {
        var reg = new RegExp("\\{" + i + "\\}", "gm");
        s = s.replace(reg, arguments[i + 1]);
    }
    return s;
}

EMPLogic.htmlEncode = function(value) {
    return $('<div/>').text(value).html();
}

EMPLogic.buildDropDown = function(title, id, extraAttr, arr, isRequired) {
    id = this.PrefixID + id;
    this.VisibleIDs.push(id);

    var html = "";
    html += this.format('<div data-role="fieldcontain">');
    html += this.format('<label for="{0}" {2}>{1}</label>', id, this.ReplaceButtonLabel(title), (isRequired === true ? 'class="RequiredIcon"' : ''));
    //remove red star and <span> tag from title
    var length = title.indexOf("<span");
    if (length > 0) {
        title = title.substring(0, length);
    }
    html += this.format('<select id="{0}" {1} emp_required="{2}" title="{3}">',
         id, extraAttr, isRequired, title);

    for (var _i = 0; _i < arr.length; _i++) {
        var val = this.htmlEncode(arr[_i]);
        html += this.format('<option value="{0}">{1}</option>', val, val);
    }

    html += this.format("</select>");
    html += this.format('</div>');
    return html;
}

EMPLogic.buildTextBox = function(title, id, extraAttr, isRequired) {
    id = this.PrefixID + id;
    this.VisibleIDs.push(id);

    var html = "";
    html += this.format('<div data-role="fieldcontain">');
    html += this.format('<label for="{0}" class="{1}">{2}</label>', id, (isRequired === true ? 'RequiredIcon' : ''), this.ReplaceButtonLabel(title));
    //remove red star and <span> tag from title
    var length = title.indexOf("<span");
    if (length > 0) {
        title = title.substring(0, length);
    }
    html += this.format('<input type="text" maxlength="50" id="{0}" {1} emp_required="{2}"  title="{3}"/>', id, extraAttr, isRequired, title);
    html += this.format('</div>');
    return html;
}
EMPLogic.buildDuration = function(title, id, extraAttr, isRequired) {
    id = this.PrefixID + id;
    this.VisibleIDs.push(id+'_year');
    this.VisibleIDs.push(id+'_month');
    
    var html = "";
    html += this.format('<div data-role="fieldcontain">');
    html += this.format('<label for="{0}" {2}>{1}</label>', id, this.ReplaceButtonLabel(title), (isRequired === true ? 'class="RequiredIcon"' : ''));
    //remove red star and <span> tag from title
    var length = title.indexOf("<span");
    if (length > 0) {
        title = title.substring(0, length);
    }
    html += this.format('<div id="div'+ id +'" class="ui-input-horizal">');
    html += this.format('<div class="col-xs-6">');
   /* html += this.format('<input type="text"  pattern="[0-9]*"  id="{0}_year" maxlength="2" class="emp_numeric emp_inyear" {1} emp_required="{2}"  title="{3}" />',
        id, extraAttr, isRequired, title);*/
    html += this.format('<select id="{0}_year" {1} emp_required="{2}"  title="{3}">', id, extraAttr, isRequired, title);
    html += this.format('<option value=" ">Years</option>');
    for (var _i = 0; _i < 100; _i++) {
        html += this.format('<option value="{0}">{1}</option>', _i, _i);
    }
    html += this.format('</select>');
    html += this.format('</div>');
    html += this.format('<div class="col-xs-6">');
    html += this.format('<select id="{0}_month" {1} emp_required="{2}"  title="{3}">', id, extraAttr, isRequired, title);
    html += this.format('<option value=" ">Months</option>');
    for (var _i = 0; _i < 12; _i++) {
        html += this.format('<option value="{0}">{1}</option>', _i, _i);
    }

    html += this.format('</select>');
    html += this.format('</div>');
    html += this.format("</div>");
    html += this.format("</div>");
    return html;
}

EMPLogic.buildDateTime = function(title, id, extraAttr, isRequired) {
    id = this.PrefixID + id;
    this.VisibleIDs.push(id);


    var html = "";
    html += this.format('<div data-role="fieldcontain">');
    html += this.format('<label for="{0}">{1}</label>', id, this.ReplaceButtonLabel(title));
    title = title.replace("<em>", "").replace("</em>",""); 
    if (!isMobile.Android()) {

        html += this.format('<input type = "text" pattern ="[0-9]*" id="{0}" {1} class="emp_indate" emp_required="{2}" emp_date="true"  title="{3}"/>', id, extraAttr, isRequired, title);

    } else {
        html += this.format('<input type = "tel" maxlength = "10" id="{0}" {1} class="emp_indate" emp_required="{2}" emp_date="true"  title="{3}"/>', id, extraAttr, isRequired, title);
    }
    html += this.format('</div>');
    return html;
}

EMPLogic.buildDateTime2 = function (title, id, extraAttr, isRequired) {
    id = this.PrefixID + id;
    this.VisibleIDs.push(id);
    this.VisibleIDs.push(id + "_txtDOB1");
    this.VisibleIDs.push(id + "_txtDOB2");
    this.VisibleIDs.push(id + "_txtDOB3");
    var html = "";
    html += this.format('<div data-role="fieldcontain" id="{0}_divEmpDOBStuff">', id);
    html += this.format('<label for="{0}" class="{1}">', id, (isRequired === true ? "dob-label RequiredIcon" : "dob-label"));
    html += this.ReplaceButtonLabel(title);
    title = title.replace("<em>", "").replace("</em>", "");
    html += this.format('<input type="hidden" id="{0}" emp_required="{1}" title="{2}"/>', id, isRequired, title);
    html += '<div class="clearfix"></div>';
    html += '</label>';
    
    if (!isMobile.Android()) {
        html += '<div id="div'+ id +'" class="row ui-input-date">';
        html += '<div class="col-xs-4">';
        html += this.format('<input type="text" pattern="[0-9]*" abort-validate="true" placeholder="mm" id="{0}_txtDOB1" maxlength="2" onkeydown="limitToNumeric(event);" onkeyup="onKeyUp(event, \'#{0}_txtDOB1\',\'#{0}_txtDOB2\', \'2\');"/>', id);
        html += '</div>';
        html += '<div class="col-xs-4">';
        html += this.format('<input type="text" pattern="[0-9]*" abort-validate="true" placeholder="dd" id="{0}_txtDOB2" maxlength="2" onkeydown="limitToNumeric(event);" onkeyup="onKeyUp(event, \'#{0}_txtDOB2\',\'#{0}_txtDOB3\', \'2\');"/>', id);
        html += '</div>';
        html += '<div class="col-xs-4" style="padding-right: 0px;">';
        html += this.format('<input type="text" pattern="[0-9]*" abort-validate="true" placeholder="yyyy" id="{0}_txtDOB3" maxlength="4"/>', id);
        html += '</div>';
        html += '</div>';

    } else {
    	html += '<div id="div' + id + '" class="row ui-input-date">';
        html += '<div class="col-xs-4">';
        html += this.format('<input type="tel" pattern="[0-9]*" abort-validate="true" placeholder="mm" id="{0}_txtDOB1" maxlength="2" onkeydown="limitToNumeric(event);" onkeyup="onKeyUp(event, \'#{0}_txtDOB1\',\'#{0}_txtDOB2\', \'2\');"/>', id);
        html += '</div>';
        html += '<div class="col-xs-4">';
        html += this.format('<input type="tel" pattern="[0-9]*" abort-validate="true" placeholder="dd" id="{0}_txtDOB2" maxlength="2" onkeydown="limitToNumeric(event);" onkeyup="onKeyUp(event, \'#{0}_txtDOB2\',\'#{0}_txtDOB3\', \'2\');"/>', id);
        html += '</div>';
        html += '<div class="col-xs-4" style="padding-right: 0px;">';
        html += this.format('<input type="tel" pattern="[0-9]*" abort-validate="true" placeholder="yyyy" id="{0}_txtDOB3" maxlength="4"/>', id);
        html += '</div>';
        html += '</div>';
    }
    html += this.format('</div>');
    return html;
}
//build ddl occupation 
EMPLogic.buildDropDownOccupationList = function (title, id, extraAttr, obj, isRequired) {
    id = this.PrefixID + id;
    this.VisibleIDs.push(id);
    var html = "";
    html += this.format('<div data-role="fieldcontain">');
    html += this.format('<label for="{0}" class="{1}">{2}</label>', id, (isRequired === true ? 'RequiredIcon' : ''), this.ReplaceButtonLabel(title));
    //remove red star and <span> tag from title
    var length = title.indexOf("<span");
    if (length > 0) {
        title = title.substring(0, length);
    }
    html += this.format('<select id="{0}" {1} emp_required="{2}" title="{3}">',
         id, extraAttr, isRequired, title);
    var oList = JSON.parse(obj);
    for (var key in oList) {
        var text = this.htmlEncode(key);
        var val = this.htmlEncode(oList[key]);
        html += this.format('<option value="{0}">{1}</option>', val, text);
    }

    html += this.format("</select>");
    html += this.format('</div>');
    return html;
}
EMPLogic.isOccupationListEmpty = function () {
    if (OCCUPATIONLIST == "")
        return true;

    var obj = JSON.parse(OCCUPATIONLIST);
    for (var key in obj) {
        if (obj.hasOwnProperty(key))
            return false;
    }

    return true;
}
EMPLogic.init = function(statusId, prefix, parentId, restID, mode) {
    this.StatusID = statusId;
    this.PrefixID = prefix;
    this.ParentID = parentId;
    this.VisibleIDs = [];
    this.PreVisibleIDs = [];
    this.RestID = restID;
	this.Mode = mode;
	var empLogic = this;
    $('#' + statusId).change(function() {
        empLogic.refreshHtml();
    });
}

EMPLogic.isDate = function(txtDate) {
    var currVal = txtDate;
    if (currVal == '')
        return false;

    //Declare Regex  
    var rxDatePattern = /^(\d{1,2})(\/|-)(\d{1,2})(\/|-)(\d{4})$/;
    var dtArray = currVal.match(rxDatePattern); // is format OK?

    if (dtArray == null)
        return false;

    //Checks for mm/dd/yyyy format.
    dtMonth = dtArray[1];
    dtDay = dtArray[3];
    dtYear = dtArray[5];

    if (dtMonth < 1 || dtMonth > 12)
        return false;
    else if (dtDay < 1 || dtDay > 31)
        return false;
    else if ((dtMonth == 4 || dtMonth == 6 || dtMonth == 9 || dtMonth == 11) && dtDay == 31)
        return false;
    else if (dtMonth == 2) {
        var isleap = (dtYear % 4 == 0 && (dtYear % 100 != 0 || dtYear % 400 == 0));
        if (dtDay > 29 || (dtDay == 29 && !isleap))
        	return false;
     }
     else if (dtYear < 1910 || dtYear > 2110)  //limit range to 
       	return false;
    return true;
}
EMPLogic.setValueToAppObject = function(target) {
    for (var _i = 0; _i < this.VisibleIDs.length; _i++) {
        if (this.VisibleIDs[_i].indexOf('txtJobTitle') >= 0 && $('#' + this.VisibleIDs[_i]).attr('type') != 'text') {
            target[this.VisibleIDs[_i]] = $('#' + this.VisibleIDs[_i] + ' option:selected').val();
       } else {
            target[this.VisibleIDs[_i]] = $('#' + this.VisibleIDs[_i]).val();
        }
    }
}
EMPLogic.validate2 = function() {
	return $.lpqValidate(this.StatusID);
}

EMPLogic.validate = function() {
    var errorMessage = '';

    // EmployedDuration_year
    var needToRequireEmployedDuration = false;
    var yearEmployedDuration = 0;
    var monthEmployedDuration = 0;
    var titleEmployedDuration = '';

    // ProfessionDuration_year
    var needToRequireProfessionDuration = false;
    var yearProfessionDuration = 0;
    var monthProfessionDuration = 0;
    var titleProfessionDuration = '';

    for (var _i = 0; _i < this.VisibleIDs.length; _i++) {
        var id = this.VisibleIDs[_i];
        var control = $('#' + id);

        if (id.indexOf('EmployedDuration_year') >= 0) {
            yearEmployedDuration = control.val();
            titleEmployedDuration = control.attr('title');
            if (control.attr('emp_required') == 'true')
                needToRequireEmployedDuration = true;
            continue;
        }

        if (id.indexOf('EmployedDuration_month') >= 0) {
            monthEmployedDuration = control.val();
            titleEmployedDuration = control.attr('title');
            if (control.attr('emp_required') == 'true')
                needToRequireEmployedDuration = true;
            continue;
        }

        if (id.indexOf('ProfessionDuration_year') >= 0) {
            yearProfessionDuration = control.val();
            titleProfessionDuration = control.attr('title');
            if (control.attr('emp_required') == 'true')
                needToRequireProfessionDuration = true;
            continue;
        }

        if (id.indexOf('ProfessionDuration_month') >= 0) {
            monthProfessionDuration = control.val();
            titleProfessionDuration = control.attr('title');

            if (control.attr('emp_required') == 'true')
                needToRequireProfessionDuration = true;
            continue;
        }


        /* assume there will not be a date in employment logic
        if (control.attr('emp_date') == 'true') {
        if (!this.isDate(control.val())) {
        errorMessage += 'Please correct the ' + control.attr('title') + ' field. <br/>';
        continue;
        }
        }*/
        if (id.indexOf('txtETS') >= 0) {
            if (control.val() != '') {
                if (control.attr("abort-validate") === "true") {
                    continue;
                }
                if (!this.isDate(control.val())) {
                    errorMessage += 'Please correct the ' + control.attr('title') + ' field. <br/>';
                    continue;
                }else{
                    var expiredDate = moment(control.val(), "MM-DD-YYYY");
                    if (this.PrefixID.indexOf("prev_") != -1) {
	                    //Previous employment ETS should not be a future date
                    	if (expiredDate.isAfter(moment())) {
                    		errorMessage += "The ETS/Expiration date is not valid. It must be equal or less than current date.<br/>";
                    		continue;
                    	} else if (expiredDate.isBefore(moment("01-01-1900", "MM-DD-YYYY"))) {
                    		errorMessage += "The ETS/Expiration date is not valid. The year you entered must be greater than 1900.<br/>";
                    		continue;
                    	}
                    } else {
                    	var maxDate = new Date("01/01/2068");
                    	if (moment().isAfter(expiredDate)) {
                    		errorMessage += "The ETS/Expiration date is not valid. It must be equal or greater than current date.<br/>";
                    		continue;
                    	} else if (expiredDate.isAfter(moment("01-01-2068", "MM-DD-YYYY"))) {
                    		errorMessage += "The ETS/Expiration date is not valid. The year you entered must be less than 2068.<br/>";
                    		continue;
                    	}
                    }
                }
            }
        }
		if (!this.isOccupationListEmpty()) { //select dropdown field
            if (id.indexOf('txtJobTitle') >= 0) {
                if (control.attr('emp_required') == 'true') {
                    if (control.val() == '') {
                        errorMessage += 'Please select the ' + control.attr('title') + ' field. <br/>';
                        continue;
                    }
                }
            }
        } else { //input text field
            if (id.indexOf('txtJobTitle') >= 0) {
                if (control.attr('emp_required') == 'true') {
                    if (control.val() == '') {
                        errorMessage += 'Please enter the ' + control.attr('title') + ' field. <br/>';
                        continue;
                    }
                }
            }
        }//end isOccupationListEmpty()
        if (id.indexOf('txtEmployer') >= 0) {
            if (control.attr('emp_required') == 'true') {
                if (control.val() == '') {
                    errorMessage += 'Please enter the ' + control.attr('title') + ' field. <br/>';
                    continue;
                }
            }
        }
        if (id.indexOf('txtBusinessType') >= 0) {
            if (control.attr('emp_required') == 'true') {
                if (control.val() == '') {
                    errorMessage += 'Please enter the ' + control.attr('title') + ' field. <br/>';
                    continue;
                }
            }
        }
        if (id.indexOf('ddlBranchOfService') >= 0) {
            if (control.attr('emp_required') == 'true') {
                if (control.val() == '') {
                    errorMessage += 'Please select the ' + control.attr('title') + ' field. <br/>';
                    continue;
                }
            }
        }
        if (id.indexOf('ddlPayGrade') >= 0) {
            if (control.attr('emp_required') == 'true') {
                if (control.val() == '') {
                    errorMessage += 'Please select the ' + control.attr('title') + ' field. <br/>';
                    continue;
                }
            }
        }
    }

    if ((needToRequireEmployedDuration && (yearEmployedDuration * 12 + monthEmployedDuration * 1) <= 0) ||
        (yearEmployedDuration * 12 + monthEmployedDuration * 1) > 9999) {
        errorMessage += 'Please correct the ' + titleEmployedDuration + ' field. <br/>';
    }

    if ((needToRequireProfessionDuration && (yearProfessionDuration * 12 + monthProfessionDuration * 1) <= 0) ||
        (yearProfessionDuration * 12 + monthProfessionDuration * 1) > 9999) {
        errorMessage += 'Please correct the ' + titleProfessionDuration + ' field. <br/>';
    }

    return errorMessage;
}
EMPLogic.ReplaceButtonLabel = function (html) {
	//don't need to do this anymore, performRename function will do this at the end
    /*if (typeof BUTTONLABELLIST == 'undefined' ||  BUTTONLABELLIST == null)
        return html;

    var key = EXTRACT_BUTTONLABEL_REGEX.exec(html);
    if (key != null && key.length > 0) {
        var value = BUTTONLABELLIST[$.trim(key[0]).toLowerCase()];
        if (typeof value == "string" && $.trim(value) !== "") {
            return html.replace(EXTRACT_BUTTONLABEL_REGEX, value);
        }
    }*/   
	return html;
}

EMPLogic.refreshHtml = function () {
	var prefix = this.PrefixID;
    this.PreVisibleIDs = this.VisibleIDs.slice(0);
    this.VisibleIDs = [];

    var finalHtml = "";
    var selectedStatus = $('#' + this.StatusID).val();
    if ($.trim(selectedStatus) === "") {
		$('#' + this.RestID).html(finalHtml);
		$('#' + this.ParentID).trigger('create');
	    return;
    }
    var jobTitleHtml = "";
    var strJobTitle = "Profession/Job Title";
    if (selectedStatus == this.EStatus.ACTIVE_MILITARY) {
    	strJobTitle = "Profession/MOS";
    } else if (selectedStatus == this.EStatus.RETIRED_MILITARY) {
    	strJobTitle = "Former Profession/MOS";
    } else if (selectedStatus == this.EStatus.RETIRED || selectedStatus == this.EStatus.UNEMPLOYED) {
        strJobTitle = "Former Profession/Job Title";
    }
        /* now Profesion/job Title should always be required no matter the employment field selection 
        else if (selectedStatus == this.EStatus.RETIRED || selectedStatus == this.EStatus.STUDENT || selectedStatus == this.EStatus.HOMEMAKER)
        jobTitleHtml = this.buildTextBox("Profession/Job Title", "txtJobTitle", "", false);*/
    //render dropdown for Job Title field if it has occupationlist, otherwise rendering text field 
    //if (selectedStatus == this.EStatus.STUDENT || selectedStatus == this.EStatus.RETIRED || selectedStatus == this.EStatus.UNEMPLOYED || selectedStatus == this.EStatus.HOMEMAKER) {
	  //  jobTitleHtml = "";
    //} else
    if (!this.isOccupationListEmpty()) {
        jobTitleHtml = this.buildDropDownOccupationList(strJobTitle, "txtJobTitle", "", OCCUPATIONLIST, true);
    } else {
        jobTitleHtml = this.buildTextBox(strJobTitle, "txtJobTitle", "", true);
    }
    finalHtml += jobTitleHtml;
    if (this.Mode != "JobTitleOnly" || selectedStatus == this.EStatus.OWNER) {
		var employerHtml = "";
		var branchOfServiceArr = ["", "Air Force", "Army", "Coast Guard", "Marine Corps", "Navy"];  //, "ORS", "CIA",

		if (selectedStatus == this.EStatus.STUDENT || selectedStatus == this.EStatus.RETIRED || selectedStatus == this.EStatus.UNEMPLOYED || selectedStatus == this.EStatus.HOMEMAKER) {
    		employerHtml = "";
		} else if (selectedStatus == this.EStatus.ACTIVE_MILITARY)
			employerHtml = this.buildDropDown("Branch of Service", "ddlBranchOfService", "", branchOfServiceArr, true);
		else if (selectedStatus == this.EStatus.RETIRED_MILITARY)
			employerHtml = this.buildDropDown("Branch of Service", "ddlBranchOfService", "", branchOfServiceArr, false);
		else if (selectedStatus == this.EStatus.GOVERNMENT_DOD || selectedStatus == this.EStatus.EMPLOYED)
			employerHtml = this.buildTextBox("Employer", "txtEmployer", "", true);
		else
			employerHtml = this.buildTextBox("Employer", "txtEmployer", "", false);
		finalHtml += employerHtml;
		var payGradeHtml = "";
		var militaryArr = ["", "E1", "E2", "E3", "E4", "E5", "E6", "E7", "E8", "E9",
										"O1", "O1E", "O2", "O2E", "O3", "O3E",
										"O4", "O5", "O6", "O7", "O8", "O9", "O10", "W1", "W2", "W3", "W4", "W5"];
		var govermentArr = ["", "GS1", "GS2", "GS3", "GS4", "GS5", "GS6", "GS7", "GS8", "GS9", "GS10", "GS11", "GS12", "GS13", "GS14", "GS15"];
		var focusPayGrade = false;
		if (selectedStatus == this.EStatus.ACTIVE_MILITARY) {
			payGradeHtml += this.buildDropDown("Pay Grade", "ddlPayGrade", "", militaryArr, true);
			focusPayGrade = true;
		}
		else if (selectedStatus == this.EStatus.RETIRED_MILITARY) {
			payGradeHtml += this.buildDropDown("Pay Grade", "ddlPayGrade", "", militaryArr, false);
			focusPayGrade = true;
		}
		else if (selectedStatus == this.EStatus.GOVERNMENT_DOD) {
			payGradeHtml += this.buildDropDown("Pay Grade", "ddlPayGrade", "", govermentArr, true);
			focusPayGrade = true;
		}
		finalHtml += payGradeHtml;

		//    var supervisorHtml = this.buildTextBox("Supervisor Name", "txtSupervisorName", "", false);
		//    finalHtml += supervisorHtml;

		var businessTypeHtml = "";
		/* business type for employee is required 
		if (selectedStatus == this.EStatus.OWNER || selectedStatus == this.EStatus.SELF_EMPLOYED)
			businessTypeHtml = this.buildTextBox("Business Type", "txtBusinessType", "", false);*/
		if (selectedStatus == this.EStatus.OWNER) {
			businessTypeHtml = this.buildTextBox("Business Type", "txtBusinessType", "", false);
		} else if (selectedStatus == this.EStatus.SELF_EMPLOYED) {
			businessTypeHtml = this.buildTextBox("Business Type", "txtBusinessType", "", true);
		}

		finalHtml += businessTypeHtml;

		//    var employmentDateHtml = "";
		//    if (selectedStatus == this.EStatus.ACTIVE_MILITARY)
		//        employmentDateHtml = this.buildDateTime("Enlistment Date<span style ='color :Red'>*</span>", "txtEmploymentStartDate", "", true);
		//    else if (selectedStatus == this.EStatus.GOVERNMENT_DOD || selectedStatus == this.EStatus.RETIRED_MILITARY)
		//        employmentDateHtml = this.buildDateTime("Enlistment Date<span style ='color :Red'>*</span>", "txtEmploymentStartDate", "", false);
		//    else
		//        employmentDateHtml = this.buildDateTime("Employment Start Date <span style='color:red'>*</span>", "txtEmploymentStartDate", "", false);
		//    finalHtml += employmentDateHtml;

		var employmentDurationHtml = "";
		//only apply for RETIRED and UNEMPLOYED
		var employmentDurationRequired = false;
		if ($("#hdEmploymentDurationRequired").length === 1 && $("#hdEmploymentDurationRequired").val().toUpperCase() === "Y") {
			employmentDurationRequired = true;
		}
		if (selectedStatus == this.EStatus.STUDENT || selectedStatus == this.EStatus.HOMEMAKER) {
			employerHtml = "";
		} else if (selectedStatus == this.EStatus.RETIRED) {
			employmentDurationHtml += this.buildDuration("Retired Duration", "txtEmployedDuration", "", employmentDurationRequired);
		} else if (selectedStatus == this.EStatus.UNEMPLOYED) {
			employmentDurationHtml += this.buildDuration("Unemployment Duration", "txtEmployedDuration", "", employmentDurationRequired);
		} else if (selectedStatus == this.EStatus.ACTIVE_MILITARY)
			employmentDurationHtml += this.buildDuration("Time in Service", "txtEmployedDuration", "", true);
		else if (selectedStatus == this.EStatus.EMPLOYED || selectedStatus == this.EStatus.OWNER || selectedStatus == this.EStatus.SELF_EMPLOYED)
			employmentDurationHtml += this.buildDuration("Employment Duration", "txtEmployedDuration", "", true);
		else
			employmentDurationHtml += this.buildDuration("Employment Duration", "txtEmployedDuration", "", false);
		finalHtml += employmentDurationHtml;

		//    var professionDurationHtml = this.buildDuration("Profession Duration", "txtProfessionDuration", "", false);
		//    finalHtml += professionDurationHtml;

		var etsHtml = "";
		var hasETS = false;
		var payGradeControl = $('#' + this.PrefixID + "ddlPayGrade");
		if (payGradeControl.length > 0) {
			var selectedPayGrade = payGradeControl.val();
			var payGradeMatchedETSArr = ["E1", "E2", "E3", "E4", "E5", "E6", "E7", "E8", "E9"];
			if (selectedStatus == this.EStatus.ACTIVE_MILITARY && payGradeMatchedETSArr.indexOf(selectedPayGrade) >= 0) {
				etsHtml = this.buildDateTime2("ETS/Expiration, Term of Service<em>(mm/dd/yyyy)</em>", "txtETS", "", false);
				hasETS = true;
			}
		}
		finalHtml += etsHtml;
		
	}
    var storePreData = {};
    for (var _i = 0; _i < this.PreVisibleIDs.length; _i++) {
    	// save to dict
    	var obj = $('#' + this.PreVisibleIDs[_i]);
    	storePreData[this.PreVisibleIDs[_i]] = obj.val();
    	// remove old html
    	obj.remove();
    }

    if (this.PreVisibleIDs.length === 0 && prefix === "") { //for the first time, pull data from LinkedIn (if any) and prefill to corresponding field
    	storePreData["txtJobTitle"] = $("#hd_linkedin_title").val();
    	storePreData["txtEmployer"] = $("#hd_linkedin_companyname").val();
    }
    $('#' + this.RestID).html(finalHtml);
    
    $('#' + this.RestID + " select").each(function (idx, srcEle) {
    	$(srcEle).on("change", function () {
    		setTimeout(function () {
    			$(srcEle).focus();
    		}, 100);
    	});
    });
    
    //init for ETS
    if (hasETS === true) {
        $('#' + prefix + 'txtETS_divEmpDOBStuff div.ui-input-date input').on('focusout', function () {
            var maxlength = parseInt($(this).attr('maxlength'));
            // add 0 if value < 10
            $(this).val(padLeft($(this).val(), maxlength));

            var month = padLeft($("#"+prefix+"txtETS_txtDOB1").val(), 2);
            var day = padLeft($("#" + prefix + "txtETS_txtDOB2").val(), 2);
            var year = padLeft($("#" + prefix + "txtETS_txtDOB3").val(), 4);

            var date = month + '/' + day + '/' + year;
            $("#" + prefix + "txtETS").val(date == "//" ? "" : date);
        });
        $("#" + prefix + "txtETS").datepicker({
            showOn: "button",
            changeMonth: true,
            changeYear: true,
            yearRange: "-0:+50",
            buttonImage: "../images/calendar.png",
            buttonImageOnly: true,
            onSelect: function (value, inst) {
                var date = $(this).datepicker('getDate');

                $("#" + prefix + "txtETS_txtDOB2").val(padLeft(date.getDate(), 2));
                $("#" + prefix + "txtETS_txtDOB1").val(padLeft(date.getMonth() + 1, 2));
                $("#" + prefix + "txtETS_txtDOB3").val(padLeft(date.getFullYear(), 4));

                $("#" + prefix + "txtETS").datepicker("hide");
                $("#" + prefix + "txtETS_txtDOB1").trigger("focus").trigger("blur");
            }
        });
    }

    $.lpqValidate.removeValidationGroup(this.StatusID);
    for (var _i = 0; _i < this.VisibleIDs.length; _i++) {
        // save to dict
    	var value = storePreData[this.VisibleIDs[_i]];
    	var ele = $('#' + this.VisibleIDs[_i]);
        if (value != undefined) {
        	ele.val(value);
        }
        if (this.VisibleIDs[_i].indexOf("EmployedDuration_year") >= 0) continue;
        if (this.VisibleIDs[_i].indexOf("EmployedDuration_month") >= 0) {
        	var containerId = "div" + this.VisibleIDs[_i].replace("_month", "");
        	var month = this.VisibleIDs[_i];
        	var year = this.VisibleIDs[_i].replace("_month", "_year");
	        $("#" + containerId).observer({
        		validators: [
					function (partial) {
						var oDurationYear = $("#"+year).val();
						var oDurationMonth = $("#" + month).val();
						var required = ($("#" + year).attr('emp_required') == 'true') || ($("#" + month).attr('emp_required') == 'true');
						if ((required && (oDurationYear * 12 + oDurationMonth * 1) <= 0) || (oDurationYear * 12 + oDurationMonth * 1) > 9999) {
							return $("#" + month).attr("title") + " is required";
						}
						return "";
					}
        		],
        		validateOnBlur: true,
        		group: this.StatusID
        	});
        }
        if (this.VisibleIDs[_i].indexOf("ProfessionDuration_year") >= 0) continue;
        if (this.VisibleIDs[_i].indexOf("ProfessionDuration_month") >= 0) {
        	var containerId = "div" + this.VisibleIDs[_i].replace("_month", "");
        	var month = this.VisibleIDs[_i];
        	var year = this.VisibleIDs[_i].replace("_month", "_year");
        	$("#" + containerId).observer({
        		validators: [
					function (partial) {
						var oDurationYear = $("#" + year).val();
						var oDurationMonth = $("#" + month).val();
						var required = ($("#" + year).attr('emp_required') == 'true') || ($("#" + month).attr('emp_required') == 'true');
						if ((required && (oDurationYear * 12 + oDurationMonth * 1) <= 0) || (oDurationYear * 12 + oDurationMonth * 1) > 9999) {
							return $("#" + month).attr("title") + " is required";
						}
						return "";
					}
        		],
        		validateOnBlur: true,
        		group: this.StatusID
        	});
	        continue;
        }
        
        if (this.VisibleIDs[_i].indexOf("txtETS_txtDOB1") >= 0) continue;
        if (this.VisibleIDs[_i].indexOf("txtETS_txtDOB2") >= 0) continue;
        if (this.VisibleIDs[_i].indexOf("txtETS_txtDOB3") >= 0) {
        	var containerId = "div" + this.VisibleIDs[_i].replace("_txtDOB3", "");
        	var ets = this.VisibleIDs[_i].replace("_txtDOB3", "");
        	$("#" + containerId).observer({
        		validators: [
					function (partial) {
						var $ETS = $("#" + ets);
						if ($ETS.val() != "") {//ETS/Expiration not required
                            var etsDate = moment($ETS.val(), "MM-DD-YYYY");
                            if (!Common.IsValidDate($ETS.val())) {
								return "ETS/Expiration date is not valid";
							}
							if (prefix.indexOf("prev_") !== -1) {
								if (etsDate.isAfter(moment())) {
									return "ETS/Expiration date must be equal or less than current date";
								} else if (etsDate.isBefore(moment("01-01-1900", "MM-DD-YYYY"))) {
									return "ETS/Expiration date must be greater than 1900";
								}
							} else {
								if (moment().isAfter(etsDate)) {
									return "ETS/Expiration date must be equal or greater than current date";
								} else if (etsDate.isAfter(moment("01-01-2068", "MM-DD-YYYY"))) {
									return "ETS/Expiration date must be less than 2068";
								}
							}
						}
						return "";
					}
        		],
        		validateOnBlur: true,
        		group: this.StatusID
        	});
        	continue;
        }
        if (this.VisibleIDs[_i].indexOf("txtJobTitle") >= 0) {
        	if ($("#" + this.VisibleIDs[_i]).attr("emp_required") != "true") continue;
        	$("#" + this.VisibleIDs[_i]).observer({
        		validators: [
					function (partial) {
						var $ele = $(this);
						if (Common.ValidateText($ele.val()) == false) {
							return $ele.attr("title") + " is required";
						}
						return "";
					}
        		],
        		validateOnBlur: true,
        		group: this.StatusID
        	});
        	continue;
        }
        if (this.VisibleIDs[_i].indexOf("txtEmployer") >= 0 || this.VisibleIDs[_i].indexOf("txtBusinessType") >= 0 || this.VisibleIDs[_i].indexOf("ddlBranchOfService") >= 0 || this.VisibleIDs[_i].indexOf("ddlPayGrade") >= 0) {
        	if ($("#" + this.VisibleIDs[_i]).attr("emp_required") != "true") continue;
        	$("#" + this.VisibleIDs[_i]).observer({
        		validators: [
					function (partial) {
						var $ele = $(this);
						if (Common.ValidateText($ele.val()) == false) {
							return $ele.attr("title") + " is required";
						}
						return "";
					}
        		],
        		validateOnBlur: true,
        		group: this.StatusID
        	});
        	continue;
        }
		
    }

    // Trigger create
    $('#' + this.ParentID).trigger('create');
    indexRenameItems('#' + this.RestID);
    if (Common.GetParameterByName("mode") == "777" && Common.GetParameterByName("feature") == "rename") {
    	attachEditButton('#' + this.RestID);
    }
    performRename('#' + this.RestID);
    // mark datetime
    //$('#' + this.ParentID + ' .emp_indate').mask('99/99/9999');
    
    
    /*TODO - the work later android is not working*/
    if (isMobile.Android()) {
    	$('#' + this.ParentID + ' .emp_indate').blur(function () {
    		$(this).val(Common.FormatDate($(this).val()));
    	});

    //} else {
    	//$('#' + this.ParentID + ' .emp_indate').inputmask('99/99/9999');
    }

    $('#' + this.ParentID + ' .emp_inyear').watermark('years');
    $('#' + this.ParentID + ' .emp_numeric').numeric({ decimal: false });

    payGradeControl = $('#' + this.PrefixID + "ddlPayGrade");
    var empLogic = this;
    if (payGradeControl.length > 0) {
    	payGradeControl.change(function () {
    		empLogic.refreshHtml();
    	});
    	if (focusPayGrade) {
		    payGradeControl.focus();
	    }
    }
        

    //    var prefix = this.PrefixID;
    //    $('#' + this.PrefixID + 'txtEmploymentStartDate').blur(function() {
    //        var val = $(this).val();
    //        if (empLogic.isDate(val)) {
    //             // calculate and update time  in service
    //            var mdate = new Date(val);
    //            var days = parseInt((new Date() - mdate) / (1000 * 60 * 60 * 24), 10);
    //           if ((days > 0) && (days < 40500)) {
    //               //$('#' + prefix + 'txtEmployedDuration_year').val(Math.floor(days / 365));
    //               $('#' + prefix + 'txtEmployedDuration_year').val("" + Math.floor(days / 365) + "").selectmenu("refresh", true);
    //                $('#' + prefix + 'txtEmployedDuration_month').val("" + Math.floor((days % 365)/30) + "").selectmenu("refresh", true);
    //            }
    //        }
    //    });
    
    // Fix change progress indicator text color
    //$("ul.progress-indicator li a").removeClass();





}
/////////////////////////////////
///   END EMPLOYMENT LOGIC    ///
/////////////////////////////////