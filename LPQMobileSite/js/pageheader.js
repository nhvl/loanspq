﻿$(document).on("mobileinit", function () {
	//is to remove hash from url 
	$.mobile.hashListeningEnabled = false;
	$.mobile.pushStateEnabled = true;
	$.mobile.changePage.defaults.changeHash = false;
});
if (!String.prototype.startsWith) {
	String.prototype.startsWith = function (searchString, position) {
		position = position || 0;
		return this.indexOf(searchString, position) === position;
	};
}
// Original JavaScript code by Brian Suda https://24ways.org/2010/calculating-color-contrast/
function getContrastYIQ(bgColor) {

    var r = parseInt(bgColor.substr(0, 2), 16);
    var g = parseInt(bgColor.substr(2, 2), 16);
    var b = parseInt(bgColor.substr(4, 2), 16);
    var yiq = ((r * 299) + (g * 587) + (b * 114)) / 1000;
    //yiq max =255, min =0
    //179 = 70% of max, 128 =50% of max
    return (yiq >= 179) ? 'black' : 'white';
}
//end set default
function applyFooterThemeCss(pageId, buttonFontColor) {
	//read 
	var buttonBgColor = "#52bc71";
	var footerBgColor = "#52bc71";
	var footerTextColor="white";
	var temp;
	//if ($(pageId + " a.div-continue-button").css("background-color") != undefined) {
	//    buttonBgColor = rgbToHex($(pageId + " a.div-continue-button").css("background-color"));
	//}
   

	var footerThemeController = $("div[data-theme-role='footer']", "#divThemeController");
	if (footerThemeController.css("background-color") != undefined) {
		temp = rgbToHex(footerThemeController.css("background-color"));
		if (temp !== "") {
			footerBgColor = temp;
		}
	    //Set default black or white color text
		footerTextColor = getContrastYIQ(footerBgColor.replace("#",""));
	}
	var footerFontThemeController = $("div[data-theme-role='footerFont']", "#divThemeController");
	if (footerFontThemeController.attr("style") != undefined  && footerFontThemeController.css("color") != undefined) {
		temp = rgbToHex(footerFontThemeController.css("color"));
		if (temp !== "" && temp !== footerBgColor) {
			footerTextColor = temp;
		}
	} else {
		footerFontThemeController.css("color", footerTextColor);
	}
	var footerBgColorStart = ColorLuminance(footerBgColor, 0.1);
	var footerBgColorEnd = ColorLuminance(footerBgColor, -0.1);
	if (footerBgColor == "#ffffff") {
		footerBgColorStart = "#ffffff";
		footerBgColorEnd = "#ffffff";
	}

	var buttonThemeController = $("div[data-theme-role='button']", "#divThemeController");
	if (buttonThemeController.css("background-color") != undefined) {
		temp = rgbToHex(buttonThemeController.css("background-color"));
		if (temp !== "") {
			buttonBgColor = temp;
		}
	}


	var buttonBgColorStart = ColorLuminance(buttonBgColor, 0.3);
	var buttonBgColorEnd = ColorLuminance(buttonBgColor, -0.3);
	if (buttonBgColor == "#ffffff") {
		buttonBgColor = "#52bc71";
		buttonBgColorStart = "#86CF8A";
		buttonBgColorEnd = "#86CF8A";
	}
	//apply



	$(pageId + " a.div-continue-button").css("border-color", buttonBgColor);
	$(pageId + " a.div-goback")
		.css('background-color', 'transparent')
		.css("border-color", buttonBgColor)
		.css("color", buttonBgColor);
	$(pageId + " a.div-finish-later")
		.css('background-color', 'transparent')
		.css("border-color", buttonBgColor)
		.css("color", buttonBgColor);

	$(pageId + " a.div-button")
		.css('background-color', 'transparent')
		.css("border-color", buttonBgColor)
		.css("color", buttonBgColor);

	$(pageId + " a.div-button").hover(function () {
		$(this).css('background-color', buttonBgColor).css('color', buttonFontColor);
	});

	$(pageId + " a.div-button").mouseleave(function () {
		$(this).css('background-color', 'transparent').css('color', buttonBgColor);
	});

	$(pageId + " a.div-goback:not(.abort-hover)").hover(function () {
		$(this).css('background-color', buttonBgColor).css('color', buttonFontColor);
	});
	$(pageId + " a.div-finish-later:not(.abort-hover)").hover(function () {
		$(this).css('background-color', buttonBgColor).css('color', buttonFontColor);
	});

	$(pageId + " a.div-goback:not(.abort-hover)").mouseleave(function () {
		$(this).css('background-color', 'transparent').css('color', buttonBgColor);
	});
	$(pageId + " a.div-finish-later:not(.abort-hover)").mouseleave(function () {
		$(this).css('background-color', 'transparent').css('color', buttonBgColor);
	});

	if ($("#footerThemeCssPlaceHolder").length == 0) {
		var style = document.createElement('style');
		style.setAttribute("id", "footerThemeCssPlaceHolder");
		var cssClasses = ".footer-theme.btn {border-color:" + buttonBgColor + "!important;background-color: " + buttonBgColor + " !important;}";
		cssClasses += "a.ui-btn[type='button']{background:" + buttonBgColor + "!important;border-color:" + buttonBgColor + "!important;color:" + buttonFontColor + ";}";
		cssClasses += "a.ui-btn.div-continue-button-reverse[type='button']{background:" + buttonFontColor + "!important;border-color:" + buttonBgColor + "!important;color:" + buttonBgColor + ";}";
		cssClasses += "a.ui-btn.div-continue-button-reverse[type='button']:hover{color:" + buttonFontColor + ";}";
		cssClasses += "a.ui-btn[type='button']:hover{border: 1px solid " + buttonBgColor + "!important;background-image: -webkit-gradient(linear, left top, left bottom, from( " + buttonBgColorStart + "), to( " + buttonBgColorEnd + "))!important;background-image: -webkit-linear-gradient( " + buttonBgColorStart + ", " + buttonBgColorEnd + ")!important;background-image:-moz-linear-gradient( " + buttonBgColorStart + ", " + buttonBgColorEnd + ")!important; background-image:-ms-linear-gradient( " + buttonBgColorStart + ", " + buttonBgColorEnd + ")!important;background-image:-o-linear-gradient( " + buttonBgColorStart + ", " + buttonBgColorEnd + ")!important;background-image:linear-gradient( " + buttonBgColorStart + ", " + buttonBgColorEnd + ")!important;}";
		cssClasses += "a.ui-btn[type='button']:focus{-webkit-box-shadow: 0 0 8px 4px " + buttonBgColorEnd + "!important;-moz-box-shadow: 0 0 8px 4px " + buttonBgColorEnd + "!important;box-shadow: 0 0 8px 4px " + buttonBgColorEnd + "!important;}";

		cssClasses += "a.ui-btn.div-continue-button-reverse[type='button']:focus{-webkit-box-shadow: 0 0 12px " + buttonBgColor + "!important;-moz-box-shadow: 0 0 12px " + buttonBgColor + "!important;box-shadow: 0 0 12px " + buttonBgColor + "!important;outline:none;}";

		cssClasses += ".divfooter{background-image: -webkit-gradient(linear, left top, left bottom, from( " + footerBgColorStart + "), to( " + footerBgColorEnd + "))!important;background-image: -webkit-linear-gradient( " + footerBgColorStart + ", " + footerBgColorEnd + ")!important;background-image:-moz-linear-gradient( " + footerBgColorStart + ", " + footerBgColorEnd + ")!important; background-image:-ms-linear-gradient( " + footerBgColorStart + ", " + footerBgColorEnd + ")!important;background-image:-o-linear-gradient( " + footerBgColorStart + ", " + footerBgColorEnd + ")!important;background-image:linear-gradient( " + footerBgColorStart + ", " + footerBgColorEnd + ")!important;}";
		cssClasses += ".ui-checkbox-on:after{background-color:" + buttonBgColor + ";}";
		//cssClasses += ".ui-radio .ui-radio-on:after{background-color:#fff;border-color:" + footerBgColor + "!important;}";
		cssClasses += ".ui-radio.ui-mini .ui-radio-on{background-color:" + footerBgColor + "!important;color:#ffffff;border:1px solid " + footerBgColorEnd + "!important;}";
		cssClasses += "a.div-goback:focus,a.div-finish-later:focus,a.div-button:focus,.footer-theme.btn:focus{-webkit-box-shadow: 0 0 12px " + buttonBgColor + "!important;-moz-box-shadow: 0 0 12px " + buttonBgColor + "!important;box-shadow: 0 0 12px " + buttonBgColor + "!important;outline:none;display:inline-block;}";
		cssClasses += "a.div-goback.abort-hover:focus,a.div-finish-later.abort-hover:focus{-webkit-box-shadow: none!important;-moz-box-shadow: none!important;box-shadow: none!important;outline:none;display:inline;}";
		//if (footerBgColor === "#ffffff") {
	        //cssClasses += ".divfooter .divfooter_top, .divfooter .divLenderName, .divfooter a, .divfooter span{color:gray!important; font-weight:bold;}";
	    //}
		
		cssClasses += ".divfooter .divfooter_top, .divfooter .divLenderName, .divfooter a, .divfooter span{color:" + footerTextColor + "!important; font-weight:normal;}";

		style.type = 'text/css';
		style.innerHTML = cssClasses;
		style.id = "styleFooterTheme";
		if (document.getElementById("styleFooterTheme") != null) {
			document.getElementById("styleFooterTheme").parentNode.removeChild(document.getElementById("styleFooterTheme"));
		}
		document.getElementsByTagName('head')[0].appendChild(style);
	}
}
function applyHeaderThemeCss(pageId) {
	var headerThemeColor = "#565e96";
	var headerThemeFont = "'Roboto-light', sans-serif";
	var headerTheme2Color = "#52bc71";
	var headerTheme2Font = "'Roboto-light', sans-serif";
	var temp;
	
	// header_theme
	//if ($(pageId + ' .divHeaderTheme').css('backgroundColor') != undefined) {
	//    headerThemeColor = rgbToHex($(pageId + ' .divHeaderTheme').css('backgroundColor'));
	//    if (headerThemeColor == "") {
	//        headerThemeColor = "#565e96";
	//    }
	//    //headerThemeFont = $(pageId + ' .divHeaderTheme').css('font-family');
	//}

	var headerThemeController = $("div[data-theme-role='header']", "#divThemeController");
	if (headerThemeController.css('backgroundColor') != undefined) {
		temp = rgbToHex(headerThemeController.css("background-color"));
		if (temp !== "") {
			headerThemeColor = temp;
		}
		//headerThemeFont = $(pageId + ' .divHeaderTheme').css('font-family');
	}
	var headerThemeColorStart = ColorLuminance(headerThemeColor, 0.3);
	var headerThemeColorEnd = ColorLuminance(headerThemeColor, -0.3);


	// header_theme2
	//if ($(pageId + ' .divHeaderTheme2').css('backgroundColor') != undefined) {
	//    headerTheme2Color = rgbToHex($(pageId + ' .divHeaderTheme2').css('backgroundColor'));
	//    if (headerTheme2Color == "") {
	//        headerTheme2Color = "#52bc71";
	//    }
	//    //headerTheme2Font = $(pageId + ' .divHeaderTheme2').css('font-family');
	//}

	var header2ThemeController = $("div[data-theme-role='header2']", "#divThemeController");
	if (header2ThemeController.css('backgroundColor') != undefined) {
		temp = rgbToHex(header2ThemeController.css("background-color"));
		if (temp !== "") {
			headerTheme2Color = temp;
		}
		//headerThemeFont = $(pageId + ' .divHeaderTheme').css('font-family');
	}

	var contentBgColor = "#ffffff";
	var contentThemeController = $("div[data-theme-role='content']", "#divThemeController");
	if (contentThemeController.css("background-color") != undefined) {
		temp = rgbToHex(contentThemeController.css("background-color"));
		if (temp !== "") {
			contentBgColor = temp;
		}
	}

	//apply style for header SVG 
	// color

	$('.header_theme svg').css({ fill: headerThemeColor });
	$('.header-theme-stroke').css({ stroke: headerThemeColor });
	$('.header_theme2 svg').css({ fill: headerTheme2Color });
	$('.header_theme .steps svg').css({ fill: '#E9EBEB', stroke: '#E5E4E2;' });
	$('.header_theme svg .shape').css({ stroke: headerTheme2Color });
	$('.header_theme svg circle.active').css({ fill: headerTheme2Color });
	$('.header_theme svg path.active').css({ fill: headerTheme2Color });
	$('.header_theme2 svg .typo.inactive').css({ fill: '#ffffff' });
	$('.heading-title').css({ color: headerThemeColor });
	//$('.heading2-title').css({ color: headerTheme2Color });
	$('.ProductCQ-Font').css({ color: headerThemeColor });
	//$('.cqTitle').css({ color: headerThemeColor });
	$('.header_theme2').css({ color: headerTheme2Color });
	$('.header_theme').css({ color: headerThemeColor });

	if (pageId != "#popupPDFViewer") {
		$('.close-button').css({ fill: headerTheme2Color });
	}

	// font
	if (headerThemeFont != "") {
		$('.sub_section_header').css("font-family", headerThemeFont);
		$('.sub_section_header').css("font-weight", "lighter");
	}
	if (headerTheme2Font != "") {
		$('.page_title').css("font-family", headerTheme2Font);
		$('.page_title').css("font-weight", "lighter");
	}
	if (pageId == '#reviewpage' || pageId == '#finalreviewpage' || pageId == "#pagesubmit") {
		//if ($(pageId + ' .divHeaderTheme').css('backgroundColor') != undefined) {
		//    headerThemeColor = rgbToHex($(pageId + ' .divHeaderTheme').css('backgroundColor'));
		//    if (headerThemeColor == "") {
		//        headerThemeColor = "#575F97";
		//    }
		//}
		$(".review-container .row-title").css("color", headerThemeColor);
		$(".review-container .group-heading").css("color", headerThemeColor);
	}
	//apply style for funding options in pageFS
	//only do this one time
	if ($("#headerThemeCssPlaceHolder").length == 0) {
		var style = document.createElement('style');
		style.setAttribute("id", "headerThemeCssPlaceHolder");
		var cssClasses = ".btn-header-theme, .btn-header-theme-ext {border:1px " + headerTheme2Color + " solid !important; color: " + headerTheme2Color + " !important;text-align:left;padding-left:10px;background:none!important;background-color: " + contentBgColor + "!important; text-shadow: none !important;}";
		cssClasses += ".btn-header-theme:focus, .btn-header-theme.ui-link:focus, .btn-header-theme-ext:focus, .btn-header-theme-ext.ui-link:focus, a.ui-link.header_theme2:focus, .ui-focus,a.ui-link.img-btn:focus, input[type='image']:focus, select.ddlUploadDoc-style:focus,.header-theme-checkbox:focus,.selected-flag:focus, .focus-able:focus{-webkit-box-shadow: 0 0 12px " + headerTheme2Color + "!important;-moz-box-shadow: 0 0 12px " + headerTheme2Color + "!important;box-shadow: 0 0 12px " + headerTheme2Color + "!important;outline:none;}";
		cssClasses += "label.ui-focus.ui-radio-on,label.ui-focus.ui-radio-off{-webkit-box-shadow: 0 0 12px " + headerThemeColor + "!important;-moz-box-shadow: 0 0 12px " + headerThemeColor + "!important;box-shadow: 0 0 12px " + headerThemeColor + "!important;outline:none;}";
		cssClasses += "a.ui-link.svg-btn:focus, div[data-command='close']:focus{opacity:0.5;-ms-opacity:0.5;outline:none;}";
		cssClasses += ".btn-header-theme-ext .btn-add{ width:27px; height: 27px; position:absolute; top:50%; right:0%; transform: translate(-50%, -50%); text-align: center; font-size: 1.2em;}";

        //Temp, signifier may be too strong so remove the shadow box
		//cssClasses += "a.shadow-btn, span.shadow-btn{padding: 4px; -webkit-box-shadow: 0 0 12px " + headerTheme2Color + "!important;-moz-box-shadow: 0 0 12px " + headerTheme2Color + "!important;box-shadow: 0 0 12px " + headerTheme2Color + "!important; text-indent:-9px;display: inline-block; padding-left:25px;cursor: pointer;font-weight :bold;}";
	    //cssClasses += "span.shadow-btn{padding: 0 4px; line-height:1.4em; font-size:0.9em; font-weight:bold; outline:none; text-indent:0px;}";

		//cssClasses += "a:not(.btn-header-theme):focus{opacity:0.5;}";
		if (!isMobile.any()) {
			//cssClasses += ".btn-header-theme:hover {border:2px " + headerTheme2Color + " solid !important; color: #ffffff !important ;background-image:none !important;background-color: " + headerTheme2Color + " !important; text-shadow: none !important;-ms-opacity: 0.6; opacity: 0.6;}";
			cssClasses += ".btn-header-theme.btnHover, .btn-header-theme.hover-effect:hover{border:1px " + headerTheme2Color + " solid !important; color: #fff !important;background-image:none !important;padding-left:10px !important;background-color: " + headerTheme2Color + " !important; text-shadow: none !important;-ms-opacity: 0.8; opacity: 0.8;}";
			cssClasses += ".btn-header-theme.btnActive_on {border:1px " + headerTheme2Color + " solid !important; color: #ffffff !important;background-image:none !important ;padding-left:10px !important;background-color: " + headerTheme2Color + " !important; text-shadow: none !important;}";
			cssClasses += ".btn-header-theme.btnActive_off {border:1px " + headerTheme2Color + " solid !important; color: " + headerTheme2Color + " !important;text-align:left;padding-left:10px !important;background-color: " + contentBgColor + "!important; text-shadow: none !important ;}";
		}

		cssClasses += ".declaration-wrapper .ui-radio.ui-mini>label.ui-radio-on{background-color: " + headerTheme2Color + " !important; border-color: " + headerTheme2Color + " !important;}";

		//TODO: need to add one for link btn
		cssClasses += ".btn-header-theme.active {border:1px " + headerTheme2Color + " solid !important; color: #ffffff !important;background-image:none!important;background-color: " + headerTheme2Color + "!important; text-shadow: none !important;}";
		cssClasses += ".label-heading-theme {border:2px " + headerTheme2Color + " solid !important; color: " + headerTheme2Color + " !important;text-align:left;padding-left:60px;background-color: " + contentBgColor + " !important; text-shadow: none !important;}";
		cssClasses += ".label-heading-theme.active {border:2px " + headerTheme2Color + " solid !important; color: #ffffff !important;background-image:none!important;background-color: " + headerTheme2Color + "!important; text-shadow: none !important;}";
		cssClasses += ".scandocs-box .caption-text {color: " + headerTheme2Color + " !important;}";
		//cssClasses += ".scandocs-box .caption-text:hover {color: " + headerTheme2Color + " !important;}";
		cssClasses += ".scandocs-box.svg-img svg {fill: " + headerTheme2Color + ";}";
		cssClasses += ".uploaddocs-box .caption-text {color: " + headerTheme2Color + " !important;}";
		//cssClasses += ".uploaddocs-box .caption-text:hover {color: " + headerTheme2Color + " !important;}";
		cssClasses += ".uploaddocs-box.svg-img svg {fill: " + headerTheme2Color + ";}";
		cssClasses += ".header_theme2 svg {fill: " + headerTheme2Color + ";}";

		cssClasses += ".account-role-item-wrapper {position: relative;margin-bottom: 6px;}";
		cssClasses += ".header-theme-dropdown-btn .ui-btn, .xsell-selection-panel .ui-btn {color: " + headerThemeColor + " !important;background-color: " + contentBgColor + " !important; text-shadow: none !important;}";
		cssClasses += ".service-item, .account-role-item, .role-attr-item {color: " + headerThemeColor + " !important;text-align:left;padding-left:10px;background:none!important;background-color: " + contentBgColor + "!important; text-shadow: none !important; cursor:default;}";
		cssClasses += ".account-role-item-wrapper .role-attr-item label.custom-checkbox>span {border: 1px solid " + headerTheme2Color + ";}";
		cssClasses += ".account-role-item-wrapper .role-attr-item label.custom-checkbox input:focus+span {outline:none;-webkit-box-shadow: 0 0 12px " + headerTheme2Color + "!important;-moz-box-shadow: 0 0 12px " + headerTheme2Color + "!important;box-shadow: 0 0 12px " + headerTheme2Color + "!important;outline:none;}";
		cssClasses += ".account-role-item-wrapper .role-attr-item label.custom-checkbox > input[type='checkbox']:checked + span:after{background: " + headerTheme2Color + ";background-image: url('data:image/svg+xml;charset=US-ASCII,%3C%3Fxml%20version%3D%221.0%22%20encoding%3D%22iso-8859-1%22%3F%3E%3C!DOCTYPE%20svg%20PUBLIC%20%22-%2F%2FW3C%2F%2FDTD%20SVG%201.1%2F%2FEN%22%20%22http%3A%2F%2Fwww.w3.org%2FGraphics%2FSVG%2F1.1%2FDTD%2Fsvg11.dtd%22%3E%3Csvg%20version%3D%221.1%22%20id%3D%22Layer_1%22%20xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%20xmlns%3Axlink%3D%22http%3A%2F%2Fwww.w3.org%2F1999%2Fxlink%22%20x%3D%220px%22%20y%3D%220px%22%20%20width%3D%2214px%22%20height%3D%2214px%22%20viewBox%3D%220%200%2014%2014%22%20style%3D%22enable-background%3Anew%200%200%2014%2014%3B%22%20xml%3Aspace%3D%22preserve%22%3E%3Cpolygon%20style%3D%22fill%3A%23FFFFFF%3B%22%20points%3D%2214%2C4%2011%2C1%205.003%2C6.997%203%2C5%200%2C8%204.966%2C13%204.983%2C12.982%205%2C13%20%22%2F%3E%3C%2Fsvg%3E');background-position: center center;background-repeat: no-repeat;}";
		cssClasses += ".account-role-item-wrapper div.btn-remove, {width:27px; height: 27px; position: absolute;top: 50%;right: 14px;transform: translate(0, -50%);background-image: url(\"data:image/svg+xml,%3csvg xmlns='http://www.w3.org/2000/svg' viewBox='-3 -4 55 55'%3e%3cpath d='M41.167,7.228C31.904-2.253,16.71-2.431,7.229,6.832S-2.43,31.289,6.833,40.771   c9.263,9.48,24.458,9.658,33.939,0.395C50.253,31.904,50.43,16.709,41.167,7.228z M37.888,38.174   c-7.885,7.703-20.521,7.555-28.224-0.33C1.96,29.959,2.108,17.323,9.993,9.62c7.885-7.703,20.522-7.555,28.224,0.33   C45.921,17.834,45.772,30.471,37.888,38.174z' fill='" + headerTheme2Color.replace("#", "%23") + "'/%3e%3cpath d='M31.71,17.537l-1.103-1.129c-0.825-0.845-1.729-0.101-1.951,0.108l-4.619,4.513l-4.59-4.709   c-0.336-0.307-1.104-0.815-1.9-0.04l-1.13,1.102c-0.845,0.824-0.103,1.729,0.106,1.952l4.511,4.629l-4.726,4.617   c-0.873,0.852-0.289,1.598-0.104,1.793l1.264,1.295c0.177,0.172,0.9,0.768,1.773-0.086l4.722-4.613l4.608,4.729 c0.851,0.873,1.597,0.291,1.794,0.105l1.295-1.262c0.174-0.176,0.769-0.9-0.083-1.773l-4.611-4.731l4.707-4.599 C31.979,19.101,32.486,18.332,31.71,17.537z' fill='" + headerTheme2Color.replace("#", "%23") + "'/%3e%3c/svg%3e\")!important;border-color:" + headerTheme2Color + "!important;}";


		cssClasses += ".product-selection-panel-v1 .pool-item-option{text-align:left;padding-left:10px;background:none!important;background-color: " + contentBgColor + "!important; text-shadow: none !important; cursor:default;word-wrap: break-word;}";
		cssClasses += ".product-selection-panel-v1 .pool-item-option-wrapper .service-item label.custom-checkbox>span {border: 1px solid " + headerTheme2Color + ";}";
		cssClasses += ".product-selection-panel-v1 .pool-item-option-wrapper .service-item label.custom-checkbox input:focus+span{outline:none;-webkit-box-shadow: 0 0 12px " + headerTheme2Color + "!important;-moz-box-shadow: 0 0 12px " + headerTheme2Color + "!important;box-shadow: 0 0 12px " + headerTheme2Color + "!important;outline:none;}";
		cssClasses += ".product-selection-panel-v1 .pool-item-option-wrapper .service-item label.custom-checkbox > input[type='checkbox']:checked + span:after{background: " + headerTheme2Color + ";background-image: url('data:image/svg+xml;charset=US-ASCII,%3C%3Fxml%20version%3D%221.0%22%20encoding%3D%22iso-8859-1%22%3F%3E%3C!DOCTYPE%20svg%20PUBLIC%20%22-%2F%2FW3C%2F%2FDTD%20SVG%201.1%2F%2FEN%22%20%22http%3A%2F%2Fwww.w3.org%2FGraphics%2FSVG%2F1.1%2FDTD%2Fsvg11.dtd%22%3E%3Csvg%20version%3D%221.1%22%20id%3D%22Layer_1%22%20xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%20xmlns%3Axlink%3D%22http%3A%2F%2Fwww.w3.org%2F1999%2Fxlink%22%20x%3D%220px%22%20y%3D%220px%22%20%20width%3D%2214px%22%20height%3D%2214px%22%20viewBox%3D%220%200%2014%2014%22%20style%3D%22enable-background%3Anew%200%200%2014%2014%3B%22%20xml%3Aspace%3D%22preserve%22%3E%3Cpolygon%20style%3D%22fill%3A%23FFFFFF%3B%22%20points%3D%2214%2C4%2011%2C1%205.003%2C6.997%203%2C5%200%2C8%204.966%2C13%204.983%2C12.982%205%2C13%20%22%2F%3E%3C%2Fsvg%3E');background-position: center center;background-repeat: no-repeat;}";
		cssClasses += ".product-selection-panel-v1 .pool-item-option-wrapper div.right-btn{width:30px; height: 30px; position: absolute;top:calc(50% - 15px);right: 14px;font-size: 2em;cursor: pointer; color: " + headerTheme2Color + "}";
		cssClasses += ".product-selection-panel-v1 .pool-item-option-wrapper div.right-btn.btn-add>span{background-image: url('data:image/svg+xml,%3C%3Fxml%20version%3D%221.0%22%20%3F%3E%3Csvg%20height%3D%2230px%22%20version%3D%221.1%22%20viewBox%3D%220%200%2016%2016%22%20width%3D%2230px%22%20xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%20xmlns%3Asketch%3D%22http%3A%2F%2Fwww.bohemiancoding.com%2Fsketch%2Fns%22%20xmlns%3Axlink%3D%22http%3A%2F%2Fwww.w3.org%2F1999%2Fxlink%22%3E%3Ctitle%2F%3E%3Cdefs%2F%3E%3Cg%20fill%3D%22none%22%20fill-rule%3D%22evenodd%22%20id%3D%22Icons%20with%20numbers%22%20stroke%3D%22none%22%20stroke-width%3D%221%22%3E%3Cg%20fill%3D%22" + headerTheme2Color.replace("#", "%23") + "%22%20id%3D%22Group%22%20transform%3D%22translate%280.000000%2C%20-528.000000%29%22%3E%3Cpath%20d%3D%22M4%2C535%20L4%2C537%20L7%2C537%20L7%2C540%20L9%2C540%20L9%2C537%20L12%2C537%20L12%2C535%20L9%2C535%20L9%2C532%20L7%2C532%20L7%2C535%20Z%20M8%2C544%20C3.58172178%2C544%200%2C540.418278%200%2C536%20C0%2C531.581722%203.58172178%2C528%208%2C528%20C12.4182782%2C528%2016%2C531.581722%2016%2C536%20C16%2C540.418278%2012.4182782%2C544%208%2C544%20Z%20M8%2C544%22%20id%3D%22Oval%20210%20copy%22%2F%3E%3C%2Fg%3E%3C%2Fg%3E%3C%2Fsvg%3E%0D%0A%0D%0A');}";
		cssClasses += ".product-selection-panel-v1 .pool-item-option-wrapper div.right-btn.btn-edit>span{background-image: url('data:image/svg+xml,%3C%3Fxml%20version%3D%221.0%22%20%3F%3E%3C%21DOCTYPE%20svg%20%20PUBLIC%20%27-%2F%2FW3C%2F%2FDTD%20SVG%201.1%2F%2FEN%27%20%20%27http%3A%2F%2Fwww.w3.org%2FGraphics%2FSVG%2F1.1%2FDTD%2Fsvg11.dtd%27%3E%3Csvg%20enable-background%3D%22new%200%200%2024%2024%22%20height%3D%2226px%22%20id%3D%22Layer_1%22%20version%3D%221.1%22%20viewBox%3D%220%200%2024%2024%22%20width%3D%2226px%22%20xml%3Aspace%3D%22preserve%22%20xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%20xmlns%3Axlink%3D%22http%3A%2F%2Fwww.w3.org%2F1999%2Fxlink%22%3E%3Cpath%20fill%3D%22" + headerTheme2Color.replace("#", "%23") + "%22%20%20d%3D%22M21.635%2C6.366c-0.467-0.772-1.043-1.528-1.748-2.229c-0.713-0.708-1.482-1.288-2.269-1.754L19%2C1C19%2C1%2C21%2C1%2C22%2C2S23%2C5%2C23%2C5%20%20L21.635%2C6.366z%20M10%2C18H6v-4l0.48-0.48c0.813%2C0.385%2C1.621%2C0.926%2C2.348%2C1.652c0.728%2C0.729%2C1.268%2C1.535%2C1.652%2C2.348L10%2C18z%20M20.48%2C7.52%20%20l-8.846%2C8.845c-0.467-0.771-1.043-1.529-1.748-2.229c-0.712-0.709-1.482-1.288-2.269-1.754L16.48%2C3.52%20%20c0.813%2C0.383%2C1.621%2C0.924%2C2.348%2C1.651C19.557%2C5.899%2C20.097%2C6.707%2C20.48%2C7.52z%20M4%2C4v16h16v-7l3-3.038V21c0%2C1.105-0.896%2C2-2%2C2H3%20%20c-1.104%2C0-2-0.895-2-2V3c0-1.104%2C0.896-2%2C2-2h11.01l-3.001%2C3H4z%22%2F%3E%3C%2Fsvg%3E');}";
		cssClasses += ".product-selection-panel-v1 .pool-item-option-wrapper div.right-btn.btn-pinfo>span{background-image: url('data:image/svg+xml,%3C%3Fxml%20version%3D%221.0%22%20%3F%3E%3C%21DOCTYPE%20svg%20%20PUBLIC%20%27-%2F%2FW3C%2F%2FDTD%20SVG%201.1%2F%2FEN%27%20%20%27http%3A%2F%2Fwww.w3.org%2FGraphics%2FSVG%2F1.1%2FDTD%2Fsvg11.dtd%27%3E%3Csvg%20enable-background%3D%22new%200%200%2045%2045%22%20height%3D%2224px%22%20id%3D%22Layer_1%22%20version%3D%221.1%22%20viewBox%3D%220%200%2085%2085%22%20width%3D%2224px%22%20xml%3Aspace%3D%22preserve%22%20xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%20xmlns%3Axlink%3D%22http%3A%2F%2Fwww.w3.org%2F1999%2Fxlink%22%3E%3Cpath%20d%3D%22M42.5%2C0.003C19.028%2C0.003%2C0%2C19.031%2C0%2C42.503s19.028%2C42.5%2C42.5%2C42.5S85%2C65.976%2C85%2C42.503S65.972%2C0.003%2C42.5%2C0.003z%20%20%20M42.288%2C66.27c0%2C0-1.972%2C1.311-3.32%2C1.305c-0.12%2C0.055-0.191%2C0.087-0.191%2C0.087l0.003-0.087c-0.283-0.013-0.568-0.053-0.855-0.125%20%20l-0.426-0.105c-2.354-0.584-3.6-2.918-3.014-5.271l3.277-13.211l1.479-5.967c1.376-5.54-4.363%2C1.178-5.54-1.374%20%20c-0.777-1.687%2C4.464-5.227%2C8.293-7.896c0%2C0%2C1.97-1.309%2C3.319-1.304c0.121-0.056%2C0.192-0.087%2C0.192-0.087l-0.005%2C0.087%20%20c0.285%2C0.013%2C0.57%2C0.053%2C0.857%2C0.124l0.426%2C0.106c2.354%2C0.584%2C3.788%2C2.965%2C3.204%2C5.318l-3.276%2C13.212l-1.482%2C5.967%20%20c-1.374%2C5.54%2C4.27-1.204%2C5.446%2C1.351C51.452%2C60.085%2C46.116%2C63.601%2C42.288%2C66.27z%20M50.594%2C24.976%20%20c-0.818%2C3.295-4.152%2C5.304-7.446%2C4.486c-3.296-0.818-5.305-4.151-4.487-7.447c0.818-3.296%2C4.152-5.304%2C7.446-4.486%20%20C49.403%2C18.346%2C51.411%2C21.68%2C50.594%2C24.976z%22%20fill%3D%22" + headerTheme2Color.replace("#", "%23") + "%22%2F%3E%3C%2Fsvg%3E');}";
		cssClasses += ".product-selection-panel-v1 .pool-item-option-wrapper div.right-btn.btn-remove>span{background-image: url('data:image/svg+xml,%3C%3Fxml%20version%3D%221.0%22%20%3F%3E%3C%21DOCTYPE%20svg%20%20PUBLIC%20%27-%2F%2FW3C%2F%2FDTD%20SVG%201.1%2F%2FEN%27%20%20%27http%3A%2F%2Fwww.w3.org%2FGraphics%2FSVG%2F1.1%2FDTD%2Fsvg11.dtd%27%3E%3Csvg%20enable-background%3D%22new%200%200%20500%20500%22%20height%3D%2230px%22%20id%3D%22Layer_1%22%20version%3D%221.1%22%20viewBox%3D%220%200%20500%20500%22%20width%3D%2230px%22%20xml%3Aspace%3D%22preserve%22%20xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%20xmlns%3Axlink%3D%22http%3A%2F%2Fwww.w3.org%2F1999%2Fxlink%22%3E%3Cpath%20clip-rule%3D%22evenodd%22%20d%3D%22M418.081%2C122.802h-59.057V68.29%20%20c0-20.077-16.262-36.34-36.341-36.34H177.316c-20.078%2C0-36.342%2C16.264-36.342%2C36.34v54.513H81.918%20%20c-12.536%2C0-22.713%2C10.177-22.713%2C22.715c0%2C12.536%2C10.177%2C22.713%2C22.713%2C22.713h13.629v263.48c0%2C20.078%2C16.262%2C36.34%2C36.341%2C36.34%20%20h236.224c20.078%2C0%2C36.341-16.262%2C36.341-36.34v-263.48h13.629c12.535%2C0%2C22.715-10.177%2C22.715-22.713%20%20C440.796%2C132.979%2C430.616%2C122.802%2C418.081%2C122.802z%20M313.598%2C122.802H186.4V97.367c0-11.083%2C8.909-19.991%2C19.991-19.991h87.216%20%20c11.084%2C0%2C19.99%2C8.909%2C19.99%2C19.991V122.802z%20M186.4%2C186.401v218.051c0%2C9.992-8.181%2C18.172-18.17%2C18.172s-18.17-8.18-18.17-18.172%20%20V186.401c0-9.989%2C8.18-18.17%2C18.17-18.17S186.4%2C176.412%2C186.4%2C186.401z%20M268.172%2C186.401v218.051%20%20c0%2C9.992-8.181%2C18.172-18.172%2C18.172c-9.99%2C0-18.17-8.18-18.17-18.172V186.401c0-9.989%2C8.181-18.17%2C18.17-18.17%20%20C259.991%2C168.231%2C268.172%2C176.412%2C268.172%2C186.401z%20M349.938%2C186.401v218.051c0%2C9.992-8.181%2C18.172-18.169%2C18.172%20%20c-9.99%2C0-18.172-8.18-18.172-18.172V186.401c0-9.989%2C8.182-18.17%2C18.172-18.17C341.758%2C168.231%2C349.938%2C176.412%2C349.938%2C186.401z%22%20fill%3D%22" + headerTheme2Color.replace("#", "%23") + "%22%20fill-rule%3D%22evenodd%22%2F%3E%3C%2Fsvg%3E');}";
		cssClasses += ".product-selection-panel-v1 .pool-item-option-wrapper div.right-btn:focus, .product-selection-panel-v1 .pool-item-option-wrapper div.right-btn:hover{outline:none;-webkit-box-shadow: 0 0 12px " + headerTheme2Color + "!important;-moz-box-shadow: 0 0 12px " + headerTheme2Color + "!important;box-shadow: 0 0 12px " + headerTheme2Color + "!important;}";

		cssClasses += ".product-selection-panel-v1 .pool-item-option-wrapper div.btn-remove:focus, .product-selection-panel-v1 .pool-item-option div.btn-remove:focus{outline:none;-webkit-box-shadow: 0 0 12px " + headerTheme2Color + "!important;-moz-box-shadow: 0 0 12px " + headerTheme2Color + "!important;box-shadow: 0 0 12px " + headerTheme2Color + "!important;outline:none;}";
		cssClasses += ".product-selection-panel-v1 .pool-item-option-wrapper .service-item label.custom-checkbox + label{line-height: 24px;display: inline; margin: 0; color: " + headerTheme2Color + ";}";
		cssClasses += ".product-selection-panel-v1 .btn-group .btn{color: " + headerTheme2Color + "; border-color:" + headerTheme2Color + "}";
		cssClasses += ".product-selection-panel-v1 .btn-group .btn.active{color: #fff; background-color:" + headerTheme2Color + "}";
		cssClasses += ".product-selection-panel-v1 .btn-group .btn:hover{color: #fff; background-color:" + headerTheme2Color + "}";

		cssClasses += ".account-role-item-wrapper div.btn-edit {width:27px; height: 27px; position: absolute;top: 50%;right: 50px;transform: translate(0, -50%);background-image: url(\"data:image/svg+xml,%3csvg xmlns='http://www.w3.org/2000/svg' viewBox='0 0 64.8 96.8'%3e%3cpath d='M29.6,12.3l4.3-7.5c2.6-4.5,8.4-6.1,12.9-3.5L60,8.8c4.5,2.6,6.1,8.4,3.5,12.9l-4.3,7.5 c-0.5,0.9-1.4,1.3-2,1L29.7,14.5C29.1,14.1,29.1,13.2,29.6,12.3z M55.1,36.5L29.6,80.9l0,0l-28.8,16L0,63.9l0,0l25.6-44.4 c0.5-0.9,1.4-1.3,2-1L55,34.3C55.6,34.6,55.7,35.6,55.1,36.5z M22.7,68.6l2.1,1.2l21-36.7l-2.1-1.2L22.7,68.6z M12,62.5l2.1,1.2 l21-36.7L33,25.8L12,62.5z M25.6,79.5c0.3-0.2,0.6-0.5,0.8-0.9c0.9-1.6-0.1-3.8-2.2-5c-2-1.1-4.2-1-5.2,0.3 c0.6-1.5-0.4-3.6-2.4-4.7c-2-1.1-4.2-1-5.3,0.3C11.9,68,11,66,9,64.8c-2.1-1.2-4.6-0.9-5.5,0.6c-0.2,0.4-0.3,0.7-0.3,1.2l0,0 l0.3,16.7l7.5,4.3L25.6,79.5L25.6,79.5z' fill='" + headerTheme2Color.replace("#", "%23") + "'/%3e%3c/svg%3e\")!important;border-color:" + headerTheme2Color + "!important;}";
		cssClasses += ".account-role-item-wrapper div.btn-remove:hover, .account-role-item-wrapper div.btn-edit:hover{cursor:pointer;}";
		cssClasses += ".account-role-item-wrapper div.btn-remove:focus, .account-role-item-wrapper div.btn-edit:focus{outline:none;-webkit-box-shadow: 0 0 12px " + headerTheme2Color + "!important;-moz-box-shadow: 0 0 12px " + headerTheme2Color + "!important;box-shadow: 0 0 12px " + headerTheme2Color + "!important;outline:none;}";
		cssClasses += ".account-role-item-wrapper .role-attr-item label.custom-checkbox + label{line-height: 24px;display: inline; margin: 0; color: " + headerTheme2Color + ";}";
		cssClasses += ".ui-radio .ui-radio-on:after{background-color:#fff;border-color:" + headerTheme2Color + "!important;}";
		cssClasses += ".header-theme-dropdown-btn .ui-btn.btnHover, .xsell-selection-panel .ui-btn.btnHover {border-color: " + headerTheme2Color + " !important solid ; color: #fff !important;background-image:none !important;background-color: " + headerTheme2Color + " !important; text-shadow: none !important;-ms-opacity: 0.8; opacity: 0.8;}";
		cssClasses += ".header-theme-dropdown-btn .ui-btn.btnActive_on, .xsell-selection-panel .ui-btn.btnActive_on {border-color: " + headerTheme2Color + " solid !important; color: #ffffff !important;background-image:none !important ;background-color: " + headerTheme2Color + " !important; text-shadow: none !important;}";
		cssClasses += ".header-theme-dropdown-btn .ui-btn.btnActive_off, .xsell-selection-panel .ui-btn.btnActive_off {border-color:" + headerTheme2Color + " solid !important; color: " + headerTheme2Color + " !important;text-align:left;background-color: " + contentBgColor + " !important; text-shadow: none !important ;}";
		cssClasses += ".header-theme-dropdown-btn .ui-btn.btnActive_on span, .header-theme-dropdown-btn .ui-btn.btnHover span, .xsell-selection-panel .ui-btn.btnActive_on span { color: #fff !important;}";
		cssClasses += ".header-theme-dropdown-btn .ui-btn.btnActive_off span, .xsell-selection-panel .ui-btn.btnActive_off span { color: " + headerTheme2Color + " !important;}";
		cssClasses += ".ui-icon-carat-d::after{background-image:url(\"data:image/svg+xml,%3csvg xmlns='http://www.w3.org/2000/svg' viewBox='0 0 100 100'%3e%3cpath d='M10,10 H90 L50,70' fill='" + headerTheme2Color.replace("#", "%23") + "'/%3e%3c/svg%3e\")!important}";
		cssClasses += ".header-theme-dropdown-btn .ui-icon-carat-d::before{background-image:url(\"data:image/svg+xml,%3csvg xmlns='http://www.w3.org/2000/svg' viewBox='0 0 100 100'%3e%3cpath d='M10,10 H90 L50,70' fill='" + headerTheme2Color.replace("#", "%23") + "'/%3e%3c/svg%3e\")!important}";
		cssClasses += ".header-theme-dropdown-btn .ui-icon-carat-d.btnHover::before, .header-theme-dropdown-btn .ui-icon-carat-d.btnActive_on::before{background-image:url(\"data:image/svg+xml,%3csvg xmlns='http://www.w3.org/2000/svg' viewBox='0 0 100 100'%3e%3cpath d='M10,10 H90 L50,70' fill='%23FFFFFF'/%3e%3c/svg%3e\")!important}";
		cssClasses += ".header-theme-checkbox {border-color:" + headerTheme2Color + "!important;}";
		cssClasses += ".header-theme-checkbox:after {border: 1px solid " + headerTheme2Color + ";}";
		cssClasses += ".header-theme-checkbox.ui-checkbox-on:after {background-image: url(\"data:image/svg+xml,%3csvg xmlns='http://www.w3.org/2000/svg' viewBox='0 0 100 100'%3e%3cpolygon fill='" + headerTheme2Color.replace("#", "%23") + "' points='38.5,78.8 0,46.9 8.2,37 36.8,60.8 87.1,0 97,8.2 38.5,78.8'/%3e%3c/svg%3e\")!important;border-color:" + headerTheme2Color + "!important;}";
		cssClasses += "div[data-role='page'],div[data-role='popup'],.ui-content{background:none!important;background-color:" + contentBgColor + "!important;}";
		cssClasses += "label.custom-radio-button{background:none!important;background-color:" + contentBgColor + "!important; border:none!important;}";
		cssClasses += "div[data-role='header']{border: 1px solid " + headerThemeColorEnd + "!important;background-image: -webkit-gradient(linear, left top, left bottom, from( " + headerThemeColorStart + "), to( " + headerThemeColorEnd + "))!important;background-image: -webkit-linear-gradient( " + headerThemeColorStart + ", " + headerThemeColorEnd + ")!important;background-image:-moz-linear-gradient( " + headerThemeColorStart + ", " + headerThemeColorEnd + ")!important; background-image:-ms-linear-gradient( " + headerThemeColorStart + ", " + headerThemeColorEnd + ")!important;background-image:-o-linear-gradient( " + headerThemeColorStart + ", " + headerThemeColorEnd + ")!important;background-image:linear-gradient( " + headerThemeColorStart + ", " + headerThemeColorEnd + ")!important;}";


		cssClasses += ".div_gmi_section .ui-checkbox {border: 1px solid " + headerTheme2Color + "!important;}";
		cssClasses += ".div_gmi_section .ui-checkbox .ui-checkbox-off {background-color: #fff!important;color: " + headerTheme2Color + "!important;}";
		cssClasses += ".div_gmi_section .ui-btn.ui-checkbox-off:after {border: 1px solid " + headerTheme2Color + "!important;background-color: #fff!important;}";
		cssClasses += ".div_gmi_section .ui-checkbox label {background-color: #fff!important;color: " + headerTheme2Color + "!important; margin:1px;}";
		cssClasses += ".div_gmi_section .ui-checkbox-on:after {background-color: " + headerTheme2Color + "!important;}";
		cssClasses += ".div_gmi_section .ui-input-text,.div_gmi_section .ui-field-contain .ui-input-text,.div_gmi_section .ui-select .ui-btn {background-color: #ffffff !important;border: 1px solid " + headerTheme2Color + " !important;}";
		cssClasses += ".div_gmi_section .ui-input-text input,.div_gmi_section .ui-field-contain .ui-input-text input,.div_gmi_section .ui-select .ui-btn input{color: " + headerTheme2Color + " !important;}";
		
		style.type = 'text/css';
		style.innerHTML = cssClasses;
		style.id = "styleHeaderTheme";
		if (document.getElementById("styleHeaderTheme") != null) {
			document.getElementById("styleHeaderTheme").parentNode.removeChild(document.getElementById("styleHeaderTheme"));
		}
		document.getElementsByTagName('head')[0].appendChild(style);
	}
}
function rgbToHex(colorval) {
	var parts = colorval.match(/^rgb\((\d+),\s*(\d+),\s*(\d+)\)$/);
	if (parts == null || parts.length == 0) return "";

	delete (parts[0]);
	for (var i = 1; i <= 3; ++i) {
		parts[i] = parseInt(parts[i]).toString(16);
		if (parts[i].length == 1) parts[i] = '0' + parts[i];
	}

	return '#' + parts.join('');
}

jQuery.fn.extend({
	live: function (event, callback) {
		if (this.selector) {
			jQuery(document).on(event, this.selector, callback);
		}
	}
});

var matched, browser;

jQuery.uaMatch = function (ua) {
	ua = ua.toLowerCase();

	var match = /(chrome)[ \/]([\w.]+)/.exec(ua) ||
		/(webkit)[ \/]([\w.]+)/.exec(ua) ||
		/(opera)(?:.*version|)[ \/]([\w.]+)/.exec(ua) ||
		/(msie) ([\w.]+)/.exec(ua) ||
		ua.indexOf("compatible") < 0 && /(mozilla)(?:.*? rv:([\w.]+)|)/.exec(ua) ||
		[];

	return {
		browser: match[1] || "",
		version: match[2] || "0"
	};
};

matched = jQuery.uaMatch(navigator.userAgent);
browser = {};

if (matched.browser) {
	browser[matched.browser] = true;
	browser.version = matched.version;
}

// Chrome is Webkit, but Webkit is also Safari.
if (browser.chrome) {
	browser.webkit = true;
} else if (browser.webkit) {
	browser.safari = true;
}

jQuery.browser = browser;
$(document).on("mobileinit", function () {
	//using old jquery mobile version( < 1.4.x)the scroll is not working in chrome mobile (current version)
	//due to the animation break down -> need to handle animation before calling jquery mobile library
	//and also prevent memory leak from the animationend function which never fire
	$.fn.animationComplete = function (callback) {
		if ($.support.cssTransitions) {
			var completeAnimation = "WebKitTransitionEvent" in window ? "webkitAnimationEnd" : "animationend";
			return $(this).one(completeAnimation, callback);
		} else {
			setTimeout(callback, 0);
			return $(this);
		}
	};

});
function appendPDFViewer() {
	var pdfPopup = '<div id="popupPDFViewer" data-role="page" role="region" aria-label="PDF Viewer">' +
				   '    <div data-role="header" style="display: none" >' +
				   '            <h1>Document Viewer</h1>' +
				   '    </div>' +
				   '    <div data-role="content" style="padding: 0 !important">' +
				   '        <div class="row" style="margin:10px 5px;"><div class="col-xs-12 header_theme2" style="padding-right: 0px;"><a onclick="gotoPreviousPage()" href="#" class="pull-right svg-btn">' + gl_icon_close + '<span style="display: none;">close</span></a></div></div>' +
			   //'        <div style="text-align: center;"><a data-role="button" data-inline="true" data-icon="arrow-l" data-theme="b" onclick="gotoPreviousPage()">Back</a></div>' +
			   '        <div style="text-align: center;" id="div_iframe"></div>' +
			   '    </div>' +
			   '</div>';
	//'        <div class="hide-button" style="text-align: center;" id="btn_bottom_close_div"><a data-role="button" data-inline="true" data-icon="arrow-l" data-theme="b" data-rel="back" onclick="hideBottomButton()">Close</a></div>' +


	if ($('#popupPDFViewer').length == 0) {
		$(pdfPopup).insertBefore('#divErrorDialog');
	}
}

function appendDocuSign() {  
	var docuSignPopup = '<div id="popupDocuSign" data-role="page" role="region">' +
				   '    <div data-role="header" style="display: none" >' +
				   '            <h1>Document Sign</h1>' +
				   '    </div>' +
				   '    <div data-role="content" style="padding: 0 !important">' +
				   '        <div class="row" style="margin:10px 5px;"><div class="col-xs-12 header_theme2" style="padding-right: 0px;"><a href="#pl_last" class="pull-right svg-btn"><span style="display: none;">close</span></a></div></div>' +
			   '        <div style="text-align: center;" id="div_docuSignIframe"></div>' +
			   '    </div>' +
			   '</div>';

	if ($('#popupDocuSign').length == 0) {
		$(docuSignPopup).insertBefore('#divErrorDialog');
	}
}

//START LINKED-IN
var is_called = false;

// Setup an event listener to make an API call once auth is complete
function onLinkedInLoad() {
	IN.Event.on(IN, "auth", getProfileData);
	if ($('#hd_linkedin_email').val() == "") {
		getProfileData();
    }   
}

// Handle the successful return from the API call
function onSuccess(data) {
    document.getElementById('hd_linkedin_firstname').value = data.firstName;
	document.getElementById('hd_linkedin_lastname').value = data.lastName;
	document.getElementById('hd_linkedin_formattedname').value = data.formattedName;
	document.getElementById('hd_linkedin_email').value = data.emailAddress;
	document.getElementById('hd_linkedin_country').value = data.location.name;

	if (data.positions.values.length > 0) {
		document.getElementById('hd_linkedin_companyname').value = data.positions.values[0].company.name;
		document.getElementById('hd_linkedin_title').value = data.positions.values[0].title;
	}

    BindLoggedInLinkedInData(data);
}

// Handle an error response from the API call
function onError(error) {
	ClearLinkedInData();
}

// Use the API call wrapper to request the member's basic profile data
function getProfileData() {
	if (!is_called) {
		IN.API.Raw("/people/~:(id,first-name,last-name,formatted-name,location,positions,email-address)").result(onSuccess).error(onError);
		is_called = true;
	}
}

function logoutLinkedIn() {
	IN.User.logout();
	ClearLinkedInData();
}
//END LINKED-IN
//OBSOLETE - NO USED ANYWHERE
//function cancelSession() {
//	if (confirm('You are about to navigate from the current application. All information in current application will be cleared. \n\nAre you sure you wish to proceed?')) {
//		setCookie('isCompletedSubmit', '1');
//		window.location.href = window.location.href;
//		return true;
//	}
//	else {
//		return false;
//	}

//}

//TODO:change back to 120 for 2 min
var IDLE_TIMEOUT = 300; // this number(300) multiply by 3 sec  = ?sec
var _idleSecondsCounter = 0;
document.onclick = function () {
	_idleSecondsCounter = 0;
};
document.onmousemove = function () {
	_idleSecondsCounter = 0;
};
document.onkeypress = function () {
	_idleSecondsCounter = 0;
};
window.setInterval(CheckIdleTime, 3000);  //check every 3sec, not working correctly when phone is in sleep or browser is not in focus
var LAST_USE_TIME = (new Date()).getTime();
function CheckIdleTime() {
	var now = new Date();
	var needToRefresh = false;
	var needToRefreshServerSession = false;
	var nTimeSpan = now.getTime() - LAST_USE_TIME;
	if (now.getTime() - LAST_USE_TIME > IDLE_TIMEOUT * 3000 - _idleSecondsCounter * 3000) {
		needToRefresh = true;
	}

	var dTempElapseTime = now.getTime() - LAST_SERVER_SESSION_REFRESH_TIME;
	if (dTempElapseTime > (40 * 60 * 1000)) {  //40min
		needToRefreshServerSession = true;
	}

	_idleSecondsCounter++;
	var oPanel = document.getElementById("SecondsUntilExpire");
	if (oPanel)
		oPanel.innerHTML = (IDLE_TIMEOUT - _idleSecondsCounter) + "";

	//special case for ; TODO: clear out paypal approved message stored in session
	if (_idleSecondsCounter >= IDLE_TIMEOUT || needToRefresh) {
		if ((gl_timeout_url != null) && (window.location.href.indexOf("MLPayPalFinished") > -1)) {
			//alert("For security reason, this application will be restarted due to time out.");
			window.location.href = gl_timeout_url;
		}
	}

	//Only display timeout msg and reload when pages have value and inactivity time is greater than IDLE_TIMEOUT x3
	if ((_idleSecondsCounter >= IDLE_TIMEOUT || needToRefresh) && !isFormClean()) {
		clearForms();
		//alert("For security reason, this application will be restarted due to time out.");
		if ($("#divErrorDialog").length === 1) {
			//logInfo("client side expired")
			$('#txtErrorMessage').html("For security reasons, this application will be restarted due to time out.");
			goToNextPage("#divErrorDialog");
			$("#divErrorDialog").off("pagehide").on("pagehide", function () {
				//logInfo("client side restarted")
				//when error dialog closed, 
				window.location.href = window.location.href;  //got back to opening page, need to keep previous  querystring para "type"
			});
		} else {
			alert("For security reasons, this application will be restarted due to time out.");
			window.location.href = window.location.href;  //got back to opening page, need to keep previous  querystring para "type"
		}
	}

	//TODO:multiple app open at eh same time, need to have unique token id or session key for each
	//we know for sure server session has expired
	if (needToRefreshServerSession) {
		if (!isFormClean()) {  //alert consumer             
			if ($("#divErrorDialog").length === 1) {
				//logInfo("client side expired")
				$('#txtErrorMessage').html("For security reasons, this application will be restarted due to time out.");
				goToNextPage("#divErrorDialog");
				$("#divErrorDialog").off("pagehide").on("pagehide", function () {
					//logInfo("client side restarted")
					//when error dialog closed, 
					window.location.href = window.location.href;  //got back to opening page, need to keep previous  querystring para "type"
				});
			} else {
				alert("For security reasons, this application will be restarted due to time out.");
				window.location.href = window.location.href;  //got back to opening page, need to keep previous  querystring para "type"
			}
		} else { //reload page since form doesn't have data
			window.location.href = window.location.href; //create new server session
		}

	} //


	LAST_USE_TIME = (new Date()).getTime();
}

function isFormClean() {
	var flag = true;
	$('input[type="text"]').each(function () {
		var element = $(this);
		element.val(element.val().trim()); //handle case where derived full name in credit card funding page may contain empty space
		if (element.val().length != 0) {
			if (element.val() == "$0.00") {
				return;
			}

			var elementId = element.attr('id');
			if (elementId == undefined) {   //some control may not have id
				return true;  //continue to next iteration
			}
			// deposit input fields are based on the select and preselected products from download. --> unable to clear all deposit input fields.
			//there, just ignore deposit input and total deposit amount fields.
			if (elementId.indexOf("txtDepositAmount") != -1 || elementId == "txtTotalDeposit") {
				return;  //continue to next iteration
			}

			if (elementId.indexOf("txtFundingDeposit") != -1) {  //alway has $251 for sdfcu                 
				return;  //continue to next iteration
			}

			flag = false; //form is not clean
			return false;  //break out of the iteration
		}
	});
	//need to check type tel
	$('input[type="tel"]').each(function () {
		var element = $(this);
		element.val(element.val().trim()); //handle case where derived full name in credit card funding page may contain empty space
		if (element.val().length != 0) {
			var depositInputId = element.attr('id');
			if (depositInputId == undefined) {
				isCleanForm = false;
				return false;  //break out of the iteration
			}
			// deposit input fields are based on the select and preselected products from download. --> unable to clear all deposit input fields.
			//there, just ignore deposit input and total deposit amount fields.
			if (depositInputId.indexOf("txtDepositAmount") == -1 && depositInputId != "txtTotalDeposit") {
				flag = false;
				return false;  //break out of the iteration
			}
		}
	});

	return flag;
}

function clearForms() {
	$('input[type="text"]').each(function () {
		$(this).val('');
	});

	$('input[type="tel"]').each(function () {
		$(this).val('');
	});
	$('input[type="password"]').each(function () {
		$(this).val('');
	});
	$('input[type="hidden"].combine-field-value').each(function () {
		$(this).val('');
	});

	$("select").each(function () {
		$(this).get(0).selectedIndex = 0;
		$(this).closest("div.ui-btn").find('span').html($(this).find("option:selected").text());
	});
	//doesn't work well, cause false validation
	//$('input[type="checkbox"]').each(function () {
	//  //$(this).removeAttr('checked');
	//  $(this).prop('checked', false).checkboxradio("refresh");
	//});

	//$('select').each(function () {
	//  $(this).val(' ').selectmenu('refresh');  //empty space
	//});

	//clear the Review Information 
	if (typeof ClearReviewInfoXA == 'function') {
		ClearReviewInfoXA();
	}
	//clear upload docs
	//if ($('#divImage').length > 0) {
	//    $('#divImage').html('');
	//    $('#co_divImage').html('');
	//}

	$("#divImage").find("button[id^='btnRemove_']").trigger("click");
	$("#co_divImage").find("button[id^='btnRemove_']").trigger("click");
	//clear document scan
	if ($('#backImagePlaceHolder').length > 0) {
		$('#backImagePlaceHolder').attr('src', "../images/dl_back_sample.jpg");
		$('#co_backImagePlaceHolder').attr('src', "../images/dl_back_sample.jpg");
		$('#divBackMessage').html('');
		$('#co_divBackMessage').html('');
	}
	//clear all checkboxes
	clearInputFields('checkbox');
	//clear radio buttons
	clearInputFields('radio');
	clearReviewData();
	//$("div[name='disclosures'] .btn-header-theme").each(function () {
	//  $(this).removeClass("active");
	//  handledBtnHeaderTheme($(this));
	//});
	$("a.btn-header-theme").each(function () {
		var $self = $(this);
		$self.removeClass("active");
		handledBtnHeaderTheme($self);
	});
	//status page
	if ($('#divSearchResults').length > 0) {
		$('#divSearchResults').html('');
	}
	if ($("#divShowFOM").length > 0) {
		resetFom();
	}
	
	if ($("#divFundingTypeList").length > 0) {
		//check this function exist before calling it(causes error in ms-accu_test)
		if (typeof resetFunding == 'function') {
			resetFunding();
		}
	}
}

function clearReviewData() {
	$("#reviewPanel").find("div.row.panel").each(function () {
		$(this).html("");
	});
}

//Keep session alive logic
//postback page token need to match the one stored in session. In kiosk mode, website may be in page one(clean page, no value) for a long time without any page reload
//so need to do fake postback to keep session alive 
var nCounter = 0;
var nSessionFailCounter = 0;
var LAST_SERVER_SESSION_REFRESH_TIME = (new Date()).getTime();
function KeepSessionAlive() {
	// A request to server
	nCounter = nCounter + 1;
	$.post("../handler/SessionCheck.aspx?time=" + nCounter, function (result) { //bogus call to keep session alive on server side
		//do nothing for sucessful callback
		LAST_SERVER_SESSION_REFRESH_TIME = (new Date()).getTime();
	}).fail(function (a, b, c) {  //connection issue also trigger this

		//failure may be due to network issue so do 1 more try in 3 min(3 x 60 = 180sec), 180000
		if (nSessionFailCounter == 0) {
			setTimeout(function () {
				KeepSessionAlive();
			}, 180000);
			nSessionFailCounter = nSessionFailCounter + 1;
			return;
		}
		else {
			nSessionFailCounter = 0;
		}

		window.clearInterval();   //no more callback until the next sucessful page load
		//logInfo("client side session expired")
		//alert("For security reason, this application will be restarted due to time out.");
		if ($("#divErrorDialog").length === 1) {
			$('#txtErrorMessage').html("For security reasons, this application will be restarted due to time out.");
			goToNextPage("#divErrorDialog");
			$("#divErrorDialog").off("pagehide").on("pagehide", function () {
				//logInfo("client side restarted")
				window.location.href = window.location.href;  //got back to opening page, need to keep previous  querystring para "type"
			});
		} else {
			alert("For security reasons, this application will be restarted due to time out.");
			window.location.href = window.location.href;  //got back to opening page, need to keep previous  querystring para "type"
		}

	});


} //end KeepSessionAlive

//now schedule this process to happen in some time interval, in this example its 19 min
//milisecond
//60000 = 1min; 1140000 =19min,600000=10min 
window.setInterval(KeepSessionAlive, 1140000);//timer is not correct when the phone is standby or off, CheckIdleTime will handle situation

function cc() {       
    /* check for a cookie */
    if (document.cookie == "") {
        var lenderRef = getParaByName("lenderref");

        //TODO: make this active when we have SOW
        //only do this for VL
        //if (lenderRef.toUpperCase().startsWith('Redwood'.toUpperCase())) {
        //    loginfo("Cookie is disabled by Redwood client");
        //    return;
        //}

		/* if a cookie is not found - alert user -
		change cookieexists field value to false */
        //loginfo("Cookie is disabled by consumer browser.  lenderRef: " + lenderRef);       
		alert("You must enable cookies in order to use this application.");
	}
}
/* Set a cookie to be sure that one exists.
	Note that this is outside the function*/
document.cookie = 'killme' + escape('nothing');
cc();

$(document).on('pagecontainerbeforeshow', function () {
	//initSiteMenu(window.bHasCoApp); // Hide site menu to reduce confusion
    //initProgressBar(window.bHasCoApp); // /Hide progress bar
    
});

//deprecated, not use
function getMaxHeight() {
	var body = document.body,
		html = document.documentElement;

	var height = Math.max(body.scrollHeight, body.offsetHeight,
		html.clientHeight, html.scrollHeight, html.offsetHeight);
	return height;
}
$(document).on('pagebeforeshow', function () {
	var pageId = $.mobile.pageContainer.pagecontainer('getActivePage').attr('id');
	applyHeaderThemeCss('#' + pageId);
    applyFooterThemeCss('#' + pageId, gl_button_font_color);    
});

$(document).on('pageshow', function () {    
	var referrer = document.referrer;

	var pageId = $.mobile.pageContainer.pagecontainer('getActivePage').attr('id');
	// Add required fields notation
	$('.required-field-notation').remove();

	if ($('#' + pageId + ' input').length > 0) {
		if (pageId.indexOf('scandocs') == -1 && pageId.indexOf('reviewpage') == -1 && pageId.indexOf('UploadDocs') == -1 && pageId.indexOf('_last') == -1 && pageId.indexOf('accuWelcome') == -1) {
			$('.div-continue').each(function(idx, ele) {
				var $ele = $(ele);
				if ($ele.hasClass("js-no-required-field") == false) {
					$ele.prepend("<div class='required-field-notation'><span class='require-span'>*</span>Required Field(s)</div>");
				}
			});
			
		}
		if (pageId.indexOf('UploadDocs') > -1 && $('#hdRequiredUploadDoc').val() == 'Y') { //add required field notation to required upload doc page
			$('#' + pageId + ' .div-continue').prepend("<div class='required-field-notation'><span class='require-span'>*</span>Required Field(s)</div>");
		}
	}

	//make color text "Or" the same with "Required Field(s)"
	var currFooterText = $('#' + pageId + ' .div-continue').clone().children().remove().end().text();
	if (currFooterText != undefined && currFooterText.trim().indexOf("Or") > -1) {
		$('#' + pageId + ' .div-continue').css('color', '#504949');
	}

	// Set min height based on device height, so the footer which is outside of mobile page is visible when content heightt is less than window height
	var currentPage = $("#" + pageId);
	var isDialog = false;
	//maybe we don't need to show footer when dialog is shown
	if (typeof currentPage != "undefined" && currentPage != null) {
		if (currentPage.data("role") != undefined && currentPage.data("role").toUpperCase() === "DIALOG") {
			isDialog = true;
		}
	}
	//if (pageId != "divErrorDialog" && pageId != "divConfirmDialog") {
	if (isDialog === false) {
		var minHeight = 0;
		var contentHeight = $('#' + pageId + ' div.ui-content').height();
		var headerHeight = $('#' + pageId + ' .ui-header').height();
		//if ($('#' + pageId + ' .div-continue').children()  .hasClass('div-continue-coapp-button')) {
		//	if ($('#' + pageId + ' .div-continue').children().hasClass('div-finish-later')) {
		//		$('#' + pageId + ' .div-continue.ui-footer').css('height', '270px');
		//	} else {
		//		$('#' + pageId + ' .div-continue.ui-footer').css('height', '220px');
		//	}
		//}
		var continueHeight = $('#' + pageId + ' .div-continue').height();
		if (contentHeight > $(window).height()) {
			minHeight = contentHeight + headerHeight + continueHeight + 120; // margin          
		} else {
			minHeight = $(window).height() - ((isMobile.any() && !isMobile.iPad()) ? 156 : 56); // subtract header and footer height TODO:substract less for bigger width device that are still consider mobile but can split the footer into left and right 
			if ($('.divfooter_top').length > 0) { // xa footer
				//subtract height of divfooter_top 
				minHeight = $(window).height() - ((isMobile.any() && !isMobile.iPad()) ? 233 : 133); // subtract header and footer height TODO:substract less for bigger width device that are still consider mobile but can split the footer into left and right
			}
			$.mobile.pageContainer.pagecontainer('getActivePage').css("min-height", minHeight + "px");
		}
		//if (pageId.indexOf('reviewpage') > -1) {
		//    minHeight = $(window).height();
		//    $('#' + pageId + ' div.ui-content').height(minHeight-headerHeight-progressbarHeight);
		//}

		//$.mobile.pageContainer.pagecontainer('getActivePage').css("min-height", minHeight + "px");

		//Hack for scroll issue running inside iOS webview
		//temporary compensate for native iOS app which doesn't correctly retore the viewport when keyboard popup or hide 
		//by making the container height longer so user can see the last field in the page
		//window height = device height
		//mobilePageHeight or mobile.activePage = mobile page(not including the footer which is outside the mobile page)
		//documentheight = lpq_container, document height, mobile page + footer
		if (isMobile.iOS() && referrer.indexOf('SSO') > 0) {

			//console.log('windowHeight= ' + $(window).height());
			var mobilePageHeight = $.mobile.pageContainer.pagecontainer('getActivePage').height();
			//console.log('mobilePageHeight= ' + mobilePageHeight);

			var hackedMinHeight = Math.max(mobilePageHeight, $(window).height()) + 400;
			//console.log('hackedMinHeight= ' + hackedMinHeight);
			$('.lpq_container').height(hackedMinHeight);  //document height
			//console.log('documentheight= ' + getMaxHeight());
		}
	}

	$("ul.progress-indicator li a").removeClass();
	//make sure to remove ui-link class of #href_show_last_dialog before calling bindLinkClickEvent() function
	//var element = $('#href_show_last_dialog');
	//var elementClass = element.attr('class');
	//if (elementClass != undefined) {
	//    if (elementClass.indexOf('ui-link') !=-1) {
	//        element.removeClass('ui-link');
	//    }
	//} //end removing ui-link class
	//remove ui-link
	//$('.disableBindLink').each(function () {
	//    var element = $(this);
	//    element.removeClass('ui-link');
	//    element.css('color', '#2489CE');
	//});
	bindLinkClickEvent(this);
	// show linkedin employment data
	//?? not sure white this is here but it cause these 2 field to be clear whenerver the page get load
	//TODO: fix for loan
	if ($.mobile.pageContainer.pagecontainer('getActivePage').attr('id') == "page2" && $("#hd_linkedin_companyname").val() != '' && $('#' + coPrefix + 'txtEmployer').val() == '') {
		$('#' + coPrefix + 'txtEmployer').val($("#hd_linkedin_companyname").val());
		$('#' + coPrefix + 'txtJobTitle').val($("#hd_linkedin_title").val());
	}
	if ($("#" + pageId).attr("auto-scrollTo") == "true") {
		

		// Auto scroll to doc upload section
		if ($("#" + pageId + " div[section-name='docupload']").length > 0) {
			$('html, body').scrollTop($("div[section-name='docupload']").offset().top - 40);
			$("#" + pageId).removeAttr("auto-scrollTo");
		}
		

	    // Auto scroll to disclosures section, 
	    //in combo mode,  there is more chance of this occurring so put it last
        //TODO: should have more precise location prodcut section or disclosure section in combo mode
		if ($("#" + pageId + " div[section-name='disclosure']:visible").length > 0) {
		    //$.mobile.silentScroll($("div[section-name='disclosure']").offset().top);
		    $('html, body').scrollTop($("div[section-name='disclosure']:visible").offset().top - 40);
		    $("#" + pageId).removeAttr("auto-scrollTo");
		}
	}
});
function getCoPrefix(isCoApp) {
	if (isCoApp) { return 'co_'; }
	return '';
}


//*********************
//check source(href) for compatible with iframe 
//*********************
//deprecated - not use anywhere
function changeAnchorToViewer($page) {
	if ($.mobile.pageContainer.pagecontainer('getActivePage').attr('id') == "popupPDFViewer") return;

	var lenderRef = $('#hfLenderRef').val();

	$('a.ui-link', $page).each(function () {
		if ($(this).attr("is-checked") == "1") return; // link already processed

		var href = $(this).attr('href');

		if (!href || ((href.indexOf("http://") != 0) && (href.indexOf("https://") != 0))) return;

		//we know for sure all href from lpq server can be displayed in iframe
		if (href.indexOf('loanspq.com') > 0) {
			CreatePdfViewerLink(this, lenderRef, href);
		}
			//check header for X-Frame-Options
		else if (href.indexOf('.pdf', href.length - 4) !== -1 || href.indexOf('/', href.length - 1) !== -1 || href.indexOf('.asp', href.length - 4) !== -1 || href.indexOf('.aspx', href.length - 5) !== -1 || href.indexOf('.html') > 0) {
			var anchor = this;
			isDownloadingFile = true;

			$.ajax({
				url: server_root + "/PdfViewer/Web/CrossDomainChecker.aspx",
				async: false,
				cache: false,
				type: "POST",
				dataType: "html",
				data: {
					check: href
				},
				success: function (responseText) {
					if (responseText.indexOf("OK") >= 0) {
						CreatePdfViewerLink(anchor, lenderRef, href);
					}
					$(anchor).attr("is-checked", "1");
				}
			});
		}
	});
}



//*********************
// Bind ui-link click event
// all links with class ui-link on the loading page will be binded to linkbuttonOnClick - not visible in source code, except for these page
//*********************
function bindLinkClickEvent($container) {
	if ($.mobile.pageContainer.pagecontainer('getActivePage').attr('id') == "popupPDFViewer") return;

	if ($.mobile.pageContainer.pagecontainer('getActivePage').attr('id') == "decisionPage") return;
	//'preven frame for all links in final status page
	if ($.mobile.pageContainer.pagecontainer('getActivePage').attr('id') == "cc_last") return;
	if ($.mobile.pageContainer.pagecontainer('getActivePage').attr('id') == "pl_last") return;
	if ($.mobile.pageContainer.pagecontainer('getActivePage').attr('id') == "vl_last") return;
	if ($.mobile.pageContainer.pagecontainer('getActivePage').attr('id') == "he_last") return;
	if ($.mobile.pageContainer.pagecontainer('getActivePage').attr('id') == "xa_last") return;
	if (true == isTouchDevice()) {
		//WEIRD,get trouble with this line of code, this will disable all click event on a.ui-link. So, can not bind click event for a.ui-link any more.
		//not sure about the logic behind. So temporary modify  to reduce the impact of these line of code
		$('a.ui-link:not([data-mode="self-handle-event"])', $container).off('tap').on('tap', function () {

			//exception
			var href = $(this).attr('href');
			if (!href || ((href.indexOf("http://") != 0) && (href.indexOf("https://") != 0) && (href.indexOf("/Information/DocViewer") != 0))) return;

			linkbuttonOnClick(this, true);
			return false;
		});
	} else {
		//WEIRD,get trouble with this line of code, this will disable all click event on a.ui-link. So, can not bind click event for a.ui-link any more.
		//not sure about the logic behind. So temporary modify  to reduce the impact of these line of code
		$('a.ui-link:not([data-mode="self-handle-event"])', $container).off('click').on('click', function () {
			//exception
			var href = $(this).attr('href');
			if (!href || ((href.indexOf("http://") != 0) && (href.indexOf("https://") != 0) && (href.indexOf("/Information/DocViewer") != 0))) return;

			linkbuttonOnClick(this, false);
			return false;
		});
	}
} //end bindLinkClickEvent

// Detect touch device
function isTouchDevice() {
	return true == ('ontouchstart' in window        // works on most browsers 
		|| navigator.maxTouchPoints || (window.DocumentTouch && document instanceof DocumentTouch));       // works on IE10/11 and Surface
};

//TODO: no use for whitelist now because also need to download pdf which will not work
//TODO: this can be remove later when we have a new server and fixed all connection issue
//need this whitelist for the follow reason
//1) 2003 server can't communicate with Apache server: (500) Internal Server Error
//2)  etc..
var framableDomainWhitelist = ["testsetsetset.com".toUpperCase()];
var framableDomainBlacklist = ["OBEE.COM", "webfederalnext.com".toUpperCase(), "trustonefoundation.org".toUpperCase()];  //list all domain that can't be frame

function linkbuttonOnClick(srcEle) {
	var href = $(srcEle).attr('href').trim();
	if ($(srcEle).hasClass("btn-header-theme")) {
		if ($(srcEle).hasClass("active") === false) {
			$(srcEle).addClass("active");
		}
	}


	var isFramable = false;
	var lenderRef = $('#hfLenderRef').val();

	//whitelist domain
	//TODO: will not work for international domain
	//var startIndex = href.lastIndexOf(".", href.lastIndexOf(".com") - 1) + 1;
	//var endIndex = href.lastIndexOf(".com") + 4;
	//var hrefDomain = href.substring(startIndex, endIndex).toUpperCase();
	var link = document.createElement('a');
	link.setAttribute('href', href);
	hrefDomain = link.hostname.toUpperCase();
	if ($.inArray(hrefDomain, framableDomainWhitelist) != -1) {
		isFramable = true;
		chooseOpeningMethod(isFramable, link, href, lenderRef, srcEle);
	}

		//blacklist domain, 
		//TODO: will not work for international domain
	else if ($.inArray(hrefDomain, framableDomainBlacklist) != -1) {
		isFramable = false;
		chooseOpeningMethod(isFramable, link, href, lenderRef, srcEle);
	}

		//we know for sure all href from lpq server can be displayed in iframe
	else if ((href.indexOf('loanspq.com') > 0) || (href.indexOf("/Information/DocViewer") == 0)) {
		isFramable = true;
		//fixed missing domain in link created on LPQ side
		if (href.indexOf("/Information/DocViewer") == 0) {
			href = gl_BaseSubmitLoanUrl.replace("/services", "") + href;
		}
		chooseOpeningMethod(isFramable, link, href, lenderRef, srcEle);
	}  //end indexOf('loanspq.com'
		//check header for X-Frame-Options
	else if (href.indexOf('.pdf', href.length - 4) !== -1 || href.indexOf('/', href.length - 1) !== -1 || href.indexOf('.asp', href.length - 4) !== -1 || href.indexOf("/Information/DocViewer") > 0 ||
		href.indexOf('.aspx', href.length - 5) !== -1 || href.indexOf('.htm') > 0 || href.indexOf('.php') > 0 || href.indexOf('.jpg') > 0 || href.indexOf('.png') > 0 || href.indexOf('.gif') > 0) {
		isDownloadingFile = true;

		$.ajax({
			url: server_root + "/PdfViewer/Web/CrossDomainChecker.aspx?lenderref=" + lenderRef,
			async: true,
			cache: false,
			type: "POST",
			dataType: "html",
			data: {
				check: href
			},
			success: function (responseText) {
				if (responseText.indexOf("OK") >= 0) {
					isFramable = true;
				}

				if (responseText.indexOf("Error") == -1 && href.indexOf('.pdf', href.length - 4) !== -1) {   //Crossdomian is still consider framable if it is PDF and dowloadable
					isFramable = true;
				}

				chooseOpeningMethod(isFramable, link, href, lenderRef, srcEle);
			}
		});
	}
	else {  //need for other file extension type. ex: php(apache server currently doesn't work with server2003)
		chooseOpeningMethod(isFramable, link, href, lenderRef, srcEle);
	}

} //end linkbuttonOnClick

//For frameable, call gotoPDFViewer; for non-frameable, open a new window and let os handle the loading
function chooseOpeningMethod(isFramable, link, href, lenderRef, srcEle) {
	var chkDisclosure = $(srcEle).closest('.ui-checkbox').find('input');
	if (chkDisclosure.closest(".disclosure-section").length > 0 && chkDisclosure.data('disabled') == true) {
		chkDisclosure.data('disabled', false);
		chkDisclosure.checkboxradio("enable");
		chkDisclosure.prop("checked", true).checkboxradio("refresh");
		setTimeout(function() {
			chkDisclosure.closest(".disclosure-section").trigger("disclosureChange");
		},200);
	}
	

	if (isFramable) {
		// Store current Page ID and move to PDF Viewer
		var pageId = $.mobile.pageContainer.pagecontainer('getActivePage').attr('id');
		if (pageId != "popupPDFViewer") {
			currentPage = pageId;
		}
		gotoPDFViewer(link, href, lenderRef);
	} else {
		if (true == isTouchDevice()) {
			if (isMobile.Android()) {
				//navigator.app.loadUrl(href, { openExternal: true });   //this doesn't work on chrome simulator or real androi device, this condition will not happen inside native android app 
				window.open(href, '_system');
			}
			if (isMobile.iOS()) {
				window.open(href, '_system');
			}
		} else {
			window.open(href, '_blank');
		}
	}
} //end chooseOpeningMethod

//*********************
//modify link popup so that the content can be displayed in iframe
//*********************
//deprecated - not use anywhere
function CreatePdfViewerLink(obj, lenderRef, href) {
	var isDisclosureDisable = false; //'flag for adding this special class back later
	if (obj.getAttribute('class').indexOf('DisclosureDisable') > -1) {
		isDisclosureDisable = true;
	}

	while (obj.attributes.length > 0) {
		obj.removeAttribute(obj.attributes[0].name);
	}

	$(obj).attr("class", "ui-link");
	if (isDisclosureDisable) {
		$(obj).attr("class", "ui-link DisclosureDisable");
	}

	//$(obj).attr("href", "#popupPDFViewer");
	$(obj).attr("style", "cursor: pointer");
	$(obj).attr("data-rel", "popup");
	$(obj).attr("onclick", "gotoPDFViewer(this, '" + href + "', '" + lenderRef + "')");
}

function closeMenu(e) {
	if ($(".menu-icon").is(e.target)) {
		return;
	}

	if ($("#nav").is(':visible') && (!$("#nav").is(e.target) && $("#nav").has(e.target).length === 0) || $("#nav li a").is(e.target)) {
		$("#nav").slideUp();
	}
	if ($("#co_nav").is(':visible') && (!$("#co_nav").is(e.target) && $("#co_nav").has(e.target).length === 0) || $("#co_nav li a").is(e.target)) {
		$("#co_nav").slideUp();
	}
}

$(document).on({
	"touchstart": function (e) { closeMenu(e); },
	"mouseup": function (e) { closeMenu(e); }
});

//------ End Site Menu section ------//
function restart(ele) {
	if ($(ele).attr("contenteditable") == "true") return false;
	goToNextPage("#divConfirmDialog");
}
function renderingRestartButton() {
	var restartBtnHtml = "<a href='#' data-role='button' type='button' class='div-continue-button' style='padding-left: 0px; padding-right: 0px;width: 100px;float: right;' data-command='restart' onclick='restart(this);'>Restart</a>";
	$("div.section-logo-steps div.restartbtn-placeholder").html(restartBtnHtml);

}

function gotoToUrl(ele,url) {
	if ($(ele).attr("contenteditable") == "true") return false;
	window.location = url;
}

//handle footer content new ui
function handleFooterContent(hasFooterRight) {
	if (hasFooterRight == "Y") {
		var col1Elem = $('.divfooterright-content');
		var col2Elem = $('.divfooterright-logo');
		var length1 = col1Elem.length;
		var length2 = col2Elem.length;
		if (length1 > 0) {
			if (col1Elem.children().length == 0 && col1Elem.text().trim().length == 0) {
				length1 = 0;
			}
		}
		if (length2 > 0) {
			if (col2Elem.children().length == 0 && col2Elem.text().trim().length == 0) {
				length2 = 0;
			}
		}

		if (length1 > 0 && length2 > 0) {
			col1Elem.addClass('col-sm-9 col-md-10 footerTextAlign');
			col2Elem.addClass('col-sm-3 col-md-2 footerTextAlign');
		} else if (length1 > 0 && length2 == 0) {
			col1Elem.addClass('col-sm-12 col-md-12 footerTextAlign');
		} else if (length1 == 0 && length2 > 0) {
			col2Elem.addClass('col-sm-12 col-md-12 footerTextAlign');
		}
		$('.divfooterright').children().find('img').addClass('img-responsive');
	}
} //end handle footer content 

/*remove ui-bar- and ui button*/
$(document).on('pagecontainerbeforeshow', function () {
	if (gl_content_data_theme != 'c') {
		$('.ui-content').css('border', 'hidden');
	}
	var currentPage = '#' + $.mobile.pageContainer.pagecontainer('getActivePage').attr('id');
	//remove default theme and add current theme for current page
	if ($(currentPage).hasClass('ui-page-theme-a')) {
		$(currentPage).removeClass('ui-page-theme-a');
	}
	if ($(".ui-content", currentPage).hasClass('ui-body-a')) {
		$(".ui-content", currentPage).removeClass('ui-body-a');
	}
	handleFooterContent(gl_has_footer_right);

});
/*handle hide and show divfooter for new jquery version: */
$(document).on('pageshow', function () {
	var pageId = $.mobile.pageContainer.pagecontainer('getActivePage').attr('id');
	var currentPage = $("#" + pageId);
	var hideFooter = false;
	//maybe we don't need to show footer when dialog is shown
	if (typeof currentPage != "undefined" && currentPage != null) {
		if (currentPage.data("role") != undefined && currentPage.data("role").toUpperCase() === "DIALOG") {
			hideFooter = true;
		}
	}
	//if (pageId == "divErrorDialog" || pageId == "divConfirmDialog" ) {
	if (hideFooter) {
		$('.divfooter').hide();
	} else {
		$('.divfooter').show();
	}

	//use header_theme2 color for clickable standalone links in final page
	$('#divOnlineUserRegistration').mouseenter(function () {
		var element = $(this);
		element.css('opacity', 0.8);
	});
	$('#divOnlineUserRegistration').mouseleave(function () {
		var element = $(this);
		element.css('opacity', 1);
	});

});
//rendering Restart Button

$(function () {
   
	$("#divErrorPopup").bind({
		popupafterclose: function (event, ui) {
			if ($("div.notifyjs-container:visible").length > 0) {
				//$('html, body').scrollTop($("div.notifyjs-container:visible").offset().top - 100);
				$('html, body').stop().animate({ scrollTop: $("div.notifyjs-container:visible").offset().top - 100 }, '500', 'swing');
			}
		}
	});


	if (hasRestartButton == "Y") {
		renderingRestartButton();
		//$(document).on('pagecontainerbeforeshow', function () {
		//  renderingRestartButton();
		//});

		$(document).on('pageshow', function () {
			$('#btnOk').off('click').on("click", function () {
				//clearForms(); - do not need to do this
				window.location.href = window.location.href;
			});
		});
	}
	if ($("#divErrorPopup").length > 0) {
		$("#divErrorPopup").enhanceWithin().popup();
	}
	if ($("#popUpUndefineCard").length > 0) {
		$("#popUpUndefineCard").enhanceWithin().popup();
	}
	//setp opacity for standalone clickable links
	$('#divMailingAddress, #co_divMailingAddress, #divOtherMonthlyIncome, #co_divOtherMonthlyIncome, #divDisagree, #divCopyAddress, #co_divCopyAddress, #qualifyProduct, .RedirectUrlPage, #divCopyAddress2Property, #divBeneficiaryLink, #divShowReferenceInfoLink, #divComments, #divEmailMeLink, #divTradeInLnk').mouseenter(function () {
		var element = $(this);
		element.css('opacity', 0.8);
	});
	$('#divMailingAddress, #co_divMailingAddress, #divOtherMonthlyIncome, #co_divOtherMonthlyIncome, #divDisagree, #divCopyAddress, #co_divCopyAddress, #qualifyProduct, .RedirectUrlPage, #divCopyAddress2Property, #divBeneficiaryLink, #divShowReferenceInfoLink, #divComments, #divEmailMeLink, #divTradeInLnk').mouseleave(function () {
		var element = $(this);
		element.css('opacity', 1);
	});
	$('.RedirectUrlPage a').css('text-decoration', 'none');
    /*
	//div footer contact color
	var footerContactElem = $('.divfooter_top');
	footerContactElem.css('color', 'white'); //by default

	//lender name color
	var lenderNameElem = $('.divLenderName');
	lenderNameElem.css('color', 'white').css('font-weight', 'bold');//by default
	var footerLinkColor = $('.divFooterLogo').find('a').css('color');
	if (footerLinkColor != undefined) {
		footerContactElem.css('color', footerLinkColor);
		lenderNameElem.css('color', footerLinkColor); //make lender name color the same as link color
	}*/


    //****************
    //Enable the use of back btn, need this bc we disable page acnhor in url
	$(document).on('pagehide', function (event, ui) {
		var lastPage = pageQueue.pop();
		if (lastPage !== ui.nextPage[0].id) {
			history.pushState({ page: ui.prevPage[0].id }, null, window.location.href);
			pageQueue.push(lastPage);
			pageQueue.push(ui.prevPage[0].id);
		}
	});

	$(window).on('popstate', function (e) {
		//	var state = e.originalEvent.state;
		var page = pageQueue[pageQueue.length - 1];
		if (typeof page != "undefined") {
			goToNextPage("#" + page);
		} else {
			history.back();
		}
	});

	//$("body").prepend('<div id="divPageLoading"><div><img src="/images/squares.gif" alt="loading"/><div class="text">Loading...</div></div></div>');
	$("body").prepend('<div id="divAjaxLoading" class="loading-bar-overlay" role="complementary" aria-label="Ajax Loading"><div class="loading-bar-wrapper"><div><div class="main-text">Processing your request...</div><div class="loading-bar-container"><div class="loading-bar"/></div><div class="annotation-text">This may take a few minutes.</div></div></div></div>');

    //**********************
    //Disable pull down refresh on touch devices
	window.addEventListener('load', function () {
	    //var touchmoveHandler = function (e) {
	    //    e.preventDefault();
	    //    return;
		//	if ($("body").scrollTop() <= 0) {
		//		e.preventDefault();
		//		return;
		//	}
		//}
	    //document.addEventListener('touchmove', touchmoveHandler, false);


	    //This version will work with chrome
	    var maybePreventPullToRefresh = false;
	    var lastTouchY = 0;
	    var touchstartHandler = function (e) {
	        if (e.touches.length != 1) return;
	        lastTouchY = e.touches[0].clientY;
	        // Pull-to-refresh will only trigger if the scroll begins when the
	        // document's Y offset is zero.
	        maybePreventPullToRefresh = window.pageYOffset == 0;
	    }

	    var touchmoveHandler = function (e) {
	        var touchY = e.touches[0].clientY;
	        var touchYDelta = touchY - lastTouchY;
	        lastTouchY = touchY;

	        if (maybePreventPullToRefresh) {
	            // To suppress pull-to-refresh it is sufficient to preventDefault the
	            // first overscrolling touchmove.
	            maybePreventPullToRefresh = false;
	            if (touchYDelta > 0) {
	                e.preventDefault();
	                return;
	            }
	        }            
	    }

	    document.addEventListener('touchstart', touchstartHandler, { passive: false });
	    document.addEventListener('touchmove', touchmoveHandler, { passive: false });

	});

	//may need to diable F5(refresh in the future
	//function disableF5(e) { if ((e.which || e.keyCode) == 116 || (e.which || e.keyCode) == 82) e.preventDefault(); };
	//$(document).on("keydown", disableF5); 
   $(window).on('load', function () {
          //ADA compliance for IFrame: adding title attribute to the iframe
       setTimeout(function () {
           $('iframe').each(function (idx, ele) {
               var title = $(ele).attr('title');          
               if (title == undefined || title == "") {
                   $(ele).attr('title', 'iframe viewer' + idx);
               }
           });
       }, 400);       
    });
});
var pageQueue = [];
function toggleReveal(srcEle) {
	$(srcEle).closest(".masking-text").toggleClass("reveal");
}
var pagePaths = [];
function pushToPagePaths(pageName) {
	pagePaths = pagePaths || [];
	if (_.indexOf(pagePaths, pageName) == -1) {
		pagePaths.push(pageName);
	}
}