﻿var LPQDocScan = (function() {
	var docScan = function (pfx, /*container,*/ fillResultFnc) {
		var self = this;
		self.prefix = "";
		//var $container;
		this.hasBackResponse = false;
		// validate params
		//if (!pfx) { throw 'Missing first parameter prefix'; }
		self.prefix = pfx;
		//if (!container || $(container).length !== 1) { throw 'Invalid document scan container'; }
		//$container = $(container);
		var $container = $("#" + self.prefix + "divScanDriverLicense");
		
		function initDesktopCapture() {
			$('#' + self.prefix + 'backImagePlaceHolder').attr('src', "../images/dl_upload.jpg");;
			$("div.desktop-capture", $container).removeClass("hidden");
			$("img.img-thumbnail", $container).on("click", function() {
				navigator.mediaDevices.getUserMedia({ video: { facingMode: { exact: "environment" } } }).then(function (stream) {
					$(".capture-stream video", $container)[0].srcObject = stream;
				}).catch(function(error) {
					$(".desktop-capture", $container).removeClass("active");
				});
				$(".desktop-capture", $container).addClass("active");
			});
			$(".capture-stream .close-btn", $container).on("click", function () {
				var video = $(".capture-stream video", $container)[0];
				$.each(video.srcObject.getVideoTracks(), function(i, track) {
					track.stop();
				});
				$(".capture-stream", $container).removeClass("captured");
				$(".desktop-capture", $container).removeClass("active");
			});
			$(".capture-stream .control-btns .capture-btn", $container).on("click", function () {
				//var canvas = document.createElement("canvas");
				var canvas = $(".capture-stream canvas", $container)[0];
				var video = $(".capture-stream video", $container)[0];
				canvas.width = video.videoWidth;
				canvas.height = video.videoHeight;
				canvas.getContext('2d').drawImage(video, 0, 0);
				//image/png
				$(".capture-stream img", $container)[0].src = canvas.toDataURL();
				$(".capture-stream", $container).addClass("captured");
			});
			$(".capture-stream .control-btns .cancel-btn", $container).on("click", function () {
				$(".capture-stream", $container).removeClass("captured");
			});
			$(".capture-stream .control-btns .continue-btn", $container).on("click", function () {
				setTimeout(function () {
					var canvas = $(".capture-stream canvas", $container)[0];
					var imagePlaceHolder = $("img.img-thumbnail", $container);
					imagePlaceHolder.attr('src', canvas.toDataURL());

					////for testing
					//var testImg = new Image();
					//testImg.src = "../images/back_license3_2mpxil.jpg";
					//var canvas = document.createElement("canvas");
					//canvas.width = 1708;
					//canvas.height = 1216;
					//testImg.onload = function() {
					//	canvas.getContext('2d').drawImage(testImg, 0, 0);
					//	$("#testImg1").attr("src", canvas.toDataURL());
					//	getBackImageInfor(canvas.toDataURL());
					//}
					////end test

					getBackImageInfor(canvas.toDataURL());
				}, 100); //end setTimeOut function
				$.mobile.loading("show", {
					theme: "a",
					text: "Scanning ... please wait",
					textonly: true,
					textVisible: true
				});
				var video = $(".capture-stream video", $container)[0];
				$.each(video.srcObject.getVideoTracks(), function (i, track) {
					track.stop();
				});
				$(".capture-stream", $container).removeClass("captured");
				$(".desktop-capture", $container).removeClass("active");
			});
		}
		var fillResult = function(docsResponse) {
			if (docsResponse != "" && self.prefix != null) {
				var res = JSON.parse(docsResponse);
				$('#' + self.prefix + 'txtFName').val(res.firstName).trigger("blur");
				$('#' + self.prefix + 'txtLName').val(res.lastName).trigger("blur");
				$('#' + self.prefix + 'txtMName').val(res.middleName).trigger("blur");
				$('#' + self.prefix + 'txtDOB').val(res.dateOfBirth).trigger("blur");
				
				//new format
				$('#' + self.prefix + 'txtDOB1').val(res.dateOfBirth.substring(0, 2)); //month
				$('#' + self.prefix + 'txtDOB2').val(res.dateOfBirth.substring(3, 5)); //day
				$('#' + self.prefix + 'txtDOB3').val(res.dateOfBirth.substring(6, 10)); //year
				$('#' + self.prefix + 'divDOB').trigger("blur");
				var gender = res.gender;
				if (gender != undefined) {
					gender = gender.toUpperCase();
				}
				if (gender == 'M') {
					gender = "MALE";
				} else if (gender == 'F') {
					gender = "FEMALE";
				}
				$('#' + self.prefix + 'gender').val(gender).trigger("blur");
				if ($('#' + self.prefix + 'gender').data("mobile-selectmenu") === undefined) {
					$('#' + self.prefix + 'gender').selectmenu(); //not initialized yet, lets do it
				}
				$('#' + self.prefix + 'gender').selectmenu('refresh');
				//address
				var fillAddress = false;
				if ($('#' + self.prefix + 'txtZip').is(":disabled") == false) {
					fillAddress = true;
				} else {
					var match = /([0-9]{5}).*?/g.exec(res.zip);
					if (match != null && match.length > 0 && match[1] == $('#' + self.prefix + 'txtZip').val()) {
						$('#' + self.prefix + 'spVerifyMessage').hide();
						fillAddress = true;
					} else {
						$('#' + self.prefix + 'spVerifyMessage').text("System can't prefill due to ZIP code discrepancy");
						$('#' + self.prefix + 'spVerifyMessage').show();
					}
				}
				if (fillAddress == true) {
					$('#' + self.prefix + 'txtAddress').val(res.streetAddress).trigger("blur");
					$('#' + self.prefix + 'txtCity').val(res.city).trigger("blur");
					$('#' + self.prefix + 'txtZip').val(res.zip).trigger("blur");
					$('#' + self.prefix + 'ddlState').val(res.state).trigger("blur");
					if ($('#' + self.prefix + 'ddlState').data("mobile-selectmenu") === undefined) {
						$('#' + self.prefix + 'ddlState').selectmenu(); //not initialized yet, lets do it
					}
					$('#' + self.prefix + 'ddlState').selectmenu('refresh');
				} else {
					$('#' + self.prefix + 'txtAddress').val("");
					$('#' + self.prefix + 'txtCity').val("");
					$('#' + self.prefix + 'ddlState').val("");
					if ($('#' + self.prefix + 'ddlState').data("mobile-selectmenu") === undefined) {
						$('#' + self.prefix + 'ddlState').selectmenu(); //not initialized yet, lets do it
					}
					$('#' + self.prefix + 'ddlState').selectmenu('refresh');
				}


				//driver license
				$('#' + self.prefix + 'txtIDCardNumber').val(res.dlNumber).trigger("blur");
				$('#' + self.prefix + 'txtIDDateIssued').val(res.dlIssueDate).trigger("blur");
				$('#' + self.prefix + 'txtIDDateExpire').val(res.dlExpirationDate).trigger("blur");
				//new format
				$('#' + self.prefix + 'txtIDDateIssued1').val(res.dlIssueDate.substring(0, 2));
				$('#' + self.prefix + 'txtIDDateIssued2').val(res.dlIssueDate.substring(3, 5));
				$('#' + self.prefix + 'txtIDDateIssued3').val(res.dlIssueDate.substring(6, 10));
				$('#' + self.prefix + 'txtIDDateExpire1').val(res.dlExpirationDate.substring(0, 2));
				$('#' + self.prefix + 'txtIDDateExpire2').val(res.dlExpirationDate.substring(3, 5));
				$('#' + self.prefix + 'txtIDDateExpire3').val(res.dlExpirationDate.substring(6, 10));
				$('#' + self.prefix + 'ddlIDState').val(res.state).trigger("blur");
				if ($('#' + self.prefix + 'ddlIDState').data("mobile-selectmenu") === undefined) {
					$('#' + self.prefix + 'ddlIDState').selectmenu(); //not initialized yet, lets do it
				}
				$('#' + self.prefix + 'ddlIDState').selectmenu('refresh');
			}
		}
		function displayDriverLicenseInfor(docsResponse) {
			var res = JSON.parse(docsResponse);
			var strDriverLicenseInfo = "";
			strDriverLicenseInfo += "<i style='font-weight:normal;'>";
			strDriverLicenseInfo += res.firstName + " " + res.middleName + " " + res.lastName + "&nbsp;&nbsp; &nbsp; &nbsp;DOB: " + res.dateOfBirth + ".<br/>";
			strDriverLicenseInfo += res.streetAddress + ". " + res.city + ", " + res.state + " " + res.zip + "<br/>";
			strDriverLicenseInfo += "DL: " + res.dlNumber + "&nbsp;&nbsp;ISS: " + res.dlIssueDate + "&nbsp;&nbsp;EXP: " + res.dlExpirationDate + ".<br/>";
			strDriverLicenseInfo += "</i >";
			return strDriverLicenseInfo;
		}
		this.cleanResult = function () {
			$('#' + self.prefix + 'txtFName').val("");
			$('#' + self.prefix + 'txtLName').val("");
			$('#' + self.prefix + 'txtMName').val("");
			$('#' + self.prefix + 'txtDOB').val("");
			//address
			$('#' + self.prefix + 'txtAddress').val("");
			$('#' + self.prefix + 'txtCity').val("");
			if ($('#' + self.prefix + 'txtZip').is(":disabled") == false) {
				$('#' + self.prefix + 'txtZip').val("");
			}
			$('#' + self.prefix + 'ddlState').val("");
			if ($('#' + self.prefix + 'ddlState').data("mobile-selectmenu") === undefined) {
				$('#' + self.prefix + 'ddlState').selectmenu(); //not initialized yet, lets do it
			}
			$('#' + self.prefix + 'ddlState').selectmenu('refresh');
			$('#' + self.prefix + 'txtIDCardNumber').val("");
			$('#' + self.prefix + 'txtIDDateIssued').val("");
			$('#' + self.prefix + 'txtIDDateExpire').val("");
			$('#' + self.prefix + 'ddlState').val("");
			if ($('#' + self.prefix + 'ddlIDState').data("mobile-selectmenu") === undefined) {
				$('#' + self.prefix + 'ddlIDState').selectmenu(); //not initialized yet, lets do it
			}
			$('#' + self.prefix + 'ddlIDState').selectmenu('refresh');

			$('#' + self.prefix + 'gender').val("");
			if ($('#' + self.prefix + 'gender').data("mobile-selectmenu") === undefined) {
				$('#' + self.prefix + 'gender').selectmenu(); //not initialized yet, lets do it
			}
			$('#' + self.prefix + 'gender').selectmenu('refresh');

		}
		function checkIESupport() {
			var userAgent = navigator.userAgent;
			var isIE = false;
			var strMessage = "Please update this IE browser to a newer version.  Picture upload feature is not available for IE browser version < 10.";
			if (userAgent.indexOf('MSIE') > 0 || userAgent.indexOf('Trident') > 0 || userAgent.indexOf('Edge') > 0) {
				isIE = true;
			}
			if (isIE && parseFloat($.browser.version) < 10.0) {
				return strMessage;
			}
			return "";
		}
		 function displayDialogMessage(errorStr) {
			if (errorStr != "") {
				// An error occurred, we need to bring up the error dialog.
				var nextpage = $('#divErrorDialog');
				$('#txtErrorMessage').html(errorStr);
				if (nextpage.length > 0) {
					$.mobile.changePage(nextpage, {
						transition: "slide",
						reverse: false,
						changeHash: true
					});
				}
			}
		}
		 function getBackImageInfor(base64Data) {
			var url = '/handler/Handler.aspx';
			var sKey = $('#hdScanDocumentKey').val();
			var sLenderRef = $('#hfLenderRef').val();
			//var sStateCode = $('#' + self.prefix + 'ddlDLState option:selected').val();
			var startlength = base64Data.indexOf("base64,") + 7;
			var sbase64Data = base64Data.substring(startlength, base64Data.length); // remove "data:image/png;base64,"
			var command = 'scanbarcode';
			if (sKey != null) {
				$.ajax({
					url: url,
					async: true,
					cache: false,
					global: false,
					type: 'POST',
					dataType: 'html',
					data: {
						command: command,
						sKey: sKey,
						//lender_id: sLenderId,
						lenderref: sLenderRef,
						//sStateCode: sStateCode,
						sBase64data: sbase64Data
					},
					success: function (responseText) {
						if (responseText.indexOf('errorScanDocument') > -1 || responseText == "") {
							var decodedMessage = "Could not extract document data. Please retake photo OR proceed with application.";
							$('#' + self.prefix + 'divBackMessage').html(decodedMessage);
						} else {
							if (responseText != "") {
								storeScanResult(responseText);
								$('#' + self.prefix + 'divBackMessage').html("<b>Got it! The system will prefill your details with the data below.</b><br/>" + displayDriverLicenseInfor(responseText));
								self.hasBackResponse = true;
							}
						}
					},
					error: function (e) {
						if (e.responseText != "") {
							$('#' + self.prefix + 'divBackMessage').html("Error Message: " + e.responseText);
						} else {
							$('#' + self.prefix + 'divBackMessage').html("Unable to scan the document data");
						}
					}
				}).always(function () {
					$.mobile.loading('hide');
				});
			}
		}
		 function storeScanResult(responseText) {
			$("#" + self.prefix + "scanResult").text(responseText);
		}
		 function retrieveScanResult() {
			return $("#" + self.prefix + "scanResult").text();
		}
		 function resizeDriverLicenseImage(objectUrl, image, maxSize) {
			// We create an image to receive the Data URI
			var canvas = document.createElement('canvas');
			var ctx = canvas.getContext('2d');
			var img = new Image();
			img.src = objectUrl;

			img.onload = function () {
				//get the original dimension of a image
				var width = img.width;
				var height = img.height;
				var imagePixel = width * height;
				var maxPixels = 2097152; //limit 2 mega pixels = 1024 *1024 *2
				var ratio = width / height;
				var resizeWidth = 0;
				var resizeHeight = 0;
				// ratios = resizeWidth/resizeHeight
				// ->calculate resizeWidth and resizeHeight base on ratio
				if (imagePixel > maxPixels) {
					resizeWidth = Math.sqrt(maxPixels * ratio);
					resizeHeight = maxPixels / resizeWidth;
					//round up resizewidth and resizeheight
					width = Math.floor(resizeWidth);
					height = Math.floor(resizeHeight);
				}
				canvas.width = width;
				canvas.height = height;
				ctx.drawImage(img, 0, 0, width, height);
				//reduce the size of the image base on the quality
				//full quality is 1.0, medium quality is .5,and low quality is 0.1
				//now we reduce the image size from 0.5 to 1.0
				var x = 0.91; // the default quality for firefox , and for google is 0.92
				var imageSize;
				var datas = "";
				//image.src = datas;      
                var imageSizeArray = [];
                for (var i = 0; i < 89; i++) {
                    var quality = parseFloat((x).toFixed(2));
                    datas = canvas.toDataURL("image/jpeg", quality);
                    imageSize = (datas.length - 23) * 3 / 4 / 1024;
                    imageSizeArray.push(imageSize);                
                    if (imageSize < maxSize || x <= 0.1 /*low quality -->no more recompress*/) {                  
                        image.attr('src', datas);
                        getBackImageInfor(datas);
                        break;
                    }
                    //reducing the number of looping to do the compression.            
                    var numOfCompress = 0;
                    if (i > 0) {
                        var compressSize = imageSizeArray[i - 1] - imageSizeArray[i];                      
                        var compressTotalSize = imageSize - maxSize;   
                        numOfCompress = (compressTotalSize / compressSize).toFixed(2);
                    }
                    if ((numOfCompress / 2).toFixed(2) > 1) {
                        x -= 0.01 * (numOfCompress / 2);
                    } else {
                        x -= 0.01;
                    } 
                }            
			};

		}
		 function scanImage(element, e) {
			element = $(element);
			if (element && element.val() != "") {
				//the IE browser version less than 10 is not support API file, therefore need to check
				//browsers before upload files.
				//  var isIE = $.browser.msie; //check IE browser --> not working for IE > 9
				var isIE = false;
				var userAgent = navigator.userAgent;
				if (userAgent.indexOf('MSIE') > 0 || userAgent.indexOf('Trident') > 0 || userAgent.indexOf('Edge') > 0) {
					isIE = true;
				}
				if (!isIE || (isIE && parseFloat($.browser.version) > 9.0)) {
					var fileName = element.val();
					var extension = element.val().split('.').pop().toUpperCase();
					var imagePlaceHolder = $('#' + self.prefix + 'backImagePlaceHolder');
					if (extension != "PNG" && extension != "JPG" && extension != "GIF" && extension != "JPEG") {
						fileName = fileName.substring(fileName.lastIndexOf("\\") + 1, fileName.length);
						// alert("The file '" + fileName + "' is invalid. Please check the input file.");
						var errorStr = "The file '" + fileName + "' is invalid. Please check the input file.";
						displayDialogMessage(errorStr);
						imagePlaceHolder.attr('src', "../images/dl_back_sample.jpg"); //clear the file image

					} else {
						if (fileName.indexOf("images/dl_back_sample.jpg") != -1) {
							return; // clear the image input -> no need to scan
						}
						var file = e.target.files[0];
						var fileReader = new FileReader();
						var imageSize = parseInt(file.size, 10) / 1024;
						var objectUrl = URL.createObjectURL(file);
						var maxSize = 100;
						$.mobile.loading("show", {
							theme: "a",
							text: "Scanning ... please wait",
							textonly: true,
							textVisible: true
						});
                        setTimeout(function (){                       
                            if(imageSize > maxSize) { //need to resize                           
								resizeDriverLicenseImage(objectUrl, imagePlaceHolder, maxSize);
							} else {
								if (file) {
									fileReader.readAsDataURL(file);
								}
								fileReader.onloadend = function () {
									imagePlaceHolder.attr('src', fileReader.result);
									getBackImageInfor(fileReader.result);
								}
							}
						}, 100); //end setTimeOut function

					}

				} else {
					alert("Please update this IE browser to a newer version.  Picture upload feature is not available for IE browser version < 10.");
				}
			}
			$("#" + self.prefix + "fileupload-backImage").fileupload("clear");
		}


		$('#' + self.prefix + 'backImage-thumbnail').click(function () {
			if (checkIESupport() == "") {
				$('#' + self.prefix + 'inputBackImage').click();
			} else {
				displayDialogMessage(checkIESupport());
			}
		});
		$('#' + self.prefix + 'inputBackImage').on('change', function (e) {
			$('#' + self.prefix + 'divBackMessage').html('');
			self.hasBackResponse = false;
			scanImage($(this), e);
		});
		//allow to override the fillResult function if needed
		if ($.isFunction(fillResultFnc)) fillResult = fillResultFnc;

		$('#' + self.prefix + 'backImagePlaceHolder').on('click', function () {
			$('#' + self.prefix + 'backImage-thumbnail').trigger('click');
		});
		this.scanAccept = function () {
			var scanResult = retrieveScanResult();
			if ($.trim(scanResult) != "") {
				//var resultObj = JSON.parse(scanResult);
				//for main applicant, if the zippool feature is enabled and zip has been set in the first step, do not override applicant info from scan result
				//may be these line of code need to move out
				
				//var match = /([0-9]{5}).*?/g.exec(resultObj.zip);
				//if ($('#' + self.prefix + 'txtZip').is(":disabled") && match != null && match.length > 0 && match[1] != $('#' + self.prefix + 'txtZip').val()) {
				//	if ($("#" + self.prefix + "popScanZipWarning").length == 1) {
				//		$("#" + self.prefix + "popScanZipWarning").popup("open", { "positionTo": "window" });
				//	}
				//	return false;
				//}
				fillResult(scanResult);
			}
			return true;
        }
        //Due to very high chance the camera will not work with PC, the camera feature is disabled by disabling the loop below with a false condition.
		//check browser suportted
		if (false && 'mediaDevices' in navigator && navigator.mediaDevices.getUserMedia && navigator.mediaDevices.enumerateDevices && !isMobile.any()) {
			navigator.mediaDevices.enumerateDevices().then(function(devices) {
				if (devices != null && devices.length > 0 && _.some(devices, { kind: "videoinput" })) {
					initDesktopCapture();
				}
			});
		}
		if (!isMobile.any()) {
			$('#' + self.prefix + 'backImagePlaceHolder').attr('src', "../images/dl_upload.jpg");
		}

	}
	
	return docScan;
})();