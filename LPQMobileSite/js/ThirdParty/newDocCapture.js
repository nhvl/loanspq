﻿var LPQDocCapture = (function () {
	var docCapture = function(options) {
		var self = this;
		var defaultSettings = {
			prefix: "",
			requireDocTitle: true,
			docTitleOptions: [],
			oneColumnLayout: false,
			requireDocUpload: false
		};
		var docArray = [];
		var itemIndex = 0;
		var settings = $.extend({}, defaultSettings, options);
		var imageSelected = true;
		var imageLoaded = false;
		$("#" + settings.prefix + "imagePlaceHolder").click(function() {
			if (detectValidBrowserVersion()) {
				if (imageSelected) { //first selected
					if (!imageLoaded) {
						$("#" + settings.prefix + "image-thumbnail").trigger('click');
					}
				} else {
					//finish loadding continue to get next images
					if (imageLoaded) {
						imageLoaded = false; //reset
						$("#" + settings.prefix + "image-thumbnail").trigger('click');
					}
					imageSelected = true; //reset
				}
			} else {
				showErrorDialog("Please update this IE browser to a newer version. Picture upload feature is not available for IE browser version < 10.");
			}
		});
		//fire the trigger to open File Dialog in case of using keyboard to support AT(assistive technology) - WCAG
		$("#" + settings.prefix + "imagePlaceHolder").parent().on("keydown", function(event) {
			if (event.which == 13) {
				$("#" + settings.prefix + "imagePlaceHolder").trigger("click");
			}
		});

		$("#" + settings.prefix + "DocUploadSrcSelector").on("pagehide", function (event, ui) {
			$('html, body').stop().animate({ scrollTop: $("#" + settings.prefix + "divDocCaptureMessage").offset().top - 100 }, '500', 'swing');
		});

        //Due to very high chance the camera will not work with PC, the camera feature is disabled by disabling the loop below with a false condition
		if (false && 'mediaDevices' in navigator && navigator.mediaDevices.getUserMedia && navigator.mediaDevices.enumerateDevices && !isMobile.any()) {
			navigator.mediaDevices.enumerateDevices().then(function (devices) {
				if (devices != null && devices.length > 0 && _.some(devices, { kind: "videoinput" })) {
					var $container = $("#" + settings.prefix + "DocUploadSrcSelector");
					$("div[data-command='upload']", $container).on("click", function () {
						$.mobile.back();
						setTimeout(function() {
							//$('html, body').stop().animate({ scrollTop: $("#" + settings.prefix + "divDocCaptureMessage").offset().top - 100 }, '500', 'swing');
							$("#" + settings.prefix + "btnFileCamera").click();
						}, 100);
					});
					$("div[data-command='capture']", $container).on("click", function () {
						navigator.mediaDevices.getUserMedia({ video: { facingMode: { exact: "environment" } } }).then(function (stream) {
							$("div.capture-stream video", $container)[0].srcObject = stream;
						}).catch(function (error) {
							$("div.capture-stream", $container).removeClass("active");
						});
						$("div.capture-stream", $container).addClass("active");
					});
					$("div.capture-stream .close-btn", $container).on("click", function () {
						var video = $("div.capture-stream video", $container)[0];
						$.each(video.srcObject.getVideoTracks(), function (i, track) {
							track.stop();
						});
						$("div.capture-stream", $container).removeClass("captured active");
					});
					$("div.capture-stream .control-btns .capture-btn", $container).on("click", function () {
						//var canvas = document.createElement("canvas");
						var canvas = $("div.capture-stream canvas", $container)[0];
						var video = $("div.capture-stream video", $container)[0];
						canvas.width = video.videoWidth;
						canvas.height = video.videoHeight;
						canvas.getContext('2d').drawImage(video, 0, 0);
						//image/png
						$("div.capture-stream img", $container)[0].src = canvas.toDataURL();
						$("div.capture-stream", $container).addClass("captured");
					});
					$("div.capture-stream .control-btns .cancel-btn", $container).on("click", function () {
						$("div.capture-stream", $container).removeClass("captured");
					});

					$("div.capture-stream .control-btns .continue-btn", $container).on("click", function () {
						setTimeout(function () {
							itemIndex++;
							var canvas = $("div.capture-stream canvas", $container)[0];
							var datas = canvas.toDataURL();
							displayImage(datas, itemIndex); //display captured photo
							var head = 'data:image/png;base64,';
							var fileSize = Math.round((datas.length - head.length) * 3 / 4);

							docArray.push(new objectImages(datas, settings.prefix + "image_" + itemIndex, "camera" + itemIndex, fileSize));
							$.mobile.loading('hide');
							imageLoaded = true;
							imageSelected = false;
							$("div.capture-stream", $container).removeClass("captured active");
							$.mobile.back();
						}, 100); //end setTimeOut function
						$.mobile.loading("show", {
							theme: "a",
							text: "Scanning ... please wait",
							textonly: true,
							textVisible: true
						});
						var video = $("div.capture-stream video", $container)[0];
						$.each(video.srcObject.getVideoTracks(), function (i, track) {
							track.stop();
						});
					});


					$("#" + settings.prefix + "image-thumbnail").on("click", function () {
						goToNextPage("#" + settings.prefix + "DocUploadSrcSelector");
					});
				} else {
					$("#" + settings.prefix + "image-thumbnail").on("click", function () {
						$("#" + settings.prefix + "btnFileCamera").click();
					});
				}
			}).catch(function (err) {
				$("#" + settings.prefix + "image-thumbnail").on("click", function () {
					$("#" + settings.prefix + "btnFileCamera").click();
				});
			});
		} else {
			$("#" + settings.prefix + "image-thumbnail").on("click", function () {
				$("#" + settings.prefix + "btnFileCamera").click();
			});
		}
		
		$("#" + settings.prefix + "divImage").on('click', function (event, ui) {
			removeImage(event.target.id);
		});
        function displayImage(urlpix, index) {
            var ddlUploadDocsElem = null;
			if (settings.docTitleOptions != null && settings.docTitleOptions.length > 0) {
				var uniqueOptions = _.uniqBy(settings.docTitleOptions, "Title");
                ddlUploadDocsElem =
                    $("<div />", { class: "col-xs-12 divFooterImageRight " + (settings.docTitleOptions.length == 1 ? "hidden " : ""), style: "padding:0px;10px;" }).append(
                        $("<div />", { class: "divFooterImageRight-left" }).append(
                            $("<select />", { "data-role": "none", id: (settings.prefix + "ddlUploadDoc" + index), class: "ddlUploadDoc-style", style: "width: 90%" }).append(
                                $.map(uniqueOptions, function (item, idx) {
                                    return $("<option />", { value: (/PLEASE\s+SELECT/i.test(item.Title) ? "" : item.Title) }).text(item.Title);
                                })
                            ),
                            $("<span />", { class: (settings.requireDocTitle ? "RequiredIcon" : "") })
                        )
                    );
            }
            var cssLayout = "col-lg-3 col-md-4 col-sm-6 ";
            if ($("#" + settings.prefix + "hdOneColumnLayout").val().toUpperCase() === "Y") {
                cssLayout = "";
            }
            var elem =
                $("<div />", { class: cssLayout + "col-xs-12 text-center", style: "height:330px;", id: settings.prefix + "divImage_" + index }).append(
                    $("<div />", { style: "height:230px;width:100%;", class: "img-thumbnail" }).append(
                        /\.pdf$/i.test(urlpix) // display file name
                            ? $("<label />", { style: "-moz-word-break: break-all;-o-word-break: break-all;word-break: break-all;margin:0;max-height:220px;", id: settings.prefix + "image_" + index, class: "img-responsive fileupload-new divImage" }).text(urlpix)
                            : $("<img />", { id: settings.prefix + "image_" + index, class: "img-responsive fileupload-new divImage", style: "max-height:220px;", src: urlpix })
                    ),
                    $("<div />", { class: "divFooterImage row" }).append(
                        $("<div />", { class: "col-xs-12 divFooterImageLeft Button", id: settings.prefix + "divButton_" + index }).append(
                            $("<button />", { "data-role": "none", class: "btn btn-sm btn-primary footer-theme btnRemove UploadDocBtn-style", type: "button", id: settings.prefix + "btnRemove_" + index }).text("Remove")
                        ),
                        ddlUploadDocsElem
                    ),
                    "<br />"
                );
			$('#' + settings.prefix + 'divImage').append(elem);

			$("#" + settings.prefix + "ddlUploadDoc" + index).observer({
				validators: [
					function (partial) {
						if (settings.requireDocTitle == true && Common.ValidateText($(this).val()) === false) {
							return "Please select title";
						}
						return "";
					}
				],
				validateOnBlur: true,
				group: settings.prefix + "ValidateUploadDocs",
			});
			$.lpqValidate.hideValidation("#" + settings.prefix + "divDocCaptureMessage");
		}

		$("#" + settings.prefix + "btnFileCamera").change(function(e) {

			if ($(this).val() != undefined && $(this).val() != "") {
				//the IE browser version less than 10 is not support API file, therefore need to check
				//browsers before upload files.
				//  var isIE = $.browser.msie; //check IE browser --> not working for IE > 9

				if (detectValidBrowserVersion) {
					var fileName = $(this).val();
					//remove the path only display file name
					fileName = fileName.substring(fileName.lastIndexOf("\\") + 1, fileName.length);

					if (/\.(pdf|jpeg|jpg|gif|png)$/i.test($(this).val())) {
						itemIndex++;

						// show progress indicator when process large file
						$.mobile.loading("show", {
							theme: "a",
							text: "Uploading ... please wait",
							textonly: true,
							textVisible: true
						});
						var file = e.target.files[0];
						var objectUrl = URL.createObjectURL(file);
						var fileSize = parseInt(file.size, 10) / 1024;

						var onLoadedCallback = function(obj) {
							docArray.push(obj);
							$.mobile.loading('hide');
							imageLoaded = true;
							imageSelected = false;
						}

						if (!/\.pdf$/i.test(fileName)) { //image
							displayImage(objectUrl, itemIndex); //display selected image
							if (fileSize > 800) {
								resizeImage(itemIndex, objectUrl, fileName, onLoadedCallback);
							} else {
								var reader = new window.FileReader();
								reader.readAsDataURL(file);
								reader.onloadend = function() {
									var base64Data = reader.result;
									onLoadedCallback(new objectImages(base64Data, settings.prefix + "image_" + itemIndex, fileName, fileSize));
								}
							}
						} else { //pdf
							pdfVersionChecker(file, function(version) {
								if (version >= 1.4) {
									//just display file name
									displayImage(fileName, itemIndex); //display selected image
									var reader = new window.FileReader();
									reader.readAsDataURL(file);
									reader.onloadend = function() {
										var base64Data = reader.result;
										onLoadedCallback(new objectImages(base64Data, settings.prefix + "image_" + itemIndex, fileName, fileSize));
									}
								} else {
									$.mobile.loading('hide'); //hide progress bar
									showErrorDialog("We don't support PDF version below 1.4. Please convert your file to version 1.4 or above and try again.");

								}
							});
						}
						$.mobile.loading('hide'); //hide progress bar
					} else {
						showErrorDialog("The file '" + fileName + "' is invalid. Please check the input file.");
					}
				} else {
					showErrorDialog("Please update this IE browser to a newer version.  Picture upload feature is not available for IE browser version < 10.");
				}
			}
			$("#" + settings.prefix + "fileupload-container").fileupload("clear"); //clear the image
		});
		

		function pdfVersionChecker(file, callback) {
			var reader = new window.FileReader();
			reader.readAsText(file);
			reader.onload = function (e) {
				var header = e.target.result;
				var version = 0;
				header = header.substr(0, 15);
				if (/PDF-([0-9].[0-9]*)/.test(header) == true) {
					var versionStr = /PDF-([0-9].[0-9]*)/.exec(header)[1];
					version = parseFloat(versionStr);
				}
				if ($.isFunction(callback)) {
					callback(version);
				}
			}
		}

		function removeImage(sourceEleId) {
			var index = sourceEleId.replace(/^\w+_/g, '');
			var btnRemoveID = settings.prefix + "btnRemove_" + index;
			if (sourceEleId == btnRemoveID) {
				var imageEleId = sourceEleId.replace("btnRemove", "image");
				var divImage = sourceEleId.replace("btnRemove", "divImage");
				for (var i = 0; i < docArray.length; i++) {
					if (imageEleId == docArray[i].imageID) {
						//remove image in the array
						docArray.splice(i, 1);
						$('#' + imageEleId).remove();//remove image
						$('#' + btnRemoveID).remove(); //remove button
						$('#' + divImage).remove(); //remove divImage;
					}
				}
				$.lpqValidate.removeValidationItem(settings.prefix + "ValidateUploadDocs", settings.prefix + "ddlUploadDoc" + index);
				self.validateUploadDocument();
			}
			//reset image index
			//if (docArray.length == 0) {
			//	itemIndex = 0;
			//	return;
			//}

		}
		self.validateUploadDocument = function() {
            //var strMessage = "";
            var validator = true;
            if (docArray.length > 0) {
                //if the file too big display message
                var uploadSize = 0;
                //Check all applicants.
                //Get all docUploadObj keys.
                var docObjKeys = $.map(Object.keys(window), function(val, i) {
                    if (val.endsWith("docUploadObj")) {
                        return val;
                    }
                });
                if(docObjKeys != null){
                    //Get all file in every docUploadObj and retrieve filesize
                    for (var i = 0; i < docObjKeys.length; i++) {
                        if(window[docObjKeys[i]] != null) {
                            var appDocArray = window[docObjKeys[i]].getDocArray();
                            if(appDocArray != null && appDocArray.length > 0){
                                for (var j = 0; j < appDocArray.length; j++) {
                                    uploadSize += appDocArray[j].fileSize;
                                }
                            }
                        }
                        if(settings.prefix == null || settings.prefix == "" || docObjKeys[i].startsWith(settings.prefix)){
                            //docObjKeys is in order.
                            //just callculate and restrict to current docUploadObj element
                            break;
                        }
                    }
                }
                
				if ($.lpqValidate(settings.prefix + "ValidateUploadDocs") == false) {
					validator = false;
				}
				//2800 =2.8mb
				if (uploadSize > 2800) {
					$.lpqValidate.showValidation("#" + settings.prefix + "divDocCaptureMessage", "Please reduce total file size to less than 2.8MB.");
					//strMessage += "Please reduce total file size to less than 2.8MB.<br/>";
					validator = false;
				} else {
					$.lpqValidate.hideValidation("#" + settings.prefix + "divDocCaptureMessage");
				}
			} else {
				if (settings.requireDocUpload == true) {
					$.lpqValidate.showValidation("#" + settings.prefix + "divDocCaptureMessage", "Please upload your document(s).");
					//strMessage = "Please upload your document(s).<br/>";
					validator = false;
				} else {
					$.lpqValidate.hideValidation("#" + settings.prefix + "divDocCaptureMessage");
				}
			}
			return validator;
		}
		self.updateSettings = function (values) {
			//don't allow to change prefix. Prefix can be set only one time on initial
            if (values.title.length > 0) {            
                $("#" + settings.prefix + "divDocCaptureWrapper").find("div.sub_section_header:first-child div.section-title").html(htmlDecode(values.title));
            }
            settings = $.extend({}, settings, values, { prefix: settings.prefix });
			//reset
			docArray = [];
			itemIndex = 0;
			imageSelected = true;
			imageLoaded = false;
			$('#' + settings.prefix + 'divImage').html("");
			$.lpqValidate.removeValidationGroup(settings.prefix + "ValidateUploadDocs");
		}
		self.setUploadDocument = function (docUploadInfo) {
			if (docArray.length == 0) return docUploadInfo;
			if (docUploadInfo == null) docUploadInfo = {};
			if (!docUploadInfo.hasOwnProperty("sDocArray")) {
				docUploadInfo.sDocArray = new Array();
				docUploadInfo.sDocInfoArray = new Array();
			}
			var incrementIndex = docUploadInfo.sDocArray.length;
			$.each(docArray, function (i, item) {
				var title = item.fileName;
				//limit title 20 characters
				if (title.length > 20) {
					title = ".." + title.substring(title.length - 20);
				}
				title = "(" + (incrementIndex + i + 1) + ")" + title;
				docUploadInfo.sDocArray.push({ title: title, base64data: item.base64data });
				if (settings.docTitleOptions != null && settings.docTitleOptions.length > 0) {
					var selectedOptionVal = $("#" + settings.prefix + item.imageID.replace(/^\w+_/g, 'ddlUploadDoc')).val();
					var selectedOptions = _.filter(settings.docTitleOptions, { Title: selectedOptionVal });
					if (typeof selectedOptions == "undefined" || selectedOptions.length == 0) {
						selectedOptions = [{ Title: "", Group: "", Code: "", LoanType: "" }]
					}
					selectedOptions.forEach(function (selectedOption) {
						docUploadInfo.sDocInfoArray.push({
							docTitle: selectedOption.Title,
							docGroup: selectedOption.Group,
							docCode: selectedOption.Code,
							docLoanType: selectedOption.LoanType,
							docFileName: title
						});
					});
				} else {
					// This may be when there are no doc titles setup. We still need to send a DocInfo object
					docUploadInfo.sDocInfoArray.push({
						docTitle: "",
						docGroup: "",
						docCode: "",
						docLoanType: "",
						docFileName: title,
					});
				}
			});
		}
		self.getDocArray = function(){
			return docArray;
		}
		function objectImages(base64data, imageID, fileName, fileSize) {
			this.base64data = base64data;
			this.imageID = imageID;
			this.fileName = fileName;
			this.fileSize = fileSize;
		}

		function detectValidBrowserVersion() {
			var userAgent = navigator.userAgent;
			return !((userAgent.indexOf('MSIE') > 0 || userAgent.indexOf('Trident') > 0 || userAgent.indexOf('Edge') > 0) && parseFloat($.browser.version) < 10.0);
		}
		function resizeImage(index, objectUrl, fileName, onLoadedCallback) {
			// We create an image to receive the Data URI
			var image = document.getElementById(settings.prefix +'image_' + index);
			var canvas = document.createElement('canvas');
			var ctx = canvas.getContext('2d');
			var img = new Image(); 
			img.src = objectUrl;
			img.onload = function () {
				//get original dimension
				var width = img.width;
				var height = img.height;
				var imagePixel = width * height;
				var maxPixels = 2097152; //limit 2 mega pixels = 1024 *1024 *2
				var ratio = width / height;
				var resizeWidth = 0;
				var resizeHeight = 0;
				// ratios = resizeWidth/resizeHeight
				// ->calculate resizeWidth and resizeHeight base on ratio
				if (imagePixel > maxPixels) {
					resizeWidth = Math.sqrt(maxPixels * ratio);
					resizeHeight = maxPixels / resizeWidth;
					//round up resizewidth and resizeheight
					width = Math.floor(resizeWidth);
					height = Math.floor(resizeHeight);
				}
				canvas.width = width;
				canvas.height = height;
				ctx.drawImage(img, 0, 0, width, height);
				//reduce the size of the image base on the quality
				//full quality is 1.0, medium quality is .5,and low quality is 0.1
				//now we reduce the image size from 0.5 to 1.0    
				var x = 1.0;
				var imageSize;
				var datas;
				var result = null;
				for (var i = 0; i < 100; i++) {
					var quality = parseFloat((x).toFixed(2));
					datas = canvas.toDataURL("image/jpeg", quality);
					imageSize = (datas.length - 23) * 3 / 4 / 1024;
					if (imageSize < 800) {
						image.src = datas;
						result = new objectImages(datas, settings.prefix + 'image_' + index, fileName, imageSize);
						break;  //exit the loop
					} else if (quality < 0.40) {  //less than medium quality just break
						result = new objectImages(datas,settings.prefix +'image_' + index, fileName, imageSize);
						break;
                    } 
					x -= 0.02;
   
				}
				onLoadedCallback(result);
			};
		}
		function showErrorDialog(errorStr) {
			if (errorStr != "") {
				// An error occurred, we need to bring up the error dialog.
				if ($("#divErrorPopup").length > 0) {
					$('#txtErrorPopupMessage').html(errorStr);
					$("#divErrorPopup").popup("open", { "positionTo": "window" });
				} else {
					var nextpage = $('#divErrorDialog');
					$('#txtErrorMessage').html(errorStr);
					if (nextpage.length > 0) {
						$.mobile.changePage(nextpage, {
							transition: "slide",
							reverse: false,
							changeHash: true
						});
					}
				}
			}
		}

		
	}
	return docCapture;
})();