﻿//Piwik For Calcubot purpose
var currentURL = function (titlePath) {
    if (titlePath == null) { titlePath = '.ui-page-active .ui-title'; }
    var title = jQuery(titlePath), currentStage = null, trackedURL = null;
    if (title.length > 0 && title[0].innerHTML.length > 0) { currentStage = title[0].innerHTML.replace(/^\s+|\s+$/g, ''); }
    else if ((title = jQuery('.ui-page-active .ui-content div[id]')).length > 0) { currentStage = title[0]['id'].replace(/^\s+|\s+$/g, ''); }
    if (currentStage != null && typeof (currentStage) != 'undefined') {
        trackedURL = window.location.protocol + "//" + window.location.host + window.location.pathname + "/" + currentStage.toLowerCase().replace(/\s|[\/]/g, '-');
        var params = (window.location.search || window.location.hash); if (params != null && params != '') { trackedURL += params; }
    } else { trackedURL = window.location.href; }
    return { stage: currentStage, trackedURL: trackedURL };
};

var _paq = _paq || [];
jQuery(document).ready(function () {
    var currURL = currentURL('.ui-page .ui-title');
    _paq.push(['setCustomUrl', currURL.trackedURL]);
    _paq.push(['trackPageView', currURL.stage]);
    _paq.push(['enableLinkTracking']);
    (function () {
        var u = "//analytics.loanspq.com/";
        _paq.push(['setTrackerUrl', u + 'piwik.php']);
        _paq.push(['setSiteId', 37]);
        var d = document, g = d.createElement('script'), s = d.getElementsByTagName('script')[0];
        g.type = 'text/javascript'; g.async = true; g.defer = true; g.src = u + 'piwik.js'; s.parentNode.insertBefore(g, s);
    })();
    setTimeout(function () {
        jQuery(window).on("pageshow", function (event) {
            var currURL = currentURL();
            _paq.push(['setCustomUrl', currURL.trackedURL]);
            _paq.push(['trackPageView', currURL.stage]);
        });
    }, 800);
});

