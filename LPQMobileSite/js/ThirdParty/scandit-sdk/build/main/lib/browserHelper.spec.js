"use strict";
/* tslint:disable:no-implicit-dependencies */
/**
 * BrowserHelper tests
 */
Object.defineProperty(exports, "__esModule", { value: true });
const ava_1 = require("ava");
const scandit_sdk_1 = require("scandit-sdk");
ava_1.test("checkBrowserCompatibility", t => {
    window.Blob = null;
    t.deepEqual(scandit_sdk_1.BrowserHelper.checkBrowserCompatibility(), new scandit_sdk_1.CustomError({
        name: "UnsupportedBrowserError",
        message: "Media devices are not supported on this OS / Browser"
    }));
    window.navigator.mediaDevices = {
        getUserMedia: () => {
            return;
        }
    };
    t.deepEqual(scandit_sdk_1.BrowserHelper.checkBrowserCompatibility(), new scandit_sdk_1.CustomError({
        name: "UnsupportedBrowserError",
        message: "Web Worker is not supported on this OS / Browser"
    }));
    window.Worker = () => {
        return;
    };
    t.deepEqual(scandit_sdk_1.BrowserHelper.checkBrowserCompatibility(), new scandit_sdk_1.CustomError({
        name: "UnsupportedBrowserError",
        message: "WebAssembly is not supported on this OS / Browser"
    }));
    window.WebAssembly = {};
    t.deepEqual(scandit_sdk_1.BrowserHelper.checkBrowserCompatibility(), new scandit_sdk_1.CustomError({
        name: "UnsupportedBrowserError",
        message: "Blob object is not supported on this OS / Browser"
    }));
    window.Blob = () => {
        return;
    };
    t.deepEqual(scandit_sdk_1.BrowserHelper.checkBrowserCompatibility(), new scandit_sdk_1.CustomError({
        name: "UnsupportedBrowserError",
        message: "URL object is not supported on this OS / Browser"
    }));
    window.URL = {
        createObjectURL: () => {
            return;
        }
    };
    t.deepEqual(scandit_sdk_1.BrowserHelper.checkBrowserCompatibility(), undefined);
});
ava_1.test("getDeviceId", t => {
    const currentDeviceId = scandit_sdk_1.BrowserHelper.getDeviceId();
    t.regex(currentDeviceId, /[0-9a-f]{40}/);
    t.deepEqual(scandit_sdk_1.BrowserHelper.getDeviceId(), currentDeviceId);
});
//# sourceMappingURL=browserHelper.spec.js.map