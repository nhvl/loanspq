import { Barcode } from "./barcode";
/**
 * A result of a scanning operation on an image.
 */
export interface ScanResult {
    /**
     * The list of barcodes found in the image (can be empty).
     */
    readonly barcodes: Barcode[];
}
