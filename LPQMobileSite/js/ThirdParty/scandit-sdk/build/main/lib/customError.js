"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
/**
 * @hidden
 */
class CustomError extends Error {
    constructor({ name = "", message = "" } = {}) {
        super(message);
        Object.setPrototypeOf(this, CustomError.prototype);
        this.name = name;
    }
}
exports.CustomError = CustomError;
//# sourceMappingURL=customError.js.map