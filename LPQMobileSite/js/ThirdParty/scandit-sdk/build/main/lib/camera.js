"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var Camera;
(function (Camera) {
    /**
     * Camera type (not guaranteed to be correct, depending on the device).
     */
    let Type;
    (function (Type) {
        /**
         * Front facing camera.
         */
        Type[Type["FRONT_DEPRECATED"] = 0] = "FRONT_DEPRECATED";
        /**
         * Back facing camera.
         */
        Type[Type["BACK_DEPRECATED"] = 1] = "BACK_DEPRECATED";
        /**
         * Front facing camera.
         */
        Type["FRONT"] = "front";
        /**
         * Back facing camera.
         */
        Type["BACK"] = "back";
    })(Type = Camera.Type || (Camera.Type = {}));
})(Camera = exports.Camera || (exports.Camera = {}));
//# sourceMappingURL=camera.js.map