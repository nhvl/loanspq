"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const customError_1 = require("./customError");
/**
 * @hidden
 */
var BrowserHelper;
(function (BrowserHelper) {
    /**
     * @returns The first matching error indicating a missing browser feature, or undefined.
     */
    function checkBrowserCompatibility() {
        if (window.navigator.mediaDevices == null || typeof window.navigator.mediaDevices.getUserMedia !== "function") {
            return new customError_1.CustomError({
                name: "UnsupportedBrowserError",
                message: "Media devices are not supported on this OS / Browser"
            });
        }
        if (!window.hasOwnProperty("Worker") || typeof window.Worker !== "function") {
            return new customError_1.CustomError({
                name: "UnsupportedBrowserError",
                message: "Web Worker is not supported on this OS / Browser"
            });
        }
        if (!window.hasOwnProperty("WebAssembly") || typeof window.WebAssembly !== "object") {
            return new customError_1.CustomError({
                name: "UnsupportedBrowserError",
                message: "WebAssembly is not supported on this OS / Browser"
            });
        }
        if (!window.hasOwnProperty("Blob") || typeof window.Blob !== "function") {
            return new customError_1.CustomError({
                name: "UnsupportedBrowserError",
                message: "Blob object is not supported on this OS / Browser"
            });
        }
        if (!window.hasOwnProperty("URL") ||
            (typeof window.URL !== "function" && typeof window.URL !== "object") ||
            typeof window.URL.createObjectURL !== "function") {
            return new customError_1.CustomError({
                name: "UnsupportedBrowserError",
                message: "URL object is not supported on this OS / Browser"
            });
        }
        return undefined;
    }
    BrowserHelper.checkBrowserCompatibility = checkBrowserCompatibility;
    /**
     * Get a value from a cookie.
     *
     * @param key The key for the cookie for which to get the value.
     * @returns The cookie value for the given key, empty string if not found.
     */
    function getCookieValue(key) {
        const cookieMatch = document.cookie.match(`(^|;)\\s*${key}\\s*=\\s*([^;]+)`);
        if (cookieMatch == null) {
            return "";
        }
        else {
            const cookieValue = cookieMatch.pop();
            // istanbul ignore next
            return cookieValue == null ? "" : cookieValue;
        }
    }
    /**
     * Store a value in a cookie.
     *
     * @param key The key for the cookie.
     * @param value The value of the cookie.
     * @param expirationDays The amount of days after which the cookie will expire.
     */
    function setCookieValue(key, value, expirationDays) {
        const date = new Date();
        date.setTime(date.getTime() + expirationDays * 24 * 60 * 60 * 1000);
        document.cookie = `${key}=${value};expires=${date.toUTCString()}`;
    }
    /**
     * Get a device id for the current browser, when available it's retrieved from a cookie,
     * when not it's randomly generated and stored in a cookie to be retrieved by later calls.
     *
     * @returns The device id for the current browser.
     */
    function getDeviceId() {
        const cookieKey = "scandit-device-id";
        const storedDeviceId = getCookieValue(cookieKey);
        if (storedDeviceId !== "") {
            return storedDeviceId;
        }
        const hexCharacters = "0123456789abcdef";
        let randomDeviceId = "";
        for (let i = 0; i < 40; ++i) {
            // tslint:disable-next-line:insecure-random
            randomDeviceId += hexCharacters.charAt(Math.floor(Math.random() * 16));
        }
        setCookieValue(cookieKey, randomDeviceId, 3650);
        return randomDeviceId;
    }
    BrowserHelper.getDeviceId = getDeviceId;
})(BrowserHelper = exports.BrowserHelper || (exports.BrowserHelper = {}));
//# sourceMappingURL=browserHelper.js.map