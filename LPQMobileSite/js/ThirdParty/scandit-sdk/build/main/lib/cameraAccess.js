"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const browserHelper_1 = require("./browserHelper");
const camera_1 = require("./camera");
const customError_1 = require("./customError");
/**
 * A helper object to interact with cameras.
 */
var CameraAccess;
(function (CameraAccess) {
    /**
     * @hidden
     *
     * Handle localized camera labels. Supported languages:
     * English, German, French, Spanish (spain), Portuguese (brasil), Portuguese (portugal), Italian,
     * Chinese (simplified), Chinese (traditional), Japanese, Russian, Turkish, Dutch, Arabic, Thai, Swedish,
     * Danish, Vietnamese, Norwegian, Polish, Finnish, Indonesian, Hebrew, Greek, Romanian, Hungarian, Czech,
     * Catalan, Slovak, Ukraininan, Croatian, Malay, Hindi.
     */
    const backCameraKeywords = [
        "rear",
        "back",
        "rück",
        "arrière",
        "trasera",
        "trás",
        "traseira",
        "posteriore",
        "后面",
        "後面",
        "背面",
        "后置",
        "後置",
        "背置",
        "задней",
        "الخلفية",
        "후",
        "arka",
        "achterzijde",
        "หลัง",
        "baksidan",
        "bagside",
        "sau",
        "bak",
        "tylny",
        "takakamera",
        "belakang",
        "אחורית",
        "πίσω",
        "spate",
        "hátsó",
        "zadní",
        "darrere",
        "zadná",
        "задня",
        "stražnja",
        "belakang",
        "बैक"
    ];
    /**
     * Get a list of cameras (if any) available on the device, a camera access permission is requested to the user
     * the first time this method is called if needed.
     *
     * Depending on device features and user permissions for camera access, any of the following errors
     * could be the rejected result of the returned promise:
     * - <tt>UnsupportedBrowserError</tt>
     * - <tt>PermissionDeniedError</tt>
     * - <tt>NotAllowedError</tt>
     * - <tt>NotFoundError</tt>
     * - <tt>AbortError</tt>
     * - <tt>NotReadableError</tt>
     * - <tt>InternalError</tt>
     *
     * @returns A promise resolving to the array of available [[Camera]] objects (could be empty).
     */
    function getCameras() {
        return new Promise((resolve, reject) => {
            const unsupportedBrowserError = browserHelper_1.BrowserHelper.checkBrowserCompatibility();
            if (unsupportedBrowserError != null) {
                return reject(unsupportedBrowserError);
            }
            let accessPermissionPromise = Promise.resolve();
            if (CameraAccess.mediaStream == null) {
                accessPermissionPromise = navigator.mediaDevices.getUserMedia({
                    video: true,
                    audio: false
                });
            }
            accessPermissionPromise
                .then(stream => {
                if (stream != null) {
                    CameraAccess.mediaStream = stream;
                }
                return enumerateDevices()
                    .then(devices => {
                    const cameras = devices
                        .filter(device => {
                        return device.kind === "videoinput";
                    })
                        .map(videoDevice => {
                        const label = videoDevice.label != null ? videoDevice.label : "";
                        const lowercaseLabel = label.toLowerCase();
                        return {
                            deviceId: videoDevice.deviceId,
                            label: label,
                            cameraType: backCameraKeywords.some(keyword => {
                                return lowercaseLabel.indexOf(keyword) !== -1;
                            })
                                ? camera_1.Camera.Type.BACK
                                : camera_1.Camera.Type.FRONT
                        };
                    });
                    if (cameras.length > 1 &&
                        !cameras.some(camera => {
                            return camera.cameraType === camera_1.Camera.Type.BACK;
                        })) {
                        const camera = cameras.slice(-1)[0];
                        cameras[cameras.length - 1] = {
                            deviceId: camera.deviceId,
                            label: camera.label,
                            cameraType: camera_1.Camera.Type.BACK
                        };
                    }
                    CameraAccess.mediaStream.getVideoTracks().forEach(track => {
                        track.stop();
                    });
                    return resolve(cameras);
                })
                    .catch(error => {
                    CameraAccess.mediaStream.getVideoTracks().forEach(track => {
                        track.stop();
                    });
                    return reject(error);
                });
            })
                .catch(error => {
                reject(error);
            });
        });
    }
    CameraAccess.getCameras = getCameras;
    /**
     * @hidden
     *
     * Try to access a given camera for video input at the given resolution level.
     *
     * @param resolutionFallbackLevel The number representing the wanted resolution, from 0 to 6,
     * resulting in higher to lower video resolutions.
     * @param camera The camera to try to access for video input.
     * @returns A promise resolving to the <tt>MediaStream</tt> object coming from the accessed camera.
     */
    function accessCameraStream(resolutionFallbackLevel, camera) {
        const getUserMediaParams = {
            audio: false,
            video: {}
        };
        if (resolutionFallbackLevel === 0) {
            getUserMediaParams.video = {
                width: {
                    min: 1400,
                    ideal: 1920,
                    max: 1920
                },
                height: {
                    min: 900,
                    ideal: 1440,
                    max: 1440
                }
            };
        }
        else if (resolutionFallbackLevel === 1) {
            getUserMediaParams.video = {
                width: {
                    min: 1200,
                    ideal: 1920,
                    max: 1920
                },
                height: {
                    min: 900,
                    ideal: 1200,
                    max: 1200
                }
            };
        }
        else if (resolutionFallbackLevel === 2) {
            getUserMediaParams.video = {
                width: {
                    min: 1080,
                    ideal: 1920,
                    max: 1920
                },
                height: {
                    min: 900,
                    ideal: 1080,
                    max: 1080
                }
            };
        }
        else if (resolutionFallbackLevel === 3) {
            getUserMediaParams.video = {
                width: {
                    min: 960,
                    ideal: 1280,
                    max: 1440
                },
                height: {
                    min: 480,
                    ideal: 960,
                    max: 960
                }
            };
        }
        else if (resolutionFallbackLevel === 4) {
            getUserMediaParams.video = {
                width: {
                    min: 720,
                    ideal: 1280,
                    max: 1440
                },
                height: {
                    min: 480,
                    ideal: 720,
                    max: 768
                }
            };
        }
        else if (resolutionFallbackLevel === 5) {
            getUserMediaParams.video = {
                width: {
                    min: 640,
                    ideal: 960,
                    max: 1440
                },
                height: {
                    min: 480,
                    ideal: 720,
                    max: 720
                }
            };
        }
        if (camera === undefined) {
            getUserMediaParams.video = true;
        }
        else {
            getUserMediaParams.video.deviceId = {
                exact: camera.deviceId
            };
        }
        return navigator.mediaDevices.getUserMedia(getUserMediaParams);
    }
    CameraAccess.accessCameraStream = accessCameraStream;
    /**
     * @hidden
     *
     * Get a list of available devices in a cross-browser compatible way.
     *
     * @returns A promise resolving to the <tt>MediaDeviceInfo</tt> array of all available devices.
     */
    function enumerateDevices() {
        if (typeof window.navigator.enumerateDevices === "function") {
            return window.navigator.enumerateDevices();
        }
        else if (typeof window.navigator.mediaDevices === "object" &&
            typeof window.navigator.mediaDevices.enumerateDevices === "function") {
            return window.navigator.mediaDevices.enumerateDevices();
        }
        else {
            return new Promise((resolve, reject) => {
                try {
                    window.MediaStreamTrack.getSources((devices) => {
                        resolve(devices
                            .filter(device => {
                            return device.kind.toLowerCase() === "video" || device.kind.toLowerCase() === "videoinput";
                        })
                            .map(device => {
                            return {
                                deviceId: device.deviceId != null ? device.deviceId : "",
                                groupId: device.groupId,
                                kind: "videoinput",
                                label: device.label
                            };
                        }));
                    });
                }
                catch (error) {
                    reject(new customError_1.CustomError({
                        name: "UnsupportedBrowserError",
                        message: "Media devices are not supported on this OS / Browser"
                    }));
                }
            });
        }
    }
})(CameraAccess = exports.CameraAccess || (exports.CameraAccess = {}));
//# sourceMappingURL=cameraAccess.js.map