"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
/**
 * @hidden
 */
// tslint:disable-next-line:max-func-body-length
function engineSDKWorkerFunction() {
    let imageBufferPointer;
    let licenseKey;
    let settings;
    let imageSettings;
    let wasmLoaded = false;
    let scannerSettingsReady = false;
    let scannerImageSettingsReady = false;
    const scanQueue = [];
    const parseQueue = [];
    let contextAvailable = false;
    let scanWorkSubmitted = false;
    let parseWorkSubmitted = false;
    let blurryDecodingEnabled = false;
    function loadLibrary(deviceId, libraryLocation) {
        const customModule = {
            locateFile: () => {
                return `${libraryLocation}scandit-engine-sdk.wasm`;
            },
            arguments: [deviceId],
            noInitialRun: true,
            preRun: () => {
                try {
                    FS.mkdir("/scandit_sync_folder");
                }
                catch (error) {
                    if (error.code !== "EEXIST") {
                        throw error;
                    }
                }
                FS.mount(IDBFS, {}, "/scandit_sync_folder");
                FS.syncfs(true, () => {
                    Module.callMain();
                    wasmLoaded = true;
                    postMessage(["status", "ready"]);
                    workOnScanQueue();
                    workOnParseQueue();
                });
            }
        };
        self["Module"] = customModule;
        if (typeof importScripts === "function") {
            try {
                importScripts(`${libraryLocation}scandit-engine-sdk.min.js`);
            }
            catch (error) {
                console.error(`Couldn't retrieve Scandit SDK Engine library at ${libraryLocation}scandit-engine-sdk.min.js, ` +
                    "did you configure the path for it correctly?");
            }
        }
    }
    function createContext() {
        if (contextAvailable || licenseKey == null || !wasmLoaded) {
            return;
        }
        const licenseKeyLength = Module.lengthBytesUTF8(licenseKey) + 1;
        const licenseKeyPointer = Module._malloc(licenseKeyLength);
        Module.stringToUTF8(licenseKey, licenseKeyPointer, licenseKeyLength);
        Module.asm._create_context(licenseKeyPointer);
        Module._free(licenseKeyPointer);
        contextAvailable = true;
    }
    function setupSettings() {
        if (settings == null || !contextAvailable || !wasmLoaded) {
            return;
        }
        scannerSettingsReady = false;
        if (licenseKey == null) {
            console.error("No license key found, the library must be set up before the scanning engine can be used");
            return;
        }
        const settingsLength = Module.lengthBytesUTF8(settings) + 1;
        const settingsPointer = Module._malloc(settingsLength);
        Module.stringToUTF8(settings, settingsPointer, settingsLength);
        const resultPointer = Module.asm._scanner_settings_new_from_json(settingsPointer, blurryDecodingEnabled ? 1 : 0);
        Module._free(settingsPointer);
        const result = Module.UTF8ToString(resultPointer);
        if (result !== "") {
            scannerSettingsReady = true;
            console.debug(JSON.parse(result));
        }
    }
    function setupImageSettings() {
        if (imageSettings == null || !wasmLoaded) {
            return;
        }
        scannerImageSettingsReady = false;
        let channels;
        // TODO: For now it's not possible to use imported variables as the worker doesn't have access at runtime
        switch (imageSettings.format.valueOf()) {
            case 0: // GRAY_8U
                channels = 1;
                break;
            case 1: // RGB_8U
                channels = 3;
                break;
            case 2: // RGBA_8U
                channels = 4;
                break;
            default:
                channels = 1;
                break;
        }
        Module.asm._scanner_image_settings_new(imageSettings.width, imageSettings.height, channels);
        if (imageBufferPointer != null) {
            Module._free(imageBufferPointer);
            imageBufferPointer = undefined;
        }
        imageBufferPointer = Module._malloc(imageSettings.width * imageSettings.height * channels);
        scannerImageSettingsReady = true;
    }
    function scanImage(imageData) {
        Module.HEAPU8.set(new Uint8Array(imageData), imageBufferPointer);
        return Module.UTF8ToString(Module.asm._scanner_scan(imageBufferPointer));
    }
    function parseString(dataFormat, dataString, options) {
        const dataStringLength = Module.lengthBytesUTF8(dataString) + 1;
        const dataStringPointer = Module._malloc(dataStringLength);
        Module.stringToUTF8(dataString, dataStringPointer, dataStringLength);
        const optionsLength = Module.lengthBytesUTF8(options) + 1;
        const optionsPointer = Module._malloc(optionsLength);
        Module.stringToUTF8(options, optionsPointer, optionsLength);
        const resultPointer = Module.asm._parser_parse_string(dataFormat.valueOf(), dataStringPointer, dataStringLength - 1, optionsPointer);
        Module._free(dataStringPointer);
        Module._free(optionsPointer);
        return Module.UTF8ToString(resultPointer);
    }
    function workOnScanQueue() {
        if ((!scannerSettingsReady || !scannerImageSettingsReady) && scanQueue.length !== 0) {
            createContext();
            setupSettings();
            setupImageSettings();
        }
        if (!scannerSettingsReady || !scannerImageSettingsReady) {
            return;
        }
        let currentScanWorkUnit;
        let resultData;
        while (scanQueue.length !== 0) {
            currentScanWorkUnit = scanQueue.shift();
            resultData = scanImage(currentScanWorkUnit.data);
            const result = JSON.parse(resultData);
            // Important! We send back data that is actually ignored by the receiver (currentScanWorkUnit.data).
            // This is needed however because some browsers run out of memory due to no GC being run
            // if this data is not send back (cleared) by the worker for some weird reason...
            if (result.error != null) {
                postMessage([
                    "work-error",
                    {
                        requestId: currentScanWorkUnit.requestId,
                        error: result.error
                    }
                ], [currentScanWorkUnit.data]);
            }
            else if (result.result != null) {
                postMessage([
                    "work-result",
                    {
                        requestId: currentScanWorkUnit.requestId,
                        result: result.result
                    }
                ], [currentScanWorkUnit.data]);
            }
            else {
                console.error("Unrecognized Scandit Engine result:", result);
                postMessage([""], [currentScanWorkUnit.data]);
            }
        }
    }
    function workOnParseQueue() {
        if (!contextAvailable && parseQueue.length !== 0) {
            createContext();
        }
        if (!contextAvailable || !wasmLoaded) {
            return;
        }
        let currentParseWorkUnit;
        let resultData;
        while (parseQueue.length !== 0) {
            currentParseWorkUnit = parseQueue.shift();
            resultData = parseString(currentParseWorkUnit.dataFormat, currentParseWorkUnit.dataString, currentParseWorkUnit.options);
            const result = JSON.parse(resultData);
            if (result.error != null) {
                postMessage([
                    "parse-string-error",
                    {
                        requestId: currentParseWorkUnit.requestId,
                        error: result.error
                    }
                ]);
            }
            else if (result.result != null) {
                postMessage([
                    "parse-string-result",
                    {
                        requestId: currentParseWorkUnit.requestId,
                        result: result.result
                    }
                ]);
            }
            else {
                console.error("Unrecognized Scandit Parser result:", result);
                postMessage([
                    "parse-string-error",
                    {
                        requestId: currentParseWorkUnit.requestId,
                        error: {
                            errorCode: -1,
                            errorMessage: "Unknown Scandit Parser error"
                        }
                    }
                ]);
            }
        }
    }
    onmessage = e => {
        // Setting settings triggers license verification and activation: delay until first frame processed
        const data = e.data;
        switch (data.type) {
            case "enable-blurry-decoding":
                blurryDecodingEnabled = true;
                if (scanWorkSubmitted) {
                    setupSettings();
                    workOnScanQueue();
                }
                break;
            case "load-library":
                loadLibrary(data.deviceId, data.libraryLocation);
                break;
            case "license-key":
                licenseKey = data.licenseKey;
                createContext();
                if (parseWorkSubmitted) {
                    workOnParseQueue();
                }
                break;
            case "settings":
                settings = data.settings;
                if (scanWorkSubmitted) {
                    setupSettings();
                    workOnScanQueue();
                }
                break;
            case "image-settings":
                imageSettings = data.imageSettings;
                if (scanWorkSubmitted) {
                    setupImageSettings();
                    workOnScanQueue();
                }
                break;
            case "work":
                scanWorkSubmitted = true;
                scanQueue.push({
                    requestId: data.requestId,
                    data: data.data
                });
                workOnScanQueue();
                break;
            case "parse-string":
                parseWorkSubmitted = true;
                parseQueue.push({
                    requestId: data.requestId,
                    dataFormat: data.dataFormat,
                    dataString: data.dataString,
                    options: data.options
                });
                workOnParseQueue();
                break;
            default:
                break;
        }
    };
}
/**
 * @hidden
 */
exports.engineSDKWorker = new Blob([`(${engineSDKWorkerFunction.toString()})()`], {
    type: "text/javascript"
});
//# sourceMappingURL=engineSDKWorker.js.map