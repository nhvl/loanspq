import { Camera } from "./camera";
/**
 * A helper object to interact with cameras.
 */
export declare namespace CameraAccess {
    /**
     * @hidden
     */
    let mediaStream: MediaStream;
    /**
     * Get a list of cameras (if any) available on the device, a camera access permission is requested to the user
     * the first time this method is called if needed.
     *
     * Depending on device features and user permissions for camera access, any of the following errors
     * could be the rejected result of the returned promise:
     * - <tt>UnsupportedBrowserError</tt>
     * - <tt>PermissionDeniedError</tt>
     * - <tt>NotAllowedError</tt>
     * - <tt>NotFoundError</tt>
     * - <tt>AbortError</tt>
     * - <tt>NotReadableError</tt>
     * - <tt>InternalError</tt>
     *
     * @returns A promise resolving to the array of available [[Camera]] objects (could be empty).
     */
    function getCameras(): Promise<Camera[]>;
    /**
     * @hidden
     *
     * Try to access a given camera for video input at the given resolution level.
     *
     * @param resolutionFallbackLevel The number representing the wanted resolution, from 0 to 6,
     * resulting in higher to lower video resolutions.
     * @param camera The camera to try to access for video input.
     * @returns A promise resolving to the <tt>MediaStream</tt> object coming from the accessed camera.
     */
    function accessCameraStream(resolutionFallbackLevel: number, camera?: Camera): Promise<MediaStream>;
}
