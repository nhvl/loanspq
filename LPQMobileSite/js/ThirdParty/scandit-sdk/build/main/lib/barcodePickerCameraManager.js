"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const base64assets_1 = require("./assets/base64assets");
const barcodePicker_1 = require("./barcodePicker");
const camera_1 = require("./camera");
const cameraAccess_1 = require("./cameraAccess");
const cameraSettings_1 = require("./cameraSettings");
const customError_1 = require("./customError");
/**
 * @hidden
 *
 * A barcode picker utility class used to handle camera interaction.
 */
class BarcodePickerCameraManager {
    constructor(barcodePicker) {
        this.barcodePicker = barcodePicker;
        this.postStreamInitializationListener = this.postStreamInitialization.bind(this);
        this.triggerManualFocusListener = this.triggerManualFocus.bind(this);
        this.triggerZoomStartListener = this.triggerZoomStart.bind(this);
        this.triggerZoomMoveListener = this.triggerZoomMove.bind(this);
        this.setupCameraSwitcher();
        this.setupTorchToggle();
    }
    setUIOptions(enableCameraSwitcher, enableTorchToggle, enableTapToFocus, enablePinchToZoom) {
        this.enableCameraSwitcher = enableCameraSwitcher;
        this.enableTorchToggle = enableTorchToggle;
        this.enableTapToFocus = enableTapToFocus;
        this.enablePinchToZoom = enablePinchToZoom;
    }
    setSelectedCamera(camera) {
        this.selectedCamera = camera;
    }
    setSelectedCameraSettings(cameraSettings) {
        this.selectedCameraSettings = cameraSettings;
    }
    setupCameras() {
        return cameraAccess_1.CameraAccess.getCameras().then(cameras => {
            if (this.enableCameraSwitcher && cameras.length > 1) {
                this.cameraSwitcherElement.classList.remove("scandit-hidden");
            }
            if (this.selectedCamera == null) {
                let autoselectedCamera = cameras.find(currentCamera => {
                    return currentCamera.cameraType === camera_1.Camera.Type.BACK;
                });
                if (autoselectedCamera === undefined) {
                    autoselectedCamera = cameras[0];
                }
                if (autoselectedCamera === undefined) {
                    return Promise.reject(new customError_1.CustomError({ name: "NoCameraAvailableError", message: "No camera available" }));
                }
                return this.initializeCameraWithSettings(autoselectedCamera, this.selectedCameraSettings);
            }
            else {
                return this.initializeCameraWithSettings(this.selectedCamera, this.selectedCameraSettings);
            }
        });
    }
    stopStream() {
        if (this.activeCamera != null) {
            this.activeCamera.currentResolution = undefined;
        }
        this.activeCamera = undefined;
        if (this.mediaStream != null) {
            window.clearTimeout(this.cameraAccessTimeout);
            window.clearInterval(this.cameraMetadataCheckInterval);
            window.clearTimeout(this.getCapabilitiesTimeout);
            window.clearTimeout(this.manualFocusWaitTimeout);
            window.clearTimeout(this.manualToAutofocusResumeTimeout);
            window.clearInterval(this.autofocusInterval);
            this.mediaStream.getVideoTracks().forEach(track => {
                track.stop();
            });
            this.mediaStream = undefined;
            this.mediaTrackCapabilities = undefined;
        }
    }
    applyCameraSettings(cameraSettings) {
        if (this.activeCamera == null) {
            return Promise.reject(new customError_1.CustomError({ name: "NoCameraAvailableError", message: "No camera available" }));
        }
        return this.initializeCameraWithSettings(this.activeCamera, cameraSettings);
    }
    reinitializeCamera() {
        if (this.activeCamera == null) {
            return Promise.reject(new customError_1.CustomError({ name: "NoCameraAvailableError", message: "No camera available" }));
        }
        return this.initializeCameraWithSettings(this.activeCamera, this.activeCameraSettings);
    }
    initializeCameraWithSettings(camera, cameraSettings) {
        this.activeCameraSettings = cameraSettings;
        if (cameraSettings != null && cameraSettings.resolutionPreference === cameraSettings_1.CameraSettings.ResolutionPreference.FULL_HD) {
            return this.initializeCamera(camera);
        }
        else {
            return this.initializeCamera(camera, 3);
        }
    }
    setTorchEnabled(enabled) {
        if (this.mediaStream != null && this.mediaTrackCapabilities != null && this.mediaTrackCapabilities.torch) {
            const videoTracks = this.mediaStream.getVideoTracks();
            if (videoTracks.length !== 0 && typeof videoTracks[0].applyConstraints === "function") {
                videoTracks[0].applyConstraints({ advanced: [{ torch: enabled }] });
            }
        }
    }
    setZoom(zoomPercentage, currentZoom) {
        if (this.mediaStream != null && this.mediaTrackCapabilities != null && this.mediaTrackCapabilities.zoom) {
            const videoTracks = this.mediaStream.getVideoTracks();
            if (videoTracks.length !== 0 && typeof videoTracks[0].applyConstraints === "function") {
                const zoomRange = this.mediaTrackCapabilities.zoom.max - this.mediaTrackCapabilities.zoom.min;
                if (currentZoom == null) {
                    currentZoom = this.mediaTrackCapabilities.zoom.min;
                }
                const targetZoom = Math.max(this.mediaTrackCapabilities.zoom.min, Math.min(currentZoom + zoomRange * zoomPercentage, this.mediaTrackCapabilities.zoom.max));
                videoTracks[0].applyConstraints({ advanced: [{ zoom: targetZoom }] });
            }
        }
    }
    postStreamInitialization() {
        window.clearTimeout(this.getCapabilitiesTimeout);
        this.getCapabilitiesTimeout = window.setTimeout(() => {
            this.storeStreamCapabilities();
            this.setupAutofocus();
            if (this.enableTorchToggle &&
                this.mediaStream != null &&
                this.mediaTrackCapabilities != null &&
                this.mediaTrackCapabilities.torch) {
                this.torchToggleElement.classList.remove("scandit-hidden");
            }
        }, BarcodePickerCameraManager.getCapabilitiesTimeoutMs);
    }
    triggerManualFocus(event) {
        if (event != null) {
            event.preventDefault();
            if (event.type === "touchend" && event.touches.length !== 0) {
                return;
            }
            // Check if we were using pinch-to-zoom
            if (this.pinchToZoomDistance != null) {
                this.pinchToZoomDistance = undefined;
                return;
            }
        }
        window.clearTimeout(this.manualFocusWaitTimeout);
        window.clearTimeout(this.manualToAutofocusResumeTimeout);
        if (this.mediaStream != null && this.mediaTrackCapabilities != null) {
            const focusModeCapability = this.mediaTrackCapabilities.focusMode;
            if (focusModeCapability instanceof Array && focusModeCapability.indexOf("single-shot") !== -1) {
                if (focusModeCapability.indexOf("continuous") !== -1 && focusModeCapability.indexOf("manual") !== -1) {
                    this.triggerFocusMode("continuous")
                        .then(() => {
                        this.manualFocusWaitTimeout = window.setTimeout(() => {
                            this.triggerFocusMode("manual");
                        }, BarcodePickerCameraManager.manualFocusWaitTimeoutMs);
                    })
                        .catch(() => {
                        // Ignored
                    });
                    this.manualToAutofocusResumeTimeout = window.setTimeout(() => {
                        this.triggerFocusMode("continuous");
                    }, BarcodePickerCameraManager.manualToAutofocusResumeTimeoutMs);
                }
                else if (focusModeCapability.indexOf("continuous") === -1) {
                    window.clearInterval(this.autofocusInterval);
                    this.triggerFocusMode("single-shot").catch(() => {
                        // Ignored
                    });
                    this.manualToAutofocusResumeTimeout = window.setTimeout(() => {
                        this.autofocusInterval = window.setInterval(this.triggerAutoFocus.bind(this), BarcodePickerCameraManager.autofocusIntervalMs);
                    }, BarcodePickerCameraManager.manualToAutofocusResumeTimeoutMs);
                }
            }
        }
    }
    triggerZoomStart(event) {
        if (event == null || event.touches.length !== 2) {
            return;
        }
        event.preventDefault();
        this.pinchToZoomDistance = Math.hypot((event.touches[1].screenX - event.touches[0].screenX) / screen.width, (event.touches[1].screenY - event.touches[0].screenY) / screen.height);
        if (this.mediaStream != null && this.mediaTrackCapabilities != null && this.mediaTrackCapabilities.zoom) {
            const videoTracks = this.mediaStream.getVideoTracks();
            if (videoTracks.length !== 0 && typeof videoTracks[0].getConstraints === "function") {
                this.pinchToZoomInitialZoom = this.mediaTrackCapabilities.zoom.min;
                const currentConstraints = videoTracks[0].getConstraints();
                if (currentConstraints.advanced != null) {
                    const currentZoomConstraint = currentConstraints.advanced.find(constraint => {
                        return "zoom" in constraint;
                    });
                    if (currentZoomConstraint != null) {
                        this.pinchToZoomInitialZoom = currentZoomConstraint.zoom;
                    }
                }
            }
        }
    }
    triggerZoomMove(event) {
        if (this.pinchToZoomDistance == null || event == null || event.touches.length !== 2) {
            return;
        }
        event.preventDefault();
        this.setZoom((Math.hypot((event.touches[1].screenX - event.touches[0].screenX) / screen.width, (event.touches[1].screenY - event.touches[0].screenY) / screen.height) -
            this.pinchToZoomDistance) *
            2, this.pinchToZoomInitialZoom);
    }
    storeStreamCapabilities() {
        if (this.mediaStream != null) {
            const videoTracks = this.mediaStream.getVideoTracks();
            if (videoTracks.length !== 0 && typeof videoTracks[0].getCapabilities === "function") {
                this.mediaTrackCapabilities = videoTracks[0].getCapabilities();
            }
        }
    }
    setupAutofocus() {
        window.clearTimeout(this.manualFocusWaitTimeout);
        window.clearTimeout(this.manualToAutofocusResumeTimeout);
        if (this.mediaStream != null && this.mediaTrackCapabilities != null) {
            const focusModeCapability = this.mediaTrackCapabilities.focusMode;
            if (focusModeCapability instanceof Array &&
                focusModeCapability.indexOf("continuous") === -1 &&
                focusModeCapability.indexOf("single-shot") !== -1) {
                window.clearInterval(this.autofocusInterval);
                this.autofocusInterval = window.setInterval(this.triggerAutoFocus.bind(this), BarcodePickerCameraManager.autofocusIntervalMs);
            }
        }
    }
    triggerAutoFocus() {
        this.triggerFocusMode("single-shot").catch(() => {
            // Ignored
        });
    }
    triggerFocusMode(focusMode) {
        if (this.mediaStream != null) {
            const videoTracks = this.mediaStream.getVideoTracks();
            if (videoTracks.length !== 0 && typeof videoTracks[0].applyConstraints === "function") {
                return videoTracks[0].applyConstraints({ advanced: [{ focusMode }] });
            }
        }
        return Promise.reject(undefined);
    }
    enableTapToFocusListeners() {
        ["touchend", "mousedown"].forEach(eventName => {
            this.barcodePicker.videoElement.addEventListener(eventName, this.triggerManualFocusListener);
        });
    }
    enablePinchToZoomListeners() {
        this.barcodePicker.videoElement.addEventListener("touchstart", this.triggerZoomStartListener);
        this.barcodePicker.videoElement.addEventListener("touchmove", this.triggerZoomMoveListener);
    }
    initializeCamera(camera, resolutionFallbackLevel = 0) {
        if (camera == null) {
            return Promise.reject(new customError_1.CustomError({ name: "NoCameraAvailableError", message: "No camera available" }));
        }
        this.stopStream();
        this.torchEnabled = false;
        this.torchToggleElement.classList.add("scandit-hidden");
        return new Promise((resolve, reject) => {
            cameraAccess_1.CameraAccess.accessCameraStream(resolutionFallbackLevel, camera)
                .then(stream => {
                // Detect weird browser behaviour that on unsupported resolution returns a 2x2 video instead
                if (typeof stream.getTracks()[0].getSettings === "function") {
                    const mediaTrackSettings = stream.getTracks()[0].getSettings();
                    if (mediaTrackSettings.width != null &&
                        mediaTrackSettings.height != null &&
                        (mediaTrackSettings.width === 2 || mediaTrackSettings.height === 2)) {
                        if (resolutionFallbackLevel === 6) {
                            return reject(new customError_1.CustomError({ name: "NotReadableError", message: "Could not initialize camera correctly" }));
                        }
                        else {
                            return this.initializeCamera(camera, resolutionFallbackLevel + 1)
                                .then(() => {
                                return resolve();
                            })
                                .catch(error => {
                                return reject(error);
                            });
                        }
                    }
                }
                this.mediaStream = stream;
                // This will add the listener only once in the case of multiple calls, identical listeners are ignored
                this.barcodePicker.videoElement.addEventListener("loadedmetadata", this.postStreamInitializationListener);
                if (this.enableTapToFocus) {
                    this.enableTapToFocusListeners();
                }
                if (this.enablePinchToZoom) {
                    this.enablePinchToZoomListeners();
                }
                this.resolveInitializeCamera(camera, resolve, reject);
                this.barcodePicker.videoElement.srcObject = stream;
                this.barcodePicker.videoElement.load();
                const playPromise = this.barcodePicker.videoElement.play();
                if (playPromise != null) {
                    playPromise.catch(() => {
                        // Can sometimes cause an incorrect rejection (all is good, ignore).
                    });
                }
            })
                .catch(error => {
                if (error.name === "OverconstrainedError" && error.constraint === "deviceId") {
                    // Camera might have changed deviceId: check for new cameras with same label and type but different deviceId
                    return cameraAccess_1.CameraAccess.getCameras().then(cameras => {
                        const newCamera = cameras.find(currentCamera => {
                            return (currentCamera.label === camera.label &&
                                currentCamera.cameraType === camera.cameraType &&
                                currentCamera.deviceId !== camera.deviceId);
                        });
                        if (newCamera == null) {
                            return reject(error);
                        }
                        else {
                            return this.initializeCamera(newCamera, resolutionFallbackLevel)
                                .then(mediaStream => {
                                return resolve(mediaStream);
                            })
                                .catch(error2 => {
                                return reject(error2);
                            });
                        }
                    });
                }
                if (error.name === "PermissionDeniedError" ||
                    error.name === "PermissionDismissedError" ||
                    error.name === "NotAllowedError" ||
                    error.name === "NotFoundError" ||
                    error.name === "AbortError") {
                    // Camera is not accessible at all
                    return reject(error);
                }
                if (resolutionFallbackLevel < 6) {
                    return this.initializeCamera(camera, resolutionFallbackLevel + 1)
                        .then(mediaStream => {
                        return resolve(mediaStream);
                    })
                        .catch(error2 => {
                        return reject(error2);
                    });
                }
                else {
                    return reject(error);
                }
            });
        });
    }
    resolveInitializeCamera(camera, resolve, reject) {
        const cameraNotReadableError = new customError_1.CustomError({
            name: "NotReadableError",
            message: "Could not initialize camera correctly"
        });
        this.cameraAccessTimeout = window.setTimeout(() => {
            this.stopStream();
            reject(cameraNotReadableError);
        }, BarcodePickerCameraManager.cameraAccessTimeoutMs);
        if (BarcodePickerCameraManager.isIOSDevice) {
            // iOS camera access should always work but can fail to call "later" video callbacks, so we check loadstart
            this.barcodePicker.videoElement.onloadstart = () => {
                this.barcodePicker.videoElement.onloadstart = null;
                window.clearTimeout(this.cameraAccessTimeout);
                this.activeCamera = camera;
                this.barcodePicker.setMirrorImageEnabled(this.activeCamera.cameraType === camera_1.Camera.Type.FRONT);
                resolve(this.barcodePicker);
            };
        }
        else {
            this.barcodePicker.videoElement.onloadeddata = () => {
                this.barcodePicker.videoElement.onloadeddata = null;
                window.clearTimeout(this.cameraAccessTimeout);
                // Detect weird browser behaviour that on unsupported resolution returns a 2x2 video instead
                // Also detect failed camera access with no error but also no video stream provided
                if (this.barcodePicker.videoElement.videoWidth > 2 &&
                    this.barcodePicker.videoElement.videoHeight > 2 &&
                    this.barcodePicker.videoElement.currentTime > 0) {
                    this.activeCamera = camera;
                    this.barcodePicker.setMirrorImageEnabled(this.activeCamera.cameraType === camera_1.Camera.Type.FRONT);
                    return resolve(this.barcodePicker);
                }
                const cameraMetadataCheckStartTime = performance.now();
                window.clearInterval(this.cameraMetadataCheckInterval);
                this.cameraMetadataCheckInterval = window.setInterval(() => {
                    if (this.barcodePicker.videoElement.videoWidth === 2 ||
                        this.barcodePicker.videoElement.videoHeight === 2 ||
                        this.barcodePicker.videoElement.currentTime === 0) {
                        if (performance.now() - cameraMetadataCheckStartTime >
                            BarcodePickerCameraManager.cameraMetadataCheckTimeoutMs) {
                            window.clearInterval(this.cameraMetadataCheckInterval);
                            this.stopStream();
                            return reject(cameraNotReadableError);
                        }
                        return;
                    }
                    window.clearInterval(this.cameraMetadataCheckInterval);
                    this.activeCamera = camera;
                    this.barcodePicker.setMirrorImageEnabled(this.activeCamera.cameraType === camera_1.Camera.Type.FRONT);
                    this.barcodePicker.videoElement.dispatchEvent(new Event("canplay"));
                    return resolve(this.barcodePicker);
                }, BarcodePickerCameraManager.cameraMetadataCheckIntervalMs);
            };
        }
    }
    setupCameraSwitcher() {
        this.cameraSwitcherElement = document.createElement("img");
        this.cameraSwitcherElement.src = base64assets_1.switchCameraImage;
        this.cameraSwitcherElement.className = barcodePicker_1.BarcodePicker.cameraSwitcherElementClassName;
        this.cameraSwitcherElement.classList.add("scandit-hidden");
        this.barcodePicker.parentElement.appendChild(this.cameraSwitcherElement);
        ["touchstart", "mousedown"].forEach(eventName => {
            this.cameraSwitcherElement.addEventListener(eventName, event => {
                event.preventDefault();
                cameraAccess_1.CameraAccess.getCameras()
                    .then(cameras => {
                    const newCameraIndex = (cameras.findIndex(camera => {
                        return camera.deviceId === (this.activeCamera == null ? camera.deviceId : this.activeCamera.deviceId);
                    }) +
                        1) %
                        cameras.length;
                    this.initializeCameraWithSettings(cameras[newCameraIndex], this.activeCameraSettings).catch(error => {
                        console.error(error);
                    });
                })
                    .catch(error => {
                    console.error(error);
                });
            });
        });
    }
    setupTorchToggle() {
        this.torchToggleElement = document.createElement("img");
        this.torchToggleElement.src = base64assets_1.toggleTorchImage;
        this.torchToggleElement.className = barcodePicker_1.BarcodePicker.torchToggleElementClassName;
        this.torchToggleElement.classList.add("scandit-hidden");
        this.barcodePicker.parentElement.appendChild(this.torchToggleElement);
        ["touchstart", "mousedown"].forEach(eventName => {
            this.torchToggleElement.addEventListener(eventName, event => {
                event.preventDefault();
                this.torchEnabled = !this.torchEnabled;
                this.setTorchEnabled(this.torchEnabled);
            });
        });
    }
}
BarcodePickerCameraManager.isIOSDevice = navigator.platform != null &&
    navigator.platform !== "" &&
    /iPad|iPhone|iPod/.test(navigator.platform);
BarcodePickerCameraManager.cameraAccessTimeoutMs = 4000;
BarcodePickerCameraManager.cameraMetadataCheckTimeoutMs = 4000;
BarcodePickerCameraManager.cameraMetadataCheckIntervalMs = 50;
BarcodePickerCameraManager.getCapabilitiesTimeoutMs = 500;
BarcodePickerCameraManager.autofocusIntervalMs = 1500;
BarcodePickerCameraManager.manualToAutofocusResumeTimeoutMs = 5000;
BarcodePickerCameraManager.manualFocusWaitTimeoutMs = 400;
exports.BarcodePickerCameraManager = BarcodePickerCameraManager;
//# sourceMappingURL=barcodePickerCameraManager.js.map