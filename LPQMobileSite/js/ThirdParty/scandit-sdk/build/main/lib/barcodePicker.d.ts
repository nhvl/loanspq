import { Camera } from "./camera";
import { CameraSettings } from "./cameraSettings";
import { Parser } from "./parser";
import { ScanResult } from "./scanResult";
import { ScanSettings } from "./scanSettings";
/**
 * A barcode picker element used to get and show camera input and perform scanning operations.
 *
 * The barcode picker will automatically fit and scale inside the given <i>originElement</i>.
 *
 * Each barcode picker internally contains a [[Scanner]] object with its own WebWorker thread running a
 * separate copy of the external Scandit Engine library. To optimize loading times and performance it's
 * recommended to reuse the same picker and to already create the picker in advance (hidden) and just
 * display it when needed whenever possible.
 *
 * As the loading of the external Scandit Engine library can take some time the picker always starts inactive
 * (but showing GUI and video) and then activates, if not paused, as soon as the library is ready to scan.
 * The [[onReady]] method can be used to set up a listener function to be called when the library is loaded.
 *
 * You are not allowed to hide the Scandit logo present in the corner of the GUI.
 */
export declare class BarcodePicker {
    private originElement;
    /**
     * @hidden
     */
    static readonly grandParentElementClassName: string;
    /**
     * @hidden
     */
    static readonly parentElementClassName: string;
    /**
     * @hidden
     */
    static readonly videoElementClassName: string;
    /**
     * @hidden
     */
    static readonly scanditLogoImageElementClassName: string;
    /**
     * @hidden
     */
    static readonly laserImageElementClassName: string;
    /**
     * @hidden
     */
    static readonly viewfinderElementClassName: string;
    /**
     * @hidden
     */
    static readonly cameraSwitcherElementClassName: string;
    /**
     * @hidden
     */
    static readonly torchToggleElementClassName: string;
    /**
     * @hidden
     */
    readonly grandParentElement: HTMLDivElement;
    /**
     * @hidden
     */
    readonly parentElement: HTMLDivElement;
    /**
     * @hidden
     */
    readonly videoElement: HTMLVideoElement;
    /**
     * @hidden
     */
    readonly laserActiveImageElement: HTMLImageElement;
    /**
     * @hidden
     */
    readonly laserPausedImageElement: HTMLImageElement;
    /**
     * @hidden
     */
    readonly viewfinderElement: HTMLDivElement;
    private cameraManager;
    private visible;
    private playSoundOnScan;
    private vibrateOnScan;
    private scanningPaused;
    private guiStyle;
    private videoFit;
    private eventEmitter;
    private scanner;
    private mirrorImage;
    private videoImageCanvasContext;
    private fatalError;
    private beepSound;
    private vibrateFunction;
    private latestVideoTimeProcessed;
    private resizeInterval;
    private lastKnownOriginElementWidth;
    private lastKnownOriginElementHeight;
    private destroyed;
    private isReadyToWork;
    private cameraAccess;
    private globalListener;
    private targetScanningFPS;
    private averageProcessingTime;
    private constructor();
    /**
     * Creates a BarcodePicker instance, creating the needed HTML in the given origin element.
     * If the <i>accessCamera</i> option is enabled (active by default) the available cameras are accessed
     * and a camera access permission is requested to the user if needed.
     * This object expects that at least a camera is available. The active camera is accessed and kept active during the
     * lifetime of the picker (also when hidden or scanning is paused), and is only released when [[destroy]] is called.
     *
     * It is required to having configured the library via [[configure]] before this object can be created.
     *
     * By default an alert is shown if an internal error during scanning is encountered which prevents the scanning
     * procedure from continuing. As this uses the built-in [[onScanError]] event functionality, if unwanted it can be
     * disabled by calling [[removeScanErrorListeners]] on the BarcodePicker instance (right after creation).
     *
     * Depending on parameters, device features and user permissions for camera access, any of the following errors
     * could be the rejected result of the returned promise:
     * - <tt>LibraryNotConfiguredError</tt>
     * - <tt>NoOriginElementError</tt>
     * - <tt>UnsupportedBrowserError</tt>
     * - <tt>PermissionDeniedError</tt>
     * - <tt>NotAllowedError</tt>
     * - <tt>NotFoundError</tt>
     * - <tt>AbortError</tt>
     * - <tt>NotReadableError</tt>
     * - <tt>InternalError</tt>
     * - <tt>NoCameraAvailableError</tt>
     *
     * @param originElement The HTMLElement inside which all the necessary elements for the picker will be added.
     * @param visible <div class="tsd-signature-symbol">Default =&nbsp;true</div>
     * Whether the picker starts in a visible state.
     * @param playSoundOnScan <div class="tsd-signature-symbol">Default =&nbsp;false</div>
     * Whether a sound is played on barcode scanned (iOS requires user input).
     * @param vibrateOnScan <div class="tsd-signature-symbol">Default =&nbsp;false</div>
     * Whether the device vibrates on barcode scanned (only Chrome & Firefox, requires user input).
     * @param scanningPaused <div class="tsd-signature-symbol">Default =&nbsp;false</div>
     * Whether the picker starts in a paused scanning state.
     * @param guiStyle <div class="tsd-signature-symbol">Default =&nbsp;GuiStyle.LASER</div>
     * The GUI style for the picker.
     * @param videoFit <div class="tsd-signature-symbol">Default =&nbsp;ObjectFit.CONTAIN</div>
     * The fit type for the video element of the picker.
     * @param enableCameraSwitcher <div class="tsd-signature-symbol">Default =&nbsp;true</div>
     * Whether to show a GUI button to switch between different cameras (when available).
     * @param enableTorchToggle <div class="tsd-signature-symbol">Default =&nbsp;true</div>
     * Whether to show a GUI button to toggle device torch on/off (when available, only Chrome).
     * @param enableTapToFocus <div class="tsd-signature-symbol">Default =&nbsp;true</div>
     * Whether to trigger a manual focus of the camera when clicking/tapping on the video (when available, only Chrome).
     * @param enablePinchToZoom <div class="tsd-signature-symbol">Default =&nbsp;true</div>
     * Whether to control the zoom of the camera when doing a pinching gesture on the video (when available, only Chrome).
     * @param accessCamera <div class="tsd-signature-symbol">Default =&nbsp;true</div>
     * Whether to immediately access the camera (and requesting user permissions if needed) on picker creation.
     * @param camera <div class="tsd-signature-symbol">Default =&nbsp;undefined</div>
     * The camera to be used for video input, if not specified the back or only camera will be used.
     * @param cameraSettings <div class="tsd-signature-symbol">Default =&nbsp;undefined</div>
     * The camera options used when accessing the camera, by default HD resolution is used.
     * @param scanSettings <div class="tsd-signature-symbol">Default =&nbsp;new ScanSettings()</div>
     * The configuration object for scanning options (All symbologies disabled by default).
     * @param targetScanningFPS <div class="tsd-signature-symbol">Default =&nbsp;30</div>
     * The target frames per second to be processed, the final speed is limited by the camera framerate (usually 30 FPS)
     * and the frame processing time of the device. By setting this to lower numbers devices can save power by performing
     * less work during scanning operations, depending on device speed (faster devices can "sleep" for longer periods).
     * Must be a number bigger than 0.
     * @returns A promise resolving to the created ready [[BarcodePicker]] object.
     */
    static create(originElement: HTMLElement, {visible, playSoundOnScan, vibrateOnScan, scanningPaused, guiStyle, videoFit, scanSettings, enableCameraSwitcher, enableTorchToggle, enableTapToFocus, enablePinchToZoom, accessCamera, camera, cameraSettings, targetScanningFPS}?: {
        visible?: boolean;
        playSoundOnScan?: boolean;
        vibrateOnScan?: boolean;
        scanningPaused?: boolean;
        guiStyle?: BarcodePicker.GuiStyle;
        videoFit?: BarcodePicker.ObjectFit;
        scanSettings?: ScanSettings;
        enableCameraSwitcher?: boolean;
        enableTorchToggle?: boolean;
        enableTapToFocus?: boolean;
        enablePinchToZoom?: boolean;
        accessCamera?: boolean;
        camera?: Camera;
        cameraSettings?: CameraSettings;
        targetScanningFPS?: number;
    }): Promise<BarcodePicker>;
    /**
     * Stop scanning and displaying video output, remove HTML elements added to the page,
     * destroy the internal [[Scanner]] and destroy the barcode picker itself; ensuring complete cleanup.
     *
     * This method should be called after you don't plan to use the picker anymore,
     * before the object is automatically cleaned up by JavaScript.
     * The barcode picker must not be used in any way after this call.
     */
    destroy(): void;
    /**
     * Applies a new set of scan settings to the internal scanner (replacing old settings).
     *
     * @param scanSettings The scan configuration object to be applied to the scanner.
     * @returns The updated [[BarcodePicker]] object.
     */
    applyScanSettings(scanSettings: ScanSettings): BarcodePicker;
    /**
     * @returns Whether the scanning is currently paused.
     */
    isScanningPaused(): boolean;
    /**
     * Pause the recognition of codes in the input image.
     *
     * By default video from the camera is still shown, if the <i>pauseCamera</i> option is enabled the camera stream
     * is paused (camera access is fully interrupted) and will be resumed when calling [[resumeScanning]],
     * possibly requesting user permissions if needed.
     *
     * @param pauseCamera Whether to also pause the camera stream.
     * @returns The updated [[BarcodePicker]] object.
     */
    pauseScanning(pauseCamera?: boolean): BarcodePicker;
    /**
     * Resume the recognition of codes in the input image.
     *
     * If the camera stream was stopped when calling [[pauseScanning]], the camera stream is also resumed and
     * user permissions are requested if needed to resume video input.
     *
     * @returns The updated [[BarcodePicker]] object.
     */
    resumeScanning(): Promise<BarcodePicker>;
    /**
     * @returns The currently active camera.
     */
    getActiveCamera(): Camera | undefined;
    /**
     * Select a camera to be used for video input.
     *
     * If camera access is enabled, the camera is enabled and accessed.
     *
     * Depending on device features and user permissions for camera access, any of the following errors
     * could be the rejected result of the returned promise:
     * - <tt>PermissionDeniedError</tt>
     * - <tt>NotAllowedError</tt>
     * - <tt>NotFoundError</tt>
     * - <tt>AbortError</tt>
     * - <tt>NotReadableError</tt>
     * - <tt>InternalError</tt>
     * - <tt>NoCameraAvailableError</tt>
     *
     * @param camera The new camera to be used.
     * @param cameraSettings The camera options used when accessing the camera, by default HD resolution is used.
     * @returns A promise resolving to the updated [[BarcodePicker]] object when the camera is set
     * (and accessed, if camera access is currently enabled).
     */
    setActiveCamera(camera: Camera, cameraSettings?: CameraSettings): Promise<BarcodePicker>;
    /**
     * Try to apply new settings to the currently used camera for video input,
     * if no settings are passed the default ones are set.
     *
     * If camera access is enabled, the camera is updated and accessed with the new settings.
     *
     * Depending on device features and user permissions for camera access, any of the following errors
     * could be the rejected result of the returned promise:
     * - <tt>PermissionDeniedError</tt>
     * - <tt>NotAllowedError</tt>
     * - <tt>NotFoundError</tt>
     * - <tt>AbortError</tt>
     * - <tt>NotReadableError</tt>
     * - <tt>InternalError</tt>
     * - <tt>NoCameraAvailableError</tt>
     *
     * @param cameraSettings The new camera options used when accessing the camera, by default HD resolution is used.
     * @returns A promise resolving to the updated [[BarcodePicker]] object when the camera is updated
     * (and accessed, if camera access is currently enabled).
     */
    applyCameraSettings(cameraSettings?: CameraSettings): Promise<BarcodePicker>;
    /**
     * @returns Whether the picker is in a visible state or not.
     */
    isVisible(): boolean;
    /**
     * Enable or disable picker visibility.
     *
     * @param visible Whether the picker is in a visible state or not.
     * @returns The updated [[BarcodePicker]] object.
     */
    setVisible(visible: boolean): BarcodePicker;
    /**
     * @returns Whether the camera video is mirrored along the vertical axis.
     */
    isMirrorImageEnabled(): boolean;
    /**
     * Enable or disable camera video mirroring along the vertical axis.
     *
     * @param enabled Whether the camera video is mirrored along the vertical axis.
     * @returns The updated [[BarcodePicker]] object.
     */
    setMirrorImageEnabled(enabled: boolean): BarcodePicker;
    /**
     * @returns Whether a sound should be played on barcode recognition (iOS requires user input).
     */
    isPlaySoundOnScanEnabled(): boolean;
    /**
     * Enable or disable playing a sound on barcode recognition (iOS requires user input).
     *
     * @param enabled Whether a sound should be played on barcode recognition.
     * @returns The updated [[BarcodePicker]] object.
     */
    setPlaySoundOnScanEnabled(enabled: boolean): BarcodePicker;
    /**
     * @returns Whether the device should vibrate on barcode recognition (only Chrome & Firefox, requires user input).
     */
    isVibrateOnScanEnabled(): boolean;
    /**
     * Enable or disable vibrating the device on barcode recognition (only Chrome & Firefox, requires user input).
     *
     * @param enabled Whether the device should vibrate on barcode recognition.
     * @returns The updated [[BarcodePicker]] object.
     */
    setVibrateOnScanEnabled(enabled: boolean): BarcodePicker;
    /**
     * Enable or disable the torch/flashlight of the device (when available, only Chrome).
     * Changing active camera or camera settings will cause the torch to become disabled.
     *
     * A button on the [[BarcodePicker]] GUI to let the user toggle this functionality can also be set
     * on creation via the <i>enableTorchToggle</i> option (enabled by default, when available).
     *
     * @param enabled Whether the torch should be enabled or disabled.
     * @returns The updated [[BarcodePicker]] object.
     */
    setTorchEnabled(enabled: boolean): BarcodePicker;
    /**
     * Set the zoom level of the device (when available, only Chrome).
     * Changing active camera or camera settings will cause the zoom to be reset.
     *
     * @param zoomPercentage The percentage of the max zoom (between 0 and 1).
     * @returns The updated [[BarcodePicker]] object.
     */
    setZoom(zoomPercentage: number): BarcodePicker;
    /**
     * @returns Whether the barcode picker has loaded the external Scandit Engine library and is ready to scan.
     */
    isReady(): boolean;
    /**
     * Add the listener function to the listeners array for the "ready" event, fired when the external
     * Scandit Engine library has been loaded and the barcode picker can thus start to scan barcodes.
     *
     * No checks are made to see if the listener has already been added.
     * Multiple calls passing the same listener will result in the listener being added, and called, multiple times.
     *
     * @param listener The listener function.
     * @returns The updated [[BarcodePicker]] object.
     */
    onReady(listener: () => void): BarcodePicker;
    /**
     * Add the listener function to the listeners array for the "scan" event, fired when new barcodes
     * are recognized in the image frame. The returned barcodes are affected
     * by the [[ScanSettings.setCodeDuplicateFilter]] option.
     *
     * No checks are made to see if the listener has already been added.
     * Multiple calls passing the same listener will result in the listener being added, and called, multiple times.
     *
     * @param listener The listener function, which will be invoked with a [[ScanResult]] object.
     * @param once Whether the listener should just be triggered only once and then discarded.
     * @returns The updated [[BarcodePicker]] object.
     */
    onScan(listener: (scanResult: ScanResult) => void, once?: boolean): BarcodePicker;
    /**
     * Removes the specified listener from the "scan" event listener array.
     * This will remove, at most, one instance of a listener from the listener array.
     * If any single listener has been added multiple times then this method must
     * be called multiple times to remove each instance.
     *
     * @param listener The listener function to be removed.
     * @returns The updated [[BarcodePicker]] object.
     */
    removeScanListener(listener: (scanResult: ScanResult) => void): BarcodePicker;
    /**
     * Removes all listeners from the "scan" event listener array.
     *
     * @returns The updated [[BarcodePicker]] object.
     */
    removeScanListeners(): BarcodePicker;
    /**
     * Add the listener function to the listeners array for the "scan error" event, fired when an error occurs
     * during scanning initialization and execution. The barcode picker will be automatically paused when this happens.
     *
     * No checks are made to see if the listener has already been added.
     * Multiple calls passing the same listener will result in the listener being added, and called, multiple times.
     *
     * @param listener The listener function, which will be invoked with an <tt>ScanditEngineError</tt> object.
     * @param once Whether the listener should just be triggered only once and then discarded.
     * @returns The updated [[BarcodePicker]] object.
     */
    onScanError(listener: (error: Error) => void, once?: boolean): BarcodePicker;
    /**
     * Removes the specified listener from the "scan error" event listener array.
     * This will remove, at most, one instance of a listener from the listener array.
     * If any single listener has been added multiple times then this method must
     * be called multiple times to remove each instance.
     *
     * @param listener The listener function to be removed.
     * @returns The updated [[BarcodePicker]] object.
     */
    removeScanErrorListener(listener: (scanResult: ScanResult) => void): BarcodePicker;
    /**
     * Removes all listeners from the "scan error" event listener array.
     *
     * @returns The updated [[BarcodePicker]] object.
     */
    removeScanErrorListeners(): BarcodePicker;
    /**
     * Set the GUI style for the picker.
     *
     * @param guiStyle The new GUI style to be applied.
     * @returns The updated [[BarcodePicker]] object.
     */
    setGuiStyle(guiStyle: BarcodePicker.GuiStyle): BarcodePicker;
    /**
     * Set the fit type for the video element of the picker.
     *
     * If the "cover" type is selected the maximum available search area for barcode detection is (continuously) adjusted
     * automatically according to the visible area of the picker.
     *
     * @param objectFit The new fit type to be applied.
     * @returns The updated [[BarcodePicker]] object.
     */
    setVideoFit(objectFit: BarcodePicker.ObjectFit): BarcodePicker;
    /**
     * Access the currently set or default camera, requesting user permissions if needed.
     * This method is meant to be used after the picker has been initialized with disabled camera access
     * (<i>cameraAccess</i>=false). Calling this doesn't do anything if the camera has already been accessed.
     *
     * Depending on device features and user permissions for camera access, any of the following errors
     * could be the rejected result of the returned promise:
     * - <tt>PermissionDeniedError</tt>
     * - <tt>NotAllowedError</tt>
     * - <tt>NotFoundError</tt>
     * - <tt>AbortError</tt>
     * - <tt>NotReadableError</tt>
     * - <tt>InternalError</tt>
     * - <tt>NoCameraAvailableError</tt>
     *
     * @returns A promise resolving to the updated [[BarcodePicker]] object when the camera is accessed.
     */
    accessCamera(): Promise<BarcodePicker>;
    /**
     * Create a new parser object.
     *
     * @param dataFormat The format of the input data for the parser.
     * @returns The newly created parser.
     */
    createParserForFormat(dataFormat: Parser.DataFormat): Parser;
    /**
     * Reassign the barcode picker to a different HTML element.
     *
     * All the barcode picker elements inside the current origin element will be moved to the new given one.
     *
     * @param originElement The HTMLElement into which all the necessary elements for the picker will be moved.
     * @returns The updated [[BarcodePicker]] object.
     */
    reassignOriginElement(originElement: HTMLElement): BarcodePicker;
    /**
     * Set the target frames per second to be processed by the scanning engine.
     *
     * The final speed is limited by the camera framerate (usually 30 FPS) and the frame processing time of the device.
     * By setting this to lower numbers devices can save power by performing less work during scanning operations,
     * depending on device speed (faster devices can "sleep" for longer periods).
     *
     * @param targetScanningFPS The target frames per second to be processed.
     * Must be a number bigger than 0, by default set to 30.
     * @returns The updated [[BarcodePicker]] object.
     */
    setTargetScanningFPS(targetScanningFPS: number): BarcodePicker;
    private resizeIfNeeded();
    private setupVideoElement(videoElement);
    private setupVideoImageCanvasElement(videoImageCanvasElement);
    private flashLaser();
    private flashViewfinder();
    private setupAssets();
    private checkAndRecoverPlayback();
    private setupGlobalListeners();
    private removeGlobalListeners();
    private videoProcessing();
}
export declare namespace BarcodePicker {
    /**
     * GUI style to be used by a barcode picker, used to hint barcode placement in the frame.
     */
    enum GuiStyle {
        /**
         * @hidden
         * @deprecated
         */
        NONE_DEPRECATED = 0,
        /**
         * @hidden
         * @deprecated
         */
        LASER_DEPRECATED = 1,
        /**
         * @hidden
         * @deprecated
         */
        VIEWFINDER_DEPRECATED = 2,
        /**
         * No GUI is shown to indicate where the barcode should be placed.
         * Be aware that the Scandit logo continues to be displayed as showing it is part of the license agreement.
         */
        NONE = "none",
        /**
         * A laser line is shown.
         */
        LASER = "laser",
        /**
         * A rectangular viewfinder with rounded corners is shown.
         */
        VIEWFINDER = "viewfinder",
    }
    /**
     * Fit type used to control the resizing (scale) of the barcode picker to fit in its container <i>originElement</i>.
     */
    enum ObjectFit {
        /**
         * Scale to maintain aspect ratio while fitting within the <i>originElement</i>'s content box.
         * Aspect ratio is preserved, so the barcode picker will be "letterboxed" if its aspect ratio
         * does not match the aspect ratio of the box.
         */
        CONTAIN = "contain",
        /**
         * Scale to maintain aspect ratio while filling the <i>originElement</i>'s entire content box.
         * Aspect ratio is preserved, so the barcode picker will be clipped to fit if its aspect ratio
         * does not match the aspect ratio of the box.
         */
        COVER = "cover",
    }
}
