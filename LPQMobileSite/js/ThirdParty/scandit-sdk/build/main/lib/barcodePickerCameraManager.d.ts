import { BarcodePicker } from "./barcodePicker";
import { Camera } from "./camera";
import { CameraSettings } from "./cameraSettings";
/**
 * @hidden
 *
 * A barcode picker utility class used to handle camera interaction.
 */
export declare class BarcodePickerCameraManager {
    private barcodePicker;
    private static readonly isIOSDevice;
    private static readonly cameraAccessTimeoutMs;
    private static readonly cameraMetadataCheckTimeoutMs;
    private static readonly cameraMetadataCheckIntervalMs;
    private static readonly getCapabilitiesTimeoutMs;
    private static readonly autofocusIntervalMs;
    private static readonly manualToAutofocusResumeTimeoutMs;
    private static readonly manualFocusWaitTimeoutMs;
    activeCamera: Camera | undefined;
    activeCameraSettings: CameraSettings | undefined;
    private selectedCamera;
    private selectedCameraSettings;
    private mediaStream;
    private mediaTrackCapabilities;
    private postStreamInitializationListener;
    private triggerManualFocusListener;
    private triggerZoomStartListener;
    private triggerZoomMoveListener;
    private cameraAccessTimeout;
    private cameraMetadataCheckInterval;
    private getCapabilitiesTimeout;
    private autofocusInterval;
    private manualToAutofocusResumeTimeout;
    private manualFocusWaitTimeout;
    private enableCameraSwitcher;
    private enableTorchToggle;
    private enableTapToFocus;
    private enablePinchToZoom;
    private torchEnabled;
    private cameraSwitcherElement;
    private torchToggleElement;
    private pinchToZoomDistance;
    private pinchToZoomInitialZoom;
    constructor(barcodePicker: BarcodePicker);
    setUIOptions(enableCameraSwitcher: boolean, enableTorchToggle: boolean, enableTapToFocus: boolean, enablePinchToZoom: boolean): void;
    setSelectedCamera(camera?: Camera): void;
    setSelectedCameraSettings(cameraSettings?: CameraSettings): void;
    setupCameras(): Promise<BarcodePicker>;
    stopStream(): void;
    applyCameraSettings(cameraSettings?: CameraSettings): Promise<BarcodePicker>;
    reinitializeCamera(): Promise<BarcodePicker>;
    initializeCameraWithSettings(camera: Camera, cameraSettings?: CameraSettings): Promise<BarcodePicker>;
    setTorchEnabled(enabled: boolean): void;
    setZoom(zoomPercentage: number, currentZoom?: number): void;
    private postStreamInitialization();
    private triggerManualFocus(event?);
    private triggerZoomStart(event?);
    private triggerZoomMove(event?);
    private storeStreamCapabilities();
    private setupAutofocus();
    private triggerAutoFocus();
    private triggerFocusMode(focusMode);
    private enableTapToFocusListeners();
    private enablePinchToZoomListeners();
    private initializeCamera(camera, resolutionFallbackLevel?);
    private resolveInitializeCamera(camera, resolve, reject);
    private setupCameraSwitcher();
    private setupTorchToggle();
}
