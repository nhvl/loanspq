/**
 * @hidden
 */
export declare namespace BrowserHelper {
    /**
     * @returns The first matching error indicating a missing browser feature, or undefined.
     */
    function checkBrowserCompatibility(): Error | undefined;
    /**
     * Get a device id for the current browser, when available it's retrieved from a cookie,
     * when not it's randomly generated and stored in a cookie to be retrieved by later calls.
     *
     * @returns The device id for the current browser.
     */
    function getDeviceId(): string;
}
