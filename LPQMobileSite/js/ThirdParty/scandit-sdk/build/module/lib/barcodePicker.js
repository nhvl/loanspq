import { EventEmitter } from "eventemitter3";
import { Howl, Howler } from "howler/dist/howler.core.min.js";
import { beepSound, laserActiveImage, laserPausedImage, scanditLogoImage } from "./assets/base64assets";
import { userLicenseKey } from "../index";
import { BarcodePickerCameraManager } from "./barcodePickerCameraManager";
import { BrowserHelper } from "./browserHelper";
import { CustomError } from "./customError";
import { ImageSettings } from "./imageSettings";
import { Scanner } from "./scanner";
import { ScanSettings } from "./scanSettings";
/**
 * A barcode picker element used to get and show camera input and perform scanning operations.
 *
 * The barcode picker will automatically fit and scale inside the given <i>originElement</i>.
 *
 * Each barcode picker internally contains a [[Scanner]] object with its own WebWorker thread running a
 * separate copy of the external Scandit Engine library. To optimize loading times and performance it's
 * recommended to reuse the same picker and to already create the picker in advance (hidden) and just
 * display it when needed whenever possible.
 *
 * As the loading of the external Scandit Engine library can take some time the picker always starts inactive
 * (but showing GUI and video) and then activates, if not paused, as soon as the library is ready to scan.
 * The [[onReady]] method can be used to set up a listener function to be called when the library is loaded.
 *
 * You are not allowed to hide the Scandit logo present in the corner of the GUI.
 */
export class BarcodePicker {
    constructor(originElement, { visible = true, playSoundOnScan = false, vibrateOnScan = false, scanningPaused = false, guiStyle = BarcodePicker.GuiStyle.LASER, videoFit = BarcodePicker.ObjectFit.CONTAIN, scanSettings = new ScanSettings(), targetScanningFPS = 30 } = {}) {
        this.originElement = originElement;
        this.isReadyToWork = false;
        this.destroyed = false;
        this.scanningPaused = scanningPaused;
        Howler.autoSuspend = false;
        this.beepSound = new Howl({
            src: beepSound
        });
        this.vibrateFunction =
            navigator.vibrate ||
                navigator.webkitVibrate ||
                navigator.mozVibrate ||
                navigator.msVibrate;
        this.eventEmitter = new EventEmitter();
        this.grandParentElement = document.createElement("div");
        this.grandParentElement.className = BarcodePicker.grandParentElementClassName;
        this.originElement.appendChild(this.grandParentElement);
        this.parentElement = document.createElement("div");
        this.parentElement.className = BarcodePicker.parentElementClassName;
        this.grandParentElement.appendChild(this.parentElement);
        this.videoElement = document.createElement("video");
        this.setupVideoElement(this.videoElement);
        this.setupVideoImageCanvasElement(document.createElement("canvas"));
        this.setVisible(visible);
        this.setPlaySoundOnScanEnabled(playSoundOnScan);
        this.setVibrateOnScanEnabled(vibrateOnScan);
        this.setTargetScanningFPS(targetScanningFPS);
        this.scanner = new Scanner({ scanSettings: scanSettings });
        this.scanner.onReady(() => {
            this.isReadyToWork = true;
            this.eventEmitter.emit("ready");
        });
        window.requestAnimationFrame(this.videoProcessing.bind(this));
        this.resizeInterval = window.setInterval(() => {
            this.resizeIfNeeded();
        }, 250);
        this.laserActiveImageElement = document.createElement("img");
        this.laserPausedImageElement = document.createElement("img");
        this.viewfinderElement = document.createElement("div");
        this.setupAssets();
        this.setGuiStyle(guiStyle);
        this.setVideoFit(videoFit);
        this.setupGlobalListeners();
        this.cameraManager = new BarcodePickerCameraManager(this);
    }
    /**
     * Creates a BarcodePicker instance, creating the needed HTML in the given origin element.
     * If the <i>accessCamera</i> option is enabled (active by default) the available cameras are accessed
     * and a camera access permission is requested to the user if needed.
     * This object expects that at least a camera is available. The active camera is accessed and kept active during the
     * lifetime of the picker (also when hidden or scanning is paused), and is only released when [[destroy]] is called.
     *
     * It is required to having configured the library via [[configure]] before this object can be created.
     *
     * By default an alert is shown if an internal error during scanning is encountered which prevents the scanning
     * procedure from continuing. As this uses the built-in [[onScanError]] event functionality, if unwanted it can be
     * disabled by calling [[removeScanErrorListeners]] on the BarcodePicker instance (right after creation).
     *
     * Depending on parameters, device features and user permissions for camera access, any of the following errors
     * could be the rejected result of the returned promise:
     * - <tt>LibraryNotConfiguredError</tt>
     * - <tt>NoOriginElementError</tt>
     * - <tt>UnsupportedBrowserError</tt>
     * - <tt>PermissionDeniedError</tt>
     * - <tt>NotAllowedError</tt>
     * - <tt>NotFoundError</tt>
     * - <tt>AbortError</tt>
     * - <tt>NotReadableError</tt>
     * - <tt>InternalError</tt>
     * - <tt>NoCameraAvailableError</tt>
     *
     * @param originElement The HTMLElement inside which all the necessary elements for the picker will be added.
     * @param visible <div class="tsd-signature-symbol">Default =&nbsp;true</div>
     * Whether the picker starts in a visible state.
     * @param playSoundOnScan <div class="tsd-signature-symbol">Default =&nbsp;false</div>
     * Whether a sound is played on barcode scanned (iOS requires user input).
     * @param vibrateOnScan <div class="tsd-signature-symbol">Default =&nbsp;false</div>
     * Whether the device vibrates on barcode scanned (only Chrome & Firefox, requires user input).
     * @param scanningPaused <div class="tsd-signature-symbol">Default =&nbsp;false</div>
     * Whether the picker starts in a paused scanning state.
     * @param guiStyle <div class="tsd-signature-symbol">Default =&nbsp;GuiStyle.LASER</div>
     * The GUI style for the picker.
     * @param videoFit <div class="tsd-signature-symbol">Default =&nbsp;ObjectFit.CONTAIN</div>
     * The fit type for the video element of the picker.
     * @param enableCameraSwitcher <div class="tsd-signature-symbol">Default =&nbsp;true</div>
     * Whether to show a GUI button to switch between different cameras (when available).
     * @param enableTorchToggle <div class="tsd-signature-symbol">Default =&nbsp;true</div>
     * Whether to show a GUI button to toggle device torch on/off (when available, only Chrome).
     * @param enableTapToFocus <div class="tsd-signature-symbol">Default =&nbsp;true</div>
     * Whether to trigger a manual focus of the camera when clicking/tapping on the video (when available, only Chrome).
     * @param enablePinchToZoom <div class="tsd-signature-symbol">Default =&nbsp;true</div>
     * Whether to control the zoom of the camera when doing a pinching gesture on the video (when available, only Chrome).
     * @param accessCamera <div class="tsd-signature-symbol">Default =&nbsp;true</div>
     * Whether to immediately access the camera (and requesting user permissions if needed) on picker creation.
     * @param camera <div class="tsd-signature-symbol">Default =&nbsp;undefined</div>
     * The camera to be used for video input, if not specified the back or only camera will be used.
     * @param cameraSettings <div class="tsd-signature-symbol">Default =&nbsp;undefined</div>
     * The camera options used when accessing the camera, by default HD resolution is used.
     * @param scanSettings <div class="tsd-signature-symbol">Default =&nbsp;new ScanSettings()</div>
     * The configuration object for scanning options (All symbologies disabled by default).
     * @param targetScanningFPS <div class="tsd-signature-symbol">Default =&nbsp;30</div>
     * The target frames per second to be processed, the final speed is limited by the camera framerate (usually 30 FPS)
     * and the frame processing time of the device. By setting this to lower numbers devices can save power by performing
     * less work during scanning operations, depending on device speed (faster devices can "sleep" for longer periods).
     * Must be a number bigger than 0.
     * @returns A promise resolving to the created ready [[BarcodePicker]] object.
     */
    static create(originElement, { visible = true, playSoundOnScan = false, vibrateOnScan = false, scanningPaused = false, guiStyle = BarcodePicker.GuiStyle.LASER, videoFit = BarcodePicker.ObjectFit.CONTAIN, scanSettings = new ScanSettings(), enableCameraSwitcher = true, enableTorchToggle = true, enableTapToFocus = true, enablePinchToZoom = true, accessCamera = true, camera, cameraSettings, targetScanningFPS = 30 } = {}) {
        const unsupportedBrowserError = BrowserHelper.checkBrowserCompatibility();
        if (unsupportedBrowserError != null) {
            return Promise.reject(unsupportedBrowserError);
        }
        if (userLicenseKey == null || userLicenseKey.trim() === "") {
            return Promise.reject(new CustomError({
                name: "LibraryNotConfiguredError",
                message: "The library has not correctly been configured yet, please call 'configure' with valid parameters"
            }));
        }
        if (!(originElement instanceof HTMLElement)) {
            return Promise.reject(new CustomError({
                name: "NoOriginElementError",
                message: "A valid origin HTML element must be given"
            }));
        }
        if (targetScanningFPS <= 0) {
            targetScanningFPS = 30;
        }
        const barcodePicker = new BarcodePicker(originElement, {
            visible,
            playSoundOnScan,
            vibrateOnScan,
            scanningPaused,
            guiStyle,
            videoFit,
            scanSettings,
            targetScanningFPS
        });
        barcodePicker.cameraManager.setUIOptions(enableCameraSwitcher, enableTorchToggle, enableTapToFocus, enablePinchToZoom);
        barcodePicker.cameraManager.setSelectedCamera(camera);
        barcodePicker.cameraManager.setSelectedCameraSettings(cameraSettings);
        barcodePicker.cameraAccess = accessCamera;
        // Show error in alert on ScanError by default for easier customer debugging
        barcodePicker.onScanError(error => {
            alert(error);
        });
        if (accessCamera) {
            return barcodePicker.cameraManager.setupCameras();
        }
        return Promise.resolve(barcodePicker);
    }
    /**
     * Stop scanning and displaying video output, remove HTML elements added to the page,
     * destroy the internal [[Scanner]] and destroy the barcode picker itself; ensuring complete cleanup.
     *
     * This method should be called after you don't plan to use the picker anymore,
     * before the object is automatically cleaned up by JavaScript.
     * The barcode picker must not be used in any way after this call.
     */
    destroy() {
        this.pauseScanning(true);
        this.destroyed = true;
        this.scanner.destroy();
        this.eventEmitter.removeAllListeners();
        window.clearInterval(this.resizeInterval);
        this.removeGlobalListeners();
        this.grandParentElement.remove();
        this.originElement.classList.remove("scandit-hidden");
    }
    /**
     * Applies a new set of scan settings to the internal scanner (replacing old settings).
     *
     * @param scanSettings The scan configuration object to be applied to the scanner.
     * @returns The updated [[BarcodePicker]] object.
     */
    applyScanSettings(scanSettings) {
        this.scanner.applyScanSettings(scanSettings);
        return this;
    }
    /**
     * @returns Whether the scanning is currently paused.
     */
    isScanningPaused() {
        return this.scanningPaused;
    }
    /**
     * Pause the recognition of codes in the input image.
     *
     * By default video from the camera is still shown, if the <i>pauseCamera</i> option is enabled the camera stream
     * is paused (camera access is fully interrupted) and will be resumed when calling [[resumeScanning]],
     * possibly requesting user permissions if needed.
     *
     * @param pauseCamera Whether to also pause the camera stream.
     * @returns The updated [[BarcodePicker]] object.
     */
    pauseScanning(pauseCamera = false) {
        this.scanningPaused = true;
        if (pauseCamera) {
            this.cameraManager.stopStream();
        }
        if (this.scanner.isReady()) {
            this.laserActiveImageElement.classList.add("scandit-hidden-opacity");
            this.laserPausedImageElement.classList.remove("scandit-hidden-opacity");
            this.viewfinderElement.classList.add("paused");
        }
        return this;
    }
    /**
     * Resume the recognition of codes in the input image.
     *
     * If the camera stream was stopped when calling [[pauseScanning]], the camera stream is also resumed and
     * user permissions are requested if needed to resume video input.
     *
     * @returns The updated [[BarcodePicker]] object.
     */
    resumeScanning() {
        this.scanningPaused = false;
        if (this.scanner.isReady()) {
            this.laserPausedImageElement.classList.add("scandit-hidden-opacity");
            this.laserActiveImageElement.classList.remove("scandit-hidden-opacity");
            this.viewfinderElement.classList.remove("paused");
        }
        if (this.cameraManager.activeCamera == null && this.cameraAccess) {
            return this.cameraManager.setupCameras();
        }
        return Promise.resolve(this);
    }
    /**
     * @returns The currently active camera.
     */
    getActiveCamera() {
        return this.cameraManager.activeCamera;
    }
    /**
     * Select a camera to be used for video input.
     *
     * If camera access is enabled, the camera is enabled and accessed.
     *
     * Depending on device features and user permissions for camera access, any of the following errors
     * could be the rejected result of the returned promise:
     * - <tt>PermissionDeniedError</tt>
     * - <tt>NotAllowedError</tt>
     * - <tt>NotFoundError</tt>
     * - <tt>AbortError</tt>
     * - <tt>NotReadableError</tt>
     * - <tt>InternalError</tt>
     * - <tt>NoCameraAvailableError</tt>
     *
     * @param camera The new camera to be used.
     * @param cameraSettings The camera options used when accessing the camera, by default HD resolution is used.
     * @returns A promise resolving to the updated [[BarcodePicker]] object when the camera is set
     * (and accessed, if camera access is currently enabled).
     */
    setActiveCamera(camera, cameraSettings) {
        if (!this.cameraAccess) {
            this.cameraManager.setSelectedCamera(camera);
            this.cameraManager.setSelectedCameraSettings(cameraSettings);
            return Promise.resolve(this);
        }
        return this.cameraManager.initializeCameraWithSettings(camera, cameraSettings);
    }
    /**
     * Try to apply new settings to the currently used camera for video input,
     * if no settings are passed the default ones are set.
     *
     * If camera access is enabled, the camera is updated and accessed with the new settings.
     *
     * Depending on device features and user permissions for camera access, any of the following errors
     * could be the rejected result of the returned promise:
     * - <tt>PermissionDeniedError</tt>
     * - <tt>NotAllowedError</tt>
     * - <tt>NotFoundError</tt>
     * - <tt>AbortError</tt>
     * - <tt>NotReadableError</tt>
     * - <tt>InternalError</tt>
     * - <tt>NoCameraAvailableError</tt>
     *
     * @param cameraSettings The new camera options used when accessing the camera, by default HD resolution is used.
     * @returns A promise resolving to the updated [[BarcodePicker]] object when the camera is updated
     * (and accessed, if camera access is currently enabled).
     */
    applyCameraSettings(cameraSettings) {
        if (!this.cameraAccess) {
            this.cameraManager.setSelectedCameraSettings(cameraSettings);
            return Promise.resolve(this);
        }
        return this.cameraManager.applyCameraSettings(cameraSettings);
    }
    /**
     * @returns Whether the picker is in a visible state or not.
     */
    isVisible() {
        return this.visible;
    }
    /**
     * Enable or disable picker visibility.
     *
     * @param visible Whether the picker is in a visible state or not.
     * @returns The updated [[BarcodePicker]] object.
     */
    setVisible(visible) {
        this.visible = visible;
        if (visible) {
            this.originElement.classList.remove("scandit-hidden");
        }
        else {
            this.originElement.classList.add("scandit-hidden");
        }
        return this;
    }
    /**
     * @returns Whether the camera video is mirrored along the vertical axis.
     */
    isMirrorImageEnabled() {
        return this.mirrorImage;
    }
    /**
     * Enable or disable camera video mirroring along the vertical axis.
     *
     * @param enabled Whether the camera video is mirrored along the vertical axis.
     * @returns The updated [[BarcodePicker]] object.
     */
    setMirrorImageEnabled(enabled) {
        this.mirrorImage = enabled;
        if (enabled) {
            this.videoElement.classList.add("mirrored");
        }
        else {
            this.videoElement.classList.remove("mirrored");
        }
        return this;
    }
    /**
     * @returns Whether a sound should be played on barcode recognition (iOS requires user input).
     */
    isPlaySoundOnScanEnabled() {
        return this.playSoundOnScan;
    }
    /**
     * Enable or disable playing a sound on barcode recognition (iOS requires user input).
     *
     * @param enabled Whether a sound should be played on barcode recognition.
     * @returns The updated [[BarcodePicker]] object.
     */
    setPlaySoundOnScanEnabled(enabled) {
        this.playSoundOnScan = enabled;
        return this;
    }
    /**
     * @returns Whether the device should vibrate on barcode recognition (only Chrome & Firefox, requires user input).
     */
    isVibrateOnScanEnabled() {
        return this.vibrateOnScan;
    }
    /**
     * Enable or disable vibrating the device on barcode recognition (only Chrome & Firefox, requires user input).
     *
     * @param enabled Whether the device should vibrate on barcode recognition.
     * @returns The updated [[BarcodePicker]] object.
     */
    setVibrateOnScanEnabled(enabled) {
        this.vibrateOnScan = enabled;
        return this;
    }
    /**
     * Enable or disable the torch/flashlight of the device (when available, only Chrome).
     * Changing active camera or camera settings will cause the torch to become disabled.
     *
     * A button on the [[BarcodePicker]] GUI to let the user toggle this functionality can also be set
     * on creation via the <i>enableTorchToggle</i> option (enabled by default, when available).
     *
     * @param enabled Whether the torch should be enabled or disabled.
     * @returns The updated [[BarcodePicker]] object.
     */
    setTorchEnabled(enabled) {
        this.cameraManager.setTorchEnabled(enabled);
        return this;
    }
    /**
     * Set the zoom level of the device (when available, only Chrome).
     * Changing active camera or camera settings will cause the zoom to be reset.
     *
     * @param zoomPercentage The percentage of the max zoom (between 0 and 1).
     * @returns The updated [[BarcodePicker]] object.
     */
    setZoom(zoomPercentage) {
        this.cameraManager.setZoom(zoomPercentage);
        return this;
    }
    /**
     * @returns Whether the barcode picker has loaded the external Scandit Engine library and is ready to scan.
     */
    isReady() {
        return this.isReadyToWork;
    }
    /**
     * Add the listener function to the listeners array for the "ready" event, fired when the external
     * Scandit Engine library has been loaded and the barcode picker can thus start to scan barcodes.
     *
     * No checks are made to see if the listener has already been added.
     * Multiple calls passing the same listener will result in the listener being added, and called, multiple times.
     *
     * @param listener The listener function.
     * @returns The updated [[BarcodePicker]] object.
     */
    onReady(listener) {
        if (this.isReadyToWork) {
            listener();
        }
        else {
            this.eventEmitter.once("ready", listener, this);
        }
        return this;
    }
    /**
     * Add the listener function to the listeners array for the "scan" event, fired when new barcodes
     * are recognized in the image frame. The returned barcodes are affected
     * by the [[ScanSettings.setCodeDuplicateFilter]] option.
     *
     * No checks are made to see if the listener has already been added.
     * Multiple calls passing the same listener will result in the listener being added, and called, multiple times.
     *
     * @param listener The listener function, which will be invoked with a [[ScanResult]] object.
     * @param once Whether the listener should just be triggered only once and then discarded.
     * @returns The updated [[BarcodePicker]] object.
     */
    onScan(listener, once) {
        if (once === true) {
            this.eventEmitter.once("scan", listener, this);
        }
        else {
            this.eventEmitter.on("scan", listener, this);
        }
        return this;
    }
    /**
     * Removes the specified listener from the "scan" event listener array.
     * This will remove, at most, one instance of a listener from the listener array.
     * If any single listener has been added multiple times then this method must
     * be called multiple times to remove each instance.
     *
     * @param listener The listener function to be removed.
     * @returns The updated [[BarcodePicker]] object.
     */
    removeScanListener(listener) {
        this.eventEmitter.removeListener("scan", listener);
        return this;
    }
    /**
     * Removes all listeners from the "scan" event listener array.
     *
     * @returns The updated [[BarcodePicker]] object.
     */
    removeScanListeners() {
        this.eventEmitter.removeAllListeners("scan");
        return this;
    }
    /**
     * Add the listener function to the listeners array for the "scan error" event, fired when an error occurs
     * during scanning initialization and execution. The barcode picker will be automatically paused when this happens.
     *
     * No checks are made to see if the listener has already been added.
     * Multiple calls passing the same listener will result in the listener being added, and called, multiple times.
     *
     * @param listener The listener function, which will be invoked with an <tt>ScanditEngineError</tt> object.
     * @param once Whether the listener should just be triggered only once and then discarded.
     * @returns The updated [[BarcodePicker]] object.
     */
    onScanError(listener, once) {
        if (once === true) {
            this.eventEmitter.once("scan-error", listener, this);
        }
        else {
            this.eventEmitter.on("scan-error", listener, this);
        }
        return this;
    }
    /**
     * Removes the specified listener from the "scan error" event listener array.
     * This will remove, at most, one instance of a listener from the listener array.
     * If any single listener has been added multiple times then this method must
     * be called multiple times to remove each instance.
     *
     * @param listener The listener function to be removed.
     * @returns The updated [[BarcodePicker]] object.
     */
    removeScanErrorListener(listener) {
        this.eventEmitter.removeListener("scan-error", listener);
        return this;
    }
    /**
     * Removes all listeners from the "scan error" event listener array.
     *
     * @returns The updated [[BarcodePicker]] object.
     */
    removeScanErrorListeners() {
        this.eventEmitter.removeAllListeners("scan-error");
        return this;
    }
    /**
     * Set the GUI style for the picker.
     *
     * @param guiStyle The new GUI style to be applied.
     * @returns The updated [[BarcodePicker]] object.
     */
    setGuiStyle(guiStyle) {
        this.guiStyle = guiStyle;
        switch (this.guiStyle) {
            case BarcodePicker.GuiStyle.NONE_DEPRECATED:
            case BarcodePicker.GuiStyle.NONE:
                this.laserActiveImageElement.classList.add("scandit-hidden");
                this.laserPausedImageElement.classList.add("scandit-hidden");
                this.viewfinderElement.classList.add("scandit-hidden");
                break;
            case BarcodePicker.GuiStyle.LASER_DEPRECATED:
            case BarcodePicker.GuiStyle.LASER:
                this.laserActiveImageElement.classList.remove("scandit-hidden");
                this.laserPausedImageElement.classList.remove("scandit-hidden");
                this.viewfinderElement.classList.add("scandit-hidden");
                break;
            case BarcodePicker.GuiStyle.VIEWFINDER_DEPRECATED:
            case BarcodePicker.GuiStyle.VIEWFINDER:
                this.laserActiveImageElement.classList.add("scandit-hidden");
                this.laserPausedImageElement.classList.add("scandit-hidden");
                this.viewfinderElement.classList.remove("scandit-hidden");
                break;
            default:
                break;
        }
        return this;
    }
    /**
     * Set the fit type for the video element of the picker.
     *
     * If the "cover" type is selected the maximum available search area for barcode detection is (continuously) adjusted
     * automatically according to the visible area of the picker.
     *
     * @param objectFit The new fit type to be applied.
     * @returns The updated [[BarcodePicker]] object.
     */
    setVideoFit(objectFit) {
        this.videoFit = objectFit;
        if (objectFit === BarcodePicker.ObjectFit.COVER) {
            this.videoElement.style.objectFit = "cover";
            this.videoElement.dataset.objectFit = "cover"; // used by "objectFitPolyfill" library
        }
        else {
            this.videoElement.style.objectFit = "contain";
            this.videoElement.dataset.objectFit = "contain"; // used by "objectFitPolyfill" library
            const scanSettings = this.scanner.getScanSettings();
            scanSettings.setBaseSearchArea({ x: 0, y: 0, width: 1.0, height: 1.0 });
            this.scanner.applyScanSettings(scanSettings);
        }
        // Retrigger resize
        this.lastKnownOriginElementWidth = 0;
        this.lastKnownOriginElementHeight = 0;
        this.resizeIfNeeded();
        window.objectFitPolyfill(this.videoElement);
        return this;
    }
    /**
     * Access the currently set or default camera, requesting user permissions if needed.
     * This method is meant to be used after the picker has been initialized with disabled camera access
     * (<i>cameraAccess</i>=false). Calling this doesn't do anything if the camera has already been accessed.
     *
     * Depending on device features and user permissions for camera access, any of the following errors
     * could be the rejected result of the returned promise:
     * - <tt>PermissionDeniedError</tt>
     * - <tt>NotAllowedError</tt>
     * - <tt>NotFoundError</tt>
     * - <tt>AbortError</tt>
     * - <tt>NotReadableError</tt>
     * - <tt>InternalError</tt>
     * - <tt>NoCameraAvailableError</tt>
     *
     * @returns A promise resolving to the updated [[BarcodePicker]] object when the camera is accessed.
     */
    accessCamera() {
        if (!this.cameraAccess) {
            this.cameraAccess = true;
            return this.cameraManager.setupCameras();
        }
        return Promise.resolve(this);
    }
    /**
     * Create a new parser object.
     *
     * @param dataFormat The format of the input data for the parser.
     * @returns The newly created parser.
     */
    createParserForFormat(dataFormat) {
        return this.scanner.createParserForFormat(dataFormat);
    }
    /**
     * Reassign the barcode picker to a different HTML element.
     *
     * All the barcode picker elements inside the current origin element will be moved to the new given one.
     *
     * @param originElement The HTMLElement into which all the necessary elements for the picker will be moved.
     * @returns The updated [[BarcodePicker]] object.
     */
    reassignOriginElement(originElement) {
        if (!(originElement instanceof HTMLElement)) {
            throw new CustomError({
                name: "NoOriginElementError",
                message: "A valid origin HTML element must be given"
            });
        }
        if (!this.visible) {
            this.originElement.classList.remove("scandit-hidden");
            originElement.classList.add("scandit-hidden");
        }
        originElement.appendChild(this.grandParentElement);
        this.checkAndRecoverPlayback();
        this.resizeIfNeeded();
        this.originElement = originElement;
        return this;
    }
    /**
     * Set the target frames per second to be processed by the scanning engine.
     *
     * The final speed is limited by the camera framerate (usually 30 FPS) and the frame processing time of the device.
     * By setting this to lower numbers devices can save power by performing less work during scanning operations,
     * depending on device speed (faster devices can "sleep" for longer periods).
     *
     * @param targetScanningFPS The target frames per second to be processed.
     * Must be a number bigger than 0, by default set to 30.
     * @returns The updated [[BarcodePicker]] object.
     */
    setTargetScanningFPS(targetScanningFPS) {
        if (targetScanningFPS > 0) {
            this.targetScanningFPS = targetScanningFPS;
        }
        return this;
    }
    resizeIfNeeded() {
        if (this.videoElement.videoHeight < 1 ||
            this.originElement.clientHeight < 1 ||
            (this.lastKnownOriginElementWidth === this.originElement.clientWidth &&
                this.lastKnownOriginElementHeight === this.originElement.clientHeight)) {
            return;
        }
        this.parentElement.style.maxWidth = null;
        this.parentElement.style.maxHeight = null;
        const videoRatio = this.videoElement.videoWidth / this.videoElement.videoHeight;
        let width = this.originElement.clientWidth;
        let height = this.originElement.clientHeight;
        this.lastKnownOriginElementWidth = width;
        this.lastKnownOriginElementHeight = height;
        if (this.videoFit === BarcodePicker.ObjectFit.COVER) {
            let widthPercentage = 1;
            let heightPercentage = 1;
            if (videoRatio < width / height) {
                heightPercentage = Math.min(1, height / (width / videoRatio));
            }
            else {
                widthPercentage = Math.min(1, width / (height * videoRatio));
            }
            const scanSettings = this.scanner.getScanSettings();
            scanSettings.setBaseSearchArea({
                x: (1 - widthPercentage) / 2,
                y: (1 - heightPercentage) / 2,
                width: widthPercentage,
                height: heightPercentage
            });
            this.scanner.applyScanSettings(scanSettings);
            return;
        }
        if (videoRatio > width / height) {
            height = width / videoRatio;
        }
        else {
            width = height * videoRatio;
        }
        this.parentElement.style.maxWidth = `${Math.ceil(width)}px`;
        this.parentElement.style.maxHeight = `${Math.ceil(height)}px`;
    }
    setupVideoElement(videoElement) {
        videoElement.setAttribute("autoplay", "autoplay");
        videoElement.setAttribute("playsinline", "true");
        videoElement.setAttribute("muted", "muted");
        videoElement.className = BarcodePicker.videoElementClassName;
        videoElement.addEventListener("canplay", () => {
            if (this.cameraManager.activeCamera != null) {
                // Retrigger resize
                this.lastKnownOriginElementWidth = 0;
                this.lastKnownOriginElementHeight = 0;
                this.resizeIfNeeded();
                this.cameraManager.activeCamera.currentResolution = {
                    width: videoElement.videoWidth,
                    height: videoElement.videoHeight
                };
                this.videoImageCanvasContext.canvas.width = this.cameraManager.activeCamera.currentResolution.width;
                this.videoImageCanvasContext.canvas.height = this.cameraManager.activeCamera.currentResolution.height;
                this.scanner.applyImageSettings({
                    width: videoElement.videoWidth,
                    height: videoElement.videoHeight,
                    format: ImageSettings.Format.RGBA_8U
                });
            }
        });
        this.parentElement.appendChild(videoElement);
    }
    setupVideoImageCanvasElement(videoImageCanvasElement) {
        const canvasContext = videoImageCanvasElement.getContext("2d");
        if (canvasContext != null) {
            this.videoImageCanvasContext = canvasContext;
        }
    }
    flashLaser() {
        this.laserActiveImageElement.classList.remove("scandit-flash-color");
        // tslint:disable-next-line:no-unused-expression
        this.laserActiveImageElement.offsetHeight; // Trigger reflow to restart animation
        this.laserActiveImageElement.classList.add("scandit-flash-color");
    }
    flashViewfinder() {
        this.viewfinderElement.classList.remove("scandit-flash-white");
        // tslint:disable-next-line:no-unused-expression
        this.viewfinderElement.offsetHeight; // Trigger reflow to restart animation
        this.viewfinderElement.classList.add("scandit-flash-white");
    }
    setupAssets() {
        const scanditLogoImageElement = document.createElement("img");
        scanditLogoImageElement.src = scanditLogoImage;
        scanditLogoImageElement.className = BarcodePicker.scanditLogoImageElementClassName;
        this.parentElement.appendChild(scanditLogoImageElement);
        this.laserActiveImageElement.src = laserActiveImage;
        this.laserActiveImageElement.className = BarcodePicker.laserImageElementClassName;
        this.parentElement.appendChild(this.laserActiveImageElement);
        this.laserPausedImageElement.src = laserPausedImage;
        this.laserPausedImageElement.className = BarcodePicker.laserImageElementClassName;
        this.parentElement.appendChild(this.laserPausedImageElement);
        this.viewfinderElement.className = BarcodePicker.viewfinderElementClassName;
        this.parentElement.appendChild(this.viewfinderElement);
        // Show inactive GUI, as for now the scanner isn't ready yet
        this.laserActiveImageElement.classList.add("scandit-hidden-opacity");
        this.laserPausedImageElement.classList.remove("scandit-hidden-opacity");
        this.viewfinderElement.classList.add("paused");
    }
    checkAndRecoverPlayback() {
        if (this.cameraManager.activeCamera != null && this.videoElement != null && this.videoElement.srcObject != null) {
            if (!this.videoElement.srcObject.active) {
                this.cameraManager.reinitializeCamera().catch(error => {
                    this.fatalError = error;
                    console.error(error);
                });
            }
            else {
                const playPromise = this.videoElement.play();
                if (playPromise != null) {
                    playPromise.catch(() => {
                        // Can sometimes cause an incorrect rejection (all is good, ignore).
                    });
                }
            }
        }
    }
    setupGlobalListeners() {
        this.globalListener = this.checkAndRecoverPlayback.bind(this);
        document.addEventListener("visibilitychange", this.globalListener);
    }
    removeGlobalListeners() {
        document.removeEventListener("visibilitychange", this.globalListener);
    }
    videoProcessing() {
        if (this.destroyed) {
            return;
        }
        if (this.cameraManager.activeCamera == null ||
            this.cameraManager.activeCamera.currentResolution == null ||
            this.fatalError != null ||
            this.scanningPaused ||
            !this.scanner.isReady() ||
            this.scanner.isBusyProcessing() ||
            this.latestVideoTimeProcessed === this.videoElement.currentTime) {
            window.requestAnimationFrame(this.videoProcessing.bind(this));
            return;
        }
        if (this.latestVideoTimeProcessed == null) {
            // Show active GUI if needed, as now it's the moment the scanner is ready and used for the first time
            this.resumeScanning();
        }
        const processingStartTime = performance.now();
        this.latestVideoTimeProcessed = this.videoElement.currentTime;
        this.videoImageCanvasContext.drawImage(this.videoElement, 0, 0);
        try {
            const imageData = this.videoImageCanvasContext.getImageData(0, 0, this.videoImageCanvasContext.canvas.width, this.videoImageCanvasContext.canvas.height).data;
            this.scanner
                .processImage(imageData)
                .then(scanResult => {
                if (!this.scanningPaused) {
                    if (scanResult.barcodes.length !== 0) {
                        if (this.guiStyle === BarcodePicker.GuiStyle.LASER ||
                            this.guiStyle === BarcodePicker.GuiStyle.LASER_DEPRECATED) {
                            this.flashLaser();
                        }
                        else if (this.guiStyle === BarcodePicker.GuiStyle.VIEWFINDER ||
                            this.guiStyle === BarcodePicker.GuiStyle.VIEWFINDER_DEPRECATED) {
                            this.flashViewfinder();
                        }
                        if (this.playSoundOnScan) {
                            this.beepSound.play();
                        }
                        if (this.vibrateOnScan && this.vibrateFunction != null) {
                            this.vibrateFunction.call(navigator, 300);
                        }
                        this.eventEmitter.emit("scan", scanResult);
                    }
                }
            })
                .catch(scanError => {
                if (!this.scanningPaused) {
                    this.pauseScanning();
                    this.eventEmitter.emit("scan-error", scanError);
                }
            });
        }
        catch (error) {
            this.fatalError = error;
            console.error(error);
        }
        if (this.targetScanningFPS < 30) {
            if (this.averageProcessingTime == null) {
                this.averageProcessingTime = performance.now() - processingStartTime;
            }
            else {
                this.averageProcessingTime = this.averageProcessingTime * 0.9 + (performance.now() - processingStartTime) * 0.1;
            }
            const nextProcessingCallDelay = 1000 / this.targetScanningFPS - this.averageProcessingTime;
            if (nextProcessingCallDelay > 16) {
                // More than 60 FPS, we have time to sleep (slower than requestAnimationFrame)
                window.setTimeout(() => {
                    this.videoProcessing();
                }, nextProcessingCallDelay);
            }
            else {
                window.requestAnimationFrame(this.videoProcessing.bind(this));
            }
        }
        else {
            window.requestAnimationFrame(this.videoProcessing.bind(this));
        }
    }
}
/**
 * @hidden
 */
BarcodePicker.grandParentElementClassName = "scandit scandit-container";
/**
 * @hidden
 */
BarcodePicker.parentElementClassName = "scandit scandit-barcode-picker";
/**
 * @hidden
 */
BarcodePicker.videoElementClassName = "scandit-video";
/**
 * @hidden
 */
BarcodePicker.scanditLogoImageElementClassName = "scandit-logo";
/**
 * @hidden
 */
BarcodePicker.laserImageElementClassName = "scandit-laser";
/**
 * @hidden
 */
BarcodePicker.viewfinderElementClassName = "scandit-viewfinder";
/**
 * @hidden
 */
BarcodePicker.cameraSwitcherElementClassName = "scandit-camera-switcher";
/**
 * @hidden
 */
BarcodePicker.torchToggleElementClassName = "scandit-torch-toggle";
// istanbul ignore next
(function (BarcodePicker) {
    /**
     * GUI style to be used by a barcode picker, used to hint barcode placement in the frame.
     */
    let GuiStyle;
    (function (GuiStyle) {
        /**
         * @hidden
         * @deprecated
         */
        GuiStyle[GuiStyle["NONE_DEPRECATED"] = 0] = "NONE_DEPRECATED";
        /**
         * @hidden
         * @deprecated
         */
        GuiStyle[GuiStyle["LASER_DEPRECATED"] = 1] = "LASER_DEPRECATED";
        /**
         * @hidden
         * @deprecated
         */
        GuiStyle[GuiStyle["VIEWFINDER_DEPRECATED"] = 2] = "VIEWFINDER_DEPRECATED";
        /**
         * No GUI is shown to indicate where the barcode should be placed.
         * Be aware that the Scandit logo continues to be displayed as showing it is part of the license agreement.
         */
        GuiStyle["NONE"] = "none";
        /**
         * A laser line is shown.
         */
        GuiStyle["LASER"] = "laser";
        /**
         * A rectangular viewfinder with rounded corners is shown.
         */
        GuiStyle["VIEWFINDER"] = "viewfinder";
    })(GuiStyle = BarcodePicker.GuiStyle || (BarcodePicker.GuiStyle = {}));
    /**
     * Fit type used to control the resizing (scale) of the barcode picker to fit in its container <i>originElement</i>.
     */
    let ObjectFit;
    (function (ObjectFit) {
        /**
         * Scale to maintain aspect ratio while fitting within the <i>originElement</i>'s content box.
         * Aspect ratio is preserved, so the barcode picker will be "letterboxed" if its aspect ratio
         * does not match the aspect ratio of the box.
         */
        ObjectFit["CONTAIN"] = "contain";
        /**
         * Scale to maintain aspect ratio while filling the <i>originElement</i>'s entire content box.
         * Aspect ratio is preserved, so the barcode picker will be clipped to fit if its aspect ratio
         * does not match the aspect ratio of the box.
         */
        ObjectFit["COVER"] = "cover";
    })(ObjectFit = BarcodePicker.ObjectFit || (BarcodePicker.ObjectFit = {}));
})(BarcodePicker || (BarcodePicker = {}));
//# sourceMappingURL=barcodePicker.js.map