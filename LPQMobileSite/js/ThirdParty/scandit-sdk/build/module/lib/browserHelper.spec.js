/* tslint:disable:no-implicit-dependencies */
/**
 * BrowserHelper tests
 */
import { test } from "ava";
import { BrowserHelper, CustomError } from "scandit-sdk";
test("checkBrowserCompatibility", t => {
    window.Blob = null;
    t.deepEqual(BrowserHelper.checkBrowserCompatibility(), new CustomError({
        name: "UnsupportedBrowserError",
        message: "Media devices are not supported on this OS / Browser"
    }));
    window.navigator.mediaDevices = {
        getUserMedia: () => {
            return;
        }
    };
    t.deepEqual(BrowserHelper.checkBrowserCompatibility(), new CustomError({
        name: "UnsupportedBrowserError",
        message: "Web Worker is not supported on this OS / Browser"
    }));
    window.Worker = () => {
        return;
    };
    t.deepEqual(BrowserHelper.checkBrowserCompatibility(), new CustomError({
        name: "UnsupportedBrowserError",
        message: "WebAssembly is not supported on this OS / Browser"
    }));
    window.WebAssembly = {};
    t.deepEqual(BrowserHelper.checkBrowserCompatibility(), new CustomError({
        name: "UnsupportedBrowserError",
        message: "Blob object is not supported on this OS / Browser"
    }));
    window.Blob = () => {
        return;
    };
    t.deepEqual(BrowserHelper.checkBrowserCompatibility(), new CustomError({
        name: "UnsupportedBrowserError",
        message: "URL object is not supported on this OS / Browser"
    }));
    window.URL = {
        createObjectURL: () => {
            return;
        }
    };
    t.deepEqual(BrowserHelper.checkBrowserCompatibility(), undefined);
});
test("getDeviceId", t => {
    const currentDeviceId = BrowserHelper.getDeviceId();
    t.regex(currentDeviceId, /[0-9a-f]{40}/);
    t.deepEqual(BrowserHelper.getDeviceId(), currentDeviceId);
});
//# sourceMappingURL=browserHelper.spec.js.map