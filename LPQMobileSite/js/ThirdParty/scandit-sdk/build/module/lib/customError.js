/**
 * @hidden
 */
export class CustomError extends Error {
    constructor({ name = "", message = "" } = {}) {
        super(message);
        Object.setPrototypeOf(this, CustomError.prototype);
        this.name = name;
    }
}
//# sourceMappingURL=customError.js.map