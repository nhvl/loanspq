import "objectFitPolyfill";
import "webrtc-adapter";
import { BrowserHelper } from "./lib/browserHelper";
import { CameraAccess } from "./lib/cameraAccess";
import { CustomError } from "./lib/customError";
import { ImageSettings } from "./lib/imageSettings";
import { Scanner } from "./lib/scanner";
import "./styles/styles.scss";
export * from "./lib/barcode";
export * from "./lib/barcodePicker";
export * from "./lib/browserHelper";
export * from "./lib/camera";
export * from "./lib/cameraAccess";
export * from "./lib/cameraSettings";
export * from "./lib/customError";
export * from "./lib/imageSettings";
export * from "./lib/parser";
export * from "./lib/scanner";
export * from "./lib/scanSettings";
export * from "./lib/symbologySettings";
/**
 * @hidden
 */
export let deviceId = BrowserHelper.getDeviceId();
/**
 * @hidden
 */
export let userLicenseKey;
/**
 * @hidden
 */
export let scanditEngineLocation;
/**
 * Initialize and configure the Scandit Barcode Scanner SDK library. This function must be called as first thing
 * before using any other function of the library.
 *
 * Depending on parameters, device features and user permissions for camera access, any of the following errors
 * could be the rejected result of the returned promise:
 * - <tt>NoLicenseKeyError</tt>
 * - <tt>UnsupportedBrowserError</tt>
 * - <tt>PermissionDeniedError</tt>
 * - <tt>NotAllowedError</tt>
 * - <tt>NotFoundError</tt>
 * - <tt>AbortError</tt>
 * - <tt>NotReadableError</tt>
 * - <tt>InternalError</tt>
 *
 * If the external Scandit Engine library is not loaded now, it can later be loaded via [[loadEngineLibrary]],
 * or it will be downloaded and prepared automatically when needed by other objects/functions.
 *
 * If the cameras are not accessed now, they can later be loaded via [[CameraAccess.getCameras]],
 * or they will be accessed automatically when needed by other objects/functions.
 *
 * Please note that preloading the Scandit Engine library only downloads the library and puts it in a local cache
 * if available (then loads it and verifies the license key). When the library is then required and used by a
 * [[BarcodePicker]] instance the external Scandit Engine library needs to be parsed and executed again and thus still
 * requires some time to be ready. To make the process faster it's recommended, if possible, to instead prepare
 * in advance library and cameras via a hidden [[BarcodePicker]] instead.
 *
 * @param licenseKey The Scandit license key to be used by the library.
 * @param engineLocation <div class="tsd-signature-symbol">Default =&nbsp;"/"</div>
 * The location of the folder containing the external scandit-engine-sdk.min.js and
 * scandit-engine-sdk.wasm files (external Scandit Engine library).
 * By default they are retrieved from the root of the web application.
 * Can be a full URL to folder or an absolute folder path.
 * @param preloadEngineLibrary <div class="tsd-signature-symbol">Default =&nbsp;false</div>
 * Whether to eagerly download and prepare the external Scandit Engine library with this call.
 * @param preloadCameras <div class="tsd-signature-symbol">Default =&nbsp;false</div>
 * Whether to eagerly request access (if needed) and access available cameras with this call.
 * @returns A promise resolving when the library has loaded and the available cameras are loaded (if selected).
 */
export function configure(licenseKey, { engineLocation = "/", preloadEngineLibrary = false, preloadCameras = false } = {}) {
    const unsupportedBrowserError = BrowserHelper.checkBrowserCompatibility();
    if (unsupportedBrowserError != null) {
        return Promise.reject(unsupportedBrowserError);
    }
    if (licenseKey == null || licenseKey.trim() === "") {
        return Promise.reject(new CustomError({ name: "NoLicenseKeyError", message: "No license key provided" }));
    }
    userLicenseKey = licenseKey;
    engineLocation += engineLocation.slice(-1) === "/" ? "" : "/";
    if (/^https?:\/\//.test(engineLocation)) {
        scanditEngineLocation = `${engineLocation}`;
    }
    else {
        engineLocation = engineLocation
            .split("/")
            .filter(s => {
            return s.length > 0;
        })
            .join("/");
        if (engineLocation === "") {
            engineLocation = "/";
        }
        else {
            engineLocation = `/${engineLocation}/`;
        }
        scanditEngineLocation = `${location.origin}${engineLocation}`;
    }
    const promises = [];
    if (preloadEngineLibrary) {
        promises.push(loadEngineLibrary());
    }
    if (preloadCameras) {
        promises.push(CameraAccess.getCameras());
    }
    return Promise.all(promises)
        .then(() => {
        return;
    })
        .catch(error => {
        return Promise.reject(error);
    });
}
/**
 * Download and prepare in memory the external Scandit Engine library.
 * If this method isn't called manually the library will be automatically loaded the first
 * time it's required by another of this library's components.
 *
 * @returns A promise resolving when the external Scandit Engine library has been loaded and is ready.
 */
export function loadEngineLibrary() {
    try {
        const scanner = new Scanner();
        scanner.applyImageSettings({ width: 2, height: 2, format: ImageSettings.Format.GRAY_8U });
        return scanner.processImage(new Uint8ClampedArray(4)).then(() => {
            return;
        });
    }
    catch (error) {
        return Promise.reject(error);
    }
}
//# sourceMappingURL=index.js.map