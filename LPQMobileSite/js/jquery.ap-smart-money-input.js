﻿(function (factory) { factory(jQuery); }(function ($) {
	$.fn.extend({
		apSmartMoneyInput: function () {
			return this.each(function () {
				this.placeholder = "$0.00";
				this.pattern = "[0-9]*";

				$(this).on("focus", function () {
					moveCursorToEnd(this);
				});
				$(this).on("input", function () {
					moveCursorToEnd(this);
				})
				$(this).on("keyup", function () {
					moveCursorToEnd(this);
				});
				$(this).on("click", function () {
					moveCursorToEnd(this);
				})
				$(this).on("keydown", function (event) {

					var numbers = getNumbers($(this).val());
					if (event.keyCode === 8) { // Backspace
						numbers = numbers.substr(0, numbers.length - 1);
						event.preventDefault();
					}

					const fixedCurrency = getCurrencyFromNumbers(numbers);
					$(this).val(Common.FormatCurrency(fixedCurrency, true));
				});
				$(this).on("keypress", function (event) {
					event.preventDefault();
					var numbers = getNumbers($(this).val());
					if (
						event.key === "0" || event.key === "1" || event.key === "2" || event.key === "3" || event.key === "4" ||
						event.key === "5" || event.key === "6" || event.key === "7" || event.key === "8" || event.key === "9"
					) {
						// Numbers 0-9, regardless of location (keyCode is different between top row of keyboard, and the numpad)
						numbers = numbers + event.key.toString();
					}

					const fixedCurrency = getCurrencyFromNumbers(numbers);
					$(this).val(Common.FormatCurrency(fixedCurrency, true));
				});
				$(this).on("blur", function () {
					$(this).val(Common.FormatCurrency($(this).val(), true));
				})
			});
		},
	});
	function getNumbers(value) {
		if (value == null || typeof(value) !== "string")
			return "";

		return value.replace(/[^0-9]/g, '');
	}
	function getCurrencyFromNumbers(numbers) {
		if (numbers == null)
			return "0.00";
		if (numbers.length < 3)
			numbers = "000" + numbers;
		
		const cents = numbers.substr(numbers.length - 2, numbers.length);
		var dollars = numbers.substr(0, numbers.length - 2);
		if (dollars.length === 0) {
			dollars = "0";
		}

		return dollars + "." + cents;
	}
	function moveCursorToEnd(input) {
		const valueLen = input.value.length;
		input.setSelectionRange(valueLen, valueLen);
	}
}));