﻿var OBSERVERDB = {};
(function (factory) { factory(jQuery); }(function ($) {
	$.notify.addStyle('lpq', {
		html: "<div><div data-notify-text/></div>",
		classes: {
			base: {
				"font-weight": "normal",
				"padding": "4px 8px 4px 8px",
				"text-shadow": "0 0px 0 rgba(255, 255, 255, 0.5)",
				"background-color": "#fcf8e3",
				"border": "1px solid #fbeed5",
				"border-radius": "4px",
				"white-space": "normal"
				//"background-repeat": "no-repeat",
			},
			error: {
				"color": "#FFFFFF",
				"background-color": "#c72826",
				"border-color": "#c72826",
				"font-size": "14px"
			}
		},
		css: ".notifyjs-wrapper{width:90%!important;left:0}.notifyjs-lpq-base.notifyjs-lpq-error span{color:#ffffff!important;font-size:14px;}"
	});
	$.fn.extend({
		observer: function (options) {
			if ($(this).length === 0) return; 
			var defaultOptions = {
				validateOnBlur: true,
				postion: "bottom left",
				validators: [],
				groupType: "",
				group: "",
                placeHolder: null
			};
			var observerItem = {};
			observerItem.options = $.extend(true, defaultOptions, $.isPlainObject(options) ? options : {});
			observerItem.element = this;
			var isExisted = false;
			if (typeof OBSERVERDB[defaultOptions.group] == "undefined") {
				OBSERVERDB[defaultOptions.group] = [];
			}
			
			$.each(OBSERVERDB[defaultOptions.group], function (idx, item) {
				if ($(item.element).is(observerItem.element)) {
					$.extend(true, item, observerItem);
					isExisted = true;
				}
			});
			if (isExisted === false) {
				OBSERVERDB[defaultOptions.group].push(observerItem);
				if (observerItem.options.validateOnBlur === true) {
					if (this.is("div") && (defaultOptions.groupType === "checkboxGroup" || defaultOptions.groupType === "complexUI")) {
						$(document).on("change blur", "#" + this.attr("id"), function (evt) {
							if ($.contains(observerItem.element[0], evt.relatedTarget) == false) {
								validate(observerItem, true);
							}
						});
					////not sure why this is needed, cause issue with SSN validation burble ==> this is for complex UI elements such as Funding, Product Selection...I will try another way
                    //} else if (this.is("div")) {
					//    $(document).on("change", "#" + this.attr("id"), function (evt) {
					//        if ($.contains(observerItem.element[0], evt.relatedTarget) == false) {
					//            validate(observerItem, true);
					//        }
					//    });
				    } else {
				        $(document).on((this.is("select") ? "change blur" : "blur"), "#" + this.attr("id"), function(evt) {
				            if ($.contains(observerItem.element[0], evt.relatedTarget) == false) {
				                validate(observerItem, true);
				            }
				        });
				    }
				}
				$(document).on("focus", "#" + this.attr("id"), function () {
					$(this).prev("div.notifyjs-hidable").trigger("notify-hide");
				});
			}
		}
	});
	function validate(observerItem, partial) {
		if (partial == undefined) partial = false;
		var result = true;
		$.each(observerItem.options.validators, function (idx, func) {
			if ($.isFunction(func)) {
				var errorMsg = func.call(observerItem.element, partial);//validate partially
				if ($.trim(errorMsg) !== "") {
				        //var $notyWrapper = $(observerItem.element).prev(".notifyjs-wrapper");
				        //var $notyContainer = $(".notifyjs-container", $notyWrapper);
					//if ($notyWrapper.length == 0 || ($notyContainer.is(":hidden") || $("div[data-notify-text]", $notyContainer).data("notify-text") === errorMsg)) {

					//the timeout here is to ensure the notify can get exact position to show error callout, 
					//because there are some section such as FOM section change there height during the validation
				    setTimeout(function () {
                        if (observerItem.options.placeHolder) {
                        	$(observerItem.options.placeHolder).notify(errorMsg, { position: "bottom left", autoHide: false, style: "lpq" });
                        } else {
                        	observerItem.element.notify(errorMsg, { position: "bottom left", autoHide: false, style: "lpq" });
                        }
				    }, 300);
				        //}
					result = false;
					return false;
				} else {
				    if (observerItem.options.placeHolder) {
				    	$(observerItem.options.placeHolder).prev("div.notifyjs-hidable").trigger("notify-hide");
				    } else {
				        $(observerItem.element).prev("div.notifyjs-hidable").trigger("notify-hide");
					}
				}
			}
		});
		return result;
	}

	$.lpqValidate = function (group) {
		var result = true;
		$.each(OBSERVERDB[group], function (idx, item) {
			if (validate(item, false) == false) result = false;
		});
		return result;
	};
	$.extend($.lpqValidate, {
		hideValidation: function (element, options) {
			if (options && options.groupType == "complexUI" && options.placeHolder.length > 0) {
				options.placeHolder.prev("div.notifyjs-hidable").trigger("notify-hide");
			} else {
				$(element).prev("div.notifyjs-hidable").trigger("notify-hide");
			}
		},
		showValidation: function (element, msg) {
		    //		    console.log(msg);
		    var $notyWrapper = $(element).prev(".notifyjs-wrapper");
		    var $notyContainer = $(".notifyjs-container", $notyWrapper);
		    if ($notyWrapper.length == 0  || ($notyContainer.is(":hidden") || $("div[data-notify-text]", $notyContainer).text() !== msg)) {
		        $(element).notify(msg, { position: "bottom left", autoHide: false, style: "lpq" });
		    }
		},
		hideAllValidation: function(group) {
			$.each(OBSERVERDB[group], function (idx, item) {
				$.lpqValidate.hideValidation(item.element, item.options);
			});
		},
		removeValidationGroup: function(groupName) {
			delete OBSERVERDB[groupName];
		},
		removeValidationItem: function(group, id) {
			$.each(OBSERVERDB[group], function (idx, item) {
				if (id === item.element[0].id) {
					OBSERVERDB[group].splice(idx, 1);
					return false;
				}
			});
		},
		validate: function (group, element) {
			var result = true;
			$.each(OBSERVERDB[group], function (idx, item) {
				if (item.element[0] === $(element)[0]) {
					if (validate(item, false) == false) result = false;
				}
			});
			return result;
		}
	});
}));





















