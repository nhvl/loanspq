﻿//reference: http://benalman.com/code/projects/jquery-postmessage/examples/iframe/

// Start

$(function () {
    function getParameterByName(name) {
        name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
        var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
            results = regex.exec(location.search);
        return results == null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
    }

    var parent_url = decodeURIComponent(getParameterByName('returl'));
    var a = "ui-page-active";

    function setSize(isNewPage) {
        if ($("#divErrorDialog").hasClass(a) || $("#divMessage").hasClass(a))
            return;
        var header = $('.' + a).find(".ui-header").outerHeight();
        var content = $('.' + a).find(".ui-content").outerHeight();
        var footer = $('.' + a).find(".ui-footer").outerHeight();
        var newPage = isNewPage === true ? "true" : "false";
        $.postMessage({ if_height: header + content + footer, txt_newPage: newPage }, parent_url, parent);
    };

    $.receiveMessage(function () { setSize(false); });

    $(document).on("pageshow", function () { setSize(true); });

    $("select,input").change(function () {
        setTimeout(function () {
            setSize(false);
        }, 10);
    });

    setSize();
});

// End
