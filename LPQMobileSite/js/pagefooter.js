var RENAME_REPOSITORY = {};

function handleTabFocusOnBtnTheme() {
    $("div.btn-header-theme[tabindex], div[tabindex], span[tabindex],label.header-theme-checkbox").on("keydown", function (event) {
        if (event.which == 13) {
            $(this).trigger("click");
        }
    });
}
function updateRenameItem(data) {
	if (BUTTONLABELLIST == null) {
		BUTTONLABELLIST = {};
		$(document).on('pagebeforeshow', function () {
			// For any new items on the page that do not yet have a renameid, add it now before performRename
			indexRenameItems(undefined, true);
			performRename();
		});
	}
	
	var originalText = EXTRACT_BUTTONLABEL_REGEX.exec($.trim(htmlDecode(data.originalText)));
	if (originalText != null && originalText.length > 0) {
		originalText = $.trim(originalText[0]).toLowerCase();
		if (data.newText == null) {
		    $.each(RENAME_REPOSITORY, function (id, ele) {	    
		        var key = EXTRACT_BUTTONLABEL_REGEX.exec($.trim(htmlDecode(ele)));
				if (key != null && key.length > 0 && key[0].toLowerCase() == originalText.toLowerCase()) {
					$("[data-renameid='" + id + "']").html(htmlDecode(ele));
				}
			});
			delete BUTTONLABELLIST[originalText];
		} else {
		 
			BUTTONLABELLIST[originalText] = data.newText;
		}
		performRename();
	}
}

function performRenameForRenameableItems(container) {
	var $container = $(container);
	//use this for text inside div and button with .rename-able
	$(".rename-able", $container).each(function (idx, ele) {
		var html = $.trim(htmlDecode(RENAME_REPOSITORY[$(ele).data("renameid")]));
		var key = EXTRACT_BUTTONLABEL_REGEX.exec(html);
		if (key != null && key.length > 0) {
			var value = BUTTONLABELLIST[$.trim(key[0]).toLowerCase()];
			if (typeof value == "string") {
				if ($.trim(value) !== "") {
					$(ele).html(html.replace(EXTRACT_BUTTONLABEL_REGEX, value));
					$(ele).removeClass("hidden apm-hidden").prev("div.btn-rename").removeClass("hidden apm-hidden");
				} else {
				  $(ele).addClass("hidden apm-hidden").prev("div.btn-rename").addClass("hidden apm-hidden");
				}
			} else if ($(ele).hasClass("apm-hidden")) {
				$(ele).html(RENAME_REPOSITORY[$(ele).data("renameid")]);
				$(ele).removeClass("hidden apm-hidden").prev("div.btn-rename").removeClass("hidden apm-hidden");
			}
		}
    });

    //using hidden class to hide dropdown options, it does not work on IE browser or iphones
    //so remove options that have hidden class
    $("select", $(container)).each(function (idx, ele) {
        var options = $(ele).children();   
        if (options.hasClass('rename-able') && options.hasClass('hidden')) {          
            options.detach();            
            options.not('.hidden').appendTo($(ele)); //remove options that have hidden class          
        }
    });
}

function translateRename(text) {
	if (BUTTONLABELLIST != null) {
		var mapValue = BUTTONLABELLIST[text.toLowerCase()];
		if (typeof mapValue == "string" && $.trim(mapValue) !== "") {
			return mapValue;
		}
	}
	return text;
}

function performRename(container) {
	var $container;
	if (typeof container != "undefined") {
		$container = $(container);
	} else {
		$container = $('#' + $.mobile.pageContainer.pagecontainer('getActivePage').attr('id'));
	}
	if (BUTTONLABELLIST != null) {
		//var currentPage = '#' + $.mobile.pageContainer.pagecontainer('getActivePage').attr('id');
		//use this for button with .div-continue-button
		$("a.div-continue-button", $container).each(function(idx, ele) {
			if (ele.textContent == ele.innerHTML) {
				var value = BUTTONLABELLIST[$.trim(RENAME_REPOSITORY[$(ele).data("renameid")]).toLowerCase()];
				if (typeof value == "string") {
					if ($.trim(value) !== "") {
						ele.textContent = value;
						$(ele).removeClass("hidden apm-hidden").prev("div.btn-rename").removeClass("hidden apm-hidden");
					} else {
						$(ele).addClass("hidden apm-hidden").prev("div.btn-rename").addClass("hidden apm-hidden");
					}
				} else if ($(ele).hasClass("apm-hidden")) {
					ele.textContent = RENAME_REPOSITORY[$(ele).data("renameid")];
					$(ele).removeClass("hidden apm-hidden").prev("div.btn-rename").removeClass("hidden apm-hidden");
				}
			}
			//console.log(ele.innerText, ele.innerText == ele.innerHTML);
		});

		//use this for button with .btn-header-theme
		$("a.btn-header-theme:not(.abort-renameable), a.header_theme2:not(.abort-renameable)", $container).each(function (idx, ele) {
			if ($.trim(ele.textContent) == $.trim(ele.innerHTML)) {
				var value = BUTTONLABELLIST[$.trim(RENAME_REPOSITORY[$(ele).data("renameid")]).toLowerCase()];
				if (typeof value == "string") {
					if ($.trim(value) !== "") {
						ele.textContent = value;
						$(ele).removeClass("hidden apm-hidden").prev("div.btn-rename").removeClass("hidden apm-hidden");
					} else {
						$(ele).addClass("hidden apm-hidden").prev("div.btn-rename").addClass("hidden apm-hidden");
					}
				} else if($(ele).hasClass("apm-hidden")) {
					ele.textContent = RENAME_REPOSITORY[$(ele).data("renameid")];
					$(ele).removeClass("hidden apm-hidden").prev("div.btn-rename").removeClass("hidden apm-hidden");
				}
			}
			//console.log(ele.innerText, ele.innerText == ele.innerHTML);
		});
		//use this for button with .btn-header-theme
		$("#divDisagree a.header_theme2").each(function(idx, ele) {
			if ($.trim(ele.textContent) == $.trim(ele.innerHTML)) {
				var value = BUTTONLABELLIST[$.trim(RENAME_REPOSITORY[$(ele).data("renameid")]).toLowerCase()];
				if (typeof value == "string") {
					if ($.trim(value) !== "") {
						ele.textContent = value;
						$(ele).removeClass("hidden apm-hidden").prev("div.btn-rename").removeClass("hidden apm-hidden");
					} else {
						$(ele).addClass("hidden apm-hidden").prev("div.btn-rename").addClass("hidden apm-hidden");
					}
				} else if ($(ele).hasClass("apm-hidden")) {
					ele.textContent = RENAME_REPOSITORY[$(ele).data("renameid")];
					$(ele).removeClass("hidden apm-hidden").prev("div.btn-rename").removeClass("hidden apm-hidden");
				}
			}
		});
		//this is redundant
		//use this for button with .rename-able
		//$("a.rename-able", $container).each(function(idx, ele) {
		//	if (ele.innerText == ele.innerHTML) {
		//		var html = $.trim(ele.innerText);
		//		var key = EXTRACT_BUTTONLABEL_REGEX.exec(html);
		//		if (key != null && key.length > 0) {
		//			var value = BUTTONLABELLIST[$.trim(key[0]).toLowerCase()];
		//			if (typeof value == "string" && $.trim(value) !== "") {
		//				$(ele).innerText(html.replace(EXTRACT_BUTTONLABEL_REGEX, value));	
		//			}
		//		}
		//	}
		//	//console.log(ele.innerText, ele.innerText == ele.innerHTML);
		//});

		//use this for field label
		$("label[for]:not(.abort-renameable):not(.sr-only)", $container).each(function(idx, ele) {
			//if ($(ele).parent().hasClass("ui-field-contain") == true) {
			var html = $.trim(htmlDecode(RENAME_REPOSITORY[$(ele).data("renameid")]));
			var key = EXTRACT_BUTTONLABEL_REGEX.exec(html);
			if (key != null && key.length > 0) {
				var value = BUTTONLABELLIST[$.trim(key[0]).toLowerCase()];
				if (typeof value == "string") {
					if ($.trim(value) !== "") {
						var decodedValue = $('<div/>').html(value).text(); //message maybe encoded in the config xml so need to decode
						html = html.replace(EXTRACT_BUTTONLABEL_REGEX, decodedValue);
						$(ele).html(html);
						$(ele).removeClass("hidden apm-hidden").prev("div.btn-rename").removeClass("hidden apm-hidden");
					} else {
						$(ele).addClass("hidden apm-hidden").prev("div.btn-rename").addClass("hidden apm-hidden");
					}
					//old, doesn't work for dob & ssn fields
					//var decoded_message = $('<div/>').html(html.replace(EXTRACT_BUTTONLABEL_REGEX, value)).text(); //message maybe encoded in the config xml so need to decode
					////decode message may cause the required span removed.So, need the line below to restore it (if any)
					//$(ele).html(decoded_message.replace(/\*$/, '<span class="require-span">*</span>'));
				} else if ($(ele).hasClass("apm-hidden")) {
					$(ele).html(RENAME_REPOSITORY[$(ele).data("renameid")]);
					$(ele).removeClass("hidden apm-hidden").prev("div.btn-rename").removeClass("hidden apm-hidden");
				}
			}
			//}
		});

		performRenameForRenameableItems($container);


		//use this for text inside review section with .row-title on final page
		$($("div.row-title span.bold"), $("div.review-container")).each(function(idx, ele) {
			var html = $.trim(htmlDecode(RENAME_REPOSITORY[$(ele).data("renameid")]));
			var key = EXTRACT_BUTTONLABEL_REGEX.exec(html);
			if (key != null && key.length > 0) {
				var value = BUTTONLABELLIST[$.trim(key[0]).toLowerCase()];
				if (typeof value == "string") {
					if ($.trim(value) !== "") {
						$(ele).html(html.replace(EXTRACT_BUTTONLABEL_REGEX, value));
						$(ele).removeClass("hidden apm-hidden").prev("div.btn-rename").removeClass("hidden apm-hidden");
						$(ele).closest(".row-title").closest(".col-sm-6.col-xs-12").removeClass("hidden apm-hidden");
					} else {
						$(ele).addClass("hidden apm-hidden").prev("div.btn-rename").addClass("hidden apm-hidden");
						$(ele).closest(".row-title").closest(".col-sm-6.col-xs-12").addClass("hidden apm-hidden");
					}
				} else if ($(ele).hasClass("apm-hidden")) {
					$(ele).html(RENAME_REPOSITORY[$(ele).data("renameid")]);
					$(ele).removeClass("hidden apm-hidden").prev("div.btn-rename").removeClass("hidden apm-hidden");
					$(ele).closest(".row-title").closest(".col-sm-6.col-xs-12").removeClass("hidden apm-hidden");
				}

			}
		});
		//using this for text inside footer section 
		$("div.divLenderName, div.divFooterLogo a", $('div.divfooter')).each(function(idx, ele) {
			var html = $.trim(htmlDecode(RENAME_REPOSITORY[$(ele).data("renameid")]));
			var key = EXTRACT_BUTTONLABEL_REGEX.exec(html);
			if (key != null && key.length > 0) {
				var value = BUTTONLABELLIST[$.trim(key[0]).toLowerCase()];
				if (typeof value == "string") {
					if ($.trim(value) !== "") {
						$(ele).html(html.replace(EXTRACT_BUTTONLABEL_REGEX, value));
						$(ele).removeClass("hidden apm-hidden").prev("div.btn-rename").removeClass("hidden apm-hidden");
					} else {
						$(ele).addClass("hidden apm-hidden").prev("div.btn-rename").addClass("hidden apm-hidden");
					}
				} else if ($(ele).hasClass("apm-hidden")) {
					$(ele).html(RENAME_REPOSITORY[$(ele).data("renameid")]);
					$(ele).removeClass("hidden apm-hidden").prev("div.btn-rename").removeClass("hidden apm-hidden");
				}
			}
		});
	}
	$("div.intl-tel-input input").on("focus", function () {
        $(this).closest("div.intl-tel-input").removeClass("ui-focus").closest("div.ui-input-text").addClass("ui-focus");
    });

    $("div.intl-tel-input input").on("blur", function () {
        $(this).closest("div.intl-tel-input").closest("div.ui-input-text").removeClass("ui-focus");
    });
}
function getDataId() {
	return "e_" + Math.random().toString(36).substr(2, 16);
}
function clearHiddenCQFields(cqItem) {
    var cqName = cqItem.attr('qname');
    var $ctrl = $("[iname='" + cqName + "']");
    if ($ctrl.is(":checkbox")) {     
        $("[iname='" + cqName + "']:checked").each(function () {
            $(this).trigger('click');//unchecked checkbox
        });
    } else if ($ctrl.is("select")) {
        //reset to first index of ddl
        $ctrl[0].selectedIndex = 0;
        $ctrl.selectmenu().selectmenu("refresh");      
    } else {
       //text, passworld
        $ctrl.val(""); 
    }
}
function performAdvancedLogics() {
	if (ADVANCEDLOGICLIST != null) {
		$.each(ADVANCEDLOGICLIST, function (y, item) {
			if (item.Conditions.length == 0) return;
			var exp = item.Expression;
			exp = exp.replace(/AND/g, "&&");
			exp = exp.replace(/OR/g, "||");
			if (item.TargetType == "CQ" && $("div.Title[qname='" + item.TargetItem + "']")) {
				$.each(item.Conditions, function (idx, condition) {
					var result = resolveAdvancedLogicCondition(condition, item);
					exp = exp.replace("$exp" + (idx + 1), result);

				});
                var cqItem = $("div.Title[qname='" + item.TargetItem + "']");
                var cqElem = $("[iname = '" + item.TargetItem + "']");
				var qid = cqItem.attr("id");
				if (eval(exp)) {
                    cqItem.removeClass("hidden");
                    if (cqElem.is(":checkbox")) {
                        cqElem.closest('div[class~="chkgrp"]').removeClass("hidden");
                        cqElem.closest('div[class~="chkgrp"]').prev('div[class~="notifyjs-wrapper"]').removeClass("hidden");
                    } else {
                        $("div.Title[qname='" + item.TargetItem + "']+div").removeClass("hidden");
                    }					
					//for password field, hide confirmation field
					$("#" + qid + "_confirm").removeClass("hidden");
					$("#" + qid + "_confirm+div").removeClass("hidden");
					cqItem.closest("#pl_cq").removeClass("hidden");
					cqItem.closest("#xa_cq").removeClass("hidden");
                } else {
                    cqItem.addClass("hidden");
                    clearHiddenCQFields(cqItem);
                    if (cqElem.is(":checkbox")) {                       
                        cqElem.closest('div[class~="chkgrp"]').addClass("hidden");
                        cqElem.closest('div[class~="chkgrp"]').prev('div[class~="notifyjs-wrapper"]').addClass("hidden");
                    } else {
                        $("div.Title[qname='" + item.TargetItem + "']+div").addClass("hidden");
                    }                     		
					//for password field, hide confirmation field
					$("#" + qid + "_confirm").addClass("hidden");
                    $("#" + qid + "_confirm+div").addClass("hidden");
                   
				}
				//show #pl_cq or xa_cq if any question is visible otherwise hide it
				if ($("#pl_cq div.Title[qname]:not(.hidden)").length == 0) {
					$("#pl_cq").addClass("hidden");
				}
				if ($("#xa_cq div.Title[qname]:not(.hidden)").length == 0) {
					$("#xa_cq").addClass("hidden");
				}
			} else if (item.TargetType == "AQ" && $("div.aq-item-wrapper[qname]").filter(function (idx, el) { return $(el).attr("qname") == item.TargetItem })) {
				//for applicant question, this is a special case because we have added more than one xaApplicantQuestion UC (for example: one for main applicant and another one for co-app)
				$("div.aq-item-wrapper[qname]").filter(function (idx, el) { return $(el).attr("qname") == item.TargetItem }).each(function (i, ele) {
                    exp = item.Expression;
                    exp = exp.replace(/AND/g, "&&");
                    exp = exp.replace(/OR/g, "||");
                    var $aqItem = $(ele);
					$.each(item.Conditions, function (idx, condition) {
						var result = resolveAdvancedLogicCondition(condition, item, $aqItem);
						exp = exp.replace("$exp" + (idx + 1), result);
					});
					if (eval(exp)) {
						$aqItem.removeClass("hidden");
						$aqItem.closest("div.aq-section").removeClass("hidden");
					} else {
						$aqItem.addClass("hidden");
					}
					if ($aqItem.closest("div.aq-section").find("div.aq-item-wrapper:not(.hidden)").length == 0) {
						$aqItem.closest("div.aq-section").addClass("hidden");
					}
				});
			} else if (item.TargetType == "PP") {
				$.each(item.Conditions, function (idx, condition) {
					var result = resolveAdvancedLogicCondition(condition, item);
					exp = exp.replace("$exp" + (idx + 1), result);

				});

				var $purposeEle;
				if ($("#btnLineOfCredit").length > 0 && $("#btnLineOfCredit").hasClass("active")) {
					$purposeEle = $("[data-command='line-of-credit'][data-key].btn-header-theme").filter(function (idx, el) { return $(el).data("key") == item.TargetItem });
				} else {
					$purposeEle = $("[data-command='loan-purpose'][data-key].btn-header-theme").filter(function (idx, el) { return $(el).data("key") == item.TargetItem });
				}
				if (eval(exp)) {
					$purposeEle.show();
				} else {
					$purposeEle.hide();
					//if it's currently selected, then need to change the selection to another item
					if ($purposeEle.hasClass("active")) {
						$purposeEle.parent().find(".btn-header-theme:visible:first-child").trigger("click");
					}
				}
			}
		});
	}
}
function addNumberOfCharacters(exp, c, count) {
	for (var i = 0; i < count; i++) {
		exp = exp + c;
	}
	return exp;
}

function showHideOptionFromSelect(selectEle, optionValue, hide) {
	var $selectEle = $(selectEle);
	if (typeof $selectEle.attr("id") == "undefined") return;
	var id = $selectEle.attr("id") + "_hiddenbk";
	var $selectEleBk = $("#" + id);
	if ($selectEleBk.length == 0) {
		$selectEleBk = $("<select/>", { "id": id, "class": "hidden", "data-role": "none" });
		$selectEle.after($selectEleBk);
	}
	var $src, $dest;
	if (hide) {
		$src = $selectEle;
		$dest = $selectEleBk;
	} else {
		$src = $selectEle;
		$dest = $selectEleBk;
	}
	var $option = $("option[value='" + optionValue + "']", $src);
	if ($option.length > 0) {
		$dest.append($option);
	}
}

function resolveAdvancedLogicCondition(condition, item, targetItem) {
	if (condition.Type == "CQ") {
		var $ctrl = $("[iname]").filter(function (idx, el) { return $(el).attr("iname") == condition.Name });
		if ($ctrl.is(":checkbox")) {
			var res = false;
			$("[iname]:checked").filter(function (idx, el) { return $(el).attr("iname") == condition.Name }).each(function (idx, ele) {
				res = res || resolveOperation($(ele).attr("answer"), condition.Value, condition.Comparer);
			});
			return res;
		} else if ($ctrl.is(":radio")) {
			var $checkedCtrl = $("[iname]:checked").filter(function (idx, el) { return $(el).attr("iname") == condition.Name });
			if ($checkedCtrl.length > 0) {
				return resolveOperation($checkedCtrl.attr("answer"), condition.Value, condition.Comparer);
			}
		} else if ($ctrl.is("select")) {
			var $selectedOption = $ctrl.find("option:selected");
			if ($selectedOption.length == 0) {
				return false;
			}
			return resolveOperation($selectedOption.text(), condition.Value, condition.Comparer);
		} else {
			var curValue = $ctrl.val();
			return resolveOperation(curValue, condition.Value, condition.Comparer);
		}
	} else if (condition.Type == "AQ") {
		var $container = $(document);
		// Do not constrain to the current container of the target item, since the answer of a CQ on one page could be affecting the visibility of a CQ on another page.
		//if (targetItem !== undefined) {
		//	$container = $(targetItem).closest("div.aq-section");
		//}
		var $ctrl = $("[iname]", $container).filter(function (idx, el) { return $(el).attr("iname") == condition.Name });
		if ($ctrl.is(":checkbox")) {
			var res = false;
			$("[iname]:checked", $container).filter(function (idx, el) { return $(el).attr("iname") == condition.Name }).each(function (idx, ele) {
				res = res || resolveOperation($(ele).attr("answer"), condition.Value, condition.Comparer);
			});
			return res;
		} else if ($ctrl.is(":radio")) {
			var $checkedCtrl = $("[iname]:checked", $container).filter(function (idx, el) { return $(el).attr("iname") == condition.Name });
			if ($checkedCtrl.length > 0) {
				return resolveOperation($checkedCtrl.attr("answer"), condition.Value, condition.Comparer);
			}
		} else if ($ctrl.is("select")) {
			var $selectedOption = $ctrl.find("option:selected");
			if ($selectedOption.length == 0) {
				return false;
			}
			return resolveOperation($selectedOption.text(), condition.Value, condition.Comparer);
		} else {
			var curValue = $ctrl.val();
			return resolveOperation(curValue, condition.Value, condition.Comparer);
		}
	} else if (condition.Type == "PP") {
		var curValue = "";
		if (/^pl/i.test(item.LoanType) && $("#btnLineOfCredit").length > 0 && $("#btnLineOfCredit").hasClass("active")) {
		    curValue = $("[data-command='line-of-credit'].btn-header-theme.active").text();
		} else {
		    curValue = $("[data-command='loan-purpose'].btn-header-theme.active").attr('data-purpose-text');
		}
		return resolveOperation(curValue, condition.Value, condition.Comparer);
	} else if (condition.Type == "VT") {
		var curValue = $('#ddlVehicleType').val();
		return resolveOperation(curValue, condition.Value, condition.Comparer);
	} else if (condition.Type == "LC") {
		var curValue = "";
		if (/(PL|PL_COMBO)$/.test(item.LoanType.toUpperCase()) && $("#btnLineOfCredit").length > 0 && $("#btnLineOfCredit").hasClass("active")) {
			curValue = "Yes";
		}
		else {
			curValue = "No";
		}
		return resolveOperation(curValue, condition.Value, condition.Comparer);
	}else if (condition.Type == "LT" && item.LoanType.toLowerCase() == "cc") {
		return resolveOperation($("#ddlCreditCardType").val(), condition.Value, condition.Comparer);
	}
	return false;
}
function resolveOperation(curValue, value, op) {
	switch (op) {
		case "equal":
			return curValue == value;
		case "greaterThan":
			return parseFloat(curValue) > parseFloat(value);
		case "lessThan":
			return parseFloat(curValue) < parseFloat(value);
		case "greaterThanOrEqual":
			return parseFloat(curValue) >= parseFloat(value);
		case "lessThanOrEqual":
			return parseFloat(curValue) <= parseFloat(value);
		case "notEqual":
			return curValue != value;
		default:
			return false;
	}
}
function attachGlobalVarialble(appInfo) {
	if (typeof appInfo == "undefined" || appInfo == null) {
		appInfo = new Object();
	}
	appInfo.previewCode = $("#hdPreviewCode").val();
	appInfo.eMode = $("#hdEmode").val();
	return appInfo;
}
function indexRenameItems(container, bOnlyNewItems) {
	var $container;
	if (typeof container != "undefined") {
		$container = $(container);
	} else {
		$container = $("body");
	}
	// If bOnlyNewItems is true, this will no change the existing datarenameid for found items
	if (bOnlyNewItems == null) {
		bOnlyNewItems = false;
	}

	$("a.header_theme2:not(.abort-renameable), a.div-continue-button,a.btn-header-theme:not(.abort-renameable),.rename-able, label[for]:not(.abort-renameable):not(.sr-only), div.review-container div.row-title.section-heading span.bold", $container).each(function (idx, ele) {
		if ((bOnlyNewItems && $(ele).attr("data-renameid") == null) || !bOnlyNewItems) {
			var dataId = getDataId();
			$(ele).attr("data-renameid", dataId);
			RENAME_REPOSITORY[dataId] = htmlEncode($(ele).html());
		}
	});

	$("div.divLenderName, div.divFooterLogo a", $('div.divfooter')).each(function (idx, ele) {
		if ((bOnlyNewItems && $(ele).attr("data-renameid") == null) || !bOnlyNewItems) {
			var dataId = getDataId();
			$(ele).attr("data-renameid", dataId);
			RENAME_REPOSITORY[dataId] = $(ele).text();
		}
	});
	$("#divDisagree a.header_theme2", $container).each(function (idx, ele) {
		if ((bOnlyNewItems && $(ele).attr("data-renameid") == null) || !bOnlyNewItems) {
			var dataId = getDataId();
			$(ele).attr("data-renameid", dataId);
			RENAME_REPOSITORY[dataId] = $(ele).text();
		}
	});
}
$(function () {
	var ftThemeController = $("div[data-theme-role='footer']", "#divThemeController");
    var bgThemeController = $("div[data-theme-role='background']", "#divThemeController");
    var bgColor = bgThemeController.css('background-color');
    //if (bgThemeController.css('background-color') == ftThemeController.css('background-color')) {
    //	bgColor = changeBackgroundColor(bgColor, .6);
    //	}
    if (gl_background_data_theme == "") { //no bg_theme -->use footer background
        bgColor = ftThemeController.css('background-color');
        bgColor = changeBackgroundColor(bgColor, .6);
    }
    $('body').css("background-color", bgColor);
    handleTabFocusOnBtnTheme();
    if (RELOCATELIST != null) {
    	$.each(RELOCATELIST, function (id, value) {
    		var $source = $("#" + id);
    		var $topAnchor = $("#" + value);
    		if ($source.length > 0 && $topAnchor.length > 0) {
    			$topAnchor.after($source);
    			$source.trigger("create");
    			bindLinkClickEvent($source);
    			var pageId = $("div[data-role='page']").first().attr("id");
    			applyHeaderThemeCss('#' + pageId);
    			applyFooterThemeCss('#' + pageId, gl_button_font_color);
    			var curPageId = $.mobile.pageContainer.pagecontainer('getActivePage').attr('id');
    			$("#btnReturnToApp").attr("href", "#" + curPageId);
    		}
    	});
    }
    if (INJECTIONLIST != null) {
    	$.each(INJECTIONLIST, function (idx, item) {
    		var $topAnchor = $("#" + item.Key);
    		if ($topAnchor.length == 0) {
    			$topAnchor = $(item.key);
    		}
    		$topAnchor.after((item.Value));
    		var pageId = $("div[data-role='page']").first().attr("id");
    		applyHeaderThemeCss('#' + pageId);
    		applyFooterThemeCss('#' + pageId, gl_button_font_color);
    	});
    }

	indexRenameItems();
	
    if (BUTTONLABELLIST != null) {
		$(document).on('pagebeforeshow', function () {
			// For any new items on the page that do not yet have a renameid, add it now before performRename
			indexRenameItems(undefined, true);
	        performRename();
        });
        performRename();
    }

    if (HIDEFIELDLIST != null) {
        var fireChangeLaterQueue = [];
        $.each(HIDEFIELDLIST, function(id, value) {
            var $ele = $("#" + id);
            if ($ele.length > 0) {
                if ($ele.is(":checkbox")) {
                    $ele.prop('checked', value === "true");
                    $ele.trigger("change");
                    $ele.MakeHideField();
                } else {
                    $ele.val(value);
                    if ($ele[0].tagName === "SELECT") {
                        if (id.indexOf("ddlEmploymentStatus") != -1) {
                            fireChangeLaterQueue.push($ele);
                        } else {
                            $ele.trigger("change");
                        }
                    }	
                    $ele.MakeHideField();
                }
                $ele.closest("div[data-role='fieldcontain']").hide(); //enclose control inside div with data-role='fieldcontain' for hidgin feature to work 
                $ele.closest("div.hide-able").hide(); //enclose control inside div with class=hide-able for hiding to work
            }
        });
        //these lines of code is to fire the event of the specified element for only one time. Due to code confliction, we cannot fire these event on document ready
        $(document).on('pagebeforeshow', function() {
            while (fireChangeLaterQueue.length > 0) {
                var $ele = $(fireChangeLaterQueue.pop());
                if ($ele[0].tagName === "SELECT") {
                    $ele.trigger("change");
                }
            }
        });
    }
    //if (SHOWFIELDLIST != null) {
    //	$.each(SHOWFIELDLIST, function (id, value) {
    //		var $ele = $("#" + value);
    //		if ($ele.length > 0) {
	//			if ($ele.is("div")) {
	//				$ele.show();
	//			} else {
	//				$ele.closest("div[data-role='fieldcontain']").hide(); //enclose control inside div with data-role='fieldcontain' for showing feature to work 
	//				$ele.closest("div.show-able").hide(); //enclose control inside div with class=show-able for showing to work	
	//			}
	//		    $ele.data("show-field", true);
	//	    }
    //	});
    //}
    
    performAdvancedLogics();
    $(document).on('pagebeforeshow', function () {
    	var curPage = $.mobile.pageContainer.pagecontainer('getActivePage');
    	if ($("div[section-name='custom-questions'], div.aq-section", curPage).length > 0) {
    	    performAdvancedLogics();
    	}
    });
    $("[data-command='loan-purpose'].btn-header-theme,[data-command='line-of-credit'].btn-header-theme,#btnLineOfCredit").each(function (idx, ele) {
    	$(ele).on("click", performAdvancedLogics);
    });
    $("#ddlCreditCardType").on("change", performAdvancedLogics);
    $(document).on('pageshow', function () {
    	$("[data-command='loan-purpose'].btn-header-theme,[data-command='line-of-credit'].btn-header-theme,#btnLineOfCredit").each(function (idx, ele) {
    		$(ele).on("click", performAdvancedLogics);
    	});
    	$("#ddlCreditCardType").on("change", performAdvancedLogics);
    	if ($(".restartbtn-placeholder a[data-command]").length > 0) {
		    $(".restartbtn-placeholder a[data-command]").focus();
    	}
	});

	$("div[section-name='custom-questions'] [iname], div.aq-section [iname]").each(function (idx, ele) {
		var $ele = $(ele);
		if ($ele.is(":text")) {
			$ele.on("blur", function () {
				performAdvancedLogics();
			});
		} else {
			$ele.on("change", function () {
				performAdvancedLogics();
			});
		}
	});
   
    $("select").each(function (idx, ele) {
    	$(ele).on("change", function () {
		    setTimeout(function () {
	    		$(ele).focus();
	    	}, 100);
	    });
	});
    
});

function htmlEncode(value) {
    //create a in-memory div, set it's inner text(which jQuery automatically encodes)
    //then grab the encoded contents back out.  The div never exists on the page.
    if ((value != '' && value != undefined)) {
        return $('<div/>').text(value).html();
    }
    return value;
}

function htmlDecode(value) {
    return $('<div/>').html(value).text();
}
function analyzeData(data) {
	var result = {};
	if (typeof data == "undefined" || data == null) {
		if (sessionStorage.getItem("bcs")) {
			data = sessionStorage.getItem("bcs");
		} else {
			data = "";
		}
	} else {
		sessionStorage.setItem("bcs", data);
	}
	var arr = data.split(/\n/);
	for (var i = 0; i < arr.length; i++) {
		if (/^(DAB|DBO|DCS)/.test(arr[i])) {
			result.LastName = arr[i].replace(/^(DAB|DBO|DCS)/, "");
		}
		if (/^(DAC|DBP|DCT)/.test(arr[i])) {
			result.FirstName = arr[i].replace(/^(DAC|DBP|DCT)/, "");
		}
		if (/^(DAD|DBQ)/.test(arr[i])) {
			result.MiddleName = arr[i].replace(/^(DAD|DBQ)/, "");
		}
		if (/^(DBL|DBB)/.test(arr[i])) {
			result.DOB = arr[i].replace(/^(DBL|DBB)/, "");
			var dobMatch = result.DOB.match(/^([0-9]{2})([0-9]{2})([0-9]{4})$/);
			if (dobMatch) {
				result.DOB = RegExp.$1 + "/" + RegExp.$2 + "/" + RegExp.$3;
			}
		}
		//if (/^(DBK|DBM)/.test(arr[i])) {
		//	result.SSN = arr[i].replace(/^(DBK|DBM)/, "");
		//}
		if (/^(DBC)/.test(arr[i])) {
			result.Sex = arr[i].replace(/^(DBC)/, "");
		}
		if (/^(DAG)/.test(arr[i])) {
			result.MailingAddress1 = arr[i].replace(/^(DAG)/, "");
		}
		//if (/^(DAH)/.test(arr[i])) {
		//	result.MailingAddress2 = arr[i].replace(/^(DAH)/, "");
		//}
		if (/^(DAI)/.test(arr[i])) {
			result.MailingCity = arr[i].replace(/^(DAI)/, "");
		}
		if (/^(DAK)/.test(arr[i])) {
			result.MailingZip = arr[i].replace(/^(DAK)/, "");
		}
		if (/^(DAJ)/.test(arr[i])) {
			result.MailingState = arr[i].replace(/^(DAJ)/, "");
		}
		//if (/^(DAL)/.test(arr[i])) {
		//	result.Address1 = arr[i].replace(/^(DAL)/, "");
		//}
		//if (/^(DAM)/.test(arr[i])) {
		//	result.Address2 = arr[i].replace(/^(DAM)/, "");
		//}
		//if (/^(DAN)/.test(arr[i])) {
		//	result.City = arr[i].replace(/^(DAN)/, "");
		//}
		//if (/^(DAP)/.test(arr[i])) {
		//	result.Zip = arr[i].replace(/^(DAP)/, "");
		//}
		//if (/^(DAO)/.test(arr[i])) {
		//	result.State = arr[i].replace(/^(DAO)/, "");
		//}
		if (/^(DBA)/.test(arr[i])) {
			result.LicenseExpiredDate = arr[i].replace(/^(DBA)/, "");
			var expiredDateMatch = result.LicenseExpiredDate.match(/^([0-9]{2})([0-9]{2})([0-9]{4})$/);
			if (expiredDateMatch) {
				result.LicenseExpiredDate = RegExp.$1 + "/" + RegExp.$2 + "/" + RegExp.$3;
			}
		}
		if (/^(DBD)/.test(arr[i])) {
			result.LicenseIssuedDate = arr[i].replace(/^(DBD)/, "");
			var issuedDateMatch = result.LicenseIssuedDate.match(/^([0-9]{2})([0-9]{2})([0-9]{4})$/);
			if (issuedDateMatch) {
				result.LicenseIssuedDate = RegExp.$1 + "/" + RegExp.$2 + "/" + RegExp.$3;
			}
		}
		if (/^(DAQ)/.test(arr[i])) {
			result.LicenseNumber = arr[i].replace(/^(DAQ)/, "");
		}
	}
	return result;
}
function hideLaserScanResult(ele) {
	$(ele).closest(".divScanDocs.laser-scan").removeClass("open");
}

function fillLaserScanResult(prefix, ele) {
	var scanResult = $(ele).closest(".scan-result-wrapper").data("scan-result");
	$('#' + prefix + 'txtFName').val(scanResult.FirstName);
	$('#' + prefix + 'txtLName').val(scanResult.LastName);
	$('#' + prefix + 'txtMName').val(scanResult.MiddleName);
	$('#' + prefix + 'txtDOB').val(scanResult.DOB);
	//new format 
	$('#' + prefix + 'txtDOB1').val(scanResult.DOB.substring(0,2)); //month
	$('#' + prefix + 'txtDOB2').val(scanResult.DOB.substring(3,5)); //day
	$('#' + prefix + 'txtDOB3').val(scanResult.DOB.substring(6,10));//year
	//address
	$('#' + prefix + 'txtAddress').val(scanResult.MailingAddress1);
	$('#' + prefix + 'txtCity').val(scanResult.MailingCity);
	$('#' + prefix + 'txtZip').val(scanResult.MailingZip);
	$('#' + prefix + 'ddlState').val(scanResult.MailingState);
	if ($('#' + prefix + 'ddlState').data("mobile-selectmenu") === undefined) {
		$('#' + prefix + 'ddlState').selectmenu(); //not initialized yet, lets do it
	}
	$('#' + prefix + 'ddlState').selectmenu('refresh');
	//get hidden driver license values
	// fill HE property address
	if ($.mobile.pageContainer.pagecontainer('getActivePage').attr('id') == 'he1') {
		$('#txtPropertyAddress').val(scanResult.MailingAddress1);
		$('#txtPropertyCity').val(scanResult.MailingCity);
		$('#txtPropertyZip').val(scanResult.MailingZip);
		$('#ddlPropertyState').val(scanResult.MailingState);
		if ($('#ddlPropertyState').data("mobile-selectmenu") === undefined) {
			$('#ddlPropertyState').selectmenu(); //not initialized yet, lets do it
		}
		$('#ddlPropertyState').selectmenu('refresh');
	}
	if ($('#hdIsComboMode').val() == "Y" || $('#hdEnableIDSection ').val() == "Y") {
		//driver license
		$('#' + prefix + 'txtIDCardNumber').val(scanResult.LicenseNumber);

		$('#' + prefix + 'txtIDDateIssued').val(scanResult.LicenseIssuedDate);
		$('#' + prefix + 'txtIDDateExpire').val(scanResult.LicenseExpiredDate);
		//new format
		$('#' + prefix + 'txtIDDateIssued1').val(scanResult.LicenseIssuedDate.substring(0, 2));
		$('#' + prefix + 'txtIDDateIssued2').val(scanResult.LicenseIssuedDate.substring(3, 5));
		$('#' + prefix + 'txtIDDateIssued3').val(scanResult.LicenseIssuedDate.substring(6, 10));
		$('#' + prefix + 'txtIDDateExpire1').val(scanResult.LicenseExpiredDate.substring(0, 2));
		$('#' + prefix + 'txtIDDateExpire2').val(scanResult.LicenseExpiredDate.substring(3, 5));
		$('#' + prefix + 'txtIDDateExpire3').val(scanResult.LicenseExpiredDate.substring(6, 10));
		$('#' + prefix + 'ddlIDState').val(scanResult.MailingState);
		if ($('#' + prefix + 'ddlIDState').data("mobile-selectmenu") === undefined) {
			$('#' + prefix + 'ddlIDState').selectmenu(); //not initialized yet, lets do it
		}
		$('#' + prefix + 'ddlIDState').selectmenu('refresh');
	}
}

$(function() {
	if (laserScanEnabled) {
		ScanditSDK.configure("ATv7ZQjaE3blD9UnpBcYh/AKMNP8NvxVU02rVOt213N3cNhjbkzmfIV4Gj+LWzrY4EhENqQo24nwcXMCzAeFkzxqUDucAp6mYHzhwbU3SzRFAuqfbm+/3HEsc1YVbUpyH2eZ0cYExgr3ZbxN7v4bjrC+ZfkHL0I0RTDiPjFYrIoIdzsp+vz6llbC0t5lfksPUvXKkRCtS8V5RTjwoJyvzh9w1uzChB+L145kfeg2id45ph8xa/qwZinfEHQwNRullhEMnMGrcdssabEcqoH6Y5Vq/YtU6xTtWcEQfQ39wj9aXu2bZxFlr2TatBjOicj3bRp6nl9zxOquaKz8V6ccrNw2WvMy/yGje9bI31r8J+eN9+HlkL2+zazgrGLW7+f6AgIaw8jWZ6Aug9+fvKw/gdbWE7qWC1ubU4EbqLmJJ2Al7+P/BLV1tlTJ+/H91dDfHC++5HJhRQrm47C/26uHh8Q4ZOV1I5r4zfDXK7Fi3qG4IEDXKrmNjs/HohrVX9en6Tt3nu7kh1zU/NFDqXUPr0OuBA4S6XYYHUnsP2EDsNh/Jysdpk3YtymDbZ92b8AIVa9QZKWj+Ki6hwRMqyZhOTwukbCxH29WKyTrUHNjkoSU+rtJVcNAGqt0TsbIMDdep1IqgBjabsBfyt7i0I8tdm/dPa2NKrT4fg1hf75VbD43L5hWZIgj2PHSB++4q4Om2G+Dp4s1m2TRinhL/1sNDyMCkZUyr4BNpblg2qdEPONnUuiEXR4wIhifQj1m+E3VmHSF3YOnljkVjG4ftx/kE0vKLpXdn5hkdsVI2SyYgFSktkBGJOwn", { engineLocation: "/js/ThirdParty/scandit-sdk/build/", preloadEngineLibrary: false, preloadCameras: false });
    }
});