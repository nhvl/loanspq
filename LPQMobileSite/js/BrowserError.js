﻿// Client Side Error Handling Consideration
// A reference to this file should be made before
// any other JavaScript statements are executed in the page.
// except for jquery.js.

//Limit the rate at which message can be send to avoid hanging, 20sec
window.onerror = debounceErr(ReportError,  20000);

$(function(){
    // jQuery code, event handling callbacks here

    //IsNeedCordova(); //DI is not using this method, using webviewer
});

function ReportError(msg, url, line, col, exception) {
    if (url == null) { return false; } /* happens sometimes w/ Netscape browsers */

    if (SuppressKnownError(msg, url, line, col)) { return true; }
   
    logError(msg, url, line, col, exception, strBrowserInfo());

       // Must return true to prevent browser from reporting error
    return true;

}  // ReportError

function strBrowserInfo() {
    // Retrieve information about the user's browser
    // Place values in a sclara array string
    var browserInfo = '';
    for (var propName in navigator)
        browserInfo += propName + ':' + navigator[propName] + '#';

    return browserInfo;
}  // strBrowserInfo()

function strGetUniqueValue() {
    // For determining window name for window.open() parameter
    // Ensure uniqueness so a new window will be displayed
    // each time using Javascript 1.0 compliant methods
    var d = new Date();
    var WinNameRnd = d.getTime();  // # milliseconds since passed
    d = null;

    return WinNameRnd;
}  // strGetUniqueValue()

//determine if running inside native app
function isCordova() {

    if (!document.URL.match(/^https?:/)) { // not http or https, cordova use file protocol
        return true; // is cordova
    }
    return false; // isn't cordova
}

function IsNeedCordova() {

    if (isCordova()) { // not http or https, cordova use file protocol
        $.getScript("cordova.js", function (data, textStatus, jqxhr) {
            loginfo(dataurl); // Data returned
            loginfo(textStatusurl); // Success
            loginfo(jqxhr.statusurl); // 200
            loginfo("cordova.js Load was performed.");
        });
    }
    else {
        //loginfo("cordova.js Load was NOT performed.");
    }
}

function SuppressKnownError(msg, url, line, col) {
    // For know browser bugs we will want to suppress reporting an error
    // This function will return true for known browser bugs
    // Add logic here as needed to filter known browser/javascript issues

    if ((navigator.appVersion.indexOf('MSIE 6.') != -1) &&
        (url.indexOf('AddEditEntry.aspx') != -1) &&
     (msg.indexOf("'contentWindow.document' is null or not an object") != -1))
    { return true; }

    //TODO: not sure what this is
    //Uncaught Error: Module org.wescomresources.cordova.accountsplugin does not exist. - URL: https://app.loanspq.com/cc/***injection***sdk/cordova.js - Line: 1406 - Column: 9 - Exception: 
    //Uncaught Error: Java exception was raised during method invocation - URL: https://app.loanspq.com/cc/***injection***sdk/cordova.js - Line: 927 - Column: 44 - Exception: 
    //ReferenceError: Can't find variable: cordova - URL: https://app.loanspq.com/pl/PersonalLoan.aspx?lenderref=mcu090115 - Line: 1 - Column: 8 - Exception: 
    if ((url.indexOf("injection***sdk/cordova.js") != -1) || (msg.indexOf("cordova") != -1))
        return true;

    if ((msg.indexOf("Browser: plugins") != -1) || (msg.indexOf("cordova") != -1))
        return true;

    if ((msg.indexOf("Blocked a frame with origin") != -1) && (msg.indexOf("app.loanspq") != -1))
        return true;

    // convert to string to avoid .indexOf is not a function
    line = line + "";
    col = col + "";

    //don't know why
    if ((line.indexOf("0") != -1) && (col.indexOf("0") != -1))
        return true;

    //browser issue but still generate app
    if ((line == "0") || (col == "0") || (line == "1") || (col == "1"))
        return true;

    //accu, already fixed so this can't be remove in next release
    if (msg.indexOf("PRODUCTCODES") != -1) 
        return true;
   
    //android issue, only for a few cu: coastal, iq, but app still submit ok
    if (msg.indexOf("ORCCAndroidApp is not defined") != -1)
        return true;

    //these still allow consumer to complete the app
    if ((msg.indexOf("window.onorientationchange") != -1) || (msg.indexOf("HtmlViewer.showHTML") != -1) )
        return true;


    //TODO
    //Can't find variable: fieldset, iOS using chrome, need futher testing
    ////https://portal.loanspq.com/logviewer/search.aspx?IsSnapshot=Y&SnapshotID=820b3fa2-793a-4196-80f5-2dd128f3ebc4
    //https://bugs.chromium.org/p/chromium/issues/detail?id=570095#c9
    if ((msg.indexOf("ReferenceError: Can't find variable: fieldset") != -1))
        return true;

    return false;
} // SuppressKnownError(msg)

function loginfo(msg) {
    logError(msg, '', '', '', '', strBrowserInfo());
}

function logError(msg, url, line, col, exception, browser) {
    g_is_silent_ajax = true;
    var sLenderRef = $('#hfLenderRef').val();
    $.ajax({
        url: "/ClientErrorLogger.aspx",
        async: true,
        cache: false,
        type: "POST",
        dataType: "json",
        data: {
            lenderref: sLenderRef,
            message: msg,
            url: url,
            line: line,
            column: col,
            exception: exception,
            browser: browser
        },
        success: function (response) {
        }
    });
}


// Returns a function, that, as long as it continues to be invoked, will not
// be triggered. The function will be called after it stops being called for
// N milliseconds. If `immediate` is passed, trigger the function on the
// leading edge, instead of the trailing.
//https://davidwalsh.name/javascript-debounce-function
function debounceErr(func, wait, immediate) {
    var timeout;
    return function () {
        var context = this, args = arguments;
        var later = function () {
            timeout = null;
            if (!immediate) func.apply(context, args);
        };
        var callNow = immediate && !timeout;
        clearTimeout(timeout);
        timeout = setTimeout(later, wait);
        if (callNow) func.apply(context, args);
    };
};