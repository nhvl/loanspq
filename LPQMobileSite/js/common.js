﻿var EMPLogicPI = {};
var EMPLogicJN = {};
var EMPLogicPI_PREV = {};
var EMPLogicJN_PREV = {};
var _NOTY = null;
var Common = {
    noty: function(type, text, timeout) {
        if (_NOTY != null) {
            _NOTY.closeCleanUp();
        }

        var noTimeOut = typeof timeout === 'undefined' || timeout == null;
        _NOTY = noty({
            text: text,
            type: type, // "alert", "success", "error", "warning", "information", "confirm"
            layout: 'topCenter', //top, topLeft, topCenter, topRight, center, centerLeft, centerRight, bottom, bottomLeft, bottomCenter, bottomRight
            container: '.dasdsdasd',
            theme: 'relax',
            closeWith: ['button','click'],
            killer: false,
            timeout: noTimeOut ? false : timeout,
            animation: {
                open: 'animated fadeInDown', // flipInX
                close: 'animated fadeOutUp' // flipOutX                                   
            }
        });
        _NOTY.setTimeout(2000);
    },
    /* -- VALIDATION -- */
    ValidateText: function(text) {
        if ($.trim(text) == '') {
            return false;
        } else if (text.indexOf('$') == 0) {
            var temp = Common.GetFloatFromMoney(text);
            if (isNaN(temp)) return true;
            if (temp == 0.00) return false;
        }
        return true;
    },

    ValidateTextAllowBlankZero: function(text) {
        if ($.trim(text) == '') {
            return true;
        } else if (text.indexOf('$') == 0) {
            var temp = Common.GetFloatFromMoney(text);  
            $('#div_iframe').html(""); //this is required to avoid hangup in iOS UIwebviewer
            if (isNaN(temp)) return false;
            if (temp == 0.00) return true;
        }
        return true;
    },

    ValidateTextAllowZero: function(text) {
        if ($.trim(text) == '') {
            return false;
        } else if (text.indexOf('$') == 0) {
            var temp = Common.GetFloatFromMoney(text);
            if (isNaN(temp)) return false;
            if (temp == 0.00) return true;
        }
        return true;
    },

    ValidateTextNonZero: function (text) {
        if ($.trim(text) == '') {
            return false;
        } else  {
            var temp = Common.GetFloatFromMoney(text);
            if (isNaN(temp)) return false;
            if (temp == 0.00) return false;
        }
        return true;
    },

    ValidateDate: function(date) {
        if (!checkDate(date))
            return false;
        return true;
    },

    ValidatePhone: function(phone) {
        if ($.trim(phone) == '')
            return false;

        var regexObj = /^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$/;
        if (!phone.match(regexObj))
            return false;
        return true;
    },
    ValidateEmail: function(email) {
        var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return re.test(email);
    },

    ValidateRadioGroup: function(groupName) {
        return $("input[name='" + groupName + "']:checked").length > 0;
    },
	ValidateZipCode: function(zip, input) {
		if (zip.match(/^([0-9]{5})[-\s]?([0-9]{4})?$/)) {
			if (typeof input != "undefined" && $(input).length > 0) {
				$(input).val(RegExp.$1 + (RegExp.$2 != "" ? "-" + RegExp.$2 : ""));
			}
			return true;
		}
		return false;
	},
    /* -- END VALIDATION -- */

    /* -- FORMAT $ -- */ // credit: Cyanide_7 (leo7278@hotmail.com) | http://www7.ewebcity.com
    FormatCurrency: function (num, hasDollarSymbol) {
        if (typeof num === 'undefined') return 0;
        num = num.toString().replace(/-|\$|\,/g, '');
        if (isNaN(num))
            num = "0";
        sign = (num == (num = Math.abs(num)));
        num = Math.floor(num * 100 + 0.50000000001);
        cents = num % 100;
        num = Math.floor(num / 100).toString();
        if (cents < 10)
            cents = "0" + cents;
        for (var i = 0; i < Math.floor((num.length - (1 + i)) / 3); i++)
            num = num.substring(0, num.length - (4 * i + 3)) + ',' + num.substring(num.length - (4 * i + 3));
        if (hasDollarSymbol) {
            return (((sign) ? '' : '-') + '$' + num + '.' + cents);
        } else {
            return (((sign) ? '' : '-') + num + '.' + cents);

        }
    },
    FormatPercent: function (num, hasPercentSymbol, decimalCount) {
	    if (typeof num === 'undefined') num = 0;
	    if (typeof decimalCount === 'undefined') decimalCount = 0;
    	num = num.toString().replace(/-|\$|\,|%/g, '');
    	if (isNaN(num)) num = "0";
    	if (isNaN(decimalCount)) decimalCount = 0;
	    var factor = Math.pow(10, decimalCount);
	    num = Math.round(num * factor) / factor;
	    var decPart = (num % 1).toFixed(decimalCount).substring(2);
	    num = Math.floor(num).toString();
	    for (var i = 0; i < Math.floor((num.length - (1 + i)) / 3); i++) {
		    num = num.substring(0, num.length - (4 * i + 3)) + ',' + num.substring(num.length - (4 * i + 3));
	    }
		if (decPart != "") {
			decPart = "." + decPart;
		}
	    if (hasPercentSymbol) {
    		return num + decPart + "%";
    	} else {
	    	return num + decPart;
	    }
    },
	GetPercentFromPercentString: function(percentValue) {
		if (typeof percentValue === 'undefined' || percentValue == null) return 0;
		var num = percentValue.toString().replace('%', '').replace(/\,/g, ''); //remove '$' and all ','
		if (isNaN(num) || num == 0 || num == null) {
			return 0;
		}
		return parseFloat(num);
	},
	GetValueFromPercentString: function (percentValue) {
		return Common.GetPercentFromPercentString(percentValue) / 100;
	},
	GetFloatFromMoney: function (money) {
        if (typeof money === 'undefined' || money == null) return 0;
        var num = money.toString().replace('$', '').replace(/\,/g, ''); //remove '$' and all ','
        if (isNaN(num) || num == 0 || num == null) {
            return 0;
        }                      
        //var temp = num.split(',');
        //var result = '';
        //for (var i = 0; i < temp.length; i++) {
        //    result += temp[i];
        //}
        return parseFloat(num);
    },

    GetSSN: function(ssn) {

        return ssn.replace(new RegExp("-", "g"), '');
    },
    FormatSSN: function(ssn) {

        if (ssn != "") {
            if (isNaN(ssn)) {
                if (ssn.substring(3, 4) != "-" || ssn.substring(6, 7) != "-") {
                   return ''; 
                }
            }
            var isSSN = ssn.toString().replace(/\D/g, "").match(/(\d{3})(\d{2})(\d{4})/);
            if (isSSN) {
                return RegExp.$1 + "-" + RegExp.$2 + "-" + RegExp.$3;
            }

        }

    },
    FormatDate: function(date) {
        if (date != '') {
            if (isNaN(date)) {
                if (date.substring(2, 3) != "/" || date.substring(5, 6) != "/") {
                    return '';
                }
            }
            var isDate = date.toString().replace(/\D/g, "").match(/(\d{2})(\d{2})(\d{4})/);
            if (isDate) {
                return RegExp.$1 + "/" + RegExp.$2 + "/" + RegExp.$3;
            }
        }
    },
    FormatExpDate: function(date) {
        if (date != "") {
            if (isNaN(date)) {
                if (date.substring(2, 3) != "-" && date.substring(2, 3) != "/") {
                    return '';
                }
            }
            var isExpDate = date.toString().replace(/\D/g, "").match(/(\d{2})(\d{4})/);
            if (isExpDate) {
                return RegExp.$1 + "-" + RegExp.$2;
            }
        }
    },
    FormatCCard: function(cardNum) {
        if (cardNum != "") {
            if (isNaN(cardNum)) {
                   if (cardNum.substring(4, 5) != "-" || cardNum.substring(9, 10) != "-" || cardNum.substring(14, 15) != "-") {
                    return ''; //not ccard format
                }
            }
            var iscardNum = cardNum.toString().replace(/\D/g, "").match(/(\d{4})(\d{4})(\d{4})(\d{4})/);
            if (iscardNum) {
                return RegExp.$1 + "-" + RegExp.$2 + "-" + RegExp.$3 + "-" + RegExp.$4;
            }
        }

    },
    FormatPhone: function (phone) {
       
        if (phone != '') {
            if (isNaN(phone)) { //user need to type the same phone format
                if (phone.substring(0, 1) != "(" || phone.substring(4, 5) != ")" || (phone.length == 13 && phone.substring(8, 9) != "-") || (phone.length == 14 && phone.substring(9, 10) != "-")) {
                    return '';
                }
            }
            var isPhone = phone.toString().replace(/\D/g, "").match(/1?(\d{3})(\d{3})(\d{4})/);  
            if (isPhone) {
                return "(" + RegExp.$1 + ") " + RegExp.$2 + "-" + RegExp.$3;
            }
        }
    },
    GetRtNumber: function(Num) {
        var isNum = Num.toString().replace(/\D/g, "").match(/^\d+$/);

        if (isNum) {
            return isNum;
        }

    },
    toJSON: function (obj) {
    	return window.JSON ? JSON.stringify(obj) : $.toJSON(obj);
    },
    GetPosNumber: function(Number, length) {

        if (length == 3)
            return Number.toString().replace(/\D/g, "").match(/^\d{3}$/);

        else if (length == 4)
            return Number.toString().replace(/\D/g, "").match(/^\d{4}$/);

        else if (length == 5)
            return Number.toString().replace(/\D/g, "").match(/^\d{5}$/);
    },
    ScrollToError: function (callback) {
    	setTimeout(function () {
		    if ($("div.notifyjs-container:visible").length > 0) {
			    $('html, body').stop().animate({ scrollTop: $("div.notifyjs-container:visible").offset().top - 100 }, '500', 'swing');
		    }
		    if ($.isFunction(callback)) callback();
    	}, 400);
    },
	GetCardTypeFromNumber: function(cardNumber) {
		//if (/^4[0-9\-]+$/.test(cardNumber)) {
		//	return "Visa";
		//} else {
		//	return "MasterCard";
		//}
		if (/^3[47]/.test(cardNumber)) {
			return "Amex";
		}
		if (/^5019/.test(cardNumber)) {
			return "Dankort";
		}
		if (/^(384100|384140|384160|606282|637095|637568|60(?!11))/.test(cardNumber)) {
			return "HiperCard";
		}
		if (/^(36|38|30[0-5])/.test(cardNumber)) {
			return "DinersClub";
		}
		if (/^(6011|65|64[4-9]|622)/.test(cardNumber)) {
			return "Discover";
		}
		if (/^35/.test(cardNumber)) {
			return "JCB";
		}
		if (/^(6706|6771|6709)/.test(cardNumber)) {
			return "Laser";
		}
		if (/^(5018|5020|5038|6304|6703|6708|6759|676[1-3])/.test(cardNumber)) {
			return "Maestro";
		}
		if (/^(5[1-5]|677189)|^(222[1-9]|2[3-6]\d{2}|27[0-1]\d|2720)/.test(cardNumber)) {
			return "MasterCard";
		}
		if (/^62/.test(cardNumber)) {
			return "UnionPay";
		}
		if (/^4(026|17500|405|508|844|91[37])/.test(cardNumber)) {
			return "VisaElectron";
		}
		if (/^(4011(78|79)|43(1274|8935)|45(1416|7393|763(1|2))|50(4175|6699|67[0-7][0-9]|9000)|627780|63(6297|6368)|650(03([^4])|04([0-9])|05(0|1)|4(0[5-9]|3[0-9]|8[5-9]|9[0-9])|5([0-2][0-9]|3[0-8])|9([2-6][0-9]|7[0-8])|541|700|720|901)|651652|655000|655021)/.test(cardNumber)) {
			return "Elo";
		}
		if (/^4/.test(cardNumber)) {
			return "Visa";
		}
		return "Unknown";
	},
	maskCardNumber: function(cardNumber) {
		return cardNumber.replace(/-/g, "").replace(/\d(?=\d{4})/g, "*");
	},
	GetParameterByName: function(paramName) {
		//name = name.replace(/[\[]/, "\\\[").replace(/[\]]/, "\\\]");
		//var regexS = "[\\?&]" + name + "=([^&#]*)";
		//var regex = new RegExp(regexS);
		//var results = regex.exec(window.location.search);
		//if (results == null)
		//    return "";
		//else
		//    return decodeURIComponent(results[1].replace(/\+/g, " "));
		if (typeof qDATA == "object" && qDATA != null && typeof qDATA[paramName] !== "undefined") {
			return qDATA[paramName];
		}
		return "";
	},
	IsNumber: function(n) {
		return !isNaN(parseFloat(n)) && isFinite(n);
	},
	validatePassword: function (password, userName) {
	    if (password.length < 6) return "Password must have at least 6 characters.";
	    if (/[0-9]{1,}/.test(password) == false) return "Password must contain at least 1 digit.";
	    if (/[a-zA-Z]{1,}/.test(password) == false) return "Password must contain at least 1 letter.";
	    if (/[\*&'"<>`]/.test(password) == true) return "Password should not contain special characters * & ' \" < >";
	    if (/[0-9]{1,}/.test(password) == false) return "Password must have at least 8 characters.";
	    if (typeof userName == "string" && password.toLowerCase().indexOf(userName.toLowerCase()) != -1) return "Password should not contain user name.";
	    return "";
	},
	buildReviewBlock: function(title, result) {
		return $("<div/>", { "class": "col-sm-6 col-xs-12" }).append(
			$("<div/>", { "class": "row" }).append(
				$("<div/>", { "class": "col-xs-6 text-right row-title" }).append(
					$("<span>", { "class": "bold" }).text(title)
				),
				$("<div/>", { "class": "col-xs-6 text-left row-data" }).append(
					$("<span>").text(result)
				)
			)
		);
	},
	concatenateFullName: function (firstName, middleName, lastName) {
		firstName = $.trim(firstName || "");
		middleName = $.trim(middleName || "");
		lastName = $.trim(lastName || "");
		var fullName = firstName + (middleName != "" ? " " + middleName + " " : " ") + lastName;
		return $.trim(fullName);
	},
	concatenateAddress: function(address, address2, city, zip, state, country) {
		address = $.trim(address || "");
		address2 = $.trim(address2 || "");
		city = $.trim(city || "");
		zip = $.trim(zip || "");
		state = $.trim(state || "");
		country = $.trim(country || "");
		var isUsAddress = (country == "USA");
		var result = address;
		result += (isUsAddress || address2.length == 0 ? "" : ", " + address2);
		result += (city.length == 0 ? "" : ", " + city);
		result += (isUsAddress ? ", " + state : "");
		result += (zip.length == 0 ? "" : " " + zip);
		result += (isUsAddress ? "" : " " + country);
		if ($.trim(result.replace(",", "")) == "") return "";
		return $.trim(result);
    },
    IsValidDate: function (date) {
        if (!Common.ValidateText(date)) return false;
        var sDate = date.replace(/-/g, "/") ; //usind date format: MM/DD/YYYY
        return moment(sDate, "MM/DD/YYYY", true).isValid();
    },

    //toogle Show/Hide SSN
    ToggleSSNText: function(toggleButton, prefix){
        prefix = prefix || "";
        var $btn = $(toggleButton);
        if ($btn.attr("contenteditable") == "true") return false;
        if ($btn.attr('is_show') == '0') {
            $btn.parent().addClass("hidden");
            $btn.closest("div.ui-label-ssn").find("label[is_show='1']").parent().removeClass("hidden");
            if (isMobile.any() && !isMobile.Windows()) { //moz-text-security does not works on window devices
                var ssn1 = "";
                var ssn2 = "";
                var matchs = /^(\w*)-(\w*)-(\w*)$/i.exec($("#" + prefix + "txtSSN").val());
                if (typeof matchs != "undefined" && matchs != null) {
                    ssn1 = matchs[1];
                    ssn2 = matchs[2];
                }
                $("#" + prefix + "txtSSN1").removeClass("mask-password").val("").val(ssn1); //keep .val("") to prevent bug on mobile devices
                $("#" + prefix + "txtSSN2").removeClass("mask-password").val("").val(ssn2);
            } else {
                $("#" + prefix + "txtSSN1").attr('type', 'text');
                $("#" + prefix + "txtSSN2").attr('type', 'text');
            }
        } else {
            $btn.parent().addClass("hidden");
            $btn.closest("div.ui-label-ssn").find("label[is_show='0']").parent().removeClass("hidden");
            if (isMobile.any() && !isMobile.Windows()) { //moz-text-security does not works on window devices
                $("#" + prefix + "txtSSN1").addClass("mask-password");
                $("#" + prefix + "txtSSN2").addClass("mask-password");
            } else {
                $("#" + prefix + "txtSSN1").attr('type', 'password');
                $("#" + prefix + "txtSSN2").attr('type', 'password');
            }
        }
        return true;
    }
};
var isMobile = {
    Android: function() {
        return navigator.userAgent.match(/Android/i);
    },
    BlackBerry: function() {
        return navigator.userAgent.match(/BlackBerry/i);
    },
    iOS: function() {
        return navigator.userAgent.match(/iPhone|iPad|iPod/i);
    },
    iPad: function () {
        return navigator.userAgent.match(/iPad/i);
    },
    Opera: function() {
        return navigator.userAgent.match(/Opera Mini/i);
    },
    Windows: function() {
        return navigator.userAgent.match(/IEMobile/i);
    },
    any: function() {
        return (isMobile.Android() || isMobile.BlackBerry() || isMobile.iOS() || isMobile.Opera() || isMobile.Windows());
    }
   
};




/*check the maxlength of the input*/
function CheckMaxLength(input) {
    var maxLength = input.maxLength;
    var length = input.value.length;
    
    if (length > maxLength) {
        input.value = input.value.slice(0, maxLength);
    }
}

// Original JavaScript code by Chirp Internet: www.chirp.com.au
function checkDate(date) {
    var minYear = 1902;
    // The original code limits the maxYear to the current year.
    // This will cause future dates like expiration dates to fail.
    // I pushed the limit 20 years into the future.
    var maxYear = (new Date()).getFullYear() + 100; //add 100 instead 20
    date = date.toString(); 
    if ($.trim(date) == '') return false;
    //make sure date has format mm/dd/yyyy
    if (date.length == 8 && date.indexOf('/') == -1) {
        var getMonth = date.substring(0, 2);
        var getDay = date.substring(2, 4);
        var getYear = date.substring(4, 8);
        //add forward slash to the date
        date = getMonth + "/" + getDay + "/" + getYear;
    }
   
    // regular expression to match required date format
    re = /^(\d{1,2})\/(\d{1,2})\/(\d{4})$/;

    if (date != '') {
        if (regs = date.match(re)) {
            if (regs[2] < 1 || regs[2] > 31) {
                return false;
            } else if (regs[1] < 1 || regs[1] > 12) {
                return false;
            } else if (regs[3] < minYear || regs[3] > maxYear) {
                return false;
            }
        } else {
            return false;
        }
    }
    return true;
}
function getEnlistmentDate(durationYear, durationMonth) {
    var currentDate = new Date();
    var currentMonth = currentDate.getMonth() + 1;
    var currentYear = currentDate.getFullYear();
    var en_Day = currentDate.getDate();
    var en_Month, en_Year;
    var en_Date = "";
    //check duration month and convert to number
    if (durationMonth != " ") {
        durationMonth = Number(durationMonth); //convert to number
    } else {
        durationMonth = 0; //not selected assign to 0
    }
    //check duration year and convert to number
    if (durationYear != " ") {
        durationYear = Number(durationYear);
        
    } else {
        durationYear = 0;
    }
    //case only select month
    if (durationMonth > 0 && durationYear == 0) {
        if (currentMonth > durationMonth) {
            en_Month = currentMonth - durationMonth;
            en_Year = currentYear;
        } else {
            en_Month = currentMonth + 12 - durationMonth;
            en_Year = currentYear - 1;
        }
        en_Date = en_Month + "/" + en_Day + "/" + en_Year;
    } else if (durationMonth == 0 && durationYear > 0)//case only select year
    {
        en_Year = currentYear - durationYear;
        en_Date = currentMonth + "/" + en_Day + "/" + en_Year;
    }else if (durationMonth > 0 && durationYear > 0)//case select month and year
    {
        if (currentMonth > durationMonth) {
            en_Month = currentMonth - durationMonth;
            en_Year = currentYear - durationYear;
        } else {
            en_Month = currentMonth + 12 - durationMonth;
            en_Year = currentYear - 1 - durationYear;
        }
        en_Date = en_Month + "/" + en_Day + "/" + en_Year;
    }//else just return empty string
    return en_Date;
}
function formatPhoneNumber(element) {
    var phone = $(element);
    var field = $(element).val();

    var regexObj = /^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$/;

    if (regexObj.test(field)) {
        phone.val(field.replace(regexObj, "($1) $2-$3"));
        var num = GetRealPhoneNumber(field);
        phone.attr('number', num);
      
    } 
}

function UpdateCSRFNumber(element) {
    element = $(element);
    var field = $(element).val();
    var value = field.substring(32);
    value = (value * 3) + 733;
    var rolledValue = field.substring(0,32) + value;
    element.val(rolledValue);
}

function htmlEncode(value) {
    //create a in-memory div, set it's inner text(which jQuery automatically encodes)
    //then grab the encoded contents back out.  The div never exists on the page.
    if ((value != '' && value != undefined)) {
        return $('<div/>').text(value).html();
    }
    return value;
}

function htmlDecode(value) {
    return $('<div/>').html(value).text();
}

function GetRealPhoneNumber(phone) {
    if (!phone) return '';
    //return phone.replace('(', '').replace(')', '').replace('-', '').replace('-', '');
    return phone.replace(/\s/g, '').replace(/\D/g, ''); 
   }

function textMessage(firstName) {
    var txtMessage = 'Dear ' + firstName.capitalize() + ',<br\> <br\> You have indicated that you do not want us to pull your credit report for an automatic decision. One of our Loan Officers will be contacting you shortly to discuss the next step. Thank you for visiting our website.';
    return txtMessage;
}
//check the PO Box address
function isPOBox(POBox) {
        var pattern = /\b(?:p\.?\s*o\.?|post\s+office)\s+box\b/i;
        if (!POBox.match(pattern))
            return false;
        return true;
    }
var bHasCoApp = false;

function SwitchHasCoAppButton(sender, refreshSlider) {
    if (sender.options[0].selected) {
        $('#href_save').hide();
        //$('#divCoApplicantJoint').hide();
        $('#mainApplicantSaveButton').show();
        $('#divAgreement').show();
     //   $('#divAgreementFinal').hide();
        if (refreshSlider) {
            $('#ddlCoApplicantJoint').val('N').slider('refresh');
            $("#href_save span[class='ui-btn-text']").html('');
           }
           window.bHasCoApp = false;   
    }
    else {
		// Co-app option was selected
        $("#href_save span[class='ui-btn-text']").html('Next');
        $('#href_save').show();
        $('#mainApplicantSaveButton').hide();
        //$('#divCoApplicantJoint').show();
        $('#DivCoApplicantSaveButton').show();
        window.bHasCoApp = true;
        
        $('#divAgreement').hide();
      //  $('#divAgreementFinal').show();
        if (refreshSlider) {
           $('#ddlCoApplicantJoint').val('Y').slider('refresh');
        }
    }
}

//OBSOLETED
//function SwitchHasCoAppButtonNewStyle(sender) {
//    if ($(sender).hasClass('active')) {
//        $('#href_save').hide();
//        //$('#divCoApplicantJoint').hide();
//        $('#mainApplicantSaveButton').show();
//        $('#divAgreement').show();
//        //   $('#divAgreementFinal').hide();
//        window.bHasCoApp = false;

//        $(sender).removeClass('active');
//    }
//    else {
//        // Co-app option was selected
//        $("#href_save span[class='ui-btn-text']").html('Next');
//        $('#href_save').show();
//        $('#mainApplicantSaveButton').hide();
//        //$('#divCoApplicantJoint').show();
//        $('#DivCoApplicantSaveButton').show();
//        window.bHasCoApp = true;

//        $('#divAgreement').hide();
//        //  $('#divAgreementFinal').show();
//        $(sender).addClass('active');
//    }
//    $(sender).removeClass('btnHover');
//}

function creditCardFormat() {
    //credit card format
    $('input.inCCardNumber').blur(function () {
        var element = $(this);
        element.attr('maxlength', '19');
        element.val(Common.FormatCCard(element.val()));
    });
    $('input.inCCardNumber').keyup(function () {
        var element = $(this);
        var cCardNumber = element.val();
        if (!isNaN(cCardNumber)) {
            element.attr('maxlength', '16');
        } else {
            element.attr('maxlength', '19');
        }
    });
    $('input.inCCardNumber').keydown(function (event) {
        var key = event.keyCode;
        if (key == 32) {//disable spacing bar for phone
            event.preventDefault();
        }
    });
}
function getParaByName(name) {
    //name = name.replace(/[\[]/, "\\\[").replace(/[\]]/, "\\\]");
    //var regexS = "[\\?&]" + name + "=([^&#]*)";
    //var regex = new RegExp(regexS);
    //var results = regex.exec(window.location.search);
    //if (results == null)
    //    return "";
    //else
	//    return decodeURIComponent(results[1].replace(/\+/g, " "));

	if (qDATA != null && typeof qDATA[name] !== "undefined") {
		return qDATA[name];
	}
	return "";
}
function handleDivContent(currentPage) {
   $(currentPage).each(function () {
        var element = $(this);
        var screenContentHeight = $(window).height()-82;
        if (element.attr('id') != "divErrorDialog" && element.attr('id') != "divConfirmDialog") {
            var contentHeight = element.find('div[data-role="content"]').height();
            if (contentHeight < screenContentHeight) {
                element.find('div[data-role="content"]').height(screenContentHeight);
            } 
            return false;
        }
    });

}


function changeBackgroundColor(bgColor, alpha) {
    var strAlpha = ',' + alpha + ')';
    if (bgColor != undefined) {
        return bgColor.replace('rgb', 'rgba').replace(')', strAlpha);
    } else {
        return "";
    }
}
//obsolete
//function displayConfirmMessage(redirectURL, isCoApp, onClickReturnToMainPage) {
//    //display confirm message when using click disagree button
//    var isOk = false;
//    var strMessage = "Are you sure you wish to decline? If you decline, we may not be able to process your application.";
//    strMessage += " If you are sure you do not want to continue, click on the OK button.";
//    if (confirm(strMessage) == true) {
//        isOk = true;
//    } else {
//        isOk = false;
//    }
//    if (isOk) { //go to the desicsion page if user want to exit
//        if (redirectURL == "") { //need to save loan infor 
//            if (typeof onClickReturnToMainPage == 'function') {
//                onClickReturnToMainPage(isCoApp);
//            }
//        }
//        location.hash = '#decisionPage';
//    }
//}
// global check submit button is clicked
var g_submit_button_clicked = false;
// customer checks disagree
var disagree_check = false;
//check the submitQuestion button
var isSubmitQuestion = false;
var isCoSubmitQuestion = false;
// check submitAnswer button
var isSubmitAnswer = false;
var isCoSubmitAnswer = false;
var isDownloadingFile = false;
var g_is_foreign_contact = false;
var g_is_silent_ajax = false;
$(document).ajaxStart(function () { 
    //prevent showing loading message when doing silent ajax postback such as client loginfo function in BrowserError.js.
    if (g_is_silent_ajax) {
        g_is_silent_ajax = false;
        return;
    }
    //if (isDownloadingFile) {
    //    $.mobile.loading("show", {
    //        theme: "a",
    //        text: "Loading ... please wait",
    //        textonly: true,
    //        textVisible: true
    //    });
    //    isDownloadingFile = false;
    //    return;
    //}

    //if (disagree_check != true) {
    //    $.mobile.loading("show", {
    //        theme: "a",
    //        text: "This may take from a few seconds to a few minutes.  Please do not hit the back button or close the browser.",
    //        textonly: true,
    //        textVisible: true
    //	});
    //}
    
    $("#divAjaxLoading").addClass("show-loading");
    g_submit_button_clicked = true;
}).ajaxStop(function () {
    /* --- no longer use $("input[name~=rdAgreement]") in new ui format
     $("input[name~=rdAgreement]").checkboxradio();  //not available for certain browser TODO:
     $("input[name~=rdAgreement]").removeAttr('checked').checkboxradio('refresh');
     --- */
    //$.mobile.loading('hide');
    g_submit_button_clicked = false;   
    $("#divAjaxLoading").removeClass("show-loading");
});

//usage: "hello world".capitalize();  =>  "Hello world"
String.prototype.capitalize = function() {
	return this.charAt(0).toUpperCase() + this.slice(1);
}

// document.ready for all app
$(function () {

    $('.numeric').numeric();
    $.watermark.options = {
        className: 'watermask',
        useNative: false,
        hideBeforeUnload: false
    };

    //$('.inmonth').watermark('months');
    //$('.inyear').watermark('years');
    $('input.btn-redirect').click(function() {
         window.location.href = $(this).attr('url');
    });

    //MV2
    $('a.btn-redirect').click(function () {
        window.location.href = $(this).attr('href');
    });
    /*old version --> no need it
    $('span.chkDisclosure').click(function() {
        var chk = $(this);
        if (chk.hasClass('ui-icon-checkbox-off')) {
            chk.removeClass('ui-icon-checkbox-off').addClass('ui-icon-checkbox-on').parent().parent().removeClass('ui-checkbox-off').addClass('ui-checkbox-on');
        } else {
            chk.removeClass('ui-icon-checkbox-on').addClass('ui-icon-checkbox-off').parent().parent().removeClass('ui-checkbox-on').addClass('ui-checkbox-off');
        }
    });*/
    
    if (!isMobile.Android()) {

        //$('.inssn').inputmask('999-99-9999');
    	//$('.indate').inputmask('99/99/9999');
    	$('.inphone').inputmask('(999) 999-9999');//don't know why to use inputmask here (instead of mask()). It's the cause of keeping unmatched format data in textbox when focus out instead of clear it out
    } else {
        //ssn format
        $('input.inssn').blur(function () {
            var element = $(this);
            element.attr('maxlength', '11'); // xxx-xx-xx -> maxlength =11
            element.val(Common.FormatSSN(element.val()));
        });
        $('input.inssn').keyup(function () {
            var element=$(this);
            var ssn = element.val();
            if (!isNaN(ssn)) {
                element.attr('maxlength', '9'); 
            } else {
                element.attr('maxlength', '11');
            }
        });
        $('input.inssn').keydown(function (event) {
            var key = event.keyCode;
            if (key == 32) {//disable spacing bar for ssn field
                event.preventDefault();
            }
        });
        //date format
        $('input.indate').blur(function () {
            var element = $(this);
            element.attr('maxlength', '10'); // mm/dd/yyyy ->maxlength =10
            element.val(Common.FormatDate(element.val()));
        });
        $('input.indate').keyup(function () {
            var element = $(this);
            var date = element.val();
            if (!isNaN(date)) {
                element.attr('maxlength', '8');
            } else {
                element.attr('maxlength', '10');
            }
        });
        $('input.indate').keydown(function (event) {
            var key = event.keyCode;
            if (key == 32) {//disable spacing bar for date
                event.preventDefault();
            }
        });
        // phone format
        $('input.inphone').blur(function () {
            var element = $(this);
            element.attr('maxlength', '14'); // (xxx) xxx-xxxx ->maxlength =14
            element.val(Common.FormatPhone(element.val()));
        });
        $('input.inphone').keyup(function () {
            var element = $(this);
            var phoneNumber = element.val();
                if (!isNaN(phoneNumber)) {
                    element.attr('maxlength', '10');
                } else {
                    if (phoneNumber.indexOf(" ") == -1) {
                        element.attr('maxlength', '13');
                    } else {
                        element.attr('maxlength', '14');
                    }
                }     
        });
        $('input.inphone').keydown(function (event) {
            var key = event.keyCode;
            if (key == 32) {//disable spacing bar for phone
                event.preventDefault();
            }
        });
        
	}

	$('input.apSmartMoney').apSmartMoneyInput();

    $('input.money').blur(function() {
        $(this).val(Common.FormatCurrency($(this).val(), true));
    });

    $('input.money').click(function () {
        if (isMobile.any()) {
            this.focus();
            this.setSelectionRange(0, 9999);
       } else {
           $(this).select();
       }
    });

    $('input.inmonth').blur(function() {
        $(this).val(Common.GetRtNumber($(this).val(), 3));
    });

    $('input.decimal').blur(function() {
        $(this).val(Common.FormatCurrency($(this).val(), false));
    });

    $('#divCoApplicantJoint').hide();
    $('#ddlHasCoApplicant').on('change', function () {
        var element = $(this);
        var hasCoApp = element.find('option:selected').val();
        if (hasCoApp == "Y") {
            $('#co_divRedirectURL').show();
            $('#divRedirectURL').hide();
        } else {
            $('#co_divRedirectURL').hide();
            $('#divRedirectURL').show();
        }
    });

    appendPDFViewer();
  
    //onchange event of the input zip code field is not working in IE when it has numeric class
    if (isIEBrowser()) {
        var inputElement = $('input[type="tel"]');
        inputElement.each(function () {
            var elementID = $(this).attr('id');
            if (elementID != undefined && elementID.toUpperCase().indexOf('ZIP') > -1) {
             $('#' + elementID).removeNumeric().addClass('inzipcode');
            }
        });
    }
    $(".number-only").on("keydown", function (e) {
    	var key = e.keyCode;
    	//arrows and special characters such as backspace=8, delete =46, tab=9, enter=13
    	if (key == 46 || key == 8 || key == 9 || key == 13 || key == 7 || (key >= 35 && key <= 39)) {
    		return;
    	} else {
    		if (key < 48 || (key > 57 && key < 96) || key > 105) {
    			e.preventDefault();
    		}
    	}
    }).on("blur", function() {
    	var $self = $(this);
		$self.val(parseInt($self.val())); //convert 000012 -> 12
	});
    $('.inzipcode').keydown(function (e) {
        if(!e.target.hasAttribute("pattern")) {
            return;
        }
        var key = e.keyCode;
        //arrows and special characters such as backspace=8, delete =46, tab=9, enter=13
        if (key == 46 || key == 8 || key == 9 || key == 13 || key == 7 || (key >= 35 && key <= 39)) {
            return;
        } else {      
           if (key < 48 || (key > 57 && key<96)||key>105){       
                e.preventDefault();
            }
        }
    });

       //handle mouse hover btn header theme
        $('.btn-header-theme').on("mouseenter click", function (ev) {
            var element = $(this);
            if (ev.type != 'click') {
                element.addClass('btnHover');
                element.removeClass('btnActive_on');
                element.removeClass('btnActive_off');
            } else {
                element.removeClass('btnHover');
            }
        }); 
        $('.btn-header-theme').on("mouseleave", function () {
            handledBtnHeaderTheme($(this));
        });
        // handle selected funding type button
        $('#divFundingTypeList').on('click', '.btn-header-theme', function () {
            var fundingTypeElem = $('#divFundingTypeList .btn-header-theme');
            fundingTypeElem.each(function () {
                var currElem = $(this);
                if (!currElem.hasClass('active')) {
                    currElem.removeClass('btnActive_on');
                }
            });
        });
        $('#showComments').on('click', function (e) {
	        var $self = $(this);
	        var selectedValue = $self.attr('status');
        	var commentElem = $('#comments');
        	if (selectedValue == "N") {
        		$self.html('Remove comments');
        		$self.addClass("minus-circle-before").removeClass("plus-circle-before");
        		$self.attr('status', 'Y');
        		commentElem.show();
        	} else {
        		$self.html('Have comments');
        		$self.addClass("plus-circle-before").removeClass("minus-circle-before");
        		$self.attr('status', 'N');
        		commentElem.hide();
        	}
        	if (BUTTONLABELLIST != null) {
        		var value = BUTTONLABELLIST[$.trim($self.html()).toLowerCase()];
        		if (typeof value == "string" && $.trim(value) !== "") {
        			$self.html(value);
        		}
        	}
            e.preventDefault();
        });
});
function getComments() {
    var comments = '';
    if ($('#showComments').attr('status') == 'Y') {
        comments=htmlEncode( $('#comments').val());
    }
    return comments;
}

// Returns a function, that, as long as it continues to be invoked, will not
// be triggered. The function will be called after it stops being called for
// N milliseconds. If `immediate` is passed, trigger the function on the
// leading edge, instead of the trailing.
//https://davidwalsh.name/javascript-debounce-function
function debounce(func, wait, immediate) {
    var timeout;
    return function () {
        var context = this, args = arguments;
        var later = function () {
            timeout = null;
            if (!immediate) func.apply(context, args);
        };
        var callNow = immediate && !timeout;
        clearTimeout(timeout);
        timeout = setTimeout(later, wait);
        if (callNow) func.apply(context, args);
    };
};

var loadJS = function (url, callback) {
    var head = document.getElementsByTagName('head')[0];
    var scriptTag = document.createElement('script');
    scriptTag.src = url;
    scriptTag.type = 'text/javascript';

    scriptTag.onload = callback;

    head.appendChild(scriptTag);
};

var currentPage = "";

//For PDF, download and display in frame; for other(html,aspx), just load content in a frame
function gotoPDFViewer(obj, fileUrl, lenderref) {
    var isPdfLink = false;   

    //only pdf file need to be dowloaded to local first before it can be view.
    if (fileUrl.indexOf('.pdf', fileUrl.length - 4) !== -1 || (fileUrl.indexOf('DocViewer.aspx') > 0)) {
        isDownloadingFile = true;
        isPdfLink = true;

        $.ajax({
            url: server_root + "/PdfViewer/Web/Downloader.aspx",
            async: true,
            cache: false,
            type: "POST",
            dataType: "html",
            data: {
                file: fileUrl,
                lenderref: lenderref
            },
            success: function(responseText) {
                if (responseText.indexOf("Error") < 0) {
                    var port = (location.port ? ':' + location.port : '');
                    fileUrl = responseText;
                    fileUrl = location.protocol + "//" + window.location.hostname + port + "/pdfviewer/web/viewer.html?file=" + fileUrl;
                    //fileUrl = "http://lpqmobile.localhost/testPDF/web/viewer.aspx?file=" + fileUrl;
                    //fileUrl = "http://mozilla.github.io/pdf.js/web/viewer.html?file=" + fileUrl; // TODO: For test only, change to hosted pdfviewer later
                    //fileUrl = "http://pdfviewer.local.com/viewer.html?file=" + fileUrl;
                }
                openFramedViewer(obj, fileUrl, lenderref, isPdfLink);
            }
        });
    } else {
        // Fix Blocked loading mixed active content
        fileUrl = fileUrl.replace("https:", "").replace("http:", "");
        openFramedViewer(obj, fileUrl, lenderref, isPdfLink);
    }

} //end gotoPDFViewer

function openFramedViewer(obj, fileUrl, lenderref, isPdfLink) {
    var height = window.innerHeight ||
                 document.documentElement.clientHeight ||
                 document.body.clientHeight;

    var footerHeight = $('.divfooter').height();

    height = height - footerHeight - 64; // height of one close button

    
    var sandboxOption = isPdfLink ? "" : "sandbox=\"allow-forms allow-scripts allow-pointer-lock allow-same-origin\"";

    
    $('#div_iframe').html("<iframe onload='iframeOnloaded()' src='" + fileUrl + "' scrolling='auto' frameborder='0' width='100%' height='" + height + "px' " + sandboxOption + "></iframe>");

	//$.mobile.changePage($("#popupPDFViewer"), {});
	//goToNextPage("#popupPDFViewer");

    $.mobile.changePage("#popupPDFViewer", {
    	transition: "fade",
    	reverse: false,
    	changeHash: false
    });
	$("#divAjaxLoading").addClass("show-loading");
} //end openFramedViewer

function iframeOnloaded() {
    $.mobile.loading('hide');
}

function gotoPreviousPage() {
    //this is required to avoid hangup in iOS UIwebviewer
    $('#div_iframe').html("");
    //turn on flag that use to scroll to disclosure section when back to mainpage from pdfViewer popup
    $("#" + currentPage).attr("auto-scrollTo", "true");
    $.mobile.changePage($("#" + currentPage), {});
}

function closePopup(popup) {
	if (typeof popup == "undefined") {
		popup = $(this).closest("div[data-role='popup']");
	}
	$(popup).popup("close");
}

function BindLoggedInLinkedInData(data) {
    is_called = true;
    showLinkedInSection();
    //TODO: need to check for coapp
    if ($('#' + coPrefix + 'txtFName').val() == '') {
        $('#' + coPrefix + 'txtFName').val(data.firstName);
        $('#' + coPrefix + 'txtLName').val(data.lastName);
        $('#' + coPrefix + 'txtEmail').val(data.emailAddress);
    }
    $('#anchor_linkedin_name').val(data.formattedName);
}

function ClearLinkedInData() {
    is_called = false;
    showLinkedInSection();
    $('#' + coPrefix + 'txtFName').val("");
    $('#' + coPrefix + 'txtLName').val("");
    $('#' + coPrefix + 'txtEmail').val("");

    $('#anchor_linkedin_name').val("");
}

//------ LinkedIn section ------//
function showLinkedInSection() {
    if ($('#hd_linkedin_email').val() != "") {
        $('#div_linkedin_login').css('display', 'none');
        $('#div_linkedin_name').css('display', 'block');

        $('#anchor_linkedin_name').html($('#hd_linkedin_formattedname').val());
    } else {
        $('#div_linkedin_login').css('display', 'block');
        $('#div_linkedin_name').css('display', 'none');
    }
}
//clearInputFields only apply for checkbox and radio input
function clearInputFields(inputType) {
    $('.ui-'+inputType).each(function () {
        var element = $(this);
        var elClasses = element.attr('class');
        if (elClasses != undefined) {
            if (elClasses.indexOf('ui-disabled') != -1) { //preselected checkbox
                return true; // continue the iteration
            }
            var lblElement = element.children('label');
            var lblClasses = lblElement.attr('class');
            if (lblClasses != undefined) {
                if (lblClasses.indexOf('ui-'+inputType +'-on') != -1) {
                	lblElement.removeClass('ui-' + inputType + '-on').removeClass('ui-btn-active').addClass('ui-' + inputType + '-off');
	                element.find("input:checkbox,input:radio").prop("checked", false);
                    var spElement = lblElement.children().children().last();
                    var spClasses = spElement.attr('class');
                    if (spClasses != undefined) {
                        if (spClasses.indexOf('ui-icon-' + inputType + '-on') != -1) {
                        	spElement.removeClass('ui-icon-' + inputType + '-on').addClass('ui-icon-' + inputType + '-off');
                        }
                    }
                }
            } //end if lblClasses
        } //end if elClasses
    });
}
//make all label text bold and dropdown answers are normal
function handledFontWeightOfDivContent() {
    changeLabelTextAndDropdownOptionText(); 
    $(document).on("pagecontainerbeforeshow", function () {
        changeLabelTextAndDropdownOptionText();
    });
}
function changeLabelTextAndDropdownOptionText() {
    var currentID = $.mobile.pageContainer.pagecontainer('getActivePage').attr('id');  
        //make all label text is bold except these elements
        var strElements = "divErrorDialog, pl_last, decisionPage";       
        if (strElements.indexOf(currentID) > -1) {
            return;
        }
        var contentElement = '#' + currentID + ' div[data-role="content"]';
        //$(contentElement).addClass('labeltext-bold');
        $(contentElement + ' .ui-btn-text').css('font-weight', 'normal');
        $('#divAgreement').css('font-weight', 'normal');
        $('#divAgreementFinal').css('font-weight', 'normal'); 
}
//if disclosure is plain text limit 160 characters, and if it contains hyperlink only get the hyperlink labels
function parseDisclosures() {

    var btnDisclosureElem = $("div[name='disclosures'] .btn-header-theme");
    var chkDisclosureElem = $("div[name='disclosures'] .ui-checkbox");

    //get all disclosures
    var disclosures = "";
    //get submit message
    var submitMessage = $('#divSubmitMessage').text().trim();
 
    if (submitMessage.length > 150) {
        submitMessage = submitMessage.substring(0,150)+"...";
    }
  
    if (btnDisclosureElem.length == 0 && chkDisclosureElem.length == 0) {
        return "\n" + submitMessage + "\n\n"; //no disclosures
    }
 
  //  disclosures += "\n"+submitMessage+"\n\n";
    //get all btn disclosures
    if (btnDisclosureElem.length > 0) {
        btnDisclosureElem.each(function () {
            var currBtnElem = $(this);
            if (currBtnElem.hasClass('active')) {
                disclosures += currBtnElem.text() + "\n\n";
            }
        });
    }//end get all btn disclosures
    
    //get all chk disclosures
    if (chkDisclosureElem.length > 0) {
        chkDisclosureElem.each(function () {     
            var element = $(this).children('label');
            var temp = element.text().trim().replace(/\s+/g, " ");
            var linkEle = element.find('a');
            if (linkEle.length > 0) {
                if (linkEle.length == 1) { //only one hyperlink
                    disclosures += linkEle.text() + "\n\n";
                } else { //more than one hyperlink
                    var labelHyperlink = "";
                    linkEle.each(function () {
                        labelHyperlink += $(this).text() + ", ";
                    });
                    labelHyperlink = labelHyperlink.substring(0, labelHyperlink.lastIndexOf(","));
                    var pos = labelHyperlink.lastIndexOf(",");
                    disclosures += labelHyperlink.substring(0, pos + 1) + " and " + labelHyperlink.substring(pos + 1) + "\n\n";
                }
            } else { //only text -->limit disclosure 160 characters
                if (temp.length > 160) {
                    disclosures += temp.substring(0, 160) + "...\n\n";
                } else {
                    disclosures += temp + "\n\n";
                }
            }
        });
    }// end get all chk disclosures
  
    return disclosures;
}
//if home phone is empty then map it to cell or work phone based on the selected contact method
function mappingHomePhone(homePhone, workPhone, cellPhone, contactMethod) {
    if (homePhone == "") {
        if (contactMethod == "WORK") {
            homePhone = workPhone;
        } else if (contactMethod == "CELL") {
            homePhone = cellPhone;
        }
    }
    return homePhone;
}

function validateForeignPhone(elementId) {
	var currentElement = $('#' + elementId);
	var errorMessage = "";
	var currentFormatLength = 10; 
	if (currentElement.attr('placeholder') != undefined && currentElement.attr('placeholder')!="") {
		currentFormatLength  = currentElement.attr('placeholder').replace(/\s/g, '').replace(/\D/g, '').length;
	}
	var inputLength = currentElement.val().replace(/\s/g, '').replace(/\D/g, '').length;
   
	//var currentCountryTitle = currentElement.prev().children().first().attr('title');
	var currentCountryTitle = $("div.selected-flag", currentElement.closest("div.intl-tel-input")).attr("title");
	if (currentCountryTitle == undefined) {
		currentCountryTitle = "";
	}
	//only validate US country phone
	if (currentCountryTitle.toUpperCase().indexOf("UNITED STATES") > -1) {
             
		if (currentFormatLength != inputLength) {
			errorMessage = "The phone number '" + currentElement.val() + "' is invalid.";
		} else {
			//validate first digit of phone number : 0 and 1 is invalid
			var firstDigit = currentElement.val().replace("(","").substring(0, 1);
			if (firstDigit == 0 || firstDigit == 1) {
				errorMessage = 'The phone number and area code can not begin with 0 or 1 and must be a valid phone number.';
			}
		}
	}
	return errorMessage;
} 

// Handles moving focus across SSN field DOB fields
function onKeyUp(evt, txt1, txt2, txt1Len) {
	//console.log(evt.target, evt.currentTarget, evt.keyCode);
    var txt1Value = $(txt1).val();
    if ($(txt1)[0].hasAttribute("mask-pattern")) {
        //use for SSN text input
        txt1Value = txt1Value.replace(/_/g, "");
    }
    if (txt1Len == txt1Value.length && $(txt1).data("disable-move-next") != true) {
	    $(txt1).data("disable-move-next", true);
        $(txt2).focus();
    } else if (txt1Len == txt1Value.length - 1) {
    	$(txt1).val(txt1Value.substr(0, 4));
    	$(txt1).data("disable-move-next", true);
    	$(txt2).focus();
    }
}

function padLeft(number, numberOfPlace, paddingCharacter) {
    if (String(number).length == 0) return number;
    if (String(number).length >= numberOfPlace) return number;
    return Array(numberOfPlace - String(number).length + 1).join(paddingCharacter || '0') + number;
}
//validate required driver license scan for loans
function validateRequiredDLScan(requiredDLScan,isCoApp) {
    var strMessage = "";
    var strRequireDLMessage = "Please complete your application in one of our local offices if the system can't extract information from your driver's license bar code.";
    var responseDLMessage = $('#divBackMessage').text();
    if (isCoApp) {
        responseDLMessage = $('#co_divBackMessage').text();
    }
    if (requiredDLScan == "Y") {
        if (!Common.ValidateText(responseDLMessage)) {
            strMessage = strRequireDLMessage;
        } else { //has response text
            if (responseDLMessage.indexOf('DOB:') == -1) { //can not find DL information
                strMessage = strRequireDLMessage;
            } 
        }
    }
    return strMessage;
}

function hasHiddenElement(element) {
    if (element.val() != undefined && element.val().toUpperCase() == "Y") {
        return true;
    }
    return false;
}
function goToNextPage(nextPage, callback) {
    $.mobile.changePage(nextPage, {
        transition: "fade",
        reverse: false,
        changeHash: true
    });
	if (typeof callback == "function") {
		callback();
	}
}

function openPopup(popup, opts) {
	$(popup).popup("open", opts);
}
function isIEBrowser() {
    var isIE = false;
    var userAgent = navigator.userAgent;
    if (userAgent.indexOf('MSIE') > 0 || userAgent.indexOf('Trident') > 0 || userAgent.indexOf('Edge') > 0) {
        isIE = true;
    }
    return isIE;
}
//handle buttons headerTheme
function handledBtnHeaderTheme(element) {
    element.removeClass('btnHover');
    if (element.hasClass('active')) {
        element.addClass('btnActive_on');
        element.removeClass('btnActive_off');
    } else {
        element.removeClass('btnActive_on');
        element.addClass('btnActive_off');
    }
}
// Convert a date string from this format:
//		MM/DD/YYYY
//      0123456789
// to this format:
//		YYYY-MM-DD	
//		0123456789
function convertDate(dateStr) {
    // The date strings that are fed into this function have a constant length and fixed format
    var convertedDate = "";

    if (!Common.ValidateDate(dateStr))
        return "";

    convertedDate += dateStr[6] + dateStr[7] + dateStr[8] + dateStr[9];
    convertedDate += "-";
    convertedDate += dateStr[0] + dateStr[1];
    convertedDate += "-";
    convertedDate += dateStr[3] + dateStr[4];


    return convertedDate;
}

//validate input fields for password type and also using regular expression from loanspq if it exists
//in loanspq, we can set either regular expression or length
function validateRegExpression(element, questionName) {
	var errorMsg = "";
	var cqType = element.attr('type');
	var answerText = element.val();
	if (cqType != undefined) {
		cqType = cqType.toLowerCase();
	}

	if (cqType == 'text' || cqType == 'password') {
		if (answerText.trim() != "") {
		    var strMessage = questionName;
		    if(strMessage.charAt(strMessage.length-1)!="."){
		        strMessage += ".";
		    }
			//using regular expression
			var reg_expr = element.attr("reg_expression");
			if (reg_expr != undefined && reg_expr != "") {
			    var reg = new RegExp(JSON.parse(reg_expr));
			    var ansObj = answerText.match(reg);
				var regMessage = element.attr("error_message");
				if (regMessage != undefined && regMessage != "") {
				    strMessage = regMessage;
				} else {
				    strMessage = strMessage + "-- " + answerText;
				}	
				if (!ansObj) {
				    errorMsg = strMessage ;
				} else {
				    var isMatched = false;
				    for (var i = 0; i < ansObj.length; i++) {
				        if (ansObj[i] == answerText) {
				            isMatched = true;
				            break;
				        }
				    }	 
				    if (!isMatched) {
				        errorMsg =  strMessage ;
				    }
				}	

			} else {
				//validate min and max value
				var maxValue = element.attr("max_value");
				var minValue = element.attr("min_value");
				var numericInputMsg = validateNumericInput(answerText);
				//required minimum length
				if (minValue == undefined || minValue == "") {
					var minLength = element.attr('min_length');
					if (minLength != undefined && minLength != "") {
						minLength = Number(minLength);
						if (answerText.length < minLength)
						    errorMsg = strMessage + " The input must be at least " + minLength + " characters.";
					}  //end minimum length 
				}
				//validate min value
				if (minValue != undefined && minValue != "") {
					if (numericInputMsg != "")
						return strMessage + numericInputMsg;
					var strMinValueMsg = validateMinValue(answerText, minValue);
					if (strMinValueMsg != "")
						return strMessage + strMinValueMsg;
				} //end minValue
				//max valude
				if (maxValue != undefined && maxValue != "") {
					if (numericInputMsg != "")
						return strMessage + numericInputMsg;
					var strMaxValueMsg = validateMaxValue(answerText, maxValue);
					if (strMaxValueMsg != "")
						return strMessage + strMaxValueMsg;
				} //end maxValue !=     ...
			}
     
		}
	} //end cqType == 'text' || ... 
	return errorMsg;
}
/* use validateRegExpression for both application and applicant questions
function validateRegExp(element, questionName) {
	var errorMsg = "";
	var cqType = element.attr('type');
	var answerText = element.val();
	if (cqType != undefined) {
		cqType = cqType.toLowerCase();
	}

	if (cqType == 'text' || cqType == 'password') {
		if (answerText.trim() != "") {
			var strMessage = questionName + ". ";
			//using regular expression
			var reg_expr = element.attr("reg_expression");
			if (reg_expr != undefined && reg_expr != "") {

				var reg = new RegExp(reg_expr);
				var validAnswerText = answerText.match(reg);
				var regMessage = element.attr("error_message");
				if (regMessage == undefined) {
					regMessage = "Valid input is required";
				}
				if (validAnswerText == null) {
					return regMessage;
					//errorMsg = strMessage + "<br/>-- " + regMessage + "<br/>";
				} else {
					if (validAnswerText.indexOf(answerText) == -1) {
						return regMessage;
						//errorMsg = strMessage + "<br/>-- " + regMessage + "<br/>";
					}
				}
			} else {
				//validate min and max value
				var maxValue = element.attr("max_value");
				var minValue = element.attr("min_value");
				var numericInputMsg = validateNumericInput(answerText);
				//required minimum length
				if (minValue == undefined || minValue == "") {
					var minLength = element.attr('min_length');
					if (minLength != undefined && minLength != "") {
						minLength = Number(minLength);
						if (answerText.length < minLength) {
							return "The input must be at least " + minLength + " charaters";
							//errorMsg = strMessage + "<br/>-- " + "The input must be at least " + minLength + " charaters.<br/>";
						}
					}  //end minimum length 
				}
				//validate min value
				if (minValue != undefined && minValue != "") {
					if (numericInputMsg != "")
						return strMessage + numericInputMsg.replace("<br/>", "");
					var strMinValueMsg = validateMinValue(answerText, minValue);
					if (strMinValueMsg != "")
						return strMessage + strMinValueMsg.replace("<br/>", "");
				} //end minValue
				//max valude
				if (maxValue != undefined && maxValue != "") {
					if (numericInputMsg != "")
						return strMessage + numericInputMsg.replace("<br/>", "");
					var strMaxValueMsg = validateMaxValue(answerText, maxValue);
					if (strMaxValueMsg != "")
						return strMessage + strMaxValueMsg.replace("<br/>", "");
				} //end maxValue !=     ...
			}
		}
	} //end cqType == 'text' || ... 
	return errorMsg;
}
*/
function validateNumericInput(answerText) {
	if (isNaN(answerText)) {
		return "The input is not a number.<br/>";
	} else {
		if (Number(answerText) < 0)
			return "The input must be a positive number.<br/>";
	}
	return "";
}
function validateMinValue(answerText, minValue) {
	if (Number(answerText) < Number(minValue)) {
		return "The input must be equal or greater than " + minValue + ".<br/>";
	}
	return "";
}
function validateMaxValue(answerText, maxValue) {
	if (Number(answerText) > Number(maxValue)) {
		return "The input must be equal or less than " + maxValue + ".<br/>";
	}
	return "";
}
//one check box question can have multiple answers -> more complicated
function checkboxQuestion(chkAnswers, chkQuestionName, chkQuestionIndex, chkLength, chkRequired) {
	this.chkAnswers = chkAnswers;
	this.chkQuestionName = chkQuestionName;
	this.chkQuestionIndex = chkQuestionIndex;
	this.chkLength = chkLength;
	this.chkRequired = chkRequired;
}
function getDivConditionQuestionStyle(element) {
	var ui_type = element.attr('type');
	var strDisplayedCQ = "";
	if (ui_type != undefined) {
	    var divCondQuestionID = "div_"+ element.attr('con_iname').toLowerCase();
	    var condDivElement = element.closest("div[id~='" + divCondQuestionID + "']");
	    if (condDivElement.attr('style') != undefined) {
	        strDisplayedCQ = condDivElement.attr('style');
	    }
	}
	return strDisplayedCQ;
}

function ViewConditionQuestions(aqPrefix) {
	var strConditionQuestions = "";
	var conAQClass = aqPrefix + 'ConditionedQuestion';
	if (!$('.' + conAQClass).length) {
		return strConditionQuestions;
	}
	$('input.' + conAQClass).each(function () {
		var element = $(this);
		var strDisplayedCQ = getDivConditionQuestionStyle(element);
		var ui_type = element.attr('type');
		var questionName = element.attr("iname");
		var answerText = element.attr("answer");
		var txtValue = element.val();

		if (strDisplayedCQ.indexOf('none') == -1) {
			if (ui_type == 'checkbox') {
				if (element.prev('.ui-checkbox-on').length > 0) {
				    strConditionQuestions += '<div class="col-xs-12 row-title"><span class="bold">' + questionName + '</span></div><div class="col-xs-12 row-data"><span>' + answerText + '</span></div>';
				}
			}
			if (ui_type == 'radio') {
				if (element.prev('.ui-radio-on').length > 0) {
				    strConditionQuestions += '<div class="col-xs-12 row-title"><span class="bold">' + questionName + '</span></div><div class="col-xs-12 row-data"><span>' + answerText + '</span></div>';
				}
			}
			if (ui_type == 'text') {
				if (txtValue != '' && txtValue != undefined)
					strConditionQuestions += '<div class="col-xs-12 row-title"><span class="bold">' + element.parent().prev().text() + '</span></div><div class="col-xs-12 row-data"><span>' + txtValue + '</span></div>';

			}
			if (ui_type == 'password') {
				if (txtValue != undefined && txtValue.trim() != '') {
					var pwValue = "";
					for (var i = 0; i < txtValue.length; i++) {
					    if (i < txtValue.length - 2) {
					        pwValue += "*"; //don't show the real value
					    } else { //only show last two characters
					        pwValue += txtValue.substring(i);
					        break;
					    }
					}
					strConditionQuestions += '<div class="col-xs-12 row-title"><span class="bold">' + element.parent().prev().text() + '</span></div><div class="col-xs-12 row-data"><span>' + pwValue + '</span></div>';
				}
			}
		}
	});

	$('select.' + conAQClass).each(function () {
		var element = $(this);
		var strDisplayedCQ = getDivConditionQuestionStyle(element);
		if (strDisplayedCQ.indexOf('none') == -1) {
			var ddlText = element.find('option:selected').text();
			var questionName = element.attr('iname');
			var ddlValue = element.find('option:selected').val().toUpperCase();
			if (ddlValue != "" && ddlValue != undefined) { //"None could be a user answer,
				strConditionQuestions += '<div class="col-xs-12 row-title"><span class="bold">' + questionName + '</span></div><div class="col-xs-12 row-data"><span>' + ddlText + '</span></div>';
			}
		}
	});

	return strConditionQuestions;
}
function getChkQuestionName(chkQuestionArray) {
	var chkQuestionNameArray = [];
	var chkQuestionName = "";
	for (var i = 0; i < chkQuestionArray.length; i++) { //filter chk Question 
		chkQuestionName = chkQuestionArray[i].chkQuestionName;
		if (chkQuestionNameArray.indexOf(chkQuestionName) == -1) {
			chkQuestionNameArray.push(chkQuestionName);
		}
	}
	return chkQuestionNameArray;
}
function limitToNumeric(evt) {
	var event = evt || window.event;
	var key = event.keyCode || event.which;
	if (key === 8 || key === 46 || key === 9 || key === 27 || (key >= 35 && key <= 39)
		|| (key >= 96 && key <= 105)
		|| (key >= 48 && key <= 57 && event.shiftKey === false)
		|| key === 35 || key === 36 || key === 37 || key === 39) {
		return;
	}
	event.returnValue = false;
	if (event.preventDefault) event.preventDefault();
}
function ColorLuminance(hex, lum) {

	// validate hex string
	hex = String(hex).replace(/[^0-9a-f]/gi, '');
	if (hex.length < 6) {
		hex = hex[0] + hex[0] + hex[1] + hex[1] + hex[2] + hex[2];
	}
	lum = lum || 0;

	// convert to decimal and change luminosity
	var rgb = "#", c, i;
	for (i = 0; i < 3; i++) {
		c = parseInt(hex.substr(i * 2, 2), 16);
		c = Math.round(Math.min(Math.max(0, c + (c * lum)), 255)).toString(16);
		rgb += ("00" + c).substr(c.length);
	}

	return rgb;
}
//--------select branch location-------
function handleButtonSelection(currElem) {
    var currElemID = '#' + currElem;
    var currBtnElemID = currElemID + '-button';
    // handle selected account type dropdown: hover, selected/unselected
    $('.header-theme-dropdown-btn ' + currBtnElemID).on('mouseenter', function () {
        var element = $(this);
        element.removeClass('btnActive_off');
        element.addClass('btnHover');
    });

    $('.header-theme-dropdown-btn ' + currBtnElemID).on('mouseleave', function () {
        var element = $(this);
        element.removeClass('btnHover');
    });

    $('select' + currElemID).on('change', function () {
        var element = $(this);
        if (element.val() == '') {
            element.parent().addClass('btnActive_off');
            element.parent().removeClass('btnActive_on');
        } else {
            element.parent().addClass('btnActive_on');
            element.parent().removeClass('btnActive_off');
        }
    });
    //end handle selected account type dropdown: hover, selected/unselected
}
function handleShowAndHideBranchNames() {
	if ($("#hdEnableBranchSection").val() !== "Y") return;
	handleButtonSelection('ddlBranchName');
    $('#ddlBranchName').observer({
        validators: [
            function (partial) {
                if ($("#divBranchSection").hasClass('hidden')) return ""; //skip validation
                if ($(this).closest("div.showfield-section").length > 0 && $(this).closest("div.showfield-section").hasClass("hidden")) return "";
                if ($(this).closest("div.validation-section").length > 0 && $(this).closest("div.validation-section").hasClass("abort-require")) return "";
            	if ($('#hdRequiredBranch').val() == "N") return ""; //skip validation
                if ($(this).find('option').length > 0) {
                    if (!Common.ValidateText($(this).val())) {
                        return "Please select branch location";
                    }
                }
                return "";
            }
        ],
        validateOnBlur: true,
        group: "ValidateBranch"
    });
}

//deprecate, not use anywhere
function validateBranchNames() {
    var strMessage = "";
    if ($('#hdVisibleBranch').val() != "Y" || $('#hdRequiredBranch').val() == "N") { //skip validation
        return "";
    }
    if ($('#ddlBranchName').find('option').length > 0) {
        if (!Common.ValidateText($('#ddlBranchName option:selected').val())) {
             strMessage= "Please select branch location.<br/>";
        }
    }
    return strMessage;
}
function viewSelectedBranch() {
    var strHtml = "";
    var selectedBranchElem = $('#ddlBranchName option:selected');   
    if ($("#hdEnableBranchSection").val() == "Y" && ($('#ddlBranchName').closest("div.showfield-section").length == 0 || $('#ddlBranchName').closest("div.showfield-section").hasClass("hidden") == false)) {
	    if (selectedBranchElem.length > 0 && selectedBranchElem.val() != "") {
		    strHtml += '<div class="col-sm-6 col-xs-12"><div class="row"><div class="col-xs-6 text-right row-title"><span class="bold">Branch Name:</span></div><div class="col-xs-6 text-left row-data"><span>' + selectedBranchElem.text() + '</span></div></div></div>';
		    $(".BranchSelection").prev().show();
		    $(".BranchSelection").html(strHtml).show();
	    } else {
		    $(".BranchSelection").prev().hide();
		    $(".BranchSelection").html("").hide();
	    }
    } else {
    	$(".BranchSelection").prev().hide();
    	$(".BranchSelection").html("").hide();
    }
}
function getBranchId() {
    var branchID = $('#hfBranchId').val();
    if ($("#hdEnableBranchSection").val() == "Y" && ($('#ddlBranchName').closest("div.showfield-section").length == 0 || $('#ddlBranchName').closest("div.showfield-section").hasClass("hidden") == false)) {
    	if ($('#ddlBranchName option:selected').val() != "") {
    		branchID = $('#ddlBranchName option:selected').val();
    	}
    }
    return branchID;
}
function setBranchId() {
	
}
function getXAComboBranchId() {
    var xaBranchID = $('#hdXAComboBranchId').val();
    if ($("#hdEnableBranchSection").val() == "Y" && ($('#ddlBranchName').closest("div.showfield-section").length == 0 || $('#ddlBranchName').closest("div.showfield-section").hasClass("hidden") == false)) {
        if ($('#ddlBranchName option:selected').val() != "") {
            xaBranchID = $('#ddlBranchName option:selected').val();
        }
    }
    return xaBranchID;
}
//---------end select branch location -----------------

function ReplaceButtonLabel(lblTitle, text) {
    if (typeof BUTTONLABELLIST == 'undefined' || BUTTONLABELLIST == null) return false;
	var title = BUTTONLABELLIST[text.toLowerCase()];
	var html = lblTitle.html();
	if (typeof title == "string" && $.trim(title) !== "") {
		lblTitle.html(html.replace(EXTRACT_BUTTONLABEL_REGEX, title));
	} else {
		lblTitle.html(html.replace(EXTRACT_BUTTONLABEL_REGEX, text));
	}
}
(function (factory) { factory(jQuery); }(function ($) {
	$.fn.extend({
		IsHideField: function () {
			return $(this).data("hide-field") === "true";
		},
		MakeHideField: function () {
			return $(this).data("hide-field", "true");
		}
	});
}));
function getDataId() {
	return "e_" + Math.random().toString(36).substr(2, 16);
}

function getWalletQuestionsAndAnswers(appInfo) {
    var index = 0;
    var numSelected = 0;
    var numOfQuestions = $('#hdNumWalletQuestions').val();
    numOfQuestions = parseInt(numOfQuestions, 10);
    var walletQuestionsAndAnswers = [];
    var walletAnswersText = [];
    for (index = 1; index <= numOfQuestions; index++) {
        var selectedAnswerText = $('input[name=walletquestion-radio-' + index.toString() + ']:checked').prev('label').text();
        var selectedAnswer = $('input[name=walletquestion-radio-' + index.toString() + ']:checked').attr('data-rq-value');
        var selectedQuestionKey = $('input[name=walletquestion-radio-' + index.toString() + ']:checked').attr('data-rq-key');
        if (selectedAnswer != "" && selectedAnswer != null) {
            var selectedQuestionAndAnswer = { q: selectedQuestionKey, a: JSON.parse(selectedAnswer) };
            walletQuestionsAndAnswers.push(selectedQuestionAndAnswer);
            walletAnswersText.push(selectedAnswerText);
        }
        appInfo.WalletQuestionsAndAnswers = JSON.stringify(walletQuestionsAndAnswers);
        appInfo.WalletAnswersText = JSON.stringify(walletAnswersText);
    }
}
$.mobile.switchPopup = function (sourceElement, destinationElement, onswitched) {
	var afterClose = function () {
		destinationElement.popup("open");
		sourceElement.off("popupafterclose", afterClose);

		if (onswitched && typeof onswitched === "function") {
			onswitched();
		}
	};

	sourceElement.on("popupafterclose", afterClose);
	sourceElement.popup("close");
};
//location pool section for standard loans
function getSelectedZipPoolNames(zipCode, zipCodePoolIds, zipCodePoolNames) {
    var selectedPoolNames = [];
    var selectedZipPoolIds = zipCodePoolIds[zipCode] ? zipCodePoolIds[zipCode] : [];
    if (selectedZipPoolIds.length == 0) return selectedPoolNames;
    for (var i = 0; i < selectedZipPoolIds.length; i++) {
        var sSelectedPoolName = zipCodePoolNames[selectedZipPoolIds[i]];
        if (sSelectedPoolName == undefined || sSelectedPoolName == "") continue;
        if ($.inArray(sSelectedPoolName, selectedPoolNames) == -1) selectedPoolNames.push(sSelectedPoolName);
    }
    return selectedPoolNames;
}
function getSelectedZipPoolProducts(zipCode, loanProducts) {
    var zpProducts = [];
    var selectedPoolNames = getSelectedZipPoolNames(zipCode, loanProducts.ZipPoolIds, loanProducts.ZipPoolNames);
    if (loanProducts.ZipPoolProducts.length == 0) return zpProducts;
    for (var zp in loanProducts.ZipPoolProducts) {
        var oProduct = loanProducts.ZipPoolProducts[zp];
        if (oProduct.ZipCodeFilterType == "ALL") {
            zpProducts.push(oProduct);
        } else if (oProduct.ZipCodeFilterType == "INCLUDE") {
            if ($.inArray(oProduct.ZipCodePoolName, selectedPoolNames) > -1) {
                zpProducts.push(oProduct);
            }
        } else if (oProduct.ZipCodeFilterType == "EXCLUDE") {
            if ($.inArray(oProduct.ZipCodePoolName, selectedPoolNames) == -1) {
                zpProducts.push(oProduct);
            }
        }
    }
    return zpProducts;
}
function getSelectedZipPoolLoanPurposes(selectedZipPoolProducts) {
    var selectedZipPoolLoanPurposes = [];
    if (selectedZipPoolProducts.length == 0) return selectedZipPoolLoanPurposes;
    for (var sp in selectedZipPoolProducts ) {
        //it could be multiple loan purposes which are seperated by comma.
        var oLoanPurposes = selectedZipPoolProducts[sp].AcceptablePurposeTypes.split(",");
        if (oLoanPurposes.length >= 0) {
            for (var lp in oLoanPurposes) {
                if ($.inArray(oLoanPurposes[lp], selectedZipPoolLoanPurposes) == -1) selectedZipPoolLoanPurposes.push(oLoanPurposes[lp]);
            } 
        }
    }
    return selectedZipPoolLoanPurposes;
}
function handledLocationPoolLoanInfo(loanType, oZPObj) {
    $("#"+loanType+"_txtLocationPoolCode").on("keyup paste", function () {
        var $self = $(this);
        var zipCode = $self.val();
        var $divLocationPool = $('#' + loanType + '_divLocationPool');
        var $divNoAvailableProduct = $('#' + loanType + '_divNoAvailableProducts');
        setTimeout(function () {
            if (/[0-9]{5}/.test(zipCode)) {                                    
                var selectedZipPoolProducts = getSelectedZipPoolProducts(zipCode, oZPObj);
                if (selectedZipPoolProducts.length == 0) {
                    $divNoAvailableProduct.removeClass('hidden');
                    $divLocationPool.next('div').addClass('hidden');
                } else {        
                    $divNoAvailableProduct.addClass('hidden');
                    $divLocationPool.next('div').removeClass('hidden');
                    $("#txtZip").val(zipCode);
                    $("#txtCity").val("");
                    $("#txtZip").attr("disabled", "disabled");
                    $("#txtZip").trigger("change");             
                }
            } else {
                $divLocationPool.next("div").addClass("hidden");
            }
        }, 400);
    });
}
//end location pool section for standard loans

/**
 * A common helper function, for use in loan/xa script.js files, to help collect the CustomAnswers from the loanInfo
 * object into the formValues object for use in Applicant Block Logic rule execution.
 * @param {"Application"|"Applicant"} cqRole
 * @param {"LoanPage"|"ApplicantPage"|"ReviewPage"} cqLocation
 * @param {string} cqApplicantPrefix
 * @param {Object} loanInfo The results of `getPersonalLoan()` or the equivalent from each script.js file. Must have `.CustomAnswers` from xaApplicantQuestion.ascx:getAllCustomQuestionAnswers().
 * @param {Object} formValues An empty or partially-filled object in which data for ABR is inserted.
 */
function collectCustomQuestionAnswers(cqRole, cqLocation, cqApplicantPrefix, loanInfo, formValues) {
	var prefix;
	if (cqRole == "Application") {
		prefix = "CQ_";
	} else if (cqRole == "Applicant") {
		prefix = "AQ_";
	} else {
		console.error("Invalid CQRole: ", cqRole);
		return;
	}
	if (["LoanPage", "ApplicantPage", "ReviewPage"].indexOf(cqLocation) < 0) {
		console.error("Invalid CQLocation: ", cqLocation);
		return;
	}

	var cqAnswers = null;
	if (loanInfo.CustomAnswers) {
		cqAnswers = $.parseJSON(loanInfo.CustomAnswers);
		if (Array.isArray(cqAnswers)) {
			// Only get the CQs from the LoanPage, not the review page, to prevent soft-locking the user.
			cqAnswers = cqAnswers.filter(function (cqa) { return cqa.CQLocation == cqLocation && cqa.CQRole == cqRole });
			// Only filter by applicant prefix if it has value
			if (cqApplicantPrefix != null) {
				cqAnswers = cqAnswers.filter(function (cqa) { return cqa.CQApplicantPrefix == cqApplicantPrefix });
			}
		}
	}
	if (cqAnswers != null && cqAnswers.length > 0) {
		_.forEach(cqAnswers, function (cqa) {
			const sCqName = prefix + cqa.CQName.replace(/\s/g, "_");
			if (cqa.CQType == "CHECKBOX") {
				// Checkboxes store the value in arrays instead of regular strings.
				// Answers come in as separate entries (because the CallBack handler uses them like that). Transform into an array here.
				if (formValues.hasOwnProperty(sCqName)) {
					formValues[sCqName].push(cqa.CQAnswer);
				} else {
					formValues[sCqName] = [cqa.CQAnswer];
				}
			} else {
				formValues[sCqName] = cqa.CQAnswer;
			}
		});
	}
}

//Script for Funding Disclosure section
function registerFundingDisclosureValidator(){
    if($("#fundingDisclosure").length === 0) return;
    $.lpqValidate.removeValidationGroup("ValidateFundingDisclosure");
    $("#fundingDisclosure").observer({
        validators: [
            function (partial) {
                if(!$(this).is(':checked')){
                    return "Please review and check funding disclosure.";
                }
                return "";
            }
        ],
        validateOnBlur: true,
        group: "ValidateFundingDisclosure",
        groupType: "complexUI",
        placeHolder: $("#divfundingDisc").find("div.error-placeholder")
    });
}
function validateFundingDisclosure(){
    if($("#fundingDisclosure").length === 0 || 
        $("#fsSelectedFundingType").length === 0 ||
        $("#fsSelectedFundingType").val() === "" ||
        $("#fsSelectedFundingType").val() === "NOT FUNDING AT THIS TIME") return true;
    return $.lpqValidate("ValidateFundingDisclosure");
}
//End Script for Funding Disclosure section