﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="ResetPassword.aspx.vb" Inherits="Sm_ResetPassword" %>
<%@ Import Namespace="System.Web.Optimization" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1 user-scalable=no" />
	<link rel="shortcut icon" href="https://app.loanspq.com/logo/lpq_favicon.ico"  type="image/ico"  />
	<%:Styles.Render("~/css/smcss")%>
</head>
<body>
    <div class="container reset-password-page">
	    <div class="row top30">
			<div class="col-xs-10 col-xs-offset-1 col-md-6 col-md-offset-3 main-form">
				<form>
					<div>
						<h2>APM Reset Password</h2>
						<%--<p>You are about to activate your account.</p>--%>
						<p>Enter a new password for your account.</p>
					</div>
					<div>
						<div class="form-group">
							<label class="required-field">New Password</label>
							<input type="password" class="form-control" data-format="password" name="Password" id="txtPassword" placeholder="Password" maxlength="50" />
						</div>
					</div>
					<div>
						<div class="form-group">
							<label class="required-field">Confirm New Password</label>
							<input type="password" class="form-control" name="ConfirmPassword" id="txtConfirmPassword" placeholder="Confirm Password" maxlength="50" />
						</div>
					</div>
					<div>
						<input type="hidden" id="hdToken" value="<%=tokenStr%>"/>
						<button type="button" id="btnResetPassword" class="btn btn-primary pull-right">Reset Password</button>
					</div>
				</form>
			</div>
		</div>
    </div>
	<%:Scripts.Render("~/js/smscript")%>
	<script type="text/javascript">
		$(function () {
			$("#txtPassword").each(function (idx, ele) {
				$(ele).observer({
					validators: [
						function (partial) {
							var $self = $(this);
							if ($.trim($self.val()) === "") {
								return "This field is required";
							}
							var result = _COMMON.validatePassword($self.val());
							if (result != "") {
								return result;
							}
							return "";
						}
					],
					validateOnBlur: true,
					group: "validatePassword"
				});
			});
			$("#txtConfirmPassword").each(function (idx, ele) {
				$(ele).observer({
					validators: [
						function (partial) {
							var $self = $(this);
							var value = $.trim($self.val());
							if (value === "") {
								return "This field is required";
							}
							if (value !== $.trim($("#txtPassword").val())) {
								return "Confirm password is not match.";
							}
							return "";
						}
					],
					validateOnBlur: true,
					group: "validatePassword"
				});
			});
			$("#btnResetPassword").on("click", function () {
				if ($.smValidate("validatePassword") == false) return;
				//update data
				var postData = {
					command: "resetPassword",
					token: $("#hdToken").val(),
					password: $("#txtPassword").val(),
					confirmPassword: $("#txtConfirmPassword").val()
				};
				$.ajax({
					url: "/smauthhandler.aspx",
					async: true,
					cache: false,
					type: 'POST',
					dataType: 'html',
					data: postData,
					success: function (responseText) {
						var response = $.parseJSON(responseText);
						if (response.IsSuccess) {
							_COMMON.noty("success", "Success. Redirecting to login screen", 500);
							setTimeout(function () {
								window.location = "login.aspx";
							}, 2000);
						} else if (response.Info == "INVALID") {
							$.smValidate.showValidation($("#txtPassword"), response.Message);
						} else {
							var errorMsg = response.Message !== "" ? response.Message : "Error";
							_COMMON.noty("error", errorMsg, 500);
						}
					}
				});
			});
			$(document).ajaxStart(function () { Pace.restart(); });
		});
	</script>
</body>
</html>
