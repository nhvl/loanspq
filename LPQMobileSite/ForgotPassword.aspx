﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="ForgotPassword.aspx.vb" Inherits="Sm_ForgotPassword" %>

<%@ Import Namespace="System.Web.Optimization" %>


<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1 user-scalable=no"/>
	<link rel="shortcut icon" href="https://app.loanspq.com/logo/lpq_favicon.ico"  type="image/ico"  />
	<%:Styles.Render("~/css/smcss")%>
	<%:Scripts.Render("~/js/smscript")%>
	
</head>
<body>
    <div class="container forgot-password-page">
	    <div class="row top30">
			<div class="col-xs-10 col-xs-offset-1 col-md-6 col-md-offset-3 main-form">
				<form>
					<h1>Reset Password</h1>
					<div>
						<div class="form-group">
							<label class="required-field">Email</label>
							<input type="text" class="form-control" value="" name="UserName" id="txtUserName" placeholder="email" maxlength="50" />
						</div>
					</div>
					<div id="divMsg"></div>
					<div>
						<button type="button" id="btnResetPassword" class="btn btn-primary pull-right">Reset Password</button>
					</div>
				</form>
			</div>
		</div>
    </div>

	<script type="text/javascript">
		$(function () {
			$("#txtUserName").observer({
				validators: [
					function(partial) {
						var $self = $(this);
						var email = $.trim($self.val());
						if (email === "") {
							return "This field is required";
						}
						if (_COMMON.ValidateEmail(email) == false) {
							return "Invalid email";
						}
						return "";
					}
				],
				validateOnBlur: true,
				group: "validateSubmitData"
			});
			$("#txtUserName").on("focus", function () {
				$("#divMsg").html("");
			});
			$("#btnResetPassword").on("click", function () {
				var $divForm = $(this).closest("div.main-form");
				if ($.smValidate("validateSubmitData") == false) return;
				//update data
				var postData = {
					command: "forgotPassword",
					userName: $("#txtUserName").val().trim()
				};
				$.ajax({
					url: "/smauthhandler.aspx",
					async: true,
					cache: false,
					type: 'POST',
					dataType: 'html',
					data: postData,
					success: function (responseText) {
						var response = $.parseJSON(responseText);
						if (response.IsSuccess) {
							//if (response.Info == "OK") {
							//	$divForm.find("form").remove();
							//	$divForm.find("p").remove();
							//	$("<br/><p></p>").text(response.Message).appendTo($divForm);
							//} else {
							//	$("#divMsg").html(response.Message);
							//}
							$("#divMsg").html(response.Message);
						} else {
							var errorMsg = response.Message !== "" ? response.Message : "Error";
							_COMMON.noty("error", errorMsg, 500);
						}
					}
				});
			});
			$(document).ajaxStart(function () { Pace.restart(); });
		});
	</script>
</body>
</html>
