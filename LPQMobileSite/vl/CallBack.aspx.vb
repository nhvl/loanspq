﻿Imports CustomList
Imports Newtonsoft.Json
Imports LPQMobile.BO
Imports LPQMobile.Utils
Imports System.Xml
Imports System.Web.Script.Serialization

Partial Class vl_CallBack
	Inherits CBasePage
	'Private QualifiedProduct As String
	Private QualifiedProduct As New List(Of String)
	'Private QualifiedProductv2 As New System.Collections.Generic.Dictionary(Of String, String)
	Private QualifiedProducts As New List(Of Dictionary(Of String, String))
	Private _WorkFlowMode As String = ""
#Region "property that need to persist"
	''need to use for 2nsd postback
	Property loanRequestXMLStr As String
		Get
			Return Common.SafeString(Session("loanRequestXMLStr"))
		End Get
		Set(value As String)
			Session("loanRequestXMLStr") = value
		End Set
	End Property
	Property loanResponseXMLStr As String
		Get
			Return Common.SafeString(Session("loanResponseXMLStr"))
		End Get
		Set(value As String)
			Session("loanResponseXMLStr") = value
		End Set
	End Property

	'need to store this for comparision with answer in the next post back
	Property questionList_persist As List(Of CWalletQuestionsRendering.questionDataClass)
		Get
			If Session("vl_questionList") IsNot Nothing Then
				Return CType(Session("vl_questionList"), System.Collections.Generic.List(Of CWalletQuestionsRendering.questionDataClass))
			End If
			Return Nothing
		End Get
		Set(value As List(Of CWalletQuestionsRendering.questionDataClass))
			Session("vl_questionList") = value
		End Set
	End Property

	Property IsJointApplication As Boolean
		Get
			If Common.SafeString(Session("IsJointApplication")) = "Y" Then
				Return True
			Else
				Return False
			End If
		End Get
		Set(value As Boolean)
			If value = True Then
				Session("IsJointApplication") = "Y"
			Else
				Session("IsJointApplication") = Nothing
			End If
		End Set
	End Property
	Property Co_FName As String
		Get
			Return Common.SafeString(Session("co_FirstName"))
		End Get
		Set(value As String)
			Session("co_FirstName") = value
		End Set
	End Property
	Property transaction_ID As String 'same as session_id
		Get
			Return Common.SafeString(Session("transaction_id"))
		End Get
		Set(value As String)
			Session("transaction_id") = value
		End Set
	End Property

	Property question_set_ID As String
		Get
			Return Common.SafeString(Session("question_set_ID"))
		End Get
		Set(value As String)
			Session("question_set_ID") = value
		End Set
	End Property 'same as quiz_id

	Property request_app_ID As String
		Get
			Return Common.SafeString(Session("request_app_ID"))
		End Get
		Set(value As String)
			Session("request_app_ID") = value
		End Set
	End Property
	Property request_client_ID As String
		Get
			Return Common.SafeString(Session("request_client_ID"))
		End Get
		Set(value As String)
			Session("request_client_ID") = value
		End Set
	End Property
	Property request_sequence_ID As String
		Get
			Return Common.SafeString(Session("request_sequence_ID"))
		End Get
		Set(value As String)
			Session("request_sequence_ID") = value
		End Set
	End Property

	Property loanID As String
		Get
			Return Common.SafeString(Session("loanID"))
		End Get
		Set(value As String)
			Session("loanID") = value
		End Set
	End Property
	Property loanNumber As String
		Get
			Return Common.SafeString(Session("loanNumber"))
		End Get
		Set(value As String)
			Session("loanNumber") = value
		End Set
	End Property
	Property reference_number As String
		Get
			Return Common.SafeString(Session("reference_number"))
		End Get
		Set(value As String)
			Session("reference_number") = value
		End Set
	End Property
	Property proposedLoanAmount As String
		Get
			Return Common.SafeString(Session("proposedLoanAmount"))
		End Get
		Set(value As String)
			Session("proposedLoanAmount") = value
		End Set
	End Property
	Property loanTerm As String
		Get
			Return Common.SafeString(Session("loanTerm"))
		End Get
		Set(value As String)
			Session("loanTerm") = value
		End Set
	End Property
	Property xaRequestXMLStr As String
		Get
			Return Common.SafeString(Session("xaRequestXMLStr"))
		End Get
		Set(value As String)
			Session("xaRequestXMLStr") = value
		End Set
	End Property
	Property xaResponseXMLStr As String
		Get
			Return Common.SafeString(Session("xaResponseXMLStr"))
		End Get
		Set(value As String)
			Session("xaResponseXMLStr") = value
		End Set
	End Property
	Property xaID As String
		Get
			Return Common.SafeString(Session("xaID"))
		End Get
		Set(value As String)
			Session("xaID") = value
		End Set
	End Property
	Property Decision_v2_Response As String
		Get
			Return Common.SafeString(Session("Decision_v2_ResponseCC"))
		End Get
		Set(value As String)
			Session("Decision_v2_ResponseCC") = value
		End Set
	End Property

	Property DecisionMessage As String
		Get
			Return Common.SafeString(Session("DecisionMessage"))
		End Get
		Set(value As String)
			Session("DecisionMessage") = value
		End Set
	End Property

	Property CrossSellMessage As String
		Get
			Return Common.SafeString(Session("CrossSellMessage"))
		End Get
		Set(value As String)
			Session("CrossSellMessage") = value
		End Set
	End Property

	Property SelectedProducts As String
		Get
			Return Common.SafeString(Session("SelectedProducts"))
		End Get
		Set(value As String)
			Session("SelectedProducts") = value
		End Set
	End Property

#End Region
	''Mode/Workflow
	'1) normal using decisionloan(v1.0)- decisionloan, check response for deny or approved(has qualify products)
	'2) normal using decisionloan/2.0(configured via loan_submit_url attribute)- decisionloan, check response for deny or approved(has qualify products), display approved product for consumer to selected
	'3) normal using decisionloan(v1.0) with IDA(cacu) - submitloan, run IDA and display wallet question, submit wallet question, decisionloan to underwrite loan or abort if IDA failed.
	'4) Combo using decisionloan/2.0 - decisionloan(cc),submitloan(XA), IDA, InstantApproved loan and XA(if pass IDA and has qualified products) .......
	Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
		CPBLogger.PBLogger.Component = Me.GetType.ToString
		CPBLogger.PBLogger.Context = Request.UserHostAddress & "/" & _CurrentLenderRef
		CPBLogger.logInfo("Vehicle Loan: Callback page is loading")
		If IsInEnvironment("LIVE") Then
			Dim editMode As Boolean = Common.SafeBoolean(Request.Form("eMode"))
			If Not String.IsNullOrEmpty(PreviewCode.Trim()) OrElse editMode = True Then
				Response.Clear()
				Response.Write(Common.PrependIconForNonApprovedMessage("[Submission result display here]", "Thank You"))
				Response.End()
				Return
			End If
		End If
		_ProceedXAAnyway = Common.SafeString(Request.Form("ProceedXAAnyway")) = "Y"	'TODO:need to hookup to client side for the btn below FOM
		_WorkFlowMode = Common.getNodeAttributes(_CurrentWebsiteConfig, "VEHICLE_LOAN", "workflow_mode")
		Dim sAutoCreatePDFCode = Common.getNodeAttributes(_CurrentWebsiteConfig, "VEHICLE_LOAN", "pdf_code_for_auto_create")
		If Common.SafeStripHtmlString(Request.Form("Task")) = "SaveIncompletedLoan" Then
			Dim loan As New SmLoan
			loan.LoanSavingID = Common.SafeGUID(Request.Form("LoanSavingID"))
			Dim vehicleLoanObject = GetVehicleLoanObject(_CurrentWebsiteConfig)
			loan.LenderID = Guid.Parse(_CurrentWebsiteConfig.LenderId)
			loan.LenderRef = _CurrentWebsiteConfig.LenderRef
			loan.VendorID = vehicleLoanObject.VendorID
			loan.UserID = vehicleLoanObject.UserID
			loan.ApplicantName = vehicleLoanObject.Applicants(0).FirstName & " " & vehicleLoanObject.Applicants(0).LastName
			loan.CreatedDate = Now
			Dim requestNode As XmlNode = Common.getSubmitLoanData(vehicleLoanObject, _CurrentWebsiteConfig, False).SelectSingleNode("INPUT/REQUEST")
			If requestNode IsNot Nothing Then
				loan.RequestParam = requestNode.InnerText
			End If
			loan.SubmitDate = Nothing
			loan.Status = "INCOMPLETE"
			loan.FundStatus = "NOT FUND"
			loan.RequestParamJson = vehicleLoanObject.LoanRawData
			loan.LoanType = SmSettings.LenderVendorType.VL
			If IsComboMode Then
				loan.LoanType = SmSettings.LenderVendorType.VL_COMBO
			End If
			Dim smBl As New SmBL()
			Dim result = smBl.SaveInCompletedLoan(loan)
			Response.Clear()
			Response.Write(result)
			Response.End()
			Return
		End If
		'--Combo mode
		If IsComboMode Then
            ExecComboModeFlow()
            Return
		End If

		'select product and display final message
		If (Request.Form("Task") = "SelectProduct") Then 'only apply for decisionloan/2.0	
			If String.IsNullOrEmpty(Decision_v2_Response) Then
				Response.Write(DecisionMessage)
				Return
			End If
			Dim selectIndex As String = Common.SafeString(Request.Form("Index"))
			Dim sResponse As String = Common.SubmitProduct(Decision_v2_Response, _CurrentWebsiteConfig, selectIndex)
			''====Docusign
			Dim bIsDocuSign As Boolean = Common.getNodeAttributes(_CurrentWebsiteConfig, "VEHICLE_LOAN/VISIBLE", "in_session_docu_sign") = "Y"
			If bIsDocuSign Then
				Dim sLoanNumber = Common.getLoanNumber(Decision_v2_Response)
				Dim docuSignUrl As String = CDocuSign.GetDocuSign(_CurrentWebsiteConfig, sLoanNumber, "VL", Request)
				If docuSignUrl <> "" Then
                    Dim docuSignHtml As String = "<iframe id='ifDocuSign' title='Loan Docusign' scrolling='auto' frameborder='0' width='100%' height='500px' sandbox='allow-same-origin allow-scripts allow-popups allow-forms' src='" + docuSignUrl + "'></iframe>"
                    DecisionMessage = docuSignHtml + DecisionMessage
				End If
			End If

			''==Xsell
			Dim bXSell As Boolean = Common.getNodeAttributes(_CurrentWebsiteConfig, "VEHICLE_LOAN/VISIBLE", "auto_cross_qualify") = "Y"
			If bXSell Then
				DecisionMessage = DecisionMessage + CrossSellMessage
				CrossSellMessage = ""
			End If
			Response.Write(DecisionMessage)
		End If

		If (Request.Form("Task") = "SubmitLoan") Then
			Dim IP_ErrMessage As String = ValidateIP()
			If Not String.IsNullOrEmpty(IP_ErrMessage) Then
				Response.Write(IP_ErrMessage)
				log.Info(IP_ErrMessage & "  IP: " & Request.UserHostAddress & "/" & _CurrentLenderRef)
				Return
			End If
			Dim sReason As String = CApplicantBlockLogic.Execute(_CurrentWebsiteConfig, Request, "VL")
			If Not String.IsNullOrEmpty(sReason) Then
				Response.Write(sReason)
				Return
			End If

			Dim loanIDAType As String = Common.SafeString(Request.Form("idaMethodType"))
			Dim isDisagreeSelect = Common.SafeString(Request.Form("isDisagreeSelect")) = "Y"

			Dim disableSSOIDA = IIf(Common.SafeString(Request.Form("HasSSOIDA")) = "Y", False, IsSSO)
			If String.IsNullOrEmpty(loanIDAType) Or disableSSOIDA Then
				''no ida, or SSO -->run normal process
				''make sure to decode html in xml config
				Dim oResponseXml As String = ""
				Dim vehicleLoan As CVehicleLoan = GetVehicleLoanObject(_CurrentWebsiteConfig)
				DecisionMessage = Server.HtmlDecode(SubmitVehicleLoan(vehicleLoan, _CurrentWebsiteConfig, oResponseXml))
				''check user select disagree button, and disagree_popup is on then we submit data to LPQ using submitloan and stop process 
				If isDisagreeSelect Then
					Return
				End If
				'TODO: detect error and skip to avoid error log
				If _CurrentWebsiteConfig.SubmitLoanUrl.Contains("decisionloan/2.0") Then
					Decision_v2_Response = oResponseXml	'for later use
					QualifiedProducts = getQualifyProduct2_0(oResponseXml)
				Else
					QualifiedProduct = (getQualifyProduct(oResponseXml))
				End If

				If QualifiedProduct.Count = 0 And QualifiedProducts.Count = 0 Then
					Response.Write(DecisionMessage)
					Return
				End If

				Dim message As String = ""
				If _CurrentWebsiteConfig.SubmitLoanUrl.Contains("decisionloan/2.0") And QualifiedProducts.Count > 0 Then
					message = displayProduct2_0() ' display only product selection						
				Else
					message = displayProduct() + "<div class='justify-text'><p>" & DecisionMessage & "</p></div>"
				End If

				'-- has qualify product below this line
				Dim sLoanNumber = Common.getLoanNumber(oResponseXml)
				Dim sloanID = Common.getResponseLoanID(oResponseXml)
				Dim sLoanBooking = Common.getNodeAttributes(_CurrentWebsiteConfig, "VEHICLE_LOAN", "loan_booking")
				Dim sAccountNumber = ""
				Dim bExecPacificCustomWorkFlow = False
				If _WorkFlowMode.Contains("PACIFIC") Then ''pacific merchant mode 
					Dim oCWF As New CCustomWorkFlows(_CurrentWebsiteConfig, _WorkFlowMode)
					bExecPacificCustomWorkFlow = oCWF.ExecPacificCustomWorkFlow(sloanID, "VL", loanRequestXMLStr)
					If Not bExecPacificCustomWorkFlow Then
						message = Server.HtmlDecode(CCustomMessage.getComboCustomResponseMessages(_CurrentWebsiteConfig, "SUBMITTED_MESSAGE", oResponseXml, loanRequestXMLStr, ""))
					End If
				Else
					Dim bookingValidationResult = ProceedBookingValidation(vehicleLoan)
                    If bookingValidationResult IsNot Nothing AndAlso bookingValidationResult.Count > 0 Then
                        ''booking validation is triggered then no booking, and update status to Referred and also update the custom approved message to custom submitting message
                        LoanSubmit.UpdateLoan(sloanID, _CurrentWebsiteConfig, "REFERREDLOAN_BOOKING_VALIDATION", "", String.Join(". ", bookingValidationResult.Select(Function(p) p.Name)))
                        Response.Write(Server.HtmlDecode(CCustomMessage.getComboCustomResponseMessages(_CurrentWebsiteConfig, "SUBMITTED_MESSAGE", oResponseXml, loanRequestXMLStr, "")))
                        Return
                    Else
                        ''run booking, checking for enable is done inside CBookingManager                  
                        sAccountNumber = CBookingManager.Book(_CurrentWebsiteConfig, sloanID, "VL")
                        'TODO:
                        'do we need to update loan status and internal comment when booking fail and update success message
                        'also add this for IDA loop and other loan
                    End If
				End If

				'auto create PDF in the Letter/Doc>Archived Section of LPQ
				If String.IsNullOrEmpty(_CurrentWebsiteConfig.Booking) Or (Not String.IsNullOrEmpty(_CurrentWebsiteConfig.Booking) AndAlso Not String.IsNullOrEmpty(sAccountNumber)) Then
					If Not String.IsNullOrEmpty(sAutoCreatePDFCode) Then CDocument.GeneratePDFs(_CurrentWebsiteConfig, sLoanNumber, "VL", "ARCHIVED", sAutoCreatePDFCode)
				End If

				''====Docusign
				Dim bIsDocuSign As Boolean = Common.getNodeAttributes(_CurrentWebsiteConfig, "VEHICLE_LOAN/VISIBLE", "in_session_docu_sign") = "Y"

				Dim bManualProductSelect As Boolean = message.ToUpper.IndexOf("ACCEPT OFFER") > -1
				If bIsDocuSign And Not bManualProductSelect Then
					Dim docuSignUrl As String = CDocuSign.GetDocuSign(_CurrentWebsiteConfig, sLoanNumber, "VL", Request)
					If docuSignUrl <> "" Then
                        Dim docuSignHtml As String = "<iframe id='ifDocuSign' title='Loan Docusign' scrolling='auto' frameborder='0' width='100%' height='500px' sandbox='allow-same-origin allow-scripts allow-popups allow-forms' src='" + docuSignUrl + "'></iframe>"
                        message = docuSignHtml + message
					End If
				End If


				''==Xsell
				Dim bXSell As Boolean = Common.getNodeAttributes(_CurrentWebsiteConfig, "VEHICLE_LOAN/VISIBLE", "auto_cross_qualify") = "Y"
				''Temporary hardcode PL# and force this to run so we can debug cross-sell
				If bXSell AndAlso Not Is2ndLoanApp Then
					Dim sXAXSellComment As String = Common.SafeString(Request.Form("xaXSellComment"))
					If sXAXSellComment.Contains("CROSS SELL FROM XA") Then ''stop the looping of CrossSell come from 
						Response.Write(message)
						Return
					End If
					Dim sessionId As String = Guid.NewGuid().ToString().Substring(0, 8).Replace("-", "").ToLower()
					Dim sCrossSellMsg As String = CXSell.GetCrossQualification(_CurrentWebsiteConfig, sLoanNumber, _LoanType, sessionId)
					Dim postedData As New NameValueCollection
					postedData = Request.Params
					''second call bXSell
					If Is2ndLoanApp AndAlso String.IsNullOrEmpty(Common.SafeString(Request.Form("SID"))) = False Then
						Dim _sid = Common.SafeString(Request.Form("SID"))
						If Session(_sid) IsNot Nothing Then
							postedData = CType(Session(_sid), NameValueCollection)
						End If
					End If
					''add xSell comment from VL
					Session.Add("xSell" & sessionId, "CROSS SELL FROM VL#" & sLoanNumber)
					Session.Add(sessionId, postedData)
					If message.ToUpper.Contains("ACCEPT OFFER") Then
						CrossSellMessage = sCrossSellMsg
					Else
						message = message + sCrossSellMsg
					End If
				End If
				Response.Write(message)
				Return
			Else ''run ida
				Dim vehicleLoan As CVehicleLoan = GetVehicleLoanObject(_CurrentWebsiteConfig)
				Dim message As String = Server.HtmlDecode(SubmitVehicleLoan(vehicleLoan, _CurrentWebsiteConfig, loanResponseXMLStr))
				''check user select disagree button, and disagree_popup is on then we submit data to LPQ using submitloan and stop process 
				If isDisagreeSelect Then
					Return
				End If
				Response.Write(message)	 ''alway expected wallet question
				Return
			End If

		End If
		If (Request.Form("Task") = "WalletQuestions") Then
			''wallet answer
			''Dim combinedAnswerStr As String = Common.SafeString(Request.Form("WalletAnswers"))
			''Dim answerIDList As List(Of String) = Common.parseAnswersForID(combinedAnswerStr)
			Dim sWalletQuestionsAndAnswers As String = Common.SafeString(Request.Form("WalletQuestionsAndAnswers"))
			Dim loanIDAType As String = Common.SafeString(Request.Form("idaMethodType"))
			Dim idaResponseIDList As List(Of String) = idaResponseIDs(loanIDAType)
			Dim hasJointWalletAnswer As String = Common.SafeString(Request.Form("hasJointWalletAnswer"))
			Dim isJointAnswers = hasJointWalletAnswer = "Y"
			Dim isExecuteAnswer As Boolean = Common.ExecWalletAnswers(_CurrentWebsiteConfig, sWalletQuestionsAndAnswers, questionList_persist, idaResponseIDList, isJointAnswers, question_set_ID, request_app_ID, request_client_ID, request_sequence_ID, reference_number)
			Dim sRequestXML As XmlDocument = New XmlDocument()
			Dim responseMessage As String = ""
			Dim loanResponseXML As String = ""
			sRequestXML.LoadXml(loanRequestXMLStr)
			Dim customSubmittedMessage As String = Server.HtmlDecode(Common.WebPostResponse(_CurrentWebsiteConfig, sRequestXML, loanResponseXMLStr, False))
			customSubmittedMessage = "<div class='justify-text'><p>" & customSubmittedMessage & "</p></div>"
			If Not isExecuteAnswer Then
				Response.Write(customSubmittedMessage) ''failed -> response submit message
				Return
			End If
			' populate questions for joint 
			If (IsJointApplication And Not isJointAnswers) Then
				''response wallet question for joint
				Dim strResponse As String = Common.getWalletQuestionsResponseXML(_CurrentWebsiteConfig, loanResponseXMLStr, loanIDAType, IsJointApplication)
				If String.IsNullOrEmpty(strResponse) Then
					Response.Write(customSubmittedMessage) ''failed -> response submit message
					Return
				End If
				Dim strRenderWalletQuestions As String = Common.renderWalletQuestionsHTML(loanIDAType, strResponse, questionList_persist, Co_FName, transaction_ID, question_set_ID, request_app_ID, request_client_ID, request_sequence_ID, reference_number)
				Response.Write(strRenderWalletQuestions) ''response wallet question for joint
				Return
			End If

			''pass ida -->update loan
			responseMessage = Server.HtmlDecode(Common.submitUpdateLoan(_CurrentWebsiteConfig, loanID, loanRequestXMLStr, loanResponseXML))
			responseMessage = "<div class='justify-text'><p>" & responseMessage & "</p></div>"

			If _CurrentWebsiteConfig.SubmitLoanUrl.Contains("decisionloan/2.0") Then
				'for later use: select product and display final message
				Decision_v2_Response = loanResponseXML
				DecisionMessage = responseMessage
				QualifiedProducts = getQualifyProduct2_0(loanResponseXML)
			Else
				QualifiedProduct = getQualifyProduct(loanResponseXML)
			End If

			If QualifiedProduct.Count = 0 And QualifiedProducts.Count = 0 Then
				Response.Write(responseMessage)
				Return
			End If

			Dim message As String = ""
			If _CurrentWebsiteConfig.SubmitLoanUrl.Contains("decisionloan/2.0") Then
				message = displayProduct2_0() ' display only product selection						
			Else
				message = displayProduct() + "<div class='justify-text'><p>" & responseMessage & "</p></div>"
			End If
			Response.Write(message)

		End If
	End Sub
	Private Function ProceedBookingValidation(vehicleLoanLoanObject As CVehicleLoan) As List(Of CBookingValidation)
		Dim result As New List(Of CustomList.CBookingValidation)
        If CustomListRuleList IsNot Nothing AndAlso CustomListRuleList.Any(Function(c) c.Code = "BOOKING_VALIDATION") Then
            Dim requiredParams As New Dictionary(Of String, String)
            For Each p In CustomListRuleList.Where(Function(c) c.Code = "BOOKING_VALIDATION")
                Dim lst = p.GetList(Of CBookingValidation)()
                If lst IsNot Nothing AndAlso lst.Count > 0 Then
                    For Each item In lst
                        If item.ParamInfo IsNot Nothing AndAlso item.ParamInfo.Count > 0 Then
                            For Each param In item.ParamInfo
                                If requiredParams.ContainsKey(param.Key) Then Continue For
                                requiredParams.Add(param.Key, param.Value)
                            Next
                        End If
                    Next
                End If
            Next
            Dim params As New Dictionary(Of String, Tuple(Of Object, String))
            If requiredParams.ContainsKey("locationPool") Then
                params.Add("locationPool", New Tuple(Of Object, String)(vehicleLoanLoanObject.SelectedLocationPool, "List(Of String)"))
            End If
            If requiredParams.ContainsKey("isJoint") Then
                params.Add("isJoint", New Tuple(Of Object, String)(vehicleLoanLoanObject.Applicants IsNot Nothing AndAlso vehicleLoanLoanObject.Applicants.Any(Function(p) p.HasSpouse), "Boolean"))
            End If
            If requiredParams.ContainsKey("vehicleType") Then
                params.Add("vehicleType", New Tuple(Of Object, String)(New List(Of String) From {vehicleLoanLoanObject.VehicleType}, "List(Of String)"))
            End If
            If requiredParams.ContainsKey("amountRequested") Then
                params.Add("amountRequested", New Tuple(Of Object, String)(vehicleLoanLoanObject.AmountRequested, "Double"))
			End If
			If requiredParams.ContainsKey("vehicleLoanPurpose") Then
				params.Add("vehicleLoanPurpose", New Tuple(Of Object, String)(New List(Of String) From {vehicleLoanLoanObject.LoanPurpose}, "List(Of String)"))
			End If
			If requiredParams.Any(Function(k) k.Key.StartsWith("CQ_")) Then
				If vehicleLoanLoanObject.CustomQuestionAnswers IsNot Nothing AndAlso vehicleLoanLoanObject.CustomQuestionAnswers.Count > 0 Then
					For Each validatedQuestionAnswers In vehicleLoanLoanObject.CustomQuestionAnswers
						Dim questionId = Regex.Replace(validatedQuestionAnswers.Question.Name, "\s", "_")
						If requiredParams.ContainsKey("CQ_" & questionId) AndAlso Not params.ContainsKey("CQ_" & questionId) Then
							Dim answerDataType = requiredParams.First(Function(p) p.Key = "CQ_" & questionId).Value
							If validatedQuestionAnswers.Answers.Count > 1 Then
								params.Add("CQ_" & questionId, New Tuple(Of Object, String)(validatedQuestionAnswers.Answers.Select(Function(q) q.AnswerValue).ToList(), answerDataType))
							ElseIf validatedQuestionAnswers.Answers.Count = 1 Then
								params.Add("CQ_" & questionId, New Tuple(Of Object, String)(validatedQuestionAnswers.Answers.First().AnswerValue, answerDataType))
							End If
						End If
					Next
				End If
			End If
			For Each app In vehicleLoanLoanObject.Applicants
                Dim subResult = EvaluateCustomList(Of CustomList.CBookingValidation)("BOOKING_VALIDATION", PrepareCustomListRuleParams(requiredParams, app, New Dictionary(Of String, Tuple(Of Object, String))(params)))
                If subResult IsNot Nothing AndAlso subResult.Count > 0 Then
                    result.AddRange(subResult.Where(Function(p) Not String.IsNullOrWhiteSpace(p.Name) AndAlso Not result.Any(Function(q) q.Name = p.Name)).ToList())
                End If
            Next
        End If
        If result IsNot Nothing AndAlso result.Count > 0 Then
            log.Error(result.Select(Function(p) p.Name))
        End If
        Return result
	End Function

	Private Function PrepareCustomListRuleParams(requiredParams As Dictionary(Of String, String), applicantInfo As CApplicant, ByVal params As Dictionary(Of String, Tuple(Of Object, String))) As Dictionary(Of String, Tuple(Of Object, String))
		If requiredParams.ContainsKey("age") Then
			params.Add("age", New Tuple(Of Object, String)(CType(Math.Floor(DateTime.Now.Subtract(Common.SafeDate(applicantInfo.DOB)).TotalDays / 365), Integer), "Integer"))
		End If
		If requiredParams.ContainsKey("citizenship") Then
			params.Add("citizenship", New Tuple(Of Object, String)(applicantInfo.CitizenshipStatus, "String"))
		End If
		If requiredParams.ContainsKey("employeeOfLender") Then
			params.Add("employeeOfLender", New Tuple(Of Object, String)(applicantInfo.EmployeeOfLender, "String"))
		End If
		If requiredParams.ContainsKey("employmentLength") Then
			params.Add("employmentLength", New Tuple(Of Object, String)(Common.SafeDouble(applicantInfo.txtProfessionDuration_month) / 12 + Common.SafeDouble(applicantInfo.txtProfessionDuration_year), "Double"))
		End If
		If requiredParams.ContainsKey("employmentStatus") Then
			params.Add("employmentStatus", New Tuple(Of Object, String)(applicantInfo.EmploymentStatus, "String"))
		End If
		If requiredParams.ContainsKey("memberNumber") Then
			params.Add("memberNumber", New Tuple(Of Object, String)(applicantInfo.MemberNumber, "String"))
		End If
		If requiredParams.ContainsKey("occupancyLength") Then
			params.Add("occupancyLength", New Tuple(Of Object, String)(Common.SafeDouble(applicantInfo.OccupancyDuration / 12), "Double"))
		End If
		If requiredParams.ContainsKey("occupancyStatus") Then
			params.Add("occupancyStatus", New Tuple(Of Object, String)(applicantInfo.OccupancyType, "String"))
		End If

		If requiredParams.Any(Function(k) k.Key.StartsWith("AQ_")) Then
			If applicantInfo.ApplicantQuestionAnswers IsNot Nothing AndAlso applicantInfo.ApplicantQuestionAnswers.Count > 0 Then
				For Each validatedQuestionAnswer In applicantInfo.ApplicantQuestionAnswers
					Dim questionId = Regex.Replace(validatedQuestionAnswer.Question.Name, "\s", "_")
					If requiredParams.ContainsKey("AQ_" & questionId) AndAlso Not params.ContainsKey("AQ_" & questionId) Then
						Dim answerDataType = requiredParams.First(Function(p) p.Key = "AQ_" & questionId).Value
						If validatedQuestionAnswer.Answers.Count > 1 Then
							params.Add("AQ_" & questionId, New Tuple(Of Object, String)(validatedQuestionAnswer.Answers.Select(Function(q) q.AnswerValue).ToList(), answerDataType))
						ElseIf validatedQuestionAnswer.Answers.Count = 1 Then
							params.Add("AQ_" & questionId, New Tuple(Of Object, String)(validatedQuestionAnswer.Answers.First().AnswerValue, answerDataType))
						End If

					End If
				Next
			End If
		End If

		For Each pa In requiredParams.Where(Function(p) Not params.ContainsKey(p.Key))
			params.Add(pa.Key, New Tuple(Of Object, String)(Nothing, pa.Value))
		Next
		Return params
	End Function
	Private Function idaResponseIDs(ByVal idaType As String) As List(Of String)
		Dim responseIDList As New List(Of String)
		Select Case idaType.ToUpper()
			Case "PID"
				responseIDList.Add(idaType)
				responseIDList.Add(loanID)
				responseIDList.Add(transaction_ID)
				''add more cases later
		End Select
		Return responseIDList
	End Function

	Private Function GetVehicleLoanObject(ByVal poConfig As CWebsiteConfig) As CVehicleLoan
		Dim vehicleLoan As New CVehicleLoan(poConfig.OrganizationId, poConfig.LenderId)
		Dim declarationList = Common.GetActiveDeclarationList(_CurrentWebsiteConfig, "VEHICLE_LOAN")
		If Not String.IsNullOrEmpty(Request.Params("SelectedLocationPool")) Then
			vehicleLoan.SelectedLocationPool = (New JavaScriptSerializer()).Deserialize(Of List(Of String))(Request.Params("SelectedLocationPool"))
		End If
		With vehicleLoan

			.ReferralSource = Common.SafeString(Request.Form("ReferralSource"))

			''indirect, dealerapp, 
			'TODO: may need to change this to the same format as merchant
			If .ReferralSource = "" AndAlso Common.SafeStripHtmlString(Request.Form("VendorId")) <> "" Then
				.ReferralSource = "DealerApp"
			End If



			.HasDebtCancellation = Request.Form("MemberProtectionPlan") = "Y"
			.LoanPurpose = Common.SafeString(Request.Form("LoanPurpose"))
			.LoanTerm = Common.SafeInteger(Request.Form("LoanTerm"))
			.VehicleValue = Common.SafeDouble(Request.Form("VehicleValue"))
			.DownPayment = Common.SafeDouble(Request.Form("DownPayment"))
			.AmountRequested = Common.SafeDouble(Request.Form("ProposedLoanAmount"))
			.VehicleType = Common.SafeString(Request.Form("VehicleType"))

			'Fix AmountRequest when it is hidden and has zero value
			If .AmountRequested = 0.0 And .VehicleValue > 0 Then
				.AmountRequested = .VehicleValue
			End If

			.KnowVehicleMake = Common.SafeString(Request.Form("KnowVehicleMake")) = "Y"
			'If .KnowVehicleMake = True Then ' refinance doesn't have this button but the info still need to be passed to LPQ
			.VehicleMake = Common.SafeStripHtmlString(Request.Form("VehicleMake"))
			.VehicleModel = Common.SafeStripHtmlString(Request.Form("VehicleModel"))
			.VehicleYear = Common.SafeInteger(Request.Form("VehicleYear"))
			.VehicleVin = Common.SafeStripHtmlString(Request.Form("VinNumber"))

			'End If

			.IsNewVehicle = Common.SafeString(Request.Form("IsNewVehicle")) = "Y"
			If (Request.Form("VehicleMileage") = "") Then
				.VehicleMileage = 0
			Else
				.VehicleMileage = IIf(.IsNewVehicle, 0, Common.SafeString(Request.Form("VehicleMileage")))
			End If

			.HasTradeIn = (Common.SafeString(Request.Form("HasTradeIn")).ToUpper() = "Y")
			If .HasTradeIn Then
				.TradeMake = Common.SafeString(Request.Form("TradeMake"))
				.TradeYear = Common.SafeInteger(Request.Form("TradeYear"))
				.TradeModel = Common.SafeString(Request.Form("TradeModel"))
				.TradeType = Common.SafeString(Request.Form("TradeType"))
				.TradePayOff = Common.SafeDouble(Request.Form("TradePayOff"))
				.TradePayment = Common.SafeDouble(Request.Form("TradePayment"))
				.TradeValue = Common.SafeDouble(Request.Form("TradeValue"))
			End If


			.BranchID = Request.Form("BranchID")
			.LoanOfficerID = Common.SafeString(Request.Form("LoanOfficerID"))   'come from htnl mark page via js

			.IsSSO = IsSSO
			.IsComboMode = IsComboMode
			'behavior
			Dim oBehahiorDic = Common.getBehaviorNode(poConfig)
			If oBehahiorDic.ContainsKey("doc_stamps_fee_is_manual") AndAlso oBehahiorDic("doc_stamps_fee_is_manual").Trim() <> "" Then
				.DocStampsFeeIsManual = oBehahiorDic("doc_stamps_fee_is_manual").Trim()
			End If

			.isCQNewAPI = Common.getVisibleAttribute(_CurrentWebsiteConfig, "custom_question_new_api") = "Y"
		End With

		Dim jsSerializer As New JavaScriptSerializer()
		' Only get Application answers, not Applicant
		Dim lstApplicationCustomAnswers As List(Of CApplicantQA) =
			jsSerializer.Deserialize(Of List(Of CApplicantQA))(Request.Form("CustomAnswers")).
			Where(Function(cqa) Not String.IsNullOrEmpty(cqa.CQAnswer) AndAlso cqa.CQRole = QuestionRole.Application).ToList()
		' Collect URL Parameter CQ Answers, if applicable
		Dim bIsUrlParaCustomQuestion = Common.SafeString(Request.Form("IsUrlParaCustomQuestion")) = "Y"
		Dim lstUrlParaCustomAnswers As New List(Of CApplicantQA)
		If bIsUrlParaCustomQuestion Then
			lstUrlParaCustomAnswers = jsSerializer.Deserialize(Of List(Of CApplicantQA))(Common.SafeString(Request.Form("UrlParaCustomQuestionAndAnswers")))
		End If
		' Set validated CQ answers
		vehicleLoan.CustomQuestionAnswers = CCustomQuestionNewAPI.
			getValidatedCustomQuestionAnswers(_CurrentWebsiteConfig, "VL", QuestionRole.Application, "", lstApplicationCustomAnswers, lstUrlParaCustomAnswers.Where(Function(a) a.CQRole = QuestionRole.Application)).ToList()

		vehicleLoan.isDisagreeSelect = Request.Form("isDisagreeSelect")
		Dim cancelSubmit As String = Request.Form("isDisagreeSelect")
		''----user agent, disclosure and IPAddress for internal comment
		If Not String.IsNullOrEmpty(Request.Form("Disclosure")) Then
			vehicleLoan.Disclosure = Request.Form("Disclosure")
		End If

		vehicleLoan.IPAddress = Request.UserHostAddress
		vehicleLoan.UserAgent = Request.ServerVariables("HTTP_USER_AGENT")
		vehicleLoan.xSellCommentMessage = Common.SafeString(Request.Form("xaXSellComment"))
		vehicleLoan.InstaTouchPrefillComment = Common.SafeString(Request.Form("InstaTouchPrefillComment"))
		Dim params = Request.Params
		If Is2ndLoanApp AndAlso String.IsNullOrEmpty(Common.SafeString(Request.Form("SID"))) = False Then
			Dim _sid = Common.SafeString(Request.Form("SID"))
			If Session(_sid) IsNot Nothing Then
				params = CType(Session(_sid), NameValueCollection)
				vehicleLoan.xSellCommentMessage = Common.SafeString(Session("xSell" & _sid))
			End If
		End If

		'===all upload document
		Dim oSelectDocsList As New List(Of selectDocuments)
		Dim oSerializer As New JavaScriptSerializer()
		Dim oDocsList As New Dictionary(Of String, String)
		''by default MaxJsonLenth = 102400, set it =int32.maxvalue = 2147483647 
		oSerializer.MaxJsonLength = Int32.MaxValue
		If Not String.IsNullOrEmpty(params("Image")) Then
			oSelectDocsList = oSerializer.Deserialize(Of List(Of selectDocuments))(params("Image"))
			If oSelectDocsList.Count > 0 Then
				Dim i As Integer
				For i = 0 To oSelectDocsList.Count - 1
					Dim oTitle As String = oSelectDocsList(i).title
					Dim obase64data As String = oSelectDocsList(i).base64data
					oDocsList.Add(oTitle, obase64data)
				Next
				If oDocsList.Count = 0 Then
					CPBLogger.logInfo("Can't de-serialize image ")
				End If
			End If
			vehicleLoan.DocBase64Dic = oDocsList
		End If
		''upload document info(title,doc_group, doc_code) if it exist
		Dim oUploadDocumentInfoList As New List(Of uploadDocumentInfo)
		If Not String.IsNullOrEmpty(params("UploadDocInfor")) Then
			Dim oDocInfoSerializer As New JavaScriptSerializer()
			oUploadDocumentInfoList = oDocInfoSerializer.Deserialize(Of List(Of uploadDocumentInfo))(params("UploadDocInfor"))
		End If
		vehicleLoan.UploadDocumentInfoList = oUploadDocumentInfoList
		vehicleLoan.Applicants = New List(Of CApplicant)
		vehicleLoan.CoApplicantType = poConfig.coApplicantType
		Dim hasCoApp As Boolean = params("HasCoApp") = "Y"
		'Dim coAppJoint As Boolean = params("CoAppJoin") = "Y"

		IsJointApplication = hasCoApp ''use for second call back

		''cuna question answers
		Dim cunaQuestionAnswers As New List(Of String)
		If Not String.IsNullOrEmpty(Request.Form("cunaQuestionAnswers")) Then
			cunaQuestionAnswers = New JavaScriptSerializer().Deserialize(Of List(Of String))(Request.Form("cunaQuestionAnswers"))
		End If
		vehicleLoan.cunaQuestionAnswers = cunaQuestionAnswers


		''get current applicant question from download
		If vehicleLoan.isCQNewAPI Then
			vehicleLoan.DownloadedAQs = CCustomQuestionNewAPI.getDownloadedCustomQuestions(_CurrentWebsiteConfig, True, "VL")
		End If

		Dim appSpouseInfo = Common.SafeString(Request.Form("AppSpouseInfo"))
		If Not String.IsNullOrEmpty(appSpouseInfo) Then
			vehicleLoan.AppNSS = JsonConvert.DeserializeObject(Of CNSSInfo)(appSpouseInfo)
		End If
		Dim coAppSpouseInfo = Common.SafeString(Request.Form("co_AppSpouseInfo"))
		If Not String.IsNullOrEmpty(coAppSpouseInfo) Then
			vehicleLoan.CoAppNSS = JsonConvert.DeserializeObject(Of CNSSInfo)(coAppSpouseInfo)
		End If
		Dim applicant As New CApplicant()
		With applicant
			.EmployeeOfLender = Common.SafeString(params("EmployeeOfLender"))
			.FirstName = Common.SafeString(params("FirstName"))
			.MiddleInitial = Common.SafeString(params("MiddleName"))
			.LastName = Common.SafeString(params("LastName"))
			.Suffix = Common.SafeString(params("NameSuffix"))
			.SSN = Common.SafeString(params("SSN"))
			.DOB = Common.SafeString(params("DOB"))
			If vehicleLoan.AppNSS IsNot Nothing Then
				vehicleLoan.AppNSS.MarriedTo = String.Format("{0} {1}", .FirstName, .LastName)
			End If
			''reference information
			If Not String.IsNullOrEmpty(Common.SafeString(params("referenceInfo"))) Then
				.referenceInfor = New JavaScriptSerializer().Deserialize(Of List(Of String))(params("referenceInfo"))
			End If

			''driver license info
			If Not String.IsNullOrEmpty(params("IDCardNumber")) Then
				.IDNumber = Common.SafeString(params("IDCardNumber"))
				.IDType = Common.SafeString(params("IDCardType"))
				.IDState = Common.SafeString(params("IDState"))
				.IDCountry = Common.SafeString(params("IDCountry"))
				.IDDateIssued = Common.SafeString(params("IDDateIssued"))
				.IDExpirationDate = Common.SafeString(params("IDDateExpire"))
			End If
			.MemberNumber = Common.SafeString(params("MemberNumber"))
			.MaritalStatus = Common.SafeString(params("MaritalStatus"))
			.MembershipLengthMonths = Common.SafeString(Request.Form("MembershipLengthMonths"))
			.CitizenshipStatus = Common.SafeString(params("CitizenshipStatus"))
			.HomePhone = Common.SafeString(params("HomePhone"))
			.HomePhoneCountry = Common.SafeString(params("HomePhoneCountry"))
			.WorkPhone = Common.SafeString(params("WorkPhone"))
			.WorkPhoneCountry = Common.SafeString(params("WorkPhoneCountry"))
			.WorkPhoneEXT = Common.SafeString(params("WorkPhoneEXT"))
			.CellPhone = Common.SafeString(params("MobilePhone"))
			.CellPhoneCountry = Common.SafeString(params("MobilePhoneCountry"))
			.Email = Common.SafeString(params("EmailAddr"))
			.PreferredContactMethod = Common.SafeString(params("ContactMethod"))
			.CurrentAddress = Common.SafeString(params("AddressStreet"))
			.CurrentAddress2 = Common.SafeString(params("AddressStreet2"))
			.CurrentZip = Common.SafeString(params("AddressZip"))
			.CurrentCity = Common.SafeString(params("AddressCity"))
			.CurrentState = Common.SafeString(params("AddressState"))
			.CurrentCountry = Common.SafeString(params("Country"))
			.OccupancyType = Common.SafeString(params("OccupyingLocation"))
			.OccupancyDuration = Common.SafeInteger(params("LiveMonths"))
			.OccupancyDescription = Common.SafeString(params("OccupancyDescription"))

			''previous Address
			.HasPreviousAddress = Common.SafeString(params("hasPreviousAddress"))
			.PreviousAddress = Common.SafeString(params("PreviousAddressStreet"))
			.PreviousZip = Common.SafeString(params("PreviousAddressZip"))
			.PreviousCity = Common.SafeString(params("PreviousAddressCity"))
			.PreviousState = Common.SafeString(params("PreviousAddressState"))
			.PreviousCountry = Common.SafeString(params("PreviousAddressCountry"))
			''end previous Address

			''mailing address
			Dim hasMailingAddress As String = params("hasMailingAddress")
			If Not String.IsNullOrEmpty(hasMailingAddress) Then
				If hasMailingAddress = "Y" Then
					.HasMailingAddress = "Y"
					.MailingAddress = params("MailingAddressStreet")
					.MailingAddress2 = params("MailingAddressStreet2")
					.MailingCity = params("MailingAddressCity")
					.MailingState = params("MailingAddressState")
					.MailingZip = params("MailingAddressZip")
					.MailingCountry = params("MailingAddressCountry")
				Else
					.MailingAddress = ""
					.MailingAddress2 = ""
					.MailingCity = ""
					.MailingState = ""
					.MailingZip = ""
					.MailingCountry = ""
				End If
			End If

			.EmploymentStatus = Common.SafeString(params("EmploymentStatus"))
			.EmploymentDescription = Common.SafeString(params("EmploymentDescription"))
			''If .EmploymentStatus = "STUDENT" Or .EmploymentStatus = "RETIRED" Or .EmploymentStatus = "HOMEMAKER" Or .EmploymentStatus = "UNEMPLOYED" Then
			''    .txtJobTitle = .EmploymentStatus
			''    .txtEmployedDuration_month = Common.SafeString(params("txtEmployedDuration_month"))
			''    .txtEmployedDuration_year = Common.SafeString(params("txtEmployedDuration_year"))
			''Else
			.txtJobTitle = Common.SafeString(params("txtJobTitle"))
			.txtEmployedDuration_month = Common.SafeString(params("txtEmployedDuration_month"))
			.txtEmployedDuration_year = Common.SafeString(params("txtEmployedDuration_year"))
			.txtEmployer = Common.SafeString(params("txtEmployer"))
			''End If
			' new employment logic
			.txtBusinessType = Common.SafeString(params("txtBusinessType"))

			.txtEmploymentStartDate = Common.SafeString(params("txtEmploymentStartDate"))
			.txtETS = Common.SafeString(params("txtETS"))
			.txtProfessionDuration_month = Common.SafeString(params("txtProfessionDuration_month"))
			.txtProfessionDuration_year = Common.SafeString(params("txtProfessionDuration_year"))
			.txtSupervisorName = Common.SafeString(params("txtSupervisorName"))
			.ddlBranchOfService = Common.SafeString(params("ddlBranchOfService"))
			.ddlPayGrade = Common.SafeString(params("ddlPayGrade"))
			' end new employment logic

			.GrossMonthlyIncome = Common.SafeDouble(params("GrossMonthlyIncome"))
			'other monthly income
			.GrossMonthlyIncomeOther = Common.SafeDouble(params("OtherMonthlyIncome"))
			.MonthlyIncomeOtherDescription = Common.SafeString(params("OtherMonthlyIncomeDesc"))

			''tax exempt
			.GrossMonthlyIncomeTaxExempt = Common.SafeString(params("GrossMonthlyIncomeTaxExempt"))
			.OtherMonthlyIncomeTaxExempt = Common.SafeString(params("OtherMonthlyIncomeTaxExempt"))

			.TotalMonthlyHousingExpense = Common.SafeDouble(params("TotalMonthlyHousingExpense"))

			''previous employment information
			.hasPrevEmployment = Common.SafeString(params("hasPreviousEmployment"))
			.prev_EmploymentStatus = Common.SafeString(params("prev_EmploymentStatus"))
			If .prev_EmploymentStatus = "STUDENT" Or .prev_EmploymentStatus = "RETIRED" Or .prev_EmploymentStatus = "HOMEMAKER" Or .prev_EmploymentStatus = "UNEMPLOYED" Then
				.prev_txtJobTitle = .prev_EmploymentStatus
				.prev_txtEmployedDuration_month = Common.SafeString(params("prev_txtEmployedDuration_month"))
				.prev_txtEmployedDuration_year = Common.SafeString(params("prev_txtEmployedDuration_year"))
			Else
				.prev_txtJobTitle = Common.SafeString(params("prev_txtJobTitle"))
				.prev_txtEmployedDuration_month = Common.SafeString(params("prev_txtEmployedDuration_month"))
				.prev_txtEmployedDuration_year = Common.SafeString(params("prev_txtEmployedDuration_year"))
				.prev_txtEmployer = Common.SafeString(params("prev_txtEmployer"))
			End If
			.prev_txtBusinessType = Common.SafeString(params("prev_txtBusinessType"))
			.prev_txtEmploymentStartDate = Common.SafeString(params("prev_txtEmploymentStartDate"))
			.prev_txtETS = Common.SafeString(params("prev_txtETS"))
			.prev_ddlBranchOfService = Common.SafeString(params("prev_ddlBranchOfService"))
			.prev_ddlPayGrade = Common.SafeString(params("prev_ddlPayGrade"))
			.prev_GrossMonthlyIncome = Common.SafeDouble(params("prev_GrossMonthlyIncome"))
			''end previous employment information

			'Neighborhood CU  customization
			If (_CurrentLenderRef.ToUpper.StartsWith("NCU_TEST") Or _CurrentLenderRef.ToUpper.StartsWith("NCU103114")) And .TotalMonthlyHousingExpense < 0.01 Then
				.TotalMonthlyHousingExpense = 500
			End If

			'Uncle CU  customization
			If _CurrentLenderRef.ToUpper.StartsWith("UNCLECU") And .TotalMonthlyHousingExpense < 600 And (.OccupancyType = "LIVE WITH PARENTS" Or .OccupancyType = "RENT" Or .OccupancyType = "OTHER") Then
				.TotalMonthlyHousingExpense = 600
			End If

			'Municipal CU  customization
			If (_CurrentLenderRef.ToUpper.StartsWith("MCU_TEST") Or _CurrentLenderRef.ToUpper.StartsWith("MCU090115")) And .TotalMonthlyHousingExpense < 600 And (.OccupancyType = "LIVE WITH PARENTS" Or .OccupancyType = "OTHER") Then
				.TotalMonthlyHousingExpense = 600
			End If

            .HasSpouse = hasCoApp
            Dim assetInfoJsonStr = Common.SafeString(Request.Form("Assets"))
            If Not String.IsNullOrWhiteSpace(assetInfoJsonStr) Then
                .Assets = JsonConvert.DeserializeObject(Of List(Of CAssetInfo))(assetInfoJsonStr)
            End If

            'declaration
            If Not String.IsNullOrEmpty(Common.SafeString(Request.Form("Declarations"))) AndAlso declarationList IsNot Nothing AndAlso declarationList.Count > 0 Then
				Dim declarationResult = JsonConvert.DeserializeObject(Of List(Of KeyValuePair(Of String, Boolean)))(Request.Form("Declarations"))
				If declarationResult IsNot Nothing AndAlso declarationResult.Count > 0 Then
					.Declarations = (From re In declarationResult
									Join en In declarationList
									On re.Key Equals en.Key
									Select re).ToList()
					If Not String.IsNullOrEmpty(Common.SafeString(Request.Form("AdditionalDeclarations"))) Then
						.AdditionalDeclarations = JsonConvert.DeserializeObject(Of Dictionary(Of String, String))(Request.Form("AdditionalDeclarations"))
					End If
				End If
			End If
			''Government Monitoring Information
			.HasGMI = Common.SafeString(params("HasGMI"))
			If .HasGMI <> "N" Then
				.SexNotProvided = Common.SafeString(Request.Form("SexNotProvided"))
				.Sex = Common.SafeString(Request.Form("Sex"))
				.EthnicityNotProvided = Common.SafeString(Request.Form("EthnicityNotProvided"))
				.EthnicityIsHispanic = Common.SafeString(Request.Form("EthnicityIsHispanic"))
				.EthnicityIsNotHispanic = Common.SafeString(Request.Form("EthnicityIsNotHispanic"))
				.EthnicityHispanic = Common.SafeString(Request.Form("EthnicityHispanic"))
				.EthnicityHispanicOther = Common.SafeString(Request.Form("EthnicityHispanicOther"))
				.RaceNotProvided = Common.SafeString(Request.Form("RaceNotProvided"))
				.RaceBase = Common.SafeString(Request.Form("RaceBase"))
				.RaceAsian = Common.SafeString(Request.Form("RaceAsian"))
				.RaceAsianOther = Common.SafeString(Request.Form("RaceAsianOther"))
				.RacePacificIslander = Common.SafeString(Request.Form("RacePacificIslander"))
				.RacePacificIslanderOther = Common.SafeString(Request.Form("RacePacificIslanderOther"))
				.RaceTribeName = Common.SafeString(Request.Form("RaceTribeName"))
			End If

			' Applicant custom question answers 
			Dim lstApplicantCustomAnswers As List(Of CApplicantQA) =
				jsSerializer.Deserialize(Of List(Of CApplicantQA))(Request.Form("CustomAnswers")).
				Where(Function(cqa) Not String.IsNullOrEmpty(cqa.CQAnswer) AndAlso cqa.CQRole = QuestionRole.Applicant AndAlso IIf(cqa.CQLocation = CustomQuestionLocation.ApplicantPage, cqa.CQApplicantPrefix = "", True)).ToList()
			' Set validated CQ answers
			.ApplicantQuestionAnswers = CCustomQuestionNewAPI.
				getValidatedCustomQuestionAnswers(_CurrentWebsiteConfig, "VL", QuestionRole.Applicant, "", lstApplicantCustomAnswers, lstUrlParaCustomAnswers.Where(Function(urlQA) urlQA.CQRole = QuestionRole.Applicant))

		End With

		vehicleLoan.Applicants.Add(applicant)

		If hasCoApp Then
			Dim coApplicant As New CApplicant()
			With coApplicant


				.EmployeeOfLender = Common.SafeString(params("co_EmployeeOfLender"))
				.FirstName = Common.SafeString(params("co_FirstName"))
				.MiddleInitial = Common.SafeString(params("co_MiddleName"))
				.LastName = Common.SafeString(params("co_LastName"))
				.SSN = Common.SafeString(params("co_SSN"))
				.Suffix = Common.SafeString(params("co_NameSuffix"))
				.DOB = Common.SafeString(params("co_DOB"))
				If vehicleLoan.CoAppNSS IsNot Nothing Then
					vehicleLoan.CoAppNSS.MarriedTo = String.Format("{0} {1}", .FirstName, .LastName)
				End If
				''reference information
				If Not String.IsNullOrEmpty(Common.SafeString(params("co_referenceInfo"))) Then
					.referenceInfor = New JavaScriptSerializer().Deserialize(Of List(Of String))(params("co_referenceInfo"))
				End If

				''identification info
				If Not String.IsNullOrEmpty(params("co_IDCardNumber")) Then
					.IDNumber = Common.SafeString(params("co_IDCardNumber"))
					.IDType = Common.SafeString(params("co_IDCardType"))
					.IDState = Common.SafeString(params("co_IDState"))
					.IDCountry = Common.SafeString(params("co_IDCountry"))
					.IDDateIssued = Common.SafeString(params("co_IDDateIssued"))
					.IDExpirationDate = Common.SafeString(params("co_IDDateExpire"))
				End If
				.MemberNumber = Common.SafeString(params("co_MemberNumber"))
				.RelationshipToPrimary = Common.SafeString(Request.Form("co_RelationshipToPrimary"))
				.MaritalStatus = Common.SafeString(params("co_MaritalStatus"))
				.MembershipLengthMonths = Common.SafeString(Request.Form("co_MembershipLengthMonths"))
				.CitizenshipStatus = Common.SafeString(params("co_CitizenshipStatus"))
				.HomePhone = Common.SafeString(params("co_HomePhone"))
				.HomePhoneCountry = Common.SafeString(params("co_HomePhoneCountry"))
				.WorkPhone = Common.SafeString(params("co_WorkPhone"))
				.WorkPhoneCountry = Common.SafeString(params("co_WorkPhoneCountry"))
				.WorkPhoneEXT = Common.SafeString(params("co_WorkPhoneEXT"))
				.CellPhone = Common.SafeString(params("co_MobilePhone"))
				.CellPhoneCountry = Common.SafeString(params("co_MobilePhoneCountry"))
				.Email = params("co_EmailAddr")
				.PreferredContactMethod = Common.SafeString(params("co_ContactMethod"))
				.CurrentAddress = Common.SafeString(params("co_AddressStreet"))
				.CurrentAddress2 = Common.SafeString(params("co_AddressStreet2"))
				.CurrentZip = Common.SafeString(params("co_AddressZip"))
				.CurrentCity = Common.SafeString(params("co_AddressCity"))
				.CurrentState = Common.SafeString(params("co_AddressState"))
				.CurrentCountry = Common.SafeString(params("co_Country"))
				.OccupancyType = Common.SafeString(params("co_OccupyingLocation"))
				.OccupancyDuration = Common.SafeInteger(params("co_LiveMonths"))
				.OccupancyDescription = Common.SafeString(params("co_OccupancyDescription"))

				''previous Address
				.HasPreviousAddress = Common.SafeString(params("co_hasPreviousAddress"))
				.PreviousAddress = Common.SafeString(params("co_PreviousAddressStreet"))
				.PreviousZip = Common.SafeString(params("co_PreviousAddressZip"))
				.PreviousCity = Common.SafeString(params("co_PreviousAddressCity"))
				.PreviousState = Common.SafeString(params("co_PreviousAddressState"))
				.PreviousCountry = Common.SafeString(params("co_PreviousAddressCountry"))
				''end previous Address

				''mailing address
				Dim hasMailingAddress As String = params("co_hasMailingAddress")
				If Not String.IsNullOrEmpty(hasMailingAddress) Then
					If hasMailingAddress = "Y" Then
						.HasMailingAddress = "Y"
						.MailingAddress = params("co_MailingAddressStreet")
						.MailingAddress2 = params("co_MailingAddressStreet2")
						.MailingCity = params("co_MailingAddressCity")
						.MailingState = params("co_MailingAddressState")
						.MailingZip = params("co_MailingAddressZip")
						.MailingCountry = params("co_MailingAddressCountry")
					Else
						.MailingAddress = ""
						.MailingAddress2 = ""
						.MailingCity = ""
						.MailingState = ""
						.MailingZip = ""
						.MailingCountry = ""
					End If
				End If

				.EmploymentStatus = Common.SafeString(params("co_EmploymentStatus"))
				.EmploymentDescription = Common.SafeString(params("co_EmploymentDescription"))
				''If .EmploymentStatus = "STUDENT" Or .EmploymentStatus = "RETIRED" Or .EmploymentStatus = "HOMEMAKER" Or .EmploymentStatus = "UNEMPLOYED" Then
				''    .txtJobTitle = .EmploymentStatus
				''    .txtEmployedDuration_month = Common.SafeString(params("co_txtEmployedDuration_month"))
				''    .txtEmployedDuration_year = Common.SafeString(params("co_txtEmployedDuration_year"))
				''Else
				.txtJobTitle = Common.SafeString(params("co_txtJobTitle"))
				.txtEmployedDuration_month = Common.SafeString(params("co_txtEmployedDuration_month"))
				.txtEmployedDuration_year = Common.SafeString(params("co_txtEmployedDuration_year"))
				.txtEmployer = Common.SafeString(params("co_txtEmployer"))
				''End If
				' new employment logic
				.txtBusinessType = Common.SafeString(params("co_txtBusinessType"))
				.txtEmploymentStartDate = Common.SafeString(params("co_txtEmploymentStartDate"))
				.txtETS = Common.SafeString(params("co_txtETS"))
				.txtProfessionDuration_month = Common.SafeString(params("co_txtProfessionDuration_month"))
				.txtProfessionDuration_year = Common.SafeString(params("co_txtProfessionDuration_year"))
				.txtSupervisorName = Common.SafeString(params("co_txtSupervisorName"))
				.ddlBranchOfService = Common.SafeString(params("co_ddlBranchOfService"))
				.ddlPayGrade = Common.SafeString(params("co_ddlPayGrade"))
				' end new employment logic

				.GrossMonthlyIncome = Common.SafeDouble(params("co_GrossMonthlyIncome"))
				'other monthly income
				.GrossMonthlyIncomeOther = Common.SafeDouble(params("co_OtherMonthlyIncome"))
				.MonthlyIncomeOtherDescription = Common.SafeString(params("co_OtherMonthlyIncomeDesc"))

				''tax exempt
				.GrossMonthlyIncomeTaxExempt = Common.SafeString(params("co_GrossMonthlyIncomeTaxExempt"))
				.OtherMonthlyIncomeTaxExempt = Common.SafeString(params("co_OtherMonthlyIncomeTaxExempt"))

				.TotalMonthlyHousingExpense = Common.SafeDouble(params("co_TotalMonthlyHousingExpense"))

				''previous employment information
				.hasPrevEmployment = Common.SafeString(params("co_hasPreviousEmployment"))
				.prev_EmploymentStatus = Common.SafeString(params("co_prev_EmploymentStatus"))
				If .prev_EmploymentStatus = "STUDENT" Or .prev_EmploymentStatus = "RETIRED" Or .prev_EmploymentStatus = "HOMEMAKER" Or .prev_EmploymentStatus = "UNEMPLOYED" Then
					.prev_txtJobTitle = .prev_EmploymentStatus
					.prev_txtEmployedDuration_month = Common.SafeString(params("co_prev_txtEmployedDuration_month"))
					.prev_txtEmployedDuration_year = Common.SafeString(params("co_prev_txtEmployedDuration_year"))
				Else
					.prev_txtJobTitle = Common.SafeString(params("co_prev_txtJobTitle"))
					.prev_txtEmployedDuration_month = Common.SafeString(params("co_prev_txtEmployedDuration_month"))
					.prev_txtEmployedDuration_year = Common.SafeString(params("co_prev_txtEmployedDuration_year"))
					.prev_txtEmployer = Common.SafeString(params("co_prev_txtEmployer"))
				End If
				.prev_txtBusinessType = Common.SafeString(params("co_prev_txtBusinessType"))
				.prev_txtEmploymentStartDate = Common.SafeString(params("co_prev_txtEmploymentStartDate"))
				.prev_txtETS = Common.SafeString(params("co_prev_txtETS"))
				.prev_ddlBranchOfService = Common.SafeString(params("co_prev_ddlBranchOfService"))
				.prev_ddlPayGrade = Common.SafeString(params("co_prev_ddlPayGrade"))
				.prev_GrossMonthlyIncome = Common.SafeDouble(params("co_prev_GrossMonthlyIncome"))
                ''end previous employment information
                Dim coAssetInfoJsonStr = Common.SafeString(Request.Form("co_Assets"))
                If Not String.IsNullOrWhiteSpace(coAssetInfoJsonStr) Then
                    .Assets = JsonConvert.DeserializeObject(Of List(Of CAssetInfo))(coAssetInfoJsonStr)
                End If
                '' Co/Joint-Declarations
                If Not String.IsNullOrEmpty(Common.SafeString(Request.Form("co_Declarations"))) AndAlso declarationList IsNot Nothing AndAlso declarationList.Count > 0 Then
					Dim declarationResult = JsonConvert.DeserializeObject(Of List(Of KeyValuePair(Of String, Boolean)))(Request.Form("co_Declarations"))
					If declarationResult IsNot Nothing AndAlso declarationResult.Count > 0 Then
						.Declarations = (From re In declarationResult
										Join en In declarationList
										On re.Key Equals en.Key
										Select re).ToList()
						If Not String.IsNullOrEmpty(Common.SafeString(Request.Form("co_AdditionalDeclarations"))) Then
							.AdditionalDeclarations = JsonConvert.DeserializeObject(Of Dictionary(Of String, String))(Request.Form("co_AdditionalDeclarations"))
						End If
					End If
				End If
				'Neighborhood CU  customization
				If (_CurrentLenderRef.ToUpper.StartsWith("NCU_TEST") Or _CurrentLenderRef.ToUpper.StartsWith("NCU103114")) And .TotalMonthlyHousingExpense < 0.01 Then
					.TotalMonthlyHousingExpense = 500
				End If

				'Uncle CU  customization
				If _CurrentLenderRef.ToUpper.StartsWith("UNCLECU") And .TotalMonthlyHousingExpense < 600 And (.OccupancyType = "LIVE WITH PARENTS" Or .OccupancyType = "RENT" Or .OccupancyType = "OTHER") Then
					.TotalMonthlyHousingExpense = 600
				End If

				'Municipal CU  customization
				If (_CurrentLenderRef.ToUpper.StartsWith("MCU_TEST") Or _CurrentLenderRef.ToUpper.StartsWith("MCU090115")) And .TotalMonthlyHousingExpense < 600 And (.OccupancyType = "LIVE WITH PARENTS" Or .OccupancyType = "OTHER") Then
					.TotalMonthlyHousingExpense = 600
				End If

				''Government Monitoring Information
				.HasGMI = Common.SafeString(params("co_HasGMI"))
				If .HasGMI <> "N" Then
					.SexNotProvided = Common.SafeString(Request.Form("co_SexNotProvided"))
					.Sex = Common.SafeString(Request.Form("co_Sex"))
					.EthnicityNotProvided = Common.SafeString(Request.Form("co_EthnicityNotProvided"))
					.EthnicityIsHispanic = Common.SafeString(Request.Form("co_EthnicityIsHispanic"))
					.EthnicityIsNotHispanic = Common.SafeString(Request.Form("co_EthnicityIsNotHispanic"))
					.EthnicityHispanic = Common.SafeString(Request.Form("co_EthnicityHispanic"))
					.EthnicityHispanicOther = Common.SafeString(Request.Form("co_EthnicityHispanicOther"))
					.RaceNotProvided = Common.SafeString(Request.Form("co_RaceNotProvided"))
					.RaceBase = Common.SafeString(Request.Form("co_RaceBase"))
					.RaceAsian = Common.SafeString(Request.Form("co_RaceAsian"))
					.RaceAsianOther = Common.SafeString(Request.Form("co_RaceAsianOther"))
					.RacePacificIslander = Common.SafeString(Request.Form("co_RacePacificIslander"))
					.RacePacificIslanderOther = Common.SafeString(Request.Form("co_RacePacificIslanderOther"))
					.RaceTribeName = Common.SafeString(Request.Form("co_RaceTribeName"))

				End If

				' Applicant custom question answers 
				Dim lstApplicantCustomAnswers As List(Of CApplicantQA) =
					jsSerializer.Deserialize(Of List(Of CApplicantQA))(Request.Form("CustomAnswers")).
					Where(Function(cqa) Not String.IsNullOrEmpty(cqa.CQAnswer) AndAlso cqa.CQRole = QuestionRole.Applicant AndAlso IIf(cqa.CQLocation = CustomQuestionLocation.ApplicantPage, cqa.CQApplicantPrefix = "co_", True)).ToList()
				' Set validated CQ answers
				.ApplicantQuestionAnswers = CCustomQuestionNewAPI.
					getValidatedCustomQuestionAnswers(_CurrentWebsiteConfig, "VL", QuestionRole.Applicant, "", lstApplicantCustomAnswers, lstUrlParaCustomAnswers.Where(Function(urlQA) urlQA.CQRole = QuestionRole.Applicant))

			End With
			vehicleLoan.Applicants.Add(coApplicant)
			Co_FName = Common.SafeString(params("co_FirstName")) ''use for second call back
		End If
		vehicleLoan.VendorID = Common.SafeStripHtmlString(params("VendorId"))
		vehicleLoan.UserID = Common.SafeGUID(params("UserId"))
		Return vehicleLoan
	End Function

	Private Function SubmitVehicleLoan(vehicleLoan As CVehicleLoan, ByVal poConfig As CWebsiteConfig, ByRef poResponseRaw As String) As String
		'     CPBLogger.logInfo("Vehicle Loan: Preparing to submit vehicle loan")
		''get foreinssn if it exist in config
		Dim ForeignSSN As String = ""
		Dim co_ForeignSSN As String = ""
		Dim isSubmitLoan As Boolean = False
		If Not String.IsNullOrEmpty(_CurrentWebsiteConfig.AppType) Then
			If _CurrentWebsiteConfig.AppType.ToUpper() = "FOREIGN" Then
				ForeignSSN = Common.SafeString(Request.Form("SSN"))
				co_ForeignSSN = Common.SafeString(Request.Form("co_SSN"))
			End If
		End If
		Dim hasCoApp As Boolean = Common.SafeString(Request.Form("HasCoApp")) = "Y"
		If hasCoApp Then 'check foreignssn and co_foreignssn
			If ForeignSSN = "999999999" Or co_ForeignSSN = "999999998" Then	''no foreignssn or co_foreignssn
				isSubmitLoan = True
			End If
		Else ''only foreignssn
			If ForeignSSN = "999999999" Then
				isSubmitLoan = True
			End If
		End If


		Dim cancelSubmit As String = Request.Form("isDisagreeSelect")
		Dim loanIDAType As String = Common.SafeString(Request.Form("idaMethodType"))
		Dim disableSSOIDA = IIf(Common.SafeString(Request.Form("HasSSOIDA")) = "Y", False, IsSSO)
		If Not String.IsNullOrEmpty(loanIDAType) And Not isSubmitLoan And cancelSubmit <> "Y" And Not disableSSOIDA And Not IsComboMode Then ''only enter for mode3(decision1.0 with IDA),
			Dim submitMessage As String = ""
			Dim responseStatus As String = Common.SubmitLoan(vehicleLoan, poConfig, cancelSubmit, poResponseRaw, True, submitMessage, loanRequestXMLStr)
			If Not String.IsNullOrEmpty(submitMessage) Then
				Return submitMessage ''fail to submit return message
			End If
			'' get loanId
			loanID = Common.getResponseLoanID(poResponseRaw)
			Dim psWalletQuestions As String = ""
			Dim sName As String = Common.SafeString(Request.Form("FirstName"))
			Dim isJoint As Boolean = False
			''execute wallet question for primary
			Dim strResponse As String = Common.getWalletQuestionsResponseXML(poConfig, poResponseRaw, loanIDAType, isJoint)
			If String.IsNullOrEmpty(strResponse) Then
				Dim loanRequestXML As XmlDocument = New XmlDocument()
				loanRequestXML.LoadXml(loanRequestXMLStr)
				submitMessage = Common.WebPostResponse(poConfig, loanRequestXML, poResponseRaw, False)
				Return submitMessage ''failed to response wallet question --> return submitmessage
			End If
			Dim strRenderWalletQuestions As String = Common.renderWalletQuestionsHTML(loanIDAType, strResponse, questionList_persist, sName, transaction_ID, question_set_ID, request_app_ID, request_client_ID, request_sequence_ID, reference_number)
			''  IsJointApplication = hasCoApp ''use for second call back
			proposedLoanAmount = Common.SafeString(Request.Form("ProposedLoanAmount")) ''use for display qualify product after pass ida
			loanTerm = Common.SafeString(Request.Form("LoanTerm"))
			Return strRenderWalletQuestions

		Else ''normal process
			Return Common.SubmitLoan(vehicleLoan, poConfig, cancelSubmit, poResponseRaw, isSubmitLoan, "", loanRequestXMLStr)
		End If
	End Function
	'to be deprecate
	'  Private Function ValidateInput() As String
	'      Dim errMessage As String = String.Empty

	'Return errMessage

	'      Dim flag As Boolean = False

	'      If String.IsNullOrEmpty(Request.Form("LenderId")) And String.IsNullOrEmpty(Request.Form("LenderRef")) Then
	'          Return "LenderId is missing!"
	'      End If

	'      Dim hasPreviousAddress As String = Common.SafeString(Request.Form("hasPreviousAddress"))
	'Dim hasCoPreviousAddress As String = Common.SafeString(Request.Form("co_hasPreviousAddress"))
	'Dim hasTradeIn As String = Common.SafeString(Request.Form("HasTradeIn"))

	'      Dim isForeignAddress As Boolean = If(Common.SafeString(Request.Form("Country")).ToUpper <> "USA" And Common.SafeString(Request.Form("Country")).ToUpper <> "", True, False)
	'      Dim isCoForeignAddress As Boolean = If(Common.SafeString(Request.Form("co_Country")).ToUpper <> "USA" And Common.SafeString(Request.Form("co_Country")).ToUpper <> "", True, False)

	'      Dim hasCoApp As Boolean = Request.Form("HasCoApp") = "Y"

	'      Dim isRetired As Boolean = Request.Form("EmploymentStatus") = "RETIRED" _
	'       Or Request.Form("EmploymentStatus") = "RETIRED MILITARY" _
	'       Or Request.Form("EmploymentStatus") = "UNEMPLOYED" _
	'       Or Request.Form("EmploymentStatus") = "STUDENT" _
	'       Or Request.Form("EmploymentStatus") = "HOMEMAKER"

	'      Dim isCoRetired As Boolean = Request.Form("co_EmploymentStatus") = "RETIRED" _
	'       Or Request.Form("co_EmploymentStatus") = "RETIRED MILITARY" _
	'        Or Request.Form("co_EmploymentStatus") = "UNEMPLOYED" _
	'        Or Request.Form("co_EmploymentStatus") = "STUDENT" _
	'        Or Request.Form("co_EmploymentStatus") = "HOMEMAKER"

	'      Dim validateIDCountryRequire As Boolean = Request.Form("IDCardType") = "BIRTH_CERT" _
	'          Or Request.Form("IDCardType") = "PASSPORT" _
	'          Or Request.Form("IDCardType") = "FOREIGN_ID" _
	'          Or Request.Form("IDCardType") = "FRGN_DRVRS"

	'      Dim validateCoIDCountryRequire As Boolean = Request.Form("co_IDCardType") = "BIRTH_CERT" _
	'          Or Request.Form("co_IDCardType") = "PASSPORT" _
	'          Or Request.Form("co_IDCardType") = "FOREIGN_ID" _
	'          Or Request.Form("co_IDCardType") = "FRGN_DRVRS"

	'      For Each key As String In Request.Form.Keys
	'          If String.IsNullOrEmpty(Request.Form.Item(key)) Then
	'              Dim sField As String = key.ToLower()

	'              If sField.StartsWith("co_") AndAlso Not hasCoApp Then Continue For
	'              If sField = "platformsource" Then Continue For
	'              If sField = "lenderid" Or sField = "lenderref" Then Continue For
	'              If sField.Contains("branchid") Then Continue For
	'              If sField = "referralsource" Then Continue For
	'              If sField = "txtjobtitle" AndAlso isRetired Then Continue For 'more relax than client side
	'              If sField = "txtemployer" Then Continue For 'more relax than client side
	'              If sField = "txtemployedduration" Then Continue For 'more relax than client side
	'              If sField = "grossmonthlyincome" AndAlso isRetired Then Continue For 'more relax than client side
	'              If sField = "totalmonthlyhousingexpense" Then Continue For 'TODO(not critical): should check occupying status also

	'              If sField = "co_txtjobtitle" AndAlso isCoRetired Then Continue For
	'              If sField = "co_txtemployer" Then Continue For
	'              If sField = "co_txtemployedduration" Then Continue For
	'              If sField = "co_grossmonthlyincome" AndAlso isCoRetired Then Continue For
	'              If sField = "co_totalmonthlyhousingexpense" Then Continue For

	'              ''skip other monthly income
	'              If sField.Contains("othermonthlyincome") Then Continue For

	'              'validate Identification
	'              If sField = "idcountry" AndAlso validateIDCountryRequire = False Then Continue For
	'              If sField = "idstate" AndAlso validateIDCountryRequire Then Continue For
	'              If sField = "co_idcountry" AndAlso validateCoIDCountryRequire = False Then Continue For
	'              If sField = "co_idstate" AndAlso validateCoIDCountryRequire Then Continue For


	'              If sField = "employeeoflender" Then Continue For
	'              If sField = "co_employeeoflender" Then Continue For

	'              If sField.Contains("vehiclemake") OrElse sField.Contains("vehiclemodel") OrElse sField.Contains("vehicleyear") Then
	'                  If Request.Form("KnowVehicleMake") <> "Y" Then Continue For
	'              End If

	'              If sField.Contains("vehiclemileage") Then Continue For
	'              If sField.Contains("loanpurpose") Then Continue For
	'              If sField.Contains("middlename") _
	'              OrElse sField.Contains("driverlicense") _
	'                OrElse sField.Contains("workphone") _
	'              OrElse sField.Contains("workphoneext") _
	'              OrElse sField.Contains("mobilephone") Then Continue For
	'              If sField.Contains("namesuffix") Then Continue For
	'              If sField.Contains("idamethodtype") Then Continue For
	'              If sField.Contains("membernumber") Then Continue For
	'              If sField.Contains("txtets") Then Continue For
	'              If sField.Contains("txtemployer") OrElse _
	'               sField.Contains("txtsupervisorname") OrElse _
	'               sField.Contains("txtemploymentstartdate") OrElse _
	'               sField.Contains("txtemployedduration_year") OrElse _
	'               sField.Contains("txtprofessionduration_year") OrElse _
	'               sField.Contains("txtbusiness") OrElse _
	'                sField.Contains("ddlbranchofservice") OrElse _
	'                  sField.Contains("ddlpaygrade") OrElse _
	'               sField.Contains("co_txtbusiness") Then
	'                  Continue For
	'              End If

	'              'foreing stuff
	'              If sField.Contains("homephonecountry") Then Continue For
	'              If sField.Contains("mobilephonecountry") Then Continue For
	'              If sField.Contains("workphonecountry") Then Continue For
	'              If isForeignAddress Then ''primary
	'                  If sField.StartsWith("zip") Then Continue For
	'                  If sField.StartsWith("city") Then Continue For
	'                  If sField.StartsWith("state") Then Continue For
	'              End If
	'              If isCoForeignAddress Then ''joint
	'                  If sField.Contains("co_zip") Then Continue For
	'                  If sField.Contains("co_city") Then Continue For
	'                  If sField.Contains("co_state") Then Continue For
	'              End If

	'              If hasPreviousAddress <> "Y" Then
	'                  If sField.StartsWith("previousaddress") Then Continue For
	'              End If
	'              If hasCoPreviousAddress <> "Y" Then
	'                  If sField.StartsWith("co_previousaddress") Then Continue For
	'              End If

	'              'skip previous employment
	'              If sField.StartsWith("prev_") Then Continue For
	'              If sField.StartsWith("co_prev_") Then Continue For

	'              'skip GMI
	'              If sField.Contains("co_hasgmi") Then Continue For
	'              If sField.Contains("hasgmi") Then Continue For
	'              If sField.Contains("co_gender") Then Continue For
	'              If sField.Contains("gender") Then Continue For
	'              If sField.Contains("co_ethnicity") Then Continue For
	'              If sField.Contains("ethnicity") Then Continue For
	'              If sField.Contains("co_race") Then Continue For
	'              If sField.Contains("race") Then Continue For
	'              If sField.Contains("co_declinedansweraacegender") Then Continue For
	'              If sField.Contains("declinedansweraacegender") Then Continue For

	'              ''skip preferred contact method
	'              If sField.Contains("contactmethod") Then Continue For

	'		If sField = "occupancydescription" Then
	'			If Request.Form("occupyinglocation").ToUpper() <> "OTHER" Or CollectDescriptionIfOccupancyStatusIsOther.ToUpper() <> "Y" Then Continue For
	'		End If
	'		If sField = "co_occupancydescription" Then
	'			If Request.Form("co_occupyinglocation").ToUpper() <> "OTHER" Or CollectDescriptionIfOccupancyStatusIsOther.ToUpper() <> "Y" Then Continue For
	'		End If

	'		If sField = "employmentdescription" Then
	'			If Request.Form("employmentstatus").ToUpper() <> "OTHER" Or CollectDescriptionIfEmploymentStatusIsOther.ToUpper() <> "Y" Then Continue For
	'		End If
	'		If sField = "co_employmentdescription" Then
	'			If Request.Form("co_employmentstatus").ToUpper() <> "OTHER" Or CollectDescriptionIfEmploymentStatusIsOther.ToUpper() <> "Y" Then Continue For
	'		End If

	'		If hasTradeIn.ToUpper() <> "Y" Then
	'			If sField = "tradetype" Then Continue For
	'			If sField = "tradeyear" Then Continue For
	'			If sField = "trademodel" Then Continue For
	'			If sField = "trademake" Then Continue For
	'			If sField = "tradevalue" Then Continue For
	'			If sField = "tradepayoff" Then Continue For
	'			If sField = "tradepayment" Then Continue For
	'		End If

	'		errMessage &= key & "<br />"
	'		flag = True
	'	End If
	'Next

	'      If Not Common.ValidateEmail(Common.SafeString(Request.Form("EmailAddr"))) Then
	'          errMessage &= "Please input valid email address</br>"
	'          flag = True
	'      End If

	'      If flag Then
	'          errMessage = "<b><span style='color: red'>Please input missing field(s):</span></b><br/>" & errMessage
	'      End If

	'      If (errMessage <> "") Then
	'          errMessage &= "<div id='MLerrorMessage'></div>"
	'      End If

	'      Return errMessage
	'  End Function
	Public Class selectDocuments
		Public title As String
		Public base64data As String
	End Class

#Region "display product utitilities"
	Private Function getQualifyProduct(ByVal xmlResponse As String, Optional ByVal proposedLoanAmount As String = "", Optional ByVal loanTerm As String = "") As List(Of String)
		Dim strQualifyProduct As New List(Of String)
		Dim card_rate As Double = 0.0
		Dim temp_rate As Double = 0.0
		Dim loan_term As Integer = 0
		Dim monthly_payment As Double = 0.0
		Dim index As Integer = 0
		If (Common.checkIsQualified(xmlResponse)) Then
			Dim doc As New XmlDocument()
			doc.LoadXml(xmlResponse)
			Dim oDecision As XmlElement = doc.SelectSingleNode("/OUTPUT/RESPONSE/DECISION")
			Dim strQualifyCard As String = oDecision.ChildNodes(0).InnerText
			Dim cdoc As New XmlDocument()
			cdoc.LoadXml(strQualifyCard)
			Dim oQualifyProduct As XmlElement = cdoc.SelectSingleNode("/QUALIFIED_PRODUCTS")
			If (oQualifyProduct IsNot Nothing) Then
				For Each productNode As XmlElement In oQualifyProduct
					Dim oProductRate As XmlElement = productNode.SelectSingleNode("RATE")
					If oProductRate Is Nothing Then Continue For
					card_rate = Common.SafeDouble(oProductRate.GetAttribute("rate"))
					If (index = 0) Then
						temp_rate = Common.SafeDouble(oProductRate.GetAttribute("rate")) ''get first rate
					End If
					If temp_rate > card_rate Then
						temp_rate = card_rate ''scan the loop and get the lowest rate
					End If
					index += 1
				Next
				''get the product with lowest rate
				For Each productNode As XmlElement In oQualifyProduct
					Dim oProductRate As XmlElement = productNode.SelectSingleNode("RATE")
					If oProductRate Is Nothing Then Continue For
					If temp_rate = Common.SafeDouble(oProductRate.GetAttribute("rate")) Then
						'strQualifyProduct = oProductRate.GetAttribute("max_loan_amount") + "," + oProductRate.GetAttribute("rate").ToString() + "," + oProductRate.GetAttribute("max_loan_term").ToString() + "," + productNode.GetAttribute("monthly_payment").ToString()

						'since the respone doesn't has the actual approved amount and term( has only max ammount and min max term)
						If Not String.IsNullOrEmpty(proposedLoanAmount) And Not String.IsNullOrEmpty(loanTerm) Then
							strQualifyProduct.Add(Common.SafeString(proposedLoanAmount.Replace("$", "")) + "|" + Common.SafeString(oProductRate.GetAttribute("rate")) + "|" + Common.SafeString(loanTerm) + "|" + Common.SafeString(productNode.GetAttribute("monthly_payment")) + "|" + Common.SafeString(oProductRate.GetAttribute("max_loan_amount")))
						Else
							strQualifyProduct.Add(Common.SafeString(Request.Form("ProposedLoanAmount")).Replace("$", "") + "|" + Common.SafeString(oProductRate.GetAttribute("rate")) + "|" + Common.SafeString(Request.Form("LoanTerm")) + "|" + Common.SafeString(productNode.GetAttribute("monthly_payment")) + "|" + Common.SafeString(oProductRate.GetAttribute("max_loan_amount")))
						End If
						Exit For
					End If
				Next
			End If
		End If
		Return strQualifyProduct
	End Function

	Private Function getQualifyProduct2_0(ByVal xmlResponse As String) As List(Of System.Collections.Generic.Dictionary(Of String, String))
		Dim QualifyProducts As New System.Collections.Generic.List(Of System.Collections.Generic.Dictionary(Of String, String))
		Dim card_rate As Double = 0.0
		Dim temp_rate As Double = 0.0
		Dim index As Integer = 0
		If Not Common.checkIsQualified(xmlResponse) Then Return QualifyProducts

		Dim doc As New XmlDocument()
		doc.LoadXml(xmlResponse)
		Dim oDecision As XmlElement = doc.SelectSingleNode("/OUTPUT/RESPONSE/DECISION")
		If oDecision Is Nothing Then Return QualifyProducts
		Dim strQualifyCard As String
		'oDecision may be qualify but has no product so null childnode
		Try
			strQualifyCard = oDecision.ChildNodes(0).InnerText
		Catch ex As Exception
			Return QualifyProducts
		End Try

		Dim cdoc As New XmlDocument()
		cdoc.LoadXml(strQualifyCard)

		'<PRODUCTS><VL_PRODUCT loan_id="6b1a9b270e3e45b7b459704ab20c7d65" date_offered="2016-12-19T15:52:38.2922388-08:00" is_prequalified="N" underwriting_service_results_index="0" amount_approved="15000" max_amount_approved="58500" grade="Auto A+" reference_id="" tier="1" set_loan_system="C" is_single_payment="false" solve_for="PAYMENT" estimated_monthly_payment="438.10" term="36"><RATE rate="1.990" original_rate="1.990" rate_code="36 mo NEW" margin="0.000" index="0.000" rate_floor="0.000" rate_ceiling="0.000" rate_type="F" rate_adjustment_option="INTEREST_RATE" index_type="" dealer_reserve_rate="2.000" initial_rate="" change_after_days="" /><STIPULATIONS><STIPULATION stip_text="PROVIDE PROOF OF INCOME IF SELF-EMPLOYED: 2 YEARS PAY STUBS OR 2 YEARS TAX RETURNS" is_required="N" /><STIPULATION stip_text="PROVIDE PROOF OF INSURANCE" is_required="Y" /><STIPULATION stip_text="ECCU MEMBERSHIP IS REQUIRED" is_required="Y" /></STIPULATIONS></VL_PRODUCT></PRODUCTS>]

		Dim oQualifyProducts As XmlNodeList = cdoc.SelectNodes("/PRODUCTS/VL_PRODUCT")
		For Each oItem As XmlElement In oQualifyProducts
			Dim oQualifyProduct As XmlElement = oItem
			Dim oProductRate As XmlElement = oQualifyProduct.SelectSingleNode("RATE")
			Dim dicQualifyProduct As New System.Collections.Generic.Dictionary(Of String, String)
			dicQualifyProduct.Add("Status", oDecision.GetAttribute("status")) '"PREQUALIFIED", QUALIFIED
			dicQualifyProduct.Add("Index", oQualifyProduct.GetAttribute("underwriting_service_results_index").ToString())
			dicQualifyProduct.Add("AmountApproved", oQualifyProduct.GetAttribute("amount_approved").Replace("$", ""))
			dicQualifyProduct.Add("MaxAmountApproved", oQualifyProduct.GetAttribute("max_amount_approved").Replace("$", ""))
			dicQualifyProduct.Add("Rate", oProductRate.GetAttribute("rate").ToString())
			dicQualifyProduct.Add("RateType", oProductRate.GetAttribute("rate_type").ToString())
			dicQualifyProduct.Add("Term", oQualifyProduct.GetAttribute("term").ToString())
			dicQualifyProduct.Add("EstimatedMonthlyPayment", oQualifyProduct.GetAttribute("estimated_monthly_payment").ToString())
			dicQualifyProduct.Add("Grade", oQualifyProduct.GetAttribute("grade").ToString())
			dicQualifyProduct.Add("RateCode", oProductRate.GetAttribute("rate_code").ToString())
			QualifyProducts.Add(dicQualifyProduct)
		Next

		Return sortQualifyProducts(QualifyProducts)
	End Function
	''sort qualify products by rate (from lowest to highest) 
	Function sortQualifyProducts(ByVal qualifiedProducts As List(Of Dictionary(Of String, String))) As List(Of Dictionary(Of String, String))
		If qualifiedProducts.Count < 2 Then
			Return qualifiedProducts
		End If
		Dim sw = True
		While sw = True
			sw = False
			For i = 0 To qualifiedProducts.Count - 2
				Dim rate1 = Convert.ToDouble(qualifiedProducts(i).Item("Rate"))
				Dim rate2 = Convert.ToDouble(qualifiedProducts(i + 1).Item("Rate"))
				If rate1 > rate2 Then
					Dim temp = qualifiedProducts(i + 1)
					qualifiedProducts(i + 1) = qualifiedProducts(i)
					qualifiedProducts(i) = temp
					sw = True
				End If
			Next
		End While
		Return qualifiedProducts
	End Function
	Private Function displayProduct() As String
		Dim strHtml As String = ""
		Dim strQualifiedMessage As String = ""
		If (QualifiedProduct.Any() AndAlso QualifiedProduct.Count > 0) Then
			Dim strQualifyProduct As String() = QualifiedProduct(0).Split("|")
			strHtml += "<div id='qualifyProduct'>"
			strHtml += "<table class='table-format'>"
			If strQualifyProduct.Length > 5 Then  'prequalify when there is more than 5 field
				''strHtml += "<tr><th colspan='5' style='white-space: nowrap' ><b><center>You have successfully pre-qualified for a vehicle loan.</center></b></th> </tr>"
				strQualifiedMessage = "<p><b><center>You have successfully pre-qualified for a vehicle loan.</center></b></p>"
				strHtml += strQualifiedMessage & "<tr style='background-color:lime'> <th colspan='5'><b><center>Pre-Qualified Product</center></b></th></tr>"
			Else 'qualify
				''strHtml += "<tr><th colspan='5' style='white-space: nowrap' ><b><center>You have successfully qualified for a vehicle loan.</center></b></th> </tr>"
				strQualifiedMessage = "<p><b><center>You have successfully qualified for a vehicle loan.</center></b><p>"
				strHtml += strQualifiedMessage & "<tr style='background-color:lime'> <th colspan='5'><b><center>Qualified Product</center></b></th></tr>"
			End If
			'strHtml += "<tr style='background-color:lime'> <th colspan='4'><b><center>Qualified Product</center></b></th></tr>"
			If strQualifyProduct(4).Trim = "" Then 'no max approved amount
				strHtml += "<tr><td><b>Pre-Approved Amount</b></td><td><b>Rate</b></td><td><b>Term (months)</b></td><td><b>Estimated Monthly Payment</b></td></tr>"
			Else
				strHtml += "<tr><td><b>Pre-Approved Amount</b></td><td><b>Rate</b></td><td><b>Term (months)</b></td><td><b>Estimated Monthly Payment</b></td><td><b>Max Approved Amount</b></td></tr>"

			End If
			strHtml += "<tr><td>" + FormatCurrency(strQualifyProduct(0), 2) + "</td>"	 'Pre-Approved Amount
			strHtml += "<td>" + strQualifyProduct(1) + "%</td>"
			strHtml += "<td>" + strQualifyProduct(2) + "</td>" 'term

			If strQualifyProduct(4) = "" Then 'estmitate montly
				strHtml += "<td>" + FormatCurrency(strQualifyProduct(3), 2) + "</td></tr></table></div><br/>"
			Else
				''make sure maxApprove Account is not negative number
				Dim maxAmount As Double = Common.SafeDouble(strQualifyProduct(4))
				Dim strMaxAmount As String = ""
				If maxAmount > 0 Then
					strMaxAmount = FormatCurrency(strQualifyProduct(4), 2)	'max apporved
				End If
				strHtml += "<td>" + FormatCurrency(strQualifyProduct(3), 2) + "</td><td>" + strMaxAmount + "</td></tr></table></div><br/>" 'estimated monthly + max approved
			End If
		End If
		Dim showQualifiedProduct As String = Common.showQualifiedProduct(_CurrentWebsiteConfig)
		If showQualifiedProduct = "N" Then
			strHtml = "<div style='display:none'>" & strHtml & "</div>"
		End If
		Return strHtml
	End Function
#End Region
	Protected Sub ExecComboModeFlow()
		Dim bIsPassUnderwrite As Boolean = False
		Dim oUnderwriteCommon As UnderwriteCommon = New UnderwriteCommon()
		Dim sloanIDA = Common.getIDAType(_CurrentWebsiteConfig, "VEHICLE_LOAN")
		If (Request.Form("Task") = "SubmitLoan") Then
			Dim sReason As String = CApplicantBlockLogic.Execute(_CurrentWebsiteConfig, Request, "VL")
			If Not String.IsNullOrEmpty(sReason) Then
				Response.Write(sReason)
				Return

			End If

			log.Info("Executing Combo mode submission process. ProceedWithXAAnyway=" & IIf(_ProceedXAAnyway, "Y", "F"))

			'1. submitloan
			Dim vehicleLoanDataObject As CVehicleLoan = GetVehicleLoanObject(_CurrentWebsiteConfig)
			DecisionMessage = Server.HtmlDecode(SubmitVehicleLoan(vehicleLoanDataObject, _CurrentWebsiteConfig, loanResponseXMLStr)) 'create new loan with external source =  MOBILE WEBSITE COMBO
			''check user select disagree button, and disagree_popup is on then we submit data to LPQ using submitloan and stop process 
			If Common.SafeString(Request.Form("isDisagreeSelect")) = "Y" Then
				log.Info("User disagreed to submitting the application. Halting Combo mode flow.")
				Return
			End If
			Decision_v2_Response = loanResponseXMLStr	'processed loandecision message,persist for later use
			loanID = Common.getResponseLoanID(loanResponseXMLStr)
			If loanID = "" Then 'something is wrong so just exit and return the message
				log.Warn("Returned LoanID is empty. Halting Combo mode flow.")
				Response.Write(DecisionMessage)
				Return
			End If

			If _CurrentWebsiteConfig.SubmitLoanUrl.Contains("decisionloan/2.0") Then
				QualifiedProducts = getQualifyProduct2_0(loanResponseXMLStr)
			Else
				QualifiedProduct = getQualifyProduct(loanResponseXMLStr)
			End If
			Dim qualifiedProductCount = Math.Max(QualifiedProducts.Count, QualifiedProduct.Count)

			Dim IsDeclinedLoan As Boolean = Common.checkIsDeclined(loanResponseXMLStr)
			''NEW MOCKUP
			'I) LOAN=DECLINED
			''CHOICE = DON'T WANT
			''NO CREATE XA
			''LOAN DECLINED MESSAGE(1)
			If IsDeclinedLoan And Not _ProceedXAAnyway Then 'declined - don't care so just respsone with SUBMITTED_MESSAGE 'This is the only senarios where XA is not submitted
				'' 3)INSTANT DELCINED LOAN (WANTS NO MEMBERSHIP) 
				log.Info("Loan is declined, but the user has agreed to proceed with XA creation anyway.")
				LoanSubmit.UpdateLoan(loanID, _CurrentWebsiteConfig, "LOANDECLINED_XANOTPROCEED") 'don't update status, just add decision comment
				Response.Write(DecisionMessage)
				Return
			End If
			''If IsDeclinedLoan And Not _ProceedXAAnyway Then 'declined - don't care so just respsone with SUBMITTED_MESSAGE 'This is the only senarios where XA is not submitted
			''    '' 3)INSTANT DELCINED LOAN (WANTS NO MEMBERSHIP) 
			''    LoanSubmit.UpdateLoan(loanID, _CurrentWebsiteConfig, "XANOTPROCEED") 'don't update status, just add decision comment
			''    Response.Write(DecisionMessage)
			''    Return
			''ElseIf qualifiedProductCount = 0 Then  'start membership eventhought consumer dont qualify for a loan, amy be able to sarvage if loan if  qualify later
			''    If _ProceedXAAnyway Then
			''        LoanSubmit.UpdateLoan(loanID, _CurrentWebsiteConfig, "XAPROCEED")
			''    Else
			''        LoanSubmit.UpdateLoan(loanID, _CurrentWebsiteConfig, "XANOTPROCEED") ' 
			''    End If
			''ElseIf qualifiedProductCount > 0 Then 'qualified, change loan status to PENDING & add decision comment so if user abort IDA/XA process the officer will know
			''    If _ProceedXAAnyway Then
			''        LoanSubmit.UpdateLoan(loanID, _CurrentWebsiteConfig, "PEN")  'TODO: how do loanofficer know XA status has been completed? Probaly use automate trigger(external_source and status change) to trigger a webms meesage to loanofficer
			''    Else
			''        LoanSubmit.UpdateLoan(loanID, _CurrentWebsiteConfig, "PENXANOTPROCEED")  'TODO: how do loanofficer know XA status has been completed? Probaly use automate trigger(external_source and status change) to trigger a webms meesage to loanofficer
			''    End If
			''End If

			'2.proceed with membership(XA)
			'include Product, FOM, funding(only for non-member secured credit card)
			'Dim currentProdList As List(Of CProduct) = CProduct.GetProducts(_CurrentWebsiteConfig, "1")	'ok for now, use psAvailability=1 filter for non minor/specail
			Dim currentProdList As New List(Of CProduct)
			Dim allProdList As List(Of CProduct) = CProduct.GetProductsForLoans(_CurrentWebsiteConfig)
			Dim allowedProductCodes As List(Of String) = GetAllowedProductCodes("VEHICLE_LOAN")	'now, loans has their own configured products, so get all product and filter by those configured products
			If allProdList IsNot Nothing And allProdList.Any() Then
				If allowedProductCodes IsNot Nothing And allowedProductCodes.Any() Then
					currentProdList = allProdList.Where(Function(p) allowedProductCodes.Contains(p.ProductCode)).ToList() ' perform filter by configured product in config xml (if any)
				Else
					currentProdList = allProdList
				End If
			End If

			If _xaContinueWhenLoanIsReferred = "N" And qualifiedProductCount = 0 And Not _ProceedXAAnyway Then
				'don't want continue with membership if loan app is refferred or fraud 
				log.Info("Portal is set to continue with loan is referred, and no products were found, and the user opted not to proceed with XA creation. Updating loan to indicate this.")
				Dim customSubmitMessage = Server.HtmlDecode(CCustomMessage.getComboCustomResponseMessages(_CurrentWebsiteConfig, "SUBMITTED_MESSAGE", loanResponseXMLStr, loanRequestXMLStr, ""))
				Dim strXADataComment As String = LoanSubmit.addXADataToCommentSection(Request, _CurrentWebsiteConfig, currentProdList)
				LoanSubmit.UpdateLoan(loanID, _CurrentWebsiteConfig, "REFERREDLOAN_XANOTPROCEED", "", strXADataComment)
				If String.IsNullOrEmpty(sloanIDA) Then
					Response.Write(customSubmitMessage)
				Else
					''run ida for loan when XA app is not created.
					''execute wallet question for primary 
					Dim sFirstName As String = Common.SafeString(Request.Form("FirstName"))
					Dim sWalletQuestionResponse As String = Common.getWalletQuestionsResponseXML(_CurrentWebsiteConfig, loanResponseXMLStr, sloanIDA, False)
					If String.IsNullOrEmpty(sWalletQuestionResponse) Then
						Response.Write(customSubmitMessage)
					Else
						Dim sRenderWalletQuestions As String = Common.renderWalletQuestionsHTML(sloanIDA, sWalletQuestionResponse, questionList_persist, sFirstName, transaction_ID, question_set_ID, request_app_ID, request_client_ID, request_sequence_ID, reference_number)
						Response.Write(sRenderWalletQuestions)
					End If
				End If
				Return
			End If

			Try
				Dim sLoanNumber = Common.getLoanNumber(loanResponseXMLStr)
				LoanSubmit.SubmitXA("VL", Request, _CurrentWebsiteConfig, xaRequestXMLStr, xaResponseXMLStr, currentProdList, sLoanNumber)	 'create new XA with external source =  MOBILE WEBSITE COMBO
				xaID = Common.getResponseLoanID(xaResponseXMLStr)

				''move the loan update immediately after XA submit so If consumer abandons the IDA, the loan still have the XA number in the comment.
				If qualifiedProductCount = 0 Then   'start membership eventhought consumer dont qualify for a loan, amy be able to sarvage if loan if  qualify later
					If _ProceedXAAnyway Then
						If IsDeclinedLoan Then
							LoanSubmit.UpdateLoan(loanID, _CurrentWebsiteConfig, "LOANDECLINED_XAPROCEED", Common.getLoanNumber(xaResponseXMLStr))
						Else
							LoanSubmit.UpdateLoan(loanID, _CurrentWebsiteConfig, "XAPROCEED", Common.getLoanNumber(xaResponseXMLStr))
						End If
					Else
						LoanSubmit.UpdateLoan(loanID, _CurrentWebsiteConfig, "XANOTPROCEED", Common.getLoanNumber(xaResponseXMLStr))
					End If
				ElseIf qualifiedProductCount > 0 Then 'qualified, change loan status to PENDING & add decision comment so if user abort IDA/XA process the officer will know
					If _ProceedXAAnyway Then
						LoanSubmit.UpdateLoan(loanID, _CurrentWebsiteConfig, "PEN", Common.getLoanNumber(xaResponseXMLStr))	 'TODO: how do loanofficer know XA status has been completed? Probaly use automate trigger(external_source and status change) to trigger a webms meesage to loanofficer
					Else
						LoanSubmit.UpdateLoan(loanID, _CurrentWebsiteConfig, "PENXANOTPROCEED", Common.getLoanNumber(xaResponseXMLStr))	 'TODO: how do loanofficer know XA status has been completed? Probaly use automate trigger(external_source and status change) to trigger a webms meesage to loanofficer
					End If
				End If

				If xaID Is Nothing Or xaID = "" Then  'For PID, the webservice will automatically add failure message to internal comment  TODO: add internal comment for other webservice
					log.Error("Error submitting XA for Combo: ")
					''Dim oRequestXML As XmlDocument = New XmlDocument()
					''oRequestXML.LoadXml(xaRequestXMLStr)
					''Dim responseMessage = Server.HtmlDecode(Common.WebPostResponse(_CurrentWebsiteConfig, oRequestXML, xaResponseXMLStr, False))
					''responseMessage = responseMessage.Replace("MLerrorMessage", "bogus") 'in combomode, dont' want consumer to resubmit duplicate loan, so force it to go to last diaglog even thought there is no XA app
					''               Response.Write(responseMessage) ''failed -> response xa SUBMITTED_MESSAGE
					Dim comboResponseSubmitMessage As String = CCustomMessage.getComboCustomResponseMessages(_CurrentWebsiteConfig, "COMBO_SUBMITTED_MESSAGE", loanResponseXMLStr, loanRequestXMLStr, "")
					Response.Write(Server.HtmlDecode(comboResponseSubmitMessage)) ''failed -> response combo submit SUBMITTED_MESSAGE
					Return
				End If
			Catch ex As Exception
				'TODO: should have clear the screen and prevent another post back
				log.Error("Error submitting XA for Combo: " & ex.Message, ex)
				''Dim oRequestXML As XmlDocument = New XmlDocument()
				''oRequestXML.LoadXml(xaRequestXMLStr)
				''Dim responseMessage = Server.HtmlDecode(Common.WebPostResponse(_CurrentWebsiteConfig, oRequestXML, xaResponseXMLStr, False))
				''responseMessage = responseMessage.Replace("MLerrorMessage", "bogus") 'in combomode, dont' want consumer to resubmit duplicate loan, so force it to go to last diaglog even thought there is no XA app
				''            Response.Write(responseMessage) ''failed -> response xa SUBMITTED_MESSAGE
				Dim comboResponseSubmitMessage As String = CCustomMessage.getComboCustomResponseMessages(_CurrentWebsiteConfig, "COMBO_SUBMITTED_MESSAGE", loanResponseXMLStr, loanRequestXMLStr, "")
				Response.Write(Server.HtmlDecode(comboResponseSubmitMessage)) ''failed -> response combo submit SUBMITTED_MESSAGE
				Return
			End Try

			''check status for loans if it is FRAUD/DUP before to do underwriting for xa
			Dim sLoanStatus As String = LoanSubmit.getLoanStatus(loanID, _CurrentWebsiteConfig).ToUpper
			If sLoanStatus = "FRAUD" Or sLoanStatus = "DUP" Then
				If _ProceedXAAnyway Then
					Response.Write(Server.HtmlDecode(CCustomMessage.getComboCustomResponseMessages(_CurrentWebsiteConfig, "COMBO_SUBMITTED_MESSAGE", loanResponseXMLStr, loanRequestXMLStr, "")))
				Else
					Response.Write(Server.HtmlDecode(CCustomMessage.getComboCustomResponseMessages(_CurrentWebsiteConfig, "SUBMITTED_MESSAGE", loanResponseXMLStr, loanRequestXMLStr, "")))
				End If
				Return '' no contiue
			End If ''end check status for loans

			''check pre UnderWrite for xaCombo
			Dim xaRequestXMLDoc As New XmlDocument()
			xaRequestXMLDoc.LoadXml(xaRequestXMLStr)
			If Not oUnderwriteCommon.PreUnderWrite(_CurrentWebsiteConfig, xaRequestXMLDoc, Request, IsJointApplication) Then
				Response.Write(Server.HtmlDecode(CCustomMessage.getComboCustomResponseMessages(_CurrentWebsiteConfig, "COMBO_SUBMITTED_MESSAGE", loanResponseXMLStr, loanRequestXMLStr, "")))
				Return
			End If

			'badMember?
			If _CurrentWebsiteConfig.LenderCode <> "" Then
				If oUnderwriteCommon.isBadMember(_CurrentWebsiteConfig, Request) Then
					Response.Write(Server.HtmlDecode(CCustomMessage.getComboCustomResponseMessages(_CurrentWebsiteConfig, "COMBO_SUBMITTED_MESSAGE", loanResponseXMLStr, loanRequestXMLStr, "")))
					Return
				End If
			End If

			'3.Underwrite if enabled(decisionxa,debit,IDA)
			'a.IDA
			'Dim oAccountNodes As XmlNodeList = _CurrentWebsiteConfig._webConfigXML.SelectNodes("VEHICLE_LOAN/ACCOUNTS/ACCOUNT")
			Dim selectedProductList As List(Of CProduct) = oUnderwriteCommon.getSettingProductList(_CurrentWebsiteConfig, GetAllowedProductCodes("VEHICLE_LOAN"), Request)
			Dim bIDA_LPQConfig As Boolean = oUnderwriteCommon.isIDALPQConfig(selectedProductList)
			SelectedProducts = ""
			If selectedProductList.Count > 0 Then
				SelectedProducts = New JavaScriptSerializer().Serialize(selectedProductList)
			End If
			''Dim prerequisiteProduct As New CProduct
			''Dim bIDA_LPQConfig As Boolean = False
			''If oAccountNodes.Count > 0 Then
			''	prerequisiteProduct.ProductCode = oAccountNodes(0).Attributes("product_code").InnerXml
			''	If currentProdList IsNot Nothing And currentProdList.Any() Then
			''		Dim cproduct As CProduct = currentProdList.FirstOrDefault(Function(cp As CProduct) cp.ProductCode = prerequisiteProduct.ProductCode)
			''		If cproduct IsNot Nothing Then
			''			prerequisiteProduct = cproduct
			''			bIDA_LPQConfig = prerequisiteProduct.AutoPullIDAuthenticationConsumerPrimary
			''		End If
			''	End If
			''End If
			Dim xaIDAType As String = _CurrentWebsiteConfig.AuthenticationType 'IDA attribute from main node for xa
			Dim bIDA_LPQMobileWebsiteConfig As Boolean = (xaIDAType <> "")
			Dim hasWalletQuestion As Boolean = True
			'Run IDA when enable on both mobile config and lender side
			If bIDA_LPQConfig And bIDA_LPQMobileWebsiteConfig Then
				''execute wallet question for primary 
				Dim strResponse As String = Common.getWalletQuestionsResponseXML(_CurrentWebsiteConfig, xaResponseXMLStr, xaIDAType, False)	'do primary first
				If strResponse = "" Then 'failed IDA request
					''Dim oRequestXML As XmlDocument = New XmlDocument()
					''oRequestXML.LoadXml(xaRequestXMLStr)
					''Dim responseMessage = Server.HtmlDecode(Common.WebPostResponse(_CurrentWebsiteConfig, oRequestXML, xaResponseXMLStr, False))
					''responseMessage = responseMessage.Replace("MLerrorMessage", "bogus") 'in combomode, dont' want consumer to resubmit duplicate loan, so force it to go to last diaglog even thought there is no XA app
					''               Response.Write(responseMessage) ''failed -> response xa SUBMITTED_MESSAGE

					''2)INSTANT APPROVED FOR LOAN & REFERRED FOR MEMBERSHIP 
					'' REFERRED FOR LOAN (WANTS NO MEMBERRSHIP) & REFERRED FOR MEMBERSHIP 
					'' REFERRED FOR LOAN (WANTS MEMBERSHIP) & REFERRED FOR MEMBERSHIP 
					''Dim comboResponseSubmitMessage As String = CCustomMessage.getComboCustomResponseMessages(_CurrentWebsiteConfig, "COMBO_SUBMITTED_MESSAGE", loanResponseXMLStr, loanRequestXMLStr, "")
					''If qualifiedProductCount.Count > 0 Then
					''	LoanSubmit.UpdateLoan(loanID, _CurrentWebsiteConfig, "AA_XAFAILED", Common.getLoanNumber(xaResponseXMLStr))	' update loan status to instant approved
					''End If
					''Response.Clear()
					''Response.Write(Server.HtmlDecode(comboResponseSubmitMessage)) ''failed -> response combo submit SUBMITTED_MESSAGE
					''Return
					hasWalletQuestion = False
				End If

				If Not String.IsNullOrEmpty(strResponse) Then
					Dim sName As String = Request.Form("FirstName")
					Dim strRenderWalletQuestions As String = Common.renderWalletQuestionsHTML(xaIDAType, strResponse, questionList_persist, sName, transaction_ID, question_set_ID, request_app_ID, request_client_ID, request_sequence_ID, reference_number)
					'4. Display Wallet question (IDA)
					Response.Write(strRenderWalletQuestions)
					Return
				End If
			End If
			bIsPassUnderwrite = True 'if reach here then IDA is not required 
			If Not hasWalletQuestion Then
				bIsPassUnderwrite = False
			End If
		End If

		If CheckComboLoanIDA(sloanIDA) Then
			ExecuteWalletQuestionsAndAnswersForComboLoan(sloanIDA)
			Return
		End If
		'5. Validate Wallet question
		If (Request.Form("Task") = "WalletQuestions") Then
			''wallet answer
			bIsPassUnderwrite = True
			'Dim combinedAnswerStr As String = Common.SafeString(Request.Form("WalletAnswers"))
			'Dim answerIDList As List(Of String) = Common.parseAnswersForID(combinedAnswerStr)
			Dim sWalletQuestionsAndAnswers As String = Common.SafeString(Request.Form("WalletQuestionsAndAnswers"))
			Dim loanIDAType As String = _CurrentWebsiteConfig.AuthenticationType
			Dim idaResponseXAIDList As List(Of String) = idaResponseXAIDs(loanIDAType)
			Dim hasJointWalletAnswer As String = Common.SafeString(Request.Form("hasJointWalletAnswer"))
			Dim isJointAnswers = hasJointWalletAnswer = "Y"
			Dim isExecuteAnswer As Boolean = Common.ExecWalletAnswers(_CurrentWebsiteConfig, sWalletQuestionsAndAnswers, questionList_persist, idaResponseXAIDList, isJointAnswers, question_set_ID, request_app_ID, request_client_ID, request_sequence_ID, reference_number)
			'isExecuteAnswer = True
			Dim oRequestXML As XmlDocument = New XmlDocument()
			oRequestXML.LoadXml(xaRequestXMLStr)
			If Not isExecuteAnswer Then	 'PID service will automatically insert msg into internal comments  TODO:log for other service
				''responseMessage = Server.HtmlDecode(Common.WebPostResponse(_CurrentWebsiteConfig, oRequestXML, xaResponseXMLStr, False))
				''Response.Write(responseMessage) ''failed -> response submit message
				bIsPassUnderwrite = False ' use in step 6
			End If
			' populate questions for joint 
			If (IsJointApplication And Not isJointAnswers) Then
				''response wallet question for joint
				Dim strResponse As String = Common.getWalletQuestionsResponseXML(_CurrentWebsiteConfig, xaResponseXMLStr, loanIDAType, IsJointApplication)
				If String.IsNullOrEmpty(strResponse) Then
					''responseMessage = Server.HtmlDecode(Common.WebPostResponse(_CurrentWebsiteConfig, oRequestXML, xaResponseXMLStr, False))
					''Response.Write(responseMessage) ''failed -> response submit message
					bIsPassUnderwrite = False ' use in step 6
				Else
					Dim strRenderWalletQuestions As String = Common.renderWalletQuestionsHTML(loanIDAType, strResponse, questionList_persist, Co_FName, transaction_ID, question_set_ID, request_app_ID, request_client_ID, request_sequence_ID, reference_number)
					Response.Clear()
					Response.Write(strRenderWalletQuestions) ''response wallet question for joint
					Return
				End If
			End If

		End If


		If _CurrentWebsiteConfig.SubmitLoanUrl.Contains("decisionloan/2.0") Then
			QualifiedProducts = getQualifyProduct2_0(loanResponseXMLStr)
		Else
			QualifiedProduct = getQualifyProduct(loanResponseXMLStr)
		End If
		Dim isPreQualified = Common.checkIsPreQualified(loanResponseXMLStr)
		'6.Update loan status 
		Dim sXALoanNumber = Common.getLoanNumber(xaResponseXMLStr)
		Dim comboSubmitMessage As String = Server.HtmlDecode(CCustomMessage.getComboCustomResponseMessages(_CurrentWebsiteConfig, "COMBO_SUBMITTED_MESSAGE", loanResponseXMLStr, loanRequestXMLStr, ""))
		Dim comboXASubmitMessage As String = Server.HtmlDecode(CCustomMessage.getComboCustomResponseMessages(_CurrentWebsiteConfig, "COMBO_XA_SUBMITTED_MESSAGE", loanResponseXMLStr, loanRequestXMLStr, ""))
		Dim declinedMessage As String = Server.HtmlDecode(CCustomMessage.getComboCustomResponseMessages(_CurrentWebsiteConfig, "DECLINED_MESSAGE", loanResponseXMLStr, loanRequestXMLStr, ""))
		Dim preQualifiedMessage As String = Server.HtmlDecode(CCustomMessage.getComboCustomResponseMessages(_CurrentWebsiteConfig, "DECLINED_MESSAGE", loanResponseXMLStr, loanRequestXMLStr, ""))
		Dim submitMessage As String = Server.HtmlDecode(CCustomMessage.getComboCustomResponseMessages(_CurrentWebsiteConfig, "SUBMITTED_MESSAGE", loanResponseXMLStr, loanRequestXMLStr, ""))
		comboSubmitMessage = Common.PrependIconForNonApprovedMessage(comboSubmitMessage, "Thank You")
		''-----------run debit, creditpull or decisionXA after passed IDA or Not required to Run IDA--------------
		If bIsPassUnderwrite Then
			'' Dim SettingProductsList = oUnderwriteCommon.getSettingProductList(_CurrentWebsiteConfig, GetAllowedProductCodes("VEHICLE_LOAN"), Request)
			Dim SettingProductsList As New List(Of CProduct)
			If SelectedProducts <> "" Then
				SettingProductsList = New JavaScriptSerializer().Deserialize(Of List(Of CProduct))(SelectedProducts)
			End If
			''-------run debit ------------
			If Not oUnderwriteCommon.executeDebit(_CurrentWebsiteConfig, SettingProductsList, xaID, IsJointApplication) Then
				bIsPassUnderwrite = False ''reset passUnderWrite
			End If

			''c. ----pull credit or do decisionXA------
			If _CurrentWebsiteConfig.IsDecisionXAEnable Then
				Dim sXMLresponse = oUnderwriteCommon.ExecuteDecisionXA(_CurrentWebsiteConfig, xaID)
				If Not oUnderwriteCommon.isXAProductQualified(sXMLresponse) Then
					bIsPassUnderwrite = False ''reset passUnderWrite
				End If
			Else
				If oUnderwriteCommon.isAutoPullCredit(SettingProductsList) Then
					If Not oUnderwriteCommon.ExecuteCreditPull(_CurrentWebsiteConfig, xaID) Then
						bIsPassUnderwrite = False ''reset passUnderWrite
					End If
				End If
			End If
			''-------end pull credit or do decisionXA(after passed wallet answer) ---------
		End If
		''--------end run debit, creditpull or decisionXA after passed IDA or Not required to Run IDA--------------

		If Not bIsPassUnderwrite Then 'failed underwrite
			If _ProceedXAAnyway And Common.checkIsDeclined(loanResponseXMLStr) Then
				''NEW MOCKUP
				'I) LOAN=DECLINED
				''CHOICE = WANT
				''XA =REFERRED
				''LOAN DECLINED MESSAGE(1)
				Response.Write(declinedMessage)
				LoanSubmit.UpdateLoan(loanID, _CurrentWebsiteConfig, "XAFAILED", sXALoanNumber)	''leave status alone but add decision comments
				Return
			End If

			If QualifiedProduct.Count > 0 Or QualifiedProducts.Count > 0 Then
				'2)INSTANT APPROVED FOR LOAN & REFERRED FOR MEMBERSHIP
				LoanSubmit.UpdateLoan(loanID, _CurrentWebsiteConfig, "AA_XAFAILED", sXALoanNumber)	''update loan status to instant approved and add decision comments: MEMBERSHIP has been REFFERED"
				If isPreQualified Then
					''NEW MOCKUP
					'IV) LOAN=PRE-QUALIFIED
					''CHOICE = WANT/DON'T WANT
					''XA =REFERRED
					''STANDARD: LOAN PRE-QUALIED MESSAGE(NEW) (7)
					Response.Clear()
					Response.Write(preQualifiedMessage)
				Else
					''NEW MOCKUP
					'III) LOAN=APPROVED
					''CHOICE = WANT/DON'T WANT
					''XA =REFERRED
					''COMBO: LOAN APPROVED AND XA REFERRED MESSAGE (5) -->using existing node <COMBO_XA_SUBMITTED_MESSAGE />
					Response.Clear()
					Response.Write(comboXASubmitMessage)
				End If
				Return
			Else
				''NEW MOCKUP
				'II) LOAN=REFERRED
				''CHOICE = WANT/DON'T WANT
				''XA =REFERRED
				If _ProceedXAAnyway Then
					''COMBO: LOAN REFERRED MESSAGE(2) --combo_submit_message
					Response.Clear()
					Response.Write(comboSubmitMessage)
				Else
					''STANDARD: LOAN REFERRED MESSAGE(3)
					Response.Clear()
					Response.Write(submitMessage)
				End If
				LoanSubmit.UpdateLoan(loanID, _CurrentWebsiteConfig, "XAFAILED", sXALoanNumber)	'leave status alone but add decision comments(MEMBERSHIP has been REFFERED")
			End If
			Return
		End If

		'7. Update loan status and XA status if passed underwrite
		If bIsPassUnderwrite Then
			''pass underwrite then update xa status (using EnumLoanStatus)
			LoanSubmit.UpdateXA(xaID, _CurrentWebsiteConfig)
			Dim vehicleLoanDataObject As CVehicleLoan = GetVehicleLoanObject(_CurrentWebsiteConfig)
			''do funding process
			Dim ntotalFunding As Double = Common.getTotalFundingAmount(xaRequestXMLStr)
			Dim sAccountNumber As String = ""
			If _ProceedXAAnyway And Common.checkIsDeclined(loanResponseXMLStr) Then
				''NEW MOCKUP
				'I) LOAN=DECLINED
				''CHOICE = WANT
				''XA =APPROVED
				''LOAN DECLINED MESSAGE(1)
				''
				''book to core process               
				If Common.executeXAComboFundingAndBooking(_CurrentWebsiteConfig, loanID, xaID, ntotalFunding, sXALoanNumber, sAccountNumber, Function()
																																				 Return ProceedBookingValidation(vehicleLoanDataObject)
																																			 End Function) Then
					LoanSubmit.UpdateLoan(loanID, _CurrentWebsiteConfig, "XAPASSED", sXALoanNumber)	 ''leave status alone but add decision comments based on the EnumLoanStatus
				End If
				Response.Write(declinedMessage)
				Return
			End If

			If QualifiedProduct.Count > 0 Or QualifiedProducts.Count > 0 Then
				Response.Clear()
				'' 1)INSTANT APPROVED LOAN & INSTANT APPROVED FOR MEMBERSHIP
				LoanSubmit.UpdateLoan(loanID, _CurrentWebsiteConfig, "AA", sXALoanNumber) ' update loan status to instant approved
				''book to core process  
				If Not Common.executeXAComboFundingAndBooking(_CurrentWebsiteConfig, loanID, xaID, ntotalFunding, sXALoanNumber, sAccountNumber, Function()
																																					 Return ProceedBookingValidation(vehicleLoanDataObject)
																																				 End Function) Then
					Response.Write(comboXASubmitMessage) ''LOAN APPROVED & XA REFERRED
					Return
				End If
				''check the custom message based on the EnumLoanStatus 
				If _CurrentWebsiteConfig.LoanStatusEnum.ToUpper <> "REFERRED" Then
					If isPreQualified Then
						''NEW MOCKUP
						'IV) LOAN=PRE-QUALIFIED
						''CHOICE = WANT/DON'T WANT
						''XA =APPROVED
						''COMBO: LOAN PRE-QUALIED,XA APPROVED MESSAGE(NEW) (8)
						Dim comboPreQualifiedMsg As String = Server.HtmlDecode(CCustomMessage.getComboCustomResponseMessages(_CurrentWebsiteConfig, "COMBO_PREQUALIFIED_MESSAGE", loanResponseXMLStr, loanRequestXMLStr, sAccountNumber))
						Response.Write(Common.PrependIconForNonApprovedMessage(comboPreQualifiedMsg, "Congratulations"))
					Else
						''NEW MOCKUP
						'III) LOAN=APPROVED
						''CHOICE = WANT/DON'T WANT
						''XA =APPROVED
						''COMBO: LOAN APPROVED AND XA APPROVED MESSAGE (6)  --combo_both_preapproved 
						Dim comboBothPreApprovedMsg As String = Server.HtmlDecode(CCustomMessage.getComboCustomResponseMessages(_CurrentWebsiteConfig, "COMBO_BOTH_PREAPPROVED_MESSAGE", loanResponseXMLStr, loanRequestXMLStr, sAccountNumber))
						comboBothPreApprovedMsg = Common.PrependIconForNonApprovedMessage(comboBothPreApprovedMsg, "Congratulations")
						Response.Write(comboBothPreApprovedMsg)
					End If
				Else
					Response.Write(comboXASubmitMessage) ''LOAN APPROVED & XA REFERRED
				End If
			Else
				''NEW MOCKUP
				'II) LOAN=REFERRED
				''CHOICE = WANT/DON'T WANT
				''XA =APPROVED
				If _ProceedXAAnyway Then
					''COMBO: LOAN REFERRED MESSAGE, XA APPROVED(4) --combo_xa_preapproved
					''book to core process
					If Not Common.executeXAComboFundingAndBooking(_CurrentWebsiteConfig, loanID, xaID, ntotalFunding, sXALoanNumber, sAccountNumber, Function()
																																						 Return ProceedBookingValidation(vehicleLoanDataObject)
																																					 End Function) Then
						Response.Write(comboSubmitMessage) ''LOAN REFERRED & XA REFERRED
						Return
					End If
					''check the custom message based on the EnumLoanStatus 
					If _CurrentWebsiteConfig.LoanStatusEnum.ToUpper <> "REFERRED" Then
						Dim comboXAPreApprovedMessage As String = Server.HtmlDecode(CCustomMessage.getComboCustomResponseMessages(_CurrentWebsiteConfig, "COMBO_XA_PREAPPROVED_MESSAGE", loanResponseXMLStr, loanRequestXMLStr, sAccountNumber))
						comboXAPreApprovedMessage = Common.PrependIconForNonApprovedMessage(comboXAPreApprovedMessage, "Thank You")
						Response.Write(comboXAPreApprovedMessage)
					Else
						Response.Write(comboSubmitMessage) ''LOAN REFERRED & XA REFERRED
					End If
				Else
					''STANDARD: LOAN REFERRED MESSAGE(3)
					Response.Write(submitMessage)
				End If
				LoanSubmit.UpdateLoan(loanID, _CurrentWebsiteConfig, "XAPASSED", sXALoanNumber)	  ''leave status alone but add decision comments based on the EnumLoanStatus           
			End If
			Return
		End If
	End Sub

	Private Function idaResponseXAIDs(ByVal idaType As String) As List(Of String)
		Dim responseIDList As New List(Of String)
		Select Case idaType.ToUpper()
			Case "PID", "RSA", "FIS", "DID", "EID", "TID"
				responseIDList.Add(idaType)
				responseIDList.Add(xaID)
				responseIDList.Add(transaction_ID)
				''add more cases later
		End Select
		Return responseIDList
	End Function

#Region "display product utitilities"
	Private Function displayProduct2_0() As String
		Dim IsManualProdcutSelect As Boolean = Common.SafeString(Common.getNodeAttributes(_CurrentWebsiteConfig, "BEHAVIOR", "manual_select_loan_product")) = "Y"

		Dim strHtml As String = ""
		If Not QualifiedProducts.Count > 0 Then Return DecisionMessage

		strHtml += "<div align='left'>"
		Dim sFirstName As String = Request.Form("FirstName")
		Dim sTitle As String = ""
		If IsManualProdcutSelect Then
			sTitle = "Congratulations <b>" + sFirstName + "</b>!"
		End If
		If QualifiedProducts(0)("Status") = "PREQUALIFIED" Then
			strHtml += sTitle + "  You have been pre-approved for the following: <br/>"
		Else
			strHtml += sTitle + "  You have been qualified for the following: <br/>"
		End If

		For Each dicQualifyProduct As Dictionary(Of String, String) In QualifiedProducts
			'strHtml += "<br/><b>" + dicQualifyProduct("Grade") + ": Up to " & FormatCurrency(dicQualifyProduct("MaxAmountApproved"), 0) & "</b><br/>"
			'strQualifyProduct(1) => interest rate
			'strQualifyProduct(3) => index
			'strHtml += "<b>Loan Class: </b>" + dicQualifyProduct("RateCode") + "<br/>"
			strHtml += "<br/><b>Approved Amount: </b>" + FormatCurrency(dicQualifyProduct("AmountApproved"), 2) + "<br/>"
			Dim sRateType As String = ""
			If dicQualifyProduct("RateType") = "F" Then sRateType = " Fixed"
			strHtml += "<b>Rate: </b>" + dicQualifyProduct("Rate") + "%" + sRateType + "<br/>"
			strHtml += "<b>Term: </b>" + dicQualifyProduct("Term") + " months" + "<br/>"
			''remove estimated Monthly Payment
			''strHtml += "<b>Estimated Monthly Payment: </b>" + FormatCurrency(dicQualifyProduct("EstimatedMonthlyPayment"), 2) + "<br/> <br/>"
			'strHtml += "<a href='javascript:void(0);' class='header_theme2' style='font-weight:bold' onclick='SelectProduct(" + dicQualifyProduct("Index") + ");'  >Accept Offer</a> <br/><br/>"
			If IsManualProdcutSelect Then
				strHtml += "<div class ='div-continue' ><a href='javascript:void(0);' type='button'  class='div-continue-button ui-btn ui-shadow ui-corner-all' onclick='SelectProduct(" + dicQualifyProduct("Index") + ");'  >Accept Offer</a></div> <br/><br/>"
			End If
		Next
		strHtml += "</div>"

		If Not IsManualProdcutSelect Then
			strHtml = DecisionMessage + "<br/>" + strHtml
		End If

		Dim showQualifiedProduct As String = Common.showQualifiedProduct(_CurrentWebsiteConfig)
		If showQualifiedProduct = "N" Then
			strHtml = DecisionMessage
		End If
		Return strHtml
	End Function
#End Region
#Region "Combo Loan IDA"
	''the conditions for running loan ida if xa app is not created:
	''      - no qualified products
	''      - existing loanIda - ida='PID'
	''      - xaContinueWhenLoanIsReferred=N
	''      - _ProceedXAAnyway = false
	Protected Function CheckComboLoanIDA(ByVal sLoanIDAType As String) As Boolean
		Dim oQualifiedProduct As New List(Of String)
		Dim oQualifiedProducts As New List(Of Dictionary(Of String, String))
		'' Dim loanIDAType As String = Common.getIDAType(_CurrentWebsiteConfig, sLoanType)
		If _CurrentWebsiteConfig.SubmitLoanUrl.Contains("decisionloan/2.0") Then
			oQualifiedProducts = getQualifyProduct2_0(loanResponseXMLStr)
		Else
			oQualifiedProduct = (getQualifyProduct(loanResponseXMLStr))
		End If
		If _xaContinueWhenLoanIsReferred = "N" And (oQualifiedProduct.Count = 0 And oQualifiedProducts.Count = 0) And Not String.IsNullOrEmpty(sLoanIDAType) And Not _ProceedXAAnyway Then
			Return True
		End If
		Return False
	End Function
	Protected Sub ExecuteWalletQuestionsAndAnswersForComboLoan(ByVal sLoanIDAType As String)
		Dim sLoanID = Common.getResponseLoanID(loanResponseXMLStr)
		Dim customSubmitMessage = Server.HtmlDecode(CCustomMessage.getComboCustomResponseMessages(_CurrentWebsiteConfig, "SUBMITTED_MESSAGE", loanResponseXMLStr, loanRequestXMLStr, ""))
		If (Request.Form("Task") = "WalletQuestions") Then
			Dim sWalletQuestionsAndAnswers As String = Common.SafeString(Request.Form("WalletQuestionsAndAnswers"))
			Dim idaResponseIDList As List(Of String) = idaResponseIDs(sLoanIDAType)
			Dim isJointAnswers = Common.SafeString(Request.Form("hasJointWalletAnswer")) = "Y"
			Dim isExecuteAnswer As Boolean = Common.ExecWalletAnswers(_CurrentWebsiteConfig, sWalletQuestionsAndAnswers, questionList_persist, idaResponseIDList, isJointAnswers, question_set_ID, request_app_ID, request_client_ID, request_sequence_ID, reference_number)
			Dim oRequestXML As XmlDocument = New XmlDocument()
			Dim responseMessage As String = ""
			oRequestXML.LoadXml(loanRequestXMLStr)
			If Not isExecuteAnswer Then
				''update comment to the internal comment: RUN LOAN IDA WHEN XA IS NOT CREATED: IDA FAILED.
				LoanSubmit.UpdateLoan(sLoanID, _CurrentWebsiteConfig, "LOAN_IDA_FAILED")
				Response.Write(customSubmitMessage)
				Return
			End If
			' populate questions for joint 
			If (IsJointApplication And Not isJointAnswers) Then
				''response wallet question for joint
				Dim sWalletQuestionResponse As String = Common.getWalletQuestionsResponseXML(_CurrentWebsiteConfig, loanResponseXMLStr, sLoanIDAType, IsJointApplication)
				If String.IsNullOrEmpty(sWalletQuestionResponse) Then
					Response.Write(customSubmitMessage)
				Else
					Dim sRenderWalletQuestions As String = Common.renderWalletQuestionsHTML(sLoanIDAType, sWalletQuestionResponse, questionList_persist, Co_FName, transaction_ID, question_set_ID, request_app_ID, request_client_ID, request_sequence_ID, reference_number)
					Response.Clear()
					Response.Write(sWalletQuestionResponse)	''response wallet question for joint
				End If
				Return
			End If
		End If
		''update comment to the internal comment: RUN LOAN IDA WHEN XA IS NOT CREATED: IDA PASSED.
		LoanSubmit.UpdateLoan(sLoanID, _CurrentWebsiteConfig, "LOAN_IDA_PASSED")
		Response.Write(customSubmitMessage)
	End Sub
#End Region
End Class
