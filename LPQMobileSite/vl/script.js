﻿var isSubmitWalletAnswer = false;
var isCoSubmitWalletAnswer = false;
var isSubmittingLoan = false;
var vinBarcodePicker;
var docScanObj, co_docScanObj;
(function (VL, $, undefined) {
    VL.DATA = {};
    VL.FACTORY = {};
    VL.FACTORY.toggleHasCoApplicant = function () {
        var $self = $(this);
        var $dataHolder = $("#" + $self.data("hidden-field"));
        if ($dataHolder.val() === "N") {
            $dataHolder.val("Y");
        } else {
            $dataHolder.val("N");
        }
        $self.toggleClass("active");
        $self.removeClass("btnHover");
    };
    VL.FACTORY.IsTradeInEnabled = function () {
		return $("#lnkHasTradeIn").data("status").toUpperCase() === "Y";
	};
	VL.FACTORY.toggleIsNewVehicle = function () {
        var $self = $(this);
        var $dataHolder = $("#hdVlIsNewVehicle");
        if ($self.is(":checked")) {
			if ($self[0].id === "chkVehicleNew") {
				$("#chkVehicleUsed").prop("checked", false).checkboxradio("refresh");
				$dataHolder.val("Y");
				//$('#divVehicleMileage').hide(400);
			} else {
				$("#chkVehicleNew").prop("checked", false).checkboxradio("refresh");
				$dataHolder.val("N");
				//$('#divVehicleMileage').show(400);
			}
        } else {
            $dataHolder.val("");
			//$('#divVehicleMileage').hide(400);
		}
		handleShowHideVehicleMileage();
	};
    VL.FACTORY.toggleKnowVehicleMake = function () {
    	var $self = $(this);
    	var $dataHolder = $("#hdVlKnowVehicleMake");
    	if ($self.is(":checked")) {
    		if ($self[0].id === "chkKnowMake") {
    			$("#chkDontKnowMake").prop("checked", false).checkboxradio("refresh");
    			$dataHolder.val("Y");
    			$('#divVehicleDetail').show(400);
		    } else {
    			$("#chkKnowMake").prop("checked", false).checkboxradio("refresh");
    			$dataHolder.val("N");
    			$('#divVehicleDetail').hide(400);
    			$.lpqValidate.hideValidation("#divVehicleMake");
    			$.lpqValidate.hideValidation("#txtVehicleModel");
    			$.lpqValidate.hideValidation("#txtVehicleYear");
		    }
    	} else {
    		$dataHolder.val("");
    		$('#divVehicleDetail').hide(400);
    		$.lpqValidate.hideValidation("#divVehicleMake");
    		$.lpqValidate.hideValidation("#txtVehicleModel");
    		$.lpqValidate.hideValidation("#txtVehicleYear");
    	}
	    handleShowHideVehicleMileage();
    };

    VL.FACTORY.showAccordingVehicleType = function (prefix) {
	    if (typeof prefix === "undefined") prefix = "";
        $('#div'+ prefix +'CarMake').hide();
        $('#div' + prefix + 'MotorCycleMake').hide();
        $('#div' + prefix + 'Make').hide();

        var vehType = $('#ddl' + prefix + 'VehicleType').val();
        switch (vehType) {
            case 'MOTORCYCLE':
            	$('#div' + prefix + 'MotorCycleMake').fadeIn('fast');
                break;
            case 'CAR':
            	$('#div' + prefix + 'CarMake').fadeIn('fast');
                break;
            default:
            	$('#div' + prefix + 'Make').fadeIn('fast');
                break;
        }
		if (prefix === "") {
			handleShowHideVehicleMileage();
		}
    };

    VL.FACTORY.hasLoanPurpose = function () {
        return $('#hdHasLoanPurpose').val().toUpperCase() === "Y";
    };
    VL.FACTORY.IsNewVehicle = function () {
        return $("#hdVlIsNewVehicle").val().toUpperCase() === "Y";
    };
    VL.FACTORY.knowVehicleMake = function () {
        return $("#hdVlKnowVehicleMake").val().toUpperCase() === "Y";
    };
    VL.FACTORY.vehicleType = function (prefix) {
	    if (typeof prefix === "undefined") prefix = "";
        var vehicleTypeElem = $("#ddl" + prefix + "VehicleType");
        if (vehicleTypeElem.val() == null) {//there no trigger event           
                vehicleTypeElem.val(document.getElementById("ddl" + prefix + "VehicleType")[0].value);
                if (vehicleTypeElem.find('option').length == 2) { //only one dropdown-->makes it by default
                    vehicleTypeElem.val(document.getElementById("ddl" + prefix + "VehicleType")[1].value);
                }
        }
        return vehicleTypeElem.val();
    };
    VL.FACTORY.vehicleMake = function () {
        var vehicleMake = "";
        if (VL.FACTORY.knowVehicleMake) {
            switch (VL.FACTORY.vehicleType().toUpperCase()) {
                case "CAR":
                    vehicleMake = $('#ddlCarMake').val();
                    break;
                case "MOTORCYCLE":
                    vehicleMake = $('#ddlMotorCycleMake').val();
                    break;
                default:
                    vehicleMake = $.trim($('#txtMake').val());
            }
        }
        return vehicleMake;
    };
    VL.FACTORY.tradeInVehicleMake = function () {
    	var vehicleMake = "";
    	switch (VL.FACTORY.vehicleType("TradeIn").toUpperCase()) {
    		case "CAR":
    			vehicleMake = $('#ddlTradeInCarMake').val();
    			break;
    		case "MOTORCYCLE":
    			vehicleMake = $('#ddlTradeInMotorCycleMake').val();
    			break;
    		default:
    			vehicleMake = $.trim($('#txtTradeInMake').val());
    	}
    	return vehicleMake;
    };
    VL.FACTORY.init = function () {
    	VL.FACTORY.showAccordingVehicleType();
    	VL.FACTORY.showAccordingVehicleType("TradeIn");
    	VL.FACTORY.ShowAndHideTxtLoanTerm(VL.FACTORY.vehicleType());
        VL.FACTORY.calculateLoanAmount();
        VL.FACTORY.vl1Init();  
        VL.FACTORY.refreshPurposeLabel();
	    registerProvideLoanInfoValidator();
	    //$('input.money').each(function () {
	    //    $(this).val(Common.FormatCurrency($(this).val(), true));
        //});
        
        $("#txtVinNumber").on("change keyup", function () {   
            var vehType = $('#ddlVehicleType option:selected').val();
            if (vehType != undefined && (vehType.toUpperCase() == "CAR" || vehType.toUpperCase() == "MOTORCYCLE")) {
                decodeVinNumber($.trim($(this).val()));
            }
	    });
        $("#txtVinNumber").on("paste", function (e) {
            var vehType = $('#ddlVehicleType option:selected').val();
            if (vehType != undefined && (vehType.toUpperCase() == "CAR" || vehType.toUpperCase() == "MOTORCYCLE")) {
                decodeVinNumber($.trim(e.originalEvent.clipboardData.getData('text')));
            }
	    });
	    $("#txtVinNumber").on("focus", function() {
	    	$("#divVinNumber .vin-scan-guide").removeClass("hidden");
	    	$("#divVinNumber .vin-scan-msg").text("");
	    	$("#divVinNumber .vin-scan-msg").addClass("hidden");
	    });
	    $("#vinscan").on("pageshow", function () {
	    	if ($("#scandit-vin-barcode-picker").length > 0 && $("#vinscan").data("no-camera") != true) {
			    ScanditSDK.CameraAccess.getCameras().then(function(cameraList) {
				    if (cameraList.length > 0) {
					    initVinLaserScan();
				    } else {
					    $("#vinscan .no-camera").removeClass("hidden");
				    }
				    return null;
			    }).catch(function(ex) {
				    //console.log(ex);
				    if (ex.name == "NotAllowedError") {
				    	//$("#vinscan .js-laser-vin-scan-container").find("a[data-rel='back']").trigger("click");
				    	$("#vinscan").data("no-camera", true);
				    	$("#vinscan .no-camera").removeClass("hidden");
				    } else {
					    $("#vinscan").data("no-camera", true);
					    $("#vinscan .no-camera").removeClass("hidden");
				    }
				    return null;
			    });
		    } else {
	    		$("#vinscan .no-camera").removeClass("hidden");
		    }
		    
	    });
	    $("#vinscan").on("pagehide", function () {
	    	if ($("#scandit-vin-barcode-picker").length > 0 && vinBarcodePicker) {
	    		vinBarcodePicker.destroy();
	    	}
	    	$("#vinscan .no-camera").addClass("hidden");
	    });
	    pushToPagePaths("vl1");
    };
    function initVinLaserScan() {
    	ScanditSDK.BarcodePicker.create(document.getElementById("scandit-vin-barcode-picker"), {
    		playSoundOnScan: true,
    		vibrateOnScan: true
    	}).then(function (barcodePicker) {
    		$("#vinscan").find(".scan-guide-text").removeClass("hidden");
    		vinBarcodePicker = barcodePicker;
    		// barcodePicker is ready here to be used
    		var scanSettings = new ScanditSDK.ScanSettings({
    			enabledSymbologies: ["code39"],
    			codeDuplicateFilter: 1000
    		});
    		barcodePicker.applyScanSettings(scanSettings);
    		$(".scandit-camera-switcher, .scandit-flash-white, .scandit-flash-color", ".scandit-barcode-picker ").addClass("hidden");
    		barcodePicker.onScan(function (scanResult) {
    			scanResult.barcodes.reduce(function (string, barcode) {
    				$("#txtVinNumber").val(barcode.data);
    				$("#txtVinNumber").trigger("change");
				    goToNextPage("#vl1");
			    }, "");
    		});
    		return null;
    	});
    }
    VL.FACTORY.pageSubmitInit = function () {
    	$("a[data-command='proceed-membership']").off("click").on("click", function () {
    		var $self = $(this);
    		//deactive others
    		$("div[data-section='proceed-membership']").find("a[data-command='proceed-membership']").each(function () {
    			$(this).removeClass("active");
    			handledBtnHeaderTheme($(this));
    		});
    		//set active for current option
    		$self.addClass("active");
    		$("#hdProceedMembership").val($self.data("key"));
    		$("div[data-section='proceed-membership']").trigger("change");

    	});
        //FOM membershipfee
    	$("#divShowFOM div.btn-header-theme").off("click").on("click", function () {
    	    var membershipFees = VL.FACTORY.calculateOneTimeMembershipFee($(this).attr('data-fom-name'));
            //update divMembership field
    	    $("#divMembershipFee").attr("data-membershipfee", membershipFees)
    	    $("#divMembershipFee").text(Common.FormatCurrency(membershipFees, true));
    	    //update total deposit field	   
    	    if (typeof loanProductFactory !== "undefined" && loanProductFactory !== null) {
    	        $('#txtFundingDeposit').val(Common.FormatCurrency(loanProductFactory.getTotalDeposit(), true));
    	    }
    	});

    	
    	//$("#divCcFundingOptions").find("a[data-value='NOT FUNDING AT THIS TIME']").parent().show();
    	$('#txtFundingDeposit').off("change").on("change", function () {
    		if (Common.GetFloatFromMoney($(this).val()) === 0) {
    			$("#divCcFundingOptions").find("a[data-value='NOT FUNDING AT THIS TIME']").parent().show();
    		} else {
    			$("#divCcFundingOptions").find("a[data-value='NOT FUNDING AT THIS TIME']").parent().hide();
    		}
    	}).trigger("change");
    	if (typeof loanProductFactory != "undefined") {
    		if (typeof loanProductFactory.refreshDepositTextFields == "function") {
    			loanProductFactory.refreshDepositTextFields();
			}
    	}
    	pushToPagePaths("pagesubmit");
    };
  
    VL.FACTORY.pageLastInit = function () {
    	if ($("#divXsellSection").length === 1) {
    		$("a.btn-header-theme", $("#divXsellSection")).on("click", function () {
    			$.mobile.loading("show", {
    				theme: "a",
    				text: "Loading ... please wait",
    				textonly: true,
    				textVisible: true
    			});
    		});
    	}
    }  
    VL.FACTORY.hasMakeAndModel = function () {
        return (VLPARAINFO.make !== "" && VLPARAINFO.model !== "");
    }
    VL.FACTORY.getMakeAndModelFromQuery = function () {
        var sMake = VLPARAINFO.make;
        var sModel = VLPARAINFO.model;
        if (VL.FACTORY.hasMakeAndModel()) {            
            $('#txtVehicleModel').val(sModel); 
            var vehType = $('#ddlVehicleType').val();
            switch (vehType) {
                case 'MOTORCYCLE':
                    $('#ddlMotorCycleMake').val(sMake.toUpperCase()).selectmenu().selectmenu('refresh');                  
                    break;
                case 'CAR':
                    $('#ddlCarMake').val(sMake.toUpperCase()).selectmenu().selectmenu('refresh');  
                    break;
                default:
                    $('#txtMake').val(sMake);
                    break;
            }
        }
    }
    //be called right after page #vl1 show to inital and rebind event for elements on page #vl1
    VL.FACTORY.vl1Init = function () {
        VL.FACTORY.getMakeAndModelFromQuery();
        if (VL.FACTORY.hasLoanPurpose()) {    
            var $loanEle = $('#divLoanPurpose a[data-command="loan-purpose"]');
            if ($loanEle.length == 1) {
                //if only one loan purpose --> preselected and hide the loan purpose    
               $(window).on('load', function () {
                   $loanEle.trigger('click');              
              });
               $('#divLoanPurpose').hide();
            } else {
                $('#divLoanPurpose').show();
          }     
        } else {
            //reset current value of Loan Purpose to prevent expected bugs
            $('#hdLoanPurpose').val("");
            $('#divLoanPurpose').hide();
        }
        $("#btnVlHasCoApplicant").off("click").on("click", VL.FACTORY.toggleHasCoApplicant);
       
        $("#chkVehicleNew, #chkVehicleUsed").on("change", VL.FACTORY.toggleIsNewVehicle);
        $("#chkKnowMake, #chkDontKnowMake").on("change", VL.FACTORY.toggleKnowVehicleMake);
        //$("#ddlLoanPurpose").on("change", function () {
        //    VL.FACTORY.refreshPurposeLabel();
        //    VL.FACTORY.calculateLoanAmount();
        //});
        $("#txtEstimatedVehicleValue, #txtDownPayment").on("blur", VL.FACTORY.calculateLoanAmount);
        $("#txtEstimatedVehicleValue, #txtDownPayment").on("keyup paste", function () {
        	setTimeout(VL.FACTORY.calculateLoanAmount, 400);
        });
        $("#ddlVehicleType").on("change", function () {
            VL.FACTORY.getMakeAndModelFromQuery();
            VL.FACTORY.showAccordingVehicleType();
            VL.FACTORY.ShowAndHideTxtLoanTerm($(this).val());
        });
        $("#ddlTradeInVehicleType").on("change", function() { VL.FACTORY.showAccordingVehicleType("TradeIn"); });
        $('#txtVehicleMileage').blur(function () {
            $(this).val(Common.GetRtNumber($(this).val()));
        });
        $('#txtVehicleYear').blur(function () {
            $(this).val(Common.GetPosNumber($(this).val(), 4));
        });
        VL.FACTORY.bindEventLoanPurposeOption();

        //enable sumbit buttons
        if($('#pagesubmit .div-continue-button').hasClass('ui-disabled')){
            $('#pagesubmit .div-continue-button').removeClass('ui-disabled');
        }
        if ($('#walletQuestions .div-continue-button').hasClass('ui-disabled')) {
            $('#walletQuestions .div-continue-button').removeClass('ui-disabled');
        }
        if ($('#co_walletQuestions .div-continue-button').hasClass('ui-disabled')) {
            $('#co_walletQuestions .div-continue-button').removeClass('ui-disabled');
        }
     
    };
    VL.FACTORY.vl2Init = function (isCoApp) {
        var currPrefillData = null;
        if (isCoApp) {
            currPrefillData = COPREFILLDATA;
        } else {
            currPrefillData = PREFILLDATA;
        }
        if (currPrefillData != null) {
            $.each(currPrefillData, function (id, value) {
                var $ele = $("#" + id);
                if ($ele.is("input") && ($ele.attr("type") === "text" || $ele.attr("type") === "tel" || $ele.attr("type") === "password" || $ele.attr("type")==="email")) {
                    $ele.val(value);
                    //$ele.trigger("blur");
                } else if ($ele.is("select")) {
                    if (id.indexOf('preferredContactMethod') > -1) {
                        value = mappingPreferredContactValue(value);
                        $ele.val(value);
                    } else {
                        $ele.val(value);
                    }
                    $ele.selectmenu("refresh");
                    $ele.trigger("change");
                }
            });
            //show mailing address if it exist
            var sMailingStreet = $('#' + coPrefix + 'txtMailingAddress').val()
            if (sMailingStreet != undefined && sMailingStreet != "") {
                $('#' + coPrefix + 'lnkHasMaillingAddress').attr('status', 'Y');
                $('#' + coPrefix + 'divMailingAddressForm').show();
            }
        }
        //reset to ensure prefill data only one time
        currPrefillData = null;
    };
    VL.FACTORY.AutoTermTextboxEnable = function () {
        return $("#hdAutoTermTextboxEnable").val() == "Y";
    }
    VL.FACTORY.ShowAndHideTxtLoanTerm = function (loanType) {
        if (loanType == "CAR" && !VL.FACTORY.AutoTermTextboxEnable()) {
            $('#txtLoanTerm').closest('div[data-role="fieldcontain"]').hide();
            $('#ddlLoanTerm').closest('div[data-role="fieldcontain"]').show();
        } else {     
            $('#txtLoanTerm').closest('div[data-role="fieldcontain"]').show();
            $('#ddlLoanTerm').closest('div[data-role="fieldcontain"]').hide();
        }
    }
    VL.FACTORY.refreshPurposeLabel = function () {        
        if (VL.FACTORY.hasLoanPurpose()) { //make sure loan purpose is not null
            var loanBalanceTitle = "Estimated Vehicle Value<span class='require-span'>*</span>";
            var purchasePriceTitle = "Estimated Purchase Price<span class='require-span'>*</span>";
            //if ($('#hfLenderRef').val().toUpperCase().indexOf('INSPIRUS') != -1) {
            //    purchasePriceTitle = "Loan Amount<span class='require-span'>*</span>";
            //}
            var selectedLoanPurpose = $("#hdLoanPurpose").val();
            var selectedCategory = getSelectedCategory(selectedLoanPurpose);
            if (selectedCategory.toUpperCase().indexOf("REFI") > -1) { //check category 
                $('#divPurchaseAmountLabel').html(loanBalanceTitle);
                handleREFINANCELoanPurpose(true);
            } else {
                if (selectedLoanPurpose.toUpperCase().indexOf("REFI") > -1 || selectedLoanPurpose.toUpperCase().indexOf("BUYOUT") > -1) {
                    $('#divPurchaseAmountLabel').html(loanBalanceTitle);
                    if (selectedLoanPurpose.toUpperCase().indexOf('REFI') > -1) {
                    	handleREFINANCELoanPurpose(true);
                    } else {
                    	handleREFINANCELoanPurpose(false);
                    }
                } else {
                    $('#divPurchaseAmountLabel').html(purchasePriceTitle);                 
                        handleREFINANCELoanPurpose(false);          
                }
            }
        } //end hasloanpurpose
        var selectedText = LOANPURPOSELIST[selectedLoanPurpose];
        if ((!g_IsHideGMI && selectedText != undefined && selectedText.toUpperCase().indexOf('HMDA') != -1)
            || (!g_IsHideGMI && selectedLoanPurpose != undefined && selectedLoanPurpose.toUpperCase().indexOf('HMDA') != -1)) {
            $('.div_gmi_section').css('display', 'block');
        } else {
            $('.div_gmi_section').css('display', 'none');
        }
	    handleShowHideVehicleMileage();
    };

    function handleREFINANCELoanPurpose(isSelected) {
       
	    if (isSelected === true) {
			//automatically changes: NewVehicle=N and hide this fields
			$("#chkVehicleUsed").prop("checked", true).checkboxradio("refresh");
			VL.FACTORY.toggleIsNewVehicle.call($("#chkVehicleUsed"));
			$("#divNewOrUsedVehicleSection").hide();
			//automatically changes: MakeModelKnown=Y, and hide this fields
            $("#chkKnowMake").prop("checked", true).checkboxradio("refresh");           
            VL.FACTORY.toggleKnowVehicleMake.call($("#chkKnowMake"));
			$("#divKnowMakeAndModelSection").hide();
			//selected refinance ->hide down payment amount
			$('#divDownPayment').hide(); 
			$.lpqValidate.hideValidation("#divDownPayment");
			$("#divTradeInPanel").hide(); //hide Trade In link section
			$("#lnkHasTradeIn").html("Add a trade-in").data("status", "N").hide();
			$("#lnkHasTradeIn").addClass("plus-circle-before").removeClass("minus-circle-before");
			$.lpqValidate.hideValidation("#ddlTradeInVehicleType");
			$.lpqValidate.hideValidation("#divTradeInVehicleMake");
			$.lpqValidate.hideValidation("#txtTradeInVehicleModel");
			$.lpqValidate.hideValidation("#txtTradeInVehicleYear");
			$.lpqValidate.hideValidation("#txtTradeInVehicleCurrentValue");
			$.lpqValidate.hideValidation("#txtTradeInVehicleOwn");
			$.lpqValidate.hideValidation("#txtTradeInVehiclePayingMonthly");
			//Requested Loan Amount field verbiage changes to "Amount Refinanced"
			$("label[for='txtProposedLoanAmount']").html("Requested Refinance Amount");
        } else {
            var isNew = VLPARAINFO.isNew;
            if (isNew !=="") {
	            if (isNew.toUpperCase() == "Y") {
	                $("#chkVehicleUsed").prop("checked", false).checkboxradio("refresh");
                    $("#chkVehicleNew").prop("checked", true).checkboxradio("refresh");
                    $("#chkVehicleNew").trigger('change');
	            } else {
	                $("#chkVehicleUsed").prop("checked", true).checkboxradio("refresh");
                    $("#chkVehicleNew").prop("checked", false).checkboxradio("refresh");   
                    $("#chkVehicleUsed").trigger('change');
                }
                
	        }else{
	            $("#chkVehicleUsed").prop("checked", false).checkboxradio("refresh");
                $("#chkVehicleNew").prop("checked", false).checkboxradio("refresh");
                VL.FACTORY.toggleIsNewVehicle.call($("#chkVehicleUsed"));
	        }
			$("#divNewOrUsedVehicleSection").show();
			if (VL.FACTORY.hasMakeAndModel()) {
			    $("#chkKnowMake").prop("checked", true).checkboxradio("refresh");
			} else {
			    $("#chkKnowMake").prop("checked", false).checkboxradio("refresh");
			}
			$("#chkDontKnowMake").prop("checked", false).checkboxradio("refresh");
			VL.FACTORY.toggleKnowVehicleMake.call($("#chkKnowMake"));
			$("#divKnowMakeAndModelSection").show();
			$.lpqValidate.hideValidation("#divNewOrUsedVehicle");
			$.lpqValidate.hideValidation("#divKnowMakeAndModel");
			$('#divDownPayment').show();
			$("#divTradeInPanel").hide();
			$("#lnkHasTradeIn").html("Add a trade-in").data("status", "N").show();
			$("#lnkHasTradeIn").addClass("plus-circle-before").removeClass("minus-circle-before");
			$("label[for='txtProposedLoanAmount']").html("Requested Loan Amount");
		}
		if (BUTTONLABELLIST != null) {
			var value = BUTTONLABELLIST[$.trim($("#lnkHasTradeIn").html()).toLowerCase()];
			if (typeof value == "string" && $.trim(value) !== "") {
				$("#lnkHasTradeIn").html(value);
			}
		}
		$.lpqValidate.hideValidation("#divVehicleMake");
		$.lpqValidate.hideValidation("#txtVehicleModel");
		$.lpqValidate.hideValidation("#txtVehicleYear");
	}

	function handleShowHideVehicleMileage() {
	    //hide Vehicle Mileage if MakeModelKnown=N or VehicleType=BOAT
	    var vehicleType = "";
	    if ($("#ddlVehicleType").val() != undefined) {
	        vehicleType = $("#ddlVehicleType").val().toUpperCase();
	    }
	    if ($("#hdVlKnowVehicleMake").val() === "N" || vehicleType === "BOAT" || $("#hdVlIsNewVehicle").val() !== "N") {
			$("#divVehicleMileage").hide(400);
		} else {
			$("#divVehicleMileage").show(400);
		}
	}

    VL.FACTORY.calculateLoanAmount = function () {
        var estVehValue = Common.GetFloatFromMoney($('#txtEstimatedVehicleValue').val());
        var downPayment = Common.GetFloatFromMoney($('#txtDownPayment').val());
        var selectedCategory = getSelectedCategory($('#hdLoanPurpose').val());
        var x = estVehValue == '' ? 0 : estVehValue;
        var y = downPayment == '' ? 0 : downPayment;
        //if selected refinance -> no down payment
        if (VL.FACTORY.hasLoanPurpose()) { //make sure the ddlLoanPurpose is existed
            if ($("#hdLoanPurpose").val().toUpperCase().indexOf("REFI") > -1 || selectedCategory.toUpperCase().indexOf("REFI")>-1) {
                //get loan amount from user input
                //allow refi loan amount greater than value of car
                $('#txtProposedLoanAmount').removeAttr("readonly");
                $('#txtProposedLoanAmount').removeAttr("style");
            } else {//need to calculate loan amount
                if (x < y || (x == 0 && y == 0)) {
                    $('#txtProposedLoanAmount').val(Common.FormatCurrency(0, true));
                } else {
                    if (x >= 0 && y >= 0) {
                        $('#txtProposedLoanAmount').val(Common.FormatCurrency(x - y, true));
                    } else {
                        $('#txtProposedLoanAmount').val(Common.FormatCurrency(0, true));
                    }
	                $('#txtProposedLoanAmount').trigger("change");
                }
                //restore attribute previous removed by refi purpose
                if ($('#txtProposedLoanAmount').attr("readonly") == undefined) {
                    $('#txtProposedLoanAmount').attr("readonly", "readonly");
                }
            }
        } else {//loanpurose is not exist
            if (x >= y) {
                var result = Common.FormatCurrency(x - y, true);
                $('#txtProposedLoanAmount').val(result);
            } else {
                $('#txtProposedLoanAmount').val(Common.FormatCurrency(0, true));
            }
            $('#txtProposedLoanAmount').trigger("change");
        }
    };
    VL.FACTORY.assignLoanPurpose = function (loanPurpose) {
        if ($.trim(loanPurpose) !== "") {
            //var $ddlLoanPurpose = $("#ddlLoanPurpose");
            ////perform this way to prevent assign unbounded value to selectbox
            //$ddlLoanPurpose.find("option").each(function (idx, element) {
            //    if ($(element).val().toUpperCase() == loanPurpose.toUpperCase()) {
            //        $(element).attr("selected", "selected");
            //    }
            //});
            //if ($ddlLoanPurpose.data("mobile-selectmenu") === undefined) {
            //    $('#ddlLoanPurpose').selectmenu(); //not initialized yet, lets do it
            //}
            //$ddlLoanPurpose.selectmenu('refresh');
            //$ddlLoanPurpose.trigger("change");
            $("#divLoanPurpose").find("a[data-command='loan-purpose'][data-key='" + loanPurpose.toUpperCase() + "']").trigger("click");          
        }
    };
    VL.FACTORY.assignVehicleType = function (vehicleType) {
        if ($.trim(vehicleType) !== "") {
            var $ddlVehicleType = $("#ddlVehicleType");
            //perform this way to prevent assign unbounded value to selectbox
            $ddlVehicleType.find("option").each(function (idx, element) {
                if ($(element).val().toUpperCase() == vehicleType.toUpperCase()) {
                	//$(element).attr("selected", "selected");
                	$ddlVehicleType.val($(element).val());
                }
            });
            if ($ddlVehicleType.data("mobile-selectmenu") === undefined) {
                $('#ddlVehicleType').selectmenu(); //not initialized yet, lets do it
            }
            $ddlVehicleType.selectmenu('refresh');
            $ddlVehicleType.trigger("change");
        }
    }
    VL.FACTORY.assignTerm = function (term) {
        if ($.trim(term) !== "") {
            if ($("#ddlVehicleType").val() == "CAR" && !VL.FACTORY.AutoTermTextboxEnable()) {
                var $ddlLoanTerm = $("#ddlLoanTerm");
                //perform this way to prevent assign unbounded value to selectbox
                $ddlLoanTerm.find("option").each(function (idx, element) {
                    if ($(element).val().toUpperCase() == term.toUpperCase()) {
                    	//$(element).attr("selected", "selected");
                    	$ddlLoanTerm.val($(element).val());
                    }
                });
                if ($ddlLoanTerm.data("mobile-selectmenu") === undefined) {
                    $('#ddlLoanTerm').selectmenu(); //not initialized yet, lets do it
                }
                $ddlLoanTerm.selectmenu('refresh');
                $ddlLoanTerm.trigger("change");                       
            } else {
                $('#txtLoanTerm').val(term);
            }
        }
    }
    function prepareAbrFormValues(pageId) {
    	var loanInfo = getVehicleLoanInfo();
    	var formValues = {};
    	_.forEach(pagePaths, function (p) {
    		collectFormValueData(p.replace("#", ""), loanInfo, formValues);
    		if (pageId == p.replace("#", "")) return false;
    	});
    	return formValues;
    }

	/**
	 * Converts the loanInfo, which is normally used on submission, into a data structure meant for Applicant Blocking Logic (ABR).
	 * This also only pulls in the information needed for each page. ABR should only work with data on the page the user is on
	 * and the pages before that.
	 * @param {string} pageId
	 * @param {Object} loanInfo
	 * @param {Object} formValues
	 */
    function collectFormValueData(pageId, loanInfo, formValues) {
    	switch (pageId) {
    		case "vl1":
    			if ($("#txtLocationPoolCode").length > 0) {
    				formValues["locationPool"] = ZIPCODEPOOLLIST[$("#txtLocationPoolCode").val()];
    			} else if ($("#vl_txtLocationPoolCode").length > 0) {
    				formValues["locationPool"] = VLPRODUCTS.ZipPoolIds[$("#vl_txtLocationPoolCode").val()];
    			}
    			formValues["amountRequested"] = loanInfo.ProposedLoanAmount;
    			formValues["vehicleType"] = [loanInfo.VehicleType];
				formValues["vehicleLoanPurpose"] = [loanInfo.LoanPurpose];

				collectCustomQuestionAnswers("Application", "LoanPage", null, loanInfo, formValues);
				collectCustomQuestionAnswers("Applicant", "LoanPage", null, loanInfo, formValues);

    			break;
    		case "vl2":
                var _dob = moment(loanInfo.DOB, "MM-DD-YYYY");
                if (Common.IsValidDate(loanInfo.DOB)) {
    				formValues["age"] = moment().diff(_dob, "years", false);
    			}
    			formValues["citizenship"] = loanInfo.CitizenshipStatus;
    			formValues["employeeOfLender"] = loanInfo.EmployeeOfLender;
    			formValues["employmentLength"] = (parseInt(loanInfo.txtEmployedDuration_year) || 0) + (parseInt(loanInfo.txtEmployedDuration_month) || 0) / 12;
    			formValues["employmentStatus"] = loanInfo.EmploymentStatus;
    			formValues["isJoint"] = (loanInfo.HasCoApp == "Y");
    			formValues["memberNumber"] = loanInfo.MemberNumber;
    			formValues["occupancyLength"] = loanInfo.LiveMonths / 12;
    			formValues["occupancyStatus"] = loanInfo.OccupyingLocation;

				collectCustomQuestionAnswers("Applicant", "ApplicantPage", "", loanInfo, formValues);

    			break;
    		case "vl6":
                var _dob = moment(loanInfo.co_DOB, "MM-DD-YYYY");
                if (Common.IsValidDate(loanInfo.co_DOB)) {
    				formValues["age"] = moment().diff(_dob, "years", false);
    			}
    			formValues["citizenship"] = loanInfo.co_CitizenshipStatus;
    			formValues["employeeOfLender"] = loanInfo.co_EmployeeOfLender;
    			formValues["employmentLength"] = (parseInt(loanInfo.co_txtEmployedDuration_year) || 0) + (parseInt(loanInfo.co_txtEmployedDuration_month) || 0) / 12;
    			formValues["employmentStatus"] = loanInfo.co_EmploymentStatus;
    			formValues["memberNumber"] = loanInfo.co_MemberNumber;
    			formValues["occupancyLength"] = loanInfo.co_LiveMonths / 12;
    			formValues["occupancyStatus"] = loanInfo.co_OccupyingLocation;

				collectCustomQuestionAnswers("Applicant", "ApplicantPage", "co_", loanInfo, formValues);

    			break;
    		case "pagesubmit":

				collectCustomQuestionAnswers("Application", "ReviewPage", null, loanInfo, formValues);
				collectCustomQuestionAnswers("Applicant", "ReviewPage", null, loanInfo, formValues);

    			break;
    		default:
    			break;
    	}
    	return formValues;
    }
    
    VL.FACTORY.validateVl1 = function (element) {
    	var currElement = $(element);
    	if (currElement.attr("contenteditable") == "true") return false;
        var validator = true;

        var loanInfoValidate = $.lpqValidate("ValidateProvideLoanInfo");
        var branchValidate = $.lpqValidate("ValidateBranch");
        if (loanInfoValidate === false || branchValidate === false) {
	        validator = false;
        }

        if ($("#vl1").find("#divShowFOM").length > 0 && $.lpqValidate("ValidateFOM") == false) {
            validator = false;
        }       
        if ($("#vl1").find("#divDisclosure").length > 0 && $.lpqValidate("ValidateDisclosure_disclosures") == false) {
            validator = false;
		}

		// Validate custom questions, which may be moved to the front page
		if ($('#loanpage_divApplicantQuestion').children().length > 0) {
			if ($.lpqValidate("loanpage_ValidateApplicantQuestionsXA") === false) {
				validator = false;
			}
		}

        if (validator) {
        	if (window.ABR) {
        		var formValues = prepareAbrFormValues("vl1");
        		if (ABR.FACTORY.evaluateRules("vl1", formValues)) {
			        return false;
		        }
        	}
			if (window.IST) {
				IST.FACTORY.carrierIdentification();
			} else {
				if ($("#hdIs2ndLoanApp").val() == "Y") {
					VL.FACTORY.renderReviewContent();
				}
				currElement.next().next().trigger('click');
			}
		} else {
			Common.ScrollToError();
		}
    };
    VL.FACTORY.validateVl2 = function (element) {
        var currElement = $(element);
        //update hiddenHasCoApplicant
        if (currElement.attr('id') == 'continueWithCoApp') {
            $("#hdVlHasCoApplicant").val('Y');
        } else {
            $("#hdVlHasCoApplicant").val('N');
        }
    	if (currElement.attr("contenteditable") == "true") return false;
        //$('#txtErrorMessage').html('');//useless
        var strMessage = '';
        var validator = true;
        if (ValidateApplicantInfo() == false) {
	        validator = false;
        }
        if (ValidateApplicantContactInfoXA() == false) {
        	validator = false;
        }
        if (ValidateApplicantAddress() == false) {
        	validator = false;
        }
        if (ValidateApplicantPreviousAddress() == false) {
        	validator = false;
        }
        if (ValidateApplicantMailingAddress() == false) {
        	validator = false;
        }
        if ($("#hdEnableIDSection").val() === "Y") {
        	if (ValidateApplicantIdXA() == false) {
        		validator = false;
        	}
        }
        if ($("#hdEnableReferenceInformation").val() == "Y") {
        	if (ValidateReferenceInfo() == false) {
        		validator = false;
        	}
        }
        if (ValidateApplicantFinancialInfo() == false) {
        	validator = false;
        }
        if ($("#divDeclarationSection").length > 0 && $.lpqValidate("ValidateDeclaration") === false) {
        	validator = false;
        }
        if (typeof docUploadObj != "undefined" && docUploadObj != null && docUploadObj.validateUploadDocument() == false) {
        	validator = false;
        }
        if(ValidateAssets() == false){
            validator = false;
        }

		if ($('#divApplicantQuestion').children().length > 0) {
			if ($.lpqValidate("ValidateApplicantQuestionsXA") === false) {
				validator = false;
			}
		}

        var requiredDLMessage = validateRequiredDLScan($('#hdRequiredDLScan').val(), false);
        if (requiredDLMessage != "") {
        	strMessage = requiredDLMessage;
        	validator = false;
        }
        if (validator === false) {
        	//for backward compatibility, need to show error popup/modal for some function which is not applied inline error message
        	if (strMessage !== "") {
        		if ($("#divErrorPopup").length > 0) {
        			$('#txtErrorPopupMessage').html(strMessage);
        			$("#divErrorPopup").popup("open", { "positionTo": "window" });
        		} else {
        			$('#txtErrorMessage').html(strMessage);
        			currElement.next().trigger('click');
        		}
        	} else {
        		Common.ScrollToError();
        	}
        } else {
        	if (window.ABR) {
        		var formValues = prepareAbrFormValues("vl2");
        		if (ABR.FACTORY.evaluateRules("vl2", formValues)) {
        			return false;
        		}
        	}
			if ($("#hdVlHasCoApplicant").val() === "Y") {
        		goToNextPage("#vl6");
			} else {
				if (window.ASI) {
					var appInfo = {};
					SetApplicantInfo(appInfo);
					SetApplicantAddress(appInfo);
					ASI.FACTORY.runNssWf1(appInfo.AddressState, appInfo.MaritalStatus, goToPageSubmit);
				} else {
					goToPageSubmit();
				}
        	}
        }
    };
    VL.FACTORY.validateVl6 = function (element) {
    	var currElement = $(element);
    	if (currElement.attr("contenteditable") == "true") return false;
        $('#txtErrorMessage').html('');
        var strMessage = '';
        var validator = true;
        if (co_ValidateApplicantInfo() == false) {
	        validator = false;
        }
        if (co_ValidateApplicantContactInfoXA() == false) {
        	validator = false;
        }
        if (co_ValidateApplicantAddress() == false) {
        	validator = false;
        }
        if (co_ValidateApplicantPreviousAddress() == false) {
        	validator = false;
        }
        if (co_ValidateApplicantMailingAddress() == false) {
        	validator = false;
        }
        
        if ($("#hdEnableIDSectionForCoApp").val() === "Y") {
        	if (co_ValidateApplicantIdXA() == false) {
        		validator = false;
        	}
        }
        if ($("#hdEnableReferenceInformationForCoApp").val() == "Y") {
        	if (co_ValidateReferenceInfo() == false) {
        		validator = false;
        	}
        }
        if (co_ValidateApplicantFinancialInfo() == false) {
        	validator = false;
        }
        if ($("#co_divDeclarationSection").length > 0 && $.lpqValidate("co_ValidateDeclaration") === false) {
        	validator = false;
        }
        if (typeof co_docUploadObj != "undefined" && co_docUploadObj != null && co_docUploadObj.validateUploadDocument() == false) {
        	validator = false;
        }
        if(co_ValidateAssets() == false){
            validators = false;
		}

		if ($('#co_divApplicantQuestion').children().length > 0) {
			if ($.lpqValidate("co_ValidateApplicantQuestionsXA") === false) {
				validator = false;
			}
		}
        var requiredDLMessage = validateRequiredDLScan($('#hdRequiredDLScan').val(), true);
        if (requiredDLMessage != "") {
        	strMessage = requiredDLMessage;
        	validator = false;
        }
        if (validator === false) {
        	//for backward compatibility, need to show error popup/modal for some function which is not applied inline error message
        	if (strMessage !== "") {
        		if ($("#divErrorPopup").length > 0) {
        			$('#txtErrorPopupMessage').html(strMessage);
        			$("#divErrorPopup").popup("open", { "positionTo": "window" });
        		} else {
        			$('#txtErrorMessage').html(strMessage);
        			currElement.next().trigger('click');
        		}
        	} else {
        		Common.ScrollToError();
        	}
        } else {
        	if (window.ABR) {
        		var formValues = prepareAbrFormValues("vl6");
        		if (ABR.FACTORY.evaluateRules("vl6", formValues)) {
        			return false;
        		}
        	}
        	if (window.ASI) {
        		var appInfo = {};
        		SetApplicantInfo(appInfo);
        		SetApplicantAddress(appInfo);
        		co_SetApplicantInfo(appInfo);
        		co_SetApplicantAddress(appInfo);
        		ASI.FACTORY.runNssWf2(appInfo.AddressState, appInfo.MaritalStatus, appInfo.co_RelationshipToPrimary, appInfo.co_AddressState, appInfo.co_MaritalStatus, goToPageSubmit);
        	} else {
        		goToPageSubmit();
        	}
        }
    };
    VL.FACTORY.renderReviewContent = function () {
    	viewSelectedBranch(); //view selected branch
        $(".ViewVehicleLoanInfo").html(viewVehicleLoanInfo.call($(".ViewVehicleLoanInfo")));
	    if ($("#hdIs2ndLoanApp").val() == "Y") return;
	    $(".ViewAcountInfo").html(ViewAccountInfo.call($(".ViewAcountInfo")));
	    if ($("#hdEnableReferenceInformation").val() == "Y") {
		    $(".ViewReferenceInfo").html(ViewReferenceInfo.call($(".ViewReferenceInfo")));
	    }
	    $(".ViewContactInfo").html(ViewContactInfo.call($(".ViewJointApplicantContactInfo")));
        $(".ViewAddress").html(ViewAddress.call($(".ViewAddress")));
        if ($("#hdEnableIDSection").val() === "Y") {
        	$(".ViewApplicantID").html(ViewPrimaryIdentification.call($(".ViewApplicantID")));
        }
        $(".ViewFinancialInfo").html(ViewFinancialInfo.call($(".ViewFinancialInfo")));
        $(".ViewPrevEmploymentInfo").html(ViewPrevEmploymentInfo.call($(".ViewPrevEmploymentInfo")));
        if ($("#divDeclarationSection").length > 0) {
        	$(".ViewDeclaration").html(ViewDeclaration.call($(".ViewDeclaration")));
        }
        if ($('#divApplicantQuestion').length > 0) {
            $(".ViewApplicantQuestion").html(ViewApplicantQuestion.call($(".ViewApplicantQuestion")));
        }

        if ($("#hdVlHasCoApplicant").val() === "Y") {
            $(".ViewJointApplicantInfo").html(co_ViewAccountInfo.call($(".ViewJointApplicantInfo")));
	        if ($("#hdEnableReferenceInformationForCoApp").val() == "Y") {
		        $(".ViewJointReferenceInfo").html(co_ViewReferenceInfo.call($(".ViewJointReferenceInfo")));
	        }
	        $(".ViewJointApplicantContactInfo").html(co_ViewContactInfo.call($(".ViewJointApplicantContactInfo")));
            $(".ViewJointApplicantAddress").html(co_ViewAddress.call($(".ViewJointApplicantAddress")));
            if ($("#hdEnableIDSectionForCoApp").val() === "Y") {
            	$(".ViewJointApplicantID").html(co_ViewPrimaryIdentification.call($(".ViewJointApplicantID")));
            }
            $(".ViewJointApplicantFinancialInfo").html(co_ViewFinancialInfo.call($(".ViewJointApplicantFinancialInfo")));
            $(".ViewJointApplicantPrevEmploymentInfo").html(co_ViewPrevEmploymentInfo.call($(".ViewJointApplicantPrevEmploymentInfo")));
            if ($("#co_divDeclarationSection").length > 0) {
            	$(".ViewJointApplicantDeclaration").html(co_ViewDeclaration.call($(".ViewJointApplicantDeclaration")));
            }
            if ($('#co_divApplicantQuestion').length > 0) {
                $(".ViewJointApplicantQuestion").html(co_ViewApplicantQuestion.call($('.ViewJointApplicantQuestion')));
            }

        } else {
            $(".jna-panel").hide();
        }
        $("div.review-container div.row-title b").each(function (idx, ele) {
        	if (typeof $(ele).data("renameid") == "undefined") {
        		var dataId = getDataId();
        		$(ele).attr("data-renameid", dataId);
        		RENAME_REPOSITORY[dataId] = htmlEncode($(ele).html());
        	}
        });
    };

    VL.FACTORY.ShowHideTradeInPanel = function (e) {
    	var trigger = e.target;
		if (trigger.tagName != "A") {
			trigger = $(trigger).parent();
		}
		var selectedValue = $(trigger).data('status');
		if (selectedValue == "N") {
			$(trigger).html('Remove trade-in');
			$("#lnkHasTradeIn").addClass("minus-circle-before").removeClass("plus-circle-before");
			$(trigger).data('status', 'Y');
			$('#divTradeInPanel').show();
		} else {
			$(trigger).html('Add a trade-in');
			$("#lnkHasTradeIn").addClass("plus-circle-before").removeClass("minus-circle-before");
			$(trigger).data('status', 'N');
			$('#divTradeInPanel').hide();
		}
		if (BUTTONLABELLIST != null) {
			var value = BUTTONLABELLIST[$.trim($(trigger).html()).toLowerCase()];
			if (typeof value == "string" && $.trim(value) !== "") {
				$(trigger).html(value);
			}
		}
	    e.preventDefault();

    };
    VL.FACTORY.calculateOneTimeMembershipFee = function (sFOMName) {
        var membershipFee = 0.0;
        if ($("#hdIsComboMode").val() == "Y" && $("#pagesubmit").find("#divShowFOM").length > 0) {
            membershipFee = parseFloat($("#hdMembershipFee").val());
            if (sFOMName != undefined) {
                if (typeof CUSTOMLISTFOMFEES[sFOMName] === "string") {
                    membershipFee = membershipFee + parseFloat(CUSTOMLISTFOMFEES[sFOMName]);
                }
            }
        }
        return membershipFee;
    };
    VL.FACTORY.registerViewVehicleLoanInfoValidator = function () {
        $('#reviewPanel').observer({
            validators: [
                function (partial) {    
                    if ($("#hdIs2ndLoanApp").val() == "Y") { //second loan app only has loan info and review pages
                        if ($('#reviewPanel .ViewVehicleLoanInfo').children().find("div").length == 0) {
                            return "Please complete all required fields. ";
                        }
                    } else { 
                        //do the view applicant information validation instead of view vehicle loan info validation in case some lenders do not have vehicle loan info              
                        if ($('#reviewPanel .ViewAcountInfo').children().find("div").length == 0) {
                            return "Please complete all required fields. ";
                        }
                    }                 
                    return "";
                }
            ],
            validateOnBlur: false,
            group: "ValidateViewVehicleLoanInfo"
        });
    };
    
    //VL.FACTORY.validateUploadDocs = function (element, isCoApp) {
    //    var currElement = $(element);
    //    var strMessage = "";
    //    if (typeof isCoApp == "undefined" || isCoApp == false) {
    //        strMessage += validateUploadDocument();
    //    } else {
    //        strMessage += co_validateUploadDocument();
    //    }
    //    if (strMessage != '') {
    //        $('#txtErrorMessage').html(strMessage);
    //        currElement.next().trigger('click');
    //    } else {
    //        currElement.next().next().trigger('click');
    //    }
    //};

    //deprecated, not use anywhere
    function validateVehicleLoanInfo() {
        var strMessage = "";
        var loanPurpose = $('#hdLoanPurpose').val().toUpperCase();
        var vehicleType = $.trim(VL.FACTORY.vehicleType()).toUpperCase();
        var hasLoanPurpose = VL.FACTORY.hasLoanPurpose();
        var knowVehicleMake = VL.FACTORY.knowVehicleMake();
        var txtVehicleModel = $('#txtVehicleModel').val();
        var txtVehicleYear = $('#txtVehicleYear').val();
        var txtEstimatedVehicleValue = $('#txtEstimatedVehicleValue').val();
        var ddlLoanTerm = $('#ddlLoanTerm').val();
      //  var ddlBranches = $('#ddlBranches').val();
       // var iBranchVisible = $('#VLBranchesDiv').length;

        var downPayment = $('#txtDownPayment').val();
        var txtProposedLoanAmount = $('#txtProposedLoanAmount').val();
        var selectedCategory = getSelectedCategory($('#hdLoanPurpose').val());
        if (hasLoanPurpose) { //validate loan purpose if it exists
            if (loanPurpose === "")
                strMessage += 'Please select Loan Purpose<br />';
        }
        if (vehicleType === "") {
            strMessage += 'Please select Vehicle Type<br />';
        }
        if (hasLoanPurpose) {
            if (!Common.ValidateText(txtEstimatedVehicleValue)) {
                var purchaseAmountLabel = $('#divPurchaseAmountLabel').text().replace("*", "");
                strMessage += purchaseAmountLabel + " is required.<br/>";
            } else {
                if (selectedCategory.indexOf("REFI") == -1) { //skip validate downpayment, if category has refinance
                    if (loanPurpose.indexOf('BUYOUT') > -1) {
                        //make sure down payment <=loan balance
                        if (Common.GetFloatFromMoney(downPayment) > Common.GetFloatFromMoney(txtEstimatedVehicleValue)) {
                            strMessage += "The Down Payment Amount must be less than the Estimated Vehicle Value.<br/>";
                        }
                        //down payment >=0
                        if (!Common.ValidateTextAllowZero(downPayment)) {
                            strMessage += 'Down Payment Amount is required<br />';
                        }

                    } else {       
                        if (loanPurpose.indexOf('REFI') == -1 && selectedCategory.indexOf('REFI') == -1) {
                            //make sure down payment <= purchase price
                            if (Common.GetFloatFromMoney(downPayment) > Common.GetFloatFromMoney(txtEstimatedVehicleValue)) {
                                strMessage += "The Down Payment Amount must be less than the Estimated Purchase Price.<br/>";
                            }
                            //down payment >=0
                            if (!Common.ValidateTextAllowZero(downPayment)) {
                                strMessage += 'Down Payment Amount is required<br />';
                            }
                        }
                    }
               }//end selectedCategory
            }
        } else { //loanpurpose is not exist
            if (!Common.ValidateText(txtEstimatedVehicleValue)) {
                strMessage += 'Estimated Purchase Price is required<br />';
            } else {
                if (Common.GetFloatFromMoney(downPayment) > Common.GetFloatFromMoney(txtEstimatedVehicleValue)) {
                    strMessage += "Down Payment Amount must be less than the Estimated Purchase Price.<br/>";
                }
            }
            if (!Common.ValidateTextAllowZero(downPayment)) {
                strMessage += 'Down Payment Amount is required<br />';
            }
        } //end if hasLoanPurpose ...

        if (hasLoanPurpose && loanPurpose.indexOf('REFI') > -1) {
            if (!Common.ValidateText(txtProposedLoanAmount)) {
                strMessage += 'Requested Loan Amount is required<br />';
            }
        }

        if (ddlLoanTerm == '0' || ddlLoanTerm == '') {
            strMessage += 'Loan Term is required<br />';
        }

        if (knowVehicleMake) {
            if (!Common.ValidateText(VL.FACTORY.vehicleMake()))
                strMessage += 'Vehicle Make is required<br />';
            if (!Common.ValidateText(txtVehicleModel))
                strMessage += 'Vehicle Model is required<br />';
            if (!Common.ValidateText(txtVehicleYear))
                strMessage += 'Vehicle Year is required<br />';
        }

      //  if (!Common.ValidateText(ddlBranches) && iBranchVisible != 0) {
        //    strMessage += 'Select the answer for: "Which branch is most convenient for you?"<br />';
      //  }
        return strMessage;
    }

	/*deprecated*/
    function validateVehicleLoanInfo2() {
        var strMessage = "";
        var loanInfoValidate = $.lpqValidate("ValidateProvideLoanInfo");
        var branchValidate = $.lpqValidate("ValidateBranch");
        if (loanInfoValidate === false || branchValidate === false) {
    		strMessage += 'Please complete all of the required field(s)<br />';
	    }
    	return strMessage;
    }

	
    function registerProvideLoanInfoValidator() {
    	$('#ddlVehicleType').observer({
    		validators: [
				function (partial) {
					if (Common.ValidateText(VL.FACTORY.vehicleType()) == false) {
						return "Vehicle Type is required";
					}
					return "";
				}
    		],
    		validateOnBlur: true,
    		group: "ValidateProvideLoanInfo"
    	});
    	$('#txtEstimatedVehicleValue').observer({
    		validators: [
				function (partial) {
					if (Common.ValidateText($(this).val()) == false) {
						var purchaseAmountLabel = "Estimated Purchase Price";
						if (VL.FACTORY.hasLoanPurpose()) {
							purchaseAmountLabel = $('#divPurchaseAmountLabel').text().replace("*", "");
						}
						return purchaseAmountLabel + " is required";
					}
					return "";
				}
    		],
    		validateOnBlur: true,
    		group: "ValidateProvideLoanInfo"
    	});
    	$('#txtDownPayment').observer({
    		validators: [
				function (partial) {
					var downPayment = $(this).val();
					var txtEstimatedVehicleValue = $('#txtEstimatedVehicleValue').val();
					if (VL.FACTORY.hasLoanPurpose()) {
						var selectedCategory = getSelectedCategory($('#hdLoanPurpose').val());
						var loanPurpose = $('#hdLoanPurpose').val().toUpperCase();
						if (selectedCategory.indexOf("REFI") > -1) { //skip validate downpayment, if category has refinance
							return "";
						}
						if (loanPurpose.indexOf('BUYOUT') > -1) {
							//down payment >=0
							if (!Common.ValidateTextAllowZero(downPayment)) {
								return 'Down Payment Amount is required';
							}
							//make sure down payment <=loan balance
							if (Common.GetFloatFromMoney(downPayment) > Common.GetFloatFromMoney(txtEstimatedVehicleValue)) {
								return "The Down Payment Amount must be less than the Estimated Vehicle Value";
							}
						} else {
							if (loanPurpose.indexOf('REFI') == -1 /*&& selectedCategory.indexOf('REFI') == -1*/) {
								//down payment >=0
								if (!Common.ValidateTextAllowZero(downPayment)) {
									return 'Down Payment Amount is required';
								}
								//make sure down payment <= purchase price
								if (Common.GetFloatFromMoney(downPayment) > Common.GetFloatFromMoney(txtEstimatedVehicleValue)) {
									return "The Down Payment Amount must be less than the Estimated Purchase Price";
								}
							}
						}
					} else {
						//down payment >=0
						if (!Common.ValidateTextAllowZero(downPayment)) {
							return 'Down Payment Amount is required';
						}
						if (Common.GetFloatFromMoney(downPayment) > Common.GetFloatFromMoney(txtEstimatedVehicleValue)) {
							return "Down Payment Amount must be less than the Estimated Purchase Price";
						}
					}
					return "";
				}
    		],
    		validateOnBlur: true,
    		group: "ValidateProvideLoanInfo"
    	});
    	$('#txtProposedLoanAmount').observer({
    		validators: [
				function (partial) {
					var txtProposedLoanAmount = $('#txtProposedLoanAmount').val();
					var loanPurpose = $('#hdLoanPurpose').val().toUpperCase();
					if (VL.FACTORY.hasLoanPurpose() && loanPurpose.indexOf('REFI') > -1) {
						if (Common.ValidateTextNonZero(txtProposedLoanAmount) == false) {
							return "Requested Loan Amount is required";
						}
					}
					return "";
				}
    		],
    		validateOnBlur: true,
    		group: "ValidateProvideLoanInfo"
    	});   	
    	$('#ddlLoanTerm').observer({
    	    validators: [
				function (partial) {
                
				    if (VL.FACTORY.vehicleType() != "CAR" || VL.FACTORY.AutoTermTextboxEnable()) {
				        return ""; // no validate ddlLoanTerm 
				    }

				    var ddlLoanTerm = $('#ddlLoanTerm').val();			  
				    if (ddlLoanTerm == '0' || ddlLoanTerm == '') {
				        return 'Loan Term is required';
				    }
				    return "";
				}
    	    ],
    	    validateOnBlur: true,
    	    group: "ValidateProvideLoanInfo"
    	});
    	$('#txtLoanTerm').observer({
    	    validators: [
				function (partial) {
				    var txtLoanTerm = $('#txtLoanTerm').val();
				    if (VL.FACTORY.vehicleType() == "CAR" && !VL.FACTORY.AutoTermTextboxEnable()) {
				        return ""; //no validate txtLoanTerm
				    }
				    if (txtLoanTerm == '0' || txtLoanTerm == '') {
				        return 'Loan Term is required';
				    }
				    return "";
				}
    	    ],
    	    validateOnBlur: true,
    	    group: "ValidateProvideLoanInfo"
    	});
    	$('#divVehicleMake').observer({
    		validators: [
				function (partial) {
					var knowVehicleMake = VL.FACTORY.knowVehicleMake();
					if (knowVehicleMake === false) return "";
					if (!Common.ValidateText(VL.FACTORY.vehicleMake())) {
						return 'Vehicle Make is required';
					}
					return "";
				}
    		],
    		validateOnBlur: true,
    		group: "ValidateProvideLoanInfo"
    	});
    	$('#txtVehicleModel').observer({
    		validators: [
				function (partial) {
					var txtVehicleModel = $('#txtVehicleModel').val();
					var knowVehicleMake = VL.FACTORY.knowVehicleMake();
					if (knowVehicleMake === false) return "";
					if (!Common.ValidateText(txtVehicleModel)) {
						return 'Vehicle Model is required';
					}
					return "";
				}
    		],
    		validateOnBlur: true,
    		group: "ValidateProvideLoanInfo"
    	});
    	$('#txtVehicleYear').observer({
    		validators: [
				function (partial) {
					var txtVehicleYear = $('#txtVehicleYear').val();
					var knowVehicleMake = VL.FACTORY.knowVehicleMake();
					if (knowVehicleMake === false) return "";
					if (!Common.ValidateText(txtVehicleYear)) {
						return 'Vehicle Year is required';
					}
					return "";
				}
    		],
    		validateOnBlur: true,
    		group: "ValidateProvideLoanInfo"
    	});
    	/*$('#ddlBranches').observer({
    		validators: [
				function (partial) {
					if ($('#VLBranchesDiv').length == 0) return "";
					if (!Common.ValidateText($(this).val())) {
						return "Select the answer for: Which branch is most convenient for you?";
					}
					return "";
				}
    		],
    		validateOnBlur: true,
    		group: "ValidateProvideLoanInfo"
    	});*/

        //this is required to pass schema validation
    	$('#ddlTradeInVehicleType').observer({
    	    validators: [
				function (partial) {
				    if (VL.FACTORY.IsTradeInEnabled() && Common.ValidateText(VL.FACTORY.vehicleType("TradeIn")) == false) {
				        return "Vehicle Type is required";
				    }
				    return "";
				}
    	    ],
    	    validateOnBlur: true,
    	    group: "ValidateProvideLoanInfo"
    	});

        /*no validate all vehicle infor fields for tradin 
    	$('#ddlTradeInVehicleType').observer({
    		validators: [
				function (partial) {
					if (VL.FACTORY.IsTradeInEnabled() && Common.ValidateText(VL.FACTORY.vehicleType("TradeIn")) == false) {
						return "Vehicle Type is required";
					}
					return "";
				}
    		],
    		validateOnBlur: true,
    		group: "ValidateProvideLoanInfo"
    	});
    	$('#divTradeInVehicleMake').observer({
    		validators: [
				function (partial) {
					if (VL.FACTORY.IsTradeInEnabled() && !Common.ValidateText(VL.FACTORY.tradeInVehicleMake())) {
						return 'Vehicle Make is required';
					}
					return "";
				}
    		],
    		validateOnBlur: true,
    		group: "ValidateProvideLoanInfo"
    	});
    	$('#txtTradeInVehicleModel').observer({
    		validators: [
				function (partial) {
					var txtVehicleModel = $('#txtTradeInVehicleModel').val();
					if (VL.FACTORY.IsTradeInEnabled() && !Common.ValidateText(txtVehicleModel)) {
						return 'Vehicle Model is required';
					}
					return "";
				}
    		],
    		validateOnBlur: true,
    		group: "ValidateProvideLoanInfo"
    	});
    	$('#txtTradeInVehicleYear').observer({
    		validators: [
				function (partial) {
					var txtVehicleYear = $('#txtTradeInVehicleYear').val();
					if (VL.FACTORY.IsTradeInEnabled() && !Common.ValidateText(txtVehicleYear)) {
						return 'Vehicle Year is required';
					}
					return "";
				}
    		],
    		validateOnBlur: true,
    		group: "ValidateProvideLoanInfo"
    	});
    	$('#txtTradeInVehicleCurrentValue').observer({
    		validators: [
				function (partial) {
					if (VL.FACTORY.IsTradeInEnabled() && !Common.ValidateTextNonZero($('#txtTradeInVehicleCurrentValue').val())) {
						return 'What is the current value of this vehicle?';
					}
					return "";
				}
    		],
    		validateOnBlur: true,
    		group: "ValidateProvideLoanInfo"
    	});
    	$('#txtTradeInVehicleOwn').observer({
    		validators: [
				function (partial) {
				    if (VL.FACTORY.IsTradeInEnabled() && !Common.ValidateTextAllowZero($('#txtTradeInVehicleOwn').val())) {
						return 'How much do you still owe on this vehicle?';
					}
					return "";
				}
    		],
    		validateOnBlur: true,
    		group: "ValidateProvideLoanInfo"
    	});
    	$('#txtTradeInVehiclePayingMonthly').observer({
    		validators: [
				function (partial) {
				    if (VL.FACTORY.IsTradeInEnabled() && !Common.ValidateTextAllowZero($('#txtTradeInVehiclePayingMonthly').val())) {
						return 'How much are you paying monthly for this vehicle?';
					}
					return "";
				}
    		],
    		validateOnBlur: true,
    		group: "ValidateProvideLoanInfo"
    	});
        */
    	$('#divLoanPurpose').observer({
    		validators: [
				function (partial) {
					if (VL.FACTORY.hasLoanPurpose()) { //validate loan purpose if it exists
						if ($('#hdLoanPurpose').val().toUpperCase() === "")
							return "Please select Loan Purpose";
					}
					return "";
				}
    		],
    		validateOnBlur: false,
    		group: "ValidateProvideLoanInfo"
    	});
    	//$('#ddlBranchName').observer({
    	//	validators: [
		//		function (partial) {
		//			if ($('#hdVisibleBranch').val() != "Y" || $('#hdRequiredBranch').val() == "N") { //skip validation
		//				return "";
		//			}
		//			if ($('#ddlBranchName').find('option').length > 0) {
		//				if (!Common.ValidateText($('#ddlBranchName option:selected').val())) {
		//					return "Please select Branch Location";
		//				}
		//			}
		//			return "";
		//		}
    	//	],
    	//	validateOnBlur: true,
    	//	group: "ValidateProvideLoanInfo"
        //});
    	$('#divNewOrUsedVehicle').observer({
    		validators: [
                function (partial) {
					if ($("#hdVlIsNewVehicle").val() === "") {
						return "Is this a new or used vehicle?";
					}
					return "";
				}
    		],
    		validateOnBlur: true,
    		container: '#divNewOrUsedVehicle',
    		groupType: "checkboxGroup",
    		group: "ValidateProvideLoanInfo"
    	});
    	$('#divKnowMakeAndModel').observer({
    		validators: [
				function (partial) {
					if ($("#hdVlKnowVehicleMake").val() === "") {
						return "Do you know the Make and Model?";
					}
					return "";
				}
    		],
    		validateOnBlur: true,
    		container: '#divKnowMakeAndModel',
    		groupType: "checkboxGroup",
    		group: "ValidateProvideLoanInfo"
    	});
    }

    function viewVehicleLoanInfo() {
        var strHtml = "";
        var strEstLabel = "Est. Purchase Price";
        strHtml += '<div class="col-sm-6 col-xs-12"><div class="row"><div class="col-xs-6 text-right row-title"><span class="bold">New Vehicle</span></div><div class="col-xs-6 text-left row-data"><span>' + (VL.FACTORY.IsNewVehicle() ? "YES" : "NO") + '</span></div></div></div>';
        if (VL.FACTORY.IsNewVehicle() == false && VL.FACTORY.knowVehicleMake() === true && VL.FACTORY.vehicleType().toUpperCase() !== "BOAT") {
            var strMileage = $.trim($("#txtVehicleMileage").val());
            strHtml += '<div class="col-sm-6 col-xs-12"><div class="row"><div class="col-xs-6 text-right row-title"><span class="bold">Vehicle Mileage</span></div><div class="col-xs-6 text-left row-data"><span>' + (strMileage != "" ? strMileage : "unknown") + '</span></div></div></div>';
        }
        if (VL.FACTORY.hasLoanPurpose()) {
            var strLoanPurpose = $("#hdLoanPurpose").val();
            strHtml += '<div class="col-sm-6 col-xs-12"><div class="row"><div class="col-xs-6 text-right row-title"><span class="bold">Loan Purpose</span></div><div class="col-xs-6 text-left row-data"><span>' + LOANPURPOSELIST[strLoanPurpose] + '</span></div></div></div>';
            if (strLoanPurpose.toUpperCase().indexOf('REFI') > -1 || strLoanPurpose.toUpperCase().indexOf('BUYOUT') > -1) {
                strEstLabel = 'Est. Vehicle Value';
            }
        }
        strHtml += '<div class="col-sm-6 col-xs-12"><div class="row"><div class="col-xs-6 text-right row-title"><span class="bold">Vehicle Type</span></div><div class="col-xs-6 text-left row-data"><span>' + $("#ddlVehicleType").val() + '</span></div></div></div>';

        strHtml += '<div class="col-sm-6 col-xs-12"><div class="row"><div class="col-xs-6 text-right row-title"><span class="bold">Know Vehicle Make</span></div><div class="col-xs-6 text-left row-data"><span>' + (VL.FACTORY.knowVehicleMake() ? "YES": "NO") + '</span></div></div></div>';

        if (VL.FACTORY.knowVehicleMake()) {
            strHtml += '<div class="col-sm-6 col-xs-12"><div class="row"><div class="col-xs-6 text-right row-title"><span class="bold">Vehicle Make</span></div><div class="col-xs-6 text-left row-data"><span>' + VL.FACTORY.vehicleMake() + '</span></div></div></div>';
            strHtml += '<div class="col-sm-6 col-xs-12"><div class="row"><div class="col-xs-6 text-right row-title"><span class="bold">Vehicle Model</span></div><div class="col-xs-6 text-left row-data"><span>' + htmlEncode($.trim($("#txtVehicleModel").val())) + '</span></div></div></div>';
            strHtml += '<div class="col-sm-6 col-xs-12"><div class="row"><div class="col-xs-6 text-right row-title"><span class="bold">Vehicle Year</span></div><div class="col-xs-6 text-left row-data"><span>' + $("#txtVehicleYear").val() + '</span></div></div></div>';
        }
        strHtml += '<div class="col-sm-6 col-xs-12"><div class="row"><div class="col-xs-6 text-right row-title"><span class="bold">' + strEstLabel + '</span></div><div class="col-xs-6 text-left row-data"><span>' + Common.FormatCurrency($("#txtEstimatedVehicleValue").val(), true) + '</span></div></div></div>';

	    var requestedLoanTitle = "Requested Loan";
        if (VL.FACTORY.hasLoanPurpose()) {
            if ($("#hdLoanPurpose").val().toUpperCase().indexOf('REFI') < 0) {
            	strHtml += '<div class="col-sm-6 col-xs-12"><div class="row"><div class="col-xs-6 text-right row-title"><span class="bold">Down Payment</span></div><div class="col-xs-6 text-left row-data"><span>' + Common.FormatCurrency($("#txtDownPayment").val(), true) + '</span></div></div></div>';
            } else {
                requestedLoanTitle = "Req. Refinance Amount";
            }
        }
        strHtml += '<div class="col-sm-6 col-xs-12"><div class="row"><div class="col-xs-6 text-right row-title"><span class="bold">' + requestedLoanTitle + '</span></div><div class="col-xs-6 text-left row-data"><span>' + Common.FormatCurrency($("#txtProposedLoanAmount").val(), true) + '</span></div></div></div>';
        var loanTerm = "";
        if (VL.FACTORY.vehicleType() == "CAR" && !VL.FACTORY.AutoTermTextboxEnable()) {
            loanTerm  = $("#ddlLoanTerm option:selected").val();
        } else {
          loanTerm = $("#txtLoanTerm").val();
        }
        if (loanTerm != "") {
            strHtml += '<div class="col-sm-6 col-xs-12"><div class="row"><div class="col-xs-6 text-right row-title"><span class="bold">Loan Term</span></div><div class="col-xs-6 text-left row-data"><span>' +loanTerm + ' months</span></div></div></div>';
        }
        if (VL.FACTORY.IsTradeInEnabled()) {
            strHtml += '<div class="col-xs-12" style="clear:left;"><div class="row row-title">Trade-in Vehicle Information</div></div>';
            if ($("#ddlTradeInVehicleType").val() != "" && !$("#ddlTradeInVehicleType").IsHideField()) {
                strHtml += '<div class="col-sm-6 col-xs-12"><div class="row"><div class="col-xs-6 text-right row-title"><span class="bold">Vehicle Type</span></div><div class="col-xs-6 text-left row-data"><span>' + $("#ddlTradeInVehicleType").val() + '</span></div></div></div>';
            }
            if (VL.FACTORY.tradeInVehicleMake() != "" && !$("#txtTradeInVehicleModel").IsHideField()) {
                strHtml += '<div class="col-sm-6 col-xs-12"><div class="row"><div class="col-xs-6 text-right row-title"><span class="bold">Vehicle Make</span></div><div class="col-xs-6 text-left row-data"><span>' + VL.FACTORY.tradeInVehicleMake() + '</span></div></div></div>';
            }
            if ($("#txtTradeInVehicleModel").val() != "" && !$("#txtTradeInVehicleModel").IsHideField()) {
                strHtml += '<div class="col-sm-6 col-xs-12"><div class="row"><div class="col-xs-6 text-right row-title"><span class="bold">Vehicle Model</span></div><div class="col-xs-6 text-left row-data"><span>' + htmlEncode($.trim($("#txtTradeInVehicleModel").val())) + '</span></div></div></div>';
            }
            if ($("#txtTradeInVehicleYear").val() != "" && !$("#txtTradeInVehicleYear").IsHideField()) {
                strHtml += '<div class="col-sm-6 col-xs-12"><div class="row"><div class="col-xs-6 text-right row-title"><span class="bold">Vehicle Year</span></div><div class="col-xs-6 text-left row-data"><span>' + $("#txtTradeInVehicleYear").val() + '</span></div></div></div>';
            }
            if ($("#txtTradeInVehicleCurrentValue").val() != "" && !$("#txtTradeInVehicleCurrentValue").IsHideField()) {
                strHtml += '<div class="col-sm-6 col-xs-12"><div class="row"><div class="col-xs-6 text-right row-title"><span class="bold">Current value of this vehicle</span></div><div class="col-xs-6 text-left row-data"><span>' + $("#txtTradeInVehicleCurrentValue").val() + '</span></div></div></div>';
            }
            if ($("#txtTradeInVehicleOwn").val() != "" && !$("#txtTradeInVehicleOwn").IsHideField()) {
                strHtml += '<div class="col-sm-6 col-xs-12"><div class="row"><div class="col-xs-6 text-right row-title"><span class="bold">You owe on this vehicle</span></div><div class="col-xs-6 text-left row-data"><span>' + $("#txtTradeInVehicleOwn").val() + '</span></div></div></div>';
            }
            if ($("#txtTradeInVehiclePayingMonthly").val() != "" && !$("#txtTradeInVehiclePayingMonthly").IsHideField()) {
                strHtml += '<div class="col-sm-6 col-xs-12"><div class="row"><div class="col-xs-6 text-right row-title"><span class="bold">Paying monthly for this vehicle</span></div><div class="col-xs-6 text-left row-data"><span>' + $("#txtTradeInVehiclePayingMonthly").val() + '</span></div></div></div>';
            }
        }
        return strHtml;
    }

	

    function getVehicleLoanInfo(disagree_check) {
        var appInfo = new Object();
        appInfo.PlatformSource = $('#hdPlatformSource').val();
        appInfo.LenderRef = $('#hfLenderRef').val();
        appInfo.Task = "SubmitLoan";
        if (disagree_check) {
              appInfo.isDisagreeSelect = "Y";
         } else {
              appInfo.isDisagreeSelect = "N";
         }
        /* if ($('.checkDisclosure').val() != undefined) {
              //get all disclosures 
              var disclosures = "";
              var temp = "";
              $('.checkDisclosure').each(function() {
                  temp = $(this).text().trim().replace(/\s+/g, " ");
                  disclosures += temp + "\n\n";
              });
              appInfo.Disclosure = disclosures;
          }*/
        if (parseDisclosures() != "") {
            appInfo.Disclosure = parseDisclosures();
        }
        //    branchID from query Param has precedent over  branchID from dropdown 
        appInfo.BranchID = getBranchId();
        appInfo.LoanOfficerID = $('#hfLoanOfficerID').val();
       // if (appInfo.BranchID == "") {
         //   appInfo.BranchID = $('#ddlBranches option:selected').val();
        //}
        appInfo.ReferralSource = $('#hfReferralSource').val();
        //appInfo.MemberProtectionPlan = '';

        if (VL.FACTORY.vehicleType() == "CAR" && !VL.FACTORY.AutoTermTextboxEnable()) {
            appInfo.LoanTerm = $("#ddlLoanTerm option:selected").val();
        } else {
            appInfo.LoanTerm = $("#txtLoanTerm").val();
        }
        
        if ($("#txtLocationPoolCode").length > 0) {
        	appInfo.SelectedLocationPool = JSON.stringify(ZIPCODEPOOLLIST[$("#txtLocationPoolCode").val()]);
        } else if ($("#vl_txtLocationPoolCode").length > 0) {
        	appInfo.SelectedLocationPool = JSON.stringify(VLPRODUCTS.ZipPoolIds[$("#vl_txtLocationPoolCode").val()]);
        }
        if (VL.FACTORY.hasLoanPurpose()) {
            appInfo.LoanPurpose = $("#hdLoanPurpose").val();
        } else {
            appInfo.LoanPurpose = " "; //there is no loan purpose, just pass empty string 
        }
        appInfo.VehicleType = VL.FACTORY.vehicleType();
        appInfo.KnowVehicleMake = $('#hdVlKnowVehicleMake').val().toUpperCase();
        appInfo.depositAmount = 0;
        appInfo.IsComboMode = $("#hdIsComboMode").val();
        appInfo.HasSSOIDA = $('#hdHasSSOIDA').val();
        if (appInfo.IsComboMode == "Y") {
            appInfo.XABranchID = getXAComboBranchId();
            appInfo.ProceedXAAnyway = $("#hdProceedMembership").val();
            if (!$('#div_fom_section').hasClass("hidden")) {
                setFOM(appInfo);
            }
            if ($("#divSelectedProductPool").length === 1 && $("#divSelectedProductPool").parent().hasClass("hidden") == false) {
                appInfo.SelectedProducts = JSON.stringify(loanProductFactory.collectProductSelectionData());
            }
            if ($("#divCcFundingOptions").length === 1) {
                appInfo.depositAmount = Common.GetFloatFromMoney($('#txtFundingDeposit').val());
                FS1.loadValueToData();
                appInfo.rawFundingSource = JSON.stringify(FS1.Data);
            }
		}

        appInfo.VehicleMake = "";
        appInfo.VehicleModel = "";
        appInfo.VehicleYear = "";
	    appInfo.VinNumber = "";
        if (appInfo.KnowVehicleMake == 'Y') {
        	appInfo.VehicleMake = VL.FACTORY.vehicleMake();
        	appInfo.VehicleModel = $('#txtVehicleModel').val();
            appInfo.VehicleYear = $('#txtVehicleYear').val();
            var vehType = $('#ddlVehicleType option:selected').val();
			var $vinNumber = $("#txtVinNumber");
			// AP-2315 - VIN can be hidden by Visibility, in which case $vinNumber.val() is undefined which leads to errors. Skip if there is no VIN field.
			if ($vinNumber.length > 0) {
				if (vehType != undefined && (vehType.toUpperCase() == "CAR" || vehType.toUpperCase() == "MOTORCYCLE")) {
					if ($vinNumber.val() != undefined && ($vinNumber.val().length > 10 && $.trim($vinNumber.val()).length < 18)) {
						appInfo.VinNumber = $.trim($vinNumber.val());
					}
				} else { //vehType =BOAT,TRAVELTRAILER, MOTORHOME ...
					if ($vinNumber.val() != undefined && ($vinNumber.val().length > 0 && $.trim($vinNumber.val()) != "")) { //save vin# if it exist
						appInfo.VinNumber = $.trim($vinNumber.val());
					}
				}
			}
        }
    	//trade in
        appInfo.TradeValue = 0;
        appInfo.TradePayOff = 0;
        appInfo.TradePayment = 0;
        appInfo.TradeType = '';
        appInfo.TradeYear = '';
        appInfo.TradeModel = '';
        appInfo.TradeMake = '';
        appInfo.HasTradeIn = (VL.FACTORY.IsTradeInEnabled() ? "Y" : "N");
        if (appInfo.HasTradeIn === "Y") {
        	appInfo.TradeValue = Common.GetFloatFromMoney($('#txtTradeInVehicleCurrentValue').val());
        	appInfo.TradePayOff = Common.GetFloatFromMoney($('#txtTradeInVehicleOwn').val());
        	appInfo.TradePayment = Common.GetFloatFromMoney($('#txtTradeInVehiclePayingMonthly').val());
	        appInfo.TradeType = VL.FACTORY.vehicleType("TradeIn");
        	appInfo.TradeYear = $('#txtTradeInVehicleYear').val();
        	appInfo.TradeModel = $('#txtTradeInVehicleModel').val();
	        appInfo.TradeMake = VL.FACTORY.tradeInVehicleMake();
        }

        var mileage = '';
        appInfo.IsNewVehicle = $('#hdVlIsNewVehicle').val().toUpperCase();
        if (appInfo.IsNewVehicle != 'Y' && VL.FACTORY.vehicleType().toUpperCase() !== "BOAT" && $('#hdVlKnowVehicleMake').val().toUpperCase() === "Y") {
            mileage = $('#txtVehicleMileage').val();
        }

        appInfo.VehicleMileage = mileage;
        var isRefinance = false;

        //TOO WEIRD LINES OF CODE. REVISE AS BELOW
        //getSelectedCategory always return empty due to incorrect code of reading values from config
        /*var loanPuroseElement = $('#ddlLoanPurpose option:selected');
        var selectedCategory = getSelectedCategory(loanPuroseElement);
        if (selectedCategory.toUpperCase().indexOf('REFI') > -1) {
            isRefinance = true;
        } else {
            if (loanPuroseElement.val() != undefined) {
                if (loanPuroseElement.val().toUpperCase().indexOf('REFI') > -1) {
                    isRefinance = true;
                }
            }
        }*/
        if ($("#hdLoanPurpose").val().toUpperCase().indexOf("REFI") > -1) {
            isRefinance = true;
        }


        //if (isRefinance) {
        //    appInfo.VehicleValue ="0";
        //} else {
        //    appInfo.VehicleValue = Common.GetFloatFromMoney($('#txtEstimatedVehicleValue').val());
        //}
        appInfo.VehicleValue = Common.GetFloatFromMoney($('#txtEstimatedVehicleValue').val());
        if (isRefinance) {
            appInfo.DownPayment = "0";
        }
        else {
            appInfo.DownPayment = Common.GetFloatFromMoney($('#txtDownPayment').val());
        }
        appInfo.ProposedLoanAmount = Common.GetFloatFromMoney($('#txtProposedLoanAmount').val());
        //get loan ida
        appInfo.idaMethodType = $('#hdIdaMethodType').val();
        // Custom Ansers
		appInfo.CustomAnswers = JSON.stringify(
			loanpage_getAllCustomQuestionAnswers()
				.concat(getAllCustomQuestionAnswers())
				.concat(co_getAllCustomQuestionAnswers())
				.concat(reviewpage_getAllCustomQuestionAnswers()));
		if (typeof reviewpage_getAllUrlParaCustomQuestionAnswers == "function") {
			var UrlParaCustomQuestions = reviewpage_getAllUrlParaCustomQuestionAnswers();
            //add UrlParamCustomQuestionAndAnswer if it exist                  
            if (UrlParaCustomQuestions.length > 0) {
                appInfo.IsUrlParaCustomQuestion = "Y";
                appInfo.UrlParaCustomQuestionAndAnswers = JSON.stringify(UrlParaCustomQuestions);
            }
        }
	    appInfo.Is2ndLoanApp = $("#hdIs2ndLoanApp").val();
        if (appInfo.Is2ndLoanApp === "Y") {
	        appInfo.SID = $("#hdSID").val();
        }
        else {
            //cross sell comment from XA App
            appInfo.xaXSellComment = $('#hdXACrossSellComment').val();
		    // Main-App
		    // Applicant Info
		    SetApplicantInfo(appInfo);
		    // Contact Info
		    SetContactInfo(appInfo);
            // Asset
            SetAssets(appInfo);
		    // Address
		    SetApplicantAddress(appInfo);
		    //Mailing Adddess
		    SetApplicantMailingAddress(appInfo);
		    //Previous Address
		    SetApplicantPreviousAddress(appInfo);
		    if ($("#divDeclarationSection").length > 0) {
		    	SetDeclarations(appInfo);
		    	appInfo.Declarations = JSON.stringify(appInfo.Declarations);
		    	appInfo.AdditionalDeclarations = JSON.stringify(appInfo.AdditionalDeclarations);
		    }
        	//reference information
	        if ($("#hdEnableReferenceInformation").val() == "Y") {
		        setReferenceInfo(appInfo);
	        }

	        if ($("#hdEnableIDSection").val() === "Y") {
			    SetApplicantID(appInfo);
		    }

            //cuna Protection Info
		    if (typeof setProtectionInfo !== "undefined") {
		        setProtectionInfo(appInfo, false);
		    }
		    // GMI
		    SetGMIInfo(appInfo);
		    // Financial
		    SetApplicantFinancialInfo(appInfo);
		    var hasCoApp = $("#hdVlHasCoApplicant").val();
		    //upload document data and info
		    var docUploadInfo = {};
		    if (typeof docUploadObj != "undefined" && docUploadObj != null) {
		    	docUploadObj.setUploadDocument(docUploadInfo);
		    }
		    if (typeof co_docUploadObj != "undefined" && co_docUploadObj != null) {
		    	co_docUploadObj.setUploadDocument(docUploadInfo);
		    }
		    if (docUploadInfo.hasOwnProperty("sDocArray") /*&& docUploadInfo.sDocArray != null && docUploadInfo.sDocArray.length > 0 */ && docUploadInfo.hasOwnProperty("sDocInfoArray")) {
		    	appInfo.Image = JSON.stringify(docUploadInfo.sDocArray);
		    	appInfo.UploadDocInfor = JSON.stringify(docUploadInfo.sDocInfoArray);
		    }
		    appInfo.HasCoApp = hasCoApp;
		    if (window.ASI) {
		    	ASI.FACTORY.setApplicantAdditionalInfo(appInfo);
		    }
		    if (appInfo.HasCoApp == 'Y') {
			    // Co-App
			    // Co-Applicant Info
			    co_SetApplicantInfo(appInfo);
			    // Co-Contact Info
			    co_SetContactInfo(appInfo);
                // Co-Assets
                co_SetAssets(appInfo);
			    // Co-Address
			    co_SetApplicantAddress(appInfo);
			    //Co-Previous Address
			    co_SetApplicantPreviousAddress(appInfo);
			    // co -Mailing Address
			    co_SetApplicantMailingAddress(appInfo);
		    	//co-reference information
			    if ($("#hdEnableReferenceInformationForCoApp").val() == "Y") {
				    co_setReferenceInfo(appInfo);
			    }
			    //Identification
			    if ($("#hdEnableIDSectionForCoApp").val() === "Y") {
				    co_SetApplicantID(appInfo);
			    }

		    	// Co-Declarations
			    if ($("#co_divDeclarationSection").length > 0) {
			    	co_SetDeclarations(appInfo);
			    	appInfo.co_Declarations = JSON.stringify(appInfo.co_Declarations);
			    	appInfo.co_AdditionalDeclarations = JSON.stringify(appInfo.co_AdditionalDeclarations);
			    }
			    // GMI
			    co_SetGMIInfo(appInfo);
			    // Co-Financial
			    co_SetApplicantFinancialInfo(appInfo);
		    }
	    }
        if ($("#hdVendorID").val() != "" && $("#hdUserID").val() != "") {
        	appInfo.VendorId = $("#hdVendorID").val();
        	appInfo.UserId = $("#hdUserID").val();
        	//clone and truncate sensitive data
        	var dataObj = JSON.stringify(appInfo);
        	dataObj = JSON.parse(dataObj);
        	dataObj.SSN = "";
        	dataObj.co_SSN = "";
        	if (typeof dataObj.rawFundingSource == "string") {
        		var rawFsObj = JSON.parse(dataObj.rawFundingSource);
        		rawFsObj.cCreditCardNumber = "";
        		rawFsObj.cExpirationDate = "";
        		rawFsObj.cCVNNumber = "";
        		rawFsObj.cCreditCardNumber = "";
        		rawFsObj.cCreditCardNumber = "";
        		dataObj.rawFundingSource = JSON.stringify(rawFsObj);
        	}
        	appInfo.rawData = JSON.stringify(dataObj);
        }
	    return appInfo;
    }

	VL.FACTORY.getVehicleLoanInfo = getVehicleLoanInfo;
    VL.FACTORY.saveAndFinishLater = function() {
    	var loanInfo = getVehicleLoanInfo(false);
    	loanInfo.Task = "SaveIncompletedLoan";
    	$.ajax({
    		type: 'POST',
    		url: 'CallBack.aspx',
    		data: loanInfo,
    		dataType: 'html',
    		success: function (response) {
    			if (response == "ok") {
    				$("#popNotification div[placeholder='content']").text("Current loan has been saved successfully.");
    				$("#popNotification").popup("open", { "positionTo": "window" });
    			} else {
    				goToDialog('There was an error when processing your request.  Please try the application again.');
    			}
    		},
    		error: function (err) {
    			goToDialog('There was an error when processing your request.  Please try the application again.');
    		}
    	});
    	return false;
    }

    function saveVehicleLoanInfo() {
        if (g_submit_button_clicked) return false;
        //disable submit button before calling ajax to prevent user submit multiple times
        $('#pagesubmit .div-continue-button').addClass('ui-disabled');   
        $('#txtErrorMessage').html('');
        var loanInfo = getVehicleLoanInfo(false);
        //save instaTouchPrefillComment
        $instaTouchComment = $('#hdInstaTouchPrefillComment');
        if ($instaTouchComment.val() != undefined) {
            loanInfo.InstaTouchPrefillComment = $instaTouchComment.val();
        } 
        loanInfo = attachGlobalVarialble(loanInfo);
        $.ajax({
            type: 'POST',
            url: 'CallBack.aspx',
            data: loanInfo,
            dataType: 'html',
            success: function (response) {
                //clear instaTouchPrefill 
                if ($instaTouchComment.val() != undefined && $instaTouchComment.val() != "") {
                    $instaTouchComment.val("");
                }
                if (response.toUpperCase().indexOf('MLERRORMESSAGE') > -1) {
                    //enable submit button
                    $('#pagesubmit .div-continue-button').removeClass('ui-disabled');
                    goToDialog(response);
                } else if (response.toUpperCase().indexOf('WALLETQUESTION') != -1) {
                    displayWalletQuestions(response);         
                } else if (response == "blockmaxapps") {
                	goToNextPage("#pageMaxAppExceeded");
                } else {
                    //submit sucessfull so clear
                    //clearForms();
                    goToLastDialog(response);               
                }
            },
            error: function (err) {
                //enable submit button
                $('#pagesubmit .div-continue-button').removeClass('ui-disabled');          
                goToDialog('There was an error when processing your request.  Please try the application again.');
            }
        }); 
        return false;
    }
    VL.FACTORY.validatePageSubmit = function (element) {
    	var currElement = $(element);
    	if (currElement.attr("contenteditable") == "true") return false;  
        var validator = true;
        var strMessage = "";

        if ($("#hdIsComboMode").val() === "Y") {
            if (!$('#div_fom_section').hasClass("hidden") && $("#pagesubmit").find("#divShowFOM").length > 0 && $.lpqValidate("ValidateFOM") == false) {
                validator = false;
            }
            if ($.lpqValidate("ValidateProceedMembership") == false) {
                validator = false;
            }
			if ($.lpqValidate("ValidateProductSelection") === false) {
        		validator = false;
			}
			if (validateFS1() == false) {
            	validator = false;
            }
        }
		// Validate custom questions
		if ($('#reviewpage_divApplicantQuestion').children().length > 0) {
			if ($.lpqValidate("reviewpage_ValidateApplicantQuestionsXA") === false) {
				validator = false;
			}
		}
        if ($("#pagesubmit").find("#divDisclosure").length > 0 && $.lpqValidate("ValidateDisclosure_disclosures") == false) {
            validator = false;
        }      
         //validate the view loan info panel, if the panel is empty display error message when clicking submit button
        //don't need to validate all the view panels because if the app is cross sell then it only has loan info and review pages 
        if ($("#reviewPanel").find(".ViewVehicleLoanInfo").length > 0 && $.lpqValidate("ValidateViewVehicleLoanInfo") == false) {      
            validator = false;
        }
        //validate verify code
        strMessage += validateVerifyCode();
		//refine error message to remove duplicate item
		var arrErr = strMessage.split("<br/>");
		var refinedErr = [];
		$.each(arrErr, function (i, el) {
			el = $.trim(el);
			if (el !== "" && $.inArray(el, refinedErr) === -1) refinedErr.push(el);
		});
		if (refinedErr.length > 0) {
			validator = false;
		}
		if (validator === false) {
			if (refinedErr.length > 0) {
				if ($("#divErrorPopup").length > 0) {
					$('#txtErrorPopupMessage').html(refinedErr.join("<br/>"));
					$("#divErrorPopup").popup("open", { "positionTo": "window" });
				} else {
					$('#txtErrorMessage').html(refinedErr.join("<br/>"));
					currElement.next().trigger('click');
				}
            } else {
               Common.ScrollToError();			
			}
		} else {
			if (window.ABR) {
				var formValues = prepareAbrFormValues("pagesubmit");
				if (ABR.FACTORY.evaluateRules("pagesubmit", formValues)) {
					return false;
				}
			}
		    saveVehicleLoanInfo();
		}
    };

    VL.FACTORY.bindEventLoanPurposeOption = function() {
        $("#divLoanPurpose").find("a[data-command='loan-purpose']").off("click").on("click", function () {
            var $self = $(this);
            //first, remove other active button
            $("#divLoanPurpose").find("a[data-command='loan-purpose']").each(function () {
                $(this).removeClass("active");
                handledBtnHeaderTheme($(this));
            });
            //then, activate selected button
            $self.addClass("active");
            $("#hdLoanPurpose").val($self.data("key"));  
            VL.FACTORY.refreshPurposeLabel();
            VL.FACTORY.calculateLoanAmount();
           
            handledBtnHeaderTheme($self);
	        $.lpqValidate.hideValidation("#divLoanPurpose");
        });
    };
    VL.FACTORY.onClickReturnToMainPage = function (e) {
        saveCancelVehicleLoanInfo();
        clearForms(); //clear data when the page is redicted to the main page
        //return to 
        var sDir = $('#hdRedirectURL').val();
        if (sDir != '') {
            window.location.href = sDir;
        }
	    e.preventDefault();
    };

	function saveCancelVehicleLoanInfo() {
        if ($('#hdEnableDisagree').val() != "Y") {
            return false;
        }
        var loanInfo = getVehicleLoanInfo(true);
        //if  data is not available then don't save data
        if (loanInfo.SSN == "" || loanInfo.DOB == "" || loanInfo.EmailAddr == "") {
            return false;
        }
        loanInfo = attachGlobalVarialble(loanInfo);
        $.ajax({
            type: 'POST',
            url: 'CallBack.aspx',
            data: loanInfo,
            dataType: 'html',
            success: function (response) {
                //submit sucessfull so clear
                clearForms();
            },
        });
        $.mobile.loading('hide'); //hide the ajax message
        return false;
    }

    function getSelectedCategory(selectedLoanPurpose) {     
        var loanPurposeCategoryObj = JSON.parse(LoanPurposeCategory);
        for (var key in loanPurposeCategoryObj) {
            if (key == selectedLoanPurpose) {
                return loanPurposeCategoryObj[key];
            }
        }     
        return "";
    }
	function fillVinInfo(data) {
		$("#txtVehicleModel").val(data.Model);
		$.lpqValidate.hideValidation($("#txtVehicleModel"));
		$("#txtVehicleYear").val(data.Year);
		$.lpqValidate.hideValidation($("#txtVehicleYear"));
		$.lpqValidate.hideValidation($("#divVehicleMake"));
		$("#ddlMotorCycleMake").val(data.Make);
		$("#ddlMotorCycleMake").selectmenu("refresh");
		$("#ddlCarMake").val(data.Make);
		$("#ddlCarMake").selectmenu("refresh");
		$("#txtMake").val(data.Make);


		//switch ($('#ddlVehicleType').val()) {
		//	case 'MOTORCYCLE':
		//		$("#ddlMotorCycleMake").val(data.Make);
		//		$("#ddlMotorCycleMake").selectmenu("refresh");
		//		break;
		//	case 'CAR':
		//		$("#ddlCarMake").val(data.Make);
		//		$("#ddlCarMake").selectmenu("refresh");
		//		break;
		//	default:
		//		$("#txtMake").val(data.Make);
		//		break;
		//}
		$("#divVinNumber .vin-scan-guide").removeClass("hidden");
		$("#divVinNumber .vin-scan-msg").text("Could not decode VIN number");
		$("#divVinNumber .vin-scan-msg").addClass("hidden");
	}
	function clearVinInfo(data) {
		$("#txtVehicleModel").val("");
		$("#txtVehicleYear").val("");
		$("#ddlMotorCycleMake").val("");
		$("#ddlMotorCycleMake").selectmenu("refresh");
		$("#ddlCarMake").val("");
		$("#ddlCarMake").selectmenu("refresh");
		$("#txtMake").val("");
	}
    function decodeVinNumber(vinNumber) {
	    vinNumber = $.trim(vinNumber);
	    if (vinNumber.length == 17) {
			//caching
	    	if (sessionStorage.getItem(vinNumber) && sessionStorage.getItem(vinNumber) !== "") {
			    fillVinInfo($.parseJSON(sessionStorage.getItem(vinNumber)));
		    } else {
	    		sessionStorage.setItem(vinNumber, "");
			    $("#divVinNumber span.loader").removeClass("hidden");
	    		$.ajax({
	    			url: '/handler/Handler.aspx',
	    			async: true,
	    			cache: false,
	    			global: false,
	    			type: 'POST',
	    			dataType: 'html',
	    			data: {
	    				command: "decodeVinNumber",
	    				lenderRef: $('#hfLenderRef').val(),
	    				vinNumber: $.trim(vinNumber)
	    			},
	    			success: function (responseText) {
	    				var response = $.parseJSON(responseText);
	    				if (response.IsSuccess) {
	    					sessionStorage.setItem(vinNumber, Common.toJSON(response.Info));
	    					fillVinInfo(response.Info);
	    				} else {
	    					$("#divVinNumber .vin-scan-guide").addClass("hidden");
	    					$("#divVinNumber .vin-scan-msg").text("Could not decode VIN number");
	    					$("#divVinNumber .vin-scan-msg").removeClass("hidden");
						    clearVinInfo();
					    }
	    			},
	    			error: function (e) {
	    				$("#divVinNumber .vin-scan-guide").addClass("hidden");
	    				$("#divVinNumber .vin-scan-msg").text("Could not decode the VIN number");
	    				$("#divVinNumber .vin-scan-msg").removeClass("hidden");
					    clearVinInfo();
				    }
	    		}).always(function () {
	    			$("#divVinNumber span.loader").addClass("hidden");
	    		});
		    }
		}
    }

    VL.FACTORY.handledLocationPoolLoanInfo = function (zipPoolVLProducts) {
        $("#vl_txtLocationPoolCode").on("keyup paste", function () {
            var $self = $(this);
            var zipCode = $self.val();       
            setTimeout(function () {
                var $divLocationPool = $('#vl_divLocationPool');
                var $divNoAvailableProduct = $('#vl_divNoAvailableProducts');
                var $divVehicleLoanInfo = $('#divVehicleLoanInfo');               
                if (/[0-9]{5}/.test(zipCode)) {
                    var selectedZipPoolProducts = getSelectedZipPoolProducts(zipCode, zipPoolVLProducts);
                    var selectedZipPoolVLPurposes = getSelectedZipPoolLoanPurposes(selectedZipPoolProducts);
                    if (selectedZipPoolVLPurposes.length == 0) {
                        //show No available products message if there is no current zip pool purposes
                        $divNoAvailableProduct.removeClass('hidden');
                        $divVehicleLoanInfo.addClass('hidden');
                    } else {                       
                        VL.FACTORY.handledShowAndHideZipPoolLoanPurposes(selectedZipPoolVLPurposes);
                        var $divLoanPurpose = $('#divLoanPurpose');
                        var $loanPurpose = $divLoanPurpose.find('a[data-command="loan-purpose"]');
                        var $hiddenLoanPurpose = $divLoanPurpose.find('a[data-command="loan-purpose"].hidden');                  
                        if ($loanPurpose.length > $hiddenLoanPurpose.length) {
                            $divNoAvailableProduct.addClass('hidden');
                            $divVehicleLoanInfo.removeClass('hidden');
                            $divLoanPurpose.show();
                            $("#txtZip").val(zipCode);
                            $("#txtCity").val("");
                            $("#txtZip").attr("disabled", "disabled");
                            $("#txtZip").trigger("change");                                                    
                            if ($loanPurpose.length == 1 || ($loanPurpose.length - $hiddenLoanPurpose.length == 1)) {
                                //preselect loan purpose and hide the loan purpose section if there is only one loan purpose
                                $divLoanPurpose.find('a[data-command="loan-purpose"]:not([class~="hidden"])').trigger('click');
                                $divLoanPurpose.hide();  
                            }
                        } else {
                            //show No available products message if there is no current zip pool purposes
                            $divNoAvailableProduct.removeClass('hidden');
                            $divVehicleLoanInfo.addClass('hidden');
                        }                      
                    }
                } else {
                    $divVehicleLoanInfo.addClass("hidden");
                }
            }, 400);
        });
    };
    VL.FACTORY.handledShowAndHideZipPoolLoanPurposes = function (selectedZipPoolLoanPurposes) {     
        $('#divLoanPurpose a[data-command="loan-purpose"]').each(function () {
            var $self = $(this);
            //de-select loanpurpose and reset hdLoanPurpose value
            if ($self.hasClass('active')) {
                $self.removeClass('active btnActive_on');
                $('#hdLoanPurpose').val("");
            }
            var loanPurpose = $self.attr('data-key');
            if ($.inArray(loanPurpose, selectedZipPoolLoanPurposes) > -1) {
                $self.removeClass('hidden');
            } else {
                $self.addClass('hidden');
            }
        });
    };
    function goToPageSubmit() {
    	VL.FACTORY.renderReviewContent();
    	goToNextPage("#pagesubmit");
    }
    
}(window.VL = window.VL || {}, jQuery));

$(function () {
	EMPLogicPI = $.extend(true, {}, EMPLogic);
	EMPLogicPI.init('ddlEmploymentStatus', '', 'vl2', 'divEmploymentStatusRest');
	$('#vl2').on("pageshow", function () {
		EMPLogicPI.refreshHtml();
	});
    EMPLogicJN = $.extend(true, {}, EMPLogic);
    EMPLogicJN.init('co_ddlEmploymentStatus', 'co_', 'vl6', 'co_divEmploymentStatusRest');
    $('#vl6').on("pageshow", function () {
    	EMPLogicJN.refreshHtml();
    });
    //primary previous employment
    EMPLogicPI_PREV = $.extend(true, {}, EMPLogic);
    EMPLogicPI_PREV.init('prev_ddlEmploymentStatus', 'prev_', 'vl2', 'prev_divEmploymentStatusRest');
    EMPLogicJN_PREV = $.extend(true, {}, EMPLogic);
    EMPLogicJN_PREV.init('co_prev_ddlEmploymentStatus', 'co_prev_', 'vl6', 'co_prev_divEmploymentStatusRest');
    VL.FACTORY.init();
    $(document).on("pageshow", function () {
        var curPageId = $.mobile.pageContainer.pagecontainer('getActivePage').attr('id');
        switch (curPageId) {
            case "vl1":
                VL.FACTORY.vl1Init();
                break;
        	case "vl2":
        		pushToPagePaths("vl2");
                VL.FACTORY.vl2Init(false); //prefill data for primary
                break;
        	case "vl6":
        		pushToPagePaths("vl6");
                VL.FACTORY.vl2Init(true); //prefill data for joint
                break;         
            case "pagesubmit": 
                VL.FACTORY.registerViewVehicleLoanInfoValidator();
            	VL.FACTORY.pageSubmitInit();
            	break;
        	case "pl_last":
        		VL.FACTORY.pageLastInit();
        		break;
        }     
    });
 
    $("div[name='disclosures'] label").click(function () {
        handleDisableDisclosures(this);
    });
    //var footerTheme = $('#hdFooterTheme').val();
    //var bgColor = $('.ui-bar-' + footerTheme).css('background-color');
    //var changeColor = changeBackgroundColor(bgColor, .6);
    //var bgTheme = $('#hdBgTheme').val();
    //if (bgTheme != "") {
    //    changeColor = bgColor;
    //}
    //$('body').css("background-color", changeColor);

    // Fill in the fields with test data
    if (getParaByName("autofill") == "true") {
        // Fill in the stuff
        autoFillData();
    }

    //user click disagree button -->popup confirm message
    $(document).on('click', '#divDisagree a', function (e) {
        var redirectURL = $('#hdRedirectURL').val();
        //don't popup confirm message--> go direct to return to mainpage
        //displayConfirmMessage(redirectURL, isCoApp, onClickReturnToMainPage);
        if (redirectURL == "") { //need to save loan infor 
            VL.FACTORY.onClickReturnToMainPage(e);
        }
        // location.hash = '#decisionPage'; --> location.hash is no working on IE browser
        goToNextPage($('#decisionPage'));
        e.preventDefault();
    });
    handleShowAndHideBranchNames();
    $("#popUpCreditDiff, #popUpSavingAmountGuid, #popEstimatePaymentCalculator").on("popupafteropen", function (event, ui) {
    	$("#" + this.id + "-screen").height("");
    });
    $("div[data-section='proceed-membership']").observer({
        validators: [
            function (partial) {
                if ($("#hdProceedMembership").val() == "") {
                    return "Do you wish to continue opening your membership?";
                }
                return "";
            }
        ],
        validateOnBlur: true,
        group: "ValidateProceedMembership",
        groupType: "complexUI"
    });
    $("#popEstimatePaymentCalculator").on("popupbeforeposition", function () {
	    var $container = $("#popEstimatePaymentCalculator");
    	$("div.required-field-notation", $container).remove();
    	$("input[data-field='LoanAmount']", $container).val(Common.FormatCurrency(0, true));
    	if (Common.GetFloatFromMoney($.trim($("#txtProposedLoanAmount").val())) > 0) {
    		$("input[data-field='LoanAmount']", $container).val($("#txtProposedLoanAmount").val());
    	}
    	if ($("#txtProposedLoanAmount").attr("readonly")) {
		    $("input[data-field='LoanAmount']", $container).attr("disabled", "disabled");
	    } else {
		    $("input[data-field='LoanAmount']", $container).removeAttr("disabled");
    	}
	    $("input[data-field='LoanAmount']", $container).trigger("create");
    	var $loanTerm = $("input[data-field='LoanTerm']", $container);
    	if ($("#txtLoanTerm").is(":visible")) {
    		$("select[data-field='LoanTerm']", $container).closest('div[data-role="fieldcontain"]').hide();
    		$loanTerm.val("1"); //set default value for textbox
    		if($.trim($("#txtLoanTerm").val()) !== "" && parseFloat($.trim($("#txtLoanTerm").val())) > 0) {
    			$loanTerm.val($("#txtLoanTerm").val());
		    }
    	} else {
    		$loanTerm = $("select[data-field='LoanTerm']", $container);
    		$("input[data-field='LoanTerm']", $container).closest('div[data-role="fieldcontain"]').hide();
    		$loanTerm.val($("#ddlLoanTerm").val()).selectmenu('refresh');

    	}
    	$loanTerm.closest('div[data-role="fieldcontain"]').show();

        //deprecate bc no longer need Apply btn in calc popup
    	//$("a.div-continue-button", "#popEstimatePaymentCalculator").off("click").on("click", function () {
    	//	var loanAmountStr = $("input[data-field='LoanAmount']", $container).val();
		//    var loanTermValue = "";
		//    $("#txtProposedLoanAmount").val(loanAmountStr);
		//    $.lpqValidate.hideValidation("#txtProposedLoanAmount");
    	//	if ($("#txtLoanTerm").is(":visible")) {
    	//		$("#txtLoanTerm").val($("input[data-field='LoanTerm']", $container).val());
		//	    loanTermValue = $("#txtLoanTerm").val();
    	//		$.lpqValidate.hideValidation("#txtLoanTerm");
    	//	} else {
    	//		$("#ddlLoanTerm").val($("select[data-field='LoanTerm']", $container).val()).selectmenu('refresh');
    	//		loanTermValue = $("#ddlLoanTerm").val();
    	//		$.lpqValidate.hideValidation("#ddlLoanTerm");
    	//	}
    	//	var estAmount = Common.FormatCurrency(calculateEstimatePayment(Common.GetFloatFromMoney(loanAmountStr), Common.GetFloatFromMoney(loanTermValue), Common.GetFloatFromMoney($("#spInterestRate").data("value"))), true);
    	//	$("span.est-payment-value", $("#divEstimatePayment")).text(estAmount);
    	//	$("#popEstimatePaymentCalculator").popup("close");
    	//});
    	bindEstimatePayment();
    });
    $("select", "#popEstimatePaymentCalculator").on("change", bindEstimatePayment);
    $("input", "#popEstimatePaymentCalculator").on("blur", bindEstimatePayment);
    $("input", "#popEstimatePaymentCalculator").on("keyup paste", function () {
    	setTimeout(bindEstimatePayment, 400);
    });
    $("#ddlLoanTerm, #txtProposedLoanAmount").on("change", fillEstimatePayment);
    $("#txtLoanTerm, #txtProposedLoanAmount").on("blur", fillEstimatePayment);
    $("#txtLoanTerm, #txtProposedLoanAmount").on("keyup paste", function () {
    	setTimeout(fillEstimatePayment, 400);
    });

    $("#popNotification").enhanceWithin().popup();
    //xaCombo location pool
    if ($('#hdHasZipCodePool').val() == 'Y' && $('#hdIsComboMode').val() == 'Y') {
        var $zipCodeElement = $("#txtLocationPoolCode");
        if (typeof loanProductFactory !== "undefined" && loanProductFactory !== null) {
            loanProductFactory.handledLocationPoolProducts($zipCodeElement);
        }
    } else if ($('#hdHasVLZipCodePool').val() == 'Y') {//standard vehicle loan location pool          
        VL.FACTORY.handledLocationPoolLoanInfo(VLPRODUCTS);
    }
    docScanObj = new LPQDocScan("");
    co_docScanObj = new LPQDocScan("co_");
    //InstaTouch: Call 1 ("Authorization") called on the loading "HE Info" and not after an applicant has clicked the "Continue" button
    if (window.IST) {
        IST.FACTORY.loadCarrierIdentification();
    }
});

function ScanAccept(prefix) {
	if (prefix == "") {
		docScanObj.scanAccept();
		goToNextPage("#vl2");
	} else {
		co_docScanObj.scanAccept();
		goToNextPage("#vl6");
	}
}
function acceptLaserScanResult(prefix, ele) {
	fillLaserScanResult(prefix, ele);
	$(ele).closest(".scan-result-wrapper").removeClass("open");
	var nextPage = "#vl2";
	if (prefix !== "") {
		nextPage = "#vl6";
	}
	goToNextPage(nextPage);
}

function getUserName() {
	var txtFName = $('#txtFName').val();
	var txtMName = $('#txtMName').val();
	var txtLName = $('#txtLName').val();
	//capitalize the first character
	txtFName = txtFName.replace(txtFName.charAt(0), txtFName.charAt(0).toUpperCase());
	txtMName = txtMName.replace(txtMName.charAt(0), txtMName.charAt(0).toUpperCase());
	txtLName = txtLName.replace(txtLName.charAt(0), txtLName.charAt(0).toUpperCase());
	var userName = "";
	if (txtMName != "")
		userName = txtFName + " " + txtMName + " " + txtLName;
	else
		userName = txtFName + " " + txtLName;
	return userName;
}
function fillEstimatePayment() {
	var $loanAmount = $("#txtProposedLoanAmount");
	var $loanTerm = $("#txtLoanTerm");
	var $container = $("#divEstimatePayment");
	if ($loanTerm.is(":hidden")) {
		$loanTerm = $("#ddlLoanTerm");
	}
	var loanAmount = Common.GetFloatFromMoney($loanAmount.val());
	var loanTerm = parseFloat($loanTerm.val());
	var rate = parseFloat($("#spInterestRate").data("value"));
	var estPayment = 0;
	if (loanAmount > 0 && loanTerm > 0) {
		estPayment = calculateEstimatePayment(loanAmount, loanTerm, rate);
	}
	$("span.est-payment-value", $container).text(Common.FormatCurrency(estPayment, true));
}
function calculateEstimatePayment(loanAmount, loanTerm, rate) {
	return (loanAmount * (rate/12) / 100) / (1 - Math.pow((1 + (rate/12) / 100), -1 * loanTerm));
}
function bindEstimatePayment() {
	var $container = $("#popEstimatePaymentCalculator");
	var $loanAmount = $("input[data-field='LoanAmount']", $container);
	var loanAmount = Common.GetFloatFromMoney($loanAmount.val());
	var $loanTerm = $("input[data-field='LoanTerm']", $container);
	if ($("#txtLoanTerm").is(":visible") == false) {
		$loanTerm = $("select[data-field='LoanTerm']", $container);
	}
	var loanTerm = 12;
	if ($loanTerm.length > 0 && parseFloat($loanTerm.val()) > 0) {
		loanTerm = parseFloat($loanTerm.val());
	} else {
		$loanTerm.val(12);
		if ($loanTerm.is("select")) {
			$loanTerm.selectmenu('refresh');
		}
	}
	var interestRate = $("input[data-field='InterestRate']", $container).val().replace("%", "");
	var estAmount = Common.FormatCurrency(calculateEstimatePayment(loanAmount, loanTerm, interestRate), true);
	$("[data-field='EstimatePayment']", $container).text(estAmount);
}

//$(function () {

    //EMPLogicPI = $.extend(true, {}, EMPLogic);
    //EMPLogicPI.init('ddlEmploymentStatus', '', 'vl4', 'divEmploymentStatusRest');
    //$('#vl4').on("pageshow", function() {
    //    EMPLogicPI.refreshHtml();
    //});

    //EMPLogicJN = $.extend(true, {}, EMPLogic);
    //EMPLogicJN.init('co_ddlEmploymentStatus', 'co_', 'vl8', 'co_divEmploymentStatusRest');

    ////primary previous employment
    //EMPLogicPI_PREV = $.extend(true, {}, EMPLogic);
    //EMPLogicPI_PREV.init('prev_ddlEmploymentStatus', 'prev_', 'vl4', 'prev_divEmploymentStatusRest');
    //joint previous employement
    //EMPLogicJN_PREV = $.extend(true, {}, EMPLogic);
    //EMPLogicJN_PREV.init('co_prev_ddlEmploymentStatus', 'co_prev_', 'vl8', 'co_prev_divEmploymentStatusRest');

    //$('#vl8').on("pageshow", function() {
    //    EMPLogicJN.refreshHtml();
    //});
    //$('#vl9').on("pageshow", function() {
    //    $('#divAgreementFinal').show();
    //});

    //$('#divVehicleDetail').hide();
    //var veh = document.getElementById('ddlIsNewVehicle');
    //showVehicleMileage(veh);
    //showVehicleDetail();
    //showAccordingVehicleType();
    //calculateLoanAmount();
    //SwitchHasCoAppButton(document.getElementById('ddlHasCoApplicant'), false);
    //$('input#txtVehicleMileage').blur(function() {
    //    $(this).val(Common.GetRtNumber($(this).val()));
    //});
    //$('input#txtVehicleYear').blur(function() {
    //    $(this).val(Common.GetPosNumber($(this).val(), 4));
    //});

    //user click disagree button -->popup confirm message
    //$('.redirectPage').click(function () {
    //    var hasCoApp = $('#ddlHasCoApplicant option:selected').val();
    //    var redirectURL = $('#hdRedirectURL').val();
    //    var isCoApp = false;
    //    if (hasCoApp == 'Y') {
    //        isCoApp = true;
    //    }
    //    //don't popup confirm message--> go direct to return to mainpage
    //    //displayConfirmMessage(redirectURL, isCoApp, onClickReturnToMainPage);
    //    if (redirectURL == "") { //need to save loan infor 
    //        onClickReturnToMainPage(isCoApp);
    //    }
    //    // location.hash = '#decisionPage'; --> location.hash is no working on IE browser
    //    goToNextPage($('#decisionPage'));
    //});
    // Fill in the fields with test data
    //if (getParaByName("autofill") == "true") {
    //    // Fill in the stuff
    //    autoFillData();
    //}
    //enabled I Agree button when click refresh
    //$('input.custom').attr('disabled', false);
    // if the LoanPurpose is exist ->show dropdown loan purpose
    //otherwise hide dropdown loan purpose
   //if ($('#hdHasLoanPurpose').val() == 'Y') {
   //     $('#divLoanPurpose').show();
   // } else {
   //     $('#divLoanPurpose').hide();
   //}
   //$('#txtDownPayment').blur(function () {
   //    VL.FACTORY.calculateLoanAmount();
   //});

   //var footerTheme = $('#hdFooterTheme').val();
   //var bgColor = $('.ui-bar-' + footerTheme).css('background-color');
   //var changeColor = changeBackgroundColor(bgColor, .6);
   //var bgTheme = $('#hdBgTheme').val();
   //if (bgTheme != "") {
   //    changeColor = bgColor;
   //}
   //$('body').css("background-color", changeColor);

   //handleEnableJoint();
    //handledCheckboxDisclosure();// disclosure click handled in gotoPDFViewer
    //hide downpayment field in case Auto REFINANCE set by default and user click fresh
   //refreshPurposeLabel();
   //handleRemoveBtnStyle('vl4'); //primary applicant
   //handleRemoveBtnStyle('vl8'); // for coApplicant
   //$('#ddlHasCoApplicant').on('change', function () {
   //    var hasCoApp = $(this).val();
   //    if (hasCoApp == 'Y') {
   //        $('#divHasCoApp').show();
   //        $('#divSubmitApplicant').hide();
   //    } else {
   //        $('#divHasCoApp').hide();
   //        $('#divSubmitApplicant').show();
   //    }
   //});
   //handledFontWeightOfDivContent();
    //handle disable disclosures 
   //$("div[name='disclosures'] label").click(function () {
   //    handleDisableDisclosures(this);
   //});
//});
//function handleEnableJoint() {
//    var isEnableJoint = $('#hdEnableJoint').val();
//    if (isEnableJoint != undefined) {
//        isEnableJoint = isEnableJoint.toUpperCase();
//    }
//    if (isEnableJoint == 'N') {
//        $('#divHasCoApplicant').hide();
//        //also hide all pages for joint   
//        $('#vl6').hide();
//        $('#vl8').hide();
//        $('#vl9').hide();
//        $('#divAgreement').css('margin-top', '0px');
//    }
//}
//function refreshPurposeLabel() {
//    if (VL.FACTORY.hasLoanPurpose()) { //make sure loan purpose is not null
//        var loanBalanceTitle = "Estimated Vehicle Value<span style ='color:Red'>*</span>";
//        var purchasePriceTitle = "Estimated Purchase Price<span style ='color:Red'>*</span>";

//        //TOO WEIRD LINES OF CODE. REVISE AS BELOW
//        //getSelectedCategory always return empty due to incorrect code of reading values from config
//        /*
//        var loanPurposeElement = $('#ddlLoanPurpose option:selected');
//        var selectedCategory = getSelectedCategory(loanPurposeElement);
//        if (selectedCategory.toUpperCase().indexOf("REFI") >-1) {
//            $('#divPurchaseAmountLabel').html(loanBalanceTitle);
//            $('#divDownPayment').hide(); //selected refinance ->hide down payment amount
//        } else {
//            if (loanPurposeElement.val() == undefined) {
//                $('#divPurchaseAmountLabel').html(purchasePriceTitle);
//            }
//            else if (loanPurposeElement.val().toUpperCase().indexOf('REFI') > -1 ||
//                    loanPurposeElement.val().toUpperCase().indexOf('BUYOUT') > -1) {
//                $('#divPurchaseAmountLabel').html(loanBalanceTitle);
//            }
//            else {
//                $('#divPurchaseAmountLabel').html(purchasePriceTitle);
//            }
//            //show and hide down payment amount
//            if (loanPurposeElement.val() != undefined) {
//                if (loanPurposeElement.val().toUpperCase().indexOf('REFI') > -1) {
//                    $('#divDownPayment').hide(); //selected refinance ->hide down payment amount
//                } else {
//                    $('#divDownPayment').show();
//                }
//            }
//        }//end if selectedCategory
//        */
//        var selectedLoanPurpose = $("#ddlLoanPurpose").val();
//        if (selectedLoanPurpose.toUpperCase().indexOf("REFI") > -1 || selectedLoanPurpose.toUpperCase().indexOf("BUYOUT") > -1) {
//            $('#divPurchaseAmountLabel').html(loanBalanceTitle);
//            if (selectedLoanPurpose.toUpperCase().indexOf('REFI') > -1) {
//                $('#divDownPayment').hide(); //selected refinance ->hide down payment amount
//            } else {
//                $('#divDownPayment').show();
//            }
//        } else {
//            $('#divPurchaseAmountLabel').html(purchasePriceTitle);
//        }

//    }
    
//    var selectedText = $('#ddlLoanPurpose option:selected').text();

//    if (!g_IsHideGMI && selectedText != undefined && selectedText.toUpperCase().indexOf('HMDA') != -1) {
//        $('.div_gmi_section').css('display', 'block');
//    } else {
//        $('.div_gmi_section').css('display', 'none');
//    }
//}
//function getSelectedCategory(loanPurposeElement) {
//    var selectedLoanPurpose = loanPurposeElement.text();
//    var loanPurposeCategoryObj = JSON.parse(LoanPurposeCategory);
//    for (var key in loanPurposeCategoryObj) {
//        if (key == selectedLoanPurpose) {
//            return loanPurposeCategoryObj[key];
//        }
//    }
//    return "";
//}
//function showVehicleDetail() {
//    var known = $('#ddlKnowVehicleMake option:selected').val();
//    if (known == 'Y')
//        $('#divVehicleDetail').fadeIn('fast');
//    else
//        $('#divVehicleDetail').hide();
//}

//function showAccordingVehicleType() {
//    $('#divCarMake').hide();
//    //    $('#divRVOrBoatMake').hide();
//    $('#divMotorCycleMake').hide();
//    $('#divMake').hide();

//    var vehType = $('#ddlVehicleType option:selected').val();
//    switch (vehType) {

//        case 'MOTORCYCLE':
//            $('#divMotorCycleMake').fadeIn('fast');
//            //TODO: set default selection for the rest dropdown
//            break;
//        case 'CAR':
//            $('#divCarMake').fadeIn('fast');
//            //TODO: set default selection for the rest dropdown
//            break;
//        default:
//            $('#divMake').fadeIn('fast');
//            //TODO: set default selection for the rest dropdown
//            break;
//    }
//}

//function showVehicleMileage(element) {
//    if (element.options[0].selected)
//        $('#divVehicleMileage').fadeIn('fast');
//    else
//        $('#divVehicleMileage').fadeOut('fast');
//}

//function clearAllInfo() {
//    $("input[type='text']").val('');
//    $("select").each(function() {
//        $(this).get(0).selectedIndex = 0;
//        $(this).parent().find('span.ui-btn-text').html('');
//    });
//}


// save applicant without Co/Joint applicant
//function finishMainApplicant(element) {
//    var currElement = $(element);
//    var hasCoApp = $('#ddlHasCoApplicant option:selected').val();
    
//    if (hasCoApp == "N") {
//        return saveVehicleLoanInfo(document.getElementById('href_save'));
//    }
//    else {
//        var strMessage = '';
//        strMessage = ValidateCustomQuestions();
//        //validate required dl scan 
//        strMessage += validateRequiredDLScan($('#hdRequiredDLScan').val(), true);
//        if (strMessage != '') {
//            $('#txtErrorMessage').html(strMessage);
//            currElement.next().trigger('click'); //go to error dialog
//        } else {
//            currElement.next().next().trigger('click'); // go to next page
//        }
//        return true;
//    }
//}

//function onIAgreeClick(isCoApp) {  
//    var errorStr = ""; 
//        errorStr = validateAll($(this),isCoApp);
//    if (errorStr != "") {
//        goToDialog(errorStr);
//    }else {
//        if (isCoApp) {
//            $('#href_save_end').trigger('click');
//        }else {
//            $('#href_save').trigger('click');
//        }
        
//        if (isSubmitAnswer) {
//            //the btnSubmitAnswer is disabled in 30 seconds, and it is enabled back after 30 seconds
//            $('input.custom').attr('disabled', true);
//            setTimeout(function() {
//                $('input.custom').attr('disabled', false);
//            }, 30000);
//            //reset isSubmitAnswer
//            isSubmitAnswer = false;
//        }
//    }
//}

//function saveVehicleLoanInfo(buttonToTrigger) {
//    if (g_submit_button_clicked) return false;
//    $('#txtErrorMessage').html('');
//    var loanInfo = getVehicleLoanInfo(false);
//    $.ajax({
//        type: 'POST',
//        url: 'CallBack.aspx',
//        data: loanInfo,
//        dataType: 'html',
//        success: function(response) {
//            if (response.toUpperCase().indexOf('MLERRORMESSAGE') > -1) {  
//                $('input.custom').attr('disabled', false); //if there is errors, enable agree button
//                goToDialog(response);
//            } else if (response.toUpperCase().indexOf('WALLETQUESTION') != -1) {
//                displayWalletQuestions(response);
//            } else {
//                //submit sucessfull so clear
//                clearForms();
//                goToLastDialog(response);
//            }
//        },
//        error: function(err) {      
//            $('input.custom').attr('disabled', false); ////if there is errors, enable agree button
//            goToDialog('There was an error when processing your request.  Please try the application again.');
      
//        }
//    });
//    //there no error
//    isSubmitAnswer = true;
//    return false;
//}

//function getVehicleLoanInfo(disagree_check) {
//    var appInfo = new Object();
//    appInfo.PlatformSource = $('#hdPlatformSource').val();
//    appInfo.LenderRef = $('#hfLenderRef').val();
//    appInfo.Task = "SubmitLoan";
//    if (disagree_check) {
//        appInfo.isDisagreeSelect = "Y";
//    } else {
//        appInfo.isDisagreeSelect = "N";
//    }
//  /* if ($('.checkDisclosure').val() != undefined) {
//        //get all disclosures 
//        var disclosures = "";
//        var temp = "";
//        $('.checkDisclosure').each(function() {
//            temp = $(this).text().trim().replace(/\s+/g, " ");
//            disclosures += temp + "\n\n";
//        });
//        appInfo.Disclosure = disclosures;
//    }*/
//    if (parseDisclosures() != "") {
//        appInfo.Disclosure = parseDisclosures();
//    }
//    //    branchID from query Param has precedent over  branchID from dropdown 
//    appInfo.BranchID = $('#hfBranchId').val();
//    if (appInfo.BranchID == "") {
//        appInfo.BranchID = $('#ddlBranches option:selected').val();
//    }
//    appInfo.ReferralSource = $('#hfReferralSource').val();
//    //appInfo.MemberProtectionPlan = '';
//    appInfo.LoanTerm = $("#ddlLoanTerm option:selected").val();
//    if (VL.FACTORY.hasLoanPurpose) {
//        appInfo.LoanPurpose = $("#ddlLoanPurpose option:selected").val();
//    } else {
//        appInfo.LoanPurpose = " "; //there is no loan purpose, just pass empty string 
//    }
//    appInfo.VehicleType = $('#ddlVehicleType option:selected').val();
//    appInfo.KnowVehicleMake = $('#ddlKnowVehicleMake option:selected').val();

//    var make = '';
//    var model = '';
//    var year = '';
//    if (appInfo.KnowVehicleMake == 'Y') {
//        switch (appInfo.VehicleType) {
//            case 'CAR':
//                make = $('#ddlCarMake option:selected').val();
//                break;
//            case 'MOTORCYCLE':
//                make = $('#ddlMotorCycleMake option:selected').val();
//                break;
//            default:
//                make = $('#txtMake').val();
//                break;
//        }
//        model = $('#txtVehicleModel').val();
//        year = $('#txtVehicleYear').val();
//    }
//    appInfo.VehicleMake = make;
//    appInfo.VehicleModel = model;
//    appInfo.VehicleYear = year;

//    var mileage = '';
//    appInfo.IsNewVehicle = $('#ddlIsNewVehicle option:selected').val();
//    if (appInfo.IsNewVehicle != 'Y') {
//        mileage = $('#txtVehicleMileage').val();
//    }

//    appInfo.VehicleMileage = mileage;
//    var isRefinance = false;

//    //TOO WEIRD LINES OF CODE. REVISE AS BELOW
//    //getSelectedCategory always return empty due to incorrect code of reading values from config
//    /*var loanPuroseElement = $('#ddlLoanPurpose option:selected');
//    var selectedCategory = getSelectedCategory(loanPuroseElement);
//    if (selectedCategory.toUpperCase().indexOf('REFI') > -1) {
//        isRefinance = true;
//    } else {
//        if (loanPuroseElement.val() != undefined) {
//            if (loanPuroseElement.val().toUpperCase().indexOf('REFI') > -1) {
//                isRefinance = true;
//            }
//        }
//    }*/
//    if ($("#ddlLoanPurpose").val().toUpperCase().indexOf("REFI") > -1) {
//        isRefinance = true;
//    }


//    //if (isRefinance) {
//    //    appInfo.VehicleValue ="0";
//    //} else {
//    //    appInfo.VehicleValue = Common.GetFloatFromMoney($('#txtEstimatedVehicleValue').val());
//    //}
//    appInfo.VehicleValue = Common.GetFloatFromMoney($('#txtEstimatedVehicleValue').val());
//    if (isRefinance) {
//        appInfo.DownPayment = "0";
//    }
//    else {
//        appInfo.DownPayment = Common.GetFloatFromMoney($('#txtDownPayment').val());
//    }
//    appInfo.ProposedLoanAmount = Common.GetFloatFromMoney($('#txtProposedLoanAmount').val());
//    //get loan ida
//    appInfo.idaMethodType = $('#hdIdaMethodType').val();
//    // Custom Ansers
//    appInfo.CustomAnswers = GetCustomAnswers();

//    // Main-App
//    // Applicant Info
//    SetApplicantInfo(appInfo);
//    // Address
//    SetApplicantAddress(appInfo);
//    //Previous Address
//    SetApplicantPreviousAddress(appInfo);
//    // GMI
//    SetGMIInfo(appInfo);
//    // Financial
//    SetApplicantFinancialInfo(appInfo);
//    var hasCoApp = $("#ddlHasCoApplicant option:selected").val();
//    //upload document data and info
//    SetUploadDocument(appInfo, hasCoApp);
//    appInfo.HasCoApp = hasCoApp;
//    appInfo.CoAppJoin = $("#ddlCoApplicantJoint option:selected").val();
//    if (appInfo.HasCoApp == 'Y') {
//        // Co-App
//        // Co-Applicant Info
//        co_SetApplicantInfo(appInfo);
//        // Co-Address
//        co_SetApplicantAddress(appInfo);
//        //Co-Previous Address
//        co_SetApplicantPreviousAddress(appInfo);
//        // GMI
//        co_SetGMIInfo(appInfo);
//        // Co-Financial
//        co_SetApplicantFinancialInfo(appInfo);
//    }

//    return appInfo;
//}
//function calculateLoanAmount() {
//    var estVehValue = Common.GetFloatFromMoney($('#txtEstimatedVehicleValue').val());
//    var downPayment = Common.GetFloatFromMoney($('#txtDownPayment').val());

//    var x = estVehValue == '' ? 0 : estVehValue;
//    var y = downPayment == '' ? 0 : downPayment;
//    //if selected refinance -> no down payment
//    if (VL.FACTORY.hasLoanPurpose) { //make sure the ddlLoanPurpose is existed
//        //TOO WEIRD LINES OF CODE. REVISE AS BELOW
//        //getSelectedCategory always return empty due to incorrect code of reading values from config
//        /*var loanPurposeElement = $('#ddlLoanPurpose option:selected');
//        if (loanPurposeElement.val() != undefined) {
//            var selectedCategory = getSelectedCategory(loanPurposeElement);
//            if (selectedCategory.toUpperCase().indexOf('REFI') > -1) {
//                //get loan amount from user input
//                //allow refi loan amount greater than value of car
//                $('#txtProposedLoanAmount').removeAttr("readonly");
//                $('#txtProposedLoanAmount').removeAttr("style");
//            } else {
//                if (loanPurposeElement.val().toUpperCase().indexOf('REFI') > -1) {
//                    //get loan amount from user input
//                    //allow refi loan amount greater than value of car
//                    $('#txtProposedLoanAmount').removeAttr("readonly");
//                    $('#txtProposedLoanAmount').removeAttr("style");

//                } else { //need to calculate loan amount
//                    if (x < y || (x == 0 && y == 0)) {
//                        $('#txtProposedLoanAmount').val('');
//                    } else {
//                        if (x >= 0 && y >= 0) {
//                            var result = Common.FormatCurrency(x - y, true);
//                            $('#txtProposedLoanAmount').val(result);
//                        } else {
//                            $('#txtProposedLoanAmount').val('');
//                        }
//                    }
//                    //restore attribute previous removed by refi purpose
//                    if ($('#txtProposedLoanAmount').attr("readonly") == undefined) {
//                        $('#txtProposedLoanAmount').attr("readonly", "readonly");
//                        $('#txtProposedLoanAmount').attr("style", "background-color: #ddd;");
//                    }
//                }
//            }//end if selectedCategory
//        }
//        */
//        if ($("#ddlLoanPurpose").val().toUpperCase().indexOf("REFI") > -1) {
//            //get loan amount from user input
//            //allow refi loan amount greater than value of car
//            $('#txtProposedLoanAmount').removeAttr("readonly");
//            $('#txtProposedLoanAmount').removeAttr("style");
//        } else {//need to calculate loan amount
//            if (x < y || (x == 0 && y == 0)) {
//                $('#txtProposedLoanAmount').val('');
//            } else {
//                if (x >= 0 && y >= 0) {
//                    $('#txtProposedLoanAmount').val(Common.FormatCurrency(x - y, true));
//                } else {
//                    $('#txtProposedLoanAmount').val('');
//                }
//            }
//            //restore attribute previous removed by refi purpose
//            if ($('#txtProposedLoanAmount').attr("readonly") == undefined) {
//                $('#txtProposedLoanAmount').attr("readonly", "readonly");
//                $('#txtProposedLoanAmount').attr("style", "background-color: #ddd;");
//            }
//        }



//    } else {//loanpurose is not exist
//        if (x >= y) {
//            var result = Common.FormatCurrency(x - y, true);
//            $('#txtProposedLoanAmount').val(result);
//        } else {
//            $('#txtProposedLoanAmount').val('');
//        }
//    }
   
//}

//function validateVehicleInformation(element) {
//    var currElement = $(element);
//    $('#txtErrorMessage').html('');
//    var ddlLoanPurpose = $('#ddlLoanPurpose option:selected').val();
//    var ddlVehicleType = $('#ddlVehicleType option:selected').val();
//    //var hasLoanPurpose = $('#hdHasLoanPurpose').val();
//    var hasLoanPurpose = VL.FACTORY.hasLoanPurpose();
//    var ddlKnowVehicleMake = $('#ddlKnowVehicleMake option:selected').val();
//    var txtVehicleModel = $('#txtVehicleModel').val();
//    var txtVehicleYear = $('#txtVehicleYear').val();
//    var ddlVehicleMake = $('#ddlCarMake option:selected').val();

//    if (ddlKnowVehicleMake == 'Y') {
//        if (ddlVehicleType == 'CAR') {
//            var ddlVehicleMake = $('#ddlCarMake option:selected').val();
//        } else if (ddlVehicleType == 'MOTORCYCLE') {
//            var ddlVehicleMake = $('#ddlMotorCycleMake option:selected').val();
//        } else {
//            var ddlVehicleMake = $('#txtMake').val();
//        }
//    }

//    var ddlIsNewVehicle = $('#ddlIsNewVehicle option:selected').val();
//    var txtVehicleMileage = '0';
//    var txtEstimatedVehicleValue = $('#txtEstimatedVehicleValue').val();
//    var ddlLoanTerm = $('#ddlLoanTerm option:selected').val();
//    var ddlBranches = $('#ddlBranches option:selected').val();
//    var iBranchVisible = $('#VLBranchesDiv').length;
//    if (ddlIsNewVehicle == 'N') {
//        var txtVehicleMileage = $('#txtVehicleMileage').val();
//    }

//    var strMessage = '';
//    var downPayment = $('#txtDownPayment').val();  
//    var txtProposedLoanAmount = $('#txtProposedLoanAmount').val();
//    //    if (!Common.ValidateRadioGroup('rdProtectionForYourLoan'))
//    //        strMessage += 'Select a Member Protection Plan option<br />';
//    if (hasLoanPurpose) { //validate loan purpose if it exists
//        if (!Common.ValidateText(ddlLoanPurpose))
//            strMessage += 'Loan Purpose is required<br />';
//    }
//    if (!Common.ValidateText(ddlVehicleType)){
//        strMessage += 'Vehicle Type is required<br />';
//    }
//    if (hasLoanPurpose) {
//        //TOO WEIRD LINES OF CODE. REVISE AS BELOW
//        //getSelectedCategory always return empty due to incorrect code of reading values from config
//        /*var loanPurposeElement = $('#ddlLoanPurpose option:selected');
//        var selectedCategory = getSelectedCategory(loanPurposeElement);
//        if (selectedCategory.toUpperCase().indexOf('REFI') > -1) { //if category is "REFINANCE"-->validate loan balance field
//           if (!Common.ValidateText(txtEstimatedVehicleValue)) {
//               strMessage += 'Estimated Vehicle Value is required.<br />';
//           }
//        } else {
//            if (!Common.ValidateText(txtEstimatedVehicleValue)) {                           
//               if (loanPurposeElement.val().toUpperCase().indexOf('REFI') > -1 ||loanPurposeElement.val().toUpperCase().indexOf('BUYOUT') > -1) {
//                   strMessage += 'Estimated Vehicle Value is required.<br />';
//               }else {
//                  strMessage += 'Estimated Purchase Price is required.<br />';
//               }               
//            } else {                    
//                if (loanPurposeElement.val().toUpperCase().indexOf('BUYOUT') > -1) {
//                    //make sure down payment <=loan balance
//                    if (Common.GetFloatFromMoney(downPayment) > Common.GetFloatFromMoney(txtEstimatedVehicleValue)) {
//                        strMessage += "The Down Payment Amount must be less than the Estimated Vehicle Value.<br/>";
//                    }
//                } else{
//                    if (loanPurposeElement.val().toUpperCase().indexOf('REFI') == -1) {
//                        //make sure down payment <= purchase price
//                        if (Common.GetFloatFromMoney(downPayment) > Common.GetFloatFromMoney(txtEstimatedVehicleValue)) {
//                            strMessage += "The Down Payment Amount must be less than the Estimated Purchase Price.<br/>";
//                        }
//                        //down payment >=0
//                        if (!Common.ValidateTextAllowZero(downPayment)) {
//                            strMessage += 'Down Payment Amount is required<br />';
//                        }
//                    }//end if loanPurposeElement.val ...'REFI'
//                }
//            }//end if !Common.ValidateText ...
//        } //end if selectedCategory
//        */
//        var selectedLoanPurpose = $("#ddlLoanPurpose").val().toUpperCase();
//        if (!Common.ValidateText(txtEstimatedVehicleValue)) {
//            if (selectedLoanPurpose.indexOf('REFI') > -1 || selectedLoanPurpose.indexOf('BUYOUT') > -1) {
//                strMessage += 'Estimated Vehicle Value is required.<br />';
//            } else {
//                strMessage += 'Estimated Purchase Price is required.<br />';
//            }
//        } else {
//            if (selectedLoanPurpose.indexOf('BUYOUT') > -1) {
//                //make sure down payment <=loan balance
//                if (Common.GetFloatFromMoney(downPayment) > Common.GetFloatFromMoney(txtEstimatedVehicleValue)) {
//                    strMessage += "The Down Payment Amount must be less than the Estimated Vehicle Value.<br/>";
//                }
//            } else {
//                if (selectedLoanPurpose.indexOf('REFI') == -1) {
//                    //make sure down payment <= purchase price
//                    if (Common.GetFloatFromMoney(downPayment) > Common.GetFloatFromMoney(txtEstimatedVehicleValue)) {
//                        strMessage += "The Down Payment Amount must be less than the Estimated Purchase Price.<br/>";
//                    }
//                    //down payment >=0
//                    if (!Common.ValidateTextAllowZero(downPayment)) {
//                        strMessage += 'Down Payment Amount is required<br />';
//                    }
//                }
//            }
//        }


//    }else{ //loanpurpose is not exist
//            if (!Common.ValidateText(txtEstimatedVehicleValue)) {
//                strMessage += 'Estimated Vehicle Value is required<br />';
//            }else{
//                if (Common.GetFloatFromMoney(downPayment) > Common.GetFloatFromMoney(txtEstimatedVehicleValue)) {
//                    strMessage += "Down Payment Amount must be less than the Estimated Purchase Price.<br/>";
//                }
//            }
//            if (!Common.ValidateTextAllowZero(downPayment)) {
//                strMessage += 'Down Payment Amount is required<br />';
//            }
//    } //end if hasLoanPurpose ...

//    if (!Common.ValidateText(txtProposedLoanAmount))
//        strMessage += 'Requested Loan Amount is required<br />';
 
//    if (ddlLoanTerm=='0'||ddlLoanTerm=='') {
//        strMessage += 'Loan Term is required<br />';
//    }

//    if (ddlKnowVehicleMake == 'Y') {
//        if (!Common.ValidateText(ddlVehicleMake))
//            strMessage += 'Vehicle Make is required<br />';
//        if (!Common.ValidateText(txtVehicleModel))
//            strMessage += 'Vehicle Model is required<br />';
//        if (!Common.ValidateText(txtVehicleYear))
//            strMessage += 'Vehicle Year is required<br />';
//    }

//    if (!Common.ValidateText(ddlBranches) && iBranchVisible != 0) {
//        strMessage += 'Select the answer for: "Which branch is most convenient for you?"<br />';
//    }

//    //validate required dl scan
//    strMessage += validateRequiredDLScan($('#hdRequiredDLScan').val(),false);
//    //validate verify code
//    strMessage += validateVerifyCode();
//        if (!Common.ValidateDisclosures('disclosures')) {
//        strMessage += 'All disclosures need to be checked<br />';
//    }

//    if (strMessage != '') {
//        $('#txtErrorMessage').html(strMessage);
//        currElement.next().trigger('click');
//    } else
//        currElement.next().next().trigger('click');
    
//    return strMessage;
//}

//function validateApplicantInformation(element, isCoApp) {
//    var currElement = $(element);
//    $('#txtErrorMessage').html('');
//    isCoApp = typeof isCoApp !== 'undefined' ? isCoApp : false;
//    var strMessage = '';
//    if (isCoApp) {
//        strMessage = co_ValidateApplicantInfo();
//        strMessage += co_ValidateApplicantAddress();
//        strMessage += co_ValidateApplicantPreviousAddress();
//        strMessage += co_ValidateApplicantMailingAddress();
//    } else {
//        strMessage = ValidateApplicantInfo();
//        strMessage += ValidateApplicantAddress();
//        strMessage += ValidateApplicantPreviousAddress();
//        strMessage += ValidateApplicantMailingAddress();
//    }

//    if (strMessage != '') {
//        $('#txtErrorMessage').html(strMessage);
//        currElement.next().trigger('click');
//    } else {
//        currElement.next().next().trigger('click');
//    }
//    return strMessage;
//}

//function validateEmployeeInformation(element, isCoApp) {
//    var currElement = $(element);
//    $('#txtErrorMessage').html('');
//    isCoApp = typeof isCoApp !== 'undefined' ? isCoApp : false;
//    var strMessage = '';
//    if (isCoApp) {
//        strMessage = co_ValidateApplicantFinancialInfo();
//        strMessage += co_validateUploadDocument();
//    } else {
//        strMessage = ValidateApplicantFinancialInfo();
//        strMessage += validateUploadDocument();
//    }

//    if (strMessage != '') {
//        $('#txtErrorMessage').html(strMessage);
//        if (disagree_check != true)
//            currElement.next().trigger('click');
//    }
//    else {
//            currElement.next().next().trigger('click');
//        }
//        return strMessage;
//}
//function validateAll(element, isCoApp) {
//    // Validate all the screens again before submitting.
//    var errorStr = "";
//    if (validateVehicleInformation(element) != "") {
//        errorStr += "Please complete all required fields for vehicle loan information.<br/>";
//    }
//    if (ValidateApplicantInfo() != "") {
//        errorStr += "Please complete all required fields for applicant information.<br/>";
//    }
//    if (ValidateApplicantAddress() != "") {
//        errorStr += "Please complete all required fields for current address.<br/>";
//    }
//    if (ValidateApplicantFinancialInfo() != "") {
//        errorStr += "Please complete all required fields for financial information.<br/>";
//    }
//    if (ValidateCustomQuestions() != "") {
//        errorStr += ValidateCustomQuestions();
//    }
//    if (isCoApp) {
//        if (co_ValidateApplicantInfo() != "") {
//            errorStr += "Please complete all required fields for Co-Applicant information.<br/>";
//        }
//        if (co_ValidateApplicantAddress() != "") {
//            errorStr += "Please complete all required fields for Co-Applicant current address.<br/>";
//        }
//        if (co_ValidateApplicantFinancialInfo() != "") {
//            errorStr += "Please complete all required fields for Co-Applicant financial information.<br/>";
//        }
//    }
//    return errorStr;
//}
////**********start wallet questions and answers ************
function displayWalletQuestions(message) {
    // The first two characters of the message contains the number of questions
    var numQuestions = parseInt(message.substr(0, 2), 10);
    var questionsHTML = message.trim().substr(2, message.length - 2);
    // Put the number of questions into the hidden input field
    $('#hdNumWalletQuestions').val(numQuestions);
    // Put the HTML for the questions into the proper location in our page
    $('#walletQuestionsDiv').html(questionsHTML);
    var nextpage = $('#walletQuestions');
    $.mobile.changePage(nextpage, {
        transition: "fade",
        reverse: false,
        changeHash: true
    });
}
//jquery mobile need to transition to another page in order to format the html autheitcation question correctly
function co_displayWalletQuestions(message) {
    // The first two characters of the message contains the number of questions
    var numQuestions = parseInt(message.substr(0, 2), 10);
    var questionsHTML = message.substr(2, message.length - 2);
    // Put the number of questions into the hidden input field
    $('#hdNumWalletQuestions').val(numQuestions);
    // Put the HTML for the questions into the proper location in our page   
    $('#walletQuestionsDiv').empty();
    $('#co_walletQuestionsDiv').html(questionsHTML);
    nextpage = $('#co_walletQuestions');
    $.mobile.changePage(nextpage, {
        transition: "slide",
        reverse: false,
        changeHash: true
    });
}
function validateWalletQuestions(element, isCoApp) {
	if ($(element).attr("contenteditable") == "true") return false; 
    // Make sure that there is a selected answer for each question.
    // After validating the questions, submit them
    var index = 0;
    var numSelected = 0;
    var errorStr = "";
    var numOfQuestions = $('#hdNumWalletQuestions').val();
    numOfQuestions = parseInt(numOfQuestions, 10);
    for (index = 1; index <= numOfQuestions; index++) {
        // Check to see if a radio button was selected
        var radioButtons = $('input[name=walletquestion-radio-' + index.toString() + ']');
        var numRadioButtons = radioButtons.length;
        for (var i = 0; i < numRadioButtons; i++) {
            if ($(radioButtons[i]).prop('checked') == true) {
                numSelected++;
            }
        }
        if (numSelected == 0) {
            errorStr += "Please answer all the questions";
            break;
        }
        numSelected = 0;
    }
    if (errorStr != "") {
    	// An error occurred, we need to bring up the error dialog.
    	if ($("#divErrorPopup").length > 0) {
    		$('#txtErrorPopupMessage').html(errorStr);
    		$("#divErrorPopup").popup("open", { "positionTo": "window" });
    	} else {
    		var nextpage = $('#divErrorDialog');
    		$('#txtErrorMessage').html(errorStr);
    		if (nextpage.length > 0) {
    			$.mobile.changePage(nextpage, {
    				transition: "slide",
    				reverse: false,
    				changeHash: true
    			});
    		}
    	}
        
        return false;
    }
    // No errors, so we can proceed
    submitWalletAnswers(isCoApp);  
    return false;
}

function submitWalletAnswers(isCoApp) {
    var coPrefix = "";
    if (isCoApp) coPrefix = "co_";
    var $submitWQButton = $('#' + coPrefix + 'walletQuestions .div-continue-button');
    //disable submit button before calling ajax to prevent user submit multiple times
    $submitWQButton.addClass('ui-disabled');
    $('#txtErrorMessage').html('');
    var answersObj = getWalletAnswers(isCoApp);
    answersObj = attachGlobalVarialble(answersObj);
    if ($("#hdIsComboMode").val() == "Y") {   
        if ($("#divCcFundingOptions").length === 1) {
            answersObj.depositAmount = Common.GetFloatFromMoney($('#txtFundingDeposit').val());
            FS1.loadValueToData();
            answersObj.rawFundingSource = JSON.stringify(FS1.Data);
        }
    }
    $.ajax({
        type: 'POST',
        url: 'Callback.aspx',
        data: answersObj,
        dataType: 'html',
        success: function (response) {
            if (response.toUpperCase().indexOf('MLERRORMESSAGE') > -1) {
                $submitWQButton.removeClass('ui-disabled'); //enable submit button
                goToDialog(response);
            }
            else {
                if (response.toUpperCase().indexOf('MLNOIDAUTHENTICATION') > -1) {                
                    goToLastDialog(response);              
                } else if (response.toUpperCase().indexOf('WALLETQUESTION') > -1) {
                    co_displayWalletQuestions(response);                 
                } else {                 
                    goToLastDialog(response);            
                }
            }
        },
        error: function (err) {
            //enable submit answer button 
            $submitWQButton.removeClass('ui-disabled');
            goToDialog('There was an error when processing your request.  Please try the application again.');
        }
    });
    return false;
}


function SelectProduct(index) {
    $('#txtErrorMessage').html('');
    var appInfo = new Object();
    appInfo.LenderRef = $('#hfLenderRef').val();
    appInfo.Index = index;
    appInfo.Task = 'SelectProduct';
    var txtLName = $('#txtLName').val();
    var txtFName = $('#txtFName').val();
    appInfo.FullName = txtFName + ' ' + txtLName;
    appInfo = attachGlobalVarialble(appInfo);
    $.ajax({
        type: 'POST',
        url: 'CallBack.aspx',
        data: appInfo,
        dataType: 'html',
        success: function (response) {
            if (response.toUpperCase().indexOf('MLERRORMESSAGE') > -1) {
                goToDialog(response);
            } else {
                //submit sucessfull so clear
                clearForms();
                goToLastDialog(response);
            }
        },
        error: function (err) {
            $('input.custom').attr('disabled', false); //if there is errors, enable agree button
            goToDialog('There was an error when processing your request.  Please try the application again.');
        }
    });
    //there no error
    return false;
}

function goToDialog(message) {
	if ($("#divErrorPopup").length > 0) {
		$('#txtErrorPopupMessage').html(message);  //error message are not encoded, endoding will remove html tag
		$("#divErrorPopup").popup("open", { "positionTo": "window" });
	} else {
		$('#txtErrorMessage').html(message);  //error message are not encoded, endoding will remove html tag
		$('#href_show_dialog_1').trigger('click');
	}
}
function hasCoApplicant() {
	return $("#hdVlHasCoApplicant").val() === "Y";
}
function goBackPageSubmit() {
	if (typeof resetFunding == 'function') {
		resetFunding();
	}
    if ($("#hdIs2ndLoanApp").val() === "Y") {
        goToNextPage("#vl1");
    } else {
	    if (window.ASI) {
		    ASI.FACTORY.goBackFromPageSubmit();
	    } else {
	    	if ($("#hdVlHasCoApplicant").val() === "Y") {
	    		goToNextPage("#vl6");
	    	} else {
	    		goToNextPage("#vl2");
	    	}
	    }
    }
	
}

function goToLastDialog(message) {
    //var decoded_message = $('<div/>').html(message).text(); //message maybe encoded in the config xml so need to decode
    // $('#div_system_message').html(decoded_message); ->already decoded in callback.aspx
    //application completed so clear
    clearForms();
    $('#div_system_message').html(message);

    //docusign window if enabled
    if ($('#div_system_message').find("iframe#ifDocuSign").length === 1) {
        appendDocuSign();
        $("#div_docuSignIframe").append($('#div_system_message').find("iframe#ifDocuSign"));

        ////goback to final decision message after docusign is closed
        //$('#div_docuSignIframe').find("iframe#ifDocuSign").on("load", function (evt) {
        //    //the initial page load twice so need this to make sure only capturing url on finish
        //    var flag = ($(this).data("loaded") || "") + "1";
        //    $(this).data("loaded", flag);
        //    if (flag === "111") {
        //        $('#href_show_last_dialog').trigger('click');
        //    }
        //});

        var height = window.innerHeight ||
                 document.documentElement.clientHeight ||
                 document.body.clientHeight;

        var footerHeight = $('.divfooter').height();

        height = height - footerHeight - 64;
        $('#div_docuSignIframe').find("iframe#ifDocuSign").attr("height", height);
        $.mobile.changePage("#popupDocuSign", {
            transition: "fade",
            reverse: false,
            changeHash: false
        });
    }
        //cross-sell link button if enabled
    else {

        $(".xsell-selection-panel .xsell-item-option.btn-header-theme", "#div_system_message").on("mouseenter click", function (ev) {
            var element = $(this);
            if (ev.type != 'click') {
                element.addClass('btnHover');
                element.removeClass('btnActive_on');
                element.removeClass('btnActive_off');
            } else {
                element.removeClass('btnHover');
            }
        }).on("mouseleave", function () {
            handledBtnHeaderTheme($(this));
        });
        $('#href_show_last_dialog').trigger('click');
    }
}
function getWalletAnswers(isCoApp) {
    // Grab the answer choices that have been selected by the user
	var appInfo = VL.FACTORY.getVehicleLoanInfo(false);
    /*var index = 0;
    var numSelected = 0;
    var numOfQuestions = $('#hdNumWalletQuestions').val();
    numOfQuestions = parseInt(numOfQuestions, 10);
    var answersArray = new Array();
    for (index = 1; index <= numOfQuestions; index++) {
        var selectedAnswer = $('input[name=walletquestion-radio-' + index.toString() + ']:checked').attr('id');
        if (selectedAnswer != "" && selectedAnswer != null) {
            //exclude "rq"+i+"_" prefix from selectectAnswer
            var rqPrefixLength = 4;
            if (i >= 10) {
                rqPrefixLength = 5;
            }
            answersArray[index - 1] = selectedAnswer.substring(rqPrefixLength, selectedAnswer.length);
        }
    }
    // Put the answers into a semicolon separated string
    // Only the ids of the answers will be passed to the callback code
    var walletAnswersConcatStr = "";
    for (var i = 0; i < numOfQuestions; i++) {
        walletAnswersConcatStr += answersArray[i] + ";";
    }
    // Get rid of the last semicolon
    walletAnswersConcatStr = walletAnswersConcatStr.substr(0, walletAnswersConcatStr.length - 1);
    appInfo.WalletAnswers = walletAnswersConcatStr;
    */

    //get wallet questions and answers
    getWalletQuestionsAndAnswers(appInfo);

    // appInfo.CSRF = $('#hfCSRF').val();
    appInfo.LenderRef = $('#hfLenderRef').val();
    appInfo.Task = "WalletQuestions";
    appInfo.idaMethodType = $('#hdIdaMethodType').val();
    appInfo.co_FirstName = $('#co_txtFName').val();
    appInfo.ProceedXAAnyway = $("#hdProceedMembership").val();
    appInfo.IsComboMode = $("#hdIsComboMode").val();
    if (isCoApp) {
        appInfo.hasJointWalletAnswer = "Y";
    } else {
        appInfo.hasJointWalletAnswer = "N";
    }
    return appInfo;
}
//$(function () {
//    // MODIFIED AND MOVED TO validateWalletQuestions
//    //disable Submit Answers button after clicking submit to avoid click submit multiple times
//    $('input.btnSubmitAnswer').click(function () {
//        if (isSubmitAnswer) {
//            $('input.btnSubmitAnswer').attr('disabled', true);
//        }
//    });

//    //disable Submit Answers button after clicking submit to avoid click submit multiple times
//    $('input.co_btnSubmitAnswer').click(function () {
//        if (isCoSubmitAnswer) {
//            $('input.co_btnSubmitAnswer').attr('disabled', true);
//        }
//    });
//    if (!isSubmitAnswer) { //not success -> keep submit button enable
//        $('input.btnSubmitAnswer').attr('disabled', false);
//    }
//    if (!isCoSubmitAnswer) { //not success -> keep submit button enable
//        $('input.co_btnSubmitAnswer').attr('disabled', false);
//    }
//});
//**********end wallet questions and answers ************
//autofile() for testing purpose
function autoFillData() {
	$('#ddlBranchName option').eq(1).prop('selected', true);
	$('#ddlBranchName').selectmenu("refresh").closest("div.ui-btn").addClass("btnActive_on");
	$("#divLoanPurpose a.btn-header-theme[data-role='button']:eq(0)").trigger("click");
	$('#ddlVehicleType').val("ATV").selectmenu("refresh");
    $('#txtEstimatedVehicleValue').val("30000").trigger("blur");
    $('#txtDownPayment').val("5000").trigger("blur");
    $("#txtLoanTerm").val("60");
    var loanPurpose = $("#hdLoanPurpose").val() != undefined ? $("#hdLoanPurpose").val().toUpperCase() : "";   
    if (loanPurpose.indexOf("REFI") == -1) { //check category 
        $("#chkVehicleNew").prop("checked", true).checkboxradio("refresh");
        $("#chkDontKnowMake").prop("checked", true).checkboxradio("refresh");
        $("#hdVlIsNewVehicle").val("Y");
        $("#hdVlKnowVehicleMake").val("N");
    }
    autoFillData_PersonalInfor();
    co_autoFillData_PersonalInfor();
    autoFillData_Address();
    co_autoFillData_Address();
    autoFillData_FinancialInFor();
    co_autoFillData_FinancialInFor();
    autoFillData_ContactInfor();
    co_autoFillData_ContactInfor();
    if ($("#txtIDDateIssued").length > 0) {
        autoFillData_ID();
    }
    if ($("#co_txtIDDateIssued").length > 0) {
        co_autoFillData_ID();
    }
    if ($("#divDeclarationSection").length > 0) {
    	autofill_Declarations();
    }
    if ($("#co_divDeclarationSection").length > 0) {
    	co_autofill_Declarations();
    }
    if (typeof autofill_Disclosure == "function") {
    	autofill_Disclosure();
    }   
    if ($("#divProceedMembership").length > 0) {
        $("a.btn-header-theme[data-command='proceed-membership'][data-key='Y']", "#divProceedMembership").addClass("active btnActive_on");
        $("#hdProceedMembership").val("Y");
    }
}
function bindSavedData(appInfo) {

}

