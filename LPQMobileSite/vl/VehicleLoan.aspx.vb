﻿Imports DownloadedSettings
Imports LPQMobile.Utils
Imports System.Xml
Imports System.Web.Script.Serialization

Partial Class vl_VehicleLoan
    Inherits CBasePage

    'Protected _Log As ILog = LogManager.GetLogger("VehicleLoan")

    Protected _VehicleLoanPurposeDropdown As String
    Protected _HasLoanPurpose As String
    Protected _VehicleLoanPurposeList As Dictionary(Of String, String)
    Protected _enableJoint As Boolean = True
    Protected _VehicleTypes As String
    Protected _CarMakeDropdown As String
    Protected _MotorCycleMakeDropdown As String
    Protected _RVOrBoatMakeDropdown As String
    Protected _NextScreen As String
    Protected _BackScreen As String
    Protected _BranchDropdown As String
	Protected _address_key As String
	Protected _declarationList As Dictionary(Of String, String)
    ''Protected _visibleCustomQuestion As String
	'Protected _bgTheme As String
    Protected _hasScanDocumentKey As String
	Protected _SubmitCancelMessage As String
    Protected _LoanPurposeCategory As String

    Protected _LoanTermDropdown As String = ""
	Protected _CreditPullMessage As String = ""
	Protected _showProductSelection As Boolean = True
    Protected _prerequisiteProduct As New CProduct
	Protected _CCMaxFunding As String = ""
	Protected _ACHMaxFunding As String = ""
	Protected _PaypalMaxFunding As String = ""
	Protected _ccStopInvalidRoutingNumber As String = ""
    Protected _SID As String = ""
    Protected _xaXSellComment As String = ""
	Protected _InterestRate As Double = 0D
    Protected _prefillData As Dictionary(Of String, String)
	Protected _coPrefillData As Dictionary(Of String, String)
	Protected _branchOptionsStr As String = ""
	Protected LaserDLScanEnabled As Boolean = False
	Protected LegacyDLScanEnabled As Boolean = False
	Protected EnableVinNumber As Boolean = False
	Protected VinBarcodeScanEnabled As Boolean = False
	Protected _autoTermTextboxEnable As String = ""
    Protected LoanSavingItem As SmLoan
    Protected _vlLocationPool As Boolean = False
    Protected _vlZipCodePoolProducts As List(Of DownloadedSettings.CVehicleLoanProduct)
    Protected _zipCodePoolList As New Dictionary(Of String, List(Of String))
    Protected _zipCodePoolNames As New Dictionary(Of String, String)
#Region "Property for SSO"
    Protected ReadOnly Property LoanPurpose() As String
        Get
            Return Common.SafeEncodeString(Request.Params("LoanPurpose"))
        End Get
    End Property

    Protected ReadOnly Property VehicleType() As String
        Get
            Return Common.SafeEncodeString(Request.Params("VehicleType"))
        End Get
    End Property
    Protected ReadOnly Property Make() As String
        Get
            Return Common.SafeEncodeString(Request.Params("Make"))
        End Get
    End Property
    Protected ReadOnly Property Model() As String
        Get
            Return Common.SafeEncodeString(Request.Params("Model"))
        End Get
    End Property
    Protected ReadOnly Property IsNew() As String
		Get
            Return Common.SafeEncodeString(Request.Params("IsNew"))
        End Get
	End Property

	Protected ReadOnly Property Term() As String
		Get
            Return Common.SafeEncodeString(Request.Params("Term"))
        End Get
	End Property

	Protected ReadOnly Property EstPurchasePrice() As String
		Get
            Return Common.SafeEncodeString(Request.Params("EstPurchasePrice"))
        End Get
	End Property

	Protected ReadOnly Property DownPayment() As String
		Get
            Return Common.SafeEncodeString(Request.Params("DownPayment"))
        End Get
	End Property


#End Region

	Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

		'DEBUG
		'CPBLogger.PBLogger.Component = Me.GetType.ToString
		'CPBLogger.PBLogger.Context = Request.UserHostAddress & "/" & _CurrentLenderRef
		'CPBLogger.logInfo("Vehicle Loan: Page loading")
		log.Debug("Vehicle Loan: Page loading")
		Session("StartedDate") = Date.Now	'this is use for keeping track the time when page first load which will be used to compare with submite_date
		'Dim log As log4net.ILog = log4net.LogManager.GetLogger(Me.GetType())
		ucPageHeader._currentConfig = _CurrentWebsiteConfig
		'ucCoUploadDocDialog.CurrentWebsiteConfig = _CurrentWebsiteConfig
		'ucCoUploadDocDialog.UploadLoandocPath = _uploadLoandocPath
		'ucUploadDocDialog.CurrentWebsiteConfig = _CurrentWebsiteConfig
		'ucUploadDocDialog.UploadLoandocPath = _uploadLoandocPath
		''      ''driver license scan 
		driverlicenseScan.CurrentConfig = _CurrentWebsiteConfig
		coAppDriverlicenseScan.CurrentConfig = _CurrentWebsiteConfig
		If Not IsPostBack Then

			''calulate loan amount when there is value via sso
			'If EstPurchasePrice <> "" And DownPayment <> "" Then
			'    Page.ClientScript.RegisterStartupScript(Me.GetType(), "MyKey", "calculateLoanAmount();", True)
			'End If
			'' Find drop down values

			'Dim _VehicleLoanPurposeDropdownParsed As Dictionary(Of String, String) = _CurrentWebsiteConfig.GetEnumItems("VEHICLE_LOAN_PURPOSES")
			If LoanPurpose <> "" Then  'purpose is in the url querystring para
				Dim dic As New Dictionary(Of String, String)
				dic.Add(LoanPurpose, LoanPurpose)
				_VehicleLoanPurposeList = dic
			Else
				_VehicleLoanPurposeList = _CurrentWebsiteConfig.GetEnumItems("VEHICLE_LOAN_PURPOSES")
			End If

			''check LoanPurspose in webconfig exist or not
			If _VehicleLoanPurposeList.Count > 0 Then
				If _VehicleLoanPurposeList.Count = 1 Then ''make default
					Dim ddlPurposeKey(0) As String
					Dim ddlPurposeValue(0) As String
					_VehicleLoanPurposeList.Keys.CopyTo(ddlPurposeKey, 0)
					_VehicleLoanPurposeList.Values.CopyTo(ddlPurposeValue, 0)
					_VehicleLoanPurposeDropdown = Common.RenderDropdownlistWithEmpty(ddlPurposeKey, ddlPurposeValue, "", "--Please Select--", ddlPurposeKey(0))
				Else
					_VehicleLoanPurposeDropdown = Common.RenderDropdownlistWithEmpty(_VehicleLoanPurposeList, "", "--Please Select--")
				End If
				_HasLoanPurpose = "Y"
			Else
				_VehicleLoanPurposeDropdown = Common.RenderDropdownlistWithEmpty(New String() {}, "", "--Please Select--")
				_HasLoanPurpose = "N"
			End If
			''loan term 
			Dim ddlLoanTerm As Dictionary(Of String, String) = _CurrentWebsiteConfig.GetEnumItems("VEHICLE_LOAN_TERM")
			If ddlLoanTerm.Count > 0 Then
				_LoanTermDropdown = Common.RenderDropdownlistWithEmpty(ddlLoanTerm, "", "--Please Select--")
			End If
			_InterestRate = GetInterestRate()

			Dim _VehicleTypesParsed As New Dictionary(Of String, String)
			_VehicleTypesParsed = _CurrentWebsiteConfig.GetEnumItems("VEHICLE_TYPES") ''override from xml config
			If _VehicleTypesParsed.Count = 0 AndAlso Not String.IsNullOrEmpty(_CurrentWebsiteConfig.LenderCode) Then
				Dim downloadVehicleTypes As List(Of DownloadedSettings.CListItem)
				downloadVehicleTypes = CDownloadedSettings.VehicleTypes(_CurrentWebsiteConfig)
				For Each item As CListItem In downloadVehicleTypes
					_VehicleTypesParsed.Add(item.Value, item.Text)
				Next
			End If
			If _VehicleTypesParsed.Count = 1 Then
				Dim ddlVLKey(0) As String
				Dim ddlVLValue(0) As String
				_VehicleTypesParsed.Keys.CopyTo(ddlVLKey, 0)
				_VehicleTypesParsed.Values.CopyTo(ddlVLValue, 0)
				_VehicleTypes = Common.RenderDropdownlistWithEmpty(ddlVLKey, ddlVLValue, "", "--Please Select--", ddlVLKey(0))
			Else
				_VehicleTypes = Common.RenderDropdownlistWithEmpty(_VehicleTypesParsed, "", "--Please Select--")
			End If

			If Common.SafeGUID(Request.QueryString("sid")) <> Guid.Empty Then
				Dim smBL As New SmBL()
				LoanSavingItem = smBL.GetLoanByID(Common.SafeGUID(Request.QueryString("sid")))
			End If
			_CarMakeDropdown = Common.RenderDrodownListFromTextWithEmpty(Server.MapPath(Common.CAR_MAKE_PATH), "", "--Please Select--")
			_MotorCycleMakeDropdown = Common.RenderDrodownListFromTextWithEmpty(Server.MapPath(Common.MOTOR_CYCLE_MAKE_PATH), "", "--Please Select--")
			_RVOrBoatMakeDropdown = Common.RenderDrodownListFromTextWithEmpty(Server.MapPath(Common.RV_OR_BOAT_MAKE_PATH), "", "--Please Select--")
			''get address key
			If Not String.IsNullOrEmpty(_CurrentWebsiteConfig.AddressKey) Then
				_address_key = _CurrentWebsiteConfig.AddressKey
			End If

			Dim employeeOfLenderDropdown As String = ""
			Dim employeeOfLenderType = _CurrentWebsiteConfig.GetEnumItems("EMPLOYEE_OF_LENDER_TYPE")
			If employeeOfLenderType IsNot Nothing AndAlso employeeOfLenderType.Count > 0 Then    'available in  LPQMobileWebsiteConfig
				''get employee of lender type from config
				employeeOfLenderDropdown = Common.RenderDropdownlist(employeeOfLenderType, "")

				ucApplicantInfo.EmployeeOfLenderDropdown = employeeOfLenderDropdown
				ucCoApplicantInfo.EmployeeOfLenderDropdown = employeeOfLenderDropdown

				Dim sLenderName As String = CDownloadedSettings.DownloadLenderServiceConfigInfo(_CurrentWebsiteConfig).LenderName
				ucApplicantInfo.LenderName = sLenderName
				ucCoApplicantInfo.LenderName = sLenderName
			End If

			xaApplicationQuestionLoanPage.Header = HeaderUtils.RenderPageTitle(0, "Please answer the following question(s)", True)
			xaApplicationQuestionReviewPage.Header = HeaderUtils.RenderPageTitle(0, "Please answer question(s) below", True)

			ucApplicantID.MappedShowFieldLoanType = mappedShowFieldLoanType
			ucCoApplicantID.MappedShowFieldLoanType = mappedShowFieldLoanType
			ucApplicantAddress.MappedShowFieldLoanType = mappedShowFieldLoanType
			ucCoApplicantAddress.MappedShowFieldLoanType = mappedShowFieldLoanType
			ucReferenceInfo.MappedShowFieldLoanType = mappedShowFieldLoanType
			ucCoReferenceInfo.MappedShowFieldLoanType = mappedShowFieldLoanType
			ucApplicantInfo.MappedShowFieldLoanType = mappedShowFieldLoanType
            ucCoApplicantInfo.MappedShowFieldLoanType = mappedShowFieldLoanType
            ucAssetInfo.MappedShowFieldLoanType = mappedShowFieldLoanType
            ucCoAssetInfo.MappedShowFieldLoanType = mappedShowFieldLoanType
            ucApplicantInfo.isComboMode = IsComboMode
			ucCoApplicantInfo.isComboMode = IsComboMode
			ucApplicantInfo.InstitutionType = _InstitutionType
			ucCoApplicantInfo.InstitutionType = _InstitutionType

			Dim citizenshipStatusDropdown As String = ""
			Dim hasBlankDefaultCitizenship As Boolean = Common.hasBlankDefaultCitizenShip(_CurrentWebsiteConfig)
			'change the default US Citizen 
			If _CurrentWebsiteConfig.GetEnumItems("CITIZENSHIP_TYPES").Count > 0 Then  'available in  LPQMobileWebsiteConfig
				Dim Citizenships As New Dictionary(Of String, String)
				Citizenships = _CurrentWebsiteConfig.GetEnumItems("CITIZENSHIP_TYPES")
				citizenshipStatusDropdown = Common.DefaultCitizenShipFromConfig(Citizenships, _DefaultCitizenship, _CurrentLenderRef, hasBlankDefaultCitizenship)
			Else
				citizenshipStatusDropdown = Common.DefaultCitizenShip(_DefaultCitizenship, _CurrentLenderRef, hasBlankDefaultCitizenship) ''from hard code
			End If
			' Dim citizenshipStatusDropdown As String = Common.RenderDropdownlist(CEnum.CITIZENSHIP_STATUS, " ", "US CITIZEN")
			ucApplicantInfo.CitizenshipStatusDropdown = citizenshipStatusDropdown
			ucCoApplicantInfo.CitizenshipStatusDropdown = citizenshipStatusDropdown

			ucApplicantInfo.EnableCitizenshipStatus = CheckShowField("divCitizenshipStatus", "", True) Or (IsInMode("777") AndAlso IsInFeature("visibility"))
			ucCoApplicantInfo.EnableCitizenshipStatus = CheckShowField("divCitizenshipStatus", "co_", True) Or (IsInMode("777") AndAlso IsInFeature("visibility"))

			Dim maritalStatusDropdown As String = Common.RenderDropdownlistWithEmpty(CEnum.MARITAL_STATUS, "", "--Please Select--")
			ucApplicantInfo.MaritalStatusDropdown = maritalStatusDropdown
			ucCoApplicantInfo.MaritalStatusDropdown = maritalStatusDropdown
			ucApplicantInfo.EnableMaritalStatus = CheckShowField("divMaritalStatus", "", False) Or (IsInMode("777") AndAlso IsInFeature("visibility"))
			ucCoApplicantInfo.EnableMaritalStatus = CheckShowField("divMaritalStatus", "co_", False) Or (IsInMode("777") AndAlso IsInFeature("visibility"))
            ucApplicantInfo.EnableMemberNumber = CheckShowField("divMemberNumber", "", True) Or (IsInMode("777") AndAlso IsInFeature("visibility"))
            ucCoApplicantInfo.EnableMemberNumber = CheckShowField("divMemberNumber", "co_", True) Or (IsInMode("777") AndAlso IsInFeature("visibility"))
            ucApplicantInfo.EnableMemberShipLength = CheckShowField("divMembershipLength", "", False) Or (IsInMode("777") AndAlso IsInFeature("visibility"))
            ucCoApplicantInfo.EnableMemberShipLength = CheckShowField("divMembershipLength", "co_", False) Or (IsInMode("777") AndAlso IsInFeature("visibility"))
			EnableVinNumber = CheckShowField("divVinNumber", "", False) Or (IsInMode("777") AndAlso IsInFeature("visibility"))

			Dim occupancyStatusesForRequiredHousingPayment = _CurrentWebsiteConfig.GetOccupancyStatusesForRequiredHousingPayment("VEHICLE_LOAN")

			Dim employmentStatusDropdown As String = Common.RenderDropdownlistWithEmpty(CEnum.EMPLOYMENT_STATUS, "", "--Please Select--")
			Dim liveMonthsDropdown As String = Common.GetMonths()
			Dim liveYearsDropdown As String = Common.GetYears()
			ucFinancialInfo.EmploymentStatusDropdown = employmentStatusDropdown
			ucCoFinancialInfo.EmploymentStatusDropdown = employmentStatusDropdown
			ucFinancialInfo.LiveMonthsDropdown = liveMonthsDropdown
			ucCoFinancialInfo.LiveMonthsDropdown = liveMonthsDropdown
			'' add ddlyear
			ucFinancialInfo.LiveYearsDropdown = liveYearsDropdown
			ucCoFinancialInfo.LiveYearsDropdown = liveYearsDropdown
			ucFinancialInfo.OccupancyStatusesForRequiredHousingPayment = occupancyStatusesForRequiredHousingPayment
			ucCoFinancialInfo.OccupancyStatusesForRequiredHousingPayment = occupancyStatusesForRequiredHousingPayment

			Dim occupyingLocationDropdown As String = Common.RenderDropdownlistWithEmpty(CEnum.OCCUPY_LOCATIONS, "", "--Please Select--")
			Dim stateDropdown As String = Common.RenderStateDropdownlistWithEmpty(CEnum.STATES, "", "--Please Select--")
			ucApplicantAddress.OccupyingLocationDropdown = occupyingLocationDropdown
			ucCoApplicantAddress.OccupyingLocationDropdown = occupyingLocationDropdown
			ucApplicantAddress.OccupancyStatusesForRequiredHousingPayment = occupancyStatusesForRequiredHousingPayment
			ucCoApplicantAddress.OccupancyStatusesForRequiredHousingPayment = occupancyStatusesForRequiredHousingPayment
			ucApplicantAddress.StateDropdown = stateDropdown
			ucCoApplicantAddress.StateDropdown = stateDropdown
			ucApplicantID.StateDropdown = stateDropdown
			ucCoApplicantID.StateDropdown = stateDropdown
			ucApplicantAddress.LiveMonthsDropdown = liveMonthsDropdown
			ucCoApplicantAddress.LiveMonthsDropdown = liveMonthsDropdown
			'' add ddlyear
			ucApplicantAddress.LiveYearsDropdown = liveYearsDropdown
			ucCoApplicantAddress.LiveYearsDropdown = liveYearsDropdown
            _previous_address_threshold = _CurrentWebsiteConfig.getPreviousThreshold("VEHICLE_LOAN", "previous_address_threshold")
            _previous_employment_threshold = _CurrentWebsiteConfig.getPreviousThreshold("VEHICLE_LOAN", "previous_employment_threshold")
            'ApplicantID
            If EnableIDSection Or EnableIDSection("co_") Then
				ucApplicantID.LoanType = _LoanType
				ucCoApplicantID.LoanType = _LoanType
				ucApplicantID.isComboMode = IsComboMode
				ucCoApplicantID.isComboMode = IsComboMode
				Dim ddlIDCardType As Dictionary(Of String, String)
				Dim _IDCardTypeDropDown As String
				If _CurrentWebsiteConfig.GetEnumItems("ID_CARD_TYPES").Count = 0 Then
					ddlIDCardType = CFOMQuestion.DownLoadIDCardTypes(_CurrentWebsiteConfig)
					If (ddlIDCardType.Count = 0) Then  'there is no id_card_type from retrieval list
						ucApplicantID.isExistIDCardType = False
						ucCoApplicantID.isExistIDCardType = False
					Else
						'ID_Card_type dropdown, default driver license
						ucApplicantID.isExistIDCardType = True
						ucCoApplicantID.isExistIDCardType = True
						_IDCardTypeDropDown = Common.RenderDropdownlist(ddlIDCardType, " ", "DRIVERS_LICENSE")
						ucApplicantID.IDCardTypeList = _IDCardTypeDropDown
						ucCoApplicantID.IDCardTypeList = _IDCardTypeDropDown
					End If
				Else
					'ID_Card_type dropdown, default driver license
					ucApplicantID.isExistIDCardType = True
					ucCoApplicantID.isExistIDCardType = True
					ddlIDCardType = _CurrentWebsiteConfig.GetEnumItems("ID_CARD_TYPES")
					_IDCardTypeDropDown = Common.RenderDropdownlist(ddlIDCardType, " ", "DRIVERS_LICENSE")
					ucApplicantID.IDCardTypeList = _IDCardTypeDropDown
					ucCoApplicantID.IDCardTypeList = _IDCardTypeDropDown
				End If
			End If ''Applicant ID

			If IsComboMode Then
				'load actual prerequisite product name
				Dim currentProdList As List(Of CProduct) = CProduct.GetProductsForLoans(_CurrentWebsiteConfig)
				If currentProdList Is Nothing OrElse currentProdList.Any() = False Then
					_showProductSelection = False
				Else
					Dim allowedProductCodes As List(Of String) = GetAllowedProductCodes("VEHICLE_LOAN")
					If allowedProductCodes IsNot Nothing AndAlso allowedProductCodes.Any() Then
						currentProdList = currentProdList.Where(Function(p) allowedProductCodes.Contains(p.ProductCode)).ToList()
					End If
					If allowedProductCodes IsNot Nothing AndAlso allowedProductCodes.Count = 1 Then
						'hide product selection section when there is only ONE configured product code - this is the same as the current system.
						_showProductSelection = False
						If currentProdList IsNot Nothing And currentProdList.Any() Then
							Dim cproduct As CProduct = currentProdList.FirstOrDefault(Function(cp As CProduct) cp.ProductCode = allowedProductCodes(0))
							If cproduct IsNot Nothing Then
								_prerequisiteProduct = cproduct
								'fix for case when mindepoist isnot corretly configure on lender side
								If Common.SafeInteger(_prerequisiteProduct.MinimumDeposit) < 0 Then
									_prerequisiteProduct.MinimumDeposit = 0
								End If
								''Don't display product and funding on combo app when there is only ONE product that has no minimum deposit, no services, and no product questions. Do display the product and funding on account that has minimum deposit, services, or product questions.
								If cproduct.MinimumDeposit > 0 OrElse (cproduct.Services IsNot Nothing AndAlso cproduct.Services.Any()) OrElse (cproduct.CustomQuestions IsNot Nothing AndAlso cproduct.CustomQuestions.Any()) Then
									_showProductSelection = True
								End If
							End If
						End If
					End If
				End If
				If _showProductSelection = True Then
					Dim ucProductSelection As Inc_MainApp_ProductSelection = CType(LoadControl("~/Inc/MainApp/ProductSelection.ascx"), Inc_MainApp_ProductSelection)
                    'should show product selection section when there is no configured product code
                    'should show product selection section when there are more than one configured product codes - this is similar to XA where configured product codes will  filter  product to show
                    Dim productRendering As New CProductRendering(currentProdList)
                    Dim currentLenderServiceConfigInfo = CDownloadedSettings.DownloadLenderServiceConfigInfo(_CurrentWebsiteConfig)
                    ucProductSelection.ZipCodePoolList = currentLenderServiceConfigInfo.ZipCodePool
                    ucProductSelection.ZipCodePoolName = currentLenderServiceConfigInfo.ZipCodePoolName
                    ucProductSelection.CurrentLenderRef = _CurrentLenderRef
					ucProductSelection.CurrentProductList = currentProdList
					ucProductSelection.ContentDataTheme = _ContentDataTheme
					ucProductSelection.FooterDataTheme = _FooterDataTheme
					ucProductSelection.HeaderDataTheme = _HeaderDataTheme
					ucProductSelection.HeaderDataTheme2 = _HeaderDataTheme2
					ucProductSelection.WebsiteConfig = _CurrentWebsiteConfig
					ucProductSelection.ProductQuestionsHtml = productRendering.BuildProductQuestions(currentProdList, _textAndroidTel, True)
					plhProductSelection.Controls.Add(ucProductSelection)
				End If
			End If ' IsComboMode

			Try

				Dim fundingSource As CFundingSourceInfo
				If isCheckFundingPage() = True Then	 'funding page is enabled
					fundingSource = CFundingSourceInfo.CurrentJsonFundingPackage(_CurrentWebsiteConfig)
				Else
					fundingSource = New CFundingSourceInfo(New CLenderServiceConfigInfo())
				End If

				'' try to append PAYPAL funding option into _FundingSourcePackageJsonString since its config stay in WEBSITE node
				If fundingSource.ListTypes IsNot Nothing AndAlso Not String.IsNullOrEmpty(_CurrentWebsiteConfig.PayPalEmail) Then
					'' make sure PAYPAL option stay on top(below not funding option)
					'TODO: temporary disable Paypal funding for secured cc due to complex implementation 
					'' fundingSource.ListTypes.Insert(1, "PAYPAL")
				End If
				'' serialize fundingSource into JSON then .aspx can access it in Page_PreRender 
				ccFundingOptions.FundingTypeList = fundingSource.ListTypes
			Catch ex As Exception  'catch connection issue
				logError("Check connection, etc..", ex)
				Return
			End Try
			ucApplicantInfo.ContentDataTheme = _ContentDataTheme
			ucCoApplicantInfo.ContentDataTheme = _ContentDataTheme
			If IsComboMode Then
				ucFomQuestion.FOMQuestions = CFOMQuestion.CurrentFOMQuestions(_CurrentWebsiteConfig)
				ucFomQuestion._CFOMQuestionList = CFOMQuestion.DownloadFomNextQuestions(_CurrentWebsiteConfig)
				ucFomQuestion._lenderRef = _CurrentLenderRef
			Else
				ucFomQuestion.Dispose()
			End If

			'' CC maxfunding
			If Not String.IsNullOrEmpty(_CurrentWebsiteConfig.CCMaxFunding) Then
				_CCMaxFunding = _CurrentWebsiteConfig.CCMaxFunding
				ccFundingOptions.CCMaxFunding = _CCMaxFunding
			End If
			'' ACH maxfunding
			If Not String.IsNullOrEmpty(_CurrentWebsiteConfig.ACHMaxFunding) Then
				_ACHMaxFunding = _CurrentWebsiteConfig.ACHMaxFunding
				ccFundingOptions.ACHMaxFunding = _ACHMaxFunding
			End If
			'' Paypal maxfunding
			If Not String.IsNullOrEmpty(_CurrentWebsiteConfig.PaypalMaxFunding) Then
				_PaypalMaxFunding = _CurrentWebsiteConfig.PaypalMaxFunding
				ccFundingOptions.PaypalMaxFunding = _PaypalMaxFunding
			End If
			_ccStopInvalidRoutingNumber = Common.SafeString(Common.getNodeAttributes(_CurrentWebsiteConfig, "BEHAVIOR", "stop_invalid_routing_number"))
            ccFundingOptions.FundingDisclosure = Common.getFundingDisclosure(_CurrentWebsiteConfig)

			'' Set disclosures
			'Dim disclosures As List(Of String) = _CurrentWebsiteConfig.GetLoanDisclosures("VEHICLE_LOAN")
			'ucDisclosures.Disclosures = disclosures
			' ''verify code
			Dim reasonableDemoCode As String = Common.getNodeAttributes(_CurrentWebsiteConfig, "VEHICLE_LOAN/VISIBLE", "reasonable_demonstration_code")
			ucDisclosures.strRenderingVerifyCode = Common.RenderingVerifyCodeField(reasonableDemoCode, _CurrentWebsiteConfig)

			ucDisclosures.MappedShowFieldLoanType = mappedShowFieldLoanType
			ucDisclosures.EnableEmailMeButton = CheckShowField("divEmailMeButton", "", False) Or (IsInMode("777") AndAlso IsInFeature("visibility"))

			VinBarcodeScanEnabled = Common.getNodeAttributes(_CurrentWebsiteConfig, "VEHICLE_LOAN/VISIBLE", "vin_barcode_scan").ToUpper().Equals("Y")

            ''''' This has to be done in Java.  I'm leaving this here temporarily as reference code.
            ' '' If Vehicle Loan purpose has the word "Refi" or "Buyout", then set the following:
            ' ''		"Estimated Purchase Price" => "Loan Balance"
            'Dim numPurposes As Integer = _VehicleLoanPurposeDropdownParsed.Count
            'For Each purposeKey As String In _VehicleLoanPurposeDropdownParsed.Keys

            '	If purposeKey.ToUpper().Contains("REFI") Or purposeKey.ToUpper().Contains("BUYOUT") Then
            '		purchaseAmountLabel.InnerHtml = "Loan Balance"
            '	End If
            'Next

            '' '' Set branches
            ''Dim branches As Dictionary(Of String, String) = _CurrentWebsiteConfig.GetBranches("HOME_EQUITY_LOAN")
            ' ''_BranchDropdown = Common.RenderDropdownlist(branches, " ")

            ''If branches.Count < 1 Or _CurrentBranchId IsNot Nothing Then
            ''    VLBranchesDiv.Visible = False
            ''End If

            ''If _CurrentBranchId IsNot Nothing Then
            ''    _BranchDropdown = Common.RenderDropdownlist(branches, " ", _CurrentBranchId)
            ''Else
            ''    _BranchDropdown = Common.RenderDropdownlist(branches, " ", _CurrentWebsiteConfig.sDefaultLoanBranchID)
            ''End If
            _branchOptionsStr = GetBranches("VL")
            Dim checkDisAgreePopup As String = Common.checkDisAgreePopup(_CurrentWebsiteConfig)
			If checkDisAgreePopup Then
				_isDisagreePopup = "Y"
			Else
				_isDisagreePopup = "N"
			End If
			_declarationList = Common.GetActiveDeclarationList(_CurrentWebsiteConfig, "VEHICLE_LOAN")
			ucDeclaration.Visible = False
			ucCoDeclaration.Visible = False
			If _declarationList IsNot Nothing AndAlso _declarationList.Count > 0 Then
				ucDeclaration.DeclarationList = _declarationList
				ucCoDeclaration.DeclarationList = _declarationList
				ucDeclaration.Visible = True
				ucCoDeclaration.Visible = True
			End If

			Dim bIsSSO As Boolean = Not String.IsNullOrEmpty(Common.SafeString(Request.Params("FName"))) '
			If Not bIsSSO Then ''hide the scan docs page if SSO via MobileApp
				If _CurrentWebsiteConfig.DLBarcodeScan = "Y" Then
					ucApplicantInfo.LaserScandocAvailable = True
					ucCoApplicantInfo.LaserScandocAvailable = True
					LaserDLScanEnabled = True
				ElseIf Not String.IsNullOrEmpty(_CurrentWebsiteConfig.ScanDocumentKey) Then
					ucApplicantInfo.LegacyScandocAvailable = True
					ucCoApplicantInfo.LegacyScandocAvailable = True
					LegacyDLScanEnabled = True
					_hasScanDocumentKey = _CurrentWebsiteConfig.ScanDocumentKey
				End If
			End If

			'' app_type ='FOREIGN'
			Dim appType As String = _CurrentWebsiteConfig.AppType
			hdForeignAppType.Value = ""	'' initial hidden value foreign
			''Make sure appType is not nothing
			If Not String.IsNullOrEmpty(appType) Then
				If appType.ToUpper() = "FOREIGN" Then
					hdForeignAppType.Value = "Y"
				End If
			End If

			''if hasUploadDocument goto uploadDocument page, otherwise goto final page
			Dim uploadDocsMessage As String = Common.getUploadDocumentMessage(_CurrentWebsiteConfig)
			Dim docTitleOptions As List(Of CDocumentTitleInfo) = Nothing
			Dim docTitleRequired = True
			If Common.getUploadDocumentEnable(_CurrentWebsiteConfig) AndAlso Not String.IsNullOrEmpty(uploadDocsMessage) Then
				docTitleRequired = Not Common.getNodeAttributes(_CurrentWebsiteConfig, "BEHAVIOR", "required_doc_title").Equals("N", StringComparison.OrdinalIgnoreCase)
				Dim clrDocTitle = New List(Of CDocumentTitleInfo)

				' Loan doc titles from Custom List Rules
				If CustomListRuleList IsNot Nothing AndAlso CustomListRuleList.Any(Function(c) c.Code = "DOCUMENT_TITLES") Then
					Dim loanClrDocTitle = EvaluateCustomList(Of CDocumentTitleInfo)("DOCUMENT_TITLES")

					If loanClrDocTitle IsNot Nothing AndAlso loanClrDocTitle.Count > 0 Then
						loanClrDocTitle.ForEach(Sub(docTitle) docTitle.LoanType = "VL")
						clrDocTitle.AddRange(loanClrDocTitle)
					End If
				End If

				' XA doc titles for Combo from Custom List Rules
				If XACustomListRuleList IsNot Nothing AndAlso XACustomListRuleList.Any(Function(c) c.Code = "DOCUMENT_TITLES") Then
					Dim xaClrDocTitle = EvaluateCustomList(Of CDocumentTitleInfo)("DOCUMENT_TITLES", XACustomListRuleList)

					If xaClrDocTitle IsNot Nothing AndAlso xaClrDocTitle.Count > 0 Then
						xaClrDocTitle.ForEach(Sub(docTitle) docTitle.LoanType = "XA")
						clrDocTitle.AddRange(xaClrDocTitle)
					End If
				End If

				If clrDocTitle.Count = 0 Then
					' Fallback to ENUMS doc titles
					docTitleOptions = Common.GetDocumentTitleInfoList(_CurrentWebsiteConfig._webConfigXML.SelectSingleNode("ENUMS/LOAN_DOC_TITLE"))
				Else
					' Fill out options
					docTitleOptions = New List(Of CDocumentTitleInfo) From {New CDocumentTitleInfo() With {.Group = "", .Code = "", .Title = "--Please select title--"}}
					docTitleOptions.AddRange(clrDocTitle.OrderBy(Function(t) t.Title))
				End If

				ucDocUpload.Title = Server.HtmlDecode(uploadDocsMessage)
				ucDocUpload.RequireDocTitle = docTitleRequired
				ucDocUpload.DocTitleOptions = docTitleOptions
				ucDocUpload.DocUploadRequired = _requiredUploadDocs.Equals("Y", StringComparison.OrdinalIgnoreCase)

				ucCoDocUpload.Title = Server.HtmlDecode(uploadDocsMessage)
				ucCoDocUpload.RequireDocTitle = docTitleRequired
				ucCoDocUpload.DocTitleOptions = docTitleOptions
				ucCoDocUpload.DocUploadRequired = _requiredUploadDocs.Equals("Y", StringComparison.OrdinalIgnoreCase)
				ucDocUpload.Visible = True
				ucCoDocUpload.Visible = True
				ucDocUploadSrcSelector.Visible = True
				ucCoDocUploadSrcSelector.Visible = True
			Else
				ucDocUpload.Visible = False
				ucCoDocUpload.Visible = False
				ucDocUploadSrcSelector.Visible = False
				ucCoDocUploadSrcSelector.Visible = False
			End If
			'ucCoUploadDocDialog.Title = _UpLoadDocument
			'ucUploadDocDialog.Title = _UpLoadDocument
			''submit cancel message
			_SubmitCancelMessage = Common.getSubmitCancelMessage(_CurrentWebsiteConfig)
			If Not String.IsNullOrEmpty(_SubmitCancelMessage) Then
				_SubmitCancelMessage = Server.HtmlDecode(_SubmitCancelMessage)	''decode code message 
			End If
			''backgroud theme
			'If String.IsNullOrEmpty(_BackgroundTheme) Then
			'    _bgTheme = _FooterDataTheme
			'Else
			'    _bgTheme = _BackgroundTheme
			'End If
			''enable attribute
			_enableJoint = Not Common.hasEnableJoint(_CurrentWebsiteConfig).ToUpper().Equals("N")
			'hdEnableJoint.Value = Common.hasEnableJoint(_CurrentWebsiteConfig)
			If _enableJoint Then
				_enableJoint = Not Common.getVisibleAttribute(_CurrentWebsiteConfig, "joint_enable_vl").ToUpper.Equals("N")
			End If
			''get auto/pick loan term dropdown enable from config
			_autoTermTextboxEnable = Common.getVisibleAttribute(_CurrentWebsiteConfig, "term_free_form_mode_vl").ToUpper

            '' get loan IDA, don't check for idaMethod if SSO via MobileApp
			If Not bIsSSO OrElse sso_ida_enable Then
				hdIdaMethodType.Value = Common.getIDAType(_CurrentWebsiteConfig, "VEHICLE_LOAN")
			End If

            If IsComboMode Then
				ucFomQuestion.FOMQuestions = CFOMQuestion.CurrentFOMQuestions(_CurrentWebsiteConfig)
				ucFomQuestion._CFOMQuestionList = CFOMQuestion.DownloadFomNextQuestions(_CurrentWebsiteConfig)
				ucFomQuestion._lenderRef = _CurrentLenderRef
				xaApplicantQuestion.nAvailability = "1"
				''FOM_V2
				ucFomQuestion.hasFOM_V2 = CFOMQuestion.hasFOM_V2(_CurrentWebsiteConfig)
			Else
				ucFomQuestion.Dispose()
			End If
			'' get loan purpose category from config
			Dim sNodeName As String = "ENUMS/VEHICLE_LOAN_PURPOSES/ITEM"
			'REDUNDANCE LINES OF CODE. DO NOT NEED ANYMORE
			'Dim oSerializer As New JavaScriptSerializer()
			_LoanPurposeCategory = New JavaScriptSerializer().Serialize(Common.getItemsFromConfig(_CurrentWebsiteConfig, sNodeName, "value", "category"))
			ucApplicantInfo.ContentDataTheme = _ContentDataTheme
			ucCoApplicantInfo.ContentDataTheme = _ContentDataTheme

			'PRIMARY/JOINT/MINOR/ALL
			Select Case _requireMemberNumber.ToUpper()
				Case "PRIMARY"
					ucApplicantInfo.MemberNumberRequired = "Y"
				Case "JOINT"
					ucCoApplicantInfo.MemberNumberRequired = "Y"
				Case "ALL"
					ucApplicantInfo.MemberNumberRequired = "Y"
					ucCoApplicantInfo.MemberNumberRequired = "Y"
				Case Else
					ucApplicantInfo.MemberNumberRequired = "N"
					ucCoApplicantInfo.MemberNumberRequired = "N"
			End Select
			ucApplicantAddress.CollectDescriptionIfOccupancyStatusIsOther = CollectDescriptionIfOccupancyStatusIsOther
			ucCoApplicantAddress.CollectDescriptionIfOccupancyStatusIsOther = CollectDescriptionIfOccupancyStatusIsOther
			ucApplicantAddress.EnableMailingAddress = EnableMailingAddressSection()
			ucCoApplicantAddress.EnableMailingAddress = EnableMailingAddressSection("co_")
			ucFinancialInfo.CollectDescriptionIfEmploymentStatusIsOther = CollectDescriptionIfEmploymentStatusIsOther
            ucCoFinancialInfo.CollectDescriptionIfEmploymentStatusIsOther = CollectDescriptionIfEmploymentStatusIsOther
            ucAssetInfo.EnableAssetSection = EnableAssetSection()
            ucCoAssetInfo.EnableAssetSection = EnableAssetSection()
            ucAssetInfo.AssetTypesString = GetAssetTypes("")
            ucCoAssetInfo.AssetTypesString = GetAssetTypes("")

            If Is2ndLoanApp Then
				_SID = Common.SafeString(Request.Params("sid"))
			End If

            ''prefill data on page 2 if reffered from XA
            If Is2ndLoanApp AndAlso String.IsNullOrEmpty(Common.SafeString(Request.QueryString("sid"))) = False Then
                Dim sectionID As String = Common.SafeString(Request.QueryString("sid"))
                Dim params = CType(Session(sectionID), NameValueCollection)
                Dim sXAXSellComment As String = Common.SafeString(Session("xaXSell" & sectionID))
                ''xaXSellComment to check it come from XA App
                If sXAXSellComment.Contains("CROSS SELL FROM XA") Then  ''If Common.SafeDouble(params("TotalMonthlyHousingExpense")) = 0 Then
                    Dim hasCoApp = Convert.ToBoolean(params("IsJoint"))
                    If Session(sectionID) IsNot Nothing Then
                        _prefillData = Common.prefilledData(False, params)
                        If hasCoApp Then
                            _coPrefillData = Common.prefilledData(True, params)
                        End If
                    End If
                    _xaXSellComment = sXAXSellComment
                    Is2ndLoanApp = False
                End If
			End If
			plhInstaTouchStuff.Visible = False
            If _CurrentWebsiteConfig.InstaTouchEnabled AndAlso Not bIsSSO And Not Is2ndLoanApp Then
                plhInstaTouchStuff.Visible = True
                Dim instaTouchStuffCtrl As Inc_MainApp_InstaTouchStuff = CType(LoadControl("~/Inc/MainApp/InstaTouchStuff.ascx"), Inc_MainApp_InstaTouchStuff)
                instaTouchStuffCtrl.PersonalInfoPage = "vl2"
                instaTouchStuffCtrl.LoanType = "VL"
                instaTouchStuffCtrl.LenderName = _CurrentWebsiteConfig.LenderName
                instaTouchStuffCtrl.LenderID = _CurrentWebsiteConfig.LenderId
                instaTouchStuffCtrl.LenderRef = _CurrentWebsiteConfig.LenderRef
                instaTouchStuffCtrl.InstaTouchMerchantId = _CurrentWebsiteConfig.InstaTouchMerchantId
                plhInstaTouchStuff.Controls.Add(instaTouchStuffCtrl)
            End If

            Dim demographicDescription = _CurrentWebsiteConfig.GetDemographicDescription("HOME_EQUITY_LOAN")
			Dim demographicDisclosure = _CurrentWebsiteConfig.GetDemographicDisclosure("HOME_EQUITY_LOAN")
			ucApplicantGMI.DemographicDescription = demographicDescription
			ucApplicantGMI.DemographicDisclosure = demographicDisclosure
			ucCoApplicantGMI.DemographicDescription = demographicDescription
			ucCoApplicantGMI.DemographicDisclosure = demographicDisclosure

		End If 'end IsPostBack

		'asign config theme for PageHeader UC, avoid reading config file unnescessary
		ucPageHeader._headerDataTheme = _HeaderDataTheme
		ucPageHeader._footerDataTheme = _FooterDataTheme
		ucPageHeader._contentDataTheme = _ContentDataTheme
		ucPageHeader._headerDataTheme2 = _HeaderDataTheme2
		ucPageHeader._backgroundDataTheme = _BackgroundTheme
		ucPageHeader._buttonDataTheme = _ButtonTheme
		ucPageHeader._buttonFontColor = _ButtonFontTheme

		plhApplicationDeclined.Visible = False
		If CustomListRuleList IsNot Nothing AndAlso CustomListRuleList.Any(Function(p) p.Code = "APPLICANT_BLOCK_RULE") Then
			plhApplicationDeclined.Visible = True
			Dim applicationBlockRulesPage As Inc_MainApp_ApplicationBlockRules = CType(LoadControl("~/Inc/MainApp/ApplicationBlockRules.ascx"), Inc_MainApp_ApplicationBlockRules)
			applicationBlockRulesPage.CustomListItem = CustomListRuleList.First(Function(p) p.Code = "APPLICANT_BLOCK_RULE")
			plhApplicationDeclined.Controls.Add(applicationBlockRulesPage)
		End If
		plhApplicantAdditionalInfo.Visible = False
		If NSSList IsNot Nothing AndAlso NSSList.Count > 0 Then
			plhApplicantAdditionalInfo.Visible = True
			Dim applicantAdditionalInfoPage As Inc_MainApp_ApplicantAdditionalInfo = CType(LoadControl("~/Inc/MainApp/ApplicantAdditionalInfo.ascx"), Inc_MainApp_ApplicantAdditionalInfo)
			applicantAdditionalInfoPage.LogoUrl = _LogoUrl
			applicantAdditionalInfoPage.PreviousPage = "vl2"
			applicantAdditionalInfoPage.PreviousCoAppPage = "vl6"
			applicantAdditionalInfoPage.NextPage = "pageSubmit"
			applicantAdditionalInfoPage.NSSList = NSSList
			applicantAdditionalInfoPage.MappedShowFieldLoanType = mappedShowFieldLoanType
			applicantAdditionalInfoPage.EnablePrimaryAppSpouseContactInfo = EnablePrimaryAppSpouseContactInfo
			applicantAdditionalInfoPage.EnableCoAppSpouseContactInfo = EnableCoAppSpouseContactInfo

			plhApplicantAdditionalInfo.Controls.Add(applicantAdditionalInfoPage)
		End If
		''credit pull message in disclosure section
		Dim oNode As XmlNode = _CurrentWebsiteConfig._webConfigXML.SelectSingleNode("CREDIT_PULL_MESSAGE")
		If oNode IsNot Nothing Then
			_CreditPullMessage = oNode.InnerText
		End If
		_vlLocationPool = Common.getNodeAttributes(_CurrentWebsiteConfig, "VEHICLE_LOAN/VISIBLE", "location_pool") = "Y"
		If _vlLocationPool Then
			Dim currentLenderServiceConfigInfo = CDownloadedSettings.DownloadLenderServiceConfigInfo(_CurrentWebsiteConfig)
			_zipCodePoolList = currentLenderServiceConfigInfo.ZipCodePool
			_zipCodePoolNames = currentLenderServiceConfigInfo.ZipCodePoolName
			_vlZipCodePoolProducts = CDownloadedSettings.VehicleLoanProducts(_CurrentWebsiteConfig).Where(Function(x) x.IsActive = "Y").ToList()
		End If
	End Sub

	Private Function isCheckFundingPage() As Boolean
		Dim result As Boolean = True
		Dim oNodes As XmlNodeList = _CurrentWebsiteConfig._webConfigXML.SelectNodes("VEHICLE_LOAN/VISIBLE")
		If oNodes.Count < 1 Then
			Return True
		End If

		For Each child As XmlNode In oNodes
			If child.Attributes("funding") Is Nothing Then Continue For
			If Common.SafeString(child.Attributes("funding").InnerText) = "N" Then
				result = False
			End If
		Next

		Return result
	End Function
End Class
