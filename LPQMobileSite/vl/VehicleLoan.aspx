﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="VehicleLoan.aspx.vb" Inherits="vl_VehicleLoan"
    EnableViewState="false" %>

<%@ Register Src="~/Inc/PageHeader.ascx" TagPrefix="uc" TagName="pageHeader" %>
<%@ Register Src="~/Inc/Disclosure.ascx" TagPrefix="uc" TagName="disclosure" %>
<%@ Register Src="~/Inc/MainApp/ApplicantInfo.ascx" TagPrefix="uc" TagName="applicantInfo" %>
<%@ Register Src="~/Inc/MainApp/xaApplicantAddress.ascx" TagPrefix="uc" TagName="applicantAddress" %>
<%@ Register Src="~/Inc/MainApp/xaApplicantContactInfo.ascx" TagPrefix="uc" TagName="ccContactInfo" %>
<%@ Register Src="~/Inc/MainApp/ApplicantGMI.ascx" TagPrefix="uc" TagName="applicantGMI" %>
<%@ Register Src="~/Inc/MainApp/xaApplicantQuestion.ascx" TagPrefix="uc" TagName="xaApplicantQuestion" %>
<%@ Register Src="~/Inc/MainApp/FinancialInfo.ascx" TagPrefix="uc" TagName="financialInfo" %>
<%--<%@ Register Src="~/Inc/MainApp/UploadDocDialog.ascx" TagPrefix="uc" TagName="ucUploadDocDialog" %>--%>
<%@ Register Src="~/Inc/NewDocCapture.ascx" TagPrefix="uc" TagName="DocUpload" %>
<%@ Register Src="~/Inc/DocCaptureSourceSelector.ascx" TagPrefix="uc" TagName="DocUploadSrcSelector" %>
<%@ Register Src="~/Inc/MainApp/xaApplicantID.ascx" TagPrefix="uc" TagName="ucApplicantID" %>
<%@ Register Src="~/Inc/NewDocumentScan.ascx" TagPrefix="uc" TagName="driverLicenseScan" %>
<%@ Register Src="~/Inc/LaserDocumentScan.ascx" TagPrefix="uc" TagName="LaserDriverLicenseScan" %>
<%@ Register TagPrefix="uc1" TagName="Piwik" Src="~/Inc/Piwik/PiwikTracking.ascx" %>
<%@ Register Src="~/Inc/MainApp/xaFOMQuestion.ascx" TagPrefix="uc" TagName="ucFOMQuestion" %>
<%@ Register TagPrefix="uc" TagName="pageFooter" Src="~/Inc/PageFooter.ascx" %>
<%@ Reference Control="~/Inc/MainApp/ProductSelection.ascx" %>
<%@ Register Src="~/Inc/MainApp/FundingOptions.ascx" TagPrefix="uc" TagName="ccFundingOptions" %>
<%@ Register Src="~/Inc/MainApp/CunaProtectionInfo.ascx" TagPrefix="uc" TagName="ProtectionInfo" %>
<%@ Register Src="~/Inc/MainApp/ReferenceInfo.ascx" TagPrefix="uc" TagName="ReferenceInfo" %>
<%@ Register Src="~/Inc/VinNumberScan.ascx" TagPrefix="uc" TagName="VimNumberScan" %>
<%@ Register Src="~/Inc/MainApp/Asset.ascx" TagPrefix="uc" TagName="Asset" %>
<%@ Register Src="~/Inc/Declaration.ascx" TagPrefix="uc" TagName="declaration" %>
<%@ Reference Control="~/Inc/MainApp/ApplicationBlockRules.ascx" %>
<%@ Reference Control="~/Inc/MainApp/InstaTouchStuff.ascx" %>
<%@ Reference Control="~/Inc/MainApp/ApplicantAdditionalInfo.ascx" %>
<%@ Import Namespace="System.Globalization" %>
<%@ Import Namespace="Newtonsoft.Json" %>
<%@ Import Namespace="LPQMobile.Utils" %>
<%@ Import Namespace="System.Web.Optimization" %>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">
<head runat="server">
    <title>LPQ Mobile</title>
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <%:Styles.Render("~/css/thirdparty/bootstrap") %>
    <uc:pageHeader ID="ucPageHeader" ScriptFolder="vl" runat="server" />
	<%:Styles.Render("~/css/thirdparty/custom") %>
	<script type="text/javascript">
		//prepo, required for dropdown
	    $(document).ready(function () {
	        VL.FACTORY.assignLoanPurpose('<%=LoanPurpose%>');
	        VL.FACTORY.assignVehicleType('<%=VehicleType%>');
            VL.FACTORY.assignTerm('<%=Term%>');
			<%--if ('<%=LoanPurpose%>' != "" && '<%=LoanPurpose%>' != null) { // need to make sure the value is not empty or not null 
			    $('#ddlLoanPurpose').val('<%=LoanPurpose.ToUpper()%>');
			}
			if ($('#ddlLoanPurpose').data("mobile-selectmenu") === undefined) {
				$('#ddlLoanPurpose').selectmenu(); //not initialized yet, lets do it
			}
			$('#ddlLoanPurpose').selectmenu('refresh');--%>

			<%--//credit card name
			if ('<%=VehicleType%>' != "" && '<%=VehicleType%>' != null) { // need to make sure the value is not empty or not null 
				$('#ddlVehicleType').val('<%=VehicleType%>');
	    	}
			if ($('#ddlVehicleType').data("mobile-selectmenu") === undefined) {
				$('#ddlVehicleType').selectmenu(); //not initialized yet, lets do it
			}
			$('#ddlVehicleType').selectmenu('refresh');--%>

	    });
	    var LoanPurposeCategory = '<%=_LoanPurposeCategory%>';
	    var OCCUPATIONLIST = '<%=_occupationList%>';
	    var LOANPURPOSELIST = <%=JsonConvert.SerializeObject(_VehicleLoanPurposeList)%>;   
        var PREFILLDATA = null;
	    var COPREFILLDATA=null; 
    	<%If _prefillData IsNot Nothing AndAlso _prefillData.Any() Then%>
	    PREFILLDATA = <%=JsonConvert.SerializeObject(_prefillData)%>;
	    <%End If%>

        <%If _coPrefillData IsNot Nothing AndAlso _coPrefillData.Any() Then%>
	    COPREFILLDATA = <%=JsonConvert.SerializeObject(_coPrefillData)%>;
	    <%End If%>
        var CUSTOMLISTFOMFEES = <%=JsonConvert.SerializeObject(_customListFomFees)%>;
        var VLPARAINFO = {
            isNew: '<%=IsNew %>',
            make: '<%=Make%>',
            model: '<%=Model%>'
        };
        var VLPRODUCTS = {};
        VLPRODUCTS.ZipPoolNames =<%=JsonConvert.SerializeObject(_zipCodePoolNames)%>;
        VLPRODUCTS.ZipPoolProducts = <%=JsonConvert.SerializeObject(_vlZipCodePoolProducts)%>;
        VLPRODUCTS.ZipPoolIds =<%=JsonConvert.SerializeObject(_zipCodePoolList)%>;  
	</script>

</head>
<body class = "lpq_container no-funding" >
    <uc1:Piwik id="ucPiwik" runat="server" ></uc1:Piwik>
	<input type="hidden" id="hdPlatformSource" value="<%=PlatformSource%>" />
  	<input type="hidden" id="hfLenderRef" value='<%=_CurrentLenderRef%>' />
    <input type="hidden" id="hfBranchId" value='<%=_CurrentBranchId%>' />
    <input type="hidden" id="hdXAComboBranchId" value='<%=_XAComboBranchId%>' />
	<input type="hidden" id="hfLoanOfficerID" value='<%=_CurrentLoanOfficerId%>' />
	<input type="hidden" id="hfReferralSource" value='<%=_ReferralSource%>' />
    <input type ="hidden" id ="hdAddressKey" value ='<%= _address_key%>' />
    <input type="hidden" runat="server" id="hdForeignAppType" />
    <input type ="hidden" id="hdScanDocumentKey" value ="<%=_hasScanDocumentKey %>" />
   <%-- <input type ="hidden" runat="server" id="hdLenderRef" />
    <input type ="hidden" runat ="server" id="hdLenderId" />--%>
    <%--<input type="hidden" id ="hdFooterTheme" value ="<%=_bgTheme %>" />
    <input type ="hidden" id ="hdBgTheme" value="<%=_BackgroundTheme%>" />--%>
    <%--<input type="hidden" runat ="server" id="hdEnableJoint" />--%>
    <input type="hidden" id="hdForeignContact" runat ="server"  />
    <input type="hidden" id="hdRedirectURL" value="<%=_RedirectURL%>" />
    <input type ="hidden" id="hdIdaMethodType" runat ="server" />
    <input type="hidden" runat="server" id="hdNumWalletQuestions" />
    <input type ="hidden" id="hdRequiredDLScan" value="<%=_requiredDLScan%>" />
    <input type="hidden" id="hdPrevEmploymentThreshold" value="<%=_previous_employment_threshold%>" />
    <input type="hidden" id="hdPrevAddressThreshold" value='<%= _previous_address_threshold%>' />
	<input type="hidden" id="hdEnableIDSection" value="<%=IIf(EnableIDSection, "Y", "N") %>"/>
	<input type="hidden" id="hdEnableIDSectionForCoApp" value="<%=IIf(EnableIDSection("co_"), "Y", "N") %>"/>
	
	<input type="hidden" id="hdEnableReferenceInformation" value="<%=IIf(CheckShowField("divReferenceInformation", "", True) Or (IsInMode("777") AndAlso IsInFeature("visibility")), "Y", "N") %>"/>
	<input type="hidden" id="hdEnableReferenceInformationForCoApp" value="<%=IIf(CheckShowField("divReferenceInformation", "co_", True) Or (IsInMode("777") AndAlso IsInFeature("visibility")), "Y", "N") %>"/>
    <input type="hidden" id="hdIsComboMode" value="<%=IIf(IsComboMode, "Y", "N")%>"/>
    <input type="hidden" id="hdProceedMembership" value=""/>
	<input type="hidden" id="hdEmploymentDurationRequired"  value="<%=_isEmploymentDurationRequired%>" />
	<input type="hidden" id="hdHomePhoneRequired"  value="<%=_isHomephoneRequired%>" />
    <input type="hidden" id="hdEnableBranchSection" value="<%=IIf(EnableBranchSection AndAlso Not String.IsNullOrEmpty(_branchOptionsStr), "Y", "N")%>"/>
    <input type="hidden" id="hdHasLoanPurpose" value="<%=IIf(_VehicleLoanPurposeList.Any(), "Y", "N") %>" />
    <input type="hidden" id="hdVlHasCoApplicant" value="N"/>
    <input type="hidden" id="hdVlIsNewVehicle" value=""/>
    <input type="hidden" id="hdVlKnowVehicleMake" value=""/>
    <input type="hidden" id="hdLoanPurpose" value=""/>
    <input type="hidden" id="hdRequiredBranch" value="<%=_requiredBranch%>" />
    <input type="hidden" id="hdCCMaxFunding" value='<%=_CCMaxFunding%>' />
	<input type="hidden" id="hdACHMaxFunding" value='<%=_ACHMaxFunding%>' />
	<input type="hidden" id="hdPaypalMaxFunding" value='<%=_PaypalMaxFunding%>' />
	<input type="hidden" id="hdStopInvalidRoutingNumber" value="<%=_ccStopInvalidRoutingNumber%>" />
	<input type="hidden" id="hdIs2ndLoanApp" value="<%=IIf(Is2ndLoanApp, "Y", "N")%>" />
	<input type="hidden" id="hdSID" value="<%=_SID%>" />
    <input type="hidden" id="hdXACrossSellComment" value="<%=_xaXSellComment%>" />
    <input type="hidden" id="hdIsCQNewAPI" value="<%=IIf(_isCQNewAPI, "Y", "N")%>" />
    <input type="hidden" id="hdEnableDisagree" value="<%=IIf(Common.checkDisAgreePopup(_CurrentWebsiteConfig), "Y", "N")%>" />
    <input type="hidden" id="hdMembershipFee" value='<%=_MembershipFee%>' />
	<input type="hidden" id="hdVendorID" value='<%=Common.SafeStripHtmlString(Request.QueryString("vendorid"))%>' />
	<input type="hidden" id="hdUserID" value='<%=Common.SafeStripHtmlString(Request.QueryString("userid"))%>' />
    <input type="hidden" id="hdAutoTermTextboxEnable" value="<%=_autoTermTextboxEnable%>" />
	<%If LoanSavingItem IsNot Nothing Then%>
	<input type="hidden" id="hdLoanSavingID" value='<%=Common.SafeEncodeString(LoanSavingItem.LoanSavingID)%>' />
	<%End If %>
    <input type="hidden" id="hdHasZipCodePool" value="<%=IIf(_locationPool, "Y", "N") %>" />
    <input type="hidden" id="hdHasVLZipCodePool" value="<%=IIf(_vlLocationPool, "Y", "N") %>" />
    <input type="hidden" id="hdHasSSOIDA"value="<%=IIf(IsSSO And sso_ida_enable, "Y", "N") %>" />
    <div>
    <!--getting started(vl1): vehicle infor +scandoc + disclosure -->
    <div data-role="page" id="vl1">
        <div data-role="header" class="sr-only">
            <h1>Vehicle Loan Information</h1>
        </div>
        <div data-role="content" style="overflow-x: visible;">
            <%=HeaderUtils.RenderLogoAndSteps(_LogoUrl, 0, "APPLY_CC")%>
            <%=HeaderUtils.RenderPageTitle(10, "Vehicle Loan", False)%>
                <%If IsComboMode And _locationPool Then%> 
                   <%If Not _showProductSelection Then %>
                        <%=HeaderUtils.RenderPageTitle(0, "No Product Available", True)%>		    
		                <%=_noAvailableProductMessage %>
                   <%else %> <!-- display the zip code field -->
                          <div id="divLocationPool">					       
					        <div data-role="fieldcontain">
                                <label for="txtLocationPoolCode"> <%=HeaderUtils.RenderPageTitle(0, "Enter your ZIP Code", True, isRequired:=True) %></label>
						        <input type="<%=_textAndroidTel%>" id="txtLocationPoolCode" pattern="[0-9]*" class="inzipcode" maxlength="5"/>
					        </div>	
					        <div class="hidden" id="divNoAvailableProducts">
						        <%=HeaderUtils.RenderPageTitle(0, "No Available Products", True)%>
						        <%=_noAvailableProductMessage %>
					        </div>
				          </div>
                   <%End if%>
                <%Else If _vlLocationPool  %> <!--location pool for standard loan-->
		            <div id="vl_divLocationPool">				
					    <div data-role="fieldcontain">
                            <label for="vl_txtLocationPoolCode"> <%=HeaderUtils.RenderPageTitle(0, "Enter your ZIP Code", True, isRequired:=True) %></label>
						    <input type="<%=_textAndroidTel%>" id="vl_txtLocationPoolCode" pattern="[0-9]*" class="inzipcode" maxlength="5"/>
					    </div>	
					    <div class="hidden" id="vl_divNoAvailableProducts">
						    <%=HeaderUtils.RenderPageTitle(0, "No Available Products", True)%>
						    <%=_noAvailableProductMessage %>
					    </div>
				    </div>
		        <%End If%>
               <div id="divVehicleLoanInfo" <%=IIf((IsComboMode And _locationPool) Or _vlLocationPool, " class='hidden'", "") %> > <!--hide all the fields of loan info if it is combo mode and location pool is on-->
                    <%--Branch location --%>
                    <%If EnableBranchSection AndAlso Not String.IsNullOrEmpty(_branchOptionsStr) Then%>
				        <div id="divBranchSection" <%=IIf(IsInMode("777") AndAlso IsInFeature("visibility"), "class=""showfield-section"" data-show-field-section-id=""" & BuildShowFieldSectionID("divBranchSection") & """ data-default-state=""off"" data-section-name= 'Branch'", "")%>>
				         <div data-role="fieldcontain">
                             <%If _requiredBranch <> "N" Then%>
                                 <%=HeaderUtils.RenderPageTitle(0, "Select branch location", True, "divTitleBranchName", isRequired:=True)%>
                             <%Else%>
                                 <%=HeaderUtils.RenderPageTitle(0, "Select branch location", True, "divTitleBranchName")%> 
                             <%End If%>
                             <div id='divBranchName' class='header-theme-dropdown-btn'>
                                <select id="ddlBranchName" aria-labelledby="divTitleBranchName"><%=_branchOptionsStr%></select> 
                            </div>
                        </div> 
                    </div>
			        <%End If %>
                    <div id="divLoanPurpose" style="display:<%=IIf(_VehicleLoanPurposeList.Any(), "block", "none")%>;">
				          <br />
				          <%=HeaderUtils.RenderPageTitle(0, "Select a purpose", True, isRequired:= true)%>
                       <%--<div data-role="fieldcontain">
                            <label for="ddlLoanPurpose">Purpose<span class="require-span">*</span></label>
                            <select id="ddlLoanPurpose">
                                <%=_VehicleLoanPurposeDropdown%>
                            </select>
                        </div>--%>
                        <%For Each item As KeyValuePair(Of String, String) In _VehicleLoanPurposeList%>
                        <a href="#" data-role="button" class="btn-header-theme" data-command="loan-purpose" data-purpose-text="<%=item.Value%>" data-key="<%=item.Key %>" style="text-align: center; padding-left: 0;"><%=item.Value %></a>
                        <%Next %>
                    </div>

		           <br />
		           <%=HeaderUtils.RenderPageTitle(0, "Provide loan information", True)%>
			        <div data-role="fieldcontain">
                        <label for="ddlVehicleType" class="RequiredIcon">Vehicle Type</label>
                        <select id="ddlVehicleType">
                            <%=_VehicleTypes%>
                        </select>
                    </div>
			        <div data-role="fieldcontain" id="divNewOrUsedVehicleSection">
                        <label class="rename-able RequiredIcon">Is this a new or used vehicle?</label>
                        <div class="row" id="divNewOrUsedVehicle">
	                        <div class="ui-checkbox" style="margin:0px 1% 0px 0px;padding-left: 0px;padding-right: 0px;float: left;  width:49%;">
						        <label for="chkVehicleNew" class="ui-btn ui-corner-all ui-btn-inherit ui-btn-icon-left ui-checkbox-off" style="padding: 10px 14px 10px 40px;">New</label>
		                        <input type="checkbox" name="checkbox-enhanced" id="chkVehicleNew" data-enhanced="true"/>
					        </div>
	                        <div class="ui-checkbox" style="margin:0px;padding-left: 0px;padding-right: 0px;float: left; width:50%; ">
						        <label for="chkVehicleUsed" class="ui-btn ui-corner-all ui-btn-inherit ui-btn-icon-left ui-checkbox-off" style="padding: 10px 14px 10px 40px;">Used</label>
		                        <input type="checkbox" name="checkbox-enhanced" id="chkVehicleUsed" data-enhanced="true"/>
					        </div>
                        </div>
                    </div>
			
                    <div data-role="fieldcontain" id="divKnowMakeAndModelSection">
                        <label class="RequiredIcon rename-able">Do you know the Make and Model?</label>
                        <div class="row" id="divKnowMakeAndModel" >
	                        <div class="ui-checkbox" style="margin:0px 1% 0px 0px;padding-left: 0px;padding-right: 0px;float: left; width:49%;">
						        <label for="chkKnowMake" class="ui-btn ui-corner-all ui-btn-inherit ui-btn-icon-left ui-checkbox-off abort-renameable" style="padding: 10px 14px 10px 40px;">Yes</label>
		                        <input type="checkbox" name="checkbox-enhanced" id="chkKnowMake" data-enhanced="true"/>
					        </div>
	                        <div class="ui-checkbox" style="margin:0px;padding-left: 0px;padding-right: 0px;float: left; width:50%;">
						        <label for="chkDontKnowMake" class="ui-btn ui-corner-all ui-btn-inherit ui-btn-icon-left ui-checkbox-off abort-renameable" style="padding: 10px 14px 10px 40px;">No</label>
		                        <input type="checkbox" name="checkbox-enhanced" id="chkDontKnowMake" data-enhanced="true"/>
					        </div>
                        </div>
                    </div>
			        <div id="divVehicleDetail" style="display: none;">
				        <%If EnableVinNumber Then%>
				        <div id="divVinNumber" <%=IIf(IsInMode("777") AndAlso IsInFeature("visibility"), "class=""showfield-section"" data-show-field-section-id=""" & BuildShowFieldSectionID("divVinNumber") & """ data-default-state=""off"" data-section-name= 'Vin Number'", " ")%>>
					        <div data-role="fieldcontain">
						        <label for="txtVinNumber">VIN<span class="loader hidden"></span><a href="#vinscan" data-rel="dialog" data-transition="pop" class="pull-right <%=IIf(VinBarcodeScanEnabled, "", "hidden") %>" style="text-align: right;"><img src="/images/barcode-scan.png" style="width: 25%;" class="img-responsive"/></a></label>
						        <input type="text" id="txtVinNumber" maxlength="17" />
					        </div>
					        <%If VinBarcodeScanEnabled Then%>
					        <div class="vin-scan-guide header_theme2">Scan VIN barcode or tap the textbox to type in a VIN</div>
					        <%End If%>
					        <div class="vin-scan-msg hidden"></div>
				        </div>
				        <%End If%>
                        <div id="divVehicleMake">
                            <div id="divCarMake" data-role="fieldcontain">
                                <label for="ddlCarMake" class="RequiredIcon">Vehicle Make</label>
                                <select id="ddlCarMake">
                                    <%=_CarMakeDropdown%>
                                </select>
                            </div>
                    
                            <div id="divMotorCycleMake" data-role="fieldcontain">
                                <label for="ddlMotorCycleMake" class="RequiredIcon">Vehicle Make</label>
                                <select id="ddlMotorCycleMake">
                                    <%=_MotorCycleMakeDropdown%>
                                </select>
                            </div>
                             <div id="divMake" data-role="fieldcontain">
                                 <label for="txtMake" class="RequiredIcon">Vehicle Make</label>
						        <input type="text" id="txtMake" maxlength="50" />
                              </div>
                        </div>
                        <div data-role="fieldcontain">
                            <label for="txtVehicleModel" class="RequiredIcon">Vehicle Model</label>
                            <input type="text" id="txtVehicleModel" maxlength="50" />
                        </div>
                        <div data-role="fieldcontain">
                            <label for="txtVehicleYear" class="RequiredIcon">Vehicle Year</label>
                            <input type="tel" id="txtVehicleYear" class="numeric" maxlength="4" />
                        </div>
                    </div>
			        <div id="divVehicleMileage" style="display: none;">
                        <div data-role="fieldcontain">
                            <label for="txtVehicleMileage">Vehicle Mileage</label>
                            <input type="tel" id="txtVehicleMileage" class="numeric" maxlength="7" />
                        </div>
                    </div>
            
                    <div data-role="fieldcontain">
                        <label for="txtEstimatedVehicleValue">
                            <div id="divPurchaseAmountLabel" runat="server">
                            Estimated Purchase Price/Vehicle Value<span class="require-span">*</span></div>
                        </label>
                        <input type="<%=_textAndroidTel%>" pattern ="[0-9]*" id="txtEstimatedVehicleValue" class="money" maxlength="12" value="<%=EstPurchasePrice%>" />
                    </div>
                    <div id="divDownPayment">
                    <div data-role="fieldcontain">
                        <label for="txtDownPayment" class="RequiredIcon">Down Payment Amount</label>
                        <input type="<%=_textAndroidTel%>" pattern ="[0-9]*" id="txtDownPayment" class="money" maxlength="12" value="<%=DownPayment%>" />
                    </div>
                    </div>
                    <div data-role="fieldcontain">
                        <label for="txtProposedLoanAmount" class="RequiredIcon abort-renameable">Requested Loan Amount</label>
                        <input type="<%=_textAndroidTel%>" pattern ="[0-9]*" id="txtProposedLoanAmount" readonly="readonly" class="money" maxlength="12" />
                    </div>
                    <div data-role="fieldcontain" >
                        <label for="ddlLoanTerm" class="RequiredIcon">Loan Term (months)</label>
                         <select id="ddlLoanTerm">
                            <%=_LoanTermDropdown%>
                            <%If String.IsNullOrEmpty(_LoanTermDropdown) Then%>
                                <option value="0">--Please Select--</option>
                                <option value="12">12</option>  
                                <option value="24">24</option>    
                                <option value="36">36</option>    
                                <option value="48">48</option>    
                                <option value="60">60</option>    
                                <option value="72">72</option>  
                            <%End If%>    
                        </select>
                    </div>
                    <div data-role="fieldcontain" style="display:none">
                         <label for="txtLoanTerm" class="RequiredIcon">Loan Term (months)</label>
                        <input type="tel" id="txtLoanTerm" class ="numeric" maxlength="3"/>
                    </div>
			        <%If CheckShowField("divEstimatePayment", "", False) Or (IsInMode("777") AndAlso IsInFeature("visibility")) Then%>
			        <div id="divEstimatePayment" <%=IIf(IsInMode("777") AndAlso IsInFeature("visibility"), "class=""showfield-section"" data-show-field-section-id=""" & BuildShowFieldSectionID("divEstimatePayment") & """ data-default-state=""off"" data-section-name=""Estimated Payment""","") %>>
				        <div>
					        <div>
						        <span>Estimated payment: </span>						
					        </div>
					        <div class="est-payment-value_container">
						        <span class="est-payment-value">$0.00</span>/mo
						        <span class="calculator-btn btn-header-theme" onclick='openPopup("#popEstimatePaymentCalculator")' data-rel='popup' data-transition='pop'><em class="fa fa-calculator" aria-hidden="true"></em></span>
					        </div>					
				        </div>
				        <div>
					        <span class="rename-able">Estimated payment based on</span><span id="spInterestRate" data-value="<%=_InterestRate %>"> <%=_interestRate.ToString("N2", New CultureInfo("en-US")) %>% </span><span class="rename-able">interest rate. Actual monthly payment may vary.</span>
				        </div>
				        <div id="popEstimatePaymentCalculator" data-role="popup" data-position-to="window">
					        <div data-role="content">
						        <div class="row">
							        <div class="col-sm-12 header_theme2">
								        <a href="#" data-rel="back" class="pull-right svg-btn"><%=HeaderUtils.IconClose %><span style="display: none;">close</span></a>
							        </div>
						        </div>
						        <div class="row">
							        <div class="col-sm-12">
								        <div data-role="fieldcontain">
									        <label>Loan Amount</label>    
									        <input type="text" title ="Loan Amount" data-field="LoanAmount" pattern="[0-9]*" class="money" maxlength ="12"/>
								        </div>   
							        </div>    
						        </div>
						        <div class="row">
							        <div class="col-sm-12">
								        <div data-role="fieldcontain">
									        <label>Loan Term (months)</label>
									         <select data-field="LoanTerm" title ="Loan Term">
										        <%=_LoanTermDropdown%>
										        <%If String.IsNullOrEmpty(_LoanTermDropdown) Then%>
											        <option value="12">12</option>  
											        <option value="24">24</option>    
											        <option value="36">36</option>    
											        <option value="48">48</option>    
											        <option value="60">60</option>    
											        <option value="72">72</option>  
										        <%End If%>    
									        </select>
								        </div>
								        <div data-role="fieldcontain" style="display:none">
									         <label>Loan Term (months)</label>
									        <input data-field="LoanTerm" type="text" title ="Loan Term" pattern="[0-9]*" class="numeric" maxlength="3"/>
								        </div>
							        </div>
						        </div>
						        <div class="row">
							        <div class="col-sm-12">
								        <div data-role="fieldcontain">
									        <label>Interest Rate</label>    
									        <input type="text" title ="Interest Rate" data-field="InterestRate" pattern="[0-9]*" class="numeric" maxlength ="4" value="<%=_interestRate.ToString("N2", New CultureInfo("en-US")) %>%"/>
								        </div>   
							        </div>    
						        </div>
						        <hr/>
						        <div class="row">
							        <div class="col-sm-12">
								        <div data-role="fieldcontain" style="text-align: center;">
									        <div>Estimated Payment</div>
									        <div style="font-weight: bold; font-size: 1.4em; margin-left: 6px;" data-field="EstimatePayment">$0.00/mo</div>
								        </div>   
							        </div>    
						        </div>

						        <br />
						        <%--<div class="div-continue">
							        <a href="#" type="button" data-role="button" class="div-continue-button">Apply</a>
						        </div>--%>
					        </div>
				        </div>
			        </div>
			        <%End If%>
			        <div data-role='fieldcontain'>
				         <div style="margin-top: 10px;" id="divTradeInLnk">
					        <a href="#" id="lnkHasTradeIn" onclick="VL.FACTORY.ShowHideTradeInPanel(event)" data-mode="self-handle-event" data-status="N" class="header_theme2 shadow-btn plus-circle-before" style="cursor: pointer; font-weight: bold;">Add a trade-in</a>
				        </div>
			        </div>
			        <br />
			        <div id="divTradeInPanel" style="display:none">
				        <div style="font-size:17px;font-weight :bold">Trade-in Information</div>
				        <div data-role="fieldcontain">
					        <label for="ddlTradeInVehicleType" class="RequiredIcon">What type of vehicle is this?</label>
					        <select id="ddlTradeInVehicleType">
						        <%=_VehicleTypes%>
					        </select>
				        </div>
				        <div id="divTradeInVehicleMake">
                            <div id="divTradeInCarMake" data-role="fieldcontain">
                                <label for="ddlTradeInCarMake">Vehicle Make</label>
                                <select id="ddlTradeInCarMake">
                                    <%=_CarMakeDropdown%>
                                </select>
                            </div>
                    
                            <div id="divTradeInMotorCycleMake" data-role="fieldcontain">
                                <label for="ddlTradeInMotorCycleMake">Vehicle Make</label>
                                <select id="ddlTradeInMotorCycleMake">
                                    <%=_MotorCycleMakeDropdown%>
                                </select>
                            </div>
                             <div id="divTradeInMake" data-role="fieldcontain">
                                 <label for="txtTradeInMake">Vehicle Make</label>
						        <input type="text" id="txtTradeInMake" maxlength="50" />
                              </div>
                        </div>
                        <div data-role="fieldcontain">
                            <label for="txtTradeInVehicleModel">Vehicle Model</label>
                            <input type="text" id="txtTradeInVehicleModel" maxlength="50" />
                        </div>
                        <div data-role="fieldcontain">
                            <label for="txtTradeInVehicleYear">Vehicle Year</label>
                            <input type="tel" id="txtTradeInVehicleYear" class="numeric" maxlength="4" />
                        </div>
				        <div data-role="fieldcontain">
                            <label for="txtTradeInVehicleCurrentValue">What is the current value of this vehicle?</label>
                            <input type="<%=_textAndroidTel%>" pattern ="[0-9]*" id="txtTradeInVehicleCurrentValue" class="money" maxlength="12" />
                        </div>
				        <div data-role="fieldcontain">
                            <label for="txtTradeInVehicleOwn">How much do you still owe on this vehicle?</label>
                            <input type="<%=_textAndroidTel%>" pattern ="[0-9]*" id="txtTradeInVehicleOwn" class="money" maxlength="12" />
                        </div>
				        <div data-role="fieldcontain">
                            <label for="txtTradeInVehiclePayingMonthly">How much are you paying monthly for this vehicle?</label>
                            <input type="<%=_textAndroidTel%>" pattern ="[0-9]*" id="txtTradeInVehiclePayingMonthly" class="money" maxlength="12" />
                        </div>
			        </div>
				   <uc:xaApplicantQuestion ID="xaApplicationQuestionLoanPage" LoanType="VL" IDPrefix="loanpage_" CQLocation="LoanPage" runat="server"  />
                    <div id="disclosure_place_holder"></div> 
                    <div class ="div-continue"  data-role="footer">
                    <a href="#" data-transition="slide" onclick="VL.FACTORY.validateVl1(this);" type="button" class="div-continue-button">Continue</a> 
                    <a href="#divErrorDialog" style="display: none;">no text</a>
                    <a href='<%=IIf(Is2ndLoanApp = true, "#pagesubmit", "#vl2") %>' style="display: none;">no text</a>
                </div>
             </div> <!--end div isComboMode and _locationPool -->       
        </div>  <!--end div data-role = "content" -->   
    </div>
      
	<%If VinBarcodeScanEnabled Then%>
    <div data-role="dialog" id="vinscan">
        <div data-role="header"  style="display:none" >
            <h1>Vin Number Scan</h1>
        </div>
        <div data-role="content">
	        <%If LaserDLScanEnabled Then%>
			<div class="js-laser-vin-scan-container">
			<a data-rel="back" href="#" class="svg-btn btn-close-dialog"><%=HeaderUtils.IconClose %><span style="display: none;">close</span></a> 
			<uc:VimNumberScan ID="ucVinNumberScan" IDPrefix="" runat="server" />
			</div>
			<%End If %>
        </div>
    </div>    
    <%End If %>
		<asp:PlaceHolder runat="server" ID="plhApplicantAdditionalInfo"></asp:PlaceHolder>
	<asp:PlaceHolder runat="server" ID="plhApplicationDeclined"></asp:PlaceHolder>
	<asp:PlaceHolder runat="server" ID="plhInstaTouchStuff"></asp:PlaceHolder>
		<div data-role="page" id="pageApplicationCancelled">
			<div data-role="header" class="sr-only">
				<h1>Application Cancelled</h1>
			</div>
		  <div data-role="content">
			  <%=HeaderUtils.RenderLogoAndSteps(_LogoUrl, 0, "APPLY_CC")%>
			  <br/>
			  <div class="text-center">
			    <%=HeaderUtils.RenderPageTitle(0, "Application Cancelled", False)%>
				</div>
			  <br/>
			  <p class="text-center">We have cancelled your application.</p>
			  <p class="text-center">If this was a mistake and you would like to fix errors you can return to the application now.</p>
			  <br/>

				<div class="row div-continue" style="margin-top: 10px; text-align: center; width:100%;" data-role="footer">
					<div class="col-md-2 hidden-xs"></div>
					<div class="col-md-4 col-xs-12">
						<a href="#" onclick="ABR.FACTORY.closeApplicationBlockRulesDialog()" type="button" data-role="button" class="div-continue-button">Return to Application</a>	
					</div>
					<div class="col-md-4 col-xs-12">
						<a href="#" onclick="gotoToUrl(this,'<%=_RedirectURL%>')" data-role="button" type="button" class="div-continue-button">Close</a>
						</div>
					<div class="col-md-2 hidden-xs"></div>
				</div>
		   </div>
	 </div>
    <!--about you(vl2): applicant infor + applicant address -->
     <div data-role="page" id="vl2">
        <div data-role="header" class="sr-only">
            <h1>Applicant Information</h1>
        </div>
        <div data-role="content">
            <%=HeaderUtils.RenderLogoAndSteps(_LogoUrl, 1, "APPLY_CC")%>
            <%=HeaderUtils.RenderPageTitle(21, "Tell Us About Yourself", false)%>
            <%=HeaderUtils.RenderPageTitle(18, "Personal Information", true)%>
            <uc:applicantInfo ID="ucApplicantInfo" IDPrefix="" runat="server" LoanType="VL"/>

               <%-- Reference info--%>
			<%If CheckShowField("divReferenceInformation", "", True) Or(IsInMode("777") AndAlso IsInFeature("visibility")) Then%>
            <uc:ReferenceInfo id="ucReferenceInfo" IDPrefix="" runat="server" />
			<%End If %>

            <%=HeaderUtils.RenderPageTitle(22, "Contact Information", true)%>
            <uc:ccContactInfo ID="ucContactInfo" IDPrefix="" runat="server" />
            <!--applicant address -->
            <%=HeaderUtils.RenderPageTitle(14, "Current Physical Address", True)%>
            <uc:applicantAddress ID="ucApplicantAddress" LoanType="VL" IDPrefix="" runat="server"  />
			<%If EnableIDSection Then%>
            <uc:ucApplicantID ID="ucApplicantID" runat="server" />
            <%End If%>
            <%=HeaderUtils.RenderPageTitle(23, "Financial Information", true)%>
            <uc:financialInfo ID="ucFinancialInfo" IDPrefix="" runat="server" />
            <uc:DocUpload ID="ucDocUpload" IDPrefix="" runat="server" />
            <uc:Asset id="ucAssetInfo" IDPrefix="" runat="server"/>
			<%If _declarationList IsNot Nothing AndAlso _declarationList.Count > 0 Then%>
            <div>
                <%=HeaderUtils.RenderPageTitle(15, "Declaration", true)%>
				<div style="clear: left; margin-top: 10px;"></div>
                <uc:declaration ID="ucDeclaration" IDPrefix="" LoanType="VL" runat="server" />
            </div>
			<%End If %>
			<% If _isCQNewAPI Then%>
                <%If IsComboMode Then%>
                    <uc:xaApplicantQuestion ID="xaComboApplicantQuestion" LoanType="VL" isComboMode="true" IDPrefix="" CQLocation="ApplicantPage" runat="server"  />
                <%Else%>
                    <uc:xaApplicantQuestion ID="applicantQuestion" LoanType="VL" isComboMode="false" IDPrefix="" CQLocation="ApplicantPage" runat="server"  />
                <%End If  %>
      		<%Else%>
                <%If IsComboMode Then%>
                    <uc:xaApplicantQuestion ID="xaApplicantQuestion" LoanType="VL" IDPrefix="" CQLocation="ApplicantPage" runat="server" />
                <%End If%>
            <%End If %>
            <!--Applicant GMI-->
            <uc:applicantGMI ID="ucApplicantGMI" IDPrefix="" runat="server" />
            
        </div>
          <div class="div-continue" data-role="footer">
              <%-- 
             <a href="#"  data-transition="slide" onclick="VL.FACTORY.validateVl2(this);" type="button" class="div-continue-button">Continue</a> 
             <a href="#divErrorDialog" style="display: none;">no text</a>
             <%--<a href="#vl4" style="display: none;"></a>--% > 
              Or <a href="#vl1" class ="div-goback" data-corners="false" data-shadow="false" data-theme="reset"><span class="hover-goback"> Go Back</span></a>   
            --%>  
               <%If _enableJoint Then%>           
                <a  type="button" class="div-continue-button div-continue-coapp-button" href="#"  id="continueWithoutCoApp" onclick="VL.FACTORY.validateVl2(this);">Continue  without Co-Applicant</a>
                <a  type="button" class="div-continue-button div-continue-coapp-button" href="#" id="continueWithCoApp"  onclick="VL.FACTORY.validateVl2(this);" >Continue with Co-Applicant</a>                       
             <%Else%>
                <a href="#"  data-transition="slide" onclick="VL.FACTORY.validateVl2(this);" type="button" class="div-continue-button">Continue</a> 
             <%End If%>
                <a href="#divErrorDialog" style="display: none;">no text</a>
                 Or <a href="#vl1" class ="div-goback" data-corners="false" data-shadow="false" data-theme="reset"><span class="hover-goback"> Go Back</span></a>   
			<%--<%If IsFinishLaterEnabled Then%>
			 <a style="display: block;" class="div-finish-later" onclick="VL.FACTORY.saveAndFinishLater()">Save & Finish Later</a>
			 <%End If%>--%>
          </div>
    </div>
	<%If LegacyDLScanEnabled OrElse LaserDLScanEnabled Then%>
    <div data-role="dialog" id="scandocs">
        <div data-role="header"  style="display:none" >
            <h1>Driver's License Scan</h1>
        </div>
        <div data-role="content">
	        <%If LaserDLScanEnabled Then%>
			<div class="js-laser-scan-container">
			<a data-rel="back" href="#" class="svg-btn btn-close-dialog"><%=HeaderUtils.IconClose %><span style="display: none;">close</span></a> 
			<uc:LaserDriverLicenseScan ID="laserDriverlicenseScan" IDPrefix="" runat="server" />
			</div>
			<%End If %>
			<div class="js-legacy-scan-container <%=IIf(LaserDLScanEnabled, "hidden", "") %>" >
				<a data-rel="back" href="#" style="margin-bottom: 5px;" class="pull-right svg-btn"><%=HeaderUtils.IconClose %><span style="display: none;">close</span></a> 
				<uc:driverLicenseScan ID="driverlicenseScan" IDPrefix="" runat="server" />
				<div class="div-continue" style="text-align: center;">
					<a href="#" onclick="ScanAccept('')" data-transition="slide" type="button" data-role="button" class="div-continue-button">Done</a> 
					<a href="#divErrorDialog" style="display: none;">no text</a>
					<a href='#vl2' style="display: none;">no text</a>
				</div>
			</div>
        </div>
    </div>    
    <%End If %>
		<uc:DocUploadSrcSelector ID="ucDocUploadSrcSelector" IDPrefix="" runat="server" />
		<uc:DocUploadSrcSelector ID="ucCoDocUploadSrcSelector" IDPrefix="co_" runat="server" />
    <!--About Your CoApp(vl6): co_applicant info + CoApp current address -->
    <div data-role="page" id="vl6">
        <div data-role="header" class="sr-only">
            <h1>Co-applicant Information</h1>
        </div>
        <div data-role="content">
            <%=HeaderUtils.RenderLogoAndSteps(_LogoUrl, 1, "APPLY_CC")%>
            <%=HeaderUtils.RenderPageTitle(21, "About Your Co-applicant", false)%>
            <%=HeaderUtils.RenderPageTitle(18, "Personal Information", true)%>
             <div id="co_scanDocumentMessage" style="display:none;"><span id="co_spScanDocument" class="require-span"></span><a href ="#co_scandocs"> Retake Photo</a></div>
             <uc:applicantInfo ID="ucCoApplicantInfo" IDPrefix="co_" runat="server" LoanType="VL"/>

             <%-- Reference info--%>
			<%If CheckShowField("divReferenceInformation", "co_", True) Or (IsInMode("777") AndAlso IsInFeature("visibility")) Then%>
            <uc:ReferenceInfo id="ucCoReferenceInfo" IDPrefix="co_" runat="server" />
			<%End If%>

            <%=HeaderUtils.RenderPageTitle(22, "Contact Information", true)%>
            <uc:ccContactInfo ID="ucCoContactInfo" IDPrefix="co_" runat="server" />
            <%=HeaderUtils.RenderPageTitle(14, "Current Address", true)%>
            <uc:applicantAddress ID="ucCoApplicantAddress" LoanType="VL" IDPrefix="co_" runat="server" />
			 <%If EnableIDSection("co_") Then%>
				<uc:ucApplicantID ID="ucCoApplicantID" IDPrefix="co_" runat="server" />
			<%End If%>
            <%=HeaderUtils.RenderPageTitle(23, "Financial Information", true)%>
            <uc:financialInfo ID="ucCoFinancialInfo" IDPrefix="co_" runat="server" />
            <uc:DocUpload ID="ucCoDocUpload" IDPrefix="co_" runat="server" />
            <uc:Asset id="ucCoAssetInfo" IDPrefix="co_" runat="server"/>
			<%If _declarationList IsNot Nothing AndAlso _declarationList.Count > 0 Then%>
                <div>
					<%=HeaderUtils.RenderPageTitle(15, "Co/Joint Declarations", true)%>
                    <uc:declaration ID="ucCoDeclaration" LoanType="VL" IDPrefix="co_" runat="server" />
                </div>
				<%End If %>
			<% If _isCQNewAPI Then%>
                <%If IsComboMode Then%>
                    <uc:xaApplicantQuestion ID="coXaComboApplicantQuestion" LoanType="VL" isComboMode ="true" IDPrefix="co_" CQLocation="ApplicantPage" runat="server" />
			    <%Else%>
                    <uc:xaApplicantQuestion ID="coApplicantQuestion" LoanType="VL" isComboMode ="false" IDPrefix="co_" CQLocation="ApplicantPage" runat="server" />
                <%End If%>
            <%Else%>
               <%If IsComboMode Then%>
                   <uc:xaApplicantQuestion ID="coXaApplicantQuestion" LoanType="VL" IDPrefix="co_" CQLocation="ApplicantPage" runat="server" />
               <%End If%>
            <%End If %>
            <!--Applicant GMI-->
            <uc:applicantGMI ID="ucCoApplicantGMI" IDPrefix="co_" runat="server" />
        </div>
        <div class ="div-continue" data-role="footer">
            <a href="#"  data-transition="slide" onclick="VL.FACTORY.validateVl6(this);" type="button" class="div-continue-button">Continue</a>
            <a href="#divErrorDialog" style="display: none;">no text</a>           
            Or <a href="#vl2" class ="div-goback" data-corners="false" data-shadow="false" data-theme="reset"><span class="hover-goback"> Go Back</span></a> 
			<%--<%If IsFinishLaterEnabled Then%>
			 <a style="display: block;" class="div-finish-later" onclick="VL.FACTORY.saveAndFinishLater()">Save & Finish Later</a>
			 <%End If%>--%>
        </div>
        
    </div>
	<%If LegacyDLScanEnabled OrElse LaserDLScanEnabled Then%>
    <div data-role="dialog" id="co_scandocs">
        <div data-role="header"  style="display:none" >
            <h1>Co-Driver's License Scan</h1>
        </div>
        <div data-role="content">
	        <%If LaserDLScanEnabled Then%>
			<div class="js-laser-scan-container">
			<a data-rel="back" href="#" class="svg-btn btn-close-dialog"><%=HeaderUtils.IconClose %><span style="display: none;">close</span></a> 
			<uc:LaserDriverLicenseScan ID="coAppLaserDriverlicenseScan" IDPrefix="co_" runat="server" />
			</div>
			<%End If %>
			<div class="js-legacy-scan-container <%=IIf(LaserDLScanEnabled, "hidden", "") %>" >
				<a data-rel="back" href="#" style="margin-bottom: 5px;" class="pull-right svg-btn"><%=HeaderUtils.IconClose %><span style="display: none;">close</span></a>  
				<uc:driverLicenseScan ID="coAppDriverlicenseScan" IDPrefix="co_" runat="server" />
				<div class="div-continue" style="text-align: center;">
					<a href="#" onclick="ScanAccept('co_')" data-transition="slide" type="button" data-role="button" class="div-continue-button">Done</a> 
					<a href="#divErrorDialog" style="display: none;">no text</a>
					<a href='#vl6' style="display: none;">no text</a> 
				</div>
			</div>
        </div>
    </div>
	<%End If %>
   
    <div data-role="page" id="pagesubmit">
        <div data-role="header" class="sr-only">
            <h1>Review and Submit</h1>
        </div>
        <div data-role="content">
            <%=HeaderUtils.RenderLogoAndSteps(_LogoUrl, 2, "APPLY_CC")%>
            <%=HeaderUtils.RenderPageTitle(22, "Review and Submit", False)%>
            <div id="reviewPanel" class="review-container container-fluid" data-role="fieldcontain">
                     <%--Branch selections --%>
                 <div class="row" style="display :none">
                        <div class="row-title section-heading">
                            <span class="bold">Branch Selection</span>        
                        </div>
                  </div>
                 <div class="row panel BranchSelection"></div>

                <div class="row">
                    <div class="row-title section-heading"><span class="bold">Vehicle Loan Information</span></div>
                </div>
                <div class="row panel ViewVehicleLoanInfo"></div>
				<%If Is2ndLoanApp = False Then%>
                <div class="row">
                    <div class="row-title section-heading"><span class="bold">Applicant Information</span></div>
                </div>
                <div class="row panel ViewAcountInfo"></div>
                <%If NSSList IsNot Nothing AndAlso NSSList.Count > 0 Then%>
				<div class="row" style="display: none;">
                    <div class="row-title section-heading"><span class="bold">Your Spouse's Information</span></div>
                </div>
                <div class="row panel ViewPrimaryAppSpouseInfo" style="display: none;"></div>
				<%End If%>
                    <%--View Reference --%>
				<%If CheckShowField("divReferenceInformation", "", True) Or (IsInMode("777") AndAlso IsInFeature("visibility")) Then%>
                <div class="row">
                    <div class="row-title section-heading"><span class="bold">Reference Information</span></div>
                </div>              
                 <div class="row panel ViewReferenceInfo"></div>
				<%End If%>
                <div class="row">
                    <div class="row-title section-heading">
                        <span class="bold">Applicant Contact Information</span>        
                    </div>
                </div>
                <div class="row panel ViewContactInfo"></div>
                <div class="row">
                    <div class="row-title section-heading">
                        <span class="bold">Address</span>        
                    </div>
                </div>
                <div class="row panel ViewAddress"></div>
				<%If EnableIDSection Then%>
					<div class="row">
						<div class="row-title section-heading">
							<span class="bold">Your Identification</span>        
						</div>
					</div>
					<div class="row panel ViewApplicantID"></div>
				<%End If %>
                <div class="row">
                    <div class="row-title section-heading">
                        <span class="bold">Financial Information</span>        
                    </div>
                </div>
                <div class="row panel ViewFinancialInfo"></div>
                <div class="row">
                    <div class="row-title section-heading">
                        <span class="bold">Previous Employment Information</span>        
                    </div>
                </div>
                <div class="row panel ViewPrevEmploymentInfo"></div>
				<%If _declarationList IsNot Nothing AndAlso _declarationList.Count > 0 Then%>
                    <div class="row" style="display: none;">
                        <div class="row-title section-heading">
                            <span class="bold">Declaration</span>        
                        </div>
                    </div>
                    <div class="row panel ViewDeclaration" style="display: none;"></div>
				<%End If%>
                <div class="row jna-panel" style="display: none;">
                    <div class="row-title section-heading">
                        <span class="bold">Joint Applicant Information</span>        
                    </div>
                </div>
                <div class="row panel jna-panel ViewJointApplicantInfo" style="display: none;"></div>
				<%If NSSList IsNot Nothing AndAlso NSSList.Count > 0 Then%>
				<div class="row jna-panel" style="display: none;">
                    <div class="row-title section-heading">
                        <span class="bold">Joint Applicant Spouse's Information</span>        
                    </div>
                </div>
				<div class="row panel jna-panel ViewCoAppSpouseInfo" style="display: none;"></div>
				<%End If%>
                 <%--View Joint Reference --%>
				<%If CheckShowField("divReferenceInformation", "co_", True) Or (IsInMode("777") AndAlso IsInFeature("visibility")) Then%>
                <div class="row jna-panel" style="display: none;">
                    <div class="row-title section-heading"><span class="bold">Joint Reference Information</span></div>
                </div> 
                <div class="row panel jna-panel ViewJointReferenceInfo" style ="display:none;"></div>
				<%End If%>
                <div class="row jna-panel" style="display: none;">
                    <div class="row-title section-heading">
                        <span class="bold">Joint Applicant Contact Information</span>        
                    </div>
                </div>
                <div class="row panel jna-panel ViewJointApplicantContactInfo" style="display: none;"></div>
                <div class="row jna-panel" style="display: none;">
                    <div class="row-title section-heading">
                        <span class="bold">Joint Applicant Address</span>        
                    </div>
                </div>
                <div class="row panel jna-panel ViewJointApplicantAddress" style="display: none;"></div>
				<%If EnableIDSection("co_") Then%>
				<div class="row jna-panel" style="display: none;">
					<div class="row-title section-heading">
						<span class="bold">Joint Applicant Identification</span>        
					</div>
				</div>
				<div class="row panel jna-panel ViewJointApplicantID" style="display: none;"></div>
				<%End If%>
                <div class="row jna-panel" style="display: none;">
                    <div class="row-title section-heading">
                        <span class="bold">Joint Applicant Financial Information</span>        
                    </div>
                </div>
                <div class="row panel jna-panel ViewJointApplicantFinancialInfo" style="display: none;"></div>
                <div class="row jna-panel" style="display: none;">
                    <div class="row-title section-heading">
                        <span class="bold">Joint Applicant Previous Employment Information</span>        
                    </div>
                </div>
                <div class="row panel jna-panel ViewJointApplicantPrevEmploymentInfo" style="display: none;"></div>
				<%If _declarationList IsNot Nothing AndAlso _declarationList.Count > 0 Then%>
                    <div class="row jna-panel" style="display: none;">
                        <div class="row-title section-heading">
                            <span class="bold">Joint Applicant Declaration</span>        
                        </div>
                    </div>
                    <div class="row panel jna-panel ViewJointApplicantDeclaration" style="display: none;"></div>
					<%End If %>
                    <%--View Applicant Questions --%>
                  <div class="row" style="display: none;">
                        <div class="row-title section-heading">
                            <span class="bold">Additional Information</span>        
                        </div>
                    </div>
                    <div class="row panel ViewApplicantQuestion" style="display: none;"></div>
                       <%--view Joint Applicant Questions --%>
                  <div class="row panel co_ViewApplicantQuestion"></div>
						<div class="row" style="display: none;">
							<div class="row-title section-heading">
								<span class="bold">Joint Additional Information</span>        
							</div>
					</div>
					<div class="row panel ViewJointApplicantQuestion" style="display: none;"></div> 
			<%End If%>
            </div>
			<%If IsComboMode Then%>
			    <div id="div_fom_section" class="<%=IIf(_HideXAFOM, "hidden", "")%>">
                <%=HeaderUtils.RenderPageTitle(18, "Eligibility", True, isRequired:=true)%>
				<p class="header_theme rename-able" style="font-weight: bold; font-size: 16px; padding-left: 3px;margin-top: 2px;"><%=IIf(String.IsNullOrEmpty(_comboEligibilityHeader), "Because membership is required to take advantage of this wonderful loan product, please tell us how you qualify for membership.", _comboEligibilityHeader)%></p>
				<uc:ucFOMQuestion runat="server" ID='ucFomQuestion' />
                </div>
				<div data-role="fieldcontain">
					<%=HeaderUtils.RenderPageTitle(0, "If I am not approved for the above loan product,", True, isRequired:=True)%>
					<div data-section="proceed-membership" id="divProceedMembership">
						<a data-role="button" href="#" class="btn-header-theme rename-able" data-command="proceed-membership" data-key="Y" style="text-align: center; padding-left: 0;"><%:IIf(_CurrentWebsiteConfig.InstitutionType = CEnum.InstitutionType.BANK, "I wish to continue opening my account.", "I wish to continue opening my membership.")%></a>
						<a data-role="button" href="#" class="btn-header-theme rename-able" data-command="proceed-membership" data-key="N" style="text-align: center; padding-left: 0;"><%:IIf(_CurrentWebsiteConfig.InstitutionType = CEnum.InstitutionType.BANK, "I don't wish to continue opening my account.", "I don't wish to continue opening my membership.")%></a>           
					</div>
				</div>
			<%End If%>
			<%If IsComboMode and _showProductSelection = true Then %>
				<asp:PlaceHolder runat="server" ID="plhProductSelection"></asp:PlaceHolder>
				<div id="divCcFundingOptions" style="margin-top: 40px;">  <%--This section is only available for ComboMode, TODO:may need to create a separate page--%>
					 <%=HeaderUtils.RenderPageTitle(23, "Funding", True)%> 
					<p class="header_theme" style="font-weight: bold; font-size: 16px; padding-left: 3px;margin-top: 2px;"><%=IIf(String.IsNullOrEmpty(_comboFundingHeader), "Your funding will not be transferred until all your accounts are approved.", _comboFundingHeader)%></p>   
					<div id="DivDepositInput">
						<%--<span class="ProductCQ-Font">How much do you want to deposit?</span>--%>
						<%=HeaderUtils.RenderPageTitle(0, "How much do you want to deposit?", True)%>
						<%--<i style="font-weight: normal; margin-left: 30px;"><%=_prerequisiteProduct.AccountName %> Account ($ <%=Common.SafeInteger(_prerequisiteProduct.MinimumDeposit)%>.00 min deposit)</i>
						<span  onclick='openPopup("#popUpSavingAmountGuid")' data-rel='popup' data-transition='pop'><div class='questionMark'></div></span>--%>
						<%--<%
							Dim maxFunding As Decimal = 0D
							If String.IsNullOrEmpty(_MaxFunding) = False AndAlso Decimal.TryParse(_MaxFunding, maxFunding) = True Then%>
							<div><i style="font-weight: normal; margin-left: 30px;">Maximum deposit amount allowed is <%=maxFunding.ToString("C2", New CultureInfo("en-US")) %></i></div>
						<%End If%>--%>
						<%--onblur="validateMaxFunding(this)"--%>
						<div class="LineSpace" id="divDepositProductPool"></div>

                        <%If _MembershipFee > 0 Or _customListFomFees.Count > 0 Then%>
					          <div data-role="fieldcontain">
						     <label for="divMembershipFee">One-time Membership Fee</label>
						     <div id="divMembershipFee" data-membershipfee="<%=_MembershipFee.ToString%>"> <%=_MembershipFee.ToString("C2", New CultureInfo("en-US")) %></div>
					    </div>
					    <%End If%>

						<div data-role="fieldcontain" style="margin-top: 0px;">
							<label for="txtFundingDeposit" class="RequiredIcon" style="font-weight: bold">Total Deposit</label>
							<input type="<%=_textAndroidTel%>" aria-labelledby="DivDepositInput" id="txtFundingDeposit" class="money" readonly="readonly"  value="<%=0D.ToString("C2", New CultureInfo("en-US")) %>"/>
						</div>
					</div>
					<uc:ccFundingOptions ID="ccFundingOptions" runat="server" />
					<div id="popUpSavingAmountGuid" data-role="popup" data-position-to="window" style="max-width: 400px;">
						<div data-role="content">
							<div class="row">
								<div class="col-xs-12 header_theme2">
									<a href="#" data-rel="back" class="pull-right svg-btn"><%=HeaderUtils.IconClose %><span style="display: none;">close</span></a>
								</div>
							</div>
							<div class="row">
								<div class="col-sm-12">
									<div style="margin: 10px 0;">
										A minimum deposit of $<%=Common.SafeInteger(_prerequisiteProduct.MinimumDeposit)%> in your <%=_prerequisiteProduct.AccountName %> Account is required to fund your Secured Credit Card. Please enter the amount that you wish to deposit and secure on your new card.
									</div>    
								</div>    
							</div>
						</div>
					</div>
					<br />
				</div> <%--end of divCcFundingOptions--%>
			<%End If%>
            <uc:xaApplicantQuestion ID="xaApplicationQuestionReviewPage" LoanType="VL" IDPrefix="reviewpage_" CQLocation="ReviewPage" runat="server" />
            
            <!--Credit Life and Insurance info -->
            <uc:ProtectionInfo ID="ucProtectionInfo" runat="server" LoanType ="VL" />
          
			<div id="divDisclosure">
				<%=HeaderUtils.RenderPageTitle(17, "Read, Sign and Submit", True)%>
				<div style="margin-top: 10px;">
					 <div id="divSubmitMessage">
						<%If Not String.IsNullOrEmpty(_CreditPullMessage) Then%>
							<p class="rename-able bold"><%=_CreditPullMessage%></p>
							<%Else%>  
							<p class="rename-able bold">Your application is not complete until you read the disclosure below and click the “I Agree” button in order to submit your application.</p>
							<p class="rename-able">You are now ready to submit your application! By clicking on "I agree", you authorize us to verify the information you submitted and may obtain your credit report. Upon your request, we will tell you if a credit report was obtained and give you the name and address of the credit reporting agency that provided the report. You warrant to us that the information you are submitting is true and correct. By submitting this application, you agree to allow us to receive the information contained in your application, as well as the status of your application.</p>
                              
						<%End If%>
					</div> 
					<uc:disclosure ID="ucDisclosures" LoanType="VL" runat ="server" Name="disclosures" />                  
				</div>
				<br />
				<div id="divDisagree" data-role='fieldcontain'>
					<a id="lnkDisagree" href  ="#" class="header_theme2 shadow-btn chevron-circle-right-before" style="cursor: pointer; font-weight: bold;">I disagree</a>
				</div>
			</div>
        </div>
        <div class="div-continue" data-role="footer">
            <a href="#"  id="vehicle-loan-submit" data-transition="slide" onclick="VL.FACTORY.validatePageSubmit(this);" type="button" class="div-continue-button">I Agree</a> 
            <a href="#divErrorDialog" style="display: none;">no text</a>
            Or <a href="#" class ="div-goback" data-corners="false" onclick="goBackPageSubmit();" data-shadow="false" data-theme="reset"><span class="hover-goback"> Go Back</span></a>   
            <a id="href_show_last_dialog" href="#vl_last" style="display: none;">no text</a>
            <a id="href_show_dialog_1" href="#divErrorDialog" data-rel="dialog" style="display: none;">no text</a>
			<%--<%If IsFinishLaterEnabled Then%>
			 <a style="display: block;" class="div-finish-later" onclick="VL.FACTORY.saveAndFinishLater()">Save & Finish Later</a>
			 <%End If%>--%>
        </div>
    </div>
    <%If IsComboMode and _showProductSelection %>
		<div id="productDetail" data-role="dialog" data-close-btn="none">
			<div data-role="header">
				<button class="header-hidden-btn">.</button>
				<div data-place-holder="heading" class="page-header-title"></div>
				<div tabindex="0" data-main-page="pagesubmit" data-command="close" class="header_theme2 ui-btn-right" data-role="none">
					<%=HeaderUtils.IconClose %><span style="display: none;">close</span>
				</div>
			</div>
			<div data-role="content" class="fieldset-container">
				<fieldset data-role="controlgroup" data-place-holder="description">
					<legend class="heading-title">Description</legend>
					<p style="margin-bottom: 0px;"></p> 
				</fieldset>
                <fieldset data-place-holder="rates">
					<legend class="heading-title">Details</legend>
				</fieldset>
				<fieldset data-place-holder="services">
					<legend class="heading-title">Select feature(s)</legend>
					<div class="ui-controlgroup-controls"></div>
				</fieldset>
				<fieldset data-place-holder="questions">
					<legend class="heading-title">Additional feature(s)</legend>
					<div class="ui-controlgroup-controls"></div>
				</fieldset>
				<div style="text-align: center;">
					<a href="#" data-role="button" data-mode="self-handle-event" type="button" data-main-page="pagesubmit" data-inline="true" id="btnAddProduct">Add Account</a>
				<a href="#" data-role="button" data-mode="self-handle-event" type="button" data-main-page="pagesubmit" data-inline="true" id="btnSaveProduct">Save Account</a>
					<a href="#" data-role="button" data-mode="self-handle-event" type="button" data-main-page="pagesubmit" data-inline="true" style="min-width: 150px;" id="btnCloseProductDetail">OK</a>
					<input type="hidden" value="" data-place-holder="productCode" />
				</div>    
			</div>
		</div>
			<div id="fundingDialog" data-role="dialog" data-close-btn="none" style="width: 100%;">
			<div data-role="header">
				<button class="header-hidden-btn">.</button>
				<div data-place-holder="heading" class="page-header-title"></div>
				<div tabindex="0" data-command="close" data-main-page="pagesubmit" class="header_theme2 ui-btn-right" data-role="none">
					<%=HeaderUtils.IconClose %><span style="display: none;">close</span>
				</div>
			</div>
			<div data-role="content" class="fieldset-container">
				<fieldset data-place-holder="rates">
					<legend class="heading-title">Choose your rate</legend>
				</fieldset>
				<fieldset data-place-holder="deposit">
					<legend class="heading-title">Specify the amount to deposit</legend>
					<div class="row">
						<div class="col-xs-12 col-sm-4 no-padding">
							<div data-role="fieldcontain">
								<input type="text" class="money" maxlength="12" id="txtRateDepositAmount"/>
							</div>
						</div>
					</div>
				</fieldset>
				<fieldset data-place-holder="term">
					<legend class="heading-title">Choose your term</legend>
					<div class="row">
						<div class="col-xs-12 col-sm-4 no-padding">
							<div data-role="fieldcontain">
								<label for="txtRateTermLength" id="lblRateTermLength" class="RequiredIcon"></label>
								<input type="<%=TextAndroidTel%>" class="number-only" maxlength="12" id="txtRateTermLength"/>
							</div>
						</div>
						<div class="col-xs-12 col-sm-8 no-padding maturity-date-wrapper">
							<div class="row">
								<div class="col-xs-12 col-sm-2 no-padding text-center-sm">
									<div data-role="fieldcontain">
										<label class="hidden-xs">&nbsp;</label>
										<label>Or</label>
									</div>
								</div>
								<div class="col-xs-12 col-sm-10 no-padding">
									<div data-role="fieldcontain" id="divRateMaturityDateStuff">
										<div class="row">
											<div class="col-xs-8 no-padding">
												<label for="txtRateMaturityDate" id="lblRateMaturityDate" class="RequiredIcon">Maturity Date</label>
											</div>
											<div class="col-xs-4 no-padding"><input type="hidden" id="txtRateMaturityDate" class="combine-field-value" value="" /></div>
										</div>
										<div id="divRateMaturityDate" class="ui-input-date row">
											<div class="col-xs-4">
												<input aria-labelledby="lblRateMaturityDate" type="<%=TextAndroidTel%>" pattern="[0-9]*" placeholder="mm" id="txtRateMaturityDate1" maxlength ='2' onkeydown="limitToNumeric(event);" onkeyup="onKeyUp(event,'#txtRateMaturityDate1','#txtRateMaturityDate2', '2');" value="" />
											</div>
											<div class="col-xs-4">
												<input aria-labelledby="lblRateMaturityDate" type="<%=TextAndroidTel%>" pattern="[0-9]*" placeholder="dd" id="txtRateMaturityDate2" maxlength ='2' onkeydown="limitToNumeric(event);" onkeyup="onKeyUp(event,'#txtRateMaturityDate2','#txtRateMaturityDate3', '2');" value="" />
											</div>
											<div class="col-xs-4" style="padding-right: 0px;">
												<input aria-labelledby="lblRateMaturityDate" type="<%=TextAndroidTel%>" pattern="[0-9]*" placeholder="yyyy" id="txtRateMaturityDate3" onkeydown="limitToNumeric(event);" maxlength ='4' value="" />
											</div>
										</div>
									</div>	
								</div>	
							</div>
						</div>
					</div>
				</fieldset>
				<br/>
				<div style="text-align: center;">
					<a href="#" data-role="button" data-mode="self-handle-event" type="button" data-main-page="pagesubmit" data-inline="true" style="min-width: 150px;" id="btnAcceptRate">Accept</a>
					<input type="hidden" value="" data-place-holder="productCode" />
					<input type="hidden" value="" data-place-holder="instanceId" />
				</div>    
			</div>
		</div>
		<div data-role="dialog" data-close-btn="none" id="fundingInternalTransfer">
			<div data-role="header" data-position="" style="display: none;">
				<h1>Fund via Internal Fund</h1>
			</div>
			<div data-role="content">
				<div class="row">
					<div class="col-xs-12 header_theme2" style="padding-right: 0px;">
						<a href="#" onclick="goBackFInternalTransfer(this)" class="pull-right svg-btn"><%=HeaderUtils.IconClose %><span style="display: none;">close</span></a>
					</div>
				</div>
				<div data-placeholder="content"></div>
				<div class="div-continue" style="text-align: center;">
					<a href="#" data-transition="slide" onclick="validateFInternalTransfer(this, true);" type="button" data-role="button" class="div-continue-button">Done</a> 
					<a href="#divErrorDialog" style="display: none;">no text</a>
					<%--Or <a href="#" onclick="goBackFInternalTransfer(this)" class ="div-goback" data-corners="false" data-shadow="false" data-theme="reset"><span class="hover-goback"> Go Back</span></a>   --%>
				</div>
			</div>
		</div>
		<div data-role="dialog" data-close-btn="none" id="fundingCreditCard">
			<div data-role="header" data-position="" style="display: none;">
				<h1>Fund via Credit Card</h1>
			</div>
			<div data-role="content">
				<div class="row">
					<div class="col-xs-12 header_theme2" style="padding-right: 0px;">
						<a href="#" onclick="goBackFCreditCard(this)" class="pull-right svg-btn"><%=HeaderUtils.IconClose %><span style="display: none;">close</span></a>
					</div>
				</div>
				<div data-placeholder="content" style="overflow: auto;padding: 4px;"></div>
				<div class ="div-continue" style="text-align: center;">
					<a href="#" data-transition="slide" onclick="validateFCreditCard(this);" type="button" data-role="button" class="div-continue-button">Done</a> 
					<a href="#divErrorDialog" style="display: none;">no text</a>
					<%--Or <a href="#" onclick="goBackFCreditCard(this)" class ="div-goback" data-corners="false" data-shadow="false" data-theme="reset"><span class="hover-goback"> Go Back</span></a>--%>   
				</div>
			</div>
		</div>
		<div id="popUpUndefineCard" data-role="popup" style="max-width: 500px; padding: 20px;">
			<div data-role="header" style="display:none" >
					<h1>Alert Popup</h1>
			</div>
			<div data-role="content">
				<div class="DialogMessageStyle">There was a problem with your request</div>
				<div style="margin: 20px 0 30px; font-weight: normal;" data-place-holder="content"></div>
			</div>
			<div class="div-continue js-no-required-field" style="text-align: center;margin-bottom: 30px;">
				<a href="#" data-transition="slide" type="button" onclick="closePopup('#popUpUndefineCard')" data-role="button" class="div-continue-button" style="display: inline;">Let Me Fix</a>
				<a href="#" data-transition="slide" type="button" data-command="remove-product" data-role="button" class="div-continue-button" style="display: inline;" onclick="validateFCreditCard('#fundingCreditCard', true)">Continue</a>
			</div>
		</div>
		<div data-role="dialog" data-close-btn="none" id="fundingFinancialIns">
			<div data-role="header" data-position="" style="display: none;">
				<h1>Transfer from another finacial institution</h1>
			</div>
			<div data-role="content">
				<div class="row">
					<div class="col-xs-12 header_theme2" style="padding-right: 0px;">
						<a href="#" onclick="goBackFFinacialIns(this)" class="pull-right svg-btn"><%=HeaderUtils.IconClose %><span style="display: none;">close</span></a>
					</div>
				</div>
				<div data-placeholder="content"></div>
				<div class ="div-continue" style="text-align: center;">
					<a href="#" data-transition="slide" onclick="validateFFinacialIns(this, true);" type="button" data-role="button" class="div-continue-button">Done</a> 
					<a href="#divErrorDialog" style="display: none;">no text</a>
					<%--Or <a href="#" onclick="goBackFFinacialIns(this)" class ="div-goback" data-corners="false" data-shadow="false" data-theme="reset"><span class="hover-goback"> Go Back</span></a>   --%>
				</div>
			</div>
			<div id="popUpAccountGuide" data-role="popup" data-position-to="window">
				<div data-role="content">
					<div class="row">
						<div class="col-xs-12 header_theme2">
							<a data-rel="back" href="#" class="pull-right svg-btn"><%=HeaderUtils.IconClose %><span style="display: none;">close</span></a>
						</div>
					</div>
					<div class="row">
						<div class="col-sm-12">
							<div style="text-align: center; margin: 10px 0;">
								<img src="/images/SampleCC.jpg" alt="" style="width: 100%;"/>
							</div>     
						</div>    
					</div>
				</div>
			</div>
		</div>
		<%End If%>        
    
     <!--start wallet questions -->
     <div data-role="page" id="walletQuestions" >
         <div data-role="header" class="sr-only">
            <h1>Authentication Questions</h1>
        </div>
        <div data-role="content">
            <%=HeaderUtils.RenderLogoAndSteps(_LogoUrl, 3, "APPLY_CC")%>		
            <%=HeaderUtils.RenderPageTitle(18, "Authentication Questions", false)%>
			<div data-role="content">
                <div id="walletQuestionsDiv"></div> 	                  
			    <div class="div-continue" style="margin-top: 10px; text-align: center;" data-role="footer">
				    <a href="#" data-transition="slide" onclick="return validateWalletQuestions(this, false);" type="button" class="btnSubmitAnswer div-continue-button">Submit Answers</a> 
						<%--<input type="button" name="btnSubmitAnswer" value="Submit Answers" onclick="return validateWalletQuestions(this, false);" data-theme="<%=_FooterDataTheme%>"  class ="btnSubmitAnswer"  autocomplete = "off"/>	--%>
				</div>
			</div>
		</div>
    </div>
           <%-- jquery mobile need to transition to another page inorder to format the html correctly--%>
    <div data-role="page" id="co_walletQuestions">
	    <div data-role="header" class="sr-only">
            <h1>Co-Authentication Questions</h1>
        </div>
        <div data-role="content">
            <%=HeaderUtils.RenderLogoAndSteps(_LogoUrl, 3, "APPLY_CC")%>		
            <%=HeaderUtils.RenderPageTitle(18, "Authentication Questions", false)%>
			<div data-role="content">
                <div id="co_walletQuestionsDiv"></div>					
				<div class="div-continue" style="margin-top: 10px; text-align: center;" data-role="footer">
					<a href="#" data-transition="slide" onclick="return validateWalletQuestions(this, true);" type="button" class="co_btnSubmitAnswer div-continue-button">Submit Answers</a> 
					<%--<input type="button" name="co_btnSubmitAnswer" value="Submit Answers"  data-theme="<%=_FooterDataTheme%>" onclick="return validateWalletQuestions(this, true);" class ="co_btnSubmitAnswer"  autocomplete = "off"/>	--%>
				</div>
			</div>
		</div>
    </div>
      <!--end wallet questions -->
    <div data-role="page" id="vl_last">
        <div data-role="header" class="sr-only">
			<h1>Application Completed</h1>
        </div>
        <div data-role="content">
            <%=HeaderUtils.RenderLogoAndSteps(_LogoUrl, 3, "APPLY_CC")%>
            <div id="div_system_message" >
            </div>        
        <%		If Not String.IsNullOrEmpty(_RedirectURL) Then%>
        <div class="div-continue" style="margin-top: 10px; text-align: center;"  data-role="footer">
	        <a id="btnRedirect" href="#" onclick="gotoToUrl(this,'<%=_RedirectURL%>')" type="button" class="div-continue-button">Return to our website</a> 
            <%--<input data-theme="<%=_FooterDataTheme%>" type="button" id="btnRedirect" class="btn-redirect  div-continue-button" data-theme="<%=_FooterDataTheme%>" url='<%=_RedirectURL%>'
                value="Return to our website" />--%>
        </div>
        <%End If%> 
        </div>
    </div>
    <div id="divErrorDialog" data-role="dialog" style="min-height: 600px;">
        <div role="dialog">
             <div data-role="header" style="display:none" >
					  <h1  >Alert Popup</h1>
			 </div>
            <div data-role="content">
                  <div class="DialogMessageStyle">There was a problem with your request</div>
                <div style="margin: 5px 0 15px 0;">
                    <span id="txtErrorMessage"></span>
                </div>
                <a href="#" data-role="button" data-rel="back" type="button">Ok</a>
            </div>
        </div>
    </div>
	<div id="divErrorPopup" data-role="popup" data-dismissible="true" data-history="false" style="max-width: 500px;padding: 20px;">
		<div data-role="header" style="display:none" >
				<h1  >Alert Popup</h1>
		</div>
		<div data-role="content">
			<%--<div class="row">
				<div class="col-xs-12 header_theme2">
					<a data-rel="back" class="pull-right"><%=HeaderUtils.IconClose %></a>
				</div>
			</div>--%>
			<div class="row">
				<div class="col-sm-12">
					<div class="DialogMessageStyle">There was a problem with your request</div>
					<div style="margin: 5px 0 15px 0;">
						<span id="txtErrorPopupMessage"></span>
					</div>       
				</div>    
			</div>
			<div class="row text-center">
				<div class="col-xs-12 text-center">
					<a href="#" data-role="button" data-rel="back" type="button">Ok</a>
				</div>
			</div>
		</div>
	</div>
	<div id="popNotification" data-role="popup" data-dismissible="true" data-history="false" style="max-width: 500px;padding: 20px;">
		<div data-role="header" style="display:none" >
				<h1  >Notification Popup</h1>
		</div>
        <div data-role="content">
            
            <div class="row">
                <div class="col-sm-12">
	                <div class="DialogMessageStyle header_theme">Notification</div>
                    <div style="margin: 5px 0 15px 0;" placeholder="content"></div>       
                </div>    
            </div>
			<div class="row text-center">
				<div class="col-xs-12 text-center">
					<a href="#" data-role="button" onclick="closePopup('#popNotification')" type="button">Ok</a>
				</div>
			</div>
        </div>
    </div>
     <div id="divMessage" data-role="dialog" style="min-height: 444px;">
        <div role="dialog">
			<div data-role="header" style="display:none">
                <h1 class="ui-title" role="heading" aria-level="1">
					Information Popup    
                 </h1>
            </div>
            <div data-role="content" role="main"><br />
                <div style="margin: 5px 0 15px 0;">
                    <span id="txtMessage" style="color: Blue;"></span>
                </div>
                <br />
				<a href='#' data-role='button' type="button" id='OKButton'>Ok</a>
               <%--<button id ="OKButton" type ="button" type="button"><b>Ok</b> </button><br />--%>
            </div>
        </div>
    </div>
   <div data-role="page" id="decisionPage">
        <%--<div data-role="header" data-theme="<%=_HeaderDataTheme%>" data-position="">
          <h1>User Declined</h1>
            </div> --%>
        <div data-role="content">
          <%=HeaderUtils.RenderLogoAndSteps(_LogoUrl, 3, "APPLY_CC")%>
          <%=HeaderUtils.RenderPageTitle(19, "Loan Submission Cancelled", False)%>
         <div style="padding:10px 10px 10px 10px;background-color:yellow;margin-bottom:15px;margin-top :10px;" class="rename-able"><%If Not String.IsNullOrEmpty(_SubmitCancelMessage) Then%><%=_SubmitCancelMessage%><%Else%>We're sorry you have declined to use our automated prequalification system. If you prefer, please come to our offices to apply or call our Consumer Loan Department and loan representative will be happily ready to assist you.<%End If%></div>
         <div id="divRedirectURL">
            <% If Not String.IsNullOrEmpty(_RedirectURL) Then%>
                <div  style="margin:10px 0px 10px 0px" class="RedirectUrlPage">
                <a href ="<%=_RedirectURL%>"  class="header_theme2 shadow-btn chevron-circle-right-before" style="font-size :18px" onclick="VL.FACTORY.onClickReturnToMainPage(event);">Return to Main Page</a></div> 
            <div class="rename-able">If you decide you would like to go through our instant decision process, please click on the following link to return to your application summary page and submit the application for decisioning.</div>
            <div style="margin-top:5px" class="RedirectUrlPage"><a class="header_theme2 shadow-btn chevron-circle-right-before" style="font-size :18px" id="btnReturnToApp" href ="#pagesubmit">Return to Application</a></div> 
             <%End If%>
        </div>
        </div>
     </div>
     <div data-role="dialog" id="divConfirmDialog">
        <div data-role="header" style="display:none" ><h1>Restart Popup</h1></div>
        <div data-role="content">                 
			<div style='margin: 5px 0 15px 0;'><span class="require-span">Restart will clear all data and return to the main page. Are you sure?</span></div>
			<div class='row' style="text-align: center;margin-top: 40px; margin-bottom: 10px;">
				<a href='#' type="button" style="width: 100px; display: inline;padding-left: 20px; padding-right: 20px;" data-role='button' id='btnOk'>Yes</a>
				<a href='#' type="button" style="width: 100px;display: inline;padding-left: 20px; padding-right: 20px;" data-role='button' data-rel='back' id='btnCancel'>No</a>
			</div>
        </div>
    </div>
		
	<div data-role="page" id="pageMaxAppExceeded">
			<div data-role="header" class="sr-only">
				<h1>Access Denied</h1>
			</div>
		  <div data-role="content">
				<%=HeaderUtils.RenderLogo(_LogoUrl)%>
			  <br/>
			  <br/>
			  <div>You have exceeded the maximum number of applications per hour. Please try again later.</div>
			  <br/>
				<div class="div-continue" style="margin-top: 10px; text-align: center;" data-role="footer">
					<a href="#" onclick="gotoToUrl(this,'<%=_RedirectURL%>')" type="button" class="div-continue-button">Return to our website</a>
				</div>
		   </div>
	 </div>
     <!--footer logo --> 
   <%--<div class = "divfooter  ui-bar-<%=_FooterDataTheme%>" > 
             <%=_strRenderFooterContact%> 

           <img  id="hdLogoImage" src="<%=_strLogoUrl%>" alt ="" style="display :none;"/>
            <div class="divfooterleft"><div class="divfooterleft-content"><%=_strFooterLeft  %></div></div>          
            <div class="divfooterright">
                <% If String.IsNullOrEmpty(_hasFooterRight) Then%>
                    <div class="divfooterright-content">
                        <div class="divfooterright-content-style"><a href="<%=_strNCUAUrl%>"  class="divfooterright-content-color ui-link" ><%=_strFooterRight%></a></div></div>
                    <div class="divfooterright-logo">
                         <div class="divfooterright-logo-style"><a href="<%=_strHUDUrl%>" class="ui-link"> <img src="<%=_strLogoUrl%>" alt ="" id="logoImage" class="footerimage-style" /></a></div></div> 
                    <div style ="clear:both"></div> 
                <%Else%>
                    <%=_strFooterRight%>
                    <div style ="clear:both"></div> 
                <%End If%>
            </div>
          <div style ="clear:both"></div>            
   </div>--%>
  <!--ui new footer content --> 
       <div class="divfooter">
           <%=_strRenderFooterContact%>  
            <div style="text-align :center">
                 <!-- lender name -->
                <div class="divLenderName"><%=_strLenderName%></div>  
                <div class="divFooterLogo">          
                    <% If String.IsNullOrEmpty(_hasFooterRight) Then%>
                        <%-- 
                         <div><a href="<%=_strNCUAUrl%>"  class="ui-link" ><%=_strFooterRight%></a><a href="<%=_strHUDUrl%>" class="ui-link"> <img src="<%=_strLogoUrl%>" alt ="logo" class="footerimage-style img-responsive" /></a></div>                 
                         --%> 
                        <div class="divfooterlabel"><a href="<%=_strNCUAUrl%>"  class="ui-link" ><%=_strFooterRight%></a> <a href="<%=_strHUDUrl%>" class="ui-link"><%=_equalHousingText%></a></div>                                      
                     <% Else%>
                        <%=_strFooterRight%>
                    <%End If%>
                </div>  
                <div><%=_strFooterLeft  %></div>   
           </div>
            <%-- <div class = "container">            
                 <div class="row divfooter_padding">
                     <!-- left footer content -->
                      <div class="col-sm-7 col-md-7 divfooterleft ">
                          <div class="footerTextAlign">
                          <%=_strFooterLeft  %>
                          </div>
                      </div>
                     <!--right footer content -->
                     <div class="col-sm-5 col-md-5 divfooterright">
                         <div class="row">
                             <% If String.IsNullOrEmpty(_hasFooterRight) Then%>
                                 <div class="col-sm-9 col-md-10 divfooterright-content footerTextAlign">
                                     <a href="<%=_strNCUAUrl%>"  class="ui-link" ><%=_strFooterRight%></a>
                                 </div>
                                 <div class="col-sm-3 col-md-2 divfooterright-logo footerTextAlign">
                                      <a href="<%=_strHUDUrl%>" class="ui-link"> <img src="<%=_strLogoUrl%>" alt ="" class="footerimage-style img-responsive" /></a>
                                 </div>
                             <% Else%>
                                    <%=_strFooterRight%>
                             <%End If%>
                         </div>
                     </div>
                 </div>
            </div> 
            --%>
       </div> 
         <!--end new ui footer content -->
  </div>
	
	<uc:pageFooter ID="ucPageFooter" runat="server" />
	<%If IsComboMode Then%>
		<%:Scripts.Render("~/js/thirdparty/funding")%>
		<%If _showProductSelection Then%>
		<%:Scripts.Render("~/js/thirdparty/product")%>
		<%End If%>
	<%End If%>
	<%If LoanSavingItem IsNot Nothing andalso LoanSavingItem.Status = "INCOMPLETE" AndAlso not String.IsNullOrEmpty(LoanSavingItem.RequestParamJson) Then%>
	<script type="text/javascript">
		$(function() {
			var savedData = <%=LoanSavingItem.RequestParamJson%>;
			bindSavedData(savedData);
		});
	</script>
	<%End If %>
</body>
</html>
