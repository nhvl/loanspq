﻿CrossDomainServices
These services are needed for working around network blockage and CORS.

1) This folder is to be setup as a website on a different server that has internet access. 
2) To enable CORS
	a) The domain for this service should be the same as the domain for calling webpage
    b) Server setup
for IIS7 add this key to web.config
   <add name="Access-Control-Allow-Origin" value="*" />

for IIS6 add this key via IIS GUI CUSTOM HEADERS tab    

other server please refer to this site:
   http://enable-cors.org/server.html

3)  Add CrossDomainServicesUrl key to web.config of the calling website.
<add key="CrossDomainServicesUrl" value="https://mlapp.loanspq.com/crossdomainservices" />

for local testing
<add key="CrossDomainServicesUrl" value="http://lpqmobile.localhost/crossdomainservices/" />


4) If there is too much staled pdf files, you can delete all PDF files from website/PDFViewer/PDF/
or use window scheduler with PDFFilesRefresh.aspx
 

-----
----Calling work flow
--Check crossdowmain/connection issue
1.(local)/Inc/PageHeader.js call (local)/PdfWiewer/web/CrossDomainChecker.aspx handler 
2.  (local)/PdfWiewer/CrossDomainChecker.aspx handler call (remote)/CrossDomainServices/CrossDomainChecker.aspx
3. (remote)/CrossDomainServices/CrossDomainChecker.aspx return "OK", Crossdomain", or "Error" 


If frameable
1. .(local)/Inc/PageHeader.js call  chooseOpeningMethod which call common.js/gotoPDFViewer
2. common.js/gotoPDFViewer call (local)/PdfViewer/Web/Downloader.aspx handleer
3. (local)/PdfViewer/Web/Downloader.aspx handleer call (remote)/CrossDomainServices/FileDownloader.
4.  (remote)/CrossDomainServices/FileDownloader.aspx returns the url to the PDF which is on the same domain as the host