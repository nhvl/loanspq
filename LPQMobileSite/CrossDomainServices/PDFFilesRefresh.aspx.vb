﻿
''' <summary>
''' This service acts as a middleware to allow remote server to call TempPdfFilesCleaner.exe.  This file should be in the same folder as this service page
''' </summary>
''' <remarks></remarks>

Partial Class TempPdfFilesCleaner_PDFFilesRefresh
    Inherits System.Web.UI.Page
	Private _log As log4net.ILog = log4net.LogManager.GetLogger(Me.GetType)
	Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
		Dim lenderref As String = Request.Params("lenderref")
		If lenderref = "" Then
			Response.Write("Missing Lenderref")
			Return
		End If

		' Since the lenderref is destined for being included in a command line operation,
		' apply the strict validation used at lenderref creation
		If Me.IsValidLenderRef(lenderref) = False Then
			Response.Write("Bad Lenderref")
			Return
		End If

		Dim sLookUpPrefix = Left(lenderref, 6)

		Try
			'cleanup pdf files that belong to this lender, can't access the file from production server
			_log.Debug("Start deleting cached pdf")
			Dim sExeFile As String = HttpContext.Current.Server.MapPath("TempPdfFilesCleaner.exe")
			System.Diagnostics.Process.Start(sExeFile, sLookUpPrefix)
			Response.Write("Done")
		Catch ex As Exception
			_log.Error("Could not ClearPDF", ex)
		End Try
	End Sub

	''' <summary>
	''' Validates that a lenderref for a new portal or for updating the lenderref of an existing portal
	''' is safe to use. This may also be used in high-security contexts for various purposes, but may not
	''' necessarily be used per-request.
	''' </summary>
	''' <remarks>Keep syncrhonized with Common.vb:IsValidLenderRef</remarks>
	''' <param name="lenderref"></param>
	''' <returns>If the new lenderref is valid.</returns>
	Public Function IsValidLenderRef(ByVal lenderref As String) As Boolean
		' Allow in the file name:
		' - Any unicode letter
		' - Any unicode number
		' - Any unicode marker
		' - Underscores and hyphens
		' Otherwise, it gets stripped. This would include characters that are unsafe
		' for SQL Or Windows Command Line, or other areas.
		' https://www.regular-expressions.info/unicode.html
		Return Regex.IsMatch(lenderref, "^[\p{L}\p{N}\p{M}_\-]{6,50}$")
	End Function
End Class
