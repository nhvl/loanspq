﻿
Imports System.Net
Imports System.IO

''' <summary>
''' This service will dowdload PDF file to local dir based on file parameter.  It return either "Error" or a new url pointed to the local pdf file.
''' This handler is needed because the local PDF viewer need the PDF file to be resided in the same domain
''' </summary>

Partial Class FileDownloader
    Inherits System.Web.UI.Page
    Private _log As log4net.ILog = log4net.LogManager.GetLogger(Me.GetType)

    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        If IsPostBack = False Then
            Try
                If Request.Params("file") Is Nothing Then
                    Throw New Exception("Parameter 'file' is required.")
                End If
                If Request.Params("lenderref") Is Nothing Then
                    Throw New Exception("Parameter 'lenderref' is required.")
                End If

				Dim pdfFolderPath As String = Server.MapPath("/") + "PdfViewer\Pdf\"

                If Directory.Exists(pdfFolderPath) = False Then
                    Directory.CreateDirectory(pdfFolderPath)
                End If

                Dim lenderref As String = Request.Params("lenderref")
				Dim fileUrl As String = Request.Params("file")

				' Sanitize the URL
				Try
					Dim fileUrlUri As Uri = New Uri(fileUrl)
					Dim query As NameValueCollection = HttpUtility.ParseQueryString(fileUrlUri.Query)
					Dim ubFileUrl As UriBuilder = New UriBuilder(fileUrlUri)

					query.Remove("_ga") ' Remove any query parameters known to cause issues.
					ubFileUrl.Fragment = Nothing ' Remove any hash fragments. None are useful here.

					' Re-encode the query and re-construct the URL
					ubFileUrl.Query = String.Join("&", query.AllKeys.Select(Function(k) k & "=" & HttpUtility.UrlEncode(query.Get(k))))
					fileUrl = ubFileUrl.Uri.ToString()
				Catch ex As Exception

				End Try

				Dim fileName As String = Path.GetFileName(fileUrl) 'deprecated

				If Not IsLegitURL(fileUrl) Then
					_log.Warn("Not a legit URL: " & fileUrl)
					Response.Write("Error")
					HttpContext.Current.ApplicationInstance.CompleteRequest()
					Return
				End If

				_log.Debug("Start downloader for url: " & fileUrl & " from lender: " & lenderref)

                'window has a file name limitation of 260 characters.
                fileName = Right(fileUrl, 150) & ".pdf"

                'remove special character from filename
                Dim rgPattern = "[\\\/:\*\?""'<>|%]"
				Dim objRegEx As New Regex(rgPattern)
				fileName = objRegEx.Replace(fileName, "")

                fileName = Left(lenderref, 80) + "_" + fileName 'make it more unique, file still need to be uniqu within the same lender 

                Dim filePath = pdfFolderPath + fileName

                'return error if DocViewer link is NOT a PDF
                'the file is created the 1st time but for 2nd request, the response will be an error
                If fileUrl.ToUpper.Contains("DOCVIEWER.ASPX") Then
					If File.Exists(filePath + "nonpdf") Then
                        Response.Write("Error")
                        HttpContext.Current.ApplicationInstance.CompleteRequest()
						Return
					End If
				End If

				If File.Exists(filePath) = False Then
					_log.Debug("Start download for url: " & fileUrl & " from lender: " & lenderref)
					Dim webClient As New WebClient()
					ServicePointManager.ServerCertificateValidationCallback = (Function(this, certificate, chain, sslPolicyErrors) True)
					ServicePointManager.SecurityProtocol = SecurityProtocolType.Ssl3 Or SecurityProtocolType.Tls Or SecurityProtocolType.Tls11 Or SecurityProtocolType.Tls12

					webClient.DownloadFile(fileUrl, filePath)

					'special case, docviewer link may contain non PDF
					If fileUrl.ToUpper.Contains("DOCVIEWER.ASPX") Then '
						Dim IsPDF As Boolean = False
						Dim myWebHeaderCollection As WebHeaderCollection = webClient.ResponseHeaders
						For i = 0 To myWebHeaderCollection.Count - 1
							'_log.Info((ControlChars.Tab + myWebHeaderCollection.GetKey(i) + " " + ChrW(61) + " " + myWebHeaderCollection.Get(i))
							If myWebHeaderCollection.Get(i).Contains("pdf") Then 'looking for:  Content-Type = application/pdf
								IsPDF = True
								Exit For
							End If
						Next i
						If Not IsPDF Then 'need to store the bogus file as a signature for future check
							Rename(filePath, filePath + "nonpdf")
							Response.Write("Error")
							HttpContext.Current.ApplicationInstance.CompleteRequest()
							Return
						End If
					End If
				Else
					_log.Debug("Skip download for url: " & fileUrl & " from lender: " & lenderref)
				End If

				Dim port As String
				If (Request.Url.Port = 80) Then
					port = ""
				Else
					port = ":" + Request.Url.Port.ToString()
				End If

                _log.Info("Return Url: " & Request.Url.Scheme + "://" + Request.Url.Host + port + "/pdfviewer/pdf/" + fileName)
                Response.Clear()
				Response.Write(Request.Url.Scheme + "://" + Request.Url.Host + port + "/pdfviewer/pdf/" + fileName)
				HttpContext.Current.ApplicationInstance.CompleteRequest()
			Catch ex As Exception
				_log.Error("Error download file " & ex.Message, ex)
				If (ex.Message <> "Thread was being aborted.") Then
					Response.Write("Error")
				End If
			End Try
        End If
	End Sub

	Private Function IsLegitURL(ByVal psURL As String) As Boolean
		'only allow https from other domain
		Dim sRUL As String = psURL.Trim

		If sRUL.Length < 15 Then Return False

		If sRUL.ToLower.IndexOf("http", 0) = -1 Then Return False

		Return True
	End Function
End Class
