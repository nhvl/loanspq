﻿Option Strict Off

Imports System.Net
Imports System.Net.Security
Imports System.Security.Cryptography.X509Certificates

''' <summary>
''' This CrossDomainChecker service need to be hosted on server that has internet access.  
''' The service will check the source content header.  This header will be use to determine if content can be used in an iFrame by mobile website
''' </summary>
''' <remarks></remarks>
Partial Class CrossDomainChecker
    Inherits System.Web.UI.Page
    Private _log As log4net.ILog = log4net.LogManager.GetLogger(Me.GetType)
	Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load

		If IsPostBack = False Then
			Try
				If Request.Params("check") Is Nothing Then
					Throw New Exception("Parameter 'check' is required.")
				End If

				Dim url As String = Request.Params("check")

				' Sanitize the URL
				Try
					Dim fileUrlUri As Uri = New Uri(url)
					Dim query As NameValueCollection = HttpUtility.ParseQueryString(fileUrlUri.Query)
					Dim ubFileUrl As UriBuilder = New UriBuilder(fileUrlUri)

					query.Remove("_ga") ' Remove any query parameters known to cause issues.
					ubFileUrl.Fragment = Nothing ' Remove any hash fragments. None are useful here.

					' Re-encode the query and re-construct the URL
					ubFileUrl.Query = String.Join("&", query.AllKeys.Select(Function(k) k & "=" & HttpUtility.UrlEncode(query.Get(k))))
					url = ubFileUrl.Uri.ToString()
				Catch ex As Exception

				End Try

				Dim lenderref = Request.Params("lenderref")
				Dim isCrossDomain As Boolean = False

				_log.Debug("CrossDomain Check for: " & lenderref & ";  " & url)
				Dim httpRequest As HttpWebRequest = HttpWebRequest.Create(url)
				' FIX: Intermittent "Underlying connection was closed" error
				httpRequest.KeepAlive = False
				ServicePointManager.ServerCertificateValidationCallback = (Function(this, certificate, chain, sslPolicyErrors) True)
				ServicePointManager.SecurityProtocol = SecurityProtocolType.Ssl3 Or SecurityProtocolType.Tls Or SecurityProtocolType.Tls11 Or SecurityProtocolType.Tls12

				'ServicePointManager.ServerCertificateValidationCallback += ValidateRemoteCertificate()
				Dim webResponse As WebResponse = httpRequest.GetResponse()
				Dim frameOption As String = webResponse.Headers("X-Frame-Options")
				_log.Debug("Done CrossDomain Check for: " & lenderref & ";  " & url & "; " & frameOption)

				Dim port As String
				If (Request.Url.Port = 80) Then
					port = ""
				Else
					port = ":" + Request.Url.Port.ToString()
				End If

				Dim origin = Request.Url.Scheme + "://" + Request.Url.Host + port

				Select Case frameOption
					Case "DENY"
						isCrossDomain = True

					Case "SAMEORIGIN"
						If url.ToLower().IndexOf(origin.ToLower()) <> 0 Then
							isCrossDomain = True
						End If
				End Select

				If isCrossDomain Then
					Response.Write("CrossDomain")
					HttpContext.Current.ApplicationInstance.CompleteRequest()
				Else
					Response.Write("OK")
					HttpContext.Current.ApplicationInstance.CompleteRequest()
				End If
			Catch ex As Exception
				_log.Error("CrossDominaChecker err: " & ex.Message, ex)
				Response.Write("Error: " & ex.Message)
				HttpContext.Current.ApplicationInstance.CompleteRequest()
			End Try
		End If
	End Sub

    Dim IgnoreSslErrors As Boolean = True
    Private Function ValidateRemoteCertificate(sender As Object, certificate As X509Certificate, chain As X509Chain, policyErrors As SslPolicyErrors) As Boolean
        Return IgnoreSslErrors OrElse policyErrors = SslPolicyErrors.None
    End Function

End Class
