﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="PageLoading.aspx.vb" Inherits="PageLoading" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Loading...</title>
    <script type="text/javascript" src="<%=ServerRoot%>/js/spin.min.js"></script>
    <script type="text/javascript">
        function Redirect() {
            setTimeout(function () { window.location.replace('<%=Url%>' + "&l=1"); }, 500);
        }
    </script>
    <style type="text/css">
        body {
             margin: 0;
             color: rgb(86, 94, 150);
             font-family: Roboto-light, sans-serif;
             font-weight: lighter;
         }
         .loading-container {
             display: table;
             position: absolute;
             height: 100%;
             width: 100%;
         }

        .loading-middle {
            display: table-cell;
            vertical-align: middle;
        }
        .loading-div {
            margin-left: auto;
            margin-right: auto;
            text-align: center;
        }
    </style>
</head>
<body onload="Redirect()">
    <div class="loading-container">
        <div class="loading-middle">
            <div class="loading-div">
                <div id="spinner"></div>
                <div style="margin-top: 150px; text-align: center;">
                    <h3 style="margin-left: 10px; font-size: 1em;">Loading... </h3>
                </div>
            </div>
        </div>
    </div>
</body>

<script type="text/javascript">
    var opts = {
        lines: 11 // The number of lines to draw
        , length: 0 // The length of each line
        , width: 20 // The line thickness
        , radius: 30 // The radius of the inner circle
        , scale: 1 // Scales overall size of the spinner
        , corners: 1 // Corner roundness (0..1)
        , color: '#0062A1' // #rgb or #rrggbb or array of colors
        , opacity: 0.25 // Opacity of the lines
        , rotate: 0 // The rotation offset
        , direction: 1 // 1: clockwise, -1: counterclockwise
        , speed: 1 // Rounds per second
        , trail: 60 // Afterglow percentage
        , fps: 20 // Frames per second when using setTimeout() as a fallback for CSS
        , zIndex: 2e9 // The z-index (defaults to 2000000000)
        , className: 'spinner' // The CSS class to assign to the spinner
        , top: '50%' // Top position relative to parent
        , left: '50%' // Left position relative to parent
        , shadow: false // Whether to render a shadow
        , hwaccel: false // Whether to use hardware acceleration
        , position: 'absolute' // Element positioning
    }
    var target = document.getElementById('spinner');
    var spinner = new Spinner(opts).spin(target);
</script>
</html>
