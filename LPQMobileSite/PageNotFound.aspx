﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="PageNotFound.aspx.vb" Inherits="PageNotFound" %>
<%@ Import Namespace="System.Web.Optimization" %>
<%@ Register TagPrefix="uc1" TagName="Piwik" Src="~/Inc/Piwik/PiwikTracking.ascx" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1 user-scalable=no"/>
    <title>Page Not Found</title>
	<link rel="shortcut icon" href="https://app.loanspq.com/logo/lpq_favicon.ico"  type="image/ico"  />
	<%:Styles.Render("~/css/smcss")%>
</head>
<body>
	<div class="container no-access-page">
	    <div class="row top30">
			<div class="col-xs-10 col-xs-offset-1 col-md-6 col-md-offset-3 main-form">
				<div style="float: left">
					<img src="/images/404.png" class="img-responsive"/>
				</div>
				<div style="float: left">
					<h2>OOPS! PAGE NOT FOUND</h2>
					<p>Looks like the page your are looking for cannot be found.</p>
					<p>Contact administrator for more information.</p>
					<%--<a href="/logout.aspx" class="btn btn-primary btn-lg active" role="button">Login with another credential</a>--%>
				</div>
			</div>
		</div>
    </div>
	<%:Scripts.Render("~/js/smscript")%>
	<uc1:Piwik id="ucPiwik" runat="server" ></uc1:Piwik>
</body>
</html>

