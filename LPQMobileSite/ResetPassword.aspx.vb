﻿Imports LPQMobile.Utils
Imports Org.BouncyCastle.Asn1.Utilities

Partial Class Sm_ResetPassword
	Inherits System.Web.UI.Page
	Protected tokenStr As String = ""
	Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
		If Not IsPostBack Then
			tokenStr = Common.SafeString(Request.QueryString("t"))
			If String.IsNullOrEmpty(tokenStr) Then
				Response.Redirect("/Login.aspx")
			Else
				Dim smAuthBL As New SmAuthBL()
				Dim tokenObj As SmUserActivateToken = smAuthBL.GetActivationTokenInfo(tokenStr)
				Dim expireExceed As Integer = Common.SafeInteger(ConfigurationManager.AppSettings("UserActivationTokenExpireAfter"))
				If tokenObj Is Nothing OrElse tokenObj.Status <> SmSettings.TokenStatus.Waiting OrElse tokenObj.CreatedDate.AddDays(expireExceed) < Now Then
					Response.Redirect("/Login.aspx")
				End If
				Dim usr As SmUser = smAuthBL.GetUserByID(tokenObj.UserID)
				If usr Is Nothing OrElse usr.Status = SmSettings.UserStatus.Disabled Then
					' disabled user is not allowed to forgot/resetpassword
					Response.Redirect("/Login.aspx")
				End If
			End If
		End If
	End Sub
End Class
