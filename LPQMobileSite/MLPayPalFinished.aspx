﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="MLPayPalFinished.aspx.vb" Inherits="MLPayPalFinished" %>
<%@ Register Src="~/Inc/PageHeader.ascx" TagPrefix="uc" TagName="pageHeader" %>
<%@ Register TagPrefix="uc" TagName="pageFooter" Src="~/Inc/PageFooter.ascx" %>
<%@ Register TagPrefix="uc1" TagName="Piwik" Src="~/Inc/Piwik/PiwikTracking.ascx" %>

<%@ Import Namespace="LPQMobile.Utils" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Paypal Finished</title>
   	<uc:pageHeader ID="ucPageHeader" runat="server" />
</head>
<body class="lpq_container">
      <%--<input type="hidden" id="hdFooterTheme" value="<%=_bgTheme %>" />
    <input type="hidden" id="hdBgTheme" value="<%=_BackgroundTheme%>" />--%>
    <div data-role="page" id="xa_last">
        <div data-role="header" data-position="">
            <h1 id="pageTitle" runat="server">Paypal Finished</h1>
        </div>
        <div data-role="content">
            <%=_OtherLogo%>
            <div style="text-align: center;"><%=_SystemMessage %></div>
            <div style="text-align: center;" id="divErrorMessage" runat="server">
                <p style="font: 8pt/11pt verdana; color: red;">
                    There is a problem with the page you are trying to reach and it cannot be displayed.<br />
                    <br />
                    You may want to close your browser and retry again.
                </p>
            </div>
            <div id="divCheckMailAddress" runat="server" style="text-align: left; display: none;">
                <br />
                <span><b>Mailing a check to fund your account? Please send the check to:</b></span>
                <br />
                <%=_CheckMailAddress%>
            </div>
            <%      If Not String.IsNullOrEmpty(_RedirectURL) Then%>
            <div style="margin-top: 10px; text-align: center;" class="div-continue" data-role="footer">
	            <a id="btnRedirect" href="<%=_RedirectURL%>" type="button" class="div-continue-button btn-redirect" style="height: auto;">Return to our website</a> 
                <%--<input type="button" id="btnRedirect" class="btn-redirect div-continue-button" url='<%=_RedirectURL%>' value="Return to our website" />--%>
            </div>
            <%    End If%>
        </div>
    </div>
   
    <%--<script type="text/javascript">
        $(function () {
            var footerTheme = $('#hdFooterTheme').val();
            var bgColor = $('.ui-bar-' + footerTheme).css('background-color');
            var changeColor = changeBackgroundColor(bgColor, .6);
            var bgTheme = $('#hdBgTheme').val();
            if (bgTheme != "") {
                changeColor = bgColor;
            }
            $('.ui-header').css('border-color', changeColor);
            $('body').css("background-color", changeColor);
        });

        function changeBackgroundColor(bgColor, alpha) {
            var strAlpha = ',' + alpha + ')';
            if (bgColor != undefined) {
                return bgColor.replace('rgb', 'rgba').replace(')', strAlpha);
            } else {
                return "";
            }
        }
    </script>--%>
	<uc1:Piwik ID="ucPiwik" runat="server"></uc1:Piwik>
	<uc:pageFooter ID="ucPageFooter" runat="server" />
</body>
</html>
