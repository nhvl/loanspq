﻿
Imports System.IO

Partial Class Downloader
	Inherits System.Web.UI.Page

	Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
		Dim tempFileID As String = Request.QueryString("tempFileID")
		If tempFileID = "" Then
			Return
		End If
		Dim fileName As String = Request.QueryString("fileName")
		If Not Regex.IsMatch(fileName, "[\/:*?<>|""]") Then
			Using oMemoryStream As MemoryStream = CType(Session(tempFileID), MemoryStream)
				With Response
					If Not .IsClientConnected Then
						Return
					End If

					.ClearContent()
					.ClearHeaders()
					.BufferOutput = False

					If fileName.Contains(".xls") Then
						.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"
					ElseIf fileName.Contains(".pdf") Then
						.ContentType = "application/pdf"
					ElseIf fileName.Contains(".csv") Then
						.ContentType = "text/csv"
					ElseIf fileName.Contains(".xml") Then
						.ContentType = "application/xml"
					End If
					.AddHeader("content-disposition", "attachment;filename=" & fileName)

					.Cache.SetCacheability(HttpCacheability.NoCache)
					.Cache.SetCacheability(HttpCacheability.Private)
					.Cache.SetCacheability(HttpCacheability.Public)
					.Cache.SetNoServerCaching()
					.Cache.SetRevalidation(HttpCacheRevalidation.AllCaches)
					.Expires = 0
					Response.OutputStream.Write(oMemoryStream.ToArray(), 0, oMemoryStream.ToArray().Length)
					Response.Flush()
					Response.End()
					oMemoryStream.Close()
				End With
			End Using
		End If

		Session(tempFileID) = Nothing
	End Sub
End Class
