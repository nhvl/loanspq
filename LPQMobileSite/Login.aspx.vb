﻿
Imports System.Web.Helpers
Imports LPQMobile.Utils
Imports System.Web.Security.AntiXss

Partial Class Sm_Login
	Inherits System.Web.UI.Page

	Protected UserName As String = String.Empty
	Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
		If Request.IsAuthenticated Then
			Response.Redirect(FormsAuthentication.GetRedirectUrl(User.Identity.Name, True), True)
		End If
		If IsPostBack Then
			Dim smAuthBL As New SmAuthBL
			UserName = AntiXssEncoder.HtmlEncode(Common.SafeString(Request.Form("UserName")), True)
			Dim password As String = Common.SafeString(Request.Form("Password"))
			Dim rememberMe As Boolean = Common.SafeBoolean(Request.Form("RememberMe"))
			Dim result As SmLoginResult = smAuthBL.ExecLogin(HttpContext.Current, UserName, password)
			If result.Result = SmSettings.LoginStatus.Success Then
				FormsAuthentication.SetAuthCookie(UserName, rememberMe)

				'Dim smBL As New SmBL()
				'If SmUtil.CheckRoleAccess(HttpContext.Current, SmSettings.Role.SuperOperator, SmSettings.Role.LegacyOperator, SmSettings.Role.Officer, SmSettings.Role.LenderAdmin, SmSettings.Role.PortalAdmin) Then
				'	Dim userRoleList = smBL.GetActiveUserRolesByUserID(result.UserInfo.UserID)
				'	If userRoleList IsNot Nothing AndAlso userRoleList.Count = 1 Then
				'		Dim backOfficeLender As SmBackOfficeLender = smBL.GetBackOfficeLenderByLenderConfigID(userRoleList.First().LenderConfigID)
				'		If backOfficeLender IsNot Nothing Then
				'			Response.Redirect(String.Format("~/sm/Index.aspx?id={0}", backOfficeLender.BackOfficeLenderID), True)
				'		End If
				'	End If
				'End If
				Response.Redirect(FormsAuthentication.GetRedirectUrl(UserName, rememberMe))
			Else
				lblResultMessage.Text = result.Message
				lblResultMessage.Visible = True
			End If
		
		End If
	End Sub

	

	Private _lenderrefWhitelist() As String = {"redwoodcu"}
	'return framebreaker code if cu is not in whitelist
	Public Function getFrameBreakerCode() As String
		Dim s As String = "<style id='antiClickjack'>body{display:none !important;}</style>" & _
			"<script type='text/javascript'>" & _
	  "if (self === top){var antiClickjack = document.getElementById('antiClickjack'); antiClickjack.parentNode.removeChild(antiClickjack);} else {top.location = self.location;}" & _
		"</script>"
		Dim sLenderRef As String = Request.Params("lenderref")
		If sLenderRef = "" Then Return s
		For Each lender As String In _lenderrefWhitelist
			If sLenderRef.ToLower.Contains(lender) Then
				Return Nothing
			End If
		Next

		Return s 'frame breaker code
	End Function


End Class
