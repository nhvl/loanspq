﻿
Imports Workflows.Base.BO
Imports Workflows.XA.BO
Imports LPQMobile.Utils

Partial Class MLPayPal
	Inherits CBasePage

	Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
		If Not IsPostBack Then
			Try
				Dim workFlowSessionID As String = Common.SafeString(Request.QueryString("wfsid"))
				If String.IsNullOrWhiteSpace(workFlowSessionID) Then
					Throw New ArgumentException("Missing Workflow SessionID")
				End If
				If String.IsNullOrEmpty(_CurrentWebsiteConfig.PayPalEmail) Then	'' just double check to avoid anonymous request
					log.Error("Lender: " + _CurrentLenderRef + " has invalid receiver paypal email")
					Throw New Exception("Invalid Email")
				End If
				'log.Debug("ML Paypal")
				Dim state As New XaState
				Dim cfg As New XaConfig(_LoanType, CustomListRuleList)
				cfg.WebsiteConfig = _CurrentWebsiteConfig
				cfg.SubmissionLimiterExemptIPs = SubmissionLimiterExemptIPs
				cfg.SubmissionMaxAppBeforeNotification = SubmissionMaxAppBeforeNotification
				cfg.SubmissionMaxAppBeforeBlocking = SubmissionMaxAppBeforeBlocking
				cfg.SubmissionLimiterInternalEmails = SubmissionLimiterInternalEmails
				cfg.CustomListRuleList = CustomListRuleList
				Dim storage As New Storage(Of XaSubmission, XaState, XaConfig)(cfg, state, workFlowSessionID)
				Dim wf As New Workflows.XA.XaPaypalWorkflow(Of XaSubmission, XaState, XaConfig)(HttpContext.Current, storage)
				wf.Start()
			Catch ex As Exception
				log.Error(ex.Message, ex)
			End Try
		End If
		
	End Sub
End Class


