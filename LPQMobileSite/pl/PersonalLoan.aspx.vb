﻿
Imports LPQMobile.BO
Imports LPQMobile.Utils
Imports System.IO
Imports System.Xml
Imports System.Web.Security.AntiXss
Partial Class pl_PersonalLoan
    Inherits CBasePage

    'Protected _LoanPurposeDropdown As String
	Protected _LoanPurposeList As Dictionary(Of String, CTextValueCatItem)
	Protected _LineOfCredit As Dictionary(Of String, CLineOfCreditItem)
    'Protected _LineOfCreditPurposeDropdown As String
    Protected _BranchDropdown As String
    Protected _address_key As String = ""
    ''Protected _visibleCustomQuestion As String
	'Protected _bgTheme As String
    Protected _hasScanDocumentKey As String
    Protected _SubmitCancelMessage As String
    Protected _LoanTermDropdown As String = ""
	Protected _enableJoint As Boolean = True
	'Protected _LineOfCreditVisibility As Boolean

	Protected _CreditPullMessage As String = ""
	Protected _showProductSelection As Boolean = True
	Protected _prerequisiteProduct As New CProduct
	Protected _CCMaxFunding As String = ""
	Protected _ACHMaxFunding As String = ""
	Protected _PaypalMaxFunding As String = ""
	Protected _ccStopInvalidRoutingNumber As String = ""
	Protected _InterestRate As Double = 0D
	Protected _branchOptionsStr As String = ""
	Protected LaserDLScanEnabled As Boolean = False
	Protected LegacyDLScanEnabled As Boolean = False
    Protected LoanSavingItem As SmLoan
    Protected _plLocationPool As Boolean = False
    Protected _plZipCodePoolProducts As List(Of DownloadedSettings.CPersonalLoanProduct)
    Protected _zipCodePoolList As New Dictionary(Of String, List(Of String))
	Protected _zipCodePoolNames As New Dictionary(Of String, String)
	Protected _declarationList As Dictionary(Of String, String)
#Region "property for sso"

	Protected ReadOnly Property LoanAmount() As String
		Get
			Return Common.SafeString(AntiXssEncoder.HtmlEncode(Request.Params("LAmount"), True))
		End Get
	End Property

	Protected ReadOnly Property Term() As String
		Get
			Return Common.SafeString(AntiXssEncoder.HtmlEncode(Request.Params("Term"), True))
		End Get
	End Property

	Protected ReadOnly Property LoanPurpose() As String
		Get
			Return Common.SafeString(AntiXssEncoder.HtmlEncode(Request.Params("LoanPurpose"), True))
		End Get
	End Property
	Protected ReadOnly Property LoCLoanPurpose() As String
		Get
			Return Common.SafeString(AntiXssEncoder.HtmlEncode(Request.Params("LoCLoanPurpose"), True))
		End Get
	End Property

	Protected ReadOnly Property IsLOC() As String
		Get
			Return Common.SafeString(AntiXssEncoder.HtmlEncode(Request.Params("IsLOC"), True))
		End Get
	End Property

#End Region

	Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

		log.Info("Personal Loan: Page loading")
		Session("StartedDate") = Date.Now	'this is use for keeping track the time when page first load which will be used to compare with submite_date
		ucPageHeader._currentConfig = _CurrentWebsiteConfig
		'ucCoUploadDocDialog.CurrentWebsiteConfig = _CurrentWebsiteConfig
		'ucCoUploadDocDialog.UploadLoandocPath = _uploadLoandocPath
		'ucUploadDocDialog.CurrentWebsiteConfig = _CurrentWebsiteConfig
		'ucUploadDocDialog.UploadLoandocPath = _uploadLoandocPath       
		'ucApplicantAddress.FooterDataTheme = _FooterDataTheme
		''driver license scan 
		DriverlicenseScan.CurrentConfig = _CurrentWebsiteConfig
		coAppDriverlicenseScan.CurrentConfig = _CurrentWebsiteConfig

		'Dim sTest As List(Of CListItem) = CDownloadedSettings.VehicleLoanPurposes(_CurrentWebsiteConfig)
		If Not IsPostBack Then
			If _ClinicID <> "" Xor _WorkerID <> "" Then	 'direct mode, no clinic config
				log.Warn("missing clinicid or workerid")
				Response.Write("both clinicid and workerid are required")
				Return
			End If

			If LoanPurpose <> "" Then  'purpose is in the url querystring para
				Dim parseLoanPurposes = _CurrentWebsiteConfig.GetEnumWithCatItems("PERSONAL_LOAN_PURPOSES")
				Dim sCategory = "", sText = ""
				If parseLoanPurposes.ContainsKey(LoanPurpose) Then
					sCategory = Common.SafeString(parseLoanPurposes(LoanPurpose).Category)
					sText = Common.SafeString(parseLoanPurposes(LoanPurpose).Text)
				End If
				If sText = "" Then sText = LoanPurpose
				_LoanPurposeList = New Dictionary(Of String, CTextValueCatItem)
				_LoanPurposeList.Add(LoanPurpose, New CTextValueCatItem() With {.Category = sCategory, .Text = sText, .Value = LoanPurpose})
			ElseIf _MerchantID <> "" Then  'using merchantid to load purpose from config
				Dim sXPathMerchant As String = String.Format("MERCHANTS/MERCHANT[@merchant_id='{0}']", _MerchantID)
				Dim oMerchantNode As XmlNode = _CurrentWebsiteConfig._webConfigXML.SelectSingleNode(sXPathMerchant)
				If oMerchantNode Is Nothing Then
					log.Warn("Merchant not found for merchantid: " & _MerchantID)
					Response.Write("invalid merchantid")
					Return
				End If
				Dim sXPathPurpose As String = String.Format("MERCHANTS/MERCHANT[@merchant_id='{0}']/LOAN_PURPOSES", _MerchantID)
				Dim clinicItems = _CurrentWebsiteConfig.GetClinicEnumItems(sXPathPurpose)
				If clinicItems IsNot Nothing AndAlso clinicItems.Any() Then
					_LoanPurposeList = clinicItems.ToDictionary(Function(x) x.Key, Function(x) New CTextValueCatItem() With {.Value = x.Key, .Text = x.Value, .Category = ""})
				End If
				If Not _LoanPurposeList.Any Then
					_LoanPurposeList = _CurrentWebsiteConfig.GetEnumWithCatItems("PERSONAL_LOAN_PURPOSES")	'default back to setting in normal loan
				End If
			Else
				_LoanPurposeList = _CurrentWebsiteConfig.GetEnumWithCatItems("PERSONAL_LOAN_PURPOSES")
			End If

			If _LoanPurposeList.Count > 0 Then
				hdHasLoanPurpose.Value = "Y"
			Else
				hdHasLoanPurpose.Value = "N"
			End If
			''set default value when there is only one value
			'If loanPurposes.Count = 1 Then
			'    Dim ddlLPkey(0) As String
			'    Dim ddlLPValue(0) As String
			'    loanPurposes.Keys.CopyTo(ddlLPkey, 0)
			'    loanPurposes.Values.CopyTo(ddlLPValue, 0)
			'    _LoanPurposeDropdown = Common.RenderDropdownlistWithEmpty(ddlLPkey, ddlLPValue, " ", "--Please Select--", ddlLPkey(0))
			'Else
			'    _LoanPurposeDropdown = Common.RenderDropdownlistWithEmpty(loanPurposes, " ", "--Please Select--")
			'End If

			''get address key
			If Not String.IsNullOrEmpty(_CurrentWebsiteConfig.AddressKey) Then
				_address_key = _CurrentWebsiteConfig.AddressKey
			End If

			If LoCLoanPurpose <> "" Then
				Dim parseLoCLoanPurposes = _CurrentWebsiteConfig.GetEnumLineOfCreditItems()
				Dim sCategory = "", sText = LoCLoanPurpose
				If parseLoCLoanPurposes.ContainsKey(LoCLoanPurpose) Then
					sCategory = Common.SafeString(parseLoCLoanPurposes(LoCLoanPurpose).Category)
					sText = Common.SafeString(parseLoCLoanPurposes(LoCLoanPurpose).Text)
				End If
				_LineOfCredit = New Dictionary(Of String, CLineOfCreditItem)
				_LineOfCredit.Add(LoCLoanPurpose, New CLineOfCreditItem() With {.Category = sCategory, .Text = sText, .Value = LoCLoanPurpose, .OverDraftOption = CEnum.LocOverDraftOption.BOTH_OVERDRAFT_AND_NON_OVERDRAFT})
			Else
				_LineOfCredit = _CurrentWebsiteConfig.GetEnumLineOfCreditItems()
			End If
			''set default value when there is only on value for loc
			'If lineOfCredits.Count = 1 Then
			'    Dim ddlLOCkey(0) As String
			'    Dim ddlLOCValue(0) As String
			'    lineOfCredits.Keys.CopyTo(ddlLOCkey, 0)
			'    lineOfCredits.Values.CopyTo(ddlLOCValue, 0)
			'    _LineOfCreditPurposeDropdown = Common.RenderDropdownlistWithEmpty(ddlLOCkey, ddlLOCValue, " ", "--Please Select--", ddlLOCkey(0))
			'Else
			'    _LineOfCreditPurposeDropdown = Common.RenderDropdownlistWithEmpty(lineOfCredits, " ", "--Please Select--")
			'End If

			'If lineOfCredits.Count = 0 Or lineOfCredits Is Nothing Then
			'    'IsLOCDiv.Visible = False
			'    _LineOfCreditVisibility = False
			'Else
			'    'IsLOCDiv.Visible = True
			'    _LineOfCreditVisibility = True
			'End If

			''loan term 
			Dim ddlLoanTerm As Dictionary(Of String, String) = _CurrentWebsiteConfig.GetEnumItems("PERSONAL_LOAN_TERM")
			If ddlLoanTerm.Count > 0 And Term = "" Then
				_LoanTermDropdown = Common.RenderDropdownlist(ddlLoanTerm, "")
			End If
			_InterestRate = GetInterestRate()

			Dim employeeOfLenderDropdown As String = ""
			Dim employeeOfLenderType = _CurrentWebsiteConfig.GetEnumItems("EMPLOYEE_OF_LENDER_TYPE")
			If employeeOfLenderType IsNot Nothing AndAlso employeeOfLenderType.Count > 0 Then    'available in  LPQMobileWebsiteConfig
				''get employee of lender type from config
				employeeOfLenderDropdown = Common.RenderDropdownlist(employeeOfLenderType, "")

				ucApplicantInfo.EmployeeOfLenderDropdown = employeeOfLenderDropdown
				ucCoApplicantInfo.EmployeeOfLenderDropdown = employeeOfLenderDropdown

				Dim sLenderName As String = CDownloadedSettings.DownloadLenderServiceConfigInfo(_CurrentWebsiteConfig).LenderName
				ucApplicantInfo.LenderName = sLenderName
				ucCoApplicantInfo.LenderName = sLenderName
			End If

			xaApplicationQuestionLoanPage.Header = HeaderUtils.RenderPageTitle(0, "Please answer the following question(s)", True)
			xaApplicationQuestionReviewPage.Header = HeaderUtils.RenderPageTitle(0, "Please answer question(s) below", True)

			ucApplicantID.MappedShowFieldLoanType = mappedShowFieldLoanType
			ucCoApplicantID.MappedShowFieldLoanType = mappedShowFieldLoanType
			ucApplicantAddress.MappedShowFieldLoanType = mappedShowFieldLoanType
			ucCoApplicantAddress.MappedShowFieldLoanType = mappedShowFieldLoanType
			ucReferenceInfo.MappedShowFieldLoanType = mappedShowFieldLoanType
			ucCoReferenceInfo.MappedShowFieldLoanType = mappedShowFieldLoanType
			ucApplicantInfo.MappedShowFieldLoanType = mappedShowFieldLoanType

			ucCoApplicantInfo.MappedShowFieldLoanType = mappedShowFieldLoanType
			ucAssetInfo.MappedShowFieldLoanType = mappedShowFieldLoanType
			ucCoAssetInfo.MappedShowFieldLoanType = mappedShowFieldLoanType
			ucApplicantInfo.isComboMode = IsComboMode
			ucCoApplicantInfo.isComboMode = IsComboMode
			ucApplicantInfo.InstitutionType = _InstitutionType
			ucCoApplicantInfo.InstitutionType = _InstitutionType
			Dim citizenshipStatusDropdown As String = ""
			Dim hasBlankDefaultCitizenship As Boolean = Common.hasBlankDefaultCitizenShip(_CurrentWebsiteConfig)
			If _CurrentWebsiteConfig.GetEnumItems("CITIZENSHIP_TYPES").Count > 0 Then  'available in  LPQMobileWebsiteConfig
				Dim Citizenships As New Dictionary(Of String, String)
				Citizenships = _CurrentWebsiteConfig.GetEnumItems("CITIZENSHIP_TYPES")
				citizenshipStatusDropdown = Common.DefaultCitizenShipFromConfig(Citizenships, _DefaultCitizenship, _CurrentLenderRef, hasBlankDefaultCitizenship)
			Else
				citizenshipStatusDropdown = Common.DefaultCitizenShip(_DefaultCitizenship, _CurrentLenderRef, hasBlankDefaultCitizenship) ''from hard code
			End If
			'Dim citizenshipStatusDropdown As String = Common.RenderDropdownlist(CEnum.CITIZENSHIP_STATUS, " ", "US CITIZEN")

			ucApplicantInfo.CitizenshipStatusDropdown = citizenshipStatusDropdown
			ucCoApplicantInfo.CitizenshipStatusDropdown = citizenshipStatusDropdown

			ucApplicantInfo.EnableCitizenshipStatus = CheckShowField("divCitizenshipStatus", "", True) Or (IsInMode("777") AndAlso IsInFeature("visibility"))
			ucCoApplicantInfo.EnableCitizenshipStatus = CheckShowField("divCitizenshipStatus", "co_", True) Or (IsInMode("777") AndAlso IsInFeature("visibility"))

			Dim maritalStatusDropdown As String = Common.RenderDropdownlistWithEmpty(CEnum.MARITAL_STATUS, "", "--Please Select--")
			ucApplicantInfo.MaritalStatusDropdown = maritalStatusDropdown
			ucCoApplicantInfo.MaritalStatusDropdown = maritalStatusDropdown
			ucApplicantInfo.EnableMaritalStatus = CheckShowField("divMaritalStatus", "", False) Or (IsInMode("777") AndAlso IsInFeature("visibility"))
			ucCoApplicantInfo.EnableMaritalStatus = CheckShowField("divMaritalStatus", "co_", False) Or (IsInMode("777") AndAlso IsInFeature("visibility"))
			ucApplicantInfo.EnableMemberNumber = CheckShowField("divMemberNumber", "", True) Or (IsInMode("777") AndAlso IsInFeature("visibility"))
			ucCoApplicantInfo.EnableMemberNumber = CheckShowField("divMemberNumber", "co_", True) Or (IsInMode("777") AndAlso IsInFeature("visibility"))
			ucApplicantInfo.EnableMemberShipLength = CheckShowField("divMembershipLength", "", False) Or (IsInMode("777") AndAlso IsInFeature("visibility"))
			ucCoApplicantInfo.EnableMemberShipLength = CheckShowField("divMembershipLength", "co_", False) Or (IsInMode("777") AndAlso IsInFeature("visibility"))

			Dim occupancyStatusesForRequiredHousingPayment = _CurrentWebsiteConfig.GetOccupancyStatusesForRequiredHousingPayment("PERSONAL_LOAN")

			Dim employmentStatusDropdown As String = Common.RenderDropdownlistWithEmpty(CEnum.EMPLOYMENT_STATUS, "", "--Please Select--")
			Dim liveMonthsDropdown As String = Common.GetMonths()
			''add ddlyears
			Dim liveYearsDropdown As String = Common.GetYears()
			ucFinancialInfo.EmploymentStatusDropdown = employmentStatusDropdown
			ucCoFinancialInfo.EmploymentStatusDropdown = employmentStatusDropdown
			ucFinancialInfo.LiveMonthsDropdown = liveMonthsDropdown
			ucCoFinancialInfo.LiveMonthsDropdown = liveMonthsDropdown
			''add ddlyears
			ucFinancialInfo.LiveYearsDropdown = liveYearsDropdown
			ucCoFinancialInfo.LiveYearsDropdown = liveYearsDropdown
			ucFinancialInfo.OccupancyStatusesForRequiredHousingPayment = occupancyStatusesForRequiredHousingPayment
			ucCoFinancialInfo.OccupancyStatusesForRequiredHousingPayment = occupancyStatusesForRequiredHousingPayment

			Dim occupyingLocationDropdown As String = Common.RenderDropdownlistWithEmpty(CEnum.OCCUPY_LOCATIONS, " ", "--Please Select--")
			Dim stateDropdown As String = Common.RenderStateDropdownlistWithEmpty(CEnum.STATES, "", "--Please Select--")
			ucApplicantAddress.OccupyingLocationDropdown = occupyingLocationDropdown
			ucCoApplicantAddress.OccupyingLocationDropdown = occupyingLocationDropdown
			ucApplicantAddress.OccupancyStatusesForRequiredHousingPayment = occupancyStatusesForRequiredHousingPayment
			ucCoApplicantAddress.OccupancyStatusesForRequiredHousingPayment = occupancyStatusesForRequiredHousingPayment
			ucApplicantAddress.StateDropdown = stateDropdown
			ucCoApplicantAddress.StateDropdown = stateDropdown
			ucApplicantID.StateDropdown = stateDropdown
			ucCoApplicantID.StateDropdown = stateDropdown
			ucApplicantAddress.LiveMonthsDropdown = liveMonthsDropdown
			ucCoApplicantAddress.LiveMonthsDropdown = liveMonthsDropdown
			''add ddlyears
			ucApplicantAddress.LiveYearsDropdown = liveYearsDropdown
			ucCoApplicantAddress.LiveYearsDropdown = liveYearsDropdown
			_previous_address_threshold = _CurrentWebsiteConfig.getPreviousThreshold("PERSONAL_LOAN", "previous_address_threshold")
			_previous_employment_threshold = _CurrentWebsiteConfig.getPreviousThreshold("PERSONAL_LOAN", "previous_employment_threshold")
			'ApplicantID

			If EnableIDSection Or EnableIDSection("co_") Then
				ucApplicantID.LoanType = _LoanType
				ucCoApplicantID.LoanType = _LoanType
				ucApplicantID.isComboMode = IsComboMode
				ucCoApplicantID.isComboMode = IsComboMode
				Dim ddlIDCardType As Dictionary(Of String, String)
				Dim _IDCardTypeDropDown As String
				If _CurrentWebsiteConfig.GetEnumItems("ID_CARD_TYPES").Count = 0 Then
					ddlIDCardType = CFOMQuestion.DownLoadIDCardTypes(_CurrentWebsiteConfig)
					If (ddlIDCardType.Count = 0) Then  'there is no id_card_type from retrieval list
						ucApplicantID.isExistIDCardType = False
						ucCoApplicantID.isExistIDCardType = False
					Else
						'ID_Card_type dropdown, default driver license
						ucApplicantID.isExistIDCardType = True
						ucCoApplicantID.isExistIDCardType = True
						_IDCardTypeDropDown = Common.RenderDropdownlist(ddlIDCardType, " ", "DRIVERS_LICENSE")
						ucApplicantID.IDCardTypeList = _IDCardTypeDropDown
						ucCoApplicantID.IDCardTypeList = _IDCardTypeDropDown
					End If
				Else
					'ID_Card_type dropdown, default driver license
					ucApplicantID.isExistIDCardType = True
					ucCoApplicantID.isExistIDCardType = True
					ddlIDCardType = _CurrentWebsiteConfig.GetEnumItems("ID_CARD_TYPES")
					_IDCardTypeDropDown = Common.RenderDropdownlist(ddlIDCardType, " ", "DRIVERS_LICENSE")
					ucApplicantID.IDCardTypeList = _IDCardTypeDropDown
					ucCoApplicantID.IDCardTypeList = _IDCardTypeDropDown
				End If
			End If

			If Common.SafeGUID(Request.QueryString("sid")) <> Guid.Empty Then
				Dim smBL As New SmBL()
				LoanSavingItem = smBL.GetLoanByID(Common.SafeGUID(Request.QueryString("sid")))
			End If

			'' temporary disabled for now
			''ucMemberProtection.LenderName = _LenderName
			'' Set custom questions

			'' Set disclosures
			'Dim disclosures As List(Of String) = _CurrentWebsiteConfig.GetLoanDisclosures("PERSONAL_LOAN")

			''Url parameter custom questions and answers (refer to the design case AP-1966: Enable Functionality of Lead Source Parameters in AP URLs)         		
			'ucDisclosures.Disclosures = disclosures
			''verify code
			Dim reasonableDemoCode As String = Common.getNodeAttributes(_CurrentWebsiteConfig, "PERSONAL_LOAN/VISIBLE", "reasonable_demonstration_code")
			ucDisclosures.strRenderingVerifyCode = Common.RenderingVerifyCodeField(reasonableDemoCode, _CurrentWebsiteConfig)

			ucDisclosures.MappedShowFieldLoanType = mappedShowFieldLoanType
			ucDisclosures.EnableEmailMeButton = CheckShowField("divEmailMeButton", "", False) Or (IsInMode("777") AndAlso IsInFeature("visibility"))

			''no longer using branch dropdown from config
			'' Set branches
			''Dim branches As Dictionary(Of String, String) = _CurrentWebsiteConfig.GetBranches("HOME_EQUITY_LOAN")

			''If branches.Count < 1 Or _CurrentBranchId IsNot Nothing Then
			''    PLBranchesDiv.Visible = False
			''End If

			''If _CurrentBranchId IsNot Nothing Then
			''    _BranchDropdown = Common.RenderDropdownlist(branches, " ", _CurrentBranchId)
			''Else
			''    _BranchDropdown = Common.RenderDropdownlist(branches, " ", _CurrentWebsiteConfig.sDefaultLoanBranchID)
			''End If
			_branchOptionsStr = GetBranches("PL")
			Dim checkDisAgreePopup As String = Common.checkDisAgreePopup(_CurrentWebsiteConfig)
			If checkDisAgreePopup Then
				_isDisagreePopup = "Y"
			Else
				_isDisagreePopup = "N"
			End If
			_declarationList = Common.GetActiveDeclarationList(_CurrentWebsiteConfig, "PERSONAL_LOAN")
			ucDeclaration.Visible = False
			ucCoDeclaration.Visible = False
			If _declarationList IsNot Nothing AndAlso _declarationList.Count > 0 Then
				ucDeclaration.DeclarationList = _declarationList
				ucCoDeclaration.DeclarationList = _declarationList
				ucDeclaration.Visible = True
				ucCoDeclaration.Visible = True
			End If

			Dim bIsSSO As Boolean = Not String.IsNullOrEmpty(Common.SafeString(Request.Params("FName"))) '
			If Not bIsSSO Then ''hide the scan docs page if SSO via MobileApp
				If _CurrentWebsiteConfig.DLBarcodeScan = "Y" Then
					ucApplicantInfo.LaserScandocAvailable = True
					ucCoApplicantInfo.LaserScandocAvailable = True
					LaserDLScanEnabled = True
				ElseIf Not String.IsNullOrEmpty(_CurrentWebsiteConfig.ScanDocumentKey) Then
					ucApplicantInfo.LegacyScandocAvailable = True
					ucCoApplicantInfo.LegacyScandocAvailable = True
					LegacyDLScanEnabled = True
					_hasScanDocumentKey = _CurrentWebsiteConfig.ScanDocumentKey
				End If
			End If

			''if hasUploadDocument goto uploadDocument page, otherwise goto final page
			Dim uploadDocsMessage As String = Common.getUploadDocumentMessage(_CurrentWebsiteConfig)
			Dim docTitleOptions As List(Of CDocumentTitleInfo) = Nothing
			Dim docTitleRequired = True
			If Common.getUploadDocumentEnable(_CurrentWebsiteConfig) AndAlso Not String.IsNullOrEmpty(uploadDocsMessage) Then
				docTitleRequired = Not Common.getNodeAttributes(_CurrentWebsiteConfig, "BEHAVIOR", "required_doc_title").Equals("N", StringComparison.OrdinalIgnoreCase)
				Dim clrDocTitle = New List(Of CDocumentTitleInfo)

				' Loan doc titles from Custom List Rules
				If CustomListRuleList IsNot Nothing AndAlso CustomListRuleList.Any(Function(c) c.Code = "DOCUMENT_TITLES") Then
					Dim loanClrDocTitle = EvaluateCustomList(Of CDocumentTitleInfo)("DOCUMENT_TITLES")

					If loanClrDocTitle IsNot Nothing AndAlso loanClrDocTitle.Count > 0 Then
						loanClrDocTitle.ForEach(Sub(docTitle) docTitle.LoanType = "PL")
						clrDocTitle.AddRange(loanClrDocTitle)
					End If
				End If

				' XA doc titles for Combo from Custom List Rules
				If XACustomListRuleList IsNot Nothing AndAlso XACustomListRuleList.Any(Function(c) c.Code = "DOCUMENT_TITLES") Then
					Dim xaClrDocTitle = EvaluateCustomList(Of CDocumentTitleInfo)("DOCUMENT_TITLES", XACustomListRuleList)

					If xaClrDocTitle IsNot Nothing AndAlso xaClrDocTitle.Count > 0 Then
						xaClrDocTitle.ForEach(Sub(docTitle) docTitle.LoanType = "XA")
						clrDocTitle.AddRange(xaClrDocTitle)
					End If
				End If

				If clrDocTitle.Count = 0 Then
					' Fallback to ENUMS doc titles
					docTitleOptions = Common.GetDocumentTitleInfoList(_CurrentWebsiteConfig._webConfigXML.SelectSingleNode("ENUMS/LOAN_DOC_TITLE"))
				Else
					' Fill out options
					docTitleOptions = New List(Of CDocumentTitleInfo) From {New CDocumentTitleInfo() With {.Group = "", .Code = "", .Title = "--Please select title--"}}
					docTitleOptions.AddRange(clrDocTitle.OrderBy(Function(t) t.Title))
				End If

				ucDocUpload.Title = Server.HtmlDecode(uploadDocsMessage)
				ucDocUpload.RequireDocTitle = docTitleRequired
				ucDocUpload.DocTitleOptions = docTitleOptions
				ucDocUpload.DocUploadRequired = _requiredUploadDocs.Equals("Y", StringComparison.OrdinalIgnoreCase)

				ucCoDocUpload.Title = Server.HtmlDecode(uploadDocsMessage)
				ucCoDocUpload.RequireDocTitle = docTitleRequired
				ucCoDocUpload.DocTitleOptions = docTitleOptions
				ucCoDocUpload.DocUploadRequired = _requiredUploadDocs.Equals("Y", StringComparison.OrdinalIgnoreCase)
				ucDocUpload.Visible = True
				ucCoDocUpload.Visible = True
				ucDocUploadSrcSelector.Visible = True
				ucCoDocUploadSrcSelector.Visible = True
			Else
				ucDocUpload.Visible = False
				ucCoDocUpload.Visible = False
				ucDocUploadSrcSelector.Visible = False
				ucCoDocUploadSrcSelector.Visible = False
			End If
			'ucCoUploadDocDialog.Title = _UpLoadDocument
			'ucUploadDocDialog.Title = _UpLoadDocument
			''submit cancel message
			_SubmitCancelMessage = Common.getSubmitCancelMessage(_CurrentWebsiteConfig)
			If Not String.IsNullOrEmpty(_SubmitCancelMessage) Then
				_SubmitCancelMessage = Server.HtmlDecode(_SubmitCancelMessage)	''decode code message 
			End If
			'' app_type ='FOREIGN'
			Dim appType As String = _CurrentWebsiteConfig.AppType
			hdForeignAppType.Value = ""	'' initial hidden value foreign
			''Make sure appType is not nothing
			If Not String.IsNullOrEmpty(appType) Then
				If appType.ToUpper() = "FOREIGN" Then
					hdForeignAppType.Value = "Y"
				End If
			End If
			''backgroud theme
			'If String.IsNullOrEmpty(_BackgroundTheme) Then
			'    _bgTheme = _FooterDataTheme
			'Else
			'    _bgTheme = _BackgroundTheme
			'End If
			''enable attribute
			'hdEnableJoint.Value = Common.hasEnableJoint(_CurrentWebsiteConfig)
			_enableJoint = Not Common.hasEnableJoint(_CurrentWebsiteConfig).ToUpper().Equals("N")

			If _enableJoint Then
				_enableJoint = Not Common.getVisibleAttribute(_CurrentWebsiteConfig, "joint_enable_pl").ToUpper.Equals("N")
			End If

			hdForeignContact.Value = Common.isForeignContact(_CurrentWebsiteConfig)

			'' get loan IDA, don't check for idaMethod if SSO via MobileApp          
			If Not bIsSSO OrElse sso_ida_enable Then
				hdIdaMethodType.Value = Common.getIDAType(_CurrentWebsiteConfig, "PERSONAL_LOAN")
			End If
			ucApplicantInfo.ContentDataTheme = _ContentDataTheme
			ucCoApplicantInfo.ContentDataTheme = _ContentDataTheme
			If IsComboMode Then
				ucFomQuestion.FOMQuestions = CFOMQuestion.CurrentFOMQuestions(_CurrentWebsiteConfig)
				ucFomQuestion._CFOMQuestionList = CFOMQuestion.DownloadFomNextQuestions(_CurrentWebsiteConfig)
				ucFomQuestion._lenderRef = _CurrentLenderRef
				xaApplicantQuestion.nAvailability = "1"
				''FOM_V2
				ucFomQuestion.hasFOM_V2 = CFOMQuestion.hasFOM_V2(_CurrentWebsiteConfig)
			Else
				ucFomQuestion.Dispose()
			End If
			'PRIMARY/JOINT/MINOR/ALL
			Select Case _requireMemberNumber.ToUpper()
				Case "PRIMARY"
					ucApplicantInfo.MemberNumberRequired = "Y"
				Case "JOINT"
					ucCoApplicantInfo.MemberNumberRequired = "Y"
				Case "ALL"
					ucApplicantInfo.MemberNumberRequired = "Y"
					ucCoApplicantInfo.MemberNumberRequired = "Y"
				Case Else
					ucApplicantInfo.MemberNumberRequired = "N"
					ucCoApplicantInfo.MemberNumberRequired = "N"
			End Select
			ucApplicantAddress.CollectDescriptionIfOccupancyStatusIsOther = CollectDescriptionIfOccupancyStatusIsOther
			ucCoApplicantAddress.CollectDescriptionIfOccupancyStatusIsOther = CollectDescriptionIfOccupancyStatusIsOther
			ucApplicantAddress.EnableMailingAddress = EnableMailingAddressSection()
			ucCoApplicantAddress.EnableMailingAddress = EnableMailingAddressSection("co_")
			ucFinancialInfo.CollectDescriptionIfEmploymentStatusIsOther = CollectDescriptionIfEmploymentStatusIsOther

			ucCoFinancialInfo.CollectDescriptionIfEmploymentStatusIsOther = CollectDescriptionIfEmploymentStatusIsOther
			ucAssetInfo.EnableAssetSection = EnableAssetSection()
			ucCoAssetInfo.EnableAssetSection = EnableAssetSection()
			ucAssetInfo.AssetTypesString = GetAssetTypes("")
			ucCoAssetInfo.AssetTypesString = GetAssetTypes("")
			If IsComboMode Then
				'load actual prerequisite product name
				Dim currentProdList As List(Of CProduct) = CProduct.GetProductsForLoans(_CurrentWebsiteConfig)
				If currentProdList Is Nothing OrElse currentProdList.Any() = False Then
					_showProductSelection = False
				Else
					Dim allowedProductCodes As List(Of String) = GetAllowedProductCodes("PERSONAL_LOAN")
					If allowedProductCodes IsNot Nothing AndAlso allowedProductCodes.Any() Then
						currentProdList = currentProdList.Where(Function(p) allowedProductCodes.Contains(p.ProductCode)).ToList()
					End If
					If allowedProductCodes IsNot Nothing AndAlso allowedProductCodes.Count = 1 Then
						'hide product selection section when there is only ONE configured product code - this is the same as the current system.
						_showProductSelection = False
						If currentProdList IsNot Nothing And currentProdList.Any() Then
							Dim cproduct As CProduct = currentProdList.FirstOrDefault(Function(cp As CProduct) cp.ProductCode = allowedProductCodes(0))
							If cproduct IsNot Nothing Then
								_prerequisiteProduct = cproduct
								'fix for case when mindepoist isnot corretly configure on lender side
								If Common.SafeInteger(_prerequisiteProduct.MinimumDeposit) < 0 Then
									_prerequisiteProduct.MinimumDeposit = 0
								End If
								''Don't display product and funding on combo app when there is only ONE product that has no minimum deposit, no services, and no product questions. Do display the product and funding on account that has minimum deposit, services, or product questions.
								If cproduct.MinimumDeposit > 0 OrElse (cproduct.Services IsNot Nothing AndAlso cproduct.Services.Any()) OrElse (cproduct.CustomQuestions IsNot Nothing AndAlso cproduct.CustomQuestions.Any()) Then
									_showProductSelection = True
								End If
							End If
						End If
					End If
				End If
				If _showProductSelection = True Then
					Dim ucProductSelection As Inc_MainApp_ProductSelection = CType(LoadControl("~/Inc/MainApp/ProductSelection.ascx"), Inc_MainApp_ProductSelection)
					'should show product selection section when there is no configured product code
					'should show product selection section when there are more than one configured product codes - this is similar to XA where configured product codes will  filter  product to show
					Dim productRendering As New CProductRendering(currentProdList)
					Dim currentLenderServiceConfigInfo = CDownloadedSettings.DownloadLenderServiceConfigInfo(_CurrentWebsiteConfig)
					ucProductSelection.ZipCodePoolList = currentLenderServiceConfigInfo.ZipCodePool
					ucProductSelection.ZipCodePoolName = currentLenderServiceConfigInfo.ZipCodePoolName
					ucProductSelection.CurrentLenderRef = _CurrentLenderRef
					ucProductSelection.CurrentProductList = currentProdList
					ucProductSelection.ContentDataTheme = _ContentDataTheme
					ucProductSelection.FooterDataTheme = _FooterDataTheme
					ucProductSelection.HeaderDataTheme = _HeaderDataTheme
					ucProductSelection.HeaderDataTheme2 = _HeaderDataTheme2
					ucProductSelection.WebsiteConfig = _CurrentWebsiteConfig
					ucProductSelection.ProductQuestionsHtml = productRendering.BuildProductQuestions(currentProdList, _textAndroidTel, True)
					plhProductSelection.Controls.Add(ucProductSelection)
				End If
				Try

					Dim fundingSource As CFundingSourceInfo
					If isCheckFundingPage() = True Then	 'funding page is enabled
						fundingSource = CFundingSourceInfo.CurrentJsonFundingPackage(_CurrentWebsiteConfig)
					Else
						fundingSource = New CFundingSourceInfo(New CLenderServiceConfigInfo())
					End If

					'' try to append PAYPAL funding option into _FundingSourcePackageJsonString since its config stay in WEBSITE node
					If fundingSource.ListTypes IsNot Nothing AndAlso Not String.IsNullOrEmpty(_CurrentWebsiteConfig.PayPalEmail) Then
						'' make sure PAYPAL option stay on top(below not funding option)
						'TODO: temporary disable Paypal funding for secured cc due to complex implementation 
						'' fundingSource.ListTypes.Insert(1, "PAYPAL")
					End If
					'' serialize fundingSource into JSON then .aspx can access it in Page_PreRender 
					ccFundingOptions.FundingTypeList = fundingSource.ListTypes
				Catch ex As Exception  'catch connection issue
					logError("Check connection, etc..", ex)
					Return
				End Try
				ucApplicantInfo.ContentDataTheme = _ContentDataTheme
				ucCoApplicantInfo.ContentDataTheme = _ContentDataTheme
				If IsComboMode Then
					ucFomQuestion.FOMQuestions = CFOMQuestion.CurrentFOMQuestions(_CurrentWebsiteConfig)
					ucFomQuestion._CFOMQuestionList = CFOMQuestion.DownloadFomNextQuestions(_CurrentWebsiteConfig)
					ucFomQuestion._lenderRef = _CurrentLenderRef
				Else
					ucFomQuestion.Dispose()
				End If

				'' CC maxfunding
				If Not String.IsNullOrEmpty(_CurrentWebsiteConfig.CCMaxFunding) Then
					_CCMaxFunding = _CurrentWebsiteConfig.CCMaxFunding
					ccFundingOptions.CCMaxFunding = _CCMaxFunding
				End If
				'' ACH maxfunding
				If Not String.IsNullOrEmpty(_CurrentWebsiteConfig.ACHMaxFunding) Then
					_ACHMaxFunding = _CurrentWebsiteConfig.ACHMaxFunding
					ccFundingOptions.ACHMaxFunding = _ACHMaxFunding
				End If
				'' Paypal maxfunding
				If Not String.IsNullOrEmpty(_CurrentWebsiteConfig.PaypalMaxFunding) Then
					_PaypalMaxFunding = _CurrentWebsiteConfig.PaypalMaxFunding
					ccFundingOptions.PaypalMaxFunding = _PaypalMaxFunding
				End If
                _ccStopInvalidRoutingNumber = Common.SafeString(Common.getNodeAttributes(_CurrentWebsiteConfig, "BEHAVIOR", "stop_invalid_routing_number"))
                ccFundingOptions.FundingDisclosure = Common.getFundingDisclosure(_CurrentWebsiteConfig)
            End If


			plhInstaTouchStuff.Visible = False
			If _CurrentWebsiteConfig.InstaTouchEnabled AndAlso Not bIsSSO And Not Is2ndLoanApp Then
				plhInstaTouchStuff.Visible = True
				Dim instaTouchStuffCtrl As Inc_MainApp_InstaTouchStuff = CType(LoadControl("~/Inc/MainApp/InstaTouchStuff.ascx"), Inc_MainApp_InstaTouchStuff)
				instaTouchStuffCtrl.PersonalInfoPage = "pl2"
				instaTouchStuffCtrl.LoanType = "PL"
				instaTouchStuffCtrl.InstaTouchMerchantId = _CurrentWebsiteConfig.InstaTouchMerchantId
				instaTouchStuffCtrl.LenderID = _CurrentWebsiteConfig.LenderId
				instaTouchStuffCtrl.LenderRef = _CurrentWebsiteConfig.LenderRef
				plhInstaTouchStuff.Controls.Add(instaTouchStuffCtrl)
			End If

		End If
			'asign config theme for PageHeader UC, avoid reading config file unnescessary
			ucPageHeader._headerDataTheme = _HeaderDataTheme
			ucPageHeader._footerDataTheme = _FooterDataTheme
			ucPageHeader._contentDataTheme = _ContentDataTheme
			ucPageHeader._headerDataTheme2 = _HeaderDataTheme2
			ucPageHeader._backgroundDataTheme = _BackgroundTheme
			ucPageHeader._buttonDataTheme = _ButtonTheme
			ucPageHeader._buttonFontColor = _ButtonFontTheme

			plhApplicationDeclined.Visible = False
			If CustomListRuleList IsNot Nothing AndAlso CustomListRuleList.Any(Function(p) p.Code = "APPLICANT_BLOCK_RULE") Then
				plhApplicationDeclined.Visible = True
				Dim applicationBlockRulesPage As Inc_MainApp_ApplicationBlockRules = CType(LoadControl("~/Inc/MainApp/ApplicationBlockRules.ascx"), Inc_MainApp_ApplicationBlockRules)
				applicationBlockRulesPage.CustomListItem = CustomListRuleList.First(Function(p) p.Code = "APPLICANT_BLOCK_RULE")
				plhApplicationDeclined.Controls.Add(applicationBlockRulesPage)
			End If
			plhApplicantAdditionalInfo.Visible = False
			If NSSList IsNot Nothing AndAlso NSSList.Count > 0 Then
				plhApplicantAdditionalInfo.Visible = True
				Dim applicantAdditionalInfoPage As Inc_MainApp_ApplicantAdditionalInfo = CType(LoadControl("~/Inc/MainApp/ApplicantAdditionalInfo.ascx"), Inc_MainApp_ApplicantAdditionalInfo)
				applicantAdditionalInfoPage.LogoUrl = _LogoUrl
				applicantAdditionalInfoPage.PreviousPage = "pl2"
				applicantAdditionalInfoPage.PreviousCoAppPage = "pl6"
				applicantAdditionalInfoPage.NextPage = "pageSubmit"
				applicantAdditionalInfoPage.NSSList = NSSList
				applicantAdditionalInfoPage.MappedShowFieldLoanType = mappedShowFieldLoanType
				applicantAdditionalInfoPage.EnablePrimaryAppSpouseContactInfo = EnablePrimaryAppSpouseContactInfo
				applicantAdditionalInfoPage.EnableCoAppSpouseContactInfo = EnableCoAppSpouseContactInfo

				plhApplicantAdditionalInfo.Controls.Add(applicantAdditionalInfoPage)
			End If
			''credit pull message in disclosure section
			Dim oNode As XmlNode = _CurrentWebsiteConfig._webConfigXML.SelectSingleNode("CREDIT_PULL_MESSAGE")
			If oNode IsNot Nothing Then
				_CreditPullMessage = oNode.InnerText
			End If
			_plLocationPool = Common.getNodeAttributes(_CurrentWebsiteConfig, "PERSONAL_LOAN/VISIBLE", "location_pool") = "Y"
			If _plLocationPool Then
				Dim currentLenderServiceConfigInfo = CDownloadedSettings.DownloadLenderServiceConfigInfo(_CurrentWebsiteConfig)
				_zipCodePoolList = currentLenderServiceConfigInfo.ZipCodePool
				_zipCodePoolNames = currentLenderServiceConfigInfo.ZipCodePoolName
				_plZipCodePoolProducts = CDownloadedSettings.PersonalLoanProducts(_CurrentWebsiteConfig).Where(Function(x) x.IsActive = "Y").ToList()
			End If
	End Sub
    Private Function isCheckFundingPage() As Boolean
		Dim result As Boolean = True
		Dim oNodes As XmlNodeList = _CurrentWebsiteConfig._webConfigXML.SelectNodes("PERSONAL_LOAN/VISIBLE")
		If oNodes.Count < 1 Then
			Return True
		End If

		For Each child As XmlNode In oNodes
			If child.Attributes("funding") Is Nothing Then Continue For
			If Common.SafeString(child.Attributes("funding").InnerText) = "N" Then
				result = False
			End If
		Next

		Return result
	End Function
End Class
