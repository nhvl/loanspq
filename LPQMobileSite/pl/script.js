﻿var isSubmitWalletAnswer = false;
var isCoSubmitWalletAnswer = false;
var isSubmittingLoan = false;
var docScanObj, co_docScanObj;

$(function () {
    EMPLogicPI = $.extend(true, {}, EMPLogic);
    EMPLogicPI.init('ddlEmploymentStatus', '', 'pl2', 'divEmploymentStatusRest');
    $('#pl2').on("pageshow", function () {
        EMPLogicPI.refreshHtml();
    });
    EMPLogicJN = $.extend(true, {}, EMPLogic);
    EMPLogicJN.init('co_ddlEmploymentStatus', 'co_', 'pl6', 'co_divEmploymentStatusRest');
    $('#pl6').on("pageshow", function () {
    	EMPLogicJN.refreshHtml();
    });
    //primary previous employment
    EMPLogicPI_PREV = $.extend(true, {}, EMPLogic);
    EMPLogicPI_PREV.init('prev_ddlEmploymentStatus', 'prev_', 'pl2', 'prev_divEmploymentStatusRest');
    //joint previous employement
    EMPLogicJN_PREV = $.extend(true, {}, EMPLogic);
    EMPLogicJN_PREV.init('co_prev_ddlEmploymentStatus', 'co_prev_', 'pl6', 'co_prev_divEmploymentStatusRest');

    /*$('#pl8').on("pageshow", function () {
        EMPLogicJN.refreshHtml();
    });
    $('#pl9').on("pageshow", function() {
        $('#divAgreementFinal').show();
    });*/

    //SwitchHasCoAppButton(document.getElementById('ddlHasCoApplicant'), false);
    //LOCTrigger();
    //SwitchLineOfCreditButton(document.getElementById('ddlIsLineOfCredit'), false);
    //user click disagree button -->popup confirm message

    $(document).on('click','#divDisagree a.header_theme2',function (e) {
        var redirectURL = $('#hdRedirectURL').val(); 
        //don't popup confirm message--> go direct to return to mainpage
        //displayConfirmMessage(redirectURL, isCoApp, onClickReturnToMainPage);
        if (redirectURL == "") { //need to save loan infor 
          onClickReturnToMainPage(e);        
        }
       // location.hash = '#decisionPage'; --> location.hash is no working on IE browser
        goToNextPage($('#decisionPage'));
        e.preventDefault();
    });
    
    //enabled I Agree button when click refresh
    //$('input.custom').attr('disabled', false);

    //var footerTheme = $('#hdFooterTheme').val();
    //var bgColor = $('.ui-bar-' + footerTheme).css('background-color');
    //var changeColor = changeBackgroundColor(bgColor, .6);
    // var bgTheme = $('#hdBgTheme').val();
	//if (bgTheme != "") {
    //    changeColor = bgColor;
    //}
    //$('body').css("background-color", changeColor);
    //handleEnableJoint();
    //handledCheckboxDisclosure();// disclosure click handled in gotoPDFViewer
    //handleRemoveBtnStyle('pl4'); //primary applicant
    //handleRemoveBtnStyle('pl8'); // for coApplicant
    /*
    $('#ddlHasCoApplicant').on('change', function () {
        var hasCoApp = $(this).val();
        if (hasCoApp == 'Y') {
            $('#divHasCoApp').show();
            $('#divSubmitApplicant').hide();
        } else {
            $('#divHasCoApp').hide();
            $('#divSubmitApplicant').show();
        }
    });
    */
    handledFontWeightOfDivContent();
    //handle disable disclosures 
    $("div[name='disclosures'] label").click(function () {
        handleDisableDisclosures(this);
    });
    pl1Init();
    
    $(document).on("pageshow", function () {
    	var curPageId = $.mobile.pageContainer.pagecontainer('getActivePage').attr('id');
    	switch (curPageId) {
    		case "pl1":
    			pl1Init();
    			break;
    		case "pl2":
    			pushToPagePaths("pl2");
    			$("#hdPlHasCoApplicant").val("N");//reset
    			break;
    		case "pl6":
    			pushToPagePaths("pl6");
    			break;
            case "pagesubmit":
                registerViewPersonalLoanInfoValidator();
                pageSubmitInit();
    			break;
    		case "pl_last":
    			pageLastInit();
    			break;
    	}
    });
    $("#popEstimatePaymentCalculator").on("popupbeforeposition", function () {
    	var $container = $("#popEstimatePaymentCalculator");
	    $("div.required-field-notation", $container).remove();
    	$("input[data-field='LoanAmount']", $container).val(Common.FormatCurrency(0, true));
    	if (Common.GetFloatFromMoney($.trim($("#txtLoanAmount").val())) > 0) {
    		$("input[data-field='LoanAmount']", $container).val($("#txtLoanAmount").val());
	    }
    	var $loanTerm = $("input[data-field='LoanTerm']", $container);
    	if ($loanTerm.length == 0) {
		    $loanTerm = $("select[data-field='LoanTerm']", $container);
	    } else {
    		$loanTerm.val("1"); //set default value for textbox
	    }
    	if ($('#hdIsLineOfCredit').val().toUpperCase() == "Y") {
		    $loanTerm.closest("div.row").hide();
	    }else
    	{
    		$loanTerm.closest("div.row").show();
    		if (getLoanTerm() !== "") {
    			$loanTerm.val(getLoanTerm());
				if ($loanTerm.is("select")) {
					$loanTerm.selectmenu('refresh');
				}
    		}
		}
    	
        //deprecate bc no longer need Apply btn in calc popup
    	//$("a.div-continue-button", "#popEstimatePaymentCalculator").off("click").on("click", function () {
    	//	var loanAmountStr = $("input[data-field='LoanAmount']", $container).val();	
    	//	$("#txtLoanAmount").val(loanAmountStr);
    	//	$.lpqValidate.hideValidation("#txtLoanAmount");
    	//	if ($("#txtLoanTerm").length > 0) {
    	//		$("#txtLoanTerm").val($("input[data-field='LoanTerm']", $container).val());
    	//		$.lpqValidate.hideValidation("#txtLoanTerm");
    	//	} else {
    	//		$("#ddlLoanTerm").val($("select[data-field='LoanTerm']", $container).val()).selectmenu('refresh');
		//		$.lpqValidate.hideValidation("#ddlLoanTerm");
    	//	}
    	//	var estAmount = Common.FormatCurrency(calculateEstimatePayment(Common.GetFloatFromMoney(loanAmountStr), Common.GetFloatFromMoney(getLoanTerm()), Common.GetFloatFromMoney($("#spInterestRate").data("value"))), true);
    	//	$("span.est-payment-value", $("#divEstimatePayment")).text(estAmount);
    	//	$("#popEstimatePaymentCalculator").popup("close");
    	//});
    	bindEstimatePayment();
    });
    $("select", "#popEstimatePaymentCalculator").on("change", bindEstimatePayment);
	$("input", "#popEstimatePaymentCalculator").on("blur", bindEstimatePayment);
    $("input", "#popEstimatePaymentCalculator").on("keyup paste", function () {
    	setTimeout(bindEstimatePayment, 400);
    });
    $("#ddlLoanTerm").on("change", fillEstimatePayment);
    $("#txtLoanTerm, #txtLoanAmount").on("blur", fillEstimatePayment);
	$("#txtLoanTerm, #txtLoanAmount").on("keyup paste", function() {
		setTimeout(fillEstimatePayment, 400);
	});
    //move to pl1Init function
    ////handle selected/unseleced joint button
    //$('#btnPlHasCoApplicant').on('click', function () {
    //    handledBtnHeaderTheme($(this));
    //});
    ////handle selected/unseleced line of credit button
    //$('#btnLineOfCredit').on('click', function () {
    //    handledBtnHeaderTheme($(this));
    //});
    registerProvideLoanInfoValidator(); 
    //branch name selection
    handleShowAndHideBranchNames();
    $("#popUpCreditDiff, #popUpSavingAmountGuid, #popEstimatePaymentCalculator").on("popupafteropen", function (event, ui) {
    	$("#" + this.id + "-screen").height("");
    });
    
    $("div[data-section='proceed-membership']").observer({
        validators: [
            function (partial) {
                if ($("#hdProceedMembership").val() == "") {
                    return "Do you wish to continue opening your membership?";
                }
                return "";
            }
        ],
        validateOnBlur: true,
        group: "ValidateProceedMembership",
        groupType: "complexUI"
	});
	toggleDescriptionTextbox(false); // Make the Description field not appear (for default visibility) by supplying that no loan purpose has been selected yet (false).
	// Fill in the fields with test data
    if (getParaByName("autofill") == "true") {
    	// Fill in the stuff
    	autoFillData();
    }
    $("#popNotification").enhanceWithin().popup();
    //xaCombo location pool
    if ($('#hdHasZipCodePool').val() == 'Y' && $('#hdIsComboMode').val() == 'Y') {   
        var $zipCodeElement = $("#txtLocationPoolCode");
        if (typeof loanProductFactory !== "undefined" && loanProductFactory !== null) {
            loanProductFactory.handledLocationPoolProducts($zipCodeElement);
        }
    } else if ($('#hdHasPLZipCodePool').val() == 'Y') { //standard loan location pool       
        handledZipPoolPersonalLoanInfo(PLPRODUCTS);
    }
    docScanObj = new LPQDocScan("");
    co_docScanObj = new LPQDocScan("co_");
    //InstaTouch: Call 1 ("Authorization") called on the loading "PL Info" and not after an applicant has clicked the "Continue" button
    if (window.IST) {
        IST.FACTORY.loadCarrierIdentification();
    }
});
function handledZipPoolPersonalLoanInfo(zipPoolPLProducts) {
    $("#pl_txtLocationPoolCode").on("keyup paste", function () {
        var $self = $(this);
        var zipCode = $self.val();
        setTimeout(function () {
            var $divLocationPool = $('#pl_divLocationPool');
            var $divNoAvailableProduct = $('#pl_divNoAvailableProducts');
            var $divPersonalLoanInfo = $('#divPersonalLoanInfo');
            if (/[0-9]{5}/.test(zipCode)) {
                var selectedZipPoolPLProducts = getSelectedZipPoolProducts(zipCode, zipPoolPLProducts);
                var selectedZipPoolPLPurposes = getSelectedZipPoolLoanPurposes(selectedZipPoolPLProducts);            
                if (selectedZipPoolPLPurposes.length == 0) {
                    //show No available products message if there is no current zip pool purposes
                    $divNoAvailableProduct.removeClass('hidden');
                    $divPersonalLoanInfo.addClass('hidden');
                } else {                    
                    handledShowAndHideZipPoolLoanPurposes(selectedZipPoolPLPurposes);
                    var $divLoanPurpose = $('#divLoanPurposePanel');
                    var $loanPurpose = $divLoanPurpose.find('a[data-command="loan-purpose"]');
                    var $hiddenLoanPurpose = $divLoanPurpose.find('a[data-command="loan-purpose"].hidden');
                    var $loanPurposeLOC = $divLoanPurpose.find('a[data-command="line-of-credit"]');
                    var $hiddenLoanPurposeLOC = $divLoanPurpose.find('a[data-command="line-of-credit"].hidden');                  
                    if (($loanPurpose.length > $hiddenLoanPurpose.length) || ($loanPurposeLOC.length > $hiddenLoanPurposeLOC.length)) {
                        $divNoAvailableProduct.addClass('hidden');
                        $divPersonalLoanInfo.removeClass('hidden');                      
                        $divLoanPurpose.show();
                        $("#txtZip").val(zipCode);
                        $("#txtCity").val("");
                        $("#txtZip").attr("disabled", "disabled");
                        $("#txtZip").trigger("change");
                        var hasLoanPurposeLOC = $loanPurposeLOC.length > 0 && $loanPurposeLOC.length > $hiddenLoanPurposeLOC.length ? true : false;
                        showAndHideZipPoolLineOfCredit(hasLoanPurposeLOC);
                        if (!hasLoanPurposeLOC && ($loanPurpose.length == 1 || ($loanPurpose.length - $hiddenLoanPurpose.length == 1))) {
                            //preselect loan purpose and hide the loan purpose section if there is only one loan purpose and there is no loan purposes for LOC
                            $divLoanPurpose.find('a[data-command="loan-purpose"]:not([class~="hidden"])').trigger('click');
                            $divLoanPurpose.hide();
                        }
                    } else {
                        //show No available products message if there is no current zip pool purposes
                        $divNoAvailableProduct.removeClass('hidden');
                        $divPersonalLoanInfo.addClass('hidden');
                    }
                }
            } else {
                $divPersonalLoanInfo.addClass("hidden");
            }
        }, 400);
    });
}
function showAndHideZipPoolLineOfCredit(hasLoanPurposeLOC) {
    var $divBtnLineOfCredit = $('#divBtnLineOfCredit');
    var $btnLineOfCredit = $('#btnLineOfCredit');
    if ($btnLineOfCredit.hasClass('active')) {
        $btnLineOfCredit.trigger('click');   //de-select This is a Line of Credit button                
    }
    //hide "This is a Line of Credit" button if there is no selected zip pool loan purposes for line of credit 
    if (hasLoanPurposeLOC) {
        $divBtnLineOfCredit.removeClass('hidden');
    } else {
        $divBtnLineOfCredit.addClass('hidden');
    }
}
function handledShowAndHideZipPoolLoanPurposes(selectedZipPoolLoanPurposes) {
    var showAndHideLoanPurpose = function (element) {
        var $self = $(element);
        //de-select loanpurpose and reset hdLoanPurpose value
        if ($self.hasClass('active')) {
            $self.removeClass('active btnActive_on');
            $('#hdLoanPurpose').val("");
        }
        var loanPurpose = $self.attr('data-key');
        if ($.inArray(loanPurpose, selectedZipPoolLoanPurposes) > -1) {
            $self.removeClass('hidden');
        } else {
            $self.addClass('hidden');
        }
    };
    $('#divLoanPurposePanel a[data-command="loan-purpose"]').each(function () {
        showAndHideLoanPurpose(this);
    });
    $('#divLoanPurposePanel a[data-command="line-of-credit"]').each(function () {
        showAndHideLoanPurpose(this);
    });
}

function ScanAccept(prefix) {
	if (prefix == "") {
		docScanObj.scanAccept();
		goToNextPage("#pl2");
	} else {
		co_docScanObj.scanAccept();
		goToNextPage("#pl6");
	}
}
function fillEstimatePayment() {
	var $loanAmount = $("#txtLoanAmount");
	var $loanTerm = $("#txtLoanTerm");
	var $container = $("#divEstimatePayment");
	if ($loanTerm.length == 0) {
		$loanTerm = $("#ddlLoanTerm");
	}
	var loanAmount = Common.GetFloatFromMoney($loanAmount.val());
	var loanTerm = parseFloat($loanTerm.val());
	var rate = parseFloat($("#spInterestRate").data("value"));
	var estPayment = 0;
	if (loanAmount > 0 && loanTerm > 0) {
		estPayment = calculateEstimatePayment(loanAmount, loanTerm, rate);
	}
	$("span.est-payment-value", $container).text(Common.FormatCurrency(estPayment, true));
}
function calculateEstimatePayment(loanAmount, loanTerm, rate) {
	return (loanAmount * (rate/12) / 100) / (1 - Math.pow((1 + (rate/12) / 100), -1 * loanTerm));
}
function bindEstimatePayment() {
	var $container = $("#popEstimatePaymentCalculator");
	var $loanAmount = $("input[data-field='LoanAmount']", $container);
	var loanAmount = Common.GetFloatFromMoney($loanAmount.val());
	var $loanTerm = $("input[data-field='LoanTerm']", $container);
	if ($loanTerm.length == 0) {
		$loanTerm = $("select[data-field='LoanTerm']", $container);
	}
	var loanTerm = 1;
	if ($loanTerm.length > 0 && parseFloat($loanTerm.val()) > 0) {
		loanTerm = parseFloat($loanTerm.val());
	} else {
		$loanTerm.val(1);
		if ($loanTerm.is("select")) {
			$loanTerm.selectmenu('refresh');
		}
	}
	var interestRate = $("input[data-field='InterestRate']", $container).val().replace("%", "");
	var estAmount = Common.FormatCurrency(calculateEstimatePayment(loanAmount, loanTerm, interestRate), true);
	$("[data-field='EstimatePayment']", $container).text(estAmount);
}

function getUserName() {
	var txtFName = $('#txtFName').val();
	var txtMName = $('#txtMName').val();
	var txtLName = $('#txtLName').val();
	//capitalize the first character
	txtFName = txtFName.replace(txtFName.charAt(0), txtFName.charAt(0).toUpperCase());
	txtMName = txtMName.replace(txtMName.charAt(0), txtMName.charAt(0).toUpperCase());
	txtLName = txtLName.replace(txtLName.charAt(0), txtLName.charAt(0).toUpperCase());
	var userName = "";
	if (txtMName != "")
		userName = txtFName + " " + txtMName + " " + txtLName;
	else
		userName = txtFName + " " + txtLName;
	return userName;
}

function assignLoanPurpose(loanPurpose) {
    if ($.trim(loanPurpose) !== "") {
        $("div[data-section='loan-purpose']").find("a[data-command='loan-purpose']").each(function() {
            var $self = $(this);
            if ($self.data("key").toString().toUpperCase() === $.trim(loanPurpose).toString().toUpperCase()) {
                $self.data("selected", false);
                $self.trigger("click");
            }
        });
        //var $ddlLoanPurpose = $("#ddlLoanPurpose");
        ////perform this way to prevent assign unbounded value to selectbox
        //$ddlLoanPurpose.find("option").each(function (idx, element) {
        //    if ($(element).val().toUpperCase() == loanPurpose.toUpperCase()) {
        //        $(element).attr("selected", "selected");
        //    }
        //});
        //if ($ddlLoanPurpose.data("mobile-selectmenu") === undefined) {
        //    $('#ddlLoanPurpose').selectmenu(); //not initialized yet, lets do it
        //}
        //$ddlLoanPurpose.selectmenu('refresh');
        //$ddlLoanPurpose.trigger("change");
    }
}
function assignLoCLoanPurpose(locLoanPurpose) {
	if ($.trim(locLoanPurpose) !== "") {
		$("div[data-section='line-of-credit']").find("a[data-command='line-of-credit']").each(function () {
			var $self = $(this);
			if ($self.data("key").toString().toUpperCase() === $.trim(locLoanPurpose).toString().toUpperCase()) {
				$self.data("selected", false);
				$self.trigger("click");
			}
		});
	}
}
function assignLOC(isLoc) {
    if (isLoc.toUpperCase() === "Y" || isLoc.toUpperCase() === "N") {   
        //hide divBtnLineOfCredit section
    	$('#divBtnLineOfCredit').addClass('hidden');
	    $("#divOverdraftQuestion").removeClass("hidden");
        setLOC(isLoc);
    }
    //showDescription();
}
function setLOC(isLoc) {
    var $divLoanPurposeElem = $("div[data-section='loan-purpose']");
    var $divLoanPurposeElem_loc = $("div[data-section='line-of-credit']");
    var $loanPurposeBtnElem = $divLoanPurposeElem.find("a[data-command='loan-purpose']");
    var $divOverdraftQuestion = $("#divOverdraftQuestion");
    if (isLoc.toUpperCase() === "Y") {
        $("#hdIsLineOfCredit").val("Y");
        //reset section line of credit
        $("#hdLineOfCredit").val("");
        $divLoanPurposeElem.hide();
        if ($divOverdraftQuestion.length > 0) {
        	$divLoanPurposeElem_loc.hide();
        	$divOverdraftQuestion.removeClass("hidden");
        	$divOverdraftQuestion.find("a[data-command]").each(function () {
        		$(this).removeClass("active");
        		handledBtnHeaderTheme($(this));
        	});
        } else {
        	var $loanPurposeBtnElem_loc = $divLoanPurposeElem_loc.find("a[data-command='line-of-credit']");
        	$loanPurposeBtnElem_loc.each(function () {
        		$(this).removeClass("active");
        		handledBtnHeaderTheme($(this));
        	});
        	if ($loanPurposeBtnElem_loc.length > 1) {
        		$divLoanPurposeElem_loc.show();
        	} else if ($loanPurposeBtnElem_loc.length == 1) {
        		$loanPurposeBtnElem_loc.trigger('click');
        		$loanPurposeBtnElem_loc.parent().addClass('hidden');
        	}
        	$("#txtDescription").val("");
            toggleDescriptionTextbox($loanPurposeBtnElem_loc.length == 0);
        }
	    
        $('#divLoanTerm').hide();
    } else {
    	$("#hdIsLineOfCredit").val("N");
        //reset section loan purpose
        $("#hdLoanPurpose").val("");
        $("#txtDescription").val("");
     
        $loanPurposeBtnElem.each(function () {
            $(this).removeClass("active");
            handledBtnHeaderTheme($(this));
        });

        if ($loanPurposeBtnElem.length > 1) {
            //end reset
            $divLoanPurposeElem.show();
        } else {
        	//if loan purpose only has one option --> preselected and hide loan purpose
        	var loanPurposeElem = $("div[data-section='loan-purpose']").find("a[data-command='loan-purpose']");
        	if (loanPurposeElem.length == 1) {
        		loanPurposeElem.trigger('click');
        		loanPurposeElem.parent().addClass('hidden');
        	}
        	$("#divOverdraftQuestion").addClass("hidden");
        }

        $divOverdraftQuestion.addClass("hidden");
        $divLoanPurposeElem_loc.hide();
        toggleDescriptionTextbox($loanPurposeBtnElem.length == 0);
        $("#divOverdraftQuestion").addClass("hidden");
        $('#divLoanTerm').show();
    }
}

function pl1Init() {
	$("#btnLineOfCredit").off("click").on("click", function () {
		var $self = $(this);
		if ($self.attr("contenteditable") == "true") return false;
        if ($("#hdIsLineOfCredit").val() === "N") {
        	setLOC("Y");
	        $("#divEstimatePayment").hide();
        } else {
        	setLOC("N");
        	//if ($("#divEstimatePayment").data("show-field")) {
        		$("#divEstimatePayment").show();
	        //}
        }
        $self.toggleClass("active");
        handledBtnHeaderTheme($(this));
        $.lpqValidate.hideValidation('#divLoanPurposePanel');
        $.lpqValidate.hideValidation('#divOverdraftQuestion');
		//LOCTrigger();
	});
    $("#btnPlHasCoApplicant").off("click").on("click", function () {
    	var $self = $(this);
    	if ($self.attr("contenteditable") == "true") return false;
        if ($("#hdPlHasCoApplicant").val() === "N") {
            $("#hdPlHasCoApplicant").val("Y");
        } else {
            $("#hdPlHasCoApplicant").val("N");
        }
        $self.toggleClass("active");
        handledBtnHeaderTheme($(this));
    });
    $("#divOverdraftQuestion a[data-command]").off("click").on("click", function (e) {
    	var $self = $(this);
    	$("#divOverdraftQuestion a[data-command]").each(function () {
    		$(this).removeClass("active");
    		handledBtnHeaderTheme($(this));
    	});
    	$self.addClass("active");
	    var command = $self.data("command");
	    var $divLoanPurposeElem_loc = $("div[data-section='line-of-credit']");
    	var $loanPurposeBtnElem_loc = $divLoanPurposeElem_loc.find("a[data-command='line-of-credit']");
    	$("#txtDescription").val("");
    	$("#hdLineOfCredit").val("");
    	$loanPurposeBtnElem_loc.each(function () {
    		$(this).removeClass("active");
    		handledBtnHeaderTheme($(this));
    	});
    	if ($loanPurposeBtnElem_loc.length > 1) {
    		//do filter overdraft items here
    		$divLoanPurposeElem_loc.find("a[data-overdraft-option]").each(function() {
		    	var $that = $(this);
		    	if ($that.data("overdraft-option") == command || $that.data("overdraft-option") == "BOTH_OVERDRAFT_AND_NON_OVERDRAFT") {
				    $that.removeClass("hidden");
			    } else{
		    		$that.addClass("hidden");
			    }
		    });
    		if ($divLoanPurposeElem_loc.find("a[data-overdraft-option]:not(.hidden)").length > 0) {
			    $divLoanPurposeElem_loc.show();
		    } else {
    			$divLoanPurposeElem_loc.hide();
		    }
    	} else {
    		var $lineOfCreditItems = $divLoanPurposeElem_loc.find("a[data-overdraft-option='" + command + "']");
    		if ($lineOfCreditItems.length == 0) {
    			$lineOfCreditItems = $divLoanPurposeElem_loc.find("a[data-overdraft-option='BOTH_OVERDRAFT_AND_NON_OVERDRAFT']");
    		}
    		if ($lineOfCreditItems.length == 1) {
    			$lineOfCreditItems.trigger('click');
    			$lineOfCreditItems.parent().addClass('hidden');
    		}
    	}
        toggleDescriptionTextbox($loanPurposeBtnElem_loc.length == 0);
    });
    bindEventLoanPurposeOption();
    bindEventLOCOption();

    //enable sumbit buttons
    if ($('#pagesubmit .div-continue-button').hasClass('ui-disabled')) {
        $('#pagesubmit .div-continue-button').removeClass('ui-disabled');
    }
    if ($('#walletQuestions .div-continue-button').hasClass('ui-disabled')) {
        $('#walletQuestions .div-continue-button').removeClass('ui-disabled');
    }
    if ($('#co_walletQuestions .div-continue-button').hasClass('ui-disabled')) {
        $('#co_walletQuestions .div-continue-button').removeClass('ui-disabled');
    }
    pushToPagePaths("pl1");
}

function pageLastInit() {
	if ($("#divXsellSection").length === 1) {
		$("a.btn-header-theme", $("#divXsellSection")).on("click", function () {
			if ($(this).attr("contenteditable") == "true") return false;
			$.mobile.loading("show", {
				theme: "a",
				text: "Loading ... please wait",
				textonly: true,
				textVisible: true
			});
		});
	}
}
function pageSubmitInit() {
    //apply this logic for combo mode only
    if ($("#hdIsComboMode").val() === "Y") {
        $("a[data-command='proceed-membership']").off("click").on("click", function () {
            var $self = $(this);
            if ($self.attr("contenteditable") == "true") return false;
            //deactive others
            $("div[data-section='proceed-membership']").find("a[data-command='proceed-membership']").each(function () {
                $(this).removeClass("active");
                handledBtnHeaderTheme($(this));
            });
            //set active for current option
            $self.addClass("active");
            $("#hdProceedMembership").val($self.data("key"));
            $("div[data-section='proceed-membership']").trigger("change");
        });
        
        //$("#divCcFundingOptions").find("a[data-value='NOT FUNDING AT THIS TIME']").parent().show();
        $('#txtFundingDeposit').off("change").on("change", function () {
            if (Common.GetFloatFromMoney($(this).val()) === 0) {
            	$("#divCcFundingOptions").find("a[data-value='NOT FUNDING AT THIS TIME']").parent().show();
            } else {
            	$("#divCcFundingOptions").find("a[data-value='NOT FUNDING AT THIS TIME']").parent().hide();
            }
        }).trigger("change");;
        if (typeof loanProductFactory != "undefined") {
			if (typeof loanProductFactory.refreshDepositTextFields == "function") {
				loanProductFactory.refreshDepositTextFields();
			}
        }
        //FOM membershipfee
        $("#divShowFOM div.btn-header-theme").off("click").on("click", function () {
            var membershipFees = calculateOneTimeMembershipFee($(this).attr('data-fom-name'));
            //update divMembership field
            $("#divMembershipFee").attr("data-membershipfee", membershipFees)
            $("#divMembershipFee").text(Common.FormatCurrency(membershipFees, true));
            //update total deposit field
            if (typeof loanProductFactory !== "undefined" && loanProductFactory !== null) {
                $('#txtFundingDeposit').val(Common.FormatCurrency(loanProductFactory.getTotalDeposit(), true));
            }
        });
    }
    pushToPagePaths("pagesubmit");
}
function bindEventLoanPurposeOption() {
	$("div[data-section='loan-purpose']").find("a[data-command='loan-purpose']").off("click").on("click", function () {
		if ($(this).attr("contenteditable") == "true") return false;
        //first, remove other active button
        $("div[data-section='loan-purpose']").find("a[data-command='loan-purpose']").each(function() {
            $(this).removeClass("active");
            handledBtnHeaderTheme($(this));
        });
        //then, activate selected button
        var $self = $(this);
        $self.addClass("active");
        $("#hdLoanPurpose").val($self.data("key"));
        toggleDescriptionTextbox($self.data("key").toString().toUpperCase().indexOf("OTHER") > -1);
        handledBtnHeaderTheme($self);
        $.lpqValidate.hideValidation("#divLoanPurposePanel");
    });
}
function bindEventLOCOption() {
	$("div[data-section='line-of-credit']").find("a[data-command='line-of-credit']").off("click").on("click", function () {
		if ($(this).attr("contenteditable") == "true") return false;
        //first, remove other active button
        $("div[data-section='line-of-credit']").find("a[data-command='line-of-credit']").each(function () {
            $(this).removeClass("active");
            handledBtnHeaderTheme($(this));
        });
        //then, activate selected button
        var $self = $(this);
        $self.addClass("active");
        $("#hdLineOfCredit").val($self.data("key"));
		toggleDescriptionTextbox($.trim($self.data("key").toString()) === "" || $self.data("key").toString().toUpperCase().indexOf("OTHER") > -1)
        handledBtnHeaderTheme($self);
	    $.lpqValidate.hideValidation("#divLoanPurposePanel");
    });
}
function calculateOneTimeMembershipFee(sFOMName) {
    var membershipFee = 0.0;
    if ($("#hdIsComboMode").val() == "Y" && $("#pagesubmit").find("#divShowFOM").length > 0) {
        membershipFee = parseFloat($("#hdMembershipFee").val());
        if (sFOMName != undefined) {
            if (typeof CUSTOMLISTFOMFEES[sFOMName] === "string") {
                membershipFee = membershipFee + parseFloat(CUSTOMLISTFOMFEES[sFOMName]);
            }
        }
    }
    return membershipFee;
};
/*
function handleEnableJoint() {
    var isEnableJoint = $('#hdEnableJoint').val();
    if (isEnableJoint != undefined) {
        isEnableJoint = isEnableJoint.toUpperCase();
    }
    if (isEnableJoint == 'N') {
        $('#divHasCoApplicant').hide();
        //also hide all pages for joint 
        $('#pl6').hide();
        $('#pl8').hide();
        $('#pl9').hide();
        $('#divAgreement').css('margin-top', '0px');
    } 
}*/

//function showDescription() {
   
//    var select = "";
//    var selectedText = "";
//    var LoanPurposeCount="";
//    if ($("#hdIsLineOfCredit").val().toUpperCase() === "Y") {
//        LoanPurposeCount = $("#ddlLineOfCreditPurposes>option").length; // initial count
//        select = $('#ddlLineOfCreditPurposes option:selected').val();
//        selectedText = $('#ddlLineOfCreditPurposes option:selected').text();
//        if (select != undefined) {
//            select = select.toUpperCase(); // to prevent the error when calling toUpperCase Function
//        } else {
//            select = "";
//        }
//        if (LoanPurposeCount == 1 || LoanPurposeCount == 0) {
//            $('#divLOC').hide();
//            $('#divLoanPurpose').hide();
//            $('#divDescription').show();
//        }
//        else if (select.indexOf('OTHER') != -1)
//            $('#divDescription').show();
//        else
//        {
//         $('#divLOC').show();
//         $('#divLoanPurpose').hide();
//         $('#divDescription').hide();
//        }
//    } else {
//      LoanPurposeCount = $('#ddlLoanPurpose>option').length; // initial count
//      select = $('#ddlLoanPurpose option:selected').val();
//      selectedText = $('#ddlLoanPurpose option:selected').text();
//      if (select != undefined) {
//          select = select.toUpperCase(); // to prevent the error when calling toUpperCase Function
//      } else {
//          select = "";
//      }
//      if (LoanPurposeCount == 1 || LoanPurposeCount == 0) {
//          $('#divLOC').hide();
//          $('#divLoanPurpose').hide();
//          $('#divDescription').show();
//      }
//      else if (select.indexOf('OTHER') != -1)
//          $('#divDescription').show();
//      else {
//          $('#divLOC').hide();
//          $('#divLoanPurpose').show();
//          $('#divDescription').hide();
//      }
//    }
//    if (!g_IsHideGMI && selectedText != undefined && selectedText.toUpperCase().indexOf('HMDA') != -1) {
//        $('.div_gmi_section').css('display', 'block');
//    } else {
//        $('.div_gmi_section').css('display', 'none');
//    }
   
//  }
//save applicant without Co/Joint applicant
/*
function finishMainApplicant(element) {
    var currElement = $(element);
    var hasCoApp = $('#ddlHasCoApplicant option:selected').val();
    if (hasCoApp == "N") {
        return saveLoanInfo(document.getElementById('href_save'));
    }
    else {
        var strMessage = '';
        strMessage = ValidateCustomQuestions();
        //validate required dl scan 
        strMessage += validateRequiredDLScan($('#hdRequiredDLScan').val(), true);
        if (strMessage != '') {
            $('#txtErrorMessage').html(strMessage);
            currElement.next().trigger('click'); //go to error dialog
        } else {
            currElement.next().next().trigger('click'); // go to next page
        }
        return true;
    }
}*/
/*
function onIAgreeClick(isCoApp) {
    var errorStr = "";
    errorStr = validateAll($(this),isCoApp);
    if (errorStr != "") { 
        goToDialog(errorStr);
    }else { //no error ->process submit infor
        if (isCoApp) {
            $('#href_save_end').trigger('click');
        }
        else {
            $('#href_save').trigger('click');
        }
       
        if (isSubmitAnswer) {
            //the btnSubmitAnswer is disabled in 30 seconds, and it is enabled back after 30 seconds
            $('input.custom').attr('disabled', true);
            setTimeout(function() {
                $('input.custom').attr('disabled', false);
                $('input.custom').closest('div').removeClass('ui-disabled');
            }, 30000);
            //reset isSubmitAnswer
            isSubmitAnswer = false;
        }
    }
}
*/

function onClickReturnToMainPage(e) {
    saveCancelLoanInfo();   
    clearForms(); //clear data when the page is redicted to the main page
    //return to        
    var sDir = $('#hdRedirectURL').val();
    if (sDir != '') {
        window.location.href = sDir;
    }
	e.preventDefault();
}


function goToDialog(message) {
    if ($("#divErrorPopup").length > 0) {
    	$('#txtErrorPopupMessage').html(message);  //error message are not encoded, endoding will remove html tag
    	$("#divErrorPopup").popup("open", { "positionTo": "window" });
    } else {
    	$('#txtErrorMessage').html(message);  //error message are not encoded, endoding will remove html tag
    	$('#href_show_dialog_1').trigger('click');
    }
}

function goToLastDialog(message) {
	//var decoded_message = $('<div/>').html(message).text(); //message maybe encoded in the config xml so need to decode
    // $('#div_system_message').html(decoded_message); ->already decoded in callback.aspx
    //application completed so clear
	clearForms();
	$('#div_system_message').html(message);

    //docusign window if enabled
	if ($('#div_system_message').find("iframe#ifDocuSign").length === 1) {
		appendDocuSign();
		$("#div_docuSignIframe").append($('#div_system_message').find("iframe#ifDocuSign"));
       
        ////goback to final decision message after docusign is closed
        //$('#div_docuSignIframe').find("iframe#ifDocuSign").on("load", function (evt) {
		//    //the initial page load twice so need this to make sure only capturing url on finish
        //    var src = $(this).attr('src');                    
        //    var flag = ($(this).data("loaded") || "") + "1";
                        
        //    //TODO: using the flag only works for single applicant, should be using status from DocuSigntracker or redirect url
        //    //For application with join, there may be multiple docusign load and many redirect
        //    loginfo('src: ' + src + 'flag: ' + flag);   
        //    if (flag === "111") {            
		//       $('#href_show_last_dialog').trigger('click');
		//    }
		//});
       
		var height = window.innerHeight ||
                 document.documentElement.clientHeight ||
                 document.body.clientHeight;

		var footerHeight = $('.divfooter').height();

		height = height - footerHeight - 64;
		$('#div_docuSignIframe').find("iframe#ifDocuSign").attr("height", height);
		$.mobile.changePage("#popupDocuSign", {
			transition: "fade",
			reverse: false,
			changeHash: false
		});
	}
        //cross-sell link button if enabled
	else {
		$(".xsell-selection-panel .xsell-item-option.btn-header-theme", "#div_system_message").on("mouseenter click", function (ev) {
			var element = $(this);
			if (ev.type != 'click') {
				element.addClass('btnHover');
				element.removeClass('btnActive_on');
				element.removeClass('btnActive_off');
			} else {
				element.removeClass('btnHover');
			}
		}).on("mouseleave", function () {
			handledBtnHeaderTheme($(this));
		});
		$('#href_show_last_dialog').trigger('click');
	}
}
function saveAndFinishLater() {
	var loanInfo = getPersonalLoanInfo(false);
	loanInfo.Task = "SaveIncompletedLoan";
	$.ajax({
		type: 'POST',
		url: 'CallBack.aspx',
		data: loanInfo,
		dataType: 'html',
		success: function (response) {
			if (response == "ok") {
				$("#popNotification div[placeholder='content']").text("Current loan has been saved successfully.");
				$("#popNotification").popup("open", { "positionTo": "window" });
			} else {
				goToDialog('There was an error when processing your request.  Please try the application again.');
			}
		},
		error: function (err) {
			goToDialog('There was an error when processing your request.  Please try the application again.');
		}
	});
	return false;
}
function saveLoanInfo(buttonToTrigger) {   
    if (g_submit_button_clicked) return false;
    //disable submit button before calling ajax to prevent user submit multiple times
    $('#pagesubmit .div-continue-button').addClass('ui-disabled'); 
    //$('#txtErrorMessage').html('');//useless
    var loanInfo = getPersonalLoanInfo(false);
    //save instaTouchPrefillComment
    $instaTouchComment = $('#hdInstaTouchPrefillComment');
    if ($instaTouchComment.val() != undefined) {
        loanInfo.InstaTouchPrefillComment = $instaTouchComment.val();
    }  
    loanInfo = attachGlobalVarialble(loanInfo);
    $.ajax({
        type: 'POST',
        url: 'CallBack.aspx',
        data: loanInfo,
        dataType: 'html',
        success: function (response) {
            //clear instaTouchPrefill 
            if ($instaTouchComment.val() != undefined && $instaTouchComment.val() != "") {
                $instaTouchComment.val("");
            }
			if (response.toUpperCase().indexOf('MLERRORMESSAGE') > -1) {
                //enable submit button 
                $('#pagesubmit .div-continue-button').removeClass('ui-disabled');
        			goToDialog(response);
			} else if (response.toUpperCase().indexOf('WALLETQUESTION') != -1) {
		        displayWalletQuestions(response);
			} else if (response == "blockmaxapps") {
				goToNextPage("#pageMaxAppExceeded");
			} else {
		        goToLastDialog(response);
	        }
        },
        error: function (err) {
            //enable submit button 
            $('#pagesubmit .div-continue-button').removeClass('ui-disabled');         
            goToDialog('There was an error when processing your request.  Please try the application again.');
        }
    });
    return false;
}

function saveCancelLoanInfo() {
    if ($('#hdEnableDisagree').val() != "Y") {
        return false; 
    }

    var loanInfo = getPersonalLoanInfo(true);
    //if  data is not available then don't save data
    if (loanInfo.SSN == "" || loanInfo.DOB == "" || loanInfo.EmailAddr == "") {
        return false; 
    }
    loanInfo = attachGlobalVarialble(loanInfo);
	$.ajax({
        type: 'POST',
        url: 'CallBack.aspx',
        data: loanInfo,
        dataType: 'html',
        success: function (response) {
               //submit sucessfull so clear
                clearForms();
        },     
    });
    $.mobile.loading('hide'); //hide the ajax message
    return false;
}
function getPersonalLoanInfo(disagree_check) {
    var appInfo = new Object();
    appInfo.PlatformSource = $('#hdPlatformSource').val();
    appInfo.LenderRef = $('#hfLenderRef').val();
    appInfo.Task = "SubmitLoan";
    appInfo.MerchantId = $('#hfMerchantID').val();
    appInfo.ClinicId_enc = $('#hfClinicID_enc').val();
    appInfo.WorkerId_enc = $('#hfWorkerID_enc').val();
    //appInfo.MemberProtectionPlan = '';
    //temporary disabled for now
    //SetMemberProtectionPlan(appInfo);
    if (disagree_check) {
        appInfo.isDisagreeSelect = "Y";
    } else {
        appInfo.isDisagreeSelect = "N";
    }
  /*  if ($('.checkDisclosure').val() != undefined) {
        //get all disclosures 
        var disclosures = "";
        var temp = "";
        $('.checkDisclosure').each(function() {
            temp = $(this).text().trim().replace(/\s+/g, " ");
            disclosures += temp + "\n\n";
        });
        appInfo.Disclosure = disclosures;
    }*/
    if (parseDisclosures() != "") {
        appInfo.Disclosure = parseDisclosures();
    }
    //branchID
    appInfo.BranchID = getBranchId();
    appInfo.LoanOfficerID = $('#hfLoanOfficerID').val();
    appInfo.ReferralSource = $('#hfReferralSource').val();
    appInfo.LoanAmount = Common.GetFloatFromMoney($("#txtLoanAmount").val());
    appInfo.IsLOC = $("#hdIsLineOfCredit").val();
	//appInfo.IsLOC = $('#ddlIsLineOfCredit option:selected').val();
    if (appInfo.IsLOC == 'Y') {
    	appInfo.LoanPurpose = $("#hdLineOfCredit").val();
    	if (!$.isEmptyObject(_LINEOFCREDIT) && _LINEOFCREDIT[appInfo.LoanPurpose]) {
		    appInfo.LoanTypeCategory = _LINEOFCREDIT[appInfo.LoanPurpose].Category;
	    } else {
		    appInfo.LoanTypeCategory = "";
	    }
        appInfo.Term = 0;
    } else {
    	appInfo.LoanPurpose = $("#hdLoanPurpose").val();
    	if (!$.isEmptyObject(_LOANPURPOSE) && _LOANPURPOSE[appInfo.LoanPurpose]) {
		    appInfo.LoanTypeCategory = _LOANPURPOSE[appInfo.LoanPurpose].Category;
	    } else {
		    appInfo.LoanTypeCategory = "";
	    }
    	
        appInfo.LoanTerm = getLoanTerm();
    }
    toggleDescriptionTextbox($.trim(appInfo.LoanPurpose) === "" || appInfo.LoanPurpose.toUpperCase().indexOf("OTHER") > -1);
    appInfo.Description = ($("#txtDescription").val() || "");
    
    if ($("#txtLocationPoolCode").length > 0) {
    	appInfo.SelectedLocationPool = JSON.stringify(ZIPCODEPOOLLIST[$("#txtLocationPoolCode").val()]);
    } else if ($("#pl_txtLocationPoolCode").length > 0) {
    	appInfo.SelectedLocationPool = JSON.stringify(PLPRODUCTS.ZipPoolIds[$("#pl_txtLocationPoolCode").val()]);
    }
    appInfo.depositAmount = 0;
    appInfo.IsComboMode = $("#hdIsComboMode").val();
    appInfo.HasSSOIDA = $('#hdHasSSOIDA').val();
    if (appInfo.IsComboMode == "Y") {
        appInfo.XABranchID = getXAComboBranchId();
        appInfo.ProceedXAAnyway = $("#hdProceedMembership").val();
        if(!$('#div_fom_section').hasClass("hidden")) {
            setFOM(appInfo);
        }
        if ($("#divSelectedProductPool").length === 1 && $("#divSelectedProductPool").parent().hasClass("hidden") == false) {
            appInfo.SelectedProducts = JSON.stringify(loanProductFactory.collectProductSelectionData());
        }
        if ($("#divCcFundingOptions").length === 1) {
            appInfo.depositAmount = Common.GetFloatFromMoney($('#txtFundingDeposit').val());
            FS1.loadValueToData();
            appInfo.rawFundingSource = JSON.stringify(FS1.Data);
        }
    } 


    //get loan ida
    appInfo.idaMethodType = $('#hdIdaMethodType').val();
    // Custom Ansers
	appInfo.CustomAnswers = JSON.stringify(
		loanpage_getAllCustomQuestionAnswers()
			.concat(getAllCustomQuestionAnswers())
			.concat(co_getAllCustomQuestionAnswers())
			.concat(reviewpage_getAllCustomQuestionAnswers()));
	if (typeof reviewpage_getAllUrlParaCustomQuestionAnswers == "function") {
		var UrlParaCustomQuestions = reviewpage_getAllUrlParaCustomQuestionAnswers();
        //add UrlParamCustomQuestionAndAnswer if it exist                  
        if (UrlParaCustomQuestions.length > 0) {
            appInfo.IsUrlParaCustomQuestion = "Y";
            appInfo.UrlParaCustomQuestionAndAnswers = JSON.stringify(UrlParaCustomQuestions);
        }
    }
    // Main-App
    // Applicant Info
    SetApplicantInfo(appInfo);
    // Contact Info
    SetContactInfo(appInfo);
    // Asset
    SetAssets(appInfo);
    // Address
    SetApplicantAddress(appInfo);

    //Mailing Adddess
    SetApplicantMailingAddress(appInfo);
    //Previous Address
    SetApplicantPreviousAddress(appInfo);
    if ($("#divDeclarationSection").length > 0) {
    	SetDeclarations(appInfo);
    	appInfo.Declarations = JSON.stringify(appInfo.Declarations);
    	appInfo.AdditionalDeclarations = JSON.stringify(appInfo.AdditionalDeclarations);
    }
    if ($("#hdEnableIDSection").val() === "Y") {
    	SetApplicantID(appInfo);
	}

    //cuna Protection Info
    if (typeof setProtectionInfo !== "undefined") {
        setProtectionInfo(appInfo, false);
    }
	//reference information
	if ($("#hdEnableReferenceInformation").val() == "Y") {
		setReferenceInfo(appInfo);
	}
	// GMI
    //SetGMIInfo(appInfo);
    // Financial
    SetApplicantFinancialInfo(appInfo);
    var hasCoApp = $("#hdPlHasCoApplicant").val().toUpperCase();
    //upload document data and info
    var docUploadInfo = {};
    if (typeof docUploadObj != "undefined" && docUploadObj != null) {
    	docUploadObj.setUploadDocument(docUploadInfo);
    }
    if (typeof co_docUploadObj != "undefined" && co_docUploadObj != null) {
    	co_docUploadObj.setUploadDocument(docUploadInfo);
    }
    if (docUploadInfo.hasOwnProperty("sDocArray") /*&& docUploadInfo.sDocArray != null && docUploadInfo.sDocArray.length > 0 */ && docUploadInfo.hasOwnProperty("sDocInfoArray")) {
    	appInfo.Image = JSON.stringify(docUploadInfo.sDocArray);
    	appInfo.UploadDocInfor = JSON.stringify(docUploadInfo.sDocInfoArray);
    }
    appInfo.HasCoApp = hasCoApp;
    if (window.ASI) {
    	ASI.FACTORY.setApplicantAdditionalInfo(appInfo);
    }
    //appInfo.CoAppJoin = $("#ddlCoApplicantJoint option:selected").val();
    if (appInfo.HasCoApp == 'Y') {
        // Co-App
        // Co-Applicant Info
        co_SetApplicantInfo(appInfo);
        // Co-Contact Info
        co_SetContactInfo(appInfo);
        // Co-Assets
        co_SetAssets(appInfo);
        // Co-Address
        co_SetApplicantAddress(appInfo);
        // co -Mailing Address
        co_SetApplicantMailingAddress(appInfo);
        //Co-Previous Address
        co_SetApplicantPreviousAddress(appInfo);

    	//co-reference information
        if ($("#hdEnableReferenceInformationForCoApp").val() == "Y") {
        	co_setReferenceInfo(appInfo);
        }
        

        //Identification
        if ($("#hdEnableIDSectionForCoApp").val() === "Y") {
        	co_SetApplicantID(appInfo);
        }
        // GMI
        //co_SetGMIInfo(appInfo);
        // Co-Financial
        co_SetApplicantFinancialInfo(appInfo);
    	// Co-Declarations
        if ($("#co_divDeclarationSection").length > 0) {
        	co_SetDeclarations(appInfo);
        	appInfo.co_Declarations = JSON.stringify(appInfo.co_Declarations);
        	appInfo.co_AdditionalDeclarations = JSON.stringify(appInfo.co_AdditionalDeclarations);
        }
    }

    if ($("#hdVendorID").val() != "" && $("#hdUserID").val() != "") {
        appInfo.VendorId = $("#hdVendorID").val();
        appInfo.UserId = $("#hdUserID").val();
    	//clone and truncate sensitive data
        var dataObj = JSON.stringify(appInfo);
        dataObj = JSON.parse(dataObj);
        dataObj.SSN = "";
        dataObj.co_SSN = "";
        if (typeof dataObj.rawFundingSource == "string") {
        	var rawFsObj = JSON.parse(dataObj.rawFundingSource);
        	rawFsObj.cCreditCardNumber = "";
        	rawFsObj.cExpirationDate = "";
        	rawFsObj.cCVNNumber = "";
        	rawFsObj.cCreditCardNumber = "";
        	rawFsObj.cCreditCardNumber = "";
        	dataObj.rawFundingSource = JSON.stringify(rawFsObj);
        }
        appInfo.rawData = JSON.stringify(dataObj);
    }

    return appInfo;
}
/*
function clearAllInfo() {
    $("input[type='text']").val('');
    $("select").each(function() {
        $(this).get(0).selectedIndex = 0;
        $(this).parent().find('span.ui-btn-text').html('');
    });
}*/
function getLoanTerm() {
    var txtLoanTerm = $('#ddlLoanTerm option:selected').val();
    if ($('#txtLoanTerm').length > 0) {
        txtLoanTerm = $('#txtLoanTerm').val();
    }
    return $.trim(txtLoanTerm);
}

/*deprecated*/
function checkValidScreen1() {
    var strMessage = '';
    //var txtLoanAmount = $('#txtLoanAmount').val();
    //if (!Common.ValidateText(txtLoanAmount))
    //    strMessage += 'Requested Loan Amount is required<br />';

    //if ($('#hdIsLineOfCredit').val().toUpperCase() == "Y") {
    //    if (!Common.ValidateText($('#hdLineOfCredit').val())) {
    //        strMessage += 'Loan Purpose is required<br />';
    //    }
    //} else {
    //    var hasLoanPurpose = $('#hdHasLoanPurpose').val();
    //    //if (getLoanTerm().trim() === "") {
    //    //    strMessage += 'Loan Term is required<br />';
    //    //}
    //    if (hasLoanPurpose == 'Y') {
    //        if (!Common.ValidateText($('#hdLoanPurpose').val())) {
    //            strMessage += 'Loan Purpose is required<br />';
    //        }
    //    }
    //}
    var loanInfoValidate = $.lpqValidate("ValidateProvideLoanInfo");
    var branchValidate = $.lpqValidate("ValidateBranch");
    if (loanInfoValidate === false || branchValidate === false) {
	    strMessage += "Please complete all of the required field(s)<br/>";
    }

    //validate branch 
    //strMessage = validateBranchNames() + strMessage;

  /* var ddlBranches = $('#ddlBranches option:selected').val();
    var iBranchVisible = $('#PLBranchesDiv').length;
    if (!Common.ValidateText(ddlBranches) && iBranchVisible != 0) {
        strMessage += 'Please select a branch.<br />';
    }*/
    return strMessage;
}

function registerProvideLoanInfoValidator() {
	$('#txtLoanAmount').observer({
		validators: [
			function (partial) {
			    var sValue = $(this).val();
			    if (Common.ValidateText(sValue) == false) {
			        return "Loan Amount is required";
			    } else {
			       if (Common.GetFloatFromMoney(sValue) <= 0) {
			             return "Loan Amount is invalid";
			        }
			    }
				return "";
			}
		],
		validateOnBlur: true,
		group: "ValidateProvideLoanInfo"
	});
	$('#txtLoanTerm').observer({
		validators: [
			function (partial) {
			    if ($('#hdIsLineOfCredit').val().toUpperCase() == "Y") return "";

			    //if loanterm is hidden -->no validation
			    if ($('#divLoanTerm').is(":hidden")) return "";

			    var loanTerm = getLoanTerm();
				if (Common.ValidateText(loanTerm) == false) {
				    return "Loan Term is required";
				} else {
				    if (isNaN(loanTerm)) {
				        return "Loan Term is invalid";
				    } else {
				        if (Number(loanTerm) <= 0) {
				            return "Loan Term is invalid";
				        }
				    }
				}
          
				return "";
			}
		],
		validateOnBlur: true,
		group: "ValidateProvideLoanInfo"
	});
	$('#ddlLoanTerm').observer({
		validators: [
			function (partial) {
				if ($('#hdIsLineOfCredit').val().toUpperCase() == "Y") return "";
				if (Common.ValidateText(getLoanTerm()) == false) {
					return "Loan Term is required";
				}
				return "";
			}
		],
		validateOnBlur: true,
		group: "ValidateProvideLoanInfo"
	});
	//$('#ddlBranchName').observer({
	//	validators: [
	//		function (partial) {
	//			if ($('#hdVisibleBranch').val() != "Y" || $('#hdRequiredBranch').val() == "N") { //skip validation
	//				return "";
	//			}
	//			if ($('#ddlBranchName').find('option').length > 0) {
	//				if (!Common.ValidateText($('#ddlBranchName option:selected').val())) {
	//					return "Please select Branch Location";
	//				}
	//			}
	//			return "";
	//		}
	//	],
	//	validateOnBlur: true,
	//	group: "ValidateProvideLoanInfo"
	//});
	$('#divLoanPurposePanel').observer({
		validators: [
			function (partial) {
				if ($(this).find("a[data-command]:visible").length == 0) return "";
				if ($('#hdIsLineOfCredit').val().toUpperCase() == "Y") {
					if (!Common.ValidateText($('#hdLineOfCredit').val())) {
						return 'Loan Purpose is required';
					}
				} else {
					var hasLoanPurpose = $('#hdHasLoanPurpose').val();
					if (hasLoanPurpose == 'Y') {
						if (!Common.ValidateText($('#hdLoanPurpose').val())) {
							return 'Loan Purpose is required';
						}
					}
				}
				return "";
			}
		],
		validateOnBlur: false,
		group: "ValidateProvideLoanInfo"
	});
	$('#divOverdraftQuestion').observer({
		validators: [
			function (partial) {
				if ($("#hdIsLineOfCredit").val() == "Y" && $(this).find("a[data-command].active").length == 0) return "Please answer this question";
				return "";
			}
		],
		validateOnBlur: false,
		group: "ValidateProvideLoanInfo"
	});
}
function registerViewPersonalLoanInfoValidator() {
    $('#reviewPanel').observer({
        validators: [
            function (partial) {
                //second loan app is not available in personal loan, so do the view applicant information validation 
                //instead of view personal loan info validation in case some lenders do not have personal loan info
                if ($('#reviewPanel .ViewAcountInfo').children().find("div").length == 0) {
                    return "Please complete all required fields. ";
                }
                return "";
            }
        ],
        validateOnBlur: false,
        group: "ValidateViewPersonalLoanInfo"
    });
}
function prepareAbrFormValues(pageId) {
	var loanInfo = getPersonalLoanInfo(false);
	var formValues = {};
	_.forEach(pagePaths, function (p) {
		collectFormValueData(p.replace("#", ""), loanInfo, formValues);
		if (pageId == p.replace("#", "")) return false;
	});
	return formValues;
}
/**
 * Converts the loanInfo, which is normally used on submission, into a data structure meant for Applicant Blocking Logic (ABR).
 * This also only pulls in the information needed for each page. ABR should only work with data on the page the user is on
 * and the pages before that.
 * @param {string} pageId
 * @param {Object} loanInfo
 * @param {Object} formValues
 */
function collectFormValueData(pageId, loanInfo, formValues) {
	switch (pageId) {
		case "pl1":
			if ($("#txtLocationPoolCode").length > 0) {
				formValues["locationPool"] = ZIPCODEPOOLLIST[$("#txtLocationPoolCode").val()];
			} else if ($("#pl_txtLocationPoolCode").length > 0) {
				formValues["locationPool"] = PLPRODUCTS.ZipPoolIds[$("#pl_txtLocationPoolCode").val()];
			}
			formValues["amountRequested"] = loanInfo.LoanAmount;
			formValues["isLineOfCredit"] = (loanInfo.IsLOC == "Y");
			if ((loanInfo.IsLOC == "Y")) {
				formValues["loanPurposeLoc"] = [loanInfo.LoanPurpose];
			} else {
				formValues["loanPurposeNonLoc"] = [loanInfo.LoanPurpose];
			}

			collectCustomQuestionAnswers("Application", "LoanPage", null, loanInfo, formValues);
			collectCustomQuestionAnswers("Applicant", "LoanPage", null, loanInfo, formValues);

			break;
		case "pl2":
            var _dob = moment(loanInfo.DOB, "MM-DD-YYYY");
            if (Common.IsValidDate(loanInfo.DOB)) {
				formValues["age"] = moment().diff(_dob, "years", false);
			}
			formValues["citizenship"] = loanInfo.CitizenshipStatus;
			formValues["employeeOfLender"] = loanInfo.EmployeeOfLender;
			formValues["employmentLength"] = (parseInt(loanInfo.txtEmployedDuration_year) || 0) + (parseInt(loanInfo.txtEmployedDuration_month) || 0) / 12;
			formValues["employmentStatus"] = loanInfo.EmploymentStatus;
			formValues["isJoint"] = (loanInfo.HasCoApp == "Y");
			formValues["memberNumber"] = loanInfo.MemberNumber;
			formValues["occupancyLength"] = loanInfo.LiveMonths / 12;
			formValues["occupancyStatus"] = loanInfo.OccupyingLocation;

			collectCustomQuestionAnswers("Applicant", "ApplicantPage", "", loanInfo, formValues);
			
			break;
		case "pl6":
            var _dob = moment(loanInfo.co_DOB, "MM-DD-YYYY");
            if (Common.IsValidDate(loanInfo.co_DOB)) {
				formValues["age"] = moment().diff(_dob, "years", false);
			}
			formValues["citizenship"] = loanInfo.co_CitizenshipStatus;
			formValues["employeeOfLender"] = loanInfo.co_EmployeeOfLender;
			formValues["employmentLength"] = (parseInt(loanInfo.co_txtEmployedDuration_year) || 0) + (parseInt(loanInfo.co_txtEmployedDuration_month) || 0) / 12;
			formValues["employmentStatus"] = loanInfo.co_EmploymentStatus;
			formValues["memberNumber"] = loanInfo.co_MemberNumber;
			formValues["occupancyLength"] = loanInfo.co_LiveMonths / 12;
			formValues["occupancyStatus"] = loanInfo.co_OccupyingLocation;

			collectCustomQuestionAnswers("Applicant", "ApplicantPage", "co_", loanInfo, formValues);

			break;
		case "pagesubmit":
			
			collectCustomQuestionAnswers("Application", "ReviewPage", null, loanInfo, formValues);
			collectCustomQuestionAnswers("Applicant", "ReviewPage", null, loanInfo, formValues);

			break;
		default:
			break;
	}
	return formValues;
}
function validateScreen1(element) {
	var currElement = $(element);
	if (currElement.attr("contenteditable") == "true") return false;
	var validator = true;
	var loanInfoValidate = $.lpqValidate("ValidateProvideLoanInfo");
	var branchValidate = $.lpqValidate("ValidateBranch");
	if (loanInfoValidate === false || branchValidate === false) {
		validator = false;
	}


    //if (!Common.ValidateDisclosures('disclosures')) {
    //    strMessage += 'All disclosures need to be checked<br />';
    //}
    //validate required dl scan
    //strMessage += validateRequiredDLScan($('#hdRequiredDLScan').val(),false);
    //validateVerifyCode 
	// strMessage += validateVerifyCode();
    if ($("#pl1").find("#divShowFOM").length > 0 && $.lpqValidate("ValidateFOM") == false) {
    	validator = false;
    }
    if ($("#pl1	").find("#divDisclosure").length > 0 && $.lpqValidate("ValidateDisclosure_disclosures") == false) {
    	validator = false; //don't need to set strMessage bc it will be remove in the next update
	}

	// Validate custom questions, which may be moved to the front page
	if ($('#loanpage_divApplicantQuestion').children().length > 0) {
		if ($.lpqValidate("loanpage_ValidateApplicantQuestionsXA") === false) {
			validator = false;
		}
	}

    if (validator == false) {
	    Common.ScrollToError();
    } else {
    	if (window.ABR) {
    		var formValues = prepareAbrFormValues("pl1");
    		if (ABR.FACTORY.evaluateRules("pl1", formValues)) {
    			return false;
    		}
    	}

    	var runBeforeGoToInfoPageCallback = function() {
			var selectedText = "";
			var selectedValue = "";
			if ($("#hdIsLineOfCredit").val() === "Y") {
				selectedText = _LINEOFCREDIT.length > 0 ? _LINEOFCREDIT[$("#hdLineOfCredit").val()].Text : '';
				selectedValue = $("#hdLineOfCredit").val();
			} else {
				selectedText = _LOANPURPOSE.length > 0 ? _LOANPURPOSE[$("#hdLoanPurpose").val()].Text : '';
				selectedValue = $("#hdLoanPurpose").val();
			}
			//check both value and text
			if ((!g_IsHideGMI && selectedText != undefined && selectedText.toUpperCase().indexOf('HMDA') != -1)
				|| (!g_IsHideGMI && selectedValue != undefined && selectedValue.toUpperCase().indexOf('HMDA') != -1)) {
				$('.div_gmi_section').css('display', 'block');
			} else {
				$('.div_gmi_section').css('display', 'none');
			}
	    }

    	if (window.IST) {
    		IST.FACTORY.carrierIdentification(runBeforeGoToInfoPageCallback);
	    } else {
		    runBeforeGoToInfoPageCallback();
	    	currElement.next().next().trigger('click');
	    }
    }
}

//function validateScreen2(element, isCoApp) {
//    var currElement = $(element);
//    $('#txtErrorMessage').html('');
//    isCoApp = typeof isCoApp !== 'undefined' ? isCoApp : false;
//    var strMessage = '';
//    if (isCoApp) {
//        strMessage = co_ValidateApplicantInfo();
//        strMessage += co_ValidateApplicantAddress();
//        strMessage += co_ValidateApplicantPreviousAddress();
//        strMessage += co_ValidateApplicantMailingAddress();
//    } else {
//        strMessage = ValidateApplicantInfo();
//        strMessage += ValidateApplicantAddress();
//        strMessage += ValidateApplicantPreviousAddress();
//        strMessage += ValidateApplicantMailingAddress();
//    }
//    if (strMessage != '') {
//        $('#txtErrorMessage').html(strMessage);
//        currElement.next().trigger('click');
//    } else {
//        currElement.next().next().trigger('click');
//    }
//    return strMessage;
//}


function validateScreen2(element) {
    var currElement = $(element);

    //update hiddenHasCoApplicant
    if (currElement.attr('id') == 'continueWithCoApp') {
        $("#hdPlHasCoApplicant").val('Y');
    } else {
        $("#hdPlHasCoApplicant").val('N');
    }

	if (currElement.attr("contenteditable") == "true") return false;
    //$('#txtErrorMessage').html('');//useless
    var strMessage = '';
    var validator = true;
    if (ValidateApplicantInfo() == false) {
    	validator = false;
    }
  
    if (ValidateApplicantContactInfoXA() == false) {
    	validator = false;
    }
    if (ValidateApplicantAddress() == false) {
    	validator = false;
    }
	if ($("#hdEnableReferenceInformation").val() == "Y") {
		if (ValidateReferenceInfo() == false) {
			validator = false;
		}
	}
	if (ValidateApplicantPreviousAddress() == false) {
    	validator = false;
    }
    if (ValidateApplicantMailingAddress() == false) {
    	validator = false;
    }
    
    if ($("#hdEnableIDSection").val() === "Y") {
    	if (ValidateApplicantIdXA() == false) {
    		validator = false;
    	}
    }
    if (ValidateApplicantFinancialInfo() == false) {
    	validator = false;
    }
    if (typeof docUploadObj != "undefined" && docUploadObj != null && docUploadObj.validateUploadDocument() == false) {
    	validator = false;
    }
    if(ValidateAssets() == false){
        validator = false;
    }

    //strMessage += validateUploadDocument();
    if ($('#divApplicantQuestion').children().length > 0) {
        if ($.lpqValidate("ValidateApplicantQuestionsXA") === false) {
            validator = false;
        }
    }
    var requiredDLMessage = validateRequiredDLScan($('#hdRequiredDLScan').val(), false);
    if (requiredDLMessage != "") {
    	strMessage = requiredDLMessage;
    	validator = false;
    }
    if ($("#divDeclarationSection").length > 0 && $.lpqValidate("ValidateDeclaration") === false) {
    	validator = false;
    }
    if (validator === false) {
    	//for backward compatibility, need to show error popup/modal for some function which is not applied inline error message
    	if (strMessage !== "") {
    		if ($("#divErrorPopup").length > 0) {
    			$('#txtErrorPopupMessage').html(strMessage);
    			$("#divErrorPopup").popup("open", { "positionTo": "window" });
    		} else {
    			$('#txtErrorMessage').html(strMessage);
    			currElement.next().trigger('click');
    		}
    	} else {
    		Common.ScrollToError();
    	}
    } else {
    	if (window.ABR) {
    		var formValues = prepareAbrFormValues("pl2");
    		if (ABR.FACTORY.evaluateRules("pl2", formValues)) {
    			return false;
    		}
    	}
    	if ($("#hdPlHasCoApplicant").val() === "Y") {
    		goToNextPage("#pl6");
    	} else {
    		if (window.ASI) {
    			var appInfo = {};
    			SetApplicantInfo(appInfo);
    			SetApplicantAddress(appInfo);
    			ASI.FACTORY.runNssWf1(appInfo.AddressState, appInfo.MaritalStatus, goToPageSubmit);
    		} else {
    			goToPageSubmit();
    		}
    	}
    }
}

function validateScreen6(element) {
	var currElement = $(element);
	if (currElement.attr("contenteditable") == "true") return false;
    $('#txtErrorMessage').html('');
    var strMessage = '';
    var validator = true;
    if (co_ValidateApplicantInfo() == false) {
	    validator = false;
    }
    if (co_ValidateApplicantContactInfoXA() == false) {
    	validator = false;
    }
    if (co_ValidateApplicantAddress() == false) {
    	validator = false;
    }
    if (co_ValidateApplicantPreviousAddress() == false) {
    	validator = false;
    }
    if (co_ValidateApplicantMailingAddress() == false) {
    	validator = false;
    }
    
    if ($("#hdEnableReferenceInformationForCoApp").val() == "Y") {
    	if (co_ValidateReferenceInfo() == false) {
    		validator = false;
    	}
    }

    if ($("#hdEnableIDSectionForCoApp").val() === "Y") {
    	if (co_ValidateApplicantIdXA() == false) {
    		validator = false;
    	}
    }
    if (co_ValidateApplicantFinancialInfo() == false) {
    	validator = false;
    }
    if (typeof co_docUploadObj != "undefined" && co_docUploadObj != null && co_docUploadObj.validateUploadDocument() == false) {
    	validator = false;
    }
    if(co_ValidateAssets() == false){
        validators = false;
    }
    
    if ($('#co_divApplicantQuestion').children().length > 0) {
        if ($.lpqValidate("co_ValidateApplicantQuestionsXA") === false) {
            validator = false;
        }
    }

    var requiredDLMessage = validateRequiredDLScan($('#hdRequiredDLScan').val(), true);
    if (requiredDLMessage != "") {
    	strMessage = requiredDLMessage;
	    validator = false;
    }
    if ($("#co_divDeclarationSection").length > 0 && $.lpqValidate("co_ValidateDeclaration") === false) {
    	validator = false;
    }
    if (validator === false) {
    	//for backward compatibility, need to show error popup/modal for some function which is not applied inline error message
    	if (strMessage !== "") {
    		if ($("#divErrorPopup").length > 0) {
    			$('#txtErrorPopupMessage').html(strMessage);
    			$("#divErrorPopup").popup("open", { "positionTo": "window" });
    		} else {
    			$('#txtErrorMessage').html(strMessage);
    			currElement.next().trigger('click');
    		}
    	} else {
    		Common.ScrollToError();
    	}
    } else {
    	if (window.ABR) {
    		var formValues = prepareAbrFormValues("pl6");
    		if (ABR.FACTORY.evaluateRules("pl6", formValues)) {
    			return false;
    		}
    	}
    	if (window.ASI) {
    		var appInfo = {};
    		SetApplicantInfo(appInfo);
    		SetApplicantAddress(appInfo);
    		co_SetApplicantInfo(appInfo);
    		co_SetApplicantAddress(appInfo);
    		ASI.FACTORY.runNssWf2(appInfo.AddressState, appInfo.MaritalStatus, appInfo.co_RelationshipToPrimary, appInfo.co_AddressState, appInfo.co_MaritalStatus, goToPageSubmit);
    	} else {
    		goToPageSubmit();
    	}
    }
}
/*
function validateFinancialInfoScreen(element, isCoApp) {
    var currElement = $(element);
    $('#txtErrorMessage').html('');
    isCoApp = typeof isCoApp !== 'undefined' ? isCoApp : false;

    var strMessage = '';
    if (isCoApp) {
        strMessage = co_ValidateApplicantFinancialInfo();
        strMessage += co_validateUploadDocument();
    } else {
        strMessage = ValidateApplicantFinancialInfo();
        strMessage +=validateUploadDocument();
    }
    if (strMessage != '') {
        $('#txtErrorMessage').html(strMessage);
        if (disagree_check != true)
            currElement.next().trigger('click');
    } else {
            currElement.next().next().trigger('click');
    }
        return strMessage;
}
*/
//function LOCTrigger() {
//    var $btn = $("#btnLineOfCredit");
//    if ($btn.hasClass("active")) {
//        $("#hdIsLineOfCredit").val("Y");
//        $('#divLoanTerm').hide();
//        $('#divLOC').show();
//        $('#divLoanPurpose').hide();
//    } else {
//        $("#hdIsLineOfCredit").val("N");
//        $('#divLoanTerm').show();
//        $('#divLOC').hide();
//        $('#divLoanPurpose').show();
//    }
//    showDescription();
//}
/*
function SwitchLineOfCreditButton(sender, refreshSlider) {
   
    if (sender == null) {
        $('#divLoanTerm').show();
        $('#divLOC').hide();
        $('#divLoanPurpose').show();
    }
    else if (sender.options[0].selected) {
        $('#divLoanTerm').show();
        $('#divLOC').hide();
        $('#divLoanPurpose').show();

    } else {
        $('#divLoanTerm').hide();
        $('#divLOC').show();
        $('#divLoanPurpose').hide();
    }
    showDescription();
}*/
/*
function validateAll(element, isCoApp) {
    // Validate all the screens again before submitting.
    var errorStr = "";
    if (validateScreen1(element) != "") {
        errorStr += "Please complete all required fields for personal loan information.<br/>";
    }
   
    if (ValidateApplicantInfo() != "") {
        errorStr += "Please complete all required fields for applicant information.<br/>";
    }
    if (ValidateApplicantAddress() != "") {
        errorStr += "Please complete all required fields for current address.<br/>";
    }
    if (ValidateApplicantFinancialInfo() != "") {
        errorStr += "Please complete all required fields for financial information.<br/>";
    }
    if (ValidateCustomQuestions() != "") {
        errorStr += ValidateCustomQuestions();
    }
    if (isCoApp) {
       
        if (co_ValidateApplicantInfo() != "") {
            errorStr += "Please complete all required fields for Co-Applicant information.<br/>";
        }
        if (co_ValidateApplicantAddress() != "") {
            errorStr += "Please complete all required fields for Co-Applicant current address.<br/>";
        }
        if (co_ValidateApplicantFinancialInfo() != "") {
            errorStr += "Please complete all required fields for Co-Applicant financial information.<br/>";
        }
    }
    return errorStr;
}*/
//**********start wallet questions and answers ************
function displayWalletQuestions(message) {
    // The first two characters of the message contains the number of questions
    var numQuestions = parseInt(message.substr(0, 2), 10);
    var questionsHTML = message.trim().substr(2, message.length - 2);
    // Put the number of questions into the hidden input field
    $('#hdNumWalletQuestions').val(numQuestions);
    // Put the HTML for the questions into the proper location in our page
    $('#walletQuestionsDiv').html(questionsHTML);
    nextpage = $('#walletQuestions');
    $.mobile.changePage(nextpage, {
        transition: "slide",
        reverse: false,
        changeHash: true
    });
}
//jquery mobile need to transition to another page in order to format the html autheitcation question correctly
function co_displayWalletQuestions(message) {
    // The first two characters of the message contains the number of questions
    var numQuestions = parseInt(message.substr(0, 2), 10);
    var questionsHTML = message.substr(2, message.length - 2);
    // Put the number of questions into the hidden input field
    $('#hdNumWalletQuestions').val(numQuestions);
    // Put the HTML for the questions into the proper location in our page   
    $('#walletQuestionsDiv').empty();
    $('#co_walletQuestionsDiv').html(questionsHTML);
    nextpage = $('#co_walletQuestions');
    $.mobile.changePage(nextpage, {
        transition: "slide",
        reverse: false,
        changeHash: true
    });
}
function validateWalletQuestions(element, isCoApp) {
	if ($(element).attr("contenteditable") == "true") return false;  
    // Make sure that there is a selected answer for each question.
    // After validating the questions, submit them
    var index = 0;
    var numSelected = 0;
    var errorStr = "";
    var numOfQuestions = $('#hdNumWalletQuestions').val();
    numOfQuestions = parseInt(numOfQuestions, 10);
    for (index = 1; index <= numOfQuestions; index++) {
        // Check to see if a radio button was selected
        var radioButtons = $('input[name=walletquestion-radio-' + index.toString() + ']');
        var numRadioButtons = radioButtons.length;
        for (var i = 0; i < numRadioButtons; i++) {
            if ($(radioButtons[i]).prop('checked') == true) {
                numSelected++;
            }
        }
        if (numSelected == 0) {
            errorStr += "Please answer all the questions";
            break;
        }
        numSelected = 0;
    }
    if (errorStr != "") {
    	// An error occurred, we need to bring up the error dialog.
    	if ($("#divErrorPopup").length > 0) {
    		$('#txtErrorPopupMessage').html(errorStr);
    		$("#divErrorPopup").popup("open", { "positionTo": "window" });
    	} else {
    		var nextpage = $('#divErrorDialog');
    		$('#txtErrorMessage').html(errorStr);
    		if (nextpage.length > 0) {
    			$.mobile.changePage(nextpage, {
    				transition: "slide",
    				reverse: false,
    				changeHash: true
    			});
    		}
    	}
        //return errorStr;
        return false;
    }
    // No errors, so we can proceed
    submitWalletAnswers(isCoApp);
    return false;
}

function submitWalletAnswers(isCoApp) {
    var coPrefix = "";
    if (isCoApp) coPrefix = "co_";
    var $submitWQButton = $('#' + coPrefix + 'walletQuestions .div-continue-button');
    //disable submit button before calling ajax to prevent user submit multiple times
    $submitWQButton.addClass('ui-disabled');
    //$('#txtErrorMessage').html('');//useless
	var answersObj = getWalletAnswers(isCoApp);
	answersObj = attachGlobalVarialble(answersObj);
	if ($("#hdIsComboMode").val() == "Y") {
	    if ($("#divCcFundingOptions").length === 1) {
	        answersObj.depositAmount = Common.GetFloatFromMoney($('#txtFundingDeposit').val());
	        FS1.loadValueToData();
	        answersObj.rawFundingSource = JSON.stringify(FS1.Data);
	    }
	}
    $.ajax({
        type: 'POST',
        url: 'Callback.aspx',
        data: answersObj,
        dataType: 'html',
        success: function (response) {      
            if (response.toUpperCase().indexOf('MLERRORMESSAGE') > -1) {
                $submitWQButton.removeClass('ui-disabled'); //enable submit button
                goToDialog(response);
            }
            else {
                if (response.toUpperCase().indexOf('MLNOIDAUTHENTICATION') > -1) {
                    isApplicationCompleted = true;
                    goToLastDialog(response);
                } else if (response.toUpperCase().indexOf('WALLETQUESTION') > -1) {
                    co_displayWalletQuestions(response);                 
                } else {
                    //   isApplicationCompleted = true;
                    goToLastDialog(response);                        
                }
            }
        },
        error: function (err) {
            //enable submit answer button 
            $submitWQButton.removeClass('ui-disabled'); 
            goToDialog('There was an error when processing your request.  Please try the application again.');
        }
    });
    return false;
}
function getWalletAnswers(isCoApp) {
    // Grab the answer choices that have been selected by the user
    var appInfo = getPersonalLoanInfo(false);
    /*var index = 0;
    var numSelected = 0;
    var numOfQuestions = $('#hdNumWalletQuestions').val();
    numOfQuestions = parseInt(numOfQuestions, 10);
    var answersArray = new Array();
    for (index = 1; index <= numOfQuestions; index++) {
        var selectedAnswer = $('input[name=walletquestion-radio-' + index.toString() + ']:checked').attr('id');
        if (selectedAnswer != "" && selectedAnswer != null) {
            //exclude "rq"+i+"_" prefix from selectectAnswer
            var rqPrefixLength = 4;
            if (i >= 10) {
                rqPrefixLength = 5;
            }
            answersArray[index - 1] = selectedAnswer.substring(rqPrefixLength, selectedAnswer.length);
        }
    }
    // Put the answers into a semicolon separated string
    // Only the ids of the answers will be passed to the callback code
    var walletAnswersConcatStr = "";
    for (var i = 0; i < numOfQuestions; i++) {
        walletAnswersConcatStr += answersArray[i] + ";";
    }
    // Get rid of the last semicolon
    walletAnswersConcatStr = walletAnswersConcatStr.substr(0, walletAnswersConcatStr.length - 1);
    appInfo.WalletAnswers = walletAnswersConcatStr;
    */
    
    //get wallet questions and answers
    getWalletQuestionsAndAnswers(appInfo);

    // appInfo.CSRF = $('#hfCSRF').val();
    appInfo.LenderRef = $('#hfLenderRef').val();
    appInfo.Task = "WalletQuestions";
    appInfo.idaMethodType = $('#hdIdaMethodType').val();
    appInfo.co_FirstName = $('#co_txtFName').val();
    appInfo.ProceedXAAnyway = $("#hdProceedMembership").val();
    appInfo.IsComboMode = $("#hdIsComboMode").val();
    if (isCoApp) {
        appInfo.hasJointWalletAnswer = "Y";
    } else {
        appInfo.hasJointWalletAnswer = "N";
    }
    return appInfo;
}
//**********end wallet questions and answers ************
//autofile() for testing purpose
function autoFillData() {
	$('#ddlBranchName option').eq(1).prop('selected', true);
	$('#ddlBranchName').selectmenu("refresh").closest("div.ui-btn").addClass("btnActive_on");
	$('#txtLoanAmount').val("2000").trigger("blur");
	$("#divLoanPurposePanel a.btn-header-theme[data-role='button']:eq(0)").trigger("click");
	$("#txtLoanTerm").val("24");
	autoFillData_PersonalInfor();
    co_autoFillData_PersonalInfor();
    autoFillData_Address();
    co_autoFillData_Address();
    autoFillData_FinancialInFor();
    co_autoFillData_FinancialInFor();
    autoFillData_ContactInfor();
    co_autoFillData_ContactInfor();
    if ($("#txtIDDateIssued").length > 0) {
        autoFillData_ID();
    }
    if ($("#co_txtIDDateIssued").length > 0) {
        co_autoFillData_ID();
    }
    if (typeof autofill_Disclosure == "function") {
    	autofill_Disclosure();
    }
    if ($("#divProceedMembership").length > 0) {
        $("a.btn-header-theme[data-command='proceed-membership'][data-key='Y']", "#divProceedMembership").addClass("active btnActive_on");
        $("#hdProceedMembership").val("Y");
    }
    if ($("#divDeclarationSection").length > 0) {
    	autofill_Declarations();
    }
    if ($("#co_divDeclarationSection").length > 0) {
    	co_autofill_Declarations();
    }
}
function bindSavedData(appInfo) {

}
//function validateUploadDocs(element, isCoApp) {
//    var currElement = $(element);
//    var strMessage = "";
//    if (typeof isCoApp == "undefined" || isCoApp == false) {
//        strMessage += validateUploadDocument();
//    } else {
//        strMessage += co_validateUploadDocument();
//    }
//    if (strMessage != '') {
//        $('#txtErrorMessage').html(strMessage);
//        currElement.next().trigger('click');
//    } else {
//        currElement.next().next().trigger('click');
//    }
//}

function ViewPersonalLoanInfo() {
    var strHtml = "";
    var showDescription = false;
    strHtml += '<div class="col-sm-6 col-xs-12"><div class="row"><div class="col-xs-6 text-right row-title"><span class="bold">Line of Credit</span></div><div class="col-xs-6 text-left row-data"><span>' + ($("#hdIsLineOfCredit").val().toUpperCase() === "Y" ? "YES" : "NO") + '</span></div></div></div>';
    strHtml += '<div class="col-sm-6 col-xs-12"><div class="row"><div class="col-xs-6 text-right row-title"><span class="bold">Loan Amount</span></div><div class="col-xs-6 text-left row-data"><span>' + $("#txtLoanAmount").val() + '</span></div></div></div>';

    if ($("#hdIsLineOfCredit").val() != undefined && $('#hdIsLineOfCredit').val().toUpperCase() == "Y") {
        strHtml += '<div class="col-sm-6 col-xs-12"><div class="row"><div class="col-xs-6 text-right row-title"><span class="bold">Loan Purpose</span></div><div class="col-xs-6 text-left row-data"><span>' + _LINEOFCREDIT[$('#hdLineOfCredit').val()].Text + '</span></div></div></div>';
        var locPurposeText = _LINEOFCREDIT[$('#hdLineOfCredit').val()].Text;
        if (locPurposeText != undefined && locPurposeText.toUpperCase().indexOf("OTHER") > -1) {
            showDescription = true;
        }
    } else {
        var hasLoanPurpose = $('#hdHasLoanPurpose').val();
        if (hasLoanPurpose == 'Y') {
            strHtml += '<div class="col-sm-6 col-xs-12"><div class="row"><div class="col-xs-6 text-right row-title"><span class="bold">Loan Purpose</span></div><div class="col-xs-6 text-left row-data"><span>' + _LOANPURPOSE[$('#hdLoanPurpose').val()].Text + '</span></div></div></div>';
            var purposeText = _LOANPURPOSE[$('#hdLoanPurpose').val()].Text;
            if (purposeText != undefined && purposeText.toUpperCase().indexOf("OTHER") > -1) {
                showDescription = true;
            }
        } else {
            showDescription = true;
        }
        if (getLoanTerm().trim() !== "") {
            strHtml += '<div class="col-sm-6 col-xs-12"><div class="row"><div class="col-xs-6 text-right row-title"><span class="bold">Term</span></div><div class="col-xs-6 text-left row-data"><span>' + getLoanTerm().trim() + ' month(s)</span></div></div></div>';
        }
    }
    var ddlBranches = $('#ddlBranches option:selected').val();
    var iBranchVisible = $('#PLBranchesDiv').length;
    if (!Common.ValidateText(ddlBranches) && iBranchVisible != 0) {
        strHtml += '<div class="col-sm-6 col-xs-12"><div class="row"><div class="col-xs-6 text-right row-title"><span class="bold">Branch</span></div><div class="col-xs-6 text-left row-data"><span>' + ddlBranches + '</span></div></div></div>';
    }
    toggleDescriptionTextbox(showDescription);
    var txtDesc = htmlEncode(($("#txtDescription").val() || "").trim());
    if (txtDesc !== "") {
        strHtml += '<div class="col-sm-6 col-xs-12"><div class="row"><div class="col-xs-6 text-right row-title"><span class="bold">Description / Reason</span></div><div class="col-xs-6 text-left row-data"><span>' + txtDesc + '</span></div></div></div>';
    }
    return strHtml;
}

function RenderReviewContent() {

    viewSelectedBranch(); //view selected Branch

    $(".ViewPersonalLoanInfo").html(ViewPersonalLoanInfo.call($(".ViewPersonalLoanInfo")));
    $(".ViewAcountInfo").html(ViewAccountInfo.call($(".ViewAcountInfo")));
	if ($("#hdEnableReferenceInformation").val() == "Y") {
		$(".ViewReferenceInfo").html(ViewReferenceInfo.call($(".ViewReferenceInfo")));
	}

	$(".ViewContactInfo").html(ViewContactInfo.call($(".ViewJointApplicantContactInfo")));
    $(".ViewAddress").html(ViewAddress.call($(".ViewAddress")));
    if ($("#hdEnableIDSection").val() === "Y") {
    	$(".ViewApplicantID").html(ViewPrimaryIdentification.call($(".ViewApplicantID")));
    }
    $(".ViewFinancialInfo").html(ViewFinancialInfo.call($(".ViewFinancialInfo")));
    $(".ViewPrevEmploymentInfo").html(ViewPrevEmploymentInfo.call($(".ViewPrevEmploymentInfo")));
    if ($("#divDeclarationSection").length > 0) {
    	$(".ViewDeclaration").html(ViewDeclaration.call($(".ViewDeclaration")));
    }
    if ($('#divApplicantQuestion').length > 0) {
        $(".ViewApplicantQuestion").html(ViewApplicantQuestion.call($(".ViewApplicantQuestion")));
    }

    if ($("#hdPlHasCoApplicant").val() === "Y") {
        $(".ViewJointApplicantInfo").html(co_ViewAccountInfo.call($(".ViewJointApplicantInfo")));
        if ($("#hdEnableReferenceInformationForCoApp").val() == "Y") {
		    $(".ViewJointReferenceInfo").html(co_ViewReferenceInfo.call($(".ViewJointReferenceInfo")));
	    }
	    $(".ViewJointApplicantContactInfo").html(co_ViewContactInfo.call($(".ViewJointApplicantContactInfo")));
        $(".ViewJointApplicantAddress").html(co_ViewAddress.call($(".ViewJointApplicantAddress")));
        if ($("#hdEnableIDSectionForCoApp").val() === "Y") {
        	$(".ViewJointApplicantID").html(co_ViewPrimaryIdentification.call($(".ViewJointApplicantID")));
        }
        $(".ViewJointApplicantFinancialInfo").html(co_ViewFinancialInfo.call($(".ViewJointApplicantFinancialInfo")));
        $(".ViewJointApplicantPrevEmploymentInfo").html(co_ViewPrevEmploymentInfo.call($(".ViewJointApplicantPrevEmploymentInfo")));
        if ($("#co_divDeclarationSection").length > 0) {
        	$(".ViewJointApplicantDeclaration").html(co_ViewDeclaration.call($(".ViewJointApplicantDeclaration")));
        }
        if ($('#co_divApplicantQuestion').length > 0) {
            $(".ViewJointApplicantQuestion").html(co_ViewApplicantQuestion.call($('.ViewJointApplicantQuestion')));
        }

    } else {
        $(".jna-panel").hide();
    }
    $("div.review-container div.row-title b").each(function (idx, ele) {
		if (typeof $(ele).data("renameid") == "undefined") {
			var dataId = getDataId();
			$(ele).attr("data-renameid", dataId);
			RENAME_REPOSITORY[dataId] = htmlEncode($(ele).html());
		}
    });
}
function goToPageSubmit() {
	RenderReviewContent();
	goToNextPage("#pagesubmit");
}
function hasCoApplicant() {
	return $("#hdPlHasCoApplicant").val() === "Y";
}
function goBackPageSubmit() {
	if (typeof resetFunding == 'function') {
		resetFunding();
	}
	if (window.ASI) {
		ASI.FACTORY.goBackFromPageSubmit();
	} else {
		if ($("#hdPlHasCoApplicant").val() === "Y") {
			goToNextPage("#pl6");
		} else {
			goToNextPage("#pl2");
		}
	}
	
}
function validatePageSubmit(element) {
	var currElement = $(element);
	if (currElement.attr("contenteditable") == "true") return false; 
    var validator = true;
    //if ($("#chkICertified").is(":checked") == false) {
    //    return false;
    //}
    var strMessage = "";

    if ($("#hdIsComboMode").val() === "Y") {
        if (!$('#div_fom_section').hasClass("hidden") && $("#pagesubmit").find("#divShowFOM").length > 0 && $.lpqValidate("ValidateFOM") == false) {
    		validator = false;
    	}
        if ($.lpqValidate("ValidateProceedMembership") == false) {
            validator = false;
        }
        if ($.lpqValidate("ValidateProductSelection") === false) {
        	validator = false;
        }
        
        if (validateFS1() == false) {
        	validator = false;
        }
        
        //strMessage += validateApplicantQuestions();
    }

	// Validate custom questions
	if ($('#reviewpage_divApplicantQuestion').children().length > 0) {
		if ($.lpqValidate("reviewpage_ValidateApplicantQuestionsXA") === false) {
			validator = false;
		}
	}
	
    if ($("#pagesubmit").find("#divDisclosure").length > 0 && $.lpqValidate("ValidateDisclosure_disclosures") == false) {
    	validator = false;
    }
     //validate the view loan info panel, if the panel is empty display error message when clicking submit button
    //don't need to validate all the view panels because if the app is cross sell then it only has loan info and review pages 
    if ($("#reviewPanel").find(".ViewPersonalLoanInfo").length > 0 && $.lpqValidate("ValidateViewPersonalLoanInfo") == false) {
        validator = false;
    }
    //validate verify code
    strMessage += validateVerifyCode();
	//refine error message to remove duplicate item
    var arrErr = strMessage.split("<br/>");
    var refinedErr = [];
    $.each(arrErr, function (i, el) {
    	el = $.trim(el);
    	if (el !== "" && $.inArray(el, refinedErr) === -1) refinedErr.push(el);
    });
    if (refinedErr.length > 0) {
    	validator = false;
    }
    if (validator === false) {
    	if (refinedErr.length > 0) {
    		if ($("#divErrorPopup").length > 0) {
    			$('#txtErrorPopupMessage').html(refinedErr.join("<br/>"));
    			$("#divErrorPopup").popup("open", { "positionTo": "window" });
    		} else {
    			$('#txtErrorMessage').html(refinedErr.join("<br/>"));
    			currElement.next().trigger('click');
    		}
        } else {
            Common.ScrollToError();    		
    	}
    } else {
    	if (window.ABR) {
    		var formValues = prepareAbrFormValues("pagesubmit");
    		if (ABR.FACTORY.evaluateRules("pagesubmit", formValues)) {
    			return false;
    		}
    	}
        saveLoanInfo();      
    }
}
function acceptLaserScanResult(prefix, ele) {
	fillLaserScanResult(prefix, ele);
	$(ele).closest(".scan-result-wrapper").removeClass("open");
	var nextPage = "#pl2";
	if (prefix !== "") {
		nextPage = "#pl6";
	}
	goToNextPage(nextPage);
}




  