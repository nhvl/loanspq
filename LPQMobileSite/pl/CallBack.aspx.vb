﻿Imports CustomList
Imports Newtonsoft.Json
Imports LPQMobile.BO
Imports LPQMobile.Utils
Imports System.Xml
Imports System.Web.Script.Serialization

Partial Class pl_CallBack
	Inherits CBasePage
	Private QualifiedProduct As String
	Private _WorkFlowMode As String = ""
	''Private _log As log4net.ILog = log4net.LogManager.GetLogger("PersonalLoanCallBack")
#Region "property that need to persist"
	''need to use for 2nsd postback
	Property loanRequestXMLStr As String
		Get
			Return Common.SafeString(Session("loanRequestXMLStr"))
		End Get
		Set(value As String)
			Session("loanRequestXMLStr") = value
		End Set
	End Property
	Property loanResponseXMLStr As String
		Get
			Return Common.SafeString(Session("loanResponseXMLStr"))
		End Get
		Set(value As String)
			Session("loanResponseXMLStr") = value
		End Set
	End Property

	Property xaRequestXMLStr As String
		Get
			Return Common.SafeString(Session("xaRequestXMLStr"))
		End Get
		Set(value As String)
			Session("xaRequestXMLStr") = value
		End Set
	End Property
	Property xaResponseXMLStr As String
		Get
			Return Common.SafeString(Session("xaResponseXMLStr"))
		End Get
		Set(value As String)
			Session("xaResponseXMLStr") = value
		End Set
	End Property


	'need to store this for comparision with answer in the next post back
	Property questionList_persist As List(Of CWalletQuestionsRendering.questionDataClass)
		Get
			If Session("pl_questionList") IsNot Nothing Then
				Return CType(Session("pl_questionList"), System.Collections.Generic.List(Of CWalletQuestionsRendering.questionDataClass))
			End If
			Return Nothing
		End Get
		Set(value As List(Of CWalletQuestionsRendering.questionDataClass))
			Session("pl_questionList") = value
		End Set
	End Property

	Property IsJointApplication As Boolean
		Get
			If Common.SafeString(Session("IsJointApplication")) = "Y" Then
				Return True
			Else
				Return False
			End If
		End Get
		Set(value As Boolean)
			If value = True Then
				Session("IsJointApplication") = "Y"
			Else
				Session("IsJointApplication") = Nothing
			End If
		End Set
	End Property
	Property Co_FName As String
		Get
			Return Common.SafeString(Session("co_FirstName"))
		End Get
		Set(value As String)
			Session("co_FirstName") = value
		End Set
	End Property
	Property transaction_ID As String 'same as session_id
		Get
			Return Common.SafeString(Session("transaction_id"))
		End Get
		Set(value As String)
			Session("transaction_id") = value
		End Set
	End Property

	Property question_set_ID As String
		Get
			Return Common.SafeString(Session("question_set_ID"))
		End Get
		Set(value As String)
			Session("question_set_ID") = value
		End Set
	End Property 'same as quiz_id

	Property request_app_ID As String
		Get
			Return Common.SafeString(Session("request_app_ID"))
		End Get
		Set(value As String)
			Session("request_app_ID") = value
		End Set
	End Property
	Property request_client_ID As String
		Get
			Return Common.SafeString(Session("request_client_ID"))
		End Get
		Set(value As String)
			Session("request_client_ID") = value
		End Set
	End Property
	Property request_sequence_ID As String
		Get
			Return Common.SafeString(Session("request_sequence_ID"))
		End Get
		Set(value As String)
			Session("request_sequence_ID") = value
		End Set
	End Property

	Property loanID As String
		Get
			Return Common.SafeString(Session("loanID"))
		End Get
		Set(value As String)
			Session("loanID") = value
		End Set
	End Property
	Property xaID As String
		Get
			Return Common.SafeString(Session("xaID"))
		End Get
		Set(value As String)
			Session("xaID") = value
		End Set
	End Property
	Property loanNumber As String
		Get
			Return Common.SafeString(Session("loanNumberCC"))
		End Get
		Set(value As String)
			Session("loanNumberCC") = value
		End Set
	End Property
	Property reference_number As String
		Get
			Return Common.SafeString(Session("reference_number"))
		End Get
		Set(value As String)
			Session("reference_number") = value
		End Set
	End Property

	Property Decision_v2_Response As String
		Get
			Return Common.SafeString(Session("Decision_v2_ResponseCC"))
		End Get
		Set(value As String)
			Session("Decision_v2_ResponseCC") = value
		End Set
	End Property

	Property DecisionMessage As String
		Get
			Return Common.SafeString(Session("DecisionMessage"))
		End Get
		Set(value As String)
			Session("DecisionMessage") = value
		End Set
	End Property

	Property SelectedProducts As String
		Get
			Return Common.SafeString(Session("SelectedProducts"))
		End Get
		Set(value As String)
			Session("SelectedProducts") = value
		End Set
	End Property

#End Region
	Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
		If IsInEnvironment("LIVE") Then
			Dim editMode As Boolean = Common.SafeBoolean(Request.Form("eMode"))
			If Not String.IsNullOrEmpty(PreviewCode.Trim()) OrElse editMode = True Then
				Response.Clear()
				Response.Write(Common.PrependIconForNonApprovedMessage("[Submission result display here]", "Thank You"))
				Response.End()
				Return
			End If
		End If
		_ProceedXAAnyway = Common.SafeString(Request.Form("ProceedXAAnyway")) = "Y"	'TODO:need to hookup to client side for the btn below FOM
		_WorkFlowMode = Common.getNodeAttributes(_CurrentWebsiteConfig, "PERSONAL_LOAN", "workflow_mode")
		Dim sAutoCreatePDFCode = Common.getNodeAttributes(_CurrentWebsiteConfig, "PERSONAL_LOAN", "pdf_code_for_auto_create")
		If Common.SafeStripHtmlString(Request.Form("Task")) = "SaveIncompletedLoan" Then
			Dim loan As New SmLoan
			loan.LoanSavingID = Common.SafeGUID(Request.Form("LoanSavingID"))
			Dim personalLoanObject = GetPersonalLoanObject(_CurrentWebsiteConfig)
			loan.LenderID = Guid.Parse(_CurrentWebsiteConfig.LenderId)
			loan.LenderRef = _CurrentWebsiteConfig.LenderRef
			loan.VendorID = personalLoanObject.VendorID
			loan.UserID = personalLoanObject.UserID
			loan.ApplicantName = personalLoanObject.Applicants(0).FirstName & " " & personalLoanObject.Applicants(0).LastName
			loan.CreatedDate = Now
			Dim requestNode As XmlNode = Common.getSubmitLoanData(personalLoanObject, _CurrentWebsiteConfig, False).SelectSingleNode("INPUT/REQUEST")
			If requestNode IsNot Nothing Then
				loan.RequestParam = requestNode.InnerText
			End If
			loan.SubmitDate = Nothing
			loan.Status = "INCOMPLETE"
			loan.FundStatus = "NOT FUND"
			loan.RequestParamJson = personalLoanObject.LoanRawData
			loan.LoanType = SmSettings.LenderVendorType.PL
			If IsComboMode Then
				loan.LoanType = SmSettings.LenderVendorType.PL_COMBO
			End If
			Dim smBl As New SmBL()
			Dim result = smBl.SaveInCompletedLoan(loan)
			Response.Clear()
			Response.Write(result)
			Response.End()
			Return
		End If

		'--Combo mode
		If IsComboMode Then
			ExecComboModeFlow()
			Return
		End If

		If (Request.Form("Task") = "SubmitLoan") Then
			If (Not IsPostBack) Then
				Dim IP_ErrMessage As String = ValidateIP()
				If Not String.IsNullOrEmpty(IP_ErrMessage) Then
					Response.Write(IP_ErrMessage)
					log.Info(IP_ErrMessage & "  IP: " & Request.UserHostAddress & "/" & _CurrentLenderRef)
					Return
				End If
				Dim sReason As String = CApplicantBlockLogic.Execute(_CurrentWebsiteConfig, Request, "PL")
				If Not String.IsNullOrEmpty(sReason) Then
					Response.Write(sReason)
					Return
				End If

				Dim personalLoanObject = GetPersonalLoanObject(_CurrentWebsiteConfig)
				Dim loanIDAType As String = Common.SafeString(Request.Form("idaMethodType"))
				Dim isDisagreeSelect = Common.SafeString(Request.Form("isDisagreeSelect")) = "Y"
				''run ida if it is SSO and also sso_ida_enabled is "Y"
				Dim disableSSOIDA = IIf(Common.SafeString(Request.Form("HasSSOIDA")) = "Y", False, IsSSO)
				If String.IsNullOrEmpty(loanIDAType) Or disableSSOIDA Then 'run normal
					''no ida -->run normal process
					''make sure to decode html in xml config
					Dim oResponseXml As String = ""	'<OUTPUT version="1.0"><RESPONSE loan_id="cafda165088143e98200399d95f70126" loan_number="10827"><DECISION validation_id="UXULuYt8vCN0huWjQh3qKpYk7BMDmgZK9iUXIp/Vdw==" status="QUALIFIED"><![CDATA[<PRODUCTS><PL_PRODUCT loan_id="cafda165088143e98200399d95f70126" date_offered="2017-06-20T15:48:06.3924391-07:00" is_prequalified="N" underwriting_service_results_index="0" amount_approved="4000" max_amount_approved="20000" grade="PLOC A PAPER" security_rate_calculation="AVERAGE" payment_mode="LOC" original_payment_percent="2" tier="0" set_loan_system="O" reference_id="" is_single_payment="false" solve_for="PAYMENT" estimated_monthly_payment="80.00" term="0"><RATE rate="9.000" original_rate="7.250" rate_code="A" margin="5.750" index="3.250" rate_floor="9.000" rate_ceiling="18.000" rate_type="V" rate_adjustment_option="INDEX_MARGIN" index_type="" initial_rate="" change_after_days="" /><PRICE_ADJUSTMENTS><PRICE_ADJUSTMENT adjust_type="APR" rule_name="Rate floor adjustment." value="1.75" operator="+" /></PRICE_ADJUSTMENTS><STIPULATIONS><STIPULATION stip_text="MUST HAVE VALID PHOTO ID" is_required="Y" /><STIPULATION stip_text="INCOME VERIFICATION REQUIRED" is_required="Y" /></STIPULATIONS></PL_PRODUCT></PRODUCTS>]]></DECISION></RESPONSE></OUTPUT>
					Dim message As String = Server.HtmlDecode(SubmitPersonalLoan(personalLoanObject, _CurrentWebsiteConfig, oResponseXml))
					''check user select disagree button, and disagree_popup is on then we submit data to LPQ using submitloan and stop process 
					If isDisagreeSelect Then
						Return
					End If

					message = "<div class='justify-text'><p>" & message & "</p></div>"
					If _CurrentWebsiteConfig.SubmitLoanUrl.Contains("decisionloan/2.0") Then
						QualifiedProduct = getQualifyProduct2_0(oResponseXml)
					Else
						QualifiedProduct = getQualifyProduct(oResponseXml)
					End If

					If Not String.IsNullOrEmpty(displayProduct()) Then	'has approved product
						message = message + displayProduct()
					Else
						Response.Write(message)
						Return
					End If

					Dim sLoanNumber = Common.getLoanNumber(oResponseXml)
					Dim sloanID = Common.getResponseLoanID(oResponseXml)
					Dim sLoanBooking = Common.getNodeAttributes(_CurrentWebsiteConfig, "PERSONAL_LOAN", "loan_booking")
					Dim sAccountNumber = ""
					Dim bExecPacificCustomWorkFlow = False
					If _WorkFlowMode.Contains("PACIFIC") Then ''pacific merchant mode 
						Dim oCWF As New CCustomWorkFlows(_CurrentWebsiteConfig, _WorkFlowMode)
						bExecPacificCustomWorkFlow = oCWF.ExecPacificCustomWorkFlow(sloanID, "PL", loanRequestXMLStr)
						If Not bExecPacificCustomWorkFlow Then
							message = Server.HtmlDecode(CCustomMessage.getComboCustomResponseMessages(_CurrentWebsiteConfig, "SUBMITTED_MESSAGE", oResponseXml, loanRequestXMLStr, ""))
						End If
					Else
						Dim bookingValidationResult = ProceedBookingValidation(personalLoanObject)
                        If bookingValidationResult IsNot Nothing AndAlso bookingValidationResult.Count > 0 Then
                            ''booking validation is triggered then no booking, and update status to Referred and also update the custom approved message to custom submitting message
                            LoanSubmit.UpdateLoan(sloanID, _CurrentWebsiteConfig, "REFERREDLOAN_BOOKING_VALIDATION", "", String.Join(". ", bookingValidationResult.Select(Function(p) p.Name)))
                            Response.Write(Server.HtmlDecode(CCustomMessage.getComboCustomResponseMessages(_CurrentWebsiteConfig, "SUBMITTED_MESSAGE", oResponseXml, loanRequestXMLStr, "")))
                            Return
                        Else
                            ''run booking, checking for enable is done inside CBookingManager                  
                            sAccountNumber = CBookingManager.Book(_CurrentWebsiteConfig, sloanID, "PL")
                        End If
					End If
					'auto create PDF in the Letter/Doc>Archived Section of LPQ
					If String.IsNullOrEmpty(sLoanBooking) Or (Not String.IsNullOrEmpty(sLoanBooking) AndAlso Not String.IsNullOrEmpty(sAccountNumber)) Or bExecPacificCustomWorkFlow Then
						If Not String.IsNullOrEmpty(sAutoCreatePDFCode) Then CDocument.GeneratePDFs(_CurrentWebsiteConfig, sLoanNumber, "PL", "ARCHIVED", sAutoCreatePDFCode)
					End If

					''====Docusign
					Dim bIsDocuSign As Boolean = Common.getNodeAttributes(_CurrentWebsiteConfig, "PERSONAL_LOAN/VISIBLE", "in_session_docu_sign") = "Y"
					If bIsDocuSign Then
						Dim docuSignUrl As String = CDocuSign.GetDocuSign(_CurrentWebsiteConfig, sLoanNumber, "PL", Request)
						If docuSignUrl <> "" Then
							Dim docuSignHtml As String = "<iframe id='ifDocuSign' scrolling='auto' frameborder='0' width='100%' height='500px' sandbox='allow-same-origin allow-scripts allow-popups allow-forms' src='" + docuSignUrl + "'></iframe>"
							message = docuSignHtml + message
						End If
					End If

					''==Xsell
					Dim bXSell As Boolean = Common.getNodeAttributes(_CurrentWebsiteConfig, "PERSONAL_LOAN/VISIBLE", "auto_cross_qualify") = "Y"
					''Temporary hardcode PL# and force this to run so we can debug cross-sell
					If bXSell AndAlso Not Is2ndLoanApp Then
						Dim sessionId As String = Guid.NewGuid().ToString().Substring(0, 8).Replace("-", "").ToLower()
						Dim sCrossSellMsg As String = CXSell.GetCrossQualification(_CurrentWebsiteConfig, sLoanNumber, _LoanType, sessionId)
						Dim postedData As New NameValueCollection
						postedData = Request.Params
						''second call bXSell
						If Is2ndLoanApp AndAlso String.IsNullOrEmpty(Common.SafeString(Request.Form("SID"))) = False Then
							Dim _sid = Common.SafeString(Request.Form("SID"))
							If Session(_sid) IsNot Nothing Then
								postedData = CType(Session(_sid), NameValueCollection)
							End If
						End If
						''add xSell comment from PL
						Session.Add("xSell" & sessionId, "CROSS SELL FROM PL#" & sLoanNumber)
						Session.Add(sessionId, postedData)
						message = message + sCrossSellMsg
					End If
					Response.Write(message)
					Return
				Else
					''run ida
					Dim message As String = Server.HtmlDecode(SubmitPersonalLoan(personalLoanObject, _CurrentWebsiteConfig, loanResponseXMLStr))
					''check user select disagree button, and disagree_popup is on then we submit data to LPQ using submitloan and stop process 
					If isDisagreeSelect Then
						Return
					End If
					Response.Write(message)	 ''resonpse wallet question
					Return
				End If
			End If
		End If

		If (Request.Form("Task") = "WalletQuestions") Then
			''wallet answer
			''Dim combinedAnswerStr As String = Common.SafeString(Request.Form("WalletAnswers"))
			''Dim answerIDList As List(Of String) = Common.parseAnswersForID(combinedAnswerStr)
			Dim sWalletQuestionsAndAnswers As String = Common.SafeString(Request.Form("WalletQuestionsAndAnswers"))
			Dim loanIDAType As String = Common.SafeString(Request.Form("idaMethodType"))
			Dim idaResponseIDList As List(Of String) = idaResponseIDs(loanIDAType)
			Dim hasJointWalletAnswer As String = Common.SafeString(Request.Form("hasJointWalletAnswer"))
			Dim isJointAnswers = hasJointWalletAnswer = "Y"
			Dim isExecuteAnswer As Boolean = Common.ExecWalletAnswers(_CurrentWebsiteConfig, sWalletQuestionsAndAnswers, questionList_persist, idaResponseIDList, isJointAnswers, question_set_ID, request_app_ID, request_client_ID, request_sequence_ID, reference_number)
			Dim sRequestXML As XmlDocument = New XmlDocument()
			Dim loanResponseXML As String = ""
			Dim responseMessage As String = ""
			sRequestXML.LoadXml(loanRequestXMLStr)
			Dim customSubmittedMessage As String = Server.HtmlDecode(Common.WebPostResponse(_CurrentWebsiteConfig, sRequestXML, loanResponseXMLStr, False))
			customSubmittedMessage = "<div class='justify-text'><p>" & customSubmittedMessage & "</p></div>"
			If Not isExecuteAnswer Then
				Response.Write(customSubmittedMessage) ''failed -> response submit message
				Return
			End If
			' populate questions for joint 
			If (IsJointApplication And Not isJointAnswers) Then
				''response wallet question for joint
				Dim strResponse As String = Common.getWalletQuestionsResponseXML(_CurrentWebsiteConfig, loanResponseXMLStr, loanIDAType, IsJointApplication)
				If String.IsNullOrEmpty(strResponse) Then
					Response.Write(customSubmittedMessage) ''failed -> response submit message
					Return
				End If
				Dim strRenderWalletQuestions As String = Common.renderWalletQuestionsHTML(loanIDAType, strResponse, questionList_persist, Co_FName, transaction_ID, question_set_ID, request_app_ID, request_client_ID, request_sequence_ID, reference_number)
				Response.Write(strRenderWalletQuestions) ''response wallet question for joint
				Return
			End If
			''pass ida -->update loan
			responseMessage = Server.HtmlDecode(Common.submitUpdateLoan(_CurrentWebsiteConfig, loanID, loanRequestXMLStr, loanResponseXML))
			responseMessage = "<div class='justify-text'><p>" & responseMessage & "</p></div>"
			''make sure to decode html in xml config
			If _CurrentWebsiteConfig.SubmitLoanUrl.Contains("decisionloan/2.0") Then
				QualifiedProduct = getQualifyProduct2_0(loanResponseXML)
			Else
				QualifiedProduct = getQualifyProduct(loanResponseXML)
			End If
			If Not String.IsNullOrEmpty(displayProduct()) Then
				responseMessage = displayProduct() + responseMessage
			End If
			Response.Write(responseMessage)
		End If
		'CPBLogger.logInfo("Personal Loan: Callback page finished loading")
	End Sub

	Private Function ProceedBookingValidation(personalLoanLoanObject As CPersonalLoan) As List(Of CBookingValidation)
		Dim result As New List(Of CustomList.CBookingValidation)
        If CustomListRuleList IsNot Nothing AndAlso CustomListRuleList.Any(Function(c) c.Code = "BOOKING_VALIDATION") Then
            Dim requiredParams As New Dictionary(Of String, String)
            For Each p In CustomListRuleList.Where(Function(c) c.Code = "BOOKING_VALIDATION")
                Dim lst = p.GetList(Of CBookingValidation)()
                If lst IsNot Nothing AndAlso lst.Count > 0 Then
                    For Each item In lst
                        If item.ParamInfo IsNot Nothing AndAlso item.ParamInfo.Count > 0 Then
                            For Each param In item.ParamInfo
                                If requiredParams.ContainsKey(param.Key) Then Continue For
                                requiredParams.Add(param.Key, param.Value)
                            Next
                        End If
                    Next
                End If
            Next
            Dim params As New Dictionary(Of String, Tuple(Of Object, String))
            If requiredParams.ContainsKey("locationPool") Then
                params.Add("locationPool", New Tuple(Of Object, String)(personalLoanLoanObject.SelectedLocationPool, "List(Of String)"))
            End If
            If requiredParams.ContainsKey("isJoint") Then
                params.Add("isJoint", New Tuple(Of Object, String)(personalLoanLoanObject.Applicants IsNot Nothing AndAlso personalLoanLoanObject.Applicants.Any(Function(p) p.HasSpouse), "Boolean"))
            End If
            If requiredParams.ContainsKey("isLineOfCredit") Then
                params.Add("isLineOfCredit", New Tuple(Of Object, String)(personalLoanLoanObject.IsLOC, "Boolean"))
            End If
            If requiredParams.ContainsKey("amountRequested") Then
                params.Add("amountRequested", New Tuple(Of Object, String)(personalLoanLoanObject.LoanAmount, "Double"))
			End If
			If requiredParams.ContainsKey("loanPurposeNonLoc") Then
				Dim pp = Nothing
				If Not personalLoanLoanObject.IsLOC Then
					pp = New List(Of String) From {personalLoanLoanObject.LoanPurpose}
				End If
				params.Add("loanPurposeNonLoc", New Tuple(Of Object, String)(pp, "List(Of String)"))
			End If
			If requiredParams.ContainsKey("loanPurposeLoc") Then
				Dim loc = Nothing
				If personalLoanLoanObject.IsLOC Then
					loc = New List(Of String) From {personalLoanLoanObject.LoanPurpose}
				End If
				params.Add("loanPurposeLoc", New Tuple(Of Object, String)(loc, "List(Of String)"))
			End If

            If requiredParams.Any(Function(k) k.Key.StartsWith("CQ_")) Then
                If personalLoanLoanObject.CustomQuestionAnswers IsNot Nothing AndAlso personalLoanLoanObject.CustomQuestionAnswers.Count > 0 Then
					For Each validatedQuestionAnswers In personalLoanLoanObject.CustomQuestionAnswers
						Dim questionId = Regex.Replace(validatedQuestionAnswers.Question.Name, "\s", "_")
						If requiredParams.ContainsKey("CQ_" & questionId) AndAlso Not params.ContainsKey("CQ_" & questionId) Then
							Dim answerDataType = requiredParams.First(Function(p) p.Key = "CQ_" & questionId).Value
							If validatedQuestionAnswers.Answers.Count > 1 Then
								params.Add("CQ_" & questionId, New Tuple(Of Object, String)(validatedQuestionAnswers.Answers.Select(Function(q) q.AnswerValue).ToList(), answerDataType))
							ElseIf validatedQuestionAnswers.Answers.Count = 1 Then
								params.Add("CQ_" & questionId, New Tuple(Of Object, String)(validatedQuestionAnswers.Answers.First().AnswerValue, answerDataType))
							End If
						End If
					Next
				End If
            End If
            For Each app In personalLoanLoanObject.Applicants
                Dim subResult = EvaluateCustomList(Of CustomList.CBookingValidation)("BOOKING_VALIDATION", PrepareCustomListRuleParams(requiredParams, app, New Dictionary(Of String, Tuple(Of Object, String))(params)))
                If subResult IsNot Nothing AndAlso subResult.Count > 0 Then
					result.AddRange(subResult.Where(Function(p) Not String.IsNullOrWhiteSpace(p.Name) AndAlso Not result.Any(Function(q) q.Name = p.Name)).ToList())
                End If
            Next
        End If
        If result IsNot Nothing AndAlso result.Count > 0 Then
            log.Error(result.Select(Function(p) p.Name))
        End If
        Return result
	End Function

	Private Function PrepareCustomListRuleParams(requiredParams As Dictionary(Of String, String), applicantInfo As CApplicant, ByVal params As Dictionary(Of String, Tuple(Of Object, String))) As Dictionary(Of String, Tuple(Of Object, String))
		If requiredParams.ContainsKey("age") Then
			params.Add("age", New Tuple(Of Object, String)(CType(Math.Floor(DateTime.Now.Subtract(Common.SafeDate(applicantInfo.DOB)).TotalDays / 365), Integer), "Integer"))
		End If
		If requiredParams.ContainsKey("citizenship") Then
			params.Add("citizenship", New Tuple(Of Object, String)(applicantInfo.CitizenshipStatus, "String"))
		End If
		If requiredParams.ContainsKey("employeeOfLender") Then
			params.Add("employeeOfLender", New Tuple(Of Object, String)(applicantInfo.EmployeeOfLender, "String"))
		End If
		If requiredParams.ContainsKey("employmentLength") Then
			params.Add("employmentLength", New Tuple(Of Object, String)(Common.SafeDouble(applicantInfo.txtProfessionDuration_month) / 12 + Common.SafeDouble(applicantInfo.txtProfessionDuration_year), "Double"))
		End If
		If requiredParams.ContainsKey("employmentStatus") Then
			params.Add("employmentStatus", New Tuple(Of Object, String)(applicantInfo.EmploymentStatus, "String"))
		End If
		If requiredParams.ContainsKey("memberNumber") Then
			params.Add("memberNumber", New Tuple(Of Object, String)(applicantInfo.MemberNumber, "String"))
		End If
		If requiredParams.ContainsKey("occupancyLength") Then
			params.Add("occupancyLength", New Tuple(Of Object, String)(Common.SafeDouble(applicantInfo.OccupancyDuration / 12), "Double"))
		End If
		If requiredParams.ContainsKey("occupancyStatus") Then
			params.Add("occupancyStatus", New Tuple(Of Object, String)(applicantInfo.OccupancyType, "String"))
		End If

		If requiredParams.Any(Function(k) k.Key.StartsWith("AQ_")) Then
			If applicantInfo.ApplicantQuestionAnswers IsNot Nothing AndAlso applicantInfo.ApplicantQuestionAnswers.Count > 0 Then
				For Each validatedQuestionAnswer In applicantInfo.ApplicantQuestionAnswers
					Dim questionId = Regex.Replace(validatedQuestionAnswer.Question.Name, "\s", "_")
					If requiredParams.ContainsKey("AQ_" & questionId) AndAlso Not params.ContainsKey("AQ_" & questionId) Then
						Dim answerDataType = requiredParams.First(Function(p) p.Key = "AQ_" & questionId).Value
						If validatedQuestionAnswer.Answers.Count > 1 Then
							params.Add("AQ_" & questionId, New Tuple(Of Object, String)(validatedQuestionAnswer.Answers.Select(Function(q) q.AnswerValue).ToList(), answerDataType))
						ElseIf validatedQuestionAnswer.Answers.Count = 1 Then
							params.Add("AQ_" & questionId, New Tuple(Of Object, String)(validatedQuestionAnswer.Answers.First().AnswerValue, answerDataType))
						End If

					End If
				Next
			End If
		End If

		For Each pa In requiredParams.Where(Function(p) Not params.ContainsKey(p.Key))
			params.Add(pa.Key, New Tuple(Of Object, String)(Nothing, pa.Value))
		Next
		Return params
	End Function


    Private Function idaResponseIDs(ByVal idaType As String) As List(Of String)
        Dim responseIDList As New List(Of String)
        Select Case idaType.ToUpper()
            Case "PID"
                responseIDList.Add(idaType)
                responseIDList.Add(loanID)
                responseIDList.Add(transaction_ID)
                ''add more cases later
        End Select
        Return responseIDList
    End Function
    Private Function idaResponseXAIDs(ByVal idaType As String) As List(Of String)
        Dim responseIDList As New List(Of String)
        Select Case idaType.ToUpper()
            Case "PID", "RSA", "FIS", "DID", "EID", "TID"
                responseIDList.Add(idaType)
                responseIDList.Add(xaID)
                responseIDList.Add(transaction_ID)
                ''add more cases later
        End Select
        Return responseIDList
	End Function

	Private Function GetPersonalLoanObject(ByVal poConfig As CWebsiteConfig) As CPersonalLoan
		Dim personalLoan As New CPersonalLoan(poConfig.OrganizationId, poConfig.LenderId)
		personalLoan.HasDebtCancellation = Request.Form("MemberProtectionPlan") = "Y"
		personalLoan.LoanAmount = Common.SafeDouble(Request.Form("LoanAmount"))
		personalLoan.LoanTerm = Common.SafeInteger(Request.Form("LoanTerm"))
		personalLoan.LoanTypeCategory = Common.SafeString(Request.Form("LoanTypeCategory"))
		personalLoan.IsLOC = Request.Form("IsLOC") = "Y"
		personalLoan.LoanPurpose = Common.SafeString(Request.Form("LoanPurpose"))
		personalLoan.ReferralSource = Common.SafeString(Request.Form("ReferralSource"))
        personalLoan.LoanOfficerID = Common.SafeString(Request.Form("LoanOfficerID"))   'come from htnl mark page via js
        personalLoan.InstaTouchPrefillComment = Common.SafeString(Request.Form("InstaTouchPrefillComment"))
        If _MerchantID <> "" Then
			Dim sXPath As String = String.Format("MERCHANTS/MERCHANT[@merchant_id='{0}']", Common.SafeString(Request.Form("MerchantID")))
			personalLoan.ClinicID = Common.getNodeAttributes(poConfig, sXPath, "clinic_id")
			personalLoan.ClinicWorkerID = Common.getNodeAttributes(poConfig, sXPath, "worker_id")
			personalLoan.ClinicName = Common.getNodeAttributes(poConfig, sXPath, "merchant_name")
		ElseIf _ClinicID <> "" Then
			personalLoan.ClinicID = _ClinicID
			personalLoan.ClinicWorkerID = _WorkerID
			personalLoan.ClinicName = Right(_ClinicID, 10)
		End If

		''From vendor portal, citizens state
		If Not String.IsNullOrEmpty(Common.SafeStripHtmlString(Request.Form("VendorId"))) AndAlso Not String.IsNullOrEmpty(Request.Form("UserId")) Then
			personalLoan.VendorID = Common.SafeStripHtmlString(Request.Form("VendorId"))
			personalLoan.UserID = Common.SafeGUID(Request.Form("UserId"))
			Dim smBL As New SmBL()
			Dim vendor As SmLenderVendor = smBL.GetLenderVendorByVendorID(Common.SafeStripHtmlString(Request.Form("VendorId")))

			If vendor IsNot Nothing Then
				personalLoan.VendorName = vendor.VendorName
				If vendor.DealerNumberLpq <> "" AndAlso vendor.DefaultLpqWorkerID <> "" Then
					personalLoan.LPQVendorID = vendor.DealerNumberLpq	'map to LPQClinicID
					personalLoan.LPQWorkerID = vendor.DefaultLpqWorkerID	'TODO, this need to change to LPQ WorkerID
				End If
			End If


		End If
        If Not String.IsNullOrEmpty(Request.Params("SelectedLocationPool")) Then
            personalLoan.SelectedLocationPool = (New JavaScriptSerializer()).Deserialize(Of List(Of String))(Request.Params("SelectedLocationPool"))
        End If
        personalLoan.IsSSO = IsSSO
		personalLoan.IsComboMode = IsComboMode
		personalLoan.BranchID = Request.Form("BranchID")
		personalLoan.Description = Request.Form("Description")

		Dim jsSerializer As New JavaScriptSerializer()
		personalLoan.isCQNewAPI = Common.getVisibleAttribute(_CurrentWebsiteConfig, "custom_question_new_api") = "Y"

		' Only get Application answers, not Applicant
		Dim lstApplicationCustomAnswers As List(Of CApplicantQA) =
			jsSerializer.Deserialize(Of List(Of CApplicantQA))(Request.Form("CustomAnswers")).
			Where(Function(cqa) Not String.IsNullOrEmpty(cqa.CQAnswer) AndAlso cqa.CQRole = QuestionRole.Application).ToList()
		' Collect URL Parameter CQ Answers, if applicable
		Dim bIsUrlParaCustomQuestion = Common.SafeString(Request.Form("IsUrlParaCustomQuestion")) = "Y"
		Dim lstUrlParaCustomAnswers As New List(Of CApplicantQA)
		If bIsUrlParaCustomQuestion Then
			lstUrlParaCustomAnswers = jsSerializer.Deserialize(Of List(Of CApplicantQA))(Common.SafeString(Request.Form("UrlParaCustomQuestionAndAnswers")))
		End If
		' Set validated CQ answers
		personalLoan.CustomQuestionAnswers = CCustomQuestionNewAPI.
			getValidatedCustomQuestionAnswers(_CurrentWebsiteConfig, "PL", QuestionRole.Application, "", lstApplicationCustomAnswers, lstUrlParaCustomAnswers.Where(Function(a) a.CQRole = QuestionRole.Application)).ToList()

		personalLoan.isDisagreeSelect = Request.Form("isDisagreeSelect")

		''----disclosures, IPAddress and user agent for internal comment
		If Not String.IsNullOrEmpty(Request.Form("Disclosure")) Then
			personalLoan.Disclosure = Request.Form("Disclosure")
		End If
		personalLoan.IPAddress = Request.UserHostAddress
		personalLoan.UserAgent = Request.ServerVariables("HTTP_USER_AGENT")

		'behavior
		Dim oBehahiorDic = Common.getBehaviorNode(poConfig)
		If oBehahiorDic.ContainsKey("doc_stamps_fee_is_manual") AndAlso oBehahiorDic("doc_stamps_fee_is_manual").Trim() <> "" Then
			personalLoan.DocStampsFeeIsManual = oBehahiorDic("doc_stamps_fee_is_manual").Trim()
		End If
		'===all upload document
		Dim oSelectDocsList As New List(Of selectDocuments)
		Dim oSerializer As New JavaScriptSerializer()
		Dim oDocsList As New Dictionary(Of String, String)
		''by default MaxJsonLenth = 102400, set it =int32.maxvalue = 2147483647 

		oSerializer.MaxJsonLength = Int32.MaxValue
		If Not String.IsNullOrEmpty(Request.Form("Image")) Then
			'TODO
			'' oDocsList = oSerializer.Deserialize(Of Dictionary(Of String, String))(Request.Params("Image"))
			'need to work on serialization
			''oDocsList.Add("test title.png", Request.Params("Image"))
			oSelectDocsList = oSerializer.Deserialize(Of List(Of selectDocuments))(Request.Form("Image"))
			If oSelectDocsList.Count > 0 Then
				Dim i As Integer
				For i = 0 To oSelectDocsList.Count - 1
					Dim oTitle As String = oSelectDocsList(i).title
					Dim obase64data As String = oSelectDocsList(i).base64data
					oDocsList.Add(oTitle, obase64data)
				Next
				If oDocsList.Count = 0 Then
					log.Info("Can't de-serialize image ")
				End If
			End If
			personalLoan.DocBase64Dic = oDocsList
		End If
		''upload document info(title,doc_group, doc_code) if it exist
		Dim oUploadDocumentInfoList As New List(Of uploadDocumentInfo)
		If Not String.IsNullOrEmpty(Request.Params("UploadDocInfor")) Then
			Dim oDocInfoSerializer As New JavaScriptSerializer()
			oUploadDocumentInfoList = oDocInfoSerializer.Deserialize(Of List(Of uploadDocumentInfo))(Request.Params("UploadDocInfor"))
		End If
		personalLoan.UploadDocumentInfoList = oUploadDocumentInfoList
		personalLoan.Applicants = New List(Of CApplicant)
		personalLoan.CoApplicantType = poConfig.coApplicantType
		Dim hasCoApp As Boolean = Request.Form("HasCoApp") = "Y"
		'Dim coAppJoint As Boolean = Request.Form("CoAppJoin") = "Y"

		IsJointApplication = hasCoApp ''use for second call back

		''cuna question answers
		Dim cunaQuestionAnswers As New List(Of String)
		If Not String.IsNullOrEmpty(Request.Form("cunaQuestionAnswers")) Then
			cunaQuestionAnswers = New JavaScriptSerializer().Deserialize(Of List(Of String))(Request.Form("cunaQuestionAnswers"))
		End If
		personalLoan.cunaQuestionAnswers = cunaQuestionAnswers

		''get current applicant question from download
		If personalLoan.isCQNewAPI Then
			personalLoan.DownloadedAQs = CCustomQuestionNewAPI.getDownloadedCustomQuestions(_CurrentWebsiteConfig, True, "PL")
		End If
		Dim declarationList = Common.GetActiveDeclarationList(_CurrentWebsiteConfig, "PERSONAL_LOAN")
		Dim appSpouseInfo = Common.SafeString(Request.Form("AppSpouseInfo"))
		If Not String.IsNullOrEmpty(appSpouseInfo) Then
			personalLoan.AppNSS = JsonConvert.DeserializeObject(Of CNSSInfo)(appSpouseInfo)
		End If
		Dim coAppSpouseInfo = Common.SafeString(Request.Form("co_AppSpouseInfo"))
		If Not String.IsNullOrEmpty(coAppSpouseInfo) Then
			personalLoan.CoAppNSS = JsonConvert.DeserializeObject(Of CNSSInfo)(coAppSpouseInfo)
		End If


		Dim applicant As New CApplicant()
		With applicant
			.EmployeeOfLender = Common.SafeString(Request.Form("EmployeeOfLender"))
			.FirstName = Common.SafeString(Request.Form("FirstName"))
			.MiddleInitial = Common.SafeString(Request.Form("MiddleName"))
			.LastName = Common.SafeString(Request.Form("LastName"))
			.Suffix = Common.SafeString(Request.Form("NameSuffix"))
			.SSN = Common.SafeString(Request.Form("SSN"))
			.DOB = Common.SafeString(Request.Form("DOB"))
			''reference information
			If Not String.IsNullOrEmpty(Common.SafeString(Request.Form("referenceInfo"))) Then
				.referenceInfor = New JavaScriptSerializer().Deserialize(Of List(Of String))(Request.Form("referenceInfo"))
			End If
			If personalLoan.AppNSS IsNot Nothing Then
				personalLoan.AppNSS.MarriedTo = String.Format("{0} {1}", .FirstName, .LastName)
			End If
			''driver license info
			If Not String.IsNullOrEmpty(Request.Form("IDCardNumber")) Then
				.IDNumber = Common.SafeString(Request.Form("IDCardNumber"))
				.IDType = Common.SafeString(Request.Form("IDCardType"))
				.IDState = Common.SafeString(Request.Form("IDState"))
				.IDCountry = Common.SafeString(Request.Form("IDCountry"))
				.IDDateIssued = Common.SafeString(Request.Form("IDDateIssued"))
				.IDExpirationDate = Common.SafeString(Request.Form("IDDateExpire"))
			End If
			.MemberNumber = Common.SafeString(Request.Form("MemberNumber"))
			.MaritalStatus = Common.SafeString(Request.Form("MaritalStatus"))
			.MembershipLengthMonths = Common.SafeString(Request.Form("MembershipLengthMonths"))
			.CitizenshipStatus = Common.SafeString(Request.Form("CitizenshipStatus"))
			.HomePhone = Common.SafeString(Request.Form("HomePhone"))
			.HomePhoneCountry = Common.SafeString(Request.Form("HomePhoneCountry"))
			.WorkPhone = Common.SafeString(Request.Form("WorkPhone"))
			.WorkPhoneCountry = Common.SafeString(Request.Form("WorkPhoneCountry"))
			.WorkPhoneEXT = Common.SafeString(Request.Form("WorkPhoneEXT"))
			.CellPhone = Common.SafeString(Request.Form("MobilePhone"))
			.CellPhoneCountry = Common.SafeString(Request.Form("MobilePhoneCountry"))
			.Email = Common.SafeString(Request.Form("EmailAddr"))
			.PreferredContactMethod = Common.SafeString(Request.Form("ContactMethod"))
			.CurrentAddress = Common.SafeString(Request.Form("AddressStreet"))
			.CurrentAddress2 = Common.SafeString(Request.Form("AddressStreet2"))
			.CurrentZip = Common.SafeString(Request.Form("AddressZip"))
			.CurrentCity = Common.SafeString(Request.Form("AddressCity"))
			.CurrentState = Common.SafeString(Request.Form("AddressState"))
			.CurrentCountry = Common.SafeString(Request.Form("Country"))
			.OccupancyType = Common.SafeString(Request.Form("OccupyingLocation"))
			.OccupancyDuration = Common.SafeInteger(Request.Form("LiveMonths"))
			.OccupancyDescription = Common.SafeString(Request.Form("OccupancyDescription"))

			''previous Address
			.HasPreviousAddress = Common.SafeString(Request.Form("hasPreviousAddress"))
			.PreviousAddress = Common.SafeString(Request.Form("PreviousAddressStreet"))
			.PreviousZip = Common.SafeString(Request.Form("PreviousAddressZip"))
			.PreviousCity = Common.SafeString(Request.Form("PreviousAddressCity"))
			.PreviousState = Common.SafeString(Request.Form("PreviousAddressState"))
			.PreviousCountry = Common.SafeString(Request.Form("PreviousAddressCountry"))
			''end previous Address

			''mailing address
			Dim hasMailingAddress As String = Request.Form("hasMailingAddress")
			If Not String.IsNullOrEmpty(hasMailingAddress) Then
				If hasMailingAddress = "Y" Then
					.HasMailingAddress = "Y"
					.MailingAddress = Request.Form("MailingAddressStreet")
					.MailingAddress2 = Request.Form("MailingAddressStreet2")
					.MailingCity = Request.Form("MailingAddressCity")
					.MailingState = Request.Form("MailingAddressState")
					.MailingZip = Request.Form("MailingAddressZip")
					.MailingCountry = Request.Form("MailingAddressCountry")
				Else
					.MailingAddress = ""
					.MailingAddress2 = ""
					.MailingCity = ""
					.MailingState = ""
					.MailingZip = ""
					.MailingCountry = ""
				End If
			End If

			.EmploymentStatus = Common.SafeString(Request.Form("EmploymentStatus"))
			.EmploymentDescription = Common.SafeString(Request.Form("EmploymentDescription"))
			''If .EmploymentStatus = "STUDENT" Or .EmploymentStatus = "RETIRED" Or .EmploymentStatus = "HOMEMAKER" Or .EmploymentStatus = "UNEMPLOYED" Then
			''    .txtJobTitle = .EmploymentStatus
			''    .txtEmployedDuration_month = Common.SafeString(Request.Form("txtEmployedDuration_month"))
			''    .txtEmployedDuration_year = Common.SafeString(Request.Form("txtEmployedDuration_year"))
			''Else
			.txtJobTitle = Common.SafeString(Request.Form("txtJobTitle"))
			.txtEmployedDuration_month = Common.SafeString(Request.Form("txtEmployedDuration_month"))
			.txtEmployedDuration_year = Common.SafeString(Request.Form("txtEmployedDuration_year"))
			.txtEmployer = Common.SafeString(Request.Form("txtEmployer"))
			''End If
			' new employment logic
			.txtBusinessType = Common.SafeString(Request.Form("txtBusinessType"))
			.txtEmploymentStartDate = Common.SafeString(Request.Form("txtEmploymentStartDate"))
			.txtETS = Common.SafeString(Request.Form("txtETS"))
			.txtProfessionDuration_month = Common.SafeString(Request.Form("txtProfessionDuration_month"))
			.txtProfessionDuration_year = Common.SafeString(Request.Form("txtProfessionDuration_year"))
			.txtSupervisorName = Common.SafeString(Request.Form("txtSupervisorName"))
			.ddlBranchOfService = Common.SafeString(Request.Form("ddlBranchOfService"))
			.ddlPayGrade = Common.SafeString(Request.Form("ddlPayGrade"))
			' end new employment logic
			.GrossMonthlyIncome = Common.SafeDouble(Request.Form("GrossMonthlyIncome"))
			'other monthly income
			.GrossMonthlyIncomeOther = Common.SafeDouble(Request.Form("OtherMonthlyIncome"))
			.MonthlyIncomeOtherDescription = Common.SafeString(Request.Form("OtherMonthlyIncomeDesc"))
			''tax exempt
			.GrossMonthlyIncomeTaxExempt = Common.SafeString(Request.Form("GrossMonthlyIncomeTaxExempt"))
			.OtherMonthlyIncomeTaxExempt = Common.SafeString(Request.Form("OtherMonthlyIncomeTaxExempt"))

			.TotalMonthlyHousingExpense = Common.SafeDouble(Request.Form("TotalMonthlyHousingExpense"))

			''previous employment information
			.hasPrevEmployment = Common.SafeString(Request.Form("hasPreviousEmployment"))
			.prev_EmploymentStatus = Common.SafeString(Request.Form("prev_EmploymentStatus"))
			If .prev_EmploymentStatus = "" Or .prev_EmploymentStatus = "" Or .prev_EmploymentStatus = "" Or .prev_EmploymentStatus = "" Then
				.prev_txtJobTitle = .prev_EmploymentStatus
			Else
				.prev_txtJobTitle = Common.SafeString(Request.Form("prev_txtJobTitle"))
				.prev_txtEmployedDuration_month = Common.SafeString(Request.Form("prev_txtEmployedDuration_month"))
				.prev_txtEmployedDuration_year = Common.SafeString(Request.Form("prev_txtEmployedDuration_year"))
				.prev_txtEmployer = Common.SafeString(Request.Form("prev_txtEmployer"))
			End If
			.prev_txtBusinessType = Common.SafeString(Request.Form("prev_txtBusinessType"))
			.prev_txtEmploymentStartDate = Common.SafeString(Request.Form("prev_txtEmploymentStartDate"))
			.prev_txtETS = Common.SafeString(Request.Form("prev_txtETS"))
			.prev_ddlBranchOfService = Common.SafeString(Request.Form("prev_ddlBranchOfService"))
			.prev_ddlPayGrade = Common.SafeString(Request.Form("prev_ddlPayGrade"))
			.prev_GrossMonthlyIncome = Common.SafeDouble(Request.Form("prev_GrossMonthlyIncome"))
			''end previous employment information
			'declaration
			If Not String.IsNullOrEmpty(Common.SafeString(Request.Form("Declarations"))) AndAlso declarationList IsNot Nothing AndAlso declarationList.Count > 0 Then
				Dim declarationResult = JsonConvert.DeserializeObject(Of List(Of KeyValuePair(Of String, Boolean)))(Request.Form("Declarations"))
				If declarationResult IsNot Nothing AndAlso declarationResult.Count > 0 Then
					.Declarations = (From re In declarationResult
									 Join en In declarationList
									On re.Key Equals en.Key
									 Select re).ToList()
					If Not String.IsNullOrEmpty(Common.SafeString(Request.Form("AdditionalDeclarations"))) Then
						.AdditionalDeclarations = JsonConvert.DeserializeObject(Of Dictionary(Of String, String))(Request.Form("AdditionalDeclarations"))
					End If
				End If
			End If
			'selfhelp fcu customization
			If _CurrentLenderRef.ToUpper.StartsWith("SHFCU") And .TotalMonthlyHousingExpense < 100 Then
				.TotalMonthlyHousingExpense = 100
			End If

			'Neighborhood CU  customization
			If (_CurrentLenderRef.ToUpper.StartsWith("NCU_TEST") Or _CurrentLenderRef.ToUpper.StartsWith("NCU103114")) And .TotalMonthlyHousingExpense < 0.01 Then
				.TotalMonthlyHousingExpense = 500
			End If

			'Uncle CU  customization
			If _CurrentLenderRef.ToUpper.StartsWith("UNCLECU") And .TotalMonthlyHousingExpense < 600 And (.OccupancyType = "LIVE WITH PARENTS" Or .OccupancyType = "RENT" Or .OccupancyType = "OTHER") Then
				.TotalMonthlyHousingExpense = 600
			End If

			'Municipal CU  customization
			If (_CurrentLenderRef.ToUpper.StartsWith("MCU_TEST") Or _CurrentLenderRef.ToUpper.StartsWith("MCU090115")) And .TotalMonthlyHousingExpense < 600 And (.OccupancyType = "LIVE WITH PARENTS" Or .OccupancyType = "OTHER") Then
				.TotalMonthlyHousingExpense = 600
			End If

			Dim assetInfoJsonStr = Common.SafeString(Request.Form("Assets"))
			If Not String.IsNullOrWhiteSpace(assetInfoJsonStr) Then
				.Assets = JsonConvert.DeserializeObject(Of List(Of CAssetInfo))(assetInfoJsonStr)
			End If

			.HasSpouse = hasCoApp

			''Government Monitoring Information
			'.HasGMI = Common.SafeString(Request.Form("HasGMI"))
			'If .HasGMI <> "N" Then
			'             .Gender = Common.SafeString(Request.Form("Gender"))
			'             .Ethnicity = Common.SafeString(Request.Form("Ethnicity"))
			'             .Race = Common.SafeString(Request.Form("Race"))
			'             .DeclinedAnswerRaceGender = Common.SafeString(Request.Form("DeclinedAnswerRaceGender"))
			'End If

			' Applicant custom question answers 
			Dim lstApplicantCustomAnswers As List(Of CApplicantQA) =
				jsSerializer.Deserialize(Of List(Of CApplicantQA))(Request.Form("CustomAnswers")).
				Where(Function(cqa) Not String.IsNullOrEmpty(cqa.CQAnswer) AndAlso cqa.CQRole = QuestionRole.Applicant AndAlso IIf(cqa.CQLocation = CustomQuestionLocation.ApplicantPage, cqa.CQApplicantPrefix = "", True)).ToList()
			' Set validated CQ answers
			.ApplicantQuestionAnswers = CCustomQuestionNewAPI.
				getValidatedCustomQuestionAnswers(_CurrentWebsiteConfig, "PL", QuestionRole.Applicant, "", lstApplicantCustomAnswers, lstUrlParaCustomAnswers.Where(Function(urlQA) urlQA.CQRole = QuestionRole.Applicant))
		End With

		personalLoan.Applicants.Add(applicant)

		If hasCoApp Then
			Dim coApplicant As New CApplicant()
			With coApplicant
				.EmployeeOfLender = Common.SafeString(Request.Form("co_EmployeeOfLender"))
				.FirstName = Common.SafeString(Request.Form("co_FirstName"))
				.MiddleInitial = Common.SafeString(Request.Form("co_MiddleName"))
				.LastName = Common.SafeString(Request.Form("co_LastName"))
				.Suffix = Common.SafeString(Request.Form("co_NameSuffix"))
				.SSN = Common.SafeString(Request.Form("co_SSN"))
				.DOB = Common.SafeString(Request.Form("co_DOB"))
				If personalLoan.CoAppNSS IsNot Nothing Then
					personalLoan.CoAppNSS.MarriedTo = String.Format("{0} {1}", .FirstName, .LastName)
				End If
				''reference information
				If Not String.IsNullOrEmpty(Common.SafeString(Request.Form("co_referenceInfo"))) Then
					.referenceInfor = New JavaScriptSerializer().Deserialize(Of List(Of String))(Request.Form("co_referenceInfo"))
				End If

				''identification info
				If Not String.IsNullOrEmpty(Request.Form("co_IDCardNumber")) Then
					.IDNumber = Common.SafeString(Request.Form("co_IDCardNumber"))
					.IDType = Common.SafeString(Request.Form("co_IDCardType"))
					.IDState = Common.SafeString(Request.Form("co_IDState"))
					.IDCountry = Common.SafeString(Request.Form("co_IDCountry"))
					.IDDateIssued = Common.SafeString(Request.Form("co_IDDateIssued"))
					.IDExpirationDate = Common.SafeString(Request.Form("co_IDDateExpire"))
				End If
				.MemberNumber = Common.SafeString(Request.Form("co_MemberNumber"))
				.RelationshipToPrimary = Common.SafeString(Request.Form("co_RelationshipToPrimary"))
				.MaritalStatus = Common.SafeString(Request.Form("co_MaritalStatus"))
				.MembershipLengthMonths = Common.SafeString(Request.Form("co_MembershipLengthMonths"))
				.CitizenshipStatus = Common.SafeString(Request.Form("co_CitizenshipStatus"))
				.HomePhone = Common.SafeString(Request.Form("co_HomePhone"))
				.HomePhoneCountry = Common.SafeString(Request.Form("co_HomePhoneCountry"))
				.WorkPhone = Common.SafeString(Request.Form("co_WorkPhone"))
				.WorkPhoneCountry = Common.SafeString(Request.Form("co_WorkPhoneCountry"))
				.WorkPhoneEXT = Common.SafeString(Request.Form("co_WorkPhoneEXT"))
				.CellPhone = Common.SafeString(Request.Form("co_MobilePhone"))
				.CellPhoneCountry = Common.SafeString(Request.Form("co_MobilePhoneCountry"))
				.Email = Request.Form("co_EmailAddr")
				.PreferredContactMethod = Common.SafeString(Request.Form("co_ContactMethod"))
				.CurrentAddress = Common.SafeString(Request.Form("co_AddressStreet"))
				.CurrentAddress2 = Common.SafeString(Request.Form("co_AddressStreet2"))
				.CurrentZip = Common.SafeString(Request.Form("co_AddressZip"))
				.CurrentCity = Common.SafeString(Request.Form("co_AddressCity"))
				.CurrentState = Common.SafeString(Request.Form("co_AddressState"))
				.CurrentCountry = Common.SafeString(Request.Form("co_Country"))
				.OccupancyType = Common.SafeString(Request.Form("co_OccupyingLocation"))
				.OccupancyDuration = Common.SafeInteger(Request.Form("co_LiveMonths"))
				.OccupancyDescription = Common.SafeString(Request.Form("co_OccupancyDescription"))

				''previous Address
				.HasPreviousAddress = Common.SafeString(Request.Form("co_hasPreviousAddress"))
				.PreviousAddress = Common.SafeString(Request.Form("co_PreviousAddressStreet"))
				.PreviousZip = Common.SafeString(Request.Form("co_PreviousAddressZip"))
				.PreviousCity = Common.SafeString(Request.Form("co_PreviousAddressCity"))
				.PreviousState = Common.SafeString(Request.Form("co_PreviousAddressState"))
				.PreviousCountry = Common.SafeString(Request.Form("co_PreviousAddressCountry"))
				''end previous Address

				''mailing address
				Dim hasMailingAddress As String = Request.Form("co_hasMailingAddress")
				If Not String.IsNullOrEmpty(hasMailingAddress) Then
					If hasMailingAddress = "Y" Then
						.HasMailingAddress = "Y"
						.MailingAddress = Request.Form("co_MailingAddressStreet")
						.MailingAddress2 = Request.Form("co_MailingAddressStreet2")
						.MailingCity = Request.Form("co_MailingAddressCity")
						.MailingState = Request.Form("co_MailingAddressState")
						.MailingZip = Request.Form("co_MailingAddressZip")
						.MailingCountry = Request.Form("co_MailingAddressCountry")
					Else
						.MailingAddress = ""
						.MailingAddress2 = ""
						.MailingCity = ""
						.MailingState = ""
						.MailingZip = ""
						.MailingCountry = ""
					End If
				End If

				.EmploymentStatus = Common.SafeString(Request.Form("co_EmploymentStatus"))
				.EmploymentDescription = Common.SafeString(Request.Form("co_EmploymentDescription"))
				''If .EmploymentStatus = "STUDENT" Or .EmploymentStatus = "RETIRED" Or .EmploymentStatus = "HOMEMAKER" Or .EmploymentStatus = "UNEMPLOYED" Then
				''    .txtJobTitle = Common.SafeString(Request.Form("co_txtJobTitle"))
				''    .txtEmployedDuration_month = Common.SafeString(Request.Form("co_txtEmployedDuration_month"))
				''    .txtEmployedDuration_year = Common.SafeString(Request.Form("co_txtEmployedDuration_year"))

				''Else
				.txtJobTitle = Common.SafeString(Request.Form("co_txtJobTitle"))
				.txtEmployedDuration_month = Common.SafeString(Request.Form("co_txtEmployedDuration_month"))
				.txtEmployedDuration_year = Common.SafeString(Request.Form("co_txtEmployedDuration_year"))
				.txtEmployer = Common.SafeString(Request.Form("co_txtEmployer"))
				'' End If
				' new employment logic
				.txtBusinessType = Common.SafeString(Request.Form("co_txtBusinessType"))
				.txtEmploymentStartDate = Common.SafeString(Request.Form("co_txtEmploymentStartDate"))
				.txtETS = Common.SafeString(Request.Form("co_txtETS"))
				.txtProfessionDuration_month = Common.SafeString(Request.Form("co_txtProfessionDuration_month"))
				.txtProfessionDuration_year = Common.SafeString(Request.Form("co_txtProfessionDuration_year"))
				.txtSupervisorName = Common.SafeString(Request.Form("co_txtSupervisorName"))
				.ddlBranchOfService = Common.SafeString(Request.Form("co_ddlBranchOfService"))
				.ddlPayGrade = Common.SafeString(Request.Form("co_ddlPayGrade"))
				' end new employment logic
				.GrossMonthlyIncome = Common.SafeDouble(Request.Form("co_GrossMonthlyIncome"))
				'other monthly income
				.GrossMonthlyIncomeOther = Common.SafeDouble(Request.Form("co_OtherMonthlyIncome"))
				.MonthlyIncomeOtherDescription = Common.SafeString(Request.Form("co_OtherMonthlyIncomeDesc"))
				''tax exempt
				.GrossMonthlyIncomeTaxExempt = Common.SafeString(Request.Form("co_GrossMonthlyIncomeTaxExempt"))
				.OtherMonthlyIncomeTaxExempt = Common.SafeString(Request.Form("co_OtherMonthlyIncomeTaxExempt"))

				.TotalMonthlyHousingExpense = Common.SafeDouble(Request.Form("co_TotalMonthlyHousingExpense"))

				''previous employment information
				.hasPrevEmployment = Common.SafeString(Request.Form("co_hasPreviousEmployment"))
				.prev_EmploymentStatus = Common.SafeString(Request.Form("co_prev_EmploymentStatus"))
				If .prev_EmploymentStatus = "" Or .prev_EmploymentStatus = "" Or .prev_EmploymentStatus = "" Or .prev_EmploymentStatus = "" Then
					.prev_txtJobTitle = Common.SafeString(Request.Form("co_prev_txtJobTitle"))
				Else
					.prev_txtJobTitle = Common.SafeString(Request.Form("co_prev_txtJobTitle"))
					.prev_txtEmployedDuration_month = Common.SafeString(Request.Form("co_prev_txtEmployedDuration_month"))
					.prev_txtEmployedDuration_year = Common.SafeString(Request.Form("co_prev_txtEmployedDuration_year"))
					.prev_txtEmployer = Common.SafeString(Request.Form("co_prev_txtEmployer"))
				End If
				.prev_txtBusinessType = Common.SafeString(Request.Form("co_prev_txtBusinessType"))
				.prev_txtEmploymentStartDate = Common.SafeString(Request.Form("co_prev_txtEmploymentStartDate"))
				.prev_txtETS = Common.SafeString(Request.Form("co_prev_txtETS"))
				.prev_ddlBranchOfService = Common.SafeString(Request.Form("co_prev_ddlBranchOfService"))
				.prev_ddlPayGrade = Common.SafeString(Request.Form("co_prev_ddlPayGrade"))
				.prev_GrossMonthlyIncome = Common.SafeDouble(Request.Form("co_prev_GrossMonthlyIncome"))
				''end previous employment information

				'' Co/Joint-Declarations
				If Not String.IsNullOrEmpty(Common.SafeString(Request.Form("co_Declarations"))) AndAlso declarationList IsNot Nothing AndAlso declarationList.Count > 0 Then
					Dim declarationResult = JsonConvert.DeserializeObject(Of List(Of KeyValuePair(Of String, Boolean)))(Request.Form("co_Declarations"))
					If declarationResult IsNot Nothing AndAlso declarationResult.Count > 0 Then
						.Declarations = (From re In declarationResult
										Join en In declarationList
										On re.Key Equals en.Key
										Select re).ToList()
						If Not String.IsNullOrEmpty(Common.SafeString(Request.Form("co_AdditionalDeclarations"))) Then
							.AdditionalDeclarations = JsonConvert.DeserializeObject(Of Dictionary(Of String, String))(Request.Form("co_AdditionalDeclarations"))
						End If
					End If
				End If
				'Neighborhood CU  customization
				If (_CurrentLenderRef.ToUpper.StartsWith("NCU_TEST") Or _CurrentLenderRef.ToUpper.StartsWith("NCU103114")) And .TotalMonthlyHousingExpense < 0.01 Then
					.TotalMonthlyHousingExpense = 500
				End If

				'Uncle CU  customization
				If _CurrentLenderRef.ToUpper.StartsWith("UNCLECU") And .TotalMonthlyHousingExpense < 600 And (.OccupancyType = "LIVE WITH PARENTS" Or .OccupancyType = "RENT" Or .OccupancyType = "OTHER") Then
					.TotalMonthlyHousingExpense = 600
				End If

				'Municipal CU  customization
				If (_CurrentLenderRef.ToUpper.StartsWith("MCU_TEST") Or _CurrentLenderRef.ToUpper.StartsWith("MCU090115")) And .TotalMonthlyHousingExpense < 600 And (.OccupancyType = "LIVE WITH PARENTS" Or .OccupancyType = "OTHER") Then
					.TotalMonthlyHousingExpense = 600
				End If

                Dim coAssetInfoJsonStr = Common.SafeString(Request.Form("co_Assets"))
                If Not String.IsNullOrWhiteSpace(coAssetInfoJsonStr) Then
                    .Assets = JsonConvert.DeserializeObject(Of List(Of CAssetInfo))(coAssetInfoJsonStr)
                End If

				''Government Monitoring Information
				'.HasGMI = Common.SafeString(Request.Form("co_HasGMI"))
				'If .HasGMI <> "N" Then
				'                .Gender = Common.SafeString(Request.Form("co_Gender"))
				'                .Ethnicity = Common.SafeString(Request.Form("co_Ethnicity"))
				'                .Race = Common.SafeString(Request.Form("co_Race"))
				'                .DeclinedAnswerRaceGender = Common.SafeString(Request.Form("co_DeclinedAnswerRaceGender"))
				'End If

				' Applicant custom question answers 
				Dim lstApplicantCustomAnswers As List(Of CApplicantQA) =
					jsSerializer.Deserialize(Of List(Of CApplicantQA))(Request.Form("CustomAnswers")).
					Where(Function(cqa) Not String.IsNullOrEmpty(cqa.CQAnswer) AndAlso cqa.CQRole = QuestionRole.Applicant AndAlso IIf(cqa.CQLocation = CustomQuestionLocation.ApplicantPage, cqa.CQApplicantPrefix = "co_", True)).ToList()
				' Set validated CQ answers
				.ApplicantQuestionAnswers = CCustomQuestionNewAPI.
					getValidatedCustomQuestionAnswers(_CurrentWebsiteConfig, "PL", QuestionRole.Applicant, "", lstApplicantCustomAnswers, lstUrlParaCustomAnswers.Where(Function(urlQA) urlQA.CQRole = QuestionRole.Applicant))
			End With
			personalLoan.Applicants.Add(coApplicant)
			Co_FName = Common.SafeString(Request.Form("co_FirstName")) ''use for second call back
		End If
		Return personalLoan
	End Function

	Private Function SubmitPersonalLoan(personalLoan As CPersonalLoan, ByVal poConfig As CWebsiteConfig, ByRef poResponseRaw As String) As String
		''get foreinssn if it exist in config
		Dim ForeignSSN As String = ""
		Dim co_ForeignSSN As String = ""
		Dim isSubmitLoan As Boolean = False
		If Not String.IsNullOrEmpty(_CurrentWebsiteConfig.AppType) Then
			If _CurrentWebsiteConfig.AppType.ToUpper() = "FOREIGN" Then
				ForeignSSN = Common.SafeString(Request.Form("SSN"))
				co_ForeignSSN = Common.SafeString(Request.Form("co_SSN"))
			End If
		End If
		Dim hasCoApp As Boolean = Request.Form("HasCoApp") = "Y"
		If hasCoApp Then 'check foreignssn and co_foreignssn
			If ForeignSSN = "999999999" Or co_ForeignSSN = "999999998" Then	''no foreignssn or co_foreignssn
				isSubmitLoan = True
			End If
		Else ''only foreignssn
			If ForeignSSN = "999999999" Then
				isSubmitLoan = True
			End If
		End If
		Dim cancelSubmit As String = Request.Form("isDisagreeSelect")
		Dim loanIDAType As String = Common.SafeString(Request.Form("idaMethodType"))
		''run ida if it is SSO and also sso_ida_enabled is "Y"
		Dim disableSSOIDA = IIf(Common.SafeString(Request.Form("HasSSOIDA")) = "Y", False, IsSSO)
		If Not String.IsNullOrEmpty(loanIDAType) And Not isSubmitLoan And cancelSubmit <> "Y" And Not disableSSOIDA And Not IsComboMode Then ''check before running ida
			Dim submitMessage As String = ""
			Dim responseStatus As String = Common.SubmitLoan(personalLoan, poConfig, cancelSubmit, poResponseRaw, True, submitMessage, loanRequestXMLStr)
			If Not String.IsNullOrEmpty(submitMessage) Then
				Return "<div class='justify-text'><p>" & submitMessage & "</p></div>" ''fail to submit return message
			End If
			'' get loanId
			loanID = Common.getResponseLoanID(poResponseRaw)
			Dim psWalletQuestions As String = ""
			Dim sName As String = Common.SafeString(Request.Form("FirstName"))
			Dim isJoint As Boolean = False
			''execute wallet question for primary 
			Dim strResponse As String = Common.getWalletQuestionsResponseXML(poConfig, poResponseRaw, loanIDAType, isJoint)
			If String.IsNullOrEmpty(strResponse) Then
				Dim loanRequestXML As XmlDocument = New XmlDocument()
				loanRequestXML.LoadXml(loanRequestXMLStr)
				submitMessage = Common.WebPostResponse(poConfig, loanRequestXML, poResponseRaw, False)
				Return "<div class='justify-text'><p>" & submitMessage & "</p></div>" ''failed to response wallet question --> return submitmessage
			End If
			Dim strRenderWalletQuestions As String = Common.renderWalletQuestionsHTML(loanIDAType, strResponse, questionList_persist, sName, transaction_ID, question_set_ID, request_app_ID, request_client_ID, request_sequence_ID, reference_number)
			''IsJointApplication = hasCoApp ''use for second call back
			Return strRenderWalletQuestions
		Else ''normal process
			Return Common.SubmitLoan(personalLoan, poConfig, cancelSubmit, poResponseRaw, isSubmitLoan, "", loanRequestXMLStr)
		End If
	End Function

	'to be deprecate and delete
	'  Private Function ValidateInput() As String
	'      Dim errMessage As String = String.Empty

	'Return errMessage
	'      Dim flag As Boolean = False

	'      'CPBLogger.logInfo("Personal Loan: Starting input validation")

	'      If String.IsNullOrEmpty(Request.Form("LenderId")) And String.IsNullOrEmpty(Request.Form("LenderRef")) Then
	'          Return "LenderId is missing!"
	'      End If

	'      Dim hasPreviousAddress As String = Common.SafeString(Request.Form("hasPreviousAddress"))
	'      Dim hasCoPreviousAddress As String = Common.SafeString(Request.Form("co_hasPreviousAddress"))

	'      Dim isForeignAddress As Boolean = If(Common.SafeString(Request.Form("Country")).ToUpper <> "USA" And Common.SafeString(Request.Form("Country")).ToUpper <> "", True, False)
	'      Dim isCoForeignAddress As Boolean = If(Common.SafeString(Request.Form("co_Country")).ToUpper <> "USA" And Common.SafeString(Request.Form("co_Country")).ToUpper <> "", True, False)

	'      Dim hasCoApp As Boolean = Request.Form("HasCoApp") = "Y"

	'      Dim isRetired As Boolean = Request.Form("EmploymentStatus") = "RETIRED" _
	'       Or Request.Form("EmploymentStatus") = "RETIRED MILITARY" _
	'       Or Request.Form("EmploymentStatus") = "UNEMPLOYED" _
	'       Or Request.Form("EmploymentStatus") = "STUDENT" _
	'       Or Request.Form("EmploymentStatus") = "HOMEMAKER"

	'      Dim isCoRetired As Boolean = Request.Form("co_EmploymentStatus") = "RETIRED" _
	'       Or Request.Form("co_EmploymentStatus") = "RETIRED MILITARY" _
	'        Or Request.Form("co_EmploymentStatus") = "UNEMPLOYED" _
	'        Or Request.Form("co_EmploymentStatus") = "STUDENT" _
	'        Or Request.Form("co_EmploymentStatus") = "HOMEMAKER"

	'      Dim validateIDCountryRequire As Boolean = Request.Form("IDCardType") = "BIRTH_CERT" _
	'          Or Request.Form("IDCardType") = "PASSPORT" _
	'          Or Request.Form("IDCardType") = "FOREIGN_ID" _
	'          Or Request.Form("IDCardType") = "FRGN_DRVRS"

	'      Dim validateCoIDCountryRequire As Boolean = Request.Form("co_IDCardType") = "BIRTH_CERT" _
	'          Or Request.Form("co_IDCardType") = "PASSPORT" _
	'          Or Request.Form("co_IDCardType") = "FOREIGN_ID" _
	'          Or Request.Form("co_IDCardType") = "FRGN_DRVRS"

	'      For Each key As String In Request.Form.Keys
	'          If String.IsNullOrEmpty(Request.Form.Item(key).Trim()) Then
	'              Dim sField As String = key.ToLower()
	'              If sField.StartsWith("co_") AndAlso Not hasCoApp Then Continue For
	'              If sField = "platformsource" Then Continue For
	'              If sField = "lenderid" Or sField = "lenderref" Then Continue For
	'              If sField.Contains("branchid") Then Continue For
	'              If sField = "referralsource" Then Continue For
	'              If sField = "image" Then Continue For
	'              If sField = "txtjobtitle" AndAlso isRetired Then Continue For 'more relax than client side
	'              If sField = "txtemployer" Then Continue For 'more relax than client side
	'              If sField = "txtemployedduration" Then Continue For 'more relax than client side
	'              If sField = "grossmonthlyincome" AndAlso isRetired Then Continue For 'more relax than client side
	'              If sField = "totalmonthlyhousingexpense" Then Continue For 'TODO(not critical): should check occupying status also
	'              If sField = "co_txtjobtitle" AndAlso isCoRetired Then Continue For
	'              If sField = "co_txtemployer" Then Continue For
	'              If sField = "co_txtemployedduration" Then Continue For
	'              If sField = "co_grossmonthlyincome" AndAlso isCoRetired Then Continue For
	'              If sField = "co_totalmonthlyhousingexpense" Then Continue For
	'              If sField.Contains("description") Then Continue For
	'              If sField = "employeeoflender" Then Continue For
	'              If sField = "co_employeeoflender" Then Continue For
	'              If sField.Contains("middlename") _
	'              OrElse sField.Contains("driverlicense") _
	'                OrElse sField.Contains("workphone") _
	'              OrElse sField.Contains("workphoneext") _
	'              OrElse sField.Contains("mobilephone") Then Continue For
	'              If sField.Contains("namesuffix") Then Continue For
	'              If sField.Contains("idamethodtype") Then Continue For
	'              If sField.Contains("loanpurpose") Then Continue For
	'              If sField.Contains("txtets") Then Continue For
	'              If sField.Contains("membernumber") Then Continue For
	'              If sField.Contains("txtemployer") OrElse _
	'                sField.Contains("txtsupervisorname") OrElse _
	'                sField.Contains("txtemploymentstartdate") OrElse _
	'                sField.Contains("txtemployedduration_year") OrElse _
	'                sField.Contains("txtprofessionduration_year") OrElse _
	'                sField.Contains("txtemployedduration_month") OrElse _
	'                sField.Contains("txtprofessionduration_month") OrElse _
	'                sField.Contains("txtbusiness") OrElse _
	'                  sField.Contains("ddlbranchofservice") OrElse _
	'                  sField.Contains("ddlpaygrade") OrElse _
	'                sField.Contains("co_txtbusiness") Then
	'                  Continue For
	'              End If
	'              ''skip other monthly income
	'              If sField.Contains("othermonthlyincome") Then Continue For

	'              'validate Identification
	'              If sField = "idcountry" AndAlso validateIDCountryRequire = False Then Continue For
	'              If sField = "idstate" AndAlso validateIDCountryRequire Then Continue For
	'              If sField = "co_idcountry" AndAlso validateCoIDCountryRequire = False Then Continue For
	'              If sField = "co_idstate" AndAlso validateCoIDCountryRequire Then Continue For

	'              'foreing stuff
	'              If sField.Contains("homephonecountry") Then Continue For
	'              If sField.Contains("mobilephonecountry") Then Continue For
	'              If sField.Contains("workphonecountry") Then Continue For
	'              If isForeignAddress Then ''primary
	'                  If sField.StartsWith("zip") Then Continue For
	'                  If sField.StartsWith("city") Then Continue For
	'                  If sField.StartsWith("state") Then Continue For
	'              End If
	'              If isCoForeignAddress Then ''joint
	'                  If sField.Contains("co_zip") Then Continue For
	'                  If sField.Contains("co_city") Then Continue For
	'                  If sField.Contains("co_state") Then Continue For
	'              End If

	'              If hasPreviousAddress <> "Y" Then
	'                  If sField.StartsWith("previousaddress") Then Continue For
	'              End If
	'              If hasCoPreviousAddress <> "Y" Then
	'                  If sField.StartsWith("co_previousaddress") Then Continue For
	'              End If

	'              'skip previous employment
	'              If sField.StartsWith("prev_") Then Continue For
	'              If sField.StartsWith("co_prev_") Then Continue For
	'              'skip GMI
	'              If sField.Contains("co_hasgmi") Then Continue For
	'              If sField.Contains("hasgmi") Then Continue For
	'              If sField.Contains("co_gender") Then Continue For
	'              If sField.Contains("gender") Then Continue For
	'              If sField.Contains("co_ethnicity") Then Continue For
	'              If sField.Contains("ethnicity") Then Continue For
	'              If sField.Contains("co_race") Then Continue For
	'              If sField.Contains("race") Then Continue For
	'              If sField.Contains("co_declinedansweraacegender") Then Continue For
	'              If sField.Contains("declinedansweraacegender") Then Continue For
	'              ''skip preferred contact method
	'		If sField.Contains("contactmethod") Then Continue For

	'		If sField = "occupancydescription" Then
	'			If Request.Form("occupyinglocation").ToUpper() <> "OTHER" Or CollectDescriptionIfOccupancyStatusIsOther.ToUpper() <> "Y" Then Continue For
	'		End If
	'		If sField = "co_occupancydescription" Then
	'			If Request.Form("co_occupyinglocation").ToUpper() <> "OTHER" Or CollectDescriptionIfOccupancyStatusIsOther.ToUpper() <> "Y" Then Continue For
	'		End If

	'		If sField = "employmentdescription" Then
	'			If Request.Form("employmentstatus").ToUpper() <> "OTHER" Or CollectDescriptionIfEmploymentStatusIsOther.ToUpper() <> "Y" Then Continue For
	'		End If
	'		If sField = "co_employmentdescription" Then
	'			If Request.Form("co_employmentstatus").ToUpper() <> "OTHER" Or CollectDescriptionIfEmploymentStatusIsOther.ToUpper() <> "Y" Then Continue For
	'		End If

	'              errMessage &= key & "<br />"
	'              flag = True
	'          End If
	'      Next
	'      If Not Common.ValidateEmail(Common.SafeString(Request.Form("EmailAddr"))) Then
	'          errMessage &= "Please input valid email address</br>"
	'          flag = True
	'      End If
	'      If flag Then
	'          errMessage = "<b><span style='color: red'>Please input missing field(s):</span></b><br/>" & errMessage
	'      End If

	'      'CPBLogger.logInfo("Personal Loan: Finished input validation")

	'      If (errMessage <> "") Then
	'          errMessage &= "<div id='MLerrorMessage'></div>"
	'      End If

	'      Return errMessage
	'  End Function

	Public Class selectDocuments
		Public title As String
		Public base64data As String
	End Class


#Region "display product utitilities"

	Private Function getQualifyProduct(ByVal xmlResponse As String) As String
		Dim strQualifyProduct As String = ""
		Dim card_rate As Double = 0.0
		Dim temp_rate As Double = 0.0
		Dim loan_term As Integer = 0
		Dim monthly_payment As Double = 0.0
		Dim index As Integer = 0
		If (Common.checkIsQualified(xmlResponse)) Then
			Dim doc As New XmlDocument()
			doc.LoadXml(xmlResponse)
			Dim oDecision As XmlElement = doc.SelectSingleNode("/OUTPUT/RESPONSE/DECISION")
			Dim strQualifyCard As String = oDecision.ChildNodes(0).InnerText
			Dim cdoc As New XmlDocument()
			cdoc.LoadXml(strQualifyCard)
			Dim oQualifyProduct As XmlElement = cdoc.SelectSingleNode("/QUALIFIED_PRODUCTS")
			If (oQualifyProduct IsNot Nothing) Then
				For Each productNode As XmlElement In oQualifyProduct
					Dim oProductRate As XmlElement = productNode.SelectSingleNode("RATE")
					If oProductRate Is Nothing Then Continue For
					card_rate = Common.SafeDouble(oProductRate.GetAttribute("rate"))
					If (index = 0) Then
						temp_rate = Common.SafeDouble(oProductRate.GetAttribute("rate")) ''get first rate
					End If
					If temp_rate > card_rate Then
						temp_rate = card_rate ''scan the loop and get the lowest rate
					End If
					index += 1
				Next
				''get the product with lowest rate
				For Each productNode As XmlElement In oQualifyProduct
					Dim oProductRate As XmlElement = productNode.SelectSingleNode("RATE")
					If oProductRate Is Nothing Then Continue For
					If temp_rate = Common.SafeDouble(oProductRate.GetAttribute("rate")) Then
						'strQualifyProduct = oProductRate.GetAttribute("max_loan_amount") + "," + oProductRate.GetAttribute("rate").ToString() + "," + oProductRate.GetAttribute("max_loan_term").ToString() + "," + productNode.GetAttribute("monthly_payment").ToString()

						'since the respone doesn't has the actual approved amount and term( has only max ammount and min max term)
						strQualifyProduct = Common.SafeString(Request.Form("LoanAmount")).Replace("$", "") + "|" + Common.SafeString(oProductRate.GetAttribute("rate")) + "|" + Common.SafeString(Request.Form("LoanTerm")) + "|" + Common.SafeString(productNode.GetAttribute("monthly_payment")) + "|" + Common.SafeString(oProductRate.GetAttribute("max_loan_amount"))	 '
						'CPBLogger.logInfo("strQualifyProduct: " & (strQualifyProduct))
						Exit For
					End If
				Next
			End If
		End If
		Return strQualifyProduct
	End Function
	Private Function getQualifyProduct2_0(ByVal xmlResponse As String) As String
		Dim strQualifyProduct As String = ""
		Dim card_rate As Double = 0.0
		Dim temp_rate As Double = 0.0
		Dim index As Integer = 0
        If (Common.checkIsQualified(xmlResponse)) Then
            Dim doc As New XmlDocument()
            doc.LoadXml(xmlResponse)
            Dim oDecision As XmlElement = doc.SelectSingleNode("/OUTPUT/RESPONSE/DECISION")
            Dim strQualifyCard As String

            'oDecision may be qualify but has no product so null childnode
            Try
                strQualifyCard = oDecision.ChildNodes(0).InnerText
            Catch ex As Exception
                Return strQualifyProduct
            End Try


            Dim cdoc As New XmlDocument()
            cdoc.LoadXml(strQualifyCard)

            Dim oQualifyProducts As XmlNodeList = cdoc.SelectNodes("/PRODUCTS/PL_PRODUCT")
            Dim oQualifyProduct As XmlElement = oQualifyProducts(0)

            Dim oProductRate As XmlElement = oQualifyProduct.SelectSingleNode("RATE")
            Dim bIsPreQual As Boolean = oDecision.GetAttribute("status") = "PREQUALIFIED"
            If oProductRate IsNot Nothing And bIsPreQual Then
				strQualifyProduct = oQualifyProduct.GetAttribute("amount_approved").Replace("$", "") + "|" + oProductRate.GetAttribute("rate").ToString() + "|" + oQualifyProduct.GetAttribute("term").ToString() + "|" + oQualifyProduct.GetAttribute("estimated_monthly_payment").ToString() + "|" + oQualifyProduct.GetAttribute("max_amount_approved").Replace("$", "") + "|" + "PreQualified"
			ElseIf oProductRate IsNot Nothing And Not bIsPreQual Then
                strQualifyProduct = oQualifyProduct.GetAttribute("amount_approved").Replace("$", "") + "|" + oProductRate.GetAttribute("rate").ToString() + "|" + oQualifyProduct.GetAttribute("term").ToString() + "|" + oQualifyProduct.GetAttribute("estimated_monthly_payment").ToString() + "|" + oQualifyProduct.GetAttribute("max_amount_approved").Replace("$", "")
            End If
        End If
        Return strQualifyProduct
	End Function
	Private Function displayProduct() As String
		Dim strHtml As String = ""
		Dim strQualifiedMessage As String = ""
		If (Not String.IsNullOrEmpty(QualifiedProduct)) Then
			Dim strQualifyProduct As String() = QualifiedProduct.Split("|")
			strHtml += "<div id='qualifyProduct'>"
			strHtml += "<table class='table-format'>"
			If strQualifyProduct.Length > 5 Then 'prequalified
				strQualifiedMessage = "<p><b><center>You have successfully pre-qualified for a personal loan.</center></b></p>"
				strHtml += strQualifiedMessage & "<tr style='background-color:lime'> <th colspan='5'><b><center>Pre-Qualified Product</center></b></th></tr>"
			Else 'qualified
				strQualifiedMessage = "<p><b><center>You have successfully qualified for a personal loan.</center></b><p>"
				strHtml += strQualifiedMessage & "<tr style='background-color:lime'> <th colspan='5'><b><center>Qualified Product</center></b></th></tr>"
			End If
			'strHtml += "<tr style='background-color:lime'> <th colspan='4'><b><center>Qualified Product</center></b></th></tr>"
			strHtml += "<tr><td><b>Pre-Approved Amount</b></td><td><b>Rate</b></td><td><b>Term (months)</b></td><td><b>Estimated Monthly Payment</b></td><td><b>Max Approved Amount</b></tr>"
			If strQualifyProduct(0) <> "" Then
				strHtml += "<tr><td>" + FormatCurrency(strQualifyProduct(0), 2) + "</td>"  'Pre-Approved Amount
			Else
				strHtml += "<tr><td></td>"	 'empty Pre-Approved Amount
			End If
			strHtml += "<td>" + strQualifyProduct(1) + "%</td>"
			strHtml += "<td>" + strQualifyProduct(2) + "</td>"
			''make sure maxApprove Account is not negative number
			Dim maxAmount As Double = Common.SafeDouble(strQualifyProduct(4))
			Dim strMaxAmount As String = ""
			If maxAmount > 0 Then
				strMaxAmount = FormatCurrency(strQualifyProduct(4), 2)	'Max Approved Amoun
			End If
			strHtml += "<td>" + FormatCurrency(strQualifyProduct(3), 2) + "</td><td>" + strMaxAmount + "</td></tr></table></div><br/>"	  'Estimated Monthly Payment , Max Approved Amoun
		End If
		Dim showQualifiedProduct As String = Common.showQualifiedProduct(_CurrentWebsiteConfig)
		If showQualifiedProduct = "N" Then
			strHtml = "<div style='display:none'>" & strHtml & "</div>"
		End If
		Return strHtml
	End Function

#End Region

#Region "comboMode"


	Protected Sub ExecComboModeFlow()
        Dim bIsPassUnderwrite As Boolean = False
        Dim oUnderwriteCommon As UnderwriteCommon = New UnderwriteCommon()
        Dim sloanIDA = Common.getIDAType(_CurrentWebsiteConfig, "PERSONAL_LOAN")
        If (Request.Form("Task") = "SubmitLoan") Then

            Dim sReason As String = CApplicantBlockLogic.Execute(_CurrentWebsiteConfig, Request, "PL")
			If Not String.IsNullOrEmpty(sReason) Then
				Response.Write(sReason)
				Return
			End If

			log.Info("Executing Combo mode submission process. ProceedWithXAAnyway=" & IIf(_ProceedXAAnyway, "Y", "F"))

			'1. submitloan
			DecisionMessage = Server.HtmlDecode(SubmitPersonalLoan(GetPersonalLoanObject(_CurrentWebsiteConfig), _CurrentWebsiteConfig, loanResponseXMLStr)) 'create new loan with external source =  MOBILE WEBSITE COMBO, error handling is already taken care of in this process
			''check user select disagree button, and disagree_popup is on then we submit data to LPQ using submitloan and stop process 
			If Common.SafeString(Request.Form("isDisagreeSelect")) = "Y" Then
				log.Info("User disagreed to submitting the application. Halting Combo mode flow.")
				Return
			End If

			Decision_v2_Response = loanResponseXMLStr   'processed loandecision message,persist for later use
            loanID = Common.getResponseLoanID(loanResponseXMLStr)
			If loanID = "" Then 'something is wrong so just exit and return the message
				log.Warn("Returned LoanID is empty. Halting Combo mode flow.")
				Response.Write(DecisionMessage)
				Return
			End If
			If _CurrentWebsiteConfig.SubmitLoanUrl.Contains("decisionloan/2.0") Then
                QualifiedProduct = getQualifyProduct2_0(loanResponseXMLStr)
            Else
                QualifiedProduct = getQualifyProduct(loanResponseXMLStr)
            End If
            Dim IsDeclinedLoan As Boolean = Common.checkIsDeclined(loanResponseXMLStr)

            ''NEW MOCKUP
            'I) LOAN=DECLINED
            ''CHOICE = DON'T WANT
            ''NO CREATE XA
            ''LOAN DECLINED MESSAGE(1)
            If IsDeclinedLoan And Not _ProceedXAAnyway Then 'declined - don't care so just respsone with SUBMITTED_MESSAGE 'This is the only senarios where XA is not submitted
                LoanSubmit.UpdateLoan(loanID, _CurrentWebsiteConfig, "LOANDECLINED_XANOTPROCEED") 'don't update status, just add decision comment
                Response.Write(DecisionMessage)
                Return
            End If
            ''If IsDeclinedLoan And Not _ProceedXAAnyway Then 'declined - don't care so just respsone with SUBMITTED_MESSAGE 'This is the only senarios where XA is not submitted
            ''    '' 3)INSTANT DELCINED LOAN (WANTS NO MEMBERSHIP) 
            ''    LoanSubmit.UpdateLoan(loanID, _CurrentWebsiteConfig, "XANOTPROCEED") 'don't update status, just add decision comment
            ''    Response.Write(DecisionMessage)
            ''    Return
            ''ElseIf QualifiedProduct.Count = 0 Then  'start membership eventhought consumer dont qualify for a loan, amy be able to sarvage if loan if  qualify later
            ''    If _ProceedXAAnyway Then
            ''        LoanSubmit.UpdateLoan(loanID, _CurrentWebsiteConfig, "XAPROCEED")
            ''    Else
            ''        LoanSubmit.UpdateLoan(loanID, _CurrentWebsiteConfig, "XANOTPROCEED") ' 
            ''    End If
            ''ElseIf QualifiedProduct.Count > 0 Then 'qualified, change loan status to PENDING & add decision comment so if user abort IDA/XA process the officer will know
            ''    If _ProceedXAAnyway Then
            ''        LoanSubmit.UpdateLoan(loanID, _CurrentWebsiteConfig, "PEN")  'TODO: how do loanofficer know XA status has been completed? Probaly use automate trigger(external_source and status change) to trigger a webms meesage to loanofficer
            ''    Else
            ''        LoanSubmit.UpdateLoan(loanID, _CurrentWebsiteConfig, "PENXANOTPROCEED")  'TODO: how do loanofficer know XA status has been completed? Probaly use automate trigger(external_source and status change) to trigger a webms meesage to loanofficer
            ''    End If
            ''End If
            '2.proceed with membership(XA)
            'include Product, FOM, funding(only for non-member secured credit card)
            'Dim currentProdList As List(Of CProduct) = CProduct.GetProducts(_CurrentWebsiteConfig, "1")	'ok for now, use psAvailability=1 filter for non minor/specail
            Dim currentProdList As New List(Of CProduct)
            Dim allProdList As List(Of CProduct) = CProduct.GetProductsForLoans(_CurrentWebsiteConfig)
            Dim allowedProductCodes As List(Of String) = GetAllowedProductCodes("PERSONAL_LOAN")    'now, loans has their own configured products, so get all product and filter by those configured products
            If allProdList IsNot Nothing And allProdList.Any() Then
                If allowedProductCodes IsNot Nothing And allowedProductCodes.Any() Then
                    currentProdList = allProdList.Where(Function(p) allowedProductCodes.Contains(p.ProductCode)).ToList() ' perform filter by configured product in config xml (if any)
                Else
                    currentProdList = allProdList
                End If
            End If

			If _xaContinueWhenLoanIsReferred = "N" And QualifiedProduct.Count = 0 And Not _ProceedXAAnyway Then
				log.Info("Loan is declined, but the user has agreed to proceed with XA creation anyway.")
				'don't want continue with membership if loan app is refferred or fraud 
				Dim customSubmitMessage = Server.HtmlDecode(CCustomMessage.getComboCustomResponseMessages(_CurrentWebsiteConfig, "SUBMITTED_MESSAGE", loanResponseXMLStr, loanRequestXMLStr, ""))
				Dim strXADataComment As String = LoanSubmit.addXADataToCommentSection(Request, _CurrentWebsiteConfig, currentProdList)
				LoanSubmit.UpdateLoan(loanID, _CurrentWebsiteConfig, "REFERREDLOAN_XANOTPROCEED", "", strXADataComment)
				If String.IsNullOrEmpty(sloanIDA) Then
					Response.Write(customSubmitMessage)
				Else
					''run ida for loan when XA app is not created.
					''execute wallet question for primary 
					Dim sFirstName As String = Common.SafeString(Request.Form("FirstName"))
					Dim sWalletQuestionResponse As String = Common.getWalletQuestionsResponseXML(_CurrentWebsiteConfig, loanResponseXMLStr, sloanIDA, False)
					If String.IsNullOrEmpty(sWalletQuestionResponse) Then
						Response.Write(customSubmitMessage)
					Else
						Dim sRenderWalletQuestions As String = Common.renderWalletQuestionsHTML(sloanIDA, sWalletQuestionResponse, questionList_persist, sFirstName, transaction_ID, question_set_ID, request_app_ID, request_client_ID, request_sequence_ID, reference_number)
						Response.Write(sRenderWalletQuestions)
					End If
				End If
				Return
			End If

			Try
                Dim sLoanNumber = Common.getLoanNumber(loanResponseXMLStr)
                LoanSubmit.SubmitXA("PL", Request, _CurrentWebsiteConfig, xaRequestXMLStr, xaResponseXMLStr, currentProdList, sLoanNumber)     'create new XA with external source =  MOBILE WEBSITE COMBO
                xaID = Common.getResponseLoanID(xaResponseXMLStr)
                '' move the loan update immediately after XA submit so if consumer abandons the IDA, the loan still have the XA number in the comment.
                If String.IsNullOrEmpty(QualifiedProduct) Then  'start membership eventhought consumer dont qualify for a loan, amy be able to sarvage if loan if  qualify later
                    If _ProceedXAAnyway Then
                        If IsDeclinedLoan Then
                            LoanSubmit.UpdateLoan(loanID, _CurrentWebsiteConfig, "LOANDECLINED_XAPROCEED", Common.getLoanNumber(xaResponseXMLStr))
                        Else
                            LoanSubmit.UpdateLoan(loanID, _CurrentWebsiteConfig, "XAPROCEED", Common.getLoanNumber(xaResponseXMLStr))
                        End If
                    Else
                        LoanSubmit.UpdateLoan(loanID, _CurrentWebsiteConfig, "XANOTPROCEED", Common.getLoanNumber(xaResponseXMLStr))
                    End If
                Else  'qualified, change loan status to PENDING & add decision comment so if user abort IDA/XA process the officer will know
                    If _ProceedXAAnyway Then
                        LoanSubmit.UpdateLoan(loanID, _CurrentWebsiteConfig, "PEN", Common.getLoanNumber(xaResponseXMLStr))  'TODO: how do loanofficer know XA status has been completed? Probaly use automate trigger(external_source and status change) to trigger a webms meesage to loanofficer
                    Else
                        LoanSubmit.UpdateLoan(loanID, _CurrentWebsiteConfig, "PENXANOTPROCEED", Common.getLoanNumber(xaResponseXMLStr))  'TODO: how do loanofficer know XA status has been completed? Probaly use automate trigger(external_source and status change) to trigger a webms meesage to loanofficer
                    End If
                End If
                If xaID Is Nothing Or xaID = "" Then  'For PID, the webservice will automatically add failure message to internal comment  TODO: add internal comment for other webservice
                    log.Error("Error submitting XA for Combo: ")
                    ''Dim oRequestXML As XmlDocument = New XmlDocument()
                    ''oRequestXML.LoadXml(xaRequestXMLStr)
                    ''Dim responseMessage = Server.HtmlDecode(Common.WebPostResponse(_CurrentWebsiteConfig, oRequestXML, xaResponseXMLStr, False))
                    ''responseMessage = responseMessage.Replace("MLerrorMessage", "bogus") 'in combomode, dont' want consumer to resubmit duplicate loan, so force it to go to last diaglog even thought there is no XA app
                    ''Response.Write(responseMessage)	''failed -> response xa SUBMITTED_MESSAGE

                    Dim comboResponseSubmitMessage As String = CCustomMessage.getComboCustomResponseMessages(_CurrentWebsiteConfig, "COMBO_SUBMITTED_MESSAGE", loanResponseXMLStr, loanRequestXMLStr, "")
                    Response.Write(Server.HtmlDecode(comboResponseSubmitMessage)) ''failed -> response combo submit SUBMITTED_MESSAGE
                    Return
                End If
            Catch ex As Exception
                'TODO: should have clear the screen and prevent another post back
                log.Error("Error submitting XA for Combo: " & ex.Message, ex)
                ''Dim oRequestXML As XmlDocument = New XmlDocument()
                ''oRequestXML.LoadXml(xaRequestXMLStr)
                ''Dim responseMessage = Server.HtmlDecode(Common.WebPostResponse(_CurrentWebsiteConfig, oRequestXML, xaResponseXMLStr, False))
                ''responseMessage = responseMessage.Replace("MLerrorMessage", "bogus") 'in combomode, dont' want consumer to resubmit duplicate loan, so force it to go to last diaglog even thought there is no XA app
                ''Response.Write(responseMessage)	''failed -> response xa SUBMITTED_MESSAGE

                Dim comboResponseSubmitMessage As String = CCustomMessage.getComboCustomResponseMessages(_CurrentWebsiteConfig, "COMBO_SUBMITTED_MESSAGE", loanResponseXMLStr, loanRequestXMLStr, "")
                Response.Write(Server.HtmlDecode(comboResponseSubmitMessage)) ''failed -> response combo submit SUBMITTED_MESSAGE
                Return
            End Try

            ''check status for loans if it is FRAUD/DUP before to do underwriting for xa
            Dim sLoanStatus As String = LoanSubmit.getLoanStatus(loanID, _CurrentWebsiteConfig).ToUpper
            If sLoanStatus = "FRAUD" Or sLoanStatus = "DUP" Then
                If _ProceedXAAnyway Then
                    Response.Write(Server.HtmlDecode(CCustomMessage.getComboCustomResponseMessages(_CurrentWebsiteConfig, "COMBO_SUBMITTED_MESSAGE", loanResponseXMLStr, loanRequestXMLStr, "")))
                Else
                    Response.Write(Server.HtmlDecode(CCustomMessage.getComboCustomResponseMessages(_CurrentWebsiteConfig, "SUBMITTED_MESSAGE", loanResponseXMLStr, loanRequestXMLStr, "")))
                End If
                Return '' no contiue
            End If ''end check status for loans

            ''check pre UnderWrite for xaCombo
            Dim xaRequestXMLDoc As New XmlDocument()
            xaRequestXMLDoc.LoadXml(xaRequestXMLStr)
            If Not oUnderwriteCommon.PreUnderWrite(_CurrentWebsiteConfig, xaRequestXMLDoc, Request, IsJointApplication) Then
                Response.Write(Server.HtmlDecode(CCustomMessage.getComboCustomResponseMessages(_CurrentWebsiteConfig, "COMBO_SUBMITTED_MESSAGE", loanResponseXMLStr, loanRequestXMLStr, "")))
                Return
            End If

            'badMember?
            If _CurrentWebsiteConfig.LenderCode <> "" Then
                If oUnderwriteCommon.isBadMember(_CurrentWebsiteConfig, Request) Then
                    Response.Write(Server.HtmlDecode(CCustomMessage.getComboCustomResponseMessages(_CurrentWebsiteConfig, "COMBO_SUBMITTED_MESSAGE", loanResponseXMLStr, loanRequestXMLStr, "")))
                    Return
                End If
            End If
            '3.Underwrite if enabled(decisionxa,debit,IDA)
            'a.IDA
            Dim oAccountNodes As XmlNodeList = _CurrentWebsiteConfig._webConfigXML.SelectNodes("PERSONAL_LOAN/ACCOUNTS/ACCOUNT")
            Dim selectedProductList As List(Of CProduct) = oUnderwriteCommon.getSettingProductList(_CurrentWebsiteConfig, GetAllowedProductCodes("PERSONAL_LOAN"), Request)
            Dim bIDA_LPQConfig As Boolean = oUnderwriteCommon.isIDALPQConfig(selectedProductList)
            SelectedProducts = ""
            If selectedProductList.Count > 0 Then
                SelectedProducts = New JavaScriptSerializer().Serialize(selectedProductList)
            End If

            ''Dim prerequisiteProduct As New CProduct
            ''Dim bIDA_LPQConfig As Boolean = False
            ''If oAccountNodes.Count > 0 Then
            ''    prerequisiteProduct.ProductCode = oAccountNodes(0).Attributes("product_code").InnerXml
            ''    If currentProdList IsNot Nothing And currentProdList.Any() Then
            ''        Dim cproduct As CProduct = currentProdList.FirstOrDefault(Function(cp As CProduct) cp.ProductCode = prerequisiteProduct.ProductCode)
            ''        If cproduct IsNot Nothing Then
            ''            prerequisiteProduct = cproduct
            ''            bIDA_LPQConfig = prerequisiteProduct.AutoPullIDAuthenticationConsumerPrimary
            ''        End If
            ''    End If
            ''End If
            Dim xaIDAType As String = _CurrentWebsiteConfig.AuthenticationType 'IDA attribute from main node for xa
            Dim bIDA_LPQMobileWebsiteConfig As Boolean = (xaIDAType <> "")
            Dim hasWalletQuestion As Boolean = True
            'Run IDA when enable on both mobile config and lender side
            If bIDA_LPQConfig And bIDA_LPQMobileWebsiteConfig Then
                ''execute wallet question for primary 
                Dim strResponse As String = Common.getWalletQuestionsResponseXML(_CurrentWebsiteConfig, xaResponseXMLStr, xaIDAType, False) 'do primary first
                If strResponse = "" Then 'failed IDA request
                    hasWalletQuestion = False
                End If

                If Not String.IsNullOrEmpty(strResponse) Then
                    Dim sName As String = Request.Form("FirstName")
                    Dim strRenderWalletQuestions As String = Common.renderWalletQuestionsHTML(xaIDAType, strResponse, questionList_persist, sName, transaction_ID, question_set_ID, request_app_ID, request_client_ID, request_sequence_ID, reference_number)
                    '4. Display Wallet question (IDA)
                    Response.Write(strRenderWalletQuestions)
                    Return
                End If
            End If
            bIsPassUnderwrite = True 'if reach here then IDA is not required 
            If Not hasWalletQuestion Then
                bIsPassUnderwrite = False
            End If
        End If
        If CheckComboLoanIDA(sloanIDA) Then
            ExecuteWalletQuestionsAndAnswersForComboLoan(sloanIDA)
            Return
        End If
        '5. Validate Wallet question
        If (Request.Form("Task") = "WalletQuestions") Then
			''wallet answer
			bIsPassUnderwrite = True
			''Dim combinedAnswerStr As String = Common.SafeString(Request.Form("WalletAnswers"))
			''Dim answerIDList As List(Of String) = Common.parseAnswersForID(combinedAnswerStr)
			Dim sWalletQuestionsAndAnswers As String = Common.SafeString(Request.Form("WalletQuestionsAndAnswers"))
			Dim loanIDAType As String = _CurrentWebsiteConfig.AuthenticationType
			Dim idaResponseXAIDList As List(Of String) = idaResponseXAIDs(loanIDAType)
			Dim hasJointWalletAnswer As String = Common.SafeString(Request.Form("hasJointWalletAnswer"))
			Dim isJointAnswers = hasJointWalletAnswer = "Y"
			Dim isExecuteAnswer As Boolean = Common.ExecWalletAnswers(_CurrentWebsiteConfig, sWalletQuestionsAndAnswers, questionList_persist, idaResponseXAIDList, isJointAnswers, question_set_ID, request_app_ID, request_client_ID, request_sequence_ID, reference_number)
			'isExecuteAnswer = True
			Dim oRequestXML As XmlDocument = New XmlDocument()
			oRequestXML.LoadXml(xaRequestXMLStr)
			If Not isExecuteAnswer Then	 'PID service will automatically insert msg into internal comments  TODO:log for other service
				''responseMessage = Server.HtmlDecode(Common.WebPostResponse(_CurrentWebsiteConfig, oRequestXML, xaResponseXMLStr, False))
				''Response.Write(responseMessage) ''failed -> response submit message
				bIsPassUnderwrite = False ' use in step 6
			End If
			' populate questions for joint 
			If (IsJointApplication And Not isJointAnswers) Then
				''response wallet question for joint
				Dim strResponse As String = Common.getWalletQuestionsResponseXML(_CurrentWebsiteConfig, xaResponseXMLStr, loanIDAType, IsJointApplication)
				If String.IsNullOrEmpty(strResponse) Then
					''responseMessage = Server.HtmlDecode(Common.WebPostResponse(_CurrentWebsiteConfig, oRequestXML, xaResponseXMLStr, False))
					''Response.Write(responseMessage) ''failed -> response submit message
					bIsPassUnderwrite = False ' use in step 6
				Else
					Dim strRenderWalletQuestions As String = Common.renderWalletQuestionsHTML(loanIDAType, strResponse, questionList_persist, Co_FName, transaction_ID, question_set_ID, request_app_ID, request_client_ID, request_sequence_ID, reference_number)
					Response.Clear()
					Response.Write(strRenderWalletQuestions) ''response wallet question for joint
					Return
				End If
			End If

		End If

        If _CurrentWebsiteConfig.SubmitLoanUrl.Contains("decisionloan/2.0") Then
            QualifiedProduct = getQualifyProduct2_0(loanResponseXMLStr)
        Else
            QualifiedProduct = getQualifyProduct(loanResponseXMLStr)
        End If
        '6.Update loan status 
        Dim isPreQualified = Common.checkIsPreQualified(loanResponseXMLStr)
        Dim sXALoanNumber = Common.getLoanNumber(xaResponseXMLStr)
        Dim comboSubmitMessage As String = Server.HtmlDecode(CCustomMessage.getComboCustomResponseMessages(_CurrentWebsiteConfig, "COMBO_SUBMITTED_MESSAGE", loanResponseXMLStr, loanRequestXMLStr, ""))
        Dim comboXASubmitMessage As String = Server.HtmlDecode(CCustomMessage.getComboCustomResponseMessages(_CurrentWebsiteConfig, "COMBO_XA_SUBMITTED_MESSAGE", loanResponseXMLStr, loanRequestXMLStr, ""))
        Dim declinedMessage As String = Server.HtmlDecode(CCustomMessage.getComboCustomResponseMessages(_CurrentWebsiteConfig, "DECLINED_MESSAGE", loanResponseXMLStr, loanRequestXMLStr, ""))
        Dim preQualifiedMessage As String = Server.HtmlDecode(CCustomMessage.getComboCustomResponseMessages(_CurrentWebsiteConfig, "DECLINED_MESSAGE", loanResponseXMLStr, loanRequestXMLStr, ""))
        Dim submitMessage As String = Server.HtmlDecode(CCustomMessage.getComboCustomResponseMessages(_CurrentWebsiteConfig, "SUBMITTED_MESSAGE", loanResponseXMLStr, loanRequestXMLStr, ""))
        comboSubmitMessage = Common.PrependIconForNonApprovedMessage(comboSubmitMessage, "Thank You")
        ''-----------run debit, creditpull or decisionXA after passed IDA or Not required to Run IDA-------------
        If bIsPassUnderwrite Then
            'Dim SettingProductsList = oUnderwriteCommon.getSettingProductList(_CurrentWebsiteConfig, GetAllowedProductCodes("PERSONAL_LOAN"), Request)
            Dim SettingProductsList As New List(Of CProduct)
            If SelectedProducts <> "" Then
                SettingProductsList = New JavaScriptSerializer().Deserialize(Of List(Of CProduct))(SelectedProducts)
            End If
            ''-------run debit ------------
            If Not oUnderwriteCommon.executeDebit(_CurrentWebsiteConfig, SettingProductsList, xaID, IsJointApplication) Then
                bIsPassUnderwrite = False ''reset passUnderWrite
            End If

            ''c. ----pull credit or do decisionXA------
            If _CurrentWebsiteConfig.IsDecisionXAEnable Then
                Dim sXMLresponse = oUnderwriteCommon.ExecuteDecisionXA(_CurrentWebsiteConfig, xaID)
                If Not oUnderwriteCommon.isXAProductQualified(sXMLresponse) Then
                    bIsPassUnderwrite = False ''reset passUnderWrite
                End If
            Else
                If oUnderwriteCommon.isAutoPullCredit(SettingProductsList) Then
                    If Not oUnderwriteCommon.ExecuteCreditPull(_CurrentWebsiteConfig, xaID) Then
                        bIsPassUnderwrite = False ''reset passUnderWrite
                    End If
                End If
            End If
            ''-------end pull credit or do decisionXA(after passed wallet answer) ---------
        End If
        ''--------end run debit, creditpull or decisionXA after passed IDA or Not required to Run IDA-------------
        If Not bIsPassUnderwrite Then 'failed underwrite
            If _ProceedXAAnyway And Common.checkIsDeclined(loanResponseXMLStr) Then
                ''NEW MOCKUP
                'I) LOAN=DECLINED
                ''CHOICE = WANT
                ''XA =REFERRED
                ''LOAN DECLINED MESSAGE(1)
                Response.Write(declinedMessage)
                LoanSubmit.UpdateLoan(loanID, _CurrentWebsiteConfig, "XAFAILED", sXALoanNumber) ''leave status alone but add decision comments
                Return
            End If
            ''for pl QualifiedProduc is a string( not a list of string)
            If Not String.IsNullOrEmpty(QualifiedProduct) Then
                '2)INSTANT APPROVED FOR LOAN & REFERRED FOR MEMBERSHIP
                LoanSubmit.UpdateLoan(loanID, _CurrentWebsiteConfig, "AA_XAFAILED", sXALoanNumber)  ''update loan status to instant approved and add decision comments: MEMBERSHIP has been REFFERED"
                If isPreQualified Then
                    ''NEW MOCKUP
                    'IV) LOAN=PRE-QUALIFIED
                    ''CHOICE = WANT/DON'T WANT
                    ''XA =REFERRED
                    ''STANDARD: LOAN PRE-QUALIED MESSAGE(NEW) (7)
                    Response.Clear()
                    Response.Write(preQualifiedMessage)
                Else
                    ''NEW MOCKUP
                    'III) LOAN=APPROVED
                    ''CHOICE = WANT/DON'T WANT
                    ''XA =REFERRED
                    ''COMBO: LOAN APPROVED AND XA REFERRED MESSAGE (5) -->using existing node <COMBO_XA_SUBMITTED_MESSAGE />
                    Response.Clear()
                    Response.Write(comboXASubmitMessage)
                End If
            Else
                ''NEW MOCKUP
                'II) LOAN=REFERRED
                ''CHOICE = WANT/DON'T WANT
                ''XA =REFERRED
                If _ProceedXAAnyway Then
                    ''COMBO: LOAN REFERRED MESSAGE(2) --combo_submit_message
                    Response.Clear()
                    Response.Write(comboSubmitMessage)
                Else
                    ''STANDARD: LOAN REFERRED MESSAGE(3)
                    Response.Clear()
                    Response.Write(submitMessage)
                End If
                LoanSubmit.UpdateLoan(loanID, _CurrentWebsiteConfig, "XAFAILED", sXALoanNumber) 'leave status alone but add decision comments((MEMBERSHIP has been REFFERED"
            End If
            Return
        End If

        '7. Update loan status and XA status if passed underwrite
        If bIsPassUnderwrite Then
            ''pass underwrite then update xa status (using EnumLoanStatus) 
            LoanSubmit.UpdateXA(xaID, _CurrentWebsiteConfig)
            ''do funding process
			Dim personalLoanObject = GetPersonalLoanObject(_CurrentWebsiteConfig)
            Dim ntotalFunding As Double = Common.getTotalFundingAmount(xaRequestXMLStr)
            Dim sAccountNumber As String = ""
            If _ProceedXAAnyway And Common.checkIsDeclined(loanResponseXMLStr) Then
                ''NEW MOCKUP
                'I) LOAN=DECLINED
                ''CHOICE = WANT
                ''XA =APPROVED
                ''LOAN DECLINED MESSAGE(1)
                ''
                ''book to core process               
				If Common.executeXAComboFundingAndBooking(_CurrentWebsiteConfig, loanID, xaID, ntotalFunding, sXALoanNumber, sAccountNumber, Function()
																																				 Return ProceedBookingValidation(personalLoanObject)
																																			 End Function) Then
					LoanSubmit.UpdateLoan(loanID, _CurrentWebsiteConfig, "XAPASSED", sXALoanNumber)	 ''leave status alone but add decision comments based on the EnumLoanStatus
				End If
                Response.Write(declinedMessage)
                Return
            End If
            If Not String.IsNullOrEmpty(QualifiedProduct) Then
                Response.Clear()
                Response.Clear()
                '' 1)INSTANT APPROVED LOAN & INSTANT APPROVED FOR MEMBERSHIP
                LoanSubmit.UpdateLoan(loanID, _CurrentWebsiteConfig, "AA", sXALoanNumber) ' update loan status to instant approved
                ''book to core process  
				If Not Common.executeXAComboFundingAndBooking(_CurrentWebsiteConfig, loanID, xaID, ntotalFunding, sXALoanNumber, sAccountNumber, Function()
																																					 Return ProceedBookingValidation(personalLoanObject)
																																				 End Function) Then
					Response.Write(comboXASubmitMessage) ''LOAN APPROVED & XA REFERRED
					Return
				End If
                ''check the custom message based on the EnumLoanStatus 
                If _CurrentWebsiteConfig.LoanStatusEnum.ToUpper <> "REFERRED" Then
                    If isPreQualified Then
                        ''NEW MOCKUP
                        'IV) LOAN=PRE-QUALIFIED
                        ''CHOICE = WANT/DON'T WANT
                        ''XA =APPROVED
                        ''COMBO: LOAN PRE-QUALIED,XA APPROVED MESSAGE(NEW) (8)
                        Dim comboPreQualifiedMsg As String = Server.HtmlDecode(CCustomMessage.getComboCustomResponseMessages(_CurrentWebsiteConfig, "COMBO_PREQUALIFIED_MESSAGE", loanResponseXMLStr, loanRequestXMLStr, sAccountNumber))
                        Response.Write(Common.PrependIconForNonApprovedMessage(comboPreQualifiedMsg, "Congratulations"))
                    Else
                        ''NEW MOCKUP
                        'III) LOAN=APPROVED
                        ''CHOICE = WANT/DON'T WANT
                        ''XA =APPROVED
                        ''COMBO: LOAN APPROVED AND XA APPROVED MESSAGE (6)  --combo_both_preapproved 
                        Dim comboBothPreApprovedMsg As String = Server.HtmlDecode(CCustomMessage.getComboCustomResponseMessages(_CurrentWebsiteConfig, "COMBO_BOTH_PREAPPROVED_MESSAGE", loanResponseXMLStr, loanRequestXMLStr, sAccountNumber))
                        comboBothPreApprovedMsg = Common.PrependIconForNonApprovedMessage(comboBothPreApprovedMsg, "Congratulations")
                        Response.Write(comboBothPreApprovedMsg)
                    End If
                Else
                    Response.Write(comboXASubmitMessage) ''LOAN APPROVED & XA REFERRED
                End If

            Else
                ''NEW MOCKUP
                'II) LOAN=REFERRED
                ''CHOICE = WANT/DON'T WANT
                ''XA =APPROVED
                If _ProceedXAAnyway Then
                    ''COMBO: LOAN REFERRED MESSAGE, XA APPROVED(4) --combo_xa_preapproved
                    ''book to core process
					If Not Common.executeXAComboFundingAndBooking(_CurrentWebsiteConfig, loanID, xaID, ntotalFunding, sXALoanNumber, sAccountNumber, Function()
																																						 Return ProceedBookingValidation(personalLoanObject)
																																					 End Function) Then
						Response.Write(comboSubmitMessage) ''LOAN REFERRED & XA REFERRED
						Return
					End If
                    ''check the custom message based on the EnumLoanStatus 
                    If _CurrentWebsiteConfig.LoanStatusEnum.ToUpper <> "REFERRED" Then
                        Dim comboXAPreApprovedMessage As String = Server.HtmlDecode(CCustomMessage.getComboCustomResponseMessages(_CurrentWebsiteConfig, "COMBO_XA_PREAPPROVED_MESSAGE", loanResponseXMLStr, loanRequestXMLStr, sAccountNumber))
                        comboXAPreApprovedMessage = Common.PrependIconForNonApprovedMessage(comboXAPreApprovedMessage, "Thank You")
                        Response.Write(comboXAPreApprovedMessage)
                    Else
                        Response.Write(comboSubmitMessage) ''LOAN REFERRED & XA REFERRED
                    End If
                Else
                    ''STANDARD: LOAN REFERRED MESSAGE(3)
                    Response.Write(submitMessage)
                End If
                LoanSubmit.UpdateLoan(loanID, _CurrentWebsiteConfig, "XAPASSED", sXALoanNumber)  ''leave status alone but add decision comments based on the EnumLoanStatus            
            End If
            Return
        End If
        ''probably don't need this for combo, don't have this for PL
        'If (Request.Form("Task") = "SelectProduct") Then 'only apply for cc using decisionloan2.0
        '	If Decision_v2_Response Is Nothing Then Return
        '	Dim selectIndex As String = Request.Form("Index")
        '	Dim sResponse As String = Common.SubmitProduct(Decision_v2_Response, _CurrentWebsiteConfig, selectIndex)
        '	Response.Clear()
        '	Response.Write(DecisionMessage)
        'End If
    End Sub


#End Region
#Region "Combo Loan IDA"
    ''the conditions for running loan ida if xa app is not created:
    ''      - no qualified products
    ''      - existing loanIda - ida='PID'
    ''      - xaContinueWhenLoanIsReferred=N
    ''      - _ProceedXAAnyway = false
    Protected Function CheckComboLoanIDA(ByVal sLoanIDAType As String) As Boolean
        Dim sQualifiedProduct As String = ""
        If _CurrentWebsiteConfig.SubmitLoanUrl.Contains("decisionloan/2.0") Then
            sQualifiedProduct = getQualifyProduct2_0(loanResponseXMLStr)
        Else
            sQualifiedProduct = (getQualifyProduct(loanResponseXMLStr))
        End If
        If _xaContinueWhenLoanIsReferred = "N" And String.IsNullOrEmpty(sQualifiedProduct) And Not String.IsNullOrEmpty(sLoanIDAType) And Not _ProceedXAAnyway Then
            Return True
        End If
        Return False
    End Function
    Protected Sub ExecuteWalletQuestionsAndAnswersForComboLoan(ByVal sLoanIDAType As String)
        Dim sLoanID = Common.getResponseLoanID(loanResponseXMLStr)
        Dim customSubmitMessage = Server.HtmlDecode(CCustomMessage.getComboCustomResponseMessages(_CurrentWebsiteConfig, "SUBMITTED_MESSAGE", loanResponseXMLStr, loanRequestXMLStr, ""))
        If (Request.Form("Task") = "WalletQuestions") Then
            Dim sWalletQuestionsAndAnswers As String = Common.SafeString(Request.Form("WalletQuestionsAndAnswers"))
            Dim idaResponseIDList As List(Of String) = idaResponseIDs(sLoanIDAType)
            Dim isJointAnswers = Common.SafeString(Request.Form("hasJointWalletAnswer")) = "Y"
            Dim isExecuteAnswer As Boolean = Common.ExecWalletAnswers(_CurrentWebsiteConfig, sWalletQuestionsAndAnswers, questionList_persist, idaResponseIDList, isJointAnswers, question_set_ID, request_app_ID, request_client_ID, request_sequence_ID, reference_number)
            Dim oRequestXML As XmlDocument = New XmlDocument()
            Dim responseMessage As String = ""
            oRequestXML.LoadXml(loanRequestXMLStr)
            If Not isExecuteAnswer Then
                ''update comment to the internal comment: RUN LOAN IDA WHEN XA IS NOT CREATED: IDA FAILED.
                LoanSubmit.UpdateLoan(sLoanID, _CurrentWebsiteConfig, "LOAN_IDA_FAILED")
                Response.Write(customSubmitMessage)
                Return
            End If
            ' populate questions for joint 
            If (IsJointApplication And Not isJointAnswers) Then
                ''response wallet question for joint
                Dim sWalletQuestionResponse As String = Common.getWalletQuestionsResponseXML(_CurrentWebsiteConfig, loanResponseXMLStr, sLoanIDAType, IsJointApplication)
                If String.IsNullOrEmpty(sWalletQuestionResponse) Then
                    Response.Write(customSubmitMessage)
                Else
                    Dim sRenderWalletQuestions As String = Common.renderWalletQuestionsHTML(sLoanIDAType, sWalletQuestionResponse, questionList_persist, Co_FName, transaction_ID, question_set_ID, request_app_ID, request_client_ID, request_sequence_ID, reference_number)
                    Response.Clear()
                    Response.Write(sWalletQuestionResponse) ''response wallet question for joint
                End If
                Return
            End If
        End If
        ''update comment to the internal comment: RUN LOAN IDA WHEN XA IS NOT CREATED: IDA PASSED.
        LoanSubmit.UpdateLoan(sLoanID, _CurrentWebsiteConfig, "LOAN_IDA_PASSED")
        Response.Write(customSubmitMessage)
    End Sub
#End Region
End Class
