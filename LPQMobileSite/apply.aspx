﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="apply.aspx.vb" Inherits="_Default" EnableViewState="false" %>
<%@ Import Namespace="LPQMobile.Utils" %>
<%@ Register TagPrefix="uc1" TagName="Piwik" Src="~/Inc/Piwik/PiwikTracking.ascx" %>
<%@ Register TagPrefix="uc" TagName="pageFooter" Src="~/Inc/PageFooter.ascx" %>
<%@ Register Src="~/xa/inc/SpecialAccountSelector.ascx" TagPrefix="uc" TagName="SpecialAccountSelector" %>
<%@ Register Src="~/xa/inc/BusinessAccountSelector.ascx" TagPrefix="uc" TagName="BusinessAccountSelector" %>
<%@ Register Src="~/xa/inc/AccountSelector.ascx" TagPrefix="uc" TagName="AccountSelector" %>
<%@ Register Src="~/bl/Inc/BusinessLoanSelector.ascx" TagPrefix="uc" TagName="BusinessLoanSelector" %>
<%@ Reference Control="~/Inc/MainApp/ApplicationBlockRules.ascx" %>
<%@ Import Namespace="System.Web.Optimization" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">
<head id="Head1" runat="server">
	
<meta charset="UTF-8"/>	
  <title>XA LPQ Mobile</title>
	<%=getFrameBreakerCode()%>
	<!--[if lte IE 8]>
		<p class="browsehappy">
		You are using an <strong>outdated</strong> browser.
		Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
		<script type="text/jscript">
			document.execCommand('Stop');
		</script>
	<![endif]-->

	<noscript>
		For full functionality of this site it is necessary to enable JavaScript.
		Here are the <a href="http://www.enable-javascript.com/" target="_blank">
		instructions how to enable JavaScript in your web browser</a>.
	</noscript>

	<meta name="viewport" content="width=device-width, initial-scale=1" />
	<link href="/css/ThirdParty/bootstrap.css" rel="stylesheet" />
    <link rel="stylesheet" href="/css/themes/default/jquery.mobile-1.4.5.min.css" />
	<%=_ThemeFileName %>
	<%:Styles.Render("~/css/default")%>
	<%:Styles.Render("~/css/thirdparty/misc")%>
   <script src="/js/jquery-1.12.4.min.js" type="text/javascript"></script>
     <%--<script src="/js/jquery.mobile-1.4.5.min.js" type="text/javascript"></script>--%>
	<%:Scripts.Render("~/js/apply")%>
	<%If String.IsNullOrEmpty(_CurrentWebsiteConfig.Favico) Then%>
	<link rel="shortcut icon" href="https://app.loanspq.com/logo/lpq_favicon.ico"  type="image/ico"  />
	<link rel="icon" href="https://app.loanspq.com/logo/lpq_favicon.ico"  type="image/ico"  />
	<%Else%>
	<link rel="shortcut icon" <%="href='" & _CurrentWebsiteConfig.Favico & "'"%> type="image/ico"  />
	<link rel="icon" <%="href='" & _CurrentWebsiteConfig.Favico & "'"%> type="image/ico"  />	
	<%End If%>
    <style>
        
        div.ui-body-c{
            background-color :#ffffff;
            color: #00457c;
        }
       
	   .lpq_container {
		background-color: #ffffff;
		box-sizing: border-box;
		margin: 0 auto;   /*outside_top, left&right*/
		max-width: 960px;
		padding: 15px 10px 5%;
		position:relative;
		width: 100%;
		}

		
		h2{
			font-family: 'Open Sans',Helvetica,Arial,sans-serif;
			font-weight: bold;
			color: #222;
			font-size: 1.3em;
			text-shadow: white 1px 1px 0;
			line-height: 1.1em;
			padding-bottom: 0.2em;
			letter-spacing: -1px;
			text-align: center;
		}

		.logo{
			margin: 0 auto;
			display: block;
		}

			/*Change style base on screen size*/
			@media (max-width: 400px) {    /*less or equal than iphone 6*/
				.logo {
					width: 250px;
				}
			}
			@media (min-width: 400px) {  /*iphone 6 plus*/
				.logo {
					width: 280px;
				}
				.lpq_container {
					padding: 20px 10px 5%;
				}
			}
    		@media (min-width: 550px) {  /*ipad*/
				.logo {
    				width: 350px;
    			}
    			.lpq_container {
                    padding: 5% 10px;
    			}
				   			
    		}
            	
         @media (min-width: 550px) and (max-height:800px){  /*less or equal then samsung galaxy tab 7.7, 8.9.1*/
                   .logo {
    				    width: 350px;
    			    }
    			.lpq_container {
                    padding: 5% 10px;
                    height:100%;
    			}
				   			
    		}
            @media (min-width: 550px) and (max-height:600px){  /*less or equal than blackBerry Play Book*/
				.logo {
    				width: 350px;
    			}
    			.lpq_container {
                    padding: 5% 10px;
                    height:70%;
    			}
				   			
    	}
      
        .button-style
        {
          /*  border: 1px solid;
			border-color: #053579;*/
			width: 90%;
			/*display: block;*/
			text-align: center;
			margin: 20px auto;

        }

        .icon-button {
            float: left;
            width: 50%;
            height: auto;
        }

        .icon-button a img {
            width: 90%;
            height: auto;
            margin-bottom: 10%;
        }

        .icon-button-container {
            max-width: 390px;
            margin: 10px auto;
            padding-left: 5%;
            overflow: hidden;
        }

        #footer {
            position: absolute;
            left: 0;
            bottom: 0;
            height: 42px;
            width: 100%;
            overflow:hidden;
        }
    		
		.browsehappy {
		  margin: 0.2em 0;
		  background: #ccc;
		  color: #000;
		  padding: 0.2em 0;
		}
		#specialAccountSelectorPage .sub_section_header, #businessAccountSelectorPage .sub_section_header, #BLSelectorPage .sub_section_header{
			padding-top: 10px;
		}
		#businessAccountEditorDialog .sub_section_header, #specialAccountEditorDialog .sub_section_header, #BLEditorDialog .sub_section_header{
			padding-top: 10px;
			padding-bottom: 10px;
		}
		#specialAccountSelectorPage .selected-roles-wrapper .sub_section_header, #businessAccountSelectorPage .selected-roles-wrapper .sub_section_header, #BLSelectorPage .selected-roles-wrapper .sub_section_header {
			margin-bottom: 10px;
		}
		#specialAccountSelectorPage .account-role-item-wrapper .account-role-item>span:after, #businessAccountSelectorPage .account-role-item-wrapper .account-role-item>span:after, #BLSelectorPage .account-role-item-wrapper .account-role-item>span:after {
			content: attr(title);
		}
		.account-editor-dialog {
			width: 100%;
		}
		.account-editor-dialog .edit-label{
			display: none;
		}
		.account-editor-dialog.edit-mod .edit-label{
			display: inline;
		}
		.account-editor-dialog.edit-mod .edit-btn {
			display: block;
		}
		.account-editor-dialog.edit-mod .edit-label+span, .account-editor-dialog.edit-mod .edit-btn+button {
			display: none;
		}
		.account-editor-dialog .div-continue {
			width: 100%;
		}
		.account-editor-dialog .div-continue .div-continue-button {
			-ms-border-radius: 4px;
			border-radius: 4px;
		}
		.account-editor-dialog .div-continue .div-continue-button.cancel-btn {
			background: none!important;
			color: inherit;
		}
    	
        .account-editor-dialog .div-continue>div.col-sm-6{
			padding: 0 6px;
		}
		.account-editor-dialog .has-joint-button+div {
			display: none;
		}
		.account-editor-dialog .has-joint-button.show-joint+div {
			display: block;
		}
		 .account-editor-dialog .has-joint-button .add-label{
			 display: inline;
		 }
		 .account-editor-dialog .has-joint-button.show-joint .add-label{
			 display: none
		 }
		 .account-editor-dialog .has-joint-button .add-label+span{
			 display: none;
		 }
		 .account-editor-dialog .has-joint-button.show-joint .add-label+span{
			 display: inline;
		 }
    </style>
	<script type="text/javascript">
	    $(function () {
	        $('.redirectURL').click(function () { 
	            var lenderBaseUrl = $('#hdLenderBaseUrl').val();
	            if (lenderBaseUrl == "") {
	                alert("Unable go to the next page.");
	                return false; //the baseLenderUrl attribute in xml config is undefined -> exit
	            }
	           
	        });
	        //var footerTheme = $('#hdFooterTheme').val();
	        //var bgColor = $('.ui-bar-' + footerTheme).css('background-color');
	        //var changeColor = changeBackgroundColor(bgColor, .6);
	        //var bgTheme = $('#hdBgTheme').val();
	        //if (bgTheme != "") {
	        //    changeColor = bgColor;
	        //}
	        ////   $('div.ui-body-c').css("background-color", changeColor);
	        //$('div.ui-page-active').css("background-color", changeColor);
            //always align company logo at center
	        $('.CompanyLogo').css('text-align', 'center');
	        handledPageHeight();
	        //handledIconHeight();
	        //$(window).resize(function () {
	        //    handledPageHeight();
	        //    handledIconHeight();
	        //});
	        
	        //$(window).load(function () {
	        //    ShowHideFooter();
	        //});
            applyHeaderThemeCss();                          
	    	//Some shortcut to directly open selector dialog without the landing page
	    	//sdfcu has special mode type=1b which the system will show a dropdown within the main app to allow consumer to select special or regular account.  With the new design, this mode is no longer supported.
	    	//For temporary backward compatibiliy, consumer hitting xa with type=1b and role is null will be automatically redirected to landing to select regualar or sepcial.  
	    	//ex: ~/xa/xpressApp.aspx?lenderref=sdfcu_test&type=1b redirect to  ~apply.aspx?lenderref=sdfcu_test&type=XA1S

	    	//for sdfcu
                <%If _URLParameterList = "XA1S" Then %>
	    	AS.FACTORY.openAccountSelectorDialog("xa");
	    	    <%ElseIf _URLParameterList = "SA2S" Then%>
	    	AS.FACTORY.openAccountSelectorDialog("sa");

	    	//go directly to specific dialog when there is only one type
                <%ElseIf _URLParameterList = "1S" Then%>
	    	SAS.FACTORY.openSelectorDialog("1s");
			    <%ElseIf _URLParameterList = "2S" Then%>
	    	SAS.FACTORY.openSelectorDialog("2s");
			    <%ElseIf _URLParameterList = "1B" Then%>
	    	BAS.FACTORY.openSelectorDialog("1b");
			    <%ElseIf _URLParameterList = "2B" Then%>
	    	BAS.FACTORY.openSelectorDialog("2b");
	    	    <%ElseIf _URLParameterList = "BL" Then%>
	    	BL.FACTORY.openBusinessDialog();
	    	<% End If%>     
        });

	    $(document).on('pagecontainerbeforeshow', function () {
	    	<%If _ContentDataTheme <> "c" Then %>
	    	$('.ui-content').css('border', 'hidden');
			<%End If%>
	    	var currentPage = '#' + $.mobile.pageContainer.pagecontainer('getActivePage').attr('id');
	    	//remove default theme and add current theme for current page
	    	if ($(currentPage).hasClass('ui-page-theme-a')) {
	    		$(currentPage).removeClass('ui-page-theme-a');
	    	}
	    	if ($(".ui-content", currentPage).hasClass('ui-body-a')) {
	    		$(".ui-content", currentPage).removeClass('ui-body-a');
	    	}
	    });
		$(document).on('pagebeforeshow', function () {
			var pageId = $.mobile.pageContainer.pagecontainer('getActivePage').attr('id');
			applyFooterThemeCss('#' + pageId, '<%=_ButtonFontTheme%>');
		});
	    function rgbToHex(colorval) {
	    	var parts = colorval.match(/^rgb\((\d+),\s*(\d+),\s*(\d+)\)$/);
	    	if (parts == null || parts.length == 0) return "";

	    	delete (parts[0]);
	    	for (var i = 1; i <= 3; ++i) {
	    		parts[i] = parseInt(parts[i]).toString(16);
	    		if (parts[i].length == 1) parts[i] = '0' + parts[i];
	    	}

	    	return '#' + parts.join('');
	    }
	    function closePopup(popup) {
	    	if (typeof popup == "undefined") {
	    		popup = $(this).closest("div[data-role='popup']");
	    	}
	    	$(popup).popup("close");
	    }
	    function ColorLuminance(hex, lum) {

	    	// validate hex string
	    	hex = String(hex).replace(/[^0-9a-f]/gi, '');
	    	if (hex.length < 6) {
	    		hex = hex[0] + hex[0] + hex[1] + hex[1] + hex[2] + hex[2];
	    	}
	    	lum = lum || 0;

	    	// convert to decimal and change luminosity
	    	var rgb = "#", c, i;
	    	for (i = 0; i < 3; i++) {
	    		c = parseInt(hex.substr(i * 2, 2), 16);
	    		c = Math.round(Math.min(Math.max(0, c + (c * lum)), 255)).toString(16);
	    		rgb += ("00" + c).substr(c.length);
	    	}

	    	return rgb;
	    }
	    var isMobile = {
	    	Android: function () {
	    		return navigator.userAgent.match(/Android/i);
	    	},
	    	BlackBerry: function () {
	    		return navigator.userAgent.match(/BlackBerry/i);
	    	},
	    	iOS: function () {
	    		return navigator.userAgent.match(/iPhone|iPad|iPod/i);
	    	},
	    	iPad: function () {
	    		return navigator.userAgent.match(/iPad/i);
	    	},
	    	Opera: function () {
	    		return navigator.userAgent.match(/Opera Mini/i);
	    	},
	    	Windows: function () {
	    		return navigator.userAgent.match(/IEMobile/i);
	    	},
	    	any: function () {
	    		return (isMobile.Android() || isMobile.BlackBerry() || isMobile.iOS() || isMobile.Opera() || isMobile.Windows());
	    	}

	    };
	    function getContrastYIQ(bgColor) {

	    	var r = parseInt(bgColor.substr(0, 2), 16);
	    	var g = parseInt(bgColor.substr(2, 2), 16);
	    	var b = parseInt(bgColor.substr(4, 2), 16);
	    	var yiq = ((r * 299) + (g * 587) + (b * 114)) / 1000;
	    	//yiq max =255, min =0
	    	//179 = 70% of max, 128 =50% of max
	    	return (yiq >= 179) ? 'black' : 'white';
	    }
	    //function handledIconHeight() {
	    //    var windowHeight = $(window).height();
	    //    var windowWidth = $(window).width();
	    //    var containerWidth = $('#apply_container').width();
	    //    var headerHeight = $('#apply_header').height();
	    //    var contentHeight = $('#icon_container').height();
	    //    var contentWidth = $('#icon_container').width();
	        
	    //    if ($('.icon-button').length > 0) {
	    //        // Portrait mode
	    //        if (windowHeight > windowWidth) {
	    //            var displayHeight = windowHeight - headerHeight - 50;

	    //            var ratio = contentWidth / contentHeight;
	    //            var newWidth = ratio * displayHeight;

	    //            if (newWidth < containerWidth - 20) {
	    //                $('#icon_container').width(newWidth);
	    //            } else {
	    //                $('#icon_container').width(containerWidth - 20);
	    //            }
	    //        }
	    //        // Landscape mode
	    //        else {
	    //            if (containerWidth - 20 > 350) {
	    //                $('#icon_container').width(350);
	    //            } else {
	    //                $('#icon_container').width(containerWidth - 20);
	    //            }
	    //        }
	    //    }
            
        //    // Fix icon height
	    //    FixIconHeightByScreenWidth();
	        
        //    //show/hide footer
	    //    ShowHideFooter();
	    //}
	    
	    //function ShowHideFooter() {
	    //    var windowHeight = $(window).height();
	    //    var headerHeight = $('#apply_header').height();
	    //    var contentHeight = $('#icon_container').height();
	    //    var footerHeight = $('#footer').height();

	    //    if (windowHeight > (contentHeight + headerHeight + footerHeight + 50)) {
	    //        $('#footer').show();
	    //    } else {
	    //        $('#footer').hide();
	    //    }
	    //}
	    
	    //function FixIconHeightByScreenWidth() {
	    //    var firstIcon = $('.icon-button').first();
	    //    if (firstIcon.length > 0) {
	    //        $('.icon-button').each(function () {
	    //            $(this).height(firstIcon.width()); // Fix height for each icon equals
	    //        });
	    //    }
	    //}

	    function changeBackgroundColor(bgColor, alpha) {
	       var strAlpha = ',' + alpha + ')';
	       return bgColor.replace('rgb', 'rgba').replace(')', strAlpha);
	    }
	    function handledPageHeight() {
	        var screenHeight = $(window).height(); 
	        var pageHeight = $('#mainPage').height();
	        var containerHeight = $('#mainPage .lpq_container').height();
	        var cPaddingTop = $('#mainPage .lpq_container').css('padding-top');
	        var cPaddingBottom = $('#mainPage .lpq_container').css('padding-bottom');
	        if (cPaddingTop != undefined) {
	            cPaddingTop = cPaddingTop.replace("px", "");
	        } else {
	            cPaddingTop = 0;
	        }

	        if (cPaddingBottom != undefined) {
	            cPaddingBottom = cPaddingBottom.replace("px", "");
	        } else {
	            cPaddingBottom = 0;
	        }
	        if (pageHeight < screenHeight) {
	            var footerGap = screenHeight - pageHeight;
	            $('#divFooterNote').css('padding-top', footerGap + 'px');
	            $('#mainPage').height(screenHeight);
	            $('#mainPage .lpq_container').height(screenHeight - cPaddingTop - cPaddingBottom);
	        } 
	    }
	   
	 
	    function applyHeaderThemeCss() {
	    	var headerThemeColor = "#565e96";
	    	var headerThemeFont = "'Roboto-light', sans-serif";
	    	var headerTheme2Color = "#52bc71";
	    	var headerTheme2Font = "'Roboto-light', sans-serif";
	    	var buttonBgColor = "#52bc71";
	    	// header_theme
	    	//if ($('div.divHeaderTheme').css('backgroundColor') != undefined) {
	    	//	headerThemeColor = rgbToHex($('div.divHeaderTheme').css('backgroundColor'));
	    	//	if (headerThemeColor == "") {
	    	//		headerThemeColor = "#565e96";
	    	//	}
	    	//}
	    	var headerThemeController = $("div[data-theme-role='header']", "#divThemeController");
	    	if (headerThemeController.css('backgroundColor') != undefined) {
	    		headerThemeColor = rgbToHex(headerThemeController.css('backgroundColor'));
	    		if (headerThemeColor == "") {
	    			headerThemeColor = "#565e96";
	    		}
	    		//headerThemeFont = $(pageId + ' .divHeaderTheme').css('font-family');
	    	}
	    	var headerThemeColorStart = ColorLuminance(headerThemeColor, 0.3);
	    	var headerThemeColorEnd = ColorLuminance(headerThemeColor, -0.3);

	    	// header_theme2
	    	//if ($('div.divHeaderTheme2').css('backgroundColor') != undefined) {
	    	//	headerTheme2Color = rgbToHex($('div.divHeaderTheme2').css('backgroundColor'));
	    	//	if (headerTheme2Color == "") {
	    	//		headerTheme2Color = "#52bc71";
	    	//	}
	    	//}
	    	var header2ThemeController = $("div[data-theme-role='header2']", "#divThemeController");
	    	if (header2ThemeController.css('backgroundColor') != undefined) {
	    		headerTheme2Color = rgbToHex(header2ThemeController.css('backgroundColor'));
	    		if (headerTheme2Color == "") {
	    			headerTheme2Color = "#565e96";
	    		}
	    	}

	    	var contentBgColor = "#ffffff";
	    	var contentThemeController = $("div[data-theme-role='content']", "#divThemeController");
	    	if (contentThemeController.css("background-color") != undefined) {
	    		contentBgColor = rgbToHex(contentThemeController.css("background-color"));
	    	}

	    	var buttonThemeController = $("div[data-theme-role='button']", "#divThemeController");
	    	if (buttonThemeController.css("background-color") != undefined) {
	    		buttonBgColor = rgbToHex(buttonThemeController.css("background-color"));
	    	}

	    	var buttonBgColorStart = ColorLuminance(buttonBgColor, 0.3);
	    	var buttonBgColorEnd = ColorLuminance(buttonBgColor, -0.3);

	    	$('.header_theme svg').css({ fill: headerThemeColor });
	    	$('.header-theme-stroke').css({ stroke: headerThemeColor });
	    	$('.header_theme2 svg').css({ fill: headerTheme2Color });
	    	$('.header_theme .steps svg').css({ fill: '#E9EBEB', stroke: '#E5E4E2;' });
	    	$('.header_theme svg .shape').css({ stroke: headerTheme2Color });
	    	$('.header_theme svg circle.active').css({ fill: headerTheme2Color });
	    	$('.header_theme svg path.active').css({ fill: headerTheme2Color });
	    	$('.header_theme2 svg .typo.inactive').css({ fill: '#ffffff' });
	    	$('.close-button').css({ fill: headerTheme2Color });
	    	$('.heading-title').css({ color: headerThemeColor });
	    	$('.header_theme2').css({ color: headerTheme2Color });
	    	$('.header_theme').css({ color: headerThemeColor, "border-color": headerThemeColor });
	    	if ($("#headerThemeCssPlaceHolder").length == 0) {
	    		var style = document.createElement('style');
	    		style.setAttribute("id", "headerThemeCssPlaceHolder");
	    		var cssClasses = ".btn-header-theme{border:1px " + headerTheme2Color + " solid !important; color: " + headerTheme2Color + " !important;text-align:center;padding-left:0px;background-color: #fff !important; text-shadow: none !important;}";
	    		cssClasses += ".btn-header-theme-ext {border:1px " + headerTheme2Color + " solid !important; color: " + headerTheme2Color + " !importbtn-header-themeant;text-align:left;padding-left:10px;background:none!important;background-color: " + contentBgColor + "!important; text-shadow: none !important;}";
	    		cssClasses += ".btn-header-theme:focus, .btn-header-theme.ui-link:focus, .btn-header-theme-ext:focus, .btn-header-theme-ext.ui-link:focus, a.ui-link.header_theme2:focus, .ui-focus,a.ui-link.img-btn:focus, input[type='image']:focus, select.ddlUploadDoc-style:focus,.header-theme-checkbox:focus,.selected-flag:focus, .focus-able:focus{-webkit-box-shadow: 0 0 12px " + headerTheme2Color + "!important;-moz-box-shadow: 0 0 12px " + headerTheme2Color + "!important;box-shadow: 0 0 12px " + headerTheme2Color + "!important;outline:none;}";
	    		cssClasses += ".account-editor-dialog .div-continue .div-continue-button.cancel-btn:focus{-webkit-box-shadow: 0 0 12px " + buttonBgColor + "!important;-moz-box-shadow: 0 0 12px " + buttonBgColor + "!important;box-shadow: 0 0 12px " + buttonBgColor + "!important;outline:none;}";
	    		cssClasses += ".btn-header-theme-ext .btn-add{ width:27px; height: 27px; position:absolute; top:50%; right:0%; transform: translate(-50%, -50%); text-align: center; font-size: 1.5em;}";
	    		cssClasses += ".btn-header-theme-ext .btn-add>svg{ width:27px; height: 27px; transform: rotate(45deg);}";
	    		cssClasses += ".btn-header-theme.active {border:1px " + headerTheme2Color + " solid !important; color: #ffffff !important;background-image:none!important;background-color: " + headerTheme2Color + "!important; text-shadow: none !important;}";
	    		cssClasses += ".header_theme2 svg {fill: " + headerTheme2Color + ";}";
	    		cssClasses += "a.ui-btn[type='button']{background:" + buttonBgColor + "!important;border-color:" + buttonBgColor + "!important;color:<%=_ButtonFontTheme%>; text-shadow: none !important;}";
	    		cssClasses += "a.ui-btn[type='button']:hover{border: 1px solid " + buttonBgColor + "!important;background-image: -webkit-gradient(linear, left top, left bottom, from( " + buttonBgColorStart + "), to( " + buttonBgColorEnd + "))!important;background-image: -webkit-linear-gradient( " + buttonBgColorStart + ", " + buttonBgColorEnd + ")!important;background-image:-moz-linear-gradient( " + buttonBgColorStart + ", " + buttonBgColorEnd + ")!important; background-image:-ms-linear-gradient( " + buttonBgColorStart + ", " + buttonBgColorEnd + ")!important;background-image:-o-linear-gradient( " + buttonBgColorStart + ", " + buttonBgColorEnd + ")!important;background-image:linear-gradient( " + buttonBgColorStart + ", " + buttonBgColorEnd + ")!important;}";
	    		cssClasses += "a.ui-btn[type='button']:focus{-webkit-box-shadow: 0 0 6px 5px " + buttonBgColorEnd + "!important;-moz-box-shadow: 0 0 6px 5px " + buttonBgColorEnd + "!important;box-shadow: 0 0 6px 5px " + buttonBgColorStart + "!important;}";
	    		cssClasses += ".account-role-item-wrapper {border:1px solid " + headerTheme2Color + ";border-radius: 4px;padding: 10px 10px;position: relative;margin-bottom: 6px;}";
	    		cssClasses += ".account-role-item, .role-attr-item {color: " + headerTheme2Color + " !important;text-align:left;padding-left:10px;background:none!important;background-color: " + contentBgColor + "!important; text-shadow: none !important; cursor:default;}";
	    		cssClasses += ".account-role-item-wrapper .role-attr-item label.custom-checkbox>span {border: 1px solid " + headerTheme2Color + ";}";
	    		cssClasses += ".account-role-item-wrapper .role-attr-item label.custom-checkbox input:focus+span {outline:none;-webkit-box-shadow: 0 0 12px " + headerTheme2Color + "!important;-moz-box-shadow: 0 0 12px " + headerTheme2Color + "!important;box-shadow: 0 0 12px " + headerTheme2Color + "!important;outline:none;}";
	    		cssClasses += ".account-role-item-wrapper .role-attr-item label.custom-checkbox > input[type='checkbox']:checked + span:after{background: " + headerTheme2Color + ";background-image: url('data:image/svg+xml;charset=US-ASCII,%3C%3Fxml%20version%3D%221.0%22%20encoding%3D%22iso-8859-1%22%3F%3E%3C!DOCTYPE%20svg%20PUBLIC%20%22-%2F%2FW3C%2F%2FDTD%20SVG%201.1%2F%2FEN%22%20%22http%3A%2F%2Fwww.w3.org%2FGraphics%2FSVG%2F1.1%2FDTD%2Fsvg11.dtd%22%3E%3Csvg%20version%3D%221.1%22%20id%3D%22Layer_1%22%20xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%20xmlns%3Axlink%3D%22http%3A%2F%2Fwww.w3.org%2F1999%2Fxlink%22%20x%3D%220px%22%20y%3D%220px%22%20%20width%3D%2214px%22%20height%3D%2214px%22%20viewBox%3D%220%200%2014%2014%22%20style%3D%22enable-background%3Anew%200%200%2014%2014%3B%22%20xml%3Aspace%3D%22preserve%22%3E%3Cpolygon%20style%3D%22fill%3A%23FFFFFF%3B%22%20points%3D%2214%2C4%2011%2C1%205.003%2C6.997%203%2C5%200%2C8%204.966%2C13%204.983%2C12.982%205%2C13%20%22%2F%3E%3C%2Fsvg%3E');background-position: center center;background-repeat: no-repeat;}";

	    		cssClasses += ".account-role-item-wrapper div.btn-remove{width:27px; height: 27px; position: absolute;top: 50%;right: 14px;transform: translate(0, -50%);background-image: url(\"data:image/svg+xml,%3csvg xmlns='http://www.w3.org/2000/svg' viewBox='-3 -4 55 55'%3e%3cpath d='M41.167,7.228C31.904-2.253,16.71-2.431,7.229,6.832S-2.43,31.289,6.833,40.771   c9.263,9.48,24.458,9.658,33.939,0.395C50.253,31.904,50.43,16.709,41.167,7.228z M37.888,38.174   c-7.885,7.703-20.521,7.555-28.224-0.33C1.96,29.959,2.108,17.323,9.993,9.62c7.885-7.703,20.522-7.555,28.224,0.33   C45.921,17.834,45.772,30.471,37.888,38.174z' fill='" + headerTheme2Color.replace("#", "%23") + "'/%3e%3cpath d='M31.71,17.537l-1.103-1.129c-0.825-0.845-1.729-0.101-1.951,0.108l-4.619,4.513l-4.59-4.709   c-0.336-0.307-1.104-0.815-1.9-0.04l-1.13,1.102c-0.845,0.824-0.103,1.729,0.106,1.952l4.511,4.629l-4.726,4.617   c-0.873,0.852-0.289,1.598-0.104,1.793l1.264,1.295c0.177,0.172,0.9,0.768,1.773-0.086l4.722-4.613l4.608,4.729 c0.851,0.873,1.597,0.291,1.794,0.105l1.295-1.262c0.174-0.176,0.769-0.9-0.083-1.773l-4.611-4.731l4.707-4.599 C31.979,19.101,32.486,18.332,31.71,17.537z' fill='" + headerTheme2Color.replace("#", "%23") + "'/%3e%3c/svg%3e\")!important;border-color:" + headerTheme2Color + "!important;}";
	    		cssClasses += ".account-role-item-wrapper div.btn-edit {width:16px; height: 24px; position: absolute;top: 50%;right: 50px;transform: translate(0, -50%);background-image: url(\"data:image/svg+xml,%3csvg xmlns='http://www.w3.org/2000/svg' viewBox='0 0 64.8 96.8'%3e%3cpath d='M29.6,12.3l4.3-7.5c2.6-4.5,8.4-6.1,12.9-3.5L60,8.8c4.5,2.6,6.1,8.4,3.5,12.9l-4.3,7.5 c-0.5,0.9-1.4,1.3-2,1L29.7,14.5C29.1,14.1,29.1,13.2,29.6,12.3z M55.1,36.5L29.6,80.9l0,0l-28.8,16L0,63.9l0,0l25.6-44.4 c0.5-0.9,1.4-1.3,2-1L55,34.3C55.6,34.6,55.7,35.6,55.1,36.5z M22.7,68.6l2.1,1.2l21-36.7l-2.1-1.2L22.7,68.6z M12,62.5l2.1,1.2 l21-36.7L33,25.8L12,62.5z M25.6,79.5c0.3-0.2,0.6-0.5,0.8-0.9c0.9-1.6-0.1-3.8-2.2-5c-2-1.1-4.2-1-5.2,0.3 c0.6-1.5-0.4-3.6-2.4-4.7c-2-1.1-4.2-1-5.3,0.3C11.9,68,11,66,9,64.8c-2.1-1.2-4.6-0.9-5.5,0.6c-0.2,0.4-0.3,0.7-0.3,1.2l0,0 l0.3,16.7l7.5,4.3L25.6,79.5L25.6,79.5z' fill='" + headerTheme2Color.replace("#", "%23") + "'/%3e%3c/svg%3e\")!important;border-color:" + headerTheme2Color + "!important;}";
	    		cssClasses += ".account-role-item-wrapper div.btn-remove:hover, .account-role-item-wrapper div.btn-edit:hover{cursor:pointer;}";
	    		cssClasses += ".account-role-item-wrapper div.btn-remove:focus, .account-role-item-wrapper div.btn-edit:focus, .btn-header-theme-ext .btn-add:focus{outline:none;-webkit-box-shadow: 0 0 12px " + headerTheme2Color + "!important;-moz-box-shadow: 0 0 12px " + headerTheme2Color + "!important;box-shadow: 0 0 12px " + headerTheme2Color + "!important;outline:none;}";
	    		cssClasses += ".account-role-item-wrapper .role-attr-item label.custom-checkbox + label{line-height: 25px;display: inline-block; padding-top: 3px; margin: 0; color: " + headerTheme2Color + ";}";
	    		cssClasses += "div[data-role='header']{border: 1px solid " + headerThemeColorEnd + "!important;background-image: -webkit-gradient(linear, left top, left bottom, from( " + headerThemeColorStart + "), to( " + headerThemeColorEnd + "))!important;background-image: -webkit-linear-gradient( " + headerThemeColorStart + ", " + headerThemeColorEnd + ")!important;background-image:-moz-linear-gradient( " + headerThemeColorStart + ", " + headerThemeColorEnd + ")!important; background-image:-ms-linear-gradient( " + headerThemeColorStart + ", " + headerThemeColorEnd + ")!important;background-image:-o-linear-gradient( " + headerThemeColorStart + ", " + headerThemeColorEnd + ")!important;background-image:linear-gradient( " + headerThemeColorStart + ", " + headerThemeColorEnd + ")!important;}";
	    		cssClasses += "div[data-role='page'],div[data-role='popup'],.ui-content{background:none!important;background-color:" + contentBgColor + "!important;}";
	    		cssClasses += ".ui-icon-carat-d::after{background-image:url(\"data:image/svg+xml,%3csvg xmlns='http://www.w3.org/2000/svg' viewBox='0 0 100 100'%3e%3cpath d='M10,10 H90 L50,70' fill='" + headerTheme2Color.replace("#", "%23") + "'/%3e%3c/svg%3e\")!important}";
	    		if (!isMobile.any()) {
	    			//cssClasses += ".btn-header-theme:hover {border:2px " + headerTheme2Color + " solid !important; color: #ffffff !important ;background-image:none !important;background-color: " + headerTheme2Color + " !important; text-shadow: none !important;-ms-opacity: 0.6; opacity: 0.6;}";
	    			cssClasses += ".btn-header-theme.btnHover {border:1px " + headerTheme2Color + " solid !important; color: #fff !important;background-image:none !important;padding-left:10px !important;background-color: " + headerTheme2Color + " !important; text-shadow: none !important;-ms-opacity: 0.8; opacity: 0.8;}";
	    			cssClasses += ".btn-header-theme.btnActive_on {border:1px " + headerTheme2Color + " solid !important; color: #ffffff !important;background-image:none !important ;padding-left:10px !important;background-color: " + headerTheme2Color + " !important; text-shadow: none !important;}";
	    			cssClasses += ".btn-header-theme.btnActive_off {border:1px " + headerTheme2Color + " solid !important; color: " + headerTheme2Color + " !important;padding-left:10px !important;background-color: " + contentBgColor + "!important; text-shadow: none !important ;}";
	    		}
	    		style.type = 'text/css';
	    		style.innerHTML = cssClasses;
	    		document.getElementsByTagName('head')[0].appendChild(style);
	    	}
	    }
		function applyFooterThemeCss(pageId, buttonFontColor) {
			//read 
			var buttonBgColor = "#52bc71";
			var footerBgColor = "#52bc71";
			var footerTextColor = "white";
			var temp;
			//if ($(pageId + " a.div-continue-button").css("background-color") != undefined) {
			//    buttonBgColor = rgbToHex($(pageId + " a.div-continue-button").css("background-color"));
			//}


			var footerThemeController = $("div[data-theme-role='footer']", "#divThemeController");
			if (footerThemeController.css("background-color") != undefined) {
				temp = rgbToHex(footerThemeController.css("background-color"));
				if (temp !== "") {
					footerBgColor = temp;
				}
				//Set default black or white color text
				footerTextColor = getContrastYIQ(footerBgColor.replace("#", ""));
			}
			var footerFontThemeController = $("div[data-theme-role='footerFont']", "#divThemeController");
			if (footerFontThemeController.css("color") != undefined) {
				temp = rgbToHex(footerThemeController.css("color"));
				if (temp !== "") {
					//Set default black or white color text
					footerTextColor = temp;
				}
			}
			var footerBgColorStart = ColorLuminance(footerBgColor, 0.1);
			var footerBgColorEnd = ColorLuminance(footerBgColor, -0.1);
			if (footerBgColor == "#ffffff") {
				footerBgColorStart = "#ffffff";
				footerBgColorEnd = "#ffffff";
			}

			var buttonThemeController = $("div[data-theme-role='button']", "#divThemeController");
			if (buttonThemeController.css("background-color") != undefined) {
				temp = rgbToHex(buttonThemeController.css("background-color"));
				if (temp !== "") {
					buttonBgColor = temp;
				}
			}


			var buttonBgColorStart = ColorLuminance(buttonBgColor, 0.3);
			var buttonBgColorEnd = ColorLuminance(buttonBgColor, -0.3);
			if (buttonBgColor == "#ffffff") {
				buttonBgColor = "#52bc71";
				buttonBgColorStart = "#86CF8A";
				buttonBgColorEnd = "#86CF8A";
			}
			//apply



			$(pageId + " a.div-continue-button").css("border-color", buttonBgColor);
			$(pageId + " a.div-goback")
				.css('background-color', 'transparent')
				.css("border-color", buttonBgColor)
				.css("color", buttonBgColor);
			$(pageId + " a.div-finish-later")
				.css('background-color', 'transparent')
				.css("border-color", buttonBgColor)
				.css("color", buttonBgColor);

			$(pageId + " a.div-button")
				.css('background-color', 'transparent')
				.css("border-color", buttonBgColor)
				.css("color", buttonBgColor);

			$(pageId + " a.div-button").hover(function () {
				$(this).css('background-color', buttonBgColor).css('color', buttonFontColor);
			});

			$(pageId + " a.div-button").mouseleave(function () {
				$(this).css('background-color', 'transparent').css('color', buttonBgColor);
			});

			$(pageId + " a.div-goback:not(.abort-hover)").hover(function () {
				$(this).css('background-color', buttonBgColor).css('color', buttonFontColor);
			});
			$(pageId + " a.div-finish-later:not(.abort-hover)").hover(function () {
				$(this).css('background-color', buttonBgColor).css('color', buttonFontColor);
			});

			$(pageId + " a.div-goback:not(.abort-hover)").mouseleave(function () {
				$(this).css('background-color', 'transparent').css('color', buttonBgColor);
			});
			$(pageId + " a.div-finish-later:not(.abort-hover)").mouseleave(function () {
				$(this).css('background-color', 'transparent').css('color', buttonBgColor);
			});

			if ($("#footerThemeCssPlaceHolder").length == 0) {
				var style = document.createElement('style');
				style.setAttribute("id", "footerThemeCssPlaceHolder");
				var cssClasses = ".footer-theme.btn {border-color:" + buttonBgColor + "!important;background-color: " + buttonBgColor + " !important;}";
				cssClasses += "a.ui-btn[type='button']{background:" + buttonBgColor + "!important;border-color:" + buttonBgColor + "!important;color:" + buttonFontColor + ";}";
				cssClasses += "a.ui-btn[type='button']:hover{border: 1px solid " + buttonBgColor + "!important;background-image: -webkit-gradient(linear, left top, left bottom, from( " + buttonBgColorStart + "), to( " + buttonBgColorEnd + "))!important;background-image: -webkit-linear-gradient( " + buttonBgColorStart + ", " + buttonBgColorEnd + ")!important;background-image:-moz-linear-gradient( " + buttonBgColorStart + ", " + buttonBgColorEnd + ")!important; background-image:-ms-linear-gradient( " + buttonBgColorStart + ", " + buttonBgColorEnd + ")!important;background-image:-o-linear-gradient( " + buttonBgColorStart + ", " + buttonBgColorEnd + ")!important;background-image:linear-gradient( " + buttonBgColorStart + ", " + buttonBgColorEnd + ")!important;}";
				cssClasses += "a.ui-btn[type='button']:focus{-webkit-box-shadow: 0 0 8px 4px " + buttonBgColorEnd + "!important;-moz-box-shadow: 0 0 8px 4px " + buttonBgColorEnd + "!important;box-shadow: 0 0 8px 4px " + buttonBgColorEnd + "!important;}";
				cssClasses += ".divfooter{background-image: -webkit-gradient(linear, left top, left bottom, from( " + footerBgColorStart + "), to( " + footerBgColorEnd + "))!important;background-image: -webkit-linear-gradient( " + footerBgColorStart + ", " + footerBgColorEnd + ")!important;background-image:-moz-linear-gradient( " + footerBgColorStart + ", " + footerBgColorEnd + ")!important; background-image:-ms-linear-gradient( " + footerBgColorStart + ", " + footerBgColorEnd + ")!important;background-image:-o-linear-gradient( " + footerBgColorStart + ", " + footerBgColorEnd + ")!important;background-image:linear-gradient( " + footerBgColorStart + ", " + footerBgColorEnd + ")!important;}";
				cssClasses += ".ui-checkbox-on:after{background-color:" + buttonBgColor + ";}";
				//cssClasses += ".ui-radio .ui-radio-on:after{background-color:#fff;border-color:" + footerBgColor + "!important;}";
				cssClasses += ".ui-radio.ui-mini .ui-radio-on{background-color:" + footerBgColor + "!important;color:#ffffff;border:1px solid " + footerBgColorEnd + "!important;}";
				cssClasses += "a.div-goback:focus,a.div-finish-later:focus,a.div-button:focus,.footer-theme.btn:focus{-webkit-box-shadow: 0 0 12px " + buttonBgColor + "!important;-moz-box-shadow: 0 0 12px " + buttonBgColor + "!important;box-shadow: 0 0 12px " + buttonBgColor + "!important;outline:none;display:inline-block;}";
				cssClasses += "a.div-goback.abort-hover:focus,a.div-finish-later.abort-hover:focus{-webkit-box-shadow: none!important;-moz-box-shadow: none!important;box-shadow: none!important;outline:none;display:inline;}";
				//if (footerBgColor === "#ffffff") {
				//cssClasses += ".divfooter .divfooter_top, .divfooter .divLenderName, .divfooter a, .divfooter span{color:gray!important; font-weight:bold;}";
				//}

				cssClasses += ".divfooter .divfooter_top, .divfooter .divLenderName, .divfooter a, .divfooter span{color:" + footerTextColor + "!important; font-weight:normal;}";

				style.type = 'text/css';
				style.innerHTML = cssClasses;
				style.id = "styleFooterTheme";
				if (document.getElementById("styleFooterTheme") != null) {
					document.getElementById("styleFooterTheme").parentNode.removeChild(document.getElementById("styleFooterTheme"));
				}
				document.getElementsByTagName('head')[0].appendChild(style);
			}
		}
		
	</script>
	<%If Request.QueryString("mode") = "777" Then%>
	<link rel="Stylesheet" href="/Sm/content/font-awesome/css/font-awesome.css" />
	<link rel="stylesheet" type="text/css" href="/css/adminStyle.css"/>
	<%End If%>
</head>
<body>
	<uc1:Piwik id="ucPiwik" runat="server" ></uc1:Piwik>

	<div data-role="page" id="mainPage">
		<div data-role="header" class="sr-only"></div>
		<input type ="hidden" id="hdLenderBaseUrl" value="<%=_LenderBaseUrl%>" />
	   <%--<input type ="hidden" id="hdFooterTheme" value="<%=_bgTheme%>" />
		<input type ="hidden" id="hdBgTheme" value ="<%=_BackgroundTheme%>" />--%>
		<input type="hidden" id="hdInputList" value ="<%=_URLParameterList%>" />
		<input type ="hidden" id="hdHasList" value="<%=_hasURLParameterList%>" />
		<input type="hidden" id="hdCurrentList" value="<%=_AvailableModules%>" />
		<div id="apply_container" class="lpq_container">
			<div id="apply_header">
				<%=_LogoUrl%>
				<%If _hasLabelOption Then%>
				<div class="row">
					<div class="col-xs-12">
						<div class="header_theme" style="margin: 20px 0px; font-size: 30px; display: table; margin-left: auto; margin-right: auto;">
							<%=HeaderUtils.RenderIcon(17)%>
							<div style="display: table-cell; vertical-align: middle;" class="rename-able"><%=_loanHeader %></div>
						</div>
					</div>
				</div>
					<%--<h2><%=_loanHeader%></h2>--%>
				<%Else%>
					   <%-- <br /><br />--%>
				<%End If%>
			</div>
			<div id="icon_container" class="<%=_iconContainerClass %>">
				<%=_strHtmlLoans%>
				<div style="clear: both;"></div>
				<div id="divFooterNote">
					<p class="header_theme rename-able" style="text-align: center; font-size: 30px; width: 90%; margin: 0 auto; border-bottom: 2px solid #ccc;padding-bottom: 10px;" >Please Note:</p>
					<p class="header_theme" style="width: 90%; margin: 0 auto;text-align: justify;padding-top: 6px;">
						<%If readFooterNoteConfig() <> "" Then%>
							<%=readFooterNoteConfig()%>
						<%Else%>
							In an effort to keep your personal information confidential, we will not be storing any information entered into any of the above applications after the application has been inactive for more than 10 minutes. Because you will have to start the application from the beginning after the 10 minutes of inactivity, please plan accordingly when you are completing an application.
						<%End If%>
					</p>							    
				</div>
			</div>
			<div id="footer" style="display: none">
				<%--<div  data-role="footer" data-theme="<%=_bgTheme%>" class="ui-bar-<%=_bgTheme %>" ></div>--%>
			</div>
		</div>  
	</div>
    
    
    <uc:BusinessLoanSelector ID="ucBusinessLoanSelector" MappedShowFieldLoanType ="BL" runat="server" />
	<uc:AccountSelector ID="ucAccountSelector" runat="server" visible="false"/>
	<uc:SpecialAccountSelector ID="ucSpecialAccountSelector" runat="server"  visible="false"/>
	<uc:BusinessAccountSelector ID="ucBusinessAccountSelector" runat="server" visible="false" />
    <asp:PlaceHolder runat="server" ID="plhApplicationDeclined"></asp:PlaceHolder>
	<uc:pageFooter ID="ucPageFooter" runat="server" />  
	
</body>
</html>
