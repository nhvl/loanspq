$(function () {
    handledContentHeight();
    $('#saveBtnEdit').click(function () {
        if (validateContentStyle() != "") {
            alert(validateContentStyle());
        }
    });
});

function handledContentHeight() {
    var scHeight = $(window).height();
    var contentHeight = $('.content-left').height();
    if (contentHeight > scHeight) {
        $('.content-left').css('height', contentHeight + 'px');
        $('.content-right').css('height', contentHeight + 'px');
    } else {
        $('.content-left').css('height', scHeight + 'px');
        $('.content-right').css('height', scHeight + 'px');
    }

}

