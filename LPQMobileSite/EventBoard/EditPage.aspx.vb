﻿Imports System.IO
Partial Class EditPage
    Inherits System.Web.UI.Page
	Protected mainPageURL As String

	Private Shared _log As log4net.ILog = log4net.LogManager.GetLogger(GetType(EditPage))

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim baseUrl As String = Request.Url.GetLeftPart(UriPartial.Authority)
		mainPageURL = baseUrl + "/EventBoard/MainPage.aspx"
		_log.Info("mainPageURL: " & mainPageURL)
    End Sub
    Protected Sub btnSave_Click(sender As Object, e As EventArgs) Handles btnSave.Click
        writeTofile()
    End Sub
    Protected Sub writeTofile()
		Dim path As String = HttpContext.Current.Server.MapPath("../PdfViewer/docTextFile.txt")
        Dim sfile As StreamWriter = New StreamWriter(path)
        Dim strTextfile As String = Server.HtmlDecode(createText.Value)
        If Not String.IsNullOrEmpty(strTextfile) Then
            sfile.Write(strTextfile)
        End If
        sfile.Close()
    End Sub
End Class
