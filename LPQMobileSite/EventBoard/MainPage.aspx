﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="MainPage.aspx.vb" Inherits="MainPage" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>mainPage</title>
    <link href="css/bootstrap.css" rel="stylesheet" />
    <link href="css/default.css" rel="stylesheet" />
    <script src="js/jquery-1.12.4.min.js"></script>
    <script src="js/bootstrap.js"></script>
    <script src="js/script.js"></script>
    
</head>
<body onload ="handledContentHeight()">
	
    <div class ="content-wrap">    
        <div class="content-left">
			<div class="content-logo"><img alt="" src="https://app.loanspq.com/logo/mltest.gif" /></div>
			<div class="content-left-doc"><%=_strTextArea%></div>
        </div>
        <div class="content-right">
			use one of 2 method to load video
			 
			<%--<iframe width="100%" height="100%" src="https://www.youtube.com/embed/HXqRStwEzpM" frameborder="0" allowfullscreen></iframe>--%>

			<%--MP4 and ogg type are supported by most browser or else browser need to have the extension installled--%>
			<video width="100%" height="100%" autoplay="autoplay" controls="controls" loop="loop">
				<source src="mov_bbb.mp4" type="video/mp4" />
				Your browser does not support the video tag.
			</video>

		</div>
    </div>
</body>
</html>
