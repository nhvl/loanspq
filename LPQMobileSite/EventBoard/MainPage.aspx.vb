﻿Imports System.IO
Partial Class MainPage
    Inherits System.Web.UI.Page
    Protected _strTextArea As String
    Protected _strDropdownMenu As String
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            _strTextArea = getTextFromFile()
            If (String.IsNullOrEmpty(_strTextArea)) Then
                _strTextArea = "<span style='color:red'>The text file is nothing.</span>"
            End If
        End If
    End Sub
    Protected Function getTextFromFile() As String
		Dim path As String = HttpContext.Current.Server.MapPath("../PdfViewer/docTextFile.txt")
        Dim strText As String = ""
        Dim strReadLine As String = ""
        Try
            Dim strReader As StreamReader = New StreamReader(path)
            Do While (strReader.Peek() >= 0)
                strReadLine = strReader.ReadLine()
                Dim strNamespace As String = ""
                If (Not String.IsNullOrEmpty(strReadLine)) Then
                    Dim nsIndex As Integer = strReadLine.Length - LTrim(strReadLine).Length
                    If (nsIndex > 0) Then
                        For i = 0 To nsIndex - 1
                            strNamespace += "&nbsp;"
                        Next
                    End If
                End If
                strText += strNamespace + Trim(Server.HtmlEncode(strReadLine)) + "<br/>"
            Loop
            strReader.Close()
        Catch ex As Exception
            Return ""
        End Try
        Return strText
    End Function
End Class
