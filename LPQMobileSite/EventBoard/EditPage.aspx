﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="EditPage.aspx.vb" Inherits="EditPage" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Edit Page</title>
      <!-- Bootstrap -->
    
    <link href="css/bootstrap.css" rel="stylesheet" />
    <link href="css/default.css" rel="stylesheet" />
    <script src="js/jquery-1.12.4.min.js"></script>
    <script src="js/bootstrap.js"></script>
    <script src="js/script.js"></script>
    <script>
        $(function () {
            $('#viewMainPage').click(function () {
                window.location.href ='<%=mainPageURL%>';
            });
            $('#btnSave').click(function () {
                var txtTextArea = $('#createText').val().trim();
                if (txtTextArea == "") {
                    alert('The text area is nothing. Please enter some text.');
                }
            });
        });
       
    </script>
</head>
<body>   
    <div class ="container">
     <div class="page-header"><div class="content-logo"><img alt="" src="https://app.loanspq.com/logo/mltest.gif" /></div></div>
        <div class="content-textarea">
        <form role="form" runat="server" >
            <div class="form-group"> 
                <label for="createText">Creating Text: </label>
                <textarea class ="form-control" id="createText" rows="30" runat ="server"  ></textarea>
            </div>
        <div>
            <asp:Button type="button" class="btn btn-primary" id="btnSave" runat ="server" Text="Save"/>
            
        <div style="display:none"><br />
           <div style="font-weight :bold">Edit content width of the main page</div>
            Font Size   &nbsp&nbsp&nbsp&nbsp&nbsp Left content width(in %)   <br />
            <asp:textbox  style="width:62px; height: 30px;margin-top:4px 2px auto"  runat="server" onkeypress='return event.charCode >= 48 && event.charCode <= 57' id="fontSize"/> &nbsp&nbsp&nbsp&nbsp
            <asp:textbox  style="width:62px; height: 30px;margin-top:4px 2px 2px 10px"  runat="server" onkeypress='return event.charCode >= 48 && event.charCode <= 57' id="leftContentWidth"/> % &nbsp&nbsp&nbsp&nbsp
            <button class="btn btn-primary" id="saveBtnEdit">Save Edit</button>
         <br /></div> 
            <button type="button" class="btn btn-primary" id="viewMainPage">View MainPage</button>
        </div> 
        </form>    
       </div>
    </div>
</body>
</html>
