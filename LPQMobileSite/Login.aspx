﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="Login.aspx.vb" Inherits="Sm_Login" %>
<%@ Import Namespace="System.Web.Optimization" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
	<!--[if lte IE 8]>
	<p class="browsehappy">
	You are using an <strong>outdated</strong> browser.
	Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
	<script type="text/jscript">
		document.execCommand('Stop');
	</script>
	<![endif]-->

	<noscript>
		For full functionality of this site it is necessary to enable JavaScript.
		Here are the <a href="http://www.enable-javascript.com/" target="_blank">
		instructions how to enable JavaScript in your web browser</a>.
	</noscript>

	<%=getFrameBreakerCode()%>

	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1 user-scalable=no"/>
	<link rel="shortcut icon" href="https://app.loanspq.com/logo/lpq_favicon.ico"  type="image/ico"  />
	<%:Styles.Render("~/css/smcss")%>
	<%:Scripts.Render("~/js/smscript")%>
	
</head>
<body>
    <div class="container login-page">
	    <div class="row top30">
		    <div class="col-10 offset-1 col-lg-6 offset-lg-3 main-form">
				<form runat="server" method="POST" id="frmLogin">
					<h1>Login</h1>
					<div>
						<div class="form-group">
							<label class="required-field">User Name</label>
							<input type="text" class="form-control" value="<%=UserName%>" name="UserName" id="txtUserName" placeholder="Email" maxlength="50" />
						</div>
					</div>
					<div>
						<div class="form-group">
							<label class="required-field">Password</label>
							<input type="password" class="form-control" name="Password" id="txtPassword" placeholder="Password" maxlength="50" autocomplete="new-password"/>
						</div>
					</div>
					<div>
						<div class="form-group forgot-password-link">
							<a href="ForgotPassword.aspx">Forgot Password?</a>
						</div>
					</div>
					<div class="checkbox">
						<input type="checkbox" name="RememberMe" id="chkRememberMe" /> <label for="chkRememberMe">Remember me</label>
					</div>
					<asp:Label runat="server" CssClass="result-message" ID="lblResultMessage" Visible="False"></asp:Label>
					<div class="text-right">
						<button type="button" id="btnLogin" class="btn btn-primary">Login</button>
					</div>
				</form>
			</div>
		</div>
    </div>
	
	
	
	  


	<script type="text/javascript">
		$(function () {
			$("#txtUserName").observer({
				validators: [
					function (partial) {
						var $self = $(this);
						var email = $.trim($self.val());
						if (email === "") {
							return "This field is required";
						}
						if (_COMMON.ValidateEmail(email) == false) {
							return "Invalid email";
						}
						return "";
					}
				],
				validateOnBlur: true,
				group: "validatePassword"
			});
			$("#txtPassword").observer({
				validators: [
					function (partial) {
						var $self = $(this);
						if ($.trim($self.val()) === "") {
							return "This field is required";
						}
						return "";
					}
				],
				validateOnBlur: true,
				group: "validatePassword"
			});
			$("#btnLogin").on("click", onSubmit);
			$('#frmLogin input.form-control').on("keypress", function (e) {
				var code = (e.keyCode ? e.keyCode : e.which);
				if (code == 13) {
					e.preventDefault();
					e.stopPropagation();
					onSubmit.call(this);
				}
			});

			function onSubmit() {
				if ($.smValidate("validatePassword") == false) return false;
				$(this).closest("form").submit();
			}
			$(document).ajaxStart(function () { Pace.restart(); });
		});
	</script>
</body>
</html>
