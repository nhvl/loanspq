﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="IPAccessDenied.aspx.vb" Inherits="IPAccessDenied"
    EnableViewState="false" %>
<%@ Import Namespace="LPQMobile.Utils" %>

<%@ Register Src="~/Inc/PageHeader.ascx" TagPrefix="uc" TagName="pageHeader" %>
<%@ Register TagPrefix="uc" TagName="pageFooter" Src="~/Inc/PageFooter.ascx" %>
<%@ Import Namespace="System.Web.Optimization" %>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">
<head runat="server">      
    <title>LPQ Mobile</title>
    <%:Styles.Render("~/css/thirdparty/bootstrap") %>
    <uc:pageHeader ID="ucPageHeader" ScriptFolder="" runat="server" />
	
</head>
<body class = "lpq_container no-funding">
<div>
 <!-- getting started(pl1): credit card infor + scan document + disclosure -->
 <div data-role="page" id="pl1">
        <div data-role="header" style="display: none">
            <h1>Access Denied</h1>
        </div>
      <div data-role="content">
            <%=HeaderUtils.RenderLogo(_LogoUrl)%>
		  <br/>
		  <br/>
		  <div><%=IPAccessDeniedMessage%></div>
		  <br/>
			<div class="div-continue" style="margin-top: 10px; text-align: center;" data-role="footer">
	            <a id="btnRedirect" href="<%=_RedirectURL%>" onclick="gotoToUrl(this,'<%=_RedirectURL%>')" type="button" class="div-continue-button">Return to our website</a>
            </div>
       </div>
 </div>
          <!--ui new footer content --> 
       <div class="divfooter">
           <%=_strRenderFooterContact%>  
            <div style="text-align :center">
                <!-- lender name -->
                <div class="divLenderName"><%=_strLenderName%></div> 
                <div class="divFooterLogo">             
                    <% If String.IsNullOrEmpty(_hasFooterRight) Then%>  
                       <div class="divfooterlabel"><a href="<%=_strNCUAUrl%>"  class="ui-link" ><%=_strFooterRight%></a> <a href="<%=_strHUDUrl%>" class="ui-link"><%=_equalHousingText%></a></div>      
					<% Else%>
                        <%=_strFooterRight%>
                    <%End If%>
                </div>  
                <div><%=_strFooterLeft  %></div>   
           </div>
             
       </div> 
         <!--end new ui footer content -->            
</div>
	

<uc:pageFooter ID="ucPageFooter" runat="server" />
</body>

</html>