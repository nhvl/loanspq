﻿var isSubmitWalletAnswer = false;
var isCoSubmitWalletAnswer = false;
var isSubmittingLoan = false;
var g_current_full_he_address = '';
var g_he_property_states = [];

//START BLOCK A
//MOVE FROM HomeEquityLoan
//deprecate, use verify address
function lookupHeZipcode(zip) {
    //var address_key = $('#hdAddressKey').val();
    //if (address_key != "") {
    var url = '/handler/Handler.aspx';
    $.ajax({
        url: url,
        async: true,
        cache: false,
        type: 'POST',
        dataType: 'html',
        data: {
            command: 'lookupZipcode',
            zip: zip,
            previewCode: $("#hdPreviewCode").val()
        },
        success: function (responseText) {
            var result = responseText.split('|');
            var zip = result[0];
            var city = result[1];
            var state = result[2];

            $('#txtPropertyZip').val(zip);
            $('#txtPropertyCity').val(city);
            $('#ddlPropertyState').val(state);
            $('#ddlPropertyState').selectmenu('refresh');

            //verifyState();
            //verifyHeAddress();
        }
    });
    //}
}

function verifyState() {
    var selectedState = $('#ddlPropertyState').val().trim();
    if (g_he_property_states.length == 0 || selectedState == '') return true;

    return g_he_property_states.indexOf(selectedState.toLowerCase()) >= 0;
}
function hasCoApplicant() {
	return $("#hdHasCoApplicant").val() === "Y";
}
function goBackPageSubmit() {
	if (typeof resetFunding == 'function') {
		resetFunding();
	}
	if (window.ASI) {
		ASI.FACTORY.goBackFromPageSubmit();
	} else {
		if ($("#hdHasCoApplicant").val() === "Y") {
			goToNextPage("#he6");
		} else {
			goToNextPage("#he2");
		}
	}
}

function verifyHeAddress() {
    var address_key = $('#hdAddressKey').val();
    if (address_key != "") {
        var address = getHeAddress();

        if (validateHeAddress() && g_current_full_he_address != address) {
            var url = '/handler/Handler.aspx';
            $.ajax({
                url: url,
                async: true,
                cache: false,
                type: 'POST',
                dataType: 'html',
                data: {
                    command: 'verifyAddress',
                    address: address,
                    previewCode: $("#hdPreviewCode").val()
                },
                success: function (responseText) {
                    if (responseText.indexOf('res="OK"') < 0) {
                        //address service is down or configuration is bad, dont do anything
                        //TODO: log the error or email admin                      
                    }
                    else if (responseText.indexOf('FoundSingleAddress="False"') > -1) {  //not found single address                      
                        $('#spHeVerifyMessage').show();
	                    var shortDesc = /ShortDescriptive="(.*?)"/g.exec(responseText);
                        $('#spHeVerifyMessage').text(shortDesc[1]);
                    }
                    else if (responseText.indexOf('FoundSingleAddress="True"') > -1) {  //found single address                      
                        $('#spHeVerifyMessage').hide();
                        updateHeAddress(responseText);
                    }
                    else { //unknow case                     
                        $('#spHeVerifyMessage').hide();

                    }
                }
            });
        }
    }
}
function getHeAddress() {
    var address = 'street=' + $('#txtPropertyAddress').val() +
                  '&city=' + $('#txtPropertyCity').val() +
                  '&state=' + $('#ddlPropertyState').val() +
                  '&zip=' + $('#txtPropertyZip').val();

    return address;
}

function updateHeAddress(responseText) {
	var txtAddress = $('#txtPropertyAddress');
	var txtCity = $('#txtPropertyCity');
	var txtState = $('#ddlPropertyState');
	var txtZip = $('#txtPropertyZip');

	txtAddress.val(/street="(.*?)"/g.exec(responseText)[1]);
	txtCity.val(/city="(.*?)"/g.exec(responseText)[1]);
	txtState.val(/state="(.*?)"/g.exec(responseText)[1]);
	txtState.selectmenu('refresh');
	txtZip.val(/zip="(.*?)[-"]/g.exec(responseText)[1]);  //zip code may be 5 or 10 digit
	$.lpqValidate.hideValidation(txtAddress);
	$.lpqValidate.hideValidation(txtCity);
	$.lpqValidate.hideValidation(txtState);
	$.lpqValidate.hideValidation(txtZip);

}

function validateHeAddress() {
    return (($('#txtPropertyAddress').val() != '' &&
           $('#txtPropertyZip').val() != '') ||
          ($('#txtPropertyAddress').val() != '' &&
           $('#txtPropertyCity').val() != '' &&
            $('#ddlPropertyState').val() != ''));
}
//END BLOCK A
//for testing purpose
function autoFillData() {
	$('#ddlBranchName option').eq(1).prop('selected', true);
	$('#ddlBranchName').selectmenu("refresh").closest("div.ui-btn").addClass("btnActive_on");
	$("#divLoanPurpose a.btn-header-theme[data-role='button']:eq(0)").trigger("click");
	$("#txtLoanTerm").val("12");
	$('#ddlLoanTerm option').eq(1).prop('selected', true);
	$('#ddlLoanTerm').selectmenu("refresh");
	$('#ddlLoanReason option').eq(1).prop('selected', true);
	$('#ddlLoanReason').selectmenu("refresh");
	$('#ddlIntendOccupyProperty option').eq(1).prop('selected', true);
	$('#ddlIntendOccupyProperty').selectmenu("refresh");
	$("#txtEstimatedPropertyValue").val("700000").trigger("blur");;
	$('#txtLoanRequestAmount').val("75000").trigger("blur");;
    // $('#txtEstimatedPropertyValue').val("60000");
	$('#txtFirstMortgageBalance').val("200000").trigger("blur");;
    $('#ddlTypeOfProperty').val("2 UNIT");
    $('#txtPropertyAddress').val("8861 TRASK AVE");
    $('#txtPropertyCity').val("GARDEN GROVE");
    $('#txtPropertyZip').val("92841");
    $('#ddlPropertyState').val("CA");
    autoFillData_PersonalInfor();
    co_autoFillData_PersonalInfor();
    autoFillData_Address();
    co_autoFillData_Address();
    autoFillData_FinancialInFor();
    co_autoFillData_FinancialInFor();
    autoFillData_ContactInfor();
    co_autoFillData_ContactInfor();
    if ($("#txtIDDateIssued").length > 0) {
        autoFillData_ID();
    }
    if ($("#co_txtIDDateIssued").length > 0) {
        co_autoFillData_ID();
    }
    if (typeof autofill_Disclosure == "function") {
    	autofill_Disclosure();
    }
    if ($("#divDeclarationSection").length > 0) {
    	autofill_Declarations();
    }
    if ($("#co_divDeclarationSection").length > 0) {
    	co_autofill_Declarations();
    }
    if ($("#divProceedMembership").length > 0) {
        $("a.btn-header-theme[data-command='proceed-membership'][data-key='Y']", "#divProceedMembership").addClass("active btnActive_on");
        $("#hdProceedMembership").val("Y");
    }
}

function displayWalletQuestions(message) {
    // The first two characters of the message contains the number of questions
    var numQuestions = parseInt(message.substr(0, 2), 10);
    var questionsHTML = message.trim().substr(2, message.length - 2);
    // Put the number of questions into the hidden input field
    $('#hdNumWalletQuestions').val(numQuestions);
    // Put the HTML for the questions into the proper location in our page
    $('#walletQuestionsDiv').html(questionsHTML);
    nextpage = $('#walletQuestions');
    $.mobile.changePage(nextpage, {
        transition: "slide",
        reverse: false,
        changeHash: true
    });
}
//jquery mobile need to transition to another page in order to format the html autheitcation question correctly
function co_displayWalletQuestions(message) {
    // The first two characters of the message contains the number of questions
    var numQuestions = parseInt(message.substr(0, 2), 10);
    var questionsHTML = message.substr(2, message.length - 2);
    // Put the number of questions into the hidden input field
    $('#hdNumWalletQuestions').val(numQuestions);
    // Put the HTML for the questions into the proper location in our page   
    $('#walletQuestionsDiv').empty();
    $('#co_walletQuestionsDiv').html(questionsHTML);
    nextpage = $('#co_walletQuestions');
    $.mobile.changePage(nextpage, {
        transition: "slide",
        reverse: false,
        changeHash: true
    });
}
function validateWalletQuestions(element, isCoApp) {
	if ($(element).attr("contenteditable") == "true") return false;
    // Make sure that there is a selected answer for each question.
    // After validating the questions, submit them
    var index = 0;
    var numSelected = 0;
    var errorStr = "";
    var numOfQuestions = $('#hdNumWalletQuestions').val();
    numOfQuestions = parseInt(numOfQuestions, 10);
    for (index = 1; index <= numOfQuestions; index++) {
        // Check to see if a radio button was selected
        var radioButtons = $('input[name=walletquestion-radio-' + index.toString() + ']');
        var numRadioButtons = radioButtons.length;
        for (var i = 0; i < numRadioButtons; i++) {
            if ($(radioButtons[i]).prop('checked') == true) {
                numSelected++;
            }
        }
        if (numSelected == 0) {
            errorStr += "Please answer all the questions";
            break;
        }
        numSelected = 0;
    }
    if (errorStr != "") {
    	// An error occurred, we need to bring up the error dialog.
    	if ($("#divErrorPopup").length > 0) {
    		$('#txtErrorPopupMessage').html(errorStr);
    		$("#divErrorPopup").popup("open", { "positionTo": "window" });
    	} else {
    		var nextpage = $('#divErrorDialog');
    		$('#txtErrorMessage').html(errorStr);
    		if (nextpage.length > 0) {
    			$.mobile.changePage(nextpage, {
    				transition: "slide",
    				reverse: false,
    				changeHash: true
    			});
    		}
    	}
        return false;
    }
    // No errors, so we can proceed
    submitWalletAnswers(isCoApp);    
    return false;
}

function submitWalletAnswers(isCoApp) {
    var coPrefix = "";
    if (isCoApp) coPrefix = "co_";
    var $submitWQButton = $('#' + coPrefix + 'walletQuestions .div-continue-button');
    //disable submit button before calling ajax to prevent user submit multiple times
    $submitWQButton.addClass('ui-disabled');
    //$('#txtErrorMessage').html(''); //useless
	var answersObj = getWalletAnswers(isCoApp);
	answersObj = attachGlobalVarialble(answersObj);
	if ($("#hdIsComboMode").val() == "Y") {
	    if ($("#divCcFundingOptions").length === 1) {
	        answersObj.depositAmount = Common.GetFloatFromMoney($('#txtFundingDeposit').val());
	        FS1.loadValueToData();
	        answersObj.rawFundingSource = JSON.stringify(FS1.Data);
	    }
	}
    $.ajax({
        type: 'POST',
        url: 'Callback.aspx',
        data: answersObj,
        dataType: 'html',
        success: function (response) {           
            if (response.toUpperCase().indexOf('MLERRORMESSAGE') > -1) {
                $submitWQButton.removeClass('ui-disabled');
                goToDialog(response);
            }
            else {
                if (response.toUpperCase().indexOf('MLNOIDAUTHENTICATION') > -1) {
                    isApplicationCompleted = true;
                    goToLastDialog(response);
                    //need to display response first before clear all inputs
                    //clearForms();           
                } else if (response.toUpperCase().indexOf('WALLETQUESTION') > -1) {
                    co_displayWalletQuestions(response);                 
                } else {
                    //   isApplicationCompleted = true;
                    goToLastDialog(response);
                    //need to display response first before clear all inputs
                    //clearForms();               
                }
            }
        },
        error: function (err) {
            //enable submit answer button
            $submitWQButton.removeClass('ui-disabled');
            goToDialog('There was an error when processing your request.  Please try the application again.');
        }
    });
    return false;
}

function getWalletAnswers(isCoApp) {
    // Grab the answer choices that have been selected by the user
	var appInfo = HE.FACTORY.getHomeEquityLoanInfo(false);
    /*var index = 0;
    var numSelected = 0;
    var numOfQuestions = $('#hdNumWalletQuestions').val();
    numOfQuestions = parseInt(numOfQuestions, 10);
    var answersArray = new Array();
    for (index = 1; index <= numOfQuestions; index++) {
        var selectedAnswer = $('input[name=walletquestion-radio-' + index.toString() + ']:checked').attr('id');
        if (selectedAnswer != "" && selectedAnswer != null) {
            //exclude "rq"+i+"_" prefix from selectectAnswer
            var rqPrefixLength = 4;
            if (i >= 10) {
                rqPrefixLength = 5;
            }
            answersArray[index - 1] = selectedAnswer.substring(rqPrefixLength, selectedAnswer.length);
        }
    }
    // Put the answers into a semicolon separated string
    // Only the ids of the answers will be passed to the callback code
    var walletAnswersConcatStr = "";
    for (var i = 0; i < numOfQuestions; i++) {
        walletAnswersConcatStr += answersArray[i] + ";";
    }
    // Get rid of the last semicolon
    walletAnswersConcatStr = walletAnswersConcatStr.substr(0, walletAnswersConcatStr.length - 1);
    appInfo.WalletAnswers = walletAnswersConcatStr;
    */

    //get wallet questions and answers
    getWalletQuestionsAndAnswers(appInfo);

    // appInfo.CSRF = $('#hfCSRF').val();
    appInfo.LenderRef = $('#hfLenderRef').val();
    appInfo.Task = "WalletQuestions";
    appInfo.idaMethodType = $('#hdIdaMethodType').val();
    appInfo.co_FirstName = $('#co_txtFName').val();
    appInfo.ProceedXAAnyway = $("#hdProceedMembership").val();
    appInfo.IsComboMode = $("#hdIsComboMode").val();
    if (isCoApp) {
        appInfo.hasJointWalletAnswer = "Y";
    } else {
        appInfo.hasJointWalletAnswer = "N";
    }
    return appInfo;
}
function SelectProduct(index) {
    $('#txtErrorMessage').html('');
    var appInfo = new Object();
    appInfo.LenderRef = $('#hfLenderRef').val();
    appInfo.Index = index;
    appInfo.Task = 'SelectProduct';
    var txtLName = $('#txtLName').val();
    var txtFName = $('#txtFName').val();
    appInfo.FullName = txtFName + ' ' + txtLName;
    appInfo = attachGlobalVarialble(appInfo);
    $.ajax({
        type: 'POST',
        url: 'CallBack.aspx',
        data: appInfo,
        dataType: 'html',
        success: function (response) {
            if (response.toUpperCase().indexOf('MLERRORMESSAGE') > -1) {
                goToDialog(response);
            } else {
                //submit sucessfull so clear
                clearForms();
                goToLastDialog(response);
            }
        },
        error: function (err) {
            $('input.custom').attr('disabled', false); //if there is errors, enable agree button
            goToDialog('There was an error when processing your request.  Please try the application again.');
        }
    });
    //there no error
    return false;
}

function goToDialog(message) {
    if ($("#divErrorPopup").length > 0) {
    	$('#txtErrorPopupMessage').html(message);  //error message are not encoded, endoding will remove html tag
    	$("#divErrorPopup").popup("open", { "positionTo": "window" });
    } else {
    	$('#txtErrorMessage').html(message);  //error message are not encoded, endoding will remove html tag
    	$('#href_show_dialog_1').trigger('click');
    }
}
function goToLastDialog(message) {
    clearForms();
    $('#div_system_message').html(message);

    //docusign window if enabled
    if ($('#div_system_message').find("iframe#ifDocuSign").length === 1) {
        appendDocuSign();
        $("#div_docuSignIframe").append($('#div_system_message').find("iframe#ifDocuSign"));

        ////goback to final decision message after docusign is closed
        //$('#div_docuSignIframe').find("iframe#ifDocuSign").on("load", function (evt) {
        //    //the initial page load twice so need this to make sure only capturing url on finish
        //    var flag = ($(this).data("loaded") || "") + "1";
        //    $(this).data("loaded", flag);
        //    if (flag === "111") {
        //        $('#href_show_last_dialog').trigger('click');
        //    }
        //});

        var height = window.innerHeight ||
                 document.documentElement.clientHeight ||
                 document.body.clientHeight;

        var footerHeight = $('.divfooter').height();

        height = height - footerHeight - 64;
        $('#div_docuSignIframe').find("iframe#ifDocuSign").attr("height", height);
        $.mobile.changePage("#popupDocuSign", {
            transition: "fade",
            reverse: false,
            changeHash: false
        });
    }
        //cross-sell link button if enabled
    else {
        $(".xsell-selection-panel .xsell-item-option.btn-header-theme", "#div_system_message").on("mouseenter click", function (ev) {
        	var element = $(this);
        	if ($self.attr("contenteditable") == "true") return false;
            if (ev.type != 'click') {
                element.addClass('btnHover');
                element.removeClass('btnActive_on');
                element.removeClass('btnActive_off');
            } else {
                element.removeClass('btnHover');
            }
        }).on("mouseleave", function () {
            handledBtnHeaderTheme($(this));
        });
        $('#href_show_last_dialog').trigger('click');
    }
}
//doing this to utilize xaApplicantID in other module

(function (HE, $, undefined) {
    HE.FACTORY = {};
    function toggleHasCoApplicant() {
    	var $self = $(this);
    	if ($self.attr("contenteditable") == "true") return false;
        var $dataHolder = $("#" + $self.data("hidden-field"));
        if ($dataHolder.val() === "N") {
            $dataHolder.val("Y");
        } else {
            $dataHolder.val("N");
        }
        $self.toggleClass("active");
        handledBtnHeaderTheme($self);
    }
    function bindEventLoanPurposeOption() {
        $("#divLoanPurpose").find("a[data-command='loan-purpose']").off("click").on("click", function () {
            var $self = $(this);
            //first, remove other active button
            $("#divLoanPurpose").find("a[data-command='loan-purpose']").each(function () {
                $(this).removeClass("active");
                handledBtnHeaderTheme($(this));
            });
            //then, activate selected button
            $self.addClass("active");
            $("#hdLoanPurpose").val($self.data("key"));
            handledBtnHeaderTheme($self);
            toggleLoanTerm();
            toggleReasonableDemoPDFView();
            toggleDisclosure();
            toggleApplicantQuestion();
            toggleCustomQuestion();
            toggleProtectionInfo();
	        $.lpqValidate.hideValidation("#divLoanPurpose");
        });
    }
    function getLoanPurpose() {
        var loanPurposeValue = $('#hdLoanPurpose').val().toUpperCase();
        var loanPurpose = '';
        if (loanPurposeValue.indexOf("LINE OF CREDIT") != -1 || loanPurposeValue.indexOf("HELOC") != -1) {
            loanPurpose = "LINE OF CREDIT";
        } else {
            loanPurpose = "HOME EQUITY LOAN";
        }
        return loanPurpose;
    }

    function toggleLoanTerm() {
        if (getLoanPurpose() == "LINE OF CREDIT") {
            if ($('#ddlLoanTerm').length > 0) {
                $('#ddlLoanTerm option:selected').val('0');
            }
            if ($('#txtLoanTerm').length > 0) {
                $('#txtLoanTerm').val('0');
            }
            $('#divLoanTerm').hide();
        } else {
            $('#txtLoanTerm').val(''); //reset the loan term field
            $('#divLoanTerm').show();
        }
    }
    function toggleReasonableDemoPDFView() {
        var loanPurpose = getLoanPurpose();
        $('#divVerifyCodeHELOC').hide();
        switch (loanPurpose) {
            case 'LINE OF CREDIT':
                $('#divVerifyCode').hide();
                $('#divVerifyCodeHELOC').show();
                break;
            case 'HOME EQUITY LOAN':
                $('#divVerifyCode').show();
                $('#divVerifyCodeHELOC').hide();
                break;
            default:
                $('#divVerifyCode').show();
                $('#divVerifyCodeHELOC').hide();
                break;
        }
    }
    function toggleDisclosure() {
        var loanPurpose =  getLoanPurpose();    
        switch (loanPurpose) {
            case 'LINE OF CREDIT':
                $("div[name='disclosuresLOC']").hide();
                $("div[name='disclosuresHELOC']").show();
                $("#divEmailMeButton").addClass('hidden');
                $("#divEmailMeButtondisclosuresHELOC").removeClass('hidden');
                break;
            case 'HOME EQUITY LOAN':
                $("div[name='disclosuresLOC']").show();
                $("div[name='disclosuresHELOC']").hide();
                $("#divEmailMeButton").removeClass('hidden');
                $("#divEmailMeButtondisclosuresHELOC").addClass('hidden');
                break;
            default:
                $("div[name='disclosuresLOC']").show();
                $("div[name='disclosuresHELOC']").hide();
                $("#divEmailMeButton").removeClass('hidden');
                $("#divEmailMeButtondisclosuresHELOC").addClass('hidden');
                break;
        }
    }
    function toggleApplicantQuestion() {
 
        var hasCoApp = $("#hdHasCoApplicant").val() == "Y";
        var strAQHE = $('#hdAQHtmlHE').val();
        var strAQHELOC = $('#hdAQHtmlHELOC').val();
        var divAQEle = $('#divApplicantQuestion');
        var co_strAQHE = $('#co_hdAQHtmlHE').val();
        var co_strAQHELOC = $('#co_hdAQHtmlHELOC').val();
        var co_divAQEle = $('#co_divApplicantQuestion');

        if ($('#hdIsCQNewAPI').val() != "Y") {
            return;
        }
        if (divAQEle.length == 0) {
            return;
        }
        if (hasCoApp) {
            if (co_divAQEle.length == 0) {
                return;
            }
        }
        if (getLoanPurpose() == 'LINE OF CREDIT') {
            divAQEle.html('');
            if (strAQHELOC != undefined) {
                divAQEle.html(JSON.parse(strAQHELOC));
            }
            if (hasCoApp) {
                co_divAQEle.html('');
                if (co_strAQHELOC != undefined) {
                    co_divAQEle.html(JSON.parse(co_strAQHELOC));
                }
            }
  
        } else {
            divAQEle.html('');
            if (strAQHE != undefined) {
                divAQEle.html(JSON.parse(strAQHE));
            }
            if (hasCoApp) {
                co_divAQEle.html('');
                if (co_strAQHE != undefined) {
                    co_divAQEle.html(JSON.parse(co_strAQHE));
                }
            }
        }

        divAQEle.trigger('create');
        delete OBSERVERDB["ValidateApplicantQuestionsXA"];
        registerXAApplicantQuestions();

        if (hasCoApp) {
            co_divAQEle.trigger('create');
            delete OBSERVERDB["co_ValidateApplicantQuestionsXA"];
            co_registerXAApplicantQuestions();
        } 

	}
	// Note: This function has no effect anymore.
    function toggleCustomQuestion(){
        //by default show HE custom question and hide HELOC custom questions
        var divHEElem = $('#div_custom_questions_HE');
        var divHELOCElem = $('#div_custom_questions_HELOC');
        divHELOCElem.addClass('HiddenElement');
        divHEElem.removeClass('HiddenElement');
        if (getLoanPurpose() == 'LINE OF CREDIT') {
            divHELOCElem.removeClass('HiddenElement');
            divHEElem.addClass('HiddenElement');
        }

        //handle rendering customquestions
        var strCQHE = $('#hdCQHtmlHE').val();
        var strCQHELOC = $('#hdCQHtmlHELOC').val();
        var divCQTitleElem = $('#pagesubmit .divCustomQuestions').children().first();
        //HE custom Questions
        if (!divHEElem.hasClass('HiddenElement')) {
            
            divHELOCElem.children().not('input[id="hdCQHtmlHELOC"]').remove();
            if (strCQHE != undefined && strCQHE != "") {
                //show custom question title
                divCQTitleElem.removeClass('HiddenElement');
                if (divHEElem.children().not('input[id="hdCQHtmlHE"]').length == 0) {
                    divHEElem.append(JSON.parse(strCQHE));
                }
                divHEElem.trigger('create');
                delete OBSERVERDB["ValidateCustomQuestionsHE"];
                delete OBSERVERDB["ValidateCustomQuestionsHELOC"];
                registerCQValidatorHE();
            } else {
                //hide custom question title
                divCQTitleElem.addClass('HiddenElement');
            }
        } else {
            divHEElem.children().not('input[id="hdCQHtmlHE"]').remove();
        }

        //HELOC custom questions
        if (!divHELOCElem.hasClass('HiddenElement')) {
            divHEElem.children().not('input[id="hdCQHtmlHE"]').remove();
            if (strCQHELOC != undefined && strCQHELOC != "") {
                //show custom question title
                divCQTitleElem.removeClass('HiddenElement');
                if (divHELOCElem.children().not('input[id="hdCQHtmlHELOC"]').length == 0) {
                    divHELOCElem.append(JSON.parse(strCQHELOC));
                }
                divHELOCElem.trigger('create');
                delete OBSERVERDB["ValidateCustomQuestionsHE"];
                delete OBSERVERDB["ValidateCustomQuestionsHELOC"];
                registerCQValidatorHELOC();
            } else {
                //hide custom question title
                divCQTitleElem.addClass('HiddenElement');
            }
        } else {
            divHELOCElem.children().not('input[id="hdCQHtmlHELOC"]').remove();
        }
    }
    function toggleProtectionInfo() {
        //by default show HE protections and hide HELOC protection
        var divHEElem = $('#divProtectionInfo');
        var divHELOCElem = $('#heloc_divProtectionInfo');
        divHELOCElem.addClass('HiddenElement');
        divHEElem.removeClass('HiddenElement');
        if (getLoanPurpose() == 'LINE OF CREDIT') {
            divHELOCElem.removeClass('HiddenElement');
            divHEElem.addClass('HiddenElement');
        }
    }
    function getLoanTerm() {
        if ($('#txtLoanTerm').length > 0) {
            return $.trim($('#txtLoanTerm').val());
        }
        return $('#ddlLoanTerm option:selected').val();
    }


    function viewHomeEquityLoanInfo() {
        var strHtml = "";
        if ($.trim($("#hdLoanPurpose").val()) !== "") {
            strHtml += '<div class="col-sm-6 col-xs-12"><div class="row"><div class="col-xs-6 text-right row-title"><span class="bold">Loan Purpose</span></div><div class="col-xs-6 text-left row-data"><span>' + LOANPURPOSE[$("#hdLoanPurpose").val()] + '</span></div></div></div>';
        }
        strHtml += '<div class="col-sm-6 col-xs-12"><div class="row"><div class="col-xs-6 text-right row-title"><span class="bold">Amount Requested</span></div><div class="col-xs-6 text-left row-data"><span>' + Common.FormatCurrency($("#txtLoanRequestAmount").val()) + '</span></div></div></div>';
        if (getLoanPurpose() !== "LINE OF CREDIT") {
            strHtml += '<div class="col-sm-6 col-xs-12"><div class="row"><div class="col-xs-6 text-right row-title"><span class="bold">Term (months)</span></div><div class="col-xs-6 text-left row-data"><span>' + getLoanTerm() + '</span></div></div></div>';
        }
        strHtml += '<div class="col-sm-6 col-xs-12"><div class="row"><div class="col-xs-6 text-right row-title"><span class="bold">Loan Reason</span></div><div class="col-xs-6 text-left row-data"><span>' + $("#ddlLoanReason option:selected").text() + '</span></div></div></div>';
        strHtml += '<div class="col-sm-6 col-xs-12"><div class="row"><div class="col-xs-6 text-right row-title"><span class="bold">Estimated Property Value</span></div><div class="col-xs-6 text-left row-data"><span>' + Common.FormatCurrency($("#txtEstimatedPropertyValue").val()) + '</span></div></div></div>';
        strHtml += '<div class="col-sm-6 col-xs-12"><div class="row"><div class="col-xs-6 text-right row-title"><span class="bold">Current Mortgage Balance</span></div><div class="col-xs-6 text-left row-data"><span>' + Common.FormatCurrency($("#txtFirstMortgageBalance").val()) + '</span></div></div></div>';
        strHtml += '<div class="col-sm-6 col-xs-12"><div class="row"><div class="col-xs-6 text-right row-title"><span class="bold">2nd Mortgage Balance (if applicable)</span></div><div class="col-xs-6 text-left row-data"><span>' + $("#txtSecondMortgageBalance").val() + '</span></div></div></div>';
        strHtml += '<div class="col-sm-6 col-xs-12"><div class="row"><div class="col-xs-6 text-right row-title"><span class="bold">How do you intend to occupy the property?</span></div><div class="col-xs-6 text-left row-data"><span>' + $("#ddlIntendOccupyProperty option:selected").text() + '</span></div></div></div>';
        if ($("#ddlProofOfIncome").val() != undefined && $("#ddlProofOfIncome").val() != "") {
            strHtml += '<div class="col-sm-6 col-xs-12"><div class="row"><div class="col-xs-6 text-right row-title"><span class="bold">Can you provide proof of your income and asset(s)?</span></div><div class="col-xs-6 text-left row-data"><span>' + $("#ddlProofOfIncome option:selected").text() + '</span></div></div></div>';
        }
        return strHtml;
    }

    function ViewPropertyAddress() {
        var strHtml = "";
        strHtml += '<div class="col-sm-6 col-xs-12"><div class="row"><div class="col-xs-6 text-right row-title"><span class="bold">Property Type</span></div><div class="col-xs-6 text-left row-data"><span>' + $("#ddlTypeOfProperty").val() + '</span></div></div></div>';
        if ($("#hdIsShowPropertyAddress").val() == "Y") {
            var strAddr = "";
            strAddr += $("#txtPropertyAddress").val();
            strAddr += " " + $("#txtPropertyCity").val();
            strAddr += ", " + $("#ddlPropertyState").val();
            strAddr += " " + $("#txtPropertyZip").val();
            strHtml += '<div class="col-sm-6 col-xs-12"><div class="row"><div class="col-xs-6 text-right row-title"><span class="bold">Property Address</span></div><div class="col-xs-6 text-left row-data"><span>' + strAddr + '</span></div></div></div>';
        } else {
            strHtml += '<div class="col-sm-6 col-xs-12 row-title"><span>Property address is the same as current address</span></div>';
        }
        return strHtml;
    }

    function ViewExpectedMonthlyExpense() {
        var strHtml = "";
        strHtml += '<div class="col-sm-6 col-xs-12"><div class="row"><div class="col-xs-6 text-right row-title"><span class="bold">Monthly Hazard Insurance</span></div><div class="col-xs-6 text-left row-data"><span>' + $("#txtHazardInsurance").val() + '</span></div></div></div>';
        strHtml += '<div class="col-sm-6 col-xs-12"><div class="row"><div class="col-xs-6 text-right row-title"><span class="bold">Monthly Real Estate Taxes</span></div><div class="col-xs-6 text-left row-data"><span>' + $("#txtRealEstateTaxes").val() + '</span></div></div></div>';
        strHtml += '<div class="col-sm-6 col-xs-12"><div class="row"><div class="col-xs-6 text-right row-title"><span class="bold">Monthly Mortgage Insurance</span></div><div class="col-xs-6 text-left row-data"><span>' + $("#txtMortgageInsurance").val() + '</span></div></div></div>';
        strHtml += '<div class="col-sm-6 col-xs-12"><div class="row"><div class="col-xs-6 text-right row-title"><span class="bold">Monthly Homeowner Assn. Dues</span></div><div class="col-xs-6 text-left row-data"><span>' + $("#txtHomeAssiciationDues").val() + '</span></div></div></div>';
        return strHtml;
    }

    function handleShowHidePropertyAddress(e) {
        var $self = $(this);
        var $hdCtrl = $("#hdIsShowPropertyAddress");
        if ($hdCtrl.val() === "Y") {
            $hdCtrl.val("N");
            $("#divPropertyAddress").hide();
            $self.html("Use different address for property");
            $self.addClass("chevron-circle-right-before").removeClass("chevron-circle-down-before");
        } else {
            $hdCtrl.val("Y");
            $("#divPropertyAddress").show();
            $self.html("Use same address for property");
            $self.addClass("chevron-circle-down-before").removeClass("chevron-circle-right-before");   
        }
        if (BUTTONLABELLIST != null) {
        	var value = BUTTONLABELLIST[$.trim($self.html()).toLowerCase()];
        	if (typeof value == "string" && $.trim(value) !== "") {
        		$self.html(value);
        	}
        }
	    e.preventDefault();
    }

	/*deprecated - don't know why do we need this function. It always return true because it allows blank or zero*/
    function validateExpectedMonthlyExpense() {
        var txtHazardInsurance = $('#txtHazardInsurance').val();
        var txtRealEstateTaxes = $('#txtRealEstateTaxes').val();
        var txtMortgageInsurance = $('#txtMortgageInsurance').val();
        var txtHomeAssiciationDues = $('#txtHomeAssiciationDues').val();
        //var txtOtherLoanInterest = $('#txtOtherLoanInterest').val();

        var strMessage = '';

        if (!Common.ValidateTextAllowBlankZero(txtHazardInsurance))
            strMessage += 'Hazard Insurance is required<br />';
        if (!Common.ValidateTextAllowBlankZero(txtRealEstateTaxes))
            strMessage += 'Real Estate Taxes is required<br />';
        if (!Common.ValidateTextAllowBlankZero(txtMortgageInsurance)) {
            strMessage += 'Mortage Insurance is required<br />';
        }
        if (!Common.ValidateTextAllowBlankZero(txtHomeAssiciationDues)) {
            strMessage += 'Homeowner Assn. Dues is required<br />';
        }
        //if (!Common.ValidateTextAllowBlankZero(txtOtherLoanInterest)) {
        //    strMessage += 'Other Loan Principal & Interest is required<br />';
        //}
        return strMessage;
    }

    function validateAddressOfTheProperty() {
        //var strHtml = '';
        //var ddlTypeOfProperty = $('#ddlTypeOfProperty').val();
        //if (!Common.ValidateText(ddlTypeOfProperty)) {
        //    strHtml += 'Property Type is required<br />';
        //}
        //if ($("#hdIsShowPropertyAddress").val() === "Y") {
        //    var strMessage = "";
        //    var txtPropertyAddress = "";

        //    //validate property address
        //    txtPropertyAddress = $('#txtPropertyAddress').val();
        //    var txtPropertyCity = $('#txtPropertyCity').val();
        //    var txtPropertyZip = $('#txtPropertyZip').val();
        //    var ddlPropertyState = $('#ddlPropertyState').val();
        //    if (!Common.ValidateText(txtPropertyAddress)) {
        //        strMessage += '&nbsp;&nbsp;Property Address is required<br />';
        //    }
        //    //not allow PO Box address
        //    if (isPOBox(txtPropertyAddress)) {
        //        strMessage += txtPropertyAddress + " is not accepted. Please enter a different address <br/>";
        //    }
        //    if (!Common.ValidateText(txtPropertyCity)) {
        //        strMessage += '&nbsp;&nbsp;City is required<br />';
        //    }
        //    if (!Common.ValidateText(txtPropertyZip)) {
        //        strMessage += '&nbsp;&nbsp;Zip is required<br />';
        //    }
        //    if (!Common.ValidateText(ddlPropertyState)) {
        //        strMessage += '&nbsp;&nbsp;State is required<br />';
        //    }
        //    if (!verifyState()) {
        //        strMessage += '&nbsp;&nbsp;We currently do not serve your area.  Please contact us at ' + g_lenderPhone + ' if you have any questions.<br />';
        //    }
        //    if (strMessage != "") {
        //        strHtml += "The Property Address is incompleted <br/>" + strMessage;
        //    }
        //} else {
        //    //validate current address instead
        //    strMessage = "";
        //    txtPropertyAddress = $("#txtAddress").val();
        //    if (isPOBox(txtPropertyAddress)) {
        //        strMessage += txtPropertyAddress + " is not accepted. Please enter a different address <br/>";
        //    }
        //    if (!verifyState()) {
        //        strMessage += '&nbsp;&nbsp;We currently do not serve your area.  Please contact us at ' + g_lenderPhone + ' if you have any questions.<br />';
        //    }
        //    if (strMessage != "") {
        //        strHtml += "The Property Address is incompleted <br/>" + strMessage;
        //    }
        //}
        //return strHtml;

        var validator1 = $.lpqValidate("ValidatePropertyType");
        if ($("#hdIsShowPropertyAddress").val() === "Y") {
	        var validator2 = $.lpqValidate("ValidatePropertyAddress");
	        return validator1 && validator2;
        } else {
        	var validator3 = true;
        	var state = $.trim($("#ddlState").val());
        	if (g_he_property_states.length == 0) return true;
        	if (g_he_property_states.indexOf(state.toLowerCase()) < 0) {
        		validator3 = false;
		        $.lpqValidate.showValidation($("#ddlState"), 'We currently do not serve your area.  Please contact us at ' + g_lenderPhone + ' if you have any questions');
        	}
	        return validator1 && validator3;
        }
    }
	
    function renderReviewContent() {
        viewSelectedBranch(); //view selected branch
        $(".ViewHomeEquityLoanInfo").html(viewHomeEquityLoanInfo.call($(".ViewHomeEquityLoanInfo")));
        $(".ViewAcountInfo").html(ViewAccountInfo.call($(".ViewAcountInfo")));
	    if ($("#hdEnableReferenceInformation").val() == "Y") {
        $(".ViewReferenceInfo").html(ViewReferenceInfo.call($(".ViewReferenceInfo")));
	    }

        $(".ViewContactInfo").html(ViewContactInfo.call($(".ViewJointApplicantContactInfo")));
        $(".ViewAddress").html(ViewAddress.call($(".ViewAddress")));
        $(".ViewPropertyAddress").html(ViewPropertyAddress.call($(".ViewPropertyAddress")));
        if ($("#hdEnableIDSection").val() === "Y") {
            $(".ViewApplicantID").html(ViewPrimaryIdentification.call($(".ViewApplicantID")));
        }
        $(".ViewFinancialInfo").html(ViewFinancialInfo.call($(".ViewFinancialInfo")));
        $(".ViewPrevEmploymentInfo").html(ViewPrevEmploymentInfo.call($(".ViewPrevEmploymentInfo")));
        if ($("#hdIsShowExpectedMonthlyExpense").val() === "Y") {
            $(".ViewExpectedMonthlyExpense").html(ViewExpectedMonthlyExpense.call($(".ViewExpectedMonthlyExpense")));
        }
        if ($("#divDeclarationSection").length > 0) {
            $(".ViewDeclaration").html(ViewDeclaration.call($(".ViewDeclaration")));
        }
        if ($('#divApplicantQuestion').length > 0) {
            $(".ViewApplicantQuestion").html(ViewApplicantQuestion.call($('.ViewApplicantQuestion')));
        }
        if ($("#hdHasCoApplicant").val() === "Y") {
            $(".ViewJointApplicantInfo").html(co_ViewAccountInfo.call($(".ViewJointApplicantInfo")));
	        if ($("#hdEnableReferenceInformationForCoApp").val() == "Y") {
            $(".ViewJointReferenceInfo").html(co_ViewReferenceInfo.call($(".ViewJointReferenceInfo")));
	        }

            $(".ViewJointApplicantContactInfo").html(co_ViewContactInfo.call($(".ViewJointApplicantContactInfo")));
            $(".ViewJointApplicantAddress").html(co_ViewAddress.call($(".ViewJointApplicantAddress")));
            if ($("#hdEnableIDSectionForCoApp").val() === "Y") {
                $(".ViewJointApplicantID").html(co_ViewPrimaryIdentification.call($(".ViewJointApplicantID")));
            }
            $(".ViewJointApplicantFinancialInfo").html(co_ViewFinancialInfo.call($(".ViewJointApplicantFinancialInfo")));
            $(".ViewJointApplicantPrevEmploymentInfo").html(co_ViewPrevEmploymentInfo.call($(".ViewJointApplicantPrevEmploymentInfo")));
            if ($("#co_divDeclarationSection").length > 0) {
                $(".ViewJointApplicantDeclaration").html(co_ViewDeclaration.call($(".ViewJointApplicantDeclaration")));
            }
            if ($('#co_divApplicantQuestion').length > 0) {
                $(".ViewJointApplicantQuestion").html(co_ViewApplicantQuestion.call($('.ViewJointApplicantQuestion')));
            }
        } else {
            $(".jna-panel").hide();
        }
        $("div.review-container div.row-title b").each(function (idx, ele) {
        	if (typeof $(ele).data("renameid") == "undefined") {
        		var dataId = getDataId();
        		$(ele).attr("data-renameid", dataId);
        		RENAME_REPOSITORY[dataId] = htmlEncode($(ele).html());
        	}
        });
    }

    function hasProofOfIncome() {
        var hasProof = $('#hdHasProofOfIncome').val();
        if (hasProof == "N")
            return false;
        return true;
    }

    // disclosures are based on the loan purpose -> 
    //if disclosure is plain text limit 160 characters, and if it contains hyperlink only get the hyperlink labels
    function parseDisclosures_HE(disclosureName) {
        //get all disclosures 
        var disclosures = "";
        var temp = "";
        //get all btn disclosures
        var btnDisclosureElem = $("div[name='" + disclosureName + "']  .btn-header-theme");
        var chkDisclosureElem = $("div[name='" + disclosureName + "'] .ui-checkbox");
        //get submit message
        var submitMessage = $('#divSubmitMessage').text().trim();
        if (submitMessage.length > 150) {
            submitMessage = submitMessage.substring(0, 150)+ "...";
        }
  
        if (btnDisclosureElem.length == 0 && chkDisclosureElem.length == 0) {
            return "\n" + submitMessage + "\n\n";
        }

        //get all btn disclosures
        if (btnDisclosureElem.length > 0) {
            btnDisclosureElem.each(function () {
                var currBtnElem = $(this);
                if (currBtnElem.hasClass('active')) {
                    disclosures += currBtnElem.text() + "\n\n";
                }
            });
        }//end get all btn disclosures

        if (chkDisclosureElem.length > 0) {
            chkDisclosureElem.each(function () {
                var element = $(this).children('label');
                temp = element.text().trim().replace(/\s+/g, " ");
                var linkEle = element.find('a');
                if (linkEle.length > 0) {
                    if (linkEle.length == 1) { //only one hyperlink
                        disclosures += linkEle.text() + "\n\n";
                    } else { //more than one hyperlink
                        var labelHyperlink = "";
                        linkEle.each(function () {
                            labelHyperlink += $(this).text() + ", ";
                        });
                        labelHyperlink = labelHyperlink.substring(0, labelHyperlink.lastIndexOf(","));
                        var pos = labelHyperlink.lastIndexOf(",");
                        disclosures += labelHyperlink.substring(0, pos + 1) + " and " + labelHyperlink.substring(pos + 1) + "\n\n";
                    }
                } else { //only text -->limit disclosure 160 characters
                    if (temp.length > 160) {
                        disclosures += temp.substring(0, 160) + "...\n\n";
                    } else {
                        disclosures += temp + "\n\n";
                    }
                }
            });
        }
        return disclosures;
    }
    //need to get right disclosures for each loan purpose selection
    function getDisclosures() {
        var disclosures = "";
        if (getLoanPurpose() == "LINE OF CREDIT") {
            disclosures = parseDisclosures_HE('disclosuresHELOC');
        } else if (getLoanPurpose() == "HOME EQUITY LOAN") {
            disclosures = parseDisclosures_HE('disclosuresLOC');
        }  
        return disclosures;
    }

    function saveHomeEquityLoanInfo() {
        //if (g_submit_button_clicked) return false;
        //$('#txtErrorMessage').html(''); //useless
        //disable submit button before calling ajax to prevent user submit multiple times
        $('#pagesubmit .div-continue-button').addClass('ui-disabled');
        var loanInfo = getHomeEquityLoanInfo(false);
        //save instaTouchPrefillComment
        $instaTouchComment = $('#hdInstaTouchPrefillComment');
        if ($instaTouchComment.val() != undefined) {
            loanInfo.InstaTouchPrefillComment = $instaTouchComment.val();
        }  
    	loanInfo = attachGlobalVarialble(loanInfo);
        $.ajax({
            type: 'POST',
            url: 'CallBack.aspx',
            data: loanInfo,
            dataType: 'html',
            success: function (response) {
                //clear instaTouchPrefill 
                if ($instaTouchComment.val() != undefined && $instaTouchComment.val() != "") {
                    $instaTouchComment.val("");
                }
                if (response.toUpperCase().indexOf('MLERRORMESSAGE') > -1) {
                    $('#pagesubmit .div-continue-button').removeClass('ui-disabled'); //if there is errors, enable agree button
                    goToDialog(response);
                } else if (response.toUpperCase().indexOf('WALLETQUESTION') != -1) {
                    displayWalletQuestions(response);                 
                } else if (response == "blockmaxapps") {
                	goToNextPage("#pageMaxAppExceeded");
                } else {
                    //submit sucessfull so clear
                    //clearForms();
                    goToLastDialog(response);              
                }
            },
            error: function (err) {
                //enable submit answer button 
                $('#pagesubmit .div-continue-button').removeClass('ui-disabled');

                $('input.custom').attr('disabled', false); //if there is errors, enable agree button
                goToDialog('There was an error when processing your request.  Please try the application again.');
            }
        });      
        return false;
    }

    function prepareAbrFormValues(pageId) {
    	var loanInfo = getHomeEquityLoanInfo(false);
    	var formValues = {};
    	_.forEach(pagePaths, function (p) {
    		collectFormValueData(p.replace("#", ""), loanInfo, formValues);
    		if (pageId == p.replace("#", "")) return false;
    	});
    	return formValues;
	}
	/**
	 * Converts the loanInfo, which is normally used on submission, into a data structure meant for Applicant Blocking Logic (ABR).
	 * This also only pulls in the information needed for each page. ABR should only work with data on the page the user is on
	 * and the pages before that.
	 * @param {string} pageId
	 * @param {Object} loanInfo
	 * @param {Object} formValues
	 */
    function collectFormValueData(pageId, loanInfo, formValues) {
    	switch (pageId) {
    		case "he1":
    			if ($("#txtLocationPoolCode").length > 0) {
    				formValues["locationPool"] = ZIPCODEPOOLLIST[$("#txtLocationPoolCode").val()];
    			} else if ($("#he_txtLocationPoolCode").length > 0) {
    				formValues["locationPool"] = HEPRODUCTS.ZipPoolIds[$("#he_txtLocationPoolCode").val()];
    			}
			    formValues["homeEquityReason"] = [loanInfo.LoanReason];
    			formValues["homeEquityPurpose"] = [loanInfo.LoanPurpose];
    			formValues["amountRequested"] = loanInfo.RequestedLoanAmount;
    			formValues["isLineOfCredit"] = (getLoanPurpose() == "LINE OF CREDIT");
				formValues["occupyStatus"] = [loanInfo.PropertyOccupancyType];

				collectCustomQuestionAnswers("Application", "LoanPage", null, loanInfo, formValues);
				collectCustomQuestionAnswers("Applicant", "LoanPage", null, loanInfo, formValues);

    			break;
    		case "he2":
    			formValues["homeEquityProperty"] = [loanInfo.PropertyType];
    			formValues["propertyState"] = [loanInfo.PropertyState];
                var _dob = moment(loanInfo.DOB, "MM-DD-YYYY");
                if (Common.IsValidDate(loanInfo.DOB)) {
    				formValues["age"] = moment().diff(_dob, "years", false);
    			}
    			formValues["citizenship"] = loanInfo.CitizenshipStatus;
    			formValues["employeeOfLender"] = loanInfo.EmployeeOfLender;
    			formValues["employmentLength"] = (parseInt(loanInfo.txtEmployedDuration_year) || 0) + (parseInt(loanInfo.txtEmployedDuration_month) || 0) / 12;
    			formValues["employmentStatus"] = loanInfo.EmploymentStatus;
    			formValues["isJoint"] = (loanInfo.HasCoApp=="Y");
    			formValues["memberNumber"] = loanInfo.MemberNumber;
    			formValues["occupancyLength"] = loanInfo.LiveMonths / 12;
    			formValues["occupancyStatus"] = loanInfo.OccupyingLocation;

				collectCustomQuestionAnswers("Applicant", "ApplicantPage", "", loanInfo, formValues);

    			break;
    		case "he6":
    			var _dob = moment(loanInfo.co_DOB, "MM-DD-YYYY");
                if (Common.IsValidDate(loanInfo.co_DOB)) {
    				formValues["age"] = moment().diff(_dob, "years", false);
    			}
    			formValues["citizenship"] = loanInfo.co_CitizenshipStatus;
    			formValues["employeeOfLender"] = loanInfo.co_EmployeeOfLender;
    			formValues["employmentLength"] = (parseInt(loanInfo.co_txtEmployedDuration_year) || 0) + (parseInt(loanInfo.co_txtEmployedDuration_month) || 0) / 12;
    			formValues["employmentStatus"] = loanInfo.co_EmploymentStatus;
    			formValues["memberNumber"] = loanInfo.co_MemberNumber;
    			formValues["occupancyLength"] = loanInfo.co_LiveMonths / 12;
    			formValues["occupancyStatus"] = loanInfo.co_OccupyingLocation;

				collectCustomQuestionAnswers("Applicant", "ApplicantPage", "co_", loanInfo, formValues);

    			break;
    		case "pagesubmit":

				collectCustomQuestionAnswers("Application", "ReviewPage", null, loanInfo, formValues);
				collectCustomQuestionAnswers("Applicant", "ReviewPage", null, loanInfo, formValues);

    			break;
    		default:
    			break;
    	}
    	return formValues;
    }

    function getHomeEquityLoanInfo(disagree_check) {
        var appInfo = new Object();
        appInfo.PlatformSource = $('#hdPlatformSource').val();
        appInfo.LenderRef = $('#hfLenderRef').val();
        appInfo.Task = "SubmitLoan";
        //appInfo.MemberProtectionPlan = '';
        if (disagree_check) {
            appInfo.isDisagreeSelect = "Y";
        } else {
            appInfo.isDisagreeSelect = "N";
        }
        appInfo.depositAmount = 0;
        appInfo.IsComboMode = $("#hdIsComboMode").val();
        appInfo.HasSSOIDA = $('#hdHasSSOIDA').val();
        if (appInfo.IsComboMode == "Y") {
            appInfo.XABranchID = getXAComboBranchId();
            appInfo.ProceedXAAnyway = $("#hdProceedMembership").val();
            setFOM(appInfo);
            if ($("#divSelectedProductPool").length === 1 && $("#divSelectedProductPool").parent().hasClass("hidden") == false) {
                appInfo.SelectedProducts = JSON.stringify(loanProductFactory.collectProductSelectionData());
            }
            if ($("#divCcFundingOptions").length === 1) {
                appInfo.depositAmount = Common.GetFloatFromMoney($('#txtFundingDeposit').val());
                FS1.loadValueToData();
                appInfo.rawFundingSource = JSON.stringify(FS1.Data);
            }
        }

        if (getDisclosures() != "") {
            appInfo.Disclosure = getDisclosures();
        }
        if ($("#txtLocationPoolCode").length > 0) {
        	appInfo.SelectedLocationPool = JSON.stringify(ZIPCODEPOOLLIST[$("#txtLocationPoolCode").val()]);
        } else if ($("#he_txtLocationPoolCode").length > 0) {
        	appInfo.SelectedLocationPool = JSON.stringify(HEPRODUCTS.ZipPoolIds[$("#he_txtLocationPoolCode").val()]);
        }
        appInfo.BranchID = getBranchId();
        appInfo.LoanOfficerID = $('#hfLoanOfficerID').val();
        appInfo.ReferralSource = $('#hfReferralSource').val();
        appInfo.LoanPurpose = $("#hdLoanPurpose").val();

        appInfo.LoanTerm = getLoanTerm();

        appInfo.RequestedLoanAmount = Common.GetFloatFromMoney($('#txtLoanRequestAmount').val());
        appInfo.LoanReason = $("#ddlLoanReason option:selected").val();
        appInfo.EstimatedPropertyValue = Common.GetFloatFromMoney($('#txtEstimatedPropertyValue').val());
        appInfo.FirstMortgageBalance = Common.GetFloatFromMoney($('#txtFirstMortgageBalance').val());
        appInfo.SecondMortgageBalance = Common.GetFloatFromMoney($('#txtSecondMortgageBalance').val());
        appInfo.PropertyOccupancyType = $('#ddlIntendOccupyProperty option:selected').val();
        var proofOfIncome = $('#ddlProofOfIncome option:selected').val();
        if (!hasProofOfIncome()) {
            proofOfIncome = "";
        }
        appInfo.DocType = proofOfIncome;
        //get loan ida
        appInfo.idaMethodType = $('#hdIdaMethodType').val();
        // Custom Ansers
		appInfo.CustomAnswers = JSON.stringify(
			loanpage_getAllCustomQuestionAnswers()
				.concat(getAllCustomQuestionAnswers())
				.concat(co_getAllCustomQuestionAnswers())
				.concat(reviewpage_getAllCustomQuestionAnswers()));
		if (typeof reviewpage_getAllUrlParaCustomQuestionAnswers == "function") {
			var UrlParaCustomQuestions = reviewpage_getAllUrlParaCustomQuestionAnswers();
            //add UrlParamCustomQuestionAndAnswer if it exist                  
            if (UrlParaCustomQuestions.length > 0) {
                appInfo.IsUrlParaCustomQuestion = "Y";
                appInfo.UrlParaCustomQuestionAndAnswers = JSON.stringify(UrlParaCustomQuestions);
            }
        }
            //cross sell comment 
            appInfo.xSellComment = $('#hdCrossSellComment').val();

		    // Main-App
		    // Declarations
		    if ($("#divDeclarationSection").length > 0) {
		    	SetDeclarations(appInfo);
		    	appInfo.Declarations = JSON.stringify(appInfo.Declarations);
		    	appInfo.AdditionalDeclarations = JSON.stringify(appInfo.AdditionalDeclarations);
		    }
		    // Applicant Info
		    SetApplicantInfo(appInfo);
		    // Contact Info
		    SetContactInfo(appInfo);
            // Asset
            SetAssets(appInfo);
		    // Address
		    SetApplicantAddress(appInfo);
		    //Mailing Adddess
		    SetApplicantMailingAddress(appInfo);
		    //Previous Address
		    SetApplicantPreviousAddress(appInfo);

            //reference information
	    if ($("#hdEnableReferenceInformation").val() == "Y") {
		    setReferenceInfo(appInfo);
	    }
		    // appInfo.OtherLoanPrincipleAndInterest = Common.GetFloatFromMoney($('#txtOtherLoanInterest').val());
		    appInfo.PropertyType = $('#ddlTypeOfProperty option:selected').val();
		    if ($("#hdIsShowPropertyAddress").val() === "Y") {
			    appInfo.PropertyAddress = $('#txtPropertyAddress').val();
			    appInfo.PropertyCity = $('#txtPropertyCity').val();
			    appInfo.PropertyZip = $('#txtPropertyZip').val();
			    appInfo.PropertyState = $('#ddlPropertyState option:selected').val();
		    } else {
			    appInfo.PropertyAddress = appInfo.AddressStreet;
			    appInfo.PropertyCity = appInfo.AddressCity;
			    appInfo.PropertyZip = appInfo.AddressZip;
			    appInfo.PropertyState = appInfo.AddressState;
		    }

		    if ($("#hdEnableIDSection").val() === "Y") {
			    SetApplicantID(appInfo);
		    }

		    // Financial
		    SetApplicantFinancialInfo(appInfo);
		    if ($("#hdIsShowExpectedMonthlyExpense").val() === "Y") {
			    appInfo.HazardInsurance = Common.GetFloatFromMoney($('#txtHazardInsurance').val());
			    appInfo.PropertyTaxes = Common.GetFloatFromMoney($('#txtRealEstateTaxes').val());
			    appInfo.MortgageInsurance = Common.GetFloatFromMoney($('#txtMortgageInsurance').val());
			    appInfo.AssociationDues = Common.GetFloatFromMoney($('#txtHomeAssiciationDues').val());
		    } else {
			    appInfo.HazardInsurance = 0;
			    appInfo.PropertyTaxes = 0;
			    appInfo.MortgageInsurance = 0;
			    appInfo.AssociationDues = 0;
		    }

	    appInfo.IsLineOfCredit = (getLoanPurpose() == "LINE OF CREDIT");
           //cuna Protection Info
		    
		    if (typeof setProtectionInfo !== "undefined") {
		    	setProtectionInfo(appInfo, appInfo.IsLineOfCredit);
		    }
		
		    // GMI
		    SetGMIInfo(appInfo);

		    var hasCoApp = $("#hdHasCoApplicant").val();
		    //upload document data and info
		    var docUploadInfo = {};
		    if (typeof docUploadObj != "undefined" && docUploadObj != null) {
		    	docUploadObj.setUploadDocument(docUploadInfo);
		    }
		    if (typeof co_docUploadObj != "undefined" && co_docUploadObj != null) {
		    	co_docUploadObj.setUploadDocument(docUploadInfo);
		    }
		    if (docUploadInfo.hasOwnProperty("sDocArray") && docUploadInfo.hasOwnProperty("sDocInfoArray")) {
		    	appInfo.Image = JSON.stringify(docUploadInfo.sDocArray);
		    	appInfo.UploadDocInfor = JSON.stringify(docUploadInfo.sDocInfoArray);
		    }
		    if (window.ASI) {
		    	ASI.FACTORY.setApplicantAdditionalInfo(appInfo);
		    }
		    appInfo.HasCoApp = hasCoApp;
		    if (appInfo.HasCoApp == 'Y') {
			    // Co-App
			    // Co-Applicant Info
			    co_SetApplicantInfo(appInfo);
			    // Contact Info
			    co_SetContactInfo(appInfo);
                // Co-Assets
                co_SetAssets(appInfo);
			    // Co-Address
			    co_SetApplicantAddress(appInfo);

			    //Co-Previous Address
			    co_SetApplicantPreviousAddress(appInfo);
			    //Mailing Adddess
			    co_SetApplicantMailingAddress(appInfo);
			    // GMI
			    co_SetGMIInfo(appInfo);

		        //co-reference information
			    if ($("#hdEnableReferenceInformationForCoApp").val() == "Y") {
			    co_setReferenceInfo(appInfo);
			    }

			    //Identification
				if ($("#hdEnableIDSectionForCoApp").val() === "Y") {
					co_SetApplicantID(appInfo);
				}

			    // Co-Financial
			    co_SetApplicantFinancialInfo(appInfo);
			    // Co-Declarations
			    if ($("#co_divDeclarationSection").length > 0) {
			    	co_SetDeclarations(appInfo);
			    	appInfo.co_Declarations = JSON.stringify(appInfo.co_Declarations);
			    	appInfo.co_AdditionalDeclarations = JSON.stringify(appInfo.co_AdditionalDeclarations);
			    }
		    }

	    return appInfo;
    }
    HE.FACTORY.getHomeEquityLoanInfo = getHomeEquityLoanInfo;
    //be called right after page #vl1 show to inital and rebind event for elements on page #vl1
    HE.FACTORY.he1Init = function () {
        $("#btnHasCoApplicant").off("click").on("click", toggleHasCoApplicant);
        bindEventLoanPurposeOption();

        //enable sumbit buttons
        if ($('#pagesubmit .div-continue-button').hasClass('ui-disabled')) {
            $('#pagesubmit .div-continue-button').removeClass('ui-disabled');
        }
        if ($('#walletQuestions .div-continue-button').hasClass('ui-disabled')) {
            $('#walletQuestions .div-continue-button').removeClass('ui-disabled');
        }
        if ($('#co_walletQuestions .div-continue-button').hasClass('ui-disabled')) {
            $('#co_walletQuestions .div-continue-button').removeClass('ui-disabled');
        }
        pushToPagePaths("he1");
    };
    HE.FACTORY.he2Init = function (isCoApp) {
        var currPrefillData = null;
        var coPrefix = "";
    	if (isCoApp) {
    	    currPrefillData = COPREFILLDATA;
    	    coPrefix = "co_";
    	} else {
    	    currPrefillData = PREFILLDATA;
    	}

    	if (currPrefillData != null) {
    	    $.each(currPrefillData, function (id, value) {
    	        var $ele = $("#" + id);
    	        if ($ele.is("input") && ($ele.attr("type") === "text" || $ele.attr("type") === "tel" || $ele.attr("type") === "password"||$ele.attr("type") === "email")) {
    	            $ele.val(value);
    	            //$ele.trigger("blur");
    	        } else if ($ele.is("select")) {
    	            if (id.indexOf('preferredContactMethod') > -1) {
    	                value = mappingPreferredContactValue(value);
    	                $ele.val(value);
    	            } else {
    	                $ele.val(value);
    	            }
    	            $ele.selectmenu("refresh");
    	            $ele.trigger("change");

    	        }
    	    });
   
    	    //show reference info if it exist
    	   if (HE.FACTORY.hasReferenceInfo(coPrefix)) {
    	        $('#' + coPrefix + 'showReferenceInfo').attr('status', 'Y');
    	        $('#' + coPrefix + 'divReferenceInfoPanel').show();
    	   }
    	    //show mailing address if it exist
		    var sMailingStreet = $('#' + coPrefix + 'txtMailingAddress').val();
    	   if (sMailingStreet != undefined && sMailingStreet != "") {
    	       $('#' + coPrefix + 'lnkHasMaillingAddress').attr('status', 'Y');
    	       $('#' + coPrefix + 'divMailingAddressForm').show();
    	   }
    	}
        //reset to ensure prefill data only one time
    	currPrefillData = null;
    };
    HE.FACTORY.hasReferenceInfo = function (coPrefix) {
        var rFirstName=$('#'+coPrefix+'reFirstName').val();
        var rLastName = $('#' + coPrefix + 'reLastName').val();
        var rEmail = $('#' + coPrefix + 'refEmail').val();
        var rPhone = $('#' + coPrefix + 'refPhone').val();
        var rRelationship = $('#' + coPrefix + 'refRelationship').val();
        if ((rFirstName != undefined && rFirstName != "") || (rLastName != undefined && rLastName != "") || (rEmail != undefined && rEmail != "") || (rPhone != undefined && rPhone != "")) {
            return true;
        }
        return false;
    };
    HE.FACTORY.pageSubmitInit = function () {
        //apply this logic for combo mode only
        if ($("#hdIsComboMode").val() === "Y") {
            $("a[data-command='proceed-membership']").off("click").on("click", function () {
                var $self = $(this);
                if ($self.attr("contenteditable") == "true") return false;
                //deactive others
                $("div[data-section='proceed-membership']").find("a[data-command='proceed-membership']").each(function () {
                    $(this).removeClass("active");
                    handledBtnHeaderTheme($(this));
                });
                //set active for current option
                $self.addClass("active");
                $("#hdProceedMembership").val($self.data("key"));

                $("div[data-section='proceed-membership']").trigger("change");
            });

            
            //$("#divCcFundingOptions").find("a[data-value='NOT FUNDING AT THIS TIME']").parent().show();
            $('#txtFundingDeposit').off("change").on("change", function () {
                if (Common.GetFloatFromMoney($(this).val()) === 0) {
                	$("#divCcFundingOptions").find("a[data-value='NOT FUNDING AT THIS TIME']").parent().show();
                } else {
                	$("#divCcFundingOptions").find("a[data-value='NOT FUNDING AT THIS TIME']").parent().hide();
                }
            }).trigger("change");
            if (typeof loanProductFactory != "undefined") {
				if (typeof loanProductFactory.refreshDepositTextFields == "function") {
					loanProductFactory.refreshDepositTextFields();
				}
            }
            
            //FOM membershipfee
            $("#divShowFOM div.btn-header-theme").off("click").on("click", function () {
                var membershipFees = HE.FACTORY.calculateOneTimeMembershipFee($(this).attr('data-fom-name'));
                //update divMembership field
	            $("#divMembershipFee").attr("data-membershipfee", membershipFees);
                $("#divMembershipFee").text(Common.FormatCurrency(membershipFees, true));
                if (typeof loanProductFactory !== "undefined" && loanProductFactory !== null) {
                    $('#txtFundingDeposit').val(Common.FormatCurrency(loanProductFactory.getTotalDeposit(), true));
                }
            });
        }
        pushToPagePaths("pagesubmit");
    };
    HE.FACTORY.calculateOneTimeMembershipFee = function (sFOMName) {
        var membershipFee = 0.0;
        if ($("#hdIsComboMode").val() == "Y" && $("#pagesubmit").find("#divShowFOM").length > 0) {
            membershipFee = parseFloat($("#hdMembershipFee").val());
            if (sFOMName != undefined) {
                if (typeof CUSTOMLISTFOMFEES[sFOMName] === "string") {
                    membershipFee = membershipFee + parseFloat(CUSTOMLISTFOMFEES[sFOMName]);
                }
            }
        }
        return membershipFee;
    };
    HE.FACTORY.pageLastInit = function () {
    	if ($("#divXsellSection").length === 1) {
    		$("a.btn-header-theme", $("#divXsellSection")).on("click", function () {
    			if ($(this).attr("contenteditable") == "true") return false;
    			$.mobile.loading("show", {
    				theme: "a",
    				text: "Loading ... please wait",
    				textonly: true,
    				textVisible: true
    			});
    		});
    	}
    }
    HE.FACTORY.init = function () {
        HE.FACTORY.he1Init();
        toggleLoanTerm();
        toggleReasonableDemoPDFView();
        toggleDisclosure();
        toggleApplicantQuestion();
        toggleCustomQuestion();
        toggleProtectionInfo();
        $("#lnkHasPropertyAddress").off("click").on("click", handleShowHidePropertyAddress);
        //hide divProofOfIncome 
        if (!hasProofOfIncome()) {
            $('#divProofOfIncome').hide();
        }
        registerPropertyAddressValidator();
        registerProvideLoanInfoValidator();

        $("#popEstimatePaymentCalculator").on("popupbeforeposition", function () {
        	var $container = $("#popEstimatePaymentCalculator");
        	$("div.required-field-notation", $container).remove();
        	$("input[data-field='LoanAmount']", $container).val(Common.FormatCurrency(0, true));
        	if (Common.GetFloatFromMoney($.trim($("#txtLoanRequestAmount").val())) > 0) {
        		$("input[data-field='LoanAmount']", $container).val($("#txtLoanRequestAmount").val());
        	}
        	var $loanTerm = $("input[data-field='LoanTerm']", $container);
        	if ($loanTerm.length == 0) {
        		$loanTerm = $("select[data-field='LoanTerm']", $container);
        	} else {
        		$loanTerm.val("1"); //set default value for textbox
        	}
        	if (getLoanTerm() !== "") {
        		$loanTerm.val(getLoanTerm());
        		if ($loanTerm.is("select")) {
        			$loanTerm.selectmenu('refresh');
        		}
        	}

            //deprecate bc no longer need Apply btn in calc popup
        	//$("a.div-continue-button", "#popEstimatePaymentCalculator").off("click").on("click", function () {
        	//	var loanAmountStr = $("input[data-field='LoanAmount']", $container).val();
        	//	$("#txtLoanRequestAmount").val(loanAmountStr);
        	//	$.lpqValidate.hideValidation("#txtLoanRequestAmount");
        	//	if ($("#txtLoanTerm").length > 0) {
        	//		$("#txtLoanTerm").val($("input[data-field='LoanTerm']", $container).val());
        	//		$.lpqValidate.hideValidation("#txtLoanTerm");
        	//	} else {
        	//		$("#ddlLoanTerm").val($("select[data-field='LoanTerm']", $container).val()).selectmenu('refresh');
        	//		$.lpqValidate.hideValidation("#ddlLoanTerm");
        	//	}
        	//	var estAmount = Common.FormatCurrency(calculateEstimatePayment(Common.GetFloatFromMoney(loanAmountStr), Common.GetFloatFromMoney(getLoanTerm()), Common.GetFloatFromMoney($("#spInterestRate").data("value"))), true);
        	//	$("span.est-payment-value", $("#divEstimatePayment")).text(estAmount);
        	//	$("#popEstimatePaymentCalculator").popup("close");
        	//});
        	bindEstimatePayment();
        });
        $("select", "#popEstimatePaymentCalculator").on("change", bindEstimatePayment);
        $("input", "#popEstimatePaymentCalculator").on("blur", bindEstimatePayment);
        $("input", "#popEstimatePaymentCalculator").on("keyup paste", function () {
        	setTimeout(bindEstimatePayment, 400);
        });
        $("#ddlLoanTerm").on("change", fillEstimatePayment);
        $("#txtLoanTerm, #txtLoanRequestAmount").on("blur", fillEstimatePayment);
        $("#txtLoanTerm, #txtLoanRequestAmount").on("keyup paste", function () {
        	setTimeout(fillEstimatePayment, 400);
        });
        function fillEstimatePayment() {
        	var $loanAmount = $("#txtLoanRequestAmount");
        	var $loanTerm = $("#txtLoanTerm");
        	var $container = $("#divEstimatePayment");
        	if ($loanTerm.length == 0) {
        		$loanTerm = $("#ddlLoanTerm");
        	}
        	var loanAmount = Common.GetFloatFromMoney($loanAmount.val());
        	var loanTerm = parseFloat($loanTerm.val());
        	var rate = parseFloat($("#spInterestRate").data("value"));
        	var estPayment = 0;
        	if (loanAmount > 0 && loanTerm > 0) {
        		estPayment = calculateEstimatePayment(loanAmount, loanTerm, rate);
        	}
        	$("span.est-payment-value", $container).text(Common.FormatCurrency(estPayment, true));
        }
        function calculateEstimatePayment(loanAmount, loanTerm, rate) {
        	return (loanAmount * (rate / 12) / 100) / (1 - Math.pow((1 + (rate / 12) / 100), -1 * loanTerm));
        }
        function bindEstimatePayment() {
        	var $container = $("#popEstimatePaymentCalculator");
        	var $loanAmount = $("input[data-field='LoanAmount']", $container);
        	var loanAmount = Common.GetFloatFromMoney($loanAmount.val());
        	var $loanTerm = $("input[data-field='LoanTerm']", $container);
        	if ($loanTerm.length == 0) {
        		$loanTerm = $("select[data-field='LoanTerm']", $container);
        	}
        	var loanTerm = 1;
        	if ($loanTerm.length > 0 && parseFloat($loanTerm.val()) > 0) {
        		loanTerm = parseFloat($loanTerm.val());
        	} else {
        		$loanTerm.val(1);
        		if ($loanTerm.is("select")) {
        			$loanTerm.selectmenu('refresh');
        		}
        	}
        	var interestRate = $("input[data-field='InterestRate']", $container).val().replace("%", "");
        	var estAmount = Common.FormatCurrency(calculateEstimatePayment(loanAmount, loanTerm, interestRate), true);
        	$("[data-field='EstimatePayment']", $container).text(estAmount);
        }
    };
    
    //HE.FACTORY.validateUploadDocs = function (element, isCoApp) {
    //    var currElement = $(element);
    //    var strMessage = "";
    //    if (typeof isCoApp == "undefined" || isCoApp === false) {
    //        strMessage += validateUploadDocument();
    //    } else {
    //        strMessage += co_validateUploadDocument();
    //    }
    //    if (strMessage != '') {
    //        $('#txtErrorMessage').html(strMessage);
    //        currElement.next().trigger('click');
    //    } else {
    //        currElement.next().next().trigger('click');
    //    }
    //};
    HE.FACTORY.validateHe1 = function (element) {
    	var currElement = $(element);
    	if (currElement.attr("contenteditable") == "true") return false;
    	var validator = true;
    	var strMessage = "";

    	var loanInfoValidate = $.lpqValidate("ValidateProvideLoanInfo");
    	var branchValidate = $.lpqValidate("ValidateBranch");
    	if (loanInfoValidate === false || branchValidate === false) {
		    validator = false;
	    }
    	if (loanInfoValidate && branchValidate) {
    	    var loanReason = $("#ddlLoanReason").val().toUpperCase();
    	    var isHELOC = getLoanPurpose().indexOf('LINE OF CREDIT') > -1 || getLoanPurpose().indexOf('LOC') > -1;
    	    //REg C, assume Reg DOESN'T prohibit FI from showing GMI section for a few corner case and backend will filter out the not required HMDA, case#249376
            //some lenders are not required to show HDMIW
    	    if (!g_IsHideGMI && loanReason != undefined && isHELOC == false) {
    		    $('.div_gmi_section').css('display', 'block');
    	        //REG B, The Reg B part could be ignored as most still don't want to collect that if they don't report HMDA on HELOCs
    		} else if (!g_IsHideGMI_HELOC && loanReason != undefined && isHELOC == true ) {
    		    $('.div_gmi_section').css('display', 'block');
    		   
    		} else {
    			$('.div_gmi_section').css('display', 'none');
    		}
    	}
        //legacy code: no using this
       //if ($("#he1").find("#divShowFOM").length > 0 && $.lpqValidate("ValidateFOM") == false) {
        //	validator = false;
       // }

		// Validate custom questions, which may be moved to the front page
		if ($('#loanpage_divApplicantQuestion').children().length > 0) {
			if ($.lpqValidate("loanpage_ValidateApplicantQuestionsXA") === false) {
				validator = false;
			}
		}
		
        if ($("#he1").find("#divDisclosure").length > 0) {
        	switch (getLoanPurpose()) {
        		case 'LINE OF CREDIT':
        			if (!$.lpqValidate('ValidateDisclosure_disclosuresHELOC')) {
        				validator = false;
        			}
        			strMessage += validateVerifyCode('HELOC');
        			break;
        		case 'HOME EQUITY LOAN':
        			if (!$.lpqValidate('ValidateDisclosure_disclosuresLOC')) {
        				validator = false;
        			}
        			strMessage += validateVerifyCode();
        			break;
        		default:
        			if (!$.lpqValidate('ValidateDisclosure_disclosuresLOC')) {
        				validator = false;
        			}
        			strMessage += validateVerifyCode();
        			break;
        	}
        }
        if (validator === false) {
			if (strMessage !== "") {
				if ($("#divErrorPopup").length > 0) {
					$('#txtErrorPopupMessage').html(strMessage);
					$("#divErrorPopup").popup("open", { "positionTo": "window" });
				} else {
					$('#txtErrorMessage').html(strMessage);
					currElement.next().trigger('click');
				}
			} else {
				Common.ScrollToError();
			}

        } else {
        	if (window.ABR) {
        		var formValues = prepareAbrFormValues("he1");
        		if (ABR.FACTORY.evaluateRules("he1", formValues)) {
        			return false;
        		}
        	}
        	if (window.IST) {
        		IST.FACTORY.carrierIdentification();
        	} else {
        		currElement.next().next().trigger('click');
        	}
			
		}
	};
    HE.FACTORY.validateHe2 = function (element) {
        var currElement = $(element);
        //update hiddenHasCoApplicant
        if (currElement.attr('id') == 'continueWithCoApp') {
            $("#hdHasCoApplicant").val('Y');
        } else {
            $("#hdHasCoApplicant").val('N');
        }
    	if (currElement.attr("contenteditable") == "true") return false;
        //$('#txtErrorMessage').html(''); //useless
        var strMessage = '';
        var validator = true;
        if (ValidateApplicantInfo() == false) {
	        validator = false;
        }
        if (ValidateApplicantContactInfoXA() == false) {
        	validator = false;
        }
        if (ValidateApplicantAddress() == false) {
        	validator = false;
        }
        if (ValidateApplicantPreviousAddress() == false) {
        	validator = false;
        }
        if (ValidateApplicantMailingAddress() == false) {
        	validator = false;
        }

        if (validateAddressOfTheProperty() == false) {
        	validator = false;
        }

        if ($("#hdEnableIDSection").val() === "Y") {
        	if (ValidateApplicantIdXA() == false) {
        		validator = false;
        	}
        }
        if ($("#hdEnableReferenceInformation").val() == "Y") {
        	if (ValidateReferenceInfo() == false) {
        		validator = false;
        	}
        }
        if (ValidateApplicantFinancialInfo() == false) {
        	validator = false;
        }
        if(ValidateGMIInfo() == false) {
            validator = false;
        }

        if (typeof docUploadObj != "undefined" && docUploadObj != null && docUploadObj.validateUploadDocument() == false) {
			validator = false;
		}
        if(ValidateAssets() == false){
            validator = false;
        }
        if ($("#divDeclarationSection").length > 0) {
			if ($.lpqValidate("ValidateDeclaration") === false) {
				validator = false;
			}
            
        }

    	/*deprecated - don't know why do we need this function. It always return true because it allows blank or zero
        if ($("#hdIsShowExpectedMonthlyExpense").val() === "Y") {
            strMessage += validateExpectedMonthlyExpense();
        }*/
		if ($('#divApplicantQuestion').children().length > 0) {
			if ($.lpqValidate("ValidateApplicantQuestionsXA") === false) {
				validator = false;
			}
		}
        var requiredDLMessage = validateRequiredDLScan($('#hdRequiredDLScan').val(), false);
        if (requiredDLMessage != "") {
        	strMessage = requiredDLMessage;
        	validator = false;
        }
        
        if (validator === false) {
        	//for backward compatibility, need to show error popup/modal for some function which is not applied inline error message
        	if (strMessage !== "") {
        		if ($("#divErrorPopup").length > 0) {
        			$('#txtErrorPopupMessage').html(strMessage);
        			$("#divErrorPopup").popup("open", { "positionTo": "window" });
        		} else {
        			$('#txtErrorMessage').html(strMessage);
        			currElement.next().trigger('click');
        		}
        	} else {
        		Common.ScrollToError();
        	}
        } else {
        	if (window.ABR) {
        		var formValues = prepareAbrFormValues("he2");
        		if (ABR.FACTORY.evaluateRules("he2", formValues)) {
        			return false;
        		}
        	}
        	if ($("#hdHasCoApplicant").val() === "Y") {
        		goToNextPage("#he6");
        	} else {
        		if (window.ASI) {
        			var appInfo = {};
        			SetApplicantInfo(appInfo);
        			SetApplicantAddress(appInfo);
        			ASI.FACTORY.runNssWf1(appInfo.AddressState, appInfo.MaritalStatus, goToPageSubmit);
        		} else {
        			goToPageSubmit();
        		}
        	}
        }
        
    };
    HE.FACTORY.validateHe6 = function (element) {
    	var currElement = $(element);
    	if (currElement.attr("contenteditable") == "true") return false;
        $('#txtErrorMessage').html('');
        var strMessage = '';
        var validator = true;
        if (co_ValidateApplicantInfo() == false) {
	        validator = false;
        }
        if (co_ValidateApplicantContactInfoXA() == false) {
        	validator = false;
        }
        if (co_ValidateApplicantAddress() == false) {
        	validator = false;
        }
        if (co_ValidateApplicantPreviousAddress() == false) {
        	validator = false;
        }
        if (co_ValidateApplicantMailingAddress() == false) {
        	validator = false;
        }
        
        if ($("#hdEnableIDSectionForCoApp").val() === "Y") {
        	if (co_ValidateApplicantIdXA() == false) {
        		validator = false;
        	}
        }
        if ($("#hdEnableReferenceInformationForCoApp").val() == "Y") {
        	if (co_ValidateReferenceInfo() == false) {
        		validator = false;
        	}
        }
        if (co_ValidateApplicantFinancialInfo() == false) {
        	validator = false;
        }
        if(co_ValidateGMIInfo() == false) {
            validator = false;
        }
        if (typeof co_docUploadObj != "undefined" && co_docUploadObj != null && co_docUploadObj.validateUploadDocument() == false) {
        	validator = false;
        }
        if(co_ValidateAssets() == false){
            validators = false;
        }
        if ($("#co_divDeclarationSection").length > 0) {
        	if ($.lpqValidate("co_ValidateDeclaration") === false) {
        		validator = false;
        	}
        }
		if ($('#co_divApplicantQuestion').children().length > 0) {
			if ($.lpqValidate("co_ValidateApplicantQuestionsXA") === false) {
				validator = false;
			}
		}
        var requiredDLMessage = validateRequiredDLScan($('#hdRequiredDLScan').val(), true);
        if (requiredDLMessage != "") {
        	strMessage = requiredDLMessage;
	        validator = false;
        }
        if (validator === false) {
        	//for backward compatibility, need to show error popup/modal for some function which is not applied inline error message
        	if (strMessage !== "") {
        		if ($("#divErrorPopup").length > 0) {
        			$('#txtErrorPopupMessage').html(strMessage);
        			$("#divErrorPopup").popup("open", { "positionTo": "window" });
        		} else {
        			$('#txtErrorMessage').html(strMessage);
        			currElement.next().trigger('click');
        		}
        	} else {
        		Common.ScrollToError();
        	}
        } else {
        	if (window.ABR) {
        		var formValues = prepareAbrFormValues("he6");
        		if (ABR.FACTORY.evaluateRules("he6", formValues)) {
        			return false;
        		}
        	}
        	if (window.ASI) {
        		var appInfo = {};
        		SetApplicantInfo(appInfo);
        		SetApplicantAddress(appInfo);
        		co_SetApplicantInfo(appInfo);
        		co_SetApplicantAddress(appInfo);
        		ASI.FACTORY.runNssWf2(appInfo.AddressState, appInfo.MaritalStatus, appInfo.co_RelationshipToPrimary, appInfo.co_AddressState, appInfo.co_MaritalStatus, goToPageSubmit);
        	} else {
        		goToPageSubmit();
        	}
        }
    };
    HE.FACTORY.validatePageSubmit = function (element) {
    	if ($(element).attr("contenteditable") == "true") return false;
        var currElement = $(element);
        var strMessage = "";
        var validator = true;
    			
        if ($("#hdIsComboMode").val() === "Y") {
            if (!$('#div_fom_section').hasClass("hidden") && $("#pagesubmit").find("#divShowFOM").length > 0 && $.lpqValidate("ValidateFOM") == false) {
        		validator = false;
        	}
            if ($.lpqValidate("ValidateProceedMembership") == false) {
                validator = false;
            }
            if ($.lpqValidate("ValidateProductSelection") === false) {
            	validator = false;
            }
            if (validateFS1() == false) {
            	validator = false;
            }
            //strMessage += validateApplicantQuestions();
		}
        

	    var selectedLoanPurpose = getLoanPurpose();
        if ($("#pagesubmit").find("#divDisclosure").length > 0){
        	switch (selectedLoanPurpose) {
				case 'LINE OF CREDIT':
					if (!$.lpqValidate('ValidateDisclosure_disclosuresHELOC')) {
						validator = false;
					}
					strMessage += validateVerifyCode('HELOC');
					break;
				case 'HOME EQUITY LOAN':
					if (!$.lpqValidate('ValidateDisclosure_disclosuresLOC')) {
						validator = false;
					}
					strMessage += validateVerifyCode();
					break;
				default:
					if (!$.lpqValidate('ValidateDisclosure_disclosuresLOC')) {
						validator = false;
					}
					strMessage += validateVerifyCode();
					break;
			}
		}

		// Validate custom questions
		if ($('#reviewpage_divApplicantQuestion').children().length > 0) {
			if ($.lpqValidate("reviewpage_ValidateApplicantQuestionsXA") === false) {
				validator = false;
			}
		}

        //validate the view loan info panel, if the panel is empty display error message when clicking submit button
        //don't need to validate all the view panels because if the app is cross sell then it only has loan info and review pages 
        if ($("#reviewPanel").find(".ViewHomeEquityLoanInfo").length > 0 && $.lpqValidate("ValidateViewHomeEquityLoanInfo") == false) {
            validator = false;
        }
    	//refine error message to remove duplicate item
        var arrErr = strMessage.split("<br/>");
        var refinedErr = [];
        $.each(arrErr, function (i, el) {
        	el = $.trim(el);
        	if (el !== "" && $.inArray(el, refinedErr) === -1) refinedErr.push(el);
        });
        if (refinedErr.length > 0) {
        	validator = false;
        }
        if (validator === false) {
        	if (refinedErr.length > 0) {
        		if ($("#divErrorPopup").length > 0) {
        			$('#txtErrorPopupMessage').html(refinedErr.join("<br/>"));
        			$("#divErrorPopup").popup("open", { "positionTo": "window" });
        		} else {
        			$('#txtErrorMessage').html(refinedErr.join("<br/>"));
        			currElement.next().trigger('click');
        		}
        	} else {
        		Common.ScrollToError();
        	}
        } else {
        	if (window.ABR) {
        		var formValues = prepareAbrFormValues("pagesubmit");
        		if (ABR.FACTORY.evaluateRules("pagesubmit", formValues)) {
        			return false;
        		}
        	}
            saveHomeEquityLoanInfo();        
        }
    };
    HE.FACTORY.onClickReturnToMainPage = function (e) {   
        saveCancelHomeEquityLoanInfo();      
        clearForms(); //clear data when the page is redicted to the main page
        //return to 
        var sDir = $('#hdRedirectURL').val();
        if (sDir != '') {
            window.location.href = sDir;
        }
	    e.preventDefault();
    };
    HE.FACTORY.registerViewHomeEquityLoanInfoValidator = function () {
        $('#reviewPanel').observer({
            validators: [
                function (partial) {
                    //second loan app is not available in home equity loan, so do the view applicant information validation 
                    //instead of view home equity loan info validation in case some lenders do not have home equity loan info
                    if ($('#reviewPanel .ViewAcountInfo').children().find("div").length == 0) {
                        return "Please complete all required fields. ";
                    }
                    return "";
                }
            ],
            validateOnBlur: false,
            group: "ValidateViewHomeEquityLoanInfo"
        });
    };
	function registerPropertyAddressValidator() {
		$('#ddlTypeOfProperty').observer({
			validators: [
				function (partial) {
					if (!Common.ValidateText($(this).val())) {
						return "Property Type is required";
					}
					return "";
				}
			],
			validateOnBlur: true,
			group: "ValidatePropertyType"
		});
		$('#txtPropertyAddress').observer({
			validators: [
				function (partial) {
					if (Common.ValidateText($(this).val()) == false) {
						return "Property Address is required";
					} else if (/^[\sa-zA-Z0-9\.'#/-]+$/.test($(this).val()) == false) {
						return 'Invalid Address was entered.  Allowed characters: letters A-Z, numbers, hyphens, periods, forwardslashes, dashes, number signs, and apostrophes.';
					}
					else
					{
						if (isPOBox($(this).val())) {
							return "Property Address is not accepted. Please enter a different address";
						}
					}
					return "";
				}
			],
			validateOnBlur: true,
			group: "ValidatePropertyAddress"
		});
		$('#txtPropertyCity').observer({
			validators: [
				function (partial) {
					if (Common.ValidateText($(this).val()) == false) {
						return "Property City is required";
					} else if (/^[\sa-zA-Z0-9\.'#/-]+$/.test($(this).val()) == false) {
						return 'Invalid City was entered.  Allowed characters: letters A-Z, numbers, hyphens, periods, forwardslashes, dashes, number signs, and apostrophes.';
					}
					return "";
				}
			],
			validateOnBlur: true,
			group: "ValidatePropertyAddress"
		});
		$('#txtPropertyZip').observer({
			validators: [
				function (partial) {
					if (Common.ValidateText($(this).val()) == false) {
						return "Property Zip is required";
					}
					return "";
				}
			],
			validateOnBlur: true,
			group: "ValidatePropertyAddress"
		});
		$('#ddlPropertyState').observer({
			validators: [
				function (partial) {
					var state = $.trim($(this).val());
					if (Common.ValidateText(state) == false) {
						return "Property State is required";
					}
					if (g_he_property_states.length == 0) return "";
					if (g_he_property_states.indexOf(state.toLowerCase()) < 0) {
						return 'We currently do not serve your area.  Please contact us at ' + g_lenderPhone + ' if you have any questions';
					}
					return "";
				}
			],
			validateOnBlur: true,
			group: "ValidatePropertyAddress"
		});
	}

	function registerProvideLoanInfoValidator() {
		$('#txtLoanRequestAmount').observer({
			validators: [
				function (partial) {
					if (Common.ValidateText($(this).val()) == false) {
					    return "Amount Requested is required";
					}
					return "";
				}
			],
			validateOnBlur: true,
			group: "ValidateProvideLoanInfo"
		});
		$('#txtLoanTerm').observer({
			validators: [
				function (partial) {
					if (getLoanPurpose() != "LINE OF CREDIT") {
						if (getLoanTerm() == '0' || getLoanTerm() == '') {
							return 'Loan Term is required';
						}
					}
					return "";
				}
			],
			validateOnBlur: true,
			group: "ValidateProvideLoanInfo"
		});
		$('#ddlLoanTerm').observer({
			validators: [
				function (partial) {
					if (getLoanPurpose() != "LINE OF CREDIT") {
						if (getLoanTerm() == '0' || getLoanTerm() == '') {
							return 'Loan Term is required';
						}
					}
					return "";
				}
			],
			validateOnBlur: true,
			group: "ValidateProvideLoanInfo"
		});
		$('#ddlLoanReason').observer({
			validators: [
				function (partial) {
					if (!Common.ValidateText($(this).val())) {
						return 'Loan Reason is required';
					}
					return "";
				}
			],
			validateOnBlur: true,
			group: "ValidateProvideLoanInfo"
		});
		$('#txtEstimatedPropertyValue').observer({
			validators: [
				function (partial) {
					if (!Common.ValidateTextNonZero($(this).val())) {
						return "Estimated Property Value is required";
					}
					return "";
				}
			],
			validateOnBlur: true,
			group: "ValidateProvideLoanInfo"
		});
		$('#txtFirstMortgageBalance').observer({
			validators: [
				function (partial) {
					if (!Common.ValidateTextAllowZero($(this).val())) {
						return "Current Mortgage Balance is required";
					}
					return "";
				}
			],
			validateOnBlur: true,
			group: "ValidateProvideLoanInfo"
		});
		$('#ddlIntendOccupyProperty').observer({
			validators: [
				function (partial) {
					if (!Common.ValidateText($(this).val())) {
						return "Select the answer for: How do you intend to occupy the property?";
					}
					return "";
				}
			],
			validateOnBlur: true,
			group: "ValidateProvideLoanInfo"
		});
		//$('#ddlBranchName').observer({
		//	validators: [
		//		function (partial) {
		//			if ($('#hdVisibleBranch').val() != "Y" || $('#hdRequiredBranch').val() == "N") { //skip validation
		//				return "";
		//			}
		//			if ($('#ddlBranchName').find('option').length > 0) {
		//				if (!Common.ValidateText($('#ddlBranchName option:selected').val())) {
		//					return "Please select Branch Location";
		//				}
		//			}
		//			return "";
		//		}
		//	],
		//	validateOnBlur: true,
		//	group: "ValidateProvideLoanInfo"
		//});
		$('#divLoanPurpose').observer({
			validators: [
				function (partial) {
					var self = $(this);
					if (self.is(":hidden") == true) return "";
					if (!Common.ValidateText($("#hdLoanPurpose").val())) {
						return "Please select Loan Purpose";
					}
					return "";
				}
			],
			validateOnBlur: false,
			group: "ValidateProvideLoanInfo"
		});
	}
	
	function saveCancelHomeEquityLoanInfo() {
	    if ($('#hdEnableDisagree').val() != "Y") {
	        return false;
	    }
	    var loanInfo = getHomeEquityLoanInfo(true);
	    //if  data is not available then don't save data
	    if (loanInfo.SSN == "" || loanInfo.DOB == "" || loanInfo.EmailAddr == "") {
	        return false;
	    }
	    loanInfo = attachGlobalVarialble(loanInfo);
        $.ajax({
            type: 'POST',
            url: 'CallBack.aspx',
            data: loanInfo,
            dataType: 'html',
            success: function (response) {
                //submit sucessfull so clear
                clearForms();
            },
        });
        $.mobile.loading('hide'); //hide the ajax message
        return false;
    }
	function goToPageSubmit() {
		renderReviewContent();
		goToNextPage("#pagesubmit");
	}
}(window.HE = window.HE || {}, jQuery));
var docScanObj, co_docScanObj;
$(function () {
    EMPLogicPI = $.extend(true, {}, EMPLogic);
    EMPLogicPI.init('ddlEmploymentStatus', '', 'he2', 'divEmploymentStatusRest');
    $('#he2').on("pageshow", function () {
    	EMPLogicPI.refreshHtml();
    });
    EMPLogicJN = $.extend(true, {}, EMPLogic);
    EMPLogicJN.init('co_ddlEmploymentStatus', 'co_', 'he6', 'co_divEmploymentStatusRest');
    $('#he6').on("pageshow", function () {
    	EMPLogicJN.refreshHtml();
    });
    //primary previous employment
    EMPLogicPI_PREV = $.extend(true, {}, EMPLogic);
    EMPLogicPI_PREV.init('prev_ddlEmploymentStatus', 'prev_', 'he2', 'prev_divEmploymentStatusRest');
    //joint previous employement
    EMPLogicJN_PREV = $.extend(true, {}, EMPLogic);
    EMPLogicJN_PREV.init('co_prev_ddlEmploymentStatus', 'co_prev_', 'he6', 'co_prev_divEmploymentStatusRest');

    HE.FACTORY.init();

    $(document).on("pageshow", function () {
        var curPageId = $.mobile.pageContainer.pagecontainer('getActivePage').attr('id');
        switch (curPageId) {
            case "he1":
                HE.FACTORY.he1Init();
                break;
        	case "he2":
        		pushToPagePaths("he2");
        		$("#hdHasCoApplicant").val("N");//reset
        		HE.FACTORY.he2Init(false); //prefill data for primary
        		break;
        	case "he6":
        		pushToPagePaths("he6");
                HE.FACTORY.he2Init(true); //prefill data for joint
                break;         
            case "pagesubmit":   
                HE.FACTORY.registerViewHomeEquityLoanInfoValidator();
            	HE.FACTORY.pageSubmitInit();
            	break;
        	case "pl_last":
        		HE.FACTORY.pageLastInit();
        		break;
        }   
    });
    //START BLOCK B
    //MOVE FROM HomeEquityLoan
    g_current_full_he_address = getHeAddress();
    var states = $('#hdPropertyStates').val().split('|');
    for (var i = 0; i < states.length; i++) {
        if (states[i] != '') {
            g_he_property_states.push(states[i].toLowerCase());
        }
    }
    $('input#txtPropertyZip').blur(function () {
        $(this).val(Common.GetPosNumber($(this).val(), 5));
    });
    //$('input.money').each(function () {
    //    $(this).val(Common.FormatCurrency($(this).val(), true));
    //});
    //handleRemoveBtnStyle('he4'); //primary applicant
    //handleRemoveBtnStyle('he7'); // for coApplicant
    //END BLOCK B
    //var footerTheme = $('#hdFooterTheme').val();
    //var bgColor = $('.ui-bar-' + footerTheme).css('background-color');
    //var changeColor = changeBackgroundColor(bgColor, .6);
    //var bgTheme = $('#hdBgTheme').val();
    //if (bgTheme != "") {
    //    changeColor = bgColor;
    //}
    //$('body').css("background-color", changeColor);
    $('#div_custom_questions').children('div').first().removeClass('Title');
    handledFontWeightOfDivContent();
    //handle disable disclosures for home loan equity
    $("div[name='disclosuresLOC'] label").click(function () {
        handleDisableDisclosures(this);
    });
    //handle disable disclosures for home equity line of credit
    $("div[name='disclosuresHELOC'] label").click(function () {
        handleDisableDisclosures(this);
    });

    // MODIFIED AND MOVED TO validateWalletQuestions
    ////disable Submit Answers button after clicking submit to avoid click submit multiple times
    //$('input.btnSubmitAnswer').click(function () {
    //    if (isSubmitAnswer) {
    //        $('input.btnSubmitAnswer').attr('disabled', true);
    //    }
    //});
    ////disable Submit Answers button after clicking submit to avoid click submit multiple times
    //$('input.co_btnSubmitAnswer').click(function () {
    //    if (isCoSubmitAnswer) {
    //        $('input.co_btnSubmitAnswer').attr('disabled', true);
    //    }
    //});
    //if (!isSubmitAnswer) { //not success -> keep submit button enable
    //    $('input.btnSubmitAnswer').attr('disabled', false);
    //}
    //if (!isCoSubmitAnswer) { //not success -> keep submit button enable
    //    $('input.co_btnSubmitAnswer').attr('disabled', false);
    //}

    

    handleHiddenLoanPurpose();

    //user click disagree button -->popup confirm message
    $(document).on('click', '#divDisagree a', function (e) {
        var redirectURL = $('#hdRedirectURL').val();
        //don't popup confirm message--> go direct to return to mainpage
        //displayConfirmMessage(redirectURL, isCoApp, onClickReturnToMainPage);
        if (redirectURL == "") { //need to save loan infor 
            HE.FACTORY.onClickReturnToMainPage(e);
        }
        // location.hash = '#decisionPage'; --> location.hash is no working on IE browser
        goToNextPage($('#decisionPage'));
        e.preventDefault();
    });
    $("#popUpCreditDiff, #popUpSavingAmountGuid, #popEstimatePaymentCalculator").on("popupafteropen", function (event, ui) {
    	$("#" + this.id + "-screen").height("");
    });

    handleShowAndHideBranchNames();
    $("div[data-section='proceed-membership']").observer({
        validators: [
            function (partial) {
                if ($("#hdProceedMembership").val() == "") {
                    return "Do you wish to continue opening your membership?";
                }
                return "";
            }
        ],
        validateOnBlur: true,
        group: "ValidateProceedMembership",
        groupType: "complexUI"
    });
	// Fill in the fields with test data
    if (getParaByName("autofill") == "true") {
    	// Fill in the stuff
    	autoFillData();
    }
    //xaCombo location pool
    if ($('#hdHasZipCodePool').val() == 'Y' && $('#hdIsComboMode').val() == 'Y') {
        var $zipCodeElement = $("#txtLocationPoolCode");
        if (typeof loanProductFactory !== "undefined" && loanProductFactory !== null) {
            loanProductFactory.handledLocationPoolProducts($zipCodeElement);
        }
    } else if ($('#hdHasHEZipCodePool').val() == 'Y') {//standard loan location pool 
        handledLocationPoolLoanInfo('he', HEPRODUCTS);
    }
    docScanObj = new LPQDocScan("");
    co_docScanObj = new LPQDocScan("co_");
    //InstaTouch: Call 1 ("Authorization") called on the loading "HE Info" and not after an applicant has clicked the "Continue" button
    if (window.IST) {
        IST.FACTORY.loadCarrierIdentification();
    }
});
function ScanAccept(prefix) {
	if (prefix == "") {
		docScanObj.scanAccept();
		goToNextPage("#he2");
	} else {
		co_docScanObj.scanAccept();
		goToNextPage("#he6");
	}
}
function getUserName() {
	var txtFName = $('#txtFName').val();
	var txtMName = $('#txtMName').val();
	var txtLName = $('#txtLName').val();
	//capitalize the first character
	txtFName = txtFName.replace(txtFName.charAt(0), txtFName.charAt(0).toUpperCase());
	txtMName = txtMName.replace(txtMName.charAt(0), txtMName.charAt(0).toUpperCase());
	txtLName = txtLName.replace(txtLName.charAt(0), txtLName.charAt(0).toUpperCase());
	var userName = "";
	if (txtMName != "")
		userName = txtFName + " " + txtMName + " " + txtLName;
	else
		userName = txtFName + " " + txtLName;
	return userName;
}


function handleHiddenLoanPurpose() {
    var loanPurposeElem = $('#divLoanPurpose a[data-role="button"]');
        //only one loan purpose --> preselected and hide loan purpose
    if (loanPurposeElem.length == 1) {
        loanPurposeElem.trigger('click');
        loanPurposeElem.parent().hide(); //hide loan purpose
        //loanPurposeElem.parent().prev().hide();//hide title: Select a purpose
    }
}

function acceptLaserScanResult(prefix, ele) {
	fillLaserScanResult(prefix, ele);
	$(ele).closest(".scan-result-wrapper").removeClass("open");
	var nextPage = "#he2";
	if (prefix !== "") {
		nextPage = "#he6";
	}
	goToNextPage(nextPage);
}