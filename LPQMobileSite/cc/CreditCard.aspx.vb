﻿Imports LPQMobile.BO
Imports LPQMobile.Utils
Imports System.IO
Imports System.Web.Script.Serialization
Imports System.Xml
Imports System.Activities.Statements
Imports System.Web.Security.AntiXss

Partial Class cc_CreditCard
    Inherits CBasePage

    'Protected _LoanPurposeDropdown As String
    'DISABLE THIS DROPDOWN VIA CHANGE REQUESTS 20160922
    Protected _CreditCardTypeDropdown As String
    'Protected _CreditCardNameDropdown As String
    Protected _CCLoanPurposeDisabled As String
    'CONVERT PURPOSE DROPDOWN TO BUTTON (CHANGE REQUESTS 20160922)
    'Protected _LoanPurposeSection As String
    'Protected _isOnlyCreditCard As String = ""
	Protected _address_key As String = ""
	Protected _declarationList As Dictionary(Of String, String)
	''upload documents: ID, Driver license ...
	'Protected _visibleCustomQuestion As String
	Protected _bgTheme As String
	Protected _hasScanDocumentKey As String
	Protected _SubmitCancelMessage As String
	Protected _LoanPurposeCategory As String = ""
	Protected _CreditCardList As String = ""
	Protected _CreditCardNames As New List(Of Tuple(Of String, String, String, String))
	Protected _CreditCardPurposes As New List(Of Tuple(Of String, String, String, String))
	Protected _LoanPurposeList As New Dictionary(Of String, String)
	Protected _IDCardTypeDropDown As String
	Protected _enableJoint As Boolean = True
	Protected _prerequisiteProduct As New CProduct
	Protected _CreditPullMessage As String = ""
	Protected _HiddenCCType As String = ""
	Protected _CCMaxFunding As String = ""
	Protected _ACHMaxFunding As String = ""
	Protected _PaypalMaxFunding As String = ""
	Protected _ccStopInvalidRoutingNumber As String = ""
	Protected _showProductSelection As Boolean = True
	Protected _SID As String = ""
	Protected _xaXSellComment As String = ""
	Protected _prefillData As Dictionary(Of String, String)
	Protected _coPrefillData As Dictionary(Of String, String)
	Protected _branchOptionsStr As String = ""
	Protected LaserDLScanEnabled As Boolean = False
	Protected LegacyDLScanEnabled As Boolean = False
	Protected LoanSavingItem As SmLoan
	Protected _ccLocationPool As Boolean = False
	Protected _ccZipCodePoolProducts As List(Of DownloadedSettings.CCreditCardProduct)
	Protected _zipCodePoolList As New Dictionary(Of String, List(Of String))
	Protected _zipCodePoolNames As New Dictionary(Of String, String)
	Protected _ComboNewCardLoanPurpose As String
	Protected _selectedCreditCardProduct As DownloadedSettings.CCreditCardProduct
#Region "property for sso"
	Protected ReadOnly Property CardName() As String
		Get
			Return Common.SafeString(AntiXssEncoder.HtmlEncode(Request.Params("CName"), True))
		End Get
	End Property

	Protected ReadOnly Property LoanPurpose() As String
		Get
			Return Common.SafeString(AntiXssEncoder.HtmlEncode(Request.Params("LoanPurpose"), True))
		End Get
	End Property

#End Region

	Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

		log.Info("Credit Card: Page loading")
		Session("StartedDate") = Date.Now	'this is use for keeping track the time when page first load which will be used to compare with submite_date
		ucPageHeader._currentConfig = _CurrentWebsiteConfig
		'ucApplicantAddress.FooterDataTheme = _FooterDataTheme
		'ucCoUploadDocDialog.CurrentWebsiteConfig = _CurrentWebsiteConfig
		'ucCoUploadDocDialog.UploadLoandocPath = _uploadLoandocPath
		'ucUploadDocDialog.CurrentWebsiteConfig = _CurrentWebsiteConfig
		'ucUploadDocDialog.UploadLoandocPath = _uploadLoandocPath
		''driver license scan 
		DriverlicenseScan.CurrentConfig = _CurrentWebsiteConfig
		coAppDriverlicenseScan.CurrentConfig = _CurrentWebsiteConfig
		If Not IsPostBack Then

			If _ClinicID <> "" Xor _WorkerID <> "" Then	 'direct mode, no clinic config
				log.Warn("missing clinicid or workerid")
				Response.Write("both clinicid and workerid are required")
				Return
			End If

			Dim loanPurposes As Dictionary(Of String, String)
			If LoanPurpose <> "" Then  'purpose is in the url querystring para
				Dim dic As New Dictionary(Of String, String)
				dic.Add(LoanPurpose, LoanPurpose)
				loanPurposes = dic
			ElseIf _MerchantID <> "" Then  'using merchantid to load purpose from config
				Dim sXPathMerchant As String = String.Format("MERCHANTS/MERCHANT[@merchant_id='{0}']", _MerchantID)
				Dim oMerchantNode As XmlNode = _CurrentWebsiteConfig._webConfigXML.SelectSingleNode(sXPathMerchant)
				If oMerchantNode Is Nothing Then
					log.Warn("Merchant not found for merchantid: " & _MerchantID)
					Response.Write("invalid merchantid")
					Return
				End If
				Dim sXPathPurpose As String = String.Format("MERCHANTS/MERCHANT[@merchant_id='{0}']/CREDIT_CARD_PURPOSES", _MerchantID)
				loanPurposes = _CurrentWebsiteConfig.GetClinicEnumItems(sXPathPurpose)
				If Not loanPurposes.Any Then
					loanPurposes = _CurrentWebsiteConfig.GetEnumItems("CREDIT_CARD_LOAN_PURPOSES")	'default back to setting in normal loan
				End If
			Else
				loanPurposes = _CurrentWebsiteConfig.GetEnumItems("CREDIT_CARD_LOAN_PURPOSES")
			End If
			_LoanPurposeList = loanPurposes	'.Select(Function(x) x.Value).ToList()			
			'_LoanPurposeDropdown = Common.RenderDropdownlist(loanPurposes, " ")
			'If (loanPurposes.Count = 1) Then ''if one option make it by default
			'	Dim sCCKeys(loanPurposes.Count - 1) As String
			'	Dim sCCValues(loanPurposes.Count - 1) As String
			'	loanPurposes.Keys.CopyTo(sCCKeys, 0)
			'	loanPurposes.Values.CopyTo(sCCValues, 0)
			'	_LoanPurposeDropdown = Common.RenderDropdownlist(sCCKeys, sCCValues, " ", sCCKeys(0))
			'End If
			'CONVERT PURPOSE DROPDOWN TO BUTTONS (CHANGE REQUESTS 20160922)
			'_LoanPurposeSection = BuildLoanPurposeSection(loanPurposes.Count, _LoanPurposeDropdown)
			''get address key
			If Not String.IsNullOrEmpty(_CurrentWebsiteConfig.AddressKey) Then
				_address_key = _CurrentWebsiteConfig.AddressKey
			End If
			''handled credit card name and type
			'Dim parsedCCNamesDic As Dictionary(Of String, String) = Common.getItemsFromConfig(_CurrentWebsiteConfig, "ENUMS/CREDIT_CARD_NAMES/ITEM", "credit_card_name", "credit_card_type")
			_LoanPurposeCategory = New JavaScriptSerializer().Serialize(Common.getItemsFromConfig(_CurrentWebsiteConfig, "ENUMS/CREDIT_CARD_LOAN_PURPOSES/ITEM", "value", "category"))
			_CreditCardPurposes = Common.getItemsFromConfig(_CurrentWebsiteConfig, "ENUMS/CREDIT_CARD_LOAN_PURPOSES/ITEM",
					Function(x)
						Dim value As String = ""
						Dim text As String = ""
						Dim category As String = ""
						If x.Attributes("value") IsNot Nothing Then
							value = Common.SafeString(x.Attributes("value").InnerXml)
						End If
						If x.Attributes("text") IsNot Nothing Then
							text = Common.SafeString(x.Attributes("text").InnerXml)
						End If
						If x.Attributes("category") IsNot Nothing Then
							category = Common.SafeString(x.Attributes("category").InnerXml)
						End If
						Return New Tuple(Of String, String, String, String)(value, text, category, "")
					End Function)
			_ComboNewCardLoanPurpose = getComboNewCardLoanPurpose(_CreditCardPurposes)
			_CreditCardNames = Common.getItemsFromConfig(_CurrentWebsiteConfig, "ENUMS/CREDIT_CARD_NAMES/ITEM",
					Function(x)
						Dim name As String = ""
						Dim type As String = ""
						Dim image As String = ""
						Dim category As String = ""
						If x.Attributes("credit_card_name") IsNot Nothing Then
							name = Common.SafeString(x.Attributes("credit_card_name").InnerXml)
						End If
						If x.Attributes("credit_card_type") IsNot Nothing Then
							type = Common.SafeString(x.Attributes("credit_card_type").InnerXml)
						End If
						If x.Attributes("image_url") IsNot Nothing Then
							image = Common.SafeString(x.Attributes("image_url").InnerXml)
						End If
						If x.Attributes("category") IsNot Nothing Then
							category = Common.SafeString(x.Attributes("category").InnerXml)
						End If
						Return New Tuple(Of String, String, String, String)(name, type, image, category)
					End Function)
			''filter credit card names for combo
			If IsComboMode Then _CreditCardNames = _CreditCardNames.Where(Function(x) x.Item4.ToUpper <> "UPGRADE").ToList()

			Dim tempCardList As New Dictionary(Of String, String)
			For Each item As Tuple(Of String, String, String, String) In _CreditCardNames
				If item.Item1 <> "" Then
					If Not tempCardList.ContainsKey(item.Item1) Then
						tempCardList.Add(item.Item1, item.Item2)
					End If
				End If
			Next
			_CreditCardList = New JavaScriptSerializer().Serialize(tempCardList)
			If Common.SafeGUID(Request.QueryString("sid")) <> Guid.Empty Then
				Dim smBL As New SmBL()
				LoanSavingItem = smBL.GetLoanByID(Common.SafeGUID(Request.QueryString("sid")))
			End If

			'_CreditCardNames = parsedCCNamesDic
			If tempCardList.Count = 0 Then ''no CREDIT_CARD_NAMES in ENUM -->use the original config
				handledRenderingCreditCardName(loanPurposes)
			End If
			If _CreditCardNames.Any() Then _HiddenCCType = "hidden"
			''init credit card type dropdown even there exist credit card names
			Dim ccTypes As Dictionary(Of String, String) = _CurrentWebsiteConfig.GetEnumItems("CREDIT_CARD_TYPES")
			Select Case True
				Case ccTypes.Count = 1
					Dim sCCKeys(ccTypes.Count - 1) As String
					Dim sCCValues(ccTypes.Count - 1) As String
					ccTypes.Keys.CopyTo(sCCKeys, 0)
					ccTypes.Values.CopyTo(sCCValues, 0)
					_CreditCardTypeDropdown = Common.RenderDropdownlist(sCCKeys, sCCValues, " ", sCCKeys(0))
					''make sure credit card information page is not empty before hiding the credit card type case
					If loanPurposes.Count > 0 Or _CreditCardNames.Count > 0 Then _HiddenCCType = "hidden"
				Case ccTypes.Count < 1
					_HiddenCCType = "hidden"
				Case Else
					_CreditCardTypeDropdown = Common.RenderDropdownlist(ccTypes, " ")
			End Select

			'If parsedCCNamesDic.Count = 0 Then ''no CREDIT_CARD_NAMES in ENUM -->use the original config
			'    Dim ccNames As Dictionary(Of String, String) = _CurrentWebsiteConfig.GetEnumItems("../CREDIT_CARD_LOAN/CREDIT_CARD_NAME")
			'    _CreditCardNames = ccNames
			'    'handledRenderingCreditCardName(loanPurposes)
			'Else
			'    Dim ccNames As Dictionary(Of String, String) = getCreditCardNamesDic(parsedCCNamesDic)
			'    _CreditCardNames = ccNames
			'    'convert credit card dropdown to images - change requests 09232016
			'    'If parsedCCNamesDic.Count = 1 Then
			'    '    _CreditCardNameDropdown = Common.RenderDropdownlist(ccNames, "") ''make credit card name by default
			'    'Else
			'    '    _CreditCardNameDropdown = Common.RenderDropdownlist(ccNames, " ")
			'    'End If
			'    Dim oCCSerializer As New JavaScriptSerializer()
			'    _CreditCardList = oCCSerializer.Serialize(parsedCCNamesDic)
			'    'CCTypeDiv.Visible = False ''hide credit card type for new config
			'End If
			''end handled credit card name and type

			Dim employeeOfLenderDropdown As String = ""
			Dim employeeOfLenderType = _CurrentWebsiteConfig.GetEnumItems("EMPLOYEE_OF_LENDER_TYPE")
			If employeeOfLenderType IsNot Nothing AndAlso employeeOfLenderType.Count > 0 Then    'available in  LPQMobileWebsiteConfig
				''get employee of lender type from config
				employeeOfLenderDropdown = Common.RenderDropdownlist(employeeOfLenderType, "")

				ucApplicantInfo.EmployeeOfLenderDropdown = employeeOfLenderDropdown
				ucCoApplicantInfo.EmployeeOfLenderDropdown = employeeOfLenderDropdown

				Dim sLenderName As String = CDownloadedSettings.DownloadLenderServiceConfigInfo(_CurrentWebsiteConfig).LenderName
				ucApplicantInfo.LenderName = sLenderName
				ucCoApplicantInfo.LenderName = sLenderName
			End If

			xaApplicationQuestionLoanPage.Header = HeaderUtils.RenderPageTitle(0, "Please answer the following question(s)", True)
			xaApplicationQuestionReviewPage.Header = HeaderUtils.RenderPageTitle(0, "Please answer question(s) below", True)

			ucApplicantID.MappedShowFieldLoanType = mappedShowFieldLoanType
			ucCoApplicantID.MappedShowFieldLoanType = mappedShowFieldLoanType
			ucApplicantAddress.MappedShowFieldLoanType = mappedShowFieldLoanType
			ucCoApplicantAddress.MappedShowFieldLoanType = mappedShowFieldLoanType
			ucReferenceInfo.MappedShowFieldLoanType = mappedShowFieldLoanType
			ucCoReferenceInfo.MappedShowFieldLoanType = mappedShowFieldLoanType
			ucApplicantInfo.MappedShowFieldLoanType = mappedShowFieldLoanType
            ucCoApplicantInfo.MappedShowFieldLoanType = mappedShowFieldLoanType
            ucAssetInfo.MappedShowFieldLoanType = mappedShowFieldLoanType
            ucCoAssetInfo.MappedShowFieldLoanType = mappedShowFieldLoanType
            ucApplicantInfo.isComboMode = IsComboMode
			ucCoApplicantInfo.isComboMode = IsComboMode
			ucApplicantInfo.InstitutionType = _InstitutionType
			ucCoApplicantInfo.InstitutionType = _InstitutionType

			Dim citizenshipStatusDropdown As String = ""
			Dim hasBlankDefaultCitizenship As Boolean = Common.hasBlankDefaultCitizenShip(_CurrentWebsiteConfig)
			'change the default US Citizen 
			If _CurrentWebsiteConfig.GetEnumItems("CITIZENSHIP_TYPES").Count > 0 Then  'available in  LPQMobileWebsiteConfig
				Dim Citizenships As New Dictionary(Of String, String)
				Citizenships = _CurrentWebsiteConfig.GetEnumItems("CITIZENSHIP_TYPES")
				citizenshipStatusDropdown = Common.DefaultCitizenShipFromConfig(Citizenships, _DefaultCitizenship, _CurrentLenderRef, hasBlankDefaultCitizenship)
			Else
				citizenshipStatusDropdown = Common.DefaultCitizenShip(_DefaultCitizenship, _CurrentLenderRef, hasBlankDefaultCitizenship) ''from hard code
			End If
			'Dim citizenshipStatusDropdown As String = Common.RenderDropdownlist(CEnum.CITIZENSHIP_STATUS, " ", "US CITIZEN")
			ucApplicantInfo.CitizenshipStatusDropdown = citizenshipStatusDropdown
			ucCoApplicantInfo.CitizenshipStatusDropdown = citizenshipStatusDropdown

			ucApplicantInfo.EnableCitizenshipStatus = CheckShowField("divCitizenshipStatus", "", True) Or (IsInMode("777") AndAlso IsInFeature("visibility"))
			ucCoApplicantInfo.EnableCitizenshipStatus = CheckShowField("divCitizenshipStatus", "co_", True) Or (IsInMode("777") AndAlso IsInFeature("visibility"))

			Dim maritalStatusDropdown As String = Common.RenderDropdownlistWithEmpty(CEnum.MARITAL_STATUS, "", "--Please Select--")
			ucApplicantInfo.MaritalStatusDropdown = maritalStatusDropdown
			ucCoApplicantInfo.MaritalStatusDropdown = maritalStatusDropdown
			ucApplicantInfo.EnableMaritalStatus = CheckShowField("divMaritalStatus", "", False) Or (IsInMode("777") AndAlso IsInFeature("visibility"))
			ucCoApplicantInfo.EnableMaritalStatus = CheckShowField("divMaritalStatus", "co_", False) Or (IsInMode("777") AndAlso IsInFeature("visibility"))
			ucApplicantInfo.EnableMemberNumber = CheckShowField("divMemberNumber", "", True) Or (IsInMode("777") AndAlso IsInFeature("visibility"))
			ucCoApplicantInfo.EnableMemberNumber = CheckShowField("divMemberNumber", "co_", True) Or (IsInMode("777") AndAlso IsInFeature("visibility"))
			ucApplicantInfo.EnableMemberShipLength = CheckShowField("divMembershipLength", "", False) Or (IsInMode("777") AndAlso IsInFeature("visibility"))
			ucCoApplicantInfo.EnableMemberShipLength = CheckShowField("divMembershipLength", "co_", False) Or (IsInMode("777") AndAlso IsInFeature("visibility"))

			Dim occupancyStatusesForRequiredHousingPayment = _CurrentWebsiteConfig.GetOccupancyStatusesForRequiredHousingPayment("CREDIT_CARD")

			Dim employmentStatusDropdown As String = Common.RenderDropdownlistWithEmpty(CEnum.EMPLOYMENT_STATUS, "", "--Please Select--")
			Dim liveMonthsDropdown As String = Common.GetMonths()
			Dim liveYearsDropdown As String = Common.GetYears()
			ucFinancialInfo.EmploymentStatusDropdown = employmentStatusDropdown
			ucCoFinancialInfo.EmploymentStatusDropdown = employmentStatusDropdown
			ucFinancialInfo.LiveMonthsDropdown = liveMonthsDropdown
			ucCoFinancialInfo.LiveMonthsDropdown = liveMonthsDropdown
			ucFinancialInfo.LiveYearsDropdown = liveYearsDropdown
			ucCoFinancialInfo.LiveYearsDropdown = liveYearsDropdown
			ucFinancialInfo.OccupancyStatusesForRequiredHousingPayment = occupancyStatusesForRequiredHousingPayment
			ucCoFinancialInfo.OccupancyStatusesForRequiredHousingPayment = occupancyStatusesForRequiredHousingPayment

			Dim occupyingLocationDropdown As String = Common.RenderDropdownlistWithEmpty(CEnum.OCCUPY_LOCATIONS, "", "--Please Select--")
			Dim stateDropdown As String = Common.RenderStateDropdownlistWithEmpty(CEnum.STATES, "", "--Please Select--")
			ucApplicantAddress.OccupyingLocationDropdown = occupyingLocationDropdown
			ucCoApplicantAddress.OccupyingLocationDropdown = occupyingLocationDropdown
			ucApplicantAddress.OccupancyStatusesForRequiredHousingPayment = occupancyStatusesForRequiredHousingPayment
			ucCoApplicantAddress.OccupancyStatusesForRequiredHousingPayment = occupancyStatusesForRequiredHousingPayment
			ucApplicantAddress.StateDropdown = stateDropdown
			ucCoApplicantAddress.StateDropdown = stateDropdown
			ucApplicantID.StateDropdown = stateDropdown
			ucCoApplicantID.StateDropdown = stateDropdown
			ucApplicantAddress.LiveMonthsDropdown = liveMonthsDropdown
			ucCoApplicantAddress.LiveMonthsDropdown = liveMonthsDropdown
			ucApplicantAddress.LiveYearsDropdown = liveYearsDropdown
			ucCoApplicantAddress.LiveYearsDropdown = liveYearsDropdown
			_previous_address_threshold = _CurrentWebsiteConfig.getPreviousThreshold("CREDIT_CARD_LOAN", "previous_address_threshold")
			_previous_employment_threshold = _CurrentWebsiteConfig.getPreviousThreshold("CREDIT_CARD_LOAN", "previous_employment_threshold")
			If EnableIDSection Or EnableIDSection("co_") Then
				ucApplicantID.LoanType = _LoanType
				ucCoApplicantID.LoanType = _LoanType
				ucApplicantID.isComboMode = IsComboMode
				ucCoApplicantID.isComboMode = IsComboMode
				Dim ddlIDCardType As Dictionary(Of String, String)
				If _CurrentWebsiteConfig.GetEnumItems("ID_CARD_TYPES").Count = 0 Then
					ddlIDCardType = CFOMQuestion.DownLoadIDCardTypes(_CurrentWebsiteConfig)
					If (ddlIDCardType.Count = 0) Then  'there is no id_card_type from retrieval list
						ucApplicantID.isExistIDCardType = False
						ucCoApplicantID.isExistIDCardType = False
					Else
						'ID_Card_type dropdown, default driver license
						ucApplicantID.isExistIDCardType = True
						ucCoApplicantID.isExistIDCardType = True
						_IDCardTypeDropDown = Common.RenderDropdownlist(ddlIDCardType, " ", "DRIVERS_LICENSE")
						ucApplicantID.IDCardTypeList = _IDCardTypeDropDown
						ucCoApplicantID.IDCardTypeList = _IDCardTypeDropDown
					End If
				Else
					'ID_Card_type dropdown, default driver license
					ucApplicantID.isExistIDCardType = True
					ucCoApplicantID.isExistIDCardType = True
					ddlIDCardType = _CurrentWebsiteConfig.GetEnumItems("ID_CARD_TYPES")
					_IDCardTypeDropDown = Common.RenderDropdownlist(ddlIDCardType, " ", "DRIVERS_LICENSE")
					ucApplicantID.IDCardTypeList = _IDCardTypeDropDown
					ucCoApplicantID.IDCardTypeList = _IDCardTypeDropDown
				End If
			End If
			If IsComboMode Then
				'load actual prerequisite product name
				Dim currentProdList As List(Of CProduct) = CProduct.GetProductsForLoans(_CurrentWebsiteConfig)
				If currentProdList Is Nothing OrElse currentProdList.Any() = False Then
					_showProductSelection = False
				Else
					Dim allowedProductCodes As List(Of String) = GetAllowedProductCodes("CREDIT_CARD_LOAN")
					If allowedProductCodes IsNot Nothing AndAlso allowedProductCodes.Any() Then
						currentProdList = currentProdList.Where(Function(p) allowedProductCodes.Contains(p.ProductCode)).ToList()
					End If
					If allowedProductCodes IsNot Nothing AndAlso allowedProductCodes.Count = 1 Then
						'hide product selection section when there is only ONE configured product code - this is the same as the current system.
						_showProductSelection = False
						If currentProdList IsNot Nothing And currentProdList.Any() Then
							'TODO: please make sure this linq doesn't modify the original cached object which will be used by other conusmers for other loantype, should do a clone before do linq filtering
							Dim cproduct As CProduct = currentProdList.FirstOrDefault(Function(cp As CProduct) cp.ProductCode = allowedProductCodes(0))
							If cproduct IsNot Nothing Then
								_prerequisiteProduct = cproduct
								'fix for case when mindepoist isnot corretly configure on lender side
								If Common.SafeInteger(_prerequisiteProduct.MinimumDeposit) < 0 Then
									_prerequisiteProduct.MinimumDeposit = 0
								End If
								''Don't display product and funding on combo app when there is only ONE product that has no minimum deposit, no services, and no product questions. Do display the product and funding on account that has minimum deposit, services, or product questions.
								If cproduct.MinimumDeposit > 0 OrElse (cproduct.Services IsNot Nothing AndAlso cproduct.Services.Any()) OrElse (cproduct.CustomQuestions IsNot Nothing AndAlso cproduct.CustomQuestions.Any()) Then
									_showProductSelection = True
								End If
							End If
						End If
					End If
				End If
				If _showProductSelection = True Then
					Dim ucProductSelection As Inc_MainApp_ProductSelection = CType(LoadControl("~/Inc/MainApp/ProductSelection.ascx"), Inc_MainApp_ProductSelection)
					'should show product selection section when there is no configured product code
					'should show product selection section when there are more than one configured product codes - this is similar to XA where configured product codes will  filter  product to show
					Dim productRendering As New CProductRendering(currentProdList)
					Dim currentLenderServiceConfigInfo = CDownloadedSettings.DownloadLenderServiceConfigInfo(_CurrentWebsiteConfig)
					ucProductSelection.ZipCodePoolList = currentLenderServiceConfigInfo.ZipCodePool
					ucProductSelection.ZipCodePoolName = currentLenderServiceConfigInfo.ZipCodePoolName
					ucProductSelection.CurrentLenderRef = _CurrentLenderRef
					ucProductSelection.CurrentProductList = currentProdList
					ucProductSelection.ContentDataTheme = _ContentDataTheme
					ucProductSelection.FooterDataTheme = _FooterDataTheme
					ucProductSelection.HeaderDataTheme = _HeaderDataTheme
					ucProductSelection.HeaderDataTheme2 = _HeaderDataTheme2
					ucProductSelection.WebsiteConfig = _CurrentWebsiteConfig
					ucProductSelection.ProductQuestionsHtml = productRendering.BuildProductQuestions(currentProdList, _textAndroidTel, True)
					plhProductSelection.Controls.Add(ucProductSelection)
				End If
			End If

			''Url parameter custom questions and answers (refer to the design case AP-1966: Enable Functionality of Lead Source Parameters in AP URLs)         
			''to be deprecate
			''verify code, 
			Dim reasonableDemoCode As String = Common.getNodeAttributes(_CurrentWebsiteConfig, "CREDIT_CARD_LOAN/VISIBLE", "reasonable_demonstration_code")
			ucDisclosures.strRenderingVerifyCode = Common.RenderingVerifyCodeField(reasonableDemoCode, _CurrentWebsiteConfig)
			_branchOptionsStr = GetBranches("CC")
			ucDisclosures.MappedShowFieldLoanType = mappedShowFieldLoanType
			ucDisclosures.EnableEmailMeButton = CheckShowField("divEmailMeButton", "", False) Or (IsInMode("777") AndAlso IsInFeature("visibility"))

			Dim checkDisAgreePopup As String = Common.checkDisAgreePopup(_CurrentWebsiteConfig)
			If checkDisAgreePopup Then
				_isDisagreePopup = "Y"
			Else
				_isDisagreePopup = "N"
			End If

			_declarationList = Common.GetActiveDeclarationList(_CurrentWebsiteConfig, "CREDIT_CARD_LOAN")
			ucDeclaration.Visible = False
			ucCoDeclaration.Visible = False
			If _declarationList IsNot Nothing AndAlso _declarationList.Count > 0 Then
				ucDeclaration.DeclarationList = _declarationList
				ucCoDeclaration.DeclarationList = _declarationList
				ucDeclaration.Visible = True
				ucCoDeclaration.Visible = True
			End If
			Dim bIsSSO As Boolean = Not String.IsNullOrEmpty(Common.SafeString(Request.Params("FName")))
			If Not bIsSSO Then ''hide the scan docs page if SSO via MobileApp
				If _CurrentWebsiteConfig.DLBarcodeScan = "Y" Then
					ucApplicantInfo.LaserScandocAvailable = True
					ucCoApplicantInfo.LaserScandocAvailable = True
					LaserDLScanEnabled = True
				ElseIf Not String.IsNullOrEmpty(_CurrentWebsiteConfig.ScanDocumentKey) Then
					ucApplicantInfo.LegacyScandocAvailable = True
					ucCoApplicantInfo.LegacyScandocAvailable = True
					LegacyDLScanEnabled = True
					_hasScanDocumentKey = _CurrentWebsiteConfig.ScanDocumentKey
				End If
			End If


			''if hasUploadDocument goto uploadDocument page, otherwise goto final page
			Dim uploadDocsMessage As String = Common.getUploadDocumentMessage(_CurrentWebsiteConfig)
			Dim docTitleOptions As List(Of CDocumentTitleInfo) = Nothing
			Dim docTitleRequired = True
			If Common.getUploadDocumentEnable(_CurrentWebsiteConfig) AndAlso Not String.IsNullOrEmpty(uploadDocsMessage) Then
				docTitleRequired = Not Common.getNodeAttributes(_CurrentWebsiteConfig, "BEHAVIOR", "required_doc_title").Equals("N", StringComparison.OrdinalIgnoreCase)
				Dim clrDocTitle = New List(Of CDocumentTitleInfo)

				' Loan doc titles from Custom List Rules
				If CustomListRuleList IsNot Nothing AndAlso CustomListRuleList.Any(Function(c) c.Code = "DOCUMENT_TITLES") Then
					Dim loanClrDocTitle = EvaluateCustomList(Of CDocumentTitleInfo)("DOCUMENT_TITLES")

					If loanClrDocTitle IsNot Nothing AndAlso loanClrDocTitle.Count > 0 Then
						loanClrDocTitle.ForEach(Sub(docTitle) docTitle.LoanType = "CC")
						clrDocTitle.AddRange(loanClrDocTitle)
					End If
				End If

				' XA doc titles for Combo from Custom List Rules
				If XACustomListRuleList IsNot Nothing AndAlso XACustomListRuleList.Any(Function(c) c.Code = "DOCUMENT_TITLES") Then
					Dim xaClrDocTitle = EvaluateCustomList(Of CDocumentTitleInfo)("DOCUMENT_TITLES", XACustomListRuleList)

					If xaClrDocTitle IsNot Nothing AndAlso xaClrDocTitle.Count > 0 Then
						xaClrDocTitle.ForEach(Sub(docTitle) docTitle.LoanType = "XA")
						clrDocTitle.AddRange(xaClrDocTitle)
					End If
				End If

				If clrDocTitle.Count = 0 Then
					' Fallback to ENUMS doc titles
					docTitleOptions = Common.GetDocumentTitleInfoList(_CurrentWebsiteConfig._webConfigXML.SelectSingleNode("ENUMS/LOAN_DOC_TITLE"))
				Else
					' Fill out options
					docTitleOptions = New List(Of CDocumentTitleInfo) From {New CDocumentTitleInfo() With {.Group = "", .Code = "", .Title = "--Please select title--"}}
					docTitleOptions.AddRange(clrDocTitle.OrderBy(Function(t) t.Title))
				End If

				ucDocUpload.Title = Server.HtmlDecode(uploadDocsMessage)
				ucDocUpload.RequireDocTitle = docTitleRequired
				ucDocUpload.DocTitleOptions = docTitleOptions
				ucDocUpload.DocUploadRequired = _requiredUploadDocs.Equals("Y", StringComparison.OrdinalIgnoreCase)

				ucCoDocUpload.Title = Server.HtmlDecode(uploadDocsMessage)
				ucCoDocUpload.RequireDocTitle = docTitleRequired
				ucCoDocUpload.DocTitleOptions = docTitleOptions
				ucCoDocUpload.DocUploadRequired = _requiredUploadDocs.Equals("Y", StringComparison.OrdinalIgnoreCase)
				ucDocUpload.Visible = True
				ucCoDocUpload.Visible = True
				ucDocUploadSrcSelector.Visible = True
				ucCoDocUploadSrcSelector.Visible = True
			Else
				ucDocUpload.Visible = False
				ucCoDocUpload.Visible = False
				ucDocUploadSrcSelector.Visible = False
				ucCoDocUploadSrcSelector.Visible = False
			End If

			'ucCoUploadDocDialog.Title = _UpLoadDocument
			'ucUploadDocDialog.Title = _UpLoadDocument
			''submit cancel message
			_SubmitCancelMessage = Common.getSubmitCancelMessage(_CurrentWebsiteConfig)
			If Not String.IsNullOrEmpty(_SubmitCancelMessage) Then
				_SubmitCancelMessage = Server.HtmlDecode(_SubmitCancelMessage)	''decode code message 
			End If
			'' app_type ='FOREIGN'
			Dim appType As String = _CurrentWebsiteConfig.AppType
			hdForeignAppType.Value = ""	'' initial hidden value foreign
			''Make sure appType is not nothing
			If Not String.IsNullOrEmpty(appType) Then
				If appType.ToUpper() = "FOREIGN" Then
					hdForeignAppType.Value = "Y"
				End If
			End If
			''backgroud theme
			If String.IsNullOrEmpty(_BackgroundTheme) Then
				_bgTheme = _FooterDataTheme
			Else
				_bgTheme = _BackgroundTheme
			End If
			''enable attribute
			'hdEnableJoint.Value = Common.hasEnableJoint(_CurrentWebsiteConfig)
			_enableJoint = Not Common.hasEnableJoint(_CurrentWebsiteConfig).ToUpper().Equals("N")

			If _enableJoint Then
				_enableJoint = Not Common.getVisibleAttribute(_CurrentWebsiteConfig, "joint_enable_cc").ToUpper.Equals("N")
			End If

			hdForeignContact.Value = Common.isForeignContact(_CurrentWebsiteConfig)

			'' get loan IDA, don't check for idaMethod if SSO via MobileApp
			If Not bIsSSO OrElse sso_ida_enable Then
				hdIdaMethodType.Value = Common.getIDAType(_CurrentWebsiteConfig, "CREDIT_CARD_LOAN")
			End If
			'' get loan purpose category from config
			'Dim sNodeName As String = "ENUMS/CREDIT_CARD_LOAN_PURPOSES/ITEM"
			'Dim oSerializer As New JavaScriptSerializer()
			'_LoanPurposeCategory = oSerializer.Serialize(Common.getItemsFromConfig(_CurrentWebsiteConfig, sNodeName, "text", "value"))


			Try

				Dim fundingSource As CFundingSourceInfo
				If isCheckFundingPage() = True Then	 'funding page is enabled
					fundingSource = CFundingSourceInfo.CurrentJsonFundingPackage(_CurrentWebsiteConfig)
				Else
					fundingSource = New CFundingSourceInfo(New CLenderServiceConfigInfo())
				End If

				'' try to append PAYPAL funding option into _FundingSourcePackageJsonString since its config stay in WEBSITE node
				If fundingSource.ListTypes IsNot Nothing AndAlso Not String.IsNullOrEmpty(_CurrentWebsiteConfig.PayPalEmail) Then
					'' make sure PAYPAL option stay on top(below not funding option)
					'TODO: temporary disable Paypal funding for secured cc due to complex implementation 
					'' fundingSource.ListTypes.Insert(1, "PAYPAL")
				End If
				'' serialize fundingSource into JSON then .aspx can access it in Page_PreRender

				ccFundingOptions.FundingTypeList = fundingSource.ListTypes
			Catch ex As Exception  'catch connection issue
				logError("Check connection, etc..", ex)
				Return
			End Try
			ucApplicantInfo.ContentDataTheme = _ContentDataTheme
			ucCoApplicantInfo.ContentDataTheme = _ContentDataTheme
			If IsComboMode Then
				ucFomQuestion.FOMQuestions = CFOMQuestion.CurrentFOMQuestions(_CurrentWebsiteConfig)
				ucFomQuestion._CFOMQuestionList = CFOMQuestion.DownloadFomNextQuestions(_CurrentWebsiteConfig)
				ucFomQuestion._lenderRef = _CurrentLenderRef
				''FOM_V2
				ucFomQuestion.hasFOM_V2 = CFOMQuestion.hasFOM_V2(_CurrentWebsiteConfig)

			Else
				ucFomQuestion.Dispose()
			End If

			'' CC maxfunding
			If Not String.IsNullOrEmpty(_CurrentWebsiteConfig.CCMaxFunding) Then
				_CCMaxFunding = _CurrentWebsiteConfig.CCMaxFunding
				ccFundingOptions.CCMaxFunding = _CCMaxFunding
			End If
			'' ACH maxfunding
			If Not String.IsNullOrEmpty(_CurrentWebsiteConfig.ACHMaxFunding) Then
				_ACHMaxFunding = _CurrentWebsiteConfig.ACHMaxFunding
				ccFundingOptions.ACHMaxFunding = _ACHMaxFunding
			End If
			'' Paypal maxfunding
			If Not String.IsNullOrEmpty(_CurrentWebsiteConfig.PaypalMaxFunding) Then
				_PaypalMaxFunding = _CurrentWebsiteConfig.PaypalMaxFunding
				ccFundingOptions.PaypalMaxFunding = _PaypalMaxFunding
			End If
			_ccStopInvalidRoutingNumber = Common.SafeString(Common.getNodeAttributes(_CurrentWebsiteConfig, "BEHAVIOR", "stop_invalid_routing_number"))
			ccFundingOptions.FundingDisclosure = Common.getFundingDisclosure(_CurrentWebsiteConfig)

			'PRIMARY/JOINT/MINOR/ALL
			Select Case _requireMemberNumber.ToUpper()
				Case "PRIMARY"
					ucApplicantInfo.MemberNumberRequired = "Y"
				Case "JOINT"
					ucCoApplicantInfo.MemberNumberRequired = "Y"
				Case "ALL"
					ucApplicantInfo.MemberNumberRequired = "Y"
					ucCoApplicantInfo.MemberNumberRequired = "Y"
				Case Else
					ucApplicantInfo.MemberNumberRequired = "N"
					ucCoApplicantInfo.MemberNumberRequired = "N"
			End Select

			ucApplicantAddress.CollectDescriptionIfOccupancyStatusIsOther = CollectDescriptionIfOccupancyStatusIsOther
			ucCoApplicantAddress.CollectDescriptionIfOccupancyStatusIsOther = CollectDescriptionIfOccupancyStatusIsOther
			ucApplicantAddress.EnableMailingAddress = EnableMailingAddressSection
			ucCoApplicantAddress.EnableMailingAddress = EnableMailingAddressSection("co_")

			ucFinancialInfo.CollectDescriptionIfEmploymentStatusIsOther = CollectDescriptionIfEmploymentStatusIsOther
            ucCoFinancialInfo.CollectDescriptionIfEmploymentStatusIsOther = CollectDescriptionIfEmploymentStatusIsOther
            ucAssetInfo.EnableAssetSection = EnableAssetSection()
            ucCoAssetInfo.EnableAssetSection = EnableAssetSection()
            ucAssetInfo.AssetTypesString = GetAssetTypes("")
            ucCoAssetInfo.AssetTypesString = GetAssetTypes("")

            If Is2ndLoanApp Then
				_SID = Common.SafeString(Request.Params("sid"))
			End If
			''prefill data on page 2 if reffered from XA
			If Is2ndLoanApp AndAlso String.IsNullOrEmpty(Common.SafeString(Request.QueryString("sid"))) = False Then
				Dim sectionID As String = Common.SafeString(Request.QueryString("sid"))
				Dim params = CType(Session(sectionID), NameValueCollection)
				Dim sXAXSellComment As String = Common.SafeString(Session("xaXSell" & sectionID))
				''xaXSellComment to check it come from XA App
				If sXAXSellComment.Contains("CROSS SELL FROM XA") Then	''If Common.SafeDouble(params("TotalMonthlyHousingExpense")) = 0 Then
					Dim hasCoApp = Convert.ToBoolean(params("IsJoint"))
					If Session(sectionID) IsNot Nothing Then
						_prefillData = Common.prefilledData(False, params)
						If hasCoApp Then
							_coPrefillData = Common.prefilledData(True, params)
						End If
					End If
					_xaXSellComment = sXAXSellComment
					Is2ndLoanApp = False
				End If
			End If

            plhInstaTouchStuff.Visible = False
            ''no instaTouch if it is second loan app
            If _CurrentWebsiteConfig.InstaTouchEnabled AndAlso Not bIsSSO And Not Is2ndLoanApp Then
                plhInstaTouchStuff.Visible = True
                Dim instaTouchStuffCtrl As Inc_MainApp_InstaTouchStuff = CType(LoadControl("~/Inc/MainApp/InstaTouchStuff.ascx"), Inc_MainApp_InstaTouchStuff)
                instaTouchStuffCtrl.PersonalInfoPage = "pl2"
                instaTouchStuffCtrl.LoanType = "CC"
                instaTouchStuffCtrl.InstaTouchMerchantId = _CurrentWebsiteConfig.InstaTouchMerchantId
                instaTouchStuffCtrl.LenderID = _CurrentWebsiteConfig.LenderId
                instaTouchStuffCtrl.LenderRef = _CurrentWebsiteConfig.LenderRef
                plhInstaTouchStuff.Controls.Add(instaTouchStuffCtrl)
            End If
        End If
        'asign config theme for PageHeader UC, avoid reading config file unnescessary
        ucPageHeader._headerDataTheme = _HeaderDataTheme
        ucPageHeader._footerDataTheme = _FooterDataTheme
        ucPageHeader._contentDataTheme = _ContentDataTheme
        ucPageHeader._headerDataTheme2 = _HeaderDataTheme2
        ucPageHeader._backgroundDataTheme = _BackgroundTheme
        ucPageHeader._buttonDataTheme = _ButtonTheme
        ucPageHeader._buttonFontColor = _ButtonFontTheme

        plhApplicationDeclined.Visible = False
        If CustomListRuleList IsNot Nothing AndAlso CustomListRuleList.Any(Function(p) p.Code = "APPLICANT_BLOCK_RULE") Then
            plhApplicationDeclined.Visible = True
            Dim applicationBlockRulesPage As Inc_MainApp_ApplicationBlockRules = CType(LoadControl("~/Inc/MainApp/ApplicationBlockRules.ascx"), Inc_MainApp_ApplicationBlockRules)
            applicationBlockRulesPage.CustomListItem = CustomListRuleList.First(Function(p) p.Code = "APPLICANT_BLOCK_RULE")
            plhApplicationDeclined.Controls.Add(applicationBlockRulesPage)
        End If
        plhApplicantAdditionalInfo.Visible = False
        If NSSList IsNot Nothing AndAlso NSSList.Count > 0 Then
            plhApplicantAdditionalInfo.Visible = True
            Dim applicantAdditionalInfoPage As Inc_MainApp_ApplicantAdditionalInfo = CType(LoadControl("~/Inc/MainApp/ApplicantAdditionalInfo.ascx"), Inc_MainApp_ApplicantAdditionalInfo)
            applicantAdditionalInfoPage.LogoUrl = _LogoUrl
            applicantAdditionalInfoPage.PreviousPage = "pl2"
            applicantAdditionalInfoPage.PreviousCoAppPage = "pl6"
            applicantAdditionalInfoPage.NextPage = "pageSubmit"
            applicantAdditionalInfoPage.NSSList = NSSList
            applicantAdditionalInfoPage.MappedShowFieldLoanType = mappedShowFieldLoanType
            applicantAdditionalInfoPage.EnablePrimaryAppSpouseContactInfo = EnablePrimaryAppSpouseContactInfo
            applicantAdditionalInfoPage.EnableCoAppSpouseContactInfo = EnableCoAppSpouseContactInfo

            plhApplicantAdditionalInfo.Controls.Add(applicantAdditionalInfoPage)
		End If

		''credit pull message in disclosure section
		Dim oNode As XmlNode = _CurrentWebsiteConfig._webConfigXML.SelectSingleNode("CREDIT_PULL_MESSAGE")
		If oNode IsNot Nothing Then
			_CreditPullMessage = oNode.InnerText
		End If
		_ccLocationPool = Common.getNodeAttributes(_CurrentWebsiteConfig, "CREDIT_CARD_LOAN/VISIBLE", "location_pool") = "Y"
		''get credit card products from download when the location_pool is "Y" or  url parameter "cname" is not nothing or empty
		Dim urlParaCCName = Common.SafeString(Request.Params("CName"))
		If _ccLocationPool Or Not String.IsNullOrWhiteSpace(urlParaCCName) Then
			_ccZipCodePoolProducts = CDownloadedSettings.CreditCardProducts(_CurrentWebsiteConfig).Where(Function(x) x.IsActive = "Y").ToList()
			If Not String.IsNullOrWhiteSpace(urlParaCCName) Then
				_selectedCreditCardProduct = _ccZipCodePoolProducts.FirstOrDefault(Function(p) p.CreditCardName.ToUpper = urlParaCCName.ToUpper)
			End If
		End If
		If _ccLocationPool Then
			Dim currentLenderServiceConfigInfo = CDownloadedSettings.DownloadLenderServiceConfigInfo(_CurrentWebsiteConfig)
			_zipCodePoolList = currentLenderServiceConfigInfo.ZipCodePool
			_zipCodePoolNames = currentLenderServiceConfigInfo.ZipCodePoolName
			Dim filterCCNames As New List(Of Tuple(Of String, String, String, String))
			''filter credit card name
			For Each oCCName In _CreditCardNames
				Dim cZPProduct = _ccZipCodePoolProducts.FirstOrDefault(Function(x) x.CreditCardName.ToUpper = oCCName.Item1.ToUpper)
				If (cZPProduct IsNot Nothing) Then
					filterCCNames.Add(oCCName)
				End If
			Next
			_CreditCardNames = filterCCNames
		End If
	End Sub
	Private Function isCheckFundingPage() As Boolean
		Dim result As Boolean = True
		Dim oNodes As XmlNodeList = _CurrentWebsiteConfig._webConfigXML.SelectNodes("CREDIT_CARD_LOAN/VISIBLE")
		If oNodes.Count < 1 Then
			Return True
		End If

		For Each child As XmlNode In oNodes
			If child.Attributes("funding") Is Nothing Then Continue For
			If Common.SafeString(child.Attributes("funding").InnerText) = "N" Then
				result = False
			End If
		Next

		Return result
	End Function


	Protected Function getCreditCardNamesDic() As Dictionary(Of String, String)
		Dim ccNames As Dictionary(Of String, String) = _CurrentWebsiteConfig.GetEnumItems("../CREDIT_CARD_LOAN/CREDIT_CARD_NAME")
		Dim currCCNames As New Dictionary(Of String, String)
		Dim ccType As String = ""

		If ccNames.Count = 0 Then
			Return ccNames
		End If
		For Each oCCName In ccNames
			ccType = "CREDIT"
			If oCCName.Value.ToUpper().IndexOf("SECURE") >= 0 Then
				ccType = "SECUREDCREDIT"
			End If
			If Not currCCNames.ContainsKey(oCCName.Key) Then
				currCCNames.Add(oCCName.Key, ccType)
			End If
		Next
		Return currCCNames


	End Function
	Private Sub handledRenderingCreditCardName(ByVal loanPurposes As Dictionary(Of String, String))
		Dim ccNames As Dictionary(Of String, String) = _CurrentWebsiteConfig.GetEnumItems("../CREDIT_CARD_LOAN/CREDIT_CARD_NAME")
		''get credit card names
		'Dim currCCNames As New Dictionary(Of String, String)
		Dim ccType As String = ""
		For Each oCCName In ccNames
			ccType = "CREDIT"
			If oCCName.Value.ToUpper().IndexOf("SECURE") >= 0 Then
				ccType = "SECUREDCREDIT"
			End If
			If _CreditCardNames.Any(Function(t) t.Item1 = oCCName.Key) = False Then
				_CreditCardNames.Add(New Tuple(Of String, String, String, String)(oCCName.Key, ccType, "", ""))
			End If
			'If Not currCCNames.ContainsKey(oCCName.Key) Then
			'    currCCNames.Add(oCCName.Key, ccType)
			'End If
		Next
		'_CreditCardNames = currCCNames
	End Sub
	Protected Function getComboNewCardLoanPurpose(ByVal poCCLoanPurpose As List(Of Tuple(Of String, String, String, String))) As String
		''currently,CC app is using "NEW CARD" purpose for combo. the problem is that for each lender they use different loan purpose values
		''for example they could use "PERSONAL CREDIT CARD-NEW" or "NEW CARD FOR SECURE CREDIT CARD". 
		''Thus, we should get the New card loan purpose from config xml. In credit card purposes node there could be more than one new card purpose
		Dim sNewCardLoanPurpose = "NEW CARD"
		Dim oNewCardLoanPurpose = poCCLoanPurpose.FirstOrDefault(Function(p) p.Item1.NullSafeToUpper_ = "NEW CARD")
		If oNewCardLoanPurpose Is Nothing Then
			oNewCardLoanPurpose = poCCLoanPurpose.FirstOrDefault(Function(p) p.Item1.NullSafeToUpper_.IndexOf("NEW CARD") > -1)
		End If
		If oNewCardLoanPurpose Is Nothing Then
			oNewCardLoanPurpose = poCCLoanPurpose.FirstOrDefault(Function(p) p.Item1.NullSafeToUpper_.IndexOf("NEW") > -1)
		End If
		If oNewCardLoanPurpose Is Nothing Then
			''if the value attritue does not have new card then search category attribute
			oNewCardLoanPurpose = poCCLoanPurpose.FirstOrDefault(Function(p) p.Item3.NullSafeToUpper_ = "NEW_CARD")
		End If
		If oNewCardLoanPurpose IsNot Nothing Then
			Return Common.SafeString(oNewCardLoanPurpose.Item1)
		End If
		Return sNewCardLoanPurpose
	End Function

	'Private Sub handledRenderingCreditCardName(ByVal loanPurposes As Dictionary(Of String, String))
	'    Dim ccTypes As Dictionary(Of String, String) = _CurrentWebsiteConfig.GetEnumItems("CREDIT_CARD_TYPES")
	'    Select Case True
	'        Case ccTypes.Count = 1
	'            Dim sCCKeys(ccTypes.Count - 1) As String
	'            Dim sCCValues(ccTypes.Count - 1) As String
	'            ccTypes.Keys.CopyTo(sCCKeys, 0)
	'            ccTypes.Values.CopyTo(sCCValues, 0)
	'            _CreditCardTypeDropdown = Common.RenderDropdownlist(sCCKeys, sCCValues, " ", sCCKeys(0))
	'            ''check if only one option
	'            If sCCKeys(0).ToUpper = "CREDIT" Then
	'                _isOnlyCreditCard = "Y"
	'            End If
	'        Case ccTypes.Count < 1
	'            CCTypeDiv.Visible = False
	'        Case Else
	'            _CreditCardTypeDropdown = Common.RenderDropdownlist(ccTypes, " ")
	'    End Select

    '    ' similar ccTypes - add new ccNames :)
    '    Dim ccNames As Dictionary(Of String, String) = _CurrentWebsiteConfig.GetEnumItems("../CREDIT_CARD_LOAN/CREDIT_CARD_NAME")
    '    Select Case True
    '        Case ccNames.Count = 1
    '            Dim sCCKeys(ccNames.Count - 1) As String
    '            Dim sCCValues(ccTypes.Count - 1) As String
    '            ccNames.Keys.CopyTo(sCCKeys, 0)
    '            ccNames.Values.CopyTo(sCCValues, 0)
    '            _CreditCardNames = ccNames.Select(Function(x) x.Key).ToList()
    '            '_CreditCardNameDropdown = Common.RenderDropdownlist(sCCKeys, sCCValues, " ", sCCKeys(0))
    '            'Case ccNames.Count < 1
    '            '    CCNameDiv.Visible = False
    '        Case Else
    '            _CreditCardNames = ccNames.Select(Function(x) x.Key).ToList()
    '            '_CreditCardNameDropdown = Common.RenderDropdownlist(ccNames, " ")
    '    End Select
    '    ''make sure credit card information page is not empty before hiding the credit card type case
    '    If loanPurposes.Count > 0 Or ccNames.Count > 0 Then
    '        If _isOnlyCreditCard = "Y" Then
    '            CCTypeDiv.Visible = False
    '        Else
    '            CCTypeDiv.Visible = True
    '        End If
    '    End If
    'End Sub
End Class
