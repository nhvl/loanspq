﻿var isSubmitWalletAnswer = false;
var isCoSubmitWalletAnswer = false;
var isSubmittingLoan = false;

function handleHiddenCreditCardName(creditCardName, validLoanPurpose) {  
    if (creditCardName != undefined && creditCardName != "") {
        creditCardName = htmlDecode(creditCardName);                  
        //hide credit card name section and hide divCCType
        $("#divCreditCardName").hide();     
        $("#divCCType").addClass('hidden');
         //update hidden card name
        $("#hdCcSelectedCardName").val(creditCardName);
        if (typeof SELECTEDCREDITCARDPRODUCT !== "undefined" && SELECTEDCREDITCARDPRODUCT !==null) {        
            $("#hdCcSelectedCardType").val(SELECTEDCREDITCARDPRODUCT.CreditCardType); 
        } else {
            //no selected Credit Card Product: set credit card type is CREDIT by default         
            $("#hdCcSelectedCardType").val("CREDIT");         
        }
		if (validLoanPurpose) { //card name show/not show depends the loan purpose so need to check the existing loanpurpose in the parameter        
			// Ensure the credit limit field is hidden
			// Ensure there are no custom questions visible
            if (!$("#divRequestCreditLimit").is(":visible") && $("#pl1 .aq-section .loanpage_ApplicantQuestion").length == 0) {
                $('#pl1 div[data-role="content"]').hide();
                //credit limit sections are hidden so go to the next page
                //in this case skip to check branch and disclosures 
                goToNextPage("#pl2");
            } else {
                //make sure continue button is always visible
                $("#pl1").find('div[class~="div-continue"]').show();
            }
        }
    }  
}
function hasCNameParameter() {
    return $('#hdHasCNameParameter').val()=="Y";
}
function handleHiddenLoanPurpose() {
    var loanPurposeElem = $('.divLoanPurposeList a[data-role="button"]');  
    //no loan purpose or combo type --> display credit card type if it exist
    if ($('#hdIsComboMode').val() == "Y" || loanPurposeElem.length == 0) {     
 		var creditCardTypeElem = $('#divCreditCardName');
    	var continueBtnElem = $('#pl1 .div-continue');
        if (creditCardTypeElem.find("div[data-panel='cc-options']").length > 0) {
            continueBtnElem.hide();
            creditCardTypeElem.show();
        } else {
            continueBtnElem.show();
            creditCardTypeElem.hide();
        }
    }
    //only one loan purpose --> preselected and hide loan purpose
    if (loanPurposeElem.length == 1) {
        loanPurposeElem.trigger('click');
        loanPurposeElem.parent().hide(); //hide loan purpose      
    }
}
function showAndHideComboCreditLimit() {
    if ($('#hdIsComboMode').val() == "Y") {
        var ccElem = $('#txtComboCreditLimit');
        var loanPurposeCategoryObj = JSON.parse(LoanPurposeCategory);
        var visibleDesiredCreditLimit = false;
     
        for (var key in loanPurposeCategoryObj) {
            if (loanPurposeCategoryObj[key].indexOf("LIMIT") != -1) {
                visibleDesiredCreditLimit = true;
                break;
            }
        }
        if (!visibleDesiredCreditLimit) {
            ccElem.val(""); //clear desired credit limit input field 
            ccElem.parent().hide(); //hide desired credit limit input field
            ccElem.parent().parent().prev().hide(); //hide desired credit limit title
        } 
    }
}
//hide the first page and go to the second page if no credit cards list and there only one or no pupose
function showAndHideCreditCardPage() {
    var creditCardTypeElem = $('#divCreditCardName');
    var loanPurposeElem = $('.divLoanPurposeList a[data-role="button"]');
    var hideCCPage = false;
    if ($('#hdIsComboMode').val() == "Y") {
        if (creditCardTypeElem.find("div[data-panel='cc-options']").length == 0) {
            //divCreditCardName does not exist then check divCCType if it is hidden then hide CCPage
            if (hiddenCCType()) {
                hideCCPage = true;
            }
        }
    } else {       
        if (creditCardTypeElem.find("div[data-panel='cc-options']").length == 0) {         
            if (loanPurposeElem.length == 0 || (loanPurposeElem.length==1 && loanPurposeElem.text().toUpperCase().indexOf("LIMIT"))==-1) {
                if (loanPurposeElem.length == 1) {
                    loanPurposeElem.trigger('click'); //select loanpurpose
                }
                hideCCPage = true;              
            }
        }
    }
    //no hide credit card page if it has location pool for combo or standard loans
    if (($('#hdHasZipCodePool').val() == "Y" && $('#hdIsComboMode').val() == "Y") || $('#hdHasCCZipCodePool').val() == "Y") {
       hideCCPage = false;
	}
	// Do not hide the CC page if it has a custom question visible
	if ($("#pl1 .aq-section .loanpage_ApplicantQuestion").length > 0) {
		hideCCPage = false;
	}
    if (hideCCPage) {
        //hide first page
        $('#pl1').children().hide();
        $('#pl1 div[data-role="footer"]').remove();
        //go to second page and hide go back button
        $('#pl2 a.div-goback').hide();
		$('#pl2 .div-continue span').hide();

		if (window.IST) {
			IST.FACTORY.loadCarrierIdentification(function () {
				IST.FACTORY.carrierIdentification();
			});
		} else {
			if ($("#hdIs2ndLoanApp").val() == "Y") {
				RenderReviewContent();
				goToNextPage("#pagesubmit");
			} else {
				goToNextPage('#pl2');
			}
		}
    }
}
function pagesubmitInit() {
	//apply this logic for combo mode only
	$("#divCcFundingOptions").find("a[data-value='NOT FUNDING AT THIS TIME']").parent().show();
    if ($("#hdIsComboMode").val() === "Y") {
		if ($("#hdCcSelectedCardType").val().toUpperCase() === "SECUREDCREDIT") {
		    if (typeof loanProductFactory !== "undefined" && loanProductFactory !== null) {  //loanProductFactory is not available for secured cc with product selection not visible
		        loanProductFactory.isSecured = true;
		    }
			//for SECUREDCREDIT, always show Funding section, and put it at the top of the review page
			$("#divCcFundingOptions").insertBefore("#divSecureCardFundingHolder");
			$("#divCcFundingOptions").find("a[data-value='NOT FUNDING AT THIS TIME']").parent().hide();
			$("#divCcFundingOptions").show();		   
			if ($("#txtFundingDeposit").attr("readonly")) {
			    var initMinDeposit = 0;
                //initial min deposit amount for SDFCU if it exist
			    if ($('#hfLenderRef').val().toUpperCase().startsWith('SDFCU')) {       
			        initMinDeposit = 251;			      
			    } 
			    $("#txtFundingDeposit").val(Common.FormatCurrency(initMinDeposit, true));
			    $("#txtFundingDeposit").removeAttr("readonly");
			}
			$("label[for='txtFundingDeposit']").hide();
			if (typeof loanProductFactory !== "undefined" && loanProductFactory !== null) {
				loanProductFactory.removeAllDepositTextFields();
			}
		} else {
			$("#divCcFundingOptions").insertBefore("#pl_cq");
			if ($("#divSelectedProductPool").length == 1 && loanProductFactory.hasFundedProducts()) {
				loanProductFactory.isSecured = false;
				//for combo mode where there are more than one products(visible), show funding section below product.
				$("#divCcFundingOptions").show();
				$("label[for='txtFundingDeposit']").show();
				loanProductFactory.refreshDepositTextFields();
				$("#txtFundingDeposit").val(Common.FormatCurrency(loanProductFactory.getTotalDeposit(), true));
				if (!$("#txtFundingDeposit").attr("readonly")) {
				    $("#txtFundingDeposit").attr("readonly", "readonly");
				}
			} else {
				//for one product where product is auto selected and hidden, funding section is not visible
				$("#divCcFundingOptions").hide();
			}

			$('#txtFundingDeposit').off("change").on("change", function () {
				if (Common.GetFloatFromMoney($(this).val()) === 0) {
					$("#divCcFundingOptions").find("a[data-value='NOT FUNDING AT THIS TIME']").parent().show();
				} else {
					$("#divCcFundingOptions").find("a[data-value='NOT FUNDING AT THIS TIME']").parent().hide();
				}
			}).trigger("change");
			
		}

	    //selected SECURED CREDIT CARD: show label min deposit amount for SDFCU 
		if ($('#hfLenderRef').val().toUpperCase().startsWith('SDFCU')) {   
		     if ($("#hdCcSelectedCardType").val().toUpperCase() === "SECUREDCREDIT") {
		         $('#lblMinDepositAmount').show();
		         initMinDeposit = Common.GetFloatFromMoney($("#txtFundingDeposit").val());
		        if (!($("#txtFundingDeposit").attr("readonly")) && initMinDeposit < 251) {
		            $("#txtFundingDeposit").val(Common.FormatCurrency(251, true));
		        }
		      } else {
		        $('#lblMinDepositAmount').hide();
		    }
		}

	    //FOM membershipfee
		$("#divShowFOM div.btn-header-theme").off("click").on("click", function () {
		    var membershipFees = CC.FACTORY.calculateOneTimeMembershipFee($(this).attr('data-fom-name'));
		    //update divMembership field
		    $("#divMembershipFee").attr("data-membershipfee", membershipFees)
		    $("#divMembershipFee").text(Common.FormatCurrency(membershipFees, true));
		    //update total deposit field
		    if (typeof loanProductFactory !== "undefined" && loanProductFactory !== null) {
		        $('#txtFundingDeposit').val(Common.FormatCurrency(loanProductFactory.getTotalDeposit(), true));
		    } 
		});
	}
    $("a[data-command='proceed-membership']").off("click").on("click", function() {
        var $self = $(this);
        //deactive others
        $("div[data-section='proceed-membership']").find("a[data-command='proceed-membership']").each(function() {
            $(this).removeClass("active");
            handledBtnHeaderTheme($(this));
        });
        //set active for current option
        $self.addClass("active");
        $("#hdProceedMembership").val($self.data("key"));
        $("div[data-section='proceed-membership']").trigger("change");
    });

    pushToPagePaths("pagesubmit");
}
function handleShowAndHideCreditCardNames(element) {
    var continueBtnElem = $('#pl1 .div-continue');
    var $divCCName = $('#divCreditCardName');
    var $divCCType = $('#divCCType');
    var showCCType = false;
    if (element.hasClass('active')) {
        if ($divCCName.find("div[data-panel='cc-options']").length > 0) {
            var sLoanPurpose = element.attr('data-key') != undefined ? element.attr('data-key').toUpperCase() : "";
            var sPurposeCategory = getSelectedCategory(sLoanPurpose) != null ? getSelectedCategory(sLoanPurpose).toUpperCase():"";       
            filterCreditCardNames(sLoanPurpose, $divCCName);
            if (sPurposeCategory.indexOf("NEW") > -1 || sLoanPurpose.indexOf("NEW") > -1) {          
                //check if there are no credit card names with upgrade category then display credit card types dropdown            
                if (hasFilterCCName("NEW")) {
                    showCCType = false;
                } else {
                    showCCType = true;
                }      
            } else if (sPurposeCategory.indexOf("UPGRADE") > -1 || sLoanPurpose.indexOf("UPGRADE") > -1) {
                //check if there are no credit card names with upgrade category then display credit card types dropdown            
                if (hasFilterCCName("UPGRADE")) {               
                    showCCType = false;                                   
                } else {
                    showCCType = true; 
                }
            } else {     
                showCCType = true;       
            }        
            if (!showCCType) {
                $divCCType.addClass('hidden'); 
                $divCCName.show();
                continueBtnElem.hide();
            } else {  
                //ccTypes only has one option so hide the ccTypes dropdowns
                if ($divCCType.find('select option').length > 0) {
                    if ($divCCType.find('select option').length <= 2) {
                        $divCCType.addClass('hidden');
                    } else {
                        $divCCType.removeClass('hidden');
                    }
                }
                $divCCName.hide();
                continueBtnElem.show(); 
            }
        } else {//no credit card name -->show continue button 
            $divCCName.hide();  
            continueBtnElem.show();
        }
    }
}
function hasFilterCCName(sCategory) {
    var hasCCName = false;
    $('#divCreditCardName').find('div[class~="col-sm-6"]').each(function () {
        var cardCategory = $(this).find('div[data-card-name]').attr('data-card-category');
        cardCategory = cardCategory != undefined ? cardCategory.toUpperCase() : "";
        if (sCategory == "UPGRADE") {
            if ((cardCategory.indexOf(sCategory) > -1 || cardCategory.indexOf("BOTH") > -1) && !$(this).hasClass('hidden')) {
                hasCCName = true;
                return false;
            }
        } else {
            if ((cardCategory.indexOf("UPGRADE") == -1) && !$(this).hasClass('hidden')) {
                hasCCName = true;
                return false;
            }
        }
    }); 

    return hasCCName;
}
function filterCreditCardNames(sLoanPurpose, $divCCName) {
    var $ccNames = $divCCName.find('div[class~="col-sm-6"]'); 
    var purposeCategory = getSelectedCategory(sLoanPurpose);
    purposeCategory = purposeCategory != null ? purposeCategory.toUpperCase() : "";
    $ccNames.each(function () {
        var $ccName = $(this);
        var cardCategory = $ccName.find('div[data-card-name]').attr('data-card-category');
        cardCategory = cardCategory != undefined ? cardCategory.toUpperCase():""; //card category ="NEW,UPGRADE,BOTH"
        if (purposeCategory.indexOf("NEW") > -1 || sLoanPurpose.indexOf("NEW") > -1 ) {
            if (cardCategory == "UPGRADE") {
                $ccName.addClass('hidden');
            } else {           
                $ccName.removeClass('hidden');           
            }
        } else if (purposeCategory.indexOf("UPGRADE") > -1 || sLoanPurpose.indexOf("UPGRADE")> -1 ) {
            if (cardCategory == "UPGRADE" || cardCategory == "BOTH") {     
               $ccName.removeClass('hidden');             
            } else {
                $ccName.addClass('hidden');
            }
        } else {
            $ccName.removeClass('hidden');
        }
    }); 
}
function pl1Init() {
    $("#pl1").find("a[data-command='loan-purpose']").off("click").on("click", function () {
    	var $self = $(this);
        //deactive others
        $("#pl1").find("a[data-command='loan-purpose']").removeClass("active");
        //set active for current option
        $self.addClass("active");
        $("#hdCcSelectedLoanPurpose").val($self.data("key"));
        handleShowAndHideCreditCardNames($self);
        showRequestCreditLimit();
        $("#divLoanPurposeList").trigger("change");
    });
    pushToPagePaths("pl1");
   /* $("#btnCCHasCoApplicant").off("click").on("click", function () {
    	var $self = $(this);
    	if ($self.attr("contenteditable") == "true") return false;
        if ($("#hdCcHasCoApplicant").val() === "N") {
            $("#hdCcHasCoApplicant").val("Y");
        } else {
            $("#hdCcHasCoApplicant").val("N");
        }
        $self.toggleClass("active");
        handledBtnHeaderTheme($(this));
    });*/

    //enable sumbit buttons
    if ($('#pagesubmit .div-continue-button').hasClass('ui-disabled')) {
        $('#pagesubmit .div-continue-button').removeClass('ui-disabled');
    }
    if ($('#walletQuestions .div-continue-button').hasClass('ui-disabled')) {
        $('#walletQuestions .div-continue-button').removeClass('ui-disabled');
    }
    if ($('#co_walletQuestions .div-continue-button').hasClass('ui-disabled')) {
        $('#co_walletQuestions .div-continue-button').removeClass('ui-disabled');
    }      
    showAndHideCreditCardPage();        
}


function pageLastInit() {
  
	if ($("#divXsellSection").length === 1) {
		$("a.btn-header-theme", $("#divXsellSection")).on("click", function () {
			$.mobile.loading("show", {
				theme: "a",
				text: "Loading ... please wait",
				textonly: true,
				textVisible: true
			});
		});
	}
}

function hiddenCCType() {
    return $('#divCCType').hasClass('hidden');
}

function showRequestCreditLimit() {
    var selectedLoanPurpose = $("#hdCcSelectedLoanPurpose").val();
    if (selectedLoanPurpose === "") {
        return false;
    }
    var selectedCategory = getSelectedCategory(selectedLoanPurpose);
    var $ccContainer= $('#divRequestCreditLimit');
    var $ccLimit = $('#txtRequestCreditLimit');
    var $ccNumber = $('#eCreditCardNumber');
    var requestCreditLimitElem = $ccLimit.closest('div[data-role="fieldcontain"]');
    var divCCNumberElem = $ccNumber.closest('div[data-role="fieldcontain"]');
    requestCreditLimitElem.show();
    divCCNumberElem.show();
    if (selectedCategory != null) { //exist category
        if (selectedCategory.toUpperCase().indexOf("INCREASE") > -1) {
            $ccLimit.val(Common.FormatCurrency($ccLimit.val(), true));
            $ccContainer.show();
            if (selectedLoanPurpose.toUpperCase().indexOf("LOWER") != -1) { //loanpurpose is lower rate--> only show ccNumber
                //reset and hide credit limit
                $ccLimit.val("0");
                requestCreditLimitElem.hide();
            }
        } else if (selectedCategory.toUpperCase().indexOf("LIMIT") != -1 && selectedCategory.toUpperCase().indexOf("NEW") != -1 ) { //show both card image and request credit limit
            $ccLimit.val(Common.FormatCurrency($ccLimit.val(), true));
            $ccContainer.show();
            //reset and hide card number
            $ccNumber.val("");
            divCCNumberElem.hide();
        } else if (selectedCategory.toUpperCase().indexOf("NEW") != -1) { //category is new card --> only show card image, don't limit
            $ccLimit.val("0");
            $ccNumber.val("");
            $ccContainer.hide(); 
        } else if (selectedCategory.toUpperCase().indexOf("UPGRADE") != -1) {
            $ccContainer.show();
            //reset and hide credit limit
            $ccLimit.val("0");
            requestCreditLimitElem.hide();

        } else {//category could be "" or any string which not contain "INCREASE", should be almost the same as NEW category
            $ccLimit.val("0");
            $ccNumber.val("");
            $ccContainer.hide(); 
        }
    } else { //category is not exist
        if (selectedLoanPurpose.toUpperCase().indexOf("INCREASE") != -1) {
            $ccLimit.val(Common.FormatCurrency($ccLimit.val(), true));
            $ccContainer.show();
        } else if (selectedLoanPurpose.toUpperCase().indexOf("LOWER") != -1) {//loanpurpose is lower rate--> only show ccNumber
            $ccContainer.show();
            //reset and hide credit limit
            $ccLimit.val('0');
            requestCreditLimitElem.hide();
        } else if (selectedLoanPurpose.toUpperCase().indexOf("UPGRADE") != -1) {
            $ccContainer.show();
            //reset and hide credit limit
            $ccLimit.val("0");
            requestCreditLimitElem.hide();

        }else {
            $ccLimit.val("0");
            $ccNumber.val("");
            $ccContainer.hide();
        }
    }
}
function getSelectedCategory(selectedLoanPurpose) {
    var loanPurposeCategoryObj = JSON.parse(LoanPurposeCategory);
    for (var key in loanPurposeCategoryObj) {
        if (selectedLoanPurpose != undefined && key.toUpperCase() == selectedLoanPurpose.toUpperCase()) {
           return loanPurposeCategoryObj[key];
       }
    }
    return null;
}

function onClickReturnToMainPage(e) { 
    saveCancelLoanInfo();     
    clearForms(); //clear data when the page is redicted to the main page
    //return to 
    var sDir = $('#hdRedirectURL').val();
    if (sDir != '') {
        window.location.href = sDir;
    }
	e.preventDefault();
}


function SelectProduct(index) {
    $('#txtErrorMessage').html('');
    var appInfo = new Object();
    appInfo.LenderRef = $('#hfLenderRef').val();
    appInfo.Index = index;
    appInfo.Task = 'SelectProduct';
    var txtLName = $('#txtLName').val();
    var txtFName = $('#txtFName').val();
    appInfo.FullName = txtFName + ' ' + txtLName;
    appInfo = attachGlobalVarialble(appInfo);
    $.ajax({
        type: 'POST',
        url: 'CallBack.aspx',
        data: appInfo,
        dataType: 'html',
        success: function (response) {
            if (response.toUpperCase().indexOf('MLERRORMESSAGE') > -1) {
                goToDialog(response);
            } else {
                //submit sucessfull so clear
                clearForms();
                goToLastDialog(response);
            }
        },
        error: function (err) {
            $('input.custom').attr('disabled', false); //if there is errors, enable agree button
            goToDialog('There was an error when processing your request.  Please try the application again.');
        }
    });
    //there no error
      return false;
}

//simulate link:href_show_dialog_1 click which will redirect to divErrorDialog dialog
function goToDialog(message) {
	if ($("#divErrorPopup").length > 0) {
		$('#txtErrorPopupMessage').html(message);  //error message are not encoded, endoding will remove html tag
		$("#divErrorPopup").popup("open", { "positionTo": "window" });
	} else {
		$('#txtErrorMessage').html(message);  //error message are not encoded, endoding will remove html tag
		$('#href_show_dialog_1').trigger('click');
	}
 }

//simulate link:href_show_last_dialog click which will redirect to pl_last page
function goToLastDialog(message) {
    //application completed so clear
    clearForms();
    $('#div_system_message').html(message);
    //docusign window if enabled
    if ($('#div_system_message').find("iframe#ifDocuSign").length === 1) {
        appendDocuSign();
        $("#div_docuSignIframe").append($('#div_system_message').find("iframe#ifDocuSign"));

        ////goback to final decision message after docusign is closed
        //$('#div_docuSignIframe').find("iframe#ifDocuSign").on("load", function (evt) {
        //    //the initial page load twice so need this to make sure only capturing url on finish
        //    var flag = ($(this).data("loaded") || "") + "1";
        //    $(this).data("loaded", flag);
        //    if (flag === "111") {
        //        $('#href_show_last_dialog').trigger('click');
        //    }
        //});

        var height = window.innerHeight ||
                 document.documentElement.clientHeight ||
                 document.body.clientHeight;

        var footerHeight = $('.divfooter').height();

        height = height - footerHeight - 64;
        $('#div_docuSignIframe').find("iframe#ifDocuSign").attr("height", height);
        $.mobile.changePage("#popupDocuSign", {
            transition: "fade",
            reverse: false,
            changeHash: false
        });
    }
        //cross-sell link button if enabled
    else {
        $(".xsell-selection-panel .xsell-item-option.btn-header-theme", "#div_system_message").on("mouseenter click", function (ev) {
            var element = $(this);
            if (ev.type != 'click') {
                element.addClass('btnHover');
                element.removeClass('btnActive_on');
                element.removeClass('btnActive_off');
            } else {
                element.removeClass('btnHover');
            }
        }).on("mouseleave", function () {
            handledBtnHeaderTheme($(this));
        });
        $('#href_show_last_dialog').trigger('click');
    }

}
function saveAndFinishLater() {
	var loanInfo = getPersonalLoanInfo(false);
	loanInfo.Task = "SaveIncompletedLoan";
	$.ajax({
		type: 'POST',
		url: 'CallBack.aspx',
		data: loanInfo,
		dataType: 'html',
		success: function (response) {
			if (response == "ok") {
				$("#popNotification div[placeholder='content']").text("Current loan has been saved successfully.");
				$("#popNotification").popup("open", { "positionTo": "window" });
			} else {
				goToDialog('There was an error when processing your request.  Please try the application again.');
			}
		},
		error: function (err) {
			goToDialog('There was an error when processing your request.  Please try the application again.');
		}
	});
	return false;
}
function saveLoanInfo(buttonToTrigger) {
    if (g_submit_button_clicked) return false;
    //$('#txtErrorMessage').html('');
    //disable submit button before calling ajax to prevent user submit multiple times
    $('#pagesubmit .div-continue-button').addClass('ui-disabled');
    var loanInfo = getPersonalLoanInfo(false);
    //save instaTouchPrefillComment
    $instaTouchComment = $('#hdInstaTouchPrefillComment');
    if ($instaTouchComment.val() != undefined) {
        loanInfo.InstaTouchPrefillComment = $instaTouchComment.val();
    }  
    loanInfo = attachGlobalVarialble(loanInfo);
    $.ajax({
        type: 'POST',
        url: 'CallBack.aspx',
        data: loanInfo,
        dataType: 'html',
        success: function (response) {
            //clear instaTouchPrefill 
            if ($instaTouchComment.val() != undefined && $instaTouchComment.val() != "") {
                $instaTouchComment.val("");
            }
            if (response.toUpperCase().indexOf('MLERRORMESSAGE') > -1) {
                //enable submit button 
                $('#pagesubmit .div-continue-button').removeClass('ui-disabled');
        		goToDialog(response);
        	} else if (response.toUpperCase().indexOf('WALLETQUESTION') != -1) {
        		displayWalletQuestions(response);      	      
        	} else if (response == "blockmaxapps") {
        		goToNextPage("#pageMaxAppExceeded");
        	} else {
        		//submit sucessfull so clear
        		goToLastDialog(response);       		
            }
        },
        error: function (err) {
        	//enable submit button 
        	$('#pagesubmit .div-continue-button').removeClass('ui-disabled');

            $('input.custom').attr('disabled', false); //if there is errors, enable agree button
            goToDialog('There was an error when processing your request.  Please try the application again.');
        }
    });
    return false;
}

function saveCancelLoanInfo() {

    if ($('#hdEnableDisagree').val() != "Y") {
        return false;
    }
    var loanInfo = getPersonalLoanInfo(true);
    //if  data is not available then don't save data
    if (loanInfo.SSN == "" || loanInfo.DOB == "" || loanInfo.EmailAddr == "") {
        return false;
    }
    loanInfo = attachGlobalVarialble(loanInfo);
    $.ajax({
        type: 'POST',
        url: 'CallBack.aspx',
        data: loanInfo,
        dataType: 'html',
        success: function (response) {
            //submit sucessfull so clear
            clearForms();
        },
    });
    $.mobile.loading('hide'); //hide the ajax message
    return false;
}
function prepareAbrFormValues(pageId) {
	var loanInfo = getPersonalLoanInfo();
	var formValues = {};
	_.forEach(pagePaths, function (p) {
		collectFormValueData(p.replace("#", ""), loanInfo, formValues);
		if (pageId == p.replace("#", "")) return false;
	});
	return formValues;
}
/**
 * Converts the loanInfo, which is normally used on submission, into a data structure meant for Applicant Blocking Logic (ABR).
 * This also only pulls in the information needed for each page. ABR should only work with data on the page the user is on
 * and the pages before that.
 * @param {string} pageId
 * @param {Object} loanInfo
 * @param {Object} formValues
 */
function collectFormValueData(pageId, loanInfo, formValues) {
	switch (pageId) {
		case "pl1":
			if ($("#txtLocationPoolCode").length > 0) {
				formValues["locationPool"] = ZIPCODEPOOLLIST[$("#txtLocationPoolCode").val()];
			} else if ($("#cc_txtLocationPoolCode").length > 0) {
				formValues["locationPool"] = CCPRODUCTS.ZipPoolIds[$("#cc_txtLocationPoolCode").val()];
			}
			formValues["creditCardType"] = [loanInfo.CreditCardType];
			formValues["creditCardPurpose"] = [loanInfo.LoanPurpose];
			formValues["amountRequested"] = loanInfo.RequestAmount;

			collectCustomQuestionAnswers("Application", "LoanPage", null, loanInfo, formValues);
			collectCustomQuestionAnswers("Applicant", "LoanPage", null, loanInfo, formValues);

			break;
		case "pl2":
            var _dob = moment(loanInfo.DOB, "MM-DD-YYYY");
            if (Common.IsValidDate(loanInfo.DOB)) {
				formValues["age"] = moment().diff(_dob, "years", false);
			}
			formValues["citizenship"] = loanInfo.CitizenshipStatus;
			formValues["employeeOfLender"] = loanInfo.EmployeeOfLender;
			formValues["employmentLength"] = (parseInt(loanInfo.txtEmployedDuration_year) || 0) + (parseInt(loanInfo.txtEmployedDuration_month) || 0) / 12;
			formValues["employmentStatus"] = loanInfo.EmploymentStatus;
			formValues["isJoint"] = (loanInfo.HasCoApp == "Y");
			formValues["memberNumber"] = loanInfo.MemberNumber;
			formValues["occupancyLength"] = loanInfo.LiveMonths / 12;
			formValues["occupancyStatus"] = loanInfo.OccupyingLocation;

			collectCustomQuestionAnswers("Applicant", "ApplicantPage", "", loanInfo, formValues);

			break;
		case "pl6":
            var _dob = moment(loanInfo.co_DOB, "MM-DD-YYYY");
            if (Common.IsValidDate(loanInfo.co_DOB)) {
				formValues["age"] = moment().diff(_dob, "years", false);
			}
			formValues["citizenship"] = loanInfo.co_CitizenshipStatus;
			formValues["employeeOfLender"] = loanInfo.co_EmployeeOfLender;
			formValues["employmentLength"] = (parseInt(loanInfo.co_txtEmployedDuration_year) || 0) + (parseInt(loanInfo.co_txtEmployedDuration_month) || 0) / 12;
			formValues["employmentStatus"] = loanInfo.co_EmploymentStatus;
			formValues["memberNumber"] = loanInfo.co_MemberNumber;
			formValues["occupancyLength"] = loanInfo.co_LiveMonths / 12;
			formValues["occupancyStatus"] = loanInfo.co_OccupyingLocation;

			collectCustomQuestionAnswers("Applicant", "ApplicantPage", "co_", loanInfo, formValues);

			break;
		case "pagesubmit":

			collectCustomQuestionAnswers("Application", "ReviewPage", null, loanInfo, formValues);
			collectCustomQuestionAnswers("Applicant", "ReviewPage", null, loanInfo, formValues);

			break;
		default:
			break;
	}
	return formValues;
}

function getPersonalLoanInfo(disagree_check) {
    var appInfo = new Object();
    appInfo.PlatformSource = $('#hdPlatformSource').val();
    appInfo.LenderRef = $('#hfLenderRef').val();    
    appInfo.Task = "SubmitLoan";
    appInfo.MerchantId = $('#hfMerchantID').val();
    appInfo.ClinicId_enc = $('#hfClinicID_enc').val();
    appInfo.WorkerId_enc = $('#hfWorkerID_enc').val();
    appInfo.IsComboMode = $("#hdIsComboMode").val();
    appInfo.HasSSOIDA = $('#hdHasSSOIDA').val(); 
    appInfo.depositAmount = 0;
    if (appInfo.IsComboMode == "Y") {
        appInfo.XABranchID = getXAComboBranchId();
    	appInfo.ProceedXAAnyway = $("#hdProceedMembership").val();
    	if ($("#divSelectedProductPool").length === 1 && $("#divSelectedProductPool").parent().hasClass("hidden") == false) {
    		appInfo.SelectedProducts = JSON.stringify(loanProductFactory.collectProductSelectionData());
	    }
    	if ($("#divCcFundingOptions").length === 1 && $("#divCcFundingOptions").is(":hidden") == false) {
    		appInfo.depositAmount = Common.GetFloatFromMoney($('#txtFundingDeposit').val());
    		FS1.loadValueToData();
    		appInfo.rawFundingSource = JSON.stringify(FS1.Data);
    	}
    }
    
    if ($("#txtLocationPoolCode").length > 0) {
    	appInfo.SelectedLocationPool = JSON.stringify(ZIPCODEPOOLLIST[$("#txtLocationPoolCode").val()]);
    } else if ($("#cc_txtLocationPoolCode").length > 0) {
		appInfo.SelectedLocationPool = JSON.stringify(CCPRODUCTS.ZipPoolIds[$("#cc_txtLocationPoolCode").val()]);
    }
	if (window.ASI) {
		ASI.FACTORY.setApplicantAdditionalInfo(appInfo);
	}
    if (disagree_check) {
        appInfo.isDisagreeSelect = "Y";
    } else {
        appInfo.isDisagreeSelect = "N";
    }
  /*  if ($('.checkDisclosure').val() != undefined) {
        //get all disclosures 
        var disclosures = "";
        var temp = "";
        $('.checkDisclosure').each(function() {
            temp = $(this).text().trim().replace(/\s+/g, " ");
            disclosures += temp + "\n\n";
        });
        appInfo.Disclosure = disclosures;
    }*/
    if (parseDisclosures() != "") {
        appInfo.Disclosure = parseDisclosures();
    }
    appInfo.BranchID = getBranchId();
    appInfo.LoanOfficerID = $('#hfLoanOfficerID').val();
    appInfo.ReferralSource = $('#hfReferralSource').val();
    appInfo.CreditCardName = htmlEncode($.trim($("#hdCcSelectedCardName").val()));
    var ccType = $.trim($("#hdCcSelectedCardType").val());
    if (hasCNameParameter()) {
        //check if there is no selected Credit Card Product then add comment to internal commment
        if (typeof SELECTEDCREDITCARDPRODUCT === "undefined" || SELECTEDCREDITCARDPRODUCT === null) {
            appInfo.CCNameComment ="The credit card name (cname URL Parameter), '"+ appInfo.CreditCardName +"' is not defined. Credit card type is set to CREDIT by default. "
        }
    } else {
        if (!hiddenCCType()) {
            ccType = $('#ddlCreditCardType option:selected').val();
        } else if ($("#pl1").find("div[data-panel='cc-options']>div").length == 0) { // no display credit card name and hidden credit card type
            if ($('#ddlCreditCardType option:selected').length > 0) { //make sure ddlCreditCard exist
                ccType = $('#ddlCreditCardType option:selected').val();
            }
        }
    }
    appInfo.CreditCardType = ccType;
    appInfo.LoanPurpose = $("#hdCcSelectedLoanPurpose").val();
    if (appInfo.IsComboMode == "Y") {
        appInfo.RequestAmount = Common.GetFloatFromMoney($('#txtComboCreditLimit').val());
    }else{
        appInfo.RequestAmount = Common.GetFloatFromMoney($('#txtRequestCreditLimit').val());
    }
    
    appInfo.eCardNumber = $('#eCreditCardNumber').val();
	
    //get loan ida
    appInfo.idaMethodType = $('#hdIdaMethodType').val();
    // Custom Ansers
	appInfo.CustomAnswers = JSON.stringify(
		loanpage_getAllCustomQuestionAnswers()
			.concat(getAllCustomQuestionAnswers())
			.concat(co_getAllCustomQuestionAnswers())
			.concat(reviewpage_getAllCustomQuestionAnswers()));
	if (typeof reviewpage_getAllUrlParaCustomQuestionAnswers == "function") {
		var UrlParaCustomQuestions = reviewpage_getAllUrlParaCustomQuestionAnswers();
        //add UrlParamCustomQuestionAndAnswer if it exist                  
        if (UrlParaCustomQuestions.length > 0) {
            appInfo.IsUrlParaCustomQuestion = "Y";
            appInfo.UrlParaCustomQuestionAndAnswers = JSON.stringify(UrlParaCustomQuestions);
        }
    }
    appInfo.Is2ndLoanApp = $("#hdIs2ndLoanApp").val();
	if (appInfo.Is2ndLoanApp === "Y") {
		appInfo.SID = $("#hdSID").val();
	} else {
	    //cross sell comment from XA App
	    appInfo.xaXSellComment = $('#hdXACrossSellComment').val();
		// Main-App
		// Applicant Info
		SetApplicantInfo(appInfo);
		// Contact Info
		SetContactInfo(appInfo);
        // Asset
        SetAssets(appInfo);
		// Address
		SetApplicantAddress(appInfo);
		//Previous Address
		SetApplicantPreviousAddress(appInfo);
		//Mailing Address
		SetApplicantMailingAddress(appInfo);
		//Applicant ID
		if ($("#hdEnableIDSection").val() === "Y") {
			SetApplicantID(appInfo);
		}
        if ($("#hdIsComboMode").val() === "Y") { 
            if (!$('#div_fom_section').hasClass("hidden")) {
               setFOM(appInfo);
            }
		}

	    //cuna Protection Info
		if (typeof setProtectionInfo !== "undefined") {
		    setProtectionInfo(appInfo, false);
		}
		if ($("#divDeclarationSection").length > 0) {
			SetDeclarations(appInfo);
			appInfo.Declarations = JSON.stringify(appInfo.Declarations);
			appInfo.AdditionalDeclarations = JSON.stringify(appInfo.AdditionalDeclarations);
		}
		//reference information
		if ($("#hdEnableReferenceInformation").val() == "Y") {
			setReferenceInfo(appInfo);
		}

		// Financial
		SetApplicantFinancialInfo(appInfo);
		var hasCoApp = $("#hdCcHasCoApplicant").val().toUpperCase();
		if (hasCoApp == "") {
			hasCoApp = "N";
		}
		//upload document data and info
		var docUploadInfo = {};
		if (typeof docUploadObj != "undefined" && docUploadObj != null) {
			docUploadObj.setUploadDocument(docUploadInfo);
		}
		if (typeof co_docUploadObj != "undefined" && co_docUploadObj != null) {
			co_docUploadObj.setUploadDocument(docUploadInfo);
		}
		if (docUploadInfo.hasOwnProperty("sDocArray") && docUploadInfo.hasOwnProperty("sDocInfoArray")) {
			appInfo.Image = JSON.stringify(docUploadInfo.sDocArray);
			appInfo.UploadDocInfor = JSON.stringify(docUploadInfo.sDocInfoArray);
		}
		appInfo.HasCoApp = hasCoApp;
		//appInfo.CoAppJoin = $("#ddlCoApplicantJoint option:selected").val();
		if (appInfo.HasCoApp == 'Y') {
			// Co-App
			// Co-Applicant Info
			co_SetApplicantInfo(appInfo);
			// Co-Contact Info
			co_SetContactInfo(appInfo);
            // Co-Assets
            co_SetAssets(appInfo);
			// Co-Address
			co_SetApplicantAddress(appInfo);
			//Co-Previous Address
			co_SetApplicantPreviousAddress(appInfo);

			//Mailing Address
			co_SetApplicantMailingAddress(appInfo);

			//co-reference information
			if ($("#hdEnableReferenceInformationForCoApp").val() == "Y") {
				co_setReferenceInfo(appInfo);
			}

			//Applicant ID
			if ($("#hdEnableIDSectionForCoApp").val() === "Y") {
				co_SetApplicantID(appInfo);
			}

			// Co-Declarations
			if ($("#co_divDeclarationSection").length > 0) {
				co_SetDeclarations(appInfo);
				appInfo.co_Declarations = JSON.stringify(appInfo.co_Declarations);
				appInfo.co_AdditionalDeclarations = JSON.stringify(appInfo.co_AdditionalDeclarations);
			}
			// Co-Financial
			co_SetApplicantFinancialInfo(appInfo);
		}
	}

	if ($("#hdVendorID").val() != "" && $("#hdUserID").val() != "") {
	    appInfo.VendorId = $("#hdVendorID").val();
	    appInfo.UserId = $("#hdUserID").val();
		//clone and truncate sensitive data
	    var dataObj = JSON.stringify(appInfo);
		dataObj = JSON.parse(dataObj);
		dataObj.SSN = "";
		dataObj.co_SSN = "";
		if (typeof dataObj.rawFundingSource == "string") {
			var rawFsObj = JSON.parse(dataObj.rawFundingSource);
			rawFsObj.cCreditCardNumber = "";
			rawFsObj.cExpirationDate = "";
			rawFsObj.cCVNNumber = "";
			rawFsObj.cCreditCardNumber = "";
			rawFsObj.cCreditCardNumber = "";
			dataObj.rawFundingSource = JSON.stringify(rawFsObj);
		}
		appInfo.rawData = JSON.stringify(dataObj);
    } 
	return appInfo;
}
//custom merchant call- verify identity
  function submitVerifyIdentity() {            
      var appInfo = {};   
      appInfo.LenderRef = $('#hfLenderRef').val();
      appInfo.Task = "VerifyIdentity";
      appInfo.FirstCallIDALoanNumber = $('#hdFirstCallIDALoanNumber').val();
      appInfo.SecondCallIDALoanNumber = $('#hdSecondCallIDALoanNumber').val();
      appInfo.VerifyIdStatus = $('#hdDocusignVeriStatus').val();
        $.ajax({
            type: 'POST',
            url: 'CallBack.aspx',
            data: appInfo,
            dataType: 'html',
            success: function (response) {
                //clear docusignVerifyStatus
                $('#hdDocusignVeriStatus').val("");           
                if (response != undefined && response.indexOf("hdSecondCallIDALoanNumber") > -1) {
                  //clear first calling Docusign IDA before rendering second calling Docusign IDA
                  $('#div_docuSignIframe').find("iframe#ifDocuSign").remove();
                }
               goToLastDialog(response);
            },
            error: function (err) {        
                goToDialog('There was an error when processing your request.  Please try the application again.');
            }
        });
        return false;
    }
function clearAllInfo() {
    $("input[type='text']").val('');
    $("select").each(function() {
        $(this).get(0).selectedIndex = 0;
        $(this).parent().find('span.ui-btn-text').html('');
    });
}



function checkValidScreen1() { 
    var strMessage = '';
    var selectedLoanPurpose= $("#hdCcSelectedLoanPurpose").val().toUpperCase();
    if ($("#pl1").find("a[data-command='loan-purpose']").length > 0) {
        if ($("#hdCcSelectedLoanPurpose").val() === "") {
            strMessage += 'Please select a purpose<br />'; 
            return strMessage;  //escape this funciton if purpose is not defined
        }
   
        var selectedCategory = getSelectedCategory($("#hdCcSelectedLoanPurpose").val());
        if (selectedCategory != null){  //use this loop when attribute category  is in xml
            if (selectedCategory.toUpperCase().indexOf("INCREASE") > -1 || selectedCategory.toUpperCase().indexOf("LOWER") > -1 ) {
                if (!Common.ValidateText($('#eCreditCardNumber').val())) {
                    strMessage += 'Credit card number is required<br />';
                }
            }
            //else  category could be "" or any string other string, will not require cc#
        }else{ //attribute category is NOT  in xml
            if (selectedLoanPurpose.indexOf("INCREASE") != -1 || selectedLoanPurpose.indexOf("LOWER") != -1) {
                if (!Common.ValidateText($('#eCreditCardNumber').val())) {
                    strMessage += 'Credit card number is required<br />';
                }
            }

        }
     }       
    return strMessage;
}

function validateScreen1(element) {
    var currElement = $(element);
	if (currElement.attr("contenteditable") == "true") return false;
    var strMessage = '';
    var validator = true;
    //validate credit card type if it visible
    if (!hiddenCCType()) {
    	if ($.lpqValidate("ValidateCreditCardType") === false) {
    		validator = false;
    	}
    }
    
    if ($.lpqValidate("ValidateBranch") === false) {
        validator = false;
    }
    if ($.lpqValidate("ValidateLoanPurpose") === false) {
        validator = false;
    }
    if ($.lpqValidate("ValidateCreditInfo") === false) {
        validator = false;
    }

    if ($("#pl1	").find("#divDisclosure").length > 0 && $.lpqValidate("ValidateDisclosure_disclosures") == false) {
    	validator = false;
	}

	// Validate custom questions, which may be moved to the front page
	if ($('#loanpage_divApplicantQuestion').children().length > 0) {
		if ($.lpqValidate("loanpage_ValidateApplicantQuestionsXA") === false) {
			validator = false;
		}
	}


    //strMessage += checkValidScreen1();


    /*
    var txtLoanPurpose = $('#ddlLoanPurpose option:selected').val();
    //add some functions
    var txtRequestAmount = $('#txtRequestCreditLimit').val();
   
    var eCreditCardNumber = $('#eCreditCardNumber').val();
    var hasProtectionLoan = false;
    var isOnlyCreditCard = $('#hdOnlyCreditCard').val();
    //    if (!Common.ValidateRadioGroup('rdProtectionForYourLoan'))
    //        strMessage += 'Select a Member Protection Plan option<br />';
    //no validate credit card type, it it is hidden
    if (!hasCCList()) {
        if (isOnlyCreditCard == "Y") {
            if (ddlCreditCardName == undefined && txtLoanPurpose == undefined) {
                if (!Common.ValidateText(ddlCreditCardType))
                    strMessage += 'Credit Card Type is required<br />';
            }

        } else {
            if (!Common.ValidateText(ddlCreditCardType))
                strMessage += 'Credit Card Type is required<br />';
        }
    }//end !hasCCList()...

    if ($("#ddlCreditCardName").length > 0 && !Common.ValidateText(ddlCreditCardName))
        strMessage += 'Credit Card Name is required<br />';
    
    var loanPurposeEnabled = true;
    var temp = "";
    temp = $('#CCLoanPurposeDiv').length; 
    if (temp == 0) {
        loanPurposeEnabled = false;
    }
    if (!Common.ValidateText(txtLoanPurpose) && loanPurposeEnabled)
        strMessage += 'Loan Purpose is required<br />';
  
    if (loanPurposeEnabled) {
        var loanPurposeElement =$('#ddlLoanPurpose option:selected');
        var selectedCategory = getSelectedCategory(loanPurposeElement);
        if(selectedCategory!=null){
            if (selectedCategory.toUpperCase().indexOf("INCREASE") > -1) {
                if (!Common.ValidateText(eCreditCardNumber)) {
                    strMessage += 'Credit card number is required<br />';
                }
            }
        } else { //category is not exist
            if (!Common.ValidateText(eCreditCardNumber) && (txtLoanPurpose.toUpperCase().indexOf("INCREASE") != -1))
                strMessage += 'Credit card number is required<br />';
        }
    }*/

    //if (!Common.ValidateDisclosures('disclosures')) {
    //    strMessage += 'All disclosures need to be checked<br />';
    //}
    //validate verify code
    //strMessage += validateVerifyCode();
    //validate required dl scan
    //strMessage += validateRequiredDLScan($('#hdRequiredDLScan').val(),false);

    if (validator) {
    	if (window.ABR) {
    		var formValues = prepareAbrFormValues("pl1");
    		if (ABR.FACTORY.evaluateRules("pl1", formValues)) {
    			return false;
    		}
    	}
    	var runBeforeGoToInfoPageCallback = function (curEle) {
		    var $curEle = $(curEle);
    		//if there are cc options
    		if ($("#pl1").find("div[data-panel='cc-options']>div").length > 0) {
    			//this case only for credit names are display and continue button is hidden
    			if (!$curEle.hasClass('div-continue-button')) { //currElement is selected credit card element
    				//then detect which cc options has been clicked and copy its card type and card name to coresponding hidden fields
                    //these data will be used to submit to server on last step
                    if (!hasCNameParameter()) {  
                        $("#hdCcSelectedCardType").val($curEle.find("div[data-card-type]").attr("data-card-type"));
    				    $("#hdCcSelectedCardName").val($curEle.find("div[data-card-type]").attr("data-card-name"));
    				    $curEle.closest("div[data-panel='cc-options']").find("div.header_theme2").removeClass("active");
                        $curEle.find(">div.header_theme2").addClass("active");
                    }
                } else {
                    if (!hasCNameParameter()) {
                        //credit card names and ccTypes are hidden
                        if (hiddenCCType() && $('#ddlCreditCardType option:selected').length > 0) {
                            $("#hdCcSelectedCardType").val($('#ddlCreditCardType option:selected').val());
                        }
                    }
    			}
    			//deprecated, alway use the glass breadscrumb
    			////CODE TO CHANGE ICON ON BREADSCRUMB. WHEN HAS FUNDING THEN DISPLAY DOLLAS SIGN OTHERWISE DISPLAY GLASS ICON
    			//if ($("#hdCcSelectedCardType").val().toUpperCase() === "SECUREDCREDIT" && $("#hdIsComboMode").val() === "Y") {
    			//  $("body").removeClass("no-funding");
    			//} else {
    			//    $("body").addClass("no-funding");
    			//}

    		}
	    }
    	if (window.IST) {
    		IST.FACTORY.carrierIdentification(runBeforeGoToInfoPageCallback);
        } else {
            runBeforeGoToInfoPageCallback(currElement);
    		if ($("#hdIs2ndLoanApp").val() == "Y") {
    			RenderReviewContent();
    			goToNextPage("#pagesubmit");
    		} else {
    			goToNextPage("#pl2");
    		}
    	}
    } else {
	    Common.ScrollToError();
    }
}
function goToPageSubmit() {
	RenderReviewContent();
	goToNextPage("#pagesubmit");
}
function validateScreen2(element) {
    var currElement = $(element);
    //update hiddenHasCoApplicant
    if (currElement.attr('id') == 'continueWithCoApp') {
        $("#hdCcHasCoApplicant").val('Y');
    } else {
        $("#hdCcHasCoApplicant").val('N');
    }

	if (currElement.attr("contenteditable") == "true") return false;
    $('#txtErrorMessage').html('');
    var strMessage = '';
    var validator = true;
    if (ValidateApplicantInfo() == false) {
	    validator = false;
    }
   if (ValidateApplicantContactInfoXA() == false) {
    	validator = false;
    }
    if (ValidateApplicantAddress() == false) {
    	validator = false;
    }
    if (ValidateApplicantPreviousAddress() == false) {
    	validator = false;
    }
    if (ValidateApplicantMailingAddress() == false) {
    	validator = false;
    }
    if ($("#hdEnableIDSection").val() === "Y") {
    	if (ValidateApplicantIdXA() == false) {
    		validator = false;
    	}
    }

    if ($("#hdEnableReferenceInformation").val() == "Y") {
    	if (ValidateReferenceInfo() == false) {
    		validator = false;
    	}
    }
    if (ValidateApplicantFinancialInfo() == false) {
    	validator = false;
    }
    
    if (typeof docUploadObj != "undefined" && docUploadObj != null && docUploadObj.validateUploadDocument() == false) {
    	validator = false;
    }
    if(ValidateAssets() == false){
        validator = false;
    }

	if ($('#divApplicantQuestion').children().length > 0) {
		if ($.lpqValidate("ValidateApplicantQuestionsXA") === false) {
			validator = false;
		}
	}

    var requiredDLMessage = validateRequiredDLScan($('#hdRequiredDLScan').val(), false);
    if (requiredDLMessage != "") {
    	strMessage += requiredDLMessage;
    	validator = false;
    }

    if ($("#divDeclarationSection").length > 0 && $.lpqValidate("ValidateDeclaration") === false) {
    	validator = false;
    }
    if (validator === false) {
		//for backward compatibility, need to show error popup/modal for some function which is not applied inline error message
    	if (strMessage !== "") {
		    if ($("#divErrorPopup").length > 0) {
			    $('#txtErrorPopupMessage').html(strMessage);
			    $("#divErrorPopup").popup("open", { "positionTo": "window" });
		    } else {
			    $('#txtErrorMessage').html(strMessage);
			    currElement.next().trigger('click');
		    }
    	} else {
		    Common.ScrollToError();
	    }
    } else {
    	if (window.ABR) {
    		var formValues = prepareAbrFormValues("pl2");
    		if (ABR.FACTORY.evaluateRules("pl2", formValues)) {
    			return false;
    		}
    	}
    	if ($("#hdCcHasCoApplicant").val() === "Y") {
            goToNextPage("#pl6");
    	} else {
    		if (window.ASI) {
			    var appInfo = {};
			    SetApplicantInfo(appInfo);
			    SetApplicantAddress(appInfo);
			    ASI.FACTORY.runNssWf1(appInfo.AddressState, appInfo.MaritalStatus, goToPageSubmit);
		    } else {
    			goToPageSubmit();
		    }
	    }
    }
}
function hasCoApplicant() {
	return $("#hdCcHasCoApplicant").val() === "Y";
}

function goBackPageSubmit() {
	if (typeof resetFunding == 'function') {
		resetFunding();
	}
    if ($("#hdIs2ndLoanApp").val() === "Y") {
        goToNextPage("#pl1");
    } else {
		if (window.ASI) {
			ASI.FACTORY.goBackFromPageSubmit();
		} else {
			if ($("#hdCcHasCoApplicant").val() === "Y") {
				goToNextPage("#pl6");
			} else {
				goToNextPage("#pl2");
			}
		}
    }
}

function validateScreen6(element) {
	var currElement = $(element);
	if (currElement.attr("contenteditable") == "true") return false;
    $('#txtErrorMessage').html('');
    var strMessage = '';
    var validator = true;
    if (co_ValidateApplicantInfo() == false) {
	    validator = false;
    }
    if (co_ValidateApplicantContactInfoXA() == false) {
    	validator = false;
    }
    if (co_ValidateApplicantAddress() == false) {
    	validator = false;
    }
    if (co_ValidateApplicantPreviousAddress() == false) {
    	validator = false;
    }
    if (co_ValidateApplicantMailingAddress() == false) {
    	validator = false;
    }

    if ($("#hdEnableIDSectionForCoApp").val() === "Y") {
    	if (co_ValidateApplicantIdXA() == false) {
    		validator = false;
    	}
    }
    if ($("#hdEnableReferenceInformationForCoApp").val() == "Y") {
    	if (co_ValidateReferenceInfo() == false) {
    		validator = false;
    	}
    }
    if (co_ValidateApplicantFinancialInfo() == false) {
    	validator = false;
    }
    if (typeof co_docUploadObj != "undefined" && co_docUploadObj != null && co_docUploadObj.validateUploadDocument() == false) {
    	validator = false;
    }
    if(co_ValidateAssets() == false){
        validators = false;
    }
    
	if ($('#co_divApplicantQuestion').children().length > 0) {
		if ($.lpqValidate("co_ValidateApplicantQuestionsXA") === false) {
			validator = false;
		}
	}

    if ($("#co_divDeclarationSection").length > 0 && $.lpqValidate("co_ValidateDeclaration") === false) {
		validator = false;
    }
    var requiredDLMessage = validateRequiredDLScan($('#hdRequiredDLScan').val(), true);
    if (requiredDLMessage != "") {
    	strMessage = requiredDLMessage;
	    validator = false;
    }
    if (validator == false) {
    	//for backward compatibility, need to show error popup/modal for some function which is not applied inline error message
	    if (strMessage != '') {
		    if ($("#divErrorPopup").length > 0) {
			    $('#txtErrorPopupMessage').html(strMessage);
			    $("#divErrorPopup").popup("open", { "positionTo": "window" });
		    } else {
			    $('#txtErrorMessage').html(strMessage);
			    currElement.next().trigger('click');
		    }
	    } else {
	    	Common.ScrollToError();
	    }
    } else {
    	if (window.ABR) {
    		var formValues = prepareAbrFormValues("pl6");
    		if (ABR.FACTORY.evaluateRules("pl6", formValues)) {
    			return false;
    		}
    	}
    	if (window.ASI) {
		    var appInfo = {};
		    SetApplicantInfo(appInfo);
		    SetApplicantAddress(appInfo);
		    co_SetApplicantInfo(appInfo);
		    co_SetApplicantAddress(appInfo);
		    ASI.FACTORY.runNssWf2(appInfo.AddressState, appInfo.MaritalStatus, appInfo.co_RelationshipToPrimary, appInfo.co_AddressState, appInfo.co_MaritalStatus, goToPageSubmit);
	    } else {
    		goToPageSubmit();
	    }
    }
}


//**********start wallet questions and answers ************
 function displayWalletQuestions(message) {
     // The first two characters of the message contains the number of questions
     var numQuestions = parseInt(message.substr(0, 2), 10);
     var questionsHTML = message.trim().substr(2, message.length - 2);
     // Put the number of questions into the hidden input field
     $('#hdNumWalletQuestions').val(numQuestions);
     // Put the HTML for the questions into the proper location in our page
     $('#walletQuestionsDiv').html(questionsHTML);
     var nextpage = $('#walletQuestions');
     $.mobile.changePage(nextpage, {
         transition: "slide",
         reverse: false,
         changeHash: true
     });
 }
 //jquery mobile need to transition to another page inorder to format the html autheitcation question correctly
 function co_displayWalletQuestions(message) {
     // The first two characters of the message contains the number of questions
     var numQuestions = parseInt(message.substr(0, 2), 10);
     var questionsHTML = message.substr(2, message.length - 2);
     // Put the number of questions into the hidden input field
     $('#hdNumWalletQuestions').val(numQuestions);
     // Put the HTML for the questions into the proper location in our page   
     $('#walletQuestionsDiv').empty();
     $('#co_walletQuestionsDiv').html(questionsHTML);
     nextpage = $('#co_walletQuestions');
     $.mobile.changePage(nextpage, {
         transition: "slide",
         reverse: false,
         changeHash: true
     });
 }
 function validateWalletQuestions(element, isCoApp) {
 	if ($(element).attr("contenteditable") == "true") return false;  
     // Make sure that there is a selected answer for each question.
     // After validating the questions, submit them
     var index = 0;
     var numSelected = 0;
     var errorStr = "";
     var numOfQuestions = $('#hdNumWalletQuestions').val();
     numOfQuestions = parseInt(numOfQuestions, 10);
     for (index = 1; index <= numOfQuestions; index++) {
         // Check to see if a radio button was selected
         var radioButtons = $('input[name=walletquestion-radio-' + index.toString() + ']');
         var numRadioButtons = radioButtons.length;
         for (var i = 0; i < numRadioButtons; i++) {
             if ($(radioButtons[i]).prop('checked') == true) {
                 numSelected++;
             }
         }
         if (numSelected == 0) {
             errorStr += "Please answer all the questions";
             break;
         }
         numSelected = 0;
     }
     if (errorStr != "") {
         // An error occurred, we need to bring up the error dialog.
         
     	if ($("#divErrorPopup").length > 0) {
     		$('#txtErrorPopupMessage').html(errorStr);
     		$("#divErrorPopup").popup("open", { "positionTo": "window" });
     	} else {
     		var nextpage = $('#divErrorDialog');
     		$('#txtErrorMessage').html(errorStr);
     		if (nextpage.length > 0) {
     			$.mobile.changePage(nextpage, {
     				transition: "slide",
     				reverse: false,
     				changeHash: true
     			});
     		}
     	}
         //return errorStr;
         return false;
     }
     // No errors, so we can proceed
     submitWalletAnswers(isCoApp);
     return false;
 }

function submitWalletAnswers(isCoApp) {
    var coPrefix = "";
    if (isCoApp) coPrefix = "co_";
    var $submitWQButton = $('#' + coPrefix + 'walletQuestions .div-continue-button');
    //disable submit button before calling ajax to prevent user submit multiple times
    $submitWQButton.addClass('ui-disabled');
     //$('#txtErrorMessage').html('');
    var answersObj = getWalletAnswers(isCoApp);
 	answersObj = attachGlobalVarialble(answersObj);
 	if ($("#hdIsComboMode").val() == "Y") {
 	    if ($("#divCcFundingOptions").length === 1) {
 	        answersObj.depositAmount = Common.GetFloatFromMoney($('#txtFundingDeposit').val());
 	        FS1.loadValueToData();
 	        answersObj.rawFundingSource = JSON.stringify(FS1.Data);
 	    }
 	}
     $.ajax({
         type: 'POST',
         url: 'Callback.aspx',
         data: answersObj,
         dataType: 'html',
         success: function (response) {            
             if (response.toUpperCase().indexOf('MLERRORMESSAGE') > -1) {
                 $submitWQButton.removeClass('ui-disabled');
                 goToDialog(response);
             }
             else {
                 if (response.toUpperCase().indexOf('MLNOIDAUTHENTICATION') > -1) {
                      isApplicationCompleted = true;
                     goToLastDialog(response);              
                 } else if (response.toUpperCase().indexOf('WALLETQUESTION') > -1) {
                     co_displayWalletQuestions(response);      
                   }else {
                     //   isApplicationCompleted = true;
                     goToLastDialog(response);                   
                 }
             }
         },
         error: function (err) {
             //enable submit button 
             $submitWQButton.removeClass('ui-disabled');
             goToDialog('There was an error when processing your request.  Please try the application again.');
         }
     });
     return false;
 }

 function getWalletAnswers(isCoApp) {
     // Grab the answer choices that have been selected by the user
 	var appInfo = getPersonalLoanInfo(false);
     /*var index = 0;
     var numSelected = 0;
     var numOfQuestions = $('#hdNumWalletQuestions').val();
     numOfQuestions = parseInt(numOfQuestions, 10);
     var answersArray = new Array();
     for (index = 1; index <= numOfQuestions; index++) {
         var selectedAnswer = $('input[name=walletquestion-radio-' + index.toString() + ']:checked').attr('id'); 
         if (selectedAnswer != "" && selectedAnswer != null) {
             //exclude "rq"+i+"_" prefix from selectectAnswer
             var rqPrefixLength = 4;
             if (i >= 10) {
                 rqPrefixLength = 5;
             }
             answersArray[index - 1] = selectedAnswer.substring(rqPrefixLength, selectedAnswer.length);
         }
     }
     // Put the answers into a semicolon separated string
     // Only the ids of the answers will be passed to the callback code
     var walletAnswersConcatStr = "";
     for (var i = 0; i < numOfQuestions; i++) {
         walletAnswersConcatStr += answersArray[i] + ";";
     }
     // Get rid of the last semicolon
     walletAnswersConcatStr = walletAnswersConcatStr.substr(0, walletAnswersConcatStr.length - 1);
     appInfo.WalletAnswers = walletAnswersConcatStr;
     */

     //get wallet questions and answers
     getWalletQuestionsAndAnswers(appInfo);
	// appInfo.CSRF = $('#hfCSRF').val();
     appInfo.LenderRef = $('#hfLenderRef').val();
     appInfo.Task = "WalletQuestions";
     appInfo.idaMethodType = $('#hdIdaMethodType').val();
     appInfo.co_FirstName = $('#co_txtFName').val();
     appInfo.ProceedXAAnyway = $("#hdProceedMembership").val();
     appInfo.IsComboMode = $("#hdIsComboMode").val();
     if (isCoApp) {
         appInfo.hasJointWalletAnswer = "Y";
     } else {
         appInfo.hasJointWalletAnswer = "N";
     }
     return appInfo;
 }
//**********end wallet questions and answers ************
 //this create selectDocs object
//autofile() for testing purpose
 function autoFillData() {
 	$('#ddlBranchName option').eq(1).prop('selected', true);
 	$('#ddlBranchName').selectmenu("refresh").closest("div.ui-btn").addClass("btnActive_on");
 	$("#ddlCreditCardType option").eq(1).prop('selected', true);
 	$('#ddlCreditCardType').selectmenu("refresh");
 	$("#txtRequestCreditLimit").val("10000").trigger("blur");
    $("#eCreditCardNumber").val("1234567890123456").trigger("blur");
     if ($("#txtComboCreditLimit").is(":visible")){
        $("#txtComboCreditLimit").val("10000").trigger("blur");
     }
	 $("#divLoanPurposeList a.btn-header-theme[data-role='button']:eq(0)").trigger("click");
    autoFillData_PersonalInfor();
  	co_autoFillData_PersonalInfor();
    autoFillData_Address();
	autoFillData_ContactInfor();   
	co_autoFillData_Address();
     autoFillData_FinancialInFor();   
    co_autoFillData_FinancialInFor();
    co_autoFillData_ContactInfor();
    if ($("#txtIDDateIssued").length > 0) {
    	autoFillData_ID();    	
    }
    if ($("#co_txtIDDateIssued").length > 0) {       
        co_autoFillData_ID();
    }
    if (typeof autofill_Disclosure == "function") {
	    autofill_Disclosure();
     }  
    if ($("#divProceedMembership").length > 0) {
        $("a.btn-header-theme[data-command='proceed-membership'][data-key='Y']", "#divProceedMembership").addClass("active btnActive_on");
        $("#hdProceedMembership").val("Y");
    }
    if ($("#divDeclarationSection").length > 0) {
    	autofill_Declarations();
    }
    if ($("#co_divDeclarationSection").length > 0) {
    	co_autofill_Declarations();
    }
     //testing redstone demo merchant
     if ($('#hfLenderRef').val().indexOf("rsfcu_") > -1 || $('#hfLenderRef').val().indexOf("redstonefcu_test") > -1) {
         $('#txtFName').val("Dennis");
         $('#txtLName').val("Ronald");
         $('#txtDOB').val("05/25/1956");
         $('#txtDOB1').val("05");
         $('#txtDOB2').val("25");
         $('#txtDOB3').val("1956");

         $('#txtSSN').val("666-08-2812");
         $('#txtSSN1').val("666");
         $('#txtSSN2').val("08");
         $('#txtSSN3').val("2812");
     } 
}

function getUserName() {
    var txtFName = $('#txtFName').val();
    var txtMName = $('#txtMName').val();
    var txtLName = $('#txtLName').val();
    //capitalize the first character
    txtFName = txtFName.replace(txtFName.charAt(0), txtFName.charAt(0).toUpperCase());
    txtMName = txtMName.replace(txtMName.charAt(0), txtMName.charAt(0).toUpperCase());
    txtLName = txtLName.replace(txtLName.charAt(0), txtLName.charAt(0).toUpperCase());
    var userName = "";
    if (txtMName != "")
        userName = txtFName + " " + txtMName + " " + txtLName;
    else
        userName = txtFName + " " + txtLName;
    return userName;
}

function ViewCreditCardInfo() {
    var strHtml = "";
    var strPurpose = $("#hdCcSelectedLoanPurpose").val();
    if (strPurpose !== "") {
        var strPurposeText = "";
        var oCCPurposes = CREDITCARDPURPOSES;
        for (var oPurpose in oCCPurposes) {
            var sValue = oCCPurposes[oPurpose].Item1 != undefined ? oCCPurposes[oPurpose].Item1.toUpperCase() : "";
            if (strPurpose.toUpperCase() == sValue) {
                strPurposeText = oCCPurposes[oPurpose].Item2;
                break;
            }
        }
        if (strPurposeText != "") {
            strHtml += '<div class="col-sm-6 col-xs-12"><div class="row"><div class="col-xs-6 text-right row-title"><span class="bold">Purpose</span></div><div class="col-xs-6 text-left row-data"><span>' + strPurposeText + '</span></div></div></div>';
        }
        var txtRequestCreditLimit = $("#txtRequestCreditLimit").val();
        var eCreditCardNumber = $("#eCreditCardNumber").val();
        if (Common.GetFloatFromMoney(txtRequestCreditLimit) != 0) {
            strHtml += '<div class="col-sm-6 col-xs-12"><div class="row"><div class="col-xs-6 text-right row-title"><span class="bold">Request Credit Limit</span></div><div class="col-xs-6 text-left row-data"><span>' + txtRequestCreditLimit + '</span></div></div></div>';
        }
        if (Common.ValidateText(eCreditCardNumber)) {
            strHtml += '<div class="col-sm-6 col-xs-12"><div class="row"><div class="col-xs-6 text-right row-title"><span class="bold">Card Number</span></div><div class="col-xs-6 text-left row-data"><span>' + eCreditCardNumber + '</span></div></div></div>';
        }
    }
    //view credit card type if it is not hidden
    if (hasCNameParameter()) {      
        strHtml += '<div class="col-sm-6 col-xs-12"><div class="row"><div class="col-xs-6 text-right row-title"><span class="bold">Credit Card Type</span></div><div class="col-xs-6 text-left row-data"><span>' + $('#hdCcSelectedCardType').val() + '</span></div></div></div>';
    } else {
        if (!hiddenCCType() && $('#ddlCreditCardType option:selected').length > 0) {
            strHtml += '<div class="col-sm-6 col-xs-12"><div class="row"><div class="col-xs-6 text-right row-title"><span class="bold">Credit Card Type</span></div><div class="col-xs-6 text-left row-data"><span>' + $('#ddlCreditCardType option:selected').text() + '</span></div></div></div>';
        }
    }
   
    if ($("#hdIsComboMode").val() == "Y") {
        var txtComboCreditLimit = $("#txtComboCreditLimit").val();
        if (Common.GetFloatFromMoney(txtComboCreditLimit) != 0) {
            strHtml += '<div class="col-sm-6 col-xs-12"><div class="row"><div class="col-xs-6 text-right row-title"><span class="bold">Request Credit Limit</span></div><div class="col-xs-6 text-left row-data"><span>' + txtComboCreditLimit + '</span></div></div></div>';
        }
    }
    var strCardName = $("#hdCcSelectedCardName").val();
    if (strCardName !== "") {
        strHtml += '<div class="col-sm-6 col-xs-12"><div class="row"><div class="col-xs-6 text-right row-title"><span class="bold">Card Name</span></div><div class="col-xs-6 text-left row-data"><span>' + strCardName + '</span></div></div></div>';
    }
    $(this).hide();
    $(this).prev("div.row").hide();
    if (strHtml !== "") {
        $(this).show();
        $(this).prev("div.row").show();
    }
    return strHtml;
}

function RenderReviewContent() {
    viewSelectedBranch(); //view selected branch
    $(".ViewCreditCardInfo").html(ViewCreditCardInfo.call($(".ViewCreditCardInfo")));
    if ($("#hdIs2ndLoanApp").val() == "Y") {
        var hasProtectionInfo = false;
        $('#divProtectionInfo .ProtectionInfo').each(function () {
            var currElem = $(this);            
            if (currElem.attr('style') != undefined && currElem.attr('style').indexOf('block') > -1) {
                hasProtectionInfo = true;
                return false; 
            }
        });

        if (ViewCreditCardInfo() == "" && $('#div_custom_questions .CustomQuestion').length ==0 && !hasProtectionInfo) {
            $('#pagesubmit .page_title').hide();
        }

        return;
    }
    $(".ViewAcountInfo").html(ViewAccountInfo.call($(".ViewAcountInfo")));
	if ($("#hdEnableReferenceInformation").val() == "Y") {
		$(".ViewReferenceInfo").html(ViewReferenceInfo.call($(".ViewReferenceInfo")));
	}

	$(".ViewContactInfo").html(ViewContactInfo.call($(".ViewContactInfo")));
    $(".ViewAddress").html(ViewAddress.call($(".ViewAddress")));
    if ($("#hdEnableIDSection").val() === "Y") {
        $(".ViewApplicantID").html(ViewPrimaryIdentification.call($(".ViewApplicantID")));
    }
    $(".ViewFinancialInfo").html(ViewFinancialInfo.call($(".ViewFinancialInfo")));
    $(".ViewPrevEmploymentInfo").html(ViewPrevEmploymentInfo.call($(".ViewPrevEmploymentInfo")));
    if ($("#divDeclarationSection").length > 0) {
    	$(".ViewDeclaration").html(ViewDeclaration.call($(".ViewDeclaration")));
    }
    if ($('#divApplicantQuestion').length > 0) {
        $(".ViewApplicantQuestion").html(ViewApplicantQuestion.call($('.ViewApplicantQuestion')));
    }
    if ($("#hdCcHasCoApplicant").val() === "Y") {
        $(".ViewJointApplicantInfo").html(co_ViewAccountInfo.call($(".ViewJointApplicantInfo")));
	    if ($("#hdEnableReferenceInformationForCoApp").val() == "Y") {
		    $(".ViewJointReferenceInfo").html(co_ViewReferenceInfo.call($(".ViewJointReferenceInfo")));
	    }
	    $(".ViewJointApplicantContactInfo").html(co_ViewContactInfo.call($(".ViewJointApplicantContactInfo")));
        $(".ViewJointApplicantAddress").html(co_ViewAddress.call($(".ViewJointApplicantAddress")));
        if ($("#hdEnableIDSectionForCoApp").val() === "Y") {
            $(".ViewJointApplicantID").html(co_ViewPrimaryIdentification.call($(".ViewJointApplicantID")));
        }
        $(".ViewJointApplicantFinancialInfo").html(co_ViewFinancialInfo.call($(".ViewJointApplicantFinancialInfo")));
        $(".ViewJointApplicantPrevEmploymentInfo").html(co_ViewPrevEmploymentInfo.call($(".ViewJointApplicantPrevEmploymentInfo")));
        if ($("#co_divDeclarationSection").length > 0) {
        	$(".ViewJointApplicantDeclaration").html(co_ViewDeclaration.call($(".ViewJointApplicantDeclaration")));
        }
        if ($('#co_divApplicantQuestion').length > 0) {
            $(".ViewJointApplicantQuestion").html(co_ViewApplicantQuestion.call($('.ViewJointApplicantQuestion')));
        }

    } else {
        $(".jna-panel").hide();
    }
    $("div.review-container div.row-title b").each(function (idx, ele) {
    	if (typeof $(ele).data("renameid") == "undefined") {
    		var dataId = getDataId();
    		$(ele).attr("data-renameid", dataId);
    		RENAME_REPOSITORY[dataId] = htmlEncode($(ele).html());
    	}
    });
}

function validatePageSubmit(element) {
	if ($(element).attr("contenteditable") == "true") return false;
	var validator = true;
	var strMessage = "";	
    var currElement = $(element); 
    if ($("#hdIsComboMode").val() === "Y") {       
        if (!$('#div_fom_section').hasClass("hidden") && $("#pagesubmit").find("#divShowFOM").length > 0 && $.lpqValidate("ValidateFOM") == false) {
             validator = false;
        }

        if ($.lpqValidate("ValidateProceedMembership") == false) {
            validator = false;
        }
		if ($("#hdCcSelectedCardType").val().toUpperCase() === "SECUREDCREDIT") {
			//validate deposit amount
			//TODO: value should be coming from configuration
			if ($('#hfLenderRef').val().toUpperCase().startsWith('SDFCU')) {
				if (Common.GetFloatFromMoney($("#txtFundingDeposit").val()) < 251) {
					strMessage += "Minimum deposit amount should be equal or greater than $251.00<br/>";
				} 
			}
		}
		if ($.lpqValidate("ValidateProductSelection") === false) {
			validator = false;
		}

		if (validateFS1() == false) {
		    validator = false;
		}

		//strMessage += validateApplicantQuestions();
    }
	
	// Validate custom questions
	if ($('#reviewpage_divApplicantQuestion').children().length > 0) {
		if ($.lpqValidate("reviewpage_ValidateApplicantQuestionsXA") === false) {
			validator = false;
		}
	}    
    if ($("#pagesubmit").find("#divDisclosure").length > 0 && $.lpqValidate("ValidateDisclosure_disclosures") == false) {
        validator = false;
    }
   
     //validate the view loan info panel, if the panel is empty display error message when clicking submit button
    //don't need to validate all the view panels because if the app is cross sell then it only has loan info and review pages 
    if ($("#reviewPanel").find(".ViewCreditCardInfo").length > 0 && $.lpqValidate("ValidateViewCreditCardInfo") == false) {
        validator = false;
    }
    //validate verify code
    strMessage += validateVerifyCode();
   
    //if (validator == false && strMessage.indexOf("Please complete all of the required field") < 0) {
    //	strMessage = "Please complete all of the required field(s)<br/>" + strMessage;
    //}
	
	//refine error message to remove duplicate item
    var arrErr = strMessage.split("<br/>");
    var refinedErr = [];
    $.each(arrErr, function (i, el) {
	    el = $.trim(el);
	    if (el !== "" && $.inArray(el, refinedErr) === -1) refinedErr.push(el);
    });
    if (refinedErr.length > 0) {
	    validator = false;
    }
	if (validator === false) {
		if (refinedErr.length > 0) {
			if ($("#divErrorPopup").length > 0) {
				$('#txtErrorPopupMessage').html(refinedErr.join("<br/>"));
				$("#divErrorPopup").popup("open", { "positionTo": "window" });
			} else {
				$('#txtErrorMessage').html(refinedErr.join("<br/>"));
				currElement.next().trigger('click');
			}
		} else {
			Common.ScrollToError();       
		}
	} else {
		if (window.ABR) {
			var formValues = prepareAbrFormValues("pagesubmit");
			if (ABR.FACTORY.evaluateRules("pagesubmit", formValues)) {
				return false;
			}
		}
	    saveLoanInfo();	    
	}
}
function acceptLaserScanResult(prefix, ele) {
	fillLaserScanResult(prefix, ele);
	$(ele).closest(".scan-result-wrapper").removeClass("open");
	var nextPage = "#pl2";
	if (prefix !== "") {
		nextPage = "#pl6";
	}
	goToNextPage(nextPage);
}
var docScanObj, co_docScanObj;
$(function () {
	$("#popNotification").enhanceWithin().popup();
    EMPLogicPI = $.extend(true, {}, EMPLogic);
    EMPLogicPI.init('ddlEmploymentStatus', '', 'pl2', 'divEmploymentStatusRest');
    $('#pl2').on("pageshow", function () {
    	EMPLogicPI.refreshHtml();
	});

    EMPLogicJN = $.extend(true, {}, EMPLogic);
    EMPLogicJN.init('co_ddlEmploymentStatus', 'co_', 'pl6', 'co_divEmploymentStatusRest');
    $('#pl6').on("pageshow", function () {
    	EMPLogicJN.refreshHtml();
    });
    //primary previous employment
    EMPLogicPI_PREV = $.extend(true, {}, EMPLogic);
    EMPLogicPI_PREV.init('prev_ddlEmploymentStatus', 'prev_', 'pl2', 'prev_divEmploymentStatusRest');
    //joint previous employement
    EMPLogicJN_PREV = $.extend(true, {}, EMPLogic);
    EMPLogicJN_PREV.init('co_prev_ddlEmploymentStatus', 'co_prev_', 'pl6', 'co_prev_divEmploymentStatusRest');
    
    // display the request credit limit textbox
    $('#divRequestCreditLimit').hide();
    //SwitchHasCoAppButton(document.getElementById('ddlHasCoApplicant'), false);
    if (!isMobile.Android()) {
        $('#eCreditCardNumber').mask('9999-9999-9999-9999');
    }
    else {
        creditCardFormat();
    }
    showRequestCreditLimit();
    pl1Init();
    $(document).on("pageshow", function () {
        var curPageId = $.mobile.pageContainer.pagecontainer('getActivePage').attr('id');
        switch (curPageId) {
            case "pl1":
                pl1Init();
                break;                
        	case "pl2":
        		pushToPagePaths("pl2");
		        $("#hdCcHasCoApplicant").val("N");//reset
                CC.FACTORY.cc2Init(false);  //Prefill data is not null when coming from Xa --for primary
                break;
        	case "pl6":
        		pushToPagePaths("pl6");
                CC.FACTORY.cc2Init(true);  //Prefill data is not null when coming from Xa --for Joint Applicant
                break;
            case "pagesubmit":   
                CC.FACTORY.registerViewCreditCardInfoValidator();
                pagesubmitInit();
                break;           
            case "pl_last":
                pageLastInit();
                break;
            default:
        }  
    });
  
    $(document).on('click', '#divDisagree a', function (e) {
        var redirectURL = $('#hdRedirectURL').val();
        //don't popup confirm message--> go direct to return to mainpage
        //displayConfirmMessage(redirectURL, isCoApp, onClickReturnToMainPage);
        if (redirectURL == "") { //need to save loan infor 
            onClickReturnToMainPage(e);
        }
        // location.hash = '#decisionPage'; --> location.hash is no working on IE browser
        goToNextPage($('#decisionPage'));
	    e.preventDefault();
    });

    
    //enabled I Agree button when click refresh
    $('input.custom').attr('disabled', false);

    //var footerTheme = $('#hdFooterTheme').val();
    //var bgColor = $('.ui-bar-' + footerTheme).css('background-color');
    //var changeColor = changeBackgroundColor(bgColor, .6);
    //var bgTheme = $('#hdBgTheme').val();
    //if (bgTheme != "") {
    //    changeColor = bgColor;
    //}
    //$('body').css("background-color", changeColor);
    // handleDivContent('.ui-page-active');
    // handleDivFooter();
    //handleEnableJoint();
    //handledCheckboxDisclosure(); // disclosure click handled in gotoPDFViewer
    //handleRemoveBtnStyle('pl4'); //primary applicant
    //handleRemoveBtnStyle('pl8'); // for coApplicant
    //$('#ddlHasCoApplicant').on('change', function () {
    //    var hasCoApp = $(this).val();
    //    if (hasCoApp == 'Y') {
    //        $('#divHasCoApp').show();
    //        $('#divSubmitApplicant').hide();
    //    } else {
    //        $('#divHasCoApp').hide();
    //        $('#divSubmitApplicant').show();
    //    }
    //});
    handledFontWeightOfDivContent();
    //handle disable disclosures 
    $("div[name='disclosures'] label").click(function () {
        handleDisableDisclosures(this);
    });
    //handle selected/unseleced Joint Applicant button
    //$('#btnCCHasCoApplicant').on('click', function () {
    //    handledBtnHeaderTheme($(this));
    //});
    //handle selected/unselected loanpurpose button
    $('.divLoanPurposeList').on('click', '.btn-header-theme', function () {
        var purposeListElem = $('.divLoanPurposeList .btn-header-theme');
        purposeListElem.each(function () {
            var currentElem = $(this);
            if (!currentElem.hasClass('active')) {
                currentElem.removeClass('btnActive_on');
            }
        });
        handledBtnHeaderTheme($(this));
    });
    $("#popUpCreditDiff, #popUpSavingAmountGuid").on("popupafteropen", function (event, ui) {
        $("#" + this.id + "-screen").height("");
    });

    handleHiddenLoanPurpose();
    showAndHideComboCreditLimit();
    //branches dropdown
    handleShowAndHideBranchNames();
       
    $("div[data-section='proceed-membership']").observer({
        validators: [
            function (partial) {
                if ($("#hdProceedMembership").val() == "") {
                    return "Do you wish to continue opening your membership?";
                }
                return "";
            }
        ],
        validateOnBlur: true,
        group: "ValidateProceedMembership",
        groupType: "complexUI"
    });
   
    $("#divLoanPurposeList").observer({
        validators: [
            function (partial) {
                if ($("#pl1").find("a[data-command='loan-purpose']").length > 0) {
                    var selectedLoanPurpose = $("#hdCcSelectedLoanPurpose").val().toUpperCase();
                    if (selectedLoanPurpose === "") {
                        return "Please select a purpose";
                    }
                }
                return "";
            }
        ],
        validateOnBlur: true,
        group: "ValidateLoanPurpose",
        groupType: "complexUI"
    });
    $("#eCreditCardNumber").observer({
        validators: [
            function (partial) {
                if ($("#pl1").find("a[data-command='loan-purpose']").length > 0) {
                    var selectedLoanPurpose = $("#hdCcSelectedLoanPurpose").val().toUpperCase();
                    if (selectedLoanPurpose === "") {
                        $.lpqValidate.showValidation("#divLoanPurposeList", "Please select a purpose");
                        return "";
                    }
                    var selectedCategory = getSelectedCategory($("#hdCcSelectedLoanPurpose").val());
                    if (selectedCategory != null) { //use this loop when attribute category  is in xml
                        if (selectedCategory.toUpperCase().indexOf("INCREASE") > -1 || selectedCategory.toUpperCase().indexOf("LOWER") > -1 || selectedCategory.toUpperCase().indexOf("UPGRADE") > -1) {
                            if (!Common.ValidateText($(this).val())) {
                                return "Credit card number is required";
                            }
                        }
                        //else  category could be "" or any string other string, will not require cc#
                    } else { //attribute category is NOT  in xml
                        if (selectedLoanPurpose.indexOf("INCREASE") != -1 || selectedLoanPurpose.indexOf("LOWER") != -1 || selectedLoanPurpose.indexOf("UPGRADE") > -1) {
                            if (!Common.ValidateText($(this).val())) {
                                return "Credit card number is required";
                            }
                        }

                    }
                }
                return "";
            }
        ],
        validateOnBlur: true,
        group: "ValidateCreditInfo"
    });
    $("#ddlCreditCardType").observer({
        validators: [
            function (partial) {
                if (Common.ValidateText((this).val()) == false) {
                    return "Please select Credit Card Type";
                }
                return "";
            }
        ],
        validateOnBlur: true,
        group: "ValidateCreditCardType"
    });
	// Fill in the fields with test data
    if (getParaByName("autofill") == "true") {   
            // Fill in the stuff
            autoFillData();     
    }
    //xaCombo location pool
    if ($('#hdHasZipCodePool').val() == 'Y' && $('#hdIsComboMode').val() == 'Y') {
        var $zipCodeElement = $("#txtLocationPoolCode");
        if (typeof loanProductFactory !== "undefined" && loanProductFactory !== null) {
            loanProductFactory.handledLocationPoolProducts($zipCodeElement);
        }
    } else if ($('#hdHasCCZipCodePool').val() == 'Y') { //standard loan location pool       
        CC.FACTORY.handledLocationPoolLoanInfo();
    }

    docScanObj = new LPQDocScan("");
    co_docScanObj = new LPQDocScan("co_");
    //InstaTouch: Call 1 ("Authorization") called on the loading of the "CC Info" and not after an applicant has clicked the "Continue" button
    if (window.IST) {
        IST.FACTORY.loadCarrierIdentification();
    }
});
function bindSavedData(appInfo) {
	
}
function ScanAccept(prefix) {
	if (prefix == "") {
		docScanObj.scanAccept();
		goToNextPage("#pl2");
	} else {
		co_docScanObj.scanAccept();
		goToNextPage("#pl6");
	}
}

(function (CC, $, undefined) {
    CC.FACTORY = {};
    CC.FACTORY.cc2Init = function (isCoApp) {
        var currPrefillData = null;

        if (isCoApp) {
            currPrefillData = COPREFILLDATA;
        } else {
            currPrefillData = PREFILLDATA;
        }

        if (currPrefillData != null) {
            $.each(currPrefillData, function (id, value) {
                var $ele = $("#" + id);
                if ($ele.is("input") && ($ele.attr("type") === "text" || $ele.attr("type") === "tel" || $ele.attr("type") === "password" || $ele.attr("type") === "email")) {
                    $ele.val(value);                      
                    //$ele.trigger("blur");
                } else if ($ele.is("select")) {
                    if (id.indexOf('preferredContactMethod')>-1) {
                        value = mappingPreferredContactValue(value);
                        $ele.val(value);          
                    } else {
                        $ele.val(value);
                    }
                    $ele.selectmenu("refresh");
                    $ele.trigger("change");
                  
                }
            });
            //show mailing address if it exist
            var sMailingStreet = $('#' + coPrefix + 'txtMailingAddress').val()
            if (sMailingStreet != undefined && sMailingStreet != "") {
                $('#' + coPrefix + 'lnkHasMaillingAddress').attr('status', 'Y');
                $('#' + coPrefix + 'divMailingAddressForm').show();
            }
        }
        //reset to ensure prefill data only one time
        currPrefillData = null;
    };
    CC.FACTORY.calculateOneTimeMembershipFee = function (sFOMName) {
        var membershipFee = 0.0;
        if ($("#hdIsComboMode").val() == "Y" && $("#pagesubmit").find("#divShowFOM").length > 0) {
            membershipFee = parseFloat($("#hdMembershipFee").val());
            if (sFOMName != undefined) {
                if (typeof CUSTOMLISTFOMFEES[sFOMName] === "string") {
                    membershipFee = membershipFee + parseFloat(CUSTOMLISTFOMFEES[sFOMName]);
                }
            }
        }
        return membershipFee;
    };
    //-------zip code pool for standard loans------------
    CC.FACTORY.hasZipCodePoolPurposes = function (zipCode) {
        //credit increase 
        //new card--> at least one new card product matches the zipcode
        //card upgrade --> at least one card upgrade product matches the zip code  
        var oCCPurposes = CREDITCARDPURPOSES;
        if ($.isEmptyObject(oCCPurposes)) {
            return false;
        }
        if (!CC.FACTORY.hasCardNames()) return true; //skip checking loan purpose if there is no card name
        for (var index in oCCPurposes) {
            var sValue = oCCPurposes[index].Item1 != undefined ? oCCPurposes[index].Item1.toUpperCase() : "";
            var sCategory = oCCPurposes[index].Item3 != undefined ? oCCPurposes[index].Item3.toUpperCase() : "";
            if (sValue.indexOf("INCREASE") > -1 || sCategory.indexOf("INCREASE") > -1) {
                return true;
            } else if (sValue.indexOf("NEW_CARD") > -1 || sCategory.indexOf("NEW") > -1) {
                if (CC.FACTORY.getCurrentCardNames(zipCode, "NEW").length > 0) return true;
            } else if (sValue.indexOf("UPGRADE") > -1 || sCategory.indexOf("UPGRADE") > -1) {
                if (CC.FACTORY.getCurrentCardNames(zipCode, "UPGRADE").length > 0) return true;
            }
        }
        return false;
    },
        CC.FACTORY.getProductItem = function (creditCardName) {
            var oCCProducts = CCPRODUCTS.ZipPoolProducts;
            if ($.isEmptyObject(oCCProducts)) {
                return null;
            }
            for (var index in oCCProducts) {
                if (creditCardName.toUpperCase() == oCCProducts[index].CreditCardName.toUpperCase()) {
                    return oCCProducts[index];
                }
            }
            return null;
        },
        CC.FACTORY.getCurrentCardNames = function (zipCode, cPrefix) {
            var newCardNames = [];
            var oCCNames = CREDITCARDNAMES;
            if ($.isEmptyObject(oCCNames)) return newCardNames;
            var selectedZipPoolNames = getSelectedZipPoolNames(zipCode, CCPRODUCTS.ZipPoolIds, CCPRODUCTS.ZipPoolNames);
            for (var index in oCCNames) {
                var sCCName = oCCNames[index].Item1 != undefined ? oCCNames[index].Item1.toUpperCase() : "";
                var sCategory = oCCNames[index].Item4 != undefined ? oCCNames[index].Item4.toUpperCase() : "";
                if (sCCName.indexOf(cPrefix) > -1 || (sCategory.indexOf(cPrefix) > -1 || sCategory.indexOf("BOTH") > -1)) {
                    var oProduct = CC.FACTORY.getProductItem(sCCName);
                    if (oProduct != null) { //do filter
                        var zpFilterType = oProduct.ZipCodeFilterType;
                        var zpCCName = oProduct.CreditCardName;
                        var zpPoolName = oProduct.ZipCodePoolName;
                        if (zpFilterType == "ALL") {
                            newCardNames.push(sCCName);
                        } else if (zpFilterType == "INCLUDE") {
                            if (zpPoolName != "" && $.inArray(zpPoolName, selectedZipPoolNames) > -1) {
                                newCardNames.push(sCCName);
                            }
                        } else if (zpFilterType == "EXCLUDE") {
                            if (zpPoolName != "" && $.inArray(zpPoolName, selectedZipPoolNames) == -1) {
                                newCardNames.push(sCCName);
                            }
                        }
                    }
                }
            }
            return newCardNames;
        },
        CC.FACTORY.hasCardNames = function () {
            return $('#divCreditCardName').find("div[data-panel='cc-options']").length > 0;
        },
        CC.FACTORY.getCreditCardTypes = function () {
            var ccTypes = [];
            if ($("#ddlCreditCardType option").length == 0) return ccTypes;
            $("#ddlCreditCardType option").each(function () {
                if ($(this).val() != undefined && $(this).val().trim() != "") ccTypes.push($(this).val());
            });
            return ccTypes;
        },
        CC.FACTORY.FilteredProductsByCCTypes = function () {
            var ccTypes = CC.FACTORY.getCreditCardTypes();
            var ccProducts = CCPRODUCTS.ZipPoolProducts;
            var ccFilteredProducts = [];
            if ($.isEmptyObject(ccTypes) || $.isEmptyObject(ccProducts)) return ccFilteredProducts;
            for (var index in ccProducts) {
                var ccType = ccProducts[index].CreditCardType;
                if ($.inArray(ccType.toUpperCase(), ccTypes) > -1) ccFilteredProducts.push(ccProducts[index]);
            }
            return ccFilteredProducts;
        },
        CC.FACTORY.getCurrentCardTypes = function (zipCode) {
            var ccTypes = [];
            var ccProducts = CC.FACTORY.FilteredProductsByCCTypes();
            var selectedZPNames = getSelectedZipPoolNames(zipCode, CCPRODUCTS.ZipPoolIds, CCPRODUCTS.ZipPoolNames);
            for (var index in ccProducts) {
                var ccType = ccProducts[index].CreditCardType;
                var zpFilterType = ccProducts[index].ZipCodeFilterType;
                var zpName = ccProducts[index].ZipCodePoolName;
                var hasCCType = false;
                if (zpFilterType == "ALL") {
                    hasCCType = true;
                } else if (zpFilterType == "INCLUDE") {
                    if (zpName != "" && $.inArray(zpName, selectedZPNames) > -1) hasCCType = true;
                } else if (zpFilterType == "EXCLUDE") {
                    if (zpName != "" && $.inArray(zpName, selectedZPNames) == -1) hasCCType = true;
                }
                if (hasCCType && $.inArray(ccType, ccTypes) == -1) {
                    ccTypes.push(ccType);
                }
            }    
            return ccTypes;
        },   
    CC.FACTORY.showAndHideZipPoolPurposes=function(zipCode) {
        var newCardNames = CC.FACTORY.getCurrentCardNames(zipCode, "NEW");
        var upgradeCardNames = CC.FACTORY.getCurrentCardNames(zipCode, "UPGRADE");
        var ccTypes = CC.FACTORY.getCurrentCardTypes(zipCode); 
        var hasCardNames = CC.FACTORY.hasCardNames(); 
            $('#divLoanPurposeList a[data-command="loan-purpose"]').each(function () {
                var $purpose = $(this);
                var sPurpose = $purpose.attr('data-key').toUpperCase();
                var sCategory = getSelectedCategory(sPurpose);
                sCategory = sCategory != null ? sCategory.toUpperCase() : "";
                if (hasCardNames) {
                   if (sPurpose.indexOf("NEW") > -1 || sCategory.indexOf('NEW') > -1) {
                        if (newCardNames.length > 0) {
                            $purpose.removeClass('hidden');
                        } else {
                            $purpose.addClass('hidden');
                        }
                    } else if (sPurpose.indexOf("UPGRADE") > -1 || sCategory.indexOf('UPGRADE') > -1) {
                        if (upgradeCardNames.length > 0) {
                            $purpose.removeClass('hidden');
                        } else {
                            $purpose.addClass('hidden');
                        }
                    } else {
                        $purpose.addClass('hidden');
                    }
                } else {
                    //no credit names: filtering loan purposes using credit card types.
                    var sPurposeType = $purpose.attr('data-key').toUpperCase().indexOf("SECURE") != -1 ? "SECUREDCREDIT" : "CREDIT";                  
                    if ($.inArray(sPurposeType, ccTypes) > -1) {   
                        $purpose.removeClass('hidden');
                    } else {
                        $purpose.addClass('hidden');
                    }           
                }
                
                //design case 303789 - filtering Credit Card Loanpurpose: no zip restrictions if loanpurpose is Line Increase
                //check the value or category attributes in the CREDIT_CARD_LOAN_PURPOSES node
                if (sPurpose.indexOf("INCREASE") > -1 || sCategory.indexOf('INCREASE') > -1) {
                    $purpose.removeClass('hidden');
                }   
                //deactive loanpurposes
                if ($purpose.hasClass('active')) {
                    $purpose.removeClass('active');
                    $purpose.removeClass('btnActive_on');
                }
        });    
        //make sure divloanpurposeList is not hidden when there are available loanpurposes for each zip pool location 
        if ($('#divLoanPurposeList a[data-command="loan-purpose"]').length > $('#divLoanPurposeList a[data-command="loan-purpose"][class~="hidden"]').length) {
            $('#divLoanPurposeList').show();           
            $('#divRequestCreditLimit').hide();
            //hide credit card type dropdown if it has credit card names section
            if (hasCardNames) $('#divCCType').addClass('hidden'); 
        } else { 
            $('#divLoanPurposeList').hide();         
        }
        $("#hdCcSelectedLoanPurpose").val(""); //reset hidden selected loan purpose
    },
    CC.FACTORY.handledLocationPoolLoanInfo=function() {
        $("#cc_txtLocationPoolCode").on("keyup paste", function () {
            var $self = $(this);
            var zipCode = $self.val();
            var $divLocationPool = $('#cc_divLocationPool');
            var $divNoAvailableProduct = $('#cc_divNoAvailableProducts');
            setTimeout(function () {
                if (/[0-9]{5}/.test(zipCode)) {
                    if (!CC.FACTORY.hasZipCodePoolPurposes(zipCode)) {
                        $divNoAvailableProduct.removeClass('hidden');
                        $divLocationPool.next('div').addClass('hidden');
                    } else {
                        var $divLoanPurpose = $('#divLoanPurposeList');
                        CC.FACTORY.showAndHideZipPoolPurposes(zipCode);
                        if ($divLoanPurpose.find('a[data-command="loan-purpose"]').length > $divLoanPurpose.find('a[data-command="loan-purpose"].hidden').length) {
                            $divNoAvailableProduct.addClass('hidden');
                            $divLocationPool.next('div').removeClass('hidden');
                        } else { 
                            //display No Available Products message if there is no available loan purpose
                            $divNoAvailableProduct.removeClass('hidden');
                            $divLocationPool.next('div').addClass('hidden');
                        }                  
                        $("#txtZip").val(zipCode);
                        $("#txtCity").val("");
                        $("#txtZip").attr("disabled", "disabled");
                        $("#txtZip").trigger("change");
                        //if puppose list only show one option-> then select the option and hide the list
                        
                        if ($divLoanPurpose.find('a[data-command="loan-purpose"]').length == 1) {
                            $divLoanPurpose.find('a[data-command="loan-purpose"]').trigger('click');
                            $divLoanPurpose.hide();//hide loan purpose 
                        } else if($divLoanPurpose.find('a[data-command="loan-purpose"]').length - $divLoanPurpose.find('a[data-command="loan-purpose"].hidden').length == 1){
                            $divLoanPurpose.find('a[data-command="loan-purpose"]:not([class~="hidden"])').trigger('click');                       
                            $divLoanPurpose.hide();//hide loan purpose                         
                        }                        
                    }
                } else {
                    $divLocationPool.next("div").addClass("hidden");
                }
            }, 400);
        });
        },
    CC.FACTORY.registerViewCreditCardInfoValidator = function () {
        $('#reviewPanel').observer({
            validators: [
                function (partial) {
                    if ($("#hdIs2ndLoanApp").val() == "Y") { //second loan app only has loan info and review pages
                        if ($('#reviewPanel .ViewCreditCardInfo').children().find("div").length == 0) {
                            return "Please complete all required fields. ";
                        }
                    } else { 
                        //do the view applicant information validation instead of view credit card loan info validation in case some lenders do not have credit card loan info              
                        if ($('#reviewPanel .ViewAcountInfo').children().find("div").length == 0) {
                            return "Please complete all required fields. ";
                        }
                    }
                    return "";
                }
            ],
            validateOnBlur: false,
            group: "ValidateViewCreditCardInfo"
        });
    }
    
}(window.CC = window.CC || {}, jQuery));