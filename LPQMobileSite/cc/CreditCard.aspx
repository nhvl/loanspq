﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="CreditCard.aspx.vb" Inherits="cc_CreditCard" %>

<%@ Import Namespace="System.Globalization" %>
<%@ Import Namespace="LPQMobile.Utils" %>
<%@ Import Namespace="Newtonsoft.Json" %>
<%@ Import Namespace="System.Web.Optimization" %>

<%@ Register Src="~/Inc/PageHeader.ascx" TagPrefix="uc" TagName="pageHeader" %>
<%@ Register Src="~/Inc/Disclosure.ascx" TagPrefix="uc" TagName="disclosure" %>
<%@ Register Src="~/Inc/MainApp/ApplicantInfo.ascx" TagPrefix="uc" TagName="applicantInfo" %>
<%@ Register Src="~/Inc/MainApp/xaApplicantID.ascx" TagPrefix="uc" TagName="ucApplicantID" %>
<%@ Register Src="~/Inc/MainApp/xaApplicantQuestion.ascx" TagPrefix="uc" TagName="xaApplicantQuestion" %>
<%@ Register Src="~/Inc/MainApp/xaApplicantAddress.ascx" TagPrefix="uc" TagName="applicantAddress" %>
<%@ Register Src="~/Inc/MainApp/FinancialInfo.ascx" TagPrefix="uc" TagName="financialInfo" %>
<%@ Register Src="~/Inc/MainApp/FundingOptions.ascx" TagPrefix="uc" TagName="ccFundingOptions" %>
<%@ Register Src="~/Inc/MainApp/xaApplicantContactInfo.ascx" TagPrefix="uc" TagName="ccContactInfo" %>
<%--<%@ Register Src="~/Inc/MainApp/UploadDocDialog.ascx" TagPrefix="uc" TagName="ucUploadDocDialog" %>--%>
<%@ Register Src="~/Inc/NewDocCapture.ascx" TagPrefix="uc" TagName="DocUpload" %>
<%@ Register Src="~/Inc/DocCaptureSourceSelector.ascx" TagPrefix="uc" TagName="DocUploadSrcSelector" %>
<%@ Register Src="~/Inc/NewDocumentScan.ascx" TagPrefix="uc" TagName="driverLicenseScan" %>
<%@ Register Src="~/Inc/MainApp/xaFOMQuestion.ascx" TagPrefix="uc" TagName="ucFOMQuestion" %>
<%@ Register TagPrefix="uc" TagName="pageFooter" Src="~/Inc/PageFooter.ascx" %>
<%@ Register TagPrefix="uc1" TagName="Piwik" Src="~/Inc/Piwik/PiwikTracking.ascx" %>
<%@ Reference Control="~/Inc/MainApp/ProductSelection.ascx" %>
<%@ Register Src="~/Inc/MainApp/CunaProtectionInfo.ascx" TagPrefix="uc" TagName="ProtectionInfo" %>
<%@ Register Src="~/Inc/LaserDocumentScan.ascx" TagPrefix="uc" TagName="LaserDriverLicenseScan" %>
<%@ Register Src="~/Inc/MainApp/ReferenceInfo.ascx" TagPrefix="uc" TagName="ReferenceInfo" %>
<%@ Register Src="~/Inc/MainApp/Asset.ascx" TagPrefix="uc" TagName="Asset" %>
<%@ Register Src="~/Inc/Declaration.ascx" TagPrefix="uc" TagName="declaration" %>
<%@ Reference Control="~/Inc/MainApp/ApplicationBlockRules.ascx" %>
<%@ Reference Control="~/Inc/MainApp/InstaTouchStuff.ascx" %>
<%@ Reference Control="~/Inc/MainApp/ApplicantAdditionalInfo.ascx" %>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">
<head runat="server">
    <title>LPQ Mobile</title>
    <%:Styles.Render("~/css/thirdparty/bootstrap") %>
    <uc:pageHeader ID="ucPageHeader" ScriptFolder="cc" runat="server" />
	<%:Styles.Render("~/css/thirdparty/custom") %>
	<script type="text/javascript">
        var SELECTEDCREDITCARDPRODUCT = <%=JsonConvert.SerializeObject(_selectedCreditCardProduct)%>;
		//prepo, required for dropdown
        $(document).ready(function () {
            var validLoanPurpose = false;
            if ('<%=LoanPurpose%>' != "" && '<%=LoanPurpose%>' != null) { // need to make sure the value is not empty or not null 
                
				$("#pl1").find("a[data-command='loan-purpose']").each(function() {
                    var $self = $(this);                  
                    if ($self.text().toLocaleUpperCase() === '<%=LoanPurpose.ToUpper()%>') {
						$("#hdCcSelectedLoanPurpose").val($self.data("key"));
                        $self.addClass("active");
                        validLoanPurpose = true;
                        return false; //exit the loop
					}
				});
            }
            handleHiddenCreditCardName('<%=CardName %>', validLoanPurpose);   
			showRequestCreditLimit(); //this is reuqired for SSO senario
        });       
		var LoanPurposeCategory = '<%=_LoanPurposeCategory%>';
		var OCCUPATIONLIST = '<%=_occupationList%>';
		var CREDITCARDLIST = '<%=_CreditCardList%>';
        var CREDITCARDPURPOSES = <%=JsonConvert.SerializeObject(_CreditCardPurposes)%>;
        var CREDITCARDNAMES=<%=JsonConvert.SerializeObject(_CreditCardNames)%>; 
		var PREFILLDATA = null;
		var COPREFILLDATA=null; 
		<%If _prefillData IsNot Nothing AndAlso _prefillData.Any() Then%>
		PREFILLDATA = <%=JsonConvert.SerializeObject(_prefillData)%>;
		<%End If%>

		<%If _coPrefillData IsNot Nothing AndAlso _coPrefillData.Any() Then%>
		COPREFILLDATA = <%=JsonConvert.SerializeObject(_coPrefillData)%>;
		<%End If%>
        var CUSTOMLISTFOMFEES = <%=JsonConvert.SerializeObject(_customListFomFees)%>;
        var CCPRODUCTS = {};
        CCPRODUCTS.ZipPoolNames =<%=JsonConvert.SerializeObject(_zipCodePoolNames)%>;
        CCPRODUCTS.ZipPoolProducts = <%=JsonConvert.SerializeObject(_ccZipCodePoolProducts)%>;
        CCPRODUCTS.ZipPoolIds =<%=JsonConvert.SerializeObject(_zipCodePoolList)%>;
	</script>
</head>
<body class = "lpq_container no-funding" >
    <uc1:Piwik id="ucPiwik" runat="server" ></uc1:Piwik>      
	<input type="hidden" id="hdPlatformSource" value="<%=PlatformSource%>" />
  	<input type="hidden" id="hfLenderRef" value='<%=_CurrentLenderRef%>' />
    <input type="hidden" id="hfBranchId" value='<%=_CurrentBranchId%>' />
    <input type="hidden" id="hdXAComboBranchId" value='<%=_XAComboBranchId%>' />
	<input type="hidden" id="hfLoanOfficerID" value='<%=_CurrentLoanOfficerId%>' />
	<input type="hidden" id="hfReferralSource" value='<%=_ReferralSource%>' />
    <%--<input type ="hidden" id="hdOnlyCreditCard" value ='<%=_isOnlyCreditCard %>' />--%>
    <input type ="hidden" id ="hdAddressKey" value ='<%= _address_key%>' />
    <input type="hidden" runat="server" id="hdForeignAppType" />
    <%--<input type="hidden" id ="hdFooterTheme" value ="<%=_bgTheme %>" />
    <input type ="hidden" id ="hdBgTheme" value="<%=_BackgroundTheme%>" />--%>
    <%--<input type="hidden" id="hdEnableJoint" runat ="server"  />--%>
    <input type="hidden" id="hdForeignContact" runat ="server"  />
    <input type="hidden" id="hdRedirectURL" value="<%=_RedirectURL%>" />
    <input type ="hidden" id="hdIdaMethodType" runat ="server" />
    <input type="hidden" runat="server" id="hdNumWalletQuestions" />
   <%-- <input type ="hidden" runat="server" id="hdLenderRef" />
    <input type ="hidden" runat ="server" id="hdLenderId" />--%>
    <input type ="hidden" id="hdScanDocumentKey" value ="<%=_hasScanDocumentKey %>" />
    <input type="hidden" id="hdHasFooterRight" value="<%=_hasFooterRight%>" />
    <input type ="hidden" id="hdRequiredDLScan" value="<%=_requiredDLScan%>" />
    <input type="hidden" id="hdPrevEmploymentThreshold" value="<%=_previous_employment_threshold%>" />
    <input type="hidden" id="hdPrevAddressThreshold" value='<%= _previous_address_threshold%>' />
    <input type="hidden" id="hdCcSelectedLoanPurpose" value="<%=IIf(IsComboMode, _ComboNewCardLoanPurpose, "")%>"/>  <%--TODO: need to come from config--%>
    <input type="hidden" id="hdCcSelectedCardType" value=""/>
    <input type="hidden" id="hdCcSelectedCardName" value=""/>
    <input type="hidden" id="hdCcHasCoApplicant" value="N"/>
	<input type="hidden" id="hdIsComboMode" value="<%=IIf(IsComboMode, "Y", "N")%>"/>
    <input type="hidden" id="hdProceedMembership" value=""/>
	<input type="hidden" id="hdEnableIDSection" value="<%=IIf(EnableIDSection, "Y", "N")%>"/>
	<input type="hidden" id="hdEnableIDSectionForCoApp" value="<%=IIf(EnableIDSection("co_"), "Y", "N")%>"/>
	
	<input type="hidden" id="hdEnableReferenceInformation" value="<%=IIf(CheckShowField("divReferenceInformation", "", True) Or (IsInMode("777") AndAlso IsInFeature("visibility")), "Y", "N") %>"/>
	<input type="hidden" id="hdEnableReferenceInformationForCoApp" value="<%=IIf(CheckShowField("divReferenceInformation", "co_", True) Or (IsInMode("777") AndAlso IsInFeature("visibility")), "Y", "N") %>"/>
	<input type="hidden" id="hdEmploymentDurationRequired"  value="<%=_isEmploymentDurationRequired%>" />
	<input type="hidden" id="hdHomePhoneRequired"  value="<%=_isHomephoneRequired%>" />
    <input type="hidden" id="hdEnableBranchSection" value="<%=IIf(EnableBranchSection AndAlso Not String.IsNullOrEmpty(_branchOptionsStr), "Y", "N")%>"/>
	<input type="hidden" id="hdRequiredBranch" value="<%=_requiredBranch%>" />
	<input type="hidden" id="hdCCMaxFunding" value='<%=_CCMaxFunding%>' />
	<input type="hidden" id="hdACHMaxFunding" value='<%=_ACHMaxFunding%>' />
	<input type="hidden" id="hdPaypalMaxFunding" value='<%=_PaypalMaxFunding%>' />
	<input type="hidden" id="hdStopInvalidRoutingNumber" value="<%=_ccStopInvalidRoutingNumber%>" />
	<input type="hidden" id="hdIs2ndLoanApp" value="<%=IIf(Is2ndLoanApp, "Y", "N")%>" />
	<input type="hidden" id="hdTotalMonthlyHousingExpense" value="<%=Common.SafeDouble(Request.Params("TotalMonthlyHousingExpense"))%>" />
	<input type="hidden" id="hdSID" value="<%=_SID%>" />
    <input type="hidden" id="hdXACrossSellComment" value="<%=_xaXSellComment%>" />
	<%--merchant--%>
	<input type="hidden" id="hfMerchantID" value='<%=_MerchantID%>' />
	<input type="hidden" id="hfClinicID_enc" value='<%=_ClinicID%>' />
	<input type="hidden" id="hfWorkerID_enc" value='<%=_WorkerID%>' />
    <input type="hidden" id="hdIsCQNewAPI" value="<%=IIf(_isCQNewAPI, "Y", "N")%>" />
    <input type="hidden" id="hdEnableDisagree" value="<%=IIf(Common.checkDisAgreePopup(_CurrentWebsiteConfig), "Y", "N")%>" />
    <input type="hidden" id="hdMembershipFee" value='<%=_MembershipFee%>' />
	<input type="hidden" id="hdVendorID" value='<%=Common.SafeEncodeString(Request.QueryString("vendorid"))%>' />
	<input type="hidden" id="hdUserID" value='<%=Common.SafeEncodeString(Request.QueryString("userid"))%>' />
	<%If LoanSavingItem IsNot Nothing Then%>
	<input type="hidden" id="hdLoanSavingID" value='<%=Common.SafeEncodeString(LoanSavingItem.LoanSavingID)%>' />
	<%End If %>
    <input type="hidden" id="hdHasZipCodePool" value="<%=IIf(_locationPool, "Y", "N") %>" />
    <input type="hidden" id="hdHasCCZipCodePool" value="<%=IIf(_ccLocationPool, "Y", "N") %>" />
    <input type="hidden" id="hdHasSSOIDA"value="<%=IIf(IsSSO And sso_ida_enable, "Y", "N") %>" />
    <input type="hidden" id="hdDocusignVeriStatus" value="" />
    <input type="hidden" id="hdHasCNameParameter" value ="<%=IIf(Not String.IsNullOrWhiteSpace(CardName), "Y", "N")%>"/>
	 
	<div>
		
     <div data-role="page" id="pl1">
	     <div data-role="header" class="sr-only">
            <h1>Credit Card Information</h1>
        </div>
        <div data-role="content">
            <%=HeaderUtils.RenderLogoAndSteps(_LogoUrl, 0, "APPLY_CC")%>
            <%=HeaderUtils.RenderPageTitle(13, "Credit Cards", False)%>        
            <%If (IsComboMode And _locationPool) Then%> <!-- display the zip code field -->
                <%If Not _showProductSelection Then %>
                     <%=HeaderUtils.RenderPageTitle(0, "No Product Available", True)%>		    
		             <%=_noAvailableProductMessage %>
                <%else %>
				    <div id="divLocationPool">					  
					    <div data-role="fieldcontain">
                            <label for="txtLocationPoolCode"> <%=HeaderUtils.RenderPageTitle(0, "Enter your ZIP Code", True, isRequired:=True) %></label>
						    <input type="<%=_textAndroidTel%>" id="txtLocationPoolCode" pattern="[0-9]*" class="inzipcode" maxlength="5"/>
					    </div>	
					    <div class="hidden" id="divNoAvailableProducts">
						    <%=HeaderUtils.RenderPageTitle(0, "No Available Products", True)%>
						    <%=_noAvailableProductMessage %>
					    </div>
				    </div>
                <%End if %>
            <%Else If not IsComboMode And _ccLocationPool  %> <!--location pool for standard loan-->
		            <div id="cc_divLocationPool">					   
					    <div data-role="fieldcontain">
                             <label for="cc_txtLocationPoolCode"> <%=HeaderUtils.RenderPageTitle(0, "Enter your ZIP Code", True, isRequired:=True) %></label>
						    <input type="<%=_textAndroidTel%>" id="cc_txtLocationPoolCode" pattern="[0-9]*" class="inzipcode" maxlength="5"/>
					    </div>	
					    <div class="hidden" id="cc_divNoAvailableProducts">
						    <%=HeaderUtils.RenderPageTitle(0, "No Available Products", True)%>
						    <%=_noAvailableProductMessage %>
					    </div>
				    </div>
            <%End If%>
           <div <%=IIf((IsComboMode And _locationPool) Or (Not IsComboMode And _ccLocationPool), "class='hidden'", "") %> > <!--hide all the fields of loan info  -->          
			        <%If EnableBranchSection AndAlso Not String.IsNullOrEmpty(_branchOptionsStr) Then%>
				        <div id="divBranchSection" <%=IIf(IsInMode("777") AndAlso IsInFeature("visibility"), "class=""showfield-section"" data-show-field-section-id=""" & BuildShowFieldSectionID("divBranchSection") & """ data-default-state=""off"" data-section-name= 'Branch'", "")%>>
			            <%--Branch location --%>
				            <div data-role="fieldcontain">
                                <%If _requiredBranch <> "N" Then%>
                                    <%=HeaderUtils.RenderPageTitle(0, "Select branch location", True, "divTitleBranchName", isRequired:=True)%>
                                <%Else%>
                                    <%=HeaderUtils.RenderPageTitle(0, "Select branch location", True, "divTitleBranchName")%> 
                                <%End If%>
                                <div id='divBranchName' class='header-theme-dropdown-btn'>
                                <select id="ddlBranchName" aria-labelledby="divTitleBranchName"><%=_branchOptionsStr%></select> 
                            </div>
                        </div> 
			        </div> 
			        <%End If%>
			                 
                    <%If _LoanPurposeList.Any() And IsComboMode = False Then%>			    
				        <div class="divLoanPurposeList" id="divLoanPurposeList">
                            <%=HeaderUtils.RenderPageTitle(0, "Select a purpose", True, isRequired:=True)%>
					        <%For Each purpose In _LoanPurposeList %>
					        <%--<a data-role="button" class="btn-header-theme" data-command="loan-purpose" style="text-align: center; padding-left: 0;"><%=StrConv(purpose, VbStrConv.ProperCase) %></a>--%>
					        <%--setting in config should use proper case--%>
					        <a data-role="button" href="#" class="btn-header-theme" data-command="loan-purpose" data-purpose-text="<%=purpose.Value%>" data-key="<%=purpose.Key %>" style="text-align: center; padding-left: 0;"><%=purpose.Value%></a>
					        <%Next %>
				        </div>
                     <%End If %>
               
                    <!--rendering credit card type -->             
                    <div id="divCCType" data-role="fieldcontain" class="<%=_HiddenCCType%>">
                        <%=HeaderUtils.RenderPageTitle(0, "Credit Card Type", True, "divCreditCardType", isRequired:=True)%>
                        <select id="ddlCreditCardType" aria-labelledby="divCreditCardType">
                            <%=_CreditCardTypeDropdown%>
                        </select>                 
                    </div>  

                     <%If _LoanPurposeList.Any() And IsComboMode = False Then%>  
				        <div id ="divRequestCreditLimit" style="display: none;">
					        <div data-role="fieldcontain">
						        <label for="txtRequestCreditLimit">Request Credit Limit</label>
						        <input type="<%=_textAndroidTel%>" pattern ="[0-9]*" id ="txtRequestCreditLimit" class ="money" maxlength="12"/>
					        </div>
  
					        <div data-role="fieldcontain">
						        <label for="eCreditCardNumber" class="RequiredIcon">Existing Credit Card Number</label>
						        <input type="<%=_textAndroidTel%>" pattern="[0-9]*" id="eCreditCardNumber" maxlength ="19" class="required inCCardNumber"/></div>
				        </div>
                      <%End If %>
			        <%--Combomode alway mean new card--%>
			        <%If IsComboMode Then%>
			            <%=HeaderUtils.RenderPageTitle(0, "Enter desired credit limit", True, "divComboCreditLimit")%>
			        <div data-role="fieldcontain">
                            <input type="<%=_textAndroidTel%>" pattern ="[0-9]*" aria-labelledby="divComboCreditLimit" id ="txtComboCreditLimit" class ="money" maxlength="12"/>
                        </div>
			        <%End If%>
					<uc:xaApplicantQuestion ID="xaApplicationQuestionLoanPage" LoanType="CC" IDPrefix="loanpage_" CQLocation="LoanPage" runat="server"  />
			        <div id="disclosure_place_holder"></div>
                        <div id="divCreditCardName" style="display:none">
                        <%If _CreditCardNames.Any() Then%>
                        <%=HeaderUtils.RenderPageTitle(0, "Select the type of credit card you are applying for", True)%>
                        <div data-panel="cc-options" class="cc-options row">
                            <%For Each card In _CreditCardNames%>
                   
                                <div class="col-sm-6 col-xs-12">
                                        <div class="header_theme2" >
                                            <div style="height: 173px;" id="<%=card.Item1.Replace(" ", "_")%>">
                                                <a href="#" data-transition="slide" onclick="validateScreen1(this);" style="display: inline-block;" class="svg-btn">
											        <%If card.Item3 = "" Then %>
												        <%=HeaderUtils.CreditCard %>
												        <div class="overlay-card-name" data-card-type="<%=card.Item2 %>" data-card-name="<%=card.Item1%>" data-card-category="<%=card.Item4%>">
													        <%=card.Item1%>
												        </div>
											        <%Else%>
												        <div  style ="width: 270px; height: 170px; margin:0 auto;"><img alt="<%=card.Item1%>" width="270" height="170" src="<%=card.Item3%>"/></div>
											        <div class="overlay-card-name" data-card-type="<%=card.Item2 %>" data-card-name="<%=card.Item1%>" data-card-category="<%=card.Item4%>"></div>
											        <%End If %>
                                                </a>
                                                <a href="#divErrorDialog" style="display: none;">no text</a>
                                            </div>

								            <%--TODO:diplay only when there is secured cc, no point for existing member because can't do funding--%> 
                                            <% If _CreditCardNames.Last().Item1 = card.Item1 And _CurrentLenderRef.ToUpper.Contains("SDFCU") Then%>    
                                            <div style="text-align: right; padding-right: 5px;"><a href="#popUpCreditDiff" data-rel='popup' data-transition='pop' style="color: inherit;">What is the difference?</a></div>
                                            <%End If%>
                                        </div>
                                </div>
                            <%Next%>
				            <%--TODO:make this configurable default to hide, configurable text--%>
                            <div id="popUpCreditDiff" data-role="popup" data-position-to="window" style="max-width: 400px;">
                                <div data-role="content" data-theme="<%=_ContentDataTheme%>">
                                    <div class="row">
                                        <div class="col-xs-12 header_theme2">
                                            <a data-rel="back" href="#" class="pull-right svg-btn"><%=HeaderUtils.IconClose %><span style="display: none;">close</span></a>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <div style="margin: 10px 0;">
									            <%If _CurrentLenderRef.ToUpper.StartsWith("SDFCU") Then%>
                                                Our Savings Secured Visa Platinum Card is designed for those applicants looking to establish credit. To apply for this card, applicants are required to deposit a minimum of $250.00 into their Regular Share Savings Account. The  deposit amount can very, but is typically equal to the card's limit. For example, if the card limit were $300, the deposit amount must also be $300.
									            <%Else%>
									            Most credit cards are unsecured, which means they don’t require a deposit as collateral in case cardholders can’t pay off their debt. However, it is sometimes difficult for members with bad credit or no credit history to qualify for our unsecured credit card. Our Secured Card is designed for people with bad credit or no credit and made to help improve their credit worthiness. Since these members are considered a bigger risk by card issuers, they’re required to make a minimum deposit to their Regular Share Savings Account. The deposit amount can vary, but it’s typically equal to the credit card’s credit limit. For example, if the credit limit were $300, the deposit would also be $300.
									            <%End If%>
								            </div>       
                                        </div>    
                                    </div>
                                </div>
                            </div>
                        </div>

                        <%End If%>
                    </div>
                    <%--<div id="CCTypeDiv" runat="server" >
                        <div data-role="fieldcontain">
                            <label for="ddlCreditCardType">Credit Card Type<span style ='color :Red'>*</span></label>
                            <select id="ddlCreditCardType">
                                <%=_CreditCardTypeDropdown%>
                            </select>          
                        </div>
                    </div>--%>
            
                    <%--CONVERT CREDIT CARD DROPDOWN TO IMAGES (CHANGE REQUESTS 09232016)--%>
                    <%--<div id="CCNameDiv" runat="server">
                        <div data-role="fieldcontain">
                            <label for="ddlCreditCardName">Credit Card Name<span style ='color :Red'>*</span></label>
                            <select id="ddlCreditCardName">
                                <%=_CreditCardNameDropdown%>
                            </select>
                        </div>
                    </div>--%>
                    <%--CONVERT LOAN PURPOSE DROPDOWN TO BUTTONS (CHANGE REQUESTS 201160922)--%>
                    <%--<% =_LoanPurposeSection%>--%>
                
                    <%--If _CreditCardNames.Any() = false Then0--%>
                    <div class ="div-continue"  data-role="footer">
                        <a href="#"  data-transition="slide" onclick="validateScreen1(this);" type="button" class="div-continue-button">Continue</a> 
                        <a href="#divErrorDialog" style="display: none;">no text</a>
				        <%--skip 2nd page if reffered  from another loan(TotalMonthlyHousingExpense <> 0) and  not XA--%>
				        <a href='<%=IIf(Is2ndLoanApp = True AndAlso Common.SafeDouble(Request.Params("TotalMonthlyHousingExpense")) > 0, "#pagesubmit", "#pl2")%>' style="display: none;">no text</a>
                    </div>     
                    <%--End If--%>
            </div> <!--end div isComboMode and _locationPool -->
       </div><!--end div data-role = "content" -->   
    </div>
		<asp:PlaceHolder runat="server" ID="plhApplicantAdditionalInfo"></asp:PlaceHolder>
		
		<asp:PlaceHolder runat="server" ID="plhApplicationDeclined"></asp:PlaceHolder>
		<asp:PlaceHolder runat="server" ID="plhInstaTouchStuff"></asp:PlaceHolder>
		<div data-role="page" id="pageApplicationCancelled">
			<div data-role="header" style="display: none">
				<h1>Application Cancelled</h1>
			</div>
		  <div data-role="content">
			  <%=HeaderUtils.RenderLogoAndSteps(_LogoUrl, 0, "APPLY_CC")%>
			  <br/>
			  <div class="text-center">
			    <%=HeaderUtils.RenderPageTitle(0, "Application Cancelled", False)%>
				</div>
			  <br/>
			  <p class="text-center">We have cancelled your application.</p>
			  <p class="text-center">If this was a mistake and you would like to fix errors you can return to the application now.</p>
			  <br/>

				<div class="row div-continue" style="margin-top: 10px; text-align: center; width:100%;" data-role="footer">
					<div class="col-md-2 hidden-xs"></div>
					<div class="col-md-4 col-xs-12">
						<a href="#" onclick="ABR.FACTORY.closeApplicationBlockRulesDialog()" type="button" data-role="button" class="div-continue-button">Return to Application</a>	
					</div>
					<div class="col-md-4 col-xs-12">
						<a href="#" onclick="gotoToUrl(this,'<%=_RedirectURL%>')" data-role="button" type="button" class="div-continue-button">Close</a>
						</div>
					<div class="col-md-2 hidden-xs"></div>
				</div>
		   </div>
	 </div>   
     <%--<div style="display :none" data-role="footer" data-theme="<%=_bgTheme%>" class="ui-bar-<%=_bgTheme %>" ></div>--%>
     <!-- about you(pl2): applicant infor + applicant adddress -->
     <div data-role="page" id="pl2">
        <div data-role="header" class="sr-only">
            <h1>Applicant Information</h1>
        </div>
        <div data-role="content">
            <%=HeaderUtils.RenderLogoAndSteps(_LogoUrl, 1, "APPLY_CC")%>
            <%=HeaderUtils.RenderPageTitle(21, "Tell Us About Yourself", false)%>
            <%--<%=HeaderUtils.RenderPageTitle(11, "Tell Us About Yourself", false)%>
            <%=HeaderUtils.RenderPageTitle(13, "Tell Us About Yourself", false)%>--%>
            <%=HeaderUtils.RenderPageTitle(18, "Personal Information", true)%>
            <uc:applicantInfo ID="ucApplicantInfo" IDPrefix="" runat="server" LoanType="CC"/>

              <%-- Reference info--%>
			<%If CheckShowField("divReferenceInformation", "", True) Or (IsInMode("777") AndAlso IsInFeature("visibility")) Then%>
            <uc:ReferenceInfo id="ucReferenceInfo" IDPrefix="" runat="server" />
			<%end if %>

            <%=HeaderUtils.RenderPageTitle(22, "Contact Information", true)%>
            <uc:ccContactInfo ID="ucContactInfo" IDPrefix="" runat="server" />
            <%=HeaderUtils.RenderPageTitle(14, "Current Physical Address", True)%>
            <uc:applicantAddress ID="ucApplicantAddress" LoanType="CC" IDPrefix="" runat="server"  />
            <%If EnableIDSection Then%>
            <uc:ucApplicantID ID="ucApplicantID" runat="server" />
            <%End If%>
            <%=HeaderUtils.RenderPageTitle(23, "Financial Information", true)%>
            <uc:financialInfo ID="ucFinancialInfo" IDPrefix="" runat="server" />
			<uc:DocUpload ID="ucDocUpload" IDPrefix="" runat="server" />
            <uc:Asset id="ucAssetInfo" IDPrefix="" runat="server"/>

           <%If _declarationList IsNot Nothing AndAlso _declarationList.Count > 0 Then%>
            <div>
                <%=HeaderUtils.RenderPageTitle(15, "Declaration", true)%>
				<div style="clear: left; margin-top: 10px;"></div>
                <uc:declaration ID="ucDeclaration" IDPrefix="" LoanType="CC" runat="server" />
            </div>
			<%End If %>
			<% If _isCQNewAPI Then%>
                <%If IsComboMode Then%>
                    <uc:xaApplicantQuestion ID="xaComboApplicantQuestion" LoanType="CC" isComboMode="true" IDPrefix="" CQLocation="ApplicantPage" runat="server"  />
                <%Else%>
                    <uc:xaApplicantQuestion ID="applicantQuestion" LoanType="CC" isComboMode="false" IDPrefix="" CQLocation="ApplicantPage" runat="server"  />
                <%End If  %>
      		<%Else%>
                <%If IsComboMode Then%>
                   <uc:xaApplicantQuestion ID="xaApplicantQuestion" LoanType="CC" IDPrefix="" CQLocation="ApplicantPage" runat="server" />
                <%End If%>
            <%End If %>
         </div> 
         <div class="div-continue" data-role="footer">
            <%If _enableJoint Then%>           
                <a  type="button" class="div-continue-button div-continue-coapp-button" href="#"  id="continueWithoutCoApp" onclick="validateScreen2(this);">Continue  without Co-Applicant</a>
                <a  type="button" class="div-continue-button div-continue-coapp-button" href="#" id="continueWithCoApp"  onclick="validateScreen2(this);" >Continue with Co-Applicant</a>                       
             <%Else%>
                <a href="#"  data-transition="slide" onclick="validateScreen2(this);" type="button" class="div-continue-button">Continue</a> 
            <%End If%>
                <a href="#divErrorDialog" style="display: none;">no text</a>
               <span> Or</span>  <a href="#pl1" class ="div-goback" data-corners="false" data-shadow="false" data-theme="reset"><span class="hover-goback"> Go Back</span></a>
			 <%--<%If IsFinishLaterEnabled Then%>
			 <a style="display: block;" class="div-finish-later" onclick="saveAndFinishLater()" >Save & Finish Later</a>
			 <%End If%>--%>
        </div>
       
    </div>
	<%If LegacyDLScanEnabled OrElse LaserDLScanEnabled Then%>
    <div data-role="dialog" id="scandocs">
        <div data-role="header" style="display:none" >
                <h1>Driver's License Scan</h1>
        </div>
        <div data-role="content">
	        <%If LaserDLScanEnabled Then%>
			<div class="js-laser-scan-container">
			<a data-rel="back" href="#" class="svg-btn btn-close-dialog"><%=HeaderUtils.IconClose %><span style="display: none;">close</span></a> 
			<uc:LaserDriverLicenseScan ID="laserDriverlicenseScan" IDPrefix="" runat="server" />
			</div>
			<%End If %>
				<div class="js-legacy-scan-container <%=IIf(LaserDLScanEnabled, "hidden", "") %>" > <a data-rel="back" href="#" style="margin-bottom: 5px;" class="pull-right svg-btn"><%=HeaderUtils.IconClose %><span style="display: none;">close</span></a>   
				<uc:driverLicenseScan ID="DriverlicenseScan" IDPrefix="" runat="server" />
				<div class="div-continue" style="text-align: center;">
					<a href="#" onclick="ScanAccept('')" data-transition="slide" type="button" data-role="button" class="div-continue-button">Done</a> 
					<a href="#divErrorDialog" style="display: none;">no text</a>
					<a href='#pl2' style="display: none;">no text</a>   
				</div>
			</div>
        </div>
        
    </div>
    <%End If %>
        
    <div data-role="page" id="pagesubmit">
        <div data-role="header" class="sr-only">
            <h1>Review and Submit</h1>
        </div>
        <div data-role="content">
            <%=HeaderUtils.RenderLogoAndSteps(_LogoUrl, 2, "APPLY_CC")%>
			<%If IsComboMode then%>
            <div id="divSecureCardFundingHolder"></div>
			<div id="divCcFundingOptions" style="margin-top: 40px;">  <%--This section is only available for ComboMode, TODO:may need to create a separate page--%>
            <%=HeaderUtils.RenderPageTitle(23, "Funding", True)%> 
			<p class="header_theme" style="font-weight: bold; font-size: 16px; padding-left: 3px;margin-top: 2px;"><%=IIf(String.IsNullOrEmpty(_comboFundingHeader), "Your funding will not be transferred until all your accounts are approved.", _comboFundingHeader)%></p> 
                <div id="DivDepositInput">
                    <%--<span class="ProductCQ-Font">How much do you want to deposit?</span>--%>
                    <%=HeaderUtils.RenderPageTitle(0, "How much do you want to deposit?", True)%>

					<%--only show for sdfcu with secured cc--%>
					<%If _CurrentLenderRef.ToUpper.StartsWith("SDFCU") Then%>
					  <div id="lblMinDepositAmount" style ="display:none">
                    	<i style="font-weight: normal; margin-left: 30px;"><%=_prerequisiteProduct.AccountName %> ($ 251.00 min deposit)</i>
						<span  onclick='openPopup("#popUpSavingAmountGuid")' data-rel='popup' data-transition='pop'><div class='questionMark'></div></span>
                      </div>
					<%End If%>
					<%--TODO: need to show this if any other lender need to show funding for secure cc, similar to sdfcu--%>
					<%--<% (hdCcSelectedCardType === "SECUREDCREDIT") then %>
					<i style="font-weight: normal; margin-left: 30px;"><%=_prerequisiteProduct.AccountName %> ($ <%=Common.SafeInteger(_prerequisiteProduct.MinimumDeposit)%>.00 min deposit)</i>
					<%End If%>--%>
					
                    <%--<%
						Dim maxFunding As Decimal = 0D
						If String.IsNullOrEmpty(_MaxFunding) = False AndAlso Decimal.TryParse(_MaxFunding, maxFunding) = True Then%>
						<div><i style="font-weight: normal; margin-left: 30px;">Maximum deposit amount allowed is <%=maxFunding.ToString("C2", New CultureInfo("en-US")) %></i></div>
					<%End If%>--%>
					<%--onblur="validateMaxFunding(this)"--%>
					<div class="LineSpace" id="divDepositProductPool"></div>
                     
                    <%If _MembershipFee > 0 Or _customListFomFees.Count > 0 Then%>
					         <div data-role="fieldcontain">
						        <label for="divMembershipFee">One-time Membership Fee</label>
						        <div id="divMembershipFee" data-membershipfee="<%=_MembershipFee.ToString%>"> <%=_MembershipFee.ToString("C2", New CultureInfo("en-US")) %></div>
					        </div>
					 <%End If%>

					<div data-role="fieldcontain" style="margin-top: 0px;">
						<label for="txtFundingDeposit" class="RequiredIcon" style="font-weight: bold;">Total Deposit</label>
                        <input type="<%=_textAndroidTel%>" aria-labelledby="DivDepositInput" id="txtFundingDeposit" class="money"  value="<%=0D.ToString("C2", New CultureInfo("en-US")) %>"/>
                    </div>
                </div>
                <uc:ccFundingOptions ID="ccFundingOptions" runat="server" />
                <div id="popUpSavingAmountGuid" data-role="popup" data-position-to="window" style="max-width: 400px;">
                    <div data-role="content">
	                    <div class="row">
							<div class="col-xs-12 header_theme2">
								<a href="#" data-rel="back" class="pull-right svg-btn"><%=HeaderUtils.IconClose %><span style="display: none;">close</span></a>
							</div>
						</div>
                        <div class="row">
                            <div class="col-sm-12">
                                <div style="margin: 10px 0;">
									<%If _CurrentLenderRef.ToUpper.StartsWith("SDFCU") Then%>
                                    A minimum deposit of $251.00 included the $1.00 fee in your <%=_prerequisiteProduct.AccountName %> Account is required to fund the Savings account for the Secured Platinum Visa card only. Please enter the amount that you wish to deposit and secure on your new card.
									<%Else%>
									A minimum deposit of $<%=Common.SafeInteger(_prerequisiteProduct.MinimumDeposit)%> in your <%=_prerequisiteProduct.AccountName %> Account is required to fund your Secured Credit Card. Please enter the amount that you wish to deposit and secure on your new card.
									<%End If%>
                                </div>    
                            </div>    
                        </div>
                    </div>
                </div>
				<br />
            </div> <%--end of divCcFundingOptions--%>
			<%End If%>  <%--//combo section--%>

			<%=HeaderUtils.RenderPageTitle(22, "Review and Submit", False)%>
			<div id="reviewPanel" class="review-container container-fluid" data-role="fieldcontain">
                
                <%--Branch selections --%>
                 <div class="row" style="display :none">
                        <div class="row-title section-heading">
                            <span class="bold">Branch Selection</span>
                        </div>
                  </div>
                 <div class="row panel BranchSelection"></div>

                <div class="row">
                    <div class="row-title section-heading"><span class="bold">Credit Card Information</span></div>
                </div>
                <div class="row panel ViewCreditCardInfo"></div>
				<%If Is2ndLoanApp = False Then%>
                <div class="row">
                    <div class="row-title section-heading"><span class="bold">Applicant Information</span></div>
                </div>
                <div class="row panel ViewAcountInfo"></div>
                <%If NSSList IsNot Nothing AndAlso NSSList.Count > 0 Then%>
				<div class="row" style="display: none;">
                    <div class="row-title section-heading"><span class="bold">Your Spouse's Information</span></div>
                </div>
                <div class="row panel ViewPrimaryAppSpouseInfo" style="display: none;"></div>
				<%End If%>
                <%--View Reference --%>
				<%If CheckShowField("divReferenceInformation", "", True) Or (IsInMode("777") AndAlso IsInFeature("visibility")) Then%>
                <div class="row">
                    <div class="row-title section-heading"><span class="bold">Reference Information</span></div>
                </div> 
                 <div class="row panel ViewReferenceInfo"></div>
				<%End If%>
                <div class="row">
                    <div class="row-title section-heading">
                        <span class="bold">Applicant Contact Information</span>        
                    </div>
                </div>
                <div class="row panel ViewContactInfo"></div>
                <div class="row">
                    <div class="row-title section-heading">
                        <span class="bold">Address</span>        
                    </div>
                </div>
                <div class="row panel ViewAddress"></div>
                <%If EnableIDSection Then%>
                <div class="row">
                    <div class="row-title section-heading">
                        <span class="bold">Your Identification</span>        
                    </div>
                </div>
                <div class="row panel ViewApplicantID"></div>
                <%End If %>
                <div class="row">
                    <div class="row-title section-heading">
                        <span class="bold">Financial Information</span>        
                    </div>
                </div>
                <div class="row panel ViewFinancialInfo"></div>
                <div class="row">
                    <div class="row-title section-heading">
                        <span class="bold">Previous Employment Information</span>        
                    </div>
                </div>
                <div class="row panel ViewPrevEmploymentInfo"></div>
				<%If _declarationList IsNot Nothing AndAlso _declarationList.Count > 0 Then%>
                    <div class="row" style="display: none;">
                        <div class="row-title section-heading">
                            <span class="bold">Declaration</span>        
                        </div>
                    </div>
                    <div class="row panel ViewDeclaration" style="display: none;"></div>
				<%End If%>
                <div class="row jna-panel" style="display: none;">
                    <div class="row-title section-heading">
                        <span class="bold">Joint Applicant Information</span>        
                    </div>
                </div>
                <div class="row panel jna-panel ViewJointApplicantInfo" style="display: none;"></div>
				<%If NSSList IsNot Nothing AndAlso NSSList.Count > 0 Then%>
				<div class="row jna-panel" style="display: none;">
                    <div class="row-title section-heading">
                        <span class="bold">Joint Applicant Spouse's Information</span>        
                    </div>
                </div>
				<div class="row panel jna-panel ViewCoAppSpouseInfo" style="display: none;"></div>
				<%End If%>
                  <%--View Joint Reference --%>
				<%If CheckShowField("divReferenceInformation", "co_", True) Or (IsInMode("777") AndAlso IsInFeature("visibility")) Then%>
                <div class="row jna-panel" style="display: none;">
                    <div class="row-title section-heading"><span class="bold">Joint Reference Information</span></div>
                </div> 
                 <div class="row panel jna-panel ViewJointReferenceInfo" style ="display:none;"></div>
				<%End If%>
                <div class="row jna-panel" style="display: none;">
                    <div class="row-title section-heading">
                        <span class="bold">Joint Applicant Contact Information</span>        
                    </div>
                </div>
                <div class="row panel jna-panel ViewJointApplicantContactInfo" style="display: none;"></div>
                <div class="row jna-panel" style="display: none;">
                    <div class="row-title section-heading">
                        <span class="bold">Joint Applicant Address</span>        
                    </div>
                </div>
                <div class="row panel jna-panel ViewJointApplicantAddress" style="display: none;"></div>
                <%If EnableIDSection("co_") Then%>
                <div class="row jna-panel" style="display: none;">
                    <div class="row-title section-heading">
                        <span class="bold">Joint Applicant Identification</span>        
                    </div>
                </div>
                <div class="row panel jna-panel ViewJointApplicantID" style="display: none;"></div>
                <%End If%>
                <div class="row jna-panel" style="display: none;">
                    <div class="row-title section-heading">
                        <span class="bold">Joint Applicant Financial Information</span>        
                    </div>
                </div>
                <div class="row panel jna-panel ViewJointApplicantFinancialInfo" style="display: none;"></div>
                <div class="row jna-panel" style="display: none;">
                    <div class="row-title section-heading">
                        <span class="bold">Joint Applicant Previous Employment Information</span>        
                    </div>
                </div>
                <div class="row panel jna-panel ViewJointApplicantPrevEmploymentInfo" style="display: none;"></div>
				<%If _declarationList IsNot Nothing AndAlso _declarationList.Count > 0 Then%>
                    <div class="row jna-panel" style="display: none;">
                        <div class="row-title section-heading">
                            <span class="bold">Joint Applicant Declaration</span>        
                        </div>
                    </div>
                    <div class="row panel jna-panel ViewJointApplicantDeclaration" style="display: none;"></div>
					<%End If %>
				<%End If%>
                     <%--View Applicant Questions --%>
                  <div class="row" style="display: none;">
                        <div class="row-title section-heading">
                            <span class="bold">Additional Information</span>        
                        </div>
                    </div>
                    <div class="row panel ViewApplicantQuestion" style="display: none;"></div>
                       <%--view Joint Applicant Questions --%>
                  <div class="row panel co_ViewApplicantQuestion" style="display: none;"></div>
						<div class="row" style="display: none;">
							<div class="row-title section-heading">
								<span class="bold">Joint Additional Information</span>        
							</div>
					</div>
					<div class="row panel ViewJointApplicantQuestion" style="display: none;"></div>         
            </div>
            <%If IsComboMode Then%>
			<div id="div_fom_section" class="<%=IIf(_HideXAFOM, "hidden", "")%>">
            <%=HeaderUtils.RenderPageTitle(18, "Eligibility", True, isRequired:=true)%>
            <p class="header_theme rename-able" style="font-weight: bold; font-size: 16px; padding-left: 3px;margin-top: 2px;"><%=IIf(String.IsNullOrEmpty(_comboEligibilityHeader), "Because membership is required to take advantage of this wonderful loan product, please tell us how you qualify for membership.", _comboEligibilityHeader)%></p>
            <uc:ucFOMQuestion runat="server" ID='ucFomQuestion' />
			</div>
			<div data-role="fieldcontain">
				<%=HeaderUtils.RenderPageTitle(0, "If I am not approved for the above loan product,", True, isRequired:=True)%>
				<div data-section="proceed-membership" id="divProceedMembership">
                   <a data-role="button" href="#" class="btn-header-theme rename-able" data-command="proceed-membership" data-key="Y" style="text-align: center; padding-left: 0;"><%:IIf(_CurrentWebsiteConfig.InstitutionType = CEnum.InstitutionType.BANK, "I wish to continue opening my account.", "I wish to continue opening my membership.")%></a>
                   <a data-role="button" href="#" class="btn-header-theme rename-able" data-command="proceed-membership" data-key="N" style="text-align: center; padding-left: 0;"><%:IIf(_CurrentWebsiteConfig.InstitutionType = CEnum.InstitutionType.BANK, "I don't wish to continue opening my account.", "I don't wish to continue opening my membership.")%></a>
                     
                </div>
			</div>
            <%End If%>
			<%If IsComboMode and _showProductSelection = true Then %>
			<asp:PlaceHolder runat="server" ID="plhProductSelection"></asp:PlaceHolder>
			<%End If%>
           
			<uc:xaApplicantQuestion ID="xaApplicationQuestionReviewPage" LoanType="CC" IDPrefix="reviewpage_" CQLocation="ReviewPage" runat="server" />
          
            <!--Credit Life and Insurance info -->        
            <uc:ProtectionInfo ID="ccProtectionInfo" runat="server" LoanType ="CC" />
          
			<div id="divDisclosure">
            <%=HeaderUtils.RenderPageTitle(17, "Read, Sign and Submit", True)%>
             <div style="margin-top: 10px;">
                <div id="divSubmitMessage">
				<%If Not String.IsNullOrEmpty(_CreditPullMessage) Then%>
					<p class="rename-able bold"><%=_CreditPullMessage%></p>
                    <%Else%>  
					<p class="rename-able bold">Your application is not complete until you read the disclosure below and click the “I Agree” button in order to submit your application.</p>
					<p class="rename-able">You are now ready to submit your application! By clicking on "I agree", you authorize us to verify the information you submitted and may obtain your credit report. Upon your request, we will tell you if a credit report was obtained and give you the name and address of the credit reporting agency that provided the report. You warrant to us that the information you are submitting is true and correct. By submitting this application, you agree to allow us to receive the information contained in your application, as well as the status of your application.</p>                     
                <%End If%>
                 </div> 
				<uc:disclosure ID="ucDisclosures" LoanType="CC" runat="server" Name="disclosures" />                  
            </div>
			<br />
             <div id="divDisagree" data-role="fieldcontain">
                <a href="#" class="header_theme2 shadow-btn chevron-circle-right-before" style="cursor: pointer; font-weight: bold;">I disagree</a>
            </div>
			</div>

        </div>
        <div class="div-continue" data-role="footer">
            <a href="#"  id="credit-card-submit" data-transition="slide" onclick="validatePageSubmit(this);" type="button" class="div-continue-button">I Agree</a> 
            <a href="#divErrorDialog" style="display: none;">no text</a>
            <a href="#pl5" style="display: none;">no text</a> 
            Or <a href="#" class ="div-goback" data-corners="false" data-shadow="false" onclick="goBackPageSubmit();" data-theme="reset"><span class="hover-goback"> Go Back</span></a>   
            <a id="href_show_last_dialog" href="#cc_last" style="display: none;">no text</a>
            <%--<a id="href_show_dialog_1" href="#divErrorDialog" data-rel="dialog" style="display: none;"></a>--%>
			<%--<%If IsFinishLaterEnabled Then%>
			 <a style="display: block;" class="div-finish-later" onclick="saveAndFinishLater()">Save & Finish Later</a>
			 <%End If%>--%>
        </div>
    </div>
	<%If IsComboMode and _showProductSelection %>
	<div id="productDetail" data-role="dialog" data-close-btn="none" style="width: 100%;">
        <div data-role="header">
            <button class="header-hidden-btn">.</button>
            <div data-place-holder="heading" class="page-header-title"></div>
            <div tabindex="0" data-main-page="pagesubmit" data-command="close" class="header_theme2 ui-btn-right" data-role="none">
                <%=HeaderUtils.IconClose %><span style="display: none;">close</span>
            </div>
        </div>
        <div data-role="content" class="fieldset-container">
            <fieldset data-role="controlgroup" data-place-holder="description">
                <legend class="heading-title">Description</legend>
                <p style="margin-bottom: 0px;"></p> 
            </fieldset>
			<fieldset data-place-holder="rates">
				<legend class="heading-title">Details</legend>
			</fieldset>
            <fieldset data-place-holder="services">
                <legend class="heading-title">Select feature(s)</legend>
                <div class="ui-controlgroup-controls"></div>
            </fieldset>
            <fieldset data-place-holder="questions">
                <legend class="heading-title">Additional feature(s)</legend>
				<div class="ui-controlgroup-controls"></div>
            </fieldset>
            <div style="text-align: center;">
				<a href="#" data-role="button" data-mode="self-handle-event" type="button" data-main-page="pagesubmit" data-inline="true" id="btnAddProduct">Add Account</a>
				<a href="#" data-role="button" data-mode="self-handle-event" type="button" data-main-page="pagesubmit" data-inline="true" id="btnSaveProduct">Save Account</a>
				<a href="#" data-role="button" data-mode="self-handle-event" type="button" data-main-page="pagesubmit" data-inline="true" style="min-width: 150px;" id="btnCloseProductDetail">OK</a>
				<input type="hidden" value="" data-place-holder="productCode" />
            </div>    
        </div>
    </div>
	<div id="fundingDialog" data-role="dialog" data-close-btn="none" style="width: 100%;">
		<div data-role="header">
			<button class="header-hidden-btn">.</button>
			<div data-place-holder="heading" class="page-header-title"></div>
			<div tabindex="0" data-command="close" data-main-page="pagesubmit" class="header_theme2 ui-btn-right" data-role="none">
				<%=HeaderUtils.IconClose %><span style="display: none;">close</span>
			</div>
		</div>
		<div data-role="content" class="fieldset-container">
			<fieldset data-place-holder="rates">
				<legend class="heading-title">Choose your rate</legend>
			</fieldset>
			<fieldset data-place-holder="deposit">
				<legend class="heading-title">Specify the amount to deposit</legend>
				<div class="row">
					<div class="col-xs-12 col-sm-4 no-padding">
						<div data-role="fieldcontain">
							<input type="text" class="money" maxlength="12" id="txtRateDepositAmount"/>
						</div>
					</div>
				</div>
			</fieldset>
			<fieldset data-place-holder="term">
				<legend class="heading-title">Choose your term</legend>
				<div class="row">
					<div class="col-xs-12 col-sm-4 no-padding">
						<div data-role="fieldcontain">
							<label for="txtRateTermLength" id="lblRateTermLength" class="RequiredIcon"></label>
							<input type="<%=TextAndroidTel%>" class="number-only" maxlength="12" id="txtRateTermLength"/>
						</div>
					</div>
					<div class="col-xs-12 col-sm-8 no-padding maturity-date-wrapper">
						<div class="row">
							<div class="col-xs-12 col-sm-2 no-padding text-center-sm">
								<div data-role="fieldcontain">
									<label class="hidden-xs">&nbsp;</label>
									<label>Or</label>
								</div>
							</div>
							<div class="col-xs-12 col-sm-10 no-padding">
								<div data-role="fieldcontain" id="divRateMaturityDateStuff">
									<div class="row">
										<div class="col-xs-8 no-padding">
											<label for="txtRateMaturityDate" id="lblRateMaturityDate" class="RequiredIcon">Maturity Date</label>
										</div>
										<div class="col-xs-4 no-padding"><input type="hidden" id="txtRateMaturityDate" class="combine-field-value" value="" /></div>
									</div>
									<div id="divRateMaturityDate" class="ui-input-date row">
										<div class="col-xs-4">
											<input aria-labelledby="lblRateMaturityDate" type="<%=TextAndroidTel%>" pattern="[0-9]*" placeholder="mm" id="txtRateMaturityDate1" maxlength ='2' onkeydown="limitToNumeric(event);" onkeyup="onKeyUp(event,'#txtRateMaturityDate1','#txtRateMaturityDate2', '2');" value="" />
										</div>
										<div class="col-xs-4">
											<input aria-labelledby="lblRateMaturityDate" type="<%=TextAndroidTel%>" pattern="[0-9]*" placeholder="dd" id="txtRateMaturityDate2" maxlength ='2' onkeydown="limitToNumeric(event);" onkeyup="onKeyUp(event,'#txtRateMaturityDate2','#txtRateMaturityDate3', '2');" value="" />
										</div>
										<div class="col-xs-4" style="padding-right: 0px;">
											<input aria-labelledby="lblRateMaturityDate" type="<%=TextAndroidTel%>" pattern="[0-9]*" placeholder="yyyy" id="txtRateMaturityDate3" onkeydown="limitToNumeric(event);" maxlength ='4' value="" />
										</div>
									</div>
								</div>	
							</div>	
						</div>
					</div>
				</div>
			</fieldset>
			<br/>
			<div style="text-align: center;">
				<a href="#" data-role="button" data-mode="self-handle-event" type="button" data-main-page="pagesubmit" data-inline="true" style="min-width: 150px;" id="btnAcceptRate">Accept</a>
				<input type="hidden" value="" data-place-holder="productCode" />
				<input type="hidden" value="" data-place-holder="instanceId" />
			</div>    
		</div>
	</div>
    <div data-role="dialog" data-close-btn="none" id="fundingInternalTransfer">
        <div data-role="header" data-position="" style="display: none;">
            <h1>Fund via Internal Fund</h1>
        </div>
        <div data-role="content">
            <div class="row">
                <div class="col-xs-12 header_theme2" style="padding-right: 0px;">
                    <a href="#" onclick="goBackFInternalTransfer(this)" class="pull-right svg-btn"><%=HeaderUtils.IconClose %><span style="display: none;">close</span></a>
                </div>
            </div>
            <div data-placeholder="content"></div>
            <div class="div-continue" style="text-align: center;">
                <a href="#" data-transition="slide" onclick="validateFInternalTransfer(this, true);" type="button" data-role="button" class="div-continue-button">Done</a> 
                <a href="#divErrorDialog" style="display: none;">no text</a>
                <%--Or <a href="#" onclick="goBackFInternalTransfer(this)" class ="div-goback" data-corners="false" data-shadow="false" data-theme="reset"><span class="hover-goback"> Go Back</span></a>   --%>
            </div>
        </div>
    </div>
    <div data-role="dialog" data-close-btn="none" id="fundingCreditCard">
        <div data-role="header" data-position="" style="display: none;">
            <h1>Fund via Credit Card</h1>
        </div>
        <div data-role="content">
            <div class="row">
                <div class="col-xs-12 header_theme2" style="padding-right: 0px;">
                    <a href="#" onclick="goBackFCreditCard(this)" class="pull-right svg-btn"><%=HeaderUtils.IconClose %><span style="display: none;">close</span></a>
                </div>
            </div>
            <div data-placeholder="content" style="overflow: auto; padding: 4px;"></div>
            <div class ="div-continue" style="text-align: center;">
                <a href="#" data-transition="slide" onclick="validateFCreditCard(this);" type="button" data-role="button" class="div-continue-button">Done</a> 
                <a href="#divErrorDialog" style="display: none;">no text</a>
                <%--Or <a href="#" onclick="goBackFCreditCard(this)" class ="div-goback" data-corners="false" data-shadow="false" data-theme="reset"><span class="hover-goback"> Go Back</span></a>--%>   
            </div>
        </div>
    </div>
	<div id="popUpUndefineCard" data-role="popup" style="max-width: 500px; padding: 20px;">
		<div data-role="header" style="display:none" >
				<h1>Alert Popup</h1>
		</div>
		<div data-role="content">
			<div class="DialogMessageStyle">There was a problem with your request</div>
			<div style="margin: 20px 0 30px; font-weight: normal;" data-place-holder="content"></div>
		</div>
		<div class="div-continue js-no-required-field" style="text-align: center;margin-bottom: 30px;">
			<a href="#" data-transition="slide" type="button" onclick="closePopup('#popUpUndefineCard')" data-role="button" class="div-continue-button" style="display: inline;">Let Me Fix</a>
			<a href="#" data-transition="slide" type="button" data-command="remove-product" data-role="button" class="div-continue-button" style="display: inline;" onclick="validateFCreditCard('#fundingCreditCard', true)">Continue</a>
		</div>
	</div>
    <div data-role="dialog" data-close-btn="none" id="fundingFinancialIns">
        <div data-role="header" data-position="" style="display: none;">
            <h1>Transfer from another finacial institution</h1>
        </div>
        <div data-role="content">
            <div class="row">
                <div class="col-xs-12 header_theme2" style="padding-right: 0px;">
                    <a href="#" onclick="goBackFFinacialIns(this)" class="pull-right svg-btn"><%=HeaderUtils.IconClose %><span style="display: none;">close</span></a>
                </div>
            </div>
            <div data-placeholder="content"></div>
            <div class ="div-continue" style="text-align: center;">
                <a href="#" data-transition="slide" onclick="validateFFinacialIns(this, true);" type="button" data-role="button" class="div-continue-button">Done</a> 
                <a href="#divErrorDialog" style="display: none;">no text</a>
                <%--Or <a href="#" onclick="goBackFFinacialIns(this)" class ="div-goback" data-corners="false" data-shadow="false" data-theme="reset"><span class="hover-goback"> Go Back</span></a>   --%>
            </div>
        </div>
        <div id="popUpAccountGuide" data-role="popup" data-position-to="window">
            <div data-role="content">
				<div class="row">
					<div class="col-xs-12 header_theme2">
                        <a data-rel="back" href="#" class="pull-right svg-btn"><%=HeaderUtils.IconClose %><span style="display: none;">close</span></a>
                    </div>
				</div>
                <div class="row">
                    <div class="col-sm-12">
                        <div style="text-align: center; margin: 10px 0;">
                            <img src="/images/SampleCC.jpg" alt="" style="width: 100%;"/>
                        </div>     
                    </div>    
                </div>
            </div>
        </div>
    </div>
    <%End If%>
		<uc:DocUploadSrcSelector ID="ucDocUploadSrcSelector" IDPrefix="" runat="server" />
		<uc:DocUploadSrcSelector ID="ucCoDocUploadSrcSelector" IDPrefix="co_" runat="server" />
     <!-- About Your CoApp(pl6): co_applicant info +  CoApp current address -->
     <div data-role="page" id="pl6">
        <div data-role="header" class="sr-only">
            <h1>Co-Applicant Information</h1>
        </div>
        <div data-role="content">
            <%=HeaderUtils.RenderLogoAndSteps(_LogoUrl, 1, "APPLY_CC")%>
            <%=HeaderUtils.RenderPageTitle(21, "About Your Co-applicant", false)%>
            <%=HeaderUtils.RenderPageTitle(18, "Personal Information", true)%>            
            <uc:applicantInfo ID="ucCoApplicantInfo" IDPrefix="co_" runat="server" LoanType="CC"/>
          
              <%-- Reference info--%>
			<%If CheckShowField("divReferenceInformation", "co_", True) Or (IsInMode("777") AndAlso IsInFeature("visibility")) Then%>
            <uc:ReferenceInfo id="ucCoReferenceInfo" IDPrefix="co_" runat="server" />
			<%End If %>
             <%=HeaderUtils.RenderPageTitle(22, "Contact Information", true)%>
            <uc:ccContactInfo ID="ucCoContactInfo" IDPrefix="co_" runat="server" />
            <%=HeaderUtils.RenderPageTitle(14, "Current Physical Address", true)%>
            <uc:applicantAddress ID="ucCoApplicantAddress" LoanType="CC" IDPrefix="co_" runat="server" />
            <%If EnableIDSection("co_") Then%>
            <uc:ucApplicantID ID="ucCoApplicantID" IDPrefix="co_" runat="server" />
            <%End If%>
            <%=HeaderUtils.RenderPageTitle(23, "Financial Information", true)%>
            <uc:financialInfo ID="ucCoFinancialInfo" IDPrefix="co_" runat="server" />
            <uc:DocUpload ID="ucCoDocUpload" IDPrefix="co_" runat="server" />
            <uc:Asset id="ucCoAssetInfo" IDPrefix="co_" runat="server"/>
			<%If _declarationList IsNot Nothing AndAlso _declarationList.Count > 0 Then%>
                <div>
					<%=HeaderUtils.RenderPageTitle(15, "Co/Joint Declarations", true)%>
                    <uc:declaration ID="ucCoDeclaration" LoanType="CC" IDPrefix="co_" runat="server" />
                </div>
				<%End If %>
			<% If _isCQNewAPI Then%>
                <%If IsComboMode Then%>
                    <uc:xaApplicantQuestion ID="coXaComboApplicantQuestion" LoanType="CC" isComboMode ="true" IDPrefix="co_" CQLocation="ApplicantPage" runat="server" />
			    <%Else%>
                    <uc:xaApplicantQuestion ID="coApplicantQuestion" LoanType="CC" isComboMode ="false" IDPrefix="co_" CQLocation="ApplicantPage" runat="server" />
                <%End If%>
            <%Else%>
               <%If IsComboMode Then%>
                   <uc:xaApplicantQuestion ID="coXaApplicantQuestion" LoanType="CC" IDPrefix="co_" CQLocation="ApplicantPage" runat="server" />
               <%End If%>
            <%End If %>
        </div>
        <div class ="div-continue" data-role="footer">
            <a href="#"  data-transition="slide" onclick="validateScreen6(this);" type="button" class="div-continue-button">Continue</a>
            <a href="#divErrorDialog" style="display: none;">no text</a>
            Or <a href="#pl2" class ="div-goback" data-corners="false" data-shadow="false" data-theme="reset"><span class="hover-goback"> Go Back</span></a> 
			<%--<%If IsFinishLaterEnabled Then%>
			 <a style="display: block;" class="div-finish-later" onclick="saveAndFinishLater()">Save & Finish Later</a>
			 <%End If%>--%>
        </div>
    </div>
    <a id="href_show_dialog_1" href="#divErrorDialog" data-rel="dialog" style="display: none;">no text</a>
	<%If LegacyDLScanEnabled OrElse LaserDLScanEnabled Then%>
    <div data-role="dialog" id="co_scandocs">
        <div data-role="header"  style="display:none" >
                <h1>Co-Driver's License Scan</h1>
        </div>
        <div data-role="content">
	        <%If LaserDLScanEnabled Then%>
			<div class="js-laser-scan-container">
			<a data-rel="back" href="#" class="svg-btn btn-close-dialog"><%=HeaderUtils.IconClose %><span style="display: none;">close</span></a> 
			<uc:LaserDriverLicenseScan ID="coAppLaserDriverlicenseScan" IDPrefix="co_" runat="server" />
			</div>
			<%End If %>
			<div class="js-legacy-scan-container <%=IIf(LaserDLScanEnabled, "hidden", "") %>" >  <a data-rel="back" href="#" style="margin-bottom: 5px;" class="pull-right svg-btn"><%=HeaderUtils.IconClose %><span style="display: none;">close</span></a>
				<uc:driverLicenseScan ID="coAppDriverlicenseScan" IDPrefix="co_" runat="server" />
				<div class="div-continue" style="text-align: center;">
					<a href="#" onclick="ScanAccept('co_')" data-transition="slide" type="button" data-role="button" class="div-continue-button">Done</a> 
					<a href="#divErrorDialog" style="display: none;">no text</a>
					<a href='#pl6' style="display: none;">no text</a>           
				</div>
			</div>
        </div>
        
    </div>    
    <%End If %>
    
     <div data-role="page" id="walletQuestions" >
		<div data-role="header" style="display: none">
            <h1>Authentication Questions</h1>
        </div>
		<div data-role="content">
		    <%=HeaderUtils.RenderLogoAndSteps(_LogoUrl, 3, "APPLY_CC")%>
            <%=HeaderUtils.RenderPageTitle(18, "Authentication Questions", false)%>
            <div id="walletQuestionsDiv"></div>                   
			<div class="div-continue" style="margin-top: 10px; text-align: center;" data-role="footer">
				<a href="#" data-transition="slide" onclick="return validateWalletQuestions(this, false);" type="button" class="btnSubmitAnswer div-continue-button">Submit Answers</a> 
				<%--<input type="button" name="btnSubmitAnswer" value="Submit Answers" onclick="validateWalletQuestions(this, false);" data-theme="<%=_FooterDataTheme%>"  class ="btnSubmitAnswer"  autocomplete = "off"/>	--%>
			</div>
		</div>
	</div>
     <%-- jquery mobile need to transition to another page inorder to format the html correctly--%>
     <div data-role="page" id="co_walletQuestions">
		<div data-role="header" style="display: none">
            <h1>Co-Authentication Questions</h1>
        </div>
		<div class="paddingDiv">
		<div data-role="content">
		    <%=HeaderUtils.RenderLogoAndSteps(_LogoUrl, 3, "APPLY_CC")%>
            <%=HeaderUtils.RenderPageTitle(18, "Authentication Questions", false)%>
            <div id="co_walletQuestionsDiv">
				</div>
				<div class="div-continue" style="margin-top: 10px; text-align: center;" data-role="footer">
					<a href="#" data-transition="slide" onclick="return validateWalletQuestions(this, true);" type="button" class="co_btnSubmitAnswer div-continue-button">Submit Answers</a> 
					<%--<input type="button" name="co_btnSubmitAnswer" value="Submit Answers"  data-theme="<%=_FooterDataTheme%>" onclick="return validateWalletQuestions(this, true);" class ="co_btnSubmitAnswer"  autocomplete = "off"/>	--%>
				</div>
			</div>
		</div>
	</div>
     <div data-role="page" id="cc_last">
        <div data-role="header" class="sr-only">
			<h1>Application Completed</h1>
        </div>
        <div data-role="content">
            <%=HeaderUtils.RenderLogoAndSteps(_LogoUrl, 3, "APPLY_CC")%>
            <div id="div_system_message" >
            </div>
            <%  If Not String.IsNullOrEmpty(_RedirectURL) Then%>
            <div class="div-continue" style="margin-top: 10px; text-align: center;" data-role="footer">
	            <a id="btnRedirect" href="#" onclick="gotoToUrl(this,'<%=_RedirectURL%>')" type="button" class="div-continue-button">Return to our website</a> 
                <%--<input type="button" data-theme="<%=_FooterDataTheme%>" id="btnRedirect" class="btn-redirect  div-continue-button" data-theme="<%=_FooterDataTheme%>" url='<%=_RedirectURL%>' value="Return to our website" />--%>
            </div>
            <%End If%>
        </div>
    </div>
     <div id="divErrorDialog" data-role="dialog" style="min-height: 600px;">
        <div role="dialog">
             <div data-role="header" style="display:none" >
					  <h1  >Alert Popup</h1>
			 </div>
            <div data-role="content">
                  <div class="DialogMessageStyle">There was a problem with your request</div>
                <div style="margin: 5px 0 15px 0;">
                    <span id="txtErrorMessage"></span>
                </div>
                <a href="#" data-role="button" data-rel="back" type="button">Ok</a>
            </div>
        </div>
    </div>
	<div id="divErrorPopup" data-role="popup" data-dismissible="true" data-history="false" style="max-width: 500px;padding: 20px;">
		<div data-role="header" style="display:none" >
				<h1  >Alert Popup</h1>
		</div>
        <div data-role="content">
            
            <div class="row">
                <div class="col-sm-12">
	                <div class="DialogMessageStyle">There was a problem with your request</div>
                    <div style="margin: 5px 0 15px 0;">
						<span id="txtErrorPopupMessage"></span>
					</div>       
                </div>    
            </div>
			<div class="row text-center">
				<div class="col-xs-12 text-center">
					<a href="#" data-role="button" data-rel="back" type="button">Ok</a>
				</div>
			</div>
        </div>
    </div>
	<div id="popNotification" data-role="popup" data-dismissible="true" data-history="false" style="max-width: 500px;padding: 20px;">
		<div data-role="header" style="display:none" >
				<h1  >Notification Popup</h1>
		</div>
        <div data-role="content">
            
            <div class="row">
                <div class="col-sm-12">
	                <div class="DialogMessageStyle header_theme">Notification</div>
                    <div style="margin: 5px 0 15px 0;" placeholder="content"></div>       
                </div>    
            </div>
			<div class="row text-center">
				<div class="col-xs-12 text-center">
					<a href="#" data-role="button" onclick="closePopup('#popNotification')" type="button">Ok</a>
				</div>
			</div>
        </div>
    </div>
     <div id="divMessage" data-role="dialog" style="min-height: 444px;">
        <div role="dialog">
			<div data-role="header" style="display:none">
                <h1 class="ui-title" role="heading" aria-level="1">
					Information Popup    
                 </h1>
            </div>
            <div data-role="content" role="main"><br />
                <div style="margin: 5px 0 15px 0;">
                    <span id="txtMessage" style="color: Blue;"></span>
                </div>
                <br />
				<a href='#' data-role='button' type="button" id='OKButton'>Ok</a>
               <%--<button id ="OKButton" type ="button" data-theme="<%=_FooterDataTheme%>"><b>Ok</b> </button><br />--%>
            </div>
        </div>
    </div>
    <div data-role="page" id="verifyIdentityPage" class="hidden" >
       <a href="#" id="submit_verify_identity" data-transition="slide" onclick="return submitVerifyIdentity();" type="button" class="hidden">Verify Identity</a>
	</div>
     <div data-role="page" id="decisionPage">
        <%--<div data-role="header" data-theme="<%=_HeaderDataTheme%>" data-position="">
          <h1>User Declined</h1>
            </div> --%>
        <div data-role="content">
          <%=HeaderUtils.RenderLogoAndSteps(_LogoUrl, 3, "APPLY_CC")%>
          <%=HeaderUtils.RenderPageTitle(19, "Loan Submission Cancelled", False)%>
         <div style="padding:10px 10px 10px 10px;background-color:yellow;margin-bottom:15px;margin-top :10px;" class="rename-able"><%If Not String.IsNullOrEmpty(_SubmitCancelMessage) Then%><%=_SubmitCancelMessage%><%Else%>We're sorry you have declined to use our automated prequalification system. If you prefer, please come to our offices to apply or call our Consumer Loan Department and loan representative will be happily ready to assist you.<%End If%>
         </div>
         <div id="divRedirectURL">
        <%  If Not String.IsNullOrEmpty(_RedirectURL) Then%>
            <div  style="margin:10px 0px 10px 0px" class="RedirectUrlPage" >
             <a href ="<%=_RedirectURL%>" class="header_theme2 shadow-btn chevron-circle-right-before" style="font-size :18px" onclick="onClickReturnToMainPage(event);">Return to Main Page</a></div>  
         <div class="rename-able">If you decide you would like to go through our instant decision process, please click on the following link to return to your application summary page and submit the application for decisioning.</div>
         <div style="margin-top:5px" class="RedirectUrlPage" ><a class="header_theme2 shadow-btn chevron-circle-right-before" style="font-size :18px" id="btnReturnToApp" href="#pagesubmit">Return to Application</a></div>
         <%End If%>
     </div>
     </div>
     </div>
     <div data-role="dialog" id="divConfirmDialog">
        <div data-role="header" style="display:none" ><h1>Restart Popup</h1></div>
        <div data-role="content">                 
			<div style='margin: 5px 0 15px 0;'><span class="require-span">Restart will clear all data and return to the main page. Are you sure?</span></div>
			<div class='row' style="text-align: center;margin-top: 40px; margin-bottom: 10px;">
				<a href='#' type="button" style="width: 100px; display: inline;padding-left: 20px; padding-right: 20px;" data-role='button' id='btnOk'>Yes</a>
				<a href='#' type="button" style="width: 100px;display: inline;padding-left: 20px; padding-right: 20px;" data-role='button' data-rel='back' id='btnCancel'>No</a>
			</div>
        </div>
    </div> 
       
	<div data-role="page" id="pageMaxAppExceeded">
			<div data-role="header" style="display: none">
				<h1>Access Denied</h1>
			</div>
		  <div data-role="content">
				<%=HeaderUtils.RenderLogo(_LogoUrl)%>
			  <br/>
			  <br/>
			  <div>You have exceeded the maximum number of applications per hour. Please try again later.</div>
			  <br/>
				<div class="div-continue" style="margin-top: 10px; text-align: center;" data-role="footer">
					<a href="#" onclick="gotoToUrl(this,'<%=_RedirectURL%>')" type="button" class="div-continue-button">Return to our website</a>
				</div>
		   </div>
	 </div>
  <!--ui new footer content --> 
       <div class="divfooter">
           <%=_strRenderFooterContact%> 
            <div style="text-align :center"> 
                 <!-- lender name -->
                <div class="divLenderName"><%=_strLenderName%></div> 
                <div class="divFooterLogo">            
                    <% If String.IsNullOrEmpty(_hasFooterRight) Then%>
                       <%-- 
                       <div><a href="<%=_strNCUAUrl%>"  class="ui-link" ><%=_strFooterRight%></a><a href="<%=_strHUDUrl%>" class="ui-link"> <img src="<%=_strLogoUrl%>" alt ="Housing Lender" class="footerimage-style img-responsive" /></a></div>                 
                        --%> 
                       <div class="divfooterlabel"><a href="<%=_strNCUAUrl%>"  class="ui-link" ><%=_strFooterRight%></a> <a href="<%=_strHUDUrl%>" class="ui-link"><%=_equalHousingText%></a></div>                                      
                    <% Else%>
                        <%=_strFooterRight%>
                    <%End If%>
                </div>  
                <div><%=_strFooterLeft  %></div>   
           </div> 
          
       </div> 
         <!--end new ui footer content -->

</div>    
	
	<uc:pageFooter ID="ucPageFooter" runat="server" />
	<%If IsComboMode Then%>
		<%:Scripts.Render("~/js/thirdparty/funding")%>
		<%If _showProductSelection Then%>
		<%:Scripts.Render("~/js/thirdparty/product")%>
		<%End If%>
	<%End If%>
	<%If LoanSavingItem IsNot Nothing andalso LoanSavingItem.Status = "INCOMPLETE" AndAlso not String.IsNullOrEmpty(LoanSavingItem.RequestParamJson) Then%>
	<script type="text/javascript">
		$(function() {
			var savedData = <%=LoanSavingItem.RequestParamJson%>;
			bindSavedData(savedData);
		});
	</script>
	<%End If %>
</body>
</html>
