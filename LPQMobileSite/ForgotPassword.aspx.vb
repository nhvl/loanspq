﻿
Imports LPQMobile.Utils

Partial Class Sm_ForgotPassword
	Inherits System.Web.UI.Page

	Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
		If Request.IsAuthenticated Then
			Response.Redirect(FormsAuthentication.GetRedirectUrl(User.Identity.Name, True), True)
		End If
	End Sub
End Class
