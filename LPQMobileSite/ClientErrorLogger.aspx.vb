﻿
Imports LPQMobile.Utils

Partial Class ClientErrorLogger
    Inherits System.Web.UI.Page
    Private _log As log4net.ILog = log4net.LogManager.GetLogger(Me.GetType)
	Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
		Try
			Dim message As String = Common.SafeString(Request.Params("message"))
			Dim url As String = Common.SafeString(Request.Params("url"))
			Dim line As String = Common.SafeString(Request.Params("line"))
			Dim column As String = Common.SafeString(Request.Params("column"))
			Dim exception As String = Common.SafeString(Request.Params("exception"))
			Dim browser As String = Common.SafeString(Request.Params("browser"))

			url = url.Replace(",/ClientErrorLogger.aspx", "")
			_log.InfoFormat("Client-Side Error - Message: {0} - URL: {1} - Line: {2} - Column: {3} - Exception: {4} - Browser: {5}", message, url, line, column, exception, browser)

		Catch ex As Exception
			_log.Error("ClientErrorLogger err: " & ex.Message, ex)
		End Try
	End Sub

End Class
