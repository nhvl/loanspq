﻿var FS1 = window.FS1 || {};
FS1.Data = {};
FS1.buildHtmlForType = function (type) {
    if (type == 'fundingCreditCard') {
        return FS1.buildCCHtml();
    }
    if (type == 'fundingFinancialIns') {
        return FS1.buildBankHtml();
    }
    if (type == 'fundingInternalTransfer') {
        return FS1.buildTransferHtml();
    }
    return '';
}
FS1.buildHtmlForSelectAccountDialog = function(accountList) {
	var html = '<div class="sub_section_header header_theme"><div class="section-title rename-able RequiredIcon">Select an account</div></div>';
	if (accountList && accountList.length > 0) {
		$.each(accountList, function (index, acc) {
			if (acc.accountNumber && acc.accountNumber != "" && acc.accountName && acc.accountName != "") {
				var label = acc.accountNumber + " " + acc.accountName;
				if (acc.accountSuffix && acc.accountSuffix != "") {
					label += "(" + acc.accountSuffix + ")";
				}
				if (acc.accountBalance && acc.accountBalance != "") {
					label += ": $" + acc.accountBalance;
				}

				html += '<div><a href="#" data-mode="self-handle-event" data-role="button" data-acc-name="' + acc.accountName + '" data-value="' + acc.accountNumber + '" class="btn-header-theme rename-able">' + label + '</a></div>';
			}
		});
	}
	html += '<div><a href="#" data-role="button" data-mode="self-handle-event" data-value="" data-acc-name="" class="btn-header-theme rename-able">Enter another account number</a></div>';
	return html;
}
function InitSelectAccountDialog() {
	var $container = $("#fundingSelectAccount");
	$.each($container.find("a.btn-header-theme"), function(idx, ele) {
		var $ele = $(ele);
		if ($ele.data("value") == "") {
			$ele.on("click", function () {
				$container.find("a.btn-header-theme.active").removeClass("active btnActive_on");
				$ele.addClass("active");
				$.mobile.changePage("#fundingInternalTransfer", {
					transition: "pop",
					reverse: false,
					changeHash: true
				});		
			});
		} else {
			$ele.on("click", function() {
				$container.find("a.btn-header-theme.active").removeClass("active btnActive_on");
				$ele.addClass("active");
			});
		}
	});
}
FS1.buildTransferHtml = function () {
	var freeformAccNum = $("#hdFundingAccountNumberFreeForm").val();
    var html = '';
    html += "<div data-role='fieldcontain'><label for='tAccountNumber' class='RequiredIcon'>Account Number</label>";
    html += "<input type='tel' id='tAccountNumber' class='required " + (freeformAccNum == "Y" ? "" : " numeric") + "' maxlength ='20'/>";
    html += "</div>";
    html += "<div data-role='fieldcontain'><label for='tAccountType' class='RequiredIcon'>Account Type</label>";
    html += "<select id='tAccountType'>";
    html += "<option value=''>--Please select--</option>";
    html += "<option value='CHECKING'>Checking</option>";
    html += "<option value='MONEY_MARKET'>Money Market</option>";
    html += "<option value='SAVINGS'>Savings</option>";
    html += "</select>";
    html += "</div>";
    return html;
}

FS1.buildCCHtml = function () {
    var html = '';
    
    //OS(>8.1) built-in optical requirement
    //post over https
    //fields need to be inside form (including name, and cvs)
    //name attribute cc number has value = cardNumber
    //type attribute for cc number has value = tel
    //limitation: if user uses the camera from the 2nd text field then the number will be wrongly parsed

    html += "<form action='#'>";

    html += "<div data-role='fieldcontain'><label for='cCreditCardNumber1' class='RequiredIcon'>Credit Card Number</label>";
    html += "<div class='ui-input-cc row'>";
    html += "<div class='col-xs-3'>";
    html += "<input type='" + (isMobile.Android() ? "tel" : "text") + "' pattern='[0-9]*' placeholder='XXXX' id='cCreditCardNumber1' autocomplete='new-password' name='cardNumber' class='masked-field' group='cardNumber' next-input='#cCreditCardNumber2' onkeydown='limitToNumeric(event);' onkeyup='onKeyUp(event,\"#cCreditCardNumber1\",\"#cCreditCardNumber2\", \"4\");'/>";
    html += "</div>";
    html += "<div class='col-xs-3'>";
    html += "<input type='" + (isMobile.Android() ? "tel" : "text") + "' pattern='[0-9]*' placeholder='XXXX' id='cCreditCardNumber2' autocomplete='new-password' group='cardNumber' class='masked-field' next-input='#cCreditCardNumber3' onkeydown='limitToNumeric(event);' onkeyup='onKeyUp(event,\"#cCreditCardNumber2\",\"#cCreditCardNumber3\", \"4\");' />";
    html += "</div>";
    html += "<div class='col-xs-3'>";
    html += "<input type='" + (isMobile.Android() ? "tel" : "text") + "' pattern='[0-9]*' placeholder='XXXX' id='cCreditCardNumber3' autocomplete='new-password' group='cardNumber' class='masked-field' next-input='#cCreditCardNumber3' onkeydown='limitToNumeric(event);' onkeyup='onKeyUp(event,\"#cCreditCardNumber3\",\"#cCreditCardNumber4\", \"4\");' />";
    html += "</div>";
    html += "<div class='col-xs-3' style='padding-right:0px;'>";
    html += "<input type='" + (isMobile.Android() ? "tel" : "text") + "' pattern='[0-9]*' placeholder='XXXX' id='cCreditCardNumber4' group='cardNumber' onkeydown='limitToNumeric(event);' onkeyup='onKeyUp(event,\"#cCreditCardNumber4\",\"#cCreditCardNumber4\", \"4\");' />";
    html += "</div>";
	html += "</div>";

	html += "</div>";
	html += "<br/><div id='ccImage'>";
	html += "</div><br/>";
    html += "<div data-role='fieldcontain'><label for='cExpirationDate1'>Expiration Date<span style='color:red'>*</span></label>";
    html += "<div class='ui-input-ccdate row'>";
    html += "<div class='col-xs-6'>";
	//html += "<input type='" + (isMobile.Android() ? "tel" : "text") + "' pattern='[0-9]*' placeholder='mm' id='cExpirationDate1' next-input='#cExpirationDate2' onkeydown='limitToNumeric(event);' onkeyup='onKeyUp(event,\"#cExpirationDate1\",\"#cExpirationDate2\", \"2\");' maxlength='2' />";
    html += "<select id='cExpirationDate1'>";
    html += "<option value=''>MM</option>";
    html += "<option value='01'>01</option>";
    html += "<option value='02'>02</option>";
    html += "<option value='03'>03</option>";
    html += "<option value='04'>04</option>";
    html += "<option value='05'>05</option>";
    html += "<option value='06'>06</option>";
    html += "<option value='07'>07</option>";
    html += "<option value='08'>08</option>";
    html += "<option value='09'>09</option>";
    html += "<option value='10'>10</option>";
    html += "<option value='11'>11</option>";
    html += "<option value='12'>12</option>";
    html += "</select>";
    html += "</div>";
    html += "<div class='col-xs-6' style='padding-right:0px;'>";
	//html += "<input type='" + (isMobile.Android() ? "tel" : "text") + "' pattern='[0-9]*' placeholder='yyyy' id='cExpirationDate2' onkeydown='limitToNumeric(event);' maxlength='4' />";
    html += "<select id='cExpirationDate2'>";
    html += "<option value=''>YYYY</option>";
	var curYear = (new Date()).getFullYear();
	for (var i = 0; i <= 10; i++) {
		var year = curYear + i;
		html += "<option value='" + year + "'>" + year + "</option>";
	}
    html += "</select>";
    html += "</div>";
    html += "</div>";
    
    html += "</div>";

    //take out "show cvn" because the title is too long and cuase wrap around
    //html += "<div data-role='fieldcontain'><label>CVN Number (xxx) <span style='color:red'>*</span><a class='pull-right header_theme2' style='margin-right: 10px; cursor: pointer;' onclick='toggleCVNText(this);'>Show CVN</a></label>";
    html += "<div data-role='fieldcontain'><label for='cCVNNumber'>CVN Number (xxx)<span style='color:red'>*</span></label>";
    html += "<input type='password'" + " pattern='[0-9]*'" + " id='cCVNNumber' onkeydown='limitToNumeric(event);' class='required numeric masked-field' maxlength='3'/>";
    html += "</div>";

    //html += "<div data-role='fieldcontain'><label for='cCardType'>Card Type<span style='color:red'>*</span></label>";
    //html += "<select id='cCardType' >";
    //html += "<option value=''>--Please Select--</option>";
    //html += "<option value='VISA'>Visa</option>";
    //html += "<option value='MASTERCARD'>MasterCard</option>";
    //html += "</select>";
    //html += "</div>";

    
    //add name on card over here
    html += "<div data-role='fieldcontain'><label for='cNameOnCard' class='RequiredIcon'>Name On Card</label>";
    html += "<select id='cNameOnCard' class='required'>";
    $.each(getApplicantNames(), function (idx, item) {
    	html += "<option " + (idx == 0 ? "selected='selected'" : "") + " data-name-prefix='" + item.prefix + "' value='" + item.name + "'>" + item.name + "</option>";
    });
    html += "</select></div>";

    html += "</form>";

	//add a button to copy address
    html += "<div class ='divCopyAddress' id='copyAddress' data-is-the-same='Y' style='width:100%; text-align:left'>";
	//html += "<input type='button' id='cBtnCopyAddress' data-theme='" + _FooterThemeData + "' value ='Same Name and Address' />"
    html += "<a href='#' data-is-the-same='Y' style='cursor: pointer;font-weight :bold; display:inline-block;margin-top:6px;' class='header_theme2 shadow-btn chevron-circle-right-before' data-mode='self-handle-event' name='cBtnCopyAddress' id='cBtnCopyAddress'>Use different address for billing</a>";
    html += "</div>";

    html += "<div id='cBillingAddressPanel' class='hidden'>";
    html += "<div data-role='fieldcontain'><label for='cBillingAddress' class='RequiredIcon'>Billing Address</label>";
    html += "<input type='text' id='cBillingAddress' class='required' maxlength='100'/>";
    html += "</div>";

    html += "<div data-role='fieldcontain'><label for='cBillingZip' class='RequiredIcon'>Billing Zip</label>";
    html += "<input type='tel' id='cBillingZip' class='numeric required' maxlength ='5'/>";
    html += "</div>";

    html += "<div data-role='fieldcontain'><label for='cBillingCity' class='RequiredIcon'>Billing City</label>";
    html += "<input type='text' id='cBillingCity' class ='required' maxlength ='50'/>";
    html += "</div>";

    html += "<div data-role='fieldcontain'><labelfor='cBillingState' class='RequiredIcon'>Billing State</label>";
    html += "<select id='cBillingState'>";
    html += this.StateOptionHtml();
    html += '</select>';
    html += "</div>";
    html += "</div>";

    return html;
}

FS1.StateOptionHtml = function () {
    var html = '';
    html += '<option value="">--Please Select--</option>';

    html += '<option value="AL">Alabama</option>';
    html += '<option value="AK">Alaska</option>';
    html += '<option value="AS">American Samoa</option>';
    html += '<option value="AZ">Arizona</option>';
    html += '<option value="AR">Arkansas</option>';
    html += '<option value="AA">Armed Forces - Americas</option>';
    html += '<option value="AE">Armed Forces – Europe</option>';
    html += '<option value="AP">Armed Forces – Pacific</option>';
    html += '<option value="CA">California</option>';
    html += '<option value="CO">Colorado</option>';
    html += '<option value="CT">Connecticut</option>';
    html += '<option value="DE">Delaware</option>';
    html += '<option value="DC">District Of Columbia</option>';
    html += '<option value="FL">Florida</option>';
    html += '<option value="FM">Federated States of Micronesia</option>';
    html += '<option value="GA">Georgia</option>';
    html += '<option value="GU">Guam</option>';
    html += '<option value="HI">Hawaii</option>';
    html += '<option value="ID">Idaho</option>';
    html += '<option value="IL">Illinois</option>';
    html += '<option value="IN">Indiana</option>';
    html += '<option value="IA">Iowa</option>';
    html += '<option value="KS">Kansas</option>';
    html += '<option value="KY">Kentucky</option>';
    html += '<option value="LA">Louisiana</option>';
    html += '<option value="ME">Maine</option>';
    html += '<option value="MD">Maryland</option>';
    html += '<option value="MA">Massachusetts</option>';
    html += '<option value="MH">Marshall Islands</option>';
    html += '<option value="MI">Michigan</option>';
    html += '<option value="MN">Minnesota</option>';
    html += '<option value="MS">Mississippi</option>';
    html += '<option value="MO">Missouri</option>';
    html += '<option value="MP">Northern Mariana Islands</option>';
    html += '<option value="MT">Montana</option>';
    html += '<option value="NE">Nebraska</option>';
    html += '<option value="NV">Nevada</option>';
    html += '<option value="NH">New Hampshire</option>';
    html += '<option value="NJ">New Jersey</option>';
    html += '<option value="NM">New Mexico</option>';
    html += '<option value="NY">New York</option>';
    html += '<option value="NC">North Carolina</option>';
    html += '<option value="ND">North Dakota</option>';
    html += '<option value="OH">Ohio</option>';
    html += '<option value="OK">Oklahoma</option>';
    html += '<option value="OR">Oregon</option>';
    html += '<option value="PA">Pennsylvania</option>';
    html += '<option value="PR">Puerto Rico</option>';
    html += '<option value="PW">Palau</option>';
    html += '<option value="RI">Rhode Island</option>';
    html += '<option value="SC">South Carolina</option>';
    html += '<option value="SD">South Dakota</option>';
    html += '<option value="TN">Tennessee</option>';
    html += '<option value="TX">Texas</option>';
    html += '<option value="UT">Utah</option>';
    html += '<option value="VT">Vermont</option>';
    html += '<option value="VI">Virgin Islands</option>';
    html += '<option value="VA">Virginia</option>';
    html += '<option value="WA">Washington</option>';
    html += '<option value="WV">West Virginia</option>';
    html += '<option value="WI">Wisconsin</option>';
    html += '<option value="WY">Wyoming</option>';
    return html;
}

function getApplicantNames() {
	var result = [];
	//first item is primary applicant
	var prefixes;
	if (BAPREFIXLIST != null && BAPREFIXLIST.length > 0) {
		prefixes = BAPREFIXLIST;
	} else {
		prefixes = SAPREFIXLIST;
	}
	if (prefixes != null && prefixes.length > 0) {
		$.each(prefixes, function (idx, pfx) {
			var fullName = $.trim($("#" + pfx + "txtFName").val());
			fullName += " " + $.trim($("#" + pfx + "txtMName").val());
			fullName += " " + $.trim($("#" + pfx + "txtLName").val());
			result.push({
				name: $('<div/>').text(fullName).html().replace(/'/g, "&apos;"),
				prefix: pfx
			});
		});
	} else {
		// prefill Name On Account when BankHtml is rendered
		var fullName = $.trim($('#txtFName').val());
		fullName += " " + $.trim($("#txtMName").val());
		fullName += " " + $.trim($("#txtLName").val());
		// encode fullName html using jquery
		result.push({
			name: $('<div/>').text(fullName).html().replace(/'/g, "&apos;"),
			prefix: ""
		});
		//secondary item is co-app
		if ($("#hdHasCoApplicant").val() === "Y") {
			var coFullName = $.trim($('#co_txtFName').val());
			coFullName += " " + $.trim($("#co_txtMName").val());
			coFullName += " " + $.trim($("#co_txtLName").val());
			// encode fullName html using jquery
			result.push({
				name: $('<div/>').text(coFullName).html().replace(/'/g, "&apos;"),
				prefix: "co_"
			});
		}
	}
	return result;
}

FS1.buildBankHtml = function () {
	var html = '';
	html += "<div data-role='fieldcontain'><label for='bAccountType' class='RequiredIcon'>Account Type</label>";
    html += "<select id='bAccountType'>";
    html += "<option value=''>--Please select--</option>";
    html += "<option value='CHECKING'>Checking</option>";
    html += "<option value='SAVINGS'>Savings</option>";
    html += "</select>";
    html += "</div>";

    html += "<div data-role='fieldcontain'><label for='bNameOnCard' class='RequiredIcon'>Name On Account</label>";
    html += "<select id='bNameOnCard' class='required'>";
	$.each(getApplicantNames(), function(idx, item) {
		html += "<option " + (idx == 0 ? "selected='selected'" : "") + " data-name-prefix='" + item.prefix + "' value='" + item.name + "'>" + item.name + "</option>";
	});
    
    html += "</select></div>";

    html += "<div data-role='fieldcontain'><div style='display: inline-block; white-space:nowrap;'><label for='bAccountNumber' class='RequiredIcon'>Account Number</label></div><div style='display: inline-block; white-space:nowrap;'>&nbsp;<a href='#popUpAccountGuide' data-position-to='window' data-rel='popup' data-transition='pop'><div class='questionMark'></div></a></div>";
    html += "<input type='tel' id='bAccountNumber' class='required numeric' maxlength ='20'/>";
    html += "</div>";

    html += "<div data-role='fieldcontain'><div style='display: inline-block; white-space:nowrap;'><label for='bRoutingNumber' class='RequiredIcon'>Routing Number</label></div><div style='display: inline-block; white-space:nowrap;'>&nbsp;<a href='#popUpAccountGuide' data-position-to='window' data-rel='popup' data-transition='pop'><div class='questionMark'></div></a></div>";
    html += "<input type='tel'  id='bRoutingNumber' class='required numeric' maxlength ='9'/>";
    html += "</div>";


    html += "<div data-role='fieldcontain'><label for='bBankName' class='RequiredIcon'>Financial Institution</label>";
    html += "<input type='text' id='bBankName' class='required' maxlength ='50'/>";
    html += "</div>";

    html += "<div data-role='fieldcontain'><label for='bBankState' class='RequiredIcon'>Financial Institution State</label>";
    html += "<select id='bBankState'>";
    //html += "<option value=''>--Please select--</option>";
    html += this.StateOptionHtml();
    html += '</select>';
    html += "</div>";
    return html;
}
FS1.SafeValue = function (id) {
    return $(id).length > 0 ? $(id).val() : '';
}

FS1.loadValueToData = function () {
    this.Data.fsFundingType = this.SafeValue('#fsSelectedFundingType');
    this.Data.cNameOnCard = "";
    //this.Data.cCardType = "";
    this.Data.cCreditCardNumber = "";
    this.Data.cExpirationDate = "";
    this.Data.cCVNNumber = "";
    this.Data.cBillingAddress = "";

    this.Data.cBillingZip = "";
    this.Data.cBillingCity = "";
    this.Data.cBillingState = "";

    this.Data.bAccountType = "";
    this.Data.bNameOnCard = "";
    this.Data.bAccountNumber = "";
    this.Data.bRoutingNumber = "";
    this.Data.bBankName = "";
    this.Data.bBankState = "";

    this.Data.tAccountNumber = "";
    this.Data.tAccountType = "";
    switch (this.Data.fsFundingType) {
        case "CREDIT CARD":
            this.Data.cNameOnCard = this.SafeValue('#cNameOnCard');
            //this.Data.cCardType = this.SafeValue('#cCardType');
            this.Data.cCreditCardNumber = this.SafeValue('#cCreditCardNumber1') + "-" + this.SafeValue('#cCreditCardNumber2') + "-" + this.SafeValue('#cCreditCardNumber3') + "-" + this.SafeValue('#cCreditCardNumber4');
            this.Data.cCardType = Common.GetCardTypeFromNumber(this.Data.cCreditCardNumber);

            this.Data.cExpirationDate = this.SafeValue('#cExpirationDate1') + "-" + this.SafeValue('#cExpirationDate2');
            this.Data.cCVNNumber = this.SafeValue('#cCVNNumber');
            this.Data.cBillingAddress = this.SafeValue('#cBillingAddress');

            this.Data.cBillingZip = this.SafeValue('#cBillingZip');
            this.Data.cBillingCity = this.SafeValue('#cBillingCity');
            this.Data.cBillingState = this.SafeValue('#cBillingState');
            break;
        case "TRANSFER FROM ANOTHER FINANCIAL INSTITUTION":
        	this.Data.bAccountType = this.SafeValue('#bAccountType');
        	this.Data.bNameOnCard = this.SafeValue('#bNameOnCard');
        	this.Data.bAccountNumber = this.SafeValue('#bAccountNumber');
            this.Data.bRoutingNumber = this.SafeValue('#bRoutingNumber');
            this.Data.bBankName = this.SafeValue('#bBankName');
            this.Data.bBankState = this.SafeValue('#bBankState');
            break;
    	case "INTERNAL TRANSFER":
    		if ($("#fundingSelectAccount").length > 0 && $("#fundingSelectAccount").attr("data-acc-number") && $("#fundingSelectAccount").attr("data-acc-number").length > 0) {
    			this.Data.tAccountNumber = $("#fundingSelectAccount").attr("data-acc-number");
    			this.Data.tAccountType = $("#fundingSelectAccount").attr("data-acc-name");
		    } else {
    			this.Data.tAccountNumber = this.SafeValue('#tAccountNumber');
    			this.Data.tAccountType = this.SafeValue('#tAccountType');
		    }
            break;
    }
}
function ViewFundingSource() {
    var fundingSource = "";
    //credit card
    var selectedFundingType = $('#fsSelectedFundingType').val();
	if ($.trim(selectedFundingType) === "") return "";
    var txtcCardOnName = htmlEncode($('#cNameOnCard').val());
	//var ddlcCardType = $('#cCardType option:selected').val();
    var txtcCreditCard = $('#cCreditCardNumber1').val() + "-" + $('#cCreditCardNumber2').val() + "-" + $('#cCreditCardNumber3').val() + "-" + $('#cCreditCardNumber4').val();
    var txtcExpiredDate = $('#cExpirationDate1').val() + "-" + $('#cExpirationDate2').val();
    //var txtcCVNNumber = $('#cCVNNumber').val();
    var txtcBillingAddress = htmlEncode($('#cBillingAddress').val());
    var txtcBillingZip = htmlEncode($('#cBillingZip').val());
    var txtcBillingCity = htmlEncode($('#cBillingCity').val());
    var ddlcBillingState = $('#cBillingState option:selected').val();
    //transfer ...
    var ddlbAccountType = $('#bAccountType option:selected').val();
    var txtbNameOnCard = htmlEncode($('#bNameOnCard').val());
    var txtbAccountNumber = htmlEncode($('#bAccountNumber').val());
    var txtbRoutingNumber = htmlEncode($('#bRoutingNumber').val());
    var txtbBankName = htmlEncode($('#bBankName').val());
    var ddlbBankState = $('#bBankState option:selected').val();
    //internal transfer
    var txttAccountNumber = htmlEncode($('#tAccountNumber').val());
    var ddltAccountType = $('#tAccountType option:selected').text();
    //credit card type
    if (selectedFundingType == "CREDIT CARD") {
   fundingSource += '<div class="col-sm-6 col-xs-12"><div class="row"><div class="col-xs-6 text-right row-title"><span class="bold">Funding Type</span></div><div class="col-xs-6 text-left row-data"><span>CREDIT CARD</span></div></div>';
        fundingSource += '<div class="row"><div class="col-xs-6 text-right row-title"><span class="bold">Name On Card</span></div><div class="col-xs-6 text-left row-data"><span>' + txtcCardOnName + '</span></div></div>';
        fundingSource += '<div class="row"><div class="col-xs-6 text-right row-title"><span class="bold">Card Type</span></div><div class="col-xs-6 text-left row-data"><span>' + Common.GetCardTypeFromNumber(txtcCreditCard) + '</span></div></div>';
        fundingSource += '<div class="row"><div class="col-xs-6 text-right row-title"><span class="bold">Credit Card Number</span></div><div class="col-xs-6 text-left row-data"><span>' + Common.maskCardNumber(txtcCreditCard) + '</span></div></div>';
        fundingSource += '<div class="row"><div class="col-xs-6 text-right row-title"><span class="bold">Expiration Date</span></div><div class="col-xs-6 text-left row-data"><span>' + txtcExpiredDate + '</span></div></div>';
        fundingSource += '<div class="row"><div class="col-xs-6 text-right row-title"><span class="bold">CVN Number</span></div><div class="col-xs-6 text-left row-data"><span>' + '***' + '</span></div></div>';
        fundingSource += '<div class="row"><div class="col-xs-6 text-right row-title"><span class="bold">Billing Address</span></div><div class="col-xs-6 text-left row-data"><span>' + txtcBillingAddress + ", " + txtcBillingCity + ", " + ddlcBillingState + " " + txtcBillingZip + '</span></div></div></div>';
    }
        //transfer from another ....
    else if (selectedFundingType == "TRANSFER FROM ANOTHER FINANCIAL INSTITUTION") {
        fundingSource += '<div class="col-sm-6 col-xs-12"><div class="row"><div class="col-xs-6 text-right row-title"><span class="bold">Funding Type</span></div><div class="col-xs-6 text-left row-data"><span>TRANSFER FROM ANOTHER FINANCIAL INSTITUTION</span></div></div>';
        fundingSource += '<div class="row"><div class="col-xs-6 text-right row-title"><span class="bold">Account Type</span></div><div class="col-xs-6 text-left row-data"><span>' + ddlbAccountType + '</span></div></div>';
        fundingSource += '<div class="row"><div class="col-xs-6 text-right row-title"><span class="bold">Name On Account</span></div><div class="col-xs-6 text-left row-data"><span>' + txtbNameOnCard + '</span></div></div>';

        fundingSource += '<div class="row"><div class="col-xs-6 text-right row-title"><span class="bold">Account Number</span></div><div class="col-xs-6 text-left row-data"><span>' + txtbAccountNumber + '</span></div></div>';
        fundingSource += '<div class="row"><div class="col-xs-6 text-right row-title"><span class="bold">Routing Number</span></div><div class="col-xs-6 text-left row-data"><span>' + txtbRoutingNumber + '</span></div></div>';
        fundingSource += '<div class="row"><div class="col-xs-6 text-right row-title"><span class="bold">Financial Institution</span></div><div class="col-xs-6 text-left row-data"><span>' + txtbBankName + '</span></div></div>';
        fundingSource += '<div class="row"><div class="col-xs-6 text-right row-title"><span class="bold">Financial Institution State</span></div><div class="col-xs-6 text-left row-data"><span>' + ddlbBankState + '</span></div></div></div>';
    }
        //internal transfer
    else if (selectedFundingType == "INTERNAL TRANSFER") {
    	if ($("#fundingSelectAccount").length > 0 && $("#fundingSelectAccount").attr("data-acc-number") && $("#fundingSelectAccount").attr("data-acc-number").length > 0) {
    		txttAccountNumber = $("#fundingSelectAccount").attr("data-acc-number");
    		ddltAccountType = $("#fundingSelectAccount").attr("data-acc-name");
	    }
        fundingSource += '<div class="col-sm-6 col-xs-12"><div class="row"><div class="col-xs-6 text-right row-title"><span class="bold">Funding Type</span></div><div class="col-xs-6 text-left row-data"><span>INTERNAL TRANSFER</span></div></div>';
        fundingSource += '<div class="row"><div class="col-xs-6 text-right row-title"><span class="bold">Account Number</span></div><div class="col-xs-6 text-left row-data"><span>' + txttAccountNumber + '</span></div></div>';
        fundingSource += '<div class="row"><div class="col-xs-6 text-right row-title"><span class="bold">Account Type</span></div><div class="col-xs-6 text-left row-data"><span>' + ddltAccountType + '</span></div></div></div>';
    } else {
        fundingSource += '<div class="col-sm-6 col-xs-12"><div class="row"><div class="col-xs-6 text-right row-title"><span class="bold">Funding Type</span></div><div class="col-xs-6 text-left row-data"><span>' + selectedFundingType + '</span></div></div></div>';
    }
    return fundingSource;
}

function CopyMailingAddress(prefix) {
	if ($("#" + prefix + "txtMailingAddress").length == 0) return; //could not find address section=> do nothing
	if ($("#" + prefix + "lnkHasMaillingAddress").attr('status') == 'Y') {
		$('#cBillingAddress').val($("#" + prefix + "txtMailingAddress").val());
		$('#cBillingZip').val($("#" + prefix + "txtMailingZip").val());
		$('#cBillingCity').val($("#" + prefix + "txtMailingCity").val());
		$('#cBillingState').val($("#" + prefix + "ddlMailingState").val());
		$('#cBillingState').selectmenu();
		$('#cBillingState').selectmenu('refresh');
	} else {
		$('#cBillingAddress').val($("#" + prefix + "txtAddress").val());
		$('#cBillingZip').val($("#" + prefix + "txtZip").val());
		$('#cBillingCity').val($("#" + prefix + "txtCity").val());
		$('#cBillingState').val($("#" + prefix + "ddlState").val());
		$('#cBillingState').selectmenu();
		$('#cBillingState').selectmenu('refresh');
	}
}

//function ClearCopyMailingAddress() {
//	$('#cBillingAddress').val("");
//	$('#cBillingZip').val("");
//	$('#cBillingCity').val("");
//	$('#cBillingState').val("");
//	$('#cBillingState').selectmenu('refresh');
//}
//not use
//function toggleCVNText(ele) {
//	var ctrl = $("input#cCVNNumber");
//	var curVal = ctrl.val();
//	if (ctrl.attr("type") === "password") {
//		ctrl.attr("type", "tel");
//		$(ele).text("Hide CVN");
//	} else {
//		ctrl.attr("type", "password");
//		$(ele).text("Show CVN");
//	}
//	ctrl.val(curVal);
//}
//calling this to do some initial stuff after page content injected
function InitStuffs(container) {
    /*if (!isMobile.Android()) {
        $('input.inCardExpiredDate').mask('99-9999');
        $('input.inCCardNumber').mask('9999-9999-9999-9999');
    }
    else {
        $('input.inCardExpiredDate').blur(function () {
            var element = $(this);
            element.attr('maxlength', '7'); // mm-yyyy ->maxlength =7
            element.val(Common.FormatExpDate(element.val()));
        });
        //expired date format 
        $('input.inCardExpiredDate').keyup(function () {
            var element = $(this);
            var date = element.val();
            if (!isNaN(date)) {
                element.attr('maxlength', '6');
            } else {
                element.attr('maxlength', '7');
            }
        });
        $('input.inCardExpiredDate').keydown(function (event) {
            var key = event.keyCode;
            if (key == 32) {//disable spacing bar for expired date
                event.preventDefault();
            }
        });
        creditCardFormat();
    }*/
    $('input#bAccountNumber').blur(function () {
        $(this).val(Common.GetRtNumber($(this).val()));
    });
    $('input#tAccountNumber').blur(function () {
		if($("#hdFundingAccountNumberFreeForm").val() !== "Y") $(this).val(Common.GetRtNumber($(this).val()));
    });
    //if (!isMobile.Android()) {
    $('input#bRoutingNumber').mask("999999999", {placeholder: ""});
    //}
    $('input#bRoutingNumber').blur(function () {
        $(this).val(Common.GetRtNumber($(this).val()));
        if (/^[0-9]{9}$/.test(($(this).val()))) {
            //call service to validate routing number
            LookupRoutingNumber($(this).val(), function (bankName, state) {
                $('#bBankName').val(bankName);
                $('#bBankState').val(state).selectmenu('refresh');
            }, function () {
                //$('input#bRoutingNumber').val("");
            });
        }
        
    });
    
    $('input#cCVNNumber').blur(function () {
        $(this).val(Common.GetPosNumber($(this).val(), 3));
    });
    $('input#cBillingZip').blur(function () {
        $(this).val(Common.GetPosNumber($(this).val(), 5));
    });

    ////clear or copy address to billing address
    //var btnCopyAddressClick = 0;
    //$('.divCopyAddress').click(function () {
    //    if (btnCopyAddressClick % 2 == 0) {
    //        CopyMailingAddress();
    //        $('#cNameOnCard').val(getUserName());
    //    } else {
    //        ClearCopyMailingAddress();
    //        $('#cNameOnCard').val("");
    //    }
    //    btnCopyAddressClick++;
    //});
    //$("input#cCreditCardNumber1").on("change", function () {
    //	var val = $.trim($(this).val());
    //	if (val === "") {
    //		$("select#cCardType").val("");
    //	}
    //	else if (val.charAt(0) === "4") {
    //		$("select#cCardType").val("VISA");
    //	} else {
    //		$("select#cCardType").val("MASTERCARD");
    //	}
    //	$("select#cCardType").selectmenu('refresh');
    //});

    //do replace label text
    if (typeof BUTTONLABELLIST != 'undefined' && BUTTONLABELLIST !== null) {
    	$("label[for]:not(.abort-renameable):not(.sr-only)", container).each(function (idx, ele) {
            var html = $.trim($(ele).html());
            var key = EXTRACT_BUTTONLABEL_REGEX.exec(html);
            if (key != null && key.length > 0) {
                var value = BUTTONLABELLIST[$.trim(key[0]).toLowerCase()];
                if (typeof value == "string" && $.trim(value) !== "") {
                    $(ele).html(html.replace(EXTRACT_BUTTONLABEL_REGEX, value));
                }
            }
        });
    }
    if (isMobile.any() && !isMobile.Windows()) {//moz-text-security does not works on window devices
    	$(".masked-field").attr("type", "tel").addClass("mask-password");
    } else {
    	$(".masked-field").attr("type", "password");
    }

    //parse value from autofill or optical scan, only parse if there is matched 16 digits
    $("#cCreditCardNumber1").on("change", function () {
	    var value = $("#cCreditCardNumber1").val();
        var m = value.match(/^([0-9]{4})-?([0-9]{4})-?([0-9]{4})-?([0-9]{4})$/);
        if (m) {
            $("#cCreditCardNumber1").val(m[1]);
            $("#cCreditCardNumber2").val(m[2]);
            $("#cCreditCardNumber3").val(m[3]);
            $("#cCreditCardNumber4").val(m[4]);
        }
    });
    $("#cCreditCardNumber2").on("change", function () {
	    var value = $("#cCreditCardNumber2").val();
        var m = value.match(/^([0-9]{4})-?([0-9]{4})-?([0-9]{4})-?([0-9]{4})$/);
        if (m) {
            $("#cCreditCardNumber1").val(m[1]);
            $("#cCreditCardNumber2").val(m[2]);
            $("#cCreditCardNumber3").val(m[3]);
            $("#cCreditCardNumber4").val(m[4]);
        }
     });
     $("#cCreditCardNumber3").on("change", function () {
	     var value = $("#cCreditCardNumber3").val();
        var m = value.match(/^([0-9]{4})-?([0-9]{4})-?([0-9]{4})-?([0-9]{4})$/);
        if (m) {
            $("#cCreditCardNumber1").val(m[1]);
            $("#cCreditCardNumber2").val(m[2]);
            $("#cCreditCardNumber3").val(m[3]);
            $("#cCreditCardNumber4").val(m[4]);
        } 
     });

     $("#cCreditCardNumber4").on("change", function () {
	     var value = $("#cCreditCardNumber4").val();
         var m = value.match(/^([0-9]{4})-?([0-9]{4})-?([0-9]{4})-?([0-9]{4})$/);
         if (m) {
             $("#cCreditCardNumber1").val(m[1]);
             $("#cCreditCardNumber2").val(m[2]);
             $("#cCreditCardNumber3").val(m[3]);
             $("#cCreditCardNumber4").val(m[4]);
         }
     });


    /*$("#cCreditCardNumber1, #cCreditCardNumber2").on("blur", function () {
    	if (/[0-9]{4}/.test($("#cCreditCardNumber1").val()) && /[0-9]{4}/.test($("#cCreditCardNumber2").val()) && $(this).val().length && $(".jp-card.jp-card-identified", "#ccImage").hasClass("jp-card-mastercard") == false && $(".jp-card.jp-card-identified", "#ccImage").hasClass("jp-card-visa") == false) {
    		$('#txtErrorPopupMessage').html("Credit Card Number is for an unsupported credit card type. Please enter a Credit Card Number for VISA or MasterCard type.");
    		$("#divErrorPopup").popup("open", { "positionTo": "window" });
		}
	});*/

     $("select#cNameOnCard").on("change", function () {
     	if ($('#cBtnCopyAddress').data("is-the-same") == "Y") {
     		CopyMailingAddress($(this).find("option:selected").data("name-prefix"));
     	}
     });
     $('#cBtnCopyAddress').off("click").on("click", function (e) {
     	var $self = $(this);
     	if ($self.data("is-the-same") == "N") {
     		$self.data("is-the-same", "Y");
     		CopyMailingAddress($("select#cNameOnCard").find("option:selected").data("name-prefix"));
     		$self.html("Use different address for billing");
     		$self.addClass("chevron-circle-right-before").removeClass("chevron-circle-down-before");
     		$("#cBillingAddressPanel").addClass("hidden");
     	} else {
     		$self.data("is-the-same", "N");
     		$self.html("Use same address for billing");
     		$self.addClass("chevron-circle-down-before").removeClass("chevron-circle-right-before");
     		$("#cBillingAddressPanel").removeClass("hidden");
     	}
     	if (BUTTONLABELLIST != null) {
     		var value = BUTTONLABELLIST[$.trim($self.html()).toLowerCase()];
     		if (typeof value == "string" && $.trim(value) !== "") {
     			$self.html(value);
     		}
     	}
     	e.preventDefault();
     });
	//$("#cBillingAddressPanel").removeClass("hidden");
    //$('#cBtnCopyAddress').off("click");
	if ($("#ccImage").length > 0) {
		$(container).card({
			container: "#ccImage",
			formSelectors: {
				numberInput: '#cCreditCardNumber1,#cCreditCardNumber2,#cCreditCardNumber3,#cCreditCardNumber4',
				expiryInput: '#cExpirationDate1,#cExpirationDate2',
				cvcInput: '#cCVNNumber',
				nameInput: '#cNameOnCard'
			},
			placeholders: {
				number: '•••• •••• •••• ••••',
				name: 'Full Name',
				expiry: '••/••••',
				cvc: '•••'
			},
			masks: {
				cardNumber: '•', // optional - mask card number
				cardCVC: '•'
			},
			prebinding: {
				name: true
			}
		});
	}
	$(container).find("select").each(function (idx, ele) {
		$(ele).on("change", function () {
			setTimeout(function () {
				$(ele).focus();
			}, 100);
		});
	});
}

function validateInternalTransfer() {
    var strMessage = "";
    var accNumber = $("#tAccountNumber").val();
    var accType = $("#tAccountType").val();
	if (accNumber == "" || accType == "") {
		strMessage += "Please complete all of the required field(s)<br />";
	}
	if (accNumber != "" && $("#hdFundingAccountNumberFreeForm").val() == "Y" && !/^[0-9A-Za-z]+([\-\.\s]+[0-9A-Za-z]+)*$/.test(accNumber)) {
    	strMessage += "Invalid Account Number</br>";
    }
    return strMessage;
}

function validateFSelectAccount(element, normalClick) {
	//by clicking continue button 
	var $self = $(element);
	if ($self.attr("contenteditable") == "true") return false;
	var strMessage = "";
	if (normalClick) {
		var selectedOption = $("#fundingSelectAccount a.btn-header-theme.active");
		if (selectedOption.length == 0) {
			if ($("#divErrorPopup").length > 0) {
				$('#txtErrorPopupMessage').html("Please select an option");
				$("#divErrorPopup").popup("open", { "positionTo": "window" });
			} else {
				$('#txtErrorMessage').html(strMessage);
				$(element).next().trigger('click');
			}
			$self.removeClass("active btnActive_on");
		} else {
			//toggle status of selected options
			var $opt = $("a.btn-header-theme[data-value='INTERNAL TRANSFER']");
			$opt.closest("div.funding-option-lst").find("a.btn-header-theme.active").removeClass("active btnActive_on");
			$opt.addClass("active");
			$("#fundingSelectAccount").attr("data-acc-number", selectedOption.data("value"));
			$("#fundingSelectAccount").attr("data-acc-name", selectedOption.data("acc-name"));
			$("#fsSelectedFundingType").val($opt.data("value"));
			ViewInformationPage($('#hdHasCoApplicant').val());
			if ($('#hdHasMinorApplicant').val() == 'Y') {
				ViewMinorInformation();
			}
			$.lpqValidate.hideValidation("#divFundingTypeList");
			$.mobile.changePage("#reviewpage", {
				transition: "slide",
				reverse: false,
				changeHash: true
			});
		}
	}
	return strMessage;
}

function validateFInternalTransfer(element, normalClick) {
    //by clicking continue button 
	var $self = $(element);
	if ($self.attr("contenteditable") == "true") return false;
    var strMessage = "";
    if (normalClick) {
        strMessage = validateInternalTransfer();
        if (strMessage !== "") {
        	if ($("#divErrorPopup").length > 0) {
        		$('#txtErrorPopupMessage').html(strMessage);
        		$("#divErrorPopup").popup("open", { "positionTo": "window" });
        	} else {
        		$('#txtErrorMessage').html(strMessage);
        		$(element).next().trigger('click');
        	}
        	$self.removeClass("active btnActive_on");
        } else {
            var accNumber = $("#tAccountNumber").val();
            var accType = $("#tAccountType").val();
            $("#tAccountNumber").attr("data-acc-number", accNumber);
            $("#tAccountType").attr("data-acc-type", accType);
            //$.mobile.changePage("#pageFS", {
            //    transition: "slide",
            //    reverse: false,
            //    changeHash: true
        	//});
        	//toggle status of selected options
            var $opt = $("a.btn-header-theme[data-value='INTERNAL TRANSFER']");
            $opt.closest("div.funding-option-lst").find("a.btn-header-theme.active").removeClass("active btnActive_on");
            $opt.addClass("active");
            if ($("#fundingSelectAccount").length > 0) {
				$("#fundingSelectAccount").removeAttr("data-acc-number");
				$("#fundingSelectAccount").removeAttr("data-acc-name");
            }
            $("#fsSelectedFundingType").val($opt.data("value"));
            ViewInformationPage($('#hdHasCoApplicant').val());
            if ($('#hdHasMinorApplicant').val() == 'Y') {
            	ViewMinorInformation();
            }
            $.lpqValidate.hideValidation("#divFundingTypeList");
            $.mobile.changePage("#reviewpage", {
            	transition: "slide",
            	reverse: false,
            	changeHash: true
            });
        }
    //} else { //calling by another validate function

    }
    return strMessage;
}

function validateFinacialInsInfo() {
    var strMessage = "";
    var bAccountType = $("#bAccountType").val();
    var bNameOnCard = $("#bNameOnCard").val();
    var bAccountNumber = $("#bAccountNumber").val();
    var bRoutingNumber = $("#bRoutingNumber").val();
    var bBankName = $("#bBankName").val();
    var bBankState = $("#bBankState").val();

	if (bAccountType == "" || bNameOnCard == "" || bAccountNumber == "" ||
		bRoutingNumber == "" || bBankName == "" || bBankState == "") {
		strMessage += "Please complete all of the required field(s)<br />";
    }
    if (bRoutingNumber != "" && $("#hdStopInvalidRoutingNumber").val().toUpperCase() === "Y") {
	    if (/^[0-9]{9}$/.test(bRoutingNumber)) {
		    //call service to validate routing number
	    	$.ajax({
	    		type: 'POST',
	    		url: '/Handler/Handler.aspx',
	    		data: { command: 'LookupRoutingNumber', routing_num: bRoutingNumber },
	    		dataType: 'json',
	    		async: false,
	    		success: function (response) {
	    			if (response.IsSuccess) {
	    				$('#bBankName').val(response.Info.BankName);
	    				$('#bBankState').val(response.Info.State).selectmenu('refresh');
	    			} else {
	    				strMessage += response.Message + "</br>";
	    			}
	    		}
	    	});
	    } else {
	    	strMessage += "Routing Number is invalid</br>";
	    }

    }
    return strMessage;
}

function validateFFinacialIns(element, normalClick) {
    //by clicking continue button 
	var $self = $(element);
	if ($self.attr("contenteditable") == "true") return false;
    var strMessage = "";
    if (normalClick) {
        strMessage = validateFinacialInsInfo();
        if (strMessage !== "") {
        	$self.removeClass("active btnActive_on");
            if ($("#divErrorPopup").length > 0) {
            	$('#txtErrorPopupMessage').html(strMessage);
            	$("#divErrorPopup").popup("open", { "positionTo": "window" });
            } else {
            	$('#txtErrorMessage').html(strMessage);
            	$(element).next().trigger('click');
            }
        } else {
            var bAccountType = $("#bAccountType").val();
            var bNameOnCard = $("#bNameOnCard").val();
            var bAccountNumber = $("#bAccountNumber").val();
            var bRoutingNumber = $("#bRoutingNumber").val();
            var bBankName = $("#bBankName").val();
            var bBankState = $("#bBankState").val();
            $("#bAccountType").attr("data-acc-type", bAccountType);
            $("#bNameOnCard").attr("data-name-oncard", bNameOnCard);
            $("#bAccountNumber").attr("data-acc-number", bAccountNumber);
            $("#bRoutingNumber").attr("data-routing-number", bRoutingNumber);
            $("#bBankName").attr("data-bank-name", bBankName);
            $("#bBankState").attr("data-bank-state", bBankState);
            //$.mobile.changePage("#pageFS", {
            //    transition: "slide",
            //    reverse: false,
            //    changeHash: true
        	//});
        	//toggle status of selected options
            var $opt = $("a.btn-header-theme[data-value='TRANSFER FROM ANOTHER FINANCIAL INSTITUTION']");
            $opt.closest("div.funding-option-lst").find("a.btn-header-theme.active").removeClass("active btnActive_on");
            $opt.addClass("active");
	        $("#fsSelectedFundingType").val($opt.data("value"));
            ViewInformationPage($('#hdHasCoApplicant').val());
            if ($('#hdHasMinorApplicant').val() == 'Y') {
            	ViewMinorInformation();
            }
            $.lpqValidate.hideValidation("#divFundingTypeList");
            $.mobile.changePage("#reviewpage", {
            	transition: "slide",
            	reverse: false,
            	changeHash: true
            });
        }
    } else { //calling by another validate function

    }
    return strMessage;
}

function validateCreditCardInfo() {
    var strMessage = "";
    //var cCardType = $("#cCardType").val();
    var cCreditCardNumber = $("#cCreditCardNumber1").val() + "-" + $("#cCreditCardNumber2").val() + "-" + $("#cCreditCardNumber3").val() + "-" + $("#cCreditCardNumber4").val();
    var cExpirationDate = $("#cExpirationDate1").val() + "-" + $("#cExpirationDate2").val();
    var cCVNNumber = $("#cCVNNumber").val();

    var cNameOnCard = $("#cNameOnCard").val();
    var cBillingAddress = $("#cBillingAddress").val();
    var cBillingZip = $("#cBillingZip").val();
    var cBillingCity = $("#cBillingCity").val();
    var cBillingState = $("#cBillingState").val();

    //if (cCardType == "") {
    //    strMessage += "Please select Card Type</br>";
    //}
    if (/^[0-9]{4}(-[0-9]{4}){3}$/.test(cCreditCardNumber) === false) {
        strMessage += "Valid Credit Card Number is required</br>";
    } else if ($(".jp-card.jp-card-identified", "#ccImage").hasClass("jp-card-mastercard") == false && $(".jp-card.jp-card-identified", "#ccImage").hasClass("jp-card-visa") == false) {
    	strMessage += "Credit Card Number is for an unsupported credit card type. Please enter a Credit Card Number for VISA or MasterCard type.</br>";
    }
	
    if (/^(0[1-9]|1[0-2])-(19[0-9]{2}|2[0-9]{3})$/.test(cExpirationDate) === false) {
        strMessage += "Valid Expiration Date is required</br>";
    //}
    //else if (!checkMonthYear(cExpirationDate)) {
    //    //validate credit card expiration format
    //    strMessage = 'Invalid date, please update the date entered.</br>';
    } else {
        //validate credit card expiration
        var cMonth = parseInt(cExpirationDate.substring(0, 2));
        var cYear = parseInt(cExpirationDate.substring(3, 8));
        var today = new Date();
        var todayYear = today.getFullYear();
        var todayMonth = today.getMonth();

        if (todayYear > cYear) {
            strMessage += "The credit card is expired, please check the expiration date.<br/>";
        } else if (todayYear == cYear) {
            if (cMonth < todayMonth + 1) {
                strMessage += "The credit card is expired, please check the expiration date.<br/>";
            }
        }
    }

    if (cCVNNumber == "") {
        strMessage += "CVN Number is required</br>";
    }

    if (cNameOnCard == "") {
        strMessage += "Name On Card is required</br>";
    }

    if (cBillingAddress == "") {
        strMessage += "Billing Address is required</br>";
    }
    if (cBillingZip == "") {
        strMessage += "Billing Zip is required</br>";
    }
    if (cBillingCity == "") {
        strMessage += "Billing City is required</br>";
    }
    if (cBillingState == "") {
        strMessage += "Please select Billing State</br>";
    }
    return strMessage;
}
function validateFCreditCard(element, force) {
    //by clicking continue button 
	var $self = $(element);
	if ($self.attr("contenteditable") == "true") return false;
    var strMessage = "";
    strMessage = validateCreditCardInfo();
    if (strMessage !== "") {
    	$self.removeClass("active btnActive_on");
    	if ($("#divErrorPopup").length > 0) {
    		$('#txtErrorPopupMessage').html(strMessage);
    		$("#divErrorPopup").popup("open", { "positionTo": "window" });
    	} else {
    		$('#txtErrorMessage').html(strMessage);
    		$(element).next().trigger('click');
    	}
    } else if ((typeof force == "undefined" || force != true) && $(".jp-card.jp-card-identified .jp-card-number.jp-card-display", "#ccImage").hasClass("jp-card-valid") == false) {
    	$("div[data-place-holder='content']", "#popUpUndefineCard").html("<div>Card number is not a valid " + ($(".jp-card.jp-card-identified", "#ccImage").hasClass("jp-card-mastercard") ? "Master Card" : "Visa Card") + " number.</div><div>Do you wish to continue using it?</div>");
    	$("#popUpUndefineCard").popup("open", { "positionTo": "window" });
    } else {
    	//var cCardType = $("#cCardType").val();
    	var cCreditCardNumber1 = $("#cCreditCardNumber1").val();
    	var cCreditCardNumber2 = $("#cCreditCardNumber2").val();
    	var cCreditCardNumber3 = $("#cCreditCardNumber3").val();
    	var cCreditCardNumber4 = $("#cCreditCardNumber4").val();
    	var cExpirationDate1 = $("#cExpirationDate1").val();
    	var cExpirationDate2 = $("#cExpirationDate2").val();
    	var cCVNNumber = $("#cCVNNumber").val();
    	var cNameOnCard = $("#cNameOnCard").val();
    	var cBillingAddress = $("#cBillingAddress").val();
    	var cBillingZip = $("#cBillingZip").val();
    	var cBillingCity = $("#cBillingCity").val();
    	var cBillingState = $("#cBillingState").val();
    	//$("#cCardType").attr("data-card-type", cCardType);
    	$("#cCreditCardNumber1").attr("data-card-number", cCreditCardNumber1);
    	$("#cCreditCardNumber2").attr("data-card-number", cCreditCardNumber2);
    	$("#cCreditCardNumber3").attr("data-card-number", cCreditCardNumber3);
    	$("#cCreditCardNumber4").attr("data-card-number", cCreditCardNumber4);
    	$("#cExpirationDate1").attr("data-expiration-date", cExpirationDate1);
    	$("#cExpirationDate2").attr("data-expiration-date", cExpirationDate2);
    	$("#cCVNNumber").attr("data-cvn-number", cCVNNumber);
    	$("#cNameOnCard").attr("data-name-oncard", cNameOnCard);
    	$("#cBillingAddress").attr("data-billing-address", cBillingAddress);
    	$("#cBillingZip").attr("data-billing-zip", cBillingZip);
    	$("#cBillingCity").attr("data-billing-city", cBillingCity);
    	$("#cBillingState").attr("data-billing-state", cBillingState);
    	$('#cBtnCopyAddress').attr("data-same-address", $('#cBtnCopyAddress').data("is-the-same"));
    	//toggle status of selected options
    	var $opt = $("a.btn-header-theme[data-value='CREDIT CARD']");
    	$opt.closest("div.funding-option-lst").find("a.btn-header-theme.active").removeClass("active btnActive_on");
    	$opt.addClass("active");
    	$("#fsSelectedFundingType").val($opt.data("value"));
    	ViewInformationPage($('#hdHasCoApplicant').val());
    	if ($('#hdHasMinorApplicant').val() == 'Y') {
    		ViewMinorInformation();
    	}
    	$.lpqValidate.hideValidation("#divFundingTypeList");
    	$.mobile.changePage("#reviewpage", {
    		transition: "slide",
    		reverse: false,
    		changeHash: true
    	});
    }
    return strMessage;
}

function goBackFSelectAccount(element) {
	$("#fundingSelectAccount a.btn-header-theme.active").each(function (idx, item) {
		$(item).removeClass("active btnActive_on");
	});
	if ($("#fundingSelectAccount").attr("data-acc-number") && $("#fundingSelectAccount").attr("data-acc-number").length > 0) {
		//because use click "go back", that mean they want to discard changes (if any) => rollback data to previous state
		$("#fundingSelectAccount a.btn-header-theme[data-value='" + $("#fundingSelectAccount").attr("data-acc-number") + "']").addClass("active");
	}
	goToNextPage("#pageFS");
}

function goBackFInternalTransfer(element) {
	//in case when dialog shown, use take no action and click "go back" => clear option selected
    if (typeof $("#tAccountNumber").attr("data-acc-number") == typeof undefined) {
    	if ($("#fundingSelectAccount").length > 0) {
    		$("#fundingSelectAccount a.btn-header-theme.active").removeClass("active btnActive_on");
    	}
    	$("#tAccountNumber").val("");
        $("#tAccountType").val("");
        $("#tAccountType").selectmenu('refresh');
        //$("#pageFS div.funding-option-lst a.btn-header-theme").removeClass("active btnActive_on");
        //$("#fsSelectedFundingType").val("");
    } else {
        //because use click "go back", that mean they want to discard changes (if any) => rollback data to previous state
        $("#tAccountNumber").val($("#tAccountNumber").attr("data-acc-number"));
        $("#tAccountType").val($("#tAccountType").attr("data-acc-type"));
        $("#tAccountType").selectmenu('refresh');
    }
    if ($("#fundingSelectAccount").length > 0) {
    	goToNextPage("#fundingSelectAccount");
    } else {
    	goToNextPage("#pageFS");
    }
}

function goBackFFinacialIns(element) {
    //in case when dialog shown, use take no action and click "go back" => clear option selected
    if (typeof $("#bNameOnCard").attr("data-name-oncard") == typeof undefined) {
        $("#bAccountType").val("");
        $("#bAccountType").selectmenu('refresh');
        $("#bNameOnCard").val("");
        $("#bAccountNumber").val("");
        $("#bRoutingNumber").val("");
        $("#bBankName").val("");
        $("#bBankState").val("");
        $("#bBankState").selectmenu('refresh');
        //$("#pageFS div.funding-option-lst a.btn-header-theme").removeClass("active btnActive_on");
        //$("#fsSelectedFundingType").val("");
    } else {
        //because use click "go back", that mean they want to discard changes (if any) => rollback data to previous state
        $("#bAccountType").val($("#bAccountType").attr("data-acc-type"));
        $("#bAccountType").selectmenu('refresh');
        $("#bNameOnCard").val($("#bNameOnCard").attr("data-name-oncard"));
        $("#bAccountNumber").val($("#bAccountNumber").attr("data-acc-number"));
        $("#bRoutingNumber").val($("#bRoutingNumber").attr("data-routing-number"));
        $("#bBankName").val($("#bBankName").attr("data-bank-name"));
        $("#bBankState").val($("#bBankState").attr("data-bank-state"));
        $("#bBankState").selectmenu('refresh');
    }
	//history.back();
    goToNextPage("#pageFS");
}

function goBackFCreditCard(element) {
    //in case when dialog shown, user take no action and click "go back" => clear option selected
	if (typeof $("#cNameOnCard").attr("data-name-oncard") == typeof undefined) {
        //$("#cCardType").val("");
        //$("#cCardType").selectmenu('refresh');
        $("#cCreditCardNumber1").val("").data("disable-move-next", false);
        $("#cCreditCardNumber2").val("").data("disable-move-next", false);
        $("#cCreditCardNumber3").val("").data("disable-move-next", false);
        $("#cCreditCardNumber4").val("").data("disable-move-next", false);
        $("#cExpirationDate1").val("");
        $("#cExpirationDate1").selectmenu('refresh');
        $("#cExpirationDate2").val("");
        $("#cExpirationDate2").selectmenu('refresh');
        $("#cCVNNumber").val("");
        var appNames = getApplicantNames();
        $("#cNameOnCard").val(appNames[0].name);
        $("#cNameOnCard").selectmenu('refresh');
        $("#cBillingAddress").val("");
        $("#cBillingZip").val("");
        $("#cBillingCity").val("");
        $("#cBillingState").val("");
        $("#cBillingState").selectmenu('refresh');
        //$("#pageFS div.funding-option-lst a.btn-header-theme").removeClass("active btnActive_on");
		//$("#fsSelectedFundingType").val("");
        $('#cBtnCopyAddress').data("is-the-same", "Y");
        $("#cBtnCopyAddress").html("Use different address for billing");
        $("#cBtnCopyAddress").addClass("chevron-circle-right-before").removeClass("chevron-circle-down-before");
        $("#cBillingAddressPanel").addClass("hidden");
    } else {
        //because user click "go back", that mean they want to discard changes (if any) => rollback data to previous state
        //$("#cCardType").val($("#cCardType").attr("data-card-type"));
        //$("#cCardType").selectmenu('refresh');
        $("#cCreditCardNumber1").val($("#cCreditCardNumber1").attr("data-card-number")).data("disable-move-next", false);
        $("#cCreditCardNumber2").val($("#cCreditCardNumber2").attr("data-card-number")).data("disable-move-next", false);
        $("#cCreditCardNumber3").val($("#cCreditCardNumber3").attr("data-card-number")).data("disable-move-next", false);
        $("#cCreditCardNumber4").val($("#cCreditCardNumber4").attr("data-card-number")).data("disable-move-next", false);
        $("#cExpirationDate1").val($("#cExpirationDate1").attr("data-expiration-date"));
        $("#cExpirationDate1").selectmenu('refresh');
        $("#cExpirationDate2").val($("#cExpirationDate2").attr("data-expiration-date"));
        $("#cExpirationDate2").selectmenu('refresh');
        $("#cCVNNumber").val($("#cCVNNumber").attr("data-cvn-number"));
        $("#cNameOnCard").val($("#cNameOnCard").attr("data-name-oncard"));
        $("#cNameOnCard").selectmenu('refresh');
        $("#cBillingAddress").val($("#cBillingAddress").attr("data-billing-address"));
        $("#cBillingZip").val($("#cBillingZip").attr("data-billing-zip"));
        $("#cBillingCity").val($("#cBillingCity").attr("data-billing-city"));
        $("#cBillingState").val($("#cBillingState").attr("data-billing-state"));
        $("#cBillingState").selectmenu('refresh');
        $('#cBtnCopyAddress').data("is-the-same", $('#cBtnCopyAddress').attr("data-same-address"));
        if ($('#cBtnCopyAddress').attr("data-same-address") == "Y") {
        	$('#cBtnCopyAddress').html("Use different address for billing");
        	$("#cBtnCopyAddress").addClass("chevron-circle-right-before").removeClass("chevron-circle-down-before");
        	$("#cBillingAddressPanel").addClass("hidden");
        } else {
        	$("#cBtnCopyAddress").html("Use same address for billing");
        	$("#cBtnCopyAddress").addClass("chevron-circle-down-before").removeClass("chevron-circle-right-before");
        	$("#cBillingAddressPanel").removeClass("hidden");
        }
        if (BUTTONLABELLIST != null) {
        	var value = BUTTONLABELLIST[$.trim($("#cBtnCopyAddress").html()).toLowerCase()];
        	if (typeof value == "string" && $.trim(value) !== "") {
        		$("#cBtnCopyAddress").html(value);
        	}
        }
	}
	$("#fundingCreditCard div[data-placeholder='content']").card("reload");
	//history.back();
    goToNextPage("#pageFS");
}
function validateFS1() {
	var validator = true;
    var curSelectedFundOption = $("#fsSelectedFundingType").val();
    if (curSelectedFundOption === "NOT FUNDING AT THIS TIME" && $("a.btn-header-theme.active", "div.funding-option-lst").length === 0) {
    	$("#fsSelectedFundingType").val("");
	    curSelectedFundOption = "";
    }
    if (curSelectedFundOption != "PAYPAL") {
    	$.lpqValidate.hideValidation($("#divFundingTypeList a.btn-header-theme[data-value='PAYPAL']"));
    }
    if (curSelectedFundOption != "CREDIT CARD") {
    	$.lpqValidate.hideValidation($("#divFundingTypeList a.btn-header-theme[data-value='CREDIT CARD']"));
    }
    if (curSelectedFundOption != "TRANSFER FROM ANOTHER FINANCIAL INSTITUTION") {
    	$.lpqValidate.hideValidation($("#divFundingTypeList a.btn-header-theme[data-value='TRANSFER FROM ANOTHER FINANCIAL INSTITUTION']"));
    }
    if (curSelectedFundOption == undefined || curSelectedFundOption === "") {
        //make sure funding options are available if not skip validating
        if ($("#divFundingTypeList a.btn-header-theme").length > 0) {
            $.lpqValidate.showValidation("#divFundingTypeList", "Please select a funding option");
            validator = false;
        }
    } else {
        //Total Funding can be zero, if FI doesn't require minimum funding
        //If there is a minimum deposit then the logic should check for minimum

    	//If user select a funding option and total funding is zero then give warning, Evangalical cu
    	$.lpqValidate.hideValidation("#txtTotalDeposit");
        if (curSelectedFundOption !== "NOT FUNDING AT THIS TIME"){
            var depositAmt = Common.GetFloatFromMoney($("#txtTotalDeposit").val());
            if (isNaN(depositAmt) || depositAmt == 0) {
	            $.lpqValidate.showValidation("#txtTotalDeposit", "Funding amount is required");
	            validator = false;
            }
        }
        
        //This is for case when user manually change the total deposit amount and it fail minimum requirement
        //strFundingAmountMsg += validateInitialDepositProducts();
        if ($.lpqValidate("ValidateDeposit") === false) {
            validator = false;
        }
        if (validateFundingAmountByPaymentMethod(curSelectedFundOption) == false) {
        	validator = false;
        }
        if(validateFundingDisclosure() === false){
            validator = false;
        }
    }
    FS1.loadValueToData();
	return validator;
}

function LookupRoutingNumber(num, callback, failedCallback) {
    $.ajax({
        type: 'POST',
        url: '/Handler/Handler.aspx',
        data: { command: 'LookupRoutingNumber', routing_num: num },
        dataType: 'json',
        async: true,
        success: function (response) {
            if (response.IsSuccess) {
                if ($.isFunction(callback)) {
                    callback(response.Info.BankName, response.Info.State);
                }
            } else {
                //$('#txtErrorMessage').html(response.Message);
                //goToNextPage("#divErrorDialog");
                $('#txtErrorPopupMessage').html(response.Message);  //error message are not encoded, endoding will remove html tag
                $("#divErrorPopup").popup("open", { "positionTo": "window" });
                if ($.isFunction(failedCallback)) {
                    failedCallback();
                }
                //alert(response.Message);
            }
        }
    });
}

function validateFundingAmountByPaymentMethod(method) {
	var totalDepositAmount = xaProductFactory.getTotalDeposit();
	var result = true;
	var $btnFundingPaypal = $("#divFundingTypeList a.btn-header-theme[data-value='PAYPAL']");
	var $btnFundingCC = $("#divFundingTypeList a.btn-header-theme[data-value='CREDIT CARD']");
	var $btnFundingFinanceIns = $("#divFundingTypeList a.btn-header-theme[data-value='TRANSFER FROM ANOTHER FINANCIAL INSTITUTION']");
	if (method === "PAYPAL") {
		var paypalMaxFunding = $("#hdPaypalMaxFunding").val();
		if ($.trim(paypalMaxFunding) !== "" && Number(paypalMaxFunding) !== 0) {
			paypalMaxFunding = parseFloat(paypalMaxFunding, 10);
			if (paypalMaxFunding < totalDepositAmount) {
				$.lpqValidate.showValidation($btnFundingPaypal, "Maximum funding amount via Paypal is " + Common.FormatCurrency(paypalMaxFunding, true));
				result = false;
			} else {
				$.lpqValidate.hideValidation($("#divFundingTypeList a.btn-header-theme[data-value='PAYPAL']"));
			}
		}
	} else if (method === "CREDIT CARD") {
		var ccMaxFunding = $("#hdCCMaxFunding").val();
		if ($.trim(ccMaxFunding) !== "" && Number(ccMaxFunding) !== 0) {
			ccMaxFunding = parseFloat(ccMaxFunding, 10);
			if (ccMaxFunding < totalDepositAmount) {
				$.lpqValidate.showValidation($btnFundingCC, "Maximum funding amount via Credit Card is " + Common.FormatCurrency(ccMaxFunding, true));
				result = false;
			} else {
				$.lpqValidate.hideValidation($("#divFundingTypeList a.btn-header-theme[data-value='CREDIT CARD']"));
			}
		}
	} else if (method === "TRANSFER FROM ANOTHER FINANCIAL INSTITUTION") {
		var ACHMaxFunding = $("#hdACHMaxFunding").val();
		if ($.trim(ACHMaxFunding) !== "" && Number(ACHMaxFunding) !== 0) {
			ACHMaxFunding = parseFloat(ACHMaxFunding, 10);
			if (ACHMaxFunding < totalDepositAmount) {
				$.lpqValidate.showValidation($btnFundingFinanceIns, "Maximum funding amount via ACH is " + Common.FormatCurrency(ACHMaxFunding, true));
				result = false;
			} else {
				$.lpqValidate.hideValidation($("#divFundingTypeList a.btn-header-theme[data-value='TRANSFER FROM ANOTHER FINANCIAL INSTITUTION']"));
			}
		}
	}
	return result;
}
function handledFundingSourceType() {
	$('#txtTotalDeposit').val(Common.FormatCurrency(xaProductFactory.getTotalDeposit(), true));
	var $btnNotFundingThisTime = $("#divFundingTypeList").find("a.btn-header-theme[data-value='NOT FUNDING AT THIS TIME']");
    var totalDepositAmount = Common.GetFloatFromMoney($('#txtTotalDeposit').val());
    if (totalDepositAmount > 0) {
	    $btnNotFundingThisTime.attr("disabled", true).removeClass("active btnActive_on");
	    $btnNotFundingThisTime.parent().hide();
    } else {
	    $btnNotFundingThisTime.removeAttr("disabled");
	    $btnNotFundingThisTime.parent().show();
    }
}

function resetFunding() {
	$("#fsSelectedFundingType").val("");
	$("#bNameOnCard").removeAttr("data-name-oncard");
	$("#cNameOnCard").removeAttr("data-name-oncard");
	$("#divFundingTypeList").find("a.btn-header-theme.active").removeClass("active btnActive_on");
}
function registerDepositAmountValidator() {
	$.lpqValidate.removeValidationGroup("ValidateDeposit");
	$("input[data-field]", "#DivDepositInput").each(function (idx, ele) {
		var $errMsgPlaceHolder = ele;
		if ($(ele).attr("type") == "hidden") {
			$errMsgPlaceHolder = $(ele).closest("div");
		}
        $(ele).observer({
            validators: [
                function (partial) {
                	var msg = "";
                	if ($(this).attr("type") != "hidden") {
		                var productItem = xaProductFactory.getProductItemByCode($(this).data('field'));
		                var minDepositAmt = productItem.ProductMinDeposit;
		                var maxDepositAmt = productItem.ProductMaxDeposit;
		                var depositAmt = $(this).val();
		                minDepositAmt = Common.GetFloatFromMoney(minDepositAmt);
		                if (minDepositAmt < 0) { //make sure initial deposit is positive number
			                minDepositAmt = 0;
		                }
		                //depositAmt = String(depositAmt).replace(/[^\d.]/g, "");
		                depositAmt = Common.GetFloatFromMoney(depositAmt);
		                depositAmt = parseFloat(depositAmt);
		                if (depositAmt < minDepositAmt) {
			                msg = "The minimum deposit amount of " + productItem.ProductName + " is " + Common.FormatCurrency(minDepositAmt, true);
		                } else if (maxDepositAmt != null && maxDepositAmt != "") {
			                maxDepositAmt = String(maxDepositAmt).replace(/[^\d.]/g, "");
			                maxDepositAmt = parseFloat(maxDepositAmt);
			                if (maxDepositAmt > 0 && depositAmt > maxDepositAmt) {
				                msg = "The maximum deposit amount of " + productItem.ProductName + " is " + Common.FormatCurrency(maxDepositAmt, true);
			                }
		                }
		                $(this).val(Common.FormatCurrency($(this).val(), true));

                	} else {
                		var instanceId = $(this).data("instance-id");
		                var selectedRates = xaProductFactory.getSelectedRates();
		                if (!selectedRates || !selectedRates[instanceId]) {
			                return "Please select a rate and deposit amount";
		                }
	                }
                	$("#txtTotalDeposit").trigger("blur");
                	$.lpqValidate.hideValidation($("#txtTotalDeposit"));
                    return msg;
                }
            ],
            validateOnBlur: true,
            group: "ValidateDeposit",
            groupType: "complexUI",
            placeHolder: $errMsgPlaceHolder

        });
    });
    
}
function collectAccounts() {
	var entryList = [];
	if (qDATA.hasOwnProperty("acct_num0")) {
		var idx = 0;
		do {
			if (qDATA.hasOwnProperty("acct_num" + idx)) {
				var entry = {};
				entry.accountNumber = qDATA["acct_num" + idx];
				if (qDATA.hasOwnProperty("acct_bal" + idx)) {
					entry.accountBalance = qDATA["acct_bal" + idx];
				}
				if (qDATA.hasOwnProperty("acct_sfx" + idx)) {
					entry.accountSuffix = qDATA["acct_sfx" + idx];
				}
				if (qDATA.hasOwnProperty("acct_name" + idx)) {
					entry.accountName = qDATA["acct_name" + idx];
				}
				if (qDATA.hasOwnProperty("acct_type" + idx)) {
					entry.accountType = qDATA["acct_type" + idx];
				}
				entryList.push(entry);
			}
			idx++;
		} while (idx < 10);
	}
	return entryList;
}
$(function () {
    handledFundingSourceType();
    $('#DivDepositInput').on('change keyup', function (event, ui) {
        handledFundingSourceType();
    });
    $(document).on("pageshow", function () {
        //init event for PageFS
    	$("#pageFS").find("a.btn-header-theme.funding-option").each(function () {
            var $self = $(this);
            $self.off("click").on("click", function () {
            	$.lpqValidate.hideValidation("#divFundingTypeList");
            	var curOption = $self.data("value");
            	if (curOption != "PAYPAL") {
            		$.lpqValidate.hideValidation($("#divFundingTypeList a.btn-header-theme[data-value='PAYPAL']"));
            	}
            	if (curOption != "CREDIT CARD") {
            		$.lpqValidate.hideValidation($("#divFundingTypeList a.btn-header-theme[data-value='CREDIT CARD']"));
            	}
            	if (curOption != "TRANSFER FROM ANOTHER FINANCIAL INSTITUTION") {
            		$.lpqValidate.hideValidation($("#divFundingTypeList a.btn-header-theme[data-value='TRANSFER FROM ANOTHER FINANCIAL INSTITUTION']"));
            	}
                var validator = true;
                //Total Funding can be zero, if FI doesn't require minimum funding
                //If there is a minimum deposit then the logic should check for minimum

                
            	
                //This is for case when user manually change the total deposit amount and it fail minimum requirement
            	//errorStr += validateInitialDepositProducts();

            	if ($.lpqValidate("ValidateDeposit") === false) {
	                validator = false;
            	}
            	//If user select a funding option and total funding is zero then give warning, Evangalical cu
            	if (curOption !== "NOT FUNDING AT THIS TIME") {
            		var depositAmt = Common.GetFloatFromMoney($("#txtTotalDeposit").val());
            		if (isNaN(depositAmt) || depositAmt == 0) {
            			validator = false;
            			$.lpqValidate.showValidation("#txtTotalDeposit", "Funding amount is required");
            		} else {
            			$.lpqValidate.hideValidation("#txtTotalDeposit");
            		}
            	}
            	if (validateFundingAmountByPaymentMethod(curOption) == false) {
            		validator = false;
            	}
            	if (validator == false) {
            		$("#fsSelectedFundingType").val("");
            		//$self.removeClass("active btnActive_on");
            		$self.closest("div.funding-option-lst").find("a.btn-header-theme.active").removeClass("active btnActive_on");
				} else {
            		if (curOption === "PAYPAL" || curOption === "MAIL A CHECK" || curOption === "NOT FUNDING AT THIS TIME") {
            			//if ($self.hasClass("active")) {
					    //    return;
				        //}
            			//toggle status of selected options
            			$self.closest("div.funding-option-lst").find("a.btn-header-theme.active").removeClass("active btnActive_on");
            			$self.addClass("active");
				        $("#fsSelectedFundingType").val(curOption);
				        ViewInformationPage($('#hdHasCoApplicant').val());
				        if ($('#hdHasMinorApplicant').val() == 'Y') {
					        ViewMinorInformation();
				        }
				        $.lpqValidate.hideValidation("#txtTotalDeposit");
				        $.lpqValidate.hideValidation("#divFundingTypeList");
						if ($("#fundingDisclosure").length === 0 || curOption === "NOT FUNDING AT THIS TIME") {
    				        $.mobile.changePage("#reviewpage", {
    					        transition: "slide",
    					        reverse: false,
    					        changeHash: true
    				        });
                        }
			        } else {
            			//init event to show dialog
            			var dialogPage = $self.attr("dialog-page-id");
            			if (typeof dialogPage != typeof undefined) {
            				var dialogContentElement = $("#" + dialogPage + " div[data-placeholder='content']");
            				//only load content one time
            				if (typeof dialogContentElement.attr("content-loaded") == typeof undefined) {
            					var dialogHtmlContent = FS1.buildHtmlForType(dialogPage);
            					$("#" + dialogPage + " div[data-placeholder='content']").html(dialogHtmlContent);
            					$("#" + dialogPage + " div[data-placeholder='content']").attr("content-loaded", true);
            					InitStuffs($("#" + dialogPage + " div[data-placeholder='content']"));
            					if (dialogPage == "fundingInternalTransfer" && qDATA.hasOwnProperty("acct_num0")) {
            						$("#fundingSelectAccount div[data-placeholder='content']").html(FS1.buildHtmlForSelectAccountDialog(collectAccounts()));
						            InitSelectAccountDialog();
					            }
				            }
            				if (dialogPage === "fundingFinancialIns") {
            					var $txtaccName = $("#" + dialogPage).find("#bNameOnCard");
					            var nameOnCard = $txtaccName.val();
            					$txtaccName.find("option").remove();
					            $.each(getApplicantNames(), function (i, item) {
					            	$txtaccName.append('<option ' + (i == 0 ? 'selected="selected"' : '') + " data-name-prefix='" + item.prefix + "' value='" + item.name + "'>" + item.name + "</option>");
            						if (nameOnCard == item.name) $txtaccName.val(nameOnCard);
					            });
            					$txtaccName.selectmenu();
            					$txtaccName.selectmenu("refresh");
				            } else if (dialogPage === "fundingCreditCard") {
            					var $txtaccName = $("#" + dialogPage).find("#cNameOnCard");
            					var nameOnCard = $txtaccName.val();
            					$txtaccName.find("option").remove();
            					$.each(getApplicantNames(), function (i, item) {
            						$txtaccName.append('<option ' + (i == 0 ? 'selected="selected"' : '') + " data-name-prefix='" + item.prefix + "' value='" + item.name + "'>" + item.name + "</option>");
            						if (nameOnCard == item.name) $txtaccName.val(nameOnCard);
            					});
            					$txtaccName.selectmenu();
            					$txtaccName.selectmenu("refresh");
            					$("#fundingCreditCard div[data-placeholder='content']").card("reload");
            					if ($("#cBtnCopyAddress").data("is-the-same") == "N") {
            						$("#cBtnCopyAddress").html("Use same address for billing");
            						$("#cBtnCopyAddress").addClass("chevron-circle-down-before").removeClass("chevron-circle-right-before");
            						$("#cBillingAddressPanel").removeClass("hidden");
            					} else {
            						$("#cBillingAddressPanel").addClass("hidden");
            						$("#cBtnCopyAddress").html("Use different address for billing");
            						$("#cBtnCopyAddress").addClass("chevron-circle-right-before").removeClass("chevron-circle-down-before");
            					}
            					if (BUTTONLABELLIST != null) {
            						var value = BUTTONLABELLIST[$.trim($("#cBtnCopyAddress").html()).toLowerCase()];
            						if (typeof value == "string" && $.trim(value) !== "") {
            							$("#cBtnCopyAddress").html(value);
            						}
            					}
				            } else if (dialogPage === "fundingInternalTransfer" && qDATA.hasOwnProperty("acct_num0")) {
					            dialogPage = "fundingSelectAccount";
				            }
				            $.lpqValidate.hideValidation("#txtTotalDeposit");
                            $.mobile.changePage("#" + dialogPage, {
                                transition: "pop",
                                reverse: false,
                                changeHash: true
                            });
            			}
			        }
		            //}
	            }
            });
        });
    });
    $('#pageFS').on('pageshow', function () {
    	handledFundingSourceType();
    	registerDepositAmountValidator();
        registerFundingDisclosureValidator();
    });
    $("#popUpAccountGuide,#popUpUndefineCard").on("popupafteropen", function (event, ui) {
        $("#" + this.id + "-screen").height("");
    });
    $("#popUpUndefineCard").on("popupafterclose", function (event, ui) {
	    $("#cCreditCardNumber1").focus();
    });
	$("#fundingCreditCard").on("pageshow", function() {
		$("#cCreditCardNumber1").focus();
	});
	$("#fundingInternalTransfer,#fundingCreditCard,#fundingFinancialIns").on("pagehide", function (event, ui) {
		$('html, body').stop().animate({ scrollTop: $("#divFundingTypeList").offset().top - 100 }, '500', 'swing');
		setTimeout(function () {
			$("#divFundingTypeList a[dialog-page-id='" + $(this).attr("id") + "']").focus();
		}, 200);
	});
});
