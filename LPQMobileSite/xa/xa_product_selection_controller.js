﻿
(function (xaProductFactory, $, undefined) {
	function ProductSelectionItem(productName, depositAmount, productQuestions, productServices, productRate, term, apy) {
		this.productName = productName;
		this.depositAmount = depositAmount;
		this.productQuestions = productQuestions;
		this.productServices = productServices ? productServices : [];
		this.productRate = productRate;
		this.term = term;
		this.apy = apy;

	}
	var actionStack = [];
	var customQuestionAnswers = {};
	var selectedServices = {};
	var selectedRates = {};
	var locationPoolCode = null;

	xaProductFactory.setLocationPoolCode = function(value) {
		locationPoolCode = value;
	}
	xaProductFactory.getSelectedRates = function() {
		return selectedRates;
	}
	function toggleResponsiveCategory() {
		var $btnGroupCategory = $(".product-selection-panel-v1 .btn-group");
		if ($btnGroupCategory.length == 0) return;
		var $btnList = $btnGroupCategory.find(".btn:last-child");
		if ($btnList.length == 0) return; //there is no additional product
        if ($btnList[0].offsetTop > 0) {
			$btnGroupCategory.closest("tr").addClass("hide-category");
		} else {
			$btnGroupCategory.closest("tr").removeClass("hide-category");
		}
	}

	//xaProductFactory.toggleResponsiveCategory = toggleResponsiveCategory;
	function performPQAdvancedLogics() {
		if (ADVANCEDLOGICLIST != null) {
			$.each(ADVANCEDLOGICLIST, function (idx_advLogic, item) {
				if (item.Conditions.length == 0) return;
				if (item.TargetType == "PQ" && $("div.pq-wrapper[qname='" + item.TargetItem + "']").length > 0) {
					// Warning: Some variables here (idx_prodQuestion, ele) are not being minified for some reason. Do not use single-character variables.
					$("div.pq-wrapper[qname='" + item.TargetItem + "']").each(function (idx_prodQuestion, ele) {
						var $container = $(ele).closest(".product-questions");
						var exp = item.Expression;
						exp = exp.replace(/AND/g, "&&");
						exp = exp.replace(/OR/g, "||");
						$.each(item.Conditions, function (idx_cond, condition) {
							var result = resolveAdvancedLogicCondition($container, condition, item);
							exp = exp.replace("$exp" + (idx_cond + 1), result);
						});
						var pqItem = $(ele);
						//var qid = pqItem.attr("id");
						if (eval(exp)) {
							pqItem.removeClass("hidden");
						} else {
							pqItem.addClass("hidden");
						}
					});

				}
			});
		}
	}
	function resolveAdvancedLogicCondition(container, condition, item, targetItem) {
		var $ctrl = $("[iname='" + condition.Name + "']", container);
		if ($ctrl.is(":checkbox")) {
			var res = false;
			$("[iname='" + condition.Name + "']:checked", container).each(function (idx, ele) {
				res = res || resolveOperation($(ele).val(), condition.Value, condition.Comparer);
			});
			return res;
		} else if ($ctrl.hasClass("yesno-group")) {
			var res = false;
			var $checkedCtrl = $ctrl.find(">div.active");
			if ($checkedCtrl.length > 0) {
				res = resolveOperation($checkedCtrl.data("value"), condition.Value, condition.Comparer);
			}
			return res;
		} else if ($ctrl.is("select")) {
			var $selectedOption = $ctrl.find("option:selected");
			if ($selectedOption.length == 0) {
				return false;
			}
			return resolveOperation($selectedOption.text(), condition.Value, condition.Comparer);
		} else {
			var curValue = $ctrl.val();
			return resolveOperation(curValue, condition.Value, condition.Comparer);
		}
	}
	function resolveOperation(curValue, value, op) {
		switch (op) {
			case "equal":
				if (typeof curValue == "string") curValue = curValue.toLowerCase();
				if (typeof value == "string") value = value.toLowerCase();
				return curValue == value;
			case "greaterThan":
				return parseFloat(curValue) > parseFloat(value);
			case "lessThan":
				return parseFloat(curValue) < parseFloat(value);
			case "greaterThanOrEqual":
				return parseFloat(curValue) >= parseFloat(value);
			case "lessThanOrEqual":
				return parseFloat(curValue) <= parseFloat(value);
			case "notEqual":
				return curValue != value;
			default:
				return false;
		}
	}
	function buildDetailPopup(productItem, popContainer, srcEle, command) {
		if (productItem != null) {
			popContainer.find('div[data-place-holder="heading"]').html(productItem.ProductName);
			popContainer.find('input:hidden[data-place-holder="productCode"]').val(productItem.ProductCode);
			var $srcEle = $(srcEle);
			if ($srcEle.hasClass("selected-prod")) {
				$("#btnAddProduct", popContainer).addClass("hidden");
				if (command == "info") {
					$("#btnSaveProduct", popContainer).addClass("hidden");
					$("#btnCloseProductDetail", popContainer).removeClass("hidden").data("loaded-id", $srcEle.attr("id"));
				} else {
					$("#btnSaveProduct", popContainer).removeClass("hidden").data("loaded-id", $srcEle.attr("id"));
					$("#btnCloseProductDetail", popContainer).addClass("hidden");
				}
				//	popContainer.find('input:hidden[data-place-holder="sourceElementId"]').val($srcEle.attr("id"));
			} else {
				$("#btnAddProduct", popContainer).removeClass("hidden");
				$("#btnSaveProduct", popContainer).addClass("hidden");
				$("#btnCloseProductDetail", popContainer).addClass("hidden");
			}
			if ($.trim(productItem.ProductDescription) !== "") {
				$("fieldset[data-place-holder='description'] p").html(productItem.ProductDescription.replace("</scripttag>", "</script>"));
				$("fieldset[data-place-holder='description'] p").trigger("create");
				$("fieldset[data-place-holder='description']").show();
			} else {
				$("fieldset[data-place-holder='description']").hide();
			}


			if (productItem.CustomQuestions.length > 0) {
				if (productItem.CustomQuestions.length == 1 && productItem.CustomQuestions[0].AnswerType == "HEADER") {
					$("fieldset[data-place-holder='questions']").hide();
				} else {
					var $questionPlaceHolder = popContainer.find("fieldset[data-place-holder='questions'] > div.ui-controlgroup-controls");
					/*if ($srcEle.hasClass("selected-prod") && $("#" + $srcEle.attr("id"), "#divSelectedProductPool").find("div.custom-question-pool").length > 0) {
						$("#" + $srcEle.attr("id"), "#divSelectedProductPool").find("div.custom-question-pool").show().appendTo($questionPlaceHolder);
					} else {
						var $block = $("#divProductQuestionPool").find("div[data-eid='divProductQuestion_chk" + productItem.ProductCode + "']");
						$block.find("input, select").each(function (idx, ele) {
							$(ele).removeAttr("data-role");
						});
						$block.clone().show().addClass("custom-question-pool").appendTo($questionPlaceHolder);
					}*/
					var $clonedCustomQuestionBlock = $("#divProductQuestionPool").find("div[data-eid='divProductQuestion_chk" + productItem.ProductCode + "']").clone();
					$clonedCustomQuestionBlock.find("input, select").each(function (idx, ele) {
						$(ele).removeAttr("data-role");
					});
					$clonedCustomQuestionBlock.addClass("custom-question-pool").show().appendTo($questionPlaceHolder);
					if ($srcEle.hasClass("selected-prod")) {
						setCustomQuestionAnswers($srcEle);
					}
					if ($questionPlaceHolder.find("div.pq-wrapper:not(.hidden)").length > 0) {
						$("fieldset[data-place-holder='questions']").show();
					} else {
						$("fieldset[data-place-holder='questions']").hide();
					}
				}
			} else {
				$("fieldset[data-place-holder='questions']").hide();
			}
			if (productItem.ProductServices.length > 0) {
				var $servicePlaceHolder = popContainer.find("fieldset[data-place-holder='services'] > div.ui-controlgroup-controls");
				//$servicePlaceHolder.attr("product-code", productItem.ProductCode);
				var $clonedServiceBlock = $("#divProductServicesPool>div[product-code='" + productItem.ProductCode + "']").clone();
				$clonedServiceBlock.find("input").each(function (idx, ele) {
					$(ele).removeAttr("data-role");
				});
				$clonedServiceBlock.addClass("service-pool").appendTo($servicePlaceHolder);
				if ($srcEle.hasClass("selected-prod")) {
					setSelectedServices($srcEle);
				}
				/*$clonedServiceBlock.find(">div").each(function () {
					var $self = $(this);
					//bind event for service items
					$self.off("click").on("click", function (evt) {
						var $ele = $self.find(">label");
						if ($ele.data("is-required") === true) {
							evt.stopPropagation();
							return;
						}
						$ele.data("selected", !$self.find("input:checkbox").is(":checked"));
					});
				});*/
				$("fieldset[data-place-holder='services']").show();
			} else {
				$("fieldset[data-place-holder='services']").hide();
			}
			if (productItem.ProductRates.length > 0) {
				var $ratePlaceHolder = popContainer.find("fieldset[data-place-holder='rates']");
				var $clonedRateBlock = $("#divProductRatesPool>div[product-code='" + productItem.ProductCode + "']").clone();
				$clonedRateBlock.find("input").each(function (idx, ele) {
					$(ele).removeAttr("data-role");
				});

				var poolNames = [];
				if (locationPoolCode) {
					poolNames = getSelectedZipPoolNames(locationPoolCode, ZIPCODEPOOLLIST, ZIPCODEPOOLNAME);
				}
				_.forEach(productItem.ProductRates, function (rateItem) {
					if (locationPoolCode != null && locationPoolCode != "" && (poolNames.length == 0 || (rateItem.ZipCodeFilterType == "INCLUDE" && _.indexOf(poolNames, rateItem.LocationPool) == -1) || (rateItem.ZipCodeFilterType == "EXCLUDE" && _.indexOf(poolNames, rateItem.LocationPool) > -1))) {
						var $beingRemovedTbl = $("#rate_" + productItem.ProductCode + "_" + rateItem.ID + "_v", $clonedRateBlock).closest("table");
						$beingRemovedTbl.next("div.separator").remove();
						$beingRemovedTbl.remove();
						$("#rate_" + productItem.ProductCode + "_" + rateItem.ID + "_h", $clonedRateBlock).closest("tr").remove();
					}
				});

				$clonedRateBlock.addClass("rate-pool").appendTo($ratePlaceHolder);
				$("fieldset[data-place-holder='rates']").show();
			} else {
				$("fieldset[data-place-holder='rates']").hide();
			}
			$(popContainer).enhanceWithin();
			bindProductQuestionEvent(popContainer);
			//popContainer.find("a[href]:not(a.btn-header-theme,a.header_theme2)").on("click tap", function () {
			//	//support clicking all a element will open doc viewer dialog
			//	//then turn on the flag to indicate that need to reopen popup detail when doc viewer closed
			//    actionStack.push("clickA");
			//});
			bindLinkClickEvent(popContainer);
		}
	}
	
	function selectRateItemHandler(srcEle, container, productItem) {
		var $self = $(srcEle);
		var $ele = $self.find("input:radio[rate_code]");
		var rateCode = $ele.attr("rate_code");
		$ele.closest("div[product-code].rates-table").attr("selected-rate", rateCode);
		var rad = $self.closest("div.rates-table").find("input:radio[rate_code='" + rateCode + "']");
		$self.closest("div.rates-table").find("input:radio[rate_code]").not(rad).each(function (idx, ctrl) {
			$(ctrl).prop("checked", false);//.checkboxradio("refresh");
			$(ctrl).prev("label").removeClass("ui-radio-on");
			$(ctrl).prev("label").addClass("ui-radio-off");
		});
		$self.closest("div.rates-table").find("input:radio[rate_code='" + rateCode + "']").each(function (idx, ctrl) {
			$(ctrl).prop("checked", true);//.checkboxradio("refresh");
			$(ctrl).prev("label").removeClass("ui-radio-off");
			$(ctrl).prev("label").addClass("ui-radio-on");
		});
		var selectedRateItem = _.find(productItem.ProductRates, { RateCode: rateCode });
		var $depositPlaceHolder = container.find("fieldset[data-place-holder='deposit']");
		$depositPlaceHolder.find("span[data-name='depositamount']").remove();
		var minDeposit = Math.max(selectedRateItem.MinDeposit, 0);
		var maxDeposit = Math.max(selectedRateItem.MaxDeposit, 0);
		var $txtRateDeposit = $("#txtRateDepositAmount", container);
		if (productItem.IsAutoCalculateTier == true) {
			var lowestMinDepositRate = findLowestMinDepositRate(productItem);
			var highestMaxDepositRate = findHighestMaxDepositRate(productItem);
			minDeposit = lowestMinDepositRate.MinDeposit;
			maxDeposit = highestMaxDepositRate.MaxDeposit;
		}
		if (minDeposit > 0 || maxDeposit > 0) {
			var $minDepositBlock = null;
			var $maxDepositBlock = null;
			var $requireDepositBlock = null;
			var $depositAmountBlock = $("<span/>", { "data-name": "depositamount" });
			$depositAmountBlock.append(document.createTextNode(" ("));
			var sep = "";
			if (minDeposit > 0) {
				$minDepositBlock = $("<span/>", { "data-name": "mindeposit" }).text(Common.FormatCurrency(minDeposit, true) + " min");
				sep = ", ";
				$requireDepositBlock = $("<span/>", { style: "display: inline", "class": "require-span" }).text("*");
			}
			if (maxDeposit > 0) {
				$maxDepositBlock = $("<span/>", { "data-name": "maxdeposit" }).text(sep + Common.FormatCurrency(maxDeposit, true) + " max");
			}
			$depositAmountBlock.append($minDepositBlock, $maxDepositBlock);
			$depositAmountBlock.append(document.createTextNode(")"));
			$depositAmountBlock.append($requireDepositBlock);
			$depositPlaceHolder.find(".heading-title").append($depositAmountBlock);
		}
		$txtRateDeposit
			.data("min-deposit", minDeposit)
			.data("max-deposit", maxDeposit)
			.data("is-auto-calculated-tier", productItem.IsAutoCalculateTier)
			.val(Common.FormatCurrency(selectedRateItem.MinDeposit, true));

		$.lpqValidate.hideAllValidation("ValidateFundingDialog");

		var $termWrapper = $("fieldset[data-place-holder='term']", container);
		if (selectedRateItem.MinTerm > 0 || selectedRateItem.MaxTerm > 0) {
			$termWrapper.removeClass("hidden");
			var $maturityDateWrapper = $termWrapper.find(".maturity-date-wrapper");
			var termTypeLabel = "";
			if (productItem.TermType == "M") {
				$maturityDateWrapper.addClass("hidden");
				termTypeLabel = "month";
			} else {
				$maturityDateWrapper.removeClass("hidden");
				termTypeLabel = "day";
			}
			var rateTermLengthLabel = "";
			if (selectedRateItem.MinTerm > 0 && selectedRateItem.MaxTerm > 0) {
				rateTermLengthLabel = padLeft(selectedRateItem.MinTerm, 2) + " - " + padLeft(selectedRateItem.MaxTerm, 2) + " " + termTypeLabel + "s";
			} else if (selectedRateItem.MinTerm > 0) {
				rateTermLengthLabel = padLeft(selectedRateItem.MinTerm, 2) + " " + termTypeLabel + (selectedRateItem.MinTerm == 1 ? "" : "s") + " (minimum)";
			} else if (selectedRateItem.MaxTerm > 0) {
				rateTermLengthLabel = padLeft(selectedRateItem.MaxTerm, 2) + " " + termTypeLabel + (selectedRateItem.MaxTerm == 1 ? "" : "s") + " (maximum)";
			}
			$("#lblRateTermLength", $termWrapper).text(rateTermLengthLabel);
			$("#txtRateTermLength", $termWrapper)
				.data("term-type", productItem.TermType)
				.data("min-term", selectedRateItem.MinTerm)
				.data("max-term", selectedRateItem.MaxTerm);
			$.lpqValidate.hideAllValidation("ValidateFundingDialog");
		} else {
			$termWrapper.addClass("hidden");
		}
		$("#txtRateMaturityDate2", "#fundingDialog").val("");
		$("#txtRateMaturityDate1", "#fundingDialog").val("");
		$("#txtRateMaturityDate3", "#fundingDialog").val("");
		$("#txtRateTermLength", "#fundingDialog").val("");
	}

	function bindRateEvent(container, productItem) {
		var $container = $(container);
		$("table.horizontal-table tbody>tr, table.vertical-table", $container).off("click").on("click", function (evt) {
			selectRateItemHandler(this, container, productItem);
		});
	}

	function buildFundingPopup(productItem, popContainer, instanceId) {
		if (productItem != null) {
			popContainer.find('div[data-place-holder="heading"]').html(productItem.ProductName);
			popContainer.find('input:hidden[data-place-holder="productCode"]').val(productItem.ProductCode);
			popContainer.find('input:hidden[data-place-holder="instanceId"]').val(instanceId);
			var $ratePlaceHolder = popContainer.find("fieldset[data-place-holder='rates']");
			if (productItem.ProductRates.length > 0) {
				var $clonedRateBlock = $("#divProductRatesPool>div[product-code='" + productItem.ProductCode + "']").clone();
				
				$clonedRateBlock.find("input").each(function (idx, ele) {
					$(ele).removeAttr("data-role");
				});
				var poolNames = [];
				if (locationPoolCode) {
					poolNames = getSelectedZipPoolNames(locationPoolCode, ZIPCODEPOOLLIST, ZIPCODEPOOLNAME);
				}
				_.forEach(productItem.ProductRates, function (rateItem) {
					if (locationPoolCode != null && locationPoolCode != "" && (poolNames.length == 0 || (rateItem.ZipCodeFilterType == "INCLUDE" && _.indexOf(poolNames, rateItem.LocationPool) == -1) || (rateItem.ZipCodeFilterType == "EXCLUDE" && _.indexOf(poolNames, rateItem.LocationPool) > -1))) {
						var $beingRemovedTbl = $("#rate_" + productItem.ProductCode + "_" + rateItem.ID + "_v", $clonedRateBlock).closest("table");
						$beingRemovedTbl.next("div.separator").remove();
						$beingRemovedTbl.remove();
						$("#rate_" + productItem.ProductCode + "_" + rateItem.ID + "_h", $clonedRateBlock).closest("tr").remove();
					}
				});

				$clonedRateBlock.addClass("rate-pool").appendTo($ratePlaceHolder);

				$ratePlaceHolder.removeClass("hidden");
				if (productItem.IsAutoCalculateTier == false) {
					$clonedRateBlock.removeClass("hide-radio");
					bindRateEvent(popContainer, productItem);
					$ratePlaceHolder.find(".heading-title").text("Choose your rate");
				} else {
					$ratePlaceHolder.find(".heading-title").text("Details");
				}


				if (selectedRates[instanceId]) { //edit
					var $selectedRateBlock = $("#rate_" + productItem.ProductCode + "_" + selectedRates[instanceId].ID + "_h", $clonedRateBlock).closest("tr");
					selectRateItemHandler($selectedRateBlock, popContainer, productItem);
					$("#txtRateDepositAmount", popContainer).val(selectedRates[instanceId].DepositAmount);
					$("#txtRateTermLength", popContainer).val(selectedRates[instanceId].Term).trigger("keyup");
				} else if (productItem.IsAutoCalculateTier == false) {
					var $firstRateRadio = popContainer.find("table.horizontal-table input:radio[rate_code]").eq(0);
					if ($firstRateRadio.length >= 0 /*&& $firstRateRadio.closest(".has-radio").length == 0*/) {
						selectRateItemHandler($firstRateRadio.closest("tr"), popContainer, productItem);
					}
				} else {
					var lowestMinDepositRate = findLowestMinBalanceRate(productItem);
					var $lowestMinDepositRateBlock = $("#rate_" + productItem.ProductCode + "_" + lowestMinDepositRate.ID + "_h", $clonedRateBlock).closest("tr");
					selectRateItemHandler($lowestMinDepositRateBlock, popContainer, productItem);
				}
			} else {
				$ratePlaceHolder.addClass("hidden");
			}

			popContainer.enhanceWithin();
			$.lpqValidate.removeValidationGroup("ValidateFundingDialog");
			registerFundingDialogValidator(popContainer);
		}
	}
	function findLowestMinBalanceRate(productItem) {
		var result = null;
		var poolNames = [];
		if (locationPoolCode) {
			poolNames = getSelectedZipPoolNames(locationPoolCode, ZIPCODEPOOLLIST, ZIPCODEPOOLNAME);
		}
		_.forEach(productItem.ProductRates, function (rateItem) {
			if (locationPoolCode != null && locationPoolCode != "" && (poolNames.length == 0 || (rateItem.ZipCodeFilterType == "INCLUDE" && _.indexOf(poolNames, rateItem.LocationPool) == -1) || (rateItem.ZipCodeFilterType == "EXCLUDE" && _.indexOf(poolNames, rateItem.LocationPool) > -1))) {
				return;
			}
			if (result == null || result.MinBalance >= rateItem.MinBalance) {
				result = rateItem;
			}
		});
		return result;
	}
	function findLowestMinDepositRate(productItem) {
		var result = null;
		var poolNames = [];
		if (locationPoolCode) {
			poolNames = getSelectedZipPoolNames(locationPoolCode, ZIPCODEPOOLLIST, ZIPCODEPOOLNAME);
		}
		_.forEach(productItem.ProductRates, function (rateItem) {
			if (locationPoolCode != null && locationPoolCode != "" && (poolNames.length == 0 || (rateItem.ZipCodeFilterType == "INCLUDE" && _.indexOf(poolNames, rateItem.LocationPool) == -1) || (rateItem.ZipCodeFilterType == "EXCLUDE" && _.indexOf(poolNames, rateItem.LocationPool) > -1))) {
				return;
			}
			if (result == null || result.MinDeposit > rateItem.MinDeposit) {
				result = rateItem;
			}
		});
		return result;
	}
	function findHighestMaxDepositRate(productItem) {
		var result = null;
		var poolNames = [];
		if (locationPoolCode) {
			poolNames = getSelectedZipPoolNames(locationPoolCode, ZIPCODEPOOLLIST, ZIPCODEPOOLNAME);
		}
		_.forEach(productItem.ProductRates, function (rateItem) {
			if (locationPoolCode != null && locationPoolCode != "" && (poolNames.length == 0 || (rateItem.ZipCodeFilterType == "INCLUDE" && _.indexOf(poolNames, rateItem.LocationPool) == -1) || (rateItem.ZipCodeFilterType == "EXCLUDE" && _.indexOf(poolNames, rateItem.LocationPool) > -1))) {
				return;
			}
			if (result == null || result.MaxDeposit < rateItem.MaxDeposit) {
				result = rateItem;
			}
		});
		return result;
	}
	function findRateBasedOnFundingAmount(productItem, fundingAmount) {
		var result = null;
		//result = MAX(rateItem.MinBalance)>fundingAmount
		var poolNames = [];
		if (locationPoolCode) {
			poolNames = getSelectedZipPoolNames(locationPoolCode, ZIPCODEPOOLLIST, ZIPCODEPOOLNAME);
		}
		_.forEach(productItem.ProductRates, function (rateItem) {
			if (locationPoolCode != null && locationPoolCode != "" && (poolNames.length == 0 || (rateItem.ZipCodeFilterType == "INCLUDE" && _.indexOf(poolNames, rateItem.LocationPool) == -1) || (rateItem.ZipCodeFilterType == "EXCLUDE" && _.indexOf(poolNames, rateItem.LocationPool) > -1))) {
				return;
			}
			if (result == null) {
				if (rateItem.MinBalance <= fundingAmount) result = rateItem;
			} else {
				if (rateItem.MinBalance <= fundingAmount && result.MinBalance < rateItem.MinBalance) result = rateItem;
			}
		});
		return result;
	}
	function updateHiddenRateMaturityDate() {
		var month = padLeft($("#txtRateMaturityDate1", "#fundingDialog").val(), 2);
		var day = padLeft($("#txtRateMaturityDate2", "#fundingDialog").val(), 2);
		var year = padLeft($("#txtRateMaturityDate3", "#fundingDialog").val(), 4);
		if (month != "" && day != "" && year != "") {
			$("#txtRateMaturityDate", "#fundingDialog").val(month + '/' + day + '/' + year);
		} else {
			$("#txtRateMaturityDate", "#fundingDialog").val("");
		}
	}
	function registerFundingDialogValidator(popContainer) {
		$("#txtRateDepositAmount", popContainer).observer({
			validators: [
				function (partial) {
					var $self = $(this);
					var productCode = $("#fundingDialog").find('input:hidden[data-place-holder="productCode"]').val();
					var productItem = getProductItemByCode(productCode);
					var value = Common.GetFloatFromMoney($self.val());
					var minDeposit, maxDeposit;
					if ($self.data("is-auto-calculated-tier") == true) {
						minDeposit = findLowestMinDepositRate(productItem).MinDeposit;
						maxDeposit = findHighestMaxDepositRate(productItem).MaxDeposit;
						if (minDeposit >= 0 && value < minDeposit) {
							return "The minimum deposit amount for " + productItem.ProductName + " is " + Common.FormatCurrency(minDeposit, true);
						}
						if (maxDeposit > 0 && value > maxDeposit) {
							return "The maximum deposit amount for " + productItem.ProductName + " is " + Common.FormatCurrency(maxDeposit, true);
						}
					} else {
						minDeposit = parseFloat($self.data("min-deposit"));
						maxDeposit = parseFloat($self.data("max-deposit"));
						if (minDeposit >= 0 && value < minDeposit) {
							return "The minimum deposit amount for the selected rate is " + Common.FormatCurrency(minDeposit, true);
						}
						if (maxDeposit > 0 && value > maxDeposit) {
							return "The maximum deposit amount for the selected rate is " + Common.FormatCurrency(maxDeposit, true);
						}
					}
					return "";
				}
			],
			validateOnBlur: true,
			group: "ValidateFundingDialog",
			groupType: "complexUI",
			placeHolder: $("#txtRateDepositAmount", popContainer).closest("fieldset")
		});
		$('#divRateMaturityDate', popContainer).observer({
			validators: [
				function (partial) {
					updateHiddenRateMaturityDate();
					if ($(this).is(":hidden")) return "";
					$.lpqValidate.hideValidation($("#txtRateTermLength", popContainer).closest("fieldset"));
					var value = $('#txtRateMaturityDate').val();
					if (value.trim() == "") {
						return "Maturity Date is required";
					}
					if (!Common.ValidateDate(value) || !Common.IsValidDate(value)) {
						return "Valid Maturity Date is required";
					}
					var $txtRateTermLength = $('#txtRateTermLength', "#fundingDialog");
					var minTerm = parseFloat($txtRateTermLength.data("min-term"));
					var maxTerm = parseFloat($txtRateTermLength.data("max-term"));
					var maturityDate = moment(value, "MM-DD-YYYY");
					var diffCount = maturityDate.diff(moment().startOf("day"), "days");
					if (minTerm >= 0 && diffCount < minTerm) {
						return "The Maturity Date must be after " + moment().add(minTerm - 1, "d").format("MM/DD/YYYY");
					}
					if (maxTerm > 0 && diffCount > maxTerm) {
						return "The Maturity Date must be before or equal " + moment().add(maxTerm + 1, "d").format("MM/DD/YYYY");
					}
					$txtRateTermLength.val(diffCount);
					return "";
				}
			],
			validateOnBlur: true,
			group: "ValidateFundingDialog",
			groupType: "complexUI",
			placeHolder: $("#divRateMaturityDateStuff", popContainer)
		});
		$('#txtRateTermLength', popContainer).observer({
			validators: [
				function (partial) {
					var $self = $(this);
					if ($self.is(":hidden")) return "";
					var minTerm = parseFloat($self.data("min-term"));
					var maxTerm = parseFloat($self.data("max-term"));
					var value = parseFloat($self.val());
					$.lpqValidate.hideValidation($("#divRateMaturityDateStuff", popContainer));
					if (isNaN(value)) return "Term is required";
					if (minTerm >= 0 && value < minTerm) {
						return "The term for the selected rate should be greater than or equal " + minTerm;
					}
					if (maxTerm > 0 && value > maxTerm) {
						return "The term for the selected rate should be less than or equal " + maxTerm;
					}
					return "";
				}
			],
			validateOnBlur: true,
			group: "ValidateFundingDialog",
			groupType: "complexUI",
			placeHolder: $("#txtRateTermLength", popContainer).closest("fieldset")
		});
	}
	function buildSelectedProductItem(productItem) {
		var $item = $("<div/>", { "class": "pool-item-option" }).append(
			$("<strong/>").text(productItem.ProductName + (productItem.ProductIsRequired == "Y" ? " (required)" : ""))
		);
		var $wrapper = $("<div/>", { "class": "pool-item-option-wrapper selected-prod", "data-zipcode-filter-type": productItem.ZipCodeFilterType, "data-zipcode-pool-id": productItem.ZipCodePoolId, "data-product-code": productItem.ProductCode, "id": "btn_" + productItem.ProductCode + Math.random().toString(36).substr(2, 16), "data-original-account-type": productItem.OriginalAccountType }).append($item);
		var $desc = $("<p/>", { "class": "desc" });
		var hasDesc = false;
		var productApy = parseFloat(productItem.ProductAPY);
		if (!isNaN(productApy) && productApy > 0) {
			$("<span/>").text("APY: " + productApy.toFixed(APYPRECISION)).appendTo($desc);
			hasDesc = true;
		}

		var selectedServiceList = [];
		if (productItem.ProductServices.length > 0) {
			if ($("#productDetail .service-pool").length > 0) {
				for (var idx = 0; idx < productItem.ProductServices.length; idx++) {
					var divPoolEle = "#serv_";
					if ($(divPoolEle + productItem.ProductCode + "_" + productItem.ProductServices[idx].ServiceCode, $("#productDetail .service-pool")).hasClass("ui-checkbox-on")) {
						selectedServiceList.push(productItem.ProductServices[idx].Description);
					}
				}
			} else {
				for (var idx = 0; idx < productItem.ProductServices.length; idx++) {
					if (productItem.ProductServices[idx].IsActive && (productItem.ProductServices[idx].IsRequired == true || productItem.ProductServices[idx].IsPreSelection == true)) {
						selectedServiceList.push(productItem.ProductServices[idx].Description);
					}
				}
			}

		};
		if (selectedServiceList.length > 0) {
			$("<div/>", {style: "margin-top: 4px;"}).html("Selected Features: " + selectedServiceList.join(", ")).appendTo($desc);
			hasDesc = true;
		}
		
		if (hasDesc) {
			$item.append($desc);
		}

		var hasVisibleProductQuestions = false;
		if (productItem.CustomQuestions.length > 0) {
			hasVisibleProductQuestions = $("#divProductQuestionPool").find("div[data-eid='divProductQuestion_chk" + productItem.ProductCode + "']").find("div.pq-wrapper:not(.hidden)").length > 0;
		}
		if (hasVisibleProductQuestions == true || productItem.ProductServices.length > 0) {
			var editBtn = $("<div/>", { "class": "right-btn btn-edit", "data-command": "edit", "tabindex": "0" });
			$("<span/>").appendTo(editBtn);
			$wrapper.append(editBtn);
		} else if (productItem.ProductRates.length > 0) {
			var infoBtn = $("<div/>", { "class": "right-btn btn-pinfo", "data-command": "info", "tabindex": "0" });
			$("<span/>").appendTo(infoBtn);
			$wrapper.append(infoBtn);
		}
		var $deleteBtn = $("<div/>", { "class": "right-btn btn-remove", "data-command": "remove", "tabindex": "0" });
		$("<span/>").appendTo($deleteBtn);
		$wrapper.append($deleteBtn);
		return $wrapper;
	}
	function getProductItemByCode(pCode) {
		for (var i = 0; i < PRODUCTLIST.length; i++) {
			if (PRODUCTLIST[i].ProductCode == pCode) {
				return _.cloneDeep(PRODUCTLIST[i]);
			}
		}
		return null;
	}
	xaProductFactory.getProductItemByCode = function (pCode) {
		return getProductItemByCode(pCode);
	}
	xaProductFactory.showFundingDialog = function(srcEle, event) {
		var $self = $(srcEle);
		var productItem = getProductItemByCode($self.next("input").data("field"));
		if (productItem == null) return false;
		buildFundingPopup(productItem, $("#fundingDialog"), $self.next("input").data("instance-id"));
		goToNextPage("#fundingDialog");
	}
	function addProductToSelectionPool(productItem) {
		var $item = buildSelectedProductItem(productItem);
		$("#divSelectedProductPool").append($item);
		bindEventSelectedProduct($item);
		$("#divSelectedProductPool").parent().removeClass("hidden");
		Common.noty("success", "<b>" + productItem.ProductName+ "</b> has been added to Your Selected Products", 5000);
		return $item;
	}
	function updateProductInSelectionPool(productItem, instanceId) {
		var $item = buildSelectedProductItem(productItem);
		$("#" + instanceId).replaceWith($item);
		bindEventSelectedProduct($item);
		return $item;
	}

	function buildDepositTextbox(instanceId, productItem) {
		if (productItem.NoFund == true) return null;
		if (productItem.ProductRates && productItem.ProductRates.length > 0) {
			return $("<div/>", { id: "divDepositAmount_chk" + productItem.ProductCode + instanceId }).append(
				$("<div/>", { "data-role": "fieldcontain" }).append(
					$("<label/>", { "class": "RequiredIcon" }).text(productItem.ProductName),
					$("<a/>", { "href": "#", "data-role": "button", "class": "btn-header-theme hover-effect", "tabindex": "0", "onclick": "xaProductFactory.showFundingDialog(this, event)", "onkeypress": "xaProductFactory.showFundingDialog(this, event)" }).text("$0.00 (0.00% APY)"),
					$("<input/>", { "id": "txtDepositAmount_chk" + productItem.ProductCode + instanceId, "data-field": productItem.ProductCode, "data-instance-id": instanceId, "type": "hidden" })
				)
			);
		} else {
			var minDepositAmt = productItem.ProductMinDeposit;
			var maxDepositAmt = productItem.ProductMaxDeposit;
			minDepositAmt = Math.max(0, Common.GetFloatFromMoney(minDepositAmt));
			maxDepositAmt = Common.GetFloatFromMoney(maxDepositAmt);
			var $depositAmountBlock = null;
			if (minDepositAmt > 0 || maxDepositAmt > 0) {
				var $minDepositBlock = null;
				var $maxDepositBlock = null;
				var $requireDepositBlock = null;
				$depositAmountBlock = $("<span/>", { "data-name": "depositamount" });
				$depositAmountBlock.append(document.createTextNode(" ("));
				var sep = "";
				if (minDepositAmt > 0) {
					$minDepositBlock = $("<span/>", { "data-name": "mindeposit" }).text(Common.FormatCurrency(minDepositAmt, true) + " minimum");
					sep = ", ";
					$requireDepositBlock = $("<span/>", { style: "display: inline", "class": "require-span" }).text("*");
				}
				if (maxDepositAmt > 0) {
					$maxDepositBlock = $("<span/>", { "data-name": "maxdeposit" }).text(sep + Common.FormatCurrency(maxDepositAmt, true) + " maximum");
				}
				$depositAmountBlock.append($minDepositBlock, $maxDepositBlock);
				$depositAmountBlock.append(document.createTextNode(")"));
				$depositAmountBlock.append($requireDepositBlock);
			}

			return $("<div/>", { id: "divDepositAmount_chk" + productItem.ProductCode + instanceId }).append(
				$("<div/>", { "data-role": "fieldcontain" }).append(
					$("<label/>")
					.append(
						document.createTextNode(productItem.ProductName),
						$depositAmountBlock,
						$("<input/>", {
							"type": "tel", "aria-labelledby": "divDepositAmount_chk" + productItem.ProductCode + instanceId, "id": "txtDepositAmount_chk" + productItem.ProductCode + instanceId, "class": "money", "data-field": productItem.ProductCode, "data-instance-id": instanceId,
						}).val(Common.FormatCurrency(minDepositAmt, true))
						)
				)
			);
		}
	}

	function setSelectedServices(container) {
		var productItem = selectedServices[$(container).attr("id")];
		if (productItem && productItem.ProductServices) {
			for (var i = 0; i < productItem.ProductServices.length; i++) {
				var curService = productItem.ProductServices[i];
				$(".service-pool #chk_serv_" + productItem.ProductCode + "_" + curService.ServiceCode, $('#productDetail')).prop("checked", curService.IsChecked == true);
			}
		}
	}

	function getSelectedServices(productItem, container) {
		var result = _.cloneDeep(productItem);
		if (result && result.ProductServices && result.ProductServices.length > 0) {
			for (var i = 0; i < result.ProductServices.length; i++) {
				var curService = result.ProductServices[i];
				curService.IsChecked = $(".service-pool #chk_serv_" + productItem.ProductCode + "_" + curService.ServiceCode, $('#productDetail')).is(":checked");
			}
		}
		return result;
	}

	function setCustomQuestionAnswers(container) {
		var productItem = customQuestionAnswers[$(container).attr("id")];
		if (productItem && productItem.CustomQuestions) {
			for (var i = 0; i < productItem.CustomQuestions.length; i++) {
				var curQuestion = productItem.CustomQuestions[i];
				var $pcq = $(".custom-question-pool div[qname='" + curQuestion.QuestionName + "']", $('#productDetail'));
				if ($pcq.hasClass('hidden')) {
					continue;
				}
				var qIndex = i + 1;
				var questionId = "question_" + qIndex + "_chk" + productItem.ProductCode;
				switch (curQuestion.AnswerType) {
					case "HEADER":
						continue;
					case "TEXTBOX":
					case "DATE":
						$('#productDetail .custom-question-pool [data-eid="' + questionId + '"]').val(curQuestion.productAnswerText);
						break;
					case "PASSWORD":
						$('#productDetail .custom-question-pool [data-eid="' + questionId + '"]').val(curQuestion.productAnswerText);
						$('#productDetail .custom-question-pool [data-eid="re_' + questionId + '"]').val(curQuestion.productAnswerText);
						break;
					case "DROPDOWN":
						for (var j = 0; j < curQuestion.RestrictedAnswers.length; j++) {
							var ra = curQuestion.RestrictedAnswers[j];
							if (ra.isSelected == true) {
								var $questionEle = $('#productDetail .custom-question-pool [data-eid="' + questionId + '"]');
								if ($questionEle.hasClass("yesno-group")) {
									$questionEle.find(">div").each(function () {
										$(this).removeClass("active");
									});
									$questionEle.find("div[data-value='" + ra.Value.toLowerCase() + "']").addClass("active");
									$questionEle.data("value", ra.Value.toLowerCase());

								} else {
									$questionEle.val(ra.Value);
								}
							}
						}
						break;
					case "CHECKBOX":
						for (var j = 0; j < curQuestion.RestrictedAnswers.length; j++) {
							var ra = curQuestion.RestrictedAnswers[j];
							$('#productDetail .custom-question-pool [data-eid="' + questionId + "_" + j + '"]').prop('checked', ra.isSelected);
						}
						break;
				}
				performPQAdvancedLogics();
			}
		}
	}
	function getCustomQuestionAnswers(productItem, container) {
		var result = _.cloneDeep(productItem);
		if (result && result.CustomQuestions && result.CustomQuestions.length > 0) {
			for (var i = 0; i < result.CustomQuestions.length; i++) {
				var curQuestion = result.CustomQuestions[i];
				curQuestion.Hidden = false;
				var $pcq = $(".custom-question-pool div[qname='" + curQuestion.QuestionName + "']", $('#productDetail'));
				if ($pcq.hasClass('hidden')) {
					curQuestion.Hidden = true;
					continue;
				}
				var qIndex = i + 1;
				var questionId = "question_" + qIndex + "_chk" + result.ProductCode;
				switch (curQuestion.AnswerType) {
					case "HEADER":
						continue;
					case "PASSWORD":
					case "TEXTBOX":
					case "DATE":
						curQuestion.productAnswerText = $('#productDetail .custom-question-pool [data-eid="' + questionId + '"]').val();
						break;
					case "DROPDOWN":
						var raVal = "";
						if ($('#productDetail .custom-question-pool [data-eid="' + questionId + '"]').hasClass("yesno-group")) {
							raVal = $('#productDetail .custom-question-pool [data-eid="' + questionId + '"]').data("value");
						} else {
							raVal = $('#productDetail .custom-question-pool [data-eid="' + questionId + '"]').val();
						}
						for (var j = 0; j < curQuestion.RestrictedAnswers.length; j++) {
							var ra = curQuestion.RestrictedAnswers[j];
							ra.isSelected = false;
							if (raVal.toUpperCase() == ra.Value.toUpperCase()) {
								ra.isSelected = true;
							}
						}
						break;
					case "CHECKBOX":
						for (var j = 0; j < curQuestion.RestrictedAnswers.length; j++) {
							curQuestion.RestrictedAnswers[j].isSelected = $('#productDetail .custom-question-pool [data-eid="' + questionId + "_" + j + '"]').is(':checked');
						}
						break;
				}
			}
		}
		return result;
	}
	function validateCustomQuestion(productItem) {
		var messageArr = [];
		if (productItem.CustomQuestions.length > 0) {
			for (var i = 0; i < productItem.CustomQuestions.length; i++) {
				var curQuestion = productItem.CustomQuestions[i];
				var $pcq = $(".custom-question-pool div[qname='" + curQuestion.QuestionName + "']", $('#productDetail'));
				if ($pcq.hasClass('hidden')) {
					continue;
				}
				//if ($("div[qname='" + curQuestion.QuestionName + "']").hasClass("hidden")) {
				//       continue;
				//	}

				var qIndex = i + 1;
				var questionId = "question_" + qIndex + "_chk" + productItem.ProductCode;
				if (curQuestion.IsRequired == true) {
					if (curQuestion.AnswerType != "CHECKBOX" && curQuestion.AnswerType != "DROPDOWN" && curQuestion.AnswerType != "HEADER") {
						validateCustomQuestionTextBoxField(messageArr, curQuestion, questionId);
					} else if (curQuestion.AnswerType == "CHECKBOX") {
						var hasAnswer = false;
						for (var j = 0; j < curQuestion.RestrictedAnswers.length; j++) {
							if ($("#productDetail .custom-question-pool [data-eid='question_" + (i + 1) + "_chk" + productItem.ProductCode + "_" + j + "']").is(":checked")) {
								hasAnswer = true;
								break;
							}
						}
						if (hasAnswer == false) {
							messageArr.push(curQuestion.QuestionText);
							//strMessage += 'Please answer the question "' + curQuestion.QuestionText + '"<br/>';
						}
					}
					else if (curQuestion.AnswerType == "DROPDOWN") {
						var answerCtrl = $("#productDetail .custom-question-pool [data-eid='question_" + (i + 1) + "_chk" + productItem.ProductCode + "']");
						var answer = "";
						if (answerCtrl.hasClass("yesno-group")) {
							answer = answerCtrl.data("value");
						} else {
							answer = answerCtrl.val();
						}
						if ($.trim(answer) === "") {
							messageArr.push(curQuestion.QuestionText);
							//strMessage += "Please answer the question '" +  curQuestion.QuestionText + "'<br/>";
						}
					}
				} else if (curQuestion.AnswerType == "PASSWORD" || curQuestion.AnswerType == "TEXTBOX" || curQuestion.AnswerType == "DATE") { //not required product: validate non empty textbox field if the field contains regular expression
					validateCustomQuestionTextBoxField(messageArr, curQuestion, questionId);
				}
			}
		}
		return messageArr;
	}
	function validateCustomQuestionTextBoxField(messageArr, curQuestion, questionId) {
		var answerCtrl = $("#productDetail .custom-question-pool [data-eid='" + questionId + "']");
		if ($.trim(answerCtrl.val()) === "") {
			if (curQuestion.IsRequired == true) { //skip validate if it is not required 
				messageArr.push(curQuestion.QuestionText);
			}
		} else {//validate regular expression if it exist
			var errorMessage = curQuestion.errorMessage;
			var regExpression = curQuestion.reg_expression;
			if (regExpression != undefined && regExpression != "") {
				var reg = new RegExp(JSON.parse(regExpression));
				var ansValue = answerCtrl.val();
				var ansExpr = ansValue.match(reg);
				if (errorMessage == undefined || errorMessage == "") {
					errorMessage = curQuestion.QuestionText + "<br/>-- " + ansValue + "<br/>";
				}
				if (!ansExpr) {
					messageArr.push(errorMessage);
				} else {
					var isMatched = false;
					for (var i = 0; i < ansExpr.length; i++) {
						if (ansExpr[i] == ansValue) {
							isMatched = true;
							break;
						}
					}
					if (!isMatched) {
						messageArr.push(errorMessage);
					}
				}
            } else {//validate min length                                 
                if (curQuestion.min_value != undefined && curQuestion.min_value != "" && parseInt(curQuestion.min_value) > answerCtrl.val().length) {                            
                    messageArr.push(curQuestion.QuestionText + "<br/>-- Answer must be at least " + curQuestion.min_value +" characters.<br/>");           
                }
			}
		} //end validate regular expression
		//validate re-entered password field
		if (curQuestion.AnswerType == "PASSWORD" && curQuestion.RequireConfirmation == true) {
			var $rePassword = $("#productDetail .custom-question-pool [data-eid='re_" + questionId + "']");
			if ($rePassword.val() != answerCtrl.val()) {
				messageArr.push(curQuestion.ConfirmationText);
			}
		}

	}
	function buildErrorMessageHtml(messageArr) {
		var strHtml = "<p>Please complete the below feature(s)</p>";
		strHtml += "<ul>";
		for (var i = 0; i < messageArr.length; i++) {
			strHtml += "<li>" + messageArr[i] + "</li>";
		}
		strHtml += "</ul>";
		return strHtml;
	}
	function bindProductQuestionEvent(container) {
		$(".pq-wrapper div.yesno-group>div", $(container)).off("click").on("click", function () {
			var $self = $(this);
			$self.closest("div.yesno-group").find(">div").each(function () {
				$(this).removeClass("active");
			});
			$self.addClass("active");
			$self.closest("div.yesno-group").data("value", $self.data("value"));
			performPQAdvancedLogics();
		});
		$(".pq-wrapper [iname]", $(container)).each(function (idx, ele) {
			var $ele = $(ele);
			if ($ele.is(":text")) {
				$ele.on("blur", function () {
					performPQAdvancedLogics();
				});
			} else {
				$ele.on("change", function () {
					performPQAdvancedLogics();
				});
			}
		});

	}
	function bindEventPopupDetail() {
		$("#productDetail").on("pagebeforeshow", function (event, ui) {
			//remove checkbox and remove "visibility:hidden" if it exist in product descriptions(case wscu_test)
			var divProductDetail = $(this).find('p');
			divProductDetail.each(function () {
				var pDescElem = $(this);
				pDescElem.find('img').css('visibility', '');
				pDescElem.find('img').prev('br').remove();
				var chkElem = pDescElem.find('input[type="checkbox"]');
				if (chkElem.length > 0) {
					chkElem.remove();
					pDescElem.find('div[class="ui-checkbox"]').remove();
				}
			});
			//end remove checkbox ....
			//actionStack = []; //reset stack;
			//prevent scrollbar by popup overlay
			$("#" + this.id + "-screen").height("");
			$("#productDetail div[data-command='close']").off("click").on("click", function () {
				goToNextPage("#" + $(this).data("main-page"));
			});
			$("#productDetail #btnAddProduct").off("click").on("click", function () {
				var $self = $(this);
				var pCode = $("#productDetail").find('input:hidden[data-place-holder="productCode"]').val();
				var productItem = getProductItemByCode(pCode);
				var messageArr = validateCustomQuestion(productItem);
				
                if (messageArr.length == 0) {
                    var $item = addProductToSelectionPool(productItem);
                    customQuestionAnswers[$item.attr("id")] = getCustomQuestionAnswers(productItem, $item);
                    selectedServices[$item.attr("id")] = getSelectedServices(productItem, $item);
                    var $txt = buildDepositTextbox($item.attr("id"), productItem);
                    if ($txt != null) { 
                        $("#divDepositProductPool").append($txt);
                        $txt.enhanceWithin();
                    }
					goToNextPage("#" + $self.data("main-page"), function () {
						setTimeout(function () {
							//$item.find(".right-btn[data-command='edit']").focus();
							$("#divRequiredProducts").trigger("change");
							$("#divAdditionalProducts").trigger("change");
						}, 1000);
					});
				} else {
					var errMsg = buildErrorMessageHtml(messageArr);
					if ($("#divErrorPopup").length > 0) {
						$('#txtErrorPopupMessage').html(errMsg);
						$("#divErrorPopup").popup("open", { "positionTo": "window" });
					} else {
						$('#txtErrorMessage').html(errMsg);
						goToNextPage("#divErrorDialog");
					}
				}
			});
			$("#productDetail #btnCloseProductDetail").off("click").on("click", function () {
				var $self = $(this);
				goToNextPage("#" + $self.data("main-page"), function () {
					//setTimeout(function () {
					//	$(".right-btn[data-command='info']", "#" + $self.data("loaded-id")).focus();
					//}, 1000);
				});
			});
			$("#productDetail #btnSaveProduct").off("click").on("click", function () {
				var $self = $(this);
				var pCode = $("#productDetail").find('input:hidden[data-place-holder="productCode"]').val();
				var productItem = getProductItemByCode(pCode);
				var messageArr = validateCustomQuestion(productItem);
				
				if (messageArr.length == 0) {
					var $item = updateProductInSelectionPool(productItem, $self.data("loaded-id"));
					delete customQuestionAnswers[$self.data("loaded-id")];
					customQuestionAnswers[$item.attr("id")] = getCustomQuestionAnswers(productItem, $item);
					delete selectedServices[$self.data("loaded-id")];
					selectedServices[$item.attr("id")] = getSelectedServices(productItem, $item);
					var $div = buildDepositTextbox($item.attr("id"), productItem);
					$("#divDepositAmount_chk" + productItem.ProductCode + $self.data("loaded-id"), "#divDepositProductPool").replaceWith($div);
					$div.enhanceWithin();
					goToNextPage("#" + $self.data("main-page"), function () {
						setTimeout(function () {
							//$item.find(".right-btn[data-command='edit']").focus();
							$("#divRequiredProducts").trigger("change");
						}, 1000);
					});
				} else {
					var errMsg = buildErrorMessageHtml(messageArr);
					if ($("#divErrorPopup").length > 0) {
						$('#txtErrorPopupMessage').html(errMsg);
						$("#divErrorPopup").popup("open", { "positionTo": "window" });
					} else {
						$('#txtErrorMessage').html(errMsg);
						goToNextPage("#divErrorDialog");
					}
				}
			});
			actionStack.push("openDetail");
		});
		$("#productDetail").on("pagehide", function (event, ui) {
			if (actionStack.length > 0) {
				var reopenPopupPattern = /openDetail$/i;
				//var autoScrollPattern = /showDoc$/i;
				if (reopenPopupPattern.test(actionStack.join("")) && $.mobile.pageContainer.pagecontainer('getActivePage').attr('id') == "popupPDFViewer") {
					return;
				}
			}
			$("#GettingStarted").css("height", "");
			if ($("#divRequiredProducts").length > 0) {
				$('html, body').stop().animate({ scrollTop: $("#divRequiredProducts").offset().top - 100 }, { duration: 0, easing: 'swing' });
			} else if ($("#availableProductLabel").length > 0) {
				$('html, body').stop().animate({ scrollTop: $("#availableProductLabel").offset().top - 100 }, { duration: 0, easing: 'swing' });
			}

			var container = $(this);
			var $questionPlaceHolder = container.find("fieldset[data-place-holder='questions'] > div.ui-controlgroup-controls");
			$questionPlaceHolder.find(".custom-question-pool").remove();
			var $servicePlaceHolder = container.find("fieldset[data-place-holder='services'] > div.ui-controlgroup-controls");
			$servicePlaceHolder.find(".service-pool").remove();
			//$servicePlaceHolder.removeAttr("product-code");

			var $ratePlaceHolder = container.find("fieldset[data-place-holder='rates']");
			$ratePlaceHolder.find(".rate-pool").remove();
		});

	}
	function bindEventFundingDialog() {
		$("#fundingDialog").on("pagebeforeshow", function (event, ui) {
			var $dialog = $(this);
			$("#" + this.id + "-screen").height("");
			$("#" + this.id + " div[data-command='close']").off("click").on("click", function () {
				goToNextPage("#" + $(this).data("main-page"));
			});
			$("#" + this.id + " #btnAcceptRate").off("click").on("click", function () {
				if ($.lpqValidate("ValidateFundingDialog")) {
					var $self = $(this);
					var pCode = $dialog.find('input:hidden[data-place-holder="productCode"]').val();
					var instanceId = $dialog.find('input:hidden[data-place-holder="instanceId"]').val();
					var productItem = getProductItemByCode(pCode);
					var rateCode = $(".horizontal-table input:radio:checked", $dialog).attr("rate_code");
					var depositAmount = $("#txtRateDepositAmount", $dialog).val();
					var term = "";
					if ($("#txtRateTermLength", $dialog).is(":visible")) {
						term = $("#txtRateTermLength", $dialog).val();
					}
					var $txtDepositAmount = $("#txtDepositAmount_chk" + productItem.ProductCode + instanceId);
					$txtDepositAmount.val($("#txtRateDepositAmount", $dialog).val());
					var selectedRate = _.find(productItem.ProductRates, { RateCode: rateCode });
					$txtDepositAmount.prev("a").text(depositAmount + (selectedRate.Apy > 0 ? " (" + selectedRate.Apy.toFixed(2) + "% APY)" : ""));
					//var apy = (Math.pow((1 + ((selectedRate/100) / 30)), 30) - 1) * 100
					selectedRates[instanceId] = { Apy: selectedRate.Apy, ID: selectedRate.ID, RateCode: selectedRate.RateCode, Term: term, DepositAmount: depositAmount };
					goToNextPage("#" + $self.data("main-page"), function () {
						setTimeout(function () {
							$txtDepositAmount.prev("a").focus();
							$txtDepositAmount.trigger("blur");
						}, 500);
					});
				}
			});
			
		});
		$("#fundingDialog").on("pagehide", function (event, ui) {
			$("#pageFS").css("height", "");
			var container = $(this);
			var $ratePlaceHolder = container.find("fieldset[data-place-holder='rates']");
			$ratePlaceHolder.find(".rate-pool").remove();
		});

	}
	xaProductFactory.lenderHasRequiredProducts = function () {
		//check required products with/without zip pool
		return $('#divRequiredProducts div.pool-item-option-wrapper:not(.hidden)').length > 0;
	}
	xaProductFactory.validateProductSelection = function () {
		//minor 
		if (getMinorAccountTypeObj().isMinor == "MINOR" && $('select#ddlSpecialAccountType option:selected').val() == "") {
			return "";  // there are no product to select--skip to validate
		}

		if ($("#divSelectedProductPool>.pool-item-option-wrapper").length == 0) {
			if (xaProductFactory.lenderHasRequiredProducts()) {
				return "Please select a required product";
			}
			return "Please select at least 1 product";
		}
		if (xaProductFactory.lenderHasRequiredProducts() === false) {
			return "";
		}
		var requiredProductSelected = false;
		$("#divSelectedProductPool").find("div.pool-item-option-wrapper[data-product-code]").each(function () {
			var $self = $(this);
			var pItem = getProductItemByCode($self.data("product-code"));
			if (pItem != null && pItem.ProductIsRequired === "Y") {
				requiredProductSelected = true;
				return;
			}
		});

		if (requiredProductSelected == false) {
			return "Please select a required product";
		}
		return "";
	}
	xaProductFactory.getTotalDeposit = function () {
		var totalDeposit = 0.0;
		$("#divSelectedProductPool div.pool-item-option-wrapper[data-product-code]").each(function () {
			var pCode = $(this).attr('data-product-code');
			var productItem = getProductItemByCode(pCode);
			totalDeposit += Common.GetFloatFromMoney($("#txtDepositAmount_chk" + productItem.ProductCode + $(this).attr('id')).val());
		});
		return totalDeposit;
	}
	xaProductFactory.setESignature = function (appInfo) {
		var secondRoleType = $('#hdSecondRoleType').val().toUpperCase();
		var strESignature = "";
		if ($('#hfLenderRef').val().toUpperCase().startsWith('SDFCU') && secondRoleType == "CUSTODIAN") {
			strESignature += $('#divMinorName').children('label').text().trim();
			strESignature += "eSignature: checked.  Date: " + $('#spDate').text();
		}
		appInfo.eSignature = strESignature;
	}

	xaProductFactory.collectProductSelectionData = function () {
		var sProductInfoArray = new Array();
		//list of selected products
		$("#divSelectedProductPool div.pool-item-option-wrapper[data-product-code]").each(function () {
			var curInstanceId = $(this).attr("id");
			var productItem = getProductItemByCode($(this).attr('data-product-code'));
			var txtDepositAmount = Common.GetFloatFromMoney($('#txtDepositAmount_chk' + productItem.ProductCode + curInstanceId).val());
			var sProductQuestions = [];
			//Question_text may have special characters(hyperlink) which is consider dangerous for form postback
			//REmove it from postback since the app don't need it
			if (customQuestionAnswers[curInstanceId]) {
				for (var i = 0; i < customQuestionAnswers[curInstanceId].CustomQuestions.length; i++) {
					if (customQuestionAnswers[curInstanceId].CustomQuestions[i].Hidden != true) {
						var q = _.cloneDeep(customQuestionAnswers[curInstanceId].CustomQuestions[i]);
						q.QuestionText = "";
						sProductQuestions.push(q);
					}
				}
			}
			var sProductServices = [];
			if (selectedServices[curInstanceId]) {
				for (var i = 0; i < selectedServices[curInstanceId].ProductServices.length; i++) {
					if (selectedServices[curInstanceId].ProductServices[i].IsChecked == true) {
						sProductServices.push(selectedServices[curInstanceId].ProductServices[i].ServiceCode);
					}
				}
			};
			var selectedRate = selectedRates[curInstanceId];
			var productRate = "", term = 0, termType = "", apy = 0;
			if (selectedRate) {
				productRate = selectedRate.RateCode;
				term = selectedRate.Term;
				apy = selectedRate.Apy;
			}
			sProductInfoArray.push(new ProductSelectionItem(productItem.ProductCode, txtDepositAmount, sProductQuestions, sProductServices, productRate, term, apy));
		});

		return sProductInfoArray;
	}
	xaProductFactory.viewProductCustomQuestion = function () {
		var strHtml = "";
		for (var ca in customQuestionAnswers) {
			if (customQuestionAnswers.hasOwnProperty(ca)) {
				var productItem = customQuestionAnswers[ca];
				if (productItem && productItem.CustomQuestions && productItem.CustomQuestions.length > 0) {
					strHtml += "<div><div class='row'><div class='col-xs-12 bold'>" + htmlEncode(productItem.ProductName) + "</div></div>";
					for (var i = 0; i < productItem.CustomQuestions.length; i++) {
						var curQuestion = productItem.CustomQuestions[i];
						if (curQuestion.Hidden == true) continue;
						strHtml += "<div class='q-block'>";
						strHtml += "<div class='row'><div class='col-xs-12'>" + curQuestion.QuestionText + "</div></div>";
						switch (curQuestion.AnswerType) {
							case "HEADER":
								continue;
							case "TEXTBOX":
							case "DATE":
								strHtml += "<div class='row'><div class='col-xs-12'><p>--" + htmlEncode(curQuestion.productAnswerText) + "</p></div></div>";
								break;
							case "PASSWORD":
								strHtml += "<div class='row'><div class='col-xs-12'><p>--***</p></div></div>";
								break;
							case "DROPDOWN":
								var hasAnswer = false;
								for (var j = 0; j < curQuestion.RestrictedAnswers.length; j++) {
									var ra = curQuestion.RestrictedAnswers[j];
									if (ra.isSelected == true) {
										hasAnswer = true;
										strHtml += "<div class='row'><div class='col-xs-12'><p>--" + htmlEncode(ra.Text) + "</p></div></div>";
									}
								}
								if (!hasAnswer) {
									strHtml += "<div class='row'><div class='col-xs-12'><p>--</p></div></div>";
								}
								break;
							case "CHECKBOX":
								var answers = [];
								for (var j = 0; j < curQuestion.RestrictedAnswers.length; j++) {
									var ra = curQuestion.RestrictedAnswers[j];
									if (ra.isSelected == true) {
										answers.push(ra.Text);
									}
								}
								strHtml += "<div class='row'><div class='col-xs-12'><p>--" + answers.join(", ") + "</p></div></div>";
								break;
						}
						strHtml += "</div>";
					}
					strHtml += "</div>";
				}
			}
		}
		if (strHtml.length > 0) {
			$(".ViewProductCustomQuestion").prev().show();
			$(".ViewProductCustomQuestion").show();
		} else {
			$(".ViewProductCustomQuestion").prev().hide();
			$(".ViewProductCustomQuestion").hide();
		}
		return strHtml;
	}
	xaProductFactory.viewProductSelection = function () {
		var strHtml = "";
		var strStack1 = []; //product has services
		var strStack2 = []; //product only
		$("#divSelectedProductPool div.pool-item-option-wrapper[data-product-code]").each(function (i, ele) {
			var strTemp = '';
			var $ele = $(ele);
			var pCode = $ele.data('product-code');
			var instanceId = $ele.attr("id");
			var productItem = getProductItemByCode(pCode);
			strTemp += '<div class="row"><div class="pname"><span>' + productItem.ProductName + '</span></div>';
			// Render Services
			var hasSelectedServices = false;
			if (selectedServices[instanceId] && selectedServices[instanceId].ProductServices.length > 0) {
				var productServices = selectedServices[instanceId].ProductServices;
				for (var idx = 0; idx < productServices.length; idx++) {
					if (productServices[idx].IsChecked == true) {
						hasSelectedServices = true;
						strTemp += '<div><i class="fa fa-check"></i><span style="margin-left:6px;">' + htmlEncode(productServices[idx].Description) + '</span></div>';
					}
				}
			}
			strTemp += '</div>';
			if (hasSelectedServices) {
				strStack1.push(strTemp);
			} else {
				strStack2.push(strTemp);
			}
		});
		if (strStack1.length === 0 && strStack2.length === 0) {
			strHtml = "";
		} else if (strStack1.length > 0 && strStack2.length > 0) {
			for (var i = 0; i < strStack1.length; i++) {
				strHtml += '<div class="col-md-4 col-sm-6 col-xs-12">';
				strHtml += strStack1[i];
				strHtml += '</div>';
			}
			for (var i = 0; i < strStack2.length; i++) {
				strHtml += '<div class="col-md-4 col-sm-6 col-xs-12">';
				strHtml += strStack2[i];
				strHtml += '</div>';
			}
		} else {
			if (strStack1.length === 0) {
				for (var i = 0; i < strStack2.length; i++) {
					strHtml += '<div class="col-md-4 col-sm-6 col-xs-12">' + strStack2[i] + '</div>';
				}
			} else {
				for (var i = 0; i < strStack1.length; i++) {
					strHtml += '<div class="col-md-4 col-sm-6 col-xs-12">' + strStack1[i] + '</div>';
				}
			}
		}
		$(this).hide();
		$(this).prev("div.row").hide();
		if (strHtml !== "") {
			$(this).show();
			$(this).prev("div.row").show();
		}
		return strHtml;
	}
	function bindEventOptionSelectionButton() {
		$("div.right-btn[data-command='add'], a[data-command='showDetail']", ".pool-item-option-wrapper").off("click").on("click", function () {
			var $self = $(this);
			var $container = $self.closest(".pool-item-option-wrapper");
			var productItem = getProductItemByCode($container.attr("data-product-code"));
			if (productItem == null) return false;

			if (ACCOUNTNUMBERLIMIT[productItem.OriginalAccountType] != -1 && $("#divSelectedProductPool div.pool-item-option-wrapper[data-original-account-type='" + productItem.OriginalAccountType + "']").length + 1 > ACCOUNTNUMBERLIMIT[productItem.OriginalAccountType]) {
				$("#popLimitSelectedAccountExceed").find("div[data-place-holder='content']").text("Maximum number of selected products per account type exceeded.");
				$("#popLimitSelectedAccountExceed").popup("open", { "positionTo": "window" });
				return false;
			}
			if (INSTANCECOUNTLIMIT[productItem.ProductCode] > 0 && $("#divSelectedProductPool div.pool-item-option-wrapper[data-product-code='" + productItem.ProductCode + "']").length + 1 > INSTANCECOUNTLIMIT[productItem.ProductCode]) {
				$("#popLimitSelectedAccountExceed").find("div[data-place-holder='content']").text("Maximum number of instances per product exceeded.");
				$("#popLimitSelectedAccountExceed").popup("open", { "positionTo": "window" });
				return false;
			}
			//check to determine whether to show detail popup or not
			var hasVisibleProductQuestions = productItem.CustomQuestions.length > 0;
			if (hasVisibleProductQuestions == true) {
				hasVisibleProductQuestions = $("[data-eid='divProductQuestion_chk" + productItem.ProductCode + "']").find("div.pq-wrapper:not(.hidden)").length > 0;
			}
			if ((hasVisibleProductQuestions == true) || productItem.ProductServices.length > 0 || productItem.ProductRates.length > 0) {
				//only show detail popup when product has service or custom question or rates
				buildDetailPopup(productItem, $("#productDetail"), $container);
				goToNextPage("#productDetail");
			} else {
				//add current product to pool
				var $item = addProductToSelectionPool(productItem);
				var $txt = buildDepositTextbox($item.attr("id"), productItem);				
                if ($txt != null) {
                    $("#divDepositProductPool").append($txt);
                    $txt.enhanceWithin();
                }
                $("#divRequiredProducts").trigger("change");
                $("#divAdditionalProducts").trigger("change");
			}
		});
		$("div.right-btn[data-command='add']", ".pool-item-option-wrapper").off("keypress").on("keypress", function (e) {
			var keycode = (e.keyCode ? e.keyCode : e.which);
			if (keycode == '13') {
				$(this).trigger("click");
			}
		});
	}
	//function checkAccountTypeHasRequireProduct(accType) {
	//	var ret = false;
	//	var PRODUCTLIST = getCurrentProductList();
	//	for (var i = 0; i < PRODUCTLIST.length; i++) {
	//		if (PRODUCTLIST[i].AccountType === accType && PRODUCTLIST[i].ProductIsRequired.toUpperCase() === "Y") {
	//			ret = true;
	//			break;
	//		}
	//	}
	//	return ret;
	//}
	function toggleAccountTypeItem(accType) {
		var $container = $("#divAdditionalProducts");
		if (PRODUCTLIST && _.filter(PRODUCTLIST, function(p) { return p.ProductIsRequired != "Y"; }).length >= 5) {
			$container.find("div[data-product-key]").addClass("hidden");
			$container.find("div[data-product-key='" + accType + "']").removeClass("hidden");
		}
		$.lpqValidate.hideValidation("#divAdditionalProducts");
	}
	xaProductFactory.hasFundedProducts = function () {
		var notFundedProducts = NOTFUNDEDPRODUCTCODES;
		var fundedProduct = false;
		var selectedElem = $("#divSelectedProductPool").find("div.pool-item-option-wrapper");
		selectedElem.each(function () {
			var currProductCode = $(this).attr('data-product-code');
			if (notFundedProducts.length > 0) {
				var notFundedProduct = false;
				for (var i = 0; i < notFundedProducts.length; i++) {
					if (currProductCode == notFundedProducts[i]) {
						notFundedProduct = true;
						break;
					}
				}
				if (!notFundedProduct) {
					fundedProduct = true;
					return false; //exit loop
				}
			} else {
				fundedProduct = true;
				return false; //exit loop
			}
		});
		return fundedProduct;
	}
	xaProductFactory.bindPreselectedProducts = function () {
		bindPreselectedProducts();
	}
	xaProductFactory.removeSelectedProductByProductCode = function(pCode) {
		$.each($("#divSelectedProductPool").find("div.pool-item-option-wrapper[data-product-code='" + pCode + "']"), function(idx, ele) {
			var $ele = $(ele);
			$('#divDepositAmount_chk' + pCode + $ele.attr("id")).remove();
			delete selectedRates[$ele.attr("id")];
			$ele.remove();
		});
		if ($("#divSelectedProductPool .pool-item-option-wrapper").length == 0) {
			$("#divSelectedProductPool").parent().addClass("hidden");
		}
	}
	function bindPreselectedProducts() {
		//handle preselect products: if products are not available in a selected zip pool --> remove preselect products from selectedproductspool
		for (var i = 0; i < PRODUCTLIST.length; i++) {
			if (PRODUCTLIST[i].ProductIsPreselected != "Y" || (PRODUCTLIST[i].CustomQuestions && PRODUCTLIST[i].CustomQuestions.length > 0)) continue;
			var pCode = PRODUCTLIST[i].ProductCode;
			var currElem = $(".product-selection-panel-v1 div.pool-item-option-wrapper[data-product-code='" + pCode + "']").first();
			if (currElem.hasClass('hidden')) {
				$("#divSelectedProductPool").find("div.pool-item-option-wrapper[data-product-code='" + pCode + "']").remove();
				$('div[id^="divDepositAmount_chk' + pCode).remove();
				if ($("#divSelectedProductPool .pool-item-option-wrapper").length == 0) {
					$("#divSelectedProductPool").parent().addClass("hidden");
				}
			} else if ($("#divSelectedProductPool div.pool-item-option-wrapper[data-product-code='" + pCode + "']").length == 0) {
				var $item = buildSelectedProductItem(PRODUCTLIST[i]);
				$("#divSelectedProductPool").append($item);
				bindEventSelectedProduct($item);
				$("#divDepositProductPool").append(buildDepositTextbox($item.attr("id"), PRODUCTLIST[i]));
				$("#divSelectedProductPool").parent().removeClass('hidden').trigger("change");
			}
		}
	}
	function bindEventSelectedProduct(srcEle) {
		var $item = $(srcEle);
		$item.find("div[data-command='remove']").off("click").on("click", function () {
			//var $self = $(this);
			var productItem = getProductItemByCode($item.attr("data-product-code"));
			if (productItem == null) return false;
			$("div[data-place-holder='content']", "#popConfirmRemove").html('Remove <span class="bold">' + productItem.ProductName + "</span> from your application?");
			$("#popConfirmRemove a[data-command='remove-product']").off("click").on("click", function () {
				$('#divDepositAmount_chk' + productItem.ProductCode + $item.attr("id")).remove();
				$("#" + $item.attr("id")).remove();
				delete selectedRates[$item.attr("id")];
				if ($("#divSelectedProductPool .pool-item-option-wrapper").length == 0) {
					$("#divSelectedProductPool").parent().addClass("hidden");
				}
				$("#divRequiredProducts").trigger("change");
				closePopup('#popConfirmRemove');
			});
			$("#popConfirmRemove").popup("open", { "positionTo": "window" });
		});
		$item.find("div[data-command='remove']").off("keypress").on("keypress", function (e) {
			var keycode = (e.keyCode ? e.keyCode : e.which);
			if (keycode == '13') {
				$(this).trigger("click");
			}
		});
		$item.find("div[data-command='edit'],div[data-command='info']").off("click").on("click", function () {
			var $self = $(this);
			var productItem = getProductItemByCode($item.attr("data-product-code"));
			if (productItem == null) return false;
			buildDetailPopup(productItem, $("#productDetail"), $item, $self.data("command"));
			goToNextPage("#productDetail");
		});
		$item.find("div[data-command='edit'],div[data-command='info']").off("keypress").on("keypress", function (e) {
			var keycode = (e.keyCode ? e.keyCode : e.which);
			if (keycode == '13') {
				$(this).trigger("click");
			}
		});

	}
	//only use 1a/2a for minor
	function updateHasMinorApplicant() {
		var mAccountTypeObj = getMinorAccountTypeObj();
		if (mAccountTypeObj.isMinor == "MINOR") {
			$('#hdHasMinorApplicant').val("Y");
		} else {
			$('#hdHasMinorApplicant').val(""); //no minor --> reset value to empty string;
		}
	}
	
	function showCurrentMinorProducts() {
		var mAccountTypeObj = getMinorAccountTypeObj();
		var $divLocationPool = $("#divLocationPool");
		if ($divLocationPool.length > 0) {
			$divLocationPool.next("div").removeClass("hidden");
		}
		if (mAccountTypeObj.mAccountType == "") {
			$("#divNoAvailableProducts").addClass("hidden");
			$("div.product-selection-panel-v1").addClass("hidden");
			$('.product-selection-panel-v1 .pool-item-option-wrapper').addClass("hidden");
		} else {
			$("div.product-selection-panel-v1").removeClass("hidden");
			var currMinorProductList = getCurrentProductList() || [];
			$('.product-selection-panel-v1 .pool-item-option-wrapper').each(function (idxProd, ele) {
				var $ele = $(ele);
				var pCode = $ele.data("product-code");
				if (!$ele.hasClass("zp-hidden")) {
					if (_.find(currMinorProductList, { ProductCode: pCode }) == undefined) {
						$ele.addClass("hidden");
						xaProductFactory.removeSelectedProductByProductCode(pCode);
					} else {
						$ele.removeClass("hidden");
					}
				}
			});
			bindPreselectedProducts();
			xaProductFactory.toggleProductSelectionSectionVisibility();
		}
	}
	function handleJointBtnAndPageTitle() {
		var mAccountTypeObj = getMinorAccountTypeObj();
		var hasSpecialJoint = $('#hdSpecialJoint').val();
		if (mAccountTypeObj.mAccountType == "" || mAccountTypeObj.mAccountType == "REGULAR") {
			$('.minorPageTitle').addClass("hidden");
			$('.pageTitle').removeClass("hidden");
			$('.minorCurrentAddressTitle').addClass("hidden");
			$('.currentAddressTitle').removeClass("hidden");
		} else {

			$('.minorPageTitle').removeClass("hidden");
			$('.pageTitle').addClass("hidden");
			$('.minorCurrentAddressTitle').removeClass("hidden");
			$('.currentAddressTitle').addClass("hidden");
			if (hasSpecialJoint == 'N') {
				$("#hdHasCoApplicant").val("N");
			}
		}
	}
	function handleShowAndHideESignature() {
		if ($('#hfLenderRef').val().toUpperCase().startsWith('SDFCU')) {
			var secondRoleType = $('#hdSecondRoleType').val().toUpperCase();
			//Custodian role type has no employment status -->hide employment status
			if (secondRoleType == "CUSTODIAN") {
				//need to show employment for custodian too
				/* if (hasSecRoleEmployment() != "Y") {
					 $('#divEmploymentStatus').addClass('HiddenElement');
				 } else {
					 $('#divEmploymentStatus').removeClass('HiddenElement');
				 } */
				$('#divESignature').removeClass('HiddenElement');

				var mFirstName = $('#m_txtFName').val();
				var mLastName = $('#m_txtLName').val();
				var mMiddleName = $('#m_txtMName').val();
				mFirstName = mFirstName.substring(0, 1).toUpperCase() + mFirstName.substring(1, mFirstName.length);
				mLastName = mLastName.substring(0, 1).toUpperCase() + mLastName.substring(1, mLastName.length);
				if (mMiddleName != "") {
					mMiddleName = mMiddleName.substring(0, 1).toUpperCase() + mMiddleName.substring(1, mMiddleName.length) + " ";
				}
				var fullName = mFirstName + " " + mMiddleName + mLastName;
				var date = new Date();
				var month = date.getMonth() + 1;
				$('#txtMinorName').html(fullName);
				$('#spDate').html(month + "/" + date.getDate() + "/" + date.getFullYear());
			} else {
				// $('#divEmploymentStatus').removeClass('HiddenElement');
				$('#divESignature').addClass('HiddenElement');
			}
		}
	}
	function getSelectedMinorProductCodes(selectedAccountType) {
		if (selectedAccountType == "") return [];
		var result = [];
		for (var i = 0; i < MINORPRODUCTLIST.length; i++) {
			if (MINORPRODUCTLIST[i].minorAccountCode == selectedAccountType) {
				result = _.cloneDeep(MINORPRODUCTLIST[i].minorAccountProductList);
				break;
			}
		}
		return result;
	}
	function getSelectedMinorProductList(selectedProductCodes) {
		var result = [];
		for (var i = 0; i < selectedProductCodes.length; i++) {
			for (var j = 0; j < PRODUCTLIST.length; j++) {
				if (selectedProductCodes[i] == PRODUCTLIST[j].ProductCode) {
					result.push(_.cloneDeep(PRODUCTLIST[j]));
					break;
				}
			}
		}
		return result;
	}
	//only use 1a/2a for minor 
	function getCurrentProductList() {
		if (isSpecialAccount()) { //only special account
			var currProductCodeList = getSelectedMinorProductCodes(getMinorAccountTypeObj().mAccountType);
			return getSelectedMinorProductList(currProductCodeList);
		} else {//no special account get normal product list
			return PRODUCTLIST;
		}
	}
	function updateSecondRoleType() {
		if ($('#hdHasMinorApplicant').val() == "Y") {
			var mAccountTypeObj = getMinorAccountTypeObj();
			var secondRoleType = "";

			for (var i = 0; i < MINORACCOUNTLIST.length; i++) {
				if (MINORACCOUNTLIST[i].minorAccountCode == mAccountTypeObj.mAccountType) {
					secondRoleType = MINORACCOUNTLIST[i].secondRoleType;
					break;
				}
			}
			$('#hdSecondRoleType').val(secondRoleType);
		}
	}
	xaProductFactory.handleShowAndHideSpecialAccountProducts = function() {
		updateHasMinorApplicant();
		//clearProductSelectionPool();
		showCurrentMinorProducts();
		handleJointBtnAndPageTitle();
		updateSecondRoleType();
		handleShowAndHideESignature();
		//var currProductList = getCurrentProductList();
		//for (var i = 0; i < currProductList.length; i++) {
		//	if (currProductList[i].ProductIsPreselected.toUpperCase() == "Y" || currProductList[i].ProductIsRequired.toUpperCase() == "Y") {
		//		var $displayCtrl = getProductOptionButton(currProductList[i].ProductCode);
		//		//no preselect products 
		//		$displayCtrl.closest('div[data-product-key]').removeClass("hidden");
		//	}
		//}

	}
	function registerESignatureControl() {
		$('#chkESignature').observer({
			validators: [
				function (partial) {
					if (!$('#hfLenderRef').val().toUpperCase().startsWith('SDFCU')) {
						return "";
					} else {
						var secondRoleType = $('#hdSecondRoleType').val().toUpperCase();
						if (secondRoleType == "CUSTODIAN") {
							if (!$(this).is(':checked')) {
								return "eSignature is required";
							}
						}
						return "";
					}
				}
			],
			validateOnBlur: true,
			group: "ValidateESignature"
		});
	}
	function showAndHideSpecialAccountButton() {
		//show special account button if it exist type=1a, 2a
		if (isSpecialAccount()) {
			$('.divAccountTypeCol').show();
			handleButtonSelection('ddlSpecialAccountType');
		} else {
			// $('.divBtnJointCol').addClass('col-sm-12 col-md-12');
			$('.divAccountTypeCol').hide();
		}
	}
	function handleRequiredProductLabel() {
		//handle required productLabel
		var availability = Common.GetParameterByName("type");
		var currProducts = getCurrentProductList();
		var institutionType = "";
		if (typeof $('#hfInstitutionType') !== "undefined" && $('#hfInstitutionType').val() !== null) {
			var institutionType = $('#hfInstitutionType').val().toUpperCase();
		}
		hasRequiredProduct = false;
		for (var i = 0; i < currProducts.length; i++) {
			if (currProducts[i].ProductIsRequired == "Y") {
				hasRequiredProduct = true;
				break;
			}
		}
		if (!hasRequiredProduct) {
			$('#reqProductLabel').html("Require at least one product to continue");
			if (institutionType == 'BANK') {
				$('#reqProductLabel').html("Require at least one account to continue");
			}
		}
		else if (availability == '2' || availability == '2a' || availability == '2b') {
			$('#reqProductLabel').html("Required product to continue");
			if (institutionType == 'BANK') {
				$('#reqProductLabel').html("Required account to continue");
			}
		}

		/*  if (availability == '2' || availability == '2a' || availability == '2b') {
			  $('#divSelectedProductPool').prev().hide();
		  }*/
	}
	function updateGroupCat() {
		$("#divAdditionalProducts div[data-product-key]").each(function () {
			var $container = $(this);
			if ($container.find("div.pool-item-option-wrapper:not(.hidden)").length == 0) {

				$("#btnGroupCat>a[data-id='" + $container.data("product-key") + "']").addClass("hidden"); //no available products then hide account type
				$("#ddlGroupCat").find(">option[value='" + $container.data("product-key") + "']").removeAttr("selected").wrap($("<span/>", {"style": "display: none;"}));
			} else {
				$("#btnGroupCat>a[data-id='" + $container.data("product-key") + "']").removeClass("hidden");
				var $opt = $("#ddlGroupCat").find("option[value='" + $container.data("product-key") + "']");
				var $optParent = $opt.parent();
				if ($optParent.is("span")) {
					$optParent.replaceWith($opt);
				}				
			}
		});
		var visibleGroupCatCount = $('#ddlGroupCat>option').length;
		if (visibleGroupCatCount == 0) {
			$('#ddlGroupCat').closest(".cat-tbl").addClass("hidden");
		} else {
			if ($('#ddlGroupCat>option:selected').length == 0) {
				$('#ddlGroupCat option').removeAttr("selected");
				$('#ddlGroupCat>option').eq(0).prop('selected', true);
			}
			$('#ddlGroupCat').selectmenu().selectmenu("refresh");
			groupCatChangeHandler($("#ddlGroupCat"));
			if (visibleGroupCatCount == 1) {
				$('#ddlGroupCat').closest(".cat-tbl").addClass("hidden");
			} else {
				$('#ddlGroupCat').closest(".cat-tbl").removeClass("hidden");
				toggleResponsiveCategory();
			}
		}
	}
	function groupCatChangeHandler($self) {
		$("#btnGroupCat a.active").removeClass("active");
		$("#btnGroupCat a[data-id='" + $self.val() + "']").addClass("active");
		toggleAccountTypeItem($self.val());
	}

	xaProductFactory.updateGroupCat = updateGroupCat;
	xaProductFactory.toggleProductSelectionSectionVisibility = function() {
		//hide required product label if there is no required products
		if (xaProductFactory.lenderHasRequiredProducts() == true) {
			$("#divRequiredProducts").parent().removeClass("hidden");
		}else{
			$("#divRequiredProducts").parent().addClass("hidden");
		}

		if ($("#divSelectedProductPool>div").length > 0) {
			$("#divSelectedProductPool").parent().removeClass("hidden");
		} else {
			$("#divSelectedProductPool").parent().addClass("hidden");
		}
		
		var visibleAvailableProductsCount = $("#divAdditionalProducts").find("div.pool-item-option-wrapper:not(.hidden)").length;
		if (visibleAvailableProductsCount == 0) {
			$("#divAdditionalProducts").find("div[data-product-key]").addClass("hidden");
		}else if (visibleAvailableProductsCount < 5) {
			$("#divAdditionalProducts").find("div[data-product-key]").removeClass("hidden");
		} else {
			$.each($("#divAdditionalProducts").find("div[data-product-key]"), function (idxAddi, ele) {
				var $ele = $(ele);
				if ($ele.find(">div.pool-item-option-wrapper:not(.hidden)").length == 0) {
					$ele.addClass("hidden");
				} else {
					$ele.removeClass("hidden");
				}
			});
		}
		

		//there are no available products-->hide product title and diplay no products message
		if ($("#divAdditionalProducts").find("div[data-product-key]:not(.hidden)").length > 0) {
			$("#divAdditionalProducts").parent().removeClass("hidden");
			if (visibleAvailableProductsCount < 5) {
				$("#divAdditionalProducts").parent().find(".cat-tbl").addClass("hidden");
			} else {
				$("#divAdditionalProducts").parent().find(".cat-tbl").removeClass("hidden");
				updateGroupCat();
			}
		} else {
			$("#divAdditionalProducts").parent().addClass("hidden");
		}
		if ($("#divLocationPool").length > 0) {//when location pool filter enabled
			if ($("#divAdditionalProducts").parent().hasClass("hidden") && ($("#divRequiredProducts").length == 0 || $("#divRequiredProducts").parent().hasClass("hidden"))) {
				$("#divLocationPool").next("div").addClass("hidden");
				$("#divNoAvailableProducts").removeClass("hidden");
			} else {
				$("#divLocationPool").next("div").removeClass("hidden");
				$("#divNoAvailableProducts").addClass("hidden");
			}
		}
	}
	xaProductFactory.init = function () {
		
		performPQAdvancedLogics();
		//bindProductQuestionEvent();
		bindEventOptionSelectionButton();
		bindEventPopupDetail();
		bindEventFundingDialog();
		//bindEventSelectedProduct();
		if (isSpecialAccount()) {
			xaProductFactory.handleShowAndHideSpecialAccountProducts();
		}
		bindPreselectedProducts();
		//dropdown special account type
		$('#btnSelectionContainer').on('change', 'select#ddlSpecialAccountType', function () {
			xaProductFactory.handleShowAndHideSpecialAccountProducts();
		});

		//has minor optional 1b,2b
		$('#btnSelectionContainer').on('click', 'a#ddlSpecialAccountType', function () {
			var mAccountTypeObj = getMinorAccountTypeObj();
			if (mAccountTypeObj.isMinor == "OPTIONAL") {
				//review code for getMinorAccountTypeObj method, there is no case that return isMinor == "OPTIONAL"
				// don't know when and how these line of code works if isMinor will never equal "OPTIONAL"
				xaProductFactory.handleShowAndHideSpecialAccountProducts();
			}
		});
		//display the red star for required product
		for (var i = 0; i < PRODUCTLIST.length; i++) {
			var cQuestionArr = PRODUCTLIST[i].CustomQuestions;
			if (cQuestionArr.length > 0) {
				for (var j = 0; j < cQuestionArr.length; j++) {
					if (cQuestionArr[j].AnswerType === "HEADER") continue;
					if (cQuestionArr[j].IsRequired === true) {
						$("[data-eid='reqQuestion_" + (j + 1) + "_chk" + PRODUCTLIST[i].ProductCode + "']").show();
						if (cQuestionArr[j].AnswerType == "PASSWORD" && cQuestionArr[j].RequireConfirmation == true) {
							$("[data-eid='re_reqQuestion_" + (j + 1) + "_chk" + PRODUCTLIST[i].ProductCode + "']").show();
						}
					} else {
						$("[data-eid='reqQuestion_" + (j + 1) + "_chk" + PRODUCTLIST[i].ProductCode + "']").hide();
					}
				}
			}
		}
		//bindSelectedAccountTypes();

		registerESignatureControl();
		showAndHideSpecialAccountButton();
		//handleButtonSelection('ddlAccountType');
		$('#chkESignature').on('click', function () {
			var currElem = $(this);
			if (currElem.is(':checked')) {
				$('#eSignDate').removeClass('HiddenElement');
			} else {
				$('#eSignDate').addClass('HiddenElement');
			}
			var validator = $.lpqValidate("ValidateESignature");
		});
		//remove checkbox and visibility:hidden if it exist in product description(case wscu_test)
		$('.product-selection-panel-v1 div[data-command-type="optionBtn"]').each(function () {
			var currElem = $(this);
			var productKey = currElem.parent().parent().attr('data-product-key');
			var pDescElem = currElem.find('p');
			pDescElem.find('img').css('visibility', '');
			pDescElem.find('img').prev('br').remove();
			if (productKey != "REQUIRED") {
				pDescElem.find('img').remove();
			}
			var chkElem = pDescElem.find('input[type="checkbox"]');
			if (chkElem.length > 0) {
				chkElem.remove();
				pDescElem.find('div[class="ui-checkbox"]').remove();
				pDescElem.find('label').remove();
			}
		});// end remove checkbox and visibility:hidden if it exist in product description(case wscu_test)

		handleRequiredProductLabel();
		//if ($('#hdHasZipCodePool').val() != "Y") {
			$('#divRequiredProducts').observer({
				validators: [
					function (partial) {
						if ($(this).is(":hidden")) return "";
						//minor 
						if (getMinorAccountTypeObj().isMinor == "MINOR" && $('select#ddlSpecialAccountType option:selected').val() == "") {
							return "";  // there are no product to select--skip to validate
						}
						if ($("#divSelectedProductPool>.pool-item-option-wrapper").length == 0) {
							return "Please select a required product";
						}
						var requiredProductSelected = false;
						$("#divSelectedProductPool").find("div.pool-item-option-wrapper[data-product-code]").each(function () {
							var $self = $(this);
							var pItem = getProductItemByCode($self.data("product-code"));
							if (pItem != null && pItem.ProductIsRequired === "Y") {
								requiredProductSelected = true;
								return;
							}
						});

						if (requiredProductSelected == false) {
							return "Please select a required product";
						}
						return "";
					}
				],
				validateOnBlur: true,
				group: "ValidateProductSelection",
				groupType: "complexUI",
				placeHolder: "#divRequiredProducts"
			});
			$('#divAdditionalProducts').observer({
				validators: [
					function (partial) {
						if ($(this).is(":hidden") || $("#divRequiredProducts").is(":visible")) return "";
						//minor 
						if (getMinorAccountTypeObj().isMinor == "MINOR" && $('select#ddlSpecialAccountType option:selected').val() == "") {
							return "";  // there are no product to select--skip to validate
						}
						if ($("#divSelectedProductPool>.pool-item-option-wrapper").length == 0) {
							return "Please select at least 1 product";
						}
						return "";
					}
				],
				validateOnBlur: true,
				group: "ValidateProductSelection",
				groupType: "complexUI",
				placeHolder: "#divAdditionalProducts"
			});
			$('#divSelectedProductPool').observer({
				validators: [
					function (partial) {
						var errMsg = "";
						if ($("#divSelectedProductPool .pool-item-option-wrapper").length > 0) {
							$.each($("#divSelectedProductPool .pool-item-option-wrapper"), function (idxSel, ele) {
								var $ele = $(ele);
								var pCode = $ele.data("product-code");
								var productItem = getProductItemByCode(pCode);
								if (productItem.CustomQuestions && productItem.CustomQuestions.length > 0 && _.filter(productItem.CustomQuestions, { IsRequired: true }).length) {
									if (!customQuestionAnswers[$ele.attr("id")]) {
										errMsg = "Please complete all required fields";
										$ele.find("div[data-command='edit']").trigger("click");
										return false;
									}
								}
							});
						}
						return errMsg;
					}
				],
				validateOnBlur: false,
				group: "ValidateProductSelection",
				groupType: "complexUI",
				placeHolder: "#divSelectedProductPool"
			});
		//}
		$("#popLimitSelectedAccountExceed, #popConfirmRemove").on("popupafteropen", function (event, ui) {
			//prevent scrollbar by popup overlay
			$("#" + this.id + "-screen").height("");
		});
		$("#popConfirmRemove").on("popupafterclose", function (event, ui) {
			if ($("#divSelectedProductPool .pool-item-option-wrapper").length > 0) {
				//$("#divSelectedProductPool .pool-item-option-wrapper").first().find(".right-btn[data-command='edit']").focus();
			} else if ($("div.pool-item-option-wrapper:visible").length > 0) {
				//$("div.pool-item-option-wrapper:visible").last().find(".right-btn[data-command='add']").focus();
			}
		});
		

		if ($("#divLocationPool").length > 0) {
			//in location pool is enabled, hide all other sections on page until valid zip code entered
			$("#divLocationPool").next("div").addClass("hidden");
		}
		toggleResponsiveCategory();
		$(window).on('resize', toggleResponsiveCategory);
		$("#ddlGroupCat").on("change", function () {
			groupCatChangeHandler($(this));
		});
		$("#btnGroupCat>a[data-id]").on("click", function () {
			var $self = $(this);
			$("#btnGroupCat a.active").removeClass("active");
			$self.addClass("active");
			$("#ddlGroupCat").val($self.data("id")).selectmenu("refresh");
			groupCatChangeHandler($("#ddlGroupCat"));
		});
		if ($("#divLocationPool").length == 0) {
			xaProductFactory.toggleProductSelectionSectionVisibility();
		}
		
		$(document).on("pageshow", function () {
			var curPageId = $.mobile.pageContainer.pagecontainer('getActivePage').attr('id');
			/*if (curPageId === "productDetail") {

				if (actionStack.length > 0) {
					var reopenPopupPattern = /openDetailshowDocopenDetail$/i;
					//var autoScrollPattern = /showDoc$/i;
					if (reopenPopupPattern.test(actionStack.join(""))) {
						//var currentProdCode = $("#productDetail").find('input:hidden[data-place-holder="productCode"]').val();
						//var sourceElementId = $("#productDetail").find('input:hidden[data-place-holder="sourceElementId"]').val();
						//var productItem = getProductItemByCode(currentProdCode);
						//if (productItem != null) {
						//	buildDetailPopup(productItem, $("#productDetail"), $("#"+sourceElementId));
						//$('html, body').scrollTop($("#btn_" + currentProdCode).offset().top - 40);

						//}
					}
				}
			}*/
			if (curPageId === "GettingStarted") {
				$("#btnGroupCat>a[data-id]").off("click").on("click", function () {
					var $self = $(this);
					$("#btnGroupCat a.active").removeClass("active");
					$self.addClass("active");
					$("#ddlGroupCat").val($self.data("id")).selectmenu("refresh");
					groupCatChangeHandler($("#ddlGroupCat"));
				});
				bindEventOptionSelectionButton();
				//bindEventSelectedProduct();
				actionStack = []; //reset stack;
			}
			if (curPageId == "page2") {
				handleShowAndHideESignature();
			}
			if (curPageId == "popupPDFViewer") {
				actionStack.push("showDoc");
			}

		});
		$("#txtRateMaturityDate", "#fundingDialog").datepicker({
			showOn: 'button',
			changeMonth: true,
			changeYear: true,
			//yearRange: "-100:+0",
			minDate: new Date(),
			buttonImage: "../images/calendar.png",
			buttonImageOnly: true,
			onSelect: function (value, inst) {
				var date = $(this).datepicker('getDate');
				$("#txtRateMaturityDate2", "#fundingDialog").val(padLeft(date.getDate(), 2));
				$("#txtRateMaturityDate1", "#fundingDialog").val(padLeft(date.getMonth() + 1, 2));
				$("#txtRateMaturityDate3", "#fundingDialog").val(padLeft(date.getFullYear(), 4));
				$("#txtRateMaturityDate", "#fundingDialog").datepicker("hide");
				$("#txtRateMaturityDate1", "#fundingDialog").trigger("focus").trigger("blur");
			}

		});
		$('#divRateMaturityDateStuff div.ui-input-date input', "#fundingDialog").on('focusout', function () {
			var maxlength = parseInt($(this).attr('maxlength'));
			// add 0 if value < 10
			$(this).val(padLeft($(this).val(), maxlength));
			updateHiddenRateMaturityDate();
		});
		$('#txtRateTermLength', "#fundingDialog").on("keyup", function() {
			var $self = $(this);
			if ($self.data("term-type") == "D") {
				var minTerm = parseFloat($self.data("min-term"));
				var maxTerm = parseFloat($self.data("max-term"));
				var value = parseFloat($self.val());
				if ((minTerm < 0 || value >= minTerm) && (maxTerm < 0 || value <= maxTerm)) {
					var maturityDate = moment().startOf("day").add(parseInt($self.val()), "d");
					$("#txtRateMaturityDate2", "#fundingDialog").val(padLeft(maturityDate.date(), 2));
					$("#txtRateMaturityDate1", "#fundingDialog").val(padLeft(maturityDate.month() + 1, 2));
					$("#txtRateMaturityDate3", "#fundingDialog").val(maturityDate.year());
				} else {
					$("#txtRateMaturityDate2", "#fundingDialog").val("");
					$("#txtRateMaturityDate1", "#fundingDialog").val("");
					$("#txtRateMaturityDate3", "#fundingDialog").val("");
				}
				updateHiddenRateMaturityDate();
			}
			
		});
		$('#txtRateDepositAmount', "#fundingDialog").on("blur", function () {
			var $self = $(this);
			var productCode = $("#fundingDialog").find('input:hidden[data-place-holder="productCode"]').val();
			var productItem = getProductItemByCode(productCode);
			var value = Common.GetFloatFromMoney($self.val());
			var minDeposit, maxDeposit;
			if ($self.data("is-auto-calculated-tier") == true) {
				minDeposit = findLowestMinDepositRate(productItem).MinDeposit;
				maxDeposit = findHighestMaxDepositRate(productItem).MaxDeposit;
				if ((minDeposit <= 0 || value > minDeposit) && (maxDeposit <= 0 || value < maxDeposit)) {
					var selectedRate = findRateBasedOnFundingAmount(productItem, value);
					var $selectedRateBlock = $("#rate_" + productItem.ProductCode + "_" + selectedRate.ID + "_h", $("#fundingDialog")).closest("tr");
					selectRateItemHandler($selectedRateBlock, $("#fundingDialog"), productItem);
					$self.val(Common.FormatCurrency(value, true));
				}
			}
		});
	}

}(window.xaProductFactory = window.xaProductFactory || {}, jQuery));
$(function () {
	if ($("div.product-selection-panel-v1").length > 0) {
		xaProductFactory.init();
	}
});