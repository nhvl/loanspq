﻿Imports Workflows.Base.BO
Imports Workflows.XA.BO
Imports LPQMobile.Utils.Common

Partial Class xa_CallBack
	Inherits CBasePage
	
	Protected Sub Page_PreLoad(sender As Object, e As EventArgs) Handles Me.PreLoad
		Dim task As String = SafeString(Request.Form("Task"))
		If task <> "SubmitLoan" AndAlso task <> "WalletQuestions" Then
			Response.Clear()
			Response.Write(String.Empty)
			Response.End()
		End If
	End Sub
	Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
		Try
			Dim workFlowSessionID As String = SafeString(Request.Form("WorkflowSessionID"))
			If String.IsNullOrWhiteSpace(workFlowSessionID) Then
				Throw New ArgumentException("Missing Workflow SessionID")
			End If
			Dim state As New XaState
			Dim cfg As New XaConfig(_LoanType, CustomListRuleList)
			cfg.WebsiteConfig = _CurrentWebsiteConfig
			cfg.SubmissionLimiterExemptIPs = SubmissionLimiterExemptIPs
			cfg.SubmissionMaxAppBeforeNotification = SubmissionMaxAppBeforeNotification
			cfg.SubmissionMaxAppBeforeBlocking = SubmissionMaxAppBeforeBlocking
			cfg.SubmissionLimiterInternalEmails = SubmissionLimiterInternalEmails
			cfg.CustomListRuleList = CustomListRuleList
			Dim storage As New Storage(Of XaSubmission, XaState, XaConfig)(cfg, state, workFlowSessionID)
			Dim wf As New Workflows.XA.XaWorkflow(Of XaSubmission, XaState, XaConfig)(HttpContext.Current, storage)
			wf.Start()
		Catch ex As Exception
			log.Error(ex.Message, ex)
		End Try
	End Sub
End Class


