﻿var isSubmitWalletAnswer = false;
var isSubmittingXA = false;
var numWalletQuestions = 0;
var isApplicationCompleted = false;
var EMPLogicPI = {};
var EMPLogicJN = {};
var EMPLogicMR = {}; //for Minor App
var EMPLogicPI_PREV = {};
var EMPLogicJN_PREV = {};
var EMPLogicMR_PREV = {}; //for Minor App
var InitBranchOptions = [];
var docScanObj, co_docScanObj;

///////////////////             TURNED OFF FOR DEBUGGING

//execute after DOM has been initialized, but before fully rendered
function initXAModule(options) {
	options = options || {};
	var defaultSettings = {
		collectJobTitleOnly: false
	};
	var settings = $.extend({}, defaultSettings, options);



    validateSpecialAccountType();
    //minor employment status
    EMPLogicMR = $.extend(true, {}, EMPLogic);
    EMPLogicMR.init('m_ddlEmploymentStatus', 'm_', 'pageMinorInfo', 'm_divEmploymentStatusRest', (settings.collectJobTitleOnly ? "JobTitleOnly" : ""));
    $('#pageMinorInfo').on("pageshow", function () {
        EMPLogicMR.refreshHtml();
    });
    EMPLogicMR_PREV = $.extend(true, {}, EMPLogic);
    EMPLogicMR_PREV.init('m_prev_ddlEmploymentStatus', 'm_prev_', 'pageMinorInfo', 'm_prev_divEmploymentStatusRest');
    //$('#pageMinorInfo').on("pageshow", function () {
    //	EMPLogicMR_PREV.refreshHtml();
    //});


    EMPLogicPI = $.extend(true, {}, EMPLogic);
    EMPLogicPI.init('ddlEmploymentStatus', '', 'page2', 'divEmploymentStatusRest', (settings.collectJobTitleOnly ? "JobTitleOnly" : ""));
    $('#page2').on("pageshow", function () {
        EMPLogicPI.refreshHtml();
    });
    EMPLogicPI_PREV = $.extend(true, {}, EMPLogic);
    EMPLogicPI_PREV.init('prev_ddlEmploymentStatus', 'prev_', 'page2', 'prev_divEmploymentStatusRest');
    //$('#page2').on("pageshow", function () {
    //	EMPLogicPI_PREV.refreshHtml();
    //});

    
    EMPLogicJN = $.extend(true, {}, EMPLogic);
    EMPLogicJN.init('co_ddlEmploymentStatus', 'co_', 'page6', 'co_divEmploymentStatusRest', (settings.collectJobTitleOnly ? "JobTitleOnly" : ""));
    $('#page6').on("pageshow", function () {
        EMPLogicJN.refreshHtml();
    });
    EMPLogicJN_PREV = $.extend(true, {}, EMPLogic);
    EMPLogicJN_PREV.init('co_prev_ddlEmploymentStatus', 'co_prev_', 'page6', 'co_prev_divEmploymentStatusRest');
    //$('#page6').on("pageshow", function () {
    //	EMPLogicJN_PREV.refreshHtml();
    //});
    

    $(document).on("pageshow", function () {
    	var curPageId = $.mobile.pageContainer.pagecontainer('getActivePage').attr('id');
    	switch (curPageId) {
    		case "pageMinorInfo":
    			EMPLogicMR.refreshHtml();
    			EMPLogicMR_PREV.refreshHtml();
    			break;
    		case "page2":
    			EMPLogicPI.refreshHtml();
    			EMPLogicPI_PREV.refreshHtml();
    			$("#hdHasCoApplicant").val("N"); //reset
    			break;
    		case "page6":
    			EMPLogicJN.refreshHtml();
    			EMPLogicJN_PREV.refreshHtml();
    			break;
    	    case "pageFS":
    			var membershipFees = calculateOneTimeMembershipFee();
    			$("#divMembershipFee").text(Common.FormatCurrency(membershipFees, true));
    			$('#txtTotalDeposit').val(Common.FormatCurrency(getTotalDeposit(), true));
                //hide membership free label if it is secondary account
    			var availability = Common.GetParameterByName("type");
    			if (availability == '2' || availability == '2a' || availability == '2b' || membershipFees == 0) {
    			    $("#divMembershipFee").closest('div[data-role="fieldcontain"]').hide();
    			} else {
    			    $("#divMembershipFee").closest('div[data-role="fieldcontain"]').show();  //for case where consumer go back and change FOM
    			}
    			break;
    		case "reviewpage":
			    pushToPagePaths("reviewpage");
			    break;
    	    case "xa_last": //if online registration btn is enable then hide Return to Home btn
    	        if ($("#btnRegisterOnline").length > 0) {
    	            $("#btnRedirect").closest("div[data-role='footer']").hide();
    	        }
    		default:
    	}
    });
   
    
    
    ///////////////////             TURNED OFF FOR DEBUGGING
    //if the Membership Eligibility is the first page, when clicking to the next page(open membership page) some functions 
    // Unbind the events for things that are locked (such as required accounts)
    $('.requiredAccountCollapsible').bind('expand', function () {}).bind('collapse', function () {$(this).trigger('expand');});
    
    // Get the initial values for the accounts
    // Record the minimum deposit amounts too.
    

    //hide Restart button on first page. Only happen in XA module
    $("#GettingStarted").find("div.restartbtn-placeholder").hide();
    $("#popScanZipWarning").on("popupafteropen", function (event, ui) {
    	//prevent scrollbar by popup overlay
    	$("#" + this.id + "-screen").height("");
    });
    $("#popScanZipWarning").on("popupafterclose", function (event, ui) {
	    goToNextPage("#page2");
    });
	// Fill in the fields with test data
    if (Common.GetParameterByName("autofill") == "true") {
    	// Fill in the stuff
    	autoFillData();
    }
    docScanObj = new LPQDocScan("");
    co_docScanObj = new LPQDocScan("co_");
    //InstaTouch: Call 1 ("Authorization") called on the loading of the "Product Info" and not after an applicant has clicked the "Continue" button
    if (window.IST) {
        IST.FACTORY.loadCarrierIdentification();
    }
    pushToPagePaths("GettingStarted");
};

function ScanAccept(prefix) {
	if (prefix == "") {
		docScanObj.scanAccept();
		goToNextPage("#page2");
	} else {
		co_docScanObj.scanAccept();
		goToNextPage("#page6");
	}
}
function calculateOneTimeMembershipFee() {
    // make sure FOM exist 
    //secondary account --> no membership fee
	var availability = Common.GetParameterByName("type");
    if (!hasFOM() ||availability=='2'||availability=='2a'||availability=='2b'){
        return 0.0;
    }
	var membershipFee = parseFloat($("#hdMembershipFee").val());
	var fomAnswer = {};
	setFOM(fomAnswer);
	if (fomAnswer.FOMIdx != -1) {
	    if (typeof CUSTOMLISTFOMFEES[fomAnswer.FOMName] === "string") {
		    membershipFee = membershipFee + parseFloat(CUSTOMLISTFOMFEES[fomAnswer.FOMName]);
		}
	}
	return membershipFee;
}

function validateForm(element) {  
    //this lines of code is to prevent user click submit multiple times accidently
    //  if (isSubmittingXA) {
    //	 return false;
    //  }
    //  isSubmittingXA = true;
	return validateAll(element);
}


function validateWalletQuestions(element) {
	if ($(element).attr("contenteditable") == "true") return false;
    //this lines of code is to prevent user click submit multiple times accidently
    // if (isSubmitWalletAnswer) {
    //    return false;
    // }
    //isSubmitWalletAnswer = true;

    // Make sure that there is a selected answer for each question.
    // After validating the questions, submit them
    var index = 0;
    var numSelected = 0;
    var errorStr = "";

    var numOfQuestions = $('#hdNumWalletQuestions').val();
    numOfQuestions = parseInt(numOfQuestions, 10);

    for (index = 1; index <= numOfQuestions; index++) {
        // Check to see if a radio button was selected
        var radioButtons = $('input[name=walletquestion-radio-' + index.toString() + ']');
        var numRadioButtons = radioButtons.length;
        for (var i = 0; i < numRadioButtons; i++) {
            if ($(radioButtons[i]).prop('checked') == true) {
                numSelected++;
            }
        }
        if (numSelected == 0) {
            errorStr += "Please answer all the questions";
            break;
        }
        numSelected = 0;
    }

    if (errorStr != "") {
    	// An error occurred, we need to bring up the error dialog.
    	if ($("#divErrorPopup").length > 0) {
    		$('#txtErrorPopupMessage').html(errorStr);
    		$("#divErrorPopup").popup("open", { "positionTo": "window" });
    	} else {
    		var nextpage = $('#divErrorDialog');
    		$('#txtErrorMessage').html(errorStr);
    		if (nextpage.length > 0) {
    			$.mobile.changePage(nextpage, {
    				transition: "slide",
    				reverse: false,
    				changeHash: true
    			});
    		}
    	}
        //return errorStr;
        return false;
    }
    // No errors, so we can proceed
    submitWalletAnswers();
    return false;
    //return true;
}
//NO USE ANYWHERE
function valButton(btn) {
    for (var i = btn.length - 1; i > -1; i--) {
        if (btn[i].checked) return btn[i].value;
    }
    return null;
}

function getTotalDeposit() {
    var totalDeposit = 0.0;
    totalDeposit = xaProductFactory.getTotalDeposit() + calculateOneTimeMembershipFee();
    return totalDeposit;
}
function hasHiddenMinorProduct(element) {
    if (isSpecialAccount()) {
        var currElement = element.parent().parent().parent().attr('style');
        if (!element.parent().hasClass('ui-checkbox')) {
            currElement = element.parent().parent().attr('style');
        }
        if (currElement != undefined && currElement.indexOf('block') == -1) {
            return true; 
        }
    }
    return false;
}
function ViewSelectedProducts() {
    var strViewSelectedProduct = '<div class="col-sm-6 col-xs-12">';

    $('.selectProduct').each(function () {
        var productType = $(this).attr('product_type') ? $(this).attr('product_type').toLowerCase() : '';
        if ($(this).is(':checked') && productType !== 'other') {
            if (hasHiddenMinorProduct($(this))) {
                return true; //continue the loop
            }
            var txtDepositAmountName = $('#txtDepositAmount_' + $(this).attr('id')).attr('text');
            var txtDepositAmount = $('#txtDepositAmount_' + $(this).attr('id')).val();
            strViewSelectedProduct += '<div class="row"><div class="col-xs-6 text-right row-title"><span class="bold">' + txtDepositAmountName + '</span></div><div class="col-xs-6 text-left row-data"><span>' + Common.FormatCurrency(txtDepositAmount, true) + '</span></div></div>';
        }
    });
    strViewSelectedProduct += '<div class="row"><div class="col-xs-6 text-right row-title"><span class="bold">Total Deposit</span></div><div class="col-xs-6 text-left row-data"><span>' + Common.FormatCurrency($('#txtTotalDeposit').val(), true) + '</span></div></div></div>';

    return strViewSelectedProduct;
}


//******end add new functions

function hasFOM() {
    return $('#HideFOMPage').val() != "Y";
}

function hasForeignTypeApp() {
    return $('#hdForeignAppType').val() == "Y";
}
function validateGettingStartedPage(nextpage) {
	var validator = true;
	if ($.lpqValidate("ValidateBranch") === false) {
		validator = false;
	}
	if (hasFOM() && $.lpqValidate("ValidateFOM") === false) {
		validator = false;
	}
	if ($.lpqValidate("ValidateProductSelection") == false) {
		validator = false;
	}
	if ($.lpqValidate("ValidateSpecialAccountType") == false) {
		validator = false;
	}
	if ($("#divDisclosureSection.showfield-section").length == 0 || $("#divDisclosureSection.showfield-section").hasClass("hidden") == false) {
		if ($("#GettingStarted").find("#divDisclosure").length > 0 && $.lpqValidate("ValidateDisclosure_disclosures") == false) {
			validator = false;
		}
	}
	// Validate custom questions, which may be moved to the front page
	if ($('#loanpage_divApplicantQuestion').children().length > 0) {
		if ($.lpqValidate("loanpage_ValidateApplicantQuestionsXA") === false) {
			validator = false;
		}
	}
	//xaProductFactory.toggleDepositTextFields();
	if (validator === false) {
		Common.ScrollToError();
	} else {
		pushToPagePaths(nextpage);
		if (window.ABR) {
			var formValues = prepareAbrFormValues("GettingStarted");
			if (ABR.FACTORY.evaluateRules("GettingStarted", formValues)) {
				return false;
			}
		}
		if (nextpage == "page2") {
			if (window.IST) {
				IST.FACTORY.carrierIdentification();
			} else {
				$.mobile.changePage($("#page2"), {
					transition: "slide",
					reverse: false,
					changeHash: true
				});
			}
		} else {
			$.mobile.changePage($("#" + nextpage), {
				transition: "slide",
				reverse: false,
				changeHash: true
			});
		}
	}
}

//modify validateActivePage based on the changing format
function validateActivePage(element, normalClick) {
    // When the validation function is not called directly, we need a way to figure out which page
    // to perform the validation, so we call this function and it will sort things out for us.
    // First, we need to figure out the ID of the active page
	var currElement = $(element);
	if (currElement.attr("contenteditable") == "true") return false;
    var activePage = $.mobile.pageContainer.pagecontainer('getActivePage').attr("id");
    var errorStr = "";
    var nextPage = "#pageFom";
    var hasCoApp = $('#hdHasCoApplicant').val();
    var hasMinorApplicant = $('#hdHasMinorApplicant').val();
    var validator = true;

    switch (String(activePage).toUpperCase()) {
    	/*case "SCANDOCS":
    		validator = true;
		    errorStr = "";
            if (scanAccept(false) == false) {
		    	$("#popScanZipWarning").popup("open", { "positionTo": "window" });
			    return false;

		    }
            //New UI --> go to page2 when clicking continue/goback button
            nextPage = "#page2";
            break;
         */ 
        case "PAGEMINORINFO":
    		validator = true;
		    errorStr = "";
    		var hasMinorApplicant = $('#hdHasMinorApplicant').val();
    		if (hasMinorApplicant == 'Y') {
    			if (m_ValidateApplicantInfoXA() === false) {
    				validator = false;
    			}
    			if (m_ValidateApplicantContactInfoXA() === false) {
    				validator = false;
    			}
    			if ($('#hdEnableIDSectionForMinor').val() == 'Y') {
    				if (m_ValidateApplicantIdXA() === false) {
    					validator = false;
    				}
    			}
			    if (m_ValidateApplicantAddress() === false) {
					validator = false;
				}
				if (m_ValidateApplicantMailingAddress() === false) {
					validator = false;
				}
				if ($('#hdEnableOccupancyStatusSectionForMinor').val() == 'Y') {
				   if (m_ValidateApplicantPreviousAddress() === false) {
					    validator = false;
				    }
			    }
			    if ($("#hdEnableEmploymentSectionForMinor").val().toUpperCase() === "Y") {
    				if (m_ValidateApplicantEmploymentXA() === false) {
    					validator = false;
    				}
    			}
				if ($('#m_divApplicantQuestion').children().length > 0) {
					if ($.lpqValidate("m_ValidateApplicantQuestionsXA") === false) {
						validator = false;
					}
				}
    		}
    	    //errorStr += validateMinorApplicantInfo();
    		if ($('#hdSecondRoleType').val() != undefined && $('#hdSecondRoleType').val().toUpperCase() == "NONE") { //only minor applicant 
    		    //make sure funding options are available --> skip funding page
    		    if ($("#divFundingTypeList a.btn-header-theme").length > 0 && xaProductFactory.hasFundedProducts()) {
    		        nextPage = "#pageFS";
    		    } else {
    		        nextPage = "#reviewpage";
    		    }
    		} else {
    		    nextPage = '#page2';
    		}
            break;
    	case "PAGEFS":
    		validator = true;
		    if (validateFS1() == false) {
				validator = false;
			}
            nextPage = "#reviewpage";
            break;
    	case "PAGE2":
		    hasCoApp = (currElement.attr('id') == 'continueWithCoApp' ? "Y" : "N");
            $("#hdHasCoApplicant").val(hasCoApp);

        	validator = true;
	        errorStr = "";
        	if(ValidateApplicantInfoXA() === false) {
		        validator = false;
        	}

            //beneficiary 	
        	if ($('#showBeneficiary').attr('status') == 'Y') {
        	    if (validateBeneficiary() == false) {
        	        validator = false;
        	    }
        	}
        	
        	if(ValidateApplicantAddress() === false) {
        		validator = false;
        	}
	        if ($('#hdEnableOccupancyStatusSection').val() == 'Y') {
		        if (ValidateApplicantPreviousAddress() === false) {
			        validator = false;
		        }
	        }
	        if(ValidateApplicantMailingAddress() === false) {
        		validator = false;
        	}
        	if(ValidateApplicantContactInfoXA() === false) {
        		validator = false;
        	}
	        if ($('#hdEnableIDSection').val() == 'Y') {
		        if (ValidateApplicantIdXA() === false) {
			        validator = false;
		        }
	        }
	        if ($("#hdEnableEmploymentSection").val().toUpperCase() !== "N") {
            	if (ValidateApplicantEmploymentXA() === false) {
            		validator = false;
            	}
            }
	        if (typeof docUploadObj != "undefined" && docUploadObj != null && docUploadObj.validateUploadDocument() == false) {
            	validator = false;
            }
          /*  var eSignatureMsg = validateESignature();
			if (eSignatureMsg !== "") {
				validator = false;
				errorStr += eSignatureMsg;
			}
            */
           /* if (validateESignature() === false) {
                validator = false;
            }*/
            if ($.lpqValidate("ValidateESignature") === false) {
                validator = false;
            }
			if ($('#divApplicantQuestion').children().length > 0) {
				if ($.lpqValidate("ValidateApplicantQuestionsXA") === false) {
					validator = false;
				}
			}
            
            var requiredDLMessage = validateRequiredDLScan($('#hdRequiredDLScan').val(), false);
            if (requiredDLMessage != "") {
            	errorStr += requiredDLMessage;
	            validator = false;
            }
            if (hasCoApp == 'Y') {
                nextPage = "#page6";
            } else {
                //make sure funding options are available --> skip funding page
                if ($("#divFundingTypeList a.btn-header-theme").length > 0 && xaProductFactory.hasFundedProducts()) {
                    nextPage = "#pageFS";
                } else {
                    nextPage = "#reviewpage";
                }
            }
            break;
    	/*case "CO_SCANDOCS":
    		validator = true;
		    errorStr = "";
        	// errorStr = validateRequiredDLScan($('#hdRequiredDLScan').val(), true);
	        scanAccept(true);
            nextPage = "#page6";
            break;
			*/
    	case "PAGE6":
    		validator = true;
    		if (co_ValidateApplicantInfoXA() === false) {
			    validator = false;
    		}
    		if (co_ValidateApplicantAddress() === false) {
    			validator = false;
    		}
		    if ($('#hdEnableOccupancyStatusSectionForCoApp').val() == 'Y') {
			    if (co_ValidateApplicantPreviousAddress() === false) {
				    validator = false;
			    }
		    }
		    if (co_ValidateApplicantMailingAddress() === false) {
    			validator = false;
    		}
    		if (co_ValidateApplicantContactInfoXA() === false) {
    			validator = false;
    		}
		    if ($('#hdEnableIDSectionForCoApp').val() == 'Y') {
			    if (co_ValidateApplicantIdXA() === false) {
				    validator = false;
			    }
		    }
		    if ($("#hdEnableEmploymentSectionForCoApp").val().toUpperCase() === "Y") {
            	if (co_ValidateApplicantEmploymentXA() === false) {
            		validator = false;
            	}
            }
		    if (typeof co_docUploadObj != "undefined" && co_docUploadObj != null && co_docUploadObj.validateUploadDocument() == false) {
            	validator = false;
            }
			if ($('#co_divApplicantQuestion').children().length > 0) {
				if ($.lpqValidate("co_ValidateApplicantQuestionsXA") === false) {
					validator = false;
				}
			}
            var requiredDLMessage = validateRequiredDLScan($('#hdRequiredDLScan').val(), true);
            if (requiredDLMessage != "") {
            	errorStr += requiredDLMessage;
	            validator = false;
            }
            //make sure funding options are available --> skip funding page
            if ($("#divFundingTypeList a.btn-header-theme").length > 0 && xaProductFactory.hasFundedProducts()) {
                nextPage = "#pageFS";
            } else {
                nextPage = "#reviewpage";
            }
            break;
        default:
            // This case should never happen.
            // If it does, then we should start the application process over
            $('#txtErrorMessage').html("An error occurred. Please start again from the beginning.");
            //nextpage = $('#page1');
            nextpage = $('#pageFom');
            $.mobile.changePage(nextpage, {
                transition: "slide",
                reverse: false,
                changeHash: true
            });

            return "Please start over";
    }

    // If there were errors, then programmatically click on the error dialog
    // otherwise, click on the link to the next screen

    if (normalClick == true) {
		if (validator === false) {
			if (errorStr !== "") {
				if ($("#divErrorPopup").length > 0) {
					$('#txtErrorPopupMessage').html(errorStr);
					$("#divErrorPopup").popup("open", { "positionTo": "window" });
				} else {
					$('#txtErrorMessage').html(errorStr);
					currElement.next().trigger('click');
				}
			} else {
				Common.ScrollToError();
			}
		} else {
			pushToPagePaths(nextPage);
			if (window.ABR) {
				var formValues = prepareAbrFormValues(activePage);
				if (ABR.FACTORY.evaluateRules(activePage, formValues)) {
					return false;
				}
			}
			$.mobile.changePage($(nextPage), {
				transition: "slide",
				reverse: false,
				changeHash: true
			});
		}
    }
    else {
        // If the validation was triggered by a swipe,
        // simply return the error message.  The swipe feature will
        // decide what to do with it.  It should bring up the error dialog.
        $('#txtErrorMessage').html(errorStr);
        return errorStr;
    }

}

function ViewApplicantInformation(hasCoApp) {
	$(".ViewAcountInfo").html(ViewAccountInfo());
	if (typeof viewBeneficiaries != "undefined") {
		var beneficiaryInfoHtml = viewBeneficiaries();
		$(".ViewBeneficiaries, .beneficiaryTitle").addClass("hidden");
		if (beneficiaryInfoHtml != "") {
			$(".ViewBeneficiaries").html(beneficiaryInfoHtml);
			$(".ViewBeneficiaries, .beneficiaryTitle").removeClass("hidden");
		}
	}
	$(".ViewContactInfo").html(ViewContactInfo());
	if ($('#hdEnableIDSection').val() == 'Y') {
		$(".ViewIdentification").html(ViewPrimaryIdentification.call($(".ViewIdentification")));
	}
	$(".ViewAddress").html(ViewAddress());
	if ($("#hdEnableEmploymentSection").val() === "Y") {
		$(".ViewEmploymentInfo").html(ViewEmploymentInfo.call($(".ViewEmploymentInfo")));
		$(".ViewPrevEmploymentInfo").html(ViewPrevEmploymentInfo.call($(".ViewPrevEmploymentInfo")));
	}
	if ($('#divApplicantQuestion').length > 0) {
		$(".ViewApplicantQuestion").html(ViewApplicantQuestion.call($(".ViewApplicantQuestion")));
	}
	//$(".ViewCustomQuestion").html(ViewCustomQuestion());
	//if (reviewPage = "#finalreviewpage") {
	if (hasCoApp === "Y") {
		$('div[data-section-name="reviewJointApplicant"]').show();
		$(".co_ViewAcountInfo").html(co_ViewAccountInfo());
		$(".co_ViewContactInfo").html(co_ViewContactInfo());
		if ($('#hdEnableIDSectionForCoApp').val() == 'Y') {
			$(".co_ViewIdentification").html(co_ViewPrimaryIdentification.call($('.co_ViewIdentification')));
		}
		if ($('#co_divApplicantQuestion').length > 0) {
			$(".ViewJointApplicantQuestion").html(co_ViewApplicantQuestion.call($('.ViewJointApplicantQuestion')));
		}
		$(".co_ViewAddress").html(co_ViewAddress());
		if ($("#hdEnableEmploymentSectionForCoApp").val() === "Y") {
			$(".co_ViewEmploymentInfo").html(co_ViewEmploymentInfo.call($(".co_ViewEmploymentInfo")));
			$(".ViewJointApplicantPrevEmploymentInfo").html(co_ViewPrevEmploymentInfo.call($(".ViewJointApplicantPrevEmploymentInfo")));
		}
	} else {
		$('div[data-section-name="reviewJointApplicant"]').hide();
	}
}

//review Page section
function ViewInformationPage() {
	viewSelectedBranch();  //view selected branch
	$(".ViewProductSelection").html(xaProductFactory.viewProductSelection.call($(".ViewProductSelection")));
    $(".ViewProductCustomQuestion").html(xaProductFactory.viewProductCustomQuestion());
    $(".ViewFundingSource").html(ViewSelectedProducts() + ViewFundingSource());
}
function ViewMinorInformation() {
    $(".ViewMinorAcountInfo").html(m_ViewAccountInfo());
    $(".ViewMinorContactInfo").html(m_ViewContactInfo());
	if ($('#hdEnableIDSectionForMinor').val() == 'Y') {
		$(".ViewMinorIdentification").html(m_ViewPrimaryIdentification.call($(".ViewMinorIdentification")));
	}
	$(".ViewMinorAddress").html(m_ViewAddress());
	if ($("#hdEnableEmploymentSectionForMinor").val() === "Y") {
		$(".ViewMinorEmploymentInfo").html(m_ViewEmploymentInfo.call($(".ViewMinorEmploymentInfo")));
		$(".ViewMinorPrevEmploymentInfo").html(m_ViewPrevEmploymentInfo.call($(".ViewMinorPrevEmploymentInfo")));
	}
	if ($('#m_divApplicantQuestion').length > 0) {
	    $(".ViewMinorApplicantQuestion").html(m_ViewApplicantQuestion.call($(".ViewMinorApplicantQuestion")));
	}
}
function ClearReviewInfoXA(){
    if ($('#reviewPanels').length > 0) {
        //clear the Review Information 
        $(".ViewProductSelection").html('');
        $(".ViewProductCustomQuestion").html('');
        $(".ViewAcountInfo").html('');
        $(".ViewContactInfo").html('');
        $(".ViewIdentification").html('');
        $(".ViewAddress").html('');
        //$(".ViewApplicantQuestion").html('');
        //$(".ViewCustomQuestion").html('');
        $(".ViewFundingSource").html('');
        $(".ViewAddress").html('');
        $(".co_ViewAcountInfo").html('');
        $(".co_ViewContactInfo").html('');
        $(".co_ViewIdentification").html('');
        $(".co_ViewAddress").html('');
        //$("#co_ViewApplicantQuestion").html('');
        //clear minor applicant
        $(".ViewMinorAcountInfo").html('');
        $(".ViewMinorContactInfo").html('');
        if( $(".ViewMinorIdentification").length>0){
            $(".ViewMinorIdentification").html('');
        }
        $(".ViewMinorAddress").html('');
        if ($(".ViewMinorEmploymentInfo").length > 0) {
            $(".ViewMinorEmploymentInfo").html('');
        }
        if ($(".ViewEmploymentInfo").length > 0) {
            $(".ViewEmploymentInfo").html('');
        }
        if ($(".co_ViewEmploymentInfo").length > 0) {
            $(".co_ViewEmploymentInfo").html('');
        }

        if ($(".ViewPrevEmploymentInfo").length > 0) {
        	$(".ViewPrevEmploymentInfo").html('');
        }
        if ($(".ViewJointApplicantPrevEmploymentInfo").length > 0) {
        	$(".ViewJointApplicantPrevEmploymentInfo").html('');
        }
        if ($(".ViewMinorPrevEmploymentInfo").length > 0) {
        	$(".ViewMinorPrevEmploymentInfo").html('');
        }

        if ($(".ViewMinorApplicantQuestion").length > 0) {
            $(".ViewMinorApplicantQuestion").html('');
        }
    }
}

//end review page section
function validateAll(element) {
    // Validate all the screens again before submitting.
    var errorStr = "";
    var validator = true;
	// Validate custom questions
	if ($('#reviewpage_divApplicantQuestion').children().length > 0) {
		if ($.lpqValidate("reviewpage_ValidateApplicantQuestionsXA") === false) {
			validator = false;
		}
	}
    //errorStr += validateInitialDepositProducts(); //move to validateFS1
	//errorStr += validateFS(element, false);
    if ($("#divDisclosureSection.showfield-section").length == 0 || $("#divDisclosureSection.showfield-section").hasClass("hidden") == false) {
    	if ($("#hdEnableDisclosureSection").val() === "Y") {
    		//validate verify code
    		errorStr += validateVerifyCode();
    		if ($.lpqValidate("ValidateDisclosure_disclosures") == false) {
    			validator = false;
    		}
	    }
		
	}
	//refine error message to remove duplicate item
    var arrErr = errorStr.split("<br/>");
    var refinedErr = [];
    $.each(arrErr, function (i, el) {
    	el = $.trim(el);
    	if (el !== "" && $.inArray(el, refinedErr) === -1) refinedErr.push(el);
    });

    if (refinedErr.length > 0) {
    	validator = false;
    }
    if (validator === false) {
        if (refinedErr.length > 0) {
            if ($("#divErrorPopup").length > 0) {
                $('#txtErrorPopupMessage').html(refinedErr.join("<br/>"));
                $("#divErrorPopup").popup("open", { "positionTo": "window" });
            } else {
                var nextpage = $('#divErrorDialog');
                $('#txtErrorMessage').html(refinedErr.join("<br/>"));
                if (nextpage.length > 0) {
                    $.mobile.changePage(nextpage, {
                        transition: "slide",
                        reverse: false,
                        changeHash: true
                    });
                }
            }
        } else {
            //Common.ScrollToError(function () {
            //  isSubmittingXA = false;              
            //}); 
            Common.ScrollToError(); 
        }
        return false;
    } else {
    	pushToPagePaths("reviewpage");
    	if (window.ABR) {
    		var formValues = prepareAbrFormValues("reviewpage");
    		if (ABR.FACTORY.evaluateRules("reviewpage", formValues)) {
    			return false;
    		}
    	}
        // No errors, so we can proceed       
        saveUserInfo();
    	return false;
    }
}



function handleAccountCollapsibleClick(clicked_obj) {
    return;
    //var curElement = $(clicked_obj);
    //var selectedObjIDStr = clicked_obj;
    //var selectedElement = document.getElementById(selectedObjIDStr);
    //var idStrPos = selectedObjIDStr.search("requiredAccount");
    //if (idStrPos >= 0) {
    //    // This is a required account, so don't let the user close it
    //    //                selectedElement..collapsible({collapsed: false});
    //    $(selectedObjIDStr).trigger('expand');
    //    $(selectedObjIDStr).trigger('expand');
    //}
}
function saveUserInfo() {
    if (g_submit_button_clicked) return false;  
    //disable submit button before calling ajax to prevent user submit multiple times
    $('#reviewpage .btnSubmit').addClass('ui-disabled');
    isApplicationCompleted = false; //init isApplicationCompleted to false
    //var fundingType = $('#fsFundingType').val();
    //if (fundingType === "PAYPAL") {
    //    saveUserInfoWithPayPal();
    //} else {
        //$('#txtErrorMessage').html('');//useless
    var loanInfo = getApplicantInfo();
    loanInfo.Task = "SubmitLoan";
    loanInfo.HasSSOIDA = $('#hdHasSSOIDA').val();
    loanInfo = attachGlobalVarialble(loanInfo);
        
    //var fundingType = $('#fsFundingType').val();
    var fundingType = $('#fsSelectedFundingType').val();
    if (fundingType === "PAYPAL") {
        loanInfo.SelectedFundingSource = 'PAYPAL';
    }
    //save instaTouchPrefillComment
    $instaTouchComment = $('#hdInstaTouchPrefillComment');  
    if ($instaTouchComment.val() != undefined) {
        loanInfo.InstaTouchPrefillComment = $instaTouchComment.val();       
    } 
    $.ajax({
        type: 'POST',
        url: 'Callback.aspx',
        data: loanInfo,
        dataType: 'html',
        success: function (response) {
            //enable submit button after getting response
           // $('#reviewpage .btnSubmit').removeClass('ui-disabled');
             //clear instaTouchPrefill 
            if ($instaTouchComment.val() != undefined && $instaTouchComment.val() != "") {
                $instaTouchComment.val("");
                }
            if (response.toUpperCase().indexOf('MLERRORMESSAGE') > -1) {
                var decoded_message = $('<div/>').html(response).text(); //message maybe encoded in the config xml so need to decode
                //$('#txtErrorMessage').html(decoded_message);
                goToDialog(decoded_message);
                $('#reviewpage .btnSubmit').removeClass('ui-disabled');
            } else if (response.toUpperCase().indexOf('MLNOIDAUTHENTICATION') > -1) {
                    isApplicationCompleted = true;
                    goToLastDialog(response); // submit success message
                    //need to display response first before clear all inputs
                    //submit sucessfull so clear
                    // clearForms();                   
            } else if (response.toUpperCase().indexOf('WALLETQUESTION') > -1) {
                displayWalletQuestions(response);                 
            } else if (response.toUpperCase().indexOf('MLPAYPAL_CREATED') > -1) {
                window.location.href = JSON.parse(response).Info.URL;
            } else if (response.toUpperCase().indexOf('MLPAYPAL_FAILED') > -1) {
                goToLastDialog(JSON.parse(response).Info.APPROVED_MESSAGE); //preapproved message               
            } else if (response == "blockmaxapps") {
                goToNextPage("#pageMaxAppExceeded");
            } else {
                isApplicationCompleted = true;
                goToLastDialog(response); //preapproved message
                //need to display response first before clear all inputs
                //submit sucessfull so clear
                //clearForms();
            }
        },
        error: function (err) {
            //enable submit button 
            $('#reviewpage .btnSubmit').removeClass('ui-disabled');
            //$('#txtErrorMessage').html('We apologize, but unfortunately there was an error when submitting the application. Please try again later.');
            goToDialog('We apologize, but unfortunately there was an error when submitting the application. Please try again later.');
        }
    });
    //}
        //hasClickedSubmitBtn(window.bHasCoApp);
    return false;
}


// Use this function to submit data for an application without having to type in all the data
//function submitTestData() {
//    $('#txtErrorMessage').html('');
//    var loanInfo = getTestLoanInfo();
//    loanInfo = attachGlobalVarialble(loanInfo);
//    $.ajax({
//        type: 'POST',
//        url: 'Callback.aspx',
//        data: loanInfo,
//        dataType: 'html',
//        success: function (response) {
//            if (response.toUpperCase().indexOf('MLERRORMESSAGE') > -1) {
//                //$('#txtErrorMessage').html(response);
//                goToDialog(response);
//            } else {
//                if (response.toUpperCase().indexOf('MLNOIDAUTHENTICATION') > -1) {
//                    isApplicationCompleted = true;
//                    goToLastDialog(response);
//                }
//                else {
//                    displayWalletQuestions(response);
//                }
//            }
//        },
//        error: function (err) {
//            //$('#txtErrorMessage').html('We apologize, but unfortunately there was an error when submitting the application. Please try again later.');
//            goToDialog('We apologize, but unfortunately there was an error when submitting the application. Please try again later.');
//        }
//    });
//    return false;
//}

function submitWalletAnswers() {

    //disable submit button before calling ajax to prevent user submit multiple times
    $('#walletQuestions .btnSubmitAnswer').addClass('ui-disabled');
    isApplicationCompleted = false; //init isApplicationCompleted to false

    //$('#txtErrorMessage').html('');//useless
    var answersObj = getWalletAnswers();
    FS1.loadValueToData();
    answersObj.rawFundingSource = JSON.stringify(FS1.Data);
    answersObj = attachGlobalVarialble(answersObj);
    //var fundingType = $('#fsFundingType').val();
    var fundingType = $('#fsSelectedFundingType').val();
    if (fundingType === "PAYPAL") {
        answersObj.SelectedFundingSource = 'PAYPAL';
    }
    $.ajax({
        type: 'POST',
        url: 'Callback.aspx',
        data: answersObj,
        dataType: 'html',
        success: function (response) {
            if (response.toUpperCase().indexOf('MLERRORMESSAGE') > -1) {
                $('#walletQuestions .btnSubmitAnswer').removeClass('ui-disabled');
                var decoded_message = $('<div/>').html(response).text(); //message maybe encoded in the config xml so need to decode
                //$('#txtErrorMessage').html(decoded_message);
                goToDialog(decoded_message);
            }
            else {
                if (response.toUpperCase().indexOf('MLNOIDAUTHENTICATION') > -1) {
                    isApplicationCompleted = true;
                    goToLastDialog(response);
                    //need to display response first before clear all inputs
                    // clearForms();               
                }             
                else if (response.toUpperCase().indexOf('WALLETQUESTION') > -1) {               
                    displayWalletQuestions(response,true);  //TODO: testing to use the same page for both join and follow on question
                    //co_displayWalletQuestions(response);      
                }
                else if (response.toUpperCase().indexOf('MLPAYPAL_CREATED') > -1) {
                    window.location.href = JSON.parse(response).Info.URL;
                }
                else if (response.toUpperCase().indexOf('MLPAYPAL_FAILED') > -1) {  //fail paypal        
                    goToLastDialog(JSON.parse(response).Info.APPROVED_MESSAGE); //preapproved message         
                }
                else {
                    isApplicationCompleted = true;
                    goToLastDialog(response);
                    //need to display response first before clear all inputs
                    // clearForms();
                }
            }
        },
        error: function (err) {
            //enable submit answer button 
            $('#walletQuestions .btnSubmitAnswer').removeClass('ui-disabled');
        	goToDialog('We apologize, but unfortunately there was an error when submitting the application. Please try again later.');
        }
    });
    return false;
}

//getCustom Question 
function getXACustomQuestions() {
    var XAQuestionAnswer = new Object;
    var XAQuestionAnswers = new Array();
    if ($('.CustomQuestion').length) {
        $('.CustomQuestion:not(span)').each(function () {
        	var element = $(this);
        	if (element.is(":hidden")) {
        		return; // abort current element and move to next
        	}
            var ui_type = element.attr('type');
            if (ui_type == "checkbox") {
                if (element.prev('.ui-checkbox-on').length > 0) {

                    XAQuestionAnswer = { q: $(this).attr('iname'), a: element.attr('answer') };
                    XAQuestionAnswers.push(XAQuestionAnswer);
                }
            }
            else {
                XAQuestionAnswer = { q: $(this).attr('iname'), a: element.val() };
                XAQuestionAnswers.push(XAQuestionAnswer);
            }

        });
    }
    //condition custom question
    if ($('.ConditionedQuestion').length) {
        $('.ConditionedQuestion:not(span)').each(function () {
            var element = $(this);
            if (element.is(":hidden")) {
            	return; // abort current element and move to next
            }
            var ui_type = element.attr('type');
            if (ui_type == "checkbox") {
                if (element.prev('.ui-checkbox-on').length > 0) {
                    XAQuestionAnswer = { q: $(this).attr('iname'), a: element.attr('answer') };
                    XAQuestionAnswers.push(XAQuestionAnswer);
                }
            } else {
            	XAQuestionAnswer = { q: $(this).attr('iname'), a: element.val() };
            	XAQuestionAnswers.push(XAQuestionAnswer);
            }
        });
    }
    return XAQuestionAnswers;
}

// This function fills out an object with preset application info
//function getTestLoanInfo() {
//    // Bundle up the information so that we can pass it along to the submission function.
//    var appInfo = new Object();

//    // Accounts (Interested Products)
//    appInfo.Account1Name = "PS";
//    appInfo.Account1Deposit = 10.0;
//    appInfo.Account2Name = "NOT SET";
//    appInfo.Account2Deposit = 0;
//    appInfo.Account3Name = "NOT SET";
//    appInfo.Account3Deposit = 0;

//    // Applicant Info
//    var txtMemberNumber = "1234567";
//    var txtFName = "Marisol";
//    var txtMName = "";
//    var txtLName = "Testcase";
//    var txtSuffix = "";
//    var txtGender = "FEMALE";
//    var txtDOB;
//    var txtMonth = "March";
//    var txtDay = "21";
//    var txtYear = "1992";
//    var txtMotherMaidenName = "Smith";
//    var txtProfession = "Governor";
//    var ddlCitizenshipStatus = "US CITIZEN";
//    var txtSSN = "000000001";

//    appInfo.LenderRef = $('#hfLenderRef').val();
//    appInfo.FirstName = txtFName;
//    appInfo.MiddleName = txtMName;
//    appInfo.LastName = txtLName;
//    appInfo.NameSuffix = txtSuffix;
//    appInfo.Gender = txtGender;
//    txtDOB = txtYear + "-" + convertMonthWordToNumber(txtMonth) + "-" + txtDay;
//    appInfo.DOB = txtDOB;
//    appInfo.MotherMaidenName = txtMotherMaidenName;
//    appInfo.Profession = txtProfession;
//    appInfo.EmploymentStatus = "EMPLOYED";
//    appInfo.EmployedMonths = "24";
//    appInfo.Employer = "US Government";
//    appInfo.Citizenship = ddlCitizenshipStatus;
//    appInfo.SSN = txtSSN;
//    appInfo.MemberNumber = txtMemberNumber;
//    // CO-APPLICANT
//    appInfo.co_FirstName = "David";
//    appInfo.co_MiddleName = "";
//    appInfo.co_LastName = "Testcase";
//    appInfo.co_NameSuffix = "";
//    appInfo.co_Gender = "MALE";
//    txtDOB = txtYear + "-" + convertMonthWordToNumber(txtMonth) + "-" + txtDay;
//    appInfo.co_DOB = "1980-08-22";
//    appInfo.co_MotherMaidenName = "Johnson";
//    appInfo.co_Profession = "Gunner";
//    appInfo.co_EmploymentStatus = "EMPLOYED";
//    appInfo.co_EmployedMonths = "24";
//    appInfo.co_Employer = "US Government";
//    appInfo.co_Citizenship = "US CITIZEN";
//    appInfo.co_SSN = "000000002";

//    // Applicant Contact Info
//    var txtEmail = "marisol@testing.org";
//    var txtHomePhone = "0000000000";
//    var txtMobilePhone = "";
//    var txtWorkPhone = "";
//    var txtWorkPhoneEXT = "";
//    var ddlContactMethod = "HOME";
//    appInfo.EmailAddr = txtEmail;
//    appInfo.HomePhone = txtHomePhone;
//    appInfo.MobilePhone = txtMobilePhone;
//    appInfo.WorkPhone = txtWorkPhone;
//    appInfo.WorkPhoneEXT = txtWorkPhoneEXT;
//    appInfo.ContactMethod = ddlContactMethod;
//    // co app
//    appInfo.co_EmailAddr = "david@testing.org";;
//    appInfo.co_HomePhone = txtHomePhone;
//    appInfo.co_MobilePhone = txtMobilePhone;
//    appInfo.co_WorkPhone = txtWorkPhone;
//    appInfo.co_WorkPhoneEXT = txtWorkPhoneEXT;
//    appInfo.co_ContactMethod = ddlContactMethod;

//    // Applicant ID
//    var ddlIDCardType = "BIRTH_CERT";
//    var txtIDCardNumber = "123123";
//    var txtIDDateIssued = "1980-01-01";
//    var txtIDDateExpire = "2013-12-12";
//    var ddlIDState = "";
//    var ddlIDCountry = "";
//    appInfo.IDCardType = ddlIDCardType;
//    appInfo.IDCardNumber = txtIDCardNumber;
//    appInfo.IDDateIssued = txtIDDateIssued;
//    appInfo.IDDateExpire = txtIDDateExpire;
//    appInfo.IDState = ddlIDState;
//    appInfo.IDCountry = ddlIDCountry;
//    // Co app
//    appInfo.co_IDCardType = ddlIDCardType;
//    appInfo.co_IDCardNumber = "99999";
//    appInfo.co_IDDateIssued = txtIDDateIssued;
//    appInfo.co_IDDateExpire = txtIDDateExpire;
//    appInfo.co_IDState = ddlIDState;
//    appInfo.co_IDCountry = ddlIDCountry;

//    // Applicant Address
//    var txtAddress = "1234 Nowhere Lane";
//    var txtZip = "92660";
//    var txtCity = "Newport Beach";
//    var ddlState = "CA";
//    appInfo.AddressStreet = txtAddress;
//    appInfo.AddressZip = txtZip;
//    appInfo.AddressCity = txtCity;
//    appInfo.AddressState = ddlState;
//    // co app
//    appInfo.co_AddressStreet = txtAddress;
//    appInfo.co_AddressZip = txtZip;
//    appInfo.co_AddressCity = txtCity;
//    appInfo.co_AddressState = ddlState;

//    return appInfo;
//}

//this create selectProducts object
function selectProducts(productName, depositAmount, productQuestions, productServices) {
    this.productName = productName;
    this.depositAmount = depositAmount;
    this.productQuestions = productQuestions;
    this.productServices = productServices ? productServices : [];
}

function goToDialog(message) {
	if ($("#divErrorPopup").length > 0) {
		$('#txtErrorPopupMessage').html(message);  //error message are not encoded, endoding will remove html tag
		$("#divErrorPopup").popup("open", { "positionTo": "window" });
	} else {
		$('#txtErrorMessage').html(message);  //error message are not encoded, endoding will remove html tag
		$('#href_show_dialog_1').trigger('click');
	}
}
function goToLastDialog(message) {
    //already decode message in xa callback code behind
    //var decoded_message = $('<div/>').html(message).text(); //message maybe encoded in the config xml so need to decode
    //get the dropdown funding source type
    //var ddlFundingSource = $('#divFundingSource option:selected').val();
    var fundingType = $('#fsSelectedFundingType').val();
    if (isApplicationCompleted) {
        if (fundingType == "MAIL A CHECK" && $('#hdCheckMailAddress').val() != "") {
            document.getElementById("divCheckMailAddress").style.display = "block";
        }
        //$('#txtTitleMessage').html("Application Completed");
        //$('#strResponseMessage').html("Application Completed");
        //clear all user inputs after application is completed
        clearForms();
    } else {
        //$('#txtTitleMessage').html("System Message");
        //$('#strResponseMessage').html("System Message");
    }
    //$('#div_system_message').html(decoded_message);
    $('#div_system_message').html(message);

    //docusign window if enabled
    if ($('#div_system_message').find("iframe#ifDocuSign").length === 1) {
        appendDocuSign();
        $("#div_docuSignIframe").append($('#div_system_message').find("iframe#ifDocuSign"));

        ////goback to final decision message after docusign is closed
        //$('#div_docuSignIframe').find("iframe#ifDocuSign").on("load", function (evt) {
        //    //the initial page load twice so need this to make sure only capturing url on finish
        //    var flag = ($(this).data("loaded") || "") + "1";
        //    $(this).data("loaded", flag);
        //    if (flag === "111") {
        //        $('#href_show_last_dialog').trigger('click');
        //    }
        //});

        var height = window.innerHeight || document.documentElement.clientHeight || document.body.clientHeight;

        var footerHeight = $('.divfooter').height();

        height = height - footerHeight - 64;
        $('#div_docuSignIframe').find("iframe#ifDocuSign").attr("height", height);
        $.mobile.changePage("#popupDocuSign", {
            transition: "fade",
            reverse: false,
            changeHash: false
        });
    }
        //cross-sell link button if enabled
    else {
        $(".xsell-selection-panel .xsell-item-option.btn-header-theme", "#div_system_message").on("mouseenter click", function (ev) {
            var element = $(this);
            if (ev.type != 'click') {
                element.addClass('btnHover');
                element.removeClass('btnActive_on');
                element.removeClass('btnActive_off');
            } else {
                element.removeClass('btnHover');
            }
        }).on("mouseleave", function () {
            handledBtnHeaderTheme($(this));
        });
        $('#href_show_last_dialog').trigger('click');
    }

}

function displayWalletQuestions(message, isCoApp) {
    if (isCoApp == undefined) {
        isCoApp = false;
    }
    // The first two characters of the message contains the number of questions
    var numQuestions = parseInt(message.substr(0, 2), 10);
    var questionsHTML = message.substr(2, message.length - 2);

    // Put the number of questions into the hidden input field
    $('#hdNumWalletQuestions').val(numQuestions);
    numWalletQuestions = numQuestions;

    // Put the HTML for the questions into the proper location in our page
    $('#walletQuestionsDiv').html('');
    $('#walletQuestionsDiv').html(questionsHTML);
    $('#walletQuestionsDiv').trigger("create");  //need to have this for 2nd time usage so jQM can render the style
    var nextpage = $('#walletQuestions');
    if (isCoApp) { //scroll top for co wallet question
        $('html, body').scrollTop(nextpage.offset().top);
        if ($('#walletQuestions .btnSubmitAnswer').hasClass('ui-disabled')) {
            $('#walletQuestions .btnSubmitAnswer').removeClass('ui-disabled'); //need to enable for join or followon
        }
    }
    $.mobile.changePage(nextpage, {
        transition: "slide",
        reverse: false,
        changeHash: true
    });
}

function autoFillData() {
    $('#ddlBranchName option').eq(1).prop('selected', true);
    $('#ddlBranchName').selectmenu("refresh").closest("div.ui-btn").addClass("btnActive_on");
  
    if (typeof autoFillData_PersonalInfor == "function") {
    	autoFillData_PersonalInfor();
    }
    if (typeof co_autoFillData_PersonalInfor == "function") {
    	co_autoFillData_PersonalInfor();
    }
    if (typeof m_autoFillData_PersonalInfor == "function") {
    	m_autoFillData_PersonalInfor();
    }

    if (typeof autoFillData_ID == "function") {
    	autoFillData_ID();
    }
    if (typeof co_autoFillData_ID == "function") {
    	co_autoFillData_ID();
    }
    if (typeof m_autoFillData_ID == "function") {
    	m_autoFillData_ID();
    }

    //// minor Applicant Address
    //$('#m_txtAddress').val("123 west");
    //$('#m_txtZip').val("92841");
    //$('#m_txtCity').val("Garden grove");
    //$('#m_ddlState').val("CA");
    if (typeof co_autoFillData_ContactInfor == "function") {
    	co_autoFillData_ContactInfor();
    }
    if (typeof autoFillData_ContactInfor == "function") {
    	autoFillData_ContactInfor();
    }
    if (typeof m_autoFillData_ContactInfor == "function") {
    	m_autoFillData_ContactInfor();
    }

    if (typeof autoFillData_EmploymentInfo == "function") {
    	autoFillData_EmploymentInfo();
    }
    if (typeof co_autoFillData_EmploymentInfo == "function") {
    	co_autoFillData_EmploymentInfo();
	}
	if (typeof m_autoFillData_EmploymentInfo == "function") {
    	m_autoFillData_EmploymentInfo();
	}

	if (typeof autoFillData_Address == "function") {
		autoFillData_Address();
	}
	if (typeof co_autoFillData_Address == "function") {
		co_autoFillData_Address();
	}
	if (typeof m_autoFillData_Address == "function") {
		m_autoFillData_Address();
	}
	if (typeof autofill_Disclosure == "function") {
		autofill_Disclosure();
	}
}

var BASE64_MARKER = ';base64,';

function convertDataURIToBinary(dataURI) {
    var base64Index = dataURI.indexOf(BASE64_MARKER) + BASE64_MARKER.length;
    var base64 = dataURI.substring(base64Index);
    var raw = window.atob(base64);
    var rawLength = raw.length;
    var array = new Uint8Array(new ArrayBuffer(rawLength));

    for (i = 0; i < rawLength; i++) {
        array[i] = raw.charCodeAt(i);
    }
    return array;
}

//check date string from this format:
//		MM-YYYY
function checkMonthYear(date) {
    var minYear = 1902;
    var maxYear = (new Date()).getFullYear() + 20;
    if ($.trim(date) == '') return false;

    // regular expression to match required date format
    re = /^(\d{1,2})-(\d{4})$/;

    if (date != '') {
        if (regs = date.match(re)) {
            if (regs[1] < 1 || regs[1] > 12) {
                return false;
            } else if (regs[2] < minYear || regs[2] > maxYear) {
                return false;
            }
        } else {
            return false;
        }
    }
    return true;
}
function getUserName() {
    var txtFName = $('#txtFName').val();
    var txtMName = $('#txtMName').val();
    var txtLName = $('#txtLName').val();
    //capitalize the first character
    txtFName = txtFName.replace(txtFName.charAt(0), txtFName.charAt(0).toUpperCase());
    txtMName = txtMName.replace(txtMName.charAt(0), txtMName.charAt(0).toUpperCase());
    txtLName = txtLName.replace(txtLName.charAt(0), txtLName.charAt(0).toUpperCase());
    var userName = "";
    if (txtMName != "")
        userName = txtFName + " " + txtMName + " " + txtLName;
    else
        userName = txtFName + " " + txtLName;
    return userName;
}



function convertMonthWordToNumber(monthWord) {
    // Make sure that month is a string
    monthWord = String(monthWord);

    switch (monthWord.toUpperCase()) {
        case "JANUARY":
            return "01";
        case "FEBRUARY":
            return "02";
        case "MARCH":
            return "03";
        case "APRIL":
            return "04";
        case "MAY":
            return "05";
        case "JUNE":
            return "06";
        case "JULY":
            return "07";
        case "AUGUST":
            return "08";
        case "SEPTEMBER":
            return "09";
        case "OCTOBER":
            return "10";
        case "NOVEMBER":
            return "11";
        case "DECEMBER":
            return "12";
        default:
            // It's always January if we can't figure out what it is
            return "01";
    }
}
//

///////////////////
// FUNDING SOURE //
///////////////////
function handleFSFooter() {
    var divLogoHeight = $('.divOtherLogo').height();
    var divDepositInputHeight = $('#DivDepositInput').height();
    var element = $('#pageFS');
    var screenContentHeight = $(window).height() - 82;
    $('#divFundingSource').on('change', function (e) {
        var elementID = e.target.id;
        if (elementID == 'fsFundingType') {
            var divFSHeight = $(this).height();
            var fsContentHeight = divLogoHeight + divDepositInputHeight + divFSHeight;
            if (element.attr('id') != "divErrorDialog") {
                if (fsContentHeight < screenContentHeight) {
                    element.find('div[data-role="content"]').height(screenContentHeight);
                } else {
                    element.find('div[data-role="content"]').height(fsContentHeight + 82);
                }
                return false;
            }
        }
    });
}

//Moved to xa_funding_controller
//function LookupRoutingNumber(num, callback, failedCallback) {
//	$.ajax({
//        type: 'POST',
//        url: '/Handler/Handler.aspx',
//        data: { command: 'LookupRoutingNumber', routing_num: num, previewCode: $("#hdPreviewCode").val() },
//        dataType: 'json',
//        async: true,
//        success: function (response) {
//            if (response.IsSuccess) {
//                if ($.isFunction(callback)) {
//                    callback(response.Info.BankName, response.Info.State);
//                }
//            } else {
//                //$('#txtErrorMessage').html(response.Message);
//                //goToNextPage("#divErrorDialog");
//                goToDialog(response.Message);
//                if ($.isFunction(failedCallback)) {
//                    failedCallback();
//                }
//                //alert(response.Message);
//            }
//        }
//    });
//}

    
//------special account type ---------
    function validateSpecialAccountType() {
            if ($('select#ddlSpecialAccountType').length > 0) {
                $('#divSpecialAccountType').observer({
                    validators: [
                        function (partial) {
                            var mAccountTypeObj = getMinorAccountTypeObj();
                            if (mAccountTypeObj.isMinor == "MINOR") {
                                if (mAccountTypeObj.mAccountType == "") {
                                    return "Please select an account type.";
                                }
                            }
                            return "";
                        }
                    ],
                    validateOnBlur: true,
                    group: "ValidateSpecialAccountType"
                });
       }
    }
    function setSpecialAccountType(appInfo) {
        var mAccountTypeObj = getMinorAccountTypeObj();
        var minorAccountList = MINORACCOUNTLIST;
        var minorRoleType = "";
        var secondRoleType = "";
        if ($('#hdHasMinorApplicant').val() == "Y") {
            for (var i = 0; i < minorAccountList.length; i++) {
                if (minorAccountList[i].minorAccountCode == mAccountTypeObj.mAccountType) {
                    minorRoleType = minorAccountList[i].minorRoleType;
                    secondRoleType = minorAccountList[i].secondRoleType;
                    break;
                }
            }
            appInfo.MinorRoleType = minorRoleType;
            appInfo.SecondRoleType = secondRoleType;
            appInfo.MinorAccountCode = mAccountTypeObj.mAccountType;
        }
    }
    function isSpecialAccount() {
    	var availability = Common.GetParameterByName("type");
        if (availability == "1a" || availability == "2a") {
            return true;
        }
        return false;
    } 
    function hasSecRoleEmployment() {    
        var mAccountTypeObj = getMinorAccountTypeObj();
        var hasEmployment = "";
        for (var i = 0; i < MINORACCOUNTLIST.length; i++) {
            if (mAccountTypeObj.mAccountType == MINORACCOUNTLIST[i].minorAccountCode) {
                hasEmployment = MINORACCOUNTLIST[i].employment;
                break;
            }
        }
        return hasEmployment;
    }
    function getMinorAccountTypeObj() {
        var accountTypeElem = $('#ddlSpecialAccountType');
        var selectedAccountType = "";
        var strMinor = "";
        function minorObj(minorAccountType, isMinor) {
            this.mAccountType = minorAccountType;
            this.isMinor = isMinor;
        }
        if (accountTypeElem.find('option').length > 0) {
            selectedAccountType = accountTypeElem.find('option:selected').val();
        } else {
            if ($('#hdMinorAccountCode').length > 0 && accountTypeElem.hasClass('active')) {
                selectedAccountType = $('#hdMinorAccountCode').val();
            }
        }
        if (isSpecialAccount()) {
            strMinor = "MINOR";
        } 
        return new minorObj(selectedAccountType, strMinor);
    }
    function isHiddenSelectedMinorProduct(chkProductID) {
        if (isSpecialAccount()) {
            if(chkProductID.prev().hasClass('ui-checkbox-on')){
                var divChkElement = $(chkProductID).parent().parent().parent().attr('style');
                if (!$(chkProductID).parent().hasClass('ui-checkbox')) {
                    divChkElement = $(chkProductID).parent().parent().attr('style');
                }
                if (divChkElement !=undefined && divChkElement.indexOf('block')==-1) {
                   return true;
               }
            }
        }
        return false;
}

$(function () {    
    if ($('#hdHasZipCodePool').val() == "Y" && !$.isEmptyObject(BRANCHZIPCODEPOOLNAME)) {
        $('option', $('#ddlBranchName')).each(function (id, element) {
            InitBranchOptions.push($(element));
        });
    }
     if ($('#hdUrlZipMode').val() != undefined && $('#hdUrlZipMode').val() != "") {
        var $zipEle = $("#txtLocationPoolCode");
        $zipEle.val($('#hdUrlZipMode').val()); //update zip code value
        $zipEle.closest('div[data-role="fieldcontain"]').addClass('hidden'); //hide zip code field
        $zipEle.closest('div[data-role="fieldcontain"]').prev('div').addClass('hidden'); // hide zip code title
        $(window).on('load', function () {
            $zipEle.trigger('paste'); 
        });
      }
     
        //$('#popUpAccountType').on('click', 'a[data-role="button"]', function () {
        //    $.lpqValidate("ValidateProductSelection");
        //});
      //  handledSpecialAccountType();
        //handle disable disclosures 
        $("div[name='disclosures'] label").click(function () {
            handleDisableDisclosures(this);
        });
        
        //show special account button if it exist
        if (isSpecialAccount()) {
           // $('#divAccountTypeCol').removeClass('col-sm-6');
          //  $('#divBtnJointCol').removeClass('col-sm-6');
            $('#divAccountTypeCol').show();
        }
       
        
        //end handle selected account type dropdown: hover, selected/unselected
        initPage1();
        $(document).on("pageshow", function () {
            var curPageId = $.mobile.pageContainer.pagecontainer('getActivePage').attr('id');
            if (curPageId === "GettingStarted") {
                initPage1();

                if ($('#reviewpage .btnSubmit').hasClass('ui-disabled')) {
                    //enable submitAnswer 
                    $('#reviewpage .btnSubmit').removeClass('ui-disabled');
                }

                if ($('#walletQuestions .btnSubmitAnswer').hasClass('ui-disabled')) {
                    //enable submitAnswer 
                    $('#walletQuestions .btnSubmitAnswer').removeClass('ui-disabled');
                }
                
            }
           // if (curPageId === "walletQuestions") {
           //     isSubmitWalletAnswer = false;
           // }
           // if (curPageId === "reviewpage") {
          //      isSubmittingXA = false;
          //  }

            if (curPageId === "xa_last") {
                pageLastInit();
            }
            
           // $("#divErrorPopup").on("popupafterclose", function () {
            	//isSubmittingXA = false;
            //	isSubmitWalletAnswer = false;
            //});
        });
        $("#txtLocationPoolCode").observer({
        	validators: [
				function (partial) {
					if (/[0-9]{5}/.test($(this).val()) === false) {
						return "Please enter valid zip code";
					}
					return "";
				}
        	],
        	validateOnBlur: true,
        	group: "ValidateLocationPoolCode"
        });
        $("#txtLocationPoolCode").on("keyup paste", function () {
        	var $self = $(this);
        	setTimeout(function () {
        		if (/[0-9]{5}/.test($self.val())) {
        			
        			filterBranchDropdown($self.val());
        			xaProductFactory.setLocationPoolCode("");
        			if (hasZipPoolIDs($self.val())) {
                    	xaProductFactory.setLocationPoolCode($self.val());
                        var uniqueZipPoolIDs = UniqueZipPoolIDs(); 
                        var includeProductCodes = []; //store all product codes which have data-zipcode-filter-type="INCLUDE" and available in current zip pool location
                        var excludeProductCodes = []; //store all product codes which have data-zipcode-filter-type="EXCLUDE" and available in current zip pool location.
                  
       		           $.each(ZIPCODEPOOLLIST[$self.val()], function (idx, zcpid) {
        		        	if ($.inArray(zcpid, uniqueZipPoolIDs) == -1) {
        		                return true; //continue the next zcpid
                            }
                            var poolNames = getSelectedZipPoolNames($self.val(), ZIPCODEPOOLLIST, ZIPCODEPOOLNAME);                        
        		        	$("div[data-zipcode-filter-type='INCLUDE']").each(function (idxIncl, prodItem) {
								var $prodItem = $(prodItem);
								var productCode = $prodItem.data("product-code");
                                var pItem = xaProductFactory.getProductItemByCode(productCode);
                                if ($.inArray(productCode, includeProductCodes) > -1) {
                                   //if productCode is in includeProductCodes the product is already checked (it is visible product)
                                   //in the previous zip pool id, so skip the to check the product again.
                                    return true; 
                                }
                                if (pItem && zcpid === $prodItem.data("zipcode-pool-id")) { 
                                    //the product is already checked (it is visible product), and it has zip pool id, so add productCode to the includeProductCodes.                             
                                    //the includeProductCode is used to determine to check/skip products for next zip pool id iteration
                                    includeProductCodes.push(productCode);  
									if (pItem.ProductRates && pItem.ProductRates.length > 0) {
										var hasValidRate = false;
										if (poolNames.length > 0) {
											_.forEach(pItem.ProductRates, function (rateItem) {
												if (rateItem.ZipCodeFilterType == "ALL"
													|| (rateItem.ZipCodeFilterType == "INCLUDE" && _.indexOf(poolNames, rateItem.LocationPool) > -1)
													|| (rateItem.ZipCodeFilterType == "EXCLUDE" && _.indexOf(poolNames, rateItem.LocationPool) == -1)) {
													hasValidRate = true;
													return false;
												}
											});
										}
										if (hasValidRate) {
											$prodItem.removeClass("hidden zp-hidden");
										} else {
											$prodItem.addClass("hidden zp-hidden");
										}
										xaProductFactory.removeSelectedProductByProductCode(productCode);
									} else {
										$prodItem.removeClass("hidden zp-hidden");
									}
								} else {
									$prodItem.addClass("hidden zp-hidden");
									xaProductFactory.removeSelectedProductByProductCode(productCode);
								}
							});
        		            $("div[data-zipcode-filter-type='EXCLUDE']").each(function (idxExcl, prodItem) {
        		            	var $prodItem = $(prodItem);
        		            	var productCode = $prodItem.data("product-code");
                                var pItem = xaProductFactory.getProductItemByCode(productCode);                              
                                if ($.inArray(productCode, excludeProductCodes) > -1) {
                                  //if productCode is in excludeProductCodes the product is already checked (it is hidden product)
                                  //in the previous zip pool id, so skip the to check the product again.
                                    return true; 
                                }
                                if (!pItem || zcpid === $prodItem.data("zipcode-pool-id")) {
                                    //the product is already checked(it is hidden product), so add productCode to the excludeProductCodes.
                                    //excludeProductCodes is used to determine to check/skip products for next zip pool id iteration
                                    excludeProductCodes.push(productCode);  
        		            		$prodItem.addClass("hidden zp-hidden");
        		            		xaProductFactory.removeSelectedProductByProductCode(productCode);
        		            	} else {
        		            		if (pItem.ProductRates && pItem.ProductRates.length > 0) {
        		            			var hasValidRate = false;
					                    if (poolNames.length > 0) {
					                    	_.forEach(pItem.ProductRates, function (rateItem) {
					                    		if (rateItem.ZipCodeFilterType == "ALL"
													|| (rateItem.ZipCodeFilterType == "INCLUDE" && _.indexOf(poolNames, rateItem.LocationPool) > -1)
													|| (rateItem.ZipCodeFilterType == "EXCLUDE" && _.indexOf(poolNames, rateItem.LocationPool) == -1)) {
					                    			hasValidRate = true;
								                    return false;
							                    }
					                    	});
					                    }
        		            			if (hasValidRate) {
        		            				$prodItem.removeClass("hidden zp-hidden");
        		            			} else {
        		            				$prodItem.addClass("hidden zp-hidden");
        		            			}
        		            			xaProductFactory.removeSelectedProductByProductCode(productCode);
									} else {
        		            			$prodItem.removeClass("hidden zp-hidden");
        		            		}
        		            	}
        		            });
        		            $("div[data-zipcode-filter-type='ALL']").each(function (idxExcl, prodItem) {
        		            	var $prodItem = $(prodItem);
        		            	var productCode = $prodItem.data("product-code");
        		            	var pItem = xaProductFactory.getProductItemByCode(productCode);
        		            	if (pItem) {
        		            		if (pItem.ProductRates && pItem.ProductRates.length > 0) {
        		            			var hasValidRate = false;
        		            			if (poolNames.length > 0) {
        		            				_.forEach(pItem.ProductRates, function (rateItem) {
        		            					if (rateItem.ZipCodeFilterType == "ALL"
													|| (rateItem.ZipCodeFilterType == "INCLUDE" && _.indexOf(poolNames, rateItem.LocationPool) > -1)
													|| (rateItem.ZipCodeFilterType == "EXCLUDE" && _.indexOf(poolNames, rateItem.LocationPool) == -1)) {
        		            						hasValidRate = true;
        		            						return false;
        		            					}
        		            				});
        		            			}
        		            			if (hasValidRate) {
        		            				$prodItem.removeClass("hidden zp-hidden");
        		            			} else {
        		            				$prodItem.addClass("hidden zp-hidden");
        		            			}
        		            			xaProductFactory.removeSelectedProductByProductCode(productCode);
									} else {
        		            			$prodItem.removeClass("hidden zp-hidden");
        		            		}
								} else {
									$prodItem.addClass("hidden zp-hidden");
									xaProductFactory.removeSelectedProductByProductCode(productCode);
								}
        		            });
        		        });
        		        prefilledAndDisabledZipCode($self.val());
                    } else if ($("div[data-zipcode-filter-type]").length > $("div[data-zipcode-filter-type='INCLUDE']").length) {
                    	//if there're products that's available for all zipcode (filter type is ALL or EXCLUDE), then make it visible
	                    $("div[data-zipcode-filter-type='EXCLUDE']").each(function(inxExcl, prodItem) {
		                    var $prodItem = $(prodItem);
		                    $prodItem.removeClass("hidden zp-hidden");
	                    });
	                    $("div[data-zipcode-filter-type='INCLUDE']").each(function(indIncl, prodItem) {
		                    var $prodItem = $(prodItem);
		                    $prodItem.addClass("hidden zp-hidden");
		                    xaProductFactory.removeSelectedProductByProductCode($prodItem.data("product-code"));
	                    });
	                    $("div[data-zipcode-filter-type='ALL']").each(function(idxExcl, prodItem) {
		                    $(prodItem).removeClass("hidden zp-hidden");
	                    });
	                    prefilledAndDisabledZipCode($self.val());
                    } else {
                    	$("div[data-zipcode-filter-type]").addClass("hidden zp-hidden");
                    }
                    if (isSpecialAccount()) {
	                    xaProductFactory.handleShowAndHideSpecialAccountProducts();
                    } else {
                    	//handle preselect products: if products are not available in a selected zip pool --> remove preselect products from selectedproductspool
                    	xaProductFactory.bindPreselectedProducts();
                    	xaProductFactory.toggleProductSelectionSectionVisibility();
                    }
                } else {
        		    $("#divLocationPool").next("div").addClass("hidden");
                }            
            }, 400);            
        });
		//for the first time, hide all products with zipcode filter =  INCLUDE
	    //$("div[data-zipcode-filter-type='INCLUDE']").each(function(idx, item) {
	    //	var $item = $(item);
	    //	$item.addClass("hidden");
		//	//if there're no visible items belongs to a specified product key, hide its corresponding button
		//    var $container = $item.closest("div[data-product-key]");
		//    if ($container.find("div[data-zipcode-filter-type].hidden").length == $container.find("div[data-command-type]").length) {
		//	    $("a[data-key='" + $container.data("product-key") + "']").addClass("hidden");
		//    } else {
		//    	$("a[data-key='" + $container.data("product-key") + "']").removeClass("hidden");
		//    }
	    //});
    	//if all products is with zipcode filter = INCLUDE, hide all other section until a valid zipcode entered
	    //if ($("div.product-selection-panel").find("div[data-zipcode-filter-type='INCLUDE']").length == $("div.product-selection-panel").find("div[data-command-type]").length) {
	    //	$("#divLocationPool").next("div").addClass("hidden");
	    //	$("div.product-selection-panel").closest("div[data-role='page']").find("a.div-continue-button").addClass("hidden");
	    //}
	    //branch name dropdown
        handleShowAndHideBranchNames();
});
//function registerValidateZipPoolProducts() {
//        $('#divRequiredProducts').observer({
//            validators: [
//                function (partial) {
//                    if (!xaProductFactory.lenderHasRequiredProducts()) return "";  
//                    return xaProductFactory.validateProductSelection();
//                }
//            ],
//            validateOnBlur: true,
//            group: "ValidateProductSelection"
//              });
 
//        $('#divAdditionalProducts').observer({
//            validators: [
//                function (partial) {
//                    if (xaProductFactory.lenderHasRequiredProducts()) return "";
//                    return xaProductFactory.validateProductSelection();
//                }
//            ],
//            validateOnBlur: true,
//            group: "ValidateProductSelection"
//         });    
//}



//get all unique zip pool id from downloading products
function UniqueZipPoolIDs() {
    var zipPoolIDList = [];
    $("div[data-command-type='optionBtn']").each(function () {
        var zcpid = $(this).attr('data-zipcode-pool-id');
        if (zcpid != undefined && zcpid != "" && $.inArray(zcpid, zipPoolIDList) == -1) {
        	zipPoolIDList.push(zcpid);
        }
    });
    return zipPoolIDList;
}
function hasZipPoolIDs(zip) {
	var result = false;
	var uniqueZipPoolIDs = UniqueZipPoolIDs();
    if (ZIPCODEPOOLLIST[zip]) {
        $.each(ZIPCODEPOOLLIST[zip], function (idx, zcpid) {
        	if ($.inArray(zcpid, uniqueZipPoolIDs) > -1) {
            	result = true;
                return false; //exit the loop
            }
        });
    }
    return result;
}
function prefilledAndDisabledZipCode(sValue) {
    $("#" + PRIMARYAPPLICANTINFOPREFIX + "txtZip").val(sValue);
    $("#" + PRIMARYAPPLICANTINFOPREFIX + "txtCity").val("");
    $("#" + PRIMARYAPPLICANTINFOPREFIX + "txtZip").attr("disabled", "disabled");
    $("#" + PRIMARYAPPLICANTINFOPREFIX + "txtZip").trigger("change");
}
function filterBranchDropdown(zip) {  
    if ($.isEmptyObject(BRANCHZIPCODEPOOLNAME)) return; //no zip pool name in the branch 
    if ($.isEmptyObject(ZIPCODEPOOLNAME)) return; //no zip pool name from download lenderServiceConfigInfo 
    var selectedPoolNames = getSelectedZipPoolNames(zip, ZIPCODEPOOLLIST, ZIPCODEPOOLNAME);
    var filterBranchIds = [];
    for (var key in BRANCHZIPCODEPOOLNAME) {
        if ($.inArray(BRANCHZIPCODEPOOLNAME[key], selectedPoolNames) > -1) {
            if ($.inArray(key, filterBranchIds) == -1) {
                filterBranchIds.push(key);
            }          
        }       
    }    
    var $branchName = $('#ddlBranchName');
    $branchName.children().not('option:first').remove();
    for (var i = 0; i<InitBranchOptions.length; i++) {
        var branchOption = InitBranchOptions[i];
        if ($.inArray(branchOption.attr('value'), filterBranchIds) > -1) {
            branchOption.appendTo($branchName);
        }
    }
    if ($branchName.children().length <= 1) {
        $('#divBranchSection').addClass('hidden');
    } else {
        $('#divBranchSection').removeClass('hidden');
    }
    $branchName.val($('#ddlBranchName option:first').val()).trigger('change'); //refresh branch dropdown when the zipcode change   
}
//end filter branch dropdown
function initPage1() {
    //handle selected Joint Applicant button
    $('#btnHasCoApplicant').off("click").on('click', function () {
    	var $self = $(this);
    	if ($self.attr("contenteditable") == "true") return false;
        if ($("#hdHasCoApplicant").val() === "N") {
            $("#hdHasCoApplicant").val("Y");
        } else {
            $("#hdHasCoApplicant").val("N");
        }
        $self.toggleClass("active");
        handledBtnHeaderTheme($(this));
    });
}

function pageLastInit() {
    if ($("#divXsellSection").length === 1) {
        $("a.btn-header-theme", $("#divXsellSection")).on("click", function () {
            $.mobile.loading("show", {
                theme: "a",
                text: "Loading ... please wait",
                textonly: true,
                textVisible: true
            });
        });
    }
}

function goBack(ele, scrollToEle, toPage) {
	var $ele = $(ele);
	var pageId = $ele.closest("div[data-role='page'],div[data-role='dialog']").attr("id");
	switch (pageId) {
		case "pageMinorInfo":
			goToNextPage("#GettingStarted");
			break;
		case "page2":
			if ($('#hdHasMinorApplicant').val() == 'Y') {
				goToNextPage("#pageMinorInfo");
			} else if ($("#pagesa_AccInfo").length > 0) {
				goToNextPage("#pagesa_AccInfo");
			} else {
				goToNextPage("#GettingStarted");
			}
			break;
		case "page6":
			goToNextPage("#page2");
			break;
		case "productConfirm":
			goToNextPage("#GettingStarted");
			break;
		case "pageFS":
			resetFunding();
			//history.back();
		    if (toPage && toPage != "") {
		        goToNextPage("#" + toPage);
		    } else if ($('#hdHasCoApplicant').val() == 'Y') {
		        goToNextPage("#page6");
		    } else {
		        goToNextPage("#page2");
		    }
			break;
		case "reviewpage":
	        if ($("#divFundingTypeList a.btn-header-theme").length > 0 && xaProductFactory.hasFundedProducts()) {
	            goToNextPage("#pageFS");
	        } else if (toPage && toPage != "") {
	                goToNextPage("#" + toPage);
	        } else if ($('#hdHasCoApplicant').val() == 'Y') {
	            goToNextPage("#page6");
	        } else {
	            goToNextPage("#page2");
	        }
			break;
		default:
	}
	if (typeof scrollToEle !== "undefined" && scrollToEle) {
		$("#" + pageId).on("pagehide", function (event, ui) {
			$('html, body').stop().animate({ scrollTop: $(scrollToEle).offset().top - 100 }, '500', 'swing');
		});
	}
}
function acceptLaserScanResult(prefix, ele) {
	fillLaserScanResult(prefix, ele);
	$(ele).closest(".scan-result-wrapper").removeClass("open");
	var nextPage = "#page2";
	if (prefix !== "") {
		nextPage = "#page6";
	}
	goToNextPage(nextPage);
}
