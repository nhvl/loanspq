﻿
Partial Class xa_Inc_ProductServices
    Inherits System.Web.UI.UserControl

    Public Property Product As CProduct = New CProduct()

    Protected _ShouldRender As Boolean = True

    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        If Not IsPostBack Then
            If Product.Services IsNot Nothing AndAlso Product.Services.Count > 0 Then
                rpProdService.DataSource = Product.Services
                rpProdService.DataBind()
            Else
                _ShouldRender = False
            End If
        End If
    End Sub

    Protected Function SafeHTML(obj As Object) As String
        Dim str As String = If(obj IsNot Nothing, obj.ToString(), "")
        Return str.Replace("'", "&apos;").Replace("""", "&quot;")
    End Function
    Protected Function getProductServiceName(ByVal sDescription As String, ByVal sDescriptionURL As String) As String
        If Not String.IsNullOrEmpty(sDescriptionURL) Then
            sDescription = "<a href=" & sDescriptionURL & ">" & sDescription & "</a>"
        End If
        Return sDescription
    End Function
End Class
