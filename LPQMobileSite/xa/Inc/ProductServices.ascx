﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="ProductServices.ascx.vb" Inherits="xa_Inc_ProductServices" %>
<%If _ShouldRender Then%>
<div class='<%=Product.MinorAccountCode%>' product-code="<%=Product.ProductCode%>" <%=If(Product.IsRequired Or Product.IsPreSelection, "", "style='display:none;'")%>>
    <b style="font-size: 17px;"><%=Product.AccountName%></b><br />
    <fieldset data-role="controlgroup" style="margin: 5px 0 10px 0;">
        <asp:Repeater ID="rpProdService" runat="server" EnableViewState="false">
            <ItemTemplate>
                <div class='<%=Product.MinorAccountCode%> ui-grid-a' <%#If(Not Container.DataItem.IsActive, "style='display:none;'", "")%>>
                    <div class='ui-block-a ProductLabel' data-corners='false'>
                        <input type="checkbox" id="chk_<%#Product.ProductCode%>_<%#Eval("ServiceCode")%>" 
                            service_name="<%#SafeHTML(Eval("Description"))%>" 
                            account_name="<%#SafeHTML(Product.AccountName)%>" 
                            <%#If(Container.DataItem.IsRequired, "checked='checked' disabled='disabled'", "")%>
                            <%#If(Container.DataItem.IsPreSelection, "checked='checked'", "")%> />  
                            <label for="chk_<%#Product.ProductCode%>_<%#Eval("ServiceCode")%>">
                                <%#getProductServiceName(Eval("Description"), Eval("DescriptionURL"))%>
                            </label>
                    </div>
                </div>
            </ItemTemplate>
        </asp:Repeater>
    </fieldset>
</div>
<%End If%>