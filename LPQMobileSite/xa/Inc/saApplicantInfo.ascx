﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="saApplicantInfo.ascx.vb" Inherits="xa_Inc_saApplicantInfo" %>
<%@ Import Namespace="LPQMobile.Utils" %>
<%@ Reference Control="~/Inc/LaserDocumentScan.ascx" %>
<%@ Reference Control="~/Inc/NewDocumentScan.ascx" %>
<%@ Reference Control="~/Inc/MainApp/xaApplicantEmployment.ascx" %>
<%@ Reference Control="~/Inc/MainApp/xaApplicantID.ascx" %>
<%@ Reference Control="~/Inc/MainApp/xaApplicantQuestion.ascx" %>
<%@ Reference Control="~/Inc/MainApp/xaBeneficiary.ascx" %>
<%@ Reference Control="~/xa/Inc/saAddress.ascx" %>
<%@ Reference Control="~/Inc/NewDocCapture.ascx" %>
<%@ Reference Control="~/Inc/DocCaptureSourceSelector.ascx" %>
<%@ Reference Control="~/xa/Inc/baApplicantInfo.ascx" %>
<div class="sa-app-info-page" data-role="page" id="page<%=IDPrefix%>">
    <div data-role="header" style="display: none">
        <h1><%="Tell Us About The " & CEnum.MapSpecialRoleValueToDisplayName(RoleType)%><%=IIf(IsJoint, " (Joint)", "") %></h1>
    </div>
    <div data-role="content">
	    <%If Not String.IsNullOrWhiteSpace(Availability) AndAlso Availability.StartsWith("2") Then%>
            <%=HeaderUtils.RenderLogoAndSteps(LogoUrl, 1, "OPEN")%>
        <%Else%>
			<%=HeaderUtils.RenderLogoAndSteps(LogoUrl, 1, "JOIN")%>	
        <%End If%>  
        <%=HeaderUtils.RenderPageTitle(21, "Tell Us About The " & CEnum.MapSpecialRoleValueToDisplayName(RoleType) & IIf(IsJoint, " (Joint)", "").ToString(), False)%>
        <%=HeaderUtils.RenderPageTitle(0, "General Information", True)%>
		<iframe name="<%=IDPrefix%>if_saApplicantInfo"  style="display: none;" src="about:blank" ></iframe>
		<form target="<%=IDPrefix%>if_saApplicantInfo" action="/handler/AutoFillHandler.aspx" id="<%=IDPrefix%>frm_saApplicantInfo">
			<%If LaserScandocAvailable = True Then%>
			<div class="scan-doc-wrapper">
				<%--<add A tag to make the div focusable (for WCAG/ADA) and set inline-block to make heigh of A 100%--%>
				<a href="#<%=IDPrefix%>scandocs" data-rel="dialog" data-transition="pop" class="img-btn">
					<div class="scandocs-box laser-scan">
						<div class="caption-text">
							<p class="caption">Click or tap here to pre-fill information with your driver's license</p>
							<div class="avatar"><img src="/images/laser_scan.png" alt="avatar" class="img-responsive"/></div>
						</div>
					</div>
				</a>
			</div>
			<%ElseIf LegacyScandocAvailable AndAlso Not String.IsNullOrEmpty(ScanDocumentKey) Then%>
			<div class="scan-doc-wrapper">
				<%--<add A tag to make the div focusable (for WCAG/ADA) and set inline-block to make heigh of A 100%--%>
				<a href="#<%=IDPrefix%>scandocs" data-rel="dialog" data-transition="pop" class="img-btn">
					<div class="scandocs-box">
						<div class="caption-text">
							<p class="caption">Click or tap here to pre-fill information with your driver's license</p>
							<div class="avatar"><img src="/images/avatar.jpg" alt="avatar" class="img-responsive"/></div>
						</div>
					</div>
			   </a>
			</div>
			<%End If%>
			
			
			<div data-role="fieldcontain">
				<label for="<%=IDPrefix%>txtFName" class="RequiredIcon">First Name</label>
				<input id="<%=IDPrefix%>txtFName" type="text" name="fname" autocomplete="given-name" value="<%=FirstName%>" maxlength="<%=FieldMaxLength%>"/>
			</div>
			<div data-role="fieldcontain">
				<label for="<%=IDPrefix%>txtMName">Middle Name</label>
				<input id="<%=IDPrefix%>txtMName" type="text" name="mname" autocomplete="additional-name" maxlength="<%=FieldMaxLength%>"/>
			</div> 
			<div data-role="fieldcontain">
				<label for="<%=IDPrefix%>txtLName" class="RequiredIcon">Last Name</label>
				<input id="<%=IDPrefix%>txtLName" type="text" maxlength="<%=FieldMaxLength%>" name="lname" value="<%=LastName %>" autocomplete="family-name"/>
			</div>
			<div data-role="fieldcontain">
				<label for="<%=IDPrefix%>ddlSuffix">Suffix (Jr., Sr., etc.)</label>
				<select name="suffix" id="<%=IDPrefix%>ddlSuffix">
					<%=Common.RenderDropdownlist(GetSuffixList(), "")%>
				</select>
			</div>
            <div data-role="fieldcontain">
	            <div class="row">
		            <div class="col-xs-8 no-padding"><label for="<%=IDPrefix%>txtSSN1" id="<%=IDPrefix%>lblSSN">SSN</label></div>
					<div class="col-xs-4 no-padding ui-label-ssn">
						<span class="pull-right header_theme2 ssn-ico" style="padding-left: 4px; padding-right: 2px; display: inline;"><%=HeaderUtils.IconLock16%><span class="sr-only">SSN IconLock</span></span>
						<span id="<%=IDPrefix%>spOpenSsnSastisfy" onclick='openPopup("#<%=IDPrefix%>popSSNSastisfy")' class="pull-right focus-able" style="display: inline;" data-rel='popup' data-position-to="window" data-transition='pop'><span class="pull-right header_theme2  ssn-ico"><%=HeaderUtils.IconQuestion16%></span><span class="sr-only">SSN/TIN IconQuestion</span></span>
						<div style="display: inline;" class="hidden">
							<label is_show="1" class="pull-right header_theme2 rename-able shadow-btn focus-able" style="margin-right: 10px; cursor: pointer;outline: none" onclick="<%=IDPrefix%>toggleSSNText(this);">Hide SSN/TIN</label>
						</div>
						<div style="display: inline;">
							<label is_show="0" class="pull-right header_theme2 rename-able shadow-btn focus-able" style="margin-right: 10px; cursor: pointer;outline: none" onclick="<%=IDPrefix%>toggleSSNText(this);">Show SSN/TIN</label>
						</div>
						<div class="clearfix"></div>
					</div>
	            </div>
				<%--<input pattern="[0-9]*" id="<%=IDPrefix%>txtSSN" class="inssn" type="<%=TextAndroidTel%>" maxlength ='11' value="<%=SSN %>" <%=SSNDisabledString%> />--%>
				<div id="<%=IDPrefix%>divSSN" class="ui-input-ssn row">
					<div class="col-xs-4">
						<input type="password" aria-labelledby="<%=IDPrefix%>lblSSN" pattern="[0-9]*" placeholder="---" id="<%=IDPrefix%>txtSSN1" autocomplete="new-password" maxlength ='3' next-input="#<%=IDPrefix%>txtSSN2" onkeydown="limitToNumeric(event);" onkeyup="onKeyUp(event,'#<%=IDPrefix%>txtSSN1','#<%=IDPrefix%>txtSSN2', '3');"/>
						<input type="hidden" id="<%=IDPrefix%>hdSSN1" value=""/>
					</div>
					<div class="col-xs-4">
						<input type="password" aria-labelledby="<%=IDPrefix%>lblSSN" pattern="[0-9]*" placeholder="--" id="<%=IDPrefix%>txtSSN2" autocomplete="new-password" maxlength ='2' next-input="#<%=IDPrefix%>txtSSN3" onkeydown="limitToNumeric(event);" onkeyup="onKeyUp(event,'#<%=IDPrefix%>txtSSN2','#<%=IDPrefix%>txtSSN3', '2');"/>
						<input type="hidden" id="<%=IDPrefix%>hdSSN2" value=""/>
					</div>
					<div class="col-xs-4" style="padding-right: 0px;">
						<input aria-labelledby="<%=IDPrefix%>lblSSN" type="<%=TextAndroidTel%>" pattern="[0-9]*" placeholder="----" id="<%=IDPrefix%>txtSSN3" maxlength ='4' onkeydown="limitToNumeric(event);"/>
						<input type="hidden" id="<%=IDPrefix%>txtSSN" class="combine-field-value"/>
					</div>
				</div>
			</div>
			<div id="<%=IDPrefix%>popSSNSastisfy" data-role="popup" style="max-width: 400px;">
				<div data-role="content">
					<div class="row">
						<div class="col-xs-12 header_theme2">
							<a data-rel="back" href="#" class="pull-right svg-btn"><%=HeaderUtils.IconClose %><span style="display: none;">close</span></a>
						</div>
					</div>
					<div class="row">
						<div class="col-sm-12">
							<div style="margin: 10px 0;">
								Your Social Security Number (SSN) is used for identification purposes and to determine your account opening eligibility.
							</div>
						</div>    
					</div>
				</div>
			</div>
			<div data-role="fieldcontain" id="<%=IDPrefix%>divDOBStuff">
				<div class="row">
					<div class="col-xs-8 no-padding"><label for="<%=IDPrefix%>txtDOB" id="<%=IDPrefix%>lblDOB" class="dob-label RequiredIcon">Date of Birth</label></div>
					<div class="col-xs-4 no-padding"><input type="hidden" id="<%=IDPrefix%>txtDOB" class="combine-field-value"/></div>
				</div>
				<div id="<%=IDPrefix%>divDOB" class="ui-input-date row">
					<div class="col-xs-4">
						<input aria-labelledby="<%=IDPrefix%>lblDOB" type="<%=TextAndroidTel%>" pattern="[0-9]*" placeholder="mm" id="<%=IDPrefix%>txtDOB1" maxlength ='2' onkeydown="limitToNumeric(event);" onkeyup="onKeyUp(event,'#<%=IDPrefix%>txtDOB1','#<%=IDPrefix%>txtDOB2', '2');"/>
					</div>
					<div class="col-xs-4">
						<input aria-labelledby="<%=IDPrefix%>lblDOB" type="<%=TextAndroidTel%>" pattern="[0-9]*" placeholder="dd" id="<%=IDPrefix%>txtDOB2" maxlength ='2' onkeydown="limitToNumeric(event);" onkeyup="onKeyUp(event,'#<%=IDPrefix%>txtDOB2','#<%=IDPrefix%>txtDOB3', '2');"/>
					</div>
					<div class="col-xs-4" style="padding-right: 0;">
						<input aria-labelledby="<%=IDPrefix%>lblDOB" type="<%=TextAndroidTel%>" pattern="[0-9]*" placeholder="yyyy" id="<%=IDPrefix%>txtDOB3" onkeydown="limitToNumeric(event);" maxlength ='4'/>
					</div>
				</div>
			</div>
            <%If EnableMemberNumber Then%>
			<div id="<%=IDPrefix%>divMemberNumber" <%=IIf(IsInMode("777") AndAlso IsInFeature("visibility"), "class=""showfield-section"" data-show-field-section-id=""" & BuildShowFieldSectionID("divMemberNumber") & """ data-default-state=""off"" data-section-name='" & IIf(InstitutionType = CEnum.InstitutionType.BANK, "Account Number", "Member Number") & "'", "")%>>
			<div data-role="fieldcontain">
			   <label for="<%=IDPrefix%>txtMemberNumber" <%=IIf(MemberNumberRequired, " class='RequiredIcon'", "")%>><%:IIf(InstitutionType = CEnum.InstitutionType.BANK, "Account Number", "Member Number")%></label>
			   <input type="text" id="<%=IDPrefix%>txtMemberNumber" name="menbernum" maxlength ="50"/>
			</div>
			</div>
			<%End If%>
			<%If EnableGender Then%>
			<div id="<%=IDPrefix%>divGender" <%=IIf(IsInMode("777") AndAlso IsInFeature("visibility"), "class=""showfield-section"" data-show-field-section-id=""" & BuildShowFieldSectionID("divGender") & """ data-section-name='Gender'", "")%>>
				<div data-role="fieldcontain">
					<label for="<%=IDPrefix%>ddlGender">Gender</label>
					<select name="gender" id="<%=IDPrefix%>ddlGender">
						<option value=""></option>
						<option value="MALE">Male</option>
						<option value="FEMALE">Female</option>
					</select>
				</div>
			</div>
			<%End If%>
			<%If EnableMotherMaidenName Then%>
			<div id="<%=IDPrefix%>divMotherMaidenName" <%=IIf(IsInMode("777") AndAlso IsInFeature("visibility"), "class=""showfield-section"" data-show-field-section-id=""" & BuildShowFieldSectionID("divMotherMaidenName") & """ data-section-name='Mother Maiden Name'", "")%>>
				<div data-role="fieldcontain">
					<label for="<%=IDPrefix%>txtMotherMaidenName">Mother's Maiden Name</label>
					<input id="<%=IDPrefix%>txtMotherMaidenName" name="mothermaidenname" autocomplete="name" type="text" maxlength ="20"/>
				</div>
			</div>
			<%End If%>
			<%If EnableCitizenshipStatus Then%>
			<div id="<%=IDPrefix%>divCitizenshipStatus" <%=IIf(IsInMode("777") AndAlso IsInFeature("visibility"), "class=""showfield-section"" data-show-field-section-id=""" & BuildShowFieldSectionID("divCitizenshipStatus") & """ data-section-name='Citizenship Status'", "")%>>
				<div data-role="fieldcontain">
					<label for="<%=IDPrefix%>ddlCitizenshipStatus">Citizenship Status</label>
					<select id="<%=IDPrefix%>ddlCitizenshipStatus" name="citizenshipStatus">
						<%=CitizenshipStatusDropdown %>
					</select>
				</div>
			</div>
			<%End If%>
			<%If EnableMaritalStatus Then%>
			<div id="<%=IDPrefix%>divMaritalStatus" <%=IIf(IsInMode("777") AndAlso IsInFeature("visibility"), "class=""showfield-section"" data-show-field-section-id=""" & BuildShowFieldSectionID("divMaritalStatus") & """ data-default-state=""" & IIf(InbranchEnableMaritalStatus, "", "off") & """ data-section-name='Marital Status'", "")%>>
				<div data-role="fieldcontain">
					<label for="<%=IDPrefix%>ddlMaritalStatus">Marital Status</label>
					<select id="<%=IDPrefix%>ddlMaritalStatus">
						<%=MaritalStatusDropdown%>
					</select>
				</div>
			</div>
			<%End If%>
            
			 <%If Not String.IsNullOrEmpty(EmployeeOfLenderDropdown) Then%>
			<div data-role="fieldcontain" class="hidden">
                <label for="<%=IDPrefix%>ddlEmployeeOfLender">Are you an employee of <span class="bold"><%=LenderName%>?</span></label>				
				<select id="<%=IDPrefix%>ddlEmployeeOfLender">
					<%=EmployeeOfLenderDropdown%>
				</select>
			</div>
            <%End If %>
			<%If Not IsPrimaryApplicant AndAlso EnableRelationshipToPrimary Then%>
			<div id="<%=IDPrefix%>divRelationshipToPrimary" <%=IIf(IsInMode("777") AndAlso IsInFeature("visibility"), "class=""showfield-section"" data-show-field-section-id=""" & BuildShowFieldSectionID("divRelationshipToPrimary") & """ data-default-state=""" & IIf(InbranchEnableRelationshipToPrimary, "", "off") & """ data-section-name='Relationship To Primary'", "")%>>
			<div data-role="fieldcontain">
				<label for="<%=IDPrefix%>ddlRelationToPrimaryApplicant">Relation to Primary Applicant</label>
				<select id="<%=IDPrefix%>ddlRelationToPrimaryApplicant">
					<%=RelationToPrimaryApplicantDropdown%>
				</select>
			</div>
			</div>
			<%End If%>

		</form>
		<asp:PlaceHolder runat="server" ID="plhBeneficiary"></asp:PlaceHolder>
		<%If EnableAddress Then%>
		<div id="<%=IDPrefix%>divPhysicalAddress" <%=IIf(IsInMode("777") AndAlso IsInFeature("visibility"), "class=""showfield-section"" data-show-field-section-id=""" & BuildShowFieldSectionID("divPhysicalAddress") & """ data-default-state=""" & IIf(InbrachEnableAddress, "", "off") & """ data-section-name='Physical Address'", "")%>>
		<asp:PlaceHolder runat="server" ID="plhAddress"></asp:PlaceHolder>
		</div>
		<%End If%>
		
		<%If EnableContactInfo Then%>
		<div id="<%=IDPrefix%>divContactInformation" <%=IIf(IsInMode("777") AndAlso IsInFeature("visibility"), "class=""showfield-section"" data-show-field-section-id=""" & BuildShowFieldSectionID("divContactInformation") & """ data-default-state=""" & IIf(InbrachEnableContactInfo, "", "off") & """ data-section-name='Contact Information'", "")%>>
		<iframe name="<%=IDPrefix%>if_ContactInfo" style="display: none;" src="about:blank" ></iframe>
		<form target="<%=IDPrefix%>if_ContactInfo" action="/handler/AutoFillHandler.aspx" id="<%=IDPrefix%>frm_ContactInfo">
		<%=HeaderUtils.RenderPageTitle(0, "Contact Information", True)%>
		<div data-role="fieldcontain">
			<label id="<%=IDPrefix%>lblPreferedMethod" for="<%=IDPrefix%>preferredContactMethod">Preferred Contact Method</label>
			<select name="preferredContactMethod" id="<%=IDPrefix%>preferredContactMethod" data-prev-value="" onchange =" <%=IDPrefix%>updateContactInfo(this)">
				<option value="">--Please Select--</option>
				<%If String.IsNullOrEmpty(PreferContactMethodDropdown) Then%> 
				<option value="EMAIL">Email</option>
				<option value="HOME">Home Phone</option>
				<option value="CELL">Cell Phone</option>
				<option value="WORK">Work Phone</option>
				<%Else%>
					<%=PreferContactMethodDropdown%>
				<%End If%>
			</select>
		</div>
		<div data-role="fieldcontain">
			<label id="<%=IDPrefix%>lblContactEmail" for="<%=IDPrefix%>txtContactEmail" class="RequiredIcon">Email</label>
			<input type="email" name="email" autocomplete="email" id="<%=IDPrefix%>txtContactEmail" maxlength ="50"/>
		</div>
		<div data-role="fieldcontain" id="<%=IDPrefix%>divContactHomePhone">
			<div>
				<div style="display: inline-block;"><label id="<%=IDPrefix%>lblContactHomePhone" for="<%=IDPrefix%>txtContactHomePhone" class="RequiredIcon" style="white-space: nowrap;">Primary/Home Phone</label></div>
				<div style="display: inline-block;"><label class="phone_format rename-able" style="font-style: italic; white-space: nowrap">(xxx) xxx-xxxx</label></div>
			</div>
			<input type="<%=TextAndroidTel%>" pattern="[0-9]*" autocomplete="tel" name="phone" id="<%=IDPrefix%>txtContactHomePhone" class="inphone" maxlength='14'/>
		</div>
		<div data-role="fieldcontain" id="<%=IDPrefix%>divContactMobilePhone">
			<div>
				<div style="display: inline-block;"><label id="<%=IDPrefix%>lblContactMobilePhone" for="<%=IDPrefix%>txtContactMobilePhone" style="white-space: nowrap;">Secondary/Mobile Phone</label></div>
				<div style="display: inline-block;"><label class="phone_format rename-able" style="font-style: italic; white-space: nowrap">(xxx) xxx-xxxx</label></div>
			</div>
			<input type= "<%=TextAndroidTel%>" pattern="[0-9]*" autocomplete="tel" name="mobile" id="<%=IDPrefix%>txtContactMobilePhone" class="inphone" maxlength='14'/>
		</div>
		</form>
		</div>
		<%End If %>
		<%If EnableContactRelative Then%>
		<div id="<%=IDPrefix%>divContactRelative" <%=IIf(IsInMode("777") AndAlso IsInFeature("visibility"), "class=""showfield-section"" data-show-field-section-id=""" & BuildShowFieldSectionID("divContactRelative") & """ data-default-state=""" & IIf(InbrachEnableContactRelative, "", "off") & """ data-section-name='Contact Relative'", "")%>>
		<form target="<%=IDPrefix%>if_RelativeNotLivingWithApplicant" action="/handler/AutoFillHandler.aspx" id="<%=IDPrefix%>frm_RelativeNotLivingWithApplicant">
		<%=HeaderUtils.RenderPageTitle(0, "Relative Not Living with Applicant", True)%>
		<div data-role="fieldcontain">
			<label for="<%=IDPrefix%>txtRelativeFirstName">First Name</label>
			<input id="<%=IDPrefix%>txtRelativeFirstName" type="text" name="fname" autocomplete="given-name" maxlength="<%=FieldMaxLength%>"/>
		</div>
		<div data-role="fieldcontain">
			<label for="<%=IDPrefix%>txtRelativeLastName">Last Name</label>
			<input id="<%=IDPrefix%>txtRelativeLastName" type="text" maxlength="<%=FieldMaxLength%>" name="lname" autocomplete="family-name"/>
		</div>
		<div data-role="fieldcontain" id="<%=IDPrefix%>divRelativePhone">
			<div>
				<div style="display: inline-block;"><label id="<%=IDPrefix%>lblRelativePhone" for="<%=IDPrefix%>txtRelativePhone" style="white-space: nowrap;">Phone</label></div>
				<div style="display: inline-block;"><label class="phone_format rename-able" style="font-style: italic; white-space: nowrap">(xxx) xxx-xxxx</label></div>
			</div>
			<input type="<%=TextAndroidTel%>" pattern="[0-9]*" autocomplete="tel" name="phone" id="<%=IDPrefix%>txtRelativePhone" class="inphone" maxlength='14'/>
		</div>
		<div data-role="fieldcontain">
			<label id="<%=IDPrefix%>lblRelativeEmail" for="<%=IDPrefix%>txtRelativeEmail">Email</label>
			<input type="email" name="email" autocomplete="email" id="<%=IDPrefix%>txtRelativeEmail" maxlength ="50"/>
		</div>
		<div data-role="fieldcontain">
			<label for="<%=IDPrefix%>txtRelativeRelationship">Relationship</label>
			<input id="<%=IDPrefix%>txtRelativeRelationship" type="text" name="relationship"/>
		</div>
		<div id="<%=IDPrefix%>div_ddl_relative_country" style="display:none" data-role="fieldcontain">
			<label for="<%=IDPrefix%>ddlRelativeCountry">Country</label>
			<select id="<%=IDPrefix%>ddlRelativeCountry" name="country" autocomplete="country">
				<option value="USA" selected="selected">USA</option>            
			</select>
		</div>
		<div data-role="fieldcontain">
			<label for="<%=IDPrefix%>txtRelativeAddress">Address</label>
			<input type="text" id="<%=IDPrefix%>txtRelativeAddress" maxlength="100" name="address" autocomplete="street-address" onchange="<%=callVerifyAddressJSFunction(False, "Relative")%>"/>
		</div>
		<div data-role="fieldcontain">
			<label for="<%=IDPrefix%>txtRelativeZip">Zip</label>
			<input type="tel" id="<%=IDPrefix%>txtRelativeZip" class="numeric" maxlength="10" pattern="[0-9]*" name="zip" autocomplete="postal-code" onchange="<%=callVerifyAddressJSFunction(True, "Relative")%>"/>
		</div>

		<div data-role="fieldcontain">
			<label for="<%=IDPrefix%>txtRelativeCity">City</label>
			<input type="text" id="<%=IDPrefix%>txtRelativeCity" maxlength="50" name="city" autocomplete="address-level2"  onchange="<%=callVerifyAddressJSFunction(False, "Relative")%>"/>
		</div>
		<div data-role="fieldcontain">
			<label for="<%=IDPrefix%>ddlRelativeState">State</label>
			<select id="<%=IDPrefix%>ddlRelativeState" name="state" autocomplete="address-level1" onchange="<%=callVerifyAddressJSFunction(False, "Relative")%>">
				<%=StateDropdown%>
			</select>
		</div>
		<div><span id="<%=IDPrefix%>spVerifyRelativeMessage" class="require-span" style="display: none;"> Unable to validate address</span></div>
		</form>
		</div>
		<%End If %>
		<asp:PlaceHolder runat="server" ID="plhEmployment"></asp:PlaceHolder>
		<asp:PlaceHolder runat="server" ID="plhIDCard"></asp:PlaceHolder>
		<asp:PlaceHolder runat="server" ID="plhUploadDoc"></asp:PlaceHolder>
		<asp:PlaceHolder runat="server" ID="plhUploadDocSrcSelector"></asp:PlaceHolder>
		<asp:PlaceHolder runat="server" ID="plhAdditionalInfo"></asp:PlaceHolder>
    </div>
    <div class ="div-continue"  data-role="footer">
        <a href="#" data-transition="slide" type="button" class="div-continue-button" onclick="<%=IDPrefix%>ValidateSAApplicantInfo()">Continue</a> 
        <a href="#divErrorDialog" style="display: none;">no text</a>
        <span> Or</span>  <a href="#" onclick="goToNextPage('#<%=PreviousPage %>');" class ="div-goback" data-corners="false" data-shadow="false" data-theme="reset"><span class="hover-goback"> Go Back</span></a>
    </div>
	<input type="hidden" id="<%=IDPrefix%>hdRelativePhoneCountry" />
	<input type="hidden" id="<%=IDPrefix%>hdContactHomePhoneCountry" />
	<input type="hidden" id="<%=IDPrefix%>hdContactMobilePhoneCountry" />
</div>
<div data-role="dialog" id="<%=IDPrefix%>scandocs">
    <div data-role="header"  style="display:none" >
        <h1>Driver's License Scan</h1>
    </div>
    <div data-role="content">
	    <%If LaserScandocAvailable Then%>
		<div class="js-laser-scan-container">
			<a data-rel="back" href="#" class="svg-btn btn-close-dialog"><%=HeaderUtils.IconClose %><span style="display: none;">close</span></a> 
			<asp:PlaceHolder runat="server" ID="plhLaserDriverLicenseScan"></asp:PlaceHolder>
		</div>
		<%End If %>
		<div class="js-legacy-scan-container <%=IIf(LaserScandocAvailable, "hidden", "")%>" >
			<a data-rel="back" href="#" class="pull-right svg-btn"><%=HeaderUtils.IconClose %><span style="display: none;">close</span></a>      
			<asp:PlaceHolder runat="server" ID="plhDriverLicenseScan"></asp:PlaceHolder>
			<div class ="div-continue" style="text-align: center;">
				<a href="#"  data-transition="slide" onclick="<%=IDPrefix%>ScanAccept()" type="button" data-role="button" class="div-continue-button">Done</a> 
				<a href="#divErrorDialog" style="display: none;">no text</a>              
			</div>
		</div>
    </div>
	<%If IsPrimaryApplicant Then%>
	<div id="<%=IDPrefix%>popScanZipWarning" data-role="popup" style="max-width: 400px;">
		<div data-role="content">
			<div class="row">
				<div class="col-xs-12 header_theme2">
					<a data-rel="back" class="pull-right svg-btn" href="#" ><%=HeaderUtils.IconClose %><span style="display: none;">close</span></a>
				</div>
			</div>
			<div class="row">
				<div class="col-sm-12">
					<div style="margin: 10px 0; font-weight: normal;">System can't prefill due to ZIP code discrepancy. Please proceed by manually entering the information or correct the value in the ZIP field in the 1st page.</div>     
				</div>    
			</div>
		</div>
		<div class="div-continue" style="text-align: center;"><a href="#" data-transition="slide" type="button" onclick="closePopup('#<%=IDPrefix%>popScanZipWarning')" data-role="button" class="div-continue-button">OK</a></div>
	</div>
	<%End If%>
</div>

<script type="text/javascript">
	var <%=IDPrefix%>xbarcodePicker;
	var <%=IDPrefix%>g_is_usa_relative_address = true;
	var EMPLogic<%=IDPrefix%> = {}; 
	var EMPLogic<%=IDPrefix%>PREV = {};

	$("#<%=IDPrefix%>scandocs").on("pageshow", function () {
		if ($("#<%=IDPrefix%>scandit-barcode-picker").length > 0 && $("#<%=IDPrefix%>scandocs").data("no-camera") != true) {
			ScanditSDK.CameraAccess.getCameras().then(function (cameraList) {
				if (cameraList.length > 0) {
					<%=IDPrefix%>initLaserScan();
				} else {
					<%=IDPrefix%>loadLegacyScan();
				}
				return null;
			}).catch(function (ex) {
				if (ex.name == "NotAllowedError") {
					$("#<%=IDPrefix%>scandocs .js-laser-scan-container").find("a[data-rel='back']").trigger("click");
				} else {
					$("#<%=IDPrefix%>scandocs").data("no-camera", true);
					<%=IDPrefix%>loadLegacyScan();
				}
				return null;
			});
		} else {
			<%=IDPrefix%>loadLegacyScan();
		}
		$("#<%=IDPrefix%>scandocs a[data-rel='back']").focus();
	});
	$("#<%=IDPrefix%>scandocs").on("pagehide", function () {
		if ($("#<%=IDPrefix%>scandit-barcode-picker").length > 0 && <%=IDPrefix%>xbarcodePicker) {
			$(".divScanDocs.laser-scan", "#<%=IDPrefix%>scandocs").removeClass("open");
			<%=IDPrefix%>xbarcodePicker.destroy();
		}
		$('a[href="#<%=IDPrefix%>scandocs"]').focus();

	});
	function <%=IDPrefix%>loadLegacyScan() {
		$("#<%=IDPrefix%>scandocs").find(".js-laser-scan-container").addClass("hidden");
		$("#<%=IDPrefix%>scandocs").find(".js-legacy-scan-container").removeClass("hidden");
	}
	function <%=IDPrefix%>initLaserScan() {
		ScanditSDK.BarcodePicker.create(document.getElementById("<%=IDPrefix%>scandit-barcode-picker"), {
			playSoundOnScan: true,
			vibrateOnScan: true
		}).then(function (barcodePicker) {
			$("#<%=IDPrefix%>scandocs").find(".scan-guide-text").removeClass("hidden");
			<%=IDPrefix%>xbarcodePicker = barcodePicker;
			// barcodePicker is ready here to be used
			var scanSettings = new ScanditSDK.ScanSettings({
				enabledSymbologies: ["pdf417"],
				codeDuplicateFilter: 1000
			});
			barcodePicker.applyScanSettings(scanSettings);
			$(".scandit-camera-switcher, .scandit-flash-white, .scandit-flash-color", ".scandit-barcode-picker ").addClass("hidden");
			barcodePicker.onScan(function (scanResult) {
				scanResult.barcodes.reduce(function (string, barcode) {
					var result = analyzeData(barcode.data);
					var $scanResultList = $(".scan-result-list", "#<%=IDPrefix%>scandocs");
					if (result.FirstName) {
						$scanResultList.find(".js-prop-name").text(result.FirstName + ($.trim(result.MiddleName).length > 0 ? " " + result.MiddleName : "") + " " + result.LastName);
					}
					$scanResultList.find(".js-prop-dob").text(result.DOB);

					$scanResultList.find(".js-prop-issued-date").text(result.LicenseIssuedDate);

					$scanResultList.find(".js-prop-expired-date").text(result.LicenseExpiredDate);
					var sex = "Unknown";
					if (result.Sex == "1") {
						sex = "Male";
					} else if (result.Sex == "2") {
						sex = "Female";
					}
					$scanResultList.find(".js-prop-sex").text(sex);
					$scanResultList.find(".js-prop-license-number").text(result.LicenseNumber);
					var address = "";
					address += "<p>" + result.MailingAddress1 + "</p>";
					address += "<p>" + result.MailingCity + " " + result.MailingState + ", " + result.MailingZip + "</p>";
					$scanResultList.find(".js-prop-address").html(address);
					$(".divScanDocs.laser-scan", "#<%=IDPrefix%>scandocs").addClass("open");
					$(".scan-result-wrapper", "#<%=IDPrefix%>scandocs").data("scan-result", result);
				}, "");
			});
			return null;
		});
	}
	<%If IsPrimaryApplicant then%>
	$("#<%=IDPrefix%>popScanZipWarning").on("popupafteropen", function (event, ui) {
		//prevent scrollbar by popup overlay
		$("#" + this.id + "-screen").height("");
	});
	$("#<%=IDPrefix%>popScanZipWarning").on("popupafterclose", function (event, ui) {
		goToNextPage("#page<%=IDPrefix%>");
	});
	<%End If%>
	
	$("#<%=IDPrefix%>popSSNSastisfy").on("popupafteropen", function (event, ui) {
		$("#" + this.id + "-screen").height("");
	});
	$("#<%=IDPrefix%>popSSNSastisfy").on("popupafterclose", function (event, ui) {
		setTimeout(function () {
			$("#<%=IDPrefix%>spOpenSsnSastisfy").focus();
			}, 100);
	});
	function <%=IDPrefix%>updateHiddenSSN() {
		if ($("#<%=IDPrefix%>txtSSN1").val().length == 3) {
			var ssn = $("#<%=IDPrefix%>txtSSN1").val() + '-' + $("#<%=IDPrefix%>txtSSN2").val() + '-' + $("#<%=IDPrefix%>txtSSN3").val();
			$("#<%=IDPrefix%>txtSSN").val(ssn);
		}
	}
	function <%=IDPrefix%>toggleSSNText(btn) {
        return Common.ToggleSSNText(btn, "<%=IDPrefix%>");
	}
	function <%=IDPrefix%>updateHiddenDOB() {
		var month = padLeft($("#<%=IDPrefix%>txtDOB1").val(), 2);
		var day = padLeft($("#<%=IDPrefix%>txtDOB2").val(), 2);
		var year = padLeft($("#<%=IDPrefix%>txtDOB3").val(), 4);

		var date = month + '/' + day + '/' + year;
		$("#<%=IDPrefix%>txtDOB").val(date == "//" ? "" : date);
	}
	<%If EnableContactInfo Then%>
	function <%=IDPrefix%>updateContactInfo(element) {
        var labelMobilePhoneElem = $('#<%=IDPrefix%>lblContactMobilePhone');
        if (($('#<%=IDPrefix%>preferredContactMethod option:selected').val() || "").toUpperCase() == "CELL") {
			labelMobilePhoneElem.addClass('RequiredIcon');
		} else {
			labelMobilePhoneElem.removeClass('RequiredIcon');
		}
		
		switch ($(element).data("prev-value")) {
			case "HOME":
				$.lpqValidate.validate("<%=IDPrefix%>ValidateSAApplicantInfo", '#<%=IDPrefix%>txtContactHomePhone');
			case "CELL":
				$.lpqValidate.validate("<%=IDPrefix%>ValidateSAApplicantInfo", '#<%=IDPrefix%>txtContactMobilePhone');
    			break;
			default:
		}
		$(element).data("prev-value", $(element).val().toUpperCase());
	}
	<%End If%>
	function <%=IDPrefix%>updateHiddenPhoneCountry(currElem, hdPhoneCountryElem) {
		if (currElem.find('li[class~="active"]')) {
			hdPhoneCountryElem.val(currElem.find('li[class~="active"]').attr('data-country-code'));
		}
	}
	
	function <%=IDPrefix%>registerSAApplicantInfoValidator() {
		$('#<%=IDPrefix%>divSSN').observer({
			validators: [
				function (partial, evt) {
					<%=IDPrefix%>updateHiddenSSN();

					//var ssn = Common.GetSSN($('#<%=IDPrefix%>txtSSN').val());
					var ssn = $('#<%=IDPrefix%>txtSSN1').val() + $('#<%=IDPrefix%>txtSSN2').val() + $('#<%=IDPrefix%>txtSSN3').val();
                    if (/^[0-9]{9}$/.test(ssn) == false) {
                        return 'Valid SSN is required';
                    } else {
                            //validate duplicate ssn            
                        if (typeof SAPREFIXLIST != "undefined") {                        
                            if (SAPREFIXLIST.length > 0){
                                var currIndex = SAPREFIXLIST.indexOf('<%=IDPrefix%>');
                                var currSSNs = [];
                                if (currIndex > 0) {
                                    for (var i = 0; i < currIndex; i++) {
                                        var currIDPrefix = SAPREFIXLIST[i];                           
                                        currSSNs.push($('#' +currIDPrefix+ 'txtSSN1').val() + $('#' + currIDPrefix + 'txtSSN2').val() + $('#' + currIDPrefix + 'txtSSN3').val());
                                    }
                                    if ($.inArray(ssn, currSSNs) > -1) {                                   
                                        var PrevIDPrefix = SAPREFIXLIST[currSSNs.indexOf(ssn)];
                                        var applicantName = $('#' + PrevIDPrefix + 'txtFName').val() + " " + $('#' + PrevIDPrefix + 'txtLName').val();
                                        return "SSN shouldn't be the same as " + applicantName + " applicant"
                                    }
                                }
                            }                   
                        }
                    }
					return "";
				}
			],
			validateOnBlur: true,
			group: "<%=IDPrefix%>ValidateSAApplicantInfo"
		});
		$('#<%=IDPrefix%>txtFName').observer({
			validators: [
				function (partial) {
					var text = $(this).val();
					if (!Common.ValidateText(text)) {
						return 'First Name is required';					
					} else if (/^[\sa-zA-Z\'-]+$/.test(text) == false) {
						return 'Enter a valid first name. Allowed characters: letters "A-Z", dashes, apostrophes and spaces.';
					}
					return "";
				}
			],
			validateOnBlur: true,
			group: "<%=IDPrefix%>ValidateSAApplicantInfo"
		});
		$('#<%=IDPrefix%>txtLName').observer({
			validators: [
				function (partial) {
					var text = $(this).val();
					if (!Common.ValidateText(text)) {
						return 'Last Name is required';
					} else if (/^[\sa-zA-Z\'-]+$/.test(text) == false) {
						return 'Enter a valid last name. Allowed characters: letters "A-Z", dashes, apostrophes and spaces.';
					}
					return "";
				}
			],
			validateOnBlur: true,
			group: "<%=IDPrefix%>ValidateSAApplicantInfo"
		});
		$('#<%=IDPrefix%>txtMName').observer({
			validators: [
				function (partial) {
					var text = $(this).val();
					if (/^[\sa-zA-Z\'-]*$/.test(text) == false) {
						return 'Enter a valid middle name. Allowed characters: letters "A-Z", dashes, apostrophes and spaces.';
					}
					return "";
				}
			],
			validateOnBlur: true,
			group: "<%=IDPrefix%>ValidateSAApplicantInfo"
		});
		$('#<%=IDPrefix%>divDOB').observer({
			validators: [
				function (partial) {
					<%=IDPrefix%>updateHiddenDOB();
					$("#<%=IDPrefix%>ddlEmployeeOfLender").closest(".ui-field-contain").addClass("hidden");
					var $DOB = $('#<%=IDPrefix%>txtDOB');
					var dob = moment($DOB.val(), "MM-DD-YYYY");
					if (!Common.ValidateDate($DOB.val())) {
						return "Date of Birth is required";
					} else if(!Common.IsValidDate($DOB.val())) {
						return "Valid Date of Birth is required";
					} else if (dob.year() < 1900) {
						return "Date of Birth is too old";
					} else if (dob.isAfter(moment())) {
						return "Date of Birth must be equal or less than current date";
					} else {
						var age = moment().diff(dob, "years");
						<%If BlockApplicantAgeUnder > 0 then%>
						if (age < <%=BlockApplicantAgeUnder%>) {
							return "The applicant age must be at least <%=BlockApplicantAgeUnder%>";
						}
						<%End If%>
						<%If BlockApplicantAgeOver > 0 Then%>
						if (age > <%=BlockApplicantAgeOver%>) {
							return "The applicant age cannot be over <%=BlockApplicantAgeOver%>";
						}
						<%End If%>
						if(moment().add(-17, "years").startOf("day").isAfter(moment($DOB.val(), 'MM-DD-YYYY'))) {
							$("#<%=IDPrefix%>ddlEmployeeOfLender").closest(".ui-field-contain").removeClass("hidden");
						}
						return "";
					}
				}
			],
			validateOnBlur: true,
			container: '#<%=IDPrefix%>divDOB',
			group: "<%=IDPrefix%>ValidateSAApplicantInfo"
        });
        <%If EnableMotherMaidenName Then%>
            //validate optional mother maiden name field
            $('#<%=IDPrefix%>txtMotherMaidenName').observer({
			    validators: [
                    function (partial) {
                        var text = $(this).val();
                        if (Common.ValidateText(text) && /^[\sa-zA-Z\'-]*$/.test(text) == false) {
						    return 'Enter a valid mother maiden name. Allowed characters: letters "A-Z", dashes, apostrophes and spaces.'			
                        }
					    return "";
				    }
			    ],
			    validateOnBlur: true,
			    group: "<%=IDPrefix%>ValidateSAApplicantInfo"
            });
        <%End If%>
		<%If EnableContactInfo %>
		$('#<%=IDPrefix%>txtContactEmail').observer({
			validators: [
				function (partial) {
					if ($(this).closest("div.showfield-section").length > 0 && $(this).closest("div.showfield-section").hasClass("hidden")) return "";
					var email = $(this).val();
					if (Common.ValidateEmail(email) == false) {
						return "Valid Email is required";
					}
					return "";
				}
			],
			validateOnBlur: true,
			group: "<%=IDPrefix%>ValidateSAApplicantInfo"
		});
		$('#<%=IDPrefix%>txtContactHomePhone').observer({
			validators: [
				function (partial) {
					if ($(this).closest("div.showfield-section").length > 0 && $(this).closest("div.showfield-section").hasClass("hidden")) return "";
					var homephone = $(this).val();
					if (!Common.ValidateText(homephone)) {
						return "Valid phone number is required";
					}

					if (is_foreign_phone) {
						return validateForeignPhone('<%=IDPrefix%>txtContactHomePhone');
					} else {
						if (!Common.ValidatePhone(homephone)) {
							return "Valid phone number is required";
						} else {
							//validate first digit of home phone : 0 and 1 is invalid
							var firstDigit = homephone.replace("(", "").substring(0, 1);
							if (firstDigit == 0 || firstDigit == 1) {
								return 'Phone number and area code can not begin with 0 or 1 and must be a valid phone number';
							}
						}
					}
					return "";
				}
			],
			validateOnBlur: true,
			group: "<%=IDPrefix%>ValidateSAApplicantInfo"
		});
		$('#<%=IDPrefix%>txtContactMobilePhone').observer({
			validators: [
				function (partial) {
					if ($(this).closest("div.showfield-section").length > 0 && $(this).closest("div.showfield-section").hasClass("hidden")) return "";
					var mobile = $(this).val();
					if (Common.ValidateText(mobile)) {
						if (is_foreign_phone) {
							return validateForeignPhone('<%=IDPrefix%>txtContactMobilePhone');
					    } else {
					    	if (!Common.ValidatePhone(mobile)) {
					    		return "Valid phone number is required";
					    	} else {
					    		//validate first digit of home phone : 0 and 1 is invalid
					    		var firstDigit = mobile.replace("(", "").substring(0, 1);
					    		if (firstDigit == 0 || firstDigit == 1) {
					    			return 'Phone number and area code can not begin with 0 or 1 and must be a valid phone number';
					    		}
					    	}
					    }
					}
					return "";
				}
			],
			validateOnBlur: true,
			group: "<%=IDPrefix%>ValidateSAApplicantInfo"
		});
		<%End If%>
		<%If EnableContactRelative Then%>
		$('#<%=IDPrefix%>txtRelativeFirstName').observer({
			validators: [
				function (partial) {
					if ($(this).closest("div.showfield-section").length > 0 && $(this).closest("div.showfield-section").hasClass("hidden")) return "";
					var text = $(this).val();
					if (Common.ValidateText(text) && /^[\sa-zA-Z\'-]+$/.test(text) == false) {
						return 'Enter a valid first name. Allowed characters: letters "A-Z", dashes, apostrophes and spaces.';
					}
					return "";
				}
			],
			validateOnBlur: true,
			group: "<%=IDPrefix%>ValidateSAApplicantInfo"
		});
		$('#<%=IDPrefix%>txtRelativeLastName').observer({
			validators: [
				function (partial) {
					if ($(this).closest("div.showfield-section").length > 0 && $(this).closest("div.showfield-section").hasClass("hidden")) return "";
					var text = $(this).val();
					if (Common.ValidateText(text) && /^[\sa-zA-Z\'-]+$/.test(text) == false) {
						return 'Enter a valid last name. Allowed characters: letters "A-Z", dashes, apostrophes and spaces.';
					}
					return "";
				}
			],
			validateOnBlur: true,
			group: "<%=IDPrefix%>ValidateSAApplicantInfo"
		});
		$('#<%=IDPrefix%>txtRelativePhone').observer({
			validators: [
				function (partial) {
					if ($(this).closest("div.showfield-section").length > 0 && $(this).closest("div.showfield-section").hasClass("hidden")) return "";
					var mobile = $(this).val();
					if (Common.ValidateText(mobile)) {
						if (is_foreign_phone) {
							return validateForeignPhone('<%=IDPrefix%>txtRelativePhone');
						} else {
							if (!Common.ValidatePhone(mobile)) {
								return "Valid phone number is required";
							} else {
								//validate first digit of home phone : 0 and 1 is invalid
								var firstDigit = mobile.replace("(", "").substring(0, 1);
								if (firstDigit == 0 || firstDigit == 1) {
									return 'Phone number and area code can not begin with 0 or 1 and must be a valid phone number';
								}
							}
						}
					}
					return "";
				}
			],
			validateOnBlur: true,
			group: "<%=IDPrefix%>ValidateSAApplicantInfo"
		});
		<%End If%>
	}

	function <%=IDPrefix%>ValidateSAApplicantInfo() {
		UpdateCSRFNumber(hfCSRF);
		var validator = true;
		if ($.lpqValidate("<%=IDPrefix%>ValidateSAApplicantInfo") == false) {
			validator = false;
		}
		<%If IsPrimaryApplicant AndAlso ShowBeneficiary Then%>
		if (<%=IDPrefix%>validateBeneficiary() == false) {
			validator = false;
		}
		<%End If%>
		
		<%If EnableAddress Then %>
		if (<%=IDPrefix%>ValidateSAAddress() == false) {
			validator = false;
		}
		<%End If%>
		<%If EnableEmployment Then%>
		if (<%=IDPrefix%>ValidateApplicantEmploymentXA() == false) {
			validator = false;
		}
		<%End If%>
		<%If EnableIDCard Then%>
		if (<%=IDPrefix%>ValidateApplicantIdXA() == false) {
			validator = false;
		}
		<%--if (<%=IDPrefix%>2ndValidateApplicantIdXA() == false) {
			validator = false;
		}--%>
		<%End If%>

		<%If EnableDocUpload then%>
    		if (typeof <%=IDPrefix%>docUploadObj != "undefined" && <%=IDPrefix%>docUploadObj != null && <%=IDPrefix%>docUploadObj.validateUploadDocument() == false) {
				validator = false;
			}
		<%End If%>

		if ($.lpqValidate("<%=IDPrefix%>ValidateApplicantQuestionsXA") === false) {
			validator = false;
		}
		if (validator) {
			if (window.ABR) {
				var formValues = prepareAbrFormValues("page<%=IDPrefix%>");
				if (window.ABR.FACTORY.evaluateRules("page<%=IDPrefix%>", formValues)) {
					return false;
				}
			}
			<%If NextPage.ToUpper() = "PAGEFS" then %>
			if ($("#divFundingTypeList a.btn-header-theme").length > 0 && xaProductFactory.hasFundedProducts()) {
				goToNextPage("#pageFS");
			} else {
				goToNextPage("#reviewpage");
			}
			<%Else%>
			goToNextPage("#<%=NextPage%>");
			<%End If%>
			
		} else {
			Common.ScrollToError();
		}
		return validator;
	}
	<%If EnableDocUpload then%>
	function <%=IDPrefix%>SetSADocUploadInfo(result) {
		if (result == null) result = {};
		<%=IDPrefix%>docUploadObj.setUploadDocument(result);
		return result;
	}
	<%End If%>

	function <%=IDPrefix%>SetSAApplicantInfo(result) {
		//var result = {};
		result.<%=IDPrefix%>Prefix = '<%=IDPrefix%>';
		result.<%=IDPrefix%>RoleType = '<%=RoleType%>';
		<%--result.<%=IDPrefix%>AccountName = '<%=Common.SafeEncodeString(AccountName)%>';
		result.<%=IDPrefix%>AccountCode = '<%=Common.SafeEncodeString(AccountCode)%>';--%>
		result.<%=IDPrefix%>IsJoint = '<%=IIf(IsJoint, "Y", "N")%>';
		<%If EnableMemberNumber Then%>
		if ($('#<%=IDPrefix%>txtMemberNumber').closest("div.showfield-section").length == 0 || $('#<%=IDPrefix%>txtMemberNumber').closest("div.showfield-section").hasClass("hidden") == false) {
			result.<%=IDPrefix%>MemberNumber = $('#<%=IDPrefix%>txtMemberNumber').val();	
		}
		<%End If%>
		result.<%=IDPrefix%>SSN = Common.GetSSN($('#<%=IDPrefix%>txtSSN').val());
		result.<%=IDPrefix%>FirstName = $('#<%=IDPrefix%>txtFName').val();
		result.<%=IDPrefix%>MiddleName = $('#<%=IDPrefix%>txtMName').val();
		result.<%=IDPrefix%>LastName = $('#<%=IDPrefix%>txtLName').val();
		result.<%=IDPrefix%>NameSuffix = $('#<%=IDPrefix%>ddlSuffix').val();
		result.<%=IDPrefix%>DOB = $('#<%=IDPrefix%>txtDOB').val();
		<%If EnableGender Then%>
		if ($('#<%=IDPrefix%>ddlGender').closest("div.showfield-section").length == 0 || $('#<%=IDPrefix%>ddlGender').closest("div.showfield-section").hasClass("hidden") == false) {
			result.<%=IDPrefix%>Gender = $('#<%=IDPrefix%>ddlGender').val();	
		}
		<%End If%>
		<%If EnableMotherMaidenName Then%>
		if ($('#<%=IDPrefix%>txtMotherMaidenName').closest("div.showfield-section").length == 0 || $('#<%=IDPrefix%>txtMotherMaidenName').closest("div.showfield-section").hasClass("hidden") == false) {
			result.<%=IDPrefix%>MotherMaidenName = $('#<%=IDPrefix%>txtMotherMaidenName').val();	
		}
		<%End If%>
		<%If EnableCitizenshipStatus Then%>
		if ($('#<%=IDPrefix%>ddlCitizenshipStatus').closest("div.showfield-section").length == 0 || $('#<%=IDPrefix%>ddlCitizenshipStatus').closest("div.showfield-section").hasClass("hidden") == false) {
			result.<%=IDPrefix%>CitizenshipStatus = $('#<%=IDPrefix%>ddlCitizenshipStatus').val();	
		}
		<%End If%>
		<%If EnableMaritalStatus Then%>
		if ($('#<%=IDPrefix%>ddlMaritalStatus').closest("div.showfield-section").length == 0 || $('#<%=IDPrefix%>ddlMaritalStatus').closest("div.showfield-section").hasClass("hidden") == false) {
			result.<%=IDPrefix%>MaritalStatus = $('#<%=IDPrefix%>ddlMaritalStatus').val();	
		}
		<%End If%>
		//result.<%=IDPrefix%>OfficialFamily = $('#<%=IDPrefix%>ddlOfficialFamily').val();
		<%If Not IsPrimaryApplicant AndAlso EnableRelationshipToPrimary Then%>
		if ($('#<%=IDPrefix%>ddlRelationToPrimaryApplicant').closest("div.showfield-section").length == 0 || $('#<%=IDPrefix%>ddlRelationToPrimaryApplicant').closest("div.showfield-section").hasClass("hidden") == false) {
			result.<%=IDPrefix%>RelationToPrimaryApplicant = $('#<%=IDPrefix%>ddlRelationToPrimaryApplicant').val();	
		}
		
		<%End If%>
		<%If IsPrimaryApplicant AndAlso ShowBeneficiary Then%>
		result.<%=IDPrefix%>Beneficiaries = JSON.stringify(<%=IDPrefix%>getBenficiaryInfo());
		<%End If%>
		<%If EnableAddress Then %>
		if ($('#<%=IDPrefix%>divPhysicalAddress').hasClass("hidden") == false) {
			<%=IDPrefix%>SetSAAddress(result);
		}
		
		<%End If%>
		<%If EnableContactInfo Then%>
		if ($('#<%=IDPrefix%>divContactInformation').hasClass("hidden") == false) {
			result.<%=IDPrefix%>ContactMethod = $('#<%=IDPrefix%>preferredContactMethod').val();
			result.<%=IDPrefix%>ContactEmail = $('#<%=IDPrefix%>txtContactEmail').val();
			result.<%=IDPrefix%>ContactHomePhone = $('#<%=IDPrefix%>txtContactHomePhone').val();
			result.<%=IDPrefix%>ContactMobilePhone = $('#<%=IDPrefix%>txtContactMobilePhone').val();
		}
		<%End If%>
		<%If EnableContactRelative Then%>
		if ($('#<%=IDPrefix%>divContactRelative').hasClass("hidden") == false) {
			result.<%=IDPrefix%>RelativeFirstName = $('#<%=IDPrefix%>txtRelativeFirstName').val();
			result.<%=IDPrefix%>RelativeLastName = $('#<%=IDPrefix%>txtRelativeLastName').val();
			result.<%=IDPrefix%>RelativePhone = $('#<%=IDPrefix%>txtRelativePhone').val();
			result.<%=IDPrefix%>RelativeEmail = $('#<%=IDPrefix%>txtRelativeEmail').val();
			result.<%=IDPrefix%>RelativeRelationship = $('#<%=IDPrefix%>txtRelativeRelationship').val();
			result.<%=IDPrefix%>RelativeAddress = $('#<%=IDPrefix%>txtRelativeAddress').val();
			result.<%=IDPrefix%>RelativeZip = $('#<%=IDPrefix%>txtRelativeZip').val();
			result.<%=IDPrefix%>RelativeCity = $('#<%=IDPrefix%>txtRelativeCity').val();
			result.<%=IDPrefix%>RelativeState = $('#<%=IDPrefix%>ddlRelativeState').val();
		}
		<%End If%>

        //get employee of lender
		<% If Not String.IsNullOrEmpty(EmployeeOfLenderDropdown) Then %>
		if ($("#<%=IDPrefix%>ddlEmployeeOfLender").closest(".ui-field-contain").hasClass("hidden") == false) {
			result.<%=IDPrefix%>EmployeeOfLender = $('#<%=IDPrefix%>ddlEmployeeOfLender option:selected').val();
		}
		<%  End If%>


		<%If EnableEmployment Then%>
		if ($('#<%=IDPrefix%>divEmploymentSection').hasClass("hidden") == false) {
			<%=IDPrefix%>SetApplicantEmployment(result);
		}
		<%End If %>
		<%If EnableIDCard Then%>
		if ($('#<%=IDPrefix%>divApplicantIdentification').hasClass("hidden") == false) {
			<%=IDPrefix%>SetApplicantID(result);
		}
		<%--<%=IDPrefix%>2ndSetApplicantID(result);--%>
		<%End If %>

		// To retrieve these, supply them in the overall CustomAnswers object and filter by prefix.
		//result.<%=IDPrefix%>ApplicantQuestionAnswers = JSON.stringify(<%=IDPrefix%>getXAApplicantQuestions());
		return result;
	}

	function <%=IDPrefix%>ScanAccept() {
		<%=IDPrefix%>docScanObj.scanAccept();
		goToNextPage("#page<%=IDPrefix%>");
	}

	function <%=IDPrefix%>ViewSAAplicantInfo() {
		var strHtml = "";
		var ssn = htmlEncode(Common.GetSSN($('#<%=IDPrefix%>txtSSN').val()));
		var firstName = htmlEncode($.trim($('#<%=IDPrefix%>txtFName').val()));
		var middleName = htmlEncode($.trim($('#<%=IDPrefix%>txtMName').val()));
		var lastName = htmlEncode($.trim($('#<%=IDPrefix%>txtLName').val()));
		var fullName = $.trim(firstName + " " + $.trim(middleName + " " + lastName));
		var suffix = $('#<%=IDPrefix%>ddlSuffix').val();
		var dob = $('#<%=IDPrefix%>txtDOB').val();
		var strApplicantInfoHtml = '<div class="row panel">';
		
		 
		if (fullName !== "") {
			strApplicantInfoHtml += '<div class="col-sm-6 col-xs-12"><div class="row"><div class="col-xs-6 text-right row-title"><span class="bold">Full Name</span></div><div class="col-xs-6 text-left row-data"><span>' + fullName + '</span></div></div></div>';
		}
		if (suffix !== "") {
			strApplicantInfoHtml += '<div class="col-sm-6 col-xs-12"><div class="row"><div class="col-xs-6 text-right row-title"><span class="bold">Suffix</span></div><div class="col-xs-6 text-left row-data"><span>' + suffix + '</span></div></div></div>';
        }
        if (ssn !== "") {
            strApplicantInfoHtml += '<div class="col-sm-6 col-xs-12"><div class="row"><div class="col-xs-6 text-right row-title"><span class="bold">SSN</span></div><div class="col-xs-6 text-left row-data"><span>' + ssn.replace(/^\d{5}/, "*****") + '</span></div></div></div>';
        }
		if (dob !== "") {
			strApplicantInfoHtml += '<div class="col-sm-6 col-xs-12"><div class="row"><div class="col-xs-6 text-right row-title"><span class="bold">Date of Birth</span></div><div class="col-xs-6 text-left row-data"><span>' + dob + '</span></div></div></div>';
		}
		<%If EnableMemberNumber Then%>
		if ($('#<%=IDPrefix%>txtMemberNumber').closest("div.showfield-section").length == 0 || $('#<%=IDPrefix%>txtMemberNumber').closest("div.showfield-section").hasClass("hidden") == false) {
			var memberNumber = htmlEncode($.trim($('#<%=IDPrefix%>txtMemberNumber').val()));
			if (memberNumber !== "") {
				strApplicantInfoHtml += '<div class="col-sm-6 col-xs-12"><div class="row"><div class="col-xs-6 text-right row-title"><span class="bold"><%:IIf(InstitutionType = CEnum.InstitutionType.BANK, "Account Number", "Member Number")%></span></div><div class="col-xs-6 text-left row-data"><span>' + memberNumber + '</span></div></div></div>';
			}
		}
		<%End If%>
		<%If EnableGender Then%>
		if ($('#<%=IDPrefix%>ddlGender').closest("div.showfield-section").length == 0 || $('#<%=IDPrefix%>ddlGender').closest("div.showfield-section").hasClass("hidden") == false) {
			var gender = $('#<%=IDPrefix%>ddlGender').val();
			if (gender !== "") {
				strApplicantInfoHtml += '<div class="col-sm-6 col-xs-12"><div class="row"><div class="col-xs-6 text-right row-title"><span class="bold">Gender</span></div><div class="col-xs-6 text-left row-data"><span>' + gender + '</span></div></div></div>';
			}
		}
		<%End If%>
		
		<%If EnableMotherMaidenName Then%>
		if ($('#<%=IDPrefix%>txtMotherMaidenName').closest("div.showfield-section").length == 0 || $('#<%=IDPrefix%>txtMotherMaidenName').closest("div.showfield-section").hasClass("hidden") == false) {
			var mmn = htmlEncode($('#<%=IDPrefix%>txtMotherMaidenName').val());
			if (mmn !== "") {
				strApplicantInfoHtml += '<div class="col-sm-6 col-xs-12"><div class="row"><div class="col-xs-6 text-right row-title"><span class="bold">MMN</span></div><div class="col-xs-6 text-left row-data masking-text"><span>' + mmn + '</span><span onclick="toggleReveal(this)"></span></div></div></div>';
			}
		}
		<%End If%>
		<%If EnableCitizenshipStatus Then%>
		if ($('#<%=IDPrefix%>ddlCitizenshipStatus').closest("div.showfield-section").length == 0 || $('#<%=IDPrefix%>ddlCitizenshipStatus').closest("div.showfield-section").hasClass("hidden") == false) {
			var citizenshipStatus = $('#<%=IDPrefix%>ddlCitizenshipStatus').val();
			if ($.trim(citizenshipStatus) !== "") {
				strApplicantInfoHtml += '<div class="col-sm-6 col-xs-12"><div class="row"><div class="col-xs-6 text-right row-title"><span class="bold">Citizenship Status</span></div><div class="col-xs-6 text-left row-data"><span>' + citizenshipStatus + '</span></div></div></div>';
			}
		}
		<%End If%>
		<%If EnableMaritalStatus Then%>
		if ($('#<%=IDPrefix%>ddlMaritalStatus').closest("div.showfield-section").length == 0 || $('#<%=IDPrefix%>ddlMaritalStatus').closest("div.showfield-section").hasClass("hidden") == false) {
			var maritalStatus = $('#<%=IDPrefix%>ddlMaritalStatus').val();
			if (maritalStatus !== "") {
				strApplicantInfoHtml += '<div class="col-sm-6 col-xs-12"><div class="row"><div class="col-xs-6 text-right row-title"><span class="bold">Marital Status</span></div><div class="col-xs-6 text-left row-data"><span>' + maritalStatus + '</span></div></div></div>';
			}
		}
		<%End If%>
		if ($("#<%=IDPrefix%>ddlEmployeeOfLender").length > 0 && $("#<%=IDPrefix%>ddlEmployeeOfLender").closest(".ui-field-contain").hasClass("hidden") == false && $('#<%=IDPrefix%>ddlEmployeeOfLender').val() != "") {
			strApplicantInfoHtml += '<div class="col-sm-6 col-xs-12"><div class="row"><div class="col-xs-6 text-right row-title"><span class="bold">Employee of Lender</span></div><div class="col-xs-6 text-left row-data"><span>' + $('#<%=IDPrefix%>ddlEmployeeOfLender option:selected').text() + '</span></div></div></div>';
        }
		<%If Not IsPrimaryApplicant AndAlso EnableRelationshipToPrimary Then%>
		if ($('#<%=IDPrefix%>ddlRelationToPrimaryApplicant').closest("div.showfield-section").length == 0 || $('#<%=IDPrefix%>ddlRelationToPrimaryApplicant').closest("div.showfield-section").hasClass("hidden") == false) {
			var relationToPrimaryApplicant = $('#<%=IDPrefix%>ddlRelationToPrimaryApplicant').val();
			if (relationToPrimaryApplicant !== "") {
				strApplicantInfoHtml += '<div class="col-sm-6 col-xs-12"><div class="row"><div class="col-xs-6 text-right row-title"><span class="bold">Relation To Primary Applicant</span></div><div class="col-xs-6 text-left row-data"><span>' + relationToPrimaryApplicant + '</span></div></div></div>';
			}
		}
		<%End If%>
		strApplicantInfoHtml += "</div>";
		strHtml += strApplicantInfoHtml;
		<%If IsPrimaryApplicant AndAlso ShowBeneficiary Then%>
		var beneficiries = <%=IDPrefix%>viewBeneficiaries();
		if (beneficiries !== "") {
			strHtml += '<div class="row"><div class="row-title section-heading"><span class="bold">Benificiary Information</span></div></div><div class="row panel">' + beneficiries + "</div>";
		}
		
		<%End If%>
		<%If EnableAddress Then%>
		if ($('#<%=IDPrefix%>divPhysicalAddress').hasClass("hidden") == false) {
			strHtml += '<div class="row"><div class="row-title section-heading"><span class="bold">Address Information</span></div></div><div class="row panel">' + <%=IDPrefix%>ViewAddress() + "</div>";
		}
		<%End If%>
		<%If EnableContactInfo Then%>
		if ($('#<%=IDPrefix%>divContactInformation').hasClass("hidden") == false) {
			var contactMethod = $.trim($('#<%=IDPrefix%>preferredContactMethod option:selected').text());
			var email = $.trim($('#<%=IDPrefix%>txtContactEmail').val());
			var homePhone = $.trim($('#<%=IDPrefix%>txtContactHomePhone').val());
			var mobilePhone = $.trim($('#<%=IDPrefix%>txtContactMobilePhone').val());
			var strContactInfo = '<div class="row"><div class="row-title section-heading"><span class="bold">Contact Information</span></div></div><div class="row panel">';
			if ($.trim($('#<%=IDPrefix%>preferredContactMethod').val()) != "") {
				strContactInfo += '<div class="col-sm-6 col-xs-12"><div class="row"><div class="col-xs-6 text-right row-title"><span class="bold">Preferred Contact Method</span></div><div class="col-xs-6 text-left row-data"><span>' + contactMethod + '</span></div></div></div>';
			}
			if (email != "") {
				strContactInfo += '<div class="col-sm-6 col-xs-12"><div class="row"><div class="col-xs-6 text-right row-title"><span class="bold">Email</span></div><div class="col-xs-6 text-left row-data"><span>' + email + '</span></div></div></div>';
			}
			if (homePhone != "") {
				strContactInfo += '<div class="col-sm-6 col-xs-12"><div class="row"><div class="col-xs-6 text-right row-title"><span class="bold">Primary/Home Phone</span></div><div class="col-xs-6 text-left row-data"><span>' + homePhone + '</span></div></div></div>';
			}
			if (mobilePhone != "") {
				strContactInfo += '<div class="col-sm-6 col-xs-12"><div class="row"><div class="col-xs-6 text-right row-title"><span class="bold">Secondary/Mobile Phone</span></div><div class="col-xs-6 text-left row-data"><span>' + mobilePhone + '</span></div></div></div>';
			}
			strContactInfo += "</div>";
			strHtml += strContactInfo;
		}
		<%End If%>
		<%If EnableContactRelative Then%>
		if ($('#<%=IDPrefix%>divContactRelative').hasClass("hidden") == false) {
			var relativeFirstName = $.trim($('#<%=IDPrefix%>txtRelativeFirstName').val());
			var relativeLastName = $.trim($('#<%=IDPrefix%>txtRelativeLastName').val());
			var relativeFullName = $.trim(relativeFirstName + " " + relativeLastName);
			var relativePhone = $.trim($('#<%=IDPrefix%>txtRelativePhone').val());
			var relativeEmail = $.trim($('#<%=IDPrefix%>txtRelativeEmail').val());
			var relativeRelationship = $.trim($('#<%=IDPrefix%>txtRelativeRelationship').val());
			var strRelative = "";
			if (relativeFullName != "") {
				strRelative += '<div class="col-sm-6 col-xs-12"><div class="row"><div class="col-xs-6 text-right row-title"><span class="bold">Full Name</span></div><div class="col-xs-6 text-left row-data"><span>' + relativeFullName + '</span></div></div></div>';
			}
			if (relativePhone != "") {
				strRelative += '<div class="col-sm-6 col-xs-12"><div class="row"><div class="col-xs-6 text-right row-title"><span class="bold">Phone</span></div><div class="col-xs-6 text-left row-data"><span>' + relativePhone + '</span></div></div></div>';
			}
			if (relativeEmail != "") {
				strRelative += '<div class="col-sm-6 col-xs-12"><div class="row"><div class="col-xs-6 text-right row-title"><span class="bold">Email</span></div><div class="col-xs-6 text-left row-data"><span>' + relativeEmail + '</span></div></div></div>';
			}
			if (relativeRelationship != "") {
				strRelative += '<div class="col-sm-6 col-xs-12"><div class="row"><div class="col-xs-6 text-right row-title"><span class="bold">Relationship</span></div><div class="col-xs-6 text-left row-data"><span>' + relativeRelationship + '</span></div></div></div>';
			}
			var relativeAddress = $.trim($('#<%=IDPrefix%>txtRelativeAddress').val());
			var relativeZip = htmlEncode($.trim($('#<%=IDPrefix%>txttxtRelativeZip').val()));
			var relativeCity = htmlEncode($.trim($('#<%=IDPrefix%>txttxtRelativeCity').val()));
			var relativeState = $.trim($('#<%=IDPrefix%>ddltxtRelativeState').val());
			var relativeCountry = $.trim($("#<%=IDPrefix%>ddltxtRelativeCountry option:selected").val());
			if (relativeCity != "") {
				relativeAddress += ", " + relativeCity;
			}
			if (relativeState != "") {
				relativeAddress += ", " + relativeState;
			}
			if (relativeZip != "") {
				relativeAddress += ", " + relativeZip;
			}
			if (relativeCountry != "USA") {
				relativeAddress += " " + relativeCountry;
			}
			strRelative += relativeAddress;
			if ($.trim(strRelative) != "") {
				strRelative = '<div class="row"><div class="row-title section-heading"><span class="bold">Relative Information</span></div></div><div class="row panel">' + strRelative + '</div>';
			}
			strHtml += strRelative;
		}
		<%End If%>
		<%If EnableEmployment Then%>
		if ($('#<%=IDPrefix%>divEmploymentSection').hasClass("hidden") == false) {
			strHtml += '<div class="row"><div class="row-title section-heading"><span class="bold">Employment Information</span></div></div><div class="row panel">' + <%=IDPrefix%>ViewEmploymentInfo() + "</div>";
			var prevEmployment = <%=IDPrefix%>ViewPrevEmploymentInfo();
			if (prevEmployment != "") {
				strHtml += '<div class="row"><div class="row-title section-heading"><span class="bold">Previous Employment Information</span></div></div><div class="row panel">' + prevEmployment + "</div>";
			}
		}
		<%End If%>
		<%If EnableIDCard Then%>
		if ($('#<%=IDPrefix%>divApplicantIdentification').hasClass("hidden") == false) {
			strHtml += '<div class="row"><div class="row-title section-heading"><span class="bold">Identification Information</span></div></div><div class="row panel">' + <%=IDPrefix%>ViewPrimaryIdentification() + "</div>";
		}
		<%End If%>
		var strViewApplicantQuestion = <%=IDPrefix%>ViewApplicantQuestion.call($(".<%=IDPrefix%>ViewApplicantQuestion"));
		if (strViewApplicantQuestion != "") {
			strHtml += '<div class="row"><div class="row-title section-heading"><span class="bold">Additional Information</span></div></div><div class="row panel <%=IDPrefix%>ViewApplicantQuestion">' + strViewApplicantQuestion + "</div>";	
		}
		return strHtml;
	}

	var <%=IDPrefix%>docScanObj;
	$(function () {
		<%=IDPrefix%>docScanObj = new LPQDocScan('<%=IDPrefix%>');
		if (isMobile.any() && !isMobile.Windows()) {//moz-text-security does not works on window devices
			$("#<%=IDPrefix%>txtSSN1,#<%=IDPrefix%>txtSSN2")
				.attr("type", "tel")
				.addClass("mask-password");
		} else {
			$("#<%=IDPrefix%>txtSSN1,#<%=IDPrefix%>txtSSN2")
				.attr("type", "password");
		}

		if (hasForeignTypeApp()) {
			$("#<%=IDPrefix%>lblSSN").removeClass("RequiredIcon"); // no required ->remove red stard
		} else {
			$("#<%=IDPrefix%>lblSSN").addClass("RequiredIcon"); // required -> add restar
		}
		$('.ui-input-ssn input', "#page<%=IDPrefix%>").on('focusout', function () {
			<%=IDPrefix%>updateHiddenSSN();
		});
		$("#<%=IDPrefix%>txtDOB").datepicker({
			showOn: "button",
			changeMonth: true,
			changeYear: true,
			yearRange: "-100:+0",
			buttonImage: "/images/calendar.png",
			buttonImageOnly: true,
			onSelect: function (value, inst) {
				var date = $(this).datepicker('getDate');
				$("#<%=IDPrefix%>txtDOB2").val(padLeft(date.getDate(), 2));
				$("#<%=IDPrefix%>txtDOB1").val(padLeft(date.getMonth() + 1, 2));
				$("#<%=IDPrefix%>txtDOB3").val(padLeft(date.getFullYear(), 4));

				$("#<%=IDPrefix%>txtDOB").datepicker("hide");
				$("#<%=IDPrefix%>txtDOB1").trigger("focus").trigger("blur");
			}
		});
		$('#<%=IDPrefix%>divDOBStuff div.ui-input-date input').on('focusout', function () {
			var maxlength = parseInt($(this).attr('maxlength'));
			// add 0 if value < 10
			$(this).val(padLeft($(this).val(), maxlength));
			<%=IDPrefix%>updateHiddenDOB();
		});
		
		if (is_foreign_phone) {
			// international country code
			//by default it display separateDialCode dropdown
			//don't want to display separateDialCode dropdown, just set separateDialCode: false
			$("#<%=IDPrefix%>txtContactHomePhone").intlTelInput({
				separateDialCode: false
			}); // don't know why it's not set default for this one
			$('#<%=IDPrefix%>txtContactMobilePhone').intlTelInput({
				separateDialCode: false
			});
			$('#<%=IDPrefix%>txtRelativePhone').intlTelInput({
				separateDialCode: false
			});

			// change the text
			//$('.phone_format', "#page<%=IDPrefix%>").html("");
			$('.phone_format', "#page<%=IDPrefix%>").parent().addClass("hidden");

			// remove mask
			if (!isMobile.Android()) {
				$('.inphone', "#page<%=IDPrefix%>").inputmask('remove');
			} else {
				$('input.inphone', "#page<%=IDPrefix%>").off('blur').off('keyup').off('keydown');
			}
			$('input.inphone', "#page<%=IDPrefix%>").numeric();

			//set hidden phone country to "us" by default
			$("#<%=IDPrefix%>hdContactHomePhoneCountry").val("us");
			$("#<%=IDPrefix%>hdContactMobilePhoneCountry").val("us");
			$("#<%=IDPrefix%>hdRelativePhoneCountry").val("us");
		}
		
		//update hidden Phone Country
		$('#<%=IDPrefix%>divContactHomePhone').on('click', '.flag-container', function () {
			<%=IDPrefix%>updateHiddenPhoneCountry($(this), $('#<%=IDPrefix%>hdContactHomePhoneCountry'));
		});
		$('#<%=IDPrefix%>divContactMobilePhone').on('click', '.flag-container', function () {
			<%=IDPrefix%>updateHiddenPhoneCountry($(this), $('#<%=IDPrefix%>hdContactMobilePhoneCountry'));
		});
		$('#<%=IDPrefix%>divRelativePhone').on('click', '.flag-container', function () {
			<%=IDPrefix%>updateHiddenPhoneCountry($(this), $('#<%=IDPrefix%>hdRelativePhoneCountry'));
		});
		EMPLogic<%=IDPrefix%> = $.extend(true, {}, EMPLogic);
		EMPLogic<%=IDPrefix%>.init('<%=IDPrefix%>ddlEmploymentStatus', '<%=IDPrefix%>', 'page<%=IDPrefix%>', '<%=IDPrefix%>divEmploymentStatusRest', '<%=IIf(CollectJobTitleOnly, "JobTitleOnly", "")%>');
		$('#page<%=IDPrefix%>').on("pageshow", function () {
			EMPLogic<%=IDPrefix%>.refreshHtml();
		});
		EMPLogic<%=IDPrefix%>PREV = $.extend(true, {}, EMPLogic);
		EMPLogic<%=IDPrefix%>PREV.init('<%=IDPrefix%>prev_ddlEmploymentStatus', '<%=IDPrefix%>prev_', 'page<%=IDPrefix%>', '<%=IDPrefix%>prev_divEmploymentStatusRest');
		<%=IDPrefix%>registerSAApplicantInfoValidator();
		$('#page<%=IDPrefix%>').on('pageshow', function () {
      		pushToPagePaths('page<%=IDPrefix%>');
		});
    }); 
    function <%=IDPrefix%>AutofillData_SA() {
        if (typeof SAPREFIXLIST == "undefined") return; //
        for (var saIndex = 0; saIndex < SAPREFIXLIST.length; saIndex++) {
            //only autofill 3 applicants
            if (saIndex >= 3) return;
            //this data for first applicant
            var saSSN3 = "", saDOB3 = "", saHomePhone = "", saCellPhone = "", saEmail = "", saIDNumber = "";
            if (SAPREFIXLIST[saIndex] == '<%=IDPrefix%>') {
                var minAge =Number('<%=BlockApplicantAgeUnder%>');
                var maxAge = Number('<%=BlockApplicantAgeOver%>');               
                if (maxAge > 0) {
                    saDOB3 = (new Date().getFullYear() - maxAge + 1).toString();
                } else {//no max age
                    if (minAge > 0) {
                        saDOB3 = (new Date().getFullYear() - minAge).toString();
                    }
                }
                if (saIndex == 0) {
                    saSSN3 = "0020",  saHomePhone = "7142342345", saCellPhone = "7144442345", saEmail = "saTest1@test.com", saIDNumber = "D123456";
                } else if (saIndex == 1) { //second applicant
                    saSSN3 = "0021",  saHomePhone = "7145553333", saEmail = "saTest2@test.com", saIDNumber = "C111222";
                } else if (saIndex == 2) { //third applicant
                    saSSN3 = "0022", saHomePhone = "71455566666", saEmail = "saTest3@test.com", saIDNumber = "B454555";
                }
            } else {
                continue;
            }
            if (saDOB3 == "") {
                saDOB3 = "2000";
            }          
            $('#<%=IDPrefix%>txtSSN1').val("000");
            $('#<%=IDPrefix%>txtSSN2').val("00");
            $('#<%=IDPrefix%>txtSSN3').val(saSSN3);
            $('#<%=IDPrefix%>txtSSN').val("000-00-"+saSSN3);
            $('#<%=IDPrefix%>txtDOB1').val("11");
            $('#<%=IDPrefix%>txtDOB2').val("11");
            $('#<%=IDPrefix%>txtDOB3').val(saDOB3);
            $('#<%=IDPrefix%>txtDOB').val("11/11/"+saDOB3);
		    <%If EnableGender Then%>
                $('#<%=IDPrefix%>ddlGender').val('MALE');
                $('#<%=IDPrefix%>ddlGender').selectmenu().selectmenu("refresh");
		    <%End If%>   
             <%If EnableCitizenshipStatus Then%>
                $('#<%=IDPrefix%>ddlCitizenshipStatus').val("US CITIZEN");
                $('#<%=IDPrefix%>ddlCitizenshipStatus').selectmenu().selectmenu("refresh");
		    <%End If%>
            <%If EnableMaritalStatus Then%>
                $('#<%=IDPrefix%>ddlMaritalStatus').val("UNMARRIED");
                $('#<%=IDPrefix%>ddlMaritalStatus').selectmenu().selectmenu("refresh");
		    <%End If%>        
            <%If EnableAddress Then %>
                if ($('#<%=IDPrefix%>divPhysicalAddress').hasClass("hidden") == false) {
                    if (typeof autoFillData_Address != "undefined") {
                        <%=IDPrefix%>autoFillData_Address();
                    }
                }
		    <%End If%>
             <%If EnableContactInfo Then%>
                if ($('#<%=IDPrefix%>divContactInformation').hasClass("hidden") == false) {
                    $('#<%=IDPrefix%>preferredContactMethod').val('EMAIL');
                    $('#<%=IDPrefix%>preferredContactMethod').selectmenu().selectmenu("refresh");
                    $('#<%=IDPrefix%>txtContactEmail').val(saEmail);
                    $('#<%=IDPrefix%>txtContactHomePhone').val(saHomePhone);
                    $('#<%=IDPrefix%>txtContactMobilePhone').val(saCellPhone);
                }
		    <%End If%>
            <%If EnableEmployment Then%>
                if ($('#<%=IDPrefix%>divEmploymentSection').hasClass("hidden") == false) {
                    $('#<%=IDPrefix%>ddlEmploymentStatus').val("HOMEMAKER");  
                    $('#page<%=IDPrefix%>').on('pageshow', function () {
                        EMPLogic<%=IDPrefix%>.refreshHtml();
                        if ($('#<%=IDPrefix%>txtJobTitle').val() == "") {
                            $('#<%=IDPrefix%>txtJobTitle').val('HOME');
                              if ($('select#<%=IDPrefix%>txtJobTitle').length>0){
                                  $('#<%=IDPrefix%>txtJobTitle').selectmenu().selectmenu("refresh");
                              }
                         }         
                       });
                    $('#<%=IDPrefix%>txtGrossMonthlyIncome').val("4000").trigger("blur");                
                }
		    <%End If %>
           <%If EnableIDCard Then%>
                if ($('#<%=IDPrefix%>divApplicantIdentification').hasClass("hidden") == false) {
                    //show id card
                    $('#<%=IDPrefix%>ddlIDCardType').val("DRIVERS_LICENSE");
                    $('#<%=IDPrefix%>txtIDCardNumber').val(saIDNumber);
                    $('#<%=IDPrefix%>ddlIDState').val("CA");
                    $('#<%=IDPrefix%>txtIDDateIssued1').val("04");
                    $('#<%=IDPrefix%>txtIDDateIssued2').val("04");
                    $('#<%=IDPrefix%>txtIDDateIssued3').val("2017");
                     $('#<%=IDPrefix%>txtIDDateIssued').val("04/04/2017");
                    $('#<%=IDPrefix%>txtIDDateExpire1').val("04");
                    $('#<%=IDPrefix%>txtIDDateExpire2').val("04");
                    $('#<%=IDPrefix%>txtIDDateExpire3').val("2021");
                    $('#<%=IDPrefix%>txtIDDateExpire').val("04/04/2021");         
                }
            <%End If %>
        } //end for
    }
</script>