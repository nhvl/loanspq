﻿
Partial Class xa_Inc_saAccountInfo
	Inherits CBaseUserControl
	Public Property LogoUrl As String
	Public Property StateDropdown As String = ""
	Public Property EnabledAddressVerification As Boolean
	Public Property EnableOccupancyStatus As Boolean = True
	Public Property OccupyingLocationDropdown As String
	Public Property CollectDescriptionIfOccupancyStatusIsOther As Boolean
	Public Property LiveYearsDropdown As String
	Public Property LiveMonthsDropdown As String

	Public Property PreviousPage As String = ""
	Public Property NextPage As String = ""
	Public Property Availability As String
	Public Property EnableMailingAddress As Boolean = True
	Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
		Dim addressCtrl As xa_Inc_saAddress = CType(LoadControl("~/xa/Inc/saAddress.ascx"), xa_Inc_saAddress)
		addressCtrl.IDPrefix = IDPrefix
		addressCtrl.StateDropdown = StateDropdown
		addressCtrl.EnableOccupancyStatus = EnableOccupancyStatus
		addressCtrl.EnabledAddressVerification = EnabledAddressVerification
		addressCtrl.OccupyingLocationDropdown = OccupyingLocationDropdown
		addressCtrl.CollectDescriptionIfOccupancyStatusIsOther = CollectDescriptionIfOccupancyStatusIsOther
		addressCtrl.LiveYearsDropdown = LiveYearsDropdown
		addressCtrl.LiveMonthsDropdown = LiveMonthsDropdown
		addressCtrl.EnableMailingAddress = EnableMailingAddress
		addressCtrl.Header = HeaderUtils.RenderPageTitle(14, "Current Physical Address", True)
		addressCtrl.MappedShowFieldLoanType = MappedShowFieldLoanType
		plhAddress.Controls.Add(addressCtrl)

	End Sub
End Class
