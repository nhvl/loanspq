﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="SpecialAccountSelector.ascx.vb" Inherits="xa_Inc_SpecialAccountSelector" %>
<%@ Import Namespace="LPQMobile.Utils" %>
<div id="specialAccountSelectorPage" data-role="dialog" data-history="false" data-close-btn="none" style="width: 100%;">
	<div data-role="header">
		<button class="header-hidden-btn">.</button>
		<div class="page-header-title">Special Accounts</div>
		<%If String.IsNullOrWhiteSpace(ScenarioName) Then%>	
		<div tabindex="0" onclick="SAS.FACTORY.closeSpecialDialog(this, 'mainPage')" class="header_theme2 ui-btn-right" data-role="none">
			<%=HeaderUtils.IconClose %><span style="display: none;">close</span>
		</div>
		<%End If%>
	</div>
	<div data-role="content">
			<div data-role="fieldcontain">
            <label for ="ddlSpecialAccountType"><%=HeaderUtils.RenderPageTitle(0, "Select your account type", True)%></label>
			<select id="ddlSpecialAccountType" <%=IIf(AccountTypeDisabled = True, "disabled=""disabled""", "")%>><%=Common.RenderDropdownlistWithEmpty(SpecialAccountTypeDic, "", "--Please select--", PreselectedAccountType)%></select> 
		</div>
		<div class="account-role-list-wrapper hidden">
			<%=HeaderUtils.RenderPageTitle(0, "Add your applicant roles", True)%>
			<% For Each type In SpecialAccountTypeList
					If type.Roles IsNot Nothing AndAlso type.Roles.Any(Function(r) r.IsActive = True) Then
			%>
					<div data-special-account-type="<%=type.AccountCode%>" class="hidden">
						<%For Each role In type.Roles.Where(Function(r) r.IsActive = true)%>
							<div class="ui-btn ui-corner-all btn-header-theme-ext special-role-item">
								<span class="<%=IIf(role.InstanceMin > 0, "require-ico", "")%>"><%=CEnum.MapSpecialRoleValueToDisplayName(role.RoleType)%></span>
								<div class="btn-add" data-instance-min="<%=role.InstanceMin%>" data-instance-max="<%=role.InstanceMax%>" data-role-type="<%=role.RoleType%>" onclick="SAS.FACTORY.openSpecialAccountEditorDialog(false, this)" tabindex="0"><%=HeaderUtils.IconClose %></div>
							</div>		
						<%Next%>
						<%If type.Roles.Any(Function(r) r.InstanceMin > 0) Then%>
						<div style="text-align: center;"><span class='require-span'>*</span>Required Roles</div>
						<%End If%>
					</div>
			<%
			End If
			Next%>
			<%--<div class="ui-btn ui-corner-all btn-header-theme-ext">
				<span class="require-ico">Role A</span>
				<div class="btn-add" tabindex="0"><i class="fa fa-plus-circle"></i></div>
			</div>
			<div class="ui-btn ui-corner-all btn-header-theme-ext">
				<span class="require-ico">Role A</span>
				<div class="btn-add" tabindex="0"><i class="fa fa-plus-circle"></i></div>
			</div>
			<div class="ui-btn ui-corner-all btn-header-theme-ext">
				<span class="require-ico">Role A</span>
				<div class="btn-add" tabindex="0"><i class="fa fa-plus-circle"></i></div>
			</div>--%>
			
		</div>
		<div class="selected-roles-wrapper hidden">
			<div style="margin-right: 25px;">
			<%=HeaderUtils.RenderPageTitle(0, "Your applicants", True)%><a class="guide-btn header_theme2 abort-renameable" href="#popSpecialAccountSelectedRole" data-position-to="window" data-rel="popup"><em class="fa fa-question-circle"></em></a>
			</div>
			<div id="divSelectedSpecialAccountRoles">
				<%--<div class="account-role-item-wrapper">
					<div class="ui-btn ui-corner-all account-role-item"><span title="(Primary Applicant)">Role A</span></div>
					<div class="ui-btn ui-corner-all role-attr-item">
						<label class="custom-checkbox">
							<input type="checkbox" data-role="none"/>	
							<span></span>
						</label>
						<label for="">Add joint applicant</label>
					</div>
					<div class="btn-remove" tabindex="0"></div>
				</div>
				<div class="account-role-item-wrapper">
					<div class="ui-btn ui-corner-all account-role-item"><span>Role A (Primary Applicant)</span></div>
					<div class="ui-btn ui-corner-all role-attr-item">
						<label class="custom-checkbox">
							<input type="checkbox" data-role="none"/>	
							<span></span>
						</label>
						<label for="">Add joint applicant</label>
					</div>
					<div class="ui-btn ui-corner-all role-attr-item">
						<label class="custom-checkbox">
							<input type="checkbox" data-role="none"/>	
							<span></span>
						</label>
						<label for="">Add joint applicant</label>
					</div>
					<div class="btn-remove" tabindex="0"></div>
				</div>
				<div class="account-role-item-wrapper">
					<div class="ui-btn ui-corner-all account-role-item"><span>Role A (Primary Applicant)</span></div>
					<div class="btn-remove" tabindex="0"></div>
				</div>--%>
			</div> 
			<div id="popSpecialAccountSelectedRole" data-role="popup" data-dismissible="true" data-history="false" style="max-width: 400px;">
				<div data-role="content">
					<div class="row">
						<div class="col-xs-12 header_theme2">
							<a class="pull-right svg-btn" data-rel="back" href="#" ><%=HeaderUtils.IconClose %><span style="display: none;">close</span></a>
						</div>
					</div>
					<div class="row">
						<div class="col-sm-12">
							<div style="margin: 10px 0; font-weight: normal;">
								<p>The first role in the list is considered to be the primary applicant.</p>
								<p>If a selected role is eligible for a joint applicant, a checkbox will appear for that role. Checking this box will create a joint applicant with the same role. The financial debts, income, and assets of both the primary and secondary applicant will be combined for qualification.</p>
							</div>     
						</div>    
					</div>
				</div>
				<div class="div-continue" style="text-align: center;"><a href="#" data-transition="slide" data-rel="back" type="button" data-role="button" class="div-continue-button">OK</a></div>
			</div>
		</div>
		<div class ="div-continue">
			<form id="frmSpecialAccountSelector" action="/xa/xpressApp.aspx" method="GET">
				<input type="hidden" name="lenderref" value="<%=LenderRef%>"/>
				<input type="hidden" name="type" value=""/>
				<%For Each item In ParseParams(DefaultParams)%>
				<input type="hidden" name="<%=item.Key%>" value="<%=item.Value%>"/>
				<%Next%>
			</form>
			<a href="#"  data-transition="slide" onclick="SAS.FACTORY.validate()" type="button" class="div-continue-button ui-btn">Continue</a> 	
			<%If String.IsNullOrWhiteSpace(ScenarioName) Then%>	
			<span>Or</span> <a href="#" class ="div-goback ui-btn" onclick="SAS.FACTORY.closeSpecialDialog(this, 'accountSelectorPage')" ><span class="hover-goback"> Go Back</span></a>   
			<%End If%>
		</div>    
	</div>
</div>
<div id="specialAccountEditorDialog" data-role="dialog" data-history="false" data-close-btn="none" class="account-editor-dialog">
	<div data-role="header">
		<button class="header-hidden-btn">.</button>
		<div class="page-header-title"><span class="edit-label">Edit</span><span>Add</span> Applicant</div>
		<div tabindex="0" onclick="SAS.FACTORY.closeSpecialDialog(this, 'specialAccountSelectorPage')" class="header_theme2 ui-btn-right" data-role="none">
			<%=HeaderUtils.IconClose %><span style="display: none;">close</span>
		</div>
	</div>
	<div data-role="content">
		<%=HeaderUtils.RenderPageTitle(0, "Applicant", True)%>
		<div data-role="fieldcontain">
			<label for="txtNewSpecialFName" class="RequiredIcon">First Name</label>
			<input type="text" id="txtNewSpecialFName" maxlength="50"/>
		</div>
		<div data-role="fieldcontain">
			<label for="txtNewSpecialLName" class="RequiredIcon">Last Name</label>
			<input type="text" id="txtNewSpecialLName" maxlength="50"/>
		</div>
		<div data-role="fieldcontain">
			<label for="ddlNewSpecialRole" class="RequiredIcon">Role</label>
			<select id="ddlNewSpecialRole"></select>
		</div>
		<div class="has-joint-button">
			<a id="lnkSpecialHasJointApp" href="#" data-mode="self-handle-event" style="cursor: pointer; font-weight: bold;" class="header_theme2 ui-link chevron-circle-right-before"><span class="add-label">Add</span><span>Remove</span> Joint Applicant</a>
		</div>
		<div>
			<%=HeaderUtils.RenderPageTitle(0, "Joint Applicant", True)%>
			<div data-role="fieldcontain">
				<label for="txtNewSpecialJointFName" class="RequiredIcon">First Name</label>
				<input type="text" id="txtNewSpecialJointFName" maxlength="50"/>
			</div>
			<div data-role="fieldcontain">
				<label for="txtNewSpecialJointLName" class="RequiredIcon">Last Name</label>
				<input type="text" id="txtNewSpecialJointLName" maxlength="50"/>
			</div>
		</div>
		<div style="text-align: center; margin-top: 10px;"><span class='require-span'>*</span>Required</div>
		<div class ="div-continue row">
			<div class="col-xs-6">
				<a tabindex="0" href="#" type="button" class="div-continue-button ui-btn cancel-btn" onclick="SAS.FACTORY.closeSpecialAccountEditorDialog()">Cancel</a>   
			</div>
			<div class="col-xs-6">
				<a tabindex="0" href="#" data-transition="slide" type="button" class="div-continue-button ui-btn" onclick="SAS.FACTORY.submitSpecialAccountEditorDialog()"><span class="edit-label">Done</span><span>Add</span></a> 
			</div>
		</div>    
	</div>
</div>
<script type="text/javascript">
	(function (SAS, $, undefined) {
		var SPECIALACCOUNTTYPELIST = <%=Newtonsoft.Json.JsonConvert.SerializeObject(SpecialAccountTypeList)%>;
		SAS.DATA = {};
        SAS.FACTORY = {};
        SAS.FACTORY.openSelectorDialog = function (type) {
			$("#frmSpecialAccountSelector input[name='type']").val(type);
			$.mobile.changePage("#specialAccountSelectorPage", {
				transition: "fade",
				reverse: false,
				changeHash: false
			});
        };
		SAS.FACTORY.closeSpecialDialog = function(sourceEle, destPage) {
			$("#frmSpecialAccountSelector input[name='type']").val("");
			if ($("#" + destPage).length == 0) destPage = 'mainPage';
			$.mobile.changePage("#" + destPage, {
				transition: "fade",
				reverse: false,
				changeHash: false
			});
		}
		SAS.FACTORY.openSpecialAccountEditorDialog = function(isEdit, sourceEle) {
			cleanUpSpecialAccountEditorDialog();
			var selectedAccountName = $("#ddlSpecialAccountType").val();
			var $ddlNewSpecialRole = $("#ddlNewSpecialRole");
			var $sourceEle = $(sourceEle);
			$("#specialAccountEditorDialog").data("source-id", $sourceEle.attr("id"));
			var roleObj;
			$ddlNewSpecialRole.html("");
			var selectedTypeObj = getSpecialAccountType(selectedAccountName);
			$("<option/>", { "value": "" }).text("--Please select--").appendTo($ddlNewSpecialRole);
			$.each(selectedTypeObj.Roles, function(idx, role) {
				$("<option/>", { "value": role.RoleType }).text(role.DisplayRoleName).appendTo($ddlNewSpecialRole);
			});
			if (isEdit) {
				$("#specialAccountEditorDialog").addClass("edit-mod");
				var info = collectSelectedRoleInfo($sourceEle);
				$("#txtNewSpecialFName").val(info.FirstName);
				$("#txtNewSpecialLName").val(info.LastName);
				roleObj = getRole(selectedTypeObj.Roles, info.RoleType);
				if (info.HasJoint || $(this).is(":checkbox")) {
					$("#txtNewSpecialJointFName").val(info.JointFirstName);
					$("#txtNewSpecialJointLName").val(info.JointLastName);
					$(".has-joint-button", $("#specialAccountEditorDialog")).addClass("show-joint");
				} else {
					$(".has-joint-button", $("#specialAccountEditorDialog")).removeClass("show-joint");
				}
				$("#ddlNewSpecialRole").val(info.RoleType);
				if ($("#ddlNewSpecialRole").data("mobile-selectmenu") === undefined) {
					$("#ddlNewSpecialRole").selectmenu(); //not initialized yet, lets do it
				}
				$("#ddlNewSpecialRole").selectmenu("refresh");
			} else {
				if (validateMaxInstance($sourceEle.closest(".special-role-item"), $sourceEle.data("role-type"), function(p1, p2) { return p1 >= p2; }) == false) {
					setTimeout(function() {
						$.lpqValidate.hideValidation($sourceEle.closest(".special-role-item"));
					}, 1500);
					return false;
				}
				$ddlNewSpecialRole.val($sourceEle.data("role-type"));
				if ($ddlNewSpecialRole.data("mobile-selectmenu") === undefined) {
					$ddlNewSpecialRole.selectmenu(); //not initialized yet, lets do it
				}
				$ddlNewSpecialRole.selectmenu("refresh");
				roleObj = getRole(selectedTypeObj.Roles, $sourceEle.data("role-type"));
				$(".has-joint-button", $("#specialAccountEditorDialog")).removeClass("show-joint");
				$("#specialAccountEditorDialog").removeClass("edit-mod");
			}
			if (roleObj.ShowJointOption == true) {
				$(".has-joint-button", $("#specialAccountEditorDialog")).removeClass("hidden");
			} else {
				$(".has-joint-button", $("#specialAccountEditorDialog")).addClass("hidden");
			}
			$.mobile.changePage("#specialAccountEditorDialog", {
				transition: "fade",
				reverse: false,
				changeHash: false
			});
		};
		SAS.FACTORY.closeSpecialAccountEditorDialog = function() {
			$.mobile.changePage("#specialAccountSelectorPage", {
				transition: "fade",
				reverse: false,
				changeHash: false
			});
		}
		SAS.FACTORY.submitSpecialAccountEditorDialog = function() {
			var $container = $("#specialAccountEditorDialog");
			if ($.lpqValidate("ValidateSpecialAccountEditor") == false) return false;
			var firstName = $("#txtNewSpecialFName").val();
			var lastName = $("#txtNewSpecialLName").val();
			var hasJoint = $(".has-joint-button", $("#specialAccountEditorDialog")).hasClass("show-joint") && $(".has-joint-button", $("#specialAccountEditorDialog")).is(":visible");
			var jointFirstName = $("#txtNewSpecialJointFName").val();
			var jointLastName = $("#txtNewSpecialJointLName").val();
			var selectedAccountName = $("#ddlSpecialAccountType").val();
			var selectedTypeObj = getSpecialAccountType(selectedAccountName);
			var roleObj = getRole(selectedTypeObj.Roles, $("#ddlNewSpecialRole").val());
			var cnt = $(".account-role-item-wrapper[data-role-type='" + roleObj.RoleType + "']", "#divSelectedSpecialAccountRoles").length;
			var jointCnt = $(".account-role-item-wrapper[data-role-type='" + roleObj.RoleType + "'] input[type='checkbox']:checked", "#divSelectedSpecialAccountRoles").length;
			if ($container.hasClass("edit-mod")) {
				var $sourceEle = $("#" + $("#specialAccountEditorDialog").data("source-id"));
				cnt = $(".account-role-item-wrapper[data-role-type='" + roleObj.RoleType + "']:not(" + "#" + $("#specialAccountEditorDialog").data("source-id") + ")", "#divSelectedSpecialAccountRoles").length;
				jointCnt = $(".account-role-item-wrapper[data-role-type='" + roleObj.RoleType + "']:not(" + "#" + $("#specialAccountEditorDialog").data("source-id") + ") input[type='checkbox']:checked", "#divSelectedSpecialAccountRoles").length;
				if (parseInt(roleObj.InstanceMax) < cnt + jointCnt + (hasJoint == true ? 2 : 1)) {
					if (hasJoint == true) {
						$.lpqValidate.showValidation($("#lnkSpecialHasJointApp"), "Maximum allowed instances exceeded.");
					} else {
						$.lpqValidate.showValidation($("#ddlNewSpecialRole"), "Maximum allowed instances exceeded.");
					}
					return false;
				}
				$sourceEle.replaceWith(buildSelectedRoleBlock(roleObj, firstName, lastName, hasJoint, jointFirstName, jointLastName));
			}else {
				cnt = $(".account-role-item-wrapper[data-role-type='" + roleObj.RoleType + "']", "#divSelectedSpecialAccountRoles").length;
				jointCnt = $(".account-role-item-wrapper[data-role-type='" + roleObj.RoleType + "'] input[type='checkbox']:checked", "#divSelectedSpecialAccountRoles").length;
				if (parseInt(roleObj.InstanceMax) < cnt + jointCnt + (hasJoint == true ? 2 : 1)) {
					if (hasJoint == true) {
						$.lpqValidate.showValidation($("#lnkSpecialHasJointApp"), "Maximum allowed instances exceeded.");
					} else {
						$.lpqValidate.showValidation($("#ddlNewSpecialRole"), "Maximum allowed instances exceeded.");
					}
					return false;
				}
				$("#divSelectedSpecialAccountRoles").append(buildSelectedRoleBlock(roleObj, firstName, lastName, hasJoint, jointFirstName, jointLastName));
			}
			$("#divSelectedSpecialAccountRoles").trigger("change");
			$(".account-role-list-wrapper .special-role-item:visible", "#specialAccountSelectorPage").each(function(idx, ele) {
				$.lpqValidate.hideValidation($(ele));
			});
			$.mobile.changePage("#specialAccountSelectorPage", {
				transition: "fade",
				reverse: false,
				changeHash: false
			});
		}
		function cleanUpSpecialAccountEditorDialog() {
			$("#txtNewSpecialFName").val("");
			$("#txtNewSpecialLName").val("");
			$(".has-joint-button", $("#specialAccountEditorDialog")).removeClass("show-joint");
			$("#txtNewSpecialJointFName").val("");
			$("#txtNewSpecialJointLName").val("");
			$("#ddlNewSpecialRole").val("");
			if ($("#ddlNewSpecialRole").data("mobile-selectmenu") === undefined) {
				$("#ddlNewSpecialRole").selectmenu(); //not initialized yet, lets do it
			}
			$("#ddlNewSpecialRole").selectmenu("refresh");
			$("#specialAccountEditorDialog").data("source-id", "");
			$.lpqValidate.hideValidation($("#ddlNewSpecialRole"));
			$.lpqValidate.hideValidation($("#txtNewSpecialFName"));
			$.lpqValidate.hideValidation($("#txtNewSpecialLName"));
			$.lpqValidate.hideValidation($("#txtNewSpecialJointFName"));
			$.lpqValidate.hideValidation($("#txtNewSpecialJointLName"));
			$.lpqValidate.hideValidation($("#lnkSpecialHasJointApp"));
			
		}
		function getSpecialAccountType(code) {
			var result = null;
			$.each(SPECIALACCOUNTTYPELIST, function(idx, item) {
				if (item.AccountCode == code) {
					result = item;
					return true;
				}
			});
			return result;
		}
		function getRole(roleList, roleType) {
			var result = null;
			$.each(roleList, function(idx, item) {
				if (item.RoleType == roleType) {
					result = item;
					return true;
				}
			});
			return result;
		}
		function collectSelectedRoleInfo(container) {
			var result = {};
			var $container = $(container);
			result.FirstName = $("span[data-name='firstName']", $container).text();
			result.LastName = $("span[data-name='lastName']", $container).text();
			result.RoleType = $("input[data-name='roleType']", $container).val();
			result.JointFirstName = "";
			result.JointLastName = "";
			result.HasJoint = false;
			if ($("input:checkbox", $container).is(":checked")) {
				result.HasJoint = true;
				result.JointFirstName = $("span[data-name='jointFirstName']", $container).text();
				result.JointLastName = $("span[data-name='jointLastName']", $container).text();
			}
			return result;
		}
		function buildSelectedRoleBlock(roleObj, firstName, lastName, hasJoint, jointFirstName, jointLastName) {
			var blockId = "div_" + Math.random().toString(36).substr(2, 16);
			var $item = $("<div/>", { "class": "account-role-item-wrapper", "data-role-type": roleObj.RoleType, "id": blockId });
			var $name = $("<div/>", { "class": "ui-btn ui-corner-all account-role-item" });
			$name.html("<span title=' ("+ roleObj.DisplayRoleName +")'>" + "<span data-name='firstName'>" + firstName + "</span>" + " " + "<span data-name='lastName'>" + lastName + "</span>" + "</span><input type='hidden' data-name='roleType' value='"+ roleObj.RoleType +"'/>");
			$item.append($name);
			if (roleObj.ShowJointOption == true) {
				var $addJointChk = $("<div/>", { "class": "ui-btn ui-corner-all role-attr-item" });
				var randomId = "chk_" + Math.random().toString(36).substr(2, 16);
				if (hasJoint == true) {
					$addJointChk.html('<label class="custom-checkbox"><input checked="checked" type="checkbox" id="' + randomId + '" data-role="none"/><span></span></label><label for="' + randomId + '">' + "<span data-name='jointFirstName'>" + jointFirstName + "</span>" + " " + "<span data-name='jointLastName'>" + jointLastName + "</span>" + " (Joint)"  + '</label>');	
					$addJointChk.find("input:checkbox").on("change", function() {
						$addJointChk.html('<label class="custom-checkbox"><input type="checkbox" id="' + randomId + '" data-role="none"/><span></span></label><label for="' + randomId + '">Add joint applicant</label>');
						$addJointChk.find("input:checkbox").off("change").on("change", function(e) {
							$(this).prop("checked", false);
							SAS.FACTORY.openSpecialAccountEditorDialog.call(this, true, $(this).closest(".account-role-item-wrapper"));
						});
					});
				} else {
					$addJointChk.html('<label class="custom-checkbox"><input type="checkbox" id="' + randomId + '" data-role="none"/><span></span></label><label for="' + randomId + '">Add joint applicant</label>');
					$addJointChk.find("input:checkbox").on("change", function(e) {
						$(this).prop("checked", false);
						SAS.FACTORY.openSpecialAccountEditorDialog.call(this, true, $(this).closest(".account-role-item-wrapper"));
					});
				}
				$item.append($addJointChk);
			}
			var $removeBtn = $("<div/>", { "class": "btn-remove", "tabindex": 0 }).appendTo($item);
			$removeBtn.on("click", function() {
				$(this).closest("div.account-role-item-wrapper").remove();
				$("#divSelectedSpecialAccountRoles").trigger("change");
				$("[data-special-account-type] [data-role-type='" + roleObj.RoleType + "']:visible").closest(".special-role-item").trigger("change");

			});
			var $editBtn = $("<div/>", { "class": "btn-edit", "tabindex": 0 }).appendTo($item);
			$editBtn.on("click", function() {
				SAS.FACTORY.openSpecialAccountEditorDialog(true, $(this).closest(".account-role-item-wrapper"));
			});
			return $item;
		}
		SAS.FACTORY.validate = function() {
			if (window.ABR) {
				var formValues = [];
				formValues["specialAccountType"] = [$("#ddlSpecialAccountType").val()];
				var m = /^([12])([abs])?$/.exec($("#frmSpecialAccountSelector input[name='type']").val());
				if (m) {
					formValues["accountPosition"] = m[1];
				}
				formValues["accountType"] = "s";
				if (ABR.FACTORY.evaluateRules("specialAccountSelectorPage", formValues)) {
					return false;
				}
			}
			if ($.lpqValidate("ValidateSpecialAccountSelector") == false) {
				return false;
			}
			var validator = true;
			$(".account-role-list-wrapper .special-role-item:visible", "#specialAccountSelectorPage").each(function(idx, ele) {
				if (validateInstance(ele) == false) {
					validator = false;
				}
			});
			if ($("#frmSpecialAccountSelector input[name='type']").val() == "") return false;
			if (validator == false) {
				return false;
			}

			var roleData = [];
			var roleDataWithoutName = [];
			$(".account-role-item-wrapper[data-role-type]", "#divSelectedSpecialAccountRoles").each(function(idx, item) {
				var roleItem = collectSelectedRoleInfo(item);
				roleData.push({fname: roleItem.FirstName, lname: roleItem.LastName, isjoint: false, roletype: roleItem.RoleType});
				roleDataWithoutName.push({isjoint: false, roletype: roleItem.RoleType});
				if (roleItem.HasJoint == true) {
					roleData.push({fname: roleItem.JointFirstName, lname: roleItem.JointLastName, isjoint: true, roletype: roleItem.RoleType});
					roleDataWithoutName.push({isjoint: true, roletype: roleItem.RoleType});
				}
			});

			var $frmSpecialAccountSelector = $("#frmSpecialAccountSelector");
			$frmSpecialAccountSelector.find("input:hidden[name='acctype']").remove();
			$("<input/>", { "type": "hidden", "name": "acctype" }).val(encodeURIComponent($("#ddlSpecialAccountType").val().toLowerCase())).appendTo($frmSpecialAccountSelector);
			if (roleData.length > 0) {
				var jsonStr = encodeURIComponent(JSON.stringify(roleDataWithoutName));
				<%--fallback, because max url length is about 2000 character, we cannot pass role data with names via query string. Solution is, we store it in current session. But user can save url and then visit later(in same browser) when session is expired, so the below is for that situation, main app will run properly but without names prefilled --%>
				$frmSpecialAccountSelector.find("input:hidden[name='roleinfo']").remove();
				$("<input/>", { "type": "hidden", "name": "roleinfo" }).val(jsonStr).appendTo($frmSpecialAccountSelector);
				$.ajax({
					url: '<%=String.Format("{0}{1}", "/handler/chandler.aspx", Common.SafeEncodeString(Request.Url.Query))%>',
					async: true,
					cache: false,
					type: 'POST',
					dataType: 'html',
					data: {
						command: 'PersistXARoleInfo',
						roleInfo: JSON.stringify(roleData)
					},
					success: function(responseText) {
						$frmSpecialAccountSelector.find("input:hidden[name='roleinfotoken']").remove();
						$("<input/>", { "type": "hidden", "name": "roleinfotoken" }).val(responseText).appendTo($frmSpecialAccountSelector);
						<%If IsInMode("777") then%>
						$frmSpecialAccountSelector.find("input:hidden[name^='xdm_']").remove();
						rpc.submitForm("/xa/xpressapp.aspx?" + $frmSpecialAccountSelector.serialize());
						<%Else%>
						$frmSpecialAccountSelector[0].submit();
						<%End If%>
					},
					error: function() {
						<%If IsInMode("777") then%>
						$frmSpecialAccountSelector.find("input:hidden[name^='xdm_']").remove();
						rpc.submitForm("/xa/xpressapp.aspx?" + $frmSpecialAccountSelector.serialize());
						<%Else%>
						$frmSpecialAccountSelector[0].submit();
						<%End If%>
					}
				});
			} else if($(".account-role-list-wrapper .special-role-item:visible", "#specialAccountSelectorPage").length > 0){
				<%--https://denovu.atlassian.net/browse/APP-33 issue #9 - workflow changed, force user select at least one applicant role in order to move forward--%>
				$.lpqValidate.showValidation($(".account-role-list-wrapper>.sub_section_header", "#specialAccountSelectorPage"), "Please select at least one applicant.");
				setTimeout(function() {
					$.lpqValidate.showValidation($(".account-role-list-wrapper>.sub_section_header", "#specialAccountSelectorPage"));
				}, 2000);
			}
		}
		function validateMaxInstance(container, roleType, comparer) {
			var $self = $(container);
			var $ctrl = $self.find(".btn-add");
			var maxInstance = parseInt($ctrl.data("instance-max"));
			var cnt = $(".account-role-item-wrapper[data-role-type='" + roleType + "']", "#divSelectedSpecialAccountRoles").length;
			var jointCnt = $(".account-role-item-wrapper[data-role-type='" + roleType + "'] input[type='checkbox']:checked", "#divSelectedSpecialAccountRoles").length;
			if (comparer(cnt + jointCnt, maxInstance)) {
				$.lpqValidate.showValidation($self, "Maximum allowed instances exceeded");
				return false;
			} else {
				$.lpqValidate.hideValidation($self);
			}
			return true;
		}
		function validateInstance(container) {
			var $self = $(container);
			var $ctrl = $self.find(".btn-add");
			var minInstance = parseInt($ctrl.data("instance-min"));
			var roleType = $ctrl.data("role-type");
			var cnt = $(".account-role-item-wrapper[data-role-type='" + roleType + "']", "#divSelectedSpecialAccountRoles").length;
			if (minInstance > 0 && minInstance > cnt) {
				$.lpqValidate.showValidation($self, "Please select at least " + minInstance + " instance" + (minInstance == 1 ? "" : "s"));
				return false;
			} else {
				$.lpqValidate.hideValidation($self);
			}
			return validateMaxInstance(container, roleType, function(p1, p2) { return p1 > p2; });
		}
		SAS.FACTORY.init = function() {
			$("#ddlSpecialAccountType").on("change", function() {
				var $self = $(this);
				$("div[data-special-account-type]").addClass("hidden");
				$("div.selected-roles-wrapper", "#specialAccountSelectorPage").addClass("hidden");
				$("#divSelectedSpecialAccountRoles").html("");
				$(".account-role-list-wrapper .special-role-item", "#specialAccountSelectorPage").each(function(idx, ele) {
					$.lpqValidate.hideValidation(ele);
				});
				var $selectRolesContainer = $("div[data-special-account-type='" + $self.val() + "']", "#specialAccountSelectorPage");
				if ($selectRolesContainer.length > 0) {
					$(".account-role-list-wrapper", "#specialAccountSelectorPage").removeClass("hidden");
					$selectRolesContainer.removeClass("hidden");
				} else {
					$(".account-role-list-wrapper", "#specialAccountSelectorPage").addClass("hidden");
				}
			}).trigger("change");
			$("#divSelectedSpecialAccountRoles").on("change", function() {
				if ($(this).find(".account-role-item-wrapper").length > 0) {
					$(this).closest(".selected-roles-wrapper").removeClass("hidden");
				} else {
					$(this).closest(".selected-roles-wrapper").addClass("hidden");
				}
			});
			$(".account-role-list-wrapper .special-role-item", "#specialAccountSelectorPage").on("change", function() {
				validateInstance(this);
			});
			$("#popSpecialAccountSelectedRole").on("popupafteropen", function (event, ui) {
				$("#" + this.id + "-screen").height("");
			});
			registerSpecialAccountValidator();
			
			$("#specialAccountSelectorPage").on("pagehide", function (event, ui) {
				$(".account-role-list-wrapper .special-role-item:visible", "#specialAccountSelectorPage").each(function(idx, ele) {
					$.lpqValidate.hideValidation($(ele));
				});
				$.lpqValidate.hideValidation("#ddlSpecialAccountType");
			});

			$('#lnkSpecialHasJointApp').on('click', function (e) {
				var $self = $(this);
				if ($self.closest("div.has-joint-button").hasClass("show-joint")) {
					$self.addClass("chevron-circle-right-before").removeClass("chevron-circle-down-before");
					$.lpqValidate.hideValidation($('#txtNewSpecialJointFName'));
					$.lpqValidate.hideValidation($('#txtNewSpecialJointLName'));
					$.lpqValidate.hideValidation($("#lnkSpecialHasJointApp"));
				} else {
					$self.addClass("chevron-circle-down-before").removeClass("chevron-circle-right-before");
				}
				$self.closest(".has-joint-button").toggleClass("show-joint");
				if (BUTTONLABELLIST != null) {
					var value = BUTTONLABELLIST[$.trim($self.html()).toLowerCase()];
					if (typeof value == "string" && $.trim(value) !== "") {
						$self.html(value);
					}
				}
				e.preventDefault();
			});
			$("#ddlNewSpecialRole").on("change", function() {
				var $self = $(this);
				var selectedAccountName = $("#ddlSpecialAccountType").val();
				var selectedTypeObj = getSpecialAccountType(selectedAccountName);
				if ($self.val() == "") {
					$(".has-joint-button", $("#specialAccountEditorDialog")).addClass("hidden");
				} else {
					var roleObj = getRole(selectedTypeObj.Roles, $self.val());
					if (roleObj.ShowJointOption == true) {
						$(".has-joint-button", $("#specialAccountEditorDialog")).removeClass("hidden");
					
					} else {
						$(".has-joint-button", $("#specialAccountEditorDialog")).addClass("hidden");
					}
					$(".has-joint-button", $("#specialAccountEditorDialog")).removeClass("show-joint");
				}
				
			});
		}

		function registerSpecialAccountValidator() {
			$("#ddlSpecialAccountType").observer({
				validators: [
					function(partial) {
						if ($(this).val() == "") return "Please select your account type";
						return "";
					}
				],
				validateOnBlur: true,
				group: "ValidateSpecialAccountSelector"
			});
			$("#txtNewSpecialFName").observer({
				validators: [
					function(partial) {
						var text = $(this).val();
					    if ($.trim(text) =="") {
						    return 'First Name is required';
					    } else if (/^[\sa-zA-Z\'-]+$/.test(text) == false) {
						    return 'Enter a valid first name. Allowed characters: letters "A-Z", dashes, apostrophes and spaces.';
					    }
					    return "";	
					}
				],
				validateOnBlur: true,
				group: "ValidateSpecialAccountEditor"
			});
			$("#txtNewSpecialLName").observer({
				validators: [
					function(partial) {
						var text = $(this).val();
					    if ($.trim(text) =="") {
						    return 'Last Name is required';
					    } else if (/^[\sa-zA-Z\'-]+$/.test(text) == false) {
						    return 'Enter a valid last name. Allowed characters: letters "A-Z", dashes, apostrophes and spaces.';
					    }
					    return "";	
					}
				],
				validateOnBlur: true,
				group: "ValidateSpecialAccountEditor"
			});
			$("#ddlNewSpecialRole").observer({
				validators: [
					function(partial) {
						if ($(this).val() == "") return "Please select role";
						return "";
					}
				],
				validateOnBlur: true,
				group: "ValidateSpecialAccountEditor"
			});
			$("#txtNewSpecialJointFName").observer({
				validators: [
					function(partial) {
						var $self = $(this);
                        if ($self.is(":visible")) {
                            if ($.trim($self.val()) == "") {
						        return 'First Name is required';
					        } else if (/^[\sa-zA-Z\'-]+$/.test($self.val()) == false) {
						        return 'Enter a valid first name. Allowed characters: letters "A-Z", dashes, apostrophes and spaces.';
					        }
                        }						
						return "";
					}
				],
				validateOnBlur: true,
				group: "ValidateSpecialAccountEditor"
			});
			$("#txtNewSpecialJointLName").observer({
				validators: [
					function(partial) {
						var $self = $(this);
                        if ($self.is(":visible")) {
                            if ($.trim($self.val()) == "") {
						        return 'Last Name is required';
					        } else if (/^[\sa-zA-Z\'-]+$/.test($self.val()) == false) {
						        return 'Enter a valid last name. Allowed characters: letters "A-Z", dashes, apostrophes and spaces.';
					        }
                        }	
						return "";
					}
				],
				validateOnBlur: true,
				group: "ValidateSpecialAccountEditor"
			});
		}
	}(window.SAS = window.SAS || {}, jQuery));
	$(function() {
		SAS.FACTORY.init();
	});
</script>