﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="AccountSelector.ascx.vb" Inherits="xa_Inc_AccountSelector" %>
<div id="accountSelectorPage" data-role="dialog" data-history="false" data-close-btn="none" style="width: 100%;">
	<div data-role="header">
		<button class="header-hidden-btn">.</button>
		<div class="page-header-title">Accounts</div>
		<div tabindex="0" onclick="AS.FACTORY.closeAccountDialog(this, 'mainPage')" class="header_theme2 ui-btn-right" data-role="none">
			<%=HeaderUtils.IconClose %><span style="display: none;">close</span>
		</div>
	</div>
	<div data-role="content">
		<div class="header_theme rename-able" style="font-size: 1.2em; margin: 20px 0; text-align: center;">What type of account are you opening?</div>
		
        <%If HasPersonalAccounts Then %>
        <div id="btnXaPersonalNew"><a rel="external" href="<%=NewPersonalAccountUrl%>" data-role="button" type="button" class="button-style div-continue-button">Personal</a></div>
		<div id="btnXaPersonalExisting"><a rel="external" href="<%=ExistingPersonalAccountUrl%>" data-role="button" type="button" class="button-style div-continue-button">Personal</a></div>
		<%End If %>

        <%If HasBusinessAccounts Then%>
		<div id="btnXaBusiness"><a href="#" data-role="button" type="button" class="button-style div-continue-button">Business</a></div>
		<%End If%>

        <%If HasSpecialAccounts Then %>
		<div id="btnXaSpecial"><a href="#" data-role="button" type="button" class="button-style div-continue-button">Other</a></div>
        <%End If %>
        
		<div class ="div-continue">
			<a href="#" class ="div-goback ui-btn" onclick="AS.FACTORY.closeAccountDialog(this, 'mainPage')" ><span class="hover-goback"> Go Back</span></a>   
		</div>    
	</div>
</div>
<script type="text/javascript">
	(function (AS, $, undefined) {
		AS.FACTORY = {};
		AS.FACTORY.openAccountSelectorDialog = function (type) {
			if (type == "xa") {
				$("#btnXaPersonalNew", "#accountSelectorPage").removeClass("hidden");
				$("#btnXaPersonalExisting", "#accountSelectorPage").addClass("hidden");
				$("#btnXaSpecial a").off("click").on("click", function (e) {
					SAS.FACTORY.openSelectorDialog("1s");
					e.preventDefault();
				});
				$("#btnXaBusiness a").off("click").on("click", function (e) {
					BAS.FACTORY.openSelectorDialog("1b");
					e.preventDefault();
				});
			} else {
				$("#btnXaPersonalNew", "#accountSelectorPage").addClass("hidden");
				$("#btnXaPersonalExisting", "#accountSelectorPage").removeClass("hidden");
				$("#btnXaSpecial a").off("click").on("click", function (e) {
					SAS.FACTORY.openSelectorDialog("2s");
					e.preventDefault();
				});
				$("#btnXaBusiness a").off("click").on("click", function (e) {
					BAS.FACTORY.openSelectorDialog("2b");
					e.preventDefault();
				});
			}
			
			$.mobile.changePage("#accountSelectorPage", {
				transition: "fade",
				reverse: false,
				changeHash: false
			});
		};
		AS.FACTORY.closeAccountDialog = function (sourceEle, destPage) {
			$.mobile.changePage("#" + destPage, {
				transition: "fade",
				reverse: false,
				changeHash: false
			});
		}
	}(window.AS = window.AS || {}, jQuery));
	$(function() {
	});
</script>