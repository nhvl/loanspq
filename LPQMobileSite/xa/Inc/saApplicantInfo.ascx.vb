﻿
Imports LPQMobile.Utils
Imports System.Web

Partial Class xa_Inc_saApplicantInfo
	Inherits CBaseUserControl

	Public Property PreviousPage As String
	Public Property Availability As String
	Public Property LogoUrl As String
	Public Property NextPage As String
	Public Property RoleType As String
	Public Property IsJoint As Boolean
	Public Property AccountName As String
	Public Property AccountCode As String
	Public Property IsPrimaryApplicant As Boolean = False
	Public Property BlockApplicantAgeUnder As Integer
	Public Property BlockApplicantAgeOver As Integer
	Public Property LaserScandocAvailable As Boolean = False
	Public Property LegacyScandocAvailable As Boolean = False
	Public Property ScanDocumentKey As String
	Public CurrentConfig As LPQMobile.Utils.CWebsiteConfig

	Public Property StateDropdown As String = ""
	Public Property EnableMemberNumber As Boolean = False   ' by default
	Public Property MemberNumberRequired As Boolean = False ' by default
	Public Property InstitutionType As CEnum.InstitutionType
	Public Property FieldMaxLength As Integer = 50
	Public Property EnableGender As Boolean
	Public Property EnableMotherMaidenName As Boolean
	Public Property EnableCitizenshipStatus As Boolean
	Public Property CitizenshipStatusDropdown As String
	Public Property EnableMaritalStatus As Boolean
	Public Property MaritalStatusDropdown As String
    Public Property EmployeeOfLenderDropdown As String
    Public Property LenderName As String
    Public Property RelationToPrimaryApplicantDropdown As String
	Public Property EnabledAddressVerification As Boolean
	Public Property EnableOccupancyStatus As Boolean = True
	Public Property OccupyingLocationDropdown As String
	Public Property CollectDescriptionIfOccupancyStatusIsOther As Boolean
	Public Property LiveYearsDropdown As String
	Public Property LiveMonthsDropdown As String
	Public Property PreferContactMethodDropdown As String
	Public Property EnableGrossMonthlyIncome As Boolean
	Public Property CollectJobTitleOnly As Boolean
	Public Property CollectDescriptionIfEmploymentStatusIsOther As String
	Public Property EmploymentStatusDropdown As String
	Public Property IDCardTypeList As String
	Public Property IsExistIDCardType As Boolean
	Public Property EnableBeneficiarySSN As Boolean
	Public Property EnableBeneficiaryAddress As Boolean
	Public Property EnableBeneficiaryTrust As Boolean
	Public Property EnableAddress As Boolean
	Public Property EnableMailingAddress As Boolean = True
	Public Property EnableContactInfo As Boolean
	Public Property EnableContactRelative As Boolean
	Public Property EnableRelationshipToPrimary As Boolean
	Public Property EnableEmployment As Boolean
	Public Property EnableIDCard As Boolean
	Public Property RequirePrimaryIDCard As Boolean
	Public Property ShowBeneficiary As Boolean

	Public Property FirstName As String = ""
	Public Property LastName As String = ""

	Public Property InbrachEnableAddress As Boolean = False
	Public Property InbrachEnableContactInfo As Boolean = False
	Public Property InbrachEnableContactRelative As Boolean = False
	Public Property InbranchEnableEmployment As Boolean = False
	Public Property InbranchEnableIDCard As Boolean = False
	Public Property InbranchEnableMaritalStatus As Boolean = False
	Public Property InbranchEnableOccupancyStatus As Boolean = False
    Public Property InbranchEnableRelationshipToPrimary As Boolean = False

    Public Property EnableDocUpload As Boolean = False
    Public Property DocTitleOptions As List(Of CDocumentTitleInfo) = Nothing
    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
		If Not IsPostBack Then
			If LaserScandocAvailable Then
				Dim laserDLScan As Inc_LaserDocumentScan = CType(LoadControl("~/Inc/LaserDocumentScan.ascx"), Inc_LaserDocumentScan)
				laserDLScan.IDPrefix = IDPrefix
				plhLaserDriverLicenseScan.Controls.Add(laserDLScan)
				plhLaserDriverLicenseScan.Visible = True
			Else
				plhLaserDriverLicenseScan.Visible = False
			End If
			Dim legacyDLScan As Inc_NewDocumentScan = CType(LoadControl("~/Inc/NewDocumentScan.ascx"), Inc_NewDocumentScan)
			legacyDLScan.CurrentConfig = CurrentConfig
			legacyDLScan.IDPrefix = IDPrefix
			plhDriverLicenseScan.Controls.Add(legacyDLScan)

			plhEmployment.Visible = False
			If EnableEmployment Then
				plhEmployment.Visible = True
				Dim emploment As Inc_MainApp_xaApplicantEmployment = CType(LoadControl("~/Inc/MainApp/xaApplicantEmployment.ascx"), Inc_MainApp_xaApplicantEmployment)
				emploment.Header = HeaderUtils.RenderPageTitle(0, "Employment", True)
				emploment.EmploymentStatusDropdown = EmploymentStatusDropdown
				emploment.IDPrefix = IDPrefix
				emploment.MappedShowFieldLoanType = MappedShowFieldLoanType
				emploment.CollectDescriptionIfEmploymentStatusIsOther = CollectDescriptionIfEmploymentStatusIsOther
				emploment.Config = CurrentConfig.GetWebConfigElement
				emploment.EnableGrossMonthlyIncome = EnableGrossMonthlyIncome
				emploment.CollectJobTitleOnly = CollectJobTitleOnly
				emploment.ShowFieldDefaultStateOn = InbranchEnableEmployment
				plhEmployment.Controls.Add(emploment)
			End If

            plhIDCard.Visible = False
            If EnableIDCard Then
                plhIDCard.Visible = True
                Dim primaryIDCard As Inc_MainApp_xaApplicantID = CType(LoadControl("~/Inc/MainApp/xaApplicantID.ascx"), Inc_MainApp_xaApplicantID)
                primaryIDCard.IDPrefix = IDPrefix
                primaryIDCard.StateDropdown = StateDropdown
                primaryIDCard.IDCardTypeList = IDCardTypeList

                primaryIDCard.MappedShowFieldLoanType = MappedShowFieldLoanType
                primaryIDCard.isExistIDCardType = IsExistIDCardType
                primaryIDCard.Header = HeaderUtils.RenderPageTitle(0, "Primary ID Card", True)
                primaryIDCard.Required = Me.InbranchEnableIDCard AndAlso Me.RequirePrimaryIDCard
                plhIDCard.Controls.Add(primaryIDCard)

                'Dim secondaryIDCard As Inc_MainApp_xaApplicantID = CType(LoadControl("~/Inc/MainApp/xaApplicantID.ascx"), Inc_MainApp_xaApplicantID)
                'secondaryIDCard.IDPrefix = IDPrefix & "2nd"
                'secondaryIDCard.StateDropdown = StateDropdown
                'secondaryIDCard.IDCardTypeList = IDCardTypeList
                'secondaryIDCard.MappedShowFieldLoanType = MappedShowFieldLoanType
                'secondaryIDCard.isExistIDCardType = IsExistIDCardType
                'secondaryIDCard.Header = HeaderUtils.RenderPageTitle(0, "Secondary ID Card", True)
                'plhIDCard.Controls.Add(secondaryIDCard)
            End If

            GenerateUploadDoc()

            Dim additionalInfo As Inc_MainApp_xaApplicantQuestion = CType(LoadControl("~/Inc/MainApp/xaApplicantQuestion.ascx"), Inc_MainApp_xaApplicantQuestion)
			additionalInfo.IDPrefix = IDPrefix
			additionalInfo.Header = HeaderUtils.RenderPageTitle(0, "Additional Information", True)
            additionalInfo.LoanType = "XA"
			additionalInfo.SpecialAccountName = AccountName
			additionalInfo.CQLocation = CustomQuestionLocation.ApplicantPage
			plhAdditionalInfo.Controls.Add(additionalInfo)

			plhBeneficiary.Visible = False
			If IsPrimaryApplicant AndAlso ShowBeneficiary Then
				plhBeneficiary.Visible = True
				Dim beneficiaryCtrl As Inc_MainApp_xaBeneficiary = CType(LoadControl("~/Inc/MainApp/xaBeneficiary.ascx"), Inc_MainApp_xaBeneficiary)
				beneficiaryCtrl.IDPrefix = IDPrefix
				beneficiaryCtrl.EnableBeneficiarySSN = EnableBeneficiarySSN
				beneficiaryCtrl.EnableBeneficiaryAddress = EnableBeneficiaryAddress
				beneficiaryCtrl.EnableBeneficiaryTrust = EnableBeneficiaryTrust
				beneficiaryCtrl.MappedShowFieldLoanType = MappedShowFieldLoanType

				plhBeneficiary.Controls.Add(beneficiaryCtrl)
			End If

			plhAddress.Visible = False
            If EnableAddress Then
                plhAddress.Visible = True
                Dim addressCtrl As xa_Inc_saAddress = CType(LoadControl("~/xa/Inc/saAddress.ascx"), xa_Inc_saAddress)
                addressCtrl.IDPrefix = IDPrefix
                addressCtrl.StateDropdown = StateDropdown
				addressCtrl.EnableOccupancyStatus = EnableOccupancyStatus
				addressCtrl.ShowFieldOccupancyStatusDefaultStateOn = InbranchEnableOccupancyStatus
                addressCtrl.EnableHousingPayment = True
                addressCtrl.EnabledAddressVerification = EnabledAddressVerification
                addressCtrl.OccupyingLocationDropdown = OccupyingLocationDropdown
                addressCtrl.CollectDescriptionIfOccupancyStatusIsOther = CollectDescriptionIfOccupancyStatusIsOther
                addressCtrl.LiveYearsDropdown = LiveYearsDropdown
				addressCtrl.LiveMonthsDropdown = LiveMonthsDropdown
				addressCtrl.EnableMailingAddress = EnableMailingAddress
                addressCtrl.Header = HeaderUtils.RenderPageTitle(0, "Current Physical Address", True)
				addressCtrl.PreviousAddressHeader = HeaderUtils.RenderPageTitle(0, "Previous Physical Address", True)
				addressCtrl.MappedShowFieldLoanType = MappedShowFieldLoanType
                plhAddress.Controls.Add(addressCtrl)
            End If
            '' handle this in xpressApp.aspx.vb 
            ''EnableRelationshipToPrimary = False
            ''Dim relationshipToPrimaryList As Dictionary(Of String, String) = CurrentConfig.GetEnumItems("RELATIONSHIP_PRIMARY")
            ''If relationshipToPrimaryList IsNot Nothing AndAlso relationshipToPrimaryList.Any() Then
            ''    EnableRelationshipToPrimary = True
            ''End If
            If RelationToPrimaryApplicantDropdown = "" Then
                ''reset EnableRelationshipToPrimary to false
                EnableRelationshipToPrimary = False
            End If
		End If
    End Sub
    Private Sub GenerateUploadDoc()
        Dim _requiredUpLoadDocsXA As String = Common.SafeString(Common.getNodeAttributes(CurrentConfig, "XA_LOAN/BEHAVIOR", "doc_upload_required"))
        Dim uploadDocsMessage As String = Common.getXASpecialUploadDocumentMessage(CurrentConfig)
        EnableDocUpload = Common.getXASpecialUploadDocumentEnable(CurrentConfig) AndAlso Not String.IsNullOrWhiteSpace(uploadDocsMessage)
        If EnableDocUpload Then
            Dim docTitleRequired = Not Common.getNodeAttributes(CurrentConfig, "XA_LOAN/BEHAVIOR", "required_doc_title").Equals("N", StringComparison.OrdinalIgnoreCase)

            If DocTitleOptions Is Nothing OrElse DocTitleOptions.Count = 0 Then
                ' Fallback to ENUMS doc titles
                DocTitleOptions = Common.GetDocumentTitleInfoList(CurrentConfig._webConfigXML.SelectSingleNode("ENUMS/XA_DOC_TITLE"))
            End If
            xa_Inc_baApplicantInfo.GenerateUploadDoc(EnableDocUpload, IDPrefix,
                                                     HttpUtility.HtmlDecode(uploadDocsMessage),
                                                     docTitleRequired, DocTitleOptions,
                                                     _requiredUpLoadDocsXA.Equals("Y", StringComparison.OrdinalIgnoreCase),
                                                     CType(LoadControl("~/Inc/NewDocCapture.ascx"), Inc_NewDocCapture),
                                                     CType(LoadControl("~/Inc/DocCaptureSourceSelector.ascx"), Inc_DocCaptureSourceSelector),
                                                     plhUploadDoc, plhUploadDocSrcSelector)
        End If
    End Sub
    Protected Function callVerifyAddressJSFunction(isZipcodeTextbox As Boolean, Optional ByVal prefixAddress As String = "") As String
		'' verifyAddress js function needs to attach to address + zip + city + state html controls
		If EnabledAddressVerification Then
			Return String.Format("{0}verifyAddress('{1}');", IDPrefix, prefixAddress)
		Else
			'' lookupZipcode js function only needs to attach to zip html control
			If isZipcodeTextbox Then
				Return String.Format("{0}lookupZipcode(this.value,'{1}');", IDPrefix, prefixAddress)
			End If
		End If
		Return String.Empty
	End Function
End Class
