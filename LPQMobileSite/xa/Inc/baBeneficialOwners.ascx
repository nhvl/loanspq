﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="baBeneficialOwners.ascx.vb" Inherits="xa_Inc_baBeneficialOwners" %>
<%@ Import Namespace="LPQMobile.Utils" %>
<div class="ba-beneficial-owners-page" data-role="page" id="page<%=IDPrefix%>">
    <div data-role="header" style="display: none">
        <h1>Beneficial Owners</h1>
    </div>
    <div data-role="content">
	    <%If Not String.IsNullOrWhiteSpace(Availability) AndAlso Availability.StartsWith("2") Then%>
            <%=HeaderUtils.RenderLogoAndSteps(LogoUrl, 1, "OPEN")%>
        <%Else%>
			<%=HeaderUtils.RenderLogoAndSteps(LogoUrl, 1, "JOIN")%>	
        <%End If%>   
        <%=HeaderUtils.RenderPageTitle(21, "Beneficial Owners", False)%>
		<div class="guide-btn-wrapper" style="padding-right: 25px;">
		<%=HeaderUtils.RenderPageTitle(0, "Beneficial Owners", True)%>
		<a class="guide-btn header_theme2 abort-renameable" style="top: 33px;" href="#<%=IDPrefix%>popBusinessAccountBeneficialOwnedInfo" data-position-to="window" data-rel="popup"><em class="fa fa-question-circle"></em></a>
		</div>
        <div id="<%=IDPrefix%>divShowBeneOwnerMessage"><span id="<%=IDPrefix%>spShowBeneOwnerMessage"></span></div>
		<div id="<%=IDPrefix%>divBeneficialOwnersContainer">
			<%--<div class='btn-header-theme ui-btn ui-corner-all beneficial-owner-item'>
				<p>Fred Smith</p>
				<p>25% Ownership</p>
				<p>Responsibility to control/manage</p>
			</div>
			<div class='btn-header-theme ui-btn ui-corner-all beneficial-owner-item'>
				<p>Fred Smith</p>
				<p>25% Ownership</p>
				<p>Responsibility to control/manage</p>
			</div>--%>
		</div>
		<div id="<%=IDPrefix%>divBeneficiaryLink" style="margin-top: 10px;" class="hide-able" >
		<a id="<%=IDPrefix%>showBeneficiary" href="#" data-mode="self-handle-event" status="N" style="cursor: pointer;font-weight :bold;" class="header_theme2 shadow-btn toogle-beneficiary-btn">Add beneficial owner</a>
		</div>
		<div id="<%=IDPrefix%>divBeneficiaryPanel" style="display:none;" >
			<div id="<%=IDPrefix%>beneficiaryContainer"></div>
			<div id="<%=IDPrefix%>divBtn">
				<div class ="addDivBtn" ><a href ="#"  data-role="button" class="ui-btn btn-header-theme ui-shadow ui-corner-all" >Add More</a></div>
				<div class="removeDivBtn"><a href="#"  data-role="button" class="ui-btn btn-header-theme ui-shadow ui-corner-all">Remove</a></div>
			</div>
			<div style ="clear:both"></div>
		</div>
		<div id="<%=IDPrefix%>popSSNSastisfy" data-role="popup" style="max-width: 400px;">
			<div data-role="content">
				<div class="row">
					<div class="col-xs-12 header_theme2">
						<a data-rel="back" href="#" class="pull-right svg-btn"><%=HeaderUtils.IconClose %><span style="display: none;">close</span></a>
					</div>
				</div>
				<div class="row">
					<div class="col-sm-12">
						<div style="margin: 10px 0;">
							Your Social Security Number (SSN) is used for identification purposes and to determine your account opening eligibility.
						</div>
					</div>    
				</div>
			</div>
		</div>
		<div id="<%=IDPrefix%>popBusinessAccountBeneficialOwnedInfo" data-role="popup" data-dismissible="true" data-history="false" style="max-width: 400px;">
			<div data-role="content">
				<div class="row">
					<div class="col-xs-12 header_theme2">
						<a class="pull-right svg-btn" data-rel="back" href="#" ><%=HeaderUtils.IconClose %><span style="display: none;">close</span></a>
					</div>
				</div>
				<div class="row">
					<div class="col-sm-12">
						<div style="margin: 10px 0; font-weight: normal;">
							<p>Due to Financial Crimes Enforcement Network (FinCEN) regulations, businesses are required to identity beneficial owners.</p>
							<p>A beneficial owner meets at least one of the following criteria:</p>
							<ul>
								<li><span class="bold">Ownership</span> - has 25% or more ownership in the business.</li>
								<li><span class="bold">Control</span> - has significant responsibility to control, manage, or direct the business.</li>
							</ul>
						</div>     
					</div>    
				</div>
			</div>
			<div class="div-continue" style="text-align: center;"><a href="#" data-transition="slide" data-rel="back" type="button" data-role="button" class="div-continue-button">OK</a></div>
		</div>
    </div>
    <div class ="div-continue"  data-role="footer">
        <a href="#" data-transition="slide" type="button" class="div-continue-button" onclick="<%=IDPrefix%>ValidateBABenficialOwnersInfo()">Continue</a> 
        <a href="#divErrorDialog" style="display: none;">no text</a>
        <span> Or</span>  <a href="#" onclick="goToNextPage('#<%=PreviousPage %>');" class ="div-goback" data-corners="false" data-shadow="false" data-theme="reset"><span class="hover-goback"> Go Back</span></a>
    </div>
</div>
<div class="hidden">
	<div id="<%=IDPrefix%>divIconQuestion16"><%=HeaderUtils.IconQuestion16%></div>
	<div id="<%=IDPrefix%>divIconLock16"><%=HeaderUtils.IconLock16%></div>
</div>
<script>
	function <%=IDPrefix%>Obj(id, type, label, isRequired, className, maxLength, index) {
		this.id = id;
		this.type = type;
		this.label = label;
		this.isRequired = isRequired;
		this.className = className;
		this.maxLength = maxLength;
		this.index = index;
	}
	<%--function <%=IDPrefix%>beneficiaryObj (businessOwned, hasControl, role, fName, mName, lName, suffix, citizenship, ssNum, birthDate, address, zip, city, state) {
		this.businessOwned = businessOwned;
		this.firstName = fName;
		this.middleName = mName;
		this.lastName = lName;
		this.suffix = suffix;
		this.citizenship = citizenship;
		this.dob = birthDate;
		this.ssn = ssNum;
		this.address = address;
		this.zip = zip;
		this.city = city;
		this.state = state;
		this.hasControl = hasControl;
		this.role = role;
	}--%>

	function <%=IDPrefix%>beneficiaryInfo(index) {
		var beneficiary = [];
		beneficiary.push(new <%=IDPrefix%>Obj("business_owned_percent" + index, "text", "% Business Owned", true, "numeric", "50", index));
		beneficiary.push(new <%=IDPrefix%>Obj("role" + index, "dropdown", "Title", true, "", null, index));
		beneficiary.push(new <%=IDPrefix%>Obj("first_name" + index, "text", "First Name", true, "", "50", index));
		beneficiary.push(new <%=IDPrefix%>Obj("middle_name" + index, "text", "Middle Name", false, "", "50", index));
		beneficiary.push(new <%=IDPrefix%>Obj("last_name" + index, "text", "Last Name", true, "", "50", index));
		beneficiary.push(new <%=IDPrefix%>Obj("suffix" + index, "dropdown", "Suffix", false, "", null, index));
		beneficiary.push(new <%=IDPrefix%>Obj("citizenship" + index, "dropdown", "Citizenship", false, "", null, index));
		<%If EnableBeneficiarySSN%>
		beneficiary.push(new <%=IDPrefix%>Obj("ssn" + index, "text", "SSN/TIN", true, "", "9", index));
		<%End If%>
		beneficiary.push(new <%=IDPrefix%>Obj("dob" + index, "text", "Date of Birth", false, "", "10", index));
		beneficiary.push(new <%=IDPrefix%>Obj("address" + index, "text", "Address", true, "", "200", index));
		beneficiary.push(new <%=IDPrefix%>Obj("zip" + index, "text", "Zip", true, "", "50", index));
		beneficiary.push(new <%=IDPrefix%>Obj("city" + index, "text", "City", true, "", "50", index));
		beneficiary.push(new <%=IDPrefix%>Obj("state" + index, "dropdown", "State", true, "", null, index));
		return beneficiary;
	}

	function <%=IDPrefix%>renderingBeneficiaries(index) {
		var beneficiaryArr = <%=IDPrefix%>beneficiaryInfo(index);
		var beneficiaryHtml = "<div class='Beneficiary' id='<%=IDPrefix%>beneficiary" + index + "'>";
		beneficiaryHtml += "<div class='header_theme2 beneficiaryLabel'>Beneficiary Owner #" + (index + $("#<%=IDPrefix%>divBeneficialOwnersContainer>.beneficial-owner-item").length) + "</div>";
		for (var i = 0; i < beneficiaryArr.length; i++) {
			beneficiaryHtml += <%=IDPrefix%>htmlBeneficiaryField(beneficiaryArr[i], index);
        }
		beneficiaryHtml += "</div>";
		return beneficiaryHtml;
	}
	function <%=IDPrefix%>htmlBeneficiaryField(obj, index) {

		var strHtml = "";
		
		if (obj.id == "dob" + index) {
			strHtml += "<div data-role='fieldcontain' id='<%=IDPrefix%>div" + obj.id + "Stuff'>";
			strHtml += '<div class="row">';
			strHtml += '<div class="col-xs-8 no-padding"><label for="<%=IDPrefix%>txt'+ obj.id +'" id="<%=IDPrefix%>lbl'+ obj.id +'" class="dob-label ' + (obj.isRequired ? " RequiredIcon" : "") + '">'+ obj.label +'</label></div>';
			strHtml += '<div class="col-xs-4 no-padding"><input type="hidden" id="<%=IDPrefix%>txt'+ obj.id +'" class="combine-field-value"/></div>';
			strHtml += '</div>';
			strHtml += "<div id='<%=IDPrefix%>div" + obj.id + "' class='ui-input-date row'>";
	        var txtDOB1 = "txt" + obj.id + "_1";
	        var txtDOB2 = "txt" + obj.id + "_2";
	        var txtDOB3 = "txt" + obj.id + "_3";

	        strHtml += "<div class='col-xs-4'><input aria-labelledby='<%=IDPrefix%>lbl" + obj.id + "' type='" + '<%=TextAndroidTel%>' + "' pattern='[0-9]*' placeholder='mm' id='<%=IDPrefix%>" + txtDOB1 + "' maxlength='2' onkeydown='limitToNumeric(event);' onkeyup='<%=IDPrefix%>onKeyUp_DOB(event,this)'/></div>";
			strHtml += "<div class='col-xs-4'><input aria-labelledby='<%=IDPrefix%>lbl" + obj.id + "' type='" + '<%=TextAndroidTel%>' + "' pattern='[0-9]*' placeholder='dd' id='<%=IDPrefix%>" + txtDOB2 + "' maxlength='2' onkeydown='limitToNumeric(event);' onkeyup='<%=IDPrefix%>onKeyUp_DOB(event,this)'/></div>";
        	strHtml += "<div class='col-xs-4'><input aria-labelledby='<%=IDPrefix%>lbl" + obj.id + "' type='" + '<%=TextAndroidTel%>' + "' pattern='[0-9]*' placeholder='yyyy' id='<%=IDPrefix%>" + txtDOB3 + "' maxlength='4' onkeydown='limitToNumeric(event);' /></div></div></div>";
		} else if (obj.id == "ssn" + index) {
			var txtSSN1 = "txt" + obj.id + "_1";
			var txtSSN2 = "txt" + obj.id + "_2";
			var txtSSN3 = "txt" + obj.id + "_3";
			var showLabel = 'Show ' + obj.label;
			var hideLabel = 'Hide ' + obj.label;
			<%--for backward compability, still keep using divBeneficiarySSN1 instead of divBeneficiarySSN --%>
			strHtml += "<div id='<%=IDPrefix%>divBeneficiarySSN" + obj.index + "' <%=IIf(IsInMode("777") AndAlso IsInFeature("visibility"), "class='showfield-section ssn-block' data-show-field-section-id='" & BuildShowFieldSectionID("divBeneficiarySSN1") & "' data-section-name='" & IDPrefix & "Beneficiary SSN' data-default-state='off'", "")%>>";
			strHtml += "<div data-role='fieldcontain' id=<%=IDPrefix%>'div" + obj.id + "Stuff'>";

			strHtml += '<div class="row">';
			strHtml += '<div class="col-xs-6 no-padding"><label for="<%=IDPrefix%>' + txtSSN1 + '" id="<%=IDPrefix%>lbl' + obj.id + '" class="' + (obj.isRequired ? ' RequiredIcon"' : "") + '>' + obj.label + '</label></div>';
			strHtml += '<div class="col-xs-6 no-padding ui-label-ssn">';
			strHtml += '<label class="pull-right header_theme2 ssn-ico lock-ico" style="padding-left: 4px; padding-right: 2px; display: inline;"></label>';
			strHtml += '<label id="<%=IDPrefix%>spOpenSsnSastisfy" onclick="openPopup(\'#<%=IDPrefix%>popSSNSastisfy\')" class="pull-right focus-able header_theme2 ssn-ico question-ico abort-renameable" tabindex="0" style="display: inline;"></label>';
			strHtml += '<div style="display: inline;" class="hidden"><label is_show="1" tabindex="0" class="pull-right header_theme2 rename-able shadow-btn focus-able" style="margin-right: 10px; cursor: pointer;" onclick="<%=IDPrefix%>toggleBeneficiarySSNText(this,\'' + obj.id + '\');" onkeypress="<%=IDPrefix%>toggleBeneficiarySSNText(this, \'' + obj.id + '\');">' + hideLabel + '</label></div>';
			strHtml += '<div style="display: inline;"><label is_show="0" tabindex="0" class="pull-right header_theme2 rename-able shadow-btn focus-able" style="margin-right: 10px; cursor: pointer;" onclick="<%=IDPrefix%>toggleBeneficiarySSNText(this,\'' + obj.id + '\');" onkeypress="<%=IDPrefix%>toggleBeneficiarySSNText(this, \'' + obj.id + '\');">' + showLabel + '</label></div>';
			strHtml += '<div class="clearfix"></div>';
			strHtml += '</div>';
			strHtml += '</div>';
			
			strHtml += "<div id='<%=IDPrefix%>div" + obj.id + "' class='ui-input-ssn row'>";
			strHtml += "<div class='col-xs-4'><input type='password' aria-labelledby='<%=IDPrefix%>lbl" + obj.id + "' pattern='[0-9]*' placeholder='---' id='<%=IDPrefix%>" + txtSSN1 + "'  maxlength='3' next-input='<%=IDPrefix%>" + txtSSN2 + "' onkeydown='limitToNumeric(event);' onkeyup='onKeyUp(event,\"#<%=IDPrefix%>" + txtSSN1 + "\",\"#<%=IDPrefix%>" + txtSSN2 + "\", \"3\");' value=''/><input type='hidden' id='<%=IDPrefix%>hd" + obj.id + "1' value=''/></div>";
			strHtml += "<div class='col-xs-4'><input type='password' aria-labelledby='lbl" + obj.id + "' pattern='[0-9]*' placeholder='--' id='<%=IDPrefix%>" + txtSSN2 + "' maxlength ='2' next-input='<%=IDPrefix%>" + txtSSN3 + "' onkeydown='limitToNumeric(event);' onkeyup='onKeyUp(event,\"#<%=IDPrefix%>" + txtSSN2 + "\",\"#<%=IDPrefix%>" + txtSSN3 + "\", \"2\");'  value='' /><input type='hidden' id='<%=IDPrefix%>hd" + obj.id + "2' value=''/></div>";
			strHtml += "<div class='col-xs-4' style='padding-right: 0px;'><input aria-labelledby='<%=IDPrefix%>lbl" + obj.id + "' type='<%=TextAndroidTel%>' pattern='[0-9]*' placeholder='----' id='<%=IDPrefix%>" + txtSSN3 + "' maxlength ='4' onkeydown='limitToNumeric(event);' value=''/><input type='hidden' id='<%=IDPrefix%>txt" + obj.id + "' value='' class='combine-field-value'/></div></div></div>";
			strHtml += "</div>";
		} else if (obj.id == "business_owned_percent" + index) {
			
			strHtml += "<div class='ui-input-inline-group row'>";
			strHtml += '<div class="col-xs-6">';
			strHtml += '<div data-role="fieldcontain" style="padding: 3px 0;">';
			strHtml += '<div class="hide-at-500px" style="width:100%;">';
			strHtml += '<label for="<%=IDPrefix%>txt' + obj.id + '" class="RequiredIcon">% Business Owned</label>';
			strHtml += '</div>';
			strHtml += '<div class="show-at-500px" style="width:100%;">';
			strHtml += '<label for="<%=IDPrefix%>txt' + obj.id + '" class="RequiredIcon">% Bus. Owned</label>	';
			strHtml += '</div>';
			strHtml += '<input type="<%=TextAndroidTel%>" pattern="[0-9]*" id="<%=IDPrefix%>' + obj.id + '" class="numeric" maxlength="6"/>';
			strHtml += '</div>';
			strHtml += '</div>';
			strHtml += '<div class="col-xs-6">';
			strHtml += '<div data-role="fieldcontain" class="guide-btn-wrapper">';
			strHtml += '<div style="padding-right: 25px;">';
			strHtml += '<label for="<%=IDPrefix%>has_control' + index + '">Has Control</label>';
			strHtml += '<a class="guide-btn header_theme2 abort-renameable" href="#<%=IDPrefix%>popBusinessAccountBeneficialOwnedInfo" data-position-to="window" data-rel="popup"><em class="fa fa-question-circle"></em></a>';
			strHtml += '</div>';
			strHtml += '<select id="<%=IDPrefix%>has_control' + index + '">';
			strHtml += '<option value="No" selected="selected">No</option>';
			strHtml += '<option value="Yes">Yes</option>';
			strHtml += '</select>';
			strHtml += '</div>';
			strHtml += '</div>';

			strHtml += "</div>";
		} else {
			strHtml += "<div data-role='fieldcontain'>";
			strHtml += "<label for='<%=IDPrefix%>" + obj.id + "' class=" + (obj.isRequired ? "'RequiredIcon'" : "") + ">" + obj.label + "</label>";
			if (obj.type == "text" || obj.type == "tel") {
				strHtml += "<input type='" + obj.type + "' id='<%=IDPrefix%>" + obj.id + "'";
            	if (obj.className != "") {
            		strHtml += " class='" + obj.className + "'";
            	}
            	if (obj.maxLength != "") {
            		strHtml += "maxlength='" + obj.maxLength + "'";
            	}
				if (obj.id == "address" + index || obj.id == "zip" + index || obj.id == "city" + index) {
					strHtml += " onchange='<%=callVerifyAddressJSFunction()%>'".replace("index", index);
				}
            	strHtml += "/>";
			} else if (obj.type == "dropdown") {
				strHtml += "<select id='<%=IDPrefix%>" + obj.id + "'";
				if (obj.id == "state" + index) {
					strHtml += " onchange='<%=callVerifyAddressJSFunction()%>'".replace("index", index);
				}
				strHtml += ">";
            	if (obj.id == "role" + index) {
            		strHtml += "<%=Common.RenderDropdownlistWithEmpty(RoleList, "", "--Please select--", "") %>";
				} else if (obj.id == "citizenship" + index) {
					strHtml += "<%=CitizenshipStatusDropdown%>";
				} else if (obj.id == "state" + index) {
					strHtml += "<%=StateDropdown%>";
				} else if (obj.id == "suffix" + index) {
					strHtml += "<%=Common.RenderDropdownlist(GetSuffixList(), "")%>";
				}
				strHtml += "</select>";
			}
			strHtml += "</div>";
			if (obj.type == "dropdown" && obj.id == "state" + index) {
				strHtml += "<div><span id='<%=IDPrefix%>spVerifyBeneficialAddressMessage"+ index +"' class='require-span' style='display: none;'>Unable to validate address</span> </div>";
			}
		}
			return strHtml;
	}

	function <%=IDPrefix%>showBeneficiary(obj) {
		var selectedValue = $(obj).attr('status');
		if (selectedValue == "N") {
			$(obj).parent().addClass("hidden");
			$(obj).attr('status', 'Y');
			$('#<%=IDPrefix%>divBeneficiaryPanel').show();
		} else {
			$(obj).parent().removeClass("hidden");
			$(obj).attr('status', 'N');
			$('#<%=IDPrefix%>divBeneficiaryPanel').hide();

		}
	}
	function <%=IDPrefix%>toggleBeneficiarySSNText(btn, id) {
		var $self = $(btn);
		if ($self.attr("contenteditable") == "true") return false;
		var $container = $self.closest("div.Beneficiary");
		if ($self.attr('is_show') == '0') {
			$self.parent().addClass("hidden");
			$self.closest("div.ui-label-ssn").find("label[is_show='1']").parent().removeClass("hidden");
			if (isMobile.any() && !isMobile.Windows()) { //moz-text-security does not works on window devices
				var ssn1 = "";
				var ssn2 = "";
				var matchs = /^(\w*)-(\w*)-(\w*)$/i.exec($("#txt" + id, $container).val());
				if (typeof matchs != "undefined" && matchs != null) {
					ssn1 = matchs[1];
					ssn2 = matchs[2];
				}
				$("#<%=IDPrefix%>txt" + id + "_1", $container).removeClass("mask-password").val("").val(ssn1); //keep .val("") to prevent bug on mobile devices
				$("#<%=IDPrefix%>txt" + id + "_2", $container).removeClass("mask-password").val("").val(ssn2);
    		} else {
    			$("#<%=IDPrefix%>txt" + id + "_1", $container).attr('type', 'text');
    			$("#<%=IDPrefix%>txt" + id + "_2", $container).attr('type', 'text');
    		}
		} else {
			$self.parent().addClass("hidden");
			$self.closest("div.ui-label-ssn").find("label[is_show='0']").parent().removeClass("hidden");
			if (isMobile.any() && !isMobile.Windows()) { //moz-text-security does not works on window devices
				$("#<%=IDPrefix%>txt" + id + "_1", $container).addClass("mask-password");
				$("#<%=IDPrefix%>txt" + id + "_2", $container).addClass("mask-password");
			} else {
				$("#<%=IDPrefix%>txt" + id + "_1", $container).attr('type', 'password');
				$("#<%=IDPrefix%>txt" + id + "_2", $container).attr('type', 'password');
			}
		}
		$(btn).focus();
	}
	function <%=IDPrefix%>ValidateBABenficialOwnersInfo() {
        var validator = true;
        if ($.lpqValidate("<%=IDPrefix%>ValidateBeneficialOwner") == false) {
            validator = false;
        }
		if ($('#<%=IDPrefix%>showBeneficiary').attr('status') == 'Y' && $.lpqValidate("<%=IDPrefix%>ValidateBeneficiary") == false) {
			validator = false;
        }
		if (validator) {
			<%If NextPage.ToUpper() = "PAGEFS" Then %>
			if ($("#divFundingTypeList a.btn-header-theme").length > 0 && xaProductFactory.hasFundedProducts()) {
				goToNextPage("#pageFS");
			} else {
				goToNextPage("#reviewpage");
			}
			<%Else%>
			goToNextPage("#<%=NextPage%>");
			<%End If%>
		} else {
			Common.ScrollToError();
		}
	}

	function <%=IDPrefix%>registerBeneficiaryValidator(index) {

		$('#<%=IDPrefix%>first_name' + index).observer({
			validators: [
                function (partial) {
                	if (!Common.ValidateText($(this).val())) {
                		return "First Name is required";
                    }
                	return '';
                }
            ],
			validateOnBlur: true,
			group: "<%=IDPrefix%>ValidateBeneficiary"
		});
		
		$('#<%=IDPrefix%>last_name' + index).observer({
			validators: [
				function (partial) {
                    if (!Common.ValidateText($(this).val())) {
                     	return "Last Name is required";
                    }
                    return '';
                }
                ],
			validateOnBlur: true,
			group: "<%=IDPrefix%>ValidateBeneficiary"
		});

		$('#<%=IDPrefix%>divdob' + index).observer({
			validators: [
				function (partial) {
					<%=IDPrefix%>updateHiddenBeneficiaryDOB("#<%=IDPrefix%>beneficiary" + index, index);
					var $DOB = $('#<%=IDPrefix%>txtdob' + index);
					//var validDateMessage = "Valid Date of Birth is required";
					//var requiredDateMessage = "Date of Birth is required";
					if ($DOB.val() == "") {
						if (!<%=IDPrefix%>IsBeneficialOwner(index)) {
							return ""; //if it is not beneficial owner -> skip validating  
						}
						return "Date of Birth is required";;
					}
                    var dob = moment($DOB.val(), "MM-DD-YYYY");
                    if (!Common.IsValidDate($DOB.val())) {
						return "Valid Date of Birth is required";
					} else if (dob.year() < 1900) {
						return "Date of Birth is too old";
					} else if (dob.isAfter(moment())) {
						//beneficiary birthday range - from the past to current date	
						return "Date of Birth must be equal or less than current date";
				    }
				    return "";
				}
            ],
			validateOnBlur: true,
			group: "<%=IDPrefix%>ValidateBeneficiary"
		});
		$('#<%=IDPrefix%>divssn' + index).observer({
			validators: [
				function (partial, evt) {
					if ($(this).closest(".ssn-block").length > 0 && $(this).closest(".ssn-block").hasClass("hidden")) {
						return "";
					}
					<%=IDPrefix%>updateHiddenBeneficiarySSN("#<%=IDPrefix%>beneficiary" + index, index);
					var ssn = $("#<%=IDPrefix%>txtssn" + index + "_1", "#<%=IDPrefix%>beneficiary" + index).val() + $("#<%=IDPrefix%>txtssn" + index + "_2", "#<%=IDPrefix%>beneficiary" + index).val() + $("#<%=IDPrefix%>txtssn" + index + "_3", "#<%=IDPrefix%>beneficiary" + index).val();
					if (/^[0-9]{9}$/.test(ssn) == false) {
						return 'Valid SSN is required';
					}
					return "";
				}
        	],
			validateOnBlur: true,
			group: "<%=IDPrefix%>ValidateBeneficiary"
		});
		$('#<%=IDPrefix%>business_owned_percent' + index).observer({
			validators: [
                function (partial) {
                    if (!<%=IDPrefix%>requiredBusinessOwned()) {
                        return "";
                    }
                    var text = $(this).val();
					if (Common.ValidateText(text) == false) {
						return "Business Owned is required";
					}
                    if ((/^[0-9]+$/.test(text) == false && /^[0-9]*.[0-9]+$/.test(text) == false) || parseFloat(text) > 100) {
                        return 'Valid Business Owned percentage value is from 0 to 100.';
					}
					return "";
				}
			],
			validateOnBlur: true,
			group: "<%=IDPrefix%>ValidateBeneficiary"
		});
		$('#<%=IDPrefix%>role' + index).observer({
			validators: [
				function (partial) {
					var text = $(this).val();
					if (Common.ValidateText(text) == false) {
						return "Role is required";
					}
					return "";
				}
			],
			validateOnBlur: true,
			group: "<%=IDPrefix%>ValidateBeneficiary"
        });                      
		    $('#<%=IDPrefix%>address' + index).observer({
			    validators: [
                    function (partial) {
                        if (!<%=IDPrefix%>IsBeneficialOwner(index)) return ""; //if it is not beneficial owner -> skip validating  
					    var text = $(this).val();
					    if (Common.ValidateText(text) == false) {
						    return "Address is required";
					    }
					    return "";
				    }
			    ],
			    validateOnBlur: true,
			    group: "<%=IDPrefix%>ValidateBeneficiary"
		    });
		    $('#<%=IDPrefix%>zip' + index).observer({
			    validators: [
                    function (partial) {
                        if (!<%=IDPrefix%>IsBeneficialOwner(index)) return ""; //if it is not beneficial owner -> skip validating 
					    var text = $(this).val();
					    if (Common.ValidateText(text) == false) {
						    return "Zip is required";
						}
						if (Common.ValidateZipCode(text, this) == false) {
							return "Valid zip is required";
						}
					    return "";
				    }
			    ],
			    validateOnBlur: true,
			    group: "<%=IDPrefix%>ValidateBeneficiary"
		    });
		    $('#<%=IDPrefix%>city' + index).observer({
			    validators: [
                    function (partial) {
                        if (!<%=IDPrefix%>IsBeneficialOwner(index)) return ""; //if it is not beneficial owner -> skip validating 
					    var text = $(this).val();
					    if (Common.ValidateText(text) == false) {
						    return "City is required";
					    }
					    return "";
				    }
			    ],
			    validateOnBlur: true,
			    group: "<%=IDPrefix%>ValidateBeneficiary"
		    });
		    $('#<%=IDPrefix%>state' + index).observer({
			    validators: [
                    function (partial) {
                        if (!<%=IDPrefix%>IsBeneficialOwner(index)) return ""; //if it is not beneficial owner -> skip validating 
					    var text = $(this).val();
					    if (Common.ValidateText(text) == false) {
						    return "State is required";
					    }
					    return "";
				    }
			    ],
			    validateOnBlur: true,
			    group: "<%=IDPrefix%>ValidateBeneficiary"
                });    
	}

	function <%=IDPrefix%>registerBeneficiariesValidator() {
		var beneficiaryEle = $('#<%=IDPrefix%>beneficiaryContainer .Beneficiary');
		var count = 0;
		beneficiaryEle.each(function () {
			count += 1;
			<%=IDPrefix%>registerBeneficiaryValidator(count);        
        });
	}
	function <%=IDPrefix%>getApplicantBeneficialOwners() {
		var result = [];
		<%If SelectedIDPrefixes IsNot Nothing AndAlso SelectedIDPrefixes.Any() Then%>
		<%	 For Each pfx As String In SelectedIDPrefixes%>
		var <%=pfx%>appInfo = {};
		<%=pfx%>SetBAApplicantInfo(<%=pfx%>appInfo);
		if (<%=pfx%>appInfo.<%=pfx%>HasControl == "Yes" || (!isNaN(<%=pfx%>appInfo.<%=pfx%>BusinessOwned) && parseFloat(<%=pfx%>appInfo.<%=pfx%>BusinessOwned) >= 25)) {
            var item = {};
            item.BusinessOwned = <%=pfx%>appInfo.<%=pfx%>BusinessOwned;
            if (isNaN(item.BusinessOwned) || item.BusinessOwned =="") item.BusinessOwned = "0";
			item.HasControl = <%=pfx%>appInfo.<%=pfx%>HasControl;
            item.RoleType = <%=pfx%>appInfo.<%=pfx%>RoleType;
            item.ControlTitle =<%=pfx%>appInfo.<%=pfx%>ControlTitle;
            item.ControlTitleText =<%=pfx%>appInfo.<%=pfx%>ControlTitleText;
			item.FirstName = <%=pfx%>appInfo.<%=pfx%>FirstName;
			item.MiddleName = <%=pfx%>appInfo.<%=pfx%>MiddleName;
			item.LastName = <%=pfx%>appInfo.<%=pfx%>LastName;
			item.NameSuffix = <%=pfx%>appInfo.<%=pfx%>NameSuffix;
			item.CitizenshipStatus = <%=pfx%>appInfo.<%=pfx%>CitizenshipStatus;
			item.Ssn = <%=pfx%>appInfo.<%=pfx%>SSN;
			item.Dob = <%=pfx%>appInfo.<%=pfx%>DOB;
			item.AddressStreet = <%=pfx%>appInfo.<%=pfx%>AddressStreet;
			item.AddressZip = <%=pfx%>appInfo.<%=pfx%>AddressZip;
			item.AddressCity = <%=pfx%>appInfo.<%=pfx%>AddressCity;
            item.AddressState = <%=pfx%>appInfo.<%=pfx%>AddressState;
             var IsBeneficialOwner = 'N';
            if (item.HasControl == "Yes") {
                IsBeneficialOwner = 'Y';
            } else {
                if (item.BusinessOwned >= 25) {
                    IsBeneficialOwner = 'Y';
                }
            }
            item.IsBeneficialOwner = IsBeneficialOwner;
			result.push(item);	
		}
		
	 <% Next%>
		<%End If%>
		return result;
	}
	function <%=IDPrefix%>getBeneficialOwners(applicantBeneficialOwnersIncluded) {
		if (typeof applicantBeneficialOwnersIncluded == "undefined") applicantBeneficialOwnersIncluded = false;
		var beneficiaryArr = [];
		if (applicantBeneficialOwnersIncluded == true) {
			beneficiaryArr = <%=IDPrefix%>getApplicantBeneficialOwners();
		}
		var beneficiaryEle = $('#<%=IDPrefix%>beneficiaryContainer .Beneficiary');
        if ($('#<%=IDPrefix%>showBeneficiary').attr('status') == 'Y') {
			var count = 0;
			beneficiaryEle.each(function (idx, ele) {
				count = idx + 1;
                var item = {};
                var IsBeneficialOwner = 'N';
                if (<%=IDPrefix%>IsBeneficialOwner(count)) {
                    IsBeneficialOwner = 'Y';
                }
                item.IsBeneficialOwner = IsBeneficialOwner;
				item.BusinessOwned = $('#<%=IDPrefix%>business_owned_percent' + count).val();
				if (isNaN(item.BusinessOwned) || item.BusinessOwned =="") item.BusinessOwned = "0";
				item.HasControl = $('#<%=IDPrefix%>has_control' + count).val();
                item.ControlTitle = $('#<%=IDPrefix%>role' + count).val();
                item.ControlTitleText=$('#<%=IDPrefix%>role' + count+' option:selected').text();
				item.FirstName = $('#<%=IDPrefix%>first_name' + count).val();
				item.MiddleName = $('#<%=IDPrefix%>middle_name' + count).val();
				item.LastName = $('#<%=IDPrefix%>last_name' + count).val();
				item.NameSuffix = $('#<%=IDPrefix%>suffix' + count).val();
				item.CitizenshipStatus = $('#<%=IDPrefix%>citizenship' + count).val();
				<%If EnableBeneficiarySSN then %>
				item.Ssn = Common.GetSSN($('#<%=IDPrefix%>txtssn' + count).val());
				<%End If%>
				item.Dob = $('#<%=IDPrefix%>txtdob' + count).val();
				item.AddressStreet = $('#<%=IDPrefix%>address' + count).val();
				item.AddressZip = $('#<%=IDPrefix%>zip' + count).val();
				item.AddressCity = $('#<%=IDPrefix%>city' + count).val();
				item.AddressState = $('#<%=IDPrefix%>state' + count).val();
				beneficiaryArr.push(item);
            });
		}
		return beneficiaryArr;
	}

	function <%=IDPrefix%>showAndHideBeneficiaryBtn(numCount) {
		if (numCount > 0) {
			$('#<%=IDPrefix%>divBtn').show();
        } else {
        	$('#<%=IDPrefix%>divBtn').hide();
			$('#<%=IDPrefix%>showBeneficiary').parent().removeClass("hidden");
        	
			$('#<%=IDPrefix%>showBeneficiary').attr('status', 'N');
        }
	}

	function <%=IDPrefix%>beneficiaryCounter() {
		var count = 0;
		function changeCounter(val) {
			count += val;
		}
		return {
			increment: function () {
				changeCounter(1);
			},
			decrement: function () {
				changeCounter(-1);
			},
			getCounter: function () {
				return count;
			}
		}
	}

	function <%=IDPrefix%>viewBeneficialOwners() {
		var beneficiaryInfo = <%=IDPrefix%>getBeneficialOwners(true);
		var strHtml = "";
		for (var i = 0; i < beneficiaryInfo.length; i++) {
			strHtml += '<div class="row">';
			strHtml += '<div class="col-sm-6 col-xs-12"><div class="row"><div class="col-xs-6 text-right row-title"><span class="bold">Last Name</span></div><div class="col-xs-6 text-left row-data"><span>' + beneficiaryInfo[i].LastName + '</span></div></div></div>';
			strHtml += '<div class="col-sm-6 col-xs-12"><div class="row"><div class="col-xs-6 text-right row-title"><span class="bold">First Name</span></div><div class="col-xs-6 text-left row-data"><span>' + beneficiaryInfo[i].FirstName + '</span></div></div></div>';
			strHtml += '<div class="col-sm-6 col-xs-12"><div class="row"><div class="col-xs-6 text-right row-title"><span class="bold">Title</span></div><div class="col-xs-6 text-left row-data"><span>' + beneficiaryInfo[i].ControlTitleText + '</span></div></div></div>';
			strHtml += '<div class="col-sm-6 col-xs-12"><div class="row"><div class="col-xs-6 text-right row-title"><span class="bold">Date of Birth</span></div><div class="col-xs-6 text-left row-data"><span>' + beneficiaryInfo[i].Dob + '</span></div></div></div>';
			strHtml += '<div class="col-sm-6 col-xs-12"><div class="row"><div class="col-xs-6 text-right row-title"><span class="bold">Ownership</span></div><div class="col-xs-6 text-left row-data"><span>' + beneficiaryInfo[i].BusinessOwned + '%</span></div></div></div>';
            strHtml += '<div class="col-sm-6 col-xs-12"><div class="row"><div class="col-xs-6 text-right row-title"><span class="bold">Control</span></div><div class="col-xs-6 text-left row-data"><span>' + beneficiaryInfo[i].HasControl + '</span></div></div></div>';
            strHtml += '<div class="col-sm-6 col-xs-12"><div class="row"><div class="col-xs-6 text-right row-title"><span class="bold">Beneficial Owner</span></div><div class="col-xs-6 text-left row-data"><span>' + (beneficiaryInfo[i].IsBeneficialOwner=="Y" ? "Yes" : "No") + '</span></div></div></div>';
       		strHtml += "</div><br/>";
		}
   		return strHtml;
	}
   function <%=IDPrefix%>updateHiddenBeneficiarySSN(container, counter) {
		if ($("#<%=IDPrefix%>txtssn" + counter + "_1", $(container)).val().length == 3) {
			var ssn = $("#<%=IDPrefix%>txtssn" + counter + "_1", $(container)).val() + '-' + $("#<%=IDPrefix%>txtssn" + counter + "_2", $(container)).val() + '-' + $("#<%=IDPrefix%>txtssn" + counter + "_3", $(container)).val();
			$("#<%=IDPrefix%>txtssn" + counter, $(container)).val(ssn);
		}
	}
	function <%=IDPrefix%>updateHiddenBeneficiaryDOB(container, counter) {
		var month = padLeft($("#<%=IDPrefix%>txtdob" + counter + "_1").val(), 2);
		var day = padLeft($("#<%=IDPrefix%>txtdob" + counter + "_2").val(), 2);
		var year = padLeft($("#<%=IDPrefix%>txtdob" + counter + "_3").val(), 4);

		var date = month + '/' + day + '/' + year;
		$("#<%=IDPrefix%>txtdob").val(date == "//" ? "" : date);
	}
	function <%=IDPrefix%>addBeneficiaryBlock(cnt) {
		$('#<%=IDPrefix%>beneficiaryContainer').append(<%=IDPrefix%>renderingBeneficiaries(cnt));
		$('#<%=IDPrefix%>divBeneficiaryPanel').trigger('create');
		$(".numeric", "#<%=IDPrefix%>beneficiary" + cnt).numeric();
		indexRenameItems("#<%=IDPrefix%>beneficiary" + cnt);
		<%If IsInMode("777") Andalso IsInFeature("rename")%>
		attachEditButton("#<%=IDPrefix%>beneficiary" + cnt);
		<%End If%>
		performRename("#<%=IDPrefix%>beneficiary" + cnt);
		$("#<%=IDPrefix%>beneficiary" + cnt + " select").each(function (idx, ele) {
			$(ele).on("change", function () {
				setTimeout(function () {
					$(ele).focus();
				}, 100);
			});
		});
		$(".lock-ico", $("#<%=IDPrefix%>beneficiary" + cnt)).html($("#<%=IDPrefix%>divIconLock16").html());
		$(".question-ico", $("#<%=IDPrefix%>beneficiary" + cnt)).html($("#<%=IDPrefix%>divIconQuestion16").html());
		if (isMobile.any() && !isMobile.Windows()) {//moz-text-security does not works on window devices
			$("#<%=IDPrefix%>txtssn" + cnt + "_1,#<%=IDPrefix%>txtssn" + cnt + "_2", "#<%=IDPrefix%>beneficiary" + cnt)
			.attr("type", "tel")
			.addClass("mask-password");
		} else {
			$("#<%=IDPrefix%>txtssn" + cnt + "_1,#<%=IDPrefix%>txtssn" + cnt + "_2", "#<%=IDPrefix%>beneficiary" + cnt)
			.attr("type", "password");
		}
		$('.ui-input-ssn input', "#<%=IDPrefix%>beneficiary" + cnt).on('focusout', function () {
			<%=IDPrefix%>updateHiddenBeneficiarySSN("#<%=IDPrefix%>beneficiary" + cnt, cnt);
		});
		$('.ui-input-date input', "#<%=IDPrefix%>beneficiary" + cnt).on('focusout', function () {
			<%=IDPrefix%>updateHiddenBeneficiaryDOB("#<%=IDPrefix%>beneficiary" + cnt, cnt);
		});
		$('#<%=IDPrefix%>txtdob' + cnt).datepicker({
			showOn: "button",
			changeMonth: true,
			changeYear: true,
			yearRange: "-100:+0",
			buttonImage: "/images/calendar.png",
			buttonImageOnly: true,
			onSelect: function (value, inst) {
				var date = $(this).datepicker('getDate');
				$("#<%=IDPrefix%>txtdob" + cnt + "_2").val(padLeft(date.getDate(), 2));
				$("#<%=IDPrefix%>txtdob" + cnt + "_1").val(padLeft(date.getMonth() + 1, 2));
				$("#<%=IDPrefix%>txtdob" + cnt + "_3").val(padLeft(date.getFullYear(), 4));
				$('#<%=IDPrefix%>txtdob' + cnt).datepicker("hide");
				$("#<%=IDPrefix%>txtdob" + cnt + "_1").trigger("focus").trigger("blur");
			}
        });
        if (typeof <%=IDPrefix%>beneficiaryPhoneFormat != "undefined") {
		    <%=IDPrefix%>beneficiaryPhoneFormat();
        }
		<%If IsInMode("777") AndAlso IsInFeature("visibility") Then%>
		<%If EnableBeneficiarySSN%>
		ShowFieldButton($("#<%=IDPrefix%>divBeneficiarySSN" + cnt));
		triggerUpdateShowField({ controllerId: '<%=BuildShowFieldSectionID("divBeneficiarySSN1")%>' });
		<%End If%>
		<%End If%>
		applyHeaderThemeCss("#page<%=IDPrefix%>");
	}
    $(function () {     
		//beneficiary
		var counter = <%=IDPrefix%>beneficiaryCounter();
    	$('#<%=IDPrefix%>showBeneficiary').on('click', function (e) {
    		if ($('#<%=IDPrefix%>beneficiaryContainer').children().length == 0) {
    			counter.increment();
    			var cnt = counter.getCounter();
    			<%=IDPrefix%>addBeneficiaryBlock(cnt);
                <%=IDPrefix%>ShowAndHideRequiredIcon(cnt);
    		}
    		<%=IDPrefix%>showBeneficiary(this);
    		<%=IDPrefix%>showAndHideBeneficiaryBtn(counter.getCounter());
    		if ($(this).attr('status') == 'Y') {
    			delete OBSERVERDB["<%=IDPrefix%>ValidateBeneficiary"];
    			<%=IDPrefix%>registerBeneficiariesValidator();               
           }
    		e.preventDefault();
    	});
		$('.addDivBtn', "#<%=IDPrefix%>divBeneficiaryPanel").on('click', function () {
    		counter.increment();
    		var cnt = counter.getCounter();
    		<%=IDPrefix%>addBeneficiaryBlock(cnt);
            <%=IDPrefix%>ShowAndHideRequiredIcon(cnt);

    	});
		$('.removeDivBtn', "#<%=IDPrefix%>divBeneficiaryPanel").on('click', function () {
    		if (counter.getCounter() > 0) {
    			$('#<%=IDPrefix%>beneficiary' + counter.getCounter()).remove();
        		counter.decrement();
        		<%=IDPrefix%>showAndHideBeneficiaryBtn(counter.getCounter());
        	}
    		$('html, body').stop().animate({ scrollTop: $("#<%=IDPrefix%>showBeneficiary").offset().top - 100 }, '500', 'swing');
    	});

    	$('#<%=IDPrefix%>divBtn').on('click', function () {
    		delete OBSERVERDB["<%=IDPrefix%>ValidateBeneficiary"];
    		<%=IDPrefix%>registerBeneficiariesValidator();
    	});
    	
    	$('#<%=IDPrefix%>beneficiaryContainer').on('blur', 'input.numeric', function (e) {
    		if ($.trim($(this).val()) == "") $(this).val(0);
    	});
    	$('#<%=IDPrefix%>beneficiaryContainer').on('focusout', 'div.ui-input-date input', function () {
    		var currentEle = $(this);
    		var maxlength = parseInt(currentEle.attr('maxlength'), 10);
    		// add 0 if value < 10
    		currentEle.val(padLeft(currentEle.val(), maxlength));
    		var currentID = currentEle.attr('id');
    		var prefixID = currentID.substring(0, currentID.length - 2);
    		var month = padLeft($("#" + prefixID + "_1").val(), 2);
    		var day = padLeft($("#" + prefixID + "_2").val(), 2);
    		var year = padLeft($("#" + prefixID + "_3").val(), 4);

    		var date = month + '/' + day + '/' + year;
    		//add birthday to hidden txtDOBID
    		var txtDOBID = prefixID.replace("div", "txt");
    		$('#' + txtDOBID).val(date == "//" ? "" : date);
    	});
		$("#<%=IDPrefix%>popBusinessAccountBeneficialOwnedInfo").on("popupafteropen", function (event, ui) {
			$("#" + this.id + "-screen").height("");
		});
		$("#<%=IDPrefix%>popSSNSastisfy").on("popupafteropen", function (event, ui) {
			$("#" + this.id + "-screen").height("");
		});
		$("#<%=IDPrefix%>popSSNSastisfy").on("popupafterclose", function (event, ui) {
			setTimeout(function () {
				$("#<%=IDPrefix%>spOpenSsnSastisfy").focus();
			}, 100);
		});
		$("#page<%=IDPrefix%>").on("pageshow", function () {
			var applicantBeneficialOwners = <%=IDPrefix%>getApplicantBeneficialOwners();
			var $container = $("#<%=IDPrefix%>divBeneficialOwnersContainer");
			$container.html("");
			if (applicantBeneficialOwners.length > 0) {
				$.each(applicantBeneficialOwners, function (idx, ele) {
					var $divItem = $("<div/>", { "class": "btn-header-theme ui-btn ui-corner-all beneficial-owner-item" });
					$divItem.append("<p>" + $.trim(ele.FirstName + ($.trim(ele.MiddleName) == "" ? " " : " " + ele.MiddleName + " ") + ele.LastName) + "</p>");
					$divItem.append("<p>" + ele.BusinessOwned + "% Ownership</p>");
					if (ele.HasControl == "Yes") {
						$divItem.append("<p>Responsibility to control/manage</p>");
					}
					$divItem.appendTo($container);
				});
			} else {
				$container.html("<p>No applicant is a beneficial owner.</p>");
			}
			$("#<%=IDPrefix%>beneficiaryContainer .beneficiaryLabel").each(function (idx, ele) {
				var $self = $(this);
				$self.text("Beneficiary Owner #" + (idx + 1 + $("#<%=IDPrefix%>divBeneficialOwnersContainer>.beneficial-owner-item").length));
			});
        });
    
        $('#<%=IDPrefix%>beneficiaryContainer').on('change', 'select', function () {
            var selectID = $(this).attr('id');
            if (selectID.indexOf("has_control") > -1) {
                <%=IDPrefix%>ShowAndHideRequiredIcon(selectID.substring(selectID.length-1));
            }
        });
	     $('#<%=IDPrefix%>beneficiaryContainer').on('focusout', 'input', function () {
            var inputID = $(this).attr('id');
            if (inputID.indexOf("business_owned_percent") > -1) {
                <%=IDPrefix%>ShowAndHideRequiredIcon(inputID.substring(inputID.length-1));
            }
        });
        $('#<%=IDPrefix%>divBeneficiaryPanel').observer({
            validators: [
                function (partial) {
                    if (<%=IDPrefix%>ValidateBeneficialOwner() != '') {
                        $('#<%=IDPrefix%>divShowBeneOwnerMessage').css('height', '35px');
                    } else {
                        $('#<%=IDPrefix%>divShowBeneOwnerMessage').css('height', '0');
                    }

                    return <%=IDPrefix%>ValidateBeneficialOwner();
                }
            ],
            validateOnBlur: false,
            group: "<%=IDPrefix%>ValidateBeneficialOwner",
            groupType: "complexUI",
            placeHolder: "#<%=IDPrefix%>spShowBeneOwnerMessage"
        });
    });
    function <%=IDPrefix%>getEntityType() {
         var selectedEntityType ="";
        <%If IsBusinessLoan Then %>
            selectedEntityType = $('#hdSelectedEntityType').val();
        <%Else %>
            selectedEntityType = '<%=BusinessType %>'; //xa business
        <%End If%>
        return selectedEntityType;
    }
    function <%=IDPrefix%>requiredControl() { 
        if ($.inArray(<%=IDPrefix%>getEntityType(), <%=RequiredControlEntityTypes %>) > -1)  { 
            return true;
        }
        return false;
    }
    function <%=IDPrefix%>requiredBusinessOwned() {
        if ($.inArray(<%=IDPrefix%>getEntityType(), <%=RequiredOwneshipEntityTypes %>) > -1) { 
            return true;
        }
        return false;
    }
    function <%=IDPrefix%>ValidateBeneficialOwner() {
        var selectedEntityType = <%=IDPrefix%>getEntityType();       
        var beneficiaryInfo = <%=IDPrefix%>getBeneficialOwners(true);
        var requiredMessage = "One individual with significant responsibility to manage the business is required";
        if (selectedEntityType != undefined && selectedEntityType != "") {
            if (<%=IDPrefix%>requiredBusinessOwned() || <%=IDPrefix%>requiredControl()) { //required at least one applicant has control =Y
                if (beneficiaryInfo.length === 0) {
                    return requiredMessage;
                }
                var hasControl = false;
                for (var i = 0; i < beneficiaryInfo.length; i++) {
                    var businessOwned = beneficiaryInfo[i].BusinessOwned;
                    if (beneficiaryInfo[i].HasControl.toUpperCase() =="YES") {                 
                        hasControl = true;
                        break; 
                    }
                }
                if (!hasControl) {
                    return requiredMessage;
                }
            }  
        }
        return "";
    }   
	// Handles moving focus across SSN field DOB fields
	function <%=IDPrefix%>onKeyUp_DOB(evt, element) {
		//console.log(evt.target, evt.currentTarget, evt.keyCode);
		var elementID = $(element).attr('id');
		var txt1Len = $(element).attr('maxlength');
		var txt1Value = $(element).val();
		if ($(element)[0].hasAttribute("mask-pattern")) {
			//use for SSN text input
			txt1Value = txt1Value.replace(/_/g, "");
		}
		if (txt1Len == txt1Value.length && $(element).data("disable-move-next") != true) {
			$(element).data("disable-move-next", true);
			if (elementID.indexOf("_1") > -1) {
				var dob2ID = elementID.replace("_1", "_2");
				$('#' + dob2ID).focus();
			} else if (elementID.indexOf("_2") > -1) {
				var dob3ID = elementID.replace("_2", "_3");
				$('#' + dob3ID).focus();
			}
		}
	}



	function <%=IDPrefix%>getAddress(index) {
		var zip = "";
		if (Common.ValidateZipCode($('#<%=IDPrefix%>zip' + index).val()) == true) {
			zip = $('#<%=IDPrefix%>zip' + index).val().replace(/-/g, "").substring(0, 5);
		}
		var address = 'street=' + $('#<%=IDPrefix%>address' + index).val() + '&city=' + $('#<%=IDPrefix%>city' + index).val() + '&state=' + $('#<%=IDPrefix%>state' + index).val() + '&zip=' + zip;
		return address;
	}
	function <%=IDPrefix%>verifyAddress(index) {
		if (Common.ValidateZipCode($('#<%=IDPrefix%>zip' + index).val(), $('#<%=IDPrefix%>zip')) == false) return;
		if ($('#hdAddressKey').val() != "") {
			var address = <%=IDPrefix%>getAddress(index);
					if (<%=IDPrefix%>validateAddress(index)) {
						var url = '/handler/Handler.aspx';
						$.ajax({
							url: url,
							async: true,
							cache: false,
							type: 'POST',
							dataType: 'html',
							data: {
								command: 'verifyAddress',
								address: address
							},
							success: function (responseText) {
								if (responseText.indexOf('res="OK"') < 0) {
									//address service is down or configuration is bad, dont do anything
									//TODO: log the error or email admin   

								} else if (responseText.indexOf('FoundSingleAddress="False"') > -1) { //not found single address                      
									$('#<%=IDPrefix%>spVerifyBeneficialAddressMessage' + index).show();
									var shortDesc = /ShortDescriptive="(.*?)"/g.exec(responseText);
									if (shortDesc[1] == 'Not Found') {
										shortDesc[1] = "Unable to Validate Address";
									}
									$('#<%=IDPrefix%>spVerifyBeneficialAddressMessage' + index).text(shortDesc[1]);
								} else if (responseText.indexOf('FoundSingleAddress="True"') > -1) { //found single address                      
									$('#<%=IDPrefix%>spVerifyBeneficialAddressMessage' + index).hide();
									<%=IDPrefix%>updateAddress(responseText, index);
								} else { //unknow case                      
									$('#<%=IDPrefix%>spVerifyBeneficialAddressMessage' + index).hide();
								}
							}
						});
			}
		}
	}
	function <%=IDPrefix%>updateAddress(responseText, index) {
		var txtAddress = $('#<%=IDPrefix%>address' + index);
		var txtCity = $('#<%=IDPrefix%>city' + index);
		var txtZip = $('#<%=IDPrefix%>zip' + index);
		var ddlState = $('#<%=IDPrefix%>state' + index);
		txtAddress.val(/street="(.*?)"/g.exec(responseText)[1]);
		$.lpqValidate.hideValidation(txtAddress);
		txtCity.val(/city="(.*?)"/g.exec(responseText)[1]);
		$.lpqValidate.hideValidation(txtCity);
		ddlState.val(/state="(.*?)"/g.exec(responseText)[1]).selectmenu().selectmenu('refresh');
		$.lpqValidate.hideValidation(ddlState);
		txtZip.val(/zip="(.*?)["]/g.exec(responseText)[1]);  //zip code may be 5 or 10 digit
		$.lpqValidate.hideValidation(txtZip);
	}
	//ok to have missing city and state
	//ok to have missing zip
	function <%=IDPrefix%>validateAddress(index) {
		return (($('#<%=IDPrefix%>address' + index).val() != '' && $('#<%=IDPrefix%>zip' + index).val() != '') || ($('#<%=IDPrefix%>address' + index).val() != '' && $('#<%=IDPrefix%>city' + index).val() != '' && $('#<%=IDPrefix%>state' + index).val() != ''));
	}
    function <%=IDPrefix%>ShowAndHideRequiredIcon(index) {  
        var $lblAddress = $('label[for="<%=IDPrefix%>address' + index+'"]');
        var $lblCity =  $('label[for="<%=IDPrefix%>city' + index+'"]');
        var $lblState =  $('label[for="<%=IDPrefix%>state' + index+'"]');
        var $lblZip = $('label[for="<%=IDPrefix%>zip' + index + '"]');
        var $lblDOB =  $('label[for="<%=IDPrefix%>txtdob' + index+'"]'); 
        var IsBeneficialOwner = <%=IDPrefix%>IsBeneficialOwner(index);        
        if (IsBeneficialOwner) {              
              //show required address
            $lblAddress.addClass('RequiredIcon');
            $lblCity.addClass('RequiredIcon');
            $lblState.addClass('RequiredIcon');
            $lblZip.addClass('RequiredIcon');
            $lblDOB.addClass('RequiredIcon');
        } else {
           //show required address
            $lblAddress.removeClass('RequiredIcon');
            $lblCity.removeClass('RequiredIcon');
            $lblState.removeClass('RequiredIcon');
            $lblZip.removeClass('RequiredIcon');
            $lblDOB.removeClass('RequiredIcon');
        }
        //show and hide required business owned percent
        var $lblBusinessOwned = $('label[for="<%=IDPrefix%>business_owned_percent' + index + '"]');
        var IsRequiredBusinessOwned =<%=IDPrefix%>requiredBusinessOwned();
        if (IsRequiredBusinessOwned) {
            $lblBusinessOwned.addClass('RequiredIcon');
        } else {
             $lblBusinessOwned.removeClass('RequiredIcon');
        }       
    }
   
    function <%=IDPrefix%>IsBeneficialOwner(index) {
        var IsBeneficialOwner = false;
        if ($('#<%=IDPrefix%>has_control' + index).val() == "Yes") {
            IsBeneficialOwner = true;
        } else {
            var businessOwned = $('#<%=IDPrefix%>business_owned_percent' + index).val();         
            if (!isNaN(businessOwned)&&parseFloat(businessOwned)>=25) {
                IsBeneficialOwner = true;
            }
        }
        return IsBeneficialOwner;
    }
    
</script>