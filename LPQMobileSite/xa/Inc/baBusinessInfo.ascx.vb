﻿
Partial Class xa_Inc_baBusinessInfo
	Inherits CBaseUserControl
	Public Property LogoUrl As String
	Public Property StateDropdown As String = ""
	Public Property EnabledAddressVerification As Boolean
	Public Property EnableMailingAddress As Boolean = True
	Public Property EnableOccupancyStatus As Boolean = True
	Public Property OccupyingLocationDropdown As String
	Public Property CollectDescriptionIfOccupancyStatusIsOther As Boolean
	Public Property LiveYearsDropdown As String
	Public Property LiveMonthsDropdown As String
	Public Property BusinessIndustryCodesDropdown As String
	Public Property PreviousPage As String = ""
	Public Property NextPage As String = ""
	Public Property EnableDoingBusinessAs As Boolean = False
	Public Property Availability As String
	Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
		Dim addressCtrl As xa_Inc_saAddress = CType(LoadControl("~/xa/Inc/saAddress.ascx"), xa_Inc_saAddress)
		addressCtrl.IDPrefix = IDPrefix
		addressCtrl.StateDropdown = StateDropdown
		addressCtrl.EnableOccupancyStatus = EnableOccupancyStatus
		addressCtrl.EnabledAddressVerification = EnabledAddressVerification
		addressCtrl.OccupyingLocationDropdown = OccupyingLocationDropdown
		addressCtrl.CollectDescriptionIfOccupancyStatusIsOther = CollectDescriptionIfOccupancyStatusIsOther
		addressCtrl.LiveYearsDropdown = LiveYearsDropdown
		addressCtrl.LiveMonthsDropdown = LiveMonthsDropdown
		addressCtrl.Header = HeaderUtils.RenderPageTitle(14, "Business Physical Address", True)
		addressCtrl.AllowControlOccupancyVisibility = False
		addressCtrl.EnableMailingAddress = EnableMailingAddress
		addressCtrl.MappedShowFieldLoanType = MappedShowFieldLoanType
		plhAddress.Controls.Add(addressCtrl)

	End Sub
End Class

