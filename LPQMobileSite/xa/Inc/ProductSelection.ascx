﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="ProductSelection.ascx.vb" Inherits="xa_Inc_ProductSelection"  %>
<%@ Import Namespace="System.Globalization" %>
<%@ Import Namespace="LPQMobile.Utils" %>
<%If NormalProductList IsNot Nothing AndAlso NormalProductList.Any() Then%>
<div class="product-selection-panel-v1">
	 
<div id="popLimitSelectedAccountExceed" data-role="popup" style="max-width: 400px;">
	<div data-role="content">
		<div class="row">
			<div class="col-xs-12 header_theme2">
				<a data-rel="back" class="pull-right" href="#" class="svg-btn"><%=HeaderUtils.IconClose %><span style="display: none;">close</span></a>
			</div>
		</div>
		<div class="row">
			<div class="col-sm-12">
				<div style="margin: 10px 0; font-weight: normal;" data-place-holder="content"></div>     
			</div>    
		</div>
	</div>
	<div class="div-continue js-no-required-field" style="text-align: center;"><a href="#" data-transition="slide" type="button" onclick="closePopup('#popLimitSelectedAccountExceed')" data-role="button" class="div-continue-button">OK</a></div>
</div>
<div id="popConfirmRemove" data-role="popup" style="max-width: 400px;">
	<div data-role="header">
	<div data-place-holder="heading" style="margin:0px;padding: 10px 0;font-size: 22px;font-weight: 400;text-align: center; color:#fff;">Remove Product</div>
	</div>
	<div data-role="content">
		<div class="row">
			<div class="col-sm-12">
				<div style="margin: 10px 0; font-weight: normal;" data-place-holder="content"></div>
			</div>    
		</div>
	</div>
	<div class=" row div-continue js-no-required-field" style="text-align: center;margin-bottom: 30px; width: 100%;">
		<div class="col-xs-6">
		<a href="#" data-transition="slide" type="button" data-command="remove-product" data-role="button" class="div-continue-button">Remove</a>
			
		</div>
		<div class="col-xs-6">
		<a href="#" data-transition="slide" type="button" onclick="closePopup('#popConfirmRemove')" data-role="button" class="div-continue-button">Cancel</a>
		</div>
	</div>
</div>
	<%If RequiredProductList IsNot Nothing AndAlso RequiredProductList.Count > 0 Then%>
	<div>
		<div id="requiredProductLabel" class="productLabel"><%=HeaderUtils.RenderPageTitle(0, "Required Products", True)%></div>
		<div id="divRequiredProducts">
			<div data-product-key="REQUIRED">
				<% For Each product As ProductInfo In RequiredProductList
						Dim productApy As Decimal, productMinDeposit, productMaxDeposit As Decimal
						If String.IsNullOrEmpty(product.ProductAPY) OrElse Not Decimal.TryParse(product.ProductAPY, productApy) Then
							productApy = -1D
						End If
						If String.IsNullOrEmpty(product.ProductMinDeposit) OrElse Not Decimal.TryParse(product.ProductMinDeposit.Replace("$", ""), productMinDeposit) Then
							productMinDeposit = -1D
						End If
						If String.IsNullOrEmpty(product.ProductMaxDeposit) OrElse Not Decimal.TryParse(product.ProductMaxDeposit.Replace("$", ""), productMaxDeposit) Then
							productMaxDeposit = -1D
						End If
						Dim productDesc = product.GetFilteredProductDescription().Trim()
						%>
				<div class="pool-item-option-wrapper required-prod" data-id="btn_<%=product.ProductCode%>" data-command-type="optionBtn" data-zipcode-filter-type="<%=product.ZipCodeFilterType %>" data-zipcode-pool-id="<%=product.ZipCodePoolId%>" data-service-count="<%=product.ProductServices.Count%>" data-question-count="<%=product.CustomQuestions.Count%>" data-product-code="<%=product.ProductCode%>">
					<div class='pool-item-option'>
						<strong><%=product.ProductName.Trim%></strong>
						<%If Not String.IsNullOrEmpty(productDesc) Then%>
							<p style="margin: 0; padding: 10px 0 0;"><%=productDesc%></p>	
						<%End If%>
						<%If productMinDeposit >= 0 OrElse productApy >= 0 OrElse productMaxDeposit >= 0 Then%>
						<p class="desc">
							<%If productApy >= 0 Then%>
								<span><%=IIf(product.ProductRates IsNot Nothing AndAlso product.ProductRates.Any, "Max APY:", "APY:")%> <%=FormatNumber(productApy, APYPrecision)%>%</span> 
							<%End If%>
							<% If productMinDeposit >= 0 Then%>
							<span data-name="mindeposit">Min Deposit: <%=FormatCurrency(productMinDeposit, 2)%></span>
                 			<%End If%>
							<% If productMaxDeposit >= 0 Then%>
							<span data-name="maxdeposit">Max Deposit: <%=FormatCurrency(productMaxDeposit, 2)%></span>
                 			<%End If%>
						</p>
						<%End If%>
					</div>
					<div class="right-btn btn-add" data-command="add" tabindex="0"><span></span></div>
				</div>
				<%Next%>
			</div>
		</div>
	</div>
	<%End If%>
	
	<div>
		<div id="availableProductLabel" class="productLabel"><%=HeaderUtils.RenderPageTitle(0, "Available Products", True)%></div>
		<div>
			<table class="cat-tbl<%=IIf(NormalProductList.Where(Function(p) p.ProductIsRequired <> "Y").Count < 5, " hidden", "")%>">
				<tr>
					<td>
						<%--Tabs are displayed in the following order--%>
						<div id="btnGroupCat" class="btn-group" role="group" aria-label="...">
							<% For Each orderedAccountType As String In OrderedAccountTypes %>
							<%If orderedAccountType.Equals("OTHER") %>
							<%-- Don't allow rename of OTHER --%>
							<a data-id="<%=HttpUtility.HtmlAttributeEncode(orderedAccountType)%>" class="btn btn-default"><%:Common.MapProductType2String(orderedAccountType)%></a>
							<%Else %>
							<a data-id="<%=HttpUtility.HtmlAttributeEncode(orderedAccountType)%>" class="btn btn-default rename-able"><%:Common.MapProductType2String(orderedAccountType)%></a>
							<%End If %>
							<%Next%>
						</div>
					</td>
				</tr>
				<tr>
					<td>
						<div data-role="fieldcontain">
                            <label for="ddlGroupCat" class="sr-only">Product Category</label>
							<select id="ddlGroupCat">
								<% For Each orderedAccountType As String In OrderedAccountTypes %>
								<%If orderedAccountType.Equals("OTHER") %>
								<%-- Don't allow rename of OTHER --%>
								<option value="<%=HttpUtility.HtmlAttributeEncode(orderedAccountType)%>"><%:Common.MapProductType2String(orderedAccountType)%></option>
								<%Else %>
								<option class="rename-able" value="<%=HttpUtility.HtmlAttributeEncode(orderedAccountType)%>"><%:Common.MapProductType2String(orderedAccountType)%></option>
								<%End If %>
								<%Next%>
							</select>
						</div>
					</td>
				</tr>
			</table>
		</div>
		<%--list all products--%>
		<div id="divAdditionalProducts">
			<% For Each item As KeyValuePair(Of String, List(Of ProductInfo)) In ProductTypeDictionary%>
			<div data-product-key="<%=item.Key%>">
				<% For Each product As ProductInfo In item.Value
						Dim productApy As Decimal, productMinDeposit, productMaxDeposit As Decimal
            			If String.IsNullOrEmpty(product.ProductAPY) OrElse Not Decimal.TryParse(product.ProductAPY, productApy) Then
            				productApy = -1D
            			End If
            			If String.IsNullOrEmpty(product.ProductMinDeposit) OrElse Not Decimal.TryParse(product.ProductMinDeposit.Replace("$", ""), productMinDeposit) Then
            				productMinDeposit = -1D
						End If
						If String.IsNullOrEmpty(product.ProductMaxDeposit) OrElse Not Decimal.TryParse(product.ProductMaxDeposit.Replace("$", ""), productMaxDeposit) Then
							productMaxDeposit = -1D
						End If
						Dim productDesc = product.GetFilteredProductDescription().Trim()
            			%>
					<div class="pool-item-option-wrapper available-prod" data-id="btn_<%=product.ProductCode%>" data-command-type="optionBtn" data-zipcode-filter-type="<%=product.ZipCodeFilterType %>" data-zipcode-pool-id="<%=product.ZipCodePoolId%>" data-service-count="<%=product.ProductServices.Count%>" data-question-count="<%=product.CustomQuestions.Count%>" data-product-code="<%=product.ProductCode%>">
					<div class="pool-item-option">
						<strong><%=product.ProductName.Trim%></strong>
						<%If Not String.IsNullOrEmpty(productDesc) Then%>
							<p style="margin: 0; padding: 10px 0 0;"><%=productDesc%></p>	
						<%End If%>
						<%If productMinDeposit >= 0 OrElse productApy >= 0 OrElse productMaxDeposit >= 0 Then%>
						<p class="desc">
							<%If productApy >= 0 Then%>
								<span><%=IIf(product.ProductRates IsNot Nothing AndAlso product.ProductRates.Any, "Max APY:", "APY:")%> <%=FormatNumber(productApy, APYPrecision)%>%</span> 
							<%End If%>
							<% If productMinDeposit >= 0 Then%>
							<span data-name="mindeposit">Min Deposit: <%=FormatCurrency(productMinDeposit, 2)%></span>
                 			<%End If%>
							<% If productMaxDeposit >= 0 Then%>
							<span data-name="maxdeposit">Max Deposit: <%=FormatCurrency(productMaxDeposit, 2)%></span>
                 			<%End If%>
						</p>
						<%End If%>
						</div>
						<div class="right-btn btn-add" data-command="add" tabindex="0"><span></span></div>
					</div>
				<%Next%>
			</div>
			<%Next%>
		</div>
	</div>
	<div  class="rename-able" style="margin-top: 20px; font-size: 0.9em; font-style: italic; padding-left: 10px;">APY (Annual Percentage Yield)</div>

	<div <%=IIf(Not ProductTypeDictionary.ContainsKey("PRESELECTED"), "class='hidden productLabel'", "class='productLabel'")%>>
		<%=HeaderUtils.RenderPageTitle(0, "Your Selected Products", True)%>
		<div id="divSelectedProductPool">
			<%If ProductTypeDictionary.ContainsKey("PRESELECTED") Then%>
			<%--<%For Each product As ProductInfo In ProductTypeDictionary("PRESELECTED")
					Dim productApy As Decimal
					If String.IsNullOrEmpty(product.ProductAPY) OrElse Not Decimal.TryParse(product.ProductAPY, productApy) Then
						productApy = -1D
					End If
					
					%>
			<div class="pool-item-option-wrapper selected-prod" id="btn_<%=product.ProductCode%>" data-zipcode-filter-type="<%=product.ZipCodeFilterType %>" data-zipcode-pool-id="<%=product.ZipCodePoolId%>" data-product-code="<%=product.ProductCode%>" data-original-account-type="<%=product.OriginalAccountType%>">
				<div class="pool-item-option">
					<strong><%=product.ProductName.Trim()%></strong>
					<%If productApy >= 0 OrElse (product.ProductServices IsNot Nothing AndAlso product.ProductServices.Any(Function(p) p.IsPreSelection OrElse p.IsRequired)) Then%>
					<p class="desc">
						<%If productApy >= 0 Then%>
							<span><%=IIf(product.ProductRates IsNot Nothing AndAlso product.ProductRates.Any, "Max APY:", "APY:")%> <%=FormatNumber(productApy, APYPrecision)%>%</span> 
						<%End If%>
						<%If product.ProductServices IsNot Nothing AndAlso product.ProductServices.Any(Function(p) p.IsPreSelection OrElse p.IsRequired) Then%>
						<span>Selected Features: <%=String.Join(", ", product.ProductServices.Where(Function(p) p.IsPreSelection OrElse p.IsRequired).Select(Function(x) x.Description))%></span>
						<%End If%> 
					</p>
					<%End If%> 
				</div>
				<div class="right-btn" data-command="remove" tabindex="0"><i class="fa fa-times-circle"></i></div>
			</div>
			<%Next%>--%>
			<%End If%>
		</div>
	</div>

	<div id="divProductServicesPool" style="display: none;">
		<%For Each product As ProductInfo In NormalProductList.Where(Function(p As ProductInfo) p.ProductServices IsNot Nothing And p.ProductServices.Any()).ToList()%>
			<div product-code="<%=product.ProductCode%>">
				<%For Each service As CProductService In product.ProductServices%>
              
					<label tabindex="0" id="serv_<%=product.ProductCode%>_<%=service.ServiceCode%>" service_name="<%=SafeHTML(service.Description)%>" service_code="<%=service.ServiceCode %>" 
						account_name="<%=SafeHTML(product.AccountType)%>" class="header-theme-checkbox" <%-- class="<%=IIf(service.IsPreSelection Or service.IsRequired, " active btnActive_on btnHover", "")%>"--%>
						 <%=IIf(Not service.IsActive, "style='display:none;margin-bottom: 2px;'", "style='margin-bottom: 2px;'")%> 
						<%=IIf(service.IsRequired Or service.IsPreSelection, "data-selected='true'", "")%>
						<%=IIf(service.IsRequired, "data-is-required='true'", "")%> >
						<input data-role="none"  id="chk_serv_<%=product.ProductCode%>_<%=service.ServiceCode%>" aria-labelledby="serv_<%=product.ProductCode%>_<%=service.ServiceCode%>" type="checkbox" style="display: none;" <%=IIf(service.IsPreSelection Or service.IsRequired, "checked='checked'", "")%> <%=IIf(service.IsRequired, "data-disabled='true' disabled='disabled' ", "")%>/>
						<%=getProductServiceName(service.Description, service.DescriptionURL)%>
						<%If service.IsRequired Then%>
							<span class='require-span'>*</span>
						<%End If%>
					</label>
				<%Next%>
			</div>
		<%Next%>
	</div>

	<div id="divProductRatesPool" style="display: none;">
		<%For Each product As ProductInfo In NormalProductList.Where(Function(p As ProductInfo) p.ProductRates IsNot Nothing And p.ProductRates.Any()).ToList()%>
			<div class="row rates-table hide-radio" product-code="<%=product.ProductCode%>" selected-rate="">
				<div class="col-sm-12 hidden-xs">
					<table class="horizontal-table" width="100%">
						<thead>
					  <tr>
						  <td style="width: 5%;"></td>
						 <%If InstitutionType = CEnum.InstitutionType.BANK Then%>
								<th>Interest Rate</th>
						  <%Else %>
					  			   <th>Dividend Rate</th>
						  <%end if %>
						  <%If product.IsApyAutoCalculated Then%>
						  <th>APY</th>
						  <%End If%>
						  <%If product.IsAutoCalculateTier Then%>
						  <th>Minimum Balance</th>
						  <%End If %>
						  <th>Minimum Deposit</th>
						  <th>Term (<%=IIf(product.TermType = "D", "days", "months")%>)</th>
					  </tr>
						</thead>
					<tbody>
				<%For Each item As CProductRate In product.ProductRates%>
					  <tr>
						  <td> 
							  <div data-role="controlgroup" style="pointer-events: none;">
								  <%--HttpUtility.UrlEncode is used to encode space in the name bc aria-labelledby can't handle space in the name--%>
							  <label class="custom-radio-button" id="rate_<%=HttpUtility.UrlEncode(product.ProductCode)%>_<%=item.ID%>_h">
								  <span style="display: none">.</span>
							  <input data-role="none" aria-labelledby="rate_<%=HttpUtility.UrlEncode(product.ProductCode)%>_<%=item.ID%>_h" rate_code="<%=item.RateCode%>" type="radio" name="radH_rate_<%=product.ProductCode%>"/>
								</label>  
							  </div>
                          
						  </td>
						  <td> 
							  <% If item.RateDecimal >= 0 Then%>
								<%:FormatNumber(item.RateDecimal, RatePrecision)%>%
							  <%else %>                          
								 --  <!-- display "--" -->
							  <%End If %>
						  </td>
						  <%If product.IsApyAutoCalculated Then%>
						  <td>
							   <%	If item.Apy >= 0 Then%>
								<%=FormatNumber(item.Apy, APYPrecision)%>%
							   <%else %>
									--  <!-- display "--" -->
							   <%End if %>
						  </td>
						  <%End If%>
						  <%If product.IsAutoCalculateTier Then%>
						  <td><%=If(item.MinBalance >= 0, item.MinBalance.ToString("C2", New CultureInfo("en-US")), "N/A")%></td>
						  <%End If%>
						  <td><%=If(item.MinDeposit >= 0, item.MinDeposit.ToString("C2", New CultureInfo("en-US")), "N/A")%></td>
						  <td><%=BindingTerm(item.MinTerm, item.MaxTerm)%></td>
					  </tr>
				<%Next%>
						</tbody>
				  </table>
				</div>
				<div class="col-xs-12 hidden-sm hidden-md hidden-lg">
					<%For Each item As CProductRate In product.ProductRates%>
					<table class="vertical-table" style="width: 100%">
						<tr>
							<td style="width: 5%;" class="radio-col">
								<div data-role="controlgroup" style="pointer-events: none;">
							  <label class="custom-radio-button" id="rate_<%=HttpUtility.UrlEncode(product.ProductCode)%>_<%=item.ID%>_v">
								  <span style="display: none">.</span>
									<input data-role="none" aria-labelledby="rate_<%=HttpUtility.UrlEncode(product.ProductCode)%>_<%=item.ID%>_v" rate_code="<%=item.RateCode%>" type="radio" name="radV_rate_<%=product.ProductCode%>"/>
								</label>    
							  </div>
							</td>
							<td>
								<table style="width: 100%;">
									<tr>
									<%If InstitutionType = CEnum.InstitutionType.BANK Then%>
									<td>Interest Rate</td>
									<%Else%>
									<td>Dividend Rate</td>
									<%End If%>
									<%If item.RateDecimal >= 0 Then%>
									<td><%:FormatNumber(item.RateDecimal, RatePrecision)%>%</td>
									<%Else%>
									<td>-- </td>
									<!-- display "--" -->
									<%End If%>
								</tr>
								<%If product.IsApyAutoCalculated Then%>
								<tr>
									<td>APY:</td>
									<%If item.Apy >= 0 Then%>
									<td><%=FormatNumber(item.Apy, APYPrecision)%>%</td>
									<%Else%>
									<td>-- </td>
									<!-- display "--" -->
									<%End If%>
								</tr>
								<%End If%>
								<%If product.IsAutoCalculateTier Then%>
								<tr>
									<td>Minimum Balance:</td>
									<td><%=If(item.MinBalance >= 0, item.MinBalance.ToString("C2", New CultureInfo("en-US")), "N/A")%></td>
								</tr>
								<%End If%>
								<tr>
									<td>Min Deposit:</td>
									<td><%=If(item.MinDeposit >= 0, item.MinDeposit.ToString("C2", New CultureInfo("en-US")), "N/A")%></td>
								</tr>
								<tr>
									<td>Term (<%=IIf(product.TermType = "D", "days", "months")%>):</td>
									<td><%=BindingTerm(item.MinTerm, item.MaxTerm)%></td>
								</tr>
								</table>
							</td>
						</tr>
					</table>
					<div class="separator"></div>
				<%Next%>
				</div>
			</div>
		<%Next%>
	</div>
	<div id="divProductQuestionPool" style="display: none;"><%=ProductQuestionsHtml%></div>
</div>
<%End If %>
<script language="javascript" type="text/javascript">
	var ZIPCODEPOOLNAME=<%=Newtonsoft.Json.JsonConvert.SerializeObject(ZipCodePoolName)%>;
	var ZIPCODEPOOLLIST = <%=Newtonsoft.Json.JsonConvert.SerializeObject(ZipCodePoolList)%>;
	var NOTFUNDEDPRODUCTCODES =<%=Newtonsoft.Json.JsonConvert.SerializeObject(_NotFundedProductCodes)%>;
	var PRODUCTLIST = <%=NormalProductListStr%>;
	var ACCOUNTNUMBERLIMIT = <%=AccountNumberLimitsStr%>;
	var INSTANCECOUNTLIMIT = <%=InstanceCountLimitsStr%>;
	var APYPRECISION = <%=APYPrecision%>;
</script>