﻿Imports System.Web.Script.Serialization
Imports LPQMobile.Utils

''' <summary>
''' This common module is shared between business account(ba) and business loan(bl)
''' </summary>
Partial Class xa_Inc_baApplicantInfo
    Inherits CBaseUserControl

    Public Property PreviousPage As String
    Public Property Availability As String
    Public Property LogoUrl As String
    Public Property NextPage As String
    Public Property RoleType As String
    Public Property IsJoint As Boolean
    Public Property BusinessType As String
    Public Property IsPrimaryApplicant As Boolean = False
    Public Property LaserScandocAvailable As Boolean = False
    Public Property LegacyScandocAvailable As Boolean = False
    Public Property ScanDocumentKey As String
    Public CurrentConfig As CWebsiteConfig
    Public Property StateDropdown As String = ""
    Public Property EnableMemberNumber As Boolean = False   ' by default
	Public Property MemberNumberRequired As Boolean = False ' by default
	Public Property InstitutionType As CEnum.InstitutionType
	Public Property FieldMaxLength As Integer = 50
    Public Property EnableGender As Boolean
    Public Property EnableMotherMaidenName As Boolean
    Public Property EnableCitizenshipStatus As Boolean
    Public Property CitizenshipStatusDropdown As String
    Public Property EnableMaritalStatus As Boolean
    Public Property MaritalStatusDropdown As String
    Public Property EmployeeOfLenderDropdown As String
    Public Property LenderName As String
    'Public Property OfficialFamilyDropdown As String
    Public Property RelationToPrimaryApplicantDropdown As String
    Public Property EnabledAddressVerification As Boolean
	Public Property EnableOccupancyStatus As Boolean = True
	Public Property EnableMailingAddress As Boolean = True
    Public Property OccupyingLocationDropdown As String
	Public Property CollectDescriptionIfOccupancyStatusIsOther As Boolean
	Public Property OccupancyStatusesForRequiredHousingPayment As List(Of String) = New List(Of String)({"RENT"})
	Public Property LiveYearsDropdown As String
    Public Property LiveMonthsDropdown As String
    Public Property PreferContactMethodDropdown As String
	Public Property EnableGrossMonthlyIncome As Boolean
	Public Property CollectJobTitleOnly As Boolean
    Public Property CollectDescriptionIfEmploymentStatusIsOther As String
    Public Property EmploymentStatusDropdown As String
    Public Property IDCardTypeList As String
    Public Property IsExistIDCardType As Boolean
    Public Property EnableBeneficiarySSN As Boolean
    Public Property EnableBeneficiaryTrust As Boolean
    Public Property EnableAddress As Boolean
    Public Property EnableContactInfo As Boolean
    Public Property EnableContactRelative As Boolean
    Public Property EnableRelationshipToPrimary As Boolean
    Public Property EnableEmployment As Boolean
	Public Property EnableIDCard As Boolean
	Public Property RequirePrimaryIDCard As Boolean
	Public Property FirstName As String = ""
    Public Property LastName As String = ""
    Public Property isBusinessLoan As Boolean = False
    Public Property selectedApplicant As CSelectedApplicant
    Public Property EnableBLDeclaration As Boolean
	Protected RequiredOwneshipEntityTypes As String

	Public Property InbrachEnableAddress As Boolean = False
	Public Property InbrachEnableContactInfo As Boolean = False
	Public Property InbrachEnableContactRelative As Boolean = False
	Public Property InbranchEnableEmployment As Boolean = False
	Public Property InbranchEnableIDCard As Boolean = False
	Public Property InbranchEnableOccupancyStatus As Boolean = False
	Public Property InbranchEnableRelationshipToPrimary As Boolean = False
    Public Property EnableReferenceInfo As Boolean = False
    Protected RoleListDropdown As String = ""

	Public Property EnableDocUpload As Boolean = False
	Public Property RequireDocTitle As Boolean = False
	Public Property DocTitleOptions As List(Of CDocumentTitleInfo)
	Public Property DocUploadTitle As String
    Public Property DocUploadRequired As Boolean

    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        If Not IsPostBack Then
            If LaserScandocAvailable Then
                Dim laserDLScan As Inc_LaserDocumentScan = CType(LoadControl("~/Inc/LaserDocumentScan.ascx"), Inc_LaserDocumentScan)
                laserDLScan.IDPrefix = IDPrefix
                plhLaserDriverLicenseScan.Controls.Add(laserDLScan)
                plhLaserDriverLicenseScan.Visible = True
            Else
                plhLaserDriverLicenseScan.Visible = False
            End If
            Dim legacyDLScan As Inc_NewDocumentScan = CType(LoadControl("~/Inc/NewDocumentScan.ascx"), Inc_NewDocumentScan)
            legacyDLScan.CurrentConfig = CurrentConfig
            legacyDLScan.IDPrefix = IDPrefix
			plhDriverLicenseScan.Controls.Add(legacyDLScan)


            plhEmployment.Visible = False
            If EnableEmployment And Not isBusinessLoan Then
                plhEmployment.Visible = True
                Dim emploment As Inc_MainApp_xaApplicantEmployment = CType(LoadControl("~/Inc/MainApp/xaApplicantEmployment.ascx"), Inc_MainApp_xaApplicantEmployment)
                emploment.Header = HeaderUtils.RenderPageTitle(0, "Employment", True)
				emploment.EmploymentStatusDropdown = EmploymentStatusDropdown
				emploment.MappedShowFieldLoanType = MappedShowFieldLoanType
                emploment.IDPrefix = IDPrefix
                emploment.CollectDescriptionIfEmploymentStatusIsOther = CollectDescriptionIfEmploymentStatusIsOther
				emploment.Config = CurrentConfig.GetWebConfigElement
				emploment.ShowFieldDefaultStateOn = InbranchEnableEmployment
				emploment.EnableGrossMonthlyIncome = EnableGrossMonthlyIncome
				emploment.CollectJobTitleOnly = CollectJobTitleOnly
                plhEmployment.Controls.Add(emploment)
            End If

            plhIDCard.Visible = False
            If EnableIDCard Then
                plhIDCard.Visible = True
                If isBusinessLoan Then
                    Dim BLIDCard As bl_Inc_BLApplicantID = CType(LoadControl("~/bl/Inc/BLApplicantID.ascx"), bl_Inc_BLApplicantID)
                    BLIDCard.IDPrefix = IDPrefix
                    BLIDCard.StateDropdown = StateDropdown
                    BLIDCard.IDCardTypeList = IDCardTypeList
					BLIDCard.IsExistIDCardType = IsExistIDCardType
					BLIDCard.MappedShowFieldLoanType = MappedShowFieldLoanType
                    plhIDCard.Controls.Add(BLIDCard)
                Else
                    Dim primaryIDCard As Inc_MainApp_xaApplicantID = CType(LoadControl("~/Inc/MainApp/xaApplicantID.ascx"), Inc_MainApp_xaApplicantID)
                    primaryIDCard.IDPrefix = IDPrefix
                    primaryIDCard.StateDropdown = StateDropdown
                    primaryIDCard.IDCardTypeList = IDCardTypeList
					primaryIDCard.isExistIDCardType = IsExistIDCardType
					primaryIDCard.MappedShowFieldLoanType = MappedShowFieldLoanType
					primaryIDCard.Header = HeaderUtils.RenderPageTitle(0, "Primary ID Card", True)
					primaryIDCard.ShowFieldDefaultStateOn = InbranchEnableIDCard
					primaryIDCard.Required = RequirePrimaryIDCard
					plhIDCard.Controls.Add(primaryIDCard)

                    'Dim secondaryIDCard As Inc_MainApp_xaApplicantID = CType(LoadControl("~/Inc/MainApp/xaApplicantID.ascx"), Inc_MainApp_xaApplicantID)
                    'secondaryIDCard.IDPrefix = IDPrefix & "2nd"
					'secondaryIDCard.StateDropdown = StateDropdown
					'secondaryIDCard.MappedShowFieldLoanType =MappedShowFieldLoanType
                    'secondaryIDCard.IDCardTypeList = IDCardTypeList
                    'secondaryIDCard.isExistIDCardType = IsExistIDCardType
                    'secondaryIDCard.Header = HeaderUtils.RenderPageTitle(0, "Secondary ID Card", True)
                    'plhIDCard.Controls.Add(secondaryIDCard)
                End If

            End If

            GenerateUploadDoc(EnableDocUpload, IDPrefix, DocUploadTitle, RequireDocTitle, DocTitleOptions, DocUploadRequired,
                              CType(LoadControl("~/Inc/NewDocCapture.ascx"), Inc_NewDocCapture),
                              CType(LoadControl("~/Inc/DocCaptureSourceSelector.ascx"), Inc_DocCaptureSourceSelector),
                              plhDocUpload, plhDocCaptureSourceSelector)

            Dim additionalInfo As Inc_MainApp_xaApplicantQuestion = CType(LoadControl("~/Inc/MainApp/xaApplicantQuestion.ascx"), Inc_MainApp_xaApplicantQuestion)
            additionalInfo.IDPrefix = IDPrefix
            additionalInfo.Header = HeaderUtils.RenderPageTitle(0, "Additional Information", True)
			If isBusinessLoan Then
				additionalInfo.LoanType = "BL"
			Else
				additionalInfo.LoanType = "XA"
			End If
			additionalInfo.CQLocation = CustomQuestionLocation.ApplicantPage
			additionalInfo.isBusinessLoan = isBusinessLoan
            plhAdditionalInfo.Controls.Add(additionalInfo)

            plhAddress.Visible = False
            If EnableAddress Then
                plhAddress.Visible = True
                Dim addressCtrl As xa_Inc_saAddress = CType(LoadControl("~/xa/Inc/saAddress.ascx"), xa_Inc_saAddress)
                addressCtrl.IsBusinessLoan = isBusinessLoan
                addressCtrl.IDPrefix = IDPrefix
                addressCtrl.IsJoint = IsJoint
                addressCtrl.StateDropdown = StateDropdown
				addressCtrl.EnableOccupancyStatus = EnableOccupancyStatus
				addressCtrl.EnableMailingAddress = EnableMailingAddress
				addressCtrl.ShowFieldOccupancyStatusDefaultStateOn = InbranchEnableOccupancyStatus
				If Not isBusinessLoan Then
                    addressCtrl.EnableHousingPayment = True
                End If
                addressCtrl.EnabledAddressVerification = EnabledAddressVerification
                addressCtrl.OccupyingLocationDropdown = OccupyingLocationDropdown
				addressCtrl.CollectDescriptionIfOccupancyStatusIsOther = CollectDescriptionIfOccupancyStatusIsOther
				addressCtrl.OccupancyStatusesForRequiredHousingPayment = OccupancyStatusesForRequiredHousingPayment
				addressCtrl.LiveYearsDropdown = LiveYearsDropdown
                addressCtrl.LiveMonthsDropdown = LiveMonthsDropdown
                addressCtrl.Header = HeaderUtils.RenderPageTitle(0, "Current Physical Address", True)
				addressCtrl.PreviousAddressHeader = HeaderUtils.RenderPageTitle(0, "Previous Physical Address", True)
				addressCtrl.MappedShowFieldLoanType = MappedShowFieldLoanType
                plhAddress.Controls.Add(addressCtrl)
            End If
            '' if relationshipToPrimaryList exist it will override enableRelationshipToPrimary of all non primary applicants to true
            ''EnableRelationshipToPrimary = False
            ''Dim relationshipToPrimaryList As Dictionary(Of String, String) = CurrentConfig.GetEnumItems("RELATIONSHIP_PRIMARY")
            ''If relationshipToPrimaryList IsNot Nothing AndAlso relationshipToPrimaryList.Any() Then
            ''    EnableRelationshipToPrimary = True
            ''End If
            If RelationToPrimaryApplicantDropdown = "" Then
                ''reset EnableRelationshipToPrimary to false
                EnableRelationshipToPrimary = False
            End If
            plhDeclaration.Visible = False
            plhFinancialInfo.Visible = False
            plhBLReference.Visible = False
            If isBusinessLoan Then
                If EnableBLDeclaration Then
                    'declaration 
                    plhDeclaration.Visible = True
                    Dim declarationCtrl As bl_Inc_Declaration = CType(LoadControl("~/bl/Inc/Declaration.ascx"), bl_Inc_Declaration)
                    declarationCtrl.IDPrefix = IDPrefix
                    declarationCtrl.selectedApplicant = selectedApplicant
                    plhDeclaration.Controls.Add(declarationCtrl)
                End If
                ''financial information 
                plhFinancialInfo.Visible = True
                Dim financialInfoCtrl As Inc_FinancialInfo = CType(LoadControl("~/Inc/MainApp/FinancialInfo.ascx"), Inc_FinancialInfo)
                financialInfoCtrl.IDPrefix = IDPrefix
                financialInfoCtrl.IsBusinessLoan = isBusinessLoan
                financialInfoCtrl.IsBLJoint = IsJoint
                financialInfoCtrl.EmploymentStatusDropdown = EmploymentStatusDropdown
				financialInfoCtrl.CollectDescriptionIfEmploymentStatusIsOther = CollectDescriptionIfEmploymentStatusIsOther
				financialInfoCtrl.OccupancyStatusesForRequiredHousingPayment = OccupancyStatusesForRequiredHousingPayment
				'' financialInfoCtrl.Config = CurrentConfig.GetWebConfigElement
				''financialInfoCtrl.EnableGrossMonthlyIncome = EnableGrossMonthlyIncome
				plhFinancialInfo.Controls.Add(financialInfoCtrl)
                If EnableReferenceInfo Then
                    ''references
                    Dim referenceCtrl As bl_Inc_BLReferenceInfo = CType(LoadControl("~/bl/Inc/BLReferenceInfo.ascx"), bl_Inc_BLReferenceInfo)
                    plhBLReference.Visible = True
                    referenceCtrl.IDPrefix = IDPrefix
					referenceCtrl.MappedShowFieldLoanType = MappedShowFieldLoanType
					referenceCtrl.EnableMailingAddress = EnableMailingAddress
                    referenceCtrl.StateDropdown = StateDropdown
                    plhBLReference.Controls.Add(referenceCtrl)
                End If
            End If
            ''required ownership entity types
            Dim requiredOSTypes = {"ASSN", "CCORP", "CCORPLLC", "CORP", "GEN_PARTNER", "LLC", "LLP", "NONMEMBER", "PARTNER", "PARTNERLLC", "PROFESSIONALCORP", "SCORP", "SCORPLLC", "TRUST"}
            RequiredOwneshipEntityTypes = New JavaScriptSerializer().Serialize(requiredOSTypes)
            ''build rolelist dropdown for business loan and business XA
            RoleListDropdown = buildRolelistDropdown()
        End If
    End Sub
    Public Shared Sub GenerateUploadDoc(ByVal EnableDocUpload As Boolean,
                                        ByVal IDPrefix As String,
                                        ByVal DocUploadTitle As String,
                                        ByVal RequireDocTitle As Boolean,
                                        ByVal DocTitleOptions As List(Of CDocumentTitleInfo),
                                        ByVal DocUploadRequired As Boolean,
                                        ByVal docUploadCtrl As Inc_NewDocCapture,
                                        ByVal docUploadSrcSelector As Inc_DocCaptureSourceSelector,
                                        ByRef plhDocUpload As PlaceHolder,
                                        ByRef plhDocCaptureSourceSelector As PlaceHolder)
        plhDocUpload.Visible = False
        plhDocCaptureSourceSelector.Visible = False
        If EnableDocUpload Then
            plhDocUpload.Visible = True
            'Dim docUploadCtrl As Inc_NewDocCapture = CType(LoadControl("~/Inc/NewDocCapture.ascx"), Inc_NewDocCapture)
            docUploadCtrl.IDPrefix = IDPrefix
            docUploadCtrl.Title = DocUploadTitle
            docUploadCtrl.RequireDocTitle = RequireDocTitle
            docUploadCtrl.DocTitleOptions = DocTitleOptions
            docUploadCtrl.DocUploadRequired = DocUploadRequired
            plhDocUpload.Controls.Add(docUploadCtrl)

            plhDocCaptureSourceSelector.Visible = True
            'Dim docUploadSrcSelector As Inc_DocCaptureSourceSelector = CType(LoadControl("~/Inc/DocCaptureSourceSelector.ascx"), Inc_DocCaptureSourceSelector)
            docUploadSrcSelector.IDPrefix = IDPrefix
            plhDocCaptureSourceSelector.Controls.Add(docUploadSrcSelector)
        End If
    End Sub
    Protected Function callVerifyAddressJSFunction(isZipcodeTextbox As Boolean, Optional ByVal prefixAddress As String = "") As String
        '' verifyAddress js function needs to attach to address + zip + city + state html controls
        If EnabledAddressVerification Then
            Return String.Format("{0}verifyAddress('{1}');", IDPrefix, prefixAddress)
        Else
            '' lookupZipcode js function only needs to attach to zip html control
            If isZipcodeTextbox Then
                Return String.Format("{0}lookupZipcode(this.value,'{1}');", IDPrefix, prefixAddress)
            End If
        End If
        Return String.Empty
    End Function
    Protected Function buildRolelistDropdown() As String
        Dim sRoleListDropDown As String = ""
        Dim RoleList As New Dictionary(Of String, String)
        RoleList.Add("Agent", "Agent")
        RoleList.Add("Authorized Signer", "Authorized Signer")
        RoleList.Add("Corporate Representative", "Corporate Representative")
        RoleList.Add("Director", "Director")
        RoleList.Add("Manager", "Manager")
        RoleList.Add("Member", "Member")
        RoleList.Add("Owner", "Owner")
        RoleList.Add("Partner", "Partner")
        RoleList.Add("President", "President")
        RoleList.Add("Secretary", "Secretary")
        RoleList.Add("Treasurer", "Treasurer")
        RoleList.Add("Vice President", "Vice President")
        Return Common.RenderDropdownlistWithEmpty(RoleList, "", "--Please select--", "")
    End Function
End Class
