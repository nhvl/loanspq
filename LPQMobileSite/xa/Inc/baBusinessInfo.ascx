﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="baBusinessInfo.ascx.vb" Inherits="xa_Inc_baBusinessInfo" %>
<%@ Import Namespace="LPQMobile.Utils" %>
<%@ Reference Control="~/xa/Inc/saAddress.ascx" %>
<div class="ba-business-info-page" data-role="page" id="page<%=IDPrefix%>">
    <div data-role="header" style="display: none">
         <h1>Open an Account</h1>
    </div>
    <div data-role="content">
	    <%If Not String.IsNullOrWhiteSpace(Availability) AndAlso Availability.StartsWith("2") Then%>
            <%=HeaderUtils.RenderLogoAndSteps(LogoUrl, 1, "OPEN")%>
        <%Else%>
			<%=HeaderUtils.RenderLogoAndSteps(LogoUrl, 1, "JOIN")%>	
        <%End If%>      
        <%=HeaderUtils.RenderPageTitle(21, "Tell Us About The Business", False)%>
		<%=HeaderUtils.RenderPageTitle(0, "Business Information", True)%>
		<div data-role="fieldcontain">
			<label for="<%=IDPrefix%>txtCompanyName" class="RequiredIcon">Company Name</label>
			<input type="text" id="<%=IDPrefix%>txtCompanyName" maxlength ="50"/>
		</div>
		<div data-role="fieldcontain">
			<div class="row">
				<div class="col-xs-8 no-padding"><label for="<%=IDPrefix%>txtTaxID1" id="<%=IDPrefix%>lblTaxID" class="RequiredIcon">Business Tax ID</label></div>
				<div class="col-xs-4 no-padding ui-label-tax">
					<label class="pull-right header_theme2 taxid-ico" style="padding-left: 4px; padding-right: 2px; display: inline;"><%=HeaderUtils.IconLock16%></label>
					<label id="<%=IDPrefix%>spOpenTaxIDSastisfy" style="display: inline;" onclick='openPopup("#<%=IDPrefix%>popTaxIDSastisfy")' class="pull-right focus-able" data-rel='popup' data-position-to="window" data-transition='pop'><span class="pull-right header_theme2  taxid-ico"><%=HeaderUtils.IconQuestion16%></span></label>
					<div style="display: inline;" class="hidden"><label is_show="1" class="pull-right header_theme2 rename-able shadow-btn focus-able" style="margin-right: 10px; cursor: pointer;outline: none;" onclick="<%=IDPrefix%>toggleTaxIDText(this);">Hide</label></div>
					<div style="display: inline;"><label is_show="0" class="pull-right header_theme2 rename-able shadow-btn focus-able" style="margin-right: 10px; cursor: pointer;outline: none;" onclick="<%=IDPrefix%>toggleTaxIDText(this);">Show</label></div>
					<div class="clearfix"></div>
				</div>
			</div>
			<div id="<%=IDPrefix%>divTaxID" class="ui-input-taxid row">
				<div class="col-xs-4">
					<input type="password" aria-labelledby="<%=IDPrefix%>lblTaxID" pattern="[0-9]*" placeholder="--" id="<%=IDPrefix%>txtTaxID1" autocomplete="new-password" maxlength='2' next-input="#<%=IDPrefix%>txtTaxID2" onkeydown="limitToNumeric(event);" onkeyup="onKeyUp(event,'#<%=IDPrefix%>txtTaxID1','#<%=IDPrefix%>txtTaxID2', '2');"/>
					<input type="hidden" id="<%=IDPrefix%>hdTaxID1" value=""/>
				</div>
				<div class="col-xs-8">
					<input type="password" aria-labelledby="<%=IDPrefix%>lblTaxID" pattern="[0-9]*" placeholder="-------" id="<%=IDPrefix%>txtTaxID2" autocomplete="new-password" maxlength='7' onkeydown="limitToNumeric(event);"/>
					<input type="hidden" id="<%=IDPrefix%>hdTaxID2" value=""/>
					<input type="hidden" id="<%=IDPrefix%>txtTaxID" class="combine-field-value"/>
				</div>
			</div>
		</div>
		<div data-role="fieldcontain">
			<label for="<%=IDPrefix%>ddlIndustry" class="RequiredIcon">Industry</label>
			<select id="<%=IDPrefix%>ddlIndustry">
				<%=BusinessIndustryCodesDropdown%>
				<%--<%= Common.RenderDropdownlistWithEmpty(BusinessIndustryCodes, " ", "--Please Select--")%>--%>
			</select>
		</div>
		<div id="<%=IDPrefix%>popTaxIDSastisfy" data-role="popup" style="max-width: 400px;">
			<div data-role="content">
				<div class="row">
					<div class="col-xs-12 header_theme2">
						<a data-rel="back" href="#" class="pull-right svg-btn"><%=HeaderUtils.IconClose %><span style="display: none;">close</span></a>
					</div>
				</div>
				<div class="row">
					<div class="col-sm-12">
						<div style="margin: 10px 0;">
							Your Business Tax ID is used for identification purposes.
						</div>
					</div>    
				</div>
			</div>
		</div>
		<div data-role="fieldcontain">
			<div class="row">
				<div class="col-xs-8 no-padding">
					<label for="<%=IDPrefix%>txtEstablishDate" id="<%=IDPrefix%>lblEstablishDate" class="dob-label RequiredIcon">Establish Date</label>
				</div>
				<div class="col-xs-4 no-padding"><input type="hidden" id="<%=IDPrefix%>txtEstablishDate" class="combine-field-value" /></div>
			</div>
			<div id="<%=IDPrefix%>divEstablishDate" class="ui-input-date id-date-establish row">
				<div class="col-xs-4">
					<input aria-labelledby="<%=IDPrefix%>lblEstablishDate" type="<%=TextAndroidTel%>" pattern="[0-9]*" placeholder="mm" id="<%=IDPrefix%>txtEstablishDate1" maxlength ='2' onkeydown="limitToNumeric(event);" onkeyup="onKeyUp(event, '#<%=IDPrefix%>txtEstablishDate1','#<%=IDPrefix%>txtEstablishDate2', '2');" />
				</div>
				<div class="col-xs-4">
					<input aria-labelledby="<%=IDPrefix%>lblEstablishDate" type="<%=TextAndroidTel%>" pattern="[0-9]*" placeholder="dd" id="<%=IDPrefix%>txtEstablishDate2" maxlength ='2' onkeydown="limitToNumeric(event);" onkeyup="onKeyUp(event, '#<%=IDPrefix%>txtEstablishDate2','#<%=IDPrefix%>txtEstablishDate3', '2');" />
				</div>
				<div class="col-xs-4" style="padding-right: 0;">
					<input aria-labelledby="<%=IDPrefix%>lblEstablishDate" type="<%=TextAndroidTel%>" pattern="[0-9]*" placeholder="yyyy" id="<%=IDPrefix%>txtEstablishDate3" maxlength ='4' onkeydown="limitToNumeric(event);"/>
				</div>
			</div>
		</div>
		<div data-role="fieldcontain">
			<label for="<%=IDPrefix%>txtBusinessDescription">Business Description</label>
			<textarea id="<%=IDPrefix%>txtBusinessDescription" rows="3"></textarea>
		</div>
		<div data-role="fieldcontain">
			<label for="<%=IDPrefix%>ddlStateRegistered">State Registered</label>
			<select id="<%=IDPrefix%>ddlStateRegistered">
				<%=StateDropdown%>
			</select>
		</div>
		<div data-role="fieldcontain">
			<label for="<%=IDPrefix%>txtAnnualRevenue">Annual Revenue</label>
			<input type="<%=TextAndroidTel%>" pattern="[0-9]*" id="<%=IDPrefix%>txtAnnualRevenue" class="money" maxlength ="10" />
		</div>
		<div data-role="fieldcontain">
			<label for="<%=IDPrefix%>txtNumberOfEmployees">Number Of Employees</label>
			<input type="<%=TextAndroidTel%>" pattern="[0-9]*" id="<%=IDPrefix%>txtNumberOfEmployees" class="numeric" maxlength ="5" />
		</div>
		<div data-role="fieldcontain">
			<label for="<%=IDPrefix%>txtPrimaryBank">Primary Bank</label>
			<input type="text" id="<%=IDPrefix%>txtPrimaryBank" maxlength ="200"/>
		</div>
		<%If EnableDoingBusinessAs Then%>
		<div data-role="fieldcontain">
			<label for="<%=IDPrefix%>ddlDoingBusinessAs">Doing Business as another Company?</label>
			<select id="<%=IDPrefix%>ddlDoingBusinessAs">
				<option value="No" selected="selected">No</option>
				<option value="Yes">Yes</option>
			</select>
		</div>
		<%End If%>
		<iframe name="<%=IDPrefix%>if_ContactInfo" style="display: none;" src="about:blank" ></iframe>
		<form target="<%=IDPrefix%>if_ContactInfo" action="/handler/AutoFillHandler.aspx" id="<%=IDPrefix%>frm_ContactInfo">
		<%=HeaderUtils.RenderPageTitle(22, "Contact Information", True)%>
		<div data-role="fieldcontain">
			<label id="<%=IDPrefix%>lblEmail" for="<%=IDPrefix%>txtEmail">Email</label>
			<input type="email" name="email" autocomplete="email" id="<%=IDPrefix%>txtEmail" maxlength ="50"/>
		</div>
       <div data-role="fieldcontain" id="<%=IDPrefix%>divHomePhone">
	       <div>
		       <div style="display: inline-block;"><label id="<%=IDPrefix%>lblHomePhone" for="<%=IDPrefix%>txtHomePhone" class="RequiredIcon" style="white-space: nowrap;">Phone</label></div>
			   <div style="display: inline-block;"><label class="phone_format rename-able" style="font-style: italic; white-space: nowrap">(xxx) xxx-xxxx</label></div>
	       </div>
			<input type="<%=TextAndroidTel%>" pattern="[0-9]*" autocomplete="tel" name="phone" id="<%=IDPrefix%>txtHomePhone" class="inphone" maxlength='14'/>
		</div>
		<div data-role="fieldcontain" id="<%=IDPrefix%>divMobilePhone">
			<div>
				<div style="display: inline-block;"><label id="<%=IDPrefix%>lblMobilePhone" for="<%=IDPrefix%>txtMobilePhone" style="white-space: nowrap;">Cell Phone</label></div>
				<div style="display: inline-block;"><label class="phone_format rename-able" style="font-style: italic; white-space: nowrap">(xxx) xxx-xxxx</label></div>
			</div>
			<input type= "<%=TextAndroidTel%>" pattern="[0-9]*" autocomplete="tel" name="mobile" id="<%=IDPrefix%>txtMobilePhone" class="inphone" maxlength='14'/>
		</div>		
		<div data-role="fieldcontain" id="<%=IDPrefix%>divFax">
			<div>
				<div style="display: inline-block;"><label id="<%=IDPrefix%>lblFax" for="<%=IDPrefix%>txtFax" style="white-space: nowrap;">Fax</label></div>
				<div style="display: inline-block;"><label class="phone_format rename-able" style="font-style: italic; white-space: nowrap">(xxx) xxx-xxxx</label></div>
			</div>
			<input type= "<%=TextAndroidTel%>" pattern="[0-9]*" autocomplete="tel" name="mobile" id="<%=IDPrefix%>txtFax" class="inphone" maxlength='14'/>
		</div>
		</form>
		<asp:PlaceHolder runat="server" ID="plhAddress"></asp:PlaceHolder>
	 </div>
    <div class ="div-continue"  data-role="footer">
        <a href="#" data-transition="slide" type="button" class="div-continue-button" onclick="<%=IDPrefix%>ValidateBAAccountInfo()">Continue</a> 
        <a href="#divErrorDialog" style="display: none;">no text</a>
        <span> Or</span>  <a href="#" onclick="goToNextPage('#<%=PreviousPage %>');" class ="div-goback" data-corners="false" data-shadow="false" data-theme="reset"><span class="hover-goback"> Go Back</span></a>
    </div>
</div>
<input type="hidden" id="<%=IDPrefix%>hdHomePhoneCountry" />
<input type="hidden" id="<%=IDPrefix%>hdMobilePhoneCountry" />
<input type="hidden" id="<%=IDPrefix%>hdFaxCountry" />
<script type="text/javascript">
    $(function () {   
		if (isMobile.any() && !isMobile.Windows()) {//moz-text-security does not works on window devices
            $("#<%=IDPrefix%>txtTaxID1,#<%=IDPrefix%>txtTaxID2")
                .attr("type", "tel")
                .addClass("mask-password");
        } else {
            $("#<%=IDPrefix%>txtTaxID1,#<%=IDPrefix%>txtTaxID2")
                .attr("type", "password");
        }
        //remove Live with parents and Government quarter options (not applicable to a business address)
        $('#<%=IDPrefix%>ddlOccupyingStatus').find('option[value="LIVE WITH PARENTS"]').remove();
        $('#<%=IDPrefix%>ddlOccupyingStatus').find('option[value="GOVERNMENT QUARTERS"]').remove();

		<%=IDPrefix%>initDatePicker("#<%=IDPrefix%>txtEstablishDate");
		$('.id-date-establish input', "#page<%=IDPrefix%>").on('focusout', function () {
			var maxlength = parseInt($(this).attr('maxlength'));
			// add 0 if value < 10
			$(this).val(padLeft($(this).val(), maxlength));
			<%=IDPrefix%>updateHiddenEstablishDate();
		});
		$('.ui-input-taxid input', "#page<%=IDPrefix%>").on('focusout', function () {
			<%=IDPrefix%>updateHiddenTaxID();
		});

        if (is_foreign_phone) {
            // international country code
            //by default it display separateDialCode dropdown
            //don't want to display separateDialCode dropdown, just set separateDialCode: false
            $("#<%=IDPrefix%>txtHomePhone").intlTelInput({
                separateDialCode: false
            }); // don't know why it's not set default for this one
            $('#<%=IDPrefix%>txtMobilePhone').intlTelInput({
                separateDialCode: false
            });
            $('#<%=IDPrefix%>txtFax').intlTelInput({
                separateDialCode: false
            });

            // change the text
            $('.phone_format', "#page<%=IDPrefix%>").parent().addClass("hidden");

            // remove mask
            if (!isMobile.Android()) {
                $('.inphone', "#page<%=IDPrefix%>").inputmask('remove');
            } else {
                $('input.inphone', "#page<%=IDPrefix%>").off('blur').off('keyup').off('keydown');
            }
            $('input.inphone', "#page<%=IDPrefix%>").numeric();

            //set hidden phone country to "us" by default
            $("#<%=IDPrefix%>hdHomePhoneCountry").val("us");
            $("#<%=IDPrefix%>hdMobilePhoneCountry").val("us");
            $("#<%=IDPrefix%>hdFaxCountry").val("us");
        } 
		//update hidden Phone Country
		$('#<%=IDPrefix%>divHomePhone').on('click', '.flag-container', function () {
			<%=IDPrefix%>updateHiddenPhoneCountry($(this), $('#<%=IDPrefix%>hdHomePhoneCountry'));
		});
		$('#<%=IDPrefix%>divMobilePhone').on('click', '.flag-container', function () {
			<%=IDPrefix%>updateHiddenPhoneCountry($(this), $('#<%=IDPrefix%>hdMobilePhoneCountry'));
		});
		$('#<%=IDPrefix%>divFax').on('click', '.flag-container', function () {
			<%=IDPrefix%>updateHiddenPhoneCountry($(this), $('#<%=IDPrefix%>hdFaxCountry'));
		});

	});
	$("#<%=IDPrefix%>popTaxIDSastisfy").on("popupafteropen", function (event, ui) {
		$("#" + this.id + "-screen").height("");
	});
	$("#<%=IDPrefix%>popTaxIDSastisfy").on("popupafterclose", function (event, ui) {
		setTimeout(function () {
			$("#<%=IDPrefix%>spOpenTaxIDSastisfy").focus();
		}, 100);
	});
	function <%=IDPrefix%>updateHiddenPhoneCountry(currElem, hdPhoneCountryElem) {
		if (currElem.find('li[class~="active"]')) {
			hdPhoneCountryElem.val(currElem.find('li[class~="active"]').attr('data-country-code'));
		}
	}
	function <%=IDPrefix%>updateHiddenTaxID() {
		if ($("#<%=IDPrefix%>txtTaxID1").val().length == 2) {
			var taxId = $("#<%=IDPrefix%>txtTaxID1").val() + '-' + $("#<%=IDPrefix%>txtTaxID2").val();
			$("#<%=IDPrefix%>txtTaxID").val(taxId);
		}
	}
	function <%=IDPrefix%>toggleTaxIDText(btn) {
		var $btn = $(btn);
		if ($btn.attr("contenteditable") == "true") return false;
		if ($btn.attr('is_show') == '0') {
			$btn.parent().addClass("hidden");
			$btn.closest("div.ui-label-tax").find("label[is_show='1']").parent().removeClass("hidden");
			if (isMobile.any() && !isMobile.Windows()) { //moz-text-security does not works on window devices
				var taxId1 = "";
				var taxId2 = "";
				var matchs = /^(\w*)-(\w*)$/i.exec($("#<%=IDPrefix%>" + "txtTaxID").val());
				if (typeof matchs != "undefined" && matchs != null) {
					taxId1 = matchs[1];
					taxId2 = matchs[2];
				}
				$("#<%=IDPrefix%>" + "txtTaxID1").removeClass("mask-password").val("").val(taxId1); //keep .val("") to prevent bug on mobile devices
				$("#<%=IDPrefix%>" + "txtTaxID2").removeClass("mask-password").val("").val(taxId2);
			} else {
				$("#<%=IDPrefix%>" + "txtTaxID1").attr('type', 'text');
				$("#<%=IDPrefix%>" + "txtTaxID2").attr('type', 'text');
			}
		} else {
			$btn.parent().addClass("hidden");
			$btn.closest("div.ui-label-tax").find("label[is_show='0']").parent().removeClass("hidden");
			if (isMobile.any() && !isMobile.Windows()) { //moz-text-security does not works on window devices
				$("#<%=IDPrefix%>" + "txtTaxID1").addClass("mask-password");
				$("#<%=IDPrefix%>" + "txtTaxID2").addClass("mask-password");
			} else {					 
				$("#<%=IDPrefix%>" + "txtTaxID1").attr('type', 'password');
				$("#<%=IDPrefix%>" + "txtTaxID2").attr('type', 'password');
			}
		}
	}
	function <%=IDPrefix%>updateHiddenEstablishDate() {
		var month = padLeft($("#<%=IDPrefix%>txtEstablishDate1").val(), 2);
		var day = padLeft($("#<%=IDPrefix%>txtEstablishDate2").val(), 2);
		var year = padLeft($("#<%=IDPrefix%>txtEstablishDate3").val(), 4);
		if (month != "" && day != "" && year != "") {
			$("#<%=IDPrefix%>txtEstablishDate").val(month + '/' + day + '/' + year);
		} else {
			$("#<%=IDPrefix%>txtEstablishDate").val("");
		}
	}
	function <%=IDPrefix%>initDatePicker(controlId) {
		$(controlId).datepicker({
			showOn: "button",
			changeMonth: true,
			changeYear: true,
			yearRange: "-50:+50",
			buttonImage: "../images/calendar.png",
			buttonImageOnly: true,
			onSelect: function (value, inst) {
				var date = $(this).datepicker('getDate');

				$(controlId + "2").val(padLeft(date.getDate(), 2));
				$(controlId + "1").val(padLeft(date.getMonth() + 1, 2));
				$(controlId + "3").val(padLeft(date.getFullYear(), 4));

				$(controlId).datepicker("hide");
				$(controlId + "1").trigger("focus").trigger("blur");
			}
		});
	}

	function <%=IDPrefix%>registerBaAccountInfoValidator() {
		$('#<%=IDPrefix%>txtCompanyName').observer({
			validators: [
				function (partial) {
					if (Common.ValidateText($(this).val()) == false) {
						return "Company Name is required";
					}
					return "";
				}
			],
			validateOnBlur: true,
			group: "<%=IDPrefix%>ValidateBAAccountInfo"
		});
		$('#<%=IDPrefix%>ddlIndustry').observer({
			validators: [
				function (partial) {
					if (Common.ValidateText($(this).val()) == false) {
						return "Industry is required";
					}
					return "";
				}
			],
			validateOnBlur: true,
			group: "<%=IDPrefix%>ValidateBAAccountInfo"
		});
		$('#<%=IDPrefix%>divEstablishDate').observer({
			validators: [
				function (partial) {
					<%=IDPrefix%>updateHiddenEstablishDate();
					var $txtEstablishDate = $('#<%=IDPrefix%>txtEstablishDate');
					var establishDate = moment($txtEstablishDate.val(), "MM-DD-YYYY");
					if ($txtEstablishDate.val() == "") {
						return "Establish Date is required";
					} else if (!Common.IsValidDate($txtEstablishDate.val())) {
						return "Valid Establish Date is required";
					} else if (establishDate.isAfter(moment())) {
						return "Establish Date must be earlier than current date";
					}
					return "";
				}
			],
			validateOnBlur: true,
			container: '#<%=IDPrefix%>divEstablishDate',
			group: "<%=IDPrefix%>ValidateBAAccountInfo"
		});
		$('#<%=IDPrefix%>divTaxID').observer({
			validators: [
				function (partial, evt) {
					<%=IDPrefix%>updateHiddenTaxID();
					var taxId = $('#<%=IDPrefix%>txtTaxID1').val() + $('#<%=IDPrefix%>txtTaxID2').val();
					if (/^[0-9]{9}$/.test(taxId) == false) {
						return 'Valid Business Tax ID is required';
					}
					return "";
				}
			],
			validateOnBlur: true,
			container: '#<%=IDPrefix%>divTaxID',
			group: "<%=IDPrefix%>ValidateBAAccountInfo"
		});
		$('#<%=IDPrefix%>txtAnnualRevenue').observer({
			validators: [
				function (partial) {
					var value = $(this).val();
					if ($.trim(value) !== "" && Common.ValidateTextAllowZero(value) == false) {
						return "Valid Annual Revenue is required";
					}
					return "";
				}
			],
			validateOnBlur: true,
			group: "<%=IDPrefix%>ValidateBAAccountInfo"
		});
		$('#<%=IDPrefix%>txtNumberOfEmployees').observer({
			validators: [
				function (partial) {
					var value = $(this).val();
					if ($.trim(value) !== "" && /^[0-9]+$/.test(value) == false) {
						return "Valid number of employees is required";
					}
					return "";
				}
			],
			validateOnBlur: true,
			group: "<%=IDPrefix%>ValidateBAAccountInfo"
		});
		$('#<%=IDPrefix%>txtEmail').observer({
			validators: [
				function (partial) {
					var email = $(this).val();
					if ($.trim(email) !== "" && Common.ValidateEmail(email) == false) {
						return "Valid Email is required";
					}
					return "";
				}
			],
			validateOnBlur: true,
			group: "<%=IDPrefix%>ValidateBAAccountInfo"
		});
		$('#<%=IDPrefix%>txtHomePhone').observer({
			validators: [
				function (partial) {
					var homephone = $(this).val();
					if (!Common.ValidateText(homephone)) {
						return "Valid phone number is required";
					}
					if (is_foreign_phone) {
						return validateForeignPhone('<%=IDPrefix%>txtHomePhone');
					} else {
						if (!Common.ValidatePhone(homephone)) {
							return "Valid phone number is required";
						} else {
							//validate first digit of home phone : 0 and 1 is invalid
							var firstDigit = homephone.replace("(", "").substring(0, 1);
							if (firstDigit == 0 || firstDigit == 1) {
								return 'Phone number and area code can not begin with 0 or 1 and must be a valid phone number';
							}
						}
					}
					return "";
				}
			],
			validateOnBlur: true,
			group: "<%=IDPrefix%>ValidateBAAccountInfo"
		});
		$('#<%=IDPrefix%>txtMobilePhone').observer({
			validators: [
				function (partial) {
					var mobile = $(this).val();
					if (Common.ValidateText(mobile)) {
						if (is_foreign_phone) {
							return validateForeignPhone('<%=IDPrefix%>txtMobilePhone');
						} else {
							if (!Common.ValidatePhone(mobile)) {
								return "Valid phone number is required";
							} else {
								//validate first digit of home phone : 0 and 1 is invalid
								var firstDigit = mobile.replace("(", "").substring(0, 1);
								if (firstDigit == 0 || firstDigit == 1) {
									return 'Phone number and area code can not begin with 0 or 1 and must be a valid phone number';
								}
							}
						}
					}
					return "";
				}
			],
			validateOnBlur: true,
			group: "<%=IDPrefix%>ValidateBAAccountInfo"
		});
		$('#<%=IDPrefix%>txtFax').observer({
			validators: [
				function (partial) {
					var fax = $(this).val();
					if (Common.ValidateText(fax)) {
						if (is_foreign_phone) {
							return validateForeignPhone('<%=IDPrefix%>txtFax');
						} else {
							if (!Common.ValidatePhone(fax)) {
								return "Valid fax number is required";
							} else {
								//validate first digit of home phone : 0 and 1 is invalid
								var firstDigit = fax.replace("(", "").substring(0, 1);
								if (firstDigit == 0 || firstDigit == 1) {
									return 'Fax number and area code can not begin with 0 or 1 and must be a valid fax number';
								}
							}
						}
					}
					return "";
				}
			],
			validateOnBlur: true,
			group: "<%=IDPrefix%>ValidateBAAccountInfo"
		});
	}
	<%=IDPrefix%>registerBaAccountInfoValidator();

	function <%=IDPrefix%>SetBAAccountInfo(appInfo) {
		//var appInfo = {};
		appInfo.<%=IDPrefix%>CompanyName = $('#<%=IDPrefix%>txtCompanyName').val().trim();
		appInfo.<%=IDPrefix%>EstablishDate = $('#<%=IDPrefix%>txtEstablishDate').val();
		appInfo.<%=IDPrefix%>TaxID = $('#<%=IDPrefix%>txtTaxID').val().replace(new RegExp("-", "g"), '');
		appInfo.<%=IDPrefix%>Industry = $('#<%=IDPrefix%>ddlIndustry').val();
		appInfo.<%=IDPrefix%>StateRegistered = $('#<%=IDPrefix%>ddlStateRegistered').val();
		appInfo.<%=IDPrefix%>AnnualRevenue = Common.GetFloatFromMoney($('#<%=IDPrefix%>txtAnnualRevenue').val());
		appInfo.<%=IDPrefix%>NumberOfEmployees = $('#<%=IDPrefix%>txtNumberOfEmployees').val();
		appInfo.<%=IDPrefix%>PrimaryBank = $('#<%=IDPrefix%>txtPrimaryBank').val();
		if ($("#<%=IDPrefix%>ddlDoingBusinessAs").length == 1) {
			appInfo.<%=IDPrefix%>DoingBusinessAs = $('#<%=IDPrefix%>ddlDoingBusinessAs').val();
		} 
		appInfo.<%=IDPrefix%>BusinessDescription = $('#<%=IDPrefix%>txtBusinessDescription').val();
		var homePhoneCountry = $('#<%=IDPrefix%>hdHomePhoneCountry').val();
		var faxCountry = $('#<%=IDPrefix%>hdFaxCountry').val();
		var mobilePhoneCountry = $('#<%=IDPrefix%>hdMobilePhoneCountry').val();
		if (!is_foreign_phone) {
			faxCountry = "";
			homePhoneCountry = "";
			mobilePhoneCountry = "";
		}
		appInfo.<%=IDPrefix%>EmailAddr = $('#<%=IDPrefix%>txtEmail').val().trim();
		appInfo.<%=IDPrefix%>HomePhone = $('#<%=IDPrefix%>txtHomePhone').val();
		appInfo.<%=IDPrefix%>HomePhoneCountry = homePhoneCountry;
		appInfo.<%=IDPrefix%>MobilePhone = $('#<%=IDPrefix%>txtMobilePhone').val();
		appInfo.<%=IDPrefix%>MobilePhoneCountry = mobilePhoneCountry;
		appInfo.<%=IDPrefix%>Fax = $('#<%=IDPrefix%>txtFax').val();
		appInfo.<%=IDPrefix%>FaxCountry = faxCountry;
		<%=IDPrefix%>SetSAAddress(appInfo);
		//return appInfo;
	}
	function <%=IDPrefix%>ValidateBAAccountInfo() {
		UpdateCSRFNumber(hfCSRF);
		var validator = true;
		if ($.lpqValidate("<%=IDPrefix%>ValidateBAAccountInfo") == false) {
			validator = false;
		}
		if (<%=IDPrefix%>ValidateSAAddress() == false) {
			validator = false;
		}
		if (validator) {
			goToNextPage("#<%=NextPage%>");
		} else {
			Common.ScrollToError();
		}
		return validator;
	}
	function <%=IDPrefix%>ViewBAAccountInfo() {
		var strHtml = "";
		var companyName = htmlEncode($.trim($('#<%=IDPrefix%>txtCompanyName').val()));
		var establishDate = htmlEncode($.trim($('#<%=IDPrefix%>txtEstablishDate').val()));
		var taxId = htmlEncode($('#<%=IDPrefix%>txtTaxID').val().replace(new RegExp("-", "g"), ''));
		var email = htmlEncode($.trim($('#<%=IDPrefix%>txtEmail').val()));
		var mobilePhone = htmlEncode($.trim($('#<%=IDPrefix%>txtMobilePhone').val()));
		var homePhone = htmlEncode($.trim($('#<%=IDPrefix%>txtHomePhone').val()));
		var fax = htmlEncode($.trim($('#<%=IDPrefix%>txtFax').val()));
		var industry = $('#<%=IDPrefix%>ddlIndustry option:selected').text();
		var businessDescription = $('#<%=IDPrefix%>txtBusinessDescription').val();
		var stateRegistered = $('#<%=IDPrefix%>ddlStateRegistered option:selected').text();
		var annualRevenue = $('#<%=IDPrefix%>txtAnnualRevenue').val();
		var numberOfEmployees = $('#<%=IDPrefix%>txtNumberOfEmployees').val();
		var primaryBank = $('#<%=IDPrefix%>txtPrimaryBank').val();
		var doingBusinessAs = $('#<%=IDPrefix%>ddlDoingBusinessAs option:selected').text();
		if (companyName !== "") {
			strHtml += '<div class="col-sm-6 col-xs-12"><div class="row"><div class="col-xs-6 text-right row-title"><span class="bold">Company Name</span></div><div class="col-xs-6 text-left row-data"><span>' + companyName + '</span></div></div></div>';
		}
		if (taxId !== "") {
			strHtml += '<div class="col-sm-6 col-xs-12"><div class="row"><div class="col-xs-6 text-right row-title"><span class="bold">Business Tax ID</span></div><div class="col-xs-6 text-left row-data"><span>' + taxId.replace(/^\d{5}/, "*****") + '</span></div></div></div>';
		}
		if (industry !== "") {
			strHtml += '<div class="col-sm-6 col-xs-12"><div class="row"><div class="col-xs-6 text-right row-title"><span class="bold">Industry</span></div><div class="col-xs-6 text-left row-data"><span>' + industry + '</span></div></div></div>';
		}
		if (establishDate !== "") {
			strHtml += '<div class="col-sm-6 col-xs-12"><div class="row"><div class="col-xs-6 text-right row-title"><span class="bold">Establish Date</span></div><div class="col-xs-6 text-left row-data"><span>' + establishDate + '</span></div></div></div>';
		}
		if (businessDescription !== "") {
			strHtml += '<div class="col-sm-6 col-xs-12"><div class="row"><div class="col-xs-6 text-right row-title"><span class="bold">Business Description</span></div><div class="col-xs-6 text-left row-data"><span>' + businessDescription + '</span></div></div></div>';
		}
		if ($('#<%=IDPrefix%>ddlStateRegistered').val() !== "") {
			strHtml += '<div class="col-sm-6 col-xs-12"><div class="row"><div class="col-xs-6 text-right row-title"><span class="bold">State Registered</span></div><div class="col-xs-6 text-left row-data"><span>' + $('#<%=IDPrefix%>ddlStateRegistered option:selected').text() + '</span></div></div></div>';
		}
		if (annualRevenue !== "") {
			strHtml += '<div class="col-sm-6 col-xs-12"><div class="row"><div class="col-xs-6 text-right row-title"><span class="bold">Annual Revenue</span></div><div class="col-xs-6 text-left row-data"><span>' + Common.FormatCurrency(annualRevenue, true) + '</span></div></div></div>';
		}
		if (numberOfEmployees !== "") {
			strHtml += '<div class="col-sm-6 col-xs-12"><div class="row"><div class="col-xs-6 text-right row-title"><span class="bold">Number Of Employees</span></div><div class="col-xs-6 text-left row-data"><span>' + numberOfEmployees + '</span></div></div></div>';
		}
		if (primaryBank !== "") {
			strHtml += '<div class="col-sm-6 col-xs-12"><div class="row"><div class="col-xs-6 text-right row-title"><span class="bold">Primary Bank</span></div><div class="col-xs-6 text-left row-data"><span>' + primaryBank + '</span></div></div></div>';
		}
		if ($('#<%=IDPrefix%>ddlDoingBusinessAs option:selected').length == 1) {
			strHtml += '<div class="col-sm-6 col-xs-12"><div class="row"><div class="col-xs-6 text-right row-title"><span class="bold">Doing Business as another Company</span></div><div class="col-xs-6 text-left row-data"><span>' + doingBusinessAs + '</span></div></div></div>';
		}
		if (email !== "") {
			strHtml += '<div class="col-sm-6 col-xs-12"><div class="row"><div class="col-xs-6 text-right row-title"><span class="bold">Email</span></div><div class="col-xs-6 text-left row-data"><span>' + email + '</span></div></div></div>';
        }
        if (homePhone !== "") {
            strHtml += '<div class="col-sm-6 col-xs-12"><div class="row"><div class="col-xs-6 text-right row-title"><span class="bold">Phone</span></div><div class="col-xs-6 text-left row-data"><span>' + homePhone + '</span></div></div></div>';
        }
		if (mobilePhone !== "") {
			strHtml += '<div class="col-sm-6 col-xs-12"><div class="row"><div class="col-xs-6 text-right row-title"><span class="bold">Mobile Phone</span></div><div class="col-xs-6 text-left row-data"><span>' + mobilePhone + '</span></div></div></div>';
		}		
		if (fax !== "") {
			strHtml += '<div class="col-sm-6 col-xs-12"><div class="row"><div class="col-xs-6 text-right row-title"><span class="bold">Fax</span></div><div class="col-xs-6 text-left row-data"><span>' + fax + '</span></div></div></div>';
		}
		strHtml += <%=IDPrefix%>ViewAddress();
		return strHtml;
    }
    function autofill_<%=IDPrefix%>() {
        $('#<%=IDPrefix%>txtCompanyName').val("BXA Company");
        $('#<%=IDPrefix%>txtEstablishDate1').val('11');
        $('#<%=IDPrefix%>txtEstablishDate2').val('11');
        $('#<%=IDPrefix%>txtEstablishDate3').val('2000');
        <%=IDPrefix%>updateHiddenEstablishDate();
        $('#<%=IDPrefix%>txtTaxID1').val('12');
        $('#<%=IDPrefix%>txtTaxID2').val('3456789');
        <%=IDPrefix%>updateHiddenTaxID();
		$('#<%=IDPrefix%>txtEmail').val('bxa@test.com');
		$('#<%=IDPrefix%>txtMobilePhone').val('7142341234');
		$('#<%=IDPrefix%>txtHomePhone').val('7147772222');
		$('#<%=IDPrefix%>txtFax').val();
        $('#<%=IDPrefix%>ddlIndustry').val("230000");
        $('#<%=IDPrefix%>ddlIndustry').selectmenu().selectmenu("refresh");
		$('#<%=IDPrefix%>txtBusinessDescription').val("Remodel small retail stores such as restaurant, liquor stores ");
        $('#<%=IDPrefix%>ddlStateRegistered').val("CA");
        $('#<%=IDPrefix%>ddlStateRegistered').selectmenu().selectmenu("refresh");
		$('#<%=IDPrefix%>txtAnnualRevenue').val("30000");
		$('#<%=IDPrefix%>txtNumberOfEmployees').val("20");
        $('#<%=IDPrefix%>txtPrimaryBank').val("Bank of America");
        if (typeof <%=IDPrefix%>autoFillData_BusinessAddress != "undefined") {
            <%=IDPrefix%>autoFillData_BusinessAddress();
        }      
    }
</script>