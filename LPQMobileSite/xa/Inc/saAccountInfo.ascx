﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="saAccountInfo.ascx.vb" Inherits="xa_Inc_saAccountInfo" %>
<%@ Reference Control="~/xa/Inc/saAddress.ascx" %>
<div class="sa-app-info-page" data-role="page" id="page<%=IDPrefix%>">
    <div data-role="header" style="display: none">
         <h1>Open an Account</h1>
    </div>
    <div data-role="content">
	    <%If Not String.IsNullOrWhiteSpace(Availability) AndAlso Availability.StartsWith("2") Then%>
            <%=HeaderUtils.RenderLogoAndSteps(LogoUrl, 1, "OPEN")%>
        <%Else%>
			<%=HeaderUtils.RenderLogoAndSteps(LogoUrl, 1, "JOIN")%>	
        <%End If%>        
        <%=HeaderUtils.RenderPageTitle(21, "Tell Us About The Account", False)%>
		<%=HeaderUtils.RenderPageTitle(0, "Account Information", True)%>
		<div data-role="fieldcontain">
			<label for="<%=IDPrefix%>txtAccountName" class="RequiredIcon">Account Name</label>
			<input type="text" id="<%=IDPrefix%>txtAccountName" name="menbernum" maxlength ="50"/>
		</div>
		<div data-role="fieldcontain">
			<div class="row">
				<div class="col-xs-8 no-padding"><label for="<%=IDPrefix%>txtEstablishDate" id="<%=IDPrefix%>lblEstablishDate" class="dob-label RequiredIcon">Establish Date</label></div>
				<div class="col-xs-4 no-padding"><input type="hidden" id="<%=IDPrefix%>txtEstablishDate" class="combine-field-value" /></div>
			</div>
			<div id="<%=IDPrefix%>divEstablishDate" class="ui-input-date id-date-establish row">
				<div class="col-xs-4">
					<input aria-labelledby="<%=IDPrefix%>lblEstablishDate" type="<%=TextAndroidTel%>" pattern="[0-9]*" placeholder="mm" id="<%=IDPrefix%>txtEstablishDate1" maxlength ='2' onkeydown="limitToNumeric(event);" onkeyup="onKeyUp(event, '#<%=IDPrefix%>txtEstablishDate1','#<%=IDPrefix%>txtEstablishDate2', '2');" />
				</div>
				<div class="col-xs-4">
					<input aria-labelledby="<%=IDPrefix%>lblEstablishDate" type="<%=TextAndroidTel%>" pattern="[0-9]*" placeholder="dd" id="<%=IDPrefix%>txtEstablishDate2" maxlength ='2' onkeydown="limitToNumeric(event);" onkeyup="onKeyUp(event, '#<%=IDPrefix%>txtEstablishDate2','#<%=IDPrefix%>txtEstablishDate3', '2');" />
				</div>
				<div class="col-xs-4" style="padding-right: 0;">
					<input aria-labelledby="<%=IDPrefix%>lblEstablishDate" type="<%=TextAndroidTel%>" pattern="[0-9]*" placeholder="yyyy" id="<%=IDPrefix%>txtEstablishDate3" maxlength ='4' onkeydown="limitToNumeric(event);"/>
				</div>
			</div>
		</div>
		<div data-role="fieldcontain">
			<div class="row">
				<div class="col-xs-6 no-padding"><label for="<%=IDPrefix%>txtSSN1" id="<%=IDPrefix%>lblSSN" class="RequiredIcon">SSN/TIN</label></div>
				<div class="col-xs-6 no-padding ui-label-ssn">
					<span class="pull-right header_theme2 ssn-ico" style="padding-left: 4px; padding-right: 2px; display: inline;"><%=HeaderUtils.IconLock16%><span class="sr-only">SSN/TIN IconLock</span></span>
					<span id="<%=IDPrefix%>spOpenSsnSastisfy" onclick='openPopup("#<%=IDPrefix%>popSSNSastisfy")' style="display: inline;" class="pull-right focus-able" data-rel='popup' data-position-to="window" data-transition='pop'><span class="pull-right header_theme2  ssn-ico"><%=HeaderUtils.IconQuestion16%></span><span class="sr-only">SSN/TIN IconQuestion</span></span>
					<div style="display: inline;" class="hidden">
						<label is_show="1" class="pull-right header_theme2 rename-able shadow-btn focus-able" style="margin-right: 10px; cursor: pointer;outline: none;" onclick="<%=IDPrefix%>toggleSSNText(this);">Hide SSN/TIN</label>
					</div>
					<div style="display: inline;">
						<label is_show="0" class="pull-right header_theme2 rename-able shadow-btn focus-able" style="margin-right: 10px; cursor: pointer;outline: none;" onclick="<%=IDPrefix%>toggleSSNText(this);">Show SSN/TIN</label>
					</div>
					<div class="clearfix"></div>
				</div>
			</div>
			<div id="<%=IDPrefix%>divSSN" class="ui-input-ssn row">
				<div class="col-xs-4">
					<input type="password" aria-labelledby="<%=IDPrefix%>lblSSN" pattern="[0-9]*" placeholder="---" id="<%=IDPrefix%>txtSSN1" autocomplete="new-password" maxlength ='3' next-input="#<%=IDPrefix%>txtSSN2" onkeydown="limitToNumeric(event);" onkeyup="onKeyUp(event,'#<%=IDPrefix%>txtSSN1','#<%=IDPrefix%>txtSSN2', '3');"/>
					<input type="hidden" id="<%=IDPrefix%>hdSSN1" value=""/>
				</div>
				<div class="col-xs-4">
					<input type="password" aria-labelledby="<%=IDPrefix%>lblSSN" pattern="[0-9]*" placeholder="--" id="<%=IDPrefix%>txtSSN2" autocomplete="new-password" maxlength ='2' next-input="#<%=IDPrefix%>txtSSN3" onkeydown="limitToNumeric(event);" onkeyup="onKeyUp(event,'#<%=IDPrefix%>txtSSN2','#<%=IDPrefix%>txtSSN3', '2');"/>
					<input type="hidden" id="<%=IDPrefix%>hdSSN2" value=""/>
				</div>
				<div class="col-xs-4" style="padding-right: 0px;">
					<input aria-labelledby="<%=IDPrefix%>lblSSN" type="<%=TextAndroidTel%>" pattern="[0-9]*" placeholder="----" id="<%=IDPrefix%>txtSSN3" maxlength ='4' onkeydown="limitToNumeric(event);"/>
					<input type="hidden" id="<%=IDPrefix%>txtSSN" class="combine-field-value"/>
				</div>
			</div>
		</div>
		<div id="<%=IDPrefix%>popSSNSastisfy" data-role="popup" style="max-width: 400px;">
			<div data-role="content">
				<div class="row">
					<div class="col-xs-12 header_theme2">
						<a data-rel="back" href="#" class="pull-right svg-btn"><%=HeaderUtils.IconClose %><span style="display: none;">close</span></a>
					</div>
				</div>
				<div class="row">
					<div class="col-sm-12">
						<div style="margin: 10px 0;">
							Your Social Security Number (SSN) is used for identification purposes and to determine your account opening eligibility.
						</div>
					</div>    
				</div>
			</div>
		</div>
		<asp:PlaceHolder runat="server" ID="plhAddress"></asp:PlaceHolder>
		<iframe name="<%=IDPrefix%>if_ContactInfo"  style="display: none;" src="about:blank" ></iframe>
		<form target="<%=IDPrefix%>if_ContactInfo" action="/handler/AutoFillHandler.aspx" id="<%=IDPrefix%>frm_ContactInfo">
		<%=HeaderUtils.RenderPageTitle(22, "Contact Information", True)%>
		<div data-role="fieldcontain">
			<label id="<%=IDPrefix%>lblEmail" for="<%=IDPrefix%>txtEmail">Email</label>
			<input type="email" name="email" autocomplete="email" id="<%=IDPrefix%>txtEmail" maxlength ="50"/>
		</div>
        <div data-role="fieldcontain" id="<%=IDPrefix%>divHomePhone">
	        <div>
		        <div style="display: inline-block;"><label id="<%=IDPrefix%>lblHomePhone" for="<%=IDPrefix%>txtHomePhone" class="RequiredIcon" style="white-space: nowrap;">Phone</label></div>
				<div style="display: inline-block;"><label class="phone_format rename-able" style="font-style: italic; white-space: nowrap">(xxx) xxx-xxxx</label></div>
	        </div>
			<input type="<%=TextAndroidTel%>" pattern="[0-9]*" autocomplete="tel" name="phone" id="<%=IDPrefix%>txtHomePhone" class="inphone" maxlength='14'/>
		</div>
		<div data-role="fieldcontain" id="<%=IDPrefix%>divMobilePhone">
			<div>
				<div style="display: inline-block;"><label id="<%=IDPrefix%>lblMobilePhone" for="<%=IDPrefix%>txtMobilePhone" style="white-space: nowrap;">Cell Phone</label></div>
				<div style="display: inline-block;"><label class="phone_format rename-able" style="font-style: italic; white-space: nowrap">(xxx) xxx-xxxx</label></div>
			</div>
			<input type= "<%=TextAndroidTel%>" pattern="[0-9]*" autocomplete="tel" name="mobile" id="<%=IDPrefix%>txtMobilePhone" class="inphone" maxlength='14'/>
		</div>		
		<div data-role="fieldcontain" id="<%=IDPrefix%>divFax">
			<div>
				<div style="display: inline-block;"><label id="<%=IDPrefix%>lblFax" for="<%=IDPrefix%>txtFax" style="white-space: nowrap;">Fax</label></div>
				<div style="display: inline-block;"><label class="phone_format rename-able" style="font-style: italic; white-space: nowrap">(xxx) xxx-xxxx</label></div>
			</div>
			<input type= "<%=TextAndroidTel%>" pattern="[0-9]*" autocomplete="tel" name="mobile" id="<%=IDPrefix%>txtFax" class="inphone" maxlength='14'/>
		</div>
		</form>
	 </div>
    <div class ="div-continue"  data-role="footer">
        <a href="#" data-transition="slide" type="button" class="div-continue-button" onclick="<%=IDPrefix%>ValidateSAAccountInfo()">Continue</a> 
        <a href="#divErrorDialog" style="display: none;">no text</a>
        <span> Or</span>  <a href="#" onclick="goToNextPage('#<%=PreviousPage %>');" class ="div-goback" data-corners="false" data-shadow="false" data-theme="reset"><span class="hover-goback"> Go Back</span></a>
    </div>
</div>
<input type="hidden" id="<%=IDPrefix%>hdHomePhoneCountry" />
<input type="hidden" id="<%=IDPrefix%>hdMobilePhoneCountry" />
<input type="hidden" id="<%=IDPrefix%>hdFaxCountry" />
<script type="text/javascript">
	$(function () {
		<%=IDPrefix%>initDatePicker("#<%=IDPrefix%>txtEstablishDate");
		$('.id-date-establish input', "#page<%=IDPrefix%>").on('focusout', function () {
			var maxlength = parseInt($(this).attr('maxlength'));
			// add 0 if value < 10
			$(this).val(padLeft($(this).val(), maxlength));
			<%=IDPrefix%>updateHiddenEstablishDate();
		});
		$('.ui-input-ssn input', "#page<%=IDPrefix%>").on('focusout', function () {
			<%=IDPrefix%>updateHiddenSSN();
		});

		if (is_foreign_phone) {
			// international country code
			//by default it display separateDialCode dropdown
			//don't want to display separateDialCode dropdown, just set separateDialCode: false
			$("#<%=IDPrefix%>txtHomePhone").intlTelInput({
				separateDialCode: false
			}); // don't know why it's not set default for this one
			$('#<%=IDPrefix%>txtMobilePhone').intlTelInput({
				separateDialCode: false
			});
			$('#<%=IDPrefix%>txtFax').intlTelInput({
				separateDialCode: false
			});

			// change the text
			$('.phone_format', "#page<%=IDPrefix%>").parent().addClass("hidden");

			// remove mask
			if (!isMobile.Android()) {
				$('.inphone', "#page<%=IDPrefix%>").inputmask('remove');
			} else {
				$('input.inphone', "#page<%=IDPrefix%>").off('blur').off('keyup').off('keydown');
			}
			$('input.inphone', "#page<%=IDPrefix%>").numeric();

			//set hidden phone country to "us" by default
			$("#<%=IDPrefix%>hdHomePhoneCountry").val("us");
			$("#<%=IDPrefix%>hdMobilePhoneCountry").val("us");
			$("#<%=IDPrefix%>hdFaxCountry").val("us");
		}
		//update hidden Phone Country
		$('#<%=IDPrefix%>divHomePhone').on('click', '.flag-container', function () {
			<%=IDPrefix%>updateHiddenPhoneCountry($(this), $('#<%=IDPrefix%>hdHomePhoneCountry'));
		});
		$('#<%=IDPrefix%>divMobilePhone').on('click', '.flag-container', function () {
			<%=IDPrefix%>updateHiddenPhoneCountry($(this), $('#<%=IDPrefix%>hdMobilePhoneCountry'));
		});
		$('#<%=IDPrefix%>divFax').on('click', '.flag-container', function () {
			<%=IDPrefix%>updateHiddenPhoneCountry($(this), $('#<%=IDPrefix%>hdFaxCountry'));
		});

	});
	$("#<%=IDPrefix%>popSSNSastisfy").on("popupafteropen", function (event, ui) {
		$("#" + this.id + "-screen").height("");
	});
	$("#<%=IDPrefix%>popSSNSastisfy").on("popupafterclose", function (event, ui) {
		setTimeout(function () {
			$("#<%=IDPrefix%>spOpenSsnSastisfy").focus();
		}, 100);
	});
	function <%=IDPrefix%>updateHiddenPhoneCountry(currElem, hdPhoneCountryElem) {
		if (currElem.find('li[class~="active"]')) {
			hdPhoneCountryElem.val(currElem.find('li[class~="active"]').attr('data-country-code'));
		}
	}
	function <%=IDPrefix%>updateHiddenSSN() {
		if ($("#<%=IDPrefix%>txtSSN1").val().length == 3) {
			var ssn = $("#<%=IDPrefix%>txtSSN1").val() + '-' + $("#<%=IDPrefix%>txtSSN2").val() + '-' + $("#<%=IDPrefix%>txtSSN3").val();
			$("#<%=IDPrefix%>txtSSN").val(ssn);
		}
	}
	function <%=IDPrefix%>toggleSSNText(btn) {
        return Common.ToggleSSNText(btn, "<%=IDPrefix%>");
	}
	function <%=IDPrefix%>updateHiddenEstablishDate() {
		var month = padLeft($("#<%=IDPrefix%>txtEstablishDate1").val(), 2);
		var day = padLeft($("#<%=IDPrefix%>txtEstablishDate2").val(), 2);
		var year = padLeft($("#<%=IDPrefix%>txtEstablishDate3").val(), 4);
		if (month != "" && day != "" && year != "") {
			$("#<%=IDPrefix%>txtEstablishDate").val(month + '/' + day + '/' + year);
		} else {
			$("#<%=IDPrefix%>txtEstablishDate").val("");
		}
	}
	function <%=IDPrefix%>initDatePicker(controlId) {
		$(controlId).datepicker({
			showOn: "button",
			changeMonth: true,
			changeYear: true,
			yearRange: "-50:+50",
			buttonImage: "../images/calendar.png",
			buttonImageOnly: true,
			onSelect: function (value, inst) {
				var date = $(this).datepicker('getDate');

				$(controlId + "2").val(padLeft(date.getDate(), 2));
				$(controlId + "1").val(padLeft(date.getMonth() + 1, 2));
				$(controlId + "3").val(padLeft(date.getFullYear(), 4));

				$(controlId).datepicker("hide");
				$(controlId + "1").trigger("focus").trigger("blur");
			}
		});
	}
	
	function <%=IDPrefix%>registerSaAccountInfoValidator() {
		$('#<%=IDPrefix%>txtAccountName').observer({
			validators: [
				function (partial) {
					if (Common.ValidateText($(this).val()) == false) {
						return "Account Name is required";
					}
					return "";
				}
			],
			validateOnBlur: true,
			group: "<%=IDPrefix%>ValidateSAAccountInfo"
		});
		$('#<%=IDPrefix%>divEstablishDate').observer({
			validators: [
				function (partial) {
					<%=IDPrefix%>updateHiddenEstablishDate();
					var $txtEstablishDate = $('#<%=IDPrefix%>txtEstablishDate');
					var establishDate = moment($txtEstablishDate.val(), "MM-DD-YYYY");
					if ($txtEstablishDate.val() == "") {
						return "Establish Date is required";
					} else if (!Common.IsValidDate($txtEstablishDate.val())) {
						return "Valid Establish Date is required";
					} else if (establishDate.isAfter(moment())) {
						return "Establish Date must be earlier than current date";
					}
					return "";
				}
			],
			validateOnBlur: true,
			container: '#<%=IDPrefix%>divEstablishDate',
			group: "<%=IDPrefix%>ValidateSAAccountInfo"
		});
		$('#<%=IDPrefix%>divSSN').observer({
			validators: [
				function (partial, evt) {
					<%=IDPrefix%>updateHiddenSSN();
					var ssn = $('#<%=IDPrefix%>txtSSN1').val() + $('#<%=IDPrefix%>txtSSN2').val() + $('#<%=IDPrefix%>txtSSN3').val();
					if (/^[0-9]{9}$/.test(ssn) == false) {
						return 'Valid SSN is required';
					}
					return "";
				}
			],
			validateOnBlur: true,
			container: '#<%=IDPrefix%>divSSN',
			group: "<%=IDPrefix%>ValidateSAAccountInfo"
		});
		$('#<%=IDPrefix%>txtEmail').observer({
			validators: [
				function (partial) {
					var email = $(this).val();
					if ($.trim(email) !== "" && Common.ValidateEmail(email) == false) {
						return "Valid Email is required";
					}
					return "";
				}
			],
			validateOnBlur: true,
			group: "<%=IDPrefix%>ValidateSAAccountInfo"
		});
		$('#<%=IDPrefix%>txtHomePhone').observer({
			validators: [
				function (partial) {
					var homephone = $(this).val();
					if (!Common.ValidateText(homephone)) {
						return "Valid phone number is required";
					}
					if (is_foreign_phone) {
						return validateForeignPhone('<%=IDPrefix%>txtHomePhone');
					} else {
						if (!Common.ValidatePhone(homephone)) {
							return "Valid phone number is required";
						} else {
							//validate first digit of home phone : 0 and 1 is invalid
							var firstDigit = homephone.replace("(", "").substring(0, 1);
							if (firstDigit == 0 || firstDigit == 1) {
								return 'Phone number and area code can not begin with 0 or 1 and must be a valid phone number';
							}
						}
					}
					return "";
				}
			],
			validateOnBlur: true,
			group: "<%=IDPrefix%>ValidateSAAccountInfo"
		});
		$('#<%=IDPrefix%>txtMobilePhone').observer({
			validators: [
				function (partial) {
					var mobile = $(this).val();
					if (Common.ValidateText(mobile)) {
						if (is_foreign_phone) {
							return validateForeignPhone('<%=IDPrefix%>txtMobilePhone');
						} else {
							if (!Common.ValidatePhone(mobile)) {
								return "Valid phone number is required";
							} else {
								//validate first digit of home phone : 0 and 1 is invalid
								var firstDigit = mobile.replace("(", "").substring(0, 1);
								if (firstDigit == 0 || firstDigit == 1) {
									return 'Phone number and area code can not begin with 0 or 1 and must be a valid phone number';
								}
							}
						}
					}
					return "";
				}
			],
			validateOnBlur: true,
			group: "<%=IDPrefix%>ValidateSAAccountInfo"
		});
		$('#<%=IDPrefix%>txtFax').observer({
			validators: [
				function (partial) {
					var fax = $(this).val();
					if (Common.ValidateText(fax)) {
						if (is_foreign_phone) {
							return validateForeignPhone('<%=IDPrefix%>txtFax');
						} else {
							if (!Common.ValidatePhone(fax)) {
								return "Valid fax number is required";
							} else {
								//validate first digit of home phone : 0 and 1 is invalid
								var firstDigit = fax.replace("(", "").substring(0, 1);
								if (firstDigit == 0 || firstDigit == 1) {
									return 'Fax number and area code can not begin with 0 or 1 and must be a valid fax number';
								}
							}
						}
					}
					return "";
				}
			],
			validateOnBlur: true,
			group: "<%=IDPrefix%>ValidateSAAccountInfo"
		});
	}
	<%=IDPrefix%>registerSaAccountInfoValidator();

	function <%=IDPrefix%>SetSAAccountInfo(appInfo) {
		//var appInfo = {};
		appInfo.<%=IDPrefix%>AccountName = $('#<%=IDPrefix%>txtAccountName').val().trim();
		appInfo.<%=IDPrefix%>EstablishDate = $('#<%=IDPrefix%>txtEstablishDate').val();
		appInfo.<%=IDPrefix%>SSN = Common.GetSSN($('#<%=IDPrefix%>txtSSN').val());

		var homePhoneCountry = $('#<%=IDPrefix%>hdHomePhoneCountry').val();
		var faxCountry = $('#<%=IDPrefix%>hdFaxCountry').val();
		var mobilePhoneCountry = $('#<%=IDPrefix%>hdMobilePhoneCountry').val();
		if (!is_foreign_phone) {
			faxCountry = "";
			homePhoneCountry = "";
			mobilePhoneCountry = "";
		}
		appInfo.<%=IDPrefix%>EmailAddr = $('#<%=IDPrefix%>txtEmail').val().trim();
		appInfo.<%=IDPrefix%>HomePhone = $('#<%=IDPrefix%>txtHomePhone').val();
		appInfo.<%=IDPrefix%>HomePhoneCountry = homePhoneCountry;
		appInfo.<%=IDPrefix%>MobilePhone = $('#<%=IDPrefix%>txtMobilePhone').val();
		appInfo.<%=IDPrefix%>MobilePhoneCountry = mobilePhoneCountry;
		appInfo.<%=IDPrefix%>Fax = $('#<%=IDPrefix%>txtFax').val();
		appInfo.<%=IDPrefix%>FaxCountry = faxCountry;
		<%=IDPrefix%>SetSAAddress(appInfo);
		//return appInfo;
	}
	function <%=IDPrefix%>ValidateSAAccountInfo() {
		UpdateCSRFNumber(hfCSRF);
		var validator = true;
		if ($.lpqValidate("<%=IDPrefix%>ValidateSAAccountInfo") == false) {
			validator = false;
		}
		if (<%=IDPrefix%>ValidateSAAddress() == false) {
			validator = false;
		}
		if (validator) {
			goToNextPage("#<%=NextPage%>");
		} else {
			Common.ScrollToError();
		}
		return validator;
	}
	function <%=IDPrefix%>ViewSAAccountInfo() {
		var strHtml = "";
		var accountName = htmlEncode($.trim($('#<%=IDPrefix%>txtAccountName').val()));
		var establishDate = htmlEncode($.trim($('#<%=IDPrefix%>txtEstablishDate').val()));
		var ssn = htmlEncode(Common.GetSSN($('#<%=IDPrefix%>txtSSN').val()));
		var email = htmlEncode($.trim($('#<%=IDPrefix%>txtEmail').val()));
		var mobilePhone = htmlEncode($.trim($('#<%=IDPrefix%>txtMobilePhone').val()));
		var homePhone = htmlEncode($.trim($('#<%=IDPrefix%>txtHomePhone').val()));
		var fax = htmlEncode($.trim($('#<%=IDPrefix%>txtFax').val()));
        
		if (accountName !== "") {
			strHtml += '<div class="col-sm-6 col-xs-12"><div class="row"><div class="col-xs-6 text-right row-title"><span class="bold">Account Name</span></div><div class="col-xs-6 text-left row-data"><span>' + accountName + '</span></div></div></div>';
		}
		if (establishDate !== "") {
			strHtml += '<div class="col-sm-6 col-xs-12"><div class="row"><div class="col-xs-6 text-right row-title"><span class="bold">Establish Date</span></div><div class="col-xs-6 text-left row-data"><span>' + establishDate + '</span></div></div></div>';
		}
		if (ssn !== "") {
			strHtml += '<div class="col-sm-6 col-xs-12"><div class="row"><div class="col-xs-6 text-right row-title"><span class="bold">SSN/TIN</span></div><div class="col-xs-6 text-left row-data"><span>' + ssn.replace(/^\d{5}/, "*****") + '</span></div></div></div>';
		}
		if (email !== "") {
			strHtml += '<div class="col-sm-6 col-xs-12"><div class="row"><div class="col-xs-6 text-right row-title"><span class="bold">Email</span></div><div class="col-xs-6 text-left row-data"><span>' + email + '</span></div></div></div>';
        }
        if (homePhone !== "") {
            strHtml += '<div class="col-sm-6 col-xs-12"><div class="row"><div class="col-xs-6 text-right row-title"><span class="bold">Phone</span></div><div class="col-xs-6 text-left row-data"><span>' + homePhone + '</span></div></div></div>';
        }
		if (mobilePhone !== "") {
			strHtml += '<div class="col-sm-6 col-xs-12"><div class="row"><div class="col-xs-6 text-right row-title"><span class="bold">Mobile Phone</span></div><div class="col-xs-6 text-left row-data"><span>' + mobilePhone + '</span></div></div></div>';
		}
		
		if (fax !== "") {
			strHtml += '<div class="col-sm-6 col-xs-12"><div class="row"><div class="col-xs-6 text-right row-title"><span class="bold">Fax</span></div><div class="col-xs-6 text-left row-data"><span>' + fax + '</span></div></div></div>';
		}
		strHtml += <%=IDPrefix%>ViewAddress();
		return strHtml;
    }
    function <%=IDPrefix%>AutofillData() {
     $('#<%=IDPrefix%>txtAccountName').val("SAX Account");
        $('#<%=IDPrefix%>txtEstablishDate1').val('11');
        $('#<%=IDPrefix%>txtEstablishDate2').val('11');
        $('#<%=IDPrefix%>txtEstablishDate3').val('2017');
        $('#<%=IDPrefix%>txtSSN1').val('000'); 
        $('#<%=IDPrefix%>txtSSN2').val('00');    
        $('#<%=IDPrefix%>txtSSN3').val('0012');    
		<%=IDPrefix%>updateHiddenEstablishDate();	
		<%=IDPrefix%>updateHiddenSSN();
        $('#<%=IDPrefix%>txtEmail').val('saTest@test.com');
        $('#<%=IDPrefix%>txtHomePhone').val('7142223333');
	    $('#<%=IDPrefix%>txtMobilePhone').val('7144442222');
        $('#<%=IDPrefix%>txtFax').val('7145552222');
        if (typeof <%=IDPrefix%>autoFillData_Address != "undefined") {
            <%=IDPrefix%>autoFillData_Address();
        }
    }
</script>