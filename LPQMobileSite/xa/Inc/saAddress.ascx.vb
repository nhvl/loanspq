﻿
Partial Class xa_Inc_saAddress
	Inherits CBaseUserControl
	Public Property StateDropdown As String = ""
	Public Property EnabledAddressVerification As Boolean
	Public Property EnableOccupancyStatus As Boolean = True
	Public Property EnableMailingAddress As Boolean = True
	Public Property OccupyingLocationDropdown As String
	Public Property CollectDescriptionIfOccupancyStatusIsOther As Boolean
	Public Property OccupancyStatusesForRequiredHousingPayment As List(Of String) = New List(Of String)({"RENT"})
	Public Property LiveYearsDropdown As String
	Public Property LiveMonthsDropdown As String
	Public Property Header As String
	Public Property PreviousAddressHeader As String
	Public Property EnableHousingPayment As Boolean = False
    Public Property IsBusinessLoan As Boolean = False
    Public Property IsAddressRequired As Boolean = True
	Public Property IsJoint As Boolean = False

	Public Property AllowControlOccupancyVisibility As Boolean = True
	Public Property ShowFieldOccupancyStatusDefaultStateOn As Boolean = False

    Protected Function callVerifyAddressJSFunction(isZipcodeTextbox As Boolean, Optional ByVal prefixAddress As String = "") As String
		'' verifyAddress js function needs to attach to address + zip + city + state html controls
		If EnabledAddressVerification Then
			Return String.Format("{0}verifyAddress('{1}');", IDPrefix, prefixAddress)
		Else
			'' lookupZipcode js function only needs to attach to zip html control
			If isZipcodeTextbox Then
				Return String.Format("{0}lookupZipcode(this.value,'{1}');", IDPrefix, prefixAddress)
			End If
		End If
		Return String.Empty
	End Function

	Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
		If Not IsPostBack Then
			If String.IsNullOrEmpty(Header) Then
				Header = HeaderUtils.RenderPageTitle(14, "Current Physical Address", True)
			End If
			If String.IsNullOrEmpty(PreviousAddressHeader) Then
				PreviousAddressHeader = HeaderUtils.RenderPageTitle(0, "Previous Address", True)
			End If
		End If
	End Sub
End Class
