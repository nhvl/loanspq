﻿
Partial Class xa_Inc_SpecialAccountSelector
	Inherits CBaseUserControl
	Public Property SpecialAccountTypeList As List(Of CSpecialAccountType)
	Protected SpecialAccountTypeDic As New Dictionary(Of String, String)
	Public Property LenderRef As String
	Public Property DefaultParams As String
	Public Property PreselectedAccountType As String = ""
	Public Property ScenarioName As String = ""
	Protected Property AccountTypeDisabled As Boolean = False

	Protected Function ParseParams(paramsStr As String) As Dictionary(Of String, String)
		Dim result As New Dictionary(Of String, String)
		If paramsStr IsNot Nothing Then
			paramsStr = paramsStr.Trim({"?"c, "&"c, " "c})
			If Not String.IsNullOrEmpty(paramsStr) Then
				For Each kvStr As String In paramsStr.Split("&"c)
					Dim kvArr = kvStr.Split("="c)
					If kvArr.Length = 2 AndAlso Not result.ContainsKey(kvArr(0)) Then
						result.Add(kvArr(0), kvArr(1))
					End If
				Next
			End If
		End If
		Return result
	End Function

	Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
		If Not IsPostBack Then
			If SpecialAccountTypeList IsNot Nothing Then
				For Each item In SpecialAccountTypeList
					If Not SpecialAccountTypeDic.ContainsKey(item.AccountCode) Then
						SpecialAccountTypeDic.Add(item.AccountCode, item.AccountName)
					End If
				Next
				If Not String.IsNullOrWhiteSpace(ScenarioName) Then
					AccountTypeDisabled = True
				End If
			End If
		End If
	End Sub
End Class
