﻿Imports System.Web.Script.Serialization
Partial Class xa_Inc_baBeneficialOwners
	Inherits CBaseUserControl
	Public Property PreviousPage As String = ""
	Public Property Availability As String
	Public Property LogoUrl As String
	Public Property NextPage As String = ""
	Public Property SelectedIDPrefixes As List(Of String)
	Protected RoleList As New Dictionary(Of String, String)
    Public Property StateDropdown As String
    Public Property CitizenshipStatusDropdown As String
	Public Property EnableBeneficiarySSN As Boolean
    Public Property EnabledAddressVerification As Boolean = False
    Protected RequiredOwneshipEntityTypes As String
    Protected RequiredControlEntityTypes As String
    Public Property BusinessType As String = ""
    Public Property IsBusinessLoan As Boolean = False
    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
		RoleList.Add("Agent", "Agent")
		RoleList.Add("Authorized Signer", "Authorized Signer")
		RoleList.Add("Corporate Representative", "Corporate Representative")
        ''RoleList.Add("Custom", "Custom") 
        RoleList.Add("Manager", "Manager")
		RoleList.Add("Member", "Member")
		RoleList.Add("Owner", "Owner")
		RoleList.Add("Partner", "Partner")
		RoleList.Add("President", "President")
		RoleList.Add("Secretary", "Secretary")
		RoleList.Add("Treasurer", "Treasurer")
        RoleList.Add("Vice President", "Vice President")
        ''required ownership entity types
        Dim requiredOSTypes = {"ASSN", "CCORP", "CCORPLLC", "CORP", "GEN_PARTNER", "LLC", "LLP", "NONMEMBER", "PARTNER", "PARTNERLLC", "PROFESSIONALCORP", "SCORP", "SCORPLLC", "TRUST"}
        ''required Control entity types
        Dim requiredControlTypes = {"NONPROFITCORP", "NONPROFITORG", "NONPROFIT"}
        RequiredOwneshipEntityTypes = New JavaScriptSerializer().Serialize(requiredOSTypes)
        RequiredControlEntityTypes = New JavaScriptSerializer().Serialize(requiredControlTypes)

    End Sub

    Protected Function callVerifyAddressJSFunction() As String
        '' verifyAddress js function needs to attach to address + zip + city + state html controls
        If EnabledAddressVerification Then
            Return String.Format("{0}verifyAddress(index);", IDPrefix)
        End If
        Return String.Empty
    End Function

End Class

