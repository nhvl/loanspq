﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="xpressApp.aspx.vb" Inherits="CXpressApp"
    EnableViewState="false" %>

<%@ Import Namespace="LPQMobile.Utils" %>
<%--<%@ Register Src="~/Inc/MemberProtectionPlan.ascx" TagPrefix="uc" TagName="memberProtection" %>--%>
<%@ Register Src="~/Inc/PageHeader.ascx" TagPrefix="uc" TagName="pageHeader" %>
<%@ Register Src="~/Inc/Disclosure.ascx" TagPrefix="uc" TagName="disclosure" %>
<%@ Register Src="~/Inc/MainApp/xaApplicantInfo.ascx" TagPrefix="uc" TagName="xaApplicantInfo" %>
<%@ Register Src="~/Inc/MainApp/xaApplicantQuestion.ascx" TagPrefix="uc" TagName="xaApplicantQuestion" %>
<%@ Register Src="~/Inc/MainApp/xaApplicantContactInfo.ascx" TagPrefix="uc" TagName="xaApplicantContactInfo" %>
<%@ Register Src="~/Inc/MainApp/xaApplicantID.ascx" TagPrefix="uc" TagName="xaApplicantID" %>
<%@ Register Src="~/Inc/MainApp/xaApplicantAddress.ascx" TagPrefix="uc" TagName="xaApplicantAddress" %>
<%@ Register Src="~/Inc/MainApp/xaApplicantEmployment.ascx" TagPrefix="uc" TagName="xaApplicantEmployment" %>
<%@ Register Src="~/Inc/MainApp/xaFundingOptions.ascx" TagPrefix="uc" TagName="xaFundingOptions" %>
<%@ Register Src="~/Inc/MainApp/xaFOMQuestion.ascx" TagPrefix="uc" TagName="xaFOMQuestion" %>
<%--<%@ Register Src="~/Inc/MainApp/UploadDocDialog.ascx" TagPrefix="uc" TagName="ucUploadDocDialog" %>--%>
<%@ Register Src="~/Inc/NewDocCapture.ascx" TagPrefix="uc" TagName="DocUpload" %>
<%@ Register Src="~/Inc/DocCaptureSourceSelector.ascx" TagPrefix="uc" TagName="DocUploadSrcSelector" %>
<%@ Register Src="~/Inc/NewDocumentScan.ascx" TagPrefix="uc" TagName="driverLicenseScan" %>
<%@ Register Src="~/xa/inc/ProductSelection.ascx" TagPrefix="uc" TagName="ProductSelection" %>
<%@ Register Src="~/Inc/MainApp/xaBeneficiary.ascx" TagPrefix="uc" TagName="xaBeneficiary" %>
<%@ Reference Control="~/xa/inc/ProductServices.ascx" %>
<%@ Reference Control="~/Inc/MainApp/ApplicationBlockRules.ascx" %>

<%@ Register TagPrefix="uc1" TagName="Piwik" Src="~/Inc/Piwik/PiwikTracking.ascx" %>
<%@ Register TagPrefix="uc" TagName="pageFooter" Src="~/Inc/PageFooter.ascx" %>
<%@ Register Src="~/Inc/LaserDocumentScan.ascx" TagPrefix="uc" TagName="LaserDriverLicenseScan" %>
<%@ Import Namespace="System.Globalization" %>
<%@ Import Namespace="Newtonsoft.Json" %>
<%@ Import Namespace="System.Web.Optimization" %>
<%@ Reference Control="~/xa/Inc/saApplicantInfo.ascx" %>
<%@ Reference Control="~/xa/Inc/saAccountInfo.ascx" %>
<%@ Reference Control="~/xa/Inc/baBusinessInfo.ascx" %>
<%@ Reference Control="~/xa/Inc/baApplicantInfo.ascx" %>
<%@ Reference Control="~/xa/Inc/baBeneficialOwners.ascx" %>
<%@ Reference Control="~/Inc/MainApp/InstaTouchStuff.ascx" %>

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">
<head id="Head1" runat="server">
    <title>XA Mobile</title>
    <meta name="viewport" content="width=device-width, initial-scale=1" />

    <script type="text/javascript">
        var BRANCHZIPCODEPOOLNAME = <%=JsonConvert.SerializeObject(_xaBranchZipPoolName)%>;
	    var MINORPRODUCTLIST=<%=_minoProductList%>;
        var MINORACCOUNTLIST =<%=_minorAccountList%>;       
	    var _ProductQuestionPackage = <%=_ProductPackageJsonString %>;
	    var _FundingSourcePackage = <%=_FundingSourcePackageJsonString%>;
	    <%--var _FooterThemeData ='<%=_FooterDataTheme%>';--%>
    	var OCCUPATIONLIST = '<%=_occupationList%>';
    	var SAPREFIXLIST = <%=JsonConvert.SerializeObject(SAPrefixList)%>;
    	var BAPREFIXLIST = <%=JsonConvert.SerializeObject(BAPrefixList)%>;
        var PRIMARYAPPLICANTINFOPREFIX = '<%=PrimaryApplicantInfoPrefix%>'; 
      
    </script>
	<%:Styles.Render("~/css/thirdparty/bootstrap") %>
    <uc:pageHeader ID="ucPageHeader" ScriptFolder="xa" runat="server" />
	<%:Styles.Render("~/css/thirdparty/custom") %>
</head>
<body class="lpq_container" >
    <uc1:Piwik ID="ucPiwik" runat="server"></uc1:Piwik>
	<input type="hidden" id="hdPlatformSource" value="<%=PlatformSource%>" />
    <input type="hidden" id="hfLenderRef" value='<%=_CurrentLenderRef%>' />
    <input type="hidden" id="hfBranchId" value='<%=_CurrentBranchId%>' />
	<input type="hidden" id="hfLoanOfficerID" value='<%=_CurrentLoanOfficerId%>' />
	<input type="hidden" id="hfInstitutionType" value="<%=_InstitutionType.ToString()%>" />
    <input type="hidden" id="hfCSRF" value='<%=_token%>' />
    <input type="hidden" id="hfReferralSource" value='<%=_ReferralSource%>' />
    <%--<input type="hidden" id="hdOccupancyStatus" value='<%=_OccupancyStatus %>' />--%>
    <input type="hidden" id="hdPrevAddressThreshold" value='<%= _previous_address_threshold%>' />
	<%--<input type="hidden" id="hdPrevEmploymentThreshold" value="<%=_previous_employment_threshold%>" />--%>
	<input type="hidden" id="hdPrevEmploymentThreshold" value="0" /> <%-- TODO: not yet suport on lender side so set a bogus value so this feature will not be activate--%>
    <input type="hidden" id="hdCheckMailAddress" value="<%=_CheckMailAddress%>" />
    <input type="hidden" id="hdAddressKey" value='<%= _address_key%>' />
    <input type="hidden" id="hdCCMaxFunding" value='<%=_CCMaxFunding%>' />
	<input type="hidden" id="hdACHMaxFunding" value='<%=_ACHMaxFunding%>' />
	<input type="hidden" id="hdPaypalMaxFunding" value='<%=_PaypalMaxFunding%>' />
	<input type="hidden" id="hdMembershipFee" value='<%=_MembershipFee%>' />
    <input type="hidden" id="hdRedirectURL" value="<%=_RedirectXAURL%>" />
    <%--<input type="hidden" id="hdHeaderTheme" value="<%=_HeaderDataTheme%>" />--%>
    <%--<input type="hidden" id="hdFooterTheme" value="<%=_bgTheme %>" />
    <input type="hidden" id="hdBgTheme" value="<%=_BackgroundTheme%>" />--%>
    <input type="hidden" id="hdHasMinorApplicant" value="<%=_HasMinorApplicant%>" />
    <input type="hidden" id="hdHasEnableSecUploadDocs" value="<%=_HasEnableSecondaryUpLoadDocs%>" />
    <input type="hidden" id="hdHasCoApplicant" value="N"/>
    <%--<input type="hidden" id="hdShowEmployment" value="<%=_ShowEmployment%>"/>
    <input type="hidden" id="hdShowMinorEmployment" value="<%=_HasMinorEmployment %>"/>--%>
    <input type="hidden" id="hdSecondRoleType" value="" />
    <%--<input type="hidden" id="hdMaritalStatus" value ="<%=_maritalStatus%>" />--%>
	<input type="hidden" id="hdEmploymentDurationRequired"  value="<%=_isEmploymentDurationRequired%>" />
	<input type="hidden" id="hdHomePhoneRequired"  value="<%=_isHomephoneRequired%>" />
	<input type="hidden" id="hdStopInvalidRoutingNumber" value="<%=_xaStopInvalidRoutingNumber%>" />
	<input type="hidden" id="hdFundingAccountNumberFreeForm" value="<%=_xaFundingAccountNumberFreeForm%>" />
    <%-- moved from inside page#1 don't wknow why page1 dissapear after submit & save--%>
    <input type="hidden" runat="server" id="hdNumWalletQuestions" />
    <input type="hidden" runat="server" id="HideFOMPage" />
    <input type="hidden" runat="server" id="hdForeignAppType" />
    <input type="hidden" runat="server" id="hdScanDocumentKey" />
   
    <input type="hidden" id="hdForeignContact" runat ="server"  />
    <input type="hidden" id="hdMinorAccountCode" runat="server" value=""/>
    <input type="hidden" id="hdRequiredDLScan" value="<%=_requiredDLScanXA%>" />
     <!--hidden secondary fields -->
    <%--<input type="hidden" id="hdSecondaryIdPage" runat="server" />--%>
    <%--<input type="hidden" id="hdSecondaryOccupancy" runat="server" />--%>
    <%--<input type="hidden" id="hdSecondaryGrossIncome" runat="server" />--%>
    <%--<input type="hidden" id="hdVisibleMemberNumber"  runat="server"  />
    <input type="hidden" id="hdRequiredMemberNumber" runat="server"/>--%>
    <input type="hidden" id="hdSpecialJoint" runat ="server" />
    <input type="hidden" id="hdRequiredBranch" value="<%=_xaRequiredBranch%>" />
	
	<input type="hidden" id="hdEnableEmploymentSection" value="<%=IIf(EnableEmploymentSection, "Y", "N") %>"/>
	<input type="hidden" id="hdEnableEmploymentSectionForCoApp" value="<%=IIf(EnableEmploymentSection("co_"), "Y", "N") %>"/>
	<input type="hidden" id="hdEnableEmploymentSectionForMinor" value="<%=IIf(EnableEmploymentSection("m_"), "Y", "N") %>"/>
	<input type="hidden" id="hdEnableIDSection" value="<%=IIf(EnableIDSection, "Y", "N") %>"/>
	<input type="hidden" id="hdEnableIDSectionForCoApp" value="<%=IIf(EnableIDSection("co_"), "Y", "N") %>"/>
	<input type="hidden" id="hdEnableIDSectionForMinor" value="<%=IIf(EnableIDSection("m_"), "Y", "N") %>"/>
	<input type="hidden" id="hdEnableDisclosureSection" value="<%=IIf(EnableDisclosureSection, "Y", "N") %>"/>
	<input type="hidden" id="hdEnableOccupancyStatusSection" value="<%=IIf(EnableOccupancyStatusSection(), "Y", "N") %>"/>
	<input type="hidden" id="hdEnableOccupancyStatusSectionForCoApp" value="<%=IIf(EnableOccupancyStatusSection("co_"), "Y", "N")%>"/>
	<input type="hidden" id="hdEnableOccupancyStatusSectionForMinor" value="<%=IIf(EnableOccupancyStatusSection("m_"), "Y", "N") %>"/>
	
	<input type="hidden" id="hdEnableBranchSection" value="<%=IIf(EnableBranchSection AndAlso Not String.IsNullOrEmpty(_xaBranchOptionsStr), "Y", "N") %>"/>
    <input type="hidden" id="hdHasZipCodePool" value="<%=IIf(_locationPool, "Y", "N") %>" />
    <input type="hidden" id="hdUrlZipMode"  value="<%=UrlZipMode %>" />
    <input type="hidden" id="hdHasSSOIDA"value="<%=IIf(IsSSO And xa_sso_ida_enable, "Y", "N") %>" />
	
	
    <div>
	    <div id="GettingStarted" data-role="page">
              <div data-role="header" class="sr-only">
                <%If _Availability = "2" Or _Availability = "2a" Or _Availability = "2b" Or _Availability = "2s" Then%>
                    <h1>Open an Account</h1>
                <%Else%>
					<%If _InstitutionType = CEnum.InstitutionType.BANK Then%>
						 <h1>Open an Account</h1>
					 <%Else%>
						<h1>New Membership</h1>
					 <%End If%>
                <%End If%>
            </div>
            <div data-role="content">        
                <%If _Availability = "2" Or _Availability = "2a" Or _Availability = "2b" Or _Availability = "2s" Then%>
                    <%=HeaderUtils.RenderLogoAndSteps(_LogoUrl, 0, "OPEN")%>
                    <%=HeaderUtils.RenderPageTitle(9, "Open an Account", False)%>
                <%ElseIf _InstitutionType = CEnum.InstitutionType.BANK Then%>
                    <%=HeaderUtils.RenderLogoAndSteps(_LogoUrl, 0, "APPLY_CC")%>
                    <%=HeaderUtils.RenderPageTitle(21, "Getting Started", False)%>
                <%Else%>
                    <%=HeaderUtils.RenderLogoAndSteps(_LogoUrl, 1, "JOIN")%>
                    <%=HeaderUtils.RenderPageTitle(21, "Eligibility", False)%>
                <%End If%>

                <%--Special Account mode--%>
                <%If NoProductAvailable = True Then%>
				    <%=HeaderUtils.RenderPageTitle(0, "No Product Available", True)%>
				    <br/>
				    <p class="rename-able">Thank you for considering us for your banking needs. We're sorry, but we are unable to serve you at this time.</p>
				    <p class="rename-able">Please, feel free to check back with us in the future.</p>
                     <div class="hidden">
                         <%--prevent script error still loading FOMquestion and Product selection user control even there are no available product --%>
                          <uc:xaFOMQuestion runat="server" ID='XaFOMQuestion1' />
				          <uc:ProductSelection ID="ProductSelection1" runat="server" />
                     </div>
				<%Else%>
                    <%--<%=HeaderUtils.RenderPageTitle(21, IIf(_Availability = "1b" or _Availability = "2s", "Membership ", "").ToString() & "Eligibility", False)%>--%>
                    <%If _locationPool Then%>
				        <div id="divLocationPool">					     
					        <div data-role="fieldcontain">
                                <label for="txtLocationPoolCode"> <%=HeaderUtils.RenderPageTitle(0, "Enter your ZIP Code", True, isRequired:=True) %></label>
						        <input type="<%=_textAndroidTel%>" id="txtLocationPoolCode" pattern="[0-9]*" class="inzipcode" maxlength="5"/>
					        </div>	
					        <div class="hidden" id="divNoAvailableProducts">
						        <%=HeaderUtils.RenderPageTitle(0, "No Available Products", True)%>
						        <p>Thank you for considering us for your banking needs. We're sorry, but we are not offering products in your ZIP Code at this time.</p>
						        <p>Please feel free to check back with us in the future.</p>
					        </div>
				        </div>
				    <%End If%>
				    <div <%=IIf(_locationPool, "class='hidden'", "") %> >
					    <%If _RenderSpecialAccountType.Length > 0 Then%>                	
						    <%=HeaderUtils.RenderPageTitle(0, "Select if this applies to you", True)%>
						 
                                 <div class="container" id="btnSelectionContainer"><div>
								    <div id="divAccountTypeCol"><%=_RenderSpecialAccountType%></div>
							    </div>
						    </div> 
                    
					     <%End If%>
					    <%If EnableBranchSection AndAlso Not String.IsNullOrEmpty(_xaBranchOptionsStr) Then%>
					    <div id="divBranchSection" <%=IIf(IsInMode("777") AndAlso IsInFeature("visibility"), "class=""showfield-section"" data-show-field-section-id=""" & BuildShowFieldSectionID("divBranchSection") & """ data-default-state=""off"" data-section-name= 'Branch'", "")%>>
					    <%--Branch location --%>
					     <div data-role="fieldcontain">
						    <%=HeaderUtils.RenderPageTitle(0, "Select branch location", True, "divTitleBranchName", isRequired:=_xaRequiredBranch <> "N")%>
						     <div id='divBranchName' class='header-theme-dropdown-btn'>
							    <select aria-labelledby="divTitleBranchName" id="ddlBranchName"><%=_xaBranchOptionsStr%></select> 
						    </div>
					    </div> 
						    </div>
					    <%End If %>
					    <%--display Eligibility Message --%>
					     <%= _eligibilityMessage%>
                          
					    <uc:xaFOMQuestion runat="server" ID='ucFomQuestion' />
					    <%-- Page 1 --%>
				       <uc:ProductSelection ID="ucProductSelection" runat="server" />
					    <%--<div>
						    <uc:OtherProduct ID="ucOtherProduct" runat="server" />
					    </div>--%>
						<uc:xaApplicantQuestion ID="xaApplicationQuestionLoanPage" LoanType="XA" IDPrefix="loanpage_" CQLocation="LoanPage" runat="server"  />
					    <div id="disclosure_place_holder"></div>
				    <div class ="div-continue" data-role="footer">
					    <a href="#" data-transition="slide" onclick="validateGettingStartedPage('<%=AfterGettingStartedPage %>');" type="button" class="div-continue-button">Continue</a> 
					    <a href="#divErrorDialog" style="display: none;">no text</a>
						    <%--<%=_HasNavLeft%>--%>
				    </div>
                 </div>
		        <%End If%>
					<%--Next Page transition and destination is done inside validateActivePage onclick="handleDepositChange(this); validateActivePage(this, true); "--%>
			</div>
        </div>
		<asp:PlaceHolder runat="server" ID="plhBusinessInfoPage"></asp:PlaceHolder>
		<asp:PlaceHolder runat="server" ID="plhSpecialAccountInfoPage"></asp:PlaceHolder>
		<asp:PlaceHolder runat="server" ID="plhBusinessAccountPagesWrapper"></asp:PlaceHolder>
	    <asp:PlaceHolder runat="server" ID="plhSpecialAccountPagesWrapper"></asp:PlaceHolder>
		<asp:PlaceHolder runat="server" ID="plhBusinessBeneficialOwnersPage"></asp:PlaceHolder>

		<div id="productDetail" data-role="dialog" data-close-btn="none" style="width: 100%;">
			<div data-role="header">
				<button class="header-hidden-btn">.</button>
				<div data-place-holder="heading" class="page-header-title"></div>
				<div tabindex="0" data-command="close" data-main-page="GettingStarted" class="header_theme2 ui-btn-right" data-role="none">
					<%=HeaderUtils.IconClose %><span style="display: none;">close</span>
				</div>
			</div>
			<div data-role="content" class="fieldset-container">
				<fieldset data-role="controlgroup" data-place-holder="description">
					<legend class="heading-title">Description</legend>
					<p style="margin-bottom: 0px;"></p> 
				</fieldset>
				<fieldset data-place-holder="rates">
					<legend class="heading-title">Details</legend>
                    <%--<div></div>--%>
				</fieldset>
				<fieldset data-place-holder="services">
					<legend class="heading-title">Select feature(s)</legend>
					<div class="ui-controlgroup-controls"></div>
				</fieldset>
				<fieldset data-place-holder="questions">
					<legend class="heading-title">Additional feature(s)</legend>
					<div class="ui-controlgroup-controls"></div>
				</fieldset>
				<div style="text-align: center;">
					<a href="#" data-role="button" data-mode="self-handle-event" type="button" data-main-page="GettingStarted" data-inline="true" id="btnAddProduct">Add Account</a>
					<a href="#" data-role="button" data-mode="self-handle-event" type="button" data-main-page="GettingStarted" data-inline="true" id="btnSaveProduct">Save Account</a>
					<a href="#" data-role="button" data-mode="self-handle-event" type="button" data-main-page="GettingStarted" data-inline="true" style="min-width: 150px;" id="btnCloseProductDetail">OK</a>
					<input type="hidden" value="" data-place-holder="productCode" />
					<%--<input type="hidden" value="" data-place-holder="sourceElementId" />--%>
				</div>    
			</div>
		</div>
		<div id="fundingDialog" data-role="dialog" data-close-btn="none" style="width: 100%;">
			<div data-role="header">
				<button class="header-hidden-btn">.</button>
				<div data-place-holder="heading" class="page-header-title"></div>
				<div tabindex="0" data-command="close" data-main-page="pageFS" class="header_theme2 ui-btn-right" data-role="none">
					<%=HeaderUtils.IconClose %><span style="display: none;">close</span>
				</div>
			</div>
			<div data-role="content" class="fieldset-container">
				<fieldset data-place-holder="rates">
					<legend class="heading-title">Choose your rate</legend>
				</fieldset>
				<fieldset data-place-holder="deposit">
					<legend class="heading-title">Specify the amount to deposit</legend>
					<div class="row">
						<div class="col-xs-12 col-sm-4 no-padding">
							<div data-role="fieldcontain">
								<input type="text" class="money" maxlength="12" id="txtRateDepositAmount"/>
							</div>
						</div>
					</div>
				</fieldset>
				<fieldset data-place-holder="term">
					<legend class="heading-title">Choose your term</legend>
					<div class="row">
						<div class="col-xs-12 col-sm-4 no-padding">
							<div data-role="fieldcontain">
								<label for="txtRateTermLength" id="lblRateTermLength" class="RequiredIcon"></label>
								<input type="<%=TextAndroidTel%>" maxlength="12" class="number-only" id="txtRateTermLength"/>
							</div>
						</div>
						<div class="col-xs-12 col-sm-8 no-padding maturity-date-wrapper">
							<div class="row">
								<div class="col-xs-12 col-sm-2 no-padding text-center-sm">
									<div data-role="fieldcontain">
										<label class="hidden-xs">&nbsp;</label>
										<label>Or</label>
									</div>
								</div>
								<div class="col-xs-12 col-sm-10 no-padding">
									<div data-role="fieldcontain" id="divRateMaturityDateStuff">
										<div class="row">
											<div class="col-xs-8 no-padding">
												<label for="txtRateMaturityDate" id="lblRateMaturityDate" class="RequiredIcon">Maturity Date</label>
											</div>
											<div class="col-xs-4 no-padding"><input type="hidden" id="txtRateMaturityDate" class="combine-field-value" value="" /></div>
										</div>
										<div id="divRateMaturityDate" class="ui-input-date row">
											<div class="col-xs-4">
												<input aria-labelledby="lblRateMaturityDate" type="<%=TextAndroidTel%>" pattern="[0-9]*" placeholder="mm" id="txtRateMaturityDate1" maxlength ='2' onkeydown="limitToNumeric(event);" onkeyup="onKeyUp(event,'#txtRateMaturityDate1','#txtRateMaturityDate2', '2');" value="" />
											</div>
											<div class="col-xs-4">
												<input aria-labelledby="lblRateMaturityDate" type="<%=TextAndroidTel%>" pattern="[0-9]*" placeholder="dd" id="txtRateMaturityDate2" maxlength ='2' onkeydown="limitToNumeric(event);" onkeyup="onKeyUp(event,'#txtRateMaturityDate2','#txtRateMaturityDate3', '2');" value="" />
											</div>
											<div class="col-xs-4" style="padding-right: 0px;">
												<input aria-labelledby="lblRateMaturityDate" type="<%=TextAndroidTel%>" pattern="[0-9]*" placeholder="yyyy" id="txtRateMaturityDate3" onkeydown="limitToNumeric(event);" maxlength ='4' value="" />
											</div>
										</div>
									</div>	
								</div>	
							</div>
						</div>
					</div>
				</fieldset>
				<br/>
				<div style="text-align: center;">
					<a href="#" data-role="button" data-mode="self-handle-event" type="button" data-main-page="pageFS" data-inline="true" style="min-width: 150px;" id="btnAcceptRate">Accept</a>
					<input type="hidden" value="" data-place-holder="productCode" />
					<input type="hidden" value="" data-place-holder="instanceId" />
				</div>    
			</div>
		</div>
		<%--<div id="productConfirm" data-role="dialog" data-close-btn="none" style="width: 100%;">
			<div data-role="content" style="padding: 30px 30px 10px;">
				<div>Proceed with the following products?</div>
				<div data-place-holder="selected-prod-list">
					<!--<div tabindex="0" class="ui-btn ui-corner-all pool-item-option">
						<span>Product 1</span><div class="btn-remove"></div>
					</div>-->
				</div>
				<div class ="div-continue" style="text-align: center;margin-top: 30px;">
					<a href="#"  data-transition="slide" onclick="validateActivePage(this, true);" type="button" data-role="button" class="div-continue-button">Continue</a> 
					<a href="#divErrorDialog" style="display: none;">no text</a>      
					Or <a href="#" class ="div-goback abort-hover" onclick="goBack(this, '#divSelectedProductPool')" data-corners="false" data-shadow="false" data-theme="reset"><span class="hover-goback"> Go Back</span></a>          
				</div>
			</div>
		</div>--%>
        <asp:PlaceHolder runat="server" ID="plhApplicationDeclined"></asp:PlaceHolder>
		<asp:PlaceHolder runat="server" ID="plhInstaTouchStuff"></asp:PlaceHolder>
		<div data-role="page" id="pageApplicationCancelled">
			  <div data-role="header" class="sr-only">
				<h1>Application Cancelled</h1>
			</div>
		  <div data-role="content">
			  <%If _Availability = "2" Or _Availability = "2a" Or _Availability = "2b" Or _Availability = "2s" Then%>
                    <%=HeaderUtils.RenderLogoAndSteps(_LogoUrl, 0, "OPEN")%>
				<%ElseIf _InstitutionType = CEnum.InstitutionType.BANK Then%>
					<%=HeaderUtils.RenderLogoAndSteps(_LogoUrl, 0, "APPLY_CC")%>
				<%Else%>
					<%=HeaderUtils.RenderLogoAndSteps(_LogoUrl, 0, "JOIN")%>
				<%End If%>
			  <br/>
			  <div class="text-center">
			    <%=HeaderUtils.RenderPageTitle(0, "Application Cancelled", False)%>
				</div>
			  <br/>
			  <p class="text-center">We have cancelled your application.</p>
			  <p class="text-center">If this was a mistake and you would like to fix errors you can return to the application now.</p>
			  <br/>

				<div class="row div-continue" style="margin-top: 10px; text-align: center; width:100%;" data-role="footer">
					<div class="col-md-2 hidden-xs"></div>
					<div class="col-md-4 col-xs-12">
						<a href="#" onclick="ABR.FACTORY.closeApplicationBlockRulesDialog()" type="button" data-role="button" class="div-continue-button">Return to Application</a>	
					</div>
					<div class="col-md-4 col-xs-12">
						<a href="#" onclick="gotoToUrl(this,'<%=_RedirectXAURL%>')" data-role="button" type="button" class="div-continue-button">Close</a>
						</div>
					<div class="col-md-2 hidden-xs"></div>
				</div>
		   </div>
	 </div>
        <div data-role='page' id='pageFS'>
              <div data-role="header" class="sr-only">
                <h1>Funding</h1>
            </div>
            <div data-role="content">
                <%If _Availability = "2" Or _Availability = "2a" Or _Availability = "2b" Or _Availability = "2s" Then%>
                    <%=HeaderUtils.RenderLogoAndSteps(_LogoUrl, 2, "OPEN")%>
                <%Else%>
                    <%=HeaderUtils.RenderLogoAndSteps(_LogoUrl, 2, "JOIN")%>
                <%End If%>
                 <%=HeaderUtils.RenderPageTitle(23, "Funding", false)%>
               
                <div id='DivDepositInput'>
                    <%=HeaderUtils.RenderPageTitle(0, "How much do you want to deposit?", True) %>
                    <div id="spProductAmount" style="padding-left: 30px;"></div>
					
                    <div class="LineSpace" id="divDepositProductPool"></div>
					<%If _MembershipFee > 0 Or _customListFomFees.Count > 0 Then%>
					<div data-role="fieldcontain">
						 <label for="divMembershipFee">One-time Membership Fee</label>
						 <div id="divMembershipFee"> <%=_MembershipFee.ToString("C2", New CultureInfo("en-US")) %></div>
					</div>
					<%End If%>
                    <div data-role="fieldcontain">
                        <label for="txtTotalDeposit" style="font-weight:700" class="RequiredIcon">Total Deposit</label>
                        <input type="<%=_textAndroidTel%>" id="txtTotalDeposit" class="money" readonly="readonly" style="font-weight: bold;" />
                    </div>
                </div>
                <div class='labeltext-bold' id='divFundingSource' style="margin-bottom: 15px">
                </div>
                <uc:xaFundingOptions ID="xaFundingOptions" runat="server" />
            </div>
            <div class ="div-continue"  data-role="footer">
                <a href="#" data-transition="slide" onclick="validateActivePage(this, true);" type="button" class="div-continue-button">Continue</a> 
                <a href="#divErrorDialog" style="display: none;">no text</a>
                Or <a href="#" onclick="goBack(this, null, '<%=pageBeforeFS %>');" class ="div-goback" data-corners="false" data-shadow="false" data-theme="reset"><span class="hover-goback"> Go Back</span></a>
            </div>
            <!--Popup content for question mark -->
            <div id="popupQuestionMark" data-role="popup">
                <div data-role="content">
                    <div class="row">
                        <div class="col-xs-12 header_theme2">
                            <a data-rel="back" class="pull-right svg-btn" href="#"><%=HeaderUtils.IconClose %><span style="display: none;">close</span></a>
                        </div>
					 </div>
					 <div class="row">
                        <div class="col-sm-12">
                            <div style="text-align: center; margin: 10px 0;">
                                <img src="/images/SampleCC.jpg" alt="sampleCC" style="width: 100%;"/>
                            </div>     
                        </div>    
                    </div>
                </div>
            </div>            
        </div>


        <div data-role="dialog" data-close-btn="none" id="fundingInternalTransfer">
            <div data-role="header" data-position="" style="display: none;">
                <h1>Fund via Internal Transfer</h1>
            </div>
            <div data-role="content">
                <div class="row">
                    <div class="col-xs-12 header_theme2" style="padding-right: 0px;">
                        <a href="#" onclick="goBackFInternalTransfer(this)" class="pull-right svg-btn"><%=HeaderUtils.IconClose %><span style="display: none;">close</span></a>
                    </div>
                </div>
                <div data-placeholder="content"></div>
                <div class="div-continue" style="text-align: center;">
                    <a href="#" data-transition="slide" onclick="validateFInternalTransfer(this, true);" data-role="button" type="button" class="div-continue-button">Done</a> 
                    <a href="#divErrorDialog" style="display: none;">no text</a>
                  </div>
            </div>
        </div>
		<%If hasSSOAccount Then%>
		<div data-role="dialog" data-close-btn="none" id="fundingSelectAccount">
            <div data-role="header" data-position="" style="display: none;">
                <h1>Fund via Internal Transfer</h1>
            </div>
            <div data-role="content">
                <div class="row">
                    <div class="col-xs-12 header_theme2" style="padding-right: 0px;">
                        <a href="#" onclick="goBackFSelectAccount(this)" class="pull-right svg-btn"><%=HeaderUtils.IconClose %><span style="display: none;">close</span></a>
                    </div>
                </div>
                <div data-placeholder="content"></div>
                <div class="div-continue" style="text-align: center;">
                    <a href="#" data-transition="slide" onclick="validateFSelectAccount(this, true);" data-role="button" type="button" class="div-continue-button">Done</a> 
                    <a href="#divErrorDialog" style="display: none;">no text</a>
                  </div>
            </div>
        </div>
		<%End If%>
        <div data-role="dialog" data-close-btn="none" id="fundingCreditCard">
            <div data-role="header" data-position="" style="display: none;">
                <h1>Fund via Credit Card</h1>
            </div>
            <div data-role="content">
                <div class="row">
                    <div class="col-xs-12 header_theme2" style="padding-right: 0px;">
                        <a href="#" onclick="goBackFCreditCard(this)" class="pull-right svg-btn"><%=HeaderUtils.IconClose %><span style="display: none;">close</span></a>
                    </div>
                </div>
                <div data-placeholder="content" style="overflow: auto; padding: 4px;"></div>
                <div class="div-continue" style="text-align: center;">
                    <a href="#" data-transition="slide" onclick="validateFCreditCard(this);" type="button" data-role="button" class="div-continue-button">Done</a> 
                    <a href="#divErrorDialog" style="display: none;">no text</a>
                    <%--Or <a href="#" onclick="goBackFCreditCard(this)" class ="div-goback" data-corners="false" data-shadow="false" data-theme="reset"><span class="hover-goback"> Go Back</span></a>   --%>
                </div>
            </div>
        </div>
		<div id="popUpUndefineCard" data-role="popup" style="max-width: 500px; padding: 20px;">
			<div data-role="header" style="display:none" >
					<h1>Alert Popup</h1>
			</div>
			<div data-role="content">
				<div class="DialogMessageStyle">There was a problem with your request</div>
				<div style="margin: 20px 0 30px; font-weight: normal;" data-place-holder="content"></div>
			</div>
			<div class="div-continue js-no-required-field" style="text-align: center;margin-bottom: 30px;">
				<a href="#" data-transition="slide" type="button" onclick="closePopup('#popUpUndefineCard')" data-role="button" class="div-continue-button" style="display: inline;">Let Me Fix</a>
				<a href="#" data-transition="slide" type="button" data-command="remove-product" data-role="button" class="div-continue-button" style="display: inline;" onclick="validateFCreditCard('#fundingCreditCard', true)">Continue</a>
			</div>
		</div>
        <div data-role="dialog" data-close-btn="none" id="fundingFinancialIns">
            <div data-role="header" data-position="" style="display: none;">
                <h1>Transfer from another finacial institution</h1>
            </div>
            <div data-role="content">
                <div class="row">
                    <div class="col-xs-12 header_theme2" style="padding-right: 0px;">
                        <a href="#" onclick="goBackFFinacialIns(this)" class="pull-right svg-btn"><%=HeaderUtils.IconClose %><span style="display: none;">close</span></a>
                    </div>
                </div>
                <div data-placeholder="content"></div>
                <div class="div-continue" style="text-align: center;">
                    <a href="#" data-transition="slide" onclick="validateFFinacialIns(this, true);" type="button" data-role="button" class="div-continue-button">Done</a> 
                    <a href="#divErrorDialog" style="display: none;">no text</a>
                    <%--Or <a href="#" onclick="goBackFFinacialIns(this)" class ="div-goback" data-corners="false" data-shadow="false" data-theme="reset"><span class="hover-goback"> Go Back</span></a>   --%>
                </div>
            </div>
            <div id="popUpAccountGuide" data-role="popup">
                <div data-role="content">
                    <div class="row">
                        <div class="col-xs-12 header_theme2">
                            <a href="#" data-rel="back" class="pull-right svg-btn"><%=HeaderUtils.IconClose %><span style="display: none;">close</span></a>
                        </div>
					 </div>
					<div class="row">
                        <div class="col-sm-12">
                            <div style="text-align: center; margin: 10px 0;">
                                <img src="/images/SampleCC.jpg" alt="samplecc" style="width: 100%;"/>
                            </div>       
                        </div>    
                    </div>
                </div>
            </div>
        </div>
		
		<%--primary or custodian--%>
        <div data-role="page" id="page2">
              <div data-role="header" class="sr-only">
                <h1><%=If(_HasMinorApplicant = "Y", "About Your Custodian or Joint Applicant", "Tell Us About Yourself")%></h1>
            </div>
            <div data-role="content" class="labeltext-bold">
                <%If _Availability = "2" Or _Availability = "2a" Or _Availability = "2b" Or _Availability = "2s" Then%>
                    <%=HeaderUtils.RenderLogoAndSteps(_LogoUrl, 1, "OPEN")%>
                <%Else%>
                    <%=HeaderUtils.RenderLogoAndSteps(_LogoUrl, 1, "JOIN")%>
                <%End If%>
               
                <div class="pageTitle">
                    <%=HeaderUtils.RenderPageTitle(21, "Tell Us About Yourself", False)%>
                </div>
                <div class="minorPageTitle hidden" >
                    <%=HeaderUtils.RenderPageTitle(21, "About Your Custodian or Joint Applicant", False)%>
                </div>
           
                <%=HeaderUtils.RenderPageTitle(18, "Personal Information", True)%>                
                <uc:xaApplicantInfo ID="xaApplicantInfo" runat="server" />
                
				<%-- Beneficiary --%> 
				<%If CheckShowField("divBeneficiary", "", _LoanTypeOther = "") Or (IsInMode("777") AndAlso IsInFeature("visibility")) Then%>              
                <uc:xaBeneficiary runat="server" ID='xaBeneficiary' />
               <%end if %>
                
				<div class="currentAddressTitle">
                    <%=HeaderUtils.RenderPageTitle(14, "Current Physical Address", True)%>
                </div>
                <div class="minorCurrentAddressTitle hidden">
                   <%=HeaderUtils.RenderPageTitle(14, "Current Physical Address", True)%>
                </div>
                <uc:xaApplicantAddress ID="xaApplicantAddress" LoanType="XA" runat="server" />
                <%=HeaderUtils.RenderPageTitle(22, "Contact Information", true)%>
                <uc:xaApplicantContactInfo ID="xaApplicantContactInfo" runat="server" />
                <%If EnableIDSection Then%>
                    <uc:xaApplicantID ID="xaApplicantID" runat="server" />
                <%End If%>
                <%If EnableEmploymentSection Then%>
                    <%--<div id="divEmploymentStatus" >--%>
                        <%--<%=HeaderUtils.RenderPageTitle(17, "About Your Employment", True)%>--%>
                        <uc:xaApplicantEmployment ID="xaApplicantEmployment" runat="server" />
                    <%--</div>--%>
                <%End If%>

                <%--verify minor--%>
                <div id="divESignature" class="HiddenElement">
                    <br />
                    <div id="divMinorName">
                         <label>As the custodian for <span id="txtMinorName" class="VerifyMinor"></span>under the Virginia Uniform Transfers to Minor Act, I make this application on the minors</label>               
                     </div>                  
                    <div class="container">
                        <div class="row eSignature">
                            <div class="col-sm-5">      
                                <input type="checkbox" id="chkESignature"/>        
                               <label for="chkESignature" id="lblESignature"> <span class="RequiredIcon">eSignature</span>
                               </label>
                            </div>
                            <div class="col-sm-2"></div>
                            <div class="col-sm-5"> 
                                <div id="eSignDate" style="padding-top:15px" class="HiddenElement">  
                                    <label>Date:<span id="spDate" class="VerifyMinor"></span></label>
                               </div> </div> 
                        </div>
                    </div>                     
                </div>  <%--end verify minor --%>
               
                <uc:DocUpload ID="ucDocUpload" IDPrefix="" runat="server"/>
				<uc:xaApplicantQuestion ID="xaApplicantQuestion" LoanType="XA" IDPrefix="" CQLocation="ApplicantPage" runat="server" />				
            </div>
            <div class ="div-continue"  data-role="footer">
                <%-- 
                <a href="#"  data-transition="slide" onclick="validateActivePage(this, true); UpdateCSRFNumber(hfCSRF);" type="button" class="div-continue-button">Continue</a> 
                <a href="#divErrorDialog" style="display: none;">no text</a>
               Or <a href="#" onclick="goBack(this);" class ="div-goback" data-corners="false" data-shadow="false" data-theme="reset"><span class="hover-goback"> Go Back</span></a>   
            --%>
             <%If _enableJoint Then%>           
                <a  type="button" class="div-continue-button div-continue-coapp-button" href="#"  id="continueWithoutCoApp"  data-transition="slide" onclick="validateActivePage(this, true); UpdateCSRFNumber(hfCSRF);" >Continue  without Co-Applicant</a>
                <a  type="button" class="div-continue-button div-continue-coapp-button" href="#" id="continueWithCoApp"   data-transition="slide" onclick="validateActivePage(this, true); UpdateCSRFNumber(hfCSRF);" >Continue with Co-Applicant</a>                       
             <%Else%>
                <a href="#" data-transition="slide" onclick="validateActivePage(this, true); UpdateCSRFNumber(hfCSRF);"  type="button" class="div-continue-button">Continue</a> 
            <%End If%>
                <a href="#divErrorDialog" style="display: none;">no text</a>
               <span> Or</span>  <a href="#" onclick="goBack(this);" class ="div-goback" data-corners="false" data-shadow="false" data-theme="reset"><span class="hover-goback"> Go Back</span></a>
            </div>
        </div>
		<div data-role="page" id="co_scandocs" runat="server" >
             <div data-role="header"  style="display:none" >
                <h1>Co-Driver's License Scan</h1>
            </div>
            <div data-role="content">
	            <%If LaserDLScanEnabled Then%>
				<div class="js-laser-scan-container">
				<a data-rel="back" href="#" class="svg-btn btn-close-dialog"><%=HeaderUtils.IconClose %><span style="display: none;">close</span></a> 
				<uc:LaserDriverLicenseScan ID="coAppLaserDriverlicenseScan" IDPrefix="co_" runat="server" />
				</div>
				<%End If %>
				<div class="js-legacy-scan-container <%=IIf(LaserDLScanEnabled, "hidden", "") %>" >             
					<a data-rel="back" href="#" class="pull-right svg-btn"><%=HeaderUtils.IconClose %><span style="display: none;">close</span></a>    
					<uc:driverLicenseScan ID="xaCoDriverlicenseScan" IDPrefix="co_"  runat="server" />
					<div class ="div-continue" style="text-align: center;">
					<a href="#"  data-transition="slide" onclick="ScanAccept('co_');" type="button" data-role="button" class="div-continue-button">Done</a> 
					<a href="#divErrorDialog" style="display: none;">no text</a>               
					</div>
				</div>
            </div>			          
        </div>
        <div data-role="dialog" id="scandocs" runat="server">
            <div data-role="header"  style="display:none" >
                <h1>Driver's License Scan</h1>
            </div>
            <div data-role="content">
	            <%If LaserDLScanEnabled Then%>
				<div class="js-laser-scan-container">
				<a data-rel="back" href="#" class="svg-btn btn-close-dialog"><%=HeaderUtils.IconClose %><span style="display: none;">close</span></a> 
				<uc:LaserDriverLicenseScan ID="laserDriverlicenseScan" IDPrefix="" runat="server" />
				</div>
				<%End If %>
				<div class="js-legacy-scan-container <%=IIf(LaserDLScanEnabled, "hidden", "") %>" >
					<a data-rel="back" href="#" class="pull-right svg-btn"><%=HeaderUtils.IconClose %><span style="display: none;">close</span></a>                
					<uc:driverLicenseScan ID="xaDriverlicenseScan" runat="server" />
					<div class ="div-continue" style="text-align: center;">
						<a href="#"  data-transition="slide" onclick="ScanAccept('');" type="button" data-role="button" class="div-continue-button">Done</a> 
						<a href="#divErrorDialog" style="display: none;">no text</a>              
					</div>
				</div>
            </div>
			<div id="popScanZipWarning" data-role="popup" style="max-width: 400px;">
				<div data-role="content">
					<div class="row">
						<div class="col-xs-12 header_theme2">
							<a data-rel="back" class="pull-right svg-btn" href="#" ><%=HeaderUtils.IconClose %><span style="display: none;">close</span></a>
						</div>
					</div>
					<div class="row">
						<div class="col-sm-12">
							<div style="margin: 10px 0; font-weight: normal;">System can't prefill due to ZIP code discrepancy. Please proceed by manually entering the information or correct the value in the ZIP field in the 1st page.</div>     
						</div>    
					</div>
				</div>
				<div class="div-continue" style="text-align: center;"><a href="#" data-transition="slide" type="button" onclick="closePopup('#popScanZipWarning')" data-role="button" class="div-continue-button">OK</a></div>
			</div>
        </div>
		
        <div data-role='page' id="pageMinorInfo">
              <div data-role="header" class="sr-only">
                <h1>Minor Applicant Information</h1>
            </div>
            <div data-role="content">
                <%If _Availability = "2" Or _Availability = "2a" Or _Availability = "2b" Or _Availability = "2s" Then%>
                    <%=HeaderUtils.RenderLogoAndSteps(_LogoUrl, 1, "OPEN")%>
                <%Else%>
                    <%=HeaderUtils.RenderLogoAndSteps(_LogoUrl, 1, "JOIN")%>
                <%End If%>
                 <%=HeaderUtils.RenderPageTitle(21, "Tell Us About Yourself", false)%>
                
               <div id="divMinorApplicant" style="display :<%=_visibleMinorApp%>" class="labeltext-bold">                  
                  <div id="divMinorPersonalInfo">                 
                    <%=HeaderUtils.RenderPageTitle(18, "Minor Information", true)%>
                    <uc:xaApplicantInfo ID="xaMinorApplicantInfo" IDPrefix="m_" runat="server" />
                      <br />
                  </div>
                 
                  <div id="divMinorContactInfo">
                    <%=HeaderUtils.RenderPageTitle(22, "Contact Information", true)%>
                    <uc:xaApplicantContactInfo ID="xaMinorApplicantContactInfo" IDPrefix="m_" runat="server" />
                      <br />
                  </div>

                   <div id="divMinorCurrentAddress">                     
                       <%=HeaderUtils.RenderPageTitle(14, "Current Physical Address", true)%>
                        <uc:xaApplicantAddress ID="xaMinorApplicantAddress" LoanType="XA" IDPrefix="m_" runat="server" />
                       <br />
                   </div>
                  <%If EnableIDSection("m_") Then %>
                    <uc:xaApplicantID ID="xaMinorApplicantID" IDPrefix="m_" Title="Identification Information" runat="server" />
                      <br />
                  <%End If%>
                   <%If EnableEmploymentSection("m_") Then%>
                       <%--<%=HeaderUtils.RenderPageTitle(17, "About Your Employment", True)%>--%>
                       <uc:xaApplicantEmployment ID="xaMinorApplicantEmployment" IDPrefix="m_" runat="server" />
                   <%End If%>
				   <uc:xaApplicantQuestion ID="xaMinorApplicantQuestion" LoanType="XA" IDPrefix="m_" CQLocation="ApplicantPage" runat="server" />
                   
               </div>
             </div>       
			<div class ="div-continue"  data-role="footer">
					<a href="#"  data-transition="slide" onclick="validateActivePage(this, true); UpdateCSRFNumber(hfCSRF);" type="button" class="div-continue-button">Continue</a> 
					<a href="#divErrorDialog" style="display: none;">no text</a>
				   Or <a href="#" class ="div-goback" onclick="goBack(this)" data-corners="false" data-shadow="false" data-theme="reset"><span class="hover-goback"> Go Back</span></a>   
			 </div>     
            
        </div>

		<%--Joint/Co applicant--%>
        <div data-role="page" id="page6">           
              <div data-role="header" class="sr-only">
                <h1>Joint Applicant Information</h1>
            </div>
            <div data-role="content" class="labeltext-bold">
                <%If _Availability = "2" Or _Availability = "2a" Or _Availability = "2b" Or _Availability = "2s" Then%>
                    <%=HeaderUtils.RenderLogoAndSteps(_LogoUrl, 1, "OPEN")%>
                <%Else%>
                    <%=HeaderUtils.RenderLogoAndSteps(_LogoUrl, 1, "JOIN")%>
                <%End If%>
                <%=HeaderUtils.RenderPageTitle(21, "About Your Joint Applicant", false)%>
                <%=HeaderUtils.RenderPageTitle(18, "Personal Information", true)%>
                <%--<%=_OtherLogo%>--%>
                <div id="co_scanDocumentMessage" style="display: none;"><span id="co_spScanDocument" class="require-span"></span><a href="#co_scandocs"> Retake Photo</a></div>
                <uc:xaApplicantInfo ID="xaCoApplicantInfo" IDPrefix="co_" runat="server" />
                <%=HeaderUtils.RenderPageTitle(14, "Current Physical Address", true)%>
                <uc:xaApplicantAddress ID="xaCoApplicantAddress" LoanType="XA" IDPrefix="co_" runat="server" />
                <%=HeaderUtils.RenderPageTitle(22, "Contact Information", true)%>
                <uc:xaApplicantContactInfo ID="xaCoApplicantContactInfo" IDPrefix="co_" runat="server" />
                <%If EnableIDSection("co_") Then%>
                    <uc:xaApplicantID ID="xaCoApplicantID" IDPrefix="co_" runat="server" />
                 <%End If%>
                <%If EnableEmploymentSection("co_") Then%>
                    <%--<div id="co_divEmploymentStatus">--%>
                        <%--<%=HeaderUtils.RenderPageTitle(17, "About Your Employment", true)%>--%>
                        <uc:xaApplicantEmployment ID="xaCoApplicantEmployment" IDPrefix="co_" runat="server" />
                     <%--</div>--%>
                <%End If %>
                <uc:DocUpload ID="ucCoDocUpload" IDPrefix="co_" runat="server"/>
				<uc:xaApplicantQuestion ID="xaCoApplicantQuestion" LoanType="XA" IDPrefix="co_" CQLocation="ApplicantPage" runat="server" />			
            </div>
            <div class ="div-continue"  data-role="footer">
                <a href="#"  data-transition="slide" onclick="validateActivePage(this, true);" type="button" class="div-continue-button">Continue</a> 
                <a href="#divErrorDialog" style="display: none;">no text</a>
               Or <a href="#" onclick="goBack(this);" class ="div-goback" data-corners="false" data-shadow="false" data-theme="reset"><span class="hover-goback"> Go Back</span></a>   
           </div>
           
        </div><%-- end Joint/Co applicant--%>
       <uc:DocUploadSrcSelector ID="ucDocUploadSrcSelector" IDPrefix="" runat="server" />
		<uc:DocUploadSrcSelector ID="ucCoDocUploadSrcSelector" IDPrefix="co_" runat="server" />
		<%--create final review page --%>
        <div data-role="page" id="reviewpage">
              <div data-role="header" class="sr-only">
                <h1>Review and Submit</h1>
            </div>
            <div data-role="content">
                <%If _Availability = "2" Or _Availability = "2a" Or _Availability = "2b" Or _Availability = "2s" Then%>
                    <%=HeaderUtils.RenderLogoAndSteps(_LogoUrl, 3, "OPEN")%>
                <%Else%>
                    <%=HeaderUtils.RenderLogoAndSteps(_LogoUrl, 3, "JOIN")%>
                <%End If%>
                <%=HeaderUtils.RenderPageTitle(19, "Review and Submit", false)%>

                <div id="reviewPanel" class="review-container container-fluid" data-role="fieldcontain">
                    <%--Branch selections --%>
                    <div class="row" style="display :none">
                        <div class="row-title section-heading">
                            <span class="bold">Branch Selection</span>        
                        </div>
                    </div>
                    <div class="row panel BranchSelection"></div>
                    <%--PRODUCT SELECTIONS--%>
                    <div class="row">
                        <div class="row-title section-heading">
                            <span class="bold">Product Selections</span>        
                        </div>
                    </div>
                    <div class="row panel ViewProductSelection"></div>
                    <%--END PRODUCT SELECTIONS--%>
                    <%--OTHER PRODUCT INTEREST--%>
                    <%--<div class="row">
                        <div class="row-title section-heading">
                            <span class="bold">Other Product Interest</span>        
                        </div>
                    </div>
                    <div class="row panel ViewOtherProductInterest"></div>--%>
                    <%--END OTHER PRODUCT INTEREST--%>
                    <%--PRODUCT CUSTOM QUESTION--%>
                    <%--TEMPORARY HIDE THIS SECTION (CHANGE REQUEST #26)--%>
                    <%--<div class="row">
                        <div class="col-xs-6 col-sm-3 text-right row-title">
                            <label>Custom Questions:</label>        
                        </div>
                        <div class="col-xs-6 col-sm-9 text-left row-data"></div>
                    </div>--%>
                     <div class="row" style="display :none">
                        <div class="row-title section-heading">
                            <span class="bold">Product Additional Features</span>        
                        </div>
                    </div>
                    <div class="row panel ViewProductCustomQuestion"></div>
                    <%--END PRODUCT CUSTOM QUESTION--%>
					<%If SelectedSpecialAccountType IsNot Nothing AndAlso SelectedSpecialAccountType.ShowSpecialInfo Then%>
					<div class="row">
                        <div class="row-title section-heading">
                            <span class="bold">Account Information</span>        
                        </div>
                    </div>
                    <div class="row panel" data-section-name="sa-account-info"></div>
					<%End If %>
					
					<%If BusinessAccountMode Then%>
					<div class="row">
                        <div class="row-title section-heading">
                            <span class="bold">Business Account Information</span>        
                        </div>
                    </div>
                    <div class="row panel" data-section-name="ba-account-info"></div>
					<%Dim index As Integer = 0
						For Each role As CXAQueryRoleInfo In BusinessAccountRoleList
							Dim idPrefix = "ba_" & role.roletype.Replace(" ", "") & IIf(role.isjoint, "_joint", "").ToString() & "_" & index%>
                     <%--
					<%=HeaderUtils.RenderPageTitle(0, (IIf(index = 0, "Primary ", "") & "Applicant Information").ToString() & " (" & CEnum.MapBusinessRoleValueToDisplayName(role.roletype) & IIf(role.isjoint, "- Joint", "").ToString() & ")", True)%>
					 --%>
                     <div class="row-title section-heading">
                            <span class="bold"><%=(IIf(index = 0, "Primary ", "") & "Applicant Information").ToString() & " (" & CEnum.MapBusinessRoleValueToDisplayName(role.roletype) & IIf(role.isjoint, "- Joint", "").ToString() & ")"%></span>        
                     </div>
                    <div data-role="<%=idPrefix %>"></div>
					
					<%If index = 0 Then%>
					<%--FUNDING SOURCE --%>
                    <div class="row">
                        <div class="row-title section-heading">
                            <span class ="bold">Funding Source</span>        
                        </div>
                    </div>
                    <div class="row panel ViewFundingSource"></div>
                    <%--END FUNDING SOURCE --%>
					<%End If%>
					<%If index = BusinessAccountRoleList.Count - 1 Then%>
					<%--BENEFICIAL OWNERS--%>
                    <div class="row beneficialOwnersTitle hidden">
	                    <%If BusinessAccountMode Then %>
                             <div class="row-title section-heading">
                                 <span class="bold">Beneficial Owner Information</span>
                             </div>
                         <%else%>
                              <%=HeaderUtils.RenderPageTitle(0, "Beneficial Owner Information", true)%>
                         <% End If %>     
                    </div>
                    <div class="row panel ViewBeneficialOwners hidden"></div>
                    <%--END BENEFICIAL OWNERS --%>
					<%End If%>
					<%
						index += 1
									Next%>
					<%ElseIf SpecialAccountRoleList IsNot Nothing AndAlso SpecialAccountRoleList.Count > 0 Then
						Dim index As Integer = 0
							For Each role As CXAQueryRoleInfo In SpecialAccountRoleList
								Dim idPrefix = "sa_" & role.roletype.Replace(" ", "") & IIf(role.isjoint, "_joint", "").ToString() & "_" & index%>
					<%=HeaderUtils.RenderPageTitle(0, (IIf(index = 0, "Primary ", "") & "Applicant Information").ToString() & " (" & CEnum.MapSpecialRoleValueToDisplayName(role.roletype) & IIf(role.isjoint, "- Joint", "").ToString() & ")", true)%>
					<div data-role="<%=idPrefix %>"></div>
					<%--<div class="row">
                        <div class="row-title section-heading">
                            <span class="bold">Applicant Information</span>        
                        </div>
                    </div>
                    <div class="row panel" data-role="<%=role.RoleType %>" data-section-name="app-info"></div>
					<div class="row">
                        <div class="row-title section-heading">
                            <span class="bold">Address Information</span>        
                        </div>
                    </div>
                    <div class="row panel" data-role="<%=role.RoleType %>" data-section-name="addr-info"></div>
					<div class="row">
                        <div class="row-title section-heading">
                            <span class="bold">Relative Information</span>        
                        </div>
                    </div>
                    <div class="row panel" data-role="<%=role.RoleType %>" data-section-name="relative-info"></div>
					<div class="row">
                        <div class="row-title section-heading">
                            <span class="bold">Employment Information</span>        
                        </div>
                    </div>
                    <div class="row panel" data-role="<%=role.RoleType %>" data-section-name="empl-info"></div>
					<div class="row">
                        <div class="row-title section-heading">
                            <span class="bold">Identification Information</span>        
                        </div>
                    </div>
                    <div class="row panel" data-role="<%=role.RoleType %>" data-section-name="id-info"></div>
					<div class="row">
                        <div class="row-title section-heading">
                            <span class="bold">Additional Information</span>        
                        </div>
                    </div>
                    <div class="row panel" data-role="<%=role.RoleType %>" data-section-name="aq-info"></div>--%>
					<%If index = 0 Then%>
					<%--FUNDING SOURCE --%>
                    <div class="row">
                        <div class="row-title section-heading">
                            <span class="bold">Funding Source</span>        
                        </div>
                    </div>
                    <div class="row panel ViewFundingSource"></div>
                    <%--END FUNDING SOURCE --%>
					<%End If%>
					<%
						index += 1
									Next%>
					<%Else %>
                    <%--APPLICANT INFORMATION --%>
                    <div class="row">
                        <div class="row-title section-heading">
                            <span class="bold">Applicant Information</span>        
                        </div>
                    </div>
                     <div class="row panel ViewAcountInfo"></div>
                    <%--END APPLICANT INFORMATION --%>
					<%If CheckShowField("divBeneficiary", "", _LoanTypeOther = "") Or (IsInMode("777") AndAlso IsInFeature("visibility")) Then%> 
                      <%--Benficiariay --%>
                     <div class="row beneficiaryTitle">
                        <div class="row-title section-heading">
                            <span class ="bold">Beneficiary Information</span>        
                        </div>
                    </div>
                    <div class="row panel ViewBeneficiaries">
                    </div>
					<%End If%>
                    <%--CONTACT INFORMATION --%>
                    <div class="row">
                        <div class="row-title section-heading">
                            <span class="bold">Contact Information</span>        
                        </div>
                    </div>
                    <div class="row panel ViewContactInfo"></div>
                    <%--END CONTACT INFORMATION --%>
                    <%--IDENTIFICATION INFORMATION --%>
					<%If EnableIDSection Then%>
                    <div class="row">
                        <div class="row-title section-heading">
                            <span class="bold">Identification Information</span>        
                        </div>
                    </div>
                    <div class="row panel ViewIdentification"></div>
					<%End If%>
                    <%--END IDENTIFICATION INFORMATION --%>
                    <%--PHYSICAL ADDRESS --%>
                    <div class="row">
                        <div class="row-title section-heading">
                            <span class="bold">Address</span>        
                        </div>
                    </div>
                    <div class="row panel ViewAddress"></div>
                    <%--END PHYSICAL ADDRESS --%>
                    <%--EMPLOYMENT INFORMATION --%>
                   <%If EnableEmploymentSection Then%>
                    <div class="row">
                        <div class="row-title section-heading">
                            <span class="bold">Employment Information</span>        
                        </div>
                    </div>
                    <div class="row panel ViewEmploymentInfo"></div>
					<div class="row">
						<div class="row-title section-heading">
							<span class ="bold">Previous Employment Information</span>        
						</div>
					</div>
					<div class="row panel ViewPrevEmploymentInfo"></div>
                    <%End If %>
                    <%--END EMPLOYMENT INFORMATION --%>
                    <%--CO APPLICANT--%>
                    <div data-section-name="reviewJointApplicant" style="display: <%=IIf(_enableJoint, "block", "none")%>">
                        <div class="row">
                            <div class="row-title section-heading">
                                <span class="bold">Joint Applicant Information</span>
                            </div>
                        </div>
                        <div class="row panel co_ViewAcountInfo"></div>
                        <div class="row">
                            <div class="row-title section-heading">
                                <span class="bold">Joint Applicant Contact Information</span>
                            </div>
                        </div>
                        <div class="row panel co_ViewContactInfo"></div>
						<%If EnableIDSection("co_") Then%>
                        <div class="row">
                            <div class="row-title section-heading">
                                <span class="bold">Joint Applicant Identification</span>
                            </div>
                        </div>
                        <div class="row panel co_ViewIdentification"></div>
						<%End If%>
                        <div class="row">
                            <div class="row-title section-heading">
                                <span class="bold">Joint Applicant Address</span>
                            </div>
                        </div>
                        <div class="row panel co_ViewAddress"></div>
                       <%If EnableEmploymentSection("co_") Then%>
                        <div class="row">
                            <div class="row-title section-heading">
                                <span class="bold">Joint Applicant Employment Information</span>
                            </div>
                        </div>
                        <div class="row panel co_ViewEmploymentInfo"></div>
						<div class="row" style="display: none;">
							<div class="row-title section-heading">
								<span class="bold">Joint Applicant Previous Employment Information</span>        
							</div>
						</div>
						<div class="row panel ViewJointApplicantPrevEmploymentInfo" style="display: none;"></div>
                        <%End If %>
                    </div>
                        <%--END CO APPLICANT--%>
					<%If (SpecialAccountRoleList Is Nothing OrElse SpecialAccountRoleList.Count = 0) Then%>
					<%--FUNDING SOURCE --%>
                    <div class="row">
                        <div class="row-title section-heading">
                            <span class="bold">Funding Source</span>        
                        </div>
                    </div>
                    <div class="row panel ViewFundingSource"></div>
                    <%--END FUNDING SOURCE --%>
					<%End If%>
                    <%--REVIEW MINOR INFORMATION --%>
                <div data-section-name="reviewMinorApplicant" style="display: <%=IIf(_HasMinorApplicant = "Y", "block", "none")%>">
       
                    <div class="row text-center section-heading">
                        <span class="bold">Review Minor Information</span>
                    </div>
                    <%--MINOR INFORMATION --%>
                    <div class="row">
                        <div class="row-title section-heading">
                            <span class="bold">Minor Information</span>        
                        </div>
                    </div>
                    <div class="row panel ViewMinorAcountInfo"></div>
                    <%--END MINOR INFORMATION --%>
                    <%--MINOR CONTACT INFORMATION --%>
                    <div class="row">
                        <div class="row-title section-heading">
                            <span class="bold">Minor Contact Information</span>        
                        </div>
                    </div>
                    <div class="row panel ViewMinorContactInfo"></div>
                    <%--END MINOR CONTACT INFORMATION --%>
                    <%--MINOR IDENTIFICATION INFORMATION --%>
                    <%If EnableIDSection("m_") Then%>
                    <div class="row">
                        <div class="row-title section-heading">
                            <span class="bold">Minor Identification Information</span>        
                        </div>
                    </div>
                    <div class="row panel ViewMinorIdentification"></div>
                    <%End If %>
                    <%--END MINOR IDENTIFICATION INFORMATION --%>
                    <%--MINOR CURRENT PHYSICAL ADDRESS --%>
                    <div class="row">
                        <div class="row-title section-heading">
                            <span class="bold">Minor Physical Address</span>        
                        </div>
                    </div>
                    <div class="row panel ViewMinorAddress"></div>
                    <%--END MINOR CURRENT PHYSICAL ADDRESS --%>
                    <%--MINOR EMPLOYMENT INFORMATION --%>
                   <%If EnableEmploymentSection("m_") Then%>
                    <div class="row">
                        <div class="row-title section-heading">
                            <span class="bold">Minor Employment Information</span>        
                        </div>
                    </div>
                    <div class="row panel ViewMinorEmploymentInfo"></div>
					<div class="row" style="display: none;">
						<div class="row-title section-heading">
							<span class="bold">Minor Previous Employment Information</span>        
						</div>
					</div>
					<div class="row panel ViewMinorPrevEmploymentInfo" style="display: none;"></div>
                    <%End If %>
                    <%--END MINOR EMPLOYMENT INFORMATION --%>
                        <%--View Applicant Questions --%>
                    <div class="row" style="display: none;">
                        <div class="row-title section-heading">
                            <span class="bold">Minor Additional Information</span>        
                        </div>
                     </div>
                    <div class="row panel ViewMinorApplicantQuestion" style="display: none;"></div>
                 </div>

                    <%--View Applicant Questions --%>
                  <div class="row" style="display: none;">
                        <div class="row-title section-heading">
                            <span class="bold">Additional Information</span>        
                        </div>
                    </div>
                    <div class="row panel ViewApplicantQuestion" style="display: none;"></div>
                       <%--view Joint Applicant Questions --%>
					<div class="row panel co_ViewApplicantQuestion"></div>
						<div class="row" style="display: none;">
							<div class="row-title section-heading">
								<span class="bold">Joint Additional Information</span>        
							</div>
					</div>
					<div class="row panel ViewJointApplicantQuestion" style="display: none;"></div>
               
					<%End If%>
                </div>
               
                   <%-- comments field--%>
                <div id="divComments" class="hide-able">
                   <a id="showComments" href="#" data-mode="self-handle-event" status="N" style="display: inline-block; margin-bottom: 8px;" class="header_theme2 shadow-btn plus-circle-before">Have comments</a>
                 	<textarea rows="" cols="" id="comments" style="display: none" maxlength ="300" aria-labelledby="showComments"></textarea>
               </div>
				
				<uc:xaApplicantQuestion ID="xaApplicationQuestionReviewPage" LoanType="XA" IDPrefix="reviewpage_" CQLocation="ReviewPage" runat="server" />
				 
				<div id="divDisclosure">
					<%If EnableDisclosureSection Then%>
<div id="divDisclosureSection" <%=IIf(IsInMode("777") AndAlso IsInFeature("visibility"), "class=""showfield-section"" data-show-field-section-id=""" & BuildShowFieldSectionID("divDisclosureSection") & """ data-section-name= 'Disclosure'", "")%>>
                <%=HeaderUtils.RenderPageTitle(17, "Read, Sign and Submit", true, isRequired:=True)%>
                <div style="margin-top: 10px;">
                    <div id="divSubmitMessage">
					    <%If Not String.IsNullOrEmpty(_CreditPullMessage) Then%>
						    <p class="rename-able bold"><%=_CreditPullMessage%></p>
                         <%Else%>  
						    <p class="rename-able bold">Your application is not complete until you read the disclosure below and click the “I Agree” button in order to submit your application.</p>
						    <p class="rename-able">You are now ready to submit your application! By clicking on "I agree", you authorize us to verify the information you submitted and may obtain your credit report. Upon your request, we will tell you if a credit report was obtained and give you the name and address of the credit reporting agency that provided the report. You warrant to us that the information you are submitting is true and correct. By submitting this application, you agree to allow us to receive the information contained in your application, as well as the status of your application.</p>
                              
                        <%End If%>
					</div>
                    <uc:disclosure ID="ucDisclosures" LoanType="XA" runat="server" Name="disclosures" />                  
                </div>
	</div>
					<%End If%>
				</div>
				
                 <div class ="div-continue"  data-role="footer"> 
                    <a href="#"  id="<%=IIf(_LoanType = "SA", "existing-member-submit", "become-a-member-submit")%>" data-transition="slide" onclick="UpdateCSRFNumber(hfCSRF); return validateForm(this);" class="btnSubmit div-continue-button" type="button" >I Agree</a>  
                   Or <a href="#" onclick="goBack(this, null, '<%=pageBeforeFS %>');" class ="div-goback" data-corners="false" data-shadow="false" ><span class="hover-goback"> Go Back</span></a>   
                     <a id="href_show_last_dialog" href="#xa_last" style="display: none;">no text</a>
                </div>
              </div>
             
        </div>
       
        <div data-role="page" id="walletQuestions">
              <div data-role="header" class="sr-only">
                <h1>Authentication Questions</h1>
            </div>
            <div data-role="content">
                <%If _Availability = "2" Or _Availability = "2a" Or _Availability = "2b" Or _Availability = "2s" Then%>
                    <%=HeaderUtils.RenderLogoAndSteps(_LogoUrl, 3, "OPEN")%>
                <%Else%>
                    <%=HeaderUtils.RenderLogoAndSteps(_LogoUrl, 3, "JOIN")%>
                <%End If%>
                <%=HeaderUtils.RenderPageTitle(19, "Authentication Questions", False)%>
                <div id="walletQuestionsDiv"></div>
                <div class="div-continue" style="margin-top: 10px; text-align: center;" data-role="footer">
                    <%--<input type="button" name="btnSubmitAnswer" value="Submit Answers" onclick="return validateWalletQuestions(this);" class="btnSubmitAnswer" autocomplete="off" />--%>
					<a href="#" data-transition="slide" onclick="return validateWalletQuestions(this);" type="button" class="btnSubmitAnswer div-continue-button">Submit Answers</a> 
                </div>
            </div>
        </div>

        <div data-role="page" id="xa_last">
			   <div data-role="header" class="sr-only">
                <h1>Application Completed</h1>
            </div>
            <div data-role="content">
                <%=HeaderUtils.RenderLogoAndSteps(_LogoUrl, 3)%>
                <div id="div_system_message">
                </div>
                <div id="divCheckMailAddress" style="text-align: left; display: none;">
                    <br />
                    <span class="bold">Mailing a check to fund your account? Please send the check to:</span>
                    <br />
                    <%=_CheckMailAddress%>
                </div>
            </div>
			<%If Not String.IsNullOrEmpty(_RedirectXAURL) Then%>
                <div class="div-continue" style="margin-top: 10px; text-align: center;" data-role="footer">
	                <a id="btnRedirect" href="#" onclick="gotoToUrl(this,'<%=_RedirectXAURL%>')" type="button" class="div-continue-button">Return to our website</a> 
                    <%--<input type="button" id="btnRedirect" class="btn-redirect div-continue-button" url='<%=_RedirectURL%>' value="Return to our website" />--%>
                </div>
                <%End If%>
        </div>


        <div id="divErrorDialog" data-role="dialog" style="min-height: 444px;">
            <div role="dialog">
                <div data-role="header" style="display:none" >
					  <h1>Alert Popup</h1>
			     </div>
                <div data-role="content">
                    <div class="DialogMessageStyle">There was a problem with your request</div>
                    <div style="margin: 5px 0 15px 0;">
                        <span id="txtErrorMessage"></span>
                    </div>
                    <a href="#" data-role="button" data-rel="back" type="button">Ok</a>
                </div>
            </div>
        </div>

		<div id="divErrorPopup" data-role="popup" data-dismissible="true" data-history="false" style="max-width: 500px;padding: 20px;">
			<div data-role="header" style="display:none" >
					<h1>Alert Popup</h1>
			</div>
			<div data-role="content">
				<%--<div class="row">
					<div class="col-xs-12 header_theme2">
						<a data-rel="back" class="pull-right"><%=HeaderUtils.IconClose %></a>
					</div>
				</div>--%>
				<div class="row">
					<div class="col-sm-12">
						<div class="DialogMessageStyle">There was a problem with your request</div>
						<div style="margin: 5px 0 15px 0;">
							<span id="txtErrorPopupMessage"></span>
						</div>       
					</div>    
				</div>
				<div class="row text-center">
					<div class="col-xs-12 text-center">
						<a href="#" data-role="button" data-rel="back" type="button">Ok</a>
					</div>
				</div>
			</div>
		</div>
	<div data-role="dialog" id="divConfirmDialog">
        <div data-role="header" style="display:none" ><h1>Restart Popup</h1></div>
        <div data-role="content">                 
			<div style='margin: 5px 0 15px 0;'><span class="require-span">Restart will clear all data and return to the main page. Are you sure?</span></div>
			<div class='row' style="text-align: center;margin-top: 40px; margin-bottom: 10px;">
				<a href='#' type="button" style="width: 100px; display: inline;padding-left: 20px; padding-right: 20px;" data-role='button' id='btnOk'>Yes</a>
				<a href='#' type="button" style="width: 100px;display: inline;padding-left: 20px; padding-right: 20px;" data-role='button' data-rel='back' id='btnCancel'>No</a>
			</div>
        </div>
    </div>
                 
	<div data-role="page" id="pageMaxAppExceeded">
			  <div data-role="header" class="sr-only">
				<h1>Access Denied</h1>
			</div>
		  <div data-role="content">
				<%=HeaderUtils.RenderLogo(_LogoUrl)%>
			  <br/>
			  <br/>
			  <div>You have exceeded the maximum number of applications per hour. Please try again later.</div>
			  <br/>
				<div class="div-continue" style="margin-top: 10px; text-align: center;" data-role="footer">
					<a href="#" onclick="gotoToUrl(this,'<%=_RedirectXAURL%>')" type="button" class="div-continue-button">Return to our website</a>
				</div>
		   </div>
	 </div>
          <!--old ui footer logo -->   
        <!--
        <div class = "divfooter  ui-bar-<%=_FooterDataTheme%>" >
         <%=_strRenderFooterContact%>    
           <img  id="hdLogoImage" src="<%=_strLogoUrl%>" alt ="" style="display :none;"/>
            <div class="divfooterleft"><div class="divfooterleft-content"><%=_strFooterLeft  %></div></div>          
            <div class="divfooterright">
                <% If String.IsNullOrEmpty(_hasFooterRight) Then%>
                    <div class="divfooterright-content">
                        <div class="divfooterright-content-style"><a href="<%=_strNCUAUrl%>"  class="divfooterright-content-color ui-link" ><%=_strFooterRight%></a></div></div>
                    <div class="divfooterright-logo">
                         <div class="divfooterright-logo-style"><a href="<%=_strHUDUrl%>" class="ui-link"> <img src="<%=_strLogoUrl%>" alt ="" id="logoImage" class="footerimage-style" /></a></div></div> 
                    <div style ="clear:both"></div> 
                <%Else%>
                    <%=_strFooterRight%>
                    <div style ="clear:both"></div> 
                <%End If%>
            </div>
          <div style ="clear:both"></div>            
            </div>  
       -->
        <!--ui new footer content --> 
       <div class="divfooter">
           <%=_strRenderFooterContact%>  
            <div style="text-align :center"> 
                  <!-- lender name -->
                <div class="divLenderName"><%=_strLenderName%></div> 
                <div class="divFooterLogo">             
                    <% If String.IsNullOrEmpty(_hasFooterRight) Then%>
                    <%-- 
                     <div><a href="<%=_strNCUAUrl%>"  class="ui-link" ><%=_strFooterRight%></a><a href="<%=_strHUDUrl%>" class="ui-link"> <img src="<%=_strLogoUrl%>" alt ="logo" class="footerimage-style img-responsive" /></a></div>                 
                   --%>
                    <div class="divfooterlabel"><a href="<%=_strNCUAUrl%>"  class="ui-link" ><%=_strFooterRight%></a> <a href="<%=_strHUDUrl%>" class="ui-link"><%=_equalHousingText%></a></div>                                      
                    <% Else%>
                        <%=_strFooterRight%>
                    <%End If%>
                </div>  
                <div><%=_strFooterLeft  %></div>   
           </div>
           <%-- 
            <div class = "container">            
                 <div class="row divfooter_padding">
                     <!-- left footer content -->
                      <div class="col-sm-7 col-md-7 divfooterleft ">
                          <div class="footerTextAlign">
                          <%=_strFooterLeft  %>
                          </div>
                      </div>
                     <!--right footer content -->
                     <div class="col-sm-5 col-md-5 divfooterright">
                         <div class="row">
                             <% If String.IsNullOrEmpty(_hasFooterRight) Then%>
                                 <div class="col-sm-9 col-md-10 divfooterright-content footerTextAlign">
                                     <a href="<%=_strNCUAUrl%>"  class="ui-link" ><%=_strFooterRight%></a>
                                 </div>
                                 <div class="col-sm-3 col-md-2 divfooterright-logo footerTextAlign">
                                      <a href="<%=_strHUDUrl%>" class="ui-link"> <img src="<%=_strLogoUrl%>" alt ="" class="footerimage-style img-responsive" /></a>
                                 </div>
                             <% Else%>
                                    <%=_strFooterRight%>
                             <%End If%>
                         </div>
                     </div>
                 </div>
            </div>
           --%> 
       </div> 
         <!--end new ui footer content -->   
     </div>   
    

    
	<%:Scripts.Render("~/xa/product")%>
	<%If SpecialAccountMode Or BusinessAccountMode Then%>
		<%:Scripts.Render("~/js/newdocumentscan")%>
	<%End If%>
	<uc:pageFooter ID="ucPageFooter" runat="server" />
	<script type="text/javascript">
		var CUSTOMLISTFOMFEES = <%=JsonConvert.SerializeObject(_customListFomFees)%>;
		$("#reviewpage").on("pagebeforeshow", function(event, ui) {
			ViewInformationPage();
		<%If SelectedSpecialAccountType isnot nothing andalso SelectedSpecialAccountType.ShowSpecialInfo then %>
			$("div[data-section-name='sa-account-info']", "#reviewPanel").html(sa_AccInfoViewSAAccountInfo());
		<%End If %>
		<%If BusinessAccountMode Then%>
			$("div[data-section-name='ba-account-info']", "#reviewPanel").html(ba_BusinessInfoViewBAAccountInfo());
			var beneficialOwnersViewHtml = ba_BeneficialOwnerviewBeneficialOwners();
			if (beneficialOwnersViewHtml != "") {
				$(".ViewBeneficialOwners").html(beneficialOwnersViewHtml);
				$(".ViewBeneficialOwners").removeClass("hidden");
				$(".beneficialOwnersTitle").removeClass("hidden");
			} else {
				$(".ViewBeneficialOwners").addClass("hidden");
				$(".beneficialOwnersTitle").addClass("hidden");
			}
		<%Dim index As Integer = 0
		For Each role As CXAQueryRoleInfo In BusinessAccountRoleList
			Dim idPrefix = "ba_" & role.roletype.Replace(" ", "") & IIf(role.isjoint, "_joint", "").ToString() & "_" & index
			%>
			$("div[data-role='<%=idPrefix%>']", "#reviewPanel").html(<%=idPrefix%>ViewBAAplicantInfo());
			<%
		index += 1
			Next%>
			<%ElseIf SpecialAccountRoleList IsNot Nothing AndAlso SpecialAccountRoleList.Count > 0 Then
			Dim index As Integer = 0
			For Each role As CXAQueryRoleInfo In SpecialAccountRoleList
				Dim idPrefix = "sa_" & role.roletype.Replace(" ", "") & IIf(role.isjoint, "_joint", "").ToString() & "_" & index
			%>
			$("div[data-role='<%=idPrefix%>']", "#reviewPanel").html(<%=idPrefix%>ViewSAAplicantInfo());
			<%
		index += 1
		Next%>
			<%Else%>
			ViewApplicantInformation($('#hdHasCoApplicant').val());
			if ($('#hdHasMinorApplicant').val() == 'Y') {
				ViewMinorInformation();
			}
			<%End If %>
			$("div.review-container div.row-title b").each(function (idx, ele) {
				if (typeof $(ele).data("renameid") == "undefined") {
					var dataId = getDataId();
					$(ele).attr("data-renameid", dataId);
					RENAME_REPOSITORY[dataId] = htmlEncode($(ele).html());
				}
			});
		});
		//move to here from script.js
        function getApplicantInfo() {
            // Bundle up the information that the user has entered so that we can pass it along to the submission function.
            var appInfo = new Object();
            var sProductInfoArray = new Array();
            sProductInfoArray = xaProductFactory.collectProductSelectionData();
            appInfo.SelectedProducts = JSON.stringify(sProductInfoArray);
            appInfo.SelectedProducts = JSON.stringify(sProductInfoArray);
			appInfo.WorkflowSessionID = '<%=WorkflowSessionID%>';
            appInfo.IsSSO = '<%= IIf(IsSSO,"Y","N") %>';
            if ($("#txtLocationPoolCode").length > 0) {
                appInfo.SelectedLocationPool = JSON.stringify(ZIPCODEPOOLLIST[$("#txtLocationPoolCode").val()]);
            }
            //get all disclosures 
            if ($("#hdEnableDisclosureSection").val() === "Y") {
                if (parseDisclosures() != "") {
                    appInfo.Disclosure = parseDisclosures();
                }
            }
            appInfo.PlatformSource = $('#hdPlatformSource').val();
            appInfo.LenderRef = $('#hfLenderRef').val();

            appInfo.BranchID = getBranchId();
            appInfo.LoanOfficerID = $('#hfLoanOfficerID').val();
            appInfo.CSRF = $('#hfCSRF').val();
            appInfo.ReferralSource = $('#hfReferralSource').val();
            //comment
            appInfo.Comments = getComments();
            appInfo.Availability = Common.GetParameterByName("type");
            xaProductFactory.setESignature(appInfo);
            //end minor applicant
            //add UrlParamCustomQuestionAndAnswer if it exist                  
            if (typeof reviewpage_getAllUrlParaCustomQuestionAnswers == "function") {
                var UrlParaCustomQuestions = reviewpage_getAllUrlParaCustomQuestionAnswers();
                //add UrlParamCustomQuestionAndAnswer if it exist
                if (UrlParaCustomQuestions.length > 0) {
                    appInfo.IsUrlParaCustomQuestion = "Y";
                    appInfo.UrlParaCustomQuestionAndAnswers = JSON.stringify(UrlParaCustomQuestions);
                }
            }
        
            if (hasFOM()) {
                setFOM(appInfo);
            }

            //FS.loadValueToData();
            FS1.loadValueToData();
            appInfo.rawFundingSource = JSON.stringify(FS1.Data);

             //get the custom_question_answer including standard xa, business xa and special account
            var oCustomAnswers = loanpage_getAllCustomQuestionAnswers()
                .concat(getAllCustomQuestionAnswers())
                .concat(co_getAllCustomQuestionAnswers())
                .concat(m_getAllCustomQuestionAnswers())
                .concat(reviewpage_getAllCustomQuestionAnswers());
            var oApplicantCustomAnswers = [];     
			<%If SelectedSpecialAccountType IsNot Nothing Then%>
            //var saAccInfo = sa_AccInfoSetSAAccountInfo();
            //appInfo = JSON.stringify(saAccInfo);
            appInfo.saAccountCode = '<%=Common.SafeEncodeString(SelectedSpecialAccountType.AccountCode)%>';
				<%If SelectedSpecialAccountType.ShowSpecialInfo Then %>
            sa_AccInfoSetSAAccountInfo(appInfo);
				<%End If%>
            <%End If %>
           
            <%If BusinessAccountMode Then%>
            appInfo.baAccountType = '<%=Common.SafeEncodeString(SelectedBusinessAccountType.BusinessType)%>';
            appInfo.ba_BeneficialOwner = JSON.stringify(ba_BeneficialOwnergetBeneficialOwners());
            ba_BusinessInfoSetBAAccountInfo(appInfo);
            var baAppRoles = [];
    			<%
    			Dim index As Integer = 0
                For Each pfx In BAPrefixList
    						%>
            baAppRoles.push('<%=pfx%>');
    					<%=pfx%>SetBAApplicantInfo(appInfo);
                //set applicant custom questions for BXA
            if (typeof <%=pfx%>getAllCustomQuestionAnswers != "undefined") {
               oApplicantCustomAnswers = oApplicantCustomAnswers.concat(<%=pfx%>getAllCustomQuestionAnswers());                         
                }
    						<%
		index += 1
	Next%>
                appInfo.baAppRoles = JSON.stringify(baAppRoles);
                <%If BusinessEnableDocUpload then %>
                    var docUploadInfo = {};
                    <% For Each pfx In BAPrefixList %>                    
                        <%=pfx%>SetBADocUploadInfo(docUploadInfo);
                    <%Next%> 
                    if (docUploadInfo.hasOwnProperty("sDocArray") && docUploadInfo.hasOwnProperty("sDocInfoArray")) {
                        appInfo.Image = JSON.stringify(docUploadInfo.sDocArray);
                        appInfo.UploadDocInfor = JSON.stringify(docUploadInfo.sDocInfoArray);
                    }
                <%End If%>
                
			<%ElseIf SAPrefixList IsNot Nothing AndAlso SAPrefixList.Count > 0 Then%>
    			var saAppRoles = [];
                <%
                Dim index As Integer = 0
                For Each pfx In SAPrefixList
    				%>
    				saAppRoles.push('<%=pfx%>');
    				<%=pfx%>SetSAApplicantInfo(appInfo);
    				//appInfo.saAppInfo = JSON.stringify(saAppInfo);
                  //set applicant custom questions for special account
                   if (typeof <%=pfx%>getAllCustomQuestionAnswers != "undefined") {
                        oApplicantCustomAnswers = oApplicantCustomAnswers.concat(<%=pfx%>getAllCustomQuestionAnswers());                         
                    }
    				<%
                    index += 1
                Next%>
    			appInfo.saAppRoles = JSON.stringify(saAppRoles);
                <%If SpecialEnableDocUpload then %>
                    var docUploadInfo = {};
                    <% For Each pfx In SAPrefixList %>                    
                        <%=pfx%>SetSADocUploadInfo(docUploadInfo);
                    <%Next%> 
                    if (docUploadInfo.hasOwnProperty("sDocArray") && docUploadInfo.hasOwnProperty("sDocInfoArray")) {
                        appInfo.Image = JSON.stringify(docUploadInfo.sDocArray);
                        appInfo.UploadDocInfor = JSON.stringify(docUploadInfo.sDocInfoArray);
                    }
                <%End If%>
            <%Else%>          
			//set special account type
			setSpecialAccountType(appInfo);
			//beneficiary information
			if (typeof getBenficiaryInfo != "undefined") {
				appInfo.beneficiaryInfo = JSON.stringify(getBenficiaryInfo());
			}

			
			SetApplicantInfo(appInfo);
			if ($("#hdEnableEmploymentSection").val() === "Y") {
				SetApplicantEmployment(appInfo);
			}
			SetContactInfo(appInfo);
			if ($("#hdEnableIDSection").val() === "Y") {
				SetApplicantID(appInfo);
			}
			SetApplicantAddress(appInfo);
			if ($("#hdEnableOccupancyStatusSection").val() === "Y") {
				SetApplicantPreviousAddress(appInfo);
			}
			SetApplicantMailingAddress(appInfo);

			EMPLogicPI.setValueToAppObject(appInfo);
			appInfo.ApplicantQuestionAnswers = JSON.stringify(getXAApplicantQuestions());
			appInfo.HasJointApplicant = false;
			if ($("#hdHasCoApplicant").val() === "Y") {
				appInfo.HasJointApplicant = true;
				co_SetApplicantInfo(appInfo);
				if ($("#hdEnableEmploymentSectionForCoApp").val() === "Y") {
					co_SetApplicantEmployment(appInfo);
				}
				co_SetContactInfo(appInfo);
				if ($("#hdEnableIDSectionForCoApp").val() === "Y") {
					co_SetApplicantID(appInfo);
				}
				co_SetApplicantAddress(appInfo);
				if ($("#hdEnableOccupancyStatusSectionForCoApp").val() === "Y") {
					co_SetApplicantPreviousAddress(appInfo);
				}
				co_SetApplicantMailingAddress(appInfo);
				appInfo.co_ApplicantQuestionAnswers = JSON.stringify(co_getXAApplicantQuestions());
			}
			//minor Applicant
			appInfo.HasMinorApplicant = $('#hdHasMinorApplicant').val();
			if (appInfo.HasMinorApplicant == 'Y') {
				m_SetApplicantInfo(appInfo);
				if ($("#hdEnableEmploymentSectionForMinor").val() === "Y") {
					m_SetApplicantEmployment(appInfo);
				}
				m_SetContactInfo(appInfo);
				if ($("#hdEnableIDSectionForMinor").val() === "Y") {
					m_SetApplicantID(appInfo);
				}
				m_SetApplicantAddress(appInfo);
				m_SetApplicantMailingAddress(appInfo);
				//get minor applicant questions
				appInfo.m_ApplicantQuestionAnswers = JSON.stringify(m_getXAApplicantQuestions());
			}
			//set UploadDocument
			var docUploadInfo = {};
			if (typeof docUploadObj != "undefined" && docUploadObj != null) {
				docUploadObj.setUploadDocument(docUploadInfo);
			}
			if (typeof co_docUploadObj != "undefined" && co_docUploadObj != null) {
				co_docUploadObj.setUploadDocument(docUploadInfo);
			}
			if (docUploadInfo.hasOwnProperty("sDocArray") /*&& docUploadInfo.sDocArray != null && docUploadInfo.sDocArray.length > 0 */ && docUploadInfo.hasOwnProperty("sDocInfoArray")) {
				appInfo.Image = JSON.stringify(docUploadInfo.sDocArray);
				appInfo.UploadDocInfor = JSON.stringify(docUploadInfo.sDocInfoArray);
			}
			<%End If %>       
            appInfo.CustomAnswers = JSON.stringify(oCustomAnswers.concat(oApplicantCustomAnswers));          
			return appInfo;
		}
		function getWalletAnswers() {
			// Grab the answer choices that have been selected by the user
			var appInfo = new Object();
			//get wallet questions and answers
			getWalletQuestionsAndAnswers(appInfo);
			appInfo.CSRF = $('#hfCSRF').val();
			appInfo.LenderRef = $('#hfLenderRef').val();
			appInfo.WorkflowSessionID = '<%=WorkflowSessionID%>';
			appInfo.Task = "WalletQuestions";
			appInfo.HasMinorApplicant = $('#hdHasMinorApplicant').val();
			var sProductInfoArray = new Array();
			sProductInfoArray = xaProductFactory.collectProductSelectionData();
			appInfo.SelectedProducts = JSON.stringify(sProductInfoArray); 
            appInfo.Availability = Common.GetParameterByName("type");
          	//add UrlParamCustomQuestionAndAnswer if it exist                  
            if (typeof reviewpage_getAllUrlParaCustomQuestionAnswers == "function") {
            	var UrlParaCustomQuestions = reviewpage_getAllUrlParaCustomQuestionAnswers();
            	//add UrlParamCustomQuestionAndAnswer if it exist
            	if (UrlParaCustomQuestions.length > 0) {
            		appInfo.IsUrlParaCustomQuestion = "Y";
            		appInfo.UrlParaCustomQuestionAndAnswers = JSON.stringify(UrlParaCustomQuestions);
            	}
            }  

			appInfo.CustomAnswers = JSON.stringify(
				loanpage_getAllCustomQuestionAnswers()
					.concat(getAllCustomQuestionAnswers())
					.concat(co_getAllCustomQuestionAnswers())
					.concat(m_getAllCustomQuestionAnswers())
					<% For Each pfx In BAPrefixList %>
					.concat(<%=pfx%>getAllCustomQuestionAnswers())
					<% Next %>
					<% For Each pfx In SAPrefixList %>
					.concat(<%=pfx%>getAllCustomQuestionAnswers())
					<% Next %>
					.concat(reviewpage_getAllCustomQuestionAnswers()));
			appInfo.XAQuestionAnswers = JSON.stringify(getXACustomQuestions());    
			//all below line are needed for 2nd_loan, xsell feature
			appInfo.PlatformSource = $('#hdPlatformSource').val();
			appInfo.BranchID = getBranchId();
			appInfo.LoanOfficerID = $('#hfLoanOfficerID').val();
			appInfo.ReferralSource = $('#hfReferralSource').val();
			if ($("#txtLocationPoolCode").length > 0) {
				appInfo.SelectedLocationPool = JSON.stringify(ZIPCODEPOOLLIST[$("#txtLocationPoolCode").val()]);
			}
			<%If SelectedSpecialAccountType IsNot Nothing Then%>
			//var saAccInfo = saAccInfoSetSAAccountInfo();
			//appInfo = JSON.stringify(saAccInfo);
			appInfo.saAccountCode = '<%=Common.SafeEncodeString(SelectedSpecialAccountType.AccountCode)%>';
				<%If SelectedSpecialAccountType.ShowSpecialInfo Then %>
			sa_AccInfoSetSAAccountInfo(appInfo);
			<%End If%>
			<%End If %>
			<%If BusinessAccountMode Then%>
			appInfo.baAccountType = '<%=Common.SafeEncodeString(SelectedBusinessAccountType.BusinessType)%>';
			appInfo.ba_BeneficialOwner = JSON.stringify(ba_BeneficialOwnergetBeneficialOwners());
			ba_BusinessInfoSetBAAccountInfo(appInfo);
			var baAppRoles = [];
				<%
        Dim index As Integer = 0
        For Each pfx In BAPrefixList
						%>
			baAppRoles.push('<%=pfx%>');
			<%=pfx%>SetBAApplicantInfo(appInfo);
						<%
            index += 1
        Next%>
			appInfo.baAppRoles = JSON.stringify(baAppRoles);
			if ($("#hdWalletQuestionApplicantIndex").length > 0) {
				appInfo.WalletQuestionApplicantIndex = $("#hdWalletQuestionApplicantIndex").val();
			}
			if ($("#hdWalletQuestionIterationIndex").length > 0) {
				appInfo.WalletQuestionIterationIndex = $("#hdWalletQuestionIterationIndex").val();
			}
			<%ElseIf SAPrefixList IsNot Nothing AndAlso SAPrefixList.Count > 0 Then%>
			var saAppRoles = [];
				<%
        Dim index As Integer = 0
        For Each pfx In SAPrefixList
				%>
			saAppRoles.push('<%=pfx%>');
			<%=pfx%>SetSAApplicantInfo(appInfo);
			//appInfo.saAppInfo = JSON.stringify(saAppInfo);
				<%
            index += 1
        Next%>
			appInfo.saAppRoles = JSON.stringify(saAppRoles);
			if ($("#hdWalletQuestionApplicantIndex").length > 0) {
				appInfo.WalletQuestionApplicantIndex = $("#hdWalletQuestionApplicantIndex").val();
			}
			if ($("#hdWalletQuestionIterationIndex").length > 0) {
				appInfo.WalletQuestionIterationIndex = $("#hdWalletQuestionIterationIndex").val();
			}
			<%Else%>

			//keep track minor wallet answers for minor, parent, and joint 
			if ($('#hdMinorWalletAnswer').length > 0) {
				appInfo.HasMinorWalletAnswer = $('#hdMinorWalletAnswer').val();
			}
			appInfo.hasCoWalletQuestions = "N";
			if ($('#hasCoWalletQuestions').length > 0) {
				appInfo.hasCoWalletQuestions = $('#hasCoWalletQuestions').val();
			}
			if ($('#hasCoFollowOnQuestions').length > 0) {
				appInfo.hasCoFollowOnQuestions = $('#hasCoFollowOnQuestions').val();
			}
			if ($('#hdIsOnlyMinorApp').length > 0) {
				appInfo.isOnlyMinorApp = $('#hdIsOnlyMinorApp').val();
			}
			//beneficiary information
			if (typeof getBenficiaryInfo != "undefined") {
				appInfo.beneficiaryInfo = JSON.stringify(getBenficiaryInfo());
			}
			//--start regular applicant 
			SetApplicantInfo(appInfo);
			if ($("#hdEnableEmploymentSection").val() === "Y") {
				SetApplicantEmployment(appInfo);
			}
			SetContactInfo(appInfo);
			if ($("#hdEnableIDSection").val() === "Y") {
				SetApplicantID(appInfo);
			}
			SetApplicantAddress(appInfo);
			if ($('#hdEnableOccupancyStatusSection').val() == 'Y') {
				SetApplicantPreviousAddress(appInfo);
			}
			SetApplicantMailingAddress(appInfo);   
			EMPLogicPI.setValueToAppObject(appInfo);
			appInfo.ApplicantQuestionAnswers = JSON.stringify(getXAApplicantQuestions());
			appInfo.HasJointApplicant = false;
			if ($("#hdHasCoApplicant").val() === "Y") {
				co_SetApplicantInfo(appInfo);
				if ($("#hdEnableEmploymentSectionForCoApp").val() === "Y") {
					co_SetApplicantEmployment(appInfo);
				}
				co_SetContactInfo(appInfo);
				if ($("#hdEnableIDSectionForCoApp").val() === "Y") {
					co_SetApplicantID(appInfo);
				}
				co_SetApplicantAddress(appInfo);
				if ($('#hdEnableOccupancyStatusSectionForCoApp').val() == 'Y') {
					co_SetApplicantPreviousAddress(appInfo);
				}
				co_SetApplicantMailingAddress(appInfo);
				appInfo.co_ApplicantQuestionAnswers = JSON.stringify(co_getXAApplicantQuestions());
				appInfo.HasJointApplicant = true;
			}
			//minor Applicant  
			appInfo.HasMinorApplicant = $('#hdHasMinorApplicant').val();
			if (appInfo.HasMinorApplicant == 'Y') {
				setSpecialAccountType(appInfo);
				m_SetApplicantInfo(appInfo);
				if ($("#hdEnableEmploymentSectionForMinor").val() === "Y") {
					m_SetApplicantEmployment(appInfo);
				}
				m_SetContactInfo(appInfo);
				if ($("#hdEnableIDSectionForMinor").val() === "Y") {
					m_SetApplicantID(appInfo);
				}
				m_SetApplicantAddress(appInfo);
				m_SetApplicantMailingAddress(appInfo);
				//get minor applicant questions
				appInfo.m_ApplicantQuestionAnswers = JSON.stringify(m_getXAApplicantQuestions());
			}  
			var docUploadInfo = {};
			if (typeof docUploadObj != "undefined" && docUploadObj != null) {
				docUploadObj.setUploadDocument(docUploadInfo);
			}
			if (typeof co_docUploadObj != "undefined" && co_docUploadObj != null) {
				co_docUploadObj.setUploadDocument(docUploadInfo);
			}
			if (docUploadInfo.hasOwnProperty("sDocArray") /*&& docUploadInfo.sDocArray != null && docUploadInfo.sDocArray.length > 0 */ && docUploadInfo.hasOwnProperty("sDocInfoArray")) {
				appInfo.Image = JSON.stringify(docUploadInfo.sDocArray);
				appInfo.UploadDocInfor = JSON.stringify(docUploadInfo.sDocInfoArray);
			}
			<%End If%>
			return appInfo;
        }
        autofill_BXAApplication();
        autofill_SAApplication();
        function autofill_BXAApplication() {  
                 // Fill in the fields with test data for BXA
            if (getParaByName("autofill") == "true" && (getParaByName("type") == "1b" || getParaByName("type") == "2b")) {
                if (typeof autofill_ba_BusinessInfo != "undefined") {            
                    autofill_ba_BusinessInfo();
                }                        
                <% Dim baIndex As Integer = 0
        For Each pfx In BAPrefixList
            If baIndex > 0 Then ''only auto fill first applicant.
                Exit For
            End If%>
            if (typeof  <%= pfx%>AutoFillData_AppicantInfo != "undefined") {
                <%= pfx%>AutoFillData_AppicantInfo();
            }						        
        <% baIndex += 1
        Next%>
         }
        } 
        function autofill_SAApplication() {
            if (getParaByName("autofill") == "true" && (getParaByName("type") == "1s" || getParaByName("type") == "2s")) {
                    //autofill special account infor page if it exist
                if (($("#pagesa_AccInfo").length > 0)) {
                    if (typeof sa_AccInfoAutofillData != "undefined") {
                        sa_AccInfoAutofillData();
                    }
                }
                //autofill special applicant information
                <%For Each pfx In SAPrefixList%>
                    if (typeof <%=pfx%>AutofillData_SA != "undefined") {
                        <%=pfx%>AutofillData_SA();
                    }    
                <%Next%>
             } 
        }

		/**
		 * Converts the loanInfo, which is normally used on submission, into a data structure meant for Applicant Blocking Logic (ABR).
		 * This also only pulls in the information needed for each page. ABR should only work with data on the page the user is on
		 * and the pages before that.
		 * @param {string} pageId
		 * @param {Object} loanInfo
		 * @param {Object} formValues
		 */
		function collectFormValueData(pageId, loanInfo, formValues) {
			switch (pageId) {
				case "GettingStarted":
					if ($("#txtLocationPoolCode").length > 0) {
						formValues["locationPool"] = ZIPCODEPOOLLIST[$("#txtLocationPoolCode").val()];
					}
					var selectedProducts;
					if (loanInfo.SelectedProducts) {
						selectedProducts = $.parseJSON(loanInfo.SelectedProducts);
					}
					var m = /^([12])([abs])?$/.exec(loanInfo.Availability);
					formValues["accountPosition"] = "1";
					formValues["accountType"] = " "; //personal by default
					if (m) {
						formValues["accountPosition"] = m[1];
						formValues["accountType"] = m[2] || " ";
						if (formValues["accountType"] == "s") {
							formValues["specialAccountType"] = [Common.GetParameterByName("acctype").toUpperCase()];
						} else if (formValues["accountType"] == "b") {
							formValues["businessAccountType"] = [Common.GetParameterByName("acctype").toUpperCase()];
						}
					}
					formValues["productCode"] = [];
					if (selectedProducts && selectedProducts.length > 0) {
						formValues["productCode"] = _.map(selectedProducts, function(p) {
							return p.productName;
						});
						_.forEach(selectedProducts, function(p) {
							if (p.productQuestions && p.productQuestions.length > 0) {
								_.forEach(p.productQuestions, function(q) {
									var qid = q.XAProductQuestionID.replace(/[\s-]/g, "_");
									if (!formValues.hasOwnProperty("PQ_" + qid)) {
										switch (q.AnswerType) {
											case "CHECKBOX":
												var answers = [];
												if (q.RestrictedAnswers && q.RestrictedAnswers.length > 0) {
													answers = _.map(_.filter(q.RestrictedAnswers, { isSelected: true }), function(a) {
														return a.Value;
													});
												}
												formValues["PQ_" + qid] = answers;
												break;
											case "RADIO":
											case "DROPDOWN":
												formValues["PQ_" + qid] = null;
												if (q.RestrictedAnswers && q.RestrictedAnswers.length > 0) {
													var answer = _.find(q.RestrictedAnswers, { isSelected: true });
													if (answer) {
														formValues["PQ_" + qid] = answer.Value;
													}
												}
												break;
											default:
												formValues["PQ_" + qid] = q.productAnswerText;
												break;
										}	
									}
								});
							}
						});
					}

					collectCustomQuestionAnswers("Application", "LoanPage", null, loanInfo, formValues);
					collectCustomQuestionAnswers("Applicant", "LoanPage", null, loanInfo, formValues);

					break;
				case "page2":
                    var _dob = moment(loanInfo.DOB, "MM-DD-YYYY");
                    if (Common.IsValidDate(loanInfo.DOB)) {
						formValues["age"] = moment().diff(_dob, "years", false);
					}
					formValues["citizenship"] = loanInfo.Citizenship;
					formValues["employeeOfLender"] = loanInfo.EmployeeOfLender;
					formValues["employmentLength"] = (parseInt(loanInfo.txtEmployedDuration_year) || 0) + (parseInt(loanInfo.txtEmployedDuration_month) || 0) / 12;
					formValues["employmentStatus"] = loanInfo.EmploymentStatus;
					formValues["isJoint"] = loanInfo.HasJointApplicant;
					formValues["memberNumber"] = loanInfo.MemberNumber;
					formValues["occupancyLength"] = loanInfo.LiveMonths / 12;
					formValues["occupancyStatus"] = loanInfo.OccupyingLocation;

					collectCustomQuestionAnswers("Applicant", "ApplicantPage", "", loanInfo, formValues);

					break;
				case "page6":
                    var _dob = moment(loanInfo.co_DOB, "MM-DD-YYYY");
                    if (Common.IsValidDate(loanInfo.co_DOB)) {
						formValues["age"] = moment().diff(_dob, "years", false);
					}
					formValues["citizenship"] = loanInfo.co_Citizenship;
					formValues["employeeOfLender"] = loanInfo.co_EmployeeOfLender;
					formValues["employmentLength"] = (parseInt(loanInfo.co_txtEmployedDuration_year) || 0) + (parseInt(loanInfo.co_txtEmployedDuration_month) || 0) / 12;
					formValues["employmentStatus"] = loanInfo.co_EmploymentStatus;
					formValues["memberNumber"] = loanInfo.co_MemberNumber;
					formValues["occupancyLength"] = loanInfo.co_LiveMonths / 12;
					formValues["occupancyStatus"] = loanInfo.co_OccupyingLocation;

					collectCustomQuestionAnswers("Applicant", "ApplicantPage", "co_", loanInfo, formValues);

					break;
				case "pageMinorInfo":
                    var _dob = moment(loanInfo.m_DOB, "MM-DD-YYYY");
                    if (Common.IsValidDate(loanInfo.m_DOB)) {
						formValues["age"] = moment().diff(_dob, "years", false);
					}
					formValues["citizenship"] = loanInfo.m_Citizenship;
					formValues["employeeOfLender"] = loanInfo.m_EmployeeOfLender;
					formValues["employmentLength"] = (parseInt(loanInfo.m_txtEmployedDuration_year) || 0) + (parseInt(loanInfo.m_txtEmployedDuration_month) || 0) / 12;
					formValues["employmentStatus"] = loanInfo.m_EmploymentStatus;
					formValues["memberNumber"] = loanInfo.m_MemberNumber;
					formValues["occupancyLength"] = loanInfo.m_LiveMonths / 12;
					formValues["occupancyStatus"] = loanInfo.m_OccupyingLocation;

					collectCustomQuestionAnswers("Applicant", "ApplicantPage", "m_", loanInfo, formValues);

					break;
					<%If BusinessAccountMode Then
						For Each pfx In BAPrefixList%>
				case "page<%=pfx%>":
                    var _dob = moment(loanInfo.<%=pfx%>DOB, "MM-DD-YYYY");
					if (Common.IsValidDate(loanInfo.<%=pfx%>DOB)) {
						formValues["age"] = moment().diff(_dob, "years", false);
					}
					formValues["citizenship"] = loanInfo.<%=pfx%>CitizenshipStatus;
					formValues["employeeOfLender"] = loanInfo.<%=pfx%>EmployeeOfLender;
					formValues["employmentLength"] = (parseInt(loanInfo.<%=pfx%>txtEmployedDuration_year) || 0) + (parseInt(loanInfo.<%=pfx%>txtEmployedDuration_month) || 0) / 12;
					formValues["employmentStatus"] = loanInfo.<%=pfx%>EmploymentStatus;
					formValues["isJoint"] = (loanInfo.<%=pfx%>IsJoint == "Y");
					formValues["memberNumber"] = loanInfo.<%=pfx%>MemberNumber;
					formValues["occupancyLength"] = loanInfo.<%=pfx%>LiveMonths / 12;
					formValues["occupancyStatus"] = loanInfo.<%=pfx%>OccupyingLocation;

					collectCustomQuestionAnswers("Applicant", "ApplicantPage", "<%=pfx%>", loanInfo, formValues);

					break;
						<%Next%>
					<%ElseIf SAPrefixList IsNot Nothing AndAlso SAPrefixList.Count > 0 Then
			For Each pfx In SAPrefixList%>
				case "page<%=pfx%>":
                    var _dob = moment(loanInfo.<%=pfx%>DOB, "MM-DD-YYYY");
					if (Common.IsValidDate(loanInfo.<%=pfx%>DOB)) {
						formValues["age"] = moment().diff(_dob, "years", false);
					}
					formValues["citizenship"] = loanInfo.<%=pfx%>CitizenshipStatus;
					formValues["employeeOfLender"] = loanInfo.<%=pfx%>EmployeeOfLender;
					formValues["employmentLength"] = (parseInt(loanInfo.<%=pfx%>txtEmployedDuration_year) || 0) + (parseInt(loanInfo.<%=pfx%>txtEmployedDuration_month) || 0) / 12;
					formValues["employmentStatus"] = loanInfo.<%=pfx%>EmploymentStatus;
					formValues["isJoint"] = (loanInfo.<%=pfx%>IsJoint == "Y");
					formValues["memberNumber"] = loanInfo.<%=pfx%>MemberNumber;
					formValues["occupancyLength"] = loanInfo.<%=pfx%>LiveMonths / 12;
					formValues["occupancyStatus"] = loanInfo.<%=pfx%>OccupyingLocation;

					collectCustomQuestionAnswers("Applicant", "ApplicantPage", "<%=pfx%>", loanInfo, formValues);

					break;
					<%Next%>
					<%End If%>
				case "reviewpage":

					collectCustomQuestionAnswers("Application", "ReviewPage", null, loanInfo, formValues);
					collectCustomQuestionAnswers("Applicant", "ReviewPage", null, loanInfo, formValues);

					break;
				default:
					break;
			}
			return formValues;
		}
		function prepareAbrFormValues(pageId) {
			var loanInfo = getApplicantInfo();
			var formValues = {};
			_.forEach(pagePaths, function (p) {
				collectFormValueData(p.replace("#", ""), loanInfo, formValues);
				if (pageId == p.replace("#", "")) return false;
			});
			return formValues;
		}

		$(function() {
			initXAModule({collectJobTitleOnly: <%=IIf(CollectJobTitleOnly, "true", "false")%>});
		});
	</script>
</body>
</html>