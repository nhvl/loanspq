﻿
Imports System.Xml
Imports System.IdentityModel
Imports CustomApplicationScenarios
Imports LPQMobile.BO
Imports LPQMobile.Utils
Imports System.Web.Script.Serialization
Imports Newtonsoft.Json
Imports System.Linq
Imports System.Collections.Generic

Partial Public Class CXpressApp
	Inherits CBasePage

	Protected _ProductPackageJsonString As String = ""
	Protected _FundingSourcePackageJsonString As String = ""
	Protected _address_key As String = ""

	Protected _CCMaxFunding As String = ""
	Protected _ACHMaxFunding As String = ""
	Protected _PaypalMaxFunding As String = ""
	Protected _CheckMailAddress As String = ""
	Protected _HasEnableSecondaryUpLoadDocs As String = ""

	''minor applicant
	Protected _HasMinorApplicant As String = ""
	Protected _visibleMinorApp As String = ""
	Protected _Availability As String = "" 'PRIMARY/SECONDARY
	Protected _minorAccountList As String = ""
	Protected _minoProductList As String = ""
	''credit pull message
	Protected _CreditPullMessage As String = ""

	Protected _RenderSpecialAccountType As String = ""
	Protected _enableJoint As Boolean = True

	Protected _eligibilityMessage As String = ""
#Region "SSO"
	'Ex:DI, 
	'Protected ReadOnly Property PlatformSource() As String
	'	Get
	'		Return Common.SafeString(Request.Params("platform_source"))
	'	End Get
	'End Property
#End Region
	Protected _xaRequiredBranch As String = ""
	Protected _xaBranchOptionsStr As String = ""
	Protected _xaBranchZipPoolName As New Dictionary(Of String, String)
	Protected _xaStopInvalidRoutingNumber As String = ""
	Protected _xaFundingAccountNumberFreeForm As String = ""
	Protected LaserDLScanEnabled As Boolean = False
	Protected LegacyDLScanEnabled As Boolean = False
	Protected SpecialAccountMode As Boolean = False
	Protected BusinessAccountMode As Boolean = False
	Protected SpecialEnableDocUpload As Boolean = False
	Protected BusinessEnableDocUpload As Boolean = False
	Protected SpecialAccountRoleList As List(Of CXAQueryRoleInfo)
	Protected BusinessAccountRoleList As List(Of CXAQueryRoleInfo)
	Protected AfterGettingStartedPage As String
	Protected SelectedSpecialAccountType As CSpecialAccountType
	Protected SelectedBusinessAccountType As CBusinessAccountType
	Protected SAPrefixList As New List(Of String)
	Protected BAPrefixList As New List(Of String)
	Protected PrimaryApplicantInfoPrefix As String = ""
	Protected NoProductAvailable As Boolean = False
	Protected pageBeforeFS As String = ""
	Protected hasSSOAccount As Boolean = False
	Protected UrlZipMode As String = ""
	Protected CollectJobTitleOnly As Boolean = False
	Protected WorkflowSessionID As Guid
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
		If Not IsPostBack Then
			'Temporary redirect for clients using type=1b/2b, this to be remove in 2019
			If Common.SafeString(Request.Params("type")).ToLower = "1b" AndAlso String.IsNullOrEmpty(Common.SafeString(Request.Params("acctype"))) Then
				log.Debug("Temporary redirect for clients using type=1b")
				Response.Redirect("/apply.aspx?lenderref=" & _CurrentLenderRef & "&list=xa1s", True)
			ElseIf Common.SafeString(Request.Params("type")).ToLower = "2b" And String.IsNullOrEmpty(Common.SafeString(Request.Params("acctype"))) Then
				log.Debug("Temporary redirect for clients using type=2b")
				Response.Redirect("/apply.aspx?lenderref=" & _CurrentLenderRef & "&list=sa2s", True)
			End If

			Try
				WorkflowSessionID = Guid.NewGuid()
				' log.Info("XA: Page loading")
				Session("StartedDate_" & WorkflowSessionID.ToString()) = DateTime.Now	'this is use for keeping track the time when page first load which will be used to compare with submite_date
				ucPageHeader._currentConfig = _CurrentWebsiteConfig
				'' clear any session related to PAYPAL stuff, this is different from  session("APPROVED_MESSAGE")
				''set token 
				If Session("token") IsNot Nothing And Session("token_count") = Nothing Then	'reuse the same token if user hasn't submit yet, occur when user open multiple windows
					_token = Common.SafeString(Session("token"))
				Else
					_token = GetToken()
				End If
				hasSSOAccount = Not String.IsNullOrEmpty(Common.SafeString(Request.Params("acct_num0")))
				Dim sAvailability As String = Common.SafeString(Request.Params("type")).ToLower	'primary/secondary  
				UrlZipMode = Common.SafeString(Request.Params("zip"))
				If UrlZipMode <> "" Then
					If Not Regex.IsMatch(UrlZipMode, "^\d{5}$|^\d{5}-\d{4}$") Then UrlZipMode = "" ''invalid zip code reset to empty string
					If UrlZipMode.Length > 5 Then UrlZipMode = UrlZipMode.Substring(0, 5) ''only use five digits - xxxxx-xxxx
				End If
				If sAvailability = "1a" Or sAvailability = "2a" Then
					_HasMinorApplicant = "Y"
					_visibleMinorApp = "block;"
					AfterGettingStartedPage = "pageMinorInfo"
				Else
					_visibleMinorApp = "none;"
					_HasMinorApplicant = "N"
					AfterGettingStartedPage = "page2"
				End If
				Select Case sAvailability
					Case "1"
						_Availability = "1"
					Case "2"
						_Availability = "2"
					Case "1a"
						_Availability = "1a"
					Case "2a"
						_Availability = "2a"
					Case "1b"
						_Availability = "1b"
						BusinessAccountMode = True
					Case "2b"
						_Availability = "2b"
						BusinessAccountMode = True
					Case "1s"
						_Availability = "1s"
						SpecialAccountMode = True
					Case "2s"
						_Availability = "2s"
						SpecialAccountMode = True
					Case Else
						_Availability = "1"
				End Select

				'process scenarios if any
				Dim scenario As CCustomApplicationScenarioItem = Nothing
				Dim scenarioId As String = Common.SafeString(Request.Params("scenario"))
				If Not String.IsNullOrWhiteSpace(scenarioId) Then
					scenario = GetXACustomApplicationScenario(scenarioId)
					If scenario IsNot Nothing Then
						If scenario.IsSecondaryAccount AndAlso _Availability.StartsWith("1") Then
							'invalid request, because current scenario is configured to run with secondary account but the "type" param goes with primary account
							scenario = Nothing
						Else
							If (scenario.AccountType = AccountType.Business AndAlso BusinessAccountMode) OrElse (scenario.AccountType = AccountType.Special AndAlso SpecialAccountMode) OrElse (scenario.AccountType = AccountType.Personal AndAlso Not BusinessAccountMode AndAlso Not SpecialAccountMode) Then
								'valid, do nothing
							Else
								scenario = Nothing
							End If
						End If
					End If
				End If
				'end process scenarios




				''displace eligibility message for new member even there is no FOMPage 
				If sAvailability <> "2" And sAvailability <> "2a" And sAvailability <> "2b" And sAvailability <> "2s" Then
					_eligibilityMessage = EligibilityMessage(sAvailability)
				End If
				'override default so it cann be called from design page
				If _RedirectXAURL <> "" Then _RedirectURL = _RedirectXAURL
				''check the citizenship from download
				Dim hasBlankDefaultCitizenship As Boolean = False
				Dim oCitizenshipNode As XmlNode = _CurrentWebsiteConfig._webConfigXML.SelectSingleNode("XA_LOAN/BLANK_DEFAULT_CITIZENSHIP")
				If oCitizenshipNode IsNot Nothing Then
					hasBlankDefaultCitizenship = True
				End If
				Dim citizenshipStatusDropdown As String = ""
				'change the default US Citizen 
				If _CurrentWebsiteConfig.GetEnumItems("CITIZENSHIP_TYPES").Count > 0 Then  'available in  LPQMobileWebsiteConfig
					Dim citizenships = _CurrentWebsiteConfig.GetEnumItems("CITIZENSHIP_TYPES")
					citizenshipStatusDropdown = Common.DefaultCitizenShipFromConfig(citizenships, _DefaultCitizenship, _CurrentLenderRef, hasBlankDefaultCitizenship)
				Else
					citizenshipStatusDropdown = Common.DefaultCitizenShip(_DefaultCitizenship, _CurrentLenderRef, hasBlankDefaultCitizenship) ''from hard code
				End If
				Dim stateDropdown As String = Common.RenderStateDropdownlistWithEmpty(CEnum.STATES, "", "--Please Select--")
				Dim maritalStatusDropdown As String = Common.RenderDropdownlistWithEmpty(CEnum.MARITAL_STATUS, "", "--Please Select--")
				'deprecated,use employee  of member instead
				'Dim officialFamilyDropdown As String = Common.RenderDropdownlist(CEnum.OFFICIAL_FAMILY_OPTIONS)
				Dim occupyingLocationDropdown As String = Common.RenderDropdownlistWithEmpty(CEnum.OCCUPY_LOCATIONS, " ", "--Please Select--")
				Dim collectDescIfOccupancyStatusIsOther As String = Common.getNodeAttributes(_CurrentWebsiteConfig, "XA_LOAN/BEHAVIOR", "collect_description_if_occupancy_status_is_other")
				Dim liveYearsDropdown As String = Common.GetYears()
				Dim liveMonthsDropdown As String = Common.GetMonths()
				Dim bIsSSO As Boolean = Not String.IsNullOrEmpty(Common.SafeString(Request.Params("FName"))) '
				Dim relationToPrimaryApplicantDropdown As String = ""
				Dim relationshipToPrimaryList As Dictionary(Of String, String) = _CurrentWebsiteConfig.GetEnumItems("RELATIONSHIP_PRIMARY")
				If relationshipToPrimaryList IsNot Nothing AndAlso relationshipToPrimaryList.Any() Then
					relationToPrimaryApplicantDropdown = Common.RenderDropdownlistWithEmpty(relationshipToPrimaryList, "", "--Please Select--")
				End If

				Dim preferContactMethods As Dictionary(Of String, String) = _CurrentWebsiteConfig.GetEnumItems("PREFERRED_CONTACT_METHODS")
				preferContactMethods = preferContactMethods.ToDictionary(Function(kv) kv.Key.NullSafeToUpper_, Function(kv) kv.Value)
				Dim preferContactMethodDropdown As String = ""
				If preferContactMethods IsNot Nothing AndAlso preferContactMethods.Any() Then
					preferContactMethodDropdown = Common.RenderDropdownlist(preferContactMethods, "")
				End If
				Dim xaCollectDescriptionIfEmploymentStatusIsOther = Common.getNodeAttributes(_CurrentWebsiteConfig, "XA_LOAN/BEHAVIOR", "collect_description_if_employment_status_is_other")
				Dim employmentStatusDropdown As String = Common.RenderDropdownlistWithEmpty(CEnum.EMPLOYMENT_STATUS, "", "--Please Select--")
				Dim IDCardTypes As Dictionary(Of String, String) = _CurrentWebsiteConfig.GetEnumItems("ID_CARD_TYPES")
				If IDCardTypes Is Nothing OrElse IDCardTypes.Count = 0 Then
					IDCardTypes = CFOMQuestion.DownLoadIDCardTypes(_CurrentWebsiteConfig)
				End If
				Dim employeeOfLenderDropdown As String = ""
				Dim employeeOfLenderType = _CurrentWebsiteConfig.GetEnumItems("EMPLOYEE_OF_LENDER_TYPE")
				If employeeOfLenderType IsNot Nothing AndAlso employeeOfLenderType.Count > 0 Then	 'available in  LPQMobileWebsiteConfig
					''get employee of lender type from config
					employeeOfLenderDropdown = Common.RenderDropdownlist(employeeOfLenderType, "")
				End If

				Dim collectJobTitleOnlyNode As XmlNode = _CurrentWebsiteConfig._webConfigXML.SelectSingleNode("XA_LOAN/VISIBLE/@collect_job_title_only")
				If collectJobTitleOnlyNode IsNot Nothing Then
					CollectJobTitleOnly = collectJobTitleOnlyNode.Value.ToUpper().Equals("Y")
				End If

				If BusinessAccountMode Then

					Dim baBusinessInfoPage As xa_Inc_baBusinessInfo = CType(LoadControl("~/xa/Inc/baBusinessInfo.ascx"), xa_Inc_baBusinessInfo)
					Dim primaryApplicantPage As String = ""
					Dim businessAccountTypeList = Common.GetBusinessAccountTypeList(_CurrentWebsiteConfig)
					SelectedBusinessAccountType = businessAccountTypeList.FirstOrDefault(Function(p) p.AccountCode.ToUpper() = HttpUtility.UrlDecode(Common.SafeString(Request.QueryString("acctype")).ToUpper()))
					plhBusinessInfoPage.Visible = False
					plhBusinessBeneficialOwnersPage.Visible = False
					If SelectedBusinessAccountType IsNot Nothing Then
						If scenario IsNot Nothing AndAlso SelectedBusinessAccountType.AccountCode <> scenario.SpecialAccountType Then scenario = Nothing
						Dim roleInfoToken As String = Common.SafeString(Request.QueryString("roleinfotoken"))
						Dim roleInfoStr As String = ""
						If Not String.IsNullOrWhiteSpace(roleInfoToken) AndAlso Session(roleInfoToken) IsNot Nothing Then
							roleInfoStr = Session(roleInfoToken).ToString()
						Else
							roleInfoStr = HttpUtility.UrlDecode(Common.SafeString(Request.QueryString("roleinfo")))
						End If
						BusinessAccountRoleList = New JavaScriptSerializer().Deserialize(Of List(Of CXAQueryRoleInfo))(roleInfoStr)
						plhBusinessInfoPage.Visible = True
						baBusinessInfoPage.LogoUrl = _LogoUrl
						baBusinessInfoPage.Availability = _Availability
						baBusinessInfoPage.IDPrefix = "ba_BusinessInfo"
						baBusinessInfoPage.StateDropdown = stateDropdown
						baBusinessInfoPage.EnabledAddressVerification = _CurrentWebsiteConfig.EnabledAddressVerification
						baBusinessInfoPage.EnableOccupancyStatus = True
						baBusinessInfoPage.EnableMailingAddress = EnableMailingAddressSection("ba_BusinessInfo")
						baBusinessInfoPage.OccupyingLocationDropdown = occupyingLocationDropdown
						baBusinessInfoPage.CollectDescriptionIfOccupancyStatusIsOther = (collectDescIfOccupancyStatusIsOther.ToUpper() = "Y")
						baBusinessInfoPage.LiveYearsDropdown = liveYearsDropdown
						baBusinessInfoPage.LiveMonthsDropdown = liveMonthsDropdown
						baBusinessInfoPage.BusinessIndustryCodesDropdown = Common.RenderDropdownlistWithEmpty(GetBusinessIndustryCodes(), " ", "--Please Select--")
						baBusinessInfoPage.EnableDoingBusinessAs = SelectedBusinessAccountType.ShowDoingBusinessAs OrElse (IsInMode("777") AndAlso IsInFeature("rename"))
						baBusinessInfoPage.PreviousPage = "GettingStarted"
						baBusinessInfoPage.NextPage = "page2"
						baBusinessInfoPage.MappedShowFieldLoanType = mappedShowFieldLoanType
						plhBusinessInfoPage.Controls.Add(baBusinessInfoPage)
						Dim selectedIDPrefixes As New List(Of String)
						If BusinessAccountRoleList IsNot Nothing AndAlso BusinessAccountRoleList.Count > 0 Then
							plhBusinessBeneficialOwnersPage.Visible = True
							Dim baBeneficialOwnersCtrl As xa_Inc_baBeneficialOwners = CType(LoadControl("~/xa/Inc/baBeneficialOwners.ascx"), xa_Inc_baBeneficialOwners)
							Dim index As Integer = 0

							'Use in UploadDoc section
							Dim docTitleOptions As List(Of CDocumentTitleInfo) = GenerateDocTitleOptions()
							Dim _requiredUpLoadDocsXA As String = Common.SafeString(Common.getNodeAttributes(_CurrentWebsiteConfig, "XA_LOAN/BEHAVIOR", "doc_upload_required"))
							Dim uploadDocsMessage As String = Common.getXABusinessUploadDocumentMessage(_CurrentWebsiteConfig)
							BusinessEnableDocUpload = Common.getXABusinessUploadDocumentEnable(_CurrentWebsiteConfig) AndAlso Not String.IsNullOrWhiteSpace(uploadDocsMessage)
							Dim docTitleRequired As Boolean = False
							If BusinessEnableDocUpload Then
								docTitleRequired = Not Common.getNodeAttributes(_CurrentWebsiteConfig, "XA_LOAN/BEHAVIOR", "required_doc_title").Equals("N", StringComparison.OrdinalIgnoreCase)
							End If

							For Each role As CXAQueryRoleInfo In BusinessAccountRoleList
								role.roletype = Common.SafeEncodeString(role.roletype)
								role.fname = Common.SafeEncodeString(role.fname)
								role.lname = Common.SafeEncodeString(role.lname)
								Dim selectedRole = SelectedBusinessAccountType.Roles.FirstOrDefault(Function(p) p.RoleType.ToUpper() = role.roletype.ToUpper())
								If selectedRole Is Nothing Then Throw New BadRequestException("Invalid request")
								Dim baAppInfoCtrl As xa_Inc_baApplicantInfo = CType(LoadControl("~/xa/Inc/baApplicantInfo.ascx"), xa_Inc_baApplicantInfo)
								Dim idPrefix As String = "ba_" & role.roletype.Replace(" ", "") & IIf(role.isjoint, "_joint", "").ToString() & "_" & index
								selectedIDPrefixes.Add(idPrefix)
								BAPrefixList.Add(idPrefix)
								baAppInfoCtrl.IsPrimaryApplicant = (index = 0)
								baAppInfoCtrl.IDPrefix = idPrefix
								baAppInfoCtrl.MappedShowFieldLoanType = mappedShowFieldLoanType
								If baAppInfoCtrl.IsPrimaryApplicant Then
									baBusinessInfoPage.NextPage = "page" & idPrefix
									baAppInfoCtrl.PreviousPage = "page" & baBusinessInfoPage.IDPrefix
									primaryApplicantPage = "page" & idPrefix
									PrimaryApplicantInfoPrefix = idPrefix
								Else
									baAppInfoCtrl.PreviousPage = "pageba_" & BusinessAccountRoleList(index - 1).roletype.Replace(" ", "") & IIf(BusinessAccountRoleList(index - 1).isjoint, "_joint", "").ToString() & "_" & (index - 1)
								End If

								If index = BusinessAccountRoleList.Count - 1 Then
									baAppInfoCtrl.NextPage = "pageba_BeneficialOwner"
									baBeneficialOwnersCtrl.PreviousPage = "page" & idPrefix
								Else
									baAppInfoCtrl.NextPage = "pageba_" & BusinessAccountRoleList(index + 1).roletype.Replace(" ", "") & IIf(BusinessAccountRoleList(index + 1).isjoint, "_joint", "").ToString() & "_" & ((index + 1))
								End If
								baAppInfoCtrl.RoleType = role.roletype
								baAppInfoCtrl.BusinessType = SelectedBusinessAccountType.BusinessType
								baAppInfoCtrl.IsJoint = role.isjoint
								baAppInfoCtrl.FirstName = role.fname
								baAppInfoCtrl.LastName = role.lname
								baAppInfoCtrl.CurrentConfig = _CurrentWebsiteConfig
								If Not bIsSSO Then ''hide the scan docs page if SSO via MobileApp
									If _CurrentWebsiteConfig.DLBarcodeScan = "Y" Then
										baAppInfoCtrl.LaserScandocAvailable = True
									ElseIf Not String.IsNullOrEmpty(_CurrentWebsiteConfig.ScanDocumentKey) Then
										baAppInfoCtrl.LegacyScandocAvailable = True
										baAppInfoCtrl.ScanDocumentKey = _CurrentWebsiteConfig.ScanDocumentKey
									End If
								End If

								baAppInfoCtrl.LaserScandocAvailable = _CurrentWebsiteConfig.DLBarcodeScan = "Y"
								plhBusinessAccountPagesWrapper.Controls.Add(baAppInfoCtrl)
								baAppInfoCtrl.EnableAddress = CheckShowField("divPhysicalAddress", idPrefix, selectedRole.ShowAddress) OrElse (IsInMode("777") AndAlso (IsInFeature("visibility") OrElse IsInFeature("rename")))
								baAppInfoCtrl.InbrachEnableAddress = selectedRole.ShowAddress
								baAppInfoCtrl.EnableContactInfo = CheckShowField("divContactInformation", idPrefix, selectedRole.ShowApplicantContactInfo) OrElse (IsInMode("777") AndAlso (IsInFeature("visibility") OrElse IsInFeature("rename")))
								baAppInfoCtrl.InbrachEnableContactInfo = selectedRole.ShowApplicantContactInfo
								baAppInfoCtrl.EnableContactRelative = CheckShowField("divContactRelative", idPrefix, selectedRole.ShowContactRelative) OrElse (IsInMode("777") AndAlso (IsInFeature("visibility") OrElse IsInFeature("rename")))
								baAppInfoCtrl.InbrachEnableContactRelative = selectedRole.ShowContactRelative
								''Always show RelationshipToPrimary for a Joint applicant
								If role.isjoint Then
									baAppInfoCtrl.EnableRelationshipToPrimary = True
								Else
									baAppInfoCtrl.EnableRelationshipToPrimary = CheckShowField("divRelationshipToPrimary", idPrefix, selectedRole.ShowRelationship) OrElse (IsInMode("777") AndAlso (IsInFeature("visibility") OrElse IsInFeature("rename")))
									baAppInfoCtrl.InbranchEnableRelationshipToPrimary = selectedRole.ShowRelationship
								End If
								baAppInfoCtrl.EnableEmployment = CheckShowField("divEmploymentSection", idPrefix, selectedRole.ShowEmployment) OrElse (IsInMode("777") AndAlso (IsInFeature("visibility") OrElse IsInFeature("rename")))
								baAppInfoCtrl.InbranchEnableEmployment = selectedRole.ShowEmployment
								baAppInfoCtrl.EnableIDCard = CheckShowField("divApplicantIdentification", idPrefix, selectedRole.ShowIDCard) OrElse (IsInMode("777") AndAlso (IsInFeature("visibility") OrElse IsInFeature("rename")))
								baAppInfoCtrl.InbranchEnableIDCard = selectedRole.ShowIDCard
								baAppInfoCtrl.RequirePrimaryIDCard = selectedRole.RequirePrimaryIDCard
								'TODO: default state of member number should be FALSE, while waiting for APM visibility of Business Account be ready, temporary set it to true for testing
								baAppInfoCtrl.EnableMemberNumber = CheckShowField("divMemberNumber", idPrefix, False) Or (IsInMode("777") AndAlso IsInFeature("visibility"))
								baAppInfoCtrl.InstitutionType = _CurrentWebsiteConfig.InstitutionType
								baAppInfoCtrl.EnableGender = CheckShowField("divGender", idPrefix, True) Or (IsInMode("777") AndAlso IsInFeature("visibility"))
								baAppInfoCtrl.EnableMotherMaidenName = CheckShowField("divMotherMaidenName", idPrefix, True) Or (IsInMode("777") AndAlso IsInFeature("visibility"))
								baAppInfoCtrl.EnableCitizenshipStatus = CheckShowField("divCitizenshipStatus", idPrefix, True) Or (IsInMode("777") AndAlso IsInFeature("visibility"))
								baAppInfoCtrl.MaritalStatusDropdown = maritalStatusDropdown
								baAppInfoCtrl.CitizenshipStatusDropdown = citizenshipStatusDropdown
								baAppInfoCtrl.StateDropdown = stateDropdown
								baAppInfoCtrl.EmployeeOfLenderDropdown = employeeOfLenderDropdown
								baAppInfoCtrl.LenderName = _strLenderName
								baAppInfoCtrl.EnabledAddressVerification = _CurrentWebsiteConfig.EnabledAddressVerification
								baAppInfoCtrl.EnableMailingAddress = EnableMailingAddressSection(idPrefix)
								baAppInfoCtrl.RelationToPrimaryApplicantDropdown = relationToPrimaryApplicantDropdown
								Dim memberNumberRequired As Boolean = Common.getNodeAttributes(_CurrentWebsiteConfig, "XA_LOAN/BEHAVIOR", "require_mem_number_secondary").ToUpper().Equals("Y")
								''required/not required member number only for secondary 
								''for new member in business xa, member number is optional
								If _Availability = "1b" Then
									memberNumberRequired = False
								End If
								baAppInfoCtrl.MemberNumberRequired = memberNumberRequired
								'BCU customization
								If _CurrentLenderRef.ToUpper.StartsWith("BCU") Then baAppInfoCtrl.FieldMaxLength = 40
								'TODO: default state of occupancy status should be FALSE, while waiting for APM visibility of Business Account be ready, temporary set it to true for testing
								baAppInfoCtrl.EnableOccupancyStatus = CheckShowField("divOccupancyStatusSection", idPrefix, selectedRole.ShowOccupancyStatus) Or (IsInMode("777") AndAlso (IsInFeature("visibility") OrElse IsInFeature("rename")))
								baAppInfoCtrl.InbranchEnableOccupancyStatus = selectedRole.ShowOccupancyStatus
								baAppInfoCtrl.OccupyingLocationDropdown = occupyingLocationDropdown
								baAppInfoCtrl.CollectDescriptionIfOccupancyStatusIsOther = (collectDescIfOccupancyStatusIsOther.ToUpper() = "Y")
								baAppInfoCtrl.LiveYearsDropdown = liveYearsDropdown
								baAppInfoCtrl.LiveMonthsDropdown = liveMonthsDropdown
								baAppInfoCtrl.PreferContactMethodDropdown = preferContactMethodDropdown
								baAppInfoCtrl.EnableGrossMonthlyIncome = CheckShowField("divGrossMonthlyIncome", idPrefix, True) Or (IsInMode("777") AndAlso IsInFeature("visibility"))
								baAppInfoCtrl.CollectJobTitleOnly = CollectJobTitleOnly
								baAppInfoCtrl.CollectDescriptionIfEmploymentStatusIsOther = xaCollectDescriptionIfEmploymentStatusIsOther
								baAppInfoCtrl.EmploymentStatusDropdown = employmentStatusDropdown
								If IDCardTypes IsNot Nothing AndAlso IDCardTypes.Count > 0 Then
									baAppInfoCtrl.IDCardTypeList = Common.RenderDropdownlist(IDCardTypes, " ", IIf(selectedRole.RequirePrimaryIDCard, "DRIVERS_LICENSE", ""))
									baAppInfoCtrl.IsExistIDCardType = True
								Else
									baAppInfoCtrl.IsExistIDCardType = False
								End If
								baAppInfoCtrl.LogoUrl = _LogoUrl
								baAppInfoCtrl.Availability = _Availability

								'Upload Doc Section
								baAppInfoCtrl.EnableDocUpload = BusinessEnableDocUpload
								If baAppInfoCtrl.EnableDocUpload Then
									baAppInfoCtrl.DocUploadTitle = uploadDocsMessage
									baAppInfoCtrl.RequireDocTitle = docTitleRequired
									baAppInfoCtrl.DocTitleOptions = docTitleOptions
									baAppInfoCtrl.DocUploadRequired = _requiredUpLoadDocsXA.Equals("Y", StringComparison.OrdinalIgnoreCase)
								End If

								index += 1
							Next
							baBeneficialOwnersCtrl.BusinessType = SelectedBusinessAccountType.BusinessType
							baBeneficialOwnersCtrl.SelectedIDPrefixes = selectedIDPrefixes
							baBeneficialOwnersCtrl.StateDropdown = stateDropdown
							baBeneficialOwnersCtrl.EnabledAddressVerification = _CurrentWebsiteConfig.EnabledAddressVerification
							baBeneficialOwnersCtrl.CitizenshipStatusDropdown = citizenshipStatusDropdown
							baBeneficialOwnersCtrl.IDPrefix = "ba_BeneficialOwner"
							baBeneficialOwnersCtrl.NextPage = "pageFS"
							pageBeforeFS = "pageba_BeneficialOwner"
							baBeneficialOwnersCtrl.LogoUrl = _LogoUrl
							baBeneficialOwnersCtrl.Availability = _Availability
							baBeneficialOwnersCtrl.MappedShowFieldLoanType = mappedShowFieldLoanType
							baBeneficialOwnersCtrl.EnableBeneficiarySSN = CheckShowField("divBeneficiarySSN1", baBeneficialOwnersCtrl.IDPrefix, True) Or (IsInMode("777") AndAlso IsInFeature("visibility"))
							plhBusinessBeneficialOwnersPage.Controls.Add(baBeneficialOwnersCtrl)
						End If
					Else
						Throw New BadRequestException("Invalid request")
					End If
					AfterGettingStartedPage = "page" & baBusinessInfoPage.IDPrefix
				End If
				If SpecialAccountMode Then
					Dim saAccountInfoPage As xa_Inc_saAccountInfo = CType(LoadControl("~/xa/Inc/saAccountInfo.ascx"), xa_Inc_saAccountInfo)
					Dim primaryApplicantPage As String = ""
					Dim specialAccountTypeList = Common.GetSpecialAccountTypeList(_CurrentWebsiteConfig)
					Dim selectedAccType = HttpUtility.UrlDecode(Common.SafeString(Request.QueryString("acctype")).ToUpper())
					If selectedAccType.IndexOf(","c) > -1 Then
						selectedAccType = selectedAccType.Substring(0, selectedAccType.IndexOf(","c))
					End If
					SelectedSpecialAccountType = specialAccountTypeList.FirstOrDefault(Function(p) p.AccountCode.ToUpper() = selectedAccType)
					plhSpecialAccountInfoPage.Visible = False
					If SelectedSpecialAccountType IsNot Nothing Then
						If scenario IsNot Nothing AndAlso SelectedSpecialAccountType.AccountCode <> scenario.SpecialAccountType Then scenario = Nothing
						Dim showSpecialInfo As Boolean = SelectedSpecialAccountType.ShowSpecialInfo
						If IsInMode("777") AndAlso (IsInFeature("visibility") OrElse IsInFeature("rename")) Then
							showSpecialInfo = True
						End If
						Dim roleInfoToken As String = Common.SafeString(Request.QueryString("roleinfotoken"))
						Dim roleInfoStr As String = ""
						If Not String.IsNullOrWhiteSpace(roleInfoToken) AndAlso Session(roleInfoToken) IsNot Nothing Then
							roleInfoStr = Session(roleInfoToken).ToString()
						Else
							roleInfoStr = HttpUtility.UrlDecode(Common.SafeString(Request.QueryString("roleinfo")))
						End If
						SpecialAccountRoleList = New JavaScriptSerializer().Deserialize(Of List(Of CXAQueryRoleInfo))(roleInfoStr)
						plhSpecialAccountInfoPage.Visible = True


						If showSpecialInfo Then
							saAccountInfoPage.LogoUrl = _LogoUrl
							saAccountInfoPage.IDPrefix = "sa_AccInfo"
							saAccountInfoPage.StateDropdown = stateDropdown
							saAccountInfoPage.EnabledAddressVerification = _CurrentWebsiteConfig.EnabledAddressVerification
							saAccountInfoPage.EnableOccupancyStatus = False
							saAccountInfoPage.EnableMailingAddress = EnableMailingAddressSection("sa_AccInfo")
							saAccountInfoPage.OccupyingLocationDropdown = occupyingLocationDropdown
							saAccountInfoPage.CollectDescriptionIfOccupancyStatusIsOther = (collectDescIfOccupancyStatusIsOther.ToUpper() = "Y")
							saAccountInfoPage.LiveYearsDropdown = liveYearsDropdown
							saAccountInfoPage.LiveMonthsDropdown = liveMonthsDropdown
							saAccountInfoPage.PreviousPage = "GettingStarted"
							saAccountInfoPage.NextPage = "page2"
							saAccountInfoPage.MappedShowFieldLoanType = mappedShowFieldLoanType
							saAccountInfoPage.Availability = _Availability
							plhSpecialAccountInfoPage.Controls.Add(saAccountInfoPage)
						End If
						If SpecialAccountRoleList IsNot Nothing AndAlso SpecialAccountRoleList.Count > 0 Then
							Dim index As Integer = 0
							'Use in UploadDoc section
							Dim docTitleOptions As List(Of CDocumentTitleInfo) = GenerateDocTitleOptions()
							Dim uploadDocsMessage As String = Common.getXASpecialUploadDocumentMessage(_CurrentWebsiteConfig)
							SpecialEnableDocUpload = Common.getXASpecialUploadDocumentEnable(_CurrentWebsiteConfig) AndAlso Not String.IsNullOrWhiteSpace(uploadDocsMessage)
							For Each role As CXAQueryRoleInfo In SpecialAccountRoleList
								role.roletype = Common.SafeEncodeString(role.roletype)
								role.fname = Common.SafeEncodeString(role.fname)
								role.lname = Common.SafeEncodeString(role.lname)
								Dim selectedRole = SelectedSpecialAccountType.Roles.FirstOrDefault(Function(p) p.RoleType.ToUpper() = role.roletype.ToUpper())
								If selectedRole Is Nothing Then Throw New BadRequestException("Invalid request")
								Dim saAppInfoCtrl As xa_Inc_saApplicantInfo = CType(LoadControl("~/xa/Inc/saApplicantInfo.ascx"), xa_Inc_saApplicantInfo)
								Dim idPrefix As String = "sa_" & role.roletype.Replace(" ", "") & IIf(role.isjoint, "_joint", "").ToString() & "_" & index
								SAPrefixList.Add(idPrefix)
								saAppInfoCtrl.IsPrimaryApplicant = (index = 0) '(role = SpecialAccountRoleList.First())
								saAppInfoCtrl.IDPrefix = idPrefix
								saAppInfoCtrl.MappedShowFieldLoanType = mappedShowFieldLoanType
								saAppInfoCtrl.DocTitleOptions = docTitleOptions
								If saAppInfoCtrl.IsPrimaryApplicant Then
									saAppInfoCtrl.EnableBeneficiaryTrust = CheckShowField("divBeneficiaryTrust1", idPrefix, True) Or (IsInMode("777") AndAlso IsInFeature("visibility"))
									saAppInfoCtrl.EnableBeneficiarySSN = CheckShowField("divBeneficiarySSN1", idPrefix, False) Or (IsInMode("777") AndAlso IsInFeature("visibility"))
									saAppInfoCtrl.EnableBeneficiaryAddress = CheckShowField("divBeneficiaryAddress", idPrefix, False) Or (IsInMode("777") AndAlso IsInFeature("visibility"))
									saAccountInfoPage.NextPage = "page" & idPrefix
									If showSpecialInfo Then
										saAppInfoCtrl.PreviousPage = "page" & saAccountInfoPage.IDPrefix
									Else
										saAppInfoCtrl.PreviousPage = "GettingStarted"
									End If
									primaryApplicantPage = "page" & idPrefix
									PrimaryApplicantInfoPrefix = idPrefix
								Else
									saAppInfoCtrl.PreviousPage = "pagesa_" & SpecialAccountRoleList(index - 1).roletype.Replace(" ", "") & IIf(SpecialAccountRoleList(index - 1).isjoint, "_joint", "").ToString() & "_" & (index - 1)
								End If
								If index = SpecialAccountRoleList.Count - 1 Then
									saAppInfoCtrl.NextPage = "pageFS"
									pageBeforeFS = "page" & saAppInfoCtrl.IDPrefix
								Else
									saAppInfoCtrl.NextPage = "pagesa_" & SpecialAccountRoleList(index + 1).roletype.Replace(" ", "") & IIf(SpecialAccountRoleList(index + 1).isjoint, "_joint", "").ToString() & "_" & ((index + 1))
								End If
								saAppInfoCtrl.RoleType = role.roletype
								saAppInfoCtrl.AccountName = SelectedSpecialAccountType.AccountName
								saAppInfoCtrl.AccountCode = SelectedSpecialAccountType.AccountCode
								saAppInfoCtrl.IsJoint = role.isjoint
								saAppInfoCtrl.FirstName = role.fname
								saAppInfoCtrl.LastName = role.lname
								saAppInfoCtrl.CurrentConfig = _CurrentWebsiteConfig
								If Not bIsSSO Then ''hide the scan docs page if SSO via MobileApp
									If _CurrentWebsiteConfig.DLBarcodeScan = "Y" Then
										saAppInfoCtrl.LaserScandocAvailable = True
									ElseIf Not String.IsNullOrEmpty(_CurrentWebsiteConfig.ScanDocumentKey) Then
										saAppInfoCtrl.LegacyScandocAvailable = True
										saAppInfoCtrl.ScanDocumentKey = _CurrentWebsiteConfig.ScanDocumentKey
									End If
								End If

								saAppInfoCtrl.LaserScandocAvailable = _CurrentWebsiteConfig.DLBarcodeScan = "Y"
								plhSpecialAccountPagesWrapper.Controls.Add(saAppInfoCtrl)
								saAppInfoCtrl.BlockApplicantAgeUnder = selectedRole.MinAge
								saAppInfoCtrl.BlockApplicantAgeOver = selectedRole.MaxAge
								saAppInfoCtrl.EnableAddress = CheckShowField("divPhysicalAddress", idPrefix, selectedRole.ShowAddress) OrElse (IsInMode("777") AndAlso (IsInFeature("visibility") OrElse IsInFeature("rename")))
								saAppInfoCtrl.EnableMailingAddress = EnableMailingAddressSection(idPrefix)
								saAppInfoCtrl.InbrachEnableAddress = selectedRole.ShowAddress
								saAppInfoCtrl.EnableContactInfo = CheckShowField("divContactInformation", idPrefix, selectedRole.ShowApplicantContactInfo) OrElse (IsInMode("777") AndAlso (IsInFeature("visibility") OrElse IsInFeature("rename")))
								saAppInfoCtrl.InbrachEnableContactInfo = selectedRole.ShowApplicantContactInfo

								saAppInfoCtrl.EnableContactRelative = CheckShowField("divContactRelative", idPrefix, selectedRole.ShowContactRelative) OrElse (IsInMode("777") AndAlso (IsInFeature("visibility") OrElse IsInFeature("rename")))
								saAppInfoCtrl.InbrachEnableContactRelative = selectedRole.ShowContactRelative
								''Always show RelationshipToPrimary for a Joint applicant
								If role.isjoint Then
									saAppInfoCtrl.EnableRelationshipToPrimary = True
								Else
									saAppInfoCtrl.EnableRelationshipToPrimary = CheckShowField("divRelationshipToPrimary", idPrefix, selectedRole.ShowRelationship) OrElse selectedRole.ShowRelationship OrElse (IsInMode("777") AndAlso (IsInFeature("visibility") OrElse IsInFeature("rename")))
									saAppInfoCtrl.InbranchEnableRelationshipToPrimary = selectedRole.ShowRelationship
								End If
								saAppInfoCtrl.EnableEmployment = CheckShowField("divEmploymentSection", idPrefix, selectedRole.ShowEmployment) OrElse (IsInMode("777") AndAlso (IsInFeature("visibility") OrElse IsInFeature("rename")))
								saAppInfoCtrl.InbranchEnableEmployment = selectedRole.ShowEmployment
								saAppInfoCtrl.EnableIDCard = CheckShowField("divApplicantIdentification", idPrefix, selectedRole.ShowIDCard) OrElse (IsInMode("777") AndAlso (IsInFeature("visibility") OrElse IsInFeature("rename")))
								saAppInfoCtrl.InbranchEnableIDCard = selectedRole.ShowIDCard
								saAppInfoCtrl.RequirePrimaryIDCard = selectedRole.RequirePrimaryIDCard
								saAppInfoCtrl.ShowBeneficiary = CheckShowField("divBeneficiary", idPrefix, Not SelectedSpecialAccountType.HideBeneficiaryPage) OrElse (IsInMode("777") AndAlso (IsInFeature("visibility") OrElse IsInFeature("rename")))
								saAppInfoCtrl.EnableMemberNumber = CheckShowField("divMemberNumber", idPrefix, False) Or (IsInMode("777") AndAlso IsInFeature("visibility"))
								saAppInfoCtrl.InstitutionType = _CurrentWebsiteConfig.InstitutionType
								saAppInfoCtrl.EnableGender = CheckShowField("divGender", idPrefix, True) Or (IsInMode("777") AndAlso IsInFeature("visibility"))
								saAppInfoCtrl.EnableMotherMaidenName = CheckShowField("divMotherMaidenName", idPrefix, True) Or (IsInMode("777") AndAlso IsInFeature("visibility"))
								saAppInfoCtrl.EnableCitizenshipStatus = CheckShowField("divCitizenshipStatus", idPrefix, True) Or (IsInMode("777") AndAlso IsInFeature("visibility"))
								saAppInfoCtrl.EnableMaritalStatus = (CheckShowField("divMaritalStatus", idPrefix, selectedRole.ShowMaritalStatus) Or (IsInMode("777") AndAlso (IsInFeature("visibility") OrElse IsInFeature("rename"))))
								saAppInfoCtrl.InbranchEnableMaritalStatus = selectedRole.ShowMaritalStatus
								saAppInfoCtrl.MaritalStatusDropdown = maritalStatusDropdown
								saAppInfoCtrl.CitizenshipStatusDropdown = citizenshipStatusDropdown
								saAppInfoCtrl.StateDropdown = stateDropdown
								If selectedRole.MaxAge < 0 OrElse selectedRole.MaxAge > 17 Then
									saAppInfoCtrl.EmployeeOfLenderDropdown = employeeOfLenderDropdown
								End If
								saAppInfoCtrl.LenderName = _strLenderName
								saAppInfoCtrl.EnabledAddressVerification = _CurrentWebsiteConfig.EnabledAddressVerification
								saAppInfoCtrl.RelationToPrimaryApplicantDropdown = relationToPrimaryApplicantDropdown
								Dim memberNumberRequired As Boolean = Common.getNodeAttributes(_CurrentWebsiteConfig, "XA_LOAN/BEHAVIOR", "require_mem_number_secondary").ToUpper().Equals("Y")
								saAppInfoCtrl.MemberNumberRequired = memberNumberRequired
								'BCU customization
								If _CurrentLenderRef.ToUpper.StartsWith("BCU") Then saAppInfoCtrl.FieldMaxLength = 40
								saAppInfoCtrl.EnableOccupancyStatus = CheckShowField("divOccupancyStatusSection", idPrefix, selectedRole.ShowOccupancyStatus) Or (IsInMode("777") AndAlso (IsInFeature("visibility") OrElse IsInFeature("rename")))
								saAppInfoCtrl.OccupyingLocationDropdown = occupyingLocationDropdown
								saAppInfoCtrl.CollectDescriptionIfOccupancyStatusIsOther = (collectDescIfOccupancyStatusIsOther.ToUpper() = "Y")
								saAppInfoCtrl.LiveYearsDropdown = liveYearsDropdown
								saAppInfoCtrl.LiveMonthsDropdown = liveMonthsDropdown
								saAppInfoCtrl.PreferContactMethodDropdown = preferContactMethodDropdown
								saAppInfoCtrl.EnableGrossMonthlyIncome = CheckShowField("divGrossMonthlyIncome", idPrefix, True) Or (IsInMode("777") AndAlso IsInFeature("visibility"))
								saAppInfoCtrl.CollectJobTitleOnly = CollectJobTitleOnly
								saAppInfoCtrl.CollectDescriptionIfEmploymentStatusIsOther = xaCollectDescriptionIfEmploymentStatusIsOther
								saAppInfoCtrl.EmploymentStatusDropdown = employmentStatusDropdown
								If IDCardTypes IsNot Nothing AndAlso IDCardTypes.Count > 0 Then
									saAppInfoCtrl.IDCardTypeList = Common.RenderDropdownlist(IDCardTypes, " ", IIf(selectedRole.RequirePrimaryIDCard, "DRIVERS_LICENSE", ""))
									saAppInfoCtrl.IsExistIDCardType = True
								Else
									saAppInfoCtrl.IsExistIDCardType = False
								End If
								saAppInfoCtrl.Availability = _Availability
								saAppInfoCtrl.LogoUrl = _LogoUrl

								xaApplicationQuestionLoanPage.SpecialAccountName = SelectedSpecialAccountType.AccountName
								xaApplicationQuestionReviewPage.SpecialAccountName = SelectedSpecialAccountType.AccountName
								' These controls are loaded, so add the special account name to prevent warning logs.
								xaApplicantQuestion.SpecialAccountName = SelectedSpecialAccountType.AccountName
								xaMinorApplicantQuestion.SpecialAccountName = SelectedSpecialAccountType.AccountName
								xaCoApplicantQuestion.SpecialAccountName = SelectedSpecialAccountType.AccountName

								index += 1
							Next

						End If
						If showSpecialInfo Then
							AfterGettingStartedPage = "page" & saAccountInfoPage.IDPrefix
						ElseIf Not String.IsNullOrEmpty(primaryApplicantPage) Then
							AfterGettingStartedPage = primaryApplicantPage
						End If
					Else
						Throw New BadRequestException("Invalid request")
					End If
				End If
				InitBeneficiary(_xaBeneficiaryLevel)
				InitApplicantEmployment(xaCollectDescriptionIfEmploymentStatusIsOther, employmentStatusDropdown, _CurrentWebsiteConfig, CollectJobTitleOnly)
				InitApplicantInfo(citizenshipStatusDropdown, maritalStatusDropdown, bIsSSO)
				InitApplicantAddress(stateDropdown, occupyingLocationDropdown, CollectDescriptionIfOccupancyStatusIsOther, liveMonthsDropdown, liveYearsDropdown)
				InitApplicantID(stateDropdown, IDCardTypes)
				Dim nAvailability As String = "1"
				If Not String.IsNullOrEmpty(Request.QueryString("type")) Then
					nAvailability = Common.SafeString(Request.QueryString("type"))
				End If
				InitApplicantQuestion(nAvailability)
				_enableJoint = Not HasEnableJointXA().ToUpper().Equals("N")
				'for XA secondary
				If nAvailability = "2" Or nAvailability = "2a" Then
					Dim enableJointSA = HasEnableJointSA().ToUpper()
					If Not String.IsNullOrEmpty(enableJointSA) Then
						_enableJoint = Not enableJointSA.Equals("N")
					End If
				End If
				InitFundingOptionUc(_Availability)
				InitProductSelection(_Availability, scenario)
				InitBranch()
				InitCustomQuestion()
				InitDisclosure()
				InitFom()

				''enable attribute
				'hdEnableJoint.Value = hasEnableJointXA(_CurrentWebsiteConfig)
				plhApplicationDeclined.Visible = False
				If CustomListRuleList IsNot Nothing AndAlso CustomListRuleList.Any(Function(p) p.Code = "APPLICANT_BLOCK_RULE") Then
					plhApplicationDeclined.Visible = True
					Dim applicationBlockRulesPage As Inc_MainApp_ApplicationBlockRules = CType(LoadControl("~/Inc/MainApp/ApplicationBlockRules.ascx"), Inc_MainApp_ApplicationBlockRules)
					applicationBlockRulesPage.CustomListItem = CustomListRuleList.First(Function(p) p.Code = "APPLICANT_BLOCK_RULE")
					plhApplicationDeclined.Controls.Add(applicationBlockRulesPage)
				End If

				plhInstaTouchStuff.Visible = False
				If _CurrentWebsiteConfig.InstaTouchEnabled AndAlso Not bIsSSO Then
					plhInstaTouchStuff.Visible = True
					Dim instaTouchStuffCtrl As Inc_MainApp_InstaTouchStuff = CType(LoadControl("~/Inc/MainApp/InstaTouchStuff.ascx"), Inc_MainApp_InstaTouchStuff)
					instaTouchStuffCtrl.PersonalInfoPage = "page2"
					instaTouchStuffCtrl.LoanType = "XA"
					instaTouchStuffCtrl.InstaTouchMerchantId = _CurrentWebsiteConfig.InstaTouchMerchantId
					instaTouchStuffCtrl.LenderID = _CurrentWebsiteConfig.LenderId
					instaTouchStuffCtrl.LenderRef = _CurrentWebsiteConfig.LenderRef
					plhInstaTouchStuff.Controls.Add(instaTouchStuffCtrl)
				End If

				hdForeignContact.Value = IsForeignContact()
				InitTheme()
			Catch ex As Exception
				log.Error(ex)
				''Response.Redirect("/apply.aspx?lenderref=" & _CurrentLenderRef, True)
			End Try
		End If

	End Sub
	Private Sub InitBeneficiary(beneficiaryLevel As String)
		xaBeneficiary.EnableBeneficiaryAddress = CheckShowField("divBeneficiaryAddress", "", False) Or (IsInMode("777") AndAlso IsInFeature("visibility"))
		xaBeneficiary.EnableBeneficiarySSN = CheckShowField("divBeneficiarySSN1", "", False) Or (IsInMode("777") AndAlso IsInFeature("visibility"))
		xaBeneficiary.ForceSSNShown = (beneficiaryLevel = "product")
		xaBeneficiary.EnableBeneficiaryTrust = CheckShowField("divBeneficiaryTrust1", "", True) Or (IsInMode("777") AndAlso IsInFeature("visibility"))
		xaBeneficiary.MappedShowFieldLoanType = mappedShowFieldLoanType
	End Sub
	Private Sub InitApplicantAddress(stateDropdown As String, occupyingLocationDropdown As String, collectDescIfOccupancyStatusIsOther As String, liveMonthsDropdown As String, liveYearsDropdown As String)
		'' ''get the previous_address_threshold if existing occupancy status
		''_previous_address_thresshold = Common.getPreviousAddressThresHold(_CurrentWebsiteConfig)
		''applicant       
		_previous_address_threshold = _CurrentWebsiteConfig.getPreviousThreshold("XA_LOAN", "previous_address_threshold")
		If CheckShowField("divOccupancyStatusSection", "", False) Or (IsInMode("777") AndAlso IsInFeature("visibility")) Then
			xaApplicantAddress.LiveMonthsDropdown = liveMonthsDropdown
			xaApplicantAddress.LiveYearsDropdown = liveYearsDropdown
			xaApplicantAddress.OccupyingLocationDropdown = occupyingLocationDropdown
			xaApplicantAddress.EnableOccupancyStatus = True
		Else
			xaApplicantAddress.EnableOccupancyStatus = False
		End If

		If CheckShowField("divOccupancyStatusSection", "co_", False) Or (IsInMode("777") AndAlso IsInFeature("visibility")) Then
			''coApplicant
			xaCoApplicantAddress.LiveMonthsDropdown = liveMonthsDropdown
			xaCoApplicantAddress.LiveYearsDropdown = liveYearsDropdown
			xaCoApplicantAddress.OccupyingLocationDropdown = occupyingLocationDropdown
			xaCoApplicantAddress.EnableOccupancyStatus = True
		Else
			xaCoApplicantAddress.EnableOccupancyStatus = False
		End If

		If CheckShowField("divOccupancyStatusSection", "m_", False) Or (IsInMode("777") AndAlso IsInFeature("visibility")) Then
			'minor
			xaMinorApplicantAddress.LiveMonthsDropdown = liveMonthsDropdown
			xaMinorApplicantAddress.LiveYearsDropdown = liveYearsDropdown
			xaMinorApplicantAddress.OccupyingLocationDropdown = occupyingLocationDropdown
			xaMinorApplicantAddress.EnableOccupancyStatus = True
		Else
			xaMinorApplicantAddress.EnableOccupancyStatus = False
		End If
		xaApplicantAddress.EnableHousingPayment = CheckShowField("divHousingPayment", "", False) Or (IsInMode("777") AndAlso IsInFeature("visibility"))
		xaCoApplicantAddress.EnableHousingPayment = CheckShowField("divHousingPayment", "co_", False) Or (IsInMode("777") AndAlso IsInFeature("visibility"))
		xaMinorApplicantAddress.EnableHousingPayment = CheckShowField("divHousingPayment", "m_", False) Or (IsInMode("777") AndAlso IsInFeature("visibility"))
		xaApplicantAddress.CollectDescriptionIfOccupancyStatusIsOther = collectDescIfOccupancyStatusIsOther
		xaCoApplicantAddress.CollectDescriptionIfOccupancyStatusIsOther = collectDescIfOccupancyStatusIsOther
		''add state dropdown
		xaApplicantAddress.StateDropdown = stateDropdown
		xaCoApplicantAddress.StateDropdown = stateDropdown
		''minor 
		xaMinorApplicantAddress.StateDropdown = stateDropdown

		xaApplicantAddress.EnableMailingAddress = EnableMailingAddressSection()
		xaCoApplicantAddress.EnableMailingAddress = EnableMailingAddressSection("co_")
		xaMinorApplicantAddress.EnableMailingAddress = EnableMailingAddressSection("m_")

		'' address_key
		If Not String.IsNullOrEmpty(_CurrentWebsiteConfig.AddressKey) Then
			_address_key = _CurrentWebsiteConfig.AddressKey
		End If
	End Sub
	Private Sub InitApplicantID(stateDropdown As String, idCardTypes As Dictionary(Of String, String))
		If idCardTypes IsNot Nothing AndAlso idCardTypes.Count > 0 Then
			'ID_Card_type dropdown, default driver license
			xaApplicantID.isExistIDCardType = True
			xaCoApplicantID.isExistIDCardType = True
			Dim idCardTypeDropDownStr = Common.RenderDropdownlist(idCardTypes, " ", "DRIVERS_LICENSE")
			xaApplicantID.IDCardTypeList = idCardTypeDropDownStr
			xaCoApplicantID.IDCardTypeList = idCardTypeDropDownStr
			''minor ID
			xaMinorApplicantID.isExistIDCardType = True
			xaMinorApplicantID.IDCardTypeList = idCardTypeDropDownStr
		Else
			'there is no id_card_type from retrieval list
			xaApplicantID.isExistIDCardType = False
			xaCoApplicantID.isExistIDCardType = False
			''minor ID
			xaMinorApplicantID.isExistIDCardType = False
		End If

		xaApplicantID.StateDropdown = stateDropdown
		xaCoApplicantID.StateDropdown = stateDropdown
		''minor 
		xaMinorApplicantID.StateDropdown = stateDropdown
	End Sub

	Private Sub InitBranch()
		_xaRequiredBranch = Common.getNodeAttributes(_CurrentWebsiteConfig, "XA_LOAN/BEHAVIOR", "branch_selection_dropdown_required")
		_xaBranchOptionsStr = GetBranches("XA")
		_xaBranchZipPoolName = getZipCodePoolName()
	End Sub

	Private Function getZipCodePoolName() As Dictionary(Of String, String)
		Dim zipCodePoolNames As New Dictionary(Of String, String)
		If Not String.IsNullOrEmpty(_CurrentWebsiteConfig.LenderCode) Then
			''When lender_code Is Not null, use data from cached object for the branch selection dropdown
			Dim branches As List(Of DownloadedSettings.CBranch) = CDownloadedSettings.Branches(_CurrentWebsiteConfig)
			If branches IsNot Nothing And branches.Any() Then
				zipCodePoolNames = branches.Where(Function(b) b.IsXA = "Y" And b.IsAvailableToConsumerXA = "Y" And (b.ZipCodePoolName IsNot Nothing AndAlso b.ZipCodePoolName <> "")).ToDictionary(Function(x) x.BranchCode, Function(x) x.ZipCodePoolName)
			End If
		End If
		Return zipCodePoolNames
	End Function

	Private Sub InitFom()
		Dim sAvailbility As String = Common.SafeString(Request.QueryString("type"))
		HideFOMPage.Value = "N"	 'new member so show fom
		If sAvailbility = "2" Or sAvailbility = "2a" Or sAvailbility = "2b" Or sAvailbility = "2s" Then
			HideFOMPage.Value = "Y"	 'existing member
		ElseIf Not CFOMQuestion.CurrentFOMQuestions(_CurrentWebsiteConfig).Count > 0 Then
			HideFOMPage.Value = "Y"
		ElseIf _CurrentWebsiteConfig.InstitutionType = CEnum.InstitutionType.BANK Then
			HideFOMPage.Value = "Y"
		Else
			Dim oFomNodes As XmlNode = _CurrentWebsiteConfig._webConfigXML.SelectSingleNode("XA_LOAN/VISIBLE/@fom")
			If oFomNodes IsNot Nothing AndAlso oFomNodes.Value.Equals("N") Then
				HideFOMPage.Value = "Y"
			End If
		End If
		'' If Not HideFOMPage.Value = "Y" Then
		ucFomQuestion.FOMQuestions = CFOMQuestion.CurrentFOMQuestions(_CurrentWebsiteConfig)
		ucFomQuestion._CFOMQuestionList = CFOMQuestion.DownloadFomNextQuestions(_CurrentWebsiteConfig)
		ucFomQuestion._lenderRef = _CurrentLenderRef

		''FOM_V2
		ucFomQuestion.hasFOM_V2 = CFOMQuestion.hasFOM_V2(_CurrentWebsiteConfig)
		''get special account type for filter FOM Questions( type =1s)
		If SpecialAccountMode Then
			Dim selectedAccType = HttpUtility.UrlDecode(Common.SafeString(Request.QueryString("acctype")).ToUpper())
			If selectedAccType.IndexOf(","c) > -1 Then
				selectedAccType = selectedAccType.Substring(0, selectedAccType.IndexOf(","c))
			End If
			ucFomQuestion.SelectedSpecialAccountType = selectedAccType
		End If

		''END FOM_V2
		'show the question fom
		If HideFOMPage.Value = "Y" Then
			ucFomQuestion.Visible = False
		Else
			ucFomQuestion.Visible = True
			ucFomQuestion.FormHeader = HeaderUtils.RenderPageTitle(0, "Select your eligibility", True, isRequired:=True)
		End If
	End Sub
	Private Sub InitApplicantQuestion(nAvailability As String)
		xaApplicantQuestion.nAvailability = nAvailability
		xaCoApplicantQuestion.nAvailability = nAvailability
		xaMinorApplicantQuestion.nAvailability = nAvailability
	End Sub
	Private Sub InitProductSelection(availability As String, scenario As CCustomApplicationScenarioItem)
		Dim currentProdList As New List(Of CProduct)
		If scenario IsNot Nothing Then
			' incase of scenarios, we ignore APM's settings for products to give client the ability to offers products that is disabled in APM in some particular scenarios
			Dim prodList As List(Of CProduct) = CProduct.GetAllProducts(_CurrentWebsiteConfig, availability)
			Dim scenarioProductList As New Dictionary(Of String, CProduct)
			If prodList IsNot Nothing AndAlso prodList.Count > 0 Then
				For Each prod In prodList
					If scenario.UseDefaultRequiredProducts Then
						If prod.IsRequired Then
							scenarioProductList.Add(prod.ProductCode, prod)
						End If
					ElseIf scenario.RequiredProducts IsNot Nothing AndAlso scenario.RequiredProducts.Contains(prod.ProductCode) Then
						prod.IsRequired = True
						scenarioProductList.Add(prod.ProductCode, prod)
					Else
						prod.IsRequired = False
					End If
					If scenario.UseDefaultPreselectedProducts Then
						If prod.IsPreSelection AndAlso Not scenarioProductList.ContainsKey(prod.ProductCode) Then
							scenarioProductList.Add(prod.ProductCode, prod)
						End If
					ElseIf scenario.PreselectedProducts IsNot Nothing AndAlso scenario.PreselectedProducts.Contains(prod.ProductCode) Then
						prod.IsPreSelection = True
						If Not scenarioProductList.ContainsKey(prod.ProductCode) Then
							scenarioProductList.Add(prod.ProductCode, prod)
						End If
					Else
						prod.IsPreSelection = False
					End If
					If scenario.UseDefaultAvailableProducts OrElse (scenario.AvailableProducts IsNot Nothing AndAlso scenario.AvailableProducts.Contains(prod.ProductCode)) Then
						If Not scenarioProductList.ContainsKey(prod.ProductCode) Then
							scenarioProductList.Add(prod.ProductCode, prod)
						End If
					End If
					currentProdList = scenarioProductList.Values.ToList()
				Next
			End If

		Else
			currentProdList = CProduct.GetProducts(_CurrentWebsiteConfig, availability)
		End If
		Dim productRendering = New CProductRendering(currentProdList)
		'FOR TESTING ONLY
		'For Each cProduct As CProduct In currentProdList
		'    If cProduct.CustomQuestions.Any() Then
		'        For Each q As CProductCustomQuestion In cProduct.CustomQuestions
		'            q.IsRequired = True
		'        Next
		'    End If
		'    If cProduct.ProductCode = "CD6" Then
		'        cProduct.IsPreSelection = True
		'    End If
		'    cProduct.MinorAccountCode = "a|b|c"
		'Next
		'END FOR TESTING
		''*****display all the products
		'_RenderSelectProducts = Server.HtmlDecode(_ProductRendering.RenderAllProductsGroup(_CurrentLenderRef))

		''check available products for xa and business xa

		If Not SpecialAccountMode Then
			If currentProdList Is Nothing OrElse currentProdList.Count = 0 Then
				NoProductAvailable = True
			End If
		End If
		ucProductSelection.CurrentLenderRef = _CurrentLenderRef
		ucProductSelection.CurrentProductList = currentProdList
		ucProductSelection.ProductQuestionsHtml = productRendering.BuildProductQuestions(currentProdList, _textAndroidTel, True)
		ucProductSelection.WebsiteConfig = _CurrentWebsiteConfig
		Dim minorProductList As List(Of MinorAccountProduct) = Common.ParsedMinorAccountProduct(_CurrentWebsiteConfig)
		Dim minorAccountList As List(Of CMinorAccount) = Common.ParsedMinorAccountType(_CurrentWebsiteConfig)
		''render special account type 
		If availability = "1a" Or availability = "2a" Then
			Dim minorProductDic As Dictionary(Of String, String) = Common.getItemsFromConfig(_CurrentWebsiteConfig, "XA_LOAN/SPECIAL_ACCOUNTS/ACCOUNT", "product_code", "minor_account_code")
			Dim parseSpecialAccountType As Dictionary(Of String, String) = Common.getItemsFromConfig(_CurrentWebsiteConfig, "XA_LOAN/SPECIAL_ACCOUNT_TYPE/ITEM", "account_code", "title")
			Dim ddlSpecialAccountType = FilterSpecialAccountType(currentProdList, parseSpecialAccountType)
			If minorProductDic.Count = 0 Then ''there is no minor products from config then filter directly from download  
				minorProductList = CProduct.GetMinorAccountProductsFromDownload(currentProdList, minorAccountList)
			End If
			Dim btnClass = "class='btn-header-theme active'"
			If ddlSpecialAccountType.Count > 1 Then	'' rendering dropdown when it has at least 2 account types
				Dim strDefaultOption As String = "<option value=''>Choose an account type</option>"
				_RenderSpecialAccountType = "<div id='divSpecialAccountType' class='header-theme-dropdown-btn' style='padding-top:1px'><label for='ddlSpecialAccountType' class='sr-only'>Special Account Type</label><select id='ddlSpecialAccountType'>" & strDefaultOption & Common.RenderDropdownlist(ddlSpecialAccountType, "") & "</select></div>"
			ElseIf ddlSpecialAccountType.Count = 1 Then
				hdMinorAccountCode.Value = ddlSpecialAccountType.ElementAt(0).Key
				_RenderSpecialAccountType = "<div id='divSpecialAccountType' class='text-align-center' ><a data-role='button'" & btnClass & " id='ddlSpecialAccountType' onclick='toggleSpecialAccountTypeButton(this);' >" & ddlSpecialAccountType.ElementAt(0).Value & "</a></div>"
			Else
				_RenderSpecialAccountType = "<div id='divSpecialAccountType' class='text-align-center' ><a data-role='button' class='btn-header-theme ui-disabled' id='ddlSpecialAccountType' onclick='toggleSpecialAccountTypeButton(this);' >Minor Account is not available.</a></div>"
			End If
		ElseIf SpecialAccountMode AndAlso scenario Is Nothing Then
			'filter product list by special account type
			'Dim specialAccountTypeList As List(Of CSpecialAccountType) = GetSpecialAccountTypeList(_CurrentWebsiteConfig)
			NoProductAvailable = True
			If currentProdList IsNot Nothing AndAlso currentProdList.Any() Then
				Dim selectedAccType = HttpUtility.UrlDecode(Common.SafeString(Request.QueryString("acctype")).ToUpper())
				If selectedAccType.IndexOf(","c) > -1 Then ''don't know why sometime it has duplicate acctype
					selectedAccType = selectedAccType.Substring(0, selectedAccType.IndexOf(","c))
				End If
				Dim specialAccountTypeList = Common.GetSpecialAccountTypeList(_CurrentWebsiteConfig)
				Dim selectedAccName = specialAccountTypeList.FirstOrDefault(Function(x) x.AccountCode.ToUpper = selectedAccType).AccountName.ToUpper
				''specialAccountTypes is a list of account names from download(account codes are not available)
				ucProductSelection.CurrentProductList = currentProdList.Where(Function(p) p.AccountType <> "OTHER" AndAlso p.specialAccountTypes IsNot Nothing AndAlso p.specialAccountTypes.Any(Function(s) s.ToUpper() = selectedAccName)).ToList()
				If ucProductSelection.CurrentProductList IsNot Nothing AndAlso ucProductSelection.CurrentProductList.Count > 0 Then
					NoProductAvailable = False
				End If
			End If
		End If
		_minoProductList = New JavaScriptSerializer().Serialize(minorProductList)
		_minorAccountList = New JavaScriptSerializer().Serialize(minorAccountList)
		hdSpecialJoint.Value = Common.getNodeAttributes(_CurrentWebsiteConfig, "XA_LOAN/VISIBLE", "special_joint")
		_locationPool = _CurrentWebsiteConfig.LocationPool
		Dim currentLenderServiceConfigInfo = CDownloadedSettings.DownloadLenderServiceConfigInfo(_CurrentWebsiteConfig)
		ucProductSelection.ZipCodePoolList = currentLenderServiceConfigInfo.ZipCodePool
		ucProductSelection.ZipCodePoolName = currentLenderServiceConfigInfo.ZipCodePoolName
		ucProductSelection.ContentDataTheme = _ContentDataTheme
		ucProductSelection.FooterDataTheme = _FooterDataTheme
		ucProductSelection.HeaderDataTheme = _HeaderDataTheme
		ucProductSelection.HeaderDataTheme2 = _HeaderDataTheme2

	End Sub
	Private Sub InitApplicantEmployment(collectDescIfEmploymentStatusIsOther As String, employmentStatusDropdown As String, config As CWebsiteConfig, jobTitleOnly As Boolean)
		xaApplicantEmployment.CollectDescriptionIfEmploymentStatusIsOther = collectDescIfEmploymentStatusIsOther
		xaCoApplicantEmployment.CollectDescriptionIfEmploymentStatusIsOther = collectDescIfEmploymentStatusIsOther

		xaMinorApplicantEmployment.EmploymentStatusDropdown = employmentStatusDropdown
		xaApplicantEmployment.EmploymentStatusDropdown = employmentStatusDropdown
		xaCoApplicantEmployment.EmploymentStatusDropdown = employmentStatusDropdown

		xaApplicantEmployment.Config = config.GetWebConfigElement
		xaCoApplicantEmployment.Config = config.GetWebConfigElement
		xaMinorApplicantEmployment.Config = config.GetWebConfigElement

		xaApplicantEmployment.EnableGrossMonthlyIncome = CheckShowField("divGrossMonthlyIncome", "", True) Or (IsInMode("777") AndAlso IsInFeature("visibility"))
		xaCoApplicantEmployment.EnableGrossMonthlyIncome = CheckShowField("divGrossMonthlyIncome", "co_", True) Or (IsInMode("777") AndAlso IsInFeature("visibility"))
		xaMinorApplicantEmployment.EnableGrossMonthlyIncome = CheckShowField("divGrossMonthlyIncome", "m_", True) Or (IsInMode("777") AndAlso IsInFeature("visibility"))

		xaApplicantEmployment.CollectJobTitleOnly = jobTitleOnly
		xaCoApplicantEmployment.CollectJobTitleOnly = jobTitleOnly
		xaMinorApplicantEmployment.CollectJobTitleOnly = jobTitleOnly
	End Sub

	Private Sub InitApplicantInfo(citizenshipStatusDropdown As String, maritalStatusDropdown As String, bIsSSO As Boolean)
		Dim liveMonthsDropdown As String = Common.GetMonths()
		xaApplicantInfo.LiveMonthsDropdown = liveMonthsDropdown
		xaApplicantInfo.CitizenshipStatusDropdown = citizenshipStatusDropdown
		xaApplicantInfo.InstitutionType = _CurrentWebsiteConfig.InstitutionType

		xaCoApplicantInfo.LiveMonthsDropdown = liveMonthsDropdown
		xaCoApplicantInfo.CitizenshipStatusDropdown = citizenshipStatusDropdown
		xaCoApplicantInfo.InstitutionType = _CurrentWebsiteConfig.InstitutionType
		''minor information
		xaMinorApplicantInfo.LiveMonthsDropdown = liveMonthsDropdown
		xaMinorApplicantInfo.CitizenshipStatusDropdown = citizenshipStatusDropdown
		xaMinorApplicantInfo.InstitutionType = _CurrentWebsiteConfig.InstitutionType
		'' employee of lender
		Dim employeeOfLenderType = _CurrentWebsiteConfig.GetEnumItems("EMPLOYEE_OF_LENDER_TYPE")
		If employeeOfLenderType IsNot Nothing AndAlso employeeOfLenderType.Count > 0 Then	 'available in  LPQMobileWebsiteConfig
			''get employee of lender type from config
			Dim employeeOfLenderDropdown = Common.RenderDropdownlist(employeeOfLenderType, "")

			xaApplicantInfo.EmployeeOfLenderDropdown = employeeOfLenderDropdown
			xaCoApplicantInfo.EmployeeOfLenderDropdown = employeeOfLenderDropdown
			Dim sLenderName As String = CDownloadedSettings.DownloadLenderServiceConfigInfo(_CurrentWebsiteConfig).LenderName
			xaApplicantInfo.LenderName = sLenderName
			xaCoApplicantInfo.LenderName = sLenderName
		End If
		'set/hide fields in Applicant - custom for GETCU
		xaApplicantInfo.Config = _CurrentWebsiteConfig.GetWebConfigElement
		xaCoApplicantInfo.Config = _CurrentWebsiteConfig.GetWebConfigElement
		xaMinorApplicantInfo.Config = _CurrentWebsiteConfig.GetWebConfigElement
		If Not bIsSSO Then ''hide the scan docs page if SSO via MobileApp
			If _CurrentWebsiteConfig.DLBarcodeScan = "Y" Then
				xaApplicantInfo.LaserScandocAvailable = True
				xaCoApplicantInfo.LaserScandocAvailable = True
				LaserDLScanEnabled = True
			ElseIf Not String.IsNullOrEmpty(_CurrentWebsiteConfig.ScanDocumentKey) Then
				xaApplicantInfo.LegacyScandocAvailable = True
				xaCoApplicantInfo.LegacyScandocAvailable = True
				'xaDriverlicenseScan._sDocScanMessage = Server.HtmlDecode(Common.getDocumentScanMessage(_CurrentWebsiteConfig))
				hdScanDocumentKey.Value = _CurrentWebsiteConfig.ScanDocumentKey
			End If
		End If

		xaApplicantInfo.EnableMotherMaidenName = CheckShowField("divMotherMaidenName", "", True) Or (IsInMode("777") AndAlso IsInFeature("visibility"))
		xaCoApplicantInfo.EnableMotherMaidenName = CheckShowField("divMotherMaidenName", "co_", True) Or (IsInMode("777") AndAlso IsInFeature("visibility"))
		xaMinorApplicantInfo.EnableMotherMaidenName = CheckShowField("divMotherMaidenName", "m_", True) Or (IsInMode("777") AndAlso IsInFeature("visibility"))

		xaApplicantInfo.EnableCitizenshipStatus = CheckShowField("divCitizenshipStatus", "", True) Or (IsInMode("777") AndAlso IsInFeature("visibility"))
		xaCoApplicantInfo.EnableCitizenshipStatus = CheckShowField("divCitizenshipStatus", "co_", True) Or (IsInMode("777") AndAlso IsInFeature("visibility"))
		xaMinorApplicantInfo.EnableCitizenshipStatus = CheckShowField("divCitizenshipStatus", "m_", True) Or (IsInMode("777") AndAlso IsInFeature("visibility"))

		xaApplicantInfo.EnableGender = CheckShowField("divGender", "", True) Or (IsInMode("777") AndAlso IsInFeature("visibility"))
		xaCoApplicantInfo.EnableGender = CheckShowField("divGender", "co_", True) Or (IsInMode("777") AndAlso IsInFeature("visibility"))
		xaMinorApplicantInfo.EnableGender = CheckShowField("divGender", "m_", True) Or (IsInMode("777") AndAlso IsInFeature("visibility"))


		xaApplicantInfo.MaritalStatusDropdown = maritalStatusDropdown
		xaCoApplicantInfo.MaritalStatusDropdown = maritalStatusDropdown
		xaMinorApplicantInfo.MaritalStatusDropdown = maritalStatusDropdown
		xaApplicantInfo.EnableMaritalStatus = CheckShowField("divMaritalStatus", "", False) Or (IsInMode("777") AndAlso IsInFeature("visibility"))
		xaCoApplicantInfo.EnableMaritalStatus = CheckShowField("divMaritalStatus", "co_", False) Or (IsInMode("777") AndAlso IsInFeature("visibility"))
		xaMinorApplicantInfo.EnableMaritalStatus = CheckShowField("divMaritalStatus", "m_", False) Or (IsInMode("777") AndAlso IsInFeature("visibility"))

		'get configuration of employment
		'_ShowEmployment = Common.getNodeAttributes(_CurrentWebsiteConfig, strVisbileNode, "employment")

		''hidden secondary member number
		If _LoanType = "SA" Then
			xaApplicantInfo.EnableMemberNumber = CheckShowField("divMemberNumber", "", False) Or (IsInMode("777") AndAlso (IsInFeature("visibility") OrElse IsInFeature("rename")))
			xaCoApplicantInfo.EnableMemberNumber = CheckShowField("divMemberNumber", "co_", False) Or (IsInMode("777") AndAlso (IsInFeature("visibility") OrElse IsInFeature("rename")))
			xaMinorApplicantInfo.EnableMemberNumber = CheckShowField("divMemberNumber", "m_", False) Or (IsInMode("777") AndAlso (IsInFeature("visibility") OrElse IsInFeature("rename")))

			Dim memberNumberRequired As Boolean = Common.getNodeAttributes(_CurrentWebsiteConfig, "XA_LOAN/BEHAVIOR", "require_mem_number_secondary").ToUpper().Equals("Y")
			xaApplicantInfo.MemberNumberRequired = memberNumberRequired
			xaCoApplicantInfo.MemberNumberRequired = memberNumberRequired
			xaMinorApplicantInfo.MemberNumberRequired = memberNumberRequired
		End If
		xaApplicantInfo.ContentDataTheme = _ContentDataTheme
		xaMinorApplicantInfo.ContentDataTheme = _ContentDataTheme
		xaCoApplicantInfo.ContentDataTheme = _ContentDataTheme

		Dim uploadDocsMessage As String = Common.getXAUploadDocumentMessage(_CurrentWebsiteConfig)
		If Common.getXAUploadDocumentEnable(_CurrentWebsiteConfig) AndAlso Not String.IsNullOrEmpty(uploadDocsMessage) Then
			Dim docTitleRequired = Not Common.getNodeAttributes(_CurrentWebsiteConfig, "XA_LOAN/BEHAVIOR", "required_doc_title").Equals("N", StringComparison.OrdinalIgnoreCase)
			Dim docTitleOptions As List(Of CDocumentTitleInfo) = GenerateDocTitleOptions()

			ucDocUpload.Title = Server.HtmlDecode(uploadDocsMessage)
			ucDocUpload.RequireDocTitle = docTitleRequired
			ucDocUpload.DocTitleOptions = docTitleOptions
			ucDocUpload.DocUploadRequired = _requiredUpLoadDocsXA.Equals("Y", StringComparison.OrdinalIgnoreCase)

			ucCoDocUpload.Title = Server.HtmlDecode(uploadDocsMessage)
			ucCoDocUpload.RequireDocTitle = docTitleRequired
			ucCoDocUpload.DocTitleOptions = docTitleOptions
			ucCoDocUpload.DocUploadRequired = _requiredUpLoadDocsXA.Equals("Y", StringComparison.OrdinalIgnoreCase)
			ucDocUpload.Visible = True
			ucCoDocUpload.Visible = True
			ucDocUploadSrcSelector.Visible = True
			ucCoDocUploadSrcSelector.Visible = True
		Else
			ucDocUpload.Visible = False
			ucCoDocUpload.Visible = False
			ucDocUploadSrcSelector.Visible = False
			ucCoDocUploadSrcSelector.Visible = False
		End If
		_HasEnableSecondaryUpLoadDocs = Common.getNodeAttributes(_CurrentWebsiteConfig, "XA_LOAN/VISIBLE", "camera_xa_secondary_enable")
		''end upload
		'' app_type ='FOREIGN'
		Dim appType As String = _CurrentWebsiteConfig.AppType
		hdForeignAppType.Value = ""	'' initial hidden value foreign
		''Make sure appType is not nothing
		If Not String.IsNullOrEmpty(appType) Then
			If appType.ToUpper() = "FOREIGN" Then
				hdForeignAppType.Value = "Y"
			End If
		End If
		xaDriverlicenseScan.CurrentConfig = _CurrentWebsiteConfig
		xaCoDriverlicenseScan.CurrentConfig = _CurrentWebsiteConfig
		xaApplicantInfo.MappedShowFieldLoanType = mappedShowFieldLoanType
		xaCoApplicantInfo.MappedShowFieldLoanType = mappedShowFieldLoanType
		xaMinorApplicantInfo.MappedShowFieldLoanType = mappedShowFieldLoanType

		xaApplicantAddress.MappedShowFieldLoanType = mappedShowFieldLoanType
		xaCoApplicantAddress.MappedShowFieldLoanType = mappedShowFieldLoanType
		xaMinorApplicantAddress.MappedShowFieldLoanType = mappedShowFieldLoanType

		xaApplicantEmployment.MappedShowFieldLoanType = mappedShowFieldLoanType
		xaCoApplicantEmployment.MappedShowFieldLoanType = mappedShowFieldLoanType
		xaMinorApplicantEmployment.MappedShowFieldLoanType = mappedShowFieldLoanType
		xaApplicantID.MappedShowFieldLoanType = mappedShowFieldLoanType
		xaCoApplicantID.MappedShowFieldLoanType = mappedShowFieldLoanType
		xaMinorApplicantID.MappedShowFieldLoanType = mappedShowFieldLoanType


	End Sub

	Private Function GenerateDocTitleOptions() As List(Of CDocumentTitleInfo)
		Dim docTitleOptions As List(Of CDocumentTitleInfo) = Nothing
		Dim clrDocTitle = New List(Of CDocumentTitleInfo)
		' XA doc titles from Custom List Rules
		If CustomListRuleList IsNot Nothing AndAlso CustomListRuleList.Any(Function(c) c.Code = "DOCUMENT_TITLES") Then
			Dim xaClrDocTitle = EvaluateCustomList(Of CDocumentTitleInfo)("DOCUMENT_TITLES")

			If xaClrDocTitle IsNot Nothing Then
				xaClrDocTitle.ForEach(Sub(docTitle) docTitle.LoanType = "XA")
				clrDocTitle.AddRange(xaClrDocTitle)
			End If
		End If

		If clrDocTitle.Count = 0 Then
			' Fallback to ENUMS doc titles
			docTitleOptions = Common.GetDocumentTitleInfoList(_CurrentWebsiteConfig._webConfigXML.SelectSingleNode("ENUMS/XA_DOC_TITLE"))
		Else
			' Fill out options
			docTitleOptions = New List(Of CDocumentTitleInfo) From {New CDocumentTitleInfo() With {.Group = "", .Code = "", .Title = "--Please select title--"}}
			docTitleOptions.AddRange(clrDocTitle.OrderBy(Function(t) t.Title))
		End If
		Return docTitleOptions
	End Function

	Private Sub InitFundingOptionUc(availability As String)

		_ProductPackageJsonString = CProduct.GetJsonProductsPackage(_CurrentWebsiteConfig, availability)

		Dim fundingSource As CFundingSourceInfo
		If IsCheckFundingPage() = True Then	 'funding page is enabled
			fundingSource = CFundingSourceInfo.CurrentJsonFundingPackage(_CurrentWebsiteConfig)
		Else
			fundingSource = New CFundingSourceInfo(New CLenderServiceConfigInfo())
		End If
		'' try to append PAYPAL funding option into _FundingSourcePackageJsonString since its config stay in WEBSITE node
		If fundingSource.ListTypes IsNot Nothing AndAlso fundingSource.ListTypes.Any AndAlso Not String.IsNullOrEmpty(_CurrentWebsiteConfig.PayPalEmail) Then
			'' make sure PAYPAL option stay on top(below not funding option)
			fundingSource.ListTypes.Insert(1, "PAYPAL")
		End If
		'' serialize fundingSource into JSON then .aspx can access it in Page_PreRender 
		_FundingSourcePackageJsonString = JsonConvert.SerializeObject(fundingSource)
		xaFundingOptions.FundingTypeList = fundingSource.ListTypes


		'' CC maxfunding
		If Not String.IsNullOrEmpty(_CurrentWebsiteConfig.CCMaxFunding) Then
			_CCMaxFunding = _CurrentWebsiteConfig.CCMaxFunding
			xaFundingOptions.CCMaxFunding = _CCMaxFunding
		End If
		'' ACH maxfunding
		If Not String.IsNullOrEmpty(_CurrentWebsiteConfig.ACHMaxFunding) Then
			_ACHMaxFunding = _CurrentWebsiteConfig.ACHMaxFunding
			xaFundingOptions.ACHMaxFunding = _ACHMaxFunding
		End If
		'' Paypal maxfunding
		If Not String.IsNullOrEmpty(_CurrentWebsiteConfig.PaypalMaxFunding) Then
			_PaypalMaxFunding = _CurrentWebsiteConfig.PaypalMaxFunding
			xaFundingOptions.PaypalMaxFunding = _PaypalMaxFunding
		End If
		_xaStopInvalidRoutingNumber = Common.getNodeAttributes(_CurrentWebsiteConfig, "XA_LOAN/BEHAVIOR", "stop_invalid_routing_number")
		_xaFundingAccountNumberFreeForm = Common.getNodeAttributes(_CurrentWebsiteConfig, "XA_LOAN/BEHAVIOR", "funding_account_number_free_form")
        ''chechmailaddress
        If Not String.IsNullOrEmpty(Common.getCheckMailAddress(_CurrentWebsiteConfig)) Then
            _CheckMailAddress = Server.HtmlDecode(Common.getCheckMailAddress(_CurrentWebsiteConfig))
        End If

        xaFundingOptions.FundingDisclosure = Common.getFundingDisclosure(_CurrentWebsiteConfig, availability)
    End Sub
	Private Sub InitCustomQuestion()
		xaApplicationQuestionLoanPage.Header = HeaderUtils.RenderPageTitle(0, "Please answer the following question(s)", True)
		xaApplicationQuestionReviewPage.Header = HeaderUtils.RenderPageTitle(0, "Please answer question(s) below", True)
	End Sub
	Private Sub InitDisclosure()
		If CheckShowField("divDisclosureSection", "", True) Or (IsInMode("777") AndAlso (IsInFeature("visibility") OrElse IsInFeature("rename"))) Then
			''TODO: refactor to disclosure control
			'' Set disclosures

			'Dim disclosures As List(Of String) = _CurrentWebsiteConfig.GetLoanDisclosures("XA_LOAN")
			'If _Availability = "2" Or _Availability = "2a" Or _Availability = "2b" Then
			'	Dim secondaryDisclosures As List(Of String) = Common.getSecondaryDisclosures(_CurrentWebsiteConfig)
			'	If secondaryDisclosures.Count > 0 Then
			'		disclosures = secondaryDisclosures
			'	End If
			'End If
			Dim disclosures As List(Of String) = Common.GetDisclosures(_CurrentWebsiteConfig, "XA", False, _Availability)
			ucDisclosures.Disclosures = disclosures

			' ''verify code
			Dim reasonableDemoCode As String = Common.getNodeAttributes(_CurrentWebsiteConfig, "XA_LOAN/VISIBLE", "reasonable_demonstration_code")
			ucDisclosures.strRenderingVerifyCode = Common.RenderingVerifyCodeField(reasonableDemoCode, _CurrentWebsiteConfig)

			ucDisclosures.MappedShowFieldLoanType = mappedShowFieldLoanType
			ucDisclosures.EnableEmailMeButton = CheckShowField("divEmailMeButton", "", False) Or (IsInMode("777") AndAlso (IsInFeature("visibility") OrElse IsInFeature("rename")))
			''get credit pull message from XA_LOAN if it exist
			Dim oXANode As XmlNode = _CurrentWebsiteConfig._webConfigXML.SelectSingleNode("XA_LOAN/CREDIT_PULL_MESSAGE")
			If oXANode IsNot Nothing AndAlso Not String.IsNullOrEmpty(oXANode.InnerText) Then
				_CreditPullMessage = oXANode.InnerText
			Else
				''credit pull messagein disclosure section
				Dim oNode As XmlNode = _CurrentWebsiteConfig._webConfigXML.SelectSingleNode("CREDIT_PULL_MESSAGE")
				If oNode IsNot Nothing Then
					_CreditPullMessage = oNode.InnerText
				End If

			End If
		End If
	End Sub

	Private Sub InitTheme()


		'asign config theme for PageHeader UC, avoid reading config file unnescessary
		ucPageHeader._headerDataTheme = _HeaderDataTheme
		ucPageHeader._footerDataTheme = _FooterDataTheme
		ucPageHeader._contentDataTheme = _ContentDataTheme
		ucPageHeader._headerDataTheme2 = _HeaderDataTheme2
		ucPageHeader._backgroundDataTheme = _BackgroundTheme
		ucPageHeader._buttonDataTheme = _ButtonTheme
		ucPageHeader._buttonFontColor = _ButtonFontTheme
	End Sub

	Private Function HasEnableJointXA() As String
		Dim oJointEnableNode As XmlNode = _CurrentWebsiteConfig._webConfigXML.SelectSingleNode("XA_LOAN/VISIBLE/@joint_enable")
		If oJointEnableNode IsNot Nothing Then
			Return oJointEnableNode.Value
		End If
		Return ""
	End Function

	Private Function HasEnableJointSA() As String
		Dim oJointEnableXANode As XmlNode = _CurrentWebsiteConfig._webConfigXML.SelectSingleNode("XA_LOAN/VISIBLE/@joint_enable_sa")
		If oJointEnableXANode IsNot Nothing Then
			Return oJointEnableXANode.Value
		End If
		Return ""
	End Function

	Private Function IsForeignContact() As String
		Dim oForeignContactNode As XmlNode = _CurrentWebsiteConfig._webConfigXML.SelectSingleNode("XA_LOAN/VISIBLE/@foreign_contact")
		If oForeignContactNode IsNot Nothing Then
			Return oForeignContactNode.Value
		End If
		Return ""
	End Function

	Private Function IsCheckFundingPage() As Boolean
		Dim oFundingNode As XmlNode = _CurrentWebsiteConfig._webConfigXML.SelectSingleNode("XA_LOAN/VISIBLE/@funding")
		If oFundingNode IsNot Nothing AndAlso oFundingNode.Value.Equals("N") Then
			Return False
		End If
		Return True
	End Function

	Private Function EligibilityMessage(Optional avaibility As String = "") As String
		Dim elMessage As String = ""
		Dim strDefaultElMessage As String = Common.GenerateDefaultEligibilityMessage()	 ''default Eligibilty message

		Dim oElMessagedNode As XmlNode = Nothing
		If avaibility = "1s" Then
			oElMessagedNode = _CurrentWebsiteConfig._webConfigXML.SelectSingleNode("XA_LOAN/SPECIAL_ACCOUNT_ELIGIBILITY_MESSAGE")
		ElseIf avaibility = "1b" Then
			oElMessagedNode = _CurrentWebsiteConfig._webConfigXML.SelectSingleNode("XA_LOAN/BUSINESS_ACCOUNT_ELIGIBILITY_MESSAGE")
		End If
		If oElMessagedNode Is Nothing Then
			oElMessagedNode = _CurrentWebsiteConfig._webConfigXML.SelectSingleNode("XA_LOAN/ELIGIBILITY_MESSAGE")
		End If
		If oElMessagedNode IsNot Nothing Then
			If (Not String.IsNullOrEmpty(oElMessagedNode.InnerText)) Then
				elMessage = Server.HtmlDecode(oElMessagedNode.InnerText)
			Else
				elMessage = strDefaultElMessage
			End If

		Else
			elMessage = strDefaultElMessage	'' get the defaut message if there is no eli message in config
		End If
		Return elMessage
	End Function

	'Public Function hasSpecialAccountCode(ByVal accountCodeList As List(Of String), ByVal accountCode As String) As Boolean
	'	Dim sIndex As Integer
	'	For sIndex = 0 To accountCodeList.Count - 1
	'		If accountCode = accountCodeList(sIndex) Then
	'			Return True
	'		End If
	'	Next
	'	Return False
	'End Function
	Public Function FilterSpecialAccountType(ByVal oProductList As List(Of CProduct), ByVal oSpecialAcountType As Dictionary(Of String, String)) As Dictionary(Of String, String)
		Dim currSpecialAccountType As New Dictionary(Of String, String)
		Dim currSpecialAccountCode As New List(Of String)
		For Each oProdNode As CProduct In oProductList
			If oProdNode.MinorAccountCode IsNot Nothing AndAlso oProdNode.MinorAccountCode.Contains("|") Then ''take care the product has more than one minor account codes
				Dim currentList As String() = oProdNode.MinorAccountCode.Split("|"c)
				For Each accountCode As String In currentList
					If Not currSpecialAccountCode.Contains(accountCode) Then
						currSpecialAccountCode.Add(accountCode)
					End If
				Next
			Else
				If Not String.IsNullOrEmpty(oProdNode.MinorAccountCode) AndAlso Not currSpecialAccountCode.Contains(oProdNode.MinorAccountCode) Then
					currSpecialAccountCode.Add(oProdNode.MinorAccountCode)
				End If
			End If
		Next

		For i As Integer = 0 To currSpecialAccountCode.Count - 1
			If oSpecialAcountType.ContainsKey(currSpecialAccountCode(i)) Then
				Dim value As String = oSpecialAcountType.Item(currSpecialAccountCode(i))
				If Not currSpecialAccountType.ContainsKey(currSpecialAccountCode(i)) Then
					currSpecialAccountType.Add(currSpecialAccountCode(i), value)
				End If
			End If
		Next
		Return currSpecialAccountType
	End Function

	'Protected Sub testApplicationSubmission()
	'    ' Define the name and type of the client scripts on the page. 
	'    Dim csname1 As [String] = "testApplicationScript"
	'    Dim cstype As Type = Me.[GetType]()

	'    ' Get a ClientScriptManager reference from the Page class. 
	'    Dim cs As ClientScriptManager = Page.ClientScript

	'    ' Check to see if the startup script is already registered. 
	'    If Not cs.IsStartupScriptRegistered(cstype, csname1) Then
	'        Dim cstext1 As New StringBuilder()
	'        cstext1.Append("<script type=text/javascript> submitTestData() </script>")
	'        cs.RegisterStartupScript(cstype, csname1, cstext1.ToString())
	'    End If
	'End Sub

	'Protected Function RenderingPageFOMHeader() As String
	'    Dim strFomQuestion As String = ""
	'    'strFomQuestion += "<div data-role='page' id='pageFom' runat ='server' >"
	'    'strFomQuestion += "<div data-role='header'  data-theme='" & _HeaderDataTheme & "' data-position=''>"
	'    'strFomQuestion += "     <h1>Membership Eligibility</h1>"
	'    'strFomQuestion += "</div>"
	'    'strFomQuestion += "<div data-role='content' data-theme='" & _ContentDataTheme & "'>"
	'    'strFomQuestion += "" & EligibilityMessage()
	'    strFomQuestion += HeaderUtils.RenderPageTitle(0, "Select your eligibility<span class='require-span'>*</span>", True)
	'    'strFomQuestion += "<div><span class='ProductCQ-Font'><br/>Please select your eligibility</span><span style='color:red'>*</span></div>"
	'    Return strFomQuestion
	'End Function
	'NOT USED ANYWHERE
	'Protected Function RenderingPageFOMFooter() As String
	'    Dim strFomQuestion As String = ""
	'    ''strFomQuestion += "</div><div data-role='footer' data-theme='" & _FooterDataTheme & "'>"
	'    '' strFomQuestion += "<a href='' style='visibility: hidden;'></a>"
	'    '' strFomQuestion += "<a href='#' data-icon='arrow-r' data-iconpos='right' data-transition='slide' onclick='validateActivePage(this, true);' "
	'    ''strFomQuestion += "class='NavIconRight'>Next</a>"
	'    ''strFomQuestion += "<a href='#divErrorDialog' style='display: none;'></a></div></div>"
	'    ''new format
	'    strFomQuestion += "</div><div class='div-continue' data-role='footer' data-theme='" & _ContentDataTheme & "'>"
	'    strFomQuestion += "<a href='#'  data-transition='slide' onclick='validateActivePage(this, true);' data-theme='" & _FooterDataTheme & "' type='button' class='div-continue-button'>Continue</a>"
	'    strFomQuestion += "<a href='#divErrorDialog' style='display: none;'></a></div></div>"
	'    Return strFomQuestion
	'End Function

	'NOT USED ANYWHERE
	'Protected Function IsSubmitted(ByVal responseXml As String) As Boolean
	'	Dim xmlDoc As New XmlDocument()
	'	xmlDoc.LoadXml(responseXml)
	'	Return (xmlDoc.GetElementsByTagName("ERROR").Count = 0)
	'End Function
End Class