﻿Imports System.Xml
Imports System.Xml.Serialization
Imports System.Threading
Imports System.IO
Imports System.Net

Partial Class getConfigSite_testGetConfigSite
    Inherits System.Web.UI.Page

	Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
		CPBLogger.PBLogger.Component = Me.GetType.ToString
		CPBLogger.PBLogger.Context = Request.UserHostAddress
		CPBLogger.logInfo("test ipad config page called")
		Dim url As String = "http://lpqmobile.localhost/getConfigSite/getConfigSite.aspx"

		Dim req As WebRequest = WebRequest.Create(url)

		req.Method = "POST"
		'req.ContentType = "text/xml"

		'Dim writer As New StreamWriter(req.GetRequestStream())
		''writer.Write(Xml)
		'writer.Close()

		Dim encoding As New ASCIIEncoding()
		Dim postData As String = "lender_id=9031c7f53eed453b987367e26bd37d43"
		postData += "&organization_id=f63c948cb9fa4614b1eee603dc2ab94b"

		Dim data() As Byte
		data = encoding.GetBytes(postData)

		'prepare to send the data
		req.ContentType = "application/x-www-form-urlencoded"
		req.ContentLength = data.Length
		Dim newStream As Stream = req.GetRequestStream()
		newStream.Write(data, 0, data.Length)
		newStream.Close()

		'read the response and send it back
		Dim webResponse As WebResponse = req.GetResponse()
		Dim enc As Encoding = System.Text.Encoding.GetEncoding(1252)
		Dim readStream As StreamReader = New StreamReader(webResponse.GetResponseStream(), enc)

		'show the response
		Dim responseSTR As String = readStream.ReadToEnd()
		CPBLogger.logInfo("config response: " & responseSTR)
		'lblResponse.Text = readStream.ReadToEnd()
		serverResponseLabel.Text = responseSTR

	End Sub
End Class
