﻿Imports log4net
Imports LPQMobile.BO
Imports System.Xml
Imports System.Data
Imports LPQMobile.Utils
Imports LPQMobile.DBManager

''' <summary>
''' iPad XA and LPQ pro call this service. This service shall load config in cascade from live db then test db.  load from xml has been comment out and no longer support
''' </summary>
''' <remarks></remarks>
Partial Class getConfigSite_getConfigSite
    Inherits System.Web.UI.Page
	Private _log As log4net.ILog = log4net.LogManager.GetLogger(GetType(getConfigSite_getConfigSite))
#Region "Fields"

    Private ReadOnly Property OrganizationID() As String
        Get
            Return Common.SafeString(Request("organization_id"))
        End Get
    End Property

    Private ReadOnly Property LenderID() As String
        Get
            Return Common.SafeString(Request("lender_id"))
        End Get
    End Property

#End Region

#Region "Page Events"

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        CPBLogger.PBLogger.Component = Me.GetType.ToString
        CPBLogger.PBLogger.Context = Request.UserHostAddress
		_log.info("ipad config page called")
		If (Not IsPostBack) Then
			getXMLConfig()
		End If
	End Sub

#End Region

#Region "Private Functions"

	Private Sub getXMLConfig()
		Try
			Dim oManager As New CLenderConfigManager()
			Dim foundMatch As Boolean = False
			Dim hasData As Boolean = False
			Dim OutputDoc As New XmlDocument
			Dim siteConfigXML As New XmlDocument
			Dim oLender As CLenderConfig = oManager.GetLenderConfigByLenderAndOrg(Common.SafeGUID(OrganizationID), Common.SafeGUID(LenderID))

			'load from live db
			If Not oLender Is Nothing Then
				Dim configData As String = Common.SafeString(oLender.ConfigData)
				If (configData <> "") Then
					siteConfigXML.LoadXml(configData)
					hasData = True
				End If
			End If

			'load from apptest db
			If hasData = False Then
				oLender = oManager.GetLenderConfigByLenderAndOrg_fromTestDB(Common.SafeGUID(OrganizationID), Common.SafeGUID(LenderID))
			End If
			If Not oLender Is Nothing Then
				Dim configData As String = Common.SafeString(oLender.ConfigData)
				If (configData <> "") Then
					siteConfigXML.LoadXml(configData)
					hasData = True
				End If
			End If

			If hasData = True Then
				outPutXMLConfig(siteConfigXML, OutputDoc, foundMatch)
				'Else 'load from xml, no longer support loading from file, this is use of ipad only
				'	siteConfigXML.Load("C:\LPQMobileWebsiteConfig\config.xml")	'live
				'	outPutXMLConfig(siteConfigXML, OutputDoc, foundMatch)

				'	If foundMatch = False Then
				'		siteConfigXML.Load("C:\LPQMobileWebsiteConfigTest\config.xml") 'beta
				'		outPutXMLConfig(siteConfigXML, OutputDoc, foundMatch)
				'	End If
			End If

			Response.Clear()
			If foundMatch Then
				_log.Info("Config was found for orgID: " & OrganizationID & "  lenderID: " & LenderID & "lenderref: " & oLender.LenderRef)
				Response.Write(OutputDoc.OuterXml)
			Else
				_log.info("No match was found for orgID: " & OrganizationID & "  lenderID: " & LenderID)
				Response.Write("No match was found")
			End If
		Catch ex As Exception
			_log.Error("Error while getXMLConfig", ex)
			Response.Write("There are some problems when getXMLConfig. Please try again.")
		End Try
		Response.End()
	End Sub

	Private Sub outPutXMLConfig(ByVal siteConfigXML As XmlDocument, ByRef OutputDoc As XmlDocument, ByRef foundMatch As Boolean)
		Dim child_nodes As XmlNodeList = siteConfigXML.GetElementsByTagName("WEBSITE")

		' Find the WEBSITE element that matches the LenderId and OrganizationId
		Dim parsedLenderID As String = ""
		Dim parsedOrgID As String = ""

		For Each child As XmlNode In child_nodes
			parsedLenderID = child.Attributes("lender_id").InnerXml
			parsedOrgID = child.Attributes("organization_id").InnerXml

			If (parsedLenderID.ToLower() = LenderID.ToLower() And parsedOrgID.ToLower() = OrganizationID.ToLower()) Then
				' We found it! Now we need to strip out the user and password
				child.Attributes("user").InnerXml = "xxx"
				child.Attributes("password").InnerXml = "xxx"

				' Now put the whole thing into a new XML document and return it
				OutputDoc.LoadXml(child.OuterXml)
				foundMatch = True
				Exit For
			End If
		Next

	End Sub

#End Region

#Region "Old Functions"

	'Sub getXMLConfig()
	'    ' Get the XML from the config file
	'    ' I am hardcoding the file location for testing convenience. It will be moved to the Common class later on.
	'    Dim siteConfigXML As New XmlDocument
	'    siteConfigXML.Load("C:\LPQMobileWebsiteConfig\config.xml")

	'    Dim OutputDoc As New System.Xml.XmlDocument
	'    Dim sLenderID As String = Request("lender_id")
	'    Dim sOrgID As String = Request("organization_id")
	'    'CPBLogger.logError(Request("lender_id"), New System.Exception("Missing LenderID"))
	'    ' Get all the elements named "Website"
	'    Dim child_nodes As XmlNodeList = siteConfigXML.GetElementsByTagName("WEBSITE")

	'    ' Find the WEBSITE element that matches the LenderId and OrganizationId
	'    Dim parsedLenderID As String = ""
	'    Dim parsedOrgID As String = ""
	'    Dim foundMatch As Boolean = False
	'    For Each child As XmlNode In child_nodes
	'        parsedLenderID = child.Attributes("lender_id").InnerXml
	'        parsedOrgID = child.Attributes("organization_id").InnerXml
	'        If (parsedLenderID.ToLower() = sLenderID.ToLower() And parsedOrgID.ToLower() = sOrgID.ToLower()) Then
	'            ' We found it! Now we need to strip out the user and password
	'            child.Attributes("user").InnerXml = "xxx"
	'            child.Attributes("password").InnerXml = "xxx"

	'            ' Now put the whole thing into a new XML document and return it
	'            OutputDoc.LoadXml(child.OuterXml)
	'            foundMatch = True
	'            Exit For
	'        End If
	'    Next

	'    'look in beta config bc this clilent may not be live
	'    If Not foundMatch Then
	'        siteConfigXML.Load("C:\LPQMobileWebsiteConfigTest\config.xml")
	'        child_nodes = siteConfigXML.GetElementsByTagName("WEBSITE")
	'        For Each child As XmlNode In child_nodes
	'            parsedLenderID = child.Attributes("lender_id").InnerXml
	'            parsedOrgID = child.Attributes("organization_id").InnerXml
	'            If (parsedLenderID.ToLower() = sLenderID.ToLower() And parsedOrgID.ToLower() = sOrgID.ToLower()) Then
	'                ' We found it! Now we need to strip out the user and password
	'                child.Attributes("user").InnerXml = "xxx"
	'                child.Attributes("password").InnerXml = "xxx"

	'                ' Now put the whole thing into a new XML document and return it
	'                OutputDoc.LoadXml(child.OuterXml)
	'                foundMatch = True
	'                Exit For
	'            End If
	'        Next
	'    End If


	'    Response.Clear()
	'    If foundMatch Then
	'        _log.info("Config was found for orgID: " & sOrgID & "  lenderID: " & sLenderID)
	'        Response.Write(OutputDoc.OuterXml)
	'    Else
	'        _log.info("No match was found for orgID: " & sOrgID & "  lenderID: " & sLenderID)
	'        Response.Write("No match was found")
	'    End If

	'    Response.End()
	'End Sub

#End Region

End Class
