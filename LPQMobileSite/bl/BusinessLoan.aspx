﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="BusinessLoan.aspx.vb" Inherits="bl_BusinessLoan"  EnableViewState="false" %>
<%@ Register Src="~/Inc/PageHeader.ascx" TagPrefix="uc" TagName="pageHeader" %>
<%@ Register Src="~/Inc/Disclosure.ascx" TagPrefix="uc" TagName="disclosure" %>
<%@ Register TagPrefix="uc1" TagName="Piwik" Src="~/Inc/Piwik/PiwikTracking.ascx" %>
<%@ Register TagPrefix="uc" TagName="pageFooter" Src="~/Inc/PageFooter.ascx" %>
<%@ Import Namespace="Newtonsoft.Json" %>
<%@ Import Namespace="System.Web.Optimization" %>
<%--<%@ Reference Control="~/xa/Inc/saApplicantInfo.ascx" %>--%>
<%--<%@ Reference Control="~/xa/Inc/saAccountInfo.ascx" %>--%>
<%--<%@ Reference Control="~/xa/Inc/baBusinessInfo.ascx" %>--%>
<%@ Reference Control="~/xa/Inc/baApplicantInfo.ascx" %>
<%@ Reference Control="~/xa/Inc/baBeneficialOwners.ascx" %>
<%@ Reference Control="~/bl/Inc/VehicleLoanInfo.ascx" %>
<%@ Reference Control="~/bl/Inc/CreditCardInfo.ascx" %>
<%@ Reference Control="~/bl/Inc/OtherLoanInfo.ascx" %>
<%@ Reference Control="~/bl/Inc/BusinessLoanInfo.ascx" %>
<%@ Reference Control="~/Inc/MainApp/ApplicationBlockRules.ascx" %>
<%@ Register Src="~/Inc/MainApp/xaApplicantQuestion.ascx" TagPrefix="uc" TagName="xaApplicantQuestion" %>

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">
<head id="Head1" runat="server">
    <title>LPQ Mobile</title>
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <%:Styles.Render("~/css/thirdparty/bootstrap") %>
    <uc:pageHeader ID="ucPageHeader" ScriptFolder="bl" runat="server" />
	<%:Styles.Render("~/css/thirdparty/custom") %>
	<script type="text/javascript">  
	    var OCCUPATIONLIST = '<%=_occupationList%>';
	    var BAPREFIXLIST = <%=JsonConvert.SerializeObject(BAPrefixList)%>;
        var SELECTEDAPPLICANTLIST =<%=JsonConvert.SerializeObject(selectedApplicantList)%>;
	</script>
  
</head>
<body class = "lpq_container no-funding" >
    <uc1:Piwik id="ucPiwik" runat="server" ></uc1:Piwik>
	<input type="hidden" id="hdPlatformSource" value="<%=PlatformSource%>" />
  	<input type="hidden" id="hfLenderRef" value='<%=_CurrentLenderRef%>' />
    <input type="hidden" id="hfBranchId" value='<%=_CurrentBranchId%>' />
    <input type="hidden" id="hdXAComboBranchId" value='<%=_XAComboBranchId%>' />
	<input type="hidden" id="hfLoanOfficerID" value='<%=_CurrentLoanOfficerId%>' />
	<input type="hidden" id="hfReferralSource" value='<%=_ReferralSource%>' />
    <input type ="hidden" id ="hdAddressKey" value ='<%= _address_key%>' />
    <input type="hidden" runat="server" id="hdForeignAppType" />
    <input type ="hidden" id="hdScanDocumentKey" value ="<%=_hasScanDocumentKey %>" />
  
    <input type="hidden" id="hdForeignContact" runat ="server"  />
    <input type="hidden" id="hdRedirectURL" value="<%=_RedirectURL%>" />
    <input type ="hidden" id="hdIdaMethodType" runat ="server" />
    <input type="hidden" runat="server" id="hdNumWalletQuestions" />
    <input type ="hidden" id="hdRequiredDLScan" value="<%=_requiredDLScan%>" />
    <input type="hidden" id="hdForeignPhone" value ="<%=_isForeignPhone%>" />
    <input type="hidden" id="hdForeignAddress" value="<%=_isForeignAddress%>"/>
    <input type="hidden" id="hdPrevEmploymentThreshold" value="<%=_previous_employment_threshold%>" />
    <input type="hidden" id="hdPrevAddressThreshold" value='<%= _previous_address_threshold%>' />
	<input type="hidden" id="hdEnableIDSection" value="<%=IIf(EnableIDSection, "Y", "N") %>"/>
	<input type="hidden" id="hdEnableIDSectionForCoApp" value="<%=IIf(EnableIDSection("co_"), "Y", "N") %>"/>
	<input type="hidden" id="hdEnableReferenceInformation" value="<%=IIf(CheckShowField("divReferenceInformation", "", True) Or (IsInMode("777") AndAlso IsInFeature("visibility")), "Y", "N") %>"/>
	<input type="hidden" id="hdEnableReferenceInformationForCoApp" value="<%=IIf(CheckShowField("divReferenceInformation","co_", True) Or (IsInMode("777") AndAlso IsInFeature("visibility")), "Y", "N") %>"/>
    <input type="hidden" id="hdIsComboMode" value="<%=IIf(IsComboMode, "Y", "N")%>"/>
    <input type="hidden" id="hdProceedMembership" value=""/>
	<input type="hidden" id="hdEmploymentDurationRequired"  value="<%=_isEmploymentDurationRequired%>" />
	<input type="hidden" id="hdHomePhoneRequired"  value="<%=_isHomephoneRequired%>" />
    <input type ="hidden" id="hdSelectedEntityType" value="" />
    <input type="hidden" id="hdIsCQNewAPI" value="<%=IIf(_isCQNewAPI, "Y", "N")%>" />
   
    <div>
        <!--loan information page -->
       <asp:PlaceHolder runat="server" ID="plhLoanInfoPage"></asp:PlaceHolder>
        <%--business info --%>
        <asp:PlaceHolder runat="server" ID="plhBusinessLoanInfo"></asp:PlaceHolder>
        <!--applicants -->
       <asp:PlaceHolder runat="server" ID="plhApplicantPagesWrapper"></asp:PlaceHolder>
        <!--beneficial owners -->
        <asp:PlaceHolder runat ="server" ID ="plhBusinessBeneficialOwnersPage"></asp:PlaceHolder>
        <div data-role="page" id="pageApplicationCancelled">
			<div data-role="header" style="display: none">
				<h1>Application Cancelled</h1>
			</div>
		  <div data-role="content">
			  <%=HeaderUtils.RenderLogoAndSteps(_LogoUrl, 0, "APPLY_CC")%>
			  <br/>
			  <div class="text-center">
			    <%=HeaderUtils.RenderPageTitle(0, "Application Cancelled", False)%>
				</div>
			  <br/>
			  <p class="text-center">We have cancelled your application.</p>
			  <p class="text-center">If this was a mistake and you would like to fix errors you can return to the application now.</p>
			  <br/>

				<div class="row div-continue" style="margin-top: 10px; text-align: center; width:100%;" data-role="footer">
					<div class="col-md-2 hidden-xs"></div>
					<div class="col-md-4 col-xs-12">
						<a href="#" onclick="ABR.FACTORY.closeApplicationBlockRulesDialog()" type="button" data-role="button" class="div-continue-button">Return to Application</a>	
					</div>
					<div class="col-md-4 col-xs-12">
						<a href="#" onclick="gotoToUrl(this,'<%=_RedirectURL%>')" data-role="button" type="button" class="div-continue-button">Close</a>
						</div>
					<div class="col-md-2 hidden-xs"></div>
				</div>
		   </div>
	 </div>
        <!--Colateral Page -->
        <!--comment out unuse pageColateral
          <div data-role="page" id="pageColateral">
             <div data-role="header" style="display: none">
            <h1>Colateral</h1>
                 </div>
            <div data-role="content">
                <label>Colateral content</label>
            </div>
         
            <div class ="div-continue"  data-role="footer">
                <a href="#pageDeclaration" data-transition="slide" onclick="" type="button" class="div-continue-button">Continue</a> 
                    <a href="#divErrorDialog" style="display: none;">no text</a>
                 Or <a href="#pageba_BeneficialOwner" class ="div-goback" data-corners="false" data-shadow="false" data-theme="reset"><span class="hover-goback"> Go Back</span></a> 
            </div>
        </div>
        -->
       <asp:PlaceHolder runat="server" ID="plhApplicationDeclined"></asp:PlaceHolder>
     <!--getting started(vl1): vehicle infor +scandoc + disclosure -->
         <div data-role="page" id="pagesubmit">
        <div data-role="header" style="display: none">
            <h1>Review and Submit </h1>
        </div>
        <div data-role="content">
            <%=HeaderUtils.RenderLogoAndSteps(_LogoUrl, 2, "APPLY_CC")%>
            <%=HeaderUtils.RenderPageTitle(22, "Review and Submit", False)%>
            <div id="reviewPanel" class="review-container container-fluid" data-role="fieldcontain">
                <!--view loan information-->
                     <%--Branch selections --%>
                 <div class="row" style="display :none">
                      <%=HeaderUtils.RenderPageTitle(0, "Branch Selection", True)%>
                  </div>
                 <div class="row panel BranchSelection"></div>
                <%=HeaderUtils.RenderPageTitle(0, "Business Loan Information", True)%>
                <% If _businessLoanType = "vehicle" Then %>
                    <div class="row panel ViewVehicleLoanInfo"></div>
                <%ElseIf _businessLoanType = "card" Then%>
                    <div class="row panel ViewCreditCardInfo"></div>  
                <%ElseIf _businessLoanType = "other" Then%>
                    <div class="row panel ViewOtherLoanInfo"></div>
                <%End If %>
                <!--view Business information -->
                   <%=HeaderUtils.RenderPageTitle(0, "Business Information", True)%>
                 <div class="row panel" data-section-name="business-info"></div>       
		
                <div class="row panel" data-section-name="ba-account-info"></div>
                <div> <!--applicant information -->
                  <% Dim index = 0
                      For Each oApp In selectedApplicantList%>
                    <%=HeaderUtils.RenderPageTitle(0, (IIf(index = 0, "Primary ", "") & "Applicant Information").ToString() & " (" & oApp.appTypeText & ")", True)%>
					<div data-role="<%=oApp.IDPrefix %>"></div>
                  <%index +=1
                      next%> 
                </div>

                <div>
                      <!--beneficial infor --> 
                  <%--BENEFICIAL OWNERS--%>
                    <div class="row beneficialOwnersTitle hidden">
                       <%=HeaderUtils.RenderPageTitle(0, "Beneficial Owner Information", True)%>      
                    </div>
                    <div class="row panel ViewBeneficialOwners hidden"></div>
                    <%--END BENEFICIAL OWNERS --%>
                </div>     
            </div>
            <!--custom questions  -->
              <uc:xaApplicantQuestion ID="xaApplicationQuestionReviewPage" LoanType="BL" IDPrefix="reviewpage_" CQLocation="ReviewPage" runat="server" />
                 <div id="divDisclosure">
			    <%If EnableDisclosureSection Then%>
                     <div id="divDisclosureSection" <%--=IIf(IsInMode("777") AndAlso IsInFeature("visibility"), "class=""showfield-section"" data-show-field-section-id='divDisclosureSection' data-section-name= 'Disclosure'", "") --%>>
                    <%=HeaderUtils.RenderPageTitle(17, "Read, Sign and Submit", True, isRequired:=True)%>
                        <div style="margin-top: 10px;">
                            <div id="divSubmitMessage">
					            <%If Not String.IsNullOrEmpty(_CreditPullMessage) Then%>
						            <p class="rename-able bold"><%=_CreditPullMessage%></p>
                                 <%Else%>  
						            <p class="rename-able bold">Your application is not complete until you read the disclosure below and click the “I Agree” button in order to submit your application.</p>
						            <p class="rename-able">You are now ready to submit your application! By clicking on "I agree", you authorize us to verify the information you submitted and may obtain your credit report. Upon your request, we will tell you if a credit report was obtained and give you the name and address of the credit reporting agency that provided the report. You warrant to us that the information you are submitting is true and correct. By submitting this application, you agree to allow us to receive the information contained in your application, as well as the status of your application.</p>
                              
                                <%End If%>
					        </div>
                            <uc:disclosure ID="ucDisclosures" LoanType="BL" runat="server" Name="disclosures" />                  
                        </div>
	                </div>
			    <%End If%>
             <div id="divDisagree" data-role="fieldcontain">
                <a href="#" class="header_theme2 shadow-btn chevron-circle-right-before" style="cursor: pointer; font-weight: bold;">I disagree</a>
            </div>
		    </div>
		</div>
        
        <div class="div-continue" data-role="footer">       
            <a href="#"  id="submitLoan" data-transition="slide" onclick="validatePageSubmit();" type="button" class="div-continue-button">I Agree</a> 
            <a href="#divErrorDialog" style="display: none;">no text</a>
            Or <a href="#pageba_BeneficialOwner" class ="div-goback" data-corners="false" data-shadow="false" data-theme="reset"><span class="hover-goback"> Go Back</span></a>   
             <a id="href_show_last_dialog" href="#bl_last" style="display: none;">no text</a>
            <a id="href_show_dialog_1" href="#divErrorDialog" data-rel="dialog" style="display: none;">no text</a>
        </div>
    </div>
  <div data-role="page" id="decisionPage">
        <%--<div data-role="header" data-theme="<%=_HeaderDataTheme%>" data-position="">
          <h1>User Declined</h1>
            </div> --%>
        <div data-role="content">
          <%=HeaderUtils.RenderLogoAndSteps(_LogoUrl, 3, "APPLY_CC")%>
          <%=HeaderUtils.RenderPageTitle(19, "Loan Submission Cancelled", False)%>
         <div style="padding:10px 10px 10px 10px;background-color:yellow;margin-bottom:15px;margin-top :10px;" class="rename-able"><%If Not String.IsNullOrEmpty(_SubmitCancelMessage) Then%><%=_SubmitCancelMessage%><%Else%>We're sorry you have declined to use our automated prequalification system. If you prefer, please come to our offices to apply or call our Consumer Loan Department and loan representative will be happily ready to assist you.<%End If%>
         </div>
         <div id="divRedirectURL">
        <%  If Not String.IsNullOrEmpty(_RedirectURL) Then%>
            <div  style="margin:10px 0px 10px 0px" class="RedirectUrlPage" >
             <a href ="<%=_RedirectURL%>" class="header_theme2 shadow-btn chevron-circle-right-before" style="font-size :18px" onclick="onClickReturnToMainPage(event);">Return to Main Page</a></div>  
         <div class="rename-able">If you decide you would like to go through our instant decision process, please click on the following link to return to your application summary page and submit the application for decisioning.</div>
         <div style="margin-top:5px" class="RedirectUrlPage" ><a class="header_theme2 shadow-btn chevron-circle-right-before" style="font-size :18px" id="btnReturnToApp" href="#pagesubmit">Return to Application</a></div>
         <%End If%>
     </div>
     </div>
  </div>
  
	    <!--end wallet questions -->
    <div data-role="page" id="bl_last">
        <div data-role="header" style="display: none">
			<h1>Application Completed</h1>
        </div>
        <div data-role="content">
            <%=HeaderUtils.RenderLogoAndSteps(_LogoUrl, 3, "APPLY_CC")%>
            <div id="div_system_message" >
            </div>        
        <%		If Not String.IsNullOrEmpty(_RedirectURL) Then%>
           <div class="div-continue" style="margin-top: 10px; text-align: center;"  data-role="footer">
	        <a id="btnRedirect" href="#" onclick="gotoToUrl(this,'<%=_RedirectURL%>')" type="button" class="div-continue-button">Return to our website</a> 
            <%--<input data-theme="<%=_FooterDataTheme%>" type="button" id="btnRedirect" class="btn-redirect  div-continue-button" data-theme="<%=_FooterDataTheme%>" url='<%=_RedirectURL%>'
                value="Return to our website" />--%>
        </div>
        <%End If%> 
        </div>
    </div>	
    <div id="divErrorDialog" data-role="dialog" style="min-height: 600px;">
        <div role="dialog">
             <div data-role="header" style="display:none" >
					  <h1  >Alert Popup</h1>
			 </div>
            <div data-role="content">
                  <div class="DialogMessageStyle">There was a problem with your request</div>
                <div style="margin: 5px 0 15px 0;">
                    <span id="txtErrorMessage"></span>
                </div>
                <a href="#" data-role="button" data-rel="back" type="button">Ok</a>
            </div>
        </div>
    </div>
	<div id="divErrorPopup" data-role="popup" data-dismissible="true" data-history="false" style="max-width: 500px;padding: 20px;">
		<div data-role="header" style="display:none" >
				<h1  >Alert Popup</h1>
		</div>
		<div data-role="content">
			<%--<div class="row">
				<div class="col-xs-12 header_theme2">
					<a data-rel="back" class="pull-right"><%=HeaderUtils.IconClose %></a>
				</div>
			</div>--%>
			<div class="row">
				<div class="col-sm-12">
					<div class="DialogMessageStyle">There was a problem with your request</div>
					<div style="margin: 5px 0 15px 0;">
						<span id="txtErrorPopupMessage"></span>
					</div>       
				</div>    
			</div>
			<div class="row text-center">
				<div class="col-xs-12 text-center">
					<a href="#" data-role="button" data-rel="back" type="button">Ok</a>
				</div>
			</div>
		</div>
	</div>
	<div id="popNotification" data-role="popup" data-dismissible="true" data-history="false" style="display:none;max-width: 500px;padding: 20px;">
		<div data-role="header" style="display:none" >
				<h1  >Notification Popup</h1>
		</div>
        <div data-role="content">
            
            <div class="row">
                <div class="col-sm-12">
	                <div class="DialogMessageStyle header_theme">Notification</div>
                    <div style="margin: 5px 0 15px 0;" placeholder="content"></div>       
                </div>    
            </div>
			<div class="row text-center">
				<div class="col-xs-12 text-center">
					<a href="#" data-role="button" onclick="closePopup('#popNotification')" type="button">Ok</a>
				</div>
			</div>
        </div>
    </div>
     <div id="divMessage" data-role="dialog" style="min-height: 444px;">
        <div role="dialog">
			<div data-role="header" style="display:none">
                <h1 class="ui-title" role="heading" aria-level="1">
					Information Popup    
                 </h1>
            </div>
            <div data-role="content" role="main"><br />
                <div style="margin: 5px 0 15px 0;">
                    <span id="txtMessage" style="color: Blue;"></span>
                </div>
                <br />
				<a href='#' data-role='button' type="button" id='OKButton'>Ok</a>
               <%--<button id ="OKButton" type ="button" type="button"><b>Ok</b> </button><br />--%>
            </div>
        </div>
    </div>

     <div data-role="dialog" id="divConfirmDialog">
        <div data-role="header" style="display:none" ><h1>Restart Popup</h1></div>
        <div data-role="content">                 
			<div style='margin: 5px 0 15px 0;'><span class="require-span">Restart will clear all data and return to the main page. Are you sure?</span></div>
			<div class='row' style="text-align: center;margin-top: 40px; margin-bottom: 10px;">
				<a href='#' type="button" style="width: 100px; display: inline;padding-left: 20px; padding-right: 20px;" data-role='button' id='btnOk'>Yes</a>
				<a href='#' type="button" style="width: 100px;display: inline;padding-left: 20px; padding-right: 20px;" data-role='button' data-rel='back' id='btnCancel'>No</a>
			</div>
        </div>
    </div>
   
	<div data-role="page" id="pageMaxAppExceeded">
			<div data-role="header" style="display: none">
				<h1>Access Denied</h1>
			</div>
		  <div data-role="content">
				<%=HeaderUtils.RenderLogo(_LogoUrl)%>
			  <br/>
			  <br/>
			  <div>You have exceeded the maximum number of applications per hour. Please try again later.</div>
			  <br/>
				<div class="div-continue" style="margin-top: 10px; text-align: center;" data-role="footer">
					<a href="#" onclick="gotoToUrl(this,'<%=_RedirectURL%>')" type="button" class="div-continue-button">Return to our website</a>
				</div>
		   </div>
	 </div>
       <div class="divfooter">
           <%=_strRenderFooterContact%>  
            <div style="text-align :center">
                 <!-- lender name -->
                <div class="divLenderName"><%=_strLenderName%></div>  
                <div class="divFooterLogo">          
                    <% If String.IsNullOrEmpty(_hasFooterRight) Then%>
                        <%-- 
                         <div><a href="<%=_strNCUAUrl%>"  class="ui-link" ><%=_strFooterRight%></a><a href="<%=_strHUDUrl%>" class="ui-link"> <img src="<%=_strLogoUrl%>" alt ="logo" class="footerimage-style img-responsive" /></a></div>                 
                         --%> 
                        <div class="divfooterlabel"><a href="<%=_strNCUAUrl%>"  class="ui-link" ><%=_strFooterRight%></a> <a href="<%=_strHUDUrl%>" class="ui-link"><%=_equalHousingText%></a></div>                                      
                     <% Else%>
                        <%=_strFooterRight%>
                    <%End If%>
                </div>  
                <div><%=_strFooterLeft  %></div>   
           </div>
          
       </div> 
       
  </div>
	<uc:pageFooter ID="ucPageFooter" runat="server" />
	<script type="text/javascript">
        $(function () {
	        //prefill first name and lastName
	        <% For Each oApp In selectedApplicantList%>	    
	        $('#<%=oApp.IDPrefix %>txtFName').val('<%=oApp.firstName  %>');
	        $('#<%=oApp.IDPrefix %>txtLName').val('<%=oApp.lastName  %>');
	        <%Next%>

	        // Fill in the fields with test data
	        if (getParaByName("autofill") == "true" &&( getParaByName("loantype") == "vehicle" ||getParaByName("loantype") == "card"||getParaByName("loantype") == "other")) {
	            // Fill in the stuf
	            if(getParaByName("loantype") == "vehicle"){
	                prefillVehicleLoanInfo();
	            }
	            prefillbl_BusinessInfo();
	            //   Dim idPrefix As String = "ba_" & role.Replace(" ", "") & "_" & index
              
	            <% For Each oApp In selectedApplicantList%>	  
                if ('<%=oApp.IDPrefix%>' == "ba_Primary_0") {
                    if (typeof <%=oApp.IDPrefix%>AutoFillData_AppicantInfo != "undefined") {
                         <%=oApp.IDPrefix%>AutoFillData_AppicantInfo();
                    }
	               
                    if (typeof <%=oApp.IDPrefix%>autoFillData_Address != "undefined") {
                        <%=oApp.IDPrefix%>autoFillData_Address();
                     }
                }
                  <%If _EnableBLDeclaration Then %>
                     <%=oApp.IDPrefix%>autofill_Declarations();
                  <%End If %>         
                <%Next%>
	        }
	        $("#pagesubmit").on("pagebeforeshow", function(event, ui) {       
	            var sLoanType='<%=_businessLoanType%>';
	            if(sLoanType=="card"){
	                RenderCreditCardInfoContent();
	            }else if(sLoanType=="vehicle"){
	                VL.FACTORY.renderReviewContent();
	            }else if(sLoanType=="other"){
	                RenderOtherLoanReviewContent();
	                //  $("div[data-section-name='creditca-info']", "#reviewPanel").html("");
	            }   
	            $("div[data-section-name='business-info']", "#reviewPanel").html(bl_BusinessInfoViewBAAccountInfo());
	            //view applicant 
	            <% For Each oApp In selectedApplicantList%>	    
	            $("div[data-role='<%=oApp.IDPrefix %>']", "#reviewPanel").html(<%=oApp.IDPrefix%>ViewBAAplicantInfo());	
	            //  $("div[data-section-name='<%=oApp.IDPrefix %>BeneficialOwner']","#reviewPanel").html(<%=oApp.IDPrefix%>viewBeneficialOwners());
	            <%Next%>
	                  
	            var beneficialOwnersViewHtml = ba_BeneficialOwnerviewBeneficialOwners();
	            if (beneficialOwnersViewHtml != "") {
	                $(".ViewBeneficialOwners").html(beneficialOwnersViewHtml);
	                $(".ViewBeneficialOwners").removeClass("hidden");
	                $(".beneficialOwnersTitle").removeClass("hidden");
	            } else {
	                $(".ViewBeneficialOwners").addClass("hidden");
	                $(".beneficialOwnersTitle").addClass("hidden");
	            }
	            //replace "current physical address" change to address for in review business information
	            $(this).find('div[class~="BusinessAddress"]').each(function () {
	                var $ele = $(this).next('div[class~="row"]').find('div[class~="row-title"]').first();
	                if ($ele) {
	                    $ele.text('Address');
	                }
	            });
		        pushToPagePaths("pagesubmit");
	        });	

            
	        $(document).on('click', '#divDisagree a', function (e) {
	            var redirectURL = $('#hdRedirectURL').val();
	            //don't popup confirm message--> go direct to return to mainpage
	            //displayConfirmMessage(redirectURL, isCoApp, onClickReturnToMainPage);               
                clearForms(); //clear data when the page is redicted to the main page
	            if (redirectURL == "") { //need to save loan infor 
	                onClickReturnToMainPage(e);
	            }
	            // location.hash = '#decisionPage'; --> location.hash is no working on IE browser
	            goToNextPage($('#decisionPage'));
	            e.preventDefault();
	        });
	    });
	    function validateDeclarations(){
	        var validators =[];
	        <% For Each oApp In selectedApplicantList %>	                
			    validators.push($.lpqValidate("<%=oApp.IDPrefix%>ValidateDeclaration"));
	        <%Next%>
	        var validator =true;
	        for(var i=0;i<validators.length;i++){
	            if(!validators[i]){
	                validator = false;
	            }
	        }
	        if(validator){
	            goToNextPage("#pagesubmit");   
	        }else{
	            Common.ScrollToError();
	        }
	    }
	    //move to here from script.js
	    function getBusinessLoanInfo() {
	        var appInfo = new Object();
	        //loan infor
	        appInfo.LenderRef = $('#hfLenderRef').val();
	        appInfo.Task = "SubmitLoan";
	        appInfo.selectedApplicantList= JSON.stringify(SELECTEDAPPLICANTLIST);
            appInfo.LoanType = '<%=_businessLoanType%>';
           //add UrlParamCustomQuestionAndAnswer if it exist                  
			if (typeof reviewpage_getAllUrlParaCustomQuestionAnswers == "function") {
				var UrlParaCustomQuestions = reviewpage_getAllUrlParaCustomQuestionAnswers();
				//add UrlParamCustomQuestionAndAnswer if it exist
				if (UrlParaCustomQuestions.length > 0) {
					appInfo.IsUrlParaCustomQuestion = "Y";
					appInfo.UrlParaCustomQuestionAndAnswers = JSON.stringify(UrlParaCustomQuestions);
				}
			}
	        //get the custom_question_answer
			appInfo.CustomAnswers = JSON.stringify(
				loanpage_getAllCustomQuestionAnswers()
					<% For Each sIdPrefix As String In BAPrefixList %>
					.concat(<%=sIdPrefix%>getAllCustomQuestionAnswers())
					<% Next %>
					.concat(reviewpage_getAllCustomQuestionAnswers()));
	        if(appInfo.LoanType=="card"){
	            getCreditCardInfo(appInfo, false);
	        }else if(appInfo.LoanType=="vehicle"){
	            VL.FACTORY.getVehicleLoanInfo(appInfo);
	        }else if(appInfo.LoanType=="other"){
	            getOtherLoanInfo(appInfo, true); 
	        }
	        //business infor
	        bl_BusinessInfoSetBAAccountInfo(appInfo);
	        //applicants Infor
            <% For Each oApp In selectedApplicantList %>	                
				<%=oApp.IDPrefix%>SetBAApplicantInfo(appInfo);
	    	<%Next%>  
	    	<%If _UploadDocEnable then %>
		    var docUploadInfo = {};
	    	<% For Each oApp In selectedApplicantList %>	                
				<%=oApp.IDPrefix%>SetBADocUploadInfo(docUploadInfo);
	    	<%Next%> 
	    	if (docUploadInfo.hasOwnProperty("sDocArray") && docUploadInfo.hasOwnProperty("sDocInfoArray")) {
	    		appInfo.Image = JSON.stringify(docUploadInfo.sDocArray);
	    		appInfo.UploadDocInfor = JSON.stringify(docUploadInfo.sDocInfoArray);
		    }
	    	
			<%End If%>
	        //beneficial owners
	        appInfo.bl_BeneficialOwner = JSON.stringify(ba_BeneficialOwnergetBeneficialOwners());
	        //colateral
	        //disclosure
	        if (parseDisclosures() != "") {
	            appInfo.Disclosure = parseDisclosures();
	        }
	        return appInfo;
	    }


		function prepareAbrFormValues(pageId) {
			var loanInfo = getBusinessLoanInfo();
			var formValues = {};
			_.forEach(pagePaths, function (p) {
				collectFormValueData(p.replace("#", ""), loanInfo, formValues);
				if (pageId == p.replace("#", "")) return false;
			});
			return formValues;
		}
		/**
		 * Converts the loanInfo, which is normally used on submission, into a data structure meant for Applicant Blocking Logic (ABR).
		 * This also only pulls in the information needed for each page. ABR should only work with data on the page the user is on
		 * and the pages before that.
		 * @param {string} pageId
		 * @param {Object} loanInfo
		 * @param {Object} formValues
		 */
		function collectFormValueData(pageId, loanInfo, formValues) {
			switch (pageId) {
				case "vehicleInfo":
					formValues["vehicleLoanPurpose"] = [loanInfo.LoanPurpose];
					formValues["amountRequested"] = loanInfo.ProposedLoanAmount;

					collectCustomQuestionAnswers("Application", "LoanPage", null, loanInfo, formValues);
					collectCustomQuestionAnswers("Applicant", "LoanPage", null, loanInfo, formValues);

					break;
				case "creditCardInfo":
					formValues["creditCardPurpose"] = [loanInfo.LoanPurpose];
					formValues["amountRequested"] = loanInfo.RequestAmount;
					formValues["cardType"] = [loanInfo.CreditCardType];

					collectCustomQuestionAnswers("Application", "LoanPage", null, loanInfo, formValues);
					collectCustomQuestionAnswers("Applicant", "LoanPage", null, loanInfo, formValues);

					break;
				case "otherLoanInfo":
					formValues["otherLoanPurpose"] = [loanInfo.LoanPurpose];
					formValues["amountRequested"] = loanInfo.LoanAmount;

					collectCustomQuestionAnswers("Application", "LoanPage", null, loanInfo, formValues);
					collectCustomQuestionAnswers("Applicant", "LoanPage", null, loanInfo, formValues);

					break;
				case "pagebl_BusinessInfo":
					formValues["businessType"] = [loanInfo.bl_BusinessInfoBusinessEntity];
					break;
				<% For Each oApp In selectedApplicantList %>	     
				case "page<%=oApp.IDPrefix%>":
					var _dob = moment(loanInfo.<%=oApp.IDPrefix%>DOB, "MM-DD-YYYY");
					if (Common.IsValidDate(loanInfo.<%=oApp.IDPrefix%>DOB)) {
						formValues["age"] = moment().diff(_dob, "years", false);
					}
					formValues["citizenship"] = loanInfo.<%=oApp.IDPrefix%>CitizenshipStatus;
					formValues["employeeOfLender"] = loanInfo.<%=oApp.IDPrefix%>EmployeeOfLender;
					formValues["employmentLength"] = (parseInt(loanInfo.<%=oApp.IDPrefix%>txtEmployedDuration_year) || 0) + (parseInt(loanInfo.<%=oApp.IDPrefix%>txtEmployedDuration_month) || 0) / 12;
					formValues["employmentStatus"] = loanInfo.<%=oApp.IDPrefix%>EmploymentStatus;
					formValues["isJoint"] = (loanInfo.<%=oApp.IDPrefix%>IsJoint=="Y");
					formValues["memberNumber"] = loanInfo.<%=oApp.IDPrefix%>MemberNumber;
					formValues["occupancyLength"] = loanInfo.<%=oApp.IDPrefix%>LiveMonths / 12;
					formValues["occupancyStatus"] = loanInfo.<%=oApp.IDPrefix%>OccupyingLocation;

					collectCustomQuestionAnswers("Applicant", "ApplicantPage", "<%=oApp.IDPrefix%>", loanInfo, formValues);

					break;
					<%Next%>
				case "pagesubmit":

					collectCustomQuestionAnswers("Application", "ReviewPage", null, loanInfo, formValues);
					collectCustomQuestionAnswers("Applicant", "ReviewPage", null, loanInfo, formValues);

					break;
				default:
					break;
			}
			return formValues;
		}
	    
	</script>
</body>
</html>
