﻿
function hasForeignTypeApp() {
    return $('#hdForeignAppType').val() == "Y";
}
function validatePageSubmit() {
    var validator = true;

	// Validate custom questions
	if ($('#reviewpage_divApplicantQuestion').children().length > 0) {
		if ($.lpqValidate("reviewpage_ValidateApplicantQuestionsXA") === false) {
			validator = false;
		}
	}
    if ($("#pagesubmit").find("#divDisclosure").length > 0 && $.lpqValidate("ValidateDisclosure_disclosures") == false) {
        validator = false;
    }
    if (validator) {
    	if (window.ABR) {
    		var formValues = prepareAbrFormValues("pagesubmit");
    		if (ABR.FACTORY.evaluateRules("pagesubmit", formValues)) {
    			return false;
    		}
    	}
        saveBusinessLoanInfo();
    } else {
        Common.ScrollToError();
    }
}
function saveBusinessLoanInfo() {
    //disable submit button before calling ajax to prevent user submit multiple times
    $('#pagesubmit .div-continue-button').addClass('ui-disabled');
    var loanInfo = getBusinessLoanInfo();
    loanInfo = attachGlobalVarialble(loanInfo);
    $.ajax({
        type: 'POST',
        url: 'CallBack.aspx',
        data: loanInfo,
        dataType: 'html',
        success: function (response) {
            //enable submit button 
            if (response.toUpperCase().indexOf('MLERRORMESSAGE') > -1) {
                //enable submit answer button 
                $('#pagesubmit .div-continue-button').removeClass('ui-disabled');
                goToDialog(response);
            } else if (response == "blockmaxapps") {
            	goToNextPage("#pageMaxAppExceeded");
            } else {
                goToLastDialog(response);           
            }
        },
        error: function (err) {
            //enable submit answer button 
            $('#pagesubmit .div-continue-button').removeClass('ui-disabled');        
            goToDialog('There was an error when processing your request.  Please try the application again.');
        }
    });
   
    return false;
}

function goToLastDialog(message) {
    //var decoded_message = $('<div/>').html(message).text(); //message maybe encoded in the config xml so need to decode
    // $('#div_system_message').html(decoded_message); ->already decoded in callback.aspx
    //application completed so clear
    clearForms();
    $('#div_system_message').html(message);
    //docusign window if enabled
    if ($('#div_system_message').find("iframe#ifDocuSign").length === 1) {
        appendDocuSign();
        $("#div_docuSignIframe").append($('#div_system_message').find("iframe#ifDocuSign"));

        ////goback to final decision message after docusign is closed
        //$('#div_docuSignIframe').find("iframe#ifDocuSign").on("load", function (evt) {
        //    //the initial page load twice so need this to make sure only capturing url on finish
        //    var flag = ($(this).data("loaded") || "") + "1";
        //    $(this).data("loaded", flag);
        //    if (flag === "111") {
        //        $('#href_show_last_dialog').trigger('click');
        //    }
        //});

        var height = window.innerHeight ||
                 document.documentElement.clientHeight ||
                 document.body.clientHeight;

        var footerHeight = $('.divfooter').height();

        height = height - footerHeight - 64;
        $('#div_docuSignIframe').find("iframe#ifDocuSign").attr("height", height);
        $.mobile.changePage("#popupDocuSign", {
            transition: "fade",
            reverse: false,
            changeHash: false
        });
    }
        //cross-sell link button if enabled
    else {

        $('#href_show_last_dialog').trigger('click');
    }
}
function goToDialog(message) {
    if ($("#divErrorPopup").length > 0) {
        $('#txtErrorPopupMessage').html(message);  //error message are not encoded, endoding will remove html tag
        $("#divErrorPopup").popup("open", { "positionTo": "window" });
    } else {
        $('#txtErrorMessage').html(message);  //error message are not encoded, endoding will remove html tag
        $('#href_show_dialog_1').trigger('click');
    }
}
