﻿Imports LPQMobile.BO
Imports LPQMobile.Utils
Imports System.Xml
Imports System.Web.Script.Serialization
Imports LPQMobile.Utils.Common
Imports System.Reflection

Partial Class bl_CallBack
	Inherits CBasePage

	Private QualifiedProducts As New System.Collections.Generic.List(Of System.Collections.Generic.Dictionary(Of String, String))
#Region "property that need to persist"
	''need to use for 2nsd postback
	Property loanRequestXMLStr As String
		Get
			Return Common.SafeString(Session("loanRequestXMLStr"))
		End Get
		Set(value As String)
			Session("loanRequestXMLStr") = value
		End Set
	End Property
	Property loanResponseXMLStr As String
		Get
			Return Common.SafeString(Session("loanResponseXMLStr"))
		End Get
		Set(value As String)
			Session("loanResponseXMLStr") = value
		End Set
	End Property

	Property loanID As String
		Get
			Return Common.SafeString(Session("loanID"))
		End Get
		Set(value As String)
			Session("loanID") = value
		End Set
	End Property
	Property loanNumber As String
		Get
			Return Common.SafeString(Session("loanNumber"))
		End Get
		Set(value As String)
			Session("loanNumber") = value
		End Set
	End Property
	Property proposedLoanAmount As String
		Get
			Return Common.SafeString(Session("proposedLoanAmount"))
		End Get
		Set(value As String)
			Session("proposedLoanAmount") = value
		End Set
	End Property
	Property loanTerm As String
		Get
			Return Common.SafeString(Session("loanTerm"))
		End Get
		Set(value As String)
			Session("loanTerm") = value
		End Set
	End Property

	Property Decision_v2_Response As String
		Get
			Return Common.SafeString(Session("Decision_v2_ResponseCC"))
		End Get
		Set(value As String)
			Session("Decision_v2_ResponseCC") = value
		End Set
	End Property

	Property DecisionMessage As String
		Get
			Return Common.SafeString(Session("DecisionMessage"))
		End Get
		Set(value As String)
			Session("DecisionMessage") = value
		End Set
	End Property

#End Region

	Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
		Dim sLoanType = Common.SafeString(Request.Form("LoanType"))
		Dim sLoadingMsg = ""
		If sLoanType = "vehicle" Then
			sLoadingMsg = "Business Vehicle"
		ElseIf sLoanType = "card" Then
			sLoadingMsg = "Business Credit Card"
		ElseIf sLoanType = "other" Then
			sLoadingMsg = "Business Other Loan Purpose"
		End If

		log.Debug(sLoadingMsg & ": Callback page is loading")
		If IsInEnvironment("LIVE") Then
			Dim editMode As Boolean = Common.SafeBoolean(Request.Form("eMode"))
			If Not String.IsNullOrEmpty(PreviewCode.Trim()) OrElse editMode = True Then
				Response.Clear()
				Response.Write(Common.PrependIconForNonApprovedMessage("[Submission result display here]", "Thank You"))
				Response.End()
				Return
			End If
		End If

		If (Request.Form("Task") = "SubmitLoan") Then
			Dim IP_ErrMessage As String = ValidateIP()
			If Not String.IsNullOrEmpty(IP_ErrMessage) Then
				Response.Write(IP_ErrMessage)
				log.Info(IP_ErrMessage & "  IP: " & Request.UserHostAddress & "/" & _CurrentLenderRef)
				Return
			End If
			''make sure to decode html in xml config
			Dim oResponseXml As String = ""
			Dim customErrorMessage = businessCustomErrorMessage()
			If sLoanType = "vehicle" Then
				DecisionMessage = Server.HtmlDecode(SubmitBusinessVehicleLoan(_CurrentWebsiteConfig, oResponseXml))
			ElseIf sLoanType = "card" Then
				DecisionMessage = Server.HtmlDecode(SubmitBusinessCreditCard(_CurrentWebsiteConfig, oResponseXml))
			ElseIf sLoanType = "other" Then
				DecisionMessage = Server.HtmlDecode(SubmitBusinessOtherLoan(_CurrentWebsiteConfig, oResponseXml))
			Else
				Response.Write(customErrorMessage)
				Return
			End If
			If String.IsNullOrEmpty(oResponseXml) Then
				Response.Write(customErrorMessage)
				Return
			End If
			Dim sLoanNumber As String = getLoanNumber(oResponseXml)
			If sLoanNumber = "" Or sLoanNumber Is Nothing Then
				Response.Write(customErrorMessage)
				Return
			End If

			''run ISTWatch for business beneficial owners
			Dim loanID = Common.getResponseLoanID(oResponseXml)
			CISTWatch.RunBusinessLoanISTWatch(loanRequestXMLStr, loanID, _CurrentWebsiteConfig)

			loanResponseXMLStr = oResponseXml ''for later use
			If _CurrentWebsiteConfig.SubmitLoanUrl.Contains("decisionloan/2.0") Then
				Decision_v2_Response = oResponseXml	'for later use
				QualifiedProducts = getQualifyProduct2_0(oResponseXml)
			End If

			If QualifiedProducts.Count = 0 Then
				DecisionMessage = businessCustomMessages(loanResponseXMLStr, loanRequestXMLStr, sLoanType.ToUpper & "_SUBMITTED_MESSAGE")
				Response.Write(DecisionMessage)
				Return
			Else
				DecisionMessage = businessCustomMessages(loanResponseXMLStr, loanRequestXMLStr, sLoanType.ToUpper & "_PREAPPROVED_MESSAGE")
			End If
			''run booking, checking for enable is done inside CBookingManager
			'Dim loanID = Common.getResponseLoanID(oResponseXml)
			'Dim sAccountNumber As String = CBookingManager.Book(_CurrentWebsiteConfig, loanID, "VL")

			'TODO:
			'do we need to update loan status and internal comment when booking fail and update success message
			'also add this for IDA loop and other loan
			Dim message As String = ""
			If _CurrentWebsiteConfig.SubmitLoanUrl.Contains("decisionloan/2.0") And QualifiedProducts.Count > 0 Then
				message = displayProduct2_0()
			End If
			Response.Write(message)
			Return
		End If
	End Sub
	Private Function GetVehicleLoanObject(ByVal poConfig As CWebsiteConfig) As CBusinessVehicleLoan
		Dim VehicleLoan As New CBusinessVehicleLoan(poConfig.OrganizationId, poConfig.LenderId)
		With VehicleLoan
			.ReferralSource = Common.SafeString(Request.Form("ReferralSource"))

			.IPAddress = Request.UserHostAddress
			.UserAgent = Request.ServerVariables("HTTP_USER_AGENT")

			.HasDebtCancellation = Request.Form("MemberProtectionPlan") = "Y"
			.LoanPurpose = Common.SafeString(Request.Form("LoanPurpose"))
			.LoanTerm = Common.SafeInteger(Request.Form("LoanTerm"))
			.VehicleValue = Common.SafeDouble(Request.Form("VehicleValue"))
			.DownPayment = Common.SafeDouble(Request.Form("DownPayment"))
			.AmountRequested = Common.SafeDouble(Request.Form("ProposedLoanAmount"))
			.VehicleType = Common.SafeString(Request.Form("VehicleType"))

			'Fix AmountRequest when it is hidden and has zero value
			If .AmountRequested = 0 And .VehicleValue > 0 Then
				.AmountRequested = .VehicleValue
			End If

			.KnowVehicleMake = Common.SafeString(Request.Form("KnowVehicleMake")) = "Y"
			If .KnowVehicleMake = True Then
				.VehicleMake = Common.SafeStripHtmlString(Request.Form("VehicleMake"))
				.VehicleModel = Common.SafeStripHtmlString(Request.Form("VehicleModel"))
				.VehicleYear = Common.SafeInteger(Request.Form("VehicleYear"))
				.VehicleVin = Common.SafeStripHtmlString(Request.Form("VinNumber"))

			End If

			.IsNewVehicle = Common.SafeString(Request.Form("IsNewVehicle")) = "Y"
			If .IsNewVehicle Then
				.VehicleMileage = 0
			Else
				.VehicleMileage = Common.SafeInteger(Request.Form("VehicleMileage"))
			End If

			.HasTradeIn = (Common.SafeString(Request.Form("HasTradeIn")).ToUpper() = "Y")
			If .HasTradeIn Then
				.TradeMake = Common.SafeString(Request.Form("TradeMake"))
				.TradeYear = Common.SafeInteger(Request.Form("TradeYear"))
				.TradeModel = Common.SafeString(Request.Form("TradeModel"))
				.TradeType = Common.SafeString(Request.Form("TradeType"))
				.TradePayOff = Common.SafeDouble(Request.Form("TradePayOff"))
				.TradePayment = Common.SafeDouble(Request.Form("TradePayment"))
				.TradeValue = Common.SafeDouble(Request.Form("TradeValue"))
			End If

			.BranchID = Request.Form("BranchID")
			.LoanOfficerID = Common.SafeString(Request.Form("LoanOfficerID"))   'come from htnl mark page via js

			.isCQNewAPI = Common.getVisibleAttribute(_CurrentWebsiteConfig, "custom_question_new_api") = "Y"
		End With

		Dim docUploadData = GetDocUploadObject(poConfig)
		If docUploadData IsNot Nothing Then
			VehicleLoan.DocBase64Dic = docUploadData.Item1
			VehicleLoan.UploadDocumentInfoList = docUploadData.Item2
		End If

		VehicleLoan.BLCustomQuestionAnswers = GetBLQuestionAnswers()

		Return VehicleLoan
	End Function


	Private Function GetDocUploadObject(ByVal poConfig As CWebsiteConfig) As Tuple(Of Dictionary(Of String, String), List(Of uploadDocumentInfo))
		Dim uploadDocEnableNode = _CurrentWebsiteConfig._webConfigXML.SelectSingleNode("BUSINESS_LOAN/VISIBLE/@document_upload_enable")
		Dim uploadDocMessageNode = _CurrentWebsiteConfig._webConfigXML.SelectSingleNode("BUSINESS_LOAN/UPLOAD_DOC_MESSAGE")
		If uploadDocEnableNode IsNot Nothing AndAlso uploadDocEnableNode.Value.Equals("Y") AndAlso uploadDocMessageNode IsNot Nothing AndAlso Not String.IsNullOrWhiteSpace(uploadDocMessageNode.InnerText) Then

			'===all upload document
			Dim oSelectDocsList As List(Of CSelectDocument)
			Dim oSerializer_doc As New JavaScriptSerializer()
			Dim oDocsList As New Dictionary(Of String, String)
			''by default MaxJsonLenth = 102400, set it =int32.maxvalue = 2147483647 
			oSerializer_doc.MaxJsonLength = Int32.MaxValue
			If Not String.IsNullOrEmpty(Request.Form("Image")) Then
				'TODO
				'' oDocsList = oSerializer.Deserialize(Of Dictionary(Of String, String))(Request.Params("Image"))
				'need to work on serialization
				''oDocsList.Add("test title.png", Request.Params("Image"))
				oSelectDocsList = oSerializer_doc.Deserialize(Of List(Of CSelectDocument))(Request.Form("Image"))
				If oSelectDocsList.Count > 0 Then
					Dim j As Integer
					For j = 0 To oSelectDocsList.Count - 1
						Dim oTitle As String = oSelectDocsList(j).title
						Dim obase64data As String = oSelectDocsList(j).base64data
						oDocsList.Add(oTitle, obase64data)
					Next
					If oDocsList.Count = 0 Then
						log.Info("Can't de-serialize image ")
					End If
				End If
			End If
			''get upload document info if it exist
			Dim oSerializerDocInfo As New JavaScriptSerializer()
			Dim oUploadDocList As New List(Of uploadDocumentInfo)
			If Not String.IsNullOrEmpty(Request.Form("Image")) Then
				oUploadDocList = oSerializerDocInfo.Deserialize(Of List(Of uploadDocumentInfo))(Request.Form("UploadDocInfor"))
			End If
			Return New Tuple(Of Dictionary(Of String, String), List(Of uploadDocumentInfo))(oDocsList, oUploadDocList)
		End If
		Return Nothing
	End Function


	Private Function GetCreditCardLoanObject(ByVal poConfig As CWebsiteConfig) As CBusinessCreditCard
		Dim creditCard As New CBusinessCreditCard(poConfig.OrganizationId, poConfig.LenderId)
		creditCard.HasDebtCancellation = Request.Form("MemberProtectionPlan") = "Y"
		creditCard.CardType = Common.SafeString(Request.Form("CreditCardType"))
		creditCard.CardName = Common.SafeString(Request.Form("CreditCardName"))
		creditCard.LoanPurpose = Common.SafeString(Request.Form("LoanPurpose"))
		''request credit limit
		creditCard.RequestAmount = Common.SafeDouble(Request.Form("RequestAmount"))
		creditCard.eCardNumber = Common.SafeString(Request.Form("eCardNumber"))
		creditCard.isDisagreeSelect = Request.Form("isDisagreeSelect")
		''---user agent, disclosure and IPAddress for internal comment
		If Not String.IsNullOrEmpty(Request.Form("Disclosure")) Then
			creditCard.Disclosure = Request.Form("Disclosure")
		End If

		creditCard.IPAddress = Request.UserHostAddress
		creditCard.UserAgent = Request.ServerVariables("HTTP_USER_AGENT")
		Dim docUploadData = GetDocUploadObject(poConfig)
		If docUploadData IsNot Nothing Then
			creditCard.DocBase64Dic = docUploadData.Item1
			creditCard.UploadDocumentInfoList = docUploadData.Item2
		End If

		creditCard.BranchID = Request.Form("BranchID")

		creditCard.BLCustomQuestionAnswers = GetBLQuestionAnswers()

		Return creditCard
	End Function
	Private Function GetOtherLoanObject(ByVal poConfig As CWebsiteConfig) As CBusinessOtherLoan
		Dim businessOtherLoan As New CBusinessOtherLoan(poConfig.OrganizationId, poConfig.LenderId)
		businessOtherLoan.LoanAmount = Common.SafeDouble(Request.Form("LoanAmount"))
		businessOtherLoan.LoanTerm = Common.SafeInteger(Request.Form("LoanTerm"))
		businessOtherLoan.LoanTypeCategory = Common.SafeString(Request.Form("LoanTypeCategory"))
		businessOtherLoan.IsLOC = Request.Form("IsLOC") = "Y"
		businessOtherLoan.LoanPurpose = Common.SafeString(Request.Form("LoanPurpose"))
		businessOtherLoan.ReferralSource = Common.SafeString(Request.Form("ReferralSource"))
		businessOtherLoan.LoanOfficerID = Common.SafeString(Request.Form("LoanOfficerID"))   'come from htnl mark page via js    
		Dim docUploadData = GetDocUploadObject(poConfig)
		If docUploadData IsNot Nothing Then
			businessOtherLoan.DocBase64Dic = docUploadData.Item1
			businessOtherLoan.UploadDocumentInfoList = docUploadData.Item2
		End If

		businessOtherLoan.IPAddress = Request.UserHostAddress
		businessOtherLoan.UserAgent = Request.ServerVariables("HTTP_USER_AGENT")

		businessOtherLoan.BranchID = Request.Form("BranchID")

		businessOtherLoan.BLCustomQuestionAnswers = GetBLQuestionAnswers()

		Return businessOtherLoan
	End Function
	Private Function GetBLQuestionAnswers() As List(Of CValidatedQuestionAnswers)
		Dim jsSerializer As New JavaScriptSerializer()
		' Only get Application answers, not Applicant
		Dim lstApplicationCustomAnswers As List(Of CApplicantQA) =
			jsSerializer.Deserialize(Of List(Of CApplicantQA))(Request.Form("CustomAnswers")).
			Where(Function(cqa) Not String.IsNullOrEmpty(cqa.CQAnswer) AndAlso cqa.CQRole = QuestionRole.Application).ToList()
		' Collect URL Parameter CQ Answers, if applicable
		Dim bIsUrlParaCustomQuestion = Common.SafeString(Request.Form("IsUrlParaCustomQuestion")) = "Y"
		Dim lstUrlParaCustomAnswers As New List(Of CApplicantQA)
		If bIsUrlParaCustomQuestion Then
			lstUrlParaCustomAnswers = jsSerializer.Deserialize(Of List(Of CApplicantQA))(Common.SafeString(Request.Form("UrlParaCustomQuestionAndAnswers")))
		End If
		' Set validated CQ answers
		Return CCustomQuestionNewAPI.
			getValidatedCustomQuestionAnswers(_CurrentWebsiteConfig, "BL", QuestionRole.Application, "", lstApplicationCustomAnswers, lstUrlParaCustomAnswers.Where(Function(a) a.CQRole = QuestionRole.Application)).ToList()
	End Function
	Private Function SubmitBusinessOtherLoan(ByVal poConfig As CWebsiteConfig, ByRef poResponseRaw As String) As String
		CPBLogger.logInfo("Business Other Loan: Preparing to submit Other Loan")
		Dim BLOther = GetOtherLoanObject(poConfig)
		Dim selectedApplicantList = New JavaScriptSerializer().Deserialize(Of List(Of CSelectedApplicant))(Request.Form("selectedApplicantList"))
		Dim BLDeclarationDic As New Dictionary(Of String, CBLDeclarationInfo)
		Dim BLApplicantInfos As New List(Of CBLApplicantInfo)
		For Each oApp As CSelectedApplicant In selectedApplicantList
			If Not BLDeclarationDic.ContainsKey(oApp.IDPrefix) Then
				BLDeclarationDic.Add(oApp.IDPrefix, collectBLDeclarationInfo(oApp.IDPrefix))
			End If
			BLApplicantInfos.Add(collectBLApplicantInfoData(oApp))
		Next
		BLOther.Disclosure = Request.Form("Disclosure")
		BLOther.isBusinessLoan = True
		BLOther.BLBusinessInfo = collectBLBusinessInfoData()
		BLOther.BLDeclarationDic = BLDeclarationDic
		BLOther.BLApplicants = BLApplicantInfos
		Dim beneficialOwners = New JavaScriptSerializer().Deserialize(Of List(Of CBeneficialOwnerInfo))(SafeString(Request.Form("bl_BeneficialOwner")))
		If beneficialOwners IsNot Nothing AndAlso beneficialOwners.Count > 0 Then
			BLOther.BLBeneficialOwners = beneficialOwners
		End If
		Return Common.SubmitLoan(BLOther, poConfig, False, poResponseRaw, False, "", loanRequestXMLStr)
	End Function
	Private Function SubmitBusinessCreditCard(ByVal poConfig As CWebsiteConfig, ByRef poResponseRaw As String) As String
        CPBLogger.logInfo("Credit Card Loan: Preparing to submit Credit Card")
        Dim BLCreditCard = GetCreditCardLoanObject(poConfig)
		Dim selectedApplicantList = New JavaScriptSerializer().Deserialize(Of List(Of CSelectedApplicant))(Request.Form("selectedApplicantList"))
		Dim BLDeclarationDic As New Dictionary(Of String, CBLDeclarationInfo)
		Dim BLApplicantInfos As New List(Of CBLApplicantInfo)
		For Each oApp As CSelectedApplicant In selectedApplicantList
			If Not BLDeclarationDic.ContainsKey(oApp.IDPrefix) Then
				BLDeclarationDic.Add(oApp.IDPrefix, collectBLDeclarationInfo(oApp.IDPrefix))
			End If
			BLApplicantInfos.Add(collectBLApplicantInfoData(oApp))
		Next
		BLCreditCard.Disclosure = Request.Form("Disclosure")
		BLCreditCard.isBusinessLoan = True
		BLCreditCard.BLBusinessInfo = collectBLBusinessInfoData()
		BLCreditCard.BLDeclarationDic = BLDeclarationDic
		BLCreditCard.BLApplicants = BLApplicantInfos
		Dim beneficialOwners = New JavaScriptSerializer().Deserialize(Of List(Of CBeneficialOwnerInfo))(SafeString(Request.Form("bl_BeneficialOwner")))	'TODO:rename ba to bl
		If beneficialOwners IsNot Nothing AndAlso beneficialOwners.Count > 0 Then
			BLCreditCard.BLBeneficialOwners = beneficialOwners
		End If
		Return Common.SubmitLoan(BLCreditCard, poConfig, False, poResponseRaw, False, "", loanRequestXMLStr)
	End Function

    Private Function SubmitBusinessVehicleLoan(ByVal poConfig As CWebsiteConfig, ByRef poResponseRaw As String) As String
        CPBLogger.logInfo("Vehicle Loan: Preparing to submit vehicle loan")
        Dim BLVehicleLoan = GetVehicleLoanObject(poConfig)
        Dim selectedApplicantList = New JavaScriptSerializer().Deserialize(Of List(Of CSelectedApplicant))(Request.Form("selectedApplicantList"))
        Dim BLDeclarationDic As New Dictionary(Of String, CBLDeclarationInfo)
        Dim BLApplicantInfos As New List(Of CBLApplicantInfo)
		For Each oApp As CSelectedApplicant In selectedApplicantList
			If Not BLDeclarationDic.ContainsKey(oApp.IDPrefix) Then
				BLDeclarationDic.Add(oApp.IDPrefix, collectBLDeclarationInfo(oApp.IDPrefix))
			End If
			BLApplicantInfos.Add(collectBLApplicantInfoData(oApp))
		Next
		BLVehicleLoan.Disclosure = Request.Form("Disclosure")
        BLVehicleLoan.isBusinessLoan = True
        BLVehicleLoan.BLBusinessInfo = collectBLBusinessInfoData()
        BLVehicleLoan.BLDeclarationDic = BLDeclarationDic
        BLVehicleLoan.BLApplicants = BLApplicantInfos
        Dim beneficialOwners = New JavaScriptSerializer().Deserialize(Of List(Of CBeneficialOwnerInfo))(SafeString(Request.Form("bl_BeneficialOwner"))) 'TODO:rename ba to bl
        If beneficialOwners IsNot Nothing AndAlso beneficialOwners.Count > 0 Then
            BLVehicleLoan.BLBeneficialOwners = beneficialOwners
        End If
        Return Common.SubmitLoan(BLVehicleLoan, poConfig, False, poResponseRaw, False, "", loanRequestXMLStr)
    End Function
    Private Function collectBLBusinessInfoData() As CBLBusinessInfo
		Dim result As New CBLBusinessInfo()
		Dim prefix As String = "bl_BusinessInfo"
		Dim type As Type = GetType(CBLBusinessInfo)
		For Each prop In type.GetProperties(BindingFlags.Public Or BindingFlags.Instance)
			If prop Is Nothing Then Continue For
			If Not Request.Form.AllKeys.Any(Function(k) k = prefix & prop.Name) Then Continue For
			Try
				Select Case prop.PropertyType
					Case GetType(String)
						prop.SetValue(result, SafeString(Request.Form(prefix & prop.Name)))
					Case GetType(Integer)
						prop.SetValue(result, SafeInteger(Request.Form(prefix & prop.Name)))
					Case GetType(Guid)
						prop.SetValue(result, SafeGUID(Request.Form(prefix & prop.Name)))
					Case GetType(Boolean)
						prop.SetValue(result, SafeBoolean(Request.Form(prefix & prop.Name)))
					Case GetType(Date)
						prop.SetValue(result, SafeDate(Request.Form(prefix & prop.Name)))
					Case GetType(Double)
						prop.SetValue(result, SafeDouble(Request.Form(prefix & prop.Name)))
				End Select
			Catch ex As Exception
				log.Error(ex)
			End Try
		Next
		Return result
	End Function

	Private Function collectBLDeclarationInfo(prefix As String) As CBLDeclarationInfo
		Dim result As New CBLDeclarationInfo()
		Dim type As Type = GetType(CBLDeclarationInfo)
		For Each prop In type.GetProperties(BindingFlags.Public Or BindingFlags.Instance)
			If prop Is Nothing Then Continue For
			If Not Request.Form.AllKeys.Any(Function(k) k = prefix & prop.Name) Then Continue For
			Try
				Select Case prop.PropertyType
					Case GetType(String)
						prop.SetValue(result, SafeString(Request.Form(prefix & prop.Name)))
					Case GetType(Integer)
						prop.SetValue(result, SafeInteger(Request.Form(prefix & prop.Name)))
					Case GetType(Guid)
						prop.SetValue(result, SafeGUID(Request.Form(prefix & prop.Name)))
					Case GetType(Boolean)
						prop.SetValue(result, SafeBoolean(Request.Form(prefix & prop.Name)))
					Case GetType(Date)
						prop.SetValue(result, SafeDate(Request.Form(prefix & prop.Name)))
					Case GetType(Double)
						prop.SetValue(result, SafeDouble(Request.Form(prefix & prop.Name)))
				End Select
			Catch ex As Exception
				log.Error(ex)
			End Try
		Next
		Return result
	End Function
	Private Function collectBLApplicantInfoData(selectedApplicant As CSelectedApplicant) As CBLApplicantInfo
		Dim result As New CBLApplicantInfo()
		Dim type As Type = GetType(CBLApplicantInfo)
		Dim prefix = selectedApplicant.IDPrefix
		For Each prop In type.GetProperties(BindingFlags.Public Or BindingFlags.Instance)
			If prop Is Nothing Then Continue For
			Try
				Dim propName As String = prop.Name
				If prop.Name = "selectedApplicant" Then
					prop.SetValue(result, selectedApplicant)
				End If
				'for backward compability with old user control submission form data
				Select Case prop.Name
					Case "OtherMonthlyIncomeDescription"
						propName = "OtherMonthlyIncomeDesc"
					Case "EmployedDurationMonth"
						propName = "txtEmployedDuration_month"
					Case "EmployedDurationYear"
						propName = "txtEmployedDuration_year"
					Case "Employer"
						propName = "txtEmployer"
					Case "JobTitle"
						propName = "txtJobTitle"
					Case "BusinessType"
						propName = "txtBusinessType"
					Case "EmploymentStartDate"
						propName = "txtEmploymentStartDate"
					Case "ETS"
						propName = "txtETS"
					Case "ProfessionDurationMonth"
						propName = "txtProfessionDuration_month"
					Case "ProfessionDurationYear"
						propName = "txtProfessionDuration_year"
					Case "SupervisorName"
						propName = "txtSupervisorName"
					Case "BranchOfService"
						propName = "ddlBranchOfService"
					Case "PayGrade"
						propName = "ddlPayGrade"
						'prev
					Case "PrevEmploymentStartDate"
						propName = "prev_txtEmploymentStartDate"
					Case "PrevEmploymentStatus"
						propName = "prev_EmploymentStatus"
					Case "PrevGrossMonthlyIncome"
						propName = "prev_grossMonthlyIncome"
					Case "PrevEmployedDuration_month"
						propName = "prev_txtEmployedDuration_month"
					Case "PrevEmployedDuration_year"
						propName = "prev_txtEmployedDuration_year"
					Case "PrevJobTitle"
						propName = "prev_txtJobTitle"
					Case "PrevEmployer"
						propName = "prev_txtEmployer"
					Case "PrevBusinessType"
						propName = "prev_txtBusinessType"
					Case "PrevEnlistmentDate"
						propName = "prev_EnlistmentDate"
					Case "PrevBranchOfService"
						propName = "prev_ddlBranchOfService"
					Case "PrevPayGrade"
						propName = "prev_ddlPayGrade"
					Case "PrevETS"
						propName = "prev_txtETS"
					Case "IDCountry"
						propName = "PrimaryIDCountry"
					Case "IDCardType"
						propName = "PrimaryIDCardType"
					Case "IDCardNumber"
						propName = "PrimaryIDCardNumber"
					Case "IDDateIssued"
						propName = "PrimaryIDDateIssued"
					Case "IDDateExpire"
						propName = "PrimaryIDDateExpire"
					Case "ApplicantQuestionAnswers"
						Continue For ' See below for setting this item
				End Select
				If Not Request.Form.AllKeys.Any(Function(k) k = prefix & propName) Then Continue For

				'end backward compability
				Select Case prop.PropertyType
					Case GetType(String)
						prop.SetValue(result, SafeString(Request.Form(prefix & propName)))
					Case GetType(Integer)
						prop.SetValue(result, SafeInteger(Request.Form(prefix & propName)))
					Case GetType(Guid)
						prop.SetValue(result, SafeGUID(Request.Form(prefix & propName)))
					Case GetType(Boolean)
						prop.SetValue(result, SafeBoolean(Request.Form(prefix & propName)))
					Case GetType(Date)
						prop.SetValue(result, SafeDate(Request.Form(prefix & propName)))
					Case GetType(Double)
						prop.SetValue(result, SafeDouble(Request.Form(prefix & propName)))
				End Select
			Catch ex As Exception
				log.Error(ex)
			End Try
		Next
		If result.EmploymentStatus = "MI" AndAlso Not String.IsNullOrEmpty(result.EnlistmentDate.Trim()) Then
			result.EmploymentStartDate = result.EnlistmentDate
		End If
		If result.PrevEmploymentStatus = "STUDENT" Or result.PrevEmploymentStatus = "HOMEMAKER" Or result.PrevEmploymentStatus = "RETIRED" Or result.PrevEmploymentStatus = "UNEMPLOYED" Then
			result.PrevJobTitle = result.PrevEmploymentStatus
		End If

		' Applicant custom question answers 
		Dim jsSerializer As New JavaScriptSerializer()
		Dim lstApplicantCustomAnswers As List(Of CApplicantQA) =
			jsSerializer.Deserialize(Of List(Of CApplicantQA))(Request.Form("CustomAnswers")).
			Where(Function(cqa) Not String.IsNullOrEmpty(cqa.CQAnswer) AndAlso cqa.CQRole = QuestionRole.Applicant AndAlso IIf(cqa.CQLocation = CustomQuestionLocation.ApplicantPage, cqa.CQApplicantPrefix = prefix, True)).ToList()
		' Collect URL Parameter CQ Answers, if applicable
		Dim bIsUrlParaCustomQuestion = Common.SafeString(Request.Form("IsUrlParaCustomQuestion")) = "Y"
		Dim lstUrlParaCustomAnswers As New List(Of CApplicantQA)
		If bIsUrlParaCustomQuestion Then
			lstUrlParaCustomAnswers = jsSerializer.Deserialize(Of List(Of CApplicantQA))(Common.SafeString(Request.Form("UrlParaCustomQuestionAndAnswers")))
		End If
		' Set validated CQ answers
		result.ApplicantQuestionAnswers = CCustomQuestionNewAPI.
			getValidatedCustomQuestionAnswers(_CurrentWebsiteConfig, "BL", QuestionRole.Applicant, "", lstApplicantCustomAnswers, lstUrlParaCustomAnswers.Where(Function(urlQA) urlQA.CQRole = QuestionRole.Applicant)).ToList()

		Return result
	End Function
#Region "business custom message"
	Private Function businessCustomSubmitMessage() As String
		Dim customSubmitMessage As String = ""
		Return customSubmitMessage
	End Function
	Private Function businessCustomMessages(ByVal sLoanResponseXML As String, ByVal sLoanRequestXML As String, ByVal sMessageNode As String) As String
		Return Server.HtmlDecode(CCustomMessage.getComboCustomResponseMessages(_CurrentWebsiteConfig, sMessageNode, sLoanResponseXML, sLoanRequestXML, ""))
	End Function
	Private Function businessCustomErrorMessage() As String
		Dim sErrorMessage As String = "We apologize, but unfortunately there was an error when submitting the application. Please try again later. <div id='MLerrorMessage'></div>"
		Dim oErrorMsgNodes As XmlNodeList = _CurrentWebsiteConfig._webConfigXML.SelectNodes("CUSTOM_ERROR")
		' We loop through the XML nodes and we only take the first instance.
		For Each childNode As XmlNode In oErrorMsgNodes
			Try
				If Not String.IsNullOrEmpty(SafeString(childNode.Attributes("submit_error").InnerXml)) Then
					sErrorMessage = childNode.Attributes("submit_error").InnerXml
				End If
				sErrorMessage += "<div id='MLerrorMessage'></div>"
				Exit For
			Catch
				' Something happened, just stick with the defaults
				Return sErrorMessage
			End Try
		Next
		Return sErrorMessage
	End Function
#End Region

#Region "display product utitilities"
	Private Function getQualifyProduct2_0(ByVal xmlResponse As String) As List(Of System.Collections.Generic.Dictionary(Of String, String))
		Dim QualifyProducts As New System.Collections.Generic.List(Of System.Collections.Generic.Dictionary(Of String, String))
		Dim card_rate As Double = 0.0
		Dim temp_rate As Double = 0.0
		Dim index As Integer = 0
		If Not Common.checkIsQualified(xmlResponse) Then Return QualifyProducts

		Dim doc As New XmlDocument()
		doc.LoadXml(xmlResponse)
		Dim oDecision As XmlElement = doc.SelectSingleNode("/OUTPUT/RESPONSE/DECISION")
		If oDecision Is Nothing Then Return QualifyProducts
		Dim strQualifyCard As String
		'oDecision may be qualify but has no product so null childnode
		Try
			strQualifyCard = oDecision.ChildNodes(0).InnerText
		Catch ex As Exception
			Return QualifyProducts
		End Try

		Dim cdoc As New XmlDocument()
		cdoc.LoadXml(strQualifyCard)

		'<PRODUCTS><VL_PRODUCT loan_id="6b1a9b270e3e45b7b459704ab20c7d65" date_offered="2016-12-19T15:52:38.2922388-08:00" is_prequalified="N" underwriting_service_results_index="0" amount_approved="15000" max_amount_approved="58500" grade="Auto A+" reference_id="" tier="1" set_loan_system="C" is_single_payment="false" solve_for="PAYMENT" estimated_monthly_payment="438.10" term="36"><RATE rate="1.990" original_rate="1.990" rate_code="36 mo NEW" margin="0.000" index="0.000" rate_floor="0.000" rate_ceiling="0.000" rate_type="F" rate_adjustment_option="INTEREST_RATE" index_type="" dealer_reserve_rate="2.000" initial_rate="" change_after_days="" /><STIPULATIONS><STIPULATION stip_text="PROVIDE PROOF OF INCOME IF SELF-EMPLOYED: 2 YEARS PAY STUBS OR 2 YEARS TAX RETURNS" is_required="N" /><STIPULATION stip_text="PROVIDE PROOF OF INSURANCE" is_required="Y" /><STIPULATION stip_text="ECCU MEMBERSHIP IS REQUIRED" is_required="Y" /></STIPULATIONS></VL_PRODUCT></PRODUCTS>]

		Dim oQualifyProducts As XmlNodeList = cdoc.SelectNodes("/PRODUCTS/BL_PRODUCT")
		For Each oItem As XmlElement In oQualifyProducts
			Dim oQualifyProduct As XmlElement = oItem
			Dim oProductRate As XmlElement = oQualifyProduct.SelectSingleNode("RATE")
			Dim dicQualifyProduct As New System.Collections.Generic.Dictionary(Of String, String)
			dicQualifyProduct.Add("Status", oDecision.GetAttribute("status")) '"PREQUALIFIED", QUALIFIED
			dicQualifyProduct.Add("Index", oQualifyProduct.GetAttribute("underwriting_service_results_index").ToString())
			dicQualifyProduct.Add("AmountApproved", oQualifyProduct.GetAttribute("amount_approved").Replace("$", ""))
			dicQualifyProduct.Add("MaxAmountApproved", oQualifyProduct.GetAttribute("max_amount_approved").Replace("$", ""))
			dicQualifyProduct.Add("Rate", oProductRate.GetAttribute("rate").ToString())
			dicQualifyProduct.Add("RateType", oProductRate.GetAttribute("rate_type").ToString())
			dicQualifyProduct.Add("Term", oQualifyProduct.GetAttribute("term").ToString())
			dicQualifyProduct.Add("EstimatedMonthlyPayment", oQualifyProduct.GetAttribute("estimated_monthly_payment").ToString())
			dicQualifyProduct.Add("Grade", oQualifyProduct.GetAttribute("grade").ToString())
			dicQualifyProduct.Add("RateCode", oProductRate.GetAttribute("rate_code").ToString())
			QualifyProducts.Add(dicQualifyProduct)
		Next

		Return sortQualifyProducts(QualifyProducts)
	End Function
	''sort qualify products by rate (from lowest to highest) 
	Function sortQualifyProducts(ByVal qualifiedProducts As List(Of Dictionary(Of String, String))) As List(Of Dictionary(Of String, String))
		If qualifiedProducts.Count < 2 Then
			Return qualifiedProducts
		End If
		Dim sw = True
		While sw = True
			sw = False
			For i = 0 To qualifiedProducts.Count - 2
				Dim rate1 = Convert.ToDouble(qualifiedProducts(i).Item("Rate"))
				Dim rate2 = Convert.ToDouble(qualifiedProducts(i + 1).Item("Rate"))
				If rate1 > rate2 Then
					Dim temp = qualifiedProducts(i + 1)
					qualifiedProducts(i + 1) = qualifiedProducts(i)
					qualifiedProducts(i) = temp
					sw = True
				End If
			Next
		End While
		Return qualifiedProducts
	End Function
	Private Function displayProduct2_0() As String
		Dim IsManualProdcutSelect As Boolean = Common.SafeString(Common.getNodeAttributes(_CurrentWebsiteConfig, "BEHAVIOR", "manual_select_loan_product")) = "Y"

		Dim strHtml As String = ""
		If Not QualifiedProducts.Count > 0 Then Return DecisionMessage

		strHtml += "<div align='left'>"
		Dim sFirstName As String = Request.Form("FirstName")
		Dim sTitle As String = ""
		If IsManualProdcutSelect Then
			sTitle = "Congratulations <b>" + sFirstName + "</b>!"
		End If
		If QualifiedProducts(0)("Status") = "PREQUALIFIED" Then
			strHtml += sTitle + "  You have been pre-approved for the following: <br/>"
		Else
			strHtml += sTitle + "  You have been qualified for the following: <br/>"
		End If
		Dim hasApprovedAmount As Boolean = False
		For Each dicQualifyProduct As Dictionary(Of String, String) In QualifiedProducts
			'strHtml += "<br/><b>" + dicQualifyProduct("Grade") + ": Up to " & FormatCurrency(dicQualifyProduct("MaxAmountApproved"), 0) & "</b><br/>"
			'strQualifyProduct(1) => interest rate
			'strQualifyProduct(3) => index
			'strHtml += "<b>Loan Class: </b>" + dicQualifyProduct("RateCode") + "<br/>"
			If Common.SafeDouble(dicQualifyProduct("AmountApproved")) = 0 Then
				Continue For
			Else
				hasApprovedAmount = True
			End If
			strHtml += "<br/><b>Approved Amount: </b>" + FormatCurrency(Common.SafeDouble(dicQualifyProduct("AmountApproved")), 2) + "<br/>"
			Dim sRateType As String = ""
			If dicQualifyProduct("RateType") = "F" Then sRateType = " Fixed"
			strHtml += "<b>Rate: </b>" + dicQualifyProduct("Rate") + "%" + sRateType + "<br/>"
			If Common.SafeString(dicQualifyProduct("Term")) <> "" Then
				strHtml += "<b>Term: </b>" + dicQualifyProduct("Term") + " months" + "<br/>"
			End If
			''remove estimated Monthly Payment
			''If Common.SafeDouble(dicQualifyProduct("EstimatedMonthlyPayment")) <> 0 Then
			''    strHtml += "<b>Estimated Monthly Payment: </b>" + FormatCurrency(Common.SafeDouble(dicQualifyProduct("EstimatedMonthlyPayment")), 2) + "<br/> <br/>"
			''End If
			'strHtml += "<a href='javascript:void(0);' class='header_theme2' style='font-weight:bold' onclick='SelectProduct(" + dicQualifyProduct("Index") + ");'  >Accept Offer</a> <br/><br/>"
			If IsManualProdcutSelect Then
				strHtml += "<div class ='div-continue' ><a href='javascript:void(0);' type='button'  class='div-continue-button ui-btn ui-shadow ui-corner-all' onclick='SelectProduct(" + dicQualifyProduct("Index") + ");'  >Accept Offer</a></div> <br/><br/>"
			End If
		Next
		strHtml += "</div>"

		If Not IsManualProdcutSelect Then
			strHtml = DecisionMessage + "<br/>" + strHtml
		End If

		Dim showQualifiedProduct As String = Common.showQualifiedProduct(_CurrentWebsiteConfig)
		If showQualifiedProduct = "N" Or Not hasApprovedAmount Then
			strHtml = DecisionMessage
		End If
		Return strHtml
	End Function
#End Region


End Class
