﻿Imports DownloadedSettings
Imports System.IdentityModel
Imports Newtonsoft.Json
Imports LPQMobile.BO
Imports LPQMobile.Utils
Imports System.IO
Imports System.Xml
Imports log4net
Imports System.Web.Script.Serialization

Partial Class bl_BusinessLoan
    Inherits CBasePage

    Protected _Log As ILog = LogManager.GetLogger("Business Loan")
    Protected _address_key As String
    Protected _hasScanDocumentKey As String
    Protected _previousPage As String = ""
    Protected Property _idPrefix As String = ""
    Protected _businessLoanType As String = ""
    Protected BAPrefixList As New List(Of String)
    Protected selectedApplicantDic As New Dictionary(Of String, String)
    Protected selectedApplicantList As New List(Of CSelectedApplicant)
    Protected _CreditPullMessage As String = ""
    Protected _visibleCustomQuestion As String = ""
    Protected _SubmitCancelMessage As String = ""
	Protected _EnableBLDeclaration As Boolean = True
    Protected _UploadDocEnable As Boolean = False
	Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
		ucPageHeader._currentConfig = _CurrentWebsiteConfig
		If Not IsPostBack Then
			Dim hasBlankDefaultCitizenship As Boolean = Common.hasBlankDefaultCitizenShip(_CurrentWebsiteConfig)
			Dim citizenshipStatusDropdown As String = ""
			'change the default US Citizen 
			If _CurrentWebsiteConfig.GetEnumItems("CITIZENSHIP_TYPES").Count > 0 Then  'available in  LPQMobileWebsiteConfig
				Dim citizenships = _CurrentWebsiteConfig.GetEnumItems("CITIZENSHIP_TYPES")
				citizenshipStatusDropdown = Common.DefaultCitizenShipFromConfig(citizenships, _DefaultCitizenship, _CurrentLenderRef, hasBlankDefaultCitizenship)
			Else
				citizenshipStatusDropdown = Common.DefaultCitizenShip(_DefaultCitizenship, _CurrentLenderRef, hasBlankDefaultCitizenship) ''from hard code
			End If
			Dim stateDropdown As String = Common.RenderStateDropdownlistWithEmpty(CEnum.STATES, "", "--Please Select--")
			Dim maritalStatusDropdown As String = Common.RenderDropdownlistWithEmpty(CEnum.MARITAL_STATUS, "", "--Please Select--")
			Dim employeeOfLenderDropdown As String = ""
			Dim employeeOfLenderType = _CurrentWebsiteConfig.GetEnumItems("EMPLOYEE_OF_LENDER_TYPE")
			If employeeOfLenderType IsNot Nothing AndAlso employeeOfLenderType.Count > 0 Then  'available in  LPQMobileWebsiteConfig
				''get employee of lender type from config
				employeeOfLenderDropdown = Common.RenderDropdownlist(employeeOfLenderType, "")
			End If
			Dim occupyingLocationDropdown As String = Common.RenderDropdownlistWithEmpty(CEnum.OCCUPY_LOCATIONS, " ", "--Please Select--")
			'' Dim collectDescIfOccupancyStatusIsOther As String = Common.getNodeAttributes(_CurrentWebsiteConfig, "XA_LOAN/BEHAVIOR", "collect_description_if_occupancy_status_is_other")
			Dim liveYearsDropdown As String = Common.GetYears()
			Dim liveMonthsDropdown As String = Common.GetMonths()

			Dim relationToPrimaryApplicantDropdown As String = ""
			Dim relationshipToPrimaryList As Dictionary(Of String, String) = _CurrentWebsiteConfig.GetEnumItems("RELATIONSHIP_PRIMARY")
			If relationshipToPrimaryList IsNot Nothing AndAlso relationshipToPrimaryList.Any() Then
				relationToPrimaryApplicantDropdown = Common.RenderDropdownlistWithEmpty(relationshipToPrimaryList, "", "--Please Select--")
			End If

			Dim preferContactMethods As Dictionary(Of String, String) = _CurrentWebsiteConfig.GetEnumItems("PREFERRED_CONTACT_METHODS")
			preferContactMethods = preferContactMethods.ToDictionary(Function(kv) kv.Key.NullSafeToUpper_, Function(kv) kv.Value)
			Dim preferContactMethodDropdown As String = ""
			If preferContactMethods IsNot Nothing AndAlso preferContactMethods.Any() Then
				preferContactMethodDropdown = Common.RenderDropdownlist(preferContactMethods, "")
			End If
			'' Dim xaCollectDescriptionIfEmploymentStatusIsOther = Common.getNodeAttributes(_CurrentWebsiteConfig, "XA_LOAN/BEHAVIOR", "collect_description_if_employment_status_is_other")
			Dim employmentStatusDropdown As String = Common.RenderDropdownlistWithEmpty(CEnum.EMPLOYMENT_STATUS, "", "--Please Select--")
			Dim IDCardTypes As Dictionary(Of String, String) = _CurrentWebsiteConfig.GetEnumItems("ID_CARD_TYPES")
			If IDCardTypes Is Nothing OrElse IDCardTypes.Count = 0 Then
				IDCardTypes = CFOMQuestion.DownLoadIDCardTypes(_CurrentWebsiteConfig)
			End If
			''submit cancel message
			_SubmitCancelMessage = Common.getSubmitCancelMessage(_CurrentWebsiteConfig)
			If Not String.IsNullOrEmpty(_SubmitCancelMessage) Then
				_SubmitCancelMessage = Server.HtmlDecode(_SubmitCancelMessage)  ''decode code message 
			End If
			_businessLoanType = Common.SafeString(Request.QueryString("loantype"))
			If _businessLoanType <> "" Then
				Dim oBusinessLoan = getBusinessLoan(_CurrentWebsiteConfig)
				Dim baBusinessInfoPage As bl_Inc_BusinessLoanInfo = CType(LoadControl("~/bl/Inc/BusinessLoanInfo.ascx"), bl_Inc_BusinessLoanInfo)
				Dim businessTypeValues = CEnum.BUSINESSTYPEVALUE.OrderBy(Function(x) x).ToArray
				Dim businessTypeDic As New Dictionary(Of String, String)
				For Each bTypeValue In businessTypeValues
					If Not businessTypeDic.ContainsKey(bTypeValue) Then
						If _CurrentLenderRef.NullSafeToUpper_.Contains("FIMERB") Then 'First Merchants Bank customization until we have customlist
							Dim sInclusiveType As String = "CORP,LLC,SP,PARTNER,NONPROFIT"
							Dim arrayInclusiveType() As String = sInclusiveType.Split(","c)
							If Not arrayInclusiveType.Contains(bTypeValue) Then Continue For
						End If
						businessTypeDic.Add(bTypeValue, CEnum.MapBusinessTypeName(bTypeValue))
					End If
				Next
				baBusinessInfoPage.BusinessTypeDropDown = Common.RenderDropdownlistWithEmpty(businessTypeDic, "", "--Please select--")
				baBusinessInfoPage.LogoUrl = _LogoUrl
				baBusinessInfoPage.IDPrefix = "bl_BusinessInfo"
				baBusinessInfoPage.MappedShowFieldLoanType = mappedShowFieldLoanType
				baBusinessInfoPage.StateDropdown = stateDropdown
				baBusinessInfoPage.EnabledAddressVerification = _CurrentWebsiteConfig.EnabledAddressVerification
				baBusinessInfoPage.EnableMailingAddress = EnableMailingAddressSection("bl_BusinessInfo")
				baBusinessInfoPage.EnableOccupancyStatus = True
				baBusinessInfoPage.OccupyingLocationDropdown = occupyingLocationDropdown
				'' baBusinessInfoPage.CollectDescriptionIfOccupancyStatusIsOther = (collectDescIfOccupancyStatusIsOther.ToUpper() = "Y")
				baBusinessInfoPage.LiveYearsDropdown = liveYearsDropdown
				baBusinessInfoPage.LiveMonthsDropdown = liveMonthsDropdown
				baBusinessInfoPage.BusinessIndustryCodesDropdown = Common.RenderDropdownlistWithEmpty(GetBusinessIndustryCodes(), " ", "--Please Select--")
				baBusinessInfoPage.InstitutionType = _CurrentWebsiteConfig.InstitutionType
				'' baBusinessInfoPage.EnableDoingBusinessAs = SelectedBusinessAccountType.ShowDoingBusinessAs
				_previous_address_threshold = _CurrentWebsiteConfig.getPreviousThreshold("BUSINESS_LOAN", "previous_address_threshold")
				_previous_employment_threshold = _CurrentWebsiteConfig.getPreviousThreshold("BUSINESS_LOAN", "previous_employment_threshold")
				If (_businessLoanType = "card") Then
					baBusinessInfoPage.PreviousPage = "creditCardInfo"
					initBusinessCreditCard(oBusinessLoan)
					_EnableBLDeclaration = oBusinessLoan.EnableCCDeclaration
					''credit card here
				ElseIf _businessLoanType = "vehicle" Then
					baBusinessInfoPage.PreviousPage = "vehicleInfo"
					initBusinessVehicleLoan(oBusinessLoan)
					_EnableBLDeclaration = oBusinessLoan.EnableVLDeclaration
				ElseIf _businessLoanType = "other" Then
					baBusinessInfoPage.PreviousPage = "otherLoanInfo"
					initBusinessOtherLoan(oBusinessLoan)
					_EnableBLDeclaration = oBusinessLoan.EnableOTDeclaration
				End If

				plhBusinessLoanInfo.Controls.Add(baBusinessInfoPage)
				Dim selectedIDPrefixes As New List(Of String)
				'Dim appRoles = Common.SafeString(Request.QueryString("roles"))


				Dim roleInfoToken As String = Common.SafeString(Request.QueryString("roleinfotoken"))
				Dim roleInfoStr As String = ""
				If Not String.IsNullOrWhiteSpace(roleInfoToken) AndAlso Session(roleInfoToken) IsNot Nothing Then
					roleInfoStr = Session(roleInfoToken).ToString()
				Else
					roleInfoStr = HttpUtility.UrlDecode(Common.SafeString(Request.QueryString("roleinfo")))
				End If
				Dim appRoleList = New JavaScriptSerializer().Deserialize(Of List(Of CXAQueryRoleInfo))(roleInfoStr)
				If appRoleList IsNot Nothing AndAlso appRoleList.Count > 0 Then
					'Dim appRoleList = appRoles.Split(","c).ToList()
					Dim baBeneficialOwnersCtrl As xa_Inc_baBeneficialOwners = CType(LoadControl("~/xa/Inc/baBeneficialOwners.ascx"), xa_Inc_baBeneficialOwners)
					Dim businessLoanApplicantRolesDic = GetBusinessLoanApplicantRoles()

					Dim uploadDocEnable = Common.getBLUploadDocumentEnable(_CurrentWebsiteConfig)
					Dim uploadDocMessage = Common.getBLUploadDocumentMessage(_CurrentWebsiteConfig)
					Dim uploadDocRequired = Common.SafeString(Common.getNodeAttributes(_CurrentWebsiteConfig, "BUSINESS_LOAN/BEHAVIOR", "doc_upload_required")).Equals("Y", StringComparison.OrdinalIgnoreCase)
					Dim requireDocTitle As Boolean = True
					Dim docTitleOptions As New List(Of CDocumentTitleInfo)
					If uploadDocEnable = True AndAlso Not String.IsNullOrWhiteSpace(uploadDocMessage) Then
						_UploadDocEnable = True
						requireDocTitle = Not Common.getNodeAttributes(_CurrentWebsiteConfig, "BUSINESS_LOAN/BEHAVIOR", "required_doc_title").Equals("N", StringComparison.OrdinalIgnoreCase)
						Dim clrDocTitle = New List(Of CDocumentTitleInfo)

						' Loan doc titles from Custom List Rules
						If CustomListRuleList IsNot Nothing AndAlso CustomListRuleList.Any(Function(c) c.Code = "DOCUMENT_TITLES") Then
							Dim loanClrDocTitle = EvaluateCustomList(Of CDocumentTitleInfo)("DOCUMENT_TITLES")

							If loanClrDocTitle IsNot Nothing AndAlso loanClrDocTitle.Count > 0 Then
								loanClrDocTitle.ForEach(Sub(docTitle) docTitle.LoanType = "BL")
								clrDocTitle.AddRange(loanClrDocTitle)
							End If
						End If

						' Do not load XA doc titles, since BL does not do Combo for XA

						If clrDocTitle.Count = 0 Then
							' Fallback to ENUMS doc titles
							docTitleOptions = Common.GetDocumentTitleInfoList(_CurrentWebsiteConfig._webConfigXML.SelectSingleNode("ENUMS/BL_DOC_TITLE"))
						Else
							' Fill out options
							docTitleOptions = New List(Of CDocumentTitleInfo) From {New CDocumentTitleInfo() With {.Group = "", .Code = "", .Title = "--Please select title--"}}
							docTitleOptions.AddRange(clrDocTitle.OrderBy(Function(t) t.Title))
						End If
					End If

					Dim occupancyStatusesForRequiredHousingPayment = _CurrentWebsiteConfig.GetOccupancyStatusesForRequiredHousingPayment("BUSINESS_LOAN")

					Dim index As Integer = 0
					For Each role In appRoleList
						Dim baAppInfoCtrl As xa_Inc_baApplicantInfo = CType(LoadControl("~/xa/Inc/baApplicantInfo.ascx"), xa_Inc_baApplicantInfo)
						Dim idPrefix As String = "ba_" & businessLoanApplicantRolesDic(role.roletype).Replace(" ", "").Replace("-", "") & IIf(role.isjoint, "_joint", "").ToString() & "_" & index
						'Dim sAppData = Common.SafeEncodeString(Request.QueryString(idPrefix)).Split(","c)
						Dim selectedApplicant As New CSelectedApplicant
						'If sAppData.Count = 4 Then
						selectedApplicant.firstName = Common.SafeEncodeString(role.fname)
						selectedApplicant.lastName = Common.SafeEncodeString(role.lname)
						selectedApplicant.appTypeValue = Common.SafeEncodeString(role.roletype)
						selectedApplicant.appTypeText = businessLoanApplicantRolesDic(role.roletype) & IIf(role.isjoint, " (joint)", "").ToString()
						'End If
						selectedApplicant.IDPrefix = idPrefix
						selectedApplicantList.Add(selectedApplicant)
						selectedApplicantDic.Add(idPrefix, New JavaScriptSerializer().Serialize(selectedApplicant))
						selectedIDPrefixes.Add(idPrefix)
						BAPrefixList.Add(idPrefix)
						baAppInfoCtrl.IsPrimaryApplicant = (index = 0)
						baAppInfoCtrl.IDPrefix = idPrefix
						baAppInfoCtrl.MappedShowFieldLoanType = mappedShowFieldLoanType
						baAppInfoCtrl.selectedApplicant = selectedApplicant
						If baAppInfoCtrl.IsPrimaryApplicant Then
							baBusinessInfoPage.NextPage = "page" & idPrefix
							baAppInfoCtrl.PreviousPage = "page" & baBusinessInfoPage.IDPrefix
							'primaryApplicantPage = "page" & idPrefix
						Else
							baAppInfoCtrl.PreviousPage = "pageba_" & businessLoanApplicantRolesDic(appRoleList(index - 1).roletype).Replace(" ", "").Replace("-", "") & IIf(appRoleList(index - 1).isjoint, "_joint", "").ToString() & "_" & (index - 1)
						End If

						If index = appRoleList.Count - 1 Then
							baAppInfoCtrl.NextPage = "pageba_BeneficialOwner"
							baBeneficialOwnersCtrl.PreviousPage = "page" & idPrefix
						Else
							baAppInfoCtrl.NextPage = "pageba_" & businessLoanApplicantRolesDic(appRoleList(index + 1).roletype).Replace(" ", "").Replace("-", "") & IIf(appRoleList(index + 1).isjoint, "_joint", "").ToString() & "_" & (index + 1)
						End If
						baAppInfoCtrl.RoleType = businessLoanApplicantRolesDic(selectedApplicant.appTypeValue)
						baAppInfoCtrl.BusinessType = "BL"
						baAppInfoCtrl.IsJoint = role.isjoint
						''If baAppInfoCtrl.IsJoint Then
						''    baAppInfoCtrl.RoleType = role.Substring(0, role.Length - 6) 'remove _joint at the end of rolename
						''End If
						baAppInfoCtrl.CurrentConfig = _CurrentWebsiteConfig
						If _CurrentWebsiteConfig.DLBarcodeScan = "Y" Then
							baAppInfoCtrl.LaserScandocAvailable = True
						Else
							''hide the scan docs page if SSO via MobileApp
							Dim scanDocsKey As String = ""
							If Common.SafeString(Request.Params("FName")) = "" Then
								scanDocsKey = _CurrentWebsiteConfig.ScanDocumentKey
							End If
							If Not String.IsNullOrEmpty(scanDocsKey) Then
								baAppInfoCtrl.LegacyScandocAvailable = True
								baAppInfoCtrl.ScanDocumentKey = _CurrentWebsiteConfig.ScanDocumentKey
							End If
						End If
						baAppInfoCtrl.LaserScandocAvailable = _CurrentWebsiteConfig.DLBarcodeScan = "Y"
						plhApplicantPagesWrapper.Controls.Add(baAppInfoCtrl)
						baAppInfoCtrl.EnableAddress = True
						baAppInfoCtrl.EnableMailingAddress = EnableMailingAddressSection(idPrefix)
						baAppInfoCtrl.EnableContactInfo = True
						baAppInfoCtrl.EnableContactRelative = False
						''currently there is no relationshipToPrimary dropdown for for business loan--> reset enableRelationshipToPrimary to false
						baAppInfoCtrl.EnableRelationshipToPrimary = False

						baAppInfoCtrl.EnableEmployment = True
						baAppInfoCtrl.EnableIDCard = CheckShowField("divApplicantID", idPrefix, True) OrElse (IsInMode("777") AndAlso IsInFeature("visibility"))
						baAppInfoCtrl.isBusinessLoan = True
						baAppInfoCtrl.EnableReferenceInfo = CheckShowField("divReferenceInformation", idPrefix, True) OrElse (IsInMode("777") AndAlso IsInFeature("visibility"))
						baAppInfoCtrl.EnableBLDeclaration = _EnableBLDeclaration
						baAppInfoCtrl.EnableMemberNumber = CheckShowField("divMemberNumber", idPrefix, False) Or (IsInMode("777") AndAlso IsInFeature("visibility"))
						baAppInfoCtrl.InstitutionType = _CurrentWebsiteConfig.InstitutionType
						'TODO: default state of member number should be FALSE, while waiting for APM visibility of Business Account be ready, temporary set it to true for testing            
						baAppInfoCtrl.EnableGender = CheckShowField("divGender", idPrefix, True) Or (IsInMode("777") AndAlso IsInFeature("visibility"))
						baAppInfoCtrl.EnableMotherMaidenName = CheckShowField("divMotherMaidenName", idPrefix, True) Or (IsInMode("777") AndAlso IsInFeature("visibility"))
						baAppInfoCtrl.EnableCitizenshipStatus = CheckShowField("divCitizenshipStatus", idPrefix, True) Or (IsInMode("777") AndAlso IsInFeature("visibility"))
						baAppInfoCtrl.EnableMaritalStatus = CheckShowField("divMaritalStatus", idPrefix, False) Or (IsInMode("777") AndAlso IsInFeature("visibility"))
						baAppInfoCtrl.MaritalStatusDropdown = maritalStatusDropdown
						baAppInfoCtrl.CitizenshipStatusDropdown = citizenshipStatusDropdown
						baAppInfoCtrl.StateDropdown = stateDropdown

						baAppInfoCtrl.EmployeeOfLenderDropdown = employeeOfLenderDropdown
						baAppInfoCtrl.LenderName = _strLenderName
						'
						baAppInfoCtrl.EnabledAddressVerification = _CurrentWebsiteConfig.EnabledAddressVerification
						baAppInfoCtrl.RelationToPrimaryApplicantDropdown = relationToPrimaryApplicantDropdown
						'' Dim memberNumberRequired As Boolean = Common.getNodeAttributes(_CurrentWebsiteConfig, "XA_LOAN/BEHAVIOR", "require_mem_number_secondary").ToUpper().Equals("Y")
						''  baAppInfoCtrl.MemberNumberRequired = memberNumberRequired
						'BCU customization
						If _CurrentLenderRef.ToUpper.StartsWith("BCU") Then baAppInfoCtrl.FieldMaxLength = 40
						'TODO: default state of occupancy status should be FALSE, while waiting for APM visibility of Business Account be ready, temporary set it to true for testing                      
						baAppInfoCtrl.EnableOccupancyStatus = CheckShowField("divOccupancyStatusSection", idPrefix, True) Or (IsInMode("777") AndAlso IsInFeature("visibility"))
						baAppInfoCtrl.OccupyingLocationDropdown = occupyingLocationDropdown
						baAppInfoCtrl.CollectDescriptionIfOccupancyStatusIsOther = False ''(collectDescIfOccupancyStatusIsOther.ToUpper() = "Y")
						baAppInfoCtrl.OccupancyStatusesForRequiredHousingPayment = occupancyStatusesForRequiredHousingPayment
						baAppInfoCtrl.LiveYearsDropdown = liveYearsDropdown
						baAppInfoCtrl.LiveMonthsDropdown = liveMonthsDropdown
						baAppInfoCtrl.PreferContactMethodDropdown = preferContactMethodDropdown
						baAppInfoCtrl.EnableGrossMonthlyIncome = CheckShowField("divGrossMonthlyIncome", idPrefix, True) Or (IsInMode("777") AndAlso IsInFeature("visibility"))
						baAppInfoCtrl.CollectDescriptionIfEmploymentStatusIsOther = "" '' xaCollectDescriptionIfEmploymentStatusIsOther
						baAppInfoCtrl.EmploymentStatusDropdown = employmentStatusDropdown
						If IDCardTypes IsNot Nothing AndAlso IDCardTypes.Count > 0 Then
							baAppInfoCtrl.IDCardTypeList = Common.RenderDropdownlist(IDCardTypes, " ", "DRIVERS_LICENSE")
							baAppInfoCtrl.IsExistIDCardType = True
						Else
							baAppInfoCtrl.IsExistIDCardType = False
						End If
						baAppInfoCtrl.LogoUrl = _LogoUrl


						baAppInfoCtrl.EnableDocUpload = _UploadDocEnable
						If baAppInfoCtrl.EnableDocUpload Then
							baAppInfoCtrl.DocUploadTitle = uploadDocMessage
							baAppInfoCtrl.RequireDocTitle = requireDocTitle
							baAppInfoCtrl.DocTitleOptions = docTitleOptions
							baAppInfoCtrl.DocUploadRequired = uploadDocRequired
						End If


						index += 1
					Next
					baBeneficialOwnersCtrl.SelectedIDPrefixes = selectedIDPrefixes
					baBeneficialOwnersCtrl.StateDropdown = stateDropdown
					baBeneficialOwnersCtrl.EnabledAddressVerification = _CurrentWebsiteConfig.EnabledAddressVerification
					baBeneficialOwnersCtrl.CitizenshipStatusDropdown = citizenshipStatusDropdown
					baBeneficialOwnersCtrl.IDPrefix = "ba_BeneficialOwner"
					baBeneficialOwnersCtrl.IsBusinessLoan = True
					baBeneficialOwnersCtrl.NextPage = "pagesubmit"
					baBeneficialOwnersCtrl.EnableBeneficiarySSN = CheckShowField("divBeneficiarySSN1", baBeneficialOwnersCtrl.IDPrefix, True) Or (IsInMode("777") AndAlso IsInFeature("visibility"))
					baBeneficialOwnersCtrl.MappedShowFieldLoanType = mappedShowFieldLoanType
					plhBusinessBeneficialOwnersPage.Controls.Add(baBeneficialOwnersCtrl)
				End If
			Else
				Throw New BadRequestException("Invalid request")
			End If ''end sloanType

			'' app_type ='FOREIGN'
			Dim appType As String = _CurrentWebsiteConfig.AppType
			hdForeignAppType.Value = "" '' initial hidden value foreign
			''Make sure appType is not nothing
			If Not String.IsNullOrEmpty(appType) Then
				If appType.ToUpper() = "FOREIGN" Then
					hdForeignAppType.Value = "Y"
				End If
			End If

			xaApplicationQuestionReviewPage.Header = HeaderUtils.RenderPageTitle(0, "Please answer question(s) below", True)

			Dim bIsCameraAllow As Boolean = True
			If PlatformSource = "DI" And _textAndroidTel = "tel" Then   'don't allow camera when running inside DI android app
				bIsCameraAllow = False
			End If
			ucPageHeader._headerDataTheme = _HeaderDataTheme
			ucPageHeader._footerDataTheme = _FooterDataTheme
			ucPageHeader._contentDataTheme = _ContentDataTheme
			ucPageHeader._headerDataTheme2 = _HeaderDataTheme2
			ucPageHeader._backgroundDataTheme = _BackgroundTheme
			ucPageHeader._buttonDataTheme = _ButtonTheme
			ucPageHeader._buttonFontColor = _ButtonFontTheme

			plhApplicationDeclined.Visible = False
			If CustomListRuleList IsNot Nothing AndAlso CustomListRuleList.Any(Function(p) p.Code = "APPLICANT_BLOCK_RULE") Then
				plhApplicationDeclined.Visible = True
				Dim applicationBlockRulesPage As Inc_MainApp_ApplicationBlockRules = CType(LoadControl("~/Inc/MainApp/ApplicationBlockRules.ascx"), Inc_MainApp_ApplicationBlockRules)
				applicationBlockRulesPage.CustomListItem = CustomListRuleList.First(Function(p) p.Code = "APPLICANT_BLOCK_RULE")
				plhApplicationDeclined.Controls.Add(applicationBlockRulesPage)
			End If
			InitCustomQuestion()
			'' address_key
			If Not String.IsNullOrEmpty(_CurrentWebsiteConfig.AddressKey) Then
				_address_key = _CurrentWebsiteConfig.AddressKey
			End If
		End If
	End Sub

	Protected Sub initBusinessOtherLoan(ByVal oBusinessLoan As CBusinessLoan)
		log.Info("Business Other Loan: Page loading")
		ucPageHeader._currentConfig = _CurrentWebsiteConfig
		_previousPage = "otherLoanInfo"
		_idPrefix = "ot"
		Dim otInfo As bl_Inc_OtherLoanInfo = CType(LoadControl("~/bl/Inc/OtherLoanInfo.ascx"), bl_Inc_OtherLoanInfo)
		''implement EnableBranchSection later(now display branch section)
		otInfo.EnableBranchSection = EnableBranchSection
		otInfo.MappedShowFieldLoanType = mappedShowFieldLoanType
		otInfo._branchOptionsStr = GetBranches("PL")
		otInfo._textAndroidTel = _textAndroidTel
		otInfo._LoanPurposeList = oBusinessLoan.OtherPurposes
		otInfo._CurrentLenderRef = _CurrentLenderRef
		otInfo._LogoUrl = _LogoUrl
		plhLoanInfoPage.Controls.Add(otInfo)
		''disclosure
		InitBusinessDisclosure(oBusinessLoan.OtherDisclosures)
	End Sub
	Protected Sub initBusinessCreditCard(ByVal oBusinessLoan As CBusinessLoan)
		log.Info("Business Credit Card: Page loading")
		Session("StartedDate") = Date.Now	'this is use for keeping track the time when page first load which will be used to compare with submite_date
		'Dim log As log4net.ILog = log4net.LogManager.GetLogger(Me.GetType())
		ucPageHeader._currentConfig = _CurrentWebsiteConfig
		_previousPage = "creditCardInfo"
		_idPrefix = "cc"
		Dim ccInfo As bl_Inc_CreditCardInfo = CType(LoadControl("~/bl/Inc/CreditCardInfo.ascx"), bl_Inc_CreditCardInfo)
		ccInfo._branchOptionsStr = GetBranches("CC")
		ccInfo.MappedShowFieldLoanType = mappedShowFieldLoanType
		ccInfo._LoanPurposeList = oBusinessLoan.CardPurposes

		Dim tempCardList As New Dictionary(Of String, String)
        For Each item As Tuple(Of String, String, String, String) In oBusinessLoan.CreditCardNames
            If item.Item1 <> "" Then
                If Not tempCardList.ContainsKey(item.Item1) Then
                    tempCardList.Add(item.Item1, item.Item2)
                End If
            End If
        Next
        ''credit card type
        Dim ccTypes = oBusinessLoan.CreditCardTypes
        Dim ccTypesDropdown = ""
        Dim hiddenCCType = ""
        If oBusinessLoan.CreditCardNames.Any() Then hiddenCCType = "hidden"
        Select Case True
            Case ccTypes.Count = 1
                Dim sCCKeys(0) As String
                Dim sCCValues(0) As String
                ccTypes.Keys.CopyTo(sCCKeys, 0)
                ccTypes.Values.CopyTo(sCCValues, 0)
                ccTypesDropdown = Common.RenderDropdownlist(sCCKeys, sCCValues, " ", sCCKeys(0))
                ''make sure credit card information page is not empty before hiding the credit card type case
                If oBusinessLoan.CardPurposes.Count > 0 Or oBusinessLoan.CreditCardNames.Count > 0 Then hiddenCCType = "hidden"
            Case ccTypes.Count < 1
                hiddenCCType = "hidden"
            Case Else
                ccTypesDropdown = Common.RenderDropdownlist(ccTypes, " ")
        End Select

        ccInfo._HiddenCCType = hiddenCCType
        ccInfo._CreditCardTypeDropdown = ccTypesDropdown
        ''implement EnableBranchSection later(now display branch section)
        ccInfo.EnableBranchSection = EnableBranchSection
		ccInfo._branchOptionsStr = GetBranches("CC")
		ccInfo._requiredBranch = _requiredBranch
		ccInfo._CreditCardList = New JavaScriptSerializer().Serialize(tempCardList)
		ccInfo._CreditCardNames = oBusinessLoan.CreditCardNames
		ccInfo._LoanPurposeCategory = New JavaScriptSerializer().Serialize(Common.getItemsFromConfig(_CurrentWebsiteConfig, "ENUMS/BL_CREDIT_CARD_PURPOSES/ITEM", "value", "category"))
		ccInfo._LogoUrl = _LogoUrl
		plhLoanInfoPage.Controls.Add(ccInfo)
		''disclosure
		InitBusinessDisclosure(oBusinessLoan.CardDisclosures)
	End Sub
	Protected Sub initBusinessVehicleLoan(ByVal oBusinessLoan As CBusinessLoan)
		log.Info("Business Vehicle Loan: Page loading")
		Session("StartedDate") = Date.Now	'this is use for keeping track the time when page first load which will be used to compare with submite_date
		'Dim log As log4net.ILog = log4net.LogManager.GetLogger(Me.GetType())
		ucPageHeader._currentConfig = _CurrentWebsiteConfig
		_previousPage = "vehicleInfo"
		_idPrefix = "vl"
		Dim vlInfo As bl_Inc_VehicleLoanInfo = CType(LoadControl("~/bl/Inc/VehicleLoanInfo.ascx"), bl_Inc_VehicleLoanInfo)
		Dim _VehicleLoanPurposeList = oBusinessLoan.VehiclePurposes
		Dim _VehicleLoanPurposeDropdown As String = ""
		vlInfo._branchOptionsStr = GetBranches("VL")
		''implement EnableBranchSection later(now display branch section)
		vlInfo.EnableBranchSection = EnableBranchSection
		vlInfo.MappedShowFieldLoanType = mappedShowFieldLoanType
		vlInfo._requiredBranch = _requiredBranch
		vlInfo.VinBarcodeScanEnabled = Common.getNodeAttributes(_CurrentWebsiteConfig, "VEHICLE_LOAN/VISIBLE", "vin_barcode_scan").ToUpper().Equals("Y")
		vlInfo.EnableVinNumber = CheckShowField("divVinNumber", "", False) Or (IsInMode("777") AndAlso IsInFeature("visibility"))
		vlInfo._autoTermTextboxEnable = Common.getVisibleAttribute(_CurrentWebsiteConfig, "term_free_form_mode_vl").ToUpper

		If _VehicleLoanPurposeList.Count > 0 Then
			If _VehicleLoanPurposeList.Count = 1 Then ''make default
				Dim ddlPurposeKey(0) As String
				Dim ddlPurposeValue(0) As String
				_VehicleLoanPurposeList.Keys.CopyTo(ddlPurposeKey, 0)
				_VehicleLoanPurposeList.Values.CopyTo(ddlPurposeValue, 0)
				_VehicleLoanPurposeDropdown = Common.RenderDropdownlistWithEmpty(ddlPurposeKey, ddlPurposeValue, "", "--Please Select--", ddlPurposeKey(0))
			Else
				_VehicleLoanPurposeDropdown = Common.RenderDropdownlistWithEmpty(_VehicleLoanPurposeList, "", "--Please Select--")
			End If
			vlInfo._HasLoanPurpose = "Y"
		Else
			_VehicleLoanPurposeDropdown = Common.RenderDropdownlistWithEmpty(New String() {}, "", "--Please Select--")
			vlInfo._HasLoanPurpose = "N"
		End If
		Dim ddlLoanTerm As Dictionary(Of String, String) = _CurrentWebsiteConfig.GetEnumItems("VEHICLE_LOAN_TERM")
		If ddlLoanTerm.Count > 0 Then
			vlInfo._LoanTermDropdown = Common.RenderDropdownlistWithEmpty(ddlLoanTerm, "", "--Please Select--")
		End If
		Dim _VehicleTypesParsed As New Dictionary(Of String, String)
		_VehicleTypesParsed = _CurrentWebsiteConfig.GetEnumItems("VEHICLE_TYPES") ''override from xml config
		If _VehicleTypesParsed.Count = 0 AndAlso Not String.IsNullOrEmpty(_CurrentWebsiteConfig.LenderCode) Then
			Dim downloadVehicleTypes As List(Of DownloadedSettings.CListItem)
			downloadVehicleTypes = CDownloadedSettings.VehicleTypes(_CurrentWebsiteConfig)
			For Each item As CListItem In downloadVehicleTypes
				_VehicleTypesParsed.Add(item.Value, item.Text)
			Next
		End If
		If _VehicleTypesParsed.Count = 1 Then
			Dim ddlVLKey(0) As String
			Dim ddlVLValue(0) As String
			_VehicleTypesParsed.Keys.CopyTo(ddlVLKey, 0)
			_VehicleTypesParsed.Values.CopyTo(ddlVLValue, 0)
			vlInfo._VehicleTypes = Common.RenderDropdownlistWithEmpty(ddlVLKey, ddlVLValue, "", "--Please Select--", ddlVLKey(0))
		Else
			vlInfo._VehicleTypes = Common.RenderDropdownlistWithEmpty(_VehicleTypesParsed, "", "--Please Select--")
		End If

		vlInfo._InterestRate = GetInterestRate()
		vlInfo._VehicleLoanPurposeList = _VehicleLoanPurposeList
		vlInfo._VehicleLoanPurposeDropdown = _VehicleLoanPurposeDropdown
		vlInfo._CarMakeDropdown = Common.RenderDrodownListFromTextWithEmpty(Server.MapPath(Common.CAR_MAKE_PATH), "", "--Please Select--")
		vlInfo._MotorCycleMakeDropdown = Common.RenderDrodownListFromTextWithEmpty(Server.MapPath(Common.MOTOR_CYCLE_MAKE_PATH), "", "--Please Select--")
		vlInfo._RVOrBoatMakeDropdown = Common.RenderDrodownListFromTextWithEmpty(Server.MapPath(Common.RV_OR_BOAT_MAKE_PATH), "", "--Please Select--")
        vlInfo._LoanPurposeCategory = New JavaScriptSerializer().Serialize(Common.getItemsFromConfig(_CurrentWebsiteConfig, "ENUMS/BL_VEHICLE_PURPOSES/ITEM", "value", "category"))
        vlInfo._LogoUrl = _LogoUrl
		plhLoanInfoPage.Controls.Add(vlInfo)
		''disclosure
		InitBusinessDisclosure(oBusinessLoan.VehicleDisclosures)
	End Sub
	Private Sub InitBusinessDisclosure(ByVal oDisclosures As List(Of String))
		If CheckShowField("divDisclosureSection", "", True) Or (IsInMode("777") AndAlso IsInFeature("visibility")) Then


			ucDisclosures.Disclosures = oDisclosures
			ucDisclosures.MappedShowFieldLoanType = mappedShowFieldLoanType
			ucDisclosures.EnableEmailMeButton = CheckShowField("divEmailMeButton", "", False) Or (IsInMode("777") AndAlso IsInFeature("visibility"))
			''get credit pull message from XA_LOAN if it exist
			Dim oXANode As XmlNode = _CurrentWebsiteConfig._webConfigXML.SelectSingleNode("CREDIT_PULL_MESSAGE")
			If oXANode IsNot Nothing AndAlso Not String.IsNullOrEmpty(oXANode.InnerText) Then
				_CreditPullMessage = oXANode.InnerText
			Else
				''credit pull messagein disclosure section
				Dim oNode As XmlNode = _CurrentWebsiteConfig._webConfigXML.SelectSingleNode("CREDIT_PULL_MESSAGE")
				If oNode IsNot Nothing Then
					_CreditPullMessage = oNode.InnerText
				End If
			End If
		End If
	End Sub
    Private Sub InitCustomQuestion()
        Dim oDownloadedBLCQs As List(Of CCustomQuestionXA) = CCustomQuestionNewAPI.getDownloadedXACustomQuestions(_CurrentWebsiteConfig, False, "BL")
        ''filter business loan custom questions
        Dim oCustomQuestions As New List(Of CCustomQuestionXA)
		If oDownloadedBLCQs IsNot Nothing Then
			'' Set custom questions
			Dim oAllowedCustomQuestionNames = GetAllowedBLCustomQuestionNames()
			If oAllowedCustomQuestionNames.Count > 0 Then
				For Each cqName In oAllowedCustomQuestionNames
					Dim oCQ = oDownloadedBLCQs.FirstOrDefault(Function(cq) cq.Name = cqName)
					If oCQ IsNot Nothing Then
						oCustomQuestions.Add(oCQ)
					End If
				Next
			Else
				oCustomQuestions = oDownloadedBLCQs
			End If
		End If
	End Sub
    Private Function GetAllowedBLCustomQuestionNames() As List(Of String)
        Dim productNames As New List(Of String)
        Dim oNodes As XmlNodeList = _CurrentWebsiteConfig._webConfigXML.SelectNodes("BUSINESS_LOAN/CUSTOM_QUESTIONS/QUESTION")
        If oNodes IsNot Nothing AndAlso oNodes.Count > 0 Then
            For Each child As XmlNode In oNodes
                If child.Attributes("name") Is Nothing Then
                    logError("QUESTION name is not correct.", Nothing)
                End If
                If child.Attributes("name") IsNot Nothing AndAlso child.Attributes("name").InnerXml <> "" Then
                    productNames.Add(child.Attributes("name").InnerXml)
                End If
            Next
        End If
        Return productNames
    End Function

End Class
