﻿
Imports DownloadedSettings
Imports Newtonsoft.Json
Imports LPQMobile.BO
Imports LPQMobile.Utils
Imports System.IO
Imports System.Xml
Imports log4net
Imports System.Web.Script.Serialization
Imports System.Web.Security.AntiXss
Partial Class bl_Inc_OtherLoanInfo
    Inherits CBaseUserControl
    Public Property _LoanPurposeDropdown As String
    Public Property _LoanPurposeList As Dictionary(Of String, CTextValueCatItem)
    Public Property _BranchDropdown As String
    Public Property _address_key As String = ""
    ''upload documents: ID, Driver license ...

    'Protected _bgTheme As String
    Public Property _hasScanDocumentKey As String
    Public Property _LoanTermDropdown As String = ""
    'Protected _LineOfCreditVisibility As Boolean
    Public Property _branchOptionsStr As String = ""
    Public Property LaserDLScanEnabled As Boolean = False
    Public Property LegacyDLScanEnabled As Boolean = False
    Public Property _requiredBranch As String = ""
    Public Property EnableBranchSection As Boolean = False
    Public Property _LogoUrl As String = ""
    Public Property _CurrentLenderRef As String
    Public Property _interestRate As Double
    Public Property _textAndroidTel As String = "text"
#Region "property for sso"

    Protected ReadOnly Property LoanAmount() As String
        Get
            Return Common.SafeString(AntiXssEncoder.HtmlEncode(Request.Params("LAmount"), True))
        End Get
    End Property

    Protected ReadOnly Property Term() As String
        Get
            Return Common.SafeString(AntiXssEncoder.HtmlEncode(Request.Params("Term"), True))
        End Get
    End Property

    Protected ReadOnly Property LoanPurpose() As String
        Get
            Return Common.SafeString(AntiXssEncoder.HtmlEncode(Request.Params("LoanPurpose"), True))
        End Get
    End Property

    Protected ReadOnly Property IsLOC() As String
        Get
            Return Common.SafeString(AntiXssEncoder.HtmlEncode(Request.Params("IsLOC"), True))
        End Get
    End Property

#End Region
    Public Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
		If Not IsPostBack Then

			xaApplicationQuestionLoanPage.Header = HeaderUtils.RenderPageTitle(0, "Please answer the following question(s)", True)

		End If
	End Sub
End Class
