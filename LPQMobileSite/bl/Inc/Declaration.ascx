﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="Declaration.ascx.vb"
    Inherits="bl_Inc_Declaration" %>
<div>
   <%=HeaderUtils.RenderPageTitle(0, "Declarations", True)%>
</div>
<div id="<%=IDPrefix%>divDeclarationSection" class="declaration-wrapper">
	<div style="height: 20px;">
		<span id="<%=IDPrefix%>spValidateDeclarationMessage"></span>
	</div>
	<div class="Title">
		<div class="col-sm-7 col-md-8 col-xs-12">
		   <span class="declaration-font RequiredIcon"> 1.&nbsp;Do you currently have any outstanding judgments or have you ever filed for bankruptcy, had a debt adjustment plan confirmed under Chapter 13, Had Property Foreclosed Upon or Repossessed in the Last 7 yrs, or been a party in a lawsuit?</span>
		</div>
		<div class="col-sm-5 col-md-4 col-xs-12">
			<fieldset data-role="controlgroup" data-type="horizontal">
				<input type="radio" name="<%=IDPrefix%>ddl_has_judge_bankrupt_foreclosure" id="<%=IDPrefix%>ddl_has_judge_bankrupt_foreclosure_no"
					value="N" data-mini="true" />
				<label for="<%=IDPrefix%>ddl_has_judge_bankrupt_foreclosure_no">
					No</label>
				<input type="radio" name="<%=IDPrefix%>ddl_has_judge_bankrupt_foreclosure" id="<%=IDPrefix%>ddl_has_judge_bankrupt_foreclosure_yes"
					value="Y" data-mini="true" />
				<label for="<%=IDPrefix%>ddl_has_judge_bankrupt_foreclosure_yes">
					Yes</label>
			</fieldset>
		</div>
		<div style="clear: both">
		</div>
	</div>
   
	<div class="Title">
		<div class="col-sm-7 col-md-8 col-xs-12">
		   <span class="declaration-font RequiredIcon">  2.&nbsp;Have you ever been declared bankrupt or filed a petition for chapter 7 or 13?</span>
		</div>
		<div class="col-sm-5 col-md-4 col-xs-12">
			<fieldset data-role="controlgroup" data-type="horizontal">
				<input type="radio" name="<%=IDPrefix%>ddl_has_declare_bankrupt" id="<%=IDPrefix%>ddl_has_declare_bankrupt_no" value="N"
					data-mini="true" />
				<label for="<%=IDPrefix%>ddl_has_declare_bankrupt_no">
					No</label>
				<input type="radio" name="<%=IDPrefix%>ddl_has_declare_bankrupt" id="<%=IDPrefix%>ddl_has_declare_bankrupt_yes" value="Y"
					data-mini="true" />
				<label for="<%=IDPrefix%>ddl_has_declare_bankrupt_yes">
					Yes</label>
			</fieldset>
		</div>
		<div style="clear: both">
		</div>
	</div>
   
	<div class="Title">
		<div class="col-sm-7 col-md-8 col-xs-12">
		  <span class="declaration-font RequiredIcon">3.&nbsp;Have you ever filed for bankruptcy or had a debt adjustment plan confirmed under chapter 13 in the past 10 years?</span>
		</div>
		<div class="col-sm-5 col-md-4 col-xs-12">
			<fieldset data-role="controlgroup" data-type="horizontal">
				<input type="radio" name="<%=IDPrefix%>ddl_has_chapter_13" id="<%=IDPrefix%>ddl_has_chapter_13_no" value="N" data-mini="true" />
				<label for="<%=IDPrefix%>ddl_has_chapter_13_no">
					No</label>
				<input type="radio" name="<%=IDPrefix%>ddl_has_chapter_13" id="<%=IDPrefix%>ddl_has_chapter_13_yes" value="Y" data-mini="true" />
				<label for="<%=IDPrefix%>ddl_has_chapter_13_yes">
					Yes</label>
			</fieldset>
		</div>
		<div style="clear: both">
		</div>
	</div>
	<div class="Title">
		<div class="col-sm-7 col-md-8 col-xs-12">
		   <span class="declaration-font RequiredIcon"> 4.&nbsp;Have you any outstanding judgments?</span> 
		</div>
		<div class="col-sm-5 col-md-4 col-xs-12">
			<fieldset data-role="controlgroup" data-type="horizontal">
                	<input type="radio" name="<%=IDPrefix%>ddl_has_judgement" id="<%=IDPrefix%>ddl_has_judgement_no"
					value="N" data-mini="true" />
				<label for="<%=IDPrefix%>ddl_has_judgement_no">
					No</label>
				<input type="radio" name="<%=IDPrefix%>ddl_has_judgement" id="<%=IDPrefix%>ddl_has_judgement_yes"
					value="Y" data-mini="true" />
				<label for="<%=IDPrefix%>ddl_has_judgement_yes">
					Yes</label>
			</fieldset>
		</div>
		<div style="clear: both">
		</div>
	</div>
   
	<div class="Title">
		<div class="col-sm-7 col-md-8 col-xs-12">
			 <span class="declaration-font RequiredIcon">5.&nbsp;Are you a party to a lawsuit?</span>
		</div>
		<div class="col-sm-5 col-md-4 col-xs-12">
			<fieldset data-role="controlgroup" data-type="horizontal">
				<input type="radio" name="<%=IDPrefix%>ddl_has_lawsuit_party" id="<%=IDPrefix%>ddl_has_lawsuit_party_no" value="N"
					data-mini="true" />
				<label for="<%=IDPrefix%>ddl_has_lawsuit_party_no">
					No</label>
				<input type="radio" name="<%=IDPrefix%>ddl_has_lawsuit_party" id="<%=IDPrefix%>ddl_has_lawsuit_party_yes" value="Y"
					data-mini="true" />
				<label for="<%=IDPrefix%>ddl_has_lawsuit_party_yes">
					Yes</label>
            </fieldset>
		</div>
		<div style="clear: both">
		</div>
	</div>
	<div class="Title">
		<div class="col-sm-7 col-md-8 col-xs-12">
			<span class="declaration-font RequiredIcon"> 6.&nbsp;. Have you ever had any auto, furniture or property repossessed?</span>
		</div>
		<div class="col-sm-5 col-md-4 col-xs-12">
			<fieldset data-role="controlgroup" data-type="horizontal">
				<input type="radio" name="<%=IDPrefix%>ddl_has_reposession" id="<%=IDPrefix%>ddl_has_reposession_no"
					value="N" data-mini="true" />
				<label for="<%=IDPrefix%>ddl_has_reposession_no">
					No</label>
				<input type="radio" name="<%=IDPrefix%>ddl_has_reposession" id="<%=IDPrefix%>ddl_has_reposession_yes"
					value="Y" data-mini="true" />
				<label for="<%=IDPrefix%>ddl_has_reposession_yes">
					Yes</label>
			</fieldset>
		</div>
		<div style="clear: both">
		</div>
	</div>
	<div class="Title">
		<div class="col-sm-7 col-md-8 col-xs-12">
			<span class="declaration-font RequiredIcon"> 7.&nbsp; Have you any obligations not listed?</span>
		</div>
		<div class="col-sm-5 col-md-4 col-xs-12">
			<fieldset data-role="controlgroup" data-type="horizontal">
				<input type="radio" name="<%=IDPrefix%>ddl_has_other_obligation" id="<%=IDPrefix%>ddl_has_other_obligation_no"
					value="N" data-mini="true" />
				<label for="<%=IDPrefix%>ddl_has_other_obligation_no">
					No</label>
				<input type="radio" name="<%=IDPrefix%>ddl_has_other_obligation" id="<%=IDPrefix%>ddl_has_other_obligation_yes"
					value="Y" data-mini="true" />
				<label for="<%=IDPrefix%>ddl_has_other_obligation_yes">
					Yes</label>
			</fieldset>
		</div>
		<div style="clear: both">
		</div>
	</div>
	<div class="Title">
		<div class="col-sm-7 col-md-8 col-xs-12">
			<span class="declaration-font RequiredIcon"> 8.&nbsp; Do you have any past due bills?</span>
		</div>
		<div class="col-sm-5 col-md-4 col-xs-12">
				<fieldset data-role="controlgroup" data-type="horizontal">
				<input type="radio" name="<%=IDPrefix%>ddl_has_past_due_bills" id="<%=IDPrefix%>ddl_has_past_due_bills_no"
					value="N" data-mini="true" />
				<label for="<%=IDPrefix%>ddl_has_past_due_bills_no">
					No</label>
				<input type="radio" name="<%=IDPrefix%>ddl_has_past_due_bills" id="<%=IDPrefix%>ddl_has_past_due_bills_yes"
					value="Y" data-mini="true" />
				<label for="<%=IDPrefix%>ddl_has_past_due_bills_yes">
					Yes</label>
			</fieldset>
		</div>
		<div style="clear: both">
		</div>
	</div>
	<div class="Title">
		<div class="col-sm-7 col-md-8 col-xs-12">
			 <span class="declaration-font RequiredIcon">9.&nbsp;Are you a Co-maker, Co-signer or Guarantor on any loan?</span>
		</div>
		<div class="col-sm-5 col-md-4 col-xs-12">
				<fieldset data-role="controlgroup" data-type="horizontal">
				<input type="radio" name="<%=IDPrefix%>ddl_has_co_maker" id="<%=IDPrefix%>ddl_has_co_maker_no" value="N" data-mini="true" />
				<label for="<%=IDPrefix%>ddl_has_co_maker_no">
					No</label>
				<input type="radio" name="<%=IDPrefix%>ddl_has_co_maker" id="<%=IDPrefix%>ddl_has_co_maker_yes" value="Y" data-mini="true" />
				<label for="<%=IDPrefix%>ddl_has_co_maker_yes">
					Yes</label>
			</fieldset>
		</div>
		<div style="clear: both">
		</div>
      
        <div id="<%=IDPrefix%>divHasCoMaker" class="hidden">
            <div data-role="fieldcontain">
                <label for="<%=IDPrefix%>primaryName" class="RequiredIcon">Primary Name</label>
                <input type="text" id="<%=IDPrefix%>primaryName" />
            </div>
             <div data-role="fieldcontain">
                <label for="<%=IDPrefix%>creditorName" class="RequiredIcon">Creditor Name</label>
                <input type="text" id="<%=IDPrefix%>creditorName" />
            </div>
              <div data-role="fieldcontain">
                <label for="<%=IDPrefix%>Amount" class="RequiredIcon">Amount</label>
                <input type="<%=_textAndroidTel%>" pattern="[0-9]*" id="<%=IDPrefix%>Amount" class="money" maxlength="12"/>
            </div>
        </div>
	</div>
    
	<div class="Title">
		<div class="col-sm-7 col-md-8 col-xs-12">
			 <span class="declaration-font RequiredIcon">10.&nbsp; Have you ever had credit in any other name?</span>
		</div>
		<div class="col-sm-5 col-md-4 col-xs-12">
			<fieldset data-role="controlgroup" data-type="horizontal">
				<input type="radio" name="<%=IDPrefix%>ddl_has_other_credit_name" id="<%=IDPrefix%>ddl_has_other_credit_name_no"
					value="N" data-mini="true" />
				<label for="<%=IDPrefix%>ddl_has_other_credit_name_no">
					No</label>
				<input type="radio" name="<%=IDPrefix%>ddl_has_other_credit_name" id="<%=IDPrefix%>ddl_has_other_credit_name_yes"
					value="Y" data-mini="true" />
				<label for="<%=IDPrefix%>ddl_has_other_credit_name_yes">
					Yes</label>
			</fieldset>
		</div>
		<div style="clear: both">
		</div>
        <div id="<%=IDPrefix%>divHasOtherCreditName" class="hidden">
            <div data-role="fieldcontain">
                <label for="<%=IDPrefix%>otherName" class="RequiredIcon">Other Name</label>
                <input type="text" id="<%=IDPrefix%>otherName" />
            </div>
        </div>
	</div>
	<div class="Title">
		<div class="col-sm-7 col-md-8 col-xs-12">
			<span class="declaration-font RequiredIcon"> 11.&nbsp;Is your income likely to decline in the next two years?</span>
		</div>
		<div class="col-sm-5 col-md-4 col-xs-12">
			<fieldset data-role="controlgroup" data-type="horizontal">
				<input type="radio" name="<%=IDPrefix%>ddl_has_income_decline" id="<%=IDPrefix%>ddl_has_income_decline_no"
					value="N" data-mini="true" />
				<label for="<%=IDPrefix%>ddl_has_income_decline_no">
					No</label>
				<input type="radio" name="<%=IDPrefix%>ddl_has_income_decline" id="<%=IDPrefix%>ddl_has_income_decline_yes"
					value="Y" data-mini="true" />
				<label for="<%=IDPrefix%>ddl_has_income_decline_yes">
					Yes</label>
			</fieldset>
		</div>
		<div style="clear: both">
		</div>
	</div>
    <div class="Title">
		<div class="col-sm-7 col-md-8 col-xs-12">
			<span class="declaration-font RequiredIcon"> 12.&nbsp;Have you any suits pending, judgments filed, alimony or support awards against you?</span>
		</div>
		<div class="col-sm-5 col-md-4 col-xs-12">
			<fieldset data-role="controlgroup" data-type="horizontal">
				<input type="radio" name="<%=IDPrefix%>ddl_has_suits_pending" id="<%=IDPrefix%>ddl_has_suits_pending_no"
					value="N" data-mini="true" />
				<label for="<%=IDPrefix%>ddl_has_suits_pending_no">
					No</label>
				<input type="radio" name="<%=IDPrefix%>ddl_has_suits_pending" id="<%=IDPrefix%>ddl_has_suits_pending_yes"
					value="Y" data-mini="true" />
				<label for="<%=IDPrefix%>ddl_has_suits_pending_yes">
					Yes</label>
			</fieldset>
		</div>
		<div style="clear: both">
		</div>
	</div>
    <div class="Title">
		<div class="col-sm-7 col-md-8 col-xs-12">
			 <span class="declaration-font RequiredIcon">13.&nbsp;Are you obligated to make Alimony, Support or Maintenance Payments?</span>
		</div>
		<div class="col-sm-5 col-md-4 col-xs-12">
			<fieldset data-role="controlgroup" data-type="horizontal">
				<input type="radio" name="<%=IDPrefix%>ddl_has_alimony" id="<%=IDPrefix%>ddl_has_alimony_no" value="N" data-mini="true" />
				<label for="<%=IDPrefix%>ddl_has_alimony_no">
					No</label>
				<input type="radio" name="<%=IDPrefix%>ddl_has_alimony" id="<%=IDPrefix%>ddl_has_alimony_yes" value="Y" data-mini="true" />
				<label for="<%=IDPrefix%>ddl_has_alimony_yes">
					Yes</label>
			</fieldset>
		</div>
		<div style="clear: both">
		</div>
         <div id="<%=IDPrefix%>divHasAlimony" class="hidden">
              <div data-role="fieldcontain">
                <label for="<%=IDPrefix%>recipientName" class="RequiredIcon">Recipient Name</label>
                <input type="text" id="<%=IDPrefix%>recipientName" />
            </div>
             <div data-role="fieldcontain">
                <label for="<%=IDPrefix%>recipientAddress" class="RequiredIcon">Recipient Address</label>
                <textarea rows="3" id="<%=IDPrefix%>recipientAddress" ></textarea>
            </div>

              <div data-role="fieldcontain">
                <label for="<%=IDPrefix%>alimonyAmount" class="RequiredIcon">Monthly Alimony</label>
                <input type="<%=_textAndroidTel%>" pattern="[0-9]*" id="<%=IDPrefix%>alimonyAmount" class="money" maxlength="12"/>
            </div>
            
              <div data-role="fieldcontain">
                <label for="<%=IDPrefix%>seperateAmount" class="RequiredIcon">Monthly Separate Maintenance</label>
                <input type="<%=_textAndroidTel%>" pattern="[0-9]*" id="<%=IDPrefix%>seperateAmount" class="money" maxlength="12"/>
            </div>
             <div data-role="fieldcontain">
                <label for="<%=IDPrefix%>childSupportAmount" class="RequiredIcon">Monthly Child Support</label>
                <input type="<%=_textAndroidTel%>" pattern="[0-9]*" id="<%=IDPrefix%>childSupportAmount" class="money" maxlength="12"/>
            </div>
        </div>
	</div>
</div>

<script type="text/javascript">
     var <%=IDPrefix%>DeclarationList = {};
    <%=IDPrefix%>DeclarationList["bankrupt_foreclosure"] = " 1. Do you currently have any outstanding judgments or have you ever filed for bankruptcy, had a debt adjustment plan confirmed under Chapter 13, Had Property Foreclosed Upon or Repossessed in the Last 7 yrs, or been a party in a lawsuit?";
    <%=IDPrefix%>DeclarationList["declare_bankruptcy"] = " 2. Have you ever been declared bankrupt or filed a petition for chapter 7 or 13? ";
    <%=IDPrefix%>DeclarationList["has_chapter_13"] = "3. Have you ever filed for bankruptcy or had a debt adjustment plan confirmed under chapter 13 in the past 10 years";
    <%=IDPrefix%>DeclarationList["outstanding_judgement"] = "4. Have you any outstanding judgments?";
    <%=IDPrefix%>DeclarationList["in_lawsuit"] = "5. Are you a party to a lawsuit?";
    <%=IDPrefix%>DeclarationList["has_possession"] = "6. Have you ever had any auto, furniture or property repossessed?";
    <%=IDPrefix%>DeclarationList["other_obligations"] = "7. Have you any obligations not listed? ";
    <%=IDPrefix%>DeclarationList["past_due_bills"] = "8. Do you have any past due bills?";  
    <%=IDPrefix%>DeclarationList["co_maker"] = "9. Are you a Co-maker, Co-signer or Guarantor on any loan?";
    <%=IDPrefix%>DeclarationList["other_credit_name"] = "10. Have you ever had credit in any other name?";
    <%=IDPrefix%>DeclarationList["income_decline"] = "11. Is your income likely to decline in the next two years?";
    <%=IDPrefix%>DeclarationList["suits_pending"] = "12. Have you any suits pending, judgments filed, alimony or support awards against you? ";
    <%=IDPrefix%>DeclarationList["has_alimony"] = "13. Are you obligated to make Alimony, Support or Maintenance Payments?";
  
    $(function() {
        <%=IDPrefix%>InitDeclaration();
	    $("input:radio", $("#<%=IDPrefix%>divDeclarationSection")).on("change", function() {
	        $("#<%=IDPrefix%>divDeclarationSection").trigger("change");
	    });
    	$("#<%=IDPrefix%>divDeclarationSection").observer({
    	    validators: [    
				function (partial) {
					var $element = $(this);
					var validator = true;
					var msg = "Please answer all of the declaration questions";
					validator = $('input[name= <%=IDPrefix%>ddl_has_judge_bankrupt_foreclosure]:checked', $element).length > 0;
				    if (validator === false) return msg;
                    
					validator = $('input[name=<%=IDPrefix%>ddl_has_declare_bankrupt]:checked', $element).length > 0;
					if (validator === false) return msg;

					validator = $('input[name=<%=IDPrefix%>ddl_has_chapter_13]:checked', $element).length > 0;
					if (validator === false) return msg;

					validator = $('input[name=<%=IDPrefix%>ddl_has_judgement]:checked', $element).length > 0;
					if (validator === false) return msg;

					validator = $('input[name=<%=IDPrefix%>ddl_has_lawsuit_party]:checked', $element).length > 0;
					if (validator === false) return msg;

					validator = $('input[name=<%=IDPrefix%>ddl_has_reposession]:checked', $element).length > 0;
					if (validator === false) return msg;

					validator = $('input[name=<%=IDPrefix%>ddl_has_other_obligation]:checked', $element).length > 0;
					if (validator === false) return msg;

					validator = $('input[name=<%=IDPrefix%>ddl_has_past_due_bills]:checked', $element).length > 0;
					if (validator === false) return msg;
                    var $hasCoMaker = $('input[name=<%=IDPrefix%>ddl_has_co_maker]:checked', $element);
					validator = $hasCoMaker.length > 0;
				    if (validator === false) {
				        return msg;
				    } else if ($hasCoMaker.length > 0 && $hasCoMaker.val() == "Y") {       	       
				       if ($hasCoMaker.val() == "Y") {
				           if($('#<%=IDPrefix%>primaryName').val() ==""){
				               return msg;
				           }
				           if($('#<%=IDPrefix%>creditorName').val() ==""){
				               return msg;
				           }
				          
				           if ($('#<%=IDPrefix%>Amount').val() == "") {
				               return msg;
				           }                     
				         } 
				    }
				    var $hasOtherCreditName = $('input[name=<%=IDPrefix%>ddl_has_other_credit_name]:checked', $element);
					validator = $hasOtherCreditName.length > 0;
					if (validator === false) {
					    return msg;
					} else if ($hasOtherCreditName.length > 0 && $hasOtherCreditName.val() == "Y") {
					    if ($('#<%=IDPrefix%>otherName').val() == "") {
					        return msg;
					    }
					}

					validator = $('input[name=<%=IDPrefix%>ddl_has_income_decline]:checked', $element).length > 0;
					if (validator === false) return msg;

					validator = $('input[name=<%=IDPrefix%>ddl_has_suits_pending]:checked', $element).length > 0;
				    if (validator === false) return msg;
				    var $hasAlimony = $('input[name=<%=IDPrefix%>ddl_has_alimony]:checked', $element);
				    validator = $hasAlimony.length > 0;
				    if (validator === false) {
				        return msg;
				    } else if ($hasAlimony.length > 0 && $hasAlimony.val() == "Y") {
				        if($('#<%=IDPrefix%>recipientName').val()==""){
				            return msg;
				        }
                        if($('#<%=IDPrefix%>recipientAddress').val()==""){
				            return msg;
                        }
				        if($('#<%=IDPrefix%>alimonyAmount').val()==""){
				            return msg;
                        }
				        if($('#<%=IDPrefix%>seperateAmount').val()==""){
				            return msg;
				        }
				        if ($('#<%=IDPrefix%>childSupportAmount').val() == "") {
				            return msg;
				        }                 
				    }
					return "";
				}
    		],
    		validateOnBlur: true,
    		group: "<%=IDPrefix%>ValidateDeclaration",
			groupType: "complexUI",
    		placeHolder: "#<%=IDPrefix%>spValidateDeclarationMessage"
    	});
    });
   
    function <%=IDPrefix%>ViewDeclaration() {
        var strHtml = "";
        var ddl_has_judge_bankrupt_foreclosure = $('input[name= <%=IDPrefix%>ddl_has_judge_bankrupt_foreclosure]:checked').val();
        var ddl_has_declare_bankrupt = $('input[name= <%=IDPrefix%>ddl_has_declare_bankrupt]:checked').val();
        var ddl_has_chapter_13 = $('input[name=<%=IDPrefix%>ddl_has_chapter_13]:checked').val();
        var ddl_has_judgement = $('input[name=<%=IDPrefix%>ddl_has_judgement]:checked').val();
        var ddl_has_lawsuit_party = $('input[name=<%=IDPrefix%>ddl_has_lawsuit_party]:checked').val();
        var ddl_has_reposession = $('input[name=<%=IDPrefix%>ddl_has_reposession]:checked').val();
        var ddl_has_other_obligation = $('input[name=<%=IDPrefix%>ddl_has_other_obligation]:checked').val();
        var ddl_has_past_due_bills = $('input[name=<%=IDPrefix%>ddl_has_past_due_bills]:checked').val();
        var ddl_has_co_maker = $('input[name=<%=IDPrefix%>ddl_has_co_maker]:checked').val();
        var ddl_has_other_credit_name = $('input[name=<%=IDPrefix%>ddl_has_other_credit_name]:checked').val();
        var ddl_has_income_decline = $('input[name=<%=IDPrefix%>ddl_has_income_decline]:checked').val();
        var ddl_has_suits_pending = $('input[name=<%=IDPrefix%>ddl_has_suits_pending]:checked').val();
        var ddl_has_alimony = $('input[name=<%=IDPrefix%>ddl_has_alimony]:checked').val();
        var primaryName = $('#<%=IDPrefix%>primaryName').val();				          
        var creditorName= $('#<%=IDPrefix%>creditorName').val();				       				          
        var amount = $('#<%=IDPrefix%>Amount').val(); 
        var otherName = $('#<%=IDPrefix%>otherName').val();
        var recipientName = $('#<%=IDPrefix%>recipientName').val();			       
        var recipientAddress = $('#<%=IDPrefix%>recipientAddress').val();				      
        var alimonyAmount = $('#<%=IDPrefix%>alimonyAmount').val();				      
        var seperateAmount = $('#<%=IDPrefix%>seperateAmount').val();				        
        var childSupportAmount = $('#<%=IDPrefix%>childSupportAmount').val();
				                     
				              
        strHtml += '<div class="col-xs-12 row-title"><span class="bold">' + <%=IDPrefix%>DeclarationList["bankrupt_foreclosure"] + '</span></div><div class="col-xs-12 row-data"><span>' + (ddl_has_judge_bankrupt_foreclosure == "Y" ? "Yes" : "No") + '</span></div>';
        strHtml += '<div class="col-xs-12 row-title"><span class="bold">' + <%=IDPrefix%>DeclarationList["declare_bankruptcy"] + '</span></div><div class="col-xs-12 row-data"><span>' + (ddl_has_declare_bankrupt == "Y" ? "Yes" : "No") + '</span></div>';
        strHtml += '<div class="col-xs-12 row-title"><span class="bold">' + <%=IDPrefix%>DeclarationList["has_chapter_13"] + '</span></div><div class="col-xs-12 row-data"><span>' + (ddl_has_chapter_13 == "Y" ? "Yes" : "No") + '</span></div>';
        strHtml += '<div class="col-xs-12 row-title"><span class="bold">' + <%=IDPrefix%>DeclarationList["outstanding_judgement"] + '</span></div><div class="col-xs-12 row-data"><span>' + (ddl_has_judgement == "Y" ? "Yes" : "No") + '</span></div>';
        strHtml += '<div class="col-xs-12 row-title"><span class="bold">' + <%=IDPrefix%>DeclarationList["in_lawsuit"] + '</span></div><div class="col-xs-12 row-data"><span>' + (ddl_has_lawsuit_party == "Y" ? "Yes" : "No") + '</span></div>';
        strHtml += '<div class="col-xs-12 row-title"><span class="bold">' + <%=IDPrefix%>DeclarationList["has_possession"] + '</span></div><div class="col-xs-12 row-data"><span>' + (ddl_has_reposession == "Y" ? "Yes" : "No") + '</span></div>';
        strHtml += '<div class="col-xs-12 row-title"><span class="bold">' + <%=IDPrefix%>DeclarationList["other_obligations"] + '</span></div><div class="col-xs-12 row-data"><span>' + (ddl_has_other_obligation == "Y" ? "Yes" : "No") + '</span></div>';
        strHtml += '<div class="col-xs-12 row-title"><span class="bold">' + <%=IDPrefix%>DeclarationList["past_due_bills"] + '</span></div><div class="col-xs-12 row-data"><span>' + (ddl_has_past_due_bills == "Y" ? "Yes" : "No") + '</span></div>';
        strHtml += '<div class="col-xs-12 row-title"><span class="bold">' + <%=IDPrefix%>DeclarationList["co_maker"] + '</span></div><div class="col-xs-12 row-data"><span>' + (ddl_has_co_maker == "Y" ? "Yes" : "No") + '</span></div>';
        if (ddl_has_co_maker == "Y") {
            strHtml += '<div class="col-xs-12 row-title"><span class="bold">Primary Name</span></div><div class="col-xs-12 row-data"><span>' + primaryName + '</span></div>';
            strHtml += '<div class="col-xs-12 row-title"><span class="bold">Creditor Name</span></div><div class="col-xs-12 row-data"><span>' +creditorName + '</span></div>';
            strHtml += '<div class="col-xs-12 row-title"><span class="bold">Amount</span></div><div class="col-xs-12 row-data"><span>' + Common.FormatCurrency(amount)+ '</span></div>';
        }
        strHtml += '<div class="col-xs-12 row-title"><span class="bold">' + <%=IDPrefix%>DeclarationList["other_credit_name"] + '</span></div><div class="col-xs-12 row-data"><span>' + (ddl_has_other_credit_name == "Y" ? "Yes" : "No") + '</span></div>';
        if (ddl_has_other_credit_name == "Y") {
            strHtml += '<div class="col-xs-12 row-title"><span class="bold">Other Name</span></div><div class="col-xs-12 row-data"><span>' + otherName + '</span></div>';
        }
        strHtml += '<div class="col-xs-12 row-title"><span class="bold">' + <%=IDPrefix%>DeclarationList["income_decline"] + '</span></div><div class="col-xs-12 row-data"><span>' + (ddl_has_income_decline == "Y" ? "Yes" : "No") + '</span></div>';
        strHtml += '<div class="col-xs-12 row-title"><span class="bold">' + <%=IDPrefix%>DeclarationList["suits_pending"] + '</span></div><div class="col-xs-12 row-data"><span>' + (ddl_has_suits_pending == "Y" ? "Yes" : "No") + '</span></div>';
        strHtml += '<div class="col-xs-12 row-title"><span class="bold">' + <%=IDPrefix%>DeclarationList["has_alimony"] + '</span></div><div class="col-xs-12 row-data"><span>' + (ddl_has_alimony == "Y" ? "Yes" : "No") + '</span></div>';
        if (ddl_has_alimony == "Y") {
            strHtml += '<div class="col-xs-12 row-title"><span class="bold">Recipient Name</span></div><div class="col-xs-12 row-data"><span>' + recipientName + '</span></div>';
            strHtml += '<div class="col-xs-12 row-title"><span class="bold">Recipient Address</span></div><div class="col-xs-12 row-data"><span>' + recipientAddress + '</span></div>';
            strHtml += '<div class="col-xs-12 row-title"><span class="bold">Alimony</span></div><div class="col-xs-12 row-data"><span>' + alimonyAmount + '</span></div>';
            strHtml += '<div class="col-xs-12 row-title"><span class="bold">Seperate Maintenance</span></div><div class="col-xs-12 row-data"><span>' + seperateAmount + '</span></div>';
            strHtml += '<div class="col-xs-12 row-title"><span class="bold">Child Support</span></div><div class="col-xs-12 row-data"><span>' + childSupportAmount + '</span></div>';
        }
        $(this).show();
        $(this).prev("div.row").show();
        return strHtml;
    }

    function <%=IDPrefix%>SetDeclarations(appInfo) {
        var primaryName = $('#<%=IDPrefix%>primaryName').val();				          
        var creditorName= $('#<%=IDPrefix%>creditorName').val();				       				          
        var amount = $('#<%=IDPrefix%>Amount').val(); 
        var otherName = $('#<%=IDPrefix%>otherName').val();
        var recipientName = $('#<%=IDPrefix%>recipientName').val();			       
        var recipientAddress = $('#<%=IDPrefix%>recipientAddress').val();				      
        var alimonyAmount = $('#<%=IDPrefix%>alimonyAmount').val();				      
        var seperateAmount = $('#<%=IDPrefix%>seperateAmount').val();				        
        var childSupportAmount = $('#<%=IDPrefix%>childSupportAmount').val();

        appInfo.<%=IDPrefix%>HasBankruptForeclosure = $('input[name=<%=IDPrefix%>dl_has_judge_bankrupt_foreclosure]:checked').val();
        appInfo.<%=IDPrefix%>HasDeclaredBankruptcy = $('input[name=<%=IDPrefix%>ddl_has_declare_bankrupt]:checked').val();
        appInfo.<%=IDPrefix%>HasChapter13 = $('input[name=<%=IDPrefix%>ddl_has_chapter_13]:checked').val();
        appInfo.<%=IDPrefix%>HasJudgment = $('input[name=<%=IDPrefix%>ddl_has_judgement]:checked').val();
        appInfo.<%=IDPrefix%>HasLawsuitParty = $('input[name=<%=IDPrefix%>ddl_has_lawsuit_party]:checked').val();
        appInfo.<%=IDPrefix%>HasRepossession = $('input[name=<%=IDPrefix%>ddl_has_reposession]:checked').val();
        appInfo.<%=IDPrefix%>HasOtherObligation = $('input[name=<%=IDPrefix%>ddl_has_other_obligation]:checked').val();
        appInfo.<%=IDPrefix%>HasPastDueBills = $('input[name=<%=IDPrefix%>ddl_has_past_due_bills]:checked').val();
        appInfo.<%=IDPrefix%>HasCoMaker = $('input[name=<%=IDPrefix%>ddl_has_co_maker]:checked').val();
        appInfo.<%=IDPrefix%>HasOtherCreditName = $('input[name=<%=IDPrefix%>ddl_has_other_credit_name]:checked').val();
        appInfo.<%=IDPrefix%>HasIncomeDecline = $('input[name=<%=IDPrefix%>ddl_has_income_decline]:checked').val();
        appInfo.<%=IDPrefix%>HasSuitsPending = $('input[name=<%=IDPrefix%>ddl_has_suits_pending]:checked').val();
        appInfo.<%=IDPrefix%>HasAlimony = $('input[name=<%=IDPrefix%>ddl_has_alimony]:checked').val();
        if (appInfo.<%=IDPrefix%>HasCoMaker == "Y") {
            appInfo.<%=IDPrefix%>PrimaryName = primaryName;
            appInfo.<%=IDPrefix%>CreditorName = creditorName;
            appInfo.<%=IDPrefix%>Amount = Common.GetFloatFromMoney(amount);
        }
        if (appInfo.<%=IDPrefix%>HasOtherCreditName == "Y") {
            appInfo.<%=IDPrefix%>OtherCreditName = otherName;
        }
        if (appInfo.<%=IDPrefix%>HasAlimony == "Y") {
            appInfo.<%=IDPrefix%>RecipientName = recipientName;
            appInfo.<%=IDPrefix%>RecipientAddress = recipientAddress;
            appInfo.<%=IDPrefix%>AlimonyAmount = Common.GetFloatFromMoney(alimonyAmount);
            appInfo.<%=IDPrefix%>SeperateAmount = Common.GetFloatFromMoney(seperateAmount);
            appInfo.<%=IDPrefix%>ChildSupportAmount = Common.GetFloatFromMoney(childSupportAmount);
        }
    return appInfo;
    }
	function <%=IDPrefix%>autofill_Declarations() {
		$("#<%=IDPrefix%>divDeclarationSection input:radio[value='N']").each(function(idx, rad) {
		    $(rad).prop("checked", true).checkboxradio().checkboxradio("refresh");
		});
	}
    function <%=IDPrefix%>InitDeclaration() {
        var $hasCoMaker = $('input[name=<%=IDPrefix%>ddl_has_co_maker]');
        var $divHasCoMaker= $('#<%=IDPrefix%>divHasCoMaker');
        var $hasOtherCreditName =$('input[name=<%=IDPrefix%>ddl_has_other_credit_name]');
        var $divHasOtherCreditName=$('#<%=IDPrefix %>divHasOtherCreditName');
        var $hasAlimony =$('input[name=<%=IDPrefix%>ddl_has_alimony]');
        var $divHasAlimony =$('#<%=IDPrefix%>divHasAlimony');
        $hasCoMaker.on('change', function () {
            $self = $(this);
            if ($self.val() == 'Y') {
                $divHasCoMaker.removeClass('hidden');
            } else {
                $divHasCoMaker.addClass('hidden');
            }
        });
        $hasOtherCreditName.on('change', function () {
            $self = $(this);
            if ($self.val() == 'Y') {
                $divHasOtherCreditName.removeClass('hidden');
            } else {
                $divHasOtherCreditName.addClass('hidden');
            }
        });
        $hasAlimony.on('change', function () {
            $self = $(this);
            if ($self.val() == 'Y') {
                $divHasAlimony.removeClass('hidden');
            } else {
                $divHasAlimony.addClass('hidden');
            }
        });
    }
</script>

