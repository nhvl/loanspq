﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="BLReferenceInfo.ascx.vb" Inherits="bl_Inc_BLReferenceInfo" %>
<%@ Import Namespace="LPQMobile.Utils" %>
<%@ Reference Control="~/xa/Inc/saAddress.ascx" %>

<div id="<%=IDPrefix%>divReferenceInformation" <%=IIf(IsInMode("777") AndAlso IsInFeature("visibility"), "class=""showfield-section"" data-show-field-section-id=""" & BuildShowFieldSectionID("divReferenceInformation") & """  data-section-name='Reference Information'", "")%>>
	<div style="margin-top: 10px;" class="hide-able" id="<%=IDPrefix%>divShowReferenceInfoLink">
		<a id="<%=IDPrefix%>showReferenceInfo" href="#" data-mode="self-handle-event" status="N" style="cursor: pointer; font-weight: bold;" class="header_theme2 shadow-btn plus-circle-before">Add a reference</a>
	</div>

	<div id="<%=IDPrefix%>divReferenceInfoPanel" style="display: none;">
        <%-- 
		<div style = "font-size: 17px; font-weight: bold; padding-top: 15px" > Reference Information</div>
                --%>
          <%= HeaderUtils.RenderPageTitle(0, "Primary Reference", True)%>
		<div data-role="fieldcontain">
			<label for="<%=IDPrefix%>PrimaryFirstName">First Name</label>
			<input type="text" id="<%=IDPrefix%>PrimaryFirstName" />
		</div>
		<div data-role="fieldcontain">
			<label for="<%=IDPrefix%>PrimaryLastName">Last Name</label>
			<input type="text" id="<%=IDPrefix%>PrimaryLastName" />
		</div>
		<div data-role="fieldcontain">
			<label for="<%=IDPrefix%>PrimaryEmail">Email</label>
			<input type="email" id="<%=IDPrefix%>PrimaryEmail" maxlength="50" />
		</div>

		<div data-role="fieldcontain" >
			<div>
				<div style="display: inline-block;"><label for="<%=IDPrefix%>PrimaryPhone" style="white-space: nowrap;">Phone</label></div>
				<div style="display: inline-block;"><label class="phone_format rename-able" style="white-space: nowrap; font-style: italic;">(xxx) xxx-xxxx</label></div>
			</div>
			<input type="<%=TextAndroidTel%>" pattern="[0-9]*" name="phone" id="<%=IDPrefix%>PrimaryPhone" class="inphone" maxlength='14' />
		</div>
		<div data-role="fieldcontain">
			<label for="<%=IDPrefix%>PrimaryRelationship">Relationship</label>
			<input type="text" id="<%=IDPrefix%>PrimaryRelationship" />
		</div>
        <asp:PlaceHolder runat="server" ID="plhPrimaryRefAddress"></asp:PlaceHolder>
        <%= HeaderUtils.RenderPageTitle(0, "Secondary Reference", True)%>
		<div data-role="fieldcontain">
			<label for="<%=IDPrefix%>SecondaryFirstName">First Name</label>
			<input type="text" id="<%=IDPrefix%>SecondaryFirstName" />
		</div>
		<div data-role="fieldcontain">
			<label for="<%=IDPrefix%>SecondaryLastName">Last Name</label>
			<input type="text" id="<%=IDPrefix%>SecondaryLastName" />
		</div>
		<div data-role="fieldcontain">
			<label for="<%=IDPrefix%>SecondaryEmail">Email</label>
			<input type="email" id="<%=IDPrefix%>SecondaryEmail" maxlength="50" />
		</div>
		<div data-role="fieldcontain">
			<div>
				<div style="display: inline-block;"><label for="<%=IDPrefix%>SecondaryPhone" style="white-space: nowrap;">Phone</label></div>
				<div style="display: inline-block;"><label class="phone_format rename-able" style="white-space: nowrap; font-style: italic;">(xxx) xxx-xxxx</label></div>
			</div>
			<input type="<%=TextAndroidTel%>" pattern="[0-9]*" name="phone" id="<%=IDPrefix%>SecondaryPhone" class="inphone" maxlength='14' />
		</div>
		<div data-role="fieldcontain">
			<label for="<%=IDPrefix%>SecondaryRelationship">Relationship</label>
			<input type="text" id="<%=IDPrefix%>SecondaryRelationship" />
		</div>
        <asp:PlaceHolder runat="server" ID="plhSecondaryRefAddress"></asp:PlaceHolder>
	</div>
</div>
<script>
	function <%=IDPrefix%>ValidateReferenceInfo() {
		if ($("#<%=IDPrefix%>divReferenceInformation.showfield-section").length > 0 && $("#<%=IDPrefix%>divReferenceInformation.showfield-section").hasClass("hidden")) return true;
		if ($("#<%=IDPrefix%>divReferenceInfoPanel").is(":visible")) {
			return $.lpqValidate("<%=IDPrefix%>ValidateReferenceInfo") && <%=IDPrefix%>PrimaryValidateSAAddress() && <%=IDPrefix%>SecondaryValidateSAAddress();
		}

		return true;
	}

    function <%=IDPrefix%>registerReferenceInfoValidator() {
		$('#<%=IDPrefix%>PrimaryEmail').observer({
    		validators: [
				function (partial) {
					if ($("#<%=IDPrefix%>showReferenceInfo").attr('status') != "Y") {
                    		return ""; //skip validate reference info
                    	}
                    	var email = $(this).val();
                    	if (email != "") {
                    		if (Common.ValidateEmail(email) == false) {
                    			return "Valid Email is required";
                    		}
                    	}
                    	return "";
                    }
                ],
            	validateOnBlur: true,
            	group: "<%=IDPrefix%>ValidateReferenceInfo"
            });

    	//validate phone
			$('#<%=IDPrefix%>PrimaryPhone').observer({
    		validators: [
				function (partial) {
					if ($("#<%=IDPrefix%>showReferenceInfo").attr('status') != "Y") {
                    		return ""; //skip validate reference info
                    	}
                    	var refPhone = $(this).val();
                    	if (refPhone != "") {
                    		if (!is_foreign_phone) {
                    			if (Common.ValidatePhone(refPhone)) {
                    				//validate first digit of home phone : 0 and 1 is invalid
                    				var firstDigit = refPhone.replace("(", "").substring(0, 1);
                    				if (firstDigit == 0 || firstDigit == 1) {
                    					return 'The area code can not begin with 0 or 1.';
                    				}
                    			} else {
                    				return 'Valid phone number is required';
                    			}
                    		}
                    	}
                    	return "";
                    }
                ],
            	validateOnBlur: true,
            	group: "<%=IDPrefix%>ValidateReferenceInfo"
        });
        //validate street address
		<%--	$('#<%=IDPrefix%>PrimarytxtAddress').observer({
                validators: [
                    function (partial) {
                        if ($("#<%=IDPrefix%>showReferenceInfo").attr('status') != "Y") {
                            return ""; //skip validate reference info
                        }                     
                        if ($(this).val() == "" && ($('#<%=IDPrefix%>PrimarytxtZip').val() != "" || $('#<%=IDPrefix%>PrimarytxtCity').val() != ""|| $('#<%=IDPrefix%>PrimaryddlState').val()!="")) {
                            return "Address is required";
                        }          
                    	return "";
                    }
                ],
            	validateOnBlur: true,
            	group: "<%=IDPrefix%>ValidateReferenceInfo"
        });
         //validate zip
			$('#<%=IDPrefix%>PrimarytxtZip').observer({
                validators: [
                    function (partial) {
                        if ($("#<%=IDPrefix%>showReferenceInfo").attr('status') != "Y") {
                            return ""; //skip validate reference info
                        }                      
                        if ($(this).val() == "" && ($('#<%=IDPrefix%>PrimarytxtAddress').val() != "")) {
                            return "Zip is required";
                        } else if ($(this).val() != "") {        
                            if (<%=IDPrefix%>Primaryg_is_usa_address && Common.ValidateZipCode($(this).val(), this) == false) {
                                return 'Valid zip is required.';
                            }
                        }          
                    	return "";
                    }
                ],
            	validateOnBlur: true,
            	group: "<%=IDPrefix%>ValidateReferenceInfo"
        });
           //validate city
			$('#<%=IDPrefix%>PrimarytxtCity').observer({
                validators: [
                    function (partial) {
                        if ($("#<%=IDPrefix%>showReferenceInfo").attr('status') != "Y") {
                            return ""; //skip validate reference info
                        }
                        if ($(this).val() == "" && ($('#<%=IDPrefix%>PrimarytxtAddress').val() != "")) {
                            return "City is required";
                        }          
                    	return "";
                    }
                ],
            	validateOnBlur: true,
            	group: "<%=IDPrefix%>ValidateReferenceInfo"
        });
           //validate state
			$('#<%=IDPrefix%>PrimaryddlState').observer({
                validators: [
                    function (partial) {
                        if ($("#<%=IDPrefix%>showReferenceInfo").attr('status') != "Y") {
                            return ""; //skip validate reference info
                        }
                        if ($(this).val() == "" && ($('#<%=IDPrefix%>PrimarytxtAddress').val() != "")) {
                            return "State is required";
                        }          
                    	return "";
                    }
                ],
            	validateOnBlur: true,
            	group: "<%=IDPrefix%>ValidateReferenceInfo"
         });--%>
        	$('#<%=IDPrefix%>SecondaryEmail').observer({
    		validators: [
				function (partial) {
					if ($("#<%=IDPrefix%>showReferenceInfo").attr('status') != "Y") {
                    		return ""; //skip validate reference info
                    	}
                    	var email = $(this).val();
                    	if (email != "") {
                    		if (Common.ValidateEmail(email) == false) {
                    			return "Valid Email is required";
                    		}
                    	}
                    	return "";
                    }
                ],
            	validateOnBlur: true,
            	group: "<%=IDPrefix%>ValidateReferenceInfo"
            });

    	//validate phone
			$('#<%=IDPrefix%>SecondaryPhone').observer({
    		validators: [
				function (partial) {
					if ($("#<%=IDPrefix%>showReferenceInfo").attr('status') != "Y") {
                    		return ""; //skip validate reference info
                    	}
                    	var refPhone = $(this).val();
                    	if (refPhone != "") {
                    		if (!is_foreign_phone) {
                    			if (Common.ValidatePhone(refPhone)) {
                    				//validate first digit of home phone : 0 and 1 is invalid
                    				var firstDigit = refPhone.replace("(", "").substring(0, 1);
                    				if (firstDigit == 0 || firstDigit == 1) {
                    					return 'The area code can not begin with 0 or 1.';
                    				}
                    			} else {
                    				return 'Valid phone number is required';
                    			}
                    		}
                    	}
                    	return "";
                    }
                ],
            	validateOnBlur: true,
            	group: "<%=IDPrefix%>ValidateReferenceInfo"
            });
		}
		<%=IDPrefix%>registerReferenceInfoValidator();

    function  <%=IDPrefix%>setReferenceInfo(appInfo) {
        if ($('#<%=IDPrefix%>showReferenceInfo').attr('status') == 'Y') {
            appInfo.<%=IDPrefix%>HasReference = "Y";
            appInfo.<%=IDPrefix%>PriRefFirstName = $('#<%=IDPrefix%>PrimaryFirstName').val();
            appInfo.<%=IDPrefix%>PriRefLastName = $('#<%=IDPrefix%>PrimaryLastName').val();
            appInfo.<%=IDPrefix%>PriRefEmail = $('#<%=IDPrefix%>PrimaryEmail').val();
            appInfo.<%=IDPrefix%>PriRefPhone = $('#<%=IDPrefix%>PrimaryPhone').val();
            appInfo.<%=IDPrefix%>PriRefRelationship = $('#<%=IDPrefix%>PrimaryRelationship').val();
            appInfo.<%=IDPrefix%>PriRefStreet = $('#<%=IDPrefix%>PrimarytxtAddress').val();
            appInfo.<%=IDPrefix%>PriRefZip = $('#<%=IDPrefix%>PrimarytxtZip').val();
            appInfo.<%=IDPrefix%>PriRefCity = $('#<%=IDPrefix%>PrimarytxtCity').val();
            appInfo.<%=IDPrefix%>PriRefState = $('#<%=IDPrefix%>PrimaryddlState').val();
            appInfo.<%=IDPrefix%>SecRefFirstName = $('#<%=IDPrefix%>SecondaryFirstName').val();
            appInfo.<%=IDPrefix%>SecRefLastName = $('#<%=IDPrefix%>SecondaryLastName').val();
            appInfo.<%=IDPrefix%>SecRefEmail = $('#<%=IDPrefix%>SecondaryEmail').val();
            appInfo.<%=IDPrefix%>SecRefPhone = $('#<%=IDPrefix%>SecondaryPhone').val();
            appInfo.<%=IDPrefix%>SecRefRelationship = $('#<%=IDPrefix%>SecondaryRelationship').val();
            appInfo.<%=IDPrefix%>SecRefStreet = $('#<%=IDPrefix%>SecondarytxtAddress').val();
            appInfo.<%=IDPrefix%>SecRefZip = $('#<%=IDPrefix%>SecondarytxtZip').val();
            appInfo.<%=IDPrefix%>SecRefCity = $('#<%=IDPrefix%>SecondarytxtCity').val();
            appInfo.<%=IDPrefix%>SecRefState = $('#<%=IDPrefix%>SecondaryddlState').val();
        }
	}
	function <%=IDPrefix%>ViewReferenceInfo() {
		var strHtml = "";
        if ($('#<%=IDPrefix%>showReferenceInfo').attr('status') == 'Y') {
            var primaryFirstName = $('#<%=IDPrefix%>PrimaryFirstName').val();
            var primaryLastName = $('#<%=IDPrefix%>PrimaryLastName').val();
            var primaryEmail = $('#<%=IDPrefix%>PrimaryEmail').val();
            var primaryPhone = $('#<%=IDPrefix%>PrimaryPhone').val();
            var primaryRelationship = $('#<%=IDPrefix%>PrimaryRelationship').val();
            var primaryStreet = $('#<%=IDPrefix%>PrimarytxtAddress').val();
            var primaryZip = $('#<%=IDPrefix%>PrimarytxtZip').val();
            var primaryCity = $('#<%=IDPrefix%>PrimarytxtCity').val();
            var primaryState = $('#<%=IDPrefix%>PrimaryddlState').val();
            var secondaryFirstName = $('#<%=IDPrefix%>SecondaryFirstName').val();
            var secondaryLastName = $('#<%=IDPrefix%>SecondaryLastName').val();
            var secondaryEmail = $('#<%=IDPrefix%>SecondaryEmail').val();
            var secondaryPhone = $('#<%=IDPrefix%>SecondaryPhone').val();
            var secondaryRelationship = $('#<%=IDPrefix%>SecondaryRelationship').val();
            var secondaryStreet = $('#<%=IDPrefix%>SecondarytxtAddress').val();
            var secondaryZip = $('#<%=IDPrefix%>SecondarytxtZip').val();
            var secondaryCity = $('#<%=IDPrefix%>SecondarytxtCity').val();
            var secondaryState = $('#<%=IDPrefix%>SecondaryddlState').val();          
            if (primaryFirstName!=""||primaryLastName != ""||primaryEmail != ""||primaryPhone != ""||primaryRelationship != ""||primaryStreet != "") {
                strHtml += '<div class="row"><div class="row-title section-heading"><span class="bold">Primary Reference</span></div></div><div class="row panel">';
                if (primaryFirstName != "") {
                    strHtml += '<div class="col-sm-6 col-xs-12"><div class="row"><div class="col-xs-6 text-right row-title"><span class="bold">First Name</span></div><div class="col-xs-6 text-left row-data"><span>' + primaryFirstName + '</span></div></div></div>';
                }
                if (primaryLastName != "") {
                    strHtml += '<div class="col-sm-6 col-xs-12"><div class="row"><div class="col-xs-6 text-right row-title"><span class="bold">Last Name</span></div><div class="col-xs-6 text-left row-data"><span>' + primaryLastName + '</span></div></div></div>';
                }
                if (primaryEmail != "") {
                    strHtml += '<div class="col-sm-6 col-xs-12"><div class="row"><div class="col-xs-6 text-right row-title"><span class="bold">Email</span></div><div class="col-xs-6 text-left row-data"><span>' + primaryEmail + '</span></div></div></div>';
                }
                if (primaryPhone != "") {
                    strHtml += '<div class="col-sm-6 col-xs-12"><div class="row"><div class="col-xs-6 text-right row-title"><span class="bold">Phone</span></div><div class="col-xs-6 text-left row-data"><span>' + primaryPhone + '</span></div></div></div>';
                }
                if (primaryRelationship != "") {
                    strHtml += '<div class="col-sm-6 col-xs-12"><div class="row"><div class="col-xs-6 text-right row-title"><span class="bold">Relationship</span></div><div class="col-xs-6 text-left row-data"><span>' + primaryRelationship + '</span></div></div></div>';
                }
                var strViewPrimaryAddress = "";
                var strPrimaryAddress = "";
                if (primaryStreet != "") {
                    strPrimaryAddress += primaryStreet;
                    if (primaryCity != "") {
                        strPrimaryAddress += ", " + primaryCity;
                    }
                    if (primaryState != "") {
                        strPrimaryAddress += ", " + primaryState;
                    }
                    if (primaryZip != "") {
                        strPrimaryAddress += " " + primaryZip;
                    }
                    if ($.trim(strPrimaryAddress.replace(",", "")) !== "") {
                        strViewPrimaryAddress += '<div class="col-sm-6 col-xs-12"><div class="row"><div class="col-xs-6 text-right row-title"><span class="bold">Reference Address</span></div><div class="col-xs-6 text-left row-data"><span>' + strPrimaryAddress + '</span></div></div></div>';
                    }
                }
                strHtml += strViewPrimaryAddress + "</div>";
            }
            if (secondaryFirstName!=""||secondaryLastName != ""||secondaryEmail != ""||secondaryPhone != ""||secondaryRelationship != ""||secondaryStreet != "") {
                strHtml += '<div class="row"><div class="row-title section-heading"><span class="bold">Secondary Reference</span></div></div><div class="row panel">';
                if (secondaryFirstName != "") {
        		    strHtml += '<div class="col-sm-6 col-xs-12"><div class="row"><div class="col-xs-6 text-right row-title"><span class="bold">First Name</span></div><div class="col-xs-6 text-left row-data"><span>' + secondaryFirstName + '</span></div></div></div>';
        	    }
        	    if (secondaryLastName != "") {
        		    strHtml += '<div class="col-sm-6 col-xs-12"><div class="row"><div class="col-xs-6 text-right row-title"><span class="bold">Last Name</span></div><div class="col-xs-6 text-left row-data"><span>' + secondaryLastName + '</span></div></div></div>';
        	    }
        	    if (secondaryEmail != "") {
        		    strHtml += '<div class="col-sm-6 col-xs-12"><div class="row"><div class="col-xs-6 text-right row-title"><span class="bold">Email</span></div><div class="col-xs-6 text-left row-data"><span>' + secondaryEmail + '</span></div></div></div>';
        	    }
        	    if (secondaryPhone != "") {
        		    strHtml += '<div class="col-sm-6 col-xs-12"><div class="row"><div class="col-xs-6 text-right row-title"><span class="bold">Phone</span></div><div class="col-xs-6 text-left row-data"><span>' + secondaryPhone + '</span></div></div></div>';
        	    }
        	    if (secondaryRelationship != "") {
        		    strHtml += '<div class="col-sm-6 col-xs-12"><div class="row"><div class="col-xs-6 text-right row-title"><span class="bold">Relationship</span></div><div class="col-xs-6 text-left row-data"><span>' + secondaryRelationship + '</span></div></div></div>';
                }       
                var strViewSecondaryAddress = "";
                var strSecondaryAddress = "";
                if (secondaryStreet != "") {
                    strSecondaryAddress += secondaryStreet;
                    if (secondaryCity != "") {
                        strSecondaryAddress += ", " + secondaryCity;
                    }
                    if (secondaryState != "") {
                        strSecondaryAddress += ", " + secondaryState;
                    }
                    if (secondaryZip != "") {
                        strSecondaryAddress += " " + secondaryZip;
                    }
                    if ($.trim(strSecondaryAddress.replace(",", "")) !== "") {
                        strViewSecondaryAddress += '<div class="col-sm-6 col-xs-12"><div class="row"><div class="col-xs-6 text-right row-title"><span class="bold">Reference Address</span></div><div class="col-xs-6 text-left row-data"><span>' + strSecondaryAddress  + '</span></div></div></div>';
                    }
                }
                 strHtml += strViewSecondaryAddress+"</div>";
            }       
        }
		return strHtml;
	}
    $(function () {
        //hide mailing address field in reference user control
        $('#<%=IDPrefix%>PrimarydivMailingAddress').hide();
        $('#<%=IDPrefix%>SecondarydivMailingAddress').hide();
        //reference address is optional, so remove the required labels
        $('.require-span', $('#<%=IDPrefix%>divReferenceInfoPanel')).remove();
		$('#<%=IDPrefix%>showReferenceInfo').on('click', function (e) {
			var $self = $(this);
			var selectedValue = $self.attr('status');
			var RefElem = $('#<%=IDPrefix%>divReferenceInfoPanel');
			if (selectedValue == "N") {
				$self.html('Remove reference');
				$self.addClass("minus-circle-before").removeClass("plus-circle-before");
				$self.attr('status', 'Y');
				RefElem.show();
				<%=IDPrefix%>PrimarytoggleAddressRequired(true);
			} else {
				$self.html("Add a reference");
				$self.addClass("plus-circle-before").removeClass("minus-circle-before");
				$self.attr('status', 'N');
				RefElem.hide();
				<%=IDPrefix%>PrimarytoggleAddressRequired(false);
			}
			if (BUTTONLABELLIST != null) {
				var value = BUTTONLABELLIST[$.trim($self.html()).toLowerCase()];
				if (typeof value == "string" && $.trim(value) !== "") {
					$self.html(value);
				}
			}
		    e.preventDefault();

        });
        //for loan reference, foreign phone format is not available in branch 
        $(window).on('load', function () {
          $('.phone_format', "#<%=IDPrefix%>divReferenceInformation").parent().removeClass("hidden");
        });                         
    });

</script>
