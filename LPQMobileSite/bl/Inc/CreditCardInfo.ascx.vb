﻿Imports DownloadedSettings
Imports Newtonsoft.Json
Imports LPQMobile.BO
Imports LPQMobile.Utils
Imports System.IO
Imports System.Xml
Imports log4net
Imports System.Web.Script.Serialization
Partial Class bl_Inc_CreditCardInfo
    Inherits CBaseUserControl
    Public Property _CreditCardTypeDropdown As String
    Public Property _CCLoanPurposeDisabled As String
    Public Property _LoanPurposeDic As String = ""
    Public Property _LoanPurposeCategory As String = ""
    Public Property _CreditCardList As String = ""
    Public Property _CreditCardNames As New List(Of Tuple(Of String, String, String, String))
    Public Property _LoanPurposeList As New Dictionary(Of String, String)
    Public Property _branchOptionsStr As String = ""
    Public Property LaserDLScanEnabled As Boolean = False
    Public Property LegacyDLScanEnabled As Boolean = False
    Public Property _requiredBranch As String = ""
    Public Property EnableBranchSection As Boolean = False
    Public Property _LogoUrl As String = ""
    Public Property _HiddenCCType As String
    Public Property _textAndroidTel As String = "text"
    Public Property _HeaderDataTheme As String
    Public Property _ContentDataTheme As String
    Public Property _FooterDataTheme As String
    Public Property _CurrentLenderRef As String
    Public Property LoanSavingItem As SmLoan
    Public Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
		If Not IsPostBack Then

			xaApplicationQuestionLoanPage.Header = HeaderUtils.RenderPageTitle(0, "Please answer the following question(s)", True)

		End If
	End Sub
End Class
