﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="BLApplicantID.ascx.vb" Inherits="bl_Inc_BLApplicantID" %>
<%@ Import Namespace="LPQMobile.Utils" %>
<%@ Reference Control="~/Inc/MainApp/xaApplicantID.ascx" %>
<div id="<%=IDPrefix%>divApplicantID" <%=IIf(IsInMode("777") AndAlso IsInFeature("visibility"), "class=""showfield-section"" data-show-field-section-id=""" & BuildShowFieldSectionID("divApplicantID") & """ data-section-name='Applicant Identification'", "")%>>
	<div style="margin-top: 10px;" class="hide-able" id="divShowApplicantIDLink">
		<a id="<%=IDPrefix%>showApplicantID" href="#" data-mode="self-handle-event" status="N" style="cursor: pointer; font-weight: bold;" class="header_theme2 shadow-btn plus-circle-before">Add applicant ID</a>
	</div>
	<div id="<%=IDPrefix%>divApplicantIDPanel" style="display: none;">
       <asp:PlaceHolder runat="server" ID="plhApplicantID"></asp:PlaceHolder>
	</div>
</div>


<script>
    $(function () {
            //business loan applicant ID     
            $('#<%=IDPrefix%>showApplicantID').on('click', function (e) {        
                var $self = $(this);
                var selectedValue = $self.attr('status');
                if (selectedValue == "N") {
                    $self.html('Remove applicant ID');
                    $self.addClass("minus-circle-before").removeClass("plus-circle-before");
                    $self.attr('status', 'Y');
                    $("#<%=IDPrefix%>divApplicantIDPanel").show();
                } else {
                    $self.html('Add applicant ID');
                    $self.addClass("plus-circle-before").removeClass("minus-circle-before");
                    $self.attr('status', 'N');
                    $("#<%=IDPrefix%>divApplicantIDPanel").hide();
                }
                if (BUTTONLABELLIST != null) {
                    var value = BUTTONLABELLIST[$.trim($self.html()).toLowerCase()];
                    if (typeof value == "string" && $.trim(value) !== "") {
                        $self.html(value);
                    }
                }
                e.preventDefault();
            });
    });
    function <%=IDPrefix%>ValidateBLApplicantID() {
        var validator = true;
        if ($("#<%=IDPrefix%>showApplicantID").attr('status') == 'Y') {
            //validate primary id
            if (<%=IDPrefix%>PrimaryValidateApplicantIdXA() == false) {
                validator = false;
            }
            <%If EnableSecondaryID Then %>
                //validate secondary id
                if (<%=IDPrefix%>SecondaryValidateApplicantIdXA() == false) {
                    validator = false;
                }
            <%End If %>
        }
        return validator;
    }
    function <%=IDPrefix%>SetBLApplicantID(appInfo) {
        if ($("#<%=IDPrefix%>showApplicantID").attr('status') == 'Y') {
            <%=IDPrefix%>PrimarySetApplicantID(appInfo);
            <%If EnableSecondaryID Then %>
                <%=IDPrefix%>SecondarySetApplicantID(appInfo);
            <%End If %>
        }
    }
    function <%=IDPrefix%>ViewBLApplicantIdentification() {
        if ($("#<%=IDPrefix%>showApplicantID").attr('status') !="Y"){
            return "";
        }
		    var strIdentification = "";
			var primary_ddlIDCardType = $('#<%=IDPrefix%>PrimaryddlIDCardType option:selected').text();
			var primary_selectIDType = $('#<%=IDPrefix%>PrimaryddlIDCardType option:selected').val();
			var primary_txtIDCardNumber = htmlEncode($('#<%=IDPrefix%>PrimarytxtIDCardNumber').val());
			var primary_txtIDDateIssued = $('#<%=IDPrefix%>PrimarytxtIDDateIssued').val();
			var primary_txtIDDateExpire = $('#<%=IDPrefix%>PrimarytxtIDDateExpire').val();
			var primary_ddlIDState = $('#<%=IDPrefix%>PrimaryddlIDState option:selected').val();
			var primary_ddlIDCountry = $('#<%=IDPrefix%>PrimaryddlIDCountry option:selected').val();
			
        if ($.trim(primary_selectIDType) !== "") {
            strIdentification += '<div class="row"><div class="row-title section-heading"><span class="bold">Primary Identification</span></div></div><div class="row panel">';
            if ($.trim(primary_ddlIDCardType) !== ""){
				strIdentification += '<div class="col-sm-6 col-xs-12"><div class="row"><div class="col-xs-6 text-right row-title"><span class="bold">ID Type</span></div><div class="col-xs-6 text-left row-data"><span>' + primary_ddlIDCardType + '</span></div></div></div>';
            }
            if (primary_txtIDCardNumber != '' && $('#<%=IDPrefix%>PrimarytxtIDCardNumber').IsHideField() === false) {
				    strIdentification += '<div class="col-sm-6 col-xs-12"><div class="row"><div class="col-xs-6 text-right row-title"><span class="bold">ID Number</span></div><div class="col-xs-6 text-left row-data"><span>' + primary_txtIDCardNumber + '</span></div></div></div>';
			    }
			    if (primary_selectIDType == 'PASSPORT' || primary_selectIDType == 'FOREIGN_ID' || primary_selectIDType == 'FRGN_DRVRS' || primary_selectIDType == 'BIRTH_CERT' || primary_selectIDType == '101') {
				    if (primary_ddlIDCountry != '') {
					    strIdentification += '<div class="col-sm-6 col-xs-12"><div class="row"><div class="col-xs-6 text-right row-title"><span class="bold">ID Country</span></div><div class="col-xs-6 text-left row-data"><span>' + primary_ddlIDCountry + '</span></div></div></div>';
				    }
			    } else {
				    if (primary_ddlIDState != '') {
					    strIdentification += '<div class="col-sm-6 col-xs-12"><div class="row"><div class="col-xs-6 text-right row-title"><span class="bold">ID State</span></div><div class="col-xs-6 text-left row-data"><span>' + primary_ddlIDState + '</span></div></div></div>';
				    }
			    }
			    if (primary_txtIDDateIssued != '' && $('#<%=IDPrefix%>PrimarytxtIDDateIssued').IsHideField() === false) {
				    strIdentification += '<div class="col-sm-6 col-xs-12"><div class="row"><div class="col-xs-6 text-right row-title"><span class="bold">ID Date Issued</span></div><div class="col-xs-6 text-left row-data"><span>' + primary_txtIDDateIssued + '</span></div></div></div>';
			    }
			    if (primary_txtIDDateExpire != '' && $('#<%=IDPrefix%>PrimarytxtIDDateExpire').IsHideField() === false) {
				    strIdentification += '<div class="col-sm-6 col-xs-12"><div class="row"><div class="col-xs-6 text-right row-title"><span class="bold">ID Expiration Date</span></div><div class="col-xs-6 text-left row-data"><span>' + primary_txtIDDateExpire + '</span></div></div></div>';
                }
            strIdentification+= "</div>";
        }
         <%If EnableSecondaryID Then %>
            //secondary
            var secondary_selectIDType = $('#<%=IDPrefix%>SecondaryddlIDCardType option:selected').val();
            if ($.trim(secondary_selectIDType)  != "") {
                var secondary_ddlIDCardType = $('#<%=IDPrefix%>SecondaryddlIDCardType option:selected').text();
			    var secondary_txtIDCardNumber = htmlEncode($('#<%=IDPrefix%>SecondarytxtIDCardNumber').val());
			    var secondary_txtIDDateIssued = $('#<%=IDPrefix%>SecondarytxtIDDateIssued').val();
			    var secondary_txtIDDateExpire = $('#<%=IDPrefix%>SecondarytxtIDDateExpire').val();
			    var secondary_ddlIDState = $('#<%=IDPrefix%>SecondaryddlIDState option:selected').val();
                var secondary_ddlIDCountry = $('#<%=IDPrefix%>SecondaryddlIDCountry option:selected').val();
                  strIdentification += '<div class="row"><div class="row-title section-heading"><span class="bold">Secondary Identification</span></div></div><div class="row panel">';
                if ($.trim(secondary_ddlIDCardType) !== ""){
				    strIdentification += '<div class="col-sm-6 col-xs-12"><div class="row"><div class="col-xs-6 text-right row-title"><span class="bold">ID Type</span></div><div class="col-xs-6 text-left row-data"><span>' + secondary_ddlIDCardType + '</span></div></div></div>';
                }
                if (secondary_txtIDCardNumber != '' && $('#<%=IDPrefix%>SecondarytxtIDCardNumber').IsHideField() === false) {
				        strIdentification += '<div class="col-sm-6 col-xs-12"><div class="row"><div class="col-xs-6 text-right row-title"><span class="bold">ID Number</span></div><div class="col-xs-6 text-left row-data"><span>' + secondary_txtIDCardNumber + '</span></div></div></div>';
			        }
			        if (secondary_selectIDType == 'PASSPORT' || secondary_selectIDType == 'FOREIGN_ID' || secondary_selectIDType == 'FRGN_DRVRS' || secondary_selectIDType == 'BIRTH_CERT' || secondary_selectIDType == '101') {
				        if (secondary_ddlIDCountry != '') {
					        strIdentification += '<div class="col-sm-6 col-xs-12"><div class="row"><div class="col-xs-6 text-right row-title"><span class="bold">ID Country</span></div><div class="col-xs-6 text-left row-data"><span>' + secondary_ddlIDCountry + '</span></div></div></div>';
				        }
			        } else {
				        if (secondary_ddlIDState != '') {
					        strIdentification += '<div class="col-sm-6 col-xs-12"><div class="row"><div class="col-xs-6 text-right row-title"><span class="bold">ID State</span></div><div class="col-xs-6 text-left row-data"><span>' + secondary_ddlIDState + '</span></div></div></div>';
				        }
			        }
			        if (secondary_txtIDDateIssued != '' && $('#<%=IDPrefix%>SecondarytxtIDDateIssued').IsHideField() === false) {
				        strIdentification += '<div class="col-sm-6 col-xs-12"><div class="row"><div class="col-xs-6 text-right row-title"><span class="bold">ID Date Issued</span></div><div class="col-xs-6 text-left row-data"><span>' + secondary_txtIDDateIssued + '</span></div></div></div>';
			        }
			        if (secondary_txtIDDateExpire != '' && $('#<%=IDPrefix%>SecondarytxtIDDateExpire').IsHideField() === false) {
				        strIdentification += '<div class="col-sm-6 col-xs-12"><div class="row"><div class="col-xs-6 text-right row-title"><span class="bold">ID Expiration Date</span></div><div class="col-xs-6 text-left row-data"><span>' + secondary_txtIDDateExpire + '</span></div></div></div>';
                    }
                strIdentification+= "</div>";
            }
        <%end if%>
        return strIdentification;
    }
</script>