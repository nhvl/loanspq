﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="CreditCardInfo.ascx.vb" Inherits="bl_Inc_CreditCardInfo" %>
<%@ Register Src="~/Inc/MainApp/xaApplicantQuestion.ascx" TagPrefix="uc" TagName="xaApplicantQuestion" %>
<%@ Import Namespace="LPQMobile.Utils" %>
    <input type="hidden" id="hdLoanPurpose" value=""/>
   <input type="hidden" id="hdCcSelectedLoanPurpose" value=""> <%--TODO: need to come from config--%>
    <input type="hidden" id="hdCcSelectedCardType" value=""/>
    <input type="hidden" id="hdCcSelectedCardName" value=""/>
    <input type="hidden" id="hdEnableBranchSection" value="<%=IIf(EnableBranchSection AndAlso Not String.IsNullOrEmpty(_branchOptionsStr), "Y", "N")%>"/> 
    <input type="hidden" id="hdRequiredBranch" value="<%=_requiredBranch%>" />

<script type="text/javascript">
    //prepo, required for dropdown
    <%--
    $(document).ready(function () {
        
        if ('<%=LoanPurpose%>' != "" && '<%=LoanPurpose%>' != null) { // need to make sure the value is not empty or not null 
		        $("#pl1").find("a[data-command='loan-purpose']").each(function() {
		            var $self = $(this);
		            if ($self.text().toLocaleUpperCase() === '<%=LoanPurpose.ToUpper()%>') {
					    $("#hdCcSelectedLoanPurpose").val($self.data("key"));
					    $self.addClass("active");
					}
				});
            }
		    showRequestCreditLimit(); //this is reuqired for SSO senario
		});
    --%>
    var LoanPurposeCategory = '<%=_LoanPurposeCategory%>';
    var CREDITCARDLIST = '<%=_CreditCardList%>';
    var LOANPURPOSELIST = '<%=_LoanPurposeDic%>';
  </script>
 <div data-role="page" id="creditCardInfo">
	     <div data-role="header" class="sr-only">
            <h1>Credit Card Information</h1>
        </div>
        <div data-role="content">
            <%=HeaderUtils.RenderLogoAndSteps(_LogoUrl, 0, "APPLY_CC")%>
            <%=HeaderUtils.RenderPageTitle(13, "Credit Cards", False)%>
                   
			<%If EnableBranchSection AndAlso Not String.IsNullOrEmpty(_branchOptionsStr) Then%>
            
				<div id="divBranchSection" <%=IIf(IsInMode("777") AndAlso IsInFeature("visibility"), "class=""showfield-section"" data-show-field-section-id=""" & BuildShowFieldSectionID("divBranchSection") & """ data-default-state=""off"" data-section-name= 'Branch'", "")%>>          
				 <div data-role="fieldcontain">
                     <%If _requiredBranch <> "N" Then%>
                         <%=HeaderUtils.RenderPageTitle(0, "Select branch location", True, "divTitleBranchName", isRequired:=True)%>
                     <%Else%>
                         <%=HeaderUtils.RenderPageTitle(0, "Select branch location", True, "divTitleBranchName")%> 
                     <%End If%>
                     <div id='divBranchName' class='header-theme-dropdown-btn'>
                        <select id="ddlBranchName" aria-labelledby="divTitleBranchName"><%=_branchOptionsStr%></select> 
                    </div>
                </div> 
			</div> 
			<%End If%>
            <%If _LoanPurposeList.Any() Then%>
				<%=HeaderUtils.RenderPageTitle(0, "Select a purpose", True, isRequired:=True)%>
				<div class="divLoanPurposeList" id="divLoanPurposeList">
					<%For Each purpose In _LoanPurposeList %>

					<%--<a data-role="button" class="btn-header-theme" data-command="loan-purpose" style="text-align: center; padding-left: 0;"><%=StrConv(purpose, VbStrConv.ProperCase) %></a>--%>
					<%--setting in config should use proper case--%>
					<a data-role="button" href="#" class="btn-header-theme" data-command="loan-purpose" data-purpose-text="<%=purpose.Value%>" data-key="<%=purpose.Key %>" style="text-align: center; padding-left: 0;"><%=purpose.Value%></a>
					<%Next %>
				</div>
             <%End if%>
             	
             <!--rendering credit card type -->             
            <div id="divCCType" data-role="fieldcontain" class="<%=_HiddenCCType%>">
                <%=HeaderUtils.RenderPageTitle(0, "Credit Card Type<span class='require-span'>*</span>", True, "divCreditCardType")%>
                <select id="ddlCreditCardType" aria-labelledby="divCreditCardType">
                    <%=_CreditCardTypeDropdown%>
                </select>                 
            </div>
        
             <%If _LoanPurposeList.Any() Then%>
				<div id ="divRequestCreditLimit" style="display: none;">
					<div data-role="fieldcontain">
						<label for="txtRequestCreditLimit">Request Credit Limit</label>
						<input type="<%=_textAndroidTel%>" pattern ="[0-9]*" id ="txtRequestCreditLimit" class ="money" maxlength="12"/>
					</div>
  
					<div data-role="fieldcontain">
						<label for="eCreditCardNumber" class="RequiredIcon">Existing Credit Card Number</label>
						<input type="<%=_textAndroidTel%>" pattern="[0-9]*" id="eCreditCardNumber" maxlength ="19" class="required inCCardNumber"/></div>
				   </div>
            <%End If %>

			<!-- Make sure the CQs are above the CC name images because clicking the image can act as Continue button -->
			<uc:xaApplicantQuestion ID="xaApplicationQuestionLoanPage" LoanType="BL" IDPrefix="loanpage_" CQLocation="LoanPage" runat="server"  />
             
             <div id="divCreditCardName" style="display:none">
                <%If _CreditCardNames.Any() Then%>
                <%=HeaderUtils.RenderPageTitle(0, "Select the type of credit card you are applying for", True)%>
                <div data-panel="cc-options" class="cc-options row">
                    <%For Each card In _CreditCardNames%>
                   
                        <div class="col-sm-6 col-xs-12">
                                <div class="header_theme2" >
                                    <div style="height: 173px;" id='<%=card.Item1.Replace(" ", "_")%>'>
                                        <a href="#" data-transition="slide" onclick="validateCreditCardInfo(this);" style="display: inline-block;" class="svg-btn">
											<%If card.Item3 = "" Then %>
												<%=HeaderUtils.CreditCard %>
												<div class="overlay-card-name" data-card-type="<%=card.Item2 %>" data-card-name="<%=card.Item1%>"  data-card-category="<%=card.Item4%>">
													<%=card.Item1%>
												</div>
											<%Else%>
												<div  style ="width: 270px; height: 170px; margin:0 auto;"><img alt="<%=card.Item1%>" width="270" height="170" src="<%=card.Item3%>"/></div>
											<div class="overlay-card-name" data-card-type="<%=card.Item2 %>" data-card-name="<%=card.Item1%>"  data-card-category="<%=card.Item4%>"></div>
											<%End If %>
                                        </a>
                                        <a href="#divErrorDialog" style="display: none;">no text</a>
                                    </div>

								    <%--TODO:diplay only when there is secured cc, no point for existing member because can't do funding--%> 
                                    <%--
                                    <% If _CreditCardNames.Last().Item1 = card.Item1 And _CurrentLenderRef.ToUpper.Contains("SDFCU") Then%>    
                                    <div style="text-align: right; padding-right: 5px;"><a href="#popUpCreditDiff" data-rel='popup' data-transition='pop' style="color: inherit;">What is the difference?</a></div>
                                    <%End If%>
                                         --%>
                                </div>
                        </div>
                    <%Next%>
				    <%--TODO:make this configurable default to hide, configurable text--%>          
                </div>

                <%End If%>
            </div>
		           
        </div>
        
        <%--If _CreditCardNames.Any() = false Then0--%>
            <div class ="div-continue"  data-role="footer">
                <a href="#"  data-transition="slide" onclick="validateCreditCardInfo(this)" type="button" class="div-continue-button">Continue</a> 
                 <a href="#divErrorDialog" style="display: none;">no text</a>
                 <a href="#pagebl_BusinessInfo" style="display: none;">no text</a>
            </div>
         <%--End If--%>
       
    </div>
<script>
    function validateCreditCardInfo(element) {
        var currElement = $(element);
        if (currElement.attr("contenteditable") == "true") return false;
        var strMessage = '';
        var validator = true;
        //validate credit card type if it visible
        if (!hiddenCCType()) {
            if ($.lpqValidate("ValidateCreditCardType") === false) {
                validator = false;
            }
        }
        if ($.lpqValidate("ValidateBranch") === false) {
            validator = false;
        }
        if ($.lpqValidate("ValidateLoanPurpose") === false) {
            validator = false;
        }
        if ($.lpqValidate("ValidateCreditInfo") === false) {
            validator = false;
        }

        //strMessage += checkValidScreen1();

		// Validate custom questions, which may be moved to the front page
		if ($('#loanpage_divApplicantQuestion').children().length > 0) {
			if ($.lpqValidate("loanpage_ValidateApplicantQuestionsXA") === false) {
				validator = false;
			}
		}


        /*
        var txtLoanPurpose = $('#ddlLoanPurpose option:selected').val();
        //add some functions
        var txtRequestAmount = $('#txtRequestCreditLimit').val();
       
        var eCreditCardNumber = $('#eCreditCardNumber').val();
        var hasProtectionLoan = false;
        var isOnlyCreditCard = $('#hdOnlyCreditCard').val();
        //    if (!Common.ValidateRadioGroup('rdProtectionForYourLoan'))
        //        strMessage += 'Select a Member Protection Plan option<br />';
        //no validate credit card type, it it is hidden
        if (!hasCCList()) {
            if (isOnlyCreditCard == "Y") {
                if (ddlCreditCardName == undefined && txtLoanPurpose == undefined) {
                    if (!Common.ValidateText(ddlCreditCardType))
                        strMessage += 'Credit Card Type is required<br />';
                }
    
            } else {
                if (!Common.ValidateText(ddlCreditCardType))
                    strMessage += 'Credit Card Type is required<br />';
            }
        }//end !hasCCList()...
    
        if ($("#ddlCreditCardName").length > 0 && !Common.ValidateText(ddlCreditCardName))
            strMessage += 'Credit Card Name is required<br />';
        
        var loanPurposeEnabled = true;
        var temp = "";
        temp = $('#CCLoanPurposeDiv').length; 
        if (temp == 0) {
            loanPurposeEnabled = false;
        }
        if (!Common.ValidateText(txtLoanPurpose) && loanPurposeEnabled)
            strMessage += 'Loan Purpose is required<br />';
      
        if (loanPurposeEnabled) {
            var loanPurposeElement =$('#ddlLoanPurpose option:selected');
            var selectedCategory = getSelectedCategory(loanPurposeElement);
            if(selectedCategory!=null){
                if (selectedCategory.toUpperCase().indexOf("INCREASE") > -1) {
                    if (!Common.ValidateText(eCreditCardNumber)) {
                        strMessage += 'Credit card number is required<br />';
                    }
                }
            } else { //category is not exist
                if (!Common.ValidateText(eCreditCardNumber) && (txtLoanPurpose.toUpperCase().indexOf("INCREASE") != -1))
                    strMessage += 'Credit card number is required<br />';
            }
        }*/

        //if (!Common.ValidateDisclosures('disclosures')) {
        //    strMessage += 'All disclosures need to be checked<br />';
        //}
        //validate verify code
        //strMessage += validateVerifyCode();
        //validate required dl scan
        //strMessage += validateRequiredDLScan($('#hdRequiredDLScan').val(),false);
        if (validator) {
            //if there are cc options
            if ($("#creditCardInfo").find("div[data-panel='cc-options']>div").length > 0) {
                if (!currElement.hasClass('div-continue-button')) { //currElement is selected credit card element
                    //then detect which cc options has been clicked and copy its card type and card name to coresponding hidden fields
                    //these data will be used to submit to server on last step
                    $("#hdCcSelectedCardType").val(currElement.find("div[data-card-type]").attr("data-card-type"));
                    $("#hdCcSelectedCardName").val(currElement.find("div[data-card-type]").attr("data-card-name"));
                    currElement.closest("div[data-panel='cc-options']").find("div.header_theme2").removeClass("active");
                    currElement.find(">div.header_theme2").addClass("active");
                }else {
                    //credit card names and ccTypes are hidden
                    if (hiddenCCType() && $('#ddlCreditCardType option:selected').length > 0) {         
                        $("#hdCcSelectedCardType").val($('#ddlCreditCardType option:selected').val());
                    }
                } 
            }
            if (window.ABR) {
            	var formValues = prepareAbrFormValues("creditCardInfo");
            	if (ABR.FACTORY.evaluateRules("creditCardInfo", formValues)) {
            		return false;
            	}
            }
           goToNextPage("#pagebl_BusinessInfo");
        } else {
            Common.ScrollToError();
        }
    }

    function handleHiddenLoanPurpose() {
        var loanPurposeElem = $('.divLoanPurposeList a[data-role="button"]');
        //no loan purpose or combo type --> display credit card type if it exist
        if ($('#hdIsComboMode').val() == "Y" || loanPurposeElem.length == 0) {         
			var creditCardTypeElem = $('#divCreditCardName');
        	var continueBtnElem = $('#creditCardInfo .div-continue');
            if (creditCardTypeElem.find("div[data-panel='cc-options']").length > 0) {
                continueBtnElem.hide();
                creditCardTypeElem.show();
            } else {
                continueBtnElem.show();
                creditCardTypeElem.hide();
            }
        }
        //only one loan purpose --> preselected and hide loan purpose
        if (loanPurposeElem.length == 1) {
            loanPurposeElem.trigger('click');
            loanPurposeElem.parent().hide(); //hide loan purpose
            loanPurposeElem.parent().prev().hide();//hide title: Select a purpose
        }
    }
    function InitCreditCardInfo() {
        $("#creditCardInfo").find("a[data-command='loan-purpose']").off("click").on("click", function () {
            var $self = $(this);
            //deactive others
            $("#creditCardInfo").find("a[data-command='loan-purpose']").removeClass("active");
            $("#creditCardInfo").find("a[data-command='loan-purpose']").removeClass("btnActive_on");
            //set active for current option
            $self.addClass("active");
            $("#hdCcSelectedLoanPurpose").val($self.data("key"));
            handleShowAndHideCreditCardNames($self);
            showRequestCreditLimit();
            $("#divLoanPurposeList").trigger("change");
        });
        /* $("#btnCCHasCoApplicant").off("click").on("click", function () {
             var $self = $(this);
             if ($self.attr("contenteditable") == "true") return false;
             if ($("#hdCcHasCoApplicant").val() === "N") {
                 $("#hdCcHasCoApplicant").val("Y");
             } else {
                 $("#hdCcHasCoApplicant").val("N");
             }
             $self.toggleClass("active");
             handledBtnHeaderTheme($(this));
         });*/

        //enable sumbit buttons
        if ($('#pagesubmit .div-continue-button').hasClass('ui-disabled')) {
            $('#pagesubmit .div-continue-button').removeClass('ui-disabled');
        }
        if ($('#walletQuestions .div-continue-button').hasClass('ui-disabled')) {
            $('#walletQuestions .div-continue-button').removeClass('ui-disabled');
        }
        if ($('#co_walletQuestions .div-continue-button').hasClass('ui-disabled')) {
            $('#co_walletQuestions .div-continue-button').removeClass('ui-disabled');
        }
        showAndHideCreditCardPage();
    }

    function hiddenCCType() {
        return $('#divCCType').hasClass('hidden');
    }
    function showRequestCreditLimit() {
        var selectedLoanPurpose = $("#hdCcSelectedLoanPurpose").val();
        if (selectedLoanPurpose === "") {
            return false;
        }
        var selectedCategory = getSelectedCategory(selectedLoanPurpose);
        var $ccContainer= $('#divRequestCreditLimit');
        var $ccLimit = $('#txtRequestCreditLimit');
        var $ccNumber = $('#eCreditCardNumber');
        var requestCreditLimitElem = $ccLimit.closest('div[data-role="fieldcontain"]');
        var divCCNumberElem = $ccNumber.closest('div[data-role="fieldcontain"]');
        requestCreditLimitElem.show();
        divCCNumberElem.show();
        if (selectedCategory != null) { //exist category
            if (selectedCategory.toUpperCase().indexOf("INCREASE") > -1) {
                $ccLimit.val(Common.FormatCurrency($ccLimit.val(), true));
                $ccContainer.show();
                if (selectedLoanPurpose.toUpperCase().indexOf("LOWER") != -1) { //loanpurpose is lower rate--> only show ccNumber
                    //reset and hide credit limit
                    $ccLimit.val("0");
                    requestCreditLimitElem.hide();
                }
            } else if (selectedCategory.toUpperCase().indexOf("LIMIT") != -1 && selectedCategory.toUpperCase().indexOf("NEW") != -1 ) { //show both card image and request credit limit
                $ccLimit.val(Common.FormatCurrency($ccLimit.val(), true));
                $ccContainer.show();
                //reset and hide card number
                $ccNumber.val("");
                divCCNumberElem.hide();
            } else if (selectedCategory.toUpperCase().indexOf("NEW") != -1) { //category is new card --> only show card image, don't limit
                $ccLimit.val("0");
                $ccNumber.val("");
                $ccContainer.hide(); 
            } else if (selectedCategory.toUpperCase().indexOf("UPGRADE") != -1) {
                $ccContainer.show();
                //reset and hide credit limit
                $ccLimit.val("0");
                requestCreditLimitElem.hide();

            } else {//category could be "" or any string which not contain "INCREASE", should be almost the same as NEW category
                $ccLimit.val("0");
                $ccNumber.val("");
                $ccContainer.hide(); 
            }
        } else { //category is not exist
            if (selectedLoanPurpose.toUpperCase().indexOf("INCREASE") != -1) {
                $ccLimit.val(Common.FormatCurrency($ccLimit.val(), true));
                $ccContainer.show();
            } else if (selectedLoanPurpose.toUpperCase().indexOf("LOWER") != -1) {//loanpurpose is lower rate--> only show ccNumber
                $ccContainer.show();
                //reset and hide credit limit
                $ccLimit.val('0');
                requestCreditLimitElem.hide();
            } else if (selectedLoanPurpose.toUpperCase().indexOf("UPGRADE") != -1) {
                $ccContainer.show();
                //reset and hide credit limit
                $ccLimit.val("0");
                requestCreditLimitElem.hide();

            }else {
                $ccLimit.val("0");
                $ccNumber.val("");
                $ccContainer.hide();
            }
        }
    }   
    function handleShowAndHideCreditCardNames(element) {
        var continueBtnElem = $('#creditCardInfo .div-continue');
        var $divCCName = $('#divCreditCardName');
        var $divCCType = $('#divCCType');
        var showCCType = false;
        if (element.hasClass('active')) {
            if ($divCCName.find("div[data-panel='cc-options']").length > 0) {
                var sLoanPurpose = element.attr('data-key') != undefined ? element.attr('data-key').toUpperCase() : "";
                var sPurposeCategory = getSelectedCategory(sLoanPurpose) != null ? getSelectedCategory(sLoanPurpose).toUpperCase():"";       
                filterCreditCardNames(sLoanPurpose, $divCCName);
                if (sPurposeCategory.indexOf("NEW") > -1 || sLoanPurpose.indexOf("NEW") > -1) {          
                    //check if there are no credit card names with upgrade category then display credit card types dropdown            
                    if (hasFilterCCName("NEW")) {
                        showCCType = false;
                    } else {
                        showCCType = true;
                    }      
                } else if (sPurposeCategory.indexOf("UPGRADE") > -1 || sLoanPurpose.indexOf("UPGRADE") > -1) {
                    //check if there are no credit card names with upgrade category then display credit card types dropdown            
                    if (hasFilterCCName("UPGRADE")) {               
                        showCCType = false;                                   
                    } else {
                        showCCType = true; 
                    }
                } else {     
                    showCCType = true;       
                }        
                if (!showCCType) {
                    $divCCType.addClass('hidden'); 
                    $divCCName.show();
                    continueBtnElem.hide();
                } else {  
                    //ccTypes only has one option so hide the ccTypes dropdowns
                    if ($divCCType.find('select option').length > 0) {
                        if ($divCCType.find('select option').length <= 2) {
                            $divCCType.addClass('hidden');
                        } else {
                            $divCCType.removeClass('hidden');
                        }
                    }
                    $divCCName.hide();
                    continueBtnElem.show(); 
                }
            } else {//no credit card name -->show continue button 
                $divCCName.hide();  
                continueBtnElem.show();
            }
        }
    }
    function hasFilterCCName(sCategory) {
        var hasCCName = false;
        $('#divCreditCardName').find('div[class~="col-sm-6"]').each(function (idx,elem) {
            var cardCategory = $(this).find('div[data-card-name]').attr('data-card-category');
            cardCategory = cardCategory != undefined ? cardCategory.toUpperCase() : "";        
            if (sCategory == "UPGRADE") {
                if ((cardCategory.indexOf(sCategory) > -1 || cardCategory.indexOf("BOTH") > -1) && !$(this).hasClass('hidden')) {
                    hasCCName = true;
                    return false;
                }
            } else {
                if ((cardCategory.indexOf("UPGRADE") == -1) && !$(this).hasClass('hidden')) {
                    hasCCName = true;
                    return false;
                }               
            }
        }); 
        return hasCCName;
    }
    function filterCreditCardNames(sLoanPurpose, $divCCName) {
        var $ccNames = $divCCName.find('div[class~="col-sm-6"]'); 
        var purposeCategory = getSelectedCategory(sLoanPurpose);
        purposeCategory = purposeCategory != null ? purposeCategory.toUpperCase() : "";
        $ccNames.each(function () {
            var $ccName = $(this);
            var cardCategory = $ccName.find('div[data-card-name]').attr('data-card-category');
            cardCategory = cardCategory != undefined ? cardCategory.toUpperCase():""; //card category ="NEW,UPGRADE,BOTH"
            if (purposeCategory.indexOf("NEW") > -1 || sLoanPurpose.indexOf("NEW") > -1 ) {
                if (cardCategory == "UPGRADE") {
                    $ccName.addClass('hidden');
                } else {
                    $ccName.removeClass('hidden');
                }
            } else if (purposeCategory.indexOf("UPGRADE") > -1 || sLoanPurpose.indexOf("UPGRADE")> -1 ) {
                if (cardCategory == "UPGRADE" || cardCategory == "BOTH") {
                    $ccName.removeClass('hidden');
                } else {
                    $ccName.addClass('hidden');
                }
            } else {
                $ccName.removeClass('hidden');
            }
        }); 
    }
    //hide the first page and go to the second page if no credit cards list and there only one or no pupose
    function showAndHideCreditCardPage() {
        var creditCardTypeElem = $('#divCreditCardName');
        var loanPurposeElem = $('.divLoanPurposeList a[data-role="button"]');
        var hideCCPage = false;
    
            if (creditCardTypeElem.find("div[data-panel='cc-options']").length == 0) {
                if (loanPurposeElem.length == 0 || (loanPurposeElem.length == 1 && loanPurposeElem.text().toUpperCase().indexOf("LIMIT")) == -1) {
                    if (loanPurposeElem.length == 1) {
                        loanPurposeElem.trigger('click'); //select loanpurpose
                    }
                    hideCCPage = true;
                }
            }
    }
	function getSelectedCategory(selectedLoanPurpose) {
		if (selectedLoanPurpose == null) return null;
        var loanPurposeCategoryObj = JSON.parse(LoanPurposeCategory);
        for (var key in loanPurposeCategoryObj) {
            if (key.toUpperCase() == selectedLoanPurpose.toUpperCase()) {
                return loanPurposeCategoryObj[key];
            }
        }
        return null;
    }
    function getCreditCardInfo(appInfo, disagree_check) {
        appInfo.PlatformSource = $('#hdPlatformSource').val();
        appInfo.LenderRef = $('#hfLenderRef').val();    
        if (disagree_check) {
            appInfo.isDisagreeSelect = "Y";
        } else {
            appInfo.isDisagreeSelect = "N";
        }
       
        appInfo.BranchID = getBranchId();
        appInfo.LoanOfficerID = $('#hfLoanOfficerID').val();
        appInfo.ReferralSource = $('#hfReferralSource').val();
        appInfo.CreditCardName = $.trim($("#hdCcSelectedCardName").val());
        var ccType = $.trim($("#hdCcSelectedCardType").val());
        if (!hiddenCCType()) {
            ccType = $('#ddlCreditCardType option:selected').val();
        } else if ($("#creditCardInfo").find("div[data-panel='cc-options']>div").length == 0) { // no display credit card name and hidden credit card type
            if ($('#ddlCreditCardType option:selected').length > 0) { //make sure ddlCreditCard exist
                ccType = $('#ddlCreditCardType option:selected').val();
            }
        }
        appInfo.CreditCardType = ccType;
        appInfo.LoanPurpose = $("#hdCcSelectedLoanPurpose").val(); 
        appInfo.RequestAmount = Common.GetFloatFromMoney($('#txtRequestCreditLimit').val());
        appInfo.eCardNumber = $('#eCreditCardNumber').val();
        // Custom Ansers
     //   appInfo.CustomAnswers = GetCustomAnswers();
        return appInfo;
    }
    function ViewCreditCardInfo() {
        var strHtml = "";
        var strPurpose = $("#hdCcSelectedLoanPurpose").val();
        if (strPurpose !== "") {
            strHtml += '<div class="col-sm-6 col-xs-12"><div class="row"><div class="col-xs-6 text-right row-title"><span class="bold">Purpose</span></div><div class="col-xs-6 text-left row-data"><span>' + strPurpose + '</span></div></div></div>';
            var txtRequestCreditLimit = $("#txtRequestCreditLimit").val();
            var eCreditCardNumber = $("#eCreditCardNumber").val();
            if (Common.GetFloatFromMoney(txtRequestCreditLimit) != 0) {
                strHtml += '<div class="col-sm-6 col-xs-12"><div class="row"><div class="col-xs-6 text-right row-title"><span class="bold">Request Credit Limit</span></div><div class="col-xs-6 text-left row-data"><span>' + txtRequestCreditLimit + '</span></div></div></div>';
            }
            if (Common.ValidateText(eCreditCardNumber)) {
                strHtml += '<div class="col-sm-6 col-xs-12"><div class="row"><div class="col-xs-6 text-right row-title"><span class="bold">Card Number</span></div><div class="col-xs-6 text-left row-data"><span>' + eCreditCardNumber + '</span></div></div></div>';
            }
        }
 
        var strCardName = $("#hdCcSelectedCardName").val();
        if (strCardName !== "") {
            strHtml += '<div class="col-sm-6 col-xs-12"><div class="row"><div class="col-xs-6 text-right row-title"><span class="bold">Card Name</span></div><div class="col-xs-6 text-left row-data"><span>' + strCardName + '</span></div></div></div>';
        }
        return strHtml;
    }
    function RenderCreditCardInfoContent() {
        viewSelectedBranch(); //view selected branch
        $(".ViewCreditCardInfo").html(ViewCreditCardInfo.call($(".ViewCreditCardInfo")));     
        $("div.review-container div.row-title b").each(function (idx, ele) {
            if (typeof $(ele).data("renameid") == "undefined") {
                var dataId = getDataId();
                $(ele).attr("data-renameid", dataId);
                RENAME_REPOSITORY[dataId] = htmlEncode($(ele).html());
            }
        });
    }
    handleShowAndHideBranchNames();
    InitCreditCardInfo();
    $(document).on("pageshow", function () {
        var curPageId = $.mobile.pageContainer.pagecontainer('getActivePage').attr('id');
        if (curPageId == "creditCardInfo") {
        	pushToPagePaths("creditCardInfo");
            InitCreditCardInfo();
        }
    });
    $(function () {
        creditCardFormat();
        $("#divLoanPurposeList").observer({
            validators: [
                function (partial) {
                    if ($("#creditCardInfo").find("a[data-command='loan-purpose']").length > 0) {
                        var selectedLoanPurpose = $("#hdCcSelectedLoanPurpose").val().toUpperCase();
                        if (selectedLoanPurpose === "") {
                            return "Please select a purpose";
                        }
                    }
                    return "";
                }
            ],
            validateOnBlur: true,
            group: "ValidateLoanPurpose",
            groupType: "complexUI"
        });
        $("#eCreditCardNumber").observer({
            validators: [
                function (partial) {
                    if ($("#creditCardInfo").find("a[data-command='loan-purpose']").length > 0) {
                        var selectedLoanPurpose = $("#hdCcSelectedLoanPurpose").val().toUpperCase();
                        if (selectedLoanPurpose === "") {
                            $.lpqValidate.showValidation("#divLoanPurposeList", "Please select a purpose");
                            return "";
                        }
                        var selectedCategory = getSelectedCategory($("#hdCcSelectedLoanPurpose").val());
                        if (selectedCategory != null) { //use this loop when attribute category  is in xml
                            if (selectedCategory.toUpperCase().indexOf("INCREASE") > -1 || selectedCategory.toUpperCase().indexOf("LOWER") > -1||selectedCategory.toUpperCase().indexOf("UPGRADE")>-1) {
                                if (!Common.ValidateText($(this).val())) {
                                    return "Credit card number is required";
                                }
                            }
                            //else  category could be "" or any string other string, will not require cc#
                        } else { //attribute category is NOT  in xml
                            if (selectedLoanPurpose.indexOf("INCREASE") != -1 || selectedLoanPurpose.indexOf("LOWER") != -1||selectedLoanPurpose.indexOf("UPGRADE") != -1) {
                                if (!Common.ValidateText($(this).val())) {
                                    return "Credit card number is required";
                                }
                            }

                        }
                    }
                    return "";
                }
            ],
            validateOnBlur: true,
            group: "ValidateCreditInfo"
        });
        $("#ddlCreditCardType").observer({
            validators: [
                function (partial) {
                    if (Common.ValidateText((this).val()) == false) {
                        return "Please select Credit Card Type";
                    }
                    return "";
                }
            ],
            validateOnBlur: true,
            group: "ValidateCreditCardType"
        });       
    });
</script>