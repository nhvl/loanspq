﻿Imports System
Imports System.Security.Permissions
Partial Class bl_Inc_BLApplicantID
    Inherits CBaseUserControl
    Public Property IDCardTypeList As String
    Public Property IsExistIDCardType As Boolean
    Public Property StateDropdown As String = ""
    ''currently don't use secondary id, so disable it
    Public Property EnableSecondaryID As Boolean = False
    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        Dim primaryIDCtrl As Inc_MainApp_xaApplicantID = CType(LoadControl("~/Inc/MainApp/xaApplicantID.ascx"), Inc_MainApp_xaApplicantID)
        primaryIDCtrl.IsBusinessLoan = True
        primaryIDCtrl.Header = HeaderUtils.RenderPageTitle(0, "Primary ID Card", True)
        primaryIDCtrl.IDPrefix = IDPrefix + "Primary"
        primaryIDCtrl.StateDropdown = StateDropdown
        primaryIDCtrl.IDCardTypeList = IDCardTypeList
		primaryIDCtrl.isExistIDCardType = IsExistIDCardType
		primaryIDCtrl.MappedShowFieldLoanType = MappedShowFieldLoanType
        plhApplicantID.Controls.Add(primaryIDCtrl)
        If EnableSecondaryID Then
            Dim secondaryIDCtrl As Inc_MainApp_xaApplicantID = CType(LoadControl("~/Inc/MainApp/xaApplicantID.ascx"), Inc_MainApp_xaApplicantID)
            secondaryIDCtrl.isBusinessLoan = True
            secondaryIDCtrl.Header = HeaderUtils.RenderPageTitle(0, "Secondary ID card", True)
            secondaryIDCtrl.IDPrefix = IDPrefix + "Secondary"
            secondaryIDCtrl.StateDropdown = StateDropdown
            secondaryIDCtrl.IDCardTypeList = IDCardTypeList
			secondaryIDCtrl.isExistIDCardType = IsExistIDCardType
			secondaryIDCtrl.MappedShowFieldLoanType = MappedShowFieldLoanType
            plhApplicantID.Controls.Add(secondaryIDCtrl)
        End If
    End Sub
End Class
