﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="VehicleLoanInfo.ascx.vb" Inherits="bl_Inc_VehicleLoanInfo" %>
<%--<%@ Reference Control="~/Inc/Disclosure.ascx" %>--%>
<%@ Register Src="~/Inc/MainApp/xaApplicantQuestion.ascx" TagPrefix="uc" TagName="xaApplicantQuestion" %>
<%@ Register Src="~/Inc/VinNumberScan.ascx" TagPrefix="uc" TagName="VimNumberScan" %>
<%@ Import Namespace="LPQMobile.Utils" %>
    <input type="hidden" id="hdHasLoanPurpose" value="<%=_HasLoanPurpose  %>"/>
    <input type="hidden" id="hdVlHasCoApplicant" value="N"/>  
    <input type="hidden" id="hdVlIsNewVehicle" value=""/>
    <input type="hidden" id="hdVlKnowVehicleMake" value=""/>
    <input type="hidden" id="hdLoanPurpose" value=""/>
    <input type="hidden" id="hdEnableBranchSection" value="<%=IIf(EnableBranchSection AndAlso Not String.IsNullOrEmpty(_branchOptionsStr), "Y", "N")%>"/>
    <input type="hidden" id="hdAutoTermTextboxEnable" value="<%=_autoTermTextboxEnable%>" />
    <input type="hidden" id="hdRequiredBranch" value="<%=_requiredBranch%>" />

<script>
      //prepo, required for dropdown
	    $(document).ready(function () {
	        VL.FACTORY.assignLoanPurpose('<%=LoanPurpose%>');
	        VL.FACTORY.assignVehicleType('<%=VehicleType%>');
	        VL.FACTORY.assignTerm('<%=Term%>');
	    });
   
     var LoanPurposeCategory = '<%=_LoanPurposeCategory%>';
	 var LOANPURPOSELIST = <%= Newtonsoft.Json.JsonConvert.SerializeObject(_VehicleLoanPurposeList)%>;
</script>
<!--getting started(vl1): vehicle infor +scandoc + disclosure -->
    <div data-role="page" id="vehicleInfo">
        <div data-role="header" class="sr-only">
            <h1>Vehicle Loan Information</h1>
        </div>
        <div data-role="content" style="overflow-x: visible;">
            <%=HeaderUtils.RenderLogoAndSteps(_LogoUrl, 0, "APPLY_CC")%>
            <%=HeaderUtils.RenderPageTitle(10, "Vehicle Loan", False)%>
            <%If EnableBranchSection AndAlso Not String.IsNullOrEmpty(_branchOptionsStr) Then%>
				<div id="divBranchSection" <%=IIf(IsInMode("777") AndAlso IsInFeature("visibility"), "class=""showfield-section"" data-show-field-section-id=""" & BuildShowFieldSectionID("divBranchSection") & """ data-default-state=""off"" data-section-name= 'Branch'", "")%>>
				 <div data-role="fieldcontain">
                     <%If _requiredBranch <> "N" Then%>
                         <%=HeaderUtils.RenderPageTitle(0, "Select branch location", True, "divTitleBranchName", isRequired:=True)%>
                     <%Else%>
                         <%=HeaderUtils.RenderPageTitle(0, "Select branch location", True, "divTitleBranchName")%> 
                     <%End If%>
                     <div id='divBranchName' class='header-theme-dropdown-btn'>
                        <select id="ddlBranchName" aria-labelledby="divTitleBranchName"><%=_branchOptionsStr%></select> 
                    </div>
                </div> 
            </div>
			<%End If %>
            <div id="divLoanPurpose">
				  <br />
				  <%=HeaderUtils.RenderPageTitle(0, "Select a purpose", True, isRequired:=True)%>
               <%--<div data-role="fieldcontain">
                    <label for="ddlLoanPurpose">Purpose<span class="require-span">*</span></label>
                    <select id="ddlLoanPurpose">
                        <%=_VehicleLoanPurposeDropdown%>
                    </select>
                </div>--%>
                <%For Each item As KeyValuePair(Of String, String) In _VehicleLoanPurposeList%>
                <a href="#" data-role="button" class="btn-header-theme" data-command="loan-purpose" data-key="<%=item.Key %>" style="text-align: center; padding-left: 0;"><%=item.Value %></a>
                <%Next %>
            </div>

		   <br />
		   <%=HeaderUtils.RenderPageTitle(0, "Provide loan information", True)%>
			<div data-role="fieldcontain">
                <label for="ddlVehicleType"class="RequiredIcon">Vehicle Type</label>
                <select id="ddlVehicleType">
                    <%=_VehicleTypes%>
                </select>
            </div>
			<div data-role="fieldcontain" id="divNewOrUsedVehicleSection">
                <label class="rename-able RequiredIcon">Is this a new or used vehicle?</label>
                <div class="row" id="divNewOrUsedVehicle">
	                <div class="ui-checkbox" style="margin:0px 1% 0px 0px;padding-left: 0px;padding-right: 0px;float: left;  width:49%;">
						<label for="chkVehicleNew" class="ui-btn ui-corner-all ui-btn-inherit ui-btn-icon-left ui-checkbox-off" style="padding: 10px 14px 10px 40px;">New</label>
		                <input type="checkbox" name="checkbox-enhanced" id="chkVehicleNew" data-enhanced="true"/>
					</div>
	                <div class="ui-checkbox" style="margin:0px;padding-left: 0px;padding-right: 0px;float: left; width:50%; ">
						<label for="chkVehicleUsed" class="ui-btn ui-corner-all ui-btn-inherit ui-btn-icon-left ui-checkbox-off" style="padding: 10px 14px 10px 40px;">Used</label>
		                <input type="checkbox" name="checkbox-enhanced" id="chkVehicleUsed" data-enhanced="true"/>
					</div>
                </div>
            </div>
			
            <div data-role="fieldcontain" id="divKnowMakeAndModelSection">
                <label class="rename-able RequiredIcon">Do you know the Make and Model?</label>
                <div class="row" id="divKnowMakeAndModel" >
	                <div class="ui-checkbox" style="margin:0px 1% 0px 0px;padding-left: 0px;padding-right: 0px;float: left; width:49%;">
						<label for="chkKnowMake" class="ui-btn ui-corner-all ui-btn-inherit ui-btn-icon-left ui-checkbox-off abort-renameable" style="padding: 10px 14px 10px 40px;">Yes</label>
		                <input type="checkbox" name="checkbox-enhanced" id="chkKnowMake" data-enhanced="true"/>
					</div>
	                <div class="ui-checkbox" style="margin:0px;padding-left: 0px;padding-right: 0px;float: left; width:50%;">
						<label for="chkDontKnowMake" class="ui-btn ui-corner-all ui-btn-inherit ui-btn-icon-left ui-checkbox-off abort-renameable" style="padding: 10px 14px 10px 40px;">No</label>
		                <input type="checkbox" name="checkbox-enhanced" id="chkDontKnowMake" data-enhanced="true"/>
					</div>
                </div>
            </div>
			<div id="divVehicleDetail" style="display: none;">
				<%If EnableVinNumber Then%>
				<div id="divVinNumber" <%=IIf(IsInMode("777") AndAlso IsInFeature("visibility"), "class=""showfield-section"" data-show-field-section-id=""" & BuildShowFieldSectionID("divVinNumber") & """ data-default-state=""off"" data-section-name= 'Vin Number'", " ")%>>
					<div data-role="fieldcontain">
						<label for="txtVehicleModel">VIN<span class="loader hidden"></span><a href="#vinscan" data-rel="dialog" data-transition="pop" class="pull-right <%=IIf(VinBarcodeScanEnabled, "", "hidden") %>" style="text-align: right;"><img src="/images/barcode-scan.png" style="width: 25%;" class="img-responsive"/></a></label>
						<input type="text" id="txtVinNumber" maxlength="17" />
					</div>
					<%If VinBarcodeScanEnabled Then%>
					<div class="vin-scan-guide header_theme2">Scan VIN barcode or tap the textbox to type in a VIN</div>
					<%End If%>
					<div class="vin-scan-msg hidden"></div>
				</div>
				<%End If%>
                <div id="divVehicleMake">
                    <div id="divCarMake" data-role="fieldcontain">
                        <label for="ddlCarMake" class="RequiredIcon">Vehicle Make</label>
                        <select id="ddlCarMake">
                            <%=_CarMakeDropdown%>
                        </select>
                    </div>
                    
                    <div id="divMotorCycleMake" data-role="fieldcontain">
                        <label for="ddlMotorCycleMake" class="RequiredIcon">Vehicle Make</label>
                        <select id="ddlMotorCycleMake">
                            <%=_MotorCycleMakeDropdown%>
                        </select>
                    </div>
                     <div id="divMake" data-role="fieldcontain">
                         <label for="txtMake" class="RequiredIcon">Vehicle Make</label>
						<input type="text" id="txtMake" maxlength="50" />
                      </div>
                </div>
                <div data-role="fieldcontain">
                    <label for="txtVehicleModel" class="RequiredIcon">Vehicle Model</label>
                    <input type="text" id="txtVehicleModel" maxlength="50" />
                </div>
                <div data-role="fieldcontain">
                    <label for="txtVehicleYear" class="RequiredIcon">Vehicle Year</label>
                    <input type="tel" id="txtVehicleYear" class="numeric" maxlength="4" />
                </div>
            </div>
			<div id="divVehicleMileage" style="display: none;">
                <div data-role="fieldcontain">
                    <label for="txtVehicleMileage">Vehicle Mileage</label>
                    <input type="tel" id="txtVehicleMileage" class="numeric" maxlength="7" />
                </div>
            </div>
            
            <div data-role="fieldcontain">
                <label for="txtEstimatedVehicleValue" class="abort-renameable">
                    <div id="divPurchaseAmountLabel" runat="server">
                    Estimated Purchase Price/Vehicle Value<span class="require-span">*</span></div>
                </label>
                <input type="<%=_textAndroidTel%>" pattern ="[0-9]*" id="txtEstimatedVehicleValue" class="money" maxlength="12" />
            </div>
            <div id="divDownPayment">
            <div data-role="fieldcontain">
                <label for="txtDownPayment" class="RequiredIcon">Down Payment Amount</label>
                <input type="<%=_textAndroidTel%>" pattern ="[0-9]*" id="txtDownPayment" class="money" maxlength="12" />
            </div>
            </div>
            <div data-role="fieldcontain">
                <label for="txtProposedLoanAmount" class="RequiredIcon">Requested Loan Amount</label>
                <input type="<%=_textAndroidTel%>" pattern ="[0-9]*" id="txtProposedLoanAmount" readonly="readonly" class="money" maxlength="12" />
            </div>
            <div data-role="fieldcontain" >
                <label for="ddlLoanTerm" class="RequiredIcon">Loan Term (months)</label>
                 <select id="ddlLoanTerm">
                    <%=_LoanTermDropdown%>
                    <%If String.IsNullOrEmpty(_LoanTermDropdown) Then%>
                        <option value="0">--Please Select--</option>
                        <option value="12">12</option>  
                        <option value="24">24</option>    
                        <option value="36">36</option>    
                        <option value="48">48</option>    
                        <option value="60">60</option>    
                        <option value="72">72</option>  
                    <%End If%>    
                </select>
            </div>
            <div data-role="fieldcontain" style="display:none">
                 <label for="txtLoanTerm" class="RequiredIcon">Loan Term (months)</label>
                <input type="tel" id="txtLoanTerm" class ="numeric" maxlength="3"/>
            </div>
			<%If showEstimatePayment Or (IsInMode("777") AndAlso IsInFeature("visibility")) Then%>
			<div id="divEstimatePayment" <%=IIf(IsInMode("777") AndAlso IsInFeature("visibility"), "class=""showfield-section"" data-show-field-section-id=""" & BuildShowFieldSectionID("divEstimatePayment") & """ data-default-state=""off"" data-section-name=""Estimated Payment""", "")%>>
				<div>
					<div>
						<span>Estimated payment: </span>						
					</div>
					<div class="est-payment-value_container">
						<span class="est-payment-value">$0.00</span>/mo
						<span class="calculator-btn btn-header-theme" onclick='openPopup("#popEstimatePaymentCalculator")' data-rel='popup' data-transition='pop'><em class="fa fa-calculator" aria-hidden="true"></em></span>
					</div>					
				</div>
				<div>
					<span class="rename-able">Estimated payment based on</span><span id="spInterestRate" data-value="<%=_InterestRate %>"> % </span><span class="rename-able">interest rate. Actual monthly payment may vary.</span>
				</div>
				<div id="popEstimatePaymentCalculator" data-role="popup" data-position-to="window">
					<div data-role="content">
						<div class="row">
							<div class="col-sm-12 header_theme2">
								<a href="#" data-rel="back" class="pull-right svg-btn"><%=HeaderUtils.IconClose %><span style="display: none;">close</span></a>
							</div>
						</div>
						<div class="row">
							<div class="col-sm-12">
								<div data-role="fieldcontain">
									<label>Loan Amount</label>    
									<input type="text" data-field="LoanAmount" pattern="[0-9]*" class="money" maxlength ="12"/>
								</div>   
							</div>    
						</div>
						<div class="row">
							<div class="col-sm-12">
								<div data-role="fieldcontain">
									<label>Loan Term (months)</label>
									 <select data-field="LoanTerm">
										<%=_LoanTermDropdown%>
										<%If String.IsNullOrEmpty(_LoanTermDropdown) Then%>
											<option value="12">12</option>  
											<option value="24">24</option>    
											<option value="36">36</option>    
											<option value="48">48</option>    
											<option value="60">60</option>    
											<option value="72">72</option>  
										<%End If%>    
									</select>
								</div>
								<div data-role="fieldcontain" style="display:none">
									 <label>Loan Term (months)</label>
									<input data-field="LoanTerm" type="text" pattern="[0-9]*" class="numeric" maxlength="3"/>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-sm-12">
								<div data-role="fieldcontain">
									<label>Interest Rate</label>    
									<input type="text" data-field="InterestRate" pattern="[0-9]*" class="numeric" maxlength ="4" />
								</div>   
							</div>    
						</div>
						<hr/>
						<div class="row">
							<div class="col-sm-12">
								<div data-role="fieldcontain" style="text-align: center;">
									<div>Estimated Payment</div>
									<div style="font-weight: bold; font-size: 1.4em; margin-left: 6px;" data-field="EstimatePayment">$0.00/mo</div>
								</div>   
							</div>    
						</div>

						<br />
						<%--<div class="div-continue">
							<a href="#" type="button" data-role="button" class="div-continue-button">Apply</a>
						</div>--%>
					</div>
				</div>
			</div>
			<%End If%>
			<div data-role='fieldcontain'>
				 <div style="margin-top: 10px;" id="divTradeInLnk">
					<a href="#" id="lnkHasTradeIn" onclick="VL.FACTORY.ShowHideTradeInPanel(event)" data-mode="self-handle-event" data-status="N" class="header_theme2 shadow-btn plus-circle-before" style="cursor: pointer; font-weight: bold;">Add a trade-in</a>
				</div>
			</div>
			<br />
			<div id="divTradeInPanel" style="display:none">
				<div style="font-size:17px;font-weight :bold">Trade-in Information</div>
				<div data-role="fieldcontain">
					<label for="ddlTradeInVehicleType" class="RequiredIcon">What type of vehicle is this?</label>
					<select id="ddlTradeInVehicleType">
						<%=_VehicleTypes%>
					</select>
				</div>
				<div id="divTradeInVehicleMake">
                    <div id="divTradeInCarMake" data-role="fieldcontain">
                        <label for="ddlTradeInCarMake">Vehicle Make</label>
                        <select id="ddlTradeInCarMake">
                            <%=_CarMakeDropdown%>
                        </select>
                    </div>
                    
                    <div id="divTradeInMotorCycleMake" data-role="fieldcontain">
                        <label for="ddlTradeInMotorCycleMake">Vehicle Make</label>
                        <select id="ddlTradeInMotorCycleMake">
                            <%=_MotorCycleMakeDropdown%>
                        </select>
                    </div>
                     <div id="divTradeInMake" data-role="fieldcontain">
                         <label for="txtTradeInMake">Vehicle Make</label>
						<input type="text" id="txtTradeInMake" maxlength="50" />
                      </div>
                </div>
                <div data-role="fieldcontain">
                    <label for="txtTradeInVehicleModel">Vehicle Model</label>
                    <input type="text" id="txtTradeInVehicleModel" maxlength="50" />
                </div>
                <div data-role="fieldcontain">
                    <label for="txtTradeInVehicleYear">Vehicle Year</label>
                    <input type="tel" id="txtTradeInVehicleYear" class="numeric" maxlength="4" />
                </div>
				<div data-role="fieldcontain">
                    <label for="txtTradeInVehicleCurrentValue">What is the current value of this vehicle?</label>
                    <input type="<%=_textAndroidTel%>" pattern ="[0-9]*" id="txtTradeInVehicleCurrentValue" class="money" maxlength="12" />
                </div>
				<div data-role="fieldcontain">
                    <label for="txtTradeInVehicleOwn">How much do you still owe on this vehicle?</label>
                    <input type="<%=_textAndroidTel%>" pattern ="[0-9]*" id="txtTradeInVehicleOwn" class="money" maxlength="12" />
                </div>
				<div data-role="fieldcontain">
                    <label for="txtTradeInVehiclePayingMonthly">How much are you paying monthly for this vehicle?</label>
                    <input type="<%=_textAndroidTel%>" pattern ="[0-9]*" id="txtTradeInVehiclePayingMonthly" class="money" maxlength="12" />
                </div>
			</div>
			<uc:xaApplicantQuestion ID="xaApplicationQuestionLoanPage" LoanType="BL" IDPrefix="loanpage_" CQLocation="LoanPage" runat="server"  />
            <div id="disclosure_place_holder"></div> 
       
        </div>

  

        <div class ="div-continue"  data-role="footer">
                <a href="#" data-transition="slide" onclick="VL.FACTORY.validateVehicleInfoPage(this)" type="button" class="div-continue-button">Continue</a> 
                 <a href="#divErrorDialog" style="display: none;">no text</a>
                 <a href="#pagebl_BusinessInfo" style="display: none;">no text</a>
        </div>
       
    </div>
      
	<%If VinBarcodeScanEnabled Then%>
    <div data-role="dialog" id="vinscan">
        <div data-role="header"  style="display:none" >
            <h1>Vin Number Scan</h1>
        </div>
        <div data-role="content">
	        <%If LaserDLScanEnabled Then%>
			<div class="js-laser-vin-scan-container">
			<a data-rel="back" href="#" style="position: absolute; top:10px; right: 10px;" class="svg-btn"><%=HeaderUtils.IconClose %><span style="display: none;">close</span></a> 
			<uc:VimNumberScan ID="ucVinNumberScan" IDPrefix="" runat="server" />
			</div>
			<%End If %>
        </div>
    </div>    
    <%End If %>

<script>
    var isSubmitWalletAnswer = false;
    var isCoSubmitWalletAnswer = false;
    var isSubmittingLoan = false;
    var vinBarcodePicker;
    (function (VL, $, undefined) {
        VL.DATA = {};
        VL.FACTORY = {};
        VL.FACTORY.toggleHasCoApplicant = function () {
            var $self = $(this);
            var $dataHolder = $("#" + $self.data("hidden-field"));
            if ($dataHolder.val() === "N") {
                $dataHolder.val("Y");
            } else {
                $dataHolder.val("N");
            }
            $self.toggleClass("active");
            $self.removeClass("btnHover");
        };
        VL.FACTORY.IsTradeInEnabled = function () {
            return $("#lnkHasTradeIn").data("status").toUpperCase() === "Y";
        };
        VL.FACTORY.toggleIsNewVehicle = function () {
            var $self = $(this);
            var $dataHolder = $("#hdVlIsNewVehicle");
            if ($self.is(":checked")) {
                if ($self[0].id === "chkVehicleNew") {
                    $("#chkVehicleUsed").prop("checked", false).checkboxradio().checkboxradio("refresh");
                    $dataHolder.val("Y");
                    //$('#divVehicleMileage').hide(400);
                } else {
                    $("#chkVehicleNew").prop("checked", false).checkboxradio().checkboxradio("refresh");
                    $dataHolder.val("N");
                    //$('#divVehicleMileage').show(400);
                }
            } else {
                $dataHolder.val("");
                //$('#divVehicleMileage').hide(400);
            }
            handleShowHideVehicleMileage();
        };
        VL.FACTORY.toggleKnowVehicleMake = function () {
            var $self = $(this);
            var $dataHolder = $("#hdVlKnowVehicleMake");
            if ($self.is(":checked")) {
                if ($self[0].id === "chkKnowMake") {
                    $("#chkDontKnowMake").prop("checked", false).checkboxradio().checkboxradio("refresh");
                    $dataHolder.val("Y");
                    $('#divVehicleDetail').show(400);
                } else {
                    $("#chkKnowMake").prop("checked", false).checkboxradio().checkboxradio("refresh");
                    $dataHolder.val("N");
                    $('#divVehicleDetail').hide(400);
                    $.lpqValidate.hideValidation("#divVehicleMake");
                    $.lpqValidate.hideValidation("#txtVehicleModel");
                    $.lpqValidate.hideValidation("#txtVehicleYear");
                }
            } else {
                $dataHolder.val("");
                $('#divVehicleDetail').hide(400);
                $.lpqValidate.hideValidation("#divVehicleMake");
                $.lpqValidate.hideValidation("#txtVehicleModel");
                $.lpqValidate.hideValidation("#txtVehicleYear");
            }
            handleShowHideVehicleMileage();
        };

        VL.FACTORY.showAccordingVehicleType = function (prefix) {
            if (typeof prefix === "undefined") prefix = "";
            $('#div' + prefix + 'CarMake').hide();
            $('#div' + prefix + 'MotorCycleMake').hide();
            $('#div' + prefix + 'Make').hide();

            var vehType = $('#ddl' + prefix + 'VehicleType').val();
            switch (vehType) {
                case 'MOTORCYCLE':
                    $('#div' + prefix + 'MotorCycleMake').fadeIn('fast');
                    break;
                case 'CAR':
                    $('#div' + prefix + 'CarMake').fadeIn('fast');
                    break;
                default:
                    $('#div' + prefix + 'Make').fadeIn('fast');
                    break;
            }
            if (prefix === "") {
                handleShowHideVehicleMileage();
            }
        };

        VL.FACTORY.hasLoanPurpose = function () {
            return $('#hdHasLoanPurpose').val().toUpperCase() === "Y";
        };
        VL.FACTORY.IsNewVehicle = function () {
            return $("#hdVlIsNewVehicle").val().toUpperCase() === "Y";
        };
        VL.FACTORY.knowVehicleMake = function () {
            return $("#hdVlKnowVehicleMake").val().toUpperCase() === "Y";
        };
        VL.FACTORY.vehicleType = function (prefix) {
            if (typeof prefix === "undefined") prefix = "";
            var vehicleTypeElem = $("#ddl" + prefix + "VehicleType");
            if (vehicleTypeElem.val() == null) {//there no trigger event 
                vehicleTypeElem.val(document.getElementById("ddl" + prefix + "VehicleType")[0].value);
                if (vehicleTypeElem.find('option').length == 2) { //only one dropdown-->makes it by default
                    vehicleTypeElem.val(document.getElementById("ddl" + prefix + "VehicleType")[1].value);
                }
            }
            return vehicleTypeElem.val();
        };
        VL.FACTORY.vehicleMake = function () {
            var vehicleMake = "";
            if (VL.FACTORY.knowVehicleMake) {
                switch (VL.FACTORY.vehicleType().toUpperCase()) {
                    case "CAR":
                        vehicleMake = $('#ddlCarMake').val();
                        break;
                    case "MOTORCYCLE":
                        vehicleMake = $('#ddlMotorCycleMake').val();
                        break;
                    default:
                        vehicleMake = $.trim($('#txtMake').val());
                }
            }
            return vehicleMake;
        };
        VL.FACTORY.tradeInVehicleMake = function () {
            var vehicleMake = "";
            switch (VL.FACTORY.vehicleType("TradeIn").toUpperCase()) {
                case "CAR":
                    vehicleMake = $('#ddlTradeInCarMake').val();
                    break;
                case "MOTORCYCLE":
                    vehicleMake = $('#ddlTradeInMotorCycleMake').val();
                    break;
                default:
                    vehicleMake = $.trim($('#txtTradeInMake').val());
            }
            return vehicleMake;
        };
        VL.FACTORY.init = function () {
            VL.FACTORY.showAccordingVehicleType();
            VL.FACTORY.showAccordingVehicleType("TradeIn");
            VL.FACTORY.ShowAndHideTxtLoanTerm(VL.FACTORY.vehicleType());
            VL.FACTORY.calculateLoanAmount();
            VL.FACTORY.vl1Init();
            VL.FACTORY.refreshPurposeLabel();
            registerProvideLoanInfoValidator();
            //$('input.money').each(function () {
            //    $(this).val(Common.FormatCurrency($(this).val(), true));
            //});           

            $("#txtVinNumber").on("change keyup", function () {
                var vehType = $('#ddlVehicleType option:selected').val();
                if (vehType != undefined && (vehType.toUpperCase() == "CAR" || vehType.toUpperCase() == "MOTORCYCLE")) {
                    decodeVinNumber($.trim($(this).val()));
                }
            });
            $("#txtVinNumber").on("paste", function (e) {
                var vehType = $('#ddlVehicleType option:selected').val();
                if (vehType != undefined && (vehType.toUpperCase() == "CAR" || vehType.toUpperCase() == "MOTORCYCLE")) {
                    decodeVinNumber($.trim(e.originalEvent.clipboardData.getData('text')));
                }
            });

            $("#txtVinNumber").on("focus", function () {
                $("#divVinNumber .vin-scan-guide").removeClass("hidden");
                $("#divVinNumber .vin-scan-msg").text("");
                $("#divVinNumber .vin-scan-msg").addClass("hidden");
            });
            $("#vinscan").on("pageshow", function () {
                if ($("#scandit-vin-barcode-picker").length > 0 && $("#vinscan").data("no-camera") != true) {
                    ScanditSDK.CameraAccess.getCameras().then(function (cameraList) {
                        if (cameraList.length > 0) {
                            initVinLaserScan();
                        } else {
                            $("#vinscan .no-camera").removeClass("hidden");
                        }
                        return null;
                    }).catch(function (ex) {
                        //console.log(ex);
                        if (ex.name == "NotAllowedError") {
                            //$("#vinscan .js-laser-vin-scan-container").find("a[data-rel='back']").trigger("click");
                            $("#vinscan").data("no-camera", true);
                            $("#vinscan .no-camera").removeClass("hidden");
                        } else {
                            $("#vinscan").data("no-camera", true);
                            $("#vinscan .no-camera").removeClass("hidden");
                        }
                        return null;
                    });
                } else {
                    $("#vinscan .no-camera").removeClass("hidden");
                }

            });
            $("#vinscan").on("pagehide", function () {
                if ($("#scandit-vin-barcode-picker").length > 0 && vinBarcodePicker) {
                    vinBarcodePicker.destroy();
                }
                $("#vinscan .no-camera").addClass("hidden");
            });
        };
        function initVinLaserScan() {
            ScanditSDK.BarcodePicker.create(document.getElementById("scandit-vin-barcode-picker"), {
                playSoundOnScan: true,
                vibrateOnScan: true
            }).then(function (barcodePicker) {
                $("#vinscan").find(".scan-guide-text").removeClass("hidden");
                vinBarcodePicker = barcodePicker;
                // barcodePicker is ready here to be used
                var scanSettings = new ScanditSDK.ScanSettings({
                    enabledSymbologies: ["code39"],
                    codeDuplicateFilter: 1000
                });
                barcodePicker.applyScanSettings(scanSettings);
                $(".scandit-camera-switcher, .scandit-flash-white, .scandit-flash-color", ".scandit-barcode-picker ").addClass("hidden");
                barcodePicker.onScan(function (scanResult) {
                    scanResult.barcodes.reduce(function (string, barcode) {
                        $("#txtVinNumber").val(barcode.data);
                        $("#txtVinNumber").trigger("change");
                        goToNextPage("#vl1");
                    }, "");
                });
                return null;
            });
        }

        VL.FACTORY.pageLastInit = function () {
            if ($("#divXsellSection").length === 1) {
                $("a.btn-header-theme", $("#divXsellSection")).on("click", function () {
                    $.mobile.loading("show", {
                        theme: "a",
                        text: "Loading ... please wait",
                        textonly: true,
                        textVisible: true
                    });
                });
            }
        }
        //be called right after page #vl1 show to inital and rebind event for elements on page #vl1
        VL.FACTORY.vl1Init = function () {
            if (VL.FACTORY.hasLoanPurpose()) {
                $('#divLoanPurpose').show();
            } else {
                //reset current value of Loan Purpose to prevent expected bugs
                $('#divLoanPurpose').val("");
                $('#divLoanPurpose').hide();
            }
            $("#btnVlHasCoApplicant").off("click").on("click", VL.FACTORY.toggleHasCoApplicant);

            $("#chkVehicleNew, #chkVehicleUsed").on("change", VL.FACTORY.toggleIsNewVehicle);
            $("#chkKnowMake, #chkDontKnowMake").on("change", VL.FACTORY.toggleKnowVehicleMake);
            //$("#ddlLoanPurpose").on("change", function () {
            //    VL.FACTORY.refreshPurposeLabel();
            //    VL.FACTORY.calculateLoanAmount();
            //});
            $("#txtEstimatedVehicleValue, #txtDownPayment").on("blur", VL.FACTORY.calculateLoanAmount);
            $("#txtEstimatedVehicleValue, #txtDownPayment").on("keyup paste", function () {
                setTimeout(VL.FACTORY.calculateLoanAmount, 400);
            });
            $("#ddlVehicleType").on("change", function () {
                VL.FACTORY.showAccordingVehicleType();
                VL.FACTORY.ShowAndHideTxtLoanTerm($(this).val());
            });
            $("#ddlTradeInVehicleType").on("change", function () { VL.FACTORY.showAccordingVehicleType("TradeIn"); });
            $('#txtVehicleMileage').blur(function () {
                $(this).val(Common.GetRtNumber($(this).val()));
            });
            $('#txtVehicleYear').blur(function () {
                $(this).val(Common.GetPosNumber($(this).val(), 4));
            });
            VL.FACTORY.bindEventLoanPurposeOption();

            //enable sumbit buttons
            if ($('#pagesubmit .div-continue-button').hasClass('ui-disabled')) {
                $('#pagesubmit .div-continue-button').removeClass('ui-disabled');
            }
            if ($('#walletQuestions .div-continue-button').hasClass('ui-disabled')) {
                $('#walletQuestions .div-continue-button').removeClass('ui-disabled');
            }
            if ($('#co_walletQuestions .div-continue-button').hasClass('ui-disabled')) {
                $('#co_walletQuestions .div-continue-button').removeClass('ui-disabled');
            }
            pushToPagePaths("vehicleInfo");
        };
 
        VL.FACTORY.AutoTermTextboxEnable = function () {
            return $("#hdAutoTermTextboxEnable").val() == "Y";
        }
        VL.FACTORY.ShowAndHideTxtLoanTerm = function (loanType) {
            if (loanType == "CAR" && !VL.FACTORY.AutoTermTextboxEnable()) {
                $('#txtLoanTerm').closest('div[data-role="fieldcontain"]').hide();
                $('#ddlLoanTerm').closest('div[data-role="fieldcontain"]').show();
            } else {
                $('#txtLoanTerm').closest('div[data-role="fieldcontain"]').show();
                $('#ddlLoanTerm').closest('div[data-role="fieldcontain"]').hide();
            }
        }
        VL.FACTORY.refreshPurposeLabel = function () {
            if (VL.FACTORY.hasLoanPurpose()) { //make sure loan purpose is not null
                var loanBalanceTitle = "Estimated Vehicle Value<span class='require-span'>*</span>";
                var purchasePriceTitle = "Estimated Purchase Price<span class='require-span'>*</span>";
                //if ($('#hfLenderRef').val().toUpperCase().indexOf('INSPIRUS') != -1) {
                //    purchasePriceTitle = "Loan Amount<span class='require-span'>*</span>";
                //}
                var selectedLoanPurpose = $("#hdLoanPurpose").val();
                var selectedCategory = getSelectedCategory(selectedLoanPurpose);
                if (selectedCategory.toUpperCase().indexOf("REFI") > -1) { //check category 
                    $('#divPurchaseAmountLabel').html(loanBalanceTitle); 
                    handleREFINANCELoanPurpose(true);
                } else {
                    if (selectedLoanPurpose.toUpperCase().indexOf("REFI") > -1 || selectedLoanPurpose.toUpperCase().indexOf("BUYOUT") > -1) {
                        $('#divPurchaseAmountLabel').html(loanBalanceTitle);
                        if (selectedLoanPurpose.toUpperCase().indexOf('REFI') > -1) {
                            handleREFINANCELoanPurpose(true);
                        } else {
                            handleREFINANCELoanPurpose(false);
                        }
                    } else {
                        $('#divPurchaseAmountLabel').html(purchasePriceTitle);
                        handleREFINANCELoanPurpose(false);
                    }
                }
            } //end hasloanpurpose
            var selectedText = LOANPURPOSELIST[selectedLoanPurpose];
            if ((!g_IsHideGMI && selectedText != undefined && selectedText.toUpperCase().indexOf('HMDA') != -1)
                || (!g_IsHideGMI && selectedLoanPurpose != undefined && selectedLoanPurpose.toUpperCase().indexOf('HMDA') != -1)) {
                $('.div_gmi_section').css('display', 'block');
            } else {
                $('.div_gmi_section').css('display', 'none');
            }
            handleShowHideVehicleMileage();
        };

        function handleREFINANCELoanPurpose(isSelected) {
            if (isSelected === true) {
                //automatically changes: NewVehicle=N and hide this fields
                $("#chkVehicleUsed").prop("checked", true).checkboxradio().checkboxradio("refresh");
                VL.FACTORY.toggleIsNewVehicle.call($("#chkVehicleUsed"));
                $("#divNewOrUsedVehicleSection").hide();
                //automatically changes: MakeModelKnown=Y, and hide this fields
                $("#chkKnowMake").prop("checked", true).checkboxradio().checkboxradio("refresh");
                VL.FACTORY.toggleKnowVehicleMake.call($("#chkKnowMake"));
                $("#divKnowMakeAndModelSection").hide();
                //selected refinance ->hide down payment amount
                $('#divDownPayment').hide();
                $.lpqValidate.hideValidation("#divDownPayment");
                $("#divTradeInPanel").hide(); //hide Trade In link section
                $("#lnkHasTradeIn").html("Add a trade-in").data("status", "N").hide();
                $("#lnkHasTradeIn").addClass("plus-circle-before").removeClass("minus-circle-before");
                $.lpqValidate.hideValidation("#ddlTradeInVehicleType");
                $.lpqValidate.hideValidation("#divTradeInVehicleMake");
                $.lpqValidate.hideValidation("#txtTradeInVehicleModel");
                $.lpqValidate.hideValidation("#txtTradeInVehicleYear");
                $.lpqValidate.hideValidation("#txtTradeInVehicleCurrentValue");
                $.lpqValidate.hideValidation("#txtTradeInVehicleOwn");
                $.lpqValidate.hideValidation("#txtTradeInVehiclePayingMonthly");
                //Requested Loan Amount field verbiage changes to "Amount Refinanced"
                $("label[for='txtProposedLoanAmount']").html("Requested Refinance Amount");
            } else {
                $("#chkVehicleUsed").prop("checked", false).checkboxradio().checkboxradio("refresh");
                $("#chkVehicleNew").prop("checked", false).checkboxradio().checkboxradio("refresh");
                VL.FACTORY.toggleIsNewVehicle.call($("#chkVehicleUsed"));
                $("#divNewOrUsedVehicleSection").show();
                $("#chkKnowMake").prop("checked", false).checkboxradio().checkboxradio("refresh");
                $("#chkDontKnowMake").prop("checked", false).checkboxradio().checkboxradio("refresh");
                VL.FACTORY.toggleKnowVehicleMake.call($("#chkKnowMake"));
                $("#divKnowMakeAndModelSection").show();
                $.lpqValidate.hideValidation("#divNewOrUsedVehicle");
                $.lpqValidate.hideValidation("#divKnowMakeAndModel");
                $('#divDownPayment').show();
                $("#divTradeInPanel").hide();
                $("#lnkHasTradeIn").html("Add a trade-in").data("status", "N").show();
                $("#lnkHasTradeIn").addClass("plus-circle-before").removeClass("minus-circle-before");
                $("label[for='txtProposedLoanAmount']").html("Requested Loan Amount");
            }
            if (BUTTONLABELLIST != null) {
                var value = BUTTONLABELLIST[$.trim($("#lnkHasTradeIn").html()).toLowerCase()];
                if (typeof value == "string" && $.trim(value) !== "") {
                    $("#lnkHasTradeIn").html(value);
                }
            }
            $.lpqValidate.hideValidation("#divVehicleMake");
            $.lpqValidate.hideValidation("#txtVehicleModel");
            $.lpqValidate.hideValidation("#txtVehicleYear");
        }

        function handleShowHideVehicleMileage() {
            //hide Vehicle Mileage if MakeModelKnown=N or VehicleType=BOAT
            var vehicleType = "";
            if ($("#ddlVehicleType").val() != undefined) {
                vehicleType = $("#ddlVehicleType").val().toUpperCase();
            }
            if ($("#hdVlKnowVehicleMake").val() === "N" || vehicleType === "BOAT" || $("#hdVlIsNewVehicle").val() !== "N") {
                $("#divVehicleMileage").hide(400);
            } else {
                $("#divVehicleMileage").show(400);
            }
        }

        VL.FACTORY.calculateLoanAmount = function () {
            var estVehValue = Common.GetFloatFromMoney($('#txtEstimatedVehicleValue').val());
            var downPayment = Common.GetFloatFromMoney($('#txtDownPayment').val());
            var selectedCategory = getSelectedCategory($('#hdLoanPurpose').val());
            var x = estVehValue == '' ? 0 : estVehValue;
            var y = downPayment == '' ? 0 : downPayment;
            //if selected refinance -> no down payment
            if (VL.FACTORY.hasLoanPurpose()) { //make sure the ddlLoanPurpose is existed
                if ($("#hdLoanPurpose").val().toUpperCase().indexOf("REFI") > -1 || selectedCategory.toUpperCase().indexOf("REFI") > -1) {
                    //get loan amount from user input
                    //allow refi loan amount greater than value of car
                    $('#txtProposedLoanAmount').removeAttr("readonly");
                    $('#txtProposedLoanAmount').removeAttr("style");
                } else {//need to calculate loan amount
                    if (x < y || (x == 0 && y == 0)) {
                        $('#txtProposedLoanAmount').val(Common.FormatCurrency(0, true));
                    } else {
                        if (x >= 0 && y >= 0) {
                            $('#txtProposedLoanAmount').val(Common.FormatCurrency(x - y, true));
                        } else {
                            $('#txtProposedLoanAmount').val(Common.FormatCurrency(0, true));
                        }
                        $('#txtProposedLoanAmount').trigger("change");
                    }
                    //restore attribute previous removed by refi purpose
                    if ($('#txtProposedLoanAmount').attr("readonly") == undefined) {
                        $('#txtProposedLoanAmount').attr("readonly", "readonly");
                    }
                }
            } else {//loanpurose is not exist
                if (x >= y) {
                    var result = Common.FormatCurrency(x - y, true);
                    $('#txtProposedLoanAmount').val(result);
                } else {
                    $('#txtProposedLoanAmount').val(Common.FormatCurrency(0, true));
                }
                $('#txtProposedLoanAmount').trigger("change");
            }
        };
        VL.FACTORY.assignLoanPurpose = function (loanPurpose) {
            if ($.trim(loanPurpose) !== "") {
                $("#divLoanPurpose").find("a[data-command='loan-purpose'][data-key='" + loanPurpose.toUpperCase() + "']").trigger("click");
            }
        };
        VL.FACTORY.assignVehicleType = function (vehicleType) {
            if ($.trim(vehicleType) !== "") {
                var $ddlVehicleType = $("#ddlVehicleType");
                //perform this way to prevent assign unbounded value to selectbox
                $ddlVehicleType.find("option").each(function (idx, element) {
                    if ($(element).val().toUpperCase() == vehicleType.toUpperCase()) {
                        $(element).attr("selected", "selected");
                    }
                });
                if ($ddlVehicleType.data("mobile-selectmenu") === undefined) {
                    $('#ddlVehicleType').selectmenu(); //not initialized yet, lets do it
                }
                $ddlVehicleType.selectmenu('refresh');
                $ddlVehicleType.trigger("change");
            }
        }
        VL.FACTORY.assignTerm = function (term) {
            if ($.trim(term) !== "") {
                if ($("#ddlVehicleType").val() == "CAR" && !VL.FACTORY.AutoTermTextboxEnable()) {
                    var $ddlLoanTerm = $("#ddlLoanTerm");
                    //perform this way to prevent assign unbounded value to selectbox
                    $ddlLoanTerm.find("option").each(function (idx, element) {
                        if ($(element).val().toUpperCase() == term.toUpperCase()) {
                            $(element).attr("selected", "selected");
                        }
                    });
                    if ($ddlLoanTerm.data("mobile-selectmenu") === undefined) {
                        $('#ddlLoanTerm').selectmenu(); //not initialized yet, lets do it
                    }
                    $ddlLoanTerm.selectmenu('refresh');
                    $ddlLoanTerm.trigger("change");
                } else {
                    $('#txtLoanTerm').val(term);
                }
            }
        }

        VL.FACTORY.validateVehicleInfoPage = function (element) {
            var currElement = $(element);
            if (currElement.attr("contenteditable") == "true") return false;
            var validator = true;

            var loanInfoValidate = $.lpqValidate("ValidateProvideLoanInfo");
            var branchValidate = $.lpqValidate("ValidateBranch");
            if (loanInfoValidate === false || branchValidate === false) {
                validator = false;
            }

            if ($("#vehicleInfo").find("#divDisclosure").length > 0 && $.lpqValidate("ValidateDisclosure_disclosures") == false) {
                validator = false;
			}

			// Validate custom questions, which may be moved to the front page
			if ($('#loanpage_divApplicantQuestion').children().length > 0) {
				if ($.lpqValidate("loanpage_ValidateApplicantQuestionsXA") === false) {
					validator = false;
				}
			}

            if (validator) {        
            	if (window.ABR) {
		            var formValues = prepareAbrFormValues("vehicleInfo");
            		if (ABR.FACTORY.evaluateRules("vehicleInfo", formValues)) {
            			return false;
            		}
            	}
                currElement.next().next().trigger('click');
            } else {
                Common.ScrollToError();
            }
        };
  
        VL.FACTORY.renderReviewContent = function () {
            viewSelectedBranch(); //view selected branch
            $(".ViewVehicleLoanInfo").html(viewVehicleLoanInfo.call($(".ViewVehicleLoanInfo")));
            $("div.review-container div.row-title b").each(function (idx, ele) {
                if (typeof $(ele).data("renameid") == "undefined") {
                    var dataId = getDataId();
                    $(ele).attr("data-renameid", dataId);
                    RENAME_REPOSITORY[dataId] = htmlEncode($(ele).html());
                }
            });
        };

        VL.FACTORY.ShowHideTradeInPanel = function (e) {
            var trigger = e.target;
            if (trigger.tagName != "A") {
                trigger = $(trigger).parent();
            }
            var selectedValue = $(trigger).data('status');
            if (selectedValue == "N") {
                $(trigger).html('Remove trade-in');
                $("#lnkHasTradeIn").addClass("minus-circle-before").removeClass("plus-circle-before");
                $(trigger).data('status', 'Y');
                $('#divTradeInPanel').show();
            } else {
                $(trigger).html('Add a trade-in');
                $("#lnkHasTradeIn").addClass("plus-circle-before").removeClass("minus-circle-before");
                $(trigger).data('status', 'N');
                $('#divTradeInPanel').hide();
            }
            if (BUTTONLABELLIST != null) {
                var value = BUTTONLABELLIST[$.trim($(trigger).html()).toLowerCase()];
                if (typeof value == "string" && $.trim(value) !== "") {
                    $(trigger).html(value);
                }
            }
            e.preventDefault();

        };
        VL.FACTORY.bindEventLoanPurposeOption = function () {
            $("#divLoanPurpose").find("a[data-command='loan-purpose']").off("click").on("click", function () {
                var $self = $(this);
                //first, remove other active button
                $("#divLoanPurpose").find("a[data-command='loan-purpose']").each(function () {
                    $(this).removeClass("active");
                    handledBtnHeaderTheme($(this));
                });
                //then, activate selected button
                $self.addClass("active");
                $("#hdLoanPurpose").val($self.data("key"));

                VL.FACTORY.refreshPurposeLabel();
                VL.FACTORY.calculateLoanAmount();

                handledBtnHeaderTheme($self);
                $.lpqValidate.hideValidation("#divLoanPurpose");
            });
        };

        function registerProvideLoanInfoValidator() {
            $('#ddlVehicleType').observer({
                validators: [
                    function (partial) {
                        if (Common.ValidateText(VL.FACTORY.vehicleType()) == false) {
                            return "Vehicle Type is required";
                        }
                        return "";
                    }
                ],
                validateOnBlur: true,
                group: "ValidateProvideLoanInfo"
            });
            $('#txtEstimatedVehicleValue').observer({
                validators: [
                    function (partial) {
                        if (Common.ValidateText($(this).val()) == false) {
                            var purchaseAmountLabel = "Estimated Purchase Price";
                            if (VL.FACTORY.hasLoanPurpose()) {
                                purchaseAmountLabel = $('#divPurchaseAmountLabel').text().replace("*", "");
                            }
                            return purchaseAmountLabel + " is required";
                        }
                        return "";
                    }
                ],
                validateOnBlur: true,
                group: "ValidateProvideLoanInfo"
            });
            $('#txtDownPayment').observer({
                validators: [
                    function (partial) {
                        var downPayment = $(this).val();
                        var txtEstimatedVehicleValue = $('#txtEstimatedVehicleValue').val();
                        if (VL.FACTORY.hasLoanPurpose()) {
                            var selectedCategory = getSelectedCategory($('#hdLoanPurpose').val());
                            var loanPurpose = $('#hdLoanPurpose').val().toUpperCase();
                            if (selectedCategory.indexOf("REFI") > -1) { //skip validate downpayment, if category has refinance
                                return "";
                            }
                            if (loanPurpose.indexOf('BUYOUT') > -1) {
                                //down payment >=0
                                if (!Common.ValidateTextAllowZero(downPayment)) {
                                    return 'Down Payment Amount is required';
                                }
                                //make sure down payment <=loan balance
                                if (Common.GetFloatFromMoney(downPayment) > Common.GetFloatFromMoney(txtEstimatedVehicleValue)) {
                                    return "The Down Payment Amount must be less than the Estimated Vehicle Value";
                                }
                            } else {
                                if (loanPurpose.indexOf('REFI') == -1 /*&& selectedCategory.indexOf('REFI') == -1*/) {
                                    //down payment >=0
                                    if (!Common.ValidateTextAllowZero(downPayment)) {
                                        return 'Down Payment Amount is required';
                                    }
                                    //make sure down payment <= purchase price
                                    if (Common.GetFloatFromMoney(downPayment) > Common.GetFloatFromMoney(txtEstimatedVehicleValue)) {
                                        return "The Down Payment Amount must be less than the Estimated Purchase Price";
                                    }
                                }
                            }
                        } else {
                            //down payment >=0
                            if (!Common.ValidateTextAllowZero(downPayment)) {
                                return 'Down Payment Amount is required';
                            }
                            if (Common.GetFloatFromMoney(downPayment) > Common.GetFloatFromMoney(txtEstimatedVehicleValue)) {
                                return "Down Payment Amount must be less than the Estimated Purchase Price";
                            }
                        }
                        return "";
                    }
                ],
                validateOnBlur: true,
                group: "ValidateProvideLoanInfo"
            });
            $('#txtProposedLoanAmount').observer({
                validators: [
                    function (partial) {
                        var txtProposedLoanAmount = $('#txtProposedLoanAmount').val();
                        var loanPurpose = $('#hdLoanPurpose').val().toUpperCase();
                        if (VL.FACTORY.hasLoanPurpose() && loanPurpose.indexOf('REFI') > -1) {
                            if (Common.ValidateTextNonZero(txtProposedLoanAmount) == false) {
                                return "Requested Loan Amount is required";
                            }
                        }
                        return "";
                    }
                ],
                validateOnBlur: true,
                group: "ValidateProvideLoanInfo"
            });
            $('#ddlLoanTerm').observer({
                validators: [
                    function (partial) {

                        if (VL.FACTORY.vehicleType() != "CAR" || VL.FACTORY.AutoTermTextboxEnable()) {
                            return ""; // no validate ddlLoanTerm 
                        }

                        var ddlLoanTerm = $('#ddlLoanTerm').val();
                        if (ddlLoanTerm == '0' || ddlLoanTerm == '') {
                            return 'Loan Term is required';
                        }
                        return "";
                    }
                ],
                validateOnBlur: true,
                group: "ValidateProvideLoanInfo"
            });
            $('#txtLoanTerm').observer({
                validators: [
                    function (partial) {
                        var txtLoanTerm = $('#txtLoanTerm').val();
                        if (VL.FACTORY.vehicleType() == "CAR" && !VL.FACTORY.AutoTermTextboxEnable()) {
                            return ""; //no validate txtLoanTerm
                        }
                        if (txtLoanTerm == '0' || txtLoanTerm == '') {
                            return 'Loan Term is required';
                        }
                        return "";
                    }
                ],
                validateOnBlur: true,
                group: "ValidateProvideLoanInfo"
            });
            $('#divVehicleMake').observer({
                validators: [
                    function (partial) {
                        var knowVehicleMake = VL.FACTORY.knowVehicleMake();
                        if (knowVehicleMake === false) return "";
                        if (!Common.ValidateText(VL.FACTORY.vehicleMake())) {
                            return 'Vehicle Make is required';
                        }
                        return "";
                    }
                ],
                validateOnBlur: true,
                group: "ValidateProvideLoanInfo"
            });
            $('#txtVehicleModel').observer({
                validators: [
                    function (partial) {
                        var txtVehicleModel = $('#txtVehicleModel').val();
                        var knowVehicleMake = VL.FACTORY.knowVehicleMake();
                        if (knowVehicleMake === false) return "";
                        if (!Common.ValidateText(txtVehicleModel)) {
                            return 'Vehicle Model is required';
                        }
                        return "";
                    }
                ],
                validateOnBlur: true,
                group: "ValidateProvideLoanInfo"
            });
            $('#txtVehicleYear').observer({
                validators: [
                    function (partial) {
                        var txtVehicleYear = $('#txtVehicleYear').val();
                        var knowVehicleMake = VL.FACTORY.knowVehicleMake();
                        if (knowVehicleMake === false) return "";
                        if (!Common.ValidateText(txtVehicleYear)) {
                            return 'Vehicle Year is required';
                        }
                        return "";
                    }
                ],
                validateOnBlur: true,
                group: "ValidateProvideLoanInfo"
            });
            /*$('#ddlBranches').observer({
                validators: [
                    function (partial) {
                        if ($('#VLBranchesDiv').length == 0) return "";
                        if (!Common.ValidateText($(this).val())) {
                            return "Select the answer for: Which branch is most convenient for you?";
                        }
                        return "";
                    }
                ],
                validateOnBlur: true,
                group: "ValidateProvideLoanInfo"
            });*/

            //this is required to pass schema validation
            $('#ddlTradeInVehicleType').observer({
                validators: [
                    function (partial) {
                        if (VL.FACTORY.IsTradeInEnabled() && Common.ValidateText(VL.FACTORY.vehicleType("TradeIn")) == false) {
                            return "Vehicle Type is required";
                        }
                        return "";
                    }
                ],
                validateOnBlur: true,
                group: "ValidateProvideLoanInfo"
            });

            /*no validate all vehicle infor fields for tradin 
            $('#ddlTradeInVehicleType').observer({
                validators: [
                    function (partial) {
                        if (VL.FACTORY.IsTradeInEnabled() && Common.ValidateText(VL.FACTORY.vehicleType("TradeIn")) == false) {
                            return "Vehicle Type is required";
                        }
                        return "";
                    }
                ],
                validateOnBlur: true,
                group: "ValidateProvideLoanInfo"
            });
            $('#divTradeInVehicleMake').observer({
                validators: [
                    function (partial) {
                        if (VL.FACTORY.IsTradeInEnabled() && !Common.ValidateText(VL.FACTORY.tradeInVehicleMake())) {
                            return 'Vehicle Make is required';
                        }
                        return "";
                    }
                ],
                validateOnBlur: true,
                group: "ValidateProvideLoanInfo"
            });
            $('#txtTradeInVehicleModel').observer({
                validators: [
                    function (partial) {
                        var txtVehicleModel = $('#txtTradeInVehicleModel').val();
                        if (VL.FACTORY.IsTradeInEnabled() && !Common.ValidateText(txtVehicleModel)) {
                            return 'Vehicle Model is required';
                        }
                        return "";
                    }
                ],
                validateOnBlur: true,
                group: "ValidateProvideLoanInfo"
            });
            $('#txtTradeInVehicleYear').observer({
                validators: [
                    function (partial) {
                        var txtVehicleYear = $('#txtTradeInVehicleYear').val();
                        if (VL.FACTORY.IsTradeInEnabled() && !Common.ValidateText(txtVehicleYear)) {
                            return 'Vehicle Year is required';
                        }
                        return "";
                    }
                ],
                validateOnBlur: true,
                group: "ValidateProvideLoanInfo"
            });
            $('#txtTradeInVehicleCurrentValue').observer({
                validators: [
                    function (partial) {
                        if (VL.FACTORY.IsTradeInEnabled() && !Common.ValidateTextNonZero($('#txtTradeInVehicleCurrentValue').val())) {
                            return 'What is the current value of this vehicle?';
                        }
                        return "";
                    }
                ],
                validateOnBlur: true,
                group: "ValidateProvideLoanInfo"
            });
            $('#txtTradeInVehicleOwn').observer({
                validators: [
                    function (partial) {
                        if (VL.FACTORY.IsTradeInEnabled() && !Common.ValidateTextAllowZero($('#txtTradeInVehicleOwn').val())) {
                            return 'How much do you still owe on this vehicle?';
                        }
                        return "";
                    }
                ],
                validateOnBlur: true,
                group: "ValidateProvideLoanInfo"
            });
            $('#txtTradeInVehiclePayingMonthly').observer({
                validators: [
                    function (partial) {
                        if (VL.FACTORY.IsTradeInEnabled() && !Common.ValidateTextAllowZero($('#txtTradeInVehiclePayingMonthly').val())) {
                            return 'How much are you paying monthly for this vehicle?';
                        }
                        return "";
                    }
                ],
                validateOnBlur: true,
                group: "ValidateProvideLoanInfo"
            });
            */
            $('#divLoanPurpose').observer({
                validators: [
                    function (partial) {
                        if (VL.FACTORY.hasLoanPurpose()) { //validate loan purpose if it exists
                            if ($('#hdLoanPurpose').val().toUpperCase() === "")
                                return "Please select Loan Purpose";
                        }
                        return "";
                    }
                ],
                validateOnBlur: false,
                group: "ValidateProvideLoanInfo"
            });
            //$('#ddlBranchName').observer({
            //	validators: [
            //		function (partial) {
            //			if ($('#hdVisibleBranch').val() != "Y" || $('#hdRequiredBranch').val() == "N") { //skip validation
            //				return "";
            //			}
            //			if ($('#ddlBranchName').find('option').length > 0) {
            //				if (!Common.ValidateText($('#ddlBranchName option:selected').val())) {
            //					return "Please select Branch Location";
            //				}
            //			}
            //			return "";
            //		}
            //	],
            //	validateOnBlur: true,
            //	group: "ValidateProvideLoanInfo"
            //});
            $('#divNewOrUsedVehicle').observer({
                validators: [
                    function (partial) {
                        if ($("#hdVlIsNewVehicle").val() === "") {
                            return "Is this a new or used vehicle?";
                        }
                        return "";
                    }
                ],
                validateOnBlur: true,
                container: '#divNewOrUsedVehicle',
                groupType: "checkboxGroup",
                group: "ValidateProvideLoanInfo"
            });
            $('#divKnowMakeAndModel').observer({
                validators: [
                    function (partial) {
                        if ($("#hdVlKnowVehicleMake").val() === "") {
                            return "Do you know the Make and Model?";
                        }
                        return "";
                    }
                ],
                validateOnBlur: true,
                container: '#divKnowMakeAndModel',
                groupType: "checkboxGroup",
                group: "ValidateProvideLoanInfo"
            });
        }

        function viewVehicleLoanInfo() {
            var strHtml = "";
            var strEstLabel = "Est. Purchase Price";
            strHtml += '<div class="col-sm-6 col-xs-12"><div class="row"><div class="col-xs-6 text-right row-title"><span class="bold">New Vehicle</span></div><div class="col-xs-6 text-left row-data"><span>' + (VL.FACTORY.IsNewVehicle() ? "YES" : "NO") + '</span></div></div></div>';
            if (VL.FACTORY.IsNewVehicle() == false && VL.FACTORY.knowVehicleMake() === true && VL.FACTORY.vehicleType().toUpperCase() !== "BOAT") {
                var strMileage = $.trim($("#txtVehicleMileage").val());
                strHtml += '<div class="col-sm-6 col-xs-12"><div class="row"><div class="col-xs-6 text-right row-title"><span class="bold">Vehicle Mileage</span></div><div class="col-xs-6 text-left row-data"><span>' + (strMileage != "" ? strMileage : "unknown") + '</span></div></div></div>';
            }
            if (VL.FACTORY.hasLoanPurpose()) {
                var strLoanPurpose = $("#hdLoanPurpose").val();
                strHtml += '<div class="col-sm-6 col-xs-12"><div class="row"><div class="col-xs-6 text-right row-title"><span class="bold">Loan Purpose</span></div><div class="col-xs-6 text-left row-data"><span>' + LOANPURPOSELIST[strLoanPurpose] + '</span></div></div></div>';
                if (strLoanPurpose.toUpperCase().indexOf('REFI') > -1 || strLoanPurpose.toUpperCase().indexOf('BUYOUT') > -1) {
                    strEstLabel = 'Est. Vehicle Value';
                }
            }
            strHtml += '<div class="col-sm-6 col-xs-12"><div class="row"><div class="col-xs-6 text-right row-title"><span class="bold">Vehicle Type</span></div><div class="col-xs-6 text-left row-data"><span>' + $("#ddlVehicleType").val() + '</span></div></div></div>';

            strHtml += '<div class="col-sm-6 col-xs-12"><div class="row"><div class="col-xs-6 text-right row-title"><span class="bold">Know Vehicle Make</span></div><div class="col-xs-6 text-left row-data"><span>' + (VL.FACTORY.knowVehicleMake() ? "YES" : "NO") + '</span></div></div></div>';

            if (VL.FACTORY.knowVehicleMake()) {
                strHtml += '<div class="col-sm-6 col-xs-12"><div class="row"><div class="col-xs-6 text-right row-title"><span class="bold">Vehicle Make</span></div><div class="col-xs-6 text-left row-data"><span>' + VL.FACTORY.vehicleMake() + '</span></div></div></div>';
                strHtml += '<div class="col-sm-6 col-xs-12"><div class="row"><div class="col-xs-6 text-right row-title"><span class="bold">Vehicle Model</span></div><div class="col-xs-6 text-left row-data"><span>' + htmlEncode($.trim($("#txtVehicleModel").val())) + '</span></div></div></div>';
                strHtml += '<div class="col-sm-6 col-xs-12"><div class="row"><div class="col-xs-6 text-right row-title"><span class="bold">Vehicle Year</span></div><div class="col-xs-6 text-left row-data"><span>' + $("#txtVehicleYear").val() + '</span></div></div></div>';
            }
            strHtml += '<div class="col-sm-6 col-xs-12"><div class="row"><div class="col-xs-6 text-right row-title"><span class="bold">' + strEstLabel + '</span></div><div class="col-xs-6 text-left row-data"><span>' + Common.FormatCurrency($("#txtEstimatedVehicleValue").val(), true) + '</span></div></div></div>';

            var requestedLoanTitle = "Requested Loan";
            if (VL.FACTORY.hasLoanPurpose()) {
                if ($("#hdLoanPurpose").val().toUpperCase().indexOf('REFI') < 0) {
                    strHtml += '<div class="col-sm-6 col-xs-12"><div class="row"><div class="col-xs-6 text-right row-title"><span class="bold">Down Payment</span></div><div class="col-xs-6 text-left row-data"><span>' + Common.FormatCurrency($("#txtDownPayment").val(), true) + '</span></div></div></div>';
                } else {
                    requestedLoanTitle = "Req. Refinance Amount";
                }
            }
            strHtml += '<div class="col-sm-6 col-xs-12"><div class="row"><div class="col-xs-6 text-right row-title"><span class="bold">' + requestedLoanTitle + '</span></div><div class="col-xs-6 text-left row-data"><span>' + Common.FormatCurrency($("#txtProposedLoanAmount").val(), true) + '</span></div></div></div>';
            var loanTerm = "";
            if (VL.FACTORY.vehicleType() == "CAR" && !VL.FACTORY.AutoTermTextboxEnable()) {
                loanTerm = $("#ddlLoanTerm option:selected").val();
            } else {
                loanTerm = $("#txtLoanTerm").val();
            }
            if (loanTerm != "") {
                strHtml += '<div class="col-sm-6 col-xs-12"><div class="row"><div class="col-xs-6 text-right row-title"><span class="bold">Loan Term</span></div><div class="col-xs-6 text-left row-data"><span>' + loanTerm + ' months</span></div></div></div>';
            }
            if (VL.FACTORY.IsTradeInEnabled()) {
                strHtml += '<div class="col-xs-12" style="clear:left;"><div class="row row-title">Trade-in Vehicle Information</div></div>';
                if ($("#ddlTradeInVehicleType").val() != "" && !$("#ddlTradeInVehicleType").IsHideField()) {
                    strHtml += '<div class="col-sm-6 col-xs-12"><div class="row"><div class="col-xs-6 text-right row-title"><span class="bold">Vehicle Type</span></div><div class="col-xs-6 text-left row-data"><span>' + $("#ddlTradeInVehicleType").val() + '</span></div></div></div>';
                }
                if (VL.FACTORY.tradeInVehicleMake() != "" && !$("#txtTradeInVehicleModel").IsHideField()) {
                    strHtml += '<div class="col-sm-6 col-xs-12"><div class="row"><div class="col-xs-6 text-right row-title"><span class="bold">Vehicle Make</span></div><div class="col-xs-6 text-left row-data"><span>' + VL.FACTORY.tradeInVehicleMake() + '</span></div></div></div>';
                }
                if ($("#txtTradeInVehicleModel").val() != "" && !$("#txtTradeInVehicleModel").IsHideField()) {
                    strHtml += '<div class="col-sm-6 col-xs-12"><div class="row"><div class="col-xs-6 text-right row-title"><span class="bold">Vehicle Model</span></div><div class="col-xs-6 text-left row-data"><span>' + htmlEncode($.trim($("#txtTradeInVehicleModel").val())) + '</span></div></div></div>';
                }
                if ($("#txtTradeInVehicleYear").val() != "" && !$("#txtTradeInVehicleYear").IsHideField()) {
                    strHtml += '<div class="col-sm-6 col-xs-12"><div class="row"><div class="col-xs-6 text-right row-title"><span class="bold">Vehicle Year</span></div><div class="col-xs-6 text-left row-data"><span>' + $("#txtTradeInVehicleYear").val() + '</span></div></div></div>';
                }
                if ($("#txtTradeInVehicleCurrentValue").val() != "" && !$("#txtTradeInVehicleCurrentValue").IsHideField()) {
                    strHtml += '<div class="col-sm-6 col-xs-12"><div class="row"><div class="col-xs-6 text-right row-title"><span class="bold">Current value of this vehicle</span></div><div class="col-xs-6 text-left row-data"><span>' + $("#txtTradeInVehicleCurrentValue").val() + '</span></div></div></div>';
                }
                if ($("#txtTradeInVehicleOwn").val() != "" && !$("#txtTradeInVehicleOwn").IsHideField()) {
                    strHtml += '<div class="col-sm-6 col-xs-12"><div class="row"><div class="col-xs-6 text-right row-title"><span class="bold">You owe on this vehicle</span></div><div class="col-xs-6 text-left row-data"><span>' + $("#txtTradeInVehicleOwn").val() + '</span></div></div></div>';
                }
                if ($("#txtTradeInVehiclePayingMonthly").val() != "" && !$("#txtTradeInVehiclePayingMonthly").IsHideField()) {
                    strHtml += '<div class="col-sm-6 col-xs-12"><div class="row"><div class="col-xs-6 text-right row-title"><span class="bold">Paying monthly for this vehicle</span></div><div class="col-xs-6 text-left row-data"><span>' + $("#txtTradeInVehiclePayingMonthly").val() + '</span></div></div></div>';
                }
            }
            return strHtml;
        }

    
        VL.FACTORY.getVehicleLoanInfo=function(appInfo) {
             appInfo.PlatformSource = $('#hdPlatformSource').val();
             appInfo.LenderRef = $('#hfLenderRef').val();
            //    branchID from query Param has precedent over  branchID from dropdown 
             appInfo.BranchID = getBranchId();
             appInfo.LoanOfficerID = $('#hfLoanOfficerID').val();
             appInfo.ReferralSource = $('#hfReferralSource').val();
            if (VL.FACTORY.vehicleType() == "CAR" && !VL.FACTORY.AutoTermTextboxEnable()) {
              appInfo.LoanTerm = $("#ddlLoanTerm option:selected").val();
           } else {
              appInfo.LoanTerm = $("#txtLoanTerm").val();
           }
       
            if (VL.FACTORY.hasLoanPurpose()) {
                appInfo.LoanPurpose = $("#hdLoanPurpose").val();
            } else {
                appInfo.LoanPurpose = " "; //there is no loan purpose, just pass empty string 
            }
            appInfo.VehicleType = VL.FACTORY.vehicleType();
            appInfo.KnowVehicleMake = $('#hdVlKnowVehicleMake').val().toUpperCase();
            appInfo.depositAmount = 0;
           
            appInfo.VehicleMake = "";
            appInfo.VehicleModel = "";
            appInfo.VehicleYear = "";
            appInfo.VinNumber = "";
            if (appInfo.KnowVehicleMake == 'Y') {
                appInfo.VehicleMake = VL.FACTORY.vehicleMake();
                appInfo.VehicleModel = $('#txtVehicleModel').val();
                appInfo.VehicleYear = $('#txtVehicleYear').val();             
                var vehType = $('#ddlVehicleType option:selected').val();
				var $vinNumber = $("#txtVinNumber");
				<%-- AP-2315 - VIN can be hidden by Visibility, in which case $vinNumber.val() is undefined which leads to errors. Skip if there is no VIN field. --%>
				if ($vinNumber.length > 0) {
					if (vehType != undefined && (vehType.toUpperCase() == "CAR" || vehType.toUpperCase() == "MOTORCYCLE")) {
						if ($vinNumber.val().length > 10 && $.trim($vinNumber.val()).length < 18) {
							appInfo.VinNumber = $.trim($vinNumber.val());
						}
					} else { //vehType =BOAT,TRAVELTRAILER, MOTORHOME ...
						if ($vinNumber.val().length > 0 && $.trim($vinNumber.val()) != "") { //save vin# if it exist
							appInfo.VinNumber = $.trim($vinNumber.val());
						}
					}
				}
            }
            //trade in
            appInfo.TradeValue = 0;
            appInfo.TradePayOff = 0;
            appInfo.TradePayment = 0;
            appInfo.TradeType = '';
            appInfo.TradeYear = '';
            appInfo.TradeModel = '';
            appInfo.TradeMake = '';
            appInfo.HasTradeIn = (VL.FACTORY.IsTradeInEnabled() ? "Y" : "N");
            if (appInfo.HasTradeIn === "Y") {
                appInfo.TradeValue = Common.GetFloatFromMoney($('#txtTradeInVehicleCurrentValue').val());
                appInfo.TradePayOff = Common.GetFloatFromMoney($('#txtTradeInVehicleOwn').val());
                appInfo.TradePayment = Common.GetFloatFromMoney($('#txtTradeInVehiclePayingMonthly').val());
                appInfo.TradeType = VL.FACTORY.vehicleType("TradeIn");
                appInfo.TradeYear = $('#txtTradeInVehicleYear').val();
                appInfo.TradeModel = $('#txtTradeInVehicleModel').val();
                appInfo.TradeMake = VL.FACTORY.tradeInVehicleMake();
            }

            var mileage = '';
            appInfo.IsNewVehicle = $('#hdVlIsNewVehicle').val().toUpperCase();
            if (appInfo.IsNewVehicle != 'Y' && VL.FACTORY.vehicleType().toUpperCase() !== "BOAT" && $('#hdVlKnowVehicleMake').val().toUpperCase() === "Y") {
                mileage = $('#txtVehicleMileage').val();
            }

            appInfo.VehicleMileage = mileage;
            var isRefinance = false;

        
            if ($("#hdLoanPurpose").val().toUpperCase().indexOf("REFI") > -1) {
                isRefinance = true;
            }

            appInfo.VehicleValue = Common.GetFloatFromMoney($('#txtEstimatedVehicleValue').val());
            if (isRefinance) {
                appInfo.DownPayment = "0";
            }
            else {
                appInfo.DownPayment = Common.GetFloatFromMoney($('#txtDownPayment').val());
            }
            appInfo.ProposedLoanAmount = Common.GetFloatFromMoney($('#txtProposedLoanAmount').val());     
            return appInfo;
        }

        function getSelectedCategory(selectedLoanPurpose) {
            var loanPurposeCategoryObj = JSON.parse(LoanPurposeCategory);
            for (var key in loanPurposeCategoryObj) {
                if (key == selectedLoanPurpose) {
                    return loanPurposeCategoryObj[key];
                }
            }
            return "";
        }

        function fillVinInfo(data) {
            $("#txtVehicleModel").val(data.Model);
            $.lpqValidate.hideValidation($("#txtVehicleModel"));
            $("#txtVehicleYear").val(data.Year);
            $.lpqValidate.hideValidation($("#txtVehicleYear"));
            $.lpqValidate.hideValidation($("#divVehicleMake"));
            $("#ddlMotorCycleMake").val(data.Make);
            $("#ddlMotorCycleMake").selectmenu().selectmenu("refresh", true);
            $("#ddlCarMake").val(data.Make);
            $("#ddlCarMake").selectmenu().selectmenu("refresh", true);
            $("#txtMake").val(data.Make);
            $("#divVinNumber .vin-scan-guide").removeClass("hidden");
            $("#divVinNumber .vin-scan-msg").text("Could not decode VIN number");
            $("#divVinNumber .vin-scan-msg").addClass("hidden");
        }
        function clearVinInfo(data) {
            $("#txtVehicleModel").val("");
            $("#txtVehicleYear").val("");
            $("#ddlMotorCycleMake").val("");
            $("#ddlMotorCycleMake").selectmenu().selectmenu("refresh", true);
            $("#ddlCarMake").val("");
            $("#ddlCarMake").selectmenu().selectmenu("refresh", true);
            $("#txtMake").val("");
        }
        function decodeVinNumber(vinNumber) {
            vinNumber = $.trim(vinNumber);
            if (vinNumber.length == 17) {
                //caching
                if (sessionStorage.getItem(vinNumber) && sessionStorage.getItem(vinNumber) !== "") {
                    fillVinInfo($.parseJSON(sessionStorage.getItem(vinNumber)));
                } else {
                    sessionStorage.setItem(vinNumber, "");
                    $("#divVinNumber span.loader").removeClass("hidden");
                    $.ajax({
                        url: '/handler/Handler.aspx',
                        async: true,
                        cache: false,
                        global: false,
                        type: 'POST',
                        dataType: 'html',
                        data: {
                            command: "decodeVinNumber",
                            lenderRef: $('#hfLenderRef').val(),
                            vinNumber: $.trim(vinNumber)
                        },
                        success: function (responseText) {
                            var response = $.parseJSON(responseText);
                            if (response.IsSuccess) {
                                sessionStorage.setItem(vinNumber, Common.toJSON(response.Info));
                                fillVinInfo(response.Info);
                            } else {
                                $("#divVinNumber .vin-scan-guide").addClass("hidden");
                                $("#divVinNumber .vin-scan-msg").text("Could not decode VIN number");
                                $("#divVinNumber .vin-scan-msg").removeClass("hidden");
                                clearVinInfo();
                            }
                        },
                        error: function (e) {
                            $("#divVinNumber .vin-scan-guide").addClass("hidden");
                            $("#divVinNumber .vin-scan-msg").text("Could not decode the VIN number");
                            $("#divVinNumber .vin-scan-msg").removeClass("hidden");
                            clearVinInfo();
                        }
                    }).always(function () {
                        $("#divVinNumber span.loader").addClass("hidden");
                    });
                }
            }
        }  
    }(window.VL = window.VL || {}, jQuery));

    $(function () {
  
        VL.FACTORY.init();

        $(document).on("pageshow", function () {
            var curPageId = $.mobile.pageContainer.pagecontainer('getActivePage').attr('id');
            switch (curPageId) {
            	case "vehicleInfo":
                    VL.FACTORY.vl1Init();
            }
        });

        $("div[name='disclosures'] label").click(function () {
            handleDisableDisclosures(this);
        });

        handleShowAndHideBranchNames();
        $("#popUpCreditDiff, #popUpSavingAmountGuid, #popEstimatePaymentCalculator").on("popupafteropen", function (event, ui) {
            $("#" + this.id + "-screen").height("");
        });
   
        $("#popEstimatePaymentCalculator").on("popupbeforeposition", function () {
            var $container = $("#popEstimatePaymentCalculator");
            $("div.required-field-notation", $container).remove();
            $("input[data-field='LoanAmount']", $container).val(Common.FormatCurrency(0, true));
            if (Common.GetFloatFromMoney($.trim($("#txtProposedLoanAmount").val())) > 0) {
                $("input[data-field='LoanAmount']", $container).val($("#txtProposedLoanAmount").val());
            }
            if ($("#txtProposedLoanAmount").attr("readonly")) {
                $("input[data-field='LoanAmount']", $container).attr("disabled", "disabled");
            } else {
                $("input[data-field='LoanAmount']", $container).removeAttr("disabled");
            }
            $("input[data-field='LoanAmount']", $container).trigger("create");
            var $loanTerm = $("input[data-field='LoanTerm']", $container);
            if ($("#txtLoanTerm").is(":visible")) {
                $("select[data-field='LoanTerm']", $container).closest('div[data-role="fieldcontain"]').hide();
                $loanTerm.val("1"); //set default value for textbox
                if ($.trim($("#txtLoanTerm").val()) !== "" && parseFloat($.trim($("#txtLoanTerm").val())) > 0) {
                    $loanTerm.val($("#txtLoanTerm").val());
                }
            } else {
                $loanTerm = $("select[data-field='LoanTerm']", $container);
                $("input[data-field='LoanTerm']", $container).closest('div[data-role="fieldcontain"]').hide();
                $loanTerm.val($("#ddlLoanTerm").val()).selectmenu().selectmenu("refresh", true);

            }
            $loanTerm.closest('div[data-role="fieldcontain"]').show();
            bindEstimatePayment();
        });
        $("select", "#popEstimatePaymentCalculator").on("change", bindEstimatePayment);
        $("input", "#popEstimatePaymentCalculator").on("blur", bindEstimatePayment);
        $("input", "#popEstimatePaymentCalculator").on("keyup paste", function () {
            setTimeout(bindEstimatePayment, 400);
        });
        $("#ddlLoanTerm, #txtProposedLoanAmount").on("change", fillEstimatePayment);
        $("#txtLoanTerm, #txtProposedLoanAmount").on("blur", fillEstimatePayment);
        $("#txtLoanTerm, #txtProposedLoanAmount").on("keyup paste", function () {
            setTimeout(fillEstimatePayment, 400);
        });
        $("#popNotification").enhanceWithin().popup();

    });
    function fillEstimatePayment() {
        var $loanAmount = $("#txtProposedLoanAmount");
        var $loanTerm = $("#txtLoanTerm");
        var $container = $("#divEstimatePayment");
        if ($loanTerm.is(":hidden")) {
            $loanTerm = $("#ddlLoanTerm");
        }
        var loanAmount = Common.GetFloatFromMoney($loanAmount.val());
        var loanTerm = parseFloat($loanTerm.val());
        var rate = parseFloat($("#spInterestRate").data("value"));
        var estPayment = 0;
        if (loanAmount > 0 && loanTerm > 0) {
            estPayment = calculateEstimatePayment(loanAmount, loanTerm, rate);
        }
        $("span.est-payment-value", $container).text(Common.FormatCurrency(estPayment, true));
    }
    function calculateEstimatePayment(loanAmount, loanTerm, rate) {
        return (loanAmount * (rate / 12) / 100) / (1 - Math.pow((1 + (rate / 12) / 100), -1 * loanTerm));
    }
    function bindEstimatePayment() {
        var $container = $("#popEstimatePaymentCalculator");
        var $loanAmount = $("input[data-field='LoanAmount']", $container);
        var loanAmount = Common.GetFloatFromMoney($loanAmount.val());
        var $loanTerm = $("input[data-field='LoanTerm']", $container);
        if ($("#txtLoanTerm").is(":visible") == false) {
            $loanTerm = $("select[data-field='LoanTerm']", $container);
        }
        var loanTerm = 12;
        if ($loanTerm.length > 0 && parseFloat($loanTerm.val()) > 0) {
            loanTerm = parseFloat($loanTerm.val());
        } else {
            $loanTerm.val(12);
            if ($loanTerm.is("select")) {
                $loanTerm.selectmenu('refresh');
            }
        }
        var interestRate = $("input[data-field='InterestRate']", $container).val().replace("%", "");
        var estAmount = Common.FormatCurrency(calculateEstimatePayment(loanAmount, loanTerm, interestRate), true);
        $("[data-field='EstimatePayment']", $container).text(estAmount);
    }

    ////**********start wallet questions and answers ************
    function displayWalletQuestions(message) {
        // The first two characters of the message contains the number of questions
        var numQuestions = parseInt(message.substr(0, 2), 10);
        var questionsHTML = message.trim().substr(2, message.length - 2);
        // Put the number of questions into the hidden input field
        $('#hdNumWalletQuestions').val(numQuestions);
        // Put the HTML for the questions into the proper location in our page
        $('#walletQuestionsDiv').html(questionsHTML);
        var nextpage = $('#walletQuestions');
        $.mobile.changePage(nextpage, {
            transition: "fade",
            reverse: false,
            changeHash: true
        });
    }
    //jquery mobile need to transition to another page in order to format the html autheitcation question correctly
    function co_displayWalletQuestions(message) {
        // The first two characters of the message contains the number of questions
        var numQuestions = parseInt(message.substr(0, 2), 10);
        var questionsHTML = message.substr(2, message.length - 2);
        // Put the number of questions into the hidden input field
        $('#hdNumWalletQuestions').val(numQuestions);
        // Put the HTML for the questions into the proper location in our page   
        $('#walletQuestionsDiv').empty();
        $('#co_walletQuestionsDiv').html(questionsHTML);
        nextpage = $('#co_walletQuestions');
        $.mobile.changePage(nextpage, {
            transition: "slide",
            reverse: false,
            changeHash: true
        });
    }
    function validateWalletQuestions(element, isCoApp) {
        if ($(element).attr("contenteditable") == "true") return false;
        //this lines of code is to prevent user click submit multiple times accidently
        if (isCoApp) {
            if (isCoSubmitWalletAnswer) {
                return false;
            }
            isCoSubmitWalletAnswer = true;
        } else {
            if (isSubmitWalletAnswer) {
                return false;
            }
            isSubmitWalletAnswer = true;
        }
        // Make sure that there is a selected answer for each question.
        // After validating the questions, submit them
        var index = 0;
        var numSelected = 0;
        var errorStr = "";
        var numOfQuestions = $('#hdNumWalletQuestions').val();
        numOfQuestions = parseInt(numOfQuestions, 10);
        for (index = 1; index <= numOfQuestions; index++) {
            // Check to see if a radio button was selected
            var radioButtons = $('input[name=walletquestion-radio-' + index.toString() + ']');
            var numRadioButtons = radioButtons.length;
            for (var i = 0; i < numRadioButtons; i++) {
                if ($(radioButtons[i]).prop('checked') == true) {
                    numSelected++;
                }
            }
            if (numSelected == 0) {
                errorStr += "Please answer all the questions";
                break;
            }
            numSelected = 0;
        }
        if (errorStr != "") {
            // An error occurred, we need to bring up the error dialog.
            if ($("#divErrorPopup").length > 0) {
                $('#txtErrorPopupMessage').html(errorStr);
                $("#divErrorPopup").popup("open", { "positionTo": "window" });
            } else {
                var nextpage = $('#divErrorDialog');
                $('#txtErrorMessage').html(errorStr);
                if (nextpage.length > 0) {
                    $.mobile.changePage(nextpage, {
                        transition: "slide",
                        reverse: false,
                        changeHash: true
                    });
                }
            }

            return false;
        }
        // No errors, so we can proceed
        submitWalletAnswers(isCoApp);

        //disable submit answer button 
        if (isCoApp) {
            //disable submit answer button 
            $('#co_walletQuestions .div-continue-button').addClass('ui-disabled');
        } else {
            $('#walletQuestions .div-continue-button').addClass('ui-disabled');
        }

        return false;
    }

    function submitWalletAnswers(isCoApp) {
        $('#txtErrorMessage').html('');
        var answersObj = getWalletAnswers(isCoApp);
        answersObj = attachGlobalVarialble(answersObj);
        if ($("#hdIsComboMode").val() == "Y") {
            if ($("#divCcFundingOptions").length === 1) {
                answersObj.depositAmount = Common.GetFloatFromMoney($('#txtFundingDeposit').val());
                FS1.loadValueToData();
                answersObj.rawFundingSource = JSON.stringify(FS1.Data);
            }
        }
        $.ajax({
            type: 'POST',
            url: 'Callback.aspx',
            data: answersObj,
            dataType: 'html',
            success: function (response) {

                //enable submit answer button 
                if (isCoApp) {
                    //disable submit answer button 
                    $('#co_walletQuestions .div-continue-button').removeClass('ui-disabled');
                } else {
                    $('#walletQuestions .div-continue-button').removeClass('ui-disabled');
                }

                if (response.toUpperCase().indexOf('MLERRORMESSAGE') > -1) {
                    goToDialog(response);
                }
                else {
                    if (response.toUpperCase().indexOf('MLNOIDAUTHENTICATION') > -1) {
                        isApplicationCompleted = true;
                        goToLastDialog(response);
                        //need to display response first before clear all inputs
                        //clearForms();

                        //disable submit answer button 
                        if (isCoApp) {
                            //disable submit answer button 
                            $('#co_walletQuestions .div-continue-button').addClass('ui-disabled');
                        } else {
                            $('#walletQuestions .div-continue-button').addClass('ui-disabled');
                        }

                    } else if (response.toUpperCase().indexOf('WALLETQUESTION') > -1) {
                        co_displayWalletQuestions(response);

                        //disable submit answer button 
                        $('#walletQuestions .div-continue-button').addClass('ui-disabled');
                    } else {
                        //   isApplicationCompleted = true;
                        goToLastDialog(response);
                        //need to display response first before clear all inputs
                        //clearForms();
                        //disable submit answer button 

                        if (isCoApp) {
                            //disable submit answer button 
                            $('#co_walletQuestions .div-continue-button').addClass('ui-disabled');
                        } else {
                            $('#walletQuestions .div-continue-button').addClass('ui-disabled');
                        }
                    }
                }
            },
            error: function (err) {

                //enable submit answer button 
                if (isCoApp) {
                    //disable submit answer button 
                    $('#co_walletQuestions .div-continue-button').removeClass('ui-disabled');
                } else {
                    $('#walletQuestions .div-continue-button').removeClass('ui-disabled');
                }

                goToDialog('There was an error when processing your request.  Please try the application again.');
            }
        });
        return false;
    }


    function SelectProduct(index) {
        $('#txtErrorMessage').html('');
        var appInfo = new Object();
        appInfo.LenderRef = $('#hfLenderRef').val();
        appInfo.Index = index;
        appInfo.Task = 'SelectProduct';
        var txtLName = $('#txtLName').val();
        var txtFName = $('#txtFName').val();
        appInfo.FullName = txtFName + ' ' + txtLName;
        appInfo = attachGlobalVarialble(appInfo);
        $.ajax({
            type: 'POST',
            url: 'CallBack.aspx',
            data: appInfo,
            dataType: 'html',
            success: function (response) {
                if (response.toUpperCase().indexOf('MLERRORMESSAGE') > -1) {
                    goToDialog(response);
                } else {
                    //submit sucessfull so clear
                    clearForms();
                    goToLastDialog(response);
                }
            },
            error: function (err) {
                $('input.custom').attr('disabled', false); //if there is errors, enable agree button
                goToDialog('There was an error when processing your request.  Please try the application again.');
            }
        });
        //there no error
        return false;
    }

    function goToDialog(message) {
        if ($("#divErrorPopup").length > 0) {
            $('#txtErrorPopupMessage').html(message);  //error message are not encoded, endoding will remove html tag
            $("#divErrorPopup").popup("open", { "positionTo": "window" });
        } else {
            $('#txtErrorMessage').html(message);  //error message are not encoded, endoding will remove html tag
            $('#href_show_dialog_1').trigger('click');
        }
    }
    function hasCoApplicant() {
        return $("#hdVlHasCoApplicant").val() === "Y";
    }
    function goBackPageSubmit() {
        if (typeof resetFunding == 'function') {
            resetFunding();
        }
        if ($("#hdIs2ndLoanApp").val() === "Y") {
            goToNextPage("#vl1");
        } else {
            if ($("#hdVlHasCoApplicant").val() === "Y") {
                goToNextPage("#vl6");
            } else {
                goToNextPage("#vl2");
            }
        }

    }

    function goToLastDialog(message) {
        //var decoded_message = $('<div/>').html(message).text(); //message maybe encoded in the config xml so need to decode
        // $('#div_system_message').html(decoded_message); ->already decoded in callback.aspx
        //application completed so clear
        clearForms();
        $('#div_system_message').html(message);

        //docusign window if enabled
        if ($('#div_system_message').find("iframe#ifDocuSign").length === 1) {
            appendDocuSign();
            $("#div_docuSignIframe").append($('#div_system_message').find("iframe#ifDocuSign"));

            ////goback to final decision message after docusign is closed
            //$('#div_docuSignIframe').find("iframe#ifDocuSign").on("load", function (evt) {
            //    //the initial page load twice so need this to make sure only capturing url on finish
            //    var flag = ($(this).data("loaded") || "") + "1";
            //    $(this).data("loaded", flag);
            //    if (flag === "111") {
            //        $('#href_show_last_dialog').trigger('click');
            //    }
            //});

            var height = window.innerHeight ||
                     document.documentElement.clientHeight ||
                     document.body.clientHeight;

            var footerHeight = $('.divfooter').height();

            height = height - footerHeight - 64;
            $('#div_docuSignIframe').find("iframe#ifDocuSign").attr("height", height);
            $.mobile.changePage("#popupDocuSign", {
                transition: "fade",
                reverse: false,
                changeHash: false
            });
        }
            //cross-sell link button if enabled
        else {

            $(".xsell-selection-panel .xsell-item-option.btn-header-theme", "#div_system_message").on("mouseenter click", function (ev) {
                var element = $(this);
                if (ev.type != 'click') {
                    element.addClass('btnHover');
                    element.removeClass('btnActive_on');
                    element.removeClass('btnActive_off');
                } else {
                    element.removeClass('btnHover');
                }
            }).on("mouseleave", function () {
                handledBtnHeaderTheme($(this));
            });
            $('#href_show_last_dialog').trigger('click');
        }
    }
    function getWalletAnswers(isCoApp) {
        // Grab the answer choices that have been selected by the user
        var appInfo = new Object();
        //get wallet questions and answers
        getWalletQuestionsAndAnswers(appInfo);

        // appInfo.CSRF = $('#hfCSRF').val();
        appInfo.LenderRef = $('#hfLenderRef').val();
        appInfo.Task = "WalletQuestions";
        appInfo.idaMethodType = $('#hdIdaMethodType').val();
        appInfo.co_FirstName = $('#co_txtFName').val();
        appInfo.ProceedXAAnyway = $("#hdProceedMembership").val();
        appInfo.IsComboMode = $("#hdIsComboMode").val();
        if (isCoApp) {
            appInfo.hasJointWalletAnswer = "Y";
        } else {
            appInfo.hasJointWalletAnswer = "N";
        }
        return appInfo;
    }

    function prefillVehicleLoanInfo(){
        $('#ddlBranchName option').eq(1).prop('selected', true);
        $('#ddlBranchName').selectmenu("refresh").closest("div.ui-btn").addClass("btnActive_on");
        $("#divLoanPurpose a.btn-header-theme[data-role='button']:eq(0)").trigger("click");
        $('#ddlVehicleType').val("ATV").selectmenu("refresh");
        $('#txtEstimatedVehicleValue').val("20000").trigger("blur");
        $('#txtDownPayment').val("500").trigger("blur");
        $("#txtLoanTerm").val("12");
        $("#chkVehicleNew").prop("checked", true).checkboxradio("refresh");
        $("#chkDontKnowMake").prop("checked", true).checkboxradio("refresh");
        $("#hdVlIsNewVehicle").val("Y");
        $("#hdVlKnowVehicleMake").val("N");
    }
</script>