﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="OtherLoanInfo.ascx.vb" Inherits="bl_Inc_OtherLoanInfo" %>
<%@ Register Src="~/Inc/MainApp/xaApplicantQuestion.ascx" TagPrefix="uc" TagName="xaApplicantQuestion" %>
<%@ Import Namespace="Newtonsoft.Json" %>
<%@ Import Namespace="LPQMobile.Utils" %>
<script type="text/javascript">
  
    var _LOANPURPOSE = <%=JsonConvert.SerializeObject(_LoanPurposeList)%>;
	</script>
    <input type="hidden" id="hdIsLineOfCredit" value="N"/>
    <input type="hidden" id="hdLoanPurpose" value=""/>
    <input type="hidden" id="hdHasLoanPurpose" value="<%=IIf(_LoanPurposeList.Count > 0, "Y", "N")%>"/>
    <input type="hidden" id="hdEnableBranchSection" value="<%=IIf(EnableBranchSection AndAlso Not String.IsNullOrEmpty(_branchOptionsStr), "Y", "N")%>"/>
	<input type="hidden" id="hfLenderRef" value='<%=_CurrentLenderRef%>' />
    <div data-role="page" id="otherLoanInfo">
        <div data-role="header" class="sr-only">
            <h1>Other Loan Purpose Information</h1>
        </div>
        <div data-role="content" style="overflow-x: visible;">
            <%=HeaderUtils.RenderLogoAndSteps(_LogoUrl, 0, "APPLY_CC")%>
            <%=HeaderUtils.RenderPageTitle(11, "Other Loan Purpose", False)%>         
            <%=HeaderUtils.RenderPageTitle(0, "Select if this applies to you", true)%>      
            <div>                            
                <a href="#" data-role="button" class="btn-header-theme<%=IIf(IsLOC.ToUpper()="Y", " active", "") %>" style="text-align: center; padding-left: 0;" id="btnLineOfCredit">
                    <%If _CurrentLenderRef.ToUpper.StartsWith("SDFCU") Then%>
                        This is a Money Line (Line Of Credit)
                    <%Else%>
                        This is a Line of Credit
                    <%End If %>
                </a>
			 </div>
			<%If EnableBranchSection AndAlso Not String.IsNullOrEmpty(_branchOptionsStr) Then%>
				<div id="divBranchSection" <%=IIf(IsInMode("777") AndAlso IsInFeature("visibility"), "class=""showfield-section"" data-show-field-section-id=""" & BuildShowFieldSectionID("divBranchSection") & """ data-default-state=""off"" data-section-name= 'Branch'", "")%>>
			   <%--Branch location --%>
				 <div data-role="fieldcontain">
                     <%If _requiredBranch <> "N" Then%>
                         <%=HeaderUtils.RenderPageTitle(0, "Select branch location", True, "divTitleBranchName", isRequired:=True)%>
                     <%Else%>
                         <%=HeaderUtils.RenderPageTitle(0, "Select branch location", True, "divTitleBranchName")%> 
                     <%End If%>
                     <div id='divBranchName' class='header-theme-dropdown-btn'>
                        <select id="ddlBranchName" aria-labelledby="divTitleBranchName"><%=_branchOptionsStr%></select> 
                    </div>
                </div> 
			</div>
			<%End If%>
			<div>
             <div data-role="fieldcontain" id="divLoanPurposePanel">               		
                <div data-section="loan-purpose">  
                    
                    <%If(_LoanPurposeList.Any()) then %>
					<%=HeaderUtils.RenderPageTitle(0, "Select a purpose", True, isRequired:=True)%>
					<%End if %>
                    
                    <%For Each item As KeyValuePair(Of String, CTextValueCatItem) In _LoanPurposeList%>
                    <a href="#" data-role="button" class="btn-header-theme" data-command="loan-purpose" data-purpose-text="<%=item.Value.Text%>" data-key="<%=item.Key %>" style="text-align: center; padding-left: 0;"><%=item.Value.Text %></a>
                    <%Next %>
                </div>
                </div>
            </div>
			
			 <%=HeaderUtils.RenderPageTitle(0, "Provide loan information", True)%>
            <div data-role="fieldcontain">
                <label for="txtLoanAmount" class="RequiredIcon">Loan Amount</label>    
                <input type="<%=_textAndroidTel%>" pattern="[0-9]*" id="txtLoanAmount" class="money" maxlength ="12" />
            </div>
            <%--<div id="IsLOCDiv" runat="server">
                <div style="float: left; width: 70%;">
                    Is this a Line of Credit?
                </div>
                <div style="float: right; width: 25%; position: relative; top: -10px;">
                    <select name="slider" id="ddlIsLineOfCredit" data-role="slider" data-mini="true"
                        onchange="SwitchLineOfCreditButton(this, true);">
                        <option value="N" selected="selected">No</option>
                        <option value="Y">Yes</option>
                    </select>
                </div>
                <div style="clear: both">
                </div>
            </div>--%>
            <div id="divLoanTerm">
               <div data-role="fieldcontain">
                    <%If Not String.IsNullOrEmpty(_LoanTermDropdown) Then%>
                    <label for="ddlLoanTerm" class="RequiredIcon">Term (months)</label>
                     <select id="ddlLoanTerm">
                       <%=_LoanTermDropdown%> 
                    </select>
                   <%Else%>
                    <label for="txtLoanTerm" class="RequiredIcon">Term (months)</label>
                     <input type="tel" id="txtLoanTerm" class="numeric" maxlength="3" value="<%=Term%>" />
                    <%end if %>
                 </div>
            </div>
            <%-- 
			<%If CheckShowField("divEstimatePayment", "", False) Or (IsInMode("777") AndAlso IsInFeature("visibility")) Then%>
			<div id="divEstimatePayment" <%=IIf(IsInMode("777") AndAlso IsInFeature("visibility"), "class=""showfield-section"" data-show-field-section-id=""" & BuildShowFieldSectionID("divEstimatePayment") & """ data-default-state=""off"" data-section-name=""Estimated Payment""","") %>>
				<div>
					<div>
						<span>Estimated payment: </span>						
					</div>					
					<div class="est-payment-value_container">
						<span class="est-payment-value">$0.00</span>/mo
						<span class="calculator-btn btn-header-theme" onclick='openPopup("#popEstimatePaymentCalculator")' data-rel='popup' data-transition='pop'><i class="fa fa-calculator" aria-hidden="true"></i></span>
					</div>
				</div>
				<div>
					<span class="rename-able">Estimated payment based on</span><span id="spInterestRate" data-value="<%=_InterestRate %>"> <%=_interestRate.ToString("N2", New CultureInfo("en-US")) %>% </span><span class="rename-able">interest rate. Actual monthly payment may vary.</span>
				</div>
				<div id="popEstimatePaymentCalculator" data-role="popup" data-position-to="window">
					<div data-role="content">
						<div class="row">
							<div class="col-sm-12 header_theme2">
								<a href="#" data-rel="back" class="pull-right svg-btn"><%=HeaderUtils.IconClose %><span style="display: none;">close</span></a>
							</div>
						</div>
						<div class="row">
							<div class="col-sm-12">
								<div data-role="fieldcontain">
									<label>Loan Amount</label>    
									<input type="text" data-field="LoanAmount" pattern="[0-9]*" class="money" maxlength ="12"/>
								</div>   
							</div>    
						</div>
						<div class="row">
							<div class="col-sm-12">
								<div data-role="fieldcontain">
									<label>Term (months)</label>
									<%If Not String.IsNullOrEmpty(_LoanTermDropdown) Then%>
									 <select data-field="LoanTerm"><%=_LoanTermDropdown%></select>
								   <%Else%>
									<input data-field="LoanTerm" type="text" pattern="[0-9]*" class="numeric" maxlength="3" />
									<%end if %>
								</div>
							</div>    
						</div>
						<div class="row">
							<div class="col-sm-12">
								<div data-role="fieldcontain">
									<label>Interest Rate</label>    
									<input type="text" data-field="InterestRate" pattern="[0-9]*" class="numeric" maxlength ="4" value="<%=_interestRate.ToString("N2", New CultureInfo("en-US")) %>%"/>
								</div>   
							</div>    
						</div>
						<hr/>
						<div class="row">
							<div class="col-sm-12">
								<div data-role="fieldcontain" style="text-align: center;">
									<div>Estimated Payment</div>
									<div style="font-weight: bold; font-size: 1.4em; margin-left: 6px;" data-field="EstimatePayment">$0.00/mo</div>
								</div>   
							</div>    
						</div>

						<br />
						<%--<div class="div-continue">
							<a href="#" type="button" data-role="button" class="div-continue-button">Apply</a>
						</div>--% >
					</div>
				</div>
			</div>
			<%End If%>	
            --%>
            <div id="divDescription" <%=IIf(_LoanPurposeList.Any(), "style='display: none;'", "") %> > 
                <div data-role="fieldcontain">
                    <label for="txtDescription">Description/Reason</label>         
                    <input type ="text"  id ="txtDescription" maxlength = "100" /> 
                </div>
            </div>     
			<uc:xaApplicantQuestion ID="xaApplicationQuestionLoanPage" LoanType="BL" IDPrefix="loanpage_" CQLocation="LoanPage" runat="server"  />
            <div id="disclosure_place_holder"></div>
        </div>
        <div class="div-continue" data-role="footer">
            <a href="#"  data-transition="slide" onclick="validateOtherLoanInfo(this)" type="button" class="div-continue-button">Continue</a>
             <a href="#divErrorDialog" style="display: none;">no text</a>
             <a href="#pagebl_BusinessInfo" style="display: none;">no text</a>
        </div>
       
    </div>
<script>
    function handleHiddenLoanPurpose(dataAttr) {
        //if loan purpose only has one option --> preselected and hide loan purpose
        var loanPurposeElem = $("div[data-section='" + dataAttr + "']").find("a[data-command='" + dataAttr + "']");
        if (loanPurposeElem.length == 1) {
            loanPurposeElem.trigger('click');
            loanPurposeElem.parent().addClass('HiddenElement');
            //update loan purpose
            if (dataAttr == 'loan-purpose') {
                $("#hdLoanPurpose").val(loanPurposeElem.data("key"));
            }
        }
    }

    function setLOC(isLoc) {
        var divLoanPurposeElem = $("div[data-section='loan-purpose']");
        var loanPurposeBtnElem = $("div[data-section='loan-purpose']").find("a[data-command='loan-purpose']");
        if (isLoc.toUpperCase() === "Y") {
            $("#hdIsLineOfCredit").val("Y");
            $('#divLoanTerm').hide();
        } else {
            $("#hdIsLineOfCredit").val("N");
            //reset section loan purpose
           // $("#hdLoanPurpose").val("");
            $("#txtDescription").val("");
     
           // loanPurposeBtnElem.each(function () {
           //     $(this).removeClass("active");
           //     handledBtnHeaderTheme($(this));
         //   });

            if (loanPurposeBtnElem.length > 1) {
                divLoanPurposeElem.show();
            } else {
                handleHiddenLoanPurpose('loan-purpose');     
            }               
            $('#divLoanTerm').show();
        }
        $("#divDescription").hide(); 
    }
    function initOtherLoanInfo() {
        $("#btnLineOfCredit").off("click").on("click", function () {
            var $self = $(this);
            if ($self.attr("contenteditable") == "true") return false;
            if ($("#hdIsLineOfCredit").val() === "N") {
                setLOC("Y");
                $("#divEstimatePayment").hide();
            } else {
                setLOC("N");
                //if ($("#divEstimatePayment").data("show-field")) {
                $("#divEstimatePayment").show();
                //}
            }
            $self.toggleClass("active");
            handledBtnHeaderTheme($(this));
        });
        bindEventLoanPurposeOption();
        //bindEventLOCOption();

        //enable sumbit buttons
        if ($('#pagesubmit .div-continue-button').hasClass('ui-disabled')) {
            $('#pagesubmit .div-continue-button').removeClass('ui-disabled');
        }
        if ($('#walletQuestions .div-continue-button').hasClass('ui-disabled')) {
            $('#walletQuestions .div-continue-button').removeClass('ui-disabled');
        }
        if ($('#co_walletQuestions .div-continue-button').hasClass('ui-disabled')) {
            $('#co_walletQuestions .div-continue-button').removeClass('ui-disabled');
        }

    }
    function bindEventLoanPurposeOption() {
        $("div[data-section='loan-purpose']").find("a[data-command='loan-purpose']").off("click").on("click", function () {
            if ($(this).attr("contenteditable") == "true") return false;
            //first, remove other active button
            $("div[data-section='loan-purpose']").find("a[data-command='loan-purpose']").each(function() {
                $(this).removeClass("active");
                handledBtnHeaderTheme($(this));
            });
            //then, activate selected button
            var $self = $(this);
            $self.addClass("active");
            $("#hdLoanPurpose").val($self.data("key"));
            if ($self.data("key").toString().toUpperCase().indexOf("OTHER") > -1) {
                $("#divDescription").show();
            } else {
                $("#divDescription").hide();
            }
            handledBtnHeaderTheme($self);
            $.lpqValidate.hideValidation("#divLoanPurposePanel");
        });
    }
   /* function bindEventLOCOption() {
        $("div[data-section='line-of-credit']").find("a[data-command='line-of-credit']").off("click").on("click", function () {
            if ($(this).attr("contenteditable") == "true") return false;
            //first, remove other active button
            $("div[data-section='line-of-credit']").find("a[data-command='line-of-credit']").each(function () {
                $(this).removeClass("active");
                handledBtnHeaderTheme($(this));
            });
            //then, activate selected button
            var $self = $(this);
            $self.addClass("active");
            $("#hdLineOfCredit").val($self.data("key"));
            if ($self.data("key").toUpperCase().indexOf("OTHER") > -1) {
                $("#divDescription").show();
            } else {
                $("#divDescription").hide();
            }
            handledBtnHeaderTheme($self);
            $.lpqValidate.hideValidation("#divLoanPurposePanel");
        });
    }*/
    function registerProvideLoanInfoValidator() {
        $('#txtLoanAmount').observer({
            validators: [
                function (partial) {
                    var sValue = $(this).val();
                    if (Common.ValidateText(sValue) == false) {
                        return "Loan Amount is required";
                    } else {
                        if (Common.GetFloatFromMoney(sValue) <= 0) {
                            return "Loan Amount is invalid";
                        }
                    }
                    return "";
                }
            ],
            validateOnBlur: true,
            group: "ValidateProvideLoanInfo"
        });
        $('#txtLoanTerm').observer({
            validators: [
                function (partial) {
                    if ($('#hdIsLineOfCredit').val().toUpperCase() == "Y") return "";

                    //if loanterm is hidden -->no validation
                    if ($('#divLoanTerm').is(":hidden")) return "";

                    var loanTerm = getLoanTerm();
                    if (Common.ValidateText(loanTerm) == false) {
                        return "Loan Term is required";
                    } else {
                        if (isNaN(loanTerm)) {
                            return "Loan Term is invalid";
                        } else {
                            if (Number(loanTerm) <= 0) {
                                return "Loan Term is invalid";
                            }
                        }
                    }
          
                    return "";
                }
            ],
            validateOnBlur: true,
            group: "ValidateProvideLoanInfo"
        });
        $('#ddlLoanTerm').observer({
            validators: [
                function (partial) {
                    if ($('#hdIsLineOfCredit').val().toUpperCase() == "Y") return "";
                    if (Common.ValidateText(getLoanTerm()) == false) {
                        return "Loan Term is required";
                    }
                    return "";
                }
            ],
            validateOnBlur: true,
            group: "ValidateProvideLoanInfo"
        });
      
        $('#divLoanPurposePanel').observer({
            validators: [
                function (partial) {        
                    if ($('#hdHasLoanPurpose').val() == 'Y') {
                        if (!Common.ValidateText($('#hdLoanPurpose').val())) {
                            return 'Loan Purpose is required';
                        }
                    }        
                    return "";
                }
            ],
            validateOnBlur: false,
            group: "ValidateProvideLoanInfo"
        });
    }
    function validateOtherLoanInfo(element) {
        var currElement = $(element);
        if (currElement.attr("contenteditable") == "true") return false;
        var validator = true;
        var loanInfoValidate = $.lpqValidate("ValidateProvideLoanInfo");
        var branchValidate = $.lpqValidate("ValidateBranch");
        if (loanInfoValidate === false || branchValidate === false) {
            validator = false;
		}

		// Validate custom questions, which may be moved to the front page
		if ($('#loanpage_divApplicantQuestion').children().length > 0) {
			if ($.lpqValidate("loanpage_ValidateApplicantQuestionsXA") === false) {
				validator = false;
			}
		}

        if (validator == false) {
            Common.ScrollToError();
        }else{          
        	if (window.ABR) {
        		var formValues = prepareAbrFormValues("otherLoanInfo");
        		if (ABR.FACTORY.evaluateRules("otherLoanInfo", formValues)) {
        			return false;
        		}
        	}
          currElement.next().next().trigger('click');
        }
    }
    function getLoanTerm() {
        var txtLoanTerm = $('#ddlLoanTerm option:selected').val();
        if ($('#txtLoanTerm').length > 0) {
            txtLoanTerm = $('#txtLoanTerm').val();
        }
        return $.trim(txtLoanTerm);
    }
    function ViewOtherLoanInfo() {
        var strHtml = "";
        var showDescription = false;
        strHtml += '<div class="col-sm-6 col-xs-12"><div class="row"><div class="col-xs-6 text-right row-title"><span class="bold">Line of Credit</span></div><div class="col-xs-6 text-left row-data"><span>' + ($("#hdIsLineOfCredit").val().toUpperCase() === "Y" ? "YES" : "NO") + '</span></div></div></div>';
        strHtml += '<div class="col-sm-6 col-xs-12"><div class="row"><div class="col-xs-6 text-right row-title"><span class="bold">Loan Amount</span></div><div class="col-xs-6 text-left row-data"><span>' + $("#txtLoanAmount").val() + '</span></div></div></div>';    
        var hasLoanPurpose = $('#hdHasLoanPurpose').val();
        if (hasLoanPurpose == 'Y') {
            strHtml += '<div class="col-sm-6 col-xs-12"><div class="row"><div class="col-xs-6 text-right row-title"><span class="bold">Loan Purpose</span></div><div class="col-xs-6 text-left row-data"><span>' + _LOANPURPOSE[$('#hdLoanPurpose').val()].Text + '</span></div></div></div>';
            if (_LOANPURPOSE[$('#hdLoanPurpose').val()].Text.indexOf("OTHER") > -1) {
                showDescription = true;
            }
        } else {
            showDescription = true;
        }
        if (getLoanTerm().trim() !== "") {
            strHtml += '<div class="col-sm-6 col-xs-12"><div class="row"><div class="col-xs-6 text-right row-title"><span class="bold">Term</span></div><div class="col-xs-6 text-left row-data"><span>' + getLoanTerm().trim() + ' month(s)</span></div></div></div>';
        }
      
        var ddlBranches = $('#ddlBranches option:selected').val();
        var iBranchVisible = $('#PLBranchesDiv').length;
        if (!Common.ValidateText(ddlBranches) && iBranchVisible != 0) {
            strHtml += '<div class="col-sm-6 col-xs-12"><div class="row"><div class="col-xs-6 text-right row-title"><span class="bold">Branch</span></div><div class="col-xs-6 text-left row-data"><span>' + ddlBranches + '</span></div></div></div>';
        }
        if (showDescription == true) {
            var txtDesc = htmlEncode($("#txtDescription").val().trim());
            if (txtDesc !== "") {
                strHtml += '<div class="col-sm-6 col-xs-12"><div class="row"><div class="col-xs-6 text-right row-title"><span class="bold">Description / Reason</span></div><div class="col-xs-6 text-left row-data"><span>' + txtDesc + '</span></div></div></div>';
            }
        }
        return strHtml;
    }
    function RenderOtherLoanReviewContent() {

        viewSelectedBranch(); //view selected Branch

        $(".ViewOtherLoanInfo").html(ViewOtherLoanInfo.call($(".ViewOtherLoanInfo")));
      
        $("div.review-container div.row-title b").each(function (idx, ele) {
            if (typeof $(ele).data("renameid") == "undefined") {
                var dataId = getDataId();
                $(ele).attr("data-renameid", dataId);
                RENAME_REPOSITORY[dataId] = htmlEncode($(ele).html());
            }
        });
    }
    function getOtherLoanInfo(appInfo, disagree_check) {
        appInfo.PlatformSource = $('#hdPlatformSource').val();
        appInfo.LenderRef = $('#hfLenderRef').val(); 
        if (disagree_check) {
            appInfo.isDisagreeSelect = "Y";
        } else {
            appInfo.isDisagreeSelect = "N";
        }
        /*  if ($('.checkDisclosure').val() != undefined) {
              //get all disclosures 
              var disclosures = "";
              var temp = "";
              $('.checkDisclosure').each(function() {
                  temp = $(this).text().trim().replace(/\s+/g, " ");
                  disclosures += temp + "\n\n";
              });
              appInfo.Disclosure = disclosures;
          }*/
     
        //branchID
        appInfo.BranchID = getBranchId();
        appInfo.LoanOfficerID = $('#hfLoanOfficerID').val();
        appInfo.ReferralSource = $('#hfReferralSource').val();
        appInfo.LoanAmount = Common.GetFloatFromMoney($("#txtLoanAmount").val());
        appInfo.IsLOC = $("#hdIsLineOfCredit").val();
        //appInfo.IsLOC = $('#ddlIsLineOfCredit option:selected').val();
        if (appInfo.IsLOC == 'Y') {
            appInfo.Term = 0;
        } else {       
            appInfo.LoanTerm = getLoanTerm();
        }
        appInfo.LoanPurpose = $("#hdLoanPurpose").val();
        appInfo.LoanTypeCategory = !$.isEmptyObject(_LOANPURPOSE) ? _LOANPURPOSE[$("#hdLoanPurpose").val()].Category : '';

        if ($.trim(appInfo.LoanPurpose) === "") {
            appInfo.Description = $("#txtDescription").val();
        } else if (appInfo.LoanPurpose.toUpperCase().indexOf("OTHER") > -1) {
            appInfo.Description = $("#txtDescription").val();
        } else {
            appInfo.Description = "";
        }
        return appInfo;
    }
    registerProvideLoanInfoValidator();
    initOtherLoanInfo();
    handleShowAndHideBranchNames();
    $(document).on("pageshow", function () {
        var curPageId = $.mobile.pageContainer.pagecontainer('getActivePage').attr('id');
        if (curPageId === "otherLoanInfo") {
        	pushToPagePaths("otherLoanInfo");
            initOtherLoanInfo();
        }
       
    });
</script>