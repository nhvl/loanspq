﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="BusinessLoanSelector.ascx.vb" Inherits="bl_Inc_BusinessLoanSelector" %>
<%@ Import Namespace="LPQMobile.Utils" %>
<div id="BLSelectorPage" data-role="dialog" data-history="false" data-close-btn="none" style="width: 100%;">
	<div data-role="header">
		<button class="header-hidden-btn">.</button>
		<div class="page-header-title">Business Loan</div>
		<div tabindex="0" onclick="BL.FACTORY.closeBLDialog(this, 'mainPage')" class="header_theme2 ui-btn-right" data-role="none">
			<%=HeaderUtils.IconClose %><span style="display: none;">close</span>
		</div>
	</div>
	<div data-role="content">
		<%=HeaderUtils.RenderPageTitle(0, "Select your loan type", True)%>
		<div data-role="fieldcontain" id="divBLLoanTypes">
            <%If EnableCreditCardLoan Then %>
                <div id="divCreditCardLoan" <%=IIf(IsInMode("777") AndAlso IsInFeature("visibility"), "class=""showfield-section"" data-show-field-section-id=""" & BuildShowFieldSectionID("divCreditCardLoan") & """ data-default-state=""on"" data-section-name= 'Credit Card Loan'", "")%>>     
                   <a href="#" data-role="button" class="btn-header-theme" data-key="card">Credit Card</a>       
                </div>
            <%End if %>
            <%If EnableVehicleLoan Then  %>
                <div id="divVehicleLoan" <%=IIf(IsInMode("777") AndAlso IsInFeature("visibility"), "class=""showfield-section"" data-show-field-section-id=""" & BuildShowFieldSectionID("divVehicleLoan") & """ data-default-state=""on"" data-section-name= 'Vehicle Loan'", "")%>>
                    <a href="#" data-role="button" class="btn-header-theme" data-key="vehicle">Vehicle</a>    
                </div>
            <%End if %>
            <%If EnableOtherLoan Then %>
                <div id="divOtherLoan" <%=IIf(IsInMode("777") AndAlso IsInFeature("visibility"), "class=""showfield-section"" data-show-field-section-id=""" & BuildShowFieldSectionID("divOtherLoan") & """ data-default-state=""on"" data-section-name= 'Other Loan'", "")%>>
                    <a href="#" data-role="button" class="btn-header-theme" data-key="other">Fixed and Line of Credit</a>    
                </div>
            <%End if %>
		</div>
		<div class="account-role-list-wrapper">
			<%=HeaderUtils.RenderPageTitle(0, "Add your applicant roles", True)%>
            <%If EnableAppRole_P Then %>
                <div id="divAppRole_P" <%=IIf(IsInMode("777") AndAlso IsInFeature("visibility"), "class=""showfield-section"" data-show-field-section-id=""" & BuildShowFieldSectionID("divAppRole_P") & """ data-default-state=""on"" data-section-name= '" + ApplicantRoles.Item("P") + "'", "")%>>  		
					<div class="ui-btn ui-corner-all btn-header-theme-ext bl-role-item">
                        <span class="require-ico"><%=ApplicantRoles.Item("P")%></span>
					    <div class="btn-add" data-role-type="P" onclick="BL.FACTORY.openBLEditorDialog(false, this)" tabindex="0"><%=HeaderUtils.IconClose %></div>		            
                    </div>
                </div>
            <%End if %>
            <%If EnableAppRole_C Then %>
               <div id="divAppRole_C" <%=IIf(IsInMode("777") AndAlso IsInFeature("visibility"), "class=""showfield-section"" data-show-field-section-id=""" & BuildShowFieldSectionID("divAppRole_C") & """ data-default-state=""on"" data-section-name= '" + ApplicantRoles.Item("C") + "'", "")%>>  							
			       <div class="ui-btn ui-corner-all btn-header-theme-ext bl-role-item">
                       <span><%=ApplicantRoles.Item("C")%></span>
                      <div class="btn-add" data-role-type="C" onclick="BL.FACTORY.openBLEditorDialog(false, this)" tabindex="0"><%=HeaderUtils.IconClose %></div>		  
                   </div> 
               </div>
            <%End if %>
            <%If EnableAppRole_S Then %>
               <div id="divAppRole_S" <%=IIf(IsInMode("777") AndAlso IsInFeature("visibility"), "class=""showfield-section"" data-show-field-section-id=""" & BuildShowFieldSectionID("divAppRole_S") & """ data-default-state=""on"" data-section-name= '" + ApplicantRoles.Item("S") + "'", "")%>>  							
		           <div class="ui-btn ui-corner-all btn-header-theme-ext bl-role-item">
                       <span><%=ApplicantRoles.Item("S")%></span>
                      <div class="btn-add" data-role-type="S" onclick="BL.FACTORY.openBLEditorDialog(false, this)" tabindex="0"><%=HeaderUtils.IconClose %></div>		  
                   </div> 
               </div>
            <%End if %>
            <%If EnableAppRole_G Then %>
               <div id="divAppRole_G" <%=IIf(IsInMode("777") AndAlso IsInFeature("visibility"), "class=""showfield-section"" data-show-field-section-id=""" & BuildShowFieldSectionID("divAppRole_G") & """ data-default-state=""on"" data-section-name= '" + ApplicantRoles.Item("G") + "'", "")%>>  							
			       <div class="ui-btn ui-corner-all btn-header-theme-ext bl-role-item">
                       <span><%=ApplicantRoles.Item("G")%></span>
                      <div class="btn-add" data-role-type="G" onclick="BL.FACTORY.openBLEditorDialog(false, this)" tabindex="0"><%=HeaderUtils.IconClose %></div>		       
                   </div>
               </div>
            <%End if %>        
		   
			<div style="text-align: center;"><span class='require-span'>*</span>Required Roles</div>
			<%--<div class="ui-btn ui-corner-all btn-header-theme-ext">
				<span class="require-ico">Role A</span>
				<div class="btn-add" tabindex="0"><i class="fa fa-plus-circle"></i></div>
			</div>
			<div class="ui-btn ui-corner-all btn-header-theme-ext">
				<span class="require-ico">Role A</span>
				<div class="btn-add" tabindex="0"><i class="fa fa-plus-circle"></i></div>
			</div>
			<div class="ui-btn ui-corner-all btn-header-theme-ext">
				<span class="require-ico">Role A</span>
				<div class="btn-add" tabindex="0"><i class="fa fa-plus-circle"></i></div>
			</div>--%>
			
		</div>
		<div class="selected-roles-wrapper hidden">
			<div><%=HeaderUtils.RenderPageTitle(0, "Your applicants", True)%></div>
			<div id="divSelectedBLRoles">
				<%--<div class="account-role-item-wrapper">
					<div class="ui-btn ui-corner-all account-role-item"><span title="(Primary Applicant)">Role A</span></div>
					<div class="ui-btn ui-corner-all role-attr-item">
						<label class="custom-checkbox">
							<input type="checkbox" data-role="none"/>	
							<span></span>
						</label>
						<label for="">Add joint applicant</label>
					</div>
					<div class="btn-remove" tabindex="0"></div>
				</div>
				<div class="account-role-item-wrapper">
					<div class="ui-btn ui-corner-all account-role-item"><span>Role A (Primary Applicant)</span></div>
					<div class="ui-btn ui-corner-all role-attr-item">
						<label class="custom-checkbox">
							<input type="checkbox" data-role="none"/>	
							<span></span>
						</label>
						<label for="">Add joint applicant</label>
					</div>
					<div class="ui-btn ui-corner-all role-attr-item">
						<label class="custom-checkbox">
							<input type="checkbox" data-role="none"/>	
							<span></span>
						</label>
						<label for="">Add joint applicant</label>
					</div>
					<div class="btn-remove" tabindex="0"></div>
				</div>
				<div class="account-role-item-wrapper">
					<div class="ui-btn ui-corner-all account-role-item"><span>Role A (Primary Applicant)</span></div>
					<div class="btn-remove" tabindex="0"></div>
				</div>--%>
			</div> 
			
		</div>
		<div class ="div-continue">
			<form id="frmBLSelector" action="/bl/BusinessLoan.aspx" method="GET">
				<input type="hidden" name="lenderref" value="<%=LenderRef%>"/>
				<input type="hidden" name="type" value=""/>
				<input type="hidden" name="loantype" value=""/>
				<%For Each item In ParseParams(DefaultParams)%>
				<input type="hidden" name="<%=item.Key%>" value="<%=item.Value%>"/>
				<%Next%>
			</form>
			<a href="#"  data-transition="slide" onclick="BL.FACTORY.validate()" type="button" class="div-continue-button ui-btn">Continue</a> 			
			<span>Or</span> <a href="#" class ="div-goback ui-btn" onclick="BL.FACTORY.closeBLDialog(this, 'mainPage')" ><span class="hover-goback"> Go Back</span></a>   
		</div>    
	</div>
</div>
<div id="BLEditorDialog" data-role="dialog" data-history="false" data-close-btn="none" class="account-editor-dialog">
	<div data-role="header">
		<button class="header-hidden-btn">.</button>
		<div class="page-header-title"><span class="edit-label">Edit</span><span>Add</span> Applicant</div>
		<div tabindex="0" onclick="BL.FACTORY.closeBLDialog(this, 'BLSelectorPage')" class="header_theme2 ui-btn-right" data-role="none">
			<%=HeaderUtils.IconClose %><span style="display: none;">close</span>
		</div>
	</div>
	<div data-role="content">
		<%=HeaderUtils.RenderPageTitle(0, "Applicant", True)%>
		<div data-role="fieldcontain">
			<label for="txtNewBLFName" class="RequiredIcon">First Name</label>
			<input type="text" id="txtNewBLFName" maxlength="50"/>
		</div>
		<div data-role="fieldcontain">
			<label for="txtNewBLLName" class="RequiredIcon">Last Name</label>
			<input type="text" id="txtNewBLLName" maxlength="50"/>
		</div>
		<div data-role="fieldcontain">
			<label for="ddlNewBLRole" class="RequiredIcon">Applicant Type</label>
			<select id="ddlNewBLRole"></select>
		</div>
		<div class="has-joint-button">
			<a id="lnkBLHasJointApp" href="#" data-mode="self-handle-event" style="cursor: pointer; font-weight: bold;" class="header_theme2 ui-link chevron-circle-right-before"><span class="add-label">Add</span><span>Remove</span> Joint Applicant</a>
		</div>
		<div>
			<%=HeaderUtils.RenderPageTitle(0, "Joint Applicant", True)%>
			<div data-role="fieldcontain">
				<label for="txtNewBLJointFName" class="RequiredIcon">First Name</label>
				<input type="text" id="txtNewBLJointFName" maxlength="50"/>
			</div>
			<div data-role="fieldcontain">
				<label for="txtNewBLJointLName" class="RequiredIcon">Last Name</label>
				<input type="text" id="txtNewBLJointLName" maxlength="50"/>
			</div>
			<div data-role="fieldcontain">
				<label for="ddlNewBLJointRole" class="RequiredIcon">Applicant Type</label>
				<select id="ddlNewBLJointRole">
                    <%=BLJointAppRoleOptions %>                  
				</select>
			</div>
		</div>
		<div style="text-align: center; margin-top: 10px;"><span class='require-span'>*</span>Required</div>
		<div class ="div-continue row">
			<div class="col-xs-6">
				<a tabindex="0" href="#" type="button" class="div-continue-button ui-btn cancel-btn" onclick="BL.FACTORY.closeBLEditorDialog()">Cancel</a>   
			</div>
			<div class="col-xs-6">
				<a tabindex="0" href="#" data-transition="slide" type="button" class="div-continue-button ui-btn" onclick="BL.FACTORY.submitBLEditorDialog()"><span class="edit-label">Done</span><span>Add</span></a> 
			</div>
		</div>    
	</div>
</div>
<script type="text/javascript">
	(function (BL, $, undefined) {
        var APPLICANTROLES = <%=Newtonsoft.Json.JsonConvert.SerializeObject(ApplicantRoles)%>;
        var BLAPPROLES = <%=Newtonsoft.Json.JsonConvert.SerializeObject(BLAppRoles)%>;
		BL.DATA = {};
		BL.FACTORY = {};
        BL.FACTORY.openBusinessDialog = function (type) {
			//$("#frmBLSelector input[name='type']").val(type);
			$.mobile.changePage("#BLSelectorPage", {
				transition: "fade",
				reverse: false,
				changeHash: false
			});
		};
		BL.FACTORY.closeBLDialog = function(sourceEle, destPage) {
			$("#frmBLSelector input[name='type']").val("");
			if ($("#" + destPage).length == 0) destPage = 'mainPage';
			$.mobile.changePage("#" + destPage, {
				transition: "fade",
				reverse: false,
				changeHash: false
			});
		}
		function getSelectedLoanType() {
			return $("#divBLLoanTypes").find("a[data-key].active").data("key");
		}
		BL.FACTORY.openBLEditorDialog = function(isEdit, sourceEle) {
			cleanUpBLEditorDialog();
			var $ddlNewBLRole = $("#ddlNewBLRole");
            var $sourceEle = $(sourceEle);            
			$("#BLEditorDialog").data("source-id", $sourceEle.attr("id"));
            $ddlNewBLRole.html("");
            //display bl role options if the roles are available in BLAPPROLES
			$("<option/>", { "value": "" }).text("--Please select--").appendTo($ddlNewBLRole);
			if ($sourceEle.data("role-type") == "P") {
                $("<option/>", { "value": "P" }).text("Primary").appendTo($ddlNewBLRole);
                if ($.inArray("G", BLAPPROLES) > -1) {
                    $("<option/>", { "value": "G" }).text("Guarantor").appendTo($ddlNewBLRole);
                }           		
            } else {
                if ($.inArray("C", BLAPPROLES) > -1) {
                    $("<option/>", { "value": "C" }).text("Co-Borrower").appendTo($ddlNewBLRole);
                }
                if ($.inArray("S", BLAPPROLES) > -1) {		
                    $("<option/>", { "value": "S" }).text("Co-Signer").appendTo($ddlNewBLRole);
                }
                if ($.inArray("G", BLAPPROLES) > -1) {
                    $("<option/>", { "value": "G" }).text("Guarantor").appendTo($ddlNewBLRole);
                }							
			}
			if (isEdit) {
				$("#BLEditorDialog").addClass("edit-mod");
				var info = collectSelectedRoleInfo($sourceEle);
				$("#txtNewBLFName").val(info.FirstName);
				$("#txtNewBLLName").val(info.LastName);
				if (info.HasJoint || $(this).is(":checkbox")) {
					$("#txtNewBLJointFName").val(info.JointFirstName);
					$("#txtNewBLJointLName").val(info.JointLastName);
					$("#ddlNewBLJointRole").val(info.JointRoleType);
					if ($("#ddlNewBLJointRole").data("mobile-selectmenu") === undefined) {
						$("#ddlNewBLJointRole").selectmenu(); //not initialized yet, lets do it
					}
					$("#ddlNewBLJointRole").selectmenu("refresh");
					$(".has-joint-button", $("#BLEditorDialog")).addClass("show-joint");
				} else {
					$(".has-joint-button", $("#BLEditorDialog")).removeClass("show-joint");
				}
				$("#ddlNewBLRole").val(info.RoleType);
				if ($("#ddlNewBLRole").data("mobile-selectmenu") === undefined) {
					$("#ddlNewBLRole").selectmenu(); //not initialized yet, lets do it
				}
				$("#ddlNewBLRole").selectmenu("refresh");
			} else {
				if ($sourceEle.hasClass("btn-add") && $sourceEle.data("role-type") == "P" && countPrimaryRole() > 0) {
					$.lpqValidate.showValidation($("#divSelectedBLRoles .account-role-item-wrapper[data-role-type='P']"), "The primary applicant is already selected.");
					setTimeout(function() {
						$.lpqValidate.hideValidation($("#divSelectedBLRoles .account-role-item-wrapper[data-role-type='P']"));
					}, 1500);
					return false;
				}else if (countSelectedApplicants() >= 9) {
					$.lpqValidate.showValidation($("#divSelectedBLRoles"), "Allow maximum 9 applicants.");
					setTimeout(function() {
						$.lpqValidate.hideValidation($("#divSelectedBLRoles"));
					}, 1500);
					return false;
				}
				
				$ddlNewBLRole.val($sourceEle.data("role-type"));
				if ($ddlNewBLRole.data("mobile-selectmenu") === undefined) {
					$ddlNewBLRole.selectmenu(); //not initialized yet, lets do it
				}
				$ddlNewBLRole.selectmenu("refresh");
				$(".has-joint-button", $("#BLEditorDialog")).removeClass("show-joint");
				$("#BLEditorDialog").removeClass("edit-mod");
			}
			$.mobile.changePage("#BLEditorDialog", {
				transition: "fade",
				reverse: false,
				changeHash: false
			});
		};
		BL.FACTORY.closeBLEditorDialog = function() {
			$.mobile.changePage("#BLSelectorPage", {
				transition: "fade",
				reverse: false,
				changeHash: false
			});
		}
		BL.FACTORY.submitBLEditorDialog = function() {
			var $container = $("#BLEditorDialog");
			if ($.lpqValidate("ValidateBLEditor") == false) return false;
			var firstName = $("#txtNewBLFName").val();
			var lastName = $("#txtNewBLLName").val();
			var hasJoint = $(".has-joint-button", $("#BLEditorDialog")).hasClass("show-joint") && $(".has-joint-button", $("#BLEditorDialog")).is(":visible");
			var jointFirstName = $("#txtNewBLJointFName").val();
			var jointLastName = $("#txtNewBLJointLName").val();
			var jointRole = $("#ddlNewBLJointRole").val();
			if ($container.hasClass("edit-mod")) {
				var $sourceEle = $("#" + $("#BLEditorDialog").data("source-id"));
				$sourceEle.replaceWith(buildSelectedRoleBlock($("#ddlNewBLRole").val(), firstName, lastName, hasJoint, jointFirstName, jointLastName, jointRole));
			}else {
				if ($("#ddlNewBLRole").val() == "P") {
					$("#divSelectedBLRoles").prepend(buildSelectedRoleBlock($("#ddlNewBLRole").val(), firstName, lastName, hasJoint, jointFirstName, jointLastName, jointRole));
				} else {
					$("#divSelectedBLRoles").append(buildSelectedRoleBlock($("#ddlNewBLRole").val(), firstName, lastName, hasJoint, jointFirstName, jointLastName, jointRole));
				}
			}
			$("#divSelectedBLRoles").trigger("change");
			$("#divAppRole_P").trigger("change");
			$(".account-role-list-wrapper .bl-role-item:visible", "#BLSelectorPage").each(function(idx, ele) {
				$.lpqValidate.hideValidation($(ele));
			});
			$.mobile.changePage("#BLSelectorPage", {
				transition: "fade",
				reverse: false,
				changeHash: false
			});
		}
		function cleanUpBLEditorDialog() {
			$("#txtNewBLFName").val("");
			$("#txtNewBLLName").val("");
			$(".has-joint-button", $("#BLEditorDialog")).removeClass("show-joint");
			$("#txtNewBLJointFName").val("");
			$("#txtNewBLJointLName").val("");
			$("#ddlNewBLRole").val("");
			if ($("#ddlNewBLRole").data("mobile-selectmenu") === undefined) {
				$("#ddlNewBLRole").selectmenu(); //not initialized yet, lets do it
			}
			$("#ddlNewBLRole").selectmenu("refresh");
			$("#ddlNewBLJointRole").val("");
			if ($("#ddlNewBLJointRole").data("mobile-selectmenu") === undefined) {
				$("#ddlNewBLJointRole").selectmenu(); //not initialized yet, lets do it
			}
			$("#ddlNewBLJointRole").selectmenu("refresh");
			$("#BLEditorDialog").data("source-id", "");
			$.lpqValidate.hideValidation($("#ddlNewBLRole"));
			$.lpqValidate.hideValidation($("#txtNewBLFName"));
			$.lpqValidate.hideValidation($("#txtNewBLLName"));
			$.lpqValidate.hideValidation($("#txtNewBLJointFName"));
			$.lpqValidate.hideValidation($("#txtNewBLJointLName"));
			$.lpqValidate.hideValidation($("#ddlNewBLJointRole"));
			$.lpqValidate.hideValidation($("#lnkBLHasJointApp"));
			
		}
		
		function collectSelectedRoleInfo(container) {
			var result = {};
			var $container = $(container);
			result.FirstName = $("span[data-name='firstName']", $container).text();
			result.LastName = $("span[data-name='lastName']", $container).text();
			result.RoleType = $("input[data-name='roleType']", $container).val();
			result.JointFirstName = "";
			result.JointLastName = "";
			result.JointRoleType = "";
			result.HasJoint = false;
			if ($("input:checkbox", $container).is(":checked")) {
				result.HasJoint = true;
				result.JointFirstName = $("span[data-name='jointFirstName']", $container).text();
				result.JointLastName = $("span[data-name='jointLastName']", $container).text();
				result.JointRoleType = $("input[data-name='jointRoleType']", $container).val();
			}
			return result;
		}
		function buildSelectedRoleBlock(role, firstName, lastName, hasJoint, jointFirstName, jointLastName, jointRole) {
			var blockId = "div_" + Math.random().toString(36).substr(2, 16);
			var $item = $("<div/>", { "class": "account-role-item-wrapper", "data-role-type": role, "id": blockId });
			var $name = $("<div/>", { "class": "ui-btn ui-corner-all account-role-item" });
			$name.html("<span title=' ("+ APPLICANTROLES[role] +")'>" + "<span data-name='firstName'>" + firstName + "</span>" + " " + "<span data-name='lastName'>" + lastName + "</span>" + "</span><input type='hidden' data-name='roleType' value='"+ role +"'/>");
			$item.append($name);
			var $addJointChk = $("<div/>", { "class": "ui-btn ui-corner-all role-attr-item" });
			var randomId = "chk_" + Math.random().toString(36).substr(2, 16);
			if (hasJoint == true) {
				$addJointChk.html('<label class="custom-checkbox"><input checked="checked" type="checkbox" id="' + randomId + '" data-role="none"/><span></span></label><label for="' + randomId + '">' + "<span data-name='jointFirstName'>" + jointFirstName + "</span>" + " " + "<span data-name='jointLastName'>" + jointLastName + "</span>" + " (" + APPLICANTROLES[jointRole] + ")<input type='hidden' data-name='jointRoleType' value='"+ jointRole +"'/>"  + '</label>');	
				$addJointChk.find("input:checkbox").on("change", function() {
					$addJointChk.html('<label class="custom-checkbox"><input type="checkbox" id="' + randomId + '" data-role="none"/><span></span></label><label for="' + randomId + '">Add joint applicant</label>');
					$addJointChk.find("input:checkbox").off("change").on("change", function(e) {
						$(this).prop("checked", false);
						BL.FACTORY.openBLEditorDialog.call(this, true, $(this).closest(".account-role-item-wrapper"));
					});
				});
			} else {
				$addJointChk.html('<label class="custom-checkbox"><input type="checkbox" id="' + randomId + '" data-role="none"/><span></span></label><label for="' + randomId + '">Add joint applicant</label>');
				$addJointChk.find("input:checkbox").on("change", function(e) {
					$(this).prop("checked", false);
					BL.FACTORY.openBLEditorDialog.call(this, true, $(this).closest(".account-role-item-wrapper"));
				});
			}
			$item.append($addJointChk);
			var $removeBtn = $("<div/>", { "class": "btn-remove", "tabindex": 0 }).appendTo($item);
			$removeBtn.on("click", function() {
				$(this).closest("div.account-role-item-wrapper").remove();
				$("#divSelectedBLRoles").trigger("change");
			});
			var $editBtn = $("<div/>", { "class": "btn-edit", "tabindex": 0 }).appendTo($item);
			$editBtn.on("click", function() {
				BL.FACTORY.openBLEditorDialog(true, $(this).closest(".account-role-item-wrapper"));
			});
			return $item;
		}
		BL.FACTORY.validate = function() {
			if ($.lpqValidate("ValidateBLAccountSelector") == false) {
				return false;
			}
			var roleData = [];
			var roleDataWithoutName = [];
            $(".account-role-item-wrapper[data-role-type]", "#divSelectedBLRoles").each(function (idx, item) {       
				var roleItem = collectSelectedRoleInfo(item);
				roleData.push({fname: roleItem.FirstName, lname: roleItem.LastName, isjoint: false, roletype: roleItem.RoleType});
				roleDataWithoutName.push({isjoint: false, roletype: roleItem.RoleType});
				if (roleItem.HasJoint == true) {
					roleData.push({fname: roleItem.JointFirstName, lname: roleItem.JointLastName, isjoint: true, roletype: roleItem.JointRoleType});
					roleDataWithoutName.push({isjoint: true, roletype: roleItem.JointRoleType});
				}
			});

			var $frmBLSelector = $("#frmBLSelector");
			$frmBLSelector.find("input:hidden[name='loantype']").remove();
			$("<input/>", { "type": "hidden", "name": "loantype" }).val(encodeURIComponent(getSelectedLoanType().toLowerCase())).appendTo($frmBLSelector);
			if (roleData.length > 0) {
				var jsonStr = encodeURIComponent(JSON.stringify(roleDataWithoutName));
				<%--fallback, because max url length is about 2000 character, we cannot pass role data with names via query string. Solution is, we store it in current session. But user can save url and then visit later(in same browser) when session is expired, so the below is for that situation, main app will run properly but without names prefilled --%>
				$frmBLSelector.find("input:hidden[name='roleinfo']").remove();
                $("<input/>", { "type": "hidden", "name": "roleinfo" }).val(jsonStr).appendTo($frmBLSelector);           
				$.ajax({
					url: '<%=String.Format("{0}{1}", "/handler/chandler.aspx", Common.SafeEncodeString(Request.Url.Query))%>',
					async: true,
					cache: false,
					type: 'POST',
					dataType: 'html',
					data: {
						command: 'PersistBLRoleInfo',
						roleInfo: JSON.stringify(roleData)
					},
					success: function(responseText) {
						$frmBLSelector.find("input:hidden[name='roleinfotoken']").remove();
						$("<input/>", { "type": "hidden", "name": "roleinfotoken" }).val(responseText).appendTo($frmBLSelector);
						<%If IsInMode("777") Then%>
						$frmBLSelector.find("input:hidden[name^='xdm_']").remove();
						rpc.submitForm("/bl/BusinessLoan.aspx?" + $frmBLSelector.serialize());
						<%Else%>
						$frmBLSelector[0].submit();
						<%End If%>
					},
					error: function() {
						<%If IsInMode("777") then%>
						$frmBLSelector.find("input:hidden[name^='xdm_']").remove();
						rpc.submitForm("/bl/BusinessLoan.aspx?" + $frmBLSelector.serialize());
						<%Else%>
						$frmBLSelector[0].submit();
						<%End If%>
					}
				});
			} else if($(".account-role-list-wrapper .bl-role-item:visible", "#BLSelectorPage").length > 0){
				
				$.lpqValidate.showValidation($(".account-role-list-wrapper>.sub_section_header", "#BLSelectorPage"), "Please select at least one applicant.");
				setTimeout(function() {
					$.lpqValidate.showValidation($(".account-role-list-wrapper>.sub_section_header", "#BLSelectorPage"));
				}, 2000);
			}
		}
		function countPrimaryRole() {
			//todo: 
			var primaryRoleCount = 0;
			//1. primary role is required and maximum instance is 1
			$(".account-role-item-wrapper[data-role-type]", "#divSelectedBLRoles").each(function(idx, item) {
				var roleItem = collectSelectedRoleInfo(item);
				if (roleItem.RoleType == "P") primaryRoleCount += 1;
			});
			return primaryRoleCount;
		}
		function countSelectedApplicants() {
			var appCount = 0;
			//1. primary role is required and maximum instance is 1
			$(".account-role-item-wrapper[data-role-type]", "#divSelectedBLRoles").each(function(idx, item) {
				appCount += 1;
				var roleItem = collectSelectedRoleInfo(item);
				if(roleItem.HasJoint) appCount += 1;
			});
			return appCount;
		}
		BL.FACTORY.init = function() {
			$("#divSelectedBLRoles").on("change", function() {
				if ($(this).find(".account-role-item-wrapper").length > 0) {
					$(this).closest(".selected-roles-wrapper").removeClass("hidden");
				} else {
					$(this).closest(".selected-roles-wrapper").addClass("hidden");
				}
			});
			registerBLValidator();
			
			$("#BLSelectorPage").on("pagehide", function (event, ui) {
				$(".account-role-list-wrapper .bl-role-item:visible", "#BLSelectorPage").each(function(idx, ele) {
					$.lpqValidate.hideValidation($(ele));
				});
			});

			$('#lnkBLHasJointApp').on('click', function (e) {
				var $self = $(this);
				if ($self.closest("div.has-joint-button").hasClass("show-joint")) {
					$self.addClass("chevron-circle-right-before").removeClass("chevron-circle-down-before");
					$.lpqValidate.hideValidation($('#txtNewBLJointFName'));
					$.lpqValidate.hideValidation($('#txtNewBLJointLName'));
					$.lpqValidate.hideValidation($('#ddlNewBLJointRole'));
					$.lpqValidate.hideValidation($("#lnkBLHasJointApp"));
				} else {
					$self.addClass("chevron-circle-down-before").removeClass("chevron-circle-right-before");
				}
				$self.closest(".has-joint-button").toggleClass("show-joint");
				if (BUTTONLABELLIST != null) {
					var value = BUTTONLABELLIST[$.trim($self.html()).toLowerCase()];
					if (typeof value == "string" && $.trim(value) !== "") {
						$self.html(value);
					}
				}
				e.preventDefault();
			});
			$("#divBLLoanTypes").find("a[data-key]").on("mouseenter", function (ev) {
				var element = $(this);
				element.addClass('btnHover');
				element.removeClass('btnActive_on');
				element.removeClass('btnActive_off');
			}); 
			$("#divBLLoanTypes").find("a[data-key]").on("mouseleave", function () {
				var element = $(this);
				element.removeClass('btnHover');
				if (element.hasClass('active')) {
					element.addClass('btnActive_on');
					element.removeClass('btnActive_off');
				} else {
					element.removeClass('btnActive_on');
					element.addClass('btnActive_off');
				}
			});
			$("#divBLLoanTypes").find("a[data-key]").off("click").on("click", function () {
				var $self = $(this);
				$("#divBLLoanTypes").find("a[data-key]").removeClass("active").removeClass("btnActive_on");
				$self.addClass("active");
				$("#frmBLSelector input[name='loantype']").val($self.data("key"));
				$("#divBLLoanTypes").trigger("change");
			});
		}
		
		function registerBLValidator() {
			$("#divBLLoanTypes").observer({
				validators: [
					function(partial) {
						if ($("#frmBLSelector input[name='loantype']").val() === "") {
							return "Please select loan type";
						}
						return "";
					}
				],
				validateOnBlur: true,
				group: "ValidateBLAccountSelector"
			});
			$("#divSelectedBLRoles").observer({
				validators: [
					function(partial) {
						if (countSelectedApplicants() > 9) {
							return "Allow maximum 9 applicants.";
						}
						return "";
					}
				],
				validateOnBlur: true,
				group: "ValidateBLAccountSelector"
			});
			$("#divAppRole_P").observer({
				validators: [
					function(partial) {
						if (countPrimaryRole()  == 0) {
							return "The primary role is required.";
						}
						return "";
					}
				],
				validateOnBlur: true,
				group: "ValidateBLAccountSelector",
				groupType: "complexUI"
            });
         
			$("#txtNewBLFName").observer({
				validators: [
                    function (partial) {
                        var text = $(this).val();
					    if ($.trim(text) =="") {
						    return 'First Name is required';
					    } else if (/^[\sa-zA-Z\'-]+$/.test(text) == false) {
						    return 'Enter a valid first name. Allowed characters: letters "A-Z", dashes, apostrophes and spaces.';
					    }
					    return "";					
					}
				],
				validateOnBlur: true,
				group: "ValidateBLEditor"
			});
			$("#txtNewBLLName").observer({
				validators: [
					function(partial) {
						var text = $(this).val();
					    if ($.trim(text) =="") {
						    return 'Last Name is required';
					    } else if (/^[\sa-zA-Z\'-]+$/.test(text) == false) {
						    return 'Enter a valid last name. Allowed characters: letters "A-Z", dashes, apostrophes and spaces.';
					    }
					    return "";	
					}
				],
				validateOnBlur: true,
				group: "ValidateBLEditor"
			});
			$("#ddlNewBLRole").observer({
				validators: [
					function(partial) {
						if ($(this).val() == "") return "Please select applicant type";
						return "";
					}
				],
				validateOnBlur: true,
				group: "ValidateBLEditor"
			});
			$("#txtNewBLJointFName").observer({
				validators: [
                    function (partial) {
                        var $self = $(this);
                        if ($self.is(":visible")) {
                            if ($.trim($self.val()) == "") {
						        return 'First Name is required';
					        } else if (/^[\sa-zA-Z\'-]+$/.test($self.val()) == false) {
						        return 'Enter a valid first name. Allowed characters: letters "A-Z", dashes, apostrophes and spaces.';
					        }
                        }						
						return "";
					}
				],
				validateOnBlur: true,
				group: "ValidateBLEditor"
			});
			$("#txtNewBLJointLName").observer({
				validators: [
					function(partial) {
						var $self = $(this);
                        if ($self.is(":visible")) {
                            if ($.trim($self.val()) == "") {
						        return 'Last Name is required';
					        } else if (/^[\sa-zA-Z\'-]+$/.test($self.val()) == false) {
						        return 'Enter a valid Last name. Allowed characters: letters "A-Z", dashes, apostrophes and spaces.';
					        }
                        }	
						return "";
					}
				],
				validateOnBlur: true,
				group: "ValidateBLEditor"
			});
			$("#ddlNewBLJointRole").observer({
				validators: [
					function(partial) {
						if ($(this).is(":visible") && $(this).val() == "") return "Please select applicant type";
						return "";
					}
				],
				validateOnBlur: true,
				group: "ValidateBLEditor"
			});
		}
	}(window.BL = window.BL || {}, jQuery));
$(function() {
	BL.FACTORY.init();
});
</script>