﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="FinancialInfo.ascx.vb" Inherits="bl_Inc_FinancialInfo" %>
<%@ Import Namespace="Newtonsoft.Json" %>
<div>
   <%=HeaderUtils.RenderPageTitle(0, "Financial Information", True)%>
</div>
<%-- 
<div data-role="fieldcontain">
    <label for="<%=IDPrefix%>txtGrossMonthlyIncome">Gross Monthly Income (before taxes)<span id='<%=IDPrefix%>redStarGrossIncome' class="require-span">*</span></label>
    <input type="<%=TextAndroidTel%>" pattern="[0-9]*" id="<%=IDPrefix%>txtGrossMonthlyIncome" class="money" maxlength ="10"/>
</div>
    --%>
<!--gross Monthly income tax exempt -->
<div data-role="fieldcontain" style="display:none">
    <label for="<%=IDPrefix%>grossMonthlyIncomeTaxExempt">Gross Monthly Income Tax Exempt?</label>
    <input type="checkbox" id="<%=IDPrefix%>grossMonthlyIncomeTaxExempt" />
</div>

<!--other monthly income -->
 <div id="<%=IDPrefix%>divOtherMonthlyIncome">
     <div style="margin-top: 10px;">
        <a id="<%=IDPrefix%>lnkHasOtherMonthlyIncome" href="#" data-mode="self-handle-event" status="N" style="cursor: pointer;font-weight :bold" class="header_theme2 shadow-btn plus-circle-before">Add other income</a>
    </div>
</div>
<div id="<%=IDPrefix%>divOtherMonthlyIncomeForm" style="display :none">
    <div data-role="fieldcontain">
        <label for="<%=IDPrefix%>txtOtherMonthlyIncome">Other Monthly Income</label>
        <input type="<%=TextAndroidTel%>" pattern="[0-9]*" class="money" maxlength="10" id="<%=IDPrefix%>txtOtherMonthlyIncome" />
    </div>
    <!--other Monthly income tax exempt -->
   <div data-role="fieldcontain">
    <label for="<%=IDPrefix%>otherMonthlyIncomeTaxExempt">Other Monthly Income Tax Exempt?</label>
    <input type="checkbox" id="<%=IDPrefix%>otherMonthlyIncomeTaxExempt" />
   </div>
    <div data-role="fieldcontain">
        <label for="<%=IDPrefix%>txtOtherMonthlyIncomeDesc">Other Monthly Income Description</label>
        <input type="text" maxlength="150" id="<%=IDPrefix%>txtOtherMonthlyIncomeDesc" />
    </div>
	<div>
		<label><i><sup>§</sup>Alimony, child support and separate maintenance income need not be revealed if you do not wish to have them considered as a basis for repaying this obligation.</i></label>
	</div>
</div>
<br />
<!--end other monthly income -->
<div data-role="fieldcontain">
    <label for="<%=IDPrefix%>txtTotalMonthlyHousingExpense" id="<%=IDPrefix%>lblTotalMonthlyHousingExpense">Monthly Mortgage/Rent Payment</label>
    <input type="<%=TextAndroidTel%>" pattern="[0-9]*" id="<%=IDPrefix%>txtTotalMonthlyHousingExpense" class="money" maxlength ="10" />
</div>

<script type="text/javascript">
   
    function <%=IDPrefix%>ValidateApplicantFinancialInfo() {
            return  $.lpqValidate("<%=IDPrefix%>ValidateFinancialInfo");
	}

	function <%=IDPrefix%>registerFinancialInfoValidator() {
       
        $('#<%=IDPrefix%>txtOtherMonthlyIncomeDesc').observer({
	        validators: [
				function (partial) {
				    var hasOtherMonthlyIncome = $('#<%=IDPrefix%>lnkHasOtherMonthlyIncome').attr('status');
				    var otherMonthlyIncome = $('#<%=IDPrefix%>txtOtherMonthlyIncome').val();
				    var desc = $(this).val();
				    if (hasOtherMonthlyIncome == 'Y' && Common.ValidateTextNonZero(otherMonthlyIncome) && Common.ValidateText(desc) == false) {
				        return "Other Monthly Income Description is required";
				    }
				    return "";
				}
			],
		    validateOnBlur: true,
		    group: "<%=IDPrefix%>ValidateFinancialInfo"
		});

        $('#<%=IDPrefix%>txtTotalMonthlyHousingExpense').observer({
	        validators: [
				function (partial) {
					if ($('#<%=IDPrefix%>ddlOccupyingStatus').length == 0) return "";
				    var housingExpense = $(this).val();
				    var occupyingStatus = $('#<%=IDPrefix%>ddlOccupyingStatus').val();
				    // Mortgage/rent payment field is required when user select Rent or overridden behavior
					const housingStatusForRequiredPayment = <%=JsonConvert.SerializeObject(OccupancyStatusesForRequiredHousingPayment)%>;
                    if (_.indexOf(housingStatusForRequiredPayment, occupyingStatus.toUpperCase()) >= 0 && Common.ValidateTextNonZero(housingExpense) == false) {
					    return "Monthly Mortgage/Rent Payment is required";
					}
					return "";
				}
			],
		    validateOnBlur: true,
		    group: "<%=IDPrefix%>ValidateFinancialInfo"
		});
    }

    <%=IDPrefix%>registerFinancialInfoValidator();

    function getTaxExempt(chkElement) {
        var taxExempt = "N";
        if (chkElement.prev().hasClass('ui-checkbox-on')) {
            taxExempt = "Y";
        }
        return taxExempt;
    }

    function <%=IDPrefix%>SetApplicantFinancialInfo(appInfo) {
	    appInfo.<%=IDPrefix%>EmploymentDescription = "";

	    var grossMonthlyIncome = $("#<%=IDPrefix%>txtGrossMonthlyIncome").val();
	    var totalMonthlyHousingExpense = $("#<%=IDPrefix%>txtTotalMonthlyHousingExpense").val();
	    var otherMonthlyIncome = $('#<%=IDPrefix%>txtOtherMonthlyIncome').val();
	    var otherMonthlyIncomeDesc = $('#<%=IDPrefix%>txtOtherMonthlyIncomeDesc').val();
	    var hasOtherMonthlyIncome = $('#<%=IDPrefix%>lnkHasOtherMonthlyIncome').attr('status');
	    if (grossMonthlyIncome == "") {
	        grossMonthlyIncome = 0.0;
	    }
	    if (totalMonthlyHousingExpense == "") {
	        totalMonthlyHousingExpense = 0.0;
	    }
	    if (hasOtherMonthlyIncome != 'Y' || otherMonthlyIncome == "") {
	        otherMonthlyIncome = 0;
	        otherMonthlyIncomeDesc = "";
	    }

		appInfo.<%=IDPrefix%>TotalMonthlyHousingExpense = Common.GetFloatFromMoney(totalMonthlyHousingExpense);
	    appInfo.<%=IDPrefix%>OtherMonthlyIncome = Common.GetFloatFromMoney(otherMonthlyIncome);
        appInfo.<%=IDPrefix%>OtherMonthlyIncomeDescription = otherMonthlyIncomeDesc;
	    appInfo.<%=IDPrefix%>GrossMonthlyInComeTaxExempt = getTaxExempt($('#<%=IDPrefix%>grossMonthlyIncomeTaxExempt'));
	    appInfo.<%=IDPrefix%>OtherMonthlyIncomeTaxExempt = getTaxExempt($('#<%=IDPrefix%>otherMonthlyIncomeTaxExempt'));
	    //set previous employment infor
	}

	function <%=IDPrefix%>ViewFinancialInfo() {
        var txtGrossMonthlyIncome = htmlEncode($('#<%=IDPrefix%>txtGrossMonthlyIncome').val());
	    var txtTotalMonthlyHousingExpense = htmlEncode($('#<%=IDPrefix%>txtTotalMonthlyHousingExpense').val());
	    var otherMonthlyIncome = $('#<%=IDPrefix%>txtOtherMonthlyIncome').val();
	    var otherMonthlyIncomeDesc = htmlEncode($('#<%=IDPrefix%>txtOtherMonthlyIncomeDesc').val());
	    var hasOtherMonthlyIncome = $('#<%=IDPrefix%>lnkHasOtherMonthlyIncome').attr('status');
	    var strFinancialInfo = ""; 
	    //other monthly income
        if (hasOtherMonthlyIncome == 'Y') {
            if (Common.ValidateText(otherMonthlyIncome)) {
                otherMonthlyIncome = Common.FormatCurrency(otherMonthlyIncome, true);
                strFinancialInfo += '<div class="col-sm-6 col-xs-12"><div class="row"><div class="col-xs-6 text-right row-title"><b>Other Monthly Income</b></div><div class="col-xs-6 text-left row-data"><span>' + otherMonthlyIncome + '</span></div></div></div>';
                //tax exempt 
                var otherMonthlyIncomeTaxExempt = getTaxExempt($('#<%=IDPrefix%>otherMonthlyIncomeTaxExempt'));
				if (otherMonthlyIncomeTaxExempt == 'Y') {
				    strFinancialInfo += '<div class="col-sm-6 col-xs-12"><div class="row"><div class="col-xs-6 text-right row-title"><b>Other Monthly Income Tax Exempt</b></div><div class="col-xs-6 text-left row-data"><span>Yes</span></div></div></div>';
				}
            }
            if (Common.ValidateText(otherMonthlyIncomeDesc)) {
                strFinancialInfo += '<div class="col-sm-6 col-xs-12"><div class="row"><div class="col-xs-6 text-right row-title"><b>Other Monthly Income Description</b></div><div class="col-xs-6 text-left row-data"><span>' + otherMonthlyIncomeDesc + '</span></div></div></div>';
            }
        }
        if (Common.ValidateText(txtTotalMonthlyHousingExpense)) {
            txtTotalMonthlyHousingExpense = Common.FormatCurrency(txtTotalMonthlyHousingExpense, true);
            strFinancialInfo += '<div class="col-sm-6 col-xs-12"><div class="row"><div class="col-xs-6 text-right row-title"><b>Monthly Mortgage/Rent Payment</b></div><div class="col-xs-6 text-left row-data"><span>' + txtTotalMonthlyHousingExpense + '</span></div></div></div>';
        }
      
        return strFinancialInfo;
    }

    $(function () {

        $('#<%=IDPrefix%>lnkHasOtherMonthlyIncome').on('click', function (e) {
	        var $self = $(this);
	        var selectedValue = $self.attr('status');
	        if (selectedValue == "N") {
	            $self.html("Remove other income");
	            $self.addClass("minus-circle-before").removeClass("plus-circle-before");
	            $self.attr('status', 'Y');
	            $('#<%=IDPrefix%>divOtherMonthlyIncomeForm').show();
            } else {
                $self.html("Add other income");
                $self.attr('status', 'N');
                $self.addClass("plus-circle-before").removeClass("minus-circle-before");
                $('#<%=IDPrefix%>divOtherMonthlyIncomeForm').hide();
            }
		    if (BUTTONLABELLIST != null) {
		        var value = BUTTONLABELLIST[$.trim($self.html()).toLowerCase()];
		        if (typeof value == "string" && $.trim(value) !== "") {
		            $self.html(value);
		        }
		    }
		    e.preventDefault();
		});    
	});

</script>

