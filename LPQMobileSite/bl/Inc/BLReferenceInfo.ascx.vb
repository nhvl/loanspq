﻿Imports LPQMobile.Utils
Partial Class bl_Inc_BLReferenceInfo
    Inherits CBaseUserControl
	Public Property EnabledAddressVerification As Boolean = False
	Public Property EnableMailingAddress As Boolean = True
    Public Property StateDropdown As String
    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        Dim primaryAddressCtrl As xa_Inc_saAddress = CType(LoadControl("~/xa/Inc/saAddress.ascx"), xa_Inc_saAddress)
        primaryAddressCtrl.IsBusinessLoan = True
		primaryAddressCtrl.Header = HeaderUtils.RenderPageTitle(0, "", False)
        primaryAddressCtrl.IDPrefix = IDPrefix + "Primary"
        primaryAddressCtrl.EnableOccupancyStatus = False
        primaryAddressCtrl.StateDropdown = StateDropdown
		primaryAddressCtrl.EnabledAddressVerification = EnabledAddressVerification
		primaryAddressCtrl.EnableMailingAddress = EnableMailingAddress
		primaryAddressCtrl.IsAddressRequired = False
		primaryAddressCtrl.MappedShowFieldLoanType = MappedShowFieldLoanType
        plhPrimaryRefAddress.Controls.Add(primaryAddressCtrl)
        Dim secondaryAddressCtrl As xa_Inc_saAddress = CType(LoadControl("~/xa/Inc/saAddress.ascx"), xa_Inc_saAddress)
        secondaryAddressCtrl.IsBusinessLoan = True
		secondaryAddressCtrl.Header = HeaderUtils.RenderPageTitle(0, "", False)
        secondaryAddressCtrl.IDPrefix = IDPrefix + "Secondary"
        secondaryAddressCtrl.EnableOccupancyStatus = False
		secondaryAddressCtrl.EnabledAddressVerification = EnabledAddressVerification
		secondaryAddressCtrl.EnableMailingAddress = EnableMailingAddress
        secondaryAddressCtrl.StateDropdown = StateDropdown
		secondaryAddressCtrl.IsAddressRequired = False
		secondaryAddressCtrl.MappedShowFieldLoanType = MappedShowFieldLoanType
        plhSecondaryRefAddress.Controls.Add(secondaryAddressCtrl)
    End Sub
End Class
