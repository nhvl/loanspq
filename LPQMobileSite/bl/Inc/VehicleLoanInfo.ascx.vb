﻿Imports DownloadedSettings
Imports Newtonsoft.Json
Imports LPQMobile.BO
Imports LPQMobile.Utils
Imports System.IO
Imports System.Xml
Imports log4net
Imports System.Web.Script.Serialization
Partial Class bl_Inc_VehicleLoanInfo
    Inherits CBaseUserControl
    Public Property _VehicleLoanPurposeDropdown As String
    Public Property _HasLoanPurpose As String
    Public Property _VehicleLoanPurposeList As Dictionary(Of String, String)
    Public Property _VehicleTypes As String
    Public Property _CarMakeDropdown As String
    Public Property _MotorCycleMakeDropdown As String
    Public Property _RVOrBoatMakeDropdown As String
    Public Property _BranchDropdown As String
    Public Property _LoanTermDropdown As String = ""
    Public Property _CreditPullMessage As String = ""
    Public Property LaserDLScanEnabled As Boolean = False
    Public Property LegacyDLScanEnabled As Boolean = False
    Public Property _InterestRate As Double = 0D
    Public Property _branchOptionsStr As String = ""
    Public Property EnableVinNumber As Boolean = False
    Public Property VinBarcodeScanEnabled As Boolean = False
    Public Property _autoTermTextboxEnable As String = ""
    ''dgdgdg
    Public Property _requiredBranch As String = ""
    Public Property EnableBranchSection As Boolean = False
    Public Property _LogoUrl As String = ""
    Public Property _textAndroidTel As String = "text"
    Public Property showReference As Boolean = False
    Public Property showEstimatePayment As Boolean = False
    Public Property _LoanPurposeCategory As String
#Region "Property for SSO"
    Protected ReadOnly Property LoanPurpose() As String
        Get
            Return Common.SafeString(Request.Params("LoanPurpose"))
        End Get
    End Property

    Protected ReadOnly Property VehicleType() As String
        Get
            Return Common.SafeString(Request.Params("VehicleType"))
        End Get
    End Property

    Protected ReadOnly Property Term() As String
        Get
            Return Common.SafeString(Request.Params("Term"))
        End Get
    End Property

    Protected ReadOnly Property EstPurchasePrice() As String
        Get
            Return Common.SafeString(Request.Params("EstPurchasePrice"))
        End Get
    End Property

    Protected ReadOnly Property DownPayment() As String
        Get
            Return Common.SafeString(Request.Params("DownPayment"))
        End Get
    End Property


#End Region
    Public Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        If Not IsPostBack Then

			xaApplicationQuestionLoanPage.Header = HeaderUtils.RenderPageTitle(0, "Please answer the following question(s)", True)

		End If
    End Sub
End Class
