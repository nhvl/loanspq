﻿Imports LPQMobile.BO
Imports LPQMobile.Utils
Imports System.IO
Imports System.Xml
Imports System.Web.Security.AntiXss
Partial Class bl_Inc_BusinessLoanSelector
    Inherits CBaseUserControl
    Public Property LenderRef As String
	Public Property DefaultParams As String
	Public Property ApplicantRoles As Dictionary(Of String, String)
    Public Property EnableVehicleLoan As Boolean = True
    Public Property EnableCreditCardLoan As Boolean = True
    Public Property EnableOtherLoan As Boolean = True
    Public Property EnableAppRole_P As Boolean = True
    Public Property EnableAppRole_C As Boolean = True
    Public Property EnableAppRole_S As Boolean = True
    Public Property EnableAppRole_G As Boolean = True
    Protected BLAppRoles As New List(Of String)
    Protected BLJointAppRoleOptions As String = ""
    Protected Function ParseParams(paramsStr As String) As Dictionary(Of String, String)
		Dim result As New Dictionary(Of String, String)
		If paramsStr IsNot Nothing Then
			paramsStr = paramsStr.Trim({"?"c, "&"c, " "c})
			If Not String.IsNullOrEmpty(paramsStr) Then
				For Each kvStr As String In paramsStr.Split("&"c)
					Dim kvArr = kvStr.Split("="c)
					If kvArr.Length = 2 AndAlso Not result.ContainsKey(kvArr(0)) Then
						result.Add(kvArr(0), kvArr(1))
					End If
				Next
			End If
		End If
		Return result
	End Function
    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        InitBLAppRoles()
    End Sub
    Protected Sub InitBLAppRoles()
        ''add all enable BL roles to the BLAppRoles.
        ''rendering available BL app role options for joint.
        Dim BLJointAppRoles As New Dictionary(Of String, String)
        If EnableAppRole_P Then
            BLAppRoles.Add("P")
        End If
        If EnableAppRole_C Then
            BLAppRoles.Add("C")
            BLJointAppRoles.Add("C", "Co - Borrower")
        End If
        If EnableAppRole_S Then
            BLAppRoles.Add("S")
            BLJointAppRoles.Add("S", "Co - Signer")
        End If
        If EnableAppRole_G Then
            BLAppRoles.Add("G")
            BLJointAppRoles.Add("G", "Guarantor")
        End If
        BLJointAppRoleOptions = Common.RenderDropdownlistWithEmpty(BLJointAppRoles, "", "--Please Select--")
    End Sub
End Class
