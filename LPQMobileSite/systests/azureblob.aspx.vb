﻿Imports Microsoft.WindowsAzure.Storage
Imports Microsoft.WindowsAzure.Storage.Blob
Imports System
Imports System.IO
Imports System.Threading.Tasks

''' <summary>
''' sample copy from
''' https://docs.microsoft.com/en-us/azure/storage/blobs/storage-quickstart-blobs-dotnet?tabs=windows
''' https://github.com/Azure-Samples/storage-blobs-dotnet-quickstart
''' </summary>
Partial Class systests_azureblob
    Inherits System.Web.UI.Page

    Private Sub systests_azureblob_Load(sender As Object, e As EventArgs) Handles Me.Load
        Console.WriteLine("Azure Blob Storage - .NET quickstart sample")
        Console.WriteLine()
        ProcessAsync().GetAwaiter().GetResult()
        Console.WriteLine("Press any key to exit the sample application.")
        Console.ReadLine()
    End Sub

    Private Async Function ProcessAsync() As Task
        Dim storageAccount As CloudStorageAccount = Nothing
        Dim cloudBlobContainer As CloudBlobContainer = Nothing
        Dim sourceFile As String = Nothing
        Dim destinationFile As String = Nothing

        'This need to be store in web.config with
        'Dim storageConnectionString As String = Environment.GetEnvironmentVariable("storageconnectionstring")
        Dim storageConnectionString As String = "DefaultEndpointsProtocol=https;AccountName=cs4d6e8cb6c1acex429bxa4a;AccountKey=eC4tGC8uTuB9JnJ0i8VveEUpZanMY+NDskCDEGFCggduXndVihkpnosdSUpm4XCc2ccKtdkXJmf7hv4+VqJjow==;EndpointSuffix=core.windows.net"

        If CloudStorageAccount.TryParse(storageConnectionString, storageAccount) Then

            Try
                Dim cloudBlobClient As CloudBlobClient = storageAccount.CreateCloudBlobClient()
                cloudBlobContainer = cloudBlobClient.GetContainerReference("quickstartblobs" & Guid.NewGuid().ToString())
                Await cloudBlobContainer.CreateAsync()
                Console.WriteLine("Created container '{0}'", cloudBlobContainer.Name)
                Console.WriteLine()
                Dim permissions As BlobContainerPermissions = New BlobContainerPermissions With {
                    .PublicAccess = BlobContainerPublicAccessType.Blob
                }
                Await cloudBlobContainer.SetPermissionsAsync(permissions)
                'Dim localPath As String = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments)
                Dim localPath As String = "c:\temp\"
                Dim localFileName As String = "QuickStart_" & Guid.NewGuid().ToString() & ".txt"
                sourceFile = Path.Combine(localPath, localFileName)
                File.WriteAllText(sourceFile, "Hello, World!")
                Console.WriteLine("Temp file = {0}", sourceFile)
                Console.WriteLine("Uploading to Blob storage as blob '{0}'", localFileName)
                Console.WriteLine()
                Dim cloudBlockBlob As CloudBlockBlob = cloudBlobContainer.GetBlockBlobReference(localFileName)
                Await cloudBlockBlob.UploadFromFileAsync(sourceFile)
                Console.WriteLine("Listing blobs in container.")
                Dim blobContinuationToken As BlobContinuationToken = Nothing

                Do
                    Dim resultSegment = Await cloudBlobContainer.ListBlobsSegmentedAsync(Nothing, blobContinuationToken)
                    blobContinuationToken = resultSegment.ContinuationToken

                    For Each item As IListBlobItem In resultSegment.Results
                        Console.WriteLine(item.Uri)
                    Next
                Loop While blobContinuationToken IsNot Nothing

                Console.WriteLine()
                destinationFile = sourceFile.Replace(".txt", "_DOWNLOADED.txt")
                Console.WriteLine("Downloading blob to {0}", destinationFile)
                Console.WriteLine()
                Await cloudBlockBlob.DownloadToFileAsync(destinationFile, FileMode.Create)
            Catch ex As StorageException
                Console.WriteLine("Error returned from the service: {0}", ex.Message)
            Finally
                Console.WriteLine("Press any key to delete the sample files and example container.")
                Console.ReadLine()
                Console.WriteLine("Deleting the container and any blobs it contains")

                If cloudBlobContainer IsNot Nothing Then
                    ' Await cloudBlobContainer.DeleteIfExistsAsync()
                End If

                Console.WriteLine("Deleting the local source file and local downloaded files")
                Console.WriteLine()
                'File.Delete(sourceFile)
                'File.Delete(destinationFile)
            End Try
        Else
            Console.WriteLine("A connection string has not been defined in the system environment variables. " & "Add a environment variable named 'storageconnectionstring' with your storage " & "connection string as a value.")
        End If
    End Function
End Class
