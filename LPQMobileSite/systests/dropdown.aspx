﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="dropdown.aspx.vb" Inherits="systests_dropdown" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
	<meta name="viewport" content="width=device-width, initial-scale=1" />
	<link rel="stylesheet" href="https://demos.jquerymobile.com/1.4.5/css/themes/default/jquery.mobile-1.4.5.min.css" />
	<script src="https://code.jquery.com/jquery-1.12.4.min.js" type="text/javascript"></script>
	<style type="text/css">
		.ui-selectmenu.ui-popup .ui-input-search {
			margin-left: .5em;
			margin-right: .5em;
		}

		.ui-selectmenu-list.ui-listview {
			max-height: 400px;
			overflow-y: auto;
			-webkit-overflow-scrolling: touch;
		}

		.ui-selectmenu.ui-popup .ui-selectmenu-list li.ui-first-child .ui-btn {
			border-top-width: 1px;
			-webkit-border-radius: 0;
			border-radius: 0;
		}
	</style>
	<script type="text/javascript">
		$(document).on("mobileinit", function () {
			//is to remove hash from url 
			$.mobile.hashListeningEnabled = false;
			$.mobile.pushStateEnabled = false;
			$.mobile.changePage.defaults.changeHash = false;
		});
	</script>
	<script src="jquery.mobile-1.4.5.js" type="text/javascript"></script>
	<%--<script src="https://demos.jquerymobile.com/1.4.5/js/jquery.mobile-1.4.5.js" type="text/javascript"></script>--%>
	<script type="text/javascript">
		$.mobile.document.on("listviewcreate", "#filter-menu-menu", function(e) {
			var input,
				listbox = $("#filter-menu-listbox"),
				form = listbox.jqmData("filter-form"),
				listview = $(e.target);
			// We store the generated form in a variable attached to the popup so we
			// avoid creating a second form/input field when the listview is
			// destroyed/rebuilt during a refresh.
			if (!form) {
				input = $("<input data-type='search'></input>");
				form = $("<form></form>").append(input);
				input.textinput();
				$("#filter-menu-listbox")
					.prepend(form)
					.jqmData("filter-form", form);
			} else {

			}
			// Instantiate a filterable widget on the newly created listview and
			// indicate that the generated input is to be used for the filtering.
			listview.filterable({ input: input });
		});
	</script>
</head>
<body>
	<div data-role="page" id="demo-page">
		<div data-role="header">
			<h1>JQM Type Ahead Dropdown Demo</h1>
		</div>
		<div role="main" class="ui-content">
			<select name="title-dropdown" id="filter-menu" data-native-menu="false">
				<option value="AL">Alabama</option>
				<option value="AK">Alaska</option>
				<option value="AZ">Arizona</option>
				<option value="AR">Arkansas</option>
				<option value="CA">California</option>
				<option value="CO">Colorado</option>
				<option value="CT">Connecticut</option>
				<option value="DE">Delaware</option>
				<option value="FL">Florida</option>
				<option value="GA">Georgia</option>
				<option value="HI">Hawaii</option>
				<option value="ID">Idaho</option>
				<option value="IL">Illinois</option>
				<option value="IN">Indiana</option>
				<option value="IA">Iowa</option>
				<option value="KS">Kansas</option>
				<option value="KY">Kentucky</option>
				<option value="LA">Louisiana</option>
				<option value="ME">Maine</option>
				<option value="MD">Maryland</option>
				<option value="MA">Massachusetts</option>
				<option value="MI">Michigan</option>
				<option value="MN">Minnesota</option>
				<option value="MS">Mississippi</option>
				<option value="MO">Missouri</option>
				<option value="MT">Montana</option>
				<option value="NE">Nebraska</option>
				<option value="NV">Nevada</option>
				<option value="NH">New Hampshire</option>
				<option value="NJ">New Jersey</option>
				<option value="NM">New Mexico</option>
				<option value="NY">New York</option>
				<option value="NC">North Carolina</option>
				<option value="ND">North Dakota</option>
				<option value="OH">Ohio</option>
				<option value="OK">Oklahoma</option>
				<option value="OR">Oregon</option>
				<option value="PA">Pennsylvania</option>
				<option value="RI">Rhode Island</option>
				<option value="SC">South Carolina</option>
				<option value="SD">South Dakota</option>
				<option value="TN">Tennessee</option>
				<option value="TX">Texas</option>
				<option value="UT">Utah</option>
				<option value="VT">Vermont</option>
				<option value="VA">Virginia</option>
				<option value="WA">Washington</option>
				<option value="WV">West Virginia</option>
				<option value="WI">Wisconsin</option>
				<option value="WY">Wyoming</option>
			</select>

		</div>
		<div data-role="footer" data-position="fixed">
			<h1>Footer</h1>
		</div>
	</div>
</body>
</html>
