﻿
Partial Class systests_PaypalFaker
    Inherits System.Web.UI.Page
	Public Property ReturnUrl As String
	Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
		If Not IsPostBack Then
			ReturnUrl = Request.QueryString("callbackurl") & "&success=true"
		End If
	End Sub
End Class
