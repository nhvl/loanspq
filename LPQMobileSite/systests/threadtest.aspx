﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="threadtest.aspx.vb" Inherits="systests_threadtest" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
	<form id="form1" runat="server">
    <div>
	    <asp:Label runat="server" ID="lblSessionID"></asp:Label>
		<asp:Button ID="btnStart" runat="server" Text="Start" />
		<asp:Button ID="btnDone" runat="server" Text="Done" />
    </div>
    </form>
</body>
</html>
