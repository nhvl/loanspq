﻿Imports System.Threading
Imports LPQMobile.Utils
Partial Class systests_threadtest
	Inherits System.Web.UI.Page

	Private log As log4net.ILog = log4net.LogManager.GetLogger(Me.GetType)
	Private Shared _threadLock_ As New Object
	Private Shared _dic As New Dictionary(Of String, String)
	'Const KEY = "key123"

	Protected Sub btnStart_Click(sender As Object, e As EventArgs) Handles btnStart.Click
		'Session(KEY) = "Waiting For IDA"
		'log.Info("Start, saved session with value: " & Common.SafeString(Session(KEY)))
		Dim id As String = Guid.NewGuid().ToString().Substring(0, 8)
		lblSessionID.Text = id
		Dim oThread As New Thread(AddressOf CheckDoneFlag)
		oThread.Name = "t1"
		oThread.IsBackground = True
		'oThread.
		oThread.Start(id)
		log.Info("START: " & id)
		_dic.Add(id, "Waiting For IDA")
		log.Info("dic: " & String.Join("--", _dic.ToArray()))
	End Sub

	'TODO: why is the session not clear inside the CheckDoneFlag scope
	Protected Sub btnDone_Click(sender As Object, e As EventArgs) Handles btnDone.Click
		'Session(KEY) = Nothing ''done
		'If Session(KEY) Is Nothing Then
		'	log.Info("Done, cleared session")
		'End If

		SyncLock _threadLock_
			If _dic.ContainsKey(lblSessionID.Text) Then
				log.Info("STOP: " & lblSessionID.Text)
				_dic.Remove(lblSessionID.Text)
				log.Info("dic: " & String.Join("--", _dic.ToArray()))
			End If
		End SyncLock
	End Sub
	''
	Private Sub CheckDoneFlag(ByVal poKey As String)


		System.Threading.Thread.Sleep(60000) '1min
		
		SyncLock _threadLock_
			If _dic.ContainsKey(poKey) Then
				log.Info("NOT DONE, ID: " & poKey & " VALUE: " & _dic(poKey))
				_dic.Remove(poKey)
				log.Info("THEN MAKE IT DONE : " & poKey)
			Else
				log.Info("DONE ALREADY: " & poKey)
			End If
		End SyncLock
		

		''TODO:why can't clear the session form the main thread

		'If Session(KEY) Is Nothing Then

		'	log.Info("Done, session not FOUND in thread scope")
		'	'already done via btnDone_Click so dont need to do anything
		'Else
		'	log.Info("Not done, session FOUND with value: " & Common.SafeString(Session(KEY)))

		'	'btnDone_Click is not click so need to run some process

		'	'Run somthing

		'End If


	End Sub

	Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
	End Sub
End Class
