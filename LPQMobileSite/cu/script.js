﻿//bogus file, pageheader is looking for this file
var g_vsl_selected_loan_num = 0;
var g_vsl_selected_loan_id = 0;
var g_vsl_selected_app_type = 'LOAN';
var g_vsl_selected_email = '';
var g_vsl_selected_doc_list = [];
$(document).on("pagecontainerbeforeshow", "#viewSubmitPage", function () { // Clear content when click Go back
    $('#divResultMessage').html('');
    $('#divImage').html('');
});

$(document).on("pagecontainerbeforeshow", "#viewMessage", function () { // Clear content when click Go back
    var pageHeight = $('#viewMessage').height();
    var screenHeight = $(window).height();
    $('#ContentFrame').height(pageHeight - 430);
    if (pageHeight > screenHeight) {
        $('#ContentFrame').height(screenHeight - 430);
    }
    $('#ContentFrame').attr('src', 'WebMessagingView.aspx?LoanID=' + g_vsl_selected_loan_id + '&lender_ref=' + $('#hfLenderRef').val() + '&loan_number=' + g_vsl_selected_loan_num + '&AppType=' + g_vsl_selected_app_type);
});
function ScrollMessageToBottom() {
    $('#ContentFrame').contents().scrollTop($('#ContentFrame').contents().height());
}

$(function () {
    if ($('#hdForeignAppType').val() == 'Y') {
    	$("#lblSSN").removeClass("RequiredIcon"); // no required ->remove red stard
    } else {
    	$("#lblSSN").addClass("RequiredIcon"); // required -> add restar
    }

    //var footerTheme = $('#hdFooterTheme').val();
    //var bgColor = $('.ui-bar-' + footerTheme).css('background-color');
    //var changeColor = changeBackgroundColor(bgColor, .6);
    //var bgTheme = $('#hdBgTheme').val();
    //if (bgTheme != "") {
    //    changeColor = bgColor;
    //}
    //$('.ui-header').css('border-color', changeColor);
    //$('body').css("background-color", changeColor);
    //handledViewMessagePageHeight();

    $("#popSSNSastisfy, #popUpMsgNote, #popVerifyDeposit, #popVerifyDepositResult, #popSecurityCodeEmailNotify, #popDocList").on("popupafteropen", function (event, ui) {
        $("#" + this.id + "-screen").height("");
    });
	$("#popVerifyDepositResult").on("popupafterclose", function(event, ui) {
		if ($("#popVerifyDepositResult").data("result-status") !== "SUCCESS" && $("#popVerifyDepositResult").data("result-status") != "EXCEED") {
			$("#popVerifyDeposit").popup("open", { "positionTo": "window" });
		}
	});
    $('.ui-input-ssn input').on('focusout', function () {
        var ssn = $("#txtSSN1").val() + '-' + $("#txtSSN2").val() + '-' + $("#txtSSN3").val();
        $("#txtSSN").val(ssn);
    });

    $(document).on("pageshow", function() {
        var pageId = $.mobile.pageContainer.pagecontainer('getActivePage').attr('id');
        if (pageId == "viewMessage") {
            $('#ContentFrame').attr('src', 'WebMessagingView.aspx?LoanID=' + g_vsl_selected_loan_id + '&lender_ref=' + $('#hfLenderRef').val() + '&loan_number=' + g_vsl_selected_loan_num + '&AppType=' + g_vsl_selected_app_type);
        }
    });
    $(document).on("pagebeforeshow", function () {
        var pageId = $.mobile.pageContainer.pagecontainer('getActivePage').attr('id');
        if (pageId == "viewMessage") {
            $('#ContentFrame').attr('src', 'about:blank');
        }
    });
    $("#lnkMfaSwitch").on("click", function(e) {
		var $self = $(this);
		if ($self.data("value") == "ssn") {
			$self.html("Confirm your identity via email instead of SSN?");
			$self.data("value", "email");
			$("#lblSSN").closest("div[data-role='fieldcontain']").removeClass("hidden");
			$("#btnGet").removeClass("hidden");
			$("#btnSendEmailAuthentication").addClass("hidden");
		} else {
			$self.html("Confirm your identity via SSN instead of email?");
			$self.data("value", "ssn");
			$("#lblSSN").closest("div[data-role='fieldcontain']").addClass("hidden");
			$("#btnGet").addClass("hidden");
			$("#btnSendEmailAuthentication").removeClass("hidden");
		}
		if (BUTTONLABELLIST != null) {
			var value = BUTTONLABELLIST[$.trim($self.html()).toLowerCase()];
			if (typeof value == "string" && $.trim(value) !== "") {
				$self.html(value);
			}
		}
		e.preventDefault();
	});

	if ($("#lnkMfaSwitch").length == 0) {
	    $("#lblSSN").closest("div[data-role='fieldcontain']").addClass("hidden");
	    $("#btnGet").addClass("hidden");
	    $("#btnSendEmailAuthentication").removeClass("hidden");
    }
    //hide the check status button 
    if ($('#hdMFAEmailOnly').val() == "Y") {
        $(window).on('load', function () {
            $('#btnGet').addClass('hidden');
       });
    }

	registerValidator();
	//end handle check status message
});
/*function handledViewMessagePageHeight() {
    $('#tbMsg').on('keyup', function (e) {
        var currElement = $(this);
        var initMsgHeight = 60;	         
        if (currElement.height() > initMsgHeight) {
            var viewMessageHeight = $(window).height() + currElement.height() + 30 - initMsgHeight;
            $('#viewMessage').height(viewMessageHeight);
            initMsgHeight = currElement.height();
        }
        if (currElement.val().trim() == "" && currElement.val().length < 2) {           
                $('#viewMessage').height($(window).height());
                currElement.height(60);            
        } 
    });
}*/
function registerValidator() {
	$('#divSSN').observer({
		validators: [
			function (partial, evt) {
				if ($("#lblSSN").closest("div[data-role='fieldcontain']").hasClass("hidden")) return "";
				if ($("#txtSSN1").val().length == 3) {
					$("#txtSSN").val($("#txtSSN1").val() + '-' + $("#txtSSN2").val() + '-' + $("#txtSSN3").val());
				}
				var ssn = $('#txtSSN1').val() + $('#txtSSN2').val() + $('#txtSSN3').val();
				if (/^[0-9]{9}$/.test(ssn) == false) {
					return 'Valid SSN is required';
				}
				return "";
			}
		],
		validateOnBlur: true,
		group: "ValidateSubmittedLoan"
	});
	$('#txtLName').observer({
		validators: [
			function (partial) {
				var text = $(this).val();
				if (!Common.ValidateText(text)) {
					return 'Last Name is required';
				} else if (/^[\sa-zA-Z\'-]+$/.test(text) == false) {
					return 'Enter a valid last name. Allowed characters: letters "A-Z", dashes, apostrophes and spaces.';
				}
				return "";
			}
		],
		validateOnBlur: true,
		group: "ValidateSubmittedLoan"
	});
	$('#txtEmail').observer({
		validators: [
			function (partial) {
				var email = $(this).val();
				if (Common.ValidateEmail(email) == false) {
					return "Valid Email is required";
				}
				return "";
			}
		],
		validateOnBlur: true,
		group: "ValidateSubmittedLoan"
	});

	$('#txtSecurityCode').observer({
		validators: [
			function (partial) {
				var code = $(this).val();
				if ($(this).closest("div[data-role='fieldcontain']").hasClass("hidden")) return "";
				if (Common.ValidateText(code) == false) {
					return "Security Code is required";
				}
				return "";
			}
		],
		validateOnBlur: true,
		group: "ValidateSubmittedLoan"
	});

	$('#txtDepositAmount1').observer({
		validators: [
			function (partial) {
				if (!Common.ValidateTextNonZero($(this).val())) {
					return 'Valid amount is required';
				}
				return "";
			}
		],
		validateOnBlur: true,
		group: "VerifyDepositAmount"
	});
	$('#txtDepositAmount2').observer({
		validators: [
			function (partial) {
				if (!Common.ValidateTextNonZero($(this).val())) {
					return 'Valid amount is required';
				}
				return "";
			}
		],
		validateOnBlur: true,
		group: "VerifyDepositAmount"
	});
}
function validateSubmitForm(element) {
    //var txtLName = $('#txtLName').val();
    //var txtSSN = Common.GetSSN($('#txtSSN').val());
    //var txtEmail = $('#txtEmail').val().trim();
    //var strMessage = '';
    //if (!Common.ValidateText(txtLName)) {
    //    //alert('Please complete the Last Name field');
    //    //return false;
    //    strMessage += 'Please complete the Last Name field.<br/>';
    //}

    //if (!Common.ValidateText(txtEmail)) {
    //    //alert('Please complete the Email field.');
    //    //return false;
    //    strMessage += 'Please complete the Email field.<br/>';
    //} else {
    //    if (Common.ValidateEmail(txtEmail) == false) {
    //        //alert('Please verify your email address entered.');
    //        //return false;
    //        strMessage += 'Please verify your email address entered.<br/>';
    //    }
    //}

    //if (!Common.ValidateText(txtSSN)) {
    //    if ($('#hdForeignAppType').val() != 'Y') { //make ssn is not required if hasForeignAppType 
    //        //alert('Please complete the Social Security Number field.');
    //        //return false;
    //        strMessage += 'Please complete the Social Security Number field.<br/>';
    //    }

    //} else {
    //    if (txtSSN.length != 9) {
    //        //alert('Please input a 9 digit social security number.');
    //        //return false;
    //        strMessage += 'Please input a 9 digit social security number.<br/>';
    //    }
    //}
    //if (strMessage != "") {
    //    // An error occurred, we need to bring up the error dialog.
    //    var nextpage = $('#divErrorDialog');
    //    $('#txtErrorMessage').html(strMessage);
    //    if (nextpage.length > 0) {
    //        $.mobile.changePage(nextpage, {
    //            transition: "pop",
    //            reverse: false,
    //            changeHash: true
    //        });
    //    }
    //    return false;
    //}
	//there is no error
	if ($.lpqValidate("ValidateSubmittedLoan")) {
		getStatus();
	}
}

function sendEmailAuthentication() {
	if ($.lpqValidate("ValidateSubmittedLoan")) {
		$.ajax({
			type: 'POST',
			url: 'ViewSubmittedLoans.aspx',
			dataType: 'json',
			data: {
				command: "SendSecurityCode",
				LenderRef: $('#hfLenderRef').val(),
				LName: $('#txtLName').val(),
				Email: $('#txtEmail').val().trim()
			},
			success: function (response) {
				$('#divSearchResults').html('');
				$("#txtSecurityCode").val("").focus();
				$("#txtSecurityCode").closest("div[data-role='fieldcontain']").removeClass("hidden");
				$("#btnGet").removeClass("hidden");
				$("#btnSendEmailAuthentication").addClass("hidden");
				$("#lnkMfaSwitch").closest("div").addClass("hidden");
				$("div.content-panel", "#popSecurityCodeEmailNotify").html('<h2>Security Code Emailed</h2><p>A Security Code has been sent to your email. Please check your email and enter the provided code.</p><p>It may take a few minutes to arrive in your Inbox</p>');
				$("#popSecurityCodeEmailNotify").popup("open", { "positionTo": "window" });
			},
			error: function (err) {}
		});



		
	}
}

function getStatus() {
    $('#divSearchResults').html('');
    var loanInfo = getApplicantInfo();
    $.ajax({
        type: 'POST',
        url: 'ViewSubmittedLoans.aspx',
        data: loanInfo,
        dataType: 'html',
        success: function (response) {
        	if (response == "InvalidCode") {
        		$("div.content-panel", "#popSecurityCodeEmailNotify").html('Invalid Security Code');
        		$("#popSecurityCodeEmailNotify").popup("open", { "positionTo": "window" });
	        } else {
		        //var decoded_message = $('<div/>').html(response).text(); //message maybe encoded in the config xml so need to decode
		        $('#divSearchResults').html(response);
		        $('#divSearchResults').trigger("contentChanged");
		        applyHeaderThemeCss("viewSubmitPage");
		        $("#txtSecurityCode").closest("div[data-role='fieldcontain']").addClass("hidden");
		        if ($("#lnkMfaSwitch").length > 0) {
			        $("#lnkMfaSwitch").closest("div").removeClass("hidden");
			        if ($("#lnkMfaSwitch").data("value") == "email") {
				        $("#lblSSN").closest("div[data-role='fieldcontain']").removeClass("hidden");
				        $("#btnGet").removeClass("hidden");
				        $("#btnSendEmailAuthentication").addClass("hidden");
			        } else {
				        $("#lblSSN").closest("div[data-role='fieldcontain']").addClass("hidden");
				        $("#btnGet").addClass("hidden");
				        $("#btnSendEmailAuthentication").removeClass("hidden");
			        }
		        } else {
		        	$("#lblSSN").closest("div[data-role='fieldcontain']").addClass("hidden");
		        	$("#btnGet").addClass("hidden");
		        	$("#btnSendEmailAuthentication").removeClass("hidden");
		        }
	        }
        },
        error: function (err) {
        	$('#divSearchResults').html('<p class="rename-able">We apologize, but unfortunately there was an error when submitting the request. Please try again later.</p>');
        	$('#divSearchResults').trigger("contentChanged");
        }
    });
    return false;
}


function getApplicantInfo() {
    // Bundle up the information that the user has entered so that we can pass it along to the submission function.
    var appInfo = new Object();
    var txtLName = $('#txtLName').val();
    var txtEmail = $('#txtEmail').val().trim();
    
    appInfo.command = "SearchLoan";
    appInfo.lenderref = $('#hfLenderRef').val();
    appInfo.LastName = txtLName;
    appInfo.EmailAddr = txtEmail;
    if ($("#lblSSN").closest("div[data-role='fieldcontain']").hasClass("hidden") == false) {
    	var txtSSN = Common.GetSSN($("#txtSSN").val());
    	if ($('#hdForeignAppType').val() == 'Y') {
    		if (txtSSN == "") {
    			txtSSN = Common.GetSSN("999-99-9999");
    		}
    	}
    	appInfo.SSN = txtSSN;
    }
    if ($("#txtSecurityCode").closest("div[data-role='fieldcontain']").hasClass("hidden") == false) {
    	appInfo.SecurityCode = $("#txtSecurityCode").val();
	}

	return appInfo;
}

function gotoDocumentUpload(obj) {
    g_vsl_selected_loan_num = $(obj).attr('loan_num');
    g_vsl_selected_loan_id = $(obj).attr('loan_id');
    g_vsl_selected_app_type = $(obj).attr('app_type');
    /*$.ajax({
        type: 'POST',
        url: 'ViewSubmittedLoans.aspx',
        data: {
            command: "changeDocTitlePath",
            lenderref: $('#hfLenderRef').val(),
            doc_title_path: (g_vsl_selected_app_type == "Account Application" ? "ENUMS/XA_DOC_TITLE/ITEM" : "ENUMS/LOAN_DOC_TITLE/ITEM")
        },
        dataType: 'json',
        success: function (response) {
            $('#hdUploadDocs').val(response.data);
        }
    });*/
    if (parsedAppType(g_vsl_selected_app_type) == "XA") {    
		docUploadObj.updateSettings(g_vsl_xa_doc_upload_settings);
    } else if (parsedAppType(g_vsl_selected_app_type) == "BL") {
    	docUploadObj.updateSettings(g_vsl_bl_doc_upload_settings);
    } else if (parsedAppType(g_vsl_selected_app_type) == "CC") {
    	docUploadObj.updateSettings(g_vsl_cc_doc_upload_settings);
    } else if (parsedAppType(g_vsl_selected_app_type) == "HE") {
    	docUploadObj.updateSettings(g_vsl_he_doc_upload_settings);
    } else if (parsedAppType(g_vsl_selected_app_type) == "PL") {
    	docUploadObj.updateSettings(g_vsl_pl_doc_upload_settings);
    } else if (parsedAppType(g_vsl_selected_app_type) == "VL") {
    	docUploadObj.updateSettings(g_vsl_vl_doc_upload_settings);
    }
    $.mobile.changePage("#docScan", {
        transition: "pop",
        reverse: false,
        changeHash: true
    });
}
function parsedAppType(appType) {
    appType = appType != undefined ? appType.toUpperCase() : "";
    switch (appType) {
        case "CREDIT CARD APPLICATION":
            return "CC";
        case "PERSONAL LOAN APPLICATION":
            return "PL";
        case "VEHICLE LOAN APPLICATION":
            return "VL";
        case "HOME EQUITY APPLICATION":
            return "HE";
        case "ACCOUNT APPLICATION":
            return "XA";
        case "BUSINESS LOAN APPLICATION":
            return "BL";         
    }
    return "";   
}
function showDocuments(obj) {
	//g_vsl_selected_loan_num = $(obj).attr('loan_num');
	//g_vsl_selected_loan_id = $(obj).attr('loan_id');
	//g_vsl_selected_app_type = $(obj).attr('app_type');
    var sLoanNumber = $(obj).attr('loan_num');
    var sAppType = $(obj).attr('app_type');
	$.ajax({
        type: 'POST',
        url: 'ViewSubmittedLoans.aspx',
        data: {
            command: "LoadDocList",
            lenderref: $('#hfLenderRef').val(),
            loanNum: sLoanNumber,
            appType: parsedAppType(sAppType)
        },
        dataType: 'json',
        success: function (response) {
            var placeHolder = $("div.content-panel", "#popDocList");
            if (response.message == "error") {
                placeHolder.html('Error! Please try again.');
            } else if (response.message == "") {
                placeHolder.html("<h2>No Available Documents for " + sAppType + " #" + sLoanNumber+ "</h2>");
			} else {
				g_vsl_selected_doc_list = response;		
				placeHolder.html("");
                placeHolder.append("<h2>Available Documents for " + sAppType + " #" + sLoanNumber + "</h2>");
				var tbl = $("<div/>", { "class": "ml-table" }).appendTo(placeHolder);
				_.forEach(response, function (doc) {
					var titleRow = $("<div/>").appendTo(tbl);
					titleRow.append($("<div/>", {"class": "text-right"}).text("Title:"));
					titleRow.append($("<div/>").text(doc.title));
					var dateRow = $("<div/>").appendTo(tbl);
					dateRow.append($("<div/>", { "class": "text-right" }).text("Created:"));
					dateRow.append($("<div/>").text(moment(doc.createDate).format("MM/DD/YYYY hh:mm:ss A")));
					var viewBtn = $("<a/>", { "href": "#" }).text("View");
					viewBtn.on("click", function() {
						viewDocContent(doc.id);
					});
					var viewRow = $("<div/>").appendTo(tbl);
					viewRow.append($("<div/>"));
					$("<div/>").append(viewBtn).appendTo(viewRow);
					$("<div/>", {"class": "separator-row"}).appendTo(tbl);
				});
			}
			$("#popDocList").popup("open", { "positionTo": "window" });
        },
		error: function() {
			$("div.content-panel", "#popDocList").html('Error! Please try again.');
			$("#popDocList").popup("open", { "positionTo": "window" });
		}
    });
}
function backToDocList() {
	goToNextPage("#viewSubmitPage", function () {
		setTimeout(function() {
			$("#popDocList").popup("open", { "positionTo": "window" });
		}, 1000);
	});

}
function viewDocContent(id) {
	var selectedDoc = _.find(g_vsl_selected_doc_list, { id: id });
	if (!selectedDoc) return;

	var $placeHolder = $("#popupDocViewer .content-panel");
	// Loaded via <script> tag, create shortcut to access PDF.js exports.
	var pdfjsLib = window['pdfjs-dist/build/pdf'];

	// The workerSrc property shall be specified.
	pdfjsLib.GlobalWorkerOptions.workerSrc = '/PdfViewer/build/pdf.worker.js';

	// Using DocumentInitParameters object to load binary data.
	$placeHolder.html("");
	for (var i = 0; i < selectedDoc.pageCount; i++) {
		$("<canvas/>", {"style": "width:100%;"}).appendTo($placeHolder);
		
	}
	var loadingTask = pdfjsLib.getDocument({ data: atob(selectedDoc.content) });
	loadingTask.promise.then(function (pdf) {
		console.log('PDF loaded');
		// Fetch the first page
		var pageNumber = 1;
		for (var j = 0; j < selectedDoc.pageCount; j++) {
			pageNumber = j + 1;
			pdf.getPage(pageNumber).then(function (page) {
				console.log('Page loaded');
				var scale = 1;
				var viewport = page.getViewport(scale);
				// Prepare canvas using PDF page dimensions
				var canvas = $("#popupDocViewer canvas")[page.pageIndex];
				var context = canvas.getContext('2d');
				canvas.height = viewport.height;
				canvas.width = viewport.width;

				// Render PDF page into canvas context
				var renderContext = {
					canvasContext: context,
					viewport: viewport
				};
				var renderTask = page.render(renderContext);
				renderTask.promise.then(function () {
					console.log('Page rendered');
				});
			});
		}
		goToNextPage("#popupDocViewer");
	}, function (reason) {
		// PDF loading error
		console.error(reason);
	});

	
}
function submitDocument() {
	if (docUploadObj.validateUploadDocument() == false) {
		return false;
	}
    //var message = validateUploadDocument();
    //if (message != '') {
    //    $('#divResultMessage').html('<span style="color: red;">' + message + '<span>');
    //    return false;
    //}
    var appInfo = new Object();
    appInfo.lenderref = $('#hfLenderRef').val();
    appInfo.command = "SubmitImage";
    appInfo.LoanNumber = g_vsl_selected_loan_num;
    appInfo.LoanId = g_vsl_selected_loan_id;
    appInfo.AppType = g_vsl_selected_app_type; 
    var docUploadInfo = {};
    if (typeof docUploadObj != "undefined" && docUploadObj != null) {
        docUploadObj.setUploadDocument(docUploadInfo);
    }
    if (docUploadInfo.hasOwnProperty("sDocArray") && docUploadInfo.hasOwnProperty("sDocInfoArray")) {
        appInfo.Image = JSON.stringify(docUploadInfo.sDocArray);
        appInfo.UploadDocInfor = JSON.stringify(docUploadInfo.sDocInfoArray);
    }

    $.ajax({
        type: 'POST',
        url: 'ViewSubmittedLoans.aspx',
        data: appInfo,
        dataType: 'html',
        success: function (response) {
            var errorMessage='Please select at least 1 document to be submitted';
            if (response.toString().indexOf(errorMessage) != -1) {
                $('#divResultMessage').html(response);
            } else {
                $('#txtSuccessMessage').html(response);
                //  window.setTimeout(function () {
                $.mobile.changePage($('#divSuccessDialog'), {
                    transition: "slide",
                    reverse: false,
                    changeHash: true
                });
                //}, 10000);
            }
         
        },
        error: function (err) {
            $('#divResultMessage').html('<span style="color: red;">We apologize, but unfortunately there was an error when submitting image(s). Please try again later.<span>');
        }
    });

    return false;
}

function onKeydownIcon(event) {
    if(event.key == "Enter") {
        event.target.click();
    }
}

function openViewMessagePopup(obj) {
    g_vsl_selected_loan_id = $(obj).attr('loan_id');
    g_vsl_selected_loan_num = $(obj).attr('loan_num');
    g_vsl_selected_email = $(obj).attr('email');
    g_vsl_selected_app_type = $(obj).attr('app_type'); 
    //send open message to lender side
    $.ajax({
        type: 'POST',
        url: 'ViewSubmittedLoans.aspx',
        dataType: 'json',
        data: {
            command: "OpenMessage",
            LoanID: g_vsl_selected_loan_id,
            lenderref: $('#hfLenderRef').val(),
            LoanNumber: g_vsl_selected_loan_num,
            EmailAddress: g_vsl_selected_email,
            AppType: g_vsl_selected_app_type         
        }
    });
    
    $.mobile.changePage($('#viewMessage'), {
        transition: "pop",
        reverse: false,
        changeHash: true
    });

    return false;
}

function openVerifyDepositPopup(ele) {
	$("#btnDoVerification", $("#popVerifyDeposit")).data("loan-id", $(ele).attr("loan_id"));
	$("#btnDoVerification", $("#popVerifyDeposit")).data("loan-num", $(ele).attr("loan_num"));
	$("#btnDoVerification", $("#popVerifyDeposit")).data("source-ele", $(ele));
	$("#popVerifyDeposit").find("input.money").val("");
	$("#popVerifyDeposit").popup("open", { "positionTo": "window" });
}
function exceedFailRetriesPopup(ele) {
	$("#divVerifyDepositResult", $("#popVerifyDepositResult")).html("We're sorry, but we're unable to verify your funds. Please call our branch for assistance.");
	$("#popVerifyDepositResult").data("result-status", "EXCEED");
	$("#popVerifyDepositResult").popup("open", { "positionTo": "window" });
}
function doVerificationDeposit(ele) {
	if ($.lpqValidate("VerifyDepositAmount")) {
		$.ajax({
			type: 'POST',
			url: 'ViewSubmittedLoans.aspx',
			dataType: 'json',
			data: {
				command: "VerifyMinDeposit",
				LoanID: $(ele).data('loan-id'),
				LenderRef: $('#hfLenderRef').val(),
				LoanNumber: $(ele).data('loan-num'),
				DepositAmount1: Common.GetFloatFromMoney($('#txtDepositAmount1').val()),
				DepositAmount2: Common.GetFloatFromMoney($('#txtDepositAmount2').val())
			},
			success: function (response) {
				$("#divVerifyDepositResult", $("#popVerifyDepositResult")).html(response.Message);
				$("#popVerifyDeposit").popup("close");
				if (response.IsSuccess == true) {
					var $sourceEle = $("#btnDoVerification", $("#popVerifyDeposit")).data("source-ele");
					$sourceEle.remove();
				}
				$("#popVerifyDepositResult").data("result-status", response.Info);
				$("#popVerifyDepositResult").popup("open", { "positionTo": "window" });
				if (response.Info == "EXCEED") {
					$("div.js-verify[loan_num='" + $(ele).data('loan-num') + "']").attr('onclick', '').unbind('click').on("click", exceedFailRetriesPopup);

				}
			},
			error: function (err) {
				$("#divVerifyDepositResult", $("#popVerifyDepositResult")).html("Problem with doing deposit verification, please try again another time.");
				$("#popVerifyDepositResult").popup("open", { "positionTo": "window" });
			}
		});
	}
}

function SendMessage() {
    if ($('#tbMsg').val().trim() == "") {
        var strMessage = "<font color='red'>Message cannot be empty.</font>";
        $('#spMessageSendEror').html(strMessage);
        return;
    }
    else {
        $('#spMessageSendEror').html('');
    }
    $.ajax({
        type: 'POST',
        url: 'ViewSubmittedLoans.aspx',
        dataType: 'json',
        data: {
            command: "SendMessage",
            LoanID: g_vsl_selected_loan_id,
            lenderref: $('#hfLenderRef').val(),
            LoanNumber: g_vsl_selected_loan_num,
            EmailAddress: g_vsl_selected_email,
            message: $('#tbMsg').val(),
            AppType: g_vsl_selected_app_type
        },
        success: function (response) {
            if (response.IsSuccess) {
                $('#tbMsg').val('');
                //reload conversation log
                $('#ContentFrame').attr('src', 'WebMessagingView.aspx?LoanID=' + g_vsl_selected_loan_id + '&lender_ref=' + $('#hfLenderRef').val() + '&loan_number=' + g_vsl_selected_loan_num + '&AppType=' + g_vsl_selected_app_type);
            } else {
                alert("Unable to send message, please try again later.");
            }
        }
    });
}
function toggleSSNText(btn) {
    return Common.ToggleSSNText(btn);
}


