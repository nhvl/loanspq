﻿Imports System.Net
Imports System.Xml
Imports System.IO
Imports CustomList
Imports LPQMobile.DBManager
Imports DBUtils
Imports System.Data
Imports System.IdentityModel
Imports System.Net.Mail
Imports Newtonsoft.Json
Imports LPQMobile.BO
Imports LPQMobile.Utils
Imports LPQMobile
Imports System.Security.Cryptography.X509Certificates
Imports System.Net.Security
Imports System.Web.Script.Serialization
Imports System.Globalization

''' <summary>
''' Get loan status via 1 of 3 methods:
''' 1)GUI
''' 2)Token
''' 3)Url parameter: lnam,ssn, email
''' </summary>
''' <remarks></remarks>
Partial Class cu_ViewSubmittedLoans
	Inherits CBasePage

	Private _bSaveXMLs As Boolean = False 'should be set to false for production
	Private constantSearchLoanUrl As String = "/searchLoans/searchLoans.aspx"
	Private constantGetLoanUrl As String = "/getLoans/getLoans.aspx"
	Protected Const constLoanSubmitURL As String = "/SubmitLoan/SubmitLoan.aspx"
	Private SearchLoanURL As String
	Private GetLoanURL As String
	Private _SearchParameter As String = "SSN|LNAME|EMAIL" 'use 3 for sso, 2 for token

	Private _xmlResponse As New XmlDocument
	Private _xmlLoanDetail As New XmlDocument

	Protected _RenderedLoanStatus As String
	Protected upLoadDocumentMessage As String = ""
	Protected xaUpLoadDocumentMessage As String = ""
	Protected blUpLoadDocumentMessage As String = ""
	Protected upLoadDocumentEnabled As Boolean = False
	Protected xaUpLoadDocumentEnabled As Boolean = False
	Protected blUpLoadDocumentEnabled As Boolean = False
	'Protected _visibleUploadDocs As String

	Const SSOTokenExpireInterval As Integer = 60
	Private _Method As String = "GUI"	'SSO, TOKEN
	Protected _bgTheme As String
	Protected _showGUI As Boolean = False
	Protected requiredDocTitle As String = ""
	Protected xaRequiredDocTitle As String = ""
	Protected blRequiredDocTitle As String = ""
	Protected docTitleOptionDic As Dictionary(Of String, List(Of CDocumentTitleInfo))

#Region "input parameter"
	Private ReadOnly Property Token As String
		Get
			Return Request("Token")
		End Get

	End Property

	Private _LName As String

	Protected Property LName As String
		Get
			If Request("lname") <> "" Then 'url parameter
				Return HttpUtility.UrlDecode(SafeString(Request("lname")).Trim())
			End If
			If Request.Form("LastName") <> "" Then	'ajax post
				Return SafeString(Request.Form("LastName")).Trim()
			End If
			Return _LName
		End Get
		Set(ByVal value As String)
			_LName = value
		End Set

	End Property

	Protected ReadOnly Property LNameDisabledString As String
		Get
			Return If(Common.SafeString(HttpUtility.UrlDecode(Request("LName"))) <> "", "disabled", "")
		End Get
	End Property

	Protected ReadOnly Property Email As String
		Get
			If Request.Form("EmailAddr") <> "" Then
				Return SafeString(Request.Form("EmailAddr")).Trim()
			End If
			Return HttpUtility.UrlDecode(SafeString(Request("email")).Trim())
		End Get

	End Property

	Private _SSN As String = ""
	Protected Property SSN As String
		Get
			If Request("ssn") <> "" Then
				Return HttpUtility.UrlDecode(SafeString(Request("ssn")).Replace("/", "").Replace("\", "").Replace("-", ""))
			End If
			If Request.Form("SSN") <> "" Then
				Return Request.Form("SSN").Replace("/", "").Replace("\", "").Replace("-", "")
			End If
			Return _SSN
		End Get
		Set(ByVal value As String)
			_SSN = value
		End Set
	End Property

	Protected ReadOnly Property SSN1() As String
		Get
			If SSN.Length > 2 Then
				Return SSN.Substring(0, 3)
			Else
				Return ""
			End If
		End Get
	End Property
	Protected ReadOnly Property SSN2() As String
		Get
			If SSN.Length > 4 Then
				Return SSN.Substring(3, 2)
			Else
				Return ""
			End If
		End Get
	End Property
	Protected ReadOnly Property SSN3() As String
		Get
			If SSN.Length > 8 Then
				Return SSN.Substring(5, 4)
			Else
				Return ""
			End If
		End Get
	End Property


	Protected ReadOnly Property SSNDisabledString As String
		Get
			Return If(Common.SafeString(HttpUtility.UrlDecode(Request.Params("SSN"))) <> "", "disabled", "")
		End Get
	End Property

	'Ex:DI, already in base class
	'Protected ReadOnly Property PlatformSource() As String
	'	Get
	'		Return Common.SafeString(Request.Params("platform_source"))
	'	End Get
	'End Property

#End Region

	Public ReadOnly Property TextAndroidTel As String
		Get
			If String.IsNullOrEmpty(_textAndroidTel) Then
				Dim useragent As String = If(Request.ServerVariables("HTTP_USER_AGENT"), "")
				_textAndroidTel = If(useragent.ToLower.Contains("android"), "tel", "text")
			End If
			Return _textAndroidTel
		End Get
	End Property

	Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
		loginfo("Page loading")
		ucPageHeader._currentConfig = _CurrentWebsiteConfig
		''ucDocCapture._ddlUpLoadDocPath = "ENUMS/LOAN_DOC_TITLE/ITEM"
		' ''ucDocCapture._ddlUpLoadDocPath = "ENUMS/XA_DOC_TITLE/ITEM"
		upLoadDocumentMessage = Server.HtmlEncode(Common.getUploadDocumentMessage(_CurrentWebsiteConfig)).Replace(vbCr, "").Replace(vbLf, "").Replace(vbTab, "")
		blUpLoadDocumentMessage = Server.HtmlEncode(Common.getBLUploadDocumentMessage(_CurrentWebsiteConfig)).Replace(vbCr, "").Replace(vbLf, "").Replace(vbTab, "")
		xaUpLoadDocumentMessage = Server.HtmlEncode(Common.getXAUploadDocumentMessage(_CurrentWebsiteConfig)).Replace(vbCr, "").Replace(vbLf, "").Replace(vbTab, "")

		upLoadDocumentEnabled = Common.getUploadDocumentEnable(_CurrentWebsiteConfig)
		xaUpLoadDocumentEnabled = Common.getXAUploadDocumentEnable(_CurrentWebsiteConfig)
		blUpLoadDocumentEnabled = Common.getBLUploadDocumentEnable(_CurrentWebsiteConfig)
		If Not IsPostBack Then

			'already inherit CheckIPAccessDeniedcheck via CBasePage
			'restrict too many request from the same IP
			'Dim IP_ErrMessage As String = ValidateIPForCU()
			'If Not String.IsNullOrEmpty(IP_ErrMessage) Then
			'    Response.Write(IP_ErrMessage)
			'    log.Info(IP_ErrMessage & "  IP: " & Request.UserHostAddress & "/" & _CurrentLenderRef)
			'    Return
			'End If

			Dim command As String = Common.SafeString(Request.Params("command"))
			''Dim docTitlePath As String = Common.SafeString(Request.Params("doc_title_path"))

			''If command = "changeDocTitlePath" And docTitlePath <> "" Then
			''    ucDocCapture._ddlUpLoadDocPath = docTitlePath
			''    ucDocCapture.RenderUploadDocDropdown()

			''    Response.Clear()
			''    Response.Write(JsonConvert.SerializeObject(New With {.data = ucDocCapture._ddlUploadDocument}))
			''    Response.End()
			''    Return
			''End If

            initialize()

			''open message
			If command = "OpenMessage" Then
				SendMsgHandler(True)
				Return
			End If

			If command = "SendMessage" Then
				Try
					SendMsgHandler()
					Response.Clear()
					Response.Write(JsonConvert.SerializeObject(New With {.IsSuccess = True}))
				Catch ex As Exception
					Response.Clear()
					Response.Write(JsonConvert.SerializeObject(New With {.IsSuccess = False}))
				End Try
				Response.End()
				Return
			End If
			If command = "VerifyMinDeposit" Then
				DoDepositVerification()
				Return
			End If
			If command = "SendSecurityCode" Then
				DoSendingSecurityCode()
				Return
			End If
			If command = "LoadDocList" Then
				LoadDocList()
				Return
			End If
			If command = "SubmitImage" Then	' doc upload is only available via GUI but not SSO or token
				Dim sResponse = submitImage()
				Response.Clear()
				Response.Write(sResponse)
				Response.End()
				Return
			End If
			If command = "SearchLoan" Then
				Dim sResponse = ExecSearchLoan()
				Response.Clear()
				Response.Write(sResponse)
				Response.End()
				Return
			End If

		End If
		ucPageHeader._headerDataTheme = _HeaderDataTheme
		ucPageHeader._footerDataTheme = _FooterDataTheme
		ucPageHeader._contentDataTheme = _ContentDataTheme
		ucPageHeader._headerDataTheme2 = _HeaderDataTheme2
		ucPageHeader._backgroundDataTheme = _BackgroundTheme
		ucPageHeader._buttonDataTheme = _ButtonTheme
		ucPageHeader._buttonFontColor = _ButtonFontTheme
	End Sub

	Private Function GetCustomList(loanNodeName As String) As List(Of CCustomListRuleItem)
		Dim result As New List(Of CCustomListRuleItem)
		Dim loanClrNode As XmlNode = _CurrentWebsiteConfig._webConfigXML.SelectSingleNode(loanNodeName & "/CUSTOM_LIST_RULES")
		If loanClrNode IsNot Nothing AndAlso Not String.IsNullOrEmpty(loanClrNode.InnerText) Then
			Dim loanClrList = JsonConvert.DeserializeObject(Of List(Of CCustomListRuleItem))(loanClrNode.InnerText)
			'Dim loanClrList = oSerializer.Deserialize(Of List(Of CCustomListRuleItem))(loanClrNode.InnerText)
			If loanClrList IsNot Nothing AndAlso loanClrList.Count > 0 AndAlso loanClrList.Any(Function(p) p.Enable = True AndAlso p.List IsNot Nothing) Then
				result.AddRange(loanClrList.Where(Function(p) p.Enable = True AndAlso p.List IsNot Nothing))
			End If
		End If
		Return result
	End Function

	Private Function GetDocTitleOptions(loanType As String, customList As List(Of CCustomListRuleItem)) As List(Of CDocumentTitleInfo)
		Dim result As List(Of CDocumentTitleInfo) = New List(Of CDocumentTitleInfo)
		If customList IsNot Nothing AndAlso customList.Any(Function(c) c.Code = "DOCUMENT_TITLES") Then
			Dim clrDocTitle = EvaluateCustomList(Of CDocumentTitleInfo)("DOCUMENT_TITLES", customList)
			If clrDocTitle IsNot Nothing Then
				result = New List(Of CDocumentTitleInfo) From {New CDocumentTitleInfo() With {.Group = "", .Code = "", .LoanType = "", .Title = "--Please select title--"}}
				result.AddRange(clrDocTitle.OrderBy(Function(t) t.Title))
			End If
		Else
			'fallback
			Dim xPath As String = "ENUMS/LOAN_DOC_TITLE"
			If loanType = "XA" Then
				xPath = "ENUMS/XA_DOC_TITLE"
			ElseIf loanType = "BL" Then
				xPath = "ENUMS/BL_DOC_TITLE"
			End If
			result = Common.GetDocumentTitleInfoList(_CurrentWebsiteConfig._webConfigXML.SelectSingleNode(xPath))
		End If
		Return result
	End Function

	Private Sub initialize()
		'' app_type ='FOREIGN'
		Dim appType As String = _CurrentWebsiteConfig.AppType
		hdForeignAppType.Value = ""	'' initial hidden value foreign
		''Make sure appType is not nothing
		If Not String.IsNullOrEmpty(appType) Then
			If appType.ToUpper() = "FOREIGN" Then
				hdForeignAppType.Value = "Y"
			End If
		End If
		''backgroud theme
		If String.IsNullOrEmpty(_BackgroundTheme) Then
			_bgTheme = _FooterDataTheme
		Else
			_bgTheme = _BackgroundTheme
		End If
		'determine method
		If LName <> "" And Email <> "" And SSN <> "" Then
			_Method = "SSO"
		ElseIf Token <> "" Then
			_Method = "TOKEN"
		Else
			_Method = "GUI"
		End If

		Dim customListDic As New Dictionary(Of String, List(Of CCustomListRuleItem))
		customListDic.Add("XA", GetCustomList("XA_LOAN"))
		customListDic.Add("CC", GetCustomList("CREDIT_CARD_LOAN"))
		customListDic.Add("HE", GetCustomList("HOME_EQUITY_LOAN"))
		customListDic.Add("PL", GetCustomList("PERSONAL_LOAN"))
		customListDic.Add("VL", GetCustomList("VEHICLE_LOAN"))
		customListDic.Add("BL", GetCustomList("BUSINESS_LOAN"))
		docTitleOptionDic = New Dictionary(Of String, List(Of CDocumentTitleInfo))
		docTitleOptionDic.Add("XA", GetDocTitleOptions("XA", customListDic("XA")))
		docTitleOptionDic.Add("CC", GetDocTitleOptions("CC", customListDic("CC")))
		docTitleOptionDic.Add("HE", GetDocTitleOptions("HE", customListDic("HE")))
		docTitleOptionDic.Add("PL", GetDocTitleOptions("PL", customListDic("PL")))
		docTitleOptionDic.Add("VL", GetDocTitleOptions("VL", customListDic("VL")))
		docTitleOptionDic.Add("BL", GetDocTitleOptions("BL", customListDic("BL")))
		requiredDocTitle = Common.getNodeAttributes(_CurrentWebsiteConfig, "BEHAVIOR", "required_doc_title")
		xaRequiredDocTitle = Common.getNodeAttributes(_CurrentWebsiteConfig, "XA_LOAN/BEHAVIOR", "required_doc_title")
		blRequiredDocTitle = Common.getNodeAttributes(_CurrentWebsiteConfig, "BUSINESS_LOAN/BEHAVIOR", "required_doc_title")

		'divGUI.Visible = False
		If _Method = "GUI" Then	''use gui interface and callback page
			'divGUI.Visible = True
			_showGUI = True
			Return
		End If

		If _Method = "TOKEN" Then
			If Not RetrieveBySSOToken(Token) Then	 'set ssn, lname
				_RenderedLoanStatus = "<div><strong>Token is not found or expired.</strong></div>"
				loginfo("Token is not found or expired.")
				Return
			End If

			If SSN = "" Or LName = "" Then
				'_RenderedLoanStatus = "<div><strong>Missing Parameter(s)</strong></div>"
				loginfo("Token doesn't have parameters so switch to GUI mode")
				_showGUI = True
				Return
			End If

			''want to default to GUI instead
			'If SSN = "" Or LName = "" Then
			'	_RenderedLoanStatus = "<div><strong>Missing Parameter(s)</strong></div>"
			'	loginfo("Token doesn't have parameters")
			'	Return
			'End If
			_SearchParameter = "SSN|LNAME"	 'use only 2 paramter for token method
		End If

		'90 days
		Dim startSearchDate As String = Now.AddDays(-90).ToString("MM/dd/yyyy")
		Dim EndSearchDate As String = Now.AddDays(1).ToString("MM/dd/yyyy")

		' Prepare the various URLs
		'Dim baseSubmitURL = _CurrentWebsiteConfig.SubmitLoanUrl
		'baseSubmitURL = Replace(baseSubmitURL, "/decisionloan/decisionloan.aspx", "")
		Dim baseSubmitURL = _CurrentWebsiteConfig.BaseSubmitLoanUrl
		Dim concatURL As String
		concatURL = baseSubmitURL + constantSearchLoanUrl
		SearchLoanURL = concatURL
		GetLoanURL = baseSubmitURL + constantGetLoanUrl

		Try
			If Not searchLoans(startSearchDate, EndSearchDate, _SearchParameter) Then
				_RenderedLoanStatus = "Please try again later"
				Return
			End If
			_RenderedLoanStatus = RenderLoanStatus()
		Catch ex As Exception
			_RenderedLoanStatus = "Please try again later"
			logError(ex.Message, ex)
		End Try
	End Sub
	Private Sub LoadDocList()
		Dim responseContent As String = ""
		Try
			Dim sUrl = _CurrentWebsiteConfig.BaseSubmitLoanUrl.Substring(0, _CurrentWebsiteConfig.BaseSubmitLoanUrl.IndexOf("services")) + "services/Docs/GetAppDocs/GetAppDocs.aspx"
			Dim req As WebRequest = WebRequest.Create(sUrl)
			req.Method = "POST"
			req.ContentType = "text/xml"
			Dim sloanNumber = Common.SafeString(Request.Form("loanNum"))
			Dim sAppType = Common.SafeString(Request.Form("appType"))
			'for testing
			'sloanNumber = "13267"
			'sAppType = "CC"

			Using requestStream = req.GetRequestStream()
				Dim writer As New StreamWriter(requestStream)
				Dim xmlRequest As String = String.Format("<?xml version='1.0' encoding='UTF-8'?><INPUT><LOGIN><API_CREDENTIALS api_user_id='{0}' api_password='{1}'/></LOGIN><REQUEST app_type='{2}' app_num='{3}' document_types='ARCHIVED'/></INPUT>", _CurrentWebsiteConfig.APIUser, _CurrentWebsiteConfig.APIPassword, sAppType, sloanNumber)
				log.Debug(String.Format("Request to {0} : {1}", sUrl, CSecureStringFormatter.MaskSensitiveXMLData(xmlRequest)))
				writer.Write(xmlRequest)
				writer.Close()

				Using res As WebResponse = req.GetResponse()
					Using stream As Stream = res.GetResponseStream()
						Dim reader As New StreamReader(stream)
						Dim sXml As String = reader.ReadToEnd()
						'log.Debug(String.Format("Response(system service retrieval) from {0} : {1}", sUrl, sXml))
						reader.Close()
						Dim responseXML As XmlDocument = New XmlDocument()
						responseXML.LoadXml(sXml)
						Dim errorNode = responseXML.SelectSingleNode("OUTPUT/ERROR")
						If errorNode IsNot Nothing Then
							responseContent = "error"
						Else
							Dim documentNodes = responseXML.SelectNodes("OUTPUT/RESPONSE/DOCUMENTS/ARCHIVED_DOCUMENT_GROUP/DOCUMENT")
							If documentNodes IsNot Nothing AndAlso documentNodes.Count > 0 Then
								Dim docList As New List(Of CArchivedDocument)
								For Each node As XmlNode In documentNodes
									Dim docItem As New CArchivedDocument
									docItem.Title = Common.SafeString(node.Attributes("title").Value)
									docItem.CreateDate = Common.SafeDate(node.Attributes("create_date").Value)
									docItem.PdfId = Common.SafeString(node.Attributes("pdf_id").Value)
									docItem.PageCount = Common.SafeInteger(node.Attributes("page_count").Value)
									Dim fileContentNode = node.SelectSingleNode("FILE_CONTENT")
									If fileContentNode IsNot Nothing Then
										docItem.FileContentBase64 = node.InnerText
									End If
									docList.Add(docItem)
								Next
								responseContent = JsonConvert.SerializeObject(docList.OrderByDescending(Function(p) p.CreateDate).Select(Function(p) New With {.title = p.Title, .createDate = p.CreateDate, .id = p.PdfId, .pageCount = p.PageCount, .content = p.FileContentBase64}))
							End If
						End If
					End Using
					res.Close()
				End Using
				requestStream.Close()
			End Using
		Catch ex As Exception
			responseContent = "error"
			log.Error(ex.Message, ex)
		End Try
		If responseContent = "" Or responseContent = "error" Then
			responseContent = JsonConvert.SerializeObject(New With {.message = responseContent})
		End If
		Response.Clear()
		Response.Write(responseContent)
		Response.End()
	End Sub
	Private Function ExecSearchLoan() As String

		'90 days
		Dim startSearchDate As String = Now.AddDays(-90).ToString("MM/dd/yyyy")
		Dim EndSearchDate As String = Now.AddDays(1).ToString("MM/dd/yyyy")
		'check parameter and set query string
		If SSN <> "" And LName <> "" And Email <> "" Then
			_SearchParameter = "SSN|LNAME|EMAIL"
		ElseIf LName <> "" AndAlso Email <> "" AndAlso SSN = "" Then
			_SearchParameter = "LNAME|EMAIL"
		Else
			'Response.Write("Missing Parameter(s)")
			loginfo("Missing Parameter(s)")
			Return "Missing Parameter(s)"
		End If

		'check security code(if any)
		If _SearchParameter = "LNAME|EMAIL" Then
			Dim securityCode As String = Common.SafeString(Request.Form("SecurityCode"))
			Dim lenderRef As String = Common.SafeString(Request.Form("lenderref"))
			If String.IsNullOrEmpty(securityCode) OrElse Not ValidateSecurityCode(Email, LName, securityCode, lenderRef) Then
				loginfo("Invalid Security Code")
				Return "InvalidCode"
			End If
		End If



		' Prepare the various URLs
		Dim baseSubmitURL = _CurrentWebsiteConfig.BaseSubmitLoanUrl

		Dim concatURL As String
		concatURL = baseSubmitURL + constantSearchLoanUrl
		SearchLoanURL = concatURL
		GetLoanURL = baseSubmitURL + constantGetLoanUrl

		Try
			If Not searchLoans(startSearchDate, EndSearchDate, _SearchParameter) Then
				_RenderedLoanStatus = "Please try again later"
			End If
			_RenderedLoanStatus = RenderLoanStatus(_SearchParameter)
		Catch ex As Exception
			_RenderedLoanStatus = "Please try again later"
			logError(ex.Message, ex)
		End Try

		Return _RenderedLoanStatus
	End Function

	Private Function ValidateSecurityCode(mail As String, lastname As String, code As String, lenderRef As String) As Boolean
		Dim db As New CSQLDBUtils(LPQMOBILE_CONNECTIONSTRING)
		Try
			Dim oWhere As New CSQLWhereStringBuilder()
			oWhere.AppendAndCondition("Email={0}", New CSQLParamValue(mail))
			oWhere.AppendAndCondition("LastName={0}", New CSQLParamValue(lastname))
			'oWhere.AppendAndCondition("Code={0}", New CSQLParamValue(code))
			oWhere.AppendAndCondition("LenderRef={0}", New CSQLParamValue(lenderRef))
			oWhere.AppendAndCondition("Status={0}", New CSQLParamValue(0))
			Dim sqlstr As String = "SELECT top 1 * FROM GetLoanStatusSecurityCodes" & oWhere.SQL & " ORDER BY GeneratedDate DESC"

			Dim dt As DataTable
			dt = db.getDataTable(sqlstr)
			If Not String.IsNullOrEmpty(code) AndAlso dt.Rows.Count > 0 AndAlso Common.SafeString(dt.Rows(0)("Code")).ToUpper().Substring(0, 6) = code.ToUpper() Then
				Dim oUpdate As New cSQLUpdateStringBuilder("GetLoanStatusSecurityCodes", oWhere)
				oUpdate.appendvalue("Status", New CSQLParamValue(1))
				oUpdate.appendvalue("ConfirmedDate", New CSQLParamValue(DateTime.Now))
				db.executeNonQuery(oUpdate.SQL, False)
				Return True
			End If
		Catch ex As Exception
			log.Error("Error while ValidateSecurityCode", ex)
		Finally
			db.closeConnection()
		End Try
		Return False
	End Function

	Function RetrieveBySSOToken(ByVal psSSOToken As String) As Boolean
		If String.IsNullOrEmpty(psSSOToken) Then
			Return False
		End If

		Dim oWhere As New CSQLWhereStringBuilder()
		oWhere.AppendAndCondition("SSOTokenID = {0}", New CSQLParamValue(Common.SafeString(psSSOToken)))
		Dim sqlstr As String = "SELECT GETDATE() As dbDate, SSOTokenCreateDate, isSSOTokenUsed, Params FROM SSOTokens" & oWhere.SQL

		Dim dt As DataTable
		Using db As New CSQLDBUtils(LPQMOBILE_CONNECTIONSTRING)
			dt = db.getDataTable(sqlstr)
			If dt.Rows.Count = 0 Then
				Return False
			End If
		End Using

		Dim oRow As DataRow = dt.Rows(0)
		If IsDate(oRow("SSOTokenCreateDate")) Then
			Dim dateNow As DateTime = Common.SafeDate(oRow("dbDate"))
			Dim tokenDate As DateTime = Common.SafeDate(oRow("SSOTokenCreateDate"))

			'absolute expiration period
			If DateDiff(DateInterval.Second, tokenDate, dateNow) > SSOTokenExpireInterval Then
				Return False
			End If
			Dim enrypter As New CRijndaelManaged()
			Dim sParams = enrypter.Decrypt(SafeString(oRow("Params")))
			Dim data As New NameValueCollection()
			data = CURLBuilder.GetQueryVars(sParams)
			SSN = SafeString(data("ssn"))
			LName = SafeString(data("lname"))
		End If
		Return True
	End Function

	Private Function searchLoans(ByVal psStartDate As String, ByVal psEndDate As String, ByVal psArg As String) As Boolean
		Dim pOptionalParameter = psArg.ToUpper
		Dim xmlRequestDoc As New XmlDocument

		Dim requestNode As XmlElement = xmlRequestDoc.CreateElement("REQUEST")
		xmlRequestDoc.AppendChild(requestNode)

		Dim loginNode As XmlElement = xmlRequestDoc.CreateElement("LOGIN")
		requestNode.AppendChild(loginNode)
		loginNode.SetAttribute("api_user_id", _CurrentWebsiteConfig.APIUser)
		loginNode.SetAttribute("api_password", _CurrentWebsiteConfig.APIPassword)

		Dim searchQueryNode As XmlElement = xmlRequestDoc.CreateElement("SEARCH_QUERY")
		requestNode.AppendChild(searchQueryNode)

		'--set date param - "AND"
		Dim bIsDateSet As Boolean = False
		If pOptionalParameter.Contains("FUNDINGDATE") Then
			searchQueryNode.SetAttribute("funding_date_start", psStartDate)
			searchQueryNode.SetAttribute("funding_date_end", psEndDate)
			pOptionalParameter = pOptionalParameter.Replace("FUNDINGDATE", "")
			bIsDateSet = True
		End If

		If pOptionalParameter.Contains("DECLINEDDATE") Then
			searchQueryNode.SetAttribute("declined_date_start", psStartDate)
			searchQueryNode.SetAttribute("declined_date_end", psEndDate)
			pOptionalParameter = pOptionalParameter.Replace("DECLINEDDATE", "")
			bIsDateSet = True
		End If
		If pOptionalParameter.Contains("APPROVALDATE") Then
			searchQueryNode.SetAttribute("approval_date_start", psStartDate)
			searchQueryNode.SetAttribute("approval_date_end", psEndDate)
			pOptionalParameter = pOptionalParameter.Replace("APPROVALDATE", "")
			bIsDateSet = True
		End If
		If pOptionalParameter.Contains("BOOKINGDATE") Then
			searchQueryNode.SetAttribute("booking_date_start", psStartDate)
			searchQueryNode.SetAttribute("booking_date_end", psEndDate)
			pOptionalParameter = pOptionalParameter.Replace("BOOKINGDATE", "")
			bIsDateSet = True
		End If

		If Not bIsDateSet Then 'default
			searchQueryNode.SetAttribute("date_start", psStartDate)
			searchQueryNode.SetAttribute("date_end", psEndDate)
		End If

		If pOptionalParameter.Contains("SSN") And pOptionalParameter.Contains("LNAME") And pOptionalParameter.Contains("EMAIL") Then
			searchQueryNode.SetAttribute("borrower_ssn", SSN)
			pOptionalParameter = pOptionalParameter.Replace("SSN", "")
			searchQueryNode.SetAttribute("borrower_lname", LName)
			pOptionalParameter = pOptionalParameter.Replace("LNAME", "")
			searchQueryNode.SetAttribute("borrower_email", Email)
			pOptionalParameter = pOptionalParameter.Replace("EMAIL", "")
		ElseIf pOptionalParameter.Contains("EMAIL") AndAlso pOptionalParameter.Contains("LNAME") AndAlso Not pOptionalParameter.Contains("SSN") Then
			searchQueryNode.SetAttribute("borrower_email", Email)
			pOptionalParameter = pOptionalParameter.Replace("EMAIL", "")
			searchQueryNode.SetAttribute("borrower_lname", LName)
			pOptionalParameter = pOptionalParameter.Replace("LNAME", "")
		ElseIf pOptionalParameter.Contains("SSN") And pOptionalParameter.Contains("LNAME") Then
			searchQueryNode.SetAttribute("borrower_ssn", SSN)
			pOptionalParameter = pOptionalParameter.Replace("SSN", "")
			searchQueryNode.SetAttribute("borrower_lname", LName)
			pOptionalParameter = pOptionalParameter.Replace("LNAME", "")
		End If

		'If pOptionalParameter.Contains("MEMBERNUMBER") And pOptionalParameter.Contains("LNAME") Then
		'	searchQueryNode.SetAttribute("member_number", MemberNumber)
		'	pOptionalParameter = pOptionalParameter.Replace("MEMBERNUMBER", "")
		'	searchQueryNode.SetAttribute("borrower_lname", LName)
		'	pOptionalParameter = pOptionalParameter.Replace("LNAME", "")
		'End If


		'--MapLoan type - "OR"
		Dim sLoanType As String = ""
		Dim oLoanTypeList As New List(Of String)
		If pOptionalParameter.Contains("BL") Then oLoanTypeList.Add("BL")
		If pOptionalParameter.Contains("CC") Then oLoanTypeList.Add("CC")
		If pOptionalParameter.Contains("HE") Then oLoanTypeList.Add("HE")
		If pOptionalParameter.Contains("ML") Then oLoanTypeList.Add("ML")
		If pOptionalParameter.Contains("PL") Then oLoanTypeList.Add("PL")
		If pOptionalParameter.Contains("VL") Then oLoanTypeList.Add("VL")
		If pOptionalParameter.Contains("XA") Then oLoanTypeList.Add("XA")
		If pOptionalParameter.Contains("LOANS_ONLY") Then oLoanTypeList.Add("LOANS_ONLY")
		sLoanType = String.Join(" ", oLoanTypeList.ToArray())
		'If sLoanType.Length > 1 Then
		searchQueryNode.SetAttribute("loan_type", sLoanType)	'empty equal all
		'End If

		'--Map Laon status - "XOR"
		Dim sLoanStatus As String = ""
		If pOptionalParameter.Contains("DEC") Then sLoanStatus = "DEC"
		If pOptionalParameter.Contains("CAN") Then sLoanStatus = "CAN"
		If pOptionalParameter.Contains("APP") Then sLoanStatus = "APP"
		If pOptionalParameter.Contains("REF") Then sLoanStatus = "REF"
		If pOptionalParameter.Contains("AA") Then sLoanStatus = "AA"
		If pOptionalParameter.Contains("INC") Then sLoanStatus = "INC"
		If pOptionalParameter.Contains("CAN") Then sLoanStatus = "CAN"
		'If sLoanStatus.Length > 1 Then
		searchQueryNode.SetAttribute("loan_status", sLoanStatus)  'empty equal all
		'End If

		'searchQueryNode.SetAttribute("loan_number", "12488") '"13465 ML" , "18433 VL", "12488 XA" "18977 ML"
		'Add fitlher for only approved?

		Dim sPath = Server.MapPath("./searchLoanRequest.xml")
		If _bSaveXMLs = True Then
			xmlRequestDoc.Save(sPath)
		End If

		Try

			Dim bIsNotLPQ As Boolean = _CurrentWebsiteConfig.SubmitLoanUrl_TPV <> "" AndAlso _CurrentWebsiteConfig.SubmitLoanUrl_TPV = _CurrentWebsiteConfig.SubmitLoanUrl
			If bIsNotLPQ Then
				SearchLoanURL = String.Format(SearchLoanURL & "?OrgCode2={0}&LenderCode2={1}", _CurrentWebsiteConfig.OrgCode2, _CurrentWebsiteConfig.LenderCode2)
			End If
			log.Debug("Request Data: " & CSecureStringFormatter.MaskSensitiveXMLData(xmlRequestDoc.OuterXml))
			log.Debug("Request Url: " & SearchLoanURL)

			Dim xmlStringResponse As String = post(xmlRequestDoc.OuterXml, SearchLoanURL)

			log.Info("Response Data: " & CSecureStringFormatter.MaskSensitiveXMLData(xmlStringResponse))



			_xmlResponse.LoadXml(xmlStringResponse)


			Dim sPath2 = Server.MapPath("./searchLoanResponse.xml")
			If _bSaveXMLs = True Then
				_xmlResponse.Save(sPath2)
			End If

			If Not parseSearchResponse(xmlStringResponse) Then
				logError("Errors Searching Loans", Nothing)
				Return False
			End If

		Catch ex As Exception
			logError("Errors Searching Loans: " & ex.Message, ex)
			Return False
		End Try
		Return True
	End Function

	Private Function getLoan(ByVal psLoanID As String) As Boolean
		Dim xmlRequestDoc As New XmlDocument

		Dim inputNode As XmlElement = xmlRequestDoc.CreateElement("INPUT")
		xmlRequestDoc.AppendChild(inputNode)

		Dim loginNode As XmlElement = xmlRequestDoc.CreateElement("LOGIN")
		inputNode.AppendChild(loginNode)
		'loginNode.SetAttribute("login", _CurrentWebsiteConfig.APIUser)
		'loginNode.SetAttribute("password", _CurrentWebsiteConfig.APIPassword)
		loginNode.SetAttribute("api_user_id", _CurrentWebsiteConfig.APIUser)
		loginNode.SetAttribute("api_password", _CurrentWebsiteConfig.APIPassword)



		Dim requestNode As XmlElement = xmlRequestDoc.CreateElement("REQUEST")
		inputNode.AppendChild(requestNode)

		Dim loanNode As XmlElement = xmlRequestDoc.CreateElement("LOAN")
		requestNode.AppendChild(loanNode)
		loanNode.SetAttribute("loan_id", psLoanID)

		'searchQueryNode.SetAttribute("loan_number", "12488") '"13465 ML" , "18433 VL", "12488 XA" "18977 ML"
		'Add fitlher for only approved?

		Dim sPath = Server.MapPath("./getLoanRequest.xml")
		If _bSaveXMLs = True Then
			xmlRequestDoc.Save(sPath)
		End If

		Try
			loginfo("Request Data: " & CSecureStringFormatter.MaskSensitiveXMLData(xmlRequestDoc.OuterXml))
			loginfo("Request Url: " & GetLoanURL)

			Dim xmlStringResponse As String = post(xmlRequestDoc.OuterXml, GetLoanURL)

			loginfo("Response Data: " & CSecureStringFormatter.MaskSensitiveXMLData(xmlStringResponse))

			_xmlLoanDetail.LoadXml(xmlStringResponse)

			Dim sPath2 = Server.MapPath("./getLoanResponse.xml")
			If _bSaveXMLs = True Then
				_xmlLoanDetail.Save(sPath2)
			End If

			'
			'If Not parseSearchResponse(xmlStringResponse) Then
			'	logError("Errors Searching Loans", Nothing)
			'	Return False
			'End If

		Catch ex As Exception
			logError("Errors Searching Loans: " & ex.Message, ex)
			Return False
		End Try
		Return True
	End Function

	Private Function setLoan(loanID As String, depositAmount1 As Double, depositAmount2 As Double) As String
		' Make changes to it
		' Submit the changes with the "UPDATE" command
		If loanID = "" Then Return ""
		Dim getLoansXmlRequestStr As String = LoanRetrieval.BuildGetLoansXML(loanID, _CurrentWebsiteConfig.APIUser, _CurrentWebsiteConfig.APIPassword)

		' Done building the XML, now ship it off
		Dim req As WebRequest = WebRequest.Create(_CurrentWebsiteConfig.BaseSubmitLoanUrl + constantGetLoanUrl)
		req.Method = "POST"
		req.ContentType = "text/xml"
		'log.Info("XA: Preparing to POST GetLoans Request: " & CSecureStringFormatter.MaskSensitiveXMLData(getLoansXmlRequestStr))
		Dim writer As New StreamWriter(req.GetRequestStream())
		writer.Write(getLoansXmlRequestStr)
		writer.Close()

		Dim res As WebResponse = req.GetResponse()

		Dim reader As New StreamReader(res.GetResponseStream())
		Dim sXml As String = reader.ReadToEnd()

		log.Info("XA: GetLoans Response: " + CSecureStringFormatter.MaskSensitiveXMLData(sXml))

		' Extract the inner xml
		Dim responseXmlDoc As XmlDocument = New XmlDocument()
		responseXmlDoc.LoadXml(sXml)
		Dim innerXmlStr As String = ""
		innerXmlStr = responseXmlDoc.InnerText
		Dim innerXmlDoc As XmlDocument = New XmlDocument()
		innerXmlDoc.LoadXml(innerXmlStr)

		' get the request wrapper
		Dim origRequestXMLDoc As XmlDocument = New XmlDocument()
		origRequestXMLDoc.LoadXml(getLoansXmlRequestStr)


		Dim bankFundingSourceNode As XmlElement = Nothing
		For Each childNode As XmlNode In innerXmlDoc.GetElementsByTagName("FUNDING_SOURCE")
			If CType(childNode, XmlElement).GetAttribute("funding_type") = "BANK" Then
				bankFundingSourceNode = CType(childNode, XmlElement)
			End If
		Next

		If bankFundingSourceNode.GetAttribute("microdeposit_status") <> "PASS" Then
			Dim minDepositAmount1 As Double = Common.SafeDouble(bankFundingSourceNode.GetAttribute("microdeposit_amount_1"))
			Dim minDepositAmount2 As Double = Common.SafeDouble(bankFundingSourceNode.GetAttribute("microdeposit_amount_2"))
			Dim minFailureCount As Integer = Common.SafeInteger(bankFundingSourceNode.GetAttribute("microdeposit_failure_count"))
			Dim internalCommentAppend As String = ""
			If (minDepositAmount1 + minDepositAmount2).Equals(depositAmount1 + depositAmount2) AndAlso Math.Abs(minDepositAmount1 - minDepositAmount2).Equals(Math.Abs(depositAmount1 - depositAmount2)) Then
				internalCommentAppend = "MICRODEPOSIT VERIFICATION SUCCESS"
				bankFundingSourceNode.SetAttribute("microdeposit_status", "PASS")
			Else
				internalCommentAppend = "MICRODEPOSIT VERIFICATION FAILED"
				bankFundingSourceNode.SetAttribute("microdeposit_failure_count", (minFailureCount + 1).ToString())
			End If
			Dim internCommentNodes As XmlNodeList = innerXmlDoc.GetElementsByTagName("INTERNAL_COMMENTS")
			Dim internCommentNode As XmlElement = CType(internCommentNodes(0), XmlElement)
			internalCommentAppend = "SYSTEM (" + DateTime.Now + "): " & internalCommentAppend & Environment.NewLine & Environment.NewLine
			internCommentNode.InnerText = internalCommentAppend & internCommentNode.InnerText
			Dim cdataSection As XmlCDataSection
			origRequestXMLDoc.SelectSingleNode("//LOAN_DATA").RemoveAll()
			cdataSection = origRequestXMLDoc.CreateCDataSection(innerXmlDoc.InnerXml)
			origRequestXMLDoc.SelectSingleNode("//LOAN_DATA").AppendChild(cdataSection)

			' Add the loan_id to LOAN_DATA
			Dim loanIDAttributeElement As XmlElement = origRequestXMLDoc.SelectSingleNode("//REQUEST")
			'loanIDAttributeElement.RemoveAllAttributes()
			loanIDAttributeElement.SetAttribute("loan_id", loanID)

			Dim loanDataElement As XmlElement = CType(origRequestXMLDoc.SelectSingleNode("//LOAN_DATA"), XmlElement)
			loanDataElement.SetAttribute("loan_type", "XA")
			loanDataElement = CType(origRequestXMLDoc.SelectSingleNode("//REQUEST"), XmlElement)
			loanDataElement.SetAttribute("action_type", "UPDATE")
			log.Info("XA: Preparing to Update loan data: " + CSecureStringFormatter.MaskSensitiveXMLData(origRequestXMLDoc.InnerXml))

			' Now send the loan request back with the updated "APPROVED_ACCOUNTS"

			req = WebRequest.Create(_CurrentWebsiteConfig.BaseSubmitLoanUrl + constLoanSubmitURL)
			req.Method = "POST"
			req.ContentType = "text/xml"
			writer = New StreamWriter(req.GetRequestStream())
			writer.Write(origRequestXMLDoc.InnerXml)
			writer.Close()

			res = req.GetResponse()

			reader = New StreamReader(res.GetResponseStream())
			sXml = reader.ReadToEnd()
			log.Debug("updateloan result: " & sXml)
			Return sXml
		End If
		Return ""
	End Function

	Private Function submitImage() As String
		Dim sloanNumber = Common.SafeString(Request.Form("LoanNumber"))
		Dim sloanId = Common.SafeString(Request.Form("LoanId"))
		Dim sAppType = Common.SafeString(Request.Form("AppType"))
		Dim SubmitURL = _CurrentWebsiteConfig.BaseSubmitLoanUrl
		Dim uploadDocumentURL = SubmitURL.Substring(0, SubmitURL.IndexOf("services")) + "Services/ipad/iPadDocumentSubmit.aspx"
		Dim poBogusLoanApp As New CCreditCard("", "")
		Dim oSelectDocsList As New List(Of selectDocuments)
		Dim oSerializer As New JavaScriptSerializer()
		Dim oDocsList As New Dictionary(Of String, String)
		''by default MaxJsonLenth = 102400, set it =int32.maxvalue = 2147483647 
		oSerializer.MaxJsonLength = Int32.MaxValue

		If Not String.IsNullOrEmpty(Request.Form("Image")) Then
			'TODO
			'' oDocsList = oSerializer.Deserialize(Of Dictionary(Of String, String))(Request.Params("Image"))
			'need to work on serialization
			''oDocsList.Add("test title.png", Request.Params("Image"))
			oSelectDocsList = oSerializer.Deserialize(Of List(Of selectDocuments))(Request.Form("Image"))
			If oSelectDocsList.Count = 0 Then
				Return "<span style='color: red'>Please select at least 1 document to be submitted.</span>"
			End If
			If oSelectDocsList.Count > 0 Then
				Dim i As Integer
				For i = 0 To oSelectDocsList.Count - 1
					Dim oTitle As String = oSelectDocsList(i).title
					Dim obase64data As String = oSelectDocsList(i).base64data
					oDocsList.Add(oTitle, obase64data)
				Next
				If oDocsList.Count = 0 Then
					log.Info("Can't de-serialize image ")
				End If
			End If
			poBogusLoanApp.DocBase64Dic = oDocsList
		End If

		Dim oUploadDocumentInfoList As New List(Of uploadDocumentInfo)
		If Not String.IsNullOrEmpty(Request.Form("UploadDocInfor")) Then
			oUploadDocumentInfoList = oSerializer.Deserialize(Of List(Of uploadDocumentInfo))(Request.Form("UploadDocInfor"))
		End If
		poBogusLoanApp.UploadDocumentInfoList = oUploadDocumentInfoList

		Dim sFilteredAppType = "LOAN"
		If SafeString(sAppType).ToUpper.Contains("ACCOUNT") Then
			sFilteredAppType = "DEPOSIT"
		End If
		Dim submiData = Common.getSubmitPDFData(poBogusLoanApp, _CurrentWebsiteConfig, sloanNumber, sFilteredAppType)	 '"apptye=DEPOSIT" for XA

		If submiData IsNot Nothing Then
			Common.WebPost(submiData, uploadDocumentURL)
		End If
		sendMsg(sloanId, "uploaded image(s) or document(s)")
		Return "<span style='color: green'>Your document(s) has been submitted and lender has been notified. </span>"
	End Function

	Private Function parseSearchResponse(ByVal pXMLResponse As String) As Boolean
		Try
			Dim returnXmlDoc As New XmlDocument
			returnXmlDoc.LoadXml(pXMLResponse)

			Dim responseNode As XmlElement = returnXmlDoc.SelectSingleNode("RESPONSE/SEARCH_RESULTS")

			If responseNode IsNot Nothing Then
				Return True
			Else ' Error NODES
				Dim errorNode As XmlElement = returnXmlDoc.SelectSingleNode("RESPONSE/ERROR")
				If errorNode IsNot Nothing Then
					Select Case errorNode.GetAttribute("type")
						Case "VALIDATION_FAILED"
							loginfo("Search Validation Error: " & errorNode.InnerText & "   " & Now.ToString)

							Return False
						Case Else
							loginfo("Search Error: " & errorNode.InnerText & "   " & Now.ToString)

							Return False
					End Select
				End If
				loginfo("Search Error: UNexpect response format")
				Return False
			End If

		Catch ex As Exception
			logError("Parsing Error: " & ex.Message, ex)
			Return False
		End Try
	End Function

	'deprecated
	'temporary usage to detect app come from mobile, will deprecate when  new webMS api is fixed
	'parse internal comment for LPQMobile signature, can't use external_source because it can be change by user
	Private Function IsMobileWebsite(ByVal poXMLResponse As XmlDocument) As Boolean
		Try
			'check status
			Dim loanStatusNodes As XmlNodeList = poXMLResponse.GetElementsByTagName("LOAN_DATA")
			If loanStatusNodes Is Nothing Or loanStatusNodes.Count = 0 Then Return False
			Dim statusCode As String = DirectCast(loanStatusNodes(0), XmlElement).GetAttribute("status_code")
			If statusCode <> "OK" Then
				log.Warn("bad detail loan")
				Throw New Exception(DirectCast(loanStatusNodes(0), XmlElement).GetAttribute("error_message"))
			End If

			Dim loanDataCData As XmlCDataSection = poXMLResponse.SelectSingleNode("OUTPUT/RESPONSE/LOAN_DATA").ChildNodes(0)
			Dim loanData As New XmlDocument
			loanData.LoadXml(loanDataCData.Value)

			Dim commentNodes As XmlNodeList = loanData.GetElementsByTagName("INTERNAL_COMMENTS")
			If commentNodes Is Nothing Or commentNodes.Count = 0 Then
				log.Warn("Can't locate comment node")
				Return False
			End If

			Dim sComment As String = SafeString(DirectCast(commentNodes(0), XmlElement).InnerText).ToUpper
			If sComment.Contains("Originating IP Address".ToUpper) And sComment.Contains("USER AGENT".ToUpper) Then
				Return True
			End If

		Catch ex As Exception
			logError("can't parse system node: " & ex.Message, ex)
		End Try
		Return False
	End Function

	'temporary usage to exclude indirect app
	Private Function IsDirect(ByVal poXMLResponse As XmlDocument) As Boolean
		Try
			'check status
			Dim loanStatusNodes As XmlNodeList = poXMLResponse.GetElementsByTagName("LOAN_DATA")
			If loanStatusNodes Is Nothing Or loanStatusNodes.Count = 0 Then Return False
			Dim statusCode As String = DirectCast(loanStatusNodes(0), XmlElement).GetAttribute("status_code")
			If statusCode <> "OK" Then
				log.Warn("bad detail loan")
				Throw New Exception(DirectCast(loanStatusNodes(0), XmlElement).GetAttribute("error_message"))
			End If

			Dim loanDataCData As XmlCDataSection = poXMLResponse.SelectSingleNode("OUTPUT/RESPONSE/LOAN_DATA").ChildNodes(0)
			Dim loanData As New XmlDocument
			loanData.LoadXml(loanDataCData.Value)

			Dim loanInfoNodes As XmlNodeList = loanData.GetElementsByTagName("LOAN_INFO")
			If loanInfoNodes Is Nothing Or loanInfoNodes.Count = 0 Then
				log.Warn("Can't locate LOAN_INFO node")
				Return False
			End If

			Dim sIsInDirectLoan As String = SafeString(DirectCast(loanInfoNodes(0), XmlElement).GetAttribute("is_indirect_loan"))
			If sIsInDirectLoan <> "Y" Then
				Return True	' not from dealerapp, 
			End If

		Catch ex As Exception
			logError("can't parse system node: " & ex.Message, ex)
		End Try
		Return False
	End Function

	'only used for approved
	Private Function parseApprovedAmount(ByVal poXMLResponse As XmlDocument, ByVal psLoanTypeEnum As String) As String
		Try
			'check status
			Dim loanStatusNodes As XmlNodeList = poXMLResponse.GetElementsByTagName("LOAN_DATA")
			If loanStatusNodes Is Nothing Or loanStatusNodes.Count = 0 Then Return Nothing
			Dim statusCode As String = DirectCast(loanStatusNodes(0), XmlElement).GetAttribute("status_code")
			If statusCode <> "OK" Then
				Throw New Exception(DirectCast(loanStatusNodes(0), XmlElement).GetAttribute("error_message"))
			End If

			Dim loanDataCData As XmlCDataSection = poXMLResponse.SelectSingleNode("OUTPUT/RESPONSE/LOAN_DATA").ChildNodes(0)
			Dim loanData As New XmlDocument
			loanData.LoadXml(loanDataCData.Value)

			Dim LoanInfoNodes As XmlNodeList = loanData.GetElementsByTagName("LOAN_INFO")

			If LoanInfoNodes Is Nothing Or LoanInfoNodes.Count = 0 Then
				Return Nothing
			Else
				Select Case psLoanTypeEnum.ToUpper
					Case "CC"
						Return SafeString(DirectCast(LoanInfoNodes(0), XmlElement).GetAttribute("credit_limit"))
					Case "VL", "PL", "HE"
						Return SafeString(DirectCast(LoanInfoNodes(0), XmlElement).GetAttribute("amount_approved"))

				End Select	'nothing for XA
			End If

		Catch ex As Exception
			logError("parseApprovedAmount Error: " & ex.Message, ex)
			Return Nothing
		End Try
		Return Nothing
	End Function

	Private Function parseApprovedAccount(ByVal poXMLResponse As XmlDocument) As System.Collections.Generic.List(Of String)
        Dim oAccount As New System.Collections.Generic.List(Of String)
        Try
            'check status
            Dim loanStatusNodes As XmlNodeList = poXMLResponse.GetElementsByTagName("LOAN_DATA")
            If loanStatusNodes Is Nothing Or loanStatusNodes.Count = 0 Then Return Nothing
            Dim statusCode As String = DirectCast(loanStatusNodes(0), XmlElement).GetAttribute("status_code")
            If statusCode <> "OK" Then
                Throw New Exception(DirectCast(loanStatusNodes(0), XmlElement).GetAttribute("error_message"))
            End If
            Dim loanDataCData As XmlCDataSection = poXMLResponse.SelectSingleNode("OUTPUT/RESPONSE/LOAN_DATA").ChildNodes(0)
            Dim loanData As New XmlDocument
            loanData.LoadXml(loanDataCData.Value)
			''get all proved accounts in the APPROVED_ACCOUNT node
			Dim nsmgr = New XmlNamespaceManager(loanData.NameTable)
			nsmgr.AddNamespace("ns", "http://www.meridianlink.com/CLF")
			Dim oApprovedAccounts = loanData.SelectSingleNode("//ns:APPROVED_ACCOUNTS", nsmgr)
			If oApprovedAccounts IsNot Nothing Then
				Dim oApprovedAccountTypes As XmlNodeList = CType(oApprovedAccounts, XmlElement).GetElementsByTagName("ACCOUNT_TYPE")
				If oApprovedAccountTypes IsNot Nothing AndAlso oApprovedAccountTypes.Count > 0 Then
					For Each oItem As XmlElement In oApprovedAccountTypes
						Dim sAccountName As String = SafeString(oItem.GetAttribute("account_name"))
						If sAccountName <> "" AndAlso Not oAccount.Contains(sAccountName) Then ''make sure no duplicate account name in oAccount list                      
							oAccount.Add(sAccountName)
						End If
					Next
				End If
			End If
		Catch ex As Exception
            logError("parseApprovedAmount Error: " & ex.Message, ex)
		End Try
		Return oAccount
	End Function

	Private Function loadLoanData(xmlResponse As XmlDocument) As XmlDocument
		'check status
		Dim loanStatusNodes As XmlNodeList = xmlResponse.GetElementsByTagName("LOAN_DATA")
		If loanStatusNodes Is Nothing Or loanStatusNodes.Count = 0 Then Return Nothing
		Dim statusCode As String = DirectCast(loanStatusNodes(0), XmlElement).GetAttribute("status_code")
		If statusCode <> "OK" Then
			Return Nothing
		End If

		Dim loanDataCData As XmlCDataSection = CType(xmlResponse.SelectSingleNode("OUTPUT/RESPONSE/LOAN_DATA").ChildNodes(0), XmlCDataSection)
		Dim loanDataDoc As New XmlDocument
		loanDataDoc.Load(New XmlTextReader(New StringReader(loanDataCData.Data)) With {.Namespaces = False})
		Return loanDataDoc
	End Function


	Private Function parseMemberNumberList(ByVal poXMLResponse As XmlDocument) As System.Collections.Generic.List(Of String)
		Dim oMemberNumberList As New System.Collections.Generic.List(Of String)

		Try

			'check status
			Dim loanStatusNodes As XmlNodeList = poXMLResponse.GetElementsByTagName("LOAN_DATA")
			If loanStatusNodes Is Nothing Or loanStatusNodes.Count = 0 Then Return Nothing
			Dim statusCode As String = DirectCast(loanStatusNodes(0), XmlElement).GetAttribute("status_code")
			If statusCode <> "OK" Then
				Throw New Exception(DirectCast(loanStatusNodes(0), XmlElement).GetAttribute("error_message"))
			End If

			Dim loanDataCData As XmlCDataSection = poXMLResponse.SelectSingleNode("OUTPUT/RESPONSE/LOAN_DATA").ChildNodes(0)
			Dim loanData As New XmlDocument
			loanData.LoadXml(loanDataCData.Value)

			Dim applicantNodes As XmlNodeList = loanData.GetElementsByTagName("APPLICANT")

			If applicantNodes Is Nothing Or applicantNodes.Count = 0 Then
				Return oMemberNumberList
			End If

			For Each oItem As XmlElement In applicantNodes
				Dim sMemberNumber As String = SafeString(oItem.GetAttribute("member_number"))
				If sMemberNumber <> "" Then
					oMemberNumberList.Add(sMemberNumber)
				End If
			Next
		Catch ex As Exception
			logError("parseMemberNumberList Error: " & ex.Message, ex)
		End Try
		Return oMemberNumberList
	End Function

	'post xml file
	Private Function post(ByVal content As String, ByVal url As String) As String
		Dim oweb As WebRequest = WebRequest.Create(url)
		oweb.Timeout = 120000 '2min search loan may be long


		'***Proxy 
		Dim proxy As IWebProxy = oweb.Proxy
		If (proxy IsNot Nothing) Then
			'' Use the default credentials of the logged on user.
			proxy.Credentials = CredentialCache.DefaultCredentials
		End If

		oweb.Method = "POST"
		'oweb.ContentLength = content.Length
		oweb.ContentType = "text/xml"

		Using streamwriter As StreamWriter = New StreamWriter(oweb.GetRequestStream())
			streamwriter.Write(content)
		End Using

		Dim response As HttpWebResponse = CType(oweb.GetResponse(), HttpWebResponse)
		Dim responseFromServer As String
		Using streamReader As New StreamReader(response.GetResponseStream())
			responseFromServer = streamReader.ReadToEnd()
		End Using

		Return responseFromServer

	End Function


	Public Function RenderLoanStatus(Optional searchParameter As String = "") As String
		Dim returnStr As String = ""
		Dim loans As XmlNodeList = _xmlResponse.GetElementsByTagName("LOAN")
		Dim oCC As New List(Of CLoanStatus)
		Dim oVL As New List(Of CLoanStatus)
		Dim oPL As New List(Of CLoanStatus)
		Dim oHE As New List(Of CLoanStatus)
		Dim oXA As New List(Of CLoanStatus)
		Dim oLoans As New List(Of CLoanStatus)
		For Each xmlele As XmlElement In loans

			'This is to undo the wildcard search used by searchloan API 
			If SafeString(xmlele.GetAttribute("borrower_lname")).ToUpper <> LName.ToUpper Then Continue For

			Dim oLoanStatus As New CLoanStatus
			oLoanStatus.LoanNumber = SafeString(xmlele.GetAttribute("loan_num"))
			oLoanStatus.LoanTypeEnum = SafeString(xmlele.GetAttribute("loan_type"))
			oLoanStatus.LoanType = formatLoanType2String(SafeString(xmlele.GetAttribute("loan_type")))
			oLoanStatus.CreatedDate = Common.SafeDate(xmlele.GetAttribute("create_date"))
			oLoanStatus.ApprovedDate = Common.SafeDate(xmlele.GetAttribute("approval_date"))
			oLoanStatus.LoanStatus = formatLoanStatus(xmlele.GetAttribute("loan_status"))
			oLoanStatus.LoanID = SafeString(xmlele.GetAttribute("loan_id"))

			Select Case oLoanStatus.LoanTypeEnum.ToUpper
				Case "CC"
					oCC.Add(oLoanStatus)
				Case "VL"
					oVL.Add(oLoanStatus)
				Case "PL"
					oPL.Add(oLoanStatus)
				Case "HE"
					oHE.Add(oLoanStatus)
				Case "XA"
					oXA.Add(oLoanStatus)
				Case Else
					oLoans.Add(oLoanStatus)
			End Select
		Next

		oCC.Sort(Function(class1 As CLoanStatus, class2 As CLoanStatus) (Comparer(Of DateTime).Default.Compare(class1.CreatedDate, class2.CreatedDate)))
		oVL.Sort(Function(class1 As CLoanStatus, class2 As CLoanStatus) (Comparer(Of DateTime).Default.Compare(class1.CreatedDate, class2.CreatedDate)))
		oPL.Sort(Function(class1 As CLoanStatus, class2 As CLoanStatus) (Comparer(Of DateTime).Default.Compare(class1.CreatedDate, class2.CreatedDate)))
		oHE.Sort(Function(class1 As CLoanStatus, class2 As CLoanStatus) (Comparer(Of DateTime).Default.Compare(class1.CreatedDate, class2.CreatedDate)))
		oXA.Sort(Function(class1 As CLoanStatus, class2 As CLoanStatus) (Comparer(Of DateTime).Default.Compare(class1.CreatedDate, class2.CreatedDate)))

		oLoans.AddRange(oCC)
		oLoans.AddRange(oVL)
		oLoans.AddRange(oPL)
		oLoans.AddRange(oHE)
		oLoans.AddRange(oXA)
		If oLoans.Count = 0 Then
			returnStr += "<div><strong class='rename-able'>No application in the last 90 days!</strong></div><br/>"
			Return returnStr
		End If

		'header
		returnStr += HeaderUtils.RenderPageTitle(0, "Applications", True)
		returnStr += "<div class='sub-title'>Only applications created in the last 90 days are displayed</div>"
		'returnStr += "&nbsp;" + System.Environment.NewLine

		Dim port As String
		If (Request.Url.Port = 80) Then
			port = ""
		Else
			port = ":" + Request.Url.Port.ToString()
		End If

		Dim serverRoot As String = Request.Url.Scheme + "://" + Request.Url.Host + port

		'FOR TESTING ONLY
		'Dim idx As Integer = 0
		'END TEST
		Dim sb As New StringBuilder()

		For Each oItem As CLoanStatus In oLoans
			sb.AppendLine("<div class='document-item'>")
			'FOR TESTING ONLY
			'If idx = 10 Then
			'    Exit For
			'End If
			'idx = idx + 1
			'END TEST

			'returnStr += "&nbsp;" + System.Environment.NewLine

			Dim sLoanNumber = oItem.LoanNumber
			Dim sLoanID = oItem.LoanID
			Dim sLoanType = oItem.LoanType
			Dim sCreatedDate = oItem.CreatedDate.ToShortDateString()
			Dim sApprovedDate = oItem.ApprovedDate.ToShortDateString()
			''date format only display mm/dd/yyyy
			'        If Not String.IsNullOrEmpty(sCreatedDate) Then
			'sCreatedDate = sCreatedDate.Substring(0, 10)
			'        End If
			'        If Not String.IsNullOrEmpty(sApprovedDate) Then
			'            sApprovedDate = sApprovedDate.Substring(0, 10)
			'        End If
			Dim sLoanStatus = oItem.LoanStatus

			Dim bIsAllowWebMsg As Boolean = True
			Dim visibleXMLNode As XmlNode = _CurrentWebsiteConfig._webConfigXML.SelectSingleNode("VISIBLE")
			If visibleXMLNode IsNot Nothing AndAlso visibleXMLNode.Attributes("webmsg") IsNot Nothing Then
				bIsAllowWebMsg = If(visibleXMLNode.Attributes("webmsg").InnerXml.NullSafeToUpper_ = "N", False, True)
			End If

			Dim loanInforNodes As XmlNodeList = Nothing
			Dim bIsGetLoanSucess As Boolean = False
			Dim bIsNotLPQ As Boolean = _CurrentWebsiteConfig.SubmitLoanUrl_TPV <> ""
			If False = bIsNotLPQ Then	'skip getloan detail if LOS is not LPQ
				bIsGetLoanSucess = getLoan(oItem.LoanID)
				Dim loanDataCData As XmlCDataSection = _xmlLoanDetail.SelectSingleNode("OUTPUT/RESPONSE/LOAN_DATA").ChildNodes(0)
				Dim loanData As New XmlDocument
				If loanDataCData IsNot Nothing Then
					loanData.LoadXml(loanDataCData.Value)
					If oItem.LoanTypeEnum.ToUpper = "XA" Then
						loanInforNodes = loanData.GetElementsByTagName("LOAN_INFO")
					Else
						loanInforNodes = loanData.GetElementsByTagName("LOAN_STATUS")
					End If
				End If
			End If

			Dim translatedStatusName As String
			'don't show real status to consumer
			'If sLoanStatus.ToUpper = "FRAUD" Then
			'	translatedStatusName = "PENDING"
			'ElseIf sLoanStatus.ToUpper.Contains("DECLINED") Then
			'	translatedStatusName = "PENDING"
			'             If loanInforNodes.Count > 0 Then
			'                 Dim sLastModifiedDate As DateTime = Common.SafeDate(loanInforNodes(0).Attributes("last_modified").InnerXml)
			'                 Dim ts As TimeSpan = sLastModifiedDate.Subtract(oItem.CreatedDate)
			'                 ''comparison created date and last modified date if total hours >12 don't change status
			'                 If ts.TotalHours >= 12 Then
			'                     translatedStatusName = sLoanStatus.ToUpper
			'                 End If
			'             End If

			If sLoanStatus.ToUpper.Contains("DECLINED") Then
				''intial show true status
				translatedStatusName = sLoanStatus.ToUpper
				If translatedStatusName.Contains("DECLINED") Then
					translatedStatusName = "DECLINED" ''don't want to show INSTANT DECLINED(loan status could be DECLINED or INSTANT DECLINED)
				End If
				If loanInforNodes IsNot Nothing AndAlso loanInforNodes.Count > 0 Then
					Dim dCurrentTimeSpan = Now().Subtract(oItem.CreatedDate)
					Dim dLastModifiedDate As DateTime = Common.SafeDate(loanInforNodes(0).Attributes("last_modified").InnerXml)
					Dim dUpdatedTime = dLastModifiedDate.Subtract(oItem.CreatedDate)
					If dCurrentTimeSpan.TotalMinutes < 30 Then ''within 30 minutes show pending status
						translatedStatusName = "PENDING"
					Else
						If dCurrentTimeSpan.TotalHours < 12 Then
							''check the app has been updated by an officer( within 30 minutes the app has been updated by the system)
							If dUpdatedTime.TotalMinutes < 30 Then
								''show pending
								translatedStatusName = "PENDING"
							End If
						End If
					End If
				End If
			ElseIf sLoanStatus.ToUpper.Contains("FRAUD") Then ' fraud will always be resolved by officer to something else so just show PEDNING 
				translatedStatusName = "PENDING"
			ElseIf sLoanStatus.ToUpper.Contains("DUPLICATE") Then
				translatedStatusName = "PENDING"
			ElseIf sLoanStatus.ToUpper = "INSTANTAPPROVED" Then
				translatedStatusName = "INSTANT APPROVED"
			ElseIf sLoanStatus.ToUpper = "APPROVEDPENDING" Then
				translatedStatusName = "APPROVED PENDING"
			ElseIf sLoanStatus.ToUpper = "NEEDMOREINFO" Then
				translatedStatusName = "NEED MORE INFO"
			ElseIf sLoanStatus.ToUpper = "CLOSEDFORINCOMPLETENESS" Then
				translatedStatusName = "CLOSED FOR INCOMPLETENESS"
			ElseIf sLoanStatus.ToUpper = "MEMBERWAITING" Then
				translatedStatusName = "MEMBER WAITING"
			Else
				translatedStatusName = sLoanStatus
			End If

            '===Get number of unread messages
            Dim nUnReadMessageCount As Integer
            If bIsAllowWebMsg Then
                nUnReadMessageCount = GetUnreadMessageCount(sLoanType, sLoanNumber)
                log.Debug("Unread message: " & nUnReadMessageCount.ToString)
            End If
            'nUnReadMessageCount = 10
            '==TODO: incoprate nUnReadMessageCount to icon
            Dim sWebMsgIconCode As String = String.Format("<div class='pull-left function-btn' loan_id='{1}' loan_num='{0}' email='{2}' app_type='{3}' tabindex='0' onkeydown='onKeydownIcon(event)' onclick='openViewMessagePopup(this)'>{4}<div>Messages</div>" & If(nUnReadMessageCount <> 0, "<span class='badge'>{5}</span>", String.Empty) & "</div>", sLoanNumber, sLoanID, Email, sLoanType, HeaderUtils.MessageThumb, nUnReadMessageCount)
            sWebMsgIconCode = If(bIsAllowWebMsg, sWebMsgIconCode, "")

            Dim sUploadIconCode As String = String.Format("<div class='pull-left function-btn' loan_num='{0}' app_type='{1}' loan_id='{2}' tabindex='0' onkeydown='onKeydownIcon(event)' onclick='gotoDocumentUpload(this)'>{3}<div>Upload</div></div>", sLoanNumber, sLoanType, sLoanID, HeaderUtils.UploadThumb)

            Dim sDownloadDoc = String.Format("<div class='pull-left function-btn' loan_num='{0}' app_type='{1}' loan_id='{2}' tabindex='0' onkeydown='onKeydownIcon(event)' onclick='showDocuments(this)'>{3}<div>Documents</div></div>", sLoanNumber, sLoanType, sLoanID, HeaderUtils.DocumentThumb)

			Dim sVerifyDeposit As String = ""

            sb.AppendLine(String.Format("<div class='document-title rename-able'><strong>{0} #{1}</strong></div>", sLoanType, sLoanNumber))

			sb.AppendLine("<div class='status-panel'>")
            sb.AppendLine(String.Format("<div><span class='prop-name rename-able'>Status</span> <span>{0}</span></div>", CultureInfo.CurrentCulture.TextInfo.ToTitleCase(translatedStatusName.ToLower())))
            sb.AppendLine(String.Format("<div><span class='prop-name rename-able'>Created Date</span> <span>{0}</span></div>", sCreatedDate))



			If sApprovedDate = "" Then Continue For
			If sLoanStatus = "APPROVED" Or sLoanStatus = "INSTANTAPPROVED" Or sLoanStatus = "INSTANT APPROVED" Then
				sb.AppendLine(String.Format("<div><span class='prop-name rename-able'>Approved Date</span> <span>{0}</span></div>", sApprovedDate))
			End If

			If False = bIsGetLoanSucess Then
				sb.AppendLine("</div>")	'this is the last closing div
				Continue For '_xmlLoanDetail get result 
			End If

			''*** This block is for displaying Amount if Getloan work
			Select Case oItem.LoanTypeEnum.ToUpper
				Case "CC"
					Dim sApprovedAmount As String = parseApprovedAmount(_xmlLoanDetail, oItem.LoanTypeEnum)
					If sApprovedAmount = "" Then Continue For
					If sLoanStatus = "APPROVED" Or sLoanStatus = "INSTANTAPPROVED" Or sLoanStatus = "INSTANT APPROVED" Then
						sb.AppendLine(String.Format("<div><span class='prop-name rename-able'>Credit Limit</span> <span>{0}</span></div>", FormatCurrency(sApprovedAmount, 2)))
					End If
				Case "VL", "PL", "HE"

					Dim sApprovedAmount As String = parseApprovedAmount(_xmlLoanDetail, oItem.LoanTypeEnum)
					If sApprovedAmount = "" Then Continue For
					If sLoanStatus = "APPROVED" Or sLoanStatus = "INSTANTAPPROVED" Or sLoanStatus = "INSTANT APPROVED" Then
						sb.AppendLine(String.Format("<div><span class='prop-name rename-able'>Approved Amount</span> <span>{0}</span></div>", FormatCurrency(sApprovedAmount, 2)))
					End If
				Case "XA"
					Dim oAppovedAccounts As Generic.List(Of String) = parseApprovedAccount(_xmlLoanDetail)
					If (sLoanStatus = "APPROVED" Or sLoanStatus = "INSTANTAPPROVED" Or sLoanStatus = "INSTANT APPROVED") And oAppovedAccounts.Count > 0 Then
						sb.AppendLine(String.Format("<div><span class='prop-name rename-able'>Approved Account</span> <span>{0}</span></div>", String.Join(", ", oAppovedAccounts.ToArray())))
					End If
					Dim loanDoc As XmlDocument = loadLoanData(_xmlLoanDetail)
					If loanDoc IsNot Nothing Then
						Dim bankFundingNode As XmlNode = loanDoc.SelectSingleNode("XPRESS_LOAN/FUNDING_SOURCES/FUNDING_SOURCE[@funding_type='BANK']")
						If bankFundingNode IsNot Nothing Then
							Dim minDepositAmount1 As Double = 0D
							If bankFundingNode.Attributes("microdeposit_amount_1") IsNot Nothing Then
								minDepositAmount1 = Common.SafeDouble(bankFundingNode.Attributes("microdeposit_amount_1").Value)
							End If
							Dim minDepositAmount2 As Double = 0D
							If bankFundingNode.Attributes("microdeposit_amount_2") IsNot Nothing Then
								minDepositAmount2 = Common.SafeDouble(bankFundingNode.Attributes("microdeposit_amount_2").Value)
							End If
							Dim minDepositAmountStatus As String = ""
                            If bankFundingNode.Attributes("microdeposit_status") IsNot Nothing Then
                                minDepositAmountStatus = bankFundingNode.Attributes("microdeposit_status").Value
                            End If
                            If minDepositAmount1 > 0 AndAlso minDepositAmount2 > 0 AndAlso minDepositAmountStatus.ToUpper() <> "PASS" Then
                                Dim failureCount As Integer = Common.SafeInteger(bankFundingNode.Attributes("microdeposit_failure_count").Value)
                                If failureCount < 3 Then
                                    sVerifyDeposit = String.Format("<div class='pull-left function-btn js-verify' loan_id='{1}' loan_num='{0}' onclick='openVerifyDepositPopup(this)'>{2}<div>Verify Deposit</div></div>", sLoanNumber, sLoanID, HeaderUtils.VerifyThumb)
                                Else
                                    sVerifyDeposit = String.Format("<div class='pull-left function-btn js-verify' loan_id='{1}' loan_num='{0}' onclick='exceedFailRetriesPopup(this)'>{2}<div>Verify Deposit</div></div>", sLoanNumber, sLoanID, HeaderUtils.VerifyThumb)
								End If

							End If
						End If
					End If
					''hide member number in XA apps status
					'' Dim oMemberNumberList As Generic.List(Of String) = parseMemberNumberList(_xmlLoanDetail)
					'' If oMemberNumberList.Count > 0 Then
					''returnStr += String.Format("<div>&nbsp;&nbsp;Member Number(s): {0}</div>", String.Join(", ", oMemberNumberList.ToArray()))
					''End If
			End Select

			'End div.status-panel
			sb.AppendLine("</div>")


			'Temporary solution to hide webms icon for app from dealertrack/routeone based on  is_indirect_loan="Y"
			If oItem.LoanTypeEnum.ToUpper = "VL" Or oItem.LoanTypeEnum.ToUpper = "PL" Then
				sWebMsgIconCode = If(IsDirect(_xmlLoanDetail), sWebMsgIconCode, "")
			End If

			sb.AppendLine("<div class='header_theme2'>")

			''Create header with icons for webms and or upload
			sb.AppendLine(sWebMsgIconCode)
			Dim showUploadIcon = False
			If oItem.LoanTypeEnum.ToUpper = "XA" Then
				showUploadIcon = xaUpLoadDocumentEnabled AndAlso Not String.IsNullOrEmpty(xaUpLoadDocumentMessage)
			ElseIf oItem.LoanTypeEnum.ToUpper = "BL" Then
				showUploadIcon = blUpLoadDocumentEnabled AndAlso Not String.IsNullOrEmpty(blUpLoadDocumentMessage)
			Else 'loans
				showUploadIcon = upLoadDocumentEnabled AndAlso Not String.IsNullOrEmpty(upLoadDocumentMessage)
			End If
			If showUploadIcon Then
				sb.AppendLine(sUploadIconCode)
			End If
			
			sb.AppendLine(sVerifyDeposit)

			'This feature is only enabled with mfa for better security
			If (_CurrentWebsiteConfig.LenderRef.ToLower.StartsWith("rsfcu") OrElse _CurrentWebsiteConfig.LenderRef.ToLower.StartsWith("lpquniver") OrElse _CurrentWebsiteConfig.LenderRef.ToLower.StartsWith("11111153eed")) AndAlso searchParameter = "LNAME|EMAIL" Then
				sb.AppendLine(sDownloadDoc)	''display PDF document on status page only for redstone
			End If

			sb.AppendLine("<div class='clearfix'></div>")
			sb.AppendLine("</div>")
			sb.AppendLine("</div>")
		Next

		Return returnStr + sb.ToString() + "<br/><p class='rename-able'>" + getLoanStatusNote() + "</p>"
	End Function

	Private Function formatLoanStatus(ByVal pStatus As String) As String
		Select Case pStatus.ToUpper
			Case "PRE-APP"
				Return "PRE-APPROVED"
			Case "PEN"
				Return "PENDING"
			Case "APP"
				Return "APPROVED"
			Case "AA"
				Return "INSTANT APPROVED"
			Case "AP"
				Return "APPROVED PENDING"
			Case "OFF"
				Return "COUNTER-OFFER"
			Case "DEC"
				Return "DECLINED"
			Case "AD"
				Return "INSTANT DECLINED"
			Case "DUP"
				Return "DUPLICATE"
			Case "INC"
				Return "INCOMPLETE"
			Case "MEMWAIT"
				Return "MEMBER WAITING"
			Case "MEMDC"
				Return "MEMBER DECLINED CREDIT"
			Case "DEN"
				Return "MEMBERSHIP DENIED"
			Case "CAN"
				Return "CANCELED"
			Case "INQ"
				Return "INQUIRING"
			Case "NMI"
				Return "NEED MORE INFO"
			Case "CODEC"
				Return "CO-APP DECLINED"
			Case "AP2S"
				Return "APPROVED PENDING 2ND SIG"
			Case "REF"
				Return "REFERRED"
			Case "AMD"
				Return "APPROVED/MEMBER DECLINED"
			Case "WTHDRN", "WTHDRNO"
				Return "WITHDRAWN"
			Case "REVIEW"
				Return "REVIEW"
			Case "FRAUD"
				Return "FRAUD"
			Case "CFI"
				Return "CLOSED FOR INCOMPLETENESS"
			Case Else
				Return pStatus.ToUpper
		End Select

		Return "Status Not Known"
	End Function

	Private Function formatLoanType2String(ByVal pLoanType As String) As String
		Select Case pLoanType.ToUpper
			Case "CC"
				Return "Credit Card Application"
			Case "PL"
				Return "Personal Loan Application"
			Case "VL"
				Return "Vehicle Loan Application"
			Case "HE"
				Return "Home Equity Application"
			Case "XA"
				Return "Account Application"
			Case "BL"
				Return "Business Loan Application"
			Case Else
				Return pLoanType.ToUpper
		End Select
	End Function
	Private Function SafeString(ByVal obj As Object) As String
		If obj Is Nothing Then
			Return ""
		End If
		Return obj.ToString()
	End Function

	Public Function getLoanStatusNote() As String
		Dim sResult As String = "Note: Any approval displayed in this check status page may be a conditional approval and subject to final review."
		Dim oUpLoadNode As System.Xml.XmlNode = _CurrentWebsiteConfig._webConfigXML.SelectSingleNode("LOAN_STATUS_NOTE")
		If oUpLoadNode IsNot Nothing Then
			sResult = oUpLoadNode.InnerText
		End If

		Return sResult
	End Function

    Private Sub SendMsgHandler(Optional ByVal isOpenMsg As Boolean = False)
        Try
            Dim loanID As String = SafeString(Request.Form("LoanID"))
            Dim message As String = SafeString(Request.Form("message"))
            Dim LoanNumber As String = SafeString(Request.Form("LoanNumber"))
            Dim emailAddress As String = SafeString(Request.Form("EmailAddress"))
            Dim sURL = _CurrentWebsiteConfig.BaseSubmitLoanUrl + "/resources.ashx/webms/appnumber/" + LoanNumber
            ''TODO:Make the logic active when the new API service is available in the LPQ server,  lpq committed on jan 21, 2019
            ''Dim sAppType As String = SafeString(Request.Form("AppType"))
            ''If sAppType <> "" Then ''new api
            ''    sAppType = If(sAppType.ToUpper.Contains("ACCOUNT"), "XA", "LOAN")
            ''    sURL = _CurrentWebsiteConfig.BaseSubmitLoanUrl + "/resources.ashx/webms/" + sAppType + "/appnumber/" + LoanNumber
            ''End If
            If isOpenMsg Then
                Dim strXMLMessage As String = buildMessage("Message opened by consumer " & emailAddress, "", isOpenMsg)
                If LoanNumber <> "" Then
                    'read message and write message to update consumer read status
                    handledSystemMessage(strXMLMessage, sURL)   'using new REST API so it will NOT trigger blinking wms on backoffice side
                End If
            Else
                If loanID <> "" AndAlso message <> "" Then
                    sendMsg(loanID, message) 'using old API so it can trigger the blinking wms on backoffice side
                    ''TODO:Make the logic active when the new API service is available in the LPQ server
                    ''Dim strXMLMessage = buildMessage(message, emailAddress, isOpenMsg)
                    ''Common.SubmitRestApiPost(_CurrentWebsiteConfig, sURL, strXMLMessage)
                End If
            End If
        Catch ex As Exception
            Response.Clear()
            Response.Write("Problem with sending message, please try again another time.")
            log.Error("Error With Sending", ex)
            Response.End()
        End Try
    End Sub

    ''''''webmsm response
    '    <OUTPUT version = "1.0" <> Response <> MESSAGES >
    '  <MESSAGE text="Message viewed by officer Im_adminco_tonyn / Support, MeridianLink" name="Im_adminco_tonyn / Support, MeridianLink" type="system" time="2019-10-31T13:24:03.0000000-07:00"/>
    '  <Message text = "l3" name="Im_adminco_tonyn / Support, MeridianLink" type="lender" time="2019-10-31T13:24:20.0000000-07:00" />
    '  <Message text = "t4" name="MARISOL TESTCASE" type="consumer" time="2019-10-31T13:24:27.0000000-07:00" />
    '  <Message text = "l4" name="Im_adminco_tonyn / Support, MeridianLink" type="lender" time="2019-10-31T13:24:53.0000000-07:00" />
    '  <Message text = "Message opened by consumer tony@ml.com" name="System" type="system" time="2019-10-31T13:25:13.0000000-07:00" />
    '  <Message text = "l4" name="Im_adminco_tonyn / Support, MeridianLink" type="lender" time="2019-10-31T13:27:22.0000000-07:00" />
    '  <Message text = "23" name="MARISOL TESTCASE" type="consumer" time="2019-10-31T13:27:29.0000000-07:00" />
    '  <Message text = "erere" name="MARISOL TESTCASE" type="consumer" time="2019-10-31T13:29:14.0000000-07:00" />
    '  <Message text = "l6" name="Im_adminco_tonyn / Support, MeridianLink" type="lender" time="2019-10-31T13:29:22.0000000-07:00" />
    '  <Message text = "l7" name="Im_adminco_tonyn / Support, MeridianLink" type="lender" time="2019-10-31T13:29:30.0000000-07:00" />
    '  <Message text = "t1" name="MARISOL TESTCASE" type="consumer" time="2019-10-31T13:29:44.0000000-07:00" />
    '</MESSAGES></RESPONSE></OUTPUT>

    'sample with no consumer message
    '<MESSAGES>
    '<Message text = "DocuSign for MERGED DOCUMENT completed" name="SYSTEM" type="system" time="2019-11-01T00:23:17.0000000-07:00" />
    '<Message text = "Message viewed by officer Im_adminco_tonyn / Support, MeridianLink" name="Im_adminco_tonyn / Support, MeridianLink" type="system" time="2019-11-01T14:06:48.0000000-07:00" />
    '<Message text = "l1" name="Im_adminco_tonyn / Support, MeridianLink" type="lender" time="2019-11-01T14:06:51.0000000-07:00" /><MESSAGE text="l2" name="Im_adminco_tonyn / Support, MeridianLink" type="lender" time="2019-11-01T14:07:21.0000000-07:00" />
    '</MESSAGES>
    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="psAppType">ACCOUNT,etcc.. </param>
    ''' <param name="psLoanNumber"></param>
    ''' <returns></returns>
    Private Function GetUnreadMessageCount(ByVal psAppType As String, psLoanNumber As String) As Int16
        Dim nUnreadCount As Int16 = 0
        If String.IsNullOrEmpty(psAppType) OrElse String.IsNullOrEmpty(psLoanNumber) Then
            log.Error("Can't GetUnreadMessageCount due to missing apptype or loan number.")
            Return 0
        End If
        Dim sAppType = If(psAppType.ToUpper.Contains("ACCOUNT") OrElse psAppType.ToUpper.Contains("XA"), "XA", "LOAN")
        Dim sURL = _CurrentWebsiteConfig.BaseSubmitLoanUrl + "/resources.ashx/webms/" + sAppType + "/appnumber/" + psLoanNumber
        Dim sResponse = Common.SubmitRestApiGet(_CurrentWebsiteConfig, sURL)
        If String.IsNullOrEmpty(sResponse) Then Return 0
        Dim xmlDoc As New XmlDocument
        xmlDoc.LoadXml(sResponse)
        Dim nodesList As XmlNodeList = xmlDoc.GetElementsByTagName("MESSAGE")

        'last Message where type=consumer -> all messages have already been read, 0 New messaqe
        'last Message where type=system And text="Message opened by consumer" -> all messages have already been read, 0 New messaqe

        'last Message where type=lender, count number of messages until one of the below conditions.  This count  Is  the number of unread message.
        '-message where type=consumer Or
        '-message where type=system And text="Message opened by consumer"

        If nodesList IsNot Nothing AndAlso nodesList.Count <> 0 Then
            Dim oLastMessageNode As XmlNode = nodesList(nodesList.Count - 1)
            If oLastMessageNode.Attributes("type").InnerXml = "consumer" Then Return 0
            If oLastMessageNode.Attributes("type").InnerXml = "system" AndAlso oLastMessageNode.Attributes("text").InnerXml.Contains("Message opened by consumer") Then Return 0

            For nIndex As Integer = nodesList.Count - 1 To 0 Step -1
                Dim oMessageNode As XmlNode = nodesList(nIndex)
                If oMessageNode.Attributes("type").InnerXml = "lender" Then
                    nUnreadCount = nUnreadCount + 1
                    Continue For
                ElseIf oMessageNode.Attributes("type").InnerXml = "consumer" Then
                    Exit For
                ElseIf oMessageNode.Attributes("type").InnerXml = "system" AndAlso oMessageNode.Attributes("text").InnerXml.Contains("Message opened by consumer") Then
                    Exit For
                End If
            Next
        End If
        Return nUnreadCount
    End Function

    Private Sub DoDepositVerification()
		Dim result As New CJsonResponse()
		Try
			Dim loanID As String = Common.SafeString(Request.Form("LoanID"))
			Dim loanRef As String = Common.SafeString(Request.Form("LenderRef"))
			Dim loanNumber As String = Common.SafeString(Request.Form("LoanNumber"))
			Dim amount1 As Double = Common.SafeDouble(Request.Form("DepositAmount1"))
			Dim amount2 As Double = Common.SafeDouble(Request.Form("DepositAmount2"))
			'==========================================================================
			' Make changes to it
			' Submit the changes with the "UPDATE" command
			If loanID = "" Then Throw New BadRequestException("Invalid submitted data")
			Dim getLoansXmlRequestStr As String = LoanRetrieval.BuildGetLoansXML(loanID, _CurrentWebsiteConfig.APIUser, _CurrentWebsiteConfig.APIPassword)

			' Done building the XML, now ship it off
			Dim req As WebRequest = WebRequest.Create(_CurrentWebsiteConfig.BaseSubmitLoanUrl + constantGetLoanUrl)
			req.Method = "POST"
			req.ContentType = "text/xml"
			'log.Info("XA: Preparing to POST GetLoans Request: " & CSecureStringFormatter.MaskSensitiveXMLData(getLoansXmlRequestStr))
			Dim writer As New StreamWriter(req.GetRequestStream())
			writer.Write(getLoansXmlRequestStr)
			writer.Close()

			Dim res As WebResponse = req.GetResponse()

			Dim reader As New StreamReader(res.GetResponseStream())
			Dim sXml As String = reader.ReadToEnd()

			log.Info("XA: GetLoans Response: " + CSecureStringFormatter.MaskSensitiveXMLData(sXml))

			' Extract the inner xml
			Dim responseXmlDoc As XmlDocument = New XmlDocument()
			responseXmlDoc.LoadXml(sXml)
			Dim innerXmlStr As String = ""
			innerXmlStr = responseXmlDoc.InnerText
			Dim innerXmlDoc As XmlDocument = New XmlDocument()
			innerXmlDoc.LoadXml(innerXmlStr)

			' Grab all the interested accounts from the origianl request
			Dim origRequestXMLDoc As XmlDocument = New XmlDocument()
			origRequestXMLDoc.LoadXml(getLoansXmlRequestStr)
			'---Update status

			Dim bankFundingSourceNode As XmlElement = Nothing
            For Each childNode As XmlNode In innerXmlDoc.GetElementsByTagName("FUNDING_SOURCE")
                If CType(childNode, XmlElement).GetAttribute("funding_type") = "BANK" Then
                    bankFundingSourceNode = CType(childNode, XmlElement)
                End If
            Next
            ''in the CLF, control_title attribute does not support "AUTHORIZED_SIGNER". it will cause an error when update the BXA app that has control_title="AUTHORIZED_SIGNER"
            ''sample response errors: <OUTPUT version="1.0"><ERROR type="VALIDATION_FAILED">Loan Application failed schema validation: ERROR: (11, 5065)  - The 'control_title' attribute is invalid - The value 'AUTHORIZED_SIGNER' is invalid according to its datatype 'String' - The Enumeration constraint failed.</ERROR></OUTPUT>
            ''to avoid the schema validation error check the mapping control_title: if control title is AUTHORIZED_SIGNER then update it to DIRECTOR.
            Dim oLoanInfo As XmlElement = CType(innerXmlDoc.GetElementsByTagName("LOAN_INFO")(0), XmlElement)
            If oLoanInfo.Attributes("entity_type") IsNot Nothing Then
                Dim sEntityType = Common.SafeString(oLoanInfo.Attributes("entity_type").InnerXml)
                If sEntityType.ToUpper = "B" Then
                    Dim oBeneOwners = innerXmlDoc.GetElementsByTagName("BENEFICIAL_OWNER")
                    For Each oBeneOwner As XmlElement In oBeneOwners
                        Dim oControlTitle = oBeneOwner.Attributes("control_title")
                        If oControlTitle IsNot Nothing Then
                            Dim sControlTitle = Common.SafeString(oControlTitle.InnerXml)
                            If sControlTitle.ToUpper = "AUTHORIZED_SIGNER" Then
                                oBeneOwner.SetAttribute("control_title", "DIRECTOR")
                            End If
                        End If
                    Next
                End If
            End If

            If bankFundingSourceNode IsNot Nothing AndAlso bankFundingSourceNode.GetAttribute("microdeposit_status") <> "PASS" Then
				Dim minDepositAmount1 As Double = Common.SafeDouble(bankFundingSourceNode.GetAttribute("microdeposit_amount_1"))
				Dim minDepositAmount2 As Double = Common.SafeDouble(bankFundingSourceNode.GetAttribute("microdeposit_amount_2"))
				Dim minFailureCount As Integer = Common.SafeInteger(bankFundingSourceNode.GetAttribute("microdeposit_failure_count"))
				Dim internalCommentAppend As String = ""
				If minFailureCount >= 3 Then
					result = New CJsonResponse(False, "We're sorry, but we're unable to verify your funds. Please call our branch for assistance.", "EXCEED")
				ElseIf (minDepositAmount1 + minDepositAmount2).Equals(amount1 + amount2) AndAlso Math.Abs(minDepositAmount1 - minDepositAmount2).Equals(Math.Abs(amount1 - amount2)) Then
					internalCommentAppend = "MICRODEPOSIT VERIFICATION SUCCESS"
					bankFundingSourceNode.SetAttribute("microdeposit_status", "PASS")
					result = New CJsonResponse(True, "Thank You! Your account verification is successful.", "SUCCESS")
				Else
					Dim bankAccount = Common.SafeString(bankFundingSourceNode.GetAttribute("bank_account_number"))
					internalCommentAppend = "MICRODEPOSIT VERIFICATION FAILED"
					bankFundingSourceNode.SetAttribute("microdeposit_failure_count", (minFailureCount + 1).ToString())
					If minFailureCount = 1 Then	'1 + current = 2 so 3 -2 equla 1
						result = New CJsonResponse(False, String.Format("Verification Failed! You have {0} remaining attempt to verify your bank account (XXXXXX-{1})", 2 - minFailureCount, bankAccount.Substring(bankAccount.Length - 4, 4)), "FAILED")
					ElseIf minFailureCount = 0 Then
						result = New CJsonResponse(False, String.Format("Verification Failed! You have {0} remaining tries to verify your bank account (XXXXXX-{1})", 2 - minFailureCount, bankAccount.Substring(bankAccount.Length - 4, 4)), "FAILED")
					Else '2
						result = New CJsonResponse(False, "We're sorry, but we're unable to verify your funds. Please call our branch for assistance.", "EXCEED")
					End If
				End If
				Dim internCommentNodes As XmlNodeList = innerXmlDoc.GetElementsByTagName("INTERNAL_COMMENTS")
				Dim internCommentNode As XmlElement = CType(internCommentNodes(0), XmlElement)
				internalCommentAppend = "SYSTEM (" + DateTime.Now + "): " & internalCommentAppend & Environment.NewLine & Environment.NewLine
				internCommentNode.InnerText = internalCommentAppend & internCommentNode.InnerText
				Dim cdataSection As XmlCDataSection
				Dim loanRequestNode As XmlElement = CType(origRequestXMLDoc.GetElementsByTagName("REQUEST")(0), XmlElement)
				If loanRequestNode.SelectSingleNode("//LOAN") IsNot Nothing Then
					loanRequestNode.RemoveChild(origRequestXMLDoc.SelectSingleNode("//LOAN"))
				End If
				Dim loanDataNode As XmlElement = CType(origRequestXMLDoc.SelectSingleNode("//LOAN_DATA"), XmlElement)
				If loanDataNode Is Nothing Then
					loanDataNode = origRequestXMLDoc.CreateElement("LOAN_DATA")
					loanRequestNode.AppendChild(loanDataNode)
				Else
					loanDataNode.RemoveAll()
				End If
				cdataSection = origRequestXMLDoc.CreateCDataSection(innerXmlDoc.InnerXml)
				loanDataNode.AppendChild(cdataSection)

				loanRequestNode.SetAttribute("loan_id", loanID)
				loanRequestNode.SetAttribute("action_type", "UPDATE")

				loanDataNode.SetAttribute("loan_type", "XA")
				log.Info("XA: Preparing to Update loan data: " + CSecureStringFormatter.MaskSensitiveXMLData(origRequestXMLDoc.InnerXml))


				log.Info("XA: " + origRequestXMLDoc.InnerXml)
				req = WebRequest.Create(_CurrentWebsiteConfig.BaseSubmitLoanUrl + constLoanSubmitURL)
				req.Method = "POST"
				req.ContentType = "text/xml"
				writer = New StreamWriter(req.GetRequestStream())
				writer.Write(origRequestXMLDoc.InnerXml)
				writer.Close()
				res = req.GetResponse()
				reader = New StreamReader(res.GetResponseStream())
				sXml = reader.ReadToEnd()
				log.Debug("updateloan result: " & sXml)
			Else
				result = New CJsonResponse(True, "Thank You! Your account verification is successful.", "SUCCESS")
			End If
			'==========================================================================
		Catch ex As Exception
			result = New CJsonResponse(False, "Problem with doing deposit verification, please try again another time.", "FAILED")
			log.Error("Error With Deposit Verification", ex)
		End Try
		Response.Clear()
		Response.Write(JsonConvert.SerializeObject(result))
		Response.End()
	End Sub


	Private Sub DoSendingSecurityCode()
		Dim result As CJsonResponse
		Try
			Dim _lenderRef As String = Common.SafeString(Request.Form("LenderRef"))
			Dim _email As String = Common.SafeString(Request.Form("Email"))
			Dim _lastName As String = Common.SafeString(Request.Form("LName"))
			Dim oManager As New CLenderConfigManager()
			If String.IsNullOrEmpty(_lenderRef) OrElse String.IsNullOrEmpty(_email) OrElse String.IsNullOrEmpty(_lastName) Then
				Throw New Exception("Invalid postback data")
			End If
			Dim backOfficeLender = oManager.GetBackOfficeLenderByLenderRef(_lenderRef)
			If backOfficeLender IsNot Nothing Then
				'==========================================================================
				Dim securityCode As String = oManager.GenerateSecurityCode(_lenderRef, _email, _lastName)
				If String.IsNullOrEmpty(securityCode) Then
					Throw New Exception("Cannot generate security code")
				End If
				Dim sb As New StringBuilder()
				'sb.Append("Dear,</br></br>")
				sb.Append("<p>Please enter the following security code to check the status of your application(s).</p>")
				sb.Append("</br>")
				sb.AppendFormat("<strong>{0}</strong>", securityCode)
				sb.Append("</br>")
				sb.Append("<p>This security code can only be used once</p>")

				Dim subject As String = "New Security Code from {0}"
				If ConfigurationManager.AppSettings.AllKeys.Contains("SecurityCodeEmailSubject") Then
					subject = ConfigurationManager.AppSettings("SecurityCodeEmailSubject")
				End If

				Dim sendEmailResult As String = ""
				sendEmailResult = Common.SendEmail(_email, sb.ToString(), String.Format(subject, backOfficeLender.LenderName))
				If sendEmailResult = "OK" Then
					result = New CJsonResponse(True, "OK.", "SUCCESS")
				Else
					result = New CJsonResponse(False, "Problem with sending security code to your email. Please try again later", "FAILED")
				End If
				'==========================================================================
			Else
				Throw New Exception("Invalid lenderref")
			End If
		Catch ex As Exception
			result = New CJsonResponse(False, "Problem with sending security code to your email. Please try again later.", "FAILED")
			log.Error("Error With Sending Security Code", ex)
		End Try
		Response.Clear()
		Response.Write(JsonConvert.SerializeObject(result))
		Response.End()
	End Sub

	Private Sub handledSystemMessage(ByVal sXMLMessage As String, ByVal sUrl As String)
		Dim pResponse = Common.SubmitRestApiGet(_CurrentWebsiteConfig, sUrl)
		Dim xmlDoc As New XmlDocument
		xmlDoc.LoadXml(pResponse)
		Dim nodesList As XmlNodeList = xmlDoc.GetElementsByTagName("MESSAGE")

		Dim sIndex As Integer = 0
		Dim messageType As String = ""
		If nodesList IsNot Nothing Then
			For Each oMessageNode As XmlNode In nodesList
				'' get the last message node
				If sIndex = nodesList.Count - 1 Then
					messageType = SafeString(oMessageNode.Attributes("type").InnerXml)
					Exit For
				End If
				sIndex += 1
			Next

			If messageType = "lender" Then
				Common.SubmitRestApiPost(_CurrentWebsiteConfig, sUrl, sXMLMessage)
			End If
		Else
			Common.SubmitRestApiPost(_CurrentWebsiteConfig, sUrl, sXMLMessage)
		End If

	End Sub
	Private Function buildMessage(ByVal sMessage As String, ByVal sName As String, ByVal isOpenMsg As Boolean) As String
		Dim strData As String = ""

		Dim xmlRequestDoc As New XmlDocument
		Dim requestNode As XmlElement = xmlRequestDoc.CreateElement("REQUEST")
		xmlRequestDoc.AppendChild(requestNode)
		Dim messageNode As XmlElement = xmlRequestDoc.CreateElement("MESSAGE")
		requestNode.AppendChild(messageNode)
		messageNode.SetAttribute("text", sMessage)

		Dim impersonateNode As XmlElement = xmlRequestDoc.CreateElement("IMPERSONATE_USER")
		requestNode.AppendChild(impersonateNode)
		Dim exactUserNode As XmlElement = xmlRequestDoc.CreateElement("EXACT_USER")
		impersonateNode.AppendChild(exactUserNode)
		If isOpenMsg Then
			exactUserNode.SetAttribute("name", "System")
			exactUserNode.SetAttribute("type", "system")
		Else
			exactUserNode.SetAttribute("name", sName)
			exactUserNode.SetAttribute("type", "consumer")
		End If


		Return xmlRequestDoc.OuterXml
	End Function
	Private Sub sendMsg(ByVal psLoanID As String, ByVal message As String)
		If _CurrentWebsiteConfig Is Nothing Then
			Return
		End If
		Dim xmlRequestDoc As New XmlDocument

		Dim inputNode As XmlElement = xmlRequestDoc.CreateElement("INPUT")
		xmlRequestDoc.AppendChild(inputNode)

		Dim loginNode As XmlElement = xmlRequestDoc.CreateElement("LOGIN")
		inputNode.AppendChild(loginNode)
		loginNode.SetAttribute("api_user_id", _CurrentWebsiteConfig.APIUser)
		loginNode.SetAttribute("api_password", _CurrentWebsiteConfig.APIPassword)

		Dim requestNode As XmlElement = xmlRequestDoc.CreateElement("REQUEST")
		inputNode.AppendChild(requestNode)
		requestNode.SetAttribute("app_id", psLoanID)

		Dim messagesNode As XmlElement = xmlRequestDoc.CreateElement("MESSAGES")
		requestNode.AppendChild(messagesNode)

		Dim msgNode As XmlElement = xmlRequestDoc.CreateElement("MESSAGE")
		messagesNode.AppendChild(msgNode)
		msgNode.SetAttribute("text", message)

		'xmlRequestDoc.Save("c:\MySideProjects\DealerApp\loanDetailRequest.xml")

		Try
			'Dim port As String
			'If (Request.Url.Port = 80) Then
			'    port = ""
			'Else
			'    port = ":" + Request.Url.Port.ToString()
			'End If

			'Dim serverRoot As String = Request.Url.Scheme + "://" + Request.Url.Host + port

			Dim urlToUse As String = _CurrentWebsiteConfig.BaseSubmitLoanUrl + "/SSFCU/WebMessageSubmission.aspx"	'"Services/SSFCU/WebMessageRetrieval.aspx"

			Dim xmlStringResponse As String = postMessage(xmlRequestDoc.OuterXml, urlToUse)
			log.Info("Submit Response: " & xmlStringResponse)

			Dim xmlResponse As New XmlDocument
			xmlResponse.LoadXml(xmlStringResponse)
			Dim xmlErrorNode As XmlNodeList = xmlResponse.GetElementsByTagName("ERROR")
			If xmlErrorNode.Count > 0 Then
				Dim xmlEle As XmlElement = DirectCast(xmlErrorNode(0), XmlElement)
				Response.Clear()
				Response.Write("There was a problem with sending the message. Please try again another time.")
				log.Info("Error With Sending: " & xmlEle.GetAttribute("type") & " - " & xmlEle.InnerText)
				Response.End()
			End If
		Catch ex As Exception
			log.Error("Errors Sending WebMsg: " & ex.Message, ex)
		End Try
	End Sub

	Private Function ValidateServerCertificate(ByVal sender As Object, ByVal certificate As X509Certificate, ByVal chain As X509Chain, ByVal sslPolicyErrors As SslPolicyErrors) As Boolean
		Return True
	End Function

	'post xml file, returns string file
	Private Function postMessage(ByVal content As String, ByVal url As String) As String
		System.Net.ServicePointManager.ServerCertificateValidationCallback = New System.Net.Security.RemoteCertificateValidationCallback(AddressOf ValidateServerCertificate)

		Dim xmlTempdoc As New XmlDocument
		xmlTempdoc.LoadXml(content)
		Dim tempNode As XmlElement = xmlTempdoc.GetElementsByTagName("LOGIN")(0)
		log.Info("POSTING DATA: " & CSecureStringFormatter.MaskSensitiveXMLData(xmlTempdoc.OuterXml))
		log.Info("POSTING TO: " & url)

		Dim oweb As WebRequest = WebRequest.Create(url)
		oweb.Method = "POST"
		'oweb.ContentLength = content.Length
		oweb.ContentType = "text/xml"

		Dim responseFromServer As String = ""

		Using streamwriter As New StreamWriter(oweb.GetRequestStream())
			streamwriter.Write(content)
		End Using

		Dim res As WebResponse = oweb.GetResponse()
		Using reader As New StreamReader(res.GetResponseStream())
			responseFromServer = reader.ReadToEnd()
		End Using

		Return responseFromServer
	End Function

	Public Class selectDocuments
		Public title As String
		Public base64data As String
	End Class


End Class

