﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="ViewSubmittedLoans.aspx.vb" Inherits="cu_ViewSubmittedLoans" %>

<%@ Import Namespace="System.Web.Optimization" %>

<%@ Register Src="~/Inc/PageHeader.ascx" TagPrefix="uc" TagName="pageHeader" %>
<%@ Register TagPrefix="uc1" TagName="Piwik" Src="~/Inc/Piwik/PiwikTracking.ascx" %>
<%@ Register Src="~/Inc/NewDocCapture.ascx" TagPrefix="uc" TagName="DocUpload" %>
<%@ Register Src="~/Inc/DocCaptureSourceSelector.ascx" TagPrefix="uc" TagName="DocUploadSrcSelector" %>
<%@ Register TagPrefix="uc" TagName="pageFooter" Src="~/Inc/PageFooter.ascx" %>
<%@ Import Namespace="Newtonsoft.Json" %>
<%@ Import Namespace="LPQMobile.Utils" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en" xml:lang="en">

<head runat="server">
    <title>Loan Status</title>
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <%:Styles.Render("~/css/thirdparty/bootstrap") %>
    <uc:pageHeader ID="ucPageHeader" ScriptFolder="cu" runat="server" />
	<%:Styles.Render("~/css/thirdparty/custom") %>
    <style type="text/css">
        .sub-title {
            padding-bottom: 20px;
            color: #595959;
        }
        .add-file-icon {
            width: 24px;
            margin-left: 10px;
        }
        /*textarea field*/
        .div_textmessage {
            width: 99%;
            position: relative;
        }

        /*override height textarea*/
        .div_textmessage textarea.ui-input-text {
            height: 100px;
            width: 100%;
            max-height: 100px;
            position: relative;
        }
        .lpq_container .ui-page-active, .lpq_container .ui-page {
            height: auto!important;
        }
        .document-item{
            margin-bottom: 60px;
        }

		.document-item>div:not(.document-title) {
			margin: 4px 12px 4px 0;
		}
        .document-item div.status-panel {
            padding-bottom: 10px;
            padding-top: 5px;
        }
        .document-item div.status-panel > div {
            display: inline-block;
            padding-bottom: 15px;
        }
        .document-item div.status-panel > div > span.prop-name{
            display: table;
            padding: 10px 20px 10px 0;
            color: #595959;
            width: 160px;
        }
        .document-item div.status-panel > div > span:not(.prop-name) {
            font-weight: bold;
            padding: 10px 20px 10px 0;
            width: 160px;
        }
		.document-item .function-btn {
			text-align: center;
			margin: 0 70px 0 0;
			font-size: 0.9em;
			cursor: pointer;
            position: relative;
		}
        .document-item .function-btn .fa{
            font-size: 25px;
        }
        .document-item .function-btn .badge{
            position: absolute;
            top: 0;
            right: 0;
            background: #C00000;
        }
        .document-item svg{
            height:35px;
        }
        .document-title {
            height: 25px;
            line-height: 25px;
            padding-right: 5px;
            font-size:1.15em;
        }
        /*Fix Focus Highlight ADA issue*/
        input:focus, #viewSubmitPage:focus {
            -webkit-box-shadow: 0 0 0 50px #e7e7e7 inset!important;
        }
    </style>

	<script type="text/javascript">
		$(function () {
			if (isMobile.any() && !isMobile.Windows()) {//moz-text-security does not works on window devices
				$("#txtSSN1,#txtSSN2")
					.attr("type", "tel")
					.addClass("mask-password");
			} else {
				$("#txtSSN1,#txtSSN2")
					.attr("type", "password");
			}

            $("h1").removeAttr("role");

			$('.ui-input-ssn input').on('focusout', function () {
				var ssn = $("#txtSSN1").val() + '-' + $("#txtSSN2").val() + '-' + $("#txtSSN3").val();
				$("#txtSSN").val(ssn);
			});
		});
	</script>

</head>
<body class="lpq_container">
    <uc1:Piwik ID="ucPiwik" runat="server"></uc1:Piwik>
    <input type="hidden" id="hfLenderRef" value='<%=_CurrentLenderRef%>'/>
    <input type="hidden" runat="server" id="hdForeignAppType"/>
    <input type="hidden" id="hdMFAEmailOnly" value="<%=IIf(_MFAEmailOnly, "Y", "N") %>" />
    <%--<input type="hidden" id="hdFooterTheme" value="<%=_bgTheme %>" />
    <input type="hidden" id="hdBgTheme" value="<%=_BackgroundTheme%>" />--%>

    <div id="viewSubmitPage" data-role="page">
        <div data-role="header" class="sr-only" >
            <h1>Application Status</h1>
        </div>
        <%If _showGUI Then%>
        <div data-role="content" aria-label="Input data">
                <%=_Logo%>
                <div data-role="fieldcontain">
                    <label for="txtLName" class="RequiredIcon">Last Name</label>
                    <input type="text" id="txtLName" maxlength="50" tabindex="0" value="<%=LName %>" <%=LNameDisabledString%> />
                </div>
                <div data-role="fieldcontain">
                    <label for="txtEmail" class="RequiredIcon">Email</label>
                    <input type="email" id="txtEmail" maxlength="50" value="<%=Email %>" autocapitalize="none" />
                </div>
				<div data-role="fieldcontain" class="hidden">
                    <label for="txtSecurityCode" class="RequiredIcon">Security Code (Sent to Email)</label>
                    <input type="text" id="txtSecurityCode" maxlength="50" value="" />
                </div>
                <div data-role="fieldcontain">

	                <div class="row">
		                <div class="col-xs-6 no-padding">
			                <label for="txtSSN" id="lblSSN">SSN</label>
		                </div>
						<div class="col-xs-6 no-padding ui-label-ssn">
			                <span class="pull-right header_theme2 ssn-ico" style="padding-left: 4px; padding-right: 2px; display: inline;"><%=HeaderUtils.IconLock16%></span>
							<span onclick='openPopup("#popSSNSastisfy")' class="pull-right" data-rel='popup' data-position-to="window" data-transition='pop' style="display: inline;"><span class="pull-right ssn-ico header_theme2"><%=HeaderUtils.IconQuestion16%></span></span>
							<div style="display: inline;" class="hidden">
								<label is_show="1" class="pull-right header_theme2 rename-able shadow-btn" style="margin-right: 10px;" onclick="toggleSSNText(this);">Hide SSN</label>
							</div>
							<div style="display: inline;">
								<label is_show="0" class="pull-right header_theme2 rename-able shadow-btn" style="margin-right: 10px;" onclick="toggleSSNText(this);">Show SSN</label>
							</div>
		                </div>	                        
	                </div>
                    <div class="ui-input-ssn row" id="divSSN">
                        <div class="col-xs-4">
                            <input aria-labelledby="lblSSN" type="password" pattern="[0-9]*" placeholder="---" id="txtSSN1" autocomplete="new-password" maxlength='3' next-input="#txtSSN2" onkeydown="limitToNumeric(event);" onkeyup="onKeyUp(event,'#txtSSN1','#txtSSN2', '3');"  value="<%=SSN1%>" <%=SSNDisabledString%> />
                            <input type="hidden" id="hdSSN1" value="" />
                        </div>
                        <div class="col-xs-4">
                            <input aria-labelledby="lblSSN" type="password" pattern="[0-9]*" placeholder="--" id="txtSSN2" autocomplete="new-password" maxlength='2' next-input="#txtSSN3" onkeydown="limitToNumeric(event);" onkeyup="onKeyUp(event,'#txtSSN2','#txtSSN3', '2');"  value="<%=SSN2%>" <%=SSNDisabledString%>/>
                            <input type="hidden" id="hdSSN2" value="" />
                        </div>
                        <div class="col-xs-4" style="padding-right: 0px;">
                            <input aria-labelledby="lblSSN" type="<%=_textAndroidTel%>" pattern="[0-9]*" placeholder="----" id="txtSSN3" maxlength='4' onkeydown="limitToNumeric(event);" value="<%=SSN3%>" <%=SSNDisabledString%> />
                            <input type="hidden" id="txtSSN"  value="<%=SSN%>" <%=SSNDisabledString%>  />
                        </div>
                    </div>
                </div>
			<%If _MFAEmailOnly = False Then%>
				<div style="margin-top: 10px;">
					<a id="lnkMfaSwitch" href="#" data-mode="self-handle-event" data-value="email" class="header_theme2 shadow-btn chevron-circle-right-before" style="cursor: pointer; font-weight: bold;">Confirm your identity via email instead of SSN?</a>
				</div>
			<%End If%>
                <div id="popSSNSastisfy" data-role="popup" style="max-width: 400px;">
                    <div data-role="content" aria-label="SSN Sastisfy">
                        <div class="row">
                            <div class="col-xs-12 header_theme2">
                                <a data-rel="back" class="pull-right"><%=HeaderUtils.IconClose %><span style="display: none;">close</span></a>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12">
                                <div style="margin: 10px 0;">
                                    Your Social Security Number (SSN) is used for identification purposes and to determine your account opening eligibility. 
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
				<div id="popVerifyDeposit" data-role="popup" data-position-to="window">
					<div data-role="content" aria-label="Verify Deposit">
						<div class="row">
							<div class="col-xs-12 header_theme2">
								<div style="float: left;font-weight: bold; margin-right: 0px;line-height: 40px;">ACH Account Verification</div>
								<a href="#" data-rel="back" class="pull-right svg-btn"><%=HeaderUtils.IconClose %><span style="display: none;">close</span></a>
							</div>
						 </div>
						<div class="row">
							<div class="col-sm-12">
								<div style="margin: 40px 0px 10px 0px;">
									<div data-role="fieldcontain">
										<label for="txtDepositAmount1" class="RequiredIcon">Deposit Amount 1</label>
										<input type="<%=TextAndroidTel%>" id="txtDepositAmount1" class="apSmartMoney" maxlength ="10"/>
									</div>
									<div data-role="fieldcontain">
										<label for="txtDepositAmount2" class="RequiredIcon">Deposit Amount 2</label>
										<input type="<%=TextAndroidTel%>" id="txtDepositAmount2" class="apSmartMoney" maxlength ="10"/>
									</div>
									<div class="div-continue">
										<a href="#" data-role="button" id="btnDoVerification" type="button" onclick="doVerificationDeposit(this)" class="div-continue-button">Confirm</a>
									</div>
								</div>
							</div>    
						</div>
					</div>
				</div>
				<div id="popVerifyDepositResult" data-role="popup" data-position-to="window">
					<div data-role="content" aria-label="Verify Deposit Result">
						<div class="row">
							<div class="col-xs-12 header_theme2">
								<div style="float: left;font-size: 1.2em; font-weight: bold; margin-right: 20px;line-height: 40px;">ACH Account Verification</div>
								<a href="#" data-rel="back" class="pull-right svg-btn"><%=HeaderUtils.IconClose %><span style="display: none;">close</span></a>
							</div>
						</div>
						<div class="row">
							<div class="col-sm-12">
								<div id="divVerifyDepositResult" style="margin: 40px 0px 10px 0px;">
									
								</div>
							</div>    
						</div>
					</div>
				</div>
				<div id="popSecurityCodeEmailNotify" data-role="popup" style="max-width: 600px;">
                    <div data-role="content" aria-label="Security Code Email Notify">
                        <div class="row">
                            <div class="col-xs-12 header_theme2">
                                <a data-rel="back" class="pull-right"><%=HeaderUtils.IconClose %><span style="display: none;">close</span></a>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="content-panel" style="margin: 10px 0;"></div>
                            </div>
                        </div>
                    </div>	
                </div>
				<div id="popDocList" data-role="popup" style="max-width: 600px;" data-dismissible="false">
                    <div data-role="content" aria-label="DocList">
                        <div class="row">
                            <div class="col-xs-12 header_theme2">
	                            <%--<div style="float: left;font-size: 1.2em; font-weight: bold; margin-right: 20px;line-height: 40px;">Available Documents</div>--%>
                                <a href="#" data-rel="back" class="pull-right svg-btn"><%=HeaderUtils.IconClose %><span style="display: none;">close</span></a>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="content-panel" style="margin: 10px 0;">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
				<%-- rollback this code and use previous version because we need to show "Invalid Security Code" message when the security code is not matched--%>
				<%--<div id="popSecurityCodeEmailNotify" data-role="popup" style="max-width: 600px;">
                    <div data-role="content" aria-label="Security Code Email Notify">
                        <div class="row">
                            <div class="col-xs-12 header_theme2">
                                <a data-rel="back" class="pull-right"><%=HeaderUtils.IconClose %><span style="display: none;">close</span></a>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12">
                                <div style="margin: 10px 0;">
	                                <h2>Security Code Emailed</h2>
                                    <p>A Security Code has been sent to your email. Please check your email and enter the provided code.</p>
									<p>It may take a few minutes to arrive in your Inbox</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>--%>
                <%--<div class="Title">
					SSN<i></i><span id="spSSNStar" style="color:red">*</span>
					<span id="errorSSN" style="color: Red;"></span>
				</div> 
				<div>
					<input type="<%=_textAndroidTel%>" pattern="[0-9]*" id="txtSSN" class="inssn"  maxlength ='11' />
				</div>
				<br />--%>
        </div>
        <div class="div-continue" data-role="footer">
	        <a href="#" id="btnSendEmailAuthentication" onclick="return sendEmailAuthentication(this);" type="button" class="btnSubmit div-continue-button  hidden">Send Email Authentication</a>
            <a href="#" id="btnGet" onclick="return validateSubmitForm(this);" type="button" class="btnSubmit div-continue-button">Get Status</a>
            <a id="href_show_dialog_1" href="#divErrorDialog" data-rel="dialog" style="display: none;">no text</a>
        </div>
        <%End If %>
        <div data-role="content" aria-label="Status">
            <%--<%=_Logo%>--%>
            <%=_RenderedLoanStatus%>
			<div id="divSearchResults">
				
				<%--<div class='sub_section_header header_theme'>
					<div class='section-title rename-able'>Application(s) in the last 90 days</div>
				</div>
				<br />
				<div class='document-item'>
					<div class='document-title'><b>Personal Loan #</b>11396</div>
					<div><span class="prop-name">Created Date:</span> <span>12/22/2017</span></div>
					<div><span class="prop-name">Status:</span> <span>PENDING</span></div>
					<div class='header_theme2'>
						<div class="pull-left function-btn" loan_num='11396' app_type='Personal Loan' loan_id='870040c0ec144b2e84e433da19d2fe49' onclick='gotoDocumentUpload(this)'>
							<%=HeaderUtils.UploadThumb %>
							<div>Upload</div>
						</div>
						<div class="pull-left function-btn" loan_id='870040c0ec144b2e84e433da19d2fe49' loan_num='11396' email='marisol@test.com' onclick='openViewMessagePopup(this)'>
							<%=HeaderUtils.MessageThumb%>
							<div>Message</div>
						</div>
						<div class="clearfix"></div>
					</div>
				</div>
				<br />
				Note: Any approval displayed in this check status page may be a conditional approval and subject to final review."--%>


			</div>
        </div>
        <%--<div style="display: none" data-role="footer" data-theme="<%=_bgTheme%>" class="ui-bar-<%=_bgTheme%>"></div>--%>
    </div>
    <div id="divErrorDialog" data-role="dialog" style="min-height: 444px;" role="region" aria-label="Error Dialog">
        <div role="dialog">
            <div data-role="header" class="sr-only" >
					  <h1  >Alert Popup</h1>
			 </div>
             <div data-role="content" aria-label="Alert Popup">
                <div class="DialogMessageStyle">There was a problem with your request</div>
                <div style="margin: 5px 0 15px 0;">
                    <span id="txtErrorMessage"></span>
                </div>
                <a href="#" data-role="button" data-rel="back" type="button">Ok</a>
            </div>
        </div>
    </div>
	<div id="popupDocViewer" data-role="page" role="region" aria-label="Doc Viewer">
		<div data-role="header" class="sr-only" >
			<h1>Document Viewer</h1>
		</div>
		<div data-role="content" style="padding: 0 !important" aria-label="Document Viewer">
			<div class="row" style="margin:10px 5px;"><div class="col-xs-12 header_theme2" style="padding-right: 0"><a onclick="backToDocList()" href="#" class="pull-right svg-btn"><%=HeaderUtils.IconClose %><span style="display: none;">close</span></a></div></div>
			<div class="content-panel"></div>
		</div>
	</div>
    <div data-role="dialog" id="docScan" role="region" aria-label="Doc Scan">
        <div data-role="header" class="sr-only">
            <h1>Upload Documents</h1>
        </div>
        <div data-role="content" aria-label="Upload Documents">
            <div class="row">
                <div class="col-xs-12 header_theme2" style="padding-right: 0px;">
                    <a href="#" data-rel="back" class="pull-right"><%=HeaderUtils.IconClose %><span style="display: none;">close</span></a>
                </div>
            </div>
            <div class="divUploadDocsContent">
				<uc:DocUpload ID="ucDocUpload" OneColumnLayout="Y" IDPrefix="" runat="server" />
            </div>
            <div id="divResultMessage" style="margin: 5px 0 15px 0; text-align: center"></div>
            <div class ="div-continue" style="text-align: center;">
                <a href="#" onclick="submitDocument()" type="button" data-role="button" class="div-continue-button">Submit</a> 
                <a href="#divErrorDialog" style="display: none;">no text</a>
                <a href="#" style="display: none;" data-rel="back" class ="div-goback" data-corners="false" data-shadow="false" data-theme="reset"><span class="hover-goback"> Go Back</span></a>   
            </div>
        </div>
    </div>

	<uc:DocUploadSrcSelector ID="ucDocUploadSrcSelector" IDPrefix="" runat="server" />
    <div data-role="dialog" id="viewMessage" role="region" aria-label="View Message">
        <div data-role="header" class="sr-only">
            <h1>Web Message</h1>
        </div>
        <div data-role="content" aria-label="Web Message">
            <div class="row">
                <div class="col-xs-12 header_theme2" style="padding-right: 0px;">
                    <a href="#viewSubmitPage" class="pull-right"><%=HeaderUtils.IconClose %><span style="display: none;">a</span></a>
                </div>
            </div>
            <div id="divWebMessage" style="position: relative">
                <div style="text-align: center">
                    Conversation Log:
                </div>
                <iframe id="ContentFrame" width="99%" height="300px" style="border: 1px solid #cccccc; -ms-border-radius: 4px; border-radius: 4px;"></iframe>
                <div style="text-align: center; font-size: 0.95em">
                    <span style="color: Blue">Blue</span> Denotes Financial Institution's Message 
                    <br />
                    <span style="color: Green">Green</span> Denotes Consumer Message
                </div>
                <br />
                <div style="text-align: center" id="divTypeMessage">
                    Type your message here:
					<span onclick='openPopup("#popUpMsgNote")' data-rel="popup" data-position-to="window" data-transition='pop'><span class="glyphicon glyphicon-question-sign" style="padding-top: 2px;" aria-hidden="true"></span></span>
                </div>
                <div class="div_textmessage">
                    <textarea aria-label="divTypeMessage" id="tbMsg"></textarea>
                </div>
                <hr />
            </div>
			<div id="popUpMsgNote" data-role="popup" style="max-width: 400px;">
                <div data-role="content" aria-label="Message Note">
                    <div class="row">
                        <div class="col-xs-12 header_theme2">
                            <a href="#" data-rel="back" class="pull-right"><%=HeaderUtils.IconClose %><span style="display: none;">close</span></a>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <div style="margin: 10px 0; font-size: 0.95em;">
								After submitting your message, we will review your inquiry and respond to you.
							</div>       
                        </div>    
                    </div>
                </div>
            </div>
			<span id="spMessageSendEror" style="color:red"></span>
            <div class="div-continue">
                <a href="#" data-role="button" id="btnSendMessage" name="btnSendMessage" type="button" onclick="SendMessage();" class="btnSubmit div-continue-button">Send Message</a>
                <%--Or--%> <a href="#viewSubmitPage" style="display: none;" class="div-goback" data-corners="false" data-shadow="false"><span class="hover-goback">Go Back</span></a>
            </div>
        </div>
    </div>
     <div id='divSuccessDialog' data-role='dialog' data-position-to="window" role="region" aria-label="Success Dialog">
			<div role='dialog'>
				<div data-role='header' style="display:none">
					<h1 class='ui-title'>Submit Success Message</h1>
				</div>
				 <div data-role='content' role='main' aria-label="Success Content">
				   <div style='margin: 5px 0 15px 0;'><span style="margin: 5px 0 15px 0; text-align: center" id="txtSuccessMessage"></span></div>
					<p><a href='#viewSubmitPage' type="button" data-role='button'>OK</a></p>					
			      </div>       
			</div>
	   </div>  
    <!--footer logo -->
    <%-- 
   <div class = "divfooter  ui-bar-<%=_FooterDataTheme%>" role="contentinfo" aria-label="footer Content">
         <%=_strRenderFooterContact%>    
           <img  id="hdLogoImage" src="<%=_strLogoUrl%>" alt ="" style="display :none;"/>
            <div class="divfooterleft"><div class="divfooterleft-content"><%=_strFooterLeft  %></div></div>          
            <div class="divfooterright">
                <% If String.IsNullOrEmpty(_hasFooterRight) Then%>
                    <div class="divfooterright-content">
                        <div class="divfooterright-content-style"><a href="<%=_strNCUAUrl%>"  class="divfooterright-content-color ui-link" ><%=_strFooterRight%></a></div></div>
                    <div class="divfooterright-logo">
                         <div class="divfooterright-logo-style"><a href="<%=_strHUDUrl%>" class="ui-link"> <img src="<%=_strLogoUrl%>" alt ="" id="logoImage" class="footerimage-style" /></a></div></div> 
                    <div style ="clear:both"></div> 
                <%Else%>
                    <%=_strFooterRight%>
                    <div style ="clear:both"></div> 
                <%End If%>
            </div>
          <div style ="clear:both"></div>            
   </div>
    --%>
    <!--ui new footer content -->
    <div class="divfooter" role="contentinfo" aria-label="new footer Content">
        <%=_strRenderFooterContact%>
         <div style="text-align :center"> 
                <div class="divLenderName"><%=_strLenderName%></div> 
                <div class="divFooterLogo">          
                    <% If String.IsNullOrEmpty(_hasFooterRight) Then%>
                    <%--
                      <div><a href="<%=_strNCUAUrl%>"  class="ui-link" ><%=_strFooterRight%></a><a href="<%=_strHUDUrl%>" class="ui-link"> <img src="<%=_strLogoUrl%>" alt="logo" class="footerimage-style img-responsive" /></a></div>                 
                     --%>
                    <div class="divfooterlabel"><a href="<%=_strNCUAUrl%>"  class="ui-link" ><%=_strFooterRight%></a> <a href="<%=_strHUDUrl%>" class="ui-link"><%=_equalHousingText%></a></div>                                                  
                    <% Else%>
                        <%=_strFooterRight%>
                    <%End If%>
                </div>  
                <div><%=_strFooterLeft  %></div>   
           </div>
        <%-- 
        <div class="container">
            <div class="row divfooter_padding">
                <!-- left footer content -->
                <div class="col-sm-7 col-md-7 divfooterleft ">
                    <div class="footerTextAlign">
                        <%=_strFooterLeft  %>
                    </div>
                </div>
                <!--right footer content -->
                <div class="col-sm-5 col-md-5 divfooterright">
                    <div class="row">
                        <% If String.IsNullOrEmpty(_hasFooterRight) Then%>
                        <div class="col-sm-9 col-md-10 divfooterright-content footerTextAlign">
                            <a href="<%=_strNCUAUrl%>" class="ui-link"><%=_strFooterRight%></a>
                        </div>
                        <div class="col-sm-3 col-md-2 divfooterright-logo footerTextAlign">
                            <a href="<%=_strHUDUrl%>" class="ui-link">
                                <img src="<%=_strLogoUrl%>" alt="" class="footerimage-style img-responsive" /></a>
                        </div>
                        <% Else%>
                        <%=_strFooterRight%>
                        <%End If%>
                    </div>
                </div>
            </div>
        </div>
            --%>
    </div>
    <!--end new ui footer content -->
   
	<uc:pageFooter ID="ucPageFooter" runat="server" />
	<script type="text/javascript" src="/PdfViewer/build/pdf.js"></script>
	
	<script type="text/javascript">
		var g_vsl_cc_doc_upload_settings = {
			requireDocTitle: <%=IIf(Not requiredDocTitle.Equals("N"), "true", "false")%>,
			docTitleOptions: <%=JsonConvert.SerializeObject(docTitleOptionDic("CC"))%>,
			requireDocUpload: true,
			title: "<%=upLoadDocumentMessage%>"
		};
		var g_vsl_he_doc_upload_settings = {
			requireDocTitle: <%=IIf(Not requiredDocTitle.Equals("N"), "true", "false")%>,
			docTitleOptions: <%=JsonConvert.SerializeObject(docTitleOptionDic("HE"))%>,
			requireDocUpload: true,
			title: "<%=upLoadDocumentMessage%>"
		};
		var g_vsl_pl_doc_upload_settings = {
			requireDocTitle: <%=IIf(Not requiredDocTitle.Equals("N"), "true", "false")%>,
			docTitleOptions: <%=JsonConvert.SerializeObject(docTitleOptionDic("PL"))%>,
			requireDocUpload: true,
			title: "<%=upLoadDocumentMessage%>"
		};
		var g_vsl_vl_doc_upload_settings = {
			requireDocTitle: <%=IIf(Not requiredDocTitle.Equals("N"), "true", "false")%>,
			docTitleOptions: <%=JsonConvert.SerializeObject(docTitleOptionDic("VL"))%>,
			requireDocUpload: true,
			title: "<%=upLoadDocumentMessage%>"
		};
		var g_vsl_xa_doc_upload_settings = {
			requireDocTitle: <%=IIf(Not xaRequiredDocTitle.Equals("N"), "true", "false")%>,
			docTitleOptions: <%=JsonConvert.SerializeObject(docTitleOptionDic("XA"))%>,
			requireDocUpload: true,
			title: "<%=xaUpLoadDocumentMessage%>"
		};
		var g_vsl_bl_doc_upload_settings = {
			requireDocTitle: <%=IIf(Not blRequiredDocTitle.Equals("N"), "true", "false")%>,
			docTitleOptions: <%=JsonConvert.SerializeObject(docTitleOptionDic("BL"))%>,
			requireDocUpload: true,
			title: "<%=blUpLoadDocumentMessage%>"
		};
		$(function() {
			$("#divSearchResults").on("contentChanged", function () {
				$(".rename-able", $(this)).each(function (idx, ele) {
					var dataId = getDataId();
					$(ele).attr("data-renameid", dataId);
					RENAME_REPOSITORY[dataId] = htmlEncode($(ele).html());
				});
				if (BUTTONLABELLIST != null) {
					performRenameForRenameableItems($(this));
				}
				<%If IsInMode("777") AndAlso IsInFeature("rename") Then%>
				attachEditButton($(this));
				<%End If%>
            });  
		});
	</script>
</body>
</html>
