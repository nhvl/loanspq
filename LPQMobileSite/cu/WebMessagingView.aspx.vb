﻿
Imports System.Xml
Imports System.Net
Imports System.IO
Imports System.Net.Security
Imports System.Security.Cryptography.X509Certificates
Imports LPQMobile.Utils

''' <summary>
''' This page is only use for getting & parsing messages then load them in the popup.
''' </summary>
''' <remarks></remarks>
Partial Class cu_WebMessagingView
    Inherits Page

    Private log As log4net.ILog = log4net.LogManager.GetLogger(Me.GetType)
    Private oConfig As CWebsiteConfig
    Private ReadOnly Property LenderRef() As String
        Get
            Return Common.SafeString(Request.Params("lender_ref"))
        End Get
    End Property

    Private ReadOnly Property LoanID() As String
        Get
            Return Common.SafeString(Request.Params("LoanID"))
        End Get
    End Property

    Private lenderName As String = ""

    Protected ServerRoot As String
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Dim port As String
            If (Request.Url.Port = 80) Then
                port = ""
            Else
                port = ":" + Request.Url.Port.ToString()
            End If

            ServerRoot = Request.Url.Scheme + "://" + Request.Url.Host + port

            Dim sHost As String = HttpContext.Current.Request.Url.Host
            oConfig = Common.GetCurrentWebsiteConfig(sHost, Nothing, LenderRef)
            
            If (oConfig Is Nothing) Then
                Throw New Exception("Website config not found")
            End If

			lenderName = Common.SafeString(CDownloadedSettings.DownloadLenderServiceConfigInfo(oConfig).LenderName)
			lenderName = lenderName.Replace("Federal Credit Union", "FCU").Replace("Credit Union", "CU")
            If lenderName.Length > 23 Then
                lenderName = lenderName.Substring(0, 20) & "..."
            End If

            ucPageHeader._currentConfig = oConfig

            Dim LoanNumber As String = Common.SafeString(Request.Params("loan_number"))
            'Response.Clear()
            If LoanID <> "" Then
                loadMsg(LoanID) ''old api
            End If
            ''TODO:Make the logic active when the new API service is available in the LPQ server
            ''Dim sAppType = Common.SafeString(Request.Params("AppType"))
            ''If sAppType <> "" Then
            ''    sAppType = IIf(sAppType.ToUpper.Contains("ACCOUNT"), "XA", "LOAN")
            ''    Dim sUrl As String = oConfig.BaseSubmitLoanUrl + "/resources.ashx/webms/" + sAppType + "/appnumber/" + LoanNumber
            ''    Dim sResponse = Common.SubmitRestApiGet(oConfig, sUrl)
            ''    parseWebMsg(sResponse)
            ''End If
        Catch ex As Exception
            Response.Write("Please try again another time.")
            log.Error("Error", ex)
        End Try
        Response.Write("")
        Response.End()
    End Sub

	'deprecate, but still need this for using with old api
    Private Sub loadMsg(ByVal psLoanID As String)
    
        Dim xmlRequestDoc As New XmlDocument

        Dim inputNode As XmlElement = xmlRequestDoc.CreateElement("INPUT")
        xmlRequestDoc.AppendChild(inputNode)

        Dim loginNode As XmlElement = xmlRequestDoc.CreateElement("LOGIN")
        inputNode.AppendChild(loginNode)
        loginNode.SetAttribute("api_user_id", oConfig.APIUser)
        loginNode.SetAttribute("api_password", oConfig.APIPassword)

        Dim requestNode As XmlElement = xmlRequestDoc.CreateElement("REQUEST")
        inputNode.AppendChild(requestNode)
        requestNode.SetAttribute("app_id", psLoanID)

        'xmlRequestDoc.Save("c:\MySideProjects\DealerApp\loanDetailRequest.xml")

        Try
            Dim urlToUse As String = oConfig.BaseSubmitLoanUrl + "/SSFCU/WebMessageRetrieval.aspx"  '"Services/SSFCU/WebMessageRetrieval.aspx"

            Dim xmlStringResponse As String = post(xmlRequestDoc.OuterXml, urlToUse)
            log.Info("Submit Response: " & xmlStringResponse)

            parseWebMsg(xmlStringResponse)

        Catch ex As Exception
            log.Error("Errors Getting Loan Details: " & ex.Message)
        End Try
    End Sub

    Private Sub parseWebMsg(ByVal pResponse As String)
        Dim msgArray As New Generic.List(Of String)

        Try

            ''&#xD;&#xA; = /r/n --> does not work in iframe need to convert to &lt;br/&gt;
            pResponse = pResponse.Replace("&#xD;&#xA;", "&lt;br/&gt;")
            Dim xmlDoc As New XmlDocument
            xmlDoc.LoadXml(pResponse)

            Dim nodesList As XmlNodeList = xmlDoc.GetElementsByTagName("MESSAGE")
            If nodesList.Count = 0 Then Return
            Dim lastMessage As XmlElement = Nothing
            Dim consumerName As String = ""

            For Each xmlEle As XmlElement In nodesList
                Dim txt As String = "<div "
                txt &= ">"
                Dim msg = xmlEle.GetAttribute("text").Trim()
                Dim dateNTime As Date = Date.Parse(xmlEle.GetAttribute("time").Trim())
                Dim name As String = xmlEle.GetAttribute("name").Trim()
                Dim type As String = xmlEle.GetAttribute("type").Trim
                Select Case type.ToLowerInvariant
                    Case "system"
                        'skip. 
                        'don't want to show system msg(officer has read msg) which may cause bad assumption on consumer side
                        Continue For
                        txt &= "<span style=""color:Gray"" >[" & dateNTime & "]" & lenderName & ": </span>"
                        If msg.IndexOf("Message viewed by officer") > -1 Then
                            msg = "Message viewed by officer"   'removed the officer name for security reason
                        End If
                    Case "lender"
                        txt &= "<span style=""color:Blue"" >[" & dateNTime & "]" & lenderName & ": </span>"
                    Case "consumer"
                        txt &= "<span style=""color:Green"" >[" & dateNTime & "]" & name & ": </span>"
                        consumerName = name
                    Case "clinic"
                        txt &= "<span style=""color:Green"" >[" & dateNTime & "]" & name & ": </span>"
                End Select

                txt &= msg
                txt &= "</div>"
                msgArray.Add(txt)

                lastMessage = xmlEle
            Next




            Dim messageFrom As String = ""
            If lastMessage IsNot Nothing Then
                messageFrom = lastMessage.GetAttribute("type").Trim()
            End If
            'If (consumerName <> "" And messageFrom = "lender") Then
            '    msgArray.Add("<span style=""color:Green"" >[" & DateTime.Now & "]" & consumerName & ": </span> Message viewed by " & consumerName)
            'End If

            For Each oStr In msgArray
                Response.Write(oStr)
            Next

            'do this only if the last message is send by consumer
            If msgArray.Count > 0 AndAlso msgArray(msgArray.Count - 1).IndexOf(lenderName) = -1 AndAlso Not LenderRef.ToUpper.StartsWith("TFCU") Then   'except for tower
                Dim sResponseTemplate As String = "<span style=""color:Gray"" >Thank you for submitting your message. We will review it and send you a response as soon as we can.</span>"
                Response.Write(sResponseTemplate)
            End If


            '' Scroll message iframe to bottom
            ''make sure msgArray is not empty
            If msgArray.Count > 0 Then

                '' send Message viewed by consumer_name
                'dont want this bc lender side will auto create another message when officer open webms
                'If (messageFrom = "lender") Then
                '	sendMsg(LoanID, "Message viewed by " & consumerName)
                'End If

                Response.Write("<script language='javascript'>parent.ScrollMessageToBottom();</script>")
            End If

        Catch ex As Exception
            Response.Write("Problem retreiving messages, please try again another time.")
            log.Error("Parse Response problem: " & ex.Message, ex)
        End Try
    End Sub

	
    Private Function ValidateServerCertificate(ByVal sender As Object, ByVal certificate As X509Certificate, ByVal chain As X509Chain, ByVal sslPolicyErrors As SslPolicyErrors) As Boolean
        Return True
    End Function

	'deprecate, not use in this file
    'post xml file, returns string file
    Private Function post(ByVal content As String, ByVal url As String) As String
        System.Net.ServicePointManager.ServerCertificateValidationCallback = New System.Net.Security.RemoteCertificateValidationCallback(AddressOf ValidateServerCertificate)
        Dim xmlTempdoc As New XmlDocument
        xmlTempdoc.LoadXml(content)
        Dim tempNode As XmlElement = xmlTempdoc.GetElementsByTagName("LOGIN")(0)
        tempNode.SetAttribute("password", "xxxxxx")
		log.Info("POSTING DATA: " & CSecureStringFormatter.MaskSensitiveXMLData(xmlTempdoc.OuterXml))
        log.Info("POSTING TO: " & url)

        Dim oweb As WebRequest = WebRequest.Create(url)
        oweb.Method = "POST"
        oweb.ContentLength = content.Length
        oweb.ContentType = "text/xml"

        Dim streamwriter As StreamWriter = New StreamWriter(oweb.GetRequestStream())
        streamwriter.Write(content)
        streamwriter.Close()

        Dim response As HttpWebResponse = CType(oweb.GetResponse(), HttpWebResponse)
        Dim datastream As Stream = response.GetResponseStream()
        Dim reader As New StreamReader(datastream)
        Dim responseFromServer As String = reader.ReadToEnd()
        reader.Close()
        datastream.Close()
        response.Close()
        Return responseFromServer
    End Function

End Class
