TempPdfFileCleaner is a service to delete all files or a set of files in a folder

Setup
1) Update FolderToDelete key in App.config(TempPdfFilesCleaner.exe.config) to be the folder where PDF files are stored
2) copy TempPdfFileCleaner.exe, log4net.dll, and app.config to destiantion folder - prefer inside CrossDomainServicesFolder
3) call TempPdfFileCleaner.exe via window scheduler
4) if deleting only a set of files, include the lenderref as the first argument

for Service
Call PDFFilesRefresh.aspx with lenderref param.  This service shoudl be in this same folder as TempPdfFileCleaner.exe


Result
log/service.txt



