﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TempPdfFilesCleaner
{
    class Program
    {
        static readonly log4net.ILog _logger = log4net.LogManager.GetLogger("TempPdfFilesCleaner");
        static void Main(string[] args)
        {
            try
            {
                _logger.Info("*** START Clean Temporary PDF files ***");

                var folderPath = ConfigurationManager.AppSettings["FolderToDelete"];

                if (Directory.Exists(folderPath))
                {
                    _logger.Info("Reading file in: " + folderPath);

                    var di = new DirectoryInfo(folderPath);
                    var files = di.GetFiles();

									
                    _logger.Info(files.Length + " file(s) found.");

                    if (files.Length > 0)
                    {
                        _logger.Info("Deleting ...");

                        foreach (FileInfo file in di.GetFiles())
                        {
							if (args != null && args.Length > 0){
								if (args[0].Length < 6 ){
									 _logger.Info("lenderref length is less 6 ...");
									 continue;
								}
								
								var FileNameFirstPart = args[0].Substring(0,6).ToLower();
								if ( file.Name.ToLower().Contains(FileNameFirstPart) == false) {									
									continue;
								}
							}

                            file.Delete();
                            _logger.Info("DELETED - " + file.FullName);
                        }
                    }
                }
                else
                {
                    _logger.Error("Folder path not found: " + folderPath);
                }

                _logger.Info("*** FINISH Clean Temporary PDF files ***");
            }
            catch (Exception ex)
            {
                _logger.Error("Error when delete file(s)", ex);
            }
        }
    }
}
