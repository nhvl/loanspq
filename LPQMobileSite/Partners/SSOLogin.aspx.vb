﻿Imports LPQMobile.Utils.Common
Imports LPQMobile.Utils
Imports DBUtils
Imports System.Data
Imports System.Xml
Imports Newtonsoft.Json
Imports System.Web.Script.Serialization
Imports System.Security.Cryptography
''' <summary>
''' extract params from db based on token and
''' redirect and post to LPQ, XA, viewSubmittedLoan
''' </summary>
''' <remarks></remarks>
Partial Class SSOLogin
    Inherits CBasePage

    Const SSOTokenExpireInterval As Integer = 300 '5min

    Private _log As log4net.ILog = log4net.LogManager.GetLogger(Me.GetType)

    Private _RenderedLoanStatus As String
    Private _sLenderRef As String

    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        Dim sToken = SafeString(Request("key"))
        _log.InfoFormat("Start SSOLogin with Token: {0} - Remote Host: {1}", sToken, _CurrentIP)

        If sToken = "" Then
            _RenderedLoanStatus = "<div><b>Missing/Empty URL Parameter: Key</b></div>"
            _log.Info("Token is not found or expired.")
            Return
        End If


        If False = HomeBankingSSOEnable Then
            _RenderedLoanStatus = "<div><b>Your Application Portal is not enable for redirect from home banking.</b></div>"
            Response.Write("<div><b>Your Application Portal is not enable for redirect from home banking.</b></div>")
            _log.Warn("Your Application Portal is not enable for redirect from home banking.")
            Return
        End If

        Dim sParams As String = RetrieveParamsBySSOToken(sToken)  'also set lenderRef
        If sParams = "" Then
            _RenderedLoanStatus = "<div><b>Token is not found or expired.  For security reasons, please start a new application.</b></div>"
            Response.Write("<div><b>Token is not found or expired.  For security reasons, please start a new application.</b></div>")
            _log.Info("Token is not found or expired.")
            Return
        End If

        'set platform_source value bassed on redirect from landing page or other homebanking page
        Dim referrer As Uri = Page.Request.UrlReferrer
        If Request("platform_source") <> "" Then 'pass from landing page
            sParams &= "&platform_source=" & Request("platform_source")
            log.Debug("reffer from landing page: " & Request("platform_source"))
        ElseIf referrer IsNot Nothing Then   'from referrer url when not using landing page
            'set platform_source value when url para
            log.Debug("reffer from home banking: " & referrer.ToString)
            If referrer.ToString.ToUpper.Contains("DISSO") Then
                sParams &= "&platform_source=DI"
            Else
                sParams &= "&platform_source=SSO_OTHER"
            End If
        Else
            'can't have direct access, this is use for SSO login only
        End If



        Dim data As New NameValueCollection()
        data = CURLBuilder.GetQueryVars(sParams)

        Dim Url As String = getUrl(data)
        If Url = "" Then
            _RenderedLoanStatus = "<div><b>Invalid Loan Type.</b></div>"
            _log.Info("Invalid loan type")
            Return
        End If
        ''debug in DI, redirect to any env without prefill, call from DI server so ip requesthost ip will alway be the same
        'If _CurrentIP.Contains("76.95.216.246") And Page.Request("lenderref").StartsWith("dfcu") And sParams.Contains("cc") Or
        '	_CurrentIP.Contains("75.5.191.67") And Page.Request("lenderref").StartsWith("abc_test") Then
        '	'huantedmansion 63.172.232.5, or 65.200.33.20  (172.58.20.202 tmobile, 76.95.216.246 home wifi)
        '	Url = String.Format("https://apptest.loanspq.com/cc/CreditCard.aspx?lenderref=abc_test&autofill=true&noencrypt=&fname=tony")	'DI blocked direct access to loan 
        '	'Url = String.Format("https://apptest.loanspq.com/cc/CreditCard.aspx?lenderref=dfcu_test&autofill=true&noencrypt=&fname=tony")
        '	'Url = String.Format("https://apptest.loanspq.com/cc/CreditCard.aspx?lenderref=ccfcu_test&autofill=true&noencrypt=&fname=tony")
        '	'Url = String.Format("https://app.loanspq.com/cc/CreditCard.aspx?lenderref=dfcu102915&autofill=true&noencrypt=") 'DI blocked direct access to loan 
        '	'Url = String.Format("https://apptest.loanspq.com/apply.aspx?lenderref=11111153eed453b987367e26bd37d43&autofill=true&noencrypt=")
        '	'Url = String.Format("https://apptest.loanspq.com/apply.aspx?lenderref=dfcu_test&autofill=true&noencrypt=&fname=tony")
        '	'Url = String.Format("https://app.loanspq.com/apply.aspx?lenderref=dfcu102915&autofill=true&noencrypt=") 'this reproduce the same issue
        '	'Url = String.Format("{0}/Partners/SSOLogin.aspx?Key={1}&lenderref={2}", sHost, sToken, _sLenderID)
        '	Logger.Info("Testmode for IP :" & _CurrentIP)
        '	Logger.Info("parameter :" & sParams)
        'End If

        'Logger.InfoFormat("Preparing for redirect and post with url/data count: {0} / {1}  ", Url, data.Keys.Count)  
        If data("SSONetTeller") = "Y" Then

            If Not IsValidConnectionString(data) Then
                Response.Write("Invalid request")
                Return
            End If

            If data("Integration") = "JXCHANGE" Then ''JXChangeCIS service
                data = getDataFromJXChange(data)
            ElseIf data("Integration") = "SYMITAR" Then
                data = GetDataFromSymitar(data)
            End If
        End If
        RedirectAndPOST(Me.Page, Url, data)
    End Sub

    Function RetrieveParamsBySSOToken(ByVal psSSOToken As String) As String
        Dim sParams As String = Nothing
        If String.IsNullOrEmpty(psSSOToken) Then
            Return False
        End If

        Dim oWhere As New CSQLWhereStringBuilder()
        oWhere.AppendAndCondition("SSOTokenID = {0}", New CSQLParamValue(Common.SafeString(psSSOToken)))
        Dim sqlstr As String = "SELECT GETDATE() As dbDate, SSOTokenCreateDate, isSSOTokenUsed, LenderRef, Params FROM SSOTokens" & oWhere.SQL

        Dim dt As DataTable
        Using db As New CSQLDBUtils(LPQMOBILE_CONNECTIONSTRING)
            dt = db.getDataTable(sqlstr)
            If dt.Rows.Count = 0 Then
                Return Nothing
            End If
        End Using

        Dim oRow As DataRow = dt.Rows(0)
        If IsDate(oRow("SSOTokenCreateDate")) Then
            Dim dateNow As DateTime = Common.SafeDate(oRow("dbDate"))
            Dim tokenDate As DateTime = Common.SafeDate(oRow("SSOTokenCreateDate"))

            'absolute expiration period
            If DateDiff(DateInterval.Second, tokenDate, dateNow) > SSOTokenExpireInterval Then
                Return Nothing
            End If
			Dim encrypter As New CRijndaelManaged()
            sParams = encrypter.DecryptUtf8(SafeString(oRow("Params")))
            _sLenderRef = SafeString(oRow("LenderRef"))

            're-submission of sso tokens 
            'If SafeString(oRow("isSSOTokenUsed")) = "Y" Then
            '	If DateDiff(DateInterval.Minute, sessionDate.Value.ToDateTime, dateNow.Value.ToDateTime) > Me.SSOTokenReuseThreshold Then
            '		log.InfoFormat("SSO session created :{0}. Token cannot be re-used after {1} minutes", sessionDate, SSOTokenReuseThreshold)
            '		Return SSOTokenStatuses.Expired
            '	End If
            'End If
        End If
        Return sParams
    End Function

    Private Function getUrl(ByRef poData As NameValueCollection) As String
        Dim url As String = HttpContext.Current.Request.Url.Host
        If url.Contains("app") Or url.Contains("mv1") Then
            url = "https://" & url
        Else
            url = "http://" & url
        End If
        Dim sLoanType = Request.QueryString("LoanType")
        If sLoanType <> "" Then
            sLoanType = Request.QueryString("LoanType").ToUpper
        Else
            sLoanType = SafeString(poData("LoanType")).ToUpper
        End If
        Select Case sLoanType
            Case "CC"
                url &= "/cc/CreditCard.aspx?lenderref={0}"
            Case "VL"
                url &= "/vl/VehicleLoan.aspx?lenderref={0}"
            Case "PL"
                url &= "/pl/PersonalLoan.aspx?lenderref={0}"
            Case "HE"
                url &= "/he/HomeEquityLoan.aspx?lenderref={0}"
            Case "XA" 'primary
                url &= "/xa/xpressApp.aspx?lenderref={0}"
            Case "ACCU" 'accu-medishare
                url &= "/accu/xpressApp.aspx?lenderref={0}"
            Case "SXA", "SA" 'secondary
                'If _CurrentLenderRef.ToUpper.StartsWith("SDFCU") Then
                '	url &= "/xa/xpressApp.aspx?lenderref={0}&type=2b"  'temporary fix to accomodate DI not able to do 2b
                'Else
                url &= "/xa/xpressApp.aspx?lenderref={0}&type=2"
                'End If
            Case "LS"
                url &= "/cu/ViewSubmittedLoans.aspx?lenderref={0}&Token=" & SafeString(Request("key"))
            Case "LP" 'landing page
                If SafeString(poData("list")) <> "" Then
                    url &= "/apply.aspx?lenderref={0}&list=" & SafeString(poData("list")) & "&Token=" & SafeString(Request("key"))
                Else
                    url &= "/apply.aspx?lenderref={0}&Token=" & SafeString(Request("key"))
                End If
                'End If
            Case "2B"
                url &= "/xa/xpressApp.aspx?lenderref={0}&type=2b"
            Case "2A"
                url &= "/xa/xpressApp.aspx?lenderref={0}&type=2a"
            Case Else
                url &= "/cc/CreditCard.aspx?lenderref={0}"
        End Select
        Return String.Format(url, _sLenderRef)
    End Function


#Region "http://www.codeproject.com/Articles/37539/Redirect-and-POST-in-ASP-NET"

    ''' <summary>
    ''' POST data and Redirect to the specified url using the specified page.
    ''' </summary>
    ''' <param name="page">The page which will be the referrer page.</param>
    ''' <param name="destinationUrl">The destination Url to which
    ''' the post and redirection is occuring.</param>
    ''' <param name="data">The data should be posted.</param>
    ''' <Author></Author>

    Private Sub RedirectAndPOST(page As Page, destinationUrl As String, data As NameValueCollection)
        'Prepare the Posting form
        Dim strForm As String = PreparePOSTForm(destinationUrl, data)
        'Add a literal control the specified page holding 
        'the Post Form, this is to submit the Posting form with the request.
        page.Controls.Add(New LiteralControl(strForm))
    End Sub

    ''' <summary>
    ''' This method prepares an Html form which holds all data
    ''' in hidden field in the addetion to form submitting script.
    ''' </summary>
    ''' <param name="url">The destination Url to which the post and redirection
    ''' will occur, the Url can be in the same App or ouside the App.</param>
    ''' <param name="data">A collection of data that
    ''' will be posted to the destination Url.</param>
    ''' <returns>Returns a string representation of the Posting form.</returns>
    ''' <Author></Author>
    Private Shared Function PreparePOSTForm(url As String, data As NameValueCollection) As [String]
        'Set a name for the form
        Dim formID As String = "PostForm"
        'Build the form using the specified data to be posted.
        Dim strForm As New StringBuilder()
        strForm.Append("<form id=""" & formID & """ name=""" & formID & """ action=""" & url & """ method=""POST"">")
        For Each key As String In data

            strForm.Append(("<input type=""hidden"" name=""" & key & """ value=""") + data(key) & """>")
        Next
        strForm.Append("</form>")
        'Build the JavaScript which will do the Posting operation.
        Dim strScript As New StringBuilder()
        strScript.Append("<script language='javascript'>")

        strScript.Append("var v" & formID & " = document." & formID & ";")
        strScript.Append("v" & formID & ".submit();")

        strScript.Append("</script>")
        'Return the form and the script concatenated.
        '(The order is important, Form then JavaScript)
        Return strForm.ToString() + strScript.ToString()
    End Function

#End Region
#Region "NetTeller"


    Private Function IsValidConnectionString(ByVal poData As NameValueCollection) As Boolean
        'return if any fail

        If String.IsNullOrEmpty(_CurrentWebsiteConfig.NetTellerSecretKey) Then
            log.Warn("net_teller_secret_key is not setup in XML config.")
            Return False
        End If

        Dim sUniqueID As String = SafeString(poData("UniqueID"))
        Dim sDateTimeValue As String = SafeString(poData("DateTimeValue"))  'yyMMddhhmmss
        Dim sAmPm As String = SafeString(poData("AmPm"))  'yyMMddhhmmss
        Dim sFINumber As String = SafeString(poData("FINumber"))
        Dim sConnectionString As String = SafeString(poData("ConnectionString"))

        If String.IsNullOrEmpty(sUniqueID) Then
            log.Warn("Unable to get UniqueID")
            Return False
        End If
        If String.IsNullOrEmpty(sDateTimeValue) Then
            log.Warn("Unable to get DateTimeValue")
            Return False
        End If
        If String.IsNullOrEmpty(sAmPm) Then
            log.Warn("Unable to get AmPm")
            Return False
        End If
        If String.IsNullOrEmpty(sFINumber) Then
            log.Warn("Unable to get FINumber")
            Return False
        End If
        If String.IsNullOrEmpty(sConnectionString) Then
            log.Warn("Unable to get ConnectionString")
            Return False
        End If

        log.Debug(sDateTimeValue & sAmPm)

        'Dim oDateTimevalueUtc As DateTime = DateTime.ParseExact("191016114912PM", “yyMMddhhmmsstt”, Nothing)
        Dim oDateTimevalueUtc As DateTime = DateTime.ParseExact(sDateTimeValue & sAmPm, “yyMMddhhmmsstt”, Nothing)
        Dim oLocaDateTimeUtc As DateTime = Now.ToUniversalTime

        Dim oDateDiff = Math.Abs(DateDiff(DateInterval.Minute, oDateTimevalueUtc, oLocaDateTimeUtc))
        If oDateDiff > 5 Then
            log.Warn("Expired timestamp")
            log.DebugFormat("Remote time {0}", oDateTimevalueUtc)
            log.DebugFormat("Local time {0}", oLocaDateTimeUtc)
            Return False
        End If


        Dim sConcatanatedInput As String = sUniqueID & sDateTimeValue & sFINumber & _CurrentWebsiteConfig.NetTellerSecretKey
        Dim sDerivedConnectionString As String = GetHash(sConcatanatedInput)
        If sDerivedConnectionString.ToUpper = sConnectionString.ToUpper Then Return True

        log.DebugFormat("Hashed connection string doesn't match. Local: {0},  Remote{1} ", sDerivedConnectionString, sConnectionString)
        Return False
    End Function


    Protected Function getDataFromJXChange(ByVal params As NameValueCollection) As NameValueCollection
        Dim oData As New NameValueCollection()
        Dim sUrl = _CurrentWebsiteConfig.BaseSubmitLoanUrl & "/Integration/API/CIS/CIS.aspx"

        Dim sNetTellerId = SafeString(params("NetTellerId"))
        If sNetTellerId = "" Then
            log.Warn("Unable to get NetTellerId")
            Return oData
        End If

        Dim oCJXChange As New CJXChange()
        Dim sJXChangeResponse = oCJXChange.getJXChangeResponse(_CurrentWebsiteConfig, sNetTellerId, sUrl)
        Return collectCISResponseData(New CCISResponse(sJXChangeResponse))
    End Function
    Protected Function GetDataFromSymitar(ByVal params As NameValueCollection) As NameValueCollection
        Dim oDatas As New NameValueCollection()
        Dim sUrl = _CurrentWebsiteConfig.BaseSubmitLoanUrl & "/Integration/API/CIS/CIS.aspx"

        Dim sMemberNumber = ""
        If SafeString(params("IsTest")) = "Y" Then
            sMemberNumber = SafeString(params("MemberNumber"))
            If sMemberNumber = "" AndAlso _sLenderRef.ToUpper = "NWFCU_TEST" Then
                sMemberNumber = "22265104" ''no member number from param        
            End If
        Else
            sMemberNumber = getMemberNumber(Common.SafeString(params("Nval")), Common.SafeString(params("Vval")), _CurrentWebsiteConfig.NetTellerSecretKey)
        End If
        If sMemberNumber = "" Then
            log.Warn("Unable to get Member Number")
            Return oDatas
        End If
        Dim sSymitarResponse = CSymitarLookup.getSymitarResponse(_CurrentWebsiteConfig, sMemberNumber, sUrl)
        Return collectCISResponseData(New CCISResponse(sSymitarResponse))
        ''comment out legacy code for simitar lookup
        ''Dim oSymitarAccounts As List(Of CSymitarAccount) = CSymitarLookup.GetSymitarAccount(_CurrentWebsiteConfig.APIUserLender, _CurrentWebsiteConfig.APIPasswordLender, sMemberNumber, sUrl)
        ''Dim isJoint As String = "N"
        ''If oSymitarAccounts.Count > 0 Then
        ''    For Each oAccount As CSymitarAccount In oSymitarAccounts
        ''        Dim coPrefix As String = ""
        ''        If oSymitarAccounts.IndexOf(oAccount) = 1 Then
        ''            coPrefix = "co"
        ''            isJoint = "Y"
        ''        ElseIf oSymitarAccounts.IndexOf(oAccount) > 1 Then
        ''            Exit For
        ''        End If
        ''        If SafeString(oAccount.FName) <> "" Then
        ''            oDatas.Add(coPrefix & "FName", Left(oAccount.FName, 50))
        ''        End If

        ''        If SafeString(oAccount.LName) <> "" Then
        ''            oDatas.Add(coPrefix & "LName", Left(oAccount.LName, 50))
        ''        End If
        ''        If SafeString(oAccount.MName) <> "" Then
        ''            oDatas.Add(coPrefix & "MName", Left(oAccount.MName, 50))
        ''        End If
        ''        If SafeString(oAccount.AccountNumber) <> "" Then
        ''            oDatas.Add(coPrefix & "MemberNumber", Left(oAccount.AccountNumber, 50))
        ''        End If
        ''        If SafeString(oAccount.Dob) <> "" Then
        ''            oDatas.Add(coPrefix & "DOB", oAccount.Dob)
        ''        End If
        ''        If SafeString(oAccount.Email) <> "" Then
        ''            oDatas.Add(coPrefix & "Email", oAccount.Email)
        ''        End If
        ''        If SafeString(oAccount.Ssn) <> "" Then
        ''            oDatas.Add(coPrefix & "SSN", oAccount.Ssn)
        ''        End If
        ''        If SafeString(oAccount.HomePhone) <> "" Then
        ''            oDatas.Add(coPrefix & "HomePhone", oAccount.HomePhone.Replace("-", ""))
        ''        End If
        ''        If SafeString(oAccount.WorkPhone) <> "" Then
        ''            oDatas.Add(coPrefix & "WorkPhone", oAccount.WorkPhone.Replace("-", ""))
        ''        End If
        ''        If SafeString(oAccount.MobilePhone) <> "" Then
        ''            oDatas.Add(coPrefix & "MobilePhone", oAccount.MobilePhone.Replace("-", ""))
        ''        End If

        ''        If SafeString(oAccount.Street) <> "" Then
        ''            oDatas.Add(coPrefix & "Address", oAccount.Street)
        ''        End If
        ''        If SafeString(oAccount.City) <> "" Then
        ''            oDatas.Add(coPrefix & "City", oAccount.City)
        ''        End If
        ''        If SafeString(oAccount.State) <> "" Then
        ''            oDatas.Add(coPrefix & "State", oAccount.State)
        ''        End If
        ''        If SafeString(oAccount.Zip) <> "" Then
        ''            oDatas.Add(coPrefix & "Zip", oAccount.Zip)
        ''        End If

        ''        Dim sIDCardType = getIDCardType(oAccount.IdType)
        ''        If sIDCardType <> "" Then
        ''            If sIDCardType = "DRIVERS_LICENSE" Then
        ''                ''get ID state from id Description
        ''                Dim sIDState = oAccount.IdDescription.Substring(0, 2) ''get first 2 digits
        ''                If sIDState <> "" Then
        ''                    oDatas.Add(coPrefix & "IDState", sIDState)
        ''                End If
        ''            ElseIf sIDCardType = "USPASSPORT" Then
        ''                sIDCardType = "PASSPORT"
        ''                oDatas.Add(coPrefix & "IDCountry", "USA")
        ''            ElseIf sIDCardType = "FOREIGNPASSPORT" Then
        ''                sIDCardType = "PASSPORT"
        ''                ''don't the format to get foreign country from description
        ''                oDatas.Add(coPrefix & "IDCountry", "")
        ''            End If
        ''            oDatas.Add(coPrefix & "IDCardType", sIDCardType)
        ''        End If
        ''        If SafeString(oAccount.IdNumber <> "") Then
        ''            oDatas.Add(coPrefix & "IDCardNumber", oAccount.IdNumber)
        ''        End If
        ''        If SafeString(oAccount.IdEpxirationDate) <> "" Then
        ''            oDatas.Add(coPrefix & "IDExpiredDate", oAccount.IdEpxirationDate)
        ''        End If
        ''        If SafeString(oAccount.IdIssuedDate <> "") Then
        ''            oDatas.Add(coPrefix & "IDIssuedDate", oAccount.IdIssuedDate)
        ''        End If
        ''    Next
        ''End If    
        ''Return oDatas
    End Function
    Protected Function collectCISResponseData(ByVal oApplicantInfo As CCISResponse) As NameValueCollection
        Dim oData As New NameValueCollection()
        If oApplicantInfo.CustomerInfoList.Count = 0 Then Return oData
        For Each oAccount As CCISCustomerInfo In oApplicantInfo.CustomerInfoList
            Dim coPrefix = ""
            If oApplicantInfo.CustomerInfoList.IndexOf(oAccount) = 1 Then
                coPrefix = "co"
            ElseIf oApplicantInfo.CustomerInfoList.IndexOf(oAccount) > 1 Then
                Exit For
            End If

            If SafeString(oAccount.FName) <> "" Then
                oData.Add(coPrefix & "FName", Left(oAccount.FName, 50))
            End If

            If SafeString(oAccount.LName) <> "" Then
                oData.Add(coPrefix & "LName", Left(oAccount.LName, 50))
            End If
            If SafeString(oAccount.MName) <> "" Then
                oData.Add(coPrefix & "MName", Left(oAccount.MName, 50))
            End If
            If SafeString(oAccount.MemberNumber) <> "" Then
                oData.Add(coPrefix & "MemberNumber", Left(oAccount.MemberNumber, 50))
            End If
            Try
                'conversion from 1/6/1959 to 01061959(mmddyyy)
                If SafeString(oAccount.DOB) <> "" Then
                    Dim dDOB As DateTime = DateTime.ParseExact(oAccount.DOB, "M/d/yyyy", System.Globalization.CultureInfo.InvariantCulture)
                    Dim reformattedDOB As String = dDOB.ToString("MMddyyyy", System.Globalization.CultureInfo.InvariantCulture)
                    oData.Add(coPrefix & "DOB", reformattedDOB)
                End If
            Catch ex As Exception
                _log.Error("Can't conver DOB from CIS", ex)
            End Try
            If SafeString(oAccount.Email) <> "" Then
                oData.Add(coPrefix & "Email", oAccount.Email)
            End If
            If SafeString(oAccount.SSN) <> "" Then
                oData.Add(coPrefix & "SSN", oAccount.SSN)
            End If
            If SafeString(oAccount.HomePhone) <> "" Then
                oData.Add(coPrefix & "HomePhone", oAccount.HomePhone.Replace("-", ""))
            End If
            If SafeString(oAccount.WorkPhone) <> "" Then
                oData.Add(coPrefix & "WorkPhone", oAccount.WorkPhone.Replace("-", ""))
                If oAccount.Extension <> "" AndAlso oAccount.Extension <> "0" Then
                    oData.Add(coPrefix & "Extension", oAccount.Extension)
                End If
            End If
            If SafeString(oAccount.MobilePhone) <> "" Then
                oData.Add(coPrefix & "MobilePhone", oAccount.MobilePhone.Replace("-", ""))
            End If
            If SafeString(oAccount.Street) <> "" Then
                oData.Add(coPrefix & "Address", oAccount.Street)
            End If
            If SafeString(oAccount.City) <> "" Then
                oData.Add(coPrefix & "City", oAccount.City)
            End If
            If SafeString(oAccount.State) <> "" Then
                oData.Add(coPrefix & "State", oAccount.State)
            End If
            If SafeString(oAccount.Zip) <> "" Then
                oData.Add(coPrefix & "Zip", oAccount.Zip)
            End If
            ''cannot get ID card because jXchange does not import id information
            If oAccount.IDCardList.Count > 0 Then
                ''only get primy id card
                Dim oIdCard = oAccount.IDCardList(0)
                If SafeString(oIdCard.IdType) <> "" Then
                    Dim sIdCardType = ""
                    Select Case oIdCard.IdType.ToUpper
                        Case "DRIVERS LICENSE"
                            sIdCardType = "DRIVERS_LICENSE"
                        Case "MATRICULA CONSULAR ID"
                            sIdCardType = "MATR_CONS_ID"
                        Case "MILITARY ID"
                            sIdCardType = "MILITARY_ID"
                        Case "PASSPORT"
                            sIdCardType = "PASSPORT"
                        Case "STATE ID"
                            sIdCardType = "ID_CARD"
                        Case "PERMANENT RESIDENT CARD"
                            sIdCardType = "ALIEN_REG_CARD"
                        Case "NON-RESIDENT VISA"
                            sIdCardType = "NON_RESIDENT_VISA"
                    End Select
                    If sIdCardType <> "" Then
                        oData.Add(coPrefix & "IDCardType", sIdCardType)
                    End If
                End If
                If SafeString(oIdCard.IdCountry) <> "" Then
                    oData.Add(coPrefix & "IDCountry", oIdCard.IdCountry)
                End If
                If SafeString(oIdCard.IdState) <> "" Then
                    oData.Add(coPrefix & "IDState", oIdCard.IdState)
                End If
                If SafeString(oIdCard.IdNumber) <> "" Then
                    oData.Add(coPrefix & "IDCardNumber", oIdCard.IdNumber)
                End If
                If SafeString(oIdCard.IdExpirationDate) <> "" Then
                    oData.Add(coPrefix & "IDExpiredDate", oIdCard.IdExpirationDate)
                End If
                If SafeString(oIdCard.IdIssuedDate) <> "" Then
                    oData.Add(coPrefix & "IDIssuedDate", oIdCard.IdIssuedDate)
                End If
            End If
        Next
        Return oData
    End Function

    Protected Function getIDCardType(ByVal sIDCardCode As String) As String
        ''        (0) Unknown 
        ''(1) Known Existing Member 
        ''(2) State Drivers License 
        ''(3) State ID Card 
        ''(4) US Passport 
        ''(5) Foreign Passport 
        ''(6) Military ID Card 
        ''(7) Foreign Government Issued ID 
        ''(8) Resident Alien Card 
        Dim sIDType As String = ""
        Select Case sIDCardCode
            Case "002"
                sIDType = "DRIVERS_LICENSE"
            Case "003"
                sIDType = "ID_CARD"
            Case "004"
                sIDType = "USPASSPORT"
            Case "005"
                sIDType = "FOREIGNPASSPORT"
            Case "006"
                sIDType = "MILITARY_ID"
            Case "008"
                sIDType = "ALIEN_REG_CARD"
        End Select

        Return sIDType
    End Function
    Protected Function getMemberNumber(ByVal nVal As String, ByVal vVal As String, ByVal sSharedSecret As String) As String
        Dim sMemberNumber As String = ""
        Dim IV As Byte() = Convert.FromBase64String(vVal)
        Dim Key As Byte() = Convert.FromBase64String(sSharedSecret)
        Dim textBytes As Byte() = Convert.FromBase64String(nVal)
        Dim decryptor As ICryptoTransform
        Dim rijndaelCipher As RijndaelManaged = New RijndaelManaged()
        rijndaelCipher.KeySize = 256
        rijndaelCipher.BlockSize = 128
        decryptor = rijndaelCipher.CreateDecryptor(Key, IV)
        Using memoryStream As System.IO.MemoryStream = New System.IO.MemoryStream(textBytes)
            Using oCryptoStream As CryptoStream = New CryptoStream(memoryStream, decryptor, CryptoStreamMode.Read)
                Dim plainText(textBytes.Length - 1) As Byte
                ' Dim plainText As Byte() = New Byte(textBytes.Length - 1) {}
                Dim decryptedCount As Integer = oCryptoStream.Read(plainText, 0, plainText.Length)
                sMemberNumber = Common.SafeString(Encoding.UTF8.GetString(plainText, 0, decryptedCount))
            End Using
        End Using
        'some member numbers have leading zeros, get rid of it
        If SafeInteger(sMemberNumber) <> 0 Then
            sMemberNumber = SafeString(SafeInteger(sMemberNumber))
        End If
        Return sMemberNumber
    End Function

    ''' <summary>
    ''' This hash is different from the other hash function is that it uses UTF8 input and returns hex output
    ''' </summary>
    ''' <param name="theInput"></param>
    ''' <returns></returns>
    Public Function GetHash(theInput As String) As String

        Using hasher As MD5 = MD5.Create()    ' create hash object

            ' Convert to byte array and get hash
            Dim dbytes As Byte() =
             hasher.ComputeHash(Encoding.UTF8.GetBytes(theInput))

            ' sb to create string from bytes
            Dim sBuilder As New StringBuilder()

            ' convert byte data to hex string
            For n As Integer = 0 To dbytes.Length - 1
                sBuilder.Append(dbytes(n).ToString("X2"))
            Next n

            Return sBuilder.ToString()
        End Using
    End Function


#End Region

End Class
