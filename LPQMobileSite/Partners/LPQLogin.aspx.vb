﻿Imports System.Net
Imports System.IO
Imports System.Net.Mail
Imports Microsoft.VisualBasic.Logging
Imports Newtonsoft.Json
Imports System.Web.Script.Serialization
Imports LPQMobile.Utils
Imports System.Security.Cryptography.X509Certificates
Imports System.Net.Security

''' <summary>
''' This page performs authentication process and redirect LPQ user to APM home page
''' </summary>
Partial Class Partners_LPQLogin
	Inherits System.Web.UI.Page

	Private _log As log4net.ILog = log4net.LogManager.GetLogger(Me.GetType())


    'override by keys in LPQSSOConfig.xml, this is provided by LPQ team per domain
    Protected _ClientID As String = "933f961c-dc2a-448f-b6be-514b4713faa3" 'beta
	Protected _ClientSecret As String = "Kg7Eu*Eu*RiJ573HutEQ#nB2cAZUQi" 'beta

    Protected _Code As String 'come from URL paramert in LPQ redirect

    'This is the redirect url post to LPQ
    Protected APM_URL As String = "{0}/Partners/LPQLogin.aspx"

    'this should be overrided by state url param
    Protected LPQ_WEBSITE As String = "https://beta.loanspq.com"
	Protected ReadOnly Property AccessTokenURL As String
		Get
			Return String.Format("{0}/services/resources.ashx/oauth/access_token",
			 LPQ_WEBSITE)
		End Get
	End Property

	Protected ReadOnly Property LoanOfficerInfoURL As String
		Get
			Return String.Format("{0}/services/resources.ashx/oauth/user/me",
			 LPQ_WEBSITE)
		End Get
	End Property

    Private Function ValidateServerCertificate(ByVal sender As Object, ByVal certificate As X509Certificate, ByVal chain As X509Chain, ByVal sslPolicyErrors As SslPolicyErrors) As Boolean
        Return True
    End Function

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Net.ServicePointManager.SecurityProtocol = Net.SecurityProtocolType.Tls Or Net.SecurityProtocolType.Tls11 Or Net.SecurityProtocolType.Tls12
        'ignore certificate so we don't have to deal with it when there is a certificate update on LPQ side
        System.Net.ServicePointManager.ServerCertificateValidationCallback = New System.Net.Security.RemoteCertificateValidationCallback(AddressOf ValidateServerCertificate)

        Dim oUserJson As String = ""
        Dim ipAddress As String = Request.ServerVariables("REMOTE_ADDR")

        'exception hanlding when user is already logged in
        If Request.IsAuthenticated AndAlso Not IsPostBack Then Return
        If Request.IsAuthenticated Then
            Session.Abandon()
            FormsAuthentication.SignOut()
        End If

        Dim IsTestMode As Boolean = Request.QueryString("Test123").NullSafeToUpper_ = "Y"
        If IsTestMode Then
            Dim fileReader = File.OpenText(Server.MapPath("~/Partners/json.json"))
            oUserJson = fileReader.ReadToEnd()
            fileReader.Dispose()
            fileReader.Close()
        End If

        ''User in LPQ is redirected to this page with url parameters
        Dim OAuthDomain As String = Common.SafeString(Request.QueryString("domain"))
        If OAuthDomain <> "" Then 'redirect from LPQ with Domain parameter, 'https://apptest.loanspq.com/Partners/LPQLogin.aspx?domain=https://beta.loanspq.com
            If Not IsWhitelistDomain(OAuthDomain) Then
                Response.Redirect("/NoAccess.aspx", True)
            Else 'get authorization code, LPQ validate clientID, user access previlege (from cookie)
                InitializeValues(OAuthDomain)
                ''Why replace(".loanspq.us", ".loanspq.com")
                ''Some FIs use ".us" for better internet routing.  For this case, OAuth requires all redirect that is done on the client side to use ".us".  But APM configuration and server side posting is still only allowed for ".com".  This is why we have to do some hacking.
                Dim state = New SmAuthBL().CreateSSOState(ipAddress, OAuthDomain.Replace(".loanspq.us", ".loanspq.com"))
                If state = "" Then Response.Redirect("/NoAccess.aspx", True)
                Dim sURL = OAuthDomain & "/login/OAuth/authorize.aspx?client_id=" & _ClientID & "&state=" & state
                _log.Debug("Response to LPQ with clientID  & state: " & sURL)
                Response.Redirect(sURL, True)
            End If
        End If

        _Code = Common.SafeString(Request.QueryString("code")) 'from lpq, expire in 1min
        Dim StateReturned = Common.SafeString(Request.QueryString("state"))  'should match APM token
        If Not IsTestMode AndAlso (_Code = "" Or StateReturned = "") Then Response.Redirect("/NoAccess.aspx", True)

        If _Code <> "" AndAlso StateReturned <> "" Then 'https://apptest.loanspq.com/Partners/LPQLogin.aspx?code=1234&state=61198EC2-C15E-43D9-8D06-B5B9695C6DDC
            _log.Debug("Return from LPQ with code & state: " & Request.Url.ToString)
            If Not New SmAuthBL().IsValidState(StateReturned, ipAddress, LPQ_WEBSITE) Then 'set LPQ_WEBSITE to OAuthDomain
                Response.Redirect("/NoAccess.aspx", True)
            End If
            InitializeValues(LPQ_WEBSITE)

            Dim sAccessToken As String = GetAccessToken() 'expire in 1 day
            If sAccessToken Is Nothing OrElse sAccessToken = "" Then
                Response.Redirect("/NoAccess.aspx", True)
            End If

            'opm#286121
            oUserJson = GetLoanOfficerInfo(sAccessToken)
            _log.Debug("GetLoanOfficerInfo response: " & oUserJson)
            If oUserJson Is Nothing OrElse oUserJson = "" Then
                Response.Redirect("/NoAccess.aspx", True)
            End If
        End If

        Dim userInfo = New JavaScriptSerializer().Deserialize(Of CSSOUser)(oUserJson)

        'TODO
        '#5 Log user into APM at the lender level for lender that has the same LenderCode and domain

        'Due to security, LPQ sends org_code, lender_code, user_code instead of orgID, LenderID, userID
        'however APM stores org_code, lender_code in xml node. This is expensive to do query.  Maybe we can cache
        'the table in memory.

        'If canManageUsers is true, allow SSO user to manage user for this lender.

        'For tracking SSO user, the system can use LoanOfficerCode which rarely change

        'If LenderCode is null, this is org level so allow access to all lenders with the same org_code
        Dim ssoSessionID As Guid = New SmAuthBL().ExecSSOLogin(HttpContext.Current, userInfo, LPQ_WEBSITE)

        If ssoSessionID = Guid.Empty Then
            FormsAuthentication.RedirectToLoginPage()
            'Response.Redirect("/Login.aspx", True)
            Return
        End If
        FormsAuthentication.SetAuthCookie(ssoSessionID.ToString(), True)
        Response.Redirect("/Sm/ManageLenders.aspx")
    End Sub

    Private Function IsWhitelistDomain(ByVal psOAuthDomain As String) As Boolean

        Dim oLPQSSOConfigs = Common.GetLPQSSOConfigByDomain(psOAuthDomain.Replace(".loanspq.us", ".loanspq.com")) ''Fi may uses ".us" instead of ".com" via Alkami private route
        'Log("oLPQSSOConfigs: & " & oLPQSSOConfigs.Domain)
        If oLPQSSOConfigs IsNot Nothing AndAlso oLPQSSOConfigs.Domain <> "" Then Return True

        If psOAuthDomain.NullSafeToLower_ = "https://beta.loanspq.com" Then Return True  'for dev without config file

        _log.Warn("Invalid OAuth Domain: " & psOAuthDomain)
        Return False
    End Function

    Private Sub InitializeValues(ByVal psDomain As String)
        'opm#289038
        _Code = Request.QueryString("code") 'code expires in 1 min
        Dim oLPQSSOConfig = Common.GetLPQSSOConfigByDomain(psDomain.Replace(".loanspq.us", ".loanspq.com")) ''Fi may uses ".us" instead of ".com" via Alkami private route
        If oLPQSSOConfig IsNot Nothing AndAlso oLPQSSOConfig.Domain <> "" Then
            _ClientID = oLPQSSOConfig.ClientID
            _ClientSecret = oLPQSSOConfig.ClientSecret
        Else
            'for dev without config, use default hardcode for beta
        End If

        APM_URL = String.Format(APM_URL, "https://" & Request.Url.Host)

    End Sub

    Private Function GetAccessToken() As String
		Try
			Dim sAuthCode As String = _Code
			_log.Debug(String.Format("GetAccessTokenURL: {0}  Code: {1}   ClientID: {2}  ClientSecret: {3}", AccessTokenURL, sAuthCode, _ClientID, _ClientSecret))
			Dim req As HttpWebRequest = getPostWebRequest(True, sAuthCode, APM_URL, "authorization_code")

			Dim resp = GetResponse(req)

			If resp.HasException Then
				_log.Warn("Should not have an exception.  " & resp.Response.StatusDescription)
				Return Nothing
			End If
			If resp.HasWebException Then
				_log.Warn("Should not have an web exception.  " & resp.Response.StatusDescription)
				Return Nothing
			End If

			Dim js As New System.Web.Script.Serialization.JavaScriptSerializer
			Dim responseJSON = ""
			Using streamReader As New StreamReader(resp.Response.GetResponseStream)
				responseJSON = streamReader.ReadToEnd()
			End Using
			Dim accessCodeProp = js.Deserialize(Of CAccessCodeProps)(responseJSON)

			If accessCodeProp.access_token = "" Then
				_log.Warn("Should have Access Token.")
				Return Nothing
			End If

			'If accessCodeProp.token_type = "bearer" Then
			'    _log.Warn("token_type is not correct")
			'    Return Nothing
			'End If

			Return accessCodeProp.access_token
		Catch ex As Exception
			_log.Warn(ex.Message, ex)
			Return Nothing
		End Try
	End Function

	Private Function GetLoanOfficerInfo(ByVal psAccessToken As String) As String
		Try
			Dim req As HttpWebRequest = getGetWebRequest(psAccessToken)
			Dim resp = GetResponse(req)
			_log.Debug("GetLoanOfficerInfo Response: " & resp.ToString)
			If resp.HasException Then
				_log.Warn("Should not have an exception.  " & resp.Response.StatusDescription)
				Return Nothing
			End If
			If resp.HasWebException Then
				_log.Warn("Should not have an web exception.  " & resp.Response.StatusDescription)
				Return Nothing
			End If

			Dim js As New System.Web.Script.Serialization.JavaScriptSerializer
			Dim responseJSON = ""
			Using streamReader As New StreamReader(resp.Response.GetResponseStream)
				responseJSON = streamReader.ReadToEnd()
			End Using

			Return responseJSON
		Catch ex As Exception
			_log.Warn(ex.Message, ex)
			Return Nothing
		End Try
	End Function


#Region "Helper"
	Public Function GetResponse(req As HttpWebRequest) As COAuthWebResponse
		Dim resp As HttpWebResponse = Nothing
		Try
			resp = DirectCast(req.GetResponse(), HttpWebResponse)
			Return New COAuthWebResponse With {.Response = resp}
		Catch ex As WebException
			Return New COAuthWebResponse With {.Response = DirectCast(ex.Response, HttpWebResponse), .HasWebException = True, .HasException = True, .Exception = ex}
		Catch ex As Exception
			Return New COAuthWebResponse With {.Response = resp, .HasWebException = False, .HasException = True, .Exception = ex}
		End Try
	End Function
	Public Class COAuthWebResponse
		Public Response As HttpWebResponse = Nothing
		Public HasException As Boolean = False
		Public HasWebException As Boolean = False
		Public Exception As Exception = Nothing
	End Class

	Protected Sub SetBasicAuthentication(ByVal pRequest As HttpWebRequest, ByVal pUsername As String, ByVal pPassword As String)
		pRequest.Headers.Remove(HttpRequestHeader.Authorization)
		Dim b() As Byte = System.Text.Encoding.ASCII.GetBytes(pUsername & ":" & pPassword)
		Dim authStr As String = System.Convert.ToBase64String(b)
		pRequest.Headers.Add("Authorization: Basic " & authStr)
	End Sub

	Protected Sub SetBeareAuthentication(ByVal pRequest As HttpWebRequest, ByVal pAccessToken As String)
		pRequest.Headers.Remove(HttpRequestHeader.Authorization)
		pRequest.Headers.Add("Authorization: Bearer " & pAccessToken)
	End Sub

	Protected Function getPostWebRequest(pSetClientSecret As Boolean) As HttpWebRequest
		Dim req As HttpWebRequest = DirectCast(HttpWebRequest.Create(AccessTokenURL), HttpWebRequest)

		req.Method = "POST"
		req.ContentLength = 0
		req.ContentType = "application/x-www-form-urlencoded"

		If pSetClientSecret Then
			SetBasicAuthentication(req, _ClientID, _ClientSecret)
		End If

		Return req
	End Function
	Protected Function getGetWebRequest(pAccessToken As String) As HttpWebRequest
		Dim req As HttpWebRequest = DirectCast(HttpWebRequest.Create(LoanOfficerInfoURL), HttpWebRequest)

		req.Method = "GET"
		req.ContentLength = 0
		req.ContentType = "application/x-www-form-urlencoded"

		SetBeareAuthentication(req, pAccessToken)

		Return req
	End Function
	Protected Function getPostWebRequest(pSetClientSecret As Boolean, code As String, redirectUri As String, grantType As String) As HttpWebRequest

		Dim req = getPostWebRequest(pSetClientSecret)
		writeBody(req, code, redirectUri, grantType)

		Return req
	End Function

	Protected Sub writeBody(req As HttpWebRequest, code As String, redirectUri As String, grantType As String)
		Dim queryString = System.Web.HttpUtility.ParseQueryString(String.Empty)
		If code <> "" Then queryString("code") = code
		If redirectUri <> "" Then queryString("redirect_uri") = redirectUri
		If grantType <> "" Then queryString("grant_type") = grantType

		Dim data = Encoding.ASCII.GetBytes(queryString.ToString)
		req.ContentLength = data.Length

		Using newStream = req.GetRequestStream
			newStream.Write(data, 0, data.Length)
		End Using
	End Sub

	Protected Class CAccessCodeProps
		Public access_token As String
		Public expires_in As Integer
		Public token_type As String
	End Class


	Public Class CJsonResponse
		Public access_token As String
		Public expires_in As Integer
		Public token_type As String
	End Class
#End Region


End Class
