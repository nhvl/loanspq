﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="LPQLogin.aspx.vb" Inherits="Partners_LPQLogin" %>
<%@ Import Namespace="LPQMobile.Utils" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
	    <%If Request.IsAuthenticated Then%>
		<p>You are currently logged into APM. Please choose one of the following options.</p>
		<button type="submit">Log out current session and log in with SSO</button>
		<p>or</p>
		<p><a href="/Sm/Default.aspx">Click here</a> to continue working on system with current session</p>
		<%Else%>
        <div>
            'DEBUG
            <%Response.Redirect("../NoAccess.aspx", True) %>
        </div>
		<%End If%>
    </form>
</body>
</html>
