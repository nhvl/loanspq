﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="migrateConfig.aspx.vb" Inherits="migrateConfig_migrateConfig"
    ValidateRequest="false" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>

    <script src="../js/jquery-1.12.4.min.js" type="text/javascript"></script>

    <style type="text/css">
        body
        {
            font-size: 12px;
            font-family: "verdana" ,tahoma,lucida grande,arial,sans-serif; ;}
        a
        {
            color: Blue;
            text-decoration: none;
            cursor: pointer;
        }
        h1
        {
            font-size: 16px;
            font-weight: bold;
        }
        .blue_table
        {
            margin-top: 30px;
            width: 60%;
            border: solid 1px #0088CC;
        }
        .blue_title
        {
            background-color: #0088CC;
            color: White;
            font-size: 14px;
            font-weight: bold;
            padding: 5px;
        }
        .blue_content
        {
            padding: 15px;
            background-color: #ffffff;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server" enctype="multipart/form-data">
    <div data-role="page" id="pl1">
        <div data-role="content" data-theme="b" style="height: 75px;">
            <div style="text-align: left" class="CompanyLogo">
                <img alt="" src="https://app.loanspq.com/logo/mltest.gif" /></div>
        </div>
        <div class="blue_table">
            <div runat="server" id="divInvalidUser" style="display: none">
                <h1>
                    Invalid username and password. Please try again.</h1>
            </div>
            <div runat="server" id="divUploadData">
                <div class="blue_title">
                    Select xml file to Upload</div>
                <div class="blue_content">
                    <asp:FileUpload ID="fuLenderConfigs" runat="server" />
                    <asp:Button runat="server" ID="btnSaveToData" Text="Migrate to DB" />
                    <asp:Label runat="server" ID="lblWarningMessage"></asp:Label>
                </div>
            </div>
        </div>
    </div>
    </form>
</body>
</html>

<script type="text/javascript">
    $(document).ready(function() {
        setTimeout("$('#lblWarningMessage').hide(1000);", 3000);
        setTimeout("$('#lblUpdateMessage').hide(1000);", 3000);
    });
</script>

