﻿Imports System.Xml
Imports System.IO
Imports System.Data
Imports LPQMobile.BO
Imports LPQMobile.DBManager
Imports LPQMobile.Utils
Imports System.Linq
Imports System.Configuration.ConfigurationManager

Partial Class migrateConfig_migrateConfig
	Inherits System.Web.UI.Page

#Region "Page Events"

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        CPBLogger.PBLogger.Component = Me.GetType.ToString
        CPBLogger.PBLogger.Context = Request.UserHostAddress
        CPBLogger.logInfo("migrateConfig: Page loading")
        lblWarningMessage.Text = ""        
		Return 'do not run this again
		If Not IsPostBack Then
			If Not Common.IsInternalIP(Request.UserHostAddress) Then
				Response.Write("This only works inside ML network")
				Return
			End If

            If Not Page.User.Identity.IsAuthenticated Then
                FormsAuthentication.RedirectToLoginPage()
            End If
        End If

    End Sub

#End Region

#Region "Controls Events"

	''OBSOLETED
	'Protected Sub btnSaveToData_Command(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.CommandEventArgs) Handles btnSaveToData.Command
	'    Dim sLenderRef As String = ""
	'    Dim sLenderID As String = ""
	'    Try
	'        If fuLenderConfigs.FileBytes.Length <= 0 Then
	'            lblWarningMessage.Style.Add("color", "red")
	'            lblWarningMessage.Text = "Please choose a xml file"
	'        Else
	'            Dim xmlDoc As XmlDocument = getXMLFromFile()
	'            Dim oNodeList As XmlNodeList
	'            oNodeList = xmlDoc.SelectNodes("LPQ_MOBILE_WEBSITES/WEBSITE")
	'            For Each oNode As XmlElement In oNodeList
	'                Dim config As New CLenderConfig(oNode)
	'                Dim oLender As New CLenderConfigManager()
	'                sLenderRef = config.LenderRef
	'                sLenderID = config.LenderId.ToString
	'                oLender.SaveLenderConfig(config)
	'	oLender.LogLenderConfig(config, Request.UserHostAddress, Common.SafeGUID(Request.Params("userId")))
	'            Next
	'            lblWarningMessage.Style.Add("color", "green")
	'            lblWarningMessage.Text = "Successfully added"
	'        End If
	'    Catch ex As Exception
	'        CPBLogger.logError("Error while save xml file to data. LenderRef/Lenderid: " & sLenderRef & "/" & sLenderID, ex)

	'        lblWarningMessage.Style.Add("color", "red")
	'        lblWarningMessage.Text = "Have some problem happens when migrate xml to DB. Please try again."
	'    End Try
	'End Sub

#End Region

#Region "Private Events"

    Private Sub onLogin()
        Dim userName As String = Common.SafeString(Request.Params("username"))
        Dim password As String = Common.SafeString(Request.Params("password"))
		Dim oUserArray() As String = AppSettings.Get("ADMIN_USER").Split(","c)
		Dim oPWArray() As String = AppSettings.Get("ADMIN_PW").Split(","c)


		If oUserArray.ToList().IndexOf(userName) > -1 And oPWArray.ToList().IndexOf(password) > -1 Then
			divInvalidUser.Visible = False
			divUploadData.Visible = True
		Else
			divInvalidUser.Visible = True
			divUploadData.Visible = False
		End If

    End Sub

    Private Function getXMLFromFile() As XmlDocument
        Dim memStream As New MemoryStream
        memStream.Write(fuLenderConfigs.FileBytes, 0, fuLenderConfigs.FileBytes.Length)
        memStream.Position = 0

        Dim xmlDoc As New XmlDocument
        xmlDoc.Load(memStream)
        memStream.Close()

        Return xmlDoc
    End Function

#End Region

End Class
