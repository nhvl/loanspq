﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="editConfig.aspx.vb" Inherits="migrateConfig_editConfig"
    ValidateRequest="false" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <script src="../js/jquery-1.12.4.min.js" type="text/javascript"></script>
	<link rel="stylesheet" href="/Admin/XmlEditor/css/themes/smoothness/jquery-ui.min.css" type="text/css" />
	<script src="/Admin/XmlEditor/lib/jquery.min.js"></script>
	<script src="/Admin/XmlEditor/lib/jquery-ui.min.js"></script>
	<script type="text/javascript">
		var editor_window = null;
		function viewSchema() {
			sURL = "Config.xsd";
			window.open(sURL, 'ViewCustomizationConfigKeys', "height= 900, width=800" );
		}

		function viewXMLTemplate() {
			sURL = "sample.xml";
			window.open(sURL, 'ViewXMLTEmplate', "height= 900, width=800");
			
        }

        
        function openUploadCSS() {
            sURL = "../css/themes/brian.aspx";
            window.open(sURL, 'UploadCSS', "height= 900, width=800");
        }

        function openUploadLogo() {
            sURL = "../logo/brian.aspx";
            window.open(sURL, 'UploadLogo', "height= 900, width=800");
        }

		function viewNoteBook() {
			sURL = "../logo/info.one";
			window.open(sURL, 'viewNoteBook', "height= 900");

		}

		function viewEditor() {
			var canSave = $("#btnAddUpdateConfigData").length == 1;
			sURL = "../admin/XmlEditor/editor.aspx?e=1&c=" + canSave;
			sessionStorage.setItem("configData", $("#txtConfigData").val());
			editor_window = window.open(sURL, 'viewEditor', 'height=900,width=1024,toolbar=no,directories=no,status=no,menubar=no,scrollbars=no,resizable=no ,modal=yes');
		}
		function closeEditorWindow() {
			if (editor_window !== null) {
				editor_window.close();
			}
		}

		function loadDetail(ele) {
			closeEditorWindow();
			var id = $(ele).data("id");
			$.post(window.location, { id: id, command: "checkOut" }, function (response) {
				if (response.result == false) {
					if (confirm(response.message) == true) {
						__doPostBack($(ele).attr("id").replace(/_/g, "$"), "");
					}
				} else {
					__doPostBack($(ele).attr("id").replace(/_/g, "$"), "");
				}
				return false;
			}, "json");
			return false;
		}

	</script>
    <style type="text/css">
        body
        {
            font-size: 12px;
            font-family: "verdana" ,tahoma,lucida grande,arial,sans-serif; ;}
        a
        {
            color: Blue;
            text-decoration: none;
            cursor: pointer;
        }
        h1
        {
            font-size: 16px;
            font-weight: bold;
        }
        .blue_table
        {
            margin-top: 30px;
            width: 100%;
            border: solid 1px #0088CC;
        }
        .blue_title
        {
            background-color: #0088CC;
            color: White;
            font-size: 14px;
            font-weight: bold;
            padding: 5px;
        }
        .blue_content
        {
            padding: 5px;
            background-color: #ffffff;
        }
        .blue_table th
        {
            background-color: #0088CC;
            color: White;
            padding: 5px;
        }
        .blue_table td
        {
            padding: 5px;
        }
        #tblAddNewData
        {
            width: 100%;
            line-height: 22px;
        }
        #tblAddNewData input[type="text"]
        {
            width: 300px;
        }
        #tblAddNewData th td
        {
            padding: 0px;
        }
        .alternativeRow {
            background-color: #e5f1fb;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server" enctype="multipart/form-data">
	<div data-role="page" id="pl1">
        <div data-role="content" data-theme="b" style="height: 75px;">
            <div data-role="content" data-theme="b" style="height: 75px;">
                <div style="text-align: left" class="CompanyLogo">
                    <img alt="" src="https://app.loanspq.com/logo/mltest.gif" /></div>
            </div>
        </div>
        <div runat="server" id="divInvalidUser" class="blue_table" style="display: none">
            <h1>
                Invalid username and password. Please try again.</h1>
        </div>
        <div runat="server" id="divUploadData">
            <div class="blue_title" style="margin-top: 10px; margin-bottom: 10px">
                LPQMobile Configuraion</div>
            <div>
                <table id="tblAddNewData">
                    <tr>
                        <td>
                        </td>
                        <td>
                        </td>
                        <td style="width: 60%">
                            Config Data
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 140px">
                            Organization ID <span style="color: Red">*</span>
                        </td>
                        <td style="width: 320px">
                            <input type="text" runat="server" id="txtOrganizationID" />
                        </td>
                        <td rowspan="7">
							<asp:HiddenField ID="hfLenderConfigID" runat="server" />
                            <textarea runat="server" id="txtConfigData" style="width: 95%; height: 300px;"></textarea>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Lender ID <span style="color: Red">*</span>
                        </td>
                        <td>
                            <input type="text" runat="server" id="txtLenderID" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Lender Ref
                        </td>
                        <td>
                            <input type="text" runat="server" id="txtLenderRef" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Return URL
                        </td>
                        <td>
                            <input type="text" runat="server" id="txtReturnURL" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            LPQ Login<span style="color: Red">*</span>
                        </td>
                        <td>
                            <input type="text" runat="server" id="txtLPQLogin" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            LPQ Password<span style="color: Red">*</span>
                        </td>
                        <td>
                            <input type="text" runat="server" id="txtLPQPassword" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            LPQ URL<span style="color: Red">*</span>
                        </td>
                        <td>
                            <input type="text" runat="server" id="txtLPQURL" />
                        </td>
                    </tr>
					
                    <tr>
                        <td>
                        </td>
                        <td colspan="1">
	                        <asp:Button runat="server" ID="btnAddUpdateConfigData" OnClientClick="return checkRevision()" Text="Add Config" />
                            <asp:Button runat="server" ID="btnClear" Text="Clear" />
							<asp:Button runat="server" ID="btnSearch" Text="Search" />
                            <br />
                            <asp:Label runat="server" ID="lblUpdateMessage"></asp:Label>
                        </td>
						<td>
							<a onclick="viewEditor()">Editor</a>
							&nbsp;&nbsp;&nbsp;
							<asp:LinkButton runat="server" ID="lnkSchema" Text="Show Schema" OnClientClick="viewSchema();"></asp:LinkButton>

							&nbsp;&nbsp;&nbsp;
							<asp:LinkButton runat="server" ID="lnkXMLTempltate" Text="Show XML Template"  OnClientClick="viewXMLTemplate();"></asp:LinkButton>
							&nbsp;&nbsp;&nbsp;
							<asp:LinkButton runat="server" ID="lnkLoadPreviousConfig" Text="Load Previous Config" ></asp:LinkButton>
							&nbsp;&nbsp;&nbsp;
                            <asp:LinkButton runat="server" ID="lnkUploadCSS" Text="Upload CSS" OnClientClick="openUploadCSS()"  ></asp:LinkButton>
							&nbsp;&nbsp;&nbsp;
							<asp:LinkButton runat="server" ID="lnkUploadLogo" Text="Upload Logo" OnClientClick="openUploadLogo()"></asp:LinkButton>
							&nbsp;&nbsp;&nbsp;							
							<asp:LinkButton runat="server" ID="lnkNoteBook" Text="Notebook" OnClientClick="viewNoteBook()"></asp:LinkButton>
							
							<asp:HiddenField runat="server" ID="hdRevisionID" Value="-1"/>
							<br />
                            <asp:Label runat="server" ID="lblUpdateMessage2"></asp:Label>
						</td>
                    </tr>
                </table>
				Note: <input type="text" runat="server" id="txtNote" maxlength="200" style="width: 1000px" />
            </div>
			
            <div>
                <div>
                    <table class="blue_table" cellpadding="0" cellspacing="0">
                        <tr>
							 <th >
								 
                                <asp:LinkButton runat="server" ID="lnkSortLenderRef" Text="LenderRef" CommandArgument='LenderRef'
                                            CommandName="SORT"></asp:LinkButton>
                            </th>
							<th >
                                Note
                            </th>
                            <%--<th>
                                OrganizationID
                            </th>--%>
                            <th>
                                LenderID
                            </th>
                            <th>
                                
								<asp:LinkButton runat="server" ID="LinkReturnURL" Text="Return URL" CommandArgument='ReturnURL'
                                            CommandName="SORT"></asp:LinkButton>
                            </th>
                            <th>
                                LPQ Login
                            </th>
                            <th>
                                
								<asp:LinkButton runat="server" ID="LinkLPQURL" Text="LPQ URL" CommandArgument='LPQURL'
                                            CommandName="SORT"></asp:LinkButton>
                            </th>
							 <th>
                               
								 <asp:LinkButton runat="server" ID="LinkCreatedDate" Text=" Created Date" CommandArgument='CreatedDate'
                                            CommandName="SORT"></asp:LinkButton>
                            </th>
							 <th>
                                
								<asp:LinkButton runat="server" ID="LinkModifiedDate" Text="ModifiedDate" CommandArgument='LastModifiedDate'
                                            CommandName="SORT"></asp:LinkButton>
                            </th>
							<th>
                                ModifiedBy
                            </th>
                            <th align="center">
                                Action
                            </th>
                        </tr>
                        <asp:Repeater runat="server" ID="rptLenderConfigs" >
                            <ItemTemplate>
                                <tr class='<%#If(Container.ItemIndex Mod 2 = 0, "", "alternativeRow")%>'>
									<td>
                                        <%#Left(Eval("LenderRef"), 20)%>
                                    </td>
									<td>
                                        <%#Left(Eval("Note"), 40)%>
                                    </td>
                                    <%--<td>
                                        <%#Left(Eval("OrganizationID").ToString, 8)%>
                                    </td>--%>
                                    <td>
                                        <%#Left(Eval("LenderID").ToString, 8)%>
                                    </td>                                    
                                    <td>
                                        <%#Left(Eval("ReturnURL"), 20)%>
                                    </td>
                                    <td>
                                        <%#Left(Eval("LPQLogin").ToString, 4)%>
                                    </td>
                                    <td>
                                        <%#Left(Eval("LPQURL"), 20)%>
                                    </td>
									 <td>
                                        <%#GetDateString(Eval("CreatedDate"))%>
                                    </td>
									 <td>
                                        <%#GetDateString(Eval("LastModifiedDate"))%>
                                    </td> 
									<td>
                                        <%#Left(Eval("FirstName").ToString, 10)%> &nbsp <%#Left(Eval("LastName").ToString, 10)%>  
                                    </td>
                                    <td style="width:140px; text-align: right;">
                                        <asp:LinkButton runat="server" ID="lnkView" OnClientClick="closeEditorWindow()" Text="View" CommandArgument='<%#Eval("LenderConfigID") %>'
                                            CommandName="VIEW"></asp:LinkButton>&nbsp;|
										<asp:LinkButton runat="server" ID="lnkEdit" OnClientClick="return loadDetail(this)" data-id='<%#Eval("LenderConfigID") %>' Text="Edit" CommandArgument='<%#Eval("LenderConfigID") %>'
                                            CommandName="EDIT"></asp:LinkButton>&nbsp;|
										<a data-command="show-audit" data-lender-config-id="<%#Eval("LenderConfigID")%>">Audit</a>
                                    </td>
                                </tr>
                            </ItemTemplate>
                        </asp:Repeater>
                    </table>
                </div>
            </div>
        </div>
    </div>
    </form>
	<div id="dlgAuditPopup" title="Modification History">
		<div class="content"></div>
	</div>
	<asp:Panel runat="server" ID="pnlControls" Visible="False">
		<table class="blue_table" cellpadding="0" cellspacing="0">
			<tr>
				<th>Modified Date</th>
				<th>Modifier Name</th>
				<th>Audit Changes</th>
			</tr>
			<asp:Repeater runat="server" ID="rptAuditLogs" >
				<ItemTemplate>
					<tr class='<%#If(Container.ItemIndex Mod 2 = 0, "", "alternativeRow")%>'>
						<td>
							<%#CDate(Eval("LogTime")).ToString("MM/dd/yyyy hh:mm tt")%>
						</td>
						<td>
							<%#Eval("LastModifiedByUserName")%>
						</td>
						<td>
							<%#Eval("AuditChanges")%>
						</td>                                    
					</tr>
				</ItemTemplate>
			</asp:Repeater>
		</table>
	</asp:Panel>
</body>
</html>
<script type="text/javascript">
	$(document).ready(function() {
		setTimeout("$('#lblWarningMessage').hide(1000);", 9000);
		setTimeout("$('#lblUpdateMessage').hide(1000);", 9000);
		setTimeout("$('#lblUpdateMessage2').hide(1000);", 3000);
		$("#dlgAuditPopup").dialog({
			autoOpen: false, modal: true, width: 1024, maxHeight: 500, buttons: {
				Close: function () { $(this).dialog("close"); }
			}
		});
		$("a[data-command='show-audit']").click(function () {
			var lenderConfigId = $(this).data("lender-config-id");
			var $self = $(this);
			$self.text("Loading");
			$.post("editconfig.aspx", { command: "getAuditLogs", lenderConfigId: lenderConfigId }, "html").success(function (responseHtml) {
				$("#dlgAuditPopup").html(responseHtml);
				$("#dlgAuditPopup").dialog("open");
			}).complete(function() {
				$self.text("Audit");
			});
		});
		
	});
	function showWarningDialog(message, yesLabel, noLabel, onCallbackYes, onCallbackNo) {
		BootstrapDialog.confirm({
			title: 'WARNING',
			message: message,
			type: BootstrapDialog.TYPE_WARNING,
			closable: false,
			draggable: true,
			btnCancelLabel: noLabel,
			btnOKLabel: yesLabel,
			btnOKClass: 'btn-warning',
			callback: function (result) {
				if (result) {
					if ($.isFunction(onCallbackYes)) {
						onCallbackYes();
					}
				} else {
					if ($.isFunction(onCallbackNo)) {
						onCallbackNo();
					}
				}
			}
		});
	}
	function checkRevision() {
		var revisionId = $("#hdRevisionID").val();
		if (revisionId == -1) {
			return true;
		}
		$.ajax({
			url: "editconfig.aspx",
			cache: false,
			type: 'POST',
			dataType: 'html',
			data: { command: "checkRevision", lenderConfigId: $("#hfLenderConfigID").val(), loadedRevision: revisionId },
			success: function (responseText) {
				var response = $.parseJSON(responseText);
				if (response.IsSuccess) {
					if (response.Info.HasChanged) {
						if (confirm(response.Info.ChangedBy + " has changed the configuration since the last time you opened. Do you want to change configuration with your settings?") == true) {
							__doPostBack("btnAddUpdateConfigData", "");
						}
					} else {
						__doPostBack("btnAddUpdateConfigData", "");
					}
				} else {
					alert("error");
					return false;
				}
			},
			async: false
		});
		return false;
	}
</script>

