﻿Imports System.Xml
Imports System.IO
Imports System.Data
Imports Newtonsoft.Json
Imports LPQMobile.BO
Imports LPQMobile.DBManager
Imports LPQMobile.Utils
Imports System.Configuration.ConfigurationManager
Imports System.Linq
Imports Microsoft.XmlDiffPatch


''' <summary>
''' http://lpqmobile.localhost/migrateConfig/editConfig.aspx?username=xxx&password=xxxx
''' </summary>
''' <remarks>
''' IterationID is only available for legacy config.  It lets user knows ahead of time if someone is already working on the document.  NOt reliable because user may just open the doc and never save it. 
''' IterationID may be deprecated later if revisionID is working good.
'''</remarks>
Partial Class migrateConfig_editConfig
	Inherits System.Web.UI.Page

	Private _log As log4net.ILog = log4net.LogManager.GetLogger(Me.GetType)

	Public ReadOnly Property CurrentUserInfo() As SmUser
		Get
			Dim user As SmUser
			If Session("CURRENT_USER_INFO") IsNot Nothing Then
				user = DirectCast(Session("CURRENT_USER_INFO"), SmUser)
			Else
				Dim smAuthBL As New SmAuthBL()
				user = smAuthBL.GetCurrentUserFromContext(Context)
				user.Password = String.Empty
				user.Salt = String.Empty
				Session.Add("CURRENT_USER_INFO", user)
			End If
			Return user
		End Get
	End Property

#Region "Page Events"
	Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
		_log.Info("editConfig: Page loading")
		lblUpdateMessage.Text = ""
		lblUpdateMessage2.Text = ""

		If Not IsPostBack Then
			If Request.Params("command") = "getAuditLogs" And Request.Params("lenderConfigId") IsNot Nothing Then
				pnlControls.Visible = True
				BindAuditLogs()

			ElseIf Request.Params("command") = "checkOut" Then	'--this is just to validate iterrationID, NO modification to db 
				Dim lenderConfigId As Guid = Common.SafeGUID(Request.Params("id"))
				Dim oManager As New CLenderConfigManager()
				Dim oLenderConfig As CLenderConfig = oManager.GetLenderConfigByID(lenderConfigId)
				Dim res = New With {.result = True, .message = ""}
				If oLenderConfig IsNot Nothing Then
					If oLenderConfig.IterationID Mod 2 <> 0 Then  'if is an odd number then it is already open
						If oLenderConfig.LastOpenedByUserID = CurrentUserInfo.UserID Then
							res = New With {.result = False, .message = "You are editing this document in another window at " & oLenderConfig.LastOpenedDate & ".  Do you want to continue and override it?"}
						Else
							res = New With {.result = False, .message = oLenderConfig.LastModifiedByUserFullName & " is editing this file at " & oLenderConfig.LastOpenedDate & ". Do you want to continue?"}
						End If
					End If
				End If
				Response.Clear()
				Response.Write(JsonConvert.SerializeObject(res))
				Response.End()
				Return
			ElseIf Request.Params("command") = "checkRevision" Then
				Dim res As CJsonResponse
				Try
					Dim lenderConfigID As Guid = Common.SafeGUID(Request.Params("lenderconfigid"))
					Dim loadedRevisionID As Integer = Common.SafeInteger(Request.Params("loadedRevision"))
					Dim LenderConfig = New CLenderConfigManager().GetLenderConfigByID(lenderConfigID)
					If loadedRevisionID = LenderConfig.RevisionID Then
						res = New CJsonResponse(True, "", New With {.HasChanged = False})
					Else
						res = New CJsonResponse(True, "", New With {.HasChanged = True, .ChangedBy = LenderConfig.LastModifiedByUserFullName})	'changed
					End If
				Catch ex As Exception
					_log.Error("Could not CheckRevision.", ex)
					res = New CJsonResponse(False, "Could not check revision. Please try again", True)
				End Try
				Response.Write(JsonConvert.SerializeObject(res))
				Response.End()
				Return
			End If
			If Not Common.IsInternalIP(Request.UserHostAddress) Then
				Response.Write("This only works inside ML network")
				Return
			End If


			''''DEPRECATED, USE CREDENTIAL OF APM INSTEAD
			'If Not Page.User.Identity.IsAuthenticated Then
			'	FormsAuthentication.RedirectToLoginPage()
			'Else
			'	Dim sHost As String = HttpContext.Current.Request.Url.Host
			'	Dim loggedInUserType = Session(sHost & "LoggedInUserType")
			'	If loggedInUserType Is Nothing Then	 'expired session
			'		FormsAuthentication.RedirectToLoginPage()
			'		Return
			'	End If
			'	If loggedInUserType Is Nothing Or loggedInUserType.ToString().ToUpper() <> "A" Then
			'		btnAddUpdateConfigData.Enabled = False
			'		btnAddUpdateConfigData.Visible = False
			'		lnkUploadCSS.Enabled = False
			'		lnkUploadCSS.Visible = False
			'		lnkUploadLogo.Enabled = False
			'		lnkUploadLogo.Visible = False
			'	End If
			'	bindLenderConfigs()
			'End If
			bindLenderConfigs()
			'Else ' postback


			'	If Not Page.User.Identity.IsAuthenticated Then
			'		FormsAuthentication.RedirectToLoginPage()
			'	Else
			'		Dim sHost As String = HttpContext.Current.Request.Url.Host
			'		Dim loggedInUserType = Session(sHost & "LoggedInUserType")
			'		If loggedInUserType Is Nothing Then	'expired session
			'			FormsAuthentication.RedirectToLoginPage()
			'			Return
			'		End If
			'	End If

		End If


	End Sub

#End Region

#Region "Controls Events"

	Protected Sub rptLenderConfigs_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.RepeaterCommandEventArgs) Handles rptLenderConfigs.ItemCommand
		If e.CommandName = "EDIT" Then
			lblUpdateMessage.Text = ""
			Dim oManager As New CLenderConfigManager()
			Dim configID As Guid = Common.SafeGUID(e.CommandArgument)
			Dim oLender As CLenderConfig = oManager.GetLenderConfigByID(configID)
			If Not oLender Is Nothing Then
				Dim curIterationID = setCheckoutLog(oLender)	 'save LastOpenedBy & (IterationID +1 or 2) to db
				txtOrganizationID.Value = oLender.OrganizationId.ToString()
				txtLenderID.Value = oLender.LenderId.ToString()
				txtLenderRef.Value = oLender.LenderRef
				txtReturnURL.Value = oLender.ReturnURL
				txtLPQLogin.Value = oLender.LPQLogin
				If btnAddUpdateConfigData.Enabled Then
                    txtLPQPassword.Value = maskPassword(oLender.LPQPW)
                    Try
                        'insert updated IterationID into xml
                        Dim oDocXML As New XmlDocument
                        oDocXML.LoadXml(oLender.ConfigData)
                        oDocXML = maskAllPasswords(oDocXML)
                        Dim oXmlElement As XmlElement = oDocXML.DocumentElement
                        oXmlElement.SetAttribute("IterationID", curIterationID) ''this is used to verify the xml pasted back is the correct version
                        txtConfigData.Value = oXmlElement.OuterXml
                    Catch ex As Exception
                        _log.Warn("Fail to load ConfigData to xmlDoc so using original ConfigData: " & oLender.ConfigData, ex)
                        _log.Warn("Fix ConfigData and use IterationID = " & curIterationID)
                        txtConfigData.Value = oLender.ConfigData
                    End Try

                Else
					txtLPQPassword.Value = "XXXXXX"
					txtConfigData.Value = CSecureStringFormatter.MaskSensitiveXMLData(oLender.ConfigData)
				End If
				hfLenderConfigID.Value = oLender.ID.ToString
				txtLPQURL.Value = oLender.LPQURL
				txtNote.Value = oLender.Note
				btnAddUpdateConfigData.Text = "Update Config"
				btnAddUpdateConfigData.Visible = True
				hdRevisionID.Value = oLender.RevisionID.ToString()
			Else
				resetForm()
			End If
		ElseIf e.CommandName = "VIEW" Then
			lblUpdateMessage.Text = ""
			Dim oManager As New CLenderConfigManager()
			Dim configID As Guid = Common.SafeGUID(e.CommandArgument)
			Dim oLender As CLenderConfig = oManager.GetLenderConfigByID(configID)
			If Not oLender Is Nothing Then
				txtOrganizationID.Value = oLender.OrganizationId.ToString()
				txtLenderID.Value = oLender.LenderId.ToString()
				txtLenderRef.Value = oLender.LenderRef
				txtReturnURL.Value = oLender.ReturnURL
				txtLPQLogin.Value = oLender.LPQLogin

				If btnAddUpdateConfigData.Enabled Then
                    txtLPQPassword.Value = maskPassword(oLender.LPQPW)
                    Try
                        Dim oDocXML As New XmlDocument
                        oDocXML.LoadXml(oLender.ConfigData)
                        oDocXML = maskAllPasswords(oDocXML)
                        txtConfigData.Value = oDocXML.InnerXml
                    Catch ex As Exception
                        _log.Warn("Fail to load ConfigData to xmlDoc so using original ConfigData: " & oLender.ConfigData, ex)
                        txtConfigData.Value = oLender.ConfigData
                    End Try
                Else
					txtLPQPassword.Value = "XXXXXX"
					txtConfigData.Value = CSecureStringFormatter.MaskSensitiveXMLData(oLender.ConfigData)
				End If
				hfLenderConfigID.Value = oLender.ID.ToString
				txtLPQURL.Value = oLender.LPQURL
				txtNote.Value = oLender.Note
				btnAddUpdateConfigData.Visible = False
			End If
		End If
	End Sub

	Protected Sub lnkLoadPreviousConfig_Click(sender As Object, e As EventArgs) Handles lnkLoadPreviousConfig.Click
		lblUpdateMessage2.Text = ""
		If Common.SafeString(txtLenderRef.Value) = "" Then
			lblUpdateMessage2.Style.Add("color", "red")
			lblUpdateMessage2.Text = "PLease select a lender first"
			Return
		End If
		Dim oManager As New CLenderConfigManager()
		Dim LenderRef As String = Common.SafeString(txtLenderRef.Value)
		Dim LenderConfigID As Guid = Common.SafeGUID(hfLenderConfigID.Value)
		Dim oLender As CLenderConfig = oManager.GetLenderConfigLogByLenderConfigID(LenderConfigID)	 'get from log
		If oLender.ID <> Guid.Empty Then
			txtOrganizationID.Value = oLender.OrganizationId.ToString()
			txtLenderID.Value = oLender.LenderId.ToString()
			txtLenderRef.Value = oLender.LenderRef
			txtReturnURL.Value = oLender.ReturnURL
			txtLPQLogin.Value = oLender.LPQLogin

			If btnAddUpdateConfigData.Enabled Then
				txtLPQPassword.Value = maskPassword(oLender.LPQPW)

				'insert current IterationID into old xml
				Dim curIterationID As Integer = oManager.GetLenderConfigByLenderConfigID(LenderConfigID).IterationID
				Dim oDocXML As New XmlDocument
				oDocXML.LoadXml(oLender.ConfigData)
				oDocXML = maskAllPasswords(oDocXML)
				Dim oXmlElement As XmlElement = oDocXML.DocumentElement
				oXmlElement.SetAttribute("IterationID", curIterationID)	''this is used to verify the xml pasted back is the correct version

				txtConfigData.Value = oXmlElement.OuterXml
			Else
				txtLPQPassword.Value = "XXXXXX"
				txtConfigData.Value = CSecureStringFormatter.MaskSensitiveXMLData(oLender.ConfigData)
			End If

			txtLPQURL.Value = oLender.LPQURL
			txtNote.Value = oLender.Note

			lblUpdateMessage2.Style.Add("color", "green")
			lblUpdateMessage2.Text = "Successfully Load Previous Config"
		End If

	End Sub

	Protected Sub lnkSortLenderRef_Click(sender As Object, e As EventArgs) Handles lnkSortLenderRef.Click, LinkCreatedDate.Click, LinkLPQURL.Click, LinkModifiedDate.Click, LinkReturnURL.Click
		bindLenderConfigs(sender.CommandArgument)
	End Sub

	Protected Sub btnAddUpdateConfigData_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAddUpdateConfigData.Click
		If Not validateConfigXml() Then
			Return
		End If

		updateConfigData()
		bindLenderConfigs()
	End Sub

	Private Function validateConfigXml() As Boolean
        'Return True
        Try
            Dim xmlConfig = Common.SafeString(txtConfigData.Value)
            If xmlConfig.IndexOf("OVERRIDE", StringComparison.Ordinal) = 0 Then	'use this to store template
				Return True
			End If

			Dim tempDoc = New XmlDocument()
			tempDoc.LoadXml(xmlConfig)
			Dim oXmlElement As XmlElement = tempDoc.DocumentElement
			If Common.SafeGUID(hfLenderConfigID.Value) = Guid.Empty Then 'this is a new config so no need for checking Iteration ID
				'do nothing
			Else 'check iterationID from xml in db
				Dim iterationID As Integer = Common.SafeInteger(oXmlElement.GetAttribute("IterationID"))
				Dim oManager As New CLenderConfigManager()
				Dim oLender As CLenderConfig = oManager.GetLenderConfigByLenderConfigID(Common.SafeGUID(hfLenderConfigID.Value)) 'guarantee existence of lender obj

				If oLender.LastOpenedByUserID <> CurrentUserInfo.UserID Then
					lblUpdateMessage.Style.Add("color", "red")
					lblUpdateMessage.Text = String.Format("Cannot save the document. {0} is editing this file", oLender.LastOpenedByUserFullName)
					Return False
				End If

				If iterationID <> oLender.IterationID Then
					lblUpdateMessage.Style.Add("color", "red")
					lblUpdateMessage.Text = String.Format("Cannot save the XML config data. This copy & paste xml is out of date.")
					Return False
				End If

				'because, in current code, we identify a config item by OrgID and LenderId, not by its ID (see the updateConfigData method)
				' there will be a situation, by chance, the modified OrgID and LenderID match with another config item => open a specified item but save changes to another item
				'so, this lines of code to make sure that user only be able to save changes a document that they have opened by themself before
				If oLender.IterationID Mod 2 = 0 And oLender.IterationID <> 0 Then
					lblUpdateMessage.Style.Add("color", "red")
					lblUpdateMessage.Text = "You cannot save changes the document without open it"
					Return False
				End If
			End If


			Dim sValidationResult = ValidateXMLConstruction(tempDoc)
			If sValidationResult <> "" Then
				lblUpdateMessage.Style.Add("color", "red")
				lblUpdateMessage.Text = "Config Data XML is invalid: " & sValidationResult
				Return False
			End If

			''validate branch id
			Dim sValidateBranchId = ValidateDefaultBranchId(oXmlElement)
			If sValidateBranchId <> "" Then
				lblUpdateMessage.Style.Add("color", "red")
				lblUpdateMessage.Text = sValidateBranchId
				Return False
			End If
		Catch ex As Exception
			lblUpdateMessage.Style.Add("color", "red")
			lblUpdateMessage.Text = "Config Data XML is invalid."
			_log.Error("validateConfigXml", ex)
			Return False
		End Try

		Return True
	End Function
    Private Function ValidateDefaultBranchId(ByVal oXmlElement As XmlElement) As String
        Dim oConfig As CWebsiteConfig = New CWebsiteConfig(CType(oXmlElement, XmlNode))

        'If there is no change on LPQ Password. Because we have masked it before, we need to get its original value from database
        If txtLPQPassword.Value.EndsWith("_XXXX") AndAlso txtLPQPassword.Value = oConfig.APIPassword Then
            Dim oManager As New CLenderConfigManager()
            Dim oLender As CLenderConfig = oManager.GetLenderConfigByLenderConfigID(Common.SafeGUID(hfLenderConfigID.Value))
            If oLender IsNot Nothing Then
                oConfig.APIPassword = oLender.LPQPW
            Else
                Return "Bad data."
            End If
        Else
            'otherwise, we use the latest one
            oConfig.APIPassword = txtLPQPassword.Value
        End If

        oConfig.APIUser = Common.SafeString(txtLPQLogin.Value)
        oConfig.SubmitLoanUrl = Common.SafeString(txtLPQURL.Value)
        oConfig.BaseSubmitLoanUrl = oConfig.SubmitLoanUrl.Substring(0, oConfig.SubmitLoanUrl.IndexOf("/decisionloan", System.StringComparison.Ordinal))
        Dim sMessage As String = ""
        If Common.SafeString(oConfig.sDefaultLoanBranchID) = "" AndAlso Common.SafeString(oConfig.sDefaultXABranchID) = "" Then Return sMessage
        Dim oDownloadedBranchIds As New List(Of String)
        Dim branchNameDic As New Dictionary(Of String, String)
        If String.IsNullOrEmpty(oConfig.LenderCode) Then
            branchNameDic = CDownloadedSettings.DownloadLenderServiceConfigInfo(oConfig).BranchNamesDic
        Else
            Dim branches As List(Of DownloadedSettings.CBranch) = CDownloadedSettings.Branches(oConfig)
            branchNameDic = branches.Where(Function(b) b.BranchCode <> "").ToDictionary(Function(x) x.BranchCode, Function(x) x.BranchName)
        End If
        If branchNameDic.Count > 0 Then
            oDownloadedBranchIds = branchNameDic.Keys.ToList()
        End If
        If Common.SafeString(oConfig.sDefaultLoanBranchID) <> "" Then
            Dim sDefaultLoanBranchId = oDownloadedBranchIds.FirstOrDefault(Function(x) x = oConfig.sDefaultLoanBranchID)
            If sDefaultLoanBranchId Is Nothing OrElse sDefaultLoanBranchId = "" Then
                sMessage += "Default Loan Branch Id " & oConfig.sDefaultLoanBranchID & " not found. "
            End If
        End If
        If Common.SafeString(oConfig.sDefaultXABranchID) <> "" Then
            Dim sDefaultXABranchId = oDownloadedBranchIds.FirstOrDefault(Function(x) x = oConfig.sDefaultXABranchID)
            If sDefaultXABranchId Is Nothing OrElse sDefaultXABranchId = "" Then
                sMessage += "Default XA Branch Id " & oConfig.sDefaultXABranchID & " not found. "
            End If
        End If
        Return sMessage
    End Function
    Private Function ValidateXMLConstruction(ByVal pXML As XmlDocument) As String
        'Return ""
        Dim oSchemaVal As New XMLTools.CSchemaValidator
        If oSchemaVal.validate(pXML.OuterXml, New XmlTextReader(Server.MapPath("~/migrateConfig/config.xsd"))) = False Then
            Dim sXMLErrorMessage As String = oSchemaVal.ParseError
            _log.Info("XML Construction failed XSD Validation: " & sXMLErrorMessage)
            Return sXMLErrorMessage
        Else
            Return "" 'pass
        End If
    End Function
    Protected Sub btnClear_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClear.Click
		resetForm()
	End Sub

#End Region

#Region "Private Events"

	Private Function maskPassword(encryptedPassword As String) As String
		Dim decryptedPassword As String = Common.DecryptPassword(encryptedPassword)
		Dim isAdmin = Common.SafeString(Request.QueryString("isadmin")).ToUpper = "Y"
        If Not String.IsNullOrWhiteSpace(decryptedPassword) Andalso Not isAdmin Then
			Return decryptedPassword.Substring(0, 3) & "_XXXX"
		End If
		Return encryptedPassword
	End Function

	Private Function maskAllPasswords(configXml As XmlDocument) As XmlDocument
		Dim passwordNode As XmlNode = configXml.SelectSingleNode("WEBSITE/@password")
		Dim passwordLenderLevel As XmlNode = configXml.SelectSingleNode("WEBSITE/@password_lender_level")
		Dim lqbPassword As XmlNode = configXml.SelectSingleNode("WEBSITE/LQB/@password")

		If passwordNode IsNot Nothing Then
			passwordNode.InnerText = maskPassword(passwordNode.InnerText)
		End If
		If passwordLenderLevel IsNot Nothing Then
			passwordLenderLevel.InnerText = maskPassword(passwordLenderLevel.InnerText)
		End If
		If lqbPassword IsNot Nothing Then
			lqbPassword.InnerText = maskPassword(lqbPassword.InnerText)
		End If
		Return configXml
	End Function

	Private Function setCheckoutLog(ByVal poLenderConfig As CLenderConfig) As Integer
		Dim iterationID As Integer = 0
		Dim oLender As CLenderConfig = poLenderConfig
		Dim rnd As New CSecureRandom()
		Dim rndNum As Integer = rnd.GetValue(100, 100000)

		'make iterationID an odd number so the system know it has been checkout
		If rndNum Mod 2 = 0 Then
			iterationID = rndNum + 1
		Else
			iterationID = rndNum + 2
		End If

		oLender.IterationID = iterationID
		oLender.LastOpenedByUserID = CurrentUserInfo.UserID
		oLender.LastOpenedDate = Now
		Dim oManager As New CLenderConfigManager()
		'oManager.SaveLenderConfig(oLender)
		oManager.UpdateCheckoutFields(oLender)

		Return iterationID

	End Function


    ''' <summary>
    ''' This will either add new config or update config
    ''' </summary>
    Private Sub updateConfigData()
        If (txtOrganizationID.Value <> "" And txtLenderID.Value <> "" And txtLPQLogin.Value <> "" And txtLPQPassword.Value <> "" And txtLPQURL.Value <> "" And txtLenderRef.Value <> "") = False Then
            lblUpdateMessage.Style.Add("color", "red")
            lblUpdateMessage.Text = "Some required fields are missing value."
            Return
        End If

        Try
            Dim oManager As New CLenderConfigManager()
            Dim oLender As CLenderConfig = oManager.GetLenderConfigByLenderConfigID(Common.SafeGUID(hfLenderConfigID.Value))

            ' Return an error for lenderrefs that contain invalid characters.
            ' Prior to the implementation of this, no existing lenderref contains
            ' invalid characters.
            If Common.IsValidLenderRef(txtLenderRef.Value) = False Then
                Throw New Exception("The LenderRef contains invalid characters")
            End If

            Dim currentLpqDomain As String = ""
            Dim currentLpqUrl As String = ""
            Dim currentLenderCode As String = ""
            Dim currentOrgCode As String = ""

            If oLender Is Nothing Then
                oLender = New CLenderConfig()
            End If
            currentLpqUrl = oLender.LPQURL
            oLender.OrganizationId = Common.SafeGUID(txtOrganizationID.Value)
            oLender.LenderId = Common.SafeGUID(txtLenderID.Value)
            oLender.LenderRef = Common.SafeString(txtLenderRef.Value)
            oLender.ReturnURL = Common.SafeString(txtReturnURL.Value)
            oLender.LPQLogin = Common.SafeString(txtLPQLogin.Value)
            If Not Common.SafeString(txtLPQPassword.Value).EndsWith("_XXXX") Then
                oLender.LPQPW = Common.EncryptPassword(Common.SafeString(txtLPQPassword.Value))
            Else
                oLender.LPQPW = Common.EncryptPassword(oLender.LPQPW)
            End If
            oLender.LPQURL = Common.SafeString(txtLPQURL.Value)
            oLender.RevisionID = oLender.RevisionID + 1

            oLender.Note = Common.SafeString(txtNote.Value)
            oLender.LastModifiedByUserID = CurrentUserInfo.UserID
            oLender.LastOpenedByUserID = CurrentUserInfo.UserID

			'make IterationID an even number after a save operation
			Dim rnd As New CSecureRandom
			Dim rndNum As Integer = rnd.GetValue(100, 100000)
			If rndNum Mod 2 = 0 Then
                oLender.IterationID = rndNum + 2
            Else
                oLender.IterationID = rndNum + 1
            End If

            'need to overwrite attribute in xml with values from UI so the system can do audit 
            'oLender.ConfigData = Common.SafeString(txtConfigData.Value)
            Dim oDocXML As New XmlDocument
            oDocXML.LoadXml(Common.SafeString(txtConfigData.Value))
            Dim oXmlElement As XmlElement = oDocXML.DocumentElement
            oXmlElement.SetAttribute("organization_id", oLender.OrganizationId.ToString.Replace("-", ""))
            oXmlElement.SetAttribute("lender_id", oLender.LenderId.ToString.Replace("-", ""))
            oXmlElement.SetAttribute("lender_ref", oLender.LenderRef)
            oXmlElement.SetAttribute("finished_url", oLender.ReturnURL)
            oXmlElement.SetAttribute("user", oLender.LPQLogin)
            oXmlElement.SetAttribute("password", oLender.LPQPW)
            oXmlElement.SetAttribute("loan_submit_url", oLender.LPQURL)
            oXmlElement.RemoveAttribute("IterationID")  'don't want this in the audit


            Dim oCurrentDocXml As New XmlDocument
            'if adding new config, oLender.ConfigData will be empty bc it is not in the system yet
            Dim bIsAddNewConfig As Boolean = String.IsNullOrEmpty(oLender.ConfigData)
            If Not bIsAddNewConfig Then
                Try
                    oCurrentDocXml.LoadXml(oLender.ConfigData)
                    If Not String.IsNullOrWhiteSpace(currentLpqUrl) Then
                        Dim workingDomainMatch As Match = Regex.Match(currentLpqUrl, SmSettings.extractDomainRegexPattern, RegexOptions.IgnoreCase)
                        If workingDomainMatch.Success Then
                            Dim extractedData = Common.ExtractDataFromConfigXml(oCurrentDocXml, New List(Of String) From {"WEBSITE/@org_code", "WEBSITE/@lender_code"})
                            currentLenderCode = extractedData("WEBSITE/@lender_code")
                            currentOrgCode = extractedData("WEBSITE/@org_code")
                            currentLpqDomain = workingDomainMatch.Groups(1).Value
                        End If
                    End If


                Catch ex As Exception
                    _log.Warn("Previous Config is not a valid XML: " & oLender.ConfigData, ex)
                    _log.Warn("All pw field need to be entered again")
                    bIsAddNewConfig = True ' since previous config is bad 
                End Try
            End If

            Dim passwordLenderLevel As XmlNode = oDocXML.SelectSingleNode("WEBSITE/@password_lender_level")
            Dim lqbPassword As XmlNode = oDocXML.SelectSingleNode("WEBSITE/LQB/@password")

            'logic for choosing between current pw or new pw
            If passwordLenderLevel IsNot Nothing Then
                If passwordLenderLevel.InnerText.EndsWith("_XXXX") AndAlso Not bIsAddNewConfig Then
                    Dim currentPasswordLenderLevel As XmlNode = oCurrentDocXml.SelectSingleNode("WEBSITE/@password_lender_level")
                    If currentPasswordLenderLevel IsNot Nothing Then
                        passwordLenderLevel.InnerText = Common.EncryptPassword(currentPasswordLenderLevel.InnerText)
                    End If
                Else
                    passwordLenderLevel.InnerText = Common.EncryptPassword(passwordLenderLevel.InnerText)
                End If
            End If
            If lqbPassword IsNot Nothing Then
                If lqbPassword.InnerText.EndsWith("_XXXX") AndAlso Not bIsAddNewConfig Then
                    Dim currentLqbPassword As XmlNode = oCurrentDocXml.SelectSingleNode("WEBSITE/LQB/@password")
                    If currentLqbPassword IsNot Nothing Then
                        lqbPassword.InnerText = Common.EncryptPassword(currentLqbPassword.InnerText)
                    End If
                Else
                    lqbPassword.InnerText = Common.EncryptPassword(lqbPassword.InnerText)
                End If
            End If
            Dim cachedDiff = getCachedDiffData(oLender.ConfigData, oXmlElement.OuterXml)
            oLender.ConfigData = oXmlElement.OuterXml
            ''get the different xml between previous config and current config and save it to CachedDiff column 
            oManager.SaveLenderConfig(oLender)
			oManager.LogLenderConfig(oLender, Request.UserHostAddress, CurrentUserInfo.UserID, cachedDiff)

			Dim newLenderCode As String = ""
            Dim newOrgCode As String = ""
            Dim newLpqDomain As String = ""

            If Not String.IsNullOrWhiteSpace(oLender.LPQURL) Then
                Dim extractedData = Common.ExtractDataFromConfigXml(oXmlElement.OwnerDocument, New List(Of String) From {"WEBSITE/@org_code", "WEBSITE/@lender_code"})
                Dim workingDomainMatch As Match = Regex.Match(oLender.LPQURL, SmSettings.extractDomainRegexPattern, RegexOptions.IgnoreCase)
                If workingDomainMatch.Success Then
                    newLpqDomain = workingDomainMatch.Groups(1).Value
                    newLenderCode = extractedData("WEBSITE/@lender_code")
                    newOrgCode = extractedData("WEBSITE/@org_code")
                End If
            End If
            If bIsAddNewConfig Then
                If Not String.IsNullOrWhiteSpace(newLpqDomain) AndAlso Not String.IsNullOrWhiteSpace(newLenderCode) AndAlso Not String.IsNullOrWhiteSpace(newOrgCode) Then
                    HttpContext.Current.Cache.Remove("SSO_AVAILABLE_ROLES_" & newLpqDomain.NullSafeToUpper_ & "_" & newLenderCode.NullSafeToUpper_() & "_" & newOrgCode.NullSafeToUpper_())
                    '_log.ErrorFormat("CLEAR GetRolesOfCurrentUser cache {0}", "SSO_AVAILABLE_ROLES_" & newLpqDomain.NullSafeToUpper_ & "_" & newLenderCode.NullSafeToUpper_() & "_" & newOrgCode.NullSafeToUpper_())
                End If
            ElseIf currentLpqDomain.NullSafeToUpper_() <> newLpqDomain.NullSafeToUpper_() OrElse currentLenderCode.NullSafeToUpper_() <> newLenderCode.NullSafeToUpper_() OrElse newOrgCode.NullSafeToUpper_() <> currentOrgCode.NullSafeToUpper_() Then
                If Not String.IsNullOrWhiteSpace(newLpqDomain) AndAlso Not String.IsNullOrWhiteSpace(newLenderCode) AndAlso Not String.IsNullOrWhiteSpace(newOrgCode) Then
                    HttpContext.Current.Cache.Remove("SSO_AVAILABLE_ROLES_" & newLpqDomain.NullSafeToUpper_ & "_" & newLenderCode.NullSafeToUpper_() & "_" & newOrgCode.NullSafeToUpper_())
                    '_log.ErrorFormat("CLEAR GetRolesOfCurrentUser cache {0}", "SSO_AVAILABLE_ROLES_" & newLpqDomain.NullSafeToUpper_ & "_" & newLenderCode.NullSafeToUpper_() & "_" & newOrgCode.NullSafeToUpper_())
                End If
                If Not String.IsNullOrWhiteSpace(currentLpqDomain) AndAlso Not String.IsNullOrWhiteSpace(currentLpqDomain) AndAlso Not String.IsNullOrWhiteSpace(currentLpqDomain) Then
                    HttpContext.Current.Cache.Remove("SSO_AVAILABLE_ROLES_" & currentLpqDomain.NullSafeToUpper_ & "_" & currentLenderCode.NullSafeToUpper_() & "_" & currentOrgCode.NullSafeToUpper_())
                    '_log.ErrorFormat("CLEAR GetRolesOfCurrentUser cache {0}", "SSO_AVAILABLE_ROLES_" & currentLpqDomain.NullSafeToUpper_ & "_" & newLenderCode.NullSafeToUpper_() & "_" & newOrgCode.NullSafeToUpper_())
                End If
            End If
            lblUpdateMessage.Style.Add("color", "green")
            lblUpdateMessage.Text = "Successfully updated"
            resetForm()
        Catch ex As Exception
            _log.Error("Error while UpdateConfigData", ex)
            lblUpdateMessage.Style.Add("color", "red")
            lblUpdateMessage.Text = ex.Message
        End Try
    End Sub
	Private Function getCachedDiffData(ByVal psPreviousConfigData As String, ByVal psCurrentConfigData As String) As String
		If String.IsNullOrEmpty(psPreviousConfigData) Then
			Return "Portal initially created"
		End If

		Dim sbDiff As New StringBuilder()
		Dim diffGramStr As String = ""
		Dim sCachedDiff As String = ""
		Dim oPreviousXMLDoc As New XmlDocument()
		Dim oCurrentXMLDoc As New XmlDocument()
		oPreviousXMLDoc.LoadXml(psPreviousConfigData)
		oCurrentXMLDoc.LoadXml(psCurrentConfigData)
		oPreviousXMLDoc = Common.EncryptAllPasswordInConfigXml(oPreviousXMLDoc)
		oCurrentXMLDoc = Common.EncryptAllPasswordInConfigXml(oCurrentXMLDoc)
		Dim xmlComparer As New CXmlComparer(oPreviousXMLDoc.OuterXml, oCurrentXMLDoc.OuterXml)
		If xmlComparer.Compare(diffGramStr) = False Then
			xmlComparer.ParseDiffGram(diffGramStr, sbDiff)
		End If
		sCachedDiff = sbDiff.ToString()
		If String.IsNullOrWhiteSpace(sCachedDiff) Then sCachedDiff = "No changes"
		Return sCachedDiff
	End Function

	Private Sub resetForm()
		txtOrganizationID.Value = ""
		txtLenderID.Value = ""
		txtLenderRef.Value = ""
		txtReturnURL.Value = ""
		txtLPQLogin.Value = ""
		txtLPQPassword.Value = ""
		txtLPQURL.Value = ""
		hfLenderConfigID.Value = ""
		txtConfigData.Value = ""
		txtNote.Value = ""
		hdRevisionID.Value = "-1"

		btnAddUpdateConfigData.Text = "Add Config"
		btnAddUpdateConfigData.Visible = True
	End Sub


	Private Sub bindLenderConfigs(Optional ByVal psSortColumnName As String = "")
		Dim oLender As New CLenderConfigManager()
		Dim oData As DataTable = oLender.GetLenderConfigs()

		If psSortColumnName <> "" Then	'aue sorting
			Dim view As New DataView(oData)	'Put your original dataset into a dataview
			If Session("SortKey") IsNot Nothing Then
				SortKey = Session("SortKey").ToString
			End If
			If Session("SortDir") IsNot Nothing Then
				SortDir = CType(Session("SortDir"), SortDirection)
			End If

			If SortKey = psSortColumnName Then
				' If we're sorting the same column, change the sort direction
				SortDir = invertSortDirection(SortDir)
			Else
				' Otherwise we're either sorting a different column or sorting a column for the first time
				SortDir = WebControls.SortDirection.Ascending
			End If
			SortKey = psSortColumnName
			Session("SortKey") = SortKey
			Session("SortDir") = SortDir

			view.Sort = psSortColumnName & " " & convertSortDirToString(SortDir) ' Sort your data view
			Dim NewDataSet As DataTable = view.ToTable() ' Put your dataview into a new datatable
			rptLenderConfigs.DataSource = NewDataSet
		Else
			rptLenderConfigs.DataSource = oData
		End If

		rptLenderConfigs.DataBind()
	End Sub


	Public Sub BindAuditLogs()
		Try
			Dim lenderConfigId As Guid = Common.SafeGUID(Request.Params("lenderConfigId"))
			Dim oLender As New CLenderConfigManager()
			Dim logList As List(Of CLenderConfigLog) = oLender.GetAuditLogs(lenderConfigId)
            'If logList.Count > 1 Then
            '             logList = ConfigComparer(logList)
            '         End If
            'If logList.Count > 0 Then
            '	logList.RemoveAt(logList.Count - 1)
            'End If
            rptAuditLogs.DataSource = logList
			rptAuditLogs.DataBind()
		Catch ex As Exception
			_log.Error("Could not load audit logs", ex)
		End Try
	End Sub

	Function ConfigComparer(logList As List(Of CLenderConfigLog)) As List(Of CLenderConfigLog)
		Dim idx As Integer = 0
		For Each logItem As CLenderConfigLog In logList
			idx += 1
			If idx = logList.Count Then
				Exit For
			End If
			Dim sbDiff As New StringBuilder()
			Dim prevLogItem As CLenderConfigLog = logList.Item(idx)
			'PropertyComparer(prevLogItem.LenderRef, logItem.LenderRef, "LenderRef", sbDiff)
			'PropertyComparer(prevLogItem.ReturnURL, logItem.ReturnURL, "ReturnURL", sbDiff)
			'PropertyComparer(prevLogItem.LPQLogin, logItem.LPQLogin, "LPQLogin", sbDiff)
			'PropertyComparer(prevLogItem.LPQPW, logItem.LPQPW, "LPQPW", sbDiff)
			'PropertyComparer(prevLogItem.LPQURL, logItem.LPQURL, "LPQURL", sbDiff)
			Dim diffGramStr As String = ""
			Dim xmlComparer As New CXmlComparer(prevLogItem.ConfigData, logItem.ConfigData)
			If xmlComparer.Compare(diffGramStr) = False Then
				xmlComparer.ParseDiffGram(diffGramStr, sbDiff)
			End If
			If sbDiff.Length > 0 Then
				logItem.AuditChanges = sbDiff.ToString()
			End If
		Next
		Return logList
	End Function

	Sub PropertyComparer(sourceValue As String, destValue As String, propName As String, ByRef sbDiff As StringBuilder)
		If sourceValue <> destValue Then
			sbDiff.AppendFormat("<p>{0}: {1} --> {2}</p>", propName, sourceValue, destValue)
		End If
	End Sub


	

	
#End Region

	Protected Sub btnSearch_Click(sender As Object, e As EventArgs) Handles btnSearch.Click
		Dim oLenderConfig As New CLenderConfig
		oLenderConfig.OrganizationId = Common.SafeGUID(txtOrganizationID.Value)
		oLenderConfig.LenderId = Common.SafeGUID(txtLenderID.Value)
		oLenderConfig.LenderRef = Common.SafeString(txtLenderRef.Value)
		oLenderConfig.ReturnURL = Common.SafeString(txtReturnURL.Value)
		oLenderConfig.LPQLogin = Common.SafeString(txtLPQLogin.Value)
		oLenderConfig.LPQURL = Common.SafeString(txtLPQURL.Value)
		oLenderConfig.ConfigData = Common.SafeString(txtConfigData.Value)
		oLenderConfig.Note = Common.SafeString(txtNote.Value)

		Dim oLenderManager As New CLenderConfigManager()
		Dim oData As DataTable = oLenderManager.SearchLenderConfigs(oLenderConfig)

		rptLenderConfigs.DataSource = oData
		rptLenderConfigs.DataBind()
	End Sub


#Region "sort helper"
	Private Function invertSortDirection(ByVal sortDirection As SortDirection) As SortDirection
		Select Case sortDirection
			Case WebControls.SortDirection.Ascending
				Return WebControls.SortDirection.Descending
			Case WebControls.SortDirection.Descending
				Return WebControls.SortDirection.Ascending
			Case Else
				'log.Error("Invalid sort direction: " & SortDir.ToString)
				Return WebControls.SortDirection.Ascending
		End Select
	End Function

	Private Function convertSortDirToString(ByVal sortDirection As SortDirection) As String
		Select Case sortDirection
			Case Web.UI.WebControls.SortDirection.Ascending
				Return "ASC"
			Case Web.UI.WebControls.SortDirection.Descending
				Return "DESC"
			Case Else
				'log.Error("Invalid sort direction: " & sortDirection.ToString)
				Return "ASC"
		End Select
	End Function
	Private _SortKey As String = ""
	Public Property SortKey() As String
		Get
			Return _SortKey
		End Get
		Set(ByVal value As String)
			_SortKey = value
		End Set
	End Property

	Private _SortDir As SortDirection = Web.UI.WebControls.SortDirection.Ascending
	Public Property SortDir() As SortDirection
		Get
			Return _SortDir
		End Get
		Set(ByVal value As SortDirection)
			_SortDir = value
		End Set
	End Property

	Public ReadOnly Property SortDirAsString() As String
		Get
			Return convertSortDirToString(SortDir)
		End Get
	End Property
#End Region

#Region "Format function"
	Protected Function GetDateString(obj As Object) As String
		Dim strResult As String

		Try
			strResult = DateTime.Parse(obj.ToString()).ToShortDateString()
		Catch ex As Exception
			strResult = ""
		End Try

		Return strResult
	End Function
#End Region

	Protected Overrides Sub Render(ByVal writer As HtmlTextWriter)
		If Request.Params("command") = "getAuditLogs" Then
			pnlControls.RenderControl(writer)
		Else
			MyBase.Render(writer)
		End If
	End Sub
End Class
