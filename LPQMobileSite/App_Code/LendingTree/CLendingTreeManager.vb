﻿
Imports System
Imports System.Xml
Imports LPQMobile.Utils
Imports LPQMobile.BO
Namespace LendingTree

	Public Class CLendingTreeManager

		Private Shared _log As log4net.ILog = log4net.LogManager.GetLogger(GetType(CLendingTreeManager))

		''' <summary>
		''' Creates an VL application  given a Lenddintree application
		''' </summary>
		Public Shared Function CreateVL(ByVal pElement As CXmlElement, poConfig As CServiceConfig) As CVehicleLoan

			_log.Info("Creating VL application...")

			'no bar
			'"f63c948cb9fa4614b1eee603dc2ab94b", "9031c7f53eed453b987367e26bd37d43"
			Dim vehicleLoan As New CVehicleLoan(Common.SafeString(poConfig.LPQOrganizationID).Replace("-", ""), Common.SafeString(poConfig.LPQLenderID).Replace("-", ""))
			With vehicleLoan

				Dim TransactionType = pElement.Source.SelectSingleNode("TransactionType").FirstChild.Name
				Dim xmlLoanElement = pElement.SelectSingleElement("TransactionType").SelectSingleElement(TransactionType) '(auto, personal)
				Dim xmlVehicleElement = xmlLoanElement.SelectSingleElement("VehicleInfo")
				Dim LoanType As String = Common.SafeString(xmlLoanElement.GetElementValue("LoanType"))

				.LoanTerm = Common.SafeInteger(LTTermLPQTerm(xmlLoanElement.GetElementValue("Term"))) '1 Year, 2 Years, 3 Years, 
				.VehicleValue = Common.SafeDouble(xmlVehicleElement.GetElementValue("PurchasePrice"))
				.AmountRequested = Common.SafeDouble(xmlLoanElement.GetElementValue("LoanAmount"))

				''##all lenders are required to have "LENDING TREE PURCHASE" or "LENDING TREE REFINANCE" as a purpose in order to use this service
				Select Case LoanType.ToUpper
					Case "REFINANCE VEHICLE", "BUYOUT LEASE"
						.LoanPurpose = "LENDING TREE REFINANCE"
					Case "BUY VEHICLE", "LEASE VEHICLE"
						.LoanPurpose = "LENDING TREE PURCHASE"
					Case Else
						.LoanPurpose = "LENDING TREE PURCHASE"
				End Select

				.VehicleType = "CAR"  ' assume to alway be auto loan

				.KnowVehicleMake = True
				.VehicleMake = IIf(.KnowVehicleMake, Common.SafeString(xmlVehicleElement.GetElementValue("Make")), "")
				.VehicleModel = IIf(.KnowVehicleMake, Common.SafeString(xmlVehicleElement.GetElementValue("Model")), "")
				.VehicleYear = IIf(.KnowVehicleMake, Common.SafeInteger(xmlVehicleElement.GetElementValue("Year")), 0)
				.VehicleVin = IIf(.KnowVehicleMake, Common.SafeString(xmlVehicleElement.GetElementValue("VinNumber")), 0)
				.IsNewVehicle = False
				If Common.SafeString(xmlVehicleElement.GetElementValue("vehicleType")).ToUpper = "NEW" Then
					.IsNewVehicle = True
				End If

				If (xmlVehicleElement.GetElementValue("Mileage") = "") Then
					.VehicleMileage = 0
				Else
					.VehicleMileage = IIf(.IsNewVehicle, 0, Common.SafeString(xmlVehicleElement.GetElementValue("Mileage")))
				End If
				'TD
				'.BranchID = Request.Form("BranchID")
				.ReferralSource = "LendingTree"	'Common.SafeString(Request.Form("ReferralSource"))

				.OtherLoanNumber = pElement.GetElementValue("LoanApplicationID") 'LendingTreeAppID store as reference # in LPQ
			End With



			'aaplicant
			vehicleLoan.Applicants = New List(Of CApplicant)
			Dim hasCoApp As Boolean = False
			If pElement.SelectElements("Borrower").Count > 1 Then hasCoApp = True
			'Dim coAppJoint As Boolean = Request.Form("CoAppJoin") = "Y"
			Dim xmlPriBorrowerElement = pElement.SelectElements("Borrower")(0)
			If hasCoApp Then
				If pElement.SelectElements("Borrower")(0).GetElementValue("IsPrimaryBorrower").ToUpper = "NO" And pElement.SelectElements("Borrower")(1).GetElementValue("IsPrimaryBorrower").ToUpper = "YES" Then
					xmlPriBorrowerElement = pElement.SelectElements("Borrower")(1)	' has to assume this one is primary
				End If
			End If

			Dim applicant As New CApplicant()
			With applicant
				Dim xmlBorrowerElement = xmlPriBorrowerElement
				'	.EmployeeOfLender = Common.SafeString(Request.Form("EmployeeOfLender"))
				.FirstName = Common.SafeString(xmlBorrowerElement.GetElementValue("BorrowerPersonalInformation/Name/FirstName"))
				.MiddleInitial = Common.SafeString(xmlBorrowerElement.GetElementValue("BorrowerPersonalInformation/Name/MiddleName"))
				.LastName = Common.SafeString(xmlBorrowerElement.GetElementValue("BorrowerPersonalInformation/Name/LastName"))
				.Suffix = Common.SafeString(xmlBorrowerElement.GetElementValue("BorrowerPersonalInformation/Name/NameSuffix")).Replace(".", "")
				.SSN = Common.SafeString(xmlBorrowerElement.GetElementValue("BorrowerPersonalInformation/SSN")).Replace("-", "") 'TODO rever of xml post
				.DOB = Common.SafeString(xmlBorrowerElement.GetElementValue("BorrowerPersonalInformation/DateOfBirth"))	   'TODO rever of xml post
				'.MemberNumber = Common.SafeString(Request.Form("MemberNumber"))
				''.MaritalStatus = Common.SafeString(Request.Form("MaritalStatus"))
				.CitizenshipStatus = LTCitizenship2LPQCitizenship(Common.SafeString(xmlBorrowerElement.GetElementValue("BorrowerPersonalInformation/CitizenshipStatus")))  'TODO mapping
				.HomePhone = Common.SafeString(xmlBorrowerElement.GetElementValue("ContactInformation/HomePhone"))
				.WorkPhone = Common.SafeString(xmlBorrowerElement.GetElementValue("ContactInformation/WorkPhone"))
				.WorkPhoneEXT = Common.SafeString(xmlBorrowerElement.GetElementValue("ContactInformation/WorkPhoneExtension"))
				'.CellPhone = Common.SafeString(Request.Form("CellPhone"))
				.Email = Common.SafeString(xmlBorrowerElement.GetElementValue("ContactInformation/Email"))

				Dim xmlBorrowerResidenceElement = xmlBorrowerElement.SelectSingleElement("BorrowerResidence")
				'TODO: may need to use "Current only ResidenceIndicator = xmlBorrowerResidenceElement.GetAttribute("ResidenceIndicator")
				.CurrentAddress = Common.SafeString(xmlBorrowerResidenceElement.GetElementValue("BorrowerResidenceInformation/Address/Street"))
				.CurrentZip = Common.SafeString(xmlBorrowerResidenceElement.GetElementValue("BorrowerResidenceInformation/Address/PostalCode"))
				.CurrentCity = Common.SafeString(xmlBorrowerResidenceElement.GetElementValue("BorrowerResidenceInformation/Address/City"))
				.CurrentState = Common.SafeString(xmlBorrowerResidenceElement.GetElementValue("BorrowerResidenceInformation/Address/State"))
				.OccupancyType = Common.SafeString(xmlBorrowerResidenceElement.GetElementValue("ResidenceType")).ToUpper 'OK

				Dim sPriUnitTimeAtResidence = xmlBorrowerResidenceElement.GetElementValue("UnitTimeAtResidence") ' Years, Months
				.OccupancyDuration = LTDuration2LPQMonth(sPriUnitTimeAtResidence, Common.SafeInteger(xmlBorrowerResidenceElement.GetElementValue("BorrowerTimeAtResidence")))

				.EmploymentStatus = LTEmploymentStatus2LPQEmploymentStatus(Common.SafeString(xmlBorrowerElement.GetElementValue("BorrowerEmployment/EmploymentStatus"))) 'TODO mapping
				' new employment logic
				.txtBusinessType = Common.SafeString(xmlBorrowerElement.GetElementValue("BorrowerEmployment/BusinessTypeDescription"))

				Dim sPriUnitTimeAtJob As String = Common.SafeString(xmlBorrowerElement.GetElementValue("BorrowerEmployment/UnitTimeAtJob"))
				.txtEmployedDuration_month = LTDuration2LPQMonth(sPriUnitTimeAtJob, Common.SafeInteger(xmlBorrowerElement.GetElementValue("BorrowerEmployment/TimeAtJob")))

				.txtEmployer = Common.SafeString(xmlBorrowerElement.GetElementValue("BorrowerEmployment/EmployerName"))
				'.txtEmploymentStartDate = Common.SafeString(Request.Form("txtEmploymentStartDate"))
				'.txtETS = Common.SafeString(Request.Form("txtETS"))
				.txtJobTitle = Common.SafeString(xmlBorrowerElement.GetElementValue("BorrowerEmployment/PositionTitle"))
				.GrossMonthlyIncome = Common.SafeDouble(xmlBorrowerElement.GetElementValue("BorrowerEmployment/CurrentSalary"))	'This is alway Monthly

				'Other income
				Dim nPriMonthlyOtherIncome As Double = Common.SafeDouble(xmlBorrowerElement.GetElementValue("BorrowerOtherIncome/OtherIncomeAmount"))
				If Common.SafeString(xmlBorrowerElement.GetElementValue("BorrowerOtherIncome/OtherIncomeFrequency")).ToUpper = "ANNUAL" Then
					nPriMonthlyOtherIncome = nPriMonthlyOtherIncome / 12
				End If
				.GrossMonthlyIncomeOther = nPriMonthlyOtherIncome
				.MonthlyIncomeOtherDescription = Common.SafeString(xmlBorrowerElement.GetElementValue("BorrowerOtherIncome/IncomeType"))

				'own/rent logic is already take care in the mxml process
				.TotalMonthlyHousingExpense = Common.SafeDouble(xmlBorrowerElement.GetElementValue("Expenses/HouseRent")) + Common.SafeDouble(xmlBorrowerElement.GetElementValue("Expenses/ContinueHousePayment"))


				.HasSpouse = hasCoApp
			End With
			vehicleLoan.Applicants.Add(applicant)

			If hasCoApp Then
				Dim xmlCoBorrowerElement As CXmlElement = pElement.SelectElements("Borrower")(1)
				If pElement.SelectElements("Borrower")(0).GetElementValue("IsPrimaryBorrower").ToUpper = "NO" And pElement.SelectElements("Borrower")(1).GetElementValue("IsPrimaryBorrower").ToUpper = "YES" Then
					xmlCoBorrowerElement = pElement.SelectElements("Borrower")(0)	' has to assume this one is co
				End If
				Dim coApplicant As New CApplicant()
				With coApplicant
					Dim xmlBorrowerElement = xmlCoBorrowerElement
					'	.EmployeeOfLender = Common.SafeString(Request.Form("EmployeeOfLender"))
					.FirstName = Common.SafeString(xmlBorrowerElement.GetElementValue("BorrowerPersonalInformation/Name/FirstName"))
					.MiddleInitial = Common.SafeString(xmlBorrowerElement.GetElementValue("BorrowerPersonalInformation/Name/MiddleName"))
					.LastName = Common.SafeString(xmlBorrowerElement.GetElementValue("BorrowerPersonalInformation/Name/LastName"))
					.Suffix = Common.SafeString(xmlBorrowerElement.GetElementValue("BorrowerPersonalInformation/Name/NameSuffix"))
					.SSN = Common.SafeString(xmlBorrowerElement.GetElementValue("BorrowerPersonalInformation/SSN")).Replace("-", "") 'TODO rever of xml post
					.DOB = Common.SafeString(xmlBorrowerElement.GetElementValue("BorrowerPersonalInformation/DateOfBirth"))	   'OK TODO rever of xml post
					'.MemberNumber = Common.SafeString(Request.Form("MemberNumber"))
					''.MaritalStatus = Common.SafeString(Request.Form("MaritalStatus"))
					.CitizenshipStatus = LTCitizenship2LPQCitizenship(Common.SafeString(xmlBorrowerElement.GetElementValue("BorrowerPersonalInformation/CitizenshipStatus")))	'TODO mapping
					.HomePhone = Common.SafeString(xmlBorrowerElement.GetElementValue("ContactInformation/HomePhone"))
					.WorkPhone = Common.SafeString(xmlBorrowerElement.GetElementValue("ContactInformation/WorkPhone"))
					.WorkPhoneEXT = Common.SafeString(xmlBorrowerElement.GetElementValue("ContactInformation/WorkPhoneExtension"))
					'.CellPhone = Common.SafeString(Request.Form("CellPhone"))
					.Email = Common.SafeString(xmlBorrowerElement.GetElementValue("ContactInformation/Email"))

					Dim xmlBorrowerResidenceElement = xmlBorrowerElement.SelectSingleElement("BorrowerResidence")
					'TODO: may need to use "Current only ResidenceIndicator = xmlBorrowerResidenceElement.GetAttribute("ResidenceIndicator")
					.CurrentAddress = Common.SafeString(xmlBorrowerResidenceElement.GetElementValue("BorrowerResidenceInformation/Address/Street"))
					.CurrentZip = Common.SafeString(xmlBorrowerResidenceElement.GetElementValue("BorrowerResidenceInformation/Address/PostalCode"))
					.CurrentCity = Common.SafeString(xmlBorrowerResidenceElement.GetElementValue("BorrowerResidenceInformation/Address/City"))
					.CurrentState = Common.SafeString(xmlBorrowerResidenceElement.GetElementValue("BorrowerResidenceInformation/Address/State"))
					.OccupancyType = Common.SafeString(xmlBorrowerResidenceElement.GetElementValue("ResidenceType")).ToUpper 'OK

					Dim sCoUnitTimeAtResidence = xmlBorrowerResidenceElement.GetElementValue("UnitTimeAtResidence")	' Years, Months
					.OccupancyDuration = LTDuration2LPQMonth(sCoUnitTimeAtResidence, Common.SafeInteger(xmlBorrowerResidenceElement.GetElementValue("BorrowerTimeAtResidence")))
					'TODO, may not be in month unit
					'UnitTimeAtResidence = xmlBorrowerResidenceElement.GetElementValue("UnitTimeAtResidence")

					.EmploymentStatus = LTEmploymentStatus2LPQEmploymentStatus(Common.SafeString(xmlBorrowerElement.GetElementValue("BorrowerEmployment/EmploymentStatus"))) 'TODO mapping
					' new employment logic
					.txtBusinessType = Common.SafeString(xmlBorrowerElement.GetElementValue("BorrowerEmployment/BusinessTypeDescription"))

					Dim sCoUnitTimeAtJob As String = Common.SafeString(xmlBorrowerElement.GetElementValue("BorrowerEmployment/UnitTimeAtJob"))
					.txtEmployedDuration_month = LTDuration2LPQMonth(sCoUnitTimeAtJob, Common.SafeInteger(xmlBorrowerElement.GetElementValue("BorrowerEmployment/TimeAtJob")))

					.txtEmployer = Common.SafeString(xmlBorrowerElement.GetElementValue("BorrowerEmployment/EmployerName"))
					.txtJobTitle = Common.SafeString(xmlBorrowerElement.GetElementValue("BorrowerEmployment/PositionTitle"))

					.GrossMonthlyIncome = Common.SafeDouble(xmlBorrowerElement.GetElementValue("BorrowerEmployment/CurrentSalary"))

					'Other income
					Dim nCoMonthlyOtherIncome As Double = Common.SafeDouble(xmlBorrowerElement.GetElementValue("BorrowerOtherIncome/OtherIncomeAmount"))
					If Common.SafeString(xmlBorrowerElement.GetElementValue("BorrowerOtherIncome/OtherIncomeFrequency")).ToUpper = "ANNUAL" Then
						nCoMonthlyOtherIncome = nCoMonthlyOtherIncome / 12
					End If
					.GrossMonthlyIncomeOther = nCoMonthlyOtherIncome
					.MonthlyIncomeOtherDescription = Common.SafeString(xmlBorrowerElement.GetElementValue("BorrowerOtherIncome/IncomeType"))

					.TotalMonthlyHousingExpense = Common.SafeDouble(xmlBorrowerElement.GetElementValue("Expenses/HouseRent")) + Common.SafeDouble(xmlBorrowerElement.GetElementValue("Expenses/ContinueHousePayment"))


				End With
				vehicleLoan.Applicants.Add(coApplicant)

			End If
			Return vehicleLoan
		End Function


		Public Shared Function getSubmitLoanData(ByVal poLoan As CBaseLoanApp, ByVal poConfig As CServiceConfig) As XmlDocument

			Dim oDoc As New XmlDocument()
			oDoc.LoadXml("<INPUT />")

			Dim oLogin As XmlElement = CType(oDoc.DocumentElement.AppendChild(oDoc.CreateElement("LOGIN")), XmlElement)
			'still has bar
			oLogin.SetAttribute("api_user_id", poConfig.LPQLogin)
			oLogin.SetAttribute("api_password", poConfig.LPQPW)
			'oLogin.SetAttribute("api_user_id", "d68c2a70-da01-4abf-8ee8-6dd8509a22f6")
			'oLogin.SetAttribute("api_password", "Uk3@GB@GB3BY,ClC3ymMr2J-o#nnFaHoAx9JW^")

			Dim oRequest As XmlElement = CType(oDoc.DocumentElement.AppendChild(oDoc.CreateElement("REQUEST")), XmlElement)
			oRequest.SetAttribute("action_type", "NEW")

			Dim oLoanData As XmlElement = CType(oRequest.AppendChild(oDoc.CreateElement("LOAN_DATA")), XmlElement)
			oLoanData.SetAttribute("loan_type", poLoan.LoanType)

			oLoanData.AppendChild(oDoc.CreateCDataSection(poLoan.GetXml(Nothing).OuterXml()))	'getXML require CWebsiteConfig which is not available in LT context

			Return oDoc
		End Function

#Region "lendingTree value to LPQ value mapping"
		Private Shared Function LTCitizenship2LPQCitizenship(ByVal psLTCitizenShip As String) As String
			Select Case psLTCitizenShip.ToUpper
				Case "US CITIZEN", "US"
					Return "USCITIZEN"
				Case "PERMANENT RESIDENT AlIEN"
					Return "PERMRESIDENT"
				Case "NON-PERMANENT RESIDENT AlIEN"
					Return "NONPERMRESIDENT"
				Case "NON-RESIDENT AlIEN"
					Return "NONRESIDENTALIEN"
				Case Else 'other, UNKNOW 
					Return "USCITIZEN"
			End Select
		End Function

		Private Shared Function LTDuration2LPQMonth(ByVal psUnit As String, ByRef psNumber As Integer) As Integer
			Select Case psUnit.ToUpper
				Case "YEARS"
					Return psNumber * 12
				Case Else 'other, assume
					Return psNumber
			End Select
		End Function


		Private Shared Function LTEmploymentStatus2LPQEmploymentStatus(ByVal psLTEmploymentStatus As String) As String
			Select Case psLTEmploymentStatus.ToUpper
				Case "FULL TIME"
					Return "EMPLOYED"
				Case "PART TIME"
					Return "EMPLOYED"
				Case "SELF-EMPLOYED"
					Return "SE"
				Case "HOMEMAKER"
					Return "HO"
				Case "STUDENT"
					Return "ST"
				Case Else 'other, assume
					Return "OT"
			End Select

		End Function

		'convert year to month
		Private Shared Function LTTermLPQTerm(ByVal psLTTerm As String) As String
			If psLTTerm = "" Then Return 0
			If psLTTerm.IndexOf(" ") > 0 Then
				Dim sTerm = psLTTerm.Substring(0, psLTTerm.IndexOf(" "))
				Return (Common.SafeDouble(sTerm) * 12).ToString
			End If
			Return (Common.SafeDouble(psLTTerm) * 12).ToString

		End Function
#End Region

	End Class
End Namespace