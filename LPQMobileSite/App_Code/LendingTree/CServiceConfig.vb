﻿
Imports System
Imports LPQMobile.Utils
Imports DBUtils
Imports System.Data
Namespace LendingTree
	<Serializable()>
	Public Class CServiceConfig
		Private Shared _log As log4net.ILog = log4net.LogManager.GetLogger(GetType(CServiceConfig))

		Protected LENDINGTREE_CONNECTIONSTRING As String = System.Web.Configuration.WebConfigurationManager.ConnectionStrings("LENDINGTREE_CONNECTIONSTRING").ConnectionString

		Public LendingTreePartnerID As Integer
		Public LendingTreeURL As String
		Public LendingTreeLogin As String
		Public LendingTreePW As String
		Public LendingTreePW_enc As String
		Public LPQOrganizationID As Guid
        Public LPQLenderID As Guid
        Public LPQBranchID As Guid
        Public LPQURL As String
		Public LPQLogin As String
		Public LPQPW As String
		Public LPQPW_enc As String
		Public note As String
		Public LastModifiedBy As String
		Public LastModifiedDate As DateTime
		Public CreatedDate As DateTime

		Public Sub New()
		End Sub

		'pnLendingTreePartnerID is integer per spec
		Public Function GetConfigByLendingTreePartnerID(ByVal pnLendingTreePartnerID As Integer) As CServiceConfig
			Dim oData As DataTable = New DataTable()
			Dim sSQL As String = "Select * From LendingTreeLPQs Where LendingTreePartnerID = " & pnLendingTreePartnerID

			Dim oDB As New CSQLDBUtils(LENDINGTREE_CONNECTIONSTRING)
			Try
				oData = oDB.getDataTable(sSQL)
			Catch ex As Exception
				_log.Error("Error while GetConfigByLendingTreePartnerID", ex)
				Return Nothing
			Finally
				oDB.closeConnection()
			End Try

			Return ConvertConfigDBToObject(oData)
		End Function

		'pnLPQLenderID may have duplicate
		'TODO: pnLPQLenderID need to be uniques within db 
		Public Function GetConfigByLPQLenderID(ByVal pnLPQLenderID As String) As CServiceConfig
			Dim oData As DataTable = New DataTable()
			Dim sSQL As String = "Select * From LendingTreeLPQs Where LPQLenderID = '" & Common.SafeGUID(pnLPQLenderID).ToString & "'"

			Dim oDB As New CSQLDBUtils(LENDINGTREE_CONNECTIONSTRING)
			Try
				oData = oDB.getDataTable(sSQL)
			Catch ex As Exception
				_log.Error("Error while GetConfigByLPQLenderID", ex)
				Return Nothing
			Finally
				oDB.closeConnection()
			End Try

			Return ConvertConfigDBToObject(oData)
		End Function

		Public Function ConvertConfigDBToObject(ByVal oData As DataTable) As CServiceConfig
			Dim oConfig As New CServiceConfig

			If oData IsNot Nothing And oData.Rows.Count > 0 Then
				oConfig.LendingTreePartnerID = Common.SafeString(oData.Rows(0)("LendingTreePartnerID"))
				oConfig.LendingTreeURL = Common.SafeString(oData.Rows(0)("LendingTreeURL"))
				oConfig.LendingTreeLogin = Common.SafeString(oData.Rows(0)("LendingTreeLogin"))
				oConfig.LendingTreePW = Common.SafeString(oData.Rows(0)("LendingTreePW"))
				oConfig.LendingTreePW_enc = Common.SafeString(oData.Rows(0)("LendingTreePW_enc"))
				oConfig.LPQOrganizationID = Common.SafeGUID(oData.Rows(0)("LPQOrganizationID"))
                oConfig.LPQLenderID = Common.SafeGUID(oData.Rows(0)("LPQLenderID"))
                oConfig.LPQBranchID = Common.SafeGUID(oData.Rows(0)("LPQBranchID"))
                oConfig.LPQLogin = Common.SafeString(oData.Rows(0)("LPQLogin"))
				oConfig.LPQPW = Common.SafeString(oData.Rows(0)("LPQPW"))
				oConfig.LPQPW_enc = Common.SafeString(oData.Rows(0)("LPQPW"))
				oConfig.LPQURL = Common.SafeString(oData.Rows(0)("LPQURL"))
				oConfig.note = Common.SafeString(oData.Rows(0)("note"))
				oConfig.LastModifiedDate = Common.SafeString(oData.Rows(0)("LastModifiedDate"))
			End If

			Return oConfig
		End Function

		Public Sub InsertLog(ByVal psLTPartnerID As Integer, ByVal psQFName As String, ByVal psIP As String)
			Dim oInsert As New cSQLInsertStringBuilder("LendingTreeLPQLogs")
			'oInsert.AppendValue("LenderConfigID", New CSQLParamValue(System.Guid.NewGuid))
			oInsert.AppendValue("LendingTreePartnerID ", New CSQLParamValue(psLTPartnerID))
			'oInsert.AppendValue("LPQLenderID", New CSQLParamValue(Common.SafeGUID(config.LenderId)))
			oInsert.AppendValue("QFName", New CSQLParamValue(psQFName))
			oInsert.AppendValue("IPAddress", New CSQLParamValue(psIP))

			Dim oDB As New CSQLDBUtils(LENDINGTREE_CONNECTIONSTRING)
			Try
				oDB.executeNonQuery(oInsert.SQL, False)
			Catch ex As Exception
				_log.Error("Error while  Saving LendingTreeLog", ex)
				'Throw New Exception("Error while  Saving LendingTreeLog", ex)
				Return
			Finally
				oDB.closeConnection()
			End Try

		End Sub

	End Class

End Namespace
