Imports System.Data
Imports System.Data.SqlClient
Imports System.Diagnostics
Imports System.Xml.Serialization
Imports Veracode.Attributes

Namespace DBUtils    
    ''' <summary>
    ''' This class provides for more convenient access into the SQL Server.  
    ''' Sample Proper use that avoids connection leaks:
    ''' 
    ''' Using oDB As New CSQLDBUtils(std.CONNECTIONSTRING)
    ''' Dim s As String = oDB.getScalerValue("SELECT top 10 * From Loans")
    ''' End Using
    ''' 
    ''' </summary>
    ''' <remarks>
    ''' * when working w/ transactions, there is a potential deadlock (exception type  'System.Data.SqlClient.SqlException')
    ''' if we try to retrieve a field value, in a separate connection, from a row that's already being modified in an active transaction,
    ''' in a transacted connection.  The work around is if we detect a current transaction, we use that transacted connection
    ''' for all queries.
    ''' * also, we dont really need to worrry about opening a connection to retrieve data, in client 
    ''' code we should worry more about closing the connection asap so that it can be pooled
    ''' </remarks>
    Public Class CSQLDBUtils
        Implements IDBUtils
        Implements IDisposable

		Protected Shared Log As log4net.ILog = log4net.LogManager.GetLogger(GetType(CSQLDBUtils))

        Public EnableCacheMetaData As Boolean 'keeps a copy of the cached data columns SELECT statements in shared variable for efficiency reasons

		Private Shared _DefaultIsTransactionViolationFatal As Boolean = ("Y" = Safestring(System.Configuration.ConfigurationManager.AppSettings.Get("IS_TRANSACTION_VIOLATION_FATAL")))
		Private _IsTransactionViolationFatal As Nullable(Of Boolean) = Nothing
		''' <summary>
		''' Governs whether attempting to execute a non-transacted UpdateDataTable or executeNonQuery while the object has a
		''' transaction should throw an exception, or only log an error.
		''' </summary>
		''' <remarks>There may be times when we don't want to enlist secondary queries in a transaction for performance reasons.  If that is the case, simply use a different instance of CDBUtils.</remarks>
		Public Property IsTransactionViolationFatal() As Boolean
			Get
				If _IsTransactionViolationFatal.HasValue Then
					Return _IsTransactionViolationFatal.Value					
				Else
					Return _DefaultIsTransactionViolationFatal
				End If
			End Get
			Set(ByVal value As Boolean)
				_IsTransactionViolationFatal = value
			End Set
		End Property

#Region "Diagnostic / Profiling Routines"
		''' <summary>
		''' Note this is a low level configuration to maximize performance by turning off all tracing (no logs nor stopwatch is internally created).   
		''' If not an emergency it's recommended to control what logs to capture via the log4net config file.
		''' </summary>		
		''' <remarks>
		''' Having this information is useful for identifying unnecessary system bottlenecks.
		''' </remarks>
		Private Shared EnableSQLProfile As Boolean = System.Configuration.ConfigurationManager.AppSettings.Get("DBUTILS.ENABLE_PROFILE") <> "N"
		Private _StopWatch As New System.Diagnostics.Stopwatch
		Private Sub startTimer()
			If EnableSQLProfile = False Then
				Return
			End If

			_StopWatch.Reset()
			_StopWatch.Start()
		End Sub
		''' <summary>
		''' Ends stopwatch in progress logs summary information.  This should be called after startTimer
		''' </summary>
		''' <param name="psPrefix"></param>
		''' <param name="sql"></param>
		''' <remarks></remarks>
		Private Sub endTimer(ByVal psPrefix As String, ByVal sql As String)
			If EnableSQLProfile = False Then
				Return
			End If

			Dim nSec As Double = _StopWatch.Elapsed().TotalSeconds
			_StopWatch.Stop()
            Log.Debug(FormatNumber(nSec, 3) & If(nSec > 1, "!!", "") & " s: [" & psPrefix & "] " & CSecureStringFormatter.MaskSensitiveData(sql))

        End Sub

#End Region


        Private _connectionString As String
        Public Sub New(ByVal sConnection As String)
            _connectionString = sConnection
        End Sub

        Private _Conn As SqlConnection
        Private _ConnTransacted As SqlConnection
        Private _sharedTransaction As SqlTransaction 'associated w/ _ConnTransacted

        'one of the problems with filling datatables is that dataset table has n fields defined and the db has n+1 fields defined, then when we fill the datatable using SELECT * and try to update, it will fail!
        'key of hashtable = table name 
        'Public Shared SelectCache As New Hashtable()

        Private _CommandTimeout As Integer = 90
        Public Property CommandTimeout() As Integer
            Get
                Return _CommandTimeout
            End Get
            Set(ByVal Value As Integer)
                _CommandTimeout = Value
            End Set
        End Property

        Public Sub closeConnection() Implements IDBUtils.closeConnection
            If IsNothing(_lastDataReader) = False AndAlso _lastDataReader.IsClosed = False Then
                _lastDataReader.Close()
            End If
            If IsNothing(_Conn) = False AndAlso _Conn.State <> ConnectionState.Closed Then
                _Conn.Close()
            End If
            _Conn = Nothing

            If False = IsNothing(_ConnTransacted) AndAlso _ConnTransacted.State <> ConnectionState.Closed Then
                _sharedTransaction = Nothing
                _ConnTransacted.Close()
            End If
            _ConnTransacted = Nothing
        End Sub

        Public Function openConnection() As System.Data.IDbConnection Implements IDBUtils.openConnection
            If IsNothing(_Conn) Then
                _Conn = New SqlConnection(_connectionString)
                _Conn.Open()
            End If
            Return _Conn
        End Function

        Public Function openConnectionTransacted() As System.Data.IDbConnection Implements IDBUtils.openConnectionTransacted
            If IsNothing(_ConnTransacted) Then
                _ConnTransacted = New SqlConnection(_connectionString)
                _ConnTransacted.Open()
            End If

            'ensure there is a valid transaction since the transaction might be invalidated after commit/rollback
            If IsNothing(_sharedTransaction) Then
                _sharedTransaction = _ConnTransacted.BeginTransaction
            ElseIf IsNothing(_sharedTransaction.Connection) Then
                _sharedTransaction = _ConnTransacted.BeginTransaction
            End If

            Return _ConnTransacted
        End Function

        'if there is no current transaction then that means no sql was executed or the connection was never opened -- 
        Public Sub commitTransaction() Implements IDBUtils.commitTransaction
            If IsNothing(_sharedTransaction) Then
                Throw New ApplicationException("No transaction to commit.  Please check that transacted connection was opened or there was some sql executed against a transacted connection!")
            End If
            _sharedTransaction.Commit()
            _sharedTransaction = Nothing 'turn off transaction 
        End Sub

		Public Sub rollbackTransaction() Implements IDBUtils.rollbackTransaction
			If IsNothing(_sharedTransaction) Then
				Throw New ApplicationException("No transaction to commit.  Please check that transacted connection was opened or there was some sql executed against a transacted connection!")
			End If
			_sharedTransaction.Rollback()
			_sharedTransaction = Nothing 'turn off transaction 
		End Sub

		''' <summary>
		''' Calls rollbackTransaction, but instead of throwing an error if there isn't one open, just does nothing.
		''' </summary>
		''' <remarks>
		''' Good for error handling situations where we want to roll back if any transaction is open, and don't really know if that's the case.
		''' </remarks>
		Public Sub rollbackTransactionIfOpen() Implements IDBUtils.rollbackTransactionIfOpen
			If IsNothing(_sharedTransaction) Then
				Return
			End If

			rollbackTransaction()
		End Sub

		''' <summary>
		''' Calls commitTransaction, but instead of throwing an error if there isn't one open, just does nothing.
		''' </summary>
		Public Sub commitTransactionIfOpen() Implements IDBUtils.commitTransactionIfOpen
			If IsNothing(_sharedTransaction) Then
				Return
			End If

			commitTransaction()
		End Sub


        Private Function isTransactionActive() As Boolean
            If IsNothing(_sharedTransaction) Then
                Return False
            ElseIf IsNothing(_sharedTransaction.Connection) Then
                Return False
            End If

            Return True
        End Function

		''' <summary>
		''' Executes an sql statement straight to our connection. 
		''' </summary>
		''' <param name="sql">SQL string that will be run.  Highly recommended you use the CSQLStringBuilders to generate this string instead of creating manually by hand.</param>
		''' <param name="isTransacted"></param>
		Public Function executeNonQuery(ByVal sql As String, Optional ByVal isTransacted As Boolean = False) As Integer Implements IDBUtils.executeNonQuery
			sql = UnescapeSQLBraces(sql)
			Dim cmd As SqlCommand
			If isTransacted Then
				cmd = New SqlCommand(sql, DirectCast(openConnectionTransacted(), SqlConnection))
				cmd.Transaction = _sharedTransaction
			Else
				If True = isTransactionActive() Then
					Dim oException As New CSQLException("Executing non-transacted non-query SQL command through a CSQLDBUtils object in transacted mode. This may cause data consistency errors and deadlocks.", sql)
					If True = IsTransactionViolationFatal Then
						Throw oException
					Else
						Log.Info(oException.ToString, oException)
					End If
				End If
				cmd = New SqlCommand(sql, DirectCast(openConnection(), SqlConnection))
			End If


			'Trace.WriteLine("ExecNonQuery:" & sql, "SQL")
			startTimer()
			Try
				cmd.CommandTimeout = CommandTimeout
				Return cmd.ExecuteNonQuery()
			Catch sqlEx As SqlClient.SqlException
				Trace.WriteLine("SQL Syntax Error:" & sql & vbCrLf & sqlEx.ToString(), "ERR")
				Throw New CSQLException(sqlEx, sql & getDeadlockSummary(sqlEx))
			Finally
				endTimer("ExecNonQuery", sql)
			End Try

		End Function

        'last datareader sql that was executed, saved for debuggin purposes
        Private _LastDataReaderSQL As String
        Private _lastDataReader As IDataReader
        Public Function getDataReader(ByVal sql As String) As System.Data.IDataReader Implements IDBUtils.getDataReader
			sql = UnescapeSQLBraces(sql)
			checkDataReaderStillOpen()

            Dim cmd As SqlCommand
            If isTransactionActive() Then
				cmd = New SqlCommand(sql, DirectCast(openConnectionTransacted(), SqlConnection))
                cmd.Transaction = _sharedTransaction
            Else
				cmd = New SqlCommand(sql, DirectCast(openConnection(), SqlConnection))
            End If

			'Trace.WriteLine("GetDataReader:" & sql, "SQL")
			startTimer()
            Try
                cmd.CommandTimeout = CommandTimeout
                _lastDataReader = cmd.ExecuteReader
                _LastDataReaderSQL = sql
            Catch sqlEx As SqlClient.SqlException
				Trace.WriteLine("SQL Syntax Error:" & sql & vbCrLf & sqlEx.ToString(), "ERR")
				Throw New CSQLException(sqlEx, sql & getDeadlockSummary(sqlEx))
			Finally
				endTimer("GetDataReader", sql)
			End Try

            Return _lastDataReader
		End Function

		''' <summary>
		''' Convenience function: Equivalent to calling getDataReader(CSQLParamString.Format(sqlFormat, poValues)) 
		''' </summary>
		Public Function getDataReader(ByVal sqlFormat As String, ByVal ParamArray poValues() As CSQLParamValue) As System.Data.IDataReader
			Return getDataReader(CSQLParamString.Format(sqlFormat, poValues))
		End Function

        Public Function getDataTable(ByVal sql As String) As System.Data.DataTable Implements IDBUtils.getDataTable
			sql = UnescapeSQLBraces(sql)
			checkDataReaderStillOpen()
            Dim ds As New DataSet()
            Dim da As SqlDataAdapter
            If isTransactionActive() Then
				da = New SqlDataAdapter(sql, DirectCast(openConnectionTransacted(), SqlConnection))
                da.SelectCommand.Transaction = _sharedTransaction
            Else
				da = New SqlDataAdapter(sql, DirectCast(openConnection(), SqlConnection))
            End If

			'Trace.WriteLine("GetDataTable:" & sql, "SQL")
			startTimer()

            Try
                da.SelectCommand.CommandTimeout = CommandTimeout
                da.Fill(ds)
            Catch sqlEx As SqlClient.SqlException
                Trace.WriteLine("SQL Syntax Error:" & sql & vbCrLf & sqlEx.ToString(), "ERR")
				Throw New CSQLException(sqlEx, sql & getDeadlockSummary(sqlEx))
			Finally
				endTimer("GetDataTable", sql)
			End Try

			Return ds.Tables(0)
        End Function
		''' <summary>
		''' Convenience function: Equivalent to calling getDataTable(CSQLParamString.Format(sqlFormat, poValues)) 
		''' </summary>
		Public Function getDataTable(ByVal sqlFormat As String, ByVal ParamArray poValues() As CSQLParamValue) As System.Data.DataTable
			Return getDataTable(CSQLParamString.Format(sqlFormat, poValues))
		End Function

        Public Function getScalerValue(ByVal sql As String) As String Implements IDBUtils.getScalerValue
			sql = UnescapeSQLBraces(sql)
			checkDataReaderStillOpen()
            Dim cmd As SqlCommand
            If isTransactionActive() Then
				cmd = New SqlCommand(sql, DirectCast(openConnectionTransacted(), SqlConnection))
                cmd.Transaction = _sharedTransaction
            Else
				cmd = New SqlCommand(sql, DirectCast(openConnection(), SqlConnection))
            End If

			'Trace.WriteLine("GetScalerValue:" & sql, "SQL")
			startTimer()

            Try
                cmd.CommandTimeout = CommandTimeout
                'Dim execResult As Object = cmd.ExecuteScalar

                Return Safestring(cmd.ExecuteScalar)
            Catch sqlEx As SqlClient.SqlException
                Trace.WriteLine("SQL Syntax Error:" & sql & vbCrLf & sqlEx.ToString(), "ERR")
				Throw New CSQLException(sqlEx, sql & getDeadlockSummary(sqlEx))
			Finally
				endTimer("GetScalerValue", sql)
			End Try
		End Function

		''' <summary>
		''' Convenience function: Equivalent to calling getScalerValue(CSQLParamString.Format(sqlFormat, poValues)) 
		''' </summary>
		Public Function getScalerValue(ByVal sqlFormat As String, ByVal ParamArray poValues() As CSQLParamValue) As String
			Return getScalerValue(CSQLParamString.Format(sqlFormat, poValues))
		End Function

		Public Sub bulkCopy(ByVal pDT As DataTable, ByVal pDestinationTableName As String, ByVal isTransacted As Boolean, Optional ByVal pSqlBulkCopyOptions As SqlBulkCopyOptions = SqlBulkCopyOptions.Default)
			Dim bulkCopy As SqlClient.SqlBulkCopy = Nothing
			If isTransacted Then
				bulkCopy = New SqlClient.SqlBulkCopy(DirectCast(openConnectionTransacted(), SqlConnection), pSqlBulkCopyOptions, _sharedTransaction)
			Else
				If True = isTransactionActive() Then
					Dim oException As New CSQLException("Executing non-transacted bulk copy through a CSQLDBUtils object in transacted mode. This may cause data consistency errors and deadlocks.")
					If True = IsTransactionViolationFatal Then
						Throw oException
					Else
						Log.Info(oException.ToString, oException)
					End If
				End If
				bulkCopy = New SqlClient.SqlBulkCopy(DirectCast(openConnection(), SqlConnection))
			End If
			Using bulkCopy
				bulkCopy.BatchSize = 10000
				bulkCopy.DestinationTableName = pDestinationTableName
				bulkCopy.WriteToServer(pDT)
			End Using
		End Sub



#Region "String Manip Routines"
        ''' <summary>
        '''READ UNCOMMITTED - Implements dirty read, or isolation level 0 locking, which means that no 
        ''' shared locks are issued and no exclusive locks are honored. When this option is set, it 
        ''' is possible to read uncommitted or dirty data; values in the data can be changed and rows 
        ''' can appear or disappear in the data set before the end of the transaction. This option 
        ''' has the same effect as setting NOLOCK on all tables in all SELECT statements in a 
        ''' transaction. This is the least restrictive of the four isolation levels.
		''' </summary>
		''' <remarks>
		''' Please note that the results from here can be LESS than expected.  Please see this article for actual example scenarios and how to do a quick test:
		''' http://sqlblogcasts.com/blogs/tonyrogerson/archive/2006/11/10/1280.aspx
		''' A future change will be to use snapshot isolation levels: 
		''' http://www.informit.com/articles/article.aspx?p=327394&amp;seqNum=2
		''' http://msdn.microsoft.com/en-us/library/ms175492.aspx
		''' </remarks>
        Public Shared Function SQLReadUncommitted(ByVal psSQL As String) As String
            Dim sResult As String = "SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED; " & psSQL & " ; SET TRANSACTION ISOLATION LEVEL READ COMMITTED;"
            Return sResult
        End Function
        Public Overloads Shared Function Safestring(ByVal oNull As DBNull) As String
            Return ""
        End Function

        Public Overloads Shared Function SafeString(ByVal s As Object) As String
            ' special case to handle Recordset Field objects
            If s Is Nothing OrElse TypeOf s Is DBNull Then
                Return ""
            ElseIf TypeOf s Is Guid Then
                'we really don't want to add too many else clauses here, but this GUID check seems like it will keep our code base
                'more maintainable since it will help our code avoid having to know the format of the GUID.  We read from db as-is, we write as-is.
                Return s.ToString()
            Else
                Return Trim(CStr(s)) 'very odd: if u return trim(s.toString()), then for dates, it will append 12:00:00 AM if there is no time :-T
            End If
        End Function

        Public Overloads Shared Function SQLString(ByVal s As Object) As String
            Return "'" & Safestring(s).Replace("'", "''") & "'"
        End Function

        'only used when fine control is needed  over how a string should be escaped w/ or w/o trimming
        'avoid using if possible
        Public Overloads Shared Function SQLString(ByVal s As String, ByVal pbTrim As Boolean) As String
            If s = "" Then
                Return "''"
            End If
            If pbTrim Then
                Return "'" & s.Trim().Replace("'", "''") & "'"
            Else
                Return "'" & s.Replace("'", "''") & "'"
            End If
        End Function
#End Region

        Private Sub checkDataReaderStillOpen()
            If False = IsNothing(_lastDataReader) Then
                If _lastDataReader.IsClosed = False Then
                    Throw New InvalidOperationException("There is already a data reader open w/ the sql:" & _LastDataReaderSQL)
                End If
            End If
        End Sub

        Private Shared _cachedSelects As New Hashtable()

        'the parameters for the select string are from table property so it can't contain bad syntax
        <SqlQueryCleanser>
        Private Function buildSelect(ByVal poDataTable As DataTable) As String
            Dim sqlFields As New System.Text.StringBuilder()
            sqlFields.Append("SELECT ")
            Dim i As Integer
            For i = 0 To poDataTable.Columns.Count - 2
                Dim oCol As DataColumn = poDataTable.Columns(i)
                sqlFields.Append("[" & oCol.ColumnName & "], ")
            Next
            sqlFields.Append("[" & poDataTable.Columns(poDataTable.Columns.Count - 1).ColumnName & "]") 'handle last column 
            sqlFields.Append(" FROM " & poDataTable.TableName & " ")

            Return sqlFields.ToString
        End Function




        Public Overloads Sub FillDataTable(ByVal dt As DataTable, ByVal poWhereClauseWithWhere As CSQLWhereStringBuilder, Optional ByVal psOrderClause As String = "")
            If dt.Columns.Count <= 0 Then
                Throw New ArgumentException("Datatable passed in must already have columns defined!  Otherwise, please use other FillDataTable overload.")
            End If

            Dim sqlFields As New System.Text.StringBuilder()

            If Me.EnableCacheMetaData Then
                If False = _cachedSelects.ContainsKey(dt.TableName) Then
                    SyncLock (_cachedSelects.SyncRoot)
                        _cachedSelects(dt.TableName) = buildSelect(dt)
                    End SyncLock
                End If

                sqlFields.Append(CStr(_cachedSelects(dt.TableName)))
            Else
                sqlFields.Append(buildSelect(dt))
            End If


            If False = IsNothing(poWhereClauseWithWhere) Then
                sqlFields.Append(poWhereClauseWithWhere.SQL)
            End If
            sqlFields.Append(" " & psOrderClause)

            Trace.WriteLine("Dynamic SQL for Fill: " & sqlFields.ToString)

            Try
                FillDataTable(dt, sqlFields.ToString)
            Catch sqlEx As SqlException
                Throw New CSQLException(sqlEx, sqlFields.ToString & getDeadlockSummary(sqlEx))
            End Try
        End Sub

        Public Overloads Sub FillDataTable(ByVal dt As DataTable, ByVal sqlSelect As String) Implements IDBUtils.FillDataTable
            Dim oConn As SqlConnection
            If isTransactionActive() Then
				oConn = DirectCast(openConnectionTransacted(), SqlConnection)
            Else
				oConn = DirectCast(openConnection(), SqlConnection)
            End If

			startTimer()

            Dim oAdapter As New SqlDataAdapter(sqlSelect, oConn)
            oAdapter.SelectCommand.Transaction = _sharedTransaction
            oAdapter.SelectCommand.CommandTimeout = Me.CommandTimeout
            Try
                oAdapter.Fill(dt)
            Catch sqlEx As SqlException
				Throw New CSQLException(sqlEx, sqlSelect & getDeadlockSummary(sqlEx))
			Finally
				endTimer("FillDataTable", sqlSelect)
			End Try
        End Sub

        'note: the command builder generates code that will acocunt for db concurrency violations.  if no row is updated, that means the row has been
        'modified and a dbexception will be thrown
        'refer to this for more info on concurrency: http://msdn.microsoft.com/msdnmag/issues/03/04/DataConcurrency/default.aspx
        Public Overloads Function UpdateDataTable(ByVal dt As DataTable, ByVal sqlSelectMetaData As String, ByVal isTransacted As Boolean) As Integer Implements IDBUtils.UpdateDataTable
            Dim oConn As SqlConnection
            If isTransacted Then
				oConn = DirectCast(openConnectionTransacted(), SqlConnection)
			Else
				If True = isTransactionActive() Then
					Dim oException As New CSQLException("Executing non-transacted non-query SQL command through a CSQLDBUtils object in transacted mode. This may cause data consistency errors and deadlocks.", "Select Metadata: " & sqlSelectMetaData)
					If True = IsTransactionViolationFatal Then
						Throw oException
					Else
						Log.Info(oException.ToString, oException)
					End If
				End If
				oConn = DirectCast(openConnection(), SqlConnection)
            End If

            Dim oAdapter As New SqlDataAdapter(sqlSelectMetaData, oConn)
            Dim cm As New SqlCommandBuilder(oAdapter)
            cm.QuotePrefix = "["
            cm.QuoteSuffix = "]"
            If isTransactionActive() Then
                'this must be done prior to update so that the command builder will auto enlist the other command objects into the transaction!
                '    'REF: http://groups.google.com/groups?hl=en&lr=&ie=UTF-8&oe=UTF-8&threadm=olb8du423jcslrjp78m1p09lufhktncagd%404ax.com&rnum=5&prev=/groups%3Fhl%3Den%26lr%3D%26ie%3DUTF-8%26oe%3DUTF-8%26q%3Dcommandbuilder%2Btransaction%2B%2522Execute%2Brequires%2Bthe%2Bcommand%2Bto%2Bhave%2Ba%2Btransaction%2Bobject%2Bwhen%2Bthe%2Bconnection%2Bassigned%2Bto%2Bthe%2Bcommand%2Bis%2Bin%2Ba%2Bpending%2Blocal%2Btransaction.%2B%2BThe%2BTransaction%2Bproperty%2Bof%2Bthe%2Bcommand%2Bhas%2Bnot%2Bbeen%2Binitialized.%2522
                oAdapter.SelectCommand.Transaction = _sharedTransaction
            End If
            Try
                oAdapter.InsertCommand = cm.GetInsertCommand
                oAdapter.UpdateCommand = cm.GetUpdateCommand
                oAdapter.DeleteCommand = cm.GetDeleteCommand

                oAdapter.InsertCommand.CommandTimeout = Me.CommandTimeout
                oAdapter.UpdateCommand.CommandTimeout = Me.CommandTimeout
                oAdapter.DeleteCommand.CommandTimeout = Me.CommandTimeout
            Catch sqlEx As SqlException
                Throw New CSQLException(sqlEx.Message, sqlEx)
            End Try

            'retrieve the update/insert commands so that we can change their timeouts.  Once that's done, the 
			Dim nAffectedCount As Integer
			startTimer()

            Try
				nAffectedCount = oAdapter.Update(dt)
			Catch dbErr As DBConcurrencyException
				Throw dbErr
			Catch sqlEx As SqlException
				Dim maxLengthError As New System.Text.StringBuilder
				'First, check if any errrors were caused by String values that are longer than the column size
				'Fill the schema, and then check each column of each row
				Try
					Dim schemaDt As DataTable = New DataTable
					oAdapter.FillSchema(schemaDt, SchemaType.Source)
					For Each row As DataRow In dt.Rows
						'Unchanged rows should be fine since they were that size when pulled from the database
						'We might not be able to access the row fields of deleted columns
						If row.RowState = DataRowState.Deleted OrElse row.RowState = DataRowState.Unchanged Then
							Continue For
						End If
						For Each column As DataColumn In schemaDt.Columns
							If column.DataType.Equals(GetType(String)) AndAlso column.MaxLength > -1 Then
								If Safestring(row(column.ColumnName)).Length > column.MaxLength Then
									maxLengthError.AppendFormat("Unable to update row in datatable ({0}). The value would be truncated." & vbCrLf & "Column: {1} ({4})" & vbCrLf & "Value: {2} ({3})" & vbCrLf, dt.TableName, column.ColumnName, row(column.ColumnName).ToString, row(column.ColumnName).ToString.Length, column.MaxLength)
								End If
							End If
						Next
					Next
				Catch err As Exception
					'If this section throw an exception, it's probably a coding error that needs to be corrected.
					'On Demo/Production, we'll get a warn/info before the following section throws a CSQLException
					'TODO: CAssert.FailGracefully(String.Format("Error occurred while comparing current row values against schema max length with message: {0}. Inner Exception: {1}", err.Message, sqlEx.Message))
				End Try
				If maxLengthError.Length > 0 Then
					Throw New CSQLException(maxLengthError.ToString, sqlEx)
				End If

				If dt.Rows.Count = 1 Then
					Dim sRowString As String
					Try
						sRowString = getRowTostring(dt.Rows(0))
					Catch ex As Exception
						'Don't want a failure prettying up the error to shadow the real problem...
						'This is most likely to happen if there are deleted rows in the update, since you can't get
						'information from them. 
						sRowString = "--Can't access row information--"
					End Try
					Throw New CSQLException("Unable to update row in datatable (" & dt.TableName & ") :" & sRowString & vbCrLf & " Error Message:" & sqlEx.Message, sqlEx)
				Else
					Dim sError As New System.Text.StringBuilder()
					sError.Append("Unable to update row in datatable (" & dt.TableName & ") :")
					Dim oRow As DataRow
					For Each oRow In dt.Rows
						Try
							sError.Append(getRowTostring(oRow) & vbCrLf)
						Catch ex As Exception
							'Don't want a failure prettying up the error to shadow the real problem...
							'This is most likely to happen if there are deleted rows in the update, since you can't get
							'information from them. 
							sError.Append("--Can't access row information--" & vbCrLf)
						End Try
					Next
					sError.Append("SQL Error Message: " & sqlEx.Message)

					Throw New CSQLException(sError.ToString, sqlEx)
				End If
			Finally
				endTimer("UpdateDataTable", sqlSelectMetaData)
			End Try
            Return nAffectedCount
        End Function

        'safe method to update datatable when the db may have more fields than the datatable we are trying to update
        'otherwise you will get invalid operation/missing datacolumn errors
		Public Overloads Function UpdateDataTable(ByVal dt As DataTable, ByVal isTransacted As Boolean) As Integer Implements IDBUtils.UpdateDataTable
			If dt.Columns.Count <= 0 Then
				Throw New ArgumentException("Datatable passed in must already have columns defined!  Otherwise, please use other FillDataTable overload.")
			End If

			Dim sqlFields As New System.Text.StringBuilder()

			If Me.EnableCacheMetaData Then
				If False = _cachedSelects.ContainsKey(dt.TableName) Then
					SyncLock (_cachedSelects.SyncRoot)
						_cachedSelects(dt.TableName) = buildSelect(dt)
					End SyncLock
				End If

				sqlFields.Append(CStr(_cachedSelects(dt.TableName)))
			Else
				sqlFields.Append(buildSelect(dt))
			End If

			Return UpdateDataTable(dt, sqlFields.ToString, isTransacted)
		End Function

        Private Function getRowTostring(ByVal oRow As DataRow) As String
            Dim sResult As String = ""
            Dim tbl As DataTable = oRow.Table
            Dim col As DataColumn
            For Each col In tbl.Columns
                Trace.WriteLine("type: " & col.ColumnName & ": " & col.DataType.ToString)

				Dim oVal As Object = oRow(col.ColumnName)
				If oRow(col.ColumnName) Is Nothing OrElse TypeOf oRow(col.ColumnName) Is DBNull Then
					sResult += col.ColumnName & "=NULL,"
				Else
					'handle conversion 
					If col.DataType.Equals(GetType(String)) Then
						sResult += col.ColumnName & "=" & SQLString(Safestring(oRow(col.ColumnName))) & ","
					ElseIf col.DataType.Equals(GetType(Date)) Then
						sResult += col.ColumnName & "=" & SQLString(Safestring(oRow(col.ColumnName))) & ","
					ElseIf col.DataType.Equals(GetType(Byte())) Then						
						sResult += col.ColumnName & "=" & VarBinaryLiteral(CType(oRow(col.ColumnName), Byte())) & ","
					Else
						sResult += col.ColumnName & "=" & Safestring(oRow(col.ColumnName)) & ", "
					End If
				End If

				

			Next
            If sResult <> "" Then
                sResult = sResult.TrimEnd(","c)
                sResult += " [Where SafeGuardClauseHere]" 'append this so we don't accidently screw things up when doing Update XXX Set .... 
            End If
            Return sResult
        End Function

		Protected Function ByteConvert(ByVal input As Byte) As String
			Return input.ToString("X2")
		End Function
		Protected Function VarBinaryLiteral(ByVal input As Byte()) As String
			Return "0x" & String.Join("", Array.ConvertAll(Of Byte, String)(input, AddressOf ByteConvert))
		End Function


		Private Function getDeadlockSummary(ByVal ex As SqlException) As String
			Dim sMsg As String = ex.Message.ToUpper
			Dim bTraceAll As Boolean = True
			If sMsg.IndexOf("DEADLOCKED") <> -1 Then
				'green to go forward!
			ElseIf sMsg.IndexOf("TIMEOUT EXPIRED") <> -1 Then 'sMsg.IndexOf("DEADLOCKED") = -1 And 
				'continue and show ENTIRE activity summary;  unfortunately, if we're timed out, the activity will not count show correct locking process... 
			Else
				Return ""
			End If

			Dim dt As DataTable
			Try
				'we want to use a diff isolated connection/transaction to avoid MSDTC issues w/ any current transactions.
				Using tx As New Transactions.TransactionScope(Transactions.TransactionScopeOption.RequiresNew)
					Using cn As New SqlConnection(_connectionString)
						Dim sqlDeadlockSummary As String = String.Format("exec {0}.dbo.GetDeadlockSummary", CSQLConnectionString.CONNECTION_DBName)

						Dim ds As New DataSet()
						Dim da As New SqlDataAdapter(sqlDeadlockSummary, cn)
						da.SelectCommand.CommandTimeout = 30
						da.Fill(ds)

						dt = ds.Tables(0)
					End Using
				End Using

				Dim rowsOfInterest As New System.Collections.Generic.List(Of DataRow)

				Dim dr As DataRow
				Dim sBuffer As New System.Text.StringBuilder()
				Dim nCount As Integer
				If bTraceAll Then
					rowsOfInterest.AddRange(dt.Select())
					nCount = rowsOfInterest.Count
				Else
					For Each dr In dt.Select("BlockingSPID >  0 ")
						rowsOfInterest.Add(dr)
						nCount += 1
						rowsOfInterest.AddRange(dt.Select(String.Format("UserSPID={0}", dr("BlockingSPID"))))
					Next
				End If

				Dim dtRowsOfInterest As New DataTable
				Dim tmp As New System.Collections.Generic.List(Of DataRow)
				tmp.AddRange(dt.Select())
				For Each r As DataRow In tmp
					If rowsOfInterest.Contains(r) = False Then
						dt.Rows.Remove(r)
					End If
				Next

				Dim sb As New System.Text.StringBuilder()

				'print header 
				sb.AppendLine()
				sb.AppendLine("<TABLE border=""1""> <TR>")

				For i As Integer = 0 To dt.Columns.Count - 1
					sb.AppendLine("<TH>" & dt.Columns(i).ColumnName & "</TH>")
				Next
				sb.AppendLine("</TR>")

				'print rows 
				Dim bAlternate As Boolean
				For Each drFinal As DataRow In dt.Rows
					bAlternate = Not bAlternate
					sb.AppendLine(String.Format("<TR {0}>", If(bAlternate, "bgcolor=""#eaeaea""", "")))
					For i As Integer = 0 To dt.Columns.Count - 1
						sb.AppendLine("<TD>" & Safestring(drFinal.Item(i)) & "</TD>")
					Next
					sb.AppendLine("</TR>")
				Next

				sb.AppendLine("</TABLE>")

				'this is useful in case of unit testing whereby nUnit will not call ToString and we need to look at logs to see this info...
				Log.Debug("Deadlock Summary Table : " & sb.ToString)

				Return vbCrLf & " Deadlock summary ( " & nCount & " are being blocked " & ") : " & sb.ToString

			Catch exDead As System.Exception
				Log.Debug("Unable to retrieve deadlock info: " & exDead.ToString)
				Return vbCrLf & "Unable to obtain deadlock info: " & exDead.Message
			End Try

		End Function

		Private Function getDeadlockSummaryOld(ByVal ex As SqlException) As String
			Dim sMsg As String = ex.Message.ToUpper
			If sMsg.IndexOf("TIMEOUT EXPIRED") = -1 Then 'sMsg.IndexOf("DEADLOCKED") = -1 And 
				Return ""
			End If

			Dim dt As DataTable
			Try
				dt = getDataTable("exec sp_lock")
			Catch exDead As System.Exception
				Debug.Write("Unable to retrieve deadlock info: " & exDead.ToString)
				Return vbCrLf & "Unable to obtain deadlock info."
			End Try

			Dim dr As DataRow
			Dim sBuffer As New System.Text.StringBuilder()
			Dim nCount As Integer
			For Each dr In dt.Rows
				If Safestring(dr("Mode")) = "X" Then 'exclusive lock 
					nCount += 1
					Dim sObjName As String = getScalerValue("Select Name From sysObjects where id=" & Safestring(dr("objID")))
					Dim sPID As String = Safestring(dr("SPID"))
					Dim sBuff As String
					Try
						Dim dtBuff As DataTable = getDataTable(String.Format("dbcc inputbuffer ({0})", sPID))
						sBuff = Safestring(dtBuff.Rows(0)("EventInfo"))
					Catch exBuff As System.Exception
						sBuff = "Unable to retrieve buffer: " & exBuff.ToString
					End Try

					sBuffer.AppendFormat("SPID: {0} Object:{1} SQL:{2}" & vbCrLf, sPID, sObjName, sBuff)

				End If
			Next

			Return vbCrLf & " Deadlock summary ( " & nCount & " blockings " & ") : " & sBuffer.ToString
		End Function

        Private disposedValue As Boolean = False        ' To detect redundant calls

        ' IDisposable
        Protected Overridable Sub Dispose(ByVal disposing As Boolean)
            If Not Me.disposedValue Then
                If disposing Then
                    ' TODO: free unmanaged resources when explicitly called
                    closeConnection()
                End If

                ' TODO: free shared unmanaged resources
            End If
            Me.disposedValue = True
        End Sub

#Region " IDisposable Support "
        ' This code added by Visual Basic to correctly implement the disposable pattern.
        Public Sub Dispose() Implements IDisposable.Dispose
            ' Do not change this code.  Put cleanup code in Dispose(ByVal disposing As Boolean) above.
            Dispose(True)
            GC.SuppressFinalize(Me)
        End Sub
#End Region

        <SqlQueryCleanser>
        Public Shared Function UnescapeSQLBraces(ByVal pSql As String) As String

            Dim leftCurlyReplace As String = System.Configuration.ConfigurationManager.AppSettings.Get("SQL_LEFT_CURLY_REPLACEMENT")
            Dim rightCurlyReplace As String = System.Configuration.ConfigurationManager.AppSettings.Get("SQL_RIGHT_CURLY_REPLACEMENT")
            If leftCurlyReplace = "" Then
                leftCurlyReplace = "{"
            End If
            If rightCurlyReplace = "" Then
                rightCurlyReplace = "}"
            End If
            Return pSql.Replace(leftCurlyReplace, "{").Replace(rightCurlyReplace, "}")
        End Function

    End Class
End Namespace