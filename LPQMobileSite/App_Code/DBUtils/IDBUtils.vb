Imports System.Data

Namespace DBUtils
    'interface that provides functions to encapsulate the dirty day to day routines involving
    'commands, data readers, dataset, connection , and scalerexecution
    Public Interface IDBUtils
        Inherits IDisposable

        Function openConnection() As IDbConnection

        Function openConnectionTransacted() As IDbConnection
		''' <summary>
		''' Commits the transaction, throws an exception if none is open.
		''' </summary>
		Sub commitTransaction()
		''' <summary>
		''' Rolls back the transaction, throws an exception if none is open.
		''' </summary>
		Sub rollbackTransaction()

		''' <summary>
		''' Rolls back the transaction, does nothing if none is open.
		''' </summary>
		Sub rollbackTransactionIfOpen()
		''' <summary>
		''' Commits the transaction, does nothing if none is open.
		''' </summary>
		Sub commitTransactionIfOpen()

        'closes both connection and transacted connection
        'in asp, this method should be invoked on page unload
        Sub closeConnection()

        'used to execute update/insert statements, return -1 if not applicable 
        Function executeNonQuery(ByVal sql As String, Optional ByVal isTransacted As Boolean = False) As Integer

        'uses the non transacted conn to return single value from SELECT sql statement 
        Function getScalerValue(ByVal sql As String) As String

        Function getDataReader(ByVal sql As String) As IDataReader
        Function getDataTable(ByVal sql As String) As DataTable

        'usefull when working with strong typed dataset.  
        Sub FillDataTable(ByVal dt As DataTable, ByVal sqlSelect As String)
        Function UpdateDataTable(ByVal dt As DataTable, ByVal sqlSelectMetaData As String, ByVal isTransacted As Boolean) As Integer
        Function UpdateDataTable(ByVal dt As DataTable, ByVal isTransacted As Boolean) As Integer
    End Interface
End Namespace