Imports System.Configuration.ConfigurationManager

Namespace DBUtils

    ''' <summary>
    ''' Returns a Connection String to the DB. Do not use unless LoansPQBO.CSystemConfig cannot be accessed
    ''' </summary>
    ''' <remarks>This class is the Connection String functionality taken from LoansPQBO.Configs.CSystemConfig. It should 
    ''' be a copy of the code used in that class. This class was designed for the use of projects that do not
    ''' have a reference to LoansPQBO, and cannot access CSystemConfig to retrieve a connection string.
    '''  This class should only be used if LoansPQBO.CSystemConfig cannot be accessed and a connection string is
    '''  needed. Any changes to CSystemConfig ConnectionString logic will require a change here.</remarks>
    Public Class CSQLConnectionString
        Protected Shared Log As log4net.ILog = log4net.LogManager.GetLogger(GetType(CSQLConnectionString))


        'These functions will be used when the userID and Userpw are stored as encrypted and must be decrypted before
        'being inserted into the ConnectionString
        'Since ConnectionString and UnderwriteConnection Strings are used most often, then their connection strings
        'for each app domain will be cached so it doesn't have to run through the regex everytime
        Private Shared _ConnectionString As String = ""
        Private Shared _ConnectionStringLender As String = ""
        Private Shared _ConnectionStringConsumer As String = ""
        Private Shared _ConnectionStringClinic As String = ""
        Private Shared _ConnectionStringServices As String = ""
        Private Shared _ConnectionStringProspects As String = ""
        Private Shared _ConnectionStringRoot As String = ""
        Private Shared _ConnectionStringReseller As String = ""
        Private Shared _ConnectionStringReports As String = ""

        Private Shared _ConnectionString_Underwrite As String = ""
        Private Shared _ConnectionString_UnderwriteLender As String = ""
        Private Shared _ConnectionString_UnderwriteConsumer As String = ""
        Private Shared _ConnectionString_UnderwriteClinic As String = ""
        Private Shared _ConnectionString_UnderwriteServices As String = ""
        Private Shared _ConnectionString_UnderwriteProspects As String = ""
        Private Shared _ConnectionString_UnderwriteRoot As String = ""
        Private Shared _ConnectionString_UnderwriteReseller As String = ""
        Private Shared _ConnectionString_UnderwriteReports As String = ""



        Public Shared ReadOnly Property CONNECTIONSTRING() As String
            Get
                Select Case (CStr(log4net.GlobalContext.Properties("appname")))
                    Case ""
                        If _ConnectionString = "" Then
                            _ConnectionString = getConnectionString("LPQ_CONNECTION")
                        End If
                        Return _ConnectionString
                    Case "web_lender"
                        If _ConnectionStringLender = "" Then
                            _ConnectionStringLender = getConnectionString("LPQ_CONNECTION")
                        End If
                        Return _ConnectionStringLender
                    Case "web_consumer"
                        If _ConnectionStringConsumer = "" Then
                            _ConnectionStringConsumer = getConnectionString("LPQ_CONNECTION")
                        End If
                        Return _ConnectionStringConsumer
                    Case "web_root"
                        If _ConnectionStringRoot = "" Then
                            _ConnectionStringRoot = getConnectionString("LPQ_CONNECTION")
                        End If
                        Return _ConnectionStringRoot
                    Case "web_clinic"
                        If _ConnectionStringClinic = "" Then
                            _ConnectionStringClinic = getConnectionString("LPQ_CONNECTION")
                        End If
                        Return _ConnectionStringClinic
                    Case "web_prospects"
                        If _ConnectionStringProspects = "" Then
                            _ConnectionStringProspects = getConnectionString("LPQ_CONNECTION")
                        End If
                        Return _ConnectionStringProspects
                    Case "web_services"
                        If _ConnectionStringServices = "" Then
                            _ConnectionStringServices = getConnectionString("LPQ_CONNECTION")
                        End If
                        Return _ConnectionStringServices
                    Case "web_reseller"
                        If _ConnectionStringReseller = "" Then
                            _ConnectionStringReseller = getConnectionString("LPQ_CONNECTION")
                        End If
                        Return _ConnectionStringReseller
                    Case "web_reports"
                        If _ConnectionStringReports = "" Then
                            _ConnectionStringReports = getConnectionString("LPQ_CONNECTION")
                        End If
                        Return _ConnectionStringReports

                End Select
                Return getConnectionString("LPQ_CONNECTION")
            End Get
        End Property
        Public Shared ReadOnly Property CONNECTIONSTRING2() As String
            Get
                Return getConnectionString("LPQ_CONNECTION2")
            End Get
        End Property
        Public Shared ReadOnly Property CONNECTIONSTRING_UNDERWRITE() As String
            Get
                Select Case (CStr(log4net.GlobalContext.Properties("appname")))
                    Case ""
                        If _ConnectionString_Underwrite = "" Then
                            _ConnectionString_Underwrite = getConnectionString("LPQ_UW_CONNECTION")
                        End If
                        Return _ConnectionString_Underwrite
                    Case "web_lender"
                        If _ConnectionString_UnderwriteLender = "" Then
                            _ConnectionString_UnderwriteLender = getConnectionString("LPQ_UW_CONNECTION")
                        End If
                        Return _ConnectionString_UnderwriteLender
                    Case "web_consumer"
                        If _ConnectionString_UnderwriteConsumer = "" Then
                            _ConnectionString_UnderwriteConsumer = getConnectionString("LPQ_UW_CONNECTION")
                        End If
                        Return _ConnectionString_UnderwriteConsumer
                    Case "web_root"
                        If _ConnectionString_UnderwriteRoot = "" Then
                            _ConnectionString_UnderwriteRoot = getConnectionString("LPQ_UW_CONNECTION")
                        End If
                        Return _ConnectionString_UnderwriteRoot
                    Case "web_clinic"
                        If _ConnectionString_UnderwriteClinic = "" Then
                            _ConnectionString_UnderwriteClinic = getConnectionString("LPQ_UW_CONNECTION")
                        End If
                        Return _ConnectionString_UnderwriteClinic
                    Case "web_prospects"
                        If _ConnectionString_UnderwriteProspects = "" Then
                            _ConnectionString_UnderwriteProspects = getConnectionString("LPQ_UW_CONNECTION")
                        End If
                        Return _ConnectionString_UnderwriteProspects
                    Case "web_services"
                        If _ConnectionString_UnderwriteServices = "" Then
                            _ConnectionString_UnderwriteServices = getConnectionString("LPQ_UW_CONNECTION")
                        End If
                        Return _ConnectionString_UnderwriteServices
                    Case "web_reseller"
                        If _ConnectionString_UnderwriteReseller = "" Then
                            _ConnectionString_UnderwriteReseller = getConnectionString("LPQ_UW_CONNECTION")
                        End If
                        Return _ConnectionString_UnderwriteReseller
                    Case "web_reports"
                        If _ConnectionString_UnderwriteReports = "" Then
                            _ConnectionString_UnderwriteReports = getConnectionString("LPQ_UW_CONNECTION")
                        End If
                        Return _ConnectionString_UnderwriteReports

                End Select
                Return getConnectionString("LPQ_UW_CONNECTION")
            End Get
        End Property

        Public Shared ReadOnly Property CONNECTIONSTRING_UNDERWRITE_HISTORY() As String
            Get
                Return getConnectionString("LPQ_UW_HISTORY_CONNECTION")
            End Get
        End Property
        Public Shared ReadOnly Property CONNECTIONSTRING_BADMEMBERS() As String
            Get
                Return getConnectionString("LPQ_BADMEMBERS")
            End Get
        End Property
        Public Shared ReadOnly Property CONNECTIONSTRING_SHARE() As String
            Get
                Return getConnectionString("LPQ_SHARE")
            End Get
        End Property
        Public Shared ReadOnly Property CONNECTIONSTRING_MEMBERS() As String
            Get
                Return getConnectionString("LPQ_MEMBERS")
            End Get
        End Property

        Public Shared ReadOnly Property CONNECTIONSTRING_REPORTS() As String
            Get
                Return getConnectionString("LPQ_REPORTS_CONNECTION")
            End Get
		End Property

		Public Shared ReadOnly Property CONNECTIONSTRING_FAS() As String
			Get
				Return getConnectionString("LPQ_FAS")
			End Get
		End Property

        Private Shared _Connection_UserId As String = ""
        Private Shared _Connection_UserPW As String = ""

        Private Shared Function getConnectionString(ByVal appKey As String) As String
			Try
				'TODO: encrypt
				'If _Connection_UserId = "" Then
				'    _Connection_UserId = CBaseStd.Decrypt("LPQ_DBUserID_2000", AppSettings.Get("LPQ_DBUserID"))
				'End If
				'If _Connection_UserPW = "" Then
				'    _Connection_UserPW = CBaseStd.Decrypt("LPQ_DBPW_2000", AppSettings.Get("LPQ_DBPW"))
				'End If
			Catch
				'_Connection_UserId = AppSettings.Get("LPQ_DBUserID")
				'_Connection_UserPW = AppSettings.Get("LPQ_DBPW")
			End Try

            Dim connectionString As String = String.Format(AppSettings.Get(appKey), _Connection_UserId, _Connection_UserPW)

            Dim regString As New System.Text.RegularExpressions.Regex("(Application Name=)([^;]*)")
            Dim sCurrentApplicationName As String = regString.Match(connectionString).Groups(2).Value
            Dim sReplaceApplicationName As String = "Application Name=" & sCurrentApplicationName
            Dim sAppDomain As String = CStr(log4net.GlobalContext.Properties("appname"))

            If sAppDomain IsNot Nothing AndAlso sAppDomain <> "" Then
                sReplaceApplicationName += " (" & sAppDomain & ")"
            End If


            Dim newString As String = System.Text.RegularExpressions.Regex.Replace(connectionString, "(Application Name=)([^;]*)", sReplaceApplicationName)
            Return newString

        End Function

        Public Shared ReadOnly Property CONNECTION_USERID() As String
            Get
                If _Connection_UserId <> "" Then
                    Return _Connection_UserId
				Else
					'TODO:decrypt
					'Try
					'    _Connection_UserId = CBaseStd.Decrypt("LPQ_DBUserID_2000", AppSettings.Get("LPQ_DBUserID"))
					'Catch
					'    _Connection_UserId = AppSettings.Get("LPQ_DBUserID")
					'End Try
					Return _Connection_UserId
                End If
            End Get
        End Property

        Public Shared ReadOnly Property CONNECTION_USERPW() As String
            Get
                If _Connection_UserPW <> "" Then
                    Return _Connection_UserPW
				Else
					'TODO
					'Try
					'    _Connection_UserPW = CBaseStd.Decrypt("LPQ_DBPW_2000", AppSettings.Get("LPQ_DBPW"))
					'Catch
					'    _Connection_UserPW = AppSettings.Get("LPQ_DBPW")
					'End Try
					Return _Connection_UserPW
                End If
            End Get
        End Property


        Public Shared ReadOnly CONNECTION_DBSOURCE As String = AppSettings.Get("LPQ_DBSOURCE") ' this is used for reporting module
        Public Shared ReadOnly CONNECTION_DBName As String = AppSettings.Get("LPQ_DBName") '"LoansPQ"

    End Class
End Namespace
