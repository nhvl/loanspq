Namespace DBUtils
    'BEN: DOO NOT USE YET!!!!!!!!!!!!!!!  This has not been thoroughly finished/designed

    ''' <summary>
    ''' This interface helps make our classes more explicit about what type of data it being generated.  
    ''' The reason for this is to help prevent sql injection by automatically escaping strings for you.
    ''' For numerics, it will force you to convert into the correct datatype before you can insert.
    ''' </summary>
    ''' <remarks></remarks>
    Public Interface ISQLEditStringBuilder2
        ReadOnly Property SQL() As String
        Sub AppendString(ByVal sField As String, ByVal sValue As String)
        Sub AppendNumber(ByVal sField As String, ByVal nValue As Integer)
        Sub AppendNumber(ByVal sField As String, ByVal nValue As Long)
        Sub AppendNumber(ByVal sField As String, ByVal nValue As Single)
        Sub AppendNumber(ByVal sField As String, ByVal nValue As Double)
        Sub reset()
    End Interface

    ''' <summary>
    ''' This class assists in building the INSERT SQL statements. It produce sql like this:
    ''' "INSERT INTO table1 (field1, field2, field3) Values ('a', 'b', 'c')
    ''' </summary>
	<Obsolete("DO NOT USE!")> Public Class cSQLInsertStringBuilder2
		Implements ISQLEditStringBuilder2

		Public Sub New(Optional ByVal sTable As String = "")
			_tableName = sTable
			m_oFields = New cSQLCommaStringBuilder()
			m_oValues = New cSQLCommaStringBuilder()
		End Sub

		Public Sub Reset() Implements ISQLEditStringBuilder2.reset
			m_oFields.Reset()
			m_oValues.Reset()
		End Sub

		Public ReadOnly Property SQL() As String Implements ISQLEditStringBuilder2.SQL
			Get
				If _tableName = String.Empty Then
					Throw New Exception("SQL Insert builder requires you to specify table")
				End If

				Dim sSQL As New System.Text.StringBuilder()
				sSQL.Append("Insert into " & _tableName & " ")
				sSQL.Append("(")
				sSQL.Append(m_oFields.SQL)
				sSQL.Append(") VALUES (")
				sSQL.Append(m_oValues.SQL)
				sSQL.Append(")")

				Return sSQL.ToString
			End Get
		End Property

		''' <summary>
		''' Append new string value. DO NOT escape the string value.  It will automatically be escaped!
		''' </summary>
		''' <param name="sField"></param>
		''' <param name="sValue">The unescaped string value to insert.</param>        
		Public Sub AppendString(ByVal sField As String, ByVal sValue As String) Implements ISQLEditStringBuilder2.AppendString
			m_oFields.AppendString(sField)
			m_oValues.AppendString(CSQLDBUtils.SQLString(sValue))
		End Sub

		Public Sub AppendNumber(ByVal sField As String, ByVal nValue As Double) Implements ISQLEditStringBuilder2.AppendNumber
			m_oFields.AppendString(sField)
			m_oValues.AppendString(nValue.ToString)
		End Sub

		Public Sub AppendNumber(ByVal sField As String, ByVal nValue As Integer) Implements ISQLEditStringBuilder2.AppendNumber
			m_oFields.AppendString(sField)
			m_oValues.AppendString(nValue.ToString)
		End Sub

		Public Sub AppendNumber(ByVal sField As String, ByVal nValue As Long) Implements ISQLEditStringBuilder2.AppendNumber
			m_oFields.AppendString(sField)
			m_oValues.AppendString(nValue.ToString)
		End Sub

		Public Sub AppendNumber(ByVal sField As String, ByVal nValue As Single) Implements ISQLEditStringBuilder2.AppendNumber
			m_oFields.AppendString(sField)
			m_oValues.AppendString(nValue.ToString)
		End Sub

		Private _tableName As String
		Private m_oFields As cSQLCommaStringBuilder
		Private m_oValues As cSQLCommaStringBuilder
	End Class

    ' 
    ''' <summary>
    ''' This class assists in building the UPDATE SQL statements. It produce sql like this:
    ''' "UPDATE table1 SET field1='a', field2='b', field3='c'"     
    ''' </summary>    
    Public Class cSQLUpdateStringBuilder2
        Implements ISQLEditStringBuilder2

        Public Sub New2()
            m_oSQL = New cSQLCommaStringBuilder()
        End Sub
        Public Sub New(ByVal sTableName As String, ByVal sConditionWithWhereClause As String)
            _tableName = sTableName
            _Condition = sConditionWithWhereClause
            m_oSQL = New cSQLCommaStringBuilder()
        End Sub

        Public ReadOnly Property SQL() As String Implements ISQLEditStringBuilder2.SQL
            Get
                If _tableName = String.Empty Or _Condition = String.Empty Then
                    Throw New Exception("SQL Update builder requires you to specify BOTH table and condition")
                End If
                Dim sSQL As New System.Text.StringBuilder()

                sSQL.Append("Update " & _tableName & " ")

                sSQL.Append("SET ")
                sSQL.Append(m_oSQL.SQL)

                If _Condition <> String.Empty Then
                    sSQL.Append(" " & _Condition)
                End If
                Return sSQL.ToString
            End Get
        End Property

        Public ReadOnly Property IsEmpty() As Boolean
            Get
                Return m_oSQL.IsEmpty
            End Get
        End Property

        'this only resets all the appends
        Public Sub Reset() Implements ISQLEditStringBuilder2.reset
            m_oSQL.Reset()
        End Sub

        ''' <summary>
        ''' Append new string value. DO NOT escape the string value.  It will automatically be escaped!
        ''' </summary>
        ''' <param name="sField"></param>
        ''' <param name="sValue">The unescaped string value to insert.</param>        
        Public Sub AppendString(ByVal sField As String, ByVal sValue As String) Implements ISQLEditStringBuilder2.AppendString
            m_oSQL.AppendString(sField & "=" & CSQLDBUtils.SQLString(sValue))
        End Sub

        Public Sub AppendNumber(ByVal sField As String, ByVal nValue As Double) Implements ISQLEditStringBuilder2.AppendNumber
            m_oSQL.AppendString(sField & "=" & nValue)
        End Sub

        Public Sub AppendNumber(ByVal sField As String, ByVal nValue As Integer) Implements ISQLEditStringBuilder2.AppendNumber
            m_oSQL.AppendString(sField & "=" & nValue)
        End Sub

        Public Sub AppendNumber(ByVal sField As String, ByVal nValue As Long) Implements ISQLEditStringBuilder2.AppendNumber
            m_oSQL.AppendString(sField & "=" & nValue)
        End Sub

        Public Sub AppendNumber(ByVal sField As String, ByVal nValue As Single) Implements ISQLEditStringBuilder2.AppendNumber
            m_oSQL.AppendString(sField & "=" & nValue)
        End Sub

        Private _tableName As String
        Private _Condition As String
        Private m_oSQL As cSQLCommaStringBuilder
    End Class

    '
    '
    ''' <summary>
    ''' This class is pretty simple but will help avoid accidental deletion of all records by forcing you to enter a condition!!! :)
    ''' If you want to manually delete all records, then it's up to you to do it manually 
    ''' </summary>
    ''' <remarks></remarks>
    Public Class CSQLDeleteStringBuilder2
        ''' <summary>
        ''' Constructor (String primary key) that initializes table and conditions to delete on.  Please note the key values need to be in unescaped form!
        ''' </summary>
        ''' <param name="sTable"></param>
        ''' <param name="sPrimaryKey"></param>
        ''' <param name="sValue">UNESCAPED primary key string value</param>        
        Public Sub New(ByVal sTable As String, ByVal sPrimaryKey As String, ByVal sValue As String)
            _WhereBuilder = New CSQLWhereStringBuilder()
            _WhereBuilder.AppendANDCondition(sPrimaryKey & "=" & CSQLDBUtils.SQLString(sValue))
            _TableName = sTable
        End Sub

        ''' <summary>
        ''' Overload constructor that accepts integer
        ''' </summary>        
        Public Sub New(ByVal sTable As String, ByVal sPrimaryKey As String, ByVal nValue As Integer)
            _WhereBuilder = New CSQLWhereStringBuilder()
            _WhereBuilder.AppendANDCondition(sPrimaryKey & "=" & nValue.ToString)
            _TableName = sTable
        End Sub

        ''' <summary>
        ''' Overload constructor that accepts long datatype key value 
        ''' </summary>        
        Public Sub New(ByVal sTable As String, ByVal sPrimaryKey As String, ByVal nValue As Long)
            _WhereBuilder = New CSQLWhereStringBuilder()
            _WhereBuilder.AppendANDCondition(sPrimaryKey & "=" & nValue.ToString)
            _TableName = sTable
        End Sub


        Public Sub New(ByVal sTable As String, ByVal oWhereBuilder As CSQLWhereStringBuilder)
            _WhereBuilder = oWhereBuilder
            _TableName = sTable
        End Sub
        Private _WhereBuilder As CSQLWhereStringBuilder
        Private _TableName As String
        Public ReadOnly Property SQL() As String
            Get
                Dim sWhere As String = _WhereBuilder.SQL
                If sWhere = "" Then
                    Throw New ApplicationException("Unexpected dangerous delete all records attempted!  ")
                End If

                Return "Delete From " & _TableName & _WhereBuilder.SQL
            End Get
        End Property
    End Class




End Namespace