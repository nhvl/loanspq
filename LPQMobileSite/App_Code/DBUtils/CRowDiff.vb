Imports System.Data
Namespace DBUtils
	'these classes help generate differences in rows 
	Public Class CRowDiff
		Public Sub New(ByVal psTableName As String)
			TableName = psTableName
		End Sub
		Public TableName As String
		Public ColumnDifferences As New CColumnDiffList()

		Public Overrides Function ToString() As String
			Dim sResult As String = TableName & vbCrLf
			Dim colDiff As CColumnDiff
			For Each colDiff In ColumnDifferences
				sResult += "    " & colDiff.ToString & vbCrLf
			Next
			Return sResult
		End Function

	End Class

	Public Class CRowDiffList
		Inherits System.Collections.ArrayList

		Default Public Shadows Property Item(ByVal index As Integer) As CRowDiff
			Get
				Return CType(MyBase.Item(index), CRowDiff)
			End Get
			Set(ByVal Value As CRowDiff)
				MyBase.Item(index) = Value
			End Set
		End Property

		Public Shadows Function Add(ByVal value As CRowDiff) As Integer
			Return MyBase.Add(value)
		End Function
	End Class

	Public Class CColumnDiff

		Public ColumnName As String
		Public OriginalValue As Object
		Public NewValue As Object

		Public Overrides Function ToString() As String
			Return ColumnName & " : " & OriginalValue.ToString & " | " & NewValue.ToString
		End Function

		Public Sub New(ByVal psColumnName As String, ByVal poOld As Object, ByVal poNew As Object)
			ColumnName = psColumnName
			OriginalValue = poOld
			NewValue = poNew
		End Sub

		Public Shared Function CompareRows(ByVal poOld As DataRow, ByVal poNew As DataRow) As CColumnDiffList
			Dim oResult As New CColumnDiffList()
			Dim c As DataColumn
			For Each c In poOld.Table.Columns

				If TypeOf poOld(c.ColumnName) Is DBNull AndAlso TypeOf poNew(c.ColumnName) Is DBNull Then
					'same 
				ElseIf TypeOf poOld(c.ColumnName) Is DBNull Or TypeOf poNew(c.ColumnName) Is DBNull Then
					'1 of the value is dbnull 
					oResult.Add(New CColumnDiff(c.ColumnName, poOld(c.ColumnName), poNew(c.ColumnName)))
				ElseIf poOld(c.ColumnName).Equals(poNew(c.ColumnName)) = False Then
					oResult.Add(New CColumnDiff(c.ColumnName, poOld(c.ColumnName), poNew(c.ColumnName)))
				End If
			Next

			Return oResult
		End Function
	End Class

	Public Class CColumnDiffList
		Inherits System.Collections.ArrayList

		Default Public Shadows Property Item(ByVal index As Integer) As CColumnDiff
			Get
				Return CType(MyBase.Item(index), CColumnDiff)
			End Get
			Set(ByVal Value As CColumnDiff)
				MyBase.Item(index) = Value
			End Set
		End Property

		Public Shadows Function Add(ByVal value As CColumnDiff) As Integer
			Return MyBase.Add(value)
		End Function
	End Class

End Namespace