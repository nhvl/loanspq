Imports System.Collections.Generic
Imports System.Globalization
'Imports SpecializedDataTypes

Namespace DBUtils
	''' <summary>
	''' Note we don't have a fieldname on this data structure.  This is so it's the API
	''' at a minimal and reusable.
	''' </summary>
	Public Class CSQLParamValue
		Implements IEquatable(Of CSQLParamValue)

		Public Enum DBDataTypes
			NumberType
			StringType
			DateType
			DatabaseNull
			BinaryType
			''' <summary>
			''' This is useful when we need to call system functions or other complex sql based calculation.   Content will be assumed to already be escaped.   Value must be
			''' constructed of type CSQLParamString, which helps format/escape data.   Instance of the CSQLParamString can then be passed into CSQLParamValue constructor.
			''' </summary>			
			EscapedString
		End Enum
		Public Enum DBStringTypes
			NormalString
			DateString
			Guid16String
		End Enum

		Private _DBType As DBDataTypes
		Public Property DBType() As DBDataTypes
			Get
				Return _DBType
			End Get
			Set(ByVal value As DBDataTypes)
				If value = DBDataTypes.DatabaseNull Then
					Throw New ArgumentException("Cannot create new DatabaseNull; use the CSQLParamValue.NullValue singleton.")
				End If

				_DBType = value
			End Set
		End Property

		Public Overrides Function ToString() As String
			Return SQLValue
		End Function

		Private _SQLValue As String
		''' <summary>
		''' Readonly value representing the escaped value that will be used in the final SQL generation.  No writing allowed to prevent possible abuse of 
		''' bypassing the escaping process.
		''' </summary>
		Public ReadOnly Property SQLValue() As String
			Get
				Return _SQLValue
			End Get
		End Property

#Region "Constructors"


#Region "Numeric Constructor"
		Public Sub New(ByVal pnValue As Integer)
			DBType = DBDataTypes.NumberType
			_SQLValue = pnValue.ToString
		End Sub

		Public Sub New(ByVal pnValue As Long)
			DBType = DBDataTypes.NumberType
			_SQLValue = pnValue.ToString
		End Sub

		Public Sub New(ByVal pnValue As Single)
			DBType = DBDataTypes.NumberType
			_SQLValue = pnValue.ToString
		End Sub

		Public Sub New(ByVal pnValue As Double)
			DBType = DBDataTypes.NumberType
			_SQLValue = pnValue.ToString
		End Sub
#End Region

		''' <summary>
		''' Empty New for use by subclasses only
		''' </summary>
		Protected Sub New()
		End Sub

		''' <summary>
		''' Construct to provide for more complex string entry that's ALREADY escaped (eg: CSQLParamString ('getDate()').
		''' </summary>
		Public Sub New(ByVal poValue As CSQLParamString)
			DBType = DBDataTypes.EscapedString

			_SQLValue = poValue.ToString
		End Sub

		''' <summary>
		''' Captures a boolean but stores it as a Y or N (instead of the less readible 0 and 1) because that's the standard we use in LPQ.
		''' </summary>
		Public Sub New(ByVal poValue As Boolean)
			DBType = DBDataTypes.StringType
			If True = poValue Then
				_SQLValue = "'Y'"
			Else
				_SQLValue = "'N'"
			End If
		End Sub

		''' <summary>
		''' Takes a GUID value and stores it as a native SQL UniqueIdentifier. To store as Char(32) use the (guid, boolean) overload
		''' </summary>
		''' <param name="poValue"></param>
		''' <remarks></remarks>
		Public Sub New(ByVal poValue As Guid)
			DBType = DBDataTypes.StringType
			_SQLValue = SQLString(poValue.ToString)
		End Sub

		''' <summary>
		''' Takes a GUID value and stores it as either guid16 (native SQL uniqueidentifier data type) or our Char(32) GUID format
		''' </summary>
		''' <param name="poValue">The GUID to store into the database</param>
		''' <param name="pbIsNativeGUIDColumn">True = Store as native GUID; False = Store as string representation</param>
		''' <remarks></remarks>
		Public Sub New(ByVal poValue As Guid, ByVal pbIsNativeGUIDColumn As Boolean)
			If pbIsNativeGUIDColumn Then
				DBType = DBDataTypes.StringType
				_SQLValue = SQLString(poValue.ToString)
			Else
				'Guid.ToString("N") returns the un-hyphenated "GUID32" format we use to store a GUID in a char(32) column
				DBType = DBDataTypes.StringType
				_SQLValue = SQLString(poValue.ToString("N"))
			End If

		End Sub

		''' <summary>
		''' This construct just escapes a simple string value for use in sql generation.
		''' </summary>
		Public Sub New(ByVal psValue As String)
			DBType = DBDataTypes.StringType
			_SQLValue = SQLString(psValue)
		End Sub


		'TODO:
		''' <summary>
		''' This construct supports simple string, but is really designed for date values that we want to store into the db safely (peStringType = DateString).   
		''' If the value cannot be safely converted for db storage, a NULL value will be generated. 
		''' </summary>
		'Public Sub New(ByVal psValue As String, ByVal peStringType As DBStringTypes)
		'	Select Case peStringType
		'		Case DBStringTypes.NormalString
		'			DBType = DBDataTypes.StringType
		'			_SQLValue = SQLString(psValue)
		'		Case DBStringTypes.DateString
		'			DBType = DBDataTypes.DateType
		'			If IsValidDate(psValue) Then
		'				_SQLValue = SQLString(CDate(psValue).ToString)
		'			Else
		'				_SQLValue = "NULL"
		'			End If
		'		Case DBStringTypes.Guid16String
		'			DBType = DBDataTypes.StringType
		'			_SQLValue = SQLGuid(psValue)
		'	End Select
		'End Sub

		'TODO:date stuff
		''' <summary>
		''' Constructor for a date param.  It's recommended you use IsValidDate routine prior to constructing this param to make sure the date you pass in 
		''' will be safe to be stored into the DB. 
		''' </summary>
		'Public Sub New(ByVal pdDateValue As Date)
		'	DBType = DBDataTypes.DateType
		'	If IsValidDate(pdDateValue) Then
		'		_SQLValue = SQLString(pdDateValue.ToString())
		'	Else
		'		_SQLValue = "NULL"
		'	End If
		'End Sub

		'TODO: add datestruct later
		'''' <summary>
		'''' Constructor for a dateStruct param.  It's recommended you use IsValidDate routine prior to constructing this param to make sure the date you pass in 
		'''' will be safe to be stored into the DB. 
		'''' </summary>
		'      Public Sub New(ByVal pdDateValue As DateStruct)
		'          Me.New(New Nullable(Of DateStruct)(pdDateValue))
		'      End Sub

		'      ''' <summary>
		'      ''' Constructor for a dateStruct param.  It's recommended you use IsValidDate routine prior to constructing this param to make sure the date you pass in 
		'      ''' will be safe to be stored into the DB. 
		'      ''' </summary>
		'      Public Sub New(ByVal pdDateValue As Nullable(Of DateStruct))
		'          DBType = DBDataTypes.DateType
		'          If IsValidDate(pdDateValue) Then
		'              _SQLValue = SQLString(pdDateValue.ToString())
		'          Else
		'              _SQLValue = "NULL"
		'          End If
		'End Sub

		'''' <summary>
		'''' Constructor for a DateTimeStruct param.  It's recommended you use IsValidDate routine prior to constructing this param to make sure the date you pass in 
		'''' will be safe to be stored into the DB. 
		'''' </summary>
		'Public Sub New(ByVal pdDateValue As Nullable(Of DateTimeStruct))
		'	DBType = DBDataTypes.DateType
		'	If IsValidDate(pdDateValue) Then
		'		_SQLValue = SQLString(pdDateValue.ToString())
		'	Else
		'		_SQLValue = "NULL"
		'	End If
		'End Sub

		''' <summary>
		''' Constructor for binary data (represented in .net by a byte array, in SQL by a binary or varbinary column.)
		''' </summary>
		Public Sub New(ByVal poBinary As Byte())
			DBType = DBDataTypes.BinaryType
			If poBinary Is Nothing Then
				_SQLValue = "NULL"
			Else
				Dim sb As New System.Text.StringBuilder
				sb.Append("0x")
				For i As Integer = 0 To poBinary.Length - 1
					sb.Append(poBinary(i).ToString("X2"))
				Next
				_SQLValue = sb.ToString
			End If
		End Sub
#End Region

		'TODO: work on date later
		'#Region "IsValidDate"
		'		''' <summary>
		'		''' This is a convenience routine that will check to make sure our value can be safely converted into a value that 
		'		''' can be safely stored in our db (sql 2005).   It handles MinValue and date ranges supported by sql server. 
		'		''' </summary>
		'		Public Shared Function IsValidDate(ByVal poValue As Object) As Boolean
		'			Dim convertedDate As Nullable(Of Date)
		'			If TypeOf poValue Is String Then
		'				convertedDate = ParseDate(DirectCast(poValue, String))
		'			ElseIf TypeOf poValue Is Nullable(Of Date) Then
		'				convertedDate = NullableCast(Of Date)(poValue)
		'			ElseIf TypeOf poValue Is Nullable(Of DateStruct) Then
		'				Return IsValidDate(NullableCast(Of DateStruct)(poValue))
		'			ElseIf TypeOf poValue Is Nullable(Of DateTimeStruct) Then
		'				Return IsValidDate(NullableCast(Of DateTimeStruct)(poValue))
		'			Else 'Date, DBNull
		'				convertedDate = NullableCast(Of Date)(poValue)
		'			End If

		'			Return IsValidDate(convertedDate)
		'		End Function

		'		''' <summary>
		'		''' This is a convenience routine that will check to make sure our value can be safely converted into a value that 
		'		''' can be safely stored in our db (sql 2005).   It handles MinValue and date ranges supported by sql server. 
		'		''' </summary>
		'		Public Shared Function IsValidDate(ByVal pDate As Nullable(Of Date)) As Boolean
		'			'For convenience
		'			If Not pDate.HasValue Then
		'				Return False
		'			End If

		'			If pDate.Value = Date.MinValue Then	'Default Date value.  Shouldn't ever be used for real data.
		'				Return False
		'			End If
		'			If pDate.Value < New Date(1753, 1, 1) OrElse pDate.Value > New Date(9999, 12, 31) Then 'Range for SQL Server 2005 DateTime
		'				Return False
		'			End If

		'			Return True
		'		End Function

		'		''' <summary>
		'		''' This is a convenience routine that will check to make sure our value can be safely converted into a value that 
		'		''' can be safely stored in our db (sql 2005).   It handles MinValue and date ranges supported by sql server. 
		'		''' </summary>
		'		Public Shared Function IsValidDate(ByVal pDate As Nullable(Of DateStruct)) As Boolean
		'			'For convenience
		'			If Not pDate.HasValue Then
		'				Return False
		'			End If

		'			Return IsValidDate(pDate.Value)
		'		End Function

		'		''' <summary>
		'		''' This is a convenience routine that will check to make sure our value can be safely converted into a value that 
		'		''' can be safely stored in our db (sql 2005).   It handles MinValue and date ranges supported by sql server. 
		'		''' </summary>
		'		Public Shared Function IsValidDate(ByVal pDateTime As Nullable(Of DateTimeStruct)) As Boolean
		'			'For convenience
		'			If Not pDateTime.HasValue Then
		'				Return False
		'			End If

		'			'Return IsValidDate(pDateTime.Value.ToTimeZone(CTimeZoneManager.LocalZone).ToDateTime())
		'			Return IsValidDate(pDateTime)
		'		End Function

		'		''' <summary>
		'		''' This is a convenience routine that will check to make sure our value can be safely converted into a value that 
		'		''' can be safely stored in our db (sql 2005).   It handles MinValue and date ranges supported by sql server. 
		'		''' </summary>
		'		Public Shared Function IsValidDate(ByVal pDateStruct As DateStruct) As Boolean
		'			If pDateStruct = Date.MinValue Then	'Default Date value.  Shouldn't ever be used for real data.
		'				Return False
		'			End If
		'			If pDateStruct < New Date(1753, 1, 1) OrElse pDateStruct > New Date(9999, 12, 31) Then 'Range for SQL Server 2005 DateTime
		'				Return False
		'			End If

		'			Return True
		'		End Function
		'#End Region

#Region "Copied from CBaseStd"

		''' <summary>
		''' Returns a string with escaped single quotes and \CrLf and \Lf SQL escape sequences and surrounded with single quotes
		''' </summary>
		''' <remarks>The expression \cr also created a difference between the log viewers. In PB, it shows up as \ followed by a box
		''' and in the log viewer it simply showed as a newline</remarks>
		Protected Shared Function SQLString(ByVal s As String) As String
			Dim sqlValue As String = "'" & Replace(s, "'", "''") & "'"
			'Replace \<cr><lf> with \\<cr><lf><cr><lf> and \<lf> with \\<lf><lf> since they are escape sequences
			Return sqlValue.Replace("\" & vbCrLf, "\\" & vbCrLf & vbCrLf).Replace("\" & vbLf, "\\" & vbLf & vbLf)
		End Function

		Protected Shared Function SQLGuid(ByVal value As String) As String
			If value Is Nothing OrElse value = "" Then
				Return SQLGuid(Guid.Empty)
			Else
				Return SQLGuid(New Guid(value))
			End If
		End Function

		Protected Shared Function SQLGuid(ByVal value As Guid) As String
			Return "'" & value.ToString() & "'"
		End Function

		''' <summary>
		''' Same as TryCast, but for value types.
		''' </summary>
		''' <returns>The input object casted to a Nullable(of T) if the object is of type T or type Nullable(of T).  Else, Nothing.</returns>
		''' <remarks>This will not work for accessing nullable value types from strongly-typed properties on DataSets.</remarks>
		Protected Shared Function NullableCast(Of T As Structure)(ByVal poObject As Object) As Nullable(Of T)
			If TypeOf poObject Is T Then
				Return DirectCast(poObject, T)
			End If
			If TypeOf poObject Is Nullable(Of T) Then
				Return DirectCast(poObject, Nullable(Of T))
			End If

			Return Nothing
		End Function

#Region "ParseDate"
		Protected Const DATE_STYLES As DateTimeStyles = DateTimeStyles.AllowLeadingWhite Or DateTimeStyles.AllowTrailingWhite Or DateTimeStyles.AssumeLocal

		Protected Shared ReadOnly Property DATE_FORMATS(ByVal pFormatInfo As DateTimeFormatInfo) As String()
			Get
				'Public Shared ReadOnly DATE_FORMATS_STANDARD As String() = {"d", "D", "M"}
				With pFormatInfo
					Return New String() {.ShortDatePattern, .LongDatePattern, .MonthDayPattern, "M/d", "MM/dd", "MM/dd/yy", "M/d/yy", "yyyy'-'MM'-'dd", "dd' 'MMM' 'yyyy"}
				End With
			End Get
		End Property

		Protected Shared ReadOnly Property TIME_FORMATS(ByVal pFormatInfo As DateTimeFormatInfo) As String()
			Get
				'Public Shared ReadOnly TIME_FORMATS_STANDARD As String() = {"t", "T"}
				With pFormatInfo
					Return New String() {.ShortTimePattern, .LongTimePattern, "HH:mm:ss", "HH:mm"}
					'Currently there is no format with time zone info (.NET 2.0 supports UTC offsets, but not full time zones in DateTime format strings)
				End With
			End Get
		End Property

		'TODO
		'''' <summary>
		'''' Custom DateTime format strings containing both date and time components.
		'''' </summary>
		'Protected Shared ReadOnly Property DATE_TIME_FORMATS(ByVal pFormatInfo As DateTimeFormatInfo) As Generic.IEnumerable(Of String)
		'	Get
		'		Dim standardFormats As String() = {"f", "F", "g", "G", "o", "R", "s", "u", "U"}
		'		Dim formats As New CSet(Of String)
		'		formats.AddRange(standardFormats)

		'		For Each dateFormat As String In DATE_FORMATS(pFormatInfo)
		'			For Each timeFormat As String In TIME_FORMATS(pFormatInfo)
		'				formats.Add(dateFormat & "' '" & timeFormat)
		'			Next
		'		Next

		'		Return formats.AsReadOnly()
		'	End Get
		'End Property

		'''' <summary>
		'''' Parse a date from a string.
		'''' </summary>
		'''' <returns>The date parsed from the string, or null if the string's value is not a date or the string is null or empty.</returns>
		'Protected Shared Function ParseDate(ByVal pValue As String) As Nullable(Of Date)
		'	If String.IsNullOrEmpty(pValue) Then
		'		Return Nothing
		'	End If

		'	Dim culture As CultureInfo = New CultureInfo("en-US")	'Assume this culture rather than using CultureInfo.CurrentCulture
		'	Dim formats As New CSet(Of String)
		'	With formats
		'		.AddRange(DATE_FORMATS(culture.DateTimeFormat))
		'		.AddRange(DATE_TIME_FORMATS(culture.DateTimeFormat))
		'		.AddRange(TIME_FORMATS(culture.DateTimeFormat))
		'	End With

		'	Dim d As Date
		'	If Date.TryParseExact(pValue, formats.ToArray, culture, DATE_STYLES, d) Then
		'		Return d
		'	End If

		'	Return Nothing
		'End Function
#End Region
#End Region

#Region "EqualityComparer"
		Public Overrides Function GetHashCode() As Integer
			Return (SQLValue & DBType.ToString).GetHashCode
		End Function

		Public Overrides Function Equals(ByVal obj As Object) As Boolean
			Return Equals(DirectCast(obj, CSQLParamValue))
		End Function

		Public Overridable Overloads Function Equals(ByVal other As CSQLParamValue) As Boolean
			Return EqualsValue(other)
		End Function

		Private Function EqualsValue(ByVal other As CSQLParamValue) As Boolean Implements System.IEquatable(Of CSQLParamValue).Equals
			If other Is Nothing Then
				Return False
			End If

			Return DBType.Equals(other.DBType) AndAlso SQLValue.Equals(other.SQLValue)
		End Function

		Public Class CEqualityComparer
			Implements IEqualityComparer(Of CSQLParamValue)

			Public Function EqualsValue(ByVal x As CSQLParamValue, ByVal y As CSQLParamValue) As Boolean Implements System.Collections.Generic.IEqualityComparer(Of CSQLParamValue).Equals
				If x Is Nothing Then
					Return y Is Nothing
				End If

				Return x.Equals(y)
			End Function

			Public Function GetHashCodeValue(ByVal obj As CSQLParamValue) As Integer Implements System.Collections.Generic.IEqualityComparer(Of CSQLParamValue).GetHashCode
				If obj Is Nothing Then
					Throw New ArgumentNullException("obj")
				End If

				Return obj.GetHashCode
			End Function
		End Class
#End Region

#Region "Converters" 'Useful for generic collection methods that take System.Converter delegates
		Public Shared Function Convert(ByVal pGuid As Guid) As CSQLParamValue
			Return New CSQLParamValue(pGuid)
		End Function
#End Region

#Region "NullValue"
		<Obsolete("Not used junk class? This class introduces another way to handle NULL that's not consistent w/ normal constructor (eg: NullableOf(DateStruct) that still maintains the datatype as Date).")> Private Class CSQLParamNull
			Inherits CSQLParamValue

			Public Sub New()
				MyBase.New()
				Me._SQLValue = "NULL"
				Me._DBType = DBDataTypes.DatabaseNull
			End Sub

			''' <summary>
			''' SQL NULL values doesn't equal anything, including SQL NULL!
			''' </summary>
			Public Overrides Function Equals(ByVal obj As Object) As Boolean
				Return False
			End Function

			''' <summary>
			''' SQL NULL values don't equal anything, including SQL NULL!
			''' </summary>
			Public Overrides Function Equals(ByVal other As CSQLParamValue) As Boolean
				Return False
			End Function
		End Class

        'Public Shared ReadOnly NullValue As CSQLParamValue = New CSQLParamNull()
#End Region
	End Class
End Namespace