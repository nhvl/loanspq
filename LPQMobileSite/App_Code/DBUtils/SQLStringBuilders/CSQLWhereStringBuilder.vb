Imports System.Collections.Generic

Namespace DBUtils

	Public Class CSQLWhereStringBuilder
		Public ReadOnly Property SQL() As String
			Get
				If Not IsEmpty Then
					Return " WHERE " & m_sSQL
				Else
					Return ""
				End If
			End Get
		End Property

		Public ReadOnly Property IsEmpty() As Boolean
			Get
				Return m_sSQL = ""
			End Get
		End Property

		Public Sub Reset()
			m_sSQL = ""
		End Sub

		''' <summary>
		''' Convenience routine commonly used to generate search sql.  
		''' </summary>
		''' <param name="psFieldName">AKA Column name on the table</param>
		''' <param name="poOptionalValue">The wrapped up value that we're going to condition on.</param>
		''' <param name="psOperator">ie: Like, =, etc.</param>
		''' <param name="psDefaultValue">Uses blank string as default.  For numerics, the
		''' default of "0" will be used unless otherwise specified.</param>
		''' <remarks>
		''' Using a default of 0 is useful for cases where we simply use SafeInteger on a user input but don't 
		''' really want to condition on it.
		''' </remarks>
		Public Sub AppendOptionalAndCondition(ByVal psFieldName As String, ByVal poOptionalValue As CSQLParamValue, ByVal psOperator As String, ByVal psDefaultValue As String)

			If poOptionalValue.DBType = CSQLParamValue.DBDataTypes.NumberType Then
				If psDefaultValue = "" Then
					psDefaultValue = "0"
				ElseIf IsNumeric(psDefaultValue) = False Then
					Throw New ArgumentException("psDefaultValue must either be blank or a numeric string for numeric sql paramater.")
				End If
				'if numeric values match within a certain fraction of default, then just skip out
				If Math.Abs(CDbl(poOptionalValue.SQLValue) - CDbl(psDefaultValue)) < 0.0000001 Then
					Return
				End If
			ElseIf poOptionalValue.SQLValue = "''" Then
				Return
			ElseIf poOptionalValue.SQLValue = psDefaultValue Then
				Return
			End If

			'AppendOptionalAndCondition(psFieldName, poOptionalValue.SQLValue, psOperator)

			Dim sOptionalValue As String = poOptionalValue.SQLValue

			If sOptionalValue <> "" AndAlso sOptionalValue <> "''" AndAlso sOptionalValue <> "'%'" AndAlso sOptionalValue <> "'%%'" Then
				AppendAndCondition(psFieldName & " " & psOperator & " " & sOptionalValue)
			End If

		End Sub

		''' <summary>
		''' Shorthand version of routine with default being ""
		''' </summary>
		Public Sub AppendOptionalAndCondition(ByVal psFieldName As String, ByVal poOptionalValue As CSQLParamValue, ByVal psOperator As String)
			AppendOptionalAndCondition(psFieldName, poOptionalValue, psOperator, "")
		End Sub

		''' <summary>
		''' Appends an sql fragment if not null and sql is not blank.  Useful for date type scenarios:
		''' 
		''' </summary>
		Public Sub AppendOptionalAndCondition(ByVal poSQLFragment As CSQLParamString)
			If IsNothing(poSQLFragment) Then
				Return
			ElseIf poSQLFragment.SQL = "" Then
				Return
			End If

			AppendAndCondition(poSQLFragment.SQL)
		End Sub

#Region "Unsafe Overloads"
		''' <summary>
		''' Please use overload with CSQLParamValue.
		''' Append an optional condition to the SQL Where clause.  The optional condition is only applied
		''' if the optional value is not empty.  
		''' </summary>
		Public Sub AppendOptionalANDCondition(ByVal sField As String, ByVal sOptionalValue As String, ByVal sOperator As String)
			If sOptionalValue <> "" AndAlso sOptionalValue <> "''" AndAlso sOptionalValue <> "'%'" AndAlso sOptionalValue <> "'%%'" Then
				AppendAndCondition(sField & " " & sOperator & " " & sOptionalValue)
			End If
		End Sub


		''' <summary>
		''' Short hand overload that will let you specify a parameterized sql and safe values to use.
		''' </summary>
		Public Sub AppendAndCondition(ByVal psParameterizedSQL As String, ByVal ParamArray poValues() As CSQLParamValue)
			AppendAndCondition(CSQLParamString.Create(psParameterizedSQL, poValues))
		End Sub

#End Region

		''' <summary>
		''' Given an sql fragment, will append as additional requirement to the final sql.		
		''' ie: 
		''' dim oSQLFrag as CSQLParamString = CSQLParamString.Create ( "FName={0} or CoFName={0} and Index=0" , new CSQLParamValue ( FName.Text) )
		''' oWhere.AppendAndCondition ( oSQLFrag)
		''' </summary>
		Public Sub AppendAndCondition(ByVal poCondition As CSQLParamString)
			AppendAndCondition(poCondition.SQL)
		End Sub


		''' <summary>
		''' Unsafe overload to add sql conditions.  RECOMMENDED TO USE OTHER OVERLOADS!
		''' </summary>
		Public Sub AppendANDCondition(ByVal sCondition As String)
			AppendANDCondition_Ex(sCondition)
		End Sub

		Private Sub AppendANDCondition_Ex(ByVal sCondition As String)
			If m_sSQL = "" Then
				m_sSQL = sCondition
			Else
				m_sSQL = m_sSQL & " AND " & sCondition
			End If
		End Sub

		''' <summary>
		''' Adds an "MyColumn IN (....)" clause to our where builder.   If list passed in is empty, then sql will contain "AND 0=1" 
		''' causing empty set to be returned b/c logically speaking, we are saying we want all records that have field X in the 
		''' empty set.
		''' </summary>
		''' <param name="psFieldName">Column name</param>
		''' <param name="poValues">A collection of CSQLParamValue objects</param>	
		Public Overloads Sub AppendAndIn(ByVal psFieldName As String, ByVal poValues As ICollection(Of CSQLParamValue))
			If poValues.Count = 0 Then
				AppendANDCondition_Ex("0=1")
				Return
			End If

			Dim sValues As New List(Of String)(poValues.Count)
			For Each oParamValue As CSQLParamValue In poValues
				sValues.Add(oParamValue.SQLValue)
			Next

			AppendANDCondition_Ex(psFieldName & " IN (" & String.Join(", ", sValues.ToArray) & ")")
		End Sub

#Region "Obsolete AppendANDInClause"

		'if arr is empty, then sql will contain "AND 0=1" causeing empty set to be returned
		'b/c logically speaking, we are saying we want all records that have field X in the empty set
		Private Overloads Sub AppendANDInClause(ByVal sFieldName As String, ByVal arr As Generic.IList(Of String))

			If arr.Count = 0 Then
				AppendAndCondition("0=1")
				Return
			End If

			Dim s As New System.Text.StringBuilder()
			s.Append(sFieldName & " IN (")
			Dim i As Integer
			For i = 0 To arr.Count - 1
				Dim sItem As String = arr(i)
				s.Append(sItem)

				If i <> arr.Count - 1 Then
					s.Append(", ")
				End If
			Next

			s.Append(")")

			AppendAndCondition(s.ToString)
		End Sub

		
#End Region

		''' <summary>
		''' Adds a "MyColumn Not IN (...)" clause to our where builder. If arr is empty, then sql this routine will be skipped b/c 
		''' logically speaking, we are saying we want all records not in the empty set.
		''' </summary>
		''' <param name="psFieldName">Column name</param>
		''' <param name="poValues">A collection of CSQLParamValue objects</param>	
		Public Overloads Sub AppendAndNotIn(ByVal psFieldName As String, ByVal poValues As ICollection(Of CSQLParamValue))
			If poValues.Count = 0 Then
				Return
			End If

			Dim sValues As New List(Of String)(poValues.Count)
			For Each oParamValue As CSQLParamValue In poValues
				sValues.Add(oParamValue.SQLValue)
			Next

			AppendANDCondition_Ex(psFieldName & " NOT IN (" & String.Join(", ", sValues.ToArray) & ")")
		End Sub

#Region "Obsolete AppendAndNotInClause"


		'if arr is empty, then sql this routine will be skipped 
		'b/c logically speaking, we are saying we want all records not in the empty set    
		Private Overloads Sub AppendANDNotInClause(ByVal sFieldName As String, ByVal arr As Generic.IList(Of String))
			If arr.Count = 0 Then
				Return
			End If

			Dim s As New System.Text.StringBuilder()
			s.Append(sFieldName & " NOT IN (")
			Dim i As Integer
			For i = 0 To arr.Count - 1
				Dim sItem As String = arr(i)
				s.Append(sItem)

				If i <> arr.Count - 1 Then
					s.Append(", ")
				End If
			Next

			s.Append(")")

			AppendAndCondition(s.ToString)
		End Sub

		
#End Region


		Private m_sSQL As String

		Public Sub New()

		End Sub

		Public Sub New(ByVal poWhere As CSQLWhereStringBuilder)
			m_sSQL = poWhere.m_sSQL
		End Sub
	End Class
End Namespace