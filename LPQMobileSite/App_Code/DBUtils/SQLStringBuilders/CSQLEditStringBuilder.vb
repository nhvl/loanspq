Namespace DBUtils
	''' <summary>
	''' Generic SQL generation class that can generate either UPDATE or INSERT statements.
	''' </summary>	
	Public Class CSQLEditStringBuilder
		Public TableName As String
		Public Sub New(ByVal psTableName As String)
			TableName = psTableName
		End Sub
		Public Sub appendString(ByVal sField As String, ByVal sValue As String)
			_fields.Add(sField)
			_values.Add(sValue)
		End Sub

		Public Function getInsertSQL() As String

			Dim oInsertBuilder As New cSQLInsertStringBuilder(TableName)
			Dim i As Integer
			For i = 0 To _fields.Count - 1
				oInsertBuilder.appendString(CStr(_fields(i)), CStr(_values(i)))
			Next

			Return oInsertBuilder.SQL
		End Function

		Public Function getUpdateSQL(ByVal psWhereClauseWithWhere As String) As String

			Dim oUpdateBuilder As New cSQLUpdateStringBuilder(TableName, psWhereClauseWithWhere)
			Dim i As Integer
			For i = 0 To _fields.Count - 1
				oUpdateBuilder.AppendString(CStr(_fields(i)), CStr(_values(i)))
			Next

			Return oUpdateBuilder.SQL
		End Function

		Public Sub reset()
			_fields.Clear()
			_values.Clear()
		End Sub

		Private _fields As New ArrayList()
		Private _values As New ArrayList()
	End Class

End Namespace