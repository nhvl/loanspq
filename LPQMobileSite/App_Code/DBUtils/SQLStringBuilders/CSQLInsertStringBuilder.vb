Namespace DBUtils
	''' <summary>
	''' Builds an SQL INSERT statement in a structured manner.
	''' </summary>
	Public Class cSQLInsertStringBuilder
		Implements ISQLEditStringBuilder

		Public Sub New(ByVal sTable As String)
			_tableName = sTable
			m_oFields = New cSQLCommaStringBuilder()
			m_oValues = New cSQLCommaStringBuilder()
		End Sub

		Public Sub Reset() Implements ISQLEditStringBuilder.Reset
			m_oFields.Reset()
			m_oValues.Reset()
		End Sub

		Public ReadOnly Property SQL() As String Implements ISQLEditStringBuilder.SQL
			Get
				If _tableName = String.Empty Then
					Throw New Exception("SQL Insert builder requires you to specify table")
				End If

				Dim sSQL As New System.Text.StringBuilder()
				sSQL.Append("INSERT INTO ")
				sSQL.Append(_tableName)
				sSQL.Append(" (")
				sSQL.Append(m_oFields.SQL)
				sSQL.Append(") VALUES (")
				sSQL.Append(m_oValues.SQL)
				sSQL.Append(")")

				Return sSQL.ToString
			End Get
		End Property

		''' <summary>
		''' Depracated.  !! Please use AppendValue to avoid SQL injection problems!!!
		''' </summary>
		Public Sub appendString(ByVal sField As String, ByVal sValue As String) Implements ISQLEditStringBuilder.appendString
			m_oFields.AppendString(sField)
			m_oValues.AppendString(sValue)
		End Sub

		''' <summary>
		''' Appends a safe sql value object for use in sql generation.  Sample usage:
		''' oInsert.AppendValue ( "Amount", new CSQLParamValue (safedouble (Amount.text)) )
		''' </summary>
		Public Sub AppendValue(ByVal psField As String, ByVal poValue As CSQLParamValue) Implements ISQLEditStringBuilder.AppendValue
			m_oFields.AppendString(psField)
			m_oValues.AppendString(poValue.SQLValue)
		End Sub

		Private _tableName As String
		Private m_oFields As cSQLCommaStringBuilder
		Private m_oValues As cSQLCommaStringBuilder
	End Class

End Namespace