Namespace DBUtils
	Public Class cSQLCommaStringBuilder
		Public ReadOnly Property SQL() As String
			Get
				If m_sSQL Is Nothing Then
					Return ""
				Else
					Return m_sSQL.ToString
				End If

			End Get
		End Property

		Public ReadOnly Property IsEmpty() As Boolean
			Get
				Return m_sSQL Is Nothing
			End Get
		End Property

		Public Sub Reset()
			m_sSQL = Nothing
		End Sub

		Public Sub AppendString(ByVal sElem As String)
			If m_sSQL Is Nothing Then
				m_sSQL = New System.Text.StringBuilder()
				m_sSQL.Append(sElem)
			Else
				m_sSQL.Append(", ")
				m_sSQL.Append(sElem)
			End If
		End Sub

		Private m_sSQL As System.Text.StringBuilder
	End Class

End Namespace