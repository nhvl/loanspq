Namespace DBUtils

    '''<summary>
    ''' This class is pretty simple but will help avoid accidental deletion of all records by forcing you to enter a condition.
    ''' If you want to manually delete all records, then you will have to use the explicity facotry method
    '''</summary>
    Public Class CSQLDeleteStringBuilder

        ''' <summary>
        '''  Depracated.  !!! Please use overload that takes CSQLParamValue instead of string as value !!!
        ''' </summary>
        Public Sub New(ByVal sTable As String, ByVal sPrimaryKey As String, ByVal sValue As String)
            _WhereBuilder = New CSQLWhereStringBuilder()
            _WhereBuilder.AppendAndCondition(sPrimaryKey & "=" & sValue)
            _TableName = sTable
        End Sub


        Public Sub New(ByVal sTable As String, ByVal sPrimaryKey As String, ByVal poValue As CSQLParamValue)
            _WhereBuilder = New CSQLWhereStringBuilder()
            _WhereBuilder.AppendAndCondition(sPrimaryKey & "=" & poValue.SQLValue)
            _TableName = sTable
        End Sub

        Public Sub New(ByVal sTable As String, ByVal oWhereBuilder As CSQLWhereStringBuilder)
            _WhereBuilder = oWhereBuilder
            _TableName = sTable
        End Sub

        ''' <summary>
        ''' Constructor internally used by factory method.
        ''' </summary>
        Private Sub New(ByVal psTable As String)
            _TableName = psTable
        End Sub

        Private _AllowEmptyWhere As Boolean = False
        ''' <summary>
        ''' This is to handle the case where we KNOW we want to do a massive update without a WHERE condition :O
        ''' </summary>
        Public Shared Function CreateDeleteWithoutWhere(ByVal psTable As String) As CSQLDeleteStringBuilder
            Dim oResult As New CSQLDeleteStringBuilder(psTable)
            oResult._AllowEmptyWhere = True
            Return oResult
        End Function

		''' <summary>
		''' Indicates how many records to delete.   This is useful to perform deletes in small batches to avoid overloading 
		''' transaction logs.   By default, a -1 means to simply delete without specifying a max.
		''' </summary>		
		Public Top As Integer = -1

        Private _WhereBuilder As CSQLWhereStringBuilder
        Private _TableName As String
        Public ReadOnly Property SQL() As String
            Get

                Dim sWhere As String = ""

                If False = IsNothing(_WhereBuilder) Then
                    sWhere = _WhereBuilder.SQL
                End If

                If sWhere = "" And _AllowEmptyWhere = False Then
                    Throw New ApplicationException("Unexpected dangerous delete all records attempted!  ")
                End If

				Dim sResult As String = "DELETE "
				If Top >= 0 Then
					sResult &= "TOP (" & Top.ToString & ") "
				End If

				sResult &= "FROM " & _TableName & sWhere
				Return sResult
            End Get
        End Property

        Protected Overrides Sub Finalize()
            MyBase.Finalize()
        End Sub
    End Class


End Namespace