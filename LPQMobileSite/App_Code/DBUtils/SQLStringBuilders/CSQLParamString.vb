Namespace DBUtils
	''' <summary>
	''' This class (AKA Parameterized String) is designed to address unsafe use of sql string generation.  It can be used to generate fragments or 
	''' full sql that are safer to use. 
	''' 
	''' Example of bad practice (FName is prone to sql injection):  
	''' dim mySQL as string = String.format ("SELECT * From Users Where FName='{0}'" , FName.text)
	''' 
	''' Good statement :
	''' dim mySQL as string = CSQLParamString.Format _
	''' ("SELECT * From Users Where FName={0}" , new CSQLParam(FName.text))
	''' 
	''' Fragment Example:
	''' Dim oDateCondition as CSQLParamString= CSQLParamString.Create( "CreateDate > {0} ", New CSQLParam(date.now))
	''' </summary>
	''' <remarks>
	''' This class does not do sql syntax analysis!  It only helps generate safe sql parameters.
	''' The following syntax error (missing AND before Age) would not be caught:
	''' dim mySQL as string = CSQLParamString.Format _
	''' ("SELECT * From Users Where FName={0} Age=21" , new CSQLParam(FName.text))
	''' </remarks>
	Public Class CSQLParamString
		''' <summary>
		''' Private constructor to force clients to use factory method :P 
		''' </summary>
		Protected Sub New()
		End Sub

		''' <summary>
		''' Simplified routine for the simplest use case scenario of safe parameterized sql generation.  
		''' Routine is designed as a simple replacement of String.Format.
		''' Note client is still responsible for making sure sql structure is correct.
		''' Example:
		''' dim mySQL as string = CSQLParamString.Format _
		''' ("SELECT * From Users Where FName={0}" , new CSQLParam(FName.text))
		''' </summary>
		''' <remarks> 
		''' If you're going to be generating fragments for use in CSQLWhereStringBuilder, it's recommended to use th CSLParamString.Create 
		''' overload to use a more OO style.
		''' </remarks>
		Public Shared Function Format(ByVal psParameterizedSQL As String, ByVal ParamArray poValues() As CSQLParamValue) As String
			Dim leftCurlyReplace As String = System.Configuration.ConfigurationManager.AppSettings.Get("SQL_LEFT_CURLY_REPLACEMENT")
			If leftCurlyReplace = "" Then
				leftCurlyReplace = "{"
			End If
			Dim rightCurlyReplace As String = System.Configuration.ConfigurationManager.AppSettings.Get("SQL_RIGHT_CURLY_REPLACEMENT")
			If rightCurlyReplace = "" Then
				rightCurlyReplace = "}"
			End If

			Dim escapedValues(poValues.Length - 1) As String
			For i As Integer = 0 To poValues.Length - 1
				escapedValues(i) = poValues(i).SQLValue.Replace("{", leftCurlyReplace).Replace("}", rightCurlyReplace)
			Next
			Return String.Format(psParameterizedSQL, escapedValues)
		
		End Function

		''' <summary>
		''' Helps to generate a date range fragment that's safe for use in the DB.   If dates are invalid, then it will be omitted from result.  
		''' If both dates are invalid, a CSQLParamString will still be returned, but it's result will be blank.
		''' </summary>
		''' <remarks>This is a more OO replacement of std.SQLDateRange</remarks>
		Public Overloads Shared Function CreateDateFragment(ByVal psFieldName As String, ByVal psStartDate As String, ByVal psEndDate As String) As CSQLParamString
			Dim dStartDate As Date
			Dim dEndDate As Date

			If psStartDate = "" Then
				dStartDate = #1/1/1000#
			Else
				dStartDate = CDate(psStartDate)
			End If

			If psEndDate = "" Then
				dEndDate = #1/1/9999#
			Else
				dEndDate = CDate(psEndDate)
			End If

			Return CreateDateFragment(psFieldName, dStartDate, dEndDate)
		End Function

		'TODO:date stuff
		''' <summary>
		''' Helps to generate a date range fragment that's safe for use in the DB.   If dates are invalid, then it will be omitted from result.  
		''' If both dates are invalid, a CSQLParamString will still be returned, but it's result will be blank.
		''' </summary>
		''' <remarks>This is a more OO replacement of std.SQLDateRange</remarks>
		'Public Overloads Shared Function CreateDateFragment(ByVal sFieldName As String, Optional ByVal sStartDate As Date = #1/1/1000#, Optional ByVal sEndDate As Date = #1/1/9999#) As CSQLParamString


		'	Dim sSQL As String = ""
		'	Dim sDelim As String = "'" 'CStr(IIf(DBType = eDBTypes.eDBType_ACCESSS, "#", "'"))

		'	If sStartDate <> #1/1/1000# AndAlso CSQLParamValue.IsValidDate(sStartDate) Then
		'		sSQL = sFieldName & " >= " & sDelim & FormatDateTime(sStartDate, DateFormat.ShortDate) & sDelim
		'	End If

		'	If sEndDate <> #1/1/9999# AndAlso CSQLParamValue.IsValidDate(sEndDate) Then
		'		If Len(sSQL) > 0 Then
		'			sSQL = sSQL & " AND " & sFieldName & " < " & sDelim & FormatDateTime(sEndDate.AddDays(1), DateFormat.ShortDate) & sDelim
		'		Else
		'			sSQL = sSQL & sFieldName & " < " & sDelim & FormatDateTime(sEndDate.AddDays(1), DateFormat.ShortDate) & sDelim
		'		End If
		'	End If

		'	If Len(sSQL) > 0 Then
		'		sSQL = "(" & sSQL & ")"
		'	End If

		'	Dim oResult As New CSQLParamString()
		'	oResult.SQL = sSQL
		'	Return oResult
		'End Function

		Public SQL As String

		''' <summary>
		''' Same as Me.SQL
		''' </summary>		
		Public Overrides Function ToString() As String
			Return SQL
		End Function

		''' <summary>
		''' Factory method to create an object representing a safe parameterized sql statement.  Example:
		''' dim s as string = CSQLParamString.Create("SELECT * From Users Where FName like '{0}%'", New CSQLParamValue ( FName.text))
		''' </summary>
		''' <param name="psParameterizedSQL">A string that contains parameters that will </param>
		''' <param name="poValues"></param>
		Public Shared Function Create(ByVal psParameterizedSQL As String, ByVal ParamArray poValues() As CSQLParamValue) As CSQLParamString
			Dim oResult As New CSQLParamString()
			oResult.SQL = Format(psParameterizedSQL, poValues)
			Return oResult
		End Function

	End Class

End Namespace