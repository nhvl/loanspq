Namespace DBUtils

	''' <summary>
	''' This class assists in building the "SET field1='a', field2='b', field3='c'" part
	''' of an UPDATE SQL statement.  You are required to pass in a where string builder to avoid accidently updating more records
	''' than you'd want.  If you that is your intention, then you must use the explicit factory method to do so.
	''' </summary>	
	Public Class cSQLUpdateStringBuilder
		Implements ISQLEditStringBuilder

		<Obsolete("WTH is this for?")> Public Sub New2()
			m_oSQL = New cSQLCommaStringBuilder()
		End Sub

		''' <summary>
		''' Depracated.  !! Please use overload that uses CSQLWhereStringBuilder instead of simple string!! 
		''' </summary>		
		Public Sub New(ByVal sTableName As String, ByVal sConditionWithWhereClause As String)
			_tableName = sTableName
			_Condition = sConditionWithWhereClause
			m_oSQL = New cSQLCommaStringBuilder()
		End Sub

		Public Sub New(ByVal sTableName As String, ByVal poWhere As CSQLWhereStringBuilder)
			_tableName = sTableName
			_Condition = poWhere.SQL
			m_oSQL = New cSQLCommaStringBuilder()
		End Sub

		''' <summary>
		''' Private contructor used by factory method to construct massive update statements without WHERE clause.
		''' </summary>
		Protected Sub New(ByVal psTableName As String)
			_tableName = psTableName
			m_oSQL = New cSQLCommaStringBuilder()
		End Sub

		Private _AllowEmptyWhere As Boolean = False
		''' <summary>
		''' This is to handle the case where we KNOW we want to do a massive update without a WHERE condition :O
		''' </summary>
		Public Shared Function CreateUpdateWithoutWhere(ByVal psTable As String) As cSQLUpdateStringBuilder
			Dim oResult As New cSQLUpdateStringBuilder(psTable)
			oResult._AllowEmptyWhere = True
			Return oResult
		End Function


		Public ReadOnly Property SQL() As String Implements ISQLEditStringBuilder.SQL
			Get
				If _tableName = String.Empty Then
					Throw New Exception("SQL Update builder requires you to specify table name.")
				ElseIf (_Condition = String.Empty And _AllowEmptyWhere = False) Then
					Throw New Exception("SQL Update builder requires you to specify WHERE condition.")
				End If

				Dim sSQL As New System.Text.StringBuilder()

				sSQL.Append("UPDATE " & _tableName & " ")

				sSQL.Append("SET ")
				sSQL.Append(m_oSQL.SQL)

				If _Condition <> String.Empty Then
					sSQL.Append(" " & _Condition)
				End If
				Return sSQL.ToString
			End Get
		End Property

		Public ReadOnly Property IsEmpty() As Boolean
			Get
				Return m_oSQL.IsEmpty
			End Get
		End Property

		'this only resets all the appends
		Public Sub Reset() Implements ISQLEditStringBuilder.reset
			m_oSQL.Reset()
		End Sub

		Public Sub AppendString(ByVal sField As String, ByVal sValue As String) Implements ISQLEditStringBuilder.appendString
			m_oSQL.AppendString(sField & "=" & sValue)
		End Sub

		Public Sub appendvalue(ByVal psField As String, ByVal poValue As CSQLParamValue) Implements ISQLEditStringBuilder.AppendValue
			m_oSQL.AppendString(psField & "=" & poValue.SQLValue)
		End Sub

		Private _tableName As String
		Private _Condition As String
		Private m_oSQL As cSQLCommaStringBuilder
	End Class

End Namespace