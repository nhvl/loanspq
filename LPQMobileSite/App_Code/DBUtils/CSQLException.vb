'this is a general SQL exception independent of DB provider -- not to be confused as only used by CSQLDBUtils !  
'The default message will not contain the sql that's used to update the db (for security reasons).  The code can manually catch and query the SQL property for that info

Namespace DBUtils
    Public Class CSQLException
        Inherits System.Exception

        Public SQL As String

        'useful when we only want to trace the sql along w/ existing error 
        Public Sub New(ByVal poInnerException As System.Exception, ByVal psSQL As String)
            MyBase.New("SQL Exception: " & poInnerException.Message, poInnerException)
            SQL = psSQL
        End Sub

        'usefull if we just want to add our own note to an existing exception 
        Public Sub New(ByVal psErrorMessage As String, ByVal poInnerException As System.Exception)
            MyBase.New(psErrorMessage, poInnerException)
        End Sub

        'usefull if we just want to add our own note to an existing exception  along w/ an sql statement 
        Public Sub New(ByVal psErrorMessage As String, ByVal poInnerException As System.Exception, ByVal psSQL As String)
            MyBase.New(psErrorMessage, poInnerException)
            SQL = psSQL
		End Sub

		Public Sub New(ByVal psMessage As String, ByVal psSQL As String)
			MyBase.New(psMessage)
			SQL = psSQL
		End Sub

		Public Sub New(ByVal psMessage As String)
			MyBase.New(psMessage)
		End Sub


		'this is provided as convenience function b/c many times we use ToString on exception in traces 
		Public Overrides Function ToString() As String
			Dim sResult As String = MyBase.ToString
			If SQL <> "" Then
				sResult += vbCrLf & " SQL: " & Me.SQL
			End If

			Return sResult
		End Function


	End Class
End Namespace