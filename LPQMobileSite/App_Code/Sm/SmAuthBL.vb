﻿Imports System.Data
Imports PayPal.Api
Imports DBUtils
Imports Microsoft.VisualBasic
Imports LPQMobile.Utils
Imports System.Xml
Imports System.Web.Script.Serialization
Imports Sm
Imports System.Web
Imports System.Text.RegularExpressions
Imports System.Configuration

Public Class SmAuthBL
	Protected LPQMOBILE_CONNECTIONSTRING As String = System.Web.Configuration.WebConfigurationManager.ConnectionStrings("LPQMOBILE_CONNECTIONSTRING").ConnectionString

	Private Const USERS_TABLE As String = "Users"
	Private Const LPQ_USERS_TABLE As String = "LPQUsers"
	Private Const USER_ACTIVATE_TOKENS_TABLE As String = "UserActivateTokens"
	Private Const SSOEMAIL_VALIDATION_TOKENS_TABLE As String = "SSOEmailValidationTokens"
	Private Const USERROLES_TABLE As String = "UserRoles"
	Private Const USERCHANGEPASSWORDLOGS_TABLE = "UserChangePasswordLogs"
	Private Const SSOSESSIONS_TABLE = "SSOSessions"
	Private Const SSOTOKENS_TABLE = "SSOTokens"
	Private _log As log4net.ILog = log4net.LogManager.GetLogger(GetType(SmAuthBL))

	Public Function GetAllUsers(ByRef paging As SmPagingInfoModel, filter As SmUserListFilter) As List(Of SmUser)
		Dim result As New List(Of SmUser)
		Dim oData As New DataTable()
		Dim sSQL As String = "SELECT * FROM " & USERS_TABLE
		Dim oWhere As New CSQLWhereStringBuilder()
		oWhere.AppendAndCondition("SSOWorkspaceLoanOfficerCode IS NULL")	' do not include SSO Users
		Dim excludedDeletedItem As Boolean = True
		If filter IsNot Nothing Then
			If filter.Roles IsNot Nothing AndAlso filter.Roles.Any() Then
				Dim list As New List(Of CSQLParamValue)
				For Each role As SmSettings.Role In filter.Roles
					list.Add(New CSQLParamValue(role.ToString()))
				Next
				oWhere.AppendAndIn("Role", list)
			End If
			If Not String.IsNullOrEmpty(filter.Keyword) Then
				oWhere.AppendAndCondition("(FirstName + '$' + LastName + '$' + Email + '$' + Phone + '$' + Role + '$' + CONVERT(NVARCHAR(40), UserID)) LIKE {0}", New CSQLParamValue("%" & filter.Keyword & "%"))
			End If
			If filter.Status IsNot Nothing AndAlso filter.Status.Any() Then
				Dim list As New List(Of CSQLParamValue)
				For Each status As SmSettings.Role In filter.Status
					list.Add(New CSQLParamValue(status))
				Next
				oWhere.AppendAndIn("Status", list)
				excludedDeletedItem = False
			End If
		End If
		If excludedDeletedItem Then
			oWhere.AppendAndCondition("Status <> {0}", New CSQLParamValue(SmSettings.UserStatus.Deleted))	'Deleted
		End If
		Dim oOrder As String = " "
		If Not String.IsNullOrEmpty(paging.SortColumn) Then
			oOrder = oOrder & "ORDER BY " & paging.SortColumn & " " & paging.SortDirection
		Else
			oOrder = oOrder & "ORDER BY CreatedDate, Email "
		End If
		Dim oDB As New CSQLDBUtils(LPQMOBILE_CONNECTIONSTRING)
		Try
			oData = oDB.getDataTable(sSQL & oWhere.SQL & oOrder)
			If oData IsNot Nothing AndAlso oData.Rows.Count > 0 Then
				For Each row As DataRow In oData.Rows
					Dim item As New SmUser()
					item.UserID = Common.SafeGUID(row("UserID"))
					item.Email = Common.SafeString(row("Email"))
					item.Password = Common.SafeString(row("Password"))
					item.Salt = Common.SafeString(row("Salt"))
					item.CreatedDate = Common.SafeDate(row("CreatedDate"))
					If Not row.IsNull("LastLoginDate") Then
						item.LastLoginDate = Common.SafeDate(row("LastLoginDate"))
					End If
					item.Status = DirectCast(Common.SafeInteger(row("Status")), SmSettings.UserStatus)
					If Not row.IsNull("Role") AndAlso Not String.IsNullOrEmpty(Common.SafeString(row("Role"))) Then
						item.Role = CType([Enum].Parse(GetType(SmSettings.Role), Common.SafeString(row("Role"))), SmSettings.Role)

					End If
					item.FirstName = Common.SafeString(row("FirstName"))
					item.LastName = Common.SafeString(row("LastName"))
					item.Phone = Common.SafeString(row("Phone"))
					item.Avatar = Common.SafeString(row("Avatar"))
					item.LoginFailedCount = Common.SafeInteger(row("LoginFailedCount"))
					item.ExpireDays = Common.SafeInteger(row("ExpireDays"))
					result.Add(item)
				Next
			End If
			paging.TotalRecord = result.Count
			If paging.PageIndex > 0 AndAlso paging.PageSize > 0 Then
				Dim originalResult = result
				result = originalResult.Skip((paging.PageIndex - 1) * paging.PageSize).Take(paging.PageSize).ToList()
				If Not result.Any() Then
					paging.PageIndex = paging.PageIndex - 1
					result = originalResult.Skip((paging.PageIndex - 1) * paging.PageSize).Take(paging.PageSize).ToList()
				End If
			End If
		Catch ex As Exception
			_log.Error("Error while GetAllUsers", ex)
		Finally
			oDB.closeConnection()
		End Try
		Return result
	End Function

	Public Function GetUserByID(ID As Guid) As SmUser
		Dim result As SmUser
		Dim oData As DataTable
		Dim sSQL As String = "SELECT * FROM " & USERS_TABLE
		Dim oWhere As New CSQLWhereStringBuilder()
		oWhere.AppendAndCondition("UserID={0}", New CSQLParamValue(ID))
		oWhere.AppendAndCondition("Status<>{0} ", New CSQLParamValue(SmSettings.UserStatus.Deleted))	'Deleted
		Dim oDB As New CSQLDBUtils(LPQMOBILE_CONNECTIONSTRING)
		Try
			oData = oDB.getDataTable(sSQL & oWhere.SQL)
			If oData IsNot Nothing AndAlso oData.Rows.Count > 0 Then
				Dim row As DataRow = oData.Rows(0)
				result = New SmUser()
				result.UserID = Common.SafeGUID(row("UserID"))
				result.Email = Common.SafeString(row("Email"))
				result.Password = Common.SafeString(row("Password"))
				result.Salt = Common.SafeString(row("Salt"))
				result.CreatedDate = Common.SafeDate(row("CreatedDate"))
				If Not row.IsNull("LastLoginDate") Then
					result.LastLoginDate = Common.SafeDate(row("LastLoginDate"))
				End If
				result.Status = DirectCast(Common.SafeInteger(row("Status")), SmSettings.UserStatus)
				If Not row.IsNull("Role") AndAlso Not String.IsNullOrEmpty(Common.SafeString(row("Role"))) Then
					result.Role = CType([Enum].Parse(GetType(SmSettings.Role), Common.SafeString(row("Role"))), SmSettings.Role)

				End If
				result.FirstName = Common.SafeString(row("FirstName"))
				result.LastName = Common.SafeString(row("LastName"))
				result.Phone = Common.SafeString(row("Phone"))
				result.Avatar = Common.SafeString(row("Avatar"))
				result.LoginFailedCount = Common.SafeInteger(row("LoginFailedCount"))
				result.ExpireDays = Common.SafeInteger(row("ExpireDays"))
				result.SSOWorkspaceLoanOfficerCode = Common.SafeString(row("SSOWorkspaceLoanOfficerCode"))
			End If
		Catch ex As Exception
			_log.Error("Error while GetUserByID", ex)
		Finally
			oDB.closeConnection()
		End Try
		Return result
	End Function

	Public Function GetSSOSessionData(ssoSessionID As Guid) As SmSSOSession
		Dim result As SmSSOSession
		Dim oData As DataTable
		Dim sSQL As String = "SELECT * FROM " & SSOSESSIONS_TABLE
		Dim oWhere As New CSQLWhereStringBuilder()
		oWhere.AppendAndCondition("SSOSessionID={0}", New CSQLParamValue(ssoSessionID))
		Dim oDB As New CSQLDBUtils(LPQMOBILE_CONNECTIONSTRING)
		Try
			oData = oDB.getDataTable(sSQL & oWhere.SQL)
			If oData IsNot Nothing AndAlso oData.Rows.Count > 0 Then
				Dim row As DataRow = oData.Rows(0)
				result = New SmSSOSession()
				result.SSOSesionID = Common.SafeGUID(row("SSOSessionID"))
				result.Email = Common.SafeString(row("Email"))
				result.OrganizationCode = Common.SafeString(row("OrganizationCode"))
				result.LenderCode = Common.SafeString(row("LenderCode"))
				result.LoanOfficerCode = Common.SafeString(row("LoanOfficerCode"))
				result.Login = Common.SafeString(row("Login"))
				result.CreatedDate = Common.SafeDate(row("CreatedDate"))
				result.FirstName = Common.SafeString(row("FirstName"))
				result.LastName = Common.SafeString(row("LastName"))
				result.MiddleName = Common.SafeString(row("MiddleName"))
				result.WorkingUrl = Common.SafeString(row("WorkingUrl"))
				result.AccessRights = New JavaScriptSerializer().Deserialize(Of SmSSOAccessRight)(Common.SafeString(row("AccessRights")))
			End If
		Catch ex As Exception
			_log.Error("Error while GetSSOSessionData", ex)
		Finally
			oDB.closeConnection()
		End Try
		Return result
	End Function
	Public Function GetCurrentUserFromContext(context As HttpContext) As SmUser
		Dim userName = context.User.Identity.Name
		If userName.Contains("@") Then 'email
			Return GetUserByEmail(context.User.Identity.Name)
		ElseIf Not String.IsNullOrEmpty(userName) Then 'For SSO, userName is SSOSessionID (GUID)
			Dim ssoSession = GetSSOSessionData(Guid.Parse(userName))
			If ssoSession IsNot Nothing Then
				Return GetUserBySSOWorkspaceLoanOfficerCode(ssoSession.WorkspaceLoanOfficerCode)
			End If
			Return New SmUser()
		Else
			Return New SmUser()
		End If
	End Function

	Public Function GetUserByEmail(email As String) As SmUser
		Dim result As SmUser = Nothing
		Dim oData As DataTable
		Dim sSQL As String = "SELECT * FROM " & USERS_TABLE
		Dim oWhere As New CSQLWhereStringBuilder()
		oWhere.AppendAndCondition("UPPER(Email)=UPPER({0})", New CSQLParamValue(email))
		oWhere.AppendAndCondition("Status<>{0} ", New CSQLParamValue(SmSettings.UserStatus.Deleted))	'Deleted
		Dim oDB As New CSQLDBUtils(LPQMOBILE_CONNECTIONSTRING)
		Try
			oData = oDB.getDataTable(sSQL & oWhere.SQL)
			If oData IsNot Nothing AndAlso oData.Rows.Count > 0 Then
				Dim row As DataRow = oData.Rows(0)
				result = New SmUser()
				result.UserID = Common.SafeGUID(row("UserID"))
				result.Email = Common.SafeString(row("Email"))
				result.Password = Common.SafeString(row("Password"))
				result.Salt = Common.SafeString(row("Salt"))
				result.CreatedDate = Common.SafeDate(row("CreatedDate"))
				If Not row.IsNull("LastLoginDate") Then
					result.LastLoginDate = Common.SafeDate(row("LastLoginDate"))
				End If
				result.Status = DirectCast(Common.SafeInteger(row("Status")), SmSettings.UserStatus)
				If Not row.IsNull("Role") AndAlso Not String.IsNullOrEmpty(Common.SafeString(row("Role"))) Then
					result.Role = CType([Enum].Parse(GetType(SmSettings.Role), Common.SafeString(row("Role"))), SmSettings.Role)

				End If
				result.FirstName = Common.SafeString(row("FirstName"))
				result.LastName = Common.SafeString(row("LastName"))
				result.Phone = Common.SafeString(row("Phone"))
				result.Avatar = Common.SafeString(row("Avatar"))
				result.LoginFailedCount = Common.SafeInteger(row("LoginFailedCount"))
				result.ExpireDays = Common.SafeInteger(row("ExpireDays"))
			End If
		Catch ex As Exception
			_log.Error("Error while GetUserByEmail", ex)
		Finally
			oDB.closeConnection()
		End Try
		Return result
	End Function
	Private Shared _CacheLock_LENDERS_BY_SSO_SESSION_DATA As New Object
	Public Function GetRolesOfCurrentUser(context As HttpContext) As List(Of String)
		If context.User.Identity.Name.Contains("@") Then  'for direct login user
			Return GetRolesByEmail(context.User.Identity.Name)
		End If

		'section below is for lpq sso user
		Dim availableRoles As New List(Of String)
		Dim ssoSessionData = GetSSOSessionData(Guid.Parse(context.User.Identity.Name))
		If ssoSessionData Is Nothing Then Return availableRoles

		'Initially, SSO user is assigned the role SSOUser by default to help be able to explore APM homepage but without other roles,the lender list will be empty
		availableRoles.Add(SmSettings.Role.SSOUser.ToString())

		'due to performance issue, cache the result of GetLendersBySSOLession to reduce DB call and parse data - this action make CPU usage increase

		Dim workingDomainMatch As Match = Regex.Match(ssoSessionData.WorkingUrl, SmSettings.extractDomainRegexPattern, RegexOptions.IgnoreCase)
		If workingDomainMatch.Success = False Then Return availableRoles

		'this cache will be removed if there are any changes on domain, lendercode, orgcode. For now, these can only be changed in legacy configs and clone lender action
		Dim cacheKey As String = "SSO_AVAILABLE_ROLES_" & workingDomainMatch.Groups(1).Value.NullSafeToUpper_ & "_" & ssoSessionData.LenderCode.NullSafeToUpper_() & "_" & ssoSessionData.OrganizationCode.NullSafeToUpper_()

		Dim cachedData As List(Of String) = CType(HttpContext.Current.Cache.Get(cacheKey), List(Of String))
		If cachedData IsNot Nothing Then
			availableRoles.AddRange(cachedData)
			Return availableRoles
		End If

		SyncLock _CacheLock_LENDERS_BY_SSO_SESSION_DATA
			cachedData = CType(HttpContext.Current.Cache.Get(cacheKey), List(Of String))
			If cachedData Is Nothing Then
				'_log.Error("GetRolesOfCurrentUser from DB")
				Dim canAccessLenders = New SmBL().GetLendersBySSOSession(New SmPagingInfoModel(), Nothing, ssoSessionData.WorkingUrl, ssoSessionData.LenderCode, ssoSessionData.OrganizationCode, pbDecrypt:=False, portalListIncluded:=False)
				cachedData = New List(Of String)
				If canAccessLenders IsNot Nothing AndAlso canAccessLenders.Count > 0 Then

					cachedData.Add(SmSettings.Role.LenderAdmin.ToString())
					For Each lender In canAccessLenders
						Dim role = String.Format("LenderScope${0}${1}", SmSettings.Role.LenderAdmin.ToString(), lender.LenderID.ToString())
						If Not cachedData.Contains(role) Then
							cachedData.Add(role)
						End If
					Next
				End If
				availableRoles.AddRange(cachedData)
				HttpContext.Current.Cache.Remove(cacheKey)
				HttpContext.Current.Cache.Insert(cacheKey, cachedData, Nothing, Caching.Cache.NoAbsoluteExpiration, New TimeSpan(22, 0, 0))
				'_log.ErrorFormat("WRITE GetRolesOfCurrentUser {0}", cacheKey)
			Else
				availableRoles.AddRange(cachedData)
				'_log.ErrorFormat("READ GetRolesOfCurrentUser from cache {0}", cacheKey)
			End If
		End SyncLock


		Return availableRoles

	End Function

	Public Function GetRolesByEmail(email As String) As List(Of String)
		Dim result As List(Of String) = Nothing
		Dim oData As DataTable
		Dim sSQL As String = String.Format("select U.Role as GlobalRole, COALESCE(UR.Role, '') as LocalRole, UR.LenderID, LenderConfigID, UR.VendorID from {0} U left join {1} UR on U.UserID = UR.UserID ", USERS_TABLE, USERROLES_TABLE)
		Dim oWhere As New CSQLWhereStringBuilder()
		oWhere.AppendAndCondition("UPPER(Email)=UPPER({0})", New CSQLParamValue(email))
		oWhere.AppendAndCondition("U.Status = {0}", New CSQLParamValue(SmSettings.UserStatus.Active))
		oWhere.AppendAndCondition("COALESCE(UR.Status, {0}) = {0}", New CSQLParamValue(SmSettings.UserRoleStatus.Active))
		Dim oDB As New CSQLDBUtils(LPQMOBILE_CONNECTIONSTRING)
		Try
			oData = oDB.getDataTable(sSQL & oWhere.SQL)
			If oData IsNot Nothing AndAlso oData.Rows.Count > 0 Then
				result = New List(Of String)
				For Each row In oData.Rows
					If Not result.Any(Function(p) p = Common.SafeString(row("GlobalRole"))) Then
						result.Add(Common.SafeString(row("GlobalRole")))
					End If
					If Not String.IsNullOrEmpty(Common.SafeString(row("LocalRole"))) Then
						If Not result.Any(Function(p) p = Common.SafeString(row("LocalRole"))) Then
							result.Add(Common.SafeString(row("LocalRole")))
						End If
						Dim lenderID = Common.SafeGUID(row("LenderID"))
						If lenderID <> Guid.Empty Then
							If Common.SafeString(row("LocalRole")) = SmSettings.Role.LenderAdmin.ToString() AndAlso Not result.Any(Function(p) p = String.Format("LenderScope${0}${1}", Common.SafeString(row("LocalRole")), lenderID.ToString())) Then
								result.Add(String.Format("LenderScope${0}${1}", Common.SafeString(row("LocalRole")), lenderID.ToString()))
							End If

							Dim lenderConfigID = Common.SafeGUID(row("LenderConfigID"))
							If lenderConfigID <> Guid.Empty Then
								If Common.SafeString(row("LocalRole")) = SmSettings.Role.PortalAdmin.ToString() AndAlso Not result.Any(Function(p) p = String.Format("PortalScope${0}${1}${2}", Common.SafeString(row("LocalRole")), lenderID.ToString(), lenderConfigID.ToString())) Then
									result.Add(String.Format("PortalScope${0}${1}${2}", Common.SafeString(row("LocalRole")), lenderID.ToString(), lenderConfigID.ToString()))
								End If
								If Common.SafeString(row("LocalRole")) = SmSettings.Role.VendorGroupAdmin.ToString() AndAlso Not result.Any(Function(p) p = String.Format("PortalScope${0}${1}${2}", Common.SafeString(row("LocalRole")), lenderID.ToString(), lenderConfigID.ToString())) Then
									result.Add(String.Format("PortalScope${0}${1}${2}", Common.SafeString(row("LocalRole")), lenderID.ToString(), lenderConfigID.ToString()))
								End If
								Dim vendorID As String = Common.SafeStripHtmlString(row("VendorID"))
								If Not String.IsNullOrEmpty(vendorID) Then
									If (Common.SafeString(row("LocalRole")) = SmSettings.Role.VendorAdmin.ToString() OrElse Common.SafeString(row("LocalRole")) = SmSettings.Role.VendorUser.ToString()) AndAlso Not result.Any(Function(p) p = String.Format("VendorScope${0}${1}${2}${3}", Common.SafeString(row("LocalRole")), lenderID.ToString(), lenderConfigID.ToString(), vendorID)) Then
										result.Add(String.Format("VendorScope${0}${1}${2}${3}", Common.SafeString(row("LocalRole")), lenderID.ToString(), lenderConfigID.ToString(), vendorID))
									End If
								End If
							End If
						End If
					End If
				Next
			End If
		Catch ex As Exception
			_log.Error("Error while GetRolesByEmail", ex)
		Finally
			oDB.closeConnection()
		End Try
		Return result
	End Function

	Public Function AddUser(user As SmUser, createdByUserId As Guid, ipAddress As String, token As String) As String
		Dim result As String = "error"
		Dim oDB As New CSQLDBUtils(LPQMOBILE_CONNECTIONSTRING)
		Try
			If user.ExpireDays = 0 Then
				user.ExpireDays = 120 ' by default
			End If
			Dim oInsert As New cSQLInsertStringBuilder(USERS_TABLE)
			oInsert.AppendValue("Email", New CSQLParamValue(Common.SafeString(user.Email)))
			oInsert.AppendValue("Status", New CSQLParamValue(SmSettings.UserStatus.InActive)) 'Pending
			If user.Role <> SmSettings.Role.LegacyOperator AndAlso user.Role <> SmSettings.Role.Officer AndAlso user.Role <> SmSettings.Role.SuperOperator AndAlso user.Role <> SmSettings.Role.Accountant Then
				user.Role = SmSettings.Role.User
			End If
			oInsert.AppendValue("Role", New CSQLParamValue(user.Role.ToString()))
			oInsert.AppendValue("FirstName", New CSQLParamValue(Common.SafeString(user.FirstName)))
			oInsert.AppendValue("LastName", New CSQLParamValue(Common.SafeString(user.LastName)))
			oInsert.AppendValue("Phone", New CSQLParamValue(Common.SafeString(user.Phone)))
			oInsert.AppendValue("Avatar", New CSQLParamValue(Common.ExtractAvatarUrl(user.Avatar)))
			oInsert.AppendValue("ExpireDays", New CSQLParamValue(Common.SafeInteger(user.ExpireDays)))
			oInsert.AppendValue("UserID", New CSQLParamValue(user.UserID))
			oInsert.AppendValue("CreatedDate", New CSQLParamValue(Now))
			oDB.executeNonQuery(oInsert.SQL, False)
			'Insert token
			Dim otInsert As New cSQLInsertStringBuilder(USER_ACTIVATE_TOKENS_TABLE)
			otInsert.AppendValue("Token", New CSQLParamValue(token))
			otInsert.AppendValue("Email", New CSQLParamValue(Common.SafeString(user.Email)))
			otInsert.AppendValue("UserID", New CSQLParamValue(user.UserID))
			otInsert.AppendValue("Status", New CSQLParamValue("1"))
			oDB.executeNonQuery(otInsert.SQL, False)
			Dim actionLogBL As New SmActionLogBL()
			actionLogBL.WriteActionLog(New SmActionLog() With {.Action = SmActionLogAction.ADD_USER, .Category = SmActionLogCategory.USERS_MANIPULATION, .CreatedByUserID = createdByUserId, .IpAddress = ipAddress, .ObjectID = user.UserID.ToString(), .ActionNote = String.Format("Add new user {0} with role {1}", user.Email, user.Role.GetEnumDescription())})
			result = "ok"
		Catch ex As Exception
			_log.Error("Error while AddUser", ex)
			result = "error"
		Finally
			oDB.closeConnection()
		End Try
		Return result
	End Function

	Public Function DeleteUser(user As SmUser, createdByUserId As Guid, ipAddress As String) As String
		Dim result As String = ""
		Dim oDB As New CSQLDBUtils(LPQMOBILE_CONNECTIONSTRING)
		Try
			Dim oWhere As New CSQLWhereStringBuilder()
			'oWhere.AppendAndCondition("UPPER(Email)=UPPER({0})", New CSQLParamValue(user.Email))
			oWhere.AppendAndCondition("UserID={0}", New CSQLParamValue(user.UserID)) '
			Dim oUpdate As New cSQLUpdateStringBuilder(USERS_TABLE, oWhere)
			oUpdate.appendvalue("Status", New CSQLParamValue(SmSettings.UserStatus.Deleted))
			oDB.executeNonQuery(oUpdate.SQL, True)

			''**
			'also need to remove any links to this user from USERROLES_TABLE
			Dim oDelete As New CSQLDeleteStringBuilder(USERROLES_TABLE, oWhere)
			oDB.executeNonQuery(oDelete.SQL, True)

			If user.Status = SmSettings.UserStatus.InActive Then
				oUpdate = New cSQLUpdateStringBuilder(USER_ACTIVATE_TOKENS_TABLE, oWhere)
				oUpdate.appendvalue("Status", New CSQLParamValue(SmSettings.TokenStatus.Deleted))
				oDB.executeNonQuery(oUpdate.SQL, True)
			End If

			Dim actionLogBL As New SmActionLogBL()
			actionLogBL.WriteActionLog(New SmActionLog() With {.Action = SmActionLogAction.DELETE_USER, .Category = SmActionLogCategory.USERS_MANIPULATION, .CreatedByUserID = createdByUserId, .IpAddress = ipAddress, .ObjectID = user.UserID.ToString(), .ActionNote = String.Format("Delete a user with email {0}", user.Email)})
			result = "ok"
		Catch ex As Exception
			_log.Error("Error while DeleteUser", ex)
			result = "error"
		Finally
			oDB.commitTransactionIfOpen()
			oDB.closeConnection()
		End Try
		Return result
	End Function

	Public Function AddSSOEmailValidationTokenRecord(user As SmUser, createdByUserId As Guid, ipAddress As String, token As String) As String
		Dim result As String = ""
		Dim oDB As New CSQLDBUtils(LPQMOBILE_CONNECTIONSTRING)
		Try
			'Insert token
			Dim otInsert As New cSQLInsertStringBuilder(SSOEMAIL_VALIDATION_TOKENS_TABLE)
			otInsert.AppendValue("Token", New CSQLParamValue(token))
			otInsert.AppendValue("Email", New CSQLParamValue(Common.SafeString(user.Email)))
			otInsert.AppendValue("UserID", New CSQLParamValue(user.UserID))
			otInsert.AppendValue("Status", New CSQLParamValue(SmSettings.TokenStatus.Waiting))
			oDB.executeNonQuery(otInsert.SQL, False)
			Dim actionLogBL As New SmActionLogBL()
			actionLogBL.WriteActionLog(New SmActionLog() With {.Action = SmActionLogAction.CREATE_SSOEMAIL_VALIDATION_AND_MLU_SSO_REQUEST, .Category = SmActionLogCategory.USERS_MANIPULATION, .CreatedByUserID = createdByUserId, .IpAddress = ipAddress, .ObjectID = user.UserID.ToString(), .ActionNote = String.Format("Do SSO email confirmation and MLU SSO request {0}", user.SSOEmail)})
			result = "ok"
		Catch ex As Exception
			_log.Error("Error while AddSSOEmailValidationTokenRecord", ex)
			result = "error"
		Finally
			oDB.closeConnection()
		End Try
		Return result
	End Function
	Public Function AddUserActivateTokenRecord(user As SmUser, createdByUserId As Guid, ipAddress As String, token As String) As String
		Dim result As String = ""
		Dim oDB As New CSQLDBUtils(LPQMOBILE_CONNECTIONSTRING)
		Try
			'Insert token
			Dim otInsert As New cSQLInsertStringBuilder(USER_ACTIVATE_TOKENS_TABLE)
			otInsert.AppendValue("Token", New CSQLParamValue(token))
			otInsert.AppendValue("Email", New CSQLParamValue(Common.SafeString(user.Email)))
			otInsert.AppendValue("UserID", New CSQLParamValue(user.UserID))
			otInsert.AppendValue("Status", New CSQLParamValue("1"))
			oDB.executeNonQuery(otInsert.SQL, False)
			Dim actionLogBL As New SmActionLogBL()
			actionLogBL.WriteActionLog(New SmActionLog() With {.Action = SmActionLogAction.CREATE_FORGOT_PASSWORD_REQUEST, .Category = SmActionLogCategory.USERS_MANIPULATION, .CreatedByUserID = createdByUserId, .IpAddress = ipAddress, .ObjectID = user.UserID.ToString(), .ActionNote = String.Format("Process forgot password request of user with email {0}", user.Email)})
			result = "ok"
		Catch ex As Exception
			_log.Error("Error while AddUserActivateTokenRecord", ex)
			result = "error"
		Finally
			oDB.closeConnection()
		End Try
		Return result
	End Function

	Public Function UpdateUser(user As SmUser, createdByUserId As Guid, ipAddress As String) As String
		Dim result As String = ""
		Dim oDB As New CSQLDBUtils(LPQMOBILE_CONNECTIONSTRING)
		Try
			Dim oWhere As New CSQLWhereStringBuilder()
			oWhere.AppendAndCondition("UserID={0}", New CSQLParamValue(user.UserID))

			Dim oUpdate As New cSQLUpdateStringBuilder(USERS_TABLE, oWhere)
			oUpdate.appendvalue("Email", New CSQLParamValue(user.Email))
			oUpdate.appendvalue("FirstName", New CSQLParamValue(user.FirstName))
			oUpdate.appendvalue("LastName", New CSQLParamValue(user.LastName))
			oUpdate.appendvalue("Phone", New CSQLParamValue(user.Phone))
			oUpdate.appendvalue("Avatar", New CSQLParamValue(Common.ExtractAvatarUrl(user.Avatar)))

			If user.ExpireDays = 0 Then
				oUpdate.appendvalue("ExpireDays", New CSQLParamValue(120))
			Else
				oUpdate.appendvalue("ExpireDays", New CSQLParamValue(user.ExpireDays))
			End If

			oUpdate.appendvalue("Role", New CSQLParamValue(user.Role.ToString()))
			oDB.executeNonQuery(oUpdate.SQL, False)

			Dim actionLogBL As New SmActionLogBL()
			actionLogBL.WriteActionLog(New SmActionLog() With {.Action = SmActionLogAction.EDIT_USER, .Category = SmActionLogCategory.USERS_MANIPULATION, .CreatedByUserID = createdByUserId, .IpAddress = ipAddress, .ObjectID = user.UserID.ToString(), .ActionNote = String.Format("Edit user info of {0}", user.Email)})
			result = "ok"
		Catch ex As Exception
			_log.Error("Error while UpdateUser", ex)
			result = "error"
		Finally
			oDB.closeConnection()
		End Try
		Return result
	End Function

	Public Function ChangePassword(userId As Guid, password As String, salt As String, createdByUserId As Guid, ipAddress As String) As String
		Dim result As String = ""
		Dim oDB As New CSQLDBUtils(LPQMOBILE_CONNECTIONSTRING)
		Try
			Dim oWhere As New CSQLWhereStringBuilder()
			oWhere.AppendAndCondition("UserID={0}", New CSQLParamValue(userId))

			Dim oUpdate As New cSQLUpdateStringBuilder(USERS_TABLE, oWhere)
			oUpdate.appendvalue("Password", New CSQLParamValue(password))
			oUpdate.appendvalue("Salt", New CSQLParamValue(salt))
			oDB.executeNonQuery(oUpdate.SQL, True)

			Dim oInsert As New cSQLInsertStringBuilder(USERCHANGEPASSWORDLOGS_TABLE)
			oInsert.AppendValue("ID", New CSQLParamValue(Guid.NewGuid()))
			oInsert.AppendValue("UserID", New CSQLParamValue(userId))
			oInsert.AppendValue("CreatedDate", New CSQLParamValue(Now))
			oInsert.AppendValue("Password", New CSQLParamValue(password))
			oInsert.AppendValue("Salt", New CSQLParamValue(salt))
			oDB.executeNonQuery(oInsert.SQL, True)

			Dim actionLogBL As New SmActionLogBL()
			actionLogBL.WriteActionLog(New SmActionLog() With {.Action = SmActionLogAction.CHANGE_PASSWORD, .Category = SmActionLogCategory.USERS_MANIPULATION, .CreatedByUserID = createdByUserId, .IpAddress = ipAddress, .ObjectID = userId.ToString(), .ActionNote = String.Format("Change password")})
			result = "ok"
		Catch ex As Exception
			_log.Error("Error while ChangePassword", ex)
			result = "error"
		Finally
			oDB.commitTransactionIfOpen()
			oDB.closeConnection()
		End Try
		Return result
	End Function

	Public Function UnlockUser(user As SmUser, createdByUserId As Guid, ipAddress As String) As String
		Dim result As String = ""
		Dim oDB As New CSQLDBUtils(LPQMOBILE_CONNECTIONSTRING)
		Try
			Dim oWhere As New CSQLWhereStringBuilder()
			oWhere.AppendAndCondition("UserID={0}", New CSQLParamValue(user.UserID))

			Dim oUpdate As New cSQLUpdateStringBuilder(USERS_TABLE, oWhere)
			oUpdate.appendvalue("LoginFailedCount", New CSQLParamValue(user.LoginFailedCount))
			If user.LastLoginDate.HasValue Then
				oUpdate.appendvalue("LastLoginDate", New CSQLParamValue(user.LastLoginDate.Value))
			End If
			oDB.executeNonQuery(oUpdate.SQL, False)

			Dim actionLogBL As New SmActionLogBL()
			actionLogBL.WriteActionLog(New SmActionLog() With {.Action = SmActionLogAction.UNLOCK_USER, .Category = SmActionLogCategory.USERS_MANIPULATION, .CreatedByUserID = createdByUserId, .IpAddress = ipAddress, .ObjectID = user.UserID.ToString(), .ActionNote = String.Format("Unlock user with email {0}", user.Email)})
			result = "ok"
		Catch ex As Exception
			_log.Error("Error while UnlockUser", ex)
			result = "error"
		Finally
			oDB.closeConnection()
		End Try
		Return result
	End Function


	Public Function ChangeUserState(user As SmUser, status As SmSettings.UserStatus, createdByUserId As Guid, ipAddress As String) As String
		Dim result As String = ""
		Dim oDB As New CSQLDBUtils(LPQMOBILE_CONNECTIONSTRING)
		Try
			Dim oWhere As New CSQLWhereStringBuilder()
			Dim actionLogBL As New SmActionLogBL()
			oWhere.AppendAndCondition("UPPER(Email)=UPPER({0})", New CSQLParamValue(user.Email))
			Dim oUpdate As New cSQLUpdateStringBuilder(USERS_TABLE, oWhere)
			oUpdate.appendvalue("Status", New CSQLParamValue(status))
			oDB.executeNonQuery(oUpdate.SQL, False)
			actionLogBL.WriteActionLog(New SmActionLog() With {.Action = SmActionLogAction.CHANGE_USER_STATE, .Category = SmActionLogCategory.USERS_MANIPULATION, .CreatedByUserID = createdByUserId, .IpAddress = ipAddress, .ObjectID = user.UserID.ToString(), .ActionNote = String.Format("Change status of user {0} to {1}", user.Email, status.GetEnumDescription())})
			result = "ok"
		Catch ex As Exception
			_log.Error("Error while ChangeUserState", ex)
			result = "error"
		Finally
			oDB.closeConnection()
		End Try
		Return result
	End Function

	Public Function ProcessSSOEmailValidationToken(tokenObj As SmSSOEmailValidationToken, ipAddress As String) As String
		Dim result As String = ""
		Dim oDB As New CSQLDBUtils(LPQMOBILE_CONNECTIONSTRING)
		Try
			'deactive other token
			Dim oWhere As New CSQLWhereStringBuilder()
			oWhere.AppendAndCondition("UserID={0}", New CSQLParamValue(tokenObj.UserID))
			Dim oUpdate As New cSQLUpdateStringBuilder(SSOEMAIL_VALIDATION_TOKENS_TABLE, oWhere)
			oUpdate.appendvalue("Status", New CSQLParamValue(SmSettings.TokenStatus.Deleted))
			oDB.executeNonQuery(oUpdate.SQL, True)


			oWhere = New CSQLWhereStringBuilder()
			oWhere.AppendAndCondition("Token={0}", New CSQLParamValue(tokenObj.Token))
			oUpdate = New cSQLUpdateStringBuilder(SSOEMAIL_VALIDATION_TOKENS_TABLE, oWhere)
			oUpdate.appendvalue("ActivatedDate", New CSQLParamValue(Now))
			oUpdate.appendvalue("Status", New CSQLParamValue(SmSettings.TokenStatus.Activated))
			oDB.executeNonQuery(oUpdate.SQL, True)

			oWhere = New CSQLWhereStringBuilder()
			oWhere.AppendAndCondition("UserID={0}", New CSQLParamValue(tokenObj.UserID))
			oUpdate = New cSQLUpdateStringBuilder(USERS_TABLE, oWhere)
			oUpdate.appendvalue("IsSSOEmailConfirmed", New CSQLParamValue("Y"))
			oDB.executeNonQuery(oUpdate.SQL, True)

			oDB.commitTransactionIfOpen()
			Dim actionLogBL As New SmActionLogBL()
			actionLogBL.WriteActionLog(New SmActionLog() With {.Action = SmActionLogAction.PROCESS_SSOEMAIL_VALIDATION_TOKEN, .Category = SmActionLogCategory.USERS_MANIPULATION, .CreatedByUserID = tokenObj.UserID, .ObjectID = tokenObj.UserID.ToString(), .IpAddress = ipAddress, .ActionNote = String.Format("SSO Email {0} has been confirmed", tokenObj.Email)})
			result = "ok"
		Catch ex As Exception
			_log.Error("Error while ProcessSSOEmailValidationByToken", ex)
			result = "error"
		Finally
			oDB.closeConnection()
		End Try
		Return result
	End Function

	Public Function GetSSOEmailValidationTokenInfo(token As String) As SmSSOEmailValidationToken
		Dim result As SmSSOEmailValidationToken = Nothing
		Dim oData As DataTable
		Dim sSQL As String = String.Format("SELECT * FROM {0} ", SSOEMAIL_VALIDATION_TOKENS_TABLE)
		Dim oWhere As New CSQLWhereStringBuilder()
		oWhere.AppendAndCondition("Token={0}", New CSQLParamValue(token))
		Dim oDB As New CSQLDBUtils(LPQMOBILE_CONNECTIONSTRING)
		Try
			oData = oDB.getDataTable(sSQL & oWhere.SQL)
			If oData IsNot Nothing AndAlso oData.Rows.Count > 0 Then
				Dim row As DataRow = oData.Rows(0)
				result = New SmSSOEmailValidationToken()
				result.Token = Common.SafeString(row("Token"))
				result.Email = Common.SafeString(row("Email"))
				result.UserID = Common.SafeGUID(row("UserID"))
				result.Status = DirectCast(Common.SafeInteger(row("Status")), SmSettings.TokenStatus)
				result.CreatedDate = Common.SafeDate(row("CreatedDate"))
				If Not row.IsNull("ActivatedDate") Then
					result.ActivatedDate = Common.SafeDate(row("ActivatedDate"))
				End If
			End If
		Catch ex As Exception
			_log.Error("Error while GetSSOEmailValidationTokenInfo", ex)
		Finally
			oDB.closeConnection()
		End Try
		Return result
	End Function

	Public Function GetActivationTokenInfo(token As String) As SmUserActivateToken
		Dim result As SmUserActivateToken = Nothing
		Dim oData As DataTable
		Dim sSQL As String = String.Format("SELECT UT.* FROM {0} UT INNER JOIN {1} U on UT.UserID = U.UserID", USER_ACTIVATE_TOKENS_TABLE, USERS_TABLE)
		Dim oWhere As New CSQLWhereStringBuilder()
		oWhere.AppendAndCondition("Token={0}", New CSQLParamValue(token))
		oWhere.AppendAndNotIn("U.Status", New List(Of CSQLParamValue)({New CSQLParamValue(SmSettings.UserStatus.Deleted), New CSQLParamValue(SmSettings.UserStatus.Disabled)}))
		Dim oDB As New CSQLDBUtils(LPQMOBILE_CONNECTIONSTRING)
		Try
			oData = oDB.getDataTable(sSQL & oWhere.SQL)
			If oData IsNot Nothing AndAlso oData.Rows.Count > 0 Then
				Dim row As DataRow = oData.Rows(0)
				result = New SmUserActivateToken()
				result.Token = Common.SafeString(row("Token"))
				result.Email = Common.SafeString(row("Email"))
				result.UserID = Common.SafeGUID(row("UserID"))
				result.Status = DirectCast(Common.SafeInteger(row("Status")), SmSettings.TokenStatus)
				result.CreatedDate = Common.SafeDate(row("CreatedDate"))
				If Not row.IsNull("ActivatedDate") Then
					result.ActivatedDate = Common.SafeDate(row("ActivatedDate"))
				End If
			End If
		Catch ex As Exception
			_log.Error("Error while GetActivationTokenInfo", ex)
		Finally
			oDB.closeConnection()
		End Try
		Return result
	End Function

	Public Function ActivateUser(tokenObj As SmUserActivateToken, profile As SmActivateAccountModel, ipAddress As String) As String
		Dim result As String = ""
		Dim oDB As New CSQLDBUtils(LPQMOBILE_CONNECTIONSTRING)
		Try
			Dim oWhere As New CSQLWhereStringBuilder()
			oWhere.AppendAndCondition("Token={0}", New CSQLParamValue(tokenObj.Token))
			Dim oUpdate As New cSQLUpdateStringBuilder(USER_ACTIVATE_TOKENS_TABLE, oWhere)
			oUpdate.appendvalue("ActivatedDate", New CSQLParamValue(Now))
			oUpdate.appendvalue("Status", New CSQLParamValue(SmSettings.TokenStatus.Activated))
			oDB.executeNonQuery(oUpdate.SQL, True)

			Dim salt As String = SmUtil.GetSalt()
			Dim hashedPassword As String = SmUtil.GetSHA256Hash(profile.Password, salt)
			oWhere = New CSQLWhereStringBuilder()
			'oWhere.AppendAndCondition("UPPER(Email)=UPPER({0})", New CSQLParamValue(tokenObj.Email))
			oWhere.AppendAndCondition("UserID={0}", New CSQLParamValue(tokenObj.UserID)) 'use this to avoid activating multiple accounts with the same email(deleted, inactive)
			oUpdate = New cSQLUpdateStringBuilder(USERS_TABLE, oWhere)
			oUpdate.appendvalue("Password", New CSQLParamValue(hashedPassword))
			oUpdate.appendvalue("Salt", New CSQLParamValue(salt))
			oUpdate.appendvalue("Status", New CSQLParamValue(SmSettings.UserStatus.Active))
			oUpdate.appendvalue("FirstName", New CSQLParamValue(profile.FirstName))
			oUpdate.appendvalue("LastName", New CSQLParamValue(profile.LastName))
			oUpdate.appendvalue("Phone", New CSQLParamValue(profile.Phone))
			oUpdate.appendvalue("Avatar", New CSQLParamValue(Common.ExtractAvatarUrl(profile.Avatar)))
			oDB.executeNonQuery(oUpdate.SQL, True)

			Dim oInsert As New cSQLInsertStringBuilder(USERCHANGEPASSWORDLOGS_TABLE)
			oInsert.AppendValue("ID", New CSQLParamValue(Guid.NewGuid()))
			oInsert.AppendValue("UserID", New CSQLParamValue(tokenObj.UserID))
			oInsert.AppendValue("CreatedDate", New CSQLParamValue(Now))
			oInsert.AppendValue("Password", New CSQLParamValue(hashedPassword))
			oInsert.AppendValue("Salt", New CSQLParamValue(salt))
			oDB.executeNonQuery(oInsert.SQL, True)

			oDB.commitTransactionIfOpen()
			Dim actionLogBL As New SmActionLogBL()
			actionLogBL.WriteActionLog(New SmActionLog() With {.Action = SmActionLogAction.ACTIVATE_ACCOUNT, .Category = SmActionLogCategory.USERS_MANIPULATION, .CreatedByUserID = tokenObj.UserID, .ObjectID = tokenObj.UserID.ToString(), .IpAddress = ipAddress, .ActionNote = String.Format("Activate an account with email {0}", tokenObj.Email)})
			result = "ok"
		Catch ex As Exception
			_log.Error("Error while ActivateUser", ex)
			result = "error"
		Finally
			oDB.closeConnection()
		End Try
		Return result
	End Function

	Public Function ResetPassword(tokenObj As SmUserActivateToken, password As String, ipAddress As String) As String
		Dim result As String = ""
		Dim oDB As New CSQLDBUtils(LPQMOBILE_CONNECTIONSTRING)
		Try
			Dim oWhere As New CSQLWhereStringBuilder()
			oWhere.AppendAndCondition("Token={0}", New CSQLParamValue(tokenObj.Token))
			Dim oUpdate As New cSQLUpdateStringBuilder(USER_ACTIVATE_TOKENS_TABLE, oWhere)
			oUpdate.appendvalue("ActivatedDate", New CSQLParamValue(Now))
			oUpdate.appendvalue("Status", New CSQLParamValue(SmSettings.TokenStatus.Activated))
			oDB.executeNonQuery(oUpdate.SQL, True)

			Dim salt As String = SmUtil.GetSalt()
			Dim hashedPassword As String = SmUtil.GetSHA256Hash(password, salt)
			oWhere = New CSQLWhereStringBuilder()
			oWhere.AppendAndCondition("UserID={0}", New CSQLParamValue(tokenObj.UserID))
			oUpdate = New cSQLUpdateStringBuilder(USERS_TABLE, oWhere)
			oUpdate.appendvalue("Password", New CSQLParamValue(hashedPassword))
			oUpdate.appendvalue("Salt", New CSQLParamValue(salt))
			oUpdate.appendvalue("Status", New CSQLParamValue(SmSettings.UserStatus.Active))
			oDB.executeNonQuery(oUpdate.SQL, True)

			Dim oInsert As New cSQLInsertStringBuilder(USERCHANGEPASSWORDLOGS_TABLE)
			oInsert.AppendValue("ID", New CSQLParamValue(Guid.NewGuid()))
			oInsert.AppendValue("UserID", New CSQLParamValue(tokenObj.UserID))
			oInsert.AppendValue("CreatedDate", New CSQLParamValue(Now))
			oInsert.AppendValue("Password", New CSQLParamValue(hashedPassword))
			oInsert.AppendValue("Salt", New CSQLParamValue(salt))
			oDB.executeNonQuery(oInsert.SQL, True)

			oDB.commitTransactionIfOpen()
			Dim actionLogBL As New SmActionLogBL()
			actionLogBL.WriteActionLog(New SmActionLog() With {.Action = SmActionLogAction.PROCESS_RESET_PASSWORD_REQUEST, .Category = SmActionLogCategory.USERS_MANIPULATION, .CreatedByUserID = tokenObj.UserID, .ObjectID = tokenObj.UserID.ToString(), .IpAddress = ipAddress, .ActionNote = String.Format("Password has been reset for email {0}", tokenObj.Email)})
			result = "ok"
		Catch ex As Exception
			_log.Error("Error while ResetPassword", ex)
			result = "error"
		Finally
			oDB.closeConnection()
		End Try
		Return result
	End Function

	Public Function UpdateLoginFailureCount(userid As Guid, count As Integer) As String
		Dim result As String = ""
		Dim oDB As New CSQLDBUtils(LPQMOBILE_CONNECTIONSTRING)
		Try
			Dim oWhere As New CSQLWhereStringBuilder()
			oWhere.AppendAndCondition("UserID={0}", New CSQLParamValue(userid))
			Dim oUpdate As New cSQLUpdateStringBuilder(USERS_TABLE, oWhere)
			oUpdate.appendvalue("LoginFailedCount", New CSQLParamValue(count))
			If count = 0 Then
				oUpdate.appendvalue("LastLoginDate", New CSQLParamValue(Now))
			End If
			oDB.executeNonQuery(oUpdate.SQL, False)
			result = "ok"
		Catch ex As Exception
			_log.Error("Error while UpdateLoginFailureCount", ex)
			result = "error"
		Finally
			oDB.closeConnection()
		End Try
		Return result
	End Function

	Public Function ResetLoginFailureCount(user As SmUser, createdByUserId As Guid, ipAddress As String) As String
		Dim result As String = ""
		Dim oDB As New CSQLDBUtils(LPQMOBILE_CONNECTIONSTRING)
		Try
			Dim oWhere As New CSQLWhereStringBuilder()
			oWhere.AppendAndCondition("UserID={0}", New CSQLParamValue(user.UserID))
			Dim oUpdate As New cSQLUpdateStringBuilder(USERS_TABLE, oWhere)
			oUpdate.appendvalue("LoginFailedCount", New CSQLParamValue(0))
			If user.LastLoginDate.HasValue Then
				oUpdate.appendvalue("LastLoginDate", New CSQLParamValue(Now))
			End If
			oDB.executeNonQuery(oUpdate.SQL, False)
			Dim actionLogBL As New SmActionLogBL()
			actionLogBL.WriteActionLog(New SmActionLog() With {.Action = SmActionLogAction.RESET_LOGIN_FAILURE_COUNT, .Category = SmActionLogCategory.USERS_MANIPULATION, .CreatedByUserID = createdByUserId, .ObjectID = user.UserID.ToString(), .IpAddress = ipAddress, .ActionNote = String.Format("Reset login failure count for user with email {0}", user.Email)})
			result = "ok"
		Catch ex As Exception
			_log.Error("Error while ResetLoginFailureCount", ex)
			result = "error"
		Finally
			oDB.closeConnection()
		End Try
		Return result
	End Function

	Public Function GetUserBySSOWorkspaceLoanOfficerCode(ssoWorkspaceLoanOfficerCode As String) As SmUser
		Dim result As SmUser = Nothing
		Dim oData As DataTable
		Dim sSQL As String = String.Format("SELECT U.*, S.Email as SessionEmail, S.AccessRights FROM {0} U LEFT JOIN {1} S ON U.SSOWorkspaceLoanOfficerCode = (S.WorkingUrl+ '/' + S.LoanOfficerCode ) ", USERS_TABLE, SSOSESSIONS_TABLE)
		Dim oWhere As New CSQLWhereStringBuilder()
		oWhere.AppendAndCondition("SSOWorkspaceLoanOfficerCode={0}", New CSQLParamValue(ssoWorkspaceLoanOfficerCode))
		oWhere.AppendAndCondition("Status<>{0} ", New CSQLParamValue(SmSettings.UserStatus.Deleted))	'Deleted
		Dim oDB As New CSQLDBUtils(LPQMOBILE_CONNECTIONSTRING)
		Try
			oData = oDB.getDataTable(sSQL & oWhere.SQL)
			If oData IsNot Nothing AndAlso oData.Rows.Count > 0 Then
				Dim row As DataRow = oData.Rows(0)
				result = New SmUser()
				result.UserID = Common.SafeGUID(row("UserID"))
				result.Email = Common.SafeString(row("SessionEmail"))
				result.CreatedDate = Common.SafeDate(row("CreatedDate"))
				If Not row.IsNull("LastLoginDate") Then
					result.LastLoginDate = Common.SafeDate(row("LastLoginDate"))
				End If
				result.Status = DirectCast(Common.SafeInteger(row("Status")), SmSettings.UserStatus)
				If Not row.IsNull("Role") AndAlso Not String.IsNullOrEmpty(Common.SafeString(row("Role"))) Then
					result.Role = CType([Enum].Parse(GetType(SmSettings.Role), Common.SafeString(row("Role"))), SmSettings.Role)
				End If
				result.FirstName = Common.SafeString(row("FirstName"))
				result.LastName = Common.SafeString(row("LastName"))
				result.SSOWorkspaceLoanOfficerCode = Common.SafeString(row("SSOWorkspaceLoanOfficerCode"))
				result.SSOAccessRights = New JavaScriptSerializer().Deserialize(Of SmSSOAccessRight)(Common.SafeString(row("AccessRights")))
				result.SSOEmail = Common.SafeString(row("SSOEmail"))
				result.IsSSOEmailConfirmed = Common.SafeString(row("IsSSOEmailConfirmed"))
			End If
		Catch ex As Exception
			_log.Error("Error while GetUserBySSOWorkspaceLoanOfficerCode", ex)
		Finally
			oDB.closeConnection()
		End Try
		Return result
	End Function

	'This is used for LPQ SSO
	Public Function CreateSSOState(ByVal psRemoteIP As String, ByVal psDomain As String) As String
		Dim oToken As Guid = Guid.NewGuid
		Dim oDB As New CSQLDBUtils(LPQMOBILE_CONNECTIONSTRING)
		Try
			Dim oInsert As New cSQLInsertStringBuilder(SSOTOKENS_TABLE)
			oInsert.AppendValue("SSOTokenID", New CSQLParamValue(oToken))
			'TODO: remove SSOToken column from DB because it has no use
			oInsert.AppendValue("SSOToken", New CSQLParamValue(""))
			oInsert.appendString("SSOTokenCreateDate", "GETDATE()")
			oInsert.AppendValue("IsSSOTokenUsed", New CSQLParamValue("N"))
			oInsert.AppendValue("LenderRef", New CSQLParamValue(psDomain))
			oInsert.AppendValue("LNAME", New CSQLParamValue(""))
			oInsert.AppendValue("SSN_enc", New CSQLParamValue(""))
			oInsert.AppendValue("Params", New CSQLParamValue(""))
			oInsert.AppendValue("RemoteIP", New CSQLParamValue(psRemoteIP))
			oInsert.AppendValue("LoanType", New CSQLParamValue("LPQSSO"))

			oDB.executeNonQuery(oInsert.SQL)

		Catch ex As Exception
			_log.Error("Error while CreateSSOState", ex)
			Return ""
		Finally
			oDB.closeConnection()
		End Try
		Return Common.SafeString(oToken)
	End Function

	Public Function IsValidState(ByVal psToken As String, ByVal psRemoteIP As String, ByRef psLPQWebsite As String) As Boolean
		If psToken.NullSafeToLower_ = "" Then Return False

		Dim oDB As New CSQLDBUtils(LPQMOBILE_CONNECTIONSTRING)
		Try
			Dim oWhere As New CSQLWhereStringBuilder()
			oWhere.AppendAndCondition("SSOTokenID={0}", New CSQLParamValue(Common.SafeGUID(psToken)))
			oWhere.AppendAndCondition("IsSSOTokenUsed={0}", New CSQLParamValue("N"))
			oWhere.AppendANDCondition("convert(datetime, SSOTokenCreateDate) > DATEADD(MINUTE,-1,GETDATE())") '1min expiration
			Dim oTable As DataTable = oDB.getDataTable("select * from " & SSOTOKENS_TABLE & " " & oWhere.SQL)
			If oTable.Rows.Count = 0 OrElse Common.SafeString(oTable.Rows(0)("LenderRef")) = "" Then
				_log.Warn("Invalid state lookup")
				Return False
			Else
				psLPQWebsite = Common.SafeString(oTable.Rows(0)("LenderRef"))
			End If

			Dim oUpdateWhere As New CSQLWhereStringBuilder()
			oUpdateWhere.AppendAndCondition("SSOTokenID={0}", New CSQLParamValue(Common.SafeGUID(psToken)))
			Dim oSSOTokenUpdate As New cSQLUpdateStringBuilder(SSOTOKENS_TABLE, oUpdateWhere)
			oSSOTokenUpdate.appendvalue("IsSSOTokenUsed", New CSQLParamValue("Y"))
			oSSOTokenUpdate.AppendString("Params", "GETDATE()")
			oDB.executeNonQuery(oSSOTokenUpdate.SQL)
		Catch ex As Exception
			_log.Error("Error while IsValidState", ex)
			Return False
		Finally
			oDB.closeConnection()
		End Try
		Return True
	End Function


	Public Function ExecSSOLogin(context As HttpContext, ssoUserInfo As CSSOUser, workingUrl As String) As Guid
		Dim result As Guid = Guid.Empty
		Dim oDB As New CSQLDBUtils(LPQMOBILE_CONNECTIONSTRING)
		Try
			'insert/update SSO User info in User table
			Dim ssoUser = GetUserBySSOWorkspaceLoanOfficerCode(workingUrl + "/" + ssoUserInfo.LoanOfficerCode)
			Dim ssoUserID = Guid.NewGuid()
			If ssoUser Is Nothing Then
				'insert new sso user. This is for tracking SSO User action logs
				Dim oSSOUserInsert As New cSQLInsertStringBuilder(USERS_TABLE)
				oSSOUserInsert.AppendValue("UserID", New CSQLParamValue(ssoUserID))
				oSSOUserInsert.AppendValue("CreatedDate", New CSQLParamValue(Now))
				oSSOUserInsert.AppendValue("LastLoginDate", New CSQLParamValue(Now))
				oSSOUserInsert.AppendValue("Status", New CSQLParamValue(SmSettings.UserStatus.Active))
				oSSOUserInsert.AppendValue("FirstName", New CSQLParamValue(ssoUserInfo.FName))
				oSSOUserInsert.AppendValue("LastName", New CSQLParamValue(ssoUserInfo.LName))
				oSSOUserInsert.AppendValue("LoginFailedCount", New CSQLParamValue(0))
				oSSOUserInsert.AppendValue("ExpireDays", New CSQLParamValue(-1))
				oSSOUserInsert.AppendValue("SSOWorkspaceLoanOfficerCode", New CSQLParamValue(workingUrl + "/" + ssoUserInfo.LoanOfficerCode))
				oSSOUserInsert.AppendValue("Role", New CSQLParamValue(SmSettings.Role.SSOUser.ToString()))
				'do not need this field because an email may be used by a normal apm user or sso user either which does not allow duplicates
				oSSOUserInsert.AppendValue("Email", New CSQLParamValue(""))
				oSSOUserInsert.AppendValue("Password", New CSQLParamValue(""))
				oSSOUserInsert.AppendValue("Salt", New CSQLParamValue(""))

				oSSOUserInsert.AppendValue("SSOEmail", New CSQLParamValue(ssoUserInfo.Email))
				oSSOUserInsert.AppendValue("IsSSOEmailConfirmed", New CSQLParamValue("N"))
				oDB.executeNonQuery(oSSOUserInsert.SQL, True)
			Else
				ssoUserID = ssoUser.UserID
				'update existed sso user. This is for tracking SSO User action logs
				Dim oSSOUserWhere As New CSQLWhereStringBuilder()
				oSSOUserWhere.AppendAndCondition("UserID={0}", New CSQLParamValue(ssoUserID))
				Dim oSSOUserUpdate As New cSQLUpdateStringBuilder(USERS_TABLE, oSSOUserWhere)
				oSSOUserUpdate.appendvalue("FirstName", New CSQLParamValue(ssoUserInfo.FName))
				oSSOUserUpdate.appendvalue("LastName", New CSQLParamValue(ssoUserInfo.LName))
				oSSOUserUpdate.appendvalue("LastLoginDate", New CSQLParamValue(Now))
				If ssoUser.SSOEmail <> ssoUserInfo.Email Then
					oSSOUserUpdate.appendvalue("SSOEmail", New CSQLParamValue(ssoUserInfo.Email))
					oSSOUserUpdate.appendvalue("IsSSOEmailConfirmed", New CSQLParamValue("N"))
				End If
				oDB.executeNonQuery(oSSOUserUpdate.SQL, True)
			End If
			'allow only one session with a LoanOfficerCode at a time for all browser windows
			Dim oWhere As New CSQLWhereStringBuilder()
			oWhere.AppendAndCondition("LoanOfficerCode={0}", New CSQLParamValue(ssoUserInfo.LoanOfficerCode))
			Dim oDelete As New CSQLDeleteStringBuilder(SSOSESSIONS_TABLE, oWhere)
			oDB.executeNonQuery(oDelete.SQL, True)
			Dim ssoSessionId = Guid.NewGuid()
			Dim oInsert As New cSQLInsertStringBuilder(SSOSESSIONS_TABLE)
			oInsert.AppendValue("SSOSessionID", New CSQLParamValue(ssoSessionId))
			oInsert.AppendValue("OrganizationCode", New CSQLParamValue(ssoUserInfo.OrganizationCode))
			oInsert.AppendValue("LenderCode", New CSQLParamValue(ssoUserInfo.LenderCode))
			oInsert.AppendValue("LoanOfficerCode", New CSQLParamValue(ssoUserInfo.LoanOfficerCode))
			oInsert.AppendValue("Login", New CSQLParamValue(ssoUserInfo.Login))
			oInsert.AppendValue("FirstName", New CSQLParamValue(ssoUserInfo.FName))
			oInsert.AppendValue("MiddleName", New CSQLParamValue(ssoUserInfo.MName))
			oInsert.AppendValue("LastName", New CSQLParamValue(ssoUserInfo.LName))
			oInsert.AppendValue("Email", New CSQLParamValue(ssoUserInfo.Email))
			oInsert.AppendValue("CreatedDate", New CSQLParamValue(Now))
			Dim ssoAccessRights As New SmSSOAccessRight
			ssoAccessRights.CanManageUsers = ssoUserInfo.AccessRights.canManageUsers
			oInsert.AppendValue("AccessRights", New CSQLParamValue(New JavaScriptSerializer().Serialize(ssoAccessRights)))
			oInsert.AppendValue("WorkingUrl", New CSQLParamValue(workingUrl))
			oDB.executeNonQuery(oInsert.SQL, True)
			oDB.commitTransactionIfOpen()
			Dim actionLogBL As New SmActionLogBL()
			actionLogBL.WriteActionLog(New SmActionLog() With {.Action = SmActionLogAction.SSO_LOGIN_SUCCESS, .Category = SmActionLogCategory.AUTHENTICATION, .CreatedByUserID = ssoUserID, .IpAddress = context.Request.ServerVariables("REMOTE_ADDR"), .ObjectID = ssoUserID.ToString(), .ActionNote = String.Format("{0} login success", ssoUserInfo.LoanOfficerCode)})
			result = ssoSessionId
		Catch ex As Exception
			_log.Error("Error while ExecSSOLogin", ex)
			result = Guid.Empty
		Finally
			oDB.closeConnection()
		End Try
		Return result
	End Function

	Public Function ExecLogin(context As HttpContext, userName As String, password As String) As SmLoginResult
		Dim user As SmUser = GetUserByEmail(userName)
		If user Is Nothing Then
			Return New SmLoginResult() With {.Result = SmSettings.LoginStatus.IncorrectPassword, .Message = "Invalid user name or password"}
		End If
		Dim actionLogBL As New SmActionLogBL()
		If user.Status <> SmSettings.UserStatus.Active Then
			actionLogBL.WriteActionLog(New SmActionLog() With {.Action = SmActionLogAction.LOGIN_FAILED, .Category = SmActionLogCategory.AUTHENTICATION, .CreatedByUserID = user.UserID, .IpAddress = context.Request.ServerVariables("REMOTE_ADDR"), .ObjectID = user.UserID.ToString(), .ActionNote = String.Format("{0} attempt login failed due to invalid status {1}", user.Email, user.Status)})
			Return New SmLoginResult() With {.Result = SmSettings.LoginStatus.IsDisabled, .Message = "Your account is invalid."}
		End If
		Dim loginFailedCountExceed As Integer = Common.SafeInteger(ConfigurationManager.AppSettings("LoginFailedCountExceed"))
		If user.LoginFailedCount > loginFailedCountExceed Then
			actionLogBL.WriteActionLog(New SmActionLog() With {.Action = SmActionLogAction.LOGIN_FAILED, .Category = SmActionLogCategory.AUTHENTICATION, .CreatedByUserID = user.UserID, .IpAddress = context.Request.ServerVariables("REMOTE_ADDR"), .ObjectID = user.UserID.ToString(), .ActionNote = String.Format("{0} attempt login failed due to failure count exceed", user.Email)})
			Return New SmLoginResult() With {.Result = SmSettings.LoginStatus.FailureCountExceed, .Message = "You have exceeded the number of allowed login attempts."}
		End If

		If user.LastLoginDate.HasValue AndAlso user.LastLoginDate.Value.AddDays(user.ExpireDays) < DateTime.Now Then
			actionLogBL.WriteActionLog(New SmActionLog() With {.Action = SmActionLogAction.LOGIN_FAILED, .Category = SmActionLogCategory.AUTHENTICATION, .CreatedByUserID = user.UserID, .IpAddress = context.Request.ServerVariables("REMOTE_ADDR"), .ObjectID = user.UserID.ToString(), .ActionNote = String.Format("{0} attempt login failed due to account expiration", user.Email)})
			Return New SmLoginResult() With {.Result = SmSettings.LoginStatus.IsExpired, .Message = "Your account is expired."}
		End If
		Dim hashPassword As String = SmUtil.GetSHA256Hash(password, user.Salt)
		If user.Password <> hashPassword Then
			UpdateLoginFailureCount(user.UserID, user.LoginFailedCount + 1)
			actionLogBL.WriteActionLog(New SmActionLog() With {.Action = SmActionLogAction.LOGIN_FAILED, .Category = SmActionLogCategory.AUTHENTICATION, .CreatedByUserID = user.UserID, .IpAddress = context.Request.ServerVariables("REMOTE_ADDR"), .ObjectID = user.UserID.ToString(), .ActionNote = String.Format("{0} attempt login failed due to incorrect password", user.Email)})
			Return New SmLoginResult() With {.Result = SmSettings.LoginStatus.IncorrectPassword, .Message = "Invalid user name or password"}
		End If

		'TODO:use enum for enum - can't use now bc need to hotfix
		'Don't allow operator access to STAGE/TEST/LIVE from outside network
		Dim sEnv As String = System.Configuration.ConfigurationManager.AppSettings.Get("ENVIRONMENT")
		If (sEnv = "LIVE" Or sEnv = "TEST" Or sEnv = "STAGE") And user.Role <> SmSettings.Role.User Then
			If Not Common.IsInternalIP(context.Request.ServerVariables("REMOTE_ADDR")) Then
				UpdateLoginFailureCount(user.UserID, user.LoginFailedCount + 1)
				actionLogBL.WriteActionLog(New SmActionLog() With {.Action = SmActionLogAction.LOGIN_FAILED, .Category = SmActionLogCategory.AUTHENTICATION, .CreatedByUserID = user.UserID, .IpAddress = context.Request.ServerVariables("REMOTE_ADDR"), .ObjectID = user.UserID.ToString(), .ActionNote = String.Format("{0} attempt login failed due to not allowed network.", user.Email)})
				Return New SmLoginResult() With {.Result = SmSettings.LoginStatus.IncorrectPassword, .Message = "Invalid network"}
			End If
		End If

		'reset login failure count
		UpdateLoginFailureCount(user.UserID, 0)
		actionLogBL.WriteActionLog(New SmActionLog() With {.Action = SmActionLogAction.LOGIN_SUCCESS, .Category = SmActionLogCategory.AUTHENTICATION, .CreatedByUserID = user.UserID, .IpAddress = context.Request.ServerVariables("REMOTE_ADDR"), .ObjectID = user.UserID.ToString(), .ActionNote = String.Format("{0} login success", user.Email)})
		user.Password = String.Empty
		user.Salt = String.Empty

		If HttpContext.Current.Session("CURRENT_USER_INFO") Is Nothing Then
			HttpContext.Current.Session.Add("CURRENT_USER_INFO", user)
		Else
			HttpContext.Current.Session("CURRENT_USER_INFO") = user
		End If
		Return New SmLoginResult() With {.Result = SmSettings.LoginStatus.Success, .Message = "", .UserInfo = user}
	End Function

	'deprecated, no longer need
	'Public Function AccountRequest(context As HttpContext, email As String, ipAddress As String, serverRootPath As String) As String
	'	Dim lpqUsers As System.Collections.Generic.List(Of SmLPQUser) = GetLPQUsers(email)
	'	Dim supporterEmails As String = ConfigurationManager.AppSettings("APMSupporterEmails")
	'	Dim sDenyEmailContent As String = "Dear client, <br/><br/>Thank you for requesting your credential via our automated system.  Unfortunately, our automated system can't auto create your credential.  Please email your request to support@loanspq.com with the following information. "
	'	sDenyEmailContent += "<br/> <b>Subject</b>: APM Account Request  <br/><b>Body</b>: <br/>Host Names (ex: beta.loanspq.com, secure.loanspq.com, etc..):"
	'	sDenyEmailContent += "<br/> Lender Name:"
	'	sDenyEmailContent += "<br/> Email:"
	'	sDenyEmailContent += "<br/> First Name & Last Name:"
	'	sDenyEmailContent += "<br/> Phone:"
	'	If lpqUsers Is Nothing OrElse Not lpqUsers.Any() Then
	'		SmUtil.SendEmail(email, "APM Account Request", sDenyEmailContent)
	'		_log.Debug(String.Format("APM can't authenticate the account request for email: {0} because it is not found in APM LPQUsers.", email))
	'		Return "ok"
	'	End If
	'	If lpqUsers.Count > 1 AndAlso Not serverRootPath.ToUpper.Contains("APPTEST") AndAlso Not serverRootPath.ToUpper.Contains("LPQMOBILE") Then 'the same email shouldn't be used multiple times in live env, only allow for beta and demo
	'		SmUtil.SendEmail(email, "APM Account Request", sDenyEmailContent)
	'		_log.Debug(String.Format("APM can't authenticate the account request for email: {0} because it is found multiple times in APM LPQUsers.", email))
	'		Return "ok"
	'	End If

	'	Dim user As SmUser = GetUserByEmail(email)
	'	If user IsNot Nothing Then
	'		SmUtil.SendEmail(email, "APM Account Request", sDenyEmailContent)
	'		_log.Debug(String.Format("APM can't create account for {0}: because the account is already active in the system", email))
	'		Return "ok"
	'	End If
	'	Dim smBl As New SmBL
	'	Dim backOfficeLenderList As New List(Of SmBackOfficeLender)
	'	'Dim backOfficeLenderListCombined As New List(Of SmBackOfficeLender)

	'	Dim bApplicationPortalFound As Boolean = False
	'	For Each lpquser As SmLPQUser In lpqUsers
	'		If lpquser.LenderID <> Guid.Empty Then 'lender level
	'			backOfficeLenderList = smBl.GetBackOfficeLenderByLenderIDAndHost(lpquser.LenderID, lpquser.Host)
	'			If backOfficeLenderList IsNot Nothing AndAlso backOfficeLenderList.Any() Then
	'				bApplicationPortalFound = True
	'				'backOfficeLenderListCombined.AddRange(backOfficeLenderList)
	'			End If
	'		Else 'org level
	'			backOfficeLenderList = smBl.GetBackOfficeLenderByOrgIDAndHost(lpquser.OrganizationID, lpquser.Host)
	'			If backOfficeLenderList IsNot Nothing AndAlso backOfficeLenderList.Any() Then
	'				bApplicationPortalFound = True
	'				'backOfficeLenderListCombined.AddRange(backOfficeLenderList)
	'			End If
	'		End If
	'	Next

	'	If Not bApplicationPortalFound Then
	'		SmUtil.SendEmail(email, "APM Account Request", sDenyEmailContent)
	'		_log.Debug(String.Format("APM can't locate an Application Portal for this email: {0}", email))
	'		Return "ok"
	'	End If

	'	Dim token As String = Guid.NewGuid().ToString().Replace("-", "").Substring(0, 20)
	'	user = New SmUser()
	'	With user
	'		.UserID = Guid.NewGuid()
	'		.CreatedDate = Now
	'		.Email = lpqUsers(0).Email
	'		.FirstName = lpqUsers(0).FName
	'		.LastName = lpqUsers(0).LName
	'		.Phone = lpqUsers(0).Phone
	'		.Role = SmSettings.Role.User
	'		.ExpireDays = 120 'by default
	'		.Status = SmSettings.UserStatus.InActive
	'	End With

	'	If AddUser(user, user.UserID, ipAddress, token) = "ok" Then
	'		Dim emailContent As String = SmUtil.BuildActivateAccountEmail(user.FirstName, token, serverRootPath)
	'		SmUtil.SendEmail(user.Email, "MeridianLink's APM Account Activation", emailContent)
	'		_log.Debug(emailContent)

	'		'don't perform this action anymore, to assign role to a specific user, go to lender/portal dashboard to do it
	'		'If backOfficeLenderListCombined IsNot Nothing AndAlso backOfficeLenderListCombined.Any() Then
	'		'	For Each lender As SmBackOfficeLender In backOfficeLenderListCombined
	'		'		smBl.AddExistedLenderConfigUser(lender, user, user.UserID, ipAddress)
	'		'	Next
	'		'End If
	'		Return "ok"
	'	Else
	'		Return "failed"
	'	End If
	'End Function

	Public Function SendMluSsoUrl(context As HttpContext, user As SmUser, ipAddress As String, serverRootPath As String) As String

		Dim token As String = Guid.NewGuid().ToString().Replace("-", "").Substring(0, 20)
		If AddSSOEmailValidationTokenRecord(user, user.UserID, ipAddress, token) = "ok" Then
			Dim emailContent As String = SmUtil.BuildMluSsoEmail(token, serverRootPath)
			SmUtil.SendEmail(user.Email, "MeridianLink's APM Account - Email Validation", emailContent)
			Return "ok"
		Else
			Return "failed"
		End If
	End Function
    Public Function ForgotPassword(context As HttpContext, email As String, ipAddress As String, serverRootPath As String) As String
		Dim user As SmUser = GetUserByEmail(email)
		If user Is Nothing Then
			Return "notFound"
		ElseIf user.Status = SmSettings.UserStatus.Disabled Then
			' disabled user is not allowed to forgot/resetpassword
			Return "disabled"
		End If

		Dim token As String = Guid.NewGuid().ToString().Replace("-", "").Substring(0, 20)
		If AddUserActivateTokenRecord(user, user.UserID, ipAddress, token) = "ok" Then
			Dim emailContent As String = SmUtil.BuildResetPasswordEmail(user.FirstName, token, serverRootPath)
			SmUtil.SendEmail(user.Email, "MeridianLink's APM Account - Reset Password", emailContent)
			Return "ok"
		Else
			Return "failed"
		End If
	End Function

	Private Function GetLPQUsers(email As String) As System.Collections.Generic.List(Of SmLPQUser)
		Dim LPQUsers As New System.Collections.Generic.List(Of SmLPQUser)
		Dim oData As DataTable
		Dim sSQL As String = "SELECT * FROM " & LPQ_USERS_TABLE
		Dim oWhere As New CSQLWhereStringBuilder()
		oWhere.AppendAndCondition("UPPER(Email)=UPPER({0})", New CSQLParamValue(email))
		Dim oDB As New CSQLDBUtils(LPQMOBILE_CONNECTIONSTRING)
		Try
			oData = oDB.getDataTable(sSQL & oWhere.SQL)
			If oData Is Nothing OrElse oData.Rows.Count = 0 Then Return LPQUsers
			For Each row As DataRow In oData.Rows
				Dim oLPQUser = New SmLPQUser()
				oLPQUser.LPQUserId = Common.SafeGUID(row("LPQUserId"))
				oLPQUser.Email = Common.SafeString(row("Email"))
				oLPQUser.OrganizationID = Common.SafeGUID(row("OrganizationID"))
				oLPQUser.LenderID = Common.SafeGUID(row("LenderID"))
				oLPQUser.LName = Common.SafeString(row("LName"))
				oLPQUser.FName = Common.SafeString(row("FName"))
				oLPQUser.Host = Common.SafeString(row("Host"))
				oLPQUser.Phone = Common.SafeString(row("Phone"))
				If Not row.IsNull("LastLoginDate") Then
					oLPQUser.LastLoginDate = Common.SafeDate(row("LastLoginDate"))
				End If
				LPQUsers.Add(oLPQUser)
			Next

		Catch ex As Exception
			_log.Error("Error while GetLPQUser", ex)
		Finally
			oDB.closeConnection()
		End Try
		Return LPQUsers
	End Function

	Public Function CheckValidPassword(user As SmUser, password As String) As String
		If password.Length < 8 Then Return "Password must have at least 8 characters."
		If Not Regex.IsMatch(password, "[0-9]{1,}") Then Return "Password must contain at least 1 digit."
		If Not Regex.IsMatch(password, "[a-zA-Z]{1,}") Then Return "Password must contain at least 1 letter."
		If Regex.IsMatch(password, "[\*&'""<>`]") Then Return "Password should not contain special characters * & ' "" < >"
		If password.ToLower().Contains(user.FullName.ToLower()) Then Return "Password should not contain user name."
		Dim logItems = GetLatestChangePasswordLogItem(user.UserID)
		If logItems IsNot Nothing AndAlso logItems.Any(Function(p) p.Password = SmUtil.GetSHA256Hash(password, p.Salt)) Then
			Return "Password must be different from last 4 passwords."
		End If
		Return ""
	End Function

	Private Function GetLatestChangePasswordLogItem(userId As Guid, Optional counter As Integer = 4) As List(Of SmUserChangePasswordLogItem)
		Dim result As New List(Of SmUserChangePasswordLogItem)
		Dim oData As DataTable
		Dim sSQL As String = String.Format("SELECT TOP {0} * FROM {1}", counter, USERCHANGEPASSWORDLOGS_TABLE)
		Dim oWhere As New CSQLWhereStringBuilder()
		oWhere.AppendAndCondition("UserID = {0}", New CSQLParamValue(userId))

		Dim oDB As New CSQLDBUtils(LPQMOBILE_CONNECTIONSTRING)
		Try
			oData = oDB.getDataTable(sSQL & oWhere.SQL & " ORDER BY CreatedDate DESC")
			If oData IsNot Nothing AndAlso oData.Rows.Count > 0 Then
				For Each row As DataRow In oData.Rows
					Dim item = New SmUserChangePasswordLogItem
					item.ID = Common.SafeGUID(row("ID"))
					item.UserID = Common.SafeGUID(row("UserID"))
					item.CreatedDate = Common.SafeDate(row("CreatedDate"))
					item.Password = Common.SafeString(row("Password"))
					item.Salt = Common.SafeString(row("Salt"))
					result.Add(item)
				Next
			End If
		Catch ex As Exception
			_log.Error("Error while GetLatestChangePasswordLogItem", ex)
		Finally
			oDB.closeConnection()
		End Try
		Return result
	End Function

	'Public Function GetUserRole(userRoleID As Guid) As SmUserRole
	'	Dim result As SmUserRole
	'	Dim oData As DataTable
	'	Dim sSQL As String = "SELECT * FROM " & USERROLES_TABLE
	'	Dim oWhere As New CSQLWhereStringBuilder()
	'	oWhere.AppendAndCondition("UserRoleID={0}", New CSQLParamValue(userRoleID))
	'	oWhere.AppendAndCondition("Status = {0} ", New CSQLParamValue(Convert.ToInt16(SmSettings.UserRoleStatus.Active)))
	'	Dim oDB As New CSQLDBUtils(LPQMOBILE_CONNECTIONSTRING)
	'	Try
	'		oData = oDB.getDataTable(sSQL & oWhere.SQL)
	'		If oData IsNot Nothing AndAlso oData.Rows.Count > 0 Then
	'			Dim row As DataRow = oData.Rows(0)
	'			result = New SmUserRole()
	'			result.UserRoleID = Common.SafeGUID(row("UserRoleID"))
	'			result.UserID = Common.SafeGUID(row("UserID"))
	'			If Not IsDBNull("Role") AndAlso Not String.IsNullOrEmpty(Common.SafeString(row("Role"))) Then
	'				result.Role = CType([Enum].Parse(GetType(SmSettings.Role), Common.SafeString(row("Role"))), SmSettings.Role)

	'			End If
	'			result.LenderID = Common.SafeGUID(row("LenderID"))
	'			If Not IsDBNull("LenderConfigID") Then
	'				result.LenderConfigID = Common.SafeGUID(row("LenderConfigID"))
	'			End If
	'			If Not IsDBNull("VendorID") Then
	'				result.VendorID = Common.SafeString(row("VendorID"))
	'			End If
	'			result.Status = DirectCast(Common.SafeInteger(row("Status")), SmSettings.UserRoleStatus)
	'		End If
	'	Catch ex As Exception
	'		_log.Error("Error while GetUserRole", ex)
	'	Finally
	'		oDB.closeConnection()
	'	End Try
	'	Return result
	'End Function
End Class

