﻿Imports System.IO
Imports Microsoft.VisualBasic
Imports System.Xml
Imports System.Net
Imports System.Configuration

Public Class SmMluHandler
	Public Const MLU_SSO_URL As String = "/sm/managelenders.aspx"
    'Public Const PROXY_AUTH_LOGIN As String = ""
    'Public Const PROXY_AUTH_PASSWORD As String = ""
    Public Const PROXY_SERVER_URL As String = "https://talentlmsproxy.meridianlink.com/Proxy/TalentProxy.aspx"
	Public Shared Function PrepareDocRequest(command As String, userInfo As SmUser) As XmlDocument
		Dim docRequest As New XmlDocument
		Dim inputNode = docRequest.CreateElement("INPUT")
		docRequest.AppendChild(inputNode)
		Dim oLoginNode As XmlElement = docRequest.CreateElement("LOGIN")
		oLoginNode.SetAttribute("login", ConfigurationManager.AppSettings("PROXY_AUTH_LOGIN"))
        oLoginNode.SetAttribute("password", ConfigurationManager.AppSettings("PROXY_AUTH_PASSWORD"))
        oLoginNode.SetAttribute("function", command)
		inputNode.AppendChild(oLoginNode)

		Dim oUserInfoNode As XmlElement = docRequest.CreateElement("USERINFO")
		With userInfo
			oUserInfoNode.SetAttribute("fname", .FirstName)
			oUserInfoNode.SetAttribute("lname", .LastName)
			oUserInfoNode.SetAttribute("email", .Email)
			oUserInfoNode.SetAttribute("company_name", "MeridianLink")
			oUserInfoNode.SetAttribute("company_address", "1600 Sunflower Ave")
			oUserInfoNode.SetAttribute("company_state", "CA")
			oUserInfoNode.SetAttribute("company_zip", "92626")
		End With
		inputNode.AppendChild(oUserInfoNode)
		Return docRequest
	End Function


	Public Shared Function PostRequest(docRequest As XmlDocument, ByRef result As String) As Boolean
		Dim log As log4net.ILog = log4net.LogManager.GetLogger(GetType(SmMluHandler))
		Try
			Dim proxyResponse = post(docRequest.OuterXml, PROXY_SERVER_URL)

			Dim oXmlDocResponose As New XmlDocument
			oXmlDocResponose.LoadXml(proxyResponse)

			Dim oOutputNode As XmlElement = DirectCast(oXmlDocResponose.GetElementsByTagName("OUTPUT")(0), XmlElement)
			Dim errorNode = oOutputNode.SelectSingleNode("OUTPUT/ERROR")
			If errorNode IsNot Nothing Then
				result = errorNode.InnerText
				Return False
			End If
			Select Case oOutputNode.GetAttribute("status")
				Case "error"
					result = oOutputNode.InnerText
					Return False
				Case "success"
					result = oOutputNode.InnerText
					Return True
			End Select
		Catch ex As Exception
			log.Error("Post Errors:" & ex.Message)
		End Try
		result = ""
		Return False
	End Function

	Public Shared Function Post(ByVal content As String, ByVal url As String) As String
		Dim isSuccessfulPost As Boolean = False
		Dim retryCount As Integer = 1
		Const numOfRetries As Integer = 3
		Dim responseFromServer As String = ""
		Dim log As log4net.ILog = log4net.LogManager.GetLogger(GetType(SmMluHandler))
        log.Debug(String.Format("Post to url: {0};    content: {1}", url, CSecureStringFormatter.MaskSensitiveXMLData(content)))
        Do While isSuccessfulPost = False AndAlso retryCount <= numOfRetries
			Try
				Dim oweb As WebRequest = WebRequest.Create(url)
				oweb.Method = "POST"  'Almost always use POST
				oweb.ContentLength = content.Length
				oweb.ContentType = "text/xml"  'Can be "application/json" type instead if your passing in a json string


				Dim streamwriter As StreamWriter = New StreamWriter(oweb.GetRequestStream())
				streamwriter.Write(content)
				streamwriter.Close()
				Dim resp As HttpWebResponse = CType(oweb.GetResponse(), HttpWebResponse)
				Dim datastream As Stream = resp.GetResponseStream()
				Dim reader As New StreamReader(datastream)
				responseFromServer = reader.ReadToEnd()
				reader.Close()
				datastream.Close()
				resp.Close()

				If resp.StatusCode = HttpStatusCode.OK Then
					isSuccessfulPost = True
				End If
			Catch ex As Exception
				retryCount = retryCount + 1
				log.Error("Posting to: " & url & " Failed. Waiting 10 secs before trying again. Retrying count:" & retryCount & " Error Msg:" & ex.Message)
				System.Threading.Thread.Sleep(10000)

				If retryCount > numOfRetries Then
					log.Error("Number of attempts reached.")
					Throw ex
				End If
				responseFromServer &= "Error - Posting to: " & url & vbCrLf & ex.ToString
			End Try
		Loop
        log.Debug(String.Format("Response from url: {0};    content: {1}", url, CSecureStringFormatter.MaskSensitiveXMLData(responseFromServer)))
        Return responseFromServer

	End Function
End Class
