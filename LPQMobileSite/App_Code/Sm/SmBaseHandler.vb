﻿Imports System.Web
Public Class SmBaseHandler
	Inherits System.Web.UI.Page

	Protected _log As log4net.ILog = log4net.LogManager.GetLogger(Me.GetType)
	Protected _serverRoot As String
	Public ReadOnly Property UserInfo() As SmUser
		Get
			Dim user As SmUser
			If Session("CURRENT_USER_INFO") IsNot Nothing Then
				user = DirectCast(Session("CURRENT_USER_INFO"), SmUser)
			Else
				Dim smAuthBL As New SmAuthBL()
				user = smAuthBL.GetCurrentUserFromContext(Context)
				user.Password = String.Empty
				user.Salt = String.Empty
				HttpContext.Current.Session.Add("CURRENT_USER_INFO", user)
			End If
			Return user
		End Get
	End Property
	Public ReadOnly Property ServerRoot() As String
		Get
			Dim port As String = ""
			If (Request.Url.Scheme = "http" AndAlso Request.Url.Port <> 80) OrElse (Request.Url.Scheme = "https" AndAlso Request.Url.Port <> 443) Then
				port = ":" + Request.Url.Port.ToString()
			End If
			Return Request.Url.Scheme + "://" + Request.Url.Host + port
		End Get
	End Property

	Private Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
		_serverRoot = ServerRoot
	End Sub

	Private _smBL As SmBL
	Public ReadOnly Property smBL() As SmBL
		Get
			If _smBL Is Nothing Then
				_smBL = New SmBL()
			End If
			Return _smBL
		End Get
	End Property
End Class
