﻿Imports DBUtils

Public Class SmActionLogBL
	Protected LPQMOBILE_CONNECTIONSTRING As String = System.Web.Configuration.WebConfigurationManager.ConnectionStrings("LPQMOBILE_CONNECTIONSTRING").ConnectionString

	Private Const ACTIONLOGS_TABLE As String = "ActionLogs"

	Private _log As log4net.ILog = log4net.LogManager.GetLogger(GetType(SmBL))


	Public Sub WriteActionLog(logItem As SmActionLog)
		Dim result As String = ""
		Dim oDB As New CSQLDBUtils(LPQMOBILE_CONNECTIONSTRING)
		Try
			Dim oInsert As New cSQLInsertStringBuilder(ACTIONLOGS_TABLE)
			oInsert.AppendValue("CreatedByUserID", New CSQLParamValue(logItem.CreatedByUserID))
			oInsert.AppendValue("IPAddress", New CSQLParamValue(logItem.IpAddress))
			oInsert.AppendValue("Action", New CSQLParamValue(logItem.Action))
			oInsert.AppendValue("ActionNote", New CSQLParamValue(logItem.ActionNote))
			oInsert.AppendValue("Category", New CSQLParamValue(logItem.Category))
			oInsert.AppendValue("ObjectID", New CSQLParamValue(logItem.ObjectID))
			oDB.executeNonQuery(oInsert.SQL, False)
		Catch ex As Exception
			_log.Error("Error while WriteActionLog", ex)
			result = "error"
		Finally
			oDB.closeConnection()
		End Try
	End Sub
End Class
