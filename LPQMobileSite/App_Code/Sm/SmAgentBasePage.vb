﻿Imports Microsoft.VisualBasic
Imports LPQMobile.Utils
Imports System.Web

Public Class SmAgentBasePage
	Inherits System.Web.UI.Page

	Private _log As log4net.ILog = log4net.LogManager.GetLogger(Me.GetType)
	Public Overridable ReadOnly Property log() As log4net.ILog
		Get
			Return _log
		End Get
	End Property

	Public Property CurrentUserRole As SmUserRole
	Public Property CurrentVendor As SmLenderVendor
	Public Property CurrentPortal As SmLenderConfigBasicInfo

	Protected Property AllowedRoles As List(Of String)
	Protected Property NotAllowedAccessRedirectUrl As String

	Public ReadOnly Property ServerRoot() As String
		Get
			Dim port As String = ""
			If (Request.Url.Scheme = "http" AndAlso Request.Url.Port <> 80) OrElse (Request.Url.Scheme = "https" AndAlso Request.Url.Port <> 443) Then
				port = ":" + Request.Url.Port.ToString()
			End If
			Return Request.Url.Scheme + "://" + Request.Url.Host + port
		End Get
	End Property
	Private _smBL As SmBL
	Public ReadOnly Property smBL() As SmBL
		Get
			If _smBL Is Nothing Then
				_smBL = New SmBL()
			End If
			Return _smBL
		End Get
	End Property
	Public ReadOnly Property UserInfo() As SmUser
		Get
			Dim user As SmUser
			If Session("CURRENT_USER_INFO") IsNot Nothing Then
				user = DirectCast(Session("CURRENT_USER_INFO"), SmUser)
			Else
				Dim smAuthBL As New SmAuthBL()
				user = smAuthBL.GetCurrentUserFromContext(Context)
				user.Password = String.Empty
				user.Salt = String.Empty
				HttpContext.Current.Session.Add("CURRENT_USER_INFO", user)
			End If
			Return user
		End Get
	End Property

	Private Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
		If Not IsPostBack Then
			If AllowedRoles IsNot Nothing AndAlso AllowedRoles.Any() AndAlso Not SmUtil.CheckRoleAccess(HttpContext.Current, AllowedRoles.ToDictionary(Function(p) p, Function(p) p)) Then
				Response.Redirect(NotAllowedAccessRedirectUrl, True)
			End If
		End If
	End Sub

	Private Sub Page_PreLoad(sender As Object, e As EventArgs) Handles Me.PreLoad
		If Not IsPostBack Then
			AllowedRoles = New List(Of String)
			Dim smBl As New SmBL()
			Dim userRoleID = Common.SafeGUID(Request.QueryString("id"))
			If userRoleID = Guid.Empty AndAlso Not Request.FilePath.ToLower().EndsWith("profile.aspx") Then
				Response.Redirect("~/NoAccess.aspx", True)
			End If
			If userRoleID <> Guid.Empty Then
				CurrentUserRole = smBl.GetUserRoleByID(userRoleID)
				If CurrentUserRole IsNot Nothing AndAlso CurrentUserRole.Status = SmSettings.UserRoleStatus.Active AndAlso Not String.IsNullOrEmpty(CurrentUserRole.VendorID) AndAlso CurrentUserRole.VendorID.ToString().ToUpper() <> Guid.Empty.ToString().ToUpper() AndAlso CurrentUserRole.Vendor IsNot Nothing Then
					AllowedRoles.Add(String.Format("VendorScope${0}${1}${2}${3}", SmSettings.Role.VendorAdmin.ToString(), CurrentUserRole.LenderID.ToString(), CurrentUserRole.LenderConfigID.ToString(), CurrentUserRole.VendorID))
					AllowedRoles.Add(String.Format("VendorScope${0}${1}${2}${3}", SmSettings.Role.VendorUser.ToString(), CurrentUserRole.LenderID.ToString(), CurrentUserRole.LenderConfigID.ToString(), CurrentUserRole.VendorID))
					NotAllowedAccessRedirectUrl = "~/NoAccess.aspx"
					CurrentVendor = CurrentUserRole.Vendor
					CurrentPortal = smBl.GetPortalBasicInfoByLenderConfigID(CurrentUserRole.LenderConfigID)
				Else
					Response.Redirect("~/NoAccess.aspx", True)
				End If
			End If
		End If
	End Sub
End Class
