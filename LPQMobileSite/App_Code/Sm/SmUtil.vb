﻿
Imports System.Xml
Imports System.Security.Cryptography
Imports System.Net.Mail
Imports System.Activities.Expressions
Imports Sm.BO
Imports LPQMobile.BO
Imports Sm
Imports LPQMobile.Utils
Imports System.Web
Imports System.Text
Imports System.Text.RegularExpressions
Imports System.Configuration

Public Class SmUtil

	Shared _log As log4net.ILog = log4net.LogManager.GetLogger("SmUtil")

	Public Shared Function MergeMessageKey(msg As String, dataNode As XmlNode) As String
		If dataNode.Attributes("num_data") IsNot Nothing Then
			For Each match As Match In Regex.Matches(msg, "{(?<index>[0-9]+)}")
				Dim attrName As String = "message_data" & match.Groups("index").Value
				If dataNode.Attributes(attrName) IsNot Nothing Then
					msg = msg.Replace(match.Value, "{{" & dataNode.Attributes(attrName).Value & "}}")
				End If
			Next
		End If
		Return msg
	End Function


	Public Shared Function BuildSuccessMessageXml(outerNodeName As String, nodeName As String, nodeValue As String) As String
		Dim sb As New StringBuilder()
		Dim xmlWriterSetting As New XmlWriterSettings()
		xmlWriterSetting.OmitXmlDeclaration = True
		Using writer As XmlWriter = XmlTextWriter.Create(sb, xmlWriterSetting)
			writer.WriteStartDocument()
			' begin SUCCESS_MESSAGES
			'writer.WriteStartElement(outerNodeName)
			writer.WriteStartElement(nodeName)

			Dim msgContent As String = nodeValue.Trim()
			Dim idx As Integer = 0
			For Each match As Match In Regex.Matches(nodeValue, "{{(?<key>[A-Z0-9_]+)}}")
				msgContent = msgContent.Replace(match.Value, "{" & idx & "}")
				writer.WriteAttributeString("message_data" & idx, match.Groups("key").Value)
				idx += 1
			Next
			writer.WriteAttributeString("num_data", idx.ToString())
			writer.WriteAttributeString("message", msgContent)
			writer.WriteEndElement()
			' end SUCCESS_MESSAGES
			'writer.WriteEndElement()

		End Using
		Return sb.ToString()
	End Function

	Public Shared Function BuildDeclarationsXml(declarationList As List(Of String), Optional rootNodeName As String = "DECLARATIONS") As String
		Dim sb As New StringBuilder()
		Dim xmlWriterSetting As New XmlWriterSettings()
		xmlWriterSetting.OmitXmlDeclaration = True
		Using writer As XmlWriter = XmlTextWriter.Create(sb, xmlWriterSetting)
			writer.WriteStartDocument()
			writer.WriteStartElement(rootNodeName)
			If declarationList IsNot Nothing AndAlso declarationList.Count > 0 Then
				For Each item In declarationList
					writer.WriteStartElement("DECLARATION")
					writer.WriteAttributeString("key", item)
					writer.WriteEndElement()
				Next
			End If
			writer.WriteEndElement()
		End Using
		Return sb.ToString()
	End Function

	Public Shared Function BuildDisclosuresXml(disclosures As List(Of String), Optional rootNodeName As String = "DISCLOSURES") As String
		Dim sb As New StringBuilder()
		Dim xmlWriterSetting As New XmlWriterSettings()
		xmlWriterSetting.OmitXmlDeclaration = True
		Using writer As XmlWriter = XmlTextWriter.Create(sb, xmlWriterSetting)
			writer.WriteStartDocument()
			' begin DISCLOSURES
			writer.WriteStartElement(rootNodeName)
			If disclosures IsNot Nothing AndAlso disclosures.Count > 0 Then
				For Each item As String In disclosures
					writer.WriteStartElement("DISCLOSURE")
					writer.WriteCData(item)
					writer.WriteEndElement()
				Next
			End If
			' end DISCLOSURES
			writer.WriteEndElement()
		End Using
		Return sb.ToString()
	End Function

	Public Shared Function BuildTextValueXml(rootName As String, items As List(Of SmTextValuePair)) As String
		Dim sb As New StringBuilder()
		Dim xmlWriterSetting As New XmlWriterSettings()
		xmlWriterSetting.OmitXmlDeclaration = True
		Using writer As XmlWriter = XmlTextWriter.Create(sb, xmlWriterSetting)
			writer.WriteStartDocument()
			writer.WriteStartElement(rootName)
			If items IsNot Nothing AndAlso items.Count > 0 Then
				For Each item As SmTextValuePair In items
					writer.WriteStartElement("ITEM")
					writer.WriteAttributeString("text", item.Text)
					writer.WriteAttributeString("value", item.Value)
					writer.WriteEndElement()
				Next
			End If
			writer.WriteEndElement()
		End Using
		Return sb.ToString()
	End Function
	Public Shared Function BuildLocTypeXml(rootName As String, items As List(Of SmLineOfCreditItem)) As String
		Dim sb As New StringBuilder()
		Dim xmlWriterSetting As New XmlWriterSettings()
		xmlWriterSetting.OmitXmlDeclaration = True
		Using writer As XmlWriter = XmlTextWriter.Create(sb, xmlWriterSetting)
			writer.WriteStartDocument()
			writer.WriteStartElement(rootName)
			If items IsNot Nothing AndAlso items.Count > 0 Then
				For Each item As SmLineOfCreditItem In items
					writer.WriteStartElement("ITEM")
					writer.WriteAttributeString("text", item.Text)
					writer.WriteAttributeString("value", item.Value)
					writer.WriteAttributeString("category", item.Category.ToString())
					writer.WriteAttributeString("overdraft_option", item.OverDraftOption.ToString())
					writer.WriteEndElement()
				Next
			End If
			writer.WriteEndElement()
		End Using
		Return sb.ToString()
	End Function

	Public Shared Function BuildTextValueCatXml(rootName As String, items As List(Of SmTextValueCatItem)) As String
		Dim sb As New StringBuilder()
		Dim xmlWriterSetting As New XmlWriterSettings()
		xmlWriterSetting.OmitXmlDeclaration = True
		Using writer As XmlWriter = XmlTextWriter.Create(sb, xmlWriterSetting)
			writer.WriteStartDocument()
			writer.WriteStartElement(rootName)
			If items IsNot Nothing AndAlso items.Count > 0 Then
				For Each item As SmTextValueCatItem In items
					writer.WriteStartElement("ITEM")
					writer.WriteAttributeString("text", item.text)
					writer.WriteAttributeString("value", item.value)
					writer.WriteAttributeString("category", item.category)
					writer.WriteEndElement()
				Next
			End If
			writer.WriteEndElement()
		End Using
		Return sb.ToString()
	End Function

	Public Shared Function BuildNodeAttributeXml(nodeName As String, attrName As String, attrValue As String) As String
		Dim sb As New StringBuilder()
		Dim xmlWriterSetting As New XmlWriterSettings()
		xmlWriterSetting.OmitXmlDeclaration = True
		Using writer As XmlWriter = XmlTextWriter.Create(sb, xmlWriterSetting)
			writer.WriteStartElement(nodeName)
			writer.WriteAttributeString(attrName, attrValue)
			writer.WriteEndElement()
		End Using
		Return sb.ToString()
	End Function

	Public Shared Function BuildCustomListRateXml(rateList As Dictionary(Of String, Double)) As String
		Dim sb As New StringBuilder()
		Dim xmlWriterSetting As New XmlWriterSettings()
		xmlWriterSetting.OmitXmlDeclaration = True
		Using writer As XmlWriter = XmlTextWriter.Create(sb, xmlWriterSetting)
			writer.WriteStartElement("RATES")
			For Each item In rateList
				writer.WriteStartElement("ITEM")
				writer.WriteAttributeString("loan_type", item.Key)
				writer.WriteAttributeString("rate", item.Value)
				writer.WriteEndElement()
			Next
			writer.WriteEndElement()
		End Using
		Return sb.ToString()
	End Function


	Public Shared Function BuildAdvancedLogicNodeXml(data As Dictionary(Of String, List(Of SmAdvancedLogicItem))) As String
		Dim sb As New StringBuilder()
		Dim xmlWriterSetting As New XmlWriterSettings()
		xmlWriterSetting.OmitXmlDeclaration = True
		Using writer As XmlWriter = XmlTextWriter.Create(sb, xmlWriterSetting)
			writer.WriteStartElement("ADVANCED_LOGICS")
			If data IsNot Nothing AndAlso data.Any() Then
				For Each item As KeyValuePair(Of String, List(Of SmAdvancedLogicItem)) In data
					Dim loanType As String = item.Key
					Dim logicItems As List(Of SmAdvancedLogicItem) = item.Value
					For Each p As SmAdvancedLogicItem In logicItems
						writer.WriteStartElement("ADVANCED_LOGIC_ITEM")
						writer.WriteAttributeString("target_item", p.TargetItem)
						writer.WriteAttributeString("target_type", p.TargetType)
						writer.WriteAttributeString("loan_type", loanType)
						writer.WriteAttributeString("expression", p.Expression)
						writer.WriteStartElement("CONDITIONS")
						Dim idx As Integer = 0
						For Each c As SmConditionItem In p.Conditions
							writer.WriteStartElement("CONDITION_ITEM")
							writer.WriteAttributeString("type", c.Type)
							writer.WriteAttributeString("name", c.Name)
							writer.WriteAttributeString("operator", c.Comparer)
							writer.WriteAttributeString("value", c.Value)
							If idx = 0 Then
								c.LogicalOperator = ""
							End If
							writer.WriteAttributeString("logical_operator", c.LogicalOperator)
							writer.WriteAttributeString("indent", c.LogicalIndent.ToString())
							writer.WriteEndElement()
							idx = idx + 1
						Next
						writer.WriteEndElement()
						writer.WriteEndElement()
					Next
				Next
			End If
			writer.WriteEndElement()
		End Using
		Return sb.ToString()
	End Function

	Public Shared Function BuildRenameButtonLabelXml(nodeName As String, attrName As String, attrValue As String) As String
		Dim sb As New StringBuilder()
		Dim xmlWriterSetting As New XmlWriterSettings()
		xmlWriterSetting.OmitXmlDeclaration = True
		Using writer As XmlWriter = XmlTextWriter.Create(sb, xmlWriterSetting)
			writer.WriteStartElement(nodeName)
			writer.WriteAttributeString(attrName, attrValue)
			writer.WriteEndElement()
		End Using
		Return sb.ToString()
	End Function

	Public Shared Function BuildContentNodeXml(nodeName As String, content As String) As String
		Dim sb As New StringBuilder()
		Dim xmlWriterSetting As New XmlWriterSettings()
		xmlWriterSetting.OmitXmlDeclaration = True
		Using writer As XmlWriter = XmlTextWriter.Create(sb, xmlWriterSetting)
			writer.WriteStartElement(nodeName)
			writer.WriteCData(content)
			writer.WriteEndElement()
		End Using
		Return sb.ToString()
	End Function

	Public Shared Function BuildRenameableNodeXml(nodeName As String, loanType As String, items As List(Of SmRenameableItemModel)) As String
		Dim sb As New StringBuilder()
		Dim xmlWriterSetting As New XmlWriterSettings()
		xmlWriterSetting.OmitXmlDeclaration = True
		Using writer As XmlWriter = XmlTextWriter.Create(sb, xmlWriterSetting)
			writer.WriteStartElement(nodeName)
			If items IsNot Nothing AndAlso items.Any() Then
				For Each item As SmRenameableItemModel In items
					writer.WriteStartElement("ITEM")
					writer.WriteAttributeString("loan_type", loanType)
					writer.WriteAttributeString("original_text", item.OriginalText)
					writer.WriteAttributeString("new_text", item.NewText)
					writer.WriteEndElement()
				Next
			End If
			writer.WriteEndElement()
		End Using
		Return sb.ToString()
	End Function

	Public Shared Function BuildShowFieldNodeXml(nodeName As String, loanType As String, items As List(Of SmShowFieldItemModel)) As String
		Dim sb As New StringBuilder()
		Dim xmlWriterSetting As New XmlWriterSettings()
		xmlWriterSetting.OmitXmlDeclaration = True
		Using writer As XmlWriter = XmlTextWriter.Create(sb, xmlWriterSetting)
			writer.WriteStartElement(nodeName)
			If items IsNot Nothing AndAlso items.Any() Then
				For Each item As SmShowFieldItemModel In items
					writer.WriteStartElement("ITEM")
					writer.WriteAttributeString("loan_type", loanType)
					writer.WriteAttributeString("controller_id", item.ControllerId)
					writer.WriteAttributeString("controller_name", item.ControllerName)
					writer.WriteAttributeString("is_visible", item.IsVisible)
					writer.WriteEndElement()
				Next
			End If
			writer.WriteEndElement()
		End Using
		Return sb.ToString()
	End Function
	Public Shared Function BuildRequireFieldNodeXml(nodeName As String, loanType As String, items As List(Of SmValidationItemModel)) As String
		Dim sb As New StringBuilder()
		Dim xmlWriterSetting As New XmlWriterSettings()
		xmlWriterSetting.OmitXmlDeclaration = True
		Using writer As XmlWriter = XmlTextWriter.Create(sb, xmlWriterSetting)
			writer.WriteStartElement(nodeName)
			If items IsNot Nothing AndAlso items.Any() Then
				For Each item As SmValidationItemModel In items
					writer.WriteStartElement("ITEM")
					writer.WriteAttributeString("loan_type", loanType)
					writer.WriteAttributeString("controller_id", item.ControllerId)
					writer.WriteAttributeString("controller_name", item.ControllerName)
					writer.WriteAttributeString("is_require", item.IsRequire)
					writer.WriteEndElement()
				Next
			End If
			writer.WriteEndElement()
		End Using
		Return sb.ToString()
	End Function

	Public Shared Function BuildCreditCardNameItemXml(rootName As String, items As List(Of SmCreditCardNameItem)) As String
		Dim sb As New StringBuilder()
		Dim xmlWriterSetting As New XmlWriterSettings()
		xmlWriterSetting.OmitXmlDeclaration = True
		Using writer As XmlWriter = XmlTextWriter.Create(sb, xmlWriterSetting)
			writer.WriteStartDocument()
			writer.WriteStartElement(rootName)
			For Each item As SmCreditCardNameItem In items
				writer.WriteStartElement("ITEM")
				writer.WriteAttributeString("credit_card_name", item.credit_card_name)
				writer.WriteAttributeString("credit_card_type", item.credit_card_type)
				writer.WriteAttributeString("image_url", item.image_url)
				writer.WriteEndElement()
			Next
			writer.WriteEndElement()
		End Using
		Return sb.ToString()
	End Function

	Public Shared Function BuildAccountsXml(items As List(Of SmTextValuePair)) As String
		Dim sb As New StringBuilder()
		Dim xmlWriterSetting As New XmlWriterSettings()
		xmlWriterSetting.OmitXmlDeclaration = True
		Using writer As XmlWriter = XmlTextWriter.Create(sb, xmlWriterSetting)
			writer.WriteStartDocument()
			writer.WriteStartElement("ACCOUNTS")
			For Each item As SmTextValuePair In items
				writer.WriteStartElement("ACCOUNT")
				writer.WriteAttributeString("product_code", item.Value)
				writer.WriteEndElement()
			Next
			writer.WriteEndElement()
		End Using
		Return sb.ToString()
	End Function
	Public Shared Function BuildCDataXml(nodeName As String, data As String) As String
		Dim sb As New StringBuilder()
		Dim xmlWriterSetting As New XmlWriterSettings()
		xmlWriterSetting.OmitXmlDeclaration = True
		Using writer As XmlWriter = XmlTextWriter.Create(sb, xmlWriterSetting)
			writer.WriteStartDocument()
			writer.WriteStartElement(nodeName)
			writer.WriteCData(data)
			writer.WriteEndElement()
		End Using
		Return sb.ToString()
	End Function

	Public Shared Function BuildCustomQuestionsXml(nodeName As String, items As List(Of SmCQInformation), Optional hideAll As Boolean = False) As String
		Dim sb As New StringBuilder()
		Dim xmlWriterSetting As New XmlWriterSettings()
		xmlWriterSetting.OmitXmlDeclaration = True
		Using writer As XmlWriter = XmlTextWriter.Create(sb, xmlWriterSetting)
			writer.WriteStartDocument()
			writer.WriteStartElement(nodeName)
			If hideAll Then
				writer.WriteStartElement("QUESTION")
				writer.WriteAttributeString("name", SmCQInformation.HideAllQuestionKey)
				writer.WriteEndElement()
			Else
				For Each item As SmCQInformation In items
					writer.WriteStartElement("QUESTION")
					writer.WriteAttributeString("name", item.Question)
					writer.WriteAttributeString("is_enable", IIf(item.IsEnable, "Y", "N"))
					writer.WriteAttributeString("display_page", item.DisplayPage)
					writer.WriteEndElement()
				Next
			End If

			writer.WriteEndElement()
		End Using
		Return sb.ToString()
	End Function

	Public Shared Function GetSHA256Hash(plainText As String, salt As String) As String
		Dim hashText As String = plainText & salt
		Dim hash As New SHA256Managed()
		Dim hashBytes = hash.ComputeHash(Encoding.UTF8.GetBytes(hashText))
		Dim sBuilder As New StringBuilder
		For j As Integer = 0 To hashBytes.Length - 1
			sBuilder.Append(hashBytes(j).ToString("X2"))
		Next
		Return sBuilder.ToString()
	End Function

	Public Shared Function GetSalt() As String
		Dim random As New RNGCryptoServiceProvider
		Dim salt(32) As Byte
		random.GetNonZeroBytes(salt)
		Return System.Convert.ToBase64String(salt).Substring(0, 32)
	End Function

	Public Shared Function BindCheckbox(value As Boolean) As String
		If value Then Return "checked"
		Return ""
	End Function

	Public Shared Function RenderDropdownFromEnum(type As Type, Optional selectedValue As String = "") As String
		' type = GetType(SmSettings.Role)
		Dim sb As New StringBuilder
		If type IsNot Nothing Then
			For Each item In [Enum].GetValues(type)
				Dim obj = DirectCast(item, [Enum])
				sb.AppendFormat("<option {2} value='{0}'>{1}</option>", obj.ToString(), obj.GetEnumDescription(), IIf(selectedValue = obj.ToString(), "selected", ""))
			Next
		End If
		Return sb.ToString()
	End Function
	Public Shared Function RenderDropdownFromRoleEnum(options As List(Of SmSettings.Role), Optional value As String = "") As String
		Dim sb As New StringBuilder
		For Each item In options
			sb.AppendFormat("<option {2} value='{0}'>{1}</option>", item.ToString(), item.GetEnumDescription(), IIf(item.ToString() = value, "selected", ""))
		Next
		Return sb.ToString()
	End Function
	Public Shared Function SendEmail(mailTo As String, subject As String, body As String) As Boolean
		Dim log As log4net.ILog = log4net.LogManager.GetLogger(GetType(SmUtil))
		Dim bNotSent As Boolean = True
		Dim retrycount As Integer = 10
		While (bNotSent AndAlso retrycount > 0)
			Try
				Dim smtpClient As New SmtpClient()
				If ConfigurationManager.AppSettings("SMTPServer") <> "" Then
					smtpClient = New System.Net.Mail.SmtpClient(ConfigurationManager.AppSettings("SMTPServer"))
				End If
				Dim mailMessage As New MailMessage()
				mailMessage.From = New System.Net.Mail.MailAddress("no_reply@loanspq.com")	'TODO: make this configurable
				mailMessage.To.Add(New MailAddress(mailTo))
				mailMessage.Subject = subject
				mailMessage.Body = body
				mailMessage.IsBodyHtml = True
				'smtpClient.EnableSsl = True
				smtpClient.Send(mailMessage)
				log.Debug(body)
				bNotSent = False
			Catch ex As Exception
				log.Info(String.Format("Could not Send Email {0} to {1}.", subject, mailTo), ex)
				System.Threading.Thread.Sleep(1000)
				retrycount -= 1
			End Try
		End While
		Return Not bNotSent
	End Function

	Public Shared Function ValidateXMLConstruction(ByVal pXML As XmlDocument) As String
		'Return ""
		Dim oSchemaVal As New XMLTools.CSchemaValidator
		If oSchemaVal.validate(pXML.OuterXml, New XmlTextReader(System.Web.HttpContext.Current.Server.MapPath("~/migrateConfig/config.xsd"))) = False Then
			Dim sXMLErrorMessage As String = oSchemaVal.ParseError
			_log.Info("XML Construction failed XSD Validation: " & sXMLErrorMessage)
			Return sXMLErrorMessage
		Else
			Return "" 'pass
		End If
	End Function
	Public Shared Function CheckRoleAccess(context As HttpContext, roleDic As Dictionary(Of String, String)) As Boolean
		If roleDic Is Nothing Then
			Return False
		End If
		If roleDic.ContainsKey(SmSettings.Role.SuperUser.ToString()) Then
			If Not roleDic.ContainsKey(SmSettings.Role.SuperOperator.ToString()) Then
				roleDic.Add(SmSettings.Role.SuperOperator.ToString(), SmSettings.Role.SuperOperator.ToString())
			End If
			If Not roleDic.ContainsKey(SmSettings.Role.LegacyOperator.ToString()) Then
				roleDic.Add(SmSettings.Role.LegacyOperator.ToString(), SmSettings.Role.LegacyOperator.ToString())
			End If
			If Not roleDic.ContainsKey(SmSettings.Role.Officer.ToString()) Then
				roleDic.Add(SmSettings.Role.Officer.ToString(), SmSettings.Role.Officer.ToString())
			End If
		End If
		Return roleDic.Any(Function(role) HttpContext.Current.User.IsInRole(role.Value))
	End Function
	Public Shared Function CheckRoleAccess(context As HttpContext, roleDic As Dictionary(Of SmSettings.Role, String)) As Boolean
		If roleDic Is Nothing Then
			Return False
		End If
		If roleDic.ContainsKey(SmSettings.Role.SuperUser) Then
			If Not roleDic.ContainsKey(SmSettings.Role.SuperOperator) Then
				roleDic.Add(SmSettings.Role.SuperOperator, SmSettings.Role.SuperOperator.ToString())
			End If
			If Not roleDic.ContainsKey(SmSettings.Role.LegacyOperator) Then
				roleDic.Add(SmSettings.Role.LegacyOperator, SmSettings.Role.LegacyOperator.ToString())
			End If
			If Not roleDic.ContainsKey(SmSettings.Role.Officer) Then
				roleDic.Add(SmSettings.Role.Officer, SmSettings.Role.Officer.ToString())
			End If
		End If
		Return roleDic.Any(Function(role) HttpContext.Current.User.IsInRole(role.Value))
	End Function



	Public Shared Function CheckRoleAccess(context As HttpContext, ParamArray allowedRoles() As SmSettings.Role) As Boolean
		If allowedRoles Is Nothing Then Return False
		Return CheckRoleAccess(context, allowedRoles.ToDictionary(Function(p) p, Function(p) p.ToString()))
	End Function

	Public Shared Function CheckRoleAccess(context As HttpContext, allowedRoles As List(Of SmSettings.Role)) As Boolean
		If allowedRoles Is Nothing Then Return False
		Return CheckRoleAccess(context, allowedRoles.ToDictionary(Function(p) p, Function(p) p.ToString()))
	End Function

	Public Shared Function CheckRoleAccess(context As HttpContext, allowedRoles As List(Of String)) As Boolean
		If allowedRoles Is Nothing Then Return False
		Return CheckRoleAccess(context, allowedRoles.ToDictionary(Function(p) p, Function(p) p.ToString()))
	End Function

	Public Shared Function CheckRoleAccess(context As HttpContext, ParamArray allowedRoles() As String) As Boolean
		If allowedRoles Is Nothing Then Return False
		Return CheckRoleAccess(context, allowedRoles.ToDictionary(Function(p) p, Function(p) p))
	End Function

	Public Shared Function BuildActivateAccountEmail(firstName As String, token As String, serverRootPath As String) As String
		Dim sb As New StringBuilder
		sb.AppendFormat("<p>Dear {0},</p>", firstName)
		sb.Append("<p>Your request access to MeridianLink's Application Portal Management (APM) system has been granted.</p>")
		sb.Append("<p>Your login is your email. To create a password and activate your account, please access the setup page via the below link.  This link is a one-time use and will expire in 7 days.</p>")
		sb.AppendFormat("<a href='{0}/activateaccount.aspx?t={1}' target='_blank'>{0}/activateaccount.aspx?t={1}</a>", serverRootPath, token)
		sb.Append("<br><p>After you created pass word via the above link, you will be automatically redirected to login page.</p>")
		sb.AppendFormat("<a href='{0}/login.aspx' target='_blank'>{0}/login.aspx</a>", serverRootPath)
		Return sb.ToString()
	End Function

	'TODO: may need to customize the message for vendor portal
	Public Shared Function BuildActivateAccountEmail(token As String, serverRootPath As String) As String
		Dim sb As New StringBuilder
		sb.Append("<p>Your request access to MeridianLink's Application Portal Management (APM) system has been granted.</p>")
		sb.Append("<p>Your login is your email. To create a password and activate your account, please access the setup page via the below link.  This link is a one-time use and will expire in 7 days.</p>")
		sb.AppendFormat("<a href='{0}/activateaccount.aspx?t={1}' target='_blank'>{0}/activateaccount.aspx?t={1}</a>", serverRootPath, token)
		sb.Append("<br><p>After you created pass word via the above link, you will be automatically redirected to login page.</p>")
		sb.AppendFormat("<a href='{0}/login.aspx' target='_blank'>{0}/login.aspx</a>", serverRootPath)
		Return sb.ToString()
	End Function

	Public Shared Function BuildLenderAdminRoleAssignedNotification(firstName As String, lenderName As String) As String
		Dim sb As New StringBuilder
		sb.AppendFormat("<p>Dear {0},</p>", firstName)
		sb.AppendFormat("You have been added to {0} as {1}", lenderName, SmSettings.Role.LenderAdmin.GetEnumDescription())
		Return sb.ToString()
	End Function

	Public Shared Function BuildLenderAdminRoleAssignedNotification(lenderName As String) As String
		Dim sb As New StringBuilder
		sb.Append("<p>Dear,</p>")
		sb.AppendFormat("You have been added to {0} as {1}", lenderName, SmSettings.Role.LenderAdmin.GetEnumDescription())
		Return sb.ToString()
	End Function

	Public Shared Function BuildPortalAdminRoleAssignedNotification(firstName As String, lenderName As String, portalName As String) As String
		Dim sb As New StringBuilder
		sb.AppendFormat("<p>Dear {0},</p>", firstName)
		sb.AppendFormat("You have been added to {0} - {1} as {2}", lenderName, portalName, SmSettings.Role.PortalAdmin.GetEnumDescription())
		Return sb.ToString()
	End Function

	Public Shared Function BuildVendorGroupAdminRoleAssignedNotification(firstName As String, lenderName As String, portalName As String) As String
		Dim sb As New StringBuilder
		If String.IsNullOrWhiteSpace(firstName) Then
			sb.Append("<p>Dear,</p>")
		Else
			sb.AppendFormat("<p>Dear {0},</p>", firstName)
		End If
		sb.AppendFormat("You have been added to {0} - {1} as {2}", lenderName, portalName, SmSettings.Role.VendorGroupAdmin.GetEnumDescription())
		Return sb.ToString()
	End Function

	Public Shared Function BuildVendorRoleAssignedNotification(firstName As String, lenderName As String, portalName As String, vendorName As String) As String
		Dim sb As New StringBuilder
		sb.AppendFormat("<p>Dear {0},</p>", firstName)
		sb.AppendFormat("You have been added to {0} - {1} - {2}", lenderName, portalName, vendorName)
		Return sb.ToString()
	End Function
	Public Shared Function BuildVendorRoleAssignedNotification(lenderName As String, portalName As String, vendorName As String) As String
		Dim sb As New StringBuilder
		sb.AppendLine("<p>Dear,</p>")
		sb.AppendFormat("You have been added to {0} - {1} - {2}", lenderName, portalName, vendorName)
		Return sb.ToString()
	End Function

	Public Shared Function BuildMluSsoEmail(token As String, serverRootPath As String) As String
		Dim sb As New StringBuilder
		sb.Append("<p>This is an automated message generated from MeridianLink's ""Application Portal Manager"" (APM).</p>")
		sb.AppendFormat("Please validate your email address by clicking on this link: <a href='{0}/performmlusso.aspx?t={1}' target='_blank'>{0}/performmlusso.aspx?t={1}</a>", serverRootPath, token)
		Return sb.ToString()
	End Function

	Public Shared Function BuildResetPasswordEmail(firstName As String, token As String, serverRootPath As String) As String
		Dim sb As New StringBuilder
		sb.AppendFormat("<p>Dear {0},</p>", firstName)
		sb.Append("<p>Your request to reset password has been processed. You can follow the link below to reset your password.</p>")
		sb.AppendFormat("<a href='{0}/resetpassword.aspx?t={1}' target='_blank'>{0}/resetpassword.aspx?t={1}</a>", serverRootPath, token)
		sb.Append("<p>This link is a one-time use and will expire in 7 days.</p>")
		sb.Append("<br><p>After you created password via the above link, you will be automatically redirected to login page.</p>")
		sb.AppendFormat("<a href='{0}/login.aspx' target='_blank'>{0}/login.aspx</a>", serverRootPath)
		Return sb.ToString()
	End Function


	Public Shared Function DownloadCustomQuestion(websiteConfig As CWebsiteConfig, Optional loanTypeList As List(Of String) = Nothing) As List(Of SmCustomQuestionModel)
		Dim copiedWebConfig As CWebsiteConfig = websiteConfig
		Dim visibleNode = copiedWebConfig._webConfigXML.SelectSingleNode("VISIBLE")
		If visibleNode Is Nothing Then
			Dim node As XmlNode = copiedWebConfig._webConfigXML.OwnerDocument.CreateElement("VISIBLE")
			copiedWebConfig._webConfigXML.AppendChild(node)
			visibleNode = copiedWebConfig._webConfigXML.SelectSingleNode("VISIBLE")
		End If
		Dim customQuestionNewApiAttrNode As XmlNode = copiedWebConfig._webConfigXML.SelectSingleNode("VISIBLE/@custom_question_new_api")
		If customQuestionNewApiAttrNode Is Nothing Then
			Dim attr As XmlAttribute = copiedWebConfig._webConfigXML.OwnerDocument.CreateAttribute("custom_question_new_api")
			attr.Value = "Y"
			visibleNode.Attributes.Append(attr)
		ElseIf customQuestionNewApiAttrNode.Value <> "Y" Then
			customQuestionNewApiAttrNode.Value = "Y"
		End If
		Dim applicationCQ As XmlDocument = CDownloadedSettings.CustomApplicationQuestions(copiedWebConfig)
		Return parseCustomQuestionXml(applicationCQ, loanTypeList)
	End Function

	Public Shared Function DownloadApplicantQuestion(websiteConfig As CWebsiteConfig) As List(Of SmApplicantQuestionModel)
		Dim result As New List(Of SmApplicantQuestionModel)

		'Download XA applicant questions
		Dim downloadedXAAQs As List(Of CCustomQuestionXA) = CCustomQuestionNewAPI.getDownloadedXACustomQuestions(websiteConfig, True, "XA")
		If downloadedXAAQs IsNot Nothing AndAlso downloadedXAAQs.Any() Then
			For Each question As CCustomQuestionXA In downloadedXAAQs
				result.Add(New SmApplicantQuestionModel() With {.LoanType = "XA", .Name = question.Name, .Text = question.Text})
			Next
		End If

		'Download loans (PL, VL, HE, CC) applicant questions
		Dim loanTypeList = New List(Of String)(New String() {"HE", "PL", "VL", "CC"})
		For Each loanType As String In loanTypeList
			Dim downloadedLoanAQs = CCustomQuestionNewAPI.getDownloadedCustomQuestions(websiteConfig, True, loanType)
			If downloadedLoanAQs IsNot Nothing AndAlso downloadedLoanAQs.Any() Then
				result.AddRange(From question In downloadedLoanAQs Select New SmApplicantQuestionModel() With {.LoanType = loanType, .Name = question.Name, .Text = question.Text})
			End If
			'get question for combo
			Dim downloadedLoanComboAQs = CCustomQuestionNewAPI.getDownloadedXAComboCustomQuestions(websiteConfig, True, loanType, "1")
			If downloadedLoanComboAQs IsNot Nothing AndAlso downloadedLoanComboAQs.Any() Then
				result.AddRange(From question In downloadedLoanComboAQs Select New SmApplicantQuestionModel() With {.LoanType = loanType & "_COMBO", .Name = question.Name, .Text = question.Text})
			End If
		Next
		Dim downloadedBLLoanAQs = CCustomQuestionNewAPI.getDownloadedCustomQuestions(websiteConfig, True, "BL")
		If downloadedBLLoanAQs IsNot Nothing AndAlso downloadedBLLoanAQs.Any() Then
			result.AddRange(From question In downloadedBLLoanAQs Select New SmApplicantQuestionModel() With {.LoanType = "BL", .Name = question.Name, .Text = question.Text})
		End If
		Return result
	End Function

	Public Shared Function parseCustomQuestionXml(xmlDoc As XmlDocument, Optional loanTypeList As List(Of String) = Nothing) As List(Of SmCustomQuestionModel)
		Dim result As New List(Of SmCustomQuestionModel)
		If xmlDoc IsNot Nothing Then
			Dim questionNodes As XmlNodeList = xmlDoc.SelectNodes("OUTPUT/CUSTOM_QUESTIONS/CUSTOM_QUESTION[contains(@app_source, 'consumer') and @is_active='true']")
			If questionNodes IsNot Nothing AndAlso questionNodes.Count > 0 Then
				For Each question As XmlNode In questionNodes
					Dim headerAnswerType As XmlNodeList = question.SelectNodes("ANSWER_TYPE[text()='HEADER']")
					If headerAnswerType IsNot Nothing AndAlso headerAnswerType.Count > 0 Then Continue For
					Dim questionName As String = question.SelectSingleNode("NAME").InnerText
					Dim questionText As String = question.SelectSingleNode("TEXT").InnerText
					If loanTypeList Is Nothing Then
						loanTypeList = New List(Of String)(New String() {"XA", "HE", "PL", "VL", "CC", "BL"})
					End If
					For Each loanType As String In loanTypeList
						Dim qList As XmlNodeList = question.SelectNodes(String.Format("APP_TYPES_SUPPORTED/APP_TYPE[text()='{0}']", loanType))
						If qList IsNot Nothing AndAlso qList.Count > 0 Then
							If Not result.Any(Function(p) p.Name = questionName And p.LoanType.ToUpper() = loanType) Then
								result.Add(New SmCustomQuestionModel() With {.Text = questionText, .Name = questionName, .LoanType = loanType})
							End If
						End If
					Next
				Next
			End If
		End If
		Return result
	End Function
	
	''this will not remove all but only update the item
	Public Shared Function UpdateShowFieldItems(liveConfigXmlDoc As XmlDocument, nodeBatch As SmLenderConfigDraft) As XmlDocument
		'WEBSITE/CUSTOM_LIST/VISIBILITIES/ITEM[@loan_type='{0}']

		''Create a list of controller_id that need to be updated for this loan type
		Dim controllerIDs As New List(Of String)
		Dim mDoc1 As New XmlDocument()
		mDoc1.LoadXml(nodeBatch.NodeContent)
		Dim newNodes1 As XmlNodeList = mDoc1.SelectNodes("SHOW_FIELDS/ITEM")
		If newNodes1 IsNot Nothing AndAlso newNodes1.Count > 0 Then
			For Each newNode As XmlNode In newNodes1
				controllerIDs.Add(newNode.Attributes("controller_id").Value)
			Next
		End If


		'remove items that need to be updated
		Dim rootNode As XmlNode = liveConfigXmlDoc.SelectSingleNode("WEBSITE")
		Dim customListNode As XmlNode = liveConfigXmlDoc.SelectSingleNode("WEBSITE/CUSTOM_LIST")
		If customListNode Is Nothing Then
			customListNode = liveConfigXmlDoc.CreateElement("CUSTOM_LIST")
			rootNode.AppendChild(customListNode)
		End If
		Dim showFieldNode As XmlNode = liveConfigXmlDoc.SelectSingleNode("WEBSITE/CUSTOM_LIST/SHOW_FIELDS")
		If showFieldNode Is Nothing Then
			showFieldNode = liveConfigXmlDoc.CreateElement("SHOW_FIELDS")
			customListNode.AppendChild(showFieldNode)
		End If
		Dim itemNodes As XmlNodeList = liveConfigXmlDoc.SelectNodes(nodeBatch.NodePath)	 'load all ITEMs with this loan type and delete item that need to be updated
		If itemNodes IsNot Nothing AndAlso itemNodes.Count > 0 Then
			For Each node As XmlNode In itemNodes
				Dim controllerID As String = node.Attributes("controller_id").Value
				If controllerIDs.Contains(controllerID) Then
					showFieldNode.RemoveChild(node)
				End If
			Next
		End If

		'add updated items to the list
		Dim mDoc As New XmlDocument()
		mDoc.LoadXml(nodeBatch.NodeContent)
		Dim newNodes As XmlNodeList = mDoc.SelectNodes("SHOW_FIELDS/ITEM")
		If newNodes IsNot Nothing AndAlso newNodes.Count > 0 Then
			For Each newNode As XmlNode In newNodes
				Dim node As XmlNode = liveConfigXmlDoc.CreateElement("ITEM")
				Dim loanTypeAttr As XmlAttribute = liveConfigXmlDoc.CreateAttribute("loan_type")
				loanTypeAttr.Value = newNode.Attributes("loan_type").Value
				Dim targetSectionIdAttr As XmlAttribute = liveConfigXmlDoc.CreateAttribute("controller_id")
				targetSectionIdAttr.Value = newNode.Attributes("controller_id").Value
				Dim targetSectionNameAttr As XmlAttribute = liveConfigXmlDoc.CreateAttribute("controller_name")
				targetSectionNameAttr.Value = newNode.Attributes("controller_name").Value
				Dim isVisibleAttr As XmlAttribute = liveConfigXmlDoc.CreateAttribute("is_visible")
				isVisibleAttr.Value = newNode.Attributes("is_visible").Value
				node.Attributes.Append(loanTypeAttr)
				node.Attributes.Append(targetSectionIdAttr)
				node.Attributes.Append(targetSectionNameAttr)
				node.Attributes.Append(isVisibleAttr)
				showFieldNode.AppendChild(node)
			Next
		End If

		Return liveConfigXmlDoc
	End Function

	'This remove all items for loantype and add new items
	Public Shared Function MergeShowFieldItems(liveConfigXmlDoc As XmlDocument, nodeBatch As SmLenderConfigDraft) As XmlDocument
		'WEBSITE/CUSTOM_LIST/SHOW_FIELDS/ITEM[@loan_type='{0}']

		Dim rootNode As XmlNode = liveConfigXmlDoc.SelectSingleNode("WEBSITE")
		Dim customListNode As XmlNode = liveConfigXmlDoc.SelectSingleNode("WEBSITE/CUSTOM_LIST")
		If customListNode Is Nothing Then
			customListNode = liveConfigXmlDoc.CreateElement("CUSTOM_LIST")
			rootNode.AppendChild(customListNode)
		End If
		Dim showFieldNode As XmlNode = liveConfigXmlDoc.SelectSingleNode("WEBSITE/CUSTOM_LIST/SHOW_FIELDS")
		If showFieldNode Is Nothing Then
			showFieldNode = liveConfigXmlDoc.CreateElement("SHOW_FIELDS")
			customListNode.AppendChild(showFieldNode)
		End If
		Dim itemNodes As XmlNodeList = liveConfigXmlDoc.SelectNodes(nodeBatch.NodePath)	 'load all ITEMs with this loan type and delete them
		If itemNodes IsNot Nothing AndAlso itemNodes.Count > 0 Then
			For Each node As XmlNode In itemNodes
				'node.RemoveAll()
				showFieldNode.RemoveChild(node)
			Next
		End If

		Dim mDoc As New XmlDocument()
		mDoc.LoadXml(nodeBatch.NodeContent)
		Dim newNodes As XmlNodeList = mDoc.SelectNodes("SHOW_FIELDS/ITEM")
		If newNodes IsNot Nothing AndAlso newNodes.Count > 0 Then
			For Each newNode As XmlNode In newNodes
				Dim node As XmlNode = liveConfigXmlDoc.CreateElement("ITEM")
				Dim loanTypeAttr As XmlAttribute = liveConfigXmlDoc.CreateAttribute("loan_type")
				loanTypeAttr.Value = newNode.Attributes("loan_type").Value
				Dim targetSectionIdAttr As XmlAttribute = liveConfigXmlDoc.CreateAttribute("controller_id")
				targetSectionIdAttr.Value = newNode.Attributes("controller_id").Value
				Dim targetSectionNameAttr As XmlAttribute = liveConfigXmlDoc.CreateAttribute("controller_name")
				targetSectionNameAttr.Value = newNode.Attributes("controller_name").Value
				Dim isVisibleAttr As XmlAttribute = liveConfigXmlDoc.CreateAttribute("is_visible")
				isVisibleAttr.Value = newNode.Attributes("is_visible").Value
				node.Attributes.Append(loanTypeAttr)
				node.Attributes.Append(targetSectionIdAttr)
				node.Attributes.Append(targetSectionNameAttr)
				node.Attributes.Append(isVisibleAttr)
				showFieldNode.AppendChild(node)
			Next
		End If

		Return liveConfigXmlDoc
	End Function

	Public Shared Function MergeRequireFieldItems(liveConfigXmlDoc As XmlDocument, nodeBatch As SmLenderConfigDraft) As XmlDocument
		'WEBSITE/CUSTOM_LIST/REQUIRE_FIELDS/ITEM[@loan_type='{0}']

		Dim rootNode As XmlNode = liveConfigXmlDoc.SelectSingleNode("WEBSITE")
		Dim customListNode As XmlNode = liveConfigXmlDoc.SelectSingleNode("WEBSITE/CUSTOM_LIST")
		If customListNode Is Nothing Then
			customListNode = liveConfigXmlDoc.CreateElement("CUSTOM_LIST")
			rootNode.AppendChild(customListNode)
		End If
		Dim requireFieldNode As XmlNode = liveConfigXmlDoc.SelectSingleNode("WEBSITE/CUSTOM_LIST/REQUIRE_FIELDS")
		If requireFieldNode Is Nothing Then
			requireFieldNode = liveConfigXmlDoc.CreateElement("REQUIRE_FIELDS")
			customListNode.AppendChild(requireFieldNode)
		End If
		Dim itemNodes As XmlNodeList = liveConfigXmlDoc.SelectNodes(nodeBatch.NodePath)	 'load all ITEMs with this loan type and delete them
		If itemNodes IsNot Nothing AndAlso itemNodes.Count > 0 Then
			For Each node As XmlNode In itemNodes
				'node.RemoveAll()
				requireFieldNode.RemoveChild(node)
			Next
		End If

		Dim mDoc As New XmlDocument()
		mDoc.LoadXml(nodeBatch.NodeContent)
		Dim newNodes As XmlNodeList = mDoc.SelectNodes("REQUIRE_FIELDS/ITEM")
		If newNodes IsNot Nothing AndAlso newNodes.Count > 0 Then
			For Each newNode As XmlNode In newNodes
				Dim node As XmlNode = liveConfigXmlDoc.CreateElement("ITEM")
				Dim loanTypeAttr As XmlAttribute = liveConfigXmlDoc.CreateAttribute("loan_type")
				loanTypeAttr.Value = newNode.Attributes("loan_type").Value
				Dim targetSectionIdAttr As XmlAttribute = liveConfigXmlDoc.CreateAttribute("controller_id")
				targetSectionIdAttr.Value = newNode.Attributes("controller_id").Value
				Dim targetSectionNameAttr As XmlAttribute = liveConfigXmlDoc.CreateAttribute("controller_name")
				targetSectionNameAttr.Value = newNode.Attributes("controller_name").Value
				Dim isRequireAttr As XmlAttribute = liveConfigXmlDoc.CreateAttribute("is_require")
				isRequireAttr.Value = newNode.Attributes("is_require").Value
				node.Attributes.Append(loanTypeAttr)
				node.Attributes.Append(targetSectionIdAttr)
				node.Attributes.Append(targetSectionNameAttr)
				node.Attributes.Append(isRequireAttr)
				requireFieldNode.AppendChild(node)
			Next
		End If

		Return liveConfigXmlDoc
	End Function

	Public Shared Function MergeRenameableItems(liveConfigXmlDoc As XmlDocument, nodeBatch As SmLenderConfigDraft) As XmlDocument
		'WEBSITE/CUSTOM_LIST/RENAME_BUTTON_LABEL/ITEM[@loan_type='{0}']

		''***
		'Comment this out until todo list can be implemented.  As a hack for supporting encoded html, include a "|" in loan_type attribute so APM will not load it or everride it
		'TODO: allow encoded html in new_text
		'This rewrite legacy config TIEM with concatenated loan type or no loan_type to individual ITEM with only one loan_tyep per ITEM
		'Dim renameButtonLabelLegacyNode As XmlNode = liveConfigXmlDoc.SelectSingleNode("WEBSITE/CUSTOM_LIST/RENAME_BUTTON_LABEL")
		'Dim LegacyNodes As XmlNodeList = liveConfigXmlDoc.SelectNodes("WEBSITE/CUSTOM_LIST/RENAME_BUTTON_LABEL/ITEM")	 'load all ITEMs with no loan type or concatenated loan_type
		'If LegacyNodes IsNot Nothing AndAlso LegacyNodes.Count > 0 Then
		'	For Each node As XmlNode In LegacyNodes
		'		If node.Attributes("loan_type") Is Nothing OrElse node.Attributes("loan_type").Value.Contains("|") Then

		'			'rebuild into unique ITEM for concatenated, pl,cc,vl,he, xa, sa(secondary),vl_combo,cc_combo,pl_combo,he_combo, 1a,1b,2a,2b,MERCHANT
		'			Dim LoanTypeArray As String() = {"PL", "CC", "VL", "HE", "XA", "SA", "VL_COMBO", "CC_COMBO", "PL_COMBO", "HE_COMBO", "1A", "1B", "2A", "2B"}

		'			'create new ITEM 
		'			For Each loanType As String In LoanTypeArray
		'				If node.Attributes("loan_type") IsNot Nothing AndAlso Not node.Attributes("loan_type").Value.NullSafeToUpper_.Contains(loanType) Then
		'					Continue For
		'				End If
		'				Dim newNode As XmlNode = liveConfigXmlDoc.CreateElement("ITEM")
		'				Dim loanTypeAttr As XmlAttribute = liveConfigXmlDoc.CreateAttribute("loan_type")
		'				loanTypeAttr.Value = loanType 'new loan type
		'				Dim originalTextAttr As XmlAttribute = liveConfigXmlDoc.CreateAttribute("original_text")
		'				originalTextAttr.Value = node.Attributes("original_text").Value
		'				Dim newTextAttr As XmlAttribute = liveConfigXmlDoc.CreateAttribute("new_text")
		'				newTextAttr.Value = node.Attributes("new_text").Value
		'				newNode.Attributes.Append(loanTypeAttr)
		'				newNode.Attributes.Append(originalTextAttr)
		'				newNode.Attributes.Append(newTextAttr)
		'				renameButtonLabelLegacyNode.AppendChild(newNode)
		'			Next
		'			renameButtonLabelLegacyNode.RemoveChild(node) 'remove legacy node after create all ITEM to replace legacy
		'		End If
		'	Next
		'End If

		Dim rootNode As XmlNode = liveConfigXmlDoc.SelectSingleNode("WEBSITE")
		Dim customListNode As XmlNode = liveConfigXmlDoc.SelectSingleNode("WEBSITE/CUSTOM_LIST")
		If customListNode Is Nothing Then
			customListNode = liveConfigXmlDoc.CreateElement("CUSTOM_LIST")
			rootNode.AppendChild(customListNode)
		End If
		Dim renameButtonLabelNode As XmlNode = liveConfigXmlDoc.SelectSingleNode("WEBSITE/CUSTOM_LIST/RENAME_BUTTON_LABEL")
		If renameButtonLabelNode Is Nothing Then
			renameButtonLabelNode = liveConfigXmlDoc.CreateElement("RENAME_BUTTON_LABEL")
			customListNode.AppendChild(renameButtonLabelNode)
		End If
		Dim itemNodes As XmlNodeList = liveConfigXmlDoc.SelectNodes(nodeBatch.NodePath)	 'load all ITEMs with this loan type and delete them
		If itemNodes IsNot Nothing AndAlso itemNodes.Count > 0 Then
			For Each node As XmlNode In itemNodes
				'node.RemoveAll()
				renameButtonLabelNode.RemoveChild(node)
			Next
		End If

		Dim mDoc As New XmlDocument()
		mDoc.LoadXml(nodeBatch.NodeContent)
		Dim newNodes As XmlNodeList = mDoc.SelectNodes("RENAME_BUTTON_LABEL/ITEM")
		If newNodes IsNot Nothing AndAlso newNodes.Count > 0 Then
			For Each newNode As XmlNode In newNodes
				Dim node As XmlNode = liveConfigXmlDoc.CreateElement("ITEM")
				Dim loanTypeAttr As XmlAttribute = liveConfigXmlDoc.CreateAttribute("loan_type")
				loanTypeAttr.Value = newNode.Attributes("loan_type").Value
				Dim originalTextAttr As XmlAttribute = liveConfigXmlDoc.CreateAttribute("original_text")
				originalTextAttr.Value = newNode.Attributes("original_text").Value
				Dim newTextAttr As XmlAttribute = liveConfigXmlDoc.CreateAttribute("new_text")
				newTextAttr.Value = newNode.Attributes("new_text").Value
				node.Attributes.Append(loanTypeAttr)
				node.Attributes.Append(originalTextAttr)
				node.Attributes.Append(newTextAttr)
				renameButtonLabelNode.AppendChild(node)
			Next
		End If

		Return liveConfigXmlDoc
	End Function

	''' <summary>
	''' Extract host from url
	''' </summary>
	''' <param name="url">Example: https://beta.loanspq.com/services/decisionloan/decisionloan.aspx</param>
	''' <returns>Example: https://beta.loanspq.com</returns>
	''' <remarks></remarks>
	Public Shared Function ExtractHostFromUrl(url As String) As String

		Dim match As Match = Regex.Match(url, "^(?:https?:\/\/)?(?:[^@\n]+@)?(?:www\.)?([^:\/\n]+)", RegexOptions.IgnoreCase)
		If match.Success Then
			Return match.Value
		End If
		Return String.Empty
	End Function

	Public Shared Function RenderDropdownStateOptions(Optional preSelectedValue As String = "") As String
		Dim sb As New StringBuilder()
		For Each state As KeyValuePair(Of String, String) In SmSettings.StateList
			sb.AppendFormat("<option value=""{0}"" {2}>{1}</option>", state.Key, state.Value, IIf(state.Key = preSelectedValue, "selected=""selected""", ""))
		Next
		Return sb.ToString()
	End Function

	Public Shared Function GetApmComboEnable(config As String) As Boolean
		Dim configXml As New XmlDocument
		configXml.LoadXml(config)
		Return GetApmComboEnable(configXml)
	End Function
	Public Shared Function GetApmComboEnable(config As XmlDocument) As Boolean
		Dim apmComboEnableNode = config.SelectSingleNode("WEBSITE/VISIBLE/@APM_combo_enable_Y")
		If apmComboEnableNode IsNot Nothing AndAlso apmComboEnableNode.Value = "N" Then
			Return False
		End If
		Return True
	End Function

	Public Shared Function GetApmSiteAnalyticsEnable(config As String) As Boolean
		Dim configXml As New XmlDocument
		configXml.LoadXml(config)
		Return GetApmSiteAnalyticsEnable(configXml)
	End Function
	Public Shared Function GetApmSiteAnalyticsEnable(config As XmlDocument) As Boolean
		Dim apmSiteAnalyticsEnableNode = config.SelectSingleNode(APM_SITE_ANALYTICS_ENABLE_N)
		Dim googleTagManagerIdNode = config.SelectSingleNode(GOOGLE_TAG_MANAGER_ID())
		Dim piwikSiteIdNode = config.SelectSingleNode(PIWIK_SITE_ID())
        Dim facebookPixelIdNode = config.SelectSingleNode(FACEBOOK_PIXEL_ID())
		If apmSiteAnalyticsEnableNode Is Nothing AndAlso (googleTagManagerIdNode IsNot Nothing Or piwikSiteIdNode IsNot Nothing Or facebookPixelIdNode IsNot Nothing) Then
			Return True
		ElseIf apmSiteAnalyticsEnableNode IsNot Nothing AndAlso apmSiteAnalyticsEnableNode.Value = "Y" Then
			Return True
		End If
		Return False
	End Function

	Public Shared Function GetApmDLScanEnable(config As String) As Boolean
		Dim configXml As New XmlDocument
		configXml.LoadXml(config)
		Return GetApmDLScanEnable(configXml)
	End Function
	Public Shared Function GetApmDLScanEnable(config As XmlDocument) As Boolean
		Dim apmDLScanEnableNode = config.SelectSingleNode(APM_DL_SCAN_ENABLE_N())
		Dim mpao_key2Node = config.SelectSingleNode(LICENSE_SCAN())
		If apmDLScanEnableNode IsNot Nothing AndAlso apmDLScanEnableNode.Value = "Y" Then
			Return True
		ElseIf apmDLScanEnableNode Is Nothing AndAlso mpao_key2Node IsNot Nothing Then
			Return True
		End If
		Return False
	End Function

	Public Shared Function GetApmLinkedInEnable(config As String) As Boolean
		Dim configXml As New XmlDocument
		configXml.LoadXml(config)
		Return GetApmLinkedInEnable(configXml)
	End Function
	Public Shared Function GetApmLinkedInEnable(config As XmlDocument) As Boolean
		Dim apmLinkedInEnableNode = config.SelectSingleNode(APM_LINKEDIN_ENABLE_N())
		Dim linkedInEnableNode = config.SelectSingleNode(LINKEDIN_ENABLE())
		If apmLinkedInEnableNode IsNot Nothing AndAlso apmLinkedInEnableNode.Value = "Y" Then
			Return True
		ElseIf apmLinkedInEnableNode Is Nothing AndAlso linkedInEnableNode IsNot Nothing Then
			Return True
		End If
		Return False
	End Function
	Public Shared Function GetApmShowInstaTouchSwitchButton(config As String) As Boolean
		Dim configXml As New XmlDocument
		configXml.LoadXml(config)
		Return GetApmShowInstaTouchSwitchButton(configXml)
	End Function
	Public Shared Function GetApmShowInstaTouchSwitchButton(config As XmlDocument) As Boolean
		Dim showInstaTouchSwitchButtonNode = config.SelectSingleNode("WEBSITE/VISIBLE/@APM_instatouch_button_N")
		Return showInstaTouchSwitchButtonNode IsNot Nothing AndAlso showInstaTouchSwitchButtonNode.Value.ToUpper() = "Y"
	End Function
    ''' <summary>
    ''' Returns potential warnings of visibility settings that have no effect, based on
    ''' other configurations that may force a section to be shown or hidden, bypassing visibility.
    ''' </summary>
    ''' <param name="visibilityLoanType">The loan type from the Visibility page</param>
    ''' <returns>A mapping of controller/section id to a warning message for the user.</returns>
    Public Shared Function GetVisibilityWarnings(ByVal visibilityLoanType As String, ByVal poConfig As CWebsiteConfig) As Dictionary(Of String, SmVisibilityWarning)
        If String.IsNullOrEmpty(visibilityLoanType) Then
            Throw New ArgumentNullException("visibilityLoanType")
        End If

        Dim warnings = New Dictionary(Of String, SmVisibilityWarning)

        If Regex.IsMatch(visibilityLoanType.Trim(), "^(XA|SA|XA_SPECIAL|SA_SPECIAL|XA_BUSINESS|SA_BUSINESS)$") Then
            If poConfig.XABeneficiaryLevel = "product" Then
                warnings.Add("divBeneficiarySSN1", New SmVisibilityWarning With {
                    .WarningText = "This field will still be visible in the application because the Application Portal is configured to require beneficiaries to supply an SSN to facilitate the auto-booking process.",
                    .Trigger = SmVisibilityWarning.WarningTrigger.Hidden
                })
            End If
        End If

        If visibilityLoanType.ToUpper.Trim() = "BL" Then
            If getBLPrimaryAppRoleVisibility(poConfig) = "N" Then
                warnings.Add("divAppRole_P", New SmVisibilityWarning With {
                       .WarningText = "The primary applicant role field should not normally be hidden, because it is normally a required field in the business loan application. This field should only be hidden if the respective field is specially configured not to be hidden in LoansPQ.",
                       .Trigger = SmVisibilityWarning.WarningTrigger.Hidden
                   })
            End If
        End If
        Return warnings
    End Function
    Public Shared Function getBLPrimaryAppRoleVisibility(ByVal poConfig As CWebsiteConfig) As String
        ''get is_visible attribute of business loan  App Role Primary 
        Dim oBLShowFieldList = Common.getItemsFromConfig(poConfig, String.Format("CUSTOM_LIST/SHOW_FIELDS/ITEM[@loan_type='{0}']", "BL"),
            Function(x)
                Return New KeyValuePair(Of String, String)(Common.SafeString(x.Attributes("controller_id").InnerXml), Common.SafeString(x.Attributes("is_visible").InnerXml))
            End Function)
        Return Common.SafeString(oBLShowFieldList.FirstOrDefault(Function(item) item.Key = "divAppRole_P").Value)
    End Function
End Class
