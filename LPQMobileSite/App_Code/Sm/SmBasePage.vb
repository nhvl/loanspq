﻿Imports LPQMobile.DBManager
Imports LPQMobile.BO
Imports Microsoft.VisualBasic
Imports LPQMobile.Utils
Imports System.Xml
Imports Microsoft.Ajax.Utilities
Imports System.Configuration
Imports System.Web

Public Class SmBasePage
	Inherits System.Web.UI.Page

	Private _log As log4net.ILog = log4net.LogManager.GetLogger(Me.GetType)
	Public Overridable ReadOnly Property log() As log4net.ILog
		Get
			Return _log
		End Get
	End Property

	Protected Property AllowedRoles As List(Of String)
	Protected Property NotAllowedAccessRedirectUrl As String
	Private Property _queryStringItems As Dictionary(Of String, String)

	Public ReadOnly Property ServerRoot() As String
		Get
			Dim port As String = ""
			If (Request.Url.Scheme = "http" AndAlso Request.Url.Port <> 80) OrElse (Request.Url.Scheme = "https" AndAlso Request.Url.Port <> 443) Then
				port = ":" + Request.Url.Port.ToString()
			End If
			Return Request.Url.Scheme + "://" + Request.Url.Host + port
		End Get
	End Property

	Public ReadOnly Property UserInfo() As SmUser
		Get
			Dim user As SmUser
			If Session("CURRENT_USER_INFO") IsNot Nothing Then
				user = DirectCast(Session("CURRENT_USER_INFO"), SmUser)
			Else
				Dim smAuthBL As New SmAuthBL()
				user = smAuthBL.GetCurrentUserFromContext(Context)
				user.Password = String.Empty
				user.Salt = String.Empty
				HttpContext.Current.Session.Add("CURRENT_USER_INFO", user)
			End If
			Return user
		End Get
	End Property

	Private _environment As String = ""
	Public ReadOnly Property Environment() As String
		Get
			If String.IsNullOrWhiteSpace(_environment) Then
				_environment = ConfigurationManager.AppSettings.Get("ENVIRONMENT")
			End If
			Return _environment
		End Get
	End Property

	Public ReadOnly Property IsInEnvironment(ParamArray env As String()) As Boolean
		Get
			If env IsNot Nothing AndAlso env.Contains(Environment) Then Return True
			Return False
		End Get
	End Property

	Private Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
		If Not IsPostBack Then
			If AllowedRoles IsNot Nothing AndAlso AllowedRoles.Any() AndAlso Not SmUtil.CheckRoleAccess(HttpContext.Current, AllowedRoles.ToDictionary(Function(p) p, Function(p) p)) Then
				Response.Redirect(NotAllowedAccessRedirectUrl, True)
			End If
			_queryStringItems = New Dictionary(Of String, String)
			For Each key As String In Request.QueryString.Keys
				If Not _queryStringItems.ContainsKey(key.ToLower()) Then
					_queryStringItems.Add(key.ToLower(), Request.QueryString.Get(key))
				End If
			Next
		End If
	End Sub

	Private Sub Page_PreLoad(sender As Object, e As EventArgs) Handles Me.PreLoad
		If Not IsPostBack Then
			AllowedRoles = New List(Of String)
		End If
	End Sub

	Protected Function BuildUrl(pagePath As String) As String
		If _queryStringItems IsNot Nothing AndAlso _queryStringItems.Count > 0 Then
			Return pagePath & "?" & String.Join("&", _queryStringItems.Select(Function(kvp) String.Format("{0}={1}", kvp.Key, kvp.Value)))
		End If
		Return Request.Path
	End Function
	Protected Function BuildUrl(pagePath As String, excludedParams As List(Of String)) As String
		If _queryStringItems IsNot Nothing AndAlso _queryStringItems.Count > 0 Then
			Return pagePath & "?" & String.Join("&", _queryStringItems.Where(Function(p) excludedParams Is Nothing OrElse Not excludedParams.Contains(p.Key.ToLower())).Select(Function(kvp) String.Format("{0}={1}", kvp.Key, kvp.Value)))
		End If
		Return Request.Path
	End Function
	Protected Function BuildUrl(pagePath As String, ParamArray params() As KeyValuePair(Of String, String)) As String
		If _queryStringItems IsNot Nothing AndAlso _queryStringItems.Count > 0 Then
			For Each param In params
				If Not String.IsNullOrEmpty(param.Key) Then
					If _queryStringItems.ContainsKey(param.Key) Then
						_queryStringItems(param.Key) = param.Value
					Else
						_queryStringItems.Add(param.Key, param.Value)
					End If
				End If
			Next
			Return pagePath & "?" & String.Join("&", _queryStringItems.Select(Function(kvp) String.Format("{0}={1}", kvp.Key, kvp.Value)))
		End If
		Return Request.Path
	End Function
End Class
