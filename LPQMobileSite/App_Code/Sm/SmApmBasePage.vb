﻿Imports LPQMobile.DBManager
Imports LPQMobile.BO
Imports Microsoft.VisualBasic
Imports LPQMobile.Utils
Imports System.Xml
Imports System.Text.RegularExpressions
Imports System.Web

Public Class SmApmBasePage
	Inherits System.Web.UI.Page

	Private _log As log4net.ILog = log4net.LogManager.GetLogger(Me.GetType)
	Public Overridable ReadOnly Property log() As log4net.ILog
		Get
			Return _log
		End Get
	End Property

	Public Property LenderConfig As CLenderConfig = Nothing
	Public Property LenderConfigXml As New XmlDocument()
	Public Property LenderRef As String

	Public Property LenderConfigID As Guid
	Public Property LenderId As Guid
	Public Property WebsiteConfig() As CWebsiteConfig
	Public ReadOnly Property IsBankPortal As Boolean
		Get
			Return WebsiteConfig.InstitutionType = CEnum.InstitutionType.BANK
		End Get
	End Property


	Private Property _queryStringItems As Dictionary(Of String, String)

	Private _smBL As SmBL
	Public ReadOnly Property smBL() As SmBL
		Get
			If _smBL Is Nothing Then
				_smBL = New SmBL()
			End If
			Return _smBL
		End Get
	End Property

	Public ReadOnly Property ServerRoot() As String
		Get
			Dim port As String = ""
			If (Request.Url.Scheme = "http" AndAlso Request.Url.Port <> 80) OrElse (Request.Url.Scheme = "https" AndAlso Request.Url.Port <> 443) Then
				port = ":" + Request.Url.Port.ToString()
			End If
			Return Request.Url.Scheme + "://" + Request.Url.Host + port
		End Get
	End Property
	Private Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
		
		LenderConfigID = Common.SafeGUID(Request.Params("lenderconfigid"))
		If LenderConfigID = Guid.Empty Then
			Server.Transfer("~/Sm/Error.aspx")
		End If

		LenderConfig = smBL.GetLiveConfig(LenderConfigID, pbDecrypt:=True)
		Dim allowRoles As New List(Of String)
		allowRoles.Add(SmSettings.Role.SuperUser.ToString())
		allowRoles.Add(String.Format("LenderScope${0}${1}", SmSettings.Role.LenderAdmin.ToString(), LenderConfig.LenderId.ToString()))
		allowRoles.Add(String.Format("PortalScope${0}${1}${2}", SmSettings.Role.PortalAdmin, LenderConfig.LenderId.ToString(), LenderConfigID.ToString()))
		If Not SmUtil.CheckRoleAccess(HttpContext.Current, allowRoles) Then
			Response.Redirect("~/NoAccess.aspx", True)
		End If
		If Not IsPostBack Then
			If LenderConfig Is Nothing OrElse LenderConfig.ConfigData Is Nothing Then
				Server.Transfer("~/Sm/Error.aspx")
			Else
				LenderId = LenderConfig.LenderId
				LenderRef = LenderConfig.LenderRef
				LenderConfigXml.LoadXml(LenderConfig.ConfigData)
				WebsiteConfig = New CWebsiteConfig(LenderConfigXml.DocumentElement)
				If LenderConfigXml.SelectSingleNode("WEBSITE/XA_LOAN") IsNot Nothing Then
					_enableXA = True
				End If
				If LenderConfigXml.SelectSingleNode("WEBSITE/CREDIT_CARD_LOAN") IsNot Nothing Then
					_enableCC = True
				End If
				If LenderConfigXml.SelectSingleNode("WEBSITE/HOME_EQUITY_LOAN") IsNot Nothing Then
					_enableHE = True
				End If
				If LenderConfigXml.SelectSingleNode("WEBSITE/PERSONAL_LOAN") IsNot Nothing Then
					_enablePL = True
				End If

				If LenderConfigXml.SelectSingleNode("WEBSITE/VEHICLE_LOAN") IsNot Nothing Then
					_enableVL = True
				End If

				If LenderConfigXml.SelectSingleNode("WEBSITE/BUSINESS_LOAN") IsNot Nothing Then
					_enableBL = True
				End If
				If LenderConfigXml.SelectSingleNode("WEBSITE/LQB") IsNot Nothing Then
					_enableLQB = True
				End If
				_apmComboEnable = SmUtil.GetApmComboEnable(LenderConfigXml)
				_apmSiteAnalyticsEnable = SmUtil.GetApmSiteAnalyticsEnable(LenderConfigXml)
				_apmDLScanEnable = SmUtil.GetApmDLScanEnable(LenderConfigXml)
				_apmLinkedInEnable = SmUtil.GetApmLinkedInEnable(LenderConfigXml)
				_queryStringItems = New Dictionary(Of String, String)
				For Each key As String In Request.QueryString.Keys
					If Not _queryStringItems.ContainsKey(key.ToLower()) Then
						_queryStringItems.Add(key.ToLower(), Request.QueryString.Get(key))
					End If
				Next
			End If
		End If
	End Sub

	Public ReadOnly Property UserInfo() As SmUser
		Get
			Dim user As SmUser
			If Session("CURRENT_USER_INFO") IsNot Nothing Then
				user = DirectCast(Session("CURRENT_USER_INFO"), SmUser)
			Else
				Dim smAuthBL As New SmAuthBL()
				user = smAuthBL.GetCurrentUserFromContext(Context)
				user.Password = String.Empty
				user.Salt = String.Empty
				HttpContext.Current.Session.Add("CURRENT_USER_INFO", user)
			End If
			Return user
		End Get
	End Property

	Private _enableXA As Boolean = False
	Protected ReadOnly Property EnableXA As Boolean
		Get
			Return _enableXA
		End Get
	End Property

	Private _enableHE As Boolean = False
	Protected ReadOnly Property EnableHE As Boolean
		Get
			Return _enableHE
		End Get
	End Property

	Private _enableCC As Boolean = False
	Protected ReadOnly Property EnableCC As Boolean
		Get
			Return _enableCC
		End Get
	End Property

	Private _enableVL As Boolean = False
	Protected ReadOnly Property EnableVL As Boolean
		Get
			Return _enableVL
		End Get
	End Property

	Private _enableBL As Boolean = False
	Protected ReadOnly Property EnableBL As Boolean
		Get
			Return _enableBL
		End Get
	End Property

	Private _enableLQB As Boolean = False
	Protected ReadOnly Property EnableLQB As Boolean
		Get
			Return _enableLQB
		End Get
	End Property

	Private _enablePL As Boolean = False
	Protected ReadOnly Property EnablePL As Boolean
		Get
			Return _enablePL
		End Get
	End Property

	Protected ReadOnly Property RevisionID As Integer
		Get
			Return LenderConfig.RevisionID
		End Get
	End Property

	Protected Function BindCheckbox(value As String, valuedList As List(Of String)) As String
		If valuedList IsNot Nothing AndAlso valuedList.Contains(value, StringComparer.OrdinalIgnoreCase) Then Return "checked"
		Return ""
	End Function

	Protected Function BindCheckbox(value As Boolean) As String
		If value Then Return "checked"
		Return ""
	End Function

	Protected Function BindSelectbox(value As String, optValue As String) As String
		If value.NullSafeToUpper_ = optValue.NullSafeToUpper_ Then Return "selected"
		Return ""
	End Function


	'APM properties to support Essential(Free) or Plus(Paid) version

	'=="N" for essential, null or Y for Plus and legacy
	Private _apmComboEnable As Boolean = True
	Protected ReadOnly Property ApmComboEnable() As Boolean
		Get
			Return _apmComboEnable
		End Get
	End Property

	'=="N" for essential, null or Y for plus and legacy->
	Private _apmSiteAnalyticsEnable As Boolean = True
	Protected ReadOnly Property ApmSiteAnalyticsEnable() As Boolean
		Get
			Return _apmSiteAnalyticsEnable
		End Get
	End Property

	'=="Y" for all new deployment, legacy client will have this set to Y if mpao_key2 is not null during publish, this will be "OR" with mpao_key2 to control visibility
	Private _apmDLScanEnable As Boolean = False
	Protected ReadOnly Property ApmDLScanEnable() As Boolean
		Get
			Return _apmDLScanEnable
		End Get
	End Property

	'=="Y" for all Plus, legacy client will have this set to Y if linkedin_enable is Y null during publish, this will be "OR" with linkedin_enable to control visibility
	Private _apmLinkedInEnable As Boolean = False
	Protected ReadOnly Property ApmLinkedInEnable() As Boolean
		Get
			Return _apmLinkedInEnable
		End Get
	End Property
	'End APM properties



	'Protected Function GetNode(path As String) As XmlNode
	'	Dim node As XmlNode = smBL.GetNodeFromDraft(path, UserName, LenderRef, LenderId)
	'	If node Is Nothing Then
	'		node = LenderConfigXml.SelectSingleNode(path)
	'	End If
	'	Return node
	'End Function

	Protected Function GetNode(path As String, Optional draftNodeIncluded As Boolean = True) As XmlNode
		If Not draftNodeIncluded Then
			Return LenderConfigXml.SelectSingleNode(path)
		End If

		Dim node As XmlNode = smBL.GetNodeFromDraft(path, UserInfo.UserID, LenderConfigID)
		If node Is Nothing Then
			node = LenderConfigXml.SelectSingleNode(path)
		Else
			Dim match As Match = Regex.Match(path, "\/@(?<attr>\w+)$")
			If match.Success Then
				node = node.SelectSingleNode("@" & match.Groups("attr").Value)
			End If
		End If
		Return node
	End Function

	Protected Function GetNodes(pathList As List(Of String)) As Dictionary(Of String, XmlNode)
		Dim nodes As Dictionary(Of String, XmlNode) = smBL.GetNodesFromDraft(pathList, UserInfo.UserID, LenderConfigID)
		For Each path As String In pathList
			If nodes.ContainsKey(path) Then
				Dim match As Match = Regex.Match(path, "\/@(?<attr>\w+)$")
				If match.Success Then
					nodes(path) = nodes(path).SelectSingleNode("@" & match.Groups("attr").Value)
				End If
			Else
				nodes.Add(path, LenderConfigXml.SelectSingleNode(path))
			End If
		Next
		Return nodes
	End Function


	Public Function CheckEnabledModule(moduleName As SmSettings.ModuleName) As Boolean
		Select Case moduleName
			Case SmSettings.ModuleName.XA
				Return _enableXA
			Case SmSettings.ModuleName.CC
				Return _enableCC
			Case SmSettings.ModuleName.HE
				Return _enableHE
			Case SmSettings.ModuleName.PL
				Return _enablePL
			Case SmSettings.ModuleName.VL
				Return _enableVL
			Case SmSettings.ModuleName.BL
				Return _enableBL
			Case SmSettings.ModuleName.LQB
				Return _enableLQB
			Case SmSettings.ModuleName.CC_COMBO
				Return _enableXA AndAlso _enableCC AndAlso ApmComboEnable
			Case SmSettings.ModuleName.HE_COMBO
				Return _enableXA AndAlso _enableHE AndAlso ApmComboEnable
			Case SmSettings.ModuleName.PL_COMBO
				Return _enableXA AndAlso _enablePL AndAlso ApmComboEnable
			Case SmSettings.ModuleName.VL_COMBO
				Return _enableXA AndAlso _enableVL AndAlso ApmComboEnable
			Case Else
				Return False
		End Select
	End Function

	Protected Function BuildUrl(pagePath As String) As String
		If _queryStringItems IsNot Nothing AndAlso _queryStringItems.Count > 0 Then
			Return pagePath & "?" & String.Join("&", _queryStringItems.Select(Function(kvp) String.Format("{0}={1}", kvp.Key, kvp.Value)))
		End If
		Return Request.Path
	End Function
	Protected Function BuildUrl(pagePath As String, excludedParams As List(Of String)) As String
		If _queryStringItems IsNot Nothing AndAlso _queryStringItems.Count > 0 Then
			Return pagePath & "?" & String.Join("&", _queryStringItems.Where(Function(p) excludedParams Is Nothing OrElse Not excludedParams.Contains(p.Key.ToLower())).Select(Function(kvp) String.Format("{0}={1}", kvp.Key, kvp.Value)))
		End If
		Return Request.Path
	End Function
	Protected Function BuildUrl(pagePath As String, ParamArray params() As KeyValuePair(Of String, String)) As String
		If _queryStringItems IsNot Nothing AndAlso _queryStringItems.Count > 0 Then
			For Each param In params
				If Not String.IsNullOrEmpty(param.Key) Then
					If _queryStringItems.ContainsKey(param.Key) Then
						_queryStringItems(param.Key) = param.Value
					Else
						_queryStringItems.Add(param.Key, param.Value)
					End If
				End If
			Next
			Return pagePath & "?" & String.Join("&", _queryStringItems.Select(Function(kvp) String.Format("{0}={1}", kvp.Key, kvp.Value)))
		End If
		Return Request.Path
	End Function
	Protected Function GetModuleName(key As String) As String
		Dim result As String = ""
		Select Case key.ToUpper()
			Case "LP"
				result = "Landing"
			Case "ST"
				result = "Check Status"
			Case "SA"
				result = "XA (Secondary)"
			Case "XA_SPECIAL"
				result = "XA (Special)"
			Case "SA_SPECIAL"
				result = "XA (Special Secondary)"
			Case "XA_MINOR"
				result = "XA (Minor)"
			Case "SA_MINOR"
				result = "XA (Minor Secondary)"
			Case "CC_COMBO"
				result = "CC Combo"
			Case "HE_COMBO"
				result = "HE Combo"
			Case "PL_COMBO"
				result = "PL Combo"
			Case "VL_COMBO"
				result = "VL Combo"
			Case "XA_BUSINESS"
				result = "XA (Business)"
			Case "SA_BUSINESS"
				result = "XA (Business Secondary)"
			Case "LQB_MAIN"
				result = "LQB"
			Case "ML"
				result = "Mortgage Loan"
			Case Else
				result = key
		End Select
		Return result
	End Function
End Class
