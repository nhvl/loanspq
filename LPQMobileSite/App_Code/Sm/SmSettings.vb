﻿Imports System.ComponentModel
Imports Microsoft.VisualBasic

Public Class SmSettings

	Public Const extractDomainRegexPattern As String = "^(?:https?:\/\/)?(?:[^@\n]+@)?(?:www\.)?([^:\/\n]+)"

	'Public Shared IDAList As New List(Of String)(New String() {"RSA", "PID", "FIS", "DID", "TID", "EID"})
	Public Shared IDAList As Dictionary(Of String, String) = New Dictionary(Of String, String) From {{"RSA", "RSA"}, {"PID", "Experian PreciseID (PID)"}, {"FIS", "Efund (FIS)"}, {"DID", "Deluxe (DID)"}, {"TID", "Transunion IDMA (TID)"}, {"EID", "Equifax (EID)"}}

	'Public Shared DebitList As New List(Of String)(New String() {"FIS", "DID", "EID", "EWS"})
	Public Shared DebitList As Dictionary(Of String, String) = New Dictionary(Of String, String) From {{"FIS", "Efund (FIS)"}, {"DID", "Deluxe (DID)"}, {"EID", "Equifax (EID)"}, {"EWS", "Experian preciseID (EWS)"}}

	Public Shared LoanStatusList As Dictionary(Of String, String) = New Dictionary(Of String, String) From {{"INSTANTAPPROVED", "INSTANT APPROVED"}, {"APPROVED", "APPROVED"}, {"AP", "APPROVED PENDING"}, {"REFERRED", "REFERRED"}}

    Public Shared BookList As Dictionary(Of String, String) = New Dictionary(Of String, String) From {{"COHESION", "COHESION"}, {"CONNECTIT", "CONNECTIT"}, {"DNA", "DNA"}, {"JXCHANGE", "JXCHANGE"}, {"OPENPATH", "OPENPATH"}, {"PATHWAY", "PATHWAY"}, {"SERVICEBUS", "SERVICEBUS"}, {"SYMITAR", "SYMITAR"}, {"XP", "XP"},
                                                                                                      {"ACCESSADVANTAGE", "ACCESSADVANTAGE"}, {"CMC", "CMC"}, {"CMCFLEXJSON", "CMCFLEXJSON"}, {"CORELATION", "CORELATION"}, {"CUDP", "CUDP"}, {"CUNIFY", "CUNIFY"}, {"EPL", "EPL"}, {"ESP", "ESP"}, {"GALAXY", "GALAXY"},
                                                                                                      {"GENERAL", "GENERAL"}, {"HORIZONXCHANGE", "HORIZONXCHANGE"}, {"PHOENIXXM", "PHOENIXXM"}, {"PREMIER", "PREMIER"}, {"SHAREONE", "SHAREONE"}, {"SIGNATURE", "SIGNATURE"}, {"FISERVCPS", "FISERVCPS"}, {"TMG", "TMG"}}

    Public Enum UserStatus
		<Description("Active")>
		Active = 1
		<Description("Inactive")>
		InActive = 2
		<Description("Deleted")>
		Deleted = 0
		<Description("Disabled")>
		Disabled = 3
		<Description("Locked")>
		Locked = 4 ' computed status. Determine by too many retries or expired
	End Enum

	Public Enum Role
        <Description("Super User")>
        SuperUser 'represent all roles that being operator (SuperOperator, LegacyOperator, Officer/operator)

        <Description("Super Operator")>
		SuperOperator

		<Description("Legacy Operator")>
		LegacyOperator

		<Description("Operator")>
		Officer

		<Description("Accountant")>
		Accountant

		<Description("User")>
		User 'represent all roles that not being operator (LenderAdmin, PortalAdmin, VendorAdmin, VendorUser)

		<Description("Lender Admin")>
		LenderAdmin
		
		<Description("Portal Admin")>
		PortalAdmin

		<Description("Vendor Group Admin")>
		VendorGroupAdmin

		<Description("Vendor Admin")>
		VendorAdmin

		<Description("Vendor User")>
		VendorUser

		<Description("SSO User")>
		SSOUser
	End Enum

	Public Enum UserRoleStatus
		<Description("Active")>
		Active = 1
		<Description("In active")>
		InActive = 0
		<Description("Deleted")>
		Deleted = 2
	End Enum

	Public Enum TokenStatus
		<Description("Waiting")>
		Waiting = 1
		<Description("Deleted")>
		Deleted = 0
		<Description("Activated")>
		Activated = 2
	End Enum

	Public Enum LoginStatus
		Success = 1
		IncorrectPassword = 2
		IsDisabled = 3
		IsExpired = 4
		FailureCountExceed = 5
	End Enum

	Public Enum LenderVendorStatus
		<Description("Active")>
		Active = 1
		<Description("In active")>
		InActive = 0
	End Enum

    Public Enum LenderVendorType
        <Description("BL")>
        BL
        <Description("CC")>
        CC
        <Description("CC COMBO")>
        CC_COMBO
        <Description("HE")>
        HE
        <Description("HE COMBO")>
        HE_COMBO
        <Description("PL")>
        PL
        <Description("PL COMBO")>
        PL_COMBO
        <Description("VL")>
        VL
        <Description("VL COMBO")>
        VL_COMBO
        <Description("XA")>
        XA
    End Enum

    Public Enum NodeGroup
		ManageXA
		ManageSpecialXA
		ManageBusinessXA
		ManageXASecondary
		ManageSpecialXASecondary
		ManageBusinessXASecondary
		ManageVL
		ManagePL
		ManageCC
		ManageLQB
		ManageBL
		ManageHE
		ManageComboVL
		ManageComboPL
		ManageComboCC
		ManageComboHE
		Settings
		GeneralSettings
		XaSettings
		AllLoansSettings
		CCSettings
		HESettings
		PLSettings
		VLSettings
		BLSettings
		LQBSettings
		DesignTheme
		ICERenameXA
		ICERenameCC
		ICERenamePL
		ICERenameHE
		ICERenameVL
		ICERenameBL
		ICERenameLanding
		ICERenameCCCombo
		ICERenamePLCombo
		ICERenameHECombo
		ICERenameVLCombo
		ICERenameSA	' XA Secondary
        ICERenameXASpecial
		ICERenameSASpecial ' XA Special Secondary 
		ICERenameXABusiness
        ICERenameSABusiness ' XA Business Secondary 
        ICERenameCheckStatus
		ICERenameLQBMain
		'ProductsConfigure ' DEPRECATED
		XAProductsConfigure
        CCComboProductsConfigure
        HEComboProductsConfigure
        PLComboProductsConfigure
        VLComboProductsConfigure
		'CustomQuestions ' DEPRECATED
        XACustomQuestions
        CCCustomQuestions
        HECustomQuestions
        PLCustomQuestions
		VLCustomQuestions
		BLCustomQuestions
        Undefinded
        AdvancedLogics
		Revision
		MLSupport
		ReportEquifaxInstatouch
        ShowFieldXA
        ShowFieldCC
        ShowFieldPL
        ShowFieldVL
        ShowFieldHE
        ShowFieldSA 'xa SECONDARY
        ShowFieldXASpecial
        ShowFieldSASpecial ' XA Secondary Special
        ShowFieldXABusiness
        ShowFieldSABusiness
        ShowFieldPLCombo
        ShowFieldVLCombo
        ShowFieldHECombo
        ShowFieldCCCombo
		ShowFieldBL
		ShowFieldLQBMain
		ValidationXA
		ValidationCC
		ValidationPL
		ValidationVL
		ValidationHE
		ValidationSA	'xa SECONDARY
		ValidationXASpecial
		ValidationSASpecial	' XA Secondary Special
		ValidationXABusiness
		ValidationSABusiness
		ValidationPLCombo
		ValidationVLCombo
		ValidationHECombo
		ValidationCCCombo
		ValidationBL
		ValidationLQBMain
		GeneralApplicationBlockRules
		XAApplicationBlockRules
		BLApplicationBlockRules
		CCApplicationBlockRules
		HEApplicationBlockRules
		PLApplicationBlockRules
		VLApplicationBlockRules
		GeneralCustomListRules
		XACustomListRules
		BLCustomListRules
		CCCustomListRules
		HECustomListRules
		PLCustomListRules
		VLCustomListRules
		XACustomApplicationScenarios
    End Enum

    Public Enum ModuleName
        XA
        CC
        PL
        VL
        HE
        CC_COMBO
        HE_COMBO
        PL_COMBO
        VL_COMBO
        UNDEFINDED
        BL
        LQB
    End Enum

    Public Shared StateList As Dictionary(Of String, String) = New Dictionary(Of String, String) From {{"AA", "Armed Forces - Americas"}, {"AE", "Armed Forces – Europe"}, {"AL", "Alabama"}, {"AK", "Alaska"}, {"AP", "Armed Forces – Pacific"}, {"AS", "American Samoa"}, {"AZ", "Arizona"}, {"AR", "Arkansas"}, {"CA", "California"}, {"CO", "Colorado"}, {"CT", "Connecticut"}, {"DE", "Delaware"}, {"DC", "District Of Columbia"}, {"FL", "Florida"}, {"FM", "Federated States Of Micronesia"}, {"GA", "Georgia"}, {"GU", "Guam"}, {"HI", "Hawaii"}, {"ID", "Idaho"}, {"IL", "Illinois"}, {"IN", "Indiana"}, {"IA", "Iowa"}, {"KS", "Kansas"}, {"KY", "Kentucky"}, {"LA", "Louisiana"}, {"MA", "Massachusetts"}, {"MH", "Marshall Islands"}, {"ME", "Maine"}, {"MD", "Maryland"}, {"MI", "Michigan"}, {"MN", "Minnesota"}, {"MS", "Mississippi"}, {"MO", "Missouri"}, {"MT", "Montana"}, {"NE", "Nebraska"}, {"MP", "Northern Mariana Islands"}, {"NV", "Nevada"}, {"NH", "New Hampshire"}, {"NJ", "New Jersey"}, {"NM", "New Mexico"}, {"NY", "New York"}, {"NC", "North Carolina"}, {"ND", "North Dakota"}, {"OH", "Ohio"}, {"OK", "Oklahoma"}, {"OR", "Oregon"}, {"PA", "Pennsylvania"}, {"PR", "Puerto Rico"}, {"PW", "Palau"}, {"RI", "Rhode Island"}, {"SC", "South Carolina"}, {"SD", "South Dakota"}, {"TN", "Tennessee"}, {"TX", "Texas"}, {"UT", "Utah"}, {"VI", "Virgin Islands"}, {"VT", "Vermont"}, {"VA", "Virginia"}, {"WA", "Washington"}, {"WV", "West rginia"}, {"WI", "Wisconsin"}, {"WY", "Wyoming"}}

    Public Enum LosDomain
		<Description("Beta")>
		beta
		<Description("Demo")>
		demo
		<Description("Cs")>
		cs
		<Description("Ws")>
		ws
	End Enum

	Public Enum LoanType
		<Description("XA")>
		XA
		<Description("CC")>
		CC
		<Description("HE")>
		HE
		<Description("PL")>
		PL
		<Description("VL")>
		VL
	End Enum

	Public Enum LandingPageStatus
		<Description("Active")>
		Active = 1
		<Description("In active")>
		InActive = 0
		<Description("Deleted")>
		Deleted = 2
	End Enum

End Class
