﻿Imports System.Data
Imports System.Net
Imports System.IO
Imports Newtonsoft.Json
Imports Sm
Imports Sm.BO
Imports DBUtils
Imports LPQMobile.DBManager
Imports LPQMobile.BO
Imports Microsoft.VisualBasic
Imports System.Xml
Imports LPQMobile.Utils
Imports System.Text.RegularExpressions
Imports System.Text
Imports System.Web
Public Class SmBL
	Protected LPQMOBILE_CONNECTIONSTRING As String = System.Web.Configuration.WebConfigurationManager.ConnectionStrings("LPQMOBILE_CONNECTIONSTRING").ConnectionString
	Protected LQB_CONNECTIONSTRING As String = System.Web.Configuration.WebConfigurationManager.ConnectionStrings("LQB_CONNECTIONSTRING").ConnectionString

	Private Shared _log As log4net.ILog = log4net.LogManager.GetLogger(GetType(SmBL))

	Const LENDERCONFIG_DRAFTS_TABLE As String = "LenderConfigDrafts"
	Const LENDERCONFIG_TABLE As String = "LenderConfigs"
	Const LENDERCONFIG_LOGS_TABLE As String = "LenderConfigLogs"
	Const LENDERCONFIG_PREVIEWS_TABLE As String = "LenderConfigPreviews"
	Const BACKOFFICELENDERS_TABLE As String = "BackOfficeLenders"
	Const LENDERVENDORS_TABLE As String = "LenderVendors"
	Const USERROLES_TABLE As String = "UserRoles"
	Const USERS_TABLE As String = "Users"
	Const ACTIONLOGS_TABLE As String = "ActionLogs"
	Const CACHED_CONFIGS As String = "CachedConfigs"
	Const LOANS_TABLE As String = "Loans"
	Const LANDINGPAGES_TABLE As String = "LandingPages"
	Const BROADCASTS_TABLE As String = "Broadcasts"
	Const GLOBAL_CONFIGS_TABLE As String = "GlobalConfigs"
	'Const VENDORAGENTPROFILES_TABLE As String = "VendorAgentProfiles"

	Public Function GetLiveConfig(lenderConfigID As Guid, Optional pbDecrypt As Boolean = False) As CLenderConfig
		Dim oManager As New CLenderConfigManager()
		Dim lenderConfig = oManager.GetLenderConfigByLenderConfigID(lenderConfigID, pbDecrypt)	'GetLiveConfig is used by APM and need api credential to download purpose
		If lenderConfig.ConfigData Is Nothing Then
			_log.InfoFormat("Could not load lender config with lenderConfigID={0}", lenderConfigID)
		End If
		Return lenderConfig
	End Function

	Private Sub CleanExpiredDraft()
		Dim oDB As New CSQLDBUtils(LPQMOBILE_CONNECTIONSTRING)
		Try
			Dim oWhere As New CSQLWhereStringBuilder()
			oWhere.AppendAndCondition("DATEADD(hour, 48, CreatedDate) <= getdate()")
			Dim oDelete As New CSQLDeleteStringBuilder(LENDERCONFIG_DRAFTS_TABLE, oWhere)
			oDB.executeNonQuery(oDelete.SQL, False)
		Catch ex As Exception
			_log.Error("Error while CleanExpiredDraft", ex)
		Finally
			oDB.closeConnection()
		End Try
	End Sub

    Public Function GetDraftRevisionID(pathList As List(Of String), lenderConfigID As Guid, userID As Guid) As Integer
        CleanExpiredDraft()
        Dim revisionID As Integer = 0
        Dim oData As DataTable = New DataTable()
        Dim sSQL As String = "SELECT MIN(RevisionID) as RevisionID FROM " & LENDERCONFIG_DRAFTS_TABLE
        Dim oWhere As New CSQLWhereStringBuilder()
        oWhere.AppendAndCondition("LenderConfigID={0}", New CSQLParamValue(lenderConfigID))
        oWhere.AppendAndIn("NodePath", pathList.Select(Function(p) New CSQLParamValue(p)).ToList())
        oWhere.AppendAndCondition("CreatedByUserID={0}", New CSQLParamValue(userID))
        Dim oDB As New CSQLDBUtils(LPQMOBILE_CONNECTIONSTRING)
        Try
            revisionID = Common.SafeInteger(oDB.getScalerValue(sSQL & oWhere.SQL))
        Catch ex As Exception
            _log.Error("Error while GetDraftRevisionID", ex)
        Finally
            oDB.closeConnection()
        End Try
        Return revisionID
    End Function
    Public Function getCachedDiffData(ByVal psPreviousConfigData As String, ByVal psCurrentConfigData As String) As String
        Dim sbDiff As New StringBuilder()
        Dim diffGramStr As String = ""
        Dim sCachedDiff As String = ""
        Dim oPreviousXMLDoc As New XmlDocument()
        Dim oCurrentXMLDoc As New XmlDocument()
        oPreviousXMLDoc.LoadXml(psPreviousConfigData)
        oCurrentXMLDoc.LoadXml(psCurrentConfigData)
        oPreviousXMLDoc = Common.EncryptAllPasswordInConfigXml(oPreviousXMLDoc)
        oCurrentXMLDoc = Common.EncryptAllPasswordInConfigXml(oCurrentXMLDoc)
        Dim xmlComparer As New CXmlComparer(oPreviousXMLDoc.OuterXml, oCurrentXMLDoc.OuterXml)
        If xmlComparer.Compare(diffGramStr) = False Then
            xmlComparer.ParseDiffGram(diffGramStr, sbDiff)
        End If
        sCachedDiff = sbDiff.ToString()
        If String.IsNullOrWhiteSpace(sCachedDiff) Then sCachedDiff = "No changes"
        Return sCachedDiff
    End Function
    Public Function GetDraftRevisionID(nodeGroup As SmSettings.NodeGroup, lenderConfigID As Guid, userID As Guid) As KeyValuePair(Of Integer, DateTime)
		CleanExpiredDraft()
		Dim oData As DataTable = New DataTable()
		Dim sSQL As String = "SELECT MIN(RevisionID) as RevisionID, CreatedDate FROM " & LENDERCONFIG_DRAFTS_TABLE
		Dim oWhere As New CSQLWhereStringBuilder()
		Dim result As New KeyValuePair(Of Integer, DateTime)(0, Now)
		oWhere.AppendAndCondition("LenderConfigID={0}", New CSQLParamValue(lenderConfigID))
		oWhere.AppendAndCondition("NodeGroup={0}", New CSQLParamValue(nodeGroup.ToString()))
		oWhere.AppendAndCondition("CreatedByUserID={0}", New CSQLParamValue(userID))
		Dim oDB As New CSQLDBUtils(LPQMOBILE_CONNECTIONSTRING)
		Try
			oData = oDB.getDataTable(sSQL & oWhere.SQL & " GROUP BY CreatedDate")
		Catch ex As Exception
			_log.Error("Error while GetDraftRevisionID", ex)
		Finally
			oDB.closeConnection()
		End Try
		If oData IsNot Nothing AndAlso oData.Rows.Count > 0 Then
			Try
				Dim revisionID As Integer = Common.SafeInteger(oData.Rows(0)("RevisionID"))
				Dim createdDate As Date = Common.SafeDate(oData.Rows(0)("CreatedDate"))
				result = New KeyValuePair(Of Integer, Date)(revisionID, createdDate)
			Catch ex As Exception
				_log.Error("Cannot read xml node from Config Draft record", ex)
			End Try
		End If
		Return result
	End Function

	'Public Function GetDraftRevisionID(nodeGroup As SmSettings.NodeGroup, lenderConfigID As Guid, userID As Guid) As Integer
	'	Dim revisionID As Integer = 0
	'	Dim sSQL As String = "SELECT MIN(RevisionID) as RevisionID FROM " & LENDERCONFIG_DRAFTS_TABLE
	'	Dim oWhere As New CSQLWhereStringBuilder()
	'	oWhere.AppendAndCondition("LenderConfigID={0}", New CSQLParamValue(lenderConfigID))
	'	oWhere.AppendAndCondition("NodeGroup={0}", New CSQLParamValue(nodeGroup.ToString()))
	'	oWhere.AppendAndCondition("CreatedByUserID={0}", New CSQLParamValue(userID))
	'	Dim oDB As New CSQLDBUtils(LPQMOBILE_CONNECTIONSTRING)
	'	Try
	'		revisionID = Common.SafeInteger(oDB.getScalerValue(sSQL & oWhere.SQL))
	'	Catch ex As Exception
	'		_log.Error("Error while GetDraftRevisionID", ex)
	'	Finally
	'		oDB.closeConnection()
	'	End Try
	'	Return revisionID
	'End Function


	Public Function GetNodeFromDraft(path As String, userID As Guid, lenderConfigID As Guid) As XmlNode
		CleanExpiredDraft()
		Dim oData As DataTable = New DataTable()
		Dim sSQL As String = "Select top 1 * From " & LENDERCONFIG_DRAFTS_TABLE
		Dim oWhere As New CSQLWhereStringBuilder()
		oWhere.AppendAndCondition("LenderConfigID={0}", New CSQLParamValue(lenderConfigID))
		oWhere.AppendAndCondition("NodePath={0}", New CSQLParamValue(path.ToUpper()))
		oWhere.AppendAndCondition("CreatedByUserID={0}", New CSQLParamValue(userID))
		Dim oDB As New CSQLDBUtils(LPQMOBILE_CONNECTIONSTRING)
		Try
			oData = oDB.getDataTable(sSQL & oWhere.SQL & " ORDER BY ID DESC")
		Catch ex As Exception
			_log.Error("Error while GetNodeFromDraft", ex)
		Finally
			oDB.closeConnection()
		End Try
		If oData IsNot Nothing AndAlso oData.Rows.Count > 0 Then
			Try
				Dim xDoc As New XmlDocument()
				xDoc.LoadXml(Common.SafeString(oData.Rows(0)("NodeContent")))
				Return xDoc.DocumentElement
			Catch ex As Exception
				_log.Error("Cannot read xml node from Config Draft record", ex)
			End Try
		End If
		Return Nothing
	End Function


	Public Function GetNodesFromDraft(pathList As List(Of String), userID As Guid, lenderConfigID As Guid) As Dictionary(Of String, XmlNode)
		CleanExpiredDraft()
		Dim result As New Dictionary(Of String, XmlNode)
		Dim oData As DataTable = New DataTable()
		Dim sSQL As String = "Select * From " & LENDERCONFIG_DRAFTS_TABLE
		Dim oWhere As New CSQLWhereStringBuilder()
		oWhere.AppendAndCondition("LenderConfigID={0}", New CSQLParamValue(lenderConfigID))
		oWhere.AppendAndCondition("CreatedByUserID={0}", New CSQLParamValue(userID))
		oWhere.AppendAndIn("NodePath", pathList.Select(Function(p) New CSQLParamValue(p)).ToList())
		Dim oDB As New CSQLDBUtils(LPQMOBILE_CONNECTIONSTRING)
		Try
			oData = oDB.getDataTable(sSQL & oWhere.SQL & " ORDER BY ID")
		Catch ex As Exception
			_log.Error("Error while GetNodesFromDraft", ex)
		Finally
			oDB.closeConnection()
		End Try
		If oData IsNot Nothing AndAlso oData.Rows.Count > 0 Then
			Try
				For Each row As DataRow In oData.Rows
					Dim xDoc As New XmlDocument()
					xDoc.LoadXml(Common.SafeString(row("NodeContent")))
					result.Add(Common.SafeString(row("NodePath")), xDoc.DocumentElement)
				Next
			Catch ex As Exception
				_log.Error("Cannot read xml nodes from Config Draft record", ex)
			End Try
		End If
		Return result
	End Function

	Public Function GetLenderConfigDrafts(lenderConfigID As Guid, createdByUserID As Guid, nodeGroup As SmSettings.NodeGroup) As List(Of SmLenderConfigDraft)
		CleanExpiredDraft()
		Dim result As New List(Of SmLenderConfigDraft)
		Dim oData As DataTable
		Dim sSQL As String = "SELECT * FROM " & LENDERCONFIG_DRAFTS_TABLE
		Dim oWhere As New CSQLWhereStringBuilder()
		oWhere.AppendAndCondition("LenderConfigID={0}", New CSQLParamValue(lenderConfigID))
		oWhere.AppendAndCondition("CreatedByUserID={0}", New CSQLParamValue(createdByUserID))
		If nodeGroup <> SmSettings.NodeGroup.Undefinded Then
			oWhere.AppendAndCondition("NodeGroup={0}", New CSQLParamValue(nodeGroup.ToString()))
		End If
		Dim oDB As New CSQLDBUtils(LPQMOBILE_CONNECTIONSTRING)
		Try
			oData = oDB.getDataTable(sSQL & oWhere.SQL)
			If oData IsNot Nothing AndAlso oData.Rows.Count > 0 Then
				For Each row As DataRow In oData.Rows
					Dim item As New SmLenderConfigDraft()
					item.ID = Common.SafeInteger(row("ID"))
					item.CreatedByUserID = Common.SafeGUID(row("CreatedByUserID"))
					item.CreatedDate = Common.SafeDate(row("CreatedDate"))
					item.NodePath = Common.SafeString(row("NodePath"))
					item.NodeContent = Common.SafeString(row("NodeContent"))
					item.LenderConfigID = Common.SafeGUID(row("LenderConfigID"))
					item.NodeGroup = CType([Enum].Parse(GetType(SmSettings.NodeGroup), Common.SafeString(row("NodeGroup"))), SmSettings.NodeGroup)
					result.Add(item)
				Next
			End If
		Catch ex As Exception
			_log.Error("Error while GetLenderConfigDrafts", ex)
		Finally
			oDB.closeConnection()
		End Try
		Return result
	End Function

	Public Function CleanLenderConfigDrafts(lenderConfigID As Guid, createdByUserID As Guid, nodeGroup As SmSettings.NodeGroup) As String
		Dim result As String = "error"
		Dim oWhere As New CSQLWhereStringBuilder()
		oWhere.AppendAndCondition("LenderConfigID={0}", New CSQLParamValue(lenderConfigID))
		oWhere.AppendAndCondition("CreatedByUserID={0}", New CSQLParamValue(createdByUserID))
		oWhere.AppendAndCondition("NodeGroup={0}", New CSQLParamValue(nodeGroup.ToString()))
		Dim oDB As New CSQLDBUtils(LPQMOBILE_CONNECTIONSTRING)
		Try
			Dim oDelete As New CSQLDeleteStringBuilder(LENDERCONFIG_DRAFTS_TABLE, oWhere)
			oDB.executeNonQuery(oDelete.SQL, False)
			result = "ok"
		Catch ex As Exception
			_log.Error("Error while CleanLenderConfigDrafts", ex)
		Finally
			oDB.closeConnection()
		End Try
		Return result
	End Function

	Public Function SaveNodesToDraft(ParamArray nodeBatchs() As SmLenderConfigDraft) As Boolean
		Dim oDB As New CSQLDBUtils(LPQMOBILE_CONNECTIONSTRING)
		Dim isSuccess As Boolean = False
		Try
			For Each item As SmLenderConfigDraft In nodeBatchs
				Dim oWhere As New CSQLWhereStringBuilder()
				oWhere.AppendAndCondition("NodePath={0}", New CSQLParamValue(item.NodePath))
				oWhere.AppendAndCondition("CreatedByUserID={0}", New CSQLParamValue(item.CreatedByUserID))
				oWhere.AppendAndCondition("LenderConfigID={0}", New CSQLParamValue(item.LenderConfigID))
				Dim oDelete As New CSQLDeleteStringBuilder(LENDERCONFIG_DRAFTS_TABLE, oWhere)
				oDB.executeNonQuery(oDelete.SQL, True)
				If item.NodeContent IsNot Nothing Then
					Dim oInsert As New cSQLInsertStringBuilder(LENDERCONFIG_DRAFTS_TABLE)
					oInsert.AppendValue("LenderConfigID", New CSQLParamValue(item.LenderConfigID))
					oInsert.AppendValue("CreatedByUserID", New CSQLParamValue(item.CreatedByUserID))
					oInsert.AppendValue("NodePath", New CSQLParamValue(item.NodePath))
					oInsert.AppendValue("NodeGroup", New CSQLParamValue(item.NodeGroup.ToString()))
					oInsert.AppendValue("NodeContent", New CSQLParamValue(item.NodeContent))
					oDB.executeNonQuery(oInsert.SQL, True)
				End If
			Next
			oDB.commitTransactionIfOpen()
			isSuccess = True
		Catch ex As Exception
			_log.Error("Error while SaveNodesToDraft", ex)
			oDB.rollbackTransactionIfOpen()
		Finally
			oDB.closeConnection()
		End Try
		Return isSuccess
	End Function
	Public Function SaveNodesToDraft(revisionID As Integer, nodeBatchs() As SmLenderConfigDraft) As Boolean
		Dim oDB As New CSQLDBUtils(LPQMOBILE_CONNECTIONSTRING)
		Dim isSuccess As Boolean = False
		Try
			For Each item As SmLenderConfigDraft In nodeBatchs
				Dim oWhere As New CSQLWhereStringBuilder()
				oWhere.AppendAndCondition("NodePath={0}", New CSQLParamValue(item.NodePath))
				oWhere.AppendAndCondition("CreatedByUserID={0}", New CSQLParamValue(item.CreatedByUserID))
				oWhere.AppendAndCondition("LenderConfigID={0}", New CSQLParamValue(item.LenderConfigID))
				''for merge items in the RATES NODE not override it
				'' get rates from database before deleting     
				'If item.NodePath = CUSTOM_LIST_RATES() Then
				'	Dim oData = oDB.getDataTable(String.Format("SELECT NodeContent FROM {0} {1}", LENDERCONFIG_DRAFTS_TABLE, oWhere.SQL))
				'	If oData IsNot Nothing AndAlso oData.Rows.Count > 0 Then
				'		Dim sContent = Common.SafeString(oData.Rows(0)("NodeContent"))
				'		item.NodeContent = getUpdateRateNodeContent(sContent, item.NodeContent)
				'	End If
				'End If
				Dim oDelete As New CSQLDeleteStringBuilder(LENDERCONFIG_DRAFTS_TABLE, oWhere)
				oDB.executeNonQuery(oDelete.SQL, True)
				If item.NodeContent IsNot Nothing Then
					Dim oInsert As New cSQLInsertStringBuilder(LENDERCONFIG_DRAFTS_TABLE)
					oInsert.AppendValue("LenderConfigID", New CSQLParamValue(item.LenderConfigID))
					oInsert.AppendValue("CreatedByUserID", New CSQLParamValue(item.CreatedByUserID))
					oInsert.AppendValue("NodePath", New CSQLParamValue(item.NodePath))
					oInsert.AppendValue("NodeGroup", New CSQLParamValue(item.NodeGroup.ToString()))
					oInsert.AppendValue("NodeContent", New CSQLParamValue(item.NodeContent))
					oInsert.AppendValue("RevisionID", New CSQLParamValue(revisionID))
					oDB.executeNonQuery(oInsert.SQL, True)
				End If
			Next
			oDB.commitTransactionIfOpen()
			isSuccess = True
		Catch ex As Exception
			_log.Error("Error while SaveNodesToDraft", ex)
			oDB.rollbackTransactionIfOpen()
		Finally
			oDB.closeConnection()
		End Try
		Return isSuccess
	End Function
	'Private Function getUpdateRateNodeContent(ByVal sNodeContent As String, ByVal sCurrentNodeContent As String) As String
	'	If (sNodeContent <> "") Then
	'		Dim currentDocs As New XmlDocument
	'		Dim configDocs As New XmlDocument
	'		currentDocs.LoadXml(sCurrentNodeContent)
	'		configDocs.LoadXml(sNodeContent)
	'		Dim currentItems As XmlNodeList = currentDocs.GetElementsByTagName("ITEM")
	'		Dim configItems As XmlNodeList = configDocs.GetElementsByTagName("ITEM")
	'		Dim strConfigItem As String = ""
	'		For Each oConfigItem As XmlElement In configItems
	'			Dim hasLoanType As Boolean = False
	'			For Each oCurrentItem As XmlElement In currentItems
	'				If Common.SafeString(oConfigItem.GetAttribute("loan_type")).ToUpper = Common.SafeString(oCurrentItem.GetAttribute("loan_type")).ToUpper Then
	'					hasLoanType = True
	'				End If
	'			Next
	'			If Not hasLoanType Then
	'				strConfigItem += oConfigItem.OuterXml()
	'			End If
	'		Next
	'		If strConfigItem <> "" Then
	'			Return "<RATES>" + strConfigItem + sCurrentNodeContent.Trim.Substring(7)
	'		End If
	'	End If
	'	Return sCurrentNodeContent
	'End Function
	Private Function publishAllDraft(configXml As XmlDocument, lenderConfigID As Guid, userId As Guid, ParamArray nodeBatchs() As SmLenderConfigDraft) As XmlDocument
		Dim allDraft As List(Of SmLenderConfigDraft) = GetLenderConfigDrafts(lenderConfigID, userId, SmSettings.NodeGroup.Undefinded)
		If nodeBatchs IsNot Nothing AndAlso nodeBatchs.Count > 0 Then
			For Each draftItem As SmLenderConfigDraft In nodeBatchs
				If draftItem.NodeGroup.ToString().StartsWith("ShowField") Then
					configXml = SmUtil.MergeShowFieldItems(configXml, draftItem)
				ElseIf draftItem.NodeGroup.ToString().StartsWith("Validation") Then
					configXml = SmUtil.MergeRequireFieldItems(configXml, draftItem)
				ElseIf draftItem.NodeGroup.ToString().StartsWith("ICERename") Then
					configXml = SmUtil.MergeRenameableItems(configXml, draftItem)
				Else
					configXml = mergeChangesToConfig(configXml, draftItem)
				End If
			Next
		End If
		If allDraft IsNot Nothing AndAlso allDraft.Count > 0 Then
			For Each draftItem As SmLenderConfigDraft In allDraft
				If nodeBatchs IsNot Nothing AndAlso nodeBatchs.Any(Function(p) p.NodeGroup = draftItem.NodeGroup) Then Continue For
				If draftItem.NodeGroup.ToString().StartsWith("ShowField") Then
					configXml = SmUtil.MergeShowFieldItems(configXml, draftItem)
				ElseIf draftItem.NodeGroup.ToString().StartsWith("Validation") Then
					configXml = SmUtil.MergeRequireFieldItems(configXml, draftItem)
				ElseIf draftItem.NodeGroup.ToString().StartsWith("ICERename") Then
					configXml = SmUtil.MergeRenameableItems(configXml, draftItem)
				Else
					configXml = mergeChangesToConfig(configXml, draftItem)
				End If
			Next
		End If
		Return configXml
	End Function

	Public Function PublishNodes(lenderConfigID As Guid, userId As Guid, remoteIP As String, publishComment As String, publishAll As Boolean, ParamArray nodeBatchs() As SmLenderConfigDraft) As Boolean
		CleanExpiredDraft()
		Dim oDB As New CSQLDBUtils(LPQMOBILE_CONNECTIONSTRING)
		Dim isSuccess As Boolean = False
		Try
			Dim configStr As String = ""
			Dim liveConfig As CLenderConfig = GetLiveConfig(lenderConfigID)
			If liveConfig IsNot Nothing Then
				Dim configXml As New XmlDocument()
				configXml.LoadXml(liveConfig.ConfigData)
				If publishAll Then
					configXml = publishAllDraft(configXml, lenderConfigID, userId, nodeBatchs)
				Else
					configXml = mergeChangesToConfig(configXml, nodeBatchs)
				End If

				Dim sValidationResult = SmUtil.ValidateXMLConstruction(configXml)
				If sValidationResult <> "" Then
					Throw New Exception("XML Construction failed XSD Validation: " & sValidationResult)
				End If

				configStr = Common.EncryptAllPasswordInConfigXml(configXml).OuterXml
			End If

			''update live config data
			Dim oWhere As New CSQLWhereStringBuilder()
			oWhere.AppendAndCondition("LenderConfigID={0}", New CSQLParamValue(lenderConfigID))
			Dim oUpdate As New cSQLUpdateStringBuilder(LENDERCONFIG_TABLE, oWhere)
			oUpdate.appendvalue("ConfigData", New CSQLParamValue(configStr))
			oUpdate.appendvalue("LastModifiedDate", New CSQLParamValue(Now))
			oUpdate.appendvalue("LastModifiedByUserID", New CSQLParamValue(userId))
			oUpdate.appendvalue("RevisionID", New CSQLParamValue(liveConfig.RevisionID + 1))
			oDB.executeNonQuery(oUpdate.SQL, True)
			SmBL.InvalidateExternallyCachedPortalConfigs(liveConfig.LenderRef, configStr)

			'insert log
			Dim oInsert As New cSQLInsertStringBuilder(LENDERCONFIG_LOGS_TABLE)
			oInsert.AppendValue("LenderConfigLogID", New CSQLParamValue(System.Guid.NewGuid))
			oInsert.AppendValue("LenderConfigID", New CSQLParamValue(liveConfig.ID))
			oInsert.AppendValue("OrganizationID", New CSQLParamValue(liveConfig.OrganizationId))
			oInsert.AppendValue("LenderID", New CSQLParamValue(Common.SafeGUID(liveConfig.LenderId)))
			oInsert.AppendValue("LenderRef", New CSQLParamValue(liveConfig.LenderRef))
			oInsert.AppendValue("ReturnURL", New CSQLParamValue(liveConfig.ReturnURL))
			oInsert.AppendValue("LPQLogin", New CSQLParamValue(liveConfig.LPQLogin))
			oInsert.AppendValue("LPQPW", New CSQLParamValue(Common.EncryptPassword(liveConfig.LPQPW)))
			oInsert.AppendValue("LPQURL", New CSQLParamValue(liveConfig.LPQURL))
			oInsert.AppendValue("ConfigData", New CSQLParamValue(CStringCompressor.Compress(configStr)))
			oInsert.AppendValue("LogTime", New CSQLParamValue(Now))
			oInsert.AppendValue("RemoteIP", New CSQLParamValue(remoteIP))
			oInsert.AppendValue("LastModifiedByUserID", New CSQLParamValue(userId))
			oInsert.AppendValue("note", New CSQLParamValue(""))
			oInsert.AppendValue("Comment", New CSQLParamValue(publishComment))
            oInsert.AppendValue("RevisionID", New CSQLParamValue(liveConfig.RevisionID + 1))
            oInsert.AppendValue("CachedDiff", New CSQLParamValue(getCachedDiffData(liveConfig.ConfigData, configStr)))
            oDB.executeNonQuery(oInsert.SQL, True)
			''clean drafts
			For Each item As SmLenderConfigDraft In nodeBatchs
				oWhere = New CSQLWhereStringBuilder()
				If Not publishAll Then
					oWhere.AppendAndCondition("NodePath={0}", New CSQLParamValue(item.NodePath))
				End If
				oWhere.AppendAndCondition("CreatedByUserID={0}", New CSQLParamValue(item.CreatedByUserID))
				oWhere.AppendAndCondition("LenderConfigID={0}", New CSQLParamValue(lenderConfigID))
				Dim oDelete As New CSQLDeleteStringBuilder(LENDERCONFIG_DRAFTS_TABLE, oWhere)
				oDB.executeNonQuery(oDelete.SQL, True)
			Next
			oDB.commitTransactionIfOpen()
			isSuccess = True
		Catch ex As Exception
			_log.Error("Error while PublishNodes", ex)
			oDB.rollbackTransactionIfOpen()
		Finally
			oDB.closeConnection()
		End Try
		Return isSuccess
	End Function

	Public Function PublishNodes(lenderConfigID As Guid, userId As Guid, remoteIP As String, publishComment As String, publishAll As Boolean, callback As Func(Of XmlDocument, SmLenderConfigDraft, XmlDocument), nodeBatch As SmLenderConfigDraft) As Boolean
		CleanExpiredDraft()
		Dim oDB As New CSQLDBUtils(LPQMOBILE_CONNECTIONSTRING)
		Dim isSuccess As Boolean = False
		Try
			Dim configStr As String = ""
			Dim liveConfig As CLenderConfig = GetLiveConfig(lenderConfigID)
			If liveConfig IsNot Nothing Then
				Dim configXml As New XmlDocument()
				configXml.LoadXml(liveConfig.ConfigData)
				If publishAll Then
					configXml = publishAllDraft(configXml, lenderConfigID, userId, nodeBatch)
				Else
					configXml = callback(configXml, nodeBatch)
				End If
				Dim sValidationResult = SmUtil.ValidateXMLConstruction(configXml)
				If sValidationResult <> "" Then
					Throw New Exception("XML Construction failed XSD Validation: " & sValidationResult)
				End If

				configStr = Common.EncryptAllPasswordInConfigXml(configXml).OuterXml
			End If

			''update live config data
			Dim oWhere As New CSQLWhereStringBuilder()
			oWhere.AppendAndCondition("LenderConfigID={0}", New CSQLParamValue(lenderConfigID))
			Dim oUpdate As New cSQLUpdateStringBuilder(LENDERCONFIG_TABLE, oWhere)
			oUpdate.appendvalue("ConfigData", New CSQLParamValue(configStr))
			oUpdate.appendvalue("LastModifiedDate", New CSQLParamValue(Now))
			oUpdate.appendvalue("LastModifiedByUserID", New CSQLParamValue(userId))
			oUpdate.appendvalue("RevisionID", New CSQLParamValue(liveConfig.RevisionID + 1))
			oDB.executeNonQuery(oUpdate.SQL, True)
			SmBL.InvalidateExternallyCachedPortalConfigs(liveConfig.LenderRef, configStr)

			'insert log
			Dim oInsert As New cSQLInsertStringBuilder(LENDERCONFIG_LOGS_TABLE)
			oInsert.AppendValue("LenderConfigLogID", New CSQLParamValue(System.Guid.NewGuid))
			oInsert.AppendValue("LenderConfigID", New CSQLParamValue(liveConfig.ID))
			oInsert.AppendValue("OrganizationID", New CSQLParamValue(liveConfig.OrganizationId))
			oInsert.AppendValue("LenderID", New CSQLParamValue(Common.SafeGUID(liveConfig.LenderId)))
			oInsert.AppendValue("LenderRef", New CSQLParamValue(liveConfig.LenderRef))
			oInsert.AppendValue("ReturnURL", New CSQLParamValue(liveConfig.ReturnURL))
			oInsert.AppendValue("LPQLogin", New CSQLParamValue(liveConfig.LPQLogin))
			oInsert.AppendValue("LPQPW", New CSQLParamValue(Common.EncryptPassword(liveConfig.LPQPW)))
			oInsert.AppendValue("LPQURL", New CSQLParamValue(liveConfig.LPQURL))
			oInsert.AppendValue("ConfigData", New CSQLParamValue(CStringCompressor.Compress(configStr)))
			oInsert.AppendValue("LogTime", New CSQLParamValue(Now))
			oInsert.AppendValue("RemoteIP", New CSQLParamValue(remoteIP))
			oInsert.AppendValue("LastModifiedByUserID", New CSQLParamValue(userId))
			oInsert.AppendValue("note", New CSQLParamValue(""))
			oInsert.AppendValue("Comment", New CSQLParamValue(publishComment))
            oInsert.AppendValue("RevisionID", New CSQLParamValue(liveConfig.RevisionID + 1))
            oInsert.AppendValue("CachedDiff", New CSQLParamValue(getCachedDiffData(liveConfig.ConfigData, configStr)))
            oDB.executeNonQuery(oInsert.SQL, True)       
            ''clean drafts
            oWhere = New CSQLWhereStringBuilder()
			If Not publishAll Then
				oWhere.AppendAndCondition("NodePath={0}", New CSQLParamValue(nodeBatch.NodePath))
			End If
			oWhere.AppendAndCondition("CreatedByUserID={0}", New CSQLParamValue(nodeBatch.CreatedByUserID))
			oWhere.AppendAndCondition("LenderConfigID={0}", New CSQLParamValue(lenderConfigID))
			Dim oDelete As New CSQLDeleteStringBuilder(LENDERCONFIG_DRAFTS_TABLE, oWhere)
			oDB.executeNonQuery(oDelete.SQL, True)

			oDB.commitTransactionIfOpen()
			isSuccess = True
		Catch ex As Exception
			_log.Error("Error while PublishNodes", ex)
			oDB.rollbackTransactionIfOpen()
		Finally
			oDB.closeConnection()
		End Try
		Return isSuccess
	End Function

	Public Function MigrateNodes(lenderConfigID As Guid, userId As Guid, remoteIP As String, publishComment As String, callback As Func(Of XmlDocument, SmLenderConfigDraft, XmlDocument), nodeBatch As SmLenderConfigDraft) As Boolean
		Dim oDB As New CSQLDBUtils(LPQMOBILE_CONNECTIONSTRING)
		Dim isSuccess As Boolean = False
		Try
			Dim configStr As String = ""
			Dim liveConfig As CLenderConfig = GetLiveConfig(lenderConfigID)
			If liveConfig IsNot Nothing Then
				Dim configXml As New XmlDocument()
				configXml.LoadXml(liveConfig.ConfigData)
				configXml = callback(configXml, nodeBatch)

				Dim sValidationResult = SmUtil.ValidateXMLConstruction(configXml)
				If sValidationResult <> "" Then
					'tempoaray comment this out
					'	Throw New Exception("XML Construction failed XSD Validation: " & sValidationResult)
				End If

				configStr = Common.EncryptAllPasswordInConfigXml(configXml).OuterXml
			End If

			''update live config data
			Dim oWhere As New CSQLWhereStringBuilder()
			oWhere.AppendAndCondition("LenderConfigID={0}", New CSQLParamValue(lenderConfigID))
			Dim oUpdate As New cSQLUpdateStringBuilder(LENDERCONFIG_TABLE, oWhere)
			oUpdate.appendvalue("ConfigData", New CSQLParamValue(configStr))
			oUpdate.appendvalue("LastModifiedDate", New CSQLParamValue(Now))
			oUpdate.appendvalue("LastModifiedByUserID", New CSQLParamValue(userId))
			oUpdate.appendvalue("RevisionID", New CSQLParamValue(liveConfig.RevisionID + 1))
			oDB.executeNonQuery(oUpdate.SQL, True)
			SmBL.InvalidateExternallyCachedPortalConfigs(liveConfig.LenderRef, configStr)

			'insert log
			Dim oInsert As New cSQLInsertStringBuilder(LENDERCONFIG_LOGS_TABLE)
			oInsert.AppendValue("LenderConfigLogID", New CSQLParamValue(System.Guid.NewGuid))
			oInsert.AppendValue("LenderConfigID", New CSQLParamValue(liveConfig.ID))
			oInsert.AppendValue("OrganizationID", New CSQLParamValue(liveConfig.OrganizationId))
			oInsert.AppendValue("LenderID", New CSQLParamValue(Common.SafeGUID(liveConfig.LenderId)))
			oInsert.AppendValue("LenderRef", New CSQLParamValue(liveConfig.LenderRef))
			oInsert.AppendValue("ReturnURL", New CSQLParamValue(liveConfig.ReturnURL))
			oInsert.AppendValue("LPQLogin", New CSQLParamValue(liveConfig.LPQLogin))
			oInsert.AppendValue("LPQPW", New CSQLParamValue(Common.EncryptPassword(liveConfig.LPQPW)))
			oInsert.AppendValue("LPQURL", New CSQLParamValue(liveConfig.LPQURL))
			oInsert.AppendValue("ConfigData", New CSQLParamValue(CStringCompressor.Compress(configStr)))
			oInsert.AppendValue("LogTime", New CSQLParamValue(Now))
			oInsert.AppendValue("RemoteIP", New CSQLParamValue(remoteIP))
			oInsert.AppendValue("LastModifiedByUserID", New CSQLParamValue(userId))
			oInsert.AppendValue("note", New CSQLParamValue(""))
			oInsert.AppendValue("Comment", New CSQLParamValue(publishComment))
            oInsert.AppendValue("RevisionID", New CSQLParamValue(liveConfig.RevisionID + 1))
            oInsert.AppendValue("CachedDiff", New CSQLParamValue(getCachedDiffData(liveConfig.ConfigData, configStr)))
            oDB.executeNonQuery(oInsert.SQL, True)

			''clean drafts
			oWhere = New CSQLWhereStringBuilder()
			oWhere.AppendAndCondition("NodePath={0}", New CSQLParamValue(nodeBatch.NodePath))
			oWhere.AppendAndCondition("CreatedByUserID={0}", New CSQLParamValue(nodeBatch.CreatedByUserID))
			oWhere.AppendAndCondition("LenderConfigID={0}", New CSQLParamValue(lenderConfigID))
			Dim oDelete As New CSQLDeleteStringBuilder(LENDERCONFIG_DRAFTS_TABLE, oWhere)
			oDB.executeNonQuery(oDelete.SQL, True)

			oDB.commitTransactionIfOpen()
			isSuccess = True
		Catch ex As Exception
			_log.Error("Error while MigrateNodes", ex)
			oDB.rollbackTransactionIfOpen()
		Finally
			oDB.closeConnection()
		End Try
		Return isSuccess
	End Function

	Public Function GeneratePreviewConfig(lenderConfigID As Guid, userName As String, callback As Func(Of CLenderConfig, String, String), ParamArray nodeBatchs() As SmLenderConfigDraft) As String
		Dim previewCode As String = ""
		Dim oDB As New CSQLDBUtils(LPQMOBILE_CONNECTIONSTRING)
		Try
			Dim configStr As String = ""
			Dim liveConfig As CLenderConfig = GetLiveConfig(lenderConfigID)
			If liveConfig IsNot Nothing Then
				Dim configXml As New XmlDocument()
				configXml.LoadXml(liveConfig.ConfigData)
				configStr = Common.EncryptAllPasswordInConfigXml(mergeChangesToConfig(configXml, nodeBatchs)).OuterXml

				Dim sValidationResult = SmUtil.ValidateXMLConstruction(configXml)
				If sValidationResult <> "" Then
					Throw New Exception("XML Construction failed XSD Validation: " & sValidationResult)
				End If

				Dim code As String = Guid.NewGuid().ToString().Replace("-", "").Substring(0, 10)
				Dim oInsert As New cSQLInsertStringBuilder(LENDERCONFIG_PREVIEWS_TABLE)
				oInsert.AppendValue("PreviewCode", New CSQLParamValue(code))
				oInsert.AppendValue("ConfigData", New CSQLParamValue(CStringCompressor.Compress(configStr)))
				oInsert.AppendValue("CreatedDate", New CSQLParamValue(Now))
				oInsert.AppendValue("LenderID", New CSQLParamValue(Common.SafeGUID(liveConfig.LenderId)))
				oInsert.AppendValue("LenderRef", New CSQLParamValue(liveConfig.LenderRef))
				oInsert.AppendValue("LenderConfigID", New CSQLParamValue(liveConfig.ID))

				oInsert.AppendValue("OrganizationID", New CSQLParamValue(liveConfig.OrganizationId))
				oInsert.AppendValue("ReturnURL", New CSQLParamValue(liveConfig.ReturnURL))
				oInsert.AppendValue("LPQLogin", New CSQLParamValue(liveConfig.LPQLogin))
				oInsert.AppendValue("LPQPW", New CSQLParamValue(Common.EncryptPassword(liveConfig.LPQPW)))
				oInsert.AppendValue("LPQURL", New CSQLParamValue(liveConfig.LPQURL))

				oInsert.AppendValue("note", New CSQLParamValue(liveConfig.Note))
				oInsert.AppendValue("LastModifiedByUserID", New CSQLParamValue(liveConfig.LastModifiedByUserID))
				oInsert.AppendValue("LastOpenedByUserID", New CSQLParamValue(liveConfig.LastOpenedByUserID))
				oInsert.AppendValue("IterationID", New CSQLParamValue(liveConfig.IterationID))
				oInsert.AppendValue("LastOpenedDate", New CSQLParamValue(liveConfig.LastOpenedDate))

				oDB.executeNonQuery(oInsert.SQL, True)
				oDB.commitTransactionIfOpen()
				previewCode = callback(liveConfig, code)
			End If
		Catch ex As Exception
			'_log.Error("Error when generate preview code", ex)
			oDB.rollbackTransactionIfOpen()
			Throw New Exception("Error when generate preview code", ex)
		Finally
			oDB.closeConnection()
		End Try
		Return previewCode
	End Function

	Public Function GeneratePreviewConfig(lenderConfigLogID As Guid, callback As Func(Of SmLenderConfigLogItem, String, String)) As String
		Dim previewCode As String = ""
		Dim oDB As New CSQLDBUtils(LPQMOBILE_CONNECTIONSTRING)
		Try
			Dim logItem As SmLenderConfigLogItem = GetLenderConfigLogItemByID(lenderConfigLogID)
			If logItem IsNot Nothing Then
				Dim code As String = Guid.NewGuid().ToString().Replace("-", "").Substring(0, 10)
				Dim oInsert As New cSQLInsertStringBuilder(LENDERCONFIG_PREVIEWS_TABLE)
				oInsert.AppendValue("PreviewCode", New CSQLParamValue(code))
				oInsert.AppendValue("ConfigData", New CSQLParamValue(CStringCompressor.Compress(Common.EncryptAllPasswordInConfigXml(logItem.ConfigData).OuterXml)))
				oInsert.AppendValue("CreatedDate", New CSQLParamValue(Now))
				oInsert.AppendValue("LenderID", New CSQLParamValue(Common.SafeGUID(logItem.LenderID)))
				oInsert.AppendValue("LenderRef", New CSQLParamValue(logItem.LenderRef))
				oInsert.AppendValue("LenderConfigID", New CSQLParamValue(logItem.LenderConfigID))
				oInsert.AppendValue("OrganizationID", New CSQLParamValue(logItem.OrganizationID))
				oInsert.AppendValue("ReturnURL", New CSQLParamValue(logItem.ReturnURL))
				oInsert.AppendValue("LPQLogin", New CSQLParamValue(logItem.LPQLogin))
				oInsert.AppendValue("LPQPW", New CSQLParamValue(Common.EncryptPassword(logItem.LPQPW)))
				oInsert.AppendValue("LPQURL", New CSQLParamValue(logItem.LPQURL))
				oDB.executeNonQuery(oInsert.SQL, True)
				oDB.commitTransactionIfOpen()
				previewCode = callback(logItem, code)
			End If
		Catch ex As Exception
			'_log.Error("Error when generate preview code", ex)
			oDB.rollbackTransactionIfOpen()
			Throw New Exception("Error when generate preview code", ex)
		Finally
			oDB.closeConnection()
		End Try
		Return previewCode
	End Function

	Private Function mergeChangesToConfig(config As XmlDocument, ParamArray changes() As SmLenderConfigDraft) As XmlDocument
		For Each item As SmLenderConfigDraft In changes
			Dim mDoc As New XmlDocument()
			mDoc.LoadXml(item.NodeContent)
			Dim node As XmlNode = config.SelectSingleNode(item.NodePath)
			If node Is Nothing Then
				Dim match As Match = Regex.Match(item.NodePath, "^(?<path>(?<pnode>[^@]+)/(?<node>[^@/]+))(?:\/@(?<attr>\w+))?$")
				Dim match2 As Match = Regex.Match(item.NodePath, "^(?<path>(?<pnode>[^@]+)/(?<node>[^@/]+))\[(?:@(?<attr>\w+)=[\'](?<attrVal>[A-Za-z]+)[\'])?\]$")
				'Dim match2 As Match = Regex.Match(item.NodePath, "^(?<path>(?<pnode>[^@]+)/(?<node>[^@/]+))\{(@(?<attr>\w+)=[\'](?<attrVal>[A-Za-z]+)[\']])\}?$")

				If match.Success Then 'Ex: WEBSITE/XA_LOAN/SUCCESS_MESSAGES/SUBMITTED_MESSAGE/@ggggf
					If Not String.IsNullOrEmpty(match.Groups("attr").Value) Then ''attribute node
						config = PrepareNode(config, match.Groups("path").Value)
						'create new attribute and add it to parent node
						node = config.SelectSingleNode(match.Groups("path").Value)
						If node IsNot Nothing Then
							Dim attr As XmlAttribute = config.CreateAttribute(match.Groups("attr").Value)
							attr.Value = mDoc.DocumentElement.GetAttribute(attr.Name)
							node.Attributes.Append(attr)
						End If
					Else ''element node
						config = PrepareNode(config, match.Groups("pnode").Value)
						''get its parent node
						node = config.SelectSingleNode(match.Groups("pnode").Value)
						If node IsNot Nothing Then
							Dim nNode As XmlElement = config.CreateElement(match.Groups("node").Value)
							nNode.InnerXml = mDoc.DocumentElement.InnerXml
							For Each attr As XmlAttribute In mDoc.DocumentElement.Attributes
								Dim cAttr As XmlAttribute = config.CreateAttribute(attr.Name)
								cAttr.Value = attr.Value
								nNode.Attributes.Append(cAttr)
							Next
							node.AppendChild(nNode)
						End If
					End If
				ElseIf match2.Success Then 'Ex: WEBSITE/CUSTOM_LIST/RATES/ITEM{@loan_type='pl']
					If item.NodePath = CUSTOM_LIST_RATES(match2.Groups("attrVal").Value) Then
						config = PrepareAttrNode(config, item.NodePath, match2.Groups("attr").Value, match2.Groups("attrVal").Value)
						'create new attribute and add it to parent node
						node = config.SelectSingleNode(item.NodePath)
						If node IsNot Nothing Then
							Dim attr As XmlAttribute = config.CreateAttribute("rate")
							attr.Value = mDoc.DocumentElement.SelectSingleNode("ITEM[@loan_type='" & match2.Groups("attrVal").Value & "']").Attributes("rate").Value
							node.Attributes.Append(attr)
						End If
					End If
				Else
					'Ex: WEBSITE/@ida=FIS
					match = Regex.Match(item.NodePath, "^(?<path>[^@/]+)\/@(?<attr>\w+)$")
					If match.Success Then
						node = config.SelectSingleNode(match.Groups("path").Value)
						If node IsNot Nothing Then
							Dim attr As XmlAttribute = config.CreateAttribute(match.Groups("attr").Value)
							attr.Value = mDoc.DocumentElement.GetAttribute(attr.Name)
							node.Attributes.Append(attr)
						End If
					End If
				End If
			Else
				Dim match2 As Match = Regex.Match(item.NodePath, "^(?<path>(?<pnode>[^@]+)/(?<node>[^@/]+))\[(?:@(?<attr>\w+)=[\'](?<attrVal>[A-Za-z]+)[\'])?\]$")
				If match2.Success AndAlso item.NodePath = CUSTOM_LIST_RATES(match2.Groups("attrVal").Value) Then
					node.Attributes("rate").Value = mDoc.DocumentElement.SelectSingleNode("ITEM[@loan_type='" & match2.Groups("attrVal").Value & "']").Attributes("rate").Value
				ElseIf node.NodeType = XmlNodeType.Attribute Then
					node.Value = mDoc.DocumentElement.GetAttribute(node.Name)
				Else
					Dim nNode As XmlElement = config.CreateElement(node.Name)
					nNode.InnerXml = mDoc.DocumentElement.InnerXml
					For Each attr As XmlAttribute In mDoc.DocumentElement.Attributes
						Dim cAttr As XmlAttribute = config.CreateAttribute(attr.Name)
						cAttr.Value = attr.Value
						nNode.Attributes.Append(cAttr)
					Next
					' ''Merging update rate to the config(not overriding the existing rate)
					'If node.Name = "RATES" Then
					'	MergeChangesRate(nNode, node)
					'End If
					node.ParentNode.ReplaceChild(nNode, node)
				End If
			End If
		Next
		Return config
	End Function
	'  Private Sub MergeChangesRate(ByVal oCurrentNode As XmlElement, ByVal oConfigNode As XmlNode)
	'      ''Rates nodes
	'      Dim oConfigItems As XmlNodeList = oConfigNode.SelectNodes("ITEM")
	''Dim oCurrentItems As XmlNodeList = oCurrentNode.SelectNodes("ITEM")
	'If oConfigItems.Count = 0 Then Return

	'For Each oConfigItem As XmlElement In oConfigItems
	'	If oCurrentNode.SelectNodes(String.Format("ITEM[@loan_type='{0}']", Common.SafeString(oConfigItem.GetAttribute("loan_type")))).Count > 0 Then Continue For
	'	oCurrentNode.AppendChild(oConfigItem)
	'	'Dim hasLoanType As Boolean = False
	'	'For Each oCurrentItem As XmlElement In oCurrentItems
	'	'	If (Common.SafeString(oConfigItem.GetAttribute("loan_type")) = Common.SafeString(oCurrentItem.GetAttribute("loan_type"))) Then
	'	'		hasLoanType = True
	'	'	End If
	'	'Next
	'	'If Not hasLoanType Then
	'	'	oCurrentNode.AppendChild(oConfigItem)
	'	'End If
	'Next
	'  End Sub

	''' <summary>
	''' create nodes in path in case it's not existed
	''' </summary>
	''' <param name="doc"></param>
	''' <param name="path"></param>
	''' <returns></returns>
	''' <remarks></remarks>
	Private Function PrepareNode(config As XmlDocument, path As String) As XmlDocument
		Dim pathAnalyserPattern As String = "^(?<path>(?<pnode>[^@]+)/(?<node>[^@/]+))$"
		Dim stack As New Stack(Of KeyValuePair(Of String, String))
		Dim curNode As XmlNode = config.SelectSingleNode(path)
		While curNode Is Nothing
			Dim match As Match = Regex.Match(path, pathAnalyserPattern)
			If match.Success = False Then Exit While
			stack.Push(New KeyValuePair(Of String, String)(match.Groups("pnode").Value, match.Groups("node").Value))
			curNode = config.SelectSingleNode(match.Groups("pnode").Value)
		End While
		While stack.Count > 0
			Dim item As KeyValuePair(Of String, String) = stack.Pop()
			Dim childNode As XmlNode = config.CreateElement(item.Value)
			curNode.AppendChild(childNode)
			curNode = childNode
		End While
		Return config
	End Function
	Private Function PrepareAttrNode(config As XmlDocument, path As String, attrName As String, attrValue As String) As XmlDocument
		Dim pathAnalyserPattern As String = "^(?<path>(?<pnode>[^@]+)/(?<node>[^@/\[]+))"
		Dim stack As New Stack(Of KeyValuePair(Of String, String))
		Dim curNode As XmlNode = config.SelectSingleNode(path)
		While curNode Is Nothing
			Dim match As Match = Regex.Match(path, pathAnalyserPattern)
			If match.Success = False Then Exit While
			stack.Push(New KeyValuePair(Of String, String)(match.Groups("pnode").Value, match.Groups("node").Value))
			curNode = config.SelectSingleNode(match.Groups("pnode").Value)
			path = match.Groups("pnode").Value
		End While
		While stack.Count > 0
			Dim item As KeyValuePair(Of String, String) = stack.Pop()
			Dim childNode As XmlNode = config.CreateElement(item.Value)
			curNode.AppendChild(childNode)
			curNode = childNode
		End While
		Dim attr As XmlAttribute = config.CreateAttribute(attrName)
		attr.Value = attrValue
		curNode.Attributes.Append(attr)
		Return config
	End Function

	'Public Function GetPortals(ByRef paging As SmPagingInfoModel, filter As SmBackOfficeLenderListFilter, Optional userID As Guid = Nothing) As List(Of SmBackOfficeLender)
	'	Dim result As New Dictionary(Of Guid, SmBackOfficeLender)
	'	Dim oData As New DataTable()
	'	Dim sSQL As String = String.Format("SELECT B.*, L.LenderConfigID, L.OrganizationID, L.LenderID, L.LenderRef, L.ReturnURL, L.LPQLogin, L.LPQPW, L.LPQURL as LpqUrl1, L.ConfigData, L.note as Note, L.LastModifiedDate, L.CreatedDate, L.LastModifiedBy, L.LastOpenedBy, L.IterationID, L.LastOpenedDate, L.RevisionID, UR.Status, UR.UserRoleID, UR.UserID FROM {0} B INNER JOIN {1} L ON B.LenderID = L.LenderID LEFT JOIN {2} UR ON L.LenderConfigID = UR.LenderConfigID", BACKOFFICELENDERS_TABLE, LENDERCONFIG_TABLE, USERROLES_TABLE)
	'	Dim oWhere As New CSQLWhereStringBuilder()
	'	If filter IsNot Nothing Then
	'		If Not String.IsNullOrEmpty(filter.Keyword) Then
	'			oWhere.AppendAndCondition("(COALESCE(B.LenderName, '') + '$' + COALESCE(B.City, '') + '$' + COALESCE(B.State, '') + '$' + COALESCE(L.LenderRef, '') + '$' + COALESCE(L.note, '')) LIKE {0}", New CSQLParamValue("%" & filter.Keyword & "%"))
	'		End If
	'	End If
	'	If userID <> Nothing Then
	'		oWhere.AppendAndCondition("UR.Status={0}", New CSQLParamValue(SmSettings.UserRoleStatus.Active))
	'		oWhere.AppendAndCondition("UR.UserID={0}", New CSQLParamValue(userID))
	'	End If
	'	Dim oOrder As String = " "
	'	If Not String.IsNullOrEmpty(paging.SortColumn) Then
	'		oOrder = oOrder & "ORDER BY " & paging.SortColumn & " " & paging.SortDirection
	'	Else
	'		oOrder = oOrder & "ORDER BY L.LastModifiedDate DESC"
	'	End If
	'	Dim oDB As New CSQLDBUtils(LPQMOBILE_CONNECTIONSTRING)
	'	Try
	'		oData = oDB.getDataTable(sSQL & oWhere.SQL & oOrder)
	'		If oData IsNot Nothing AndAlso oData.Rows.Count > 0 Then
	'			For Each row As DataRow In oData.Rows
	'				'LenderID in BackOfficeLenders is not quarantee to be unique so need to match by host name too
	'				Dim match1 As Match = Regex.Match(Common.SafeString(row("LpqUrl1")), SmSettings.extractDomainRegexPattern, RegexOptions.IgnoreCase)
	'				Dim match2 As Match = Regex.Match(Common.SafeString(row("LpqUrl")), SmSettings.extractDomainRegexPattern, RegexOptions.IgnoreCase)
	'				If (match1.Success AndAlso match2.Success AndAlso match1.Value = match2.Value) Or row.IsNull("LenderName") Or row.IsNull("LpqUrl") Then
	'					Dim item As New SmBackOfficeLender()
	'					item.BackOfficeLenderID = Common.SafeGUID(row("BackOfficeLenderID"))
	'					item.LenderName = Common.SafeString(row("LenderName"))
	'					item.LenderID = Common.SafeGUID(row("LenderID"))
	'					item.LpqUrl = Common.SafeString(row("LpqUrl"))
	'					item.City = Common.SafeString(row("City"))
	'					item.State = Common.SafeString(row("State"))
	'					If Not row.IsNull("LastModifiedDate") Then
	'						item.LastModifiedDate = Common.SafeDate(row("LastModifiedDate"))
	'					End If
	'					item.LastModifiedBy = Common.SafeString(row("LastModifiedBy"))
	'					item.Host = Common.SafeString(match2.Value)
	'					If Not result.ContainsKey(item.BackOfficeLenderID) Then
	'						result.Add(item.BackOfficeLenderID, item)
	'					End If
	'					If result(item.BackOfficeLenderID) IsNot Nothing Then
	'						Dim portalList = result(item.BackOfficeLenderID).PortalList
	'						If portalList Is Nothing Then
	'							portalList = New List(Of SmLenderConfig)
	'						End If
	'						If Not portalList.Any(Function(p) p.LenderConfigID = Common.SafeGUID(row("LenderConfigID"))) Then
	'							Dim portal As New SmLenderConfig
	'							portal.LenderConfigID = Common.SafeGUID(row("LenderConfigID"))
	'							portal.OrganizationID = Common.SafeGUID(row("OrganizationID"))
	'							portal.LenderID = Common.SafeGUID(row("LenderID"))
	'							portal.LenderRef = Common.SafeString(row("LenderRef"))
	'							portal.ReturnURL = Common.SafeString(row("ReturnURL"))
	'							portal.LPQLogin = Common.SafeString(row("LPQLogin"))
	'							portal.LPQPW = Common.SafeString(row("LPQPW"))
	'							portal.LPQURL = Common.SafeString(row("LPQURL"))
	'							portal.Note = Common.SafeString(row("Note"))
	'							portal.ConfigData = Common.SafeString(row("ConfigData"))
	'							If Not row.IsNull("LastModifiedDate") Then
	'								portal.LastModifiedDate = Common.SafeDate(row("LastModifiedDate"))
	'							End If
	'							portal.CreatedDate = Common.SafeDate(row("CreatedDate"))
	'							If Not row.IsNull("LastOpenedDate") Then
	'								portal.LastModifiedDate = Common.SafeDate(row("LastOpenedDate"))
	'							End If
	'							portal.LastModifiedBy = Common.SafeString(row("LastModifiedBy"))
	'							portal.LastOpenedBy = Common.SafeString(row("LastOpenedBy"))
	'							portal.IterationID = Common.SafeInteger(row("IterationID"))
	'							portal.RevisionID = Common.SafeInteger(row("RevisionID"))
	'							portalList.Add(portal)
	'						End If
	'						result(item.BackOfficeLenderID).PortalList = portalList
	'					End If
	'				End If
	'			Next
	'		End If
	'		paging.TotalRecord = result.Count
	'		If paging.PageIndex > 0 AndAlso paging.PageSize > 0 Then
	'			result = result.Skip((paging.PageIndex - 1) * paging.PageSize).Take(paging.PageSize).ToDictionary(Function(x) x.Key, Function(x) x.Value)
	'		End If
	'	Catch ex As Exception
	'		_log.Error("Error while GetLenders", ex)
	'	Finally
	'		oDB.closeConnection()
	'	End Try
	'	Return result.Values.ToList()
	'End Function


	''' <summary>
	''' Get list of all portals(SmBackOfficeLender) filtered by host then filtered again by org_code and lender_code
	''' This function is db query instensive and cpu instensive due to the parsing of xml config for matching org_code and lender_code
	''' </summary>
	''' <param name="paging"></param>
	''' <param name="filter"></param>
	''' <param name="workingUrl"></param>
	''' <param name="lenderCode"></param>
	''' <param name="orgCode"></param>
	''' <param name="pbDecrypt"></param>
	''' <param name="portalListIncluded"></param>
	''' <returns></returns>
	Public Function GetLendersBySSOSession(ByRef paging As SmPagingInfoModel, filter As SmBackOfficeLenderListFilter, workingUrl As String, lenderCode As String, orgCode As String, Optional pbDecrypt As Boolean = True, Optional portalListIncluded As Boolean = True) As List(Of SmBackOfficeLender)
		Dim workingDomainMatch As Match = Regex.Match(workingUrl, SmSettings.extractDomainRegexPattern, RegexOptions.IgnoreCase)
		If Not workingDomainMatch.Success Then Return New List(Of SmBackOfficeLender) 'there is nothing to do if domain is invalid
		'_log.Error("GetLendersBySSOSession")
		Dim result As New Dictionary(Of Guid, SmBackOfficeLender)
		Dim oData As New DataTable()
		Dim sSQL As String = String.Format("SELECT B.*, L.LenderConfigID, L.OrganizationID, L.LenderID, L.LenderRef, L.ReturnURL, L.LPQLogin, L.LPQPW, L.LPQURL as LpqUrl1, L.ConfigData, L.note as Note, L.LastModifiedDate, L.CreatedDate, L.LastModifiedByUserID, L.LastOpenedByUserID, U.FirstName + ' ' + U.LastName as LastModifiedByUserName, U2.FirstName + ' ' + U2.LastName as LastOpenedByUserName, L.IterationID, L.LastOpenedDate, L.RevisionID FROM {0} B RIGHT JOIN {1} L ON B.LenderID = L.LenderID LEFT JOIN {2} U on L.LastModifiedByUserID = U.UserID LEFT JOIN {2} U2 on L.LastOpenedByUserID = U2.UserID ", BACKOFFICELENDERS_TABLE, LENDERCONFIG_TABLE, USERS_TABLE)
		Dim oWhere As New CSQLWhereStringBuilder()
		'filtering by domain to limits query results
		oWhere.AppendAndCondition("L.LPQURL LIKE {0}", New CSQLParamValue("%" & workingDomainMatch.Groups(1).Value & "%"))
		If filter IsNot Nothing Then
			If Not String.IsNullOrEmpty(filter.Keyword) Then
				oWhere.AppendAndCondition("(COALESCE(B.LenderName, '') + '$' + COALESCE(B.City, '') + '$' + COALESCE(B.State, '') + '$' + COALESCE(L.LenderRef, '') ) LIKE {0}", New CSQLParamValue("%" & filter.Keyword & "%"))
			End If
		End If
		Dim oOrder As String = " "
		If Not String.IsNullOrEmpty(paging.SortColumn) Then
			oOrder = oOrder & "ORDER BY " & paging.SortColumn & " " & paging.SortDirection
		Else
			oOrder = oOrder & "ORDER BY L.LastModifiedDate DESC"
		End If
		Dim oDB As New CSQLDBUtils(LPQMOBILE_CONNECTIONSTRING)
		Try
			oData = oDB.getDataTable(sSQL & oWhere.SQL & oOrder)

			'filter again (one by one) to make sure the result set only returns those records that match the working domain criteria
			Dim matchDomainPattern As String = String.Format("^(?:https?:\/\/)?(?:[^@\n]+@)?(?:www\.)?({0})", workingDomainMatch.Groups(1).Value)
			If oData IsNot Nothing AndAlso oData.Rows.Count > 0 Then
				Dim extractKeyList As New List(Of String)
				extractKeyList.Add("WEBSITE/@org_code")
				If Not String.IsNullOrWhiteSpace(lenderCode) Then
					extractKeyList.Add("WEBSITE/@lender_code")
				End If
				For Each row As DataRow In oData.Rows
					'And LenderID in BackOfficeLenders is not quarantee to be unique so need to match by host name too
					Dim match1 As Match = Regex.Match(Common.SafeString(row("LpqUrl1")), matchDomainPattern, RegexOptions.IgnoreCase)
					Dim match2 As Match = Regex.Match(Common.SafeString(row("LpqUrl")), matchDomainPattern, RegexOptions.IgnoreCase)
					If match1.Success AndAlso match2.Success AndAlso match1.Value = match2.Value Then
						Dim oBackOfficeLender As New SmBackOfficeLender()
						oBackOfficeLender.BackOfficeLenderID = Common.SafeGUID(row("BackOfficeLenderID"))
						oBackOfficeLender.LenderName = Common.SafeString(row("LenderName"))
                        oBackOfficeLender.CustomerInternalID = Common.SafeString(row("CustomerInternalID"))
                        oBackOfficeLender.LenderID = Common.SafeGUID(row("LenderID"))
                        oBackOfficeLender.LpqUrl = Common.SafeString(row("LpqUrl"))
                        oBackOfficeLender.City = Common.SafeString(row("City"))
                        oBackOfficeLender.State = Common.SafeString(row("State"))
                        If Not row.IsNull("LastModifiedDate") Then
                            oBackOfficeLender.LastModifiedDate = Common.SafeDate(row("LastModifiedDate"))
                        End If
                        oBackOfficeLender.LastModifiedByUserID = Common.SafeGUID(row("LastModifiedByUserID"))
                        oBackOfficeLender.LastModifiedByUserName = Common.SafeString(row("LastModifiedByUserName"))
                        oBackOfficeLender.Host = Common.SafeString(match2.Value)
                        If Not result.ContainsKey(oBackOfficeLender.BackOfficeLenderID) Then
                            result.Add(oBackOfficeLender.BackOfficeLenderID, oBackOfficeLender)
                        End If

                        'filter for portals that belong to the same org or lender
                        If portalListIncluded = True AndAlso result(oBackOfficeLender.BackOfficeLenderID) IsNot Nothing Then
                            Dim portalList = result(oBackOfficeLender.BackOfficeLenderID).PortalList
                            If portalList Is Nothing Then
                                portalList = New List(Of SmLenderConfig)
                            End If
                            If Not portalList.Any(Function(p) p.LenderConfigID = Common.SafeGUID(row("LenderConfigID"))) Then

                                'TODO: if this extraction cause the performance issue, may be we need cache the extract result or something like that to speed up
                                'filter by org_code and lender_code
                                Dim configXml As New XmlDocument
                                configXml.LoadXml(Common.SafeString(row("ConfigData")))
                                If pbDecrypt = True Then
                                    configXml = Common.DecryptAllPasswordInConfigXml(configXml)
                                End If
                                Dim extractedData = Common.ExtractDataFromConfigXml(configXml, extractKeyList)
                                If String.IsNullOrEmpty(extractedData("WEBSITE/@org_code")) OrElse extractedData("WEBSITE/@org_code").ToUpper() <> orgCode.ToUpper() Then
                                    Continue For
                                ElseIf Not String.IsNullOrEmpty(lenderCode) AndAlso Not String.IsNullOrEmpty(extractedData("WEBSITE/@lender_code")) AndAlso extractedData("WEBSITE/@lender_code").ToUpper() <> lenderCode.ToUpper() Then
                                    'If LenderCode is null, this is org level so allow access to all lenders with the same org_code
                                    Continue For
                                End If
                                Dim portal As New SmLenderConfig
                                portal.LenderConfigID = Common.SafeGUID(row("LenderConfigID"))
                                portal.OrganizationID = Common.SafeGUID(row("OrganizationID"))
                                portal.LenderID = Common.SafeGUID(row("LenderID"))
                                portal.LenderRef = Common.SafeString(row("LenderRef"))
                                portal.ReturnURL = Common.SafeString(row("ReturnURL"))
                                portal.LPQLogin = Common.SafeString(row("LPQLogin"))
                                portal.LPQPW = Common.SafeString(row("LPQPW"))
                                If pbDecrypt = True Then
                                    portal.LPQPW = Common.DecryptPassword(portal.LPQPW)
                                End If

                                portal.LPQURL = Common.SafeString(row("LPQURL"))
                                portal.Note = Common.SafeString(row("Note"))
                                portal.ConfigData = configXml.OuterXml
                                If Not row.IsNull("LastModifiedDate") Then
                                    portal.LastModifiedDate = Common.SafeDate(row("LastModifiedDate"))
                                End If
                                portal.CreatedDate = Common.SafeDate(row("CreatedDate"))
                                If Not row.IsNull("LastOpenedDate") Then
                                    portal.LastOpenedDate = Common.SafeDate(row("LastOpenedDate"))
                                End If
                                portal.LastModifiedByUserID = Common.SafeGUID(row("LastModifiedByUserID"))
                                portal.LastModifiedByUserName = Common.SafeString(row("LastModifiedByUserName"))
                                portal.LastOpenedByUserID = Common.SafeGUID(row("LastOpenedByUserID"))
                                portal.LastOpenedByUserName = Common.SafeString(row("LastOpenedByUserName"))
                                portal.IterationID = Common.SafeInteger(row("IterationID"))
                                portal.RevisionID = Common.SafeInteger(row("RevisionID"))
                                portalList.Add(portal)
                            End If
                            result(oBackOfficeLender.BackOfficeLenderID).PortalList = portalList
                        End If
                    End If
                Next
            End If

            'remove lenders which there is no portal
            If portalListIncluded Then
                result = result.Where(Function(p) p.Value.PortalList IsNot Nothing AndAlso p.Value.PortalList.Count > 0).ToDictionary(Function(x) x.Key, Function(x) x.Value)
            End If
            paging.TotalRecord = result.Count
            If paging.PageIndex > 0 AndAlso paging.PageSize > 0 Then
                Dim originalResult = result
                result = originalResult.Skip((paging.PageIndex - 1) * paging.PageSize).Take(paging.PageSize).ToDictionary(Function(x) x.Key, Function(x) x.Value)
                If Not result.Any() Then
                    paging.PageIndex = paging.PageIndex - 1
                    result = originalResult.Skip((paging.PageIndex - 1) * paging.PageSize).Take(paging.PageSize).ToDictionary(Function(x) x.Key, Function(x) x.Value)
                End If
            End If
        Catch ex As Exception
            _log.Error("Error while GetLendersBySSOSession", ex)
        Finally
            oDB.closeConnection()
        End Try
        Return result.Values.ToList()
    End Function

    Public Function GetPortals(ByRef paging As SmPagingInfoModel, filter As SmBackOfficeLenderListFilter, Optional userID As Guid = Nothing) As List(Of SmBackOfficeLender)
        Dim result As New Dictionary(Of Guid, SmBackOfficeLender)
        Dim oData As New DataTable()
        Dim sSQL As String = String.Format("SELECT B.*, L.LenderConfigID, L.OrganizationID, L.LenderID, L.LenderRef, L.ReturnURL, L.LPQLogin, L.LPQPW, L.LPQURL as LpqUrl1, L.ConfigData, L.note as Note, L.LastModifiedDate, L.CreatedDate, L.LastModifiedByUserID, L.LastOpenedByUserID, U.FirstName + ' ' + U.LastName as LastModifiedByUserName, U2.FirstName + ' ' + U2.LastName as LastOpenedByUserName, L.IterationID, L.LastOpenedDate, L.RevisionID, UR.Status, UR.UserRoleID, UR.UserID, LV.VendorID, LV.Status as VendorStatus FROM {0} B RIGHT JOIN {1} L ON B.LenderID = L.LenderID LEFT JOIN {2} UR ON L.LenderID = UR.LenderID LEFT JOIN {3} LV on UR.VendorID = LV.VendorID LEFT JOIN {4} U on L.LastModifiedByUserID = U.UserID LEFT JOIN {4} U2 on L.LastOpenedByUserID = U2.UserID ", BACKOFFICELENDERS_TABLE, LENDERCONFIG_TABLE, USERROLES_TABLE, LENDERVENDORS_TABLE, USERS_TABLE)
        Dim oWhere As New CSQLWhereStringBuilder()
        If filter IsNot Nothing Then
            If Not String.IsNullOrEmpty(filter.Keyword) Then
                oWhere.AppendAndCondition("(COALESCE(B.LenderName, '') + '$' + COALESCE(B.City, '') + '$' + COALESCE(B.State, '') + '$' + COALESCE(L.LenderRef, '') ) LIKE {0}", New CSQLParamValue("%" & filter.Keyword & "%"))
            End If
        End If
        If userID <> Nothing Then
            oWhere.AppendAndCondition("UR.Status={0}", New CSQLParamValue(SmSettings.UserRoleStatus.Active))
            oWhere.AppendAndCondition("UR.UserID={0}", New CSQLParamValue(userID))
            oWhere.AppendANDCondition("L.LenderConfigID=COALESCE(UR.LenderConfigID, L.LenderConfigID)")
        End If


        'Dim userRoleList As List(Of SmUserRole)
        'If userID <> Nothing Then
        '	userRoleList = GetActiveUserRolesByUserID(userID)
        'End If

        Dim oOrder As String = " "
        If Not String.IsNullOrEmpty(paging.SortColumn) Then
            oOrder = oOrder & "ORDER BY " & paging.SortColumn & " " & paging.SortDirection
        Else
            oOrder = oOrder & "ORDER BY L.LastModifiedDate DESC"
        End If
        Dim oDB As New CSQLDBUtils(LPQMOBILE_CONNECTIONSTRING)
        Try
            oData = oDB.getDataTable(sSQL & oWhere.SQL & oOrder)
            If oData IsNot Nothing AndAlso oData.Rows.Count > 0 Then
                For Each row As DataRow In oData.Rows
                    'If userRoleList IsNot Nothing Then
                    '	'when a vendor has been deactived, VendorUser and VendorAdmin who are assigned to that vendor will no longer see that lender/portal on lender-port list. It is to make sure that VendorUser/VendorAdmin only see those lender/portal that they have privilege on
                    '	If Not String.IsNullOrEmpty(Common.SafeStripHtmlString(row("VendorID"))) AndAlso DirectCast(Common.SafeInteger(row("VendorStatus")), SmSettings.LenderVendorStatus) = SmSettings.LenderVendorStatus.InActive AndAlso userRoleList.Any(Function(r) (r.Role = SmSettings.Role.VendorAdmin OrElse r.Role = SmSettings.Role.VendorUser) AndAlso r.LenderID = Common.SafeGUID(row("LenderID")) AndAlso r.LenderConfigID = Common.SafeGUID(row("LenderConfigID"))) Then
                    '		Continue For
                    '	End If
                    'End If

                    'LenderID in BackOfficeLenders is not quarantee to be unique so need to match by host name too
                    Dim match1 As Match = Regex.Match(Common.SafeString(row("LpqUrl1")), SmSettings.extractDomainRegexPattern, RegexOptions.IgnoreCase)
                    Dim match2 As Match = Regex.Match(Common.SafeString(row("LpqUrl")), SmSettings.extractDomainRegexPattern, RegexOptions.IgnoreCase)
                    If (match1.Success AndAlso match2.Success AndAlso match1.Value = match2.Value) Or row.IsNull("LenderName") Or row.IsNull("LpqUrl") Then
                        Dim item As New SmBackOfficeLender()
                        item.BackOfficeLenderID = Common.SafeGUID(row("BackOfficeLenderID"))
                        item.LenderName = Common.SafeString(row("LenderName"))
                        item.CustomerInternalID = Common.SafeString(row("CustomerInternalID"))
                        item.LenderID = Common.SafeGUID(row("LenderID"))
                        item.LpqUrl = Common.SafeString(row("LpqUrl"))
                        item.City = Common.SafeString(row("City"))
                        item.State = Common.SafeString(row("State"))
                        If Not row.IsNull("LastModifiedDate") Then
                            item.LastModifiedDate = Common.SafeDate(row("LastModifiedDate"))
                        End If
                        item.LastModifiedByUserID = Common.SafeGUID(row("LastModifiedByUserID"))
                        item.LastModifiedByUserName = Common.SafeString(row("LastModifiedByUserName"))
                        'item.Host = Common.SafeString(row("Host"))
                        'If String.IsNullOrEmpty(item.Host) Then
                        '	item.Host = Common.SafeString(match2.Value)
                        'End If
                        item.Host = Common.SafeString(match2.Value)
                        If Not result.ContainsKey(item.BackOfficeLenderID) Then
                            result.Add(item.BackOfficeLenderID, item)
                        End If
                        If result(item.BackOfficeLenderID) IsNot Nothing Then
                            Dim portalList = result(item.BackOfficeLenderID).PortalList
                            If portalList Is Nothing Then
                                portalList = New List(Of SmLenderConfig)
                            End If
                            If Not portalList.Any(Function(p) p.LenderConfigID = Common.SafeGUID(row("LenderConfigID"))) Then
                                Dim portal As New SmLenderConfig
                                portal.LenderConfigID = Common.SafeGUID(row("LenderConfigID"))
                                portal.OrganizationID = Common.SafeGUID(row("OrganizationID"))
                                portal.LenderID = Common.SafeGUID(row("LenderID"))
                                portal.LenderRef = Common.SafeString(row("LenderRef"))
                                portal.ReturnURL = Common.SafeString(row("ReturnURL"))
                                portal.LPQLogin = Common.SafeString(row("LPQLogin"))
                                'PW is not needed thus comment out to increase performance by a facto of 10
                                'portal.LPQPW = Common.DecryptPassword(Common.SafeString(row("LPQPW")))
                                'portal.ConfigData = Common.DecryptAllPasswordInConfigStrXml(Common.SafeString(row("ConfigData")))
                                portal.LPQURL = Common.SafeString(row("LPQURL"))
                                portal.Note = Common.SafeString(row("Note"))
                                If Not row.IsNull("LastModifiedDate") Then
                                    portal.LastModifiedDate = Common.SafeDate(row("LastModifiedDate"))
                                End If
                                portal.CreatedDate = Common.SafeDate(row("CreatedDate"))
                                If Not row.IsNull("LastOpenedDate") Then
                                    portal.LastOpenedDate = Common.SafeDate(row("LastOpenedDate"))
                                End If
                                portal.LastModifiedByUserID = Common.SafeGUID(row("LastModifiedByUserID"))
                                portal.LastModifiedByUserName = Common.SafeString(row("LastModifiedByUserName"))
                                portal.LastOpenedByUserID = Common.SafeGUID(row("LastOpenedByUserID"))
                                portal.LastOpenedByUserName = Common.SafeString(row("LastOpenedByUserName"))
                                portal.IterationID = Common.SafeInteger(row("IterationID"))
                                portal.RevisionID = Common.SafeInteger(row("RevisionID"))
                                portalList.Add(portal)
                            End If
                            result(item.BackOfficeLenderID).PortalList = portalList
                        End If
                    End If
                Next
            End If
            paging.TotalRecord = result.Count
            If paging.PageIndex > 0 AndAlso paging.PageSize > 0 Then
                Dim originalResult = result
                result = originalResult.Skip((paging.PageIndex - 1) * paging.PageSize).Take(paging.PageSize).ToDictionary(Function(x) x.Key, Function(x) x.Value)
                If Not result.Any() Then
                    paging.PageIndex = paging.PageIndex - 1
                    result = originalResult.Skip((paging.PageIndex - 1) * paging.PageSize).Take(paging.PageSize).ToDictionary(Function(x) x.Key, Function(x) x.Value)
                End If
            End If
        Catch ex As Exception
            _log.Error("Error while GetPortals", ex)
        Finally
            oDB.closeConnection()
        End Try
        Return result.Values.ToList()
    End Function

    ''' <summary>
    ''' get list of all portals and group them into lender group, order by cache created date
    ''' </summary>
    ''' <returns></returns>
    Public Function GetPortalForCache() As List(Of SmBackOfficeLender)
        Dim result As New Dictionary(Of Guid, SmBackOfficeLender)
        Dim oData As New DataTable()
        'B.Host may be null so use URL1
        Dim sSQL As String = String.Format("SELECT B.*, L.LenderConfigID, L.OrganizationID, L.LenderID, L.LenderRef, L.ReturnURL, L.LPQLogin, L.LPQPW, L.LPQURL as LpqUrl1, L.ConfigData, L.note as Note, C.CreatedDate FROM {0} B RIGHT JOIN {1} L ON B.LenderID = L.LenderID LEFT JOIN {2} C on C.CachedKey like '%' +  LOWER('_' + REPLACE( CONVERT(varchar(50), L.LenderID), '-', '') +  '_' + LEFT(L.LPQURL,20) + '%')", BACKOFFICELENDERS_TABLE, LENDERCONFIG_TABLE, CACHED_CONFIGS)
        Dim oDB As New CSQLDBUtils(LPQMOBILE_CONNECTIONSTRING)
        Try
            Dim oWhere As New CSQLWhereStringBuilder()
            oWhere.AppendANDCondition("B.LenderID IS NOT NULL")
            oData = oDB.getDataTable(sSQL & oWhere.SQL & " ORDER BY C.CreatedDate")
            If oData IsNot Nothing AndAlso oData.Rows.Count > 0 Then
                For Each row As DataRow In oData.Rows
                    'LenderID in BackOfficeLenders is not quarantee to be unique so need to match by host name too
                    Dim match1 As Match = Regex.Match(Common.SafeString(row("LpqUrl1")), SmSettings.extractDomainRegexPattern, RegexOptions.IgnoreCase)
                    Dim match2 As Match = Regex.Match(Common.SafeString(row("LpqUrl")), SmSettings.extractDomainRegexPattern, RegexOptions.IgnoreCase)
                    If (match1.Success AndAlso match2.Success AndAlso match1.Value = match2.Value) Or row.IsNull("LenderName") Or row.IsNull("LpqUrl") Then
                        Dim item As New SmBackOfficeLender()
                        item.BackOfficeLenderID = Common.SafeGUID(row("BackOfficeLenderID"))
                        item.LenderName = Common.SafeString(row("LenderName"))
                        item.CustomerInternalID = Common.SafeString(row("CustomerInternalID"))
                        item.LenderID = Common.SafeGUID(row("LenderID"))
                        item.LpqUrl = Common.SafeString(row("LpqUrl"))
                        item.Host = Common.SafeString(match2.Value)
                        If Not result.ContainsKey(item.BackOfficeLenderID) Then 'group portals from same lenderID so system only need to do 1 configuration download per lenderID
                            result.Add(item.BackOfficeLenderID, item)
                        End If
                        If result(item.BackOfficeLenderID) IsNot Nothing Then
                            Dim portalList = result(item.BackOfficeLenderID).PortalList
                            If portalList Is Nothing Then
                                portalList = New List(Of SmLenderConfig)
                            End If
                            If Not portalList.Any(Function(p) p.LenderConfigID = Common.SafeGUID(row("LenderConfigID"))) Then
                                Dim portal As New SmLenderConfig
                                portal.LenderConfigID = Common.SafeGUID(row("LenderConfigID"))
                                portal.OrganizationID = Common.SafeGUID(row("OrganizationID"))
                                portal.LenderID = Common.SafeGUID(row("LenderID"))
                                portal.LenderRef = Common.SafeString(row("LenderRef"))
                                portal.ReturnURL = Common.SafeString(row("ReturnURL"))
                                portal.LPQLogin = Common.SafeString(row("LPQLogin"))
                                portal.LPQURL = Common.SafeString(row("LPQURL"))
                                portal.Note = Common.SafeString(row("Note"))
                                'these are not needed so take out for performance
                                'portal.LPQPW = Common.DecryptPassword(Common.SafeString(row("LPQPW")))
                                'portal.ConfigData = Common.DecryptAllPasswordInConfigStrXml(Common.SafeString(row("ConfigData")))
                                'ConfigData is need by keepalive to identify which module is enabled
                                portal.ConfigData = Common.SafeString(row("ConfigData"))
                                portalList.Add(portal)
                            End If
                            result(item.BackOfficeLenderID).PortalList = portalList
                        End If
                    End If
                Next
            End If

        Catch ex As Exception
            _log.Error("Error while GetPortalForCache", ex)
        Finally
            oDB.closeConnection()
        End Try
        Return result.Values.ToList()
    End Function

    'Public Function GetPortals(ByRef paging As SmPagingInfoModel, filter As SmBackOfficeLenderListFilter, roles As String()) As List(Of SmBackOfficeLender)
    '	Dim result As New Dictionary(Of Guid, SmBackOfficeLender)
    '	Dim oData As New DataTable()
    '	Dim sSQL As String = String.Format("SELECT B.*, L.LenderConfigID, L.OrganizationID, L.LenderID, L.LenderRef, L.ReturnURL, L.LPQLogin, L.LPQPW, L.LPQURL as LpqUrl1, L.ConfigData, L.note as Note, L.LastModifiedDate, L.CreatedDate, L.LastModifiedByUserID, L.LastOpenedByUserID, U.FirstName + ' ' + U.LastName as LastModifiedByUserName, U2.FirstName + ' ' + U2.LastName as LastOpenedByUserName, L.IterationID, L.LastOpenedDate, L.IterationID, L.LastOpenedDate, L.RevisionID, UR.Status, UR.UserRoleID, UR.UserID FROM {0} B RIGHT JOIN {1} L ON B.LenderID = L.LenderID LEFT JOIN {2} UR ON L.LenderID = UR.LenderID LEFT JOIN {3} U on L.LastModifiedByUserID = U.UserID LEFT JOIN {3} U2 on L.LastOpenedByUserID = U2.UserID", BACKOFFICELENDERS_TABLE, LENDERCONFIG_TABLE, USERROLES_TABLE, USERS_TABLE)
    '	Dim oWhere As New CSQLWhereStringBuilder()
    '	If filter IsNot Nothing Then
    '		If Not String.IsNullOrEmpty(filter.Keyword) Then
    '			oWhere.AppendAndCondition("(COALESCE(B.LenderName, '') + '$' + COALESCE(B.City, '') + '$' + COALESCE(B.State, '') + '$' + COALESCE(L.LenderRef, '')) LIKE {0}", New CSQLParamValue("%" & filter.Keyword & "%"))
    '		End If
    '	End If
    '	Dim oOrder As String = " "
    '	If Not String.IsNullOrEmpty(paging.SortColumn) Then
    '		oOrder = oOrder & "ORDER BY " & paging.SortColumn & " " & paging.SortDirection
    '	Else
    '		oOrder = oOrder & "ORDER BY L.LastModifiedDate DESC"
    '	End If
    '	Dim oDB As New CSQLDBUtils(LPQMOBILE_CONNECTIONSTRING)
    '	Try
    '		oData = oDB.getDataTable(sSQL & oWhere.SQL & oOrder)
    '		If oData IsNot Nothing AndAlso oData.Rows.Count > 0 Then
    '			For Each row As DataRow In oData.Rows
    '				'LenderID in BackOfficeLenders is not quarantee to be unique so need to match by host name too
    '				Dim match1 As Match = Regex.Match(Common.SafeString(row("LpqUrl1")), SmSettings.extractDomainRegexPattern, RegexOptions.IgnoreCase)
    '				Dim match2 As Match = Regex.Match(Common.SafeString(row("LpqUrl")), SmSettings.extractDomainRegexPattern, RegexOptions.IgnoreCase)
    '				If (match1.Success AndAlso match2.Success AndAlso match1.Value = match2.Value) Or row.IsNull("LenderName") Or row.IsNull("LpqUrl") Then
    '					Dim item As New SmBackOfficeLender()
    '					item.BackOfficeLenderID = Common.SafeGUID(row("BackOfficeLenderID"))
    '					item.LenderName = Common.SafeString(row("LenderName"))
    '					item.LenderID = Common.SafeGUID(row("LenderID"))
    '					item.LpqUrl = Common.SafeString(row("LpqUrl"))
    '					item.City = Common.SafeString(row("City"))
    '					item.State = Common.SafeString(row("State"))
    '					If Not row.IsNull("LastModifiedDate") Then
    '						item.LastModifiedDate = Common.SafeDate(row("LastModifiedDate"))
    '					End If
    '					item.LastModifiedByUserID = Common.SafeGUID(row("LastModifiedByUserID"))
    '					item.Host = Common.SafeString(match2.Value)
    '					If Not result.ContainsKey(item.BackOfficeLenderID) Then
    '						result.Add(item.BackOfficeLenderID, item)
    '					End If
    '					If result(item.BackOfficeLenderID) IsNot Nothing Then
    '						Dim portalList = result(item.BackOfficeLenderID).PortalList
    '						If portalList Is Nothing Then
    '							portalList = New List(Of SmLenderConfig)
    '						End If
    '						If Not portalList.Any(Function(p) p.LenderConfigID = Common.SafeGUID(row("LenderConfigID"))) Then
    '							Dim portal As New SmLenderConfig
    '							portal.LenderConfigID = Common.SafeGUID(row("LenderConfigID"))
    '							portal.OrganizationID = Common.SafeGUID(row("OrganizationID"))
    '							portal.LenderID = Common.SafeGUID(row("LenderID"))
    '							portal.LenderRef = Common.SafeString(row("LenderRef"))
    '							portal.ReturnURL = Common.SafeString(row("ReturnURL"))
    '							portal.LPQLogin = Common.SafeString(row("LPQLogin"))
    '							portal.LPQPW = Common.SafeString(row("LPQPW"))
    '							portal.LPQURL = Common.SafeString(row("LPQURL"))
    '							portal.Note = Common.SafeString(row("Note"))
    '							portal.ConfigData = Common.SafeString(row("ConfigData"))
    '							If Not row.IsNull("LastModifiedDate") Then
    '								portal.LastModifiedDate = Common.SafeDate(row("LastModifiedDate"))
    '							End If
    '							portal.CreatedDate = Common.SafeDate(row("CreatedDate"))
    '							If Not row.IsNull("LastOpenedDate") Then
    '								portal.LastModifiedDate = Common.SafeDate(row("LastOpenedDate"))
    '							End If
    '							portal.LastModifiedByUserID = Common.SafeGUID(row("LastModifiedByUserID"))
    '							portal.LastModifiedByUserName = Common.SafeString(row("LastModifiedByUserName"))
    '							portal.LastOpenedByUserID = Common.SafeGUID(row("LastOpenedByUserID"))
    '							portal.LastOpenedByUserName = Common.SafeString(row("LastOpenedByUserName"))
    '							portal.IterationID = Common.SafeInteger(row("IterationID"))
    '							portal.RevisionID = Common.SafeInteger(row("RevisionID"))
    '							portalList.Add(portal)
    '						End If
    '						result(item.BackOfficeLenderID).PortalList = portalList
    '					End If
    '				End If
    '			Next
    '		End If
    '		paging.TotalRecord = result.Count
    '		If paging.PageIndex > 0 AndAlso paging.PageSize > 0 Then
    '			Dim originalResult = result
    '			result = originalResult.Skip((paging.PageIndex - 1) * paging.PageSize).Take(paging.PageSize).ToDictionary(Function(x) x.Key, Function(x) x.Value)
    '			If Not result.Any() Then
    '				paging.PageIndex = paging.PageIndex - 1
    '				result = originalResult.Skip((paging.PageIndex - 1) * paging.PageSize).Take(paging.PageSize).ToDictionary(Function(x) x.Key, Function(x) x.Value)
    '			End If
    '		End If
    '	Catch ex As Exception
    '		_log.Error("Error while GetLenders", ex)
    '	Finally
    '		oDB.closeConnection()
    '	End Try
    '	Return result.Values.ToList()
    'End Function

    Public Function GetBackOfficeLenderByLenderConfigID(lenderConfigID As Guid) As SmBackOfficeLender
        Dim result As SmBackOfficeLender
        Dim oData As New DataTable()
        Dim sSQL As String = String.Format("SELECT B.*, L.LastModifiedDate, L.LastModifiedByUserID, L.LenderConfigID, L.LPQURL as LpqUrl1, L.LenderRef,  L.LenderID as LenderID1 FROM {0} B INNER JOIN {1} L ON B.LenderID = L.LenderID", BACKOFFICELENDERS_TABLE, LENDERCONFIG_TABLE)
        Dim oWhere As New CSQLWhereStringBuilder()
        oWhere.AppendAndCondition("L.LenderConfigID={0}", New CSQLParamValue(lenderConfigID))
        Dim oDB As New CSQLDBUtils(LPQMOBILE_CONNECTIONSTRING)
        Try
            oData = oDB.getDataTable(sSQL & oWhere.SQL)
            If oData IsNot Nothing AndAlso oData.Rows.Count > 0 Then
                For Each row As DataRow In oData.Rows
                    Dim match1 As Match = Regex.Match(Common.SafeString(row("LpqUrl1")), SmSettings.extractDomainRegexPattern, RegexOptions.IgnoreCase)
                    Dim match2 As Match = Regex.Match(Common.SafeString(row("LpqUrl")), SmSettings.extractDomainRegexPattern, RegexOptions.IgnoreCase)
                    If match1.Success AndAlso match2.Success AndAlso match1.Value = match2.Value Or row.IsNull("BackOfficeLenderID") Then
                        result = New SmBackOfficeLender()
                        result.BackOfficeLenderID = Common.SafeGUID(row("BackOfficeLenderID"))
                        result.LenderName = Common.SafeString(row("LenderName"))
                        result.CustomerInternalID = Common.SafeString(row("CustomerInternalID"))
                        result.LenderID = Common.SafeGUID(row("LenderID1"))
                        result.LpqUrl = Common.SafeString(row("LpqUrl1"))
                        result.City = Common.SafeString(row("City"))
                        result.State = Common.SafeString(row("State"))
                        If Not row.IsNull("LastModifiedDate") Then
                            result.LastModifiedDate = Common.SafeDate(row("LastModifiedDate"))
                        End If
                        result.LastModifiedByUserID = Common.SafeGUID(row("LastModifiedByUserID"))
                        result.Host = Common.SafeString(match2.Value)
                        Exit For
                    End If
                Next
            End If
        Catch ex As Exception
            _log.Error("Error while GetBackOfficeLenderByLenderConfigID", ex)
        Finally
            oDB.closeConnection()
        End Try
        If result Is Nothing Then
            _log.Warn("There is no available data for lender config Id: ")
        End If
        Return result
    End Function
    Public Function CheckCustomerInternalIDInUse(customerInternalID As String, backOfficeLenderID As Guid) As Boolean
        Dim result As Boolean = False
        Dim sSQL As String = String.Format("SELECT COUNT(*) FROM {0}", BACKOFFICELENDERS_TABLE)
        Dim oWhere As New CSQLWhereStringBuilder()
        oWhere.AppendANDCondition("CustomerInternalID IS NOT NULL")
        oWhere.AppendAndCondition("CustomerInternalID = {0}", New CSQLParamValue(customerInternalID))
        If backOfficeLenderID <> Guid.Empty Then
            oWhere.AppendAndCondition("BackOfficeLenderID <> {0}", New CSQLParamValue(backOfficeLenderID))
        End If
        Dim oDB As New CSQLDBUtils(LPQMOBILE_CONNECTIONSTRING)
        Try
            result = Common.SafeInteger(oDB.getScalerValue(sSQL & oWhere.SQL)) > 0
        Catch ex As Exception
            _log.Error("Error while CheckCustomerInternalIDInUse", ex)
        Finally
            oDB.closeConnection()
        End Try
        Return result
    End Function

    Public Function GetAllBackOfficeLenders() As List(Of SmBackOfficeLender)
        Dim result As New List(Of SmBackOfficeLender)
        Dim oData As New DataTable()
        Dim oDB As New CSQLDBUtils(LPQMOBILE_CONNECTIONSTRING)
        Try
            oData = oDB.getDataTable(String.Format("SELECT * FROM {0}", BACKOFFICELENDERS_TABLE))
            If oData IsNot Nothing AndAlso oData.Rows.Count > 0 Then
                For Each row As DataRow In oData.Rows
                    Dim item As New SmBackOfficeLender()
                    item.BackOfficeLenderID = Common.SafeGUID(row("BackOfficeLenderID"))
                    item.LenderName = Common.SafeString(row("LenderName"))
                    item.CustomerInternalID = Common.SafeString(row("CustomerInternalID"))
                    item.LenderID = Common.SafeGUID(row("LenderID"))
                    item.LpqUrl = Common.SafeString(row("LpqUrl"))
                    item.City = Common.SafeString(row("City"))
                    item.State = Common.SafeString(row("State"))
                    If Not row.IsNull("LastModifiedDate") Then
                        item.LastModifiedDate = Common.SafeDate(row("LastModifiedDate"))
                    End If
                    item.LastModifiedByUserID = Common.SafeGUID(row("LastModifiedByUserID"))
                    item.Host = Common.SafeString(row("Host"))
                    result.Add(item)
                Next
            End If
        Catch ex As Exception
            _log.Error("Error while GetAllBackOfficeLenders", ex)
        Finally
            oDB.closeConnection()
        End Try
        Return result
    End Function

    Public Function GetBackOfficeLenderByID(backOfficeLenderID As Guid) As SmBackOfficeLender
        Dim result As SmBackOfficeLender
        Dim oData As New DataTable()
        Dim sSQL As String = String.Format("SELECT B.*, L.LastModifiedDate, L.LastModifiedByUserID, L.LenderConfigID, L.LPQURL as LpqUrl1, L.LenderRef FROM {0} B INNER JOIN {1} L ON B.LenderID = L.LenderID", BACKOFFICELENDERS_TABLE, LENDERCONFIG_TABLE)
        Dim oWhere As New CSQLWhereStringBuilder()
        oWhere.AppendAndCondition("B.BackOfficeLenderID={0}", New CSQLParamValue(backOfficeLenderID))
        Dim oDB As New CSQLDBUtils(LPQMOBILE_CONNECTIONSTRING)
        Try
            oData = oDB.getDataTable(sSQL & oWhere.SQL)
            If oData IsNot Nothing AndAlso oData.Rows.Count > 0 Then
                For Each row As DataRow In oData.Rows
                    Dim match1 As Match = Regex.Match(Common.SafeString(row("LpqUrl1")), SmSettings.extractDomainRegexPattern, RegexOptions.IgnoreCase)
                    Dim match2 As Match = Regex.Match(Common.SafeString(row("LpqUrl")), SmSettings.extractDomainRegexPattern, RegexOptions.IgnoreCase)
                    If match1.Success AndAlso match2.Success AndAlso match1.Value = match2.Value Then
                        result = New SmBackOfficeLender()
                        result.BackOfficeLenderID = Common.SafeGUID(row("BackOfficeLenderID"))
                        result.LenderName = Common.SafeString(row("LenderName"))
                        result.CustomerInternalID = Common.SafeString(row("CustomerInternalID"))
                        result.LenderID = Common.SafeGUID(row("LenderID"))
                        result.LpqUrl = Common.SafeString(row("LpqUrl"))
                        result.City = Common.SafeString(row("City"))
                        result.State = Common.SafeString(row("State"))
                        If Not row.IsNull("LastModifiedDate") Then
                            result.LastModifiedDate = Common.SafeDate(row("LastModifiedDate"))
                        End If
                        result.LastModifiedByUserID = Common.SafeGUID(row("LastModifiedByUserID"))
                        result.Host = Common.SafeString(match2.Value)
                        Exit For
                    End If
                Next
            End If
        Catch ex As Exception
            _log.Error("Error while GetBackOfficeLenderByID", ex)
        Finally
            oDB.closeConnection()
        End Try
        Return result
    End Function

    Public Function GetBackOfficeLenderByLenderIDAndHost(lenderID As Guid, host As String) As List(Of SmBackOfficeLender)
        Dim result As New List(Of SmBackOfficeLender)
        Dim oData As New DataTable()
        Dim sSQL As String = String.Format("SELECT B.*, L.LenderConfigID, L.LPQURL as LpqUrl1, L.LenderRef, L.LastModifiedDate, L.LastModifiedByUserID FROM {0} B INNER JOIN {1} L ON B.LenderID = L.LenderID", BACKOFFICELENDERS_TABLE, LENDERCONFIG_TABLE)
        Dim oWhere As New CSQLWhereStringBuilder()
        oWhere.AppendAndCondition("B.LenderID={0}", New CSQLParamValue(lenderID))
        oWhere.AppendOptionalAndCondition("B.LPQURL", New CSQLParamValue("%" & host & "%"), "like")
        Dim oDB As New CSQLDBUtils(LPQMOBILE_CONNECTIONSTRING)
        Try
            oData = oDB.getDataTable(sSQL & oWhere.SQL)
            If oData IsNot Nothing AndAlso oData.Rows.Count > 0 Then
                For Each row As DataRow In oData.Rows
                    Dim match1 As Match = Regex.Match(Common.SafeString(row("LpqUrl1")), SmSettings.extractDomainRegexPattern, RegexOptions.IgnoreCase)
                    Dim match2 As Match = Regex.Match(Common.SafeString(row("LpqUrl")), SmSettings.extractDomainRegexPattern, RegexOptions.IgnoreCase)
                    If match1.Success AndAlso match2.Success AndAlso match1.Value = match2.Value Then
                        Dim item = New SmBackOfficeLender()
                        item.BackOfficeLenderID = Common.SafeGUID(row("BackOfficeLenderID"))
                        item.LenderName = Common.SafeString(row("LenderName"))
                        item.CustomerInternalID = Common.SafeString(row("CustomerInternalID"))
                        item.LenderID = Common.SafeGUID(row("LenderID"))
                        item.LpqUrl = Common.SafeString(row("LpqUrl"))
                        item.City = Common.SafeString(row("City"))
                        item.State = Common.SafeString(row("State"))
                        If Not row.IsNull("LastModifiedDate") Then
                            item.LastModifiedDate = Common.SafeDate(row("LastModifiedDate"))
                        End If
                        item.LastModifiedByUserID = Common.SafeGUID(row("LastModifiedByUserID"))
                        item.Host = Common.SafeString(match2.Value)
                        result.Add(item)
                    End If
                Next
            End If
        Catch ex As Exception
            _log.Error("Error while GetBackOfficeLenderByLenderIDAndHost", ex)
        Finally
            oDB.closeConnection()
        End Try
        Return result
    End Function

    Public Function GetBackOfficeLenderByOrgIDAndHost(orgID As Guid, host As String) As List(Of SmBackOfficeLender)
        Dim result As New List(Of SmBackOfficeLender)
        Dim oData As New DataTable()
        Dim sSQL As String = String.Format("SELECT B.*, L.LastModifiedDate, L.LastModifiedByUserID, L.LenderConfigID, L.LPQURL as LpqUrl1, L.LenderRef FROM {0} B INNER JOIN {1} L ON B.LenderID = L.LenderID", BACKOFFICELENDERS_TABLE, LENDERCONFIG_TABLE)
        Dim oWhere As New CSQLWhereStringBuilder()
        oWhere.AppendAndCondition("L.OrganizationID={0}", New CSQLParamValue(orgID))
        oWhere.AppendOptionalAndCondition("B.LPQURL", New CSQLParamValue("%" & host & "%"), "like")
        Dim oDB As New CSQLDBUtils(LPQMOBILE_CONNECTIONSTRING)
        Try
            oData = oDB.getDataTable(sSQL & oWhere.SQL)
            If oData IsNot Nothing AndAlso oData.Rows.Count > 0 Then
                For Each row As DataRow In oData.Rows
                    Dim match1 As Match = Regex.Match(Common.SafeString(row("LpqUrl1")), SmSettings.extractDomainRegexPattern, RegexOptions.IgnoreCase)
                    Dim match2 As Match = Regex.Match(Common.SafeString(row("LpqUrl")), SmSettings.extractDomainRegexPattern, RegexOptions.IgnoreCase)
                    If match1.Success AndAlso match2.Success AndAlso match1.Value = match2.Value Then
                        Dim item = New SmBackOfficeLender()
                        item.BackOfficeLenderID = Common.SafeGUID(row("BackOfficeLenderID"))
                        item.LenderName = Common.SafeString(row("LenderName"))
                        item.CustomerInternalID = Common.SafeString(row("CustomerInternalID"))
                        item.LenderID = Common.SafeGUID(row("LenderID"))
                        item.LpqUrl = Common.SafeString(row("LpqUrl"))
                        item.City = Common.SafeString(row("City"))
						item.State = Common.SafeString(row("State"))
						If Not row.IsNull("LastModifiedDate") Then
							item.LastModifiedDate = Common.SafeDate(row("LastModifiedDate"))
						End If
						item.LastModifiedByUserID = Common.SafeGUID(row("LastModifiedByUserID"))
						item.Host = Common.SafeString(match2.Value)
						result.Add(item)
					End If
				Next
			End If
		Catch ex As Exception
			_log.Error("Error while GetBackOfficeLenderByOrgIDAndHost", ex)
		Finally
			oDB.closeConnection()
		End Try
		Return result
	End Function

	Public Function GetLenderUserRoles(ByRef paging As SmPagingInfoModel, filter As SmLenderUserRoleListFilter) As List(Of SmUserRole)
		Dim result As New List(Of SmUserRole)
		Dim oData As New DataTable()
		Dim sSQL As String = String.Format("SELECT UR.UserRoleID, UR.Role, UR.LenderConfigID, UR.LenderID, UR.Status, Ur.CreatedDate, U.UserID, U.Email, U.ExpireDays, U.Status as 'UserStatus', U.LoginFailedCount, U.LastLoginDate, CASE WHEN U.LastLoginDate IS NOT NULL AND U.LastLoginDate > T.MinCreatedDate then U.FirstName else '' end as 'FirstName', CASE WHEN U.LastLoginDate IS NOT NULL AND U.LastLoginDate > T.MinCreatedDate then U.LastName else '' end as 'LastName', CASE WHEN U.LastLoginDate IS NOT NULL AND U.LastLoginDate > T.MinCreatedDate then U.Phone else '' end as 'Phone', CASE WHEN U.LastLoginDate IS NOT NULL AND U.LastLoginDate > T.MinCreatedDate then U.Avatar else '' end as 'Avatar' FROM {0} UR INNER JOIN {1} U ON UR.UserID = U.UserID INNER JOIN (select min(CreatedDate) as MinCreatedDate, LenderID, UserID from {0} Group By LenderID, UserID) T on T.LenderID = UR.LenderID and T.UserID = UR.UserID ", USERROLES_TABLE, USERS_TABLE)
		Dim oWhere As New CSQLWhereStringBuilder()
		Dim oOrder As String = " "
		If Not String.IsNullOrEmpty(paging.SortColumn) Then
			oOrder = oOrder & "ORDER BY " & paging.SortColumn & " " & paging.SortDirection
		Else
			oOrder = oOrder & "ORDER BY UR.CreatedDate "
		End If
		If filter IsNot Nothing Then
			If Not String.IsNullOrEmpty(filter.Keyword) Then
				oWhere.AppendAndCondition("COALESCE(U.Email, '') LIKE {0}", New CSQLParamValue("%" & filter.Keyword & "%"))
			End If
			oWhere.AppendAndCondition("UR.LenderID={0}", New CSQLParamValue(filter.LenderID))
			oWhere.AppendAndCondition("UR.LenderConfigID IS NULL")
		End If
		oWhere.AppendAndCondition("U.Status <> {0}", New CSQLParamValue(SmSettings.UserStatus.Deleted))
		Dim oDB As New CSQLDBUtils(LPQMOBILE_CONNECTIONSTRING)
		Try
			oData = oDB.getDataTable(sSQL & oWhere.SQL & oOrder)
			_log.Debug(sSQL & oWhere.SQL & oOrder)
			If oData IsNot Nothing AndAlso oData.Rows.Count > 0 Then
				For Each row As DataRow In oData.Rows
					Dim item As New SmUserRole()
					item.UserRoleID = Common.SafeGUID(row("UserRoleID"))
					item.Role = CType([Enum].Parse(GetType(SmSettings.Role), Common.SafeString(row("Role"))), SmSettings.Role)
					item.LenderConfigID = Common.SafeGUID(row("LenderConfigID"))
					item.LenderID = Common.SafeGUID(row("LenderID"))
					item.Status = DirectCast(Common.SafeInteger(row("Status")), SmSettings.UserRoleStatus)
					item.UserID = Common.SafeGUID(row("UserID"))
					item.CreatedDate = Common.SafeDate(row("CreatedDate"))
					item.User = New SmUser() With {
						.Email = Common.SafeString(row("Email")),
						.LastName = Common.SafeString(row("LastName")),
						.Phone = Common.SafeString(row("Phone")),
						.ExpireDays = Common.SafeInteger(row("ExpireDays")),
						.FirstName = Common.SafeString(row("FirstName")),
						.Avatar = Common.SafeString(row("Avatar")),
						.Status = DirectCast(Common.SafeInteger(row("UserStatus")), SmSettings.UserStatus),
						.LoginFailedCount = Common.SafeInteger(row("LoginFailedCount"))
					}
					If Not row.IsNull("LastLoginDate") Then
						item.User.LastLoginDate = Common.SafeDate(row("LastLoginDate"))
					End If
					result.Add(item)
				Next
			End If
			paging.TotalRecord = result.Count
			If paging.PageIndex > 0 AndAlso paging.PageSize > 0 Then
				Dim originalResult = result
				result = originalResult.Skip((paging.PageIndex - 1) * paging.PageSize).Take(paging.PageSize).ToList()
				If Not result.Any() Then
					paging.PageIndex = paging.PageIndex - 1
					result = originalResult.Skip((paging.PageIndex - 1) * paging.PageSize).Take(paging.PageSize).ToList()
				End If
			End If
		Catch ex As Exception
			_log.Error("Error while GetLenderUserRoles", ex)
		Finally
			oDB.closeConnection()
		End Try
		Return result
	End Function

	Public Function GetPortalUserRoles(ByRef paging As SmPagingInfoModel, filter As SmPortalUserRoleListFilter) As List(Of SmUserRole)
		Dim result As New List(Of SmUserRole)
		Dim oData As New DataTable()
		Dim sSQL As String = String.Format("SELECT UR.UserRoleID, UR.Role, UR.LenderConfigID, UR.LenderID, UR.Status, Ur.CreatedDate, U.UserID, U.Email, U.ExpireDays, U.Status as 'UserStatus', U.LoginFailedCount, U.LastLoginDate, CASE WHEN U.LastLoginDate IS NOT NULL AND U.LastLoginDate > T.MinCreatedDate then U.FirstName else '' end as 'FirstName', CASE WHEN U.LastLoginDate IS NOT NULL AND U.LastLoginDate > T.MinCreatedDate then U.LastName else '' end as 'LastName', CASE WHEN U.LastLoginDate IS NOT NULL AND U.LastLoginDate > T.MinCreatedDate then U.Phone else '' end as 'Phone', CASE WHEN U.LastLoginDate IS NOT NULL AND U.LastLoginDate > T.MinCreatedDate then U.Avatar else '' end as 'Avatar' FROM {0} UR INNER JOIN {1} U ON UR.UserID = U.UserID INNER JOIN (select min(CreatedDate) as MinCreatedDate, LenderID, UserID from {0} Group By LenderID, UserID) T on T.LenderID = UR.LenderID and T.UserID = UR.UserID ", USERROLES_TABLE, USERS_TABLE)
		Dim oWhere As New CSQLWhereStringBuilder()
		Dim oOrder As String = " "
		If Not String.IsNullOrEmpty(paging.SortColumn) Then
			oOrder = oOrder & "ORDER BY " & paging.SortColumn & " " & paging.SortDirection
		Else
			oOrder = oOrder & "ORDER BY UR.CreatedDate "
		End If
		If filter IsNot Nothing Then
			If Not String.IsNullOrEmpty(filter.Keyword) Then
				oWhere.AppendAndCondition("COALESCE(U.Email, '') LIKE {0}", New CSQLParamValue("%" & filter.Keyword & "%"))
			End If
			oWhere.AppendAndCondition("UR.LenderConfigID={0}", New CSQLParamValue(filter.LenderConfigID))
			'oWhere.AppendAndCondition("COALESCE(UR.VendorID, '') = ''")
		End If
		oWhere.AppendAndCondition("UR.Role = {0}", New CSQLParamValue(SmSettings.Role.PortalAdmin.ToString()))
		oWhere.AppendAndCondition("U.Status <> {0}", New CSQLParamValue(SmSettings.UserStatus.Deleted))
		Dim oDB As New CSQLDBUtils(LPQMOBILE_CONNECTIONSTRING)
		Try
			oData = oDB.getDataTable(sSQL & oWhere.SQL & oOrder)
			_log.Debug(sSQL & oWhere.SQL & oOrder)
			If oData IsNot Nothing AndAlso oData.Rows.Count > 0 Then
				For Each row As DataRow In oData.Rows
					Dim item As New SmUserRole()
					item.UserRoleID = Common.SafeGUID(row("UserRoleID"))
					item.Role = CType([Enum].Parse(GetType(SmSettings.Role), Common.SafeString(row("Role"))), SmSettings.Role)
					item.LenderConfigID = Common.SafeGUID(row("LenderConfigID"))
					item.LenderID = Common.SafeGUID(row("LenderID"))
					item.Status = DirectCast(Common.SafeInteger(row("Status")), SmSettings.UserRoleStatus)
					item.UserID = Common.SafeGUID(row("UserID"))
					item.CreatedDate = Common.SafeDate(row("CreatedDate"))
					item.User = New SmUser() With {
						.Email = Common.SafeString(row("Email")),
						.LastName = Common.SafeString(row("LastName")),
						.Phone = Common.SafeString(row("Phone")),
						.ExpireDays = Common.SafeInteger(row("ExpireDays")),
						.FirstName = Common.SafeString(row("FirstName")),
						.Avatar = Common.SafeString(row("Avatar")),
						.Status = DirectCast(Common.SafeInteger(row("UserStatus")), SmSettings.UserStatus),
						.LoginFailedCount = Common.SafeInteger(row("LoginFailedCount"))
					}
					If Not row.IsNull("LastLoginDate") Then
						item.User.LastLoginDate = Common.SafeDate(row("LastLoginDate"))
					End If
					result.Add(item)
				Next
			End If
			paging.TotalRecord = result.Count
			If paging.PageIndex > 0 AndAlso paging.PageSize > 0 Then
				Dim originalResult = result
				result = originalResult.Skip((paging.PageIndex - 1) * paging.PageSize).Take(paging.PageSize).ToList()
				If Not result.Any() Then
					paging.PageIndex = paging.PageIndex - 1
					result = originalResult.Skip((paging.PageIndex - 1) * paging.PageSize).Take(paging.PageSize).ToList()
				End If
			End If
		Catch ex As Exception
			_log.Error("Error while GetPortalUserRoles", ex)
		Finally
			oDB.closeConnection()
		End Try
		Return result
	End Function

	Public Function GetVendorGroupAdminUserRoles(ByRef paging As SmPagingInfoModel, filter As SmVendorGroupAdminUserRoleListFilter) As List(Of SmUserRole)
		Dim result As New List(Of SmUserRole)
		Dim oData As New DataTable()
		Dim sSQL As String = String.Format("SELECT UR.UserRoleID, UR.Role, UR.LenderConfigID, UR.LenderID, UR.Status, Ur.CreatedDate, U.UserID, U.Email, U.ExpireDays, U.Status as 'UserStatus', U.LoginFailedCount, U.LastLoginDate, CASE WHEN U.LastLoginDate IS NOT NULL AND U.LastLoginDate > T.MinCreatedDate then U.FirstName else '' end as 'FirstName', CASE WHEN U.LastLoginDate IS NOT NULL AND U.LastLoginDate > T.MinCreatedDate then U.LastName else '' end as 'LastName', CASE WHEN U.LastLoginDate IS NOT NULL AND U.LastLoginDate > T.MinCreatedDate then U.Phone else '' end as 'Phone', CASE WHEN U.LastLoginDate IS NOT NULL AND U.LastLoginDate > T.MinCreatedDate then U.Avatar else '' end as 'Avatar', UR.AssignedVendorGroupByTags FROM {0} UR INNER JOIN {1} U ON UR.UserID = U.UserID INNER JOIN (select min(CreatedDate) as MinCreatedDate, LenderID, UserID from {0} Group By LenderID, UserID) T on T.LenderID = UR.LenderID and T.UserID = UR.UserID ", USERROLES_TABLE, USERS_TABLE)
		Dim oWhere As New CSQLWhereStringBuilder()
		Dim oOrder As String = " "
		If Not String.IsNullOrEmpty(paging.SortColumn) Then
			oOrder = oOrder & "ORDER BY " & paging.SortColumn & " " & paging.SortDirection
		Else
			oOrder = oOrder & "ORDER BY UR.CreatedDate "
		End If
		If filter IsNot Nothing Then
			If Not String.IsNullOrEmpty(filter.Keyword) Then
				oWhere.AppendAndCondition("COALESCE(U.Email, '') LIKE {0}", New CSQLParamValue("%" & filter.Keyword & "%"))
			End If
			oWhere.AppendAndCondition("UR.LenderConfigID={0}", New CSQLParamValue(filter.LenderConfigID))
		End If
		oWhere.AppendAndCondition("UR.Role = {0}", New CSQLParamValue(SmSettings.Role.VendorGroupAdmin.ToString()))
		oWhere.AppendAndCondition("U.Status <> {0}", New CSQLParamValue(SmSettings.UserStatus.Deleted))
		Dim oDB As New CSQLDBUtils(LPQMOBILE_CONNECTIONSTRING)
		Try
			oData = oDB.getDataTable(sSQL & oWhere.SQL & oOrder)
			_log.Debug(sSQL & oWhere.SQL & oOrder)
			If oData IsNot Nothing AndAlso oData.Rows.Count > 0 Then
				For Each row As DataRow In oData.Rows
					Dim item As New SmUserRole()
					item.UserRoleID = Common.SafeGUID(row("UserRoleID"))
					item.Role = CType([Enum].Parse(GetType(SmSettings.Role), Common.SafeString(row("Role"))), SmSettings.Role)
					item.LenderConfigID = Common.SafeGUID(row("LenderConfigID"))
					item.LenderID = Common.SafeGUID(row("LenderID"))
					item.Status = DirectCast(Common.SafeInteger(row("Status")), SmSettings.UserRoleStatus)
					item.UserID = Common.SafeGUID(row("UserID"))
					item.CreatedDate = Common.SafeDate(row("CreatedDate"))
					item.AssignedVendorGroupByTags = Common.SafeString(row("AssignedVendorGroupByTags"))
					item.User = New SmUser() With {
						.Email = Common.SafeString(row("Email")),
						.LastName = Common.SafeString(row("LastName")),
						.Phone = Common.SafeString(row("Phone")),
						.ExpireDays = Common.SafeInteger(row("ExpireDays")),
						.FirstName = Common.SafeString(row("FirstName")),
						.Avatar = Common.SafeString(row("Avatar")),
						.Status = DirectCast(Common.SafeInteger(row("UserStatus")), SmSettings.UserStatus),
						.LoginFailedCount = Common.SafeInteger(row("LoginFailedCount"))
					}
					If Not row.IsNull("LastLoginDate") Then
						item.User.LastLoginDate = Common.SafeDate(row("LastLoginDate"))
					End If
					result.Add(item)
				Next
			End If
			paging.TotalRecord = result.Count
			If paging.PageIndex > 0 AndAlso paging.PageSize > 0 Then
				Dim originalResult = result
				result = originalResult.Skip((paging.PageIndex - 1) * paging.PageSize).Take(paging.PageSize).ToList()
				If Not result.Any() Then
					paging.PageIndex = paging.PageIndex - 1
					result = originalResult.Skip((paging.PageIndex - 1) * paging.PageSize).Take(paging.PageSize).ToList()
				End If
			End If
		Catch ex As Exception
			_log.Error("Error while GetVendorGroupAdminUserRoles", ex)
		Finally
			oDB.closeConnection()
		End Try
		Return result
	End Function

	Public Function GetVendorUserRoles(ByRef paging As SmPagingInfoModel, filter As SmVendorUserRoleListFilter) As List(Of SmUserRole)
		Dim result As New List(Of SmUserRole)
		Dim oData As New DataTable()
		'Dim sSQL As String = String.Format("SELECT UR.UserRoleID, UR.Role, UR.LenderConfigID, UR.LenderID, UR.Status, LV.VendorID, VendorName, LV.City as VendorCity, LV.State as VendorState, LV.Zip as VendorZip, LV.Phone as VendorPhone, LV.Fax as VendorFax, LV.DefaultLpqWorkerID, LV.DealerNumberLpq, U.* FROM {0} UR INNER JOIN {1} U ON UR.UserID = U.UserID INNER JOIN {2} LV ON LV.VendorID = UR.VendorID AND COALESCE(UR.VendorID, '') <> '' ", USERROLES_TABLE, USERS_TABLE, LENDERVENDORS_TABLE)
		'for backward compability, get firstname, lastname...from Users when there is no corresponding record in VendorAgentProfles table
		Dim sSQL As String = String.Format("SELECT UR.UserRoleID, UR.Role, UR.LenderConfigID, UR.LenderID, UR.Status, LV.VendorID, VendorName, LV.City as VendorCity, LV.State as VendorState, LV.Zip as VendorZip, LV.Phone as VendorPhone, LV.Fax as VendorFax, LV.DefaultLpqWorkerID, LV.DealerNumberLpq, Ur.CreatedDate, U.UserID, U.Email, U.ExpireDays, U.Status as 'UserStatus', U.LoginFailedCount, U.LastLoginDate, CASE WHEN U.LastLoginDate IS NOT NULL AND U.LastLoginDate > T.MinCreatedDate then U.FirstName else '' end as 'FirstName', CASE WHEN U.LastLoginDate IS NOT NULL AND U.LastLoginDate > T.MinCreatedDate then U.LastName else '' end as 'LastName', CASE WHEN U.LastLoginDate IS NOT NULL AND U.LastLoginDate > T.MinCreatedDate then U.Phone else '' end as 'Phone', CASE WHEN U.LastLoginDate IS NOT NULL AND U.LastLoginDate > T.MinCreatedDate then U.Avatar else '' end as 'Avatar', LV.Tags FROM {0} UR INNER JOIN {1} U ON UR.UserID = U.UserID  INNER JOIN {2} LV ON LV.VendorID = UR.VendorID AND COALESCE(UR.VendorID, '') <> '' INNER JOIN (select min(CreatedDate) as MinCreatedDate, LenderID, UserID from {0} Group By LenderID, UserID) T on T.LenderID = UR.LenderID and T.UserID = UR.UserID ", USERROLES_TABLE, USERS_TABLE, LENDERVENDORS_TABLE)
		Dim oWhere As New CSQLWhereStringBuilder()
		Dim oOrder As String = " "
		If Not String.IsNullOrEmpty(paging.SortColumn) Then
			oOrder = oOrder & "ORDER BY " & paging.SortColumn & " " & paging.SortDirection
		End If
		If filter IsNot Nothing Then
			If Not String.IsNullOrEmpty(filter.Keyword) Then
				oWhere.AppendAndCondition("(COALESCE(U.Email, '') + '$' + COALESCE(LV.VendorName, '') + '$' + COALESCE(LV.City, '') + '$' + COALESCE(LV.State, '') + '$' + COALESCE(LV.Zip, '') + '$' + COALESCE(LV.Phone, '') + '$' + COALESCE(LV.Fax, '') + '$' + COALESCE(LV.DefaultLpqWorkerID, '') + '$' + COALESCE(LV.DealerNumberLpq, '')) LIKE {0}", New CSQLParamValue("%" & filter.Keyword & "%"))
			End If
			oWhere.AppendAndCondition("UR.LenderConfigID={0}", New CSQLParamValue(filter.LenderConfigID))
			If filter.Roles IsNot Nothing AndAlso filter.Roles.Any() Then
				oWhere.AppendAndIn("UR.Role", (From role In filter.Roles Select New CSQLParamValue(role.ToString())).ToList())
			End If
			If filter.VendorIDList IsNot Nothing AndAlso filter.VendorIDList.Any() Then
				oWhere.AppendAndIn("UR.VendorID", (From vendorID In filter.VendorIDList Select New CSQLParamValue(vendorID)).ToList())
			End If
		End If
		Dim oDB As New CSQLDBUtils(LPQMOBILE_CONNECTIONSTRING)
		Try
			oData = oDB.getDataTable(sSQL & oWhere.SQL & oOrder)
			_log.Debug(sSQL & oWhere.SQL & oOrder)
			If oData IsNot Nothing AndAlso oData.Rows.Count > 0 Then
				For Each row As DataRow In oData.Rows
					Dim item As New SmUserRole()
					Dim tags As String = Common.SafeString(row("Tags"))
					'Vendors with empty tags can be seen by everyone
					If Not String.IsNullOrWhiteSpace(tags) Then
						Dim tagList As List(Of String) = tags.Split(","c).ToList()
						'vendor group admin can see those vendors that belongs to groups(tags) that they are assigned to
						If filter.FilterByTags AndAlso Not filter.TagList.Any(Function(x) tagList.Contains(x)) Then
							Continue For
						End If
					End If
					item.UserRoleID = Common.SafeGUID(row("UserRoleID"))
					item.Role = CType([Enum].Parse(GetType(SmSettings.Role), Common.SafeString(row("Role"))), SmSettings.Role)
					item.LenderConfigID = Common.SafeGUID(row("LenderConfigID"))
					item.LenderID = Common.SafeGUID(row("LenderID"))
					item.Status = DirectCast(Common.SafeInteger(row("Status")), SmSettings.UserRoleStatus)
					item.UserID = Common.SafeGUID(row("UserID"))
					item.CreatedDate = Common.SafeDate(row("CreatedDate"))
					item.User = New SmUser() With {
						.Email = Common.SafeString(row("Email")),
						.LastName = Common.SafeString(row("LastName")),
						.Phone = Common.SafeString(row("Phone")),
						.ExpireDays = Common.SafeInteger(row("ExpireDays")),
						.FirstName = Common.SafeString(row("FirstName")),
						.Avatar = Common.SafeString(row("Avatar")),
						.Status = DirectCast(Common.SafeInteger(row("UserStatus")), SmSettings.UserStatus),
						.LoginFailedCount = Common.SafeInteger(row("LoginFailedCount"))
					}
					If Not row.IsNull("LastLoginDate") Then
						item.User.LastLoginDate = Common.SafeDate(row("LastLoginDate"))
					End If
					item.VendorID = Common.SafeString(row("VendorID"))
					item.Vendor = New SmLenderVendor() With {
						.VendorName = Common.SafeString(row("VendorName")),
						.City = Common.SafeString(row("VendorCity")),
						.State = Common.SafeString(row("VendorState")),
						.Zip = Common.SafeString(row("VendorZip")),
						.Phone = Common.SafeString(row("VendorPhone")),
						.Fax = Common.SafeString(row("VendorFax")),
						.DefaultLpqWorkerID = Common.SafeString(row("DefaultLpqWorkerID")),
						.DealerNumberLpq = Common.SafeString(row("DealerNumberLpq"))
					}
					result.Add(item)
				Next
			End If
			paging.TotalRecord = result.Count
			If paging.PageIndex > 0 AndAlso paging.PageSize > 0 Then
				Dim originalResult = result
				result = originalResult.Skip((paging.PageIndex - 1) * paging.PageSize).Take(paging.PageSize).ToList()
				If Not result.Any() Then
					paging.PageIndex = paging.PageIndex - 1
					result = originalResult.Skip((paging.PageIndex - 1) * paging.PageSize).Take(paging.PageSize).ToList()
				End If
			End If
		Catch ex As Exception
			_log.Error("Error while GetVendorUserRoles", ex)
		Finally
			oDB.closeConnection()
		End Try
		Return result
	End Function

	Public Function GetLenderUsers(ByRef paging As SmPagingInfoModel, filter As SmLenderUserListFilter) As List(Of SmUser)
		Dim result As New List(Of SmUser)
		Dim oData As New DataTable()
		Dim sSQL As String = String.Format("SELECT distinct U.* FROM {0} U inner join {1} UR on U.UserID = UR.UserID", USERS_TABLE, USERROLES_TABLE)
		Dim oWhere As New CSQLWhereStringBuilder()
		Dim excludedDeletedItem As Boolean = True
		If filter IsNot Nothing Then
			If filter.Roles IsNot Nothing AndAlso filter.Roles.Any() Then
				Dim list As New List(Of CSQLParamValue)
				For Each role As SmSettings.Role In filter.Roles
					list.Add(New CSQLParamValue(role.ToString()))
				Next
				oWhere.AppendAndIn("UR.Role", list)
			End If
			If Not String.IsNullOrEmpty(filter.Keyword) Then
				oWhere.AppendAndCondition("(FirstName + '$' + LastName + '$' + Email) LIKE {0}", New CSQLParamValue("%" & filter.Keyword & "%"))
			End If
			If filter.Status IsNot Nothing AndAlso filter.Status.Any() Then
				Dim list As New List(Of CSQLParamValue)
				For Each status As SmSettings.Role In filter.Status
					list.Add(New CSQLParamValue(status))
				Next
				oWhere.AppendAndIn("U.Status", list)
				excludedDeletedItem = False
			End If
			If filter.LenderID <> Guid.Empty Then
				oWhere.AppendAndCondition("LenderID = {0}", New CSQLParamValue(filter.LenderID))
			End If
			If filter.PortalID <> Guid.Empty Then
				oWhere.AppendAndCondition("PortalID = {0}", New CSQLParamValue(filter.LenderID))
			End If
			If Not String.IsNullOrEmpty(filter.VendorID) Then
				oWhere.AppendAndCondition("VendorID = {0}", New CSQLParamValue(filter.VendorID))
			End If
			oWhere.AppendAndCondition("UR.Status <> {0}", New CSQLParamValue(SmSettings.UserRoleStatus.Deleted))
		End If
		If excludedDeletedItem Then
			oWhere.AppendAndCondition("Status<>0")	'Deleted
		End If
		Dim oOrder As String = " "
		If Not String.IsNullOrEmpty(paging.SortColumn) Then
			oOrder = oOrder & "ORDER BY " & paging.SortColumn & " " & paging.SortDirection
		Else
			oOrder = oOrder & "ORDER BY FirstName, LastName"
		End If
		Dim oDB As New CSQLDBUtils(LPQMOBILE_CONNECTIONSTRING)
		Try
			oData = oDB.getDataTable(sSQL & oWhere.SQL & oOrder)
			If oData IsNot Nothing AndAlso oData.Rows.Count > 0 Then
				For Each row As DataRow In oData.Rows
					Dim item As New SmUser()
					item.UserID = Common.SafeGUID(row("UserID"))
					item.Email = Common.SafeString(row("Email"))
					item.Password = Common.SafeString(row("Password"))
					item.Salt = Common.SafeString(row("Salt"))
					item.CreatedDate = Common.SafeDate(row("CreatedDate"))
					If Not row.IsNull("LastLoginDate") Then
						item.LastLoginDate = Common.SafeDate(row("LastLoginDate"))
					End If
					item.Status = DirectCast(Common.SafeInteger(row("Status")), SmSettings.UserStatus)
					If Not row.IsNull("Role") AndAlso Not String.IsNullOrEmpty(Common.SafeString(row("Role"))) Then
						item.Role = CType([Enum].Parse(GetType(SmSettings.Role), Common.SafeString(row("Role"))), SmSettings.Role)

					End If
					item.FirstName = Common.SafeString(row("FirstName"))
					item.LastName = Common.SafeString(row("LastName"))
					item.Phone = Common.SafeString(row("Phone"))
					item.Avatar = Common.SafeString(row("Avatar"))
					item.LoginFailedCount = Common.SafeInteger(row("LoginFailedCount"))
					item.ExpireDays = Common.SafeInteger(row("ExpireDays"))
					result.Add(item)
				Next
			End If
			paging.TotalRecord = result.Count
			If paging.PageIndex > 0 AndAlso paging.PageSize > 0 Then
				Dim originalResult = result
				result = originalResult.Skip((paging.PageIndex - 1) * paging.PageSize).Take(paging.PageSize).ToList()
				If Not result.Any() Then
					paging.PageIndex = paging.PageIndex - 1
					result = originalResult.Skip((paging.PageIndex - 1) * paging.PageSize).Take(paging.PageSize).ToList()
				End If
			End If
		Catch ex As Exception
			_log.Error("Error while GetLenderUsers", ex)
		Finally
			oDB.closeConnection()
		End Try
		Return result
	End Function
	Public Function ChangeLenderUserRoleState(userRoleID As Guid, status As SmSettings.UserRoleStatus, backOfficeLender As SmBackOfficeLender, user As SmUser, createdByUserId As Guid, ipAddress As String, comment As String) As String
		Dim result As String = ""
		Dim oDB As New CSQLDBUtils(LPQMOBILE_CONNECTIONSTRING)
		Try
			Dim oWhere As New CSQLWhereStringBuilder()
			Dim actionLogBL As New SmActionLogBL()
			oWhere.AppendAndCondition("UserRoleID={0}", New CSQLParamValue(userRoleID))
			Dim oUpdate As New cSQLUpdateStringBuilder(USERROLES_TABLE, oWhere)
			oUpdate.appendvalue("Status", New CSQLParamValue(status))
			oDB.executeNonQuery(oUpdate.SQL, False)
			actionLogBL.WriteActionLog(New SmActionLog() With {.Action = SmActionLogAction.CHANGE_LENDER_USER_ROLE_STATE, .Category = SmActionLogCategory.USER_ROLES_MANIPULATION, .CreatedByUserID = createdByUserId, .IpAddress = ipAddress, .ObjectID = userRoleID.ToString(), .ActionNote = String.Format("Lender {0}: Change role status of user {1} to {2} with comment: ""{3}""", backOfficeLender.LenderName, user.Email, status.GetEnumDescription(), comment)})
			result = "ok"
		Catch ex As Exception
			_log.Error("Error while ChangeLenderUserRoleState", ex)
			result = "error"
		Finally
			oDB.closeConnection()
		End Try
		Return result
	End Function

	Public Function ChangePortalUserRoleState(userRoleID As Guid, status As SmSettings.UserRoleStatus, lenderConfig As SmLenderConfigBasicInfo, user As SmUser, createdByUserId As Guid, ipAddress As String, comment As String) As String
		Dim result As String = ""
		Dim oDB As New CSQLDBUtils(LPQMOBILE_CONNECTIONSTRING)
		Try
			Dim oWhere As New CSQLWhereStringBuilder()
			Dim actionLogBL As New SmActionLogBL()
			oWhere.AppendAndCondition("UserRoleID={0}", New CSQLParamValue(userRoleID))
			Dim oUpdate As New cSQLUpdateStringBuilder(USERROLES_TABLE, oWhere)
			oUpdate.appendvalue("Status", New CSQLParamValue(status))
			oDB.executeNonQuery(oUpdate.SQL, False)
			actionLogBL.WriteActionLog(New SmActionLog() With {.Action = SmActionLogAction.CHANGE_PORTAL_USER_ROLE_STATE, .Category = SmActionLogCategory.USER_ROLES_MANIPULATION, .CreatedByUserID = createdByUserId, .IpAddress = ipAddress, .ObjectID = userRoleID.ToString(), .ActionNote = String.Format("Portal {0}: Change role status of user {1} to {2} with comment: ""{3}""", lenderConfig.Note, user.Email, status.GetEnumDescription(), comment)})
			result = "ok"
		Catch ex As Exception
			_log.Error("Error while ChangePortalUserRoleState", ex)
			result = "error"
		Finally
			oDB.closeConnection()
		End Try
		Return result
	End Function

	Public Function ChangeVendorGroupAdminUserRoleState(userRoleID As Guid, status As SmSettings.UserRoleStatus, lenderConfig As SmLenderConfigBasicInfo, user As SmUser, createdByUserId As Guid, ipAddress As String, comment As String) As String
		Dim result As String = ""
		Dim oDB As New CSQLDBUtils(LPQMOBILE_CONNECTIONSTRING)
		Try
			Dim oWhere As New CSQLWhereStringBuilder()
			Dim actionLogBL As New SmActionLogBL()
			oWhere.AppendAndCondition("UserRoleID={0}", New CSQLParamValue(userRoleID))
			Dim oUpdate As New cSQLUpdateStringBuilder(USERROLES_TABLE, oWhere)
			oUpdate.appendvalue("Status", New CSQLParamValue(status))
			oDB.executeNonQuery(oUpdate.SQL, False)
			actionLogBL.WriteActionLog(New SmActionLog() With {.Action = SmActionLogAction.CHANGE_VENDOR_GROUP_ADMIN_USER_ROLE_STATE, .Category = SmActionLogCategory.USER_ROLES_MANIPULATION, .CreatedByUserID = createdByUserId, .IpAddress = ipAddress, .ObjectID = userRoleID.ToString(), .ActionNote = String.Format("Portal {0}: Change role status of user {1} to {2} with comment: ""{3}""", lenderConfig.Note, user.Email, status.GetEnumDescription(), comment)})
			result = "ok"
		Catch ex As Exception
			_log.Error("Error while ChangeVendorGroupAdminUserRoleState", ex)
			result = "error"
		Finally
			oDB.closeConnection()
		End Try
		Return result
	End Function

	Public Function ChangeVendorUserRoleState(userRoleID As Guid, status As SmSettings.UserRoleStatus, lenderConfig As SmLenderConfigBasicInfo, user As SmUser, vendor As SmLenderVendor, createdByUserId As Guid, ipAddress As String, comment As String) As String
		Dim result As String = ""
		Dim oDB As New CSQLDBUtils(LPQMOBILE_CONNECTIONSTRING)
		Try
			Dim oWhere As New CSQLWhereStringBuilder()
			Dim actionLogBL As New SmActionLogBL()
			oWhere.AppendAndCondition("UserRoleID={0}", New CSQLParamValue(userRoleID))
			Dim oUpdate As New cSQLUpdateStringBuilder(USERROLES_TABLE, oWhere)
			oUpdate.appendvalue("Status", New CSQLParamValue(status))
			oDB.executeNonQuery(oUpdate.SQL, False)
			actionLogBL.WriteActionLog(New SmActionLog() With {.Action = SmActionLogAction.CHANGE_VENDOR_USER_ROLE_STATE, .Category = SmActionLogCategory.USER_ROLES_MANIPULATION, .CreatedByUserID = createdByUserId, .IpAddress = ipAddress, .ObjectID = userRoleID.ToString(), .ActionNote = String.Format("Portal {0}: Change role status of user belong to vendor {4} from {1} to {2} with comment: ""{3}""", lenderConfig.Note, user.Email, status.GetEnumDescription(), comment, vendor.VendorName)})
			result = "ok"
		Catch ex As Exception
			_log.Error("Error while ChangeVendorUserRoleState", ex)
			result = "error"
		Finally
			oDB.closeConnection()
		End Try
		Return result
	End Function

	Public Function RemoveLenderUserRole(userRoleID As Guid, comment As String, backOfficeLender As SmBackOfficeLender, user As SmUser, createdByUserId As Guid, ipAddress As String) As String
		Dim result As String = ""
		Dim oDB As New CSQLDBUtils(LPQMOBILE_CONNECTIONSTRING)
		Try
			Dim oWhere As New CSQLWhereStringBuilder()
			oWhere.AppendAndCondition("UserRoleID={0}", New CSQLParamValue(userRoleID))
			Dim oDelete As New CSQLDeleteStringBuilder(USERROLES_TABLE, oWhere)
			oDB.executeNonQuery(oDelete.SQL, False)
			Dim actionLogBL As New SmActionLogBL()
			actionLogBL.WriteActionLog(New SmActionLog() With {.Action = SmActionLogAction.REMOVE_LENDER_USER_ROLE, .Category = SmActionLogCategory.USER_ROLES_MANIPULATION, .CreatedByUserID = createdByUserId, .IpAddress = ipAddress, .ObjectID = userRoleID.ToString(), .ActionNote = String.Format("Lender {0}: Remove a role of user {1} with comment ""{2}""", backOfficeLender.LenderName, user.Email, comment)})
			result = "ok"
		Catch ex As Exception
			_log.Error("Error while RemoveLenderUserRole", ex)
			result = "error"
		Finally
			oDB.closeConnection()
		End Try
		Return result
	End Function

	Public Function RemovePortalUserRole(userRoleID As Guid, comment As String, lenderConfig As SmLenderConfigBasicInfo, user As SmUser, createdByUserId As Guid, ipAddress As String) As String
		Dim result As String = ""
		Dim oDB As New CSQLDBUtils(LPQMOBILE_CONNECTIONSTRING)
		Try
			Dim oWhere As New CSQLWhereStringBuilder()
			oWhere.AppendAndCondition("UserRoleID={0}", New CSQLParamValue(userRoleID))
			Dim oDelete As New CSQLDeleteStringBuilder(USERROLES_TABLE, oWhere)
			oDB.executeNonQuery(oDelete.SQL, False)
			Dim actionLogBL As New SmActionLogBL()
			actionLogBL.WriteActionLog(New SmActionLog() With {.Action = SmActionLogAction.REMOVE_PORTAL_USER_ROLE, .Category = SmActionLogCategory.USER_ROLES_MANIPULATION, .CreatedByUserID = createdByUserId, .IpAddress = ipAddress, .ObjectID = userRoleID.ToString(), .ActionNote = String.Format("Portal {0}: Remove a role of user {1} with comment ""{2}""", lenderConfig.Note, user.Email, comment)})
			result = "ok"
		Catch ex As Exception
			_log.Error("Error while RemovePortalUserRole", ex)
			result = "error"
		Finally
			oDB.closeConnection()
		End Try
		Return result
	End Function

	Public Function RemoveVendorGroupAdminUserRole(userRoleID As Guid, comment As String, lenderConfig As SmLenderConfigBasicInfo, user As SmUser, createdByUserId As Guid, ipAddress As String) As String
		Dim result As String = ""
		Dim oDB As New CSQLDBUtils(LPQMOBILE_CONNECTIONSTRING)
		Try
			Dim oWhere As New CSQLWhereStringBuilder()
			oWhere.AppendAndCondition("UserRoleID={0}", New CSQLParamValue(userRoleID))
			Dim oDelete As New CSQLDeleteStringBuilder(USERROLES_TABLE, oWhere)
			oDB.executeNonQuery(oDelete.SQL, False)
			Dim actionLogBL As New SmActionLogBL()
			actionLogBL.WriteActionLog(New SmActionLog() With {.Action = SmActionLogAction.REMOVE_VENDOR_GROUP_ADMIN_USER_ROLE, .Category = SmActionLogCategory.USER_ROLES_MANIPULATION, .CreatedByUserID = createdByUserId, .IpAddress = ipAddress, .ObjectID = userRoleID.ToString(), .ActionNote = String.Format("Portal {0}: Remove a role of user {1} with comment ""{2}""", lenderConfig.Note, user.Email, comment)})
			result = "ok"
		Catch ex As Exception
			_log.Error("Error while RemoveVendorGroupAdminUserRole", ex)
			result = "error"
		Finally
			oDB.closeConnection()
		End Try
		Return result
	End Function

	Public Function RemoveVendorUser(userRole As SmUserRole, comment As String, lenderConfig As SmLenderConfigBasicInfo, user As SmUser, createdByUserId As Guid, ipAddress As String) As String
		Dim result As String = ""
		Dim oDB As New CSQLDBUtils(LPQMOBILE_CONNECTIONSTRING)
		Try
			Dim oWhere As New CSQLWhereStringBuilder()
			oWhere.AppendAndCondition("UserRoleID={0}", New CSQLParamValue(userRole.UserRoleID))
			Dim oDelete As New CSQLDeleteStringBuilder(USERROLES_TABLE, oWhere)
			oDB.executeNonQuery(oDelete.SQL, True)
			'oWhere = New CSQLWhereStringBuilder()
			'oWhere.AppendAndCondition("VendorID={0}", New CSQLParamValue(userRole.VendorID))
			'oWhere.AppendAndCondition("UserID={0}", New CSQLParamValue(userRole.UserID))
			'oDelete = New CSQLDeleteStringBuilder(VENDORAGENTPROFILES_TABLE, oWhere)
			'oDB.executeNonQuery(oDelete.SQL, True)
			Dim actionLogBL As New SmActionLogBL()
			actionLogBL.WriteActionLog(New SmActionLog() With {.Action = SmActionLogAction.REMOVE_VENDOR_USER, .Category = SmActionLogCategory.USER_ROLES_MANIPULATION, .CreatedByUserID = createdByUserId, .IpAddress = ipAddress, .ObjectID = userRole.UserRoleID.ToString(), .ActionNote = String.Format("Portal {0}: Remove user {1} belong to vendor {3} with comment ""{2}""", lenderConfig.Note, user.Email, comment, userRole.Vendor.VendorName)})
			result = "ok"
		Catch ex As Exception
			_log.Error("Error while RemoveVendorUserRole", ex)
			result = "error"
		Finally
			oDB.commitTransactionIfOpen()
			oDB.closeConnection()
		End Try
		Return result
	End Function

	Public Function WriteGlobalConfigItems(configItems As Dictionary(Of String, SmGlobalConfigItem), lenderID As Guid, createdByUserId As Guid, ipAddress As String) As Boolean
		Dim oDB As New CSQLDBUtils(LPQMOBILE_CONNECTIONSTRING)
		Dim result = False
		Try
			Dim oWhere As New CSQLWhereStringBuilder()
			oWhere.AppendAndIn("Name", configItems.Select(Function(p) New CSQLParamValue(p.Key)).ToList())
			oWhere.AppendAndCondition("LenderID={0}", New CSQLParamValue(lenderID))
			Dim oDelete As New CSQLDeleteStringBuilder(GLOBAL_CONFIGS_TABLE, oWhere)
			oDB.executeNonQuery(oDelete.SQL, True)
			For Each item As KeyValuePair(Of String, SmGlobalConfigItem) In configItems
				Dim oInsert As New cSQLInsertStringBuilder(GLOBAL_CONFIGS_TABLE)
				oInsert.AppendValue("ID", New CSQLParamValue(item.Value.ID))
				oInsert.AppendValue("LenderID", New CSQLParamValue(lenderID))
				oInsert.AppendValue("RevisionID", New CSQLParamValue(item.Value.RevisionID))
				oInsert.AppendValue("ConfigJson", New CSQLParamValue(item.Value.ConfigJson))
				oInsert.AppendValue("Name", New CSQLParamValue(item.Value.Name))
				oInsert.AppendValue("LastModifiedDate", New CSQLParamValue(item.Value.LastModifiedDate))
				oDB.executeNonQuery(oInsert.SQL, True)
			Next
			Dim actionLogBL As New SmActionLogBL()
			actionLogBL.WriteActionLog(New SmActionLog() With {.Action = SmActionLogAction.UPDATE_GLOBAL_CONFIGS, .Category = SmActionLogCategory.GLOBAL_CONFIGS_MANIPULATION, .CreatedByUserID = createdByUserId, .IpAddress = ipAddress, .ObjectID = lenderID.ToString(), .ActionNote = String.Format("Update global configurations of lender")})
			result = True
		Catch ex As Exception
			_log.Error("Error while WriteGlobalConfigItems", ex)
			result = False
		Finally
			oDB.commitTransactionIfOpen()
			oDB.closeConnection()
		End Try
		Return result
	End Function

	Public Function ReadGlobalConfigItems(configNames As List(Of String), lenderID As Guid) As Dictionary(Of String, SmGlobalConfigItem)
		If configNames Is Nothing OrElse configNames.Count = 0 Then Return New Dictionary(Of String, SmGlobalConfigItem)()
		Dim sSQL As String = String.Format("select * from {0}", GLOBAL_CONFIGS_TABLE)
		Dim oWhere As New CSQLWhereStringBuilder()
		oWhere.AppendAndIn("Name", configNames.Select(Function(p) New CSQLParamValue(p)).ToList())
		oWhere.AppendAndCondition("LenderID={0}", New CSQLParamValue(lenderID))
		Dim oDB As New CSQLDBUtils(LPQMOBILE_CONNECTIONSTRING)
		Dim result As New Dictionary(Of String, SmGlobalConfigItem)
		Try
			Dim oData = oDB.getDataTable(sSQL & oWhere.SQL)
			If oData IsNot Nothing AndAlso oData.Rows.Count > 0 Then
				For Each row As DataRow In oData.Rows
					Dim item As New SmGlobalConfigItem
					item.ID = Common.SafeGUID(row("ID"))
					item.LenderConfigID = Common.SafeGUID(row("LenderConfigID"))
					item.LenderID = Common.SafeGUID(row("LenderID"))
					item.VendorID = Common.SafeString(row("VendorID"))
					item.RevisionID = Common.SafeInteger(row("RevisionID"))
					item.ConfigJson = Common.SafeString(row("ConfigJson"))
					item.Name = Common.SafeString(row("Name"))
					item.LastModifiedDate = Common.SafeDate(row("LastModifiedDate"))
					If Not result.ContainsKey(item.Name) Then
						result.Add(item.Name, item)
					ElseIf result(item.Name).RevisionID < item.RevisionID Then
						result(item.Name) = item
					End If
				Next
			End If
		Catch ex As Exception
			_log.Error("Error while ReadGlobalConfigItems", ex)
		Finally
			oDB.closeConnection()
		End Try
		Return result
	End Function

	Public Function UpdateBackOfficeLenderInfo(backOfficeLender As SmBackOfficeLender, createdByUserId As Guid, ipAddress As String) As String
		Dim result As String = ""
		Dim oDB As New CSQLDBUtils(LPQMOBILE_CONNECTIONSTRING)
		Try
			Dim oWhere As New CSQLWhereStringBuilder()
			oWhere.AppendAndCondition("BackOfficeLenderID={0}", New CSQLParamValue(backOfficeLender.BackOfficeLenderID))
			Dim oUpdate As New cSQLUpdateStringBuilder(BACKOFFICELENDERS_TABLE, oWhere)
			oUpdate.appendvalue("LenderName", New CSQLParamValue(backOfficeLender.LenderName))
			oUpdate.appendvalue("CustomerInternalID", New CSQLParamValue(backOfficeLender.CustomerInternalID))
			oUpdate.appendvalue("City", New CSQLParamValue(backOfficeLender.City))
			oUpdate.appendvalue("State", New CSQLParamValue(backOfficeLender.State))
			oDB.executeNonQuery(oUpdate.SQL, False)

			Dim actionLogBL As New SmActionLogBL()
			actionLogBL.WriteActionLog(New SmActionLog() With {.Action = SmActionLogAction.EDIT_USER, .Category = SmActionLogCategory.USERS_MANIPULATION, .CreatedByUserID = createdByUserId, .IpAddress = ipAddress, .ObjectID = backOfficeLender.BackOfficeLenderID.ToString(), .ActionNote = String.Format("Edit back office lender info of {0}", backOfficeLender.LenderName)})
			result = "ok"
		Catch ex As Exception
			_log.Error("Error while UpdateBackOfficeLenderInfo", ex)
			result = "error"
		Finally
			oDB.closeConnection()
		End Try
		Return result
	End Function

	Public Function UpdateLenderConfigInfo(lenderConfig As SmLenderConfigBasicInfo, createdByUserId As Guid, ipAddress As String) As String
		Dim result As String = ""
		Dim oDB As New CSQLDBUtils(LPQMOBILE_CONNECTIONSTRING)
		Try
			Dim oWhere As New CSQLWhereStringBuilder()
			oWhere.AppendAndCondition("LenderConfigID={0}", New CSQLParamValue(lenderConfig.LenderConfigID))
			Dim oUpdate As New cSQLUpdateStringBuilder(LENDERCONFIG_TABLE, oWhere)
			oUpdate.appendvalue("note", New CSQLParamValue(lenderConfig.Note))
			oUpdate.appendvalue("LenderRef", New CSQLParamValue(lenderConfig.LenderRef))
			oDB.executeNonQuery(oUpdate.SQL, False)

			Dim actionLogBL As New SmActionLogBL()
			actionLogBL.WriteActionLog(New SmActionLog() With {.Action = SmActionLogAction.EDIT_LENDER_CONFIG, .Category = SmActionLogCategory.LENDERS_MANIPULATION, .CreatedByUserID = createdByUserId, .IpAddress = ipAddress, .ObjectID = lenderConfig.LenderConfigID.ToString(), .ActionNote = String.Format("Edit lender config info of {0}", lenderConfig.LenderRef)})
			result = "ok"
		Catch ex As Exception
			_log.Error("Error while UpdateLenderConfigInfo", ex)
			result = "error"
		Finally
			oDB.closeConnection()
		End Try
		Return result
	End Function

	Public Function AddBackOfficeLenderInfo(backOfficeLender As SmBackOfficeLender, createdByUserId As Guid, ipAddress As String) As String
		Dim result As String = ""
		Dim oDB As New CSQLDBUtils(LPQMOBILE_CONNECTIONSTRING)
		Try

			Dim oInsert As New cSQLInsertStringBuilder(BACKOFFICELENDERS_TABLE)
			oInsert.AppendValue("BackOfficeLenderID", New CSQLParamValue(backOfficeLender.BackOfficeLenderID))
			oInsert.AppendValue("LenderName", New CSQLParamValue(backOfficeLender.LenderName))
			oInsert.AppendValue("CustomerInternalID", New CSQLParamValue(backOfficeLender.CustomerInternalID))
			oInsert.AppendValue("City", New CSQLParamValue(backOfficeLender.City))
			oInsert.AppendValue("State", New CSQLParamValue(backOfficeLender.State))
			oInsert.AppendValue("LenderID", New CSQLParamValue(backOfficeLender.LenderID))
			oInsert.AppendValue("LpqUrl", New CSQLParamValue(backOfficeLender.LpqUrl))
			oDB.executeNonQuery(oInsert.SQL, False)

			Dim actionLogBL As New SmActionLogBL()
			actionLogBL.WriteActionLog(New SmActionLog() With {.Action = SmActionLogAction.ADD_BACK_OFFICE_LENDER, .Category = SmActionLogCategory.LENDERS_MANIPULATION, .CreatedByUserID = createdByUserId, .IpAddress = ipAddress, .ObjectID = backOfficeLender.BackOfficeLenderID.ToString(), .ActionNote = String.Format("Add back-office lender info of {0}", backOfficeLender.LenderName)})
			result = "ok"
		Catch ex As Exception
			_log.Error("Error while AddBackOfficeLenderInfo", ex)
			result = "error"
		Finally
			oDB.closeConnection()
		End Try
		Return result
	End Function


	Public Function GetLenderConfigLogs(ByRef paging As SmPagingInfoModel, lenderConfigID As Guid) As List(Of SmLenderConfigLogItem)
		Dim result As New List(Of SmLenderConfigLogItem)
		Dim oData As New DataTable()
		Dim sSQL As String = String.Format("SELECT * FROM {0} L LEFT JOIN {1} U on L.LastModifiedByUserID = U.UserID ", LENDERCONFIG_LOGS_TABLE, USERS_TABLE)
		Dim oWhere As New CSQLWhereStringBuilder()
		Dim oOrder As String = " "
		If Not String.IsNullOrEmpty(paging.SortColumn) Then
			oOrder = oOrder & "ORDER BY " & paging.SortColumn & " " & paging.SortDirection
		Else
			oOrder = oOrder & "ORDER BY RevisionID DESC"
		End If
		oWhere.AppendAndCondition("RevisionID>{0}", New CSQLParamValue(0))
		If lenderConfigID <> Guid.Empty Then
			oWhere.AppendAndCondition("LenderConfigID={0}", New CSQLParamValue(lenderConfigID))
		End If
		Dim oDB As New CSQLDBUtils(LPQMOBILE_CONNECTIONSTRING)
		Try
			oData = oDB.getDataTable(sSQL & oWhere.SQL & oOrder)
			If oData IsNot Nothing AndAlso oData.Rows.Count > 0 Then
				For Each row As DataRow In oData.Rows
					Dim item As New SmLenderConfigLogItem()
					item.LenderConfigLogID = Common.SafeGUID(row("LenderConfigLogID"))
					item.OrganizationID = Common.SafeGUID(row("OrganizationID"))
					item.LenderID = Common.SafeGUID(row("LenderID"))
					item.LenderRef = Common.SafeString(row("LenderRef"))
					item.ReturnURL = Common.SafeString(row("ReturnURL"))
					item.LPQLogin = Common.SafeString(row("LPQLogin"))
					item.LPQURL = Common.SafeString(row("LPQURL"))
					'comment out for performance
					'item.LPQPW = Common.DecryptPassword(Common.SafeString(row("LPQPW")))
					'item.ConfigData = Common.DecryptAllPasswordInConfigStrXml(CStringCompressor.Decompress(Common.SafeString(row("ConfigData"))))
					item.LogTime = Common.SafeDate(row("LogTime"))
					item.RemoteIP = Common.SafeString(row("RemoteIP"))
					item.LastModifiedByUserID = Common.SafeGUID(row("LastModifiedByUserID"))
					If item.LastModifiedByUserID <> Guid.Empty Then
						item.LastModifiedBy = New SmUser()
						item.LastModifiedBy.UserID = item.LastModifiedByUserID
						item.LastModifiedBy.FirstName = Common.SafeString(row("FirstName"))
						item.LastModifiedBy.LastName = Common.SafeString(row("LastName"))
						item.LastModifiedBy.Email = Common.SafeString(row("Email"))
						item.LastModifiedBy.Avatar = Common.SafeString(row("Avatar"))
					End If
					item.Note = Common.SafeString(row("note"))
					item.LenderConfigID = Common.SafeGUID(row("LenderConfigID"))
					item.RevisionID = Common.SafeInteger(row("RevisionID"))
					item.Comment = Common.SafeString(row("Comment"))
					result.Add(item)
				Next
			End If
			paging.TotalRecord = result.Count
			If paging.PageIndex > 0 AndAlso paging.PageSize > 0 Then
				Dim originalResult = result
				result = originalResult.Skip((paging.PageIndex - 1) * paging.PageSize).Take(paging.PageSize).ToList()
				If Not result.Any() Then
					paging.PageIndex = paging.PageIndex - 1
					result = originalResult.Skip((paging.PageIndex - 1) * paging.PageSize).Take(paging.PageSize).ToList()
				End If
			End If
		Catch ex As Exception
			_log.Error("Error while GetLenderConfigLogs", ex)
		Finally
			oDB.closeConnection()
		End Try
		Return result
	End Function

	Public Function GetLenderConfigLogItemByID(lenderConfigLogID As Guid) As SmLenderConfigLogItem
		Dim oData As New DataTable()
		Dim sSQL As String = String.Format("select * from {0} L LEFT JOIN {1} U ON L.LastModifiedByUserID = U.UserID ", LENDERCONFIG_LOGS_TABLE, USERS_TABLE)
		Dim oWhere As New CSQLWhereStringBuilder()
		oWhere.AppendAndCondition("LenderConfigLogID={0}", New CSQLParamValue(lenderConfigLogID))
		Dim oDB As New CSQLDBUtils(LPQMOBILE_CONNECTIONSTRING)
		Try
			oData = oDB.getDataTable(sSQL & oWhere.SQL)
			If oData IsNot Nothing AndAlso oData.Rows.Count > 0 Then
				Dim row As DataRow = oData.Rows(0)
				Dim result As New SmLenderConfigLogItem()
				result.LenderConfigLogID = Common.SafeGUID(row("LenderConfigLogID"))
				result.OrganizationID = Common.SafeGUID(row("OrganizationID"))
				result.LenderID = Common.SafeGUID(row("LenderID"))
				result.LenderRef = Common.SafeString(row("LenderRef"))
				result.ReturnURL = Common.SafeString(row("ReturnURL"))
				result.LPQLogin = Common.SafeString(row("LPQLogin"))
				result.LPQPW = Common.DecryptPassword(Common.SafeString(row("LPQPW")))
				result.LPQURL = Common.SafeString(row("LPQURL"))
				'Don't need to deryp pw: 1) Revision>Show Change - pw need to be masked, 2)PW doen't need to be encrypted on save
				result.LPQPW = Common.SafeString(row("LPQPW"))
				result.ConfigData = CStringCompressor.Decompress(Common.SafeString(row("ConfigData")))
				result.LogTime = Common.SafeDate(row("LogTime"))
				result.RemoteIP = Common.SafeString(row("RemoteIP"))
				result.LastModifiedByUserID = Common.SafeGUID(row("LastModifiedByUserID"))
				If result.LastModifiedByUserID <> Guid.Empty Then
					result.LastModifiedBy = New SmUser()
					result.LastModifiedBy.UserID = result.LastModifiedByUserID
					result.LastModifiedBy.FirstName = Common.SafeString(row("FirstName"))
					result.LastModifiedBy.LastName = Common.SafeString(row("LastName"))
					result.LastModifiedBy.Email = Common.SafeString(row("Email"))
					result.LastModifiedBy.Avatar = Common.SafeString(row("Avatar"))
				End If
				result.Note = Common.SafeString(row("note"))
				result.LenderConfigID = Common.SafeGUID(row("LenderConfigID"))
				result.RevisionID = Common.SafeInteger(row("RevisionID"))
				result.RemoteIP = Common.SafeString(row("RemoteIP"))
				Return result
			End If
		Catch ex As Exception
			_log.Error("Error while GetLenderConfigLogItemByID", ex)
		Finally
			oDB.closeConnection()
		End Try
		Return Nothing
	End Function

	Public Function GetLenderConfigLogItemByRevisionID(lenderConfigID As Guid, revisionID As Integer) As SmLenderConfigLogItem
		Dim oData As New DataTable()
		Dim sSQL As String = String.Format("select * from {0}", LENDERCONFIG_LOGS_TABLE)
		Dim oWhere As New CSQLWhereStringBuilder()
		oWhere.AppendAndCondition("LenderConfigID={0}", New CSQLParamValue(lenderConfigID))
		oWhere.AppendAndCondition("RevisionID={0}", New CSQLParamValue(revisionID))
		Dim oDB As New CSQLDBUtils(LPQMOBILE_CONNECTIONSTRING)
		Try
			oData = oDB.getDataTable(sSQL & oWhere.SQL)
			If oData IsNot Nothing AndAlso oData.Rows.Count > 0 Then
				Dim row As DataRow = oData.Rows(0)
				Dim result As New SmLenderConfigLogItem()
				result.LenderConfigLogID = Common.SafeGUID(row("LenderConfigLogID"))
				result.OrganizationID = Common.SafeGUID(row("OrganizationID"))
				result.LenderID = Common.SafeGUID(row("LenderID"))
				result.LenderRef = Common.SafeString(row("LenderRef"))
				result.ReturnURL = Common.SafeString(row("ReturnURL"))
				result.LPQLogin = Common.SafeString(row("LPQLogin"))
				result.LPQURL = Common.SafeString(row("LPQURL"))

				'Don't need to decryp pw
				result.LPQPW = Common.SafeString(row("LPQPW"))
				result.ConfigData = CStringCompressor.Decompress(Common.SafeString(row("ConfigData")))

				result.LogTime = Common.SafeDate(row("LogTime"))
				result.RemoteIP = Common.SafeString(row("RemoteIP"))
				result.LastModifiedByUserID = Common.SafeGUID(row("LastModifiedByUserID"))
				result.Note = Common.SafeString(row("note"))
				result.LenderConfigID = Common.SafeGUID(row("LenderConfigID"))
				result.RevisionID = Common.SafeInteger(row("RevisionID"))
				result.RemoteIP = Common.SafeString(row("RemoteIP"))
				Return result
			End If
		Catch ex As Exception
			_log.Error("Error while GetLenderConfigLogItemByRevisionID", ex)
		Finally
			oDB.closeConnection()
		End Try
		Return Nothing
	End Function

	Public Function RevertToRevision(lenderConfigLogID As Guid, comment As String, userId As Guid, userName As String, remoteIP As String) As String
		Dim result As String = ""
		Dim oDB As New CSQLDBUtils(LPQMOBILE_CONNECTIONSTRING)
		Try
			Dim lenderConfigLogItem = GetLenderConfigLogItemByID(lenderConfigLogID)
			If lenderConfigLogItem IsNot Nothing Then
				Dim liveConfig = GetLiveConfig(lenderConfigLogItem.LenderConfigID)
				''update live config data
				Dim oWhere As New CSQLWhereStringBuilder()
				oWhere.AppendAndCondition("LenderConfigID={0}", New CSQLParamValue(lenderConfigLogItem.LenderConfigID))
				Dim oUpdate As New cSQLUpdateStringBuilder(LENDERCONFIG_TABLE, oWhere)
				Dim configStr = Common.EncryptAllPasswordInConfigXml(lenderConfigLogItem.ConfigData).OuterXml
				oUpdate.appendvalue("OrganizationID", New CSQLParamValue(lenderConfigLogItem.OrganizationID))
				oUpdate.appendvalue("LenderID", New CSQLParamValue(lenderConfigLogItem.LenderID))
				oUpdate.appendvalue("LenderRef", New CSQLParamValue(lenderConfigLogItem.LenderRef))
				oUpdate.appendvalue("ReturnURL", New CSQLParamValue(lenderConfigLogItem.ReturnURL))
				oUpdate.appendvalue("LPQLogin", New CSQLParamValue(lenderConfigLogItem.LPQLogin))
				oUpdate.appendvalue("LPQPW", New CSQLParamValue(Common.EncryptPassword(lenderConfigLogItem.LPQPW)))
				oUpdate.appendvalue("LPQURL", New CSQLParamValue(lenderConfigLogItem.LPQURL))
				oUpdate.appendvalue("ConfigData", New CSQLParamValue(configStr))
				oUpdate.appendvalue("note", New CSQLParamValue(lenderConfigLogItem.Note))
				oUpdate.appendvalue("LastModifiedDate", New CSQLParamValue(Now))
				oUpdate.appendvalue("LastModifiedByUserID", New CSQLParamValue(userId))
				oUpdate.appendvalue("RevisionID", New CSQLParamValue(liveConfig.RevisionID + 1))
				oUpdate.appendvalue("IterationID", New CSQLParamValue(0))
				oDB.executeNonQuery(oUpdate.SQL, True)
				SmBL.InvalidateExternallyCachedPortalConfigs(lenderConfigLogItem.LenderRef, configStr)


				'insert log
				Dim oInsert As New cSQLInsertStringBuilder(LENDERCONFIG_LOGS_TABLE)
				oInsert.AppendValue("LenderConfigLogID", New CSQLParamValue(System.Guid.NewGuid))
				oInsert.AppendValue("OrganizationID", New CSQLParamValue(lenderConfigLogItem.OrganizationID))
				oInsert.AppendValue("LenderID", New CSQLParamValue(Common.SafeGUID(lenderConfigLogItem.LenderID)))
				oInsert.AppendValue("LenderRef", New CSQLParamValue(lenderConfigLogItem.LenderRef))
				oInsert.AppendValue("ReturnURL", New CSQLParamValue(lenderConfigLogItem.ReturnURL))
				oInsert.AppendValue("LPQLogin", New CSQLParamValue(lenderConfigLogItem.LPQLogin))
				oInsert.AppendValue("LPQPW", New CSQLParamValue(Common.EncryptPassword(lenderConfigLogItem.LPQPW)))
				oInsert.AppendValue("LPQURL", New CSQLParamValue(lenderConfigLogItem.LPQURL))
				oInsert.AppendValue("ConfigData", New CSQLParamValue(CStringCompressor.Compress(Common.EncryptAllPasswordInConfigXml(lenderConfigLogItem.ConfigData).OuterXml)))
				oInsert.AppendValue("LogTime", New CSQLParamValue(Now))
				oInsert.AppendValue("RemoteIP", New CSQLParamValue(remoteIP))
				oInsert.AppendValue("LastModifiedByUserID", New CSQLParamValue(userId))
				oInsert.AppendValue("note", New CSQLParamValue(""))
				oInsert.AppendValue("LenderConfigID", New CSQLParamValue(lenderConfigLogItem.LenderConfigID))
				oInsert.AppendValue("Comment", New CSQLParamValue(String.Format("Revert to revision #{0} with comment: {1}", lenderConfigLogItem.RevisionID, comment)))
                oInsert.AppendValue("RevisionID", New CSQLParamValue(liveConfig.RevisionID + 1))
                oInsert.AppendValue("CachedDiff", New CSQLParamValue(getCachedDiffData(liveConfig.ConfigData, configStr)))
                oDB.executeNonQuery(oInsert.SQL, True)
				Dim actionLogBL As New SmActionLogBL()
				actionLogBL.WriteActionLog(New SmActionLog() With {.Action = SmActionLogAction.REVERT_TO_REVISION, .Category = SmActionLogCategory.CONFIGURATION, .CreatedByUserID = userId, .IpAddress = remoteIP, .ObjectID = liveConfig.ID.ToString(), .ActionNote = String.Format("Revert the settings of live site to revision {0}", lenderConfigLogItem.RevisionID)})
				oDB.commitTransactionIfOpen()
			End If
			result = "ok"
		Catch ex As Exception
			_log.Error("Error while RevertToRevision", ex)
			result = "error"
			oDB.rollbackTransactionIfOpen()
		Finally
			oDB.closeConnection()
		End Try
		Return result
	End Function

	Public Function GetActionLogs(ByRef paging As SmPagingInfoModel, filter As SmActionLogListFilter) As List(Of SmActionLogView)
		Dim result As New List(Of SmActionLogView)
		Dim oData As New DataTable()
		Dim sSQL As String = String.Format("select TransactionDate, IPAddress, Action, ActionNote, Category, COALESCE(CONCAT(U.FirstName, ' ', U.LastName), '') AS ActionBy from {0} AL left join {1} U on AL.CreatedByUserID = U.UserID ", ACTIONLOGS_TABLE, USERS_TABLE)
		Dim oWhere As New CSQLWhereStringBuilder()
		Dim oOrder As String = " "
		If Not String.IsNullOrEmpty(paging.SortColumn) Then
			oOrder = oOrder & "ORDER BY " & paging.SortColumn & " " & paging.SortDirection
		Else
			oOrder = oOrder & "ORDER BY TransactionDate DESC"
		End If
        If filter IsNot Nothing Then

            If filter.FromDate.HasValue Then
                oWhere.AppendAndCondition("TransactionDate >= {0}", New CSQLParamValue(filter.FromDate.Value.ToString("yyyy-MM-dd HH:mm:ss")))
            End If
            If filter.ToDate.HasValue Then
                oWhere.AppendAndCondition("TransactionDate <= {0}", New CSQLParamValue(filter.ToDate.Value.AddDays(1).AddSeconds(-1).ToString("yyyy-MM-dd HH:mm:ss")))
            End If
            If Not String.IsNullOrWhiteSpace(filter.IPAddress) Then
                oWhere.AppendAndCondition("IPAddress LIKE {0}", New CSQLParamValue("%" & filter.IPAddress & "%"))
            End If
            If Not String.IsNullOrWhiteSpace(filter.Action) Then
                oWhere.AppendAndCondition("Action = {0}", New CSQLParamValue(filter.Action))
            End If
            If Not String.IsNullOrWhiteSpace(filter.Category) Then
                oWhere.AppendAndCondition("Category = {0}", New CSQLParamValue(filter.Category))
            End If

            If filter.Keyword.StartsWith("@action:") Then
                oWhere.AppendAndCondition("Action LIKE {0}", New CSQLParamValue("%" & filter.Keyword.Replace("@action:", "").TrimStart(" "c) & "%"))
            ElseIf filter.Keyword.StartsWith("@note:") Then
                oWhere.AppendAndCondition("ActionNote LIKE {0}", New CSQLParamValue("%" & filter.Keyword.Replace("@note:", "").TrimStart(" "c) & "%"))
            ElseIf filter.Keyword.StartsWith("@by:") Then
                oWhere.AppendAndCondition("COALESCE(CONCAT(U.FirstName, ' ', U.LastName), '') LIKE {0}", New CSQLParamValue("%" & filter.Keyword.Replace("@by:", "").TrimStart(" "c) & "%"))
            ElseIf filter.Keyword.StartsWith("@category:") Then
                oWhere.AppendAndCondition("Category LIKE {0}", New CSQLParamValue("%" & filter.Keyword.Replace("@category:", "").TrimStart(" "c) & "%"))
            ElseIf Not String.IsNullOrWhiteSpace(filter.Keyword) Then
                oWhere.AppendAndCondition("(COALESCE(Action, '') + '$' + COALESCE(ActionNote, '') + '$' + COALESCE(CONCAT(U.FirstName, ' ', U.LastName), '') + '$' + COALESCE(Category, '')) LIKE {0}", New CSQLParamValue("%" & filter.Keyword & "%"))
            End If

            ''this is to reduce db load when there is no filter
            If Not filter.FromDate.HasValue AndAlso Not filter.ToDate.HasValue AndAlso String.IsNullOrWhiteSpace(filter.IPAddress) AndAlso String.IsNullOrWhiteSpace(filter.Action) AndAlso String.IsNullOrWhiteSpace(filter.Category) AndAlso String.IsNullOrWhiteSpace(filter.Keyword) Then
                oWhere.AppendAndCondition("TransactionDate >= {0}", New CSQLParamValue(Now.AddMonths(-1).ToString("yyyy-MM-dd HH:mm:ss")))
            End If
        End If

            Dim oDB As New CSQLDBUtils(LPQMOBILE_CONNECTIONSTRING)
		Try
			oData = oDB.getDataTable(sSQL & oWhere.SQL & oOrder)
			If oData IsNot Nothing AndAlso oData.Rows.Count > 0 Then
				For Each row As DataRow In oData.Rows
					Dim item As New SmActionLogView()
					item.IpAddress = Common.SafeString(row("IPAddress"))
					item.ActionBy = Common.SafeString(row("ActionBy"))
					item.Action = Common.SafeString(row("Action"))
					item.Category = Common.SafeString(row("Category"))
					item.ActionNote = Common.SafeString(row("ActionNote"))
					item.TransactionDate = Common.SafeDate(row("TransactionDate"))
					result.Add(item)
				Next
			End If
			paging.TotalRecord = result.Count
			If paging.PageIndex > 0 AndAlso paging.PageSize > 0 Then
				Dim originalResult = result
				result = originalResult.Skip((paging.PageIndex - 1) * paging.PageSize).Take(paging.PageSize).ToList()
				If Not result.Any() Then
					paging.PageIndex = paging.PageIndex - 1
					result = originalResult.Skip((paging.PageIndex - 1) * paging.PageSize).Take(paging.PageSize).ToList()
				End If
			End If
		Catch ex As Exception
			_log.Error("Error while GetActionLogs", ex)
		Finally
			oDB.closeConnection()
		End Try
		Return result
	End Function


	Public Function GetLenderVendors(ByRef paging As SmPagingInfoModel, filter As SmVendorListFilter) As List(Of SmLenderVendor)
		Dim result As New List(Of SmLenderVendor)
		Dim oData As New DataTable()
		Dim sSQL As String = String.Format("SELECT V.* FROM {0} V ", LENDERVENDORS_TABLE)
		Dim oWhere As New CSQLWhereStringBuilder()
		Dim oOrder As String = " "
		If Not String.IsNullOrEmpty(paging.SortColumn) Then
			oOrder = oOrder & "ORDER BY " & paging.SortColumn & " " & paging.SortDirection
		Else
			oOrder = oOrder & "ORDER BY VendorName"
		End If
		If filter IsNot Nothing Then
			If Not String.IsNullOrEmpty(filter.Keyword) Then
				If filter.Keyword.StartsWith("@") Then
					_log.Debug(filter.Keyword)
					oWhere.AppendAndCondition("',' + COALESCE(Tags, '') LIKE {0}", New CSQLParamValue("%" & filter.Keyword.Replace("@", ",") & "%"))
				Else
					oWhere.AppendAndCondition("(COALESCE(VendorName, '') + '$' + COALESCE(City, '') + '$' + COALESCE(Phone, '') + '$' + COALESCE(Fax, '') + '$' + COALESCE(Zip, '') + '$' + COALESCE(DefaultLpqWorkerID, '') + '$' + COALESCE(DealerNumberLpq, '') + '$' + COALESCE(Type, '') + '$' + COALESCE(Address, '')) LIKE {0}", New CSQLParamValue("%" & filter.Keyword & "%"))
				End If

			End If
			If filter.LenderID <> Guid.Empty Then
				oWhere.AppendAndCondition("LenderID={0}", New CSQLParamValue(filter.LenderID))
			End If
			If filter.Status IsNot Nothing AndAlso filter.Status.Any() Then
				oWhere.AppendAndIn("Status", (From status As SmSettings.Role In filter.Status Select New CSQLParamValue(status)).ToList())
			End If

			If filter.VendorIDList IsNot Nothing AndAlso filter.VendorIDList.Any() Then
				oWhere.AppendAndIn("VendorID", (From vendorID As String In filter.VendorIDList Select New CSQLParamValue(vendorID)).ToList())
			End If
		End If
		Dim oDB As New CSQLDBUtils(LPQMOBILE_CONNECTIONSTRING)
		Try
			oData = oDB.getDataTable(sSQL & oWhere.SQL & oOrder)
			If oData IsNot Nothing AndAlso oData.Rows.Count > 0 Then
				For Each row As DataRow In oData.Rows
					Dim item As New SmLenderVendor()
					item.Tags = Common.SafeString(row("Tags"))
					'Vendors with empty tags can be seen by everyone
					If Not String.IsNullOrWhiteSpace(item.Tags) Then
						'vendor group admin can see those vendors that belongs to groups(tags) that they are assigned to
						If filter.FilterByTags AndAlso Not filter.TagList.Any(Function(x) item.TagList.Contains(x)) Then
							Continue For
						End If
					End If
					item.LenderVendorID = Common.SafeGUID(row("LenderVendorID"))
					item.VendorID = Common.SafeString(row("VendorID"))
					item.LenderID = Common.SafeGUID(row("LenderID"))
					item.VendorName = Common.SafeString(row("VendorName"))
					item.Address = Common.SafeString(row("Address"))
					item.City = Common.SafeString(row("City"))
					item.State = Common.SafeString(row("State"))
					item.Zip = Common.SafeString(row("Zip"))
					item.Phone = Common.SafeString(row("Phone"))
					item.Fax = Common.SafeString(row("Fax"))
					item.DefaultLpqWorkerID = Common.SafeString(row("DefaultLpqWorkerID"))
					item.DealerNumberLpq = Common.SafeString(row("DealerNumberLpq"))
					item.Tags = Common.SafeString(row("Tags"))
					item.Status = DirectCast(Common.SafeInteger(row("Status")), SmSettings.LenderVendorStatus)
					item.Logo = Common.SafeString(row("Logo"))
					'item.Type = CType([Enum].Parse(GetType(SmSettings.LenderVendorType), Common.SafeString(row("Type"))), SmSettings.LenderVendorType)
					If Not String.IsNullOrEmpty(Common.SafeString(row("Type"))) Then
						item.Type = Common.SafeString(row("Type")).Split(New Char() {","c}).Select(Function(s) s.Trim()).ToList()
					End If
					result.Add(item)
				Next
			End If
			paging.TotalRecord = result.Count
			If paging.PageIndex > 0 AndAlso paging.PageSize > 0 Then
				Dim originalResult = result
				result = originalResult.Skip((paging.PageIndex - 1) * paging.PageSize).Take(paging.PageSize).ToList()
				If Not result.Any() Then
					paging.PageIndex = paging.PageIndex - 1
					result = originalResult.Skip((paging.PageIndex - 1) * paging.PageSize).Take(paging.PageSize).ToList()
				End If
			End If
		Catch ex As Exception
			_log.Error("Error while GetLenderVendors", ex)
		Finally
			oDB.closeConnection()
		End Try
		Return result
	End Function

	'Public Function GetAllLenderVendors(ByRef paging As SmPagingInfoModel, filter As SmVendorListFilter) As List(Of SmLenderVendor)
	'	Dim result As New List(Of SmLenderVendor)
	'	Dim oData As New DataTable()
	'	Dim sSQL As String = "SELECT * FROM " & LENDERVENDORS_TABLE
	'	Dim oWhere As New CSQLWhereStringBuilder()
	'	If filter IsNot Nothing Then
	'		If filter.LenderID <> Guid.Empty Then
	'			oWhere.AppendAndCondition("LenderID={0}", New CSQLParamValue(filter.LenderID))
	'		End If
	'		If Not String.IsNullOrEmpty(filter.Keyword) Then
	'			oWhere.AppendAndCondition("(VendorName + '$' + VendorID + '$' + City + '$' + Zip + '$' + State) LIKE {0}", New CSQLParamValue("%" & filter.Keyword & "%"))
	'		End If
	'		If filter.Status IsNot Nothing AndAlso filter.Status.Any() Then
	'			oWhere.AppendAndIn("Status", (From status As SmSettings.Role In filter.Status Select New CSQLParamValue(status)).ToList())
	'		End If

	'		If filter.VendorIDList IsNot Nothing AndAlso filter.VendorIDList.Any() Then
	'			oWhere.AppendAndIn("VendorID", (From vendorID As String In filter.VendorIDList Select New CSQLParamValue(vendorID)).ToList())
	'		End If
	'	End If

	'	Dim oOrder As String = " "
	'	If Not String.IsNullOrEmpty(paging.SortColumn) Then
	'		oOrder = oOrder & "ORDER BY " & paging.SortColumn & " " & paging.SortDirection
	'	Else
	'		oOrder = oOrder & "ORDER BY VendorName"
	'	End If
	'	Dim oDB As New CSQLDBUtils(LPQMOBILE_CONNECTIONSTRING)
	'	Try
	'		oData = oDB.getDataTable(sSQL & oWhere.SQL & oOrder)
	'		If oData IsNot Nothing AndAlso oData.Rows.Count > 0 Then
	'			For Each row As DataRow In oData.Rows
	'				Dim item As New SmLenderVendor()
	'				item.LenderVendorID = Common.SafeGUID(row("LenderVendorID"))
	'				item.VendorID = Common.SafeString(row("VendorID"))
	'				item.LenderID = Common.SafeGUID(row("LenderID"))
	'				item.VendorName = Common.SafeString(row("VendorName"))
	'				item.City = Common.SafeString(row("City"))
	'				item.State = Common.SafeString(row("State"))
	'				item.Zip = Common.SafeString(row("Zip"))
	'				item.Phone = Common.SafeString(row("Phone"))
	'				item.Fax = Common.SafeString(row("Fax"))
	'				item.DefaultLpqWorkerID = Common.SafeString(row("DefaultLpqWorkerID"))
	'				item.DealerNumberLpq = Common.SafeString(row("DealerNumberLpq"))
	'				item.Status = DirectCast(Common.SafeInteger(row("Status")), SmSettings.LenderVendorStatus)
	'				result.Add(item)
	'			Next
	'		End If
	'		paging.TotalRecord = result.Count
	'		If paging.PageIndex > 0 AndAlso paging.PageSize > 0 Then
	'			result = result.Skip((paging.PageIndex - 1) * paging.PageSize).Take(paging.PageSize).ToList()
	'		End If
	'	Catch ex As Exception
	'		_log.Error("Error while GetAllLenderVendors", ex)
	'	Finally
	'		oDB.closeConnection()
	'	End Try
	'	Return result
	'End Function

	Public Function GetLenderVendorByName(lenderID As Guid, vendorName As String) As SmLenderVendor
		Dim result As SmLenderVendor = Nothing
		Dim oData As DataTable
		Dim sSQL As String = "SELECT * FROM " & LENDERVENDORS_TABLE
		Dim oWhere As New CSQLWhereStringBuilder()
		oWhere.AppendAndCondition("UPPER(VendorName)=UPPER({0})", New CSQLParamValue(vendorName))
		oWhere.AppendAndCondition("LenderID={0}", New CSQLParamValue(lenderID))
		Dim oDB As New CSQLDBUtils(LPQMOBILE_CONNECTIONSTRING)
		Try
			oData = oDB.getDataTable(sSQL & oWhere.SQL)
			If oData IsNot Nothing AndAlso oData.Rows.Count > 0 Then
				Dim row As DataRow = oData.Rows(0)
				result = New SmLenderVendor()
				result.LenderVendorID = Common.SafeGUID(row("LenderVendorID"))
				result.VendorID = Common.SafeString(row("VendorID"))
				result.LenderID = Common.SafeGUID(row("LenderID"))
				result.VendorName = Common.SafeString(row("VendorName"))
				result.Address = Common.SafeString(row("Address"))
				result.City = Common.SafeString(row("City"))
				result.State = Common.SafeString(row("State"))
				result.Zip = Common.SafeString(row("Zip"))
				result.Phone = Common.SafeString(row("Phone"))
				result.Fax = Common.SafeString(row("Fax"))
				result.DefaultLpqWorkerID = Common.SafeString(row("DefaultLpqWorkerID"))
				result.DealerNumberLpq = Common.SafeString(row("DealerNumberLpq"))
				result.Status = DirectCast(Common.SafeInteger(row("Status")), SmSettings.LenderVendorStatus)
				result.Tags = Common.SafeString(row("Tags"))
				result.Logo = Common.SafeString(row("Logo"))
				'result.Type = CType([Enum].Parse(GetType(SmSettings.LenderVendorType), Common.SafeString(row("Type"))), SmSettings.LenderVendorType)
				If Not String.IsNullOrEmpty(Common.SafeString(row("Type"))) Then
					result.Type = Common.SafeString(row("Type")).Split(New Char() {","c}).Select(Function(s) s.Trim()).ToList()
				End If

			End If
		Catch ex As Exception
			_log.Error("Error while GetLenderVendor", ex)
		Finally
			oDB.closeConnection()
		End Try
		Return result
	End Function

	Public Function GetLenderVendorByLenderVendorID(lenderVendorID As Guid) As SmLenderVendor
		Dim result As SmLenderVendor = Nothing
		Dim oData As DataTable
		Dim sSQL As String = "SELECT * FROM " & LENDERVENDORS_TABLE
		Dim oWhere As New CSQLWhereStringBuilder()
		oWhere.AppendAndCondition("LenderVendorID={0}", New CSQLParamValue(lenderVendorID))
		Dim oDB As New CSQLDBUtils(LPQMOBILE_CONNECTIONSTRING)
		Try
			oData = oDB.getDataTable(sSQL & oWhere.SQL)
			If oData IsNot Nothing AndAlso oData.Rows.Count > 0 Then
				Dim row As DataRow = oData.Rows(0)
				result = New SmLenderVendor()
				result.LenderVendorID = Common.SafeGUID(row("LenderVendorID"))
				result.VendorID = Common.SafeString(row("VendorID"))
				result.LenderID = Common.SafeGUID(row("LenderID"))
				result.VendorName = Common.SafeString(row("VendorName"))
				result.Address = Common.SafeString(row("Address"))
				result.City = Common.SafeString(row("City"))
				result.State = Common.SafeString(row("State"))
				result.Zip = Common.SafeString(row("Zip"))
				result.Phone = Common.SafeString(row("Phone"))
				result.Fax = Common.SafeString(row("Fax"))
				result.Tags = Common.SafeString(row("Tags"))
				result.Logo = Common.SafeString(row("Logo"))
				result.DefaultLpqWorkerID = Common.SafeString(row("DefaultLpqWorkerID"))
				result.DealerNumberLpq = Common.SafeString(row("DealerNumberLpq"))
				result.Status = DirectCast(Common.SafeInteger(row("Status")), SmSettings.LenderVendorStatus)
				'result.Type = CType([Enum].Parse(GetType(SmSettings.LenderVendorType), Common.SafeString(row("Type"))), SmSettings.LenderVendorType)
				If Not String.IsNullOrEmpty(Common.SafeString(row("Type"))) Then
					result.Type = Common.SafeString(row("Type")).Split(New Char() {","c}).Select(Function(s) s.Trim()).ToList()
				End If

			End If
		Catch ex As Exception
			_log.Error("Error while GetLenderVendorByLenderVendorID", ex)
		Finally
			oDB.closeConnection()
		End Try
		Return result
	End Function

	Public Function GetLenderVendorByVendorID(vendorID As String) As SmLenderVendor
		Dim result As SmLenderVendor = Nothing
		Dim oData As DataTable
		Dim sSQL As String = "SELECT * FROM " & LENDERVENDORS_TABLE
		Dim oWhere As New CSQLWhereStringBuilder()
		oWhere.AppendAndCondition("VendorID={0}", New CSQLParamValue(vendorID))
		Dim oDB As New CSQLDBUtils(LPQMOBILE_CONNECTIONSTRING)
		Try
			oData = oDB.getDataTable(sSQL & oWhere.SQL)
			If oData IsNot Nothing AndAlso oData.Rows.Count > 0 Then
				Dim row As DataRow = oData.Rows(0)
				result = New SmLenderVendor()
				result.LenderVendorID = Common.SafeGUID(row("LenderVendorID"))
				result.VendorID = Common.SafeString(row("VendorID"))
				result.LenderID = Common.SafeGUID(row("LenderID"))
				result.VendorName = Common.SafeString(row("VendorName"))
				result.Address = Common.SafeString(row("Address"))
				result.City = Common.SafeString(row("City"))
				result.State = Common.SafeString(row("State"))
				result.Zip = Common.SafeString(row("Zip"))
				result.Phone = Common.SafeString(row("Phone"))
				result.Tags = Common.SafeString(row("Tags"))
				result.Fax = Common.SafeString(row("Fax"))
				result.DefaultLpqWorkerID = Common.SafeString(row("DefaultLpqWorkerID"))
				result.DealerNumberLpq = Common.SafeString(row("DealerNumberLpq"))
				result.Status = DirectCast(Common.SafeInteger(row("Status")), SmSettings.LenderVendorStatus)
				result.Logo = Common.SafeString(row("Logo"))
				'result.Type = CType([Enum].Parse(GetType(SmSettings.LenderVendorType), Common.SafeString(row("Type"))), SmSettings.LenderVendorType)
				If Not String.IsNullOrEmpty(Common.SafeString(row("Type"))) Then
					result.Type = Common.SafeString(row("Type")).Split(New Char() {","c}).Select(Function(s) s.Trim()).ToList()
				End If
			End If
		Catch ex As Exception
			_log.Error("Error while GetLenderVendorByVendorID", ex)
		Finally
			oDB.closeConnection()
		End Try
		Return result
	End Function

	Public Function AddLenderVendor(lenderVendor As SmLenderVendor, createdByUserId As Guid, ipAddress As String) As String
		Dim result As String = ""
		Dim oDB As New CSQLDBUtils(LPQMOBILE_CONNECTIONSTRING)
		Try
			Dim oInsert As New cSQLInsertStringBuilder(LENDERVENDORS_TABLE)
			oInsert.AppendValue("LenderVendorID", New CSQLParamValue(lenderVendor.LenderVendorID))
			oInsert.AppendValue("VendorID", New CSQLParamValue(Common.SafeString(lenderVendor.VendorID)))
			oInsert.AppendValue("LenderID", New CSQLParamValue(lenderVendor.LenderID))
			oInsert.AppendValue("VendorName", New CSQLParamValue(Common.SafeString(lenderVendor.VendorName)))
			oInsert.AppendValue("Address", New CSQLParamValue(Common.SafeString(lenderVendor.Address)))
			oInsert.AppendValue("City", New CSQLParamValue(Common.SafeString(lenderVendor.City)))
			oInsert.AppendValue("State", New CSQLParamValue(Common.SafeString(lenderVendor.State)))
			oInsert.AppendValue("Zip", New CSQLParamValue(Common.SafeString(lenderVendor.Zip)))
			oInsert.AppendValue("Phone", New CSQLParamValue(Common.SafeString(lenderVendor.Phone)))
			oInsert.AppendValue("Fax", New CSQLParamValue(Common.SafeString(lenderVendor.Fax)))
			oInsert.AppendValue("DefaultLpqWorkerID", New CSQLParamValue(Common.SafeString(lenderVendor.DefaultLpqWorkerID)))
			oInsert.AppendValue("DealerNumberLpq", New CSQLParamValue(Common.SafeString(lenderVendor.DealerNumberLpq)))
			oInsert.AppendValue("Status", New CSQLParamValue(SmSettings.LenderVendorStatus.Active))
			oInsert.AppendValue("Type", New CSQLParamValue(String.Join(",", lenderVendor.Type)))
			oInsert.AppendValue("Logo", New CSQLParamValue(lenderVendor.Logo))
			oInsert.AppendValue("Tags", New CSQLParamValue(lenderVendor.Tags))
			oDB.executeNonQuery(oInsert.SQL, False)
			Dim actionLogBL As New SmActionLogBL()
			actionLogBL.WriteActionLog(New SmActionLog() With {.Action = SmActionLogAction.ADD_LENDER_VENDOR, .Category = SmActionLogCategory.LENDERS_MANIPULATION, .CreatedByUserID = createdByUserId, .IpAddress = ipAddress, .ObjectID = lenderVendor.LenderVendorID.ToString(), .ActionNote = String.Format("Add new lender vendor with ID {0}", lenderVendor.VendorID)})
			result = "ok"
		Catch ex As Exception
			_log.Error("Error while AddLenderVendor", ex)
			result = "error"
		Finally
			oDB.closeConnection()
		End Try
		Return result
	End Function

	Public Function ChangeLenderVendorState(lenderVendor As SmLenderVendor, status As SmSettings.LenderVendorStatus, createdByUserId As Guid, ipAddress As String, comment As String) As String
		Dim result As String = ""
		Dim oDB As New CSQLDBUtils(LPQMOBILE_CONNECTIONSTRING)
		Try
			Dim oWhere As New CSQLWhereStringBuilder()
			Dim actionLogBL As New SmActionLogBL()
			oWhere.AppendAndCondition("LenderVendorID={0}", New CSQLParamValue(lenderVendor.LenderVendorID))
			Dim oUpdate As New cSQLUpdateStringBuilder(LENDERVENDORS_TABLE, oWhere)
			oUpdate.appendvalue("Status", New CSQLParamValue(status))
			oDB.executeNonQuery(oUpdate.SQL, False)
			actionLogBL.WriteActionLog(New SmActionLog() With {.Action = SmActionLogAction.CHANGE_LENDER_VENDOR_STATE, .Category = SmActionLogCategory.LENDERS_MANIPULATION, .CreatedByUserID = createdByUserId, .IpAddress = ipAddress, .ObjectID = lenderVendor.LenderVendorID.ToString(), .ActionNote = String.Format("Change status of lender vendor {0} to {1} with comment: ""{2}""", lenderVendor.VendorName, status.GetEnumDescription(), comment)})
			result = "ok"
		Catch ex As Exception
			_log.Error("Error while ChangeLenderVendorState", ex)
			result = "error"
		Finally
			oDB.closeConnection()
		End Try
		Return result
	End Function

	Public Function UpdateLenderVendor(lenderVendor As SmLenderVendor, createdByUserId As Guid, ipAddress As String) As String
		Dim result As String = ""
		Dim oDB As New CSQLDBUtils(LPQMOBILE_CONNECTIONSTRING)
		Try
			Dim oWhere As New CSQLWhereStringBuilder()
			oWhere.AppendAndCondition("LenderVendorID={0}", New CSQLParamValue(lenderVendor.LenderVendorID))

			Dim oUpdate As New cSQLUpdateStringBuilder(LENDERVENDORS_TABLE, oWhere)
			oUpdate.appendvalue("VendorID", New CSQLParamValue(Common.SafeString(lenderVendor.VendorID)))
			oUpdate.appendvalue("LenderID", New CSQLParamValue(lenderVendor.LenderID))
			oUpdate.appendvalue("VendorName", New CSQLParamValue(Common.SafeString(lenderVendor.VendorName)))
			oUpdate.appendvalue("Address", New CSQLParamValue(Common.SafeString(lenderVendor.Address)))
			oUpdate.appendvalue("City", New CSQLParamValue(Common.SafeString(lenderVendor.City)))
			oUpdate.appendvalue("State", New CSQLParamValue(Common.SafeString(lenderVendor.State)))
			oUpdate.appendvalue("Zip", New CSQLParamValue(Common.SafeString(lenderVendor.Zip)))
			oUpdate.appendvalue("Phone", New CSQLParamValue(Common.SafeString(lenderVendor.Phone)))
			oUpdate.appendvalue("Fax", New CSQLParamValue(Common.SafeString(lenderVendor.Fax)))
			oUpdate.appendvalue("DefaultLpqWorkerID", New CSQLParamValue(Common.SafeString(lenderVendor.DefaultLpqWorkerID)))
			oUpdate.appendvalue("DealerNumberLpq", New CSQLParamValue(Common.SafeString(lenderVendor.DealerNumberLpq)))
			oUpdate.appendvalue("Status", New CSQLParamValue(SmSettings.LenderVendorStatus.Active))
			oUpdate.appendvalue("Logo", New CSQLParamValue(Common.SafeString(lenderVendor.Logo)))
			oUpdate.appendvalue("Type", New CSQLParamValue(String.Join(",", lenderVendor.Type)))
			oUpdate.appendvalue("Tags", New CSQLParamValue(Common.SafeString(lenderVendor.Tags)))
			oDB.executeNonQuery(oUpdate.SQL, False)

			Dim actionLogBL As New SmActionLogBL()
			actionLogBL.WriteActionLog(New SmActionLog() With {.Action = SmActionLogAction.EDIT_LENDER_VENDOR, .Category = SmActionLogCategory.LENDERS_MANIPULATION, .CreatedByUserID = createdByUserId, .IpAddress = ipAddress, .ObjectID = lenderVendor.LenderVendorID.ToString(), .ActionNote = String.Format("Edit info of vendor {0}", lenderVendor.VendorID)})
			result = "ok"
		Catch ex As Exception
			_log.Error("Error while UpdateLenderVendor", ex)
			result = "error"
		Finally
			oDB.closeConnection()
		End Try
		Return result
	End Function

	Public Function GetLenderUserRoleList(userID As Guid, lenderID As Guid) As List(Of SmUserRole)
		Dim result As List(Of SmUserRole)
		Dim oData As New DataTable()
		Dim sSQL As String = String.Format("SELECT * FROM {0} UR", USERROLES_TABLE)
		Dim oWhere As New CSQLWhereStringBuilder()
		oWhere.AppendAndCondition("UserID={0}", New CSQLParamValue(userID))
		oWhere.AppendAndCondition("LenderID={0}", New CSQLParamValue(lenderID))
		Dim oDB As New CSQLDBUtils(LPQMOBILE_CONNECTIONSTRING)
		Try
			oData = oDB.getDataTable(sSQL & oWhere.SQL)
			If oData IsNot Nothing AndAlso oData.Rows.Count > 0 Then
				result = New List(Of SmUserRole)
				For Each row In oData.Rows
					Dim item As New SmUserRole
					item.UserRoleID = Common.SafeGUID(row("UserRoleID"))
					item.Role = CType([Enum].Parse(GetType(SmSettings.Role), Common.SafeString(row("Role"))), SmSettings.Role)
					item.LenderID = Common.SafeGUID(row("LenderID"))
					item.LenderConfigID = Common.SafeGUID(row("LenderConfigID"))
					item.VendorID = Common.SafeString(row("VendorID"))
					If Not String.IsNullOrEmpty(item.VendorID) Then
						item.Vendor = GetLenderVendorByVendorID(item.VendorID)
					End If
					item.UserID = Common.SafeGUID(row("UserID"))
					item.Status = DirectCast(Common.SafeInteger(row("Status")), SmSettings.UserRoleStatus)
					result.Add(item)
				Next
			Else
				result = Nothing
			End If
		Catch ex As Exception
			_log.Error("Error while GetLenderUserRoleList", ex)
		Finally
			oDB.closeConnection()
		End Try
		Return result
	End Function

	Public Function GetLenderUserRoleList(email As String, lenderID As Guid) As List(Of SmUserRole)
		Dim result As List(Of SmUserRole)
		Dim oData As New DataTable()
		Dim sSQL As String = String.Format("SELECT * FROM {0} UR", USERROLES_TABLE)
		Dim oWhere As New CSQLWhereStringBuilder()
		oWhere.AppendAndCondition("Email={0}", New CSQLParamValue(email))
		oWhere.AppendAndCondition("LenderID={0}", New CSQLParamValue(lenderID))
		Dim oDB As New CSQLDBUtils(LPQMOBILE_CONNECTIONSTRING)
		Try
			oData = oDB.getDataTable(sSQL & oWhere.SQL)
			If oData IsNot Nothing AndAlso oData.Rows.Count > 0 Then
				result = New List(Of SmUserRole)
				For Each row In oData.Rows
					Dim item As New SmUserRole
					item.UserRoleID = Common.SafeGUID(row("UserRoleID"))
					item.Role = CType([Enum].Parse(GetType(SmSettings.Role), Common.SafeString(row("Role"))), SmSettings.Role)
					item.LenderID = Common.SafeGUID(row("LenderID"))
					item.LenderConfigID = Common.SafeGUID(row("LenderConfigID"))
					item.AssignedVendorGroupByTags = Common.SafeString(row("AssignedVendorGroupByTags"))
					item.VendorID = Common.SafeString(row("VendorID"))
					If Not String.IsNullOrEmpty(item.VendorID) Then
						item.Vendor = GetLenderVendorByVendorID(item.VendorID)
					End If
					item.UserID = Common.SafeGUID(row("UserID"))
					item.Status = DirectCast(Common.SafeInteger(row("Status")), SmSettings.UserRoleStatus)
					result.Add(item)
				Next
			Else
				result = Nothing
			End If
		Catch ex As Exception
			_log.Error("Error while GetLenderUserRoleList", ex)
		Finally
			oDB.closeConnection()
		End Try
		Return result
	End Function

	Public Function GetActiveUserRolesByUserID(userID As Guid) As List(Of SmUserRole)
		Dim result As List(Of SmUserRole)
		Dim oData As New DataTable()
		Dim sSQL As String = String.Format("SELECT * FROM {0} UR", USERROLES_TABLE)
		Dim oWhere As New CSQLWhereStringBuilder()
		oWhere.AppendAndCondition("UserID={0}", New CSQLParamValue(userID))
		oWhere.AppendAndCondition("Status={0}", New CSQLParamValue(SmSettings.UserRoleStatus.Active))
		Dim oDB As New CSQLDBUtils(LPQMOBILE_CONNECTIONSTRING)
		Try
			oData = oDB.getDataTable(sSQL & oWhere.SQL)
			If oData IsNot Nothing AndAlso oData.Rows.Count > 0 Then
				result = New List(Of SmUserRole)
				For Each row In oData.Rows
					Dim item As New SmUserRole
					item.UserRoleID = Common.SafeGUID(row("UserRoleID"))
					item.Role = CType([Enum].Parse(GetType(SmSettings.Role), Common.SafeString(row("Role"))), SmSettings.Role)
					item.LenderID = Common.SafeGUID(row("LenderID"))
					item.LenderConfigID = Common.SafeGUID(row("LenderConfigID"))
					item.AssignedVendorGroupByTags = Common.SafeString(row("AssignedVendorGroupByTags"))
					item.VendorID = Common.SafeString(row("VendorID"))
					If Not String.IsNullOrEmpty(item.VendorID) Then
						item.Vendor = GetLenderVendorByVendorID(item.VendorID)
					End If
					item.UserID = Common.SafeGUID(row("UserID"))
					item.Status = DirectCast(Common.SafeInteger(row("Status")), SmSettings.UserRoleStatus)
					result.Add(item)
				Next
			Else
				result = Nothing
			End If
		Catch ex As Exception
			_log.Error("Error while GetActiveUserRolesByUserID", ex)
		Finally
			oDB.closeConnection()
		End Try
		Return result
	End Function

	Public Function GetUserRolesByUserID(userID As Guid) As List(Of SmUserRole)
		Dim result As List(Of SmUserRole)
		Dim oData As New DataTable()
		Dim sSQL As String = String.Format("SELECT * FROM {0} UR", USERROLES_TABLE)
		Dim oWhere As New CSQLWhereStringBuilder()
		oWhere.AppendAndCondition("UserID={0}", New CSQLParamValue(userID))
		oWhere.AppendAndCondition("Status<>{0}", New CSQLParamValue(SmSettings.UserRoleStatus.Deleted))
		Dim oDB As New CSQLDBUtils(LPQMOBILE_CONNECTIONSTRING)
		Try
			oData = oDB.getDataTable(sSQL & oWhere.SQL)
			If oData IsNot Nothing AndAlso oData.Rows.Count > 0 Then
				result = New List(Of SmUserRole)
				For Each row In oData.Rows
					Dim item As New SmUserRole
					item.UserRoleID = Common.SafeGUID(row("UserRoleID"))
					item.Role = CType([Enum].Parse(GetType(SmSettings.Role), Common.SafeString(row("Role"))), SmSettings.Role)
					item.LenderID = Common.SafeGUID(row("LenderID"))
					item.LenderConfigID = Common.SafeGUID(row("LenderConfigID"))
					item.AssignedVendorGroupByTags = Common.SafeString(row("AssignedVendorGroupByTags"))
					item.VendorID = Common.SafeString(row("VendorID"))
					If Not String.IsNullOrEmpty(item.VendorID) Then
						item.Vendor = GetLenderVendorByVendorID(item.VendorID)
					End If
					item.UserID = Common.SafeGUID(row("UserID"))
					item.Status = DirectCast(Common.SafeInteger(row("Status")), SmSettings.UserRoleStatus)
					result.Add(item)
				Next
			Else
				result = Nothing
			End If
		Catch ex As Exception
			_log.Error("Error while GetUserRolesByUserID", ex)
		Finally
			oDB.closeConnection()
		End Try
		Return result
	End Function

	'Public Function GetLenderUserRoleByUserID(userID As Guid, lenderID As Guid) As SmUserRole
	'	Dim result As SmUserRole
	'	Dim oData As New DataTable()
	'	Dim sSQL As String = String.Format("SELECT * FROM {0} UR", USERROLES_TABLE)
	'	Dim oWhere As New CSQLWhereStringBuilder()
	'	oWhere.AppendAndCondition("UserID={0}", New CSQLParamValue(userID))
	'	oWhere.AppendAndCondition("LenderID={0}", New CSQLParamValue(lenderID))
	'	Dim oDB As New CSQLDBUtils(LPQMOBILE_CONNECTIONSTRING)
	'	Try
	'		oData = oDB.getDataTable(sSQL & oWhere.SQL)
	'		If oData IsNot Nothing AndAlso oData.Rows.Count > 0 Then
	'			Dim row As DataRow = oData.Rows(0)
	'			result = New SmUserRole()
	'			result.UserRoleID = Common.SafeGUID(row("UserRoleID"))
	'			result.Role = CType([Enum].Parse(GetType(SmSettings.Role), Common.SafeString(row("Role"))), SmSettings.Role)
	'			result.LenderID = Common.SafeGUID(row("LenderID"))
	'			result.LenderConfigID = Common.SafeGUID(row("LenderConfigID"))
	'			result.VendorID = Common.SafeString(row("VendorID"))
	'			result.UserID = Common.SafeGUID(row("UserID"))
	'			result.Status = DirectCast(Common.SafeInteger(row("Status")), SmSettings.UserRoleStatus)
	'		Else
	'			result = Nothing
	'		End If
	'	Catch ex As Exception
	'		_log.Error("Error while GetLenderUserRoleByUserID", ex)
	'	Finally
	'		oDB.closeConnection()
	'	End Try
	'	Return result
	'End Function

	'Public Function GetPortalUserRoleByUserID(userID As Guid, lenderConfigID As Guid) As SmUserRole
	'	Dim result As SmUserRole
	'	Dim oData As New DataTable()
	'	Dim sSQL As String = String.Format("SELECT * FROM {0} UR", USERROLES_TABLE)
	'	Dim oWhere As New CSQLWhereStringBuilder()
	'	oWhere.AppendAndCondition("UserID={0}", New CSQLParamValue(userID))
	'	oWhere.AppendAndCondition("LenderConfigID={0}", New CSQLParamValue(lenderConfigID))
	'	Dim oDB As New CSQLDBUtils(LPQMOBILE_CONNECTIONSTRING)
	'	Try
	'		oData = oDB.getDataTable(sSQL & oWhere.SQL)
	'		If oData IsNot Nothing AndAlso oData.Rows.Count > 0 Then
	'			Dim row As DataRow = oData.Rows(0)
	'			result = New SmUserRole()
	'			result.UserRoleID = Common.SafeGUID(row("UserRoleID"))
	'			result.Role = CType([Enum].Parse(GetType(SmSettings.Role), Common.SafeString(row("Role"))), SmSettings.Role)
	'			result.LenderID = Common.SafeGUID(row("LenderID"))
	'			result.LenderConfigID = Common.SafeGUID(row("LenderConfigID"))
	'			result.VendorID = Common.SafeString(row("VendorID"))
	'			result.UserID = Common.SafeGUID(row("UserID"))
	'			result.Status = DirectCast(Common.SafeInteger(row("Status")), SmSettings.UserRoleStatus)
	'		Else
	'			result = Nothing
	'		End If
	'	Catch ex As Exception
	'		_log.Error("Error while GetPortalUserRoleByUserID", ex)
	'	Finally
	'		oDB.closeConnection()
	'	End Try
	'	Return result
	'End Function

	Public Function GetUserRoleByID(userRoleID As Guid) As SmUserRole
		Dim result As SmUserRole
		Dim oData As New DataTable()
		Dim sSQL As String = String.Format("SELECT * FROM {0} UR", USERROLES_TABLE)
		Dim oWhere As New CSQLWhereStringBuilder()
		oWhere.AppendAndCondition("UserRoleID={0}", New CSQLParamValue(userRoleID))
		Dim oDB As New CSQLDBUtils(LPQMOBILE_CONNECTIONSTRING)
		Try
			oData = oDB.getDataTable(sSQL & oWhere.SQL)
			If oData IsNot Nothing AndAlso oData.Rows.Count > 0 Then
				Dim row As DataRow = oData.Rows(0)
				result = New SmUserRole()
				result.UserRoleID = Common.SafeGUID(row("UserRoleID"))
				result.Role = CType([Enum].Parse(GetType(SmSettings.Role), Common.SafeString(row("Role"))), SmSettings.Role)
				result.LenderID = Common.SafeGUID(row("LenderID"))
				result.LenderConfigID = Common.SafeGUID(row("LenderConfigID"))
				result.VendorID = Common.SafeString(row("VendorID"))
				result.AssignedVendorGroupByTags = Common.SafeString(row("AssignedVendorGroupByTags"))
				If Not String.IsNullOrEmpty(result.VendorID) Then
					result.Vendor = GetLenderVendorByVendorID(result.VendorID)
				End If
				result.UserID = Common.SafeGUID(row("UserID"))
				If result.UserID <> Nothing AndAlso result.UserID <> Guid.Empty Then
					result.User = New SmAuthBL().GetUserByID(result.UserID)
				End If
				result.Status = DirectCast(Common.SafeInteger(row("Status")), SmSettings.UserRoleStatus)
			Else
				result = Nothing
			End If
		Catch ex As Exception
			_log.Error("Error while GetUserRoleByID", ex)
		Finally
			oDB.closeConnection()
		End Try
		Return result
	End Function

	Public Function GetVendorUserRoleByID(userRoleID As Guid) As SmUserRole
		Dim result As SmUserRole
		Dim oData As New DataTable()
		Dim sSQL As String = String.Format("SELECT * FROM {0} UR", USERROLES_TABLE)
		Dim oWhere As New CSQLWhereStringBuilder()
		oWhere.AppendAndCondition("UserRoleID={0}", New CSQLParamValue(userRoleID))
		Dim oDB As New CSQLDBUtils(LPQMOBILE_CONNECTIONSTRING)
		Try
			oData = oDB.getDataTable(sSQL & oWhere.SQL)
			If oData IsNot Nothing AndAlso oData.Rows.Count > 0 Then
				Dim row As DataRow = oData.Rows(0)
				result = New SmUserRole()
				result.UserRoleID = Common.SafeGUID(row("UserRoleID"))
				result.Role = CType([Enum].Parse(GetType(SmSettings.Role), Common.SafeString(row("Role"))), SmSettings.Role)
				result.LenderID = Common.SafeGUID(row("LenderID"))
				result.LenderConfigID = Common.SafeGUID(row("LenderConfigID"))
				result.VendorID = Common.SafeString(row("VendorID"))
				If Not String.IsNullOrEmpty(result.VendorID) Then
					result.Vendor = GetLenderVendorByVendorID(result.VendorID)
				End If
				result.UserID = Common.SafeGUID(row("UserID"))
				If result.UserID <> Nothing AndAlso result.UserID <> Guid.Empty Then
					result.User = New SmAuthBL().GetUserByID(result.UserID)
				End If
				result.Status = DirectCast(Common.SafeInteger(row("Status")), SmSettings.UserRoleStatus)
			Else
				result = Nothing
			End If
		Catch ex As Exception
			_log.Error("Error while GetUserRoleByID", ex)
		Finally
			oDB.closeConnection()
		End Try
		Return result
	End Function

	''' <summary>
	''' Grant a role to a user. If the user is locked due to expired date or login failed exceed and unlock user is true, unlock that user
	''' </summary>
	''' <param name="userRole"></param>
	''' <param name="createdByUserId"></param>
	''' <param name="ipAddress"></param>
	''' <param name="UnlockUser">False by default</param>
	''' <returns></returns>
	''' <remarks></remarks>
	Public Function AddUserRole(userRole As SmUserRole, createdByUserId As Guid, ipAddress As String, Optional unlockUser As Boolean = False) As String
		Dim result As String = ""
		Dim oDB As New CSQLDBUtils(LPQMOBILE_CONNECTIONSTRING)
		Try
			If unlockUser = True Then
				Dim usr = New SmAuthBL().GetUserByID(userRole.UserID)
				Dim oWhere As New CSQLWhereStringBuilder()
				oWhere.AppendAndCondition("UserID={0}", New CSQLParamValue(userRole.UserID))
				Dim oUpdate As New cSQLUpdateStringBuilder(USERS_TABLE, oWhere)
				oUpdate.appendvalue("LoginFailedCount", New CSQLParamValue(0))
				If usr IsNot Nothing AndAlso usr.LastLoginDate.HasValue AndAlso usr.LastLoginDate.Value.AddDays(usr.ExpireDays) < Now Then
					oUpdate.appendvalue("LastLoginDate", New CSQLParamValue(Now.AddDays(-1)))
				End If
				oDB.executeNonQuery(oUpdate.SQL, True)
			End If
			Dim oInsert As New cSQLInsertStringBuilder(USERROLES_TABLE)
			oInsert.AppendValue("UserRoleID", New CSQLParamValue(userRole.UserRoleID))
			If Not String.IsNullOrEmpty(userRole.VendorID) Then
				oInsert.AppendValue("VendorID", New CSQLParamValue(Common.SafeString(userRole.VendorID)))
			End If
			If Not String.IsNullOrWhiteSpace(userRole.AssignedVendorGroupByTags) Then
				oInsert.AppendValue("AssignedVendorGroupByTags", New CSQLParamValue(Common.SafeString(userRole.AssignedVendorGroupByTags)))
			End If
			oInsert.AppendValue("LenderID", New CSQLParamValue(userRole.LenderID))
			oInsert.AppendValue("UserID", New CSQLParamValue(userRole.UserID))
			If userRole.LenderConfigID <> Guid.Empty Then
				oInsert.AppendValue("LenderConfigID", New CSQLParamValue(userRole.LenderConfigID))
			End If
			oInsert.AppendValue("Role", New CSQLParamValue(userRole.Role.ToString()))
			oInsert.AppendValue("CreatedDate", New CSQLParamValue(Now))
			oInsert.AppendValue("Status", New CSQLParamValue(SmSettings.UserRoleStatus.Active))
			oDB.executeNonQuery(oInsert.SQL, True)
			Dim actionLogBL As New SmActionLogBL()
			actionLogBL.WriteActionLog(New SmActionLog() With {.Action = SmActionLogAction.ADD_USER_ROLE, .Category = SmActionLogCategory.USER_ROLES_MANIPULATION, .CreatedByUserID = createdByUserId, .IpAddress = ipAddress, .ObjectID = userRole.UserRoleID.ToString(), .ActionNote = String.Format("Add new user role")})
			result = "ok"
		Catch ex As Exception
			_log.Error("Error while AddUserRole", ex)
			result = "error"
		Finally
			oDB.commitTransactionIfOpen()
			oDB.closeConnection()
		End Try
		Return result
	End Function
	'Public Function ForceAddVendorAgentUserRole(userRole As SmUserRole, createdByUserId As Guid, ipAddress As String) As String
	'	Dim result As String = ""
	'	Dim oDB As New CSQLDBUtils(LPQMOBILE_CONNECTIONSTRING)
	'	Try
	'		Dim oWhere As New CSQLWhereStringBuilder()
	'		oWhere.AppendAndCondition("UserID={0}", New CSQLParamValue(userRole.UserID))
	'		oWhere.AppendAndCondition("VendorID={0}", New CSQLParamValue(userRole.VendorID))
	'		oWhere.AppendAndCondition("LenderConfigID={0}", New CSQLParamValue(userRole.LenderConfigID))
	'		Dim oDelete As New CSQLDeleteStringBuilder(USERROLES_TABLE, oWhere)
	'		oDB.executeNonQuery(oDelete.SQL, False)

	'		Dim oInsert As New cSQLInsertStringBuilder(USERROLES_TABLE)
	'		oInsert.AppendValue("UserRoleID", New CSQLParamValue(userRole.UserRoleID))
	'		oInsert.AppendValue("VendorID", New CSQLParamValue(Common.SafeString(userRole.VendorID)))
	'		oInsert.AppendValue("LenderID", New CSQLParamValue(userRole.LenderID))
	'		oInsert.AppendValue("UserID", New CSQLParamValue(userRole.UserID))
	'		oInsert.AppendValue("LenderConfigID", New CSQLParamValue(userRole.LenderConfigID))
	'		oInsert.AppendValue("Role", New CSQLParamValue(userRole.Role.ToString()))
	'		oInsert.AppendValue("Status", New CSQLParamValue(SmSettings.UserRoleStatus.Active))
	'		oDB.executeNonQuery(oInsert.SQL, True)
	'		Dim actionLogBL As New SmActionLogBL()
	'		actionLogBL.WriteActionLog(New SmActionLog() With {.Action = SmActionLogAction.ADD_USER_ROLE, .Category = SmActionLogCategory.USER_ROLES_MANIPULATION, .CreatedByUserID = createdByUserId, .IpAddress = ipAddress, .ObjectID = userRole.UserRoleID.ToString(), .ActionNote = String.Format("Force add user role")})
	'		result = "ok"
	'	Catch ex As Exception
	'		_log.Error("Error while ForceAddVendorAgentUserRole", ex)
	'		result = "error"
	'	Finally
	'		oDB.commitTransactionIfOpen()
	'		oDB.closeConnection()
	'	End Try
	'	Return result
	'End Function
	Public Function RemoveUserRoleByUserRoleID(userRoleID As Guid) As String
		Dim result As String = ""
		Dim oDB As New CSQLDBUtils(LPQMOBILE_CONNECTIONSTRING)
		Try
			Dim oWhere As New CSQLWhereStringBuilder()
			oWhere.AppendAndCondition("UserRoleID={0}", New CSQLParamValue(userRoleID))
			Dim oDelete As New CSQLDeleteStringBuilder(USERROLES_TABLE, oWhere)
			oDB.executeNonQuery(oDelete.SQL, False)
			result = "ok"
		Catch ex As Exception
			_log.Error("Error while RemoveUserRoleByUserRoleID", ex)
			result = "error"
		Finally
			oDB.closeConnection()
		End Try
		Return result
	End Function

	Public Function UpdateUserRole(userRole As SmUserRole, createdByUserId As Guid, ipAddress As String) As String
		Dim result As String = ""
		Dim oDB As New CSQLDBUtils(LPQMOBILE_CONNECTIONSTRING)
		Try
			Dim oWhere As New CSQLWhereStringBuilder()
			oWhere.AppendAndCondition("UserRoleID={0}", New CSQLParamValue(userRole.UserRoleID))

			Dim oUpdate As New cSQLUpdateStringBuilder(USERROLES_TABLE, oWhere)
			oUpdate.appendvalue("Role", New CSQLParamValue(Common.SafeString(userRole.Role.ToString())))
			oUpdate.appendvalue("LenderID", New CSQLParamValue(userRole.LenderID))
			oUpdate.appendvalue("LenderConfigID", New CSQLParamValue(Common.SafeString(userRole.LenderConfigID)))
			oUpdate.appendvalue("VendorID", New CSQLParamValue(Common.SafeString(userRole.VendorID)))
			oUpdate.appendvalue("UserID", New CSQLParamValue(Common.SafeString(userRole.UserID)))
			oUpdate.appendvalue("Status", New CSQLParamValue(userRole.Status))
			oUpdate.appendvalue("AssignedVendorGroupByTags", New CSQLParamValue(userRole.AssignedVendorGroupByTags))
			oDB.executeNonQuery(oUpdate.SQL, False)
			Dim actionLogBL As New SmActionLogBL()
			actionLogBL.WriteActionLog(New SmActionLog() With {.Action = SmActionLogAction.UPDATE_USER_ROLE, .Category = SmActionLogCategory.USER_ROLES_MANIPULATION, .CreatedByUserID = createdByUserId, .IpAddress = ipAddress, .ObjectID = userRole.UserRoleID.ToString(), .ActionNote = String.Format("Update user role")})
			result = "ok"
		Catch ex As Exception
			_log.Error("Error while UpdateUserRole", ex)
			result = "error"
		Finally
			oDB.closeConnection()
		End Try
		Return result
	End Function

	Public Function UpgradeUserRoleToLenderAdmin(userRole As SmUserRole, createdByUserId As Guid, ipAddress As String) As String
		Dim result As String = ""
		Dim oDB As New CSQLDBUtils(LPQMOBILE_CONNECTIONSTRING)
		Try
			Dim oWhere As New CSQLWhereStringBuilder()
			oWhere.AppendAndCondition("UserID={0}", New CSQLParamValue(userRole.UserID))
			oWhere.AppendAndCondition("LenderID={0}", New CSQLParamValue(userRole.LenderID))
			oWhere.AppendAndIn("Role", New List(Of CSQLParamValue) From {New CSQLParamValue(SmSettings.Role.PortalAdmin.ToString()), New CSQLParamValue(SmSettings.Role.VendorGroupAdmin.ToString())})
			Dim oDelete As New CSQLDeleteStringBuilder2(USERROLES_TABLE, oWhere)
			oDB.executeNonQuery(oDelete.SQL, True)
			Dim oInsert As New cSQLInsertStringBuilder(USERROLES_TABLE)
			oInsert.AppendValue("LenderID", New CSQLParamValue(userRole.LenderID))
			oInsert.AppendValue("UserRoleID", New CSQLParamValue(userRole.UserRoleID))
			oInsert.AppendValue("UserID", New CSQLParamValue(userRole.UserID))
			oInsert.AppendValue("Role", New CSQLParamValue(SmSettings.Role.LenderAdmin.ToString()))
			oInsert.AppendValue("Status", New CSQLParamValue(SmSettings.LenderVendorStatus.Active))
			oDB.executeNonQuery(oInsert.SQL, True)

			Dim actionLogBL As New SmActionLogBL()
			actionLogBL.WriteActionLog(New SmActionLog() With {.Action = SmActionLogAction.UPGRADE_USER_ROLE_TO_LENDERADMIN, .Category = SmActionLogCategory.USER_ROLES_MANIPULATION, .CreatedByUserID = createdByUserId, .IpAddress = ipAddress, .ObjectID = userRole.UserRoleID.ToString(), .ActionNote = String.Format("Upgrade user role to {0}", SmSettings.Role.LenderAdmin.GetEnumDescription())})
			result = "ok"
		Catch ex As Exception
			_log.Error("Error while UpgradeUserRoleToLenderAdmin", ex)
			result = "error"
		Finally
			oDB.commitTransactionIfOpen()
			oDB.closeConnection()
		End Try
		Return result
	End Function

	Public Function UpgradeUserRoleToPortalAdmin(userRole As SmUserRole, createdByUserId As Guid, ipAddress As String) As String
		Dim result As String = ""
		Dim oDB As New CSQLDBUtils(LPQMOBILE_CONNECTIONSTRING)
		Try
			Dim oWhere As New CSQLWhereStringBuilder()
			oWhere.AppendAndCondition("UserID={0}", New CSQLParamValue(userRole.UserID))
			oWhere.AppendAndCondition("LenderID={0}", New CSQLParamValue(userRole.LenderID))
			oWhere.AppendAndCondition("LenderConfigID={0}", New CSQLParamValue(userRole.LenderConfigID))
			oWhere.AppendAndIn("Role", New List(Of CSQLParamValue) From {New CSQLParamValue(SmSettings.Role.VendorGroupAdmin.ToString())})
			Dim oDelete As New CSQLDeleteStringBuilder2(USERROLES_TABLE, oWhere)
			oDB.executeNonQuery(oDelete.SQL, True)
			Dim oInsert As New cSQLInsertStringBuilder(USERROLES_TABLE)
			oInsert.AppendValue("LenderID", New CSQLParamValue(userRole.LenderID))
			oInsert.AppendValue("UserRoleID", New CSQLParamValue(userRole.UserRoleID))
			oInsert.AppendValue("LenderConfigID", New CSQLParamValue(userRole.LenderConfigID))
			oInsert.AppendValue("UserID", New CSQLParamValue(userRole.UserID))
			oInsert.AppendValue("Role", New CSQLParamValue(SmSettings.Role.PortalAdmin.ToString()))
			oInsert.AppendValue("Status", New CSQLParamValue(SmSettings.LenderVendorStatus.Active))
			oDB.executeNonQuery(oInsert.SQL, True)

			Dim actionLogBL As New SmActionLogBL()
			actionLogBL.WriteActionLog(New SmActionLog() With {.Action = SmActionLogAction.UPGRADE_USER_ROLE_TO_PORTALADMIN, .Category = SmActionLogCategory.USER_ROLES_MANIPULATION, .CreatedByUserID = createdByUserId, .IpAddress = ipAddress, .ObjectID = userRole.UserRoleID.ToString(), .ActionNote = String.Format("Upgrade user role to {0}", SmSettings.Role.PortalAdmin.GetEnumDescription())})
			result = "ok"
		Catch ex As Exception
			_log.Error("Error while UpgradeUserRoleToPortalAdmin", ex)
			result = "error"
		Finally
			oDB.commitTransactionIfOpen()
			oDB.closeConnection()
		End Try
		Return result
	End Function

	Public Function DowngradeUserRoleToPortalAdmin(userRole As SmUserRole, createdByUserId As Guid, ipAddress As String) As String
		Dim result As String = ""
		Dim oDB As New CSQLDBUtils(LPQMOBILE_CONNECTIONSTRING)
		Try
			Dim oWhere As New CSQLWhereStringBuilder()
			oWhere.AppendAndCondition("UserID={0}", New CSQLParamValue(userRole.UserID))
			oWhere.AppendAndCondition("LenderID={0}", New CSQLParamValue(userRole.LenderID))
			oWhere.AppendAndIn("Role", New List(Of CSQLParamValue) From {New CSQLParamValue(SmSettings.Role.LenderAdmin.ToString())})
			Dim oDelete As New CSQLDeleteStringBuilder2(USERROLES_TABLE, oWhere)
			oDB.executeNonQuery(oDelete.SQL, True)
			Dim oInsert As New cSQLInsertStringBuilder(USERROLES_TABLE)
			oInsert.AppendValue("LenderID", New CSQLParamValue(userRole.LenderID))
			oInsert.AppendValue("UserRoleID", New CSQLParamValue(userRole.UserRoleID))
			oInsert.AppendValue("LenderConfigID", New CSQLParamValue(userRole.LenderConfigID))
			oInsert.AppendValue("UserID", New CSQLParamValue(userRole.UserID))
			oInsert.AppendValue("Role", New CSQLParamValue(SmSettings.Role.PortalAdmin.ToString()))
			oInsert.AppendValue("Status", New CSQLParamValue(SmSettings.LenderVendorStatus.Active))
			oDB.executeNonQuery(oInsert.SQL, True)

			Dim actionLogBL As New SmActionLogBL()
			actionLogBL.WriteActionLog(New SmActionLog() With {.Action = SmActionLogAction.DOWNGRADE_USER_ROLE_TO_PORTALADMIN, .Category = SmActionLogCategory.USER_ROLES_MANIPULATION, .CreatedByUserID = createdByUserId, .IpAddress = ipAddress, .ObjectID = userRole.UserRoleID.ToString(), .ActionNote = String.Format("Downgrade user role to {0}", SmSettings.Role.PortalAdmin.GetEnumDescription())})
			result = "ok"
		Catch ex As Exception
			_log.Error("Error while DowngradeUserRoleToPortalAdmin", ex)
			result = "error"
		Finally
			oDB.commitTransactionIfOpen()
			oDB.closeConnection()
		End Try
		Return result
	End Function
	Public Function DowngradeUserRoleToVendorGroupAdmin(userRole As SmUserRole, createdByUserId As Guid, ipAddress As String) As String
		Dim result As String = ""
		Dim oDB As New CSQLDBUtils(LPQMOBILE_CONNECTIONSTRING)
		Try
			Dim oWhere As New CSQLWhereStringBuilder()
			oWhere.AppendAndCondition("UserID={0}", New CSQLParamValue(userRole.UserID))
			oWhere.AppendAndCondition("LenderID={0}", New CSQLParamValue(userRole.LenderID))
			oWhere.AppendAndIn("Role", New List(Of CSQLParamValue) From {New CSQLParamValue(SmSettings.Role.LenderAdmin.ToString())})
			Dim oDelete As New CSQLDeleteStringBuilder2(USERROLES_TABLE, oWhere)
			oDB.executeNonQuery(oDelete.SQL, True)

			oWhere = New CSQLWhereStringBuilder()
			oWhere.AppendAndCondition("UserID={0}", New CSQLParamValue(userRole.UserID))
			oWhere.AppendAndCondition("LenderID={0}", New CSQLParamValue(userRole.LenderID))
			oWhere.AppendAndCondition("LenderConfigID={0}", New CSQLParamValue(userRole.LenderConfigID))
			oWhere.AppendAndIn("Role", New List(Of CSQLParamValue) From {New CSQLParamValue(SmSettings.Role.PortalAdmin.ToString())})
			oDelete = New CSQLDeleteStringBuilder2(USERROLES_TABLE, oWhere)
			oDB.executeNonQuery(oDelete.SQL, True)

			Dim oInsert As New cSQLInsertStringBuilder(USERROLES_TABLE)
			oInsert.AppendValue("LenderID", New CSQLParamValue(userRole.LenderID))
			oInsert.AppendValue("UserRoleID", New CSQLParamValue(userRole.UserRoleID))
			oInsert.AppendValue("LenderConfigID", New CSQLParamValue(userRole.LenderConfigID))
			oInsert.AppendValue("UserID", New CSQLParamValue(userRole.UserID))
			oInsert.AppendValue("Role", New CSQLParamValue(userRole.Role.ToString()))
			oInsert.AppendValue("AssignedVendorGroupByTags", New CSQLParamValue(userRole.AssignedVendorGroupByTags))
			oInsert.AppendValue("Status", New CSQLParamValue(SmSettings.LenderVendorStatus.Active))
			oDB.executeNonQuery(oInsert.SQL, True)

			Dim actionLogBL As New SmActionLogBL()
			actionLogBL.WriteActionLog(New SmActionLog() With {.Action = SmActionLogAction.DOWNGRADE_USER_ROLE_TO_VENDORGROUPADMIN, .Category = SmActionLogCategory.USER_ROLES_MANIPULATION, .CreatedByUserID = createdByUserId, .IpAddress = ipAddress, .ObjectID = userRole.UserRoleID.ToString(), .ActionNote = String.Format("Downgrade user role to {0}", SmSettings.Role.PortalAdmin.GetEnumDescription())})
			result = "ok"
		Catch ex As Exception
			_log.Error("Error while DowngradeUserRoleToVendorGroupAdmin", ex)
			result = "error"
		Finally
			oDB.commitTransactionIfOpen()
			oDB.closeConnection()
		End Try
		Return result
	End Function
	Public Function GetAvailableTagsByLenderID(lenderID As Guid) As List(Of String)
		Dim result As New List(Of String)
		Dim oData As New DataTable()
		Dim sSQL As String = String.Format("SELECT Tags FROM {0} ", LENDERVENDORS_TABLE)
		Dim oWhere As New CSQLWhereStringBuilder()
		oWhere.AppendAndCondition("LenderID={0}", New CSQLParamValue(lenderID))
		oWhere.AppendAndCondition("Tags IS NOT NULL")
		Dim oDB As New CSQLDBUtils(LPQMOBILE_CONNECTIONSTRING)
		Try
			oData = oDB.getDataTable(sSQL & oWhere.SQL)
			If oData IsNot Nothing AndAlso oData.Rows.Count > 0 Then
				Dim tagsStr As String = ""
				For Each row In oData.Rows
					Dim tags = Common.SafeString(row("Tags"))
					If Not String.IsNullOrWhiteSpace(tags) Then
						tagsStr = tagsStr & "," & tags
					End If
				Next
				If Not String.IsNullOrWhiteSpace(tagsStr) Then
					result = tagsStr.Split(","c).Distinct().Where(Function(s) Not String.IsNullOrWhiteSpace(s)).ToList()
				End If
			End If
		Catch ex As Exception
			_log.Error("Error while GetAvailableTagsByLenderID", ex)
		Finally
			oDB.closeConnection()
		End Try
		Return result
	End Function

	Public Function GetPortalByLenderConfigID(lenderConfigID As Guid, Optional pbDecrypt As Boolean = True) As SmLenderConfig
		Dim portal As SmLenderConfig
		Dim oData As New DataTable()
		Dim sSQL As String = String.Format("SELECT L.* FROM {0} L ", LENDERCONFIG_TABLE)
		Dim oWhere As New CSQLWhereStringBuilder()
		oWhere.AppendAndCondition("L.LenderConfigID={0}", New CSQLParamValue(lenderConfigID))
		Dim oDB As New CSQLDBUtils(LPQMOBILE_CONNECTIONSTRING)
		Try
			oData = oDB.getDataTable(sSQL & oWhere.SQL)
			If oData IsNot Nothing AndAlso oData.Rows.Count > 0 Then
				Dim row = oData.Rows(0)
				portal = New SmLenderConfig
				portal.LenderConfigID = Common.SafeGUID(row("LenderConfigID"))
				portal.OrganizationID = Common.SafeGUID(row("OrganizationID"))
				portal.LenderID = Common.SafeGUID(row("LenderID"))
				portal.LenderRef = Common.SafeString(row("LenderRef"))
				portal.ReturnURL = Common.SafeString(row("ReturnURL"))
				portal.LPQLogin = Common.SafeString(row("LPQLogin"))
				portal.LPQPW = Common.SafeString(row("LPQPW"))
				If pbDecrypt = True Then
					portal.LPQPW = Common.DecryptPassword(portal.LPQPW)
				End If
				portal.LPQURL = Common.SafeString(row("LPQURL"))
				portal.Note = Common.SafeString(row("note"))

				portal.ConfigData = Common.SafeString(row("ConfigData"))
				If Not String.IsNullOrWhiteSpace(portal.ConfigData) Then
					Dim configXml As New XmlDocument
					'this try catch is to handl logic when the configData is of invalid xml format 
					Try
						configXml.LoadXml(portal.ConfigData)
						Dim oXmlElement As XmlElement = configXml.DocumentElement
						oXmlElement.SetAttribute("organization_id", portal.OrganizationID.ToString.Replace("-", ""))
						oXmlElement.SetAttribute("lender_id", portal.LenderID.ToString.Replace("-", ""))
						oXmlElement.SetAttribute("lender_ref", portal.LenderRef)
						oXmlElement.SetAttribute("finished_url", portal.ReturnURL)
						oXmlElement.SetAttribute("user", portal.LPQLogin)
						oXmlElement.SetAttribute("loan_submit_url", portal.LPQURL)
						oXmlElement.SetAttribute("password", portal.LPQPW)
						If pbDecrypt = True Then
							'by running DecryptAllPasswordInConfigXml after oXmlElement.SetAttribute("password", oLender.LPQPW), we reduce 1 time decrypt password call
							oXmlElement = Common.DecryptAllPasswordInConfigXml(oXmlElement)
						End If
						portal.ConfigData = oXmlElement.OuterXml
					Catch ex As Exception
						_log.Warn("GetPortalByLenderConfigID Error: Cannot load configXml" & portal.ConfigData, ex)
					End Try
				End If

				If Not row.IsNull("LastModifiedDate") Then
					portal.LastModifiedDate = Common.SafeDate(row("LastModifiedDate"))
				End If
				portal.CreatedDate = Common.SafeDate(row("CreatedDate"))
				If Not row.IsNull("LastOpenedDate") Then
					portal.LastModifiedDate = Common.SafeDate(row("LastOpenedDate"))
				End If
				portal.LastModifiedByUserID = Common.SafeGUID(row("LastModifiedByUserID"))
				portal.LastOpenedByUserID = Common.SafeGUID(row("LastOpenedByUserID"))
				portal.IterationID = Common.SafeInteger(row("IterationID"))
				portal.RevisionID = Common.SafeInteger(row("RevisionID"))
			End If
		Catch ex As Exception
			_log.Error("Error while GetPortalByLenderConfigID", ex)
		Finally
			oDB.closeConnection()
		End Try
		Return portal
	End Function


	Public Function GetPortalBasicInfoByLenderConfigID(lenderConfigID As Guid) As SmLenderConfigBasicInfo
		Dim portal As SmLenderConfigBasicInfo
		Dim oData As New DataTable()
		Dim sSQL As String = String.Format("SELECT L.* FROM {0} L ", LENDERCONFIG_TABLE)
		Dim oWhere As New CSQLWhereStringBuilder()
		oWhere.AppendAndCondition("L.LenderConfigID={0}", New CSQLParamValue(lenderConfigID))
		Dim oDB As New CSQLDBUtils(LPQMOBILE_CONNECTIONSTRING)
		Try
			oData = oDB.getDataTable(sSQL & oWhere.SQL)
			If oData IsNot Nothing AndAlso oData.Rows.Count > 0 Then
				Dim row = oData.Rows(0)
				portal = New SmLenderConfigBasicInfo
				portal.LenderConfigID = Common.SafeGUID(row("LenderConfigID"))
				portal.OrganizationID = Common.SafeGUID(row("OrganizationID"))
				portal.LenderID = Common.SafeGUID(row("LenderID"))
				portal.LenderRef = Common.SafeString(row("LenderRef"))
				portal.ReturnURL = Common.SafeString(row("ReturnURL"))
				portal.LPQURL = Common.SafeString(row("LPQURL"))
				portal.Note = Common.SafeString(row("note"))
			End If
		Catch ex As Exception
			_log.Error("Error while GetPortalBasicInfoByLenderConfigID", ex)
		Finally
			oDB.closeConnection()
		End Try
		Return portal
	End Function

	Public Function CheckLenderExistedByLenderConfigID(lenderConfigID As Guid) As Boolean
		Dim sSQL As String = String.Format("SELECT Count(L.LenderConfigID) FROM {0} L ", LENDERCONFIG_TABLE)
		Dim oWhere As New CSQLWhereStringBuilder()
		oWhere.AppendAndCondition("L.LenderConfigID={0}", New CSQLParamValue(lenderConfigID))
		Dim oDB As New CSQLDBUtils(LPQMOBILE_CONNECTIONSTRING)
		Try
			Return Common.SafeInteger(oDB.getScalerValue(sSQL & oWhere.SQL)) > 0
		Catch ex As Exception
			_log.Error("Error while CheckLenderExistedByLenderConfigID", ex)
		Finally
			oDB.closeConnection()
		End Try
		Return False
	End Function

	Public Function CheckLenderExistedByLenderRef(lenderRef As String) As Boolean
		Dim sSQL As String = String.Format("SELECT Count(L.LenderConfigID) FROM {0} L ", LENDERCONFIG_TABLE)
		Dim oWhere As New CSQLWhereStringBuilder()
		oWhere.AppendAndCondition("L.LenderRef={0}", New CSQLParamValue(lenderRef))
		Dim oDB As New CSQLDBUtils(LPQMOBILE_CONNECTIONSTRING)
		Try
			Return Common.SafeInteger(oDB.getScalerValue(sSQL & oWhere.SQL)) > 0
		Catch ex As Exception
			_log.Error("Error while CheckLenderExistedByLenderRef", ex)
		Finally
			oDB.closeConnection()
		End Try
		Return False
	End Function


	Public Function GetPortalByLenderRef(lenderRef As String, Optional ByVal pbDecrypt As Boolean = True) As SmLenderConfig
		Dim portal As SmLenderConfig
		Dim oData As New DataTable()
		Dim sSQL As String = String.Format("SELECT L.* FROM {0} L ", LENDERCONFIG_TABLE)
		Dim oWhere As New CSQLWhereStringBuilder()
		oWhere.AppendAndCondition("L.LenderRef={0}", New CSQLParamValue(lenderRef))
		Dim oDB As New CSQLDBUtils(LPQMOBILE_CONNECTIONSTRING)
		Try
			oData = oDB.getDataTable(sSQL & oWhere.SQL)
			If oData IsNot Nothing AndAlso oData.Rows.Count > 0 Then
				Dim row = oData.Rows(0)
				portal = New SmLenderConfig
				portal.LenderConfigID = Common.SafeGUID(row("LenderConfigID"))
				portal.OrganizationID = Common.SafeGUID(row("OrganizationID"))
				portal.LenderID = Common.SafeGUID(row("LenderID"))
				portal.LenderRef = Common.SafeString(row("LenderRef"))
				portal.ReturnURL = Common.SafeString(row("ReturnURL"))
				portal.LPQLogin = Common.SafeString(row("LPQLogin"))
				portal.LPQPW = Common.SafeString(row("LPQPW"))
				portal.LPQURL = Common.SafeString(row("LPQURL"))
				portal.Note = Common.SafeString(row("note"))
				portal.ConfigData = Common.SafeString(row("ConfigData"))
				If pbDecrypt = True Then
					portal.LPQPW = Common.DecryptPassword(portal.LPQPW)
				End If
				If Not String.IsNullOrWhiteSpace(portal.ConfigData) Then
					Dim configXml As New XmlDocument
					'this try catch is to handl logic when the configData is of invalid xml format 
					Try
						configXml.LoadXml(portal.ConfigData)
						Dim oXmlElement As XmlElement = configXml.DocumentElement
						oXmlElement.SetAttribute("organization_id", portal.OrganizationID.ToString.Replace("-", ""))
						oXmlElement.SetAttribute("lender_id", portal.LenderID.ToString.Replace("-", ""))
						oXmlElement.SetAttribute("lender_ref", portal.LenderRef)
						oXmlElement.SetAttribute("finished_url", portal.ReturnURL)
						oXmlElement.SetAttribute("user", portal.LPQLogin)
						oXmlElement.SetAttribute("loan_submit_url", portal.LPQURL)
						oXmlElement.SetAttribute("password", portal.LPQPW)
						If pbDecrypt = True Then
							'by running DecryptAllPasswordInConfigXml after oXmlElement.SetAttribute("password", oLender.LPQPW), we reduce 1 time decrypt password call
							oXmlElement = Common.DecryptAllPasswordInConfigXml(oXmlElement)
						End If
						portal.ConfigData = oXmlElement.OuterXml
					Catch ex As Exception
						_log.Warn("GetPortalByLenderRef Error: Cannot load configXml" & portal.ConfigData, ex)
					End Try
				End If
				If Not row.IsNull("LastModifiedDate") Then
					portal.LastModifiedDate = Common.SafeDate(row("LastModifiedDate"))
				End If
				portal.CreatedDate = Common.SafeDate(row("CreatedDate"))
				If Not row.IsNull("LastOpenedDate") Then
					portal.LastModifiedDate = Common.SafeDate(row("LastOpenedDate"))
				End If
				portal.LastModifiedByUserID = Common.SafeGUID(row("LastModifiedByUserID"))
				portal.LastOpenedByUserID = Common.SafeGUID(row("LastOpenedByUserID"))
				portal.IterationID = Common.SafeInteger(row("IterationID"))
				portal.RevisionID = Common.SafeInteger(row("RevisionID"))
			End If
		Catch ex As Exception
			_log.Error("Error while GetPortalByLenderRef", ex)
		Finally
			oDB.closeConnection()
		End Try
		Return portal
	End Function


	Public Function RemoveAllRoleOfUser(user As SmUser, comment As String, createdByUserId As Guid, ipAddress As String) As String
		Dim result As String = ""
		Dim oDB As New CSQLDBUtils(LPQMOBILE_CONNECTIONSTRING)
		Try
			Dim oWhere As New CSQLWhereStringBuilder()
			oWhere.AppendAndCondition("UserID={0}", New CSQLParamValue(user.UserID))
			Dim oDelete As New CSQLDeleteStringBuilder(USERROLES_TABLE, oWhere)
			oDB.executeNonQuery(oDelete.SQL, False)
			Dim actionLogBL As New SmActionLogBL()
			actionLogBL.WriteActionLog(New SmActionLog() With {.Action = SmActionLogAction.REMOVE_USER_ROLES, .Category = SmActionLogCategory.USER_ROLES_MANIPULATION, .CreatedByUserID = createdByUserId, .IpAddress = ipAddress, .ObjectID = user.UserID.ToString(), .ActionNote = String.Format("Remove all role of user {0} with comment ""{1}""", user.Email, comment)})
			result = "ok"
		Catch ex As Exception
			_log.Error("Error while RemoveAllRoleOfUser", ex)
			result = "error"
		Finally
			oDB.closeConnection()
		End Try
		Return result
	End Function

	'Private Function PrepareLenderConfigData(source As String, model As SmCloneLenderModel) As String
	'	Dim oDocXML As New XmlDocument
	'	oDocXML.LoadXml(Common.SafeString(source))
	'	Dim oXmlElement As XmlElement = oDocXML.DocumentElement
	'	oXmlElement.SetAttribute("organization_id", model.OrganizationID.ToString.Replace("-", ""))
	'	oXmlElement.SetAttribute("lender_id", model.LenderID.ToString.Replace("-", ""))
	'	oXmlElement.SetAttribute("lender_ref", model.LenderRef)
	'	oXmlElement.SetAttribute("user", model.ApiLogin)
	'	oXmlElement.SetAttribute("password", model.ApiLogin)
	'	oXmlElement.RemoveAttribute("IterationID")	'don't want this in the audit
	'	Return oXmlElement.OuterXml
	'End Function



	Public Function CloneLender(model As SmCloneLenderModel, sourcePortal As SmLenderConfig, user As SmUser, ipAddr As String) As String
		Dim result As String = ""
		Dim oDB As New CSQLDBUtils(LPQMOBILE_CONNECTIONSTRING)
		Try
			Dim newConfigData As String = ""
			Dim oDocXML As New XmlDocument
			oDocXML.LoadXml(Common.SafeString(sourcePortal.ConfigData))
			Dim oXmlElement As XmlElement = oDocXML.DocumentElement

			Dim newLpqUrl As String = sourcePortal.LPQURL
			Dim match As Match = Regex.Match(newLpqUrl, "^(?<protocol>https?://)(?<subdomain>\w+\.)?(?<domain>\w+\.[a-z0-9]+)/", RegexOptions.IgnoreCase)
			If match.Success Then
				newLpqUrl = Regex.Replace(newLpqUrl, "^(?<protocol>https?://)(?<subdomain>\w+\.)?(?<domain>\w+\.[a-z0-9]+)/", String.Format("{0}{1}.{2}/", match.Groups("protocol").Value, model.LosDomain, match.Groups("domain").Value))
			End If

			oXmlElement.SetAttribute("organization_id", model.OrganizationID.ToString.Replace("-", ""))
			oXmlElement.SetAttribute("lender_id", model.LenderID.ToString.Replace("-", ""))
			oXmlElement.SetAttribute("lender_ref", model.LenderRef)
			oXmlElement.SetAttribute("org_code", model.OrganizationCode)
			oXmlElement.SetAttribute("lender_code", model.LenderCode)
			oXmlElement.SetAttribute("user", model.ApiLogin)
			oXmlElement.SetAttribute("password", model.ApiLogin)
			oXmlElement.SetAttribute("loan_submit_url", newLpqUrl)
			oXmlElement.RemoveAttribute("IterationID")	'don't want this in the audit
			newConfigData = Common.EncryptAllPasswordInConfigXml(oXmlElement.OuterXml).OuterXml

			Dim sourceBackOfficeLender As SmBackOfficeLender = GetBackOfficeLenderByLenderConfigID(sourcePortal.LenderConfigID)
			Dim newBackOfficeLenderID As Guid = Guid.NewGuid()
			Dim oInsert As New cSQLInsertStringBuilder(BACKOFFICELENDERS_TABLE)
			oInsert.AppendValue("BackOfficeLenderID", New CSQLParamValue(newBackOfficeLenderID))
			oInsert.AppendValue("LenderName", New CSQLParamValue(model.LenderName))
			oInsert.AppendValue("LenderID", New CSQLParamValue(model.LenderID))
			oInsert.AppendValue("LpqUrl", New CSQLParamValue(newLpqUrl))
			oInsert.AppendValue("City", New CSQLParamValue(model.City))
			oInsert.AppendValue("State", New CSQLParamValue(model.State))
			'oInsert.AppendValue("LastModifiedDate", New CSQLParamValue(Now))
			'oInsert.AppendValue("LastModifiedBy", New CSQLParamValue(user.Email))
			oInsert.AppendValue("Host", New CSQLParamValue(SmUtil.ExtractHostFromUrl(newLpqUrl)))
			oDB.executeNonQuery(oInsert.SQL, True)

			Dim newLenderConfigID As Guid = Guid.NewGuid()
			oInsert = New cSQLInsertStringBuilder(LENDERCONFIG_TABLE)
			oInsert.AppendValue("LenderConfigID", New CSQLParamValue(newLenderConfigID))
			oInsert.AppendValue("OrganizationID", New CSQLParamValue(model.OrganizationID))
			oInsert.AppendValue("LenderID", New CSQLParamValue(model.LenderID))
			oInsert.AppendValue("LenderRef", New CSQLParamValue(model.LenderRef))
			oInsert.AppendValue("ReturnURL", New CSQLParamValue(sourcePortal.ReturnURL))
			oInsert.AppendValue("LPQLogin", New CSQLParamValue(model.ApiLogin))
			oInsert.AppendValue("LPQPW", New CSQLParamValue(Common.EncryptPassword(model.ApiPassword)))
			oInsert.AppendValue("LPQURL", New CSQLParamValue(newLpqUrl))
			oInsert.AppendValue("ConfigData", New CSQLParamValue(newConfigData))
			oInsert.AppendValue("note", New CSQLParamValue(sourcePortal.Note))
			oInsert.AppendValue("LastModifiedDate", New CSQLParamValue(Now))
			oInsert.AppendValue("CreatedDate", New CSQLParamValue(Now))
			oInsert.AppendValue("LastModifiedByUserID", New CSQLParamValue(user.UserID))
			oInsert.AppendValue("LastOpenedByUserID", New CSQLParamValue(user.UserID))
			oInsert.AppendValue("IterationID", New CSQLParamValue(0))
			oInsert.AppendValue("LastOpenedDate", New CSQLParamValue(Now))
			oInsert.AppendValue("RevisionID", New CSQLParamValue(1))
			oDB.executeNonQuery(oInsert.SQL, True)


			oInsert = New cSQLInsertStringBuilder(LENDERCONFIG_LOGS_TABLE)
			oInsert.AppendValue("LenderConfigLogID", New CSQLParamValue(Guid.NewGuid))
			oInsert.AppendValue("LenderConfigID", New CSQLParamValue(newLenderConfigID)) 'this is unique
			oInsert.AppendValue("OrganizationID", New CSQLParamValue(model.OrganizationID))
			oInsert.AppendValue("LenderID", New CSQLParamValue(model.LenderID))
			oInsert.AppendValue("LenderRef", New CSQLParamValue(model.LenderRef))
			oInsert.AppendValue("ReturnURL", New CSQLParamValue(sourcePortal.ReturnURL))
			oInsert.AppendValue("LPQLogin", New CSQLParamValue(model.ApiLogin))
			oInsert.AppendValue("LPQPW", New CSQLParamValue(Common.EncryptPassword(model.ApiPassword)))
			oInsert.AppendValue("LPQURL", New CSQLParamValue(newLpqUrl))
			oInsert.AppendValue("ConfigData", New CSQLParamValue(CStringCompressor.Compress(newConfigData)))
			oInsert.AppendValue("LogTime", New CSQLParamValue(Now))
			oInsert.AppendValue("RemoteIP", New CSQLParamValue(ipAddr))
			oInsert.AppendValue("LastModifiedByUserID", New CSQLParamValue(user.UserID))
			oInsert.AppendValue("note", New CSQLParamValue(sourcePortal.Note))
			oInsert.AppendValue("Comment", New CSQLParamValue("clone"))
            oInsert.AppendValue("RevisionID", New CSQLParamValue(1))
            oInsert.AppendValue("CachedDiff", New CSQLParamValue(getCachedDiffData(sourcePortal.ConfigData, newConfigData)))
            oDB.executeNonQuery(oInsert.SQL, True)
			Dim actionLogBL As New SmActionLogBL()
			actionLogBL.WriteActionLog(New SmActionLog() With {.Action = SmActionLogAction.CLONE_LENDER, .Category = SmActionLogCategory.LENDERS_MANIPULATION, .CreatedByUserID = user.UserID, .IpAddress = ipAddr, .ObjectID = newBackOfficeLenderID.ToString(), .ActionNote = String.Format("Clone lender ""{0}"" from ""{1}""", model.LenderName, sourceBackOfficeLender.LenderName)})


			'clear cache
			Dim workingDomainMatch As Match = Regex.Match(newLpqUrl, SmSettings.extractDomainRegexPattern, RegexOptions.IgnoreCase)
			If workingDomainMatch.Success Then
				HttpContext.Current.Cache.Remove("SSO_AVAILABLE_ROLES_" & workingDomainMatch.Groups(1).Value.NullSafeToUpper_ & "_" & model.LenderCode.NullSafeToUpper_() & "_" & model.OrganizationCode.NullSafeToUpper_())
			End If
			result = "ok"
		Catch ex As Exception
			_log.Error("Error while CloneLender", ex)
			result = "error"
		Finally
			oDB.commitTransactionIfOpen()
			oDB.closeConnection()
		End Try
		Return result

	End Function

	Public Function ClonePortal(model As SmClonePortalModel, sourcePortal As SmLenderConfig, user As SmUser, ipAddr As String) As String
		Dim result As String = ""
		Dim oDB As New CSQLDBUtils(LPQMOBILE_CONNECTIONSTRING)
		Try
			Dim newConfigData As String = ""
			Dim oDocXML As New XmlDocument
			oDocXML.LoadXml(Common.SafeString(sourcePortal.ConfigData))
			Dim oXmlElement As XmlElement = oDocXML.DocumentElement
			oXmlElement.SetAttribute("lender_ref", model.LenderRef)
			oXmlElement.SetAttribute("user", model.ApiLogin)
			oXmlElement.SetAttribute("password", model.ApiLogin)
			oXmlElement.RemoveAttribute("IterationID")	'don't want this in the audit
			newConfigData = Common.EncryptAllPasswordInConfigXml(oXmlElement.OuterXml).OuterXml


			Dim newLenderConfigID As Guid = Guid.NewGuid()
			Dim oInsert As New cSQLInsertStringBuilder(LENDERCONFIG_TABLE)
			oInsert.AppendValue("LenderConfigID", New CSQLParamValue(newLenderConfigID))
			oInsert.AppendValue("OrganizationID", New CSQLParamValue(sourcePortal.OrganizationID))
			oInsert.AppendValue("LenderID", New CSQLParamValue(sourcePortal.LenderID))
			oInsert.AppendValue("LenderRef", New CSQLParamValue(model.LenderRef))
			oInsert.AppendValue("ReturnURL", New CSQLParamValue(sourcePortal.ReturnURL))
			oInsert.AppendValue("LPQLogin", New CSQLParamValue(model.ApiLogin))
			oInsert.AppendValue("LPQPW", New CSQLParamValue(Common.EncryptPassword(model.ApiPassword)))
			oInsert.AppendValue("LPQURL", New CSQLParamValue(sourcePortal.LPQURL))
			oInsert.AppendValue("ConfigData", New CSQLParamValue(newConfigData))
			oInsert.AppendValue("note", New CSQLParamValue(model.PortalName))
			oInsert.AppendValue("LastModifiedDate", New CSQLParamValue(Now))
			oInsert.AppendValue("CreatedDate", New CSQLParamValue(Now))
			oInsert.AppendValue("LastModifiedByUserID", New CSQLParamValue(user.UserID))
			oInsert.AppendValue("LastOpenedByUserID", New CSQLParamValue(user.UserID))
			oInsert.AppendValue("IterationID", New CSQLParamValue(0))
			oInsert.AppendValue("LastOpenedDate", New CSQLParamValue(Now))
			oInsert.AppendValue("RevisionID", New CSQLParamValue(1))
			oDB.executeNonQuery(oInsert.SQL, True)


			oInsert = New cSQLInsertStringBuilder(LENDERCONFIG_LOGS_TABLE)
			oInsert.AppendValue("LenderConfigLogID", New CSQLParamValue(Guid.NewGuid))
			oInsert.AppendValue("LenderConfigID", New CSQLParamValue(newLenderConfigID)) 'this is unique
			oInsert.AppendValue("OrganizationID", New CSQLParamValue(sourcePortal.OrganizationID))
			oInsert.AppendValue("LenderID", New CSQLParamValue(sourcePortal.LenderID))
			oInsert.AppendValue("LenderRef", New CSQLParamValue(model.LenderRef))
			oInsert.AppendValue("ReturnURL", New CSQLParamValue(sourcePortal.ReturnURL))
			oInsert.AppendValue("LPQLogin", New CSQLParamValue(model.ApiLogin))
			oInsert.AppendValue("LPQPW", New CSQLParamValue(Common.EncryptPassword(model.ApiPassword)))
			oInsert.AppendValue("LPQURL", New CSQLParamValue(sourcePortal.LPQURL))
			oInsert.AppendValue("ConfigData", New CSQLParamValue(CStringCompressor.Compress(newConfigData)))
			oInsert.AppendValue("LogTime", New CSQLParamValue(Now))
			oInsert.AppendValue("RemoteIP", New CSQLParamValue(ipAddr))
			oInsert.AppendValue("LastModifiedByUserID", New CSQLParamValue(user.UserID))
			oInsert.AppendValue("note", New CSQLParamValue(model.PortalName))
			oInsert.AppendValue("Comment", New CSQLParamValue("clone"))
            oInsert.AppendValue("RevisionID", New CSQLParamValue(1))
            oInsert.AppendValue("CachedDiff", New CSQLParamValue(getCachedDiffData(sourcePortal.ConfigData, newConfigData)))
            oDB.executeNonQuery(oInsert.SQL, True)
			Dim actionLogBL As New SmActionLogBL()
			actionLogBL.WriteActionLog(New SmActionLog() With {.Action = SmActionLogAction.CLONE_PORTAL, .Category = SmActionLogCategory.LENDERS_MANIPULATION, .CreatedByUserID = user.UserID, .IpAddress = ipAddr, .ObjectID = newLenderConfigID.ToString(), .ActionNote = String.Format("Clone portal ""{0}"" from ""{1}""", model.PortalName, sourcePortal.Note)})
			result = "ok"
		Catch ex As Exception
			_log.Error("Error while ClonePortal", ex)
			result = "error"
		Finally
			oDB.commitTransactionIfOpen()
			oDB.closeConnection()
		End Try
		Return result

	End Function

#Region "Loan"
	Public Function GetLoanList(ByRef paging As SmPagingInfoModel, filter As SmLoanListFilter) As List(Of SmLoan)
		Dim result As New List(Of SmLoan)
		Dim oData As New DataTable()
		Dim sSQL As String = String.Format("SELECT L.* FROM {0} L INNER JOIN {1} LV ON L.LenderID = LV.LenderID AND L.VendorID = LV.VendorID AND LV.Status = {3} INNER JOIN {2} UR on LV.LenderID = UR.LenderID and LV.VendorID = UR.VendorID AND UR.Status = {4} ", LOANS_TABLE, LENDERVENDORS_TABLE, USERROLES_TABLE, Convert.ToInt16(SmSettings.LenderVendorStatus.Active), Convert.ToInt16(SmSettings.UserRoleStatus.Active))
		Dim oWhere As New CSQLWhereStringBuilder()
		Dim oOrder As String = " "
		If Not String.IsNullOrEmpty(paging.SortColumn) Then
			oOrder = oOrder & "ORDER BY " & paging.SortColumn & " " & paging.SortDirection
		Else
			oOrder = oOrder & "ORDER BY CreatedDate DESC, ApplicantName "
		End If
		If filter IsNot Nothing Then
			If filter.UserID <> Nothing AndAlso filter.UserID <> Guid.Empty Then
				oWhere.AppendAndCondition("UR.UserID = {0}", New CSQLParamValue(filter.UserID))
			End If
		End If
		Dim oDB As New CSQLDBUtils(LPQMOBILE_CONNECTIONSTRING)
		Try
			oData = oDB.getDataTable(sSQL & oWhere.SQL & oOrder)
			If oData IsNot Nothing AndAlso oData.Rows.Count > 0 Then
				For Each row As DataRow In oData.Rows
					Dim item As New SmLoan()
					item.LoanSavingID = Common.SafeGUID(row("ID"))
					item.LenderID = Common.SafeGUID(row("LenderID"))
					item.LenderRef = Common.SafeString(row("LenderRef"))
					item.VendorID = Common.SafeString(row("VendorID"))
					item.UserID = Common.SafeGUID(row("UserID"))
					item.ApplicantName = Common.SafeString(row("ApplicantName"))
					If Not row.IsNull("CreatedDate") Then
						item.CreatedDate = Common.SafeDate(row("CreatedDate"))
					End If
					If Not row.IsNull("SubmitDate") Then
						item.SubmitDate = Common.SafeDate(row("SubmitDate"))
					End If
					item.LoanType = CType([Enum].Parse(GetType(SmSettings.LenderVendorType), Common.SafeString(row("LoanType"))), SmSettings.LenderVendorType)
					item.LoanNumber = Common.SafeString(row("LoanNumber"))
					item.LoanID = Common.SafeString(row("LoanID"))
					item.RequestParam = Common.SafeString(row("RequestParam"))
					item.RequestParam_Enc = Common.SafeString(row("RequestParam_Enc"))
					item.XmlResponse = Common.SafeString(row("XmlResponse"))
					item.Status = Common.SafeString(row("Status"))
					If Not row.IsNull("StatusDate") Then
						item.StatusDate = Common.SafeDate(row("StatusDate"))
					End If
					item.FundStatus = Common.SafeString(row("FundStatus"))
					If Not row.IsNull("FundDate") Then
						item.FundDate = Common.SafeDate(row("FundDate"))
					End If
					result.Add(item)
				Next
			ElseIf System.Configuration.ConfigurationManager.AppSettings.Get("ENVIRONMENT") <> "LIVE" Then
				'fake data
				'For i As Integer = 0 To 15
				'	Dim oInsert As New cSQLInsertStringBuilder(LOANS_TABLE)
				'	oInsert.AppendValue("ID", New CSQLParamValue(Guid.NewGuid()))
				'	oInsert.AppendValue("LenderID", New CSQLParamValue("65FBAD52-C528-48A1-B241-97FAF6F531DA"))
				'	oInsert.AppendValue("VendorID", New CSQLParamValue("a41d6c8e-3cf5-4e95-8ae5-b970e3492326"))
				'	oInsert.AppendValue("UserID", New CSQLParamValue("487FBE55-C299-4467-B06D-0478A59A1888"))
				'	oInsert.AppendValue("ApplicantName", New CSQLParamValue("Lam Hoang Hiep"))
				'	oInsert.AppendValue("CreatedDate", New CSQLParamValue(Now))
				'	oInsert.AppendValue("SubmitDate", New CSQLParamValue(Now))
				'	oInsert.AppendValue("LoanType", New CSQLParamValue("CC"))
				'	oInsert.AppendValue("LoanNumber", New CSQLParamValue("24011"))
				'	oInsert.AppendValue("LoanID", New CSQLParamValue("de4f9c7e5010488ca808f337041acb45"))
				'	oInsert.AppendValue("RequestParam", New CSQLParamValue("<VEHICLE_LOAN xmlns=""http://www.meridianlink.com/CLF"" xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" xsi:schemaLocation=""http://www.meridianlink.com/CLF VEHICLE_LOAN.xsd http://www.meridianlink.com/CLF Integration\SSFCU.xsd"" version=""5.070""><APPLICANTS><APPLICANT first_name=""MARISOL"" last_name=""TESTCASE"" suffix=""IV"" middle_name="""" ssn=""324000001"" dob=""1990-11-11"" citizenship=""USCITIZEN"" member_number=""testing1111"" marital_status="""" membership_months=""72"" applicant_type=""P""><ID_CARD card_type=""DRIVERS_LICENSE"" card_number=""5465464"" country="""" state=""DC"" date_issued=""2018-04-02"" exp_date=""2018-04-24"" /><APPLICANT_QUESTIONS><APPLICANT_QUESTION question_name=""app_chk_box"" question_type=""CHECKBOX""><APPLICANT_QUESTION_ANSWER answer_text=""aqcheck"" answer_value=""aqchk"" /></APPLICANT_QUESTION><APPLICANT_QUESTION question_name=""app_text_field"" question_type=""TEXT""><APPLICANT_QUESTION_ANSWER answer_text=""fdsf"" answer_value=""fdsf"" /></APPLICANT_QUESTION></APPLICANT_QUESTIONS><CURRENT_ADDRESS occupancy_status=""RENT"" occupancy_duration=""143"" other_occupancy_description=""""><LOOSE_ADDRESS street_address_1=""1234 SQUARE CIRCLE"" city=""NEWPORT BEACH"" state=""CA"" zip=""92660"" /></CURRENT_ADDRESS><MAILING_ADDRESS is_current=""Y"" /><FINANCIAL_INFO><CURRENT_EMPLOYMENT employer="""" occupation=""FDSF"" employment_business_type="""" supervisor_name="""" pay_grade="""" employment_status=""HO"" other_employment_description="""" /><MONTHLY_INCOME monthly_income_base_salary=""333"" monthly_income_other_1=""0"" monthly_income_other_description_1="""" is_tax_exempt_monthly_income_base_salary=""N"" is_tax_exempt_income_other_1=""N"" /><MONTHLY_DEBT monthly_housing_cost=""3000"" /></FINANCIAL_INFO><REFERENCE /><CONTACT_INFO home_phone=""2015551215"" work_phone="""" work_phone_extension="""" cell_phone="""" email=""MINORDAVID@TEST.COM"" /><ASSETS /></APPLICANT></APPLICANTS><LOAN_INFO loan_term=""4"" request_type=""BALLOON"" total_vehicle_value=""19500"" total_sales_price=""19500"" amount_requested=""19500"" down_payment=""500"" /><LOAN_STATUS loan_status=""PEN"" initial_entry_timestamp=""2018-04-09T09:50:01.0169932+07:00"" is_locked=""N"" /><FUNDING><INSURANCE_OPTIONS loan_class=""C"" /></FUNDING><COMMENTS><DECISION_COMMENTS /><EXTERNAL_COMMENTS /><INTERNAL_COMMENTS>minordavid@test.com (4/9/2018 9:55:30 AM): Disclosure(s) Checked:  Application and Solicitation Disclosure.SYSTEM (4/9/2018 9:55:30 AM): Originating IP Address: 192.168.1.7SYSTEM (4/9/2018 9:55:30 AM): USER AGENT: Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36</INTERNAL_COMMENTS></COMMENTS><VEHICLES><VEHICLE mileage=""0"" is_new_vehicle=""Y"" vehicle_value=""20000"" vehicle_sales_price=""20000"" vehicle_type=""ATV"" year=""2013"" make=""Audi"" model=""Q5"" vin="""" /></VEHICLES><CUSTOM_QUESTIONS><CUSTOM_QUESTION question_name=""Reconstructed Titles"" question_type=""CHECKBOX""><CUSTOM_QUESTION_ANSWER answer_text=""Yes"" answer_value=""Yes"" /></CUSTOM_QUESTION><CUSTOM_QUESTION question_name=""Payment Frequency"" question_type=""DROPDOWN""><CUSTOM_QUESTION_ANSWER answer_text=""Weekly"" answer_value=""Weekly"" /></CUSTOM_QUESTION><CUSTOM_QUESTION question_name=""Payment Protection"" question_type=""DROPDOWN""><CUSTOM_QUESTION_ANSWER answer_text=""Option 2 Disability and Involuntary Unemployment"" answer_value=""Option 2 Disability and Involuntary Unemployment"" /></CUSTOM_QUESTION><CUSTOM_QUESTION question_name=""Payments"" question_type=""DROPDOWN""><CUSTOM_QUESTION_ANSWER answer_text="""" answer_value="""" /></CUSTOM_QUESTION></CUSTOM_QUESTIONS><SYSTEM source=""GATEWAY"" type=""LPQ"" origination_ip=""192.168.1.7"" external_source=""MOBILE WEBSITE""><BRANCH id=""6c4b02f008f049f9b118a35d2159803d"" /><LENDER id=""9031c7f53eed453b987367e26bd37d43"" /><ORGANIZATION id=""f63c948cb9fa4614b1eee603dc2ab94b"" /></SYSTEM><HMDA_XML><ITEM key=""applicant_0_sex_not_provided"" value=""N"" /><ITEM key=""applicant_0_sex"" value="""" /><ITEM key=""applicant_0_ethnicity_not_provided"" value=""N"" /><ITEM key=""applicant_0_ethnicity_is_not_hispanic"" value=""N"" /><ITEM key=""applicant_0_ethnicity_is_hispanic"" value=""N"" /><ITEM key=""applicant_0_ethnicity_hispanic_other"" value="""" /><ITEM key=""applicant_0_ethnicity_hispanic"" value="""" /><ITEM key=""applicant_0_race_asian"" value="""" /><ITEM key=""applicant_0_race_asian_other"" value="""" /><ITEM key=""applicant_0_race_base"" value="""" /><ITEM key=""applicant_0_race_not_provided"" value=""N"" /><ITEM key=""applicant_0_race_pacificislander"" value="""" /><ITEM key=""applicant_0_race_pacificislander_other"" value="""" /><ITEM key=""applicant_0_tribe_name"" value="""" /></HMDA_XML></VEHICLE_LOAN>"))
				'	oInsert.AppendValue("RequestParam_Enc", New CSQLParamValue(""))
				'	oInsert.AppendValue("XmlResponse", New CSQLParamValue("<OUTPUT version=""1.0""><RESPONSE loan_id=""c32e11d0de30460b8ebbb8224e2a41aa"" loan_number=""1672285""><DECISION validation_id=""TTV5posjZgo1E24RD/lhAUD8FcRp9puU2jtYD9KSxw=="" status=""QUALIFIED""><![CDATA[<PRODUCTS><VL_PRODUCT loan_id=""c32e11d0de30460b8ebbb8224e2a41aa"" date_offered=""2018-03-27T14:24:45.4474135-04:00"" is_prequalified=""N"" underwriting_service_results_index=""0"" amount_approved=""10500"" max_amount_approved=""13700"" grade=""Auto - Tier 1"" reference_id="""" tier=""1"" set_loan_system=""C"" is_single_payment=""false"" solve_for=""PAYMENT"" estimated_monthly_payment=""231.54"" term=""48""><RATE rate=""2.750"" original_rate=""2.750"" rate_code="""" margin="""" index="""" rate_floor="""" rate_ceiling="""" rate_type=""F"" rate_adjustment_option=""INTEREST_RATE"" index_type="""" dealer_reserve_rate="""" initial_rate=""0.000"" change_after_days=""0"" /><STIPULATIONS><STIPULATION stip_text=""Stated income must be verifiable if requested"" is_required=""N"" /><STIPULATION stip_text=""No loss to Suncoast Credit Union"" is_required=""Y"" /><STIPULATION stip_text=""Up to 125% loan-to-value of MSRP(New) or NADA(Used)"" is_required=""Y"" /><STIPULATION stip_text=""Open membership with Suncoast Credit Union"" is_required=""Y"" /></STIPULATIONS></VL_PRODUCT></PRODUCTS>]]></DECISION> </RESPONSE> </OUTPUT>"))
				'	oInsert.AppendValue("Status", New CSQLParamValue("QUALIFIED"))
				'	oInsert.AppendValue("StatusDate", New CSQLParamValue(Now))
				'	oInsert.AppendValue("FundStatus", New CSQLParamValue("NOTFUND"))
				'	oInsert.AppendValue("FundDate", New CSQLParamValue(Now))
				'	oDB.executeNonQuery(oInsert.SQL, True)
				'Next

			End If
			paging.TotalRecord = result.Count
			If paging.PageIndex > 0 AndAlso paging.PageSize > 0 Then
				Dim originalResult = result
				result = originalResult.Skip((paging.PageIndex - 1) * paging.PageSize).Take(paging.PageSize).ToList()
				If Not result.Any() Then
					paging.PageIndex = paging.PageIndex - 1
					result = originalResult.Skip((paging.PageIndex - 1) * paging.PageSize).Take(paging.PageSize).ToList()
				End If
			End If
		Catch ex As Exception
			_log.Error("Error while GetLoanList", ex)
		Finally
			oDB.commitTransactionIfOpen()
			oDB.closeConnection()
		End Try
		Return result
	End Function

	Public Function SaveLoan(loan As SmLoan, ipAddress As String) As String
		Dim result As String = ""
		Dim oDB As New CSQLDBUtils(LPQMOBILE_CONNECTIONSTRING)
		Try
			Dim requestParam As String = RemoveSensitiveData(loan.RequestParam)
			Dim encrypter As New CRijndaelManaged()
			If loan.LoanSavingID <> Guid.Empty Then
				Dim oWhere As New CSQLWhereStringBuilder()
				oWhere.AppendAndCondition("ID={0}", New CSQLParamValue(loan.LoanSavingID))
				Dim oUpdate As New cSQLUpdateStringBuilder(LOANS_TABLE, oWhere)
				oUpdate.appendvalue("ApplicantName", New CSQLParamValue(loan.ApplicantName))
				If loan.SubmitDate.HasValue Then
					oUpdate.appendvalue("SubmitDate", New CSQLParamValue(loan.SubmitDate.Value))
				End If
				oUpdate.appendvalue("LoanNumber", New CSQLParamValue(loan.LoanNumber))
				oUpdate.appendvalue("LoanID", New CSQLParamValue(loan.LoanID))
				oUpdate.appendvalue("RequestParam", New CSQLParamValue(requestParam))
				oUpdate.appendvalue("RequestParam_Enc", New CSQLParamValue(encrypter.EncryptUtf8(requestParam)))
				oUpdate.appendvalue("RequestParamJson", New CSQLParamValue(loan.RequestParamJson))
				oUpdate.appendvalue("XmlResponse", New CSQLParamValue(loan.XmlResponse))
				oUpdate.appendvalue("Status", New CSQLParamValue(loan.Status))
				oDB.executeNonQuery(oUpdate.SQL, False)
			Else
				Dim oInsert As New cSQLInsertStringBuilder(LOANS_TABLE)
				oInsert.AppendValue("ID", New CSQLParamValue(Guid.NewGuid()))
				oInsert.AppendValue("LenderID", New CSQLParamValue(loan.LenderID))
				oInsert.AppendValue("LenderRef", New CSQLParamValue(loan.LenderRef))
				oInsert.AppendValue("VendorID", New CSQLParamValue(loan.VendorID))
				oInsert.AppendValue("UserID", New CSQLParamValue(loan.UserID))
				oInsert.AppendValue("ApplicantName", New CSQLParamValue(loan.ApplicantName))
				oInsert.AppendValue("CreatedDate", New CSQLParamValue(loan.CreatedDate))
				If loan.SubmitDate.HasValue Then
					oInsert.AppendValue("SubmitDate", New CSQLParamValue(loan.SubmitDate.Value))
				End If
				oInsert.AppendValue("LoanType", New CSQLParamValue(loan.LoanType.ToString()))
				oInsert.AppendValue("LoanNumber", New CSQLParamValue(loan.LoanNumber))
				oInsert.AppendValue("LoanID", New CSQLParamValue(loan.LoanID))
				oInsert.AppendValue("RequestParam", New CSQLParamValue(requestParam))
				oInsert.AppendValue("RequestParam_Enc", New CSQLParamValue(encrypter.EncryptUtf8(requestParam)))
				oInsert.AppendValue("RequestParamJson", New CSQLParamValue(loan.RequestParamJson))
				oInsert.AppendValue("XmlResponse", New CSQLParamValue(loan.XmlResponse))
				oInsert.AppendValue("Status", New CSQLParamValue(loan.Status))
				If loan.StatusDate.HasValue Then
					oInsert.AppendValue("StatusDate", New CSQLParamValue(loan.StatusDate.Value))
				End If
				oInsert.AppendValue("FundStatus", New CSQLParamValue(loan.FundStatus))
				If loan.FundDate.HasValue Then
					oInsert.AppendValue("FundDate", New CSQLParamValue(loan.FundDate.Value))
				End If
				oDB.executeNonQuery(oInsert.SQL, False)
			End If
			Dim actionLogBL As New SmActionLogBL()
			actionLogBL.WriteActionLog(New SmActionLog() With {.Action = SmActionLogAction.SUBMIT_LOAN, .Category = SmActionLogCategory.LOANS_MANIPULATION, .CreatedByUserID = loan.UserID, .IpAddress = ipAddress, .ObjectID = loan.LoanSavingID.ToString(), .ActionNote = String.Format("Submit loan")})
			result = "ok"
		Catch ex As Exception
			_log.Error("Error while SaveLoan", ex)
			result = "error"
		Finally
			oDB.closeConnection()
		End Try
		Return result
	End Function
	Private Shared Function RemoveSensitiveData(input As String) As String
		'todo: remove sensitive data from input string
		Return input
	End Function
	Public Function SaveInCompletedLoan(loan As SmLoan) As String
		Dim result As String = ""
		Dim oDB As New CSQLDBUtils(LPQMOBILE_CONNECTIONSTRING)
		Try
			If loan.LoanSavingID <> Guid.Empty Then
				Dim oWhere As New CSQLWhereStringBuilder()
				oWhere.AppendAndCondition("ID={0}", New CSQLParamValue(loan.LoanSavingID))
				Dim oDelete As New CSQLDeleteStringBuilder(LOANS_TABLE, oWhere)
				oDB.executeNonQuery(oDelete.SQL, True)
			End If

			Dim encrypter As New CRijndaelManaged()
			Dim requestParam = RemoveSensitiveData(loan.RequestParam)
			Dim oInsert As New cSQLInsertStringBuilder(LOANS_TABLE)
			oInsert.AppendValue("ID", New CSQLParamValue(Guid.NewGuid()))
			oInsert.AppendValue("LenderID", New CSQLParamValue(loan.LenderID))
			oInsert.AppendValue("LenderRef", New CSQLParamValue(loan.LenderRef))
			oInsert.AppendValue("VendorID", New CSQLParamValue(loan.VendorID))
			oInsert.AppendValue("UserID", New CSQLParamValue(loan.UserID))
			oInsert.AppendValue("ApplicantName", New CSQLParamValue(loan.ApplicantName))
			oInsert.AppendValue("CreatedDate", New CSQLParamValue(Now))
			oInsert.AppendValue("LoanType", New CSQLParamValue(loan.LoanType.ToString()))
			oInsert.AppendValue("RequestParam", New CSQLParamValue(requestParam))
			oInsert.AppendValue("RequestParam_Enc", New CSQLParamValue(encrypter.EncryptUtf8(requestParam)))
			oInsert.AppendValue("RequestParamJson", New CSQLParamValue(loan.RequestParamJson))
			oInsert.AppendValue("Status", New CSQLParamValue(loan.Status))
			If loan.StatusDate.HasValue Then
				oInsert.AppendValue("StatusDate", New CSQLParamValue(loan.StatusDate.Value))
			End If
			oInsert.AppendValue("FundStatus", New CSQLParamValue(loan.FundStatus))
			If loan.FundDate.HasValue Then
				oInsert.AppendValue("FundDate", New CSQLParamValue(loan.FundDate.Value))
			End If
			oDB.executeNonQuery(oInsert.SQL, False)
			oDB.commitTransactionIfOpen()
			result = "ok"
		Catch ex As Exception
			_log.Error("Error while SaveInCompletedLoan", ex)
			result = "error"
		Finally
			oDB.closeConnection()
		End Try
		Return result
	End Function

	Public Function GetLoanByID(ID As Guid) As SmLoan
		Dim result As SmLoan = Nothing
		Dim oData As DataTable
		Dim sSQL As String = String.Format("SELECT * FROM {0}  ", LOANS_TABLE)
		Dim oWhere As New CSQLWhereStringBuilder()
		oWhere.AppendAndCondition("ID={0}", New CSQLParamValue(ID))
		Dim oDB As New CSQLDBUtils(LPQMOBILE_CONNECTIONSTRING)
		Try
			oData = oDB.getDataTable(sSQL & oWhere.SQL)
			If oData IsNot Nothing AndAlso oData.Rows.Count > 0 Then
				Dim row As DataRow = oData.Rows(0)
				result = New SmLoan()
				result.LoanSavingID = Common.SafeGUID(row("ID"))
				result.LenderID = Common.SafeGUID(row("LenderID"))
				result.LenderRef = Common.SafeString(row("LenderRef"))
				result.UserID = Common.SafeGUID(row("UserID"))
				result.ApplicantName = Common.SafeString(row("ApplicantName"))
				result.CreatedDate = Common.SafeDate(row("CreatedDate"))
				If Not row.IsNull("SubmitDate") Then
					result.SubmitDate = Common.SafeDate(row("SubmitDate"))
				End If
				result.LoanType = CType([Enum].Parse(GetType(SmSettings.LenderVendorType), Common.SafeString(row("LoanType"))), SmSettings.LenderVendorType)
				result.LoanNumber = Common.SafeString(row("LoanNumber"))
				result.LoanID = Common.SafeString(row("LoanID"))
				result.RequestParam = Common.SafeString(row("RequestParam"))
				result.RequestParam_Enc = Common.SafeString(row("RequestParam_Enc"))
				result.XmlResponse = Common.SafeString(row("XmlResponse"))
				result.Status = Common.SafeString(row("Status"))
				If Not row.IsNull("StatusDate") Then
					result.StatusDate = Common.SafeDate(row("StatusDate"))
				End If
				result.FundStatus = Common.SafeString(row("FundStatus"))
				If Not row.IsNull("FundDate") Then
					result.FundDate = Common.SafeDate(row("FundDate"))
				End If
				result.VendorID = Common.SafeString(row("VendorID"))
				result.RequestParamJson = Common.SafeString(row("RequestParamJson"))
			End If
		Catch ex As Exception
			_log.Error("Error while GetLoanByID", ex)
		Finally
			oDB.closeConnection()
		End Try
		Return result
	End Function
#End Region

#Region "Landing Pages"
	Public Function GetLandingPages(ByRef paging As SmPagingInfoModel, filter As SmLandingPageListFilter) As List(Of SmLandingPageModel)
		Dim result As New List(Of SmLandingPageModel)
		Dim oData As New DataTable()
		Dim sSQL As String = String.Format("SELECT LP.*, U.Email AS 'CreatedByUserWithEmail', U1.Email AS 'LastModifiedByUserWithEmail' FROM {0} LP LEFT JOIN {1} U ON LP.CreatedBy = U.UserID LEFT JOIN {1} U1 ON LP.LastModifiedBy = U1.UserID ", LANDINGPAGES_TABLE, USERS_TABLE)
		Dim oWhere As New CSQLWhereStringBuilder()
		Dim oOrder As String = " "
		If Not String.IsNullOrEmpty(paging.SortColumn) Then
			oOrder = oOrder & "ORDER BY " & paging.SortColumn & " " & paging.SortDirection
		Else
			oOrder = oOrder & "ORDER BY RefID"
		End If
		oWhere.AppendAndCondition("LP.Status <> {0}", New CSQLParamValue(Convert.ToInt16(SmSettings.LandingPageStatus.Deleted)))
		If filter IsNot Nothing Then
			If Not String.IsNullOrEmpty(filter.Keyword) Then
				oWhere.AppendAndCondition("(COALESCE(RefID, '') + '$' + COALESCE(Title, '')) LIKE {0}", New CSQLParamValue("%" & filter.Keyword & "%"))
			End If
			If filter.LenderConfigID <> Guid.Empty Then
				oWhere.AppendAndCondition("LenderConfigID={0}", New CSQLParamValue(filter.LenderConfigID))
			End If

		End If
		Dim oDB As New CSQLDBUtils(LPQMOBILE_CONNECTIONSTRING)
		Try
			oData = oDB.getDataTable(sSQL & oWhere.SQL & oOrder)
			If oData IsNot Nothing AndAlso oData.Rows.Count > 0 Then
				For Each row As DataRow In oData.Rows
					Dim item As New SmLandingPageModel()
					item.LandingPageID = Common.SafeGUID(row("LandingPageID"))
					item.LenderConfigID = Common.SafeGUID(row("LenderConfigID"))
					item.RefID = Common.SafeString(row("RefID"))
					item.Title = Common.SafeString(row("Title"))
					item.PageData = Common.SafeString(row("PageData"))
					item.Status = DirectCast(Common.SafeInteger(row("Status")), SmSettings.LandingPageStatus)
					item.CreatedDate = Common.SafeDate(row("CreatedDate"))
					If Not row.IsNull("LastModifiedDate") Then
						item.LastModifiedDate = Common.SafeDate(row("LastModifiedDate"))
					End If
					item.CreatedBy = Common.SafeGUID(row("CreatedBy"))
					item.CreatedByUserWithEmail = Common.SafeString(row("CreatedByUserWithEmail"))
					If Not row.IsNull("LastModifiedDate") Then
						item.LastModifiedDate = Common.SafeDate(row("LastModifiedDate"))
					End If
					If Not row.IsNull("LastModifiedBy") Then
						item.LastModifiedBy = Common.SafeGUID(row("LastModifiedBy"))
						item.LastModifiedByUserWithEmail = Common.SafeString(row("LastModifiedByUserWithEmail"))
					End If
					result.Add(item)
				Next
			End If
			paging.TotalRecord = result.Count
			If paging.PageIndex > 0 AndAlso paging.PageSize > 0 Then
				Dim originalResult = result
				result = originalResult.Skip((paging.PageIndex - 1) * paging.PageSize).Take(paging.PageSize).ToList()
				If Not result.Any() Then
					paging.PageIndex = paging.PageIndex - 1
					result = originalResult.Skip((paging.PageIndex - 1) * paging.PageSize).Take(paging.PageSize).ToList()
				End If
			End If
		Catch ex As Exception
			_log.Error("Error while GetLandingPages", ex)
		Finally
			oDB.closeConnection()
		End Try
		Return result
	End Function

	Public Function GetLandingPageByRefID(refID As String) As SmLandingPageModel
		Dim result As SmLandingPageModel = Nothing
		Dim oData As DataTable
		Dim sSQL As String = String.Format("SELECT LP.*, U.Email AS 'CreatedByUserWithEmail', U1.Email AS 'LastModifiedByUserWithEmail' FROM {0} LP LEFT JOIN {1} U ON LP.CreatedBy = U.UserID LEFT JOIN {1} U1 ON LP.LastModifiedBy = U1.UserID ", LANDINGPAGES_TABLE, USERS_TABLE)
		Dim oWhere As New CSQLWhereStringBuilder()
		oWhere.AppendAndCondition("RefID={0}", New CSQLParamValue(refID))
		oWhere.AppendAndCondition("LP.Status <> {0}", New CSQLParamValue(Convert.ToInt16(SmSettings.LandingPageStatus.Deleted)))
		Dim oDB As New CSQLDBUtils(LPQMOBILE_CONNECTIONSTRING)
		Try
			oData = oDB.getDataTable(sSQL & oWhere.SQL)
			If oData IsNot Nothing AndAlso oData.Rows.Count > 0 Then
				Dim row As DataRow = oData.Rows(0)
				result = New SmLandingPageModel()
				result.LandingPageID = Common.SafeGUID(row("LandingPageID"))
				result.LenderConfigID = Common.SafeGUID(row("LenderConfigID"))
				result.RefID = Common.SafeString(row("RefID"))
				result.Title = Common.SafeString(row("Title"))
				result.PageData = Common.SafeString(row("PageData"))
				result.Status = DirectCast(Common.SafeInteger(row("Status")), SmSettings.LandingPageStatus)
				result.CreatedDate = Common.SafeDate(row("CreatedDate"))
				If Not row.IsNull("LastModifiedDate") Then
					result.LastModifiedDate = Common.SafeDate(row("LastModifiedDate"))
				End If
				result.CreatedBy = Common.SafeGUID(row("CreatedBy"))
				result.CreatedByUserWithEmail = Common.SafeString(row("CreatedByUserWithEmail"))
				If Not row.IsNull("LastModifiedDate") Then
					result.LastModifiedDate = Common.SafeDate(row("LastModifiedDate"))
				End If
				If Not row.IsNull("LastModifiedBy") Then
					result.LastModifiedBy = Common.SafeGUID(row("LastModifiedBy"))
					result.LastModifiedByUserWithEmail = Common.SafeString(row("LastModifiedByUserWithEmail"))
				End If
			End If
		Catch ex As Exception
			_log.Error("Error while GetLandingPageByRefID", ex)
		Finally
			oDB.closeConnection()
		End Try
		Return result
	End Function
	Public Function GetLandingPageByID(landingPageID As Guid) As SmLandingPageModel
		Dim result As SmLandingPageModel = Nothing
		Dim oData As DataTable
		Dim sSQL As String = String.Format("SELECT LP.*, U.Email AS 'CreatedByUserWithEmail', U1.Email AS 'LastModifiedByUserWithEmail' FROM {0} LP LEFT JOIN {1} U ON LP.CreatedBy = U.UserID LEFT JOIN {1} U1 ON LP.LastModifiedBy = U1.UserID ", LANDINGPAGES_TABLE, USERS_TABLE)
		Dim oWhere As New CSQLWhereStringBuilder()
		oWhere.AppendAndCondition("LandingPageID={0}", New CSQLParamValue(landingPageID))
		oWhere.AppendAndCondition("LP.Status <> {0}", New CSQLParamValue(Convert.ToInt16(SmSettings.LandingPageStatus.Deleted)))
		Dim oDB As New CSQLDBUtils(LPQMOBILE_CONNECTIONSTRING)
		Try
			oData = oDB.getDataTable(sSQL & oWhere.SQL)
			If oData IsNot Nothing AndAlso oData.Rows.Count > 0 Then
				Dim row As DataRow = oData.Rows(0)
				result = New SmLandingPageModel()
				result.LandingPageID = Common.SafeGUID(row("LandingPageID"))
				result.LenderConfigID = Common.SafeGUID(row("LenderConfigID"))
				result.RefID = Common.SafeString(row("RefID"))
				result.Title = Common.SafeString(row("Title"))
				result.PageData = Common.SafeString(row("PageData"))
				result.Status = DirectCast(Common.SafeInteger(row("Status")), SmSettings.LandingPageStatus)
				result.CreatedDate = Common.SafeDate(row("CreatedDate"))
				If Not row.IsNull("LastModifiedDate") Then
					result.LastModifiedDate = Common.SafeDate(row("LastModifiedDate"))
				End If
				result.CreatedBy = Common.SafeGUID(row("CreatedBy"))
				result.CreatedByUserWithEmail = Common.SafeString(row("CreatedByUserWithEmail"))
				If Not row.IsNull("LastModifiedDate") Then
					result.LastModifiedDate = Common.SafeDate(row("LastModifiedDate"))
				End If
				If Not row.IsNull("LastModifiedBy") Then
					result.LastModifiedBy = Common.SafeGUID(row("LastModifiedBy"))
					result.LastModifiedByUserWithEmail = Common.SafeString(row("LastModifiedByUserWithEmail"))
				End If
			End If
		Catch ex As Exception
			_log.Error("Error while GetLandingPageByID", ex)
		Finally
			oDB.closeConnection()
		End Try
		Return result
	End Function

	Public Function AddLandingPage(landingPage As SmLandingPage, createdByUserId As Guid, ipAddress As String) As String
		Dim result As String = ""
		Dim oDB As New CSQLDBUtils(LPQMOBILE_CONNECTIONSTRING)
		Try
			Dim oInsert As New cSQLInsertStringBuilder(LANDINGPAGES_TABLE)
			oInsert.AppendValue("LandingPageID", New CSQLParamValue(landingPage.LandingPageID))
			oInsert.AppendValue("LenderConfigID", New CSQLParamValue(landingPage.LenderConfigID))
			oInsert.AppendValue("RefID", New CSQLParamValue(Common.SafeString(landingPage.RefID)))
			oInsert.AppendValue("Title", New CSQLParamValue(Common.SafeString(landingPage.Title)))
			oInsert.AppendValue("PageData", New CSQLParamValue(Common.SafeString(landingPage.PageData)))
			oInsert.AppendValue("CreatedDate", New CSQLParamValue(Now))
			oInsert.AppendValue("CreatedBy", New CSQLParamValue(Common.SafeString(landingPage.CreatedBy)))
			oInsert.AppendValue("Status", New CSQLParamValue(landingPage.Status))
			oDB.executeNonQuery(oInsert.SQL, False)
			Dim actionLogBL As New SmActionLogBL()
			actionLogBL.WriteActionLog(New SmActionLog() With {.Action = SmActionLogAction.ADD_LANDING_PAGE, .Category = SmActionLogCategory.LANDINGPAGE_MANIPULATION, .CreatedByUserID = createdByUserId, .IpAddress = ipAddress, .ObjectID = landingPage.LandingPageID.ToString(), .ActionNote = String.Format("Add new landing page with RefId {0}", landingPage.RefID)})
			result = "ok"
		Catch ex As Exception
			_log.Error("Error while AddLandingPage", ex)
			result = "error"
		Finally
			oDB.closeConnection()
		End Try
		Return result
	End Function

	Public Function UpdateLandingPage(landingPage As SmLandingPage, createdByUserId As Guid, ipAddress As String) As String
		Dim result As String = ""
		Dim oDB As New CSQLDBUtils(LPQMOBILE_CONNECTIONSTRING)
		Try
			Dim oWhere As New CSQLWhereStringBuilder()
			oWhere.AppendAndCondition("LandingPageID={0}", New CSQLParamValue(landingPage.LandingPageID))

			Dim oUpdate As New cSQLUpdateStringBuilder(LANDINGPAGES_TABLE, oWhere)
			oUpdate.appendvalue("Title", New CSQLParamValue(Common.SafeString(landingPage.Title)))
			oUpdate.appendvalue("RefID", New CSQLParamValue(Common.SafeString(landingPage.RefID)))
			oUpdate.appendvalue("LastModifiedDate", New CSQLParamValue(Now))
			oUpdate.appendvalue("LastModifiedBy", New CSQLParamValue(createdByUserId))
			oDB.executeNonQuery(oUpdate.SQL, False)
			Dim actionLogBL As New SmActionLogBL()
			actionLogBL.WriteActionLog(New SmActionLog() With {.Action = SmActionLogAction.UPDATE_LANDING_PAGE, .Category = SmActionLogCategory.LANDINGPAGE_MANIPULATION, .CreatedByUserID = createdByUserId, .IpAddress = ipAddress, .ObjectID = landingPage.LandingPageID.ToString(), .ActionNote = String.Format("Update landing page")})
			result = "ok"
		Catch ex As Exception
			_log.Error("Error while UpdateLandingPage", ex)
			result = "error"
		Finally
			oDB.closeConnection()
		End Try
		Return result
	End Function

	Public Function ChangeLandingPageStatus(landingPage As SmLandingPage, status As SmSettings.LandingPageStatus, createdByUserId As Guid, ipAddress As String, comment As String) As String
		Dim result As String = ""
		Dim oDB As New CSQLDBUtils(LPQMOBILE_CONNECTIONSTRING)
		Try
			Dim oWhere As New CSQLWhereStringBuilder()
			Dim actionLogBL As New SmActionLogBL()
			oWhere.AppendAndCondition("LandingPageID={0}", New CSQLParamValue(landingPage.LandingPageID))
			Dim oUpdate As New cSQLUpdateStringBuilder(LANDINGPAGES_TABLE, oWhere)
			oUpdate.appendvalue("Status", New CSQLParamValue(status))
			oDB.executeNonQuery(oUpdate.SQL, False)
			actionLogBL.WriteActionLog(New SmActionLog() With {.Action = SmActionLogAction.CHANGE_LANDING_PAGE_STATUS, .Category = SmActionLogCategory.LANDINGPAGE_MANIPULATION, .CreatedByUserID = createdByUserId, .IpAddress = ipAddress, .ObjectID = landingPage.LandingPageID.ToString(), .ActionNote = String.Format("Change status of landing page {0} from {1} to {2} with comment: ""{3}""", landingPage.RefID, landingPage.Status.GetEnumDescription(), status.GetEnumDescription(), comment)})
			result = "ok"
		Catch ex As Exception
			_log.Error("Error while ChangeLandingPageStatus", ex)
			result = "error"
		Finally
			oDB.closeConnection()
		End Try
		Return result
	End Function

	Public Function UpdateLandingPageContent(landingPage As SmLandingPage, createdByUserId As Guid, ipAddress As String) As String
		Dim result As String = ""
		Dim oDB As New CSQLDBUtils(LPQMOBILE_CONNECTIONSTRING)
		Try
			Dim oWhere As New CSQLWhereStringBuilder()
			Dim actionLogBL As New SmActionLogBL()
			oWhere.AppendAndCondition("LandingPageID={0}", New CSQLParamValue(landingPage.LandingPageID))
			Dim oUpdate As New cSQLUpdateStringBuilder(LANDINGPAGES_TABLE, oWhere)
			oUpdate.appendvalue("PageData", New CSQLParamValue(landingPage.PageData))
			oUpdate.appendvalue("LastModifiedBy", New CSQLParamValue(createdByUserId))
			oUpdate.appendvalue("LastModifiedDate", New CSQLParamValue(Now))
			oDB.executeNonQuery(oUpdate.SQL, False)
			actionLogBL.WriteActionLog(New SmActionLog() With {.Action = SmActionLogAction.UPDATE_LANDING_PAGE_CONTENT, .Category = SmActionLogCategory.LANDINGPAGE_MANIPULATION, .CreatedByUserID = createdByUserId, .IpAddress = ipAddress, .ObjectID = landingPage.LandingPageID.ToString(), .ActionNote = String.Format("Update landing page content")})
			result = "ok"
		Catch ex As Exception
			_log.Error("Error while UpdateLandingPageContent", ex)
			result = "error"
		Finally
			oDB.closeConnection()
		End Try
		Return result
	End Function

	Public Function CloneLandingPage(model As SmLandingPage, sourceLandingPage As SmLandingPage, user As SmUser, ipAddr As String) As String
		Dim result As String = ""
		Dim oDB As New CSQLDBUtils(LPQMOBILE_CONNECTIONSTRING)
		Try

			Dim oInsert As New cSQLInsertStringBuilder(LANDINGPAGES_TABLE)
			oInsert.AppendValue("LandingPageID", New CSQLParamValue(model.LandingPageID))
			oInsert.AppendValue("LenderConfigID", New CSQLParamValue(sourceLandingPage.LenderConfigID))
			oInsert.AppendValue("RefID", New CSQLParamValue(model.RefID))
			oInsert.AppendValue("Title", New CSQLParamValue(model.Title))
			oInsert.AppendValue("PageData", New CSQLParamValue(sourceLandingPage.PageData))
			oInsert.AppendValue("Status", New CSQLParamValue(SmSettings.LandingPageStatus.InActive))
			oInsert.AppendValue("CreatedDate", New CSQLParamValue(Now))
			oInsert.AppendValue("CreatedBy", New CSQLParamValue(user.UserID))
			oDB.executeNonQuery(oInsert.SQL, False)
			Dim actionLogBL As New SmActionLogBL()
			actionLogBL.WriteActionLog(New SmActionLog() With {.Action = SmActionLogAction.CLONE_LANDINGPAGE, .Category = SmActionLogCategory.LANDINGPAGE_MANIPULATION, .CreatedByUserID = user.UserID, .IpAddress = ipAddr, .ObjectID = model.LandingPageID.ToString(), .ActionNote = String.Format("Clone landing page ""{0}"" from ""{1}""", model.RefID, sourceLandingPage.RefID)})
			result = "ok"
		Catch ex As Exception
			_log.Error("Error while CloneLandingPage", ex)
			result = "error"
		Finally
			oDB.closeConnection()
		End Try
		Return result

	End Function
#End Region
	'#Region "VendorAgentProfiles"
	'	Public Function GetVendorAgentProfileByUserRoleID(userRoleID As Guid) As SmVendorAgentProfile
	'		Dim result As SmVendorAgentProfile
	'		Dim oData As New DataTable()
	'		'for backward compability, get firstname, lastname...from Users when there is no corresponding record in VendorAgentProfiles table
	'		Dim sSQL As String = String.Format(" SELECT VendorAgentProfileID, U.UserID, COALESCE(VAP.FirstName, U.FirstName) as 'FirstName', COALESCE(VAP.LastName, U.LastName) as 'LastName', COALESCE(VAP.Phone, U.Phone) as 'Phone', COALESCE(VAP.Avatar, U.Avatar) As 'Avatar', VAP.VendorID FROM {0} UR inner join {1} U on UR.UserID = U.UserID left join {2} VAP on UR.UserID = VAP.UserID AND  UR.VendorID = VAP.VendorID", USERROLES_TABLE, USERS_TABLE, VENDORAGENTPROFILES_TABLE)
	'		Dim oWhere As New CSQLWhereStringBuilder()
	'		oWhere.AppendAndCondition("UR.UserRoleID={0}", New CSQLParamValue(userRoleID))
	'		Dim oDB As New CSQLDBUtils(LPQMOBILE_CONNECTIONSTRING)
	'		Try
	'			oData = oDB.getDataTable(sSQL & oWhere.SQL)
	'			If oData IsNot Nothing AndAlso oData.Rows.Count > 0 Then
	'				Dim row As DataRow = oData.Rows(0)
	'				result = New SmVendorAgentProfile()
	'				result.VendorAgentProfileID = Common.SafeGUID(row("VendorAgentProfileID"))
	'				result.UserID = Common.SafeGUID(row("UserID"))
	'				result.FirstName = Common.SafeString(row("FirstName"))
	'				result.LastName = Common.SafeString(row("LastName"))
	'				result.Phone = Common.SafeString(row("Phone"))
	'				result.Avatar = Common.SafeString(row("Avatar"))
	'				result.VendorID = Common.SafeString(row("VendorID"))
	'			Else
	'				result = Nothing
	'			End If
	'		Catch ex As Exception
	'			_log.Error("Error while GetVendorAgentProfileByUserID", ex)
	'		Finally
	'			oDB.closeConnection()
	'		End Try
	'		Return result
	'	End Function


	'	Public Function UpdateVendorAgentProfile(vendorAgentProfile As SmVendorAgentProfile) As String
	'		Dim result As String = ""
	'		Dim oDB As New CSQLDBUtils(LPQMOBILE_CONNECTIONSTRING)
	'		Try
	'			Dim oWhere As New CSQLWhereStringBuilder()
	'			oWhere.AppendAndCondition("UserID={0}", New CSQLParamValue(vendorAgentProfile.UserID))
	'			oWhere.AppendAndCondition("VendorID={0}", New CSQLParamValue(vendorAgentProfile.VendorID))
	'			Dim oDelete As New CSQLDeleteStringBuilder(VENDORAGENTPROFILES_TABLE, oWhere)
	'			oDB.executeNonQuery(oDelete.SQL, True)

	'			Dim oInsert As New cSQLInsertStringBuilder(VENDORAGENTPROFILES_TABLE)
	'			oInsert.AppendValue("VendorAgentProfileID", New CSQLParamValue(vendorAgentProfile.VendorAgentProfileID))
	'			oInsert.AppendValue("UserID", New CSQLParamValue(vendorAgentProfile.UserID))
	'			oInsert.AppendValue("FirstName", New CSQLParamValue(vendorAgentProfile.FirstName))
	'			oInsert.AppendValue("LastName", New CSQLParamValue(vendorAgentProfile.LastName))
	'			oInsert.AppendValue("Phone", New CSQLParamValue(vendorAgentProfile.Phone))
	'			oInsert.AppendValue("Avatar", New CSQLParamValue(vendorAgentProfile.Avatar))
	'			oInsert.AppendValue("VendorID", New CSQLParamValue(vendorAgentProfile.VendorID))
	'			oDB.executeNonQuery(oInsert.SQL, True)
	'			result = "ok"
	'		Catch ex As Exception
	'			_log.Error("Error while UpdateVendorAgentProfile", ex)
	'			result = "error"
	'		Finally
	'			oDB.commitTransactionIfOpen()
	'			oDB.closeConnection()
	'		End Try
	'		Return result
	'	End Function

	'	Public Function RemoveVendorAgentProfile(userID As Guid, vendorID As String) As String
	'		Dim result As String = ""
	'		Dim oDB As New CSQLDBUtils(LPQMOBILE_CONNECTIONSTRING)
	'		Try
	'			Dim oWhere As New CSQLWhereStringBuilder()
	'			oWhere.AppendAndCondition("UserID={0}", New CSQLParamValue(userID))
	'			oWhere.AppendAndCondition("VendorID={0}", New CSQLParamValue(vendorID))
	'			Dim oDelete As New CSQLDeleteStringBuilder(VENDORAGENTPROFILES_TABLE, oWhere)
	'			oDB.executeNonQuery(oDelete.SQL, False)
	'			result = "ok"
	'		Catch ex As Exception
	'			_log.Error("Error while UpdateVendorAgentProfile", ex)
	'			result = "error"
	'		Finally
	'			oDB.closeConnection()
	'		End Try
	'		Return result
	'	End Function
	'#End Region
#Region "Broadcast"
	Public Function GetLatestBroadcast() As String
		Dim result As String = String.Empty
		Dim oData As DataTable
		Dim sSQL As String = String.Format("SELECT Top 1 * FROM {0} ORDER BY CreatedDate DESC", BROADCASTS_TABLE)
		Dim oWhere As New CSQLWhereStringBuilder()
		Dim oDB As New CSQLDBUtils(LPQMOBILE_CONNECTIONSTRING)
		Try
			oData = oDB.getDataTable(sSQL & oWhere.SQL)
			If oData IsNot Nothing AndAlso oData.Rows.Count > 0 Then
				Dim row As DataRow = oData.Rows(0)
				result = Common.SafeString(row("BroadcastContent"))
			End If
		Catch ex As Exception
			_log.Error("Error while GetLatestBroadcast", ex)
		Finally
			oDB.closeConnection()
		End Try
		Return result
	End Function
	Public Function SaveBroadcast(item As SmBroadcast, ipAddress As String) As String
		Dim result As String = ""
		Dim oDB As New CSQLDBUtils(LPQMOBILE_CONNECTIONSTRING)
		Try
			Dim oInsert As New cSQLInsertStringBuilder(BROADCASTS_TABLE)

			oInsert.AppendValue("ID", New CSQLParamValue(item.ID))
			oInsert.AppendValue("CreatedDate", New CSQLParamValue(item.CreatedDate))
			oInsert.AppendValue("CreatedBy", New CSQLParamValue(item.CreatedBy))
			oInsert.AppendValue("BroadcastContent", New CSQLParamValue(item.BroadcastContent))
			oDB.executeNonQuery(oInsert.SQL, False)
			result = "ok"
		Catch ex As Exception
			_log.Error("Error while SaveBroadcast", ex)
			result = "error"
		Finally
			oDB.closeConnection()
		End Try
		Return result
	End Function
#End Region
	Public Function GetEquifaxInstaTouchRecords(startDate As DateTime, endDate As DateTime, lenderRef As String, lenderID As Guid) As List(Of SmEquifaxInstaTouchRecord)
		Dim result As New List(Of SmEquifaxInstaTouchRecord)
		Dim oData As DataTable
		Dim oWhere As New CSQLWhereStringBuilder()
		oWhere.AppendAndCondition("DateRequested >= {0}", New CSQLParamValue(startDate.ToString("yyyy-MM-dd")))
		oWhere.AppendAndCondition("DateRequested < {0}", New CSQLParamValue(endDate.AddDays(1).ToString("yyyy-MM-dd")))
		oWhere.AppendAndCondition("LenderRef = {0}", New CSQLParamValue(lenderRef))
		oWhere.AppendAndCondition("LenderID = {0}", New CSQLParamValue(lenderID))
		Dim oLPQDB As New CSQLDBUtils(LPQMOBILE_CONNECTIONSTRING)
		Dim oLQBDB As New CSQLDBUtils(LQB_CONNECTIONSTRING)
		Try
			oData = oLPQDB.getDataTable("SELECT * FROM EquifaxInstaTouchRecords" & oWhere.SQL)
			If oData IsNot Nothing AndAlso oData.Rows.Count > 0 Then
				For Each row In oData.Rows
					Dim record As New SmEquifaxInstaTouchRecord
					record.ID = Common.SafeString(row("ID"))
					record.RecordType = Common.SafeString(row("RecordType"))
					record.ConsumerFirstName = Common.SafeString(row("ConsumerFirstName"))
					record.ConsumerLastName = Common.SafeString(row("ConsumerLastName"))
					record.DateRequested = Common.SafeDateTimeOffset(row("DateRequested"))
					record.DateResponded = Common.SafeDateTimeOffset(row("DateResponded"))
					record.TransactionID = Common.SafeGUID(row("TransactionID"))
					record.SessionID = Common.SafeString(row("SessionID"))
					record.MerchantId = Common.SafeString(row("MerchantId"))
					record.LenderID = Common.SafeGUID(row("LenderID"))
					record.LenderRef = Common.SafeString(row("LenderRef"))
					record.LoanType = Common.SafeString(row("LoanType"))
					result.Add(record)
				Next
			End If
			oData = oLQBDB.getDataTable("SELECT * FROM EquifaxInstaTouchRecords" & oWhere.SQL)
			If oData IsNot Nothing AndAlso oData.Rows.Count > 0 Then
				For Each row In oData.Rows
					Dim record As New SmEquifaxInstaTouchRecord
					record.ID = Common.SafeString(row("Id"))
					record.RecordType = Common.SafeString(row("RecordType"))
					record.ConsumerFirstName = Common.SafeString(row("ConsumerFirstName"))
					record.ConsumerLastName = Common.SafeString(row("ConsumerLastName"))
					record.DateRequested = Common.SafeDateTimeOffset(row("DateRequested"))
					record.DateResponded = Common.SafeDateTimeOffset(row("DateResponded"))
					record.TransactionID = Common.SafeGUID(row("TransactionId"))
					record.SessionID = Common.SafeString(row("SessionId"))
					record.MerchantId = Common.SafeString(row("MerchantId"))
					record.LenderID = Common.SafeGUID(row("LenderId"))
					record.LenderRef = Common.SafeString(row("LenderRef"))
					record.LoanType = "ML"
					result.Add(record)
				Next
			End If
		Catch ex As Exception
			_log.Error("Error while GetEquifaxInstaTouchRecords", ex)
		Finally
			oLPQDB.closeConnection()
			oLQBDB.closeConnection()
		End Try
		Return result
	End Function
	Public Function GetEquifaxInstaTouchAccountingReportData(timePeriod As DateTime) As List(Of SmEquifaxInstaTouchAccountingReportRecord)
		Dim result As New List(Of SmEquifaxInstaTouchAccountingReportRecord)
		Dim oData As DataTable
		Dim oWhere As New CSQLWhereStringBuilder()
		oWhere.AppendAndCondition("RecordType = {0}", New CSQLParamValue("Prefill"))
		oWhere.AppendAndCondition("DATEPART(month, DateRequested) = {0}", New CSQLParamValue(timePeriod.Month))
		oWhere.AppendAndCondition("DATEPART(year, DateRequested) = {0}", New CSQLParamValue(timePeriod.Year))
		Dim oLPQDB As New CSQLDBUtils(LPQMOBILE_CONNECTIONSTRING)
		Dim oLQBDB As New CSQLDBUtils(LQB_CONNECTIONSTRING)
		Try
			oData = oLPQDB.getDataTable("SELECT CustomerInternalID, LenderName as CustomerName, '' as ItemInternalID, '' as ItemName, COUNT(ID) as Quantity, DATEFROMPARTS(YEAR(DateRequested),MONTH(DateRequested),1) as PeriodDate FROM EquifaxInstaTouchRecords E left join BackOfficeLenders B on E.LenderID = B.LenderID " & oWhere.SQL & " GROUP BY LenderName, CustomerInternalID, DATEFROMPARTS(YEAR(DateRequested),MONTH(DateRequested),1)")
			If oData IsNot Nothing AndAlso oData.Rows.Count > 0 Then
				For Each row In oData.Rows
					Dim record As New SmEquifaxInstaTouchAccountingReportRecord
					record.CustomerInternalID = "00" & Common.SafeString(row("CustomerInternalID"))
					record.CustomerName = Common.SafeString(row("CustomerName"))
					record.ItemInternalID = Common.SafeString(row("ItemInternalID"))
					record.ItemName = Common.SafeString(row("ItemName"))
					record.Quantity = Common.SafeInteger(row("Quantity"))
					record.PeriodDate = Common.SafeDate(row("PeriodDate"))
					result.Add(record)
				Next
			End If
			' This will have to be re-done to get logs from the cloud.
			'oData = oLQBDB.getDataTable("SELECT '' as CustomerInternalID, '' as CustomerName, '' as ItemInternalID, '' as ItemName, LenderId, COUNT(Id) as Quantity, DATEFROMPARTS(YEAR(DateRequested),MONTH(DateRequested),1) as PeriodDate FROM EquifaxInstaTouchRecords " & oWhere.SQL & " GROUP BY LenderId, DATEFROMPARTS(YEAR(DateRequested),MONTH(DateRequested),1)")
			'If oData IsNot Nothing AndAlso oData.Rows.Count > 0 Then
			'	Dim backOfficeLenderList = GetAllBackOfficeLenders()
			'	For Each row In oData.Rows
			'		Dim record As New SmEquifaxInstaTouchAccountingReportRecord
			'		record.ItemInternalID = Common.SafeString(row("ItemInternalID"))
			'		record.ItemName = Common.SafeString(row("ItemName"))
			'		record.Quantity = Common.SafeInteger(row("Quantity"))
			'		record.PeriodDate = Common.SafeDate(row("PeriodDate"))
			'		If backOfficeLenderList IsNot Nothing AndAlso backOfficeLenderList.Count > 0 Then
			'			Dim backOfficeLender = backOfficeLenderList.Find(Function(b) b.LenderID = Common.SafeGUID(row("LenderId")))
			'			If backOfficeLender IsNot Nothing Then
			'				record.CustomerInternalID = "03" & backOfficeLender.CustomerInternalID
			'				record.CustomerName = backOfficeLender.LenderName
			'			End If
			'		Else
			'			record.CustomerInternalID = Common.SafeString(row("CustomerInternalID"))
			'			record.CustomerName = Common.SafeString(row("CustomerName"))
			'		End If
			'		result.Add(record)
			'	Next
			'End If
		Catch ex As Exception
			_log.Error("Error while GetEquifaxInstaTouchAccountingReportData", ex)
		Finally
			oLPQDB.closeConnection()
			oLQBDB.closeConnection()
		End Try
		Return result
	End Function
	Public Function SearchMortgageLoan(ByRef paging As SmPagingInfoModel, filter As SmGeneralListFilter) As SmMortgageLoanSearchResult
		Try
			Dim req As WebRequest = WebRequest.Create(String.Format("{0}/Service/GetLoanConsumers?lenderref={1}&loanNumber={2}", Common.LQB_WEBSITE, filter.LenderRef, filter.Keyword))
			req.Method = "POST"
			req.ContentType = "application/json"
			req.ContentLength = 0
			Dim responseText As String
			Using resp As WebResponse = req.GetResponse()
				Using stream As Stream = resp.GetResponseStream()
					Dim reader As New StreamReader(stream)
					responseText = reader.ReadToEnd()
				End Using
				resp.Close()
			End Using
			'for testing
			'responseText = "{""IsSuccess"":true,""Message"":null,""Data"":{""LoanNumber"":""TEST2019050002"",""Consumers"":[{""IsPrimaryConsumer"":true,""FirstName"":""Marisol"",""LastName"":""Testcase"",""Phone"":""2223334444"",""Email"":""jamesl@meridianlink.com""},{""IsPrimaryConsumer"":false,""FirstName"":""David"",""LastName"":""Testcase"",""Phone"":""2223334444"",""Email"":""jamesl@meridianlink.com""},{""IsPrimaryConsumer"":false,""FirstName"":""Janet"",""LastName"":""Testcase"",""Phone"":""2223334444"",""Email"":""jamesl@meridianlink.com""},{""IsPrimaryConsumer"":false,""FirstName"":""Joseph"",""LastName"":""Testcase"",""Phone"":""223334444"",""Email"":""jamesl@meridianlink.com""}]}}"
			If Not String.IsNullOrEmpty(responseText) Then
				Dim resultObj = JsonConvert.DeserializeObject(Of SmApiStatusViewModel(Of SmMortgageLoanSearchResult))(responseText)
				If resultObj IsNot Nothing AndAlso resultObj.IsSuccess Then
					Dim result = resultObj.Data
					If result IsNot Nothing AndAlso result.Consumers IsNot Nothing AndAlso result.Consumers.Count > 0 Then
						paging.TotalRecord = result.Consumers.Count
						If paging.PageIndex > 0 AndAlso paging.PageSize > 0 Then
							Dim originalResult = result.Consumers
							result.Consumers = originalResult.Skip((paging.PageIndex - 1) * paging.PageSize).Take(paging.PageSize).ToList()
							If Not result.Consumers.Any() Then
								paging.PageIndex = paging.PageIndex - 1
								result.Consumers = originalResult.Skip((paging.PageIndex - 1) * paging.PageSize).Take(paging.PageSize).ToList()
							End If
						End If
					End If
					Return result
				End If
			End If
		Catch ex As Exception
			_log.Error("Search Mortgage Loan error", ex)
		End Try
		Return New SmMortgageLoanSearchResult
	End Function


	Public Function GenerateMLSearchToken(postData As String, lenderRef As String) As KeyValuePair(Of String, String)
		Try
			Dim req As WebRequest = WebRequest.Create(String.Format("{0}/Service/GetToken?lenderref={1}", Common.LQB_WEBSITE, lenderRef))
			req.Method = "POST"
			req.ContentType = "application/json"
			req.Headers.Add(HttpRequestHeader.Authorization, "Basic " + Convert.ToBase64String(Encoding.UTF8.GetBytes(Common.LQB_WEBSITE_GETTOKEN_USER + ":" + Common.LQB_WEBSITE_GETTOKEN_PASSWORD)))
			Dim responseText As String
			Using requestStream = req.GetRequestStream()
				Dim writer As New StreamWriter(requestStream)
				writer.Write(postData)
				writer.Close()
				Using resp As WebResponse = req.GetResponse()
					Using stream As Stream = resp.GetResponseStream()
						Dim reader As New StreamReader(stream)
						responseText = reader.ReadToEnd()
					End Using
					resp.Close()
				End Using
				requestStream.Close()
			End Using
			'for testing
			'responseText = "{""Token"": ""ffffffff"", ""SignInURL"": ""https://denovu.atlassian.net/browse/APP-124""}"
			If Not String.IsNullOrEmpty(responseText) Then
				Dim resultObj = JsonConvert.DeserializeAnonymousType(responseText, New With {.Token = "", .SignInURL = ""})
				If resultObj IsNot Nothing Then
					Return New KeyValuePair(Of String, String)(resultObj.Token, resultObj.SignInURL)
				End If
			End If
		Catch ex As Exception
			_log.Error("GenerateMLSearchToken error", ex)
		End Try
		Return New KeyValuePair(Of String, String)("", "")
	End Function

	Private Shared Sub InvalidateMortgagePOSCachedPortalConfig(lenderref As String)
		Try
			Dim req As WebRequest = WebRequest.Create(String.Format("{0}/Service/InvalidateConfigCache?lenderref={1}", Common.LQB_WEBSITE, lenderref))
			req.Method = "POST"
			req.ContentType = "application/json"
			req.Headers.Add(HttpRequestHeader.Authorization, "Basic " + Convert.ToBase64String(Encoding.UTF8.GetBytes(Common.LQB_WEBSITE_GETTOKEN_USER + ":" + Common.LQB_WEBSITE_GETTOKEN_PASSWORD)))
			Dim responseText As String
			Using requestStream = req.GetRequestStream()
				Dim writer As New StreamWriter(requestStream)
				writer.Write("")
				writer.Close()
				Using resp As WebResponse = req.GetResponse()
					Using stream As Stream = resp.GetResponseStream()
						Dim reader As New StreamReader(stream)
						responseText = reader.ReadToEnd()
					End Using
					resp.Close()
				End Using
				requestStream.Close()
			End Using

			If String.IsNullOrEmpty(responseText) Then
				Throw New Exception("No response text")
			End If
			Dim resultObj = JsonConvert.DeserializeAnonymousType(responseText, New With {.IsSuccess = False, .Message = ""})
			If resultObj Is Nothing Then
				Throw New Exception("Invalid response text: " & responseText)
			End If
			If Not resultObj.IsSuccess Then
				Throw New Exception("Failed to invalidate cache. Message: " & resultObj.Message)
			End If

			_log.Info("Cleared MortgagePOS lender config cache for lenderref " & lenderref)
		Catch ex As Exception
			_log.Error("InvalidateMortgagePOSCachedPortalConfig error", ex)
		End Try
	End Sub

	Public Shared Sub InvalidateExternallyCachedPortalConfigs(lenderref As String, newConfigData As String)
		' Wrap this in a try/catch to avoid this disrupting normal operations.
		Try
			Dim configXmlDoc = New XmlDocument()
			configXmlDoc.LoadXml(newConfigData)
			Dim enableMortgageLoans = configXmlDoc.SelectSingleNode("WEBSITE/LQB") IsNot Nothing

			' 1. Invalidate on AP Mortgage
			' Since AP Mortgage only cares if the lender is enabled for Mortgage Loans, only attempt to clear
			' AP Mortgage's config cache for this lenderref if it's enabled for Mortgage Loans.
			If enableMortgageLoans Then
				SmBL.InvalidateMortgagePOSCachedPortalConfig(lenderref)
			End If

			' 2. ...
		Catch ex As Exception
			_log.Error("Error attempting to invalidate external config for lenderref " & lenderref, ex)
		End Try
	End Sub
End Class
