﻿Imports Microsoft.VisualBasic
Imports LPQMobile.Utils

Public Class SmLandingPage
	Public Property LandingPageID As Guid
	Public Property LenderConfigID As Guid

	Public Property VendorID As String
	Public Property RefID As String
	Public Property Title As String
	Public Property PageData As String
	Public Property Status As SmSettings.LandingPageStatus
	Public Property CreatedDate As DateTime
	Public Property LastModifiedDate As Nullable(Of DateTime)
	Public Property CreatedBy As Guid
	Public Property LastModifiedBy As Nullable(Of Guid)

	Public Function MakeItSafe() As SmLandingPage
		Me.Title = Common.SafeStripHtmlString(Me.Title)
		Me.VendorID = Common.SafeStripHtmlString(Me.VendorID)
		Return Me
	End Function
End Class
