﻿Imports Microsoft.VisualBasic

Public Class SmLPQUser

	Public Property LPQUserId As Guid
	Public Property OrganizationID As Guid
	Public Property LenderID As Guid
	Public Property LName As String
	Public Property FName As String
	Public Property Email As String
	Public Property Phone As String
	Public Property LastLoginDate As DateTime
	Public Property Host As String
End Class
