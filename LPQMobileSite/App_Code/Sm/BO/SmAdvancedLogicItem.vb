﻿Imports Microsoft.VisualBasic

Public Class SmAdvancedLogicItem
	Public Property TargetItem As String
	Public Property TargetText As String
	Public Property Expression As String
	Public Property TargetType As String
	Public Property Conditions As List(Of SmConditionItem)
End Class
