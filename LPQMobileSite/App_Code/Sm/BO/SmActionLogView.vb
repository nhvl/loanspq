﻿Imports Microsoft.VisualBasic

Public Class SmActionLogView
	Public Property TransactionDate As DateTime
	Public Property ActionBy As String
	Public Property IpAddress As String
	Public Property Category As String
	Public Property Action As String
	Public Property ActionNote As String
End Class
