﻿Imports Microsoft.VisualBasic
Imports LPQMobile.Utils

Public Class SmChangePasswordModel

	Public Property CurrentPassword As String
	Public Property NewPassword As String
	Public Property ConfirmNewPassword As String
	Public Property UserID As Guid

	Public Function MakeItSafe() As SmChangePasswordModel
		Me.CurrentPassword = Common.SafeStripHtmlString(Me.CurrentPassword)
		Me.NewPassword = Common.SafeStripHtmlString(Me.NewPassword)
		Me.ConfirmNewPassword = Common.SafeStripHtmlString(Me.ConfirmNewPassword)
		Return Me
	End Function

End Class
