﻿<Serializable()>
Public Class SmActionLog
	Public Property ID As Integer
	Public Property TransactionDate As DateTime
	Public Property CreatedByUserID As Guid
	Public Property IpAddress As String
	Public Property Category As String
	Public Property Action As String
	Public Property ActionNote As String
	Public Property ObjectID As String
End Class
