﻿Imports Microsoft.VisualBasic
Imports LPQMobile.Utils

Public Class SmGeneralListFilter
	Public Property Keyword As String
	Public Property LenderRef As String


	Public Function MakeItSafe() As SmGeneralListFilter
		Me.Keyword = Common.SafeStripHtmlString(Me.Keyword)
		Me.LenderRef = Common.SafeStripHtmlString(Me.LenderRef)
		Return Me
	End Function
End Class
