﻿Imports Microsoft.VisualBasic
Imports LPQMobile.Utils

Public Class SmBackOfficeLender

	Public Property BackOfficeLenderID As Guid
	Public Property LenderName As String
	Public Property CustomerInternalID As String
	Public Property LenderID As Guid
	Public Property LpqUrl As String
	Public Property City As String
	Public Property State As String
	Public Property LastModifiedDate As DateTime?
	Public Property LastModifiedByUserID As Guid
	Public Property LastModifiedByUserName As String
	Public Property Host As String
	Public Property PortalList As List(Of SmLenderConfig)

	Public Function MakeItSafe() As SmBackOfficeLender
		Me.LenderName = Common.SafeStripHtmlString(Me.LenderName)
		Me.CustomerInternalID = Common.SafeStripHtmlString(Me.CustomerInternalID)
		Me.LpqUrl = Common.SafeStripHtmlString(Me.LpqUrl)
		Me.City = Common.SafeStripHtmlString(Me.City)
		Me.State = Common.SafeStripHtmlString(Me.State)
		Me.Host = Common.SafeStripHtmlString(Me.Host)
		Return Me
	End Function
End Class
