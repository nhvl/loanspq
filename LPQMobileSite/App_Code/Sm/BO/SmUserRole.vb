﻿Imports Microsoft.VisualBasic
Imports LPQMobile.Utils

Public Class SmUserRole
	Public Property UserRoleID As Guid
	Public Property LenderID As Guid
	Public Property LenderConfigID As Guid
	Public Property VendorID As String
	Public Property Vendor As SmLenderVendor
	Public Property UserID As Guid
	Public Property Role As SmSettings.Role
	Public Property Status As SmSettings.UserRoleStatus
	Public Property User As SmUser
	Public Property CreatedDate As DateTime

	Public Property AssignedVendorGroupByTags As String

	Public ReadOnly Property AssignedVendorGroupByTagList() As List(Of String)
		Get
			Dim ret As New List(Of String)
			If Not String.IsNullOrWhiteSpace(AssignedVendorGroupByTags) Then
				ret = AssignedVendorGroupByTags.Split(","c).ToList()
			End If
			Return ret
		End Get
	End Property


	Public Function MakeItSafe() As SmUserRole
		Me.VendorID = Common.SafeStripHtmlString(Me.VendorID)
		Return Me
	End Function
End Class
