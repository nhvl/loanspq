﻿Imports Microsoft.VisualBasic

Public Class SmCustomListFilterItem
	Public Property id As String
	Public Property label As String
	Public Property type As String
	Public Property input As String
	Public Property values As Object
	Public Property operators As String
	Public Property multiple As String
	Public Property optgroup As String
End Class
