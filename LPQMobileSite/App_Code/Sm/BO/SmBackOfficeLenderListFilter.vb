﻿Imports Microsoft.VisualBasic
Imports LPQMobile.Utils

<Serializable()>
Public Class SmBackOfficeLenderListFilter
	Public Property Keyword As String


	Public Function MakeItSafe() As SmBackOfficeLenderListFilter
		Me.Keyword = Common.SafeStripHtmlString(Me.Keyword)
		Return Me
	End Function
End Class
