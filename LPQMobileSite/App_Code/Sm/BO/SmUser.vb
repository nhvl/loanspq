﻿Imports Microsoft.VisualBasic
Imports LPQMobile.Utils

<Serializable()> _
Public Class SmUser

	Public Property UserID As Guid
	Public Property Email As String
	Public Property Password As String
	Public Property Salt As String
	Public Property CreatedDate As DateTime
	Public Property LastLoginDate As Nullable(Of DateTime)
	Public Property Status As SmSettings.UserStatus
	Public Property Role As SmSettings.Role
	Public Property FirstName As String
	Public Property LastName As String
	Public Property Phone As String
	Public Property LoginFailedCount As Integer
	Public Property ExpireDays As Integer
	Private _avatar As String = ""
	Public Property Avatar As String
		Get
			Return _avatar
		End Get
		Set(value As String)
			_avatar = Common.CompileAvatarUrl(value)
		End Set
	End Property
	Public ReadOnly Property DaysTillExpire As Integer
		Get
			If LastLoginDate.HasValue Then
				Dim result As Integer = ExpireDays - CInt(DateDiff(DateInterval.Day, LastLoginDate.Value, Now))
				Return Math.Max(result, 0)
			Else
				Return ExpireDays
			End If
		End Get
	End Property

	Public ReadOnly Property FullName As String
		Get
			Return FirstName & " " & LastName
		End Get
	End Property


	Public Sub New()
		UserID = Guid.Empty
		'SSOSessionID = Guid.Empty
	End Sub
	Public Function MakeItSafe() As SmUser
		Me.Email = Common.SafeStripHtmlString(Me.Email)
		Me.Password = Common.SafeStripHtmlString(Me.Password)
		Me.FirstName = Common.SafeStripHtmlString(Me.FirstName)
		Me.LastName = Common.SafeStripHtmlString(Me.LastName)
		Me.Phone = Common.SafeStripHtmlString(Me.Phone)
		Me.Avatar = Common.SafeStripHtmlString(Me.Avatar)
		Return Me
	End Function

	'Public Property SSOSessionID As Guid
	Public Property SSOAccessRights As SmSSOAccessRight
	Public Property SSOWorkspaceLoanOfficerCode As String

	Public Property SSOEmail As String
	Public Property IsSSOEmailConfirmed As String

End Class
