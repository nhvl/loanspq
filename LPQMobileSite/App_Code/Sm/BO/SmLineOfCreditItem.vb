﻿Imports Newtonsoft.Json.Converters
Imports Newtonsoft.Json
<Serializable()> _
Public Class SmLineOfCreditItem
	Public Property Text As String
	Public Property Value As String
	<JsonConverter(GetType(StringEnumConverter))>
	Public Property Category As CEnum.LocCategory
	<JsonConverter(GetType(StringEnumConverter))>
	Public Property OverDraftOption As CEnum.LocOverDraftOption
End Class
