﻿Imports Microsoft.VisualBasic
Imports iTextSharp.text.pdf.qrcode
Imports LPQMobile.Utils

<Serializable()>
Public Class SmPagingInfoModel

	Public Property PageIndex As Integer
	Private _pageSize As Integer = 10

	Public Property PageSize As Integer
		Get
			Return _pageSize
		End Get
		Set(value As Integer)
			_pageSize = value
		End Set
	End Property

	Private _sortColumn As String = ""

	Public Property SortColumn() As String
		Get
			Return _sortColumn
		End Get
		Set(value As String)
			_sortColumn = value
		End Set
	End Property

	Private _sortDirection As String = ""
	Public Property SortDirection() As String
		Get
			Return _sortDirection
		End Get
		Set(value As String)
			_sortDirection = value
		End Set
	End Property

	Private _gridInstanceName As String = ""
	Public Property GridInstanceName() As String
		Get
			Return _gridInstanceName
		End Get
		Set(value As String)
			_gridInstanceName = value
		End Set
	End Property

	Public Property TotalRecord As Integer

	Public ReadOnly Property TotalPage As Integer
		Get
			If TotalRecord <= PageSize Then
				Return 1
			ElseIf TotalRecord Mod PageSize > 0 Then
				Return CInt((TotalRecord - TotalRecord Mod PageSize) / PageSize) + 1
			Else
				Return CInt(TotalRecord / PageSize)
			End If
		End Get
	End Property

	Public Function MakeItSafe() As SmPagingInfoModel
		Me.SortDirection = Common.SafeStripHtmlString(Me.SortDirection)
		Me.SortColumn = Common.SafeStripHtmlString(Me.SortColumn)
		Return Me
	End Function
End Class
