﻿Imports Microsoft.VisualBasic

Public Class SmThemeEditorModel
	Public Property LogoUrl As String
	Public Property ButtonColor As String
	Public Property ButtonFontColor As String
	Public Property FooterColor As String
	Public Property FooterFontColor As String
	Public Property HeaderColor As String
	Public Property Header2Color As String
	Public Property BackgroundColor As String
	Public Property ContentColor As String

	Public Property FavIco As String
End Class
