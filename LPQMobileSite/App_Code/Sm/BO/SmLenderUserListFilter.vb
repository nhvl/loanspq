﻿Imports Microsoft.VisualBasic
Imports LPQMobile.Utils

Public Class SmLenderUserListFilter
	Public Property Keyword As String
	Public Property LenderID As Guid
	Public Property PortalID As Guid
	Public Property VendorID As String

	Private _status As List(Of SmSettings.UserStatus)
	Private _roles As List(Of SmSettings.Role)

	Public Property Status As List(Of SmSettings.UserStatus)
		Get
			Return _status
		End Get
		Set(value As List(Of SmSettings.UserStatus))
			_status = value
		End Set
	End Property

	Public Property Roles As List(Of SmSettings.Role)
		Get
			Return _roles
		End Get
		Set(value As List(Of SmSettings.Role))
			_roles = value
		End Set
	End Property
	Public Sub New()
		LenderID = Guid.Empty
		PortalID = Guid.Empty
		VendorID = String.Empty
		_roles = New List(Of SmSettings.Role)
		_status = New List(Of SmSettings.UserStatus)
	End Sub


	Public Function MakeItSafe() As SmLenderUserListFilter
		Me.Keyword = Common.SafeStripHtmlString(Me.Keyword)
		Return Me
	End Function
End Class
