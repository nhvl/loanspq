﻿Imports LPQMobile.Utils

Public Class SmActionLogListFilter

	Public Property Keyword As String
	Public Property IPAddress As String
	Public Property FromDate As Date?
	Public Property ToDate As Date?
	Public Property Action As String
	Public Property Category As String

	Public Function MakeItSafe() As SmActionLogListFilter
		Me.IPAddress = Common.SafeString(Me.IPAddress)
        Me.Keyword = Common.SafeString(Me.Keyword).NullSafeToLower_
        Return Me
	End Function
End Class
