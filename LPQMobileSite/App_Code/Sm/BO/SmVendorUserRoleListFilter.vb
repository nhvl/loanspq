﻿Imports Microsoft.VisualBasic
Imports LPQMobile.Utils

Public Class SmVendorUserRoleListFilter
	Public Property Keyword As String
	Public Property LenderConfigID As Guid

	Public Property Roles As List(Of SmSettings.Role)
	Public Property VendorIDList As List(Of String)

	Public Property FilterByTags As Boolean
	Public Property TagList As List(Of String)

	Public Sub New()
		LenderConfigID = Guid.Empty
		Roles = New List(Of SmSettings.Role)
		VendorIDList = New List(Of String)
		TagList = New List(Of String)
		FilterByTags = False
	End Sub

	Public Function MakeItSafe() As SmVendorUserRoleListFilter
		Me.Keyword = Common.SafeStripHtmlString(Me.Keyword)
		Return Me
	End Function
End Class
