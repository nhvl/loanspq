﻿Imports Microsoft.VisualBasic

Public Class SmUserChangePasswordLogItem

	Public Property ID As Guid
	Public Property UserID As Guid
	Public Property CreatedDate As DateTime
	Public Property Password As String
	Public Property Salt As String
End Class
