﻿Imports Microsoft.VisualBasic

Public Class SmConditionItem

	Public Property Type As String
	Public Property Name As String
	Public Property Text As String
	Public Property Comparer As String
	Public Property Value As String
	Public Property LogicalOperator As String
	Public Property LogicalIndent As Integer
End Class
