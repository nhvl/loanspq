﻿Imports Microsoft.VisualBasic

Namespace Sm.BO
	Public Class SmLenderConfigDraft

		Public Property ID As Integer
		Public Property LenderConfigID As Guid
		Public Property CreatedDate As DateTime
		Public Property CreatedByUserID As Guid
		Public Property NodePath As String
		Public Property NodeContent As String
		Public Property Status As String
		Public Property NodeGroup As SmSettings.NodeGroup
		Public Property RevisionID As Integer

	End Class
End Namespace
