﻿
Imports System.ComponentModel

Public Class SmEquifaxInstaTouchAccountingReportRecord
	<DisplayName("Customer Internal ID")>
	Public Property CustomerInternalID As String
	<DisplayName("Customer Name")>
	Public Property CustomerName As String
	<DisplayName("Item Internal ID")>
	Public Property ItemInternalID As String
	<DisplayName("Item Name")>
	Public Property ItemName As String
	<DisplayName("Quantity")>
	Public Property Quantity As Integer
	<DisplayName("Date")>
	Public Property PeriodDate As DateTime
End Class
