﻿Imports Microsoft.VisualBasic
Imports LPQMobile.Utils

<Serializable()>
Public Class SmUserListFilter

	Private _status As List(Of SmSettings.UserStatus)
	Private _roles As List(Of SmSettings.Role)

	Public Property Status As List(Of SmSettings.UserStatus)
		Get
			Return _status
		End Get
		Set(value As List(Of SmSettings.UserStatus))
			_status = value
		End Set
	End Property

	Public Property Roles As List(Of SmSettings.Role)
		Get
			Return _roles
		End Get
		Set(value As List(Of SmSettings.Role))
			_roles = value
		End Set
	End Property

	Public Property Keyword As String
	Public Sub New()
		_roles = New List(Of SmSettings.Role)
		_status = New List(Of SmSettings.UserStatus)
	End Sub


	Public Function MakeItSafe() As SmUserListFilter
		Me.Keyword = Common.SafeStripHtmlString(Me.Keyword)
		Return Me
	End Function
End Class
