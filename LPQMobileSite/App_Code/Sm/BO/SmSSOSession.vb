﻿Imports Microsoft.VisualBasic
Imports LPQMobile.Utils

Public Class SmSSOSession
	Public Property SSOSesionID As Guid
	Public Property OrganizationCode As String
	Public Property LenderCode As String
	Public Property LoanOfficerCode As String
	Public Property Login As String
	Public Property FirstName As String
	Public Property MiddleName As String
	Public Property LastName As String
	Public Property Email As String
	Public Property CreatedDate As DateTime
	Public Property AccessRights As SmSSOAccessRight
	Public Property WorkingUrl As String
	Public ReadOnly Property WorkspaceLoanOfficerCode As String
		Get
			' don't trim https:// in WorkingUrl as discussed because it's maybe useful later, we can extract working url when needed
			Return Common.SafeString(WorkingUrl) & "/" & Common.SafeString(LoanOfficerCode)
		End Get
	End Property
End Class
