﻿Imports Microsoft.VisualBasic

Public Class SmApiStatusViewModel(Of T)
	Public Property IsSuccess As Boolean
	Public Property Message As String
	Public Property Data As T
End Class
