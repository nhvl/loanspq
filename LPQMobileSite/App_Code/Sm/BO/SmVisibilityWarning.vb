﻿Imports Microsoft.VisualBasic
Imports Newtonsoft.Json
Imports Newtonsoft.Json.Converters

Public Class SmVisibilityWarning

	Public Enum WarningTrigger
		Visible
		Hidden
		Any
	End Enum

	Public Property WarningText As String
	<JsonConverter(GetType(StringEnumConverter))>
	Public Property Trigger As WarningTrigger

End Class
