﻿Imports Microsoft.VisualBasic
<Serializable()>
Public Class SmUserActivateToken
	Public Property Token As String
	Public Property UserID As Guid
	Public Property CreatedDate As DateTime
	Public Property Email As String
	Public Property Status As SmSettings.TokenStatus

	Public Property ActivatedDate As Nullable(Of DateTime)
End Class
