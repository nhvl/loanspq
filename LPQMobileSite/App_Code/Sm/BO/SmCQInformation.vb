﻿Imports System.Xml
Imports Microsoft.VisualBasic

Public Class SmCQInformation
    Public Property Question As String
    Public Property IsEnable As Boolean
    Public Property DisplayPage As String
    Public Shared ReadOnly FirstDisplayPage As String = "FirstPage"
    Public Shared ReadOnly DefaultDisplayPage As String = "DefaultPage"
    Public Shared ReadOnly HideAllQuestionKey As String = "bogus_for_hiding_all_question"
    Public Sub New()
    End Sub
    ''' <summary>
    ''' Show All: all questions have is_enable = false
    ''' Hide All: question has name "bogus_for_hiding_all_question"
    ''' Other cases: questions may have is_enable true or false, 
    ''' </summary>
    ''' <param name="questionList"></param>
    ''' <returns></returns>
    Public Shared Function IsShowAll(ByVal questionList As List(Of SmCQInformation)) As Boolean
        Return questionList Is Nothing OrElse Not questionList.Any(Function(q) q.IsEnable)
    End Function
    Public Shared Function IsHideAll(ByVal questionList As List(Of SmCQInformation)) As Boolean
        Return questionList IsNot Nothing AndAlso questionList.Any(Function(q) q.Question = HideAllQuestionKey)
    End Function
End Class
