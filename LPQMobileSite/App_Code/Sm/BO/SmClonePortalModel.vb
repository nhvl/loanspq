﻿Imports Microsoft.VisualBasic
Imports LPQMobile.Utils

Public Class SmClonePortalModel
	Public Property LenderConfigID As Guid
	Public Property PortalName As String
	Public Property LenderRef As String
	Public Property ApiLogin As String
	Public Property ApiPassword As String
	Public Property Login As String
	Public Property Password As String


	Public Function MakeItSafe() As SmClonePortalModel
		Me.ApiLogin = Common.SafeStripHtmlString(Me.ApiLogin)
		Me.ApiPassword = Common.SafeStripHtmlString(Me.ApiPassword)
		Me.Login = Common.SafeStripHtmlString(Me.Login)
		Me.Password = Common.SafeStripHtmlString(Me.Password)
		Me.PortalName = Common.SafeStripHtmlString(Me.PortalName)
		Me.LenderRef = Common.SafeStripHtmlString(Me.LenderRef)
		Return Me
	End Function
End Class
