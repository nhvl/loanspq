﻿Imports Microsoft.VisualBasic
Imports LPQMobile.Utils

Public Class SmPortalUserRoleListFilter
	Public Property Keyword As String
	Public Property LenderConfigID As Guid
	Public Property LenderID As Guid
	Public Sub New()
		LenderConfigID = Guid.Empty
		LenderID = Guid.Empty
	End Sub

	Public Function MakeItSafe() As SmPortalUserRoleListFilter
		Me.Keyword = Common.SafeStripHtmlString(Me.Keyword)
		Return Me
	End Function
End Class
