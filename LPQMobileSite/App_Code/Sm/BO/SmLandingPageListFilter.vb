﻿Imports Microsoft.VisualBasic
Imports LPQMobile.Utils

Public Class SmLandingPageListFilter
	Public Property Keyword As String
	Public Property LenderConfigID As Guid
	Public Function MakeItSafe() As SmLandingPageListFilter
		Me.Keyword = Common.SafeStripHtmlString(Me.Keyword)
		Return Me
	End Function
End Class
