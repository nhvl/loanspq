﻿Imports Microsoft.VisualBasic
Imports LPQMobile.Utils

Public Class SmLenderVendor
	Public Property LenderVendorID As Guid
	Public Property LenderID As Guid
	Public Property VendorID As String
	Public Property VendorName As String
	Public Property Zip As String
	Public Property City As String
	Public Property State As String
	Public Property Phone As String
	Public Property Fax As String
	Public Property DefaultLpqWorkerID As String
	Public Property DealerNumberLpq As String
	Public Property Status As SmSettings.LenderVendorStatus

	Public Property Type As List(Of String)
	Public Property Logo As String
	Public Property Address As String
	Public Property Tags As String

	Public ReadOnly Property TagList() As List(Of String)
		Get
			Dim ret As New List(Of String)
			If Not String.IsNullOrWhiteSpace(Tags) Then
				ret = Tags.Split(","c).ToList()
			End If
			Return ret
		End Get
	End Property

	Public Sub New()
		Type = New List(Of String)()
	End Sub

	Public Function MakeItSafe() As SmLenderVendor
		Me.VendorID = Common.SafeStripHtmlString(Me.VendorID)
		Me.VendorName = Common.SafeStripHtmlString(Me.VendorName)
		Me.Zip = Common.SafeStripHtmlString(Me.Zip)
		Me.City = Common.SafeStripHtmlString(Me.City)
		Me.State = Common.SafeStripHtmlString(Me.State)
		Me.Phone = Common.SafeStripHtmlString(Me.Phone)
		Me.Fax = Common.SafeStripHtmlString(Me.Fax)
		Me.DefaultLpqWorkerID = Common.SafeStripHtmlString(Me.DefaultLpqWorkerID)
		Me.DealerNumberLpq = Common.SafeStripHtmlString(Me.DealerNumberLpq)
		Me.Logo = Common.SafeStripHtmlString(Me.Logo)
		Me.Address = Common.SafeStripHtmlString(Me.Address)
		Me.Tags = Common.SafeStripHtmlString(Me.Tags)
		Return Me
	End Function
End Class
