﻿Imports Microsoft.VisualBasic
Imports LPQMobile.Utils

Public Class SmProfileModel
	Public Property Email As String
	Public Property FirstName As String
	Public Property LastName As String
	Public Property Phone As String
	Public Property Avatar As String
	Public Property UserID As Guid
	Public Function MakeItSafe() As SmProfileModel
		Me.Email = Common.SafeStripHtmlString(Me.Email)
		Me.FirstName = Common.SafeStripHtmlString(Me.FirstName)
		Me.LastName = Common.SafeStripHtmlString(Me.LastName)
		Me.Phone = Common.SafeStripHtmlString(Me.Phone)
		Me.Avatar = Common.SafeStripHtmlString(Me.Avatar)
		Return Me
	End Function
End Class
