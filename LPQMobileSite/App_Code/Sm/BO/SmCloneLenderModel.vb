﻿Imports Microsoft.VisualBasic
Imports LPQMobile.Utils

Public Class SmCloneLenderModel

	Public Property LenderConfigID As Guid
	Public Property LosDomain As SmSettings.LosDomain
	Public Property OrganizationID As Guid
	Public Property LenderID As Guid
	Public Property OrganizationCode As String
	Public Property LenderCode As String
	Public Property ApiLogin As String
	Public Property ApiPassword As String
	Public Property Login As String
	Public Property Password As String
	Public Property LenderName As String
	Public Property LenderRef As String
	Public Property City As String
	Public Property State As String

	Public Function MakeItSafe() As SmCloneLenderModel
		Me.OrganizationCode = Common.SafeStripHtmlString(Me.OrganizationCode)
		Me.LenderCode = Common.SafeStripHtmlString(Me.LenderCode)
		Me.ApiLogin = Common.SafeStripHtmlString(Me.ApiLogin)
		Me.ApiPassword = Common.SafeStripHtmlString(Me.ApiPassword)
		Me.Login = Common.SafeStripHtmlString(Me.Login)
		Me.Password = Common.SafeStripHtmlString(Me.Password)
		Me.LenderName = Common.SafeStripHtmlString(Me.LenderName)
		Me.LenderRef = Common.SafeStripHtmlString(Me.LenderRef)
		Me.City = Common.SafeStripHtmlString(Me.City)
		Me.State = Common.SafeStripHtmlString(Me.State)
		Return Me
	End Function
End Class
