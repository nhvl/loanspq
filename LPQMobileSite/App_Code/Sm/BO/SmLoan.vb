﻿Imports System.Globalization
Imports PayPal.Api
Imports Microsoft.VisualBasic
Imports System.Xml
Imports LPQMobile.Utils

Public Class SmLoan
	Public Property LoanSavingID As Guid
	Public Property LenderID As Guid
	Public Property LenderRef As String
	Public Property VendorID As String
	Public Property UserID As Guid
	Public Property ApplicantName As String
	Public Property CreatedDate As Nullable(Of DateTime)
	Public Property SubmitDate As Nullable(Of DateTime)
	Public Property LoanType As SmSettings.LenderVendorType
	Public Property LoanNumber As String
	Public Property LoanID As String

	Private _applicantInfo As String
	Public ReadOnly Property ApplicantInfo As String
		Get
			Return _applicantInfo
		End Get
	End Property

	Private _requestParam As String
	Public Property RequestParam As String
		Get
			Return _requestParam
		End Get
		Set(value As String)
			_requestParam = value
			'todo: extract ApplicantInfo from RequestParam
			Dim doc As New XmlDocument()
			doc.LoadXml(_requestParam)
			'format: state model. Ex: Use 2017 BMW XS
			If Not String.IsNullOrEmpty(_requestParam) AndAlso doc IsNot Nothing Then
				_applicantInfo = ExtractApplicationInfo(doc)
			End If
		End Set
	End Property
	Public Property RequestParam_Enc As String
	Public Property RequestParamJson As String
	Public Property XmlResponse As String
	Public Property Status As String
	Public Property StatusDate As Nullable(Of DateTime)
	Public Property FundStatus As String
	Public Property FundDate As Nullable(Of DateTime)

	Public Function GetLoanInfo() As Dictionary(Of String, String)
		Dim result As New Dictionary(Of String, String)
		Select Case LoanType
			Case SmSettings.LenderVendorType.VL
				Return GetVLInfo()
			Case SmSettings.LenderVendorType.VL_COMBO
				Return GetVLInfo()
			Case SmSettings.LenderVendorType.PL
				Return GetPLInfo()
			Case SmSettings.LenderVendorType.PL_COMBO
				Return GetPLInfo()
			Case SmSettings.LenderVendorType.CC
				Return GetCCInfo()
			Case SmSettings.LenderVendorType.CC_COMBO
				Return GetCCInfo()
			Case Else
				Return result
		End Select
	End Function

	Public Function GetVLInfo() As Dictionary(Of String, String)
		Dim result As New Dictionary(Of String, String)
		'todo: extract LoanInfo from RequestParam property
		'fieldName:amount|duration|rate
		'For Approved Loan, use info from XmlResponse else use RequestParam. Leave blank for unknown field
		result.Add("amount", "")
		result.Add("duration", "")
		result.Add("rate", "")
		If Status = "QUALIFIED" AndAlso Not String.IsNullOrEmpty(XmlResponse) Then
			Dim doc As New XmlDocument
			doc.LoadXml(XmlResponse)
			Dim cNode As XmlNode = doc.SelectSingleNode("/OUTPUT/RESPONSE/DECISION")
			If cNode Is Nothing OrElse String.IsNullOrEmpty(cNode.InnerText) Then Return result
			doc.LoadXml(cNode.InnerText)
			Dim vlProductNodes = doc.SelectSingleNode("/PRODUCTS/VL_PRODUCT")
			If vlProductNodes IsNot Nothing Then
				Dim termNode = vlProductNodes.SelectSingleNode("@term")
				If termNode IsNot Nothing Then
					result.Item("duration") = termNode.Value & " mo"
				End If
				Dim amountNode = vlProductNodes.SelectSingleNode("@amount_approved")
				If amountNode IsNot Nothing Then
					result.Item("amount") = CInt(amountNode.Value).ToString("C0", New CultureInfo("en-US"))
				End If
			End If
			Dim rateNode = vlProductNodes.SelectSingleNode("RATE")
			If rateNode IsNot Nothing AndAlso rateNode.SelectSingleNode("@rate") IsNot Nothing Then
				result.Item("rate") = CDec(rateNode.SelectSingleNode("@rate").Value).ToString("N2", New CultureInfo("en-US")) & "%"
			End If

		ElseIf Status <> "QUALIFIED" AndAlso Not String.IsNullOrEmpty(RequestParam) Then
			Dim doc As New XmlDocument
			doc.LoadXml(RequestParam)
			Dim node As XmlNode
			If doc.DocumentElement.HasAttribute("xmlns") Then
				Dim nsmgr As New XmlNamespaceManager(doc.NameTable)
				nsmgr.AddNamespace("ns", Common.NAMESPACE_URI)
				node = doc.SelectSingleNode("//ns:VEHICLE_LOAN/ns:LOAN_INFO", nsmgr)
			Else
				node = doc.SelectSingleNode("VEHICLE_LOAN/LOAN_INFO")
			End If
			If node IsNot Nothing Then
				Dim termNode = node.SelectSingleNode("@loan_term")
				If termNode IsNot Nothing Then
					result.Item("duration") = termNode.Value & " mo"
				End If
				Dim amountNode = node.SelectSingleNode("@amount_requested")
				If amountNode IsNot Nothing Then
					result.Item("amount") = CInt(amountNode.Value).ToString("C0", New CultureInfo("en-US"))
				End If
			End If
		End If
		Return result
	End Function


	Public Function GetPLInfo() As Dictionary(Of String, String)
		Dim result As New Dictionary(Of String, String)
		'todo: extract LoanInfo from RequestParam property
		'fieldName:amount|duration|rate
		'For Approved Loan, use info from XmlResponse else use RequestParam. Leave blank for unknown field
		result.Add("amount", "")
		result.Add("duration", "")
		result.Add("rate", "")
		result.Add("monthly", "")
		'<OUTPUT version="1.0"><RESPONSE loan_number="189021" loan_id="044820477a0c4295992d76fbd02fc040"><DECISION status="QUALIFIED"><![CDATA[
		'<QUALIFIED_PRODUCTS><PRODUCT name="PERSONAL A" monthly_payment="128.56"><RATE rate_code="25-60M" rate_type="F" rate="10.490" min_loan_term="25" max_loan_term="66" max_loan_amount="10000.00" /></PRODUCT></QUALIFIED_PRODUCTS>]]></DECISION></RESPONSE></OUTPUT>
		If Status = "QUALIFIED" AndAlso Not String.IsNullOrEmpty(XmlResponse) Then
			Dim doc As New XmlDocument
			doc.LoadXml(XmlResponse)
			Dim cNode As XmlNode = doc.SelectSingleNode("/OUTPUT/RESPONSE/DECISION")
			If cNode Is Nothing OrElse String.IsNullOrEmpty(cNode.InnerText) Then Return result
			doc.LoadXml(cNode.InnerText)
			Dim plProductNodes = doc.SelectNodes("/PRODUCTS/PL_PRODUCT")
			If plProductNodes Is Nothing OrElse plProductNodes.Count = 0 Then Return result

			Dim monthlyNode = plProductNodes(0).SelectSingleNode("@estimated_monthly_payment")
			If monthlyNode IsNot Nothing Then
				result.Item("monthly") = monthlyNode.Value & " per mo"
			End If

			Dim termNode = plProductNodes(0).SelectSingleNode("@term")
			If termNode IsNot Nothing Then
				result.Item("duration") = termNode.Value & " mo"
			End If

			If plProductNodes(0).SelectSingleNode("@max_amount_approved") IsNot Nothing Then
				result.Item("amount") = CInt(plProductNodes(0).SelectSingleNode("@max_amount_approved").Value).ToString("C0", New CultureInfo("en-US"))
			End If

			Dim rateNode = plProductNodes(0).SelectSingleNode("RATE")
			If rateNode IsNot Nothing AndAlso rateNode.SelectSingleNode("@rate") IsNot Nothing Then
				result.Item("rate") = CDec(rateNode.SelectSingleNode("@rate").Value).ToString("N2", New CultureInfo("en-US")) & "%"
			End If

		ElseIf Status <> "QUALIFIED" AndAlso Not String.IsNullOrEmpty(RequestParam) Then
			Dim doc As New XmlDocument
			doc.LoadXml(RequestParam)
			Dim node As XmlNode
			If doc.DocumentElement.HasAttribute("xmlns") Then
				Dim nsmgr As New XmlNamespaceManager(doc.NameTable)
				nsmgr.AddNamespace("ns", Common.NAMESPACE_URI)
				node = doc.SelectSingleNode("//ns:PERSONAL_LOAN/ns:LOAN_INFO", nsmgr)
			Else
				node = doc.SelectSingleNode("PERSONAL_LOAN/LOAN_INFO")
			End If
			If node IsNot Nothing Then
				Dim termNode = node.SelectSingleNode("@loan_term_requested")
				If termNode IsNot Nothing Then
					result.Item("duration") = termNode.Value & " mo"
				End If
				Dim amountNode = node.SelectSingleNode("@amount_requested")
				If amountNode IsNot Nothing Then
					result.Item("amount") = CInt(amountNode.Value).ToString("C0", New CultureInfo("en-US"))
				End If
			End If
		End If
		Return result
	End Function

	Public Function GetCCInfo() As Dictionary(Of String, String)
		Dim result As New Dictionary(Of String, String)
		'todo: extract LoanInfo from RequestParam property
		'fieldName:amount|duration|rate
		'For Approved Loan, use info from XmlResponse else use RequestParam. Leave blank for unknown field
		result.Add("amount", "")
		result.Add("duration", "")
		result.Add("rate", "")
		result.Add("monthly", "")
		'<OUTPUT version="1.0"><RESPONSE loan_id="55bfe480aedd4ba79f301505a92d0fe0" loan_number="103865">
		'<DECISION validation_id="wJ6hRIFBidOF/EIBzt8y7d2PJg+aKsVynkU6JJWwoA==" status="QUALIFIED"><![CDATA[
		'<PRODUCTS><CC_PRODUCT loan_id="55bfe480aedd4ba79f301505a92d0fe0" date_offered="2018-05-04T16:15:36.7856780-07:00" is_prequalified="N" underwriting_service_results_index="0" amount_approved="0" max_amount_approved="2000" card_name="Savings Secured Visa Platinum Card" card_type="SECUREDCREDIT" rate_code="" default_apr="13.49" original_default_apr="13.49" payment_percent="2" original_payment_percent="2" index="4.75" margin="8.74" rate_floor="-999999" rate_ceiling="18" index_type="PRIME" rate_type="V" tier="0" reference_id="4103,1000,7000" /></PRODUCTS>]]></DECISION></RESPONSE></OUTPUT>
		If Status = "QUALIFIED" AndAlso Not String.IsNullOrEmpty(XmlResponse) Then
			Dim doc As New XmlDocument
			doc.LoadXml(XmlResponse)
			Dim cNode As XmlNode = doc.SelectSingleNode("/OUTPUT/RESPONSE/DECISION")
			If cNode Is Nothing OrElse String.IsNullOrEmpty(cNode.InnerText) Then Return result
			doc.LoadXml(cNode.InnerText)
			Dim ccProductNodes = doc.SelectNodes("/PRODUCTS/CC_PRODUCT")
			If ccProductNodes Is Nothing Then Return result

			If ccProductNodes(0).SelectSingleNode("@max_amount_approved") IsNot Nothing Then
				Dim approvedAmountNode = CInt(ccProductNodes(0).SelectSingleNode("@max_amount_approved").Value).ToString("C0", New CultureInfo("en-US")) & " max"	  'TODO: which product to use, highest amount or lowest rate
				result.Item("amount") = approvedAmountNode
			End If
			If ccProductNodes(0).SelectSingleNode("@default_apr") IsNot Nothing Then
				result.Item("rate") = CDec(ccProductNodes(0).SelectSingleNode("@default_apr").Value).ToString("N2", New CultureInfo("en-US")) & "%   APR"
			End If

		ElseIf Status <> "QUALIFIED" AndAlso Not String.IsNullOrEmpty(RequestParam) Then
			Dim doc As New XmlDocument
			doc.LoadXml(RequestParam)
			Dim node As XmlNode
			If doc.DocumentElement.HasAttribute("xmlns") Then
				Dim nsmgr As New XmlNamespaceManager(doc.NameTable)
				nsmgr.AddNamespace("ns", Common.NAMESPACE_URI)
				node = doc.SelectSingleNode("//ns:CREDITCARD_LOAN/ns:LOAN_INFO", nsmgr)
			Else
				node = doc.SelectSingleNode("CREDITCARD_LOAN/LOAN_INFO")
			End If
			If node IsNot Nothing Then
				Dim amountNode = node.SelectSingleNode("@requested_credit_limit")
				If amountNode IsNot Nothing Then
					result.Item("amount") = CInt(amountNode.Value).ToString("C0", New CultureInfo("en-US"))
				End If
			End If
		End If
		Return result
	End Function


	Public Function GetStipulations() As Dictionary(Of String, String)
		'Todo: extract Stipulation items from XmlResponse
		' For Approved, use XmlResponse else blank
		Dim result As New Dictionary(Of String, String)
		If Status = "QUALIFIED" AndAlso Not String.IsNullOrEmpty(XmlResponse) Then
			Dim doc As New XmlDocument
			doc.LoadXml(XmlResponse)
			Dim cNode As XmlNode = doc.SelectSingleNode("/OUTPUT/RESPONSE/DECISION")
			If cNode IsNot Nothing AndAlso Not String.IsNullOrEmpty(cNode.InnerText) Then
				doc.LoadXml(cNode.InnerText)
				Dim stipulationNodes = doc.SelectNodes("/PRODUCTS/VL_PRODUCT/STIPULATIONS/STIPULATION")
				If stipulationNodes IsNot Nothing AndAlso stipulationNodes.Count > 0 Then
					For Each node As XmlNode In stipulationNodes
						Dim stipTextNode = node.SelectSingleNode("@stip_text")
						If stipTextNode IsNot Nothing AndAlso Not String.IsNullOrEmpty(stipTextNode.Value) AndAlso Not result.ContainsKey(stipTextNode.Value) Then
							result.Add(stipTextNode.Value, node.SelectSingleNode("@is_required").Value)
						End If
					Next
				End If
			End If
		End If
		Return result
	End Function

	Private Function ExtractApplicationInfo(doc As XmlDocument) As String
		Select Case LoanType
			Case SmSettings.LenderVendorType.VL
				Return ExtractVLInfo(doc)
			Case SmSettings.LenderVendorType.VL_COMBO
				Return ExtractVLInfo(doc)
			Case SmSettings.LenderVendorType.PL
				Return ExtractPLInfo(doc)
			Case SmSettings.LenderVendorType.PL_COMBO
				Return ExtractPLInfo(doc)
			Case SmSettings.LenderVendorType.CC
				Return ExtractCCInfo(doc)
			Case SmSettings.LenderVendorType.CC_COMBO
				Return ExtractCCInfo(doc)
			Case Else
				Return ""
		End Select

	End Function

	Private Function ExtractVLInfo(doc As XmlDocument) As String
		Dim result As String = String.Empty
		Dim node As XmlNode
		If doc.DocumentElement.HasAttribute("xmlns") Then
			Dim nsmgr As New XmlNamespaceManager(doc.NameTable)
			nsmgr.AddNamespace("ns", Common.NAMESPACE_URI)
			node = doc.SelectSingleNode("//ns:VEHICLE_LOAN/ns:VEHICLES", nsmgr)
		Else
			node = doc.SelectSingleNode("VEHICLE_LOAN/VEHICLES")
		End If
		If node Is Nothing OrElse Not node.HasChildNodes Then Return result
		node = node.ChildNodes(0)
		Dim isNewNode = node.SelectSingleNode("@is_new_vehicle")
		If isNewNode IsNot Nothing Then
			result = result & IIf(isNewNode.Value.ToUpper() = "Y", "New", "Used")
		End If
		Dim yearNode = node.SelectSingleNode("@year")
		If yearNode IsNot Nothing AndAlso yearNode.Value <> "0" Then
			result = result & " " & yearNode.Value
		End If
		Dim makeNode = node.SelectSingleNode("@make")
		If makeNode IsNot Nothing Then

			If makeNode.Value = "UNDECIDED" Then
				result = result & " Vehicle" & " " & makeNode.Value
			Else
				result = result & " " & makeNode.Value
			End If

		End If
		Dim modelNode = node.SelectSingleNode("@model")
		If modelNode IsNot Nothing Then
			result = result & " " & modelNode.Value
		End If

		Return result
	End Function

	Private Function ExtractPLInfo(doc As XmlDocument) As String
		Dim result As String = String.Empty
		Dim node As XmlNode
		If doc.DocumentElement.HasAttribute("xmlns") Then
			Dim nsmgr As New XmlNamespaceManager(doc.NameTable)
			nsmgr.AddNamespace("ns", Common.NAMESPACE_URI)
			node = doc.SelectSingleNode("//ns:PERSONAL_LOAN/ns:LOAN_INFO", nsmgr)
		Else
			node = doc.SelectSingleNode("PERSONAL_LOAN/LOAN_INFO")
		End If
		If node IsNot Nothing Then
			Dim purposeNode = node.SelectSingleNode("@purpose_type")
			If purposeNode IsNot Nothing Then
				result = purposeNode.Value
			End If
		End If
		Return result
	End Function

	Private Function ExtractCCInfo(doc As XmlDocument) As String
		Dim result As String = String.Empty
		Dim node As XmlNode
		If doc.DocumentElement.HasAttribute("xmlns") Then
			Dim nsmgr As New XmlNamespaceManager(doc.NameTable)
			nsmgr.AddNamespace("ns", Common.NAMESPACE_URI)
			node = doc.SelectSingleNode("//ns:CREDITCARD_LOAN/ns:LOAN_INFO", nsmgr)
		Else
			node = doc.SelectSingleNode("CREDITCARD_LOAN/LOAN_INFO")
		End If
		If node IsNot Nothing Then
			Dim purposeNode = node.SelectSingleNode("@purpose_type")
			If purposeNode IsNot Nothing Then
				result = purposeNode.Value
			End If
		End If
		Return result
	End Function
End Class
