﻿Imports LPQMobile.Utils

Public Class SmVendorGroupAdminUserRoleListFilter
	Public Property Keyword As String
	Public Property LenderConfigID As Guid
	Public Property LenderID As Guid
	Public Sub New()
		LenderConfigID = Guid.Empty
		LenderID = Guid.Empty
	End Sub

	Public Function MakeItSafe() As SmVendorGroupAdminUserRoleListFilter
		Me.Keyword = Common.SafeStripHtmlString(Me.Keyword)
		Return Me
	End Function
End Class
