﻿Imports Microsoft.VisualBasic

<Serializable()> _
Public Class SmTextValuePair
	Public Property Text As String
	Public Property Value As String
End Class
