﻿
Public Class SmGlobalConfigItem
	Public Property ID As Guid
	Public Property LenderID As Guid
	Public Property LenderConfigID As Guid
	Public Property VendorID As String
	Public Property RevisionID As Integer
	Public Property ConfigJson As String
	Public Property Name As String
	Public Property LastModifiedDate As DateTime

End Class


