﻿Imports Microsoft.VisualBasic
Imports LPQMobile.Utils


Public Class SmLenderConfigBasicInfo
	Public Property LenderConfigID As Guid
	Public Property OrganizationID As Guid
	Public Property LenderID As Guid
	Public Property LenderRef As String
	Public Property ReturnURL As String
	Public Property LPQURL As String
	Public Property Note As String

	Public Function MakeItSafe() As SmLenderConfigBasicInfo
		Me.LenderRef = Common.SafeStripHtmlString(Me.LenderRef)
		Me.ReturnURL = Common.SafeStripHtmlString(Me.ReturnURL)
		Me.LPQURL = Common.SafeStripHtmlString(Me.LPQURL)
		Me.Note = Common.SafeStripHtmlString(Me.Note)
		Return Me
	End Function
End Class
'Public Class SmLenderConfig
'	Inherits SmLenderConfigBasicInfo
'	Public Property LPQLogin As String
'	Public Property LPQPW As String
'	Public Property ConfigData As String
'	Public Property LastModifiedDate As DateTime?
'	Public Property CreatedDate As DateTime
'	Public Property LastModifiedByUserID As Guid
'	Public Property LastModifiedByUserName As String
'	Public Property LastOpenedByUserID As Guid
'	Public Property LastOpenedByUserName As String
'	Public Property LastOpenedDate As DateTime?
'	Public Property IterationID As Integer
'	Public Property RevisionID As Integer

'	Public Function MakeItSafe() As SmLenderConfig
'		Me.LenderRef = Common.SafeStripHtmlString(Me.LenderRef)
'		Me.ReturnURL = Common.SafeStripHtmlString(Me.ReturnURL)
'		Me.LPQLogin = Common.SafeStripHtmlString(Me.LPQLogin)
'		Me.LPQPW = Common.SafeStripHtmlString(Me.LPQPW)
'		Me.LPQURL = Common.SafeStripHtmlString(Me.LPQURL)
'		Me.Note = Common.SafeStripHtmlString(Me.Note)
'		Return Me
'	End Function
'End Class


Public Class SmLenderConfig
	Public Property LenderConfigID As Guid
	Public Property OrganizationID As Guid
	Public Property LenderID As Guid
	Public Property LenderRef As String
	Public Property ReturnURL As String
	Public Property LPQLogin As String
	Public Property LPQPW As String
	Public Property LPQURL As String
	Public Property ConfigData As String
	Public Property Note As String
	Public Property LastModifiedDate As DateTime?
	Public Property CreatedDate As DateTime
	Public Property LastModifiedByUserID As Guid
	Public Property LastModifiedByUserName As String
	Public Property LastOpenedByUserID As Guid
	Public Property LastOpenedByUserName As String
	Public Property LastOpenedDate As DateTime?
	Public Property IterationID As Integer
	Public Property RevisionID As Integer

	Public Function MakeItSafe() As SmLenderConfig
		Me.LenderRef = Common.SafeStripHtmlString(Me.LenderRef)
		Me.ReturnURL = Common.SafeStripHtmlString(Me.ReturnURL)
		Me.LPQLogin = Common.SafeStripHtmlString(Me.LPQLogin)
		Me.LPQPW = Common.SafeStripHtmlString(Me.LPQPW)
		Me.LPQURL = Common.SafeStripHtmlString(Me.LPQURL)
		Me.Note = Common.SafeStripHtmlString(Me.Note)
		Return Me
	End Function
End Class