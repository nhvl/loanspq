﻿
Public Class SmMortgageLoanSearchResult
	Public Property LoanNumber As String
	Public Property Consumers As List(Of SmMortgageLoanConsumerInfo)
End Class


Public Class SmMortgageLoanConsumerInfo
	Public Property FirstName As String
	Public Property LastName As String
	Public Property IsPrimaryConsumer As Boolean
	Public Property Email As String
	Public Property Phone As String
End Class
