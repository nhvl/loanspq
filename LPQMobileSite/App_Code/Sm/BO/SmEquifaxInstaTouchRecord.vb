﻿Imports Microsoft.VisualBasic

Public Class SmEquifaxInstaTouchRecord
	Public Property ID As String
	Public Property RecordType As String
	Public Property ConsumerFirstName As String
	Public Property ConsumerLastName As String
	Public Property DateRequested As DateTimeOffset
	Public Property DateResponded As DateTimeOffset
	Public Property TransactionID As Guid
	Public Property SessionID As String
	Public Property MerchantId As String
	Public Property LenderID As Guid
	Public Property LenderRef As String
	Public Property LoanType As String
End Class
