﻿Imports Microsoft.VisualBasic
Imports LPQMobile.Utils

Public Class SmVendorListFilter
	Public Property Keyword As String
	Public Property LenderID As Guid
	Public Property LenderConfigID As Guid
	Public Property Status As List(Of SmSettings.LenderVendorStatus)
	Public Property VendorIDList As List(Of String)
	Public Property TagList As List(Of String)
	Public Property FilterByTags As Boolean
	Public Sub New()
		LenderID = Guid.Empty
		LenderConfigID = Guid.Empty
		Status = New List(Of SmSettings.LenderVendorStatus)
		VendorIDList = New List(Of String)
		FilterByTags = False
	End Sub

	Public Function MakeItSafe() As SmVendorListFilter
		Me.Keyword = Common.SafeStripHtmlString(Me.Keyword)
		Return Me
	End Function
End Class
