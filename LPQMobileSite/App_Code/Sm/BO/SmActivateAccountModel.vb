﻿Imports Microsoft.VisualBasic
Imports LPQMobile.Utils

Public Class SmActivateAccountModel

	Public Property Token As String
	Public Property FirstName As String
	Public Property LastName As String
	Public Property Phone As String
	Public Property Avatar As String
	Public Property Password As String
	Public Property ConfirmPassword As String

	Public Function MakeItSafe() As SmActivateAccountModel
		Me.Token = Common.SafeStripHtmlString(Me.Token)
		Me.FirstName = Common.SafeStripHtmlString(Me.FirstName)
		Me.LastName = Common.SafeStripHtmlString(Me.LastName)
		Me.Phone = Common.SafeStripHtmlString(Me.Phone)
		Me.Avatar = Common.SafeStripHtmlString(Me.Avatar)
		Me.Password = Common.SafeStripHtmlString(Me.Password)
		Me.ConfirmPassword = Common.SafeStripHtmlString(Me.ConfirmPassword)
		Return Me
	End Function
End Class
