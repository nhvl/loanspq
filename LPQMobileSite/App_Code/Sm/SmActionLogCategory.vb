﻿
Public Class SmActionLogCategory

	Public Const USERS_MANIPULATION As String = "Users"
	Public Const AUTHENTICATION As String = "Authentication"
	Public Const LENDERS_MANIPULATION As String = "Lenders"
	Public Const CONFIGURATION As String = "Configuration"
	Public Const USER_ROLES_MANIPULATION As String = "UserRoles"
	Public Const LOANS_MANIPULATION As String = "Loans"
	Public Const LANDINGPAGE_MANIPULATION As String = "LandingPages"
	Public Const GLOBAL_CONFIGS_MANIPULATION As String = "GlobalConfigs"
End Class
