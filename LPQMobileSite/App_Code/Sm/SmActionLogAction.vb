﻿Imports Microsoft.VisualBasic
Imports Sm

Public Class SmActionLogAction
	Public Const ADD_BACK_OFFICE_LENDER As String = "Add back-office lender"
	Public Const CHANGE_USER_STATE As String = "Change user state"
	Public Const CHANGE_LENDER_USER_ROLE_STATE As String = "Change lender user role state"
	Public Const CHANGE_PORTAL_USER_ROLE_STATE As String = "Change portal user role state"
	Public Const CHANGE_VENDOR_GROUP_ADMIN_USER_ROLE_STATE As String = "Change vendor group admin user role state"
	Public Const ADD_USER As String = "Add user"
	Public Const EDIT_USER As String = "Edit user"
	Public Const CHANGE_PASSWORD As String = "Change password"
	Public Const UNLOCK_USER As String = "Unlock user"
	Public Const DELETE_USER As String = "Delete user"
	Public Const CREATE_FORGOT_PASSWORD_REQUEST As String = "Forgot password"
	Public Const CREATE_SSOEMAIL_VALIDATION_AND_MLU_SSO_REQUEST As String = "Validate SSO Email and perform MLU SSO"
	Public Const REMOVE_LENDER_USER_ROLE As String = "Remove lender user role"
	Public Const REMOVE_USER_ROLES As String = "Remove all role of specified user"
	Public Const REMOVE_PORTAL_USER_ROLE As String = "Remove portal user role"
	Public Const REMOVE_VENDOR_GROUP_ADMIN_USER_ROLE As String = "Remove vendor group admin user role"
	Public Const REMOVE_VENDOR_USER As String = "Remove vendor user"
	Public Const UPDATE_GLOBAL_CONFIGS As String = "Update global configuration items"
	Public Const ACTIVATE_ACCOUNT As String = "Activate account"
	Public Const PROCESS_RESET_PASSWORD_REQUEST As String = "Reset password"
	Public Const PROCESS_SSOEMAIL_VALIDATION_TOKEN As String = "Process SSO email validation token"
	Public Const LOGIN_FAILED As String = "Login attempt failed"
	Public Const LOGIN_SUCCESS As String = "Login success"
	Public Const SSO_LOGIN_SUCCESS As String = "SSO login success"
	Public Const RESET_LOGIN_FAILURE_COUNT As String = "Reset Login Failure Count"
	Public Const REVERT_TO_REVISION As String = "Revert to revision"
	Public Const ADD_LENDER_VENDOR As String = "Add lender vendor"
	Public Const EDIT_LENDER_VENDOR As String = "Edit lender vendor"
	Public Const CHANGE_LENDER_VENDOR_STATE As String = "Change lender vendor state"
	Public Const CHANGE_VENDOR_USER_ROLE_STATE As String = "Change vendor user role state"
	Public Const ADD_USER_ROLE As String = "Add user role"
	Public Const UPDATE_USER_ROLE As String = "Update user role"
	Public Const SUBMIT_LOAN As String = "Submit loan"
	Public Const ADD_LANDING_PAGE As String = "Add landing page"
	Public Const UPDATE_LANDING_PAGE As String = "Update landing page"
	Public Const UPDATE_LANDING_PAGE_CONTENT As String = "Update landing page content"
	Public Const CHANGE_LANDING_PAGE_STATUS As String = "Change landing page status"
	Public Shared ReadOnly Property UPGRADE_USER_ROLE_TO_LENDERADMIN As String
		Get
			Return "Upgrade user role to " & SmSettings.Role.LenderAdmin.GetEnumDescription()
		End Get
	End Property
	Public Shared ReadOnly Property UPGRADE_USER_ROLE_TO_PORTALADMIN As String
		Get
			Return "Upgrade user role to " & SmSettings.Role.PortalAdmin.GetEnumDescription()
		End Get
	End Property

	Public Shared ReadOnly Property DOWNGRADE_USER_ROLE_TO_PORTALADMIN As String
		Get
			Return "Downgrade user role to " & SmSettings.Role.PortalAdmin.GetEnumDescription()
		End Get
	End Property
	Public Shared ReadOnly Property DOWNGRADE_USER_ROLE_TO_VENDORGROUPADMIN As String
		Get
			Return "Downgrade user role to " & SmSettings.Role.VendorGroupAdmin.GetEnumDescription()
		End Get
	End Property
	Public Const EDIT_LENDER_CONFIG As String = "Edit lender config"
	Public Const CLONE_LENDER As String = "Clone lender"
	Public Const CLONE_PORTAL As String = "Clone portal"
	Public Const CLONE_LANDINGPAGE As String = "Clone landing page"
End Class
