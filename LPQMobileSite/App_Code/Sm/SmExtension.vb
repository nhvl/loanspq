﻿Imports System.ComponentModel
Imports System.Runtime.CompilerServices
Imports Microsoft.VisualBasic

Namespace Sm
	Public Module SmExtension

		<Extension()> _
		Public Function GetEnumDescription(ByVal enumConstant As [Enum]) As String
			Dim attr() As DescriptionAttribute = DirectCast(enumConstant.GetType().GetField(enumConstant.ToString()).GetCustomAttributes(GetType(DescriptionAttribute), False), DescriptionAttribute())
			Return If(attr.Length > 0, attr(0).Description, enumConstant.ToString)
		End Function
	End Module
End Namespace