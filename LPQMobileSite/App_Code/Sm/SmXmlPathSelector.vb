﻿Imports Microsoft.VisualBasic
Namespace Sm
	Public Module SmXmlPathSelector

		Public Function BL_VEHICLE_PREAPPROVED_MESSAGE() As String
			Return "WEBSITE/BUSINESS_LOAN/SUCCESS_MESSAGES/VEHICLE_PREAPPROVED_MESSAGE"
		End Function
		Public Function BL_VEHICLE_SUBMITTED_MESSAGE() As String
			Return "WEBSITE/BUSINESS_LOAN/SUCCESS_MESSAGES/VEHICLE_SUBMITTED_MESSAGE"
		End Function
		Public Function BL_CARD_PREAPPROVED_MESSAGE() As String
			Return "WEBSITE/BUSINESS_LOAN/SUCCESS_MESSAGES/CARD_PREAPPROVED_MESSAGE"
		End Function
		Public Function BL_CARD_SUBMITTED_MESSAGE() As String
			Return "WEBSITE/BUSINESS_LOAN/SUCCESS_MESSAGES/CARD_SUBMITTED_MESSAGE"
		End Function
		Public Function BL_OTHER_PREAPPROVED_MESSAGE() As String
			Return "WEBSITE/BUSINESS_LOAN/SUCCESS_MESSAGES/OTHER_PREAPPROVED_MESSAGE"
		End Function
		Public Function BL_OTHER_SUBMITTED_MESSAGE() As String
			Return "WEBSITE/BUSINESS_LOAN/SUCCESS_MESSAGES/OTHER_SUBMITTED_MESSAGE"
		End Function

		Public Function ELIGIBILITY_MESSAGE(loanType As String) As String
			Return String.Format("WEBSITE/{0}/ELIGIBILITY_MESSAGE", loanType)
		End Function
		Public Function XA_SPECIAL_ACCOUNT_ELIGIBILITY_MESSAGE() As String
			Return "WEBSITE/XA_LOAN/SPECIAL_ACCOUNT_ELIGIBILITY_MESSAGE"
		End Function
		Public Function XA_BUSINESS_ACCOUNT_ELIGIBILITY_MESSAGE() As String
			Return "WEBSITE/XA_LOAN/BUSINESS_ACCOUNT_ELIGIBILITY_MESSAGE"
		End Function
        Public Function PREAPPROVED_MESSAGE(loanType As String) As String
            Return String.Format("WEBSITE/{0}/SUCCESS_MESSAGES/PREAPPROVED_MESSAGE", loanType)
        End Function
        Public Function PREVIOUS_EMPLOYMENT_THRESHOLD(loantype As String) As String
            Return String.Format("WEBSITE/{0}/@previous_employment_threshold", loantype)
        End Function
        Public Function PREVIOUS_ADDRESS_THRESHOLD(loantype As String) As String
            Return String.Format("WEBSITE/{0}/@previous_address_threshold", loantype)
        End Function
        Public Function XA_SPECIAL_ACCOUNT_PREAPPROVED_MESSAGE() As String
			Return "WEBSITE/XA_LOAN/SPECIAL_ACCOUNT_SUCCESS_MESSAGES/PREAPPROVED_MESSAGE"
		End Function
		Public Function XA_BUSINESS_ACCOUNT_PREAPPROVED_MESSAGE() As String
			Return "WEBSITE/XA_LOAN/BUSINESS_ACCOUNT_SUCCESS_MESSAGES/PREAPPROVED_MESSAGE"
		End Function
		Public Function SUBMITTED_MESSAGE(loanType As String) As String
			Return String.Format("WEBSITE/{0}/SUCCESS_MESSAGES/SUBMITTED_MESSAGE", loanType)
		End Function
		Public Function XA_SPECIAL_ACCOUNT_SUBMITTED_MESSAGE() As String
			Return "WEBSITE/XA_LOAN/SPECIAL_ACCOUNT_SUCCESS_MESSAGES/SUBMITTED_MESSAGE"
		End Function
		Public Function XA_BUSINESS_ACCOUNT_SUBMITTED_MESSAGE() As String
			Return "WEBSITE/XA_LOAN/BUSINESS_ACCOUNT_SUCCESS_MESSAGES/SUBMITTED_MESSAGE"
		End Function
		Public Function DECLINED_MESSAGE(loanType As String) As String
			Return String.Format("WEBSITE/{0}/SUCCESS_MESSAGES/DECLINED_MESSAGE", loanType)
		End Function
		Public Function COMBO_BOTH_PREAPPROVED_MESSAGE(loanType As String) As String
			Return String.Format("WEBSITE/{0}/SUCCESS_MESSAGES/COMBO_BOTH_PREAPPROVED_MESSAGE", loanType)
		End Function
		Public Function COMBO_SUBMITTED_MESSAGE(loanType As String) As String
			Return String.Format("WEBSITE/{0}/SUCCESS_MESSAGES/COMBO_SUBMITTED_MESSAGE", loanType)
		End Function
        Public Function COMBO_XA_PREAPPROVED_MESSAGE(loanType As String) As String
            Return String.Format("WEBSITE/{0}/SUCCESS_MESSAGES/COMBO_XA_PREAPPROVED_MESSAGE", loanType)
        End Function
        Public Function COMBO_XA_SUBMITTED_MESSAGE(loanType As String) As String
            Return String.Format("WEBSITE/{0}/SUCCESS_MESSAGES/COMBO_XA_SUBMITTED_MESSAGE", loanType)
        End Function
		Public Function PREQUALIFIED_MESSAGE(loanType As String) As String
			Return String.Format("WEBSITE/{0}/SUCCESS_MESSAGES/PREQUALIFIED_MESSAGE", loanType)
		End Function
		Public Function DEMOGRAPHIC_DESCRIPTION_MESSAGE(loanType As String) As String
			Return String.Format("WEBSITE/{0}/SUCCESS_MESSAGES/DEMOGRAPHIC_DESCRIPTION_MESSAGE", loanType)
		End Function
		Public Function DEMOGRAPHIC_DISCLOSURE_MESSAGE(loanType As String) As String
			Return String.Format("WEBSITE/{0}/SUCCESS_MESSAGES/DEMOGRAPHIC_DISCLOSURE_MESSAGE", loanType)
		End Function
		Public Function COMBO_PREQUALIFIED_MESSAGE(loantype As String) As String
            Return String.Format("WEBSITE/{0}/SUCCESS_MESSAGES/COMBO_PREQUALIFIED_MESSAGE", loantype)
        End Function
        Public Function XA_APPROVED_SECONDARY_MESSAGE() As String
			Return "WEBSITE/XA_LOAN/SUCCESS_MESSAGES/APPROVED_SECONDARY_MESSAGE"
		End Function
		Public Function XA_SPECIAL_ACCOUNT_APPROVED_SECONDARY_MESSAGE() As String
			Return "WEBSITE/XA_LOAN/SPECIAL_ACCOUNT_SUCCESS_MESSAGES/APPROVED_SECONDARY_MESSAGE"
		End Function
		Public Function XA_BUSINESS_ACCOUNT_APPROVED_SECONDARY_MESSAGE() As String
			Return "WEBSITE/XA_LOAN/BUSINESS_ACCOUNT_SUCCESS_MESSAGES/APPROVED_SECONDARY_MESSAGE"
		End Function

		Public Function DISCLOSURES(loanType As String) As String
			Return String.Format("WEBSITE/{0}/DISCLOSURES", loanType)
		End Function
		Public Function FUNDING_DISCLOSURE(loanType As String) As String
			Return String.Format("WEBSITE/{0}/FUNDING_DISCLOSURE", loanType)
		End Function
		Public Function XA_SPECIAL_ACCOUNT_DISCLOSURES() As String
			Return "WEBSITE/XA_LOAN/SPECIAL_ACCOUNT_DISCLOSURES"
		End Function
		Public Function XA_SPECIAL_ACCOUNT_FUNDING_DISCLOSURE() As String
			Return "WEBSITE/XA_LOAN/SPECIAL_ACCOUNT_FUNDING_DISCLOSURE"
		End Function
		Public Function XA_BUSINESS_ACCOUNT_DISCLOSURES() As String
			Return "WEBSITE/XA_LOAN/BUSINESS_ACCOUNT_DISCLOSURES"
		End Function
		Public Function XA_BUSINESS_ACCOUNT_FUNDING_DISCLOSURE() As String
			Return "WEBSITE/XA_LOAN/BUSINESS_ACCOUNT_FUNDING_DISCLOSURE"
		End Function
		Public Function BL_VEHICLE_DISCLOSURES() As String
			Return "WEBSITE/BUSINESS_LOAN/VEHICLE_DISCLOSURES"
		End Function
		Public Function BL_CARD_DISCLOSURES() As String
			Return "WEBSITE/BUSINESS_LOAN/CARD_DISCLOSURES"
		End Function
		Public Function BL_OTHER_DISCLOSURES() As String
			Return "WEBSITE/BUSINESS_LOAN/OTHER_DISCLOSURES"
		End Function

		Public Function DISCLOSURES_LOC(loanType As String) As String
			Return String.Format("WEBSITE/{0}/DISCLOSURES_LOC", loanType)
		End Function

		Public Function DISCLOSURES_SECONDARY(loanType As String) As String
			Return String.Format("WEBSITE/{0}/DISCLOSURES_SECONDARY", loanType)
		End Function
		Public Function FUNDING_DISCLOSURE_SECONDARY(loanType As String) As String
			Return String.Format("WEBSITE/{0}/FUNDING_DISCLOSURE_SECONDARY", loanType)
		End Function
		Public Function XA_SPECIAL_ACCOUNT_DISCLOSURES_SECONDARY() As String
			Return "WEBSITE/XA_LOAN/SPECIAL_ACCOUNT_DISCLOSURES_SECONDARY"
		End Function
		Public Function XA_SPECIAL_ACCOUNT_FUNDING_DISCLOSURE_SECONDARY() As String
			Return "WEBSITE/XA_LOAN/SPECIAL_ACCOUNT_FUNDING_DISCLOSURE_SECONDARY"
		End Function
		Public Function XA_BUSINESS_ACCOUNT_DISCLOSURES_SECONDARY() As String
			Return "WEBSITE/XA_LOAN/BUSINESS_ACCOUNT_DISCLOSURES_SECONDARY"
		End Function
		Public Function XA_BUSINESS_ACCOUNT_FUNDING_DISCLOSURE_SECONDARY() As String
			Return "WEBSITE/XA_LOAN/BUSINESS_ACCOUNT_FUNDING_DISCLOSURE_SECONDARY"
		End Function

		Public Function VEHICLE_LOAN_PURPOSES() As String
			Return "WEBSITE/ENUMS/VEHICLE_LOAN_PURPOSES"
		End Function
		Public Function VEHICLE_TYPES() As String
			Return "WEBSITE/ENUMS/VEHICLE_TYPES"
		End Function
		Public Function PERSONAL_LOAN_PURPOSES() As String
			Return "WEBSITE/ENUMS/PERSONAL_LOAN_PURPOSES"
		End Function
		Public Function PERSONAL_LOC_TYPES() As String
			Return "WEBSITE/ENUMS/PERSONAL_LOC_PURPOSES"
		End Function

		Public Function CREDIT_CARD_LOAN_PURPOSES() As String
			Return "WEBSITE/ENUMS/CREDIT_CARD_LOAN_PURPOSES"
		End Function
		Public Function CREDIT_CARD_NAMES() As String
			Return "WEBSITE/ENUMS/CREDIT_CARD_NAMES"
		End Function
		Public Function CREDIT_CARD_TYPES() As String
			Return "WEBSITE/ENUMS/CREDIT_CARD_TYPES"
		End Function
		Public Function BL_CREDIT_CARD_PURPOSES() As String
			Return "WEBSITE/ENUMS/BL_CREDIT_CARD_PURPOSES"
		End Function
		Public Function BL_VEHICLE_PURPOSES() As String
			Return "WEBSITE/ENUMS/BL_VEHICLE_PURPOSES"
		End Function
		Public Function BL_OTHER_PURPOSES() As String
			Return "WEBSITE/ENUMS/BL_OTHER_PURPOSES"
		End Function
		Public Function BL_CREDIT_CARD_TYPES() As String
			Return "WEBSITE/ENUMS/BL_CREDIT_CARD_TYPES"
		End Function
		Public Function HOME_EQUITY_LOAN_PURPOSES() As String
			Return "WEBSITE/ENUMS/HOME_EQUITY_LOAN_PURPOSES"
		End Function

		Public Function HOME_EQUITY_LOAN_REASONS() As String
			Return "WEBSITE/ENUMS/HOME_EQUITY_LOAN_REASONS"
		End Function

		Public Function PROPERTY_TYPES() As String
			Return "WEBSITE/ENUMS/PROPERTY_TYPES"
		End Function

		Public Function LICENSE_SCAN() As String
			Return "WEBSITE/@mpao_key2"
		End Function

		Public Function DL_BARCODE_SCAN() As String
			Return "WEBSITE/@dl_barcode_scan"
		End Function

		Public Function FOREIGN_ADDRESS() As String
			Return "WEBSITE/VISIBLE/@foreign_address"
		End Function

		Public Function FOREIGN_PHONE() As String
			Return "WEBSITE/VISIBLE/@foreign_phone"
		End Function

		Public Function MFA_EMAIL_ONLY() As String
			Return "WEBSITE/@mfa_email_only"
		End Function
		Public Function INSTATOUCH_ENABLED() As String
			Return "WEBSITE/@instatouch_enabled"
		End Function

		Public Function LINKEDIN_ENABLE() As String
			Return "WEBSITE/VISIBLE/@linkedin_enable"
		End Function

		Public Function APM_LINKEDIN_ENABLE_N() As String
			Return "WEBSITE/VISIBLE/@APM_linkedin_enable_N"
		End Function

		Public Function APM_DL_SCAN_ENABLE_N() As String
			Return "WEBSITE/VISIBLE/@APM_dl_scan_enable_N"
		End Function

		Public Function APM_SITE_ANALYTICS_ENABLE_N() As String
			Return "WEBSITE/VISIBLE/@APM_site_analytics_enable_N"
		End Function

		Public Function ADDRESS_KEY() As String
			Return "WEBSITE/@address_key"
		End Function

		Public Function HOMEPAGE_EDITOR() As String
			Return "WEBSITE/@homepage_editor"
		End Function

		Public Function XA_UPLOAD_DOC_MESSAGE() As String
			Return "WEBSITE/XA_LOAN/UPLOAD_DOC_MESSAGE"
		End Function

        Public Function XA_UPLOAD_DOC_ENABLE() As String
            Return "WEBSITE/XA_LOAN/VISIBLE/@document_upload_enable"
        End Function
        Public Function XA_SPECIAL_UPLOAD_DOC_MESSAGE() As String
            Return "WEBSITE/XA_LOAN/SPECIAL_UPLOAD_DOC_MESSAGE"
        End Function

        Public Function XA_SPECIAL_UPLOAD_DOC_ENABLE() As String
            Return "WEBSITE/XA_LOAN/VISIBLE/@document_special_upload_enable"
        End Function
        Public Function XA_BUSINESS_UPLOAD_DOC_MESSAGE() As String
            Return "WEBSITE/XA_LOAN/BUSINESS_UPLOAD_DOC_MESSAGE"
        End Function

        Public Function XA_BUSINESS_UPLOAD_DOC_ENABLE() As String
            Return "WEBSITE/XA_LOAN/VISIBLE/@document_business_upload_enable"
        End Function

        Public Function BL_UPLOAD_DOC_MESSAGE() As String
			Return "WEBSITE/BUSINESS_LOAN/UPLOAD_DOC_MESSAGE"
		End Function

		Public Function BL_UPLOAD_DOC_ENABLE() As String
			Return "WEBSITE/BUSINESS_LOAN/VISIBLE/@document_upload_enable"
		End Function

		Public Function UPLOAD_DOC_ENABLE() As String
			Return "WEBSITE/VISIBLE/@document_upload_enable"
		End Function

		Public Function UPLOAD_DOC_MESSAGE() As String
			Return "WEBSITE/UPLOAD_DOC_MESSAGE"
		End Function

		Public Function XA_JOINT_ENABLE() As String
			Return "WEBSITE/XA_LOAN/VISIBLE/@joint_enable"
		End Function
		Public Function XA_COLLECT_JOB_TITLE_ONLY() As String
			Return "WEBSITE/XA_LOAN/VISIBLE/@collect_job_title_only"
		End Function

		Public Function SA_JOINT_ENABLE() As String
			Return "WEBSITE/XA_LOAN/VISIBLE/@joint_enable_sa"
		End Function

		Public Function XA_FUNDING_ACCOUNT_NUMBER_FREE_FORM() As String
			Return "WEBSITE/XA_LOAN/BEHAVIOR/@funding_account_number_free_form"
		End Function


		Public Function XA_LOCATION_POOL() As String
			Return "WEBSITE/@location_pool"
		End Function

		Public Function CREDIT_PULL() As String
			Return "WEBSITE/@credit_pull"
		End Function

		Public Function DECISIONXA_ENABLE() As String
			Return "WEBSITE/@decisionXA_enable"
		End Function

		Public Function CC_JOINT_ENABLE() As String
			Return "WEBSITE/VISIBLE/@joint_enable_cc"
		End Function
		Public Function HE_JOINT_ENABLE() As String
			Return "WEBSITE/VISIBLE/@joint_enable_he"
		End Function
		Public Function PL_JOINT_ENABLE() As String
			Return "WEBSITE/VISIBLE/@joint_enable_pl"
		End Function
		Public Function VL_JOINT_ENABLE() As String
			Return "WEBSITE/VISIBLE/@joint_enable_vl"
		End Function
		Public Function VL_TERM_FREE_FORM_MODE() As String
			Return "WEBSITE/VISIBLE/@term_free_form_mode_vl"
		End Function
		Public Function IDA() As String
			Return "WEBSITE/@ida"
		End Function
		Public Function DEBIT() As String
			Return "WEBSITE/@debit"
		End Function

		Public Function XA_STATUS() As String
			Return "WEBSITE/@loan_status_enum"
		End Function

		Public Function XA_FUNDING_ENABLE() As String
			Return "WEBSITE/@funding_enable"
		End Function

		Public Function XA_BOOKING() As String
			Return "WEBSITE/@booking"
		End Function

		Public Function GOOGLE_TAG_MANAGER_ID() As String
			Return "WEBSITE/@google_tag_manager_id"
		End Function

		Public Function PIWIK_SITE_ID() As String
			Return "WEBSITE/@piwik_site_id"
		End Function

        Public Function FACEBOOK_PIXEL_ID() As String
			Return "WEBSITE/@facebook_pixel_id"
		End Function

		Public Function LOAN_BOOKING(loanType As String) As String
			Return String.Format("WEBSITE/{0}/@loan_booking", loanType)
		End Function

		Public Function VIN_BARCODE_SCAN(loanType As String) As String
			Return String.Format("WEBSITE/{0}/VISIBLE/@vin_barcode_scan", loanType)
		End Function


		Public Function XSELL(loanType As String) As String
			Return String.Format("WEBSITE/{0}/VISIBLE/@auto_cross_qualify", loanType)
		End Function

		Public Function DOCU_SIGN(loanType As String) As String
			Return String.Format("WEBSITE/{0}/VISIBLE/@in_session_docu_sign", loanType)
		End Function

		Public Function DOCU_SIGN_PDF_GROUP(loanType As String) As String
			Return String.Format("WEBSITE/{0}/VISIBLE/@in_session_docu_sign_pdf_group_override", loanType)
		End Function

		Public Function LOGO_URL() As String
			Return "WEBSITE/@logo_url"
		End Function

		Public Function FAV_ICO() As String
			Return "WEBSITE/@fav_ico"
		End Function

		Public Function FOOTER_THEME() As String
			Return "WEBSITE/COLOR_SCHEME/@footer_theme"
		End Function

		Public Function FOOTER_FONT_THEME() As String
			Return "WEBSITE/COLOR_SCHEME/@footer_font_theme"
		End Function

		Public Function BUTTON_THEME() As String
			Return "WEBSITE/COLOR_SCHEME/@button_theme"
		End Function

		Public Function BUTTON_FONT_THEME() As String
			Return "WEBSITE/COLOR_SCHEME/@button_font_theme"
		End Function

		Public Function HEADER_THEME() As String
			Return "WEBSITE/COLOR_SCHEME/@header_theme"
		End Function

		Public Function HEADER2_THEME() As String
			Return "WEBSITE/COLOR_SCHEME/@header_theme2"
		End Function

		Public Function BACKGROUND_THEME() As String
			Return "WEBSITE/COLOR_SCHEME/@bg_theme"
		End Function

		Public Function CONTENT_THEME() As String
			Return "WEBSITE/COLOR_SCHEME/@content_theme"
		End Function

		Public Function RENAME_BUTTON_LABEL_ITEMS(loanType As String) As String
			Return String.Format("WEBSITE/CUSTOM_LIST/RENAME_BUTTON_LABEL/ITEM[@loan_type='{0}']", loanType)
		End Function

		Public Function SHOW_FIELDS_ITEMS(loanType As String) As String
			Return String.Format("WEBSITE/CUSTOM_LIST/SHOW_FIELDS/ITEM[@loan_type='{0}']", loanType)
		End Function

		Public Function REQUIRE_FIELDS_ITEMS(loanType As String) As String
			Return String.Format("WEBSITE/CUSTOM_LIST/REQUIRE_FIELDS/ITEM[@loan_type='{0}']", loanType)
		End Function

		Public Function CUSTOM_LIST_RATES(loanType As String) As String
			Return String.Format("WEBSITE/CUSTOM_LIST/RATES/ITEM[@loan_type='{0}']", loanType)
		End Function

		Public Function RENAME_BUTTON_LEGACY_LABEL() As String
			Return String.Format("WEBSITE/CUSTOM_LIST/RENAME_BUTTON_LABEL/ITEM")
		End Function

		Public Function ACCOUNTS(loanType As String) As String
			Return String.Format("WEBSITE/{0}/ACCOUNTS", loanType)
		End Function
		Public Function CUSTOM_QUESTIONS(loanType As String) As String
			Return String.Format("WEBSITE/{0}/CUSTOM_QUESTIONS", loanType)
		End Function
		Public Function CUSTOM_APPLICANT_QUESTIONS(loanType As String) As String
			Return String.Format("WEBSITE/{0}/CUSTOM_APPLICANT_QUESTIONS", loanType)
		End Function

		Public Function APPLICATION_BLOCK_RULES(loanType As String) As String
			If String.IsNullOrWhiteSpace(loanType) Then Return "WEBSITE/APPLICATION_BLOCK_RULES"
			Return String.Format("WEBSITE/{0}/APPLICATION_BLOCK_RULES", loanType)
		End Function
		Public Function CUSTOM_LIST_RULES(loanType As String) As String
			If String.IsNullOrWhiteSpace(loanType) Then Return "WEBSITE/CUSTOM_LIST_RULES"
			Return String.Format("WEBSITE/{0}/CUSTOM_LIST_RULES", loanType)
		End Function
		Public Function CUSTOM_APPLICATION_SCENARIOS(loanType As String) As String
			Return String.Format("WEBSITE/{0}/CUSTOM_APPLICATION_SCENARIOS", loanType)
		End Function

		Public Function ENABLE_CUSTOM_QUESTION_NEW_API() As String
			Return "WEBSITE/VISIBLE/@custom_question_new_api"
		End Function

        Public Function ADVANCED_LOGICS() As String
            Return "WEBSITE/ADVANCED_LOGICS"
        End Function
        Public Function LOAN_LOCATION_POOL(loanType As String) As String
            Return String.Format("WEBSITE/{0}/VISIBLE/@location_pool", loanType)
		End Function

		Public Function LOAN_DECLARATIONS(loanType As String) As String
			Return String.Format("WEBSITE/{0}/DECLARATIONS", loanType)
		End Function

		Public Function ENUM_VEHICLE_TYPES() As String
			Return "WEBSITE/ENUMS/VEHICLE_TYPES"
		End Function

		Public Function LQB_MESSAGES_CONTACT_INFO() As String
			Return "WEBSITE/LQB/MESSAGES/CONTACT_INFO"
		End Function
		Public Function LQB_MESSAGES_ELIGIBLE_LOAN_PROGRAM_DISCLAIMER() As String
			Return "WEBSITE/LQB/MESSAGES/ELIGIBLE_LOAN_PROGRAM_DISCLAIMER"
		End Function
		Public Function LQB_MESSAGES_FULL_APP_SUBMIT_CONFIRMATION() As String
			Return "WEBSITE/LQB/MESSAGES/FULL_APP_SUBMIT_CONFIRMATION"
		End Function
		Public Function LQB_MESSAGES_SIGNED_OUT_PAGE_MESSAGE() As String
			Return "WEBSITE/LQB/MESSAGES/SIGNED_OUT_PAGE_MESSAGE"
		End Function
		Public Function LQB_MESSAGES_GET_RATES_PAGE_DISCLAIMER() As String
			Return "WEBSITE/LQB/MESSAGES/GET_RATES_PAGE_DISCLAIMER"
		End Function
		Public Function LQB_MESSAGES_NO_ELIGIBLE_LOAN_PROGRAM_DISCLAIMER() As String
			Return "WEBSITE/LQB/MESSAGES/NO_ELIGIBLE_LOAN_PROGRAM"
		End Function
		Public Function LQB_MESSAGES_ONLINE_APP_PRIVACY_POLICY() As String
			Return "WEBSITE/LQB/MESSAGES/ONLINE_APP_PRIVACY_POLICY"
		End Function

		Public Function LQB_APPLICATION_DEFAULT_LOAN_TEMPLATE() As String
			Return "WEBSITE/LQB/APPLICATION/@default_loan_template"
		End Function
		Public Function LQB_APPLICATION_LOAN_OFFICER() As String
			Return "WEBSITE/LQB/APPLICATION/@loan_officer"
		End Function
		Public Function LQB_APPLICATION_SAVE_FULL_APP_AS() As String
			Return "WEBSITE/LQB/APPLICATION/@save_full_app_as"
		End Function

		Public Function LQB_BEHAVIOR_ALLOW_CONSUMER_TO_CREATE_APPS() As String
			Return "WEBSITE/LQB/BEHAVIOR/@allow_consumer_to_create_apps"
		End Function
		Public Function LQB_BEHAVIOR_AUTHENTICATION_VIA_LENDER() As String
			Return "WEBSITE/LQB/BEHAVIOR/@authentication_via_lender"
		End Function
		Public Function LQB_BEHAVIOR_AUTHENTICATION_VIA_LOCAL() As String
			Return "WEBSITE/LQB/BEHAVIOR/@authentication_via_local"
		End Function
		Public Function LQB_BEHAVIOR_SHOW_PRICER_AT_END() As String
			Return "WEBSITE/LQB/BEHAVIOR/@show_pricer_at_end"
		End Function
		Public Function LQB_BEHAVIOR_CREDIT_PULL() As String
			Return "WEBSITE/LQB/BEHAVIOR/@credit_pull"
		End Function
		Public Function LQB_CREDIT_REPORTING_AGENCY() As String
			Return "WEBSITE/LQB/@credit_reporting_agency"
		End Function
		Public Function LQB_PASSWORD() As String
			Return "WEBSITE/LQB/@password"
		End Function
		Public Function LQB_USER() As String
			Return "WEBSITE/LQB/@user"
		End Function
		Public Function LQB_PIWIK_SITE_ID() As String
			Return "WEBSITE/LQB/@piwik_site_id"
		End Function
		Public Function LQB_GOOGLE_TAG_MANAGER_ID() As String
			Return "WEBSITE/LQB/@google_tag_manager_id"
		End Function
		Public Function LQB_INSTATOUCH_ENABLED() As String
			Return "WEBSITE/LQB/@instatouch_enabled"
		End Function
		Public Function LQB_VOA_PREFILL_ENABLED() As String
			Return "WEBSITE/LQB/@voa_prefill_enabled"
		End Function
        Public Function LQB_ENVIRONMENT() As String
            Return "WEBSITE/LQB/@environment"
        End Function
    End Module
End Namespace
