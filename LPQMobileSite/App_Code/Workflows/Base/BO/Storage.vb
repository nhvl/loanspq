﻿
Imports Workflows.XA.BO
Imports LPQMobile.Utils

Namespace Workflows.Base.BO
	Public Class Storage(Of TS As {New, Submission}, TR As {State}, TC As {Config})
		Private _submissionData As TS
		Private _config As TC
		Private _sessionID As String
		Public ReadOnly Property SubmissionData As TS
			Get
				Return _submissionData
			End Get
		End Property
		
		Public Property State As TR
		Public ReadOnly Property Config As TC
			Get
				Return _config
			End Get
		End Property
		Public ReadOnly Property SessionID As String
			Get
				Return _sessionID
			End Get
		End Property
		Public ReadOnly Property StateSessionID As String
			Get
				Return _sessionID & "_State"
			End Get
		End Property
		Public ReadOnly Property RequestParamsSessionID As String
			Get
				Return _sessionID & "_RequestParam"
			End Get
		End Property
		Public Sub New(submissionData As TS, config As TC, state As TR, sessionID As String)
			_submissionData = submissionData
			If _submissionData Is Nothing Then _submissionData = New TS()
			_config = config
			Me.State = state
			_sessionID = sessionID
		End Sub
		Public Sub New(config As TC, state As TR, sessionID As String)
			_config = config
			_submissionData = New TS()
			Me.State = state
			_sessionID = sessionID
		End Sub
		Public Sub SetSubmissionData(data As TS)
			_submissionData = data
		End Sub
		Public Sub SetState(data As TR)
			_State = data
		End Sub
	End Class
End Namespace

