﻿Imports CustomList
Imports LPQMobile.Utils

Namespace Workflows.Base.BO
	Public Class Config
		Public Property SubmissionLimiterExemptIPs As List(Of CIPCommentsItem)
		Public Property SubmissionMaxAppBeforeNotification As Integer
		Public Property SubmissionMaxAppBeforeBlocking As Integer
		Public Property SubmissionLimiterInternalEmails As List(Of String)
		Public Property WebsiteConfig As CWebsiteConfig
		Public Property LoanType As String
		Public Property CustomListRuleList As List(Of CCustomListRuleItem)
	End Class
End Namespace

