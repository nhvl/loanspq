﻿Namespace Workflows.Base.BO
	<Serializable()>
	Public Class State
		Public Property IsUpdateReferralStatusRequired As Boolean
		Public Property ShouldKeepTheSessionDataPersistent As Boolean
		Public Sub New()
			ShouldKeepTheSessionDataPersistent = False
			IsUpdateReferralStatusRequired = True
		End Sub
	End Class
End Namespace

