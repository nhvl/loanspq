﻿Imports Workflows.Base.BO
Imports log4net
Imports LPQMobile.Utils.Common

Namespace Workflows.Base.Handlers

	Public Class CheckEModeHandler(Of TS As {New, Submission}, TR As {State}, TC As {Config})
		Inherits BaseHandler(Of TS, TR, TC)
		Private log As ILog = LogManager.GetLogger(GetType(CheckEModeHandler(Of TS, TR, TC)))
		Public Sub New(storage As Storage(Of TS, TR, TC), httpContext As HttpContext)
			MyBase.New(storage, httpContext)
		End Sub
		Protected Overrides Function Process() As Result
			If ConfigurationManager.AppSettings.Get("ENVIRONMENT") = "LIVE" Then
				If Not String.IsNullOrWhiteSpace(RequestParamAsString("previewcode")) OrElse RequestParamAsBoolean("eMode") Then
					CurrentHttpResponse.Write(PrependIconForNonApprovedMessage("[Submission result display here]", "Thank You"))
					Return Result.Terminate
				End If
			End If
			Return Result.MoveNext
		End Function

		Private Sub OnTerminated() Handles Me.Terminated
			log.Info("CheckEModeHandler: Terminated")
		End Sub
		Private Sub OnCompleted() Handles Me.Completed
			log.Info("CheckEModeHandler: Completed")
		End Sub
	End Class
End Namespace
