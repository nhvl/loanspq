﻿
Imports Workflows.Base.BO
Imports DBUtils
Imports log4net
Imports LPQMobile.Utils

Namespace Workflows.Base.Handlers
	Public Class ValidateIPHandler(Of TS As {New, Submission}, TR As {State}, TC As {Config})
		Inherits BaseHandler(Of TS, TR, TC)
		Private log As ILog = LogManager.GetLogger(GetType(ValidateIPHandler(Of TS, TR, TC)))

		Public Sub New(storage As Storage(Of TS, TR, TC), httpContext As HttpContext)
			MyBase.New(storage, httpContext)
		End Sub
		Protected Overrides Function Process() As Result
			Dim errMsg As String = ValidateIP()
			If Not String.IsNullOrEmpty(errMsg) Then
				log.Info(String.Format("{0}  IP: {1}/{2}", errMsg, CurrentHttpRequest.UserHostAddress, CurrentStorage.Config.WebsiteConfig.LenderRef))
				CurrentHttpResponse.Write(errMsg)
				Return Result.Terminate
			End If
			Return Result.MoveNext
		End Function
		''' <summary>
		''' 'handy function used to ignore certain pple.. ie: test pple, internal employee
		''' </summary>
		''' <returns>True when the IP is in the whitelist</returns>
		Private Function IsWhiteListIP() As Boolean

			'TODO: put in configuration
			Dim arrIgnoreIPs() As String = New String() {"192.168.111.8", "127.0.0.1", "::1"} ', "10.11.13.245", "127.0.0.1"
			'arrIgnoreIPs = oIgnore.GetAttribute("value").Split(","c)

			If IsNothing(HttpContext.Current) Then
				Return False
			End If

			Return Array.IndexOf(arrIgnoreIPs, CurrentHttpRequest.UserHostAddress) <> -1
		End Function


		Private Function CheckSubmissionLimiterExemptIP(p As CIPCommentsItem) As Boolean
			If p.IP = CurrentHttpRequest.UserHostAddress Then Return True
			If p.IP.EndsWith("*") Then '123.23.12.*
				Return CurrentHttpRequest.UserHostAddress.StartsWith(p.IP.Substring(0, p.IP.LastIndexOf("."c) + 1))
			End If
			Return False
		End Function

		''' <summary>
		''' Check the number of submitted app again the limiter.  This is mostl done in callback
		''' </summary>
		''' <returns>"null" for excluded IP; "blockmaxapps" when count exceeded limit </returns>
		Private Function ValidateIP() As String
			'temporary white list
			If IsWhiteListIP() Then
				Return ""
			ElseIf CurrentStorage.Config.SubmissionLimiterExemptIPs IsNot Nothing AndAlso CurrentStorage.Config.SubmissionLimiterExemptIPs.Any(AddressOf CheckSubmissionLimiterExemptIP) Then
				Return ""
			Else
				'If sHost.Contains("apptest") Or sHost.Contains("appstage") Or _CurrentLenderRef.NullSafeToUpper_.Contains("BCU") Or _CurrentLenderRef.NullSafeToLower_.Contains("comonefcu") Or _CurrentLenderRef.NullSafeToLower_.Contains("commonwealth") Then
				'	_nIPCount = 250
				'Else
				'	_nIPCount = 25
				'End If
				Dim counter As Integer = GetIPCount(CurrentHttpRequest.UserHostAddress, Guid.Parse(CurrentStorage.Config.WebsiteConfig.LenderId)) + 1
				If CurrentStorage.Config.SubmissionMaxAppBeforeNotification > 0 AndAlso counter = CurrentStorage.Config.SubmissionMaxAppBeforeNotification AndAlso counter < CurrentStorage.Config.SubmissionMaxAppBeforeBlocking AndAlso CurrentStorage.Config.SubmissionLimiterInternalEmails IsNot Nothing AndAlso CurrentStorage.Config.SubmissionLimiterInternalEmails.Count > 0 Then
					Common.SendSubmissionRateLimiterNotificationEmails(CurrentStorage.Config.SubmissionLimiterInternalEmails, counter, CurrentStorage.Config.SubmissionMaxAppBeforeBlocking, CurrentHttpRequest.UserHostAddress)
				End If

				' Use >= here because below, an extra logIP will raise the counter to 1 extra after a block.
				If CurrentStorage.Config.SubmissionMaxAppBeforeBlocking > 0 AndAlso counter >= CurrentStorage.Config.SubmissionMaxAppBeforeBlocking Then
					' Only send an email for the first submission to trigger the block.
					If counter = CurrentStorage.Config.SubmissionMaxAppBeforeBlocking Then
						If CurrentStorage.Config.SubmissionLimiterInternalEmails IsNot Nothing AndAlso CurrentStorage.Config.SubmissionLimiterInternalEmails.Count > 0 Then
							Common.SendSubmissionRateLimiterBlockingEmails(CurrentStorage.Config.SubmissionLimiterInternalEmails, CurrentStorage.Config.SubmissionMaxAppBeforeBlocking, CurrentHttpRequest.UserHostAddress)
						End If
						' Log one extra here, so that the next block that comes in will not incur an email.
						' If this doesn't occur, then counter = CurrentStorage.Config.SubmissionMaxAppBeforeBlocking will always occur.
						LogIP()
					End If
					Return "blockmaxapps"
				End If
			End If
			LogIP()
			Return ""
		End Function
		Private Sub LogIP()
			Dim oInsert As New cSQLInsertStringBuilder("IPTracker")
			oInsert.AppendValue("IPTrackerID", New CSQLParamValue(Guid.NewGuid))
			oInsert.AppendValue("LogTime", New CSQLParamValue(Now))
			oInsert.AppendValue("RemoteIP", New CSQLParamValue(CurrentHttpRequest.UserHostAddress))
			oInsert.AppendValue("RemoteHost", New CSQLParamValue(CurrentHttpRequest.Url.Host))
			oInsert.AppendValue("RemoteUrl", New CSQLParamValue(CurrentHttpRequest.Url.AbsolutePath))
			oInsert.AppendValue("OrgID", New CSQLParamValue(Common.SafeGUID(CurrentStorage.Config.WebsiteConfig.OrganizationId)))
			oInsert.AppendValue("LenderID", New CSQLParamValue(Common.SafeGUID(CurrentStorage.Config.WebsiteConfig.LenderId)))
			oInsert.AppendValue("APIUser", New CSQLParamValue(Common.SafeGUID(CurrentStorage.Config.WebsiteConfig.APIUser)))
			oInsert.AppendValue("LenderRef", New CSQLParamValue(CurrentStorage.Config.WebsiteConfig.LenderRef))
			Dim oDB As New CSQLDBUtils(LpqMobileConnectionString)
			Try
				oDB.executeNonQuery(oInsert.SQL, False)
			Catch ex As Exception
				'log.Error("Get Timezone by phone fail: " & ex.Message, ex)
				log.Error("SQL Insertion failure: " & ex.Message, ex)
			Finally
				oDB.closeConnection()
			End Try
		End Sub
		Private Function GetIPCount(ipAddress As String, lenderId As Guid) As Integer
			Dim oWhere As New CSQLWhereStringBuilder()
			'number of same ip within the time frame
			Dim sSql As String = "Select count(RemoteIP) from IPTracker"
			oWhere.AppendAndCondition("LogTime > DATEADD(HOUR,-1,GETDATE())")
			oWhere.AppendAndCondition("RemoteIP={0}", New CSQLParamValue(ipAddress))
			If lenderId <> Guid.Empty Then
				oWhere.AppendAndCondition("LenderID={0}", New CSQLParamValue(lenderId))
			End If
			Dim oDB As New CSQLDBUtils(LpqMobileConnectionString)
			Dim nIPCount As Integer = 0
			Try
				nIPCount = Common.SafeInteger(oDB.getScalerValue(sSql & oWhere.SQL))
			Catch ex As Exception
				'log.Error("Get Timezone by phone fail: " & ex.Message, ex)
				log.Error("SQL failure: " & ex.Message, ex)
			Finally
				oDB.closeConnection()
			End Try
			Return nIPCount

		End Function
		Private Sub OnTerminated() Handles Me.Terminated
			log.Info("ValidateIPHandler: Terminated")
		End Sub
		Private Sub OnCompleted() Handles Me.Completed
			log.Info("ValidateIPHandler: Completed")
		End Sub
	End Class
End Namespace
