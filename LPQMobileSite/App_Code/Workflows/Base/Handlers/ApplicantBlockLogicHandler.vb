﻿Imports Workflows.Base.BO
Imports log4net

Namespace Workflows.Base.Handlers

	Public Class ApplicantBlockLogicHandler(Of TS As {New, Submission}, TR As {State}, TC As {Config})
		Inherits BaseHandler(Of TS, TR, TC)
		Private log As ILog = LogManager.GetLogger(GetType(ApplicantBlockLogicHandler(Of TS, TR, TC)))
		Public Sub New(storage As Storage(Of TS, TR, TC), httpContext As HttpContext)
			MyBase.New(storage, httpContext)
		End Sub
		Protected Overrides Function Process() As Result
			Dim sReason As String = CApplicantBlockLogic.Execute(CurrentStorage.Config.WebsiteConfig, CurrentHttpRequest, CurrentStorage.Config.LoanType)
			If Not String.IsNullOrEmpty(sReason) Then
				CurrentStorage.State.IsUpdateReferralStatusRequired = False
				CurrentHttpResponse.Write(sReason)
				Return Result.Terminate
			End If
			Return Result.MoveNext
		End Function
		Private Sub OnTerminated() Handles Me.Terminated
			log.Info("ApplicantBlockLogicHandler: Terminated")
		End Sub
		Private Sub OnCompleted() Handles Me.Completed
			log.Info("ApplicantBlockLogicHandler: Completed")
		End Sub
	End Class
End Namespace