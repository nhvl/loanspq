﻿
Imports Workflows.Base.BO
Imports LPQMobile.Utils
Namespace Workflows.Base.Handlers
	Public MustInherit Class BaseHandler(Of TS As {New, Submission}, TR As {State}, TC As {Config})
		Private _storage As Storage(Of TS, TR, TC)
		Private _httpContext As HttpContext
		Public ReadOnly Property LpqMobileConnectionString As String
			Get
				Return Web.Configuration.WebConfigurationManager.ConnectionStrings("LPQMOBILE_CONNECTIONSTRING").ConnectionString
			End Get
		End Property
		Public ReadOnly Property CurrentHttpContext As HttpContext
			Get
				Return _httpContext
			End Get
		End Property
		Public ReadOnly Property CurrentStorage As Storage(Of TS, TR, TC)
			Get
				Return _storage
			End Get
		End Property
		Public ReadOnly Property CurrentHttpRequest As HttpRequest
			Get
				Return _httpContext.Request
			End Get
		End Property
		Public ReadOnly Property CurrentHttpResponse As HttpResponse
			Get
				Return _httpContext.Response
			End Get
		End Property
		Public Property NextHandler As BaseHandler(Of TS, TR, TC)  ' this handler is to move the process to the next handler
		'Private ReadOnly _exitHandler As Action(Of Storage(Of TS, TR))	' this procedure is invoked in case we need to terminate the handler chain and response something to client
		'Private ReadOnly _completedHandler As Action(Of Storage(Of TS, TR))	' this procedure plays the role of Page_Completed, when we need updateLoan or something like that before the process ended or terminated

		'Public Sub New(storage As Storage(Of TS, TR), nextHandler As BaseHandler(Of TS, TR), exitHandler As Action(Of Storage(Of TS, TR)), completedHandler As Action(Of Storage(Of TS, TR)))
		'	_storage = storage
		'	_nextHandler = nextHandler
		'	_exitHandler = exitHandler
		'	_completedHandler = completedHandler
		'End Sub
		Public Sub New(storage As Storage(Of TS, TR, TC), httpContext As HttpContext)
			_storage = storage
			_httpContext = httpContext
		End Sub

		Protected MustOverride Function Process() As Result
		Public Event Terminated()
		Public Event Completed()
		Public Sub Run()
			Dim result = Process()
			If result = result.MoveNext Then
				RaiseEvent Completed()
				If _NextHandler IsNot Nothing Then _NextHandler.Run()
			Else
				RaiseEvent Terminated()
			End If
		End Sub
		Protected Function RequestParamAsString(ByVal name As String) As String
			Return Common.SafeString(_httpContext.Request.Params(name))
		End Function
		Protected Function RequestParamAsBoolean(ByVal name As String) As Boolean
			Return Common.SafeBoolean(_httpContext.Request.Params(name))
		End Function
		Protected Function RequestParamAsDouble(ByVal name As String) As Double
			Return Common.SafeDouble(_httpContext.Request.Params(name))
		End Function
		Protected Function RequestParamAsInteger(ByVal name As String) As Integer
			Return Common.SafeInteger(_httpContext.Request.Params(name))
		End Function
		Protected Function RequestParamAsGuid(ByVal name As String) As Guid
			Return Common.SafeGUID(_httpContext.Request.Params(name))
		End Function
	End Class
End Namespace

