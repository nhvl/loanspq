﻿
Imports Workflows.Base.BO
Imports log4net
Imports LPQMobile.Utils

Namespace Workflows.Base.Handlers
	Public Class ValidateTokenHandler(Of TS As {New, Submission}, TR As {State}, TC As {Config})
		Inherits BaseHandler(Of TS, TR, TC)
		Private log As ILog = LogManager.GetLogger(GetType(ValidateTokenHandler(Of TS, TR, TC)))

		Public Sub New(storage As Storage(Of TS, TR, TC), httpContext As HttpContext)
			MyBase.New(storage, httpContext)
		End Sub
		Protected Overrides Function Process() As Result
			If ValidateToken(RequestParamAsString("CSRF")) Then
				Return Result.MoveNext
			End If
			CurrentStorage.State.IsUpdateReferralStatusRequired = False
			log.Info("Invalid Token")
			CurrentHttpResponse.Write("<b><span style='color: red'>For security reason, your session has timed out. Please restart your browser.</span></b><br/>")
			Return Result.Terminate
		End Function
		Private Function ValidateToken(ByVal psToken As String) As Boolean
			If psToken = "" Then Return False
			Dim sStoredToken = Common.SafeString(CurrentHttpContext.Session("token"))
			log.Info("validate session ID: " & HttpContext.Current.Session.SessionID & "; token: " & psToken)
			If sStoredToken = "" Then
				log.Info("Invalid Token: No session found.")
				'Dim sessionId = System.Web.HttpContext.Current.Session.SessionID
				'_log.info("validate session ID: " & sessionId & "; token: " & psToken)
				Return False
			End If

			Dim nValue As Long = (Common.SafeLong(sStoredToken.Substring(32)) * 3) + 733	'roll the integer part of the token start from index 32 to
			nValue = (nValue * 3) + 733	 '2nd time because it is done twice on client side
			Dim sRolledToken As String = sStoredToken.Substring(0, 32) & Common.SafeString(nValue)
			Dim bMatch As Boolean = False

			'account for revisiting page, multiple click due to fail validation
			For i = 0 To 20
				If psToken = sRolledToken Then
					bMatch = True
					log.Info("Token Matched with " & i & " iteration")
					Exit For
				Else
					nValue = (nValue * 3) + 733
					sRolledToken = sStoredToken.Substring(0, 32) & Common.SafeString(nValue)
				End If
			Next

			If bMatch Then
				'want to allow a leat 6 submit to account for authentication questions
				Dim nSubmitCount As Integer = Common.SafeInteger(CurrentHttpContext.Session("token_count"))
				nSubmitCount += 1
				CurrentHttpContext.Session("token_count") = nSubmitCount
				If nSubmitCount >= 6 Then
					CurrentHttpContext.Session("token") = Nothing
					CurrentHttpContext.Session("token_count") = Nothing
				End If
				Return True
			End If
			Return False
		End Function
		Private Sub OnTerminated() Handles Me.Terminated
			log.Info("ValidateTokenHandler: Terminated")
		End Sub
		Private Sub OnCompleted() Handles Me.Completed
			log.Info("ValidateTokenHandler: Completed")
		End Sub
	End Class
End Namespace
