﻿Imports Workflows.XA.Handlers
Imports log4net
Imports Workflows.Base.BO
Imports Workflows.Base.Handlers

Namespace Workflows.Base
	Public MustInherit Class BaseWorkflow(Of TS As {New, Submission}, TR As {State}, TC As {Config})
		Private log As ILog = LogManager.GetLogger(Me.GetType())
		Private _nodeList As List(Of BaseHandler(Of TS, TR, TC))
		Public ReadOnly Property Nodes As List(Of BaseHandler(Of TS, TR, TC))
			Get
				Return _nodeList
			End Get
		End Property
		Private _storage As Storage(Of TS, TR, TC)
		Private _context As HttpContext
		Public ReadOnly Property CurrentStorage As Storage(Of TS, TR, TC)
			Get
				Return _storage
			End Get
		End Property
		Public ReadOnly Property CurrentHttpContext As HttpContext
			Get
				Return _context
			End Get
		End Property

		Public Sub New(httpContext As HttpContext, storage As Storage(Of TS, TR, TC))
			_nodeList = New List(Of BaseHandler(Of TS, TR, TC))()
			_context = httpContext
			_storage = storage
			LoadPersistentData()
			CurrentStorage.State.ShouldKeepTheSessionDataPersistent = False	'reset
			Dim registeredNodes = RegisterNodes()
			If registeredNodes IsNot Nothing AndAlso registeredNodes.Count > 0 Then
				For i As Integer = 0 To registeredNodes.Count - 1
					_nodeList.Add(registeredNodes(i))
					AddHandler registeredNodes(i).Terminated, AddressOf OnHandlerTerminated
					If i > 0 Then
						registeredNodes(i - 1).NextHandler = registeredNodes(i)
					End If
				Next
				AddHandler _nodeList.Last().Completed, AddressOf OnLastHanlderCompleted
				AddHandler _nodeList.Last().Terminated, AddressOf OnLastHanlderTerminated
			End If
		End Sub
		Private Sub OnHandlerTerminated()
			RaiseEvent Terminated()	'workflow ended
			If CurrentStorage.State.ShouldKeepTheSessionDataPersistent Then
				SavePersistentData()
				'log.Debug("save session " & CurrentStorage.SessionID)
			Else
				ClearAllPersistentData()
				'log.Debug("clear session " & CurrentStorage.SessionID)
			End If
		End Sub
		Private Sub OnLastHanlderCompleted() ' workflow completed without response.write anythings
			RaiseEvent Completed()
		End Sub
		Private Sub OnLastHanlderTerminated() ' workflow completed with response.write somethings
			RaiseEvent Completed()
		End Sub

		Public Sub Start()
			If _nodeList.Count > 0 Then
				RaiseEvent BeforeStart()
				_nodeList.First().Run()
			End If
		End Sub

		Protected MustOverride Function RegisterNodes() As List(Of BaseHandler(Of TS, TR, TC))
		Protected Event BeforeStart()
		Protected Event Terminated()
		Protected Event Completed()
		Protected MustOverride Sub SavePersistentData()
		Protected MustOverride Sub LoadPersistentData()
		Protected Overridable Sub ClearAllPersistentData()
			CurrentHttpContext.Session.Remove(CurrentStorage.StateSessionID)
			CurrentHttpContext.Session.Remove(CurrentStorage.RequestParamsSessionID)
			CurrentHttpContext.Session.Remove("StartedDate_" & CurrentStorage.SessionID)
		End Sub
	End Class
End Namespace

