﻿Imports CustomList
Imports LPQMobile.Utils

Namespace Workflows.XA.BO
	Public Class XaConfig
		Inherits Base.BO.Config
		Public Property FundingAndBookingExecutingMode As String
		Public Sub New(loanType As String, customListRuleList As List(Of CCustomListRuleItem))
			Me.LoanType = loanType
			Me.CustomListRuleList = customListRuleList
		End Sub
		Public ReadOnly Property LpqContinueIdaReferral As Boolean
			Get
				Return Common.getNodeAttributes(WebsiteConfig, "XA_LOAN/BEHAVIOR", "continue_on_id_authentication_referral").Equals("Y")
			End Get
		End Property
	End Class
End Namespace