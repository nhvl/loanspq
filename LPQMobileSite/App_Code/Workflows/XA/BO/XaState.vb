﻿
Namespace Workflows.XA.BO
	<Serializable()>
	Public Class XaState
		Inherits Base.BO.State
		Public Property IsFailedIDA As Boolean
		Public Property IsFailPreUnderWrite As Boolean
		Public Property CustomErrorMessage As String
		Public Property CustomPreapprovedSuccessMessage As String
		Public Property CustomSubmittedSuccessMessage As String
		Public Property CustomDeclinedMessage As String
		Public Property CustomSubmittedDataList As List(Of String)
		Public Property CustomPreapprovedDataList As List(Of String)
		Public Property CustomDeclinedDataList As List(Of String)
		Public Property LoanRequestXMLStr As String
		Public Property LoanResponseXMLStr As String
		Public Property LoanID As String
		Public Property LoanNumber As String
		Public Property SettingProductList As List(Of CProduct)
		Public Property PaypalReceipt As List(Of CPayPalReceiptItem)
		Public Property TotalFunding As Double
		Public Property IsFraud As Boolean
		Public Property InternalCommentAppend As String
		Public Property TransactionID As String
		Public Property QuestionSetID As String
		Public Property RequestAppID As String
		Public Property RequestClientID As String
		Public Property RequestSequenceID As String
		Public Property ReferenceNumber As String
		Public Property WalletQuestionList As List(Of CWalletQuestionRendering.questionDataClass)

		Public Property RequestParams As NameValueCollection
		Public Property IsDoneWalletQuestionForCoApp As String
		Public Property CrossSellMsg As String
		Public Property PaymentID As String
		Public Sub New()
			CustomErrorMessage = "There was an error while submitting your application. <div id='MLerrorMessage'></div>"
			CustomPreapprovedSuccessMessage = "Your application has been preapproved.  We will contact you shortly."
			CustomSubmittedSuccessMessage = "Thank you for your application."
			CustomDeclinedMessage = "Thank you for your application."
			CustomSubmittedDataList = New List(Of String)
			CustomPreapprovedDataList = New List(Of String)
			CustomDeclinedDataList = New List(Of String)
			IsFraud = False
			IsFailPreUnderWrite = False
		End Sub
	End Class
End Namespace

