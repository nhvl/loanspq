﻿Imports LPQMobile.BO

Namespace Workflows.XA.BO
	<Serializable()>
	Public Class XaSubmission
		Inherits Base.BO.Submission
		Private _availability As String
		Public Sub New()
			MyBase.New()
		End Sub
		Public Property Availability As String
			Get
				Return _availability
			End Get
			Set(value As String)
				If New Regex("^([12][abs]?)$").IsMatch(value) Then
					_availability = value
				Else
					_availability = "1"
				End If
			End Set
		End Property

		Public ReadOnly Property IsSecondary As Boolean
			Get
				Return Availability.StartsWith("2")
			End Get
		End Property
		Public Property SelectedProducts As List(Of SelectProducts)
		Public Property IsSSO As Boolean = False
		Public Property CustomQuestionAnswers As List(Of CValidatedQuestionAnswers)
		Public Property CustomQuestions As List(Of CCustomQuestionXA)
		Public Property SelectedLocationPool As List(Of String)
		Public Property Disclosure As String
		Public Property MinorAccountCode As String
		Public Property IsMinorDecisionXaEnable As Boolean
		Public Property MinorUnderWrite As CMinorAccount
		Public Property SsnFOMAnswer As String
		Public Property MinorRoleType As String
		Public Property SecondRoleType As String
		Public Property IsOnlyMinorApp As String
		Public Property ESignature As String

		Public Property BABusinessInfo As CBABusinessInfo
		Public Property BAApplicants As List(Of CBAApplicantInfo)
		Public Property NoneApplicantBeneficialOwners As List(Of CBeneficialOwnerInfo)
		Public Property SAAccountInfo As CSAAccountInfo
		Public Property SAApplicants As List(Of CSAApplicantInfo)
		Public Property SaApplicantNames As New Dictionary(Of String, String)
		Public Property Personal As CXAPersonalInfo
		Public Property Contact As CContactInfo
		Public Property IDCard As CIdentificationInfo
		Public Property Address As CAddressInfo
		Public Property PreviousAddress As CAddressInfo
		Public Property MailingAddress As CAddressInfo
		Public Property ApplicantQuestionList As List(Of CValidatedQuestionAnswers)
		Public Property HasJointApplicant As Boolean
		Public Property CoAppPersonal As CXAPersonalInfo
		Public Property CoAppIDCard As CIdentificationInfo
		Public Property CoAppAddress As CAddressInfo
		Public Property CoAppPreviousAddress As CAddressInfo
		Public Property CoAppMailingAddress As CAddressInfo
		Public Property CoAppContact As CContactInfo
		Public Property CoAppApplicantQuestionList As List(Of CValidatedQuestionAnswers)
		Public Property BeneficiaryList As List(Of CBeneficiaryInfo)
		Public Property AccountList As List(Of AccountInfo)
		Public Property DocBase64Dic As Dictionary(Of String, String)
		Public Property UploadDocumentInfoList As List(Of uploadDocumentInfo)
		Public Property LoanInfo As CLoanInfo
		Public Property HasMinorApplicant As Boolean
		Public Property MinorAppPersonal As CXAPersonalInfo
		Public Property MinorAppContact As CContactInfo
		Public Property MinorAppIDCard As CIdentificationInfo
		Public Property MinorAppAddress As CAddressInfo
		Public Property MinorAppMailingAddress As CAddressInfo
		Public Property MinorApplicantQuestionList As List(Of CValidatedQuestionAnswers)
		Public Property FundingSourceInfo As CFundingSourceInfo
		Public Property FOMQuestionsAndAnswers As List(Of Dictionary(Of String, String))
		Public Property HasSSOIDA As String
		Public Property SAAccountCode As String
		Public Property HasCoWalletQuestions As String
		Public Property ApplicantIndex As Integer
		Public Property HasCoFollowOnQuestions As String
		Public Property WalletQuestionsAndAnswers As String
		Public Property WalletAnswersText As String
		Public Property WalletQuestionApplicantIndex As Integer
		Public Property WalletQuestionIterationIndex As Integer
		Public Property SelectedFundingSource As String
	End Class
End Namespace

