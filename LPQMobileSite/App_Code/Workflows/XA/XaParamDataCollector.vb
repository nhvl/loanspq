﻿Imports System.Reflection
Imports Workflows.XA.Handlers
Imports log4net
Imports Workflows.XA.BO
Imports LPQMobile.BO
Imports Newtonsoft.Json
Imports Microsoft.VisualBasic
Imports LPQMobile.Utils
Imports System.Web.Script.Serialization
Imports Workflows.XA
Imports LPQMobile.Utils.Common

Public Class XaParamDataCollector
	Private requestParams As NameValueCollection
	Private websiteConfig As CWebsiteConfig
	Private log As ILog = LogManager.GetLogger(Me.GetType())
	Public Sub New(params As NameValueCollection, config As CWebsiteConfig)
		requestParams = params
		websiteConfig = config
	End Sub
	Public Sub CollectSubmissionData(submissionData As XaSubmission)
		Dim xaMisc As New XaMisc(websiteConfig)
		With submissionData
			.Availability = RequestParamAsString("Availability")
			.IsSSO = RequestParamAsString("IsSSO").Equals("Y", StringComparison.OrdinalIgnoreCase)
			.HasSSOIDA = RequestParamAsString("HasSSOIDA")
			Dim sSelectedLocationPool As String = RequestParamAsString("SelectedLocationPool")
			If Not String.IsNullOrWhiteSpace(sSelectedLocationPool) Then
				.SelectedLocationPool = JsonConvert.DeserializeObject(Of List(Of String))(sSelectedLocationPool)
			End If

			' Custom Question Answer
			Dim jsSerializer As New JavaScriptSerializer()

			' Only get Application answers, not Applicant
			Dim lstApplicationCustomAnswers As List(Of CApplicantQA) =
				jsSerializer.Deserialize(Of List(Of CApplicantQA))(requestParams("CustomAnswers")).
				Where(Function(cqa) Not String.IsNullOrEmpty(cqa.CQAnswer) AndAlso cqa.CQRole = QuestionRole.Application).ToList()
			' Collect URL Parameter CQ Answers, if applicable
			Dim bIsUrlParaCustomQuestion = Common.SafeString(requestParams("IsUrlParaCustomQuestion")) = "Y"
			Dim lstUrlParaCustomAnswers As New List(Of CApplicantQA)
			If bIsUrlParaCustomQuestion Then
				lstUrlParaCustomAnswers = jsSerializer.Deserialize(Of List(Of CApplicantQA))(Common.SafeString(requestParams("UrlParaCustomQuestionAndAnswers")))
			End If
			' Set validated CQ answers
			.CustomQuestionAnswers = CCustomQuestionNewAPI.
				getValidatedCustomQuestionAnswers(websiteConfig, "XA", QuestionRole.Application, .Availability, lstApplicationCustomAnswers, lstUrlParaCustomAnswers.Where(Function(a) a.CQRole = QuestionRole.Application)).ToList()

			.Disclosure = RequestParamAsString("Disclosure")
			'TODO: need to migrate clients and remove this in 2019
			.MinorAccountCode = RequestParamAsString("MinorAccountCode")
			If Not String.IsNullOrEmpty(.MinorAccountCode) Then
				.MinorUnderWrite = xaMisc.SetUnderwriteMinor(.MinorAccountCode)
				.IsMinorDecisionXaEnable = .MinorUnderWrite.decisionXAEnable = "Y"

			End If
			.SsnFOMAnswer = RequestParamAsString("ssnFOMAnswer")
			.MinorRoleType = RequestParamAsString("MinorRoleType")
			.SecondRoleType = RequestParamAsString("SecondRoleType")
			.SecondRoleType = RequestParamAsString("isOnlyMinorApp")
			.ESignature = RequestParamAsString("eSignature")


			Dim businessAccountMode As Boolean = False
			If .Availability = "1b" OrElse .Availability = "2b" Then ' business account
				businessAccountMode = True

				Dim baAccountType As String = RequestParamAsString("baAccountType")
				Dim selectedAccountType As CBusinessAccountType = Nothing
				If Not String.IsNullOrEmpty(baAccountType) Then
					Dim businessAccountTypeList = GetBusinessAccountTypeList(websiteConfig)
					If businessAccountTypeList IsNot Nothing Then
						selectedAccountType = businessAccountTypeList.FirstOrDefault(Function(p) p.BusinessType.ToUpper() = baAccountType.ToUpper())
						'If selectedAccountType IsNot Nothing AndAlso selectedAccountType.ShowSpecialInfo Then
						If selectedAccountType IsNot Nothing Then  'alway have to post special node
							.BABusinessInfo = collectBABusinessInfoData()
							.BABusinessInfo.AccountCode = selectedAccountType.AccountCode
							.BABusinessInfo.BusinessType = selectedAccountType.BusinessType
						End If
					End If
				End If

				'TODO: use collectSAApplicantInfoData, 2019
				Dim baAppRolesJsonStr As String = RequestParamAsString("baAppRoles")
				Dim baApplicants As New List(Of CBAApplicantInfo)
				If Not String.IsNullOrEmpty(baAppRolesJsonStr) Then
					For Each str As String In JsonConvert.DeserializeObject(Of List(Of String))(baAppRolesJsonStr)
                        Dim app = collectBAApplicantInfoData(str, selectedAccountType, lstUrlParaCustomAnswers, .Availability)
                        If app IsNot Nothing Then
							Dim roleConfig As CBusinessAccountRole = selectedAccountType.Roles.FirstOrDefault(Function(p) p.RoleType.ToUpper() = app.RoleType.ToUpper())
							If roleConfig IsNot Nothing Then
								app.AllowOfac = roleConfig.AllowOfac
								app.AllowDebit = roleConfig.AllowDebit
							End If
						End If
						baApplicants.Add(app)
					Next
				End If
				.BAApplicants = baApplicants
				Dim beneficialOwners = JsonConvert.DeserializeObject(Of List(Of CBeneficialOwnerInfo))(RequestParamAsString("ba_BeneficialOwner"))
				If beneficialOwners IsNot Nothing AndAlso beneficialOwners.Count > 0 Then
					.NoneApplicantBeneficialOwners = beneficialOwners
				End If

			ElseIf .Availability = "1s" OrElse .Availability = "2s" Then ' special account
				.SAAccountCode = RequestParamAsString("saAccountCode")
				Dim selectedAccountType As CSpecialAccountType = Nothing
				If Not String.IsNullOrEmpty(.SAAccountCode) Then
					Dim specialAccountTypeList = GetSpecialAccountTypeList(websiteConfig)
					If specialAccountTypeList IsNot Nothing Then
						selectedAccountType = specialAccountTypeList.FirstOrDefault(Function(p) p.AccountCode.ToUpper() = .SAAccountCode.ToUpper())
						If selectedAccountType IsNot Nothing Then
							'<SPECIAL_INFO special_account_type_code="BC1001" is needed regardless of CONFIG ITEM NAMED "SHOWSPECIALINFO".  Backend uses this info for mapping Special Account 
							.SAAccountInfo = collectSAAccountInfoData()
							.SAAccountInfo.AccountTypeCode = selectedAccountType.AccountCode
						End If
					End If
				End If

				'TODO: use collectSAApplicantInfoData, 2019
				Dim saAppRolesJsonStr As String = RequestParamAsString("saAppRoles")
				Dim saApplicants As New List(Of CSAApplicantInfo)
				If Not String.IsNullOrEmpty(saAppRolesJsonStr) Then
					Dim saAppNamesPersisted = New Dictionary(Of String, String)
					For Each str As String In JsonConvert.DeserializeObject(Of List(Of String))(saAppRolesJsonStr)
                        Dim app = collectSAApplicantInfoData(str, selectedAccountType, lstUrlParaCustomAnswers, .Availability)
                        If app IsNot Nothing Then
							Dim roleConfig As CSpecialAccountRole = selectedAccountType.Roles.FirstOrDefault(Function(p) p.RoleType.ToUpper() = app.RoleType.ToUpper())
							app.AllowOfac = roleConfig.AllowOfac
							app.AllowDebit = roleConfig.AllowDebit
						End If
						saAppNamesPersisted.Add(str, app.FirstName)
						.SaApplicantNames = saAppNamesPersisted
						saApplicants.Add(app)
					Next
				End If
				.SAApplicants = saApplicants

				' AP-1838 - LPQ in-branch encounters problems when the first Special XA account is not Minor, if any minor accounts exist in the loan. 
				' Re-order SAApplicants to ensure all Minor applicants are first, before any other applicants. Do it here when building the loan object
				' in order to ensure it gets submitted in the correct order to in-branch.
				Dim saApplicantsMinorsFirst = New List(Of CSAApplicantInfo)()
				' Import minors
				saApplicantsMinorsFirst.AddRange(.SAApplicants.Where(Function(app) app.RoleType.Equals("MINOR", StringComparison.OrdinalIgnoreCase)))
				' Import non-minors
				saApplicantsMinorsFirst.AddRange(.SAApplicants.Where(Function(app) Not app.RoleType.Equals("MINOR", StringComparison.OrdinalIgnoreCase)))
				.SAApplicants = saApplicantsMinorsFirst
			End If


			If Not businessAccountMode AndAlso (.SAApplicants Is Nothing OrElse .SAApplicants.Count = 0) Then
				'TODO: refactor to get rid of this loop in 2019
				'' load personal info
				Dim personalInfo As New CXAPersonalInfo
				personalInfo.EmployeeOfLender = RequestParamAsString("EmployeeOfLender")
				personalInfo.MemberNumber = RequestParamAsString("MemberNumber")
				personalInfo.FirstName = RequestParamAsString("FirstName")
				personalInfo.LastName = RequestParamAsString("LastName")
				personalInfo.MiddleName = RequestParamAsString("MiddleName")
				personalInfo.Gender = RequestParamAsString("Gender")
				personalInfo.Suffix = RequestParamAsString("NameSuffix")
				personalInfo.DateOfBirth = RequestParamAsString("DOB")
				personalInfo.HasMinor = RequestParamAsString("HasMinorApplicant")
				If RequestParamAsString("EmploymentStatus") IsNot Nothing Then
					personalInfo.EmploymentSectionEnabled = True
					personalInfo.EmploymentStatus = RequestParamAsString("EmploymentStatus")
					personalInfo.EmploymentDescription = RequestParamAsString("EmploymentDescription")
					personalInfo.txtJobTitle = RequestParamAsString("txtJobTitle")
					personalInfo.txtEmployedDuration_month = RequestParamAsString("txtEmployedDuration_month")
					personalInfo.txtEmployedDuration_year = RequestParamAsString("txtEmployedDuration_year")
					personalInfo.txtEmployer = RequestParamAsString("txtEmployer")
					'End If
					' new employment logic
					personalInfo.txtBusinessType = RequestParamAsString("txtBusinessType")
					''enlistment date for active military
					Dim employmentStatus = CEnum.MapEmploymentStatusToValue(personalInfo.EmploymentStatus)
					Dim enlistmentDate As String = RequestParamAsString("EnlistmentDate")
					If (employmentStatus = "MI" And enlistmentDate <> "") Then
						personalInfo.txtEmploymentStartDate = Common.SafeString(enlistmentDate)
					Else
						personalInfo.txtEmploymentStartDate = RequestParamAsString("txtEmploymentStartDate")
					End If
					personalInfo.txtETS = RequestParamAsString("txtETS")
					personalInfo.txtProfessionDuration_month = RequestParamAsString("txtProfessionDuration_month")
					personalInfo.txtProfessionDuration_year = RequestParamAsString("txtProfessionDuration_year")
					personalInfo.txtSupervisorName = RequestParamAsString("txtSupervisorName")
					personalInfo.ddlBranchOfService = RequestParamAsString("ddlBranchOfService")
					personalInfo.ddlPayGrade = RequestParamAsString("ddlPayGrade")
					personalInfo.txtGrossMonthlyIncome = RequestParamAsDouble("txtGrossMonthlyIncome")
					' end new employment logic

					'prev employment
					personalInfo.hasPrevEmployment = RequestParamAsString("hasPreviousEmployment")
					If (personalInfo.hasPrevEmployment = "Y") Then
						personalInfo.prev_EmploymentStatus = RequestParamAsString("prev_EmploymentStatus")
						personalInfo.prev_GrossMonthlyIncome = RequestParamAsDouble("prev_grossMonthlyIncome")
						If personalInfo.prev_EmploymentStatus = "STUDENT" Or personalInfo.prev_EmploymentStatus = "HOMEMAKER" Then
							personalInfo.prev_txtJobTitle = personalInfo.prev_EmploymentStatus
						ElseIf personalInfo.prev_EmploymentStatus = "RETIRED" Or personalInfo.prev_EmploymentStatus = "UNEMPLOYED" Then
							personalInfo.prev_txtJobTitle = personalInfo.prev_EmploymentStatus
							personalInfo.prev_txtEmployedDuration_month = RequestParamAsString("prev_txtEmployedDuration_month")
							personalInfo.prev_txtEmployedDuration_year = RequestParamAsString("prev_txtEmployedDuration_year")
						Else
							personalInfo.prev_txtJobTitle = RequestParamAsString("prev_txtJobTitle")
							personalInfo.prev_txtEmployedDuration_month = RequestParamAsString("prev_txtEmployedDuration_month")
							personalInfo.prev_txtEmployedDuration_year = RequestParamAsString("prev_txtEmployedDuration_year")
							personalInfo.prev_txtEmployer = RequestParamAsString("prev_txtEmployer")
						End If


						personalInfo.prev_txtBusinessType = RequestParamAsString("prev_txtBusinessType")
						''enlistment date for active military
						Dim prevEmploymentStatus = CEnum.MapEmploymentStatusToValue(personalInfo.prev_EmploymentStatus)
						Dim prevEnlistmentDate As String = RequestParamAsString("prev_EnlistmentDate")
						If (prevEmploymentStatus = "MI" And prevEnlistmentDate <> "") Then
							personalInfo.prev_txtEmploymentStartDate = prevEnlistmentDate
						Else
							personalInfo.prev_txtEmploymentStartDate = RequestParamAsString("prev_txtEmploymentStartDate")
						End If
						personalInfo.prev_txtETS = RequestParamAsString("prev_txtETS")
						personalInfo.prev_ddlBranchOfService = RequestParamAsString("prev_ddlBranchOfService")
						personalInfo.prev_ddlPayGrade = RequestParamAsString("prev_ddlPayGrade")
					End If
					'end prev employment
				End If


				personalInfo.Citizenship = RequestParamAsString("Citizenship")
				personalInfo.MotherMaidenName = RequestParamAsString("MotherMaidenName")
				personalInfo.SSN = RequestParamAsString("SSN")
				personalInfo.MaritalStatus = RequestParamAsString("MaritalStatus")
				If requestParams("HousingPayment") IsNot Nothing Then
					personalInfo.HousingPayment = RequestParamAsDouble("HousingPayment")
				End If
				.Personal = personalInfo



				' load contact info
				Dim contact As New CContactInfo
				contact.CellPhone = RequestParamAsString("MobilePhone")
				contact.CellPhoneCountry = RequestParamAsString("MobilePhoneCountry")
				contact.Email = RequestParamAsString("EmailAddr")
				contact.HomePhone = RequestParamAsString("HomePhone")
				contact.HomePhoneCountry = RequestParamAsString("HomePhoneCountry")
				contact.PreferredContactMethod = RequestParamAsString("ContactMethod")
				contact.WorkPhone = RequestParamAsString("WorkPhone")
				contact.WorkPhoneCountry = RequestParamAsString("WorkPhoneCountry")
				contact.WorkPhoneExt = RequestParamAsString("WorkPhoneEXT")
				.Contact = contact


				' load identification info
				Dim idCardInfo As New CIdentificationInfo
				idCardInfo.CardNumber = RequestParamAsString("IDCardNumber")
				idCardInfo.CardType = RequestParamAsString("IDCardType")
				idCardInfo.Country = RequestParamAsString("IDCountry")
				idCardInfo.DateIssued = RequestParamAsString("IDDateIssued")
				idCardInfo.ExpirationDate = RequestParamAsString("IDDateExpire")
				idCardInfo.State = RequestParamAsString("IDState")
				.IDCard = idCardInfo

				'' load address info
				Dim addressInfo As New CAddressInfo
				addressInfo.Address = RequestParamAsString("AddressStreet")
				addressInfo.Address2 = RequestParamAsString("AddressStreet2")
				addressInfo.City = RequestParamAsString("AddressCity")
				addressInfo.State = RequestParamAsString("AddressState")
				addressInfo.Zip = RequestParamAsString("AddressZip")
				addressInfo.Country = RequestParamAsString("Country")
				''---make sure occupancy status is existing
				'' previous address
				Dim previousAddress As New CAddressInfo
				If Not String.IsNullOrEmpty(RequestParamAsString("OccupyingLocation")) Then
					addressInfo.OccupancyDuration = RequestParamAsInteger("LiveMonths")
					addressInfo.OccupancyStatus = RequestParamAsString("OccupyingLocation")
					addressInfo.OccupancyDescription = RequestParamAsString("OccupancyDescription")
					Dim hasPreviousAddress As String = RequestParamAsString("hasPreviousAddress")
					If Not String.IsNullOrEmpty(hasPreviousAddress) Then
						If hasPreviousAddress = "Y" Then
							previousAddress.Address = RequestParamAsString("PreviousAddressStreet")
							previousAddress.Address2 = RequestParamAsString("PreviousAddressStreet2")
							previousAddress.City = RequestParamAsString("PreviousAddressCity")
							previousAddress.State = RequestParamAsString("PreviousAddressState")
							previousAddress.Zip = RequestParamAsString("PreviousAddressZip")
						Else
							previousAddress.Address = ""
							previousAddress.Address2 = ""
							previousAddress.City = ""
							previousAddress.State = ""
							previousAddress.Zip = ""
						End If
					End If
				End If
				.PreviousAddress = previousAddress
				.Address = addressInfo
				''mailing address
				Dim mailingAddress As New CAddressInfo
				Dim hasMailingAddress As String = RequestParamAsString("hasMailingAddress")
				If Not String.IsNullOrEmpty(hasMailingAddress) Then
					If hasMailingAddress = "Y" Then
						mailingAddress.Address = RequestParamAsString("MailingAddressStreet")
						mailingAddress.Address2 = RequestParamAsString("MailingAddressStreet2")
						mailingAddress.City = RequestParamAsString("MailingAddressCity")
						mailingAddress.State = RequestParamAsString("MailingAddressState")
						mailingAddress.Zip = RequestParamAsString("MailingAddressZip")
						mailingAddress.Country = RequestParamAsString("MailingAddressCountry")
					Else
						mailingAddress.Address = ""
						mailingAddress.Address2 = ""
						mailingAddress.City = ""
						mailingAddress.State = ""
						mailingAddress.Zip = ""
						mailingAddress.Country = ""
					End If
				End If
				.MailingAddress = mailingAddress

				' Applicant custom question answers 
				Dim lstApplicantCustomAnswers As List(Of CApplicantQA) =
					jsSerializer.Deserialize(Of List(Of CApplicantQA))(requestParams("CustomAnswers")).
					Where(Function(cqa) Not String.IsNullOrEmpty(cqa.CQAnswer) AndAlso cqa.CQRole = QuestionRole.Applicant AndAlso IIf(cqa.CQLocation = CustomQuestionLocation.ApplicantPage, cqa.CQApplicantPrefix = "", True)).ToList()
				' Set validated CQ answers
				.ApplicantQuestionList = CCustomQuestionNewAPI.
					getValidatedCustomQuestionAnswers(websiteConfig, "XA", QuestionRole.Applicant, .Availability, lstApplicantCustomAnswers, lstUrlParaCustomAnswers.Where(Function(urlQA) urlQA.CQRole = QuestionRole.Applicant))

				.HasJointApplicant = RequestParamAsBoolean("HasJointApplicant")
				.CoAppPersonal = Nothing
				If .HasJointApplicant Then
					Dim coAppPersonalInfo As New CXAPersonalInfo
					coAppPersonalInfo.EmployeeOfLender = RequestParamAsString("co_EmployeeOfLender")
					coAppPersonalInfo.MemberNumber = RequestParamAsString("co_MemberNumber")
					coAppPersonalInfo.RelationshipToPrimary = RequestParamAsString("co_RelationshipToPrimary")
					coAppPersonalInfo.FirstName = RequestParamAsString("co_FirstName")
					coAppPersonalInfo.LastName = RequestParamAsString("co_LastName")
					coAppPersonalInfo.MiddleName = RequestParamAsString("co_MiddleName")
					coAppPersonalInfo.Gender = RequestParamAsString("co_Gender")
					coAppPersonalInfo.Suffix = RequestParamAsString("co_NameSuffix")
					coAppPersonalInfo.DateOfBirth = RequestParamAsString("co_DOB")
					If requestParams("co_EmploymentStatus") IsNot Nothing Then
						coAppPersonalInfo.EmploymentSectionEnabled = True
						coAppPersonalInfo.EmploymentStatus = RequestParamAsString("co_EmploymentStatus")
						coAppPersonalInfo.EmploymentDescription = RequestParamAsString("co_EmploymentDescription")
						coAppPersonalInfo.txtJobTitle = RequestParamAsString("co_txtJobTitle")
						coAppPersonalInfo.txtEmployedDuration_month = RequestParamAsString("co_txtEmployedDuration_month")
						coAppPersonalInfo.txtEmployedDuration_year = RequestParamAsString("co_txtEmployedDuration_year")
						coAppPersonalInfo.txtEmployer = RequestParamAsString("co_txtEmployer")
						''End If
						' new employment logic
						coAppPersonalInfo.txtBusinessType = RequestParamAsString("co_txtBusinessType")

						''enlistment date for active military
						Dim coEmploymentStatus = CEnum.MapEmploymentStatusToValue(coAppPersonalInfo.EmploymentStatus)
						Dim coEnlistmentDate As String = RequestParamAsString("co_EnlistmentDate")
						If (coEmploymentStatus = "MI" And coEnlistmentDate <> "") Then
							coAppPersonalInfo.txtEmploymentStartDate = Common.SafeString(coEnlistmentDate)
						Else
							coAppPersonalInfo.txtEmploymentStartDate = RequestParamAsString("co_txtEmploymentStartDate")
						End If
						coAppPersonalInfo.txtETS = RequestParamAsString("co_txtETS")
						coAppPersonalInfo.txtProfessionDuration_month = RequestParamAsString("co_txtProfessionDuration_month")
						coAppPersonalInfo.txtProfessionDuration_year = RequestParamAsString("co_txtProfessionDuration_year")
						coAppPersonalInfo.txtSupervisorName = RequestParamAsString("co_txtSupervisorName")
						coAppPersonalInfo.ddlBranchOfService = RequestParamAsString("co_ddlBranchOfService")
						coAppPersonalInfo.ddlPayGrade = RequestParamAsString("co_ddlPayGrade")
						coAppPersonalInfo.txtGrossMonthlyIncome = RequestParamAsDouble("co_txtGrossMonthlyIncome")
						' end new employment logic


						'prev employment
						coAppPersonalInfo.hasPrevEmployment = RequestParamAsString("co_hasPreviousEmployment")
						If (coAppPersonalInfo.hasPrevEmployment = "Y") Then
							coAppPersonalInfo.prev_EmploymentStatus = RequestParamAsString("co_prev_EmploymentStatus")
							coAppPersonalInfo.prev_GrossMonthlyIncome = RequestParamAsDouble("co_prev_grossMonthlyIncome")
							If coAppPersonalInfo.prev_EmploymentStatus = "STUDENT" Or coAppPersonalInfo.prev_EmploymentStatus = "HOMEMAKER" Then
								coAppPersonalInfo.prev_txtJobTitle = coAppPersonalInfo.prev_EmploymentStatus
							ElseIf coAppPersonalInfo.prev_EmploymentStatus = "RETIRED" Or coAppPersonalInfo.prev_EmploymentStatus = "UNEMPLOYED" Then
								coAppPersonalInfo.prev_txtJobTitle = coAppPersonalInfo.prev_EmploymentStatus
								coAppPersonalInfo.prev_txtEmployedDuration_month = RequestParamAsString("co_prev_txtEmployedDuration_month")
								coAppPersonalInfo.prev_txtEmployedDuration_year = RequestParamAsString("co_prev_txtEmployedDuration_year")
							Else
								coAppPersonalInfo.prev_txtJobTitle = RequestParamAsString("co_prev_txtJobTitle")
								coAppPersonalInfo.prev_txtEmployedDuration_month = RequestParamAsString("co_prev_txtEmployedDuration_month")
								coAppPersonalInfo.prev_txtEmployedDuration_year = RequestParamAsString("co_prev_txtEmployedDuration_year")
								coAppPersonalInfo.prev_txtEmployer = RequestParamAsString("co_prev_txtEmployer")
							End If


							coAppPersonalInfo.prev_txtBusinessType = RequestParamAsString("co_prev_txtBusinessType")
							''enlistment date for active military
							Dim coPrevEmploymentStatus = CEnum.MapEmploymentStatusToValue(coAppPersonalInfo.prev_EmploymentStatus)
							Dim coPrevEnlistmentDate As String = RequestParamAsString("co_prev_EnlistmentDate")
							If (coPrevEmploymentStatus = "MI" And coPrevEnlistmentDate <> "") Then
								coAppPersonalInfo.prev_txtEmploymentStartDate = Common.SafeString(coPrevEnlistmentDate)
							Else
								coAppPersonalInfo.prev_txtEmploymentStartDate = RequestParamAsString("co_prev_txtEmploymentStartDate")
							End If
							coAppPersonalInfo.prev_txtETS = RequestParamAsString("co_prev_txtETS")
							coAppPersonalInfo.prev_ddlBranchOfService = RequestParamAsString("co_prev_ddlBranchOfService")
							coAppPersonalInfo.prev_ddlPayGrade = RequestParamAsString("co_prev_ddlPayGrade")
						End If
						'end prev employment
					End If


					coAppPersonalInfo.Citizenship = RequestParamAsString("co_Citizenship")
					coAppPersonalInfo.MotherMaidenName = RequestParamAsString("co_MotherMaidenName")
					coAppPersonalInfo.SSN = RequestParamAsString("co_SSN")
					coAppPersonalInfo.MaritalStatus = RequestParamAsString("co_MaritalStatus")
					If requestParams("co_HousingPayment") IsNot Nothing Then
						coAppPersonalInfo.HousingPayment = RequestParamAsDouble("co_HousingPayment")
					End If

					.CoAppPersonal = coAppPersonalInfo
					Dim coIdCardInfo As New CIdentificationInfo
					coIdCardInfo.CardNumber = RequestParamAsString("co_IDCardNumber")
					coIdCardInfo.CardType = RequestParamAsString("co_IDCardType")
					coIdCardInfo.Country = RequestParamAsString("co_IDCountry")
					coIdCardInfo.DateIssued = RequestParamAsString("co_IDDateIssued")
					coIdCardInfo.ExpirationDate = RequestParamAsString("co_IDDateExpire")
					coIdCardInfo.State = RequestParamAsString("co_IDState")
					.CoAppIDCard = coIdCardInfo

					Dim coAddress As New CAddressInfo
					coAddress.Address = RequestParamAsString("co_AddressStreet")
					coAddress.Address2 = RequestParamAsString("co_AddressStreet2")
					coAddress.City = RequestParamAsString("co_AddressCity")
					coAddress.State = RequestParamAsString("co_AddressState")
					coAddress.Zip = RequestParamAsString("co_AddressZip")
					coAddress.Country = RequestParamAsString("co_Country")
					''---make sure occupancy status is existing
					'' previous address
					Dim coPreviousAddress As New CAddressInfo
					If Not String.IsNullOrEmpty(RequestParamAsString("co_OccupyingLocation")) Then
						coAddress.OccupancyDuration = RequestParamAsInteger("co_LiveMonths")
						coAddress.OccupancyStatus = RequestParamAsString("co_OccupyingLocation")
						coAddress.OccupancyDescription = RequestParamAsString("co_OccupancyDescription")
						Dim hasCoPreviousAddress As String = RequestParamAsString("co_hasPreviousAddress")
						If Not String.IsNullOrEmpty(hasCoPreviousAddress) Then
							If hasCoPreviousAddress = "Y" Then
								coPreviousAddress.Address = RequestParamAsString("co_PreviousAddressStreet")
								coPreviousAddress.Address2 = RequestParamAsString("co_PreviousAddressStreet2")
								coPreviousAddress.City = RequestParamAsString("co_PreviousAddressCity")
								coPreviousAddress.State = RequestParamAsString("co_PreviousAddressState")
								coPreviousAddress.Zip = RequestParamAsString("co_PreviousAddressZip")
							Else
								coPreviousAddress.Address = ""
								coPreviousAddress.Address2 = ""
								coPreviousAddress.City = ""
								coPreviousAddress.State = ""
								coPreviousAddress.Zip = ""
							End If
						End If
					End If
					.CoAppPreviousAddress = coPreviousAddress
					.CoAppAddress = coAddress
					''co mailing address
					Dim coMailingAddress As New CAddressInfo
					Dim hasCoMailingAddress As String = RequestParamAsString("co_hasMailingAddress")
					If Not String.IsNullOrEmpty(hasCoMailingAddress) Then
						If hasCoMailingAddress = "Y" Then
							coMailingAddress.Address = RequestParamAsString("co_MailingAddressStreet")
							coMailingAddress.Address2 = RequestParamAsString("co_MailingAddressStreet2")
							coMailingAddress.City = RequestParamAsString("co_MailingAddressCity")
							coMailingAddress.State = RequestParamAsString("co_MailingAddressState")
							coMailingAddress.Zip = RequestParamAsString("co_MailingAddressZip")
							coMailingAddress.Country = RequestParamAsString("co_MailingAddressCountry")
						Else
							coMailingAddress.Address = ""
							coMailingAddress.Address2 = ""
							coMailingAddress.City = ""
							coMailingAddress.State = ""
							coMailingAddress.Zip = ""
							coMailingAddress.Country = ""
						End If
					End If

					.CoAppMailingAddress = coMailingAddress
					Dim coContact As New CContactInfo
					coContact.CellPhone = RequestParamAsString("co_MobilePhone")
					coContact.CellPhoneCountry = RequestParamAsString("co_MobilePhoneCountry")
					coContact.Email = RequestParamAsString("co_EmailAddr")
					coContact.HomePhone = RequestParamAsString("co_HomePhone")
					coContact.HomePhoneCountry = RequestParamAsString("co_HomePhoneCountry")
					coContact.PreferredContactMethod = RequestParamAsString("co_ContactMethod")
					coContact.WorkPhone = RequestParamAsString("co_WorkPhone")
					coContact.WorkPhoneCountry = RequestParamAsString("co_WorkPhoneCountry")
					coContact.WorkPhoneExt = RequestParamAsString("co_WorkPhoneEXT")
					.CoAppContact = coContact

					' Co Applicant custom question answers 
					Dim lstCoApplicantCustomAnswers As List(Of CApplicantQA) =
						jsSerializer.Deserialize(Of List(Of CApplicantQA))(requestParams("CustomAnswers")).
						Where(Function(cqa) Not String.IsNullOrEmpty(cqa.CQAnswer) AndAlso cqa.CQRole = QuestionRole.Applicant AndAlso IIf(cqa.CQLocation = CustomQuestionLocation.ApplicantPage, cqa.CQApplicantPrefix = "co_", True)).ToList()
					' Set validated CQ answers
					.CoAppApplicantQuestionList = CCustomQuestionNewAPI.
						getValidatedCustomQuestionAnswers(websiteConfig, "XA", QuestionRole.Applicant, .Availability, lstCoApplicantCustomAnswers, lstUrlParaCustomAnswers.Where(Function(urlQA) urlQA.CQRole = QuestionRole.Applicant))
				End If
				Dim beneficiaryList As New List(Of CBeneficiaryInfo)
				Dim sBeneficiaryInfo As String = RequestParamAsString("beneficiaryInfo")
				If Not String.IsNullOrEmpty(sBeneficiaryInfo) Then
					beneficiaryList = JsonConvert.DeserializeObject(Of List(Of CBeneficiaryInfo))(sBeneficiaryInfo)
					If beneficiaryList IsNot Nothing AndAlso beneficiaryList.Count > 0 Then
						.BeneficiaryList = beneficiaryList
					End If
				End If
				''--Minor Applicant
				.HasMinorApplicant = RequestParamAsString("HasMinorApplicant") = "Y"
				If .HasMinorApplicant Then
					CreateMinorLoanApp(submissionData)
				End If
			End If


			''just get all accounts in the accountslist instead


			Dim productsList As New List(Of SelectProducts)
			Dim sSelectedProducts = RequestParamAsString("SelectedProducts")
			If Not String.IsNullOrWhiteSpace(sSelectedProducts) Then
				productsList = JsonConvert.DeserializeObject(Of List(Of SelectProducts))(sSelectedProducts)
			End If
			Dim accountInfoList As New List(Of AccountInfo)
			Dim i As Integer
			''make sure oProductsList has at least one products
			If productsList.Count > 0 Then
				For i = 0 To productsList.Count - 1
					Dim acountInfor As New AccountInfo(websiteConfig)
					acountInfor.Amount = SafeDouble(productsList(i).depositAmount)
					acountInfor.CustomQuestions = productsList(i).productQuestions
					acountInfor.Product = productsList(i).productName
					acountInfor.ProductServices = productsList(i).productServices
					acountInfor.ProductRate = productsList(i).productRate
					acountInfor.Term = productsList(i).term
					acountInfor.Apy = productsList(i).apy
					accountInfoList.Add(acountInfor)
				Next
			End If
			.AccountList = accountInfoList

			'===all upload document
			Dim selectDocsList As List(Of CSelectDocument)
			Dim docSerializer As New JavaScriptSerializer()
			Dim docList As New Dictionary(Of String, String)
			''by default MaxJsonLenth = 102400, set it =int32.maxvalue = 2147483647 
			docSerializer.MaxJsonLength = Int32.MaxValue
			If Not String.IsNullOrEmpty(requestParams("Image")) Then
				'TODO
				selectDocsList = docSerializer.Deserialize(Of List(Of CSelectDocument))(requestParams("Image"))
				If selectDocsList.Count > 0 Then
					Dim j As Integer
					For j = 0 To selectDocsList.Count - 1
						Dim oTitle As String = selectDocsList(j).title
						Dim obase64data As String = selectDocsList(j).base64data
						docList.Add(oTitle, obase64data)
					Next
					If docList.Count = 0 Then
						log.Info("Can't de-serialize image ")
					End If
				End If
				.DocBase64Dic = docList
			End If
			''get upload document info if it exist
			Dim uploadDocInfoList As New List(Of uploadDocumentInfo)
			If Not String.IsNullOrEmpty(requestParams("Image")) Then
				uploadDocInfoList = JsonConvert.DeserializeObject(Of List(Of uploadDocumentInfo))(requestParams("UploadDocInfor"))
			End If
			.UploadDocumentInfoList = uploadDocInfoList
			Dim loanInfo As New CLoanInfo
			loanInfo.account_position = IIf(.IsSecondary, "2", "1").ToString()
			loanInfo.referral_source = RequestParamAsString("ReferralSource")
			loanInfo.LoanOfficerID = RequestParamAsString("LoanOfficerID")
			.LoanInfo = loanInfo
			Dim rawFundingSource As String = RequestParamAsString("rawFundingSource")
			If Not String.IsNullOrWhiteSpace(rawFundingSource) Then
				.FundingSourceInfo = JsonConvert.DeserializeObject(Of CFundingSourceInfo)(rawFundingSource)
			End If
			Dim sFOMQuestionsAndAnswers As String = RequestParamAsString("FOMQuestionsAndAnswers")
			If Not String.IsNullOrWhiteSpace(sFOMQuestionsAndAnswers) Then
				.FOMQuestionsAndAnswers = JsonConvert.DeserializeObject(Of List(Of Dictionary(Of String, String)))(sFOMQuestionsAndAnswers)
			End If
			.HasCoWalletQuestions = RequestParamAsString("hasCoWalletQuestions").ToUpper()
			.HasCoFollowOnQuestions = RequestParamAsString("hasCoFollowOnQuestions")
			.WalletQuestionsAndAnswers = RequestParamAsString("WalletQuestionsAndAnswers")
			.WalletAnswersText = RequestParamAsString("WalletAnswersText")
			.WalletQuestionIterationIndex = RequestParamAsInteger("WalletQuestionIterationIndex")
			.WalletQuestionApplicantIndex = RequestParamAsInteger("WalletQuestionApplicantIndex")
			.SelectedFundingSource = RequestParamAsString("SelectedFundingSource")
		End With
	End Sub

	Private Function collectBABusinessInfoData() As CBABusinessInfo
		Dim result As New CBABusinessInfo()
		Const prefix As String = "ba_BusinessInfo"
		Dim type As Type = GetType(CBABusinessInfo)
		For Each prop In type.GetProperties(BindingFlags.Public Or BindingFlags.Instance)
			If prop Is Nothing Then Continue For
			If Not requestParams.AllKeys.Any(Function(k) k = prefix & prop.Name) Then Continue For
			Try
				Select Case prop.PropertyType
					Case GetType(String)
						prop.SetValue(result, SafeString(requestParams(prefix & prop.Name)))
					Case GetType(Integer)
						prop.SetValue(result, SafeInteger(requestParams(prefix & prop.Name)))
					Case GetType(Guid)
						prop.SetValue(result, SafeGUID(requestParams(prefix & prop.Name)))
					Case GetType(Boolean)
						prop.SetValue(result, SafeBoolean(requestParams(prefix & prop.Name)))
					Case GetType(Date)
						prop.SetValue(result, SafeDate(requestParams(prefix & prop.Name)))
					Case GetType(Double)
						prop.SetValue(result, SafeDouble(requestParams(prefix & prop.Name)))
				End Select
			Catch ex As Exception
				log.Error(ex)
			End Try
		Next
		Return result
	End Function
    Private Function collectBAApplicantInfoData(prefix As String, accountType As CBusinessAccountType, lstUrlParaCustomAnswers As List(Of CApplicantQA), availability As String) As CBAApplicantInfo
        Dim result As New CBAApplicantInfo()
        Dim type As Type = GetType(CBAApplicantInfo)
        For Each prop In type.GetProperties(BindingFlags.Public Or BindingFlags.Instance)
            If prop Is Nothing Then Continue For
            Try
                Dim propName As String = prop.Name
                'for backward compability with old user control submission form data
                Select Case prop.Name
                    Case "GrossMonthlyIncome"
                        propName = "txtGrossMonthlyIncome"
                    Case "EmployedDurationMonth"
                        propName = "txtEmployedDuration_month"
                    Case "EmployedDurationYear"
                        propName = "txtEmployedDuration_year"
                    Case "Employer"
                        propName = "txtEmployer"
                    Case "JobTitle"
                        propName = "txtJobTitle"
                    Case "BusinessType"
                        propName = "txtBusinessType"
                    Case "EmploymentStartDate"
                        propName = "txtEmploymentStartDate"
                    Case "ETS"
                        propName = "txtETS"
                    Case "ProfessionDurationMonth"
                        propName = "txtProfessionDuration_month"
                    Case "ProfessionDurationYear"
                        propName = "txtProfessionDuration_year"
                    Case "SupervisorName"
                        propName = "txtSupervisorName"
                    Case "BranchOfService"
                        propName = "ddlBranchOfService"
                    Case "PayGrade"
                        propName = "ddlPayGrade"
                        'prev
                    Case "PrevEmploymentStartDate"
                        propName = "prev_txtEmploymentStartDate"
                    Case "PrevEmploymentStatus"
                        propName = "prev_EmploymentStatus"
                    Case "PrevGrossMonthlyIncome"
                        propName = "prev_grossMonthlyIncome"
                    Case "PrevEmployedDuration_month"
                        propName = "prev_txtEmployedDuration_month"
                    Case "PrevEmployedDuration_year"
                        propName = "prev_txtEmployedDuration_year"
                    Case "PrevJobTitle"
                        propName = "prev_txtJobTitle"
                    Case "PrevEmployer"
                        propName = "prev_txtEmployer"
                    Case "PrevBusinessType"
                        propName = "prev_txtBusinessType"
                    Case "PrevEnlistmentDate"
                        propName = "prev_EnlistmentDate"
                    Case "PrevBranchOfService"
                        propName = "prev_ddlBranchOfService"
                    Case "PrevPayGrade"
                        propName = "prev_ddlPayGrade"
                    Case "PrevETS"
                        propName = "prev_txtETS"
                    Case "ApplicantQuestionAnswers"
                        Dim jsSerializer As New JavaScriptSerializer()
						Dim lstApplicantCustomAnswers As List(Of CApplicantQA) = jsSerializer.Deserialize(Of List(Of CApplicantQA))(requestParams("CustomAnswers")).
						 Where(Function(cqa) Not String.IsNullOrEmpty(cqa.CQAnswer) AndAlso cqa.CQRole = QuestionRole.Applicant AndAlso IIf(cqa.CQLocation = CustomQuestionLocation.ApplicantPage, cqa.CQApplicantPrefix = prefix, True)).ToList()
						' Set validated CQ answers
						Dim ApplicantQuestionList = CCustomQuestionNewAPI.getValidatedCustomQuestionAnswers(websiteConfig, "XA", QuestionRole.Applicant, availability, lstApplicantCustomAnswers, lstUrlParaCustomAnswers.Where(Function(urlQA) urlQA.CQRole = QuestionRole.Applicant))
                        prop.SetValue(result, ApplicantQuestionList)
                End Select
                If Not requestParams.AllKeys.Any(Function(k) k = prefix & propName) Then Continue For

                'end backward compability
                Select Case prop.PropertyType
                    Case GetType(String)
                        prop.SetValue(result, SafeString(requestParams(prefix & propName)))
                    Case GetType(Integer)
                        prop.SetValue(result, SafeInteger(requestParams(prefix & propName)))
                    Case GetType(Guid)
                        prop.SetValue(result, SafeGUID(requestParams(prefix & propName)))
                    Case GetType(Boolean)
                        prop.SetValue(result, SafeBoolean(requestParams(prefix & propName)))
                    Case GetType(Date)
                        prop.SetValue(result, SafeDate(requestParams(prefix & propName)))
                    Case GetType(Double)
                        prop.SetValue(result, SafeDouble(requestParams(prefix & propName)))
                End Select
            Catch ex As Exception
                log.Error(ex)
            End Try
        Next
        If CEnum.MapEmploymentStatusToValue(result.EmploymentStatus) = "MI" AndAlso Not String.IsNullOrEmpty(SafeString(result.EnlistmentDate).Trim()) Then
            result.EmploymentStartDate = result.EnlistmentDate
        End If
        If result.PrevEmploymentStatus = "STUDENT" Or result.PrevEmploymentStatus = "HOMEMAKER" Or result.PrevEmploymentStatus = "RETIRED" Or result.PrevEmploymentStatus = "UNEMPLOYED" Then
            result.PrevJobTitle = result.PrevEmploymentStatus
        End If
        Return result
    End Function
    Private Function collectSAAccountInfoData() As CSAAccountInfo
		Dim result As New CSAAccountInfo()
		Const prefix As String = "sa_AccInfo"
		Dim type As Type = GetType(CSAAccountInfo)
		For Each prop In type.GetProperties(BindingFlags.Public Or BindingFlags.Instance)
			If prop Is Nothing Then Continue For
			If Not requestParams.AllKeys.Any(Function(k) k = prefix & prop.Name) Then Continue For
			Try
				Select Case prop.PropertyType
					Case GetType(String)
						prop.SetValue(result, SafeString(requestParams(prefix & prop.Name)))
					Case GetType(Integer)
						prop.SetValue(result, SafeInteger(requestParams(prefix & prop.Name)))
					Case GetType(Guid)
						prop.SetValue(result, SafeGUID(requestParams(prefix & prop.Name)))
					Case GetType(Boolean)
						prop.SetValue(result, SafeBoolean(requestParams(prefix & prop.Name)))
					Case GetType(Date)
						prop.SetValue(result, SafeDate(requestParams(prefix & prop.Name)))
					Case GetType(Double)
						prop.SetValue(result, SafeDouble(requestParams(prefix & prop.Name)))
				End Select
			Catch ex As Exception
				log.Error(ex)
			End Try
		Next
		Return result
	End Function
    Private Function collectSAApplicantInfoData(prefix As String, accountType As CSpecialAccountType, lstUrlParaCustomAnswers As List(Of CApplicantQA), availability As String) As CSAApplicantInfo
        Dim result As New CSAApplicantInfo()
        Dim type As Type = GetType(CSAApplicantInfo)
        For Each prop In type.GetProperties(BindingFlags.Public Or BindingFlags.Instance)
            If prop Is Nothing Then Continue For
            Try
                Dim propName As String = prop.Name
                'for backward compability with old user control submission form data
                Select Case prop.Name
                    Case "GrossMonthlyIncome"
                        propName = "txtGrossMonthlyIncome"
                    Case "EmployedDurationMonth"
                        propName = "txtEmployedDuration_month"
                    Case "EmployedDurationYear"
                        propName = "txtEmployedDuration_year"
                    Case "Employer"
                        propName = "txtEmployer"
                    Case "JobTitle"
                        propName = "txtJobTitle"
                    Case "BusinessType"
                        propName = "txtBusinessType"
                    Case "EmploymentStartDate"
                        propName = "txtEmploymentStartDate"
                    Case "ETS"
                        propName = "txtETS"
                    Case "ProfessionDurationMonth"
                        propName = "txtProfessionDuration_month"
                    Case "ProfessionDurationYear"
                        propName = "txtProfessionDuration_year"
                    Case "SupervisorName"
                        propName = "txtSupervisorName"
                    Case "BranchOfService"
                        propName = "ddlBranchOfService"
                    Case "PayGrade"
                        propName = "ddlPayGrade"
                        'prev
                    Case "PrevEmploymentStartDate"
                        propName = "prev_txtEmploymentStartDate"
                    Case "PrevEmploymentStatus"
                        propName = "prev_EmploymentStatus"
                    Case "PrevGrossMonthlyIncome"
                        propName = "prev_grossMonthlyIncome"
                    Case "PrevEmployedDuration_month"
                        propName = "prev_txtEmployedDuration_month"
                    Case "PrevEmployedDuration_year"
                        propName = "prev_txtEmployedDuration_year"
                    Case "PrevJobTitle"
                        propName = "prev_txtJobTitle"
                    Case "PrevEmployer"
                        propName = "prev_txtEmployer"
                    Case "PrevBusinessType"
                        propName = "prev_txtBusinessType"
                    Case "PrevEnlistmentDate"
                        propName = "prev_EnlistmentDate"
                    Case "PrevBranchOfService"
                        propName = "prev_ddlBranchOfService"
                    Case "PrevPayGrade"
                        propName = "prev_ddlPayGrade"
                    Case "PrevETS"
                        propName = "prev_txtETS"
                    Case "Beneficiaries"
                        If requestParams(prefix & propName) IsNot Nothing Then
                            prop.SetValue(result, JsonConvert.DeserializeObject(Of List(Of CBeneficiaryInfo))(requestParams(prefix & propName)))
                        End If
                    Case "ApplicantQuestionAnswers"
                        Dim jsSerializer As New JavaScriptSerializer()
						Dim lstApplicantCustomAnswers As List(Of CApplicantQA) = jsSerializer.Deserialize(Of List(Of CApplicantQA))(requestParams("CustomAnswers")).
						 Where(Function(cqa) Not String.IsNullOrEmpty(cqa.CQAnswer) AndAlso cqa.CQRole = QuestionRole.Applicant AndAlso IIf(cqa.CQLocation = CustomQuestionLocation.ApplicantPage, cqa.CQApplicantPrefix = prefix, True)).ToList()
						' Set validated CQ answers
						Dim ApplicantQuestionList = CCustomQuestionNewAPI.getValidatedCustomQuestionAnswers(websiteConfig, "XA", QuestionRole.Applicant, availability, lstApplicantCustomAnswers, lstUrlParaCustomAnswers.Where(Function(urlQA) urlQA.CQRole = QuestionRole.Applicant))
                        prop.SetValue(result, ApplicantQuestionList)
                End Select
                If Not requestParams.AllKeys.Any(Function(k) k = prefix & propName) Then Continue For
                'end backward compability
                Select Case prop.PropertyType
                    Case GetType(String)
                        prop.SetValue(result, SafeString(requestParams(prefix & propName)))
                    Case GetType(Integer)
                        prop.SetValue(result, SafeInteger(requestParams(prefix & propName)))
                    Case GetType(Guid)
                        prop.SetValue(result, SafeGUID(requestParams(prefix & propName)))
                    Case GetType(Boolean)
                        prop.SetValue(result, SafeBoolean(requestParams(prefix & propName)))
                    Case GetType(Date)
                        prop.SetValue(result, SafeDate(requestParams(prefix & propName)))
                    Case GetType(Double)
                        prop.SetValue(result, SafeDouble(requestParams(prefix & propName)))
                End Select
            Catch ex As Exception
                log.Error(ex)
            End Try
        Next
        If CEnum.MapEmploymentStatusToValue(result.EmploymentStatus) = "MI" AndAlso Not String.IsNullOrEmpty(SafeString(result.EnlistmentDate).Trim()) Then
            result.EmploymentStartDate = result.EnlistmentDate
        End If
        If result.PrevEmploymentStatus = "STUDENT" Or result.PrevEmploymentStatus = "HOMEMAKER" Or result.PrevEmploymentStatus = "RETIRED" Or result.PrevEmploymentStatus = "UNEMPLOYED" Then
            result.PrevJobTitle = result.PrevEmploymentStatus
        End If
        Return result
    End Function
    Private Sub CreateMinorLoanApp(submissionData As XaSubmission)
		Dim mAppPersonalInfo As New CXAPersonalInfo()
		'' account info
		mAppPersonalInfo.EmployeeOfLender = RequestParamAsString("m_EmployeeOfLender")
		mAppPersonalInfo.MemberNumber = RequestParamAsString("m_MemberNumber")
		mAppPersonalInfo.RelationshipToPrimary = RequestParamAsString("m_RelationshipToPrimary")
		mAppPersonalInfo.FirstName = RequestParamAsString("m_FirstName")
		mAppPersonalInfo.LastName = RequestParamAsString("m_LastName")
		mAppPersonalInfo.MiddleName = RequestParamAsString("m_MiddleName")
		mAppPersonalInfo.Gender = RequestParamAsString("m_Gender")
		mAppPersonalInfo.Suffix = RequestParamAsString("m_NameSuffix")
		mAppPersonalInfo.DateOfBirth = RequestParamAsString("m_DOB")
		If requestParams("m_EmploymentStatus") IsNot Nothing Then
			mAppPersonalInfo.EmploymentSectionEnabled = True
			mAppPersonalInfo.EmploymentStatus = RequestParamAsString("m_EmploymentStatus")
			mAppPersonalInfo.txtJobTitle = RequestParamAsString("m_txtJobTitle")
			mAppPersonalInfo.txtEmployedDuration_month = RequestParamAsString("m_txtEmployedDuration_month")
			mAppPersonalInfo.txtEmployedDuration_year = RequestParamAsString("m_txtEmployedDuration_year")
			mAppPersonalInfo.txtEmployer = RequestParamAsString("m_txtEmployer")

			' new employment logic
			mAppPersonalInfo.txtBusinessType = RequestParamAsString("m_txtBusinessType")

			''enlistment date for active military
			Dim mEmploymentStatus = CEnum.MapEmploymentStatusToValue(mAppPersonalInfo.EmploymentStatus)
			Dim mMnlistmentDate As String = RequestParamAsString("m_EnlistmentDate")
			If mEmploymentStatus = "MI" And mMnlistmentDate <> "" Then
				mAppPersonalInfo.txtEmploymentStartDate = mMnlistmentDate
			Else
				mAppPersonalInfo.txtEmploymentStartDate = RequestParamAsString("m_txtEmploymentStartDate")
			End If
			mAppPersonalInfo.txtETS = RequestParamAsString("m_txtETS")
			mAppPersonalInfo.txtProfessionDuration_month = RequestParamAsString("m_txtProfessionDuration_month")
			mAppPersonalInfo.txtProfessionDuration_year = RequestParamAsString("m_txtProfessionDuration_year")
			mAppPersonalInfo.txtSupervisorName = RequestParamAsString("m_txtSupervisorName")
			mAppPersonalInfo.ddlBranchOfService = RequestParamAsString("m_ddlBranchOfService")
			mAppPersonalInfo.ddlPayGrade = RequestParamAsString("m_ddlPayGrade")
			mAppPersonalInfo.txtGrossMonthlyIncome = RequestParamAsDouble("m_txtGrossMonthlyIncome")
			' end new employment logic

			'prev employment
			mAppPersonalInfo.hasPrevEmployment = RequestParamAsString("m_hasPreviousEmployment")
			If mAppPersonalInfo.hasPrevEmployment = "Y" Then
				mAppPersonalInfo.prev_EmploymentStatus = RequestParamAsString("m_prev_EmploymentStatus")
				mAppPersonalInfo.prev_GrossMonthlyIncome = RequestParamAsDouble("m_prev_grossMonthlyIncome")
				If mAppPersonalInfo.prev_EmploymentStatus = "STUDENT" OrElse mAppPersonalInfo.prev_EmploymentStatus = "HOMEMAKER" Then
					mAppPersonalInfo.prev_txtJobTitle = mAppPersonalInfo.prev_EmploymentStatus
				ElseIf mAppPersonalInfo.prev_EmploymentStatus = "RETIRED" OrElse mAppPersonalInfo.prev_EmploymentStatus = "UNEMPLOYED" Then
					mAppPersonalInfo.prev_txtJobTitle = mAppPersonalInfo.prev_EmploymentStatus
					mAppPersonalInfo.prev_txtEmployedDuration_month = RequestParamAsString("m_prev_txtEmployedDuration_month")
					mAppPersonalInfo.prev_txtEmployedDuration_year = RequestParamAsString("m_prev_txtEmployedDuration_year")
				Else
					mAppPersonalInfo.prev_txtJobTitle = RequestParamAsString("m_prev_txtJobTitle")
					mAppPersonalInfo.prev_txtEmployedDuration_month = RequestParamAsString("m_prev_txtEmployedDuration_month")
					mAppPersonalInfo.prev_txtEmployedDuration_year = RequestParamAsString("m_prev_txtEmployedDuration_year")
					mAppPersonalInfo.prev_txtEmployer = RequestParamAsString("m_prev_txtEmployer")
				End If


				mAppPersonalInfo.prev_txtBusinessType = RequestParamAsString("m_prev_txtBusinessType")
				''enlistment date for active military
				Dim mPrevEmploymentStatus = CEnum.MapEmploymentStatusToValue(mAppPersonalInfo.prev_EmploymentStatus)
				Dim mPrevEnlistmentDate As String = RequestParamAsString("m_prev_EnlistmentDate")
				If (mPrevEmploymentStatus = "MI" And mPrevEnlistmentDate <> "") Then
					mAppPersonalInfo.prev_txtEmploymentStartDate = mPrevEnlistmentDate
				Else
					mAppPersonalInfo.prev_txtEmploymentStartDate = RequestParamAsString("m_prev_txtEmploymentStartDate")
				End If
				mAppPersonalInfo.prev_txtETS = RequestParamAsString("m_prev_txtETS")
				mAppPersonalInfo.prev_ddlBranchOfService = RequestParamAsString("m_prev_ddlBranchOfService")
				mAppPersonalInfo.prev_ddlPayGrade = RequestParamAsString("m_prev_ddlPayGrade")
			End If
			'end prev employment
		End If

		mAppPersonalInfo.Citizenship = RequestParamAsString("m_Citizenship")
		mAppPersonalInfo.MotherMaidenName = RequestParamAsString("m_MotherMaidenName")
		mAppPersonalInfo.SSN = RequestParamAsString("m_SSN")
		If requestParams("m_HousingPayment") IsNot Nothing Then
			mAppPersonalInfo.HousingPayment = RequestParamAsDouble("m_HousingPayment")
		End If
		submissionData.MinorAppPersonal = mAppPersonalInfo
		''end minor App personal info
		''minor contact info
		Dim mContact As New CContactInfo()
		mContact.CellPhone = RequestParamAsString("m_MobilePhone")
		mContact.CellPhoneCountry = RequestParamAsString("m_MobilePhoneCountry")
		mContact.Email = RequestParamAsString("m_EmailAddr")
		mContact.HomePhone = RequestParamAsString("m_HomePhone")
		mContact.HomePhoneCountry = RequestParamAsString("m_HomePhoneCountry")
		mContact.PreferredContactMethod = RequestParamAsString("m_ContactMethod")
		mContact.WorkPhone = RequestParamAsString("m_WorkPhone")
		mContact.WorkPhoneCountry = RequestParamAsString("m_WorkPhoneCountry")
		mContact.WorkPhoneExt = RequestParamAsString("m_WorkPhoneEXT")
		submissionData.MinorAppContact = mContact
		''minor Identification
		Dim mIdCard As New CIdentificationInfo()
		mIdCard.CardNumber = RequestParamAsString("m_IDCardNumber")
		mIdCard.CardType = RequestParamAsString("m_IDCardType")
		mIdCard.Country = RequestParamAsString("m_IDCountry")
		mIdCard.DateIssued = RequestParamAsString("m_IDDateIssued")
		mIdCard.ExpirationDate = RequestParamAsString("m_IDDateExpire")
		mIdCard.State = RequestParamAsString("m_IDState")
		submissionData.MinorAppIDCard = mIdCard

		''minor current address
		Dim mAddress As New CAddressInfo()
		mAddress.Address = RequestParamAsString("m_AddressStreet")
		mAddress.City = RequestParamAsString("m_AddressCity")
		mAddress.State = RequestParamAsString("m_AddressState")
		mAddress.Zip = RequestParamAsString("m_AddressZip")
		mAddress.Country = RequestParamAsString("m_Country")
		Dim sMinorOccupyingLocation = RequestParamAsString("m_OccupyingLocation")
		If Not String.IsNullOrEmpty(sMinorOccupyingLocation) Then
			mAddress.OccupancyDuration = RequestParamAsInteger("m_LiveMonths")
			mAddress.OccupancyStatus = sMinorOccupyingLocation
		End If
		submissionData.MinorAppAddress = mAddress
		''Minor mailing address
		Dim mMailingAddress As New CAddressInfo()
		mMailingAddress.Address = RequestParamAsString("m_MailingAddressStreet")
		mMailingAddress.City = RequestParamAsString("m_MailingAddressCity")
		mMailingAddress.State = RequestParamAsString("m_MailingAddressState")
		mMailingAddress.Zip = RequestParamAsString("m_MailingAddressZip")
		submissionData.MinorAppMailingAddress = mMailingAddress

		' Minor Applicant custom question answers 
		Dim lstCoApplicantCustomAnswers As List(Of CApplicantQA) =
			New JavaScriptSerializer().Deserialize(Of List(Of CApplicantQA))(requestParams("CustomAnswers")).
			Where(Function(cqa) Not String.IsNullOrEmpty(cqa.CQAnswer) AndAlso cqa.CQRole = QuestionRole.Applicant AndAlso IIf(cqa.CQLocation = CustomQuestionLocation.ApplicantPage, cqa.CQApplicantPrefix = "m_", True)).ToList()
		' Set validated CQ answers
		submissionData.MinorApplicantQuestionList = CCustomQuestionNewAPI.
			getValidatedCustomQuestionAnswers(websiteConfig, "XA", QuestionRole.Applicant, submissionData.Availability, lstCoApplicantCustomAnswers, New List(Of CApplicantQA)(0))
		'' --end minor applicant
	End Sub

	Private Function RequestParamAsString(ByVal name As String) As String
		Return Common.SafeString(requestParams(name))
	End Function
	Private Function RequestParamAsBoolean(ByVal name As String) As Boolean
		Return Common.SafeBoolean(requestParams(name))
	End Function
	Private Function RequestParamAsDouble(ByVal name As String) As Double
		Return Common.SafeDouble(requestParams(name))
	End Function
	Private Function RequestParamAsInteger(ByVal name As String) As Integer
		Return Common.SafeInteger(requestParams(name))
	End Function
	Private Function RequestParamAsGuid(ByVal name As String) As Guid
		Return Common.SafeGUID(requestParams(name))
	End Function
End Class
