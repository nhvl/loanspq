﻿Imports CustomList
Imports log4net
Imports LPQMobile.Utils
Imports Workflows.Base.BO
Imports Workflows.Base.Handlers
Imports Workflows.XA.BO
Imports System.Xml

Namespace Workflows.XA.Handlers

	Public Class XaStandardAccountFundingAndBookingHandler(Of TS As {New, XaSubmission}, TR As {XaState}, TC As {XaConfig})
		Inherits BaseHandler(Of TS, TR, TC)
		Private log As ILog = LogManager.GetLogger(GetType(XaStandardAccountFundingAndBookingHandler(Of TS, TR, TC)))
		Public Sub New(storage As Storage(Of TS, TR, TC), httpContext As HttpContext)
			MyBase.New(storage, httpContext)
		End Sub
		Protected Overrides Function Process() As Result
			''---Funding & booking-----
			'XA funding via cc & paypal which mean Funding is alway via GL - >fail doesn't book

			'XA funding via ACH(NACHO or Magic)
			'Separate configuration for a)CD(GL, direct, skip), b)other(GL,DIRECT)
			'For direct funding to MICR, then book 1st if fail then skip funding
			'Not direct, fund via GL, if Funding fail then skip booking because funding need a MIRC
			'hack, Issue:if booking fail, the booking service will change the funding status to not fund which sdfcu doesnt want, so we will do booking first then do the funding

			''get fundingType       
			Dim sFundingType As String = CEnum.FundingSourceText2FundingSourceValue(CurrentStorage.SubmissionData.FundingSourceInfo.fsFundingType)
			Dim xaMisc As New XaMisc(CurrentStorage.Config.WebsiteConfig)
			If CurrentStorage.Config.WebsiteConfig.IsStandAloneFundingEnabled AndAlso CurrentStorage.Config.WebsiteConfig.Booking.ToUpper() = "" Then 'stand alone funding with out booking
				''skip funding process if Funding type is empty or not funding(there is no funding source)
				If sFundingType = "" Then
					CurrentStorage.State.InternalCommentAppend += "Funding Source is not available. "
					CurrentStorage.State.IsUpdateReferralStatusRequired = True	'' can not fund --> need to update status to referred
					If CurrentStorage.Config.FundingAndBookingExecutingMode = "SILENCE" Then
						Return Result.MoveNext
					End If
					CurrentHttpResponse.Clear()
					CurrentHttpResponse.Write(CurrentStorage.State.CustomSubmittedSuccessMessage)
					Return Result.Terminate
				End If
				''direct funding only work with Credit Card funding type
				If CurrentStorage.Config.WebsiteConfig.IsACHFundingDirect And sFundingType = CEnum.FundingSourceValue.BANK.ToString() OrElse sFundingType = CEnum.FundingSourceValue.INTERNALTRANSFER.ToString() Then
					If CurrentStorage.Config.FundingAndBookingExecutingMode = "SILENCE" Then
						Return Result.MoveNext
					End If
					'CurrentStorage.State.IsUpdateReferralStatusRequired = True	'' this is a setup issue so shouldn't change loan_status
					'CurrentStorage.State.InternalCommentAppend += "Unable to do ACH funding direct to account: Need to do booking before funding."
					CurrentHttpResponse.Clear()
					''no changing status, so display custom message base on the loan status
					If CurrentStorage.Config.WebsiteConfig.LoanStatusEnum = "INSTANTAPPROVED" Or CurrentStorage.Config.WebsiteConfig.LoanStatusEnum = "APPROVED" Or CurrentStorage.Config.WebsiteConfig.LoanStatusEnum = "AP" Then
						CurrentHttpResponse.Write(CurrentStorage.State.CustomPreapprovedSuccessMessage)
					Else
						CurrentHttpResponse.Write(CurrentStorage.State.CustomSubmittedSuccessMessage)
					End If
					Return Result.Terminate
				End If

				Dim oFundDeposits As CFundDeposits = New CFundDeposits
				Dim sFundepositMessage = oFundDeposits.Execute(CurrentStorage.State.LoanID, CurrentStorage.Config.WebsiteConfig, xaMisc.FundDepositsURL)
				If sFundepositMessage <> "SUCCESS" Then
					CurrentStorage.State.InternalCommentAppend += "Funding failed: " & sFundepositMessage
					CurrentStorage.State.IsUpdateReferralStatusRequired = True	'' can not fund --> need to update status to referred
					If CurrentStorage.Config.FundingAndBookingExecutingMode = "SILENCE" Then
						Return Result.MoveNext
					End If
					CurrentHttpResponse.Clear()
					CurrentHttpResponse.Write(CurrentStorage.State.CustomSubmittedSuccessMessage)
					Return Result.Terminate
				Else
					CurrentStorage.State.IsUpdateReferralStatusRequired = False
					''decode Message before rendering
					CurrentStorage.State.CustomPreapprovedSuccessMessage = CurrentHttpContext.Server.HtmlDecode(CurrentStorage.State.CustomPreapprovedSuccessMessage)
					If CurrentStorage.Config.FundingAndBookingExecutingMode = "SILENCE" Then
						Return Result.MoveNext
					End If
					CurrentHttpResponse.Clear()
					CurrentHttpResponse.Write(CurrentStorage.State.CustomPreapprovedSuccessMessage)
					Return Result.Terminate
				End If

			ElseIf CurrentStorage.Config.WebsiteConfig.Booking.ToUpper() <> "" Then

				Dim oFundDeposits As CFundDeposits = New CFundDeposits
				Dim sFundepositMessage = "SUCCESS"

				'indirect GLA, funding 1st senario, then book
				Dim IsFundingSourceACH As Boolean = IIf(sFundingType = CEnum.FundingSourceValue.BANK.ToString(), True, False)
				Dim IsRequireMicroACHVerification = IsFundingSourceACH AndAlso CurrentStorage.State.TotalFunding > Common.SafeDouble(CurrentStorage.Config.WebsiteConfig.ACHFundingThresholdRequireMicroDeposit) AndAlso Common.SafeDouble(CurrentStorage.Config.WebsiteConfig.ACHFundingThresholdRequireMicroDeposit) > 0
				''execute funding only when funding is enabled 
				If CurrentStorage.Config.WebsiteConfig.IsAutoFundingWBookingEnabled Then
					log.DebugFormat("sFundingType: {0}, IsIsFundingSourceACH: {1}, IsRequireMicroACHVerification: {2}, IsACHFundingDirect: {3}", sFundingType, IsFundingSourceACH.ToString, IsRequireMicroACHVerification.ToString, CurrentStorage.Config.WebsiteConfig.IsACHFundingDirect.ToString)
					If sFundingType = "" Then
						sFundepositMessage = "Funding Source is not available. "   'This is out side of funding source ccondition checking below so ssytem can log
						CurrentStorage.State.InternalCommentAppend += sFundepositMessage
					Else
						If Not IsFundingSourceACH Or (IsFundingSourceACH And Not CurrentStorage.Config.WebsiteConfig.IsACHFundingDirect AndAlso Not CurrentStorage.Config.WebsiteConfig.ACHAutoFundingDisabled AndAlso Not IsRequireMicroACHVerification) Then
							sFundepositMessage = oFundDeposits.Execute(CurrentStorage.State.LoanID, CurrentStorage.Config.WebsiteConfig, xaMisc.FundDepositsURL)
							If sFundepositMessage <> "SUCCESS" Then
								CurrentStorage.State.InternalCommentAppend += "Funding failed: " & sFundepositMessage
							End If
						End If
					End If
				End If

				Dim sAccountNumber As String = ""
				If sFundepositMessage = "SUCCESS" And CurrentStorage.Config.WebsiteConfig.Booking <> "" Then
					If xaMisc.DisabledPrimaryBooking(CurrentStorage.SubmissionData.Availability, CurrentStorage.State.LoanRequestXMLStr, CurrentStorage.SubmissionData.IsSecondary) OrElse xaMisc.DisabledBusinessBooking(CurrentStorage.SubmissionData.Availability) OrElse xaMisc.DisabledSpecialBooking(CurrentStorage.SubmissionData.Availability) Then
						CurrentStorage.State.IsUpdateReferralStatusRequired = False
						If CurrentStorage.Config.FundingAndBookingExecutingMode = "SILENCE" Then
							Return Result.MoveNext
						End If
						''decode Message before rendering
						CurrentStorage.State.CustomPreapprovedSuccessMessage = CurrentHttpContext.Server.HtmlDecode(CurrentStorage.State.CustomPreapprovedSuccessMessage)
						CurrentHttpResponse.Clear()
						CurrentHttpResponse.Write(CurrentStorage.State.CustomPreapprovedSuccessMessage)
						Return Result.Terminate
					Else
						If CurrentStorage.Config.WebsiteConfig.LenderRef.ToUpper.StartsWith("ARMYAVFCU") Then
							Dim sBookingMessage = Common.ValidateApplicantsAndBeneficiariesXABooking(CurrentStorage.SubmissionData)
							If sBookingMessage <> "" Then
								log.Error(sBookingMessage)
								CurrentStorage.State.InternalCommentAppend += sBookingMessage & "Change status to referred. "
								CurrentStorage.State.IsUpdateReferralStatusRequired = True	'' can not book --> need to update status to referred
								If CurrentStorage.Config.FundingAndBookingExecutingMode = "SILENCE" Then
									Return Result.MoveNext
								End If
								CurrentHttpResponse.Clear()
								CurrentHttpResponse.Write(CurrentStorage.State.CustomSubmittedSuccessMessage)
								Return Result.Terminate
							End If
						End If
                        If CurrentStorage.Config.CustomListRuleList IsNot Nothing AndAlso CurrentStorage.Config.CustomListRuleList.Any(Function(c) c.Code = "BOOKING_VALIDATION") Then
                            Dim requiredParams As New Dictionary(Of String, String)
                            For Each p In CurrentStorage.Config.CustomListRuleList.Where(Function(c) c.Code = "BOOKING_VALIDATION")
                                Dim lst = p.GetList(Of CBookingValidation)()
                                If lst IsNot Nothing AndAlso lst.Count > 0 Then
                                    For Each item In lst
                                        If item.ParamInfo IsNot Nothing AndAlso item.ParamInfo.Count > 0 Then
                                            For Each param In item.ParamInfo
                                                If requiredParams.ContainsKey(param.Key) Then Continue For
                                                requiredParams.Add(param.Key, param.Value)
                                            Next
                                        End If
                                    Next
                                End If
                            Next
                            Dim params As New Dictionary(Of String, Tuple(Of Object, String))
                            If requiredParams.ContainsKey("locationPool") Then
                                params.Add("locationPool", New Tuple(Of Object, String)(CurrentStorage.SubmissionData.SelectedLocationPool, "List(Of String)"))
                            End If
                            If requiredParams.ContainsKey("isJoint") Then
                                params.Add("isJoint", New Tuple(Of Object, String)(CurrentStorage.SubmissionData.CoAppPersonal IsNot Nothing, "Boolean"))
                            End If
                            If requiredParams.ContainsKey("accountType") OrElse requiredParams.ContainsKey("accountPosition") OrElse requiredParams.ContainsKey("specialAccountType") OrElse requiredParams.ContainsKey("businessAccountType") Then
                                Dim accountType As String = ""
                                Dim accountPosition As String = "1"
                                Dim accountMatch = Regex.Match(CurrentStorage.SubmissionData.Availability, "^([12])([abs])?$")
                                If accountMatch.Success Then
                                    accountPosition = accountMatch.Groups(1).Value
                                    accountType = accountMatch.Groups(2).Value
                                End If
                                If accountType = "" Then accountType = " " 'personal by default
                                params.Add("accountType", New Tuple(Of Object, String)(accountType, "String"))
                                params.Add("accountPosition", New Tuple(Of Object, String)(accountPosition, "String"))
                                Dim specialAccountType As String = ""
                                Dim businessAccountType As String = ""
                                If accountType = "s" Then
                                    specialAccountType = CurrentStorage.SubmissionData.SAAccountInfo.AccountTypeCode
                                ElseIf accountType = "b" Then
                                    businessAccountType = CurrentStorage.SubmissionData.BABusinessInfo.BusinessType
                                End If
                                params.Add("specialAccountType", New Tuple(Of Object, String)(New List(Of String) From {specialAccountType}, "List(Of String)"))
                                params.Add("businessAccountType", New Tuple(Of Object, String)(New List(Of String) From {businessAccountType}, "List(Of String)"))
                            End If
                            If requiredParams.Any(Function(k) k.Key.StartsWith("CQ_")) Then
                                If CurrentStorage.SubmissionData.CustomQuestionAnswers IsNot Nothing AndAlso CurrentStorage.SubmissionData.CustomQuestionAnswers.Count > 0 Then
                                    For Each validatedQuestionAnswers In CurrentStorage.SubmissionData.CustomQuestionAnswers
                                        Dim questionId = Regex.Replace(validatedQuestionAnswers.Question.Name, "\s", "_")
                                        If requiredParams.ContainsKey("CQ_" & questionId) AndAlso Not params.ContainsKey("CQ_" & questionId) Then

                                            Dim type As String
                                            Select Case validatedQuestionAnswers.Question.DataType
                                                Case "integer"
                                                    type = "Integer"
                                                Case "double"
                                                    type = "Double"
                                                Case Else
                                                    type = "String"
                                            End Select
                                            If validatedQuestionAnswers.Answers.Count > 1 Then
                                                params.Add("CQ_" & questionId, New Tuple(Of Object, String)(validatedQuestionAnswers.Answers.Select(Function(q) q.AnswerValue).ToList(), "List(Of " & type & ")"))
                                            ElseIf validatedQuestionAnswers.Answers.Count = 1 Then
                                                params.Add("CQ_" & questionId, New Tuple(Of Object, String)(validatedQuestionAnswers.Answers.First().AnswerValue, type))
                                            End If
                                        End If
                                    Next
                                End If
                                'For Each pa In requiredParams.Where(Function(p) p.Key.StartsWith("CQ_") AndAlso Not params.ContainsKey(p.Key))
                                '	params.Add(pa.Key, New Tuple(Of Object, String)(Nothing, pa.Value))
                                'Next
                            End If

                            If requiredParams.ContainsKey("productCode") Then
                                Dim selectedProductCodes As New List(Of String)
                                If CurrentStorage.SubmissionData.AccountList IsNot Nothing AndAlso CurrentStorage.SubmissionData.AccountList.Count > 0 Then
                                    selectedProductCodes = CurrentStorage.SubmissionData.AccountList.Select(Function(p) p.Product).Distinct().ToList()
                                End If
                                params.Add("productCode", New Tuple(Of Object, String)(selectedProductCodes, "List(Of String)"))
                            End If
                            'AP-2367,2368: the new XA product selection mechanism allows the selection of multiple instances of a product. The consumer may select different products that share a product question or select multiple instances of the same product. These cases will result in multiple instances of the same product question. Each instance may have a different answer
                            '=> When a product custom question is used as a condition in a booking validation rule, the condition will evaluate to a TRUE value if there is at least one instance of the question where the consumer supplies the identified answer.

                            Dim multipleInstancesProductAnswers As New List(Of Dictionary(Of String, Tuple(Of Object, String)))
                            Dim traversedProducts As New HashSet(Of String)
                            If requiredParams.Any(Function(k) k.Key.StartsWith("PQ_")) Then
                                If CurrentStorage.SubmissionData.AccountList IsNot Nothing AndAlso CurrentStorage.SubmissionData.AccountList.Count > 0 Then
                                    For Each prod In CurrentStorage.SubmissionData.AccountList
                                        Dim multipleInstanceProduct As Boolean
                                        If traversedProducts.Contains(prod.Product) Then
                                            multipleInstanceProduct = True
                                        Else
                                            traversedProducts.Add(prod.Product)
                                            multipleInstanceProduct = False
                                        End If
                                        If prod.CustomQuestions IsNot Nothing AndAlso prod.CustomQuestions.Count > 0 Then
                                            Dim pqParams As New Dictionary(Of String, Tuple(Of Object, String))
                                            For Each pq In prod.CustomQuestions
                                                Dim questionId = Regex.Replace(pq.XAProductQuestionID, "[\s-]", "_")
                                                If requiredParams.ContainsKey("PQ_" & questionId) AndAlso Not pqParams.ContainsKey("PQ_" & questionId) Then
                                                    Dim paramItem As Tuple(Of Object, String) = Nothing
                                                    Select Case pq.AnswerType
                                                        Case "CHECKBOX"
                                                            If pq.RestrictedAnswers IsNot Nothing AndAlso pq.RestrictedAnswers.Count > 0 Then
                                                                Dim answers = pq.RestrictedAnswers.Where(Function(p) p.isSelected = True)
                                                                If answers IsNot Nothing AndAlso answers.Count > 0 Then
                                                                    paramItem = New Tuple(Of Object, String)(answers.Select(Function(p) p.Value).ToList(), "List(of String)")
                                                                    'pqParams.Add("PQ_" & questionId, New Tuple(Of Object, String)(answers.Select(Function(p) p.Value).ToList(), "List(of String)"))
                                                                End If
                                                            End If
                                                        Case "RADIO", "DROPDOWN"
                                                            If pq.RestrictedAnswers IsNot Nothing AndAlso pq.RestrictedAnswers.Count > 0 Then
                                                                Dim answer = pq.RestrictedAnswers.Find(Function(p) p.isSelected = True)
                                                                If answer IsNot Nothing Then
                                                                    paramItem = New Tuple(Of Object, String)(answer.Value, "String")
                                                                    'pqParams.Add("PQ_" & questionId, New Tuple(Of Object, String)(answer.Value, "String"))
                                                                End If
                                                            End If
                                                        Case Else
                                                            paramItem = New Tuple(Of Object, String)(pq.productAnswerText, "String")
                                                            'pqParams.Add("PQ_" & questionId, New Tuple(Of Object, String)(pq.productAnswerText, "String"))
                                                    End Select
                                                    If paramItem IsNot Nothing Then
                                                        If multipleInstanceProduct Then
                                                            pqParams.Add("PQ_" & questionId, paramItem)
                                                        Else
                                                            params.Add("PQ_" & questionId, paramItem)
                                                        End If
                                                    End If
                                                End If
                                            Next
                                            'because we have multiple instances of a product, we will collect the answers by instance of products and evaluate booking validation expression using those group of answers
                                            'for ex: if prodA have 2 questions and we have 2 instances of prodA. We will evaluate the expression twice
                                            'result = evaluate(prodA_instance1_answerOfQuestion1, prodA_instance1_answerOfQuestion2) OR evaluate(prodA_instance2_answerOfQuestion1, prodA_instance2_answerOfQuestion2)
                                            If multipleInstanceProduct Then
                                                multipleInstancesProductAnswers.Add(pqParams)
                                            End If
                                        End If
                                    Next
                                End If
                                'For Each pa In requiredParams.Where(Function(p) p.Key.StartsWith("PQ_") AndAlso Not params.ContainsKey(p.Key))
                                '	params.Add(pa.Key, New Tuple(Of Object, String)(Nothing, pa.Value))
                                'Next
                            End If
                            Dim evaluateResult = EvaluateBookingValidationBaseOnAppInfo(CurrentStorage.SubmissionData, requiredParams, params, xaMisc)
                            If (evaluateResult Is Nothing OrElse evaluateResult.Count = 0) AndAlso multipleInstancesProductAnswers.Count > 0 Then
                                Dim subResult As List(Of CustomList.CBookingValidation)
                                For Each item In multipleInstancesProductAnswers
                                    For Each answer In item
                                        If params.ContainsKey(answer.Key) Then
                                            params(answer.Key) = answer.Value
                                        Else
                                            params.Add(answer.Key, answer.Value)
                                        End If
                                    Next
                                    subResult = EvaluateBookingValidationBaseOnAppInfo(CurrentStorage.SubmissionData, requiredParams, params, xaMisc)
                                    If subResult IsNot Nothing AndAlso subResult.Count > 0 Then
                                        If evaluateResult Is Nothing Then evaluateResult = New List(Of CustomList.CBookingValidation)
                                        evaluateResult.AddRange(subResult)
                                        Exit For 'for performance, termminate the loop in case one of instances's answers cause the result = true
                                    End If
                                Next
                            End If
                            'End AP-2367,2368
                            If evaluateResult IsNot Nothing AndAlso evaluateResult.Count > 0 Then
                                CurrentStorage.State.InternalCommentAppend += "Booking Validation Rule (" + String.Join(". ", evaluateResult.Select(Function(p) p.Name)) + ") trigger. Booking not run and status change to REFERRED."
                                log.Info(CurrentStorage.State.InternalCommentAppend)
                                CurrentStorage.State.IsUpdateReferralStatusRequired = True  '' can not book --> need to update status to referred
                                If CurrentStorage.Config.FundingAndBookingExecutingMode = "SILENCE" Then
                                    Return Result.MoveNext
                                End If
                                CurrentHttpResponse.Clear()
                                CurrentHttpResponse.Write(CurrentStorage.State.CustomSubmittedSuccessMessage)
                                Return Result.Terminate
                            End If
                        End If
                        sAccountNumber = CBookingManager.Book(CurrentStorage.Config.WebsiteConfig, CurrentStorage.State.LoanID, "XA")

                        If sAccountNumber = "" Then
							CurrentStorage.State.InternalCommentAppend += " STATUS CHANGED TO PENDING BECAUSE BOOKING FAILED "
							CurrentStorage.State.IsUpdateReferralStatusRequired = True	'' can not book --> need to update status to referred
							If CurrentStorage.Config.FundingAndBookingExecutingMode = "SILENCE" Then
								Return Result.MoveNext
							End If
							CurrentHttpResponse.Clear()
							CurrentHttpResponse.Write(CurrentStorage.State.CustomSubmittedSuccessMessage)
							Return Result.Terminate
						End If
					End If
				End If

				'Direct to MIRC, funding 2nd senario, book then fund
				''execute funding only when funding is enabled 
				If CurrentStorage.Config.WebsiteConfig.IsAutoFundingWBookingEnabled Then
					If IsFundingSourceACH And CurrentStorage.Config.WebsiteConfig.IsACHFundingDirect AndAlso Not CurrentStorage.Config.WebsiteConfig.ACHAutoFundingDisabled And sAccountNumber <> "" AndAlso Not IsRequireMicroACHVerification Then
						If sFundingType = "" Then
							sFundepositMessage = "Funding Source is not available. "
							CurrentStorage.State.InternalCommentAppend += sFundepositMessage
						Else
							sFundepositMessage = oFundDeposits.Execute(CurrentStorage.State.LoanID, CurrentStorage.Config.WebsiteConfig, xaMisc.FundDepositsURL)
							If sFundepositMessage <> "SUCCESS" Then
								CurrentStorage.State.InternalCommentAppend += "Funding failed: " & sFundepositMessage
							End If
						End If
					End If
				End If

				If sAccountNumber <> "" And sFundepositMessage = "SUCCESS" Then	' update preapproved message with membernumber
					Dim requestXML As XmlDocument = New XmlDocument()
					requestXML.LoadXml(CurrentStorage.State.LoanRequestXMLStr)
					Dim loanXML As XmlDocument = New XmlDocument()
					loanXML.LoadXml(requestXML.InnerText)
					CurrentStorage.State.IsUpdateReferralStatusRequired = False
					If CurrentStorage.Config.WebsiteConfig.LoanStatusEnum.ToUpper <> "REFERRED" Then
						Common.GetCustomResponseMessages(CurrentStorage.Config.WebsiteConfig, "XA_LOAN", CurrentStorage.State.CustomErrorMessage, CurrentStorage.State.CustomPreapprovedSuccessMessage, CurrentStorage.State.CustomSubmittedSuccessMessage, CurrentStorage.State.CustomDeclinedMessage, CurrentStorage.State.CustomSubmittedDataList, CurrentStorage.State.CustomPreapprovedDataList, CurrentStorage.State.CustomDeclinedDataList, CurrentStorage.SubmissionData.Availability)
						CurrentStorage.State.CustomPreapprovedSuccessMessage = Common.buildNonApprovedMessage(CurrentStorage.State.CustomPreapprovedSuccessMessage, CurrentStorage.State.CustomPreapprovedDataList, loanXML, CurrentStorage.State.LoanResponseXMLStr, sAccountNumber)
						''add cross sell message to customPreapprovedSuccessMessage after booking success
						CurrentStorage.State.CustomPreapprovedSuccessMessage = CurrentStorage.State.CustomPreapprovedSuccessMessage + CurrentStorage.State.CrossSellMsg
						''using LoanTypeQueryString parameter to get customPreapproveMessage for secondary in getCustomResponseMessage function
						''override customPreapprovedSuccessMessage to APPROVED_SECONDARY_MESSAGE  if it is secondary
						''If _IsSecondary Then
						''	customPreapprovedSuccessMessage = CCustomMessage.getComboCustomResponseMessages(CurrentStorage.Config.WebsiteConfig, "APPROVED_SECONDARY_MESSAGE", loanResponseXMLStr, loanRequestXMLStr, sAccountNumber, LoanTypeQueryString)
						''End If

						CurrentStorage.State.CustomPreapprovedSuccessMessage = Common.PrependIconForNonApprovedMessage(CurrentStorage.State.CustomPreapprovedSuccessMessage, "Congratulations")
					End If
					'Home banking registration
					If CurrentStorage.Config.WebsiteConfig.OnlineUserRegistration <> "" And CurrentStorage.Config.LoanType = "XA" Then
						Dim sessionId As String = Guid.NewGuid().ToString().Substring(0, 8).Replace("-", "").ToLower()
						Dim postedData As New NameValueCollection(CurrentStorage.State.RequestParams)
						postedData.Add("MemberID", sAccountNumber)
						CurrentHttpContext.Session.Add(sessionId, postedData)
						Select Case CurrentStorage.Config.WebsiteConfig.OnlineUserRegistration
							Case "Q2"
								CurrentStorage.State.CustomPreapprovedSuccessMessage = Common.AppendCreateButtonForApprovedMessage(CurrentStorage.State.CustomPreapprovedSuccessMessage, "../HomeBanking/OnlineUserRegistrationQ2.aspx?sid=" + sessionId + "&lenderref=" + CurrentStorage.Config.WebsiteConfig.LenderRef)
							Case "DI"
								CurrentStorage.State.CustomPreapprovedSuccessMessage = Common.AppendCreateButtonForApprovedMessage(CurrentStorage.State.CustomPreapprovedSuccessMessage, "../HomeBanking/OnlineUserRegistrationDI.aspx?sid=" + sessionId + "&lenderref=" + CurrentStorage.Config.WebsiteConfig.LenderRef)
							Case "ALKAMI"
								CurrentStorage.State.CustomPreapprovedSuccessMessage = Common.AppendCreateButtonForApprovedMessage(CurrentStorage.State.CustomPreapprovedSuccessMessage, "../HomeBanking/OnlineUserRegistrationAlkami.aspx?sid=" + sessionId + "&lenderref=" + CurrentStorage.Config.WebsiteConfig.LenderRef)
							Case Else
								'TODO
						End Select
					End If

					'only run cross sell & docusign after a successful booked & funded
					''====Docusign
					Dim bIsDocuSign As Boolean = Common.getNodeAttributes(CurrentStorage.Config.WebsiteConfig, "XA_LOAN/VISIBLE", "in_session_docu_sign").Equals("Y")
					If bIsDocuSign Then
						Dim sLoanNumber = Common.getLoanNumber(CurrentStorage.State.LoanResponseXMLStr)
						Dim docuSignUrl As String = CDocuSign.GetDocuSign(CurrentStorage.Config.WebsiteConfig, sLoanNumber, "XA", CurrentHttpRequest, CurrentStorage.State.SettingProductList)
						If docuSignUrl <> "" Then
							Dim docuSignHtml As String = "<iframe id='ifDocuSign' title='XA Docusign' scrolling='auto' frameborder='0' width='100%' height='500px' sandbox='allow-same-origin allow-scripts allow-popups allow-forms' src='" + docuSignUrl + "'></iframe>"
							CurrentStorage.State.CustomPreapprovedSuccessMessage = docuSignHtml + CurrentStorage.State.CustomPreapprovedSuccessMessage
						End If
					End If


					''decode Message before rendering
					CurrentStorage.State.CustomPreapprovedSuccessMessage = CurrentHttpContext.Server.HtmlDecode(CurrentStorage.State.CustomPreapprovedSuccessMessage)
					If CurrentStorage.Config.FundingAndBookingExecutingMode = "SILENCE" Then
						Return Result.MoveNext
					End If
					CurrentHttpResponse.Clear()
					CurrentHttpResponse.Write(CurrentStorage.State.CustomPreapprovedSuccessMessage)
					Return Result.Terminate
				Else
					CurrentStorage.State.IsUpdateReferralStatusRequired = True	'' can not book --> need to update status to referred
					If CurrentStorage.Config.FundingAndBookingExecutingMode = "SILENCE" Then
						Return Result.MoveNext
					End If
					CurrentHttpResponse.Clear()
					CurrentHttpResponse.Write(CurrentStorage.State.CustomSubmittedSuccessMessage)
					Return Result.Terminate
				End If 'end message update after funding & booking
			Else '' no booking or funding setup in config
				CurrentStorage.State.CustomPreapprovedSuccessMessage = CurrentHttpContext.Server.HtmlDecode(CurrentStorage.State.CustomPreapprovedSuccessMessage)
				If CurrentStorage.Config.FundingAndBookingExecutingMode = "SILENCE" Then
					Return Result.MoveNext
				End If
				CurrentHttpResponse.Clear()
				CurrentHttpResponse.Write(CurrentStorage.State.CustomPreapprovedSuccessMessage)
				CurrentStorage.State.IsUpdateReferralStatusRequired = False
				Return Result.Terminate
			End If 'end standalone funding or funding&booking
		End Function
		Private Sub OnTerminated() Handles Me.Terminated
			log.Info("XaStandardAccountFundingAndBookingHandler: Terminated")
		End Sub
		Private Sub OnCompleted() Handles Me.Completed
			log.Info("XaStandardAccountFundingAndBookingHandler: Completed")
		End Sub
		Private Function EvaluateBookingValidationBaseOnAppInfo(appInfo As XaSubmission, requiredParams As Dictionary(Of String, String), params As Dictionary(Of String, Tuple(Of Object, String)), xaMisc As XaMisc) As List(Of CustomList.CBookingValidation)
			Dim result As New List(Of CustomList.CBookingValidation)

			If appInfo.BAApplicants IsNot Nothing AndAlso appInfo.BAApplicants.Any() Then
				For Each app In appInfo.BAApplicants
					Dim customListRuleHandler As New CCustomListRuleHandler(Of CBookingValidation)(CurrentStorage.Config.CustomListRuleList)
					Dim subResult = customListRuleHandler.Evaluate("BOOKING_VALIDATION", xaMisc.PrepareCustomListRuleParams(requiredParams, app, New Dictionary(Of String, Tuple(Of Object, String))(params)))
					If subResult IsNot Nothing AndAlso subResult.Count > 0 Then
						result.AddRange(subResult.Where(Function(p) Not String.IsNullOrWhiteSpace(p.Name) AndAlso Not result.Any(Function(q) q.Name = p.Name)).ToList())
					End If
				Next
			ElseIf appInfo.SAApplicants IsNot Nothing AndAlso appInfo.SAApplicants.Any() Then
				For Each app In appInfo.SAApplicants
					Dim customListRuleHandler As New CCustomListRuleHandler(Of CBookingValidation)(CurrentStorage.Config.CustomListRuleList)
					Dim subResult = customListRuleHandler.Evaluate("BOOKING_VALIDATION", xaMisc.PrepareCustomListRuleParams(requiredParams, app, New Dictionary(Of String, Tuple(Of Object, String))(params)))
					If subResult IsNot Nothing AndAlso subResult.Count > 0 Then
						result.AddRange(subResult.Where(Function(p) Not String.IsNullOrWhiteSpace(p.Name) AndAlso Not result.Any(Function(q) q.Name = p.Name)).ToList())
					End If
				Next
			Else
				If appInfo.Personal IsNot Nothing Then
					Dim customListRuleHandler As New CCustomListRuleHandler(Of CBookingValidation)(CurrentStorage.Config.CustomListRuleList)
					Dim subResult = customListRuleHandler.Evaluate("BOOKING_VALIDATION", xaMisc.PrepareCustomListRuleParams(requiredParams, appInfo.Personal, appInfo.Address, appInfo.ApplicantQuestionList, New Dictionary(Of String, Tuple(Of Object, String))(params)))
					If subResult IsNot Nothing AndAlso subResult.Count > 0 Then
						result.AddRange(subResult.Where(Function(p) Not String.IsNullOrWhiteSpace(p.Name) AndAlso Not result.Any(Function(q) q.Name = p.Name)).ToList())
					End If
				End If
				If appInfo.CoAppPersonal IsNot Nothing Then
					Dim customListRuleHandler As New CCustomListRuleHandler(Of CBookingValidation)(CurrentStorage.Config.CustomListRuleList)
					Dim subResult = customListRuleHandler.Evaluate("BOOKING_VALIDATION", xaMisc.PrepareCustomListRuleParams(requiredParams, appInfo.CoAppPersonal, appInfo.CoAppAddress, appInfo.CoAppApplicantQuestionList, New Dictionary(Of String, Tuple(Of Object, String))(params)))
					If subResult IsNot Nothing AndAlso subResult.Count > 0 Then
						result.AddRange(subResult.Where(Function(p) Not String.IsNullOrWhiteSpace(p.Name) AndAlso Not result.Any(Function(q) q.Name = p.Name)).ToList())
					End If
				End If
			End If
			Return result
		End Function

	End Class
End Namespace

