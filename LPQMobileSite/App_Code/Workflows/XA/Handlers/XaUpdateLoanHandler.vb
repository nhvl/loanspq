﻿Imports log4net
Imports Workflows.Base.BO
Imports Workflows.Base.Handlers
Imports Workflows.XA.BO

Namespace Workflows.XA.Handlers

	Public Class XaUpdateLoanHandler(Of TS As {New, XaSubmission}, TR As {XaState}, TC As {XaConfig})
		Inherits BaseHandler(Of TS, TR, TC)
		Private log As ILog = LogManager.GetLogger(GetType(XaUpdateLoanHandler(Of TS, TR, TC)))
		Public Sub New(storage As Storage(Of TS, TR, TC), httpContext As HttpContext)
			MyBase.New(storage, httpContext)
		End Sub
		Protected Overrides Function Process() As Result
			' We passed the authentication phase.
			' If the application was approved, then we need to update the status of the application on the server
			' We send the request again, but this time we list the approved products under the APPROVED category
			' First, we have to do a "getloans" to get the loan data, then we make the changes to the loan data and 
			' resubmit it back to the server with the "update" command
			If CurrentStorage.State.IsFailedIDA Then
				CurrentStorage.State.IsUpdateReferralStatusRequired = True
				CurrentHttpResponse.Clear()
				CurrentHttpResponse.Write(CurrentStorage.State.CustomSubmittedSuccessMessage)
				Return Result.Terminate
			Else
				If CurrentStorage.Config.WebsiteConfig.LoanStatusEnum.ToUpper() <> "" Then
					Dim isFraud As Boolean = False
					Dim xaMisc As New XaMisc(CurrentStorage.Config.WebsiteConfig)
					xaMisc.UpdateLoan(CurrentStorage.Config.WebsiteConfig.LoanStatusEnum.ToUpper(), CurrentStorage.State, CurrentStorage.SubmissionData, isFraud)
					If isFraud Then
						CurrentHttpResponse.Clear()
						CurrentHttpResponse.Write(CurrentStorage.State.CustomSubmittedSuccessMessage)
						Return Result.Terminate
					End If
					CurrentStorage.State.IsUpdateReferralStatusRequired = False
					''loan status enum is Referred --> set  customPreapprovedSuccessMessage to customSubmittedSuccessMessage
					If CurrentStorage.Config.WebsiteConfig.LoanStatusEnum.ToUpper() = "REFERRED" Then
						CurrentStorage.State.CustomPreapprovedSuccessMessage = CurrentStorage.State.CustomSubmittedSuccessMessage
					End If
				End If
			End If
			Return Result.MoveNext
		End Function
		Private Sub OnTerminated() Handles Me.Terminated
			log.Info("XaUpdateLoanHandler: Terminated")
		End Sub
		Private Sub OnCompleted() Handles Me.Completed
			log.Info("XaUpdateLoanHandler: Completed")
		End Sub
	End Class
End Namespace