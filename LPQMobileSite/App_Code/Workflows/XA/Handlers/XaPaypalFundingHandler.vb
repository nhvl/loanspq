﻿Imports log4net
Imports Workflows.Base.BO
Imports Workflows.Base.Handlers
Imports Workflows.XA.BO

Namespace Workflows.XA.Handlers

	Public Class XaPaypalFundingHandler(Of TS As {New, XaSubmission}, TR As {XaState}, TC As {XaConfig})
		Inherits BaseHandler(Of TS, TR, TC)
		Private log As ILog = LogManager.GetLogger(GetType(XaPaypalFundingHandler(Of TS, TR, TC)))
		Public Sub New(storage As Storage(Of TS, TR, TC), httpContext As HttpContext)
			MyBase.New(storage, httpContext)
		End Sub
		Protected Overrides Function Process() As Result
			'' --------------- PAYPAL funding ---------------------------------------------------------------------------------------''
			'' Application is Approved or InstantApproved and isPayPalFunding then execute paypal process
			'' Call getloan and updateloan to update funding status and internal comments.
			''execute booking for symitar if enabled (done in MLPayPal.aspx)
			'' check PAYPAL email is available

			If Not String.IsNullOrEmpty(CurrentStorage.Config.WebsiteConfig.PayPalEmail) AndAlso CurrentStorage.SubmissionData.Contact IsNot Nothing AndAlso Not String.IsNullOrEmpty(CurrentStorage.SubmissionData.Contact.Email) Then
				'' and also selected funding source is PAYPAL
				If CurrentStorage.SubmissionData.SelectedFundingSource.ToUpper().Equals("PAYPAL") Then
					'' Redirect to MLPayPal.aspx to process PayPal payment
					'CurrentHttpContext.Session("PAYPAL_RECEIPT") = CurrentStorage.State.PaypalReceipt
					'CurrentHttpContext.Session("APPROVED_MESSAGE") = CurrentStorage.State.CustomPreapprovedSuccessMessage
					'' build redirect url and params
					'' Note: Since MLPayPal.aspx page inherits from CBasePage so it requires lenderref value to get website config data

					'CurrentHttpContext.Session(CurrentStorage.StateSessionID) = CurrentStorage.State
					'CurrentHttpContext.Session(CurrentStorage.RequestParamsSessionID) = CurrentStorage.State.RequestParams
					CurrentStorage.State.ShouldKeepTheSessionDataPersistent = True
					CurrentHttpResponse.Redirect(String.Format("~/MLPayPal.aspx?lenderref={0}&sender={1}&wfsid={2}", CurrentStorage.Config.WebsiteConfig.LenderRef, CurrentStorage.SubmissionData.Contact.Email, CurrentStorage.SessionID), False)
					'CurrentHttpResponse.Redirect(redirURL, True) '' End the response here, all codes below this line will be ignored. go directly to MLPay on server side
					Return Result.Terminate
				End If
				'' --------------- END PAYPAL funding ---------------------------------------------------------------------------------------''
			End If
			Return Result.MoveNext
		End Function
		Private Sub OnTerminated() Handles Me.Terminated
			log.Info("XaPaypalFundingHandler: Terminated")
		End Sub
		Private Sub OnCompleted() Handles Me.Completed
			log.Info("XaPaypalFundingHandler: Completed")
		End Sub
	End Class
End Namespace

