﻿
Imports System.IO
Imports Newtonsoft.Json
Imports LPQMobile.BO
Imports log4net
Imports System.Net
Imports Workflows.Base.BO
Imports Workflows.Base.Handlers
Imports Workflows.XA.BO
Imports LPQMobile.Utils
Imports System.Xml

Namespace Workflows.XA.Handlers

	Public Class XaCreateNewLoanHandler(Of TS As {New, XaSubmission}, TR As {XaState}, TC As {XaConfig})
		Inherits BaseHandler(Of TS, TR, TC)
		Private log As ILog = LogManager.GetLogger(GetType(XaCreateNewLoanHandler(Of TS, TR, TC)))
		Public Sub New(storage As Storage(Of TS, TR, TC), httpContext As HttpContext)
			MyBase.New(storage, httpContext)
		End Sub
		Protected Overrides Function Process() As Result
			If Not CreateNewLoan(CurrentStorage.SubmissionData, CurrentHttpRequest.UserHostAddress, CurrentStorage.State.LoanRequestXMLStr, CurrentStorage.State.LoanResponseXMLStr, CurrentStorage.Config.WebsiteConfig) Then
				CurrentStorage.State.IsUpdateReferralStatusRequired = False	' need this to escape status update in Page_loadComplete sub
				CurrentHttpResponse.Clear()
				CurrentHttpResponse.Write(CurrentStorage.State.CustomErrorMessage)	' Was not succesful, so don't bother trying the next step
				Return Result.Terminate
			End If
			Return Result.MoveNext
		End Function
		Private Sub OnTerminated() Handles Me.Terminated
			log.Info("XaCreateNewLoanHandler: Terminated")
		End Sub
		Private Sub OnCompleted() Handles Me.Completed
			log.Info("XaCreateNewLoanHandler: Completed")
		End Sub


		Private Function CreateNewLoan(ByVal oXApp As XaSubmission, ByVal ipAddress As String, ByRef loanRequestXmlStr As String, ByRef loanResponseXmlStr As String, websiteConfig As CWebsiteConfig) As Boolean
			' In order to submit the application, we need to create an XML document with the proper structure
			' and fill it out with the data that the user has entered.
			Dim xaMisc As New XaMisc(websiteConfig)
			' TODO:  Move this URL into the XML config file and read it from there 
			Dim xml As String = LoadTemplateXml(oXApp, websiteConfig, CDate(CurrentHttpContext.Session("StartedDate_" & CurrentStorage.SessionID)))

			' Save the xml request string so we can refer to it in future callbacks
			loanRequestXmlStr = xml

			Dim requestXMLDoc As XmlDocument = New XmlDocument()
			requestXMLDoc.LoadXml(xml)

			Dim req As WebRequest = WebRequest.Create(xaMisc.LoanSubmitURL)

			req.Method = "POST"
			req.ContentType = "text/xml"

			'TODO: MaskSensitiveXMLData can't work into cdata
			log.Info("XA: Preparing to POST data(new loan): " & CSecureStringFormatter.MaskSensitiveXMLData(xml))

			Dim writer As New StreamWriter(req.GetRequestStream())
			writer.Write(xml)
			writer.Close()

			log.Info("Start the request to " + xaMisc.LoanSubmitURL)

			Dim res As WebResponse = req.GetResponse()

			Dim reader As StreamReader = New StreamReader(res.GetResponseStream())

			Dim sXml As String = reader.ReadToEnd()
			' Save the xml respnonse string so we can refer to it in other func
			loanResponseXmlStr = sXml

			log.Info("XA: Response from server(new loan): " + sXml)

			Dim bIsOkflag As Boolean = xaMisc.IsSubmitted(sXml)
			If Not bIsOkflag Then
				log.Warn("XA: Response from server(new loan) error: " + sXml)
			End If


			If bIsOkflag Then
				'log basic info for billing purpose
				'if the loan is successfully submitted, we need to keek track some basic information for billing purpose
				Dim xmlDocBogus As New XmlDocument	''this is required so it doesn't break the smBl.SaveLoan
				xmlDocBogus.LoadXml("<INPUT><REQUEST><LOAN_DATA><![CDATA[<XA_LOAN/>]]></LOAN_DATA></REQUEST></INPUT>")
				Dim loan = BuildLoanSavingObject(oXApp, CurrentStorage.Config.WebsiteConfig, True, sXml, xmlDocBogus)
				Dim smBl As New SmBL
				smBl.SaveLoan(loan, ipAddress)

			End If


			reader.Close()
			res.GetResponseStream().Close()
			req.GetRequestStream().Close()
			Return bIsOkflag

		End Function

		Private Function BuildLoanSavingObject(ByVal poLoanApp As XaSubmission, ByVal poConfig As CWebsiteConfig, isSubmitted As Boolean, sResponse As String, sRequestXML As XmlDocument) As SmLoan
			Dim loan As New SmLoan
			'loan.LoanSavingID = System.Guid.NewGuid
			loan.LenderID = Guid.Parse(poConfig.LenderId)
			loan.LenderRef = poConfig.LenderRef
			loan.VendorID = Guid.Empty.ToString
			loan.UserID = Guid.Empty
			If poLoanApp.Personal IsNot Nothing Then
				loan.ApplicantName = poLoanApp.Personal.FirstName & " " & poLoanApp.Personal.LastName
			ElseIf poLoanApp.SAApplicants IsNot Nothing AndAlso poLoanApp.SAApplicants.Count > 0 Then
				loan.ApplicantName = poLoanApp.SAApplicants(0).FirstName & " " & poLoanApp.SAApplicants(0).LastName
			End If
			loan.CreatedDate = Now
			Dim requestNode As XmlNode = sRequestXML.SelectSingleNode("INPUT/REQUEST")
			If requestNode IsNot Nothing Then
				loan.RequestParam = requestNode.InnerText
			End If
			If isSubmitted Then
				loan.SubmitDate = Now
				Dim doc As New XmlDocument
				doc.LoadXml(sResponse)
				Dim cNode As XmlNode = doc.SelectSingleNode("/OUTPUT/RESPONSE")
				If cNode IsNot Nothing Then
					loan.LoanNumber = cNode.SelectSingleNode("@loan_number").Value
					loan.LoanID = cNode.SelectSingleNode("@loan_id").Value
				End If
				loan.XmlResponse = sResponse
				Dim decisionNode As XmlNode = cNode.SelectSingleNode("DECISION")
				If decisionNode IsNot Nothing Then
					loan.Status = decisionNode.SelectSingleNode("@status").Value
					loan.StatusDate = Now
				End If
				loan.FundStatus = "NOT FUND"
			Else
				loan.SubmitDate = Nothing
				loan.Status = "INCOMPLETE"
				loan.FundStatus = "NOT FUND"
			End If
			loan.RequestParamJson = "dasdasdasdasd"
			loan.LoanType = SmSettings.LenderVendorType.XA

			Return loan
		End Function
		Private Function LoadTemplateXml(oLoanApp As XaSubmission, websiteConfig As CWebsiteConfig, createdDate As DateTime) As String
			Dim sb As New StringBuilder()

			Using writer As XmlWriter = XmlTextWriter.Create(sb)
				writer.WriteStartDocument()
				' begin input
				writer.WriteStartElement("INPUT")

				' begin login
				writer.WriteStartElement("LOGIN")
				'      writer.WriteAttributeString("login", "quanntest")
				'writer.WriteAttributeString("password", "lpq123")
				writer.WriteAttributeString("api_user_id", websiteConfig.APIUser)
				writer.WriteAttributeString("api_password", websiteConfig.APIPassword)
				writer.WriteEndElement()
				' end login

				' begin request
				writer.WriteStartElement("REQUEST")
				writer.WriteAttributeString("action_type", "NEW")

				writer.WriteStartElement("LOAN_DATA")
				writer.WriteAttributeString("loan_type", "XA")
				writer.WriteCData(WriteAppInfoToXmlString(oLoanApp, websiteConfig, createdDate))
				writer.WriteEndElement()
				' end request
				writer.WriteEndElement()
				' end input
				writer.WriteEndElement()
				writer.WriteEndDocument()
			End Using
			Return sb.ToString()
		End Function
		Private Function WriteAppInfoToXmlString(xaAppInfo As XaSubmission, websiteConfig As CWebsiteConfig, createdDate As DateTime) As String
			Dim sb As New StringBuilder
			Dim writer As XmlWriter = XmlTextWriter.Create(sb)
			Dim hasMinorApp As String = "N"
			Dim coAppSSNDic As New Dictionary(Of String, String)
			If xaAppInfo.Personal IsNot Nothing AndAlso Not String.IsNullOrEmpty(xaAppInfo.Personal.HasMinor) Then
				hasMinorApp = xaAppInfo.Personal.HasMinor
			End If
			writer.WriteStartDocument()
			writer.WriteStartElement("XPRESS_LOAN", "http://www.meridianlink.com/CLF")
			writer.WriteAttributeString("xmlns", "xsi", Nothing, "http://www.w3.org/2001/XMLSchema-instance")
			writer.WriteAttributeString("xsi", "schemaLocation", Nothing, "http://www.meridianlink.com/CLF C:\LoansPQ2\CLF\xpress_loan.xsd http://www.meridianlink.com/CLF C:\LoansPQ2\CLF\Integration\SSFCU.xsd")
			'writer.WriteAttributeString("version", "5.070")
			'writer.WriteAttributeString("version", "5.203")
			If xaAppInfo.Availability = "1b" Or xaAppInfo.Availability = "2b" Then
				writer.WriteAttributeString("version", "5.206")	''for XA business loan 
			Else
				writer.WriteAttributeString("version", "5.181")	''for standard and special
			End If
			'' begin applicants
			writer.WriteStartElement("APPLICANTS")
			''----minor applicant(type=1a/2a)---------
			If (hasMinorApp = "Y") Then
				WriteMinorAppInfoXMLString(writer, xaAppInfo.MinorAppPersonal, xaAppInfo.MinorAppContact, xaAppInfo.MinorAppAddress, xaAppInfo.MinorAppMailingAddress, xaAppInfo.MinorAppIDCard, xaAppInfo.MinorApplicantQuestionList, xaAppInfo.MinorRoleType)
			End If
			If xaAppInfo.BAApplicants IsNot Nothing AndAlso xaAppInfo.BAApplicants.Any() Then
				For Each app As CBAApplicantInfo In xaAppInfo.BAApplicants
					''add co-applicants to CoAppSSNDic
					If xaAppInfo.BAApplicants.IndexOf(app) > 0 Then
						If app.SSN IsNot Nothing AndAlso Not coAppSSNDic.ContainsKey(app.SSN) Then
							coAppSSNDic.Add(app.SSN, app.IsJoint)
						End If
					End If
					If app.IsJoint IsNot Nothing AndAlso app.IsJoint.ToUpper() = "Y" Then Continue For
					writer.WriteStartElement("APPLICANT")
					WriteBAApplicantInfoToXmlString(writer, app)
					If app IsNot xaAppInfo.BAApplicants.Last() AndAlso (xaAppInfo.BAApplicants(xaAppInfo.BAApplicants.IndexOf(app) + 1).IsJoint IsNot Nothing AndAlso xaAppInfo.BAApplicants(xaAppInfo.BAApplicants.IndexOf(app) + 1).IsJoint.ToUpper() = "Y") Then
						writer.WriteStartElement("SPOUSE")
						WriteBAApplicantInfoToXmlString(writer, xaAppInfo.BAApplicants(xaAppInfo.BAApplicants.IndexOf(app) + 1))
						writer.WriteEndElement()
					End If
					writer.WriteEndElement()
				Next
			ElseIf xaAppInfo.SAApplicants IsNot Nothing AndAlso xaAppInfo.SAApplicants.Any() Then
				For Each app As CSAApplicantInfo In xaAppInfo.SAApplicants
					''add co-applicants to CoAppSSNDic
					If xaAppInfo.SAApplicants.IndexOf(app) > 0 Then
						If app.SSN IsNot Nothing AndAlso Not coAppSSNDic.ContainsKey(app.SSN) Then
							coAppSSNDic.Add(app.SSN, app.IsJoint)
						End If
					End If
					If app.IsJoint IsNot Nothing AndAlso app.IsJoint.ToUpper() = "Y" Then Continue For
					writer.WriteStartElement("APPLICANT")
					WriteSAApplicantInfoToXmlString(writer, app)
					If app IsNot xaAppInfo.SAApplicants.Last() AndAlso (xaAppInfo.SAApplicants(xaAppInfo.SAApplicants.IndexOf(app) + 1).IsJoint IsNot Nothing AndAlso xaAppInfo.SAApplicants(xaAppInfo.SAApplicants.IndexOf(app) + 1).IsJoint.ToUpper() = "Y") Then
						writer.WriteStartElement("SPOUSE")
						WriteSAApplicantInfoToXmlString(writer, xaAppInfo.SAApplicants(xaAppInfo.SAApplicants.IndexOf(app) + 1))
						writer.WriteEndElement()
					End If
					writer.WriteEndElement()
				Next
			ElseIf xaAppInfo.SecondRoleType.ToUpper <> "NONE" Then ''if secondRoleType is "NONE" only submit minor applicant
				'' begin applicant
				writer.WriteStartElement("APPLICANT")
				WriteApplicantToXMLString(writer, False, xaAppInfo)
				''Applicant Question Answers
				WriteApplicantQuestionAnswer(xaAppInfo.ApplicantQuestionList, writer)
				If xaAppInfo.CoAppPersonal IsNot Nothing Then	' BEGIN JOint-APPLICANT
					writer.WriteStartElement("SPOUSE")
					WriteApplicantToXMLString(writer, True, xaAppInfo)
					''Co Applicant Question Answers
					WriteApplicantQuestionAnswer(xaAppInfo.CoAppApplicantQuestionList, writer)
					writer.WriteEndElement() '' end applicant FOR JOINT-APPLICANT
				End If	'End joint
				writer.WriteEndElement()  '' end applicant
			End If
			writer.WriteEndElement() '' end applicants
			'' begin new xml writer
			writer.WriteStartElement("COMMENTS")
			''consumer comments
			Dim sComment = Common.SafeString(HttpContext.Current.Request.Form("Comments"))
			If Not String.IsNullOrEmpty(sComment) Then
				''using EXTERNAL_COMMENT for consumer comment
				writer.WriteStartElement("EXTERNAL_COMMENTS")
				writer.WriteValue("SYSTEM (" + DateTime.Now + "): " + sComment)
				writer.WriteValue(Environment.NewLine)
				writer.WriteEndElement()
			End If ''end consumer comments

			''''write internal comment element: get disclosures and originating ip address
			writer.WriteStartElement("INTERNAL_COMMENTS")
			'' only for lender start with "BCU"
			Dim bFailPreUnderwrite As Boolean = False
			If websiteConfig.LenderRef.StartsWith("bcu") Then
				''check bad member for FOM SSN question answer
				Dim sFOMSSNAnswer = Common.SafeString(HttpContext.Current.Request.Form("ssnFOMAnswer"))
				If websiteConfig.LenderCodeBadMember <> "" And sFOMSSNAnswer <> "" Then
					Dim SSNList As New List(Of String)
					Dim memberName = sFOMSSNAnswer.Split("|"c)(0)
					Dim ssn = sFOMSSNAnswer.Split("|"c)(1)
					SSNList.Add(ssn)
					Dim isBadMember = Common.GetBadMember(SSNList, websiteConfig)
					If isBadMember Then
						writer.WriteValue("SYSTEM (" + DateTime.Now + "):" & memberName.ToUpper & " SSN MATCHES ENTRY ON HIGH RISK CONSUMER LIST")
						writer.WriteValue(Environment.NewLine)
						bFailPreUnderwrite = True
					End If
				End If
				If xaAppInfo.SecondRoleType.ToUpper <> "NONE" Then
					Dim dlComments As String = ""
					If xaAppInfo.IDCard IsNot Nothing AndAlso xaAppInfo.Personal IsNot Nothing Then dlComments = DriverLiencseComments(xaAppInfo.IDCard, xaAppInfo.Personal)
					If Not String.IsNullOrEmpty(dlComments) Then
						writer.WriteValue("SYSTEM (" + DateTime.Now + "): " + dlComments)
						writer.WriteValue(Environment.NewLine)
						bFailPreUnderwrite = True
					End If
					If xaAppInfo.CoAppPersonal IsNot Nothing Then
						Dim codlComments As String = ""
						If xaAppInfo.CoAppIDCard IsNot Nothing Then codlComments = DriverLiencseComments(xaAppInfo.CoAppIDCard, xaAppInfo.CoAppPersonal)
						If Not String.IsNullOrEmpty(codlComments) Then
							writer.WriteValue("SYSTEM (" + DateTime.Now + "): " + codlComments)
							writer.WriteValue(Environment.NewLine)
							bFailPreUnderwrite = True
						End If
					End If
				Else
					Dim midlComments As String = ""
					If xaAppInfo.MinorAppIDCard IsNot Nothing AndAlso xaAppInfo.MinorAppPersonal IsNot Nothing Then midlComments = DriverLiencseComments(xaAppInfo.MinorAppIDCard, xaAppInfo.MinorAppPersonal)
					If Not String.IsNullOrEmpty(midlComments) Then
						writer.WriteValue("SYSTEM (" + DateTime.Now + "): " + midlComments)
						writer.WriteValue(Environment.NewLine)
						bFailPreUnderwrite = True
					End If
				End If
			End If
			''===iqcu: SSN TIN Certification and Backup Withholding custom question
			If websiteConfig.LenderRef.ToLower.StartsWith("iqcu") Then
				Dim IRScomments As String = IRSWithholdingComments(xaAppInfo.CustomQuestionAnswers)
				If Not String.IsNullOrEmpty(IRScomments) Then
					writer.WriteValue("SYSTEM (" + DateTime.Now + "): " + IRScomments)
					writer.WriteValue(Environment.NewLine)
					bFailPreUnderwrite = True
				End If
			End If
			'===trustonefcu BSA question
			If websiteConfig.LenderRef.NullSafeToLower_.StartsWith("trustonefcu") Then
				Dim comments As String = BSACommnents(xaAppInfo.ApplicantQuestionList)
				If Not String.IsNullOrEmpty(comments) Then
					writer.WriteValue("SYSTEM (" + DateTime.Now + "): " + comments)
					writer.WriteValue(Environment.NewLine)
					bFailPreUnderwrite = True
				End If
			End If
			If xaAppInfo.Contact IsNot Nothing AndAlso Not String.IsNullOrEmpty(xaAppInfo.Contact.Email) Then
				writer.WriteValue(xaAppInfo.Contact.Email + " (" + DateTime.Now + "): Disclosure(s) checked: " + xaAppInfo.Disclosure)
			Else
				writer.WriteValue("(" + DateTime.Now + "): Disclosure(s) checked: " + xaAppInfo.Disclosure)
			End If
			If Not String.IsNullOrEmpty(xaAppInfo.ESignature) Then
				Dim index As Integer = xaAppInfo.ESignature.IndexOf("eSignature:")
				writer.WriteValue(Environment.NewLine)
				writer.WriteValue(xaAppInfo.ESignature.Substring(0, index))
				writer.WriteValue(Environment.NewLine)
				writer.WriteValue(Environment.NewLine)
				writer.WriteValue(xaAppInfo.ESignature.Substring(index))
			End If
			writer.WriteValue(Environment.NewLine)
			writer.WriteValue("SYSTEM (" + DateTime.Now + "): Originating IP Address: " + websiteConfig.RequestedIP)
			writer.WriteValue(Environment.NewLine)
			writer.WriteValue("SYSTEM (" + DateTime.Now + "): User Agent: " + websiteConfig.RequestedUserAgent)
			writer.WriteValue(Environment.NewLine)

			'add high risk consumer message to internal comment
			If websiteConfig.LenderCode <> "" Then
				Dim SSNList As New List(Of String)
				If xaAppInfo.SecondRoleType.ToUpper <> "NONE" Then
					If xaAppInfo.Personal IsNot Nothing AndAlso xaAppInfo.Personal.SSN <> "" Then
						SSNList.Add(xaAppInfo.Personal.SSN)
					End If
					''if has joint add coSSN to the list
					If xaAppInfo.CoAppPersonal IsNot Nothing AndAlso xaAppInfo.CoAppPersonal.SSN <> "" Then
						SSNList.Add(xaAppInfo.CoAppPersonal.SSN)
					End If
				End If
				''if has minor add minorSSN to the list
				If hasMinorApp = "Y" Then
					If xaAppInfo.MinorAppPersonal.SSN <> "" Then
						SSNList.Add(xaAppInfo.MinorAppPersonal.SSN)
					End If
				End If

				Dim isBadMember = Common.GetBadMember(SSNList, websiteConfig)
				If isBadMember Then
					writer.WriteValue("SYSTEM (" + DateTime.Now + "): Underwite process stopped because applicant's SSN is on the high risk consumer list. ")
					writer.WriteValue(Environment.NewLine)
				Else
					writer.WriteValue("SYSTEM (" + DateTime.Now + "): Applicant's SSN is NOT on the high risk consumer list.")
					writer.WriteValue(Environment.NewLine)
				End If
			End If

			'add instaTouchPrefillComment to Internal comment if it exist
			Dim sInstaTouchPrefillComment = Common.SafeString(HttpContext.Current.Request.Form("InstaTouchPrefillComment"))
			If sInstaTouchPrefillComment <> "" Then
				writer.WriteValue("SYSTEM (" + DateTime.Now + "): " + sInstaTouchPrefillComment)
				writer.WriteValue(Environment.NewLine)
			End If
			writer.WriteEndElement() ''end internal comment
			writer.WriteEndElement() ''en comment

			writer.WriteStartElement("LOAN_INFO")
			If websiteConfig.LoanStatusEnum <> "" Then 'set default status for XA that had underwritting
				If bFailPreUnderwrite Then
					writer.WriteAttributeString("status", "REFERRED")  'fail preUnderwrite
				Else
					writer.WriteAttributeString("status", "PENDING")
				End If
			Else
				writer.WriteAttributeString("status", "NEW")
			End If
			writer.WriteStartAttribute("create_date")
			writer.WriteValue(createdDate)
			writer.WriteEndAttribute()

			'If Not oConfig.LenderRef.ToLower.StartsWith("bcu") Then 'backward compatible with old clf
			writer.WriteStartAttribute("submit_date")
			writer.WriteValue(DateTime.Now)
			writer.WriteEndAttribute()
			'End If

			writer.WriteAttributeString("account_position", xaAppInfo.LoanInfo.account_position)
			If xaAppInfo.LoanInfo.account_position <> "2" Then
				writer.WriteAttributeString("is_new_customer", "Y")
			End If
			''minor Applicant(special account)'TODO deprecate hasMinorApp code in 2019
			If xaAppInfo.BAApplicants IsNot Nothing AndAlso xaAppInfo.BAApplicants.Count > 0 Then
				writer.WriteAttributeString("entity_type", "B")
			ElseIf hasMinorApp = "Y" OrElse (xaAppInfo.SAApplicants IsNot Nothing AndAlso xaAppInfo.SAApplicants.Any()) Then
				writer.WriteAttributeString("entity_type", "S")
			End If
			writer.WriteEndElement() ''end loan info
			''CUSTOM QUESTIONS 
			WriteCustomQuestionsToXMLString(writer, xaAppInfo.CustomQuestions, xaAppInfo.CustomQuestionAnswers)
			''SYSTEM element
			WriteSystemElementToXMLString(writer, websiteConfig, xaAppInfo.LoanInfo, xaAppInfo.IsSSO)
			Dim sFundingType As String = ""
			If xaAppInfo.FundingSourceInfo IsNot Nothing Then
				sFundingType = xaAppInfo.FundingSourceInfo.ValueFromType(xaAppInfo.FundingSourceInfo.fsFundingType)
			End If
			''if has coApp get coAppSSN
			Dim coAppSSN As String = String.Empty
			If xaAppInfo.CoAppPersonal IsNot Nothing Then
				coAppSSN = xaAppInfo.CoAppPersonal.SSN
			End If

			'add approved account 
			Dim productList As List(Of CProduct) = CProduct.GetProducts(websiteConfig, xaAppInfo.Availability)	'' TODO: NEED TO PASS IN ACCOUNT TYPE

			'XA feature attribute' deprecated, doesn't work out for kscfcu
			'         Dim oFeatureNodes As XmlNodeList = oConfig._webConfigXML.SelectNodes("XA_LOAN/FEATURE")
			'Dim isDisableAutoApprovedAccount As Boolean = False
			'         If oFeatureNodes.Count > 0 Then
			'             If oFeatureNodes(0).Attributes("disable_auto_approved_account").InnerText = "Y" Then
			'                 isDisableAutoApprovedAccount = True
			'             End If
			'End If

			Dim isDecisionXAEnable As Boolean = websiteConfig.IsDecisionXAEnable
			Dim sGaurdianSSN = ""
			If hasMinorApp = "Y" Then
				isDecisionXAEnable = xaAppInfo.IsMinorDecisionXaEnable
				If xaAppInfo.SecondRoleType <> "NONE" Then
					sGaurdianSSN = xaAppInfo.Personal.SSN
				End If
			End If
			'If Not isDisableAutoApprovedAccount And Not oConfig.IsDecisionXAEnable Then
			If Not isDecisionXAEnable Then
				'add approved account
				writer.WriteStartElement("APPROVED_ACCOUNTS")
				Dim approved_account_index As Integer
				If xaAppInfo.AccountList.Count > 0 Then
					Dim beneficiaries As List(Of CBeneficiaryInfo)
					If Not String.IsNullOrWhiteSpace(websiteConfig.XABeneficiaryLevel) AndAlso websiteConfig.XABeneficiaryLevel = "product" Then
						beneficiaries = xaAppInfo.BeneficiaryList
					End If
					For approved_account_index = 0 To xaAppInfo.AccountList.Count - 1
						writer.WriteRaw(xaAppInfo.AccountList(approved_account_index).ToXmlString(productList, True, sFundingType, coAppSSN, sGaurdianSSN, coAppSSNDic, beneficiaries))
					Next
				End If
				writer.WriteEndElement()
			End If

			''adding account here
			writer.WriteStartElement("INTERESTED_ACCOUNTS")
			Dim interested_account_index As Integer
			If xaAppInfo.AccountList.Count > 0 Then
				For interested_account_index = 0 To xaAppInfo.AccountList.Count - 1
					writer.WriteRaw(xaAppInfo.AccountList(interested_account_index).ToXmlString(productList, False, sFundingType, coAppSSN, sGaurdianSSN, coAppSSNDic))
				Next
			End If
			writer.WriteEndElement() ''INTERESTED_ACCOUNTS

			'move to before Approved account so we can check for funding status
			'Dim funding As CFundingSourceInfo = (New JavaScriptSerializer()).Deserialize(Of CFundingSourceInfo)(HttpContext.Current.Request.Params("rawFundingSource"))

			''FUNDING SOURCES
			WriteFundingSourcesToXMLString(writer, xaAppInfo.FundingSourceInfo)
			''Membership fee 
			WriteMembershipFeesToXMLString(writer, sFundingType, websiteConfig)
			If xaAppInfo.SAApplicants IsNot Nothing AndAlso xaAppInfo.SAApplicants.Any() Then
				Dim beneficiariesList = xaAppInfo.SAApplicants.First().Beneficiaries
				If beneficiariesList IsNot Nothing AndAlso beneficiariesList.Any() Then
					WriteBeneficiariesToXMLString(writer, beneficiariesList)
				End If
			ElseIf xaAppInfo.SecondRoleType.ToUpper <> "NONE" AndAlso xaAppInfo.BeneficiaryList IsNot Nothing AndAlso xaAppInfo.BeneficiaryList.Count > 0 Then
				''beneficiaries
				WriteBeneficiariesToXMLString(writer, xaAppInfo.BeneficiaryList)
			End If
			If xaAppInfo.BABusinessInfo IsNot Nothing Then
				WriteBusinessInfoToXmlString(writer, xaAppInfo.BABusinessInfo)
			ElseIf hasMinorApp = "Y" Then
				''minor applicant special info
				writer.WriteStartElement("SPECIAL_INFO")
				If Not String.IsNullOrEmpty(xaAppInfo.MinorAccountCode) Then
					writer.WriteAttributeString("special_account_type_code", xaAppInfo.MinorAccountCode)
				Else
					log.Info("Unable to get minor code")
				End If
				writer.WriteEndElement() ''end special info
			ElseIf xaAppInfo.SAAccountInfo IsNot Nothing AndAlso xaAppInfo.SAAccountInfo.AccountTypeCode <> "" Then	 'alway has to write out this node
				WriteSpecialInfoToXmlString(writer, xaAppInfo.SAAccountInfo)
			End If
			'FOM Questions section
			WriteFOMQuestionsToXMLString(writer, websiteConfig, xaAppInfo.FOMQuestionsAndAnswers)
			''BENEFICIAL_OWNERS only available for business xa or business loan(not for regular xa)
			If xaAppInfo.Availability = "1b" Or xaAppInfo.Availability = "2b" Then
				WriteBeneficialOwnersToXMLString(writer, xaAppInfo.BAApplicants, xaAppInfo.NoneApplicantBeneficialOwners)
			End If
			' end xpress loan
			writer.WriteEndElement()
			' end document
			writer.WriteEndDocument()
			writer.Close()

			Return sb.ToString()
		End Function

		Private Sub WriteBAApplicantInfoToXmlString(writer As XmlWriter, app As CBAApplicantInfo)

			If app.MemberNumber IsNot Nothing Then
				writer.WriteAttributeString("member_number", app.MemberNumber)
			End If
			If app.FirstName IsNot Nothing Then
				writer.WriteAttributeString("first_name", app.FirstName)
			End If
			If app.MiddleName IsNot Nothing Then
				writer.WriteAttributeString("middle_name", app.MiddleName)
			End If
			If app.LastName IsNot Nothing Then
				writer.WriteAttributeString("last_name", app.LastName)
			End If
			If app.NameSuffix IsNot Nothing Then
				writer.WriteAttributeString("suffix", app.NameSuffix)
			End If

			If app.RoleType IsNot Nothing Then
				writer.WriteAttributeString("role_type", app.RoleType)
			End If

			If app.HasControl IsNot Nothing AndAlso Not app.IsJoint.ToUpper().Equals("Y") Then
				writer.WriteAttributeString("is_equity_owner", IIf(app.HasControl = "Yes", "Y", "N").ToString())
			End If

			If Not app.IsJoint.ToUpper().Equals("Y") Then
				' AP-2304 - LPQ CLF specifies percent_business_owned as integer. Round to nearest integer and truncate the decimal before submitting.
				' There is no requirement in AP or LPQ that they all must add to 100% exactly, so 33.34 + 33.33 + 33.33 -> 33+33+33=99 is okay.
				writer.WriteAttributeString("percent_business_owned", Int(Math.Round(app.BusinessOwned)).ToString())
			End If

			writer.WriteAttributeString("dob", app.DOB.ToString("yyyy-MM-dd"))
			If app.Gender IsNot Nothing Then
				writer.WriteAttributeString("gender", app.Gender)
			End If
			If app.MotherMaidenName IsNot Nothing Then
				writer.WriteAttributeString("mother_maiden_name", app.MotherMaidenName)
			End If
			If app.CitizenshipStatus IsNot Nothing Then
				writer.WriteAttributeString("citizenship", CEnum.MapCitizenshipStatusToValue(app.CitizenshipStatus))
			End If
			If app.MaritalStatus IsNot Nothing Then
				writer.WriteAttributeString("marital_status", app.MaritalStatus)
			End If
			'If app.OfficialFamily IsNot Nothing Then
			'	writer.WriteAttributeString("official_family", app.OfficialFamily)
			'End If
			If app.RelationToPrimaryApplicant IsNot Nothing AndAlso app.RelationToPrimaryApplicant <> "" Then
				writer.WriteAttributeString("relation_to_primary", app.RelationToPrimaryApplicant)
			End If
			If app.ContactMethod IsNot Nothing AndAlso app.ContactMethod <> "" Then
				writer.WriteAttributeString("preferred_contact_method", app.ContactMethod)
			End If
			If app.ContactEmail IsNot Nothing AndAlso app.ContactEmail <> "" Then
				writer.WriteAttributeString("email", app.ContactEmail)
			End If
			If app.ContactHomePhone IsNot Nothing AndAlso app.ContactHomePhone <> "" Then
				writer.WriteAttributeString("home_phone", app.ContactHomePhone)
			End If
			If app.ContactHomePhoneCountry IsNot Nothing AndAlso app.ContactHomePhoneCountry <> "" AndAlso app.ContactHomePhoneCountry.ToUpper() <> "US" Then
				writer.WriteAttributeString("home_phone_country", app.ContactHomePhoneCountry)
			End If
			If app.ContactMobilePhone IsNot Nothing AndAlso app.ContactMobilePhone <> "" Then
				writer.WriteAttributeString("cell_phone", app.ContactMobilePhone)
			End If
			If app.ContactMobilePhoneCountry IsNot Nothing AndAlso app.ContactMobilePhoneCountry <> "" AndAlso app.ContactMobilePhoneCountry.ToUpper() <> "US" Then
				writer.WriteAttributeString("cell_phone_country", app.ContactMobilePhoneCountry)
			End If
			If app.ContactWorkPhone IsNot Nothing AndAlso app.ContactWorkPhone <> "" Then
				writer.WriteAttributeString("work_phone", app.ContactWorkPhone)
			End If
			If app.ContactWorkPhoneCountry IsNot Nothing AndAlso app.ContactWorkPhoneCountry <> "" AndAlso app.ContactWorkPhoneCountry.ToUpper() <> "US" Then
				writer.WriteAttributeString("work_phone_country", app.ContactWorkPhoneCountry)
			End If
			If app.ContactWorkPhoneExt IsNot Nothing AndAlso app.ContactWorkPhoneExt <> "" Then
				writer.WriteAttributeString("work_phone_extension", app.ContactWorkPhoneExt)
			End If
			If app.SSN IsNot Nothing Then
				writer.WriteAttributeString("ssn", app.SSN)
				If app.SSN.StartsWith("9") Then
					writer.WriteAttributeString("is_ssn_tax_id", "Y")
				End If
			End If
			'writer.WriteAttributeString("joint_account_option", app.IsJoint)
			writer.WriteStartElement("CURRENT_ADDRESS")
			If app.OccupyingLocation IsNot Nothing AndAlso Not String.IsNullOrEmpty(app.OccupyingLocation) Then
				writer.WriteAttributeString("occupancy_status", CEnum.MapOccupancyTypeToValue(app.OccupyingLocation))
				writer.WriteAttributeString("occupancy_duration", app.OccupancyDuration.ToString())
				If app.OccupancyDescription IsNot Nothing AndAlso Not String.IsNullOrEmpty(app.OccupancyDescription) Then
					writer.WriteAttributeString("other_occupancy_description", app.OccupancyDescription)
				End If
			End If
			If app.AddressCountry IsNot Nothing AndAlso app.AddressCountry.ToUpper() = "USA" Then
				writer.WriteStartElement("LOOSE_ADDRESS")
				If app.AddressStreet IsNot Nothing Then
					writer.WriteAttributeString("street_address_1", app.AddressStreet.Trim().Replace(",", "").Replace(".", ""))
				End If
				If app.AddressStreet2 IsNot Nothing Then
					writer.WriteAttributeString("street_address_2", app.AddressStreet2.Trim().Replace(",", "").Replace(".", ""))
				End If
				If app.AddressCity IsNot Nothing Then
					writer.WriteAttributeString("city", app.AddressCity.Trim().Replace(",", "").Replace(".", ""))
				End If
				If app.AddressState IsNot Nothing Then
					writer.WriteAttributeString("state", app.AddressState)
				End If
				If app.AddressZip IsNot Nothing Then
					writer.WriteAttributeString("zip", app.AddressZip)
				End If
				writer.WriteEndElement() ''end LOOSE_ADDRESS
			Else
				writer.WriteStartElement("THREE_LINE_ADDRESS")
				If app.AddressStreet IsNot Nothing Then
					writer.WriteAttributeString("street_address_1", app.AddressStreet.Trim().Replace(",", "").Replace(".", ""))
				End If
				If app.AddressStreet2 IsNot Nothing Then
					writer.WriteAttributeString("street_address_2", app.AddressStreet2.Trim().Replace(",", "").Replace(".", ""))
				End If
				If app.AddressCity IsNot Nothing Then
					writer.WriteAttributeString("city", app.AddressCity.Trim().Replace(",", "").Replace(".", ""))
				End If
				If app.AddressState IsNot Nothing Then
					writer.WriteAttributeString("state", app.AddressState)
				End If
				If app.AddressZip IsNot Nothing Then
					writer.WriteAttributeString("zip", app.AddressZip)
				End If
				If app.AddressCountry IsNot Nothing Then
					writer.WriteAttributeString("country", app.AddressCountry)
				End If
				writer.WriteEndElement() ''end LOOSE_ADDRESS
			End If

			writer.WriteEndElement() ''end CURRENT_ADDRESS
			writer.WriteStartElement("PREVIOUS_ADDRESS")
			If app.HasPreviousAddress = "Y" Then
				If app.PreviousAddressStreet IsNot Nothing Then
					writer.WriteAttributeString("street_address_1", app.PreviousAddressStreet.Trim().Replace(",", "").Replace(".", ""))
				End If
				If app.PreviousAddressCity IsNot Nothing Then
					writer.WriteAttributeString("city", app.PreviousAddressCity.Trim().Replace(",", "").Replace(".", ""))
				End If
				If app.PreviousAddressState IsNot Nothing Then
					writer.WriteAttributeString("state", app.PreviousAddressState)
				End If
				If app.PreviousAddressZip IsNot Nothing Then
					writer.WriteAttributeString("zip", app.PreviousAddressZip)
				End If
			End If
			writer.WriteEndElement() ''end PREVIOUS_ADDRESS
			writer.WriteStartElement("MAILING_ADDRESS")
			If app.HasMailingAddress = "Y" Then
				If app.MailingAddressStreet IsNot Nothing Then
					writer.WriteAttributeString("street_address_1", app.MailingAddressStreet.Trim().Replace(",", "").Replace(".", ""))
				End If
				If app.MailingAddressStreet2 IsNot Nothing Then
					writer.WriteAttributeString("street_address_1", app.MailingAddressStreet2.Trim().Replace(",", "").Replace(".", ""))
				End If
				If app.MailingAddressCity IsNot Nothing Then
					writer.WriteAttributeString("city", app.MailingAddressCity.Trim().Replace(",", "").Replace(".", ""))
				End If
				If app.MailingAddressState IsNot Nothing Then
					writer.WriteAttributeString("state", app.MailingAddressState)
				End If
				If app.MailingAddressZip IsNot Nothing Then
					writer.WriteAttributeString("zip", app.MailingAddressZip)
				End If
				If app.MailingAddressCountry IsNot Nothing Then
					writer.WriteAttributeString("country", app.MailingAddressCountry)
				End If
				writer.WriteAttributeString("is_current", "N")
			Else
				writer.WriteAttributeString("is_current", "Y")
			End If
			writer.WriteEndElement() ''end MAILING_ADDRESS

			writer.WriteStartElement("FINANCIAL_INFO")

			''housing payment
			Dim fHousingPayment = Common.SafeDoubleFromMoney(app.HousingPayment)
			If fHousingPayment > 0 Then
				writer.WriteAttributeString("monthly_rent", fHousingPayment.ToString())
			End If

			If app.EmploymentStatus IsNot Nothing AndAlso Not String.IsNullOrEmpty(app.EmploymentStatus) Then
				Dim employmentStatus = CEnum.MapEmploymentStatusToValue(app.EmploymentStatus)
				If Not String.IsNullOrEmpty(employmentStatus) Then
					writer.WriteAttributeString("employment_status", employmentStatus)
					If app.EmploymentDescription <> "" Then
						writer.WriteAttributeString("other_employment_description", app.EmploymentDescription)
					End If
					' new employment logic
					If app.EmploymentStatus.Contains("MILITARY") Then
						writer.WriteAttributeString("employer", CEnum.MapBranchOfServiceToValue(app.BranchOfService))
						writer.WriteAttributeString("is_monthly_income_base_salary_manual", "N")
					Else
						If app.EmploymentStatus = "HOMEMAKER" Or app.EmploymentStatus = "STUDENT" Or app.EmploymentStatus = "UNEMPLOYED" Or app.EmploymentStatus.Contains("RETIRED") Then
							app.Employer = "" ''no employer 
						End If
						writer.WriteAttributeString("employer", app.Employer)
					End If
					Dim employedMonths = app.EmployedDurationYear * 12 + app.EmployedDurationMonth
					Dim professionMonths As Integer = app.ProfessionDurationYear * 12 + app.ProfessionDurationMonth
					If app.EmploymentStatus <> "STUDENT" And app.EmploymentStatus <> "HOMEMAKER" Then
						writer.WriteAttributeString("employed_months", employedMonths.ToString())
						writer.WriteAttributeString("profession_months", IIf(professionMonths = 0, employedMonths, professionMonths).ToString())
					End If
					writer.WriteAttributeString("occupation", app.JobTitle)
					writer.WriteAttributeString("employment_business_type", app.BusinessType)
					writer.WriteAttributeString("supervisor_name", app.SupervisorName)
					writer.WriteAttributeString("pay_grade", app.PayGrade)
					If Common.SafeString(app.GrossMonthlyIncome) <> "" Then
						writer.WriteAttributeString("monthly_income_base_salary", app.GrossMonthlyIncome)
					End If
					If Not String.IsNullOrEmpty(Common.SafeDateXml(app.EmploymentStartDate)) Then
						writer.WriteAttributeString("employment_start_date", Common.SafeDateXml(app.EmploymentStartDate))
					End If
					If Not String.IsNullOrEmpty(app.ETS) Then
						writer.WriteAttributeString("ets", Common.SafeDateXml(app.ETS))
					End If

					If Not String.IsNullOrEmpty(app.EmployeeOfLender) And app.EmployeeOfLender <> "NONE" Then
						writer.WriteAttributeString("employee_of_lender_type", app.EmployeeOfLender)
					End If
					' end new employment logic
				Else
					writer.WriteAttributeString("employment_status", "OT")	'this is required for decisionXA
				End If
			End If

			writer.WriteEndElement() ' END </FINANCIAL_INFO>

			writer.WriteStartElement("ID_CARD")
			If app.IDCardType IsNot Nothing Then
				writer.WriteAttributeString("card_type", app.IDCardType)
			End If
			If app.IDCardNumber IsNot Nothing Then
				writer.WriteAttributeString("card_number", app.IDCardNumber)
			End If
			If app.IDState IsNot Nothing Then
				writer.WriteAttributeString("state", app.IDState)
			End If
			If app.IDCountry IsNot Nothing Then
				writer.WriteAttributeString("country", app.IDCountry)
			End If
			If app.IDDateIssued IsNot Nothing AndAlso Not String.IsNullOrEmpty(Common.SafeDateXml(app.IDDateIssued)) Then
				writer.WriteAttributeString("date_issued", Common.SafeDateXml(app.IDDateIssued))
			End If
			If app.IDDateExpire IsNot Nothing AndAlso Not String.IsNullOrEmpty(app.IDDateExpire) Then
				writer.WriteAttributeString("exp_date", Common.SafeDateXml(app.IDDateExpire))
			End If
			writer.WriteEndElement() 'end ID card 
			If app.ApplicantQuestionAnswers IsNot Nothing Then
				WriteApplicantQuestionAnswer(app.ApplicantQuestionAnswers, writer)
			End If
			writer.WriteStartElement("REFERENCE")
			If app.RelativeFirstName IsNot Nothing Then
				writer.WriteAttributeString("first_name", app.RelativeFirstName)
			End If
			If app.RelativeLastName IsNot Nothing Then
				writer.WriteAttributeString("last_name", app.RelativeLastName)
			End If
			If app.RelativePhone IsNot Nothing Then
				writer.WriteAttributeString("phone", app.RelativePhone)
			End If
			If app.RelativeEmail IsNot Nothing Then
				writer.WriteAttributeString("email", app.RelativeEmail)
			End If
			If app.RelativeRelationship IsNot Nothing Then
				writer.WriteAttributeString("relationship", app.RelativeRelationship)
			End If
			If app.RelativeAddress IsNot Nothing Then
				writer.WriteAttributeString("address", app.RelativeAddress)
			End If
			If app.RelativeCity IsNot Nothing Then
				writer.WriteAttributeString("city", app.RelativeCity)
			End If
			If app.RelativeState IsNot Nothing Then
				writer.WriteAttributeString("state", app.RelativeState)
			End If
			If app.RelativeZip IsNot Nothing Then
				writer.WriteAttributeString("zip", app.RelativeZip)
			End If
			writer.WriteEndElement() 'end REFERENCE
		End Sub
		Private Sub WriteApplicantQuestionAnswer(oAQAnswerList As List(Of CValidatedQuestionAnswers), ByVal writer As XmlWriter)
			If oAQAnswerList.Any() Then
				writer.WriteStartElement("APPLICANT_QUESTIONS")

				For Each oQuestionAnswers As CValidatedQuestionAnswers In oAQAnswerList
					If Not oQuestionAnswers.Answers.Any() Then Continue For

					writer.WriteStartElement("APPLICANT_QUESTION")
					writer.WriteAttributeString("question_name", oQuestionAnswers.Question.Name)
					writer.WriteAttributeString("question_type", oQuestionAnswers.Question.AnswerType)
					For Each oAnswer In oQuestionAnswers.Answers
						writer.WriteStartElement("APPLICANT_QUESTION_ANSWER")
						writer.WriteAttributeString("answer_text", oAnswer.AnswerText)
						writer.WriteAttributeString("answer_value", oAnswer.AnswerValue)
						writer.WriteEndElement() ' APPLICANT_QUESTION_ANSWER
					Next

					writer.WriteEndElement() ' APPLICANT_QUESTION
				Next

				writer.WriteEndElement()  ' APPLICANT_QUESTIONS
			End If
		End Sub
		Private Function DriverLiencseComments(ByVal oAppIDCard As CIdentificationInfo, ByVal oAppPersonal As CXAPersonalInfo) As String
			Dim strComment As String = ""
			Dim IDType As String = oAppIDCard.CardType
			Dim maxExpirationDate As New DateTime(2078, 12, 31)	''  "12/31/2078"
			If IDType.ToUpper = "DRIVERS_LICENSE" Then
				If Not String.IsNullOrEmpty(oAppIDCard.ExpirationDate) Then
					Dim ExpirationDate As DateTime = Date.Parse(oAppIDCard.ExpirationDate)
					If DateTime.Compare(ExpirationDate, maxExpirationDate) > 0 Then
						strComment += "Status=REFERRED because " & oAppPersonal.FirstName & " " & oAppPersonal.LastName & " Drivers License expiration date: " & ExpirationDate & " > " & maxExpirationDate
					End If
				End If
			End If
			Return strComment
		End Function
		Private Function IRSWithholdingComments(ByVal xaQuestionAnswers As List(Of CValidatedQuestionAnswers)) As String
			''IRSWithholdingComments only for lender iqcu. BCU lender is no longer using IRS_WITHHOLDING custom question to check underwriting
			' deserialize array string from json

			If xaQuestionAnswers Is Nothing Then Return ""

			Dim answerFound As Boolean = xaQuestionAnswers.Any(Function(qa) "CERTIFICATION AND BACKUP WITHHOLDING".Equals(qa.Question.Name, StringComparison.OrdinalIgnoreCase) AndAlso
																						   qa.Answers.Any(Function(a) "I AM SUBJECT TO BACKUP WITHHOLDING".Equals(a.AnswerValue, StringComparison.OrdinalIgnoreCase)))

			If answerFound Then
				Return "Status=REFERRED because the answer of SSN TIN Certification and Backup Withholding question = I am subject to backup withholding."
			End If

			Return ""
		End Function

		Private Function BSACommnents(ByVal applicantQuestionAnswers As List(Of CValidatedQuestionAnswers)) As String

			If applicantQuestionAnswers Is Nothing Then Return ""

			Dim bsaQuestionNames() As String = {"BSA QUESTION A", "BSA QUESTION B", "BSA QUESTION C", "BSA QUESTION D"}
			Dim answerFound = applicantQuestionAnswers.Any(Function(qa) bsaQuestionNames.Any(Function(name) qa.Question.Name.StartsWith(name)) AndAlso
															   qa.Answers.Any(Function(x) x.AnswerValue.Equals("YES", StringComparison.OrdinalIgnoreCase)))

			If answerFound Then Return "Status=REFERRED because BSA Question = Yes."

			Return ""
		End Function

		Private Sub WriteMinorAppInfoXMLString(ByVal writer As XmlWriter, ByVal oMinorAppPersonal As CXAPersonalInfo, ByVal oMinorAppContact As CContactInfo, ByVal oMinorAppAddress As CAddressInfo, ByVal oMinorMailingAddress As CAddressInfo, ByVal oMinorAppIDCard As CIdentificationInfo, ByVal oMinorAppAQAnswerList As List(Of CValidatedQuestionAnswers), ByVal minorRoleType As String)
			''start minor applicant
			writer.WriteStartElement("APPLICANT")
			writer.WriteAttributeString("first_name", oMinorAppPersonal.FirstName)
			writer.WriteAttributeString("middle_name", oMinorAppPersonal.MiddleName)
			writer.WriteAttributeString("last_name", oMinorAppPersonal.LastName)
			writer.WriteAttributeString("suffix", oMinorAppPersonal.Suffix)
			writer.WriteAttributeString("gender", oMinorAppPersonal.Gender)

			If Not String.IsNullOrEmpty(oMinorAppPersonal.DateOfBirth) Then
				writer.WriteAttributeString("dob", Common.SafeDateXml(oMinorAppPersonal.DateOfBirth))
			End If
			writer.WriteAttributeString("mother_maiden_name", oMinorAppPersonal.MotherMaidenName)
			writer.WriteAttributeString("citizenship", CEnum.MapCitizenshipStatusToValue(oMinorAppPersonal.Citizenship))
			writer.WriteAttributeString("ssn", oMinorAppPersonal.SSN)
			If oMinorAppPersonal.SSN.StartsWith("9") Then
				writer.WriteAttributeString("is_ssn_tax_id", "Y")
			End If
			writer.WriteAttributeString("member_number", oMinorAppPersonal.MemberNumber)
			If Not String.IsNullOrEmpty(oMinorAppPersonal.RelationshipToPrimary) Then
				writer.WriteAttributeString("relation_to_primary", oMinorAppPersonal.RelationshipToPrimary)
			End If
			If (Not String.IsNullOrEmpty(minorRoleType)) Then
				writer.WriteAttributeString("role_type", minorRoleType)
			Else
				log.Info("Unable to get role type for minor account")
			End If

			''minor contact infor
			writer.WriteAttributeString("email", oMinorAppContact.Email)
			If oMinorAppContact.HomePhoneCountry <> "" And Common.SafeString(oMinorAppContact.HomePhoneCountry).ToUpper <> "US" Then
				writer.WriteAttributeString("home_phone_country", oMinorAppContact.HomePhoneCountry.ToUpper)
				'writer.WriteAttributeString("is_home_phone_foreign", "Y")
				writer.WriteAttributeString("home_phone", oMinorAppContact.HomePhone)
			Else
				writer.WriteAttributeString("home_phone", FormatPhone(oMinorAppContact.HomePhone))
			End If


			If oMinorAppContact.CellPhoneCountry <> "" And Common.SafeString(oMinorAppContact.CellPhoneCountry).ToUpper <> "US" Then
				writer.WriteAttributeString("cell_phone_country", oMinorAppContact.CellPhoneCountry.ToUpper)
				'writer.WriteAttributeString("is_cell_phone_foreign", "Y")
				writer.WriteAttributeString("cell_phone", oMinorAppContact.CellPhone)
			Else
				writer.WriteAttributeString("cell_phone", FormatPhone(oMinorAppContact.CellPhone))
			End If


			If oMinorAppContact.WorkPhoneCountry.ToUpper <> "" And Common.SafeString(oMinorAppContact.WorkPhoneCountry).ToUpper <> "US" Then
				writer.WriteAttributeString("work_phone_country", oMinorAppContact.WorkPhoneCountry.ToUpper)
				'writer.WriteAttributeString("is_work_phone_foreign", "Y")
				writer.WriteAttributeString("work_phone", oMinorAppContact.WorkPhone)
			Else
				writer.WriteAttributeString("work_phone", FormatPhone(oMinorAppContact.WorkPhone))
			End If
			writer.WriteAttributeString("work_phone_extension", FormatPhoneExt(oMinorAppContact.WorkPhoneExt))

			If oMinorAppContact.PreferredContactMethod <> "" Then writer.WriteAttributeString("preferred_contact_method", oMinorAppContact.PreferredContactMethod)

			''minor current address
			writer.WriteStartElement("CURRENT_ADDRESS")
			''---mapping the occupancy status   
			If Not String.IsNullOrEmpty(oMinorAppAddress.OccupancyStatus) Then
				writer.WriteAttributeString("occupancy_status", CEnum.MapOccupancyTypeToValue(oMinorAppAddress.OccupancyStatus))
				writer.WriteAttributeString("occupancy_duration", oMinorAppAddress.OccupancyDuration)
			End If
			'' remove . and , characters
			oMinorAppAddress.Address = Common.SafeString(oMinorAppAddress.Address).Trim().Replace(",", "").Replace(".", "")
			oMinorAppAddress.City = Common.SafeString(oMinorAppAddress.City).Trim().Replace(",", "").Replace(".", "")

			''select either threeline or loose format
			If oMinorAppAddress.Country <> "" And oMinorAppAddress.Country <> "USA" Then
				' three line address, foreign
				writer.WriteStartElement("THREE_LINE_ADDRESS")
				writer.WriteAttributeString("street_address_1", oMinorAppAddress.Address)
				writer.WriteAttributeString("city", oMinorAppAddress.City)
				writer.WriteAttributeString("country", oMinorAppAddress.Country)
				writer.WriteAttributeString("zip", oMinorAppAddress.Zip)
				writer.WriteEndElement() ' end three line address
			Else
				'domestic
				writer.WriteStartElement("LOOSE_ADDRESS")
				writer.WriteAttributeString("street_address_1", oMinorAppAddress.Address)
				writer.WriteAttributeString("street_address_2", "")
				writer.WriteAttributeString("city", oMinorAppAddress.City)
				writer.WriteAttributeString("state", oMinorAppAddress.State)
				writer.WriteAttributeString("zip", oMinorAppAddress.Zip)
				writer.WriteEndElement() 'end LOOSE_ADDRESS
			End If
			writer.WriteEndElement() '' end current address
			''minor mailing address
			Dim mailingAddress As String = oMinorMailingAddress.Address
			If Not String.IsNullOrEmpty(mailingAddress) Then
				'' remove . and , characters
				oMinorMailingAddress.Address = Common.SafeString(oMinorMailingAddress.Address).Trim().Replace(",", "").Replace(".", "")
				oMinorMailingAddress.City = Common.SafeString(oMinorMailingAddress.City).Trim().Replace(",", "").Replace(".", "")

				writer.WriteStartElement("MAILING_ADDRESS")
				writer.WriteAttributeString("street_address_1", oMinorMailingAddress.Address)
				writer.WriteAttributeString("street_address_2", "")
				writer.WriteAttributeString("city", oMinorMailingAddress.City)
				writer.WriteAttributeString("state", oMinorMailingAddress.State)
				writer.WriteAttributeString("zip", oMinorMailingAddress.Zip)
				writer.WriteAttributeString("is_current", "N")
				writer.WriteEndElement()
			Else
				writer.WriteStartElement("MAILING_ADDRESS")
				writer.WriteAttributeString("is_current", "Y")
				writer.WriteEndElement()
			End If
			writer.WriteStartElement("FINANCIAL_INFO")
			If Not String.IsNullOrEmpty(oMinorAppPersonal.EmployeeOfLender) And oMinorAppPersonal.EmployeeOfLender <> "NONE" Then
				writer.WriteAttributeString("employee_of_lender_type", oMinorAppPersonal.EmployeeOfLender)
			End If
			If oMinorAppPersonal.EmploymentSectionEnabled = True Then
				Dim employment_status = CEnum.MapEmploymentStatusToValue(oMinorAppPersonal.EmploymentStatus)
				If Not String.IsNullOrEmpty(employment_status) Then
					writer.WriteAttributeString("employment_status", employment_status)
					' new employment logic
					If oMinorAppPersonal.EmploymentStatus.Contains("MILITARY") Then
						writer.WriteAttributeString("employer", CEnum.MapBranchOfServiceToValue(oMinorAppPersonal.ddlBranchOfService))
					Else
						If oMinorAppPersonal.EmploymentStatus = "HOMEMAKER" Or oMinorAppPersonal.EmploymentStatus = "STUDENT" Or oMinorAppPersonal.EmploymentStatus = "UNEMPLOYED" Or oMinorAppPersonal.EmploymentStatus.Contains("RETIRED") Then
							oMinorAppPersonal.txtEmployer = "" ''no employer 
						End If
						writer.WriteAttributeString("employer", oMinorAppPersonal.txtEmployer)
					End If
					Dim employed_months As Integer
					employed_months = Common.SafeInteger(oMinorAppPersonal.txtEmployedDuration_year) * 12 + Common.SafeInteger(oMinorAppPersonal.txtEmployedDuration_month)
					Dim profession_months As Integer
					profession_months = Common.SafeInteger(oMinorAppPersonal.txtProfessionDuration_year) * 12 + Common.SafeInteger(oMinorAppPersonal.txtProfessionDuration_month)
					If oMinorAppPersonal.EmploymentStatus <> "STUDENT" And oMinorAppPersonal.EmploymentStatus <> "HOMEMAKER" Then
						writer.WriteAttributeString("employed_months", employed_months.ToString())
						If profession_months = 0 Then
							writer.WriteAttributeString("profession_months", employed_months.ToString())
						Else
							writer.WriteAttributeString("profession_months", profession_months.ToString())
						End If
					End If
					writer.WriteAttributeString("occupation", oMinorAppPersonal.txtJobTitle)
					writer.WriteAttributeString("employment_business_type", oMinorAppPersonal.txtBusinessType)
					writer.WriteAttributeString("supervisor_name", oMinorAppPersonal.txtSupervisorName)
					writer.WriteAttributeString("pay_grade", oMinorAppPersonal.ddlPayGrade)
					If Common.SafeString(oMinorAppPersonal.txtGrossMonthlyIncome) <> "" Then
						writer.WriteAttributeString("monthly_income_base_salary", oMinorAppPersonal.txtGrossMonthlyIncome)
					End If
					If Not String.IsNullOrEmpty(Common.SafeDateXml(oMinorAppPersonal.txtEmploymentStartDate)) Then
						writer.WriteAttributeString("employment_start_date", Common.SafeDateXml(oMinorAppPersonal.txtEmploymentStartDate))
					End If
					If Not String.IsNullOrEmpty(oMinorAppPersonal.txtETS) Then
						writer.WriteAttributeString("ets", Common.SafeDateXml(oMinorAppPersonal.txtETS))
					End If
				Else
					writer.WriteAttributeString("employment_status", "OT")
					' end new employment logic
				End If
			End If	 ''end hasMinor
			''housing payment
			If oMinorAppPersonal.HousingPayment > 0 Then
				writer.WriteAttributeString("monthly_rent", oMinorAppPersonal.HousingPayment.ToString())
			End If
			writer.WriteEndElement() ' END </FINANCIAL_INFO>
			''minor identification
			writer.WriteStartElement("ID_CARD")
			writer.WriteAttributeString("card_type", oMinorAppIDCard.CardType)
			writer.WriteAttributeString("card_number", oMinorAppIDCard.CardNumber)
			writer.WriteAttributeString("state", oMinorAppIDCard.State)
			writer.WriteAttributeString("country", oMinorAppIDCard.Country)

			If Not String.IsNullOrEmpty(Common.SafeDateXml(oMinorAppIDCard.DateIssued)) Then
				writer.WriteAttributeString("date_issued", Common.SafeDateXml(oMinorAppIDCard.DateIssued))
			End If

			If Not String.IsNullOrEmpty(oMinorAppIDCard.ExpirationDate) Then
				writer.WriteAttributeString("exp_date", Common.SafeDateXml(oMinorAppIDCard.ExpirationDate))
			End If
			writer.WriteEndElement() 'end ID card 

			'' minor Applicant Question Answers
			WriteApplicantQuestionAnswer(oMinorAppAQAnswerList, writer)
			writer.WriteEndElement() 'end minor applicant 
		End Sub
		Private Function FormatPhone(ByVal src As String) As String
			Dim sb As New StringBuilder
			Dim count As Integer = 0
			For Each c As Char In src
				If Char.IsDigit(c) Then
					sb.Append(c)
					count += 1
				End If

				If count = 10 Then
					Exit For
				End If
			Next
			If count < 10 Then
				Return ""
			End If
			Return sb.ToString()
		End Function
		''phone work ext is not exactly 4 digits(could be 2, 3, ... depend on lenders) -> need to update the phone work ext format
		Private Function FormatPhoneExt(ByVal src As String) As String
			Dim sb As New StringBuilder
			Dim count As Integer = 0
			For Each c As Char In src
				If Char.IsDigit(c) Then
					sb.Append(c)
					count += 1
				End If

				If count = 5 Then '' in loanspq ext maxlength =5 
					Exit For
				End If
			Next

			If count < 1 Then ''no ext just return empty string
				Return ""
			End If

			Return sb.ToString()
		End Function
		Private Sub WriteSAApplicantInfoToXmlString(writer As XmlWriter, app As CSAApplicantInfo)
			If app.MemberNumber IsNot Nothing Then
				writer.WriteAttributeString("member_number", app.MemberNumber)
			End If
			If app.FirstName IsNot Nothing Then
				writer.WriteAttributeString("first_name", app.FirstName)
			End If
			If app.MiddleName IsNot Nothing Then
				writer.WriteAttributeString("middle_name", app.MiddleName)
			End If
			If app.LastName IsNot Nothing Then
				writer.WriteAttributeString("last_name", app.LastName)
			End If
			If app.NameSuffix IsNot Nothing Then
				writer.WriteAttributeString("suffix", app.NameSuffix)
			End If
			writer.WriteAttributeString("dob", app.DOB.ToString("yyyy-MM-dd"))
			If app.Gender IsNot Nothing Then
				writer.WriteAttributeString("gender", app.Gender)
			End If
			If app.MotherMaidenName IsNot Nothing Then
				writer.WriteAttributeString("mother_maiden_name", app.MotherMaidenName)
			End If
			If app.CitizenshipStatus IsNot Nothing Then
				writer.WriteAttributeString("citizenship", CEnum.MapCitizenshipStatusToValue(app.CitizenshipStatus))
			End If
			If app.MaritalStatus IsNot Nothing Then
				writer.WriteAttributeString("marital_status", app.MaritalStatus)
			End If
			'If app.OfficialFamily IsNot Nothing Then
			'	writer.WriteAttributeString("official_family", app.OfficialFamily)
			'End If
			If app.RelationToPrimaryApplicant IsNot Nothing AndAlso app.RelationToPrimaryApplicant <> "" Then
				writer.WriteAttributeString("relation_to_primary", app.RelationToPrimaryApplicant)
			End If
			If app.ContactMethod IsNot Nothing AndAlso app.ContactMethod <> "" Then
				writer.WriteAttributeString("preferred_contact_method", app.ContactMethod)
			End If
			If app.ContactEmail IsNot Nothing AndAlso app.ContactEmail <> "" Then
				writer.WriteAttributeString("email", app.ContactEmail)
			End If
			If app.ContactHomePhone IsNot Nothing AndAlso app.ContactHomePhone <> "" Then
				writer.WriteAttributeString("home_phone", app.ContactHomePhone)
			End If
			If app.ContactMobilePhone IsNot Nothing AndAlso app.ContactMobilePhone <> "" Then
				writer.WriteAttributeString("cell_phone", app.ContactMobilePhone)
			End If
			writer.WriteAttributeString("role_type", app.RoleType)
			If app.SSN IsNot Nothing Then
				writer.WriteAttributeString("ssn", app.SSN)
				If app.SSN.StartsWith("9") Then
					writer.WriteAttributeString("is_ssn_tax_id", "Y")
				End If
			End If
			'writer.WriteAttributeString("joint_account_option", app.IsJoint)
			writer.WriteStartElement("CURRENT_ADDRESS")
			If app.OccupyingLocation IsNot Nothing AndAlso Not String.IsNullOrEmpty(app.OccupyingLocation) Then
				writer.WriteAttributeString("occupancy_status", CEnum.MapOccupancyTypeToValue(app.OccupyingLocation))
				writer.WriteAttributeString("occupancy_duration", app.OccupancyDuration.ToString())
				If app.OccupancyDescription IsNot Nothing AndAlso Not String.IsNullOrEmpty(app.OccupancyDescription) Then
					writer.WriteAttributeString("other_occupancy_description", app.OccupancyDescription)
				End If
			End If
			If app.AddressCountry IsNot Nothing AndAlso app.AddressCountry.ToUpper() = "USA" Then
				writer.WriteStartElement("LOOSE_ADDRESS")
				If app.AddressStreet IsNot Nothing Then
					writer.WriteAttributeString("street_address_1", app.AddressStreet.Trim().Replace(",", "").Replace(".", ""))
				End If
				If app.AddressStreet2 IsNot Nothing Then
					writer.WriteAttributeString("street_address_2", app.AddressStreet2.Trim().Replace(",", "").Replace(".", ""))
				End If
				If app.AddressCity IsNot Nothing Then
					writer.WriteAttributeString("city", app.AddressCity.Trim().Replace(",", "").Replace(".", ""))
				End If
				If app.AddressState IsNot Nothing Then
					writer.WriteAttributeString("state", app.AddressState)
				End If
				If app.AddressZip IsNot Nothing Then
					writer.WriteAttributeString("zip", app.AddressZip)
				End If
				writer.WriteEndElement() ''end LOOSE_ADDRESS
			Else
				writer.WriteStartElement("THREE_LINE_ADDRESS")
				If app.AddressStreet IsNot Nothing Then
					writer.WriteAttributeString("street_address_1", app.AddressStreet.Trim().Replace(",", "").Replace(".", ""))
				End If
				If app.AddressStreet2 IsNot Nothing Then
					writer.WriteAttributeString("street_address_2", app.AddressStreet2.Trim().Replace(",", "").Replace(".", ""))
				End If
				If app.AddressCity IsNot Nothing Then
					writer.WriteAttributeString("city", app.AddressCity.Trim().Replace(",", "").Replace(".", ""))
				End If
				If app.AddressState IsNot Nothing Then
					writer.WriteAttributeString("state", app.AddressState)
				End If
				If app.AddressZip IsNot Nothing Then
					writer.WriteAttributeString("zip", app.AddressZip)
				End If
				If app.AddressCountry IsNot Nothing Then
					writer.WriteAttributeString("country", app.AddressCountry)
				End If
				writer.WriteEndElement() ''end LOOSE_ADDRESS
			End If

			writer.WriteEndElement() ''end CURRENT_ADDRESS
			writer.WriteStartElement("PREVIOUS_ADDRESS")
			If app.HasPreviousAddress IsNot Nothing AndAlso app.HasPreviousAddress = "Y" Then
				If app.PreviousAddressStreet IsNot Nothing Then
					writer.WriteAttributeString("street_address_1", app.PreviousAddressStreet.Trim().Replace(",", "").Replace(".", ""))
				End If
				If app.PreviousAddressCity IsNot Nothing Then
					writer.WriteAttributeString("city", app.PreviousAddressCity.Trim().Replace(",", "").Replace(".", ""))
				End If
				If app.PreviousAddressState IsNot Nothing Then
					writer.WriteAttributeString("state", app.PreviousAddressState)
				End If
				If app.PreviousAddressZip IsNot Nothing Then
					writer.WriteAttributeString("zip", app.PreviousAddressZip)
				End If
			End If
			writer.WriteEndElement() ''end PREVIOUS_ADDRESS
			writer.WriteStartElement("MAILING_ADDRESS")
			If app.HasMailingAddress IsNot Nothing AndAlso app.HasMailingAddress = "Y" Then
				If app.MailingAddressStreet IsNot Nothing Then
					writer.WriteAttributeString("street_address_1", app.MailingAddressStreet.Trim().Replace(",", "").Replace(".", ""))
				End If
				If app.MailingAddressStreet2 IsNot Nothing Then
					writer.WriteAttributeString("street_address_1", app.MailingAddressStreet2.Trim().Replace(",", "").Replace(".", ""))
				End If
				If app.MailingAddressCity IsNot Nothing Then
					writer.WriteAttributeString("city", app.MailingAddressCity.Trim().Replace(",", "").Replace(".", ""))
				End If
				If app.MailingAddressState IsNot Nothing Then
					writer.WriteAttributeString("state", app.MailingAddressState)
				End If
				If app.MailingAddressZip IsNot Nothing Then
					writer.WriteAttributeString("zip", app.MailingAddressZip)
				End If
				If app.MailingAddressCountry IsNot Nothing Then
					writer.WriteAttributeString("country", app.MailingAddressCountry)
				End If
				writer.WriteAttributeString("is_current", "N")
			Else
				writer.WriteAttributeString("is_current", "Y")
			End If
			writer.WriteEndElement() ''end MAILING_ADDRESS

			writer.WriteStartElement("FINANCIAL_INFO")

			''housing payment
			Dim fHousingPayment = Common.SafeDoubleFromMoney(app.HousingPayment)
			If fHousingPayment > 0 Then
				writer.WriteAttributeString("monthly_rent", fHousingPayment.ToString())
			End If

			If app.EmploymentStatus IsNot Nothing AndAlso Not String.IsNullOrEmpty(app.EmploymentStatus) Then
				Dim employmentStatus = CEnum.MapEmploymentStatusToValue(app.EmploymentStatus)
				If Not String.IsNullOrEmpty(employmentStatus) Then
					writer.WriteAttributeString("employment_status", employmentStatus)
					If app.EmploymentDescription <> "" Then
						writer.WriteAttributeString("other_employment_description", app.EmploymentDescription)
					End If
					' new employment logic
					If app.EmploymentStatus.Contains("MILITARY") Then
						writer.WriteAttributeString("employer", CEnum.MapBranchOfServiceToValue(app.BranchOfService))
						writer.WriteAttributeString("is_monthly_income_base_salary_manual", "N")
					Else
						If app.EmploymentStatus = "HOMEMAKER" Or app.EmploymentStatus = "STUDENT" Or app.EmploymentStatus = "UNEMPLOYED" Or app.EmploymentStatus.Contains("RETIRED") Then
							app.Employer = "" ''no employer 
						End If
						writer.WriteAttributeString("employer", app.Employer)
					End If
					Dim employedMonths = app.EmployedDurationYear * 12 + app.EmployedDurationMonth
					Dim professionMonths As Integer = app.ProfessionDurationYear * 12 + app.ProfessionDurationMonth
					If app.EmploymentStatus <> "STUDENT" AndAlso app.EmploymentStatus <> "HOMEMAKER" Then
						writer.WriteAttributeString("employed_months", employedMonths.ToString())
						writer.WriteAttributeString("profession_months", IIf(professionMonths = 0, employedMonths, professionMonths).ToString())
					End If
					writer.WriteAttributeString("occupation", app.JobTitle)
					writer.WriteAttributeString("employment_business_type", app.BusinessType)
					writer.WriteAttributeString("supervisor_name", app.SupervisorName)
					writer.WriteAttributeString("pay_grade", app.PayGrade)
					If Common.SafeString(app.GrossMonthlyIncome) <> "" Then
						writer.WriteAttributeString("monthly_income_base_salary", app.GrossMonthlyIncome)
					End If
					If Not String.IsNullOrEmpty(Common.SafeDateXml(app.EmploymentStartDate)) Then
						writer.WriteAttributeString("employment_start_date", Common.SafeDateXml(app.EmploymentStartDate))
					End If
					If Not String.IsNullOrEmpty(app.ETS) Then
						writer.WriteAttributeString("ets", Common.SafeDateXml(app.ETS))
					End If

					If Not String.IsNullOrEmpty(app.EmployeeOfLender) AndAlso app.EmployeeOfLender <> "NONE" Then
						writer.WriteAttributeString("employee_of_lender_type", app.EmployeeOfLender)
					End If
					' end new employment logic
				Else
					writer.WriteAttributeString("employment_status", "OT")	'this is required for decisionXA
				End If
			End If

			writer.WriteEndElement() ' END </FINANCIAL_INFO>

			writer.WriteStartElement("ID_CARD")
			If app.IDCardType IsNot Nothing Then
				writer.WriteAttributeString("card_type", app.IDCardType)
			End If
			If app.IDCardNumber IsNot Nothing Then
				writer.WriteAttributeString("card_number", app.IDCardNumber)
			End If
			If app.IDState IsNot Nothing Then
				writer.WriteAttributeString("state", app.IDState)
			End If
			If app.IDCountry IsNot Nothing Then
				writer.WriteAttributeString("country", app.IDCountry)
			End If
			If app.IDDateIssued IsNot Nothing AndAlso Not String.IsNullOrEmpty(Common.SafeDateXml(app.IDDateIssued)) Then
				writer.WriteAttributeString("date_issued", Common.SafeDateXml(app.IDDateIssued))
			End If
			If app.IDDateExpire IsNot Nothing AndAlso Not String.IsNullOrEmpty(app.IDDateExpire) Then
				writer.WriteAttributeString("exp_date", Common.SafeDateXml(app.IDDateExpire))
			End If
			writer.WriteEndElement() 'end ID card 
			If app.ApplicantQuestionAnswers IsNot Nothing Then
				WriteApplicantQuestionAnswer(app.ApplicantQuestionAnswers, writer)
			End If
			writer.WriteStartElement("REFERENCE")
			If app.RelativeFirstName IsNot Nothing Then
				writer.WriteAttributeString("first_name", app.RelativeFirstName)
			End If
			If app.RelativeLastName IsNot Nothing Then
				writer.WriteAttributeString("last_name", app.RelativeLastName)
			End If
			If app.RelativePhone IsNot Nothing Then
				writer.WriteAttributeString("phone", app.RelativePhone)
			End If
			If app.RelativeEmail IsNot Nothing Then
				writer.WriteAttributeString("email", app.RelativeEmail)
			End If
			If app.RelativeRelationship IsNot Nothing Then
				writer.WriteAttributeString("relationship", app.RelativeRelationship)
			End If
			If app.RelativeAddress IsNot Nothing Then
				writer.WriteAttributeString("address", app.RelativeAddress)
			End If
			If app.RelativeCity IsNot Nothing Then
				writer.WriteAttributeString("city", app.RelativeCity)
			End If
			If app.RelativeState IsNot Nothing Then
				writer.WriteAttributeString("state", app.RelativeState)
			End If
			If app.RelativeZip IsNot Nothing Then
				writer.WriteAttributeString("zip", app.RelativeZip)
			End If
			writer.WriteEndElement() 'end REFERENCE
		End Sub
		Private Sub WriteApplicantToXMLString(ByVal writer As XmlWriter, ByVal isJoint As Boolean, ByVal appInfo As XaSubmission)
			Dim currPersonal As CXAPersonalInfo
			Dim currContact As CContactInfo
			Dim currIDCard As CIdentificationInfo
			Dim currAddress As CAddressInfo
			Dim currPreviousAddress As CAddressInfo
			Dim currMailingAddress As CAddressInfo
			If isJoint Then
				currPersonal = appInfo.CoAppPersonal
				currContact = appInfo.CoAppContact
				currIDCard = appInfo.CoAppIDCard
				currAddress = appInfo.CoAppAddress
				currPreviousAddress = appInfo.CoAppPreviousAddress
				currMailingAddress = appInfo.CoAppMailingAddress
			Else
				currPersonal = appInfo.Personal
				currContact = appInfo.Contact
				currIDCard = appInfo.IDCard
				currAddress = appInfo.Address
				currPreviousAddress = appInfo.PreviousAddress
				currMailingAddress = appInfo.MailingAddress
			End If

			writer.WriteAttributeString("first_name", currPersonal.FirstName)
			writer.WriteAttributeString("middle_name", currPersonal.MiddleName)
			writer.WriteAttributeString("last_name", currPersonal.LastName)
			writer.WriteAttributeString("suffix", currPersonal.Suffix)
			writer.WriteAttributeString("gender", currPersonal.Gender)
			If (Not String.IsNullOrEmpty(appInfo.SecondRoleType)) Then
				writer.WriteAttributeString("role_type", appInfo.SecondRoleType)
			End If
			If Not String.IsNullOrEmpty(currPersonal.DateOfBirth) Then
				writer.WriteAttributeString("dob", Common.SafeDateXml(currPersonal.DateOfBirth))
			End If
			writer.WriteAttributeString("mother_maiden_name", currPersonal.MotherMaidenName)
			writer.WriteAttributeString("citizenship", CEnum.MapCitizenshipStatusToValue(currPersonal.Citizenship))
			If (Not String.IsNullOrEmpty(currPersonal.MaritalStatus)) Then
				writer.WriteAttributeString("marital_status", CEnum.MapMaritalStatusToValue(currPersonal.MaritalStatus))
			End If
			writer.WriteAttributeString("ssn", currPersonal.SSN)
			If currPersonal.SSN.StartsWith("9") Then
				writer.WriteAttributeString("is_ssn_tax_id", "Y")
			End If
			writer.WriteAttributeString("email", currContact.Email)
			writer.WriteAttributeString("member_number", currPersonal.MemberNumber)
			If isJoint = True AndAlso Not String.IsNullOrEmpty(currPersonal.RelationshipToPrimary) Then
				writer.WriteAttributeString("relation_to_primary", currPersonal.RelationshipToPrimary)
			End If

			If currContact.HomePhoneCountry <> "" And Common.SafeString(currContact.HomePhoneCountry).ToUpper <> "US" Then
				writer.WriteAttributeString("home_phone_country", currContact.HomePhoneCountry.ToUpper)
				'writer.WriteAttributeString("is_home_phone_foreign", "Y")
				writer.WriteAttributeString("home_phone", currContact.HomePhone)
			Else
				writer.WriteAttributeString("home_phone", FormatPhone(currContact.HomePhone))
			End If


			If currContact.CellPhoneCountry <> "" And Common.SafeString(currContact.CellPhoneCountry).ToUpper <> "US" Then
				writer.WriteAttributeString("cell_phone_country", currContact.CellPhoneCountry.ToUpper)
				'writer.WriteAttributeString("is_cell_phone_foreign", "Y")
				writer.WriteAttributeString("cell_phone", currContact.CellPhone)
			Else
				writer.WriteAttributeString("cell_phone", FormatPhone(currContact.CellPhone))
			End If
			If currContact.WorkPhoneCountry <> "" And Common.SafeString(currContact.WorkPhoneCountry).ToUpper <> "US" Then
				writer.WriteAttributeString("work_phone_country", currContact.WorkPhoneCountry.ToUpper)
				'writer.WriteAttributeString("is_work_phone_foreign", "Y")
				writer.WriteAttributeString("work_phone", currContact.WorkPhone)
			Else
				writer.WriteAttributeString("work_phone", FormatPhone(currContact.WorkPhone))
			End If
			writer.WriteAttributeString("work_phone_extension", FormatPhoneExt(currContact.WorkPhoneExt))

			If currContact.PreferredContactMethod <> "" Then writer.WriteAttributeString("preferred_contact_method", currContact.PreferredContactMethod)

			'' current address
			'' remove . and , characters
			currAddress.Address = Common.SafeString(currAddress.Address).Trim().Replace(",", "").Replace(".", "")
			currAddress.Address2 = Common.SafeString(currAddress.Address2).Trim().Replace(",", "").Replace(".", "")
			currAddress.City = Common.SafeString(currAddress.City).Trim().Replace(",", "").Replace(".", "")

			writer.WriteStartElement("CURRENT_ADDRESS")
			''---mapping the occupancy status   
			If Not String.IsNullOrEmpty(currAddress.OccupancyStatus) Then
				Dim OStatus As String = ""
				OStatus = CEnum.MapOccupancyTypeToValue(currAddress.OccupancyStatus)
				writer.WriteAttributeString("occupancy_status", OStatus)
				writer.WriteAttributeString("occupancy_duration", currAddress.OccupancyDuration)
				If currAddress.OccupancyDescription <> "" Then
					writer.WriteAttributeString("other_occupancy_description", currAddress.OccupancyDescription)
				End If
			End If
			''select either threeline or loose format
			If currAddress.Country <> "" And currAddress.Country <> "USA" Then
				' three line address, foreign
				writer.WriteStartElement("THREE_LINE_ADDRESS")
				writer.WriteAttributeString("street_address_1", currAddress.Address)
				writer.WriteAttributeString("street_address_2", currAddress.Address2)
				writer.WriteAttributeString("city", currAddress.City)
				writer.WriteAttributeString("country", currAddress.Country)
				writer.WriteAttributeString("zip", currAddress.Zip)
				writer.WriteEndElement() ' end three line address
			Else
				'domestic
				writer.WriteStartElement("LOOSE_ADDRESS")
				writer.WriteAttributeString("street_address_1", currAddress.Address)
				writer.WriteAttributeString("street_address_2", "")
				writer.WriteAttributeString("city", currAddress.City)
				writer.WriteAttributeString("state", currAddress.State)
				writer.WriteAttributeString("zip", currAddress.Zip)
				writer.WriteEndElement() 'end LOOSE_ADDRESS
			End If
			writer.WriteEndElement() '' end current address



			Dim previousAddress As String = currPreviousAddress.Address
			If Not String.IsNullOrEmpty(previousAddress) Then
				'' remove . and , characters
				currPreviousAddress.Address = Common.SafeString(currPreviousAddress.Address).Trim().Replace(",", "").Replace(".", "")
				currPreviousAddress.Address2 = Common.SafeString(currPreviousAddress.Address2).Trim().Replace(",", "").Replace(".", "")
				currPreviousAddress.City = Common.SafeString(currPreviousAddress.City).Trim().Replace(",", "").Replace(".", "")

				writer.WriteStartElement("PREVIOUS_ADDRESS")
				writer.WriteAttributeString("street_address_1", currPreviousAddress.Address)
				writer.WriteAttributeString("street_address_2", "")
				writer.WriteAttributeString("city", currPreviousAddress.City)
				writer.WriteAttributeString("state", currPreviousAddress.State)
				writer.WriteAttributeString("zip", currPreviousAddress.Zip)
				writer.WriteEndElement()
			Else
				writer.WriteStartElement("PREVIOUS_ADDRESS")
				writer.WriteEndElement()
			End If


			Dim mailingAddress As String = currMailingAddress.Address
			If Not String.IsNullOrEmpty(mailingAddress) Then
				'' remove . and , characters
				currMailingAddress.Address = Common.SafeString(currMailingAddress.Address).Trim().Replace(",", "").Replace(".", "")
				currMailingAddress.Address2 = Common.SafeString(currMailingAddress.Address2).Trim().Replace(",", "").Replace(".", "")
				currMailingAddress.City = Common.SafeString(currMailingAddress.City).Trim().Replace(",", "").Replace(".", "")

				writer.WriteStartElement("MAILING_ADDRESS")
				writer.WriteAttributeString("street_address_1", currMailingAddress.Address)
				writer.WriteAttributeString("street_address_2", currMailingAddress.Address2)
				writer.WriteAttributeString("city", currMailingAddress.City)
				writer.WriteAttributeString("state", currMailingAddress.State)
				writer.WriteAttributeString("zip", currMailingAddress.Zip)
				writer.WriteAttributeString("country", currMailingAddress.Country)
				writer.WriteAttributeString("is_current", "N")
				writer.WriteEndElement()
			Else
				writer.WriteStartElement("MAILING_ADDRESS")
				writer.WriteAttributeString("is_current", "Y")
				writer.WriteEndElement()
			End If
			writer.WriteStartElement("FINANCIAL_INFO")
			If Not String.IsNullOrEmpty(currPersonal.EmployeeOfLender) And currPersonal.EmployeeOfLender <> "NONE" Then
				writer.WriteAttributeString("employee_of_lender_type", currPersonal.EmployeeOfLender)
			End If
			If currPersonal.EmploymentSectionEnabled Then
				Dim employment_status = CEnum.MapEmploymentStatusToValue(currPersonal.EmploymentStatus)
				If Not String.IsNullOrEmpty(employment_status) Then
					writer.WriteAttributeString("employment_status", employment_status)
					If currPersonal.EmploymentDescription <> "" Then
						writer.WriteAttributeString("other_employment_description", currPersonal.EmploymentDescription)
					End If
					' new employment logic
					If currPersonal.EmploymentStatus.Contains("MILITARY") Then
						writer.WriteAttributeString("employer", CEnum.MapBranchOfServiceToValue(currPersonal.ddlBranchOfService))
						writer.WriteAttributeString("is_monthly_income_base_salary_manual", "N")
					Else
						If currPersonal.EmploymentStatus = "HOMEMAKER" Or currPersonal.EmploymentStatus = "STUDENT" Or currPersonal.EmploymentStatus = "UNEMPLOYED" Or currPersonal.EmploymentStatus.Contains("RETIRED") Then
							currPersonal.txtEmployer = "" ''no employer 
						End If
						writer.WriteAttributeString("employer", currPersonal.txtEmployer)
					End If
					Dim employed_months As Integer
					employed_months = Common.SafeInteger(currPersonal.txtEmployedDuration_year) * 12 + Common.SafeInteger(currPersonal.txtEmployedDuration_month)
					Dim profession_months As Integer
					profession_months = Common.SafeInteger(currPersonal.txtProfessionDuration_year) * 12 + Common.SafeInteger(currPersonal.txtProfessionDuration_month)
					If currPersonal.EmploymentStatus <> "STUDENT" And currPersonal.EmploymentStatus <> "HOMEMAKER" Then
						writer.WriteAttributeString("employed_months", employed_months.ToString())
						If profession_months = 0 Then
							writer.WriteAttributeString("profession_months", employed_months.ToString())
						Else
							writer.WriteAttributeString("profession_months", profession_months.ToString())
						End If
					End If
					writer.WriteAttributeString("occupation", currPersonal.txtJobTitle)
					writer.WriteAttributeString("employment_business_type", currPersonal.txtBusinessType)
					writer.WriteAttributeString("supervisor_name", currPersonal.txtSupervisorName)
					writer.WriteAttributeString("pay_grade", currPersonal.ddlPayGrade)
					If Common.SafeString(currPersonal.txtGrossMonthlyIncome) <> "" Then
						writer.WriteAttributeString("monthly_income_base_salary", currPersonal.txtGrossMonthlyIncome)
					End If
					If Not String.IsNullOrEmpty(Common.SafeDateXml(currPersonal.txtEmploymentStartDate)) Then
						writer.WriteAttributeString("employment_start_date", Common.SafeDateXml(currPersonal.txtEmploymentStartDate))
					End If
					If Not String.IsNullOrEmpty(currPersonal.txtETS) Then
						writer.WriteAttributeString("ets", Common.SafeDateXml(currPersonal.txtETS))
					End If
					' end new employment logic
				Else
					writer.WriteAttributeString("employment_status", "OT")	'this is required for decisionXA
				End If
			End If
			''housing payment
			If currPersonal.HousingPayment > 0 Then
				writer.WriteAttributeString("monthly_rent", currPersonal.HousingPayment.ToString())
			End If
			writer.WriteEndElement() ' END </FINANCIAL_INFO>
			writer.WriteStartElement("ID_CARD")
			writer.WriteAttributeString("card_type", currIDCard.CardType)
			writer.WriteAttributeString("card_number", currIDCard.CardNumber)
			writer.WriteAttributeString("state", currIDCard.State)
			writer.WriteAttributeString("country", currIDCard.Country)

			If Not String.IsNullOrEmpty(Common.SafeDateXml(currIDCard.DateIssued)) Then
				writer.WriteAttributeString("date_issued", Common.SafeDateXml(currIDCard.DateIssued))
			End If

			If Not String.IsNullOrEmpty(currIDCard.ExpirationDate) Then
				writer.WriteAttributeString("exp_date", Common.SafeDateXml(currIDCard.ExpirationDate))
			End If
			writer.WriteEndElement() 'end ID card 
		End Sub
		Private Sub WriteCustomQuestionsToXMLString(ByVal writer As XmlWriter, customQuestions As List(Of CCustomQuestionXA), customQuestionAnswers As List(Of CValidatedQuestionAnswers))
			writer.WriteStartElement("CUSTOM_QUESTIONS")

			For Each oQuestionAnswers As CValidatedQuestionAnswers In customQuestionAnswers
				If Not oQuestionAnswers.Answers.Any() Then Continue For

				writer.WriteStartElement("CUSTOM_QUESTION")
				writer.WriteAttributeString("question_name", oQuestionAnswers.Question.Name)
				writer.WriteAttributeString("question_type", oQuestionAnswers.Question.AnswerType)
				For Each oAnswer In oQuestionAnswers.Answers
					writer.WriteStartElement("CUSTOM_QUESTION_ANSWER")
					writer.WriteAttributeString("answer_text", oAnswer.AnswerText)
					writer.WriteAttributeString("answer_value", oAnswer.AnswerValue)
					writer.WriteEndElement() ' CUSTOM_QUESTION_ANSWER
				Next

				writer.WriteEndElement() ' CUSTOM_QUESTION
			Next

			writer.WriteEndElement() ' CUSTOM_QUESTIONS
		End Sub

		Private Sub WriteSystemElementToXMLString(ByVal writer As XmlWriter, websiteConfig As CWebsiteConfig, oLoanInfo As CLoanInfo, isSSO As Boolean)
			writer.WriteStartElement("SYSTEM")
			writer.WriteAttributeString("source", "GATEWAY")
			writer.WriteAttributeString("type", "LPQ")
			Dim urn = "http://www.meridianlink.com/InternalUse"
			writer.WriteAttributeString("xmlns", "d2p1", Nothing, urn)

			'TODO: enable this for all lender in mid 2016 when all lpq code is updated
			'If oConfig.LenderRef.StartsWith("bcu_test") Then
			writer.WriteAttributeString("origination_ip", websiteConfig.RequestedIP)
			'End If

			If oLoanInfo.referral_source <> "" Then
				writer.WriteAttributeString("external_source", Right(oLoanInfo.referral_source, 20))
			ElseIf isSSO Then
				writer.WriteAttributeString("external_source", "MOBILE WEBSITE SSO")
			Else
				writer.WriteAttributeString("external_source", "MOBILE WEBSITE")
			End If

			If Not String.IsNullOrEmpty(oLoanInfo.LoanOfficerID) Then
				writer.WriteStartElement("LOAN_OFFICER")
				writer.WriteAttributeString("internal_id", urn, oLoanInfo.LoanOfficerID)
				writer.WriteEndElement()
			End If

			Dim branchID As String = Common.SafeString(HttpContext.Current.Request.Form("BranchID"))
			If Not String.IsNullOrEmpty(branchID) Then
				writer.WriteStartElement("BRANCH")
				'writer.WriteAttributeString("id", branchID)	'' "id" attribute is not supported in CLF version>5.070
				writer.WriteAttributeString("internal_id", urn, branchID) '' "id" attribute is not supported in CLF version>5.070
				writer.WriteEndElement()
				''Dim branchRefID = ""
				''Dim branchRefIDDic = CLenderServiceConfigInfo.getBranchRefIDs("XA", oConfig)
				''If branchRefIDDic.ContainsKey(branchID) Then
				''    branchRefID = branchRefIDDic.Item(branchID)
				''    If Common.SafeString(branchRefID) <> "" Then
				''        writer.WriteStartElement("BRANCH")
				''        writer.WriteAttributeString("reference_id", branchRefID)
				''        writer.WriteEndElement()
				''    End If
				''End If

				''Else  --->already checked the branchID condition on the CBasePage class
				''    If oConfig.sDefaultXABranchName <> "" And oConfig.sDefaultXABranchID <> "" Then 'this has higher priority than CurrentBranchID bc CurrentBranchID can be derived from DEFAULT_LOAN_BRANCH or url parameter
				''        writer.WriteStartElement("BRANCH")
				''        writer.WriteAttributeString("id", oConfig.sDefaultXABranchID)
				''        writer.WriteEndElement()
				''    ElseIf oConfig.CurrentBranchID <> "" Then
				''        writer.WriteStartElement("BRANCH")
				''        writer.WriteAttributeString("id", oConfig.CurrentBranchID)
				''        writer.WriteEndElement()
				''    End If
			End If

			writer.WriteStartElement("LENDER")
			writer.WriteAttributeString("internal_id", urn, websiteConfig.LenderId)
			writer.WriteEndElement()
			writer.WriteStartElement("ORGANIZATION")
			writer.WriteAttributeString("internal_id", urn, websiteConfig.OrganizationId)
			writer.WriteEndElement() ''end organization
			writer.WriteEndElement() ''end system
		End Sub

		Private Sub WriteFundingSourcesToXMLString(ByVal writer As XmlWriter, ByVal funding As CFundingSourceInfo)
			writer.WriteStartElement("FUNDING_SOURCES")
			If funding IsNot Nothing Then
				writer.WriteStartElement("FUNDING_SOURCE")
				If funding.ValueFromType(funding.fsFundingType) = "CASH" Then
					writer.WriteAttributeString("clf_funding_source_id", "0")
				ElseIf funding.ValueFromType(funding.fsFundingType) = "MAIL" Then
					writer.WriteAttributeString("clf_funding_source_id", "1")
				ElseIf funding.ValueFromType(funding.fsFundingType) = "NOT_FUNDING" Then
					writer.WriteAttributeString("clf_funding_source_id", "2")
				ElseIf funding.ValueFromType(funding.fsFundingType) = "PAYPAL" Then
					writer.WriteAttributeString("clf_funding_source_id", "3")
				ElseIf funding.ValueFromType(funding.fsFundingType) = "INTERNALTRANSFER" Then
					writer.WriteAttributeString("clf_funding_source_id", "3")
				ElseIf funding.ValueFromType(funding.fsFundingType) = "BANK" Then
					writer.WriteAttributeString("clf_funding_source_id", "3")
				ElseIf funding.ValueFromType(funding.fsFundingType) = "CREDITCARD" Then
					writer.WriteAttributeString("clf_funding_source_id", "3")
				Else
					writer.WriteAttributeString("clf_funding_source_id", "10")
				End If

				writer.WriteAttributeString("funding_type", funding.ValueFromType(funding.fsFundingType))

				Select Case funding.fsFundingType
					Case "CREDIT CARD"
						writer.WriteAttributeString("name_on_card", funding.cNameOnCard)
						writer.WriteAttributeString("cc_card_type", funding.cCardType)
						writer.WriteAttributeString("cc_card_number", funding.cCreditCardNumber)
						writer.WriteAttributeString("cc_card_exp_date", String.Format("{0}-{1}-01", funding.cExpirationDate.Substring(3), funding.cExpirationDate.Substring(0, 2)))
						''cc_cvn_number does not exist in new clf version
						''  writer.WriteAttributeString("cc_cvn_number", funding.cCVNNumber)
						writer.WriteAttributeString("billing_street_address", funding.cBillingAddress)
						writer.WriteAttributeString("billing_city", funding.cBillingCity)
						writer.WriteAttributeString("billing_state", funding.cBillingState)
						writer.WriteAttributeString("billing_zip", funding.cBillingZip)
					Case "TRANSFER FROM ANOTHER FINANCIAL INSTITUTION"
						writer.WriteAttributeString("routing_number", funding.bRoutingNumber)
						writer.WriteAttributeString("bank_name_on_card", funding.bNameOnCard)
						writer.WriteAttributeString("bank_account_number", funding.bAccountNumber)
						writer.WriteAttributeString("bank_bankstate", funding.bBankState)
						writer.WriteAttributeString("bank_bankname", funding.bBankName)
						writer.WriteAttributeString("bank_account_type", funding.bAccountType)

					Case "INTERNAL TRANSFER"
						writer.WriteAttributeString("transfer_account_number", funding.tAccountNumber)
						writer.WriteAttributeString("transfer_account_type", funding.ValueFromTransferType(funding.tAccountType))

				End Select
				writer.WriteEndElement() 'FUNDING_SOURCE
			End If
			writer.WriteEndElement() 'FUNDING_SOURCES
		End Sub

		Private Sub WriteMembershipFeesToXMLString(ByVal writer As XmlWriter, ByVal sFundingType As String, oConfig As CWebsiteConfig)

			Dim customListFomFees As Dictionary(Of String, String)
			customListFomFees = Common.GetFomFeesFromCustomList(oConfig)
			If Common.SafeDouble(oConfig.MembershipFee) > 0 Or customListFomFees.Count > 0 Then
				Dim selectedFOMQuestionName As String = Common.SafeString(HttpContext.Current.Request.Params("FOMName"))
				Dim fundingFOMFee As Double = 0D
				Dim fundingBaseFee As Double = Common.SafeDouble(oConfig.MembershipFee)
				If Not String.IsNullOrEmpty(selectedFOMQuestionName) Then
					If customListFomFees.ContainsKey(selectedFOMQuestionName) Then
						fundingFOMFee = Common.SafeDouble(customListFomFees(selectedFOMQuestionName))
					End If
				End If
				Dim totalFundingAmount As Double = fundingBaseFee + fundingFOMFee
				''adding membership feed
				writer.WriteStartElement("MEMBERSHIP_FEE")
				writer.WriteAttributeString("membership_fee_funding_manual_fee", totalFundingAmount)
				writer.WriteAttributeString("membership_fee_funding_base_fee", fundingBaseFee)
				writer.WriteAttributeString("membership_fee_funding_fom_fee", fundingFOMFee)
				writer.WriteAttributeString("is_manual_membership_fee", "Y")
				If sFundingType <> "" Then
					If sFundingType = "CASH" Then
						writer.WriteAttributeString("membership_fee_funding_source_id", "0")
					ElseIf sFundingType = "MAIL" Then
						writer.WriteAttributeString("membership_fee_funding_source_id", "1")
					ElseIf sFundingType = "NOT_FUNDING" Then
						writer.WriteAttributeString("membership_fee_funding_source_id", "2")

						'source_id=3 will be used for all these types, only one will have source_id, other will have null for source id
					ElseIf sFundingType = "PAYPAL" Then
						writer.WriteAttributeString("membership_fee_funding_source_id", "3") '??
					ElseIf sFundingType = "BANK" Then
						writer.WriteAttributeString("membership_fee_funding_source_id", "3") ' ACH 
					ElseIf sFundingType = "INTERNALTRANSFER" Then
						writer.WriteAttributeString("membership_fee_funding_source_id", "3") ''this doesn't work , probably doesn't matter bc new member has no internal accoutn
					ElseIf sFundingType = "CREDITCARD" Then
						writer.WriteAttributeString("membership_fee_funding_source_id", "3")
					Else
						writer.WriteAttributeString("membership_fee_funding_source_id", "10")  '??
					End If
				End If
				'TODO: future enhancement, add this to combo mode too
				'writer.WriteAttributeString("membership_fee_funding_fom_fee", oConfig.MembershipFee)
				'writer.WriteAttributeString("membership_fee_funding_manual_fee", oConfig.MembershipFee)
				'writer.WriteAttributeString("is_manual_membership_fee", oConfig.MembershipFee)

				writer.WriteEndElement() ''MEMBERSHIP_FEE
			End If
		End Sub
		Private Sub WriteBeneficialOwnersToXMLString(ByVal writer As XmlWriter, baApplicants As List(Of CBAApplicantInfo), beneficialOwnerList As List(Of CBeneficialOwnerInfo))
			writer.WriteStartElement("BENEFICIAL_OWNERS")
			If baApplicants IsNot Nothing AndAlso baApplicants.Count > 0 Then
				For Each oItem As CBAApplicantInfo In baApplicants
					writer.WriteStartElement("BENEFICIAL_OWNER")
					writer.WriteAttributeString("first_name", Common.SafeString(oItem.FirstName))
					writer.WriteAttributeString("middle_name", Common.SafeString(oItem.MiddleName))
					writer.WriteAttributeString("last_name", Common.SafeString(oItem.LastName))
					writer.WriteAttributeString("dob", Common.SafeDateXml(oItem.DOB.ToString("yyyy-MM-dd")))
					writer.WriteAttributeString("is_tin_ssn", IIf(Common.SafeString(oItem.SSN).StartsWith("9"), "Y", "N").ToString())
					writer.WriteAttributeString("ssn", Common.SafeString(oItem.SSN))
					writer.WriteAttributeString("address", Common.SafeString(oItem.AddressStreet))
					writer.WriteAttributeString("zip", Common.SafeString(oItem.AddressZip))
					writer.WriteAttributeString("citizenship", CEnum.MapCitizenshipStatusToValue(Common.SafeString(oItem.CitizenshipStatus)))
					writer.WriteAttributeString("state", Common.SafeString(oItem.AddressState))
					writer.WriteAttributeString("city", Common.SafeString(oItem.AddressCity))
					writer.WriteAttributeString("is_beneficial_owner", IIf(oItem.BusinessOwned >= 25, "Y", "N").ToString())
					writer.WriteAttributeString("percent_business_owned", Common.SafeString(oItem.BusinessOwned))
					writer.WriteAttributeString("is_control", IIf(oItem.HasControl.ToUpper() = "YES", "Y", "N").ToString())
					writer.WriteAttributeString("control_title", CEnum.MapRoleNameToValue(oItem.ControlTitle).ToUpper())
					writer.WriteStartElement("ID_CARD")
					writer.WriteAttributeString("card_type", Common.SafeString(oItem.IDCardType))
					writer.WriteAttributeString("card_number", Common.SafeString(oItem.IDCardNumber))
					writer.WriteAttributeString("state", Common.SafeString(oItem.IDState))
					writer.WriteAttributeString("country", Common.SafeString(oItem.IDCountry))
					If Not String.IsNullOrEmpty(Common.SafeDateXml(oItem.IDDateExpire)) Then
						writer.WriteAttributeString("exp_date", Common.SafeString(oItem.IDDateExpire))
					End If
					If Not String.IsNullOrEmpty(Common.SafeDateXml(oItem.IDDateIssued)) Then
						writer.WriteAttributeString("date_issued", Common.SafeString(oItem.IDDateIssued))
					End If
					writer.WriteEndElement()
					writer.WriteEndElement()
				Next
			End If
			If beneficialOwnerList IsNot Nothing AndAlso beneficialOwnerList.Count > 0 Then
				For Each oItem As CBeneficialOwnerInfo In beneficialOwnerList
					writer.WriteStartElement("BENEFICIAL_OWNER")
					writer.WriteAttributeString("first_name", Common.SafeString(oItem.FirstName))
					writer.WriteAttributeString("middle_name", Common.SafeString(oItem.MiddleName))
					writer.WriteAttributeString("last_name", Common.SafeString(oItem.LastName))
					writer.WriteAttributeString("dob", Common.SafeDateXml(oItem.Dob.ToString("yyyy-MM-dd")))
					writer.WriteAttributeString("ssn", Common.SafeString(oItem.Ssn))
					writer.WriteAttributeString("is_tin_ssn", IIf(Common.SafeString(oItem.Ssn).StartsWith("9"), "Y", "N").ToString())
					writer.WriteAttributeString("address", Common.SafeString(oItem.AddressStreet))
					writer.WriteAttributeString("zip", Common.SafeString(oItem.AddressZip))
					writer.WriteAttributeString("citizenship", CEnum.MapCitizenshipStatusToValue(Common.SafeString(oItem.CitizenshipStatus)))
					writer.WriteAttributeString("state", Common.SafeString(oItem.AddressState))
					writer.WriteAttributeString("city", Common.SafeString(oItem.AddressCity))
					Dim isBeneficialOwner = "N"
					If oItem.HasControl.ToUpper() = "YES" Then
						isBeneficialOwner = "Y"
					Else
						If oItem.BusinessOwned >= 25 Then
							isBeneficialOwner = "Y"
						End If
					End If
					writer.WriteAttributeString("is_beneficial_owner", isBeneficialOwner)
					writer.WriteAttributeString("percent_business_owned", Common.SafeString(oItem.BusinessOwned))
					writer.WriteAttributeString("is_control", IIf(oItem.HasControl.ToUpper() = "YES", "Y", "N").ToString())
					writer.WriteAttributeString("control_title", CEnum.MapRoleNameToValue(oItem.ControlTitle).ToUpper())
					writer.WriteStartElement("ID_CARD")
					writer.WriteAttributeString("card_type", "")
					writer.WriteAttributeString("card_number", "")
					writer.WriteAttributeString("state", "")
					writer.WriteAttributeString("country", "")
					'TODO
					'writer.WriteAttributeString("exp_date", "")
					'writer.WriteAttributeString("date_issued", "")
					writer.WriteEndElement()
					writer.WriteEndElement()
				Next
			End If
			writer.WriteEndElement()
		End Sub
		Private Sub WriteBeneficiariesToXMLString(ByVal writer As XmlWriter, beneficiaryList As List(Of CBeneficiaryInfo))
			If (beneficiaryList.Count > 0) Then
				writer.WriteStartElement("BENEFICIARIES")
				For Each oItem As CBeneficiaryInfo In beneficiaryList
					writer.WriteStartElement("BENEFICIARY")
					writer.WriteAttributeString("first_name", Common.SafeString(oItem.firstName))
					If oItem.isTrust = "N" Then
						writer.WriteAttributeString("last_name", Common.SafeString(oItem.lastName))
						If Common.SafeString(oItem.ssn).Length = 9 Then
							writer.WriteAttributeString("ssn", Common.SafeString(oItem.ssn))
						End If
					End If
					If Common.SafeString(oItem.phone) <> "" Then
						writer.WriteAttributeString("phone", Common.SafeString(oItem.phone))
					End If
					writer.WriteAttributeString("relationship", Common.SafeString(oItem.relationship))
					If Not String.IsNullOrEmpty(Common.SafeString(oItem.percentShare)) Then
						writer.WriteAttributeString("percent_share", Common.SafeString(oItem.percentShare))
					End If
					writer.WriteAttributeString("is_trust", Common.SafeString(oItem.isTrust))
					writer.WriteAttributeString("dob", Common.SafeDateXml(oItem.dob))
					If Not String.IsNullOrEmpty(oItem.address) Then
						writer.WriteAttributeString("address", Common.SafeString(oItem.address))
					End If
					If Not String.IsNullOrEmpty(oItem.city) Then
						writer.WriteAttributeString("city", Common.SafeString(oItem.city))
					End If
					If Not String.IsNullOrEmpty(oItem.state) Then
						writer.WriteAttributeString("state", Common.SafeString(oItem.state))
						writer.WriteAttributeString("country", "USA")
					End If
					If Not String.IsNullOrEmpty(oItem.zip) Then
						writer.WriteAttributeString("zip", Common.SafeString(oItem.zip))
					End If
					writer.WriteEndElement()
				Next

				writer.WriteEndElement()
			End If
		End Sub
		Private Sub WriteBusinessInfoToXmlString(writer As XmlWriter, businessInfo As CBABusinessInfo)
			writer.WriteStartElement("BUSINESS_INFO")
			writer.WriteStartElement("COMPANY_INFO")
			writer.WriteAttributeString("tax_id", businessInfo.TaxID)
			writer.WriteAttributeString("business_type", businessInfo.BusinessType)
			writer.WriteAttributeString("business_description", businessInfo.BusinessDescription)
			writer.WriteAttributeString("business_account_type_code", businessInfo.AccountCode)
			writer.WriteAttributeString("company_name", businessInfo.CompanyName)
			'TODO: missing mapping here
			'writer.WriteAttributeString("is_tax_id_ssn", "")
			'writer.WriteAttributeString("xxxx", businessInfo.DoingBusinessAs)
			If Not String.IsNullOrEmpty(businessInfo.Fax) Then
				writer.WriteAttributeString("fax", businessInfo.Fax)
			End If
			If Not String.IsNullOrEmpty(businessInfo.EmailAddr) Then
				writer.WriteAttributeString("email", businessInfo.EmailAddr)
			End If
			writer.WriteAttributeString("phone", businessInfo.HomePhone)
			writer.WriteAttributeString("cell_phone", businessInfo.MobilePhone)
			writer.WriteAttributeString("industry", businessInfo.Industry)
			writer.WriteAttributeString("establish_date", businessInfo.EstablishDate.ToString("yyyy-MM-dd"))
			writer.WriteAttributeString("state_registered", businessInfo.StateRegistered)
			writer.WriteAttributeString("annual_revenue", businessInfo.AnnualRevenue.ToString())
			writer.WriteAttributeString("num_employees", businessInfo.NumberOfEmployees.ToString())
			writer.WriteAttributeString("primary_bank", businessInfo.PrimaryBank)
			writer.WriteStartElement("CURRENT_ADDRESS")
			writer.WriteAttributeString("occupancy_status", CEnum.MapOccupancyTypeToValue(Common.SafeString(businessInfo.OccupyingLocation)))
			writer.WriteAttributeString("occupancy_duration", Common.SafeString(businessInfo.OccupancyDuration))
			writer.WriteAttributeString("other_occupancy_description", Common.SafeString(businessInfo.OccupancyDescription))
			If businessInfo.AddressCountry <> "" And businessInfo.AddressCountry <> "USA" Then
				' three line address, foreign
				writer.WriteStartElement("THREE_LINE_ADDRESS")
				writer.WriteAttributeString("street_address_1", businessInfo.AddressStreet)
				writer.WriteAttributeString("street_address_2", "")
				writer.WriteAttributeString("city", businessInfo.AddressCity)
				writer.WriteAttributeString("country", businessInfo.AddressCountry)
				writer.WriteAttributeString("zip", businessInfo.AddressZip)
				writer.WriteEndElement() ' end three line address
			Else
				'domestic
				writer.WriteStartElement("LOOSE_ADDRESS")
				writer.WriteAttributeString("street_address_1", businessInfo.AddressStreet)
				writer.WriteAttributeString("street_address_2", "")
				writer.WriteAttributeString("city", businessInfo.AddressCity)
				writer.WriteAttributeString("state", businessInfo.AddressState)
				writer.WriteAttributeString("zip", businessInfo.AddressZip)
				writer.WriteEndElement() 'end LOOSE_ADDRESS
			End If
			writer.WriteEndElement() ''end current address
			'If businessInfo.HasMailingAddress Then  'schema require this all the time
			writer.WriteStartElement("MAILING_ADDRESS")
			writer.WriteAttributeString("street_address_1", businessInfo.MailingAddressStreet)
			writer.WriteAttributeString("street_address_2", "")
			writer.WriteAttributeString("city", businessInfo.MailingAddressCity)
			writer.WriteAttributeString("state", businessInfo.MailingAddressState)
			writer.WriteAttributeString("zip", businessInfo.MailingAddressZip)
			writer.WriteAttributeString("country", businessInfo.MailingAddressCountry)
			writer.WriteEndElement() ''end mailing address
			'End If
			If businessInfo.HasPreviousAddress Then
				writer.WriteStartElement("PREVIOUS_ADDRESS")
				writer.WriteAttributeString("street_address_1", businessInfo.PreviousAddressStreet)
				writer.WriteAttributeString("street_address_2", "")
				writer.WriteAttributeString("city", businessInfo.PreviousAddressCity)
				writer.WriteAttributeString("state", businessInfo.PreviousAddressState)
				writer.WriteAttributeString("zip", businessInfo.PreviousAddressZip)
				writer.WriteAttributeString("zip", businessInfo.PreviousAddressCountry)
				writer.WriteEndElement() ''end previous address
			End If
			writer.WriteEndElement() ''end company info
			writer.WriteEndElement() ''end business info
		End Sub
		Private Sub WriteSpecialInfoToXmlString(writer As XmlWriter, specialAccountInfo As CSAAccountInfo)
			writer.WriteStartElement("SPECIAL_INFO")
			writer.WriteAttributeString("special_account_type_code", specialAccountInfo.AccountTypeCode)
			writer.WriteAttributeString("name", specialAccountInfo.AccountName)
			writer.WriteAttributeString("establish_date", specialAccountInfo.EstablishDate.ToString("yyyy-MM-dd"))
			writer.WriteAttributeString("is_tax_id_ssn", "Y")
			writer.WriteAttributeString("tax_id", specialAccountInfo.SSN)
			writer.WriteAttributeString("address1", specialAccountInfo.AddressStreet)
			writer.WriteAttributeString("city", specialAccountInfo.AddressCity)
			writer.WriteAttributeString("zip", specialAccountInfo.AddressZip)
			writer.WriteAttributeString("country", "USA")
			writer.WriteAttributeString("state", specialAccountInfo.AddressState)
			If specialAccountInfo.HasMailingAddress = "Y" Then
				writer.WriteAttributeString("is_mailing_current", "N")
				writer.WriteAttributeString("mailing_address1", specialAccountInfo.MailingAddressStreet)
				writer.WriteAttributeString("mailing_city", specialAccountInfo.MailingAddressCity)
				writer.WriteAttributeString("mailing_zip", specialAccountInfo.MailingAddressZip)
				writer.WriteAttributeString("mailing_country", "USA")
				writer.WriteAttributeString("mailing_state", specialAccountInfo.MailingAddressState)
			Else
				writer.WriteAttributeString("is_mailing_current", "Y")
				writer.WriteAttributeString("mailing_address1", specialAccountInfo.AddressStreet)
				writer.WriteAttributeString("mailing_city", specialAccountInfo.AddressCity)
				writer.WriteAttributeString("mailing_zip", specialAccountInfo.AddressZip)
				writer.WriteAttributeString("mailing_country", "USA")
				writer.WriteAttributeString("mailing_state", specialAccountInfo.AddressState)
			End If
			writer.WriteAttributeString("email", specialAccountInfo.EmailAddr)
			writer.WriteAttributeString("phone", specialAccountInfo.HomePhone)
			writer.WriteEndElement() ''end special info
		End Sub
		Private Sub WriteFOMQuestionsToXMLString(ByVal writer As XmlWriter, oConfig As CWebsiteConfig, oFOMQAs As List(Of Dictionary(Of String, String)))
			If oFOMQAs IsNot Nothing AndAlso oFOMQAs.Count > 0 Then
				'' begin fom question section
				writer.WriteStartElement("FOM_ANSWERED_QUESTIONS")
				For i = 0 To oFOMQAs.Count - 1
					Dim oFQs As List(Of CFOMQuestion)
					Dim oFQ As CFOMQuestion
					Dim sFQName = oFOMQAs(i).Item("FOMName")
					If i = 0 Then ''start question
						oFQs = CFOMQuestion.CurrentFOMQuestions(oConfig).Where(Function(FQ) FQ.Name.ToUpper = sFQName.ToUpper).ToList()
					Else ''next question
						oFQs = CFOMQuestion.DownloadFomNextQuestions(oConfig).Where(Function(FQ) FQ.Name.ToUpper = sFQName.ToUpper).ToList()
					End If
					If oFQs.Count = 0 Then
						Exit For
					End If
					oFQ = oFQs(0)
					writer.WriteStartElement("FOM_ANSWERED_QUESTION")
					writer.WriteAttributeString("name", oFQ.Name)
					writer.WriteStartElement("text_template")
					writer.WriteString(oFQ.TextTemplate)
					writer.WriteEndElement()
					writer.WriteStartElement("FOM_ANSWERS")
					Dim fieldIdx As Integer = 0
					For Each _field As CFOMQuestionField In oFQ.Fields
						writer.WriteStartElement("FOM_ANSWER")
						writer.WriteAttributeString("field_type", _field.Type.ToUpper())
						Dim listFomAnswers = JsonConvert.DeserializeObject(Of List(Of String))(oFOMQAs(i).Item("FOMAnswers"))
						Dim _val As String = If(listFomAnswers.Count > fieldIdx, listFomAnswers(fieldIdx), "")
						Dim _text As String = _val
						writer.WriteStartElement("value")
						writer.WriteString(_val)
						writer.WriteEndElement()
						For Each _ans As CFOMQuestionAnswer In _field.Answers
							If _ans.Value.Equals(_val) Then
								_text = _ans.Text
								Exit For
							End If
						Next
						writer.WriteStartElement("text")
						writer.WriteString(_text)
						writer.WriteEndElement()
						writer.WriteEndElement() ''End FOM_ANSWER
						fieldIdx += 1
					Next
					writer.WriteEndElement() ''End FOM_ANSWERS
					writer.WriteEndElement() ''End FOM_ANSWERED_QUESTION
				Next
				writer.WriteEndElement()
				'' end fom
			End If
		End Sub
	End Class
End Namespace

