﻿Imports log4net
Imports Workflows.Base.BO
Imports Workflows.Base.Handlers
Imports Workflows.XA.BO

Namespace Workflows.XA.Handlers

	Public Class XaPaypalSuccessHandler(Of TS As {New, XaSubmission}, TR As {XaState}, TC As {XaConfig})
		Inherits BaseHandler(Of TS, TR, TC)
		Private log As ILog = LogManager.GetLogger(GetType(XaPaypalSuccessHandler(Of TS, TR, TC)))
		Public Sub New(storage As Storage(Of TS, TR, TC), httpContext As HttpContext)
			MyBase.New(storage, httpContext)
		End Sub
		Protected Overrides Function Process() As Result
            log.Info("Payment is COMPLETED")

            'Flag RunMiscServicesAfterPaypalReturn is here to prevent 2nd call from Paypal
            If CurrentHttpContext.Session("RunMiscServicesAfterPaypalReturn" & CurrentStorage.SessionID) Is Nothing Then
                Dim xaMisc As New XaMisc(CurrentStorage.Config.WebsiteConfig)
                xaMisc.UpdateLoan(True, CurrentStorage.State.LoanID, CurrentStorage.State.LoanRequestXMLStr, CurrentStorage.State.PaymentID)
                CurrentHttpContext.Session("RunMiscServicesAfterPaypalReturn" & CurrentStorage.SessionID) = "yes"
            End If
            '' Either paid or not, just put approve message into session then display it in MLPayPalFinished.aspx
            'this is duplicated of session("APPROVED_MESSSAGE"), but required so we know this process actually go throught
            CurrentHttpContext.Session("PAYPAL_PAYMENT_MESSAGE_" & CurrentStorage.SessionID) = CurrentStorage.State.CustomPreapprovedSuccessMessage
			CurrentHttpResponse.Redirect(String.Format("~/MLPayPalFinished.aspx?lenderref={0}&wfsid={1}", CurrentStorage.Config.WebsiteConfig.LenderRef, CurrentStorage.SessionID), False)
			CurrentStorage.State.ShouldKeepTheSessionDataPersistent = True
			Return Result.Terminate
		End Function
		Private Sub OnTerminated() Handles Me.Terminated
			log.Info("XaPaypalSuccessHandler: Terminated")
		End Sub
		Private Sub OnCompleted() Handles Me.Completed
			log.Info("XaPaypalSuccessHandler: Completed")
		End Sub
	End Class
End Namespace

