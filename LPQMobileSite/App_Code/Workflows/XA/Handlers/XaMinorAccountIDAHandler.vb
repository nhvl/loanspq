﻿Imports log4net
Imports LPQMobile.Utils
Imports Workflows.Base.BO
Imports Workflows.Base.Handlers
Imports Workflows.XA.BO

Namespace Workflows.XA.Handlers

	Public Class XaMinorAccountIDAHandler(Of TS As {New, XaSubmission}, TR As {XaState}, TC As {XaConfig})
		Inherits BaseHandler(Of TS, TR, TC)
		Private log As ILog = LogManager.GetLogger(GetType(XaMinorAccountIDAHandler(Of TS, TR, TC)))
		Public Sub New(storage As Storage(Of TS, TR, TC), httpContext As HttpContext)
			MyBase.New(storage, httpContext)
		End Sub
		Protected Overrides Function Process() As Result
			If Not CurrentStorage.Config.WebsiteConfig.IsDecisionXAEnable AndAlso Common.SafeStringNoTrim(CurrentStorage.Config.WebsiteConfig.CreditPull).ToUpper <> "Y" And CurrentStorage.Config.WebsiteConfig.AuthenticationType = "" AndAlso CurrentStorage.Config.WebsiteConfig.DebitType = "" Then
				CurrentHttpResponse.Clear()
				CurrentHttpResponse.Write(CurrentStorage.State.CustomSubmittedSuccessMessage)
				Return Result.Terminate
			End If
			Dim xaMisc As New XaMisc(CurrentStorage.Config.WebsiteConfig)
			Dim bIDA_LPQConfig As Boolean = xaMisc.IsIDALPQConfig(CurrentStorage.State.SettingProductList, CurrentStorage.SubmissionData.IsSecondary)

			''=======IDA for first applicant==============
			If CurrentStorage.SubmissionData.SecondRoleType.ToUpper <> "NONE" Then
				Dim sApplicantIndex As String = "1"	'' for parent by default

				If CurrentStorage.SubmissionData.MinorUnderWrite.minorIDAEnable = "Y" Then
					sApplicantIndex = "0" ''for minor
				End If
				Dim mExecuteIDAResult = xaMisc.ExecuteIDAMinor(CurrentStorage.SubmissionData.MinorUnderWrite.authenticationType, bIDA_LPQConfig, sApplicantIndex, False, CurrentStorage.SubmissionData, CurrentStorage.State)
				If Not String.IsNullOrEmpty(mExecuteIDAResult) Then
					CurrentHttpResponse.Clear()
					CurrentHttpResponse.Write(mExecuteIDAResult)
					Return Result.Terminate
				End If
			Else ''only minor app
				If CurrentStorage.SubmissionData.MinorUnderWrite.minorIDAEnable = "Y" Then
					Dim mExecuteIDAResult = xaMisc.ExecuteIDAMinor(CurrentStorage.SubmissionData.MinorUnderWrite.authenticationType, bIDA_LPQConfig, "0", False, CurrentStorage.SubmissionData, CurrentStorage.State, True)
					If Not String.IsNullOrEmpty(mExecuteIDAResult) Then
						CurrentHttpResponse.Clear()
						CurrentHttpResponse.Write(mExecuteIDAResult)
						Return Result.Terminate
					End If
				End If
			End If
			Return Result.MoveNext
		End Function

		Private Sub OnTerminated() Handles Me.Terminated
			log.Info("XaMinorAccountIDAHandler: Terminated")
		End Sub
		Private Sub OnCompleted() Handles Me.Completed
			log.Info("XaMinorAccountIDAHandler: Completed")
		End Sub
	End Class
End Namespace