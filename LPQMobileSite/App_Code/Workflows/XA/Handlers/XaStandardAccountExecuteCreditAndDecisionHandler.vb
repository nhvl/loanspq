﻿
Imports log4net
Imports LPQMobile.Utils
Imports Workflows.Base.BO
Imports Workflows.Base.Handlers
Imports Workflows.XA.BO
Imports System.Xml

Namespace Workflows.XA.Handlers

	Public Class XaStandardAccountExecuteCreditAndDecisionHandler(Of TS As {New, XaSubmission}, TR As {XaState}, TC As {XaConfig})
		Inherits BaseHandler(Of TS, TR, TC)
		Private log As ILog = LogManager.GetLogger(GetType(XaStandardAccountExecuteCreditAndDecisionHandler(Of TS, TR, TC)))
		Public Sub New(storage As Storage(Of TS, TR, TC), httpContext As HttpContext)
			MyBase.New(storage, httpContext)
		End Sub
		Protected Overrides Function Process() As Result
			''pull credit or do decisionXA
			Dim xaMisc As New XaMisc(CurrentStorage.Config.WebsiteConfig)
			If IsCreditLPQConfig(CurrentStorage.State.SettingProductList, CurrentStorage.SubmissionData.IsSecondary) And Not CurrentStorage.Config.WebsiteConfig.IsDecisionXAEnable Then
				If Not xaMisc.ExecuteCreditPull(CurrentStorage.State.LoanID, CurrentStorage.SubmissionData.Availability) Then
					CurrentStorage.State.InternalCommentAppend += "Credit pull not successfully run.  "
					CurrentStorage.State.IsUpdateReferralStatusRequired = True
					CurrentHttpResponse.Clear()
					CurrentHttpResponse.Write(CurrentStorage.State.CustomSubmittedSuccessMessage)
					Return Result.Terminate
				End If
			End If
			''underwrite XA product, 
			''for backward compatiblity, this feature need to be enabled for each cu based on available of lender side live code
			''TODO: don't know how this will effect funding(PAYPAL) since it is selected before prodcut is approved
			If CurrentStorage.Config.WebsiteConfig.IsDecisionXAEnable Then
				Dim sXMLresponse = xaMisc.ExecuteDecisionXA(CurrentStorage.State.LoanID)
				If String.IsNullOrWhiteSpace(sXMLresponse) OrElse Not xaMisc.IsOneProductQualified(sXMLresponse) Then
					CurrentStorage.State.InternalCommentAppend += "Products underwriting not successfully run or none of the product is qualified.  "
					CurrentStorage.State.IsUpdateReferralStatusRequired = True
					CurrentHttpResponse.Clear()
					CurrentHttpResponse.Write(CurrentStorage.State.CustomSubmittedSuccessMessage)
					Return Result.Terminate
				End If
				'' PRIMARY ONLY: if decisionXA is enabled, only allow instant approved and booking when ALL products are qualified
				If Not CurrentStorage.SubmissionData.IsSecondary Then
					If Not IsAllProductsQualified(sXMLresponse) Then
						CurrentStorage.State.InternalCommentAppend += "Products underwriting not successfully run or all products are not qualified.  "
						CurrentStorage.State.IsUpdateReferralStatusRequired = True
						CurrentHttpResponse.Clear()
						CurrentHttpResponse.Write(CurrentStorage.State.CustomSubmittedSuccessMessage)
						Return Result.Terminate
					End If
				End If
			End If
			Return Result.MoveNext
		End Function
		Private Sub OnTerminated() Handles Me.Terminated
			log.Info("XaStandardAccountExecuteCreditAndDecisionHandler: Terminated")
		End Sub
		Private Sub OnCompleted() Handles Me.Completed
			log.Info("XaStandardAccountExecuteCreditAndDecisionHandler: Completed")
		End Sub
		Private Function IsAllProductsQualified(ByVal responseXmlStr As String) As Boolean
			Dim responseXmlDoc As New XmlDocument()
			responseXmlDoc.LoadXml(responseXmlStr)
			Dim innerResponseStr = responseXmlDoc.InnerText
			Dim innerResponseXml As XmlDocument = New XmlDocument()
			innerResponseXml.LoadXml(innerResponseStr)
			Dim productNodes As XmlNodeList = innerResponseXml.GetElementsByTagName("XA_PRODUCT")
			If productNodes.Count = 0 Then Return False
			For Each productNode As XmlNode In productNodes
				If Common.SafeString(productNode.Attributes("result").InnerXml).ToUpper <> "QUALIFIED" Then
					Return False
				End If
			Next
			Return True
		End Function
		Private Function IsCreditLPQConfig(ByVal settingProductsList As List(Of CProduct), ByVal isSecondary As Boolean) As Boolean
			If settingProductsList IsNot Nothing AndAlso settingProductsList.Count > 0 Then
				For cIndex As Integer = 0 To settingProductsList.Count - 1
					If (settingProductsList(cIndex) IsNot Nothing AndAlso settingProductsList(cIndex).AutoPullCreditsConsumerPrimary And Not isSecondary) Or
					(settingProductsList(cIndex) IsNot Nothing AndAlso settingProductsList(cIndex).AutoPullCreditsConsumerSecondary And isSecondary) Then
						Return True
					End If
				Next
			End If
			Return False
		End Function
	End Class
End Namespace