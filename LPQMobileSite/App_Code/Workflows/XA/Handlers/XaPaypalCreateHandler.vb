﻿Imports Newtonsoft.Json
Imports log4net
Imports LPQMobile.Utils
Imports Workflows.Base.BO
Imports Workflows.Base.Handlers
Imports Workflows.XA.BO
Imports PayPal.Api

Namespace Workflows.XA.Handlers

	Public Class XaPaypalCreateHandler(Of TS As {New, XaSubmission}, TR As {XaState}, TC As {XaConfig})
		Inherits BaseHandler(Of TS, TR, TC)
		Private log As ILog = LogManager.GetLogger(GetType(XaPaypalCreateHandler(Of TS, TR, TC)))
		Public Sub New(storage As Storage(Of TS, TR, TC), httpContext As HttpContext)
			MyBase.New(storage, httpContext)
		End Sub
		Protected Overrides Function Process() As Result
			Dim res As New CJsonResponse
			Try
				Dim senderEmail As String = CurrentHttpRequest.QueryString("sender")
				'' create item list
				Dim ppItems = buildPayPalItems(CurrentStorage.State.PaypalReceipt)
				Dim paramCollector As New XaParamDataCollector(CurrentStorage.State.RequestParams, CurrentStorage.Config.WebsiteConfig)
				paramCollector.CollectSubmissionData(CurrentStorage.SubmissionData)
				'' define call back url
				Dim url = CurrentHttpRequest.Url.Scheme + "://" + CurrentHttpRequest.Url.Authority + "/MLPayPal.aspx"
				Dim callBackURL = String.Format("{0}?lenderref={1}&type={2}&wfsid={3}", url, CurrentStorage.Config.WebsiteConfig.LenderRef, CurrentStorage.SubmissionData.Availability, CurrentStorage.SessionID)
				'' create payment
				log.InfoFormat("Begin create payment for {0}", CurrentStorage.Config.WebsiteConfig.PayPalEmail)
				Dim sDescription As String = String.Format("Deposit for XA #{0}", CurrentStorage.State.LoanNumber)
				Dim pp As New CPayPalAdaptive()
				res = pp.CreatePayment(ppItems, senderEmail, CurrentStorage.Config.WebsiteConfig.PayPalEmail, callBackURL, sDescription)

				'FOR TESTING
				'res.IsSuccess = True
				'res.Info = New With {.URL = String.Format("/systests/PaypalFaker.aspx?callbackurl={0}", CurrentHttpContext.Server.UrlEncode(callBackURL))}
				'res.Message = "MLPAYPAL_CREATED"
				'END FOR TESTING

				If res.IsSuccess Then '' payment is set
					'https://www.paypal.com/webscr&cmd=_ap-payment&paykey=AP-0FR68581LV699281V
					log.InfoFormat("Payment CREATED. AuthorizationURL: {0}", res.Info.URL)
					res.Info.URL = res.Info.URL.ToString.Replace("webscr&", "webscr?")
					log.InfoFormat("Payment CREATED. Modified AuthorizationURL: {0}", res.Info.URL)
					Dim xaMisc As New XaMisc(CurrentStorage.Config.WebsiteConfig)
					xaMisc.UpdateLoan(False, CurrentStorage.State.LoanID, CurrentStorage.State.LoanRequestXMLStr, CurrentStorage.State.PaymentID, "PENDING")	'put note in comment so officer know that the system is waiting for funding
					CurrentHttpResponse.Write(JsonConvert.SerializeObject(res))	 'response back to xa/script.js saveUserInfo or submitWalletAnswers
					CurrentStorage.State.ShouldKeepTheSessionDataPersistent = True
					Return Result.Terminate
				Else  'fail to create payment but system still need to display approved message, will deal with payment on lenderside
					log.WarnFormat("Payment IS NOT CREATED. Message: {0}", res.Message)
					Dim sPaypalErrorMsg As String = res.Info.APPROVED_MESSAGE
					Dim xaMisc As New XaMisc(CurrentStorage.Config.WebsiteConfig)
					xaMisc.UpdateLoan(False, CurrentStorage.State.LoanID, CurrentStorage.State.LoanRequestXMLStr, CurrentStorage.State.PaymentID, sPaypalErrorMsg)
					'don't want to show all the message to consumer so replace with generic message
					res.Info.APPROVED_MESSAGE = "Unfortunately, we are unable to process your PayPal funding at this time.<br/><br/>" + CurrentStorage.State.CustomPreapprovedSuccessMessage
					CurrentHttpResponse.Write(JsonConvert.SerializeObject(res))	 'response back to xa/script.js 
					Return Result.Terminate
				End If
			Catch ex As Exception
				log.Error(ex)
				res.IsSuccess = False
				res.Message = "MLPAYPAL_FAILED"
				res.Info = New With {.APPROVED_MESSAGE = "Unfortunately, we are unable to process your PayPal funding at this time.<br/><br/>" + CurrentStorage.State.CustomPreapprovedSuccessMessage}
                Dim sPaypalErrorMsg As String = res.Info.APPROVED_MESSAGE
                Dim xaMisc As New XaMisc(CurrentStorage.Config.WebsiteConfig)
                xaMisc.UpdateLoan(False, CurrentStorage.State.LoanID, CurrentStorage.State.LoanRequestXMLStr, CurrentStorage.State.PaymentID, sPaypalErrorMsg)
                CurrentHttpResponse.Write(JsonConvert.SerializeObject(res))  'response back to xa/script.js 
                Return Result.Terminate
			End Try
		End Function
		Private Sub OnTerminated() Handles Me.Terminated
			log.Info("XaPaypalCreateHandler: Terminated")
		End Sub
		Private Sub OnCompleted() Handles Me.Completed
			log.Info("XaPaypalCreateHandler: Completed")
		End Sub
		Private Function buildPayPalItems(products As List(Of CPayPalReceiptItem)) As List(Of Item)
			Dim ppItems As New List(Of Item)
			For Each p In products
				Dim i As New Item()
				i.currency = "USD"
				i.description = p.ProductDescription
				i.name = p.ProductName
				i.quantity = "1"
				i.price = p.DepositAmount.ToString().Replace("$", "")
				ppItems.Add(i)
			Next
			Return ppItems
		End Function
	End Class
End Namespace

