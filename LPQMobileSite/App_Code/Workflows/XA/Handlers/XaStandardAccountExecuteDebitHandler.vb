﻿Imports log4net
Imports LPQMobile.Utils
Imports Workflows.Base.BO
Imports Workflows.Base.Handlers
Imports Workflows.XA.BO

Namespace Workflows.XA.Handlers

	Public Class XaStandardAccountExecuteDebitHandler(Of TS As {New, XaSubmission}, TR As {XaState}, TC As {XaConfig})
		Inherits BaseHandler(Of TS, TR, TC)
		Private log As ILog = LogManager.GetLogger(GetType(XaMinorAccountExecuteDebitHandler(Of TS, TR, TC)))
		Public Sub New(storage As Storage(Of TS, TR, TC), httpContext As HttpContext)
			MyBase.New(storage, httpContext)
		End Sub
		Protected Overrides Function Process() As Result
			Dim xaMisc As New XaMisc(CurrentStorage.Config.WebsiteConfig)
			If xaMisc.IsDebitLPQConfig(CurrentStorage.State.SettingProductList, CurrentStorage.SubmissionData.IsSecondary) Then	''products have AUTOPULLDEBIT  
				If CurrentStorage.Config.WebsiteConfig.DebitType <> "" Then
					If CurrentStorage.SubmissionData.SAApplicants IsNot Nothing AndAlso CurrentStorage.SubmissionData.SAApplicants.Any() Then ''run debit for special accounts
						Dim oSADebit = New CBSADebit()
						oSADebit.oConfig = CurrentStorage.Config.WebsiteConfig
						oSADebit.RequestDebitUrl = xaMisc.GetCurrentDebitRequestUrl()
						oSADebit.SAApplicants = CurrentStorage.SubmissionData.SAApplicants
						If Not oSADebit.ExecuteSADebit(CurrentStorage.State.LoanID) Then	''stop process if not pass
							CurrentStorage.State.InternalCommentAppend += "Debit not successfully run.  "
							CurrentStorage.State.IsUpdateReferralStatusRequired = True
							CurrentHttpResponse.Clear()
							CurrentHttpResponse.Write(CurrentStorage.State.CustomSubmittedSuccessMessage)
							Return Result.Terminate
						End If
					ElseIf CurrentStorage.SubmissionData.BAApplicants IsNot Nothing AndAlso CurrentStorage.SubmissionData.BAApplicants.Any() Then ''run debit for business xa
						Dim oBADebit = New CBSADebit()
						oBADebit.oConfig = CurrentStorage.Config.WebsiteConfig
						oBADebit.RequestDebitUrl = xaMisc.GetCurrentDebitRequestUrl()
						oBADebit.BAApplicants = CurrentStorage.SubmissionData.BAApplicants
						If Not oBADebit.ExecuteBADebit(CurrentStorage.State.LoanID) Then	''stop process if not pass
							CurrentStorage.State.InternalCommentAppend += "Debit not successfully run.  "
							CurrentStorage.State.IsUpdateReferralStatusRequired = True
							CurrentHttpResponse.Clear()
							CurrentHttpResponse.Write(CurrentStorage.State.CustomSubmittedSuccessMessage)
							Return Result.Terminate
						End If
					Else ''run debit for standard xa
						If Not xaMisc.ExecuteDebit(CurrentStorage.State.LoanID, CurrentStorage.SubmissionData.CoAppPersonal IsNot Nothing) Then	''stop process if not pass
							CurrentStorage.State.InternalCommentAppend += "Debit not successfully run.  "
							CurrentStorage.State.IsUpdateReferralStatusRequired = True
							CurrentHttpResponse.Clear()
							CurrentHttpResponse.Write(CurrentStorage.State.CustomSubmittedSuccessMessage)
							Return Result.Terminate
						End If
					End If
				End If
			End If
			Return Result.MoveNext
		End Function
		Private Sub OnTerminated() Handles Me.Terminated
			log.Info("XaStandardAccountExecuteDebitHandler: Terminated")
		End Sub
		Private Sub OnCompleted() Handles Me.Completed
			log.Info("XaStandardAccountExecuteDebitHandler: Completed")
		End Sub

	End Class
End Namespace