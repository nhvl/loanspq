﻿Imports log4net
Imports Workflows.Base.BO
Imports Workflows.Base.Handlers
Imports Workflows.XA.BO
Imports LPQMobile.Utils

Namespace Workflows.XA.Handlers

	Public Class XaStandardAccountIDAHandler(Of TS As {New, XaSubmission}, TR As {XaState}, TC As {XaConfig})
		Inherits BaseHandler(Of TS, TR, TC)
		Private log As ILog = LogManager.GetLogger(GetType(XaStandardAccountIDAHandler(Of TS, TR, TC)))
		Public Sub New(storage As Storage(Of TS, TR, TC), httpContext As HttpContext)
			MyBase.New(storage, httpContext)
		End Sub
		Protected Overrides Function Process() As Result
			Dim xaMisc As New XaMisc(CurrentStorage.Config.WebsiteConfig)
			Dim bIDA_LPQConfig As Boolean = xaMisc.IsIDALPQConfig(CurrentStorage.State.SettingProductList, CurrentStorage.SubmissionData.IsSecondary)
			''run ida if it is SSO and also sso_ida_enabled is "Y"
			Dim disableSSOIDA = CurrentStorage.SubmissionData.HasSSOIDA <> "Y" AndAlso CurrentStorage.SubmissionData.IsSSO
			''for ida authentication
			If bIDA_LPQConfig AndAlso CurrentStorage.Config.WebsiteConfig.AuthenticationType <> "" AndAlso Not disableSSOIDA Then
				Dim selectedSARoles As New Dictionary(Of String, CSpecialAccountRole)
				Dim selectedBARoles As New Dictionary(Of String, CBusinessAccountRole)
				Dim sAuthMessage As String = ""
				Dim sWalletQuestions As String = ""
				Dim bExecWalletQuestions As Boolean
				Dim bAllowIDAuthentication As Boolean = False
				'for IDA Authentication of special account
				If CurrentStorage.SubmissionData.Availability = "1s" Or CurrentStorage.SubmissionData.Availability = "2s" Then
					Dim saAccountTypeList = Common.GetSpecialAccountTypeList(CurrentStorage.Config.WebsiteConfig)
					Dim selectedSAAccountType = saAccountTypeList.FirstOrDefault(Function(p) p.AccountCode.ToUpper() = CurrentStorage.SubmissionData.SAAccountCode.ToUpper())
					If selectedSAAccountType IsNot Nothing AndAlso selectedSAAccountType.Roles IsNot Nothing AndAlso selectedSAAccountType.Roles.Any() Then
						If CurrentStorage.SubmissionData.SAApplicants IsNot Nothing AndAlso CurrentStorage.SubmissionData.SAApplicants.Any() Then
							For Each app In CurrentStorage.SubmissionData.SAApplicants
								Dim selectedRole = selectedSAAccountType.Roles.FirstOrDefault(Function(p) p.RoleType.ToUpper() = app.RoleType.ToUpper())
								If selectedRole IsNot Nothing Then selectedSARoles.Add(app.Prefix, selectedRole)
							Next
						End If
					End If
				ElseIf CurrentStorage.SubmissionData.Availability = "1b" Or CurrentStorage.SubmissionData.Availability = "2b" Then
					Dim baAccountTypeList = Common.GetBusinessAccountTypeList(CurrentStorage.Config.WebsiteConfig)
					Dim selectedBAAccountType = baAccountTypeList.FirstOrDefault(Function(p) p.BusinessType.ToUpper() = CurrentStorage.SubmissionData.BABusinessInfo.BusinessType.ToUpper())
					If selectedBAAccountType IsNot Nothing AndAlso selectedBAAccountType.Roles IsNot Nothing AndAlso selectedBAAccountType.Roles.Any() Then
						For Each app In CurrentStorage.SubmissionData.BAApplicants
							Dim selectedRole = selectedBAAccountType.Roles.FirstOrDefault(Function(p) p.RoleType.ToUpper() = app.RoleType.ToUpper())
							If selectedRole IsNot Nothing Then selectedBARoles.Add(app.Prefix, selectedRole)
						Next
					End If
				End If
				If CurrentStorage.SubmissionData.BAApplicants IsNot Nothing AndAlso CurrentStorage.SubmissionData.BAApplicants.Any() Then 'incase there are ba roles selected, run [IDA] follow the settings corresponding to each role
					Dim applicantIndex As Integer = -1
					Dim iterationIndex As Integer = 0
					For Each applicant In CurrentStorage.SubmissionData.BAApplicants
						If Not applicant.IsJoint.ToUpper().Equals("Y") Then applicantIndex += 1
						If selectedBARoles(applicant.Prefix).AllowIDAuthentication Then
							bAllowIDAuthentication = True
							bExecWalletQuestions = xaMisc.ExecWalletQuestionsBA(sAuthMessage, sWalletQuestions, applicant.IsJoint.ToUpper().Equals("Y"), applicantIndex, iterationIndex, applicant.FirstName, CurrentStorage.State.CustomSubmittedSuccessMessage, CurrentStorage.State.LoanID, CurrentStorage.State.TransactionID, CurrentStorage.State.QuestionSetID, CurrentStorage.State.RequestAppID, CurrentStorage.State.RequestClientID, CurrentStorage.State.RequestSequenceID, CurrentStorage.State.ReferenceNumber, CurrentStorage.State.WalletQuestionList)
							' if run execWallet question successfully, exit for loop to display the questions
							' otherwise, keep the for loop running till to the next applicant that is enabled for IDA running
							If bExecWalletQuestions Then Exit For
						End If
						iterationIndex += 1
					Next
					If Not bAllowIDAuthentication Then ''bypass bExecWalletQuestions if there is no AllowAuthentication
						bExecWalletQuestions = True
					End If
				ElseIf CurrentStorage.SubmissionData.SAApplicants IsNot Nothing AndAlso CurrentStorage.SubmissionData.SAApplicants.Any() Then 'incase there are sa roles selected, run [IDA] follow the settings corresponding to each role
					Dim applicantIndex As Integer = -1
					Dim iterationIndex As Integer = 0
					For Each applicant In CurrentStorage.SubmissionData.SAApplicants
						If Not applicant.IsJoint.ToUpper().Equals("Y") Then applicantIndex += 1
						If selectedSARoles(applicant.Prefix).AllowIDAuthentication Then
							bAllowIDAuthentication = True
							bExecWalletQuestions = xaMisc.ExecWalletQuestionsSA(sAuthMessage, sWalletQuestions, applicant.IsJoint.ToUpper().Equals("Y"), applicantIndex, iterationIndex, applicant.FirstName, CurrentStorage.State.LoanID, CurrentStorage.State.CustomSubmittedSuccessMessage, CurrentStorage.State.TransactionID, CurrentStorage.State.QuestionSetID, CurrentStorage.State.RequestAppID, CurrentStorage.State.RequestClientID, CurrentStorage.State.RequestSequenceID, CurrentStorage.State.ReferenceNumber, CurrentStorage.State.WalletQuestionList)
							If bExecWalletQuestions Then Exit For
						End If
						iterationIndex += 1
					Next
					If Not bAllowIDAuthentication Then ''bypass bExecWalletQuestions if there is no AllowAuthentication
						bExecWalletQuestions = True
					End If
				Else 'otherwise, run [IDA, Debit, Credit] for App, Joint App as normal
					'ida authentication
					CurrentStorage.State.IsDoneWalletQuestionForCoApp = String.Empty	'initialize just in case the process was abondaon the last time
					bExecWalletQuestions = xaMisc.ExecWalletQuestions(sAuthMessage, sWalletQuestions, False, CurrentStorage.SubmissionData.Personal.FirstName, CurrentStorage.State.LoanID, CurrentStorage.State.CustomSubmittedSuccessMessage, CurrentStorage.State.TransactionID, CurrentStorage.State.QuestionSetID, CurrentStorage.State.RequestAppID, CurrentStorage.State.RequestClientID, CurrentStorage.State.RequestSequenceID, CurrentStorage.State.ReferenceNumber, CurrentStorage.State.WalletQuestionList)	 'check for attribute "ida" in LPQMobileWebsite config

				End If
				If Not bExecWalletQuestions Then
					If CurrentStorage.Config.LpqContinueIdaReferral Then
						CurrentStorage.State.IsFailedIDA = True	'' set to true and continue process
					Else
						'Response.Write(customErrorMessage) 'submit result with error so stop and return error msg
						'don't want to sent the error message to consumer to avoid mulitple submission
						CurrentStorage.State.InternalCommentAppend += "IDA not successfully run.  "
						CurrentHttpResponse.Clear()
						CurrentHttpResponse.Write(CurrentStorage.State.CustomSubmittedSuccessMessage)	'
						Return Result.Terminate
					End If
				End If
				If sWalletQuestions <> "" Then 'stop and return wallet question
					CurrentHttpResponse.Write(sWalletQuestions)
					CurrentStorage.State.IsUpdateReferralStatusRequired = False	'don't to update loanstatus before user answer authenticate question
					CurrentStorage.State.ShouldKeepTheSessionDataPersistent = True
					Return Result.Terminate
				End If

				'if bExecWalletQuestions and sAuthMessage <> "" 
				'**IDA not require by Lender  so go on
			End If
			Return Result.MoveNext
		End Function
		Private Sub OnTerminated() Handles Me.Terminated
			log.Info("XaStandardAccountIDAHandler: Terminated")
		End Sub
		Private Sub OnCompleted() Handles Me.Completed
			log.Info("XaStandardAccountIDAHandler: Completed")
		End Sub
	End Class
End Namespace