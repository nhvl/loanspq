﻿Imports log4net
Imports LPQMobile.Utils
Imports Workflows.Base.BO
Imports Workflows.Base.Handlers
Imports Workflows.XA.BO

Namespace Workflows.XA.Handlers

	Public Class XaStandardAccountWalletQuestionsHandler(Of TS As {New, XaSubmission}, TR As {XaState}, TC As {XaConfig})
		Inherits BaseHandler(Of TS, TR, TC)
		Private log As ILog = LogManager.GetLogger(GetType(XaStandardAccountWalletQuestionsHandler(Of TS, TR, TC)))
		Public Sub New(storage As Storage(Of TS, TR, TC), httpContext As HttpContext)
			MyBase.New(storage, httpContext)
		End Sub
		Protected Overrides Function Process() As Result
			CurrentStorage.State.IsUpdateReferralStatusRequired = True
			' After having built the XML and receiving the response with LOAN_ID, we can build the custom non-approved success message
			Dim xaMisc As New XaMisc(CurrentStorage.Config.WebsiteConfig)
			xaMisc.BuildSuccessMessages(CurrentStorage.State, CurrentStorage.SubmissionData.IsSecondary, "", CurrentHttpContext.Server)
			' The second time this page is visited will be for the purpose of submitting the answers to wallet questions
			' Validate the answers then format the answers into proper XML and submit!

			'Submit the answers
			Dim selectedSARoles As New Dictionary(Of String, CSpecialAccountRole)
			Dim selectedBARoles As New Dictionary(Of String, CBusinessAccountRole)
			Dim sAnswerValidate As String = ""
			Dim sWalletMessage As String = ""
			If CurrentStorage.SubmissionData.Availability = "1s" Or CurrentStorage.SubmissionData.Availability = "2s" Then
				Dim saAccountTypeList = Common.GetSpecialAccountTypeList(CurrentStorage.Config.WebsiteConfig)
				Dim selectedSAAccountType = saAccountTypeList.FirstOrDefault(Function(p) p.AccountCode.ToUpper() = CurrentStorage.SubmissionData.SAAccountCode.ToUpper())
				If selectedSAAccountType IsNot Nothing AndAlso selectedSAAccountType.Roles IsNot Nothing AndAlso selectedSAAccountType.Roles.Any() Then
					If CurrentStorage.SubmissionData.SAApplicants IsNot Nothing AndAlso CurrentStorage.SubmissionData.SAApplicants.Any() Then
						For Each app In CurrentStorage.SubmissionData.SAApplicants
							Dim selectedRole = selectedSAAccountType.Roles.FirstOrDefault(Function(p) p.RoleType.ToUpper() = app.RoleType.ToUpper())
							If selectedRole IsNot Nothing Then
								selectedSARoles.Add(app.Prefix, selectedRole)
							End If
						Next
					End If
				End If
			ElseIf CurrentStorage.SubmissionData.Availability = "1b" Or CurrentStorage.SubmissionData.Availability = "2b" Then
				Dim baAccountTypeList = Common.GetBusinessAccountTypeList(CurrentStorage.Config.WebsiteConfig)
				Dim selectedBAAccountType = baAccountTypeList.FirstOrDefault(Function(p) p.BusinessType.ToUpper() = CurrentStorage.SubmissionData.BABusinessInfo.BusinessType.ToUpper())
				If selectedBAAccountType IsNot Nothing AndAlso selectedBAAccountType.Roles IsNot Nothing AndAlso selectedBAAccountType.Roles.Any() Then
					For Each app In CurrentStorage.SubmissionData.BAApplicants
						Dim selectedRole = selectedBAAccountType.Roles.FirstOrDefault(Function(p) p.RoleType.ToUpper() = app.RoleType.ToUpper())
						If selectedRole IsNot Nothing Then
							selectedBARoles.Add(app.Prefix, selectedRole)
						End If
					Next
				End If
			End If
			'incase there are sa roles selected, run [IDA Debit, Credit] follow the settings corresponding to each role
			Dim answerValidateResult As Boolean
			If CurrentStorage.SubmissionData.BAApplicants IsNot Nothing AndAlso CurrentStorage.SubmissionData.BAApplicants.Any() Then
				answerValidateResult = xaMisc.ExecWalletAnswersBA(sAnswerValidate, sWalletMessage, CurrentStorage.SubmissionData.BAApplicants(CurrentStorage.SubmissionData.WalletQuestionIterationIndex).IsJoint.ToUpper().Equals("Y"), CurrentStorage.SubmissionData.WalletQuestionApplicantIndex, CurrentStorage.SubmissionData.BAApplicants(CurrentStorage.SubmissionData.WalletQuestionIterationIndex).FirstName, CurrentStorage.SubmissionData.WalletQuestionsAndAnswers, CurrentStorage.SubmissionData.HasCoFollowOnQuestions.ToUpper().Equals("Y"), CurrentStorage.State.LoanID, CurrentStorage.State.CustomSubmittedSuccessMessage, CurrentStorage.State.TransactionID, CurrentStorage.State.QuestionSetID, CurrentStorage.State.RequestAppID, CurrentStorage.State.RequestClientID, CurrentStorage.State.RequestSequenceID, CurrentStorage.State.ReferenceNumber, CurrentStorage.State.WalletQuestionList, CurrentStorage.SubmissionData.WalletAnswersText)
			ElseIf CurrentStorage.SubmissionData.SAApplicants IsNot Nothing AndAlso CurrentStorage.SubmissionData.SAApplicants.Any() Then
				answerValidateResult = xaMisc.ExecWalletAnswersSA(sAnswerValidate, sWalletMessage, CurrentStorage.SubmissionData.SAApplicants(CurrentStorage.SubmissionData.WalletQuestionIterationIndex).IsJoint.ToUpper().Equals("Y"), CurrentStorage.SubmissionData.WalletQuestionApplicantIndex, CurrentStorage.SubmissionData.SAApplicants(CurrentStorage.SubmissionData.WalletQuestionIterationIndex).FirstName, CurrentStorage.SubmissionData.WalletQuestionsAndAnswers, CurrentStorage.SubmissionData.HasCoFollowOnQuestions.ToUpper().Equals("Y"), CurrentStorage.State.LoanID, CurrentStorage.State.CustomSubmittedSuccessMessage, CurrentStorage.State.TransactionID, CurrentStorage.State.QuestionSetID, CurrentStorage.State.RequestAppID, CurrentStorage.State.RequestClientID, CurrentStorage.State.RequestSequenceID, CurrentStorage.State.ReferenceNumber, CurrentStorage.State.WalletQuestionList, CurrentStorage.SubmissionData.WalletAnswersText)
			Else
				answerValidateResult = xaMisc.ExecWalletAnswers(sAnswerValidate, sWalletMessage, CurrentStorage.SubmissionData.HasCoWalletQuestions.Equals("Y"), CurrentStorage.SubmissionData, CurrentStorage.SubmissionData.WalletQuestionsAndAnswers, CurrentStorage.SubmissionData.HasCoFollowOnQuestions.ToUpper().Equals("Y"), CurrentStorage.State.LoanID, CurrentStorage.State.CustomSubmittedSuccessMessage, CurrentStorage.State.TransactionID, CurrentStorage.State.QuestionSetID, CurrentStorage.State.RequestAppID, CurrentStorage.State.RequestClientID, CurrentStorage.State.RequestSequenceID, CurrentStorage.State.ReferenceNumber, CurrentStorage.State.WalletQuestionList, CurrentStorage.SubmissionData.WalletAnswersText, CurrentStorage.State.IsDoneWalletQuestionForCoApp)
			End If
			If Not answerValidateResult Then
				If CurrentStorage.Config.LpqContinueIdaReferral Then
					CurrentStorage.State.IsFailedIDA = True
				Else
					'Response.Write(customErrorMessage)	'submit erro
					'don't want to sent the error message to consumer to avoid mulitple submission
					CurrentStorage.State.InternalCommentAppend += "IDA not successfully run.  "
					CurrentHttpResponse.Clear()
					CurrentHttpResponse.Write(CurrentStorage.State.CustomSubmittedSuccessMessage)
					Return Result.Terminate
				End If
			End If
			If sAnswerValidate <> "" Then
				If Not CurrentStorage.Config.LpqContinueIdaReferral Then
					CurrentHttpResponse.Write(sAnswerValidate)	'incomplete answer
					CurrentStorage.State.ShouldKeepTheSessionDataPersistent = True
					Return Result.Terminate
				End If
				CurrentStorage.State.IsFailedIDA = True
			End If
			If sWalletMessage.Contains("walletquestion") Then 'follow-on question for deluxe
				Dim strFollowOnQuestion As String = ""
				If CurrentStorage.SubmissionData.HasCoWalletQuestions = "Y" Then
					strFollowOnQuestion = "<input type='hidden' id='hasCoFollowOnQuestions' value='Y' />"
				End If
				CurrentHttpResponse.Write(sWalletMessage + strFollowOnQuestion)
				CurrentStorage.State.IsUpdateReferralStatusRequired = False	'don't to update loanstatus before user answer authenticate question
				CurrentStorage.State.ShouldKeepTheSessionDataPersistent = True
				Return Result.Terminate
			End If
			If CurrentStorage.SubmissionData.BAApplicants IsNot Nothing AndAlso CurrentStorage.SubmissionData.BAApplicants.Any() Then
				'run IDA for other BA applicants
				If CurrentStorage.SubmissionData.WalletQuestionIterationIndex < CurrentStorage.SubmissionData.BAApplicants.Count Then
					Dim index As Integer = 0
					Dim sAuthMessage As String = ""
					Dim sWalletQuestions As String = ""
					Dim bExecWalletQuestions As Boolean = False
					Dim bAllowIDAuthentication As Boolean = False
					For Each applicant In CurrentStorage.SubmissionData.BAApplicants
						If index <= CurrentStorage.SubmissionData.WalletQuestionIterationIndex Then
							index += 1
							Continue For
						End If 'skip the executed applicants
						If Not applicant.IsJoint.ToUpper().Equals("Y") Then CurrentStorage.SubmissionData.WalletQuestionApplicantIndex += 1
						If selectedBARoles(applicant.Prefix).AllowIDAuthentication Then
							bAllowIDAuthentication = True
							bExecWalletQuestions = xaMisc.ExecWalletQuestionsBA(sAuthMessage, sWalletQuestions, applicant.IsJoint.ToUpper().Equals("Y"), CurrentStorage.SubmissionData.WalletQuestionApplicantIndex, index, applicant.FirstName, CurrentStorage.State.CustomSubmittedSuccessMessage, CurrentStorage.State.LoanID, CurrentStorage.State.TransactionID, CurrentStorage.State.QuestionSetID, CurrentStorage.State.RequestAppID, CurrentStorage.State.RequestClientID, CurrentStorage.State.RequestSequenceID, CurrentStorage.State.ReferenceNumber, CurrentStorage.State.WalletQuestionList)
							If bExecWalletQuestions Then Exit For
						End If
						index += 1
					Next
					''bypass bExecWalletQuestions if there is no AllowAuthentication
					If Not bAllowIDAuthentication Then bExecWalletQuestions = True
					If Not bExecWalletQuestions Then
						If CurrentStorage.Config.LpqContinueIdaReferral Then
							CurrentStorage.State.IsFailedIDA = True
						Else
							'Response.Write(customErrorMessage) 'submit result with error so stop and return error msg
							'don't want to sent the error message to consumer to avoid mulitple submission
							CurrentStorage.State.InternalCommentAppend += "IDA not successfully run.  "
							CurrentHttpResponse.Clear()
							CurrentHttpResponse.Write(CurrentStorage.State.CustomSubmittedSuccessMessage)
							Return Result.Terminate
						End If
					End If
					If sWalletQuestions <> "" Then 'stop and return wallet question
						CurrentHttpResponse.Write(sWalletQuestions)
						'need to set this in submitAnswer so authentication will not run
						CurrentStorage.State.IsUpdateReferralStatusRequired = False	'don't to update loanstatus before user answer authenticate question
						CurrentStorage.State.ShouldKeepTheSessionDataPersistent = True
						Return Result.Terminate
					End If
				End If
			ElseIf CurrentStorage.SubmissionData.SAApplicants IsNot Nothing AndAlso CurrentStorage.SubmissionData.SAApplicants.Any() Then
				'run IDA for other SA applicants
				If CurrentStorage.SubmissionData.WalletQuestionIterationIndex < CurrentStorage.SubmissionData.SAApplicants.Count Then
					Dim index As Integer = 0
					Dim sAuthMessage As String = ""
					Dim sWalletQuestions As String = ""
					Dim bExecWalletQuestions As Boolean = False
					Dim bAllowIDAuthentication As Boolean = False
					For Each applicant In CurrentStorage.SubmissionData.SAApplicants
						If index <= CurrentStorage.SubmissionData.WalletQuestionIterationIndex Then
							index += 1
							Continue For
						End If ' skip the executed applicants
						If Not applicant.IsJoint.ToUpper().Equals("Y") Then CurrentStorage.SubmissionData.WalletQuestionApplicantIndex += 1
						If selectedSARoles(applicant.Prefix).AllowIDAuthentication Then
							bAllowIDAuthentication = True
							bExecWalletQuestions = xaMisc.ExecWalletQuestionsSA(sAuthMessage, sWalletQuestions, applicant.IsJoint.ToUpper().Equals("Y"), CurrentStorage.SubmissionData.WalletQuestionApplicantIndex, index, applicant.FirstName, CurrentStorage.State.LoanID, CurrentStorage.State.CustomSubmittedSuccessMessage, CurrentStorage.State.TransactionID, CurrentStorage.State.QuestionSetID, CurrentStorage.State.RequestAppID, CurrentStorage.State.RequestClientID, CurrentStorage.State.RequestSequenceID, CurrentStorage.State.ReferenceNumber, CurrentStorage.State.WalletQuestionList)
							If bExecWalletQuestions Then Exit For
						End If
						index += 1
					Next
					''bypass bExecWalletQuestions if there is no AllowAuthentication
					If Not bAllowIDAuthentication Then bExecWalletQuestions = True
					If Not bExecWalletQuestions Then
						If CurrentStorage.Config.LpqContinueIdaReferral Then
							CurrentStorage.State.IsFailedIDA = True
						Else
							'Response.Write(customErrorMessage) 'submit result with error so stop and return error msg
							'don't want to sent the error message to consumer to avoid mulitple submission
							CurrentStorage.State.InternalCommentAppend += "IDA not successfully run.  "
							CurrentHttpResponse.Clear()
							CurrentHttpResponse.Write(CurrentStorage.State.CustomSubmittedSuccessMessage)
							Return Result.Terminate
						End If
					End If
					If sWalletQuestions <> "" Then 'stop and return wallet question
						CurrentHttpResponse.Write(sWalletQuestions)
						'need to set this in submitAnswer so authentication will not run
						CurrentStorage.State.IsUpdateReferralStatusRequired = False	'don't to update loanstatus before user answer authenticate question
						CurrentStorage.State.ShouldKeepTheSessionDataPersistent = True
						Return Result.Terminate
					End If
				End If
			Else
				''run IDA for joint
				'dont need to check for isIDAEnabled bc by design this else statement is executed when  IDA is enabled,
				If CurrentStorage.SubmissionData.HasJointApplicant And CurrentStorage.State.IsDoneWalletQuestionForCoApp = String.Empty Then
					CurrentStorage.State.IsDoneWalletQuestionForCoApp = "False"
					'ida authentication
					Dim sAuthMessage As String = ""
					Dim sWalletQuestions As String = ""
					If Not xaMisc.ExecWalletQuestions(sAuthMessage, sWalletQuestions, True, CurrentStorage.SubmissionData.CoAppPersonal.FirstName, CurrentStorage.State.LoanID, CurrentStorage.State.CustomSubmittedSuccessMessage, CurrentStorage.State.TransactionID, CurrentStorage.State.QuestionSetID, CurrentStorage.State.RequestAppID, CurrentStorage.State.RequestClientID, CurrentStorage.State.RequestSequenceID, CurrentStorage.State.ReferenceNumber, CurrentStorage.State.WalletQuestionList) Then
						If CurrentStorage.Config.LpqContinueIdaReferral Then
							CurrentStorage.State.IsFailedIDA = True
						Else
							'Response.Write(customErrorMessage) 'submit result with error so stop and return error msg
							'don't want to sent the error message to consumer to avoid mulitple submission
							CurrentStorage.State.InternalCommentAppend += "IDA not successfully run.  "
							CurrentHttpResponse.Clear()
							CurrentHttpResponse.Write(CurrentStorage.State.CustomSubmittedSuccessMessage)
							CurrentStorage.State.IsDoneWalletQuestionForCoApp = String.Empty
							Return Result.Terminate
						End If
					End If

					If sWalletQuestions <> "" Then 'stop and return wallet question
						CurrentHttpResponse.Write(sWalletQuestions + "<input type='hidden' id='hasCoWalletQuestions' value='Y' />")
						'need to set this in submitAnswer so authentication will not run
						CurrentStorage.State.IsUpdateReferralStatusRequired = False	'don't to update loanstatus before user answer authenticate question
						CurrentStorage.State.ShouldKeepTheSessionDataPersistent = True
						Return Result.Terminate
					End If

					'if bExecWalletQuestions and sAuthMessage <> "" 
					'**IDA not require by Lender  so go on
					'need to modify EIDA process
				End If
				CurrentStorage.State.IsDoneWalletQuestionForCoApp = String.Empty
			End If
			Return Result.MoveNext
		End Function
		Private Sub OnTerminated() Handles Me.Terminated
			log.Info("XaStandardAccountWalletQuestionsHandler: Terminated")
		End Sub
		Private Sub OnCompleted() Handles Me.Completed
			log.Info("XaStandardAccountWalletQuestionsHandler: Completed")
		End Sub
	End Class
End Namespace