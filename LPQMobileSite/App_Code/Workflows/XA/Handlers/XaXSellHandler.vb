﻿
Imports log4net
Imports LPQMobile.Utils
Imports Workflows.Base.BO
Imports Workflows.Base.Handlers
Imports Workflows.XA.BO

Namespace Workflows.XA.Handlers

	Public Class XaXSellHandler(Of TS As {New, XaSubmission}, TR As {XaState}, TC As {XaConfig})
		Inherits BaseHandler(Of TS, TR, TC)
		Private log As ILog = LogManager.GetLogger(GetType(XaXSellHandler(Of TS, TR, TC)))
		Public Sub New(storage As Storage(Of TS, TR, TC), httpContext As HttpContext)
			MyBase.New(storage, httpContext)
		End Sub
		Protected Overrides Function Process() As Result
			''at this point the app has passed UW so we want to run xsell even though the app may fial fundig or booking later
			''==Xsell
			Dim bXSell As Boolean = Common.getNodeAttributes(CurrentStorage.Config.WebsiteConfig, "XA_LOAN/VISIBLE", "auto_cross_qualify").Equals("Y")
			CurrentStorage.State.CrossSellMsg = ""
			If bXSell Then
				Dim sessionId As String = Guid.NewGuid().ToString().Substring(0, 8).Replace("-", "").ToLower()
				Dim sLoanNumber = Common.getLoanNumber(CurrentStorage.State.LoanResponseXMLStr)
				CurrentStorage.State.CrossSellMsg = CXSell.GetCrossQualification(CurrentStorage.Config.WebsiteConfig, sLoanNumber, CurrentStorage.Config.LoanType, sessionId)
				Dim postedData = CurrentHttpRequest.Params
				''add xSell comment from XA
				CurrentHttpContext.Session.Add("xaXSell" & sessionId, "CROSS SELL FROM XA#" & sLoanNumber)
				CurrentHttpContext.Session.Add(sessionId, postedData)
				CurrentStorage.State.CustomPreapprovedSuccessMessage = CurrentStorage.State.CustomPreapprovedSuccessMessage + CurrentStorage.State.CrossSellMsg
				CurrentStorage.State.CustomSubmittedSuccessMessage = CurrentStorage.State.CustomSubmittedSuccessMessage + CurrentStorage.State.CrossSellMsg
			End If
			Return Result.MoveNext
		End Function
		Private Sub OnTerminated() Handles Me.Terminated
			log.Info("XaXSellHandler: Terminated")
		End Sub
		Private Sub OnCompleted() Handles Me.Completed
			log.Info("XaXSellHandler: Completed")
		End Sub
		
	End Class
End Namespace