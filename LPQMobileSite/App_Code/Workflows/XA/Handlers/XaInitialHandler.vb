﻿Imports Workflows.Base.BO
Imports Workflows.Base.Handlers
Imports Workflows.XA.BO
Imports LPQMobile.BO
Imports Newtonsoft.Json
Imports System.Reflection
Imports log4net
Imports LPQMobile.Utils.Common
Imports LPQMobile.Utils
Imports System.Web.Script.Serialization

Namespace Workflows.XA.Handlers

	Public Class XaInitialHandler(Of TS As {New, XaSubmission}, TR As {XaState}, TC As {XaConfig})
		Inherits BaseHandler(Of TS, TR, TC)
		Private log As ILog = LogManager.GetLogger(GetType(XaInitialHandler(Of TS, TR, TC)))
		Public Sub New(storage As Storage(Of TS, TR, TC), httpContext As HttpContext)
			MyBase.New(storage, httpContext)
		End Sub
		Protected Overrides Function Process() As Result
			CurrentStorage.SubmissionData.Availability = RequestParamAsString("Availability")
			' Get the custom error and success messages
			' Before we get the custom response messages, we should set the default messages just in case we can't read the values from the XML config file
			GetCustomResponseMessages(CurrentStorage.Config.WebsiteConfig, "XA_LOAN", CurrentStorage.State.CustomErrorMessage, CurrentStorage.State.CustomPreapprovedSuccessMessage, CurrentStorage.State.CustomSubmittedSuccessMessage, CurrentStorage.State.CustomDeclinedMessage, CurrentStorage.State.CustomSubmittedDataList, CurrentStorage.State.CustomPreapprovedDataList, CurrentStorage.State.CustomDeclinedDataList, CurrentStorage.SubmissionData.Availability)
			Dim sSelectedProducts = RequestParamAsString("SelectedProducts")
			If Not String.IsNullOrWhiteSpace(sSelectedProducts) Then
				CurrentStorage.SubmissionData.SelectedProducts = JsonConvert.DeserializeObject(Of List(Of SelectProducts))(sSelectedProducts)
			End If

			Dim paypalReceipt As New List(Of CPayPalReceiptItem)
			Dim settingProductsList As New List(Of CProduct)
			Dim nTotalFunding As Double = 0.0 ' this is used for comparing with ACH micro deposit threshold
			SetProductList(CurrentStorage.SubmissionData.SelectedProducts, CurrentStorage.Config.WebsiteConfig, CurrentStorage.SubmissionData.Availability, settingProductsList, paypalReceipt, nTotalFunding)
			''prevent submitting the application if settingProductsList is nothing
			If settingProductsList.Count = 0 Then
				log.Warn("Unable to submit the application because there is no products.")
				CurrentStorage.State.IsUpdateReferralStatusRequired = False
				CurrentHttpResponse.Write(CurrentStorage.State.CustomErrorMessage)
				Return Result.Terminate
			End If
			CurrentStorage.State.SettingProductList = settingProductsList
			CurrentStorage.State.TotalFunding = nTotalFunding
			CurrentStorage.State.PaypalReceipt = paypalReceipt
			CurrentStorage.State.RequestParams = CurrentHttpRequest.Params
			Dim paramCollector As New XaParamDataCollector(CurrentStorage.State.RequestParams, CurrentStorage.Config.WebsiteConfig)
			paramCollector.CollectSubmissionData(CurrentStorage.SubmissionData)
			CurrentStorage.State.IsFailedIDA = False
			Return Result.MoveNext
		End Function
		Private Sub SetProductList(selectedProducts As List(Of SelectProducts), websiteConfig As CWebsiteConfig, availability As String, ByRef settingProductsList As List(Of CProduct), ByRef paypalReceipt As List(Of CPayPalReceiptItem), ByRef nTotalFunding As Double)
			Dim sAllProducts = CProduct.GetAllProducts(websiteConfig, availability)
			Dim pNum As Integer
			''make sure oProductsList is at least one product
			If selectedProducts.Count > 0 Then
				For pNum = 0 To selectedProducts.Count - 1
					Dim productCode As String = selectedProducts(pNum).productName
					For Each prod As CProduct In sAllProducts
						If (productCode = prod.ProductCode) Then
							settingProductsList.Add(prod)
							If prod.AccountType = "OTHER" Then Exit For 'dont want to fund future interest product, TODO:this should not be necessay if the funding page was done correct
							Dim pp As New CPayPalReceiptItem()
							pp.ProductName = prod.AccountName
							pp.ProductCode = prod.ProductCode
							pp.DepositAmount = selectedProducts(pNum).depositAmount
							pp.ProductDescription = prod.ProductDescription
							'' put it into list, we will use it below if FUNDING option is PAYPAL
							paypalReceipt.Add(pp)
							nTotalFunding += selectedProducts(pNum).depositAmount
							Exit For
						End If
					Next
				Next
			End If
			''Membership fee 
			If SafeDouble(websiteConfig.MembershipFee) > 0 Then
				Dim pp As New CPayPalReceiptItem()
				pp.ProductName = "One-time Membership Fee"
				pp.ProductCode = ""
				pp.DepositAmount = websiteConfig.MembershipFee
				pp.ProductDescription = "One-time Membership Fee"
				'' put it into list, we will use it below if FUNDING option is PAYPAL
				paypalReceipt.Add(pp)
				nTotalFunding += websiteConfig.MembershipFee
			End If
		End Sub
		Private Sub OnTerminated() Handles Me.Terminated
			log.Info("XaInitialHandler: Terminated")
		End Sub
		Private Sub OnCompleted() Handles Me.Completed
			log.Info("XaInitialHandler: Completed")
		End Sub
		
		
	End Class
End Namespace
