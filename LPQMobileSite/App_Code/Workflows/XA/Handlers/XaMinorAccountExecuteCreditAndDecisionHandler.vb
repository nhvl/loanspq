﻿
Imports log4net
Imports Workflows.Base.BO
Imports Workflows.Base.Handlers
Imports Workflows.XA.BO

Namespace Workflows.XA.Handlers

	Public Class XaMinorAccountExecuteCreditAndDecisionHandler(Of TS As {New, XaSubmission}, TR As {XaState}, TC As {XaConfig})
		Inherits BaseHandler(Of TS, TR, TC)
		Private log As ILog = LogManager.GetLogger(GetType(XaMinorAccountExecuteCreditAndDecisionHandler(Of TS, TR, TC)))
		Public Sub New(storage As Storage(Of TS, TR, TC), httpContext As HttpContext)
			MyBase.New(storage, httpContext)
		End Sub
		Protected Overrides Function Process() As Result
			''pull credit or do decisionXA 
			Dim xaMisc As New XaMisc(CurrentStorage.Config.WebsiteConfig)
			If CurrentStorage.SubmissionData.MinorUnderWrite.creditPull = "Y" And CurrentStorage.SubmissionData.MinorUnderWrite.decisionXAEnable <> "Y" Then
				If Not xaMisc.ExecuteCreditPull(CurrentStorage.State.LoanID, CurrentStorage.SubmissionData.Availability, True) Then
					CurrentStorage.State.InternalCommentAppend += "Credit pull not successfully run.  "
					CurrentHttpResponse.Clear()
					CurrentHttpResponse.Write(CurrentStorage.State.CustomSubmittedSuccessMessage)
					CurrentStorage.State.IsUpdateReferralStatusRequired = True
					Return Result.Terminate	''no pass
				End If
			End If
			''underwrite XA product, 
			''for backward compatiblity, this feature need to be enabled for each cu based on available of lender side live code
			''TODO: don't know how this will effect funding(PAYPAL) since it is selected before prodcut is approved
			If CurrentStorage.SubmissionData.MinorUnderWrite.decisionXAEnable = "Y" Then
				Dim sXMLresponse = xaMisc.ExecuteDecisionXA(CurrentStorage.State.LoanID, True)
				If String.IsNullOrWhiteSpace(sXMLresponse) OrElse Not xaMisc.IsOneProductQualified(sXMLresponse) Then
					CurrentStorage.State.InternalCommentAppend += "Products underwriting not successfully run or none of the product is qualified. "
					CurrentHttpResponse.Clear()
					CurrentHttpResponse.Write(CurrentStorage.State.CustomSubmittedSuccessMessage)
					CurrentStorage.State.IsUpdateReferralStatusRequired = True
					Return Result.Terminate	''no pass
				End If
			End If
			Return Result.MoveNext
		End Function
		Private Sub OnTerminated() Handles Me.Terminated
			log.Info("XaMinorAccountExecuteCreditAndDecisionHandler: Terminated")
		End Sub
		Private Sub OnCompleted() Handles Me.Completed
			log.Info("XaMinorAccountExecuteCreditAndDecisionHandler: Completed")
		End Sub
	End Class
End Namespace