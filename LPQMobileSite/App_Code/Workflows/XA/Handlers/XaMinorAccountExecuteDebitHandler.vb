﻿Imports log4net
Imports LPQMobile.Utils
Imports Workflows.Base.BO
Imports Workflows.Base.Handlers
Imports Workflows.XA.BO

Namespace Workflows.XA.Handlers

	Public Class XaMinorAccountExecuteDebitHandler(Of TS As {New, XaSubmission}, TR As {XaState}, TC As {XaConfig})
		Inherits BaseHandler(Of TS, TR, TC)
		Private log As ILog = LogManager.GetLogger(GetType(XaMinorAccountExecuteDebitHandler(Of TS, TR, TC)))
		Public Sub New(storage As Storage(Of TS, TR, TC), httpContext As HttpContext)
			MyBase.New(storage, httpContext)
		End Sub
		Protected Overrides Function Process() As Result
			''if reach here it passed IDA then run Credit Pull or DecisionXA
			''=========Run CreditPull or DecisionXA============= 
			Dim xaMisc As New XaMisc(CurrentStorage.Config.WebsiteConfig)
			Dim bDebitLPQConfig As Boolean = xaMisc.IsDebitLPQConfig(CurrentStorage.State.SettingProductList, CurrentStorage.SubmissionData.IsSecondary)
			'Dim isCreditPull As Boolean = isCreditLPQConfig(settingProductsList)

			Dim sOnlyMinorApp = CurrentStorage.SubmissionData.SecondRoleType.ToUpper = "NONE" OrElse CurrentStorage.SubmissionData.IsOnlyMinorApp = "Y"

			If bDebitLPQConfig Then	''products have AUTOPULLDEBIT
				''run debit
				Dim bDebitLPQMobileWebsiteConfig As String = CurrentStorage.SubmissionData.MinorUnderWrite.debitType.ToUpper
				Dim sMinorDebitEnable As String = CurrentStorage.SubmissionData.MinorUnderWrite.minorDebitEnable
				Dim bExeDebitSuccess As Boolean = True
				Select Case bDebitLPQMobileWebsiteConfig
					Case "FIS" '' -->> for efund
						Dim oEFund As CDebitFIS = New CDebitFIS()
						bExeDebitSuccess = oEFund.Execute_minor(CurrentStorage.State.LoanID, CurrentStorage.SubmissionData.HasJointApplicant, CurrentStorage.Config.WebsiteConfig, xaMisc.EFundsURL, sMinorDebitEnable, sOnlyMinorApp)
					Case "EID"	'' --> for retailbanking
						Dim oRetailBanking As CRetailBankingEID = New CRetailBankingEID()
						bExeDebitSuccess = oRetailBanking.Execute_minor(CurrentStorage.State.LoanID, CurrentStorage.SubmissionData.HasJointApplicant, CurrentStorage.Config.WebsiteConfig, xaMisc.RetailBankingURL, sMinorDebitEnable, sOnlyMinorApp)
					Case "DID"	 '' --> for Deluxe detec
						Dim oDeluxe As CDeluxe = New CDeluxe()
						bExeDebitSuccess = oDeluxe.Execute_minor(CurrentStorage.State.LoanID, CurrentStorage.SubmissionData.HasJointApplicant, CurrentStorage.Config.WebsiteConfig, xaMisc.DeluxeDebitURL, sMinorDebitEnable, sOnlyMinorApp)
					Case "EWS"	 '' --> for Experian Precise EWS
						Dim oExperianEWS As CExperianEWS = New CExperianEWS()
						bExeDebitSuccess = oExperianEWS.Execute_minor(CurrentStorage.State.LoanID, CurrentStorage.SubmissionData.HasJointApplicant, CurrentStorage.Config.WebsiteConfig, xaMisc.ExperianEWSURL, sMinorDebitEnable, sOnlyMinorApp)
				End Select
				If Not bExeDebitSuccess Then
					CurrentStorage.State.InternalCommentAppend += "Debit not successfully run.  "
					CurrentHttpResponse.Clear()
					CurrentHttpResponse.Write(CurrentStorage.State.CustomSubmittedSuccessMessage)
					Return Result.Terminate
				End If
			End If
			Return Result.MoveNext
		End Function
		Private Sub OnTerminated() Handles Me.Terminated
			log.Info("XaMinorAccountExecuteDebitHandler: Terminated")
		End Sub
		Private Sub OnCompleted() Handles Me.Completed
			log.Info("XaMinorAccountExecuteDebitHandler: Completed")
		End Sub
	End Class
End Namespace