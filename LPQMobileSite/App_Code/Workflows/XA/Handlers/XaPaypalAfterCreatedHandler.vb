﻿
Imports log4net
Imports Workflows.Base.BO
Imports Workflows.Base.Handlers
Imports Workflows.XA.BO

Namespace Workflows.XA.Handlers

	Public Class XaPaypalAfterCreatedHandler(Of TS As {New, XaSubmission}, TR As {XaState}, TC As {XaConfig})
		Inherits BaseHandler(Of TS, TR, TC)
		Private log As ILog = LogManager.GetLogger(GetType(XaPaypalAfterCreatedHandler(Of TS, TR, TC)))
		Public Sub New(storage As Storage(Of TS, TR, TC), httpContext As HttpContext)
			MyBase.New(storage, httpContext)
		End Sub
		Protected Overrides Function Process() As Result
			CurrentStorage.State.PaymentID = RequestParamAsString("paymentId")
			CurrentStorage.Config.FundingAndBookingExecutingMode = "SILENCE"
			Dim paramCollector As New XaParamDataCollector(CurrentStorage.State.RequestParams, CurrentStorage.Config.WebsiteConfig)
			paramCollector.CollectSubmissionData(CurrentStorage.SubmissionData)
			Return Result.MoveNext
		End Function
		Private Sub OnTerminated() Handles Me.Terminated
			log.Info("XaPaypalAfterCreatedHandler: Terminated")
		End Sub
		Private Sub OnCompleted() Handles Me.Completed
			log.Info("XaPaypalAfterCreatedHandler: Completed")
		End Sub



	End Class
End Namespace

