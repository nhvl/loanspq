﻿Imports log4net
Imports LPQMobile.Utils
Imports Workflows.Base.BO
Imports Workflows.Base.Handlers
Imports Workflows.XA.BO

Namespace Workflows.XA.Handlers

	Public Class XaMinorAccountLoanSubmitHandler(Of TS As {New, XaSubmission}, TR As {XaState}, TC As {XaConfig})
		Inherits BaseHandler(Of TS, TR, TC)
		Private log As ILog = LogManager.GetLogger(GetType(XaMinorAccountLoanSubmitHandler(Of TS, TR, TC)))
		Public Sub New(storage As Storage(Of TS, TR, TC), httpContext As HttpContext)
			MyBase.New(storage, httpContext)
		End Sub
		Protected Overrides Function Process() As Result
			Dim xaMisc As New XaMisc(CurrentStorage.Config.WebsiteConfig)
			CurrentStorage.State.LoanID = xaMisc.GetLoanID(CurrentStorage.State.LoanResponseXMLStr)
			CurrentStorage.State.LoanNumber = LPQMobile.Utils.Common.getLoanNumber(CurrentStorage.State.LoanResponseXMLStr)
			xaMisc.UploadDocuments(CurrentStorage.State.LoanNumber, CurrentStorage.SubmissionData)
			xaMisc.BuildSuccessMessages(CurrentStorage.State, CurrentStorage.SubmissionData.IsSecondary, "", CurrentHttpContext.Server)
			If CurrentStorage.State.IsFailPreUnderWrite Then
				CurrentHttpResponse.Clear()
				CurrentHttpResponse.Write(CurrentStorage.State.CustomSubmittedSuccessMessage)
				Return Result.Terminate
			End If
			Dim SSNList As New List(Of String)
			If CurrentStorage.SubmissionData.SecondRoleType.ToUpper <> "NONE" Then
				If Not String.IsNullOrWhiteSpace(CurrentStorage.SubmissionData.Personal.SSN) Then
					SSNList.Add(CurrentStorage.SubmissionData.Personal.SSN)
				End If
				If CurrentStorage.SubmissionData.CoAppPersonal IsNot Nothing AndAlso Not String.IsNullOrWhiteSpace(CurrentStorage.SubmissionData.CoAppPersonal.SSN) Then
					SSNList.Add(CurrentStorage.SubmissionData.CoAppPersonal.SSN)
				End If
			End If
			If CurrentStorage.SubmissionData.MinorAppPersonal IsNot Nothing AndAlso Not String.IsNullOrWhiteSpace(CurrentStorage.SubmissionData.MinorAppPersonal.SSN) Then
				SSNList.Add(CurrentStorage.SubmissionData.MinorAppPersonal.SSN)
			End If

			If Common.GetBadMember(SSNList, CurrentStorage.Config.WebsiteConfig) Then
				CurrentHttpResponse.Clear()
				CurrentHttpResponse.Write(CurrentStorage.State.CustomSubmittedSuccessMessage)
				Return Result.Terminate
			End If
			Return Result.MoveNext
		End Function
		Private Sub OnTerminated() Handles Me.Terminated
			log.Info("XaMinorAccountLoanSubmitHandler: Terminated")
		End Sub
		Private Sub OnCompleted() Handles Me.Completed
			log.Info("XaMinorAccountLoanSubmitHandler: Completed")
		End Sub
	End Class
End Namespace