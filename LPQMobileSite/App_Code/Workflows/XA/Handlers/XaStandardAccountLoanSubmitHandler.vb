﻿Imports log4net
Imports LPQMobile.Utils
Imports Workflows.Base.BO
Imports Workflows.Base.Handlers
Imports Workflows.XA.BO

Namespace Workflows.XA.Handlers

	Public Class XaStandardAccountLoanSubmitHandler(Of TS As {New, XaSubmission}, TR As {XaState}, TC As {XaConfig})
		Inherits BaseHandler(Of TS, TR, TC)
		Private log As ILog = LogManager.GetLogger(GetType(XaStandardAccountLoanSubmitHandler(Of TS, TR, TC)))
		Public Sub New(storage As Storage(Of TS, TR, TC), httpContext As HttpContext)
			MyBase.New(storage, httpContext)
		End Sub
		Protected Overrides Function Process() As Result
			Dim xaMisc As New XaMisc(CurrentStorage.Config.WebsiteConfig)
			CurrentStorage.State.LoanID = xaMisc.GetLoanID(CurrentStorage.State.LoanResponseXMLStr)
			CurrentStorage.State.LoanNumber = LPQMobile.Utils.Common.getLoanNumber(CurrentStorage.State.LoanResponseXMLStr)
			xaMisc.UploadDocuments(CurrentStorage.State.LoanNumber, CurrentStorage.SubmissionData)
			xaMisc.BuildSuccessMessages(CurrentStorage.State, CurrentStorage.SubmissionData.IsSecondary, "", CurrentHttpContext.Server)
			If CurrentStorage.State.IsFailPreUnderWrite Then
				CurrentHttpResponse.Clear()
				CurrentHttpResponse.Write(CurrentStorage.State.CustomSubmittedSuccessMessage)
				Return Result.Terminate
			End If
			'check for bad member enable attribute and run result. this is required to prevent further processing cost
			Dim SSNList As New List(Of String)
			If CurrentStorage.SubmissionData.BAApplicants IsNot Nothing AndAlso CurrentStorage.SubmissionData.BAApplicants.Any() Then
				SSNList.AddRange(From app In CurrentStorage.SubmissionData.BAApplicants Select app.SSN)
			ElseIf CurrentStorage.SubmissionData.SAApplicants IsNot Nothing AndAlso CurrentStorage.SubmissionData.SAApplicants.Any() Then
				SSNList.AddRange(From app In CurrentStorage.SubmissionData.SAApplicants Select app.SSN)
			Else
				If CurrentStorage.SubmissionData.Personal IsNot Nothing Then SSNList.Add(CurrentStorage.SubmissionData.Personal.SSN)
				If CurrentStorage.SubmissionData.CoAppPersonal IsNot Nothing Then SSNList.Add(CurrentStorage.SubmissionData.CoAppPersonal.SSN)
			End If

			If Common.GetBadMember(SSNList, CurrentStorage.Config.WebsiteConfig) Then
				CurrentHttpResponse.Clear()
				CurrentHttpResponse.Write(CurrentStorage.State.CustomSubmittedSuccessMessage)
				Return Result.Terminate
			End If
			Return Result.MoveNext
		End Function
		Private Sub OnTerminated() Handles Me.Terminated
			log.Info("XaStandardAccountLoanSubmitHandler: Terminated")
		End Sub
		Private Sub OnCompleted() Handles Me.Completed
			log.Info("XaStandardAccountLoanSubmitHandler: Completed")
		End Sub
	End Class
End Namespace