﻿Imports log4net
Imports Workflows.Base.BO
Imports Workflows.Base.Handlers
Imports Workflows.XA.BO

Namespace Workflows.XA.Handlers

	Public Class XaPaypalCancelHandler(Of TS As {New, XaSubmission}, TR As {XaState}, TC As {XaConfig})
		Inherits BaseHandler(Of TS, TR, TC)
		Private log As ILog = LogManager.GetLogger(GetType(XaPaypalCancelHandler(Of TS, TR, TC)))
		Public Sub New(storage As Storage(Of TS, TR, TC), httpContext As HttpContext)
			MyBase.New(storage, httpContext)
		End Sub
		Protected Overrides Function Process() As Result
			log.Info("Payment is CANCELLED")
			Dim xaMisc As New XaMisc(CurrentStorage.Config.WebsiteConfig)
			xaMisc.UpdateLoan(False, CurrentStorage.State.LoanID, CurrentStorage.State.LoanRequestXMLStr, CurrentStorage.State.PaymentID)

			'still display Approved message also add paypal cancelled msg
			CurrentHttpContext.Session("PAYPAL_PAYMENT_MESSAGE_" & CurrentStorage.SessionID) = "PAYPAL FUNDING HAS BEEN CANCELLED<br/><br/>" + CurrentStorage.State.CustomPreapprovedSuccessMessage
			CurrentHttpResponse.Redirect(String.Format("~/MLPayPalFinished.aspx?lenderref={0}&wfsid={1}", CurrentStorage.Config.WebsiteConfig.LenderRef, CurrentStorage.SessionID), False)
			CurrentStorage.State.ShouldKeepTheSessionDataPersistent = True
			Return Result.Terminate
		End Function
		Private Sub OnTerminated() Handles Me.Terminated
			log.Info("XaPaypalCancelHandler: Terminated")
		End Sub
		Private Sub OnCompleted() Handles Me.Completed
			log.Info("XaPaypalCancelHandler: Completed")
		End Sub
	End Class
End Namespace

