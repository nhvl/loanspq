﻿
Imports System.IO
Imports LPQMobile.BO
Imports log4net
Imports System.Net
Imports Workflows.Base.BO
Imports Workflows.Base.Handlers
Imports Workflows.XA.BO
Imports LPQMobile.Utils
Imports System.Xml

Namespace Workflows.XA.Handlers

	Public Class XaPreUnderWriteHandler(Of TS As {New, XaSubmission}, TR As {XaState}, TC As {XaConfig})
		Inherits BaseHandler(Of TS, TR, TC)
		Private log As ILog = LogManager.GetLogger(GetType(XaPreUnderWriteHandler(Of TS, TR, TC)))
		Public Sub New(storage As Storage(Of TS, TR, TC), httpContext As HttpContext)
			MyBase.New(storage, httpContext)
		End Sub
		Protected Overrides Function Process() As Result
			If Not PreUnderWrite(CurrentStorage.SubmissionData) Then
				CurrentStorage.State.IsFailPreUnderWrite = True	 'need thisfor escaping the loop later
				CurrentStorage.State.IsUpdateReferralStatusRequired = False	' need this to escape status update in Page_loadComplete sub
			End If
			Return Result.MoveNext
		End Function
		Private Sub OnTerminated() Handles Me.Terminated
			log.Info("XAPreUnderWriteHandler: Terminated")
		End Sub
		Private Sub OnCompleted() Handles Me.Completed
			log.Info("XAPreUnderWriteHandler: Completed")
		End Sub
		Public Function IsValidDLExpirationDate(ByVal idCardType As String, ByVal idDateExpire As Date, ByVal hasJointApplicant As Boolean, ByVal coAppIDCardType As String, ByVal coAppIDDateExpire As Date) As Boolean
			Dim maxExpirationDate As New DateTime(2078, 12, 31)	''  "12/31/2078"
			If idCardType.ToUpper = "DRIVERS_LICENSE" Then
				If idDateExpire <> Nothing AndAlso DateTime.Compare(idDateExpire, maxExpirationDate) > 0 Then
					Return False '' is not valid 
				End If
			End If '

			If hasJointApplicant = False Then
				Return True
			Else ''check joint expiration date
				If coAppIDCardType.ToUpper = "DRIVERS_LICENSE" Then
					If coAppIDDateExpire <> Nothing AndAlso DateTime.Compare(coAppIDDateExpire, maxExpirationDate) > 0 Then
						Return False '' is not valid 
					End If
				End If
			End If
			Return True
		End Function
		Private Function PreUnderWrite(ByVal xaAppInfo As XaSubmission) As Boolean
			''check bad member for FOM SSN question answer for BCU 
			If CurrentStorage.Config.WebsiteConfig.LenderRef.NullSafeToLower_.StartsWith("bcu") Then
				If CurrentStorage.Config.WebsiteConfig.LenderCodeBadMember <> "" AndAlso xaAppInfo.SsnFOMAnswer <> "" Then
					Dim SSNList As New List(Of String)
					SSNList.Add(xaAppInfo.SsnFOMAnswer.Split(CType("|", Char))(1))
					Dim isBadMember = Common.GetBadMember(SSNList, CurrentStorage.Config.WebsiteConfig)
					If isBadMember Then
						Return False
					End If
				End If
			End If
			If CurrentStorage.Config.WebsiteConfig.LenderRef.NullSafeToLower_.StartsWith("bcu") OrElse CurrentStorage.Config.WebsiteConfig.LenderRef.NullSafeToLower_.StartsWith("iqcu") Then
                ' 'DL exipration - look for xmlLoan atrribute to match  then   return fail
                If Not IsValidDLExpirationDate(xaAppInfo.IDCard.CardType, Common.SafeDate(xaAppInfo.IDCard.ExpirationDate), xaAppInfo.CoAppPersonal IsNot Nothing, If(xaAppInfo.CoAppIDCard IsNot Nothing, xaAppInfo.CoAppIDCard.CardType, ""), If(xaAppInfo.CoAppIDCard IsNot Nothing, Common.SafeDate(xaAppInfo.CoAppIDCard.ExpirationDate), Nothing)) Then
                    ''fail PreUnderWrite
                    Return False
                End If
                'IRS withholding-look for xmlLoan atrribute to match  then   return fail          
                If IsIRSWithHolding(xaAppInfo.CustomQuestionAnswers) Then
					''fail PreUnderWrite
					Return False
				End If
			End If

			If CurrentStorage.Config.WebsiteConfig.LenderRef.NullSafeToLower_.StartsWith("trustonefcu") Then
				If IsBSA(xaAppInfo.ApplicantQuestionList) Then
					''fail PreUnderWrite
					Return False
				End If
			End If

			Return True
		End Function
		Private Function IsBSA(ByVal listCQJsonQuestionAnswers As List(Of CValidatedQuestionAnswers)) As Boolean
			'' make sure the list is not empty

			If listCQJsonQuestionAnswers Is Nothing Then Return ""

			Dim bsaQuestionNames() As String = {"BSA QUESTION A", "BSA QUESTION B", "BSA QUESTION C", "BSA QUESTION D"}
			Dim answerFound = listCQJsonQuestionAnswers.Any(Function(qa) bsaQuestionNames.Any(Function(name) qa.Question.Name.StartsWith(name)) AndAlso
																qa.Answers.Any(Function(x) x.AnswerValue.Equals("YES", StringComparison.OrdinalIgnoreCase)))

			Return answerFound
		End Function
		Private Function IsIRSWithHolding(customQuestionAnswers As List(Of CValidatedQuestionAnswers)) As Boolean
			''BCU lender is no longer using IRS_WITHHOLDING custom question to check underwriting
			' make sure the list is not empty

			If customQuestionAnswers Is Nothing Then Return ""

			Dim answerFound As Boolean = customQuestionAnswers.Any(Function(qa) "CERTIFICATION AND BACKUP WITHHOLDING".Equals(qa.Question.Name, StringComparison.OrdinalIgnoreCase) AndAlso
																	   qa.Answers.Any(Function(a) "I AM SUBJECT TO BACKUP WITHHOLDING".Equals(a.AnswerValue, StringComparison.OrdinalIgnoreCase)))

			Return answerFound
		End Function
	End Class
End Namespace

