﻿Imports log4net
Imports Workflows.Base.BO
Imports Workflows.Base.Handlers
Imports Workflows.XA.BO
Imports System.Xml

Namespace Workflows.XA.Handlers

	Public Class XaMinorAccountWalletQuestionsHandler(Of TS As {New, XaSubmission}, TR As {XaState}, TC As {XaConfig})
		Inherits BaseHandler(Of TS, TR, TC)
		Private log As ILog = LogManager.GetLogger(GetType(XaMinorAccountWalletQuestionsHandler(Of TS, TR, TC)))
		Public Sub New(storage As Storage(Of TS, TR, TC), httpContext As HttpContext)
			MyBase.New(storage, httpContext)
		End Sub
		Protected Overrides Function Process() As Result
			''run ida for parent and parent joint if available
			'execute for id authentication answer submission
			CurrentStorage.State.IsUpdateReferralStatusRequired = True
			' After having built the XML and receiving the response with LOAN_ID, we can build the custom non-approved success message
			Dim xaMisc As New XaMisc(CurrentStorage.Config.WebsiteConfig)
			xaMisc.BuildSuccessMessages(CurrentStorage.State, CurrentStorage.SubmissionData.IsSecondary, "", CurrentHttpContext.Server)
			' The second time this page is visited will be for the purpose of submitting the answers to wallet questions
			' Validate the answers then format the answers into proper XML and submit!
			'Submit the answers
			Dim sAnswerValidate As String = ""
			Dim sWalletMessage As String = ""
			Dim sMinorWalletAnswer As String = RequestParamAsString("HasMinorWalletAnswer")
			Dim sWalletQuestionsAndAnswers As String = RequestParamAsString("walletQuestionsAndAnswers")
			Dim sWalletAnswersText As String = RequestParamAsString("WalletAnswersText")
			Dim bIDA_LPQConfig As Boolean = xaMisc.IsIDALPQConfig(CurrentStorage.State.SettingProductList, CurrentStorage.SubmissionData.IsSecondary)
			If sMinorWalletAnswer = "MINOR" Then
				''wallet answers for minor
				If Not xaMisc.ExecWalletAnswersMinor(sAnswerValidate, sWalletMessage, "0", False, CurrentStorage.SubmissionData.MinorUnderWrite.authenticationType, CurrentStorage.SubmissionData, sWalletQuestionsAndAnswers, sWalletAnswersText, CurrentStorage.State) Then
					If CurrentStorage.Config.LpqContinueIdaReferral Then
						CurrentStorage.State.IsFailedIDA = True
					Else
						CurrentStorage.State.InternalCommentAppend += "IDA not successfully run.  "
						CurrentHttpResponse.Clear()
						CurrentHttpResponse.Write(CurrentStorage.State.CustomSubmittedSuccessMessage)
						Return Result.Terminate
					End If
				End If
				If sAnswerValidate <> "" Then
					If CurrentStorage.Config.LpqContinueIdaReferral Then
						CurrentStorage.State.IsFailedIDA = True
					Else
						CurrentHttpResponse.Write(sAnswerValidate)	'incomplete answer
						CurrentStorage.State.ShouldKeepTheSessionDataPersistent = True
						Return Result.Terminate
					End If
				End If

				If sWalletMessage <> "" Then
					If sWalletMessage.Contains("walletquestion") Then ''DID 
						Dim sHiddenIsOnlyMinorApp As String = ""
						If CurrentStorage.SubmissionData.IsOnlyMinorApp = "Y" Then
							sHiddenIsOnlyMinorApp = "<input type='hidden' id='hdIsOnlyMinorApp' value ='Y'/>"
						End If
						CurrentHttpResponse.Write(sWalletMessage + "<input type='hidden' id='hdMinorWalletAnswer' value ='MINOR'/>" + sHiddenIsOnlyMinorApp)
						CurrentStorage.State.IsUpdateReferralStatusRequired = False	'don't to update loanstatus before user answer authenticate question
						CurrentStorage.State.ShouldKeepTheSessionDataPersistent = True
						Return Result.Terminate
					End If

					If CurrentStorage.State.IsUpdateReferralStatusRequired Then
						CurrentStorage.State.IsFailedIDA = True	''continue process
					Else
						CurrentStorage.State.InternalCommentAppend += "IDA not successfully run.  "
						CurrentHttpResponse.Write(sWalletMessage)	'fail autehntication '
						Return Result.Terminate
					End If
				End If
				If CurrentStorage.SubmissionData.IsOnlyMinorApp <> "Y" Then
					''Pass ida for minor, then check ida for custodian
					Dim mExecuteIDAResult = xaMisc.ExecuteIDAMinor(CurrentStorage.SubmissionData.MinorUnderWrite.authenticationType, bIDA_LPQConfig, "1", False, CurrentStorage.SubmissionData, CurrentStorage.State)	''for custodian applicant
					If Not String.IsNullOrEmpty(mExecuteIDAResult) Then
						CurrentHttpResponse.Clear()
						CurrentHttpResponse.Write(mExecuteIDAResult)
						Return Result.Terminate
					End If
				End If
			ElseIf sMinorWalletAnswer = "CUSTODIAN" Then
				''wallet answers for Custodian/parent
				If Not xaMisc.ExecWalletAnswersMinor(sAnswerValidate, sWalletMessage, "1", False, CurrentStorage.SubmissionData.MinorUnderWrite.authenticationType, CurrentStorage.SubmissionData, sWalletQuestionsAndAnswers, sWalletAnswersText, CurrentStorage.State) Then
					If CurrentStorage.Config.LpqContinueIdaReferral Then
						CurrentStorage.State.IsFailedIDA = True	''continue process
					Else
						CurrentStorage.State.InternalCommentAppend += "IDA not successfully run.  "
						CurrentHttpResponse.Clear()
						CurrentHttpResponse.Write(CurrentStorage.State.CustomSubmittedSuccessMessage)
						Return Result.Terminate
					End If
				End If

				If sAnswerValidate <> "" Then
					If CurrentStorage.Config.LpqContinueIdaReferral Then
						CurrentStorage.State.IsFailedIDA = True	''continue process
					Else
						CurrentHttpResponse.Write(sAnswerValidate)	'incomplete answer
						CurrentStorage.State.ShouldKeepTheSessionDataPersistent = True
						Return Result.Terminate
					End If
				End If
				If sWalletMessage <> "" Then
					If sWalletMessage.Contains("walletquestion") Then
						CurrentHttpResponse.Write(sWalletMessage + "<input type='hidden' id='hdMinorWalletAnswer' value ='CUSTODIAN'/>")
						CurrentStorage.State.IsUpdateReferralStatusRequired = False	'don't to update loanstatus before user answer authenticate question
						CurrentStorage.State.ShouldKeepTheSessionDataPersistent = True
						Return Result.Terminate
					End If

					If CurrentStorage.Config.LpqContinueIdaReferral Then
						CurrentStorage.State.IsFailedIDA = True	''continue process
					Else
						CurrentStorage.State.InternalCommentAppend += "IDA not successfully run.  "
						CurrentHttpResponse.Write(sWalletMessage)	'fail autehntication '
						Return Result.Terminate
					End If
				End If

				If CurrentStorage.SubmissionData.HasJointApplicant Then
					''Pass ida for custodian, then check ida for joint custodian 
					Dim mExecuteIDAResult = xaMisc.ExecuteIDAMinor(CurrentStorage.SubmissionData.MinorUnderWrite.authenticationType, bIDA_LPQConfig, "1", True, CurrentStorage.SubmissionData, CurrentStorage.State)	''for joint custodian  applicant
					If Not String.IsNullOrEmpty(mExecuteIDAResult) Then
						CurrentHttpResponse.Clear()
						CurrentHttpResponse.Write(mExecuteIDAResult)
						Return Result.Terminate
					End If
				End If

			ElseIf sMinorWalletAnswer = "CUSTODIAN_JOINT" Then
				''wallet answers for custodian/parent joint
				If Not xaMisc.ExecWalletAnswersMinor(sAnswerValidate, sWalletMessage, "1", True, CurrentStorage.SubmissionData.MinorUnderWrite.authenticationType, CurrentStorage.SubmissionData, sWalletQuestionsAndAnswers, sWalletAnswersText, CurrentStorage.State) Then
					If CurrentStorage.Config.LpqContinueIdaReferral Then
						CurrentStorage.State.IsFailedIDA = True	''continue process
					Else
						CurrentStorage.State.InternalCommentAppend += "IDA not successfully run.  "
						CurrentHttpResponse.Clear()
						CurrentHttpResponse.Write(CurrentStorage.State.CustomSubmittedSuccessMessage)
						Return Result.Terminate
					End If
				End If

				If sAnswerValidate <> "" Then
					If CurrentStorage.Config.LpqContinueIdaReferral Then
						CurrentStorage.State.IsFailedIDA = True	''continue process
					Else
						CurrentHttpResponse.Write(sAnswerValidate)	'incomplete answer
						CurrentStorage.State.ShouldKeepTheSessionDataPersistent = True
						Return Result.Terminate
					End If
				End If

				If sWalletMessage <> "" Then
					If sWalletMessage.Contains("walletquestion") Then
						CurrentHttpResponse.Write(sWalletMessage + "<input type='hidden' id='hdMinorWalletAnswer' value ='CUSTODIAN_JOINT'/>")
						CurrentStorage.State.IsUpdateReferralStatusRequired = False	'don't to update loanstatus before user answer authenticate question
						CurrentStorage.State.ShouldKeepTheSessionDataPersistent = True
						Return Result.Terminate
					End If

					If CurrentStorage.Config.LpqContinueIdaReferral Then
						CurrentStorage.State.IsFailedIDA = True	''continue process
					Else
						CurrentStorage.State.InternalCommentAppend += "IDA not successfully run.  "
						CurrentHttpResponse.Write(sWalletMessage)	'fail autehntication '
						Return Result.Terminate
					End If
				End If
			End If
			Return Result.MoveNext
		End Function
		Private Sub OnTerminated() Handles Me.Terminated
			log.Info("XaMinorAccountWalletQuestionsHandler: Terminated")
		End Sub
		Private Sub OnCompleted() Handles Me.Completed
			log.Info("XaMinorAccountWalletQuestionsHandler: Completed")
		End Sub
	End Class
End Namespace