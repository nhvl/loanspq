﻿Imports LPQMobile.Utils
Imports Workflows.XA.Handlers
Imports Workflows.Base.Handlers
Imports Workflows.Base.BO
Imports Workflows.XA.BO
Imports Workflows.Base

Namespace Workflows.XA
	Public Class XaPaypalWorkflow(Of TS As {New, XaSubmission}, TR As {XaState}, TC As {XaConfig})
		Inherits BaseWorkflow(Of TS, TR, TC)

		Public Sub New(httpContext As HttpContext, storage As Storage(Of TS, TR, TC))
			MyBase.New(httpContext, storage)
			CurrentStorage.State.InternalCommentAppend = ""	'reset
		End Sub

		Protected Overrides Function RegisterNodes() As List(Of BaseHandler(Of TS, TR, TC))
			Dim nodeList As New List(Of BaseHandler(Of TS, TR, TC))
			Dim isCancelled As Boolean = Common.SafeString(CurrentHttpContext.Request.Params("cancel")).Equals("true", StringComparison.OrdinalIgnoreCase)
			Dim isPaid As Boolean = Common.SafeString(CurrentHttpContext.Request.Params("success")).Equals("true", StringComparison.OrdinalIgnoreCase)
			If isCancelled OrElse isPaid Then
				' found out that the paypal uses a server side script which redirects the user to the return url after a while and when we explicitly click on return button, then the paypal server script again hits the return url on its own thus we get two responses/hits on our return url.
				'to prevent some of services run more than one time in case user explicitly click on  return button, we use session flag "RunServicesAfterPaypalReturn" & CurrentStorage.SessionID to determine whether those services run already or not and keep the workflow session alive until MLPaypalFinished page loaded
				nodeList.Add(New XaPaypalAfterCreatedHandler(Of TS, TR, TC)(CurrentStorage, CurrentHttpContext))
                If isPaid AndAlso CurrentHttpContext.Session("RunServicesAfterPaypalReturn" & CurrentStorage.SessionID) Is Nothing Then
                    nodeList.Add(New XaStandardAccountFundingAndBookingHandler(Of TS, TR, TC)(CurrentStorage, CurrentHttpContext))
                    CurrentHttpContext.Session("RunServicesAfterPaypalReturn" & CurrentStorage.SessionID) = "yes"
                End If
                If isPaid Then
                    ''execute payment,  Request from PAYPAL, $ is deposited
                    'this is always required so system can be redirected to final page in the event Paypal do multiple callback
                    nodeList.Add(New XaPaypalSuccessHandler(Of TS, TR, TC)(CurrentStorage, CurrentHttpContext))
                ElseIf isCancelled Then	'' Request from PAYPAL, $ is cancelled
					'' Request from PAYPAL, $ is cancelled
					nodeList.Add(New XaPaypalCancelHandler(Of TS, TR, TC)(CurrentStorage, CurrentHttpContext))
				End If
			Else
				''create payment preapproval, request from  XA callback page, not from PAYPAL
				nodeList.Add(New XaPaypalCreateHandler(Of TS, TR, TC)(CurrentStorage, CurrentHttpContext))
			End If
			Return nodeList
		End Function
		Private Sub OnCompleted() Handles Me.Completed
			If CurrentStorage.State.IsUpdateReferralStatusRequired And CurrentStorage.Config.WebsiteConfig.LoanStatusEnum.ToUpper() <> "" Then
				Dim xaMisc As New XaMisc(CurrentStorage.Config.WebsiteConfig)
				xaMisc.UpdateLoan("REFERRED", CurrentStorage.State, CurrentStorage.SubmissionData, False)
			End If
		End Sub

		Protected Overrides Sub LoadPersistentData()
			If CurrentHttpContext.Session(CurrentStorage.StateSessionID) IsNot Nothing Then
				Dim inSessionStorage = CType(CurrentHttpContext.Session(CurrentStorage.StateSessionID), TR)
				CurrentStorage.SetState(inSessionStorage)
			End If

			If CurrentHttpContext.Session(CurrentStorage.RequestParamsSessionID) IsNot Nothing Then
				CurrentStorage.State.RequestParams = CType(CurrentHttpContext.Session(CurrentStorage.RequestParamsSessionID), NameValueCollection)
			End If
		End Sub

		Protected Overrides Sub SavePersistentData()
			CurrentHttpContext.Session(CurrentStorage.StateSessionID) = CurrentStorage.State
		End Sub
	End Class
End Namespace

