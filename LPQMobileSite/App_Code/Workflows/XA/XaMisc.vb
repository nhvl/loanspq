﻿Imports System.IO
Imports Newtonsoft.Json
Imports Workflows.XA.BO
Imports LPQMobile.Utils
Imports System.Xml
Imports System.Net
Imports LPQMobile.BO

Namespace Workflows.XA
	Public Class XaMisc
#Region "Private Members"
		Private websiteConfig As CWebsiteConfig
		Private log As log4net.ILog = log4net.LogManager.GetLogger(GetType(XaMisc))

#End Region
#Region "Public Members"
		Public ReadOnly Property LoanSubmitURL As String
			Get
				Return String.Concat(websiteConfig.BaseSubmitLoanUrl, "/SubmitLoan/SubmitLoan.aspx")
			End Get
		End Property
		Public ReadOnly Property GetLoansURL As String
			Get
				Return String.Concat(websiteConfig.BaseSubmitLoanUrl, "/GetLoans/GetLoans.aspx")
			End Get
		End Property
		Public ReadOnly Property EFundsURL As String
			Get
				Return String.Concat(websiteConfig.BaseSubmitLoanUrl, "/eFunds/eFunds.aspx")
			End Get
		End Property
		Public ReadOnly Property RetailBankingURL As String
			Get
				Return String.Concat(websiteConfig.BaseSubmitLoanUrl, "/RetailBanking/RetailBanking.aspx")
			End Get
		End Property
		Public ReadOnly Property DeluxeDebitURL As String
			Get
				Return String.Concat(websiteConfig.BaseSubmitLoanUrl, "/DeluxeDetection/DeluxeDetect.aspx")
			End Get
		End Property
		Public ReadOnly Property ExperianEWSURL As String
			Get
				Return String.Concat(websiteConfig.BaseSubmitLoanUrl, "PreciseID/EWS.aspx")
			End Get
		End Property
		Public ReadOnly Property CreditPullURL As String
			Get
				Return String.Concat(websiteConfig.BaseSubmitLoanUrl, "/pullCreditReport/pullCreditReport.aspx")
			End Get
		End Property
		Public ReadOnly Property DecisionXAURL As String
			Get
				Return String.Concat(websiteConfig.BaseSubmitLoanUrl, "/decisionXA/decisionXA.aspx")
			End Get
		End Property

		Public ReadOnly Property FundDepositsURL As String
			Get
				Return String.Concat(websiteConfig.BaseSubmitLoanUrl, "/funddeposits/funddeposits.aspx")
			End Get
		End Property

#End Region
#Region "Public Methods"

		Public Function ExecuteDebit(ByVal loanID As String, hasJointApplicant As Boolean) As Boolean
			Dim bExeDebitSuccess As Boolean = True
			Select Case Common.SafeStringNoTrim(websiteConfig.DebitType).ToUpper
				Case "FIS"
					Dim sEfundMessage As String = ""
					bExeDebitSuccess = ExecuteEfund(sEfundMessage, loanID, hasJointApplicant)
					If sEfundMessage <> "ACCEPT" Then	 '
						'Refer or failed, abandon other steps
						Return False
					End If
				Case "EID"
					Dim sRBankingMessage As String = ""
					bExeDebitSuccess = ExecuteRetailBanking(sRBankingMessage, loanID, hasJointApplicant)
					If sRBankingMessage <> "ACCEPT" Then	'   
						'Refer or failed, abandon other steps
						Return False
					End If
				Case "DID"
					Dim oDeluxe As New CDeluxe()
					bExeDebitSuccess = oDeluxe.Execute(loanID, hasJointApplicant, websiteConfig, DeluxeDebitURL)
				Case "EWS"
					Dim oExperianEWS As CExperianEWS = New CExperianEWS()
					bExeDebitSuccess = oExperianEWS.Execute(loanID, hasJointApplicant, websiteConfig, ExperianEWSURL)
			End Select
			Return bExeDebitSuccess
		End Function
		Public Function GetCurrentDebitRequestUrl() As String
			Select Case Common.SafeStringNoTrim(websiteConfig.DebitType).ToUpper
				Case "FIS"
					Return EFundsURL
				Case "EID"
					Return RetailBankingURL
				Case "DID"
					Return DeluxeDebitURL
				Case "EWS"
					Return ExperianEWSURL
			End Select
			Return ""
		End Function
		Public Function ExecWalletAnswers(ByRef psAnswerValidate As String, ByRef psWalletMessage As String, isJoint As Boolean, ByVal xaAppInfo As XaSubmission, ByVal walletQuestionsAndAnswers As String, ByVal hasCoFollowOnQuestions As Boolean, ByVal loanID As String, ByVal customSubmittedSuccessMessage As String, ByRef transactionID As String, ByRef questionSetID As String, ByRef requestAppID As String, ByRef requestClientID As String, ByRef requestSequenceID As String, ByRef referenceNumber As String, ByRef questionListPersist As List(Of CWalletQuestionRendering.questionDataClass), ByVal walletAnswersText As String, ByRef isDoneWalletQuestionForCoApp As String) As Boolean
			' First, we need to get the id's of the questions and answers
			Dim questionIDList As New List(Of String)
			Dim answerIDList As New List(Of String)
			''answerIDList = parseAnswersForID()
			''Dim areAnswersValid As Boolean = getQuestionIDs(questionIDList, answerIDList, questionList_persist)

			' '' If the answer IDs didn't match up with any question, then there was something wrong
			''If (areAnswersValid = False) Then
			''    psAnswerValidate = ("Answers to Out of Wallet Questions are invalid!")
			''    Return True
			''End If

			''dim get wallet question and answer
			Dim oQuestionsAndAnswers = New List(Of CXAQuestionAnswer)
			If Not String.IsNullOrEmpty(walletQuestionsAndAnswers) Then
				oQuestionsAndAnswers = JsonConvert.DeserializeObject(Of List(Of CXAQuestionAnswer))(walletQuestionsAndAnswers)
			End If
			If oQuestionsAndAnswers.Count = 0 Then
				psAnswerValidate = "Answers to Out of Wallet Questions are invalid!"
				Return True	''no wallet question and answer
			End If
			For Each oQA In oQuestionsAndAnswers
				questionIDList.Add(oQA.q)
				answerIDList.Add(oQA.a)
			Next

			' Determine which authentication method to use.
			' Each method has different URLs and different XML requests.
			Dim questionsURL As String = ""
			Dim answersURL As String = ""
			Dim authenticationMethod As String = DetermineAuthenticationType(questionsURL, answersURL)
			' The answers are valid, so we package them up in the proper XML and send it off
			Dim xaBuildWalletAnswer As New CXABuildWalletAnswers(websiteConfig, isJoint, loanID, transactionID, questionIDList)
			Dim walletAnswerXML As String = BuildWalletAnswerXML(xaBuildWalletAnswer, answerIDList, authenticationMethod, questionSetID, requestAppID, requestClientID, requestSequenceID, referenceNumber, walletAnswersText, hasCoFollowOnQuestions)
			' Done building the XML, now ship it off
			Dim req As WebRequest = WebRequest.Create(answersURL)

			req.Method = "POST"
			req.ContentType = "text/xml"

			log.Info("ExecWalletAnswers Data: " & CSecureStringFormatter.MaskSensitiveXMLData(walletAnswerXML))

			Using writer As New StreamWriter(req.GetRequestStream())
				writer.Write(walletAnswerXML)
			End Using

			Dim res As WebResponse = req.GetResponse()
			Dim sXml As String
			Using reader As New StreamReader(res.GetResponseStream())
				sXml = reader.ReadToEnd()

				If sXml.Contains("INTERNAL_ERROR") Then
					log.Warn("ExecWalletAnswers Respsonse: " + sXml)
				Else
					log.Info("ExecWalletAnswers Response: " + sXml)
				End If
				If isJoint Then isDoneWalletQuestionForCoApp = "Done"

			End Using
			res.GetResponseStream().Close()
			req.GetRequestStream().Close()

			' Check to see if the server's response to the wallet answers is not an error
			If Not IsSubmitted(sXml) Then
				Return False
			End If


			' There are no errors, so check to see if PID or RSA was approved. 
			If Not HasPassedAuthentication(authenticationMethod, sXml) Then	 'execute this loop when not approved

				If HasFollowOnQuestion(authenticationMethod, sXml) Then
					Dim appName As String = xaAppInfo.Personal.FirstName
					If isDoneWalletQuestionForCoApp <> "" And isJoint Then
						appName = xaAppInfo.CoAppPersonal.FirstName
					End If
					psWalletMessage = GetFollowOnQuestion(authenticationMethod, sXml, appName, transactionID, questionSetID, requestAppID, requestClientID, requestSequenceID, referenceNumber, questionListPersist)
				Else
					psWalletMessage = customSubmittedSuccessMessage	'question failed ' Return the custom "Non-approved" message
					Return False ''authentication fail and there is no follow wallet questions 
				End If
			End If

			Return True


		End Function

		Public Function ExecWalletAnswersBA(ByRef psAnswerValidate As String, ByRef psWalletMessage As String, ByVal isJoint As Boolean, ByVal applicantIndex As Integer, ByVal firstName As String, ByVal walletQuestionsAndAnswers As String, ByVal hasCoFollowOnQuestions As Boolean, ByVal loanID As String, ByVal customSubmittedSuccessMessage As String, ByRef transactionID As String, ByRef questionSetID As String, ByRef requestAppID As String, ByRef requestClientID As String, ByRef requestSequenceID As String, ByRef referenceNumber As String, ByRef questionListPersist As List(Of CWalletQuestionRendering.questionDataClass), ByVal walletAnswersText As String) As Boolean
			' First, we need to get the id's of the questions and answers
			Dim questionIDList As New List(Of String)
			Dim answerIDList As New List(Of String)
			''dim get wallet question and answer
			Dim oQuestionsAndAnswers = New List(Of CXAQuestionAnswer)
			If Not String.IsNullOrEmpty(walletQuestionsAndAnswers) Then
				oQuestionsAndAnswers = JsonConvert.DeserializeObject(Of List(Of CXAQuestionAnswer))(walletQuestionsAndAnswers)
			End If
			If oQuestionsAndAnswers.Count = 0 Then
				psAnswerValidate = "Answers to Out of Wallet Questions are invalid!"
				Return True	''no wallet question and answer
			End If
			For Each oQA In oQuestionsAndAnswers
				questionIDList.Add(oQA.q)
				answerIDList.Add(oQA.a)
			Next

			' Determine which authentication method to use.
			' Each method has different URLs and different XML requests.
			Dim questionsURL As String = ""
			Dim answersURL As String = ""
			Dim authenticationMethod As String = DetermineAuthenticationType(questionsURL, answersURL)
			' The answers are valid, so we package them up in the proper XML and send it off
			Dim baBuildWalletAnswer As New CBABuildWalletAnswers(websiteConfig, isJoint, loanID, transactionID, questionIDList, applicantIndex)
			Dim walletAnswerXML As String = BuildWalletAnswerXML(baBuildWalletAnswer, answerIDList, authenticationMethod, questionSetID, requestAppID, requestClientID, requestSequenceID, referenceNumber, walletAnswersText, hasCoFollowOnQuestions)
			' Done building the XML, now ship it off
			Dim req As WebRequest = WebRequest.Create(answersURL)

			req.Method = "POST"
			req.ContentType = "text/xml"

			log.Info("ExecWalletAnswers Data: " & CSecureStringFormatter.MaskSensitiveXMLData(walletAnswerXML))

			Using writer As New StreamWriter(req.GetRequestStream())
				writer.Write(walletAnswerXML)
			End Using

			Dim res As WebResponse = req.GetResponse()
			Dim sXml As String
			Using reader As New StreamReader(res.GetResponseStream())
				sXml = reader.ReadToEnd()

				If sXml.Contains("INTERNAL_ERROR") Then
					log.Warn("ExecWalletAnswers Respsonse: " + sXml)
				Else
					log.Info("ExecWalletAnswers Response: " + sXml)
				End If
			End Using

			res.GetResponseStream().Close()
			req.GetRequestStream().Close()

			' Check to see if the server's response to the wallet answers is not an error
			If Not IsSubmitted(sXml) Then
				Return False
			End If


			' There are no errors, so check to see if PID or RSA was approved. 
			If Not HasPassedAuthentication(authenticationMethod, sXml) Then	 'execute this loop when not approved

				If HasFollowOnQuestion(authenticationMethod, sXml) Then
					psWalletMessage = GetFollowOnQuestion(authenticationMethod, sXml, firstName, transactionID, questionSetID, requestAppID, requestClientID, requestSequenceID, referenceNumber, questionListPersist)
				Else
					psWalletMessage = customSubmittedSuccessMessage	'question failed ' Return the custom "Non-approved" message
					Return False ''authentication fail and there is no follow wallet questions 
				End If
			End If
			Return True
		End Function

		Public Function ExecWalletAnswersSA(ByRef psAnswerValidate As String, ByRef psWalletMessage As String, ByVal isJoint As Boolean, ByVal applicantIndex As Integer, ByVal firstName As String, ByVal walletQuestionsAndAnswers As String, ByVal hasCoFollowOnQuestions As Boolean, ByVal loanID As String, ByVal customSubmittedSuccessMessage As String, ByRef transactionID As String, ByRef questionSetID As String, ByRef requestAppID As String, ByRef requestClientID As String, ByRef requestSequenceID As String, ByRef referenceNumber As String, ByRef questionListPersist As List(Of CWalletQuestionRendering.questionDataClass), ByVal walletAnswersText As String) As Boolean
			' First, we need to get the id's of the questions and answers
			Dim questionIDList As New List(Of String)
			Dim answerIDList As New List(Of String)
			''dim get wallet question and answer
			Dim oQuestionsAndAnswers = New List(Of CXAQuestionAnswer)
			If Not String.IsNullOrEmpty(walletQuestionsAndAnswers) Then
				oQuestionsAndAnswers = JsonConvert.DeserializeObject(Of List(Of CXAQuestionAnswer))(walletQuestionsAndAnswers)
			End If
			If oQuestionsAndAnswers.Count = 0 Then
				psAnswerValidate = "Answers to Out of Wallet Questions are invalid!"
				Return True	''no wallet question and answer
			End If
			For Each oQA In oQuestionsAndAnswers
				questionIDList.Add(oQA.q)
				answerIDList.Add(oQA.a)
			Next

			' Determine which authentication method to use.
			' Each method has different URLs and different XML requests.
			Dim questionsURL As String = ""
			Dim answersURL As String = ""
			Dim authenticationMethod As String = DetermineAuthenticationType(questionsURL, answersURL)
			' The answers are valid, so we package them up in the proper XML and send it off
			Dim saBuildWalletAnswer As New CSABuildWalletAnswers(websiteConfig, isJoint, loanID, transactionID, questionIDList, applicantIndex)
			Dim walletAnswerXML As String = BuildWalletAnswerXML(saBuildWalletAnswer, answerIDList, authenticationMethod, questionSetID, requestAppID, requestClientID, requestSequenceID, referenceNumber, walletAnswersText, hasCoFollowOnQuestions)
			' Done building the XML, now ship it off
			Dim req As WebRequest = WebRequest.Create(answersURL)

			req.Method = "POST"
			req.ContentType = "text/xml"

			log.Info("ExecWalletAnswers Data: " & CSecureStringFormatter.MaskSensitiveXMLData(walletAnswerXML))

			Using writer As New StreamWriter(req.GetRequestStream())
				writer.Write(walletAnswerXML)
			End Using

			Dim res As WebResponse = req.GetResponse()
			Dim sXml As String
			Using reader As New StreamReader(res.GetResponseStream())
				sXml = reader.ReadToEnd()
				If sXml.Contains("INTERNAL_ERROR") Then
					log.Warn("ExecWalletAnswers Respsonse: " + sXml)
				Else
					log.Info("ExecWalletAnswers Response: " + sXml)
				End If
			End Using
			res.GetResponseStream().Close()
			req.GetRequestStream().Close()

			' Check to see if the server's response to the wallet answers is not an error
			If Not IsSubmitted(sXml) Then
				Return False
			End If


			' There are no errors, so check to see if PID or RSA was approved. 
			If Not HasPassedAuthentication(authenticationMethod, sXml) Then	 'execute this loop when not approved

				If HasFollowOnQuestion(authenticationMethod, sXml) Then
					psWalletMessage = GetFollowOnQuestion(authenticationMethod, sXml, firstName, transactionID, questionSetID, requestAppID, requestClientID, requestSequenceID, referenceNumber, questionListPersist)
				Else
					psWalletMessage = customSubmittedSuccessMessage	'question failed ' Return the custom "Non-approved" message
					Return False ''authentication fail and there is no follow wallet questions 
				End If
			End If

			Return True


		End Function
		Public Function ExecWalletQuestionsBA(ByRef psMessage As String, ByRef psWalletQuestions As String, ByVal isJoint As Boolean, ByVal applicantIndex As Integer, ByVal iterationIndex As Integer, ByVal firstName As String, ByVal customSubmittedSuccessMessage As String, ByVal loanID As String, ByRef transactionID As String, ByRef questionSetID As String, ByRef requestAppID As String, ByRef requestClientID As String, ByRef requestSequenceID As String, ByRef referenceNumber As String, ByRef questionListPersist As List(Of CWalletQuestionRendering.questionDataClass)) As Boolean
			'log.Info("XA: Preparing request for Out of Wallet Questions")

			' Get the correct URL depending on which authentication method was given in the config.xml file.
			Dim questionsURL As String = ""
			Dim answersURL As String = ""
			Dim authenticationMethod As String = DetermineAuthenticationType(questionsURL, answersURL).Trim()
			Dim url As String = questionsURL

			If authenticationMethod.ToUpper() = "NONE" Then
				psMessage = customSubmittedSuccessMessage + "<div id='MLNOIDAUTHENTICATION'></div>"
				Return True
			End If
			Dim walletRequest As String = ""
			Dim baGetWalletQuestion As New CBAGetWalletQuestions(authenticationMethod, websiteConfig, isJoint, loanID, applicantIndex)
			walletRequest = baGetWalletQuestion.GetWalletRequest()
			Dim req As WebRequest = WebRequest.Create(url)
			req.Method = "POST"
			req.ContentType = "text/xml"

			log.Info("ExecWalletQuestions Request: " & CSecureStringFormatter.MaskSensitiveXMLData(walletRequest))

			Dim writer As New StreamWriter(req.GetRequestStream())
			writer.Write(walletRequest)
			writer.Close()
			log.Info("Start the request to " + url)

			Dim res As WebResponse = req.GetResponse()
			Dim reader As StreamReader = New StreamReader(res.GetResponseStream())
			Dim sXml As String = reader.ReadToEnd()
			If sXml.Contains("INTERNAL_ERROR") Then
				log.Warn("ExecWalletQuestions Response from server: " + sXml)
			Else
				log.Info("ExecWalletQuestions Response from server: " + sXml)
			End If


			reader.Close()
			res.GetResponseStream().Close()
			req.GetRequestStream().Close()

			Dim bIsFailedAuthenticate As Boolean = False
			If Not IsSubmitted(sXml, bIsFailedAuthenticate) Then
				Return False
			End If
			If bIsFailedAuthenticate Then
				psMessage = customSubmittedSuccessMessage	'case for unable to lcate the person, invalid social
				Return False
			End If

			Dim walletObject As CWalletQuestionRendering
			' Process the Out of Wallet Questions and return them so they can be displayed to the user
			walletObject = New CWalletQuestionRendering(sXml, websiteConfig)
			walletObject.setAuthenticationType(authenticationMethod)
			walletObject.populateQuestions(transactionID, questionSetID, requestAppID, requestClientID, requestSequenceID, referenceNumber)

			questionListPersist = walletObject.questionsList

			Dim walletQuestionsHTML As String
			walletQuestionsHTML = walletObject.RenderWalletQuestions(firstName, transactionID, questionSetID, requestAppID, requestClientID, requestSequenceID, referenceNumber)


			Dim numQuestionsStr = Format(walletObject.NumQuestions, "00")

			psWalletQuestions = (numQuestionsStr + walletQuestionsHTML)
			psWalletQuestions = psWalletQuestions + "<input type='hidden' id='hdWalletQuestionApplicantIndex' value='" & applicantIndex & "' />"
			psWalletQuestions = psWalletQuestions + "<input type='hidden' id='hdWalletQuestionIterationIndex' value='" & iterationIndex & "' />"
			Return True
		End Function
		Public Function ExecWalletQuestionsSA(ByRef psMessage As String, ByRef psWalletQuestions As String, isJoint As Boolean, applicantIndex As Integer, iterationIndex As Integer, firstName As String, ByVal loanID As String, ByVal customSubmittedSuccessMessage As String, ByRef transactionID As String, ByRef questionSetID As String, ByRef requestAppID As String, ByRef requestClientID As String, ByRef requestSequenceID As String, ByRef referenceNumber As String, ByRef questionListPersist As List(Of CWalletQuestionRendering.questionDataClass)) As Boolean
			'log.Info("XA: Preparing request for Out of Wallet Questions")

			' Get the correct URL depending on which authentication method was given in the config.xml file.
			Dim questionsURL As String = ""
			Dim answersURL As String = ""
			Dim authenticationMethod As String = DetermineAuthenticationType(questionsURL, answersURL).Trim()
			Dim url As String = questionsURL

			If authenticationMethod.ToUpper() = "NONE" Then
				psMessage = customSubmittedSuccessMessage + "<div id='MLNOIDAUTHENTICATION'></div>"
				Return True
			End If
			Dim walletRequest As String = ""
			Dim saGetWalletQuestion As New CSAGetWalletQuestions(authenticationMethod, websiteConfig, isJoint, loanID, applicantIndex)
			walletRequest = saGetWalletQuestion.GetWalletRequest()
			Dim req As WebRequest = WebRequest.Create(url)
			req.Method = "POST"
			req.ContentType = "text/xml"

			log.Info("ExecWalletQuestions Request: " & CSecureStringFormatter.MaskSensitiveXMLData(walletRequest))

			Dim writer As New StreamWriter(req.GetRequestStream())
			writer.Write(walletRequest)
			writer.Close()
			log.Info("Start the request to " + url)

			Dim res As WebResponse = req.GetResponse()
			Dim reader As StreamReader = New StreamReader(res.GetResponseStream())
			Dim sXml As String = reader.ReadToEnd()
			If sXml.Contains("INTERNAL_ERROR") Then
				log.Warn("ExecWalletQuestions Response from server: " + sXml)
			Else
				log.Info("ExecWalletQuestions Response from server: " + sXml)
			End If


			reader.Close()
			res.GetResponseStream().Close()
			req.GetRequestStream().Close()

			Dim bIsFailedAuthenticate As Boolean = False
			If Not IsSubmitted(sXml, bIsFailedAuthenticate) Then
				Return False
			End If
			If bIsFailedAuthenticate Then
				psMessage = customSubmittedSuccessMessage	'case for unable to lcate the person, invalid social
				Return False
			End If

			Dim walletObject As CWalletQuestionRendering
			' Process the Out of Wallet Questions and return them so they can be displayed to the user
			walletObject = New CWalletQuestionRendering(sXml, websiteConfig)
			walletObject.setAuthenticationType(authenticationMethod)
			walletObject.populateQuestions(transactionID, questionSetID, requestAppID, requestClientID, requestSequenceID, referenceNumber)

			questionListPersist = walletObject.questionsList

			Dim walletQuestionsHTML As String
			walletQuestionsHTML = walletObject.RenderWalletQuestions(firstName, transactionID, questionSetID, requestAppID, requestClientID, requestSequenceID, referenceNumber)


			Dim numQuestionsStr = Format(walletObject.NumQuestions, "00")

			psWalletQuestions = (numQuestionsStr + walletQuestionsHTML)
			psWalletQuestions = psWalletQuestions + "<input type='hidden' id='hdApplicantIndex' value='" & applicantIndex & "' />"
			psWalletQuestions = psWalletQuestions + "<input type='hidden' id='hdIterationIndex' value='" & iterationIndex & "' />"
			Return True
		End Function

		Public Function ExecWalletQuestions(ByRef psMessage As String, ByRef psWalletQuestions As String, ByVal isJoint As Boolean, ByVal firstName As String, ByVal loanID As String, ByVal customSubmittedSuccessMessage As String, ByRef transactionID As String, ByRef questionSetID As String, ByRef requestAppID As String, ByRef requestClientID As String, ByRef requestSequenceID As String, ByRef referenceNumber As String, ByRef questionListPersist As List(Of CWalletQuestionRendering.questionDataClass)) As Boolean
			'log.Info("XA: Preparing request for Out of Wallet Questions")

			' Get the correct URL depending on which authentication method was given in the config.xml file.
			Dim questionsURL As String = ""
			Dim answersURL As String = ""
			Dim authenticationMethod As String = DetermineAuthenticationType(questionsURL, answersURL).Trim
			Dim url As String = questionsURL

			Dim walletRequest As String = "RSA"
			Dim xaGetWalletQuestion As New CXAGetWalletQuestions(websiteConfig, isJoint, loanID)
			Select Case authenticationMethod.ToUpper()
				Case "RSA"
					walletRequest = xaGetWalletQuestion.GetOutOfWalletQuestionsRSA()
				Case "PID"
					walletRequest = xaGetWalletQuestion.GetOutOfWalletQuestionsPID()
				Case "FIS"
					walletRequest = xaGetWalletQuestion.GetOutOfWalletQuestionsFIS()
				Case "DID"
					walletRequest = xaGetWalletQuestion.GetOutOfWalletQuestionsDID()
					'equifax
				Case "EID"
					walletRequest = xaGetWalletQuestion.GetOutOfWalletQuestionsEquifax()
				Case "TID" ''TransUnion
					walletRequest = xaGetWalletQuestion.GetOutOfWalletQuestionsTID()
				Case "NONE"
					' If the option was set to NONE or there was no option set, then the lender does not support ID authentication
					' Then we're done.
					psMessage = customSubmittedSuccessMessage + "<div id='MLNOIDAUTHENTICATION'></div>"
					Return True
			End Select

			Dim req As WebRequest = WebRequest.Create(url)
			req.Method = "POST"
			req.ContentType = "text/xml"

			log.Info("ExecWalletQuestions Request: " & CSecureStringFormatter.MaskSensitiveXMLData(walletRequest))

			Using writer As New StreamWriter(req.GetRequestStream())
				writer.Write(walletRequest)
			End Using
			log.Info("Start the request to " + url)

			Dim res As WebResponse = req.GetResponse()
			Dim reader As StreamReader = New StreamReader(res.GetResponseStream())
			Dim sXml As String = reader.ReadToEnd()
			If sXml.Contains("INTERNAL_ERROR") Then
				log.Warn("ExecWalletQuestions Response from server: " + sXml)
			Else
				log.Info("ExecWalletQuestions Response from server: " + sXml)
			End If


			reader.Close()
			res.GetResponseStream().Close()
			req.GetRequestStream().Close()

			Dim bIsFailedAuthenticate As Boolean = False
			If Not IsSubmitted(sXml, bIsFailedAuthenticate) Then
				Return False
			End If
			If bIsFailedAuthenticate Then
				psMessage = customSubmittedSuccessMessage	'case for unable to lcate the person, invalid social
				Return False
			End If

			Dim walletObject As CWalletQuestionRendering
			' Process the Out of Wallet Questions and return them so they can be displayed to the user
			walletObject = New CWalletQuestionRendering(sXml, websiteConfig)
			walletObject.setAuthenticationType(authenticationMethod)
			walletObject.populateQuestions(transactionID, questionSetID, requestAppID, requestClientID, requestSequenceID, referenceNumber)

			questionListPersist = walletObject.questionsList

			Dim walletQuestionsHTML As String = walletObject.RenderWalletQuestions(firstName, transactionID, questionSetID, requestAppID, requestClientID, requestSequenceID, referenceNumber)

			Dim numQuestionsStr = Format(walletObject.NumQuestions, "00")

			psWalletQuestions = (numQuestionsStr + walletQuestionsHTML)
			Return True
		End Function
		Public Function PrepareCustomListRuleParams(requiredParams As Dictionary(Of String, String), applicantInfo As CBAApplicantInfo, ByVal params As Dictionary(Of String, Tuple(Of Object, String))) As Dictionary(Of String, Tuple(Of Object, String))
			If requiredParams.ContainsKey("age") Then
				params.Add("age", New Tuple(Of Object, String)(CType(Math.Floor(DateTime.Now.Subtract(applicantInfo.DOB).TotalDays / 365), Integer), "Integer"))
			End If
			If requiredParams.ContainsKey("citizenship") Then
				params.Add("citizenship", New Tuple(Of Object, String)(applicantInfo.CitizenshipStatus, "String"))
			End If
			If requiredParams.ContainsKey("employeeOfLender") Then
				params.Add("employeeOfLender", New Tuple(Of Object, String)(applicantInfo.EmployeeOfLender, "String"))
			End If
			If requiredParams.ContainsKey("employmentLength") Then
				params.Add("employmentLength", New Tuple(Of Object, String)(Common.SafeDouble(applicantInfo.EmployedDurationMonth) / 12 + Common.SafeDouble(applicantInfo.EmployedDurationYear), "Double"))
			End If
			If requiredParams.ContainsKey("employmentStatus") Then
				params.Add("employmentStatus", New Tuple(Of Object, String)(applicantInfo.EmploymentStatus, "String"))
			End If
			If requiredParams.ContainsKey("memberNumber") Then
				params.Add("memberNumber", New Tuple(Of Object, String)(applicantInfo.MemberNumber, "String"))
			End If
			If requiredParams.ContainsKey("occupancyLength") Then
				params.Add("occupancyLength", New Tuple(Of Object, String)(Common.SafeDouble(applicantInfo.OccupancyDuration / 12), "Double"))
			End If
			If requiredParams.ContainsKey("occupancyStatus") Then
				params.Add("occupancyStatus", New Tuple(Of Object, String)(applicantInfo.OccupyingLocation, "String"))
			End If

			If requiredParams.Any(Function(k) k.Key.StartsWith("AQ_")) Then
				If applicantInfo.ApplicantQuestionAnswers IsNot Nothing AndAlso applicantInfo.ApplicantQuestionAnswers.Count > 0 Then
					For Each validatedQuestionAnswer In applicantInfo.ApplicantQuestionAnswers
						Dim questionId = Regex.Replace(validatedQuestionAnswer.Question.Name, "\s", "_")
						If requiredParams.ContainsKey("AQ_" & questionId) AndAlso Not params.ContainsKey("AQ_" & questionId) Then
							Dim answerDataType = requiredParams.First(Function(p) p.Key = "AQ_" & questionId).Value
							If validatedQuestionAnswer.Answers.Count > 1 Then
								params.Add("AQ_" & questionId, New Tuple(Of Object, String)(validatedQuestionAnswer.Answers.Select(Function(q) q.AnswerValue).ToList(), answerDataType))
							ElseIf validatedQuestionAnswer.Answers.Count = 1 Then
								params.Add("AQ_" & questionId, New Tuple(Of Object, String)(validatedQuestionAnswer.Answers.First().AnswerValue, answerDataType))
							End If

						End If
					Next
				End If
			End If
			For Each pa In requiredParams.Where(Function(p) Not params.ContainsKey(p.Key))
				params.Add(pa.Key, New Tuple(Of Object, String)(Nothing, pa.Value))
			Next
			Return params
		End Function

		Public Function PrepareCustomListRuleParams(requiredParams As Dictionary(Of String, String), applicantInfo As CSAApplicantInfo, ByVal params As Dictionary(Of String, Tuple(Of Object, String))) As Dictionary(Of String, Tuple(Of Object, String))
			If requiredParams.ContainsKey("age") Then
				params.Add("age", New Tuple(Of Object, String)(CType(Math.Floor(DateTime.Now.Subtract(applicantInfo.DOB).TotalDays / 365), Integer), "Integer"))
			End If
			If requiredParams.ContainsKey("citizenship") Then
				params.Add("citizenship", New Tuple(Of Object, String)(applicantInfo.CitizenshipStatus, "String"))
			End If
			If requiredParams.ContainsKey("employeeOfLender") Then
				params.Add("employeeOfLender", New Tuple(Of Object, String)(applicantInfo.EmployeeOfLender, "String"))
			End If
			If requiredParams.ContainsKey("employmentLength") Then
				params.Add("employmentLength", New Tuple(Of Object, String)(Common.SafeDouble(applicantInfo.EmployedDurationMonth) / 12 + Common.SafeDouble(applicantInfo.EmployedDurationYear), "Double"))
			End If
			If requiredParams.ContainsKey("employmentStatus") Then
				params.Add("employmentStatus", New Tuple(Of Object, String)(applicantInfo.EmploymentStatus, "String"))
			End If
			If requiredParams.ContainsKey("memberNumber") Then
				params.Add("memberNumber", New Tuple(Of Object, String)(applicantInfo.MemberNumber, "String"))
			End If
			If requiredParams.ContainsKey("occupancyLength") Then
				params.Add("occupancyLength", New Tuple(Of Object, String)(Common.SafeDouble(applicantInfo.OccupancyDuration / 12), "Double"))
			End If
			If requiredParams.ContainsKey("occupancyStatus") Then
				params.Add("occupancyStatus", New Tuple(Of Object, String)(applicantInfo.OccupyingLocation, "String"))
			End If

			If requiredParams.Any(Function(k) k.Key.StartsWith("AQ_")) Then
				If applicantInfo.ApplicantQuestionAnswers IsNot Nothing AndAlso applicantInfo.ApplicantQuestionAnswers.Count > 0 Then
					For Each validatedQuestionAnswer In applicantInfo.ApplicantQuestionAnswers
						Dim questionId = Regex.Replace(validatedQuestionAnswer.Question.Name, "\s", "_")
						If requiredParams.ContainsKey("AQ_" & questionId) AndAlso Not params.ContainsKey("AQ_" & questionId) Then
							Dim answerDataType = requiredParams.First(Function(p) p.Key = "AQ_" & questionId).Value
							If validatedQuestionAnswer.Answers.Count > 1 Then
								params.Add("AQ_" & questionId, New Tuple(Of Object, String)(validatedQuestionAnswer.Answers.Select(Function(q) q.AnswerValue).ToList(), answerDataType))
							ElseIf validatedQuestionAnswer.Answers.Count = 1 Then
								params.Add("AQ_" & questionId, New Tuple(Of Object, String)(validatedQuestionAnswer.Answers.First().AnswerValue, answerDataType))
							End If

						End If
					Next
				End If
			End If
			For Each pa In requiredParams.Where(Function(p) Not params.ContainsKey(p.Key))
				params.Add(pa.Key, New Tuple(Of Object, String)(Nothing, pa.Value))
			Next
			Return params
		End Function

		Public Function PrepareCustomListRuleParams(requiredParams As Dictionary(Of String, String), applicantInfo As CXAPersonalInfo, addressInfo As CAddressInfo, applicantQuestionAnswers As List(Of CValidatedQuestionAnswers), ByVal params As Dictionary(Of String, Tuple(Of Object, String))) As Dictionary(Of String, Tuple(Of Object, String))
			If requiredParams.ContainsKey("age") Then
				params.Add("age", New Tuple(Of Object, String)(CType(Math.Floor(DateTime.Now.Subtract(Common.SafeDate(applicantInfo.DateOfBirth)).TotalDays / 365), Integer), "Integer"))
			End If
			If requiredParams.ContainsKey("citizenship") Then
				params.Add("citizenship", New Tuple(Of Object, String)(applicantInfo.Citizenship, "String"))
			End If
			If requiredParams.ContainsKey("employeeOfLender") Then
				params.Add("employeeOfLender", New Tuple(Of Object, String)(applicantInfo.EmployeeOfLender, "String"))
			End If
			If requiredParams.ContainsKey("employmentLength") Then
				params.Add("employmentLength", New Tuple(Of Object, String)(Common.SafeDouble(applicantInfo.txtProfessionDuration_month) / 12 + Common.SafeDouble(applicantInfo.txtProfessionDuration_year), "Double"))
			End If
			If requiredParams.ContainsKey("employmentStatus") Then
				params.Add("employmentStatus", New Tuple(Of Object, String)(applicantInfo.EmploymentStatus, "String"))
			End If
			If requiredParams.ContainsKey("memberNumber") Then
				params.Add("memberNumber", New Tuple(Of Object, String)(applicantInfo.MemberNumber, "String"))
			End If
			If requiredParams.ContainsKey("occupancyLength") Then
				params.Add("occupancyLength", New Tuple(Of Object, String)(Common.SafeDouble(addressInfo.OccupancyDuration / 12), "Double"))
			End If
			If requiredParams.ContainsKey("occupancyStatus") Then
				params.Add("occupancyStatus", New Tuple(Of Object, String)(addressInfo.OccupancyStatus, "String"))
			End If

			If requiredParams.Any(Function(k) k.Key.StartsWith("AQ_")) Then
				If applicantQuestionAnswers IsNot Nothing AndAlso applicantQuestionAnswers.Count > 0 Then
					For Each validatedQuestionAnswer In applicantQuestionAnswers
						Dim questionId = Regex.Replace(validatedQuestionAnswer.Question.Name, "\s", "_")
						If requiredParams.ContainsKey("AQ_" & questionId) AndAlso Not params.ContainsKey("AQ_" & questionId) Then
							Dim answerDataType = requiredParams.First(Function(p) p.Key = "AQ_" & questionId).Value
							If validatedQuestionAnswer.Answers.Count > 1 Then
								params.Add("AQ_" & questionId, New Tuple(Of Object, String)(validatedQuestionAnswer.Answers.Select(Function(q) q.AnswerValue).ToList(), answerDataType))
							ElseIf validatedQuestionAnswer.Answers.Count = 1 Then
								params.Add("AQ_" & questionId, New Tuple(Of Object, String)(validatedQuestionAnswer.Answers.First().AnswerValue, answerDataType))
							End If

						End If
					Next
				End If
			End If

			For Each pa In requiredParams.Where(Function(p) Not params.ContainsKey(p.Key))
				params.Add(pa.Key, New Tuple(Of Object, String)(Nothing, pa.Value))
			Next
			Return params
		End Function

		Public Function DisabledBusinessBooking(ByVal loanType As String) As Boolean
			If loanType = "1b" OrElse loanType = "2b" Then
				Return Common.getNodeAttributes(websiteConfig, "XA_LOAN/BEHAVIOR", "booking_business") = "N"
			End If
			Return False
		End Function

		Public Function DisabledSpecialBooking(ByVal loanType As String) As Boolean
			If loanType = "1s" OrElse loanType = "2s" OrElse loanType = "1a" OrElse loanType = "2a" Then
				Return Common.getNodeAttributes(websiteConfig, "XA_LOAN/BEHAVIOR", "booking_special") = "N"
			End If
			Return False
		End Function
		Public Function DisabledPrimaryBooking(ByVal loanType As String, ByVal loanRequestXmlStr As String, ByVal isSecondary As Boolean) As Boolean
			''temporary fix for MCU, LAPFCU, and BMIFCU lenderref-disable booking for secondary
			''TODO: remove this and use customlist
			If (loanType = "2" OrElse loanType = "2s" OrElse loanType = "2b") Then
				If websiteConfig.LenderRef.ToUpper.StartsWith("BMIFCU") Or websiteConfig.LenderRef.ToUpper.StartsWith("LAPFCU") Then
					Return True
				ElseIf websiteConfig.LenderRef.ToUpper.StartsWith("MCU") Then
					Dim oLoanRequestXmlDoc As New XmlDocument
					Dim oInnerRequestXmlDoc As New XmlDocument
					Dim sMemberNumber As String = ""
					oLoanRequestXmlDoc.LoadXml(loanRequestXmlStr)
					oInnerRequestXmlDoc.LoadXml(oLoanRequestXmlDoc.InnerText)
					Dim oApplicantNodes As XmlNodeList = oInnerRequestXmlDoc.GetElementsByTagName("APPLICANT")
					If oApplicantNodes(0).Attributes("member_number") IsNot Nothing Then
						sMemberNumber = Common.SafeString(oApplicantNodes(0).Attributes("member_number").InnerXml) ''get primary member number
					End If
					If String.IsNullOrWhiteSpace(sMemberNumber) Then Return True ''disable booking if the app does not have member number.
				End If
			End If
			''get disable_primary_booking
			If isSecondary Then
				Return False
			Else
				Return Common.getNodeAttributes(websiteConfig, "XA_LOAN/BEHAVIOR", "disable_primary_booking") = "Y"
			End If
		End Function
		Public Sub UpdateLoan(ByVal pbIsFunded As Boolean, ByVal loanID As String, ByVal loanRequestXMLStr As String, ByVal paymentID As String, Optional ByVal psPaypalErrorMessage As String = "")
			Dim getLoansXMLRequestStr As String = LoanRetrieval.BuildGetLoansXML(loanID, websiteConfig.APIUser, websiteConfig.APIPassword)

			'' Done building the XML, now ship it off
			Dim req As WebRequest = WebRequest.Create(GetLoansURL)
			req.Method = "POST"
			req.ContentType = "text/xml"
			log.Info("XA: Preparing to POST GetLoans Request: " & CSecureStringFormatter.MaskSensitiveXMLData(getLoansXMLRequestStr))
			Dim writer As New StreamWriter(req.GetRequestStream())
			writer.Write(getLoansXMLRequestStr)
			writer.Close()

			Dim res As WebResponse = req.GetResponse()
			Dim reader As New StreamReader(res.GetResponseStream())
			Dim sXml As String = reader.ReadToEnd()

			log.Info("XA: GetLoans Response: " + CSecureStringFormatter.MaskSensitiveXMLData(sXml))

			' Extract the inner xml and update the funding status and internal comment
			Dim responseXMLDoc As XmlDocument = New XmlDocument()
			responseXMLDoc.LoadXml(sXml)
			Dim innerXMLStr As String = ""
			innerXMLStr = responseXMLDoc.InnerText
			Dim innerXMLDoc As XmlDocument = New XmlDocument()
			innerXMLDoc.LoadXml(innerXMLStr)

			' Grab the outer xml to use for template in the next request
			Dim origRequestXMLDoc As XmlDocument = New XmlDocument()
			origRequestXMLDoc.LoadXml(loanRequestXMLStr)


			'add funding source
			Dim oFundingSources As XmlElement = innerXMLDoc.GetElementsByTagName("FUNDING_SOURCES")(0)
			Dim oFundingSource As XmlElement = CType(oFundingSources.OwnerDocument.CreateElement("FUNDING_SOURCE", Common.NAMESPACE_URI), XmlElement)
			oFundingSources.AppendChild(oFundingSource)
			oFundingSource.SetAttribute("clf_funding_source_id", "3")
			oFundingSource.SetAttribute("funding_type", "PAYPAL")
			oFundingSource.SetAttribute("paypal_paykey", paymentId)

			'********
			' FUNDED
			If pbIsFunded Then
				'change status back
				Dim loanInfoNodes As XmlNodeList = innerXMLDoc.GetElementsByTagName("LOAN_INFO")
				Dim loanInfoNode As XmlElement = loanInfoNodes(0)
				loanInfoNode.SetAttribute("status", websiteConfig.LoanStatusEnum)

				''''update funding status only for  approved products
				Dim ApprovedProductList As XmlNodeList
				ApprovedProductList = innerXMLDoc.GetElementsByTagName("ACCOUNT_TYPE")
				Dim ApprovedProduct As XmlElement
				For Each childNode As XmlNode In ApprovedProductList
					ApprovedProduct = CType(childNode, XmlElement)
					If ApprovedProduct.ParentNode.Name <> "APPROVED_ACCOUNTS" Then Continue For ' only do this for approved account
					'need to set bogus fund status for Other account of interest in APPROVED_ACCOUNT node so it will not impact the overall funding status.
					If ApprovedProduct.GetAttribute("account_type") = "OTHER" And Common.SafeString(ApprovedProduct.GetAttribute("amount_deposit") = "0") Then
						ApprovedProduct.SetAttribute("funded_status", "FUN")
						ApprovedProduct.SetAttribute("funding_date", Now.ToString("yyyy-MM-dd"))
						Continue For
					End If

					If Common.SafeDouble(ApprovedProduct.GetAttribute("amount_deposit")) > 0 Then
						ApprovedProduct.SetAttribute("funded_status", "FUN")
						ApprovedProduct.SetAttribute("funding_date", Now.ToString("yyyy-MM-dd"))
						ApprovedProduct.SetAttribute("clf_funding_source_id", "3")	' have to define Paypal id as 3 too
					End If
				Next

				'membership fee
				Dim oMembershipFees As XmlNodeList = innerXMLDoc.GetElementsByTagName("MEMBERSHIP_FEE")
				For Each childNode As XmlNode In oMembershipFees
					Dim xmlMembershipFee = CType(childNode, XmlElement)
					If Common.SafeDouble(xmlMembershipFee.GetAttribute("membership_fee_funding_amount")) > 0 Then
						xmlMembershipFee.SetAttribute("membership_fee_funding_status", "FUN")	 'unable to fund
						xmlMembershipFee.SetAttribute("membership_fee_funding_date", Now.ToString("yyyy-MM-dd"))
						xmlMembershipFee.SetAttribute("membership_fee_funding_source_id", "3")	' have to define Paypal id as 3 too
					End If
				Next

				'add internal comment
				Dim oInternalComments As XmlElement = innerXMLDoc.GetElementsByTagName("INTERNAL_COMMENTS")(0)
				oInternalComments.InnerText = Environment.NewLine + Environment.NewLine + oInternalComments.InnerText
				oInternalComments.InnerText = "SYSTEM (" + DateTime.Now + "): STATUS REVERTED BACK TO " + websiteConfig.LoanStatusEnum + " AFTER APPLICANT COMPLETED PAYPAL" + oInternalComments.InnerText
			End If

			'********************* 
			' NOT FUNDED/CANCELLED
			If Not pbIsFunded And psPaypalErrorMessage = "PENDING" Then
				'---Update status PENDING so officer know the system is waiting for consumer to login to PAYPAL to pay
				Dim loanInfoNodes As XmlNodeList = innerXMLDoc.GetElementsByTagName("LOAN_INFO")
				Dim loanInfoNode As XmlElement = loanInfoNodes(0)
				loanInfoNode.SetAttribute("status", "PENDING")

				'add internal comment
				Dim oInternalComments As XmlElement = innerXMLDoc.GetElementsByTagName("INTERNAL_COMMENTS")(0)

				oInternalComments.InnerText = Environment.NewLine + Environment.NewLine + oInternalComments.InnerText
				oInternalComments.InnerText = "SYSTEM (" + DateTime.Now + "): " + "STATUS CHANGED TO PENDING WHILE WAITING FOR APPLICANT TO LOGIN TO PAYPAL TO COMPLETE PAYMENT" + oInternalComments.InnerText ' 

			ElseIf Not pbIsFunded Then

				'---Update status due to failed funding
				Dim loanInfoNodes As XmlNodeList = innerXMLDoc.GetElementsByTagName("LOAN_INFO")
				Dim loanInfoNode As XmlElement = loanInfoNodes(0)
				loanInfoNode.SetAttribute("status", "REFERRED")


				''''update funding status for products
				Dim ApprovedProductList As XmlNodeList
				ApprovedProductList = innerXMLDoc.GetElementsByTagName("ACCOUNT_TYPE")
				Dim ApprovedProduct As XmlElement
				For Each childNode As XmlNode In ApprovedProductList
					ApprovedProduct = CType(childNode, XmlElement)
					If ApprovedProduct.ParentNode.Name <> "APPROVED_ACCOUNTS" Then Continue For ' only do this for approved account
					If Common.SafeDouble(ApprovedProduct.GetAttribute("amount_deposit")) > 0 Then
						ApprovedProduct.SetAttribute("funded_status", "UF")	 'unable to fund
						ApprovedProduct.SetAttribute("funding_date", Now.ToString("yyyy-MM-dd"))
						ApprovedProduct.SetAttribute("clf_funding_source_id", "3")	' have to define Paypal id as 3 too
					End If
				Next

				'membership fee
				Dim oMembershipFees As XmlNodeList = innerXMLDoc.GetElementsByTagName("MEMBERSHIP_FEE")
				For Each childNode As XmlNode In oMembershipFees
					Dim xmlMembershipFee = CType(childNode, XmlElement)
					If Common.SafeDouble(xmlMembershipFee.GetAttribute("membership_fee_funding_amount")) > 0 Then
						xmlMembershipFee.SetAttribute("membership_fee_funding_status", "UF")	 'unable to fund
						xmlMembershipFee.SetAttribute("membership_fee_funding_date", Now.ToString("yyyy-MM-dd"))
						xmlMembershipFee.SetAttribute("membership_fee_funding_source_id", "3")	' have to define Paypal id as 3 too
					End If
				Next

				'add internal comment
				Dim oInternalComments As XmlElement = innerXMLDoc.GetElementsByTagName("INTERNAL_COMMENTS")(0)

				oInternalComments.InnerText = Environment.NewLine + Environment.NewLine + oInternalComments.InnerText
				If psPaypalErrorMessage <> "" Then
					oInternalComments.InnerText = "SYSTEM (" + DateTime.Now + "): " + psPaypalErrorMessage + oInternalComments.InnerText ' paypal error
				Else
					oInternalComments.InnerText = "CONSUMER (" + DateTime.Now + "): PAYPAL FUNDING HAS BEEN CANCELLED" + oInternalComments.InnerText 'consumer canccel
				End If

				oInternalComments.InnerText = Environment.NewLine + Environment.NewLine + oInternalComments.InnerText
				oInternalComments.InnerText = "SYSTEM (" + DateTime.Now + "): " + "STATUS CHANGED TO PENDING BECAUSE FUNDING FAILED" + oInternalComments.InnerText ' changing status

			End If



			'put the the getloan cdata in request wrapper
			Dim cdataSection As XmlCDataSection
			origRequestXMLDoc.SelectSingleNode("//LOAN_DATA").RemoveAll()
			cdataSection = origRequestXMLDoc.CreateCDataSection(innerXMLDoc.InnerXml)
			origRequestXMLDoc.SelectSingleNode("//LOAN_DATA").AppendChild(cdataSection)

			' Add the loan_id to LOAN_DATA
			Dim loanIDAttributeElement As XmlElement = CType(origRequestXMLDoc.SelectSingleNode("//REQUEST"), XmlElement)
			'loanIDAttributeElement.RemoveAllAttributes()
			loanIDAttributeElement.SetAttribute("loan_id", loanID)

			Dim loanDataElement As XmlElement = CType(origRequestXMLDoc.SelectSingleNode("//LOAN_DATA"), XmlElement)
			loanDataElement.SetAttribute("loan_type", "XA")
			loanDataElement = CType(origRequestXMLDoc.SelectSingleNode("//REQUEST"), XmlElement)
			loanDataElement.SetAttribute("action_type", "UPDATE")
			log.Info("XA: Preparing to Update loan data: " & CSecureStringFormatter.MaskSensitiveXMLData(origRequestXMLDoc.InnerXml))

			' Now send the loan request back with updated funding info
			req = WebRequest.Create(LoanSubmitURL)
			req.Method = "POST"
			req.ContentType = "text/xml"
			writer = New StreamWriter(req.GetRequestStream())
			writer.Write(origRequestXMLDoc.InnerXml)
			writer.Close()

			res = req.GetResponse()

			reader = New StreamReader(res.GetResponseStream())
			sXml = reader.ReadToEnd()

			log.Info("XA: Update loan response: " + sXml)

		End Sub

		Public Sub UpdateLoan(ByVal psLoanStatus As String, state As XaState, ByVal xaAppInfo As XaSubmission, ByRef isFraud As Boolean)
			' Make changes to it
			' Submit the changes with the "UPDATE" command
			If state.LoanID = "" Then Exit Sub
			Dim getLoansXmlRequestStr As String = LoanRetrieval.BuildGetLoansXML(state.LoanID, websiteConfig.APIUser, websiteConfig.APIPassword)

			'    ' Done building the XML, now ship it off
			Dim req As WebRequest = WebRequest.Create(GetLoansURL)

			req.Method = "POST"
			req.ContentType = "text/xml"

			log.Info("XA: Preparing to POST GetLoans Request: " & CSecureStringFormatter.MaskSensitiveXMLData(getLoansXmlRequestStr))

			Dim writer As New StreamWriter(req.GetRequestStream())
			writer.Write(getLoansXmlRequestStr)
			writer.Close()

			Dim res As WebResponse = req.GetResponse()

			Dim reader As New StreamReader(res.GetResponseStream())
			Dim sXml As String = reader.ReadToEnd()

			log.Info("XA: GetLoans Response: " + CSecureStringFormatter.MaskSensitiveXMLData(sXml))

			' Extract the inner xml and update the status of the loans as "APPROVED"
			Dim responseXMLDoc As XmlDocument = New XmlDocument()
			responseXMLDoc.LoadXml(sXml)
			Dim innerXMLDoc As XmlDocument = New XmlDocument()
			innerXMLDoc.LoadXml(responseXMLDoc.InnerText)

			' Grab all the interested accounts from the origianl request
			Dim origRequestXMLDoc As XmlDocument = New XmlDocument()
			origRequestXMLDoc.LoadXml(state.LoanRequestXMLStr)
			Dim origInnerXMLDoc As XmlDocument = New XmlDocument()
			origInnerXMLDoc.LoadXml(origRequestXMLDoc.InnerText)
			Dim xmlFundingSources As XmlNodeList = origInnerXMLDoc.GetElementsByTagName("FUNDING_SOURCE")
			Dim sFundingSoure As String = ""
			If xmlFundingSources IsNot Nothing AndAlso xmlFundingSources.Count > 0 Then
				sFundingSoure = CType(xmlFundingSources(0), XmlElement).GetAttribute("clf_funding_source_id")
			End If

			'---Update status
			Dim loanInfoNodes As XmlNodeList = innerXMLDoc.GetElementsByTagName("LOAN_INFO")
			Dim loanInfoNode As XmlElement = loanInfoNodes(0)
			If "FRAUD" = loanInfoNode.Attributes("status").InnerXml Or "DUP" = loanInfoNode.Attributes("status").InnerXml Then
				isFraud = True
				log.Info("Skip loan status update b/c credit report has fraud warning or status is fraud or DUP")
				Exit Sub ' don't update status when credit report has fraud warning or status is fraud or DUP
			End If
			loanInfoNode.SetAttribute("status", psLoanStatus)
			'' set the approval/denial date to current date
			If psLoanStatus = "AA" Or psLoanStatus = "APP" Or psLoanStatus = "APPROVED" Or psLoanStatus = "INSTANTAPPROVED" Then ''set approval date if loanStatus=APPROVED(APP) or INSTANTAPPROVED(AA)
				loanInfoNode.SetAttribute("approval_date", Common.SafeDateTimeXml(Now.ToString()))
			End If
			'Update Internnal comment
			If state.InternalCommentAppend <> "" Then
				Dim internCommentNodes As XmlNodeList = innerXMLDoc.GetElementsByTagName("INTERNAL_COMMENTS")
				Dim internCommentNode As XmlElement = internCommentNodes(0)
				state.InternalCommentAppend = "SYSTEM (" + DateTime.Now + "): " & state.InternalCommentAppend & Environment.NewLine & Environment.NewLine
				internCommentNode.InnerText = state.InternalCommentAppend & internCommentNode.InnerText
				'log.Debug("INTERNAL COMMENTS: " & internCommentNode.InnerText)
			End If

			''restore the funding infor after decisionXA there are no qualified products     
			If websiteConfig.IsDecisionXAEnable AndAlso state.IsUpdateReferralStatusRequired Then
				RestoreFundingInfoWhenDecisionXAFailed(innerXMLDoc, xaAppInfo)
			End If
			''Update Approved account
			''''update Date & funding source(because decisionXA didn't copy funding source for approved account)
			Dim approvedNodeList = innerXMLDoc.GetElementsByTagName("ACCOUNT_TYPE")
			Dim productList As List(Of CProduct) = CProduct.GetProducts(websiteConfig, xaAppInfo.Availability)
			Dim DateElement As XmlElement
			For Each childNode As XmlNode In approvedNodeList
				If childNode.ParentNode.Name <> "APPROVED_ACCOUNTS" Then Continue For
				DateElement = childNode
				DateElement.SetAttribute("issue_date", Common.SafeDateXml(Now.ToString()))
				Dim sProductCode = CType(childNode, XmlElement).GetAttribute("product_code")
				Dim currProducts = productList.Where(Function(x) sProductCode = x.ProductCode).ToList
				If currProducts.Count > 0 AndAlso currProducts(0).cannotBeFunded = "Y" Then
					DateElement.SetAttribute("clf_funding_source_id", "") ''don't set funding source for cannot be funded product
				Else
					If DateElement.GetAttribute("clf_funding_source_id") = "" And sFundingSoure <> "" Then
						DateElement.SetAttribute("clf_funding_source_id", sFundingSoure)
					End If
				End If
			Next

			'fix missing clf_funding_source_id in FUNDING_SOURCE after getloan -
			Dim fundingSourceNodeList = innerXMLDoc.GetElementsByTagName("FUNDING_SOURCE")
			For Each childNode As XmlNode In fundingSourceNodeList
				Dim oXmnlFundingSource = CType(childNode, XmlElement)
				If oXmnlFundingSource.GetAttribute("clf_funding_source_id") = "" And sFundingSoure <> "" Then
					oXmnlFundingSource.SetAttribute("clf_funding_source_id", sFundingSoure)
				End If
			Next

			''fix missing membership_fee_funding_source_id in MEMBERSHIP_FEE after getloan
			Dim membershipFeeNodeList As XmlNodeList = innerXMLDoc.GetElementsByTagName("MEMBERSHIP_FEE")
			For Each childNode As XmlNode In membershipFeeNodeList
				Dim oXmlMembershipFee = CType(childNode, XmlElement)
				If oXmlMembershipFee.GetAttribute("membership_fee_funding_source_id") = "" And sFundingSoure <> "" Then
					oXmlMembershipFee.SetAttribute("membership_fee_funding_source_id", sFundingSoure)
				End If
			Next

			Dim cdataSection As XmlCDataSection
			origRequestXMLDoc.SelectSingleNode("//LOAN_DATA").RemoveAll()
			cdataSection = origRequestXMLDoc.CreateCDataSection(innerXMLDoc.InnerXml)
			origRequestXMLDoc.SelectSingleNode("//LOAN_DATA").AppendChild(cdataSection)

			' Add the loan_id to LOAN_DATA
			Dim loanIDAttributeElement As XmlElement = CType(origRequestXMLDoc.SelectSingleNode("//REQUEST"), XmlElement)
			'loanIDAttributeElement.RemoveAllAttributes()
			loanIDAttributeElement.SetAttribute("loan_id", state.LoanID)

			'responseXMLDoc.InnerText = innerXMLDoc.InnerXml
			' Change LOAN_INFO status="UPDATE"
			' For some reason, selectsinglenode doesn't work on the LOAN_INFO element, so I have to manually get it

			'' Set the version of the CLF
			'Dim versionNode As XmlNode = innerXMLDoc.DocumentElement
			'Dim versionElement As XmlElement = versionNode
			'versionElement.SetAttribute("version", "5.069")

			' Change LOAN_DATA loan_type="XA" and REQUEST action_type="UPDATE"

			Dim loanDataElement As XmlElement = CType(origRequestXMLDoc.SelectSingleNode("//LOAN_DATA"), XmlElement)
			loanDataElement.SetAttribute("loan_type", "XA")
			loanDataElement = CType(origRequestXMLDoc.SelectSingleNode("//REQUEST"), XmlElement)
			loanDataElement.SetAttribute("action_type", "UPDATE")
			log.Info("XA: Preparing to Update loan data: " + CSecureStringFormatter.MaskSensitiveXMLData(origRequestXMLDoc.InnerXml))

			' Now send the loan request back with the updated "APPROVED_ACCOUNTS"

			req = WebRequest.Create(LoanSubmitURL)
			req.Method = "POST"
			req.ContentType = "text/xml"
			writer = New StreamWriter(req.GetRequestStream())
			writer.Write(origRequestXMLDoc.InnerXml)
			writer.Close()

			res = req.GetResponse()

			reader = New StreamReader(res.GetResponseStream())
			sXml = reader.ReadToEnd()

			log.Info("XA: Update loan response: " + sXml)
			If Not IsSubmitted(sXml) Then
				log.Warn("XA: Update loan error - " + sXml)
			End If

		End Sub
		Public Function ExecWalletAnswersMinor(ByRef psAnswerValidate As String, ByRef psWalletMessage As String, ByVal sApplicantIndex As String, ByVal isJoint As Boolean, ByVal sAuthenticationType As String, ByVal xaAppInfo As XaSubmission, ByVal walletQuestionsAndAnswers As String, ByVal walletAnswersText As String, ByVal state As XaState) As Boolean
			' First, we need to get the id's of the questions and answers
			Dim questionIDList As New List(Of String)
			Dim answerIDList As New List(Of String)
			''dim get wallet question and answer
			Dim oQuestionsAndAnswers = New List(Of CXAQuestionAnswer)
			If Not String.IsNullOrEmpty(walletQuestionsAndAnswers) Then
				oQuestionsAndAnswers = JsonConvert.DeserializeObject(Of List(Of CXAQuestionAnswer))(walletQuestionsAndAnswers)
			End If
			If oQuestionsAndAnswers.Count = 0 Then
				psAnswerValidate = "Answers to Out of Wallet Questions are invalid!"
				Return True	''no wallet question and answer
			End If
			For Each oQA In oQuestionsAndAnswers
				questionIDList.Add(oQA.q)
				answerIDList.Add(oQA.a)
			Next
			' Determine which authentication method to use.
			' Each method has different URLs and different XML requests.
			Dim questionsURL As String = ""
			Dim answersURL As String = ""
			Dim authenticationMethod As String = DetermineAuthenticationType(questionsURL, answersURL, True, sAuthenticationType)
			Dim url As String = answersURL
			Dim oMinorWalletAnswer = New CMinorWalletAnswers()
			''get answer text
			Dim answerTextList As New List(Of String)

			If Not String.IsNullOrEmpty(walletAnswersText) Then
				answerTextList = JsonConvert.DeserializeObject(Of List(Of String))(walletAnswersText)
			End If
			' The answers are valid, so we package them up in the proper XML and send it off
			Dim walletAnswerXML As String = oMinorWalletAnswer.buildWalletAnswerXML(questionIDList, answerIDList, answerTextList, authenticationMethod, state.LoanID, websiteConfig, sApplicantIndex, isJoint, state.QuestionSetID, state.TransactionID, state.RequestAppID, state.RequestClientID, state.RequestSequenceID, state.ReferenceNumber)
			' Done building the XML, now ship it off
			Dim req As WebRequest = WebRequest.Create(url)
			req.Method = "POST"
			req.ContentType = "text/xml"
			Dim strMinor As String = "(MINOR)"
			If sApplicantIndex = "1" Then
				If isJoint Then
					strMinor = "(CUSTODIAN JOINT)"
				Else
					strMinor = "(CUSTODIAN)"
				End If
			End If
			log.Info("XA" & strMinor & ": Preparing to POST wallet answer Data: " & CSecureStringFormatter.MaskSensitiveXMLData(walletAnswerXML))
			Dim writer As New StreamWriter(req.GetRequestStream())
			writer.Write(walletAnswerXML)
			writer.Close()
			Dim res As WebResponse = req.GetResponse()
			Dim reader As New StreamReader(res.GetResponseStream())
			Dim sXml As String = reader.ReadToEnd()
			log.Info("XA" & strMinor & ": Response from server: " + sXml)
			reader.Close()
			res.GetResponseStream().Close()
			req.GetRequestStream().Close()
			' Check to see if the server's response to the wallet answers is not an error
			If Not IsSubmitted(sXml) Then
				Return False
			End If
			' There are no errors, so check to see if PID or RSA was approved. 
			If Not HasPassedAuthentication(authenticationMethod, sXml) Then	 'execute this loop when not approved
				If HasFollowOnQuestion(authenticationMethod, sXml) Then	 ''only for DID
					psWalletMessage = GetFollowOnQuestionMinor(authenticationMethod, sXml, sApplicantIndex, isJoint, state, xaAppInfo)
				Else
					psWalletMessage = state.CustomSubmittedSuccessMessage	'question failed ' Return the custom "Non-approved" message
					Return False ''authentication fail and there is no follow wallet questions 
				End If
			End If
			Return True
		End Function
		Public Sub New(websiteConfig As CWebsiteConfig)
			Me.websiteConfig = websiteConfig
		End Sub
		Public Function SetUnderwriteMinor(ByVal sMinorAccountCode As String) As CMinorAccount
			Dim currMinorAccount As New CMinorAccount()
			Dim minorAccountList As List(Of CMinorAccount) = Common.ParsedMinorAccountType(websiteConfig)
			For Each mAccount In minorAccountList
				If sMinorAccountCode = mAccount.minorAccountCode Then
					currMinorAccount.minorIDAEnable = mAccount.minorIDAEnable
					currMinorAccount.minorDebitEnable = mAccount.minorDebitEnable
					currMinorAccount.decisionXAEnable = mAccount.decisionXAEnable
					currMinorAccount.creditPull = mAccount.creditPull
					currMinorAccount.authenticationType = mAccount.authenticationType
					currMinorAccount.debitType = mAccount.debitType
					Return currMinorAccount
				End If
			Next
			Return currMinorAccount
		End Function
		Public Function GetLoanID(ByVal responseStr As String) As String
			Dim myDocument As New XmlDocument
			myDocument.LoadXml(responseStr)

			Dim tempNodes As XmlNodeList
			Dim loanID As String = ""
			Dim loanIDXMLStr As String
			tempNodes = myDocument.GetElementsByTagName("RESPONSE")
			For Each childNode As XmlNode In tempNodes
				loanIDXMLStr = childNode.Attributes("loan_id").InnerXml
				If (Not String.IsNullOrEmpty(loanIDXMLStr)) Then
					loanID = loanIDXMLStr
					Exit For
				End If
			Next
			Return loanID
		End Function

		Public Sub UploadDocuments(loanNumber As String, submissionData As XaSubmission)
			If Not String.IsNullOrEmpty(loanNumber) Then
				''execute uploadDocument 
				Try
					If ExecuteUpLoadDocument(submissionData, loanNumber) Then
						log.Info("XA: upload document sucessfull")
					End If
				Catch ex As Exception
					log.Error("XA: upload document failed:", ex)
				End Try
			End If
		End Sub
		Public Sub BuildSuccessMessages(state As XaState, isSecondary As Boolean, availability As String, httpServerUtility As HttpServerUtility)
			Dim requestXML As XmlDocument = New XmlDocument()
			requestXML.LoadXml(state.LoanRequestXMLStr)
			Dim loanXML As XmlDocument = New XmlDocument()
			loanXML.LoadXml(requestXML.InnerText)
			state.CustomSubmittedSuccessMessage = Common.buildNonApprovedMessage(state.CustomSubmittedSuccessMessage, state.CustomSubmittedDataList, loanXML, state.LoanResponseXMLStr)
			state.CustomSubmittedSuccessMessage = Common.PrependIconForNonApprovedMessage(state.CustomSubmittedSuccessMessage, "Thank You")
			state.CustomPreapprovedSuccessMessage = Common.buildNonApprovedMessage(state.CustomPreapprovedSuccessMessage, state.CustomPreapprovedDataList, loanXML, state.LoanResponseXMLStr)
			state.CustomPreapprovedSuccessMessage = Common.PrependIconForNonApprovedMessage(state.CustomPreapprovedSuccessMessage, "Congratulations")
			If isSecondary Then
				state.CustomPreapprovedSuccessMessage = CCustomMessage.getComboCustomResponseMessages(websiteConfig, "APPROVED_SECONDARY_MESSAGE", state.LoanResponseXMLStr, state.LoanRequestXMLStr, "", availability)
			End If
			''decode Message before rendering
			state.CustomSubmittedSuccessMessage = httpServerUtility.HtmlDecode(state.CustomSubmittedSuccessMessage)
			state.CustomPreapprovedSuccessMessage = httpServerUtility.HtmlDecode(state.CustomPreapprovedSuccessMessage)
		End Sub
		Public Function IsSubmitted(ByVal responseXml As String, Optional ByRef pIsFailed As Boolean = False) As Boolean
			Dim xmlDoc As New XmlDocument()

			xmlDoc.LoadXml(responseXml)
			'check the response is nothing
			If (xmlDoc.GetElementsByTagName("RESPONSE").Count = 0) Then
				pIsFailed = True
			End If
			If xmlDoc.GetElementsByTagName("ERROR").Count > 0 Then
				If Not xmlDoc.GetElementsByTagName("ERROR")(0).InnerText.ToUpper.Contains("EXPIRED") Then 'expired  pw
					Return False
				End If

				If Not xmlDoc.GetElementsByTagName("ERROR")(0).InnerText.ToUpper.Contains("TIME OUT") Then 'TIME OUT
					Return False
				End If

				Dim xmlError As XmlElement = CType(xmlDoc.GetElementsByTagName("ERROR")(0), XmlElement)
				If Not xmlError.GetAttribute("loan_number") <> "" Then	'fail schema validation and NO loan number
					Return False
				End If
			End If

			If xmlDoc.GetElementsByTagName("FAILED").Count > 0 Then
				Return False
			End If

			If responseXml.Contains("faileddetails") OrElse responseXml.Contains("errordetails") Then 'Failed: Individual Not Located, invalid ssn
				pIsFailed = True
				Return True
			End If

			'for preciseID, RAS,Equifax EID
			If xmlDoc.GetElementsByTagName("QUESTIONS").Count = 0 Then	'no question is found
				pIsFailed = True   '
				Return True
			End If
			Return True
		End Function
		Public Function IsIDALPQConfig(ByVal settingProductsList As List(Of CProduct), ByVal isSecondary As Boolean) As Boolean
			If settingProductsList IsNot Nothing AndAlso settingProductsList.Count > 0 Then
				For idaIndex As Integer = 0 To settingProductsList.Count - 1
					If (settingProductsList(idaIndex) IsNot Nothing AndAlso settingProductsList(idaIndex).AutoPullIDAuthenticationConsumerPrimary And Not isSecondary) Or
					(settingProductsList(idaIndex) IsNot Nothing AndAlso settingProductsList(idaIndex).AutoPullIDAuthenticationConsumerSecondary And isSecondary) Then
						Return True
					End If
				Next
			End If
			Return False
		End Function

		Public Function ExecuteIDAMinor(ByVal sMinorAuthenticationType As String, ByVal bIDA_LPQConfig As Boolean, ByVal sApplicantIndex As String, ByVal isJoint As Boolean, ByVal xaAppInfo As XaSubmission, state As XaState, Optional ByVal isOnlyMinorApp As Boolean = False) As String
			Dim sLPQContinueIDAReferral = Common.getNodeAttributes(websiteConfig, "XA_LOAN/BEHAVIOR", "continue_on_id_authentication_referral") = "Y"
			If bIDA_LPQConfig And Not String.IsNullOrEmpty(sMinorAuthenticationType) Then
				'ida authentication
				Dim sWalletQuestions As String = ""
				Dim bExecWalletQuestions As Boolean = ExecWalletQuestionsMinor(sWalletQuestions, sApplicantIndex, isJoint, sMinorAuthenticationType, state.LoanID, state.TransactionID, state.QuestionSetID, state.RequestAppID, state.RequestClientID, state.RequestSequenceID, state.ReferenceNumber, state.WalletQuestionList, xaAppInfo, state.LoanResponseXMLStr)	'check for attribute "ida" in LPQMobileWebsite config
				If Not bExecWalletQuestions Then
					'Response.Write(customErrorMessage) 'submit result with error so stop and return error msg
					'don't want to sent the error message to consumer to avoid mulitple submission
					state.InternalCommentAppend += "IDA not successfully run.  "
					If sLPQContinueIDAReferral Then
						state.IsFailedIDA = True
					Else
						Return state.CustomSubmittedSuccessMessage
					End If
				End If
				If sWalletQuestions <> "" Then 'stop and return wallet question
					Dim strWalletAnswer As String = "MINOR"
					If sApplicantIndex = "1" Then
						If isJoint Then
							strWalletAnswer = "CUSTODIAN_JOINT"
						Else
							strWalletAnswer = "CUSTODIAN"
						End If
					End If
					Dim sHiddenIsOnlyMinorApp As String = ""
					If isOnlyMinorApp Then
						sHiddenIsOnlyMinorApp = "<input type='hidden' id='hdIsOnlyMinorApp' value ='Y'/>"
					End If
					state.IsUpdateReferralStatusRequired = False 'don't to update loanstatus before user answer authenticate question
					state.ShouldKeepTheSessionDataPersistent = True
					Return sWalletQuestions + "<input type='hidden' id='hdMinorWalletAnswer' value ='" + strWalletAnswer + "'/>" + sHiddenIsOnlyMinorApp
				End If

			End If
			Return ""
		End Function

		Public Function IsDebitLPQConfig(ByVal settingProductsList As List(Of CProduct), ByVal isSecondary As Boolean) As Boolean
			If settingProductsList IsNot Nothing AndAlso settingProductsList.Count > 0 Then
				For dIndex As Integer = 0 To settingProductsList.Count - 1
					If (settingProductsList(dIndex) IsNot Nothing AndAlso settingProductsList(dIndex).AutoPullDebitConsumerPrimary And Not isSecondary) Or
					(settingProductsList(dIndex) IsNot Nothing AndAlso settingProductsList(dIndex).AutoPullDebitConsumerSecondary And isSecondary) Then
						Return True
					End If
				Next
			End If
			Return False
		End Function

		Public Function ExecuteCreditPull(ByVal loanID As String, ByVal availability As String, Optional ByVal isMinor As Boolean = False) As Boolean
			If Not isMinor AndAlso Common.SafeStringNoTrim(websiteConfig.CreditPull).ToUpper <> "Y" Then Return True 'for minor, CreditPull not setup for this lender
			Dim creditPullXMLRequest As String = BuildCreditPullXMLRequest(loanID)
			''add parameter pullOnlyIfRequired=Y to creditpullURL when the app is BXA or SA
			Dim req As WebRequest = WebRequest.Create(CreditPullURL & If(availability = "1s" OrElse availability = "2s" OrElse availability = "1b" OrElse availability = "2b", "?pullOnlyIfRequired=Y", ""))

			req.Method = "POST"
			req.ContentType = "text/xml"

			log.Info("XA: Preparing to POST credit pull Data: " & CSecureStringFormatter.MaskSensitiveXMLData(creditPullXMLRequest))

			Using writer As New StreamWriter(req.GetRequestStream())
				writer.Write(creditPullXMLRequest)
			End Using

			Dim res As WebResponse = req.GetResponse()

			Dim reader As New StreamReader(res.GetResponseStream())
			Dim sXml As String = reader.ReadToEnd()

			log.Info("XA: Response from server(credit pull): " & sXml)

			reader.Close()
			res.GetResponseStream().Close()
			req.GetRequestStream().Close()

			If sXml.Contains("ERROR") Then Return False

			Return True

		End Function

		Public Function ExecuteDecisionXA(ByVal loanID As String, Optional ByVal isMinor As Boolean = False) As String
			If Not isMinor AndAlso Not websiteConfig.IsDecisionXAEnable Then Return "" 'DecisionXA not setup 
			Dim decisionXAXMLRequest As String = BuildDecisionXAXMLRequest(loanID)
			Dim req As WebRequest = WebRequest.Create(DecisionXAURL)

			req.Method = "POST"
			req.ContentType = "text/xml"

			log.Info("Preparing to POST decisionXA Data: " & CSecureStringFormatter.MaskSensitiveXMLData(decisionXAXMLRequest))

			Using writer As New StreamWriter(req.GetRequestStream())
				writer.Write(decisionXAXMLRequest)
			End Using

			Dim res As WebResponse = req.GetResponse()

			Dim reader As New StreamReader(res.GetResponseStream())
			Dim sXml As String = reader.ReadToEnd()

			log.Info("Response from server(decisionXA): " & sXml)

			reader.Close()
			res.GetResponseStream().Close()
			req.GetRequestStream().Close()

			Return sXml

		End Function

		Public Function IsOneProductQualified(ByVal responseXmlStr As String) As Boolean
			Try
				Dim responseXmlDoc As New XmlDocument()
				responseXmlDoc.LoadXml(responseXmlStr)
				' Get the attribute from the XML response
				Dim responseAttribute As String
				Dim responseNode As XmlNode = responseXmlDoc.SelectSingleNode("//DECISION")
				If responseNode Is Nothing Then Return False
				responseAttribute = Common.SafeString(responseNode.Attributes("status").Value)
				' Compare the result with the proper success result
				Return responseAttribute.ToUpper() = "QUALIFIED"
			Catch ex As Exception
				log.Error("Error processing decisionXA response: " & ex.Message, ex)
			End Try
			Return False
		End Function
#End Region
#Region "Private Methods"
		Private Function IsEFundsSuccessful(ByVal responseXmlStr As String) As Boolean
			' Return true if eFunds was successful
			Dim responseXmlDoc As New XmlDocument()
			responseXmlDoc.LoadXml(responseXmlStr)
			' Get the attribute from the XML response
			Dim responseAttribute As String
			Dim passedIdentityManager As String
			Dim passedOFAC As String

			Dim responseAttributeNode As XmlNode = responseXmlDoc.SelectSingleNode("//@qualifile_decision")
			Dim resIdentityManagerNode As XmlNode = responseXmlDoc.SelectSingleNode("//@passed_identity_manager")
			Dim resOFACNode As XmlNode = responseXmlDoc.SelectSingleNode("//@passed_ofac")

			'' check qualify_decision
			If responseAttributeNode Is Nothing Then Return False

			responseAttribute = responseAttributeNode.InnerXml.ToUpper
			Dim qualifileDecision As Boolean = (responseAttribute = "ACCEPT" OrElse responseAttribute = "APPROVE" OrElse responseAttribute = "APPROVED")

			''exist quailifeDecision, identiyManagerNode and OFACNode 
			If resIdentityManagerNode IsNot Nothing AndAlso resOFACNode IsNot Nothing Then
				passedIdentityManager = resIdentityManagerNode.InnerXml.ToUpper
				passedOFAC = resOFACNode.InnerXml.ToUpper
				Return (qualifileDecision AndAlso passedIdentityManager = "Y" AndAlso passedOFAC = "Y")
			ElseIf resIdentityManagerNode IsNot Nothing And resOFACNode Is Nothing Then	''exist qualifileDecision and IdentityMangager
				passedIdentityManager = resIdentityManagerNode.InnerXml.ToUpper
				Return (qualifileDecision AndAlso passedIdentityManager = "Y")
			ElseIf resIdentityManagerNode Is Nothing AndAlso resOFACNode IsNot Nothing Then	''exist qualifile decision and OFAC
				passedOFAC = resOFACNode.InnerXml.ToUpper
				Return (qualifileDecision AndAlso passedOFAC = "Y")
			End If

			''by default exist only qualifile decision
			Return qualifileDecision
		End Function

		Private Function IsRBankingSuccessful(ByVal responseXmlStr As String) As Boolean
			Dim responseXmlDoc As New XmlDocument()
			responseXmlDoc.LoadXml(responseXmlStr)
			''enable the retailbanking when DebitType = "EID"
			If websiteConfig.DebitType.ToUpper = "EID" Then
				''get the attribute from the xml response
				Dim responseAttributeNode As XmlNode = responseXmlDoc.SelectSingleNode("//@decision_code")
				If responseAttributeNode Is Nothing Then Return False
				' Compare the result with the proper success result
				Return responseAttributeNode.InnerXml.Equals("APPROVED", StringComparison.OrdinalIgnoreCase)
			Else
				Return False
			End If
		End Function
		Private Function BuildeBasicDebitXMLRequest(ByVal loanID As String, Optional ByVal pbIsJoint As Boolean = False) As String

			' Now construct the XML response
			Dim sb As New StringBuilder()
			Using writer As XmlWriter = XmlTextWriter.Create(sb)
				writer.WriteStartDocument()
				' begin input
				writer.WriteStartElement("INPUT")
				writer.WriteStartElement("LOGIN")
				writer.WriteAttributeString("api_user_id", websiteConfig.APIUser)
				writer.WriteAttributeString("api_password", websiteConfig.APIPassword)
				writer.WriteEndElement()
				writer.WriteStartElement("REQUEST")
				writer.WriteAttributeString("xpressappid", loanID)
				If pbIsJoint = False Then
					writer.WriteAttributeString("applicant_index", "0")
					writer.WriteAttributeString("applicant_type", "PRIMARY")
				Else
					writer.WriteAttributeString("applicant_index", "0")
					writer.WriteAttributeString("applicant_type", "JOINT")
				End If
				writer.WriteEndElement()

				' end input
				writer.WriteEndElement()
			End Using
			Dim returnStr = sb.ToString()
			Return returnStr
		End Function
		Private Function ExecuteRetailBanking(ByRef psMessage As String, ByVal loanID As String, ByVal hasJointApplicant As Boolean) As Boolean
			Dim rBankingXMLRequest As String = BuildeBasicDebitXMLRequest(loanID)
			Dim req As WebRequest = WebRequest.Create(RetailBankingURL)

			req.Method = "POST"
			req.ContentType = "text/xml"

			log.Info("XA: Preparing to POST RetailBanking Data: " & CSecureStringFormatter.MaskSensitiveXMLData(rBankingXMLRequest))

			Using writer As New StreamWriter(req.GetRequestStream())
				writer.Write(rBankingXMLRequest)
			End Using

			Dim res As WebResponse = req.GetResponse()

			Dim reader As New StreamReader(res.GetResponseStream())
			Dim sXml As String = reader.ReadToEnd()

			log.Info("XA: Response from server(RetailBanking): " + sXml)

			reader.Close()
			res.GetResponseStream().Close()
			req.GetRequestStream().Close()

			' Check to see if the eFunds response is not an error
			If Not IsSubmitted(sXml) Then
				Return False
			End If

			If IsRBankingSuccessful(sXml) Then
				psMessage = "ACCEPT" ''pass question
			Else
				psMessage = "REFER"	 '' fail question
				Return False '' exit 
			End If
			''======Run for joint==========
			If Not hasJointApplicant Then Return True

			Dim rBankingXMLRequest2 As String = BuildeBasicDebitXMLRequest(loanID, True)
			Dim req2 As WebRequest = WebRequest.Create(RetailBankingURL)

			req2.Method = "POST"
			req2.ContentType = "text/xml"

			log.Info("XA Joint: Preparing to POST RetailBanking Data: " & CSecureStringFormatter.MaskSensitiveXMLData(rBankingXMLRequest2))

			Using writer2 As New StreamWriter(req2.GetRequestStream())
				writer2.Write(rBankingXMLRequest2)
			End Using

			Dim res2 As WebResponse = req2.GetResponse()

			Dim reader2 As New StreamReader(res2.GetResponseStream())
			Dim sXml2 As String = reader2.ReadToEnd()

			log.Info("XA Joint: Response from server(RetailBanking): " + sXml2)

			reader2.Close()
			res2.GetResponseStream().Close()
			req2.GetRequestStream().Close()

			' Check to see if the eFunds response is not an error
			If Not IsSubmitted(sXml2) Then
				Return False
			End If

			If IsRBankingSuccessful(sXml2) Then
				psMessage = "ACCEPT"	'pass question
			Else
				psMessage = "REFER"	 'fail question
				Return False ''exit 
			End If
			Return True

		End Function
		Private Function ExecuteEfund(ByRef psMessage As String, ByVal loanID As String, ByVal hasJointApplicant As Boolean) As Boolean
			Dim eFundsXMLRequest As String = BuildeBasicDebitXMLRequest(loanID)
			Dim req As WebRequest = WebRequest.Create(EFundsURL)

			req.Method = "POST"
			req.ContentType = "text/xml"

			log.Info("XA: Preparing to POST eFunds Data: " & CSecureStringFormatter.MaskSensitiveXMLData(eFundsXMLRequest))

			Dim writer As New StreamWriter(req.GetRequestStream())
			writer.Write(eFundsXMLRequest)
			writer.Close()

			Dim res As WebResponse = req.GetResponse()

			Dim reader As New StreamReader(res.GetResponseStream())
			Dim sXml As String = reader.ReadToEnd()

			log.Info("XA: Response from server(eFund): " + sXml)

			''strip off invalid append after OUTPUT element - patch invalid xml format from lpq side 
			''don't know hwy this is only happening for sdcc offsite
			sXml = sXml.Substring(0, sXml.IndexOf("</OUTPUT>", StringComparison.OrdinalIgnoreCase) + 9)
			'log.Info("XA: Response from server(eFund) - stripped append: " + sXml)

			reader.Close()
			res.GetResponseStream().Close()
			req.GetRequestStream().Close()

			' Check to see if the eFunds response is not an error
			If Not IsSubmitted(sXml) Then
				Return False
			End If


			If IsEFundsSuccessful(sXml) Then	'reserve 10/2
				psMessage = "ACCEPT"	'pass question
			Else
				psMessage = "REFER"	 'fail question
				Return False  'fail return false dont want to run join if the primary is referred
			End If

			''======Run for joint==========
			If Not hasJointApplicant Then Return True

			Dim eFundsXMLRequest2 As String = BuildeBasicDebitXMLRequest(loanID, True)
			Dim req2 As WebRequest = WebRequest.Create(EFundsURL)

			req2.Method = "POST"
			req2.ContentType = "text/xml"

			log.Info("XA Joint: Preparing to POST eFunds Data: " & CSecureStringFormatter.MaskSensitiveXMLData(eFundsXMLRequest2))

			Dim writer2 As New StreamWriter(req2.GetRequestStream())
			writer2.Write(eFundsXMLRequest2)
			writer2.Close()

			Dim res2 As WebResponse = req2.GetResponse()

			Dim reader2 As New StreamReader(res2.GetResponseStream())
			Dim sXml2 As String = reader2.ReadToEnd()

			log.Info("XA Joint: Response from server(eFund): " + sXml2)

			''strip off invalid append after OUTPUT element - patch invalid xml format from lpq side 
			sXml2 = sXml2.Substring(0, sXml2.IndexOf("</OUTPUT>", StringComparison.OrdinalIgnoreCase) + 9)
			'log.Info("XA Joint: Response from server(eFund) - stripped append: " + sXml2)

			reader2.Close()
			res2.GetResponseStream().Close()
			req2.GetRequestStream().Close()

			' Check to see if the eFunds response is not an error
			If Not IsSubmitted(sXml2) Then
				Return False
			End If


			If IsEFundsSuccessful(sXml2) Then	'reserve 10/2
				psMessage = "ACCEPT"	'pass question
			Else
				psMessage = "REFER"	 'fail question
				Return False '' exit 
			End If
			Return True

		End Function

		'TODO: this is for DID only
		Private Function GetFollowOnQuestion(ByVal psAuthenticationType As String, ByVal psXmlresponse As String, ByVal firstName As String, ByRef transactionID As String, ByRef questionSetID As String, ByRef requestAppID As String, ByRef requestClientID As String, ByRef requestSequenceID As String, ByRef referenceNumber As String, ByRef questionListPersist As List(Of CWalletQuestionRendering.questionDataClass)) As String
			Dim walletObject As CWalletQuestionRendering
			' Process the Out of Wallet Questions and return them so they can be displayed to the user
			walletObject = New CWalletQuestionRendering(psXmlresponse, websiteConfig)
			walletObject.setAuthenticationType(psAuthenticationType)
			walletObject.populateQuestions(transactionID, questionSetID, requestAppID, requestClientID, requestSequenceID, referenceNumber)

			questionListPersist = walletObject.questionsList

			Dim walletQuestionsHtml As String = walletObject.RenderWalletQuestions(firstName, transactionID, questionSetID, requestAppID, requestClientID, requestSequenceID, referenceNumber)
			Dim numQuestionsStr = Format(walletObject.NumQuestions, "00")
			Dim sWalletQuestions = numQuestionsStr & walletQuestionsHtml
			'sWalletQuestions = sWalletQuestions.Replace("WALLETQUESTION", "WALLETQUESTION_FOLLOWON")

			Return sWalletQuestions
		End Function
		Private Function BuildWalletAnswerXML(ByVal builder As IBuildWalletAnswers, ByVal answerIDList As List(Of String), ByVal authenticationMethod As String, ByVal questionSetID As String, ByVal requestAppID As String, ByVal requestClientID As String, ByVal requestSequenceID As String, ByVal referenceNumber As String, ByVal walletAnswersText As String, ByVal hasCoFollowOnQuestions As Boolean) As String
			Select Case authenticationMethod.ToUpper()
				Case "RSA"
					Return builder.BuildWalletAnswerXMLRSA(questionSetID, answerIDList)
				Case "PID"
					Return builder.BuildWalletAnswerXMLPID(answerIDList)
				Case "FIS"
					Return builder.BuildWalletAnswerXMLFIS(questionSetID, answerIDList)
				Case "DID"
					''get answer text
					Dim answerTextList As New List(Of String)
					If Not String.IsNullOrEmpty(walletAnswersText) Then
						answerTextList = JsonConvert.DeserializeObject(Of List(Of String))(walletAnswersText)
					End If
					Return builder.BuildWalletAnswerXMLDID(hasCoFollowOnQuestions, requestAppID, requestClientID, requestSequenceID, answerTextList)
					' Equifax 
				Case "EID"
					Return builder.BuildWalletAnswerXMLEquifax(answerIDList)
				Case "TID"
					Return builder.BuildWalletAnswerXMLTID(referenceNumber, answerIDList)
			End Select

			Return Nothing

		End Function
		Public Sub RestoreFundingInfoWhenDecisionXAFailed(ByVal innerXMLDoc As XmlDocument, xaAppInfo As XaSubmission)
			Dim oApprovedAccountNode As XmlNode = innerXMLDoc.GetElementsByTagName("APPROVED_ACCOUNTS")(0)
			Dim oInterestedAccountNode As XmlNode = innerXMLDoc.GetElementsByTagName("INTERESTED_ACCOUNTS")(0)
			Dim NAMESPACE_URI = "http://www.meridianlink.com/CLF"
			If Not oApprovedAccountNode.HasChildNodes Then
				Dim oCoAppSSNDic As New Dictionary(Of String, String)
				If xaAppInfo.BAApplicants IsNot Nothing AndAlso xaAppInfo.BAApplicants.Any() Then
					For Each app As CBAApplicantInfo In xaAppInfo.BAApplicants
						''add co-applicants to CoAppSSNDic
						If xaAppInfo.BAApplicants.IndexOf(app) > 0 Then
							If app.SSN IsNot Nothing AndAlso Not oCoAppSSNDic.ContainsKey(app.SSN) Then
								oCoAppSSNDic.Add(app.SSN, app.IsJoint)
							End If
						End If
					Next
				ElseIf xaAppInfo.SAApplicants IsNot Nothing AndAlso xaAppInfo.SAApplicants.Any() Then
					For Each app As CSAApplicantInfo In xaAppInfo.SAApplicants
						''add co-applicants to CoAppSSNDic
						If xaAppInfo.SAApplicants.IndexOf(app) > 0 Then
							If app.SSN IsNot Nothing AndAlso Not oCoAppSSNDic.ContainsKey(app.SSN) Then
								oCoAppSSNDic.Add(app.SSN, app.IsJoint)
							End If
						End If
					Next
				End If
				oApprovedAccountNode.AppendChild(innerXMLDoc.CreateElement("ACCOUNT_TYPE", NAMESPACE_URI))
				oApprovedAccountNode.InnerXml = oInterestedAccountNode.InnerXml
				Dim oAccountTypeNodes As XmlNodeList = oApprovedAccountNode.ChildNodes()
				For Each oAccountType As XmlElement In oAccountTypeNodes
					''min_deposit, max_deposit,and is_require_confirmation are not valid attributes in approved account node so remove it
					oAccountType.RemoveAttribute("min_deposit")
					oAccountType.RemoveAttribute("max_deposit")
					oAccountType.RemoveAttribute("is_require_confirmation")
					oAccountType.AppendChild(innerXMLDoc.CreateElement("ACCOUNT_TYPE_RELATIONS", NAMESPACE_URI))
					Dim hasOnwershipLevel As String = Common.hasOwnerShipLevel(websiteConfig)
					If hasOnwershipLevel = "Y" Then
						Dim oAccountTypeRelationNodes As XmlNodeList = oAccountType.GetElementsByTagName("ACCOUNT_TYPE_RELATIONS")
						If oCoAppSSNDic.Count > 0 Then ''special account and business xa
							Dim oAccountTypeRelationElement As XmlElement = oAccountTypeRelationNodes(0)
							Dim index = 0
							For Each oSSN As KeyValuePair(Of String, String) In oCoAppSSNDic
								oAccountTypeRelationElement.AppendChild(innerXMLDoc.CreateElement("RELATION", NAMESPACE_URI))
								Dim oRelationElement As XmlElement = oAccountTypeRelationElement.GetElementsByTagName("RELATION")(index)
								oRelationElement.SetAttribute("ssn", oSSN.Key)
								oRelationElement.SetAttribute("benefactor_type", If(oSSN.Value.ToUpper = "Y", "J", "C"))
								index += 1
							Next
						ElseIf Common.SafeString(HttpContext.Current.Request.Form("co_SSN")) <> "" Then	''standard XA
							Dim oAccountTypeRelationElement As XmlElement = oAccountTypeRelationNodes(0)
							oAccountTypeRelationElement.AppendChild(innerXMLDoc.CreateElement("RELATION", NAMESPACE_URI))
							Dim oRelationElement As XmlElement = oAccountTypeRelationElement.GetElementsByTagName("RELATION")(0)
							oRelationElement.SetAttribute("ssn", Common.SafeString(HttpContext.Current.Request.Form("co_SSN")))
							oRelationElement.SetAttribute("benefactor_type", "J")
						End If
					End If

					Dim oInterestedProdCQ As XmlNodeList = oAccountType.GetElementsByTagName("PRODUCT_CUSTOM_QUESTIONS_INTERESTED")
					If oInterestedProdCQ IsNot Nothing AndAlso oInterestedProdCQ.Count > 0 Then
						Dim oApprovedProdCQ = innerXMLDoc.CreateElement("PRODUCT_CUSTOM_QUESTIONS_APPROVED", NAMESPACE_URI)
						oApprovedProdCQ.AppendChild(innerXMLDoc.CreateElement("CUSTOM_QUESTION", NAMESPACE_URI))
						oAccountType.AppendChild(oApprovedProdCQ)
						oApprovedProdCQ.InnerXml = oInterestedProdCQ(0).InnerXml ''add custom questions to PRODUCT_CUSTOM_QUESTION_APPROVED node
						oAccountType.RemoveChild(oInterestedProdCQ(0)) ''remove PRODUCT_CUSTOM_QUESTIONS_INTERESTED node
					End If
				Next
			End If
		End Sub

		Private Function BuildDecisionXAXMLRequest(ByVal loanID As String) As String
			' Now construct the XML response
			Dim sb As New StringBuilder()
			Using writer As XmlWriter = XmlTextWriter.Create(sb)
				writer.WriteStartDocument()
				' begin input
				writer.WriteStartElement("INPUT")
				writer.WriteStartElement("LOGIN")
				writer.WriteAttributeString("api_user_id", websiteConfig.APIUser)
				writer.WriteAttributeString("api_password", websiteConfig.APIPassword)
				writer.WriteEndElement() 'end LOGIN

				writer.WriteStartElement("REQUEST")
				writer.WriteAttributeString("xpress_app_id", loanID)
				writer.WriteAttributeString("action_type", "UPDATE")
				writer.WriteEndElement() 'end REQUEST

				writer.WriteEndElement() ' end input
			End Using
			Dim returnStr = sb.ToString()
			Return returnStr
		End Function
		Private Function BuildCreditPullXMLRequest(ByVal loanID As String) As String
			' Now construct the XML response
			Dim sb As New StringBuilder()
			Using writer As XmlWriter = XmlTextWriter.Create(sb)
				writer.WriteStartDocument()

				' begin input
				writer.WriteStartElement("INPUT")

				writer.WriteStartElement("LOGIN")
				writer.WriteAttributeString("api_user_id", websiteConfig.APIUser)
				writer.WriteAttributeString("api_password", websiteConfig.APIPassword)
				writer.WriteEndElement()

				writer.WriteStartElement("REQUEST")
				writer.WriteAttributeString("app_id", loanID)
				writer.WriteEndElement()

				writer.WriteEndElement() ' end input
			End Using
			Dim returnStr = sb.ToString()
			Return returnStr
		End Function
		Private Function HasPassedAuthentication(ByVal authMethod As String, ByVal responseXmlStr As String) As Boolean
			' Return true if we passed the authentication 
			' We have to know which authentication method was used.
			Dim responseXmlDoc As New XmlDocument()
			responseXmlDoc.LoadXml(responseXmlStr)

			' Get the result depending on the authentication method
			Dim successAttribute As String
			Dim successValue As String
			Select Case authMethod.ToUpper()
				Case "RSA"
					successAttribute = "transaction_result"
					successValue = "PASSED"
				Case "PID"
					successAttribute = "decision"
					successValue = "ACCEPT"
				Case "FIS"
					successAttribute = "decision"
					successValue = "PASS"
				Case "DID"
					successAttribute = "decision"
					successValue = "PASSED"
					'equifax 
				Case "EID"
					successAttribute = "decision_code"
					successValue = "Y"
					''transunion
				Case "TID"
					successAttribute = "decision"
					successValue = "ACCEPT"
				Case Else
					successAttribute = "transaction_result"
					successValue = "ACCEPT"
			End Select

			' Get the attribute from the XML response
			Dim responseAttribute As String = ""
			Dim responseAttributeNode As XmlNode = responseXmlDoc.SelectSingleNode("//@" + successAttribute)
			'equifax - to avoid the exception error in case the node has nothing
			If responseAttributeNode IsNot Nothing Then
				responseAttribute = responseAttributeNode.InnerXml
			End If
			' Compare the result with the proper success result
			Return responseAttribute.ToUpper() = successValue.ToUpper()

		End Function

		Private Function HasFollowOnQuestion(ByVal authMethod As String, ByVal responseXmlStr As String) As Boolean
			' Return true if there is follow on question
			' We have to know which authentication method was used.
			Dim responseXmlDoc As New XmlDocument()
			responseXmlDoc.LoadXml(responseXmlStr)

			' Get the result depending on the authentication method
			Dim successAttribute As String
			Dim successValue As String
			Select Case authMethod.ToUpper()
				'Case "RSA"
				'	successAttribute = "transaction_result"
				'	successValue = "PASSED"
				'Case "PID"
				'	successAttribute = "decision"
				'	successValue = "ACCEPT"
				'Case "FIS"
				'	successAttribute = "decision"
				'	successValue = "PASS"
				Case "DID"
					successAttribute = "decision"
					successValue = "MORE QUESTIONS"
					'equifax 
					'Case "EID"
					'	successAttribute = "decision_code"
					'	successValue = "Y"
					'	''transunion
					'Case "TID"
					'	successAttribute = "decision"
					'	successValue = "ACCEPT"
				Case Else
					successAttribute = "transaction_result"
					successValue = "bogus blah so this method return a fail when not DID"
			End Select

			' Get the attribute from the XML response
			Dim responseAttribute As String = ""
			Dim responseAttributeNode As XmlNode = responseXmlDoc.SelectSingleNode("//@" + successAttribute)
			'equifax - to avoid the exception error in case the node has nothing
			If responseAttributeNode IsNot Nothing Then
				responseAttribute = responseAttributeNode.InnerXml
			End If
			' Compare the result with the proper success result
			Return responseAttribute.ToUpper() = successValue.ToUpper()

		End Function

		Private Function GetFollowOnQuestionMinor(ByVal psAuthenticationType As String, ByVal psXMLresponse As String, ByVal sApplicantIndex As String, ByVal isJoint As Boolean, ByVal state As XaState, ByVal xaAppInfo As XaSubmission) As String
			' Process the Out of Wallet Questions and return them so they can be displayed to the user
			Dim walletObject As New CWalletQuestionRendering(psXMLresponse, websiteConfig)
			walletObject.setAuthenticationType(psAuthenticationType)
			walletObject.populateQuestions(state.TransactionID, state.QuestionSetID, state.RequestAppID, state.RequestClientID, state.RequestSequenceID, state.ReferenceNumber)

			state.WalletQuestionList = walletObject.questionsList
			Dim sName = xaAppInfo.MinorAppPersonal.FirstName
			If sApplicantIndex = "1" Then
				sName = If(isJoint, xaAppInfo.CoAppPersonal.FirstName, xaAppInfo.Personal.FirstName)
			End If

			Dim walletQuestionsHTML = walletObject.RenderWalletQuestions(sName, state.TransactionID, state.QuestionSetID, state.RequestAppID, state.RequestClientID, state.RequestSequenceID, state.ReferenceNumber)

			Dim numQuestionsStr = Format(walletObject.NumQuestions, "00")

			Dim sWalletQuestions = (numQuestionsStr + walletQuestionsHTML)

			Return sWalletQuestions
		End Function
		Private Function DetermineAuthenticationType(ByRef questionsURL As String, ByRef answersURL As String, Optional ByVal isMinor As Boolean = False, Optional ByVal sMinorAuthenticationType As String = "") As String
			' Read the attribute from the config.xml file and determinie which type of authentication we need to use
			Dim sAuthenticationType As String = websiteConfig.AuthenticationType.ToUpper()
			If isMinor Then
				sAuthenticationType = sMinorAuthenticationType.ToUpper()
			End If

			Select Case sAuthenticationType
				Case "RSA"
					questionsURL = String.Concat(websiteConfig.BaseSubmitLoanUrl, "/rsa/getquestions.aspx")
					answersURL = String.Concat(websiteConfig.BaseSubmitLoanUrl, "/rsa/submitanswers.aspx")
					Return "RSA"
				Case "PID"
					questionsURL = String.Concat(websiteConfig.BaseSubmitLoanUrl, "/PreciseID/GetQuestions.aspx")
					answersURL = String.Concat(websiteConfig.BaseSubmitLoanUrl, "/PreciseID/SubmitAnswers.aspx")
					Return "PID"
					'EquiFax
				Case "EID"
					questionsURL = String.Concat(websiteConfig.BaseSubmitLoanUrl, "/EquifaxInterConnect/GetQuestions.aspx")
					answersURL = String.Concat(websiteConfig.BaseSubmitLoanUrl, "/EquifaxInterConnect/SubmitAnswers.aspx")
					Return "EID"

				Case "FIS"
					questionsURL = String.Concat(websiteConfig.BaseSubmitLoanUrl, "/eFunds/GetQuestions.aspx")
					answersURL = String.Concat(websiteConfig.BaseSubmitLoanUrl, "/eFunds/SubmitAnswers.aspx")
					Return "FIS"

				Case "DID"
					questionsURL = String.Concat(websiteConfig.BaseSubmitLoanUrl, "/DeluxeIDA/GetQuestions.aspx")
					answersURL = String.Concat(websiteConfig.BaseSubmitLoanUrl, "/DeluxeIDA/SubmitAnswers.aspx")
					Return "DID"
				Case "TID" '' TransUnion
					questionsURL = String.Concat(websiteConfig.BaseSubmitLoanUrl, "/IDMA/GetQuestions.aspx")
					answersURL = String.Concat(websiteConfig.BaseSubmitLoanUrl, "/IDMA/SubmitAnswers.aspx")
					Return "TID"
			End Select
			' If nothing is specified, don't use any method at all
			questionsURL = String.Concat(websiteConfig.BaseSubmitLoanUrl, "/rsa/getquestions.aspx")
			answersURL = String.Concat(websiteConfig.BaseSubmitLoanUrl, "/rsa/submitanswers.aspx")
			Return "NONE"
		End Function

		Private Function ExecWalletQuestionsMinor(ByRef psWalletQuestions As String, ByVal sApplicantIndex As String, ByVal isJoint As Boolean, ByVal sMinorAuthenticationType As String, ByVal loanID As String, ByRef transactionID As String, ByRef questionSetID As String, ByRef requestAppID As String, ByRef requestClientID As String, ByRef requestSequenceID As String, ByRef referenceNumber As String, ByRef questionListPersist As List(Of CWalletQuestionRendering.questionDataClass), ByVal xaAppInfo As XaSubmission, ByVal loanResponseXMLStr As String) As Boolean
			'log.Info("XA: Preparing request for Out of Wallet Questions")

			' Get the correct URL depending on which authentication method was given in the config.xml file.
			Dim questionsURL As String = ""
			Dim answersURL As String = ""
			Dim authenticationMethod As String = DetermineAuthenticationType(questionsURL, answersURL, True, sMinorAuthenticationType).Trim
			Dim url As String = questionsURL
			Dim oGetWalletQuestion = New CMinorGetWalletQuestions()

			Dim walletRequest As String = "RSA"
			Select Case authenticationMethod
				Case "RSA", "PID", "FIS", "DID", "EID", "TID"
					walletRequest = oGetWalletQuestion.GetOutOfWalletQuestions(loanResponseXMLStr, loanID, websiteConfig, sApplicantIndex, isJoint, authenticationMethod)
				Case "NONE"
					' If the option was set to NONE or there was no option set, then the lender does not support ID authentication
					' Then we're done.
					Return True
			End Select

			Dim req As WebRequest = WebRequest.Create(url)
			req.Method = "POST"
			req.ContentType = "text/xml"

			log.Info("ExecWalletQuestions Request: " & CSecureStringFormatter.MaskSensitiveXMLData(walletRequest))

			Dim writer As New StreamWriter(req.GetRequestStream())
			writer.Write(walletRequest)
			writer.Close()
			log.Info("Start the request to " + url)

			Dim res As WebResponse = req.GetResponse()
			Dim reader As StreamReader = New StreamReader(res.GetResponseStream())
			Dim sXml As String = reader.ReadToEnd()

			If sXml.Contains("INTERNAL_ERROR") Then
				log.Warn("ExecWalletQuestions Response: " + sXml)
				'case for unable to lcate the person, invalid social
				Return False
			Else
				log.Info("ExecWalletQuestions Response: " + sXml)
			End If


			reader.Close()
			res.GetResponseStream().Close()
			req.GetRequestStream().Close()

			Dim bIsFailedAuthenticate As Boolean = False
			If Not IsSubmitted(sXml, bIsFailedAuthenticate) Then
				Return False
			End If
			If bIsFailedAuthenticate Then
				'case for unable to lcate the person, invalid social
				Return False
			End If

			Dim walletObject As CWalletQuestionRendering
			' Process the Out of Wallet Questions and return them so they can be displayed to the user
			walletObject = New CWalletQuestionRendering(sXml, websiteConfig)
			walletObject.setAuthenticationType(authenticationMethod)
			walletObject.populateQuestions(transactionID, questionSetID, requestAppID, requestClientID, requestSequenceID, referenceNumber)

			questionListPersist = walletObject.questionsList

			Dim sName = xaAppInfo.MinorAppPersonal.FirstName
			If sApplicantIndex = "1" Then
				sName = If(isJoint, xaAppInfo.CoAppPersonal.FirstName, xaAppInfo.Personal.FirstName)
			End If
			Dim walletQuestionsHTML = walletObject.RenderWalletQuestions(sName, transactionID, questionSetID, requestAppID, requestClientID, requestSequenceID, referenceNumber)

			Dim numQuestionsStr = Format(walletObject.NumQuestions, "00")

			psWalletQuestions = (numQuestionsStr + walletQuestionsHTML)
			Return True
		End Function
		Private Function ExecuteUpLoadDocument(oLoanApp As XaSubmission, ByVal loanNumber As String) As Boolean
			If oLoanApp.DocBase64Dic Is Nothing OrElse oLoanApp.DocBase64Dic.Count = 0 Then Return True
			Dim uploadDocument As String = BuildUploadDocumentsRequest(oLoanApp, loanNumber)
			If String.IsNullOrWhiteSpace(uploadDocument) Then Return False
			Dim submitUploadDocumentUrl As String = String.Concat(websiteConfig.BaseSubmitLoanUrl.Substring(0, websiteConfig.BaseSubmitLoanUrl.IndexOf("services", StringComparison.OrdinalIgnoreCase)), "Services/ipad/iPadDocumentSubmit.aspx")
			Dim req As WebRequest = WebRequest.Create(submitUploadDocumentUrl)
			req.Method = "POST"
			req.ContentType = "text/xml"
			log.Info("Preparing to POST upload document: " & Left(CSecureStringFormatter.MaskSensitiveXMLData(uploadDocument), 100))

			Using writer As New StreamWriter(req.GetRequestStream())
				writer.Write(uploadDocument)
			End Using

			Dim res As WebResponse = req.GetResponse()

			Dim reader As New StreamReader(res.GetResponseStream())
			Dim sXml As String = reader.ReadToEnd()
			log.Info("XA: Response from server(upload document): " + sXml)
			''need to make sure submit document to the system success
			Dim xmlDoc As New XmlDocument()
			xmlDoc.LoadXml(sXml)
			Dim responseNode As XmlElement = CType(xmlDoc.SelectSingleNode("OUTPUT/RESPONSE"), XmlElement)
			If responseNode.GetAttribute("status") = "FAILURE" Then Return False 'May be there is not enough space on the disk ....
			reader.Close()
			res.GetResponseStream().Close()
			req.GetRequestStream().Close()
			Return IsSubmitted(sXml)
		End Function
		Private Function BuildUploadDocumentsRequest(ByVal oLoanApp As XaSubmission, ByVal loanNumber As String) As String
			Dim oDocCleaned As New Dictionary(Of String, String) 'preprocess or format document type
			Dim sb As New StringBuilder()
			If oLoanApp.DocBase64Dic IsNot Nothing AndAlso oLoanApp.DocBase64Dic.Count > 0 Then	''make sure  docbase64dic is not null
				' Prepare the various URLs
				For Each doc As KeyValuePair(Of String, String) In oLoanApp.DocBase64Dic
					Dim fileExtensionMatch = New Regex("\.([a-z]{3})$", RegexOptions.IgnoreCase).Match(doc.Key)
					If fileExtensionMatch.Success Then
						Dim fileExtension As String = fileExtensionMatch.Groups(1).Value
						If fileExtension.ToUpper = "PDF" Then
							Dim sPDF As String = doc.Value.Replace("data:application/pdf;base64,", "")
							sPDF = sPDF.Replace(" ", "") ''remove white space if it exist
							''make sure input string is base64string 
							If (sPDF.Length Mod 4 = 0) Then
								Dim sPDFByte As Byte() = Convert.FromBase64String(sPDF)
								Dim sPDFString As String = Convert.ToBase64String(sPDFByte)
								oDocCleaned.Add(doc.Key, sPDFString)
							Else
								log.Info("The input string '" & doc.Key & "' is not base64string")
							End If
						ElseIf fileExtension.ToUpper = "PNG" OrElse fileExtension.ToUpper = "JPG" OrElse fileExtension.ToUpper = "GIF" Then	'need to convert to pdf
							Dim sImage As String = doc.Value.Replace("data:image/png;base64,", "")
							''need to replace for all cases jpeg, png, jpg, and gif
							sImage = sImage.Replace("data:image/jpeg;base64,", "")
							sImage = sImage.Replace("data:image/jpg;base64,", "")
							sImage = sImage.Replace("data:image/gif;base64,", "")
							'Dim sImage = StreamFile2Base64("front_license3.jpg")
							sImage = sImage.Replace(" ", "") ''remove white space if it exist
							''make sure input string is base64string
							If (sImage.Length Mod 4 = 0) Then
								oDocCleaned.Add(doc.Key, Common.convertBase64Image2Base64PDF(sImage, doc.Key))
							Else
								log.Info("The input string of '" & doc.Key & "' is not base64string")
							End If
						Else
							log.Info("Invalid file extension: " & fileExtension)
						End If
					Else
						log.Info("Invalid file format: missing file extension")
						Continue For
					End If
				Next
				If oDocCleaned.Count > 0 Then
					Using writer As XmlWriter = XmlTextWriter.Create(sb)
						' Now construct the XML response
						writer.WriteStartDocument()
						' begin input
						writer.WriteStartElement("INPUT")
						writer.WriteStartElement("LOGIN")
						writer.WriteAttributeString("api_user_id", websiteConfig.APIUser)
						writer.WriteAttributeString("api_password", websiteConfig.APIPassword)
						writer.WriteEndElement()

						writer.WriteStartElement("REQUEST")
						writer.WriteAttributeString("app_num", loanNumber)
						writer.WriteAttributeString("app_type", "DEPOSIT")
						writer.WriteEndElement() ''request
						writer.WriteStartElement("DOCUMENTS")
						''for loop for each document
						For Each item As KeyValuePair(Of String, String) In oDocCleaned
							''get DocCode and DocGroup user input
							Dim docGroup As String = ""
							Dim docCode As String = ""
							Dim docTitle As String = ""
							If oLoanApp.UploadDocumentInfoList.Count > 0 Then
								For Each oUploadDocInfo As uploadDocumentInfo In oLoanApp.UploadDocumentInfoList
									If item.Key = oUploadDocInfo.docFileName Then
										docGroup = oUploadDocInfo.docGroup
										docCode = oUploadDocInfo.docCode
										docTitle = oUploadDocInfo.docTitle
										Exit For
									End If
								Next
							End If
							writer.WriteStartElement("DOCUMENT")
							writer.WriteAttributeString("title", If(String.IsNullOrEmpty(docTitle), item.Key, docTitle))
							writer.WriteAttributeString("DocID", Guid.NewGuid().ToString)
							If Not String.IsNullOrEmpty(docCode) Then
								writer.WriteAttributeString("DocCode", docCode)
							End If
							If Not String.IsNullOrEmpty(docGroup) Then
								writer.WriteAttributeString("DocGroup", docGroup)
							End If
							writer.WriteValue(item.Value)
							writer.WriteEndElement() 'end documents
						Next
						writer.WriteEndElement() ''end documents
						writer.WriteEndElement() ''end input
					End Using
				End If
			End If
			Return sb.ToString()
		End Function
#End Region
	End Class
End Namespace

