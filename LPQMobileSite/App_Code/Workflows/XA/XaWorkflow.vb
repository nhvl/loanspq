﻿Imports LPQMobile.Utils
Imports Workflows.XA.Handlers
Imports Workflows.Base.Handlers
Imports Workflows.Base.BO
Imports Workflows.XA.BO
Imports Workflows.Base

Namespace Workflows.XA
	Public Class XaWorkflow(Of TS As {New, XaSubmission}, TR As {XaState}, TC As {XaConfig})
		Inherits BaseWorkflow(Of TS, TR, TC)

		Public Sub New(httpContext As HttpContext, storage As Storage(Of TS, TR, TC))
			MyBase.New(httpContext, storage)
			CurrentStorage.State.InternalCommentAppend = ""	'reset
		End Sub

		Protected Overrides Function RegisterNodes() As List(Of BaseHandler(Of TS, TR, TC))
			Dim nodeList As New List(Of BaseHandler(Of TS, TR, TC))
			nodeList.Add(New CheckEModeHandler(Of TS, TR, TC)(CurrentStorage, CurrentHttpContext))
			nodeList.Add(New ValidateTokenHandler(Of TS, TR, TC)(CurrentStorage, CurrentHttpContext))
			nodeList.Add(New ValidateIPHandler(Of TS, TR, TC)(CurrentStorage, CurrentHttpContext))
			nodeList.Add(New ApplicantBlockLogicHandler(Of TS, TR, TC)(CurrentStorage, CurrentHttpContext))
			nodeList.Add(New XaInitialHandler(Of TS, TR, TC)(CurrentStorage, CurrentHttpContext))
			Dim task As String = Common.SafeString(CurrentHttpContext.Request.Form("Task"))
			If Common.SafeString(CurrentHttpContext.Request.Form("HasMinorApplicant")).Equals("Y") Then
				'exec minor account process
				If task = "SubmitLoan" Then
					nodeList.Add(New XaValidateInputHandler(Of TS, TR, TC)(CurrentStorage, CurrentHttpContext))
					nodeList.Add(New XaPreUnderWriteHandler(Of TS, TR, TC)(CurrentStorage, CurrentHttpContext))
					nodeList.Add(New XaCreateNewLoanHandler(Of TS, TR, TC)(CurrentStorage, CurrentHttpContext))
					nodeList.Add(New XaMinorAccountLoanSubmitHandler(Of TS, TR, TC)(CurrentStorage, CurrentHttpContext))
					nodeList.Add(New XaMinorAccountIDAHandler(Of TS, TR, TC)(CurrentStorage, CurrentHttpContext))
				ElseIf task = "WalletQuestions" Then
					nodeList.Add(New XaMinorAccountWalletQuestionsHandler(Of TS, TR, TC)(CurrentStorage, CurrentHttpContext))
				End If
				nodeList.Add(New XaMinorAccountExecuteDebitHandler(Of TS, TR, TC)(CurrentStorage, CurrentHttpContext))
				nodeList.Add(New XaMinorAccountExecuteCreditAndDecisionHandler(Of TS, TR, TC)(CurrentStorage, CurrentHttpContext))
				nodeList.Add(New XaUpdateLoanHandler(Of TS, TR, TC)(CurrentStorage, CurrentHttpContext))
				nodeList.Add(New XaMinorAccountFundingAndBookingHandler(Of TS, TR, TC)(CurrentStorage, CurrentHttpContext))
			Else
				'exec standard account process
				If task = "SubmitLoan" Then
					nodeList.Add(New XaValidateInputHandler(Of TS, TR, TC)(CurrentStorage, CurrentHttpContext))
					nodeList.Add(New XaPreUnderWriteHandler(Of TS, TR, TC)(CurrentStorage, CurrentHttpContext))
					nodeList.Add(New XaCreateNewLoanHandler(Of TS, TR, TC)(CurrentStorage, CurrentHttpContext))
					nodeList.Add(New XaStandardAccountLoanSubmitHandler(Of TS, TR, TC)(CurrentStorage, CurrentHttpContext))
					nodeList.Add(New XaStandardAccountIDAHandler(Of TS, TR, TC)(CurrentStorage, CurrentHttpContext))
				ElseIf task = "WalletQuestions" Then
					nodeList.Add(New XaStandardAccountWalletQuestionsHandler(Of TS, TR, TC)(CurrentStorage, CurrentHttpContext))
				End If
				nodeList.Add(New XaRunISTWatchHandler(Of TS, TR, TC)(CurrentStorage, CurrentHttpContext))
				nodeList.Add(New XaStandardAccountExecuteDebitHandler(Of TS, TR, TC)(CurrentStorage, CurrentHttpContext))
				nodeList.Add(New XaStandardAccountExecuteCreditAndDecisionHandler(Of TS, TR, TC)(CurrentStorage, CurrentHttpContext))
				nodeList.Add(New XaUpdateLoanHandler(Of TS, TR, TC)(CurrentStorage, CurrentHttpContext))
				nodeList.Add(New XaXSellHandler(Of TS, TR, TC)(CurrentStorage, CurrentHttpContext))
				''put it here so we don't have to deal with Fraud case and persist customSubmittedSuccessMessage, no need to update status
				nodeList.Add(New XaPaypalFundingHandler(Of TS, TR, TC)(CurrentStorage, CurrentHttpContext))
				nodeList.Add(New XaStandardAccountFundingAndBookingHandler(Of TS, TR, TC)(CurrentStorage, CurrentHttpContext))
			End If
			Return nodeList
		End Function
		Private Sub OnCompleted() Handles Me.Completed
			If CurrentStorage.State.IsUpdateReferralStatusRequired And CurrentStorage.Config.WebsiteConfig.LoanStatusEnum.ToUpper() <> "" Then
				Dim xaMisc As New XaMisc(CurrentStorage.Config.WebsiteConfig)
				xaMisc.UpdateLoan("REFERRED", CurrentStorage.State, CurrentStorage.SubmissionData, False)
			End If
		End Sub

		Protected Overrides Sub SavePersistentData()
			CurrentHttpContext.Session(CurrentStorage.StateSessionID) = CurrentStorage.State
			CurrentHttpContext.Session(CurrentStorage.RequestParamsSessionID) = CurrentStorage.State.RequestParams
		End Sub
		Protected Overrides Sub LoadPersistentData()
			If CurrentHttpContext.Session(CurrentStorage.StateSessionID) IsNot Nothing Then
				Dim inSessionStorage = CType(CurrentHttpContext.Session(CurrentStorage.StateSessionID), TR)
				CurrentStorage.SetState(inSessionStorage)
			End If
		End Sub
	End Class
End Namespace

