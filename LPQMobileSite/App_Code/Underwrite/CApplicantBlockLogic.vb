﻿Imports LPQMobile.BO
Imports LPQMobile.Utils


''' <summary>
''' This contain all customization for each FIs to block aaplication from submitting if applicants do not meet all requirements
''' </summary>
''' <remarks></remarks>
Public Class CApplicantBlockLogic
	Private Shared log As log4net.ILog = log4net.LogManager.GetLogger(GetType(CApplicantBlockLogic))


	''' <summary>
	''' Return a list of reasons why the applicantion can't be submitted
	''' Block application from continuing if applicants do not meet requirements
	''' </summary>
	''' <param name="poConfig"></param>
	''' <param name="poRequest"></param>
	''' <param name="psLoanType"></param>
	''' <returns>html formated messages</returns>
	''' <remarks></remarks>
	Public Shared Function Execute(ByVal poConfig As CWebsiteConfig, ByVal poRequest As System.Web.HttpRequest, ByVal psLoanType As String) As String
		Dim sReasons As String = String.Empty
		Dim flag As Boolean = False

		'log.dabug("Starting ApplicantBlockLogic")

		If String.IsNullOrEmpty(poRequest.Form("LenderId")) And String.IsNullOrEmpty(poRequest.Form("LenderRef")) Then
			Return "LenderRef is missing!"
		End If

		If poConfig.LenderRef.ToUpper.StartsWith("ACHIEVE") Then
			Dim sApprovedStates As String = "CT"
			Dim IsApprovedState As Boolean = sApprovedStates.Contains(Common.SafeString(poRequest.Form("AddressState")).ToUpper)
			Dim IsCoApprovedState As Boolean = True
			If Common.SafeString(poRequest.Form("co_AddressState")) <> "" Then
				IsCoApprovedState = sApprovedStates.Contains(Common.SafeString(poRequest.Form("co_AddressState")).ToUpper)
			End If
			If (Not IsApprovedState Or Not IsCoApprovedState) Then
				sReasons &= "It appears that you are currently located outside Achieve Financial's service area.</br>"
				flag = True
			End If
		End If

        '-------block logic per FI---------
        ' Three Rivers FCU: demo"9B7B4A28  live"79a71bb1dc9d4e
        If poConfig.LenderRef.ToUpper.StartsWith("9B7B4A28") Or poConfig.LenderRef.ToUpper.StartsWith("79A71BB1") Or poConfig.LenderRef.ToUpper.StartsWith("3RIVERS") Then
            'PL <$1000
            Dim IsLOC As Boolean = poRequest.Form("IsLOC") = "Y"
            If psLoanType = "PL" And Not IsLOC And Common.SafeDouble(poRequest.Form("LoanAmount")) < 1000 Then
                sReasons &= "It looks like your desired loan amount is under $1000. Please contact a member of our Lending Team at (260) 490-8328 to better assist you.</br>"
                flag = True
            End If
            'Potential CC Fraud
            Dim sCCPurpose As String = Common.SafeString(poRequest.Form("LoanPurpose")).ToUpper
            Dim sApprovedStates As String = "IN,OH,FL"
            Dim IsApprovedState As Boolean = sApprovedStates.Contains(Common.SafeString(poRequest.Form("AddressState")).ToUpper)
            Dim IsCoApprovedState As Boolean = True
            If Common.SafeString(poRequest.Form("co_AddressState")) <> "" Then
                IsCoApprovedState = sApprovedStates.Contains(Common.SafeString(poRequest.Form("co_AddressState")).ToUpper)
            End If
            If psLoanType = "CC" And sCCPurpose = "NEW CARD" And (Not IsApprovedState Or Not IsCoApprovedState) Then
                sReasons &= "It appears that you are currently located outside 3Rivers' Service Area. Please contact us at (260) 490-8328 to continue with this credit card application. </br>"
                flag = True
            End If
            'VL <$5000
            If psLoanType = "VL" And Common.SafeDouble(poRequest.Form("ProposedLoanAmount")) < 5000 Then
                sReasons &= "It looks like the amount you are trying to borrow is under $5000. Please contact one of our Loan Specialists at (260) 490-8328 for further assistance.</br>"
                flag = True
            End If
        End If


        If poConfig.LenderRef.ToUpper.StartsWith("PROPONENT") Then
			If psLoanType = "VL" And Common.SafeDouble(poRequest.Form("VehicleValue")) < 5000 Then
				sReasons &= "Please process a Personal Loan for this request.</br>"
				flag = True
			End If
		End If
        ''tropicalfcu_test - block states: Hawaii, Mississippi, Oregon, Rhode Island, South Carolina, Texas, Puerto Rico
        If poConfig.LenderRef.ToUpper.StartsWith("TROPICAL") Then
            Dim blockedStates As String = "HI,MS,OR,RI,SC,TX,PR"
            Dim currentState As String = Common.SafeString(poRequest.Form("AddressState")).ToUpper
            Dim coCurrentState As String = Common.SafeString(poRequest.Form("co_AddressState")).ToUpper
            If (currentState <> "" AndAlso blockedStates.Contains(currentState)) Or (coCurrentState <> "" AndAlso blockedStates.Contains(coCurrentState)) Then
				sReasons &= "It appears that you are currently located outside Tropical's service area."
            End If
        End If
       
            If flag Then
                'sReasons = "<b><span style='color: red'>Please input missing field(s):</span></b><br/>" & sReasons
            End If


            If (sReasons <> "") Then
                log.Info("Applicant Blocking Logic reasons: " & sReasons)
                sReasons &= "<div id='MLerrorMessage'></div>"  ''required for client side to display error message
            End If

            Return sReasons
    End Function
End Class
