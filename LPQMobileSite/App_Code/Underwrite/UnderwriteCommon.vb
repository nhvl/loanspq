﻿Imports Microsoft.VisualBasic
Imports System.IO
Imports System.Xml
Imports System.Net
Imports LPQMobile.BO
Imports LPQMobile.DBManager
Imports LPQMobile.Utils
Imports LPQMobile.Utils.Common
Imports System.Net.Json
Imports System.Web
Imports System.Linq
Imports System.Configuration.ConfigurationManager
Imports System.Web.Script.Serialization
Imports System.Text


''' <summary>
''' This UnderwriteCommon contains most methods related to underwrite
''' </summary>
''' <remarks></remarks>
''' 
Public Class UnderwriteCommon
    Private Shared _log As log4net.ILog = log4net.LogManager.GetLogger(GetType(UnderwriteCommon))

#Region "xaCombo underwrite check"
    Private Function isDebitLPQConfig(ByVal settingProductsList As List(Of CProduct)) As Boolean
        Dim bDebitLPQConfig As Boolean = False
        Dim dIndex As Integer
        If settingProductsList.Count > 0 Then
            For dIndex = 0 To settingProductsList.Count - 1
                If (settingProductsList(dIndex) IsNot Nothing AndAlso settingProductsList(dIndex).AutoPullDebitConsumerPrimary) Then
                    Return True
                End If
            Next
        End If
        Return False
    End Function
    ''execute debit
    Public Function executeDebit(ByVal oConfig As CWebsiteConfig, ByVal settingProductsList As List(Of CProduct), ByVal xaID As String, ByVal isJoint As Boolean) As Boolean
        Dim bDebitLPQConfig = isDebitLPQConfig(settingProductsList)
        Dim bDebitLPQMobileWebsiteConfig = oConfig.DebitType
        If bDebitLPQConfig Then
            If bDebitLPQMobileWebsiteConfig = "FIS" Then '' -->> for efund
                Dim sEfundMessage As String = ""
                If Not Common.ExecuteEfund(sEfundMessage, oConfig, xaID, isJoint) Then
                    Return False
                End If

            ElseIf bDebitLPQMobileWebsiteConfig = "EID" Then  '' --> for retailbanking
                Dim sRBankingMessage As String = ""
                If Not Common.ExecuteRetailBanking(sRBankingMessage, oConfig, xaID, isJoint) Then
                    Return False
                End If
            ElseIf bDebitLPQMobileWebsiteConfig = "DID" Then  '' --> for Deluxe detec
                Dim oDeluxe As CDeluxe = New CDeluxe()
                Dim DeluxeDebitURL As String = oConfig.BaseSubmitLoanUrl & "/DeluxeDetection/DeluxeDetect.aspx"
                If Not oDeluxe.Execute(xaID, isJoint, oConfig, DeluxeDebitURL) Then 'Refer or failed, abandon other steps
                    Return False
                End If
            ElseIf bDebitLPQMobileWebsiteConfig = "EWS" Then  '' --> for Experian Precise EWS
                Dim oExperianEWS As CExperianEWS = New CExperianEWS()
                Dim ExperianEWSURL As String = oConfig.BaseSubmitLoanUrl & "/PreciseID/EWS.aspx"
                If Not oExperianEWS.Execute(xaID, isJoint, oConfig, ExperianEWSURL) Then 'Refer or failed, abandon other steps
                    Return False
                End If
            End If
        End If
        ''pass debit or no run debit
        Return True
    End Function
    Public Function isBadMember(ByVal oConfig As CWebsiteConfig, ByVal oRequest As System.Web.HttpRequest) As Boolean
        'check for bad member enable attribute and run result
        Dim strSSN As String = SafeString(oRequest.Form("SSN"))
        Dim strCoSSN As String = SafeString(oRequest.Form("co_SSN"))
        Dim SSNList As New System.Collections.Generic.List(Of String)
        If Not String.IsNullOrEmpty(strSSN) Then
            SSNList.Add(strSSN)
        End If
        If Not String.IsNullOrEmpty(strCoSSN) Then
            SSNList.Add(strCoSSN)
        End If
        Return Common.GetBadMember(SSNList, oConfig)
    End Function
    Public Function isIDALPQConfig(ByVal settingProductsList As List(Of CProduct)) As Boolean
        Dim idaIndex As Integer
        If settingProductsList.Count > 0 Then
            For idaIndex = 0 To settingProductsList.Count - 1
                If (settingProductsList(idaIndex) IsNot Nothing AndAlso settingProductsList(idaIndex).AutoPullIDAuthenticationConsumerPrimary) Then
                    Return True
                End If
            Next
        End If
        Return False
    End Function
    ''------------------implement CreditPull-----------------------------
    Public Function isAutoPullCredit(ByVal settingProductsList As List(Of CProduct)) As Boolean
        Dim cIndex As Integer
        If settingProductsList.Count > 0 Then
            For cIndex = 0 To settingProductsList.Count - 1
                If (settingProductsList(cIndex) IsNot Nothing AndAlso settingProductsList(cIndex).AutoPullCreditsConsumerPrimary) Then
                    Return True
                End If
            Next
        End If
        Return False
    End Function
    Public Function ExecuteCreditPull(ByVal oConfig As CWebsiteConfig, ByVal sLoanID As String) As Boolean
        If SafeString(oConfig.CreditPull).ToUpper <> "Y" Then Return True 'credit pull not set up for this lender
        Dim creditPullURL As String = oConfig.BaseSubmitLoanUrl + "/pullCreditReport/pullCreditReport.aspx"
        Dim creditPullXMLRequest As String = BuildCreditPullXMLRequest(oConfig, sLoanID)
        Dim req As WebRequest = WebRequest.Create(creditPullURL)

        req.Method = "POST"
        req.ContentType = "text/xml"

        _log.Info("XA COMBO: Preparing to POST credit pull Data: " & CSecureStringFormatter.MaskSensitiveXMLData(creditPullXMLRequest))

        Dim writer As New StreamWriter(req.GetRequestStream())
        writer.Write(creditPullXMLRequest)
        writer.Close()

        Dim res As WebResponse = req.GetResponse()

        Dim reader As New StreamReader(res.GetResponseStream())
        Dim sXml As String = reader.ReadToEnd()

        _log.Info("XA COMBO: Response from server(credit pull): " & sXml)

        reader.Close()
        res.GetResponseStream().Close()
        req.GetRequestStream().Close()

        If sXml.Contains("ERROR") Then Return False

        Return True

    End Function
    Private Function BuildCreditPullXMLRequest(ByVal oConfig As CWebsiteConfig, ByVal sLoanID As String) As String
        ' Now construct the XML response
        Dim sb As New StringBuilder()
        Dim writer As XmlWriter = XmlTextWriter.Create(sb)
        writer.WriteStartDocument()

        ' begin input
        writer.WriteStartElement("INPUT")

        writer.WriteStartElement("LOGIN")
        writer.WriteAttributeString("api_user_id", oConfig.APIUser)
        writer.WriteAttributeString("api_password", oConfig.APIPassword)
        writer.WriteEndElement()

        writer.WriteStartElement("REQUEST")
        writer.WriteAttributeString("app_id", sLoanID)
        writer.WriteEndElement()

        writer.WriteEndElement() ' end input
        writer.Flush()
        writer.Close()

        Dim returnStr = sb.ToString()
        Return returnStr
    End Function
    ''------------------end implement CreditPull-----------------------------
    ''------------------implement DecisionXA---------------------------------
    Public Function ExecuteDecisionXA(ByVal oConfig As CWebsiteConfig, ByVal sLoanID As String) As String

        If Not oConfig.IsDecisionXAEnable Then Return True 'DecisionXA not setup for this lender

        Dim decisionXAURL As String = oConfig.BaseSubmitLoanUrl + "/decisionXA/decisionXA.aspx"
        Dim decisionXAXMLRequest As String = BuildDecisionXAXMLRequest(oConfig, sLoanID)
        Dim req As WebRequest = WebRequest.Create(decisionXAURL)

        req.Method = "POST"
        req.ContentType = "text/xml"

        _log.Info("XA COMBO:Preparing to POST decisionXA Data: " & CSecureStringFormatter.MaskSensitiveXMLData(decisionXAXMLRequest))

        Dim writer As New StreamWriter(req.GetRequestStream())
        writer.Write(decisionXAXMLRequest)
        writer.Close()

        Dim res As WebResponse = req.GetResponse()

        Dim reader As New StreamReader(res.GetResponseStream())
        Dim sXml As String = reader.ReadToEnd()

        _log.Info("XA COMBO: Response from server(decisionXA): " & sXml)

        reader.Close()
        res.GetResponseStream().Close()
        req.GetRequestStream().Close()

        Return sXml

    End Function
    Private Function BuildDecisionXAXMLRequest(ByVal oConfig As CWebsiteConfig, ByVal sLoanID As String) As String
        ' Now construct the XML response
        Dim sb As New StringBuilder()
        Dim writer As XmlWriter = XmlTextWriter.Create(sb)
        writer.WriteStartDocument()

        ' begin input
        writer.WriteStartElement("INPUT")
        writer.WriteStartElement("LOGIN")
        writer.WriteAttributeString("api_user_id", oConfig.APIUser)
        writer.WriteAttributeString("api_password", oConfig.APIPassword)
        writer.WriteEndElement() 'end LOGIN

        writer.WriteStartElement("REQUEST")
        writer.WriteAttributeString("xpress_app_id", sLoanID)
        writer.WriteAttributeString("action_type", "UPDATE")
        writer.WriteEndElement() 'end REQUEST

        writer.WriteEndElement() ' end input
        writer.Flush()
        writer.Close()

        Dim returnStr = sb.ToString()
        Return returnStr
    End Function
    Public Function isXAProductQualified(ByVal responseXMLStr As String) As Boolean
        Try

            Dim responseXMLDoc As XmlDocument = New XmlDocument()
            responseXMLDoc.LoadXml(responseXMLStr)
            ' Get the attribute from the XML response
            Dim responseAttribute As String = ""
            Dim responseNode As XmlNode = responseXMLDoc.SelectSingleNode("//DECISION")
            If responseNode Is Nothing Then Return False
            responseAttribute = Common.SafeString(responseNode.Attributes("status").Value)
            ' Compare the result with the proper success result
            If responseAttribute.ToUpper() = "QUALIFIED" Then
                Return True
            Else
                Return False
            End If
        Catch ex As Exception
            _log.Error("XA COMBO: Error processing  decisionXA response: " & ex.Message, ex)
        End Try
        Return False
    End Function

    ''------------------end implement decisionXA-----------------------------
    ''-----------------get Setting ProductList For Loan ----------------------
    Public Function getSettingProductList(ByVal oConfig As CWebsiteConfig, ByVal sAllowedProductCodes As List(Of String), ByVal sRequest As System.Web.HttpRequest) As List(Of CProduct)

        Dim allProdList As List(Of CProduct) = CProduct.GetProductsForLoans(oConfig)
        Dim settingProductsList As New List(Of CProduct)
		If allProdList IsNot Nothing AndAlso allProdList.Any() Then
			'because we don't show product selection section when there is ONLY ONE ConfiguredAccountNode
			Dim getSettingProductsFromSubmission As Boolean = True
			If sAllowedProductCodes IsNot Nothing AndAlso sAllowedProductCodes.Count = 1 Then
				getSettingProductsFromSubmission = False
				Dim cProduct = allProdList.FirstOrDefault(Function(p) p.ProductCode = sAllowedProductCodes(0))
				If cProduct IsNot Nothing Then
					''Don't display product and funding on combo app when there is only ONE product that has no minimum deposit, no services, and no product questions. Do display the product and funding on account that has minimum deposit, services, or product questions. ==> get product data from submission when  there is only ONE product and has minimum deposit, services, or product questions.
					If cProduct.MinimumDeposit > 0 OrElse (cProduct.Services IsNot Nothing AndAlso cProduct.Services.Any()) OrElse (cProduct.CustomQuestions IsNot Nothing AndAlso cProduct.CustomQuestions.Any()) Then
						getSettingProductsFromSubmission = True
					Else
						settingProductsList.Add(cProduct)
					End If
				End If
			End If
			If getSettingProductsFromSubmission = True Then
				Dim oAccountSerializer As New JavaScriptSerializer()
				Dim oProductList As New List(Of SelectProducts)
				If Not String.IsNullOrEmpty(sRequest.Params("SelectedProducts")) Then
					oProductList = oAccountSerializer.Deserialize(Of List(Of SelectProducts))(sRequest.Params("SelectedProducts"))
				End If
				If oProductList.Any() Then
					For Each selectProduct As SelectProducts In oProductList
						Dim settingProduct = allProdList.FirstOrDefault(Function(p) p.ProductCode = selectProduct.productName)
						If settingProduct IsNot Nothing Then
							settingProductsList.Add(settingProduct)
						End If
					Next
				End If
			End If
		End If
		Return settingProductsList
    End Function
    Public Function PreUnderWrite(ByVal oConfig As CWebsiteConfig, ByVal xaRequestXMLDoc As XmlDocument, ByVal oRequest As System.Web.HttpRequest, ByVal isJoint As Boolean) As Boolean
        If oConfig.LenderRef.StartsWith("bcu") Or oConfig.LenderRef.StartsWith("iqcu") Then
            'look for xmlLoan atrribute to match  then   return fail
            If isIRSWithHolding(xaRequestXMLDoc) Or Not isValidDLExpirationDate(oRequest, isJoint) Then
                ''fail PreUnderWrite
                Return False
            End If
            Return True
        End If
        Return True
    End Function
    Private Function isIRSWithHolding(ByVal xaRequestXMLDoc As XmlDocument) As Boolean
        Dim oRequestXML As XmlDocument = New XmlDocument()
        oRequestXML.LoadXml(xaRequestXMLDoc.InnerText)
        Dim questionList As XmlNodeList = oRequestXML.GetElementsByTagName("CUSTOM_QUESTION")
        Dim questionName As String = ""
        Dim questionAnswer As String = ""
        Dim questionType As String = ""
        For Each questionNode As XmlNode In questionList
            questionType = SafeString(questionNode.Attributes("question_type").InnerXml)
            If questionType.ToUpper = "DROPDOWN" Then
                Dim answerNodes As XmlNodeList = questionNode.ChildNodes()
                questionAnswer = SafeString(answerNodes(0).Attributes("answer_value").InnerXml).ToUpper
                questionName = SafeString(questionNode.Attributes("question_name").InnerXml).ToUpper
                If questionName.Contains("IRS_WITHHOLDING") Then 'bcu
                    If questionAnswer = "YES" Then
                        Return True
                    End If
                    Return False
                ElseIf questionName.Contains("CERTIFICATION AND BACKUP WITHHOLDING") Then 'iqcu
                    If questionAnswer = "I AM SUBJECT TO BACKUP WITHHOLDING" Then
                        Return True
                    End If
                    Return False
                End If
            End If
        Next
        Return False
    End Function

    Private Function isValidDLExpirationDate(ByVal oRequest As System.Web.HttpRequest, ByVal isJoint As Boolean) As Boolean
        Dim IDType As String = oRequest.Form("IDCardType")
        Dim maxExpirationDate As New DateTime(2078, 12, 31) ''  "12/31/2078"
        If IDType.ToUpper = "DRIVERS_LICENSE" Then
            If Not String.IsNullOrEmpty(oRequest.Form("IDDateExpire")) Then
                Dim ExpirationDate As DateTime = Date.Parse(oRequest.Form("IDDateExpire"))
                If DateTime.Compare(ExpirationDate, maxExpirationDate) > 0 Then
                    Return False '' is not valid 
                End If
            End If
        End If '
        If Not isJoint Then
            Return True
        Else ''check joint expiration date
            Dim co_IDType As String = oRequest.Form("co_IDCardType")
            If co_IDType.ToUpper = "DRIVERS_LICENSE" Then
                If Not String.IsNullOrEmpty(oRequest.Form("co_IDDateExpire")) Then
                    Dim co_ExpirationDate As DateTime = Date.Parse(oRequest.Form("co_IDDateExpire"))
                    If DateTime.Compare(co_ExpirationDate, maxExpirationDate) > 0 Then
                        Return False '' is not valid 
                    End If
                End If
            End If
        End If
        Return True
    End Function
#End Region

End Class
