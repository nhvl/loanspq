﻿Imports Workflows.XA.BO
Imports LPQMobile.BO
Imports LPQMobile.Utils
Imports LPQMobile.Utils.Common
Imports System.Xml
Imports System.IO
Imports System.Net
Imports System.Text
Public Class CISTWatch

	Private Shared _log As log4net.ILog = log4net.LogManager.GetLogger(GetType(UnderwriteCommon))


	Private Shared Function ConcatBaseSubmitUrl(destinationPath As String, poConfig As CWebsiteConfig) As String
		Dim baseSubmitUrl = poConfig.BaseSubmitLoanUrl
		Return baseSubmitUrl & destinationPath
	End Function

	Public Shared Sub RunBusinessLoanISTWatch(strLoanRequestXML As String, poLoanID As String, poConfig As CWebsiteConfig)
		'' <INPUT><LOGIN api_user_id="1121d7e6-a4b4-4d49-8dde-3b9faa2067d8" api_password="xxxxx" />
		''<REQUEST app_id = "da3c9f9c8a414e5c8239d015f057ca07" run_type="BUSINESS_BENEFICIAL_OWNER" position="1" /></INPUT>
		''response
		''<OUTPUT version="1.0"><RESPONSE><HAS_OFAC>N</HAS_OFAC><RAW_RESPONSE><![CDATA[<?xml version="1.0" encoding="utf-8"?>
		''<ArrayOfWsIstWatch xmlns : xsi ="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
		''<WsIstWatch><ReturnCodes xmlns = "http://www.intelligentsearch.com/HostedWebServices/" > 0</ReturnCodes><ErrorDesc xmlns="http://www.intelligentsearch.com/HostedWebServices/" />
		''<SearchesLeft xmlns = "http://www.intelligentsearch.com/HostedWebServices/" > 235174</SearchesLeft></WsIstWatch></ArrayOfWsIstWatch>]]></RAW_RESPONSE></RESPONSE></OUTPUT>
		Dim istWatchBeneficialOwnerEnable = SafeString(getNodeAttributes(poConfig, "BEHAVIOR", "istwatch_business_loan_beneficial_owner")).ToUpper() = "Y"
		If istWatchBeneficialOwnerEnable Then
			Dim loanRequestXML As XmlDocument = New XmlDocument()
			Dim innerLoanRequestXML As XmlDocument = New XmlDocument()
			loanRequestXML.LoadXml(strLoanRequestXML)
			innerLoanRequestXML.LoadXml(loanRequestXML.InnerText)
			Dim bOwnerNodes As XmlNodeList = innerLoanRequestXML.GetElementsByTagName("BENEFICIAL_OWNER")
			'run istWatch for beneficial owners
			If bOwnerNodes.Count > 0 Then
				Dim bIndex = 0
				For bIndex = 0 To bOwnerNodes.Count - 1
					ExecuteISTWatch(poConfig, poLoanID, Function(writer, isJoint, index)
															writer.WriteAttributeString("run_type", "BUSINESS_BENEFICIAL_OWNER")
															writer.WriteAttributeString("position", index.ToString())
															Return True
														End Function, index:=bIndex)
				Next
			End If
		End If
	End Sub
	Public Shared Sub RunISTWatch(poXaInfo As XaSubmission, poLoanID As String, poConfig As CWebsiteConfig)
		If poXaInfo.Availability = "1b" Or poXaInfo.Availability = "2b" Then	' primary business account / secondary business account
			Dim istWatchBusisnessAppConfigKey As String = "istwatch_primary_business_applicant"
			Dim istWatchBusinessEntityConfigKey As String = "istwatch_primary_business_entity"
			Dim istWatchBusinessOwnerConfigKey As String = "istwatch_primary_beneficial_owner"
			If poXaInfo.Availability.ToLower().Equals("2b") Then
				istWatchBusisnessAppConfigKey = "istwatch_secondary_business_applicant"
				istWatchBusinessEntityConfigKey = "istwatch_secondary_business_entity"
				istWatchBusinessOwnerConfigKey = "istwatch_secondary_beneficial_owner"
			End If
			Dim istWatchBusinessApplicantEnable As String = SafeString(getNodeAttributes(poConfig, "XA_LOAN/BEHAVIOR", istWatchBusisnessAppConfigKey)).ToUpper()
			Dim istWatchBeneficialOwnerEnable As String = SafeString(getNodeAttributes(poConfig, "XA_LOAN/BEHAVIOR", istWatchBusinessOwnerConfigKey)).ToUpper()
			Dim istWatchBusinessEntityEnable As String = SafeString(getNodeAttributes(poConfig, "XA_LOAN/BEHAVIOR", istWatchBusinessEntityConfigKey)).ToUpper()

			'run istwatch for business entity
			If istWatchBusinessEntityEnable = "Y" AndAlso poXaInfo.BABusinessInfo IsNot Nothing AndAlso poXaInfo.BABusinessInfo.AddressStreet <> "" Then
				ExecuteISTWatch(poConfig, poLoanID, Function(writer, isJoint, index)
																	writer.WriteAttributeString("run_type", "BUSINESS_ENTITY")
																	Return True
																End Function)
			End If

			Dim beneficialOwnerIndex As Integer = 0
			Dim applicantIndex As Integer = -1
			For Each baApp In poXaInfo.BAApplicants
				Dim joint As Boolean = baApp.IsJoint.ToUpper().Equals("Y")
				If Not joint Then applicantIndex += 1
				If istWatchBusinessApplicantEnable = "Y" Or baApp.AllowOfac Then
					ExecuteISTWatch(poConfig, poLoanID, Function(writer, isJoint, index)
																		   writer.WriteAttributeString("run_type", "APPLICANT")
																		   writer.WriteAttributeString("applicant_index", index.ToString())
																		   writer.WriteAttributeString("is_joint", IIf(isJoint, "true", "false").ToString())
																		   Return True
																	   End Function, joint, applicantIndex)
				End If
				'run istWatch for beneficial owners
				'If istWatchBeneficialOwnerEnable = "Y" Then
				'	ExecuteISTWatch(Function(writer, isJoint, index)
				'									   writer.WriteAttributeString("run_type", "BUSINESS_BENEFICIAL_OWNER")
				'									   writer.WriteAttributeString("position", index.ToString())
				'									   Return True
				'								   End Function, index:=beneficialOwnerIndex)
				'	beneficialOwnerIndex += 1
				'End If
			Next
			'run istWatch for none applicant
			If istWatchBeneficialOwnerEnable = "Y" AndAlso poXaInfo.NoneApplicantBeneficialOwners IsNot Nothing AndAlso poXaInfo.NoneApplicantBeneficialOwners.Count > 0 Then
				For Each beneficialOwner In poXaInfo.NoneApplicantBeneficialOwners
					ExecuteISTWatch(poConfig, poLoanID, Function(writer, isJoint, index)
																		   writer.WriteAttributeString("run_type", "BUSINESS_BENEFICIAL_OWNER")
																		   writer.WriteAttributeString("position", index.ToString())
																		   Return True
																	   End Function, index:=beneficialOwnerIndex)
					beneficialOwnerIndex += 1
				Next
			End If
		ElseIf poXaInfo.Availability = "1s" Or poXaInfo.Availability = "2s" Then	' primary special account / secondary special account
			Dim istWatchSpecialApplicantConfigKey As String = "istwatch_primary_special_applicant"
			Dim istWatchSpecialEntityConfigKey As String = "istwatch_primary_special_entity"
			If poXaInfo.Availability = "2s" Then
				istWatchSpecialApplicantConfigKey = "istwatch_secondary_special_applicant"
				istWatchSpecialEntityConfigKey = "istwatch_secondary_special_entity"
			End If
			Dim istWatchSpecialApplicantEnable As String = SafeString(getNodeAttributes(poConfig, "XA_LOAN/BEHAVIOR", istWatchSpecialApplicantConfigKey)).ToUpper()
			Dim istWatchSpecialEntityEnable As String = SafeString(getNodeAttributes(poConfig, "XA_LOAN/BEHAVIOR", istWatchSpecialEntityConfigKey)).ToUpper()

			If poXaInfo.SAAccountInfo IsNot Nothing AndAlso poXaInfo.SAAccountInfo.AddressStreet <> "" AndAlso istWatchSpecialEntityEnable = "Y" Then
				'run istWatch for special info
				ExecuteISTWatch(poConfig, poLoanID, Function(writer, isJoint, index)
																	writer.WriteAttributeString("run_type", "SPECIAL_ENTITY")
																	Return True
																End Function)
			End If

			If poXaInfo.SAApplicants IsNot Nothing AndAlso poXaInfo.SAApplicants.Count > 0 Then
				'in case there are roles in selected special account, run IST Watch for those corresponding applicants
				Dim applicantIndex As Integer = -1
				For Each saApp In poXaInfo.SAApplicants
					Dim joint As Boolean = saApp.IsJoint.ToUpper().Equals("Y")
					If Not joint Then applicantIndex += 1
					If istWatchSpecialApplicantEnable = "Y" Or saApp.AllowOfac Then
						ExecuteISTWatch(poConfig, poLoanID, Function(writer, isJoint, index)
																				  writer.WriteAttributeString("run_type", "APPLICANT")
																				  writer.WriteAttributeString("applicant_index", index.ToString())
																				  writer.WriteAttributeString("is_joint", IIf(isJoint, "true", "false").ToString())
																				  Return True
																			  End Function, joint, applicantIndex)
					End If
				Next
			Else
				'in case there is no role in selected special account, run IST Watch for primary app and joint app(if any)
				If istWatchSpecialApplicantEnable = "Y" Then
					ExecuteISTWatch(poConfig, poLoanID, Function(writer, isJoint, index)
																		   writer.WriteAttributeString("run_type", "APPLICANT")
																		   writer.WriteAttributeString("applicant_index", "0")
																		   writer.WriteAttributeString("is_joint", "false")
																		   Return True
																	   End Function)
					If poXaInfo.CoAppPersonal IsNot Nothing Then
						ExecuteISTWatch(poConfig, poLoanID, Function(writer, isJoint, index)
																				  writer.WriteAttributeString("run_type", "APPLICANT")
																				  writer.WriteAttributeString("applicant_index", "0")
																				  writer.WriteAttributeString("is_joint", "true")
																				  Return True
																			  End Function)
					End If
				End If
			End If
		ElseIf poXaInfo.Availability = "1" Or poXaInfo.Availability = "2" Then ' primary account / ' secondary account
			Dim istWatchApplicantConfigKey As String = "istwatch_primary_applicant"
			If poXaInfo.Availability = "2" Then istWatchApplicantConfigKey = "istwatch_secondary_applicant"
			Dim istWatchApplicantEnable As String = SafeString(getNodeAttributes(poConfig, "XA_LOAN/BEHAVIOR", istWatchApplicantConfigKey)).ToUpper()
			If istWatchApplicantEnable = "Y" Then
				ExecuteISTWatch(poConfig, poLoanID, Function(writer, isJoint, index)
																	writer.WriteAttributeString("run_type", "APPLICANT")
																	writer.WriteAttributeString("applicant_index", "0")
																	writer.WriteAttributeString("is_joint", "false")
																	Return True
																End Function)
				If poXaInfo.CoAppPersonal IsNot Nothing Then
					ExecuteISTWatch(poConfig, poLoanID, Function(writer, isJoint, index)
																		   writer.WriteAttributeString("run_type", "APPLICANT")
																		   writer.WriteAttributeString("applicant_index", "0")
																		   writer.WriteAttributeString("is_joint", "true")
																		   Return True
																	   End Function)
				End If
			End If
		End If
	End Sub

	Private Shared Function ExecuteISTWatch(poConfig As CWebsiteConfig, poLoanID As String, writeCustomAttrFunc As Func(Of XmlWriter, Boolean, Integer, Boolean), Optional isJoint As Boolean = False, Optional index As Integer = 0) As Boolean
		Dim requestXml As String = BuildeISTWatchRequestXml(poConfig, poLoanID, writeCustomAttrFunc, isJoint, index)
		Dim submitISTWatchURL = ConcatBaseSubmitUrl("/ISTWatch/ISTWatch.aspx", poConfig)
		Dim req As WebRequest = WebRequest.Create(submitISTWatchURL)
		req.Method = "POST"
		req.ContentType = "text/xml"
		_log.Info("Preparing to POST IST Watch Data: " & CSecureStringFormatter.MaskSensitiveXMLData(requestXml))
		_log.Info("Begin request to " & submitISTWatchURL)
		Dim writer As New StreamWriter(req.GetRequestStream())
		writer.Write(requestXml)
		writer.Close()

		Dim res As WebResponse = req.GetResponse()

		Dim reader As New StreamReader(res.GetResponseStream())
		Dim sXml As String = reader.ReadToEnd()

		_log.Info("Response from server(IST Watch): " & sXml)

		reader.Close()
		res.GetResponseStream().Close()
		req.GetRequestStream().Close()

		'If sXml.Contains("ERROR") Then Return False
		Return True
	End Function

	Private Shared Function BuildeISTWatchRequestXml(poConfig As CWebsiteConfig, poLoanID As String, writeCustomAttrFunc As Func(Of XmlWriter, Boolean, Integer, Boolean), Optional isJoint As Boolean = False, Optional index As Integer = 0) As String
		' Now construct the XML response
		Dim sb As New StringBuilder()
		Dim writer As XmlWriter = XmlTextWriter.Create(sb)
		writer.WriteStartDocument()

		' begin input
		writer.WriteStartElement("INPUT")
		writer.WriteStartElement("LOGIN")
		writer.WriteAttributeString("api_user_id", poConfig.APIUser)
		writer.WriteAttributeString("api_password", poConfig.APIPassword)
		writer.WriteEndElement()

		writer.WriteStartElement("REQUEST")
		'writer.WriteAttributeString("app_id", loanID)
		writer.WriteAttributeString("app_id", poLoanID)
		writeCustomAttrFunc(writer, isJoint, index)
		writer.WriteEndElement()
		' end input
		writer.WriteEndElement()
		writer.Flush()
		writer.Close()
		Dim returnStr = sb.ToString()
		Return returnStr
	End Function



End Class
