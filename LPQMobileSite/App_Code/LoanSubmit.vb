﻿Imports LPQMobile.BO
Imports LPQMobile.Utils
Imports System.Xml
Imports System.Xml.Serialization
Imports System.Threading
Imports System.IO
Imports System.Web.Script.Serialization
Imports System.Net
Imports System.Web
Imports System.Text
Public Class LoanSubmit
	Private Shared log As log4net.ILog = log4net.LogManager.GetLogger(GetType(Common))

	Public Shared Function BuildSubmitNewXAXML(ByVal poConfig As CWebsiteConfig, psCDATA As String) As String
		Dim sb As New StringBuilder
		Dim writer As XmlWriter = XmlTextWriter.Create(sb)

		writer.WriteStartDocument()
		writer.WriteStartElement("INPUT")

		'begin login
		writer.WriteStartElement("LOGIN")
		writer.WriteAttributeString("api_password", poConfig.APIPassword)
		writer.WriteAttributeString("api_user_id", poConfig.APIUser)
		writer.WriteEndElement()

		'' begin REQUEST
		writer.WriteStartElement("REQUEST")
		writer.WriteAttributeString("action_type", "NEW")

		' Begin LOAN_DATA
		writer.WriteStartElement("LOAN_DATA")
		writer.WriteAttributeString("loan_type", "XA")
		If psCDATA <> "" Then
			writer.WriteCData(psCDATA)
		End If
		' end LOAN_DATA
		writer.WriteEndElement()

		' end REQUEST
		writer.WriteEndElement()

		' End INPUT
		writer.WriteEndElement()
		writer.Flush()
		writer.Close()

		Dim xmlStr As String = ""

		xmlStr = sb.ToString()

		Return xmlStr
	End Function

	''Create the completed clf(UPDATE) ready for posting to submitloan
	Public Shared Function BuildUpdateRequestXmlDoc(ByVal poConfig As CWebsiteConfig, poCDATA As XmlDocument) As XmlDocument
		Dim sLoanID As String = ""
		Dim sAppType As String = ""
		Dim oLPQCLF As CXmlElement
		oLPQCLF = New CXmlElement(poCDATA.DocumentElement)
		'need to have this inorder to select
		oLPQCLF.AddNamespace("default", "http://www.meridianlink.com/CLF")
		oLPQCLF.CurrentNamespace = "default"
		Dim oSYSTEM As CXmlElement = oLPQCLF.SelectSingleElement("SYSTEM")
		sLoanID = oSYSTEM.GetAttribute("d2p1:loan_id")
		sAppType = poCDATA.DocumentElement.Name	' "PERSONAL_LOAN" , "VEHICLE_LOAN"

		Dim sSubmitNewXAXmlStr As String = BuildSubmitNewXAXML(poConfig, Nothing)
		Dim oUpdateLoanRequestXmlDoc As New XmlDocument
		oUpdateLoanRequestXmlDoc.LoadXml(sSubmitNewXAXmlStr)
		Dim cdataSection As XmlCDataSection
		cdataSection = oUpdateLoanRequestXmlDoc.CreateCDataSection(poCDATA.InnerXml)
		oUpdateLoanRequestXmlDoc.SelectSingleNode("//LOAN_DATA").AppendChild(cdataSection)

		' Add the loan_id to LOAN_DATA
		Dim loanIDAttributeElement As XmlElement = CType(oUpdateLoanRequestXmlDoc.SelectSingleNode("//REQUEST"), XmlElement)
		loanIDAttributeElement.SetAttribute("loan_id", sLoanID)

		''update loan type and action
		Dim loanDataElement As XmlElement = CType(oUpdateLoanRequestXmlDoc.SelectSingleNode("//LOAN_DATA"), XmlElement)
		loanDataElement.SetAttribute("loan_type", ConverAppType2Short(sAppType))
		loanDataElement = CType(oUpdateLoanRequestXmlDoc.SelectSingleNode("//REQUEST"), XmlElement)
		loanDataElement.SetAttribute("action_type", "UPDATE")


		Return oUpdateLoanRequestXmlDoc
	End Function

	Public Shared Function ConverAppType2Short(ByVal sLongAppType As String) As String
		Select Case sLongAppType.ToUpper

			Case "PERSONAL_LOAN"
				Return "PL"
			Case "VEHICLE_LOAN"
				Return "VL"
			Case "CREDITCARD_LOAN"
				Return "CC"
			Case "MORTGAGE_LOAN"
				Return "HE"
			Case "XPRESS_LOAN"
				Return "XA"
			Case Else
				Return ""
		End Select
	End Function

	''' <summary>
	''' combo mode update loan status
	''' </summary>
	''' <param name="psLoanID"></param>
	''' <param name="poConfig"></param>
	''' <param name="psStatus"></param>
	''' <remarks></remarks>
	Public Shared Sub UpdateLoan(ByVal psLoanID As String, ByVal poConfig As CWebsiteConfig, Optional ByVal psStatus As String = "", Optional ByVal psXALoanNumber As String = "", Optional ByVal additionalComments As String = "")
		log.InfoFormat("UpdateLoan called for loan_id '{0}', with psStatus='{1}', XA loan_number='{2}'", psLoanID, psStatus, psXALoanNumber)
		' Make changes to it
		' Submit the changes with the "UPDATE" command

		If psLoanID = "" Then Return ' this is used for loan only
		'TODO: partition all URL creation into a enum
		Const constGetLoansURL As String = "/GetLoans/GetLoans.aspx"
		Dim url As String = poConfig.BaseSubmitLoanUrl + constGetLoansURL

		Dim getLoansXMLRequestStr As String = LoanRetrieval.BuildGetLoansXML(psLoanID, poConfig.APIUser, poConfig.APIPassword)

		'    ' Done building the XML, now ship it off
		Dim req As WebRequest = WebRequest.Create(url)

		req.Method = "POST"
		req.ContentType = "text/xml"

		CPBLogger.logInfo("Preparing to POST GetLoans Request: " & CSecureStringFormatter.MaskSensitiveXMLData(getLoansXMLRequestStr))

		Dim writer As New StreamWriter(req.GetRequestStream())
		writer.Write(getLoansXMLRequestStr)
		writer.Close()

		Dim res As WebResponse = req.GetResponse()

		Dim reader As New StreamReader(res.GetResponseStream())
		Dim sXml As String = reader.ReadToEnd()

		log.Info("GetLoans Response: " & CSecureStringFormatter.MaskSensitiveXMLData(sXml))

		' Extract the inner xml and update the status of the loans 
		Dim responseXMLDoc As XmlDocument = New XmlDocument()
		responseXMLDoc.LoadXml(sXml)
		Dim innerXMLStr As String = ""
		innerXMLStr = responseXMLDoc.InnerText
		Dim innerXMLDoc As XmlDocument = New XmlDocument()
		innerXMLDoc.LoadXml(innerXMLStr)

		'TODO: refactor or simplify, tobe remove
		'Dim childNodeList As XmlNodeList = innerXMLDoc.DocumentElement.ChildNodes
		'For Each childNode As XmlNode In childNodeList
		'	If (childNode.Name.ToUpper() = "LOAN_STATUS") Then
		'		Dim tempElement As XmlElement = CType(childNode, XmlElement)
		'		If psStatus.ToUpper = "PEN" Then
		'			tempElement.SetAttribute("loan_status", "PEN")
		'		ElseIf psStatus.ToUpper = "AA" Then
		'			tempElement.SetAttribute("loan_status", "AA")
		'		ElseIf psStatus.ToUpper = "XAPASSED" Or psStatus.ToUpper = "XAFAILED" Then
		'			'don't change status but update decision comments
		'		Else
		'			tempElement.SetAttribute("loan_status", "REF")
		'		End If
		'		'' set the approval/denial date to current date
		'		'tempElement.SetAttribute("approval_date", common.SafeDateTimeXml(Now.ToString()))

		'		'dont want to alter the time
		'		tempElement.RemoveAttribute("response_date")
		'		tempElement.RemoveAttribute("app_receive_date")
		'		Exit For
		'	End If
		'Next

		''update status
		Dim loanStatusNodes As XmlNodeList = innerXMLDoc.GetElementsByTagName("LOAN_STATUS")
		Dim loanStatusNode As XmlElement = loanStatusNodes(0)
		If psStatus.ToUpper = "PEN" Or psStatus.ToUpper = "PENXANOTPROCEED" Then
			loanStatusNode.SetAttribute("loan_status", "PEN")
		ElseIf psStatus.ToUpper = "AA" Or psStatus.ToUpper = "AA_XAFAILED" Then
			loanStatusNode.SetAttribute("loan_status", "AA")
		ElseIf psStatus.ToUpper.StartsWith("LOAN_IDA") Or psStatus.ToUpper = "XAPASSED" Or psStatus.ToUpper = "XAFAILED" Or psStatus.ToUpper = "XAPROCEED" Or psStatus.ToUpper = "XANOTPROCEED" Or psStatus.ToUpper = "LOANDECLINED_XAPROCEED" Or psStatus.ToUpper = "LOANDECLINED_XANOTPROCEED" Or psStatus.ToUpper = "REFERREDLOAN_XANOTPROCEED" Then
			'don't change status but update decision comments
		Else
			loanStatusNode.SetAttribute("loan_status", "REF")
		End If
		'dont want to alter the time
		loanStatusNode.RemoveAttribute("response_date")
		loanStatusNode.RemoveAttribute("app_receive_date")


		'add reason to decision comment section so officier know what happen
		Dim sComment As String = ""	'Environment.NewLine
		sComment &= "SYSTEM (" & Now.ToShortDateString & "  " & Now.ToShortTimeString & "): "
		If psStatus.ToUpper = "PEN" Then
			sComment &= "STATUS CHANGED TO PENDING WHILE WAITING FOR APPLICANT TO COMPLETE ID AUTHENTICATION.  CONSUMER WANTS TO PROCEED WITH MEMBERSHIP APPLICATION " & psXALoanNumber & " EVEN WHEN THERE IS NO QUALIFY PRODUCT."
		ElseIf psStatus.ToUpper = "PENXANOTPROCEED" Then
			sComment &= "STATUS CHANGED TO PENDING WHILE WAITING FOR APPLICANT TO COMPLETE ID AUTHENTICATION.  CONSUMER DOES NOT WANT TO PROCEED WITH MEMBERSHIP APPLICATION " & psXALoanNumber & " UNLESS LOAN IS APPROVED"
		ElseIf psStatus.ToUpper = "AA" Then	'Expect XA number
			sComment &= "STATUS REVERTED BACK TO INSTANT APPROVED AFTER APPLICANT COMPLETED AND PASSED ID AUTHENTICATION & DEBIT BUREAU FOR MEMBERSHIP APPLICATION " & psXALoanNumber
		ElseIf psStatus.ToUpper = "AA_XAFAILED" Then
			sComment &= "STATUS REVERTED BACK TO INSTANT APPROVED BUT MEMBERSHIP APPLICATION " & psXALoanNumber & " HAS BEEN REFERRED"
		ElseIf psStatus.ToUpper = "XAPASSED" Then
			Dim sStatusText = "INSTANT APPROVED"
			''get loan status from config
			If poConfig.LoanStatusEnum <> "" Then
				If poConfig.LoanStatusEnum.ToUpper = "APPROVED" Then
					sStatusText = "APPROVED"
				ElseIf poConfig.LoanStatusEnum.ToUpper = "AP" Then
					poConfig.LoanStatusEnum = "APPROVED PENDING"
				ElseIf poConfig.LoanStatusEnum = "REFERRED" Then
					sStatusText = "REFERRED"
				End If
			End If
			sComment &= "MEMBERSHIP APPLICATION " & psXALoanNumber & " HAS BEEN " & sStatusText
		ElseIf psStatus.ToUpper = "XAFAILED" Then
			If psXALoanNumber <> "" Then
				sComment &= "MEMBERSHIP APPLICATION " & psXALoanNumber & " HAS BEEN REFERRED"
			Else
				sComment &= "MEMBERSHIP APPLICATION HAS BEEN REFERRED"
			End If
		ElseIf psStatus.ToUpper = "XAPROCEED" Then
			sComment &= "NO QUALIFY PRODUCT BUT CONSUMER WANTS TO PROCEED WITH MEMBERSHIP APPLICATION " & psXALoanNumber
		ElseIf psStatus.ToUpper = "XANOTPROCEED" Then
			If psXALoanNumber <> "" Then
				sComment &= "NO QUALIFY PRODUCT AND CONSUMER DOES NOT WANT TO PROCEED WITH MEMBERSHIP APPLICATION " & psXALoanNumber & " UNLESS LOAN IS APPROVED"
			Else
				sComment &= "NO QUALIFY PRODUCT AND CONSUMER DOES NOT WANT TO PROCEED WITH MEMBERSHIP APPLICATION UNLESS LOAN IS APPROVED"
			End If
		ElseIf psStatus.ToUpper = "REFERREDLOAN_XANOTPROCEED" Then
			'sComment &= "NO QUALIFY PRODUCT SO OUR SYSTEM IS NOT CONTINUE TO PROCEED WITH MEMBERSHIP APPLICATION UNLESS LOAN IS APPROVED"
			sComment &= "IN COMBO MODE, SYSTEM IS CONFIGURED TO NOT AUTO CREATE MEMBERSHIP APPLICATION UNLESS LOAN IS APPROVED "
			''add XANOTPROCEEDComment
			If additionalComments <> "" Then
				sComment &= additionalComments.Replace("/n", Environment.NewLine).Replace("<br/>", "")
			End If
		ElseIf psStatus.ToUpper() = "REFERREDLOAN_BOOKING_VALIDATION" Then
            sComment &= "Booking Validation Rule (" & additionalComments & ") trigger. Booking not run and status change to REFERRED."
        ElseIf psStatus.ToUpper = "LOANDECLINED_XAPROCEED" Then
			sComment &= "LOAN IS DECLINED BUT CONSUMER WANTS TO PROCEED WITH MEMBERSHIP APPLICATION " & psXALoanNumber
		ElseIf psStatus.ToUpper = "LOANDECLINED_XANOTPROCEED" Then
			sComment &= "NO XA CREATED BECAUSE LOAN IS DECLINED AND CONSUMER DOES NOT WANT TO PROCEED WITH MEMBERSHIP APPLICATION"
		ElseIf psStatus.ToUpper = "LOAN_IDA_FAILED" Then
			sComment &= "RUN LOAN IDA WHEN XA IS NOT CREATED: IDA FAILED. "
		ElseIf psStatus.ToUpper = "LOAN_IDA_PASSED" Then
            sComment &= "RUN LOAN IDA WHEN XA IS NOT CREATED: IDA PASSED. "
        Else
			If psXALoanNumber <> "" Then
				sComment &= "STATUS CHANGED TO REFERRED BECAUSE MEMBERSHIP APPLICATION " & psXALoanNumber & " IS NOT INSTANT APPROVED"
			Else
				sComment &= "STATUS CHANGED TO REFERRED BECAUSE MEMBERSHIP APPLICATION IS NOT INSTANT APPROVED"
			End If

		End If

		sComment &= Environment.NewLine
		sComment &= Environment.NewLine

		'TODO: refactor or simplify, tobe delete
		'For Each childNode As XmlNode In childNodeList
		'	If (childNode.Name.ToUpper() = "COMMENTS") Then
		'		Dim childNodeList2 As XmlNodeList = childNode.ChildNodes
		'		For Each childNode2 As XmlNode In childNodeList2
		'			If (childNode2.Name.ToUpper() = "DECISION_COMMENTS") Then
		'				childNode2.InnerText = sComment & childNode2.InnerText
		'				Exit For
		'			End If
		'		Next
		'		Exit For
		'	End If
		'Next

		Dim decisionCommentsNodes As XmlNodeList = innerXMLDoc.GetElementsByTagName("DECISION_COMMENTS")
		Dim decisionCommentsNode As XmlElement = decisionCommentsNodes(0)
		decisionCommentsNode.InnerText = sComment & decisionCommentsNode.InnerText

		'buil xml clf for posting
		Dim UpdateLoanRequestXmlDoc As XmlDocument = LoanSubmit.BuildUpdateRequestXmlDoc(poConfig, innerXMLDoc)
		log.Info("Preparing to Update loan data: " & CSecureStringFormatter.MaskSensitiveXMLData(UpdateLoanRequestXmlDoc.InnerXml))

		Dim submitLoanUrl As String = poConfig.BaseSubmitLoanUrl + "/SubmitLoan/SubmitLoan.aspx"
		log.Info("start request to " & submitLoanUrl)
		' Now send the loan request back with the updated "status, 
		req = WebRequest.Create(submitLoanUrl)
		req.Method = "POST"
		req.ContentType = "text/xml"
		writer = New StreamWriter(req.GetRequestStream())
		writer.Write(UpdateLoanRequestXmlDoc.InnerXml)
		writer.Close()

		res = req.GetResponse()

		reader = New StreamReader(res.GetResponseStream())
		sXml = reader.ReadToEnd()
		log.Info("Update loan response: " + sXml)
	End Sub

	''' <summary>
	''' Update Xa to instant approved in combomode
	''' </summary>
	''' <param name="psXaID"></param>
	''' <param name="poConfig"></param>
	''' <param name="psStatus"></param>
	''' <remarks></remarks>
    Public Shared Sub UpdateXA(ByVal psXaID As String, ByVal poConfig As CWebsiteConfig, Optional psStatus As String = "", Optional psFundingComment As String = "")
        ' Make changes to it
        ' Submit the changes with the "UPDATE" command

        If psXaID = "" Then Return
        'TODO: partition all URL creation into a enum
        Const constGetLoansURL As String = "/GetLoans/GetLoans.aspx"
        Dim url As String = poConfig.BaseSubmitLoanUrl + constGetLoansURL

        Dim getLoansXMLRequestStr As String = LoanRetrieval.BuildGetLoansXML(psXaID, poConfig.APIUser, poConfig.APIPassword)

        '    ' Done building the XML, now ship it off
        Dim req As WebRequest = WebRequest.Create(url)

        req.Method = "POST"
        req.ContentType = "text/xml"

        log.Info("Preparing to POST GetLoans Request: " & CSecureStringFormatter.MaskSensitiveXMLData(getLoansXMLRequestStr))

        Dim writer As New StreamWriter(req.GetRequestStream())
        writer.Write(getLoansXMLRequestStr)
        writer.Close()

        Dim res As WebResponse = req.GetResponse()

        Dim reader As New StreamReader(res.GetResponseStream())
        Dim sXml As String = reader.ReadToEnd()

        log.Info("GetLoans Response: " & CSecureStringFormatter.MaskSensitiveXMLData(sXml))

        ' Extract the inner xml and update the status of the loans 
        Dim responseXMLDoc As XmlDocument = New XmlDocument()
        responseXMLDoc.LoadXml(sXml)
        Dim innerXMLStr As String = ""
        innerXMLStr = responseXMLDoc.InnerText
        Dim innerXMLDoc As XmlDocument = New XmlDocument()
        innerXMLDoc.LoadXml(innerXMLStr)

        'TODO: refactor or simplify
        'Dim childNodeList As XmlNodeList = innerXMLDoc.DocumentElement.ChildNodes
        'For Each childNode As XmlNode In childNodeList
        '	If (childNode.Name.ToUpper() = "LOAN_INFO") Then
        '		Dim tempElement As XmlElement = CType(childNode, XmlElement)
        '		tempElement.SetAttribute("status", "INSTANTAPPROVED")
        '		'' set the approval/denial date to current date
        '		tempElement.SetAttribute("approval_date", Common.SafeDateTimeXml(Now.ToString()))
        '		Exit For
        '	End If
        'Next

        '---Update status
        Dim loanInfoNodes As XmlNodeList = innerXMLDoc.GetElementsByTagName("LOAN_INFO")
        Dim loanInfoNode As XmlElement = loanInfoNodes(0)
        If "FRAUD" = loanInfoNode.Attributes("status").InnerXml Or "DUP" = loanInfoNode.Attributes("status").InnerXml Then
            Return ' don't update status when credit report has fraud warning or status is fraud or DUP
        End If
        Dim sLoanStatus As String = "INSTANTAPPROVED"
        Dim sStatusText As String = "INSTANT APPROVED"
        ''get loan status from config
        If poConfig.LoanStatusEnum <> "" Then
            sLoanStatus = poConfig.LoanStatusEnum
            If sLoanStatus.ToUpper = "APPROVED" Then
                sStatusText = "APPROVED"
            ElseIf sLoanStatus.ToUpper = "AP" Then
                sStatusText = "APPROVED PENDING"
            ElseIf sLoanStatus = "REFERRED" Then
                sStatusText = "REFERRED"
            End If
        End If
        Dim sLoanComment As String = "STATUS CHANGED TO " & sStatusText & " AFTER PASSING UNDERWRITING"
        If psStatus = "REFERRED" Then
            sLoanStatus = "REFERRED"
            sLoanComment = "STATUS CHANGED TO REFERRED;BOOKING OR FUNDING IS NOT SUCCESS"
        End If
        If psStatus <> "REFERRED" And poConfig.IsDecisionXAEnable Then
            ''update clf_funding_source_id in approve account
            Dim accountTypes As XmlNodeList = innerXMLDoc.GetElementsByTagName("ACCOUNT_TYPE")
            Dim sFundingType As String = Common.getSelectedFundingType()
            For Each accountType As XmlElement In accountTypes
                If accountType.ParentNode.Name <> "APPROVED_ACCOUNTS" Then Continue For
                Select Case sFundingType
                    Case "CASH"
                        accountType.SetAttribute("clf_funding_source_id", "0")
                    Case "MAIL"
                        accountType.SetAttribute("clf_funding_source_id", "1")
                    Case "NOT_FUNDING"
                        accountType.SetAttribute("clf_funding_source_id", "2")
                    Case "PAYPAL", "INTERNALTRANSFER", "BANK", "CREDITCARD"
                        accountType.SetAttribute("clf_funding_source_id", "3")
                    Case Else
                        accountType.SetAttribute("clf_funding_source_id", "10")
                End Select
            Next
        End If
        loanInfoNode.SetAttribute("status", sLoanStatus)
        '' set the approval/denial date to current date
        loanInfoNode.SetAttribute("approval_date", Common.SafeDateTimeXml(Now.ToString()))


        'add reason to decision comment section
        Dim sComment As String = "" 'Environment.NewLine
        sComment &= "SYSTEM (" & Now.ToShortDateString & "  " & Now.ToShortTimeString & "): "
        sComment &= sLoanComment
        sComment &= Environment.NewLine
        sComment &= Environment.NewLine

        If psFundingComment <> "" Then
            sComment &= "SYSTEM (" & Now.ToShortDateString & "  " & Now.ToShortTimeString & "): "
            sComment &= psFundingComment
            sComment &= Environment.NewLine
            sComment &= Environment.NewLine
        End If
        'TODO: refactor or simplify
        'For Each childNode As XmlNode In childNodeList
        '	If (childNode.Name.ToUpper() = "COMMENTS") Then
        '		Dim childNodeList2 As XmlNodeList = childNode.ChildNodes
        '		For Each childNode2 As XmlNode In childNodeList2
        '			If (childNode2.Name.ToUpper() = "INTERNAL_COMMENTS") Then
        '				childNode2.InnerText = sComment & childNode2.InnerText
        '				Exit For
        '			End If
        '		Next
        '		Exit For
        '	End If
        'Next

        Dim internalCommentsNodes As XmlNodeList = innerXMLDoc.GetElementsByTagName("INTERNAL_COMMENTS")
        Dim internalCommentsNode As XmlElement = internalCommentsNodes(0)
        internalCommentsNode.InnerText = sComment & internalCommentsNode.InnerText

        ''''update Date
        Dim ApprovedNodeList As XmlNodeList
        ApprovedNodeList = innerXMLDoc.GetElementsByTagName("ACCOUNT_TYPE")
        Dim DateElement As XmlElement
        For Each childNode As XmlNode In ApprovedNodeList
            DateElement = CType(childNode, XmlElement)
            DateElement.SetAttribute("issue_date", Common.SafeDateXml(Now.ToString()))
        Next


        'buil xml clf for posting
        Dim UpdateLoanRequestXmlDoc As XmlDocument = LoanSubmit.BuildUpdateRequestXmlDoc(poConfig, innerXMLDoc)
        log.Info("Preparing to Update XA data: " & CSecureStringFormatter.MaskSensitiveXMLData(UpdateLoanRequestXmlDoc.InnerXml))


        Dim submitLoanUrl As String = poConfig.BaseSubmitLoanUrl + "/SubmitLoan/SubmitLoan.aspx"
        log.Info("start request to " & submitLoanUrl)
        ' Now send the loan request back with the updated "status, 
        req = WebRequest.Create(submitLoanUrl)
        req.Method = "POST"
        req.ContentType = "text/xml"
        writer = New StreamWriter(req.GetRequestStream())
        writer.Write(UpdateLoanRequestXmlDoc.InnerXml)
        writer.Close()

        res = req.GetResponse()

        reader = New StreamReader(res.GetResponseStream())
        sXml = reader.ReadToEnd()
        log.Info("Update XA response: " + sXml)
    End Sub

    'deprecated, seem like the code is in common file
    'Public Shared Function getBookingNumber(ByVal _CurrentWebsiteConfig As CWebsiteConfig, ByVal poResponse As String) As String
    '    Dim sbookingNumber As String = ""

    '    Dim symitarBookingURL As String = _CurrentWebsiteConfig.BaseSubmitLoanUrl + "/SymitarBookService/XABook.aspx"
    '    Dim DNABookingURL As String = _CurrentWebsiteConfig.BaseSubmitLoanUrl + "/bookapp/bookapp.aspx"
    '    Dim loanID As String = Common.getResponseLoanID(poResponse)
    '    If _CurrentWebsiteConfig.Booking.ToUpper() <> "" Or _CurrentWebsiteConfig.IsFundingEnable Then

    '        If _CurrentWebsiteConfig.Booking.ToUpper() = "SYMITAR" Then
    '            Dim oSymitar As CSymitar = New CSymitar
    '            sbookingNumber = oSymitar.Book(loanID, _CurrentWebsiteConfig, symitarBookingURL)

    '        ElseIf _CurrentWebsiteConfig.Booking.ToUpper() = "DNA" Then
    '            Dim oDNACore As CDNACore = New CDNACore
    '            sbookingNumber = oDNACore.Book(loanID, _CurrentWebsiteConfig, DNABookingURL)
    '        End If
    '    End If

    '    If String.IsNullOrEmpty(sbookingNumber) Then
    '        sbookingNumber = Common.getLoanNumber(poResponse)
    '    End If

    '    Return sbookingNumber
    'End Function
    Public Shared Function getLoanStatus(ByVal psLoanID As String, ByVal poConfig As CWebsiteConfig) As String

        If psLoanID = "" Then Return ""
        'TODO: partition all URL creation into a enum
        Const constGetLoansURL As String = "/GetLoans/GetLoans.aspx"
        Dim url As String = poConfig.BaseSubmitLoanUrl + constGetLoansURL

        Dim getLoansXMLRequestStr As String = LoanRetrieval.BuildGetLoansXML(psLoanID, poConfig.APIUser, poConfig.APIPassword)

        '    ' Done building the XML, now ship it off
        Dim req As WebRequest = WebRequest.Create(url)

        req.Method = "POST"
        req.ContentType = "text/xml"

        log.Info("Preparing to POST GetLoans Request: " & CSecureStringFormatter.MaskSensitiveXMLData(getLoansXMLRequestStr))

        Dim writer As New StreamWriter(req.GetRequestStream())
        writer.Write(getLoansXMLRequestStr)
        writer.Close()

        Dim res As WebResponse = req.GetResponse()

        Dim reader As New StreamReader(res.GetResponseStream())
        Dim sXml As String = reader.ReadToEnd()

        log.Info("GetLoans Response: " & CSecureStringFormatter.MaskSensitiveXMLData(sXml))

        ' Extract the inner xml and update the status of the loans 
        Dim responseXMLDoc As XmlDocument = New XmlDocument()
        responseXMLDoc.LoadXml(sXml)
        Dim innerXMLStr As String = ""
        innerXMLStr = responseXMLDoc.InnerText
        Dim innerXMLDoc As XmlDocument = New XmlDocument()
        innerXMLDoc.LoadXml(innerXMLStr)

        Dim loanInfoNodes As XmlNodeList = innerXMLDoc.GetElementsByTagName("LOAN_STATUS")
        Dim loanInfoNode As XmlElement = loanInfoNodes(0)
        Dim loanStatusAttr = loanInfoNode.Attributes("loan_status")
        If loanStatusAttr Is Nothing Then
            Return ""
        Else
            Return Common.SafeString(loanStatusAttr.InnerXml)
        End If

        Return ""
    End Function
#Region "these will be used by all loan in combo mode"

    Public Shared Function SubmitXA(ByVal psLoanType As String, ByVal poRequest As System.Web.HttpRequest, ByVal poConfig As CWebsiteConfig, ByRef psRequestRaw As String, ByRef psResponseRaw As String, currentProdList As List(Of CProduct), Optional psLoanNumber As String = "") As String
        Dim XpressApp As New CXpressApp(poConfig.OrganizationId, poConfig.LenderId)
        Dim selectedProductList As New List(Of CProduct)
        Dim oConfiguredAccountNodes As XmlNodeList = poConfig._webConfigXML.SelectNodes(Common.GetConfigLoanTypeFromShort(psLoanType) & "/ACCOUNTS/ACCOUNT")
        Dim oConfiguredServiceNodes As XmlNodeList = poConfig._webConfigXML.SelectNodes(Common.GetConfigLoanTypeFromShort(psLoanType) & "/ACCOUNTS/ACCOUNT/SERVICES/SERVICE")
        Dim oConfiguredQuestionNodes As XmlNodeList = poConfig._webConfigXML.SelectNodes(Common.GetConfigLoanTypeFromShort(psLoanType) & "/ACCOUNTS/ACCOUNT/QUESTIONS/QUESTION")
        Dim ConfiguredServiceCode As New List(Of String)
        For Each oService As XmlElement In oConfiguredServiceNodes
            ConfiguredServiceCode.Add(oService.GetAttribute("service_code").Trim)
        Next
        If currentProdList IsNot Nothing And currentProdList.Any() Then
            Dim getSettingProductsFromSubmission As Boolean = True
            'because we don't show product selection section when there is ONLY ONE ConfiguredAccountNode
            'therefore, in this case, we get its data by from download
            If oConfiguredAccountNodes IsNot Nothing AndAlso oConfiguredAccountNodes.Count = 1 Then
                getSettingProductsFromSubmission = False
                'Get the default product, product_code in xml config and other missing attribute from download
                Dim ConfiguredProduct As New CProduct
                ConfiguredProduct.ProductCode = oConfiguredAccountNodes(0).Attributes("product_code").InnerXml.Trim
                Dim cproduct As CProduct = currentProdList.FirstOrDefault(Function(cp As CProduct) cp.ProductCode = ConfiguredProduct.ProductCode)
                If cproduct IsNot Nothing Then
                    If cproduct.MinimumDeposit > 0 OrElse (cproduct.Services IsNot Nothing AndAlso cproduct.Services.Any()) OrElse (cproduct.CustomQuestions IsNot Nothing AndAlso cproduct.CustomQuestions.Any()) Then
                        getSettingProductsFromSubmission = True
                    Else
                        ''all of below should be derived from product dowload
                        ConfiguredProduct.AccountName = cproduct.AccountName
                        ConfiguredProduct.AccountType = cproduct.AccountType
                        ConfiguredProduct.Rate = cproduct.Rate
                        ConfiguredProduct.Apy = cproduct.Apy
                        ConfiguredProduct.Term = cproduct.Term

                        If Common.SafeDouble(poRequest.Params("depositAmount")) > 0 Then
                            ConfiguredProduct.DepositeAmount = poRequest.Params("depositAmount") ''this only apply for secured cc which has funding ui
                        Else
                            ConfiguredProduct.DepositeAmount = 0 '
                        End If

                        'add service
                        Dim oServices As New List(Of CProductService)
                        For Each oItem As CProductService In cproduct.Services
                            If ConfiguredServiceCode.Contains(oItem.ServiceCode.Trim) Then
                                oServices.Add(oItem)
                            End If
                        Next
                        ConfiguredProduct.Services = oServices

                        'add custom_question
                        Dim oQuestions As New List(Of CProductCustomQuestion)
                        For Each oxmlConfiguredQuestion As XmlElement In oConfiguredQuestionNodes
                            Dim sConfiguredQuestionName = oxmlConfiguredQuestion.GetAttribute("QuestionName")
                            Dim cproductCustomQuestion As CProductCustomQuestion = cproduct.CustomQuestions.FirstOrDefault(Function(cp As CProductCustomQuestion) cp.QuestionName = sConfiguredQuestionName)
                            If cproductCustomQuestion Is Nothing Then Continue For
                            Dim oConfiguredAnswer As XmlElement = oxmlConfiguredQuestion.SelectSingleNode("CUSTOM_QUESTION_ANSWER")
                            If String.Equals(cproductCustomQuestion.AnswerType, "CHECKBOX") OrElse String.Equals(cproductCustomQuestion.AnswerType, "DROPDOWN") Then
                                For Each ans As CProductRestrictedAnswer In cproductCustomQuestion.RestrictedAnswers 'initialize to unselect
                                    ans.isSelected = False
                                Next
                                For Each ans As CProductRestrictedAnswer In cproductCustomQuestion.RestrictedAnswers
                                    If ans.Text.Trim.ToUpper = oConfiguredAnswer.GetAttribute("answer_text").ToUpper Then
                                        ans.isSelected = True
                                    End If
                                Next
                            Else
                                'this is free text entry
                                cproductCustomQuestion.productAnswerText = oConfiguredAnswer.GetAttribute("answer_text")
                            End If
                            oQuestions.Add(cproductCustomQuestion)

                        Next
                        ConfiguredProduct.CustomQuestions = oQuestions
                        selectedProductList.Add(ConfiguredProduct)
                    End If
                End If
            End If
            If getSettingProductsFromSubmission = True Then
                'because we DO SHOW product selection section when the number of Configured Account Nodes in config xml <> 1
                'so, we will collect its data from form submitted data and other misssing attribute from download
                Dim oAccountSerializer As New JavaScriptSerializer()
                Dim oProductList As New List(Of SelectProducts)
                If Not String.IsNullOrEmpty(poRequest.Params("SelectedProducts")) Then
                    oProductList = oAccountSerializer.Deserialize(Of List(Of SelectProducts))(poRequest.Params("SelectedProducts"))
                End If
                If oProductList IsNot Nothing And oProductList.Count > 0 Then
                    For Each selectProduct As SelectProducts In oProductList
                        Dim cproduct As CProduct = currentProdList.FirstOrDefault(Function(cp As CProduct) cp.ProductCode = selectProduct.productName)
                        If cproduct IsNot Nothing Then
                            Dim ConfiguredProduct As New CProduct
                            ConfiguredProduct.ProductCode = cproduct.ProductCode
                            ConfiguredProduct.AccountName = cproduct.AccountName
                            ConfiguredProduct.AccountType = cproduct.AccountType
                            ConfiguredProduct.AvailableRates = cproduct.AvailableRates
							ConfiguredProduct.Rate = cproduct.Rate
							ConfiguredProduct.Apy = selectProduct.apy
							ConfiguredProduct.Term = selectProduct.term
							ConfiguredProduct.TermType = cproduct.TermType
							ConfiguredProduct.DepositeAmount = Common.SafeDecimal(selectProduct.depositAmount)

                            If poRequest.Params("CreditCardType") = "SECUREDCREDIT" Then
                                'TODO: for combo mode, when CC secured selected, there is no deposit amount for each selected product. please have a look on this
                                ConfiguredProduct.DepositeAmount = 0
                            End If

                            'add service
                            Dim oServices As New List(Of CProductService)
                            For Each oItem As CProductService In cproduct.Services
                                If selectProduct.productServices.Contains(oItem.ServiceCode.Trim) Then
                                    oServices.Add(oItem)
                                End If
                            Next
                            ConfiguredProduct.Services = oServices
                            ConfiguredProduct.CustomQuestions = selectProduct.productQuestions
                            ConfiguredProduct.SelectedRate = selectProduct.productRate
                            selectedProductList.Add(ConfiguredProduct)
                        End If
                    Next
                End If
            End If
        End If
        'TODO:add other funding option such as  paypal
        ''only non-member and secured card credit card require funding section, 'mirror xa
        Dim TempFundingSource As New CFundingSourceInfo
        'If poRequest.Form("CreditCardType") IsNot Nothing AndAlso Common.SafeString(poRequest.Form("CreditCardType").ToUpper() = "SECUREDCREDIT") Then
        If poRequest.Form("rawFundingSource") IsNot Nothing And String.IsNullOrEmpty(poRequest.Form("rawFundingSource")) = False Then
            'TODO: for now, funding section has been added for all loans. @Tony: please help double check this to make sure it work correctly
            Dim funding As CFundingSourceInfo = (New JavaScriptSerializer()).Deserialize(Of CFundingSourceInfo)(poRequest.Form("rawFundingSource"))
            If funding IsNot Nothing Then
                Select Case funding.fsFundingType.ToUpper()
                    Case "CASH"
                        TempFundingSource.fsFundingType = funding.fsFundingType
                        XpressApp.FundType = "CASH" '0

                    Case "MAIL A CHECK"
                        TempFundingSource.fsFundingType = funding.fsFundingType
                        XpressApp.FundType = "MAIL" '1

                    Case "NOT FUNDING AT THIS TIME"
                        TempFundingSource.fsFundingType = funding.fsFundingType
                        XpressApp.FundType = "NOT_FUNDING" '2
                    Case "PAYPAL"
                        TempFundingSource.fsFundingType = funding.fsFundingType
                        XpressApp.FundType = "PAYPAL"   '3

                    Case "CREDIT CARD"
                        TempFundingSource.fsFundingType = funding.fsFundingType
                        TempFundingSource.cCreditCardNumber = funding.cCreditCardNumber
                        TempFundingSource.cCardType = funding.cCardType
                        TempFundingSource.cExpirationDate = funding.cExpirationDate
                        TempFundingSource.cCVNNumber = funding.cCVNNumber
                        TempFundingSource.cNameOnCard = funding.cNameOnCard
                        TempFundingSource.cBillingAddress = funding.cBillingAddress
                        TempFundingSource.cBillingCity = funding.cBillingCity
                        TempFundingSource.cBillingState = funding.cBillingState
                        TempFundingSource.cBillingZip = funding.cBillingZip
                        XpressApp.FundType = "CREDITCARD"  '3
                    Case "INTERNAL TRANSFER"
                        TempFundingSource.fsFundingType = funding.fsFundingType
                        TempFundingSource.tAccountType = funding.tAccountType
                        TempFundingSource.tAccountNumber = funding.tAccountNumber
                        XpressApp.FundType = "INTERNALTRANSFER" '3
                    Case "TRANSFER FROM ANOTHER FINANCIAL INSTITUTION"
                        TempFundingSource.fsFundingType = funding.fsFundingType
                        TempFundingSource.bAccountType = funding.bAccountType
                        TempFundingSource.bNameOnCard = funding.bNameOnCard
                        TempFundingSource.bAccountNumber = funding.bAccountNumber
                        TempFundingSource.bRoutingNumber = funding.bRoutingNumber
                        TempFundingSource.bBankName = funding.bBankName
                        TempFundingSource.bBankState = funding.bBankState
                        XpressApp.FundType = "BANK" '3
                End Select
            End If
        End If
        'End If

        XpressApp.products = selectedProductList
        XpressApp.IsDecisionXAEnable = poConfig.IsDecisionXAEnable
        XpressApp.Funding = TempFundingSource

        'FOM
        XpressApp.FOMQuestionsAndAnswers = Common.SafeString(poRequest.Form("FOMQuestionsAndAnswers"))
        XpressApp.FOMQName = Common.SafeString(poRequest.Form("FOMName"))
        XpressApp.FOMAnswers = Common.SafeString(poRequest.Form("FOMAnswers"))
        'END FORM
        'Disclosure
        XpressApp.Disclosure = Common.SafeString(poRequest.Form("Disclosure"))
        XpressApp.IPAddress = poRequest.UserHostAddress
        XpressApp.UserAgent = poRequest.ServerVariables("HTTP_USER_AGENT")

        '===all upload document ' but do it again here bc officer may not have access to loan
        Dim oSelectDocsList As New List(Of selectDocuments)
        Dim oSerializer As New JavaScriptSerializer()
        Dim oDocsList As New Dictionary(Of String, String)
        ''by default MaxJsonLenth = 102400, set it =int32.maxvalue = 2147483647 

        oSerializer.MaxJsonLength = Int32.MaxValue
        If Not String.IsNullOrEmpty(poRequest.Form("Image")) Then
            oSelectDocsList = oSerializer.Deserialize(Of List(Of selectDocuments))(poRequest.Form("Image"))
            If oSelectDocsList.Count > 0 Then
                Dim i As Integer
                For i = 0 To oSelectDocsList.Count - 1
                    Dim oTitle As String = oSelectDocsList(i).title
                    Dim obase64data As String = oSelectDocsList(i).base64data
                    oDocsList.Add(oTitle, obase64data)
                Next
                If oDocsList.Count = 0 Then
                    log.Info("Can't de-serialize image ")
                End If
            End If
            XpressApp.DocBase64Dic = oDocsList
        End If
        ''upload document info(title,doc_group, doc_code) if it exist
        Dim oUploadDocumentInfoList As New List(Of uploadDocumentInfo)
		If Not String.IsNullOrEmpty(poRequest.Params("UploadDocInfor")) Then
			Dim oDocInfoSerializer As New JavaScriptSerializer()
			oUploadDocumentInfoList = oDocInfoSerializer.Deserialize(Of List(Of uploadDocumentInfo))(poRequest.Params("UploadDocInfor"))
		End If
		XpressApp.UploadDocumentInfoList = oUploadDocumentInfoList
        XpressApp.Applicants = New List(Of CApplicant)
        Dim hasCoApp As Boolean = poRequest.Form("HasCoApp") = "Y"
        'Dim coAppJoint As Boolean = poRequest.Form("CoAppJoin") = "Y"
        XpressApp.BranchID = poRequest.Form("XABranchID")
        XpressApp.LoanOfficerID = Common.SafeString(poRequest.Form("LoanOfficerID"))    'come from htnl mark page via js
        XpressApp.ReferralSource = Common.SafeString(poRequest.Form("ReferralSource"))
        XpressApp.IsSSO = False
        XpressApp.IsComboMode = True
        XpressApp.ComboComment = psLoanType & " " & psLoanNumber & ".  "
        If poRequest.Form("ProceedXAAnyway") = "Y" Then
            XpressApp.ComboComment += "Applicant wanted to proceed with membership if his or her loan application was not approved."
        Else
            XpressApp.ComboComment += "Applicant didn't want to proceed with membership if his or her loan application was not approved."
        End If
        'bad member comment
        If poConfig.LenderCode <> "" Then
            Dim oUnderWrite = New UnderwriteCommon()
            If oUnderWrite.isBadMember(poConfig, poRequest) Then
                XpressApp.BadMemberComment = "Underwite process stopped because applicant's SSN is on the high risk consumer list."
            Else
                XpressApp.BadMemberComment = "Applicant's SSN is NOT on the high risk consumer list."
            End If
        End If

        XpressApp.isCQNewAPI = Common.getVisibleAttribute(poConfig, "custom_question_new_api") = "Y"
		XpressApp.DownloadedXAAQs = CCustomQuestionNewAPI.getDownloadedXACustomQuestions(poConfig, True, "XA")

		Dim jsSerializer As New JavaScriptSerializer()
		' Only get Application answers, not Applicant
		Dim lstApplicationCustomAnswers As List(Of CApplicantQA) =
			jsSerializer.Deserialize(Of List(Of CApplicantQA))(poRequest.Form("CustomAnswers")).
			Where(Function(cqa) Not String.IsNullOrEmpty(cqa.CQAnswer) AndAlso cqa.CQRole = QuestionRole.Application).ToList()
		' Collect URL Parameter CQ Answers, if applicable
		Dim bIsUrlParaCustomQuestion = Common.SafeString(poRequest.Form("IsUrlParaCustomQuestion")) = "Y"
		Dim lstUrlParaCustomAnswers As New List(Of CApplicantQA)
		If bIsUrlParaCustomQuestion Then
			lstUrlParaCustomAnswers = jsSerializer.Deserialize(Of List(Of CApplicantQA))(Common.SafeString(poRequest.Form("UrlParaCustomQuestionAndAnswers")))
		End If
		' Set validated CQ answers
		XpressApp.CustomQuestionAnswers = CCustomQuestionNewAPI.
			getValidatedCustomQuestionAnswers(poConfig, "XA", QuestionRole.Application, "", lstApplicationCustomAnswers, lstUrlParaCustomAnswers.Where(Function(a) a.CQRole = QuestionRole.Application)).ToList()

		Dim applicant As New CApplicant()
		With applicant
			.EmployeeOfLender = Common.SafeString(poRequest.Form("EmployeeOfLender"))
			.FirstName = Common.SafeString(poRequest.Form("FirstName"))
			.MiddleInitial = Common.SafeString(poRequest.Form("MiddleName"))
			.LastName = Common.SafeString(poRequest.Form("LastName"))
			.Suffix = Common.SafeString(poRequest.Form("NameSuffix"))
			.SSN = Common.SafeString(poRequest.Form("SSN"))
			.DOB = Common.SafeString(poRequest.Form("DOB"))
			.MotherMaidenName = Common.SafeString(poRequest.Form("MotherMaidenName"))
			''driver license info

			If Not String.IsNullOrEmpty(poRequest.Form("IDCardNumber")) Then
				.IDNumber = Common.SafeString(poRequest.Form("IDCardNumber"))
				.IDType = Common.SafeString(poRequest.Form("IDCardType"))
				.IDState = Common.SafeString(poRequest.Form("IDState"))
				.IDCountry = Common.SafeString(poRequest.Form("IDCountry"))
				.IDDateIssued = Common.SafeString(poRequest.Form("IDDateIssued"))
				.IDExpirationDate = Common.SafeString(poRequest.Form("IDDateExpire"))
			End If
			.MemberNumber = Common.SafeString(poRequest.Form("MemberNumber"))
			.MaritalStatus = Common.SafeString(poRequest.Form("MaritalStatus"))
			.MembershipLengthMonths = Common.SafeString(poRequest.Form("MembershipLengthMonths"))
			.CitizenshipStatus = Common.SafeString(poRequest.Form("CitizenshipStatus"))
			.HomePhone = Common.SafeString(poRequest.Form("HomePhone"))
			.HomePhoneCountry = Common.SafeString(poRequest.Form("HomePhoneCountry"))
			.WorkPhone = Common.SafeString(poRequest.Form("WorkPhone"))
			.WorkPhoneCountry = Common.SafeString(poRequest.Form("WorkPhoneCountry"))
			.WorkPhoneEXT = Common.SafeString(poRequest.Form("WorkPhoneEXT"))
			.CellPhone = Common.SafeString(poRequest.Form("MobilePhone"))
			.CellPhoneCountry = Common.SafeString(poRequest.Form("MobilePhoneCountry"))
			.Email = Common.SafeString(poRequest.Form("EmailAddr"))
			.PreferredContactMethod = Common.SafeString(poRequest.Form("ContactMethod"))
			.CurrentAddress = Common.SafeString(poRequest.Form("AddressStreet"))
			.CurrentAddress2 = Common.SafeString(poRequest.Form("AddressStreet2"))
			.CurrentZip = Common.SafeString(poRequest.Form("AddressZip"))
			.CurrentCity = Common.SafeString(poRequest.Form("AddressCity"))
			.CurrentState = Common.SafeString(poRequest.Form("AddressState"))
			.CurrentCountry = Common.SafeString(poRequest.Form("Country"))
			.OccupancyType = Common.SafeString(poRequest.Form("OccupyingLocation"))
			.OccupancyDuration = Common.SafeInteger(poRequest.Form("LiveMonths"))
			.OccupancyDescription = Common.SafeString(poRequest.Form("OccupancyDescription"))
			''previous Address
			.HasPreviousAddress = Common.SafeString(poRequest.Form("hasPreviousAddress"))
			.PreviousAddress = Common.SafeString(poRequest.Form("PreviousAddressStreet"))
			.PreviousZip = Common.SafeString(poRequest.Form("PreviousAddressZip"))
			.PreviousCity = Common.SafeString(poRequest.Form("PreviousAddressCity"))
			.PreviousState = Common.SafeString(poRequest.Form("PreviousAddressState"))
			.PreviousCountry = Common.SafeString(poRequest.Form("PreviousAddressCountry"))
			''end previous Address
			''mailing address        
			.HasMailingAddress = Common.SafeString(poRequest.Form("hasMailingAddress"))
			.MailingAddress = Common.SafeString(poRequest.Form("MailingAddressStreet"))
			.MailingAddress2 = Common.SafeString(poRequest.Form("MailingAddressStreet2"))
			.MailingCity = Common.SafeString(poRequest.Form("MailingAddressCity"))
			.MailingState = Common.SafeString(poRequest.Form("MailingAddressState"))
			.MailingZip = Common.SafeString(poRequest.Form("MailingAddressZip"))
			.MailingCountry = Common.SafeString(poRequest.Form("MailingAddressCountry"))

			' Applicant questions
			Dim oAQSerializer As New JavaScriptSerializer()
			If Not String.IsNullOrEmpty(Common.SafeString(poRequest.Form("CustomAnswers"))) Then
				Dim lstApplicantCustomAnswers As List(Of CApplicantQA) =
					oAQSerializer.Deserialize(Of List(Of CApplicantQA))(poRequest.Form("CustomAnswers")).
					Where(Function(cqa) Not String.IsNullOrEmpty(cqa.CQAnswer) AndAlso cqa.CQRole = QuestionRole.Applicant AndAlso IIf(cqa.CQLocation = CustomQuestionLocation.ApplicantPage, cqa.CQApplicantPrefix = "", True)).ToList()
				' Set validated CQ answers
				.ApplicantQAList = CCustomQuestionNewAPI.
					getValidatedCustomQuestionAnswers(poConfig, "XA", QuestionRole.Applicant, "", lstApplicantCustomAnswers, lstUrlParaCustomAnswers.Where(Function(urlQA) urlQA.CQRole = QuestionRole.Applicant))
			End If

			.EmploymentStatus = Common.SafeString(poRequest.Form("EmploymentStatus"))
			''If .EmploymentStatus = "STUDENT" Or .EmploymentStatus = "RETIRED" Or .EmploymentStatus = "HOMEMAKER" Or .EmploymentStatus = "UNEMPLOYED" Then
			''	.txtJobTitle = .EmploymentStatus
			''	.txtEmployedDuration_month = Common.SafeString(poRequest.Form("txtEmployedDuration_month"))
			''	.txtEmployedDuration_year = Common.SafeString(poRequest.Form("txtEmployedDuration_year"))
			''Else
			.txtJobTitle = Common.SafeString(poRequest.Form("txtJobTitle"))
			.txtEmployedDuration_month = Common.SafeString(poRequest.Form("txtEmployedDuration_month"))
			.txtEmployedDuration_year = Common.SafeString(poRequest.Form("txtEmployedDuration_year"))
			.txtEmployer = Common.SafeString(poRequest.Form("txtEmployer"))
			''End If
			' new employment logic
			.txtBusinessType = Common.SafeString(poRequest.Form("txtBusinessType"))
			.txtEmploymentStartDate = Common.SafeString(poRequest.Form("txtEmploymentStartDate"))
			.txtETS = Common.SafeString(poRequest.Form("txtETS"))
			.txtProfessionDuration_month = Common.SafeString(poRequest.Form("txtProfessionDuration_month"))
			.txtProfessionDuration_year = Common.SafeString(poRequest.Form("txtProfessionDuration_year"))
			.txtSupervisorName = Common.SafeString(poRequest.Form("txtSupervisorName"))
			.ddlBranchOfService = Common.SafeString(poRequest.Form("ddlBranchOfService"))
			.ddlPayGrade = Common.SafeString(poRequest.Form("ddlPayGrade"))
			' end new employment logic
			.GrossMonthlyIncome = Common.SafeDouble(poRequest.Form("GrossMonthlyIncome"))
			.TotalMonthlyHousingExpense = Common.SafeDouble(poRequest.Form("TotalMonthlyHousingExpense"))

			''previous employment information
			.hasPrevEmployment = Common.SafeString(poRequest.Form("hasPreviousEmployment"))
			.prev_EmploymentStatus = Common.SafeString(poRequest.Form("prev_EmploymentStatus"))
			If .prev_EmploymentStatus = "STUDENT" Or .prev_EmploymentStatus = "RETIRED" Or .prev_EmploymentStatus = "HOMEMAKER" Or .prev_EmploymentStatus = "UNEMPLOYED" Then
				.prev_txtJobTitle = .prev_EmploymentStatus
				.prev_txtEmployedDuration_month = Common.SafeString(poRequest.Form("prev_txtEmployedDuration_month"))
				.prev_txtEmployedDuration_year = Common.SafeString(poRequest.Form("prev_txtEmployedDuration_year"))
			Else
				.prev_txtJobTitle = Common.SafeString(poRequest.Form("prev_txtJobTitle"))
				.prev_txtEmployedDuration_month = Common.SafeString(poRequest.Form("prev_txtEmployedDuration_month"))
				.prev_txtEmployedDuration_year = Common.SafeString(poRequest.Form("prev_txtEmployedDuration_year"))
				.prev_txtEmployer = Common.SafeString(poRequest.Form("prev_txtEmployer"))
			End If
			.prev_txtBusinessType = Common.SafeString(poRequest.Form("prev_txtBusinessType"))
			.prev_txtEmploymentStartDate = Common.SafeString(poRequest.Form("prev_txtEmploymentStartDate"))
			.prev_txtETS = Common.SafeString(poRequest.Form("prev_txtETS"))
			.prev_ddlBranchOfService = Common.SafeString(poRequest.Form("prev_ddlBranchOfService"))
			.prev_ddlPayGrade = Common.SafeString(poRequest.Form("prev_ddlPayGrade"))
			.prev_GrossMonthlyIncome = Common.SafeDouble(poRequest.Form("prev_GrossMonthlyIncome"))
			''end previous employment information

			''Neighborhood CU  customization
			'If (_CurrentLenderRef.ToUpper.StartsWith("NCU_TEST") Or _CurrentLenderRef.ToUpper.StartsWith("NCU103114")) And .TotalMonthlyHousingExpense < 0.01 Then
			'	.TotalMonthlyHousingExpense = 500
			'End If

			''Uncle CU  customization
			'If _CurrentLenderRef.ToUpper.StartsWith("UNCLECU") And .TotalMonthlyHousingExpense < 600 And (.OccupancyType = "LIVE WITH PARENTS" Or .OccupancyType = "RENT" Or .OccupancyType = "OTHER") Then
			'	.TotalMonthlyHousingExpense = 600
			'End If

			.HasSpouse = hasCoApp
		End With

		XpressApp.Applicants.Add(applicant)

        If hasCoApp Then
            Dim coApplicant As New CApplicant()
            With coApplicant
                .EmployeeOfLender = Common.SafeString(poRequest.Form("co_EmployeeOfLender"))
                .FirstName = Common.SafeString(poRequest.Form("co_FirstName"))
                .MiddleInitial = Common.SafeString(poRequest.Form("co_MiddleName"))
                .LastName = Common.SafeString(poRequest.Form("co_LastName"))
                .Suffix = Common.SafeString(poRequest.Form("co_NameSuffix"))
                .SSN = Common.SafeString(poRequest.Form("co_SSN"))
                .DOB = Common.SafeString(poRequest.Form("co_DOB"))
                .MotherMaidenName = Common.SafeString(poRequest.Form("co_MotherMaidenName"))
                ''identification info
                If Not String.IsNullOrEmpty(poRequest.Form("co_IDCardNumber")) Then
                    .IDNumber = Common.SafeString(poRequest.Form("co_IDCardNumber"))
                    .IDType = Common.SafeString(poRequest.Form("co_IDCardType"))
                    .IDState = Common.SafeString(poRequest.Form("co_IDState"))
                    .IDCountry = Common.SafeString(poRequest.Form("co_IDCountry"))
                    .IDDateIssued = Common.SafeString(poRequest.Form("co_IDDateIssued"))
                    .IDExpirationDate = Common.SafeString(poRequest.Form("co_IDDateExpire"))
                End If
                .MemberNumber = Common.SafeString(poRequest.Form("co_MemberNumber"))
                .RelationshipToPrimary = Common.SafeString(poRequest.Form("co_RelationshipToPrimary"))
                .MaritalStatus = Common.SafeString(poRequest.Form("co_MaritalStatus"))
                .MembershipLengthMonths = Common.SafeString(poRequest.Form("co_MembershipLengthMonths"))
                .CitizenshipStatus = Common.SafeString(poRequest.Form("co_CitizenshipStatus"))
                .HomePhone = Common.SafeString(poRequest.Form("co_HomePhone"))
                .HomePhoneCountry = Common.SafeString(poRequest.Form("co_HomePhoneCountry"))
                .WorkPhone = Common.SafeString(poRequest.Form("co_WorkPhone"))
                .WorkPhoneCountry = Common.SafeString(poRequest.Form("co_WorkPhoneCountry"))
                .WorkPhoneEXT = Common.SafeString(poRequest.Form("co_WorkPhoneEXT"))
                .CellPhone = Common.SafeString(poRequest.Form("co_MobilePhone"))
                .CellPhoneCountry = Common.SafeString(poRequest.Form("co_MobilePhoneCountry"))
                .Email = poRequest.Form("co_EmailAddr")
                .PreferredContactMethod = Common.SafeString(poRequest.Form("co_ContactMethod"))
                .CurrentAddress = Common.SafeString(poRequest.Form("co_AddressStreet"))
                .CurrentAddress2 = Common.SafeString(poRequest.Form("co_AddressStreet2"))
                .CurrentZip = Common.SafeString(poRequest.Form("co_AddressZip"))
                .CurrentCity = Common.SafeString(poRequest.Form("co_AddressCity"))
                .CurrentState = Common.SafeString(poRequest.Form("co_AddressState"))
                .CurrentCountry = Common.SafeString(poRequest.Form("co_Country"))
                .OccupancyType = Common.SafeString(poRequest.Form("co_OccupyingLocation"))
                .OccupancyDuration = Common.SafeInteger(poRequest.Form("co_LiveMonths"))
                .OccupancyDescription = Common.SafeString(poRequest.Form("co_OccupancyDescription"))
                ''previous Address
                .HasPreviousAddress = Common.SafeString(poRequest.Form("co_hasPreviousAddress"))
                .PreviousAddress = Common.SafeString(poRequest.Form("co_PreviousAddressStreet"))
                .PreviousZip = Common.SafeString(poRequest.Form("co_PreviousAddressZip"))
                .PreviousCity = Common.SafeString(poRequest.Form("co_PreviousAddressCity"))
                .PreviousState = Common.SafeString(poRequest.Form("co_PreviousAddressState"))
                .PreviousCountry = Common.SafeString(poRequest.Form("co_PreviousAddressCountry"))
                ''end previous Address

                ''mailing address        
                .HasMailingAddress = Common.SafeString(poRequest.Form("co_hasMailingAddress"))
                .MailingAddress = Common.SafeString(poRequest.Form("co_MailingAddressStreet"))
                .MailingAddress2 = Common.SafeString(poRequest.Form("co_MailingAddressStreet2"))
                .MailingCity = Common.SafeString(poRequest.Form("co_MailingAddressCity"))
                .MailingState = Common.SafeString(poRequest.Form("co_MailingAddressState"))
                .MailingZip = Common.SafeString(poRequest.Form("co_MailingAddressZip"))
				.MailingCountry = Common.SafeString(poRequest.Form("co_MailingAddressCountry"))

				' Applicant questions
				Dim oAQSerializer As New JavaScriptSerializer()
				If Not String.IsNullOrEmpty(Common.SafeString(poRequest.Form("CustomAnswers"))) Then
					Dim lstApplicantCustomAnswers As List(Of CApplicantQA) =
						oAQSerializer.Deserialize(Of List(Of CApplicantQA))(poRequest.Form("CustomAnswers")).
						Where(Function(cqa) Not String.IsNullOrEmpty(cqa.CQAnswer) AndAlso cqa.CQRole = QuestionRole.Applicant AndAlso IIf(cqa.CQLocation = CustomQuestionLocation.ApplicantPage, cqa.CQApplicantPrefix = "co_", True)).ToList()
					' Set validated CQ answers
					.ApplicantQAList = CCustomQuestionNewAPI.
						getValidatedCustomQuestionAnswers(poConfig, "XA", QuestionRole.Applicant, "", lstApplicantCustomAnswers, lstUrlParaCustomAnswers.Where(Function(urlQA) urlQA.CQRole = QuestionRole.Applicant))
				End If

				.EmploymentStatus = Common.SafeString(poRequest.Form("co_EmploymentStatus"))
                ''If .EmploymentStatus = "STUDENT" Or .EmploymentStatus = "RETIRED" Or .EmploymentStatus = "HOMEMAKER" Or .EmploymentStatus = "UNEMPLOYED" Then
                ''	.txtJobTitle = .EmploymentStatus
                ''	.txtEmployedDuration_month = Common.SafeString(poRequest.Form("co_txtEmployedDuration_month"))
                ''	.txtEmployedDuration_year = Common.SafeString(poRequest.Form("co_txtEmployedDuration_year"))
                ''Else
                .txtJobTitle = Common.SafeString(poRequest.Form("co_txtJobTitle"))
                .txtEmployedDuration_month = Common.SafeString(poRequest.Form("co_txtEmployedDuration_month"))
                .txtEmployedDuration_year = Common.SafeString(poRequest.Form("co_txtEmployedDuration_year"))
                .txtEmployer = Common.SafeString(poRequest.Form("co_txtEmployer"))
                ''End If
                ' new employment logic
                .txtBusinessType = Common.SafeString(poRequest.Form("co_txtBusinessType"))
                .txtEmploymentStartDate = Common.SafeString(poRequest.Form("co_txtEmploymentStartDate"))
                .txtETS = Common.SafeString(poRequest.Form("co_txtETS"))
                .txtProfessionDuration_month = Common.SafeString(poRequest.Form("co_txtProfessionDuration_month"))
                .txtProfessionDuration_year = Common.SafeString(poRequest.Form("co_txtProfessionDuration_year"))
                .txtSupervisorName = Common.SafeString(poRequest.Form("co_txtSupervisorName"))
                .ddlBranchOfService = Common.SafeString(poRequest.Form("co_ddlBranchOfService"))
                .ddlPayGrade = Common.SafeString(poRequest.Form("co_ddlPayGrade"))
                ' end new employment logic
                .GrossMonthlyIncome = Common.SafeDouble(poRequest.Form("co_GrossMonthlyIncome"))
                .TotalMonthlyHousingExpense = Common.SafeDouble(poRequest.Form("co_TotalMonthlyHousingExpense"))

                ''previous employment information
                .hasPrevEmployment = Common.SafeString(poRequest.Form("co_hasPreviousEmployment"))
                .prev_EmploymentStatus = Common.SafeString(poRequest.Form("co_prev_EmploymentStatus"))
                If .prev_EmploymentStatus = "STUDENT" Or .prev_EmploymentStatus = "RETIRED" Or .prev_EmploymentStatus = "HOMEMAKER" Or .prev_EmploymentStatus = "UNEMPLOYED" Then
                    .prev_txtJobTitle = .prev_EmploymentStatus
                    .prev_txtEmployedDuration_month = Common.SafeString(poRequest.Form("co_prev_txtEmployedDuration_month"))
                    .prev_txtEmployedDuration_year = Common.SafeString(poRequest.Form("co_prev_txtEmployedDuration_year"))
                Else
                    .prev_txtJobTitle = Common.SafeString(poRequest.Form("co_prev_txtJobTitle"))
                    .prev_txtEmployedDuration_month = Common.SafeString(poRequest.Form("co_prev_txtEmployedDuration_month"))
                    .prev_txtEmployedDuration_year = Common.SafeString(poRequest.Form("co_prev_txtEmployedDuration_year"))
                    .prev_txtEmployer = Common.SafeString(poRequest.Form("co_prev_txtEmployer"))
                End If
                .prev_txtBusinessType = Common.SafeString(poRequest.Form("co_prev_txtBusinessType"))
                .prev_txtEmploymentStartDate = Common.SafeString(poRequest.Form("co_prev_txtEmploymentStartDate"))
                .prev_txtETS = Common.SafeString(poRequest.Form("co_prev_txtETS"))
                .prev_ddlBranchOfService = Common.SafeString(poRequest.Form("co_prev_ddlBranchOfService"))
                .prev_ddlPayGrade = Common.SafeString(poRequest.Form("co_prev_ddlPayGrade"))
                .prev_GrossMonthlyIncome = Common.SafeDouble(poRequest.Form("co_prev_GrossMonthlyIncome"))
                ''end previous employment information

                'Neighborhood CU  customization
                'If (_CurrentLenderRef.ToUpper.StartsWith("NCU_TEST") Or _CurrentLenderRef.ToUpper.StartsWith("NCU103114")) And .TotalMonthlyHousingExpense < 0.01 Then
                '	.TotalMonthlyHousingExpense = 500
                'End If

                ''Uncle CU  customization
                'If _CurrentLenderRef.ToUpper.StartsWith("UNCLECU") And .TotalMonthlyHousingExpense < 600 And (.OccupancyType = "LIVE WITH PARENTS" Or .OccupancyType = "RENT" Or .OccupancyType = "OTHER") Then
                '	.TotalMonthlyHousingExpense = 600
                'End If

            End With
            'IsJointApplication = hasCoApp 'why?
            XpressApp.Applicants.Add(coApplicant)
            Dim Co_FName = Common.SafeString(poRequest.Form("co_FirstName")) ''use for second call back
        End If
        ''get foreinssn if it exist in config
        Dim ForeignSSN As String = ""
        Dim co_ForeignSSN As String = ""
        Dim isSubmitLoan As Boolean = False
        If Not String.IsNullOrEmpty(poConfig.AppType) Then
            If poConfig.AppType.ToUpper() = "FOREIGN" Then
                ForeignSSN = Common.SafeString(poRequest.Form("SSN"))
                co_ForeignSSN = Common.SafeString(poRequest.Form("co_SSN"))
            End If
        End If
        If hasCoApp Then 'check foreignssn and co_foreignssn
            If ForeignSSN = "999999999" Or co_ForeignSSN = "999999998" Then ''no foreignssn or co_foreignssn
                isSubmitLoan = True
            End If
        Else ''only foreignssn
            If ForeignSSN = "999999999" Then
                isSubmitLoan = True
            End If
        End If

        ''need this incomplete template for GetSuccessMessage
        Dim sXARequestXMLTempplate As XmlDocument = Common.getSubmitLoanData(XpressApp, poConfig, True)
        psRequestRaw = sXARequestXMLTempplate.OuterXml()    'store in  session for for later use

        Return Common.SubmitLoan(XpressApp, poConfig, "N", psResponseRaw, True)

    End Function

    Protected Shared Function getConditionQuestionXAs(ByVal poConfig As CWebsiteConfig, ByVal oCurrentXACQs As List(Of CCustomQuestionXA)) As List(Of CCustomQuestion)
        Dim xaConQuestions As New List(Of CCustomQuestion)
        Dim conQuestions As List(Of CCustomQuestion) = poConfig.GetConditionedQuestions("XA_LOAN")
        If conQuestions.Count = 0 Then
            Return xaConQuestions
        End If
        For Each conCQ As CCustomQuestion In conQuestions
            For Each cq As CCustomQuestionXA In oCurrentXACQs
                If (conCQ.Name = cq.Name) Then
                    Select Case cq.AnswerType.ToLower()
                        Case "text"
                            conCQ.UIType = QuestionUIType.TEXT
                        Case "dropdown"
                            conCQ.UIType = QuestionUIType.DROPDOWN
                        Case "checkbox"
                            conCQ.UIType = QuestionUIType.CHECK
                        Case "radio"
                            conCQ.UIType = QuestionUIType.RADIO
                        Case "hidden"
                            conCQ.UIType = QuestionUIType.HIDDEN
                    End Select
                    conCQ.CustomQuestionOptions = cq.CustomQuestionOptions
                    conCQ.Text = cq.Text
                    conCQ.RegExpression = cq.reg_expression
                    If String.IsNullOrEmpty(Common.SafeString(conCQ.isRequired)) Then
                        If (cq.IsRequired) Then
                            conCQ.isRequired = "Y"
                        Else
                            conCQ.isRequired = "N"
                        End If

                    End If

                    xaConQuestions.Add(conCQ)
                End If
            Next
        Next
        Return xaConQuestions
    End Function
    'Private Shared Function SafeString(ByVal obj As Object) As String
    '	If obj Is Nothing Then
    '		Return ""
    '	End If
    '	Return obj.ToString()
    'End Function
    Protected Shared Function GetXAAllowedCustomQuestionNames(ByVal poConfig As CWebsiteConfig) As List(Of String)
        Dim ProductNames As New List(Of String)
        Dim oNodes As XmlNodeList = poConfig._webConfigXML.SelectNodes("XA_LOAN/CUSTOM_QUESTIONS/QUESTION")

        If oNodes.Count < 1 Then
            Return ProductNames
        End If

        For Each child As XmlNode In oNodes
            If child.Attributes("name") Is Nothing Then
                log.Error("QUESTION name is not correct.", Nothing)
            End If
            If child.Attributes("name").InnerXml <> "" Then
                ProductNames.Add(child.Attributes("name").InnerXml.ToLower)
            End If
        Next

        Return ProductNames

    End Function
    Public Class selectDocuments
        Public title As String
        Public base64data As String
    End Class
#End Region
#Region "Add XA Data to Internal Comment"
    Public Shared Function addXADataToCommentSection(ByVal poRequest As System.Web.HttpRequest, ByVal poConfig As CWebsiteConfig, currentProdList As List(Of CProduct)) As String
        Dim strSelectedProductList = getSelectedProducts(poRequest, poConfig, currentProdList)
        Dim strFOMQuestionAndAnswer = getFOMQuestionAndAnswers(poRequest, poConfig)
        Dim strFundingMethod As String = ""
        Dim strXADataComment As String = ""
        Dim TempFundingSource As New CFundingSourceInfo
        If poRequest.Form("rawFundingSource") IsNot Nothing And String.IsNullOrEmpty(poRequest.Form("rawFundingSource")) = False Then
            'TODO: for now, funding section has been added for all loans. @Tony: please help double check this to make sure it work correctly
            Dim funding As CFundingSourceInfo = (New JavaScriptSerializer()).Deserialize(Of CFundingSourceInfo)(poRequest.Form("rawFundingSource"))

            If funding IsNot Nothing Then
                strFundingMethod = funding.fsFundingType
            End If
        End If
        If strFOMQuestionAndAnswer <> "" Then
            strXADataComment += "/n/nFOM QUESTIONS: " & strFOMQuestionAndAnswer
        End If
        If strSelectedProductList <> "" Then
            strXADataComment += "SELECTED ACCOUNTS: " & strSelectedProductList
        End If
        If strFundingMethod <> "" Then
            strXADataComment += "/nSELECTED FUNDING METHOD: " & strFundingMethod
        End If
        Return strXADataComment
    End Function
    Public Shared Function getSelectedProducts(ByVal poRequest As System.Web.HttpRequest, ByVal poConfig As CWebsiteConfig, currentProdList As List(Of CProduct)) As String
        Dim selectedProductList As New List(Of CProduct)
        Dim oConfiguredAccountNodes As XmlNodeList = poConfig._webConfigXML.SelectNodes("XA_LOAN/ACCOUNTS/ACCOUNT")
        Dim strSelectedProducts As String = ""
        If currentProdList IsNot Nothing And currentProdList.Any() Then
            Dim getSettingProductsFromSubmission As Boolean = True
            If oConfiguredAccountNodes IsNot Nothing AndAlso oConfiguredAccountNodes.Count = 1 Then
                getSettingProductsFromSubmission = False
                'Get the default product, product_code in xml config and other missing attribute from download
                Dim ConfiguredProduct As New CProduct
                ConfiguredProduct.ProductCode = oConfiguredAccountNodes(0).Attributes("product_code").InnerXml.Trim
                Dim cproduct As CProduct = currentProdList.FirstOrDefault(Function(cp As CProduct) cp.ProductCode = ConfiguredProduct.ProductCode)
                If cproduct IsNot Nothing Then
                    ''Don't display product and funding on combo app when there is only ONE product that has no minimum deposit, no services, and no product questions. Do display the product and funding on account that has minimum deposit, services, or product questions. ==> get product data from submission when  there is only ONE product and has minimum deposit, services, or product questions.
                    If cproduct.MinimumDeposit > 0 OrElse (cproduct.Services IsNot Nothing AndAlso cproduct.Services.Any()) OrElse (cproduct.CustomQuestions IsNot Nothing AndAlso cproduct.CustomQuestions.Any()) Then
                        getSettingProductsFromSubmission = True
                    Else
                        'because we don't show product selection section when there is ONLY ONE ConfiguredAccountNode
                        'therefore, in this case, we get its data by from download

                        ''all of below should be derived from product dowload
                        ConfiguredProduct.AccountName = cproduct.AccountName
                        ConfiguredProduct.AccountType = cproduct.AccountType

                        If Common.SafeDouble(poRequest.Params("depositAmount")) > 0 Then
                            ConfiguredProduct.DepositeAmount = poRequest.Params("depositAmount") ''this only apply for secured cc which has funding ui
                        Else
                            ConfiguredProduct.DepositeAmount = 0 '
                        End If
                        selectedProductList.Add(ConfiguredProduct)
                    End If
                End If
            End If
            If getSettingProductsFromSubmission = True Then
                'because we DO SHOW product selection section when the number of Configured Account Nodes in config xml <> 1
                'so, we will collect its data from form submitted data and other misssing attribute from download
                Dim oAccountSerializer As New JavaScriptSerializer()
                Dim oProductList As New List(Of SelectProducts)
                If Not String.IsNullOrEmpty(poRequest.Params("SelectedProducts")) Then
                    oProductList = oAccountSerializer.Deserialize(Of List(Of SelectProducts))(poRequest.Params("SelectedProducts"))
                End If
                If oProductList IsNot Nothing And oProductList.Count > 0 Then
                    For Each selectProduct As SelectProducts In oProductList
                        Dim cproduct As CProduct = currentProdList.FirstOrDefault(Function(cp As CProduct) cp.ProductCode = selectProduct.productName)
                        If cproduct IsNot Nothing Then
                            Dim ConfiguredProduct As New CProduct
                            ConfiguredProduct.AccountName = cproduct.AccountName
                            ConfiguredProduct.AccountType = cproduct.AccountType
                            If poRequest.Params("CreditCardType") = "SECUREDCREDIT" Then
                                'TODO: for combo mode, when CC secured selected, there is no deposit amount for each selected product. please have a look on this
                                ConfiguredProduct.DepositeAmount = 0
                            Else
                                'done => TODO:will update to actual amount if we have funding UI for combo mode later
                                If oProductList.Any(Function(p) p.productName = cproduct.ProductCode) Then
                                    Dim depositAmount As Decimal = 0
                                    Decimal.TryParse(oProductList.First(Function(p) p.productName = cproduct.ProductCode).depositAmount, depositAmount)
                                    ConfiguredProduct.DepositeAmount = depositAmount
                                End If
                            End If
                            selectedProductList.Add(ConfiguredProduct)
                        End If
                    Next
                End If
            End If
        End If
        If selectedProductList.Count > 0 Then
            Dim totalAmount As Double = 0.0
            For Each oProduct In selectedProductList
                strSelectedProducts += oProduct.AccountName + ", "
                totalAmount += Convert.ToDouble(oProduct.DepositeAmount)
            Next
            If totalAmount > 0 Then
                strSelectedProducts += "/nTOTAL DEPOSIT AMOUNT: $" & totalAmount.ToString()
            End If
        End If
        Return strSelectedProducts
    End Function
    Public Shared Function getFOMQuestionAndAnswers(ByVal poRequest As System.Web.HttpRequest, ByVal poConfig As CWebsiteConfig) As String
        Dim strFOMQuestionAndAnswers As String = ""
        Dim sTextTemplate As String = ""
        Dim oFOMQAs As New List(Of Dictionary(Of String, String))      
        If poRequest.Form("FOMQuestionsAndAnswers") IsNot Nothing Then
            oFOMQAs = (New JavaScriptSerializer()).Deserialize(Of List(Of Dictionary(Of String, String)))(poRequest.Form("FOMQuestionsAndAnswers"))
        End If
		If oFOMQAs IsNot Nothing AndAlso oFOMQAs.Any() Then
			'' begin fom question section
			For Each FOMQAIndexed As IndexedItem(Of Dictionary(Of String, String)) In oFOMQAs.IndexedItems()
				Dim FOMQA As Dictionary(Of String, String) = FOMQAIndexed.Item
				Dim oFQs As New List(Of CFOMQuestion)
				Dim sFQName As String = FOMQA.Item("FOMName")
				Dim listFomAnswers = (New JavaScriptSerializer()).Deserialize(Of List(Of String))(FOMQA.Item("FOMAnswers"))
				If FOMQAIndexed.Index = 0 Then ''start question
					oFQs = CFOMQuestion.CurrentFOMQuestions(poConfig).Where(Function(FQ) FQ.Name IsNot Nothing AndAlso FQ.Name.Equals(sFQName, StringComparison.OrdinalIgnoreCase)).ToList()
				Else ''next question
					oFQs = CFOMQuestion.DownloadFomNextQuestions(poConfig).Where(Function(FQ) FQ.Name IsNot Nothing AndAlso FQ.Name.Equals(sFQName, StringComparison.OrdinalIgnoreCase)).ToList()
				End If

				Dim oFQ As CFOMQuestion = oFQs.FirstOrDefault()
				If oFQ Is Nothing Then
					Exit For
				End If

				sTextTemplate = If(oFQ.TextTemplate, "")
				If oFQ.Fields IsNot Nothing AndAlso oFQ.Fields.Any() Then
					For Each _fieldIndexed As IndexedItem(Of CFOMQuestionField) In oFQ.Fields.IndexedItems()
						Dim _field As CFOMQuestionField = _fieldIndexed.Item
						Dim _val As String = IIf(listFomAnswers.Count > _fieldIndexed.Index, listFomAnswers(_fieldIndexed.Index), "")
						Dim _text As String = _val
						If "DROPDOWN".Equals(_field.Type, StringComparison.OrdinalIgnoreCase) AndAlso _field.Answers IsNot Nothing AndAlso _field.Answers.Any() Then
							For Each oAnswer As CFOMQuestionAnswer In _field.Answers
								If _val IsNot Nothing AndAlso _val.Equals(oAnswer.Value, StringComparison.OrdinalIgnoreCase) AndAlso Not String.IsNullOrEmpty(oAnswer.Text) Then
									_val = oAnswer.Text
									Exit For
								End If
							Next
						End If
						If oFQ.TextTemplate.IndexOf("{") > -1 AndAlso oFQ.TextTemplate.IndexOf("}") > -1 Then
							sTextTemplate = sTextTemplate.Replace("{" & _fieldIndexed.Index.ToString & "}", _val)
						Else
							sTextTemplate += _val + ", "
						End If
					Next
				End If

				If sTextTemplate <> "" Then
					' The "/n" is on purpose, here, with the forward slash. It will correctly add a newline.
					strFOMQuestionAndAnswers += sTextTemplate & "/n"
				End If
			Next
		End If
		Return strFOMQuestionAndAnswers
    End Function
 
#End Region

End Class
