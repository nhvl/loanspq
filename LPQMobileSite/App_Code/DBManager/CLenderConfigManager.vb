﻿Imports Microsoft.VisualBasic
Imports LPQMobile.BO
Imports DBUtils
Imports LPQMobile.Utils
Imports System.Data
Imports System.Xml
Imports System.Web
Imports System.Text.RegularExpressions

Namespace LPQMobile.DBManager

	Public Class CLenderConfigManager
		Private _log As log4net.ILog = log4net.LogManager.GetLogger(GetType(CLenderConfigManager))
		Protected LPQMOBILE_CONNECTIONSTRING As String = System.Web.Configuration.WebConfigurationManager.ConnectionStrings("LPQMOBILE_CONNECTIONSTRING").ConnectionString
		Protected LPQMOBILE_CONNECTIONSTRING_TEST As String = System.Web.Configuration.WebConfigurationManager.ConnectionStrings("LPQMOBILE_CONNECTIONSTRING_TEST").ConnectionString

		Const LENDERCONFIG_TABLE As String = "LenderConfigs"
		Const USERS_TABLE As String = "Users"
		Const BACKOFFICELENDERS_TABLE As String = "BackOfficeLenders"
		Const LENDERCONFIGPREVIEWS_TABLE As String = "LenderConfigPreviews"
		Const LENDERCONFIGLOGS_TABLE As String = "LenderConfigLogs"

		Public Sub New()
			CPBLogger.PBLogger.Component = Me.GetType.ToString
			CPBLogger.PBLogger.Context = "CLenderConfigManager"
		End Sub

		Public Sub SaveLenderConfig(ByVal config As CLenderConfig)
			'If Not IsExistedLender(config.OrganizationId.ToString(), config.LenderId.ToString()) Then
			If config.ID = Guid.Empty Then
				InsertLenderConfig(config)
                InsertBackOfficeLender(config)
            Else
				UpdateLenderConfig(config)
				InsertBackOfficeLender(config)
			End If
		End Sub

		Public Sub UpdateCheckoutFields(ByVal config As CLenderConfig)
			'Dim oUpdate As New cSQLUpdateStringBuilder("LenderConfigs", "Where OrganizationID = " & Common.SQLString(config.OrganizationId.ToString()) & _
			'														   " And LenderID = " & Common.SQLString(config.LenderId.ToString()))

			Dim oWhere As New CSQLWhereStringBuilder()
			oWhere.AppendAndCondition("LenderConfigID={0}", New CSQLParamValue(config.ID))
			Dim oUpdate As New cSQLUpdateStringBuilder("LenderConfigs", oWhere)

			oUpdate.appendvalue("LastOpenedByUserID", New CSQLParamValue(config.LastOpenedByUserID))
			oUpdate.appendvalue("LastOpenedDate", New CSQLParamValue(config.LastOpenedDate))
			oUpdate.appendvalue("IterationID", New CSQLParamValue(config.IterationID))
			Dim oDB As New CSQLDBUtils(LPQMOBILE_CONNECTIONSTRING)
			Try
				oDB.executeNonQuery(oUpdate.SQL, False)
			Catch ex As Exception
				_log.Error("Error while UpdateCheckoutFields", ex)
			Finally
				oDB.closeConnection()
			End Try

		End Sub


		Public Sub InsertLenderConfig(ByVal config As CLenderConfig)
			Dim oInsert As New cSQLInsertStringBuilder("LenderConfigs")
			oInsert.AppendValue("LenderConfigID", New CSQLParamValue(System.Guid.NewGuid))
			oInsert.AppendValue("OrganizationID", New CSQLParamValue(Common.SafeGUID(config.OrganizationId)))
			oInsert.AppendValue("LenderID", New CSQLParamValue(Common.SafeGUID(config.LenderId)))
			oInsert.AppendValue("LenderRef", New CSQLParamValue(config.LenderRef))
			oInsert.AppendValue("ReturnURL", New CSQLParamValue(config.ReturnURL))
			oInsert.AppendValue("LPQLogin", New CSQLParamValue(config.LPQLogin))
			oInsert.AppendValue("LPQPW", New CSQLParamValue(config.LPQPW))
			oInsert.AppendValue("LPQURL", New CSQLParamValue(config.LPQURL))
			oInsert.AppendValue("ConfigData", New CSQLParamValue(config.ConfigData))
			oInsert.AppendValue("note", New CSQLParamValue(config.Note))
			oInsert.AppendValue("LastModifiedDate", New CSQLParamValue(Now))
			oInsert.AppendValue("LastModifiedByUserID", New CSQLParamValue(config.LastModifiedByUserID))
			oInsert.AppendValue("CreatedDate", New CSQLParamValue(Now))
			oInsert.AppendValue("LastOpenedByUserID", New CSQLParamValue(config.LastOpenedByUserID))
			oInsert.AppendValue("IterationID", New CSQLParamValue(0))
			oInsert.AppendValue("RevisionID", New CSQLParamValue(1))
			Dim oDB As New CSQLDBUtils(LPQMOBILE_CONNECTIONSTRING)
			Try
				oDB.executeNonQuery(oInsert.SQL, False)
			Catch ex As Exception
				_log.Error("Error while  Saving LenderConfig", ex)
				If ex.Message.Contains("Cannot insert duplicate key") Then
					Throw New Exception("Can't save lender config due to duplicate lender Ref", ex)
				Else
					Throw New Exception("Error while  Saving LenderConfig", ex)
				End If

			Finally
				oDB.closeConnection()
			End Try
		End Sub


        ''' <summary>
        ''' Create entry about the lender (lendername, city, state) in BackOfficeLender table
        '''  senarios:
        '''  1) Can't get lender info - create entry with lender name as "Unknow"
        '''''1) Same lenderID is already in DB but with different LPQUrl - create new entry in DB, don't delete old one so NOT to break linkage with other portals 
        '''' 2) lenderID is not in DB - create new entry
        ''' </summary>
        ''' <param name="poLenderConfig"></param>
        Public Sub InsertBackOfficeLender(ByVal poLenderConfig As CLenderConfig)

			Dim oDocXML As New XmlDocument
			oDocXML.LoadXml(Common.SafeString(poLenderConfig.ConfigData))
			Dim oXmlElement As XmlElement = oDocXML.DocumentElement
			Dim lender_code = oXmlElement.GetAttribute("lender_code")
			Dim org_code = oXmlElement.GetAttribute("org_code")

            If lender_code = "" Or org_code = "" Then
                _log.Warn("Missing lender_code or org_code")
            End If

            Dim oData As DataTable
			Dim sSQL As String = "SELECT * FROM " & BACKOFFICELENDERS_TABLE
			Dim oWhere As New CSQLWhereStringBuilder()
			oWhere.AppendAndCondition("LenderID={0}", New CSQLParamValue(poLenderConfig.LenderId))
			Dim oDB As New CSQLDBUtils(LPQMOBILE_CONNECTIONSTRING)
			Try
                oData = oDB.getDataTable(sSQL & oWhere.SQL)
                For Each oDataRow As DataRow In oData.Rows 'There may be multiple lenders with same lenderID but on different host
                    Dim extractDomainRegexPattern As String = "^(?:https?:\/\/)?(?:[^@\n]+@)?(?:www\.)?([^:\/\n]+)"
                    Dim match1 As Match = Regex.Match(poLenderConfig.LPQURL, extractDomainRegexPattern, RegexOptions.IgnoreCase)
                    Dim match2 As Match = Regex.Match(Common.SafeString(oDataRow("LpqUrl")), extractDomainRegexPattern, RegexOptions.IgnoreCase)
                    If match1.Success AndAlso match2.Success AndAlso match1.Value.ToLower = match2.Value.ToLower Then
                        Return 'Backoffice lender already exist so don't need to create one
                    End If
                Next
            Catch ex As Exception
				_log.Error("Error while Getting BACKOFFICELENDERS_TABLE", ex)
				Return
			Finally
				oDB.closeConnection()
			End Try

            Dim sGetLenderUrl = poLenderConfig.LPQURL.Substring(0, poLenderConfig.LPQURL.IndexOf("services")) + "Services/resources.ashx/orgs/" + org_code + "/fis/" + lender_code + "/settings"

            'If interface to other LOS then update API end point
            Dim sURL_TPV As String = oXmlElement.GetAttribute("loan_submit_url_tpv")
            If sURL_TPV <> "" AndAlso sURL_TPV = poLenderConfig.LPQURL Then 'if only interface to ACTIon then use ACTion
                Dim lender_code2 = oXmlElement.GetAttribute("lender_code2")
                Dim org_code2 = oXmlElement.GetAttribute("org_code2")
                If lender_code2 = "" Or org_code2 = "" Then
                    _log.Warn("Can't create Lender row. Missing lender_code2 or org_code2")
                    Return
                End If
                sGetLenderUrl = String.Format(sGetLenderUrl & "?OrgCode2={0}&LenderCode2={1}", org_code2, lender_code2)
            End If

            _log.InfoFormat("Start getting lender info for {0} from {1}", poLenderConfig.LenderRef, sGetLenderUrl)
            Dim oResult = ""
			Try
                oResult = Common.SubmitRestApiGet(poLenderConfig.LPQLogin, Common.DecryptPassword(poLenderConfig.LPQPW), sGetLenderUrl)
            Catch ex As Exception
                _log.Error("can't update lenderref:" & poLenderConfig.LenderRef)
            End Try

            Dim sLenderName As String = "Unknow"
            Dim sCity As String = "Unknow"
            Dim sState As String = "Unknow"
            If oResult = "" Then '1)Can't get lender info, system still create an entry in db for the unknow lender with lenderID, user can manually update the info later via APM UI 
                _log.Warn(String.Format("Can't get lender name for {0} but still create an unknow lender", poLenderConfig.LenderRef))
            Else
                _log.InfoFormat("Response for lenderRef: {0} ; {1}", poLenderConfig.LenderRef, oResult)
                Dim oXMLDocLenderInfo As New XmlDocument
                oXMLDocLenderInfo.LoadXml(oResult)
                sLenderName = Left(oXMLDocLenderInfo.SelectSingleNode("/LENDER/LENDER_NAME").InnerText, 100)
                sCity = Left(oXMLDocLenderInfo.SelectSingleNode("/LENDER/CITY").InnerText, 50)
                sState = oXMLDocLenderInfo.SelectSingleNode("/LENDER/STATE").InnerText
            End If

            'alway create new entry for update or create, old one is not delete so not to break linkage with other portals
            Dim oInsert As New cSQLInsertStringBuilder(BACKOFFICELENDERS_TABLE)
			oInsert.AppendValue("BackOfficeLenderID", New CSQLParamValue(System.Guid.NewGuid))
			oInsert.AppendValue("LenderID", New CSQLParamValue(Common.SafeGUID(poLenderConfig.LenderId)))
            oInsert.AppendValue("LenderName", New CSQLParamValue(sLenderName))
            oInsert.AppendValue("LpqUrl", New CSQLParamValue(poLenderConfig.LPQURL))
            oInsert.AppendValue("Host", New CSQLParamValue(SmUtil.ExtractHostFromUrl(poLenderConfig.LPQURL)))
            oInsert.AppendValue("City", New CSQLParamValue(sCity))
            oInsert.AppendValue("State", New CSQLParamValue(sState))

            Try
				oDB.executeNonQuery(oInsert.SQL, False)
			Catch ex As Exception
                _log.Error(String.Format("Error while  Saving lender {0} BACKOFFICELENDERS_TABLE", sLenderName), ex)
                Return
				'Throw New Exception("Error while  Saving BACKOFFICELENDERS_TABLE", ex)
			Finally
				oDB.closeConnection()
			End Try
            _log.InfoFormat("Inserted new lender {0} to BACKOFFICELENDERS_TABLE", sLenderName)

        End Sub

        Public Sub LogLenderConfig(ByVal config As CLenderConfig, ByVal psRemoteIP As String, ByVal psByUserID As Guid, ByVal psCachedDiff As String)
            Dim oInsert As New cSQLInsertStringBuilder("LenderConfigLogs")
            oInsert.AppendValue("LenderConfigLogID", New CSQLParamValue(System.Guid.NewGuid))
            oInsert.AppendValue("LenderConfigID", New CSQLParamValue(Common.SafeGUID(config.ID))) 'this is unique
            oInsert.AppendValue("OrganizationID", New CSQLParamValue(Common.SafeGUID(config.OrganizationId)))
            oInsert.AppendValue("LenderID", New CSQLParamValue(Common.SafeGUID(config.LenderId)))
            oInsert.AppendValue("LenderRef", New CSQLParamValue(config.LenderRef))
            oInsert.AppendValue("ReturnURL", New CSQLParamValue(config.ReturnURL))
            oInsert.AppendValue("LPQLogin", New CSQLParamValue(config.LPQLogin))
            oInsert.AppendValue("LPQPW", New CSQLParamValue(config.LPQPW))
            oInsert.AppendValue("LPQURL", New CSQLParamValue(config.LPQURL))
            oInsert.AppendValue("ConfigData", New CSQLParamValue(CStringCompressor.Compress(config.ConfigData)))
            oInsert.AppendValue("LogTime", New CSQLParamValue(Now))
            oInsert.AppendValue("RemoteIP", New CSQLParamValue(psRemoteIP))
            oInsert.AppendValue("LastModifiedByUserID", New CSQLParamValue(psByUserID))
            oInsert.AppendValue("note", New CSQLParamValue(config.Note))
            oInsert.AppendValue("Comment", New CSQLParamValue(config.Note))
            oInsert.AppendValue("RevisionID", New CSQLParamValue(config.RevisionID))
            oInsert.AppendValue("CachedDiff", New CSQLParamValue(psCachedDiff))

            Dim oDB As New CSQLDBUtils(LPQMOBILE_CONNECTIONSTRING)
            Try
                oDB.executeNonQuery(oInsert.SQL, False)
            Catch ex As Exception
                _log.Error("Error while saving SaveLenderConfigLog", ex)
            Finally
                oDB.closeConnection()
            End Try

        End Sub

        ''deprecated UpdateLenderConfig(
        'Public Sub UpdateLenderConfig(ByVal config As CLenderConfig)
        '	Dim oUpdate As New cSQLUpdateStringBuilder("LenderConfigs", "Where OrganizationID = " & Common.SQLString(config.OrganizationId.ToString()) & _
        '															   " And LenderID = " & Common.SQLString(config.LenderId.ToString()))
        '	oUpdate.appendvalue("LenderRef", New CSQLParamValue(config.LenderRef))
        '	oUpdate.appendvalue("ReturnURL", New CSQLParamValue(config.ReturnURL))
        '	oUpdate.appendvalue("LPQLogin", New CSQLParamValue(config.LPQLogin))
        '	oUpdate.appendvalue("LPQPW", New CSQLParamValue(config.LPQPW))
        '	oUpdate.appendvalue("LPQURL", New CSQLParamValue(config.LPQURL))
        '	oUpdate.appendvalue("ConfigData", New CSQLParamValue(config.ConfigData))
        '	oUpdate.appendvalue("note", New CSQLParamValue(config.Note))
        '	oUpdate.appendvalue("LastModifiedDate", New CSQLParamValue(Now))
        '	oUpdate.appendvalue("LastModifiedBy", New CSQLParamValue(config.LastModifiedBy))
        '	oUpdate.appendvalue("LastOpenedBy", New CSQLParamValue(config.LastOpenedBy))
        '	oUpdate.appendvalue("IterationID", New CSQLParamValue(config.IterationID))
        '	Dim oDB As New CSQLDBUtils(LPQMOBILE_CONNECTIONSTRING)
        '	Try
        '		oDB.executeNonQuery(oUpdate.SQL, False)
        '	Catch ex As Exception
        '		_log.Error("Error while check SaveLenderConfig", ex)
        '	Finally
        '		oDB.closeConnection()
        '	End Try

        'End Sub

        Public Sub UpdateLenderConfig(ByVal config As CLenderConfig)
			Dim oDB As New CSQLDBUtils(LPQMOBILE_CONNECTIONSTRING)
            Try
                'don't want to update backOfficeLender based on a portal because it will break other portals that are linked to this backOfficeLender
                'Dim backOfficeLenderID As Guid = Guid.Empty
                'Dim sSQL As String = String.Format("SELECT B.*, L.LastModifiedDate, L.LastModifiedByUserID, L.LenderConfigID, L.LPQURL as LpqUrl1, L.LenderRef,  L.LenderID as LenderID1 FROM {0} B INNER JOIN {1} L ON B.LenderID = L.LenderID", BACKOFFICELENDERS_TABLE, LENDERCONFIG_TABLE)
                'Dim oWhere As New CSQLWhereStringBuilder()
                'oWhere.AppendAndCondition("L.LenderConfigID={0}", New CSQLParamValue(config.ID))
                'Dim oData = oDB.getDataTable(sSQL & oWhere.SQL)
                'If oData IsNot Nothing AndAlso oData.Rows.Count > 0 Then
                '	For Each row As DataRow In oData.Rows
                '		Dim match1 As Match = Regex.Match(Common.SafeString(row("LpqUrl1")), SmSettings.extractDomainRegexPattern, RegexOptions.IgnoreCase)
                '		Dim match2 As Match = Regex.Match(Common.SafeString(row("LpqUrl")), SmSettings.extractDomainRegexPattern, RegexOptions.IgnoreCase)
                '		If match1.Success AndAlso match2.Success AndAlso match1.Value = match2.Value Then
                '			backOfficeLenderID = Common.SafeGUID(row("BackOfficeLenderID"))
                '			Exit For
                '		End If
                '	Next
                'End If
                Dim oWhere As New CSQLWhereStringBuilder()
                oWhere.AppendAndCondition("LenderConfigID={0}", New CSQLParamValue(config.ID))
                Dim oUpdate As New cSQLUpdateStringBuilder(LENDERCONFIG_TABLE, oWhere)
                oUpdate.appendvalue("OrganizationID", New CSQLParamValue(Common.SafeGUID(config.OrganizationId)))
                oUpdate.appendvalue("LenderID", New CSQLParamValue(Common.SafeGUID(config.LenderId)))
                oUpdate.appendvalue("LenderRef", New CSQLParamValue(config.LenderRef))
                oUpdate.appendvalue("ReturnURL", New CSQLParamValue(config.ReturnURL))
                oUpdate.appendvalue("LPQLogin", New CSQLParamValue(config.LPQLogin))
                oUpdate.appendvalue("LPQPW", New CSQLParamValue(config.LPQPW))
                oUpdate.appendvalue("LPQURL", New CSQLParamValue(config.LPQURL))
                oUpdate.appendvalue("ConfigData", New CSQLParamValue(config.ConfigData))
                oUpdate.appendvalue("note", New CSQLParamValue(config.Note))
                oUpdate.appendvalue("LastModifiedDate", New CSQLParamValue(Now))
                oUpdate.appendvalue("LastModifiedByUserID", New CSQLParamValue(config.LastModifiedByUserID))
                oUpdate.appendvalue("LastOpenedByUserID", New CSQLParamValue(config.LastOpenedByUserID))
                oUpdate.appendvalue("IterationID", New CSQLParamValue(config.IterationID))
                oUpdate.appendvalue("RevisionID", New CSQLParamValue(config.RevisionID))
				oDB.executeNonQuery(oUpdate.SQL, True)
				SmBL.InvalidateExternallyCachedPortalConfigs(config.LenderRef, config.ConfigData)


				'update backOfficeLender table
				'If backOfficeLenderID <> Guid.Empty Then
				'	oWhere = New CSQLWhereStringBuilder()
				'	oWhere.AppendAndCondition("BackOfficeLenderID={0}", New CSQLParamValue(backOfficeLenderID))
				'	oUpdate = New cSQLUpdateStringBuilder(BACKOFFICELENDERS_TABLE, oWhere)
				'	oUpdate.appendvalue("Host", New CSQLParamValue(SmUtil.ExtractHostFromUrl(config.LPQURL)))
				'	oUpdate.appendvalue("LpqUrl", New CSQLParamValue(config.LPQURL))
				'	'oUpdate.appendvalue("LastModifiedDate", New CSQLParamValue(Now))
				'	'oUpdate.appendvalue("LastModifiedBy", New CSQLParamValue(config.LastModifiedBy))
				'	oDB.executeNonQuery(oUpdate.SQL, True)
				'End If
			Catch ex As CSQLException
                _log.Error("Error while check UpdateLenderConfig", ex)
                Throw New Exception("Error while saving to db", ex)    'need to propagate ex to top to display the message
            Catch ex As Exception
                _log.Error("Error while SaveLenderConfig", ex)
                Throw New Exception(ex.Message, ex) 'need to propagate ex to top to display the message
            Finally
                oDB.commitTransactionIfOpen()
				oDB.closeConnection()
			End Try

		End Sub

		'deprecated, no longer need
		'Public Function IsExistedLender(ByVal organizationID As String, ByVal lenderID As String) As Boolean
		'	Dim bIsExisted As Boolean = False
		'	Dim sSQL As String = "Select LenderConfigID From LenderConfigs"
		'	Dim oWhere As New CSQLWhereStringBuilder()
		'	oWhere.AppendAndCondition("OrganizationID = " & Common.SQLString(Common.SafeGUID(organizationID).ToString()))
		'	oWhere.AppendAndCondition("LenderID = " & Common.SQLString(Common.SafeGUID(lenderID).ToString()))

		'	Dim oDB As New CSQLDBUtils(LPQMOBILE_CONNECTIONSTRING)
		'	Try
		'		bIsExisted = oDB.getScalerValue(sSQL + oWhere.SQL()) <> ""
		'	Catch ex As Exception
		'		_log.Error("Error while check IsExistedLender", ex)
		'	Finally
		'		oDB.closeConnection()
		'	End Try

		'	Return bIsExisted
		'End Function

		Public Function GetLenderConfigs() As DataTable
			Dim oData As DataTable = New DataTable()

			Dim sSQL As String = String.Format("SELECT top 401 *, U.FirstName + ' ' + U.LastName as LastModifiedBy, U2.FirstName + ' ' + U2.LastName as LastOpenedBy  FROM {0} L LEFT JOIN {1} U on L.LastModifiedByUserID = U.UserID LEFT JOIN {1} U2 on L.LastOpenedByUserID = U2.UserID ", LENDERCONFIG_TABLE, USERS_TABLE)
			Dim oOrder As String = " order by LastModifiedDate DESC"
			Dim oDB As New CSQLDBUtils(LPQMOBILE_CONNECTIONSTRING)
			Try
				oData = oDB.getDataTable(sSQL & oOrder)
			Catch ex As Exception
				_log.Error("Error while GetLenderConfigs", ex)
			Finally
				oDB.closeConnection()
			End Try

			Return oData
		End Function

		Public Function SearchLenderConfigs(ByVal poConfig As CLenderConfig) As DataTable
			Dim oData As DataTable = New DataTable()
			Dim sSQL As String = String.Format("Select * From {0} B left join {1} U on B.LastModifiedByUserID = U.UserID ", LENDERCONFIG_TABLE, USERS_TABLE)
			Dim oWhere As New CSQLWhereStringBuilder()

			If poConfig.OrganizationId.ToString <> "00000000-0000-0000-0000-000000000000" Then
				oWhere.AppendAndCondition("OrganizationID = " & Common.SQLString(poConfig.OrganizationId.ToString))
			End If
			If poConfig.LenderId.ToString <> "00000000-0000-0000-0000-000000000000" Then
				oWhere.AppendAndCondition("LenderID = " & Common.SQLString(poConfig.LenderId.ToString))
			End If

			If poConfig.LenderRef <> "" Then
				oWhere.AppendAndCondition("LenderRef like '%" & poConfig.LenderRef & "%'")
			End If
			If poConfig.ReturnURL <> "" Then
				oWhere.AppendAndCondition("ReturnURL like '%" & poConfig.ReturnURL & "%'")
			End If

			If poConfig.LPQLogin <> "" Then
				oWhere.AppendAndCondition("LPQLogin like '%" & poConfig.LPQLogin & "%'")
			End If

			If poConfig.LPQURL <> "" Then
				oWhere.AppendAndCondition("LPQURL like '%" & poConfig.LPQURL & "%'")
			End If

			If poConfig.ConfigData <> "" Then
				oWhere.AppendAndCondition("ConfigData like '%" & poConfig.ConfigData & "%'")
			End If

			If poConfig.Note <> "" Then
				oWhere.AppendAndCondition("note like '%" & poConfig.Note & "%'")
			End If

			Dim oDB As New CSQLDBUtils(LPQMOBILE_CONNECTIONSTRING)
			Try
				oData = oDB.getDataTable(sSQL & oWhere.SQL & " order by LenderRef")
			Catch ex As Exception
				_log.Error("Error while SearchLenderConfigs", ex)
			Finally
				oDB.closeConnection()
			End Try

			Return oData
		End Function

		Public Function GetLenderConfigByID(ByVal ID As Guid) As CLenderConfig
			Dim oData As DataTable = New DataTable()

			Dim sSQL As String = String.Format("SELECT L.*, U.FirstName + ' ' + U.LastName as LastModifiedBy, U2.FirstName + ' ' + U2.LastName as LastOpenedBy  FROM {0} L LEFT JOIN {1} U on L.LastModifiedByUserID = U.UserID LEFT JOIN {1} U2 on L.LastOpenedByUserID = U2.UserID ", LENDERCONFIG_TABLE, USERS_TABLE)
			Dim oWhere As New CSQLWhereStringBuilder()
			oWhere.AppendAndCondition("LenderConfigID={0}", New CSQLParamValue(ID))
			Dim oDB As New CSQLDBUtils(LPQMOBILE_CONNECTIONSTRING)
			Try
				oData = oDB.getDataTable(sSQL & oWhere.SQL)
			Catch ex As Exception
				_log.Error("Error while GetLenderConfigByID", ex)
			Finally
				oDB.closeConnection()
			End Try

			Return ConvertLenderConfigToObject(oData)
		End Function


		Private Function ConvertLenderConfigToObject(ByVal oData As DataTable, Optional ByVal pbDecrypt As Boolean = True) As CLenderConfig
			Dim oLender As New CLenderConfig()

			If Not oData Is Nothing And oData.Rows.Count > 0 Then
				oLender.ID = Common.SafeGUID(oData.Rows(0)("LenderConfigID"))
				oLender.OrganizationId = Common.SafeGUID(oData.Rows(0)("OrganizationId"))
				oLender.LenderId = Common.SafeGUID(oData.Rows(0)("LenderId"))
				oLender.LenderRef = Common.SafeString(oData.Rows(0)("LenderRef"))
				oLender.ReturnURL = Common.SafeString(oData.Rows(0)("ReturnURL"))
				oLender.LPQLogin = Common.SafeString(oData.Rows(0)("LPQLogin"))
				oLender.LPQPW = Common.SafeString(oData.Rows(0)("LPQPW"))
				oLender.LPQURL = Common.SafeString(oData.Rows(0)("LPQURL"))
				oLender.LastModifiedDate = Common.SafeDate(oData.Rows(0)("LastModifiedDate"))
				oLender.Note = Common.SafeString(oData.Rows(0)("note"))
				oLender.LastModifiedByUserID = Common.SafeGUID(oData.Rows(0)("LastModifiedByUserID"))
				If oLender.LastModifiedByUserID <> Guid.Empty Then
					oLender.LastModifiedByUserFullName = Common.SafeString(oData.Rows(0)("LastModifiedBy"))
				End If
				If oData.Columns.Contains("RevisionID") Then
					oLender.RevisionID = Common.SafeInteger(oData.Rows(0)("RevisionID"))
				End If
				If oData.Columns.Contains("IterationID") Then
					oLender.IterationID = Common.SafeInteger(oData.Rows(0)("IterationID"))
					oLender.LastOpenedByUserID = Common.SafeGUID(oData.Rows(0)("LastOpenedByUserID"))
					oLender.LastOpenedByUserFullName = Common.SafeString(oData.Rows(0)("LastOpenedBy"))
					oLender.LastOpenedDate = Common.SafeDate(oData.Rows(0)("LastOpenedDate"))
				End If
				'need to overwrite/add attribute in here with attribute from sql
				oLender.ConfigData = CStringCompressor.Decompress(Common.SafeString(oData.Rows(0)("ConfigData")))
				If pbDecrypt = True Then
					oLender.LPQPW = Common.DecryptPassword(oLender.LPQPW)
				End If
				Try
					Dim oDocXml As New XmlDocument
					oDocXml.LoadXml(oLender.ConfigData)
					Dim oXmlElement As XmlElement = oDocXml.DocumentElement
					oXmlElement.SetAttribute("organization_id", oLender.OrganizationId.ToString.Replace("-", ""))
					oXmlElement.SetAttribute("lender_id", oLender.LenderId.ToString.Replace("-", ""))
					oXmlElement.SetAttribute("lender_ref", oLender.LenderRef)
					oXmlElement.SetAttribute("finished_url", oLender.ReturnURL)
					oXmlElement.SetAttribute("user", oLender.LPQLogin)
					oXmlElement.SetAttribute("password", oLender.LPQPW)
					oXmlElement.SetAttribute("loan_submit_url", oLender.LPQURL)
					'oLender.ConfigData = Common.SafeString(oData.Rows(0)("ConfigData"))
					If pbDecrypt = True Then
						'by running DecryptAllPasswordInConfigXml after oXmlElement.SetAttribute("password", oLender.LPQPW), we reduce 1 time decrypt password call
						oXmlElement = Common.DecryptAllPasswordInConfigXml(oXmlElement)
					End If
					oLender.ConfigData = oXmlElement.OuterXml
				Catch ex As Exception
					oLender.ConfigData = Common.SafeString(oData.Rows(0)("ConfigData"))
					_log.Warn("Fail to update ConfigData: " & oLender.ConfigData, ex)
				End Try
			End If

			Return oLender
		End Function

		'deprecated, only used for ipad getconfig
		Public Function GetLenderConfigByLenderAndOrg(ByVal orgID As Guid, ByVal lenderID As Guid) As CLenderConfig
			Dim oData As DataTable = New DataTable()
			Dim sSQL As String = String.Format("SELECT L.*, U.FirstName + ' ' + U.LastName as LastModifiedBy, U2.FirstName + ' ' + U2.LastName as LastOpenedBy  FROM {0} L LEFT JOIN {1} U on L.LastModifiedByUserID = U.UserID LEFT JOIN {1} U2 on L.LastOpenedByUserID = U2.UserID ", LENDERCONFIG_TABLE, USERS_TABLE)
			Dim oWhere As New CSQLWhereStringBuilder()
			oWhere.AppendANDCondition("OrganizationID = " & Common.SQLString(orgID.ToString().ToUpper()))
			oWhere.AppendANDCondition("LenderID = " & Common.SQLString(lenderID.ToString().ToUpper()))

			Dim oDB As New CSQLDBUtils(LPQMOBILE_CONNECTIONSTRING)
			Try
				oData = oDB.getDataTable(sSQL & oWhere.SQL)
			Catch ex As Exception
				_log.Error("Error while GetLenderConfigByLenderAndOrg", ex)
			Finally
				oDB.closeConnection()
			End Try

			Return ConvertLenderConfigToObject(oData)
		End Function

		''' <summary>
		''' Ipad appp will use config from both live and test db.  This function serve as a fall back when config is not found in live
		''' </summary>
		''' <param name="orgID"></param>
		''' <param name="lenderID"></param>
		''' <returns></returns>
		''' <remarks></remarks>
		Public Function GetLenderConfigByLenderAndOrg_fromTestDB(ByVal orgID As Guid, ByVal lenderID As Guid) As CLenderConfig
			Dim oData As DataTable = New DataTable()
			Dim sSQL As String = String.Format("SELECT L.*, U.FirstName + ' ' + U.LastName as LastModifiedBy, U2.FirstName + ' ' + U2.LastName as LastOpenedBy  FROM {0} L LEFT JOIN {1} U on L.LastModifiedByUserID = U.UserID LEFT JOIN {1} U2 on L.LastOpenedByUserID = U2.UserID ", LENDERCONFIG_TABLE, USERS_TABLE)
			Dim oWhere As New CSQLWhereStringBuilder()
			oWhere.AppendANDCondition("OrganizationID = " & Common.SQLString(orgID.ToString().ToUpper()))
			oWhere.AppendANDCondition("LenderID = " & Common.SQLString(lenderID.ToString().ToUpper()))

			Dim oDB As New CSQLDBUtils(LPQMOBILE_CONNECTIONSTRING_TEST)
			Try
				oData = oDB.getDataTable(sSQL & oWhere.SQL)
				_log.Debug("GetLenderConfigByLenderAndOrg_fromTestDB sql : " & sSQL & oWhere.SQL)
			Catch ex As Exception
				_log.Error("Error while GetLenderConfigByLenderAndOrg from apptest", ex)
			Finally
				oDB.closeConnection()
			End Try

			Return ConvertLenderConfigToObject(oData)
		End Function
		
		'shouyld be deprecated, use GetLenderConfigByLenderConfigID instead
		'Public Function GetLenderConfigByLenderID(ByVal lenderID As String) As CLenderConfig
		'	Dim oData As DataTable = New DataTable()
		'	Dim sSQL As String = String.Format("SELECT L.*, U.FirstName + ' ' + U.LastName as LastModifiedBy, U2.FirstName + ' ' + U2.LastName as LastOpenedBy  FROM {0} L LEFT JOIN {1} U on L.LastModifiedByUserID = U.UserID LEFT JOIN {1} U2 on L.LastOpenedByUserID = U2.UserID ", LENDERCONFIG_TABLE, USERS_TABLE)
		'	Dim oWhere As New CSQLWhereStringBuilder()
		'	oWhere.AppendAndCondition("LenderID = {0} ", New CSQLParamValue(Common.SafeGUID(lenderID)))

		'	'TODO: this is for system refresh, update lender_code based on LPQ host domain, should comment out for normal operation
		'	'oWhere.AppendOptionalAndCondition("LPQURL", New CSQLParamValue("%cs.loanspq.com%"), "like")

		'	Dim oDB As New CSQLDBUtils(LPQMOBILE_CONNECTIONSTRING)
		'	Try
		'		oData = oDB.getDataTable(sSQL & oWhere.SQL)
		'	Catch ex As Exception
		'		_log.Error("Error while GetLenderConfigByLenderID", ex)
		'	Finally
		'		oDB.closeConnection()
		'	End Try

		'	Return ConvertLenderConfigToObject(oData)
		'End Function

		'may use this in the future
		'Private Shared Sub SetCache(Of T)(sCacheKey As String, data As T)
		'    HttpContext.Current.Cache.Insert(sCacheKey, data, Nothing, Caching.Cache.NoAbsoluteExpiration, New TimeSpan(0, 0, 15))  'h, m, 5sec
		'    HttpContext.Current.Application(sCacheKey) = DateTime.Now
		'End Sub

		'Public Function GetCachedLenderConfigByLenderConfigID(ByVal poLenderConfigID As System.Guid) As CLenderConfig
		'    Dim oLenderConfig As CLenderConfig
		'    oLenderConfig = CType(HttpContext.Current.Cache.Get(poLenderConfigID.ToString), CLenderConfig)
		'    If oLenderConfig IsNot Nothing Then Return oLenderConfig

		'    'get from db, decrypt and save to cache
		'    oLenderConfig = GetLenderConfigByLenderConfigID(poLenderConfigID)
		'    SetCache(poLenderConfigID.ToString, oLenderConfig)
		'    Return oLenderConfig

		'End Function

		Public Function GetLenderConfigByLenderConfigID(ByVal poLenderConfigID As System.Guid, Optional ByVal pbDecrypt As Boolean = True) As CLenderConfig
            If poLenderConfigID = Nothing OrElse poLenderConfigID = Guid.Empty Then Return New CLenderConfig

            Dim oData As DataTable = New DataTable()
            Dim sSQL As String = String.Format("SELECT L.*, U.FirstName + ' ' + U.LastName as LastModifiedBy, U2.FirstName + ' ' + U2.LastName as LastOpenedBy  FROM {0} L LEFT JOIN {1} U on L.LastModifiedByUserID = U.UserID LEFT JOIN {1} U2 on L.LastOpenedByUserID = U2.UserID ", LENDERCONFIG_TABLE, USERS_TABLE)
			Dim oWhere As New CSQLWhereStringBuilder()
			oWhere.AppendAndCondition("LenderConfigID = {0} ", New CSQLParamValue(poLenderConfigID))

			Dim oDB As New CSQLDBUtils(LPQMOBILE_CONNECTIONSTRING)
			Try
				oData = oDB.getDataTable(sSQL & oWhere.SQL)
			Catch ex As Exception
				_log.Error("Error while GetLenderConfigByLenderConfigID", ex)
			Finally
				oDB.closeConnection()
			End Try

			Return ConvertLenderConfigToObject(oData, pbDecrypt)
		End Function


		'deprecated, use LenderConfigID instead
		'Public Function GetLenderConfigLogByLenderID(ByVal lenderID As Guid) As CLenderConfig
		'	Dim oData As DataTable = New DataTable()
		'	Dim sSQL As String = "Select TOP 1 * From LenderConfigLogs"
		'	Dim oWhere As New CSQLWhereStringBuilder()
		'	oWhere.AppendAndCondition("LenderID = {0}", New CSQLParamValue(lenderID))

		'	oWhere.AppendAndCondition("LogTime < " & "(Select MAX(LogTime) From LenderConfigLogs where UPPER(LenderID) = " & Common.SQLString(lenderID.ToString().ToUpper()) & ")")

		'	Dim oOrder As String = " order by LogTime DESC"

		'	Dim oDB As New CSQLDBUtils(LPQMOBILE_CONNECTIONSTRING)
		'	Try
		'		oData = oDB.getDataTable(sSQL & oWhere.SQL & oOrder)
		'	Catch ex As Exception
		'		_log.Error("Error while GetLenderConfigByLenderID", ex)
		'	Finally
		'		oDB.closeConnection()
		'	End Try

		'	Return ConvertLenderConfigToObject(oData)
		'End Function

		'deprecated
		'Public Function GetLenderConfigLogByLenderRef(ByVal psLenderRef As String) As CLenderConfig
		'	Dim oData As DataTable = New DataTable()
		'	Dim sSQL As String = "Select TOP 1 * From LenderConfigLogs"
		'	Dim oWhere As New CSQLWhereStringBuilder()
		'	oWhere.AppendAndCondition("UPPER(LenderRef) = {0}", New CSQLParamValue(psLenderRef.NullSafeToUpper_))

		'	oWhere.AppendAndCondition("LogTime < " & "(Select MAX(LogTime) From LenderConfigLogs where UPPER(LenderRef) = '" & psLenderRef.NullSafeToUpper_ & "')")

		'	Dim oOrder As String = " order by LogTime DESC"

		'	Dim oDB As New CSQLDBUtils(LPQMOBILE_CONNECTIONSTRING)
		'	Try
		'		oData = oDB.getDataTable(sSQL & oWhere.SQL & oOrder)
		'	Catch ex As Exception
		'		_log.Error("Error while GetLenderConfigLogByLenderRef", ex)
		'	Finally
		'		oDB.closeConnection()
		'	End Try

		'	Return ConvertLenderConfigToObject(oData)
		'End Function

		Public Function GetLenderConfigLogByLenderConfigID(ByVal poLenderConfigID As System.Guid) As CLenderConfig
			Dim oData As DataTable = New DataTable()
			Dim sSQL As String = String.Format("SELECT TOP 1 L.*, U.FirstName + ' ' + U.LastName as LastModifiedBy FROM {0} L LEFT JOIN {1} U on L.LastModifiedByUserID = U.UserID ", LENDERCONFIGLOGS_TABLE, USERS_TABLE)
			Dim oWhere As New CSQLWhereStringBuilder()
			oWhere.AppendAndCondition("LenderConfigID = {0}", New CSQLParamValue(poLenderConfigID))

			oWhere.AppendANDCondition("LogTime < " & "(Select MAX(LogTime) From LenderConfigLogs where LenderConfigID = '" & poLenderConfigID.ToString & "')")

			Dim oOrder As String = " order by LogTime DESC"

			Dim oDB As New CSQLDBUtils(LPQMOBILE_CONNECTIONSTRING)
			Try
				oData = oDB.getDataTable(sSQL & oWhere.SQL & oOrder)
			Catch ex As Exception
				_log.Error("Error while GetLenderConfigLogByLenderConfigID", ex)
			Finally
				oDB.closeConnection()
			End Try

			Dim lenderConfig = ConvertLenderConfigToObject(oData)
			Return lenderConfig
		End Function
		
		'Public Function GetLenderConfigByLenderRef(ByVal lenderRef As String) As CLenderConfig
		'	Dim oData As DataTable = New DataTable()
		'	Dim sSQL As String = String.Format("SELECT L.*, U.FirstName + ' ' + U.LastName as LastModifiedBy, U2.FirstName + ' ' + U2.LastName as LastOpenedBy  FROM {0} L LEFT JOIN {1} U on L.LastModifiedByUserID = U.UserID LEFT JOIN {1} U2 on L.LastOpenedByUserID = U2.UserID ", LENDERCONFIG_TABLE, USERS_TABLE)
		'	Dim oWhere As New CSQLWhereStringBuilder()
		'	oWhere.AppendAndCondition("UPPER(LenderRef)={0}", New CSQLParamValue(lenderRef.NullSafeToUpper_))
		'	'oWhere.AppendAndCondition("UPPER(LenderRef) = " & Common.SQLString(lenderRef.ToString().ToUpper()))

		'	Dim oDB As New CSQLDBUtils(LPQMOBILE_CONNECTIONSTRING)
		'	Try
		'		oData = oDB.getDataTable(sSQL & oWhere.SQL)
		'	Catch ex As Exception
		'		_log.Error("Error while GetLenderConfigByLenderRef", ex)
		'	Finally
		'		oDB.closeConnection()
		'	End Try

		'	'TODO:overrite some attribute
		'	Return ConvertLenderConfigToObject(oData)
		'End Function

		Private Shared _CacheLock_LENDER_CONFIGS As New Object
		Public Function GetLenderConfig(bUseLenderRef As Boolean, ByVal psLenderId As String, ByVal psLenderRef As String) As CLenderConfig
			Dim oLenderConfig As CLenderConfig
			Dim sSQL As String = String.Format("SELECT L.*, U.FirstName + ' ' + U.LastName as LastModifiedBy, U2.FirstName + ' ' + U2.LastName as LastOpenedBy  FROM {0} L LEFT JOIN {1} U on L.LastModifiedByUserID = U.UserID LEFT JOIN {1} U2 on L.LastOpenedByUserID = U2.UserID ", LENDERCONFIG_TABLE, USERS_TABLE)
			Dim oWhere As New CSQLWhereStringBuilder()
			If bUseLenderRef Then
				oWhere.AppendAndCondition("UPPER(LenderRef)={0}", New CSQLParamValue(psLenderRef.NullSafeToUpper_))
			Else  'this is for a few legacy portals that has only one portal per lender, may no longer be valid
				oWhere.AppendAndCondition("LenderID = {0} ", New CSQLParamValue(Common.SafeGUID(psLenderId)))
			End If

			Dim oDB As New CSQLDBUtils(LPQMOBILE_CONNECTIONSTRING)
			Try
				Dim oData = oDB.getDataTable(sSQL & oWhere.SQL)
				If oData IsNot Nothing And oData.Rows.Count > 0 Then
					oLenderConfig = New CLenderConfig
					oLenderConfig.LastModifiedDate = Common.SafeDate(oData.Rows(0)("LastModifiedDate"))
					oLenderConfig.ID = Common.SafeGUID(oData.Rows(0)("LenderConfigID"))
					oLenderConfig.LenderId = Common.SafeGUID(oData.Rows(0)("LenderID"))
					Dim cacheKey As String = "CONFIG_XML_" & oLenderConfig.ID.NullSafeToUpper_	 'this xml config is unique per portal
					Dim cachedData = CType(HttpContext.Current.Cache.Get(cacheKey), CLenderConfig)
					If cachedData Is Nothing OrElse oLenderConfig.LastModifiedDate > cachedData.LastModifiedDate Then
						SyncLock _CacheLock_LENDER_CONFIGS
							cachedData = CType(HttpContext.Current.Cache.Get(cacheKey), CLenderConfig)
							If cachedData Is Nothing OrElse oLenderConfig.LastModifiedDate > cachedData.LastModifiedDate Then
								oLenderConfig = ConvertLenderConfigToObject(oData)
								HttpContext.Current.Cache.Remove(cacheKey)
								HttpContext.Current.Cache.Insert(cacheKey, oLenderConfig, Nothing, Caching.Cache.NoAbsoluteExpiration, New TimeSpan(22, 0, 0))
								'HttpContext.Current.Application(cacheKey) = DateTime.Now
								'_log.Error("Get LenderConfig from DB")
							Else
								'fail-safe to throw a hard error if the returned lender config does not match the requested lenderref
								If oLenderConfig.LenderId <> cachedData.LenderId Then
									HttpContext.Current.Cache.Remove(cacheKey)
									Throw New Exception("Unexpected corrupted cache content!")
								End If
								oLenderConfig = cachedData
								'_log.Error("Get LenderConfig from cache")
							End If
						End SyncLock
					Else
						'fail-safe to throw a hard error if the returned lender config does not match the requested lenderref
						If oLenderConfig.LenderId <> cachedData.LenderId Then
							HttpContext.Current.Cache.Remove(cacheKey)
							Throw New Exception("Unexpected corrupted cache content!")
						End If
						oLenderConfig = cachedData
						'_log.Error("Get LenderConfig from cache")
					End If
				End If
			Catch ex As Exception
				_log.Error("Error while GetLenderConfig", ex)
			Finally
				oDB.closeConnection()
			End Try
			'TODO:overrite some attribute
			Return oLenderConfig
		End Function


		Public Function GetBackOfficeLenderByLenderRef(lenderRef As String) As SmBackOfficeLender
			Dim result As SmBackOfficeLender = Nothing
			Dim oData As New DataTable()
			Dim sSQL As String = String.Format("select B.*, L.LenderConfigID, L.LPQURL as LpqUrl1, L.LenderRef, L.LastModifiedDate, L.LastModifiedByUserID from {0} B inner join {1} L on B.LenderID = L.LenderID", BACKOFFICELENDERS_TABLE, LENDERCONFIG_TABLE)
			Dim oWhere As New CSQLWhereStringBuilder()
			Dim extractDomainRegexPattern As String = "^(?:https?:\/\/)?(?:[^@\n]+@)?(?:www\.)?([^:\/\n]+)"
			oWhere.AppendAndCondition("L.LenderRef={0}", New CSQLParamValue(lenderRef))
			Dim oDB As New CSQLDBUtils(LPQMOBILE_CONNECTIONSTRING)
			Try
				oData = oDB.getDataTable(sSQL & oWhere.SQL)
				If oData IsNot Nothing AndAlso oData.Rows.Count > 0 Then
					For Each row As DataRow In oData.Rows
						Dim match1 As Match = Regex.Match(Common.SafeString(row("LpqUrl1")), extractDomainRegexPattern, RegexOptions.IgnoreCase)
						Dim match2 As Match = Regex.Match(Common.SafeString(row("LpqUrl")), extractDomainRegexPattern, RegexOptions.IgnoreCase)
						If match1.Success AndAlso match2.Success AndAlso match1.Value = match2.Value Then
							Dim item = New SmBackOfficeLender()
							item.BackOfficeLenderID = Common.SafeGUID(row("BackOfficeLenderID"))
							item.LenderName = Common.SafeString(row("LenderName"))
							item.LenderID = Common.SafeGUID(row("LenderID"))
							item.LpqUrl = Common.SafeString(row("LpqUrl"))
							item.City = Common.SafeString(row("City"))
							item.State = Common.SafeString(row("State"))
							If Not row.IsNull("LastModifiedDate") Then
								item.LastModifiedDate = Common.SafeDate(row("LastModifiedDate"))
							End If
							item.LastModifiedByUserID = Common.SafeGUID(row("LastModifiedByUserID"))
							item.Host = Common.SafeString(match2.Value)
							result = item
							Exit For
						End If
					Next
				End If
			Catch ex As Exception
				_log.Error("Error while GetBackOfficeLenderByLenderRef", ex)
			Finally
				oDB.closeConnection()
			End Try
			Return result
		End Function

		'TODO: this should be tie to LenderConfigID
		'should be deprecated
		'Public Function GetAuditLogs(lenderId As String, organizationId As String) As DataTable
		'	Dim oData As DataTable = New DataTable()
		'	Dim sSQL As String = "SELECT TOP 20  *, ' ' as AuditChanges FROM LenderConfigLogs WHERE LenderID = '" & lenderId & "' AND OrganizationID='" & organizationId & "' AND configData <> '' ORDER BY LogTime DESC"

		'	Dim oDB As New CSQLDBUtils(LPQMOBILE_CONNECTIONSTRING)
		'	Try
		'		oData = oDB.getDataTable(sSQL)
		'	Catch ex As Exception
		'		_log.Error("Error while GetAuditLogs", ex)
		'	Finally
		'		oDB.closeConnection()
		'	End Try

		'	Return oData
		'End Function

		Public Function GetAuditLogs(ByVal psLenderConfigID As Guid) As List(Of CLenderConfigLog)
			Dim result As New List(Of CLenderConfigLog)
            Dim sSQL As String = String.Format("SELECT LenderConfigLogID, OrganizationID, LenderID, LenderRef, ReturnURL, LPQLogin, LPQPW, LPQURL, LogTime, RemoteIP, note, LenderConfigID, RevisionID, Comment, LastModifiedByUserID, CachedDiff, ' ' as AuditChanges, U.FirstName + ' ' + U.LastName as LastModifiedByUserName FROM {0} L LEFT JOIN {1} U on L.LastModifiedByUserID = U.UserID ", LENDERCONFIGLOGS_TABLE, USERS_TABLE)
            Dim oWhere As New CSQLWhereStringBuilder()
			oWhere.AppendAndCondition("LenderConfigID={0}", New CSQLParamValue(psLenderConfigID))
            oWhere.AppendAndCondition("configData<>{0}", New CSQLParamValue(""))
            oWhere.AppendAndCondition("LogTime >= {0}", New CSQLParamValue(Date.Today.AddYears(-1)))
            Dim oOrder As String = " ORDER BY LogTime DESC"
            Dim oDB As New CSQLDBUtils(LPQMOBILE_CONNECTIONSTRING)
			Try
				Dim oData As DataTable = New DataTable()
				oData = oDB.getDataTable(sSQL & oWhere.SQL & oOrder)
				If oData IsNot Nothing AndAlso oData.Rows.Count > 0 Then
					For Each row As DataRow In oData.Rows
						Dim logItem As New CLenderConfigLog
                        logItem.AuditChanges = ""
                        If Not IsDBNull(row("CachedDiff")) Then
                            If Not String.IsNullOrWhiteSpace(row("CachedDiff").ToString()) Then
                                logItem.AuditChanges = row("CachedDiff").ToString()
                            End If
                        End If
                        If Not IsDBNull(row("LPQLogin")) Then
							logItem.LPQLogin = row("LPQLogin").ToString()
						End If
						If Not IsDBNull(row("LPQPW")) Then
							logItem.LPQPW = row("LPQPW").ToString()
						End If
						If Not IsDBNull(row("LPQURL")) Then
							logItem.LPQURL = row("LPQURL").ToString()
						End If
						If Not IsDBNull(row("LenderConfigID")) Then
							logItem.LenderConfigID = Common.SafeGUID(row("LenderConfigID"))
						End If
						If Not IsDBNull(row("LenderConfigLogID")) Then
							logItem.LenderConfigLogID = Common.SafeGUID(row("LenderConfigLogID"))
						End If
						If Not IsDBNull(row("LenderID")) Then
							logItem.LenderID = Common.SafeGUID(row("LenderID"))
						End If
						If Not IsDBNull(row("LenderRef")) Then
							logItem.LenderRef = row("LenderRef").ToString()
						End If
						If Not IsDBNull(row("LogTime")) Then
							logItem.LogTime = Common.SafeDate(row("LogTime"))
						End If
						If Not IsDBNull(row("RemoteIP")) Then
							logItem.RemoteIP = row("RemoteIP").ToString()
						End If
						If Not IsDBNull(row("LastModifiedByUserID")) Then
							logItem.LastModifiedByUserID = Common.SafeGUID(row("LastModifiedByUserID"))
							logItem.LastModifiedByUserName = Common.SafeString(row("LastModifiedByUserName"))
						End If
						If Not IsDBNull(row("note")) Then
							logItem.Note = row("note").ToString()
						End If
						If Not IsDBNull(row("Comment")) Then
							logItem.Comment = row("Comment").ToString()
						End If
						If Not IsDBNull(row("OrganizationID")) Then
							logItem.OrganizationID = Common.SafeGUID(row("OrganizationID"))
						End If
						If Not IsDBNull(row("ReturnURL")) Then
							logItem.ReturnURL = row("ReturnURL").ToString()
						End If
						result.Add(logItem)
					Next
				End If
			Catch ex As Exception
				_log.Error("Error while GetAuditLogs", ex)
			Finally
				oDB.closeConnection()
			End Try
			Return result
		End Function


		Public Function GetLenderConfigPreview(ByVal previewCode As String) As CLenderConfig
			Dim oData As DataTable = New DataTable()
			Dim sSQL As String = String.Format("SELECT L.*, U.FirstName + ' ' + U.LastName as LastModifiedBy, U2.FirstName + ' ' + U2.LastName as LastOpenedBy  FROM {0} L LEFT JOIN {1} U on L.LastModifiedByUserID = U.UserID LEFT JOIN {1} U2 on L.LastOpenedByUserID = U2.UserID ", LENDERCONFIGPREVIEWS_TABLE, USERS_TABLE)
			Dim oWhere As New CSQLWhereStringBuilder()
			oWhere.AppendAndCondition("UPPER(PreviewCode) = " & Common.SQLString(previewCode.ToUpper()))

			Dim oDB As New CSQLDBUtils(LPQMOBILE_CONNECTIONSTRING)
			Try
				oData = oDB.getDataTable(sSQL & oWhere.SQL)
			Catch ex As Exception
				_log.Error("Error while GetLenderConfigPreview", ex)
			Finally
				oDB.closeConnection()
			End Try

			'TODO:overrite some attribute
			Return ConvertLenderConfigToObject(oData)
		End Function


		Public Function GenerateSecurityCode(lenderRef As String, email As String, lastName As String) As String
            Dim oInsert As New cSQLInsertStringBuilder("GetLoanStatusSecurityCodes")
            Dim oGui As String = Guid.NewGuid().ToString().Replace("-", "")
            Dim code As String = oGui.Substring(0, 6)
            Dim codeDB As String = oGui.Substring(0, 19)  'this increas the possible combination to 99999999999999999999
            oInsert.AppendValue("Code", New CSQLParamValue(codeDB))
            oInsert.AppendValue("LenderRef", New CSQLParamValue(lenderRef))
			oInsert.AppendValue("Email", New CSQLParamValue(email))
			oInsert.AppendValue("LastName", New CSQLParamValue(lastName))
			oInsert.AppendValue("Status", New CSQLParamValue(0))
			oInsert.AppendValue("GeneratedDate", New CSQLParamValue(DateTime.Now))
			Dim oDB As New CSQLDBUtils(LPQMOBILE_CONNECTIONSTRING)
			Try
				oDB.executeNonQuery(oInsert.SQL, False)
			Catch ex As Exception
				_log.Error("Error while GenerateSecurityCode", ex)
				code = ""
			Finally
				oDB.closeConnection()
			End Try
			Return code
		End Function

	End Class

End Namespace

