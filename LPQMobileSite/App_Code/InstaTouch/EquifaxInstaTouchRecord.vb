﻿Imports Microsoft.VisualBasic

Namespace InstaTouch

	Public Class EquifaxInstaTouchRecord
		Public Property ID As Guid
		Public Property RecordType As RecordType
		Public Property ConsumerFirstName As String
		Public Property ConsumerLastName As String
        Public Property DateRequested As DateTimeOffset
        Public Property DateResponded As DateTimeOffset
        Public Property TransactionID As Guid
		Public Property SessionID As String
        Public Property MerchantId As String
        Public Property LenderID As Guid
		Public Property LenderRef As String
		Public Property LoanType As String
		Public Property IPAddress As String
	End Class

	Public Enum RecordType
		OptIn
		Prefill
	End Enum

End Namespace