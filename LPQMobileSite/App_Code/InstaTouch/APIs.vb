﻿Imports System.Net.Http
Imports System.Threading.Tasks
Imports System.Web.UI.WebControls.Expressions
Imports System.Net.Http.Headers
Imports System.Net
Imports Newtonsoft.Json
Imports System.Configuration
Imports System.Text.RegularExpressions

Namespace InstaTouch

	Public MustInherit Class InstaTouchAPI
		Private _log As log4net.ILog = log4net.LogManager.GetLogger(GetType(InstaTouchAPI))
		Private _baseUrl As String

		''' <summary>
		''' This is the InstaTouch domain that is the same across all
		''' API calls. It it set by this abstract base class and should
		''' not be changed.
		''' </summary>
		''' <value></value>
		''' <returns></returns>
		''' <remarks></remarks>
		Public Property BaseURL() As String
			Get
				Return _baseUrl
			End Get
			Set(value As String)
				_baseUrl = value
			End Set
		End Property

        Public MustOverride ReadOnly Property Method() As Method
        ''' <summary>
        ''' The request input to send to the API endpoint.
        ''' </summary>
        ''' <value></value>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Property RequestObj As IRequest
        ''' <summary>
        ''' The raw string content of the HTTP response. This is for debugging and 
        ''' logging purposes, since the Request method deserializes this into an object automatically.
        ''' </summary>
        ''' <value></value>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Property ResponseContent As String

        ''' <summary>
        ''' The client used to make the HTTP requests. Set to <see cref="HttpClient"/> by
        ''' default by the base class, but can be overridden if needed.
        ''' </summary>
        ''' <value></value>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Protected Property _Client As HttpClient

        Public Sub New(requestObj As IRequest)
            'Get the base domain from the configuration
            Me.BaseURL = ConfigurationManager.AppSettings.Get("InstaTouchBaseUrl")
            Me.RequestObj = requestObj
            _Client = New HttpClient()
        End Sub

		Public Overridable Function Request(Of T As {IResponse, New})(ParamArray params As Object()) As T
			Dim relativePath = Method.RelativePath
			Dim httpMethod = Method.HttpMethod
			' This supports replacing parts of the url (marked by brackets, e.g. /api/v2/[id]/options) with
			' the parameters inputted into this method. 
			' Note: These don't happen to be used in InstaTouch V2. This code will remain for if it becomes needed in the future.

			' Validate the number of parameters
			Dim regex As New Regex("(\[[^\]]+\])")
			If regex.Matches(relativePath).Count <> params.Count Then
				Throw New ArgumentException("Number of parameters do not match definition", "params")
			End If
			'Perform the bracketed item replacement
			Dim i As Integer = 0
			relativePath = regex.Replace(relativePath, Function(p)
														   i += 1
														   Return params(i).ToString()
													   End Function)
			'Prepare the request
			Dim requestUri As New Uri(New Uri(Me.BaseURL), relativePath)
			Dim requestBody As String = ""
			If RequestObj IsNot Nothing Then
				requestBody = RequestObj.ToBodyString()
			End If
			Dim _request As New HttpRequestMessage(httpMethod, requestUri.AbsoluteUri)
			If RequestObj.Headers IsNot Nothing AndAlso RequestObj.Headers.Count > 0 Then
				For Each header In RequestObj.Headers
					If header.Key = "Authorization" Then
						_request.Headers.Authorization = New AuthenticationHeaderValue("Bearer", header.Value)
					Else
						_request.Headers.Add(header.Key, header.Value)
					End If
				Next
			End If
			Dim sRequestCall = getRequestCall(requestUri.AbsoluteUri)
			_log.Info(sRequestCall & ": InstaTouch API request to " & requestUri.AbsoluteUri)
			If Not String.IsNullOrWhiteSpace(requestBody) AndAlso requestBody.Trim() <> "{}" Then
				' InstaTouch doesn't seem to work when the Content-Type is 'application/json; UTF-8',
				' so we override the ContentType and don't include the encoding.
				_request.Content = New StringContent(requestBody)
				_request.Content.Headers.ContentType = New MediaTypeHeaderValue(RequestObj.ContentType)
				_log.Info(sRequestCall & ": InstaTouch API request Data " & RequestObj.MaskRequestForLogging(requestBody))
			End If
			Dim response = _Client.SendAsync(_request).GetAwaiter().GetResult()
			Try
				Me.ResponseContent = response.Content.ReadAsStringAsync().GetAwaiter().GetResult()
			Catch ex As Exception
				_log.Error("InstaTouchAPI: Failed to read response body.", ex)
				ResponseContent = String.Empty
			End Try
			If response.StatusCode <> HttpStatusCode.OK Then
				_log.WarnFormat("ERROR Response from InstaTouchAPI: StatusCode={0}, Content='{1}'", response.StatusCode, ResponseContent)
				Return Nothing
			End If

			Dim maskedResponseBody As String = New T().MaskResponseForLogging(Me.ResponseContent)
			_log.Debug(sRequestCall & ": InstaTouch API Response:" & maskedResponseBody)

			If ResponseContent.StartsWith("jsonp(") Then
				Dim pattern As New Regex("jsonp\((.*)\)")
				Dim match = pattern.Match(ResponseContent)
				ResponseContent = match.Groups(2).Value.Trim()
			End If
			Return JsonConvert.DeserializeObject(Of T)(ResponseContent)
		End Function
		Protected Function getRequestCall(ByVal sUrl As String) As String
            sUrl = sUrl.Substring(sUrl.LastIndexOf("/"))
            Dim sCall = ""
            If sUrl.Contains("token") Then
                sCall = "Call 1 - Authorization"
            ElseIf sUrl.Contains("user-sessions") Then
                sCall = "Call 2 - Handshake"
            ElseIf sUrl.Contains("network-identifications") Then
                sCall = "Call 4 - Update MDN"
            ElseIf sUrl.Contains("user-consents") Then
                sCall = "Call 5 - Consent"
            ElseIf sUrl.Contains("send") Then
                sCall = "Call 6a - Send One-Time Passcode(OTP)"
            ElseIf sUrl.Contains("validate") Then
                sCall = "Call 6b - Validate OPT"
            ElseIf sUrl.Contains("user-attributes") Then
                sCall = "Call 7 - Identity"
            End If
            Return sCall
        End Function
    End Class

    Public Class Method
        ''' <summary>
        ''' The relative path may include bracketed input parameters,
        ''' e.g. /api/v1/[id]/options. The '[id]' will be replaced.
        ''' See <see cref="InstaTouch.IRequest"/>" for more information.
        ''' </summary>
        ''' <value></value>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Property RelativePath As String
        Public Property HttpMethod As HttpMethod

        Public Sub New(relativePath As String, httpMethod As HttpMethod)
            Me.RelativePath = relativePath
            Me.HttpMethod = httpMethod
        End Sub
    End Class
#Region "APIs"
    ''' <summary>
    ''' Call 1
    ''' </summary>
    ''' <remarks></remarks>
    Public Class AuthorizationAPI
        Inherits InstaTouchAPI

        Public Sub New(requestObj As AuthorizationRequest)
            MyBase.New(requestObj)
        End Sub

        Public Overrides ReadOnly Property Method() As Method
            Get
                Return New Method("v1/oauth/token", HttpMethod.Post)
            End Get
        End Property
    End Class

    ''' <summary>
    ''' Call 2
    ''' </summary>
    ''' <remarks></remarks>
    Public Class HandshakeAPI
        Inherits InstaTouchAPI

        Public Sub New(requestObj As HandshakeRequest)
            MyBase.New(requestObj)
        End Sub

        Public Overrides ReadOnly Property Method As Method
            Get
                Return New Method("business/instatouch-identity/v2/user-sessions", HttpMethod.Post)
            End Get
        End Property
    End Class

    'Call 3 - Header Enrichment: must be invoked using an <img> tag

    ''' <summary>
    ''' Call 4
    ''' </summary>
    ''' <remarks></remarks>
    Public Class UpdateMDNAPI
        Inherits InstaTouchAPI
        Public Sub New(requestObj As UpdateMDNRequest)
            MyBase.New(requestObj)
        End Sub
        Public Overrides ReadOnly Property Method As Method
            Get
                Return New Method("business/instatouch-identity/v2/user-sessions/network-identifications", HttpMethod.Post)
            End Get
        End Property
    End Class

    ''' <summary>
    ''' Call 5
    ''' </summary>
    ''' <remarks></remarks>
    Public Class ConsentAPI
        Inherits InstaTouchAPI
        Public Sub New(requestObj As ConsentRequest)
            MyBase.New(requestObj)
        End Sub
        Public Overrides ReadOnly Property Method As Method
            Get
                Return New Method("business/instatouch-identity/v2/user-sessions/user-consents", HttpMethod.Post)
            End Get
        End Property
    End Class

    ''' <summary>
    ''' Call 6a
    ''' </summary>
    ''' <remarks></remarks>
    Public Class SendOTPAPI
        Inherits InstaTouchAPI
        Public Sub New(requestObj As SendOTPRequest)
            MyBase.New(requestObj)
        End Sub
        Public Overrides ReadOnly Property Method As Method
            Get
                Return New Method("business/instatouch-identity/v2/wifi-sessions/identity-verifications/one-time-passcodes/send", HttpMethod.Post)
            End Get
        End Property
    End Class


    ''' <summary>
    ''' Call 6b
    ''' </summary>
    ''' <remarks></remarks>
    Public Class ValidateOTPAPI
        Inherits InstaTouchAPI
        Public Sub New(requestObj As ValidateOTPRequest)
            MyBase.New(requestObj)
        End Sub
        Public Overrides ReadOnly Property Method As Method
            Get
                Return New Method("business/instatouch-identity/v2/wifi-sessions/identity-verifications/one-time-passcodes/validate", HttpMethod.Post)
            End Get
        End Property
    End Class

    ''' <summary>
    ''' Call 7
    ''' </summary>
    ''' <remarks></remarks>
    Public Class IdentityAPI
        Inherits InstaTouchAPI
        Public Sub New(requestObj As IdentityRequest)
            MyBase.New(requestObj)
        End Sub
        Public Overrides ReadOnly Property Method As Method
            Get
                Return New Method("business/instatouch-identity/v2/user-sessions/user-attributes", HttpMethod.Post)
            End Get
        End Property
    End Class

#End Region
End Namespace
