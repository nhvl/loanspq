﻿Imports System.Threading.Tasks
Imports Newtonsoft.Json
Imports System.Configuration

Namespace InstaTouch
    Public MustInherit Class IRequest
        Protected Const INSTATOUCH_AUTHORIZATION_TOKEN = "Authorization"
        Protected Const EFX_SESSIONID As String = "efx-sessionId"
        Public Property Headers As List(Of RequestHeader)

        Public Function ShouldSerializeHeaders() As Boolean
            Return False
        End Function
        Public Overridable Property ContentType As String = "application/json"
        Public Overridable Function MaskRequestForLogging(sRequest As String) As String
            Return sRequest
        End Function
		Public Overridable Function ToBodyString() As String
			Return JsonConvert.SerializeObject(Me)
		End Function
	End Class
    Public Class AuthorizationRequest
        Inherits IRequest
        Public Property client_id As String
        Public Property client_secret As String
        Public Property grant_type As String
        Public Property scope As String
        Public Sub New()
            Dim instaTouchConsumerKey As String = ConfigurationManager.AppSettings.Get("InstaTouchConsumerKey")
            Dim instaTouchConsumerSecret As String = ConfigurationManager.AppSettings.Get("InstaTouchConsumerSecret")
            Dim instaTouchOauthGrantType As String = ConfigurationManager.AppSettings.Get("InstaTouchOauthGrantType")
            Dim instaTouchAuthorizationV2 As String = ConfigurationManager.AppSettings.Get("InstaTouchAuthorizationV2")
            Dim instaTouchOauthScope As String = ConfigurationManager.AppSettings.Get("InstaTouchOauthScope")
            If String.IsNullOrWhiteSpace(instaTouchConsumerKey) OrElse String.IsNullOrWhiteSpace(instaTouchConsumerSecret) OrElse String.IsNullOrWhiteSpace(instaTouchOauthGrantType) OrElse String.IsNullOrWhiteSpace(instaTouchOauthScope) Then
                Throw New Exception("InstaTouch V2 is attempting an authorization when some values are not configured correctly.")
            End If
            ContentType = "application/x-www-form-urlencode"
            client_id = instaTouchConsumerKey
            client_secret = instaTouchConsumerSecret
            grant_type = instaTouchOauthGrantType
            scope = instaTouchOauthScope
            'Headers = New List(Of RequestHeader)()
            'Headers.Add(New RequestHeader("x-efx-consumer-key", instaTouchConsumerKey))
            'Headers.Add(New RequestHeader("x-efx-consumer-secret", instaTouchConsumerSecret))
            'Headers.Add(New RequestHeader("x-efx-oauth-grant-type", instaTouchOauthGrantType))
            'Headers.Add(New RequestHeader("Authorization", instaTouchAuthorizationV2))
        End Sub
        Public Overrides Function MaskRequestForLogging(ByVal sRequest As String) As String
            Dim idxClientSecret As Integer = sRequest.IndexOf("client_secret") + "client_secret".Length + 1
            Dim idxAfterClientSecret As Integer = sRequest.Substring(idxClientSecret).IndexOf("&")
            Return sRequest.Substring(0, idxClientSecret) & "[MASKED]" & sRequest.Substring(idxClientSecret).Substring(idxAfterClientSecret)
        End Function
		Public Overrides Function ToBodyString() As String
			Return New System.Net.Http.FormUrlEncodedContent(New Dictionary(Of String, String) From {
				{"client_id", client_id},
				{"client_secret", client_secret},
				{"grant_type", grant_type},
				{"scope", scope}
		}).ReadAsStringAsync().GetAwaiter().GetResult()
		End Function
	End Class

    Public Class HandshakeRequest
        Inherits IRequest

        Public Property merchantId As String
        Public Sub New(sMerchantId As String, accessToken As String)
            Headers = New List(Of RequestHeader)
            Headers.Add(New RequestHeader(INSTATOUCH_AUTHORIZATION_TOKEN, accessToken))
            merchantId = sMerchantId
        End Sub
    End Class

    Public Class UpdateMDNRequest
        Inherits IRequest
        Public Property merchantId As String
        Public Sub New(sMerchantId As String, accessToken As String, sessionId As String)
            Headers = New List(Of RequestHeader)
            Headers.Add(New RequestHeader(INSTATOUCH_AUTHORIZATION_TOKEN, accessToken))
            Headers.Add(New RequestHeader(EFX_SESSIONID, sessionId))
            merchantId = sMerchantId
        End Sub
    End Class

    Public Class ConsentRequest
        Inherits IRequest
        Public Property merchantId As String
        Public Property consumerIdentifier As ConsumerIdentifier
        Public Property consumerConsent As ConsumerConsent
        Public Sub New(sMerchantId As String, accessToken As String, sessionId As String)
            Headers = New List(Of RequestHeader)
            Headers.Add(New RequestHeader(INSTATOUCH_AUTHORIZATION_TOKEN, accessToken))
            Headers.Add(New RequestHeader(EFX_SESSIONID, sessionId))
            merchantId = sMerchantId
            consumerIdentifier = New ConsumerIdentifier() With {.subjectIdentifier = sessionId, .subjectType = "SESSIONID"}
            consumerConsent = New ConsumerConsent() With {.consentEventDate = DateTimeOffset.UtcNow.ToString("o"), .consentEvent = "opt-in", .consentType = "SMS", .consentMethod = "other", .consentForServices = {"identity"}}
        End Sub
    End Class

    Public Class SendOTPRequest
        Inherits IRequest
        Public Property merchantId As String
        Public Property consumerIdentifier As ConsumerIdentifier
        Public Sub New(sMerchantId As String, accessToken As String, sessionId As String, phone As String)
            Headers = New List(Of RequestHeader)
            Headers.Add(New RequestHeader(INSTATOUCH_AUTHORIZATION_TOKEN, accessToken))
            Headers.Add(New RequestHeader(EFX_SESSIONID, sessionId))
            merchantId = sMerchantId
            consumerIdentifier = New ConsumerIdentifier() With {.subjectIdentifier = phone, .subjectType = "MDN"}
        End Sub
    End Class

    Public Class ValidateOTPRequest
        Inherits IRequest
        Public Property merchantId As String
        Public Property consumerIdentifier As ConsumerIdentifier
        Public Property consumerAuthentication As ConsumerAuthentication
        Public Property otpLifecycle As OTPLifecycle
        Public Sub New(sMerchantId As String, accessToken As String, sessionId As String, phone As String, code As String, transactionKey As String)
            Headers = New List(Of RequestHeader)
            Headers.Add(New RequestHeader(INSTATOUCH_AUTHORIZATION_TOKEN, accessToken))
            Headers.Add(New RequestHeader(EFX_SESSIONID, sessionId))
            consumerIdentifier = New ConsumerIdentifier() With {.subjectIdentifier = phone, .subjectType = "MDN"}
            consumerAuthentication = New ConsumerAuthentication() With {.passcode = code}
            otpLifecycle = New OTPLifecycle() With {.transactionKey = transactionKey}
            merchantId = sMerchantId
        End Sub
    End Class

    Public Class IdentityRequest
        Inherits IRequest

        Public Property merchantId As String
        Public Property outputCode As String
        Public Property authentication As Authentication
        Public Sub New(sMerchantId As String, accessToken As String, sessionId As String, zip As String, ssn As String)
            Headers = New List(Of RequestHeader)
            Headers.Add(New RequestHeader(INSTATOUCH_AUTHORIZATION_TOKEN, accessToken))
            Headers.Add(New RequestHeader(EFX_SESSIONID, sessionId))
            merchantId = sMerchantId
            outputCode = "AO"
            authentication = New Authentication() With {.zipCode = zip, .ssn = ssn}
        End Sub
    End Class

#Region "Data Classes"

    Public Class ConsumerIdentifier
        Public Property subjectIdentifier As String
        Public Property subjectType As String
    End Class

    Public Class ConsumerConsent
        Public Property consentEventDate As String
        Public Property consentEvent As String
        Public Property consentType As String
        ''' <summary>
        ''' Any of: identity, location, jobhistory, phone
        ''' </summary>
        ''' <value></value>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Property consentMethod As String
        Public Property consentForServices As String()
    End Class

    Public Class ConsumerAuthentication
        Public Property passcode As String
    End Class
    Public Class OTPLifecycle
        Public Property status As String
        Public Property transactionKey As String
    End Class

    Public Class Authentication
        Public Property zipCode As String
        Public Property ssn As String
    End Class

    Public Class RequestHeader
        Public Property Key As String
        Public Property Value As String
        Public Sub New(key As String, value As String)
            Me.Key = key
            Me.Value = value
        End Sub
    End Class
#End Region

End Namespace