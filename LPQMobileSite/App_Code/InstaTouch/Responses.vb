﻿Imports Newtonsoft.Json

Namespace InstaTouch

	Public MustInherit Class IResponse

		Protected Const Mask As String = "[***MASKED***]"

		''' <summary>
		''' Override to perform masking if necessary before logging.
		''' </summary>
		''' <param name="sRequest">The serialized message, in JSON.</param>
		''' <returns>The same as <paramref name="sRequest"/> except with sensitive fields masked.</returns>
		Public Overridable Function MaskResponseForLogging(sRequest As String) As String
			Return sRequest
		End Function

		Public Overridable Function ToJsonString() As String
			Return JsonConvert.SerializeObject(Me)
		End Function
	End Class

	Public Class AuthorizationResponse
		Inherits IResponse
		Public Property access_token As String
		Public Property expires_in_secs As String

		Public Overrides Function MaskResponseForLogging(sRequest As String) As String
			Try
				Dim obj = Linq.JObject.Parse(sRequest)
				obj.SelectToken("access_token").Replace(Mask)
				Return JsonConvert.SerializeObject(obj)
			Catch ex As Exception
				Return sRequest
			End Try
		End Function
	End Class

	Public Class HandshakeResponse
		Inherits IResponse
		Public Property transactionid As String
        Public Property sessionID As String
        Public Property dateTime As String
		Public Property instaTouch As String
        Public Property carrier As String
    End Class

	Public Class UpdateMDNResponse
		Inherits IResponse
        Public Property transactionId As String
        Public Property status As String
	End Class

	Public Class ConsentResponse
		Inherits IResponse
		Public Property transactionID As String
		Public Property consumerIdentifier As ConsumerIdentifier
	End Class

	Public Class OTPResponse
		Inherits IResponse
        Public Property transactionId As String
        Public Property otpLifecycle As OTPLifecycle
        Public Property InstaTouch As String
    End Class

	Public Class IdentityResponse
		Inherits IResponse
		Public Property transactionID As String
		Public Property identity As Identity
		Public Property identification As Identification

		Public Overrides Function MaskResponseForLogging(sRequest As String) As String
			Try
				Dim obj = Linq.JObject.Parse(sRequest)
				obj.SelectToken("identification.ssn").Replace(Mask)
				Return JsonConvert.SerializeObject(obj)
			Catch ex As Exception
				Return sRequest
			End Try
		End Function
	End Class

	Public Class Identity
		Public Property id As String
		Public Property name As Name
        Public Property addresses As List(Of Address)
        Public Property contact As Contact
	End Class

	Public Class Identification
		Public Property ssn As String
		Public Property dob As String
	End Class

	Public Class Name
		Public Property firstName As String
		Public Property lastName As String
	End Class
	Public Class Address
		Public Property streetAddress As String
		Public Property city As String
		Public Property state As String
		Public Property zipCode As String
	End Class
	Public Class Contact
		Public Property homePhone As String
		Public Property mobile As String
		Public Property email As String
	End Class
End Namespace