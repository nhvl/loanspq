﻿Imports DBUtils
Imports LPQMobile.Utils

Namespace InstaTouch

	Public Class Utils
		Protected Shared LPQMOBILE_CONNECTIONSTRING As String = System.Web.Configuration.WebConfigurationManager.ConnectionStrings("LPQMOBILE_CONNECTIONSTRING").ConnectionString


		Private Shared _log As log4net.ILog = log4net.LogManager.GetLogger(GetType(Utils))

		Public Shared Sub WriteEquifaxInstaTouchLog(record As EquifaxInstaTouchRecord)
			Dim oDB As New CSQLDBUtils(LPQMOBILE_CONNECTIONSTRING)
			Try
				Dim oInsert As New cSQLInsertStringBuilder("EquifaxInstaTouchRecords")
				oInsert.AppendValue("ID", New CSQLParamValue(Guid.NewGuid()))
				oInsert.AppendValue("RecordType", New CSQLParamValue(record.RecordType.ToString()))
				oInsert.AppendValue("ConsumerFirstName", New CSQLParamValue(record.ConsumerFirstName))
				oInsert.AppendValue("ConsumerLastName", New CSQLParamValue(record.ConsumerLastName))
                oInsert.AppendValue("DateRequested", New CSQLParamValue(record.DateRequested.ToString()))
                oInsert.AppendValue("DateResponded", New CSQLParamValue(record.DateResponded.ToString()))
                oInsert.AppendValue("TransactionID", New CSQLParamValue(record.TransactionID))
				oInsert.AppendValue("SessionID", New CSQLParamValue(record.SessionID))
                oInsert.AppendValue("MerchantId", New CSQLParamValue(record.MerchantId))
                oInsert.AppendValue("LenderID", New CSQLParamValue(record.LenderID))
				oInsert.AppendValue("LenderRef", New CSQLParamValue(record.LenderRef))
				oInsert.AppendValue("LoanType", New CSQLParamValue(record.LoanType))
				oInsert.AppendValue("IPAddress", New CSQLParamValue(record.IPAddress))
				oDB.executeNonQuery(oInsert.SQL, False)
				_log.Info(CSecureStringFormatter.MaskSensitiveData(oInsert.SQL)) ''want to keep track the record in the logview
			Catch ex As Exception
				_log.Error("Error while WriteEquifaxInstaTouchLog", ex)
			Finally
				oDB.closeConnection()
			End Try
		End Sub

		Public Shared Sub WriteInstaTouchRequestTracking(lenderID As Guid, lenderRef As String, remoteIP As String)
			Dim oDB As New CSQLDBUtils(LPQMOBILE_CONNECTIONSTRING)
			Try
				Dim oInsert As New cSQLInsertStringBuilder("EquifaxInstaTouchRequestTracking")
				oInsert.AppendValue("ID", New CSQLParamValue(Guid.NewGuid()))
				oInsert.AppendValue("LogTime", New CSQLParamValue(DateTime.Now))
				oInsert.AppendValue("LenderID", New CSQLParamValue(lenderID))
				oInsert.AppendValue("LenderRef", New CSQLParamValue(lenderRef))
				oInsert.AppendValue("RemoteIP", New CSQLParamValue(remoteIP))
				oDB.executeNonQuery(oInsert.SQL, False)
			Catch ex As Exception
				_log.Error("Error while WriteInstaTouchRequestTracking", ex)
			Finally
				oDB.closeConnection()
			End Try
		End Sub

		Public Shared Function CountInstaTouchRequestTracking(lenderID As Guid, lenderRef As String, ipAddress As String) As Integer
			Dim oDB As New CSQLDBUtils(LPQMOBILE_CONNECTIONSTRING)
			Dim counter As Integer = 0
			Try
				Dim oWhere As New CSQLWhereStringBuilder()
				'number of same ip within the time frame
				Dim sSql As String = "Select count(RemoteIP) from EquifaxInstaTouchRequestTracking"
				oWhere.AppendAndCondition("LogTime > DATEADD(HOUR,-1,GETDATE())")
				oWhere.AppendAndCondition("RemoteIP={0}", New CSQLParamValue(ipAddress))
				oWhere.AppendAndCondition("LenderID={0}", New CSQLParamValue(lenderID))
				counter = Common.SafeInteger(oDB.getScalerValue(sSql & oWhere.SQL))
			Catch ex As Exception
				_log.Error("Error while CountInstaTouchRequestTracking", ex)
			Finally
				oDB.closeConnection()
			End Try
			Return counter
		End Function
	End Class

End Namespace