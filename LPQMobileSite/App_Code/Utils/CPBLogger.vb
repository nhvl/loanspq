﻿
Imports log4net
Imports log4net.Core
Imports Microsoft.VisualBasic

'Public Class CPBLogger

'	'Public Shared PBLogger As New PaulBunyan.PBLogger

'	Sub New()
'		'PBLogger.Context = "LPQMOBILE"
'	End Sub

'	Public Shared Sub logError(ByVal pErrorMessage As String, ByVal pErrorException As Exception)
'		'PBLogger.Log("ERROR", pErrorMessage & pErrorException.ToString)
'	End Sub


'	Public Shared Sub logInfo(ByVal pMsg As String)
'		'PBLogger.Log("INFO", pMsg)
'	End Sub

'End Class

Public Class CPBLogger  

    Public Shared PBLogger As New PBLogger()

    Sub New()
        PBLogger.Context = "LPQMOBILE"
    End Sub

    Public Shared Sub logError(ByVal pErrorMessage As String, ByVal pErrorException As Exception)
        PBLogger.Log.Error(pErrorMessage, pErrorException)
    End Sub

    Public Shared Sub logInfo(ByVal pMsg As String)
        PBLogger.Log.Info(pMsg)
    End Sub

End Class

Public Class PBLogger
    Public Property Context As String = ""
    Public Property Component As String
    Public Property File As String

    Public ReadOnly Property Log As log4net.ILog
        Get
            Return log4net.LogManager.GetLogger(Context)
        End Get
    End Property

End Class
