﻿Imports Microsoft.VisualBasic

Public Class CJsonResponse

    Public Sub New()

    End Sub

    Public Sub New(_IsSuccess As Boolean)
        IsSuccess = _IsSuccess
        Message = ""
    End Sub

    Public Sub New(_IsSuccess As Boolean, _Message As String)
        IsSuccess = _IsSuccess
        Message = _Message
    End Sub

    Public Sub New(_IsSuccess As Boolean, _Message As String, _Info As Object)
        IsSuccess = _IsSuccess
        Message = _Message
        Info = _Info
    End Sub

    Public Property IsSuccess As Boolean
    Public Property Message As String
    Public Property Info As Object

End Class
