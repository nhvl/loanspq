﻿Imports Microsoft.VisualBasic
Imports System.IO
Imports LPQMobile.Utils.Common
Imports System.Web
Public Class CRoutingInformation

    Private ReadOnly _log As log4net.ILog = log4net.LogManager.GetLogger(Me.GetType)
    Public Property RoutingNumber As String
    Public Property BankName As String
    Public Property City As String
    Public Property State As String
    Public Property Address As String

    'These values include the extra ending space.  This is for file downloaded from LPQ routing numner.
    Private Const ROUTING_NUMBER_LENGTH As Integer = 9
    'length of the string between the routing number and bank name that we dont need
    Private Const DONT_CARE_LENGTH As Integer = 26  '10-36
    Private Const BANK_NAME_LENGTH As Integer = 36   '36 - 72
    Private Const BANK_ADDRESS_LENGTH As Integer = 36 '72-108
    Private Const BANK_CITY_LENGTH As Integer = 20   '108-128
    Private Const BANK_STATE_LENGTH As Integer = 2   '128-130
    Private Const BANK_ZIP_LENGTH As Integer = 5



    Public ReadOnly Property CityAndState As String
        Get
            Dim result = ""
            If Not String.IsNullOrWhiteSpace(City) AndAlso Not String.IsNullOrWhiteSpace(State) Then
                result = City.Trim() & ", " & State.Trim()
            Else
                result = If(City IsNot Nothing, City.Trim(), "") & If(State IsNot Nothing, State.Trim(), "")
            End If
            Return result
        End Get
    End Property

    Private ReadOnly _path As String = ""

    Private Sub New()
        If HttpContext.Current Is Nothing Then
            _log.Error("Could not get HttpContext.Current. Hence could not use Server.MapPath to find RoutingInfo.txt file")
        Else
            _path = HttpContext.Current.Server.MapPath(LPQMobile.Utils.Common.ROUTING_INFO_PATH)
        End If
    End Sub

    Private Shared _instance As CRoutingInformation
    ''' <summary>
    ''' Get Singleton Instance
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared ReadOnly Property Instance() As CRoutingInformation
        Get
            If _instance Is Nothing Then
                _instance = New CRoutingInformation()
            End If
            Return _instance
        End Get
    End Property

    ''' <summary>
    ''' Find BankName and State from matched routing number param
    ''' </summary>
    ''' <param name="bankRoutingNumber">Routing number to seek</param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function GetData(bankRoutingNumber As String) As CRoutingInformation
        Dim list = seekRoutingNumber(_path, New List(Of String) From {bankRoutingNumber})
        If list IsNot Nothing AndAlso list.Count > 0 Then
            Return list(0)
        End If
        Return Nothing
    End Function

    ''' <summary>
    ''' Find List of BankName and State from matched routing numbers param
    ''' </summary>
    ''' <param name="routingNumbers">List of Routing numbers to seek</param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function GetData(routingNumbers As List(Of String)) As List(Of CRoutingInformation)
        Return seekRoutingNumber(_path, routingNumbers)
    End Function

    Private Function seekRoutingNumber(path As String, routingNums As List(Of String)) As List(Of CRoutingInformation)
        Dim list As New List(Of CRoutingInformation)
        Try
            Using sr As New StreamReader(path)
                While sr.Peek() >= 0
                    Dim s = sr.ReadLine()
                    '' init a variable to store founded routing number
                    Dim foundNum As String = ""
                    '' terminate seeking if there's no routing number to seek
                    If routingNums.Count = 0 Then
                        Exit While
                    End If
                    '' loop each routing number in routingNums param
                    For Each num In routingNums
                        Dim obj As CRoutingInformation = parseLine(s, num)
                        If obj IsNot Nothing Then '' founded one!
                            list.Add(obj)
                            '' store founded number
                            foundNum = num
                        Else
                            Continue For
                        End If
                    Next
                    '' delete just founded number from list
                    If foundNum <> "" Then
                        routingNums.Remove(foundNum)
                    End If
                End While
            End Using
        Catch ex As Exception
            _log.Error("Could not load data from " & path, ex)
        End Try
        Return list
    End Function

    'This is the old format that require delimiter "|"
    'Private Function parseLine(line As String, routingNum As String) As CRoutingInformation
    '    '' dont worry about empty space or missing since the file already formatted well.
    '    '' it always has 5 columns.
    '    Dim columns = line.Split("|".ToCharArray(), StringSplitOptions.None)
    '    '' find matching routing number
    '    If columns(0) = routingNum Then
    '        Dim info As New CRoutingInformation()
    '        info.RoutingNumber = columns(0)
    '        info.BankName = columns(1).Trim()
    '        ''info.Address = columns(2).Trim() '' ignore for now
    '        info.City = columns(3).Trim()
    '        info.State = columns(4).Trim()
    '        Return info
    '    End If
    '    Return Nothing
    'End Function

    'This parse line from downloaded routing number file from LPQ with the assumed format
    Private Function parseLine(line As String, routingNum As String) As CRoutingInformation

        Dim sRoutingNumber As String = ""
        Dim sBankName As String = ""
        Dim sBankAddress As String = ""
        Dim sBankZip As String = ""
        Dim sBankCity As String = ""
        Dim sBankState As String = ""
        Dim oStringReader As New StringReaderSimple(line)
        Dim sDontCare As String = ""

        sRoutingNumber = SafeString(oStringReader.Read(ROUTING_NUMBER_LENGTH))
        sDontCare = SafeString(oStringReader.Read(DONT_CARE_LENGTH))
        sBankName = SafeString(oStringReader.Read(BANK_NAME_LENGTH))
        sBankAddress = SafeString(oStringReader.Read(BANK_ADDRESS_LENGTH))
        sBankCity = SafeString(oStringReader.Read(BANK_CITY_LENGTH))
        sBankState = SafeString(oStringReader.Read(BANK_STATE_LENGTH))
        sBankZip = SafeString(oStringReader.Read(BANK_ZIP_LENGTH))

        If sRoutingNumber <> routingNum Then Return Nothing

        Dim info As New CRoutingInformation()
        info.RoutingNumber = sRoutingNumber
        info.BankName = sBankName
        info.Address = sBankAddress
        info.City = sBankCity
        info.State = sBankState
        Return info
    End Function

    'class adapts and provides an easy interface to read/pop strings from a buffer
    'the reverse equivalent of this would be the StringBuilder class
    Public Class StringReaderSimple

        Private _stm As System.IO.TextReader
        Public Sub New(ByVal psData As String)
            'stringreader doesn't like reading nulls :-P
            Me.New(New System.IO.StringReader(psData & ""))
        End Sub

        Public Sub New(ByVal stm As System.IO.TextReader)
            _stm = stm
            _eof = False
            _TotalBytesRead = 0
        End Sub

        Private _eof As Boolean
        Public ReadOnly Property EOF() As Boolean
            Get
                Return _eof
            End Get
        End Property

        Private _TotalBytesRead As Long
        Public ReadOnly Property TotalBytesRead() As Long
            Get
                Return _TotalBytesRead
            End Get
        End Property

        Public Overloads Function Read(ByVal pnLength As Integer) As String
            If pnLength <= 0 Then
                Return ""
            End If

            Dim bRead() As Char
            ReDim bRead(pnLength - 1)
            Dim nCount As Integer = _stm.Read(bRead, 0, bRead.Length)

            _TotalBytesRead += nCount

            _eof = _stm.Peek = -1 'eof OR stream doesn't support seeking

            Dim sResult As String = New String(bRead, 0, nCount)
            Return sResult
        End Function

        Public Sub Close()
            _stm.Close()
        End Sub
    End Class


End Class
