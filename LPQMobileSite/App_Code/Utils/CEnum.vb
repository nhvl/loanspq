﻿Imports System.ComponentModel
Imports Microsoft.VisualBasic

Public Class CEnum

	Public Shared Function SafeParseWithDefault(Of T As Structure)(sValue As String, tDefault As T) As T
		Dim eValue As T = tDefault
		If System.Enum.TryParse(sValue, eValue) Then
			Return eValue
		End If

		Return tDefault
	End Function

	Public Shared Function SafeParseWithDefaultNothing(Of T As Structure)(sValue As String) As T?
		Dim eValue As T = Nothing
		If System.Enum.TryParse(sValue, eValue) Then
			Return eValue
		End If

		Return Nothing
	End Function

	Public Shared OPTION_TAG_FORMAT As String = "<option Then Return '{0}'>{1}</option>"

	Public Shared MONTHS As String() = {"JAN - 1", "FEB - 2", "MAR - 3", "APR - 4", "MAY - 5", "JUN - 6", "JUL - 7", "AUG - 8", "SEP - 9", _
										"OCT - 10", "NOV - 11", "DEC - 12"}

	'Public Shared PROPERTY_TYPES As String() = {"SINGLE FAMILY RESIDENCE", "2 UNIT", "3 UNIT", "4 UNIT", "TOWNHOUSE", "CONDO", "MANUFACTURED HOME"}
	'Public Shared Function MapPropertyTypeToValue(ByVal psPropertyType As String) As String
	'    If psPropertyType = "SINGLE FAMILY RESIDENCE" Then Return "SFR"
	'    If psPropertyType = "CONDO" Then Return "LOW RISE CONDO"
	'    Return psPropertyType
	'End Function

	Public Shared PERSONAL_LOAN_PURPOSES As String() = {"CONSOLIDATION", "NEW PERSONAL LOAN", "REFINANCE", "SUMMER/HOLIDAY LOAN SPECIAL"}
	Public Shared CREDIT_CARD_LOAN_PURPOSES As String() = {"LIMIT INCREASE", "NEW VISA", "CARD UPGRADE"}


	'Public Shared VEHICLE_LOAN_PURPOSES As String() = {"AUTO PURCHASE", "AUTO REFINANCE", "LEASE BUYOUT"}

	Public Shared OCCUPY_LOCATIONS As String() = {"BUYING/OWN WITH MORTGAGE", "LIVE WITH PARENTS", "OWN - FREE AND CLEAR", "RENT", "GOVERNMENT QUARTERS", "OTHER"}
	Public Shared Function MapOccupancyTypeToValue(ByVal psOccupancyType As String) As String
		If psOccupancyType = "LIVE WITH PARENTS" Then Return "LIVEWITHPARENTS"
		If psOccupancyType = "BUYING/OWN WITH MORTGAGE" Then Return "BUYING"
		If psOccupancyType = "OWN - FREE AND CLEAR" Then Return "OWN"
		If psOccupancyType = "GOVERNMENT QUARTERS" Then Return "GOVQUARTERS"
		If psOccupancyType = "RENT" Then Return "RENT"
		If psOccupancyType = "OTHER" Then Return "OTHER"

		Return psOccupancyType
	End Function

    Public Shared STATES As String() = {"AL", "AK", "AS", "AZ", "AR", "AA", "AE", "AP", "CA", "CO", "CT", "DE", "DC", "FL", "FM",
                                        "GA", "GU", "HI", "ID", "IL", "IN", "IA", "KS", "KY", "LA", "ME",
                                        "MD", "MA", "MH", "MI", "MN", "MS", "MO", "MP", "MT", "NE", "NV", "NH",
                                        "NJ", "NM", "NY", "NC", "ND", "OH", "OK", "OR", "PA", "PR", "PW",
                                        "RI", "SC", "SD", "TN", "TX", "UT", "VT", "VI", "VA", "WA", "WV",
                                        "WI", "WY"}

    Public Shared Function MapStatesToString(ByVal state As String) As String
		If state = "AA" Then Return "Armed Forces - Americas"
		If state = "AE" Then Return "Armed Forces – Europe"
		If state = "AL" Then Return "Alabama"
		If state = "AK" Then Return "Alaska"
		If state = "AP" Then Return "Armed Forces – Pacific"
		If state = "AS" Then Return "American Samoa"
		If state = "AZ" Then Return "Arizona"
		If state = "AR" Then Return "Arkansas"
		If state = "CA" Then Return "California"
		If state = "CO" Then Return "Colorado"
		If state = "CT" Then Return "Connecticut"
		If state = "DE" Then Return "Delaware"
		If state = "DC" Then Return "District Of Columbia"
        If state = "FL" Then Return "Florida"
        If state = "FM" Then Return "Federated States of Micronesia"
        If state = "GA" Then Return "Georgia"
		If state = "GU" Then Return "Guam"
		If state = "HI" Then Return "Hawaii"
		If state = "ID" Then Return "Idaho"
		If state = "IL" Then Return "Illinois"
		If state = "IN" Then Return "Indiana"
		If state = "IA" Then Return "Iowa"
		If state = "KS" Then Return "Kansas"
		If state = "KY" Then Return "Kentucky"
		If state = "LA" Then Return "Louisiana"
		If state = "ME" Then Return "Maine"
		If state = "MD" Then Return "Maryland"
        If state = "MA" Then Return "Massachusetts"
        If state = "MH" Then Return "Marshall Islands"
        If state = "MI" Then Return "Michigan"
		If state = "MN" Then Return "Minnesota"
		If state = "MS" Then Return "Mississippi"
        If state = "MO" Then Return "Missouri"
        If state = "MP" Then Return "Northern Mariana Islands"
        If state = "MT" Then Return "Montana"
		If state = "NE" Then Return "Nebraska"
		If state = "NV" Then Return "Nevada"
		If state = "NH" Then Return "New Hampshire"
		If state = "NJ" Then Return "New Jersey"
		If state = "NM" Then Return "New Mexico"
		If state = "NY" Then Return "New York"
		If state = "NC" Then Return "North Carolina"
		If state = "ND" Then Return "North Dakota"
		If state = "OH" Then Return "Ohio"
		If state = "OK" Then Return "Oklahoma"
		If state = "OR" Then Return "Oregon"
		If state = "PA" Then Return "Pennsylvania"
        If state = "PR" Then Return "Puerto Rico"
        If state = "PW" Then Return "Palau"
        If state = "RI" Then Return "Rhode Island"
		If state = "SC" Then Return "South Carolina"
		If state = "SD" Then Return "South Dakota"
		If state = "TN" Then Return "Tennessee"
		If state = "TX" Then Return "Texas"
		If state = "UT" Then Return "Utah"
		If state = "VI" Then Return "Virgin Islands"
		If state = "VT" Then Return "Vermont"
		If state = "VA" Then Return "Virginia"
		If state = "WA" Then Return "Washington"
		If state = "WV" Then Return "West Virginia"
		If state = "WI" Then Return "Wisconsin"
		If state = "WY" Then Return "Wyoming"

		Return ""
	End Function

	Public Shared EMPLOYMENT_STATUS As String() = {"EMPLOYED", "ACTIVE MILITARY", "HOMEMAKER", "RETIRED", _
												   "SELF EMPLOYED", "STUDENT", "UNEMPLOYED", "OTHER", "RETIRED MILITARY", "GOVERNMENT/DOD"}
	Public Shared Function MapEmploymentStatusToValue(ByVal psEmploymentStatus As String) As String
		If psEmploymentStatus = "EMPLOYED" Then Return "EM"
		If psEmploymentStatus = "ACTIVE MILITARY" Then Return "MI"
		If psEmploymentStatus = "HOMEMAKER" Then Return "HO"
		If psEmploymentStatus = "OWNER" Then Return "OW"
		If psEmploymentStatus = "RETIRED" Then Return "RE"
		If psEmploymentStatus = "SELF EMPLOYED" Then Return "SE"
		If psEmploymentStatus = "STUDENT" Then Return "ST"
		If psEmploymentStatus = "UNEMPLOYED" Then Return "UN"
		If psEmploymentStatus = "OTHER" Then Return "OT"
		If psEmploymentStatus = "RETIRED MILITARY" Then Return "RM"
		If psEmploymentStatus = "GOVERNMENT/DOD" Then Return "GOV"
		If psEmploymentStatus = "" Then Return "OT" 'required for decision XA
		Return psEmploymentStatus
	End Function

	Public Shared Function MapBranchOfServiceToValue(ByVal psValue As String) As String
		If psValue = "Coast Guard" Then Return "COAST_GUARD"
		If psValue = "Air Force" Then Return "AIR_FORCE"
		If psValue = "Army" Then Return "ARMY"
		If psValue = "CIA" Then Return "CIA"
		If psValue = "Marine Corps" Then Return "MARINES"
		If psValue = "Navy" Then Return "NAVY"
		If psValue = "ORS" Then Return "ORS"
		Return psValue
	End Function

	Public Shared MARITAL_STATUS As String() = {"MARRIED", "REGISTERED DOMESTIC PARTNER", "SEPARATED", "UNMARRIED"}
	'TODO: need to define more options for this. For now, it's not described in specs (APP-11)
	Public Shared OFFICIAL_FAMILY_OPTIONS As String() = {"NONE"}

	Public Shared Function MapMaritalStatusToValue(ByVal psMaritalStatus As String) As String
		If psMaritalStatus = "MARRIED" Then Return "MARRIED"
		If psMaritalStatus = "REGISTERED DOMESTIC PARTNER" Then Return "DOMESTIC_PARTNER"
		If psMaritalStatus = "SEPARATED" Then Return "SEPARATED"
		If psMaritalStatus = "UNMARRIED" Then Return "UNMARRIED"
		Return psMaritalStatus
	End Function

	Public Shared CITIZENSHIP_STATUS As String() = {"PERM RESIDENT", "US CITIZEN"}
	Public Shared Function MapCitizenshipStatusToValue(ByVal psCitizenshipStatus As String) As String
		'If psCitizenshipStatus = "NON-PERM RESIDENT" Then Return "NONPERMRESIDENT"
		If psCitizenshipStatus = "PERM RESIDENT" Then Return "PERMRESIDENT"
		If psCitizenshipStatus = "US CITIZEN" Then Return "USCITIZEN"
		Return psCitizenshipStatus
	End Function

	'this is for mapping to control_title attribute in clf 
	Public Shared Function MapRoleNameToValue(ByVal psRoleName As String) As String
		Select Case psRoleName.ToUpper()
			Case "AUTHORIZED_SIGNER", "AUTHORIZED SIGNER"
				Return "DIRECTOR"
			Case "CORPORATE_REPRESENTATIVE", "CORPORATE REPRESENTATIVE"
				Return "CORP_REP"
			Case "VICE PRESIDENT"
				Return "VICE_PRESIDENT"
			Case Else
				Return psRoleName.ToUpper()
		End Select
	End Function

	Public Shared BUSINESSTYPEVALUE As String() = {"GOVERNMENT", "LLC", "LLP", "ASSN", "CORP", "CCORP", "SCORP", "SCORPLLC", "CCORPLLC", "PARTNER", "GEN_PARTNER", "IOLTA", "SP", "TRUST", "UAHROF", "UNINCORPORATED_ASSN", "JOINT_VEN", "NONPROFIT", "LIMPARTNER",
	 "PARTNERLLC", "SPLLC", "ORG_OR_CLUB", "NONPROFITCORP", "NONPROFITORG", "PROFESSIONALCORP", "PUBLICFUNDS", "NONMEMBER", "OTHER"}
	''map all available business type names in the CLF   
	Public Shared Function MapBusinessTypeName(ByVal type As String) As String
		Dim result As String = type
		Select Case type.ToUpper()
			Case "GOVERNMENT"
				result = StrConv("GOVERNMENT", VbStrConv.ProperCase)
			Case "LLC"
				result = StrConv("LIMITED LIABILITY COMPANY", VbStrConv.ProperCase)
			Case "LLP"
				result = StrConv("LIMITED LIABILITY PARTNERSHIP", VbStrConv.ProperCase)
			Case "ASSN"
				result = StrConv("ASSOCIATION", VbStrConv.ProperCase)
			Case "CORP"
				result = StrConv("CORPORATION", VbStrConv.ProperCase)
			Case "CCORP"
				result = StrConv("C CORPORATION", VbStrConv.ProperCase)
			Case "SCORP"
				result = StrConv("S CORPORATION", VbStrConv.ProperCase)
			Case "SCORPLLC"
				result = "S Corporation LLC"
			Case "CCORPLLC"
				result = "C Corporation LLC"
			Case "PARTNER"
				result = StrConv("PARTNERSHIP", VbStrConv.ProperCase)
			Case "GEN_PARTNER"
				result = StrConv("GENERAL PARTNERSHIP", VbStrConv.ProperCase)
			Case "IOLTA"
				result = "IOLTA"
			Case "SP"
				result = StrConv("SOLE PROPRIETORSHIP", VbStrConv.ProperCase)
			Case "TRUST"
				result = StrConv("TRUST", VbStrConv.ProperCase)
			Case "UAHROF"
				result = "UAHROF"
			Case "UNINCORPORATED_ASSN"
				result = StrConv("UNINCORPORATED ASSOCIATION", VbStrConv.ProperCase)
			Case "JOINT_VEN"
				result = StrConv("JOINT VENTURE", VbStrConv.ProperCase)
			Case "NONPROFIT"
				result = "Non-Profit Organization or Corporation"
			Case "LIMPARTNER"
				result = StrConv("LIMITED PARTNERSHIP", VbStrConv.ProperCase)
			Case "PARTNERLLC"
				result = "Partnership LLC"
			Case "SPLLC"
				result = "Sole Proprietor LLC"
			Case "ORG_OR_CLUB"
				result = "Organization/Club"
			Case "NONPROFITCORP"
				result = "Non-Profit Corporation"
			Case "NONPROFITORG"
				result = "Non-Profit Organization"
			Case "PROFESSIONALCORP"
				result = StrConv("PROFESSIONAL CORPORATION", VbStrConv.ProperCase)
			Case "PUBLICFUNDS"
				result = StrConv("PUBLIC FUNDS", VbStrConv.ProperCase)
			Case "NONMEMBER"
				result = "Non-Member"
			Case "OTHER"
				result = "Other Business Type"
		End Select
		Return result
	End Function
	Public Shared Function MapBusinessRoleValueToDisplayName(ByVal psRoleName As String) As String
		'for now, don't have the full list of role name, just convert the original name to more descriptive name by replace underscore by space and capitalize transform
		'for example: AUTHORIZED_SIGNER --> Authorized Signer
		Dim result As String = ""
		Select Case psRoleName.ToUpper()
			Case "CORPORATE_REPRESENTATIVE", "CORPORATE REPRESENTATIVE"
				result = "Corporate Representative"
			Case "MANAGER"
				result = "Manager"
			Case "OWNER"
				result = "Owner"
			Case "PARTNER"
				result = "Partner"
			Case "PRESIDENT"
				result = "President"
			Case "SECRETARY"
				result = "Secretary"
			Case "TREASURER"
				result = "Treasurer"
			Case "VICE_PRESIDENT"
				result = "Vice President"
			Case Else
				result = StrConv(psRoleName.Replace("_"c, " "c), VbStrConv.ProperCase)
		End Select
		Return result
	End Function
	Public Shared Function MapSpecialRoleValueToDisplayName(ByVal psRoleName As String) As String
		'for now, don't have the full list of role name, just convert the original name to more descriptive name by replace underscore by space and capitalize transform
		'for example: AUTHORIZED_SIGNER --> Authorized Signer
		Dim result As String = ""
		Select Case psRoleName.ToUpper()
			Case "CORP"
				result = "CORPORATION"
			Case "ACC_DESIGNATED_REP", "ACC DESIGNATED REP"
				result = "Account Designated Representative"
			Case "ADMIN"
				result = "Administrator"
			Case "ALTERNATE_MAILING"
				result = "Alternate Mailing"
			Case "APPLICANT"
				result = "Applicant"
			Case "AUTHORIZE_SIGNER", "AUTHORIZE SIGNER"
				result = "Authorized Signer"
			Case "BENEFICIARY"
				result = "Beneficiary"
			Case "COAPPLICANT"
				result = "CoApplicant"
			Case "COBORROWER"
				result = "Co - Borrower"
			Case "COMMITTEE"
				result = "Committee"
			Case "CONSERVATOR"
				result = "Conservator"
			Case "COSIGNER"
				result = "Co - Signer"
			Case "COVERDELL_ESA_BENEF"
				result = "Coverdell ESA Beneficiary"
			Case "DEATH_BENEFICIARY"
				result = "Death Beneficiary"
			Case "DIV_PAYEE"
				result = "Div Payee"
			Case "EXECUTOR"
				result = "Executor"
			Case "EXECUTRIX"
				result = "Executrix"
			Case "GOVERNING_MEMBER"
				result = "Governing Member"
			Case "GRANTOR"
				result = "Grantor"
			Case "GUARANTOR"
				result = "Guarantor"
			Case "GUARDIAN"
				result = "Guardian"
			Case "INTERESTED_PARTY"
				result = "Interested Party"
			Case "LOAN_COAPPLICANT"
				result = "Loan Co-Applicant"
			Case "LOAN_COMAKER"
				result = "Loan Co-Maker"
			Case "MAILING_ONLY"
				result = "Mailing Only"
			Case "NEXT_OF_KIN"
				result = "Next of Kin"
			Case "OWNER"
				result = "Owner"
			Case "PARENT"
				result = "Parent"
			Case "PERSONAL_REP"
				result = "Personal Representative"
			Case "POWER_OF_ATTORNEY"
				result = "Power Of Attorney"
			Case "REPRESENTATIVE_PAYEE"
				result = "Representative Payee"
			Case "RESP_INDIVIDUAL"
				result = "Responsible Individual"
			Case "SUCC_BEN"
				result = "Successor Beneficiary"
			Case "SUCC_CUSTODIAN"
				result = "Successor Custodian"
			Case "SUCC_RESP_INDIVID", "SUCC RESP INDIVID"
				result = "Successor Responsible Individual"
			Case "SUCC_TRUSTEE"
				result = "Successor Trustee"
			Case "TRANSFEREE"
				result = "Transferee"
			Case "TRANSFEROR"
				result = "Transferor"
			Case "TRUSTEE"
				result = "Trustee"
			Case "TRUSTOR"
				result = "Trustor"
			Case Else
				result = StrConv(psRoleName.Replace("_"c, " "c), VbStrConv.ProperCase)
		End Select
		Return result
	End Function


	'Public Shared HOME_EQUITY_LOAN_PURPOSES As String() = {"CLOSED END HOME EQUITY LOAN", "HOME EQUITY LINE OF CREDIT"}

	'Public Shared Function MapHELOANPurposeToValue(ByVal psHEPurposeID As String) As String
	'    If psHEPurposeID = "CLOSED END HOME EQUITY LOAN" Then Return "56bb7956b101418589ad24cc3db8d480"
	'    If psHEPurposeID = "HOME EQUITY LINE OF CREDIT" Then Return "418574feafe4416fae4e3985910623e5"
	'    Return psHEPurposeID
	'End Function


	'Public Shared HOME_EQUITY_LOAN_REASONS As String() = {"DEBT CONSOLIDATION", "HOME IMPROVEMENT", "HOME PURCHASE", "OTHER", "REFINANCE HOME LOAN"}
	'Public Shared Function MapHomeEquityRefinancePurposeToValue(ByVal psHomeEquityRefinancePurpose As String) As String
	'    If psHomeEquityRefinancePurpose = "REFINANCE HOME LOAN" Then Return "REFINANCE"
	'    Return psHomeEquityRefinancePurpose
	'End Function

	Public Shared INTEND_OCCUPY_PROPERTY As String() = {"PRIMARY RESIDENCE", "RENTAL/INVESTMENT", "SECOND HOME"}
	Public Shared Function MapPropertyOccupancyTypeToValue(ByVal psPropertyOccupancyType As String) As String
		If psPropertyOccupancyType = "PRIMARY RESIDENCE" Then Return "PRIMARY RESIDENCE"
		If psPropertyOccupancyType = "RENTAL/INVESTMENT" Then Return "INVESTMENT"
		If psPropertyOccupancyType = "SECOND HOME" Then Return "SECOND HOME"
		Return psPropertyOccupancyType
	End Function

#Region "Funding soure"
    Public Shared Function AccountText2AccountValue(ByVal text As String) As String
        Select Case text.ToUpper
            Case "MONEY MARKET"
                Return "MONEY_MARKET"
            Case "SAVINGS"
                Return "SAVINGS"
            Case "CHECKING"
                Return "CHECKING"
        End Select
        Return text
    End Function

    Public Shared Function FundingSourceText2FundingSourceValue(ByVal text As String) As String
        Select Case text
            Case "NOT FUNDING AT THIS TIME"
                Return "NOT_FUNDING"
            Case "CASH"
                Return "CASH"
            Case "MAIL A CHECK"
                Return "MAIL"
            Case "CREDIT CARD"
                Return "CREDITCARD"
            Case "TRANSFER FROM ANOTHER FINANCIAL INSTITUTION"
                Return "BANK"
            Case "INTERNAL TRANSFER"
                Return "INTERNALTRANSFER"
            Case "PAYPAL"
                Return "PAYPAL"
        End Select

        Return "NOT_FUNDING"
    End Function

    <Serializable()>
    Public Enum FundingSourceValue
        NOT_FUNDING
        CASH
        MAIL
        CREDITCARD
        BANK
        INTERNALTRANSFER
        PAYPAL
    End Enum
#End Region

    Public Shared PROOF_OF_INCOME As String() = {"FULL DOC", "NO INCOME NO ASSET", "STATED INCOME"}
	Public Shared Function MapDocType(ByVal psDocType As String) As String
		If psDocType = "FULL DOC" Then Return "FULL_DOC"
		If psDocType = "NO INCOME NO ASSET" Then Return "NINA"
		If psDocType = "STATED INCOME" Then Return "STATED"
		Return psDocType
	End Function

	Public Shared XABookingList As String() = {"ACCESSADVANTAGE", "CMC", "CMCFLEXJSON", "COHESION", "CONNECTIT", "CORELATION", "CUDP", "CUNIFY", "EPL", "ESP", "GALAXY", "GENERAL",
											"HORIZONXCHANGE", "JXCHANGE", "OPENPATH", "PATHWAY", "PHOENIXXM", "PREMIER", "SHAREONE", "SIGNATURE", "SERVICEBUS", "XP"}
    Public Shared LoanBookingList As String() = {"CMC", "CMCFLEXJSON", "COHESION", "CORELATION", "FISERVCPS", "JXCHANGE", "PATHWAY", "SYMITAR", "TMG", "XP"}

	Public Enum InstitutionType
		CU
		BANK
	End Enum

	Public Enum LocCategory
		<Description("Unsecured")>
		UNSECURE
		<Description("Secured")>
		SECURE
		<Description("Secured with Shares")>
		SHARES
		<Description("Secured with CD")>
		CD
	End Enum
	Public Enum LocOverDraftOption
		<Description("Only Overdraft")>
		ONLY_OVERDRAFT
		<Description("Only Non-Overdraft")>
		ONLY_NON_OVERDRAFT
		<Description("Both Overdraft and Non-Overdraft")>
		BOTH_OVERDRAFT_AND_NON_OVERDRAFT
	End Enum

End Class
<Serializable()>
Public Enum QuestionUIType
	TEXT
	DROPDOWN
	CHECK
	RADIO
	HIDDEN
End Enum


Public Class GlobalConfigDictionary
	Public Const GENERAL_COUNTRIES_ALLOWED_ACCESS As String = "general.CountriesAllowedAccess"
	Public Const GENERAL_EXCLUDED_IP As String = "general.ExcludedIp"
	Public Const GENERAL_SUBMISSION As String = "general.Submission"
	Public Const GENERAL_INSTATOUCH As String = "general.InstaTouch"
End Class
Public Enum AssetType
	<Description("Certificate")>
	CERTIFICATE
    <Description("Cash Deposit")>
    CASH_DEPOSIT
    <Description("Share")>
    SHARE_BACK
    <Description("Stock")>
    STOCK
    <Description("Bond")>
    BOND
    <Description("Checking Account")>
    CHECKING_ACCOUNT
    <Description("Savings Account")>
    SAVINGS_ACCOUNT
    <Description("Real Estate")>
    REALESTATE
    <Description("Machinery and Equipment")>
    MACHINERY_AND_EQUIPMENT
    <Description("Furniture and Fixtures")>
    FURNITURE_AND_FIXTURES
    <Description("Automobile")>
    AUTOMOBILE
    <Description("Life Insurance")>
    LIFE_INSURANCE
    <Description("Retirement Fund")>
    RETIREMENT_FUND
    <Description("Owned Business")>
    OWNED_BUSINESS
    <Description("Other Liquid")>
    OTHER
    <Description("Other Non-Liquid")>
    OTHER_NON_LIQUID
    <Description("Bridge Loan Not Deposited")>
    BRIDGE_LOAN_NOT_DEPOSITED
    <Description("Cash on Hand")>
    CASH_ON_HAND
    <Description("Gift")>
    GIFT
    <Description("Money Market")>
    MONEY_MARKET
    <Description("Mutual Funds")>
    MUTUAL_FUNDS
    <Description("Secured Borrower Funds")>
    SECURED_BORROWER_FUNDS
    <Description("Trust Funds")>
    TRUST_FUNDS
    <Description("IRA")>
    IRA
    <Description("Net Sale Proceeds")>
    NET_SALE_PROCEEDS
    <Description("Gift Of Equity")>
    GIFT_OF_EQUITY
    <Description("Aircraft")>
    AIRCRAFT
    <Description("As-Extracted Collateral")>
    AS_EXTRACTED
    <Description("Chattel Paper")>
    CHATTEL
    <Description("Documented Vessel")>
    DOC_VESSEL
    <Description("Farm Equipment")>
    FARM_EQUIP
    <Description("Farm Products")>
    FARM_PROD
    <Description("General Intangibles")>
    GEN_INTANGIBLES
    <Description("Instruments")>
    INSTRUMENTS
    <Description("Inventory")>
    INVENTORY
    <Description("Lease And Rents")>
    LEASE_AND_RENTS
    <Description("Mfg Housing")>
    MFG_HOUSING
    <Description("Non-Titled Personal Property")>
    NON_TITLED_PERSONAL_PROP
    <Description("Partnership Interest")>
    PARTNERSHIP_INTEREST
    <Description("Real Estate Construction")>
    REAL_ESTATE_CONST
    <Description("Standing Timber")>
    STANDING_TIMBER
    <Description("Watercraft")>
    WATERCRAFT
End Enum
<Serializable()>
Public Enum CustomQuestionLocation
	LoanPage
	ApplicantPage
	ReviewPage
End Enum