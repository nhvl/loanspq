Imports System.Xml
Imports System.Xml.Schema

Namespace XMLTools
  
	Public Class CSchemaValidator
		Private _ValidationType As ValidationType = ValidationType.Schema
		Public Sub New()

		End Sub
		Public Sub New(ByVal pValidationType As ValidationType)
			_ValidationType = pValidationType
		End Sub

		Public ReadOnly Property ParseError() As String
			Get
				Dim sb As New System.Text.StringBuilder
				For Each sErr As String In Errors
					sb.AppendLine("ERROR: " & sErr)
				Next
				For Each sWarn As String In Warnings
					sb.AppendLine("WARNING: " & sWarn)
				Next

				Return sb.ToString
			End Get
		End Property


		Public Warnings As New System.Collections.Generic.List(Of String)
		Public Errors As New System.Collections.Generic.List(Of String)



		''' <summary>
		''' This is an explicit routine to validate dtd based files.  It is assumed the DTD's contain the necessary
		''' link to the actual DTD file.
		''' </summary>
		''' <param name="sDataXML"></param>
		''' <returns></returns>
		''' <remarks></remarks>
		Public Function ValidateDTD(ByVal sDataXML As String) As Boolean
            'CAssert.IsTrue(_ValidationType = ValidationType.DTD, "Routine only available for DTD.")
			Errors.Clear()
			Warnings.Clear()

			Dim xmlSettings As New XmlReaderSettings()
			xmlSettings.ValidationType = ValidationType.DTD
			AddHandler xmlSettings.ValidationEventHandler, AddressOf Me.ValidationEventHandler

			Dim dataXMLReader As New XmlTextReader(New System.IO.StringReader(sDataXML))
			Dim objValidator As XmlReader = XmlReader.Create(dataXMLReader, xmlSettings)

			'iterate thru the doc reading and validating each element
			Try
				While objValidator.Read
					'do nothing
				End While
			Catch ex As Exception
				Errors.Add(ex.Message)
				Return False
			Finally
				dataXMLReader.Close()
				objValidator.Close()
			End Try

			Return Errors.Count = 0
		End Function

		Public Function validate(ByVal sDataXML As String, ByVal sSchemaXML As String) As Boolean
            'CAssert.IsTrue(_ValidationType <> ValidationType.DTD, "Routine not available for DTD.")

			Errors.Clear()
			Warnings.Clear()

			Dim xmlSettings As New XmlReaderSettings()
			xmlSettings.ValidationType = _ValidationType
			AddHandler xmlSettings.ValidationEventHandler, AddressOf Me.ValidationEventHandler

			Dim schemaXMLReader As XmlTextReader = New XmlTextReader(New System.IO.StringReader(sSchemaXML))
			xmlSettings.Schemas.Add(Nothing, schemaXMLReader)

			Dim dataXMLReader As New XmlTextReader(New System.IO.StringReader(sDataXML))
			Dim objValidator As XmlReader = XmlReader.Create(dataXMLReader, xmlSettings)

			'iterate thru the doc reading and validating each element
			Try
				While objValidator.Read
					'do nothing
				End While
			Catch ex As Exception
				Errors.Add(ex.Message)
				Return False
			Finally
				dataXMLReader.Close()
				schemaXMLReader.Close()
				objValidator.Close()
			End Try

			Return Errors.Count = 0
		End Function


		''' <summary>
		''' usefull overload to allow reading schema from a stream/file instead.  This is handy if we don't want to deal w/ a schema that links up multiple files 
		''' b/c the .net framework will handle it for us 
		''' ie: if validate ( poDoc.outerxml, new XMLTextReader("c:\temp\myschema.xsd")) = false then ...     
		''' </summary>
		''' <param name="sDataXML">Actual XML data</param>
		''' <param name="schemaXMLReader"></param>
		''' <param name="psNamespaceToValidate">Namespace to validate.  Leave empty/nothing/"" to validate the target namespace of the XSD file</param>
		''' <returns></returns>
		''' <remarks>
		''' Migration logic for .net 2 came from http://blogs.msdn.com/johnls/archive/2005/12/28/507951.aspx
		''' </remarks>
		Public Function validate(ByVal sDataXML As String, ByVal schemaXMLReader As XmlTextReader, Optional ByVal psNamespaceToValidate As String = Nothing) As Boolean
			Dim schemaXMLReaders As New List(Of XmlTextReader)
			schemaXMLReaders.Add(schemaXMLReader)
			Return validate(sDataXML, schemaXMLReaders, psNamespaceToValidate)
		End Function

		''' <summary>
		''' Takes in a list of XMLTextReaders to allow for validation of XML that uses multiple schema files.
		''' </summary>
		''' <param name="sDataXML"></param>
		''' <param name="schemaXMLReaders"></param>
		''' <param name="psNamespaceToValidate"></param>
		''' <returns></returns>
		''' <remarks></remarks>
		Public Function validate(ByVal sDataXML As String, ByVal schemaXMLReaders As List(Of XmlTextReader), Optional ByVal psNamespaceToValidate As String = Nothing) As Boolean
            'CAssert.IsTrue(_ValidationType <> ValidationType.DTD, "Routine not available for DTD.")
			Errors.Clear()
			Warnings.Clear()

			Dim xmlSettings As New XmlReaderSettings()
			xmlSettings.ValidationType = _ValidationType
			AddHandler xmlSettings.ValidationEventHandler, AddressOf Me.ValidationEventHandler

			Try
				For Each schemaXMLReader As XmlTextReader In schemaXMLReaders
					xmlSettings.Schemas.Add(psNamespaceToValidate, schemaXMLReader)
				Next
			Catch ex As System.Exception
				Errors.Add(ex.Message)
				Return False
			End Try

			Dim dataXMLReader As New XmlTextReader(New System.IO.StringReader(sDataXML))
			Dim objValidator As XmlReader = XmlReader.Create(dataXMLReader, xmlSettings)

			'iterate thru the doc reading and validating each element
			Try
				While objValidator.Read
					'do nothing
				End While
			Catch ex As Exception
				Errors.Add(ex.Message)
				Return False
			Finally
				dataXMLReader.Close()
				For Each schemaXMLReader As XmlTextReader In schemaXMLReaders
					schemaXMLReader.Close()
				Next
				objValidator.Close()
			End Try

			Return Errors.Count = 0
		End Function

		''' <summary>
		''' This is a lighter-weight function than the others in this class. It acts mostly as a wrapper around .NET's
		''' own facility for Schema validation.
		''' </summary>
		Public Function validateBySchemaPath(ByVal poXML As XmlDocument, ByVal psSchemaPath As String) As Boolean
			poXML.Schemas.Add(Nothing, psSchemaPath)
			poXML.Validate(AddressOf ValidationEventHandler)

			Return Errors.Count = 0
		End Function


		Public Sub ValidationEventHandler(ByVal sender As Object, ByVal args As ValidationEventArgs)
			If args.Severity = XmlSeverityType.Warning Then
				Warnings.Add("No schema found to enforce validation.")
			ElseIf args.Severity = XmlSeverityType.Error Then
				'Console.WriteLine("Error occurred during validation.")
				If False = IsNothing(args.Exception) Then
					Errors.Add(String.Format("({0}, {1}) {2} - {3}", args.Exception.LineNumber, args.Exception.LinePosition, args.Exception.SourceUri, args.Message))
				Else
					Errors.Add(args.Message)
				End If

			End If

		End Sub	'ValidationEventHandler


	End Class

End Namespace

