﻿Imports System.Configuration.ConfigurationManager
Imports System.Net
Imports System.IO
Imports System.Text
Imports System.Security.Cryptography.X509Certificates
Imports System.Net.Security
Imports System.Xml

''' <summary>
''' utility wrapper class for the http request objects to support:
'''1. page persistance request
'''2. Post request of XML document
'''3. post request of form encoded data
'''4. simple Get requests
''' </summary>
''' <remarks></remarks>
Public Class CWebRequest

	Public Headers As New WebHeaderCollection()
	Public ResponseHeaders As New WebHeaderCollection()
	Public ResponseType As eResponseTypes = eResponseTypes.eText
	''' <summary>
	''' True - the cookie container will be shared between calls to the POST/GET methods.
	''' False - cookie container will be cleared before each POST/GET operation.
	''' </summary>
	Public PersistRequest As Boolean
	Public Cookies As CookieContainer
	Public AllowAutoRedirect As Boolean = True

	' Digital signature properties
	Public UseDigitalSignature As Boolean = False
	Public DigitalCertificateFriendlyName As String = "LPQ_CERTIFICATE"
	Public DigitalSignatureHeaderKey As String = "LPQ_DIGITAL_SIGNATURE"

	''' <summary>
	''' Number of milliseconds to wait before timeout.
	''' </summary>
	''' <remarks>100 Second default same as HttpWebRequest default.</remarks>
	Public TimeOut As Integer = 100000

	''' <summary>
	''' Used for basic authentication only.  
	''' </summary>    
	Public Login As String
	''' <summary>
	''' Used for basic authentication only.  
	''' </summary>
	Public Password As String

	''' <summary>
	''' create case insensitive dictionary of params to send
	''' </summary>    
	''' <remarks>Warning: the way .NET implements case-insensitivity on the key is by lowercasing. So if you add "KEY"/"VALUE" to 
	''' this collection, the key will actually be stored as "key". If you need this to be case sensitive, swap it out with our custom
	''' CCaseSensitiveStringDictionary class.</remarks>
	Public Params As New System.Collections.Specialized.StringDictionary()

	''' <summary>
	''' Number of times we should try to send request to remote server should this fail.
	''' </summary>
	''' <remarks>
	''' Be VERY carefull about sending requests multiple times.  If there is an error coming back, the request
	''' may still have been processed on the remote server.  Sending multiple times may create
	''' unwanted side affects.
	''' </remarks>
	Public NumRetries As Integer = 0

	''' <summary>
	''' Number of milliseconds to wait between retries
	''' </summary>
	''' <remarks></remarks>
	Public WaitMsBetweenRetries As Integer = 0

	Public UseGzipCompression As Boolean = True

	Private _ContentLength As Long
	''' <summary>
	''' The length in bytes, excluding headers, of the response
	''' </summary>
	Public ReadOnly Property ContentLength() As Long
		Get
			Return _ContentLength
		End Get
	End Property

	Public Enum eResponseTypes
		eText
		eStream
	End Enum

#Region "ResponseXXX Properties  Only 1 response type can be utilized per web request"

	Private _ResponseText As String
	''' <summary>
	''' Exposes the response after a POST/GET operation when request object is in Text mode
	''' </summary>
	Public ReadOnly Property ResponseText() As String
		Get
			If ResponseType <> eResponseTypes.eText Then
				Throw New ApplicationException("Response type is invalid for text operation:" & Me.ResponseType.ToString)
			End If
			Return _ResponseText
		End Get
	End Property

	Private _ResponseStream As System.IO.Stream
	''' <summary>
	''' Exposes the response after a POST/GET operation when request object is in Stream mode
	''' </summary>
	''' <remarks> 
	''' To read data out to a byte array, do something like this:
	''' Dim binReader As New System.IO.BinaryReader(oWebPost.ResponseStream)
	''' Dim bData() As Byte = binReader.ReadBytes(CInt(oWebPost.ContentLength))
	''' </remarks>
	Public ReadOnly Property ResponseStream() As System.IO.Stream
		Get
			If ResponseType <> eResponseTypes.eStream Then
				Throw New ApplicationException("Response type is invalid for stream operation:" & Me.ResponseType.ToString)
			End If
			Return _ResponseStream
		End Get
	End Property
#End Region

	''' <summary>
	''' Creates a request object with specified configuration.
	''' </summary>
	''' <param name="pbPersistRequest">
	''' True - the cookie container will be shared between calls to the POST/GET methods.
	''' False - cookie container will be cleared before each POST/GET operation.
	''' </param>
	''' <param name="pbResponseType">
	''' Indicates which method (mutually exclusive) will be used to expose the response:
	''' eText - the response will be read out into a string and exposed by the ResponseText property
	''' eStream - a System.IO.Stream will be exposed by the ResponseStream, allowing external code to read out the response
	''' </param>
	Public Sub New(ByVal pbPersistRequest As Boolean, ByVal pbResponseType As eResponseTypes)
		PersistRequest = pbPersistRequest
		If pbPersistRequest Then
			Cookies = New CookieContainer()
		End If
		ResponseType = pbResponseType

		_ResponseText = ""
	End Sub

	''' <summary>
	''' Creates a request object with PersistRequest = False, ResponseType = Text.
	''' </summary>
	Public Sub New()
		Me.New(False, eResponseTypes.eText)
	End Sub

	''' <summary>
	''' Helper function to add LoansPQ digital signature to the headers of an HttpWebRequest
	''' </summary>
	Public Sub AttachLpqDigitalSignature(ByVal pRequest As HttpWebRequest, Optional ByVal pRequestBytes() As Byte = Nothing)
		'Dim signatureService As New CXmlDigitalSignatureService()
		'Dim cert As X509Certificate2 = _
		'    signatureService.GetCertificateByFriendlyName(DigitalCertificateFriendlyName, StoreName.My, StoreLocation.LocalMachine)

		'Dim requestString As String = ""

		'If pRequestBytes IsNot Nothing Then
		'    requestString = System.Text.Encoding.UTF8.GetString(pRequestBytes)
		'End If

		'Dim signature As String = signatureService.GenerateDigitalSignature(requestString, cert)

		'pRequest.Headers.Add(DigitalSignatureHeaderKey, signature)
	End Sub

	Public Sub PostSOAP(ByVal psURL As String, ByVal soapAction As String, ByVal poXMLDoc As XmlDocument)
		_PostSOAP(psURL, soapAction, poXMLDoc, Nothing)
	End Sub

	Public Sub PostSOAP(ByVal psURL As String, ByVal soapAction As String, ByVal poXMLDoc As XmlDocument, ByVal poCertificate As X509Certificate)
		If poCertificate Is Nothing Then
			Throw New ArgumentNullException("poCertificate")
		End If
		_PostSOAP(psURL, soapAction, poXMLDoc, poCertificate)
	End Sub

	Private Sub _PostSOAP(ByVal psURL As String, ByVal soapAction As String, ByVal poXMLDoc As XmlDocument, ByVal poCertificate As X509Certificate)
		For i As Integer = 0 To NumRetries
			Try
				cleanup()

				'Dim req As System.Net.WebRequest = System.Net.WebRequest.Create(psURL)
				Dim objRequest As HttpWebRequest = CType(WebRequest.Create(psURL), HttpWebRequest)
				objRequest.Method = "POST"
				objRequest.Headers.Add("SOAPAction", """" & soapAction & """")
				objRequest.Headers.Add(Headers)
				objRequest.UserAgent = "Mozilla/4.0 (compatible; MSIE 6.0; MS Web Services Client Protocol 1.1.4322.2032)"
				objRequest.KeepAlive = True
				objRequest.ContentType = "text/xml"
				objRequest.AllowAutoRedirect = AllowAutoRedirect
				If poCertificate IsNot Nothing Then
					objRequest.ClientCertificates.Add(poCertificate)
				End If
				objRequest.Timeout = TimeOut

				addBasicAuthentication(objRequest)

				'send the data out
				objRequest.CookieContainer = Cookies
				Dim bytes As Byte() = System.Text.Encoding.UTF8.GetBytes(poXMLDoc.OuterXml)

				' Attach LoansPQ digital signature if needed
				If UseDigitalSignature Then
					AttachLpqDigitalSignature(objRequest, bytes)
				End If

				objRequest.ContentLength = bytes.Length
				Dim stm As System.IO.Stream = objRequest.GetRequestStream()
				Try
					stm.Write(bytes, 0, CInt(bytes.Length))
				Finally
					stm.Close()
				End Try

				processResult(objRequest)

				'successfully got here so exit the retry loop
				Exit For
			Catch err As Exception
				'no more retries left, so pass the error on
				If i >= NumRetries Then
					Throw
				ElseIf WaitMsBetweenRetries > 0 Then
					System.Threading.Thread.Sleep(WaitMsBetweenRetries)
				End If
			End Try
		Next
	End Sub

	''' <summary>
	''' submits Params collection to the psURL simliar to form post
	''' </summary>
	''' <param name="psURL"></param>
	''' <remarks></remarks>
	Public Sub PostData(ByVal psURL As String, Optional ByVal poClientCertificate As X509Certificate = Nothing)
		For i As Integer = 0 To NumRetries
			Try
				cleanup()

				Dim strPost As String = ""
				If Params.Count > 0 Then
					Dim oStrBuilder As New System.Text.StringBuilder()
					Dim sKey As String
					For Each sKey In Params.Keys
						oStrBuilder.Append(sKey & "=" & System.Web.HttpUtility.UrlEncode(Params(sKey)) & "&")
					Next
					strPost = oStrBuilder.ToString
					If strPost.EndsWith("&") Then
						strPost = strPost.Substring(0, strPost.Length - 1)
					End If
				End If

				Dim myWriter As StreamWriter = Nothing

				Dim objRequest As HttpWebRequest = CType(WebRequest.Create(psURL), HttpWebRequest)
				objRequest.Method = "POST"
				objRequest.KeepAlive = False
				objRequest.ContentLength = strPost.Length
				objRequest.ContentType = "application/x-www-form-urlencoded"
				objRequest.CookieContainer = Cookies
				If UseGzipCompression Then
					objRequest.Headers.Add("Accept-Encoding", "gzip")
					objRequest.AutomaticDecompression = DecompressionMethods.GZip
				End If
				objRequest.Headers.Add(Headers)
				objRequest.AllowAutoRedirect = AllowAutoRedirect
				objRequest.Timeout = TimeOut
				addBasicAuthentication(objRequest)

				If poClientCertificate IsNot Nothing Then
					objRequest.ClientCertificates.Add(poClientCertificate)
				End If

				' Attach LoansPQ digital signature if needed
				If UseDigitalSignature Then
					AttachLpqDigitalSignature(objRequest)
				End If

				Try
					myWriter = New StreamWriter(objRequest.GetRequestStream())
					myWriter.Write(strPost)
				Finally
					If Not IsNothing(myWriter) Then
						myWriter.Close()
					End If
				End Try

				'Dim objResponse As HttpWebResponse = CType(objRequest.GetResponse(), HttpWebResponse)
				processResult(objRequest)

				'successfully got here so exit the retry loop
				Exit For
			Catch err As Exception
				'no more retries left, so pass the error on
				If i >= NumRetries Then
					Throw
				ElseIf WaitMsBetweenRetries > 0 Then
					System.Threading.Thread.Sleep(WaitMsBetweenRetries)
				End If
			End Try
		Next
	End Sub

	''' <summary>
	''' post byte array to the psURL param. Params object is ignored
	''' </summary>
	''' <param name="psURL"></param>
	''' <param name="poBytes"></param>
	''' <remarks></remarks>
	Public Sub PostData(ByVal psURL As String, _
	  ByVal poBytes() As Byte, _
	  Optional ByVal poClientCertificate As X509Certificate = Nothing, _
	  Optional ByVal psContentType As String = "")
		For i As Integer = 0 To NumRetries
			Try
				cleanup()

				'Dim req As System.Net.WebRequest = System.Net.WebRequest.Create(psURL)
				Dim objRequest As HttpWebRequest = CType(WebRequest.Create(psURL), HttpWebRequest)
				objRequest.Method = "POST"
				objRequest.KeepAlive = False
				If UseGzipCompression Then
					objRequest.Headers.Add("Accept-Encoding", "gzip")
					objRequest.AutomaticDecompression = DecompressionMethods.GZip
				End If
				objRequest.Headers.Add(Headers)
				objRequest.AllowAutoRedirect = AllowAutoRedirect
				objRequest.Timeout = TimeOut
				If psContentType <> "" Then
					objRequest.ContentType = psContentType
				End If
				addBasicAuthentication(objRequest)

				If poClientCertificate IsNot Nothing Then
					objRequest.ClientCertificates.Add(poClientCertificate)
				End If

				' Attach LoansPQ digital signature if needed
				If UseDigitalSignature Then
					AttachLpqDigitalSignature(objRequest, poBytes)
				End If

				'send the data out
				objRequest.CookieContainer = Cookies
				'Dim bytes As Byte() = System.Text.Encoding.UTF8.GetBytes(poXMLDoc.OuterXml)
				objRequest.ContentLength = poBytes.Length
				Dim stm As System.IO.Stream = objRequest.GetRequestStream()
				Try
					stm.Write(poBytes, 0, CInt(poBytes.Length))
				Finally
					stm.Close()
				End Try

				'read the data back in
				processResult(objRequest)

				'successfully got here so exit the retry loop
				Exit For
			Catch err As Exception
				'no more retries left, so pass the error on
				If i >= NumRetries Then
					Throw
				ElseIf WaitMsBetweenRetries > 0 Then
					System.Threading.Thread.Sleep(WaitMsBetweenRetries)
				End If
			End Try
		Next
	End Sub

	''' <summary>
	''' post poXMLDoc to the psURL param. Params object is ignored
	''' </summary>
	''' <param name="psURL"></param>
	''' <param name="poXMLDoc"></param>
	''' <remarks>This is a simple wrapper around the Byte() overload of PostData(</remarks>
	Public Sub PostData(ByVal psURL As String, _
		 ByVal poXMLDoc As System.Xml.XmlDocument, _
		 Optional ByVal poClientCertificate As X509Certificate = Nothing, _
		 Optional ByVal psContentType As String = "")

		Dim bytes As Byte() = System.Text.Encoding.UTF8.GetBytes(poXMLDoc.OuterXml)
		PostData(psURL, bytes, poClientCertificate, psContentType)
	End Sub

	''' <summary>
	''' submits a GET request to psURL and appends Params to the psURL
	''' </summary>
	''' <param name="psURL"></param>
	''' <remarks></remarks>
	Public Sub GetData(ByVal psURL As String, Optional ByVal pCertificate As X509Certificate = Nothing)
		For i As Integer = 0 To NumRetries
			Try
				cleanup()

				If Params.Count > 0 Then
					'build the strParams
					Dim oStrBuilder As New System.Text.StringBuilder()
					Dim sKey As String
					For Each sKey In Params.Keys
						oStrBuilder.Append(sKey & "=" & System.Web.HttpUtility.UrlEncode(Params(sKey)) & "&")
					Next
					Dim strParams As String = oStrBuilder.ToString
					If strParams.EndsWith("&") Then
						strParams = strParams.Substring(0, strParams.Length - 1)
					End If

					'append params to the URL
					If psURL.ToLower.EndsWith(".aspx") OrElse psURL.ToLower.EndsWith("asp") Then
						psURL += "?" & strParams
					Else
						psURL += "&" & strParams
					End If
				End If

				Dim req As HttpWebRequest
				req = CType(WebRequest.Create(psURL), HttpWebRequest)
				req.KeepAlive = False
				req.CookieContainer = Cookies
				req.Headers.Add(Headers)
				req.Timeout = TimeOut
				addBasicAuthentication(req)

				If pCertificate IsNot Nothing Then
					req.ClientCertificates.Add(pCertificate)
				End If

				' Attach LoansPQ digital signature if needed
				If UseDigitalSignature Then
					AttachLpqDigitalSignature(req)
				End If

				'result = req.GetResponse()
				processResult(req)

				'successfully got here so exit the retry loop
				Exit For
			Catch err As Exception
				'no more retries left, so pass the error on
				If i >= NumRetries Then
					Throw
				ElseIf WaitMsBetweenRetries > 0 Then
					System.Threading.Thread.Sleep(WaitMsBetweenRetries)
				End If
			End Try
		Next
	End Sub

	''' <summary>
	''' handy util function
	''' </summary>
	''' <param name="sURL"></param>
	''' <returns></returns>
	''' <remarks></remarks>
	Public Shared Function GetDataFrom(ByVal sURL As String, Optional ByVal pnNumRetries As Integer = 0) As String
		Dim oWebRequest As New CWebRequest(False, eResponseTypes.eText)
		oWebRequest.UseDigitalSignature = True
		oWebRequest.NumRetries = pnNumRetries
		oWebRequest.GetData(sURL)
		Return oWebRequest.ResponseText
	End Function

#Region "HELPER FUNCTIONS AND CLASSES"
	''' <summary>
	''' determines how to store the result from the web request.  sometimes we may want to use it as a stream for efficency reasons
	''' </summary>
	''' <param name="poRequest"></param>
	''' <remarks></remarks>
	Private Sub processResult(ByVal poRequest As HttpWebRequest)
		Dim objResponse As HttpWebResponse = CType(poRequest.GetResponse(), HttpWebResponse)
		Dim stmREsponse As System.IO.Stream = objResponse.GetResponseStream
		_ContentLength = objResponse.ContentLength
		ResponseHeaders = objResponse.Headers

		Select Case ResponseType
			Case eResponseTypes.eText
				Dim encode As Encoding
				encode = System.Text.Encoding.GetEncoding("utf-8")
				Dim sr As StreamReader
				sr = New StreamReader(stmREsponse, encode)
				Try
					_ResponseText = sr.ReadToEnd
				Finally
					stmREsponse.Close()
					sr.Close()
				End Try
			Case eResponseTypes.eStream
				_ResponseStream = stmREsponse
		End Select
	End Sub

	''' <summary>
	''' Refer to http://support.microsoft.com/?id=303436 for CSharpHTTP sample code that includes BASIC and NTLM authentication .  However, we don't use the credential object b/c it seemed too buggy :-/
	''' </summary>
	''' <param name="poReq"></param>
	''' <remarks></remarks>
	Private Sub addBasicAuthentication(ByVal poReq As WebRequest)
		If Login = "" Then
			Return
		End If
		'adds header containing basic authentication and proper encoding
		Dim b() As Byte = System.Text.Encoding.UTF8.GetBytes(Login & ":" & Password)
		Dim authStr As String = System.Convert.ToBase64String(b)
		poReq.Headers.Add("Authorization: Basic " & authStr)
	End Sub

	Private Sub cleanup()
		If Not _ResponseStream Is Nothing Then
			_ResponseStream.Close()
		End If

		If PersistRequest = False Then
			Cookies = Nothing
		End If

		_ResponseText = ""

	End Sub

	''' <summary>
	''' Generally you don't want to ignore certificate validation.  What happens is behind the scene, SSL will contact a third party like Verisign to make sure it's valid.
	''' </summary>
	''' <remarks></remarks>
	Public Shared Sub setIgnoreCertificatePolicy()
		System.Net.ServicePointManager.ServerCertificateValidationCallback = New System.Net.Security.RemoteCertificateValidationCallback(AddressOf CCertificateIgnorePolicy.ValidateServerCertificate)
	End Sub


	''' <summary>
	''' This class is used to bypass SSL certificates for testing purposes.
	''' .net 1.1 usage: System.Net.ServicePointManager.CertificatePolicy = New LoansPQ.WebServices.CCertificateIgnorePolicy()
	''' For .net 2.0, it uses the shared function ValidateServerCertificate.
	''' </summary>
	''' <remarks></remarks>
	Private Class CCertificateIgnorePolicy
		Implements ICertificatePolicy


		Public Function CheckValidationResult(ByVal srvPoint As ServicePoint, _
			 ByVal cert As X509Certificate, ByVal request As WebRequest, _
			 ByVal certificateProblem As Integer) _
			As Boolean Implements ICertificatePolicy.CheckValidationResult
			'Return True to force the certificate to be accepted.
			Return True
		End Function

		''' <summary>
		''' This routine is a dummy callback used for .net 2 to bypass ssl 
		''' </summary>
		''' <param name="sender"></param>
		''' <param name="certificate"></param>
		''' <param name="chain"></param>
		''' <param name="sslPolicyErrors"></param>
		''' <returns></returns>
		''' <remarks>
		''' Sample usage: 
		''' ServicePointManager.ServerCertificateValidationCallback = New System.Net.Security.RemoteCertificateValidationCallback(AddressOf LoansPQ.WebServices.CCertificateIgnorePolicy.ValidateServerCertificate)
		''' </remarks>
		Public Shared Function ValidateServerCertificate(ByVal sender As Object, ByVal certificate As X509Certificate, ByVal chain As X509Chain, ByVal sslPolicyErrors As SslPolicyErrors) As Boolean
			Return True
		End Function
	End Class
#End Region

End Class