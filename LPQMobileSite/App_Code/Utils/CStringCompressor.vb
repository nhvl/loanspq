﻿Imports System.IO.Compression
Imports System.IO
Imports System.Text

Public Class CStringCompressor

	Private Const marker As String = "**COMPRESSED**"

	Public Shared Function Compress(text As String) As String
		If text.StartsWith(marker) Then Return text ' to prevent compressing many times
		Dim buffer As Byte() = Encoding.UTF8.GetBytes(text)
		Dim ms As New MemoryStream()
		Using gZipStream = New GZipStream(ms, CompressionMode.Compress, True)
			gZipStream.Write(buffer, 0, buffer.Length)
		End Using
		ms.Position = 0
		Dim compressedData(ms.Length - 1) As Byte
		ms.Read(compressedData, 0, compressedData.Length)
		Dim gZipBuffer(compressedData.Length + 3) As Byte
		System.Buffer.BlockCopy(compressedData, 0, gZipBuffer, 4, compressedData.Length)
		System.Buffer.BlockCopy(BitConverter.GetBytes(buffer.Length), 0, gZipBuffer, 0, 4)
		Return marker & Convert.ToBase64String(gZipBuffer)
	End Function

	Public Shared Function Decompress(compressedText As String) As String
		If Not compressedText.StartsWith(marker) Then Return compressedText ' to prevent decompressing a uncompressed string
		compressedText = compressedText.Substring(marker.Length)
		Dim gZipBuffer = Convert.FromBase64String(compressedText)
		Using ms As New MemoryStream
			Dim dataLength = BitConverter.ToInt32(gZipBuffer, 0)
			ms.Write(gZipBuffer, 4, gZipBuffer.Length - 4)
			Dim buffer(dataLength - 1) As Byte
			ms.Position = 0
			Using gZipStream As New GZipStream(ms, CompressionMode.Decompress)
				gZipStream.Read(buffer, 0, buffer.Length)
			End Using
			Return Encoding.UTF8.GetString(buffer)
		End Using
	End Function
End Class
