﻿Imports Microsoft.VisualBasic
Imports LPQMobile.Utils

Public Class CBaseUserControl
    Inherits System.Web.UI.UserControl

	Protected log As log4net.ILog = log4net.LogManager.GetLogger(Me.GetType)

    Dim _contentDataTheme As String
    Dim _headerDataTheme As String
    Dim _headerDataTheme2 As String
	Dim _footerDataTheme As String
	Dim _footerFontDataTheme As String
	Dim _backgroundDataTheme As String
	Dim _buttonDataTheme As String
	Dim _loanType As String
    Private _textAndroidTel As String
	Public Property isComboMode As Boolean = False
	Public Property MappedShowFieldLoanType As String = Nothing
    Public ReadOnly Property TextAndroidTel As String
        Get
            If String.IsNullOrEmpty(_textAndroidTel) Then
                Dim useragent As String = If(Request.ServerVariables("HTTP_USER_AGENT"), "")
				_textAndroidTel = If(useragent.ToLower.Contains("android"), "tel", "text")
            End If
            Return _textAndroidTel
        End Get
    End Property
	

	Private _idPrefix As String = ""
	Public Property IDPrefix As String
		Get
			Return _idPrefix
		End Get
		Set(value As String)
			_idPrefix = value
		End Set
	End Property

    Public Property AutoFillTestData As Boolean = False
    Public Property HasPropertyAddress As Boolean
    Protected ReadOnly Property _CopyPrefix As String
        Get
            Return If(HasPropertyAddress, "Copy", "")
        End Get
    End Property

	Public Property LoanType() As String '"CC,VL,PL,HE, HELOC, XA
		Get
			Return _loanType
		End Get
		Set(value As String)
			_loanType = value
		End Set
	End Property

    Public ReadOnly Property JS_TYPE() As String
        Get
            If System.Configuration.ConfigurationManager.AppSettings.Get("ENVIRONMENT") = "LIVE" Then
				Return ".min"
            Else
                Return ""
            End If

        End Get
    End Property

    Property ContentDataTheme() As String
        Get
            Return _contentDataTheme
        End Get
        Set(value As String)
            _contentDataTheme = value
        End Set
    End Property
    Property HeaderDataTheme() As String
        Get
            Return _headerDataTheme
        End Get
        Set(value As String)
            _headerDataTheme = value
        End Set
    End Property

    Property HeaderDataTheme2() As String
        Get
            Return _headerDataTheme2
        End Get
        Set(value As String)
            _headerDataTheme2 = value
        End Set
    End Property

    Property FooterDataTheme() As String
        Get
            Return _footerDataTheme
        End Get
        Set(value As String)
            _footerDataTheme = value
        End Set
	End Property
	Property FooterFontDataTheme() As String
		Get
			Return _footerFontDataTheme
		End Get
		Set(value As String)
			_footerFontDataTheme = value
		End Set
	End Property

	Property BackgroundDataTheme() As String
		Get
			Return _backgroundDataTheme
		End Get
		Set(value As String)
			_backgroundDataTheme = value
		End Set
	End Property

	Property ButtonDataTheme() As String
		Get
			Return _buttonDataTheme
		End Get
		Set(value As String)
			_buttonDataTheme = value
		End Set
	End Property

    Public Shadows Property Page As CBasePage
        Get
            Return CType(MyBase.Page, CBasePage)
        End Get
        Set(value As CBasePage)
            MyBase.Page = value
        End Set
    End Property

	Public Function IsInMode(mode As String) As Boolean
		If Request.QueryString("mode") = mode Then Return True
		Return False
	End Function

	Public Function IsInFeature(feature As String) As Boolean
		If Request.QueryString("feature") = feature Then Return True
		Return False
	End Function

    Public Function BuildShowFieldSectionID(elementID As String) As String
        Dim sPath As String = Common.SafeString(Request.Url.AbsolutePath).ToUpper
        If sPath.Contains("/ACCU/") Then '' not support show and hide field in APM so stop process
            Return ""
        End If
        If String.IsNullOrEmpty(elementID) Or MappedShowFieldLoanType Is Nothing Then
            'to prevent logic error when empty elementID passing to this function or no mappedShowFieldLoanType specified then cause error at runtime, not build time
            log.Error("[BuildShowFieldSectionID] Invalid parameter value")
            Throw New Exception("Invalid parameter value")
        End If
        Dim result As String
        If MappedShowFieldLoanType = "XA_BUSINESS" OrElse MappedShowFieldLoanType = "SA_BUSINESS" OrElse MappedShowFieldLoanType = "XA_SPECIAL" OrElse MappedShowFieldLoanType = "SA_SPECIAL" OrElse MappedShowFieldLoanType = "BL" Then
            result = IIf(String.IsNullOrEmpty(IDPrefix), "", "ns_").ToString() & elementID  ' ns = no scope
        Else
            result = IDPrefix & elementID
        End If
        Return result
    End Function

    Public ReadOnly Property PreviewCode() As String
        Get
            Return Common.SafeString(Request.Params("previewcode"))
        End Get
    End Property

    Public Function GetApmVisibilitySectionNamePrefix() As String
        If IDPrefix = "co_" Then Return "Co-App"
        If IDPrefix = "m_" Then Return "Minor"
        If MappedShowFieldLoanType = "XA_SPECIAL" OrElse MappedShowFieldLoanType = "SA_SPECIAL" OrElse MappedShowFieldLoanType = "XA_BUSINESS" OrElse MappedShowFieldLoanType = "SA_BUSINESS" OrElse MappedShowFieldLoanType = "BL" Then
            Return ""
        Else
            Return IDPrefix
		End If
	End Function

	Protected Function GetSuffixList() As Dictionary(Of String, String)
		Dim result As New Dictionary(Of String, String)
		'TODO: maybe, we need to put Suffix list to CustomListTemplates.xml for centralized management as the way we are dealing with Business Industry Codes. For now, temporary hard code in this function
		result.Add("", "None")
        result.Add("JR", "JR")
        result.Add("SR", "SR")
		result.Add("II", "II")
		result.Add("III", "III")
		result.Add("IV", "IV")
		result.Add("V", "V")
		result.Add("VI", "VI")
		result.Add("VII", "VII")
		result.Add("VIII", "VIII")
		Return result
	End Function
End Class

