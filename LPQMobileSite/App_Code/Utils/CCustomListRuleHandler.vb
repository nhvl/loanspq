﻿Imports log4net
Namespace CustomList
	Public Class CCustomListRuleHandler(Of T As CCustomListRuleBaseItem)
		Private _customListRuleList As List(Of CCustomListRuleItem)
		Private log As ILog = LogManager.GetLogger(GetType(CCustomListRuleHandler(Of T)))
		Public ReadOnly Property CustomListRuleList As List(Of CCustomListRuleItem)
			Get
				Return _customListRuleList
			End Get
		End Property

		Public Sub New(customListRuleList As List(Of CCustomListRuleItem))
			_customListRuleList = customListRuleList
		End Sub
		Public Function Evaluate(code As String, Optional params As Dictionary(Of String, Tuple(Of Object, String)) = Nothing) As List(Of T)
			Dim result As List(Of T)
			If _customListRuleList IsNot Nothing AndAlso _customListRuleList.Count > 0 AndAlso _customListRuleList.Any(Function(p) p.Code = code) Then
				Dim lst As New List(Of T)
				For Each ru In _customListRuleList.Where(Function(p) p.Code = code)
					lst.AddRange(ru.GetList(Of T)())
				Next
				If lst.Any(Function(p) p.Enable = True) Then
					result = (From item In lst.Where(Function(p) p.Enable = True) Where EvaluateCustomListItem(item, params)).ToList()
				End If
			End If
			Return result
		End Function
		Private Function EvaluateCustomListItem(item As CCustomListRuleBaseItem, params As Dictionary(Of String, Tuple(Of Object, String))) As Boolean
			If item.Expression Is Nothing OrElse String.IsNullOrWhiteSpace(item.Expression.vb) Then Return True
			Dim expression2 As String = CompileExpression2(item)
			Dim eval As New CEvaluator(expression2, GetType(Boolean))
			If item.ParamInfo IsNot Nothing AndAlso item.ParamInfo.Count > 0 AndAlso params IsNot Nothing AndAlso params.Count > 0 Then
				For Each p In params.Where(Function(pi) item.ParamInfo.Any(Function(c) c.Key = pi.Key))
					eval.AddVariable(p.Key, p.Value.Item1, p.Value.Item2)
					'log.Debug(p.Key)
					'log.Debug(p.Value.Item1)
					'log.Debug(p.Value.Item2)
					'log.Debug("------------")
				Next
			End If

			Dim result = eval.Eval()
			log.Debug(result.GeneratedCode)
			If result.ThrewException Then
				log.Error(result.Exception)
				Return False
			Else
				Return Convert.ToBoolean(result.Result)
			End If
		End Function
		Private Function CompileExpression2(item As CCustomListRuleBaseItem) As String
			Dim ret As String = ""
			Dim expression2 As String = item.Expression2.vb
			If Not String.IsNullOrWhiteSpace(expression2) Then
				'Dim x = "CQ_Deed ='gfgd\'ghfh\""hgf'"
				'Console.WriteLine(x)
				'Dim s = System.Text.RegularExpressions.Regex.Replace(x, "([^\\])(['])", "$1""")
				'Console.WriteLine(s)
				's = System.Text.RegularExpressions.Regex.Replace(s, "(\\')", "'")
				'Console.WriteLine(s)
				's = System.Text.RegularExpressions.Regex.Replace(s, "(\\"")", """""")
				'Console.WriteLine(s)

				ret = Regex.Replace(expression2, "([^\\])(['])", "$1""")
				ret = Regex.Replace(ret, "(\\')", "'")
				ret = Regex.Replace(ret, "(\\"")", """""")
			End If
			Return ret
		End Function
	End Class
End Namespace

