﻿Imports Microsoft.VisualBasic
Imports System.Xml
Imports LPQMobile.BO
Imports System
Imports System.Configuration
Imports System.Text.RegularExpressions
Namespace LPQMobile.Utils
    <Serializable()>
    Public Class CWebsiteConfig
        Private ReadOnly _log As log4net.ILog = log4net.LogManager.GetLogger(Me.GetType)
        Public OrganizationId As String ' this is unique within LPQ platform on the same host
        Public LenderId As String ' this is unique within LPQ platform on the same host
        Public LenderRef As String 'this is unique within mobile platform, but changeable by admin
        Public LenderName As String
        Public OrgCode As String  'this is required for most REST API
        Public LenderCode As String
        Public InstitutionType As CEnum.InstitutionType 'default is CU, BANK
        Public LogoUrl As String
        Public Favico As String
        Public OtherLogoUrl As String 'deprecated
        Public FinishedUrl As String  ' if empty then it
        Public FinishedXAUrl As String
        Public APIUser As String
        Public APIPassword As String
        Public APIUserLender As String   'credential tie to lender, this is required for certain API such as XSell.
        Public APIPasswordLender As String
        Public Host As String
        Public SubmitLoanUrl As String
        Public BaseSubmitLoanUrl As String

        'These are for ACTion LOS or Third party vendor integration
        Public SubmitLoanUrl_TPV As String '
        Public BaseSubmitLoanUrl_TPV As String
        Public OrgCode2 As String  'this is required for most REST API
        Public LenderCode2 As String

        'Public AccountsXML As String  'not use anywhere
        Public AuthenticationType As String
        Public DebitType As String
        Public CreditPull As String
        Public sDefaultLoanBranchID As String
        Public sDefaultLoanBranchName As String
        Public sDefaultXABranchID As String
        Public sDefaultXABranchName As String
        Public LoanStatusEnum As String
        Public Booking As String  'xa symitar, etc..
        'Public VLBooking As String	'
        'Public CCBooking As String	'
        'Public PLBooking As String	'
        'Public HEBooking As String	'
        'Public HELOCBooking As String	'
        Public OnlineUserRegistration As String  'Home bankin, Q2, DI, JAWAALA, etc..
        'Public IsFundingEnable As Boolean  'symitar, etc..
        Public IsDecisionXAEnable As Boolean    'Y= enable underwrite of product
        Public LenderCodeBadMember As String 'use lendercode for badmember url, deprecated
        Public CurrentBranchID As String
        Public RequestedIP As String
        Public RequestedHost As String
        Public RequestedUserAgent As String
        Public AddressKey As String
        Public CCMaxFunding As String
        Public ACHMaxFunding As String
        Public PaypalMaxFunding As String
        Public MembershipFee As String
        Public IsACHFundingDirect As Boolean = False
        Public ACHFundingThresholdRequireMicroDeposit As String
        Public ACHAutoFundingDisabled As Boolean = False
        Public IsStandAloneFundingEnabled As Boolean = False
        Public IsAutoFundingWBookingEnabled As Boolean = True   ' this is used with booking
        Public AppType As String
        'not implement yet
        Public DocGroup As String
        Public DocCode As String
        Public ScanDocumentKey As String
        Public DLBarcodeScan As String
        Public PiWikSiteID As String
        Public GoogleTagManagerID As String
        Public FacebookPixelID As String
        Public EnabledAddressVerification As Boolean
        Public PayPalEmail As String
        Public DLScanImage As String
        Public PreviousAddressThresdhold As String
        Public PreviousEmploymentThresdhold As String
        Public _webConfigXML As XmlElement
        ''minor account      
        ''Public minorAuthenticationType As String = ""
        ''Public minorDebitType As String = ""
        ''Public minorCreditPull As String = ""
        ''Public minorDecisionXA As String = ""

        ''reasonable demonstration code
        Public demoPDFViewURL As String = ""
        Public demoPDFViewCode As String = ""
        Public authKey As String = "" ' this is used to restrict access to the system, only allow from reffer with the correct haskey(hasc(static_key+date+time)
        Public coApplicantType As String = "C" 'S, G
        Public LocationPool As Boolean = False
        Public MfaEmailOnly As Boolean = False
        Public HomepageEditor As Boolean = False
        Public InstaTouchEnabled As Boolean = False
        Public InstaTouchMerchantId As String = ""
        Public NetTellerSecretKey As String = ""

        Public ReadOnly Property CacheIDSuffix() As String
            Get
                Return LenderId.Replace("-", "").ToLower() & "_" & Host
            End Get
		End Property
		Private _xaBeneficiaryLevel As String = "bogus text"
		Public ReadOnly Property XABeneficiaryLevel As String
			Get
				If _xaBeneficiaryLevel = "bogus text" Then
					_xaBeneficiaryLevel = GetXABeneficiaryLevel()
				End If
				Return _xaBeneficiaryLevel
			End Get
		End Property
        Public Sub New(ByVal xmlElement As XmlElement)
            Me.OrganizationId = xmlElement.GetAttribute("organization_id")
            Me.LenderId = xmlElement.GetAttribute("lender_id")
            Me.LenderRef = Common.SafeString(xmlElement.GetAttribute("lender_ref"))
            ' Me.LenderName = xmlElement.GetAttribute("lender_name")
            Me.OrgCode = xmlElement.GetAttribute("org_code")
            Me.LenderCode = xmlElement.GetAttribute("lender_code")
            Me.authKey = xmlElement.GetAttribute("auth_key")
            Me.InstitutionType = CEnum.SafeParseWithDefault(xmlElement.GetAttribute("institution_type"), CEnum.InstitutionType.CU)
            Me.LogoUrl = Common.CompileLogoUrl(xmlElement.GetAttribute("logo_url"))
            Me.Favico = Common.CompileLogoUrl(xmlElement.GetAttribute("fav_ico"))
            Me.OtherLogoUrl = xmlElement.GetAttribute("other_logo_url")
            Me.FinishedUrl = xmlElement.GetAttribute("finished_url")
            Me.FinishedXAUrl = xmlElement.GetAttribute("finished_url_xa")
            Me.Host = SmUtil.ExtractHostFromUrl(xmlElement.GetAttribute("loan_submit_url"))
            Dim sSubmitLoanUrl = xmlElement.GetAttribute("loan_submit_url").NullSafeToLower_
            If sSubmitLoanUrl.Contains("/decisionloan/") Then
                ''get SubmitLoanUrl and BaseSubmitLoanUrl from Web.config to avoid the error CWE 918 when using  
                ''veracode to check server-side request forgery(ssrf): CWE-918
                ''refer case AP-2184 26 SSRF issues found by Veracode in LPQMobileSite code
                Dim sBaseSubmitUrl = sSubmitLoanUrl.Substring(0, sSubmitLoanUrl.IndexOf("/decisionloan/", System.StringComparison.Ordinal))
                Dim sConstSubmitUrl = sSubmitLoanUrl.Substring(sSubmitLoanUrl.IndexOf("/decisionloan/", System.StringComparison.Ordinal))
                Dim sAllowedBaseSubmitUrl = getAllowedBaseSubmitUrl(RemovePathFromUri(sSubmitLoanUrl))
                Me.BaseSubmitLoanUrl = sAllowedBaseSubmitUrl + sBaseSubmitUrl.Substring(sAllowedBaseSubmitUrl.Length)
                Me.SubmitLoanUrl = Me.BaseSubmitLoanUrl + sConstSubmitUrl
            End If
            Dim sSubmitLoanUrl_TPV = xmlElement.GetAttribute("loan_submit_url_tpv").NullSafeToLower_
            If sSubmitLoanUrl_TPV.Contains("/decisionloan/") Then
                Dim sBaseSubmitUrl_TPV = sSubmitLoanUrl_TPV.Substring(0, sSubmitLoanUrl_TPV.IndexOf("/decisionloan/", System.StringComparison.Ordinal))
                Dim sConstSubmitUrl_TPV = sSubmitLoanUrl_TPV.Substring(sSubmitLoanUrl_TPV.IndexOf("/decisionloan/", System.StringComparison.Ordinal))
                Dim sAllowedBaseSubmitUrl_TPV = getAllowedBaseSubmitUrl(RemovePathFromUri(sSubmitLoanUrl_TPV))
                Me.BaseSubmitLoanUrl_TPV = sAllowedBaseSubmitUrl_TPV + sBaseSubmitUrl_TPV.Substring(sAllowedBaseSubmitUrl_TPV.Length)
                Me.SubmitLoanUrl_TPV = Me.BaseSubmitLoanUrl_TPV + sConstSubmitUrl_TPV
                Me.OrgCode2 = xmlElement.GetAttribute("org_code2")
                Me.LenderCode2 = xmlElement.GetAttribute("lender_code2")
            End If
            Me.AuthenticationType = xmlElement.GetAttribute("ida")
            Me.DebitType = xmlElement.GetAttribute("debit")
            Me.CreditPull = xmlElement.GetAttribute("credit_pull")
            If Common.SafeString(xmlElement.GetAttribute("mfa_email_only")) = "Y" Then Me.MfaEmailOnly = True
            If Common.SafeString(xmlElement.GetAttribute("location_pool")) = "Y" Then Me.LocationPool = True
            If Common.SafeString(xmlElement.GetAttribute("homepage_editor")) = "Y" Then Me.HomepageEditor = True

            'deprecate
            Me.LenderCodeBadMember = xmlElement.GetAttribute("lender_code_bad_member")

            'Bad member will run when either LenderCodeBadMember or LenderCode is not null
            If Me.LenderCodeBadMember = "" And Me.LenderCode <> "" Then
                Me.LenderCodeBadMember = Me.LenderCode
            End If

            Me.AddressKey = xmlElement.GetAttribute("address_key")
            Me.CCMaxFunding = xmlElement.GetAttribute("cc_max_funding")
            Me.ACHMaxFunding = xmlElement.GetAttribute("ach_max_funding")
            Me.PaypalMaxFunding = xmlElement.GetAttribute("paypal_max_funding")
            Me.MembershipFee = xmlElement.GetAttribute("membership_fee")
            Me.IsACHFundingDirect = Common.SafeString(xmlElement.GetAttribute("ach_funding_direct_to_account")) = "Y"
            Me.ACHFundingThresholdRequireMicroDeposit = Common.SafeString(xmlElement.GetAttribute("ach_funding_threshold_require_micro_deposit"))
            Me.ACHAutoFundingDisabled = Common.SafeString(xmlElement.GetAttribute("auto_ach_funding_disabled_N")) = "Y"
            Me.IsStandAloneFundingEnabled = Common.SafeString(xmlElement.GetAttribute("funding_enable")) = "Y"
            Me.IsAutoFundingWBookingEnabled = Common.SafeString(xmlElement.GetAttribute("auto_funding_wbooking_enabled_y")) <> "N"
            'IsAutoFundingWBookingDisabled

            Me._webConfigXML = xmlElement
            Me.ScanDocumentKey = xmlElement.GetAttribute("mpao_key2") 'DL scan using ClearImage
            Me.DLBarcodeScan = xmlElement.GetAttribute("dl_barcode_scan") 'DL scan using Scandit
            'Me.AccountsXML = xmlElement.GetAttribute("ACCOUNTS")

            Me.APIUser = xmlElement.GetAttribute("user")
            Me.APIPassword = xmlElement.GetAttribute("password")
            Me.APIUserLender = xmlElement.GetAttribute("user_lender_level")
            Me.APIPasswordLender = xmlElement.GetAttribute("password_lender_level")
            Me.LoanStatusEnum = xmlElement.GetAttribute("loan_status_enum")
            Me.Booking = xmlElement.GetAttribute("booking")
            'Me.VLBooking = If(xmlElement.GetElementsByTagName("VEHICLE_LOAN").Count > 0, CType(xmlElement.GetElementsByTagName("VEHICLE_LOAN")(0), XmlElement).GetAttribute("loan_booking"), "")
            'Me.CCBooking = If(xmlElement.GetElementsByTagName("CREDIT_CARD_LOAN").Count > 0, CType(xmlElement.GetElementsByTagName("CREDIT_CARD_LOAN")(0), XmlElement).GetAttribute("loan_booking"), "")
            'Me.PLBooking = If(xmlElement.GetElementsByTagName("PERSONAL_LOAN").Count > 0, CType(xmlElement.GetElementsByTagName("PERSONAL_LOAN")(0), XmlElement).GetAttribute("loan_booking"), "")
            'Me.HEBooking = If(xmlElement.GetElementsByTagName("HOME_EQUITY_LOAN").Count > 0, CType(xmlElement.GetElementsByTagName("HOME_EQUITY_LOAN")(0), XmlElement).GetAttribute("loan_booking"), "")
            'Me.HELOCBooking = If(xmlElement.GetElementsByTagName("HOME_EQUITY_LOC").Count > 0, CType(xmlElement.GetElementsByTagName("HOME_EQUITY_LOC")(0), XmlElement).GetAttribute("loan_booking"), "")
            Me.OnlineUserRegistration = xmlElement.GetAttribute("online_user_registration")
            'Me.IsFundingEnable = If(Common.SafeString(xmlElement.GetAttribute("funding_enable")) = "Y", True, False)
            Me.IsDecisionXAEnable = Common.SafeString(xmlElement.GetAttribute("decisionXA_enable")) = "Y"
            Me.AppType = xmlElement.GetAttribute("app_type")
            Me.PiWikSiteID = xmlElement.GetAttribute("piwik_site_id")
            Me.GoogleTagManagerID = xmlElement.GetAttribute("google_tag_manager_id")
            Me.FacebookPixelID = xmlElement.GetAttribute("facebook_pixel_id")
            Me.EnabledAddressVerification = Not String.IsNullOrEmpty(xmlElement.GetAttribute("address_key"))
            Me.PayPalEmail = xmlElement.GetAttribute("paypal_email")
            Me.DocGroup = xmlElement.GetAttribute("doc_group")
            Me.DocCode = xmlElement.GetAttribute("doc_code")
            Me.DLScanImage = xmlElement.GetAttribute("dl_scan_image_url")
            Me.PreviousAddressThresdhold = xmlElement.GetAttribute("previous_address_threshold")
            Me.PreviousEmploymentThresdhold = xmlElement.GetAttribute("previous_employment_threshold")
            GetDefaultBranch(xmlElement)
            ''  GetMinorAccountAttributes(xmlElement)

            Dim showInstaTouchSwitchButtonNode = xmlElement.SelectSingleNode("VISIBLE/@APM_instatouch_button_N")
            Dim showInstaTouchSwitchButton = showInstaTouchSwitchButtonNode IsNot Nothing AndAlso showInstaTouchSwitchButtonNode.Value.ToUpper() = "Y"

            Me.InstaTouchEnabled = Common.SafeString(xmlElement.GetAttribute("instatouch_enabled")).Equals("Y") AndAlso showInstaTouchSwitchButton
            Me.InstaTouchMerchantId = Common.SafeString(xmlElement.GetAttribute("instatouch_merchant_id"))
            If Me.InstaTouchMerchantId = "" AndAlso InstaTouchEnabled Then
                Me.InstaTouchMerchantId = "meridian"
            End If
            ''reasonable demonstration code
            Me.demoPDFViewCode = Common.SafeString(xmlElement.GetAttribute("reasonable_demonstration_code_for_pdf_view"))
            Me.demoPDFViewURL = Common.SafeString(xmlElement.GetAttribute("reasonable_demonstration_pdf_url"))
            Me.coApplicantType = Common.SafeString(xmlElement.GetAttribute("co_applicant_type"))
            Me.NetTellerSecretKey = Common.SafeString(xmlElement.GetAttribute("net_teller_secret_key"))
        End Sub
        Private Function getAllowedBaseSubmitUrl(ByVal psBaseSubmitUrl As String) As String
            ''server-side request forgery(ssrf): CWE-918 (refer case AP-2184 26 SSRF issues found by Veracode in LPQMobileSite code)
            ''instead of getting baseSubmitUrl directly form config.xml file, get baseSubmitUrl from Web.config if it exist
            ''otherwise throw exception and display log error message  
            Dim baseSubmitUrlList = Common.SafeString(ConfigurationManager.AppSettings.Get("BaseSubmitUrls")).Split(",")
            Dim sAllowedBaseSubmitUrl = baseSubmitUrlList.FirstOrDefault(Function(x) psBaseSubmitUrl.Equals(x.Trim, StringComparison.OrdinalIgnoreCase))
            If String.IsNullOrEmpty(sAllowedBaseSubmitUrl) Then
                _log.Error("The base submit url is invalid. The domain name of the base submit url '" & psBaseSubmitUrl & "' is not set in web.config file. If this is an off-site URL, please contact AP Development immediately to add this domain to the whitelist.")
                Throw New ArgumentException("The base submit url '" & psBaseSubmitUrl & "' is invalid. The base submit url is not set in web.config file. If this is an off-site URL, please contact AP Development immediately to add this domain to the whitelist.")
                'Web.Configuration should have this
                '<add key = "BaseSubmitUrls" value="https://beta.loanspq.com, https://cs.loanspq.com, https://demo.loanspq.com/>
            End If
            Return sAllowedBaseSubmitUrl.Trim
        End Function
        Private Function RemovePathFromUri(sUrl As String) As String
            Dim o = New Uri(sUrl)
            Dim uriBuilder = New UriBuilder()
            uriBuilder.Scheme = o.Scheme
            uriBuilder.Host = o.Host
            uriBuilder.Port = o.Port
            Dim sUri = uriBuilder.Uri.ToString
            Return sUri.Substring(0, sUri.LastIndexOf("/"))  ''remove the last slash "/" in the sUri string exmple: https://beta.loanspq.com/    
        End Function
        Public Function GetWebConfigElement() As XmlElement
            Return _webConfigXML
        End Function

		Public Function GetLoanDisclosures(ByVal psLoanType As String, Optional psAvailability As String = "") As List(Of String)
			Dim disclosures As New List(Of String)
			If _webConfigXML Is Nothing Then Return disclosures
			disclosures = Common.ParseDisclosures(_webConfigXML, psLoanType, psAvailability)
			Return disclosures
		End Function

		Public Function GetLoanDisclosuresLoc(ByVal psLoanType As String) As List(Of String)
			Dim disclosures As New List(Of String)
			If _webConfigXML Is Nothing Then Return disclosures
			disclosures = Common.ParseDisclosuresLoc(_webConfigXML, psLoanType)
			Return disclosures
		End Function

        Public Function GetCustomQuestions(ByVal psLoanType As String) As List(Of CCustomQuestion)
            Return Common.ParseCustomQuestions(_webConfigXML, psLoanType)
        End Function

        Public Function GetConditionedQuestions(ByVal psLoanType As String) As List(Of CCustomQuestion)
            Return Common.ParseConditionedQuestions(_webConfigXML, psLoanType)
        End Function
        Public Function GetBranches(ByVal psLoanType As String) As Dictionary(Of String, String)
            Return Common.ParseBranches(_webConfigXML, psLoanType)
        End Function

        Public Function GetEnumItems(ByVal enumName As String) As Dictionary(Of String, String)
            Dim dic As New Dictionary(Of String, String)
            If _webConfigXML Is Nothing Then Return dic
            dic = Common.GetItemKeyValueCollectionFromEnumConfig(_webConfigXML, enumName)
            Return dic
		End Function

		Public Function GetEnumWithCatItems(ByVal enumName As String) As Dictionary(Of String, CTextValueCatItem)
			Dim dic As New Dictionary(Of String, CTextValueCatItem)
			If _webConfigXML Is Nothing Then Return dic
			dic = Common.GetItemKeyValueCatCollectionFromEnumConfig(_webConfigXML, enumName)
			Return dic
		End Function

		Public Function GetEnumLineOfCreditItems() As Dictionary(Of String, CLineOfCreditItem)
			Dim dic As New Dictionary(Of String, CLineOfCreditItem)
			If _webConfigXML Is Nothing Then Return dic
			Dim oEnumsNodes As XmlNodeList = _webConfigXML.SelectNodes("ENUMS/PERSONAL_LOC_PURPOSES/ITEM")
			For Each node As XmlElement In oEnumsNodes
				Dim key As String = node.GetAttribute("value")
				If Not dic.ContainsKey(key) Then
					Dim sCategory As String = If(node.Attributes("category") IsNot Nothing, node.Attributes("category").Value, "")
					Dim overDraftOption As CEnum.LocOverDraftOption = CEnum.SafeParseWithDefault(node.GetAttribute("overdraft_option"), CEnum.LocOverDraftOption.BOTH_OVERDRAFT_AND_NON_OVERDRAFT)
					dic.Add(key, New CLineOfCreditItem() With {.Value = key, .Text = node.Attributes("text").Value, .Category = sCategory, .OverDraftOption = overDraftOption})
				End If
			Next
			Return dic
		End Function

		Public Function GetClinicEnumItems(ByVal enumName As String) As Dictionary(Of String, String)
			Dim dic As New Dictionary(Of String, String)
			If _webConfigXML Is Nothing Then Return dic
			dic = Common.GetItemKeyValueCollectionFromClinicEnumConfig(_webConfigXML, enumName)
			Return dic
		End Function

        ''get the occupancy status
        Public Function getOccupancyStatus(ByVal psLoanType As String) As String
            Return Common.ParseOccupancyStatus(_webConfigXML, psLoanType)
        End Function
        ''override previous address/employment threshold for each loan module if the previous address/employment threshold attribute is existing 
        ''in the loan module node otherwise using previous address/employment threshold in website node.
        Public Function getPreviousThreshold(ByVal psLoanType As String, ByVal psPreviousThresholdAttr As String) As String
            Dim oLoanNode = _webConfigXML.SelectSingleNode(psLoanType)
            Dim sPreviousThreshold = ""
            If oLoanNode IsNot Nothing AndAlso oLoanNode.Attributes(psPreviousThresholdAttr) IsNot Nothing AndAlso Common.SafeString(oLoanNode.Attributes(psPreviousThresholdAttr).InnerText) <> "" Then
                sPreviousThreshold = Common.SafeString(oLoanNode.Attributes(psPreviousThresholdAttr).InnerText)
            Else  ''use previous threshold from global node
                Return Common.SafeString(_webConfigXML.GetAttribute(psPreviousThresholdAttr))
            End If
            Return sPreviousThreshold
        End Function
        ''Private Sub GetMinorAccountAttributes(ByVal poWebsiteNode As XmlElement)
        ''    ''minor account
        ''    Dim sMinorAccountXpath As String = "XA_LOAN/SPECIAL_ACCOUNTS"
        ''    Try
        ''        Dim sMinorAccountNode As XmlElement = poWebsiteNode.SelectSingleNode(sMinorAccountXpath)
        ''        If sMinorAccountNode.HasAttributes Then
        ''            minorDecisionXA = Common.SafeString(sMinorAccountNode.GetAttribute("decisionXA_enable"))
        ''            minorDebitType = Common.SafeString(sMinorAccountNode.GetAttribute("debit"))
        ''            minorCreditPull = Common.SafeString(sMinorAccountNode.GetAttribute("credit_pull"))
        ''            
        ''        End If
        ''    Catch ex As Exception


        ''    End Try
        ''End Sub
        Private Sub GetDefaultBranch(ByVal poWebsiteNode As XmlElement)
            Dim sLoanBranchXpath As String = "DEFAULT_LOAN_BRANCH"
            Dim sXABranchXpath As String = "XA_LOAN/DEFAULT_XA_BRANCH"
            Dim oDefaultLoanBranch As XmlElement
            Dim oDefaultXABranch As XmlElement

            sDefaultLoanBranchID = ""
            sDefaultLoanBranchName = ""
            sDefaultXABranchID = ""
            sDefaultXABranchName = ""

            Try
                oDefaultLoanBranch = poWebsiteNode.SelectSingleNode(sLoanBranchXpath)
                If oDefaultLoanBranch.HasAttributes Then
                    sDefaultLoanBranchID = oDefaultLoanBranch.GetAttribute("branch_id")
                    sDefaultLoanBranchName = oDefaultLoanBranch.GetAttribute("branch_name")
                End If
            Catch ex As Exception
                sDefaultLoanBranchID = ""
                sDefaultLoanBranchName = ""
            End Try

            Try
                oDefaultXABranch = poWebsiteNode.SelectSingleNode(sXABranchXpath)
                If oDefaultXABranch.HasAttributes Then
                    sDefaultXABranchID = oDefaultXABranch.GetAttribute("branch_id")
                    sDefaultXABranchName = oDefaultXABranch.GetAttribute("branch_name")
                End If
            Catch ex As Exception
                sDefaultXABranchID = ""
                sDefaultXABranchName = ""
            End Try
        End Sub

		Public Function GetDemographicDescription(ByVal psLoanType As String) As String
			Dim xmlNode = _webConfigXML.SelectSingleNode(psLoanType & "/SUCCESS_MESSAGES/DEMOGRAPHIC_DESCRIPTION_MESSAGE")
			Return Common.GetDemographicDescriptionMessage(xmlNode)
		End Function

		Public Function GetDemographicDisclosure(ByVal psLoanType As String) As String
			Dim xmlNode = _webConfigXML.SelectSingleNode(psLoanType & "/SUCCESS_MESSAGES/DEMOGRAPHIC_DISCLOSURE_MESSAGE")
			Return Common.GetDemographicDisclosureMessage(xmlNode)
		End Function

        Public Function GetXABeneficiaryLevel() As String
            '' get ownership_level from downloading XA preferences         
            ''Dim _xaBeneficiaryLevelNode = _webConfigXML.SelectSingleNode("XA_LOAN/@beneficiary_level")
            ''If _xaBeneficiaryLevelNode IsNot Nothing Then
            ''    Return _xaBeneficiaryLevelNode.Value.ToLower()
            ''End If
            Dim xaPreferencesDoc = CDownloadedSettings.GetXAPreferences(Me)
            If xaPreferencesDoc IsNot Nothing Then
                Dim oBeneficiaryLevelNode As XmlNode = xaPreferencesDoc.SelectSingleNode("PREFERENCES/CORE_SYSTEM/BENEFICIARY_LEVEL")
                If oBeneficiaryLevelNode IsNot Nothing AndAlso Not String.IsNullOrWhiteSpace(oBeneficiaryLevelNode.InnerText) Then
                    Return Common.SafeString(oBeneficiaryLevelNode.InnerText).ToLower()
                End If
            End If
            Return Nothing
        End Function
        Public Function GetOccupancyStatusesForRequiredHousingPayment(psLoanType As String) As List(Of String)
			Dim occupancyStatusesForRequiredHousingPaymentNode = _webConfigXML.SelectSingleNode(psLoanType & "/BEHAVIOR/OCCUPANCY_STATUSES_FOR_REQUIRED_HOUSING_PAYMENT")
			If occupancyStatusesForRequiredHousingPaymentNode Is Nothing Then
				Return New List(Of String)({"RENT"})
			End If

			Dim lstStatuses = New List(Of String)
			For Each status As XmlElement In occupancyStatusesForRequiredHousingPaymentNode.SelectNodes("OCCUPANCY_STATUS")
				lstStatuses.Add(status.GetAttribute("value"))
			Next
			Return lstStatuses
		End Function
	End Class
    Public NotInheritable Class CConfigFile
        Inherits ConfigurationSection

        <ConfigurationProperty("fileLocation", IsRequired:=True)>
        Public ReadOnly Property FileLocation() As String
            Get
                Return CStr(Me("fileLocation"))
            End Get
        End Property

        <ConfigurationProperty("LPQSSO_fileLocation", IsRequired:=False)>
        Public ReadOnly Property LPQSSO_fileLocation() As String
            Get
                Return CStr(Me("LPQSSO_fileLocation"))
            End Get
		End Property

		<ConfigurationProperty("RijndaelManagedKeyFileLocation", IsRequired:=False)>
		Public ReadOnly Property RijndaelManagedKeyFileLocation() As String
			Get
				Return CStr(Me("RijndaelManagedKeyFileLocation"))
			End Get
		End Property

        Public Shared Function getConfig() As CConfigFile
            Return CType(ConfigurationManager.GetSection("ConfigFile"), CConfigFile)
        End Function

    End Class

End Namespace
