﻿Imports System.Xml

Namespace LPQMobile.Utils

	''' <summary>
	''' This is our more practical implementation of day to day usage of the XML data.
	''' It contains only routines and properties we use really need to use and simplies
	''' use of namespaces.
	''' </summary>
	''' <remarks>
	''' Note:
	''' 1. We don't inherit from XmlElement because we want to hide a lot of the API which 
	''' we don't use.
	''' 2. Because we're working closely with XML, most of our input/outputs are simple strings.   We're not trying to blow up this class to support every possible data type.
	''' </remarks>
	Public Class CXmlElement

		Private Shared log As log4net.ILog = log4net.LogManager.GetLogger(GetType(CXmlElement))

		''' <summary>
		''' This is to help us prevent working with obsolete/outdate XML documents.  Turning this on will trigger an INFO trace when 
		''' trying to retrieve an XML Element that does not exist.  
		''' </summary>
		''' <remarks></remarks>
		Public TraceMissingGets As Boolean = False


		''' <summary>
		''' An XML document can contain data from many different namespaces.  Most of the time
		''' we only want to work with one namespace (by setting this property).  If 
		''' left at blank, then all xpaths will execute unmodified.  This value should be the short name that was used to call 
		''' CXmlElement.AddNamespace.
		''' </summary>    
		Public CurrentNamespace As String

		Private _Node As XmlElement
		''' <summary>
		''' Underlying XmlElement we are wrapping.
		''' </summary>
		Public ReadOnly Property Source() As XmlElement
			Get
				Return _Node
			End Get
		End Property

		Private _namespaceManager As System.Xml.XmlNamespaceManager

		''' <summary>
		''' Simple constructor to create this node and start using the data!
		''' </summary>
		''' <param name="poSourceNode"></param>
		''' <remarks>
		''' </remarks>
		Public Sub New(ByVal poSourceNode As XmlNode)
			If poSourceNode Is Nothing Then
				Throw New ArgumentException("Unexpected null paramater: poSourceNode")
			End If
			_Node = CType(poSourceNode, XmlElement)

		End Sub

		''' <summary>
		''' Constructor routine to wrap an XmlNode into a CXmlElement.  If poSourceNode is null, a null will be returned. 
		''' </summary>
		'''<example>
		'''Dim oSTATUS As CXmlElement = CXmlElement.CreateFromNode(ResultDoc.DocumentElement.SelectSingleNode("STATUS"))
		'''If IsNothing(oSTATUS) Then
		'''    Return
		'''End If
		''' </example>
		''' <param name="poSourceNode"></param>        
		Public Shared Function CreateFromNode(ByVal poSourceNode As XmlNode) As CXmlElement
			If IsNothing(poSourceNode) Then
				Return Nothing
			End If
			If poSourceNode.NodeType <> XmlNodeType.Element Then
				Throw New ArgumentException("Invalid node type (expected Element): " & poSourceNode.NodeType)
			End If

			Return New CXmlElement(poSourceNode)


		End Function

		''' <summary>
		''' Adds namespace that we want to support.  Please note that namespaces are copied
		''' on new CXMLElement that are instanted and re-wrapped from here (ie: CreateAndAppendElement).
		''' If you construct a new instance of this class by hand, you'll need to re-add all the namespaces 
		''' in again.
		''' </summary>
		''' <param name="psNamespacePrefix">Usually some short prefix we want to use. </param>
		''' <param name="psFullNamespaceURI">Usuall the full url of the namespace (ie: http://www.google.com/myspace) </param>
		Public Sub AddNamespace(ByVal psNamespacePrefix As String, ByVal psFullNamespaceURI As String)
			If _namespaceManager Is Nothing Then
				_namespaceManager = New System.Xml.XmlNamespaceManager(_Node.OwnerDocument.NameTable)
			End If

			_namespaceManager.AddNamespace(psNamespacePrefix, psFullNamespaceURI)
		End Sub

		''' <summary>
		''' Private constructor used to create new CXmlElement with current node's namespace
		''' setup.  This routine should be used for all CXmlElement node constructions to ensure
		''' all CXmlElement nodes we work with have the proper namespace settings setup.
		''' </summary>
		Private Sub New(ByVal poSourceNode As XmlElement, ByVal psCurrentNamespace As String, ByVal poNamespaceManager As XmlNamespaceManager)
			_Node = poSourceNode
			CurrentNamespace = psCurrentNamespace
			_namespaceManager = poNamespaceManager
		End Sub

#Region "Routines for working with XmlElements & XmlNode"
		''' <summary>
		''' Replacement of the XmlDocument.CreateElement which supports namespace automatically.
		''' </summary>
		''' <param name="psElementName"></param>
		Public Function CreateElement(ByVal psElementName As String) As CXmlElement
			If CurrentNamespace = "" Then
				Return reWrap(_Node.OwnerDocument.CreateElement(psElementName))
			Else
				Return reWrap(_Node.OwnerDocument.CreateElement(psElementName, _namespaceManager.LookupNamespace(CurrentNamespace)))
			End If
		End Function

		''' <summary>
		''' Instead of just creating the element, this routine allows to create AND append after creation.
		''' This will help reduce scenarios where nodes were created but forgot to get
		''' appended to the final document.
		''' </summary>
		Public Function CreateAndAppendElement(ByVal psElementName As String) As CXmlElement
			Dim oresult As CXmlElement = CreateElement(psElementName)
			Me.AppendChild(oresult)
			Return oresult
		End Function

		''' <summary>
		''' 
		''' </summary>
		''' <param name="pSrcElement"></param>
		''' <param name="pRefElement"></param>
		''' <remarks></remarks>
		Public Sub InsertBefore(ByVal pSrcElement As CXmlElement, ByVal pRefElement As CXmlElement)
			'in .net api, if pRefElement is nothing, pSrcElement inserts as first child
			_Node.InsertBefore(pSrcElement.Source, If(pRefElement Is Nothing, Nothing, pRefElement.Source))
		End Sub

		Public Sub InsertAfter(ByVal pSrcElement As CXmlElement, ByVal pRefElement As CXmlElement)
			'in .net api, if pRefElement is nothing, pSrcElement inserts as first child
			_Node.InsertAfter(pSrcElement.Source, If(pRefElement Is Nothing, Nothing, pRefElement.Source))
		End Sub

		''' <summary>
		''' Creates new element and inserts it before the reference element
		''' </summary>
		''' <param name="psNewElementName"></param>
		''' <param name="pRefElement"></param>
		''' <returns></returns>
		''' <remarks></remarks>
		Public Function CreateAndInsertBefore(ByVal psNewElementName As String, ByVal pRefElement As CXmlElement) As CXmlElement
			Dim oResult As CXmlElement = CreateElement(psNewElementName)
			Me.InsertBefore(oResult, pRefElement)
			Return oResult
		End Function

		''' <summary>
		''' Creates new element and inserts it after the reference element
		''' </summary>
		''' <param name="psNewElementName"></param>
		''' <param name="pRefElement"></param>
		''' <returns></returns>
		''' <remarks></remarks>
		Public Function CreateAndInsertAfter(ByVal psNewElementName As String, ByVal pRefElement As CXmlElement) As CXmlElement
			Dim oResult As CXmlElement = CreateElement(psNewElementName)
			Me.InsertAfter(oResult, pRefElement)
			Return oResult
		End Function

		Public Sub AppendChild(ByVal poElement As CXmlElement)
			_Node.AppendChild(poElement.Source)
		End Sub
		Public Sub AppendChild(ByVal poNode As XmlNode)
			_Node.AppendChild(poNode)
		End Sub

		'''' <summary>
		'''' Helper routine to read back a node's ( element, attribute, etc.) value.
		'''' </summary>
		'''' <param name="psNodeNamePath"></param>
		'''' <returns></returns>
		'''' <remarks></remarks>
		'Public Function GetNodeValue(ByVal psNodeNamePath As String) As String
		'    Dim oTmp As XmlNode = _Node.SelectSingleNode(getCorrectPath(psNodeNamePath))
		'    If IsNothing(oTmp) Then
		'        Return ""
		'    End If
		'    Return oTmp.Value()

		'End Function

		''' <summary>
		''' More readible routine to correlate to AddElementValue to access element value.  
		''' </summary>
		''' <param name="psElementName">Can be the name of the element or an xpath to an element</param>
		''' <returns></returns>
		''' <remarks></remarks>
		Public Function GetElementValue(ByVal psElementName As String) As String
			Dim oTmp As XmlNode = SelectSingleNode(psElementName)


			If IsNothing(oTmp) Then
				Return ""
			End If
			If oTmp.NodeType <> XmlNodeType.Element Then
				Return ""
			End If

			Return CType(oTmp, XmlElement).InnerText
		End Function

		''' <summary>
		''' More readible routine to correlate to AddElementValue to access element value.  Supports truncating of data read to avoid data corruption.
		''' </summary>
		''' <param name="psElementName"></param>
		''' <param name="pnMaxReadLength">Should be 0 or greater.</param>
		Public Function GetElementValue(ByVal psElementName As String, ByVal pnMaxReadLength As Integer) As String
			Dim oTmp As XmlNode = SelectSingleNode(psElementName)

			If IsNothing(oTmp) Then
				Return ""
			End If
			If oTmp.NodeType <> XmlNodeType.Element Then
				Return ""
			End If

			Return Left(CType(oTmp, XmlElement).InnerText, pnMaxReadLength)
		End Function


		'Public Function GetElementValueDate(ByVal psElementName As String) As String
		'	Dim dDate As Nullable(Of DateStruct) = GetElementValueDateStruct(psElementName)

		'	If dDate.HasValue Then
		'		Return dDate.Value.ToShortDateString()
		'	Else
		'		Return ""
		'	End If
		'End Function

		'Public Function GetElementValueDateStruct(ByVal psElementName As String) As Nullable(Of DateStruct)
		'	Dim sValue As String = GetElementValue(psElementName)
		'	If sValue = "" Then
		'		Return Nothing
		'	End If

		'	Return ParseDateStruct(sValue)
		'End Function

		''' <summary>
		''' Parses out the specified named attribute and returns a date result with time information.  Use this 
		''' if the attribute is of datatype xs:dateTime
		''' </summary>
		''' <returns>Returns time in local timezone.  If no UTC offset info is provided, time specified is assumed to be local server timezone.</returns>
		''' <remarks>UTC time will automatically be converted into local timezone.</remarks>
		'Public Function GetElementValueDateTime(ByVal psElementName As String) As String
		'	Dim dDate As Nullable(Of DateTimeStruct) = GetElementValueDateTimeStruct(psElementName)

		'	If dDate.HasValue Then
		'		Return dDate.Value.ToTimeZone(CTimeZoneManager.LocalZone).ToDateTime.ToString()
		'	Else
		'		Return ""
		'	End If
		'End Function

		'Public Function GetElementValueDateTimeStruct(ByVal psElementName As String) As Nullable(Of DateTimeStruct)
		'	Dim sValue As String = GetElementValue(psElementName)
		'	If sValue = "" Then
		'		Return Nothing
		'	End If

		'	Return ParseDateTimeStruct(sValue, CTimeZoneManager.LocalZone)
		'End Function

		Public Function AddElementValue(ByVal psElementName As String, ByVal psValue As String) As CXmlElement
			Dim newNode As XmlElement = createNamespaceSafeElement(psElementName)
			newNode.InnerText = psValue
			_Node.AppendChild(newNode)
			Return reWrap(newNode)
		End Function

		''' <summary>
		''' Adds an element with a given value.  If the value exceeds the maxlength, the excess portion will be discarded.
		''' </summary>
		Public Function AddElementValue(ByVal psElementName As String, ByVal psValue As String, ByVal MaxLength As Integer) As CXmlElement
			Return AddElementValue(psElementName, Left(psValue, MaxLength))
		End Function

		''' <summary>
		''' Conditionally create and append a new element as a child of this node IF it's psValue is
		''' not empty string.
		''' </summary>
		''' <param name="psElementName"></param>
		''' <param name="psValue"></param>
		''' <returns>Returns reference to newly created XmlElement object;  null is returned 
		''' if psValue is empty.</returns>    
		Public Function AddOptionalElementValue(ByVal psElementName As String, ByVal psValue As String) As CXmlElement
			If Trim(psValue) = "" Then
				Return Nothing
			End If

			Return AddElementValue(psElementName, psValue)
		End Function

		'Public Function AddElementValueDate(ByVal psElementName As String, ByVal psDateValue As String) As CXmlElement
		'	Return AddElementValue(psElementName, mapDateToXml(ParseDateStruct(psDateValue)))
		'End Function

		'Public Function AddElementValueDate(ByVal psElementName As String, ByVal pdDateValue As DateStruct) As CXmlElement
		'	Return AddElementValue(psElementName, mapDateToXml(pdDateValue))
		'End Function

		'Public Function AddOptionalElementValueDate(ByVal psElementName As String, ByVal psDateValue As String) As CXmlElement
		'	If Trim(psDateValue) = "" Then
		'		Return Nothing
		'	End If

		'	Return AddElementValueDate(psElementName, psDateValue)
		'End Function

		'Public Function AddOptionalElementValueDate(ByVal psElementName As String, ByVal pdDateValue As Nullable(Of DateStruct)) As CXmlElement
		'	If Not pdDateValue.HasValue Then
		'		Return Nothing
		'	End If

		'	Return AddElementValueDate(psElementName, pdDateValue.Value)
		'End Function

		'Public Function AddElementValueDateTime(ByVal psElementName As String, ByVal psDateTimeValue As String) As CXmlElement
		'	Return AddElementValue(psElementName, mapDateTimeToXml(ParseDateTimeStruct(psDateTimeValue, CTimeZoneManager.LocalZone)))
		'End Function

		'Public Function AddElementValueDateTime(ByVal psElementName As String, ByVal pdDateValue As DateTimeStruct) As CXmlElement
		'	Return AddElementValue(psElementName, mapDateTimeToXml(pdDateValue))
		'End Function

		'Public Function AddOptionalElementValueDateTime(ByVal psElementName As String, ByVal psDateTimeValue As String) As CXmlElement
		'	If Trim(psDateTimeValue) = "" Then
		'		Return Nothing
		'	End If

		'	Return AddElementValueDateTime(psElementName, psDateTimeValue)
		'End Function

		'Public Function AddOptionalElementValueDateTime(ByVal psElementName As String, ByVal pdDateValue As Nullable(Of DateTimeStruct)) As CXmlElement
		'	If Not pdDateValue.HasValue Then
		'		Return Nothing
		'	End If

		'	Return AddElementValueDateTime(psElementName, pdDateValue.Value)
		'End Function

		''' <summary>
		''' Polymorphic routine to parrallel the XmlNode object but with added support 
		''' for namespaces
		''' </summary>
		''' <param name="xpath"></param>
		Public Function SelectSingleNode(ByVal xpath As String) As XmlNode
			Dim oResult As XmlNode
			If CurrentNamespace = "" Then
				oResult = _Node.SelectSingleNode(xpath)
			Else
				oResult = _Node.SelectSingleNode(getCorrectPath(xpath), _namespaceManager)
			End If
			If Me.TraceMissingGets AndAlso oResult Is Nothing Then
				log.Info("SelectSingleNode::xpath points to nothing: " & xpath)
			End If

			Return oResult
		End Function

		''' <summary>
		''' Convenience routine to select a single XmlElement as a CXmlElement.
		''' </summary>    
		Public Function SelectSingleElement(ByVal psXpath As String) As CXmlElement
			Return reWrap(CType(SelectSingleNode(psXpath), XmlElement))
		End Function

		''' <summary>
		''' Polymorphic routine to parrallel the XmlNode object but with added support
		''' for namespaces.
		''' </summary>
		''' <param name="psXpath"></param>
		Public Function SelectNodes(ByVal psXpath As String) As XmlNodeList
			If CurrentNamespace = "" Then
				Return _Node.SelectNodes(psXpath)
			Else
				Return _Node.SelectNodes(getCorrectPath(psXpath), _namespaceManager)
			End If
		End Function

		''' <summary>
		''' Convenience routine to select a bunch of XmlElement as a list of CXmlElement.
		''' </summary>
		''' <param name="psXpath"></param>
		''' <returns></returns>
		''' <remarks>
		''' Assumes psXPath will return a set of XmlElement;  otherwise will 
		''' cause error when outputting results.
		''' </remarks>
		Public Function SelectElements(ByVal psXpath As String) As CXmlElementList
			Dim oResult As New CXmlElementList

			For Each oNode As XmlNode In SelectNodes(psXpath)
				oResult.Add(reWrap(CType(oNode, XmlElement)))
			Next

			Return oResult
		End Function

		Public Sub RemoveChildElement(ByVal psElementName As CXmlElement)
			_Node.RemoveChild(psElementName._Node)
		End Sub

#End Region

#Region "Routines for working with Attributes"
#Region "SetAttributeXXX"


		''' <summary>
		''' Routine to conditionally create an attribute with a given value.  If value 
		''' is considered empty ( blank string, 0' time) then no attribute will be 
		''' created.
		''' </summary>
		''' <param name="psAttribName"></param>
		''' <param name="psValue"></param>    
		Public Sub SetAttribute(ByVal psAttribName As String, ByVal psValue As String)
			If psAttribName = "" Then
				'this check is needed or else our code maybe setting empty values and we might catch the bug until real data is entered
				Throw New System.ArgumentException("psAttribName cannot be empty!")
			End If

			Select Case Trim(psValue) 'we trim b/c schema's don't allow for "" or " " tokens anyhow
				Case "", "0000-00-00T00:00:00", "0000-00-00"
					Return
			End Select

			_Node.SetAttribute(psAttribName, psValue)
		End Sub

		''' <summary>
		''' Re-routes to the original XmlElement's setAttribute without any special checks.
		''' </summary>
		Public Sub SetAttributeRaw(ByVal psAttribName As String, ByVal psValue As String)
			If psAttribName = "" Then
				'this check is needed or else our code maybe setting empty values and we might catch the bug until real data is entered
				Throw New System.ArgumentException("psAttribName cannot be empty!")
			End If

			_Node.SetAttribute(psAttribName, psValue)
		End Sub

		''' <summary>
		''' Re-routes to the original XmlElement's setAttribute by namespace without any special checks.
		''' </summary>
		Public Sub SetAttributeWithNameSpace(ByVal psAttribName As String, ByVal NameSpaceURI As String, ByVal psValue As String)
			If psAttribName = "" Then
				'this check is needed or else our code maybe setting empty values and we might catch the bug until real data is entered
				Throw New System.ArgumentException("psAttribName cannot be empty!")
			End If

			_Node.SetAttribute(psAttribName, NameSpaceURI, psValue)
		End Sub

		''' <summary>
		''' Adds attribute formatted to 3 decimal places with rounding as needed.
		''' </summary>
		Public Sub SetAttributeRate(ByVal psAttribName As String, ByVal pnValue As Double)
			_Node.SetAttribute(psAttribName, FormatNumber(pnValue, 3, , TriState.False, TriState.False))
		End Sub
		''' <summary>
		''' Adds attribute formatted to 2 decimal places with rounding as needed.
		''' </summary>
		Public Sub SetAttributeMoney(ByVal psAttribName As String, ByVal pnValue As Double)
			_Node.SetAttribute(psAttribName, FormatNumber(pnValue, 2, , TriState.False, TriState.False))
		End Sub

		'Public Sub SetAttributeDateTime(ByVal psAttribName As String, ByVal psValue As String)
		'	Dim dDate As Nullable(Of DateTime) = ParseDate(psValue)

		'	If dDate.HasValue Then
		'		SetAttributeDateTime(psAttribName, New DateTimeStruct(dDate.Value))
		'	Else
		'		SetAttributeDateTime(psAttribName, New Nullable(Of DateTimeStruct)(Nothing))
		'	End If
		'End Sub

		'Public Sub SetAttributeDate(ByVal psAttribName As String, ByVal psValue As String)
		'	SetAttributeDate(psAttribName, ParseDateStruct(psValue))
		'End Sub

		'Public Sub SetAttributeDateTime(ByVal psAttribName As String, ByVal pdValue As Nullable(Of DateTimeStruct))
		'	SetAttribute(psAttribName, mapDateTimeToXml(pdValue))
		'End Sub

		'Public Sub SetAttributeDate(ByVal psAttribName As String, ByVal pdValue As Nullable(Of DateStruct))
		'	SetAttribute(psAttribName, mapDateToXml(pdValue))
		'End Sub
		Public Sub SetAttributeDateTime(ByVal psAttribName As String, ByVal pdValue As Nullable(Of DateTimeOffset))
			SetAttribute(psAttribName, mapDateTimeToXml(pdValue))
		End Sub

		Public Sub SetAttributeYNBool(ByVal psAttribName As String, ByVal pbValue As Boolean)
			If True = pbValue Then
				_Node.SetAttribute(psAttribName, "Y")
			Else
				_Node.SetAttribute(psAttribName, "N")
			End If
		End Sub

		Public Sub SetAttributeYNBool(ByVal psAttribName As String, ByVal poValue As Nullable(Of Boolean))
			If poValue.HasValue Then
				If True = poValue.Value Then
					_Node.SetAttribute(psAttribName, "Y")
				Else
					_Node.SetAttribute(psAttribName, "N")
				End If
			Else
				_Node.SetAttribute(psAttribName, "")
			End If

		End Sub

#End Region
#Region "GetAttributeXXX"

		Public Function GetAttribute(ByVal psName As String, Optional ByVal psDefault As String = "") As String
			If _Node.Attributes(psName) Is Nothing Then
				Return psDefault
			Else
				Return _Node.GetAttribute(psName)
			End If
		End Function


		''' <summary>
		''' Parses out the specified named attribute and returns a date result (NO time information).  Use this 
		''' if the attribute is of datatype xs:date.
		''' </summary>
		''' <param name="psName"></param>
		''' <returns>Short date in the format of mm/dd/yyyy.  If value is invalid, then a blank will be returned.</returns>
		''' <remarks></remarks>
		'Public Function GetAttributeDate(ByVal psName As String) As String
		'	Dim dDate As Nullable(Of DateStruct) = GetAttributeDateStruct(psName)

		'	If dDate.HasValue Then
		'		Return dDate.Value.ToShortDateString
		'	Else
		'		Return ""
		'	End If
		'End Function

		'Public Function GetAttributeNativeDateTime(ByVal psName As String) As DateTime
		'	Dim dDate As Nullable(Of DateTimeStruct) = GetAttributeDateTimeStruct(psName)

		'	If dDate.HasValue Then
		'		Return dDate.Value.ToTimeZone(CTimeZoneManager.LocalZone).ToDateTime
		'	Else
		'		Return DateTime.MinValue
		'	End If
		'End Function

		''' <summary>
		''' Get the date portion of the attribute value.  This ignores all time and timezone information.
		''' </summary>
		''' <param name="psName"></param>
		''' <returns></returns>
		''' <remarks></remarks>
		'Public Function GetAttributeDateStruct(ByVal psName As String) As Nullable(Of DateStruct)
		'	Dim sValue As String = _Node.GetAttribute(psName)

		'	Return ParseDateStruct(sValue)
		'End Function

		''' <summary>
		''' Parses out the specified named attribute and returns a date result with time information.  Use this 
		''' if the attribute is of datatype xs:dateTime
		''' </summary>
		''' <param name="psName"></param>
		''' <returns>Returns time in local timezone.  If no UTC offset info is provided, time specified is assumed to be local server timezone.</returns>
		''' <remarks>UTC time will automatically be converted into local timezone.</remarks>
		'Public Function GetAttributeDateTime(ByVal psName As String) As String
		'	Dim dDate As Nullable(Of DateTimeStruct) = GetAttributeDateTimeStruct(psName)

		'	If dDate.HasValue Then
		'		Return dDate.Value.ToTimeZone(CTimeZoneManager.LocalZone).ToDateTime.ToString
		'	Else
		'		Return ""
		'	End If
		'End Function

		'Public Function GetAttributeDateTimeStruct(ByVal psName As String) As Nullable(Of DateTimeStruct)
		'	Dim sValue As String = _Node.GetAttribute(psName)
		'	If sValue = "" Then
		'		Return Nothing
		'	End If

		'	Return ParseDateTimeStruct(sValue, CTimeZoneManager.LocalZone)
		'End Function
#End Region

#Region "TryGetAttributeXXX"
		Public Function TryGetAttribute(ByVal psName As String, ByRef psOutValue As String) As Boolean
			If _Node.Attributes(psName) Is Nothing Then
				Return False
			Else
				psOutValue = _Node.GetAttribute(psName)
				Return True
			End If
		End Function

		Public Function TryGetAttribute(ByVal psName As String, ByRef psOutValue As Double) As Boolean
			If _Node.Attributes(psName) Is Nothing Then
				Return False
			Else
				Return Double.TryParse(_Node.GetAttribute(psName), psOutValue)
			End If
		End Function

		Public Function TryGetAttribute(ByVal psName As String, ByRef psOutValue As Integer) As Boolean
			If _Node.Attributes(psName) Is Nothing Then
				Return False
			Else
				Return Integer.TryParse(_Node.GetAttribute(psName), psOutValue)
			End If
		End Function

		Public Function TryGetAttribute(ByVal psName As String, ByRef psOutValue As Long) As Boolean
			If _Node.Attributes(psName) Is Nothing Then
				Return False
			Else
				Return Long.TryParse(_Node.GetAttribute(psName), psOutValue)
			End If
		End Function

		'Public Function TryGetAttributeDate(ByVal psName As String, ByRef psOutValue As String) As Boolean
		'	Dim sDate As String = GetAttributeDate(psName)

		'	If SafeString(sDate) = "" Then
		'		Return False
		'	Else
		'		psOutValue = sDate
		'		Return True
		'	End If
		'End Function

		'Public Function TryGetAttributeDateStruct(ByVal psName As String, ByRef pdOutValue As DateStruct) As Boolean
		'	Dim dDate As Nullable(Of DateStruct) = GetAttributeDateStruct(psName)

		'	If dDate.HasValue Then
		'		pdOutValue = dDate.Value
		'		Return True
		'	Else
		'		Return False
		'	End If
		'End Function

		'Public Function TryGetAttributeDateTime(ByVal psName As String, ByRef psOutValue As String) As Boolean
		'	Dim sDate As String = GetAttributeDateTime(psName)

		'	If SafeString(sDate) = "" Then
		'		Return False
		'	Else
		'		psOutValue = sDate
		'		Return True
		'	End If
		'End Function

		'Public Function TryGetAttributeDateTimeStruct(ByVal psName As String, ByRef pdOutValue As DateTimeStruct) As Boolean
		'	Dim dDate As Nullable(Of DateTimeStruct) = GetAttributeDateTimeStruct(psName)

		'	If dDate.HasValue Then
		'		pdOutValue = dDate.Value
		'		Return True
		'	Else
		'		Return False
		'	End If
		'End Function

		'Public Function TryGetAttributeNativeDateTime(ByVal psName As String, ByRef pdOutValue As DateTime) As Boolean
		'	Dim dDate As Nullable(Of DateTimeStruct) = GetAttributeDateTimeStruct(psName)

		'	If dDate.HasValue Then
		'		pdOutValue = dDate.Value.ToTimeZone(CTimeZoneManager.LocalZone).ToDateTime
		'		Return True
		'	Else
		'		Return False
		'	End If
		'End Function

		Public Function TryGetElementValue(ByVal psName As String, ByRef psOutValue As String) As Boolean
			Dim oTmp As XmlNode = SelectSingleNode(psName)

			If IsNothing(oTmp) Then
				Return False
			End If
			If oTmp.NodeType <> XmlNodeType.Element Then
				Return False
			End If

			psOutValue = CType(oTmp, XmlElement).InnerText
			Return True
		End Function

		Public Function TryGetElementValue(ByVal psName As String, ByVal pnMaxReadLength As Integer, ByRef psOutValue As String) As Boolean
			Dim sOutput As String = ""
			Dim bSuccess As Boolean = False

			bSuccess = TryGetElementValue(psName, sOutput)
			If False = bSuccess Then
				Return False
			Else
				psOutValue = Left(sOutput, pnMaxReadLength)
				Return True
			End If
		End Function


		'Public Function TryGetElementDate(ByVal psName As String, ByRef psOutValue As String) As Boolean
		'	Dim sDate As String = GetElementValueDate(psName)

		'	If SafeString(sDate) = "" Then
		'		Return False
		'	Else
		'		psOutValue = sDate
		'		Return True
		'	End If
		'End Function

		'Public Function TryGetElementDateStruct(ByVal psName As String, ByRef pdOutValue As DateStruct) As Boolean
		'	Dim dDate As Nullable(Of DateStruct) = GetElementValueDateStruct(psName)

		'	If dDate.HasValue Then
		'		pdOutValue = dDate.Value
		'		Return True
		'	Else
		'		Return False
		'	End If
		'End Function

		'Public Function TryGetElementDateTime(ByVal psName As String, ByRef psOutValue As String) As Boolean
		'	Dim sDate As String = GetElementValueDateTime(psName)

		'	If SafeString(sDate) = "" Then
		'		Return False
		'	Else
		'		psOutValue = sDate
		'		Return True
		'	End If
		'End Function

		'Public Function TryGetElementDateTimeStruct(ByVal psName As String, ByRef pdOutValue As DateTimeStruct) As Boolean
		'	Dim dDate As Nullable(Of DateTimeStruct) = GetElementValueDateTimeStruct(psName)

		'	If dDate.HasValue Then
		'		pdOutValue = dDate.Value
		'		Return True
		'	Else
		'		Return False
		'	End If
		'End Function

		'Public Function TryGetElementNativeDateTime(ByVal psName As String, ByRef pdOutValue As DateTime) As Boolean
		'	Dim dDate As Nullable(Of DateTimeStruct) = GetElementValueDateTimeStruct(psName)

		'	If dDate.HasValue Then
		'		pdOutValue = dDate.Value.ToTimeZone(CTimeZoneManager.LocalZone).ToDateTime
		'		Return True
		'	Else
		'		Return False
		'	End If
		'End Function
#End Region
		Public Sub RemoveAttribute(ByVal pName As String)
			_Node.RemoveAttribute(pName)
		End Sub
#End Region

		''' <summary>
		''' Returns the source's OuterXml content.
		''' </summary>		
		Public Overrides Function ToString() As String
			If Source Is Nothing Then
				Return ""
			Else
				Return Source.OuterXml
			End If
		End Function


#Region "Internal Helper & Mapping Routines"

		Private Function reWrap(ByVal poXmlElement As XmlElement) As CXmlElement
			If IsNothing(poXmlElement) Then
				Return Nothing
			Else
				Dim oResult As New CXmlElement(poXmlElement, CurrentNamespace, _namespaceManager)
				oResult.TraceMissingGets = Me.TraceMissingGets
				Return oResult
			End If

		End Function

		Private Function createNamespaceSafeElement(ByVal psElementName As String) As XmlElement
			If CurrentNamespace = "" Then
				Return _Node.OwnerDocument.CreateElement(psElementName)
			Else
				Return _Node.OwnerDocument.CreateElement(psElementName, _namespaceManager.LookupNamespace(CurrentNamespace))
			End If
		End Function

		'Private Function mapDateTimeToXml(ByVal pdDate As Nullable(Of DateTimeStruct)) As String
		'	If pdDate.HasValue Then
		'		Dim dDate As DateTimeOffset = pdDate.Value.ToTimeZone(CTimeZoneManager.LocalZone).ToDateTimeOffset

		'		Return System.Xml.XmlConvert.ToString(dDate, "o")
		'	End If

		'	Return "0000-00-00T00:00:00"
		'End Function

		Private Function mapDateTimeToXml(ByVal pdDate As Nullable(Of DateTimeOffset)) As String
			If pdDate.HasValue Then
				Return System.Xml.XmlConvert.ToString(pdDate.Value, "o")
			End If

			Return "0000-00-00T00:00:00"
		End Function

		'Private Function mapDateToXml(ByVal pdDate As Nullable(Of DateStruct)) As String
		'	If pdDate.HasValue Then
		'		Return pdDate.Value.ToString("yyyy-MM-dd")
		'	End If

		'	Return "0000-00-00"
		'End Function

		''' <summary>
		''' Formats our xpath that we're used to with the appropriate prefixes to make
		''' it work with our DOM parser.
		''' ie: Assume our current namespace is ml, then an xpath like:
		''' /Book/Author/Name 
		''' will be converted to:
		'''  /ml:Book/ml:Author/ml:Name
		''' </summary>
		Private Function getCorrectPath(ByVal sXPath As String) As String
			If CurrentNamespace = "" Then
				Return sXPath
			End If

			Dim xPathWithNS As String = ""
			Dim arrXPath() As String
			arrXPath = sXPath.Split("/"c)
			For i As Integer = 0 To arrXPath.Length - 1
				If arrXPath(i).IndexOf(":") = -1 Then
					xPathWithNS += CurrentNamespace & ":" & arrXPath(i)
				End If
				If i <> arrXPath.Length - 1 Then
					xPathWithNS += "/"
				End If
			Next
			Return xPathWithNS
		End Function
#End Region

	End Class

	Public Class CXmlElementList
		Inherits System.Collections.Generic.List(Of CXmlElement)
	End Class
End Namespace
