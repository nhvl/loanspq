﻿Imports System.Net.Mail
Imports System.Web.Security.AntiXss
Imports CustomList
Imports Microsoft.VisualBasic
Imports System.IO
Imports System.Xml
Imports System.Net
Imports log4net
Imports LPQMobile.BO
Imports LPQMobile.DBManager
Imports System.Net.Json
Imports System.Web
Imports System.Linq
Imports iTextSharp.text
Imports iTextSharp.text.pdf
Imports System.Configuration.ConfigurationManager
Imports System.Web.Script.Serialization
Imports System.Text
Imports System.Text.RegularExpressions
Imports System.Collections.Specialized
Imports System.Web.UI
Imports System.Configuration
Imports Newtonsoft.Json
Imports Newtonsoft.Json.Serialization
Imports System.Reflection
Imports System.ComponentModel
Imports CustomApplicationScenarios

Namespace LPQMobile.Utils

    Partial Public Class Common
        'Shared _Log As ILog = LogManager.GetLogger("Common")

        Private Shared _log As log4net.ILog = log4net.LogManager.GetLogger(GetType(Common))

#Region "Static String"

        Public Shared NAMESPACE_URI As String = "http://www.meridianlink.com/CLF"
        Public Shared MOTOR_CYCLE_MAKE_PATH As String = "~/App_Data/MOTOR_CYCLE_MAKE.txt"
        Public Shared RV_OR_BOAT_MAKE_PATH As String = "~/App_Data/RV_OR_BOAT_MAKE.txt"
        Public Shared CAR_MAKE_PATH As String = "~/App_Data/CAR_MAKE.txt"
        Public Shared EMPLOYMENT_STATUS_PATH As String = "~/App_Data/EMPLOYMENT_STATUS.xml"
        Public Shared ROUTING_INFO_PATH As String = "~/App_Data/ROUTING_INFO.txt"
		Public Shared OPTION_TAG_FORMAT As String = "<option class='rename-able' value='{0}'>{1}</option>"
		Public Shared OPTION_TAG_FORMAT_WITH_SELECTED_ITEM As String = "<option value='{0}' selected='selected'>{1}</option>"

#End Region

		Public Shared ReadOnly Property LPQ_WEBSITE As String
			Get
				Dim sLPQ_WEBSITE As String = SafeString(AppSettings.Get("LPQ_WEBSITE"))
				If sLPQ_WEBSITE <> "" Then
					Return sLPQ_WEBSITE
				Else
					Return "https://apptest.loanspq.com"
				End If
			End Get
		End Property

		Public Shared ReadOnly Property LQB_WEBSITE As String
			Get
				Dim sLQB_WEBSITE As String = SafeString(AppSettings.Get("LQB_WEBSITE"))
				If sLQB_WEBSITE <> "" Then
					Return sLQB_WEBSITE
				Else
					Return "http://merchant.loanspq.com"
				End If
			End Get
		End Property

		Public Shared ReadOnly Property LQB_WEBSITE_GETTOKEN_USER As String
			Get
				Dim sLQB_WEBSITE_GETTOKEN_USER As String = SafeString(AppSettings.Get("LQB_WEBSITE_GETTOKEN_USER"))
				If sLQB_WEBSITE_GETTOKEN_USER <> "" Then
					Return sLQB_WEBSITE_GETTOKEN_USER
				Else
					Return ""
				End If
			End Get
		End Property

		Public Shared ReadOnly Property LQB_WEBSITE_GETTOKEN_PASSWORD As String
			Get
				Dim sLQB_WEBSITE_GETTOKEN_PASSWORD As String = SafeString(AppSettings.Get("LQB_WEBSITE_GETTOKEN_PASSWORD"))
				If sLQB_WEBSITE_GETTOKEN_PASSWORD <> "" Then
					Return sLQB_WEBSITE_GETTOKEN_PASSWORD
				Else
					Return ""
				End If
			End Get
		End Property


        Public Shared Function GetEnumDescription(e As [Enum]) As String

            Dim t As Type = e.GetType()

            Dim attr = CType(t.
                    GetField([Enum].GetName(t, e)).
                    GetCustomAttribute(GetType(DescriptionAttribute)),
                    DescriptionAttribute)

            If attr IsNot Nothing Then
                Return attr.Description
            Else
                Return e.ToString
            End If

        End Function
        Public Shared Function GetDays(Optional ByVal psOptionalValue As String = "") As String
			Dim arr As New List(Of String)
			For i As Integer = 1 To 31
				arr.Add(i.ToString())
			Next
			Return RenderDropdownlist(arr.ToArray(), psOptionalValue)
		End Function

		Public Shared Function GetMonths(Optional ByVal psOptionalValue As String = "") As String
			Dim arr As New List(Of String)
			For i As Integer = 0 To 11
				arr.Add(i.ToString())
			Next
			Return RenderDropdownlist(arr.ToArray(), psOptionalValue)
		End Function
		'getyearsdropdownlist
		Public Shared Function GetYears(Optional ByVal psOptionalValue As String = "") As String
			Dim arr As New List(Of String)
			For i As Integer = 0 To 99
				arr.Add(i.ToString())
			Next
			Return RenderDropdownlist(arr.ToArray(), psOptionalValue)

		End Function

		Public Shared Function RenderDropdownlist(ByVal items As String(),
												  Optional ByVal optionalValue As String = "",
												  Optional ByVal selectedItem As String = "") As String
			Dim dropdown As New StringBuilder()
			If Not String.IsNullOrEmpty(optionalValue) Then
				dropdown.AppendFormat(OPTION_TAG_FORMAT, optionalValue, StrConv(optionalValue, VbStrConv.ProperCase))
			End If
			For Each item As String In items
				If item.ToLower() = selectedItem.ToLower() Then
					dropdown.AppendFormat(OPTION_TAG_FORMAT_WITH_SELECTED_ITEM, item, StrConv(item, VbStrConv.ProperCase))
				Else
					dropdown.AppendFormat(OPTION_TAG_FORMAT, item, StrConv(item, VbStrConv.ProperCase))
				End If
			Next
			Return dropdown.ToString()
		End Function


		Public Shared Function RenderDropdownlistWithEmpty(ByVal items As String(),
												  ByVal emptyValue As String,
												  ByVal emptyText As String,
												  Optional ByVal selectedItem As String = "") As String
			Dim dropdown As New StringBuilder()
			dropdown.AppendFormat(OPTION_TAG_FORMAT, emptyValue, StrConv(emptyText, VbStrConv.ProperCase))
			For Each item As String In items
				If item.ToLower() = selectedItem.ToLower() Then
					Dim sItemTemp As String = StrConv(item, VbStrConv.ProperCase)
					If item.Contains("/DOD") Then  'restore propercase for Government/DOD
						sItemTemp = sItemTemp.Replace("/Dod", "/DOD")
					End If
					If item.Contains("US C") Then  'restore propercase for Government/DOD
						sItemTemp = sItemTemp.Replace("Us C", "US C")
					End If
					dropdown.AppendFormat(OPTION_TAG_FORMAT_WITH_SELECTED_ITEM, item, sItemTemp)
				Else
					Dim sItemTemp As String = StrConv(item, VbStrConv.ProperCase)
					If item.Contains("/DOD") Then  'restore propercase for Government/DOD
						sItemTemp = sItemTemp.Replace("/Dod", "/DOD")
					End If
					If item.Contains("US C") Then  'restore propercase for Government/DOD
						sItemTemp = sItemTemp.Replace("Us C", "US C")
					End If
					dropdown.AppendFormat(OPTION_TAG_FORMAT, item, sItemTemp)
				End If
			Next
			Return dropdown.ToString()
		End Function

		Public Shared Function RenderStateDropdownlist(ByVal items As String(), Optional ByVal optionalValue As String = "", Optional ByVal selectedItem As String = "") As String
			Dim dropdown As New StringBuilder()
			If Not String.IsNullOrEmpty(optionalValue) Then
				dropdown.AppendFormat(OPTION_TAG_FORMAT, optionalValue, optionalValue)
			End If
			For Each item As String In items
				If item.ToLower() = selectedItem.ToLower() Then
					dropdown.AppendFormat(OPTION_TAG_FORMAT_WITH_SELECTED_ITEM, item, CEnum.MapStatesToString(item))
				Else
					dropdown.AppendFormat(OPTION_TAG_FORMAT, item, CEnum.MapStatesToString(item))
				End If
			Next
			Return dropdown.ToString()
		End Function

		Public Shared Function RenderStateDropdownlistWithEmpty(ByVal items As String(), ByVal emptyValue As String, ByVal emptyText As String, Optional ByVal selectedItem As String = "") As String
			Dim dropdown As New StringBuilder()
			dropdown.AppendFormat(OPTION_TAG_FORMAT, emptyValue, emptyText)
			For Each item As String In items
				If item.ToLower() = selectedItem.ToLower() Then
					dropdown.AppendFormat(OPTION_TAG_FORMAT_WITH_SELECTED_ITEM, item, CEnum.MapStatesToString(item))
				Else
					dropdown.AppendFormat(OPTION_TAG_FORMAT, item, CEnum.MapStatesToString(item))
				End If
			Next
			Return dropdown.ToString()
		End Function

		Public Shared Function RenderDropdownlist(ByVal items As String(),
												ByVal itemNames As String(),
												Optional ByVal optionalValue As String = "",
												Optional ByVal selectedItem As String = ""
												) As String
			Dim dropdown As New StringBuilder()
			If Not String.IsNullOrEmpty(optionalValue) Then
				dropdown.AppendFormat(OPTION_TAG_FORMAT, optionalValue, StrConv(optionalValue, VbStrConv.ProperCase))
			End If

			Dim forceProperCase As Boolean = True
			For j As Integer = 0 To itemNames.Length - 1
				''If the 1st or 2nd item already has proper case then skip the proper case logic
				If forceProperCase AndAlso itemNames(j) = StrConv(itemNames(j), VbStrConv.ProperCase) Then
					forceProperCase = False
				End If
				If j = 2 Then Exit For
			Next

			For I As Integer = 0 To items.Length - 1
				Dim sItemNamesTemp As String = itemNames(I)

				''proper case
				If forceProperCase Then
					sItemNamesTemp = StrConv(itemNames(I), VbStrConv.ProperCase)

					'undo propercase for a few senarios
					If itemNames(I).Contains(" ID") And Not sItemNamesTemp.Contains(" Identi") Then
						sItemNamesTemp = sItemNamesTemp.Replace(" Id", " ID") 'ecu
					End If
					If itemNames(I).Contains("US ") Then
						sItemNamesTemp = sItemNamesTemp.Replace("Us ", "US ")
					End If
					If itemNames(I).Contains("PIV ") Then
						sItemNamesTemp = sItemNamesTemp.Replace("Piv ", "PIV ")	'ecu
					End If
					If itemNames(I).Contains("'S ") Then
						sItemNamesTemp = sItemNamesTemp.Replace("'S ", "'s ")	'Foreign Driver's License
					End If
				End If

				If items(I).ToLower() = selectedItem.ToLower() Then
					dropdown.AppendFormat(OPTION_TAG_FORMAT_WITH_SELECTED_ITEM, items(I), sItemNamesTemp)
				Else
					dropdown.AppendFormat(OPTION_TAG_FORMAT, items(I), sItemNamesTemp)
				End If
			Next
			Return dropdown.ToString()
		End Function

		Public Shared Function RenderDropdownlistWithEmpty(ByVal items As String(),
												ByVal itemNames As String(),
												ByVal emptyValue As String,
												ByVal emptyText As String,
												Optional ByVal selectedItem As String = ""
												) As String
			Dim dropdown As New StringBuilder()
			dropdown.AppendFormat(OPTION_TAG_FORMAT, emptyValue, emptyText)
			Dim I As Integer

			For I = 0 To items.Length - 1
				If items(I).ToLower() = selectedItem.ToLower() Then
					dropdown.AppendFormat(OPTION_TAG_FORMAT_WITH_SELECTED_ITEM, items(I), itemNames(I))
				Else
					dropdown.AppendFormat(OPTION_TAG_FORMAT, items(I), itemNames(I))
				End If
			Next
			Return dropdown.ToString()
		End Function

		Public Shared Function RenderDropdownlist(ByVal nameValueCollection As Dictionary(Of String, String),
		  ByVal optionalValue As String, ByVal selectedItem As String) As String
			Dim items(nameValueCollection.Count - 1) As String
			Dim values(nameValueCollection.Count - 1) As String

			nameValueCollection.Keys.CopyTo(items, 0)
			nameValueCollection.Values.CopyTo(values, 0)

			Return RenderDropdownlist(items, values, " ", selectedItem)
		End Function

		Public Shared Function RenderDropdownlistWithEmpty(ByVal nameValueCollection As Dictionary(Of String, String), ByVal optionalValue As String, ByVal optionalText As String, ByVal selectedItem As String) As String
			Dim items(nameValueCollection.Count - 1) As String
			Dim values(nameValueCollection.Count - 1) As String

			nameValueCollection.Keys.CopyTo(items, 0)
			nameValueCollection.Values.CopyTo(values, 0)

			Return RenderDropdownlistWithEmpty(items, values, " ", optionalText, selectedItem)
		End Function

		Public Shared Function RenderDropdownlist(ByVal nameValueCollection As Dictionary(Of String, String), Optional ByVal optionalValue As String = "") As String
			Dim dropdown As New StringBuilder()
			If Not String.IsNullOrEmpty(optionalValue) Then
				dropdown.AppendFormat(OPTION_TAG_FORMAT, optionalValue, optionalValue)
			End If
			For Each key As String In nameValueCollection.Keys
				dropdown.AppendFormat(OPTION_TAG_FORMAT, key, nameValueCollection(key))
			Next
			Return dropdown.ToString()
		End Function

		Public Shared Function RenderDropdownlistWithEmpty(ByVal nameValueCollection As Dictionary(Of String, String), ByVal emptyValue As String, ByVal emptyText As String) As String
			Dim dropdown As New StringBuilder()
			dropdown.AppendFormat(OPTION_TAG_FORMAT, emptyValue, emptyText)
			For Each key As String In nameValueCollection.Keys
				dropdown.AppendFormat(OPTION_TAG_FORMAT, key, nameValueCollection(key))
			Next
			Return dropdown.ToString()
		End Function

		Public Shared Function RenderDrodownListFromXML(ByVal xmlPath As String, Optional ByVal optionalValue As String = "") As String
			Dim dropdown As New StringBuilder()
			If Not File.Exists(xmlPath) Then
				Return dropdown.AppendFormat(OPTION_TAG_FORMAT, optionalValue, optionalValue).ToString()
			End If

			If Not String.IsNullOrEmpty(optionalValue) Then
				dropdown.AppendFormat(OPTION_TAG_FORMAT, optionalValue, optionalValue)
			End If

			'' Read XML to get value and text
			Dim oDoc As New XmlDocument()
			oDoc.Load(xmlPath)
			For Each element As XmlElement In oDoc.DocumentElement.ChildNodes
				dropdown.AppendFormat(OPTION_TAG_FORMAT, element.GetAttribute("value"), element.GetAttribute("text"))
			Next

			Return dropdown.ToString()
		End Function

		Public Shared Function RenderDrodownListFromText(ByVal textFilePath As String, Optional ByVal optionalValue As String = "") As String
			Dim dropdown As New StringBuilder()
			If Not File.Exists(textFilePath) Then
				Return dropdown.AppendFormat(OPTION_TAG_FORMAT, optionalValue, optionalValue).ToString()
			End If

			If Not String.IsNullOrEmpty(optionalValue) Then
				dropdown.AppendFormat(OPTION_TAG_FORMAT, optionalValue, optionalValue)
			End If

			'' Read text file to get value and text
			Dim data As New List(Of String)
			'          _Log.Debug("Read line from file: " + textFilePath)
			If File.Exists(textFilePath) Then
				Using reader As New StreamReader(textFilePath)
					Dim line As String = String.Empty
					Do While reader.Peek() <> -1
						line = reader.ReadLine()
						dropdown.AppendFormat(OPTION_TAG_FORMAT, line, line)
					Loop
				End Using
			End If
			Return dropdown.ToString()
		End Function

		Public Shared Function RenderDrodownListFromTextWithEmpty(ByVal textFilePath As String, ByVal emptyValue As String, ByVal emptyText As String) As String
			Dim dropdown As New StringBuilder()
			If Not File.Exists(textFilePath) Then
				Return dropdown.AppendFormat(OPTION_TAG_FORMAT, emptyValue, emptyText).ToString()
			End If
			dropdown.AppendFormat(OPTION_TAG_FORMAT, emptyValue, emptyText)
			'' Read text file to get value and text
			Dim data As New List(Of String)
			'          _Log.Debug("Read line from file: " + textFilePath)
			If File.Exists(textFilePath) Then
				Using reader As New StreamReader(textFilePath)
					Dim line As String = String.Empty
					Do While reader.Peek() <> -1
						line = reader.ReadLine()
						dropdown.AppendFormat(OPTION_TAG_FORMAT, line, line)
					Loop
				End Using
			End If
			Return dropdown.ToString()
		End Function


		Public Shared Function FormatPhoneNumber(ByVal psUnformattedPhone As String) As String
			Return System.Text.RegularExpressions.Regex.Replace(SafePhone(psUnformattedPhone),
				"(\d{3})(\d{3})(\d{4})",
				"$1-$2-$3")
		End Function

		Public Shared Function FormatSSN(ByVal psUnformattedSSN As String) As String
			Return System.Text.RegularExpressions.Regex.Replace(psUnformattedSSN,
				"(\d{3})(\d{2})(\d{4})",
				"$1-$2-$3")
		End Function

		Public Shared Function SafePhone(ByVal psPhone As String) As String
			Dim sPhone As String = System.Text.RegularExpressions.Regex.Replace(psPhone, "[^0-9]", "")
			If sPhone.StartsWith("1") And sPhone.Length > 10 Then
				sPhone = sPhone.Substring(1)
			End If

			Return Left(sPhone, 10)
		End Function

		Public Shared Function SafeDate(ByVal s As Object) As Date
			If IsDate(s) Then
				Return CDate(s)
			Else
				Return Nothing
			End If
		End Function

		Public Shared Function SafeDateTimeOffset(ByVal s As Object) As DateTimeOffset
			If IsDate(s.ToString()) Then
				Return DateTimeOffset.Parse(s.ToString())
			Else
				Return DateTimeOffset.MinValue
			End If
		End Function

		Public Shared Function SafeBoolean(ByVal s As Object) As Boolean
			If s Is GetType(Boolean) Then
				Return CType(s, Boolean)
			End If
			If (s Is Nothing) OrElse (s Is DBNull.Value) OrElse (s Is String.Empty) Then
				Return False
			Else
				Try
					Return CBool(s)
				Catch ex As Exception
					Return False
				End Try
			End If
		End Function

		Public Shared Function SafeString(ByVal s As Object) As String
			' special case to handle Recordset Field objects
			If s Is Nothing OrElse TypeOf s Is DBNull Then
				Return ""
			ElseIf TypeOf s Is Date AndAlso CDate(s) = Date.MinValue Then
				Return ""
			ElseIf TypeOf s Is WebControls.ListItem Then
				Return CType(s, WebControls.ListItem).Value
			ElseIf TypeOf s Is Guid Then
				Return s.ToString()
			Else
				Return Trim(CStr(s)) 'very odd: if u return trim(s.toString()), then for dates, it will append 12:00:00 AM if there is no time :-T
			End If
		End Function

		Public Shared Function SafeStringNoTrim(ByVal s As Object) As String
			If s Is Nothing Then
				Return ""
			End If
			Return s.ToString()
		End Function

		Public Shared Function SafeLong(ByVal s As Object) As Long
			Dim s2 As String
			s2 = SafeString(s)

			If s2 = "" Then
				Return 0
			ElseIf IsNumeric(s2) Then
				SafeLong = CLng(s2)
			Else
				SafeLong = 0
			End If
		End Function

		Public Shared Function SafeInteger(ByVal s As Object) As Integer
			'Dim s2 As String
			's2 = SafeString(s)

			'If s2 = "" Then
			'    Return 0SafeString
			'ElseIf IsNumeric(s2) Then
			'    SafeInteger = CInt(s2)
			'Else
			'    SafeInteger = 0
			'End If
			Dim res As Integer = 0
			If s IsNot Nothing Then
				Integer.TryParse(s.ToString(), res)
			End If
			Return res
		End Function

		Public Shared Function SafeDouble(ByVal s As Object) As Double
			'Dim s2 As String
			's2 = SafeString(s)

			'If IsNumeric(s2) Then
			'    Return CDbl(s2)
			'Else
			'    Return 0
			'End If
			Dim res As Double = 0
			If s IsNot Nothing Then
				Double.TryParse(s.ToString(), res)
			End If
			Return res
		End Function
		Public Shared Function SafeDecimal(ByVal s As Object) As Decimal
			Dim res As Decimal = 0
			If s IsNot Nothing Then
				Decimal.TryParse(s.ToString(), res)
			End If
			Return res
		End Function
		Public Overloads Shared Function SafeStripHtmlString(ByVal s As Object) As String
			Return SafeString(s).StripHTML()
		End Function
		Public Overloads Shared Function SafeEncodeString(ByVal s As Object) As String
			Return AntiXssEncoder.HtmlEncode(SafeString(s), True)
		End Function

		Public Shared Function SafeDoubleFromMoney(ByVal s As String) As Double
			''remove "$" or "," if it exist and then convert to double
			Return SafeDouble(SafeStripHtmlString(s).Replace("$", "").Replace(",", ""))
		End Function

#Region "SafeGuid"
		''' <summary>
		''' Robust routine to guarantee a guid is returned.  If value is blank or non-guid compatible, a guid.empty will be returned.  
		''' This routine most useful when working with datasets.
		''' </summary>
		''' <param name="value"></param>
		Public Shared Function SafeGUID(ByVal value As Object) As Guid
			If IsNothing(value) Then
				Return Guid.Empty
			ElseIf TypeOf value Is DBNull Then
				Return Guid.Empty
			ElseIf TypeOf value Is Guid Then
				Return CType(value, Guid) 'faster than trying to parse out the guid in the else clause
			ElseIf value.ToString() = String.Empty Then
				Return Guid.Empty
			Else
				Try
					Dim tempguid As String = Trim(value.ToString).Replace("-", "").Replace("{", "").Replace("}", "")
					If tempguid.Length = 32 Then
						Return New Guid(tempguid)
					Else
						Return Guid.Empty
					End If
				Catch ex As Exception
					Return Guid.Empty
				End Try
			End If
		End Function
#End Region

#Region "SQLString overloads"
		''' <summary>
		''' only used when fine control is needed  over how a string should be escaped w/ or w/o trimming
		''' avoid using if possiblel
		''' </summary>	
		Public Overloads Shared Function SQLString(ByVal s As String, Optional ByVal pbTrim As Boolean = True) As String
			If s = "" Then
				Return "''"
			End If
			If pbTrim Then
				Return "'" & s.Trim().Replace("'", "''") & "'"
			Else
				Return "'" & s.Replace("'", "''") & "'"
			End If
		End Function

		'Public Overloads Shared Function SQLString(ByVal s As Object) As String
		'	If TypeOf s Is Guid Then
		'		Return SQLGuid(CType(s, Guid)) 'this will avoid our warning traces from safestring
		'	Else
		'		Return "'" & SafeString(s).Replace("'", "''") & "'"
		'	End If

		'End Function
#End Region


		Public Shared Function ValidateEmail(ByVal email As String) As Boolean
			'Dim strRegex As String = "^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}" & _
			'"\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\" & _
			'".)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$"
			Dim strRegex As String = "^(([A-Za-z0-9]+_+)|([A-Za-z0-9]+\-+)|([A-Za-z0-9]+\.+)|([A-Za-z0-9]+\++))*[A-Za-z0-9]+@((\w+\-+)|(\w+\.))*\w{1,63}\.[a-zA-Z]{2,6}$"
			Dim regex As New Regex(strRegex)
			Return regex.IsMatch(email)
		End Function

		' Process the tags for setting the color scheme of the site
		' Since we're using Jquery Mobile, everything is controlled in CSS
		Public Shared Sub ProcessColorScheme(ByVal poCurrentConfig As CWebsiteConfig, ByRef psHeaderDataTheme As String, ByRef psFooterDataTheme As String, ByRef psFooterFontDataTheme As String, ByRef psContentDataTheme As String, ByRef psHeaderDataTheme2 As String, ByRef psBackgroundTheme As String, ByRef psButtonTheme As String, ByRef psButtonFontTheme As String)
			' 
			Dim oNodes As XmlNodeList = poCurrentConfig._webConfigXML.SelectNodes("COLOR_SCHEME")

			' Set the default theme values first
			psHeaderDataTheme = "b"
			psHeaderDataTheme2 = "z" 'intentionally set to not defined theme so it will use default light green color '"s" is dark green from customswatch, r
			psFooterDataTheme = "b"
			psFooterFontDataTheme = "#fff"
			psBackgroundTheme = ""
			psContentDataTheme = "c"
			psButtonTheme = "s"
			psButtonFontTheme = "#ffffff"

			'psReviewDataTheme = reviewThemeDefault
			If (oNodes.Count = 0) Then
				' The Color_Scheme attribute is not defined, so just use the defaults
				Return
			End If

			Dim headerThemeAttribute As String = ""
			Dim headerTheme2Attribute As String = ""
			Dim footerThemeAttribute As String = ""
			Dim footerFontThemeAttribute As String = ""
			Dim backgroundThemeAttribute As String = ""
			Dim buttonThemeAttribute As String = ""
			Dim contentThemeAttribute As String = ""
			Dim buttonFontThemeAttribute As String = ""
			'Dim reviewThemeAttribute As String = ""
			For Each childNode As XmlNode In oNodes
				Try
					If childNode.Attributes.ItemOf("header_theme") IsNot Nothing Then
						headerThemeAttribute = childNode.Attributes("header_theme").InnerXml
					End If
					If childNode.Attributes.ItemOf("footer_theme") IsNot Nothing Then
						footerThemeAttribute = childNode.Attributes("footer_theme").InnerXml
					End If
					If childNode.Attributes.ItemOf("footer_font_theme") IsNot Nothing Then
						footerFontThemeAttribute = childNode.Attributes("footer_font_theme").InnerXml
					End If
					If childNode.Attributes.ItemOf("content_theme") IsNot Nothing Then
						contentThemeAttribute = childNode.Attributes("content_theme").InnerXml
					End If
					If childNode.Attributes.ItemOf("header_theme2") IsNot Nothing Then
						headerTheme2Attribute = childNode.Attributes("header_theme2").InnerXml
					End If
					If childNode.Attributes.ItemOf("bg_theme") IsNot Nothing AndAlso childNode.Attributes("bg_theme").InnerXml.Trim() <> "" Then
						backgroundThemeAttribute = childNode.Attributes("bg_theme").InnerXml
						''Else
						''    backgroundThemeAttribute = footerThemeAttribute 'derived from footer_theme if it is not specified in xml config
					End If
					If childNode.Attributes.ItemOf("button_theme") IsNot Nothing AndAlso childNode.Attributes("button_theme").InnerXml.Trim() <> "" Then
						buttonThemeAttribute = childNode.Attributes("button_theme").InnerXml
					Else
						buttonThemeAttribute = footerThemeAttribute	'derived from footer_theme if it is not specified in xml config
					End If

					If childNode.Attributes.ItemOf("button_font_theme") IsNot Nothing AndAlso childNode.Attributes("button_font_theme").InnerXml.Trim() <> "" Then
						buttonFontThemeAttribute = childNode.Attributes("button_font_theme").InnerXml
					End If
				Catch
					' Something happened, just stick with the defaults
					Return
				End Try
				If (headerTheme2Attribute <> "") Then
					psHeaderDataTheme2 = headerTheme2Attribute
				End If
				If (headerThemeAttribute <> "") Then
					psHeaderDataTheme = headerThemeAttribute
				End If
				If (footerThemeAttribute <> "") Then
					psFooterDataTheme = footerThemeAttribute
				End If
				If (footerFontThemeAttribute <> "") Then
					psFooterFontDataTheme = footerFontThemeAttribute
				End If
				If (contentThemeAttribute <> "") Then
					psContentDataTheme = contentThemeAttribute
				End If
				If (backgroundThemeAttribute <> "") Then
					psBackgroundTheme = backgroundThemeAttribute
				End If
				If (buttonThemeAttribute <> "") Then
					psButtonTheme = buttonThemeAttribute
				End If
				If (buttonFontThemeAttribute <> "") Then
					psButtonFontTheme = buttonFontThemeAttribute
				End If
			Next
		End Sub
		'Public Shared Function getBackgroundTheme(ByVal poCurrentConfig As CWebsiteConfig) As String
		'    Dim oNodes As XmlNodeList = poCurrentConfig._webConfigXML.SelectNodes("COLOR_SCHEME")
		'    Dim bgTheme As String = ""
		'    If oNodes.Count = 0 Then
		'        Return ""
		'    End If
		'    For Each childNode As XmlNode In oNodes
		'        Try
		'            bgTheme = childNode.Attributes("bg_theme").InnerXml
		'        Catch ex As Exception
		'            Return ""
		'        End Try
		'    Next
		'    Return bgTheme
		'End Function
		Public Shared Function hasEnableJoint(ByVal poCurrentConfig As CWebsiteConfig) As String
			Dim isEnableJoint As String = ""
			Dim oNodes As XmlNodeList = poCurrentConfig._webConfigXML.SelectNodes("VISIBLE")
			If oNodes.Count = 0 Then
				Return "" ''return by default
			End If
			For Each childNode As XmlNode In oNodes
				Try
					isEnableJoint = childNode.Attributes("joint_enable").InnerXml
				Catch ex As Exception
					Return ""
				End Try
			Next
			Return isEnableJoint
		End Function
		Public Shared Function isForeignContact(ByVal poCurrentConfig As CWebsiteConfig) As String
			Dim isForeign As String = ""
			Dim oNodes As XmlNodeList = poCurrentConfig._webConfigXML.SelectNodes("VISIBLE")
			If oNodes.Count = 0 Then
				Return "" ''return by default
			End If
			For Each childNode As XmlNode In oNodes
				Try
					isForeign = childNode.Attributes("foreign_contact").InnerXml
				Catch ex As Exception
					Return ""
				End Try
			Next
			Return isForeign
		End Function
		Public Shared Function showQualifiedProduct(ByVal poCurrentConfig As CWebsiteConfig) As String
			Dim sShowQualifiedProduct As String = ""
			Dim oNodes As XmlNodeList = poCurrentConfig._webConfigXML.SelectNodes("VISIBLE")
			If oNodes.Count = 0 Then
				Return ""
			End If
			For Each childNode As XmlNode In oNodes
				Try
					sShowQualifiedProduct = childNode.Attributes("show_qualified_product").InnerXml
				Catch ex As Exception
					Return ""
				End Try
			Next
			Return sShowQualifiedProduct
		End Function
		Public Shared Function getVisibleAttribute(ByVal poCurrentConfig As CWebsiteConfig, ByVal sAttribute As String) As String
			Dim attributeValue As String = ""
			If String.IsNullOrWhiteSpace(sAttribute) Then Return ""
			Dim oNode = poCurrentConfig._webConfigXML.SelectSingleNode(String.Format("VISIBLE/@{0}", sAttribute))
			If oNode IsNot Nothing Then
				attributeValue = SafeString(oNode.Value)
			End If
			Return attributeValue
		End Function
		Public Shared Function GetLPQSSOConfigByDomain(ByVal psDomain As String) As CLPQSSOConfig
			Dim oLPQSSOConfigs As List(Of CLPQSSOConfig) = GetLPQSSOConfigs()
			If oLPQSSOConfigs Is Nothing OrElse oLPQSSOConfigs.Count = 0 Then Return Nothing
			Dim oFoundLPQSSOConfig = oLPQSSOConfigs.SingleOrDefault(Function(x) x.Domain.NullSafeToLower_.Replace("https://", "") = psDomain.NullSafeToLower_.Replace("https://", ""))
			Return oFoundLPQSSOConfig
		End Function
		
		Public Shared Function GetCurrentWebsiteConfig(ByVal psHost As String, ByVal psLenderId As String, ByVal psLenderRef As String, Optional psPreviewCode As String = "") As CWebsiteConfig

			'_log.Debug("GetCurrentWebsiteConfig")

			Dim oManager As New CLenderConfigManager()
			Dim oLender As CLenderConfig
			Dim revisionId As Integer = 0
			'Dim lenderConfigId As Guid
			Dim oNodes As XmlNodeList
			Dim oSelectedNode As XmlNode
			Dim matchedXML As New XmlDocument
			Dim bUseLenderRef As Boolean = True
			Dim hasSqlData As Boolean = False
			'' check LenderRef first
			If String.IsNullOrEmpty(psLenderRef) Then
				If String.IsNullOrEmpty(psLenderId) Then
					_log.Debug("Missing LenderID and LenderRef")
					Throw New ArgumentException("Missing LenderID and LenderRef")

				End If
				bUseLenderRef = False
			End If

			''in preview mode, load draft config
			If Not String.IsNullOrEmpty(psPreviewCode) Then
				oLender = oManager.GetLenderConfigPreview(psPreviewCode)
				If oLender IsNot Nothing AndAlso (oLender.LenderId.ToString() = psLenderId Or oLender.LenderRef = psLenderRef) Then
					Dim configData As String = Common.SafeString(oLender.ConfigData)
					If (configData <> "") Then
						matchedXML.LoadXml(configData)
						hasSqlData = True
					End If
					'lenderConfigId = oLender.ID
				Else
					_log.Debug("Previewcode is invalid")
					Throw New ArgumentException("Previewcode is invalid")
				End If
			Else
				'***retrieve from DB first
				'           If bUseLenderRef Then
				'               oLender = oManager.GetLenderConfigByLenderRef(psLenderRef)
				'           Else
				'oLender = oManager.GetLenderConfigByLenderID(psLenderId)
				'           End If
				oLender = oManager.GetLenderConfig(bUseLenderRef, psLenderId, psLenderRef)
				If oLender IsNot Nothing Then
					Dim configData As String = Common.SafeString(oLender.ConfigData)
					If (configData <> "") Then
						matchedXML.LoadXml(configData)
						'lenderConfigId = oLender.ID
						revisionId = oLender.RevisionID
						hasSqlData = True
					End If
				End If
			End If



			'***Retrieve from xml file second if NOT found in DB - this file is suppose to only exist on local env
			If False = hasSqlData Then
				'' get the config file
				Dim oDoc = GetConfigXmlFromFile()
				If oDoc Is Nothing Then
					'no longer use file, still need this here for local debug use
					_log.Debug("This host (" & psHost & ": " & psLenderRef & psLenderId & ") is not yet configured. - can't find local configuration file")
					Throw New ArgumentException("This host (" & psHost & ": " & psLenderRef & psLenderId & ") is not yet configured.")
				End If


				If bUseLenderRef = False Then
					oNodes = oDoc.SelectNodes("LPQ_MOBILE_WEBSITES/WEBSITE[@lender_id=" & Common.SQLString(psLenderId) & "]")
					oSelectedNode = oDoc.SelectSingleNode("LPQ_MOBILE_WEBSITES/WEBSITE[@lender_id=" & Common.SQLString(psLenderId) & "]")
				Else
					' if the lender_id doesn't exist, then use the lender_ref
					oNodes = oDoc.SelectNodes("LPQ_MOBILE_WEBSITES/WEBSITE[@lender_ref=" & Common.SQLString(psLenderRef) & "]")
					oSelectedNode = oDoc.SelectSingleNode("LPQ_MOBILE_WEBSITES/WEBSITE[@lender_ref=" & Common.SQLString(psLenderRef) & "]")
				End If

				If oNodes Is Nothing Or oNodes.Count = 0 Then
					_log.Debug("This host (" & psHost & ": " & psLenderRef & psLenderId & ") is not yet configured. - can't find configuration")
					Throw New ArgumentException("This host (" & psHost & ": " & psLenderRef & psLenderId & ") is not yet configured.")
				End If

				'matchedXML.LoadXml(oSelectedNode.OuterXml)
			Else
				matchedXML = DecryptAllPasswordInConfigXml(matchedXML)
				oSelectedNode = matchedXML.DocumentElement	'from db
			End If

			' _configXML = matchedXML

			Return New CWebsiteConfig(oSelectedNode)
			'Return New CWebsiteConfig(matchedNode)
		End Function

		Public Shared Function EncryptPassword(pw As String) As String
			Const prefix As String = "Encrypted"

			'twist it to do migrate from Unicode(UTF-16 by default) to UTF8 encoding
			'TODO: remove it when migration compeleted
			If Not String.IsNullOrWhiteSpace(pw) AndAlso pw.StartsWith("Ec^123") Then
				Dim encrypter As New CRijndaelManaged()
				pw = encrypter.Decrypt(pw, "Ec^123")
			End If
			'end twist

			If Not String.IsNullOrWhiteSpace(pw) AndAlso Not pw.StartsWith(prefix) Then
				Dim encrypter As New CRijndaelManaged()
				Return encrypter.EncryptUtf8(pw, prefix)
			End If
			Return pw
		End Function

		Public Shared Function DecryptPassword(encryptedPw As String) As String
			Const prefix As String = "Encrypted"
			'twist it to do migrate from Unicode(UTF-16 by default) to UTF8 encoding
			'TODO: remove it when migration compeleted
			If Not String.IsNullOrWhiteSpace(encryptedPw) AndAlso encryptedPw.StartsWith("Ec^123") Then
				Dim encrypter As New CRijndaelManaged()
				Try
					Return encrypter.Decrypt(encryptedPw, "Ec^123")
				Catch ex As Exception
					_log.Warn("DecryptPassword Error for: " & encryptedPw)
					Return encryptedPw
				End Try
			End If
			'end twist

			If Not String.IsNullOrWhiteSpace(encryptedPw) AndAlso encryptedPw.StartsWith(prefix) Then
				Dim encrypter As New CRijndaelManaged()
				Try
					'_log.Debug("+1")
					Return encrypter.DecryptUtf8(encryptedPw, prefix)
				Catch ex As Exception
					_log.Warn("DecryptPassword Error for: " & encryptedPw)
					Return encryptedPw
				End Try
			End If
			Return encryptedPw
		End Function


		Public Shared Function EncryptAllPasswordInConfigXml(configXml As XmlDocument) As XmlDocument
			Dim passwordNode As XmlNode = configXml.SelectSingleNode("WEBSITE/@password")
			Dim passwordLenderLevel As XmlNode = configXml.SelectSingleNode("WEBSITE/@password_lender_level")
			Dim lqbPassword As XmlNode = configXml.SelectSingleNode("WEBSITE/LQB/@password")

			If passwordNode IsNot Nothing Then
				passwordNode.InnerText = EncryptPassword(passwordNode.InnerText)
			End If
			If passwordLenderLevel IsNot Nothing Then
				passwordLenderLevel.InnerText = EncryptPassword(passwordLenderLevel.InnerText)
			End If
			If lqbPassword IsNot Nothing Then
				lqbPassword.InnerText = EncryptPassword(lqbPassword.InnerText)
			End If
			Return configXml
		End Function

		Public Shared Function EncryptAllPasswordInConfigXml(configStr As String) As XmlDocument
			Dim configXml As New XmlDocument
			If Not String.IsNullOrWhiteSpace(configStr) Then
				configXml.LoadXml(configStr)
				configXml = EncryptAllPasswordInConfigXml(configXml)
			End If
			Return configXml
		End Function

		Public Shared Function DecryptAllPasswordInConfigXml(configXml As XmlDocument) As XmlDocument
			Dim passwordNode As XmlNode = configXml.SelectSingleNode("WEBSITE/@password")
			Dim passwordLenderLevel As XmlNode = configXml.SelectSingleNode("WEBSITE/@password_lender_level")
			Dim lqbPassword As XmlNode = configXml.SelectSingleNode("WEBSITE/LQB/@password")
			'_log.Debug("start counter")
			If passwordNode IsNot Nothing Then
				passwordNode.InnerText = DecryptPassword(passwordNode.InnerText)
			End If
			If passwordLenderLevel IsNot Nothing Then
				passwordLenderLevel.InnerText = DecryptPassword(passwordLenderLevel.InnerText)
			End If
			If lqbPassword IsNot Nothing Then
				lqbPassword.InnerText = DecryptPassword(lqbPassword.InnerText)
			End If
			'_log.Debug("end counter")
			Return configXml
		End Function

		Public Shared Function DecryptAllPasswordInConfigXml(configXml As XmlElement) As XmlElement
			Dim passwordNode As XmlNode = configXml.SelectSingleNode("@password")
			Dim passwordLenderLevel As XmlNode = configXml.SelectSingleNode("@password_lender_level")
			Dim lqbPassword As XmlNode = configXml.SelectSingleNode("LQB/@password")
			'_log.Debug("start counter")
			If passwordNode IsNot Nothing Then
				passwordNode.InnerText = DecryptPassword(passwordNode.InnerText)
			End If
			If passwordLenderLevel IsNot Nothing Then
				passwordLenderLevel.InnerText = DecryptPassword(passwordLenderLevel.InnerText)
			End If
			If lqbPassword IsNot Nothing Then
				lqbPassword.InnerText = DecryptPassword(lqbPassword.InnerText)
			End If
			'_log.Debug("end counter")
			Return configXml
		End Function


		'Public Shared Function DecryptAllPasswordInConfigXml(configStr As String) As XmlDocument
		'    Dim configXml As New XmlDocument
		'    If Not String.IsNullOrWhiteSpace(configStr) Then
		'        configXml.LoadXml(configStr)
		'        configXml = DecryptAllPasswordInConfigXml(configXml)
		'    End If
		'    Return configXml
		'End Function
		Public Shared Function DecryptAllPasswordInConfigStrXml(configStr As String) As String
			Dim configXml As New XmlDocument
			If Not String.IsNullOrWhiteSpace(configStr) Then
				Try
					configXml.LoadXml(configStr)
				Catch ex As Exception
					_log.Warn("DecryptAllPasswordInConfigStrXml Error: " & configStr, ex)
					Return configStr
				End Try
				configXml = DecryptAllPasswordInConfigXml(configXml)
			End If
			Return configXml.OuterXml
		End Function

		'Public Shared Function GetConfigXML() As XmlDocument
		'	''TODO
		'	Dim temp As New XmlDocument
		'	Return temp	'"" '_configXML
		'End Function
		''using poConfig.SubmitLoanUrl instead of GetSubmitLoanURLFromConfig(psLenderId ) function
		''Public Shared Function GetSubmitLoanURLFromConfig(ByVal psLenderId As String) As String
		''    '' get the config file
		''    Dim configFile As String = CConfigFile.getConfig().FileLocation

		''    If String.IsNullOrEmpty(configFile) OrElse Not File.Exists(configFile) Then
		''        _log.Error("Missing Config File", New System.Exception("Missing Config File"))
		''        Throw New FileNotFoundException("Missing Config File")
		''    End If

		''    '' load config file xml
		''    Dim oDoc As New XmlDocument()
		''    oDoc.Load(configFile)

		''    '' get post url
		''    '     Dim sURL As String = oDoc.DocumentElement.SelectSingleNode("WEBSITE[@lender_id='" & psLenderId & "']").getAttribute("loan_submit_url")
		''    Dim sURL As String = DirectCast(oDoc.DocumentElement.SelectSingleNode("WEBSITE[@lender_id='" & psLenderId & "']"), XmlElement).GetAttribute("loan_submit_url")
		''    If sURL = "" Then
		''        _log.Error("Loan Submit URL is not yet configured", New System.Exception("Loan Submit URL is not yet configured"))
		''        Throw New ArgumentException("Loan Submit URL is not yet configured.")
		''    End If

		''    Return sURL
		''End Function

		Public Shared Function WebPost(ByVal poDoc As XmlDocument, ByVal psURL As String) As String
			_log.InfoFormat("Prepare data to POST {0}", CSecureStringFormatter.MaskSensitiveXMLData(poDoc.OuterXml)) 'use MaskSensitiveData for incomplete xml doc
			'_log.info("Preparing to POST data: " + Left(poDoc.OuterXml, 4000))

			Dim oRequest As HttpWebRequest = CType(WebRequest.Create(psURL), HttpWebRequest)
			Dim oBytes As Byte() = System.Text.Encoding.UTF8.GetBytes(poDoc.OuterXml)
			oRequest.Method = "POST"
			oRequest.ContentLength = oBytes.Length
			oRequest.ContentType = "text/xml; encoding='utf-8'"

			Dim oRequestStream As Stream = oRequest.GetRequestStream()
			oRequestStream.Write(oBytes, 0, oBytes.Length)
			oRequestStream.Close()
			oRequestStream.Dispose()

			Dim streamBuilder As New StringBuilder()
			Dim responseXML As String = ""
			Try
				_log.Info("Begin request to: " & psURL)
				'_log.info("Start the request to " + psURL)

				Dim oResponse As HttpWebResponse = CType(oRequest.GetResponse(), HttpWebResponse)
				''status = oResponse.StatusCode = HttpStatusCode.OK
				' _Log.Info("Request successfully!")
				'  _log.info("Request was successful")
				Dim receiveStream As Stream = oResponse.GetResponseStream()
				Dim encode As Encoding = System.Text.Encoding.GetEncoding("utf-8")
				Dim readStream As New StreamReader(receiveStream, encode)
				Dim read(256) As [Char]
				' Reads 256 characters at a time.    
				Dim count As Integer = readStream.Read(read, 0, 256)
				While count > 0
					Dim str As New [String](read, 0, count)
					streamBuilder.Append(str)
					count = readStream.Read(read, 0, 256)
				End While
				' Releases the resources of the Stream.
				readStream.Close()
				' Releases the resources of the response.

				oResponse.Close()

				responseXML = streamBuilder.ToString()

				' _Log.Info("Response status Code: " & oResponse.StatusCode.ToString())
				'_log.info("Response status Code: " + oResponse.StatusCode.ToString())

				_log.Info("Response string: " & responseXML)
				'_log.info("Response string: " + responseXML)

			Catch ex As Exception
				_log.Error("Unable to get response from: " & psURL, ex)
				'_log.Error("Unable to get response from: " + psURL, ex)
			End Try
			Return responseXML
		End Function

		Public Shared Function PrependIconForNonApprovedMessage(message As String, Optional title As String = "") As String
			Dim sb As New StringBuilder()
			sb.AppendLine(HeaderUtils.RenderPageTitle(19, title, False))
			sb.AppendLine("<br/>")
			sb.AppendLine(String.Format("<div>{0}</div>", message))
			Return sb.ToString()
		End Function

		''' <summary>
		''' append a link for redirect to Online User Registration page
		''' </summary>
		''' <param name="psPreapprovedSuccessMessage"></param>
		''' <param name="psUrl"></param>
		''' <param name="psOnlineRegistrationMessage"></param>
		''' <returns></returns>
		''' <remarks></remarks>
		Public Shared Function AppendCreateButtonForApprovedMessage(psPreapprovedSuccessMessage As String, psUrl As String, Optional ByVal psOnlineRegistrationMessage As String = "") As String
			Dim sb As New StringBuilder()
			'sb.AppendLine(String.Format("<div>{0}</div>", message))
			'sb.AppendLine("<br/>")
			sb.AppendLine(psPreapprovedSuccessMessage)
			sb.AppendLine("<br/>")
			sb.AppendLine("<br/>")
			sb.AppendLine("<div class=""div-continue"" style=""margin-top: 10px; text-align: center;"" data-role=""footer"">")
			Dim sLinkMessage As String = "Register for Online Banking"
			If psOnlineRegistrationMessage <> "" Then sLinkMessage = psOnlineRegistrationMessage
			sb.AppendLine(String.Format("<a id=""btnRegisterOnline"" href=""{0}"" type=""button"" target=""newwin"" class=""div-continue-button btn-redirect"">{1}</a> ", psUrl, sLinkMessage))
			sb.AppendLine("</div>")
			'Dim sLinkBtn As String = String.Format("<div id='divOnlineUserRegistration'><a href='{0}' target='newwin' data-mode='self-handle-event' style='cursor: pointer;font-weight :bold;' class='header_theme2'>{1}</a></div>", psUrl, sLinkMessage)
			'sb.AppendLine(sLinkBtn)
			_log.Debug("AppendCreateButtonForApprovedMessage: " & sb.ToString())
			Return sb.ToString()
		End Function

		''' <summary>
		''' create final message(preapproved/submitted message)
		''' </summary>
		''' <param name="poCurrentConfig"></param>
		''' <param name="poRequestXml"></param>
		''' <param name="poResponse">no loannumber(failed) will just return submitted message</param>
		''' <param name="isSuccessSubmitted">if false then just get submitted message</param>
		''' <returns></returns>
		''' <remarks></remarks>
		Public Shared Function WebPostResponse(ByVal poCurrentConfig As CWebsiteConfig, ByVal poRequestXml As XmlDocument, ByVal poResponse As String, Optional isSuccessSubmitted As Boolean = True) As String
			' Before we get the custom response messages, we should set the default messages just in case we can't read the values from the XML config file
			Dim customErrorMessage As String = "There was an error while submitting your application."
			Dim customPreapprovedSuccessMessage As String = "Your application has been preapproved.  We will contact you shortly."
			Dim customSubmittedSuccessMessage As String = "Thank you for your application."
			Dim customDeclinedMessage As String = "Thank you for your application."
			Dim customSubmittedDataList As List(Of String) = New List(Of String)
			Dim customPreapprovedDataList As List(Of String) = New List(Of String)
			Dim customDeclinedDataList As List(Of String) = New List(Of String)
			Dim loanType As String = ""
			Dim LoanXML As XmlDocument
			loanType = getCurrentLoanType(poRequestXml)
			loanType = MatchSubmissionTypeWithConfigType(loanType, poRequestXml)
			LoanXML = getLoanXML(poRequestXml)
			GetCustomResponseMessages(poCurrentConfig, loanType, customErrorMessage, customPreapprovedSuccessMessage, customSubmittedSuccessMessage, customDeclinedMessage, customSubmittedDataList, customPreapprovedDataList, customDeclinedDataList)

			Dim status As String = customErrorMessage
			Dim sLoanNumber As String = getLoanNumber(poResponse) 'identify the type of error in log 
			If sLoanNumber = "" Or sLoanNumber Is Nothing Then	'failed submit process
				Return status
			End If

			If (Not isSuccessSubmitted) Then
				status = buildNonApprovedMessage(customSubmittedSuccessMessage, customSubmittedDataList, LoanXML, poResponse)
				status = PrependIconForNonApprovedMessage(status, "Thank You")
				Return status
			End If

			'xa
			'

			'only for loan module
			Dim isDeclined As Boolean = checkIsDeclined(poResponse)
			Dim isQualified As Boolean = checkIsQualified(poResponse)
			If isDeclined Then
				status = buildNonApprovedMessage(customDeclinedMessage, customDeclinedDataList, LoanXML, poResponse)
				status = PrependIconForNonApprovedMessage(status, "Thank You")
			ElseIf isQualified Then
				status = buildNonApprovedMessage(customPreapprovedSuccessMessage, customPreapprovedDataList, LoanXML, poResponse)
				status = PrependIconForNonApprovedMessage(status, "Congratulations")
			Else
				status = buildNonApprovedMessage(customSubmittedSuccessMessage, customSubmittedDataList, LoanXML, poResponse)
				status = PrependIconForNonApprovedMessage(status, "Thank You")
			End If

			Return status
		End Function


		Public Shared Function webGet(ByVal sURL As String) As String
			Dim req As System.Net.WebRequest = System.Net.WebRequest.Create(sURL)
			req.Method = "GET"

			'read the data back in
			Dim resp As System.Net.HttpWebResponse = CType(req.GetResponse(), System.Net.HttpWebResponse)
			Dim strmReader As New System.IO.StreamReader(resp.GetResponseStream())

			Return strmReader.ReadToEnd()
		End Function

		Private Shared Function getProductSelectData(ByVal poDecisionOutputXML As String, ByVal poConfig As CWebsiteConfig, ByVal pnSelectedIndex As Integer) As XmlDocument
			Dim oDecisionOutput As New XmlDocument
			oDecisionOutput.LoadXml(poDecisionOutputXML)
			Dim validation_id = oDecisionOutput.SelectSingleNode("//DECISION").Attributes("validation_id").InnerXml
			Dim loanID As String = oDecisionOutput.SelectSingleNode("//RESPONSE").Attributes("loan_id").InnerXml
			Dim selectIndex As String = pnSelectedIndex
			Dim oProducts As XmlCDataSection = oDecisionOutput.SelectSingleNode("//DECISION").ChildNodes(0)

			Dim oDoc As New XmlDocument()
			oDoc.LoadXml("<INPUT />")

			Dim oLogin As XmlElement = CType(oDoc.DocumentElement.AppendChild(oDoc.CreateElement("LOGIN")), XmlElement)
			oLogin.SetAttribute("api_user_id", poConfig.APIUser)
			oLogin.SetAttribute("api_password", poConfig.APIPassword)

			Dim oRequest As XmlElement = CType(oDoc.DocumentElement.AppendChild(oDoc.CreateElement("REQUEST")), XmlElement)
			oRequest.SetAttribute("loan_id", loanID)
			oRequest.SetAttribute("validation_id", validation_id)
			oRequest.SetAttribute("underwriting_service_results_index", selectIndex)


			oRequest.AppendChild(oDoc.CreateCDataSection(oProducts.Value))

			Return oDoc
		End Function

		Public Shared Function getSubmitLoanData(ByVal poLoan As CBaseLoanApp, ByVal poConfig As CWebsiteConfig, Optional ByVal pbUseSubmitLoan As Boolean = False) As XmlDocument
			Dim oDoc As New XmlDocument()
			oDoc.LoadXml("<INPUT />")

			Dim oLogin As XmlElement = CType(oDoc.DocumentElement.AppendChild(oDoc.CreateElement("LOGIN")), XmlElement)
			oLogin.SetAttribute("api_user_id", poConfig.APIUser)
			oLogin.SetAttribute("api_password", poConfig.APIPassword)

			Dim oRequest As XmlElement = CType(oDoc.DocumentElement.AppendChild(oDoc.CreateElement("REQUEST")), XmlElement)
			oRequest.SetAttribute("action_type", "NEW")
			''require user to manually select one of the product, only work for decision 2.0, TODO:test with IDA
			Dim IsManualProdcutSelect As Boolean = Common.SafeString(Common.getNodeAttributes(poConfig, "BEHAVIOR", "manual_select_loan_product")) = "Y"
			If (poLoan.LoanType = "CC" Or poLoan.LoanType = "VL") And IsManualProdcutSelect And poConfig.SubmitLoanUrl.Contains("decisionloan/2.0") And Not pbUseSubmitLoan Then
				oRequest.SetAttribute("skip_product_selection", "true")
			End If

			Dim oLoanData As XmlElement = CType(oRequest.AppendChild(oDoc.CreateElement("LOAN_DATA")), XmlElement)
			oLoanData.SetAttribute("loan_type", poLoan.LoanType)
			oLoanData.AppendChild(oDoc.CreateCDataSection(poLoan.GetXml(poConfig).OuterXml()))

			Return oDoc
		End Function

		'get pdf encapsulate in XML ready for submission, only pdf that passed virus scan is included
		Public Shared Function getSubmitPDFData(ByVal poLoan As CBaseLoanApp, ByVal poConfig As CWebsiteConfig, ByVal psLoanNum As String, ByVal appType As String) As XmlDocument

			Dim oDocCleaned As New Dictionary(Of String, String) 'preprocess or format document type
			For Each doc As KeyValuePair(Of String, String) In poLoan.DocBase64Dic
				Dim fileExtension As String = ""
				If doc.Key.LastIndexOf(".") > 0 Then  ''data:application/pdf;base64,
					fileExtension = doc.Key.Substring(doc.Key.LastIndexOf(".") + 1)
				End If
				If fileExtension = "" Then
					_log.Info("Invalid file format: missing file extension")
					Continue For
				End If

				If fileExtension.ToUpper = "PDF" Then
					If String.IsNullOrEmpty(doc.Value) Then Continue For 'fake pdf with no content
					Dim sPDF As String = doc.Value.Replace("data:application/pdf;base64,", "")
					sPDF = sPDF.Replace(" ", "") ''remove white space if it exist
					''make sure input string is base64string 
					If (sPDF.Length Mod 4 = 0) Then
						Dim sPDFByte As Byte() = Convert.FromBase64String(sPDF)
						Dim sPDFString As String = Convert.ToBase64String(sPDFByte)
						oDocCleaned.Add(doc.Key, sPDFString)
					Else
						_log.Info("The input string '" & doc.Key & "' is not base64string")
					End If
				ElseIf fileExtension.ToUpper = "JPEG" Or fileExtension.ToUpper = "PNG" Or fileExtension.ToUpper = "JPG" Or fileExtension.ToUpper = "GIF" Then 'need to convert to pdf
					Dim sImage As String = doc.Value.Replace("data:image/png;base64,", "")
					''need to replace for all case jpeg, png, jpg, and gif
					sImage = sImage.Replace("data:image/jpeg;base64,", "")
					sImage = sImage.Replace("data:image/jpg;base64,", "")
					sImage = sImage.Replace("data:image/gif;base64,", "")
					'Dim sImage = StreamFile2Base64("front_license3.jpg")
					sImage = sImage.Replace(" ", "") ''remove white space if it exist
					''make sure input string is base64string
					If (sImage.Length Mod 4 = 0) Then
						Dim base64PDF = convertBase64Image2Base64PDF(sImage, doc.Key)
						oDocCleaned.Add(doc.Key, base64PDF)
					Else
						_log.Info("The input string of '" & doc.Key & "' is not base64string")
					End If

				Else
					_log.Info("Invalid file extension: " & fileExtension)
				End If
			Next

			'Inject virus for testing
			'Adds anti-virus test string to the output stream.  Anti-viruses will detect as infected.
			'Anti-virus test string: http://eicar.org/
			'Dim virusString As String = "X5O!P%@AP[4\PZX54(P^)7CC)7}$EICAR-STANDARD-ANTIVIRUS-TEST-FILE!$H+H*"
			'oDocCleaned.Add("Virus file", virusString)

			If oDocCleaned.Count = 0 Then
				Return Nothing
			End If

			'virus scan
			Dim oDocScanned As New Dictionary(Of String, String) 'virus scanned
			For Each item As KeyValuePair(Of String, String) In oDocCleaned
				If IsVirus(poConfig, item) Then Continue For
				oDocScanned.Add(item.Key, item.Value)
			Next

			If oDocScanned.Count = 0 Then
				Return Nothing
			End If

			Dim oDoc As New XmlDocument()
			oDoc.LoadXml("<INPUT />")

			Dim oLogin As XmlElement = CType(oDoc.DocumentElement.AppendChild(oDoc.CreateElement("LOGIN")), XmlElement)
			oLogin.SetAttribute("api_user_id", poConfig.APIUser)
			oLogin.SetAttribute("api_password", poConfig.APIPassword)

			Dim oRequest As XmlElement = CType(oDoc.DocumentElement.AppendChild(oDoc.CreateElement("REQUEST")), XmlElement)
			oRequest.SetAttribute("app_num", psLoanNum)
			oRequest.SetAttribute("app_type", appType)

			Dim oDocuments As XmlElement = CType(oDoc.DocumentElement.AppendChild(oDoc.CreateElement("DOCUMENTS")), XmlElement)
			For Each item As KeyValuePair(Of String, String) In oDocScanned
				''get DocCode and DocGroup user input
				' AP-1843 - Only upload a document if it is in the loan's UploadDocumentInfoList,
				'   to prevent cross-upload of docs for combo apps.
				' AP-3022 - Fix issue where documents may never be uploaded. Ensure that a document is
				'   is uploaded at least once, in case the UploadDocumentInfoList is empty.
				Dim docInfosByName As List(Of uploadDocumentInfo) = poLoan.UploadDocumentInfoList.Where(Function(docInfo) docInfo.docFileName.Equals(item.Key)).ToList()
				Dim docInfosByNameAndLoanType As List(Of uploadDocumentInfo) = docInfosByName.Where(Function(docInfo) String.IsNullOrEmpty(docInfo.docLoanType) OrElse docInfo.docLoanType = poLoan.LoanType).ToList()

				' This overall function is called twice on Combo. Once for the loan, and once for XA.
				' If on the loan, this document was only meant for XA because it is in docInfosByName but not
				' in docInfosByNameAndLoanType, then it will be skipped. However, if this document is in 
				' neither list (due to some coding error before this function), then it will be uploaded for 
				' the loan and also for the XA.
				If Not docInfosByNameAndLoanType.Any() AndAlso Not docInfosByName.Any() Then
					_log.WarnFormat("The document with the title '{0}' did not receive any matching doc titles. This will be uploaded regardless.", item.Key)
					docInfosByNameAndLoanType.Add(New uploadDocumentInfo With {
						.docFileName = item.Key,
						.docGroup = "",
						.docCode = "",
						.docTitle = ""
					})
				End If

				If Not docInfosByNameAndLoanType.Any() Then
					_log.InfoFormat("The document with the title '{0}' will not be uploaded to the {1} app #{2} because the doc titles were setup in combo mode to only upload to the {3} application.", item.Key, poLoan.LoanType, psLoanNum, docInfosByName.First().docLoanType)
				Else

					For Each oUploadDocInfo As uploadDocumentInfo In docInfosByNameAndLoanType
						If item.Key = oUploadDocInfo.docFileName Then
							Dim docGroup = oUploadDocInfo.docGroup
							Dim docCode = oUploadDocInfo.docCode
							Dim docTitle = oUploadDocInfo.docTitle

							Dim oDocument As XmlElement = CType(oDocuments.AppendChild(oDoc.CreateElement("DOCUMENT")), XmlElement)
							If String.IsNullOrEmpty(docTitle) Then
								docTitle = item.Key
							End If
							oDocument.SetAttribute("title", docTitle)
							oDocument.SetAttribute("DocID", System.Guid.NewGuid().ToString)
							If Not String.IsNullOrEmpty(docCode) Then
								oDocument.SetAttribute("DocCode", docCode)
							End If
							If Not String.IsNullOrEmpty(docGroup) Then
								oDocument.SetAttribute("DocGroup", docGroup)
							End If
							Dim sPdf = item.Value
							oDocument.InnerText = sPdf
							Exit For
						End If
					Next

				End If
			Next
			Return oDoc
		End Function

		Private Shared Function IsVirus(ByVal poConfig As CWebsiteConfig, ByVal poItem As KeyValuePair(Of String, String)) As Boolean

			Dim sUrl As String = poConfig.BaseSubmitLoanUrl & "/UploadChecker/check.aspx?filename=" & poItem.Key
			Try
				Dim oRequest As HttpWebRequest = CType(WebRequest.Create(sUrl), HttpWebRequest)
				Dim oBytes As Byte() = System.Text.Encoding.UTF8.GetBytes(poItem.Value)
				oRequest.Method = "POST"
				oRequest.Timeout = 15000
				'oRequest.ContentLength = oBytes.Length
				'oRequest.ContentType = "text/xml; encoding='utf-8'"

				Dim oRequestStream As Stream = oRequest.GetRequestStream()
				oRequestStream.Write(oBytes, 0, oBytes.Length)
				oRequestStream.Close()
				oRequestStream.Dispose()

				Dim streamBuilder As New StringBuilder()
				Dim sResponse As String = ""

				_log.Info("Begin request to: " & sUrl)

				Dim oResponse As HttpWebResponse = CType(oRequest.GetResponse(), HttpWebResponse)

				Dim receiveStream As Stream = oResponse.GetResponseStream()
				Dim encode As Encoding = System.Text.Encoding.GetEncoding("utf-8")
				Dim readStream As New StreamReader(receiveStream, encode)
				Dim read(256) As [Char]
				' Reads 256 characters at a time.    
				Dim count As Integer = readStream.Read(read, 0, 256)
				While count > 0
					Dim str As New [String](read, 0, count)
					streamBuilder.Append(str)
					count = readStream.Read(read, 0, 256)
				End While
				' Releases the resources of the Stream.
				readStream.Close()
				' Releases the resources of the response.

				oResponse.Close()

				sResponse = streamBuilder.ToString()

				Select Case sResponse
					Case "No Virus"
						Return False
					Case "Virus Detected"
						_log.Info(sResponse)
						Return True
					Case Else
						'_log.Warn(sResponse)
						_log.InfoFormat("Response from Uploadchecker: {0} ; FileSize: {1}", sResponse, oBytes.Length)
						Return False
				End Select

			Catch ex As Exception
				_log.Error("Unable to get response from: " & sUrl, ex)
			End Try
			Return False
		End Function

		Public Shared Function SubmitRestApiGet(ByVal poConfig As CWebsiteConfig, ByVal psURL As String) As String
			Dim sResult As String = ""
			Dim oRequest As HttpWebRequest = CType(WebRequest.Create(psURL), HttpWebRequest)
			oRequest.Method = "GET"
			Dim oBytes As Byte() = System.Text.Encoding.ASCII.GetBytes(poConfig.APIUser + ":" + poConfig.APIPassword)
			Dim sAuthStr As String = System.Convert.ToBase64String(oBytes)
			oRequest.Headers.Add("Authorization: Basic " + sAuthStr)

			Try
				_log.Info("Begin REST API request to: " & psURL)
				Dim oResponse As HttpWebResponse = CType(oRequest.GetResponse(), HttpWebResponse)
				If oResponse.StatusCode = HttpStatusCode.OK Then
					Using respStream As Stream = oResponse.GetResponseStream
						Dim reader As StreamReader = New StreamReader(respStream, Encoding.UTF8)
						sResult = reader.ReadToEnd
						_log.InfoFormat("REST API response from {0}: {1}", psURL, sResult)
					End Using
				Else
					_log.WarnFormat("Status Code: {0}, Status Description: {1}", oResponse.StatusCode, oResponse.StatusDescription)
				End If

			Catch ex As Exception '
				If ex.Message.Contains("500") Or ex.Message.Contains("404") Then
					_log.Debug("SubmitRestApiGet Error(name may has special character): " & ex.Message, ex)
				Else
					_log.Warn("SubmitRestApiGet Error: " & ex.Message, ex)
				End If

			End Try

			Return sResult
		End Function

		Public Shared Function SubmitRestApiGet(ByVal poAPILogin As String, ByVal poAPIPassword As String, ByVal psURL As String) As String
			Dim sResult As String = ""
			Dim oRequest As HttpWebRequest = CType(WebRequest.Create(psURL), HttpWebRequest)
			oRequest.Method = "GET"
			Dim oBytes As Byte() = System.Text.Encoding.ASCII.GetBytes(poAPILogin + ":" + poAPIPassword)
			Dim sAuthStr As String = System.Convert.ToBase64String(oBytes)
			oRequest.Headers.Add("Authorization: Basic " + sAuthStr)

			Try
				_log.Info("Begin REST API request to: " & psURL)
				Dim oResponse As HttpWebResponse = CType(oRequest.GetResponse(), HttpWebResponse)
				If oResponse.StatusCode = HttpStatusCode.OK Then
					Using respStream As Stream = oResponse.GetResponseStream
						Dim reader As StreamReader = New StreamReader(respStream, Encoding.UTF8)
						sResult = reader.ReadToEnd
						_log.InfoFormat("REST API response from {0}: {1}", psURL, sResult)
					End Using
				Else
					_log.WarnFormat("Status Code: {0}, Status Description: {1}", oResponse.StatusCode, oResponse.StatusDescription)
				End If

			Catch ex As Exception '
				If ex.Message.Contains("500") Or ex.Message.Contains("404") Then
					_log.Debug("SubmitRestApiGet Error(name may has special character): " & ex.Message, ex)
				Else
					_log.Warn("SubmitRestApiGet Error: " & ex.Message, ex)
				End If
			End Try

			Return sResult
		End Function

		Public Shared Function SubmitRestApiPost(ByVal poConfig As CWebsiteConfig, ByVal psURl As String, ByVal sData As String) As String
			Dim sResult As String = ""
			Dim oRequest As HttpWebRequest = CType(WebRequest.Create(psURl), HttpWebRequest)

			oRequest.Method = "POST"
			Dim oBytes As Byte() = System.Text.Encoding.ASCII.GetBytes(poConfig.APIUser + ":" + poConfig.APIPassword)
			Dim sAuthStr As String = System.Convert.ToBase64String(oBytes)
			oRequest.Headers.Add("Authorization: Basic " + sAuthStr)
			oRequest.ContentType = "text/xml"
			Try
				_log.Info("POSTING DATA: " & CSecureStringFormatter.MaskSensitiveData(sData))
				_log.Info("Begin REST API request: " & psURl)
				oRequest.ContentLength = sData.Length

				Dim streamwriter As StreamWriter = New StreamWriter(oRequest.GetRequestStream())
				streamwriter.Write(sData)
				streamwriter.Close()
				Dim oResponse As HttpWebResponse = CType(oRequest.GetResponse(), HttpWebResponse)
				If oResponse.StatusCode = HttpStatusCode.OK Then
					Using respStream As Stream = oResponse.GetResponseStream
						Dim reader As StreamReader = New StreamReader(respStream, Encoding.UTF8)
						sResult = reader.ReadToEnd
						_log.InfoFormat("REST API response from {0}: {1}", psURl, sResult)
					End Using
				Else
					_log.WarnFormat("Status Code: {0}, Status Description: {1}", oResponse.StatusCode, oResponse.StatusDescription)
				End If
				oResponse.Close()
			Catch ex As Exception
				_log.Error("SubmitRestApiPost Error: " & ex.Message, ex)
			End Try

			Return sResult
		End Function
		'true=found bad member
		Private Shared Function WebPostBadMember(ByVal poConfig As CWebsiteConfig, ByVal psURL As String) As Boolean
			Dim oRequest As HttpWebRequest = CType(WebRequest.Create(psURL), HttpWebRequest)
			oRequest.Method = "GET"
			Dim oBytes As Byte() = System.Text.Encoding.ASCII.GetBytes(poConfig.APIUser + ":" + poConfig.APIPassword)
			Dim sAuthStr As String = System.Convert.ToBase64String(oBytes)
			oRequest.Headers.Add("Authorization: Basic " + sAuthStr)

            Try
                ''the psURL is containing ssn. when display the url in the log the ssn should be masked.             
                _log.Info("Begin request to: " & psURL.Replace(psURL.Substring(psURL.LastIndexOf("/") + 1), "xxxxxxxxx"))
                Dim oResponse As HttpWebResponse = CType(oRequest.GetResponse(), HttpWebResponse)

                If oResponse.StatusCode = "200" Then
                    _log.Info("Found Bad Member!")
                    Return True
                End If

                'the 2 if statements below will not be executed, but is here for backup
                If oResponse.StatusCode = "404" Then
                    _log.Info("NOT Found Bad Member!")
                    Return False
                End If
                If oResponse.StatusCode <> "200" Then
                    _log.Info("NOT Found Bad Member!")
                    _log.Info("Request NOT successful!  Status code: " & oResponse.StatusCode)
                    Return False
                End If

            Catch ex As Exception 'any status other then 200 will trigger this
                _log.Info("Not Found in Bad Member List! Exception Loop")
				'_log.Error("Unable to get response from: " + psURL, ex)
			End Try

			Return False
		End Function

		'true=found
		'false=not found/setup or error
		'test case:  000-00-0006=bad on kersnchool beta
		Private Shared Function GetBadMember(ByVal poLoan As CBaseLoanApp, ByVal poConfig As CWebsiteConfig) As Boolean
			If poConfig.LenderCode = "" Then Return False
            If poLoan.isBusinessLoan Then
                If poLoan.BLApplicants IsNot Nothing AndAlso poLoan.BLApplicants.Count > 0 Then
                    For Each app As CBLApplicantInfo In poLoan.BLApplicants
                        Dim sUrl As String = poConfig.BaseSubmitLoanUrl & "/resources.ashx/lenders/" & poConfig.LenderCode & "/badmembers/" & app.SSN
                        If WebPostBadMember(poConfig, sUrl) Then
                            Return True
                        End If
                    Next
                End If
            Else
                If poLoan.Applicants IsNot Nothing AndAlso poLoan.Applicants.Count > 0 Then
                    For Each app As CApplicant In poLoan.Applicants
                        Dim sUrl As String = poConfig.BaseSubmitLoanUrl & "/resources.ashx/lenders/" & poConfig.LenderCode & "/badmembers/" & app.SSN
                        If WebPostBadMember(poConfig, sUrl) Then
                            Return True
                        End If
                    Next
                End If
            End If
            Return False
		End Function


        'for xa, true = bad member
        Public Shared Function GetBadMember(ByVal poSSNList As List(Of String), ByVal poConfig As CWebsiteConfig) As Boolean
            If poConfig.LenderCode = "" Then Return False
            For Each app As String In poSSNList
                Dim sUrlBadMember = poConfig.BaseSubmitLoanUrl & "/resources.ashx/lenders/" & poConfig.LenderCode & "/badmembers/" & app
                If WebPostBadMember(poConfig, sUrlBadMember) Then
                    Return True
                End If
            Next
            Return False
        End Function
        

		Public Shared Function BuildLoanSavingObject(ByVal poLoanApp As CBaseLoanApp, ByVal poConfig As CWebsiteConfig, isSubmitted As Boolean, sResponse As String, sRequestXML As XmlDocument) As SmLoan
			Dim loan As New SmLoan
			loan.LoanSavingID = poLoanApp.LoanSavingID
			loan.LenderID = Guid.Parse(poConfig.LenderId)
			loan.LenderRef = poConfig.LenderRef
			loan.VendorID = poLoanApp.VendorID
			loan.UserID = poLoanApp.UserID
			If poLoanApp.isBusinessLoan Then
				If poLoanApp.BLApplicants IsNot Nothing AndAlso poLoanApp.BLApplicants.Count > 0 Then
					loan.ApplicantName = poLoanApp.BLApplicants(0).FirstName & " " & poLoanApp.BLApplicants(0).LastName
				End If
			Else
				If poLoanApp.Applicants IsNot Nothing AndAlso poLoanApp.Applicants.Count > 0 Then
					loan.ApplicantName = poLoanApp.Applicants(0).FirstName & " " & poLoanApp.Applicants(0).LastName
				End If
			End If
			loan.CreatedDate = Now
			Dim requestNode As XmlNode = sRequestXML.SelectSingleNode("INPUT/REQUEST")
			If requestNode IsNot Nothing Then
				loan.RequestParam = requestNode.InnerText
			End If
			If isSubmitted Then
				loan.SubmitDate = Now
				Dim doc As New XmlDocument
				doc.LoadXml(sResponse)
				Dim cNode As XmlNode = doc.SelectSingleNode("/OUTPUT/RESPONSE")
				If cNode IsNot Nothing Then
					loan.LoanNumber = cNode.SelectSingleNode("@loan_number").Value
					loan.LoanID = cNode.SelectSingleNode("@loan_id").Value
					Dim decisionNode As XmlNode = cNode.SelectSingleNode("DECISION")
					If decisionNode IsNot Nothing Then
						loan.Status = decisionNode.SelectSingleNode("@status").Value
						loan.StatusDate = Now
					End If
				End If
				loan.XmlResponse = sResponse
				loan.FundStatus = "NOT FUND"
			Else
				loan.SubmitDate = Nothing
				loan.Status = "INCOMPLETE"
				loan.FundStatus = "NOT FUND"
			End If
			loan.RequestParamJson = poLoanApp.LoanRawData

            If poLoanApp.LoanType = "PL" Then
                loan.LoanType = SmSettings.LenderVendorType.PL
                If poLoanApp.IsComboMode Then
                    loan.LoanType = SmSettings.LenderVendorType.PL_COMBO
                End If
            ElseIf poLoanApp.LoanType = "VL" Then
                loan.LoanType = SmSettings.LenderVendorType.VL
                If poLoanApp.IsComboMode Then
                    loan.LoanType = SmSettings.LenderVendorType.VL_COMBO
                End If
            ElseIf poLoanApp.LoanType = "CC" Then
                loan.LoanType = SmSettings.LenderVendorType.CC
                If poLoanApp.IsComboMode Then
                    loan.LoanType = SmSettings.LenderVendorType.CC_COMBO
                End If
            ElseIf poLoanApp.LoanType = "HE" Then
                loan.LoanType = SmSettings.LenderVendorType.HE
                If poLoanApp.IsComboMode Then
                    loan.LoanType = SmSettings.LenderVendorType.HE_COMBO
                End If
            ElseIf poLoanApp.LoanType = "XA" Then
				loan.LoanType = SmSettings.LenderVendorType.XA
			ElseIf poLoanApp.LoanType = "BL" Then
				loan.LoanType = SmSettings.LenderVendorType.BL
			Else
				loan.LoanType = SmSettings.LenderVendorType.CC
				If poLoanApp.IsComboMode Then
					loan.LoanType = SmSettings.LenderVendorType.CC_COMBO
				End If
			End If
			Return loan
		End Function

		'TODO
		'1) move WebPostResponse to outside
		Public Shared Function SubmitLoan(ByVal poLoanApp As CBaseLoanApp, ByVal poConfig As CWebsiteConfig, ByVal poCancelSubmit As String, ByRef poResponseRaw As String, ByVal pbIsSubmitLoan As Boolean, Optional ByRef sSubmitMessage As String = "", Optional ByRef loanRequestXMLStr As String = "") As String
			'  _log.info("Common: SubmitLoan *begin*")
			Dim sUrl As String = poConfig.SubmitLoanUrl	 '' GetSubmitLoanURLFromConfig(poConfig.LenderId)

			'check for bad member enable attribute and return result
			Dim isBadMember As Boolean = False
			If poConfig.LenderCodeBadMember <> "" Then
				isBadMember = GetBadMember(poLoanApp, poConfig)
			End If

			'change url base on case
			''isSubmitLoan =true -> go to submit loan
			Dim bUseSubmitLoan As Boolean = poCancelSubmit = "Y" Or isBadMember = True Or pbIsSubmitLoan = True
			If bUseSubmitLoan Then	   'bad member then skip credit check or underwriting
				sUrl = poConfig.BaseSubmitLoanUrl & "/SubmitLoan/SubmitLoan.aspx"
			End If


			Dim sRequestXML As XmlDocument = getSubmitLoanData(poLoanApp, poConfig, bUseSubmitLoan)
			Dim sResponse As String

			''===Modified submit url for non-LPQ, also include loanNumber if this is a refferal loan from xsell
			Dim sResolvedSubmitLoanUrl = sUrl
			If poConfig.SubmitLoanUrl_TPV <> "" Then
				sResolvedSubmitLoanUrl = String.Format(poConfig.SubmitLoanUrl_TPV & "?OrgCode2={0}&LenderCode2={1}", poConfig.OrgCode2, poConfig.LenderCode2)

			End If
			If poConfig.SubmitLoanUrl_TPV <> "" AndAlso poLoanApp.xSellCommentMessage <> "" AndAlso poLoanApp.xSellCommentMessage.IndexOf("#") > 0 Then
				'sample meessage CROSS SELL FROM CC#123123123
				Dim nFoundIndex = poLoanApp.xSellCommentMessage.IndexOf("#")
				Dim sLoanNumber As String = poLoanApp.xSellCommentMessage.Substring(nFoundIndex + 1)
				sResolvedSubmitLoanUrl = sResolvedSubmitLoanUrl & "&LoanNumber=" & sLoanNumber
			End If
			sResponse = WebPost(sRequestXML, sResolvedSubmitLoanUrl)

			Dim isSubmitted As Boolean = IsSubmittedSuccessful(sResponse)

            'indirect loan, save loan even if it is not successfully submitted
            'If poLoanApp.VendorID <> "" AndAlso (poLoanApp.LoanType = "VL" Or poLoanApp.LoanType = "PL" Or poLoanApp.LoanType = "CC") Then
            If poLoanApp.VendorID <> "" Then
                Dim loan = BuildLoanSavingObject(poLoanApp, poConfig, isSubmitted, sResponse, sRequestXML)
                Dim smBl As New SmBL
                smBl.SaveLoan(loan, poLoanApp.IPAddress)
            End If

            If Not isSubmitted Then
				sSubmitMessage = WebPostResponse(poConfig, sRequestXML, sResponse, isSubmitted)
				Return sSubmitMessage
			End If

			'if the loan is successfully submitted, we need to keek track some basic information for billing purpose
			If String.IsNullOrWhiteSpace(poLoanApp.VendorID) Then
				Dim xmlDocBogus As New XmlDocument	''this is required so it doesn't break the smBl.SaveLoan
				Dim loanNodeName As String
				Select Case poLoanApp.LoanType
					Case "CC"
						loanNodeName = "CREDIT_CARD_LOAN"
					Case "HE"
						loanNodeName = "HOME_EQUITY_LOAN"
					Case "PL"
						loanNodeName = "PERSONAL_LOAN"
					Case "BL"
						loanNodeName = "BUSINESS_LOAN"
					Case "XA"
						loanNodeName = "XA_LOAN"
					Case Else
						loanNodeName = "VEHICLE_LOAN"
				End Select

				xmlDocBogus.LoadXml("<INPUT><REQUEST><LOAN_DATA><![CDATA[<" & loanNodeName & "/>]]></LOAN_DATA></REQUEST></INPUT>")
				Dim loan = BuildLoanSavingObject(poLoanApp, poConfig, isSubmitted, sResponse, xmlDocBogus)
				loan.VendorID = System.Guid.Empty.ToString	'db doesn't allow null
				loan.UserID = System.Guid.Empty
				Dim smBl As New SmBL
				smBl.SaveLoan(loan, poLoanApp.IPAddress)
			End If

			loanRequestXMLStr = sRequestXML.InnerXml
			poResponseRaw = sResponse
			''submit supporting document
			If poLoanApp.DocBase64Dic.Count > 0 Then
				Dim sloanNumber = getLoanNumber(sResponse)
				Dim sSubmitPDF_Url = sUrl.Substring(0, sUrl.IndexOf("services")) + "Services/ipad/iPadDocumentSubmit.aspx"
				Dim sAppType As String = "LOAN"
				If poLoanApp.LoanType = "XA" Then sAppType = "DEPOSIT"
				Dim submiData = getSubmitPDFData(poLoanApp, poConfig, sloanNumber, sAppType)
				If submiData IsNot Nothing Then
					WebPost(submiData, sSubmitPDF_Url)	 '"appteyp="DEPOSIT" for xa
				End If
			End If



			Return WebPostResponse(poConfig, sRequestXML, sResponse)
		End Function


		Public Shared Function SubmitProduct(ByVal poDecisionOutputXML As String, ByVal poConfig As CWebsiteConfig, ByVal pnSelectedIndex As String) As String
			'  _log.info("Common: SubmitLoan *begin*")
			Dim sUrl As String = poConfig.SubmitLoanUrl
			sUrl = poConfig.BaseSubmitLoanUrl & "/ProductSelect/2.0/ProductSelect.aspx"
			Dim sRequestXML As XmlDocument = getProductSelectData(poDecisionOutputXML, poConfig, pnSelectedIndex)
			Dim sResponse As String = WebPost(sRequestXML, sUrl)
			Return sResponse
		End Function


		Public Shared Function RenderLogo(ByVal poCurrentConfig As CWebsiteConfig, ByVal psLogoUrl As String) As String
			If String.IsNullOrEmpty(psLogoUrl) Then Return String.Empty
			Dim oNodes As XmlNodeList = poCurrentConfig._webConfigXML.SelectNodes("LAYOUT")

			' Set the default layout first
			Dim logoPosition As String = "c" ' center

			For Each childNode As XmlNode In oNodes
				Try
					logoPosition = childNode.Attributes("logo").InnerXml
				Catch
					' Something happened, just stick with the defaults
				End Try
			Next
			Dim sAlign As String = "style='text-align:center'"
			Select Case logoPosition.ToUpper
				Case "C"
					sAlign = "style='text-align:center'"
				Case "L"
					sAlign = "style='text-align:left'"
				Case "R"
					sAlign = "style='text-align:right'"
				Case Else
					sAlign = "style='text-align:left'"
			End Select
			Return String.Format("<div class='CompanyLogo' {1} ><img src='{0}' alt='CU Logo' /></div>", psLogoUrl, sAlign)
		End Function

		Public Shared Function GetBuildVersion() As String
			Return ConfigurationManager.AppSettings("BuildVersion")
		End Function

		Public Shared Function GetCrossDomainServicesUrl() As String
			Return ConfigurationManager.AppSettings("CrossDomainServicesUrl")
		End Function

		Public Shared Function SafeDateXml(ByVal psDate As String) As String
			If IsDate(psDate) Then
				Return CDate(psDate).ToString("yyyy-MM-dd")
			End If
			_log.Debug("Not a valid date: " & psDate)
			Return ""
		End Function

		Public Shared Function SafeDateTimeXml(ByVal psDate As String) As String
			If IsDate(psDate) Then
				Return CDate(psDate).ToString("yyyy-MM-ddTHH:mm:ss")
			End If

			Return ""
		End Function

		Public Shared Function ParseDisclosures(ByVal poWebsiteNode As XmlElement, ByVal psLoanType As String, Optional psAvailability As String = "") As List(Of String)
			Dim disclosures As New List(Of String)
			If poWebsiteNode Is Nothing OrElse String.IsNullOrEmpty(psLoanType) Then Return disclosures
			Dim oDisclosureNodes As XmlNodeList = Nothing
			If psLoanType = "XA_LOAN" Then
				If psAvailability = "1s" Then
					oDisclosureNodes = poWebsiteNode.SelectNodes("XA_LOAN/SPECIAL_ACCOUNT_DISCLOSURES/DISCLOSURE")
				ElseIf psAvailability = "1b" Then
					oDisclosureNodes = poWebsiteNode.SelectNodes("XA_LOAN/BUSINESS_ACCOUNT_DISCLOSURES/DISCLOSURE")
				End If
				If oDisclosureNodes Is Nothing Then
					oDisclosureNodes = poWebsiteNode.SelectNodes("XA_LOAN/DISCLOSURES/DISCLOSURE")
				End If
			Else
				oDisclosureNodes = poWebsiteNode.SelectNodes(psLoanType & "/DISCLOSURES/DISCLOSURE")
			End If


			For Each node As XmlElement In oDisclosureNodes
				disclosures.Add(node.InnerText)
			Next
			Return disclosures
		End Function
		Public Shared Function ParseDisclosuresLoc(ByVal poWebsiteNode As XmlElement, ByVal psLoanType As String) As List(Of String)
			Dim disclosures As New List(Of String)
			If poWebsiteNode Is Nothing OrElse String.IsNullOrEmpty(psLoanType) Then Return disclosures
			Dim xpath As String = psLoanType & "/DISCLOSURES_LOC/DISCLOSURE"
			Dim oDisclosureNodes As XmlNodeList = poWebsiteNode.SelectNodes(xpath)
			For Each node As XmlElement In oDisclosureNodes
				disclosures.Add(node.InnerText)
			Next
			Return disclosures
		End Function
		Public Shared Function getSecondaryDisclosures(ByVal poCurrentConfig As CWebsiteConfig, Optional availability As String = "") As List(Of String)
			Dim oDisclosures As New List(Of String)
			Dim oDisclosureNodes As XmlNodeList = Nothing ' = poCurrentConfig._webConfigXML.SelectNodes("XA_LOAN/DISCLOSURES_SECONDARY/DISCLOSURE")
			If availability = "2s" Then
				oDisclosureNodes = poCurrentConfig._webConfigXML.SelectNodes("XA_LOAN/SPECIAL_ACCOUNT_DISCLOSURES_SECONDARY/DISCLOSURE")
			ElseIf availability = "2b" Then
				oDisclosureNodes = poCurrentConfig._webConfigXML.SelectNodes("XA_LOAN/BUSINESS_ACCOUNT_DISCLOSURES_SECONDARY/DISCLOSURE")
			End If
			If oDisclosureNodes Is Nothing Then
				oDisclosureNodes = poCurrentConfig._webConfigXML.SelectNodes("XA_LOAN/DISCLOSURES_SECONDARY/DISCLOSURE")
			End If
			If oDisclosureNodes.Count = 0 Then
				Return oDisclosures
			End If
			For Each node As XmlElement In oDisclosureNodes
				oDisclosures.Add(node.InnerText)
			Next
			Return oDisclosures
		End Function

		Public Shared Function ParseBranches(ByVal poWebsiteNode As XmlElement, ByVal psLoanType As String) As Dictionary(Of String, String)
			Dim branches As New Dictionary(Of String, String)
			If poWebsiteNode Is Nothing OrElse String.IsNullOrEmpty(psLoanType) Then Return branches
			Dim xpath As String = psLoanType & "/BRANCHES/BRANCH"
			Dim oDisclosureNodes As XmlNodeList = poWebsiteNode.SelectNodes(xpath)
			For Each node As XmlElement In oDisclosureNodes
				If Not branches.ContainsKey(node.GetAttribute("branch_id")) Then
					branches.Add(node.GetAttribute("branch_id"), node.GetAttribute("branch_name"))
				End If
			Next
			Return branches
		End Function

		Public Shared Function GetItemKeyValueCollectionFromEnumConfig(ByVal poWebsiteNode As XmlElement, ByVal psEnumNodeName As String) As Dictionary(Of String, String)
			Dim dic As New Dictionary(Of String, String)
			If poWebsiteNode Is Nothing OrElse String.IsNullOrEmpty(psEnumNodeName) Then Return dic
			Dim xpath As String = "ENUMS/" & psEnumNodeName & "/ITEM"
			Dim oEnumsNodes As XmlNodeList = poWebsiteNode.SelectNodes(xpath)
			For Each node As XmlElement In oEnumsNodes
				Dim key As String = node.GetAttribute("value")
				If Not dic.ContainsKey(key) Then
					dic.Add(key, node.Attributes("text").Value)
				End If
			Next
			Return dic
		End Function

		Public Shared Function GetItemKeyValueCatCollectionFromEnumConfig(ByVal poWebsiteNode As XmlElement, ByVal psEnumNodeName As String) As Dictionary(Of String, CTextValueCatItem)
			Dim dic As New Dictionary(Of String, CTextValueCatItem)
			If poWebsiteNode Is Nothing OrElse String.IsNullOrEmpty(psEnumNodeName) Then Return dic
			Dim xpath As String = "ENUMS/" & psEnumNodeName & "/ITEM"
			Dim oEnumsNodes As XmlNodeList = poWebsiteNode.SelectNodes(xpath)
			For Each node As XmlElement In oEnumsNodes
				Dim key As String = node.GetAttribute("value")
				If Not dic.ContainsKey(key) Then
					Dim sCategory As String = If(node.Attributes("category") IsNot Nothing, node.Attributes("category").Value, "")
					dic.Add(key, New CTextValueCatItem() With {.Value = key, .Text = node.Attributes("text").Value, .Category = sCategory})
				End If
			Next
			Return dic
		End Function

		Public Shared Function GetItemKeyValueCollectionFromClinicEnumConfig(ByVal poWebsiteNode As XmlElement, ByVal psEnumNodeName As String) As Dictionary(Of String, String)
			Dim dic As New Dictionary(Of String, String)
			If poWebsiteNode Is Nothing OrElse String.IsNullOrEmpty(psEnumNodeName) Then Return dic
			Dim xpath As String = psEnumNodeName & "/ITEM"
			Dim oEnumsNodes As XmlNodeList = poWebsiteNode.SelectNodes(xpath)
			For Each node As XmlElement In oEnumsNodes
				Dim key As String = node.GetAttribute("value")
				If Not dic.ContainsKey(key) Then
					dic.Add(key, node.Attributes("text").Value)
				End If
			Next
			Return dic
		End Function

		Public Shared Function MapHELOANPurposeToValue(ByVal poConfig As CWebsiteConfig, ByVal psMortgageLoanType As String) As String
			Dim oLoanPurpose As Dictionary(Of String, String) = poConfig.GetEnumItems("MapHELOANPurposeToValue")
			For Each key As String In oLoanPurpose.Keys
				If oLoanPurpose(key) = psMortgageLoanType Then
					Return key
				End If
			Next
			Return psMortgageLoanType
		End Function

		Public Shared Function checkIsDeclined(ByVal xmlResponse As String) As Boolean
			Try
				Dim doc As New XmlDocument()
				doc.LoadXml(xmlResponse)
				Dim decision As XmlElement = doc.SelectSingleNode("/OUTPUT/RESPONSE/DECISION")
				If decision IsNot Nothing Then
					Return decision.GetAttribute("status") = "INSTANT_DECLINED"
				End If
			Catch ex As Exception
				_log.Error("Unable to checkIsDeclinced", ex)
			End Try
			Return False
		End Function

		Public Shared Function checkIsQualified(ByVal xmlResponse As String) As Boolean
			Try
				Dim doc As New XmlDocument()
				doc.LoadXml(xmlResponse)
				Dim decision As XmlElement = doc.SelectSingleNode("/OUTPUT/RESPONSE/DECISION")
				If decision IsNot Nothing Then
					Return decision.GetAttribute("status") = "QUALIFIED" Or decision.GetAttribute("status") = "PREQUALIFIED"
				End If
			Catch ex As Exception
				_log.Error("Unable to checkIsQualified", ex)
			End Try
			Return False
		End Function
		Public Shared Function checkIsPreQualified(ByVal xmlResponse As String) As Boolean
			Try
				Dim doc As New XmlDocument()
				doc.LoadXml(xmlResponse)
				Dim decision As XmlElement = doc.SelectSingleNode("/OUTPUT/RESPONSE/DECISION")
				If decision IsNot Nothing Then
					Return decision.GetAttribute("status") = "PREQUALIFIED"
				End If
			Catch ex As Exception
				_log.Error("Unable to checkIsPreQualified", ex)
			End Try
			Return False
		End Function

		Public Shared Function getLoanNumber(ByVal pXMLResponse As String) As String
			Try
				Dim returnXmlDoc As New XmlDocument
				returnXmlDoc.LoadXml(pXMLResponse)

				Dim responseNode As XmlElement = returnXmlDoc.SelectSingleNode("OUTPUT/RESPONSE")

				If responseNode IsNot Nothing Then
					'_log.Info("Loan submit successful. Loan number: " & responseNode.GetAttribute("loan_number") & "    " & Now.ToString)
					Return responseNode.GetAttribute("loan_number")
				Else ' Error NODES
					Dim errorNode As XmlElement = returnXmlDoc.SelectSingleNode("OUTPUT/ERROR")
					If errorNode IsNot Nothing Then
						Select Case errorNode.GetAttribute("type")
							'Case "INTERNAL_ERROR"
							'    _Log.Warn("Loan Submitted with errors: " & errorNode.InnerText & "   " & Now.ToString)
							'    Return Nothing
							Case "INTERNAL_ERROR" 'fail realtime credit check
								_log.Warn("Loan Submitted with errors: " & errorNode.InnerText & "   " & Now.ToString)
								Return errorNode.GetAttribute("loan_number")
							Case "VALIDATION_FAILED"
								_log.Warn("Loan Submit Failed with Schema Validation Problems: " & errorNode.InnerText & "   " & Now.ToString)
								Return errorNode.GetAttribute("loan_number")
							Case Else
								_log.Error("Loan Submit with total failure: " & errorNode.InnerText & "   " & Now.ToString)
								Return Nothing
						End Select
					End If
				End If
				Return Nothing
			Catch ex As Exception
				_log.Error("Parsing Error: " & ex.ToString())
				Return Nothing
			End Try
		End Function
		Private Shared Function getLoanID(ByVal pXMLResponse As String) As String
			Try
				Dim returnXmlDoc As New XmlDocument
				returnXmlDoc.LoadXml(pXMLResponse)

				Dim responseNode As XmlElement = returnXmlDoc.SelectSingleNode("OUTPUT/RESPONSE")

				If responseNode IsNot Nothing Then
					_log.Info("loan_id: " & responseNode.GetAttribute("loan_id"))
					Return responseNode.GetAttribute("loan_id")
				Else ' Error NODES

				End If
				Return Nothing
			Catch ex As Exception
				_log.Error("Parsing Error: " & ex.ToString())
				Return Nothing
			End Try
		End Function

		Public Shared Sub GetCustomResponseMessages(ByVal poCurrentConfig As CWebsiteConfig, ByVal LoanType As String, ByRef customErrorStr As String, ByRef customPreapprovedStr As String, ByRef customSubmittedStr As String, ByRef customDeclinedStr As String, ByRef customSubmittedDataList As List(Of String), ByRef customPreapprovedDataList As List(Of String), ByRef customDeclinedDataList As List(Of String), Optional loanTypeQueryString As String = "")
			' Read the XML config data and extract the custom error message and success messages.
			' Since the custom success messages are defined per loan type, we need to know the type of loan that is being used
			If poCurrentConfig Is Nothing OrElse String.IsNullOrEmpty(LoanType) Then Return

			' =====================
			' Get the Error Message
			' =====================
			Dim oErrorMsgNode As XmlNode = poCurrentConfig._webConfigXML.SelectSingleNode("CUSTOM_ERROR/@submit_error")

			' There is no custom error message, so we can use a default error message
			customErrorStr = "We apologize, but unfortunately there was an error when submitting the application. Please try again later."
			If oErrorMsgNode IsNot Nothing AndAlso Not String.IsNullOrWhiteSpace(oErrorMsgNode.Value) Then
				customErrorStr = oErrorMsgNode.Value
			End If
			customErrorStr += "<div id='MLerrorMessage'></div>"

			' ===================================
			' Get the Preapproved Success Message
			' ===================================

			Dim oPreapprovedMsgNode = Nothing
			If LoanType = "XA_LOAN" Then
				If loanTypeQueryString = "1s" Then
					oPreapprovedMsgNode = poCurrentConfig._webConfigXML.SelectSingleNode("XA_LOAN/SPECIAL_ACCOUNT_SUCCESS_MESSAGES/PREAPPROVED_MESSAGE[string(@message)]")
				ElseIf loanTypeQueryString = "2s" Then
					oPreapprovedMsgNode = poCurrentConfig._webConfigXML.SelectSingleNode("XA_LOAN/SPECIAL_ACCOUNT_SUCCESS_MESSAGES/APPROVED_SECONDARY_MESSAGE[string(@message)]")
				ElseIf loanTypeQueryString = "1b" Then
					oPreapprovedMsgNode = poCurrentConfig._webConfigXML.SelectSingleNode("XA_LOAN/BUSINESS_ACCOUNT_SUCCESS_MESSAGES/PREAPPROVED_MESSAGE[string(@message)]")
				ElseIf loanTypeQueryString = "2b" Then
					oPreapprovedMsgNode = poCurrentConfig._webConfigXML.SelectSingleNode("XA_LOAN/BUSINESS_ACCOUNT_SUCCESS_MESSAGES/APPROVED_SECONDARY_MESSAGE[string(@message)]")
				ElseIf loanTypeQueryString = "2" Then
					oPreapprovedMsgNode = poCurrentConfig._webConfigXML.SelectSingleNode("XA_LOAN/SUCCESS_MESSAGES/APPROVED_SECONDARY_MESSAGE[string(@message)]")
				End If
				If oPreapprovedMsgNode Is Nothing Then
					oPreapprovedMsgNode = poCurrentConfig._webConfigXML.SelectSingleNode("XA_LOAN/SUCCESS_MESSAGES/PREAPPROVED_MESSAGE[string(@message)]")
				End If
			Else
				oPreapprovedMsgNode = poCurrentConfig._webConfigXML.SelectSingleNode(LoanType & "/SUCCESS_MESSAGES/PREAPPROVED_MESSAGE[string(@message)]")
			End If

			' There are no custom success messages, so we can use the default ones
			customPreapprovedStr = "Your application has been preapproved.  We will contact you shortly."

			If oPreapprovedMsgNode IsNot Nothing Then
				customPreapprovedDataList = New List(Of String)
				customPreapprovedStr = oPreapprovedMsgNode.Attributes("message").Value.ToString()
				If oPreapprovedMsgNode.Attributes.ItemOf("num_data") IsNot Nothing Then
					Dim numData As Integer = 0
					Dim hasNumData = Integer.TryParse(oPreapprovedMsgNode.Attributes("num_data").Value.ToString(), numData)
					If hasNumData AndAlso numData > 0 Then
						For i As Integer = 0 To numData - 1
							customPreapprovedDataList.Add(oPreapprovedMsgNode.Attributes("message_data" + i.ToString()).Value.ToString())
						Next
					End If
				End If
			End If

			' ===================================
			' Get the Submitted Success Message
			' ===================================
			Dim oSubmittedMsgNode = Nothing
			If LoanType = "XA_LOAN" Then
				If loanTypeQueryString = "1s" Or loanTypeQueryString = "2s" Then
					oSubmittedMsgNode = poCurrentConfig._webConfigXML.SelectSingleNode("XA_LOAN/SPECIAL_ACCOUNT_SUCCESS_MESSAGES/SUBMITTED_MESSAGE[string(@message)]")
				ElseIf loanTypeQueryString = "1b" Or loanTypeQueryString = "2b" Then
					oSubmittedMsgNode = poCurrentConfig._webConfigXML.SelectSingleNode("XA_LOAN/BUSINESS_ACCOUNT_SUCCESS_MESSAGES/SUBMITTED_MESSAGE[string(@message)]")
				End If
				If oSubmittedMsgNode Is Nothing Then
					oSubmittedMsgNode = poCurrentConfig._webConfigXML.SelectSingleNode("XA_LOAN/SUCCESS_MESSAGES/SUBMITTED_MESSAGE[string(@message)]")
				End If
			Else
				oSubmittedMsgNode = poCurrentConfig._webConfigXML.SelectSingleNode(LoanType & "/SUCCESS_MESSAGES/SUBMITTED_MESSAGE[string(@message)]")
			End If

			' There are no custom success messages, so we can use the default ones
			customSubmittedStr = "Thank you for your application, it has been submitted."
			If oSubmittedMsgNode IsNot Nothing Then
				customSubmittedDataList = New List(Of String)
				customSubmittedStr = oSubmittedMsgNode.Attributes("message").Value.ToString()
				If oSubmittedMsgNode.Attributes.ItemOf("num_data") IsNot Nothing Then
					Dim numData As Integer = 0
					Dim hasNumData = Integer.TryParse(oSubmittedMsgNode.Attributes("num_data").Value.ToString(), numData)
					If hasNumData AndAlso numData > 0 Then
						For i As Integer = 0 To numData - 1
							customSubmittedDataList.Add(oSubmittedMsgNode.Attributes("message_data" + i.ToString()).Value.ToString())
						Next
					End If
				End If
			End If


			' ===================================
			' Get the Declined Message if available or use submitted message
			' ===================================
			Dim oDeclinedMsgNode = poCurrentConfig._webConfigXML.SelectSingleNode(LoanType & "/SUCCESS_MESSAGES/DECLINED_MESSAGE[string(@message)]")
			' There are no custom decline messages, so we can use the submitted ones
			customDeclinedStr = customSubmittedStr
			customDeclinedDataList = customSubmittedDataList

			If oDeclinedMsgNode IsNot Nothing Then
				customDeclinedDataList = New List(Of String)
				customDeclinedStr = oDeclinedMsgNode.Attributes("message").Value
				If oDeclinedMsgNode.Attributes.ItemOf("num_data") IsNot Nothing Then
					Dim numData = 0
					Dim hasNumData = Integer.TryParse(oDeclinedMsgNode.Attributes("num_data").Value, numData)
					If hasNumData AndAlso numData > 0 Then
						For i As Integer = 0 To numData - 1
							customDeclinedDataList.Add(oDeclinedMsgNode.Attributes("message_data" + i.ToString()).Value)
						Next
					End If
				End If
			End If
		End Sub

		Public Shared Function getCurrentLoanType(ByVal currentLoanXML As XmlDocument) As String
			' Get the loan type from the loan XML
			Dim loanDoc As XmlDocument = New XmlDocument()
			loanDoc.LoadXml(currentLoanXML.InnerText)

			Return loanDoc.DocumentElement.Name

		End Function

		Public Shared Function getLoanXML(ByVal currentLoanXML As XmlDocument) As XmlDocument
			' Get the loan type from the loan XML
			Dim loanDoc As XmlDocument = New XmlDocument()
			loanDoc.LoadXml(currentLoanXML.InnerText)

			Return loanDoc

		End Function

		'this one has the membernumber parameter
		Public Shared Function buildNonApprovedMessage(ByVal customMessage As String, ByVal dataRequestList As List(Of String), ByVal loanXML As XmlDocument, ByVal oResponse As String, ByVal psMemberNumber As String) As String
			' Create a new list of strings that will hold the data that was requested in the dataRequestList
			Dim dataList As List(Of String) = New List(Of String)
			For Each dataRequest As String In dataRequestList
				dataList.Add(getDataValueFromRequest(dataRequest, loanXML, oResponse, psMemberNumber))
			Next
			' Now put the customMessage together with the data values
			Dim resultMessage = String.Format(customMessage, dataList.ToArray())
			Return resultMessage
		End Function

		Public Shared Function buildNonApprovedMessage(ByVal customMessage As String, ByVal dataRequestList As List(Of String), ByVal loanXML As XmlDocument, ByVal oResponse As String) As String
			' Create a new list of strings that will hold the data that was requested in the dataRequestList
			Dim dataList As List(Of String) = New List(Of String)
			For Each dataRequest As String In dataRequestList
				dataList.Add(getDataValueFromRequest(dataRequest, loanXML, oResponse))
			Next

			Dim resultMessage As String = ""

			'this error occur so frequent so it help to display the message to suppot
			Try
				resultMessage = String.Format(customMessage, dataList.ToArray()) ' Now put the customMessage together with the data values
			Catch ex As Exception
				resultMessage = "Message setup error." & customMessage
				_log.Error("Message setup error in xml config.  Index is not correct    " & ex.Message)
			End Try
			Return resultMessage
		End Function

		Private Shared Function getDataValueFromRequest(ByVal dataRequest As String, ByVal loanXML As XmlDocument, ByVal poResponse As String, ByVal psMemberNumber As String) As String
			Select Case dataRequest
				Case "MEMBER_NUMBER"
					Return psMemberNumber
				Case "MEMBER_LOAN_NUMBER"
					If psMemberNumber <> "" Then Return psMemberNumber
			End Select
			Return getDataValueFromRequest(dataRequest, loanXML, poResponse)
		End Function

		Private Shared Function getDataValueFromRequest(ByVal dataRequest As String, ByVal loanXML As XmlDocument, ByVal poResponse As String) As String
			' Get the value of the data that was requested
			Select Case dataRequest
				Case "LOAN_NUMBER"
					Dim sLoanNumber As String = getLoanNumber(poResponse)
					Return sLoanNumber
				Case "FIRST_NAME"
					Dim firstNameNode As XmlNode
					Dim name As String = ""
					Try

						firstNameNode = loanXML.SelectSingleNode("//@first_name")

						If firstNameNode.InnerXml.Length > 1 Then
							name = firstNameNode.InnerXml.Substring(0, 1).ToUpper & firstNameNode.InnerXml.Substring(1, firstNameNode.InnerXml.Length - 1).ToLower
						End If

					Catch ex As Exception
						_log.Warn("Unable to get first_name attribute   " & ex.Message)
						Return ""
					End Try

					Return name
				Case "FULL_NAME" '' get first name and last name
					Dim firstNameNode, lastNameNode As XmlNode
					Dim fullName As String = ""
					Try
						firstNameNode = loanXML.SelectSingleNode("//@first_name")
						lastNameNode = loanXML.SelectSingleNode("//@last_name")
						If firstNameNode.InnerXml.Length > 1 Then
							fullName = firstNameNode.InnerXml.Substring(0, 1).ToUpper & firstNameNode.InnerXml.Substring(1, firstNameNode.InnerXml.Length - 1).ToLower
						End If
						If lastNameNode.InnerXml.Length > 1 Then
							fullName += " " + lastNameNode.InnerXml.Substring(0, 1).ToUpper & lastNameNode.InnerXml.Substring(1, lastNameNode.InnerXml.Length - 1).ToLower
						End If
					Catch ex As Exception
						_log.Warn("Unable to get first_name and last_name attributes   " & ex.Message)
						Return ""
					End Try
					Return fullName
				Case "CARD_NAME"
					Dim cardNameNode As XmlNode
					Dim name As String = ""
					Try
						cardNameNode = loanXML.SelectSingleNode("//@requested_card_name")
						name = cardNameNode.InnerXml
					Catch ex As Exception
						_log.Warn("Unable to get requested_card_name attribute   " & ex.Message)
						Return ""
					End Try

					Return name


					'Dim child_nodes As XmlNodeList = loanXML.GetElementsByTagName("APPLICANT")

					'If child_nodes.Count < 1 Then
					'    Return ""
					'End If

					'Dim numNames As Integer = child_nodes.Count
					'Dim valueStr As String = ""

					'For Each child As XmlNode In child_nodes
					'    ' Grab the names of the products
					'    Return child.Attributes("first_name").InnerXml
					'Next
			End Select

			Return ""
		End Function

		Public Shared Function MatchSubmissionTypeWithConfigType(ByVal loanType As String, ByVal loanXML As XmlDocument) As String
			' For some reason, the loan type credit card loan appears as
			' CREDIT_CARD_LOAN in the config XML but as
			' CREDITCARD_LOAN in the submission XML (input)

			Select Case loanType
				Case "XPRESS_LOAN"
					Return "XA_LOAN"
				Case "CREDITCARD_LOAN"
					Return "CREDIT_CARD_LOAN"
				Case "MORTGAGE_LOAN"
					'' There are two types of mortgage loans
					'' Home Equity Loan and Home Equity Line Of Credit
					'Dim loanPurposeNode As XmlNode
					'Dim purposeName As String
					'Dim innerXML As XmlDocument = New XmlDocument()
					'innerXML.LoadXml(loanXML.InnerText)
					'Try
					'    loanPurposeNode = innerXML.SelectSingleNode("//@purpose_name")
					'    purposeName = loanPurposeNode.InnerXml
					'    If (purposeName = "HOME EQUITY LINE OF CREDIT") Then
					'        Return "HOME_EQUITY_LOC"
					'    End If
					'Catch ex As Exception
					'    Return "HOME_EQUITY_LOAN"
					'End Try

					Return "HOME_EQUITY_LOAN"
				Case Else
					Return loanType
			End Select
		End Function

		''' <summary>
		''' 
		''' </summary>
		''' <param name="loanType"></param>
		''' <returns>equivalance LPQ clf loan node</returns>
		''' <remarks></remarks>
		Public Shared Function GetCLFLoanTypeFromShort(ByVal loanType As String) As String
			' For some reason, the loan type credit card loan appears as
			' CREDIT_CARD_LOAN in the config XML but as
			' CREDITCARD_LOAN in the submission XML (input)

			Select Case Common.SafeString(loanType).ToUpper
				Case "XA_LOAN"
					Return "XPRESS_LOAN"
				Case "XA"
					Return "XPRESS_LOAN"
				Case "CC"
					Return "CREDITCARD_LOAN"
				Case "VL"
					Return "VEHICLE_LOAN"
				Case "PL"
					Return "PERSONAL_LOAN"
				Case "HE"
					'Note: In the schema, a Home Equity (HE) application is a subcategory of the Mortgage (ML) application. 
					'To create a Home Equity application, please set is_home_equity="Y" similar to Sample 1
					Return "MORTGAGE_LOAN"
				Case "HELOC"
					Return "MORTGAGE_LOAN"
					'
				Case Else
					_log.Warn("unknow loan type")
					Return loanType
			End Select
		End Function

		''' <summary>
		''' 
		''' </summary>
		''' <param name="loanType"></param>
		''' <returns>XML config loan node name</returns>
		''' <remarks></remarks>
		Public Shared Function GetConfigLoanTypeFromShort(ByVal loanType As String) As String
			' For some reason, the loan type credit card loan appears as
			' CREDIT_CARD_LOAN in the config XML but as
			' CREDITCARD_LOAN in the submission XML (input)

			Select Case Common.SafeString(loanType).ToUpper
				Case "XA"
					Return "XA_LOAN"
				Case "CC"
					Return "CREDIT_CARD_LOAN"
				Case "VL"
					Return "VEHICLE_LOAN"
				Case "PL"
					Return "PERSONAL_LOAN"
				Case "HE"
					Return "HOME_EQUITY_LOAN"
				Case "HELOC"
					Return "HOME_EQUITY_LOAN"
				Case "BL"
					Return "BUSINESS_LOAN"
				Case Else
					_log.Warn("unknow loan type")
					Return loanType
			End Select
		End Function

		''--get Upload document message in config in config
		Public Shared Function getUploadDocumentMessage(ByVal poCurrentConfig As CWebsiteConfig) As String
			Dim upload_docs_message As String = ""
			Dim oUpLoadNode As XmlNode = poCurrentConfig._webConfigXML.SelectSingleNode("UPLOAD_DOC_MESSAGE")
			If oUpLoadNode IsNot Nothing Then
				upload_docs_message = oUpLoadNode.InnerText
			End If
			Return upload_docs_message
		End Function
        Public Shared Function getBLUploadDocumentMessage(ByVal poCurrentConfig As CWebsiteConfig) As String
            Dim upload_docs_message As String = ""
            Dim oUpLoadNode As XmlNode = poCurrentConfig._webConfigXML.SelectSingleNode("BUSINESS_LOAN/UPLOAD_DOC_MESSAGE")
            If oUpLoadNode IsNot Nothing Then
                upload_docs_message = oUpLoadNode.InnerText
            End If
            Return upload_docs_message
        End Function
        Public Shared Function getFundingDisclosure(ByVal poCurrentConfig As CWebsiteConfig, Optional ByVal availability As String = "1") As String
            Dim fundingMsg As String = ""
            Dim fundingXPath As String
            Dim isSecondary As Boolean = availability.Contains("2")
            Dim type As AccountType = AccountType.Personal
            If availability.Contains("s") Then
                type = AccountType.Special
            End If
            If availability.Contains("b") Then
                type = AccountType.Business
            End If
            Select Case Type
                Case AccountType.Special
                    fundingXPath = If(isSecondary, "XA_LOAN/SPECIAL_ACCOUNT_FUNDING_DISCLOSURE_SECONDARY", "XA_LOAN/SPECIAL_ACCOUNT_FUNDING_DISCLOSURE")
                Case AccountType.Business
                    fundingXPath = If(isSecondary, "XA_LOAN/BUSINESS_ACCOUNT_FUNDING_DISCLOSURE_SECONDARY", "XA_LOAN/BUSINESS_ACCOUNT_FUNDING_DISCLOSURE")
                Case Else
                    fundingXPath = If(isSecondary, "XA_LOAN/FUNDING_DISCLOSURE_SECONDARY", "XA_LOAN/FUNDING_DISCLOSURE")
            End Select
            Dim oFundingNode As XmlNode = poCurrentConfig._webConfigXML.SelectSingleNode(fundingXPath)
            If oFundingNode IsNot Nothing Then
                fundingMsg = oFundingNode.InnerText
            End If
            Return fundingMsg
        End Function
        Public Shared Function getUploadDocumentEnable(ByVal poCurrentConfig As CWebsiteConfig) As Boolean
            Dim oUpLoadDocEnableNode As XmlNode = poCurrentConfig._webConfigXML.SelectSingleNode("VISIBLE/@document_upload_enable")
            If oUpLoadDocEnableNode IsNot Nothing Then
                Return Not oUpLoadDocEnableNode.Value.ToUpper().Equals("N")
            End If
            Return True
        End Function

        Public Shared Function getBLUploadDocumentEnable(ByVal poCurrentConfig As CWebsiteConfig) As Boolean
            Dim oUpLoadDocEnableNode As XmlNode = poCurrentConfig._webConfigXML.SelectSingleNode("BUSINESS_LOAN/VISIBLE/@document_upload_enable")
            If oUpLoadDocEnableNode IsNot Nothing Then
                Return oUpLoadDocEnableNode.Value.ToUpper().Equals("Y")
            End If
            Return True
        End Function

        Public Shared Function getXAUploadDocumentEnable(ByVal poCurrentConfig As CWebsiteConfig) As Boolean
            Dim oUpLoadDocEnableNode As XmlNode = poCurrentConfig._webConfigXML.SelectSingleNode("XA_LOAN/VISIBLE/@document_upload_enable")
            If oUpLoadDocEnableNode IsNot Nothing Then
                Return Not oUpLoadDocEnableNode.Value.ToUpper().Equals("N")
            End If
            Return True
        End Function
        Public Shared Function getXASpecialUploadDocumentEnable(ByVal poCurrentConfig As CWebsiteConfig) As Boolean
            Dim oUpLoadDocEnableNode As XmlNode = poCurrentConfig._webConfigXML.SelectSingleNode("XA_LOAN/VISIBLE/@document_special_upload_enable")
            If oUpLoadDocEnableNode IsNot Nothing Then
                Return Not oUpLoadDocEnableNode.Value.ToUpper().Equals("N")
            End If
            Return False
        End Function
        Public Shared Function getXABusinessUploadDocumentEnable(ByVal poCurrentConfig As CWebsiteConfig) As Boolean
            Dim oUpLoadDocEnableNode As XmlNode = poCurrentConfig._webConfigXML.SelectSingleNode("XA_LOAN/VISIBLE/@document_business_upload_enable")
            If oUpLoadDocEnableNode IsNot Nothing Then
                Return Not oUpLoadDocEnableNode.Value.ToUpper().Equals("N")
            End If
            Return False
        End Function
        Public Shared Function getXAUploadDocumentMessage(ByVal poCurrentConfig As CWebsiteConfig) As String
            Dim upload_docs_message As String = ""
            Dim oUpLoadNode As XmlNode = poCurrentConfig._webConfigXML.SelectSingleNode("XA_LOAN/UPLOAD_DOC_MESSAGE")
            If oUpLoadNode IsNot Nothing Then
                upload_docs_message = oUpLoadNode.InnerText
            End If
            Return upload_docs_message
        End Function
        Public Shared Function getXASpecialUploadDocumentMessage(ByVal poCurrentConfig As CWebsiteConfig) As String
            Dim upload_docs_message As String = ""
            Dim oUpLoadNode As XmlNode = poCurrentConfig._webConfigXML.SelectSingleNode("XA_LOAN/SPECIAL_UPLOAD_DOC_MESSAGE")
            If oUpLoadNode IsNot Nothing Then
                upload_docs_message = oUpLoadNode.InnerText
            End If
            Return upload_docs_message
        End Function
        Public Shared Function getXABusinessUploadDocumentMessage(ByVal poCurrentConfig As CWebsiteConfig) As String
            Dim upload_docs_message As String = ""
            Dim oUpLoadNode As XmlNode = poCurrentConfig._webConfigXML.SelectSingleNode("XA_LOAN/BUSINESS_UPLOAD_DOC_MESSAGE")
            If oUpLoadNode IsNot Nothing Then
                upload_docs_message = oUpLoadNode.InnerText
            End If
            Return upload_docs_message
        End Function
        ''--get  document scan message in config in config
        Public Shared Function getDocumentScanMessage(ByVal poCurrentConfig As CWebsiteConfig) As String
            Dim doc_scan_message As String = ""
            Dim oScanNode As XmlNode = poCurrentConfig._webConfigXML.SelectSingleNode("DOC_SCAN_MESSAGE")
            If oScanNode IsNot Nothing Then
                doc_scan_message = oScanNode.InnerText
            End If
            Return doc_scan_message
        End Function
        ''--get submit cancel message in config in config
        Public Shared Function getSubmitCancelMessage(ByVal poCurrentConfig As CWebsiteConfig) As String
            Dim submit_cancel_message As String = ""
            Dim oNode As XmlNode = poCurrentConfig._webConfigXML.SelectSingleNode("SUBMIT_CANCEL_MESSAGE")
            If oNode IsNot Nothing Then
                submit_cancel_message = oNode.InnerText
            End If
            Return submit_cancel_message
        End Function
        ''-get innerText message
        Public Shared Function getInnerTextMessage(ByVal poCurrentConfig As CWebsiteConfig, ByVal sNodeName As String) As String
            Dim innerTextMessage As String = ""
            Dim oNode As XmlNode = poCurrentConfig._webConfigXML.SelectSingleNode(sNodeName)
            If oNode IsNot Nothing Then
                innerTextMessage = oNode.InnerText
            End If
            Return innerTextMessage
        End Function
        Public Shared Function GetInnerTextValue(parentNode As XmlNode, xPath As String, Optional defaultValue As String = Nothing) As String
            Dim result As String = defaultValue
            Dim oNode = parentNode.SelectSingleNode(xPath)
            If oNode IsNot Nothing Then
                result = oNode.InnerText
            End If
            Return result
        End Function
        Public Shared Function GetAndParseInnerTextValue(parentNode As XmlNode, xPath As String, callback As Func(Of String, Boolean), Optional defaultValue As Boolean = False) As Boolean
            Dim result As Boolean = defaultValue
            Dim oNode = parentNode.SelectSingleNode(xPath)
            If oNode IsNot Nothing Then
                result = callback(oNode.InnerText)
            End If
            Return result
        End Function
        Public Shared Function GetAndParseInnerTextValue(parentNode As XmlNode, xPath As String, callback As Func(Of String, Integer), Optional defaultValue As Integer = 0) As Integer
            Dim result As Integer = defaultValue
            Dim oNode = parentNode.SelectSingleNode(xPath)
            If oNode IsNot Nothing Then
                result = callback(oNode.InnerText)
            End If
            Return result
        End Function
        Public Shared Function getNodeAttributes(ByVal poCurrentConfig As CWebsiteConfig, ByVal sNodeName As String, ByVal sAttributeName As String) As String
            '' if xpath is not exist return empty string
            If poCurrentConfig Is Nothing OrElse poCurrentConfig._webConfigXML.SelectSingleNode(sNodeName) Is Nothing Then
                Return ""
            End If
            Dim sXmlElement As XmlElement = poCurrentConfig._webConfigXML.SelectSingleNode(sNodeName)
            If Not sXmlElement.HasAttributes Then '' if no attribute return empty string
                Return ""
            End If
            Return SafeString(sXmlElement.GetAttribute(sAttributeName))
        End Function
        Public Shared Function getItemsFromConfig(ByVal poCurrentConfig As CWebsiteConfig, ByVal strNodeName As String, ByVal strAttrText As String, ByVal strAttrValue As String) As Dictionary(Of String, String)
            Dim dicItems As New Dictionary(Of String, String)
            Dim oCurrentNodes As XmlNodeList = poCurrentConfig._webConfigXML.SelectNodes(strNodeName)
            Dim strCurrentAttrText As String = ""
            Dim strCurrentAttrValue As String = ""
            If oCurrentNodes.Count = 0 Then
                Return dicItems
            End If
            For Each oCurrentNode As XmlNode In oCurrentNodes
                If (oCurrentNode.Attributes(strAttrText) IsNot Nothing And oCurrentNode.Attributes(strAttrValue) IsNot Nothing) Then
                    strCurrentAttrText = SafeString(oCurrentNode.Attributes(strAttrText).InnerXml)
                    strCurrentAttrValue = SafeString(oCurrentNode.Attributes(strAttrValue).InnerXml)
                    If Not dicItems.ContainsKey(strCurrentAttrText) Then
                        dicItems.Add(strCurrentAttrText, strCurrentAttrValue)
                    End If
                End If
            Next
            Return dicItems
        End Function




        Public Shared Function getButtonLabelByKey(key As String, buttonLabelList As Dictionary(Of String, String), Optional defaultValue As String = "") As String
            If buttonLabelList Is Nothing Or buttonLabelList.Any() = False Or buttonLabelList.ContainsKey(key) = False Then
                Return defaultValue
            End If
            Return buttonLabelList(key)
        End Function
        Public Shared Function getItemsFromConfig(ByVal poCurrentConfig As CWebsiteConfig, ByVal strNodeName As String, parser As Func(Of XmlNode, Tuple(Of String, String, String, String))) As List(Of Tuple(Of String, String, String, String))
            Dim ret As New List(Of Tuple(Of String, String, String, String))
            Dim oCurrentNodes As XmlNodeList = poCurrentConfig._webConfigXML.SelectNodes(strNodeName)
            If oCurrentNodes.Count = 0 Then
                Return ret
            End If
            For Each oCurrentNode As XmlNode In oCurrentNodes
                Dim item As Tuple(Of String, String, String, String)
                item = parser(oCurrentNode)
                If ret.Any(Function(t) t.Item1 = item.Item1) = False Then
                    ret.Add(item)
                End If
            Next
            Return ret
        End Function
        Public Shared Function getItemsFromConfig(ByVal poCurrentConfig As CWebsiteConfig, ByVal strNodeName As String, parser As Func(Of XmlNode, String)) As List(Of String)
            Dim ret As New List(Of String)
            Dim oCurrentNodes As XmlNodeList = poCurrentConfig._webConfigXML.SelectNodes(strNodeName)
            If oCurrentNodes.Count = 0 Then
                Return ret
            End If
            For Each oCurrentNode As XmlNode In oCurrentNodes
                Dim item As String
                item = parser(oCurrentNode)
                If ret.Any(Function(t) t = item) = False Then
                    ret.Add(item)
                End If
            Next
            Return ret
        End Function

        Public Shared Function getItemsFromConfig(ByVal poCurrentConfig As CWebsiteConfig, ByVal strNodeName As String, parser As Func(Of XmlNode, KeyValuePair(Of String, String))) As Dictionary(Of String, String)
            Dim ret As New Dictionary(Of String, String)
            Dim oCurrentNodes As XmlNodeList = poCurrentConfig._webConfigXML.SelectNodes(strNodeName)
            If oCurrentNodes.Count = 0 Then
                Return ret
            End If
            For Each oCurrentNode As XmlNode In oCurrentNodes
                Dim item As KeyValuePair(Of String, String)
                item = parser(oCurrentNode)
                If ret.ContainsKey(item.Key) = False Then
                    ret.Add(item.Key, item.Value)
                End If
            Next
            Return ret
        End Function

        Public Shared Function hasNodeName(ByVal poCurrentConfig As CWebsiteConfig, ByVal xPath As String) As String
            Dim sXmlNode As XmlElement = poCurrentConfig._webConfigXML.SelectSingleNode(xPath)
            If sXmlNode IsNot Nothing Then
                Return "Y"
            End If
            Return ""
        End Function
		Public Shared Function checkDisAgreePopup(ByVal poCurrentConfig As CWebsiteConfig) As Boolean
			Dim isDisAgreePopup As Boolean = False
			Dim oVisibleNode As XmlNodeList = poCurrentConfig._webConfigXML.SelectNodes("VISIBLE")
			If oVisibleNode.Count = 0 Then
				Return isDisAgreePopup '' return default there is no attribute
			End If
			For Each childNode As XmlNode In oVisibleNode
				Try
					If childNode.Attributes("disagree_popup").InnerXml.ToUpper = "Y" Then
						isDisAgreePopup = True
						Exit For
					End If
				Catch ex As Exception
					Return isDisAgreePopup '' stick with default
				End Try
			Next
			Return isDisAgreePopup
		End Function

        '---get the occupancy status for config
        Public Shared Function ParseOccupancyStatus(ByVal poConfig As XmlElement, ByVal psLoanType As String) As String
            Dim xpath As String = psLoanType & "/VISIBLE"
            Dim isOccupancyStatus As String = "N"
            Dim oVisible As XmlElement
            Try
                oVisible = poConfig.SelectSingleNode(xpath)
                If oVisible.HasAttributes Then
                    If oVisible.GetAttribute("occupancy_status") = "Y" Then
                        Return "Y"
                    End If
                End If
            Catch ex As Exception
                Return isOccupancyStatus ''if something happen just stick with the default
            End Try
            Return isOccupancyStatus  ''return default if there is no occupance status
        End Function

        Public Shared Function GetAllDeclarations(psLoanType As String) As Dictionary(Of String, String)
            'TODO: we can move this stuff to xml config if needed. But for now, put it here can save performance
            Dim dic As New Dictionary(Of String, String)
            dic.Add("has_outstanding_judgement", "Are there any outstanding judgments against you?")
            dic.Add("has_bankruptcy", "Have you been declared bankrupt within the past 7 years?")
            dic.Add("has_bad_loan", "Have you had property foreclosed upon or given title or deed in lieu thereof in the last 7 years?")
            dic.Add("is_in_lawsuit", "Are you a party to a lawsuit?")
            dic.Add("is_related_to_bad_loan", "Have you directly or indirectly been obligated on any loan which resulted in foreclosure, transfer of title in lieu of foreclosure, or judgment? (This would include such loans as home mortgage loans, SBA loans, home improvement loans, educational loans, manufactured (mobile) home loans, any mortgage, financial obligation, bond, or loan guarantee.")
            dic.Add("is_presently_delinquent", "Are you presently delinquent or in default on any Federal debt or any other loan, mortgage, financial obligation, bond, or loan guarantee? If 'Yes,' give details as described in the preceding question.")
            dic.Add("has_separate_maintenance", "Are you obligated to pay alimony, child support, or separate maintenance?")
            dic.Add("is_down_payment_borrowed", "Is any part of the down payment borrowed?")
            dic.Add("is_endorser_on_note", "Are you a co-maker or endorser on a note?")
            'dic.Add("", "Are you a U.S. citizen?")
            'dic.Add("", "Are you a permanent resident alien?")
            dic.Add("is_property_primary_residence", "Do you intend to occupy the property as your primary residence? ")
            dic.Add("has_owner_ship_interest", "Have you had an ownership interest in a property in the last three years?")
            dic.Add("has_judge_bankrupt_foreclosure", "Do you currently have any outstanding judgments or have you ever filed for bankruptcy, had a debt adjustment plan confirmed under Chapter 13, Had Property Foreclosed Upon or Repossessed in the Last 7 yrs, or been a party in a lawsuit?")
            dic.Add("has_declare_bankrupt", "Have you ever been declared bankrupt or filed a petition for chapter 7 or 13?")
            dic.Add("has_chapter_13", "Have you ever filed for bankruptcy or had a debt adjustment plan confirmed under chapter 13 in the past 10 years? ")
            dic.Add("has_judgement", "Have you any outstanding judgments?")
            dic.Add("has_lawsuit_party", "Are you a party to a lawsuit?")
            dic.Add("has_foreclosure", "Have you had property foreclosed upon or given a deed in lieu of foreclosure in the last 7 years?")
            dic.Add("has_reposession", "Have you ever had any auto, furniture or property repossessed?")
            dic.Add("has_other_obligation", "Have you any obligations not listed?")
            dic.Add("has_past_due_bills", "Do you have any past due bills?")
            dic.Add("has_co_maker", "Are you a Co-maker, Co-signer or Guarantor on any loan?")
            dic.Add("has_alias", "Have you ever had credit in any other name?")
            dic.Add("has_income_decline", "Is Your Income Likely to Decline in the next two years?")
            dic.Add("has_suits_pending", "Have you any suits pending, judgments filed, alimony or support awards against you?")
            dic.Add("has_alimony", "Are you obligated to make Alimony, Support or Maintenance Payments?")
            dic.Add("has_property_for_sale", "Is the property securing this loan you are applying for currently for sale?")
            Dim heGroup As New Dictionary(Of String, Integer)
            heGroup.Add("has_outstanding_judgement", 1)
            heGroup.Add("has_bankruptcy", 2)
            heGroup.Add("has_bad_loan", 3)
            heGroup.Add("is_in_lawsuit", 4)
            heGroup.Add("is_related_to_bad_loan", 5)
            heGroup.Add("is_presently_delinquent", 6)
            heGroup.Add("has_separate_maintenance", 7)
            heGroup.Add("is_down_payment_borrowed", 8)
            heGroup.Add("is_endorser_on_note", 9)
            'heGroup.Add("", 10)
            'heGroup.Add("", 11)
            heGroup.Add("is_property_primary_residence", 12)
            heGroup.Add("has_owner_ship_interest", 13)
            heGroup.Add("has_judge_bankrupt_foreclosure", 14)
            heGroup.Add("has_declare_bankrupt", 15)
            heGroup.Add("has_chapter_13", 16)
            heGroup.Add("has_judgement", 17)
            heGroup.Add("has_lawsuit_party", 18)
            heGroup.Add("has_foreclosure", 19)
            heGroup.Add("has_reposession", 20)
            heGroup.Add("has_other_obligation", 21)
            heGroup.Add("has_past_due_bills", 22)
            heGroup.Add("has_co_maker", 23)
            heGroup.Add("has_alias", 24)
            heGroup.Add("has_income_decline", 25)
            heGroup.Add("has_suits_pending", 26)
            heGroup.Add("has_alimony", 27)
            heGroup.Add("has_property_for_sale", 28)
            Dim ccGroup As New Dictionary(Of String, Integer)
            ccGroup.Add("has_judge_bankrupt_foreclosure", 14)
            ccGroup.Add("has_declare_bankrupt", 15)
            ccGroup.Add("has_chapter_13", 16)
            ccGroup.Add("has_judgement", 17)
            ccGroup.Add("has_lawsuit_party", 18)
            ccGroup.Add("has_foreclosure", 19)
            ccGroup.Add("has_reposession", 20)
            ccGroup.Add("has_other_obligation", 21)
            ccGroup.Add("has_past_due_bills", 22)
            ccGroup.Add("has_co_maker", 23)
            ccGroup.Add("has_alias", 24)
            ccGroup.Add("has_income_decline", 25)
            ccGroup.Add("has_suits_pending", 26)
            ccGroup.Add("has_alimony", 27)
            ccGroup.Add("has_property_for_sale", 28)
            Select Case psLoanType
                Case "HOME_EQUITY_LOAN"
                    Return (From item In heGroup.OrderBy(Function(p) p.Value)
                            Where dic.ContainsKey(item.Key)
                            Let value = dic(item.Key)
                            Select New With {.Key = item.Key, .Value = value}).ToDictionary(Function(p) p.Key, Function(q) q.Value)

                Case "CREDIT_CARD_LOAN", "PERSONAL_LOAN", "VEHICLE_LOAN"
                    Return (From item In ccGroup.OrderBy(Function(p) p.Value)
                            Where dic.ContainsKey(item.Key)
                            Let value = dic(item.Key)
                            Select New With {.Key = item.Key, .Value = value}).ToDictionary(Function(p) p.Key, Function(q) q.Value)
                Case Else
                    Return New Dictionary(Of String, String)()
            End Select
        End Function

        Public Shared Function GetActiveDeclarationList(poConfig As CWebsiteConfig, psLoanType As String) As Dictionary(Of String, String)
            Dim result As New Dictionary(Of String, String)
            Dim declarationList = GetAllDeclarations(psLoanType)
            Dim declarationItemNodes = poConfig._webConfigXML.SelectNodes(String.Format("{0}/DECLARATIONS/DECLARATION", psLoanType))
            If declarationItemNodes IsNot Nothing AndAlso declarationItemNodes.Count > 0 Then
                For Each item As XmlNode In declarationItemNodes
                    Dim key = item.Attributes("key").Value
                    If declarationList.ContainsKey(key) Then
                        result.Add(key, declarationList(key))
                    End If
                Next
            End If
            Return result
        End Function
        ''' <summary>
        ''' A, B, C, D, E, F, G, H, I, J, K, L, M, N, O, P, Q, R, S, T, U, V, W, X, Y, Z, AA, AB, AC, AD, AE, AF, AG, AH, ....
        ''' </summary>
        ''' <param name="number"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared Function GenerateAlphabetBulletName(number As Integer) As String
            Const charList = "abcdefghijklmnopqrstuvwxyz"
            If number > charList.Length Then
                If number Mod charList.Length = 0 Then
                    Return GenerateAlphabetBulletName(number \ charList.Length - 1) & GenerateAlphabetBulletName(charList.Length)
                Else
                    Return GenerateAlphabetBulletName(number \ charList.Length) & GenerateAlphabetBulletName(number Mod charList.Length)
                End If
            Else
                Return charList(number - 1).ToString()
            End If
        End Function


        '' ownwership level
        Public Shared Function hasOwnerShipLevel(ByVal poCurrentConfig As CWebsiteConfig) As String
            '' get ownership_level from downloading XA preferences 
            ''Dim oHasOwnerShip As XmlNodeList = poCurrentConfig._webConfigXML.SelectNodes("XA_LOAN")
            ''If oHasOwnerShip.Count = 0 Then
            ''    Return "N"  '' return default there is no attribute
            ''End If
            ''For Each childNode As XmlNode In oHasOwnerShip
            ''    Try
            ''        If childNode.Attributes("ownership_level").InnerXml.ToLower = "product" Then
            ''            Return "Y"
            ''        End If
            ''    Catch ex As Exception
            ''        Return "N"  '' stick with default
            ''    End Try
            ''Next
            Dim xaPreferencesDoc = CDownloadedSettings.GetXAPreferences(poCurrentConfig)
            If xaPreferencesDoc IsNot Nothing Then
                Dim ownershipLevelNode As XmlNode = xaPreferencesDoc.SelectSingleNode("PREFERENCES/CORE_SYSTEM/OWNERSHIP_LEVEL")
                If ownershipLevelNode IsNot Nothing AndAlso Common.SafeString(ownershipLevelNode.InnerText).ToLower = "product" Then
                    Return "Y"
                End If
            End If
            Return "N"
        End Function
        '' ''get previous address threshold
        ''Public Shared Function getPreviousAddressThresHold(ByVal poCurrentConfig As CWebsiteConfig) As String
        ''	Dim preAddressThreshold As String = String.Empty
        ''	Dim oNode As XmlElement = poCurrentConfig._webConfigXML

        ''	Try
        ''		preAddressThreshold = oNode.Attributes("previous_address_threshold").InnerXml
        ''	Catch ex As Exception
        ''		preAddressThreshold = String.Empty '' there is no threshold
        ''	End Try

        ''	Return preAddressThreshold
        ''End Function

        Public Shared Function Theme_File(ByVal poCurrentConfig As CWebsiteConfig) As String
			Dim defaultTheme As String = "customSwatches.css"
			If poCurrentConfig Is Nothing OrElse poCurrentConfig._webConfigXML.SelectSingleNode("COLOR_SCHEME") Is Nothing Then
				Return defaultTheme	''stick with the default
			End If
			Dim sXmlElement As XmlElement = poCurrentConfig._webConfigXML.SelectSingleNode("COLOR_SCHEME")
			Dim getThemeFile As String = SafeString(sXmlElement.GetAttribute("theme_file"))
			If (Not String.IsNullOrEmpty(getThemeFile)) Then
				Return getThemeFile
			End If

			''  Dim getThemFile As String =SafeString(p
			''Dim oNodes As XmlNodeList = poCurrentConfig._webConfigXML.SelectNodes("COLOR_SCHEME")
			''Dim FileName As String = String.Empty
			''If oNodes.Count = 0 Then
			''    Return defaultTheme  'return the default if there is no attribute
			''End If
			''For Each childNode As XmlNode In oNodes
			''    Try
			''        FileName = childNode.Attributes("theme_file").InnerXml
			''    Catch
			''        Return defaultTheme  'if something happen just stick with the default
			''    End Try
			''    If FileName IsNot Nothing Then
			''        Return FileName
			''    End If
			''Next

			Return defaultTheme
		End Function
		''Public Shared Function getNodeAttributes(ByVal poCurrentConfig As CWebsiteConfig, ByVal sNodeName As String, ByVal sAttributeName As String) As String
		''    '' if xpath is not exist return empty string
		''    If poCurrentConfig._webConfigXML.SelectSingleNode(sNodeName) Is Nothing Then
		''        Return ""
		''    End If
		''    Dim sXmlElement As XmlElement = poCurrentConfig._webConfigXML.SelectSingleNode(sNodeName)
		''    If Not sXmlElement.HasAttributes Then '' if no attribute return empty string
		''        Return ""
		''    End If
		''    Return SafeString(sXmlElement.GetAttribute(sAttributeName))
		''End Function
		Public Shared Function hasBlankDefaultCitizenShip(ByVal poCurrentConfig As CWebsiteConfig) As Boolean
			Dim oNode As XmlNode = poCurrentConfig._webConfigXML.SelectSingleNode("BLANK_DEFAULT_CITIZENSHIP")
			If oNode IsNot Nothing Then
				Return True
			End If
			Return False
		End Function
		Public Shared Function DefaultCitizenShip(ByVal _DefaultCitizenship As String, ByVal sLenderref As String, ByVal blankDefaulCitizenship As Boolean) As String

			'default US citizen
			Dim citizenshipStatusDropdown As String = RenderDropdownlistWithEmpty(CEnum.CITIZENSHIP_STATUS, " ", "--Please Select--", "US CITIZEN")

			''blank default citizenship from config 
			If blankDefaulCitizenship Then
				Return RenderDropdownlistWithEmpty(CEnum.CITIZENSHIP_STATUS, " ", "--Please Select--", " ")
			End If
			''default from url parameter
			If _DefaultCitizenship IsNot Nothing Then
				If _DefaultCitizenship.ToUpper = "US" Then
					citizenshipStatusDropdown = RenderDropdownlistWithEmpty(CEnum.CITIZENSHIP_STATUS, " ", "--Please Select--", "US CITIZEN")
				ElseIf _DefaultCitizenship.ToUpper = "PE" Then
					citizenshipStatusDropdown = RenderDropdownlistWithEmpty(CEnum.CITIZENSHIP_STATUS, " ", "--Please Select--", "PERM RESIDENT")
				Else

					citizenshipStatusDropdown = RenderDropdownlistWithEmpty(CEnum.CITIZENSHIP_STATUS, " ", "--Please Select--", " ")
				End If

			Else
				Return citizenshipStatusDropdown 'if no citizenship parameter in url return default
			End If
			Return citizenshipStatusDropdown 'return no default
		End Function

		'shouldn't be using this
		'Public Shared Function DefaultCitizenShip(ByVal _DefaultCitizenship As String) As String
		'    Return DefaultCitizenShip(_DefaultCitizenship, "")
		'End Function

		Public Shared Function DefaultCitizenShipFromConfig(ByVal dicCitizenship As Dictionary(Of String, String), ByVal _DefaultCitizenship As String, ByVal sLenderref As String, ByVal blankDefaulCitizenship As Boolean) As String
			If dicCitizenship.Count = 0 Then
				Return String.Empty	''
			End If

			'default US citizen
			Dim citizenshipStatusDropdown As String = RenderDropdownlistWithEmpty(dicCitizenship, " ", "--Please Select--", "USCITIZEN")
			''blank default citizenship from config 
			If blankDefaulCitizenship Then
				Return RenderDropdownlistWithEmpty(dicCitizenship, " ", "--Please Select--", " ")
			End If
			''default from url parameter
			If _DefaultCitizenship IsNot Nothing Then
				If _DefaultCitizenship.ToUpper = "US" Then
					citizenshipStatusDropdown = RenderDropdownlistWithEmpty(dicCitizenship, " ", "--Please Select--", "USCITIZEN")
				ElseIf _DefaultCitizenship.ToUpper = "PE" Then
					citizenshipStatusDropdown = RenderDropdownlistWithEmpty(dicCitizenship, " ", "--Please Select--", "PERMRESIDENT")
				Else
					citizenshipStatusDropdown = RenderDropdownlistWithEmpty(dicCitizenship, " ", "--Please Select--", " ")
				End If

			Else
				Return citizenshipStatusDropdown 'if no citizenship parameter in url return default
			End If
			Return citizenshipStatusDropdown 'return no default
		End Function

		Public Shared Function getCheckMailAddress(ByVal poCurrentConfig As CWebsiteConfig) As String
			Dim CheckMailAddress As String = ""
			Dim oCheckMailAddress As XmlNode = poCurrentConfig._webConfigXML.SelectSingleNode("CHECK_MAIL_ADDRESS")
			If oCheckMailAddress IsNot Nothing Then
				CheckMailAddress = oCheckMailAddress.InnerText
			End If

			Return CheckMailAddress
		End Function
		Public Shared Function GetFomFeesFromCustomList(ByVal poCurrentConfig As CWebsiteConfig) As Dictionary(Of String, String)
			Dim dic As New Dictionary(Of String, String)
			Dim xpath As String = "CUSTOM_LIST/FOM_FEES/ITEM"
			Dim oEnumsNodes As XmlNodeList = poCurrentConfig._webConfigXML.SelectNodes(xpath)
			For Each node As XmlElement In oEnumsNodes
				Dim key As String = node.GetAttribute("text")
				If Not dic.ContainsKey(key) Then
					dic.Add(key, node.GetAttribute("value"))
				End If
			Next
			Return dic
		End Function

		Public Shared Function RenderingVerifyCodeField(ByVal sReasonableDemoCode As String, ByVal oConfig As CWebsiteConfig, Optional ByVal isHELOC As Boolean = False) As String
			Dim strHtml As String = ""
			Dim strVerifyCodeText As String = "To proceed, please click on the ""Reasonable Demonstration"" link below. If you can successfully open the PDF you will see an alpha-numeric code displayed. Enter the code (case sensitive) in the box below."
			Dim hePrefix As String = ""
			If isHELOC Then
				hePrefix = "HELOC"
			End If
			If sReasonableDemoCode = "Y" And Not String.IsNullOrEmpty(oConfig.demoPDFViewCode) And Not String.IsNullOrEmpty(oConfig.demoPDFViewURL) Then
				strHtml += "<input type='hidden' id='hdVerifyCode" & hePrefix & "' value='" & oConfig.demoPDFViewCode & "' />"
				strHtml += "<div id='divVerifyCode" & hePrefix & "' style='margin:15px 0px'><div>" & strVerifyCodeText & "<br/><br/><a href ='" & oConfig.demoPDFViewURL & "'>Reasonable Demonstration (PDF)</a><br/></div>"
				strHtml += "<div class='divVerifyCode'><div class='divVerifyCodeLeft'><div class='divVerifyCodeLeft_style'><input id='txtVerifyCode" & hePrefix & "' type='text' class='verify_code_n' /></div></div>"
				strHtml += "<div class='divVerifyCodeRight'><div class='divVerifyCodeRight_style'><a id='btnVerifyCode" & hePrefix & "' type='button' class='ui-btn ui-btn-c' href='#'>Verify Code</a></div></div>"
				strHtml += "<div style='clear: both'></div><div id='verifyCodeMessage" & hePrefix & "'></div></div></div> "
			End If
			Return strHtml
		End Function

		Public Shared Function StripHTML(ByVal input As String) As String
			If input Is Nothing Then
				Return String.Empty
			End If

			Return Regex.Replace(input, "<.*?>", String.Empty)
		End Function

		Public Shared Function GenerateDefaultEligibilityMessage() As String
			Dim sbDefaultElMessage As New StringBuilder	 ''default Eligibilty message
			sbDefaultElMessage.Append(HeaderUtils.RenderPageTitle(0, "Applying is fast and easy!", True))
			sbDefaultElMessage.Append("You will need the following information in order to complete the application:<br/>")
			sbDefaultElMessage.Append("<div class ='CheckMark MemLabel-Indent'>Government issued ID/Drivers License/State ID or Passport with current address</div>")
			sbDefaultElMessage.Append("<div class ='CheckMark MemLabel-Indent'>Your complete physical and mailing address</div>")
			sbDefaultElMessage.Append("<div class='CheckMark MemLabel-Indent'>Social Security Number</div>")
			sbDefaultElMessage.Append("<div class ='CheckMark MemLabel-Indent'>Date of Birth</div>")
			Return sbDefaultElMessage.ToString()
		End Function

#Region "Custom Question"

		Public Shared Function ParseCustomQuestions(ByVal poWebsiteNode As XmlElement, ByVal psLoanType As String) As List(Of CCustomQuestion)
			Dim customQuestions As New List(Of CCustomQuestion)
			If poWebsiteNode Is Nothing OrElse String.IsNullOrEmpty(psLoanType) Then Return customQuestions
			Dim xpath As String = psLoanType & "/CUSTOM_QUESTIONS/QUESTION"
			Dim oQuestions As XmlNodeList = poWebsiteNode.SelectNodes(xpath)

			For Each node As XmlElement In oQuestions
				Dim question As New CCustomQuestion(node)
				customQuestions.Add(question)

			Next

			Return customQuestions
		End Function

		Public Shared Function RenderCustomQuestions(ByVal oCustomQuestions As List(Of CCustomQuestion),
													 ByVal oConditionedQuestions As List(Of CCustomQuestion)) As String
			Dim htmlControl As New StringBuilder()

			Try
				Dim controlIndex As Integer = 0
				Dim groupIndex As Integer = 0
				For Each oItem As CCustomQuestion In oCustomQuestions
					Dim sCustomQuestions As String = RenderQuestionElements(oItem, controlIndex, groupIndex, "", "CustomQuestion")
					htmlControl.AppendLine(sCustomQuestions)

					Dim sConditionedQuestions As String = RenderConditionedQuestions(oConditionedQuestions, oItem.Name)
					htmlControl.AppendLine(sConditionedQuestions)
				Next
				'    _Log.Debug("Rendered custom question html: " + htmlControl.ToString())
			Catch ex As Exception
				_log.Error("Unable to RenderCustomQuestions", ex)
			End Try

			Return htmlControl.ToString()

		End Function

		Public Shared Function ParseConditionedQuestions(ByVal poWebsiteNode As XmlElement, ByVal psLoanType As String) As List(Of CCustomQuestion)
			Dim customQuestions As New List(Of CCustomQuestion)
			If poWebsiteNode Is Nothing OrElse String.IsNullOrEmpty(psLoanType) Then Return customQuestions
			Dim xpath As String = psLoanType & "/CUSTOM_CONDITIONED_QUESTIONS/QUESTION"
			Dim oQuestions As XmlNodeList = poWebsiteNode.SelectNodes(xpath)

			For Each node As XmlElement In oQuestions
				Dim question As New CCustomQuestion(node)
				customQuestions.Add(question)
			Next

			Return customQuestions
		End Function

		Public Shared Function RenderConditionedQuestions(ByVal oConditionedQuestions As List(Of CCustomQuestion), ByVal sCusiName As String) As String
			Dim builder As New StringBuilder()
			Try
				Dim controlIndex As Integer = 0
				Dim groupIndex As Integer = 0
				Dim sConditioning As String = ""
				Dim sConiName As String = ""
				Dim sShortConiName As String = ""

				For Each oItem As CCustomQuestion In oConditionedQuestions
					sConditioning = oItem.Conditioning
					sConiName = sConditioning.Split("=".ToCharArray())(0)
					sShortConiName = generateConditionedQuestionElementID(oItem.Name)

					If sConiName = sCusiName Then
						builder.AppendLine("<div id='div_" + sShortConiName + "'  style='display:none'>")
						builder.AppendLine(RenderQuestionElements(oItem, controlIndex, groupIndex, sShortConiName, "ConditionedQuestion", True))
						builder.AppendLine("</div>")

						'Looking for and generate children elements
						For Each oChildren As CCustomQuestion In oConditionedQuestions
							sConditioning = oChildren.Conditioning
							sConiName = sConditioning.Split("=".ToCharArray())(0)
							sShortConiName = generateConditionedQuestionElementID(oChildren.Name)

							If oItem.Name = sConiName Then
								builder.AppendLine("<div id='div_" + sShortConiName + "'>")
								builder.AppendLine(RenderQuestionElements(oChildren, controlIndex, groupIndex, sShortConiName, "ConditionedQuestion", True))
								builder.AppendLine("</div>")
							End If
						Next

					End If

				Next

			Catch ex As Exception
				_log.Error("Unable to RenderConditionedQuestions", ex)
			End Try

			Return builder.ToString()
		End Function

		Public Shared Function generateConditionedQuestionElementID(ByVal sConiName As String) As String
			Dim cleanString As String = Regex.Replace(sConiName, "[^A-Za-z0-9]", "")
			Return cleanString.ToLower()
		End Function
		Public Shared Function isNumericInputField(ByVal sRegExp As String) As Boolean

			If sRegExp.IndexOf("[0-9]") > -1 Or sRegExp.IndexOf("[0123456789]") > -1 Then
				Return True
			End If
			Return False
		End Function
		Private Shared Function RenderQuestionElements(ByVal oItem As CCustomQuestion, ByRef controlIndex As Integer, ByRef groupIndex As Integer,
													   ByVal sShortConiName As String, ByVal cssClass As String, Optional ByVal isConditionQuestion As Boolean = False) As String

			Dim htmlControl As New StringBuilder()
			Dim autoElementId As String = Guid.NewGuid().ToString().Substring(0, 8).Replace("-", "").ToLower()
			Dim strMaxValue As String = ""
			Dim strRegExpression As String = ""
			Dim maxLength As String = "200"
			Dim strExtraAttr As String = ""
			If Not String.IsNullOrEmpty(oItem.maxLength) Then
				maxLength = oItem.maxLength
			End If
			If Not String.IsNullOrEmpty(oItem.minLength) Then
				strExtraAttr += " min_length='" + oItem.minLength + "' "
			End If
			If Not String.IsNullOrEmpty(oItem.maxValue) Then
				strExtraAttr += " max_value='" + oItem.maxValue + "' "
			End If
			If Not String.IsNullOrEmpty(oItem.minValue) Then
				strExtraAttr += " min_value='" + oItem.minValue + "' "
			End If
			If Not String.IsNullOrEmpty(oItem.errorMessage) Then
				strExtraAttr += " error_message='" + oItem.errorMessage + "' "
			End If
			Dim sTitle As String = ""
			Dim sText As String = "<input aria-labelledby='" & autoElementId & "' type='text' id='txt_" & autoElementId & "' iname='{0}' maxlength='" + maxLength + "' class='" + cssClass + "' conditioning='{1}' is_required='{2}' con_iname='{3}' onchange='checkConditionedQuestions(this);' reg_expression='{4}'" + strExtraAttr + " />"
			Dim sDropdown As String = "<select aria-labelledby='" & autoElementId & "' type='dropdown' id='ddl_" & autoElementId & "' iname='{0}' class='" + cssClass + "' conditioning='{2}' is_required='{3}' con_iname='{4}' onchange='checkConditionedQuestions(this);' >{1}</select>"
			Dim sOption As String = "<option value=""{0}"">{1}</option>"
			Dim prefixChkId As String = "chk_cq_"
			If isConditionQuestion Then
				prefixChkId = "chk_con_cq_"
			End If
			Dim sCheck As String =
			"<label for='" & prefixChkId & "{1}'><input type='checkbox' iname='{0}' id='" & prefixChkId & "{1}' name='chk_cq_grp_{2}' answer=""{3}"" class='" + cssClass + "' conditioning='{5}' is_required='{6}' con_iname='{7}' onclick='checkConditionedQuestions(this);' />" &
			"{4}</label>"

			Dim sRadio As String =
			 "<input type='radio' iname='{0}' id='rd_cq_{1}' name='chk_cq_grp_{2}' answer=""{3}"" class='" + cssClass + "' conditioning='{5}' is_required='{6}' con_iname='{7}' onclick='checkConditionedQuestions(this);'/>" &
			 "<label for='rd_cq_{1}'>{4}</label>"
			Dim sHidden As String = "<input type='hidden' iname='{0}' value=""{1}"" class='CustomQuestion' />"

			If oItem.isRequired = "Y" Then
				sTitle = "<div class='Title' qname='{2}' {0}>{1}<span class='require-span'>*</span></div>"
			Else
				sTitle = "<div class='Title' qname='{2}' {0}>{1}</div>"
			End If

			Select Case oItem.UIType
				Case QuestionUIType.TEXT

					Select Case oItem.DataType
						Case "D"
							sText = "<input aria-labelledby='" & autoElementId & "' type='text' id='txt_" & autoElementId & "' iname='{0}' maxlength='" + maxLength + "' class='indate " + cssClass + "' conditioning='{1}' is_required='{2}' con_iname='{3}' onchange='checkConditionedQuestions(this);' reg_expression='{4}'" + strExtraAttr + " />"
						Case "N", "C"
							sText = "<input aria-labelledby='" & autoElementId & "' type='text' id='txt_" & autoElementId & "' iname='{0}' maxlength='" + maxLength + "' class='numeric " + cssClass + "' conditioning='{1}' is_required='{2}' con_iname='{3}' onchange='checkConditionedQuestions(this);' reg_expression='{4}'" + strExtraAttr + " />"
						Case "P"
							If isNumericInputField(oItem.RegExpression) Then
								cssClass += " numeric"
							End If
							sText = "<input aria-labelledby='" & autoElementId & "' type='password' id='txt_" & autoElementId & "' iname='{0}' maxlength='" + maxLength + "' class='" + cssClass + "' conditioning='{1}' is_required='{2}' con_iname='{3}' onchange='checkConditionedQuestions(this);' reg_expression='{4}'" + strExtraAttr + " />"

					End Select
					htmlControl.AppendFormat(sTitle, "id='" & autoElementId & "'", HttpUtility.HtmlEncode(oItem.Text), oItem.Name)
					htmlControl.AppendFormat(sText, HttpUtility.HtmlEncode(oItem.Name.Replace("'", "&#39;")), HttpUtility.HtmlEncode(oItem.Conditioning), oItem.isRequired, sShortConiName, oItem.RegExpression)
					''add re-enter Password input field, if type is password
					If oItem.DataType = "P" Then

						If Not String.IsNullOrEmpty(oItem.ConfirmationText) Then
							htmlControl.AppendFormat("<div id='" & autoElementId & "_confirm'><label>{0}</label></div>", HttpUtility.HtmlEncode(oItem.ConfirmationText))
						Else
							htmlControl.AppendFormat("<div id='" & autoElementId & "_confirm'><label>Re-enter: {0}</label></div>", HttpUtility.HtmlEncode(oItem.Text))
						End If

						htmlControl.AppendFormat("<input aria-labelledby='" & autoElementId & "_confirm' type='password' id='re_txt_" & autoElementId & "' iname='re_{0}' maxlength='{1}' {2} reg_expression='{3}'  />", oItem.Name, maxLength, strExtraAttr, oItem.RegExpression)
					End If
				Case QuestionUIType.DROPDOWN
					Dim htmlOption As New StringBuilder()

					htmlControl.AppendFormat(sTitle, "id='" & autoElementId & "'", oItem.Text, oItem.Name)
					For Each opt As CCustomQuestionOption In oItem.CustomQuestionOptions
						htmlOption.AppendFormat(sOption, HttpUtility.HtmlEncode(opt.Value), HttpUtility.HtmlEncode(opt.Text))
					Next
					htmlControl.AppendFormat(sDropdown, oItem.Name, htmlOption.ToString(), HttpUtility.HtmlEncode(oItem.Conditioning), oItem.isRequired, sShortConiName)
				Case QuestionUIType.CHECK
					htmlControl.AppendFormat(sTitle, "id='" & autoElementId & "'", oItem.Text, oItem.Name)
					htmlControl.AppendFormat("<div class='chkgrp' id='div_{0}' is_required='{1}'>", autoElementId, oItem.isRequired)
					For Each opt As CCustomQuestionOption In oItem.CustomQuestionOptions
						htmlControl.AppendFormat(sCheck, oItem.Name, controlIndex, groupIndex, HttpUtility.HtmlEncode(opt.Value), HttpUtility.HtmlEncode(opt.Text), HttpUtility.HtmlEncode(oItem.Conditioning), oItem.isRequired, sShortConiName)
						controlIndex += 1
					Next
					htmlControl.Append("</div>")
					groupIndex += 1

				Case QuestionUIType.RADIO
					htmlControl.AppendFormat(sTitle, "id='" & autoElementId & "'", oItem.Text, oItem.Name)
					htmlControl.AppendFormat("<div class='radgrp' id='div_{0}' is_required='{1}'>", autoElementId, oItem.isRequired)
					For Each opt As CCustomQuestionOption In oItem.CustomQuestionOptions
						htmlControl.AppendFormat(sRadio, oItem.Name, controlIndex, groupIndex, HttpUtility.HtmlEncode(opt.Value), HttpUtility.HtmlEncode(opt.Text), HttpUtility.HtmlEncode(oItem.Conditioning), oItem.isRequired, sShortConiName)
						controlIndex += 1
					Next
					htmlControl.Append("</div>")
					groupIndex += 1

				Case QuestionUIType.HIDDEN
					htmlControl.AppendFormat(sTitle, "style='display:none;'", oItem.Text, oItem.Name)
					htmlControl.AppendFormat(sHidden, oItem.Name, oItem.Value)

			End Select

			Return htmlControl.ToString()
		End Function

		Public Shared Function RenderCustomQuestionXAs(ByVal oCustomQuestions As List(Of CCustomQuestionXA), ByVal oConditionedQuestions As List(Of CCustomQuestion), ByVal pnAvailability As Integer) As String
			Dim htmlControl As New StringBuilder()
			'' add id for each question title 
			Dim sTitle As String = "<div id='CustomQuestion_{2}' cq_required ='{3}' class='Title labeltext-bold' {0}>{1}</div>"
			Dim sText As String = "<input aria-labelledby='CustomQuestion_{2}' qindex='{2}' iname='{0}' {1} is_required='{3}'/>"
			Dim autoElementId As String = Guid.NewGuid().ToString().Substring(0, 8).Replace("-", "").ToLower()
			Dim sDropdown As String = "<select qindex ='{2}' iname='{0}' class='CustomQuestion'>{1}</select>"

			Dim sOption As String = "<option value=""{0}"">{1}</option>"

			Dim sCheck As String =
			 "<input type='checkbox' qindex='{5}' chk_cq_length ='{6}' iname='{0}' id='chk_cq_{1}' name='chk_cq_grp_{2}'  answer=""{3}"" class='CustomQuestion'/>" &
			 "<label for='chk_cq_{1}'>{4}</label>"

			Dim sRadio As String =
			 "<input type='radio' iname='{0}' id='rd_cq_{1}' name='chk_cq_grp_{2}' answer=""{3}"" class='CustomQuestion'/>" &
			 "<label for='rd_cq_{1}'>{3}</label>"

			Dim sHidden As String = "<input type='hidden' iname='{0}' value=""{1}"" class='CustomQuestion' />"
			Try
				Dim controlIndex As Integer = 0
				Dim groupIndex As Integer = 0
				Dim questionIndex As Integer = 1
				Dim isRequired As String = "N"
				For Each oItem As CCustomQuestionXA In oCustomQuestions
					If oItem.IsHeader Then
						''no rendering
						''htmlControl.Append("<b>")
						''htmlControl.AppendFormat(sTitle, "", oItem.Text)
						''htmlControl.Append("</b>")
						Continue For
					End If

					If oItem.AnswerType = "HEADER" Then	'' check header type question for new api
						Continue For
					End If

					Dim accountScope As String = oItem.AccountPositionScope
					If pnAvailability = 2 Then
						If accountScope = "P" Then
							Continue For
						End If
					Else
						If accountScope = "S" Then
							Continue For
						End If
					End If
					''removed question index
					Dim preTitle As String = ""	' String.Format("<b>{0}. &nbsp;</b>", questionIndex)
					Dim subTitle As String = ""
					If oItem.IsRequired Then
						subTitle = String.Format("<span class='require-span'>*</span>")
						isRequired = "Y"
					Else
						isRequired = "N"
					End If
					Dim minLength As String = ""
					Dim maxLength As String = ""

					Select Case oItem.AnswerType
						Case "TEXTBOX"
							Dim extraAttr As String = "class='CustomQuestion' type='text' maxlength='150' id='txt_cq_" & questionIndex & "'"  ''change maxlength to 150

							If Not String.IsNullOrEmpty(oItem.maxLength) Or Not String.IsNullOrEmpty(oItem.minLength) Then
								Dim strLengthAttrs As String = "maxlength='150'"
								If Not String.IsNullOrEmpty(oItem.maxLength) Then
									strLengthAttrs = " maxlength='" & oItem.maxLength & "' "
								End If
								If Not String.IsNullOrEmpty(oItem.minLength) Then
									strLengthAttrs += " min_length='" & oItem.minLength & "' "
								End If
								extraAttr = "class='CustomQuestion' type='text'  id='txt_cq_" & questionIndex & "'" & strLengthAttrs
							End If

							Select Case oItem.DataType
								Case "D"
									extraAttr = "class='indate CustomQuestion' type='text' maxlength='10' id='txt_cq_" & questionIndex & "'"
								Case "N"
									extraAttr = "class='numeric CustomQuestion' type='text' maxlength='10' id='txt_cq_" & questionIndex & "'"
								Case "C"
									extraAttr = "class='numeric CustomQuestion' type='text' id='txt_cq_" & questionIndex & "'"
								Case "P"
									If String.IsNullOrEmpty(oItem.reg_expression) Then
										If Not String.IsNullOrEmpty(oItem.minLength) Then
											minLength = " min_length ='" & oItem.minLength & "' "
										End If
										If Not String.IsNullOrEmpty(oItem.maxLength) Then
											maxLength = " maxlength ='" & oItem.maxLength & "' "
										Else
											maxLength = " maxlength ='20' "	''default maxlength
										End If
									End If
									extraAttr = "class='CustomQuestion' id='txt_cq_" & questionIndex & "' type='password'" & minLength & maxLength

									If isNumericInputField(oItem.reg_expression) Then
										extraAttr = "class='numeric CustomQuestion' id='txt_cq_" & questionIndex & "' type='password'" & minLength & maxLength
									End If

							End Select
							''need to add reg_expression to the textbox if it exist
							If Not String.IsNullOrEmpty(oItem.reg_expression) Then
								extraAttr = extraAttr & " reg_expression='" & oItem.reg_expression & "'"
							End If
							''add error message attribute
							If Not String.IsNullOrEmpty(oItem.ErrorMessage) Then
								extraAttr = extraAttr & " error_message='" & oItem.ErrorMessage & "' "
							End If

							Dim conQuestionCheck As String = ""
							For Each sConCQ As CCustomQuestion In oConditionedQuestions
								If sConCQ.Conditioning.Contains(oItem.Name) Then
									conQuestionCheck = "onchange='checkConditionedQuestions(this);'"
								End If
							Next

							sText = "<input aria-labelledby='CustomQuestion_{2}' qindex='{2}' iname='{0}' {1} is_required='{3}'" & conQuestionCheck & "/>"

							htmlControl.AppendFormat(sTitle, " qname='" & oItem.Name & "'", preTitle + oItem.Text + subTitle, questionIndex, isRequired)
							htmlControl.AppendFormat(sText, HttpUtility.HtmlEncode(oItem.Name.Replace("'", "&#39;")), extraAttr, questionIndex, isRequired)
							''add re-enter Password input field, if type is password
							If oItem.DataType = "P" Then

								If Not String.IsNullOrEmpty(oItem.ConfirmationText) Then
									htmlControl.AppendFormat("<div id='CustomQuestion_{1}_confirm'><label>{0}</label></div>", oItem.ConfirmationText, questionIndex)
								Else
									htmlControl.AppendFormat("<div id='CustomQuestion_{1}_confirm'><label>Re-enter: {0}</label></div>", oItem.Text, questionIndex)
								End If
								htmlControl.AppendFormat("<input aria-labelledby='CustomQuestion_{2}_confirm' type='password' id='re_txt_cq_" & questionIndex & "' iname='re_{0}' {1} />", oItem.Name, maxLength + minLength, questionIndex)
							End If

						Case "DROPDOWN"
							htmlControl.AppendFormat(sTitle, " qname='" & oItem.Name & "'", preTitle + oItem.Text + subTitle, questionIndex, isRequired)
							Dim htmlOption As New StringBuilder()
							For Each opt As CCustomQuestionOption In oItem.CustomQuestionOptions

								''----add to replace opt.text to opt.value to second parameter
								' htmlOption.AppendFormat(sOption, HttpUtility.HtmlEncode(opt.Text), HttpUtility.HtmlEncode(opt.Text))

								htmlOption.AppendFormat(sOption, HttpUtility.HtmlEncode(opt.Value), HttpUtility.HtmlEncode(opt.Text))
							Next
							''add conditionQuestionCheck function if Condition custom question
							Dim conQuestionCheck As String = ""
							For Each sConCQ As CCustomQuestion In oConditionedQuestions
								If sConCQ.Conditioning.Contains(oItem.Name) Then
									conQuestionCheck = "onchange='checkConditionedQuestions(this);'"
								End If
							Next
							sDropdown = "<select aria-labelledby='CustomQuestion_{2}' qindex ='{2}' iname='{0}' id='ddl_cq_{2}' class='CustomQuestion'" & conQuestionCheck & " is_required='{3}'>{1}</select>"
							htmlControl.AppendFormat(sDropdown, oItem.Name, htmlOption.ToString(), questionIndex, isRequired)

						Case "CHECKBOX"
							htmlControl.AppendFormat(sTitle, " qname='" & oItem.Name & "'", preTitle + oItem.Text + subTitle, questionIndex, isRequired)
							htmlControl.AppendFormat("<div class='chkgrp' id='div_chk{0}' is_required='{1}'>", groupIndex, isRequired)
							For Each opt As CCustomQuestionOption In oItem.CustomQuestionOptions
								''add conditionQuestionCheck function if it is condition custom question
								Dim conQuestionCheck As String = ""
								For Each sConCQ As CCustomQuestion In oConditionedQuestions
									If sConCQ.Conditioning.Contains(opt.Value) Then
										Dim sShortConiName As String = generateConditionedQuestionElementID(sConCQ.Name)
										conQuestionCheck = "onchange='checkConditionedQuestions(this);'"
										Exit For
									End If
								Next
								If Not String.IsNullOrEmpty(conQuestionCheck) Then
									sCheck = "<input type='checkbox' qindex='{5}' chk_cq_length ='{6}' iname='{0}' id='chk_cq_{1}' name='chk_cq_grp_{2}'  answer=""{3}"" class='CustomQuestion'" & conQuestionCheck & " />" &
											"<label for='chk_cq_{1}'>{4}</label>"
								End If
								''----replace opt.text to opt.value
								htmlControl.AppendFormat(sCheck, oItem.Name, controlIndex, groupIndex, HttpUtility.HtmlEncode(opt.Value), HttpUtility.HtmlEncode(opt.Text), questionIndex, oItem.CustomQuestionOptions.Count)
								controlIndex += 1
							Next
							htmlControl.Append("</div>")
							groupIndex += 1
					End Select
					questionIndex += 1
					''add condition question 
					Dim strConQuestions As String = RenderConditionedQuestions(oConditionedQuestions, oItem.Name)
					htmlControl.AppendLine(strConQuestions)
				Next
				'   _log.Debug("Rendered custom question html: " + htmlControl.ToString())
			Catch ex As Exception
				_log.Error("Unable to RenderCustomQuestions", ex)
			End Try
			'If String.IsNullOrEmpty(htmlControl.ToString()) Then
			'	qTitle = ""	'' no custom questions
			'End If
			'Return qTitle + htmlControl.ToString()
			Return htmlControl.ToString()

		End Function

		Public Shared Function RenderCustomQuestionHiddenInputs(ByVal oCustomQuestions As List(Of CCustomQuestion)) As String
			' Create hidden input fields that indicate whether a custom question is required or not
			' 
			Dim i As Integer
			Dim returnStr As String
			returnStr = ""
			i = 1
			Dim inputCount As Integer = 1
			Dim selectCount As Integer = 1
			For Each q As CCustomQuestion In oCustomQuestions

				'If q.IsHeader Then
				'    Continue For
				'End If
				''----hdRequiredQuestionInputs for textbox also for checkbox and radio button(add checkbox and radio button to this case)
				' If q.AnswerType.Equals("TEXTBOX") Then
				Dim isRequired As Boolean = False
				If q.UIType = QuestionUIType.TEXT Or q.UIType = QuestionUIType.CHECK Or q.UIType = QuestionUIType.RADIO Or q.UIType = QuestionUIType.HIDDEN Then
					returnStr += "<input type='hidden' runat='server' id='hdRequiredQuestionInputs"
					returnStr += inputCount.ToString() + "'"
					If (q.isRequired = "Y") Then
						isRequired = True
					End If
					returnStr += " value='" + IIf(isRequired, "Y", "N") + "' />"
					returnStr += System.Environment.NewLine
					inputCount += 1
				End If
				''----hdRequiredQuestionSelections only for dropdown selection, thus check box is not in this case(take out check box)
				If q.UIType = QuestionUIType.DROPDOWN Then
					returnStr += "<input type='hidden' runat='server' id='hdRequiredQuestionSelections"
					returnStr += selectCount.ToString() + "'"
					If (q.isRequired = "Y") Then
						isRequired = True
					End If
					returnStr += " value='" + IIf(isRequired, "Y", "N") + "' />"
					returnStr += System.Environment.NewLine
					selectCount += 1
				End If
				i += 1
			Next

			Return returnStr
		End Function

		Public Shared Function RenderCustomQuestionHiddenInputXAs(ByVal oCustomQuestions As List(Of CCustomQuestionXA), ByVal pnAvailability As Integer) As String
			' Create hidden input fields that indicate whether a custom question is required or not
			' 
			Dim i As Integer
			Dim returnStr As String
			returnStr = ""

			i = 1
			Dim inputCount As Integer = 1
			Dim selectCount As Integer = 1
			For Each q As CCustomQuestionXA In oCustomQuestions
				If q.IsHeader Then
					Continue For
				End If
				Dim accountScope As String = q.AccountPositionScope
				If pnAvailability = 2 Then
					If accountScope = "P" Then
						Continue For
					End If
				Else
					If accountScope = "S" Then
						Continue For
					End If
				End If
				''----hdRequiredQuestionInputs for textbox also for checkbox and radio button(add checkbox and radio button to this case)
				' If q.AnswerType.Equals("TEXTBOX") Then
				If q.AnswerType.Equals("TEXTBOX") Or q.AnswerType.Equals("CHECKBOX") Or q.AnswerType.Equals("RADIO") Then
					returnStr += "<input type='hidden' runat='server' id='hdRequiredQuestionInputs"
					returnStr += inputCount.ToString() + "'"
					returnStr += " value='" + IIf(q.IsRequired, "Y", "N") + "' />"
					returnStr += System.Environment.NewLine
					inputCount += 1
				End If
				''----hdRequiredQuestionSelections only for dropdown selection, thus check box is not in this case(take out check box)
				'If q.AnswerType.Equals("CHECKBOX") Or q.AnswerType.Equals("DROPDOWN") Then
				If q.AnswerType.Equals("DROPDOWN") Then
					returnStr += "<input type='hidden' runat='server' id='hdRequiredQuestionSelections"
					returnStr += selectCount.ToString() + "'"
					returnStr += " value='" + IIf(q.IsRequired, "Y", "N") + "' />"
					returnStr += System.Environment.NewLine
					selectCount += 1
				End If
				i += 1
			Next

			Return returnStr
		End Function

		Public Shared Function ConvertJsonToAnswers(ByVal psJSONText As String) As List(Of CCustomQuestionAnswer)
			Dim answerList As New CCustomQuestionAnswerList()
			_log.Info("JSON data from custom answers: " + psJSONText)
			If String.IsNullOrEmpty(psJSONText) Then Return answerList
			'' Parse json text to VB.Net object
			Dim jsonParser As New JsonTextParser()
			Try
				Dim jsonObj As JsonObject = jsonParser.Parse(psJSONText)
				Dim jsonArrCollection As JsonArrayCollection = CType(jsonObj, JsonArrayCollection)
				'' Parse all answers
				For Each oItem As JsonObject In jsonArrCollection
					Dim jsonObjCollection As JsonObjectCollection = CType(oItem, JsonObjectCollection)
					Dim nameObj As JsonObject = jsonObjCollection(0)
					Dim answerObj As JsonObject = jsonObjCollection(1)
					''----missing text, create answerTextObj for text
					Dim answerTextObj As JsonObject = jsonObjCollection(2)
					''------add type to object
					Dim answerTypeObj As JsonObject = jsonObjCollection(3)
					'' Dim question As New CCustomQuestionAnswer(CType(nameObj.GetValue(), String), CType(answerObj.GetValue(), String), CType(answerTextObj.GetValue(), String), CType(answerTypeObj.GetValue(), String))
					Dim question As New CCustomQuestionAnswer(HttpUtility.HtmlEncode(CType(nameObj.GetValue(), String)), CType(answerObj.GetValue(), String), CType(answerTextObj.GetValue(), String), CType(answerTypeObj.GetValue(), String))
					answerList.Add(question)
				Next
			Catch ex As Exception
				_log.Error("Unable to ConvertJsonToAnswers", ex)
			End Try

			Return answerList
		End Function
		Public Shared Function getFilterCustomQuestionAnswers(ByVal oConfig As CWebsiteConfig, ByVal sLoanType As String, ByVal oCustomAnswers As List(Of CCustomQuestionAnswer)) As CCustomQuestionAnswerList
			Dim CQAnswers As New CCustomQuestionAnswerList()
			Dim CCQuestions As List(Of CCustomQuestion) = oConfig.GetCustomQuestions(GetConfigLoanTypeFromShort(sLoanType))
			Dim conCCQuestions As List(Of CCustomQuestion) = oConfig.GetConditionedQuestions(GetConfigLoanTypeFromShort(sLoanType))
			If oCustomAnswers.Count = 0 Then
				Return CQAnswers
			End If
			For Each oCustomAnswer As CCustomQuestionAnswer In oCustomAnswers
				''decode single quote '
				oCustomAnswer.QuestionName = oCustomAnswer.QuestionName.Replace("&#39;", "'")
				If CCQuestions.Any(Function(q As CCustomQuestion) q.Name.ToLower = oCustomAnswer.QuestionName.ToLower) OrElse conCCQuestions.Any(Function(q As CCustomQuestion) q.Name.ToLower = oCustomAnswer.QuestionName.ToLower) Then
					CQAnswers.Add(oCustomAnswer)
				End If
			Next
			Return CQAnswers
		End Function
#End Region

#Region "utility"
		''' <summary>
		''' Controls how multiple IP (separated by comma) should be handled.  We normally only care for the client's IP, not the proxy.  
		''' Default behavior will be to return the first entry.	
		''' To capture last entry, use a -1 value in the appSettings.  
		''' To capture everything raw, use a -2 value in the appSettings.  	
		''' </summary>	
		''' <remarks>
		''' If index specified is out of bound, the entire IP will be returned.  This may cause unexpected issues if 
		''' not handled properly by client code (eg: data too large to fit db column)
		''' See here for more info on format: http://stackoverflow.com/questions/753645/how-do-i-get-the-correct-ip-from-http-x-forwarded-for-if-it-contains-multiple-ip
		''' </remarks>
		Private Shared STRIP_MULTIPLE_IP_INDEX As Integer = If(System.Configuration.ConfigurationManager.AppSettings.Get("STRIP_MULTIPLE_IP_INDEX") = "", 0, CInt(System.Configuration.ConfigurationManager.AppSettings.Get("STRIP_MULTIPLE_IP_INDEX")))


		''' <summary>
		''' Retrieves client browser IP, striping out any proxy information.  
		''' </summary>	
		Public Shared Function getBrowserIP(ByVal poRequest As System.Web.HttpRequest) As String
			Dim sResult As String = ""
			'handle forwarded for that may be masked by Barracuda
			sResult = poRequest.ServerVariables("HTTP_X_FORWARDED_FOR_ML")

			'default
			If sResult = "" Then
				sResult = poRequest.ServerVariables("REMOTE_ADDR")
			End If

			If STRIP_MULTIPLE_IP_INDEX >= 0 Then
				Dim arrTmp() As String = Split(sResult, ","c)
				If arrTmp.Length > STRIP_MULTIPLE_IP_INDEX Then
					sResult = arrTmp(STRIP_MULTIPLE_IP_INDEX)
				End If
			ElseIf STRIP_MULTIPLE_IP_INDEX = -1 Then
				sResult = Split(sResult, ","c).Last()
			Else
				'just leave as raw
			End If

			Return sResult
		End Function


		Public Shared Function ExtractDataFromConfigXml(configXml As XmlDocument, xPathList As List(Of String)) As Dictionary(Of String, String)
			Dim result As New Dictionary(Of String, String)
			If xPathList IsNot Nothing AndAlso xPathList.Count > 0 Then
				For Each xPath As String In xPathList
					Dim node = configXml.SelectSingleNode(xPath)
					If node IsNot Nothing Then
						result.Add(xPath, node.Value)
					Else
						result.Add(xPath, Nothing)
					End If
				Next
			End If
			Return result
		End Function
#End Region

#Region "document upload utility"

		Public Shared Function convertBase64Image2Base64PDF(ByVal psImage As String, ByVal psTitle As String) As String
			Dim oImageByte As Byte() = Convert.FromBase64String(psImage)
			Dim oPDFByte As Byte() = ConvertImageToPdf(oImageByte, psTitle)
			Dim sBase64PDF As String = Convert.ToBase64String(oPDFByte)
			Return sBase64PDF
		End Function
		Private Shared Function ConvertImageToPdf(ByVal poImage As Byte(), ByVal psTitle As String) As Byte()
			Dim oPDFByte As Byte()
			'Create Document class object and set its size to letter and give space left, right, Top, Bottom Margin
			Dim pdfDoc As New iTextSharp.text.Document(iTextSharp.text.PageSize.LETTER, 10, 10, 42, 35)
			Dim img As iTextSharp.text.Image = iTextSharp.text.Image.GetInstance(poImage)
			img.ScaleToFit(420.0F, 390.0F) 'Image resize
			img.SpacingBefore = 50.0F
			img.SpacingAfter = 1.0F
			img.Alignment = Element.ALIGN_CENTER
			Dim paragraph = New Paragraph(psTitle)	' add paragraph to the document
			Using memoryStream As New MemoryStream()
				Dim writer As PdfWriter = PdfWriter.GetInstance(pdfDoc, memoryStream)  'write to byte
				pdfDoc.Open()
				pdfDoc.Add(paragraph)
				pdfDoc.Add(img)	'add an image to the created pdf document
				pdfDoc.Close()
				oPDFByte = memoryStream.ToArray()
				memoryStream.Close()
			End Using
			Return oPDFByte
		End Function
		''=debug purpose===========
		Private Function StreamFile2Base64(ByVal psfilename As String) As String
			Return Convert.ToBase64String(StreamFile(psfilename))
		End Function

		Private Function Base642ASCI(ByVal psBase64 As String) As String
			Dim decodedBytes As Byte()
			decodedBytes = Convert.FromBase64String(psBase64)

			Dim decodedText As String
			decodedText = Encoding.UTF8.GetString(decodedBytes)
			Return decodedText
		End Function

		Private Function StreamFile(ByVal psfilename As String) As Byte()
			Dim filename As String = System.Web.HttpContext.Current.Server.MapPath(psfilename)
			Dim fs As New FileStream(filename, FileMode.Open, FileAccess.Read)

			' Create a byte array of file stream length 
			Dim ImageData As Byte() = New Byte(fs.Length - 1) {}

			'Read block of bytes from stream into the byte array 
			fs.Read(ImageData, 0, System.Convert.ToInt32(fs.Length))

			'Close the File Stream 
			fs.Close()
			'return the byte data 

			Return ImageData

		End Function


#End Region


#Region "IP validation"

		'
		Public Shared Function IsInternalIP(ByVal psRemoteIp As String) As Boolean


			Dim INTERNAL_IPS_ADD() As String = AppSettings.Get("INTERNAL_IPS_ADD").Split(","c)

			'If IsNothing(HttpContext.Current) Then
			'	Return False
			'End If

			Return INTERNAL_IPS_ADD.Any(Function(sIP) psRemoteIp.StartsWith(getSubnetMask(sIP.Trim)))
		End Function

		''' <summary>
		''' This routine just extracts set of IP's up to the last "0" fragments. 
		''' eg: 1.2.0.0 -> 1.2.
		''' 1.2.3.4 -> 1.2.3.4
		''' </summary>
		Public Shared Function getSubnetMask(ByVal IP As String) As String
			Dim newip As String = String.Copy(IP) 'make our own copy before we mutilate it
			If newip = "0.0.0.0" Then
				Return ""
			End If
			While newip.EndsWith(".0")
				newip = newip.Remove(newip.Length - 2, 2)
			End While

			'if we did remove any .0's, end the subnetmask with .
			'this is so that if we shrink 12.0.0.0 down to just 12, we wanna make it 11. so that
			'we also dont match 127.x, 128.x, etc
			If newip <> IP Then
				newip += "."
			End If

			Return newip
		End Function

#End Region
#Region "Loan IDA"
		Public Shared Function getIDAType(ByVal poCurrentConfig As CWebsiteConfig, ByVal poLoanType As String) As String
			Dim getIDA As String = ""
			Dim oNodes As XmlNodeList = poCurrentConfig._webConfigXML.SelectNodes(poLoanType)
			If oNodes.Count = 0 Then
				Return ""
			End If
			For Each childNode As XmlNode In oNodes
				Try
					getIDA = childNode.Attributes("ida").InnerXml
				Catch ex As Exception
					Return ""
				End Try
			Next
			Return getIDA
		End Function

		Public Shared Function getQuestionURL(ByVal oLoanIDA As String, ByVal poConfig As CWebsiteConfig) As String
			Dim oQuestionURL As String = ""
			Select Case oLoanIDA
				Case "RSA"
					oQuestionURL = poConfig.BaseSubmitLoanUrl & "/rsa/getquestions.aspx"
				Case "PID"
					oQuestionURL = poConfig.BaseSubmitLoanUrl & "/PreciseID/GetQuestions.aspx"
					'EquiFax
				Case "EID"
					oQuestionURL = poConfig.BaseSubmitLoanUrl & "/EquifaxInterConnect/GetQuestions.aspx"
				Case "FIS"
					oQuestionURL = poConfig.BaseSubmitLoanUrl & "/eFunds/GetQuestions.aspx"

				Case "DID"
					oQuestionURL = poConfig.BaseSubmitLoanUrl & "/DeluxeIDA/GetQuestions.aspx"
				Case "TID" '' TransUnion
					oQuestionURL = poConfig.BaseSubmitLoanUrl & "/IDMA/GetQuestions.aspx"
			End Select
			Return oQuestionURL
		End Function
		Public Shared Function getSubmitAnswerURL(ByVal oLoanIDA As String, ByVal poConfig As CWebsiteConfig) As String
			Dim oSubmitAnswerURL As String = ""
			Select Case oLoanIDA
				Case "RSA"
					oSubmitAnswerURL = poConfig.BaseSubmitLoanUrl & "/rsa/submitanswers.aspx"
				Case "PID"
					oSubmitAnswerURL = poConfig.BaseSubmitLoanUrl & "/PreciseID/SubmitAnswers.aspx"
					'EquiFax
				Case "EID"
					oSubmitAnswerURL = poConfig.BaseSubmitLoanUrl & "/EquifaxInterConnect/SubmitAnswers.aspx"
				Case "FIS"
					oSubmitAnswerURL = poConfig.BaseSubmitLoanUrl & "/eFunds/SubmitAnswers.aspx"
				Case "DID"
					oSubmitAnswerURL = poConfig.BaseSubmitLoanUrl & "/DeluxeIDA/SubmitAnswers.aspx"
				Case "TID" '' TransUnion
					oSubmitAnswerURL = poConfig.BaseSubmitLoanUrl & "/IDMA/SubmitAnswers.aspx"
			End Select
			Return oSubmitAnswerURL
		End Function
		''' <summary>
		''' check for successful submission(loansubmit, IDA), return fail when is is reject by LPQ lender side
		''' Return true when it is accepted by LPQ, will set pIsFailedOtherProcess to true if fail other process such as credit pull
		''' </summary>
		''' <param name="responseXml"></param>
		''' <param name="pIsFailedOtherProcess"></param>
		''' <returns></returns>
		''' <remarks></remarks>
		Public Shared Function IsSubmittedSuccessful(ByVal responseXml As String, Optional ByRef pIsFailedOtherProcess As Boolean = False) As Boolean
			Dim xmlDoc As New XmlDocument()
			Try
				xmlDoc.LoadXml(responseXml)
			Catch ex As Exception  'LPQ server return nothing or bad format
				_log.Warn("Bad response from decisionloan or submitloan webservice", ex)
				Return False
			End Try
			'check the response is nothing
			If (xmlDoc.GetElementsByTagName("RESPONSE").Count = 0) Then
				pIsFailedOtherProcess = True
			End If
			If xmlDoc.GetElementsByTagName("ERROR").Count > 0 Then
				If xmlDoc.GetElementsByTagName("ERROR")(0).InnerText.ToUpper.Contains("EXPIRED") Then 'expired  pw login pw
					Return False
				End If
				If xmlDoc.GetElementsByTagName("ERROR")(0).InnerText.Contains("failed schema validation") Then	'
					Return False
				End If
			End If

			If xmlDoc.GetElementsByTagName("FAILED").Count > 0 Then
				Return False
			End If

			If (responseXml.IndexOf("faileddetails") > -1) Or (responseXml.IndexOf("errordetails") > -1) Then 'Failed: Individual Not Located, invalid ssn
				pIsFailedOtherProcess = True
				Return True
			End If

			If (responseXml.IndexOf("MCL Exception") > -1) Then	'Unable to retrieve credit report first try.  MCL Exception: Missing Postal Code
				pIsFailedOtherProcess = True
				Return True
			End If

			'for preciseID, RAS,Equifax EID
			If xmlDoc.GetElementsByTagName("QUESTIONS").Count = 0 Then	'no question is found
				pIsFailedOtherProcess = True   '
				Return True
			End If

			Return True
		End Function

		Public Shared Function getWalletQuestionsResponseXML(ByVal poConfig As CWebsiteConfig, ByVal loanResponseXMLStr As String, ByVal sIDAType As String, ByVal IsJointApp As Boolean) As String
			' Get the correct URL depending on which authentication method was given in the config.xml file.
			Dim url As String = getQuestionURL(sIDAType, poConfig)
			Dim walletRequest As String = ""
			Select Case sIDAType.ToUpper()
				Case "PID"
					walletRequest = GetOutOfWalletQuestionsPID(loanResponseXMLStr, poConfig, IsJointApp)
				Case "RSA"
					walletRequest = GetOutOfWalletQuestionsRSA(loanResponseXMLStr, poConfig, IsJointApp)
				Case "FIS"
					walletRequest = GetOutOfWalletQuestionsFIS(loanResponseXMLStr, poConfig, IsJointApp)
				Case "DID"
					walletRequest = GetOutOfWalletQuestionsDID(loanResponseXMLStr, poConfig, IsJointApp)
					'equifax
				Case "EID"
					walletRequest = GetOutOfWalletQuestionsEquifax(loanResponseXMLStr, poConfig, IsJointApp)
				Case "TID" ''TransUnion
					walletRequest = GetOutOfWalletQuestionsTID(loanResponseXMLStr, poConfig, IsJointApp)
				Case "NONE"
					' If the option was set to NONE or there was no option set, then the lender does not support ID authentication
					' Then we're done.
					Return ""
			End Select
			''make sure walletRequest and  url  are not empty before going further
			If String.IsNullOrEmpty(walletRequest) Or String.IsNullOrEmpty(url) Then
				Return ""
			End If

			Dim req As WebRequest = WebRequest.Create(url)
			req.Method = "POST"
			req.ContentType = "text/xml"
			If IsJointApp Then
				' _log.info("Loans: Preparing request for Out of Wallet Questions for Joint Applicant")
				_log.Info("Loans: Preparing to POST Data(Authenticate) for Joint Applicant: " & CSecureStringFormatter.MaskSensitiveXMLData(walletRequest))
			Else
				'_log.info("Loans: Preparing request for Out of Wallet Questions")
				_log.Info("Loans: Preparing to POST Data(Authenticate): " & CSecureStringFormatter.MaskSensitiveXMLData(walletRequest))
			End If

			Dim writer As New StreamWriter(req.GetRequestStream())
			writer.Write(walletRequest)
			writer.Close()
			_log.Info("Start the request to " + url)

			Dim res As WebResponse = req.GetResponse()
			Dim reader As StreamReader = New StreamReader(res.GetResponseStream())
			Dim sXml As String = reader.ReadToEnd()
			_log.Info("Loans: Response from server(Authenticate): " + sXml)
			reader.Close()
			res.GetResponseStream().Close()
			req.GetRequestStream().Close()

			Dim bIsFailedAuthenticate As Boolean = False
			If Not IsSubmittedSuccessful(sXml, bIsFailedAuthenticate) Then '' make sure loan submitted
				Return ""
			End If
			If bIsFailedAuthenticate Then ''make sure authenticate is not failed
				Return ""
			End If
			Return sXml
		End Function
		Public Shared Function renderWalletQuestionsHTML(ByVal sIDAType As String, ByVal sXml As String, ByRef questionList_persist As List(Of CWalletQuestionsRendering.questionDataClass), ByRef sName As String, ByRef transaction_ID As String, ByRef question_set_ID As String, ByRef request_app_ID As String, ByRef request_client_ID As String, ByRef request_sequence_ID As String, ByRef reference_number As String) As String
			Dim walletObject As CWalletQuestionsRendering
			Dim psWalletQuestions As String = ""
			' Process the Out of Wallet Questions and return them so they can be displayed to the user
			walletObject = New CWalletQuestionsRendering(sXml, sIDAType)
			walletObject.populateQuestions(transaction_ID, question_set_ID, request_app_ID, request_client_ID, request_sequence_ID, reference_number)
			questionList_persist = walletObject.questionsList

			Dim walletQuestionsHTML As String = walletObject.RenderWalletQuestions(sName, transaction_ID, question_set_ID, request_app_ID, request_client_ID, request_sequence_ID, reference_number)
			Dim numQuestionsStr = Format(walletObject.NumQuestions, "00")
			psWalletQuestions = (numQuestionsStr + walletQuestionsHTML)
			Return psWalletQuestions
		End Function
		Public Shared Function GetOutOfWalletQuestionsPID(ByRef responseStr As String, ByVal oConfig As CWebsiteConfig, ByVal IsJointApp As Boolean) As String
			Dim myDocument As New XmlDocument
			myDocument.LoadXml(responseStr)
			Dim tempNodes As XmlNodeList
			Dim loanID As String = ""
			Dim loanIDXMLStr As String
			tempNodes = myDocument.GetElementsByTagName("RESPONSE")
			For Each childNode As XmlNode In tempNodes
				loanIDXMLStr = childNode.Attributes("loan_id").InnerXml
				If (Not String.IsNullOrEmpty(loanIDXMLStr)) Then
					loanID = loanIDXMLStr
				End If
			Next
			If (loanID = "") Then
				Return ""
			End If
			' Now construct the XML response
			Dim sb As New StringBuilder()
			Dim writer As XmlWriter = XmlTextWriter.Create(sb)
			writer.WriteStartDocument()
			' begin input
			writer.WriteStartElement("INPUT")

			writer.WriteStartElement("LOGIN")
			writer.WriteAttributeString("api_user_id", oConfig.APIUser)
			writer.WriteAttributeString("api_password", oConfig.APIPassword)

			writer.WriteEndElement()

			writer.WriteStartElement("REQUEST")
			writer.WriteAttributeString("app_id", loanID)
			writer.WriteAttributeString("applicant_index", "0")
			If IsJointApp Then
				writer.WriteAttributeString("is_joint", "true")
			Else
				writer.WriteAttributeString("is_joint", "false")
			End If
			writer.WriteEndElement()
			' end input
			writer.WriteEndElement()
			writer.Flush()
			writer.Close()
			Return sb.ToString()
		End Function

		Shared Function GetOutOfWalletQuestionsRSA(ByRef responseStr As String, ByVal oConfig As CWebsiteConfig, ByVal IsJointApp As Boolean) As String
			' Build the XML to submit for getting the Out of Wallet Questions
			' Build an XML object from the response string 
			Dim myDocument As New XmlDocument
			myDocument.LoadXml(responseStr)

			Dim tempNodes As XmlNodeList
			Dim loanID = ""
			Dim loanIDXMLStr As String
			tempNodes = myDocument.GetElementsByTagName("RESPONSE")
			For Each childNode As XmlNode In tempNodes
				loanIDXMLStr = childNode.Attributes("loan_id").InnerXml
				If (Not String.IsNullOrEmpty(loanIDXMLStr)) Then
					loanID = loanIDXMLStr
				End If
			Next

			If (loanID = "") Then
				Return ""
			End If

			' Now construct the XML response
			Dim sb As New StringBuilder()
			Dim writer As XmlWriter = XmlTextWriter.Create(sb)
			writer.WriteStartDocument()

			' begin input
			writer.WriteStartElement("INPUT")

			writer.WriteStartElement("LOGIN")
			'writer.WriteAttributeString("login", "quanntest")
			'writer.WriteAttributeString("password", "lpq123")
			writer.WriteAttributeString("api_user_id", oConfig.APIUser)
			writer.WriteAttributeString("api_password", oConfig.APIPassword)
			writer.WriteEndElement()

			writer.WriteStartElement("REQUEST")
			writer.WriteAttributeString("app_id", loanID)
			writer.WriteAttributeString("applicant_index", "0")
			If IsJointApp Then
				writer.WriteAttributeString("is_joint", "true")
			Else
				writer.WriteAttributeString("is_joint", "false")
			End If

			writer.WriteEndElement()

			' end input
			writer.WriteEndElement()
			writer.Flush()
			writer.Close()

			Dim returnStr = sb.ToString()
			Return returnStr
		End Function

		Shared Function GetOutOfWalletQuestionsFIS(ByVal responseStr As String, ByVal oConfig As CWebsiteConfig, ByVal IsJointApp As Boolean) As String
			' Build the XML to submit for getting the Out of Wallet Questions
			' Build an XML object from the response string 
			Dim myDocument As New XmlDocument
			myDocument.LoadXml(responseStr)

			Dim tempNodes As XmlNodeList
			Dim loanID = ""
			Dim loanIDXMLStr As String
			tempNodes = myDocument.GetElementsByTagName("RESPONSE")
			For Each childNode As XmlNode In tempNodes
				loanIDXMLStr = childNode.Attributes("loan_id").InnerXml
				If (Not String.IsNullOrEmpty(loanIDXMLStr)) Then
					loanID = loanIDXMLStr
				End If
			Next

			If (loanID = "") Then
				Return ""
			End If

			' Now construct the XML response
			Dim sb As New StringBuilder()
			Dim writer As XmlWriter = XmlTextWriter.Create(sb)
			writer.WriteStartDocument()

			' begin input
			writer.WriteStartElement("INPUT")

			writer.WriteStartElement("LOGIN")

			'FIS IDA ONLY work with lender level user
			If oConfig.APIUserLender <> "" Then
				writer.WriteAttributeString("api_user_id", oConfig.APIUserLender)
				writer.WriteAttributeString("api_password", oConfig.APIPasswordLender)
			Else
				writer.WriteAttributeString("api_user_id", oConfig.APIUser)
				writer.WriteAttributeString("api_password", oConfig.APIPassword)
			End If

			writer.WriteEndElement()

			writer.WriteStartElement("REQUEST")
			writer.WriteAttributeString("xpressappid", loanID)
			writer.WriteAttributeString("applicant_index", "0")

			If IsJointApp Then
				writer.WriteAttributeString("applicant_type", "JOINT")	'primary is used for both primary and coborrower, there is no spouse     
			Else
				writer.WriteAttributeString("applicant_type", "PRIMARY")	'primary is used for both primary and coborrower, there is no spouse     
			End If
			writer.WriteEndElement()
			' end input
			writer.WriteEndElement()
			writer.Flush()
			writer.Close()

			Dim returnStr = sb.ToString()
			Return returnStr
		End Function

		Shared Function GetOutOfWalletQuestionsDID(ByVal responseStr As String, ByVal oConfig As CWebsiteConfig, ByVal IsJointApp As Boolean) As String
			' Build the XML to submit for getting the Out of Wallet Questions
			' Build an XML object from the response string 
			Dim myDocument As New XmlDocument
			myDocument.LoadXml(responseStr)

			Dim tempNodes As XmlNodeList
			Dim loanID = ""
			Dim loanIDXMLStr As String
			tempNodes = myDocument.GetElementsByTagName("RESPONSE")
			For Each childNode As XmlNode In tempNodes
				loanIDXMLStr = childNode.Attributes("loan_id").InnerXml
				If (Not String.IsNullOrEmpty(loanIDXMLStr)) Then
					loanID = loanIDXMLStr
				End If
			Next

			If (loanID = "") Then
				Return ""
			End If

			' Now construct the XML response
			Dim sb As New StringBuilder()
			Dim writer As XmlWriter = XmlTextWriter.Create(sb)
			writer.WriteStartDocument()

			' begin input
			writer.WriteStartElement("INPUT")

			writer.WriteStartElement("LOGIN")
			writer.WriteAttributeString("api_user_id", oConfig.APIUser)
			writer.WriteAttributeString("api_password", oConfig.APIPassword)

			writer.WriteEndElement()

			writer.WriteStartElement("REQUEST")
			writer.WriteAttributeString("xpress_app_id", loanID)
			writer.WriteAttributeString("applicant_index", "0")

			If IsJointApp Then
				writer.WriteAttributeString("is_joint", "true")
			Else
				writer.WriteAttributeString("is_joint", "false")
			End If

			'writer.WriteAttributeString("is_joint", "false")  'join is only used for spouse not-coborrower
			writer.WriteEndElement()

			' end input
			writer.WriteEndElement()
			writer.Flush()
			writer.Close()

			Dim returnStr = sb.ToString()
			Return returnStr
		End Function
		Shared Function GetOutOfWalletQuestionsEquifax(ByVal responseStr As String, ByVal oConfig As CWebsiteConfig, ByVal IsJointApp As Boolean) As String
			' Build the XML to submit for getting the Out of Wallet Questions
			' Build an XML object from the response string 
			Dim myDocument As New XmlDocument
			myDocument.LoadXml(responseStr)

			Dim tempNodes As XmlNodeList
			Dim loanID = ""
			Dim loanIDXMLStr As String
			tempNodes = myDocument.GetElementsByTagName("RESPONSE")
			For Each childNode As XmlNode In tempNodes
				loanIDXMLStr = childNode.Attributes("loan_id").InnerXml
				If (Not String.IsNullOrEmpty(loanIDXMLStr)) Then
					loanID = loanIDXMLStr
				End If
			Next

			If (loanID = "") Then
				Return ""
			End If

			' Now construct the XML response
			Dim sb As New StringBuilder()
			Dim writer As XmlWriter = XmlTextWriter.Create(sb)
			writer.WriteStartDocument()

			' begin input
			writer.WriteStartElement("INPUT")

			writer.WriteStartElement("LOGIN")
			'writer.WriteAttributeString("login", "quanntest")
			'writer.WriteAttributeString("password", "lpq123")
			writer.WriteAttributeString("api_user_id", oConfig.APIUser)
			writer.WriteAttributeString("api_password", oConfig.APIPassword)
			writer.WriteEndElement()

			writer.WriteStartElement("REQUEST")
			writer.WriteAttributeString("app_id", loanID)
			writer.WriteAttributeString("applicant_index", "0")

			If IsJointApp Then
				writer.WriteAttributeString("is_joint", "true")
			Else
				writer.WriteAttributeString("is_joint", "false")
			End If

			writer.WriteEndElement()

			' end input
			writer.WriteEndElement()
			writer.Flush()
			writer.Close()

			Dim returnStr = sb.ToString()
			Return returnStr
		End Function

		Shared Function GetOutOfWalletQuestionsTID(ByVal responseStr As String, ByVal oConfig As CWebsiteConfig, ByVal IsJointApp As Boolean) As String
			' Build the XML to submit for getting the Out of Wallet Questions
			' Build an XML object from the response string 
			Dim myDocument As New XmlDocument
			myDocument.LoadXml(responseStr)

			Dim tempNodes As XmlNodeList
			Dim loanID = ""
			Dim loanIDXMLStr As String
			tempNodes = myDocument.GetElementsByTagName("RESPONSE")
			For Each childNode As XmlNode In tempNodes
				loanIDXMLStr = childNode.Attributes("loan_id").InnerXml
				If (Not String.IsNullOrEmpty(loanIDXMLStr)) Then
					loanID = loanIDXMLStr
				End If
			Next

			If (loanID = "") Then
				Return ""
			End If

			' Now construct the XML response
			Dim sb As New StringBuilder()
			Dim writer As XmlWriter = XmlTextWriter.Create(sb)
			writer.WriteStartDocument()

			' begin input
			writer.WriteStartElement("INPUT")

			writer.WriteStartElement("LOGIN")
			writer.WriteAttributeString("api_user_id", oConfig.APIUser)
			writer.WriteAttributeString("api_password", oConfig.APIPassword)
			writer.WriteEndElement()

			writer.WriteStartElement("REQUEST")
			writer.WriteAttributeString("xpressappid", loanID)
			writer.WriteAttributeString("mode", "IDA")
			writer.WriteAttributeString("applicant_index", "0")
			If IsJointApp Then
				writer.WriteAttributeString("applicant_type", "JOINT")
				'Session("isJoinQuestionDone") = "Done"
			Else
				writer.WriteAttributeString("applicant_type", "PRIMARY")
			End If

			writer.WriteEndElement()

			' end input
			writer.WriteEndElement()
			writer.Flush()
			writer.Close()

			Dim returnStr = sb.ToString()
			Return returnStr
		End Function

		''Public Shared Function parseAnswersForID(ByVal combinedAnswerStr As String) As List(Of String)
		''    ' Parse the answer ids that were posted to this form
		''    ' The answer ids are concatenated into a string separated by semicolons
		''    ' Read the string up to but not including a semicolon and write out what was read
		''    ' repeat until end of string
		''    Dim length As Integer = combinedAnswerStr.Length
		''    Dim answerIDList As New List(Of String)
		''    Dim answerIDStr As String = ""
		''    For I As Integer = 0 To length - 1
		''        If (combinedAnswerStr.Chars(I) = ";") Then
		''            ' Add the current string to the list
		''            answerIDList.Add(answerIDStr)
		''            answerIDStr = ""
		''        Else
		''            answerIDStr += combinedAnswerStr.Chars(I)
		''        End If
		''    Next
		''    ' Add the last one to the list
		''    answerIDList.Add(answerIDStr)
		''    Return answerIDList
		''End Function
		''Public Shared Function getQuestionIDs(ByVal authType As String, ByRef questionIDList As List(Of String), ByVal answerIDlist As List(Of String), ByVal poQuestionList As List(Of CWalletQuestionsRendering.questionDataClass)) As Boolean
		''    ' Based on the ids of the answers, match each answer with the correct question
		''    Dim foundMatch As Boolean = False
		''    'equifax
		''    If authType.ToUpper() = "EID" Then
		''        Dim answerIDCount As Integer = 0
		''        For Each questionObject As CWalletQuestionsRendering.questionDataClass In poQuestionList
		''            ' For each questionObject, find the choiceID that matches the answerID
		''            For Each choiceObject As CWalletQuestionsRendering.choiceClass In questionObject.choicesList
		''                If (choiceObject.choiceID.ToUpper() = answerIDlist(answerIDCount).ToUpper()) Then
		''                    questionIDList.Add(questionObject.questionID)
		''                    'go to next answer ID
		''                    answerIDCount = answerIDCount + 1
		''                    foundMatch = True
		''                    Exit For
		''                End If
		''            Next
		''            If (foundMatch = False) Then
		''                Return False
		''            End If
		''            foundMatch = False
		''        Next
		''    Else
		''        For Each answerID As String In answerIDlist
		''            ' For each answerID, find the questionID that it is associated with
		''            For Each questionObject As CWalletQuestionsRendering.questionDataClass In poQuestionList
		''                ' For each questionObject, find the choiceID that matches the answerID
		''                For Each choiceObject As CWalletQuestionsRendering.choiceClass In questionObject.choicesList
		''                    If (choiceObject.choiceID.ToUpper() = answerID.ToUpper()) Then
		''                        questionIDList.Add(questionObject.questionID)
		''                        foundMatch = True
		''                        Exit For
		''                    End If
		''                Next
		''                If (foundMatch) Then
		''                    Exit For
		''                End If
		''            Next
		''            If (Not foundMatch) Then
		''                Return False   ''not match something wrong return false
		''            End If
		''            foundMatch = False ''reset foundMatch for the next answerID
		''        Next
		''    End If
		''    Return True  ''answers and questions are matching
		''End Function

		Public Shared Function ExecWalletAnswers(ByVal oConfig As CWebsiteConfig, ByVal sWalletQuestionAndAnswers As String, ByVal questionList_persist As List(Of CWalletQuestionsRendering.questionDataClass), ByVal idaResponseIDList As List(Of String), ByVal HasJointApplication As Boolean, ByVal question_set_ID As String, ByVal request_app_ID As String, ByVal request_client_ID As String, ByVal request_sequence_ID As String, ByVal reference_number As String) As Boolean
			' First, we need to get the id's of the questions and answers
			Dim oWalletQuestionsAndAnswer As New List(Of CXAQuestionAnswer)
			Dim questionIDList As New List(Of String)
			Dim answerIDList As New List(Of String)
			Dim authType As String = idaResponseIDList(0)
			''Dim areAnswersValid As Boolean = getQuestionIDs(authType, questionIDList, answerIDList, questionList_persist)
			' '' If the answer IDs didn't match up with any question, then there was something wrong
			''If Not areAnswersValid Then
			''    Return False
			''End If
			If Not String.IsNullOrEmpty(sWalletQuestionAndAnswers) Then
				oWalletQuestionsAndAnswer = New JavaScriptSerializer().Deserialize(Of List(Of CXAQuestionAnswer))(sWalletQuestionAndAnswers)
			End If
			If oWalletQuestionsAndAnswer.Count = 0 Then
				Return False ''not wallet questions and answers
			End If

			For Each oQA In oWalletQuestionsAndAnswer
				questionIDList.Add(oQA.q)
				answerIDList.Add(oQA.a)
			Next

			Dim url As String = getSubmitAnswerURL(authType, oConfig)
			If String.IsNullOrEmpty(url) Then
				Return False
			End If
			' The answers are valid, so we package them up in the proper XML and send it off
			Dim walletAnswerXML As String = buildWalletAnswerXML(oConfig, questionIDList, answerIDList, idaResponseIDList, HasJointApplication, question_set_ID, request_app_ID, request_client_ID, request_sequence_ID, reference_number)
			''make sure walletAnswerXml is not nothing
			If String.IsNullOrEmpty(walletAnswerXML) Then
				Return False ''failed
			End If
			' Done building the XML, now ship it off
			Dim req As WebRequest = WebRequest.Create(url)
			req.Method = "POST"
			req.ContentType = "text/xml"
			If HasJointApplication Then
				_log.Info("Loans: Preparing to POST wallet answer Data for Joint: " & CSecureStringFormatter.MaskSensitiveXMLData(walletAnswerXML))
			Else
				_log.Info("Loans: Preparing to POST wallet answer Data: " & CSecureStringFormatter.MaskSensitiveXMLData(walletAnswerXML))
			End If

			Dim writer As New StreamWriter(req.GetRequestStream())
			writer.Write(walletAnswerXML)
			writer.Close()
			Dim res As WebResponse = req.GetResponse()
			Dim reader As New StreamReader(res.GetResponseStream())
			Dim sXml As String = reader.ReadToEnd()
			_log.Info("Loans: Response from server: " + sXml)
			reader.Close()
			res.GetResponseStream().Close()
			req.GetRequestStream().Close()
			' Check to see if the server's response to the wallet answers is not an error
			If Not IsSubmittedSuccessful(sXml) Then
				Return False
			End If
			' There are no errors, so check to see if PID was approved. 
			' If so, proceed to the final step.
			If Not hasPassedAuthentication(authType, sXml) Then
				Return False
			End If
			Return True	''passed ida
		End Function
		Public Shared Function buildWalletAnswerXML(ByVal oConfig As CWebsiteConfig, ByVal questionIDList As List(Of String), ByVal answerIDList As List(Of String), ByVal idaResponseIDList As List(Of String), ByVal IsJointApplication As Boolean, ByVal question_set_ID As String, ByVal request_app_ID As String, ByVal request_client_ID As String, ByVal request_sequence_ID As String, ByVal reference_number As String) As String
			Dim authenticationMethod As String = idaResponseIDList(0)
			Select Case authenticationMethod
				Case "RSA"
					Return buildWalletAnswerXMLRSA(oConfig, questionIDList, answerIDList, idaResponseIDList, IsJointApplication, question_set_ID)
				Case "PID"
					Return buildWalletAnswerXMLPID(oConfig, questionIDList, answerIDList, idaResponseIDList, IsJointApplication, question_set_ID)
				Case "FIS"
					Return buildWalletAnswerXMLFIS(oConfig, questionIDList, answerIDList, idaResponseIDList, IsJointApplication, question_set_ID)
				Case "DID"
					Return buildWalletAnswerXMLDID(oConfig, questionIDList, answerIDList, idaResponseIDList, IsJointApplication, question_set_ID, request_app_ID, request_client_ID, request_sequence_ID)
					' Equifax 
				Case "EID"
					Return buildWalletAnswerXMLEquifax(oConfig, questionIDList, answerIDList, idaResponseIDList, IsJointApplication, question_set_ID)
				Case "TID"
					Return buildWalletAnswerXMLTID(oConfig, questionIDList, answerIDList, idaResponseIDList, IsJointApplication, question_set_ID, reference_number)
			End Select
			Return Nothing
		End Function
		Public Shared Function buildWalletAnswerXMLPID(ByVal oConfig As CWebsiteConfig, ByVal questionIDList As List(Of String), ByVal answerIDList As List(Of String), ByVal idaResponseIDList As List(Of String), ByVal IsJointApplication As Boolean, ByVal question_set_ID As String) As String
			Dim sb As New StringBuilder()
			Dim loanID As String = idaResponseIDList(1)
			Dim transaction_ID As String = idaResponseIDList(2)
			Dim writer As XmlWriter = XmlTextWriter.Create(sb)
			writer.WriteStartDocument()
			' begin input
			writer.WriteStartElement("INPUT")
			writer.WriteStartElement("LOGIN")
			writer.WriteAttributeString("api_user_id", oConfig.APIUser)
			writer.WriteAttributeString("api_password", oConfig.APIPassword)
			writer.WriteEndElement()

			writer.WriteStartElement("REQUEST")
			writer.WriteAttributeString("app_id", loanID)
			writer.WriteAttributeString("applicant_index", "0")
			If IsJointApplication Then
				writer.WriteAttributeString("is_joint", "true")
			Else
				writer.WriteAttributeString("is_joint", "false")
			End If
			writer.WriteAttributeString("session_id", transaction_ID)

			Dim numQuestions As Integer = questionIDList.Count
			writer.WriteStartElement("ANSWERS")
			For i As Integer = 0 To numQuestions - 1
				writer.WriteStartElement("ANSWER")
				writer.WriteAttributeString("answer_text", answerIDList(i))
				writer.WriteEndElement()
			Next
			' END </ANSWERS>
			writer.WriteEndElement()
			' END </REQUEST>
			writer.WriteEndElement()
			' END </INPUT>
			writer.WriteEndElement()
			writer.Flush()
			writer.Close()
			Dim returnStr As String = sb.ToString()
			Return returnStr
		End Function


		Shared Function buildWalletAnswerXMLFIS(ByVal oConfig As CWebsiteConfig, ByVal questionIDList As List(Of String), ByVal answerIDList As List(Of String), ByVal idaResponseIDList As List(Of String), ByVal IsJointApplication As Boolean, ByVal question_set_ID As String) As String
			Dim sb As New StringBuilder()
			Dim loanID As String = idaResponseIDList(1)
			Dim transaction_ID As String = idaResponseIDList(2)
			Dim writer As XmlWriter = XmlTextWriter.Create(sb)
			writer.WriteStartDocument()

			' begin input
			writer.WriteStartElement("INPUT")

			writer.WriteStartElement("LOGIN")

			'FIS IDA ONLY work with lender level user
			If oConfig.APIUserLender <> "" Then
				writer.WriteAttributeString("api_user_id", oConfig.APIUserLender)
				writer.WriteAttributeString("api_password", oConfig.APIPasswordLender)
			Else
				writer.WriteAttributeString("api_user_id", oConfig.APIUser)
				writer.WriteAttributeString("api_password", oConfig.APIPassword)
			End If

			writer.WriteEndElement()


			writer.WriteStartElement("REQUEST")
			writer.WriteAttributeString("xpressappid", loanID)
			writer.WriteAttributeString("applicant_index", "0")

			If IsJointApplication Then
				writer.WriteAttributeString("applicant_type", "JOINT")	'for both prmary and co, there is no spouse
			Else
				writer.WriteAttributeString("applicant_type", "PRIMARY")
			End If

			writer.WriteAttributeString("quiz_id", question_set_ID)
			writer.WriteAttributeString("transaction_id", transaction_ID)

			Dim numQuestions As Integer = questionIDList.Count
			writer.WriteStartElement("ANSWERS")
			For i As Integer = 0 To numQuestions - 1
				writer.WriteStartElement("ANSWER")
				writer.WriteAttributeString("question_id", questionIDList(i))
				writer.WriteAttributeString("answer_id", answerIDList(i))
				writer.WriteEndElement()
			Next
			' END </ANSWERS>
			writer.WriteEndElement()

			' END </REQUEST>
			writer.WriteEndElement()

			' END </INPUT>
			writer.WriteEndElement()
			writer.Flush()
			writer.Close()

			Dim returnStr As String = sb.ToString()

			Return returnStr
		End Function

		Shared Function buildWalletAnswerXMLDID(ByVal oConfig As CWebsiteConfig, ByVal questionIDList As List(Of String), ByVal answerIDList As List(Of String), ByVal idaResponseIDList As List(Of String), ByVal IsJointApplication As Boolean, ByVal question_set_ID As String, ByVal request_app_ID As String, ByVal request_client_ID As String, ByVal request_sequence_ID As String) As String
			Dim sb As New StringBuilder()
			Dim loanID As String = idaResponseIDList(1)
			Dim transaction_ID As String = idaResponseIDList(2)
			Dim writer As XmlWriter = XmlTextWriter.Create(sb)
			writer.WriteStartDocument()

			' begin input
			writer.WriteStartElement("INPUT")

			writer.WriteStartElement("LOGIN")
			writer.WriteAttributeString("api_user_id", oConfig.APIUser)
			writer.WriteAttributeString("api_password", oConfig.APIPassword)
			writer.WriteEndElement()

			writer.WriteStartElement("REQUEST")
			writer.WriteAttributeString("xpress_app_id", loanID)
			writer.WriteAttributeString("applicant_index", "0")

			If IsJointApplication Then
				writer.WriteAttributeString("is_joint", "true")
			Else
				writer.WriteAttributeString("is_joint", "false")
			End If

			writer.WriteAttributeString("session_id", transaction_ID)
			writer.WriteAttributeString("request_app_id", request_app_ID)
			writer.WriteAttributeString("request_client_id", request_client_ID)
			writer.WriteAttributeString("request_sequence_id", request_sequence_ID)

			Dim numQuestions As Integer = questionIDList.Count
			writer.WriteStartElement("ANSWERS")
			For i As Integer = 0 To numQuestions - 1
				writer.WriteStartElement("ANSWER")
				writer.WriteAttributeString("question_number", questionIDList(i))
				writer.WriteAttributeString("answer_text", answerIDList(i))
				writer.WriteEndElement()
			Next
			' END </ANSWERS>
			writer.WriteEndElement()

			' END </REQUEST>
			writer.WriteEndElement()

			' END </INPUT>
			writer.WriteEndElement()
			writer.Flush()
			writer.Close()

			Dim returnStr As String = sb.ToString()

			Return returnStr
		End Function

		Shared Function buildWalletAnswerXMLRSA(ByVal oConfig As CWebsiteConfig, ByVal questionIDList As List(Of String), ByVal answerIDList As List(Of String), ByVal idaResponseIDList As List(Of String), ByVal IsJointApplication As Boolean, ByVal question_set_ID As String) As String
			Dim loanID As String = idaResponseIDList(1)
			Dim transaction_ID As String = idaResponseIDList(2)
			Dim sb As New StringBuilder()
			Dim writer As XmlWriter = XmlTextWriter.Create(sb)
			writer.WriteStartDocument()

			' begin input
			writer.WriteStartElement("INPUT")

			writer.WriteStartElement("LOGIN")
			'      writer.WriteAttributeString("login", "quanntest")
			'writer.WriteAttributeString("password", "lpq123")
			writer.WriteAttributeString("api_user_id", oConfig.APIUser)
			writer.WriteAttributeString("api_password", oConfig.APIPassword)
			writer.WriteEndElement()


			writer.WriteStartElement("REQUEST")
			writer.WriteAttributeString("transaction_id", transaction_ID)
			writer.WriteAttributeString("question_set_id", question_set_ID)
			writer.WriteAttributeString("app_id", loanID)
			writer.WriteAttributeString("applicant_index", "0")
			If IsJointApplication Then
				writer.WriteAttributeString("is_joint", "true")
			Else
				writer.WriteAttributeString("is_joint", "false")
			End If
			'writer.WriteAttributeString("is_joint", _IsJointApplication)
			'writer.WriteAttributeString("is_joint", "false")  'join is only used for spouse not-coborrower

			Dim numQuestions As Integer = questionIDList.Count
			writer.WriteStartElement("ANSWERS")
			For i As Integer = 0 To numQuestions - 1
				writer.WriteStartElement("ANSWER")
				writer.WriteAttributeString("question_id", questionIDList(i))
				writer.WriteAttributeString("choice_id", answerIDList(i))
				writer.WriteEndElement()
			Next
			' END </ANSWERS>
			writer.WriteEndElement()

			' END </REQUEST>
			writer.WriteEndElement()

			' END </INPUT>
			writer.WriteEndElement()
			writer.Flush()
			writer.Close()

			Dim returnStr As String = sb.ToString()

			Return returnStr
		End Function

		Shared Function buildWalletAnswerXMLEquifax(ByVal oConfig As CWebsiteConfig, ByVal questionIDList As List(Of String), ByVal answerIDList As List(Of String), ByVal idaResponseIDList As List(Of String), ByVal IsJointApplication As Boolean, ByVal question_set_ID As String) As String
			Dim sb As New StringBuilder()
			Dim loanID As String = idaResponseIDList(1)
			Dim transaction_ID As String = idaResponseIDList(2)
			Dim writer As XmlWriter = XmlTextWriter.Create(sb)
			writer.WriteStartDocument()

			' begin input
			writer.WriteStartElement("INPUT")

			writer.WriteStartElement("LOGIN")
			'      writer.WriteAttributeString("login", "quanntest")
			'writer.WriteAttributeString("password", "lpq123")
			writer.WriteAttributeString("api_user_id", oConfig.APIUser)
			writer.WriteAttributeString("api_password", oConfig.APIPassword)
			writer.WriteEndElement()
			writer.WriteStartElement("REQUEST")
			writer.WriteAttributeString("transaction_id", transaction_ID)
			writer.WriteAttributeString("app_id", loanID)
			writer.WriteAttributeString("applicant_index", "0")

			If IsJointApplication Then
				writer.WriteAttributeString("is_joint", "true")
			Else
				writer.WriteAttributeString("is_joint", "false")
			End If

			'writer.WriteAttributeString("is_joint", _IsJointApplication)
			'writer.WriteAttributeString("is_joint", "false")  'join is only used for spouse not-coborrower

			Dim numQuestions As Integer = questionIDList.Count
			writer.WriteStartElement("ANSWERS")
			For i As Integer = 0 To numQuestions - 1
				writer.WriteStartElement("ANSWER")
				writer.WriteAttributeString("question_number", questionIDList(i))
				writer.WriteAttributeString("answer_number", answerIDList(i))
				writer.WriteEndElement()
			Next
			' END </ANSWERS>
			writer.WriteEndElement()

			' END </REQUEST>
			writer.WriteEndElement()

			' END </INPUT>
			writer.WriteEndElement()
			writer.Flush()
			writer.Close()

			Dim returnStr As String = sb.ToString()

			Return returnStr
		End Function


		''TransUnion build wallet answer
		Shared Function buildWalletAnswerXMLTID(ByVal oConfig As CWebsiteConfig, ByVal questionIDList As List(Of String), ByVal answerIDList As List(Of String), ByVal idaResponseIDList As List(Of String), ByVal IsJointApplication As Boolean, ByVal question_set_ID As String, ByVal reference_number As String) As String
			Dim sb As New StringBuilder()
			Dim loanID As String = idaResponseIDList(1)
			Dim writer As XmlWriter = XmlTextWriter.Create(sb)
			writer.WriteStartDocument()

			' begin input
			writer.WriteStartElement("INPUT")

			writer.WriteStartElement("LOGIN")
			writer.WriteAttributeString("api_user_id", oConfig.APIUser)
			writer.WriteAttributeString("api_password", oConfig.APIPassword)
			writer.WriteEndElement()
			writer.WriteStartElement("REQUEST")
			writer.WriteAttributeString("xpressappid", loanID)
			writer.WriteAttributeString("applicant_index", "0")
			writer.WriteAttributeString("mode", "IDA")
			writer.WriteAttributeString("reference_number", reference_number)
			If IsJointApplication Then
				writer.WriteAttributeString("applicant_type", "JOINT")
			Else
				writer.WriteAttributeString("applicant_type", "PRIMARY")
			End If
			Dim numQuestions As Integer = questionIDList.Count
			writer.WriteStartElement("ANSWERS")
			For i As Integer = 0 To numQuestions - 1
				writer.WriteStartElement("ANSWER")
				writer.WriteAttributeString("question_name", questionIDList(i))
				writer.WriteAttributeString("choice_key", answerIDList(i))
				writer.WriteEndElement()
			Next
			' END </ANSWERS>
			writer.WriteEndElement()

			' END </REQUEST>
			writer.WriteEndElement()

			' END </INPUT>
			writer.WriteEndElement()
			writer.Flush()
			writer.Close()

			Dim returnStr As String = sb.ToString()

			Return returnStr
		End Function

		''end transunion

		Public Shared Function hasPassedAuthentication(ByVal authMethod As String, ByVal responseXMLStr As String) As Boolean
			' Return true if we passed the authentication 
			' We have to know which authentication method was used.
			Dim responseXMLDoc As XmlDocument = New XmlDocument()
			responseXMLDoc.LoadXml(responseXMLStr)
			' Get the result depending on the authentication method
			Dim successAttribute As String = ""
			Dim successValue As String = ""
			Select Case authMethod.ToUpper()
				Case "RSA"
					successAttribute = "transaction_result"
					successValue = "PASSED"
				Case "PID"
					successAttribute = "decision"
					successValue = "ACCEPT"
				Case "FIS"
					successAttribute = "decision"
					successValue = "PASS"
				Case "DID"
					successAttribute = "decision"
					successValue = "PASSED"
					'equifax 
				Case "EID"
					successAttribute = "decision_code"
					successValue = "Y"
					''transunion
				Case "TID"
					successAttribute = "decision"
					successValue = "ACCEPT"
				Case Else
					successAttribute = "transaction_result"
					successValue = "ACCEPT"
			End Select

			' Get the attribute from the XML response
			Dim responseAttribute As String = ""
			Dim responseAttributeNode As XmlNode = responseXMLDoc.SelectSingleNode("//@" + successAttribute)
			'equifax - to avoid the exception error in case the node has nothing
			If responseAttributeNode IsNot Nothing Then
				responseAttribute = responseAttributeNode.InnerXml
			End If
			' Compare the result with the proper success result
			If responseAttribute.ToUpper() = successValue.ToUpper() Then
				Return True
			Else
				Return False
			End If
		End Function
		Public Shared Function getResponseLoanID(ByVal responseStr As String) As String
			Dim myDocument As New XmlDocument
			myDocument.LoadXml(responseStr)
			Dim tempNodes As List(Of XmlNode) = New List(Of XmlNode)
			Dim loanID As String = ""
			For Each childNode In myDocument.GetElementsByTagName("RESPONSE")
				tempNodes.Add(childNode)
			Next
			For Each childNode In myDocument.GetElementsByTagName("ERROR")
				tempNodes.Add(childNode)
			Next
			For Each childNode As XmlNode In tempNodes
				loanID = childNode.Attributes("loan_id").InnerXml
				If (Not String.IsNullOrEmpty(loanID)) Then
					Return loanID
				End If
			Next
			Return Nothing ''loanID is not exist
		End Function

		'TODO: need to skip product select for decisionloan2.0
		''This function is used after running IDA for loan
		''getloan then update loan using decisionloan to underwrite if IDA passed
		Public Shared Function submitUpdateLoan(ByVal oConfig As CWebsiteConfig, ByVal loanID As String, ByVal loanRequestXMLStr As String, ByRef loanResponseXMLStr As String) As String
			Const constGetLoansURL As String = "/GetLoans/GetLoans.aspx"
			Dim url As String = oConfig.BaseSubmitLoanUrl + constGetLoansURL

			Dim getLoansXMLRequestStr As String = LoanRetrieval.BuildGetLoansXML(loanID, oConfig.APIUser, oConfig.APIPassword)

			'    ' Done building the XML, now ship it off
			Dim req As WebRequest = WebRequest.Create(url)

			req.Method = "POST"
			req.ContentType = "text/xml"

			_log.Info("Preparing to POST GetLoans Request: " & CSecureStringFormatter.MaskSensitiveXMLData(getLoansXMLRequestStr))

			Dim writer As New StreamWriter(req.GetRequestStream())
			writer.Write(getLoansXMLRequestStr)
			writer.Close()

			Dim res As WebResponse = req.GetResponse()

			Dim reader As New StreamReader(res.GetResponseStream())
			Dim sXml As String = reader.ReadToEnd()

			_log.Info("GetLoans Response: " & CSecureStringFormatter.MaskSensitiveXMLData(sXml))

			' Extract the inner xml and update the status of the loans 
			Dim responseXMLDoc As XmlDocument = New XmlDocument()
			responseXMLDoc.LoadXml(sXml)
			Dim innerXMLStr As String = ""
			innerXMLStr = responseXMLDoc.InnerText
			Dim innerXMLDoc As XmlDocument = New XmlDocument()
			innerXMLDoc.LoadXml(innerXMLStr)

			'TODO: refactor or simplify
			Dim childNodeList As XmlNodeList = innerXMLDoc.DocumentElement.ChildNodes
			For Each childNode As XmlNode In childNodeList
				If (childNode.Name.ToUpper() = "LOAN_STATUS") Then
					Dim tempElement As XmlElement = CType(childNode, XmlElement)
					'If psStatus.ToUpper = "PEN" Then
					'	tempElement.SetAttribute("loan_status", "PEN")
					'ElseIf psStatus.ToUpper = "AA" Then
					'	tempElement.SetAttribute("loan_status", "AA")
					'Else
					'	tempElement.SetAttribute("loan_status", "REF")
					'End If
					' '' set the approval/denial date to current date
					''tempElement.SetAttribute("approval_date", common.SafeDateTimeXml(Now.ToString()))

					'dont want to alter the time
					tempElement.RemoveAttribute("response_date")
					tempElement.RemoveAttribute("app_receive_date")
					Exit For
				End If
			Next

			'add reason to decision comment section so officier know what happen
			Dim sComment As String = ""	'Environment.NewLine
			sComment &= "SYSTEM (" & Now.ToShortDateString & "  " & Now.ToShortTimeString & "): "
			sComment &= "IDA PASSED - UNDERWRITE AND UPDATED LOAN STATUS"
			sComment &= Environment.NewLine
			sComment &= Environment.NewLine

			For Each childNode As XmlNode In childNodeList
				If (childNode.Name.ToUpper() = "COMMENTS") Then
					Dim childNodeList2 As XmlNodeList = childNode.ChildNodes
					For Each childNode2 As XmlNode In childNodeList2
						If (childNode2.Name.ToUpper() = "DECISION_COMMENTS") Then
							childNode2.InnerText = sComment & childNode2.InnerText
							Exit For
						End If
					Next
					Exit For
				End If
			Next

			'buil xml clf for posting
			Dim UpdateLoanRequestXmlDoc As XmlDocument = LoanSubmit.BuildUpdateRequestXmlDoc(oConfig, innerXMLDoc)
			Dim loanRquestElement As XmlElement = UpdateLoanRequestXmlDoc.SelectSingleNode("//REQUEST")
			loanRquestElement.SetAttribute("loan_id", loanID) ''add loan id
			loanRquestElement.SetAttribute("action_type", "UPDATE")	''update action type
			_log.Info("Preparing to Update loan data: " & CSecureStringFormatter.MaskSensitiveXMLData(UpdateLoanRequestXmlDoc.InnerXml))

			Dim loanSubmitURL As String = oConfig.SubmitLoanUrl
			_log.Info("start request to " & loanSubmitURL)	'should be posting to decision

			Dim req2 As WebRequest = WebRequest.Create(loanSubmitURL)
			req2.Method = "POST"
			req2.ContentType = "text/xml"
			Dim writer2 As New StreamWriter(req2.GetRequestStream())
			writer2.Write(UpdateLoanRequestXmlDoc.InnerXml)
			writer2.Close()
			Dim res2 As WebResponse = req2.GetResponse()
			Dim reader2 As New StreamReader(res2.GetResponseStream())
			Dim sXml2 As String = reader2.ReadToEnd()
			_log.Info("Loan: Update loan response: " + sXml2)
			reader2.Close()
			res2.GetResponseStream().Close()
			req2.GetRequestStream().Close()

			loanResponseXMLStr = sXml2
			Return WebPostResponse(oConfig, UpdateLoanRequestXmlDoc, sXml2)
		End Function

		''' <summary>
		''' True=process oK, False = not setup/ connection issue
		''' psMessage return ACCEPT of REFER
		''' </summary>
		''' <param name="psMessage"></param>
		''' <returns></returns>
		''' <remarks></remarks>
		Public Shared Function ExecuteEfund(ByRef psMessage As String, ByVal poConfig As CWebsiteConfig, ByVal loanID As String, Optional ByVal hasJointApplicant As Boolean = False) As Boolean
			Dim eFundsXMLRequest As String = BuildeFundsXMLRequest(poConfig, loanID)
			Dim eFundsURL As String = poConfig.BaseSubmitLoanUrl & "/eFunds/eFunds.aspx"
			Dim req As WebRequest = WebRequest.Create(eFundsURL)

			req.Method = "POST"
			req.ContentType = "text/xml"

			_log.Info("XA: Preparing to POST eFunds Data: " & CSecureStringFormatter.MaskSensitiveXMLData(eFundsXMLRequest))

			Dim writer As New StreamWriter(req.GetRequestStream())
			writer.Write(eFundsXMLRequest)
			writer.Close()

			Dim res As WebResponse = req.GetResponse()

			Dim reader As New StreamReader(res.GetResponseStream())
			Dim sXml As String = reader.ReadToEnd()

			_log.Info("XA: Response from server(eFund): " + sXml)

			reader.Close()
			res.GetResponseStream().Close()
			req.GetRequestStream().Close()

			' Check to see if the eFunds response is not an error
			If Not IsSubmitted(sXml) Then
				Return False
			End If


			If isEFundsSuccessful(sXml) Then   'reserve 10/2
				psMessage = "ACCEPT"	'pass question
			Else
				psMessage = "REFER"	 'fail question
				Return False  'fail return false dont want to run join if the primary is referred
			End If

			''======Run for joint==========
			If Not hasJointApplicant Then Return True

			Dim eFundsXMLRequest2 As String = BuildeFundsXMLRequest(poConfig, loanID, True)
			Dim req2 As WebRequest = WebRequest.Create(eFundsURL)

			req2.Method = "POST"
			req2.ContentType = "text/xml"

			_log.Info("XA Joint: Preparing to POST eFunds Data: " & CSecureStringFormatter.MaskSensitiveXMLData(eFundsXMLRequest2))

			Dim writer2 As New StreamWriter(req2.GetRequestStream())
			writer2.Write(eFundsXMLRequest2)
			writer2.Close()

			Dim res2 As WebResponse = req2.GetResponse()

			Dim reader2 As New StreamReader(res2.GetResponseStream())
			Dim sXml2 As String = reader2.ReadToEnd()

			_log.Info("XA Joint: Response from server(eFund): " + sXml2)

			reader2.Close()
			res2.GetResponseStream().Close()
			req2.GetRequestStream().Close()

			' Check to see if the eFunds response is not an error
			If Not IsSubmitted(sXml2) Then
				Return False
			End If


			If isEFundsSuccessful(sXml2) Then	'reserve 10/2
				psMessage = "ACCEPT"	'pass question
			Else
				psMessage = "REFER"	 'fail question
				Return False '' exit 
			End If
			Return True

		End Function

		Shared Function IsSubmitted(ByVal responseXml As String, Optional ByRef pIsFailed As Boolean = False) As Boolean
			Dim xmlDoc As New XmlDocument()

			xmlDoc.LoadXml(responseXml)
			'check the response is nothing
			If (xmlDoc.GetElementsByTagName("RESPONSE").Count = 0) Then
				pIsFailed = True
			End If
			If xmlDoc.GetElementsByTagName("ERROR").Count > 0 Then
				If Not xmlDoc.GetElementsByTagName("ERROR")(0).InnerText.ToUpper.Contains("EXPIRED") Then 'expired  pw
					Return False
				End If

				Dim xmlError As XmlElement = xmlDoc.GetElementsByTagName("ERROR")(0)
				If Not xmlError.GetAttribute("loan_number") <> "" Then	'fail schema validation and NO loan number
					Return False
				End If
			End If

			If xmlDoc.GetElementsByTagName("FAILED").Count > 0 Then
				Return False
			End If

			If (responseXml.IndexOf("faileddetails") > -1) Or (responseXml.IndexOf("errordetails") > -1) Then 'Failed: Individual Not Located, invalid ssn
				pIsFailed = True
				Return True
			End If

			'for preciseID, RAS,Equifax EID
			If xmlDoc.GetElementsByTagName("QUESTIONS").Count = 0 Then	'no question is found
				pIsFailed = True   '
				Return True
			End If

			Return True
		End Function


		Public Shared Function IsAjaxRequest(request As System.Web.HttpRequest) As Boolean
			If request Is Nothing Then
				Throw New ArgumentNullException("request")
			End If
			Dim context = System.Web.HttpContext.Current
			Dim isCallbackRequest = False
			' callback requests are ajax requests
			If context IsNot Nothing AndAlso context.CurrentHandler IsNot Nothing AndAlso TypeOf context.CurrentHandler Is System.Web.UI.Page Then
				isCallbackRequest = DirectCast(context.CurrentHandler, System.Web.UI.Page).IsCallback
			End If
			Return isCallbackRequest OrElse (request("X-Requested-With") = "XMLHttpRequest") OrElse (request.Headers("X-Requested-With") = "XMLHttpRequest")
		End Function

		Shared Function BuildeFundsXMLRequest(poConfig As CWebsiteConfig, ByVal loanID As String, Optional ByVal pbIsJoint As Boolean = False) As String

			' Now construct the XML response
			Dim sb As New StringBuilder()
			Dim writer As XmlWriter = XmlTextWriter.Create(sb)
			writer.WriteStartDocument()

			' begin input
			writer.WriteStartElement("INPUT")

			writer.WriteStartElement("LOGIN")
			'writer.WriteAttributeString("login", "quanntest")
			'writer.WriteAttributeString("password", "lpq123")
			writer.WriteAttributeString("api_user_id", poConfig.APIUser)
			writer.WriteAttributeString("api_password", poConfig.APIPassword)
			writer.WriteEndElement()

			writer.WriteStartElement("REQUEST")
			'writer.WriteAttributeString("app_id", loanID)
			writer.WriteAttributeString("applicant_index", "0")
			writer.WriteAttributeString("xpressappid", loanID)
			If pbIsJoint = False Then
				writer.WriteAttributeString("applicant_type", "PRIMARY")
			Else
				writer.WriteAttributeString("applicant_type", "JOINT")
			End If
			'writer.WriteAttributeString("applicant_type", "PRIMARY")
			writer.WriteEndElement()

			' end input
			writer.WriteEndElement()
			writer.Flush()
			writer.Close()
			Dim returnStr = sb.ToString()
			Return returnStr
		End Function

		''' <summary>
		''' True=accept  else = refer
		''' </summary>
		''' <param name="authMethod"></param>
		''' <param name="responseXMLStr"></param>
		''' <returns></returns>
		''' <remarks></remarks>
		Shared Function isEFundsSuccessful(ByVal responseXMLStr As String) As Boolean
			' Return true if eFunds was successful
			Dim responseXMLDoc As XmlDocument = New XmlDocument()
			responseXMLDoc.LoadXml(responseXMLStr)
			' Get the attribute from the XML response
			Dim responseAttribute As String = ""
			Dim passedIdentityManager As String = ""
			Dim passedOFAC As String = ""

			Dim responseAttributeNode As XmlNode = responseXMLDoc.SelectSingleNode("//@qualifile_decision")
			Dim resIdentityManagerNode As XmlNode = responseXMLDoc.SelectSingleNode("//@passed_identity_manager")
			Dim resOFACNode As XmlNode = responseXMLDoc.SelectSingleNode("//@passed_ofac")

			'' check qualify_decision
			If responseAttributeNode Is Nothing Then Return False

			responseAttribute = responseAttributeNode.InnerXml.ToUpper
			Dim qualifileDecision As Boolean = If(responseAttribute = "ACCEPT" Or responseAttribute = "APPROVE" Or responseAttribute = "APPROVED", True, False)

			''exist quailifeDecision, identiyManagerNode and OFACNode 
			If resIdentityManagerNode IsNot Nothing And resOFACNode IsNot Nothing Then
				passedIdentityManager = resIdentityManagerNode.InnerXml.ToUpper
				passedOFAC = resOFACNode.InnerXml.ToUpper
				If qualifileDecision And passedIdentityManager = "Y" And passedOFAC = "Y" Then
					Return True	''success result
				Else
					Return False
				End If
			ElseIf resIdentityManagerNode IsNot Nothing And resOFACNode Is Nothing Then	''exist qualifileDecision and IdentityMangager
				passedIdentityManager = resIdentityManagerNode.InnerXml.ToUpper
				If qualifileDecision And passedIdentityManager = "Y" Then
					Return True	''success result
				Else
					Return False
				End If
			ElseIf resIdentityManagerNode Is Nothing And resOFACNode IsNot Nothing Then	''exist qualifile decision and OFAC
				passedOFAC = resOFACNode.InnerXml.ToUpper
				If qualifileDecision And passedOFAC = "Y" Then
					Return True	''success result
				Else
					Return False
				End If
			End If

			''by default exist only qualifile decision
			If qualifileDecision Then
				Return True
			Else
				Return False
			End If
		End Function

		''' <summary>
		''' True=process oK, False = not setup/ connection issue
		''' psMessage return ACCEPT of REFER
		''' </summary>
		''' <param name="psMessage"></param>
		''' <returns></returns>
		''' <remarks></remarks>
		Shared Function ExecuteRetailBanking(ByRef psMessage As String, ByVal poConfig As CWebsiteConfig, ByVal loanID As String, Optional ByVal hasJointApplicant As Boolean = False) As Boolean

			Dim RetailBankingURL As String = poConfig.BaseSubmitLoanUrl & "/RetailBanking/RetailBanking.aspx"

			Dim rBankingXMLRequest As String = BuildRetailBankingXMLRequest(poConfig, loanID)
			Dim req As WebRequest = WebRequest.Create(RetailBankingURL)

			req.Method = "POST"
			req.ContentType = "text/xml"

			_log.Info("XA: Preparing to POST RetailBanking Data: " & CSecureStringFormatter.MaskSensitiveXMLData(rBankingXMLRequest))

			Dim writer As New StreamWriter(req.GetRequestStream())
			writer.Write(rBankingXMLRequest)
			writer.Close()

			Dim res As WebResponse = req.GetResponse()

			Dim reader As New StreamReader(res.GetResponseStream())
			Dim sXml As String = reader.ReadToEnd()

			_log.Info("XA: Response from server(RetailBanking): " + sXml)

			reader.Close()
			res.GetResponseStream().Close()
			req.GetRequestStream().Close()

			' Check to see if the eFunds response is not an error
			If Not IsSubmitted(sXml) Then
				Return False
			End If

			If isRBankingSuccessful(sXml, poConfig) Then
				psMessage = "ACCEPT" ''pass question
			Else
				psMessage = "REFER"	 '' fail question
				Return False '' exit 
			End If
			''======Run for joint==========
			If Not hasJointApplicant Then Return True

			Dim rBankingXMLRequest2 As String = BuildRetailBankingXMLRequest(poConfig, loanID, True)
			Dim req2 As WebRequest = WebRequest.Create(RetailBankingURL)

			req2.Method = "POST"
			req2.ContentType = "text/xml"

			_log.Info("XA Joint: Preparing to POST RetailBanking Data: " & CSecureStringFormatter.MaskSensitiveXMLData(rBankingXMLRequest2))

			Dim writer2 As New StreamWriter(req2.GetRequestStream())
			writer2.Write(rBankingXMLRequest2)
			writer2.Close()

			Dim res2 As WebResponse = req2.GetResponse()

			Dim reader2 As New StreamReader(res2.GetResponseStream())
			Dim sXml2 As String = reader2.ReadToEnd()

			_log.Info("XA Joint: Response from server(RetailBanking): " + sXml2)

			reader2.Close()
			res2.GetResponseStream().Close()
			req2.GetRequestStream().Close()

			' Check to see if the eFunds response is not an error
			If Not IsSubmitted(sXml2) Then
				Return False
			End If

			If isRBankingSuccessful(sXml2, poConfig) Then
				psMessage = "ACCEPT"	'pass question
			Else
				psMessage = "REFER"	 'fail question
				Return False ''exit 
			End If
			Return True

		End Function

		Shared Function BuildRetailBankingXMLRequest(ByVal poConfig As CWebsiteConfig, ByVal loanID As String, Optional ByVal pbIsJoint As Boolean = False) As String
			''construct the xml response
			Dim sb As New StringBuilder()
			Dim writer As XmlWriter = XmlWriter.Create(sb)
			writer.WriteStartDocument()
			''begin input
			writer.WriteStartElement("INPUT")
			writer.WriteStartElement("LOGIN")
			writer.WriteAttributeString("api_user_id", poConfig.APIUser)
			writer.WriteAttributeString("api_password", poConfig.APIPassword)
			writer.WriteEndElement()
			writer.WriteStartElement("REQUEST")
			writer.WriteAttributeString("xpressappid", loanID)
			writer.WriteAttributeString("applicant_index", "0")
			If pbIsJoint = False Then
				writer.WriteAttributeString("applicant_type", "PRIMARY")
			Else
				writer.WriteAttributeString("applicant_type", "JOINT")
			End If
			'writer.WriteAttributeString("applicant_type", "PRIMARY")
			writer.WriteEndElement()
			'end input
			writer.WriteEndElement()
			writer.Flush()
			writer.Close()
			Dim returnStr = sb.ToString()
			Return returnStr
		End Function
		Shared Function isRBankingSuccessful(ByVal responseXMLStr As String, ByVal poConfig As CWebsiteConfig) As Boolean
			Dim responseXMLDoc As XmlDocument = New XmlDocument()
			responseXMLDoc.LoadXml(responseXMLStr)
			Dim DebitType As String = poConfig.DebitType
			''enable the retailbanking when DebitType = "EID"
			If (DebitType.ToUpper = "EID") Then
				''get the attribute from the xml response
				Dim responseAttribute As String = ""
				Dim responseAttributeNode As XmlNode = responseXMLDoc.SelectSingleNode("//@decision_code")
				If responseAttributeNode Is Nothing Then Return False
				responseAttribute = responseAttributeNode.InnerXml
				' Compare the result with the proper success result
				If responseAttribute.ToUpper() = "APPROVED" Then
					Return True
				Else
					Return False
				End If
			Else
				Return False
			End If
		End Function
#End Region

#Region "helper method"
		Public Shared Function getBehaviorNode(poCurrentWebsiteConfig As CWebsiteConfig) As Dictionary(Of String, String)
			Dim result As New Dictionary(Of String, String)

			Dim oNodes As XmlNodeList = poCurrentWebsiteConfig._webConfigXML.SelectNodes("BEHAVIOR")

			If oNodes.Count > 0 Then
				For Each attribute As XmlAttribute In oNodes(0).Attributes
					result.Add(attribute.Name, attribute.Value)
				Next
			End If

			Return result
		End Function

		Public Const DEFAULT_DEMOGRAPHIC_DESCRIPTION_MESSAGE = "<em><strong>Why we collect the following information.</strong> Federal law requires that lending institutions ask for demographic information. However, applicants are not required to provide it. This information is used to ensure that applicants are treated fairly and that the housing needs of communities and neighborhoods are being fulfilled.</em>"
		Public Shared Function GetDemographicDescriptionMessage(xnMessageNode As XmlNode) As String
			If xnMessageNode IsNot Nothing Then
				Dim sMessage = xnMessageNode.Attributes("message").Value
				If sMessage IsNot Nothing AndAlso Not String.IsNullOrWhiteSpace(sMessage) Then
					Return sMessage
				Else
					Return DEFAULT_DEMOGRAPHIC_DESCRIPTION_MESSAGE
				End If
			End If

			Return DEFAULT_DEMOGRAPHIC_DESCRIPTION_MESSAGE
		End Function


		Public Const DEFAULT_DEMOGRAPHIC_DISCLOSURE_MESSAGE = "<h3>Why we collect demographic information</h3>" &
"<strong>The purpose of collecting this information</strong> is to help ensure that all applicants are treated fairly and that the housing needs of communities and neighborhoods are being fulfilled. For residential mortgage lending, Federal law requires that we ask applicants for their demographic information (ethnicity, sex, and race) in order to monitor our compliance with equal credit opportunity, fair housing, and home mortgage disclosure laws." &
"<br /><br />" &
"You are not required to provide this information, but are encouraged to do so. You may select one or more designations for ""Ethnicity"" and one or more designations for ""Race"". <strong>The law provides that we may not discriminate</strong> on the basis of this information, or on whether you choose to provide it. However, if you choose not to provide the information and you have made this application in person, Federal regulations require us to note your ethnicity, sex, and race on the basis of visual observation or surname. The law also provides that we may not discriminate on the basis of age or marital status information you provide in this application. If you do not wish to provide some or all of this information, please check below."
		Public Shared Function GetDemographicDisclosureMessage(xnMessageNode As XmlNode) As String
			If xnMessageNode IsNot Nothing Then
				Dim sMessage = xnMessageNode.Attributes("message").Value
				If sMessage IsNot Nothing AndAlso Not String.IsNullOrWhiteSpace(sMessage) Then
					Return sMessage
				Else
					Return DEFAULT_DEMOGRAPHIC_DISCLOSURE_MESSAGE
				End If
			End If

			Return DEFAULT_DEMOGRAPHIC_DISCLOSURE_MESSAGE
		End Function
#End Region

#Region "minor account"
		Public Shared Function ParsedMinorAccountType(poConfig As CWebsiteConfig) As List(Of CMinorAccount)
			Dim minorAccountList As New List(Of CMinorAccount)
			Dim oNodes As XmlNodeList = poConfig._webConfigXML.SelectNodes("XA_LOAN/SPECIAL_ACCOUNT_TYPE/ITEM")
			If oNodes.Count > 0 Then
				For Each oChildNode As XmlNode In oNodes
					Dim minorAccount As New CMinorAccount
					minorAccount.minorAccountCode = ""
					minorAccount.minorMaxAge = ""
					minorAccount.minorMinAge = ""
					minorAccount.minorTitle = ""
					minorAccount.minorRoleType = ""
					minorAccount.secondRoleType = ""
					minorAccount.employment = ""

					minorAccount.minorIDAEnable = ""
					minorAccount.minorDebitEnable = ""
					minorAccount.decisionXAEnable = ""
					minorAccount.creditPull = ""
					minorAccount.debitType = ""
					minorAccount.authenticationType = ""

					If oChildNode.Attributes("account_code") IsNot Nothing Then
						minorAccount.minorAccountCode = SafeString(oChildNode.Attributes("account_code").InnerXml)
					End If
					If oChildNode.Attributes("max_age") IsNot Nothing Then
						minorAccount.minorMaxAge = SafeString(oChildNode.Attributes("max_age").InnerXml)
					End If
					If oChildNode.Attributes("min_age") IsNot Nothing Then
						minorAccount.minorMinAge = SafeString(oChildNode.Attributes("min_age").InnerXml)
					End If
					If oChildNode.Attributes("title") IsNot Nothing Then
						minorAccount.minorTitle = SafeString(oChildNode.Attributes("title").InnerXml)
					End If
					If oChildNode.Attributes("role_type") IsNot Nothing Then
						minorAccount.minorRoleType = SafeString(oChildNode.Attributes("role_type").InnerXml)
					End If
					If oChildNode.Attributes("second_role_type") IsNot Nothing Then
						minorAccount.secondRoleType = SafeString(oChildNode.Attributes("second_role_type").InnerXml)
					End If
					If oChildNode.Attributes("employment") IsNot Nothing Then
						minorAccount.employment = SafeString(oChildNode.Attributes("employment").InnerXml)
					End If

					If oChildNode.Attributes("minor_ida_enable") IsNot Nothing Then
						minorAccount.minorIDAEnable = SafeString(oChildNode.Attributes("minor_ida_enable").InnerXml)
					End If

					If oChildNode.Attributes("minor_debit_enable") IsNot Nothing Then
						minorAccount.minorDebitEnable = SafeString(oChildNode.Attributes("minor_debit_enable").InnerXml)
					End If
					If oChildNode.Attributes("decisionXA_enable") IsNot Nothing Then
						minorAccount.decisionXAEnable = SafeString(oChildNode.Attributes("decisionXA_enable").InnerXml)
					End If
					If oChildNode.Attributes("credit_pull") IsNot Nothing Then
						minorAccount.creditPull = SafeString(oChildNode.Attributes("credit_pull").InnerXml)
					End If
					If oChildNode.Attributes("ida") IsNot Nothing Then
						minorAccount.authenticationType = SafeString(oChildNode.Attributes("ida").InnerXml)
					End If
					If oChildNode.Attributes("debit") IsNot Nothing Then
						minorAccount.debitType = SafeString(oChildNode.Attributes("debit").InnerXml)
					End If

					minorAccountList.Add(minorAccount)
				Next
			End If
			Return minorAccountList
		End Function
		Public Shared Function ParsedMinorAccountProduct(poConfig As CWebsiteConfig) As List(Of MinorAccountProduct)
			Dim minorProductList As New List(Of MinorAccountProduct)
			Dim specialAccountNode As String = "XA_LOAN/SPECIAL_ACCOUNTS/ACCOUNT"
			Dim specialAccountTypeNode As String = "XA_LOAN/SPECIAL_ACCOUNT_TYPE/ITEM"
			Dim minorProductDic As Dictionary(Of String, String) = getItemsFromConfig(poConfig, specialAccountNode, "product_code", "minor_account_code")
			Dim minorAccountTypeDic As Dictionary(Of String, String) = getItemsFromConfig(poConfig, specialAccountTypeNode, "account_code", "title")
			Dim minorAccountCode As New List(Of String)
			If minorAccountTypeDic.Count = 0 Then
				Return minorProductList
			End If
			For Each oAccountType As KeyValuePair(Of String, String) In minorAccountTypeDic
				Dim mAccountCode As String = oAccountType.Key
				Dim mProductCodeList As New List(Of String)
				Dim minorProduct As New MinorAccountProduct
				For Each oMinorProduct As KeyValuePair(Of String, String) In minorProductDic
					If (oMinorProduct.Value.Contains("|")) Then
						Dim strProductCodeArray As String() = oMinorProduct.Value.Split("|")
						For i = 0 To strProductCodeArray.Length - 1
							If (mAccountCode = strProductCodeArray(i)) Then
								mProductCodeList.Add(oMinorProduct.Key)
								Exit For
							End If
						Next
					Else
						If (mAccountCode = oMinorProduct.Value) Then
							mProductCodeList.Add(oMinorProduct.Key)
						End If
					End If
				Next
				minorProduct.minorAccountCode = mAccountCode
				minorProduct.minorAccountProductList = mProductCodeList
				minorProductList.Add(minorProduct)
			Next
			Return minorProductList
		End Function

#End Region

#Region "Email"
		Public Shared Function SendEmail(toEmail As String, body As String, subject As String, Optional retryCount As Integer = 10, Optional fromEmail As String = "no_reply@loanspq.com") As String
			Dim result As String = ""
			Dim bNotSent As Boolean = True
			While (bNotSent AndAlso retryCount > 0)
				Try
					Dim smtpClient As New SmtpClient()
					If ConfigurationManager.AppSettings("SMTPServer") <> "" Then
						smtpClient = New System.Net.Mail.SmtpClient(ConfigurationManager.AppSettings("SMTPServer"))
					End If
					Dim mailMessage As New MailMessage()
					mailMessage.From = New System.Net.Mail.MailAddress(fromEmail)
					mailMessage.To.Add(New MailAddress(toEmail))
					mailMessage.Subject = subject
					mailMessage.Body = body
					mailMessage.IsBodyHtml = True
					'smtpClient.EnableSsl = True
					'_log.Info(subject)
					_log.Info(body)
					smtpClient.Send(mailMessage)
					bNotSent = False
					result = "OK"
				Catch ex As Exception
					_log.Info("Could not SendEmail with subject - " & subject, ex)
					result = "Unable to send email. Please try again later."
					System.Threading.Thread.Sleep(10000)
					retryCount -= 1
				End Try
			End While
			Return result
		End Function
#End Region
#Region "Funding"
		Public Shared Function getSelectedFundingType() As String
			Dim funding As CFundingSourceInfo = Nothing
			Dim sFundingType As String = ""
			If HttpContext.Current.Request.Params("rawFundingSource") IsNot Nothing Then
				funding = (New JavaScriptSerializer()).Deserialize(Of CFundingSourceInfo)(HttpContext.Current.Request.Params("rawFundingSource"))
			End If
			If funding IsNot Nothing Then
				sFundingType = funding.ValueFromType(funding.fsFundingType)
			End If
			Return sFundingType
		End Function
		Public Shared Function getTotalFundingAmount(ByVal strRequestXML As String) As Double
			Dim xmlDoc As XmlDocument = New XmlDocument()
			xmlDoc.LoadXml(strRequestXML)
			Dim innerXMLStr As String = xmlDoc.InnerText
			Dim innerXMLDoc As XmlDocument = New XmlDocument()
			innerXMLDoc.LoadXml(innerXMLStr)
			Dim responseNodeList As XmlNodeList = innerXMLDoc.GetElementsByTagName("ACCOUNT_TYPE")
			Dim totalAmount As Double = 0.0
			Dim accountType As XmlElement
			For Each oNode As XmlNode In responseNodeList
				If oNode.ParentNode.Name = "APPROVED_ACCOUNTS" Then Continue For
				accountType = oNode
				If accountType.GetAttribute("amount_deposit") IsNot Nothing Then
					totalAmount += SafeDouble(accountType.GetAttribute("amount_deposit"))
				End If
			Next
			Return totalAmount
		End Function
		Public Shared Function executeXAComboFundingAndBooking(ByVal oConfig As CWebsiteConfig, ByVal sLoanID As String, ByVal sXAID As String, ByVal ntotalFunding As Double, ByVal sXALoanNumber As String, ByRef sAccountNumber As String, validateBooking As Func(Of List(Of CBookingValidation))) As Boolean
			Dim sfundingType = Common.getSelectedFundingType()
			Dim _sInternalCommentAppend As String = ""
			Dim fundDepositsURL = oConfig.BaseSubmitLoanUrl + "/funddeposits/funddeposits.aspx"
			If oConfig.IsStandAloneFundingEnabled AndAlso oConfig.Booking.ToUpper() = "" Then 'stand alone funding with out booking
				''skip funding process if Funding type is empty or not funding(there is no funding source)
				If sfundingType = "" Then
					_sInternalCommentAppend = "Funding Source is not available. "
					LoanSubmit.UpdateLoan(sLoanID, oConfig, "XAFAILED", sXALoanNumber)	''leave status alone but add decision comments(MEMBERSHIP has been preferred"
					LoanSubmit.UpdateXA(sXAID, oConfig, "REFERRED", _sInternalCommentAppend)  'update XA status to referred and funding comment
					Return False
				End If
				''direct funding only work with Credit Card funding type
				If oConfig.IsACHFundingDirect And sfundingType = "BANK" OrElse sfundingType = "INTERNALTRANSFER" Then ''using LPQUniversity for testing this case
					_sInternalCommentAppend = "Unable to do ACH funding direct to account: Need to do booking before funding."
					LoanSubmit.UpdateLoan(sLoanID, oConfig, "XAFAILED", sXALoanNumber)	''leave status alone but add decision comments(MEMBERSHIP has been preferred"
					LoanSubmit.UpdateXA(sXAID, oConfig, "REFERRED", _sInternalCommentAppend)  'update XA status to referred and funding comment
					Return False
				End If

				Dim oFundDeposits As CFundDeposits = New CFundDeposits
				Dim sFundepositMessage = oFundDeposits.Execute(sXAID, oConfig, fundDepositsURL)
				If sFundepositMessage <> "SUCCESS" Then
					_sInternalCommentAppend = "Funding failed: " & sFundepositMessage
					LoanSubmit.UpdateLoan(sLoanID, oConfig, "XAFAILED", sXALoanNumber)	''leave status alone but add decision comments(MEMBERSHIP has been preferred"
					LoanSubmit.UpdateXA(sXAID, oConfig, "REFERRED", _sInternalCommentAppend)  'update XA status to referred and funding comment
					Return False
				Else
					Return True	''execute funding success 
				End If
			ElseIf oConfig.Booking.ToUpper() <> "" Then
				Dim oFundDeposits As CFundDeposits = New CFundDeposits
				Dim sFundepositMessage = "SUCCESS"
				'indirect GLA, funding 1st senario, then book
				Dim IsFundingSourceACH As Boolean = sfundingType = "BANK"
				Dim IsRequireMicroACHVerification As Boolean = False
				Dim sLPQDisabledPrimaryBooking = Common.getNodeAttributes(oConfig, "XA_LOAN/BEHAVIOR", "disable_primary_booking") = "Y"
				If IsFundingSourceACH AndAlso (Common.SafeDouble(ntotalFunding) > Common.SafeDouble(oConfig.ACHFundingThresholdRequireMicroDeposit) AndAlso Common.SafeDouble(oConfig.ACHFundingThresholdRequireMicroDeposit) > 0) Then IsRequireMicroACHVerification = True

				''execute funding only when funding is enabled 
				If oConfig.IsAutoFundingWBookingEnabled Then
					If sfundingType = "" Then
						sFundepositMessage = "Funding Source is not available. "
						_sInternalCommentAppend = sFundepositMessage
					Else
						If Not IsFundingSourceACH Or (IsFundingSourceACH And Not oConfig.IsACHFundingDirect AndAlso Not oConfig.ACHAutoFundingDisabled AndAlso Not IsRequireMicroACHVerification) Then
							sFundepositMessage = oFundDeposits.Execute(sXAID, oConfig, fundDepositsURL)
							If sFundepositMessage <> "SUCCESS" Then
								_sInternalCommentAppend = "Funding failed: " & sFundepositMessage
							End If
						End If
					End If
				End If
				'' Dim sAccountNumber As String = ""
				If sFundepositMessage = "SUCCESS" And oConfig.Booking <> "" Then  'no funding source mean no funding or booking
					If sLPQDisabledPrimaryBooking Then
						Return True	'' 
					Else
						Dim validateBookingResult = validateBooking()
                        If validateBookingResult IsNot Nothing AndAlso validateBookingResult.Count > 0 Then
                            _sInternalCommentAppend += "Booking Validation Rule (" & String.Join(". ", validateBookingResult.Select(Function(p) p.Name)) & ") trigger. Booking not run and status change to REFERRED."
                        Else
                            sAccountNumber = CBookingManager.Book(oConfig, sXAID, "COMBO")
							If sAccountNumber = "" Then
								_sInternalCommentAppend += "Booking failed.  "
								LoanSubmit.UpdateLoan(sLoanID, oConfig, "XAFAILED", sXALoanNumber)	''leave status alone but add decision comments(MEMBERSHIP has been preferred"
								LoanSubmit.UpdateXA(sXAID, oConfig, "REFERRED", _sInternalCommentAppend)  'update XA status to referred
								Return False
							End If
						End If
					End If
				End If
				'Direct to MIRC, funding 2nd senario, book then fund
				''execute funding only when funding is enabled 
				If oConfig.IsAutoFundingWBookingEnabled Then
					If sfundingType = "" Then
						sFundepositMessage = "Funding Source is not available. "
						_sInternalCommentAppend = sFundepositMessage
					Else
						If IsFundingSourceACH And oConfig.IsACHFundingDirect And sAccountNumber <> "" AndAlso Not IsRequireMicroACHVerification Then
							sFundepositMessage = oFundDeposits.Execute(sXAID, oConfig, fundDepositsURL)
							If sFundepositMessage <> "SUCCESS" Then
								_sInternalCommentAppend = "Funding failed: " & sFundepositMessage
							End If
						End If
					End If
				End If
				If sAccountNumber <> "" And sFundepositMessage = "SUCCESS" Then
					Return True
				Else
					LoanSubmit.UpdateLoan(sLoanID, oConfig, "XAFAILED", sXALoanNumber)	''leave status alone but add decision comments(MEMBERSHIP has been preferred"
					LoanSubmit.UpdateXA(sXAID, oConfig, "REFERRED", _sInternalCommentAppend)  'update XA status to referred
					Return False
				End If
			Else '' no booking or funding in setup
				Return True
			End If
		End Function

#End Region
#Region "CXSell"
		Public Shared Function prefilledData(ByVal isCoApp As Boolean, ByVal params As NameValueCollection, Optional ByVal loanPrefillData As Boolean = False) As Dictionary(Of String, String)
			Dim prefillData As New Dictionary(Of String, String)
			Dim coPrefix As String = ""
			If isCoApp Then
				coPrefix = "co_"
			End If
			If loanPrefillData Then
				prefillData.Add(coPrefix & "txtTotalMonthlyHousingExpense", SafeString(params(coPrefix & "TotalMonthlyHousingExpense")))
				prefillData.Add(coPrefix & "txtGrossMonthlyIncome", SafeString(params(coPrefix & "GrossMonthlyIncome")))
				prefillData.Add(coPrefix & "txtOtherMonthlyIncome", SafeString(params(coPrefix & "OtherMonthlyIncome")))
				prefillData.Add(coPrefix & "txtOtherMonthlyIncomeDesc", SafeString(params(coPrefix & "OtherMonthlyIncomeDesc")))
				prefillData.Add(coPrefix & "txtOtherMonthlyIncomeTaxExempt", SafeString(params(coPrefix & "OtherMonthlyIncomeTaxExempt")))
				prefillData.Add(coPrefix & "ddlCitizenshipStatus", SafeString(params(coPrefix & "CitizenshipStatus")))
				''reference infor
				If Not String.IsNullOrEmpty(Common.SafeString(params(coPrefix & "referenceInfo"))) Then
					Dim refInfor = New JavaScriptSerializer().Deserialize(Of List(Of String))(params(coPrefix & "referenceInfo"))
					If refInfor.Count > 0 Then
						For Each oItem In refInfor
							Dim sName = SafeString(oItem.Split("|")(0))
							Dim sValue = SafeString(oItem.Split("|")(1))
							If Not String.IsNullOrEmpty(sValue) Then
								If sName = "ref_first_name" Then
									prefillData.Add(coPrefix & "refFirstName", sValue)
								ElseIf sName = "ref_last_name" Then
									prefillData.Add(coPrefix & "refLastName", sValue)
								ElseIf sName = "ref_phone" Then
									prefillData.Add(coPrefix & "refPhone", sValue)
								ElseIf sName = "ref_email" Then
									prefillData.Add(coPrefix & "refEmail", sValue)
								ElseIf sName = "ref_relationship" Then
									prefillData.Add(coPrefix & "refRelationship", sValue)
								End If
							End If
						Next
					End If
				End If
			Else
				prefillData.Add(coPrefix & "txtGrossMonthlyIncome", params(coPrefix & "txtGrossMonthlyIncome"))
				prefillData.Add(coPrefix & "ddlCitizenshipStatus", SafeString(params(coPrefix & "Citizenship")))
			End If

			prefillData.Add(coPrefix & "txtFName", SafeString(params(coPrefix & "FirstName")))
			prefillData.Add(coPrefix & "txtMName", SafeString(params(coPrefix & "MiddleName")))
			prefillData.Add(coPrefix & "txtLName", SafeString(params(coPrefix & "LastName")))
			prefillData.Add(coPrefix & "suffix", SafeString(params(coPrefix & "NameSuffix")))
			prefillData.Add(coPrefix & "txtMemberNumber", SafeString(params(coPrefix & "MemberNumber")))
			prefillData.Add(coPrefix & "txtSSN", SafeString(params(coPrefix & "SSN")))
			Dim regExSSN As New Regex("^(?<p1>\d{3})(?<p2>\d{2})(?<p3>\d{4})$")
			Dim matchSSN = regExSSN.Match(SafeString(params(coPrefix & "SSN")))
			prefillData.Add(coPrefix & "txtSSN1", SafeString(matchSSN.Groups("p1").Value))
			prefillData.Add(coPrefix & "txtSSN2", SafeString(matchSSN.Groups("p2").Value))
			prefillData.Add(coPrefix & "txtSSN3", SafeString(matchSSN.Groups("p3").Value))

			prefillData.Add(coPrefix & "txtDOB", SafeString(params(coPrefix & "DOB")))
			Dim regExDOB As New Regex("^(?<p1>\d{1,2})/(?<p2>\d{1,2})/(?<p3>\d{4})$")
			Dim matchDOB = regExDOB.Match(SafeString(params(coPrefix & "DOB")))
			prefillData.Add(coPrefix & "txtDOB1", SafeString(matchDOB.Groups("p1").Value))
			prefillData.Add(coPrefix & "txtDOB2", SafeString(matchDOB.Groups("p2").Value))
			prefillData.Add(coPrefix & "txtDOB3", SafeString(matchDOB.Groups("p3").Value))

			prefillData.Add(coPrefix & "txtAddress", SafeString(params(coPrefix & "AddressStreet")))
			prefillData.Add(coPrefix & "txtZip", SafeString(params(coPrefix & "AddressZip")))
			prefillData.Add(coPrefix & "txtCity", SafeString(params(coPrefix & "AddressCity")))
			prefillData.Add(coPrefix & "ddlState", SafeString(params(coPrefix & "AddressState")))
			prefillData.Add(coPrefix & "txtAddress2", SafeString(params(coPrefix & "AddressStreet2")))
			prefillData.Add(coPrefix & "ddlCountry", SafeString(params(coPrefix & "Country")))
			prefillData.Add(coPrefix & "ddlOccupyingStatus", SafeString(params(coPrefix & "OccupyingLocation")))
			prefillData.Add(coPrefix & "txtOccupancyDescription", SafeString(params(coPrefix & "OccupancyDescription")))
			''mailing address if available 
			prefillData.Add(coPrefix & "txtMailingAddress", SafeString(params(coPrefix & "MailingAddressStreet")))
			prefillData.Add(coPrefix & "txtMailingZip", SafeString(params(coPrefix & "MailingAddressZip")))
			prefillData.Add(coPrefix & "txtMailingCity", SafeString(params(coPrefix & "MailingAddressCity")))
			prefillData.Add(coPrefix & "ddlMailingState", SafeString(params(coPrefix & "MailingAddressState")))
			prefillData.Add(coPrefix & "ddlMailingCountry", SafeString(params(coPrefix & "MailingAddressCountry")))

			Dim liveMonths As Integer = SafeInteger(params(coPrefix & "LiveMonths"))
			If Integer.TryParse(params(coPrefix & "LiveMonths"), liveMonths) Then
				prefillData.Add(coPrefix & "ddlOccupancyDurationYear", SafeString(liveMonths \ 12))
				prefillData.Add(coPrefix & "ddlOccupancyDurationMonth", SafeString(liveMonths Mod 12))
			End If
			prefillData.Add(coPrefix & "preferredContactMethod", SafeString(params(coPrefix & "ContactMethod")))
			prefillData.Add(coPrefix & "txtWorkPhoneExt", SafeString(params(coPrefix & "WorkPhoneEXT")))
			prefillData.Add(coPrefix & "txtWorkPhone", SafeString(params(coPrefix & "WorkPhone")))
			prefillData.Add(coPrefix & "txtMobilePhone", SafeString(params(coPrefix & "MobilePhone")))
			prefillData.Add(coPrefix & "txtEmail", SafeString(params(coPrefix & "EmailAddr")))
			Dim homePhone As String = SafeString(params(coPrefix & "HomePhone"))
			If homePhone = "" Then
				If SafeString(params(coPrefix & "ContactMethod")).ToUpper() = "WORK" Then
					homePhone = SafeString(params(coPrefix & "WorkPhone"))
				ElseIf SafeString(params(coPrefix & "ContactMethod")).ToUpper() = "CELL" Then
					homePhone = SafeString(params(coPrefix & "MobilePhone"))
				End If
			End If
			prefillData.Add(coPrefix & "txtHomePhone", homePhone)

			prefillData.Add(coPrefix & "ddlIDCardType", SafeString(params(coPrefix & "IDCardType")))
			prefillData.Add(coPrefix & "txtIDCardNumber", SafeString(params(coPrefix & "IDCardNumber")))

			prefillData.Add(coPrefix & "txtIDDateIssued", SafeString(params(coPrefix & "IDDateIssued")))
			Dim regExtxtIDDateIssued As New Regex("^(?<p3>\d{4})-(?<p1>\d{1,2})-(?<p2>\d{1,2})$")
			Dim matchtxtIDDateIssued = regExtxtIDDateIssued.Match(SafeString(params(coPrefix & "IDDateIssued")))
			prefillData.Add(coPrefix & "txtIDDateIssued1", SafeString(matchtxtIDDateIssued.Groups("p1").Value))
			prefillData.Add(coPrefix & "txtIDDateIssued2", SafeString(matchtxtIDDateIssued.Groups("p2").Value))
			prefillData.Add(coPrefix & "txtIDDateIssued3", SafeString(matchtxtIDDateIssued.Groups("p3").Value))

			prefillData.Add(coPrefix & "txtIDDateExpire", SafeString(params(coPrefix & "IDDateExpire")))
			Dim regExtxtIDDateExpire As New Regex("^(?<p3>\d{4})-(?<p1>\d{1,2})-(?<p2>\d{1,2})$")
			Dim matchtxtIDDateExpire = regExtxtIDDateExpire.Match(SafeString(params(coPrefix & "IDDateExpire")))
			prefillData.Add(coPrefix & "txtIDDateExpire1", SafeString(matchtxtIDDateExpire.Groups("p1").Value))
			prefillData.Add(coPrefix & "txtIDDateExpire2", SafeString(matchtxtIDDateExpire.Groups("p2").Value))
			prefillData.Add(coPrefix & "txtIDDateExpire3", SafeString(matchtxtIDDateExpire.Groups("p3").Value))
			prefillData.Add(coPrefix & "ddlIDState", SafeString(params(coPrefix & "IDState")))
			prefillData.Add(coPrefix & "ddlIDCountry", SafeString(params(coPrefix & "IDCountry")))
			''add employment status
			prefillData.Add(coPrefix & "ddlEmploymentStatus", SafeString(params(coPrefix & "EmploymentStatus")))
			prefillData.Add(coPrefix & "txtJobTitle", SafeString(params(coPrefix & "txtJobTitle")))
			prefillData.Add(coPrefix & "txtEmployer", SafeString(params(coPrefix & "txtEmployer")))
			prefillData.Add(coPrefix & "txtEmployedDuration_month", SafeString(params(coPrefix & "txtEmployedDuration_month")))
			prefillData.Add(coPrefix & "txtEmployedDuration_year", SafeString(params(coPrefix & "txtEmployedDuration_year")))
			prefillData.Add(coPrefix & "ddlBranchOfService", SafeString(params(coPrefix & "ddlBranchOfService")))
			prefillData.Add(coPrefix & "ddlPayGrade", SafeString(params(coPrefix & "ddlPayGrade")))
			Dim txtETS As String = Common.SafeString(params(coPrefix & "txtETS"))
			If (IsDate(txtETS)) Then
				Dim etsDate = txtETS.Split("/")
				prefillData.Add(coPrefix & "txtETS_txtDOB1", etsDate(0).ToString)
				prefillData.Add(coPrefix & "txtETS_txtDOB2", etsDate(1).ToString)
				prefillData.Add(coPrefix & "txtETS_txtDOB3", etsDate(2).ToString)
			End If

			Return prefillData
		End Function

#End Region
#Region "LenderRef Validation"
		''' <summary>
		''' Validates that a lenderref for a new portal or for updating the lenderref of an existing portal
		''' is safe to use. This may also be used in high-security contexts for various purposes, but may not
		''' necessarily be used per-request.
		''' </summary>
		''' <remarks>Keep syncrhonized with PDFFilesRefresh.aspx.vb:IsValidLenderRef</remarks>
		''' <param name="lenderref"></param>
		''' <returns>If the new lenderref is valid.</returns>
		Public Shared Function IsValidLenderRef(ByVal lenderref As String) As Boolean
			' Allow in the file name:
			' - Any unicode letter
			' - Any unicode number
			' - Any unicode marker
			' - Underscores and hyphens
			' Otherwise, it gets stripped. This would include characters that are unsafe
			' for SQL Or Windows Command Line, or other areas.
			' https://www.regular-expressions.info/unicode.html
			Return Regex.IsMatch(lenderref, "^[\p{L}\p{N}\p{M}_\-]{6,50}$")
		End Function
#End Region


	End Class

End Namespace