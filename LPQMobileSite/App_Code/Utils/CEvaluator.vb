﻿Imports System.CodeDom.Compiler
Imports Microsoft.VisualBasic
Imports System.Text
Public Class CEvaluator

	Private Shared _methodLookup As New Dictionary(Of String, [Delegate])
	Private Property Expression As String
	Private Property ReturnType As Type
	'using SortedLists to guarantee same order during class text generation
	Private Property Variables As SortedList(Of String, Variable)
	Private Property Methods As SortedList(Of String, String)
	Private Property References As SortedList(Of String, Object)

	Public Sub New(expression As String, returnType As Type)
		Me.Expression = expression
		Me.ReturnType = returnType
	End Sub

	
	Public Structure Variable
		Public Property Value As Object
		Public Property Type As String
	End Structure
	Public Structure EvaluationResult
		Public Property Result As Object
		'Public Property Variables As SortedList(Of String, Variable)
		Public Property ThrewException As Boolean
		Public Property Exception As Exception
		Public Property GeneratedCode As String
	End Structure

	Public Sub AddVariable(name As String, value As Object, type As String)
		If Me.Variables Is Nothing Then Me.Variables = New SortedList(Of String, Variable)
		Dim var As New Variable
		var.Value = value
		var.Type = type
		Me.Variables.Add(name, var)
	End Sub

	Public Sub AddMethod(name As String, content As String)
		If Me.Methods Is Nothing Then Me.Methods = New SortedList(Of String, String)
		Me.Methods.Add(name, content)
	End Sub

	Public Sub AddReference(ref As String)
		If Me.References Is Nothing Then Me.References = New SortedList(Of String, Object)
		Me.References.Add(ref, Nothing)
	End Sub
	Public Function Eval() As EvaluationResult
		Const className As String = "CustomEvaluator"
		Dim classContent As New StringBuilder
		classContent.AppendLine("Imports System")
		classContent.AppendLine("Imports System.Collections.Generic")
		classContent.AppendLine("Imports Common = QueryBuilder.Common")
		classContent.AppendLine(String.Format("Class {0}", className))
		Dim args As New List(Of String)
		If Me.Variables IsNot Nothing AndAlso Me.Variables.Count > 0 Then
			args.AddRange(Me.Variables.Select(Function(p) String.Format("ByVal {0} as {1}", p.Key, p.Value.Type)))
		End If
		classContent.AppendLine(String.Format("Public Delegate Function EvalDelegate({0}) as {1}", String.Join(",", args), Me.ReturnType.FullName))

		classContent.AppendLine(String.Format("Public Shared Function Eval({0}) as {1}", String.Join(",", args), Me.ReturnType.FullName))
		classContent.AppendLine(String.Format("Return {0}", Me.Expression))
		classContent.AppendLine("End Function")

		If Me.Methods IsNot Nothing AndAlso Me.Methods.Count > 0 Then
			For Each item In Me.Methods
				classContent.AppendLine(item.Value)
			Next
		End If
		classContent.AppendLine("End Class")
		Dim classContentStr As String = classContent.ToString()
		If Not _methodLookup.ContainsKey(classContentStr) Then
			Using provider As New VBCodeProvider
				Dim options As New CompilerParameters
				If Me.References IsNot Nothing AndAlso Me.References.Count > 0 Then
					options.ReferencedAssemblies.AddRange(Me.References.Select(Function(p) p.Key).ToArray())
				End If
				options.ReferencedAssemblies.Add(GetType(QueryBuilder.Common).Assembly.Location)
				options.CompilerOptions = "/t:library"
				options.GenerateInMemory = True
				options.IncludeDebugInformation = False
				options.TreatWarningsAsErrors = False
				Dim compileResults = provider.CompileAssemblyFromSource(options, classContentStr)
				If compileResults.Errors.HasErrors Then
					Dim errResult As New EvaluationResult
					errResult.ThrewException = True
					Dim exceptionMsg As New StringBuilder
					For Each err As CompilerError In compileResults.Errors
						exceptionMsg.AppendLine(String.Format("\tLine {0}: {1}", err.Line.ToString(), err.ErrorText))
					Next
					errResult.Exception = New Exception(exceptionMsg.ToString())
					errResult.GeneratedCode = classContentStr
					Return errResult
				End If
				Dim evalClass As Type = compileResults.CompiledAssembly.GetType(className)
				Dim delegateType As Type = evalClass.GetNestedType("EvalDelegate")
				Dim method As [Delegate] = [Delegate].CreateDelegate(delegateType, evalClass.GetMethod("Eval"), True)
				_methodLookup(classContentStr) = method
			End Using
		End If
		Try
			'Dim variableValues = Nothing
			'If Me.Variables IsNot Nothing Then
			'	variableValues = Me.Variables.Values.Select(Function(p) p.Value).ToArray()
			'End If
			Dim variableValues() As Object
			If Me.Variables IsNot Nothing Then
				variableValues = New Object(Me.Variables.Count - 1) {}
				Dim index As Integer = 0
				For Each v As Variable In Me.Variables.Values
					variableValues(index) = v.Value
					index += 1
				Next
			End If
			'Dim variableValues1() As Object
			'variableValues1 = New Object() {20}
			Dim expressionResult = _methodLookup(classContentStr).DynamicInvoke(variableValues)
			Return New EvaluationResult() With {.Result = expressionResult, .GeneratedCode = classContentStr}
		Catch ex As Exception
			Return New EvaluationResult() With {.ThrewException = True, .Exception = ex, .GeneratedCode = classContentStr}
		End Try
	End Function
End Class