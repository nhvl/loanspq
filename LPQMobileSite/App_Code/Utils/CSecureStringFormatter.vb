﻿Imports System.Text.RegularExpressions
Imports System.Configuration.ConfigurationManager
Imports System.Xml
''' <summary>
''' This class provides a means to quickly mask out sensitive data inside strings/xml documents.  It's not designed to be super efficient
''' as it does rely heavily on regex and brute force checking.  .
''' </summary>	
Public Class CSecureStringFormatter
	Private Shared log As log4net.ILog = log4net.LogManager.GetLogger(GetType(CSecureStringFormatter))

	Private Const ENCRYPTED_PW As String = "xxxxx"

	''' <summary>
	''' Masks sensitive data using the RegEx defined in the SENSITIVE_DATA_REGEX app setting.
	''' </summary>
	''' <param name="psData"></param>
	''' <returns>The input string with sensitive data masked with 'x' characters.</returns>
	''' <remarks>Note if the regex 'sensitive' group only captures 5 char, then only the first 5 characters are masked out!</remarks>
	Public Shared Function MaskSensitiveData(ByVal psData As String) As String
		Dim oRegString As String = "(?&lt;prefix&gt;(api_password|password|pw|pwd|pass|pass_word)\s*[:=]\s*)(?&lt;sensitive&gt;[^;\r\n]{0,5})"
		If AppSettings("SENSITIVE_DATA_REGEX") <> "" Then
			oRegString = AppSettings("SENSITIVE_DATA_REGEX")
		End If

		Dim oMatches As MatchCollection = Regex.Matches(psData, AppSettings("SENSITIVE_DATA_REGEX"), RegexOptions.IgnoreCase Or RegexOptions.ExplicitCapture)

		For Each oMatch As Match In oMatches
			psData = psData.Replace(oMatch.Value, oMatch.Groups("prefix").Value & ENCRYPTED_PW)	'New String("x"c, oMatch.Groups("sensitive").Value.Length))
		Next

		Return psData
	End Function

	''' <summary>
	''' Masks sensitive XML data from nodes or attributes which match the names in the SENSITIVE_DATA_XML_NAMES app setting. 
	''' This function is private so that we don't support taking in a DOM document to avoid potentially creating any side effects.
	''' The routine internally does modify the DOM.
	''' </summary>
	''' <param name="psXMLData">Note we don't support taking in a DOM document since we don't want to potentially create any side affects.  The routine internally 
	''' does modify the DOM.</param>
	''' <returns>If no error, the XML data with the sensitive information masked with 'x' characters.  Else, the input string.</returns>
	''' <remarks>Note the search here is not case insensitive.</remarks>
	Public Shared Function MaskSensitiveXMLData(ByVal psXMLData As String) As String
		If Trim(psXMLData) = "" Then
			Return psXMLData
		End If

		Dim doc As XDocument = XDocument.Parse(psXMLData)
		Dim oRegSXML As String = "api_password,password,pw,pwd,password_lender_level,pass,pass_word,LoginAccountPassword,ExpPassword,TucPassword,EqfSecuritycode"
		If AppSettings("SENSITIVE_DATA_XML_NAMES") <> "" Then
			oRegSXML = AppSettings("SENSITIVE_DATA_XML_NAMES")
		End If

		Dim sensitive_names As String() = oRegSXML.Split(","c)

		Dim sensitiveElements As IEnumerable(Of XElement) = From ele As XElement In doc.Descendants Where sensitive_names.Contains(ele.Name.ToString, StringComparer.InvariantCultureIgnoreCase)
		For Each ele As XElement In sensitiveElements
			If ele.Descendants.Count = 0 Then 'Don't want to replace nested nodes
				ele.Value = ENCRYPTED_PW
			End If
		Next

		Dim sensitiveAttributes As IEnumerable(Of XAttribute) = From attr As XAttribute In doc.Descendants.Attributes Where sensitive_names.Contains(attr.Name.ToString, StringComparer.InvariantCultureIgnoreCase)
		For Each attr As XAttribute In sensitiveAttributes
			attr.Value = ENCRYPTED_PW
		Next

		Dim sensitiveCData As IEnumerable(Of XNode) = From ele As XNode In doc.DescendantNodes Where ele.NodeType = XmlNodeType.CDATA Select ele
		For Each node As XNode In sensitiveCData
			Try
				' Recursive call, 
				' try to mask CDATA as though it were an xml document since 
				' that seems to be a common usage of CDATA 
				Dim oDoc As XDocument = XDocument.Parse(CType(node, XCData).Value)
				CType(node, XCData).Value = MaskSensitiveXMLData(oDoc.ToString)
			Catch ex As Exception
				' potentially expensive especially if CDATA is large + multiple replacements
				CType(node, XCData).Value = MaskSensitiveData(CType(node, XCData).Value)
			End Try
		Next

		Return doc.ToString(SaveOptions.DisableFormatting)


	End Function

End Class
