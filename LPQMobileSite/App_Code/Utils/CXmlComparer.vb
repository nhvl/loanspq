﻿Imports System.IO
Imports Microsoft.VisualBasic
Imports System.Xml
Imports Microsoft.XmlDiffPatch
Imports System.Web
Imports System.Text
Imports System.Text.RegularExpressions

Public Class CXmlComparer

	Private Shared ReadOnly _log As log4net.ILog = log4net.LogManager.GetLogger(GetType(CXmlComparer))

	Private _source As String
	Private _dest As String
	Public ReadOnly Property Source As String
		Get
			Return _source
		End Get
	End Property

	Public ReadOnly Property Destination As String
		Get
			Return _dest
		End Get
	End Property

	Public Sub New(source As String, destination As String)
		_source = source
		_dest = destination
		Dim trimStartCDataNewLineRegex As New Regex("<!\[CDATA\[\n*\r*\s*")
		_source = trimStartCDataNewLineRegex.Replace(_source, "<![CDATA[")
		_dest = trimStartCDataNewLineRegex.Replace(_dest, "<![CDATA[")
		Dim trimEndCDataNewLine As New Regex("[\n\r]*\s*\]\]>")
		_source = trimEndCDataNewLine.Replace(_source, "]]>")
		_dest = trimEndCDataNewLine.Replace(_dest, "]]>")

		'Due to auto format done by CKEditor, do in-memory cleanup so xml compare doesn't give false alarm
		Dim reformatBRTag As New Regex("[\n\r]*\s*\<\s*\/*\\*\s*br\s*\/*\s*\>[\n\r]*\s*")
		_source = reformatBRTag.Replace(_source, "<br/>")
		_dest = reformatBRTag.Replace(_dest, "<br/>")
		Dim reformatEncodedBRTag As New Regex("[\n\r]*\s*&lt;\s*\/*\\*\s*br\s*\/*\s*&gt;[\n\r]*\s*")
		_source = reformatEncodedBRTag.Replace(_source, "*&lt;br/&gt;")
		_dest = reformatEncodedBRTag.Replace(_dest, "*&lt;br/&gt;")
		_dest = _dest.Replace("&#xA;", "").Replace("&amp;quot;", "&quot;")
		_source = _source.Replace("&#xA;", "").Replace("&amp;quot;", "&quot;")
		_source = _source.Replace("&#xA;", "").Replace("&amp;quot;", "&quot;").Replace("&#xD;", "")
		_dest = _dest.Replace("&#xA;", "").Replace("&amp;quot;", "&quot;").Replace("&#xD;", "")
		Dim trimBeforeEndTag As New Regex("\s*\>")
		Dim trimBeforeEndTag2 As New Regex("\s*\/\>")
		_source = trimBeforeEndTag.Replace(_source, ">")
		_source = trimBeforeEndTag2.Replace(_source, "/>")
		_dest = trimBeforeEndTag.Replace(_dest, ">")
		_dest = trimBeforeEndTag2.Replace(_dest, "/>")
		'Dim trimBeforeTargetAttribute As New Regex("\s*target=")
		'_source = trimBeforeTargetAttribute.Replace(_source, " target=")
		'_dest = trimBeforeTargetAttribute.Replace(_dest, " target=")

		'replace double quote with single quotes
		Dim replaceDoubleQuoteWithSingle As New Regex("\s*target=[""_blank""|""newwin""]+")
		_source = replaceDoubleQuoteWithSingle.Replace(_source, " target='_blank'")
		_dest = replaceDoubleQuoteWithSingle.Replace(_dest, " target='_blank'")
		'_source = _source.Replace("target='_blank'", "target=""_blank""").Replace("target='newwin'", "target=""newwin""")
		'_dest = _dest.Replace("target='_blank'", "target=""_blank""").Replace("target='newwin'", "target=""newwin""")
		'_source = _source.Replace("ref='", "ref=""").Replace(".pdf'", ".pdf""")
		'_dest = _dest.Replace("ref='", "ref=""").Replace(".pdf'", ".pdf""")

		'_log.Debug("source: " & _source)
		'_log.Debug("_dest: " & _dest)

	End Sub

	Private Function eval(ByVal m As Match) As String
		Return m.ToString.ToUpper()
	End Function

	''' <summary>
	''' There is a bug in Microsoft.XmlDiffPatch where if an attribute has the value " " (just whitespace, nothing else)
	''' throws an error. This function will trim the attributes with just whitespace to stop the error from occurring.
	''' 
	''' If an attribute begins or ends with whitespace but has text in it, this will not modify those, in order to keep
	''' the audit log as similar to the real data as possible.
	''' </summary>
	''' <param name="xEl"></param>
	Private Sub TrimAttributes(xEl As XmlElement)
		If xEl Is Nothing Then Return

		For Each xAttr As XmlAttribute In xEl.Attributes
			If xAttr.Value IsNot Nothing Then
				Dim sTrimmed = xAttr.Value.Trim()
				' Only replace if the value was just whitespace.
				If sTrimmed = "" Then xAttr.Value = xAttr.Value.Trim()
			End If
		Next

		For Each xSubEl As XmlNode In xEl.ChildNodes
			If TypeOf xSubEl Is XmlElement Then
				TrimAttributes(xSubEl)
			End If
		Next
	End Sub


	Public Function Compare(ByRef diffGramStr As String) As Boolean
		Dim xml1 As New XmlDocument()
		xml1.LoadXml(_source)
		TrimAttributes(xml1.DocumentElement)
		Dim xml2 As New XmlDocument()
		xml2.LoadXml(_dest)
		TrimAttributes(xml2.DocumentElement)

		Dim xmlDiff As New XmlDiff()
		xmlDiff.IgnoreComments = True
		xmlDiff.IgnoreChildOrder = True
		xmlDiff.IgnoreWhitespace = True

		Dim result As Boolean = False
		Dim sbWriter As New StringBuilder()
		Using sw As New StringWriter(sbWriter)
			Using xr As XmlWriter = XmlWriter.Create(sw)
				'handle case where config data is empty
				'If xml1.FirstChild.InnerXml = "" Or xml2.FirstChild.InnerXml = "" Then
				'	diffGramStr = "Config Data is null"
				'	Return True
				'End If

				'code may have comment at the top so remove these until the firstchild is WEBSITE
				Do Until xml1.FirstChild.LocalName.Contains("WEBSITE")
					xml1.RemoveChild(xml1.FirstChild)
				Loop
				Do Until xml2.FirstChild.LocalName.Contains("WEBSITE")
					xml2.RemoveChild(xml2.FirstChild)
				Loop
                Try
                    result = xmlDiff.Compare(xml1.FirstChild, xml2.FirstChild, xr)
                Catch ex As Exception  'TODO: not sure what the error is so skip this comparision for now
                    _log.Warn("xmlDiff.Compare error", ex)
                    result = True ''need this to skip parrent step
                End Try

            End Using
		End Using
		If result = False Then
			diffGramStr = sbWriter.ToString()
		End If
		Return result
	End Function

	Public Sub ParseDiffGram(diffGramStr As String, ByRef sbDiff As StringBuilder)
		Dim xdoc As XDocument = XDocument.Parse(diffGramStr)
		Dim xmlSrc As New XmlDocument()
		xmlSrc.LoadXml(_source)
		Dim xmlDest As New XmlDocument()
		xmlDest.LoadXml(_dest)

		'code may have comment at the top so remove these until the firstchild is WEBSITE
		Do Until xmlSrc.FirstChild.LocalName.Contains("WEBSITE")
			xmlSrc.RemoveChild(xmlSrc.FirstChild)
		Loop
		Do Until xmlDest.FirstChild.LocalName.Contains("WEBSITE")
			xmlDest.RemoveChild(xmlDest.FirstChild)
		Loop

		Dim changes As List(Of XElement) = xdoc.Descendants().Where(Function(d) d.Name.LocalName = "change" Or d.Name.LocalName = "add" Or d.Name.LocalName = "remove").ToList()
		For Each ele As XElement In changes
			If DetectSubtreeIsNoChildNode(ele) = False Then
				Dim ancestorIndexes As List(Of Integer) = ele.Ancestors().Where(Function(a) a.Name.LocalName = "node" Or a.Name.LocalName = "change").Reverse().Select(Function(a) CInt(a.Attribute("match").Value)).ToList()
				Dim ancestors As New List(Of XmlNode)
				Dim currentNode As XmlNode
				If ele.Name.LocalName = "change" Then
					currentNode = xmlSrc
					For Each ancestorIndex As Integer In ancestorIndexes
						ancestors.Add(currentNode.ChildNodes(ancestorIndex - 1))
						currentNode = currentNode.ChildNodes(ancestorIndex - 1)
					Next
					If ele.HasAttributes And ele.Attribute("match") IsNot Nothing And ele.Attribute("match").Value.StartsWith("@") Then
						Dim att As String = ele.Attribute("match").Value.Replace("@", "")
						Dim newValue As String = HttpUtility.HtmlEncode(ele.Value)
						Dim oldValue As String = HttpUtility.HtmlEncode(currentNode.Attributes.ItemOf(att).Value)
						If newValue.ToUpper() <> oldValue.ToUpper() Then
							sbDiff.AppendFormat("<p><span style='color: #c25808'>{0}</span>: <span style='color: #e0392d'>""{1}""</span> -> <span style='color: #24bd92'>""{2}""</span></p>", BuildNodePath(ancestors) & att, oldValue, newValue)
						End If
					Else
						Dim newValue As String = HttpUtility.HtmlEncode(ele.Value)
						Dim oldValue As String = HttpUtility.HtmlEncode(currentNode.InnerText)
						If newValue.ToUpper() <> oldValue.ToUpper() Then
							sbDiff.AppendFormat("<p><span style='color: #c25808'>{0}</span>: <span style='color: #e0392d'>""{1}""</span> -> <span style='color: #24bd92'>""{2}""</span></p>", BuildNodePath(ancestors), oldValue, newValue)
						End If
					End If
				ElseIf ele.Name.LocalName = "add" Then
					currentNode = xmlSrc
					For Each ancestorIndex As Integer In ancestorIndexes
						ancestors.Add(currentNode.ChildNodes(ancestorIndex - 1))
						currentNode = currentNode.ChildNodes(ancestorIndex - 1)
					Next
					If ele.HasAttributes = True And ele.Attribute("name") IsNot Nothing Then 'add attributes
						sbDiff.AppendFormat("<p>Add attribute <span style='color: #24bd92'>""{0}={1}""</span> to <span style='color: #c25808'>{2}</span></p>", ele.Attribute("name").Value, HttpUtility.HtmlEncode(ele.Value), BuildNodePath(ancestors))
					ElseIf ele.HasAttributes And ele.Attribute("match") IsNot Nothing And ele.Attribute("opid") IsNot Nothing Then 'abort move action
					Else 'add child nodes
						If ele.HasElements = False Then	'text node
							sbDiff.AppendFormat("<p>Add text <span style='color: #24bd92'>""{1}""</span> to <span style='color: #c25808'>{0}</span></p>", BuildNodePath(ancestors), ele.Value)
						Else
							If ele.Nodes().Any() Then
								For Each nod In ele.Nodes()
									sbDiff.AppendFormat("<p>Add node <span style='color: #24bd92'>""{1}""</span> to <span style='color: #c25808'>{0}</span></p>", BuildNodePath(ancestors), HttpUtility.HtmlEncode(nod))
								Next
							End If

						End If
					End If
				ElseIf ele.Name.LocalName = "remove" Then
					currentNode = xmlSrc
					For Each ancestorIndex As Integer In ancestorIndexes
						ancestors.Add(currentNode.ChildNodes(ancestorIndex - 1))
						currentNode = currentNode.ChildNodes(ancestorIndex - 1)
					Next
					If ele.HasAttributes And ele.Attribute("match") IsNot Nothing And ele.Attribute("match").Value.StartsWith("@") = True Then	'remove attributes
						Dim removedValue As String = currentNode.Attributes.ItemOf(ele.Attribute("match").Value.Replace("@", "")).Value
						sbDiff.AppendFormat("<p>Remove attibute <span style='color: #e0392d'>""{0}={1}""</span> from <span style='color: #c25808'>{2}</span></p>", ele.Attribute("match").Value.Replace("@", ""), removedValue, BuildNodePath(ancestors))
					ElseIf ele.HasAttributes And ele.Attribute("match") IsNot Nothing And ele.Attribute("opid") IsNot Nothing Then 'abort move action
					Else 'remove nodes
						'ancestors.Add(currentNode.ChildNodes(CInt(ele.Attribute("match").Value) - 1))
						Dim regEx As New Regex("([0-9]+)", RegexOptions.IgnoreCase)
						For Each m As Match In regEx.Matches(ele.Attribute("match").Value)
							sbDiff.AppendFormat("<p>Remove node <span style='color: #e0392d'>""{0}""</span> from <span style='color: #c25808'>{1}</span></p>", HttpUtility.HtmlEncode(currentNode.ChildNodes(CInt(m.Value) - 1).OuterXml), BuildNodePath(ancestors))
						Next
					End If
				End If
			End If
		Next
	End Sub

	Public Function DetectSubtreeIsNoChildNode(node As XElement) As Boolean
		Dim ancestorNode As List(Of XElement)
		ancestorNode = node.Ancestors().ToList()
		For Each parentNode As XElement In ancestorNode
			If parentNode.HasAttributes And parentNode.Attribute("subtree") = "no" And parentNode.Name.LocalName = "remove" Then
				Return True
			End If
		Next
		Return False
	End Function

	Function BuildNodePath(nodeList As List(Of XmlNode)) As String
		Dim sb As New StringBuilder()
		For Each node As XmlNode In nodeList
			sb.AppendFormat("{0}/", node.Name)
		Next
		Return sb.ToString()
	End Function
End Class
