﻿Imports System.IO
Imports Microsoft.VisualBasic
Imports System.Security.Cryptography
Imports LPQMobile.Utils
Imports System.Text

Public Class CRijndaelManaged

    Private _log As log4net.ILog = log4net.LogManager.GetLogger(Me.GetType)
    Private _key As String = String.Empty
	Private _salt As Byte()

	Public Sub New()
		Dim configData = Common.GetRijndaelManagedKey()
		_key = configData.Key
		_salt = configData.Value
		'Dim keyFile As String = CConfigFile.getConfig().RijndaelManagedKeyFileLocation
		'If File.Exists(keyFile) Then
		'	Dim reader As New StreamReader(keyFile)
		'	_key = reader.ReadLine()
		'	_salt = Encoding.ASCII.GetBytes(reader.ReadLine())
		'	reader.Close()
		'	reader.Dispose()
		'Else
		'	'Throw New Exception("RijndaelManaged Key File not found")
		'	'To reduce friction for dev env, we will use default, the path to this file should be in web.comfig
		'	'<ConfigFile RijndaelManagedKeyFileLocation = "C:\LPQMobileWebsiteConfig\rijndaelManagedKeyFile.txt" />
		'	_key = "NDKnO4Q4jIWuriELSOu5b5GSSc1"
		'	_salt = Encoding.ASCII.GetBytes("4Q4jIWuriELS")
		'	_log.Info("RijndaelManaged Key File not found so using default key and salt.")
		'End If
	End Sub

	''' <summary>
	''' Encrypts any string using the Rijndael algorithm.
	''' </summary>
	''' <param name="input">The string to encrypt.</param>
	''' <returns>A Base64 encrypted string.</returns>
	Public Function EncryptUtf8(input As String, Optional prefix As String = "") As String
		If String.IsNullOrWhiteSpace(_key) Then Return input
		Dim rijndaelCipher As New RijndaelManaged()
		Dim plainText As Byte() = Encoding.UTF8.GetBytes(input)
		Dim secretKey As New Rfc2898DeriveBytes(_key, _salt)

		Using encryptor As ICryptoTransform = rijndaelCipher.CreateEncryptor(secretKey.GetBytes(32), secretKey.GetBytes(16))
			Using memoryStream As New MemoryStream()
				Using cryptoStream As New CryptoStream(memoryStream, encryptor, CryptoStreamMode.Write)
					cryptoStream.Write(plainText, 0, plainText.Length)
					cryptoStream.FlushFinalBlock()
					Dim result As String = Convert.ToBase64String(memoryStream.ToArray())
					If Not String.IsNullOrWhiteSpace(prefix) Then
						Return prefix & result
					End If
					Return result
				End Using
			End Using
		End Using
	End Function

	''' <summary>
	''' Decrypts a previously encrypted string.
	''' </summary>
	''' <param name="input">The encrypted string to decrypt.</param>
	''' <returns>A decrypted string.</returns>
	Public Function DecryptUtf8(input As String, Optional prefix As String = "") As String
		If String.IsNullOrWhiteSpace(_key) Then Return input
		If Not String.IsNullOrWhiteSpace(prefix) Then
			input = input.Substring(prefix.Length)
		End If
		Dim rijndaelCipher As New RijndaelManaged()
		Dim encryptedData As Byte() = Convert.FromBase64String(input)
		Dim secretKey As New Rfc2898DeriveBytes(_key, _salt)

		Using decryptor As ICryptoTransform = rijndaelCipher.CreateDecryptor(secretKey.GetBytes(32), secretKey.GetBytes(16))
			Using memoryStream As New MemoryStream(encryptedData)
				Using cryptoStream As New CryptoStream(memoryStream, decryptor, CryptoStreamMode.Read)
					Dim plainText As Byte() = New Byte(encryptedData.Length - 1) {}
					Dim decryptedCount As Integer = cryptoStream.Read(plainText, 0, plainText.Length)
					Return Encoding.UTF8.GetString(plainText, 0, decryptedCount)
				End Using
			End Using
		End Using
	End Function

	''' <summary>
	''' Encrypts(UTF-16) any string using the Rijndael algorithm.
	''' </summary>
	''' <param name="input">The string to encrypt.</param>
	''' <returns>A Base64 encrypted string.</returns>
	''' DEPRECATED DUE TO PERFORMANCE AND PROBLEMATIC FOR DB ISSUE
	Public Function Encrypt(input As String, Optional prefix As String = "") As String
		If String.IsNullOrWhiteSpace(_key) Then Return input
		Dim rijndaelCipher As New RijndaelManaged()
		Dim plainText As Byte() = Encoding.Unicode.GetBytes(input)
		Dim secretKey As New Rfc2898DeriveBytes(_key, _salt)

		Using encryptor As ICryptoTransform = rijndaelCipher.CreateEncryptor(secretKey.GetBytes(32), secretKey.GetBytes(16))
			Using memoryStream As New MemoryStream()
				Using cryptoStream As New CryptoStream(memoryStream, encryptor, CryptoStreamMode.Write)
					cryptoStream.Write(plainText, 0, plainText.Length)
					cryptoStream.FlushFinalBlock()
					Dim result As String = Convert.ToBase64String(memoryStream.ToArray())
					If Not String.IsNullOrWhiteSpace(prefix) Then
						Return prefix & result
					End If
					Return result
				End Using
			End Using
		End Using
	End Function

	''' <summary>
	''' Decrypts a previously encrypted string(UTF-16).
	''' </summary>
	''' <param name="input">The encrypted string to decrypt.</param>
	''' <returns>A decrypted string.</returns>
	''' DEPRECATED DUE TO PERFORMANCE AND PROBLEMATIC FOR DB ISSUE.
	Public Function Decrypt(input As String, Optional prefix As String = "") As String
		If String.IsNullOrWhiteSpace(_key) Then Return input
		If Not String.IsNullOrWhiteSpace(prefix) Then
			input = input.Substring(prefix.Length)
		End If
		Dim rijndaelCipher As New RijndaelManaged()
		Dim encryptedData As Byte() = Convert.FromBase64String(input)
		Dim secretKey As New Rfc2898DeriveBytes(_key, _salt)

		Using decryptor As ICryptoTransform = rijndaelCipher.CreateDecryptor(secretKey.GetBytes(32), secretKey.GetBytes(16))
			Using memoryStream As New MemoryStream(encryptedData)
				Using cryptoStream As New CryptoStream(memoryStream, decryptor, CryptoStreamMode.Read)
					Dim plainText As Byte() = New Byte(encryptedData.Length - 1) {}
					Dim decryptedCount As Integer = cryptoStream.Read(plainText, 0, plainText.Length)
					Return Encoding.Unicode.GetString(plainText, 0, decryptedCount)
				End Using
			End Using
		End Using
	End Function
End Class
