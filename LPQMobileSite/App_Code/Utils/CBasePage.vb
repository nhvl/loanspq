﻿
Imports CustomApplicationScenarios
Imports CustomList
Imports LPQMobile.BO
Imports System.Net
Imports Newtonsoft.Json
Imports Microsoft.VisualBasic
Imports LPQMobile.Utils
Imports DBUtils
Imports System.Web.Script.Serialization
Imports System.Security.Cryptography.X509Certificates
Imports System.Net.Security
Imports System.Xml
Imports System.Web.Security.AntiXss
Imports System.Web
Imports System.Text
Imports System.Text.RegularExpressions
Imports System.Configuration

Public Class CBasePage
	Inherits System.Web.UI.Page

	Private _log As log4net.ILog = log4net.LogManager.GetLogger(Me.GetType)
	Public Overridable ReadOnly Property log() As log4net.ILog
		Get
			Return _log
		End Get
	End Property

	'TODO: deprecate soon
#Region "Pbunyan"
	'Protected PBLogger As New PaulBunyan.PBLogger

	'Sub New()
	'	PBLogger.Context = "LPQMOBILE"
	'End Sub

	'Protected Sub logError(ByVal pErrorMessage As String, ByVal pErrorException As Exception)
	'	PBLogger.Log("ERROR", pErrorMessage & pErrorException.ToString)
	'End Sub


	'Protected Sub _log.info(ByVal pMsg As String)
	'	PBLogger.Log("INFO", pMsg)
	'End Sub

	Protected PBLogger As PBLogger = CPBLogger.PBLogger

	Sub New()
		PBLogger.Context = "LPQMOBILE"
	End Sub

	Protected Sub logError(ByVal pErrorMessage As String, ByVal pErrorException As Exception)
		log.Error(pErrorMessage, pErrorException)
	End Sub

	Protected Sub loginfo(ByVal pMsg As String)
		log.Info(pMsg)
	End Sub

#End Region

	Public _isDisagreePopup As String = "N"

	Public _textAndroidTel As String = "text"
	Public _CurrentUserAgent As String = ""
	Public _CurrentIP As String
	Public _CurrentHost As String
	Public _StateDropdown As String
	Public _Logo As String
	Public _MFAEmailOnly As Boolean
	Public _LogoUrl As String
	Public _OtherLogo As String
	Public _RedirectURL As String
	Public _RedirectXAURL As String
	Public _CurrentLenderRef As String
	Public _CurrentLenderId As String
	Public _CurrentBranchId As String
	Public _XAComboBranchId As String = ""
	Public _HideXAFOM As Boolean = False
	Public _CurrentOrgId As String
	Public _CurrentLoanOfficerId As String
	Public _ReferralSource As String
	Public _CurrentAPIUserID As String = ""
	Public _CurrentWebsiteConfig As CWebsiteConfig
	Public _HeaderDataTheme As String
	Public _HeaderDataTheme2 As String
	Public _FooterDataTheme As String
	Public _FooterFontDataTheme As String
	Public _ContentDataTheme As String
	'Public _ReviewDataTheme As String
	Public _BackgroundTheme As String
	Public _ButtonTheme As String
	Public _ButtonFontTheme As String
	Public _DefaultCitizenship As String
	Public LPQMOBILE_CONNECTIONSTRING As String = System.Web.Configuration.WebConfigurationManager.ConnectionStrings("LPQMOBILE_CONNECTIONSTRING").ConnectionString
	''div footer content
	Public _strRenderFooterContact As String
	Public _headerMessage As String
	Public _strFooterLeft As String
	Public _strFooterRight As String
	Public _strLenderName As String
	Public _strNCUAUrl As String
	Public _strHUDUrl As String
	Public _strLogoUrl As String
	Public _hasFooterRight As String = ""
	Public _equalHousingText As String = ""
	''_nIPCount within _nHourSpan hour 
	Const _nHourSpan As Integer = 1	 'time spa
	Public _nIPCount As Integer = 15
	''-----
	Public _SelectProduct As String
	Public _isReApproved As String
	''---
	Public _token As String

	Private lenderName As String
	Public _lenderPhone As String
	Public _InstitutionType As CEnum.InstitutionType
	Public IsSSO As Boolean	' single sign-on use by callback page
	Public IsComboMode As Boolean ' creat both loan and xa for new member
	Public _LoanType As String
	Public _LoanTypeOther As String	 ' this is sub category of loantype such as minor, merchant
	Public _LoanNodeName As String
	Public Property mappedShowFieldLoanType As String
	Public _ProceedXAAnyway As Boolean = False ' proceed with xa eventhough consumer doesn't qualify for loan

	Public Is2ndLoanApp As Boolean = False

	''occupation 
	Public _occupationList As String = ""
	Public _requiredDLScan As String = ""
	Public _requiredDLScanXA As String = ""
	Public _requiredUploadDocs As String = ""
	Public _requiredUpLoadDocsXA As String = ""
	Public _previous_address_threshold As String = ""
	Public _previous_employment_threshold As String = ""
	Protected _isForeignPhone As String
	Protected _isForeignAddress As String
	Protected _isEmploymentDurationRequired As String = ""
	Protected _isHomephoneRequired As String = ""
	Protected _requireMemberNumber As String = ""
	'Public _branchNameDropdown As String = ""
	Public _requiredBranch As String = ""
	Public _comboEligibilityHeader As String = ""
	Public _comboFundingHeader As String = ""
	Public _xaContinueWhenLoanIsReferred As String = ""
	Public _xaBeneficiaryLevel As String = ""

	'**merchant/clinic mode
	Public _MerchantID As String = ""
	Public _ClinicID As String = ""
	Public _WorkerID As String = ""
	Public _MerchantName As String = ""

	''new api for customquestions
	Public _isCQNewAPI As Boolean = False
	Public _showFieldList As Dictionary(Of String, String)
	Public _requireFieldList As Dictionary(Of String, String)

	''membership fees
	Protected _MembershipFee As Decimal = 0D

	'the actual post back amount is done on server side for security reasons
	Protected _customListFomFees As Dictionary(Of String, String)
	Protected _locationPool As Boolean = False
	Protected _noAvailableProductMessage As String = ""
	Protected sso_ida_enable As Boolean = False
	Protected xa_sso_ida_enable As Boolean = False
	Protected ReadOnly Property _LenderName() As String
		Get
			If lenderName.ToLower().EndsWith("s") Then
				Return lenderName & "'"
			Else
				Return lenderName & "'s"
			End If
		End Get
	End Property

	'Ex:DI, 
	Public ReadOnly Property PlatformSource() As String
		Get
			Return Common.SafeString(AntiXssEncoder.HtmlEncode(Request.Params("platform_source"), True))
		End Get
	End Property

	Private Function ValidateServerCertificate(ByVal sender As Object, ByVal certificate As X509Certificate, ByVal chain As X509Chain, ByVal sslPolicyErrors As SslPolicyErrors) As Boolean
		Return True
	End Function

	Private _customListRuleList As List(Of CCustomListRuleItem)

	Public ReadOnly Property CustomListRuleList() As List(Of CCustomListRuleItem)
		Get
			Return _customListRuleList
		End Get
	End Property
	Private _nssList As Dictionary(Of String, List(Of String))

	Public ReadOnly Property NSSList() As Dictionary(Of String, List(Of String))
		Get
			Return _nssList
		End Get
	End Property

	Private _xaCustomListRuleList As List(Of CCustomListRuleItem)

	'This is used in combo mode scenarios when we may need to evaluate XA Custom List Rules also
	Public ReadOnly Property XACustomListRuleList() As List(Of CCustomListRuleItem)
		Get
			Return _xaCustomListRuleList
		End Get
	End Property

	Private _collectDescriptionIfEmploymentStatusIsOther As String = ""
	Public ReadOnly Property CollectDescriptionIfEmploymentStatusIsOther() As String
		Get
			Return _collectDescriptionIfEmploymentStatusIsOther
		End Get
	End Property

	Private _collectDescriptionIfOccupancyStatusIsOther As String = ""
	Public ReadOnly Property CollectDescriptionIfOccupancyStatusIsOther() As String
		Get
			Return _collectDescriptionIfOccupancyStatusIsOther
		End Get
	End Property

	Public ReadOnly Property PreviewCode() As String
		Get
			Return Common.SafeString(Request.Params("previewcode"))
		End Get
	End Property

	Public ReadOnly Property TextAndroidTel As String
		Get
			If String.IsNullOrEmpty(_textAndroidTel) Then
				Dim useragent As String = If(Request.ServerVariables("HTTP_USER_AGENT"), "")
				_textAndroidTel = If(useragent.ToLower.Contains("android"), "tel", "text")
			End If
			Return _textAndroidTel
		End Get
	End Property

	Public ReadOnly Property IsInEnvironment(environmentName As String) As Boolean
		Get
			Return ConfigurationManager.AppSettings.Get("ENVIRONMENT") = environmentName
		End Get
	End Property


	Public ReadOnly Property IsFinishLaterEnabled() As Boolean
		Get
			Return Common.SafeGUID(Request.QueryString("vendorid")) <> Guid.Empty
		End Get
	End Property

	Protected ReadOnly Property EnableEmploymentSection(Optional applicantType As String = "") As Boolean
		Get
			If CheckShowField("divEmploymentSection", applicantType, Not applicantType = "m_") Or (IsInMode("777") AndAlso IsInFeature("visibility")) Then
				Return True
			End If
			Return False
		End Get
	End Property

	Protected ReadOnly Property EnableIDSection(Optional applicantType As String = "") As Boolean
		Get
			Dim bDefaultVisibility = IsComboMode Or _LoanType = "XA" Or _LoanType = "SA"
			If applicantType = "m_" Then bDefaultVisibility = False
			If CheckShowField("divApplicantIdentification", applicantType, bDefaultVisibility) Or (IsInMode("777") AndAlso IsInFeature("visibility")) Then
				Return True
			End If
			Return False
		End Get
	End Property

	Protected ReadOnly Property EnableOccupancyStatusSection(Optional applicantType As String = "") As Boolean
		Get
			Dim bDefaultVisibility = Not (_LoanType = "XA" Or _LoanType = "SA")
			If CheckShowField("divOccupancyStatusSection", applicantType, bDefaultVisibility) Or (IsInMode("777") AndAlso IsInFeature("visibility")) Then
				Return True
			End If
			Return False
		End Get
	End Property

	Protected ReadOnly Property EnableMailingAddressSection(Optional applicantType As String = "") As Boolean
		Get
			If CheckShowField("divMailingAddressSection", applicantType, True) Or (IsInMode("777") AndAlso IsInFeature("visibility")) Then
				Return True
			End If
			Return False
		End Get
	End Property
	Protected ReadOnly Property EnableAssetSection(Optional applicantType As String = "") As Boolean
		Get
			If CheckShowField("divAssetSection", applicantType, False) Or (IsInMode("777") AndAlso IsInFeature("visibility")) Then
				Return True
			End If
			Return False
		End Get
	End Property

	Protected ReadOnly Property EnableDisclosureSection As Boolean
		Get
			If CheckShowField("divDisclosureSection", "", True) Or (IsInMode("777") AndAlso IsInFeature("visibility")) Then
				Return True
			End If
			Return False
		End Get
	End Property

	Protected ReadOnly Property EnableBranchSection As Boolean
		Get
			If CheckShowField("divBranchSection", "", False) Or (IsInMode("777") AndAlso IsInFeature("visibility")) Then
				Return True
			End If
			Return False
		End Get
	End Property

	Protected ReadOnly Property EnablePrimaryAppSpouseContactInfo As Boolean
		Get
			If CheckShowField("divNSSContactInfo", "AppSpouseInfo", False) Or (IsInMode("777") AndAlso IsInFeature("visibility")) Then
				Return True
			End If
			Return False
		End Get
	End Property
	Protected ReadOnly Property EnableCoAppSpouseContactInfo As Boolean
		Get
			If CheckShowField("co_divNSSContactInfo", "AppSpouseInfo", False) Or (IsInMode("777") AndAlso IsInFeature("visibility")) Then
				Return True
			End If
			Return False
		End Get
	End Property

	Protected ReadOnly Property RequireBranchField As Boolean
		Get
			If CheckRequireField("divBranchField", "", True) Then
				Return True
			End If
			Return False
		End Get
	End Property

	'"N" for all Essential, null or yes for PLus ans legacy
	Private _HomeBankingSSOEnable As Boolean = True
	Protected ReadOnly Property HomeBankingSSOEnable() As Boolean
		Get
			Return _HomeBankingSSOEnable
		End Get
	End Property

	Private Sub LoadGlobalConfigs()

		Dim lenderID As Guid = Guid.Empty

		_CurrentLenderRef = AntiXssEncoder.HtmlEncode(Request.Params("lenderref"), True)
		Dim sLenderId = AntiXssEncoder.HtmlEncode(Request.Params("lenderid"), True)
		If String.IsNullOrEmpty(sLenderId) Then
			sLenderId = AntiXssEncoder.HtmlEncode(Request.Params("lender_id"), True)
		End If

		If Not String.IsNullOrEmpty(_CurrentLenderRef) Then
			Dim oLender As CLenderConfig = Common.GetLenderConfigByLenderRef(_CurrentLenderRef)
			If oLender IsNot Nothing Then
				lenderID = oLender.LenderId
			End If
		ElseIf Not String.IsNullOrEmpty(sLenderId) Then
			Dim oLender As CLenderConfig = Common.GetLenderConfigByLenderRef(sLenderId)	'url parameter => ?lenderid=lenderref or ?lender_id=lenderref
			If oLender IsNot Nothing Then
				lenderID = oLender.LenderId
			Else
				lenderID = Common.SafeGUID(sLenderId)  ''url parameter => ?lenderid= system lenderid 
			End If
		End If

		If lenderID <> Guid.Empty Then
			_CurrentLenderId = lenderID.ToString()
			Dim globalConfigsDic As Dictionary(Of String, String) = Common.ReadGlobalConfigItems(New List(Of String) From {
																								 GlobalConfigDictionary.GENERAL_COUNTRIES_ALLOWED_ACCESS,
																								 GlobalConfigDictionary.GENERAL_EXCLUDED_IP,
																								 GlobalConfigDictionary.GENERAL_INSTATOUCH,
																								 GlobalConfigDictionary.GENERAL_SUBMISSION
																							 }, lenderID)
			If Not String.IsNullOrEmpty(globalConfigsDic(GlobalConfigDictionary.GENERAL_EXCLUDED_IP)) Then
				Dim obj = New With {.ExcludedIPMessage = "", .ExcludedIPs = New List(Of CIPCommentsItem)}
				obj = JsonConvert.DeserializeAnonymousType(globalConfigsDic(GlobalConfigDictionary.GENERAL_EXCLUDED_IP), obj)
				If obj IsNot Nothing Then
					_ipAccessDenied = obj.ExcludedIPs
					_ipAccessDeniedMessage = obj.ExcludedIPMessage
				End If
			End If
			If Not String.IsNullOrEmpty(globalConfigsDic(GlobalConfigDictionary.GENERAL_COUNTRIES_ALLOWED_ACCESS)) Then
				_countriesAllowedAccess = JsonConvert.DeserializeObject(Of List(Of String))(globalConfigsDic(GlobalConfigDictionary.GENERAL_COUNTRIES_ALLOWED_ACCESS))
			Else
				_countriesAllowedAccess = Common.CountryList.Where(Function(p) p.Value.Status = 1).Select(Function(c) c.Value.Code2).ToList()	'default values
			End If

			_instaTouchRestrictionInternalEmails = New List(Of String)
			_instaTouchRestrictionMaxTimesBeforeBlocking = -1
			_instaTouchRestrictionMaxTimesBeforeNotification = -1
			_instaTouchRestrictionExemptIPs = New List(Of CIPCommentsItem)
			If Not String.IsNullOrEmpty(globalConfigsDic(GlobalConfigDictionary.GENERAL_INSTATOUCH)) Then
				Dim obj = New With {
					.InstaTouchInternalEmailAddresses = "",
					.InstaTouchRestrictionIPs = New List(Of CIPCommentsItem),
					.InstaTouchMaxTimesBeforeNotification = -1,
					.InstaTouchMaxTimesBeforeBlocking = -1
				} ' by default
				obj = JsonConvert.DeserializeAnonymousType(globalConfigsDic(GlobalConfigDictionary.GENERAL_INSTATOUCH), obj)
				If obj IsNot Nothing Then
					_instaTouchRestrictionInternalEmails = obj.InstaTouchInternalEmailAddresses.Split(";"c).Where(Function(p) Not String.IsNullOrWhiteSpace(p)).ToList()
					_instaTouchRestrictionMaxTimesBeforeBlocking = obj.InstaTouchMaxTimesBeforeBlocking
					_instaTouchRestrictionMaxTimesBeforeNotification = obj.InstaTouchMaxTimesBeforeNotification
					_instaTouchRestrictionExemptIPs = obj.InstaTouchRestrictionIPs
				End If
			End If

			_submissionLimiterInternalEmails = New List(Of String)
			If IsInEnvironment("LIVE") Then
				_submissionMaxAppBeforeBlocking = 25
			Else
				_submissionMaxAppBeforeBlocking = 250
			End If
			_submissionMaxAppBeforeNotification = -1
			_submissionLimiterExemptIPs = New List(Of CIPCommentsItem)
			If Not String.IsNullOrEmpty(globalConfigsDic(GlobalConfigDictionary.GENERAL_SUBMISSION)) Then
				Dim obj = New With {
					.InternalEmailAddresses = "",
					.SubmissionRateLimiterIPs = New List(Of CIPCommentsItem),
					.MaxAppBeforeBlocking = -1,
					.MaxAppBeforeNotification = -1
				} ' by default
				obj = JsonConvert.DeserializeAnonymousType(globalConfigsDic(GlobalConfigDictionary.GENERAL_SUBMISSION), obj)
				If obj IsNot Nothing Then
					_submissionLimiterInternalEmails = obj.InternalEmailAddresses.Split(";"c).Where(Function(p) Not String.IsNullOrWhiteSpace(p)).ToList()
					_submissionMaxAppBeforeBlocking = obj.MaxAppBeforeBlocking
					_submissionMaxAppBeforeNotification = obj.MaxAppBeforeNotification
					_submissionLimiterExemptIPs = obj.SubmissionRateLimiterIPs
				End If
			End If
		Else
			log.Info("This portal (" & _CurrentHost & ": " & _CurrentLenderRef & ") is not yet configured.  IP: " & _CurrentIP)
			Throw New ArgumentException("This portal (" & _CurrentHost & ": " & _CurrentLenderRef & ") is not yet configured.  IP:  " & _CurrentIP)
		End If
	End Sub



	Private _ipAccessDeniedMessage As String = ""
	Public ReadOnly Property IPAccessDeniedMessage As String
		Get
			Return _ipAccessDeniedMessage
		End Get
	End Property

	Private _ipAccessDenied As New List(Of CIPCommentsItem)
	Public ReadOnly Property IPAccessDenied As List(Of CIPCommentsItem)
		Get
			Return _ipAccessDenied
		End Get
	End Property

	Private _countriesAllowedAccess As New List(Of String)
	Public ReadOnly Property CountriesAllowedAccess As List(Of String)
		Get
			Return _countriesAllowedAccess
		End Get
	End Property

	Private _instaTouchRestrictionInternalEmails As New List(Of String)
	Public ReadOnly Property InstaTouchRestrictionInternalEmails As List(Of String)
		Get
			Return _instaTouchRestrictionInternalEmails
		End Get
	End Property

	Private _instaTouchRestrictionExemptIPs As New List(Of CIPCommentsItem)
	Public ReadOnly Property InstaTouchRestrictionExemptIPs As List(Of CIPCommentsItem)
		Get
			Return _instaTouchRestrictionExemptIPs
		End Get
	End Property

	Private _instaTouchRestrictionMaxTimesBeforeNotification As Integer
	Public ReadOnly Property InstaTouchRestrictionMaxTimesBeforeNotification As Integer
		Get
			Return _instaTouchRestrictionMaxTimesBeforeNotification
		End Get
	End Property
	Private _instaTouchRestrictionMaxTimesBeforeBlocking As Integer
	Public ReadOnly Property InstaTouchRestrictionMaxTimesBeforeBlocking As Integer
		Get
			Return _instaTouchRestrictionMaxTimesBeforeBlocking
		End Get
	End Property

	Private _submissionLimiterInternalEmails As New List(Of String)
	Public ReadOnly Property SubmissionLimiterInternalEmails As List(Of String)
		Get
			Return _submissionLimiterInternalEmails
		End Get
	End Property

	Private _submissionLimiterExemptIPs As New List(Of CIPCommentsItem)
	Public ReadOnly Property SubmissionLimiterExemptIPs As List(Of CIPCommentsItem)
		Get
			Return _submissionLimiterExemptIPs
		End Get
	End Property

	Private _submissionMaxAppBeforeNotification As Integer
	Public ReadOnly Property SubmissionMaxAppBeforeNotification As Integer
		Get
			Return _submissionMaxAppBeforeNotification
		End Get
	End Property
	Private _submissionMaxAppBeforeBlocking As Integer
	Public ReadOnly Property SubmissionMaxAppBeforeBlocking As Integer
		Get
			Return _submissionMaxAppBeforeBlocking
		End Get
	End Property

	''' <summary>
	''' Check Ip again list of block/blacklist IPs(ipList)
	''' </summary>
	''' <param name="ipList"></param>
	''' <param name="currentIP"></param>
	''' <returns>true when the ip is in EXCLUDED IP ADDRESSES list</returns>
	Private Function CheckIPAccessDenied(ipList As List(Of CIPCommentsItem), currentIP As String) As Boolean
		Return ipList IsNot Nothing AndAlso ipList.Any(Function(p)
																 If p.IP = currentIP Then Return True
																 If p.IP.EndsWith("*") Then	  '123.23.12.*
																	 Return currentIP.StartsWith(p.IP.Substring(0, p.IP.LastIndexOf("."c) + 1))
																 End If
																 Return False
															 End Function)
	End Function
	''' <summary>
	''' Check the IP again allowed/whitelist countries
	''' </summary>
	''' <param name="countryList"></param>
	''' <param name="currentIP"></param>
	''' <returns>True when the IP is in the allowed countries</returns>
	Private Function CheckCountriesAllowedAccess(countryList As List(Of String), currentIP As String) As Boolean
		If countryList Is Nothing OrElse countryList.Count = 0 Then Return False 'countryList is default to "US" if it never been set
		Dim sResult As String = ""
		Dim sIPLookupURL As String = String.Format("https://iplookup.loanspq.com/default.aspx?IP={0}", currentIP)
		Dim oRequest As System.Net.HttpWebRequest = CType(System.Net.WebRequest.Create(sIPLookupURL), System.Net.HttpWebRequest)
		oRequest.Method = "GET"
		Try
			_log.Info("Begin get request to: " & sIPLookupURL)
			Dim oResponse As System.Net.HttpWebResponse = CType(oRequest.GetResponse(), System.Net.HttpWebResponse)
			If oResponse.StatusCode = System.Net.HttpStatusCode.OK Then
				Using respStream As System.IO.Stream = oResponse.GetResponseStream
					Dim reader As System.IO.StreamReader = New System.IO.StreamReader(respStream, Encoding.UTF8)
					sResult = reader.ReadToEnd
					_log.DebugFormat("REST API response from {0}: {1}", sIPLookupURL, sResult)
				End Using
				If Not String.IsNullOrEmpty(sResult) Then
					Dim docXml As New XmlDocument
					docXml.LoadXml(sResult)
					Dim resultCodeNode As XmlNode = docXml.SelectSingleNode("RESULT/CODE")
					If resultCodeNode IsNot Nothing Then

						'blacklist region
						'Return (sResult.NullSafeToUpper_.IndexOf("NIGERIA") <> -1 OrElse sResult.NullSafeToUpper_.IndexOf("GHANA") <> -1 OrElse sResult.NullSafeToUpper_.IndexOf("TURKEY") <> -1)
						Dim countryBlackList As New List(Of String)({"NG", "GH", "TR"})
						If countryBlackList.Contains(resultCodeNode.InnerText.ToUpper()) Then Return False

						Return countryList.Contains(resultCodeNode.InnerText.ToUpper())
					End If
				End If
			Else
				_log.WarnFormat("Status Code: {0}, Status Description: {1}", oResponse.StatusCode, oResponse.StatusDescription)
			End If
		Catch ex As Exception '
			If ex.Message.Contains("500") Or ex.Message.Contains("404") Then
				_log.Debug("CheckCountriesAllowedAccess Error: " & ex.Message, ex)
			Else
				_log.Warn("CheckCountriesAllowedAccess Error(no 500 or 404): " & ex.Message, ex)
			End If
		End Try

		Return True	' by default, don't limit access by country
	End Function

	Private Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
		_CurrentIP = Request.UserHostAddress

		'For testing overwrite
		'_CurrentIP = "123.123.123.122"  'China
		'_CurrentIP = "12.106.86.230"  'US
		'_CurrentIP = "41.73.157.255"  'NIgeria
		_log.Debug("my ip: " & _CurrentIP)

		_CurrentHost = Request.Url.Host
		If _CurrentIP <> "::1" Then	''skip it for local
			LoadGlobalConfigs()
			'for testing
			'If IsInEnvironment("DEV") Then
			'   _CurrentIP = "98.254.172.17"
			'End If
			'!!!IMPORTANT, DO NOT REMOVE THIS LINE, IT CAUSE ENDLESS LOOP
			If Request.CurrentExecutionFilePath.ToLower().Contains("ipaccessdenied") Then Return
			'!!!END IMPORTANT
			If CheckIPAccessDenied(IPAccessDenied, _CurrentIP) Then
				Server.Transfer("/IPAccessDenied.aspx", True)
			ElseIf Not CheckCountriesAllowedAccess(CountriesAllowedAccess, _CurrentIP) Then
				Server.Transfer("/PageNotFound.aspx", False)
			End If
		End If
		IsSSO = (Request.Form("PlatformSource") <> "") Or Common.SafeString(Request.Params("FName")) <> "" Or Common.SafeString(Request.Params("LName")) <> "" 'TODO: need to do for case when using SSO direct
		If IsSSO Then Return 'going to the loading page will cause a loss of all the hidden value in post

		'This is just for fixing XSS
		'testcase: GET /he/HomeEquityLoan.aspx?lenderref=LPQPenTest&l=1xruep</script><script>alert(1)</script>go0j  -- this produces the 1
		If Request.QueryString("l").NullSafeToUpper_.Length > 1 Then
			Server.Transfer("~/GenericErrorPage.aspx")
		End If

		'TODO:need better implementation
		If Request.QueryString("l") = "" AndAlso (Request.Url.LocalPath.ToLower().Contains("personalloan.aspx") = True Or Request.Url.LocalPath.ToLower().Contains("creditcard.aspx") = True Or Request.Url.LocalPath.ToLower().Contains("homeequityloan.aspx") = True Or Request.Url.LocalPath.ToLower().Contains("vehicleloan.aspx") = True Or Request.Url.LocalPath.ToLower().Contains("xa/xpressapp.aspx") = True) Then	  'xa/xpressapp.aspx bc dont want accu/xpressapp.aspx
			Server.Transfer("~/PageLoading.aspx?target=" & HttpUtility.UrlEncode(Request.Url.ToString()))
		End If
	End Sub

	Private Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
		'ignore certificate so we don't have to deal with it when there is a certificate update on LPQ side
		System.Net.ServicePointManager.ServerCertificateValidationCallback = New System.Net.Security.RemoteCertificateValidationCallback(AddressOf ValidateServerCertificate)

		'callback page use Request.Form, main page  use Request.Params
		IsSSO = (Request.Form("PlatformSource") <> "") Or Common.SafeString(Request.Params("FName")) <> "" 'TODO: need to do for case when using SSO direct
		'comboMode only for loan modules, submit both loan and XA at the same time 
		If Request.Form("IsComboMode") <> "" Then
			IsComboMode = Request.Form("IsComboMode") = "Y"
		Else
			IsComboMode = Common.SafeString(Request.Params("type")) = "1" And Common.SafeString(Request.Url.AbsolutePath).ToUpper.IndexOf("XPRESSAPP") = -1
		End If
		'**Xsell mode
		If Request.Form("Is2ndLoanApp") = "Y" Then
			Is2ndLoanApp = True
		ElseIf Request.Params("type") = "3" Then
			''get sid url parameter and make sure the session has loan info
			''we use this loan info to submit second loan app
			Dim sSessionId = Common.SafeString(Request.Params("sid"))
			If Not String.IsNullOrWhiteSpace(sSessionId) AndAlso Session(sSessionId) IsNot Nothing Then
				Is2ndLoanApp = True
			End If
		End If
		'*************************************************************
		'****merchant mode 
		'direct, clinic(MERCHANTS/MERCHANT) configuration is NOT needed
		_ClinicID = AntiXssEncoder.HtmlEncode(Request.QueryString("clinicid"), True) 'AntiXssEncoder bc it will be store on client side
		_WorkerID = AntiXssEncoder.HtmlEncode(Request.QueryString("workerid"), True)

		'indirect, clinicid and workerid are defined in MERCHANTS/MERCHANT with merchant_id attribute
		_MerchantID = AntiXssEncoder.HtmlEncode(Request.QueryString("merchantid"), True)

		'if call back then use for,, 
		If Request.Form("MerchantId") <> "" Or Request.Form("ClinicId_enc") <> "" Then
			_ClinicID = Request.Form("ClinicID_enc")
			_WorkerID = Request.Form("WorkerId_enc")

			'via clinic configuraiton override
			_MerchantID = Request.Form("MerchantId")
		End If

		'***set _LoanType and _LoanTypeOther
		Dim sPath As String = Common.SafeString(Request.Url.AbsolutePath).ToUpper
		If sPath.Contains("/XA/") OrElse sPath.Contains("/ACCU/") OrElse sPath.Contains("/MLPAYPAL.ASPX") Then
			_LoanType = "XA"
			If Common.SafeString(Request.Params("type")).StartsWith("2") Then
				_LoanType = "SA" 'secondary
			End If
			If Common.SafeString(Request.Params("type")).ToLower = "1a" OrElse Common.SafeString(Request.Params("type")).ToLower = "2a" OrElse Common.SafeString(Request.Params("type")).ToLower = "1b" OrElse Common.SafeString(Request.Params("type")).ToLower = "2b" OrElse Common.SafeString(Request.Params("type")).ToLower = "1s" OrElse Common.SafeString(Request.Params("type")).ToLower = "2s" Then
				_LoanTypeOther = Common.SafeString(Request.Params("type")).ToLower	'special account or business account
			End If
			_LoanNodeName = "XA_LOAN"
		ElseIf sPath.Contains("/PL/") Then
			_LoanType = "PL"
			If _MerchantID <> "" Or _ClinicID <> "" Then
				_LoanTypeOther = "MERCHANT"	'deprecated, APM doesn't support so just use normal pl
			End If
			_LoanNodeName = "PERSONAL_LOAN"
		ElseIf sPath.Contains("/VL/") Then
			_LoanType = "VL"
			_LoanNodeName = "VEHICLE_LOAN"
		ElseIf sPath.Contains("/CC/") Then
			_LoanType = "CC"
			If _MerchantID <> "" Or _ClinicID <> "" Then
				_LoanTypeOther = "MERCHANT"	'deprecated
			End If
			_LoanNodeName = "CREDIT_CARD_LOAN"
		ElseIf sPath.Contains("/HE/") Then
			_LoanType = "HE"
			_LoanNodeName = "HOME_EQUITY_LOAN"
		ElseIf sPath.Contains("/BL/") Then
			_LoanType = "BL"
			_LoanNodeName = "BUSINESS_LOAN"
		ElseIf sPath.Contains("APPLY.ASPX") Then 'landing page
			_LoanType = "LP"
		ElseIf sPath.Contains("/CU/") Then	'status page
			_LoanType = "ST"
		Else
			_LoanType = ""
		End If

		If Request.ServerVariables("HTTP_USER_AGENT") IsNot Nothing Then
			_CurrentUserAgent = Request.ServerVariables("HTTP_USER_AGENT")
		End If

		''change input type to numeric so android will popup numeric key pad
		If _CurrentUserAgent.ToLower.Contains("android") Then
			_textAndroidTel = "tel"
		End If

		_StateDropdown = Common.RenderStateDropdownlistWithEmpty(CEnum.STATES, "", "--Please Select--")

		Dim sLenderId As String = AntiXssEncoder.HtmlEncode(Request.Params("lenderid"), True)

		'lender_id is used in services/xx/getToken and getConfigSite so it may not need to be here
		'for backward compatibility with V1, getToken needs to use lender_id
		If sLenderId = "" Then sLenderId = AntiXssEncoder.HtmlEncode(Request.Params("lender_id"), True)
		'If sLenderId <> "" Then sLenderId = sLenderId.Replace("-", "")

		Dim sLenderRef As String = AntiXssEncoder.HtmlEncode(Request.Params("lenderref"), True)
		'If sLenderRef <> "" Then sLenderRef = sLenderRef.Replace("-", "")
		'use Request.Params when cbase is call by html makup page, value is store in hiddenfield which will be called via Request.form
		_CurrentBranchId = Common.SafeString(AntiXssEncoder.HtmlEncode(Request.Params("branchid"), True))
		_DefaultCitizenship = AntiXssEncoder.HtmlEncode(Request.Params("citizenship"), True)
		_ReferralSource = Common.SafeString(AntiXssEncoder.HtmlEncode(Request.Params("ReferralSource"), True))
		_CurrentLoanOfficerId = Common.SafeString(AntiXssEncoder.HtmlEncode(Request.Params("LoanOfficerID"), True))
		Dim oConfig As CWebsiteConfig
		If (Not String.IsNullOrEmpty(sLenderId)) Then
			' If a lenderID is given, then use it (for backwards compatibility)
			' If not, then use the lender reference string instead
			oConfig = Common.GetCurrentWebsiteConfig(Request.Url.Host, sLenderId, "", psPreviewCode:=PreviewCode)
		Else
			' Tell the function to use the lenderRef instead
			oConfig = Common.GetCurrentWebsiteConfig(Request.Url.Host, Nothing, sLenderRef, psPreviewCode:=PreviewCode)

			'backward compatible, just incase client use lenderid as lenderref
			If oConfig Is Nothing Then
				oConfig = Common.GetCurrentWebsiteConfig(Request.Url.Host, sLenderRef, "", psPreviewCode:=PreviewCode)
			End If
		End If
		'Deprecated, ArgumentException is already throw in common.vb or preint:LoanGlobalConfig
		If oConfig Is Nothing Then
			log.Info("This host (" & _CurrentHost & ": " & sLenderRef & ") is not yet configured.  IP: " & _CurrentIP)
			Throw New ArgumentException("This host (" & _CurrentHost & ": " & sLenderRef & ") is not yet configured.  IP:  " & _CurrentIP)
		End If


		_CurrentOrgId = oConfig.OrganizationId
		_CurrentLenderId = oConfig.LenderId
		_CurrentWebsiteConfig = oConfig


		If Common.SafeString(Request.Params("isRecycle")).ToUpper = "Y" Then
			'This block of codes will be run when there is a querystring name isrecycle=y in request url
			'For ex: /xa/xpressapp.aspx?lenderref=abc&isrecycle=y
			'It will:
			'1. Clear cached data in memory of ALL APP INSTANCES
			'2. Clear cached data in database
			'3. Re-cache data into database
			'4. Re-cache data into memory of CURRENT APP INSTANCES
			log.Debug("Start cache recycle")
			Dim appServiceDomains = ConfigurationManager.AppSettings.Get("AZURE_APP_SERVICE_DOMAINS")
			Dim appServiceDomainList As New List(Of String)
			If Not String.IsNullOrWhiteSpace(appServiceDomains) Then
				appServiceDomainList = appServiceDomains.Split(","c).Where(Function(p) Not String.IsNullOrWhiteSpace(p)).Distinct().ToList()
			End If
			If appServiceDomainList.Count > 1 Then
				Dim idx As Integer = 0
				For Each domain As String In appServiceDomainList
					Try
						Dim req = WebRequest.Create(String.Format("{0}://{1}/handler/recycleCache.aspx?suffix={2}{3}", Request.Url.Scheme, domain, oConfig.CacheIDSuffix, IIf(idx = 0, "&recycledb=y", "")))
						req.GetResponse()
					Catch ex As Exception
						_log.Warn(ex)
					End Try
					idx += 1
				Next
			Else
				CDownloadedSettings.Recycle(oConfig.CacheIDSuffix, True)
			End If
		ElseIf Common.SafeString(Request.Params("recyclememory")).ToUpper = "Y" Then
			'This block of codes is using by keepalive service which is run by Schedule Task to keep all settings up to date.
			'Keepalive service will send request to ALL APP INSTANCES(app service in Azure or IIS server) to
			'1. Clear cached data in memory of ALL APP INSTANCES
			'2. Clear cached data in database (if recycledb=y)
			'3. Re-cache data into database (if recycledb=y)
			'4. Re-cache data into memory of ALL APP INSTANCES
			'Note: By sending request to ALL APP INSTANCES, Keepalive service will re-cache data in memory/database of ALL APP INSTANCES
			CDownloadedSettings.Recycle(oConfig.CacheIDSuffix, Common.SafeString(Request.QueryString("recycledb")).ToUpper().Equals("Y"))
		End If
		_MFAEmailOnly = oConfig.MfaEmailOnly
		_LogoUrl = oConfig.LogoUrl
		_Logo = Common.RenderLogo(_CurrentWebsiteConfig, oConfig.LogoUrl)
		_OtherLogo = Common.RenderLogo(_CurrentWebsiteConfig, oConfig.OtherLogoUrl)
		_RedirectURL = oConfig.FinishedUrl
		_RedirectXAURL = oConfig.FinishedXAUrl

		lenderName = oConfig.LenderName
		_InstitutionType = oConfig.InstitutionType
		Common.ProcessColorScheme(_CurrentWebsiteConfig, _HeaderDataTheme, _FooterDataTheme, _FooterFontDataTheme, _ContentDataTheme, _HeaderDataTheme2, _BackgroundTheme, _ButtonTheme, _ButtonFontTheme)
		'_BackgroundTheme = Common.getBackgroundTheme(_CurrentWebsiteConfig)
		_CurrentLenderRef = oConfig.LenderRef
		_CurrentAPIUserID = oConfig.APIUser
		'_CurrentLenderName = oConfig.LenderName

		'use default loans branchID if there is NO branchID in url parameter
		If String.IsNullOrEmpty(_CurrentBranchId) Then
			If _CurrentWebsiteConfig.sDefaultXABranchID <> "" And Request.Url.AbsolutePath.ToUpper.Contains("XPRESS") Then
				_CurrentBranchId = _CurrentWebsiteConfig.sDefaultXABranchID	'use branch from xa if available or use the main 
			Else
				_CurrentBranchId = _CurrentWebsiteConfig.sDefaultLoanBranchID 'loan
			End If
		End If
		''xaCombo branch ID, xaComboFom
		If IsComboMode Then
			If String.IsNullOrEmpty(_CurrentBranchId) Then
				_XAComboBranchId = _CurrentWebsiteConfig.sDefaultXABranchID
			Else
				_XAComboBranchId = _CurrentBranchId
			End If
			_HideXAFOM = Common.getNodeAttributes(_CurrentWebsiteConfig, "XA_LOAN/VISIBLE", "fom").ToUpper = "N"
		End If
		''easier for passing info to new object
		_CurrentWebsiteConfig.CurrentBranchID = _CurrentBranchId
		_CurrentWebsiteConfig.RequestedIP = _CurrentIP
		_CurrentWebsiteConfig.RequestedHost = _CurrentHost
		_CurrentWebsiteConfig.RequestedUserAgent = _CurrentUserAgent

		''get header menu message for config file in future
		_headerMessage = Server.HtmlDecode(Common.getInnerTextMessage(_CurrentWebsiteConfig, "HEADER_MESSAGE"))
		''footer content
		InitContentFooter()
		''get occupation from config
		Dim sNodeName As String = "ENUMS/JOB_TITLE/ITEM"
		Dim oSerializer As New JavaScriptSerializer()
		Dim occupationDic As Dictionary(Of String, String) = Common.getItemsFromConfig(_CurrentWebsiteConfig, sNodeName, "text", "value")
		If occupationDic.Count > 0 Then
			_occupationList = oSerializer.Serialize(occupationDic)
		End If




		'End If
		''dl scan required
		_requiredDLScan = Common.SafeString(Common.getNodeAttributes(_CurrentWebsiteConfig, "BEHAVIOR", "dl_extract_required"))
		_requiredDLScanXA = Common.SafeString(Common.getNodeAttributes(_CurrentWebsiteConfig, "XA_LOAN/BEHAVIOR", "dl_extract_required"))
		''upload document required
		_requiredUploadDocs = Common.SafeString(Common.getNodeAttributes(_CurrentWebsiteConfig, "BEHAVIOR", "doc_upload_required"))
		_requiredUpLoadDocsXA = Common.SafeString(Common.getNodeAttributes(_CurrentWebsiteConfig, "XA_LOAN/BEHAVIOR", "doc_upload_required"))

		''foreign phone and address
		_previous_address_threshold = Common.SafeString(_CurrentWebsiteConfig.PreviousAddressThresdhold)
		_previous_employment_threshold = Common.SafeString(_CurrentWebsiteConfig.PreviousEmploymentThresdhold)
		_isForeignPhone = Common.SafeString(Common.getVisibleAttribute(_CurrentWebsiteConfig, "foreign_phone"))
		_isForeignAddress = Common.SafeString(Common.getVisibleAttribute(_CurrentWebsiteConfig, "foreign_address"))
		_isEmploymentDurationRequired = Common.SafeString(Common.getNodeAttributes(_CurrentWebsiteConfig, "BEHAVIOR", "employment_duration_required"))
		_isHomephoneRequired = Common.SafeString(Common.getNodeAttributes(_CurrentWebsiteConfig, "BEHAVIOR", "home_phone_required"))
		_requireMemberNumber = Common.SafeString(Common.getNodeAttributes(_CurrentWebsiteConfig, "BEHAVIOR", "require_mem_number"))

		_requiredBranch = Common.getNodeAttributes(_CurrentWebsiteConfig, "BEHAVIOR", "branch_selection_dropdown_required")
		_collectDescriptionIfEmploymentStatusIsOther = Common.getNodeAttributes(_CurrentWebsiteConfig, "BEHAVIOR", "collect_description_if_employment_status_is_other")
		_collectDescriptionIfOccupancyStatusIsOther = Common.getNodeAttributes(_CurrentWebsiteConfig, "BEHAVIOR", "collect_description_if_occupancy_status_is_other")


		Dim comboEligibilityHeaderNode As XmlNode = _CurrentWebsiteConfig._webConfigXML.SelectSingleNode("CUSTOM_TEXT/COMBO_ELIGIBILITY_HEADER")
		If comboEligibilityHeaderNode IsNot Nothing Then
			_comboEligibilityHeader = comboEligibilityHeaderNode.InnerText.Trim()
		End If
		Dim comboFundingHeaderNode As XmlNode = _CurrentWebsiteConfig._webConfigXML.SelectSingleNode("CUSTOM_TEXT/COMBO_FUNDING_HEADER")
		If comboFundingHeaderNode IsNot Nothing Then
			_comboFundingHeader = comboFundingHeaderNode.InnerText.Trim()
		End If

		_xaContinueWhenLoanIsReferred = Common.getNodeAttributes(_CurrentWebsiteConfig, "BEHAVIOR", "xa_continue_when_loanisreferred")

		_isCQNewAPI = Common.getVisibleAttribute(_CurrentWebsiteConfig, "custom_question_new_api") = "Y"
		mappedShowFieldLoanType = _LoanType
		If IsComboMode AndAlso _LoanType <> "BL" Then
			mappedShowFieldLoanType = _LoanType & "_COMBO"
		ElseIf sPath.Contains("/XA/") Then
			Select Case _LoanTypeOther
				Case "1a"
					mappedShowFieldLoanType = "XA_MINOR"
				Case "2a"
					mappedShowFieldLoanType = "SA_MINOR"
				Case "1s"
					mappedShowFieldLoanType = "XA_SPECIAL"
				Case "2s"
					mappedShowFieldLoanType = "SA_SPECIAL"
				Case "1b"
					mappedShowFieldLoanType = "XA_BUSINESS"
				Case "2b"
					mappedShowFieldLoanType = "SA_BUSINESS"
			End Select
		End If
		'For backward compatibility and avoid the need for migration script, use values “xa_special” and “sa_special” for loan_type attribute for both XA Special and XA Minor - don't use values “xa_minor” or “sa_minor”
		'This may have the side effect that a change in XA Special may leak to XA Minor. We would have to ask client to use as-is.
		Dim convertedType As String = mappedShowFieldLoanType
		If mappedShowFieldLoanType = "XA_MINOR" Then
			convertedType = "XA_SPECIAL"
		ElseIf mappedShowFieldLoanType = "SA_MINOR" Then
			convertedType = "SA_SPECIAL"
		End If
		_showFieldList = Common.getItemsFromConfig(_CurrentWebsiteConfig, String.Format("CUSTOM_LIST/SHOW_FIELDS/ITEM[@loan_type='{0}']", convertedType),
					Function(x)
						Return New KeyValuePair(Of String, String)(Common.SafeString(x.Attributes("controller_id").InnerXml), Common.SafeString(x.Attributes("is_visible").InnerXml))
					End Function)
		_requireFieldList = Common.getItemsFromConfig(_CurrentWebsiteConfig, String.Format("CUSTOM_LIST/REQUIRE_FIELDS/ITEM[@loan_type='{0}']", convertedType),
					Function(x)
						Return New KeyValuePair(Of String, String)(Common.SafeString(x.Attributes("controller_id").InnerXml), Common.SafeString(x.Attributes("is_require").InnerXml))
					End Function)

		''get business loan purpose type from Landing page
		If mappedShowFieldLoanType = "LP" Then
			_showFieldList = Common.getItemsFromConfig(_CurrentWebsiteConfig, String.Format("CUSTOM_LIST/SHOW_FIELDS/ITEM[@loan_type='{0}']", "BL"),
				Function(x)
					Return New KeyValuePair(Of String, String)(Common.SafeString(x.Attributes("controller_id").InnerXml), Common.SafeString(x.Attributes("is_visible").InnerXml))
				End Function)
			_requireFieldList = Common.getItemsFromConfig(_CurrentWebsiteConfig, String.Format("CUSTOM_LIST/REQUIRE_FIELDS/ITEM[@loan_type='{0}']", "BL"),
						Function(x)
							Return New KeyValuePair(Of String, String)(Common.SafeString(x.Attributes("controller_id").InnerXml), Common.SafeString(x.Attributes("is_require").InnerXml))
						End Function)
		End If

		''membership fees
		If Not String.IsNullOrEmpty(_CurrentWebsiteConfig.MembershipFee) Then
			If Decimal.TryParse(_CurrentWebsiteConfig.MembershipFee, _MembershipFee) = False Then
				_MembershipFee = 0D
			End If
		End If
		''END FOM_V2
		_customListFomFees = Common.GetFomFeesFromCustomList(_CurrentWebsiteConfig)

		'HomeBanking
		_HomeBankingSSOEnable = Common.GetHomeBankingEnable(_CurrentWebsiteConfig._webConfigXML.OuterXml)
		''location pool
		_locationPool = _CurrentWebsiteConfig.LocationPool
		''combo no available message
		_noAvailableProductMessage = "<br/><p <p class=""rename-able"">Thank you for considering us for your banking needs. We're sorry, but we are not offering products in your Zip Code at this time.</p><p class=""rename-able"">Please, feel free to check back with us in the future.</p>"
		_xaBeneficiaryLevel = _CurrentWebsiteConfig.GetXABeneficiaryLevel()
		sso_ida_enable = Common.getNodeAttributes(_CurrentWebsiteConfig, "BEHAVIOR", "sso_ida_enable") = "Y"
		xa_sso_ida_enable = Common.getNodeAttributes(_CurrentWebsiteConfig, "XA_LOAN/BEHAVIOR", "sso_ida_enable") = "Y"

		If Not String.IsNullOrEmpty(_LoanNodeName) Then
			Dim clr = GetCustomListRuleList(_LoanNodeName)
			If clr IsNot Nothing AndAlso clr.Count > 0 Then
				_customListRuleList = clr
			End If
			If IsComboMode Then
				Dim xaClr = GetCustomListRuleList("XA_LOAN")
				If xaClr IsNot Nothing AndAlso xaClr.Count > 0 Then
					_xaCustomListRuleList = xaClr
				End If
			End If
		End If

		If Regex.IsMatch(_LoanType, "^(cc|he|pl|vl)$", RegexOptions.IgnoreCase) Then
			_nssList = GetNonSigningSpouseSettings(_LoanNodeName)
		End If
	End Sub
	Protected Function GetXACustomApplicationScenarioList() As List(Of CCustomApplicationScenarioItem)
		Dim result As New List(Of CCustomApplicationScenarioItem)
		Dim xaCASNode As XmlNode = _CurrentWebsiteConfig._webConfigXML.SelectSingleNode("XA_LOAN/CUSTOM_APPLICATION_SCENARIOS")
		If xaCASNode IsNot Nothing AndAlso Not String.IsNullOrEmpty(xaCASNode.InnerText) Then
			Dim lst = JsonConvert.DeserializeObject(Of List(Of CCustomApplicationScenarioItem))(xaCASNode.InnerText)
			If lst IsNot Nothing AndAlso lst.Count > 0 Then
				result = lst.Where(Function(p) p.Enabled).ToList()
			End If
		End If
		Return result
	End Function
	Protected Function GetXACustomApplicationScenario(scenariosName As String) As CCustomApplicationScenarioItem
		Dim result As CCustomApplicationScenarioItem
		Dim lst = GetXACustomApplicationScenarioList()
		If lst IsNot Nothing AndAlso lst.Count > 0 Then
			result = lst.FirstOrDefault(Function(p) p.Name = scenariosName)
		End If
		Return result
	End Function
	Private Function GetCustomListRuleList(nodeName As String) As List(Of CCustomListRuleItem)
		Dim result As New List(Of CCustomListRuleItem)
		Dim loanClrNode As XmlNode = _CurrentWebsiteConfig._webConfigXML.SelectSingleNode(nodeName & "/CUSTOM_LIST_RULES")
		If loanClrNode IsNot Nothing AndAlso Not String.IsNullOrEmpty(loanClrNode.InnerText) Then
			Dim loanClrList = JsonConvert.DeserializeObject(Of List(Of CCustomListRuleItem))(loanClrNode.InnerText)
			If loanClrList IsNot Nothing AndAlso loanClrList.Count > 0 AndAlso loanClrList.Any(Function(p) p.Enable = True AndAlso p.List IsNot Nothing) Then
				result = loanClrList.Where(Function(p) p.Enable = True AndAlso p.List IsNot Nothing).ToList()
			End If
		End If
		Return result
	End Function

	Public Function EvaluateCustomList(Of T As CCustomListRuleBaseItem)(code As String, Optional params As Dictionary(Of String, Tuple(Of Object, String)) = Nothing) As List(Of T)
		Return New CCustomListRuleHandler(Of T)(_customListRuleList).Evaluate(code, params)
	End Function
	Public Function EvaluateCustomList(Of T As CCustomListRuleBaseItem)(code As String, cl As List(Of CCustomListRuleItem), Optional params As Dictionary(Of String, Tuple(Of Object, String)) = Nothing) As List(Of T)
		Return New CCustomListRuleHandler(Of T)(cl).Evaluate(code, params)
	End Function

    Public Function CheckShowField(controllerId As String, prefixID As String, Optional showByDefault As Boolean = False) As Boolean
		If mappedShowFieldLoanType = "XA_BUSINESS" OrElse mappedShowFieldLoanType = "SA_BUSINESS" OrElse mappedShowFieldLoanType = "XA_SPECIAL" OrElse mappedShowFieldLoanType = "SA_SPECIAL" OrElse mappedShowFieldLoanType = "BL" Then
			controllerId = IIf(String.IsNullOrEmpty(prefixID), "", "ns_").ToString() & controllerId	   ' ns = no scope
		Else
			controllerId = prefixID & controllerId
		End If
		If _showFieldList IsNot Nothing AndAlso _showFieldList.ContainsKey(controllerId) Then
			Dim value As String = _showFieldList(controllerId)
			If value.Equals("Y") Then
				Return True
			ElseIf value.Equals("N") Then
				Return False
			Else
				Return showByDefault
			End If
		End If
		Return showByDefault
	End Function

	Public Function CheckRequireField(controllerId As String, prefixID As String, Optional showByDefault As Boolean = False) As Boolean
		If mappedShowFieldLoanType = "XA_BUSINESS" OrElse mappedShowFieldLoanType = "SA_BUSINESS" OrElse mappedShowFieldLoanType = "XA_SPECIAL" OrElse mappedShowFieldLoanType = "SA_SPECIAL" OrElse mappedShowFieldLoanType = "BL" Then
			controllerId = IIf(String.IsNullOrEmpty(prefixID), "", "ns_").ToString() & controllerId	   ' ns = no scope
		Else
			controllerId = prefixID & controllerId
		End If
		If _requireFieldList IsNot Nothing AndAlso _requireFieldList.ContainsKey(controllerId) Then
			Dim value As String = _requireFieldList(controllerId)
			If value.Equals("Y") Then
				Return True
			ElseIf value.Equals("N") Then
				Return False
			Else
				Return showByDefault
			End If
		End If
		Return showByDefault
	End Function

	Public Function GetBranches(ByVal loanType As String) As String
		''branchName dictionary
		Dim branchNameDic As New Dictionary(Of String, String)
		Dim branchNameDropdown As String = ""
		If String.IsNullOrEmpty(_CurrentWebsiteConfig.LenderCode) Then
			branchNameDic = CDownloadedSettings.DownloadLenderServiceConfigInfo(_CurrentWebsiteConfig).BranchNamesDic
		Else
			'When lender_code is not null, use data from cached object for the branch selection dropdown
			Dim branches As List(Of DownloadedSettings.CBranch) = CDownloadedSettings.Branches(_CurrentWebsiteConfig)
			If branches IsNot Nothing And branches.Any() Then
				Select Case loanType
					Case "XA"
						'XA should filter for branches based on IsXA & IsAvailableToConsumerXA
						branchNameDic = branches.Where(Function(b) b.IsXA = "Y" And b.IsAvailableToConsumerXA = "Y").ToDictionary(Function(x) x.BranchCode, Function(x) x.BranchName)
					Case "CC"
						'Loan should filter for branches based on Is[Loantype] & IsAvailableToConsumerLoan
						branchNameDic = branches.Where(Function(b) b.IsCredit = "Y" And b.IsAvailableToConsumerLoan = "Y").ToDictionary(Function(x) x.BranchCode, Function(x) x.BranchName)
					Case "HE"
						'Loan should filter for branches based on Is[Loantype] & IsAvailableToConsumerLoan
						branchNameDic = branches.Where(Function(b) b.IsHE = "Y" And b.IsAvailableToConsumerLoan = "Y").ToDictionary(Function(x) x.BranchCode, Function(x) x.BranchName)
					Case "PL"
						'Loan should filter for branches based on Is[Loantype] & IsAvailableToConsumerLoan
						branchNameDic = branches.Where(Function(b) b.IsPL = "Y" And b.IsAvailableToConsumerLoan = "Y").ToDictionary(Function(x) x.BranchCode, Function(x) x.BranchName)
					Case "VL"
						'Loan should filter for branches based on Is[Loantype] & IsAvailableToConsumerLoan
						branchNameDic = branches.Where(Function(b) b.IsVL = "Y" And b.IsAvailableToConsumerLoan = "Y").ToDictionary(Function(x) x.BranchCode, Function(x) x.BranchName)

				End Select
			End If
		End If

		If branchNameDic.Count Then
			branchNameDic = branchNameDic.OrderBy(Function(p) p.Value).ToDictionary(Function(p) p.Key, Function(p) p.Value)
			branchNameDropdown = Common.RenderDropdownlistWithEmpty(branchNameDic, "", "")
		End If
		Return branchNameDropdown
	End Function

	Public Function GetAssetTypes(ByVal loanType As String) As String
		Dim assetTypesDic As New Dictionary(Of String, String)
		Dim assetTypesDropdown As String = ""
		Dim enumValues As Array = [Enum].GetValues(GetType(AssetType))
		For Each value As [Enum] In enumValues
			assetTypesDic.Add(value.ToString(), Common.GetEnumDescription(value))
		Next
		If assetTypesDic.Count Then
			assetTypesDic = assetTypesDic.OrderBy(Function(p) p.Value).ToDictionary(Function(p) p.Key, Function(p) p.Value)
			assetTypesDropdown = Common.RenderDropdownlistWithEmpty(assetTypesDic, "", "--Please Select--")
		End If
		Return assetTypesDropdown
	End Function
	Public Function GetToken() As String
		' Generate random value between 10 and 310. 
		Dim value As Integer = New CSecureRandom().GetValue(10, 310)
		Dim oGuid As Guid = Guid.NewGuid ' this will make the number longer and more random
		Dim sToken As String = Common.SafeString(oGuid).Replace("-", "") & Common.SafeString(value)
		Session("token") = sToken
		Session("token_count") = Nothing
		Dim sessionId = System.Web.HttpContext.Current.Session.SessionID
		log.Info("session ID: " & sessionId & "; token: " & sToken)
		Return sToken
	End Function

	Public Function ValidateToken(ByVal psToken As String) As Boolean
		If psToken = "" Then Return False
		Dim sStoredToken = Common.SafeString(Session("token"))
		log.Info("validate session ID: " & HttpContext.Current.Session.SessionID & "; token: " & psToken)
		If sStoredToken = "" Then
			log.Info("Invalid Token: No session found.")
			'Dim sessionId = System.Web.HttpContext.Current.Session.SessionID
			'_log.info("validate session ID: " & sessionId & "; token: " & psToken)
			Return False
		End If

		Dim nValue As Long = (Common.SafeLong(sStoredToken.Substring(32)) * 3) + 733	'roll the integer part of the token start from index 32 to
		nValue = (nValue * 3) + 733	 '2nd time because it is done twice on client side
		Dim sRolledToken As String = sStoredToken.Substring(0, 32) & Common.SafeString(nValue)
		Dim bMatch As Boolean = False

		'account for revisiting page, multiple click due to fail validation
		For i = 0 To 20
			If psToken = sRolledToken Then
				bMatch = True
				log.Info("Token Matched with " & i & " iteration")
				Exit For
			Else
				nValue = (nValue * 3) + 733
				sRolledToken = sStoredToken.Substring(0, 32) & Common.SafeString(nValue)
			End If
		Next

		If bMatch Then
			'want to allow a leat 6 submit to account for authentication questions
			Dim nSubmitCount As Integer = Common.SafeInteger(Session("token_count"))
			nSubmitCount += 1
			Session("token_count") = nSubmitCount
			If nSubmitCount >= 6 Then
				Session("token") = Nothing
				Session("token_count") = Nothing
			End If
			Return True
		End If
		Return False
	End Function

	Protected Function GetAllowedProductCodes(ByVal loanName As String) As List(Of String)
		Dim ProductCodes As New List(Of String)
		Dim oNodes As XmlNodeList = _CurrentWebsiteConfig._webConfigXML.SelectNodes(String.Format("{0}/ACCOUNTS/ACCOUNT", loanName))

		If oNodes.Count < 1 Then
			Return ProductCodes
		End If

		For Each child As XmlNode In oNodes
			If child.Attributes("product_code") Is Nothing Then
				logError("product_code is not correct.", Nothing)
			End If
			If child.Attributes("product_code").InnerXml <> "" Then
				ProductCodes.Add(child.Attributes("product_code").InnerXml)
			End If
		Next

		Return ProductCodes

	End Function

	Protected Function GetInterestRate() As Double
		Dim result As Double = 2.5D
		'Dim xpath As String = String.Format("CUSTOM_LIST/RATES/ITEM[lower-case(@loan_type)='{0}']", _LoanType.NullSafeToLower_)
		'Dim node = _CurrentWebsiteConfig._webConfigXML.SelectSingleNode(xpath)
		'If node IsNot Nothing AndAlso node.Attributes.ItemOf("rate") IsNot Nothing Then
		'	Try
		'		result = Common.SafeDouble(node.Attributes.ItemOf("rate").InnerText)
		'	Catch ex As Exception

		'	End Try
		'End If

		Dim sLoanType As String = _LoanType.ToLower
		Dim sLoanTypeOther As String = _LoanTypeOther.NullSafeToLower_

		'interest for combo is the same as interest rate for standard app, so disable this lines of code
		'If IsComboMode Then
		'	sLoanType = sLoanType & "_combo"
		'End If

		Dim xpath As String = "CUSTOM_LIST/RATES/ITEM"
		Dim oEnumsNodes As XmlNodeList = _CurrentWebsiteConfig._webConfigXML.SelectNodes(xpath)
		For Each node As XmlElement In oEnumsNodes
			Dim enumLoanType = Common.SafeString(node.GetAttribute("loan_type")) 'loan_type may be null
			If enumLoanType <> "" Then
				Dim arrayLoanType() As String = enumLoanType.ToLower().Split("|"c)

				Dim bIsSpecialApp_APM = (sLoanTypeOther <> "" AndAlso arrayLoanType.Contains("xa_special") And Regex.IsMatch(sLoanTypeOther.Trim().ToLower(), "^(1a|1b|2a|2b)$"))
				Dim bIsSpecialApp_Legacy = (sLoanTypeOther <> "" AndAlso arrayLoanType.Contains(sLoanTypeOther))	'(loanTypeOther=1a,1b,2a,2b and loan_type=1a,1b,2a,2b)   or (loanTypeOther=MERCHANT and loan_type="MERCHANT")
				Dim bIsNormalorComboLoan = arrayLoanType.Contains(sLoanType)	' ex: VL, VL_COMBO

				If bIsSpecialApp_Legacy OrElse bIsSpecialApp_APM OrElse bIsNormalorComboLoan Then
					Dim rate As String = Common.SafeString(node.GetAttribute("rate")).ToLower
					If rate IsNot Nothing Then
						Return rate
					End If
				End If
			End If
		Next
		Return result
	End Function

	Protected Function GetBusinessIndustryCodes() As Dictionary(Of String, String)
		Dim result As New Dictionary(Of String, String)
		Dim xmlDoc As New XmlDocument
		xmlDoc.Load(Server.MapPath("~/App_Data/CustomlistTemplates.xml"))
		Dim node = xmlDoc.SelectSingleNode("CUSTOM_LIST_TEMPLATES/CUSTOM_LIST_TEMPLATE/CODE[text()='BUSINESS_INDUSTRY_CODES']")
		If node IsNot Nothing Then
			Dim businessIndustryCodeItems = node.ParentNode.SelectNodes("ITEMS/ITEM")
			If businessIndustryCodeItems IsNot Nothing AndAlso businessIndustryCodeItems.Count > 0 Then
				For Each item As XmlNode In businessIndustryCodeItems
					Dim value = Common.GetInnerTextValue(item, "VALUE", "")
					Dim text = Common.GetInnerTextValue(item, "TEXT", "")
					If Not String.IsNullOrWhiteSpace(value) AndAlso Not String.IsNullOrWhiteSpace(text) AndAlso Not result.ContainsKey(value) Then
						result.Add(value, text)
					End If
				Next
			End If

		End If
		Return result
	End Function
	Protected Function getBusinessLoan(ByVal oConfig As CWebsiteConfig) As CBusinessLoan
		Dim result As New CBusinessLoan()
		'Dim BLNode = oConfig._webConfigXML.SelectSingleNode("BUSINESS_LOAN")
		result.VehiclePurposes = _CurrentWebsiteConfig.GetEnumItems("BL_VEHICLE_PURPOSES")
		result.CardPurposes = _CurrentWebsiteConfig.GetEnumItems("BL_CREDIT_CARD_PURPOSES")
		result.OtherPurposes = _CurrentWebsiteConfig.GetEnumWithCatItems("BL_OTHER_PURPOSES")
		result.CreditCardTypes = _CurrentWebsiteConfig.GetEnumItems("BL_CREDIT_CARD_TYPES")
		result.CardDisclosures = getBusinessDisclosures(oConfig, "BUSINESS_LOAN/CARD_DISCLOSURES/DISCLOSURE")
		result.VehicleDisclosures = getBusinessDisclosures(oConfig, "BUSINESS_LOAN/VEHICLE_DISCLOSURES/DISCLOSURE")
		result.OtherDisclosures = getBusinessDisclosures(oConfig, "BUSINESS_LOAN/OTHER_DISCLOSURES/DISCLOSURE")
		result.CreditCardNames = Common.getItemsFromConfig(_CurrentWebsiteConfig, "ENUMS/BL_CREDIT_CARD_NAMES/ITEM",
					Function(x)
						Dim name As String = ""
						Dim type As String = ""
						Dim image As String = ""
						Dim category As String = ""
						If x.Attributes("credit_card_name") IsNot Nothing Then
							name = Common.SafeString(x.Attributes("credit_card_name").InnerXml)
						End If
						If x.Attributes("credit_card_type") IsNot Nothing Then
							type = Common.SafeString(x.Attributes("credit_card_type").InnerXml)
						End If
						If x.Attributes("image_url") IsNot Nothing Then
							image = Common.SafeString(x.Attributes("image_url").InnerXml)
						End If
						If x.Attributes("category") IsNot Nothing Then
							category = Common.SafeString(x.Attributes("category").InnerXml)
						End If
						Return New Tuple(Of String, String, String, String)(name, type, image, category)
					End Function)
		result.EnableCCDeclaration = Common.getVisibleAttribute(oConfig, "bl_enable_declaration_cc") = "Y"
		result.EnableOTDeclaration = Common.getVisibleAttribute(oConfig, "bl_enable_declaration_ot") = "Y"
		result.EnableVLDeclaration = Common.getVisibleAttribute(oConfig, "bl_enable_declaration_vl") = "Y"
		Return result
	End Function
	Private Function getBusinessDisclosures(ByVal oConfig As CWebsiteConfig, ByVal sXPath As String) As List(Of String)
		Dim oDisclosures As New List(Of String)
		Dim oDisclosureNodes As XmlNodeList = oConfig._webConfigXML.SelectNodes(sXPath)
		If oDisclosureNodes Is Nothing OrElse oDisclosureNodes.Count = 0 Then
			Return oDisclosures
		End If
		For Each node As XmlElement In oDisclosureNodes
			oDisclosures.Add(node.InnerText)
		Next
		Return oDisclosures
	End Function

	Protected Function GetBusinessLoanApplicantRoles() As Dictionary(Of String, String)
		Dim applicantRoles As New Dictionary(Of String, String)()
		applicantRoles.Add("P", "Primary") 'this item is required
		applicantRoles.Add("C", "Co-Borrower")
		applicantRoles.Add("S", "Co-Signer")
		applicantRoles.Add("G", "Guarantor")
		Return applicantRoles
	End Function

	Private Function GetNonSigningSpouseSettings(nodeName As String) As Dictionary(Of String, List(Of String))
		Dim result As New Dictionary(Of String, List(Of String))
		Dim nssNodes = _CurrentWebsiteConfig._webConfigXML.SelectNodes(nodeName & "/BEHAVIOR/NON_SIGNING_SPOUSE_STATES/STATE")
		If nssNodes IsNot Nothing AndAlso nssNodes.Count > 0 Then
			Dim disclosureList As List(Of String)
			For Each stateNode As XmlNode In nssNodes
				If stateNode.NodeType = XmlNodeType.Comment Then Continue For
				Dim disclosureNodes = stateNode.SelectNodes("DISCLOSURES/DISCLOSURE")
				disclosureList = New List(Of String)
                If disclosureNodes IsNot Nothing AndAlso disclosureNodes.Count > 0 Then
                    For Each disclosureNode As XmlNode In disclosureNodes
                        disclosureList.Add(disclosureNode.InnerText)
                    Next
                    disclosureList.AddRange(From disclosureNode As XmlNode In disclosureNodes Where disclosureNode.NodeType = XmlNodeType.CDATA Select disclosureNode.InnerText)
                Else 'use default text if FI doesn't have one
                    disclosureList.Add("I understand that this financial institution is required by state law to notify my spouse by mail if my credit card or loan is granted. I certify that the credit being applied for, if granted, will be incurred or obtained during marriage and will be in the interest of the marriage or family.")
                End If
                If stateNode.Attributes("code") IsNot Nothing AndAlso Regex.IsMatch(stateNode.Attributes("code").Value, "^[a-zA-Z]{2}$", RegexOptions.IgnoreCase) Then
                    result.Add(stateNode.Attributes("code").Value.ToUpper(), disclosureList)
                End If
            Next
		End If
		Return result
	End Function

#Region "IP validation"

	''Only used by ViewSubmittedLoans but ViewSubmittedLoans already inherit CheckIPAccessDeniedcheck via CBasePage
	'   Public Function ValidateIPForCU() As String
	'	Dim errMessage As String = String.Empty
	'	Dim sHost As String = Request.Url.Host

	'	'temporary white list
	'	If _CurrentLenderRef.ToUpper.Contains("SDFCU_TEST") Then Return String.Empty


	'	'If IsBlackListRegion() Then
	'	'	errMessage &= "<b><span style='color: red'>Invalid Source</span></b><br/>"
	'	'End If

	'	If isWhiteListIP() Then Return String.Empty

	'	If sHost.Contains("apptest") Or sHost.Contains("appstage") Or _CurrentLenderRef.NullSafeToUpper_.Contains("BCU") Or _CurrentLenderRef.NullSafeToLower_.Contains("comonefcu") Or _CurrentLenderRef.NullSafeToLower_.Contains("commonwealth") Then
	'		_nIPCount = 250
	'	Else
	'		_nIPCount = 25
	'	End If

	'	If getIPCount(_CurrentIP, Guid.Empty) > _nIPCount Then
	'		errMessage &= "<b><span style='color: red'>Please try again later.  You have exceeded the maximum number of applications per hour.</span></b><br/>"
	'	End If

	'	If (errMessage <> "") Then
	'		errMessage &= "<div id='MLerrorMessage'></div>"
	'		Return errMessage
	'	End If

	'	logIP()

	'	Return errMessage
	'End Function

	''' <summary>
	''' Check the number of submitted app again the limiter.  This is mostl done in callback
	''' </summary>
	''' <returns>"null" for excluded IP; "blockmaxapps" when count exceeded limit </returns>
	Public Function ValidateIP() As String
		'temporary white list
		If isWhiteListIP() Then
			Return ""
		ElseIf SubmissionLimiterExemptIPs IsNot Nothing AndAlso SubmissionLimiterExemptIPs.Any(Function(p)
																										 If p.IP = Request.UserHostAddress Then Return True
																										 If p.IP.EndsWith("*") Then	'123.23.12.*
																											 Return Request.UserHostAddress.StartsWith(p.IP.Substring(0, p.IP.LastIndexOf("."c) + 1))
																										 End If
																										 Return False
																									 End Function) Then
			Return ""
		Else
			'If sHost.Contains("apptest") Or sHost.Contains("appstage") Or _CurrentLenderRef.NullSafeToUpper_.Contains("BCU") Or _CurrentLenderRef.NullSafeToLower_.Contains("comonefcu") Or _CurrentLenderRef.NullSafeToLower_.Contains("commonwealth") Then
			'	_nIPCount = 250
			'Else
			'	_nIPCount = 25
			'End If
			Dim counter As Integer = getIPCount(_CurrentIP, Guid.Parse(_CurrentLenderId)) + 1
			If SubmissionMaxAppBeforeNotification > 0 AndAlso counter = SubmissionMaxAppBeforeNotification AndAlso counter < SubmissionMaxAppBeforeBlocking AndAlso SubmissionLimiterInternalEmails IsNot Nothing AndAlso SubmissionLimiterInternalEmails.Count > 0 Then
				Common.SendSubmissionRateLimiterNotificationEmails(SubmissionLimiterInternalEmails, counter, SubmissionMaxAppBeforeBlocking, Request.UserHostAddress)
			End If

			' Use >= here because below, an extra logIP will raise the counter to 1 extra after a block.
			If SubmissionMaxAppBeforeBlocking > 0 AndAlso counter >= SubmissionMaxAppBeforeBlocking Then
				' Only send an email for the first submission to trigger the block.
				If counter = SubmissionMaxAppBeforeBlocking Then
					If SubmissionLimiterInternalEmails IsNot Nothing AndAlso SubmissionLimiterInternalEmails.Count > 0 Then
						Common.SendSubmissionRateLimiterBlockingEmails(SubmissionLimiterInternalEmails, SubmissionMaxAppBeforeBlocking, Request.UserHostAddress)
					End If
					' Log one extra here, so that the next block that comes in will not incur an email.
					' If this doesn't occur, then counter = SubmissionMaxAppBeforeBlocking will always occur.
					logIP()
				End If
				Return "blockmaxapps"
			End If
		End If
		logIP()
		Return ""
	End Function


	''' <summary>
	''' 'handy function used to ignore certain pple.. ie: test pple, internal employee
	''' </summary>
	''' <returns>True when the IP is in the whitelist</returns>
	Private Function isWhiteListIP() As Boolean

		'TODO: put in configuration
		Dim arrIgnoreIPs() As String = New String() {"192.168.111.8", "127.0.0.1", "::1"} ', "10.11.13.245", "127.0.0.1"
		'arrIgnoreIPs = oIgnore.GetAttribute("value").Split(","c)

		If IsNothing(HttpContext.Current) Then
			Return False
		End If

		Return Array.IndexOf(arrIgnoreIPs, Request.UserHostAddress) <> -1
	End Function

	Private Function getIPCount(ipAddress As String, lenderId As Guid) As Integer
		Dim oWhere As New CSQLWhereStringBuilder()
		'number of same ip within the time frame
		Dim sSql As String = "Select count(RemoteIP) from IPTracker"
		oWhere.AppendAndCondition("LogTime > DATEADD(HOUR,-1,GETDATE())")
		oWhere.AppendAndCondition("RemoteIP={0}", New CSQLParamValue(ipAddress))
		If lenderId <> Guid.Empty Then
			oWhere.AppendAndCondition("LenderID={0}", New CSQLParamValue(lenderId))
		End If

		Dim oDB As New CSQLDBUtils(LPQMOBILE_CONNECTIONSTRING)
		Dim nIPCount As Integer = 0
		Try
			nIPCount = Common.SafeInteger(oDB.getScalerValue(sSql & oWhere.SQL))

		Catch ex As Exception
			'log.Error("Get Timezone by phone fail: " & ex.Message, ex)
			log.Error("SQL failure: " & ex.Message, ex)
		Finally
			oDB.closeConnection()

		End Try
		Return nIPCount

	End Function

	Private Sub logIP()
		Dim url As String = Request.Url.AbsolutePath

		Dim oInsert As New cSQLInsertStringBuilder("IPTracker")
		oInsert.AppendValue("IPTrackerID", New CSQLParamValue(System.Guid.NewGuid))
		oInsert.AppendValue("LogTime", New CSQLParamValue(Now))
		oInsert.AppendValue("RemoteIP", New CSQLParamValue(_CurrentIP))
		oInsert.AppendValue("RemoteHost", New CSQLParamValue(_CurrentHost))
		oInsert.AppendValue("RemoteUrl", New CSQLParamValue(url))
		oInsert.AppendValue("OrgID", New CSQLParamValue(Common.SafeGUID(_CurrentOrgId)))
		oInsert.AppendValue("LenderID", New CSQLParamValue(Common.SafeGUID(_CurrentLenderId)))
		oInsert.AppendValue("APIUser", New CSQLParamValue(Common.SafeGUID(_CurrentAPIUserID)))
		oInsert.AppendValue("LenderRef", New CSQLParamValue(_CurrentLenderRef))

		Dim oDB As New CSQLDBUtils(LPQMOBILE_CONNECTIONSTRING)
		Try
			oDB.executeNonQuery(oInsert.SQL, False)
		Catch ex As Exception
			'log.Error("Get Timezone by phone fail: " & ex.Message, ex)
			log.Error("SQL Insertion failure: " & ex.Message, ex)
		Finally
			oDB.closeConnection()
		End Try
	End Sub
	Public Function IsInMode(mode As String) As Boolean
		If Request.QueryString("mode") = mode Then Return True
		Return False
	End Function
	Public Function IsInFeature(feature As String) As Boolean
		If Request.QueryString("feature") = feature Then Return True
		Return False
	End Function

	Public Function BuildShowFieldSectionID(elementID As String, Optional prefixID As String = "") As String
		If String.IsNullOrEmpty(elementID) Then
			'to prevent logic error when empty elementID passing to this function then cause error at runtime, not build time
			log.Error("[BuildShowFieldSectionID] Invalid parameter value")
			Throw New Exception("Invalid parameter value")
		End If
		Dim result As String
		If mappedShowFieldLoanType = "XA_SPECIAL" OrElse mappedShowFieldLoanType = "SA_SPECIAL" OrElse mappedShowFieldLoanType = "XA_BUSINESS" OrElse mappedShowFieldLoanType = "SA_BUSINESS" OrElse mappedShowFieldLoanType = "BL" Then
			result = IIf(String.IsNullOrEmpty(prefixID), "", "ns_").ToString() & elementID ' ns = no scope
		Else
			result = prefixID & elementID
		End If
		Return result
	End Function
	Public Function BuildValidationFieldID(elementID As String, Optional prefixID As String = "") As String
		If String.IsNullOrEmpty(elementID) Then
			'to prevent logic error when empty elementID passing to this function then cause error at runtime, not build time
			log.Error("[BuildValidationFieldID] Invalid parameter value")
			Throw New Exception("Invalid parameter value")
		End If
		Dim result As String
		If mappedShowFieldLoanType = "XA_SPECIAL" OrElse mappedShowFieldLoanType = "SA_SPECIAL" OrElse mappedShowFieldLoanType = "XA_BUSINESS" OrElse mappedShowFieldLoanType = "SA_BUSINESS" OrElse mappedShowFieldLoanType = "BL" Then
			result = IIf(String.IsNullOrEmpty(prefixID), "", "ns_").ToString() & elementID ' ns = no scope
		Else
			result = prefixID & elementID
		End If
		Return result
	End Function
#End Region
#Region "footer content"
	Protected Sub InitContentFooter()
		''footer content by default
		Dim defaultFLText As String = "<span style='font-size:12px'>&copy; " & "2013-" & Now().Year & " MeridianLink, Inc., All Rights Reserved.</span>"
		Dim defaultFRText As String = "Federally Insured by NCUA."
		Dim equalHousingOpportunity As String = Common.getNodeAttributes(_CurrentWebsiteConfig, "LAYOUT", "footer_use_eq_housing_opportunity")
		_equalHousingText = "Equal Housing Lender."
		If equalHousingOpportunity = "Y" Then
			_equalHousingText = "Equal Housing Opportunity."
		End If

		Dim equalOpportunityLender As String = Common.getNodeAttributes(_CurrentWebsiteConfig, "LAYOUT", "footer_use_eq_opportunity_lender")
		If equalOpportunityLender = "Y" Then
			_equalHousingText = "Equal Opportunity Lender."
		End If

		_strLogoUrl = "/images/ehl_trans_370.gif"
		_strNCUAUrl = "https://www.ncua.gov/Pages/default.aspx"
		_strHUDUrl = "http://portal.hud.gov/hudportal/HUD?src=/"

		If _InstitutionType = CEnum.InstitutionType.BANK Then
			defaultFRText = "Federally Insured by FDIC."
			_strNCUAUrl = "https://www.fdic.gov/"
		End If

		Dim footerRightNode As String = "FOOTER_RIGHT"
		Dim footerLeftNode As String = "FOOTER_LEFT"
		Dim isFooterLeft As String = Common.getVisibleAttribute(_CurrentWebsiteConfig, "footer_left")
		Dim isFooterRight As String = Common.getVisibleAttribute(_CurrentWebsiteConfig, "footer_right")
		Dim strFooterLeft As String = Common.getInnerTextMessage(_CurrentWebsiteConfig, footerLeftNode)
		Dim strFooterRight As String = Common.getInnerTextMessage(_CurrentWebsiteConfig, footerRightNode)
		_strLenderName = Common.SafeString(CDownloadedSettings.DownloadLenderServiceConfigInfo(_CurrentWebsiteConfig).LenderName)
		''left footer
		If isFooterLeft = "N" Then
			_strFooterLeft = ""
		Else

			If Not String.IsNullOrEmpty(strFooterLeft) Then
				_strFooterLeft += Server.HtmlDecode(strFooterLeft)
			Else
				_strFooterLeft += Server.HtmlDecode(defaultFLText)
			End If
			_strFooterLeft = Server.HtmlDecode((_strFooterLeft))
		End If
		''right footer
		If isFooterRight = "N" Then
			_strFooterRight = ""
			_strLogoUrl = ""
			_strNCUAUrl = ""
			_strHUDUrl = ""
		Else
			_strFooterRight = Server.HtmlDecode(strFooterRight)
			If strFooterRight.IndexOf("<img") > -1 Or strFooterRight.IndexOf("<image") > -1 Then
				_strFooterRight = Server.HtmlDecode(strFooterRight).Replace("div", "span").Replace("divfooterright-content", "").Replace("divfooterright-logo", "")
			End If
			If String.IsNullOrEmpty(strFooterRight) Then
				_strFooterRight = defaultFRText
			Else
				_strNCUAUrl = ""
				_strHUDUrl = ""
				_strLogoUrl = ""
				_hasFooterRight = "Y"
			End If
		End If
		Dim enableFooterContact As String = Common.getVisibleAttribute(_CurrentWebsiteConfig, "footer_contact_enable")
		If enableFooterContact.ToUpper = "Y" Then
			_strRenderFooterContact = renderingFooterContact()
		End If
	End Sub
	Protected Function renderingFooterContact() As String
		Dim strRenderingFooterContact As String = ""
		Dim strFooterIntro As String = Common.getNodeAttributes(_CurrentWebsiteConfig, "LAYOUT", "footer_intro")
		Dim strFooterPhone As String = Common.getNodeAttributes(_CurrentWebsiteConfig, "LAYOUT", "footer_phone")
		Dim strFooterEmail As String = Common.getNodeAttributes(_CurrentWebsiteConfig, "LAYOUT", "footer_email")
		Dim oCurrentLenderServiceConfigInfo As CLenderServiceConfigInfo = CDownloadedSettings.DownloadLenderServiceConfigInfo(_CurrentWebsiteConfig)
		Dim visibleFooterIntro As String = Common.getVisibleAttribute(_CurrentWebsiteConfig, "footer_intro")
		If (visibleFooterIntro.ToUpper <> "N") Then
			If String.IsNullOrEmpty(strFooterIntro) Then
				strFooterIntro = "Your interest always comes first." ''default
			End If
		Else
			strFooterIntro = ""
		End If
		strRenderingFooterContact += "<div class='divfooter_top'><div class='divfooter_intro'>" & strFooterIntro & "</div>"

		If String.IsNullOrEmpty(strFooterPhone) Then
			strFooterPhone = oCurrentLenderServiceConfigInfo.LenderPhone '' get phone from download
		End If
		If String.IsNullOrEmpty(strFooterEmail) Then
			strFooterEmail = oCurrentLenderServiceConfigInfo.LenderEmail '' get email from download
		End If
		strRenderingFooterContact += "<div class='divfooter_contact'><div>Need assistance?</div>"
		strRenderingFooterContact += "<div><a href='tel:1-" & strFooterPhone & "'>" & strFooterPhone & "</a> Or <a href ='mailto:" & strFooterEmail & "?Subject=Hello%20again' target='_top'>Contact Us</a> <hr/></div></div></div>"

		Return strRenderingFooterContact
	End Function
#End Region
End Class
