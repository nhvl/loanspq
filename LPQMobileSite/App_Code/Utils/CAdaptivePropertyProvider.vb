﻿
Imports System.Web
Namespace Logging

	''' <summary>
	''' This class provides a means to safely integrate custom properties of log4net for use in both non-asp.net context and asp.net.
	''' It solves the issue of asp.net request potentially being served by multiple threads (notion referred to as agile threading)
	''' by relying on HttpContext.Items and internal knowledge of log4net using ToString.
	''' 
	''' Example Usage:
	''' log4net.ThreadContext.Properties["UserName"] =  AdaptivePropertyProvider.Create("UserName", Thread.CurrentPrincipal.Identity.Name)
	''' </summary>
	''' <remarks>
	''' Note this class is not publicly creatable, and that the second version of 
	''' this class is not a generic provides a factory method to more intuitively create these providers.
	''' 
	''' Original solution translated from here:
	''' http://blog.marekstoj.com/2011/12/log4net-contextual-properties-and.html
	''' </remarks>	
	Public Class CAdaptivePropertyProvider(Of T)
		Implements IConvertible	'to support common vb use of CStr

		Private ReadOnly _propertyName As String
		Private ReadOnly _propertyValue As T

#Region "Constructor(s)"

		Protected Friend Sub New(ByVal propertyName As String, ByVal propertyValue As T)
			If String.IsNullOrEmpty(propertyName) Then
				Throw New ArgumentNullException("propertyName")
			End If

			_propertyName = propertyName
			_propertyValue = propertyValue

			If HttpContext.Current IsNot Nothing Then
				HttpContext.Current.Items(GetPropertyName()) = propertyValue
			End If
		End Sub

#End Region

#Region "Overrides of object"

		''' <summary>
		''' Returns stored value from HttpContext.Items (if web context) or _propertyValue
		''' </summary>
		''' <returns></returns>
		''' <remarks>
		''' For all the contexts (ie log4net.ThreadContext), log4net supports the concept of delayed resolution of the context value. 
		''' To be more specific, log4net gets the context value by calling ToString() on the contents of the context bags 
		''' when they're frozen and attached to a logging call. If rather than adding the actual value, you add an object 
		''' who's ToString() provides the value, then you've deferred resolution of what the actual value was.
		''' 
		''' http://piers7.blogspot.com/2005/12/log4net-context-problems-with-aspnet.html
		''' </remarks>
		Public Overrides Function ToString() As String
			If HttpContext.Current IsNot Nothing Then
				Dim item = HttpContext.Current.Items(GetPropertyName())

				Return If(item IsNot Nothing, item.ToString(), Nothing)
			End If

			If Not ReferenceEquals(_propertyValue, Nothing) Then
				Return _propertyValue.ToString()
			End If

			Return Nothing
		End Function

#End Region

#Region "Private helper methods"

		Private Function GetPropertyName() As String
			Return String.Format("{0}{1}", CAdaptivePropertyProvider.PropertyNamePrefix, _propertyName)
		End Function

#End Region


#Region "Conversion Overloads"
		''' <summary>
		''' Convenience conversion since we are really after the string representation 
		''' </summary>
		''' <param name="prop"></param>
		''' <remarks>
		''' Note this only works if we have a strong type (not a reference of Object) 
		''' </remarks>
		Public Shared Widening Operator CType(ByVal prop As CAdaptivePropertyProvider(Of T)) As String
			If prop Is Nothing Then
				Return ""
			End If

			Return prop.ToString()
		End Operator

#End Region

#Region "Implementation of IConvertable"


		Public Function GetTypeCode() As System.TypeCode Implements System.IConvertible.GetTypeCode
			Return TypeCode.String
		End Function

		Public Function ToString1(ByVal provider As System.IFormatProvider) As String Implements System.IConvertible.ToString
			Return ToString()
		End Function

		Public Function ToType(ByVal conversionType As System.Type, ByVal provider As System.IFormatProvider) As Object Implements System.IConvertible.ToType
			Return ToString()
		End Function


		Public Function ToBoolean(ByVal provider As System.IFormatProvider) As Boolean Implements System.IConvertible.ToBoolean
			Return ToString()
		End Function

		Public Function ToByte(ByVal provider As System.IFormatProvider) As Byte Implements System.IConvertible.ToByte
			Return ToString()
		End Function

		Public Function ToChar(ByVal provider As System.IFormatProvider) As Char Implements System.IConvertible.ToChar
			Return ToString()
		End Function

		Public Function ToDateTime(ByVal provider As System.IFormatProvider) As Date Implements System.IConvertible.ToDateTime
			Return ToString()
		End Function

		Public Function ToDecimal(ByVal provider As System.IFormatProvider) As Decimal Implements System.IConvertible.ToDecimal
			Return ToString()
		End Function

		Public Function ToDouble(ByVal provider As System.IFormatProvider) As Double Implements System.IConvertible.ToDouble
			Return ToString()
		End Function

		Public Function ToInt16(ByVal provider As System.IFormatProvider) As Short Implements System.IConvertible.ToInt16
			Return ToString()
		End Function

		Public Function ToInt32(ByVal provider As System.IFormatProvider) As Integer Implements System.IConvertible.ToInt32
			Return ToString()
		End Function

		Public Function ToInt64(ByVal provider As System.IFormatProvider) As Long Implements System.IConvertible.ToInt64
			Return ToString()
		End Function

		Public Function ToSByte(ByVal provider As System.IFormatProvider) As SByte Implements System.IConvertible.ToSByte
			Return ToString()
		End Function

		Public Function ToSingle(ByVal provider As System.IFormatProvider) As Single Implements System.IConvertible.ToSingle
			Return ToString()
		End Function

		
		Public Function ToUInt16(ByVal provider As System.IFormatProvider) As UShort Implements System.IConvertible.ToUInt16
			Return ToString()
		End Function

		Public Function ToUInt32(ByVal provider As System.IFormatProvider) As UInteger Implements System.IConvertible.ToUInt32
			Return ToString()
		End Function

		Public Function ToUInt64(ByVal provider As System.IFormatProvider) As ULong Implements System.IConvertible.ToUInt64
			Return ToString()
		End Function
#End Region
	End Class


	''' <summary>
	''' Convenience class to provide factory methods to conveniently create the generic version
	''' and take advantage of compiler inference.
	''' </summary>	
	Public Class CAdaptivePropertyProvider
		Public Const PropertyNamePrefix As String = "log4net_app_"

#Region "Factory methods"

		Public Shared Function Create(Of T)(ByVal propertyName As String, ByVal propertyValue As T) As CAdaptivePropertyProvider(Of T)
			Return New CAdaptivePropertyProvider(Of T)(propertyName, propertyValue)
		End Function

#End Region
	End Class
End Namespace