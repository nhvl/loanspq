﻿Imports Microsoft.VisualBasic
Imports System.Security.Cryptography

Public Class CSecureRandom

	Private _rng As RNGCryptoServiceProvider = New RNGCryptoServiceProvider()
	Private _uint32Buffer(4) As Byte

	Public Function GetValue(minValue As Int32, maxValue As Int32) As Int32
		If minValue > maxValue Then
			Throw New ArgumentOutOfRangeException("minValue")
		End If
		If minValue = maxValue Then
			Return minValue
		End If

		Dim diff As Int64 = maxValue - minValue
		While True
			_rng.GetBytes(_uint32Buffer)
			Dim rand As UInt32 = BitConverter.ToUInt32(_uint32Buffer, 0)

			Dim max As Int64 = 1 + CType(UInt32.MaxValue, Int64)
			Dim remainder As Int64 = max Mod diff
			If rand < (max - remainder) Then
				Return CType(minValue + (rand Mod diff), Int32)
			End If
		End While
	End Function

End Class
