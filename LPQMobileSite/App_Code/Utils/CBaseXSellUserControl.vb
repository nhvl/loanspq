﻿Imports Microsoft.VisualBasic

Public Class CBaseXSellUserControl
	Inherits CBaseUserControl
	Public Property VlRefinanceGrades() As List(Of CXSellRefinanceGrade) 'vehicle
	Public Property HeRefinanceGrades() As List(Of CXSellRefinanceGrade) 'he
	Public Property CreditCardGrades() As List(Of CXSellNewGrade)
	Public Property VlGrade() As CXSellNewGrade	'vehicle
	Public Property LenderRef() As String
	Public Property SessionId() As String
End Class
