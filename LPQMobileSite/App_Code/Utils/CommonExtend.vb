﻿Imports System.Xml
Imports Workflows.XA.BO
Imports Microsoft.WindowsAzure.Storage.Blob
Imports DBUtils
Imports LPQMobile.BO
Imports System.IO
Imports OfficeOpenXml.Style
Imports System.Data
Imports System.Configuration
Imports System.Text
Imports System.Text.RegularExpressions
Imports System.Web
Imports System.Web.Script.Serialization

Namespace LPQMobile.Utils
	Partial Public Class Common
		Protected Shared LPQMOBILE_CONNECTIONSTRING As String = System.Web.Configuration.WebConfigurationManager.ConnectionStrings("LPQMOBILE_CONNECTIONSTRING").ConnectionString
#Region "Disclosures"
		Public Shared Function BuildEmailDisclosureContent(ByVal disclosureItems As Dictionary(Of String, String), currentWebsiteConfig As CWebsiteConfig) As String
			Dim sb As New StringBuilder()
			If currentWebsiteConfig.InstitutionType = CEnum.InstitutionType.BANK Then
				sb.Append("You recently requested a copy of the Agreement(s) and Disclosure(s) related to your loan or account application. To review your Agreement(s) and Disclosure(s), click the link(s) below.")	 'TODO: text configurable
			Else
				sb.Append("You recently requested a copy of the Agreement(s) and Disclosure(s) related to your loan or membership application. To review your Agreement(s) and Disclosure(s), click the link(s) below.")	'TODO: text configurable
			End If

			sb.Append("<ul>")
			For Each item In disclosureItems
				sb.Append(String.Format("<li><a {0}>{1}</a></li>", item.Value, item.Key))
			Next
			sb.Append("</ul>")
			Return sb.ToString()
		End Function
		Public Shared Function GetDisclosures(websiteConfig As CWebsiteConfig, loanType As String, isComboMode As Boolean, Optional availability As String = "") As List(Of String)
			Dim result As List(Of String)
			If loanType = "XA" Then
				Dim disclosures As List(Of String) = websiteConfig.GetLoanDisclosures("XA_LOAN", availability)
				If availability = "2" OrElse availability = "2a" OrElse availability = "2b" OrElse availability = "2s" Then
					Dim secondaryDisclosures As List(Of String) = getSecondaryDisclosures(websiteConfig, availability)
					If secondaryDisclosures.Count > 0 Then
						disclosures = secondaryDisclosures
					End If
				End If
				result = disclosures
			Else
				Dim CCDisclosures As List(Of String)
				If loanType = "HELOC" Then
					'use DISCLOSURES_LOC when purpose contain keyword "LOC" and <HOME_EQUITY_LOAN><DISCLOSURES_LOC> is not null.
					CCDisclosures = websiteConfig.GetLoanDisclosuresLoc(GetConfigLoanTypeFromShort(loanType))
					If CCDisclosures Is Nothing Or Not CCDisclosures.Any() Then
						CCDisclosures = websiteConfig.GetLoanDisclosures(GetConfigLoanTypeFromShort(loanType))
					End If
				Else
					CCDisclosures = websiteConfig.GetLoanDisclosures(Common.GetConfigLoanTypeFromShort(loanType))
				End If
				result = CCDisclosures

				'Since consumer will be applying for both loan and membership(XA) at the same time, the system needs to show questions and disclosures for both loan and XA.
				If isComboMode Then
					Dim XADisclosures As List(Of String) = websiteConfig.GetLoanDisclosures("XA_LOAN")
					''Dim CCDisclosures_Link As New Dictionary(Of String, String)
					''Dim CCDisclosures_Text As New Dictionary(Of String, String)

					''Dim regExLinkButton As New Regex("^<a.*\shref=['""](.[^""]{3,})['""](\s.*)?>(.+)</a>$", RegexOptions.IgnoreCase)
					''Dim regExContainHtmlTag As New Regex("<.+>", RegexOptions.IgnoreCase)
					''For Each ccDis As String In CCDisclosures
					''    Dim m As Match = regExLinkButton.Match(ccDis.Trim())
					''    If m.Success Then
					''        If CCDisclosures_Link.ContainsKey(m.Groups(1).Value) Then Continue For
					''        CCDisclosures_Link.Add(m.Groups(1).Value, ccDis)
					''    ElseIf regExContainHtmlTag.IsMatch(ccDis) Then
					''        'TODO: don't know what should I do with this - a disclosures mixed between text and link. For exmple: I agree<a href="abc.com">condition</a>
					''    Else 'disclosures with text only
					''        Dim maxLength = ccDis.Trim().Length
					''        If maxLength > 100 Then
					''            maxLength = 100
					''        End If
					''        CCDisclosures_Text.Add(ccDis.Trim().Substring(0, maxLength), ccDis)
					''    End If
					''Next
					''For Each xaDis As String In XADisclosures
					''    Dim m As Match = regExLinkButton.Match(xaDis.Trim())
					''    If m.Success Then
					''        If CCDisclosures_Link.ContainsKey(m.Groups(1).Value) Then
					''            Continue For ' this disclosures existed, abort it
					''        End If
					''    ElseIf regExContainHtmlTag.IsMatch(xaDis) Then
					''    Else
					''        Dim maxLength = xaDis.Trim().Length
					''        If maxLength > 100 Then
					''            maxLength = 100
					''        End If
					''        If CCDisclosures_Text.ContainsKey(xaDis.Trim().Substring(0, maxLength)) Then
					''            Continue For ' this disclosures existed, abort it
					''        End If
					''    End If
					''    Disclosures.Add(xaDis)
					''Next
					Dim comboDisclosuresDic As New Dictionary(Of String, String)
					Dim comboDisclosures As New List(Of String)
					''get loans disclosures
					Dim strCCDis As String = ""
					For Each loanCCDis In CCDisclosures
						strCCDis = HttpUtility.HtmlEncode(loanCCDis.Trim())
						If Not comboDisclosuresDic.ContainsKey(strCCDis) Then
							comboDisclosuresDic.Add(strCCDis, strCCDis)
						End If
					Next
					'' get xa disclosures
					Dim strXADic As String = ""
					For Each xaDic In XADisclosures
						strXADic = HttpUtility.HtmlEncode(xaDic.Trim())
						If Not comboDisclosuresDic.ContainsKey(strXADic) Then
							comboDisclosuresDic.Add(strXADic, strXADic)
						End If
					Next
					''get combo disclosures
					For Each comboDic In comboDisclosuresDic
						comboDisclosures.Add(HttpUtility.HtmlDecode(comboDic.Value))
					Next
					result = comboDisclosures
				End If
			End If
			Return result
		End Function

		Public Shared Function CompileDisclosures(disclosures As List(Of String), ByRef ButtonClosureList As Dictionary(Of String, String), ByRef CheckClosureList As List(Of String)) As Dictionary(Of String, String)
			Dim disclosureLinks As New Dictionary(Of String, String)
			If ButtonClosureList Is Nothing Then
				ButtonClosureList = New Dictionary(Of String, String)()
			End If
			If CheckClosureList Is Nothing Then
				CheckClosureList = New List(Of String)()
			End If
			If disclosures IsNot Nothing AndAlso disclosures.Any() Then

				For Each disclosure As String In disclosures
					Dim regEx As New Regex("^<a(.+)>(.+)</a>$", RegexOptions.IgnoreCase)
					Dim m As Match = regEx.Match(disclosure.Trim())
					If m.Success Then
						Dim sValue As String = m.Groups(1).Value
						If sValue.Contains("DisclosureDisable") Then
							sValue = sValue.Replace("DisclosureDisable", "btn-header-theme")
						End If
						If Not ButtonClosureList.ContainsKey(m.Groups(2).Value) Then	'' don't add item if key exist
							ButtonClosureList.Add(m.Groups(2).Value, sValue)
						End If
					Else
						If Not CheckClosureList.Contains(disclosure) Then
							CheckClosureList.Add(disclosure)
						End If
					End If

					'get links from link button and/or normal disclosure
					Dim regEx2 As New Regex("<a[\s]+([^>]+)>((?:.(?!\<\/a\>))*.)</a>", RegexOptions.IgnoreCase)
					Dim matches = regEx2.Matches(disclosure.Trim())
					If matches IsNot Nothing AndAlso matches.Count > 0 Then
						For Each m2 In matches
							If m2.Success And disclosureLinks.ContainsKey(m2.Groups(2).Value) = False Then
								disclosureLinks.Add(m2.Groups(2).Value, m2.Groups(1).Value)
							End If
						Next
					End If

				Next
			End If
			Return disclosureLinks
		End Function
#End Region


		Public Shared Function GetHomeBankingEnable(config As String) As Boolean
			Dim configXml As New XmlDocument
			configXml.LoadXml(config)
			Return GetHomeBankingEnable(configXml)
		End Function
		Public Shared Function GetHomeBankingEnable(config As XmlDocument) As Boolean
			Dim HomeBankingEnableNode = config.SelectSingleNode("WEBSITE/VISIBLE/@home_banking_SSO_Y")
			If HomeBankingEnableNode IsNot Nothing AndAlso HomeBankingEnableNode.Value = "N" Then
				Return False
			End If
			Return True
		End Function

		Public Shared Function GetDocumentTitleInfoList(ByVal enumDocTitleNode As XmlNode) As List(Of CDocumentTitleInfo)
			Dim result As New List(Of CDocumentTitleInfo)
			If enumDocTitleNode IsNot Nothing AndAlso enumDocTitleNode.HasChildNodes Then
				Dim title As String
				Dim group As String
				Dim code As String
				For Each node As XmlElement In enumDocTitleNode.SelectNodes("ITEM")
					title = ""
					group = ""
					code = ""
					If node.GetAttribute("title") IsNot Nothing Then title = node.GetAttribute("title")
					If node.GetAttribute("doc_group") IsNot Nothing Then group = node.GetAttribute("doc_group")
					If node.GetAttribute("doc_code") IsNot Nothing Then code = node.GetAttribute("doc_code")
					result.Add(New CDocumentTitleInfo() With {.Code = code, .Group = group, .Title = title, .LoanType = ""})
				Next
			End If
			Return result
		End Function

		Public Shared Function ExportExcel(Of T)(wsName As String, title As String, titleDetails As String,
										  data As List(Of T)) As MemoryStream
			Dim stream As New MemoryStream()
			Using excelPackage = New OfficeOpenXml.ExcelPackage
				Dim excelWorksheet = excelPackage.Workbook.Worksheets.Add(wsName)
				excelWorksheet.Cells("A1").Value = title
				excelWorksheet.Cells("A1").Style.Font.Size = 18
				excelWorksheet.Cells("A1").Style.Font.Bold = True
				excelWorksheet.Cells("A2").Value = titleDetails
				excelWorksheet.Cells("A2").Style.Font.Size = 16

				excelWorksheet.Cells("A5").LoadFromCollection(data, True)
				excelWorksheet.Row(5).Style.Font.Bold = True
				excelWorksheet.Column(5).Style.Numberformat.Format = "MM/dd/yyyy HH:mm:ss"
				excelWorksheet.Column(6).Style.Numberformat.Format = "MM/dd/yyyy HH:mm:ss"
				excelWorksheet.Cells.AutoFitColumns()
				stream = New MemoryStream(excelPackage.GetAsByteArray())
			End Using
			Return stream
		End Function
		Public Shared Function GetConditionQuestionXAs(ByVal oConfig As CWebsiteConfig, isCQNewAPI As Boolean, Optional type As String = "") As List(Of CCustomQuestion)
			Dim xaConQuestions As New List(Of CCustomQuestion)
			Dim conQuestions As List(Of CCustomQuestion) = oConfig.GetConditionedQuestions("XA_LOAN")
			If conQuestions.Count = 0 Then
				Return xaConQuestions
			End If
			''check custom
			Dim cqList As List(Of CCustomQuestionXA)
			If isCQNewAPI Then
				cqList = CCustomQuestionNewAPI.getDownloadedXACustomQuestions(oConfig, False, "XA", type)
			Else
				cqList = CCustomQuestionXA.CurrentXACustomQuestions(oConfig, type)
			End If

			For Each conCQ As CCustomQuestion In conQuestions
				For Each cq As CCustomQuestionXA In cqList
					If (conCQ.Name = cq.Name) Then
						Select Case cq.AnswerType.ToLower()
							Case "textbox"
								conCQ.UIType = QuestionUIType.TEXT
								conCQ.DataType = cq.DataType
								Select Case cq.DataType
									Case "D"
										conCQ.maxLength = "10"
									Case "C", "N"
										conCQ.maxLength = String.Empty
									Case "P"
										If String.IsNullOrEmpty(cq.reg_expression) Then
											If Not String.IsNullOrEmpty(cq.maxLength) Then
												conCQ.maxLength = cq.maxLength
											Else
												conCQ.maxLength = "20"
											End If
										End If
									Case Else
										conCQ.maxLength = "150"
								End Select

								conCQ.RegExpression = cq.reg_expression
								conCQ.minLength = cq.minLength
								conCQ.errorMessage = cq.ErrorMessage
								conCQ.ConfirmationText = cq.ConfirmationText

							Case "dropdown"
								conCQ.UIType = QuestionUIType.DROPDOWN
							Case "checkbox"
								conCQ.UIType = QuestionUIType.CHECK
							Case "radio"
								conCQ.UIType = QuestionUIType.RADIO
							Case "hidden"
								conCQ.UIType = QuestionUIType.HIDDEN
						End Select
						conCQ.CustomQuestionOptions = cq.CustomQuestionOptions
						conCQ.Text = cq.Text

						If String.IsNullOrEmpty(Common.SafeString(conCQ.isRequired)) Then
							If (cq.IsRequired) Then
								conCQ.isRequired = "Y"
							Else
								conCQ.isRequired = "N"
							End If

						End If

						xaConQuestions.Add(conCQ)
					End If
				Next
			Next
			Return xaConQuestions
		End Function
		Public Shared Function GetXAAllowedCustomQuestionNames(ByVal oConfig As CWebsiteConfig) As List(Of String)
			Dim productNames As New List(Of String)
			Dim oNodes As XmlNodeList = oConfig._webConfigXML.SelectNodes("XA_LOAN/CUSTOM_QUESTIONS/QUESTION")

			If oNodes IsNot Nothing AndAlso oNodes.Count > 0 Then
				For Each child As XmlNode In oNodes
					If child.Attributes("name") Is Nothing Then
						_log.Error("QUESTION name is not correct.", Nothing)
					End If
					If child.Attributes("name") IsNot Nothing AndAlso child.Attributes("name").InnerXml <> "" Then
						productNames.Add(child.Attributes("name").InnerXml.ToLower)
					End If
				Next
			End If
			Return productNames
		End Function


		Public Shared Function ReadGlobalConfigItems(configNames As List(Of String), lenderID As Guid) As Dictionary(Of String, String)
			If configNames Is Nothing OrElse configNames.Count = 0 Then Return New Dictionary(Of String, String)
			Dim oWhere As New CSQLWhereStringBuilder()
			oWhere.AppendAndIn("Name", configNames.Select(Function(p) New CSQLParamValue(p)).ToList())
			oWhere.AppendAndCondition("LenderID={0}", New CSQLParamValue(lenderID))
			Dim oDB As New CSQLDBUtils(LPQMOBILE_CONNECTIONSTRING)
			Dim result As New Dictionary(Of String, String)
			Try
				Dim oData = oDB.getDataTable("select * from GlobalConfigs " & oWhere.SQL)
				If oData IsNot Nothing AndAlso oData.Rows.Count > 0 Then
					For Each row As DataRow In oData.Rows
						result.Add(SafeString(row("Name")), SafeString(row("ConfigJson")))
					Next
				End If
				For Each missingItem As String In From missingItem1 In configNames Where Not result.ContainsKey(missingItem1)
					result.Add(missingItem, "")
				Next
			Catch ex As Exception
				_log.Error("Error while ReadGlobalConfigItems", ex)
			Finally
				oDB.closeConnection()
			End Try
			Return result
		End Function


		Public Shared Function GetLenderConfigByLenderRef(lenderRef As String, Optional ByVal pbDecrypt As Boolean = True) As CLenderConfig
			Dim oLender As CLenderConfig
			Dim oWhere As New CSQLWhereStringBuilder()
			oWhere.AppendAndCondition("LenderRef={0}", New CSQLParamValue(lenderRef))
			Dim oDB As New CSQLDBUtils(LPQMOBILE_CONNECTIONSTRING)
			Try
				Dim oData = oDB.getDataTable("SELECT * FROM LenderConfigs " & oWhere.SQL)
				If oData IsNot Nothing AndAlso oData.Rows.Count > 0 Then
					Dim row = oData.Rows(0)
					oLender = New CLenderConfig
					oLender.ID = SafeGUID(oData.Rows(0)("LenderConfigID"))
					oLender.OrganizationId = SafeGUID(oData.Rows(0)("OrganizationID"))
					oLender.LenderId = SafeGUID(oData.Rows(0)("LenderID"))
					oLender.LenderRef = SafeString(oData.Rows(0)("LenderRef"))
					oLender.ReturnURL = SafeString(oData.Rows(0)("ReturnURL"))
					oLender.LPQLogin = SafeString(oData.Rows(0)("LPQLogin"))
					oLender.LPQPW = SafeString(oData.Rows(0)("LPQPW"))
					oLender.LPQURL = SafeString(oData.Rows(0)("LPQURL"))
					oLender.LastModifiedDate = SafeDate(oData.Rows(0)("LastModifiedDate"))
					oLender.Note = SafeString(oData.Rows(0)("note"))
					oLender.LastModifiedByUserID = SafeGUID(oData.Rows(0)("LastModifiedByUserID"))
					If oData.Columns.Contains("RevisionID") Then
						oLender.RevisionID = SafeInteger(oData.Rows(0)("RevisionID"))
					End If
					If oData.Columns.Contains("IterationID") Then
						oLender.IterationID = SafeInteger(oData.Rows(0)("IterationID"))
						oLender.LastOpenedByUserID = SafeGUID(oData.Rows(0)("LastOpenedByUserID"))
						oLender.LastOpenedDate = SafeDate(oData.Rows(0)("LastOpenedDate"))
					End If
				End If
			Catch ex As Exception
				_log.Error("Error while GetLenderConfigByLenderRef", ex)
			Finally
				oDB.closeConnection()
			End Try
			Return oLender
		End Function

		Public Shared Sub SendSubmissionRateLimiterNotificationEmails(emails As List(Of String), submittedCounter As Integer, blockingLimitation As Integer, ipAddress As String)
			Dim sb As New StringBuilder()
			'sb.Append("Dear,</br></br>")
			sb.AppendFormat("<p>The following IP address has submitted {0} applications in 60 minutes. This IP address will be blocked from submitting any additional applications once the maximum number ({1}) is reached).</p>", submittedCounter, blockingLimitation)
			sb.Append("</br>")
			sb.AppendFormat("<strong>{0}</strong>", ipAddress)
			sb.Append("</br>")
			sb.Append("<p>You can edit these preferences in the Application Portal Manager.</p>")

			Const subject As String = "Applicant Rate Limiter - Warning Limit Reached"
			For Each email As String In emails
				SendEmail(email, sb.ToString(), subject)
			Next
		End Sub

		Public Shared Sub SendSubmissionRateLimiterBlockingEmails(emails As List(Of String), blockingLimitation As Integer, ipAddress As String)
			Dim sb As New StringBuilder()
			'sb.Append("Dear,</br></br>")
			sb.AppendFormat("<p>The following IP address has submitted the maximum number of applications ({0}) in 60 minutes. This IP address has now been blocked from submitting any additional applications.</p>", blockingLimitation)
			sb.Append("</br>")
			sb.AppendFormat("<strong>{0}</strong>", ipAddress)
			sb.Append("</br>")
			sb.Append("<p>You can edit these preferences in the Application Portal Manager.</p>")

			Const subject As String = "Applicant Rate Limiter - Limit Reached"
			For Each email As String In emails
				SendEmail(email, sb.ToString(), subject)
			Next
		End Sub

		Public Shared Sub SendInstaTouchRestrictionNotificationEmails(emails As List(Of String), submittedCounter As Integer, blockingLimitation As Integer, ipAddress As String)
			Dim sb As New StringBuilder()
			'sb.Append("Dear,</br></br>")
			sb.AppendFormat("<p>The following IP address has touched the InstaTouch feature {0} times in 60 minutes. This IP address will be blocked from touching InstaTouch feature once the maximum number ({1}) is reached).</p>", submittedCounter, blockingLimitation)
			sb.Append("</br>")
			sb.AppendFormat("<strong>{0}</strong>", ipAddress)
			sb.Append("</br>")
			sb.Append("<p>You can edit these preferences in the Application Portal Manager.</p>")

			Const subject As String = "InstaTouch Rate Limiter - Warning Limit Reached"
			For Each email As String In emails
				SendEmail(email, sb.ToString(), subject)
			Next
		End Sub

		Public Shared Sub SendInstaTouchRestrictionBlockingEmails(emails As List(Of String), blockingLimitation As Integer, ipAddress As String)
			Dim sb As New StringBuilder()
			'sb.Append("Dear,</br></br>")
			sb.AppendFormat("<p>The following IP address has reached the maximum number of times({0}) InstaTouch can be accessed in 60 minutes. This IP address has now been blocked from reaching InstaTouch feature.</p>", blockingLimitation)
			sb.Append("</br>")
			sb.AppendFormat("<strong>{0}</strong>", ipAddress)
			sb.Append("</br>")
			sb.Append("<p>You can edit these preferences in the Application Portal Manager.</p>")

			Const subject As String = "InstaTouch Rate Limiter - Limit Reached"
			For Each email As String In emails
				SendEmail(email, sb.ToString(), subject)
			Next
		End Sub


		Public Shared Function CompileLogoUrl(input As String) As String
			If Not String.IsNullOrWhiteSpace(input) AndAlso Regex.IsMatch(input, "^[a-zA-z0-9]{10}\.(png|jpg|jpeg|ico)$") Then
				If Not String.IsNullOrWhiteSpace(ConfigurationManager.AppSettings.Get("AZURE_MEDIA_ACCOUNT_NAME")) AndAlso Not String.IsNullOrWhiteSpace(ConfigurationManager.AppSettings.Get("AZURE_MEDIA_ACCOUNT_KEY")) AndAlso Not String.IsNullOrWhiteSpace(ConfigurationManager.AppSettings.Get("AZURE_LOGO_CONTAINER")) Then
					Return String.Format("https://{0}.blob.core.windows.net/{1}/{2}", ConfigurationManager.AppSettings.Get("AZURE_MEDIA_ACCOUNT_NAME"), ConfigurationManager.AppSettings.Get("AZURE_LOGO_CONTAINER"), input)
				Else
					Return String.Format("/images/logos/{0}", input)
				End If
			End If
			Return input
		End Function

		Public Shared Function ExtractLogoUrl(input As String) As String
			If input.StartsWith("/images/logos/") OrElse (Not String.IsNullOrWhiteSpace(ConfigurationManager.AppSettings.Get("AZURE_MEDIA_ACCOUNT_NAME")) AndAlso Not String.IsNullOrWhiteSpace(ConfigurationManager.AppSettings.Get("AZURE_MEDIA_ACCOUNT_KEY")) AndAlso Not String.IsNullOrWhiteSpace(ConfigurationManager.AppSettings.Get("AZURE_LOGO_CONTAINER"))) Then
				Dim m As Match = Regex.Match(input, "/(?<filename>[a-zA-z0-9]{10}\.(?:png|jpg|jpeg|ico))$")
				If m.Success Then
					Return m.Groups("filename").Value
				End If
			End If
			Return input
		End Function

		Public Shared Function CompileAvatarUrl(input As String) As String
			If Not String.IsNullOrWhiteSpace(input) AndAlso Regex.IsMatch(input, "^[a-zA-z0-9]{10}\.(png|jpg|jpeg|ico)$") Then
				If Not String.IsNullOrWhiteSpace(ConfigurationManager.AppSettings.Get("AZURE_MEDIA_ACCOUNT_NAME")) AndAlso Not String.IsNullOrWhiteSpace(ConfigurationManager.AppSettings.Get("AZURE_MEDIA_ACCOUNT_KEY")) AndAlso Not String.IsNullOrWhiteSpace(ConfigurationManager.AppSettings.Get("AZURE_LOGO_CONTAINER")) Then
					Return String.Format("https://{0}.blob.core.windows.net/{1}/{2}", ConfigurationManager.AppSettings.Get("AZURE_MEDIA_ACCOUNT_NAME"), ConfigurationManager.AppSettings.Get("AZURE_AVATAR_CONTAINER"), input)
				Else
					Return String.Format("/images/avatars/{0}", input)
				End If
			End If
			Return input
		End Function

		Public Shared Function ExtractAvatarUrl(input As String) As String
			If input.StartsWith("/images/avatars/") OrElse (Not String.IsNullOrWhiteSpace(ConfigurationManager.AppSettings.Get("AZURE_MEDIA_ACCOUNT_NAME")) AndAlso Not String.IsNullOrWhiteSpace(ConfigurationManager.AppSettings.Get("AZURE_MEDIA_ACCOUNT_KEY")) AndAlso Not String.IsNullOrWhiteSpace(ConfigurationManager.AppSettings.Get("AZURE_AVATAR_CONTAINER"))) Then
				Dim m As Match = Regex.Match(input, "/(?<filename>[a-zA-z0-9]{10}\.(?:png|jpg|jpeg|ico))$")
				If m.Success Then
					Return m.Groups("filename").Value
				End If
			End If
			Return input
		End Function

		Public Shared Function GetRijndaelManagedKey() As KeyValuePair(Of String, Byte())
			Dim azureMediaAccountName As String = ConfigurationManager.AppSettings.Get("AZURE_MEDIA_ACCOUNT_NAME")
			Dim azureMediaAccountKey As String = ConfigurationManager.AppSettings.Get("AZURE_MEDIA_ACCOUNT_KEY")
			Dim azureConfigFolderName As String = ConfigurationManager.AppSettings.Get("AZURE_CONFIG_CONTAINER")
			Dim key As String = ""
			Dim salt As String = ""
			Dim defaultKey As String = "NDKnO4Q4jIWuriELSOu5b5GSSc1"
			Dim defaultSalt As String = "4Q4jIWuriELS"

			If Not String.IsNullOrWhiteSpace(azureMediaAccountName) AndAlso Not String.IsNullOrWhiteSpace(azureMediaAccountKey) AndAlso Not String.IsNullOrWhiteSpace(azureConfigFolderName) Then
				Dim azureSrv As New CAzureStorageService(azureMediaAccountName, azureMediaAccountKey)
				Dim existInAzure As Boolean = False
				Using ms = azureSrv.ReadBlobToStream(azureConfigFolderName, "rijndaelManagedKeyFile.txt")
					If ms IsNot Nothing Then
						ms.Position = 0
						Using reader = New StreamReader(ms)
							key = reader.ReadLine()
							salt = reader.ReadLine()
						End Using
						existInAzure = True
					End If
				End Using
				If Not existInAzure Then
					'To reduce friction for dev env, we will use default
					key = defaultKey
					salt = defaultSalt
					_log.Info("RijndaelManaged Key File not found so using default key and salt.")
					'End If
					If Not String.IsNullOrWhiteSpace(azureMediaAccountName) AndAlso Not String.IsNullOrWhiteSpace(azureMediaAccountKey) Then
						Dim content() As Byte
						Using ms As New MemoryStream()
							Using sw As New StreamWriter(ms)
								sw.WriteLine(key)
								sw.Write(salt)
							End Using
							content = ms.ToArray()
						End Using
						azureSrv.UploadBlob(azureConfigFolderName, "rijndaelManagedKeyFile.txt", content, "text/plain", BlobContainerPublicAccessType.Off)
					End If
				End If
			Else
				Dim keyFile As String = CConfigFile.getConfig().RijndaelManagedKeyFileLocation
				If Not String.IsNullOrEmpty(keyFile) AndAlso File.Exists(keyFile) Then
					Using reader As New StreamReader(keyFile)
						key = reader.ReadLine()
						salt = reader.ReadLine()
					End Using
				Else
					'Throw New Exception("RijndaelManaged Key File not found")
					'To reduce friction for dev env, we will use default, the path to this file should be in web.comfig
					'<ConfigFile RijndaelManagedKeyFileLocation = "C:\LPQMobileWebsiteConfig\rijndaelManagedKeyFile.txt" />
					key = defaultKey
					salt = defaultSalt
					_log.Info("RijndaelManaged Key File not found so using default key and salt.")
				End If
			End If

			Return New KeyValuePair(Of String, Byte())(key, Encoding.ASCII.GetBytes(salt))
		End Function
		Public Shared Function GetLPQSSOConfigs() As List(Of CLPQSSOConfig)
			Dim azureMediaAccountName As String = ConfigurationManager.AppSettings.Get("AZURE_MEDIA_ACCOUNT_NAME")
			Dim azureMediaAccountKey As String = ConfigurationManager.AppSettings.Get("AZURE_MEDIA_ACCOUNT_KEY")
			Dim azureConfigFolderName As String = ConfigurationManager.AppSettings.Get("AZURE_CONFIG_CONTAINER")
			Dim oLPQSSOConfigs As New List(Of CLPQSSOConfig)
			Dim oDoc As XmlDocument = Nothing
			If Not String.IsNullOrWhiteSpace(azureMediaAccountName) AndAlso Not String.IsNullOrWhiteSpace(azureMediaAccountKey) AndAlso Not String.IsNullOrWhiteSpace(azureConfigFolderName) Then
				Dim azureSrv As New CAzureStorageService(azureMediaAccountName, azureMediaAccountKey)
				Dim configXml As String = ""
				Using ms = azureSrv.ReadBlobToStream(azureConfigFolderName, "LPQSSO_Config.xml")
					If ms IsNot Nothing Then
						ms.Position = 0
						Using reader = New StreamReader(ms)
							configXml = reader.ReadToEnd()
						End Using
					End If
				End Using
				If Not String.IsNullOrEmpty(configXml) Then
					'' load config xml
					oDoc = New XmlDocument()
					oDoc.LoadXml(configXml)
				End If
			Else
				Dim configFile As String = CConfigFile.getConfig().LPQSSO_fileLocation
				If Not String.IsNullOrEmpty(configFile) AndAlso File.Exists(configFile) Then
					'' load config file xml
					oDoc = New XmlDocument()
					oDoc.Load(configFile)
				End If
			End If
			If oDoc Is Nothing Then
				_log.Warn("can't find LPQSSO configuration file")
				Return oLPQSSOConfigs
			End If

			Dim oNodes = oDoc.SelectNodes("LPQSSO_CONFIG/SERVERS/SERVER")
			If oNodes Is Nothing OrElse oNodes.Count = 0 Then
				_log.Warn("Bad LPQSSO configuration file")
				Return oLPQSSOConfigs
			End If

			For Each xmlElement As XmlElement In oNodes
				Dim oLPQSSOConfig As New CLPQSSOConfig
				oLPQSSOConfig.Domain = SafeString(xmlElement.GetAttribute("domain"))
				oLPQSSOConfig.ClientID = SafeString(xmlElement.GetAttribute("clientID"))
				oLPQSSOConfig.ClientSecret = SafeString(xmlElement.GetAttribute("client_secret"))
				oLPQSSOConfig.Comment = SafeString(xmlElement.GetAttribute("comment"))
				oLPQSSOConfigs.Add(oLPQSSOConfig)
			Next
			Return oLPQSSOConfigs
		End Function
		Public Shared Function GetConfigXmlFromFile() As XmlDocument
			Dim azureMediaAccountName As String = ConfigurationManager.AppSettings.Get("AZURE_MEDIA_ACCOUNT_NAME")
			Dim azureMediaAccountKey As String = ConfigurationManager.AppSettings.Get("AZURE_MEDIA_ACCOUNT_KEY")
			Dim azureConfigFolderName As String = ConfigurationManager.AppSettings.Get("AZURE_CONFIG_CONTAINER")
			Dim oDoc As XmlDocument = Nothing
			If Not String.IsNullOrWhiteSpace(azureMediaAccountName) AndAlso Not String.IsNullOrWhiteSpace(azureMediaAccountKey) AndAlso Not String.IsNullOrWhiteSpace(azureConfigFolderName) Then
				Dim azureSrv As New CAzureStorageService(azureMediaAccountName, azureMediaAccountKey)
				Dim configXml As String = ""
				Using ms = azureSrv.ReadBlobToStream(azureConfigFolderName, "config.xml")
					If ms IsNot Nothing Then
						ms.Position = 0
						Using reader = New StreamReader(ms)
							configXml = reader.ReadToEnd()
						End Using
					End If
				End Using
				If Not String.IsNullOrEmpty(configXml) Then
					'' load config xml
					oDoc = New XmlDocument()
					oDoc.LoadXml(configXml)
				End If
			Else
				Dim configFile As String = CConfigFile.getConfig().FileLocation
				If Not String.IsNullOrEmpty(configFile) AndAlso File.Exists(configFile) Then
					'' load config file xml
					oDoc = New XmlDocument()
					oDoc.Load(configFile)
				End If
			End If
			Return oDoc
		End Function

		Public Shared Function GetSpecialAccountTypeList(config As CWebsiteConfig, Optional activeTypeOnly As Boolean = True, Optional activeRoleOnly As Boolean = True, Optional consumerOnly As Boolean = True) As List(Of CSpecialAccountType)
			'this is new version of Special Account
			Dim result As New List(Of CSpecialAccountType)
			Dim parseIntegerValue As Func(Of String, Integer) = Function(s)
																	Dim ret As Integer
																	If Integer.TryParse(s, ret) Then Return ret Else Return 0
																End Function
			Dim parseBooleanValue As Func(Of String, Boolean) = Function(s) s = "Y"
			Dim specialAccountTypesNode As XmlNode
			Dim downloadedXASpecialAccountTypesDoc = CDownloadedSettings.GetXASpecialAccountTypes(config)
			If downloadedXASpecialAccountTypesDoc IsNot Nothing Then
				specialAccountTypesNode = downloadedXASpecialAccountTypesDoc.SelectSingleNode("SETTINGS_EXPORT_RESULT/DATA/SPECIAL_ACCOUNT_TYPES")
			End If
			If specialAccountTypesNode Is Nothing Then
				'fallback
				specialAccountTypesNode = config._webConfigXML.SelectSingleNode("XA_LOAN/SPECIAL_ACCOUNT_TYPES")
			End If
			If specialAccountTypesNode IsNot Nothing AndAlso specialAccountTypesNode.HasChildNodes Then
				Dim specialAccountTypeNodes = specialAccountTypesNode.SelectNodes("SPECIAL_ACCOUNT_TYPE")
				If specialAccountTypeNodes IsNot Nothing AndAlso specialAccountTypeNodes.Count > 0 Then
					For Each specialAccountTypeNode As XmlNode In specialAccountTypeNodes
						Dim specialAccountTypeItem As New CSpecialAccountType
						specialAccountTypeItem.IsActive = Common.GetAndParseInnerTextValue(specialAccountTypeNode, "ISACTIVE", parseBooleanValue, True)
						If activeTypeOnly = True AndAlso specialAccountTypeItem.IsActive = False Then Continue For
						specialAccountTypeItem.AccountName = Common.GetInnerTextValue(specialAccountTypeNode, "ACCOUNTNAME", "")
						specialAccountTypeItem.AccountCode = Common.GetInnerTextValue(specialAccountTypeNode, "ACCOUNTCODE", "")
						specialAccountTypeItem.ShowSpecialInfo = Common.GetAndParseInnerTextValue(specialAccountTypeNode, "SHOWSPECIALINFO", parseBooleanValue, True)
						specialAccountTypeItem.AutoCreateApplicants = Common.GetAndParseInnerTextValue(specialAccountTypeNode, "AUTOCREATEAPPLICANTS", parseBooleanValue, True)
						specialAccountTypeItem.HideBeneficiaryPage = Common.GetAndParseInnerTextValue(specialAccountTypeNode, "HIDEBENEFICIARYPAGE", parseBooleanValue, True)
						specialAccountTypeItem.EnableOnConsumer = Common.GetAndParseInnerTextValue(specialAccountTypeNode, "ENABLEONCONSUMER", parseBooleanValue, True)
						If consumerOnly = True AndAlso specialAccountTypeItem.EnableOnConsumer = False Then Continue For
						Dim specialAccountRoleNodes = specialAccountTypeNode.SelectNodes("SPECIAL_ACCOUNT_ROLE")
						If activeRoleOnly = True AndAlso (specialAccountRoleNodes Is Nothing OrElse specialAccountRoleNodes.Count = 0) Then Continue For
						If specialAccountRoleNodes IsNot Nothing AndAlso specialAccountRoleNodes.Count > 0 Then
							specialAccountTypeItem.Roles = New List(Of CSpecialAccountRole)()
							For Each specialAccountRoleNode As XmlNode In specialAccountRoleNodes
								Dim specialAccountRoleItem As New CSpecialAccountRole
								specialAccountRoleItem.IsActive = Common.GetAndParseInnerTextValue(specialAccountRoleNode, "ISACTIVE", parseBooleanValue, True)
								If activeRoleOnly = True AndAlso specialAccountRoleItem.IsActive = False Then Continue For
								specialAccountRoleItem.RoleType = Common.GetInnerTextValue(specialAccountRoleNode, "ROLETYPE", "")
								specialAccountRoleItem.InstanceMin = Common.GetAndParseInnerTextValue(specialAccountRoleNode, "INSTANCEMIN", parseIntegerValue, 0)
								specialAccountRoleItem.InstanceMax = Common.GetAndParseInnerTextValue(specialAccountRoleNode, "INSTANCEMAX", parseIntegerValue, 0)
								specialAccountRoleItem.AllowIDAuthentication = Common.GetAndParseInnerTextValue(specialAccountRoleNode, "ALLOWIDAUTHENTICATION", parseBooleanValue, True)
								specialAccountRoleItem.AllowIDVerification = Common.GetAndParseInnerTextValue(specialAccountRoleNode, "ALLOWIDVERIFICATION", parseBooleanValue, True)
								specialAccountRoleItem.AllowDebit = Common.GetAndParseInnerTextValue(specialAccountRoleNode, "ALLOWDEBIT", parseBooleanValue, True)
								specialAccountRoleItem.AllowCreditReport = Common.GetAndParseInnerTextValue(specialAccountRoleNode, "ALLOWCREDITREPORT", parseBooleanValue, True)
								specialAccountRoleItem.AllowOfac = Common.GetAndParseInnerTextValue(specialAccountRoleNode, "ALLOWOFAC", parseBooleanValue, True)
								specialAccountRoleItem.ShowJointOption = Common.GetAndParseInnerTextValue(specialAccountRoleNode, "SHOWJOINTOPTION", parseBooleanValue, True)
								specialAccountRoleItem.ShowAddress = Common.GetAndParseInnerTextValue(specialAccountRoleNode, "SHOWADDRESS", parseBooleanValue, True)
								specialAccountRoleItem.ShowEmployment = Common.GetAndParseInnerTextValue(specialAccountRoleNode, "SHOWEMPLOYMENT", parseBooleanValue, True)
								specialAccountRoleItem.ShowContactRelative = Common.GetAndParseInnerTextValue(specialAccountRoleNode, "SHOWCONTACTRELATIVE", parseBooleanValue, True)
								specialAccountRoleItem.ShowIDCard = Common.GetAndParseInnerTextValue(specialAccountRoleNode, "SHOWIDCARD", parseBooleanValue, True)
								specialAccountRoleItem.ShowRelationship = Common.GetAndParseInnerTextValue(specialAccountRoleNode, "SHOWRELATIONSHIP", parseBooleanValue, True)
								specialAccountRoleItem.RoleIndex = Common.GetAndParseInnerTextValue(specialAccountRoleNode, "ROLEINDEX", parseIntegerValue, 0)
								specialAccountRoleItem.DisplayForAtmDebitCard = Common.GetAndParseInnerTextValue(specialAccountRoleNode, "DISPLAYFORATMDEBITCARD", parseBooleanValue, True)
								specialAccountRoleItem.DisplayForYellowHammer = Common.GetAndParseInnerTextValue(specialAccountRoleNode, "DISPLAYFORYELLOWHAMMER", parseBooleanValue, True)
								specialAccountRoleItem.ShowApplicantContactInfo = Common.GetAndParseInnerTextValue(specialAccountRoleNode, "SHOWAPPLICANTCONTACTINFO", parseBooleanValue, True)
								specialAccountRoleItem.MinAge = Common.GetAndParseInnerTextValue(specialAccountRoleNode, "MINAGE", parseIntegerValue, -999999)
								specialAccountRoleItem.MaxAge = Common.GetAndParseInnerTextValue(specialAccountRoleNode, "MAXAGE", parseIntegerValue, -999999)
								specialAccountRoleItem.RequirePrimaryIDCard = Common.GetAndParseInnerTextValue(specialAccountRoleNode, "REQUIREPRIMARYIDCARD", parseBooleanValue, True)
								specialAccountRoleItem.RequireSecondaryIDCard = Common.GetAndParseInnerTextValue(specialAccountRoleNode, "REQUIRESECONDARYIDCARD", parseBooleanValue, True)
								specialAccountRoleItem.ShowOccupancyStatus = Common.GetAndParseInnerTextValue(specialAccountRoleNode, "SHOWOCCUPANCYSTATUS", parseBooleanValue, True)
								specialAccountRoleItem.ShowMaritalStatus = Common.GetAndParseInnerTextValue(specialAccountRoleNode, "SHOWMARITALSTATUS", parseBooleanValue, True)
								specialAccountTypeItem.Roles.Add(specialAccountRoleItem)
							Next
						End If
						If activeRoleOnly = True AndAlso specialAccountTypeItem.Roles.Count = 0 Then Continue For
						result.Add(specialAccountTypeItem)
					Next
				End If
			End If
			Return result
		End Function
		Public Shared Function CleanupQuestionText(input As String) As String
			Dim output = input.Replace(vbTab, "").Replace(vbCr, "").Replace(vbLf, "").Replace(vbCrLf, "")
			output = Regex.Replace(output, "<p.*?>", "<p>")
			If output.StartsWith("<") Then
				Dim sTemp = output.Substring(0, 8) ''get first 8 charaters
				Dim brIndex = sTemp.IndexOf("<br", StringComparison.CurrentCultureIgnoreCase)
				Dim pIndex = sTemp.IndexOf("<p>", StringComparison.CurrentCultureIgnoreCase)
				If brIndex > -1 And pIndex > -1 Then ''contain both <br> and <p> at the beginning
					''remove <br> or <br/> 
					If sTemp.Contains("<br/>") Then
						output = output.Remove(brIndex, 5)
					Else
						output = output.Remove(brIndex, 4)
					End If
					''move <p> at the beginning to the end of the paragraph <p></p>
					Dim sPara = If(output.IndexOf("</p>", StringComparison.CurrentCultureIgnoreCase) > 0, output.Substring(0, output.IndexOf("</p>", StringComparison.CurrentCultureIgnoreCase)), output)	  'compensate for missing closing tag
					sPara = sPara.Replace("<p>", "").Replace("</p>", "") + "<p></p>"
					output = sPara + output.Substring(output.IndexOf("</p>", StringComparison.CurrentCultureIgnoreCase) + 4)
				ElseIf brIndex > -1 And pIndex = -1 Then '' contain only <br>
					''remove <br> or <br/> 
					If sTemp.Contains("<br/>") Then
						output = output.Remove(brIndex, 5)
					Else
						output = output.Remove(brIndex, 4)
					End If
				ElseIf brIndex = -1 And pIndex > -1 Then '' contain only <p>
					''move <p> at the beginning to the end of the paragraph <p></p>
					Dim sPara = If(output.IndexOf("</p>", StringComparison.CurrentCultureIgnoreCase) > 0, output.Substring(0, output.IndexOf("</p>", StringComparison.CurrentCultureIgnoreCase)), output)	'compensate for missing closing tag
					sPara = sPara.Replace("<p>", "").Replace("</p>", "") + "<p></p>"
					output = sPara + output.Substring(output.IndexOf("</p>", StringComparison.CurrentCultureIgnoreCase) + 4)
				End If
			End If
			output = Regex.Replace(output, "<div.*?>", String.Empty).Replace("</div>", String.Empty)
			Return output
		End Function
		Public Shared Function GetBusinessAccountTypeList(config As CWebsiteConfig, Optional activeTypeOnly As Boolean = True, Optional activeRoleOnly As Boolean = True, Optional consumerOnly As Boolean = True) As List(Of CBusinessAccountType)
			'this is new version of Special Account
			Dim result As New List(Of CBusinessAccountType)
			Dim parseIntegerValue As Func(Of String, Integer) = Function(s)
																	Dim ret As Integer
																	If Integer.TryParse(s, ret) Then Return ret Else Return 0
																End Function
			Dim parseBooleanValue As Func(Of String, Boolean) = Function(s) s = "Y"

			Dim businessAccountTypesNode As XmlNode
			Dim downloadedXABusinessAccountTypesDoc = CDownloadedSettings.GetXABusinessAccountTypes(config)
			If downloadedXABusinessAccountTypesDoc IsNot Nothing Then
				businessAccountTypesNode = downloadedXABusinessAccountTypesDoc.SelectSingleNode("SETTINGS_EXPORT_RESULT/DATA/XA_BUSINESS_ACCOUNT_TYPES")
			End If
			If businessAccountTypesNode Is Nothing Then
				'fallback
				businessAccountTypesNode = config._webConfigXML.SelectSingleNode("XA_LOAN/XA_BUSINESS_ACCOUNT_TYPES")
			End If
			If businessAccountTypesNode IsNot Nothing AndAlso businessAccountTypesNode.HasChildNodes Then
				Dim businessAccountTypeNodes = businessAccountTypesNode.SelectNodes("XA_BUSINESS_ACCOUNT_TYPE")
				If businessAccountTypeNodes IsNot Nothing AndAlso businessAccountTypeNodes.Count > 0 Then
					For Each businesAccountTypeNode As XmlNode In businessAccountTypeNodes
						Dim businessAccountTypeItem As New CBusinessAccountType
						businessAccountTypeItem.IsActive = Common.GetAndParseInnerTextValue(businesAccountTypeNode, "ISACTIVE", parseBooleanValue, True)
						If activeTypeOnly = True AndAlso businessAccountTypeItem.IsActive = False Then Continue For
						businessAccountTypeItem.BusinessType = Common.GetInnerTextValue(businesAccountTypeNode, "BUSINESSTYPE", "")
						businessAccountTypeItem.AccountCode = Common.GetInnerTextValue(businesAccountTypeNode, "ACCOUNTCODE", "")
						businessAccountTypeItem.ShowDoingBusinessAs = Common.GetAndParseInnerTextValue(businesAccountTypeNode, "SHOWDOINGBUSINESSAS", parseBooleanValue, True)
						businessAccountTypeItem.AutoCreateApplicants = Common.GetAndParseInnerTextValue(businesAccountTypeNode, "AUTOCREATEAPPLICANTS", parseBooleanValue, True)
						businessAccountTypeItem.EnableOnConsumer = Common.GetAndParseInnerTextValue(businesAccountTypeNode, "ENABLEONCONSUMER", parseBooleanValue, True)
						If consumerOnly = True AndAlso businessAccountTypeItem.EnableOnConsumer = False Then Continue For
						Dim businessAccountRoleNodes = businesAccountTypeNode.SelectNodes("XA_BUSINESS_ACCOUNT_ROLE")
						If activeRoleOnly = True AndAlso (businessAccountRoleNodes Is Nothing OrElse businessAccountRoleNodes.Count = 0) Then Continue For
						If businessAccountRoleNodes IsNot Nothing AndAlso businessAccountRoleNodes.Count > 0 Then
							businessAccountTypeItem.Roles = New List(Of CBusinessAccountRole)()
							For Each businessAccountRoleNode As XmlNode In businessAccountRoleNodes
								Dim businessAccountRoleItem As New CBusinessAccountRole
								businessAccountRoleItem.IsActive = Common.GetAndParseInnerTextValue(businessAccountRoleNode, "ISACTIVE", parseBooleanValue, True)
								If activeRoleOnly = True AndAlso businessAccountRoleItem.IsActive = False Then Continue For
								businessAccountRoleItem.RoleType = Common.GetInnerTextValue(businessAccountRoleNode, "ROLETYPE", "")
								businessAccountRoleItem.InstanceMin = Common.GetAndParseInnerTextValue(businessAccountRoleNode, "INSTANCEMIN", parseIntegerValue, 0)
								businessAccountRoleItem.InstanceMax = Common.GetAndParseInnerTextValue(businessAccountRoleNode, "INSTANCEMAX", parseIntegerValue, 0)
								businessAccountRoleItem.AllowIDAuthentication = Common.GetAndParseInnerTextValue(businessAccountRoleNode, "ALLOWIDAUTHENTICATION", parseBooleanValue, True)
								businessAccountRoleItem.AllowIDVerification = Common.GetAndParseInnerTextValue(businessAccountRoleNode, "ALLOWIDVERIFICATION", parseBooleanValue, True)
								businessAccountRoleItem.AllowDebit = Common.GetAndParseInnerTextValue(businessAccountRoleNode, "ALLOWDEBIT", parseBooleanValue, True)
								businessAccountRoleItem.AllowCreditReport = Common.GetAndParseInnerTextValue(businessAccountRoleNode, "ALLOWCREDITREPORT", parseBooleanValue, True)
								businessAccountRoleItem.AllowOfac = Common.GetAndParseInnerTextValue(businessAccountRoleNode, "ALLOWOFAC", parseBooleanValue, True)
								businessAccountRoleItem.ShowJointOption = Common.GetAndParseInnerTextValue(businessAccountRoleNode, "SHOWJOINTOPTION", parseBooleanValue, True)
								businessAccountRoleItem.ShowAddress = Common.GetAndParseInnerTextValue(businessAccountRoleNode, "SHOWADDRESS", parseBooleanValue, True)
								businessAccountRoleItem.ShowEmployment = Common.GetAndParseInnerTextValue(businessAccountRoleNode, "SHOWEMPLOYMENT", parseBooleanValue, True)
								businessAccountRoleItem.ShowContactRelative = Common.GetAndParseInnerTextValue(businessAccountRoleNode, "SHOWCONTACTRELATIVE", parseBooleanValue, True)
								businessAccountRoleItem.ShowIDCard = Common.GetAndParseInnerTextValue(businessAccountRoleNode, "SHOWIDCARD", parseBooleanValue, True)
								businessAccountRoleItem.ShowRelationship = Common.GetAndParseInnerTextValue(businessAccountRoleNode, "SHOWRELATIONSHIP", parseBooleanValue, True)
								businessAccountRoleItem.RoleIndex = Common.GetAndParseInnerTextValue(businessAccountRoleNode, "ROLEINDEX", parseIntegerValue, 0)
								businessAccountRoleItem.DisplayForAtmDebitCard = Common.GetAndParseInnerTextValue(businessAccountRoleNode, "DISPLAYFORATMDEBITCARD", parseBooleanValue, True)
								businessAccountRoleItem.ShowApplicantContactInfo = Common.GetAndParseInnerTextValue(businessAccountRoleNode, "SHOWAPPLICANTCONTACTINFO", parseBooleanValue, True)
								businessAccountRoleItem.RequirePrimaryIDCard = Common.GetAndParseInnerTextValue(businessAccountRoleNode, "REQUIREPRIMARYIDCARD", parseBooleanValue, True)
								businessAccountRoleItem.RequireSecondaryIDCard = Common.GetAndParseInnerTextValue(businessAccountRoleNode, "REQUIRESECONDARYIDCARD", parseBooleanValue, True)
								businessAccountRoleItem.ShowOccupancyStatus = Common.GetAndParseInnerTextValue(businessAccountRoleNode, "SHOWOCCUPANCYSTATUS", parseBooleanValue, True)
								businessAccountTypeItem.Roles.Add(businessAccountRoleItem)
							Next
						End If
						If activeRoleOnly = True AndAlso businessAccountTypeItem.Roles.Count = 0 Then Continue For
						result.Add(businessAccountTypeItem)
					Next
				End If
			End If
			Return result
		End Function

		Public Shared CountryList As New Dictionary(Of String, CCountry) From {
			{"US", New CCountry() With {.Name = "United States of America", .Code = "840", .Code2 = "US", .Code3 = "USA", .Region = "Americas", .SubRegion = "Northern America", .Status = 1}},
			{"AF", New CCountry() With {.Name = "Afghanistan", .Code = "4", .Code2 = "AF", .Code3 = "AFG", .Region = "Asia", .SubRegion = "Southern Asia", .Status = 1}},
			{"AX", New CCountry() With {.Name = "Åland Islands", .Code = "248", .Code2 = "AX", .Code3 = "ALA", .Region = "Europe", .SubRegion = "Northern Europe", .Status = 1}},
			{"AL", New CCountry() With {.Name = "Albania", .Code = "8", .Code2 = "AL", .Code3 = "ALB", .Region = "Europe", .SubRegion = "Southern Europe", .Status = 1}},
			{"DZ", New CCountry() With {.Name = "Algeria", .Code = "12", .Code2 = "DZ", .Code3 = "DZA", .Region = "Africa", .SubRegion = "Northern Africa", .Status = 1}},
			{"AS", New CCountry() With {.Name = "American Samoa", .Code = "16", .Code2 = "AS", .Code3 = "ASM", .Region = "Oceania", .SubRegion = "Polynesia", .Status = 1}},
			{"AD", New CCountry() With {.Name = "Andorra", .Code = "20", .Code2 = "AD", .Code3 = "AND", .Region = "Europe", .SubRegion = "Southern Europe", .Status = 1}},
			{"AO", New CCountry() With {.Name = "Angola", .Code = "24", .Code2 = "AO", .Code3 = "AGO", .Region = "Africa", .SubRegion = "Sub-Saharan Africa", .Status = 1}},
			{"AI", New CCountry() With {.Name = "Anguilla", .Code = "660", .Code2 = "AI", .Code3 = "AIA", .Region = "Americas", .SubRegion = "Latin America and the Caribbean", .Status = 1}},
			{"AQ", New CCountry() With {.Name = "Antarctica", .Code = "10", .Code2 = "AQ", .Code3 = "ATA", .Region = "", .SubRegion = "", .Status = 1}},
			{"AG", New CCountry() With {.Name = "Antigua and Barbuda", .Code = "28", .Code2 = "AG", .Code3 = "ATG", .Region = "Americas", .SubRegion = "Latin America and the Caribbean", .Status = 1}},
			{"AR", New CCountry() With {.Name = "Argentina", .Code = "32", .Code2 = "AR", .Code3 = "ARG", .Region = "Americas", .SubRegion = "Latin America and the Caribbean", .Status = 1}},
			{"AM", New CCountry() With {.Name = "Armenia", .Code = "51", .Code2 = "AM", .Code3 = "ARM", .Region = "Asia", .SubRegion = "Western Asia", .Status = 1}},
			{"AW", New CCountry() With {.Name = "Aruba", .Code = "533", .Code2 = "AW", .Code3 = "ABW", .Region = "Americas", .SubRegion = "Latin America and the Caribbean", .Status = 1}},
			{"AU", New CCountry() With {.Name = "Australia", .Code = "36", .Code2 = "AU", .Code3 = "AUS", .Region = "Oceania", .SubRegion = "Australia and New Zealand", .Status = 1}},
			{"AT", New CCountry() With {.Name = "Austria", .Code = "40", .Code2 = "AT", .Code3 = "AUT", .Region = "Europe", .SubRegion = "Western Europe", .Status = 1}},
			{"AZ", New CCountry() With {.Name = "Azerbaijan", .Code = "31", .Code2 = "AZ", .Code3 = "AZE", .Region = "Asia", .SubRegion = "Western Asia", .Status = 1}},
			{"BS", New CCountry() With {.Name = "Bahamas", .Code = "44", .Code2 = "BS", .Code3 = "BHS", .Region = "Americas", .SubRegion = "Latin America and the Caribbean", .Status = 1}},
			{"BH", New CCountry() With {.Name = "Bahrain", .Code = "48", .Code2 = "BH", .Code3 = "BHR", .Region = "Asia", .SubRegion = "Western Asia", .Status = 1}},
			{"BD", New CCountry() With {.Name = "Bangladesh", .Code = "50", .Code2 = "BD", .Code3 = "BGD", .Region = "Asia", .SubRegion = "Southern Asia", .Status = 1}},
			{"BB", New CCountry() With {.Name = "Barbados", .Code = "52", .Code2 = "BB", .Code3 = "BRB", .Region = "Americas", .SubRegion = "Latin America and the Caribbean", .Status = 1}},
			{"BY", New CCountry() With {.Name = "Belarus", .Code = "112", .Code2 = "BY", .Code3 = "BLR", .Region = "Europe", .SubRegion = "Eastern Europe", .Status = 1}},
			{"BE", New CCountry() With {.Name = "Belgium", .Code = "56", .Code2 = "BE", .Code3 = "BEL", .Region = "Europe", .SubRegion = "Western Europe", .Status = 1}},
			{"BZ", New CCountry() With {.Name = "Belize", .Code = "84", .Code2 = "BZ", .Code3 = "BLZ", .Region = "Americas", .SubRegion = "Latin America and the Caribbean", .Status = 1}},
			{"BJ", New CCountry() With {.Name = "Benin", .Code = "204", .Code2 = "BJ", .Code3 = "BEN", .Region = "Africa", .SubRegion = "Sub-Saharan Africa", .Status = 1}},
			{"BM", New CCountry() With {.Name = "Bermuda", .Code = "60", .Code2 = "BM", .Code3 = "BMU", .Region = "Americas", .SubRegion = "Northern America", .Status = 1}},
			{"BT", New CCountry() With {.Name = "Bhutan", .Code = "64", .Code2 = "BT", .Code3 = "BTN", .Region = "Asia", .SubRegion = "Southern Asia", .Status = 1}},
			{"BO", New CCountry() With {.Name = "Bolivia (Plurinational State of)", .Code = "68", .Code2 = "BO", .Code3 = "BOL", .Region = "Americas", .SubRegion = "Latin America and the Caribbean", .Status = 1}},
			{"BQ", New CCountry() With {.Name = "Bonaire, Sint Eustatius and Saba", .Code = "535", .Code2 = "BQ", .Code3 = "BES", .Region = "Americas", .SubRegion = "Latin America and the Caribbean", .Status = 1}},
			{"BA", New CCountry() With {.Name = "Bosnia and Herzegovina", .Code = "70", .Code2 = "BA", .Code3 = "BIH", .Region = "Europe", .SubRegion = "Southern Europe", .Status = 1}},
			{"BW", New CCountry() With {.Name = "Botswana", .Code = "72", .Code2 = "BW", .Code3 = "BWA", .Region = "Africa", .SubRegion = "Sub-Saharan Africa", .Status = 1}},
			{"BV", New CCountry() With {.Name = "Bouvet Island", .Code = "74", .Code2 = "BV", .Code3 = "BVT", .Region = "Americas", .SubRegion = "Latin America and the Caribbean", .Status = 1}},
			{"BR", New CCountry() With {.Name = "Brazil", .Code = "76", .Code2 = "BR", .Code3 = "BRA", .Region = "Americas", .SubRegion = "Latin America and the Caribbean", .Status = 1}},
			{"IO", New CCountry() With {.Name = "British Indian Ocean Territory", .Code = "86", .Code2 = "IO", .Code3 = "IOT", .Region = "Africa", .SubRegion = "Sub-Saharan Africa", .Status = 1}},
			{"BN", New CCountry() With {.Name = "Brunei Darussalam", .Code = "96", .Code2 = "BN", .Code3 = "BRN", .Region = "Asia", .SubRegion = "South-eastern Asia", .Status = 1}},
			{"BG", New CCountry() With {.Name = "Bulgaria", .Code = "100", .Code2 = "BG", .Code3 = "BGR", .Region = "Europe", .SubRegion = "Eastern Europe", .Status = 1}},
			{"BF", New CCountry() With {.Name = "Burkina Faso", .Code = "854", .Code2 = "BF", .Code3 = "BFA", .Region = "Africa", .SubRegion = "Sub-Saharan Africa", .Status = 1}},
			{"BI", New CCountry() With {.Name = "Burundi", .Code = "108", .Code2 = "BI", .Code3 = "BDI", .Region = "Africa", .SubRegion = "Sub-Saharan Africa", .Status = 1}},
			{"CV", New CCountry() With {.Name = "Cabo Verde", .Code = "132", .Code2 = "CV", .Code3 = "CPV", .Region = "Africa", .SubRegion = "Sub-Saharan Africa", .Status = 1}},
			{"KH", New CCountry() With {.Name = "Cambodia", .Code = "116", .Code2 = "KH", .Code3 = "KHM", .Region = "Asia", .SubRegion = "South-eastern Asia", .Status = 1}},
			{"CM", New CCountry() With {.Name = "Cameroon", .Code = "120", .Code2 = "CM", .Code3 = "CMR", .Region = "Africa", .SubRegion = "Sub-Saharan Africa", .Status = 1}},
			{"CA", New CCountry() With {.Name = "Canada", .Code = "124", .Code2 = "CA", .Code3 = "CAN", .Region = "Americas", .SubRegion = "Northern America", .Status = 1}},
			{"KY", New CCountry() With {.Name = "Cayman Islands", .Code = "136", .Code2 = "KY", .Code3 = "CYM", .Region = "Americas", .SubRegion = "Latin America and the Caribbean", .Status = 1}},
			{"CF", New CCountry() With {.Name = "Central African Republic", .Code = "140", .Code2 = "CF", .Code3 = "CAF", .Region = "Africa", .SubRegion = "Sub-Saharan Africa", .Status = 1}},
			{"TD", New CCountry() With {.Name = "Chad", .Code = "148", .Code2 = "TD", .Code3 = "TCD", .Region = "Africa", .SubRegion = "Sub-Saharan Africa", .Status = 1}},
			{"CL", New CCountry() With {.Name = "Chile", .Code = "152", .Code2 = "CL", .Code3 = "CHL", .Region = "Americas", .SubRegion = "Latin America and the Caribbean", .Status = 1}},
			{"CN", New CCountry() With {.Name = "China", .Code = "156", .Code2 = "CN", .Code3 = "CHN", .Region = "Asia", .SubRegion = "Eastern Asia", .Status = 1}},
			{"CX", New CCountry() With {.Name = "Christmas Island", .Code = "162", .Code2 = "CX", .Code3 = "CXR", .Region = "Oceania", .SubRegion = "Australia and New Zealand", .Status = 1}},
			{"CC", New CCountry() With {.Name = "Cocos (Keeling) Islands", .Code = "166", .Code2 = "CC", .Code3 = "CCK", .Region = "Oceania", .SubRegion = "Australia and New Zealand", .Status = 1}},
			{"CO", New CCountry() With {.Name = "Colombia", .Code = "170", .Code2 = "CO", .Code3 = "COL", .Region = "Americas", .SubRegion = "Latin America and the Caribbean", .Status = 1}},
			{"KM", New CCountry() With {.Name = "Comoros", .Code = "174", .Code2 = "KM", .Code3 = "COM", .Region = "Africa", .SubRegion = "Sub-Saharan Africa", .Status = 1}},
			{"CG", New CCountry() With {.Name = "Congo", .Code = "178", .Code2 = "CG", .Code3 = "COG", .Region = "Africa", .SubRegion = "Sub-Saharan Africa", .Status = 1}},
			{"CD", New CCountry() With {.Name = "Congo, Democratic Republic of the", .Code = "180", .Code2 = "CD", .Code3 = "COD", .Region = "Africa", .SubRegion = "Sub-Saharan Africa", .Status = 1}},
			{"CK", New CCountry() With {.Name = "Cook Islands", .Code = "184", .Code2 = "CK", .Code3 = "COK", .Region = "Oceania", .SubRegion = "Polynesia", .Status = 1}},
			{"CR", New CCountry() With {.Name = "Costa Rica", .Code = "188", .Code2 = "CR", .Code3 = "CRI", .Region = "Americas", .SubRegion = "Latin America and the Caribbean", .Status = 1}},
			{"CI", New CCountry() With {.Name = "Côte d'Ivoire", .Code = "384", .Code2 = "CI", .Code3 = "CIV", .Region = "Africa", .SubRegion = "Sub-Saharan Africa", .Status = 1}},
			{"HR", New CCountry() With {.Name = "Croatia", .Code = "191", .Code2 = "HR", .Code3 = "HRV", .Region = "Europe", .SubRegion = "Southern Europe", .Status = 1}},
			{"CU", New CCountry() With {.Name = "Cuba", .Code = "192", .Code2 = "CU", .Code3 = "CUB", .Region = "Americas", .SubRegion = "Latin America and the Caribbean", .Status = 1}},
			{"CW", New CCountry() With {.Name = "Curaçao", .Code = "531", .Code2 = "CW", .Code3 = "CUW", .Region = "Americas", .SubRegion = "Latin America and the Caribbean", .Status = 1}},
			{"CY", New CCountry() With {.Name = "Cyprus", .Code = "196", .Code2 = "CY", .Code3 = "CYP", .Region = "Asia", .SubRegion = "Western Asia", .Status = 1}},
			{"CZ", New CCountry() With {.Name = "Czechia", .Code = "203", .Code2 = "CZ", .Code3 = "CZE", .Region = "Europe", .SubRegion = "Eastern Europe", .Status = 1}},
			{"DK", New CCountry() With {.Name = "Denmark", .Code = "208", .Code2 = "DK", .Code3 = "DNK", .Region = "Europe", .SubRegion = "Northern Europe", .Status = 1}},
			{"DJ", New CCountry() With {.Name = "Djibouti", .Code = "262", .Code2 = "DJ", .Code3 = "DJI", .Region = "Africa", .SubRegion = "Sub-Saharan Africa", .Status = 1}},
			{"DM", New CCountry() With {.Name = "Dominica", .Code = "212", .Code2 = "DM", .Code3 = "DMA", .Region = "Americas", .SubRegion = "Latin America and the Caribbean", .Status = 1}},
			{"DO", New CCountry() With {.Name = "Dominican Republic", .Code = "214", .Code2 = "DO", .Code3 = "DOM", .Region = "Americas", .SubRegion = "Latin America and the Caribbean", .Status = 1}},
			{"EC", New CCountry() With {.Name = "Ecuador", .Code = "218", .Code2 = "EC", .Code3 = "ECU", .Region = "Americas", .SubRegion = "Latin America and the Caribbean", .Status = 1}},
			{"EG", New CCountry() With {.Name = "Egypt", .Code = "818", .Code2 = "EG", .Code3 = "EGY", .Region = "Africa", .SubRegion = "Northern Africa", .Status = 1}},
			{"SV", New CCountry() With {.Name = "El Salvador", .Code = "222", .Code2 = "SV", .Code3 = "SLV", .Region = "Americas", .SubRegion = "Latin America and the Caribbean", .Status = 1}},
			{"GQ", New CCountry() With {.Name = "Equatorial Guinea", .Code = "226", .Code2 = "GQ", .Code3 = "GNQ", .Region = "Africa", .SubRegion = "Sub-Saharan Africa", .Status = 1}},
			{"ER", New CCountry() With {.Name = "Eritrea", .Code = "232", .Code2 = "ER", .Code3 = "ERI", .Region = "Africa", .SubRegion = "Sub-Saharan Africa", .Status = 1}},
			{"EE", New CCountry() With {.Name = "Estonia", .Code = "233", .Code2 = "EE", .Code3 = "EST", .Region = "Europe", .SubRegion = "Northern Europe", .Status = 1}},
			{"SZ", New CCountry() With {.Name = "Eswatini", .Code = "748", .Code2 = "SZ", .Code3 = "SWZ", .Region = "Africa", .SubRegion = "Sub-Saharan Africa", .Status = 1}},
			{"ET", New CCountry() With {.Name = "Ethiopia", .Code = "231", .Code2 = "ET", .Code3 = "ETH", .Region = "Africa", .SubRegion = "Sub-Saharan Africa", .Status = 1}},
			{"FK", New CCountry() With {.Name = "Falkland Islands (Malvinas)", .Code = "238", .Code2 = "FK", .Code3 = "FLK", .Region = "Americas", .SubRegion = "Latin America and the Caribbean", .Status = 1}},
			{"FO", New CCountry() With {.Name = "Faroe Islands", .Code = "234", .Code2 = "FO", .Code3 = "FRO", .Region = "Europe", .SubRegion = "Northern Europe", .Status = 1}},
			{"FJ", New CCountry() With {.Name = "Fiji", .Code = "242", .Code2 = "FJ", .Code3 = "FJI", .Region = "Oceania", .SubRegion = "Melanesia", .Status = 1}},
			{"FI", New CCountry() With {.Name = "Finland", .Code = "246", .Code2 = "FI", .Code3 = "FIN", .Region = "Europe", .SubRegion = "Northern Europe", .Status = 1}},
			{"FR", New CCountry() With {.Name = "France", .Code = "250", .Code2 = "FR", .Code3 = "FRA", .Region = "Europe", .SubRegion = "Western Europe", .Status = 1}},
			{"GF", New CCountry() With {.Name = "French Guiana", .Code = "254", .Code2 = "GF", .Code3 = "GUF", .Region = "Americas", .SubRegion = "Latin America and the Caribbean", .Status = 1}},
			{"PF", New CCountry() With {.Name = "French Polynesia", .Code = "258", .Code2 = "PF", .Code3 = "PYF", .Region = "Oceania", .SubRegion = "Polynesia", .Status = 1}},
			{"TF", New CCountry() With {.Name = "French Southern Territories", .Code = "260", .Code2 = "TF", .Code3 = "ATF", .Region = "Africa", .SubRegion = "Sub-Saharan Africa", .Status = 1}},
			{"GA", New CCountry() With {.Name = "Gabon", .Code = "266", .Code2 = "GA", .Code3 = "GAB", .Region = "Africa", .SubRegion = "Sub-Saharan Africa", .Status = 1}},
			{"GM", New CCountry() With {.Name = "Gambia", .Code = "270", .Code2 = "GM", .Code3 = "GMB", .Region = "Africa", .SubRegion = "Sub-Saharan Africa", .Status = 1}},
			{"GE", New CCountry() With {.Name = "Georgia", .Code = "268", .Code2 = "GE", .Code3 = "GEO", .Region = "Asia", .SubRegion = "Western Asia", .Status = 1}},
			{"DE", New CCountry() With {.Name = "Germany", .Code = "276", .Code2 = "DE", .Code3 = "DEU", .Region = "Europe", .SubRegion = "Western Europe", .Status = 1}},
			{"GH", New CCountry() With {.Name = "Ghana", .Code = "288", .Code2 = "GH", .Code3 = "GHA", .Region = "Africa", .SubRegion = "Sub-Saharan Africa", .Status = 1}},
			{"GI", New CCountry() With {.Name = "Gibraltar", .Code = "292", .Code2 = "GI", .Code3 = "GIB", .Region = "Europe", .SubRegion = "Southern Europe", .Status = 1}},
			{"GR", New CCountry() With {.Name = "Greece", .Code = "300", .Code2 = "GR", .Code3 = "GRC", .Region = "Europe", .SubRegion = "Southern Europe", .Status = 1}},
			{"GL", New CCountry() With {.Name = "Greenland", .Code = "304", .Code2 = "GL", .Code3 = "GRL", .Region = "Americas", .SubRegion = "Northern America", .Status = 1}},
			{"GD", New CCountry() With {.Name = "Grenada", .Code = "308", .Code2 = "GD", .Code3 = "GRD", .Region = "Americas", .SubRegion = "Latin America and the Caribbean", .Status = 1}},
			{"GP", New CCountry() With {.Name = "Guadeloupe", .Code = "312", .Code2 = "GP", .Code3 = "GLP", .Region = "Americas", .SubRegion = "Latin America and the Caribbean", .Status = 1}},
			{"GU", New CCountry() With {.Name = "Guam", .Code = "316", .Code2 = "GU", .Code3 = "GUM", .Region = "Oceania", .SubRegion = "Micronesia", .Status = 1}},
			{"GT", New CCountry() With {.Name = "Guatemala", .Code = "320", .Code2 = "GT", .Code3 = "GTM", .Region = "Americas", .SubRegion = "Latin America and the Caribbean", .Status = 1}},
			{"GG", New CCountry() With {.Name = "Guernsey", .Code = "831", .Code2 = "GG", .Code3 = "GGY", .Region = "Europe", .SubRegion = "Northern Europe", .Status = 1}},
			{"GN", New CCountry() With {.Name = "Guinea", .Code = "324", .Code2 = "GN", .Code3 = "GIN", .Region = "Africa", .SubRegion = "Sub-Saharan Africa", .Status = 1}},
			{"GW", New CCountry() With {.Name = "Guinea-Bissau", .Code = "624", .Code2 = "GW", .Code3 = "GNB", .Region = "Africa", .SubRegion = "Sub-Saharan Africa", .Status = 1}},
			{"GY", New CCountry() With {.Name = "Guyana", .Code = "328", .Code2 = "GY", .Code3 = "GUY", .Region = "Americas", .SubRegion = "Latin America and the Caribbean", .Status = 1}},
			{"HT", New CCountry() With {.Name = "Haiti", .Code = "332", .Code2 = "HT", .Code3 = "HTI", .Region = "Americas", .SubRegion = "Latin America and the Caribbean", .Status = 1}},
			{"HM", New CCountry() With {.Name = "Heard Island and McDonald Islands", .Code = "334", .Code2 = "HM", .Code3 = "HMD", .Region = "Oceania", .SubRegion = "Australia and New Zealand", .Status = 1}},
			{"VA", New CCountry() With {.Name = "Holy See", .Code = "336", .Code2 = "VA", .Code3 = "VAT", .Region = "Europe", .SubRegion = "Southern Europe", .Status = 1}},
			{"HN", New CCountry() With {.Name = "Honduras", .Code = "340", .Code2 = "HN", .Code3 = "HND", .Region = "Americas", .SubRegion = "Latin America and the Caribbean", .Status = 1}},
			{"HK", New CCountry() With {.Name = "Hong Kong", .Code = "344", .Code2 = "HK", .Code3 = "HKG", .Region = "Asia", .SubRegion = "Eastern Asia", .Status = 1}},
			{"HU", New CCountry() With {.Name = "Hungary", .Code = "348", .Code2 = "HU", .Code3 = "HUN", .Region = "Europe", .SubRegion = "Eastern Europe", .Status = 1}},
			{"IS", New CCountry() With {.Name = "Iceland", .Code = "352", .Code2 = "IS", .Code3 = "ISL", .Region = "Europe", .SubRegion = "Northern Europe", .Status = 1}},
			{"IN", New CCountry() With {.Name = "India", .Code = "356", .Code2 = "IN", .Code3 = "IND", .Region = "Asia", .SubRegion = "Southern Asia", .Status = 1}},
			{"ID", New CCountry() With {.Name = "Indonesia", .Code = "360", .Code2 = "ID", .Code3 = "IDN", .Region = "Asia", .SubRegion = "South-eastern Asia", .Status = 1}},
			{"IR", New CCountry() With {.Name = "Iran (Islamic Republic of)", .Code = "364", .Code2 = "IR", .Code3 = "IRN", .Region = "Asia", .SubRegion = "Southern Asia", .Status = 1}},
			{"IQ", New CCountry() With {.Name = "Iraq", .Code = "368", .Code2 = "IQ", .Code3 = "IRQ", .Region = "Asia", .SubRegion = "Western Asia", .Status = 1}},
			{"IE", New CCountry() With {.Name = "Ireland", .Code = "372", .Code2 = "IE", .Code3 = "IRL", .Region = "Europe", .SubRegion = "Northern Europe", .Status = 1}},
			{"IM", New CCountry() With {.Name = "Isle of Man", .Code = "833", .Code2 = "IM", .Code3 = "IMN", .Region = "Europe", .SubRegion = "Northern Europe", .Status = 1}},
			{"IL", New CCountry() With {.Name = "Israel", .Code = "376", .Code2 = "IL", .Code3 = "ISR", .Region = "Asia", .SubRegion = "Western Asia", .Status = 1}},
			{"IT", New CCountry() With {.Name = "Italy", .Code = "380", .Code2 = "IT", .Code3 = "ITA", .Region = "Europe", .SubRegion = "Southern Europe", .Status = 1}},
			{"JM", New CCountry() With {.Name = "Jamaica", .Code = "388", .Code2 = "JM", .Code3 = "JAM", .Region = "Americas", .SubRegion = "Latin America and the Caribbean", .Status = 1}},
			{"JP", New CCountry() With {.Name = "Japan", .Code = "392", .Code2 = "JP", .Code3 = "JPN", .Region = "Asia", .SubRegion = "Eastern Asia", .Status = 1}},
			{"JE", New CCountry() With {.Name = "Jersey", .Code = "832", .Code2 = "JE", .Code3 = "JEY", .Region = "Europe", .SubRegion = "Northern Europe", .Status = 1}},
			{"JO", New CCountry() With {.Name = "Jordan", .Code = "400", .Code2 = "JO", .Code3 = "JOR", .Region = "Asia", .SubRegion = "Western Asia", .Status = 1}},
			{"KZ", New CCountry() With {.Name = "Kazakhstan", .Code = "398", .Code2 = "KZ", .Code3 = "KAZ", .Region = "Asia", .SubRegion = "Central Asia", .Status = 1}},
			{"KE", New CCountry() With {.Name = "Kenya", .Code = "404", .Code2 = "KE", .Code3 = "KEN", .Region = "Africa", .SubRegion = "Sub-Saharan Africa", .Status = 1}},
			{"KI", New CCountry() With {.Name = "Kiribati", .Code = "296", .Code2 = "KI", .Code3 = "KIR", .Region = "Oceania", .SubRegion = "Micronesia", .Status = 1}},
			{"KP", New CCountry() With {.Name = "Korea (Democratic People's Republic of)", .Code = "408", .Code2 = "KP", .Code3 = "PRK", .Region = "Asia", .SubRegion = "Eastern Asia", .Status = 1}},
			{"KR", New CCountry() With {.Name = "Korea, Republic of", .Code = "410", .Code2 = "KR", .Code3 = "KOR", .Region = "Asia", .SubRegion = "Eastern Asia", .Status = 1}},
			{"KW", New CCountry() With {.Name = "Kuwait", .Code = "414", .Code2 = "KW", .Code3 = "KWT", .Region = "Asia", .SubRegion = "Western Asia", .Status = 1}},
			{"KG", New CCountry() With {.Name = "Kyrgyzstan", .Code = "417", .Code2 = "KG", .Code3 = "KGZ", .Region = "Asia", .SubRegion = "Central Asia", .Status = 1}},
			{"LA", New CCountry() With {.Name = "Lao People's Democratic Republic", .Code = "418", .Code2 = "LA", .Code3 = "LAO", .Region = "Asia", .SubRegion = "South-eastern Asia", .Status = 1}},
			{"LV", New CCountry() With {.Name = "Latvia", .Code = "428", .Code2 = "LV", .Code3 = "LVA", .Region = "Europe", .SubRegion = "Northern Europe", .Status = 1}},
			{"LB", New CCountry() With {.Name = "Lebanon", .Code = "422", .Code2 = "LB", .Code3 = "LBN", .Region = "Asia", .SubRegion = "Western Asia", .Status = 1}},
			{"LS", New CCountry() With {.Name = "Lesotho", .Code = "426", .Code2 = "LS", .Code3 = "LSO", .Region = "Africa", .SubRegion = "Sub-Saharan Africa", .Status = 1}},
			{"LR", New CCountry() With {.Name = "Liberia", .Code = "430", .Code2 = "LR", .Code3 = "LBR", .Region = "Africa", .SubRegion = "Sub-Saharan Africa", .Status = 1}},
			{"LY", New CCountry() With {.Name = "Libya", .Code = "434", .Code2 = "LY", .Code3 = "LBY", .Region = "Africa", .SubRegion = "Northern Africa", .Status = 1}},
			{"LI", New CCountry() With {.Name = "Liechtenstein", .Code = "438", .Code2 = "LI", .Code3 = "LIE", .Region = "Europe", .SubRegion = "Western Europe", .Status = 1}},
			{"LT", New CCountry() With {.Name = "Lithuania", .Code = "440", .Code2 = "LT", .Code3 = "LTU", .Region = "Europe", .SubRegion = "Northern Europe", .Status = 1}},
			{"LU", New CCountry() With {.Name = "Luxembourg", .Code = "442", .Code2 = "LU", .Code3 = "LUX", .Region = "Europe", .SubRegion = "Western Europe", .Status = 1}},
			{"MO", New CCountry() With {.Name = "Macao", .Code = "446", .Code2 = "MO", .Code3 = "MAC", .Region = "Asia", .SubRegion = "Eastern Asia", .Status = 1}},
			{"MG", New CCountry() With {.Name = "Madagascar", .Code = "450", .Code2 = "MG", .Code3 = "MDG", .Region = "Africa", .SubRegion = "Sub-Saharan Africa", .Status = 1}},
			{"MW", New CCountry() With {.Name = "Malawi", .Code = "454", .Code2 = "MW", .Code3 = "MWI", .Region = "Africa", .SubRegion = "Sub-Saharan Africa", .Status = 1}},
			{"MY", New CCountry() With {.Name = "Malaysia", .Code = "458", .Code2 = "MY", .Code3 = "MYS", .Region = "Asia", .SubRegion = "South-eastern Asia", .Status = 1}},
			{"MV", New CCountry() With {.Name = "Maldives", .Code = "462", .Code2 = "MV", .Code3 = "MDV", .Region = "Asia", .SubRegion = "Southern Asia", .Status = 1}},
			{"ML", New CCountry() With {.Name = "Mali", .Code = "466", .Code2 = "ML", .Code3 = "MLI", .Region = "Africa", .SubRegion = "Sub-Saharan Africa", .Status = 1}},
			{"MT", New CCountry() With {.Name = "Malta", .Code = "470", .Code2 = "MT", .Code3 = "MLT", .Region = "Europe", .SubRegion = "Southern Europe", .Status = 1}},
			{"MH", New CCountry() With {.Name = "Marshall Islands", .Code = "584", .Code2 = "MH", .Code3 = "MHL", .Region = "Oceania", .SubRegion = "Micronesia", .Status = 1}},
			{"MQ", New CCountry() With {.Name = "Martinique", .Code = "474", .Code2 = "MQ", .Code3 = "MTQ", .Region = "Americas", .SubRegion = "Latin America and the Caribbean", .Status = 1}},
			{"MR", New CCountry() With {.Name = "Mauritania", .Code = "478", .Code2 = "MR", .Code3 = "MRT", .Region = "Africa", .SubRegion = "Sub-Saharan Africa", .Status = 1}},
			{"MU", New CCountry() With {.Name = "Mauritius", .Code = "480", .Code2 = "MU", .Code3 = "MUS", .Region = "Africa", .SubRegion = "Sub-Saharan Africa", .Status = 1}},
			{"YT", New CCountry() With {.Name = "Mayotte", .Code = "175", .Code2 = "YT", .Code3 = "MYT", .Region = "Africa", .SubRegion = "Sub-Saharan Africa", .Status = 1}},
			{"MX", New CCountry() With {.Name = "Mexico", .Code = "484", .Code2 = "MX", .Code3 = "MEX", .Region = "Americas", .SubRegion = "Latin America and the Caribbean", .Status = 1}},
			{"FM", New CCountry() With {.Name = "Micronesia (Federated States of)", .Code = "583", .Code2 = "FM", .Code3 = "FSM", .Region = "Oceania", .SubRegion = "Micronesia", .Status = 1}},
			{"MD", New CCountry() With {.Name = "Moldova, Republic of", .Code = "498", .Code2 = "MD", .Code3 = "MDA", .Region = "Europe", .SubRegion = "Eastern Europe", .Status = 1}},
			{"MC", New CCountry() With {.Name = "Monaco", .Code = "492", .Code2 = "MC", .Code3 = "MCO", .Region = "Europe", .SubRegion = "Western Europe", .Status = 1}},
			{"MN", New CCountry() With {.Name = "Mongolia", .Code = "496", .Code2 = "MN", .Code3 = "MNG", .Region = "Asia", .SubRegion = "Eastern Asia", .Status = 1}},
			{"ME", New CCountry() With {.Name = "Montenegro", .Code = "499", .Code2 = "ME", .Code3 = "MNE", .Region = "Europe", .SubRegion = "Southern Europe", .Status = 1}},
			{"MS", New CCountry() With {.Name = "Montserrat", .Code = "500", .Code2 = "MS", .Code3 = "MSR", .Region = "Americas", .SubRegion = "Latin America and the Caribbean", .Status = 1}},
			{"MA", New CCountry() With {.Name = "Morocco", .Code = "504", .Code2 = "MA", .Code3 = "MAR", .Region = "Africa", .SubRegion = "Northern Africa", .Status = 1}},
			{"MZ", New CCountry() With {.Name = "Mozambique", .Code = "508", .Code2 = "MZ", .Code3 = "MOZ", .Region = "Africa", .SubRegion = "Sub-Saharan Africa", .Status = 1}},
			{"MM", New CCountry() With {.Name = "Myanmar", .Code = "104", .Code2 = "MM", .Code3 = "MMR", .Region = "Asia", .SubRegion = "South-eastern Asia", .Status = 1}},
			{"NA", New CCountry() With {.Name = "Namibia", .Code = "516", .Code2 = "NA", .Code3 = "NAM", .Region = "Africa", .SubRegion = "Sub-Saharan Africa", .Status = 1}},
			{"NR", New CCountry() With {.Name = "Nauru", .Code = "520", .Code2 = "NR", .Code3 = "NRU", .Region = "Oceania", .SubRegion = "Micronesia", .Status = 1}},
			{"NP", New CCountry() With {.Name = "Nepal", .Code = "524", .Code2 = "NP", .Code3 = "NPL", .Region = "Asia", .SubRegion = "Southern Asia", .Status = 1}},
			{"NL", New CCountry() With {.Name = "Netherlands", .Code = "528", .Code2 = "NL", .Code3 = "NLD", .Region = "Europe", .SubRegion = "Western Europe", .Status = 1}},
			{"NC", New CCountry() With {.Name = "New Caledonia", .Code = "540", .Code2 = "NC", .Code3 = "NCL", .Region = "Oceania", .SubRegion = "Melanesia", .Status = 1}},
			{"NZ", New CCountry() With {.Name = "New Zealand", .Code = "554", .Code2 = "NZ", .Code3 = "NZL", .Region = "Oceania", .SubRegion = "Australia and New Zealand", .Status = 1}},
			{"NI", New CCountry() With {.Name = "Nicaragua", .Code = "558", .Code2 = "NI", .Code3 = "NIC", .Region = "Americas", .SubRegion = "Latin America and the Caribbean", .Status = 1}},
			{"NE", New CCountry() With {.Name = "Niger", .Code = "562", .Code2 = "NE", .Code3 = "NER", .Region = "Africa", .SubRegion = "Sub-Saharan Africa", .Status = 1}},
			{"NG", New CCountry() With {.Name = "Nigeria", .Code = "566", .Code2 = "NG", .Code3 = "NGA", .Region = "Africa", .SubRegion = "Sub-Saharan Africa", .Status = 1}},
			{"NU", New CCountry() With {.Name = "Niue", .Code = "570", .Code2 = "NU", .Code3 = "NIU", .Region = "Oceania", .SubRegion = "Polynesia", .Status = 1}},
			{"NF", New CCountry() With {.Name = "Norfolk Island", .Code = "574", .Code2 = "NF", .Code3 = "NFK", .Region = "Oceania", .SubRegion = "Australia and New Zealand", .Status = 1}},
			{"MK", New CCountry() With {.Name = "North Macedonia", .Code = "807", .Code2 = "MK", .Code3 = "MKD", .Region = "Europe", .SubRegion = "Southern Europe", .Status = 1}},
			{"MP", New CCountry() With {.Name = "Northern Mariana Islands", .Code = "580", .Code2 = "MP", .Code3 = "MNP", .Region = "Oceania", .SubRegion = "Micronesia", .Status = 1}},
			{"NO", New CCountry() With {.Name = "Norway", .Code = "578", .Code2 = "NO", .Code3 = "NOR", .Region = "Europe", .SubRegion = "Northern Europe", .Status = 1}},
			{"OM", New CCountry() With {.Name = "Oman", .Code = "512", .Code2 = "OM", .Code3 = "OMN", .Region = "Asia", .SubRegion = "Western Asia", .Status = 1}},
			{"PK", New CCountry() With {.Name = "Pakistan", .Code = "586", .Code2 = "PK", .Code3 = "PAK", .Region = "Asia", .SubRegion = "Southern Asia", .Status = 1}},
			{"PW", New CCountry() With {.Name = "Palau", .Code = "585", .Code2 = "PW", .Code3 = "PLW", .Region = "Oceania", .SubRegion = "Micronesia", .Status = 1}},
			{"PS", New CCountry() With {.Name = "Palestine, State of", .Code = "275", .Code2 = "PS", .Code3 = "PSE", .Region = "Asia", .SubRegion = "Western Asia", .Status = 1}},
			{"PA", New CCountry() With {.Name = "Panama", .Code = "591", .Code2 = "PA", .Code3 = "PAN", .Region = "Americas", .SubRegion = "Latin America and the Caribbean", .Status = 1}},
			{"PG", New CCountry() With {.Name = "Papua New Guinea", .Code = "598", .Code2 = "PG", .Code3 = "PNG", .Region = "Oceania", .SubRegion = "Melanesia", .Status = 1}},
			{"PY", New CCountry() With {.Name = "Paraguay", .Code = "600", .Code2 = "PY", .Code3 = "PRY", .Region = "Americas", .SubRegion = "Latin America and the Caribbean", .Status = 1}},
			{"PE", New CCountry() With {.Name = "Peru", .Code = "604", .Code2 = "PE", .Code3 = "PER", .Region = "Americas", .SubRegion = "Latin America and the Caribbean", .Status = 1}},
			{"PH", New CCountry() With {.Name = "Philippines", .Code = "608", .Code2 = "PH", .Code3 = "PHL", .Region = "Asia", .SubRegion = "South-eastern Asia", .Status = 1}},
			{"PN", New CCountry() With {.Name = "Pitcairn", .Code = "612", .Code2 = "PN", .Code3 = "PCN", .Region = "Oceania", .SubRegion = "Polynesia", .Status = 1}},
			{"PL", New CCountry() With {.Name = "Poland", .Code = "616", .Code2 = "PL", .Code3 = "POL", .Region = "Europe", .SubRegion = "Eastern Europe", .Status = 1}},
			{"PT", New CCountry() With {.Name = "Portugal", .Code = "620", .Code2 = "PT", .Code3 = "PRT", .Region = "Europe", .SubRegion = "Southern Europe", .Status = 1}},
			{"PR", New CCountry() With {.Name = "Puerto Rico", .Code = "630", .Code2 = "PR", .Code3 = "PRI", .Region = "Americas", .SubRegion = "Latin America and the Caribbean", .Status = 1}},
			{"QA", New CCountry() With {.Name = "Qatar", .Code = "634", .Code2 = "QA", .Code3 = "QAT", .Region = "Asia", .SubRegion = "Western Asia", .Status = 1}},
			{"RE", New CCountry() With {.Name = "Réunion", .Code = "638", .Code2 = "RE", .Code3 = "REU", .Region = "Africa", .SubRegion = "Sub-Saharan Africa", .Status = 1}},
			{"RO", New CCountry() With {.Name = "Romania", .Code = "642", .Code2 = "RO", .Code3 = "ROU", .Region = "Europe", .SubRegion = "Eastern Europe", .Status = 1}},
			{"RU", New CCountry() With {.Name = "Russian Federation", .Code = "643", .Code2 = "RU", .Code3 = "RUS", .Region = "Europe", .SubRegion = "Eastern Europe", .Status = 1}},
			{"RW", New CCountry() With {.Name = "Rwanda", .Code = "646", .Code2 = "RW", .Code3 = "RWA", .Region = "Africa", .SubRegion = "Sub-Saharan Africa", .Status = 1}},
			{"BL", New CCountry() With {.Name = "Saint Barthélemy", .Code = "652", .Code2 = "BL", .Code3 = "BLM", .Region = "Americas", .SubRegion = "Latin America and the Caribbean", .Status = 1}},
			{"SH", New CCountry() With {.Name = "Saint Helena, Ascension and Tristan da Cunha", .Code = "654", .Code2 = "SH", .Code3 = "SHN", .Region = "Africa", .SubRegion = "Sub-Saharan Africa", .Status = 1}},
			{"KN", New CCountry() With {.Name = "Saint Kitts and Nevis", .Code = "659", .Code2 = "KN", .Code3 = "KNA", .Region = "Americas", .SubRegion = "Latin America and the Caribbean", .Status = 1}},
			{"LC", New CCountry() With {.Name = "Saint Lucia", .Code = "662", .Code2 = "LC", .Code3 = "LCA", .Region = "Americas", .SubRegion = "Latin America and the Caribbean", .Status = 1}},
			{"MF", New CCountry() With {.Name = "Saint Martin (French part)", .Code = "663", .Code2 = "MF", .Code3 = "MAF", .Region = "Americas", .SubRegion = "Latin America and the Caribbean", .Status = 1}},
			{"PM", New CCountry() With {.Name = "Saint Pierre and Miquelon", .Code = "666", .Code2 = "PM", .Code3 = "SPM", .Region = "Americas", .SubRegion = "Northern America", .Status = 1}},
			{"VC", New CCountry() With {.Name = "Saint Vincent and the Grenadines", .Code = "670", .Code2 = "VC", .Code3 = "VCT", .Region = "Americas", .SubRegion = "Latin America and the Caribbean", .Status = 1}},
			{"WS", New CCountry() With {.Name = "Samoa", .Code = "882", .Code2 = "WS", .Code3 = "WSM", .Region = "Oceania", .SubRegion = "Polynesia", .Status = 1}},
			{"SM", New CCountry() With {.Name = "San Marino", .Code = "674", .Code2 = "SM", .Code3 = "SMR", .Region = "Europe", .SubRegion = "Southern Europe", .Status = 1}},
			{"ST", New CCountry() With {.Name = "Sao Tome and Principe", .Code = "678", .Code2 = "ST", .Code3 = "STP", .Region = "Africa", .SubRegion = "Sub-Saharan Africa", .Status = 1}},
			{"SA", New CCountry() With {.Name = "Saudi Arabia", .Code = "682", .Code2 = "SA", .Code3 = "SAU", .Region = "Asia", .SubRegion = "Western Asia", .Status = 1}},
			{"SN", New CCountry() With {.Name = "Senegal", .Code = "686", .Code2 = "SN", .Code3 = "SEN", .Region = "Africa", .SubRegion = "Sub-Saharan Africa", .Status = 1}},
			{"RS", New CCountry() With {.Name = "Serbia", .Code = "688", .Code2 = "RS", .Code3 = "SRB", .Region = "Europe", .SubRegion = "Southern Europe", .Status = 1}},
			{"SC", New CCountry() With {.Name = "Seychelles", .Code = "690", .Code2 = "SC", .Code3 = "SYC", .Region = "Africa", .SubRegion = "Sub-Saharan Africa", .Status = 1}},
			{"SL", New CCountry() With {.Name = "Sierra Leone", .Code = "694", .Code2 = "SL", .Code3 = "SLE", .Region = "Africa", .SubRegion = "Sub-Saharan Africa", .Status = 1}},
			{"SG", New CCountry() With {.Name = "Singapore", .Code = "702", .Code2 = "SG", .Code3 = "SGP", .Region = "Asia", .SubRegion = "South-eastern Asia", .Status = 1}},
			{"SX", New CCountry() With {.Name = "Sint Maarten (Dutch part)", .Code = "534", .Code2 = "SX", .Code3 = "SXM", .Region = "Americas", .SubRegion = "Latin America and the Caribbean", .Status = 1}},
			{"SK", New CCountry() With {.Name = "Slovakia", .Code = "703", .Code2 = "SK", .Code3 = "SVK", .Region = "Europe", .SubRegion = "Eastern Europe", .Status = 1}},
			{"SI", New CCountry() With {.Name = "Slovenia", .Code = "705", .Code2 = "SI", .Code3 = "SVN", .Region = "Europe", .SubRegion = "Southern Europe", .Status = 1}},
			{"SB", New CCountry() With {.Name = "Solomon Islands", .Code = "90", .Code2 = "SB", .Code3 = "SLB", .Region = "Oceania", .SubRegion = "Melanesia", .Status = 1}},
			{"SO", New CCountry() With {.Name = "Somalia", .Code = "706", .Code2 = "SO", .Code3 = "SOM", .Region = "Africa", .SubRegion = "Sub-Saharan Africa", .Status = 1}},
			{"ZA", New CCountry() With {.Name = "South Africa", .Code = "710", .Code2 = "ZA", .Code3 = "ZAF", .Region = "Africa", .SubRegion = "Sub-Saharan Africa", .Status = 1}},
			{"GS", New CCountry() With {.Name = "South Georgia and the South Sandwich Islands", .Code = "239", .Code2 = "GS", .Code3 = "SGS", .Region = "Americas", .SubRegion = "Latin America and the Caribbean", .Status = 1}},
			{"SS", New CCountry() With {.Name = "South Sudan", .Code = "728", .Code2 = "SS", .Code3 = "SSD", .Region = "Africa", .SubRegion = "Sub-Saharan Africa", .Status = 1}},
			{"ES", New CCountry() With {.Name = "Spain", .Code = "724", .Code2 = "ES", .Code3 = "ESP", .Region = "Europe", .SubRegion = "Southern Europe", .Status = 1}},
			{"LK", New CCountry() With {.Name = "Sri Lanka", .Code = "144", .Code2 = "LK", .Code3 = "LKA", .Region = "Asia", .SubRegion = "Southern Asia", .Status = 1}},
			{"SD", New CCountry() With {.Name = "Sudan", .Code = "729", .Code2 = "SD", .Code3 = "SDN", .Region = "Africa", .SubRegion = "Northern Africa", .Status = 1}},
			{"SR", New CCountry() With {.Name = "Suriname", .Code = "740", .Code2 = "SR", .Code3 = "SUR", .Region = "Americas", .SubRegion = "Latin America and the Caribbean", .Status = 1}},
			{"SJ", New CCountry() With {.Name = "Svalbard and Jan Mayen", .Code = "744", .Code2 = "SJ", .Code3 = "SJM", .Region = "Europe", .SubRegion = "Northern Europe", .Status = 1}},
			{"SE", New CCountry() With {.Name = "Sweden", .Code = "752", .Code2 = "SE", .Code3 = "SWE", .Region = "Europe", .SubRegion = "Northern Europe", .Status = 1}},
			{"CH", New CCountry() With {.Name = "Switzerland", .Code = "756", .Code2 = "CH", .Code3 = "CHE", .Region = "Europe", .SubRegion = "Western Europe", .Status = 1}},
			{"SY", New CCountry() With {.Name = "Syrian Arab Republic", .Code = "760", .Code2 = "SY", .Code3 = "SYR", .Region = "Asia", .SubRegion = "Western Asia", .Status = 1}},
			{"TW", New CCountry() With {.Name = "Taiwan, Province of China", .Code = "158", .Code2 = "TW", .Code3 = "TWN", .Region = "Asia", .SubRegion = "Eastern Asia", .Status = 1}},
			{"TJ", New CCountry() With {.Name = "Tajikistan", .Code = "762", .Code2 = "TJ", .Code3 = "TJK", .Region = "Asia", .SubRegion = "Central Asia", .Status = 1}},
			{"TZ", New CCountry() With {.Name = "Tanzania, United Republic of", .Code = "834", .Code2 = "TZ", .Code3 = "TZA", .Region = "Africa", .SubRegion = "Sub-Saharan Africa", .Status = 1}},
			{"TH", New CCountry() With {.Name = "Thailand", .Code = "764", .Code2 = "TH", .Code3 = "THA", .Region = "Asia", .SubRegion = "South-eastern Asia", .Status = 1}},
			{"TL", New CCountry() With {.Name = "Timor-Leste", .Code = "626", .Code2 = "TL", .Code3 = "TLS", .Region = "Asia", .SubRegion = "South-eastern Asia", .Status = 1}},
			{"TG", New CCountry() With {.Name = "Togo", .Code = "768", .Code2 = "TG", .Code3 = "TGO", .Region = "Africa", .SubRegion = "Sub-Saharan Africa", .Status = 1}},
			{"TK", New CCountry() With {.Name = "Tokelau", .Code = "772", .Code2 = "TK", .Code3 = "TKL", .Region = "Oceania", .SubRegion = "Polynesia", .Status = 1}},
			{"TO", New CCountry() With {.Name = "Tonga", .Code = "776", .Code2 = "TO", .Code3 = "TON", .Region = "Oceania", .SubRegion = "Polynesia", .Status = 1}},
			{"TT", New CCountry() With {.Name = "Trinidad and Tobago", .Code = "780", .Code2 = "TT", .Code3 = "TTO", .Region = "Americas", .SubRegion = "Latin America and the Caribbean", .Status = 1}},
			{"TN", New CCountry() With {.Name = "Tunisia", .Code = "788", .Code2 = "TN", .Code3 = "TUN", .Region = "Africa", .SubRegion = "Northern Africa", .Status = 1}},
			{"TR", New CCountry() With {.Name = "Turkey", .Code = "792", .Code2 = "TR", .Code3 = "TUR", .Region = "Asia", .SubRegion = "Western Asia", .Status = 1}},
			{"TM", New CCountry() With {.Name = "Turkmenistan", .Code = "795", .Code2 = "TM", .Code3 = "TKM", .Region = "Asia", .SubRegion = "Central Asia", .Status = 1}},
			{"TC", New CCountry() With {.Name = "Turks and Caicos Islands", .Code = "796", .Code2 = "TC", .Code3 = "TCA", .Region = "Americas", .SubRegion = "Latin America and the Caribbean", .Status = 1}},
			{"TV", New CCountry() With {.Name = "Tuvalu", .Code = "798", .Code2 = "TV", .Code3 = "TUV", .Region = "Oceania", .SubRegion = "Polynesia", .Status = 1}},
			{"UG", New CCountry() With {.Name = "Uganda", .Code = "800", .Code2 = "UG", .Code3 = "UGA", .Region = "Africa", .SubRegion = "Sub-Saharan Africa", .Status = 1}},
			{"UA", New CCountry() With {.Name = "Ukraine", .Code = "804", .Code2 = "UA", .Code3 = "UKR", .Region = "Europe", .SubRegion = "Eastern Europe", .Status = 1}},
			{"AE", New CCountry() With {.Name = "United Arab Emirates", .Code = "784", .Code2 = "AE", .Code3 = "ARE", .Region = "Asia", .SubRegion = "Western Asia", .Status = 1}},
			{"GB", New CCountry() With {.Name = "United Kingdom of Great Britain and Northern Ireland", .Code = "826", .Code2 = "GB", .Code3 = "GBR", .Region = "Europe", .SubRegion = "Northern Europe", .Status = 1}},
			{"UM", New CCountry() With {.Name = "United States Minor Outlying Islands", .Code = "581", .Code2 = "UM", .Code3 = "UMI", .Region = "Oceania", .SubRegion = "Micronesia", .Status = 1}},
			{"UY", New CCountry() With {.Name = "Uruguay", .Code = "858", .Code2 = "UY", .Code3 = "URY", .Region = "Americas", .SubRegion = "Latin America and the Caribbean", .Status = 1}},
			{"UZ", New CCountry() With {.Name = "Uzbekistan", .Code = "860", .Code2 = "UZ", .Code3 = "UZB", .Region = "Asia", .SubRegion = "Central Asia", .Status = 1}},
			{"VU", New CCountry() With {.Name = "Vanuatu", .Code = "548", .Code2 = "VU", .Code3 = "VUT", .Region = "Oceania", .SubRegion = "Melanesia", .Status = 1}},
			{"VE", New CCountry() With {.Name = "Venezuela (Bolivarian Republic of)", .Code = "862", .Code2 = "VE", .Code3 = "VEN", .Region = "Americas", .SubRegion = "Latin America and the Caribbean", .Status = 1}},
			{"VN", New CCountry() With {.Name = "Viet Nam", .Code = "704", .Code2 = "VN", .Code3 = "VNM", .Region = "Asia", .SubRegion = "South-eastern Asia", .Status = 1}},
			{"VG", New CCountry() With {.Name = "Virgin Islands (British)", .Code = "92", .Code2 = "VG", .Code3 = "VGB", .Region = "Americas", .SubRegion = "Latin America and the Caribbean", .Status = 1}},
			{"VI", New CCountry() With {.Name = "Virgin Islands (U.S.)", .Code = "850", .Code2 = "VI", .Code3 = "VIR", .Region = "Americas", .SubRegion = "Latin America and the Caribbean", .Status = 1}},
			{"WF", New CCountry() With {.Name = "Wallis and Futuna", .Code = "876", .Code2 = "WF", .Code3 = "WLF", .Region = "Oceania", .SubRegion = "Polynesia", .Status = 1}},
			{"EH", New CCountry() With {.Name = "Western Sahara", .Code = "732", .Code2 = "EH", .Code3 = "ESH", .Region = "Africa", .SubRegion = "Northern Africa", .Status = 1}},
			{"YE", New CCountry() With {.Name = "Yemen", .Code = "887", .Code2 = "YE", .Code3 = "YEM", .Region = "Asia", .SubRegion = "Western Asia", .Status = 1}},
			{"ZM", New CCountry() With {.Name = "Zambia", .Code = "894", .Code2 = "ZM", .Code3 = "ZMB", .Region = "Africa", .SubRegion = "Sub-Saharan Africa", .Status = 1}},
			{"ZW", New CCountry() With {.Name = "Zimbabwe", .Code = "716", .Code2 = "ZW", .Code3 = "ZWE", .Region = "Africa", .SubRegion = "Sub-Saharan Africa", .Status = 1}}
		}
		Public Shared Function ValidateApplicantsAndBeneficiariesXABooking(ByVal oXAAppInfo As XaSubmission) As String
			''Disable auto booking if the application has more than 4 beneficiaries or the application has more than 3 applicants
			''Customlist booking validation does not support this feature. Currently, this feature is only for ARMYAVFCU(Army Aviation Center  FCU)          
			Dim bookingBeneficiaryThreshold = 4
			Dim bookingApplicantThreshold = 3
			Dim sMessage = ""
			If oXAAppInfo.BAApplicants IsNot Nothing AndAlso oXAAppInfo.BAApplicants.Any() Then	''Business XA            
				If bookingApplicantThreshold < oXAAppInfo.BAApplicants.Count Then
					sMessage = "No booking because the application has more than " & bookingApplicantThreshold.ToString & " applicants. "
				End If
			ElseIf oXAAppInfo.SAApplicants IsNot Nothing AndAlso oXAAppInfo.SAApplicants.Any Then '' Special Account              
				Dim beneficiariesList = oXAAppInfo.SAApplicants.First().Beneficiaries
				If bookingApplicantThreshold < oXAAppInfo.SAApplicants.Count Then
					sMessage = "No booking because the application has more than " & bookingApplicantThreshold.ToString & " applicants. "
				ElseIf beneficiariesList IsNot Nothing AndAlso beneficiariesList.Any() Then
					If bookingBeneficiaryThreshold < beneficiariesList.Count Then
						sMessage = "No booking because the application has more than " & bookingBeneficiaryThreshold.ToString & " beneficiaries. "
					End If
				End If
			Else ''Standard XA
				If oXAAppInfo.BeneficiaryList IsNot Nothing AndAlso oXAAppInfo.BeneficiaryList.Any() Then
					If bookingBeneficiaryThreshold < oXAAppInfo.BeneficiaryList.Count Then
						sMessage = "No booking because the application has more than " & bookingBeneficiaryThreshold.ToString & " beneficiaries. "
					End If
				End If
			End If
			Return sMessage
		End Function

		Public Shared Function MapProductType2String(ByVal psProductType As String) As String

			Select Case psProductType.ToUpper
				Case "CHECKING"
					Return "Checking"
				Case "SAVINGS"
					Return "Savings"
				Case "SAVINGS_BONDS"
					Return "Savings Bonds"
				Case "CD"
					Return "Certificate Account"
				Case "IRACD"
					Return "IRA Certificate Account"
				Case "IRA"
					Return "IRA"
				Case "LOANS"
					Return "Loans"
				Case "HSA"
					Return "HSA"
				Case "MONEY_MARKET"
					Return "Money Market"
				Case "OTHER"
					Return "Other"
				Case "RECOMMENDED"
					Return "Recommended"
				Case "REQUIRED"
					Return "Required"
				Case "PRESELECTED"
					Return "PreSelected"
				Case Else
					If String.IsNullOrWhiteSpace(psProductType) Then Return "N/A"
					Return psProductType
			End Select

		End Function

#Region "URL Parameter custom questions"

        Public Shared Function parseUrlParaCustomQuestionNamesAndAnswers(ByVal psQueryStr As String) As List(Of CXAQuestionAnswer)
            '' using "cq_" prefix in the url parameter to get the custom question name and custom question answer
            '' then store question name/answers to the dictionary.
            Dim oCQAnswers As New List(Of CXAQuestionAnswer)
            Dim oNVCQueryStr = HttpUtility.ParseQueryString(psQueryStr)
            If oNVCQueryStr IsNot Nothing Then
                Dim cqName = "", cqAnswer = ""
                For Each key In oNVCQueryStr
                    If key.StartsWith("cq_") Then
                        'remove cq_ prefix in cqName
                        cqName = Common.SafeString(key).Replace("cq_", "")
                        Dim values = oNVCQueryStr.GetValues(key)
                        For Each value In values
                            cqAnswer = Common.SafeString(value)
                            oCQAnswers.Add(New CXAQuestionAnswer() With {.q = cqName, .a = cqAnswer})
                        Next
                    End If
                Next
            End If
            Return oCQAnswers
        End Function
		Public Shared Function getUrlParaXACustomQuestionsAndAnswers(ByVal poConfig As CWebsiteConfig, ByVal poCustomQuestions As List(Of CCustomQuestionXA), ByVal poUrlParaCustomQuestionsAndAnswers As List(Of CXAQuestionAnswer)) As List(Of CApplicantQA)
			''- check all custom question names in the poUrlParaCustomQuestionsAndAnswers 
			''- add the custom question object to the list of oParseCustomquestion if the custom question name is existed in the downloaded Custom questions from LPQ
			''- display the warning message to the logviewer if the question name is not existed in the downloaded custom questions
			Dim oAvailableUrlParaCustomQuestions As New List(Of CApplicantQA)
			Dim bIsUrlParaCustomQuestion = poUrlParaCustomQuestionsAndAnswers IsNot Nothing
			If Not bIsUrlParaCustomQuestion Then Return oAvailableUrlParaCustomQuestions '' there is no Url Parameter custom question in the url
			'Dim currentDownLoadedXAQuestions = CCustomQuestionNewAPI.getDownloadedXACustomQuestions(poConfig, pbIsApplicant, psLoanType, psAvailability, True)
			For Each oItem In poUrlParaCustomQuestionsAndAnswers
				Dim oCQ = poCustomQuestions.FirstOrDefault(Function(cq) cq.Name = oItem.q)
				If oCQ IsNot Nothing Then
					oAvailableUrlParaCustomQuestions.Add(New CApplicantQA With {
														 .CQName = oCQ.Name,
														 .CQAnswer = oItem.a,
														 .CQApplicantPrefix = Nothing,
														 .CQLocation = CustomQuestionLocation.ReviewPage,
														 .CQRole = oCQ.Identifer.Role
														 })
				Else
					_log.Warn("Invalid url parameter custom question name: " & oItem.q)
				End If
			Next
			Return oAvailableUrlParaCustomQuestions
		End Function
		Public Shared Function getUrlParaCustomQuestionsAndAnswers(ByVal poConfig As CWebsiteConfig, ByVal psLoanType As String, ByVal pbIsApplicant As Boolean, ByVal poCustomQuestions As List(Of CCustomQuestion), ByVal poUrlParaCustomQuestionsAndAnswers As List(Of CXAQuestionAnswer)) As List(Of CURLParaCustomQuestionAndAnswer)
            ''- check all custom question names in the poUrlParaCustomQuestionsAndAnswers 
            ''- add the custom question object to the list of oParseCustomquestion if the custom question name is existed in the downloaded Custom questions from LPQ
            ''- display the warning message to the logviewer if the question name is not existed in the downloaded custom questions
            Dim oAvailableUrlParaCustomQuestions As New List(Of CXAQuestionAnswer)
            Dim oUrlParaCustomQuestions As New List(Of CURLParaCustomQuestionAndAnswer)
            Dim bIsUrlParaCustomQuestion = poUrlParaCustomQuestionsAndAnswers IsNot Nothing
            If Not bIsUrlParaCustomQuestion Then Return oUrlParaCustomQuestions  '' there is no Url Parameter custom question in the url
            Dim currentDownLoadedQuestions = CCustomQuestionNewAPI.getDownloadedCustomQuestions(poConfig, pbIsApplicant, psLoanType, True)
            For Each oItem In poUrlParaCustomQuestionsAndAnswers
                Dim oCQ = currentDownLoadedQuestions.FirstOrDefault(Function(cq) cq.Name = oItem.q)
                If oCQ IsNot Nothing Then
                    oAvailableUrlParaCustomQuestions.Add(oItem)
                Else
                    _log.Warn("Invalid url parameter custom question name: " & oItem.q)
                End If
            Next
            If oAvailableUrlParaCustomQuestions IsNot Nothing AndAlso oAvailableUrlParaCustomQuestions.Any() Then
                For Each oCQ In oAvailableUrlParaCustomQuestions
                    Dim oDownloadedQuestions = currentDownLoadedQuestions.FirstOrDefault(Function(cq) cq.Name = oCQ.q)
                    If oDownloadedQuestions IsNot Nothing Then
                        Dim sAnswerType = ParsedUIType(oDownloadedQuestions.UIType)
                        If oDownloadedQuestions.UIType = QuestionUIType.CHECK Then ''get the text of the selected answer
                            Dim sChkAnswerText = ""
                            For Each opt In oDownloadedQuestions.CustomQuestionOptions
                                If oCQ.a.ToUpper = opt.Value.ToUpper Then
                                    sChkAnswerText = opt.Text
                                    Exit For
                                End If
                            Next
                            '' add question name, text, and answer to the list
                            If sChkAnswerText <> "" Then
                                oUrlParaCustomQuestions.Add(New CURLParaCustomQuestionAndAnswer() With {.Name = oCQ.q, .Answer = oCQ.a, .AnswerType = sAnswerType, .AnswerText = sChkAnswerText})
                            End If
                        Else
                            oUrlParaCustomQuestions.Add(New CURLParaCustomQuestionAndAnswer() With {.Name = oCQ.q, .Answer = oCQ.a, .AnswerType = sAnswerType, .AnswerText = oDownloadedQuestions.Text})
                        End If
                    End If
                Next
            End If
            Return oUrlParaCustomQuestions
        End Function
        Public Shared Function ParsedUIType(ByVal psQuestionType As QuestionUIType) As String
            Select Case psQuestionType
                Case QuestionUIType.TEXT
                    Return "text"
                Case QuestionUIType.CHECK
                    Return "checkbox"
                Case QuestionUIType.DROPDOWN
                    Return "dropdown"
                Case QuestionUIType.RADIO
                    Return "radio" ''not use it for custom questions
                Case QuestionUIType.HIDDEN
                    Return "hidden"
                Case Else
                    Return ""
            End Select
        End Function
#End Region
	End Class
End Namespace