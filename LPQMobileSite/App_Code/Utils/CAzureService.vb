﻿Imports System.IO
Imports Microsoft.WindowsAzure.Storage.Blob
Imports Microsoft.WindowsAzure.Storage

Public Class CAzureStorageService
	Private _log As log4net.ILog = log4net.LogManager.GetLogger(Me.GetType)
	Private Property storageAccount As CloudStorageAccount
	Public Sub New(accountName As String, accountKey As String)
		Dim connectionString As String = String.Format("DefaultEndpointsProtocol=https;AccountName={0};AccountKey={1};EndpointSuffix=core.windows.net", accountName, accountKey)
		If Not CloudStorageAccount.TryParse(connectionString, storageAccount) Then
			Throw New ArgumentException("Cannot initial storage account object")
		End If
	End Sub
	Public Sub New(connectionString As String)
		If Not CloudStorageAccount.TryParse(connectionString, storageAccount) Then
			Throw New ArgumentException("Cannot initial storage account object")
		End If
	End Sub

	Public Function UploadBlob(container As String, name As String, content As Byte(), Optional contentType As String = "", Optional containerPublicAccessType As BlobContainerPublicAccessType = BlobContainerPublicAccessType.Blob) As String
		Dim cloudBlobClient As CloudBlobClient = storageAccount.CreateCloudBlobClient()
		Dim cloudBlobContainer = cloudBlobClient.GetContainerReference(container)
		cloudBlobContainer.CreateIfNotExists()

		_log.Debug(String.Format("Created container '{0}'", cloudBlobContainer.Name))
		Dim permissions As BlobContainerPermissions = New BlobContainerPermissions With {
			.PublicAccess = containerPublicAccessType
		}
		cloudBlobContainer.SetPermissions(permissions)
		Dim cloudBlockBlob As CloudBlockBlob = cloudBlobContainer.GetBlockBlobReference(name)
		cloudBlockBlob.UploadFromByteArray(content, 0, content.Length)
		If Not String.IsNullOrEmpty(contentType) Then
			cloudBlockBlob.Properties.ContentType = contentType
			cloudBlockBlob.SetProperties()
		End If

		Return cloudBlockBlob.StorageUri.GetUri(StorageLocation.Primary).AbsoluteUri
	End Function

	Public Function ReadBlobToStream(container As String, name As String) As Stream
		Dim cloudBlobClient As CloudBlobClient = storageAccount.CreateCloudBlobClient()
		Dim cloudBlobContainer = cloudBlobClient.GetContainerReference(container)
		If Not cloudBlobContainer.Exists() Then Return Nothing
		Dim cloudBlockBlob As CloudBlockBlob = cloudBlobContainer.GetBlockBlobReference(name)
		If Not cloudBlockBlob.Exists() Then Return Nothing
		Dim ms As New MemoryStream
		cloudBlockBlob.DownloadToStream(ms)
		Return ms
	End Function

	Public Sub DeleteBlob(container As String, name As String)
		Dim cloudBlobClient As CloudBlobClient = storageAccount.CreateCloudBlobClient()
		Dim cloudBlobContainer = cloudBlobClient.GetContainerReference(container)
		If cloudBlobContainer.Exists() Then
			Dim cloudBlockBlob As CloudBlockBlob = cloudBlobContainer.GetBlockBlobReference(name)
			If cloudBlockBlob.Exists() Then
				cloudBlockBlob.Delete()
			End If
		End If
	End Sub
End Class
