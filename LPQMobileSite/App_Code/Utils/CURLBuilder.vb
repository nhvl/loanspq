Imports System.Web
Imports System.Collections.Generic
Imports System.Collections
Imports System.Collections.Specialized

Namespace LPQMobile.Utils
	Public Class CURLBuilder
		''' <summary>
		''' Get query string given params.
		''' </summary>
		''' <returns>Query string without leading '?'</returns>
		Public Shared Function BuildQueryString(ByVal poVariables As Generic.IDictionary(Of String, String), Optional ByVal pbEncodeParams As Boolean = True) As String
			Dim sPairs As New Generic.List(Of String)
			For Each oPair As Generic.KeyValuePair(Of String, String) In poVariables
				If pbEncodeParams Then
					sPairs.Add(HttpUtility.UrlEncode(oPair.Key) & "=" & HttpUtility.UrlEncode(oPair.Value))
				Else
					sPairs.Add(oPair.Key & "=" & oPair.Value)
				End If
			Next

			Return String.Join("&", sPairs.ToArray)
		End Function

		''' <summary>
		''' Get query string given params.
		''' </summary>
		''' <returns>Query string without leading '?'</returns>
		Public Shared Function BuildQueryString(ByVal poVariables As Generic.IDictionary(Of String, IList(Of String)), Optional ByVal pbEncodeParams As Boolean = True) As String
			Dim sPairs As New Generic.List(Of String)
			For Each oPair As Generic.KeyValuePair(Of String, IList(Of String)) In poVariables
				For Each sValue As String In oPair.Value
					If pbEncodeParams Then
						sPairs.Add(HttpUtility.UrlEncode(oPair.Key) & "=" & HttpUtility.UrlEncode(sValue))
					Else
						sPairs.Add(oPair.Key & "=" & sValue)
					End If
				Next
			Next

			Return String.Join("&", sPairs.ToArray)
		End Function

		''' <summary>
		''' Convert a query string into a collection of values indexed by variable name.
		''' </summary>
		''' <remarks>Each entry in the returned dictionary is guaranteed to have at least one value.</remarks>
		'Public Shared Function GetQueryVars(ByVal psQueryString As String, Optional ByVal pbCaseSensitive As Boolean = False) As Dictionary(Of String, IList(Of String))
		'	Dim oComparer As StringComparer = IIfStrict(pbCaseSensitive, StringComparer.Ordinal, StringComparer.OrdinalIgnoreCase)
		'	Dim oQueryVars As New Generic.Dictionary(Of String, IList(Of String))(oComparer)

		'	If psQueryString.StartsWith("?") Then
		'		psQueryString = Right(psQueryString, psQueryString.Length - 1)
		'	End If

		'	Dim sQueryVars As String() = psQueryString.Split("&"c)
		'	For Each sQueryVar As String In sQueryVars
		'		Dim sQueryVarPair As String() = sQueryVar.Split("="c)
		'		Dim sVar As String = HttpUtility.UrlDecode(sQueryVarPair(0))
		'		Dim sValue As String = String.Empty
		'		If sQueryVarPair.Length > 1 Then 'Check in case "=" is missing
		'			sValue = HttpUtility.UrlDecode(sQueryVarPair(1))
		'		End If

		'		If Not oQueryVars.ContainsKey(sVar) Then
		'			oQueryVars.Add(sVar, New List(Of String))
		'		End If
		'		oQueryVars(sVar).Add(sValue)
		'	Next

		'	Return oQueryVars
		'End Function

		''' <summary>
		''' Convert a query string into a collection of values indexed by variable name.
		''' </summary>
		''' <remarks>Each entry in the returned dictionary is guaranteed to have at least one value.</remarks>
		Public Shared Function GetQueryVars(ByVal psQueryString As String, Optional ByVal pbCaseSensitive As Boolean = False) As NameValueCollection
			Dim oComparer As StringComparer = IIfStrict(pbCaseSensitive, StringComparer.Ordinal, StringComparer.OrdinalIgnoreCase)
			Dim oQueryVars As New NameValueCollection(oComparer)

			If psQueryString.StartsWith("?") Then
				psQueryString = Right(psQueryString, psQueryString.Length - 1)
			End If

			Dim sQueryVars As String() = psQueryString.Split("&"c)
			For Each sQueryVar As String In sQueryVars
				Dim sQueryVarPair As String() = sQueryVar.Split("="c)
				Dim sVar As String = HttpUtility.UrlDecode(sQueryVarPair(0))
				Dim sValue As String = String.Empty
				If sQueryVarPair.Length > 1 Then 'Check in case "=" is missing
					sValue = HttpUtility.UrlDecode(sQueryVarPair(1))
				End If

				oQueryVars(sVar) = (sValue)
			Next

			Return oQueryVars
		End Function
		Public Shared Function ConvertToBase64URLString(ByVal input As Byte()) As String
			'base64url standard: http://en.wikipedia.org/wiki/Base64#URL_applications
			Return Convert.ToBase64String(input).TrimEnd("="c).Replace("+", "-").Replace("/", "_")
		End Function

		Public Shared Function ConvertFromBase64URLString(ByVal input As String) As Byte()
			Dim base64Std As String = PadBase64String(input.Replace("-", "+").Replace("_", "/"))
			Return Convert.FromBase64String(base64Std)
		End Function

		Public Shared Function PadBase64String(ByVal input As String) As String
			'Pad to multiple of 4
			Dim paddingChars As Integer = (4 - input.Length Mod 4) Mod 4
			Return input.PadRight(input.Length + paddingChars, "="c)
		End Function

		''' <summary>
		''' Type-safe generic version of IIf.  This is also more Option Strict friendly.
		''' </summary>
		''' <typeparam name="T"></typeparam>
		''' <param name="expression"></param>
		''' <param name="truePart"></param>
		''' <param name="falsePart"></param>
		''' <returns></returns>
		''' <remarks>Note   The expressions in the argument list can include function calls. 
		''' As part of preparing the argument list for the call to IIf, the Visual Basic compiler calls every function in every expression. 
		''' This means that you cannot rely on a particular function not being called if the other argument is selected by Expression.
		''' 
		''' It's recommended to use the new .net If(exp,trueResult,falseResult) as it's short circuited.
		''' </remarks>
		Public Shared Function IIfStrict(Of T)(ByVal expression As Boolean, ByVal truePart As T, ByVal falsePart As T) As T
			If expression Then
				Return truePart
			Else
				Return falsePart
			End If
		End Function
	End Class
End Namespace