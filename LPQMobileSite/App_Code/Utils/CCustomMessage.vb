﻿Imports Microsoft.VisualBasic
Imports System.Xml
Imports System.Net
Imports LPQMobile.BO
Imports LPQMobile.Utils
Public Class CCustomMessage
    Private Shared _log As log4net.ILog = log4net.LogManager.GetLogger(GetType(CCustomMessage))

	Public Shared Function getComboCustomResponseMessages(ByVal poCurrentConfig As CWebsiteConfig, ByVal comboMsgNodeName As String, ByVal poResponse As String, ByVal poRequest As String, ByVal sAccountNumber As String, Optional loanTypeQueryString As String = "") As String
		If poCurrentConfig Is Nothing Then Return ""


		Dim loanRequest As XmlDocument = New XmlDocument()
		loanRequest.LoadXml(poRequest)
		Dim resultMessage As String = ""

		Dim innerXMLStr As String = ""
		innerXMLStr = loanRequest.InnerText

		Dim loanXML As XmlDocument = New XmlDocument
		loanXML.LoadXml(innerXMLStr)


		Dim loanType As String = Common.getCurrentLoanType(loanRequest)
		loanType = Common.MatchSubmissionTypeWithConfigType(loanType, loanRequest)

		Dim oComboMessageNode = poCurrentConfig._webConfigXML.SelectSingleNode(loanType & "/SUCCESS_MESSAGES/" & comboMsgNodeName)

		If loanType = "XA_LOAN" Then
			If loanTypeQueryString = "2s" Then 'special secondary
				oComboMessageNode = poCurrentConfig._webConfigXML.SelectSingleNode("XA_LOAN/SPECIAL_ACCOUNT_SUCCESS_MESSAGES/" & comboMsgNodeName)
			ElseIf loanTypeQueryString = "2b" Then ' business secondary
				oComboMessageNode = poCurrentConfig._webConfigXML.SelectSingleNode("XA_LOAN/BUSINESS_ACCOUNT_SUCCESS_MESSAGES/" & comboMsgNodeName)
			End If
			If oComboMessageNode Is Nothing Then
				oComboMessageNode = poCurrentConfig._webConfigXML.SelectSingleNode("XA_LOAN/SUCCESS_MESSAGES/" & comboMsgNodeName)
			End If
		Else
			oComboMessageNode = poCurrentConfig._webConfigXML.SelectSingleNode(loanType & "/SUCCESS_MESSAGES/" & comboMsgNodeName)
		End If
		If oComboMessageNode IsNot Nothing AndAlso oComboMessageNode.Attributes.ItemOf("message") IsNot Nothing Then
			Dim strCustomResponseMessage As String = ""
			Dim numData As Integer = 0
			Dim comboCustomDataList = New List(Of String)
			strCustomResponseMessage = oComboMessageNode.Attributes("message").Value
			If oComboMessageNode.Attributes.ItemOf("num_data") IsNot Nothing Then
				Dim hasNumData = Integer.TryParse(oComboMessageNode.Attributes("num_data").Value.ToString(), numData)
				If hasNumData AndAlso numData > 0 Then
					For i As Integer = 0 To numData - 1
						comboCustomDataList.Add(oComboMessageNode.Attributes("message_data" + i.ToString()).Value.ToString())
					Next
				End If
			End If
			' Create a new list of strings that will hold the data that was requested in the dataRequestList
			Dim dataList As List(Of String) = New List(Of String)
			For Each dataRequest As String In comboCustomDataList
				dataList.Add(GetComboDataValueFromRequest(dataRequest, poResponse, loanXML, sAccountNumber))
			Next
			'this error occur so frequent so it help to display the message to suppot
			Try
				resultMessage = String.Format(strCustomResponseMessage, dataList.ToArray())	' Now put the customMessage together with the data values
			Catch ex As Exception
				resultMessage = ""
				_log.Error("Message setup error in xml config.  Index is not correct    " & ex.Message)
			End Try
		End If
		Return resultMessage
	End Function

	Private Shared Function GetComboDataValueFromRequest(ByVal dataRequest As String, ByVal poResponse As String, ByVal loanXML As XmlDocument, ByVal sAccountNumber As String) As String
		Dim applicantNodeList As XmlNodeList = loanXML.GetElementsByTagName("APPLICANT")
		Dim applicantNode = applicantNodeList(0)

		''get first name
		Dim firstName As String = ""
		Dim lastName As String = ""

		If applicantNode IsNot Nothing Then
			If applicantNode.Attributes("first_name") IsNot Nothing Then
				firstName = applicantNode.Attributes("first_name").InnerXml
				firstName = firstName.Substring(0, 1).ToUpper & firstName.Substring(1, firstName.Length - 1)
			End If
			''get last name
			If applicantNode.Attributes("last_name") IsNot Nothing Then
				lastName = applicantNode.Attributes("last_name").InnerXml
				lastName = lastName.Substring(0, 1).ToUpper & lastName.Substring(1, lastName.Length - 1)
			End If
		End If

		'' get request card name
		Dim loanInfoNodeList As XmlNodeList = loanXML.GetElementsByTagName("LOAN_INFO")
		Dim loanInfo = loanInfoNodeList(0)
		Dim cardName As String = ""

		If loanInfo IsNot Nothing Then
			If loanInfo.Attributes("requested_card_name") IsNot Nothing Then
				cardName = loanInfo.Attributes("requested_card_name").InnerXml
			End If
		End If

		Dim sLoanNumber As String = Common.getLoanNumber(poResponse)
		Select Case dataRequest
			Case "LOAN_NUMBER"
				Return sLoanNumber

			Case "MEMBER_LOAN_NUMBER", "MEMBER_NUMBER"
				Return sAccountNumber

			Case "FIRST_NAME"
				Return firstName

			Case "FULL_NAME"
				Return firstName + " " + lastName

			Case "CARD_NAME"
				Return cardName

		End Select

		Return ""
	End Function

    Public Shared Function getLoanStatus(ByVal poResponse As String) As String
        Dim loanStatus As String = ""  
        Try
            Dim responseXMLDoc As XmlDocument = New XmlDocument()
            responseXMLDoc.LoadXml(poResponse)
            Dim decision As XmlElement = responseXMLDoc.SelectSingleNode("/OUTPUT/RESPONSE/DECISION")
            If decision IsNot Nothing Then
                Return decision.GetAttribute("status")
            End If
        Catch ex As Exception
            _log.Error("Unable to get loan status ", ex)
            Return ""
        End Try
        Return loanStatus
    End Function

End Class
