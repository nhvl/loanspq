﻿Imports OfficeOpenXml.FormulaParsing.Excel.Functions.Text

Namespace QueryBuilder
	Public Class Common
		Public Shared Function IS_ARRAY(ByVal p1 As List(Of String), ByVal ParamArray ps() As String) As Boolean
			Return IS_ARRAY(Of String)(p1, ps)
		End Function
		Public Shared Function IS_ARRAY(ByVal p1 As List(Of Integer), ByVal ParamArray ps() As Integer) As Boolean
			Return IS_ARRAY(Of Integer)(p1, ps)
		End Function
		Public Shared Function IS_ARRAY(ByVal p1 As List(Of Double), ByVal ParamArray ps() As Double) As Boolean
			Return IS_ARRAY(Of Double)(p1, ps)
		End Function
		Public Shared Function IS_ARRAY(ByVal p1 As List(Of Decimal), ByVal ParamArray ps() As Decimal) As Boolean
			Return IS_ARRAY(Of Decimal)(p1, ps)
		End Function
		Public Shared Function IS_ARRAY(ByVal p1 As List(Of DateTime), ByVal ParamArray ps() As DateTime) As Boolean
			Return IS_ARRAY(Of DateTime)(p1, ps)
		End Function

		Public Shared Function INTERSECTS(ByVal p1 As List(Of String), ByVal ParamArray ps() As String) As Boolean
			Return INTERSECTS(Of String)(p1, ps)
		End Function
		Public Shared Function INTERSECTS(ByVal p1 As List(Of Integer), ByVal ParamArray ps() As Integer) As Boolean
			Return INTERSECTS(Of Integer)(p1, ps)
		End Function
		Public Shared Function INTERSECTS(ByVal p1 As List(Of Double), ByVal ParamArray ps() As Double) As Boolean
			Return INTERSECTS(Of Double)(p1, ps)
		End Function
		Public Shared Function INTERSECTS(ByVal p1 As List(Of Decimal), ByVal ParamArray ps() As Decimal) As Boolean
			Return INTERSECTS(Of Decimal)(p1, ps)
		End Function
		Public Shared Function INTERSECTS(ByVal p1 As List(Of DateTime), ByVal ParamArray ps() As DateTime) As Boolean
			Return INTERSECTS(Of DateTime)(p1, ps)
		End Function

		Public Shared Function IS_A_SUPERSET_OF(ByVal p1 As List(Of String), ByVal ParamArray ps() As String) As Boolean
			Return IS_A_SUPERSET_OF(Of String)(p1, ps)
		End Function
		Public Shared Function IS_A_SUPERSET_OF(ByVal p1 As List(Of Integer), ByVal ParamArray ps() As Integer) As Boolean
			Return IS_A_SUPERSET_OF(Of Integer)(p1, ps)
		End Function
		Public Shared Function IS_A_SUPERSET_OF(ByVal p1 As List(Of Double), ByVal ParamArray ps() As Double) As Boolean
			Return IS_A_SUPERSET_OF(Of Double)(p1, ps)
		End Function
		Public Shared Function IS_A_SUPERSET_OF(ByVal p1 As List(Of Decimal), ByVal ParamArray ps() As Decimal) As Boolean
			Return IS_A_SUPERSET_OF(Of Decimal)(p1, ps)
		End Function
		Public Shared Function IS_A_SUPERSET_OF(ByVal p1 As List(Of DateTime), ByVal ParamArray ps() As DateTime) As Boolean
			Return IS_A_SUPERSET_OF(Of DateTime)(p1, ps)
		End Function

		Public Shared Function IS_A_SUBSET_OF(ByVal p1 As List(Of String), ByVal ParamArray ps() As String) As Boolean
			Return IS_A_SUBSET_OF(Of String)(p1, ps)
		End Function
		Public Shared Function IS_A_SUBSET_OF(ByVal p1 As List(Of Integer), ByVal ParamArray ps() As Integer) As Boolean
			Return IS_A_SUBSET_OF(Of Integer)(p1, ps)
		End Function
		Public Shared Function IS_A_SUBSET_OF(ByVal p1 As List(Of Double), ByVal ParamArray ps() As Double) As Boolean
			Return IS_A_SUBSET_OF(Of Double)(p1, ps)
		End Function
		Public Shared Function IS_A_SUBSET_OF(ByVal p1 As List(Of Decimal), ByVal ParamArray ps() As Decimal) As Boolean
			Return IS_A_SUBSET_OF(Of Decimal)(p1, ps)
		End Function
		Public Shared Function IS_A_SUBSET_OF(ByVal p1 As List(Of DateTime), ByVal ParamArray ps() As DateTime) As Boolean
			Return IS_A_SUBSET_OF(Of DateTime)(p1, ps)
		End Function
		Public Shared Function X_DAYS_BEFORE_TODAY(d As String, x As Integer) As Boolean
			If IsDate(d) Then
				Dim dd = CDate(d)
				Return x > 0 AndAlso Math.Floor(Now.Subtract(dd).TotalDays).Equals(CDbl(x))
			End If
			Return False
		End Function
		Public Shared Function WITHIN_X_DAYS_BEFORE_TODAY(d As String, x As Integer) As Boolean
			If IsDate(d) Then
				Dim dd = CDate(d)
				Return x > 0 AndAlso Now.Subtract(dd).TotalDays <= x
			End If
			Return False
		End Function
		Public Shared Function OLDER_THAN_X_DAYS_BEFORE_TODAY(d As String, x As Integer) As Boolean
			If IsDate(d) Then
				Dim dd = CDate(d)
				Return x > 0 AndAlso Now.Subtract(dd).TotalDays > x
			End If
			Return False
		End Function
		Public Shared Function IS_DEFINED(input As Date) As Boolean
			Return IsDate(input)
		End Function
		Public Shared Function IS_DEFINED(input As String) As Boolean
			Return input IsNot Nothing AndAlso input.Trim() <> ""
		End Function
		Public Shared Function ENDS_WITH(p1 As String, p2 As String) As Boolean
			Return Not String.IsNullOrEmpty(p1) AndAlso Not String.IsNullOrEmpty(p2) AndAlso p1.EndsWith(p2)
		End Function
		Public Shared Function BEGINS_WITH(p1 As String, p2 As String) As Boolean
			Return Not String.IsNullOrEmpty(p1) AndAlso Not String.IsNullOrEmpty(p2) AndAlso p1.StartsWith(p2)
		End Function
		Public Shared Function CONTAINS(p1 As String, p2 As String) As Boolean
			Return Not String.IsNullOrEmpty(p1) AndAlso Not String.IsNullOrEmpty(p2) AndAlso p1.Contains(p2)
		End Function
		Public Shared Function BETWEEN(p As Integer, lower As Integer, upper As Integer) As Boolean
			Return p >= lower AndAlso p <= Upper
		End Function
		Public Shared Function IS_IN(p As Integer, ByVal ParamArray ps() As Integer) As Boolean
			Return IS_ONE_OF(Of Integer)(p, ps)
		End Function
		Public Shared Function IS_IN(p As String, ByVal ParamArray ps() As String) As Boolean
			Return IS_ONE_OF(Of String)(p, ps)
		End Function
		Public Shared Function IS_IN(p As Date, ByVal ParamArray ps() As Date) As Boolean
			Return IS_ONE_OF(Of Date)(p, ps)
		End Function
		Public Shared Function IS_ONE_OF(p As Integer, ByVal ParamArray ps() As Integer) As Boolean
			Return IS_ONE_OF(Of Integer)(p, ps)
		End Function
		Public Shared Function IS_ONE_OF(p As String, ByVal ParamArray ps() As String) As Boolean
			Return IS_ONE_OF(Of String)(p, ps)
		End Function
		Public Shared Function IS_ONE_OF(p As Date, ByVal ParamArray ps() As Date) As Boolean
			Return IS_ONE_OF(Of Date)(p, ps)
		End Function
		Private Shared Function IS_ONE_OF(Of T)(p As T, ByVal ParamArray ps() As T) As Boolean
			Return ps IsNot Nothing AndAlso ps.Any() AndAlso ps.Contains(p)
		End Function

		Public Shared Function IS_A_SUPERSET_OF(Of T)(ByVal p1 As List(Of T), ByVal ParamArray ps() As T) As Boolean
			If p1 Is Nothing OrElse p1.Count = 0 OrElse (ps IsNot Nothing AndAlso p1.Count < ps.Count()) Then Return False
			Return p1.Except(ps).Count() >= 0 AndAlso ps.Except(p1).Count() = 0
		End Function
		Public Shared Function INTERSECTS(Of T)(ByVal p1 As List(Of T), ByVal ParamArray ps() As T) As Boolean
			If p1 Is Nothing OrElse ps Is Nothing Then Return False
			Return p1.Intersect(ps).Count > 0
		End Function
		Private Shared Function IS_A_SUBSET_OF(Of T)(ByVal p1 As List(Of T), ByVal ParamArray ps() As T) As Boolean
			If p1 Is Nothing OrElse p1.Count = 0 OrElse ps Is Nothing OrElse ps.Count() = 0 OrElse ps.Count() < p1.Count Then Return False
			Return p1.Except(ps).Count = 0 AndAlso ps.Except(p1).Count >= 0
		End Function
		Private Shared Function IS_ARRAY(Of T)(ByVal p1 As List(Of T), ByVal ParamArray ps() As T) As Boolean
			If p1 Is Nothing OrElse p1.Count = 0 OrElse ps Is Nothing OrElse ps.Count = 0 OrElse ps.Count() <> p1.Count Then Return False
			Return p1.Except(ps).Count = 0 AndAlso ps.Except(p1).Count = 0
		End Function
	End Class
End Namespace




