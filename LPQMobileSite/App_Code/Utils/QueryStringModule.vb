﻿


Imports System.IO
Imports System.Web
Imports System.Text
Imports System.Security.Cryptography
Imports System.Configuration

''' <summary>
''' Summary description for QueryStringModule
''' https://madskristensen.net/post/httpmodule-for-query-string-encryption
''' don't really need to use IHttpModule since the invocation has been moved to global.asax.  this is required so this module can be called before the log4net initialization occurs
''' </summary>
Public Class QueryStringModule
	Implements IHttpModule

	Protected Shared Log As log4net.ILog = log4net.LogManager.GetLogger(GetType(QueryStringModule))

#Region "IHttpModule Members"

	Public Sub Dispose() Implements IHttpModule.Dispose
		' Nothing to dispose
	End Sub

	Public Sub Init(context As HttpApplication) Implements IHttpModule.Init
		'context.BeginRequest += New EventHandler(AddressOf context_BeginRequest)

		'TODO:tobe remove
		''moved to BeginRequest in globasl.asax
		'Log.Info("start QueryStringModule init")
		'AddHandler context.BeginRequest, AddressOf context_BeginRequest
	End Sub


#End Region

	Private Shared ENCRYPTION_KEY As String = If(ConfigurationManager.AppSettings.Get("URL_ENCRYPTION_KEY") <> "", ConfigurationManager.AppSettings.Get("URL_ENCRYPTION_KEY"), "123$ml1789!")
	Private Const PARAMETER_NAME As String = "enc="
	Private Const PARAMETER_NAME_DEBUG As String = "enc123="  ' decrypting url and display it back in url

	Public Shared Sub context_BeginRequest(sender As Object, e As EventArgs)
        'Log.Info("start QueryStringModule context_BeginRequest")
        'comment this if we want to encryption in local env
        Dim sEnv As String = System.Configuration.ConfigurationManager.AppSettings.Get("ENVIRONMENT")
        If (sEnv <> "LIVE") AndAlso (sEnv <> "TEST") AndAlso (sEnv <> "STAGE") Then Return 'don't do encrytion for local 

        Dim context As HttpContext = HttpContext.Current

        'only do encryption for application page so httpmodule will not interfer with other callback
        If Not (context.Request.Url.LocalPath.ToLower().Contains("apply.aspx") = True Or context.Request.Url.LocalPath.ToLower().Contains("personalloan.aspx") = True Or context.Request.Url.LocalPath.ToLower().Contains("creditcard.aspx") = True Or context.Request.Url.LocalPath.ToLower().Contains("homeequityloan.aspx") = True Or context.Request.Url.LocalPath.ToLower().Contains("vehicleloan.aspx") = True Or context.Request.Url.LocalPath.ToLower().Contains("xa/xpressapp.aspx") = True Or context.Request.Url.LocalPath.ToLower().Contains("bl/businessloan.aspx") = True Or context.Request.Url.LocalPath.ToLower().Contains("cu/viewsubmittedloans.aspx") = True) Then 'dont want accu/xpressapp.aspx
            Return
        End If

        If context.Request.QueryString("mode") = "777" Then Return

		'Log.Info("start QueryStringModule context_BeginRequest OK")

		If context.Request.Url.OriginalString.Contains("aspx") AndAlso context.Request.RawUrl.Contains("?") Then
			Dim query As String = ExtractQuery(context.Request.RawUrl)
			Dim path As String = GetVirtualPath()

			If query.Length < 6 Then Return 'probably incorrect parameter so skip and avoid wasting resource

            'This for supporting GoogleAnalytic query appending with either encrypted or unencrypted url
            Dim sGoogleAnalyticQuery = ExtractGoogleAnalyticQuery(query) ' null if there is no ga parameter
            If sGoogleAnalyticQuery <> "" Then sGoogleAnalyticQuery = "&" & sGoogleAnalyticQuery

            If query.StartsWith(PARAMETER_NAME_DEBUG, StringComparison.OrdinalIgnoreCase) Then
                'this loop is for decrypting url and display it back in url, remove "noencrypt" from url to get the origianal query string
                Dim rawQuery As String = query.Replace(PARAMETER_NAME_DEBUG, String.Empty)
                If sGoogleAnalyticQuery <> "" Then
                    rawQuery = query.Replace(PARAMETER_NAME_DEBUG, String.Empty).Replace(sGoogleAnalyticQuery, String.Empty)
                End If
                Dim decryptedQuery As String = Decrypt(rawQuery)
                context.Response.Redirect(path & "?" & decryptedQuery & "&noencrypt=" & sGoogleAnalyticQuery)
            ElseIf query.StartsWith(PARAMETER_NAME, StringComparison.OrdinalIgnoreCase) Then
                ' Decrypts the query string and rewrites the path.
                Dim rawQuery As String = query.Replace(PARAMETER_NAME, String.Empty)
                If sGoogleAnalyticQuery <> "" Then
                    rawQuery = query.Replace(PARAMETER_NAME, String.Empty).Replace(sGoogleAnalyticQuery, String.Empty)
                End If
                Dim decryptedQuery As String = Decrypt(rawQuery)
                context.RewritePath(path, String.Empty, decryptedQuery & sGoogleAnalyticQuery)
            ElseIf context.Request.HttpMethod = "GET" And Not query.Contains("noencrypt") Then
                ' Encrypt the query string and redirects to the encrypted URL.
                ' Remove if you don't want all query strings to be encrypted automatically.
                Dim encryptedQuery As String = Encrypt(If(sGoogleAnalyticQuery <> "", query.Replace(sGoogleAnalyticQuery, String.Empty), query))
                context.Response.Redirect(path & encryptedQuery & sGoogleAnalyticQuery)
            End If

		End If
	End Sub

	''' <summary>
	''' Parses the current URL and extracts the virtual path without query string.
	''' </summary>
	''' <returns>The virtual path of the current URL.</returns>
	Private Shared Function GetVirtualPath() As String
		Dim path As String = HttpContext.Current.Request.RawUrl
		path = path.Substring(0, path.IndexOf("?"))
		path = path.Substring(path.LastIndexOf("/") + 1)
		Return path
	End Function

	''' <summary>
	''' Parses a URL and returns the query string.
	''' </summary>
	''' <param name="url">The URL to parse.</param>
	''' <returns>The query string without the question mark.</returns>
	Private Shared Function ExtractQuery(url As String) As String
		Dim index As Integer = url.IndexOf("?") + 1
		Return url.Substring(index)
	End Function

    Private Shared Function ExtractGoogleAnalyticQuery(psRawQery As String) As String
        Dim sGoogleAnalyticQuery As String = ""
        Dim sParameters As List(Of String) = psRawQery.Split(New Char() {"&"c}).Select(Function(s) s.Trim()).ToList()
        For Each sParameter As String In sParameters
            If sParameter.NullSafeToLower_.StartsWith("_ga") Then
                sGoogleAnalyticQuery = sParameter
                Exit For
            End If
        Next
        Return sGoogleAnalyticQuery
    End Function
#Region "Encryption/decryption"


    ''' <summary>
    ''' The salt value used to strengthen the encryption.
    ''' </summary>
    Private Shared ReadOnly SALT As Byte() = Encoding.ASCII.GetBytes(ENCRYPTION_KEY.Length.ToString())


	''' <summary>
	''' Encrypts any string using the Rijndael algorithm.
	''' </summary>
	''' <param name="inputText">The string to encrypt.</param>
	''' <returns>A Base64 encrypted string.</returns>
	Public Shared Function Encrypt(inputText As String) As String
		If inputText Is Nothing Or inputText = "" Then Return ""
		Dim rijndaelCipher As New RijndaelManaged()
		Dim plainText As Byte() = Encoding.Unicode.GetBytes(inputText)
		Dim SecretKey As New PasswordDeriveBytes(ENCRYPTION_KEY, SALT)

		Using encryptor As ICryptoTransform = rijndaelCipher.CreateEncryptor(SecretKey.GetBytes(32), SecretKey.GetBytes(16))
			Using memoryStream As New MemoryStream()
				Using cryptoStream As New CryptoStream(memoryStream, encryptor, CryptoStreamMode.Write)
					cryptoStream.Write(plainText, 0, plainText.Length)
					cryptoStream.FlushFinalBlock()
					Return (Convert.ToString("?") & PARAMETER_NAME) + ConvertToBase64URLString(memoryStream.ToArray())
				End Using
			End Using
		End Using
	End Function

	''' <summary>
	''' Decrypts a previously encrypted string.
	''' </summary>
	''' <param name="inputText">The encrypted string to decrypt.</param>
	''' <returns>A decrypted string.</returns>
	Public Shared Function Decrypt(inputText As String) As String
		If inputText Is Nothing Or inputText = "" Then Return ""

		Dim rijndaelCipher As New RijndaelManaged()
		Dim encryptedData As Byte() = ConvertFromBase64URLString(inputText)
		Dim secretKey As New PasswordDeriveBytes(ENCRYPTION_KEY, SALT)

		Using decryptor As ICryptoTransform = rijndaelCipher.CreateDecryptor(secretKey.GetBytes(32), secretKey.GetBytes(16))
			Using memoryStream As New MemoryStream(encryptedData)
				Using cryptoStream As New CryptoStream(memoryStream, decryptor, CryptoStreamMode.Read)
					Dim plainText As Byte() = New Byte(encryptedData.Length - 1) {}
					Dim decryptedCount As Integer = cryptoStream.Read(plainText, 0, plainText.Length)
					Return Encoding.Unicode.GetString(plainText, 0, decryptedCount)
				End Using
			End Using
		End Using
	End Function

	''========================================
	''use this so browser won't need auto encode url
	Public Shared Function ConvertToBase64URLString(ByVal input As Byte()) As String
		'base64url standard: http://en.wikipedia.org/wiki/Base64#URL_applications
		Return Convert.ToBase64String(input).TrimEnd("="c).Replace("+", "-").Replace("/", "_")
	End Function

	Public Shared Function ConvertFromBase64URLString(ByVal input As String) As Byte()
		Dim base64Std As String = PadBase64String(input.Replace("-", "+").Replace("_", "/").Replace("%3d", "=").Replace("%2f", "/").Replace("%2b", "+"))
		Return Convert.FromBase64String(base64Std)
	End Function

	Public Shared Function PadBase64String(ByVal input As String) As String
		'Pad to multiple of 4
		Dim paddingChars As Integer = (4 - input.Length Mod 4) Mod 4
		Return input.PadRight(input.Length + paddingChars, "="c)
	End Function

#End Region


End Class
