﻿Imports LPQMobile.BO
Imports LPQMobile.Utils
Imports System.Xml
Imports System.Xml.Serialization
Imports System.Threading
Imports System.IO
Imports System.Web.Script.Serialization
Imports System.Net
Imports System.Web
Imports System.Text
Public Class CCustomWorkFlows
    Protected Const constSearchingUrl = "/Integration/API/CIS/CIS.aspx"
    Protected Const constBookingURL As String = "/bookapp/bookapp.aspx"
    Private _log As log4net.ILog = log4net.LogManager.GetLogger(GetType(CCustomWorkFlows))
    Public oConfig As CWebsiteConfig
    Public WorkFlowMode As String
    Sub New(ByVal poConfig As CWebsiteConfig, ByVal psWorkFlowMode As String)
        Me.oConfig = poConfig
        Me.workFlowMode = psWorkFlowMode
    End Sub
    Public Function ExecRedstoneCustomWorkFlow(ByVal sLoanId As String, ByVal sLoanType As String, ByVal sExistMemberNumber As String) As Boolean
        _log.Info("Start ExecRedstoneCustomWorkFlow")
        Dim bookingSuccess As Boolean = False
        If sExistMemberNumber <> "" Then ''has member number then do the TMG booking (no need to do CMC booking to create membership)         
            UpdateLoan(sLoanId, "UPDATE_MEMBERNUMBER", "", sExistMemberNumber) ''update member number before do the booking
            Dim sAccountNumber = CBookingManager.Book(oConfig, sLoanId, sLoanType) ''TMGBooking(sLoanId, sLoanType)
            If sAccountNumber <> "" Then
                bookingSuccess = True
            Else
                UpdateLoan(sLoanId, "TMG_FAILED", "", "") ''booking(TMG) failed: update status to referred and update comment: "Booking App(TMG) is not successfully.
            End If
        Else ''no existing member number => book membership without xa, book account
            'Dim sMemberNumber = CBookingManager.Book(oConfig, sLoanId, sLoanType, True)  ''booking(CMC) to create a membership for loan
            Dim sMemberNumber = CBookingManager.CreatMembershipNoXA(oConfig, sLoanId, sLoanType)
            If sMemberNumber <> "" Then ''book membership success then do the TMG booking
                UpdateLoan(sLoanId, "UPDATE_MEMBERNUMBER", "", sMemberNumber) ''update member number before do the booking
                Dim sAccountNumber = CBookingManager.Book(oConfig, sLoanId, sLoanType) ''TMGBooking(sLoanId, sLoanType)
                If sAccountNumber <> "" Then
                    bookingSuccess = True
                Else
                    UpdateLoan(sLoanId, "TMG_FAILED", "", "") ''booking(TMG) failed: update status to referred and update comment: "Booking App(TMG) is not successfully.
                End If
            Else ''book membership not success
                UpdateLoan(sLoanId, "CMC_FAILED", "", "") '' update status to referred and update comment: "Booking Membership(CMC) is not successfully.
            End If
        End If
        Return bookingSuccess
    End Function
    Public Function ExecPacificCustomWorkFlow(ByVal psLoanId As String, ByVal psLoanType As String, ByVal psloanRequestXMLStr As String) As Boolean
        ''if the application has member number then using CIS service to validate the member number
        ''   -if the member number is valid then do the booking otherwise update status to pending
        ''if the application does not have member then using CIS service to search the member number
        ''      if it has member number then update member number and do the booking otherwise update status to pending
        Dim sMemberNumber = GetSelectedAttrValue(psloanRequestXMLStr, "member_number")
        Dim sSSN = GetSelectedAttrValue(psloanRequestXMLStr, "ssn")
        Dim sIntegrationType = Common.getNodeAttributes(oConfig, Common.GetConfigLoanTypeFromShort(psLoanType), "loan_booking")
        If String.IsNullOrWhiteSpace(sMemberNumber) Then
            ''using primary SSN to search existing member number
            Dim sSearchResponseXml = searchApplicantInfo(sSSN, sIntegrationType, "SSN")
            Dim oApplicantInfo = New CCISResponse(sSearchResponseXml)
            Dim oAppInfoList = oApplicantInfo.CustomerInfoList.Where(Function(app) app.SSN = sSSN And Not String.IsNullOrWhiteSpace(app.MemberNumber)).ToList()
            If oAppInfoList IsNot Nothing AndAlso oAppInfoList.Count > 0 Then
                If oAppInfoList.Count = 1 Then ''has one member number 
                    ''update member number before do the booking
                    UpdateLoan(psLoanId, "UPDATE_MEMBERNUMBER", "", oAppInfoList(0).MemberNumber)
                    ''corelation booking
                    Dim sAccountNumber = CBookingManager.Book(oConfig, psLoanId, psLoanType)
                    If sAccountNumber = "" Then ''booking failed
                        ''update status to referred, and update comment:Update loan status from Instant Approved to Referred because booking Corelation is not successfully. 
                        UpdateLoan(psLoanId, "BOOKING_FAILED", "", "")
                        Return False
                    End If
                    Return True
                Else '' has more then one member number, update status to pending
                    UpdateLoan(psLoanId, "MANY_MEMBERNUMBER", "", "")
                    Return False
                End If
            Else
                ''update status to pending
                UpdateLoan(psLoanId, "MEMBERNUMBER_NOTFOUND", "", "")
                Return False
            End If
        Else
            ''using member number to search app info, if it map ssn then do the corelation booking
            ''otherwise update status to pending, and update comment: Update loan status from Instant Approved to Pending because the service could not find a primary member number.
            Dim sSearchResponseXml = searchApplicantInfo(sMemberNumber, sIntegrationType, "MEMBERNUMBER")
            Dim oApplicantInfo = New CCISResponse(sSearchResponseXml)
            Dim oAppInfoList = oApplicantInfo.CustomerInfoList.Where(Function(app) app.SSN = sSSN And app.MemberNumber = sMemberNumber)
            If oAppInfoList IsNot Nothing AndAlso oAppInfoList.Count > 0 Then
                Dim sAccountNumber = CBookingManager.Book(oConfig, psLoanId, psLoanType)
                If sAccountNumber = "" Then ''booking failed
                    ''update status to referred, and update comment:Update loan status from Instant Approved to Referred because booking Corelation is not successfully. 
                    UpdateLoan(psLoanId, "BOOKING_FAILED", "", "")
                    Return False
                End If
                Return True
            Else
                ''update status to Pending, and update comment: Update loan status from Instant Approved to Pending because the member number xxxxx is not valid.
                UpdateLoan(psLoanId, "MEMBERNUMBER_NOTFOUND", "", sMemberNumber)
                Return False
            End If
        End If
    End Function
    Public Function searchApplicantInfo(ByVal sLookupNumber As String, ByVal sIntegrationType As String, ByVal sSearchType As String) As String
        Dim sXml As String = ""
        If String.IsNullOrEmpty(oConfig.APIUserLender) Or String.IsNullOrEmpty(oConfig.APIPasswordLender) Then
            _log.Warn("This search API require lender level credential.")
            Return sXml
        End If
        Dim sRequestXML = BuildSearchingRequestXML(sLookupNumber, sIntegrationType, sSearchType)
        ''Begin post
        Try
            Dim sUrl =  oConfig.BaseSubmitLoanUrl + constSearchingUrl
            Dim req As WebRequest = WebRequest.Create(sUrl)
            req.Method = "POST"
            req.ContentType = "text/xml"
            _log.InfoFormat("Preparing to POST Data To: {0}", sUrl)
            _log.InfoFormat("Preparing to POST Searching Data: {0}", CSecureStringFormatter.MaskSensitiveXMLData(sRequestXML))
            Dim streamWriter As New System.IO.StreamWriter(req.GetRequestStream())
            streamWriter.Write(sRequestXML)
            streamWriter.Close()
            Dim res As WebResponse = req.GetResponse()
            Dim reader As New System.IO.StreamReader(res.GetResponseStream())
            sXml = reader.ReadToEnd()
            _log.DebugFormat("Searching response from Core: {0}", sXml)
            reader.Close()
            res.GetResponseStream().Close()
            req.GetRequestStream().Close()
        Catch ex As Exception
            _log.Error("Searching error: " & ex.Message, ex)
        End Try
        Return sXml
    End Function
    Public Function getExistMemberNumber(ByVal sloanRequestXMLStr As String, ByRef bAPIError As Boolean) As String
        Dim sSSN = GetSelectedAttrValue(sloanRequestXMLStr, "ssn")
        Dim sSearchResponseXml = searchApplicantInfo(sSSN, "CMC", "SSN")
        If sSearchResponseXml = "" Then
            bAPIError = True
            Return ""
        End If
        Dim oApplicantInfo = New CCISResponse(sSearchResponseXml)
        Dim oCMCAppInfo = oApplicantInfo.CustomerInfoList
        If oCMCAppInfo.Count > 0 Then
            ''only check the first applicant info(primary applicant)
            Return Common.SafeString(oCMCAppInfo(0).MemberNumber)
        Else
            If Not (sSearchResponseXml.ToUpper.Contains("NO PERSON FOUND") Or sSearchResponseXml.ToUpper.Contains("NO CUSTOMER FOUND")) Then
                bAPIError = True
            End If
        End If
        Return ""
    End Function
    '' add integrationType and searchType parameters to BuildSearchingRequestXML function
    '' so this function could be used for different integration type and search type
    Public Function BuildSearchingRequestXML(ByVal sLookupNumber As String, ByVal sIntegrationType As String, ByVal sSearchType As String) As String
        Dim sb As New StringBuilder()
        Dim writer As XmlWriter = XmlTextWriter.Create(sb)
        writer.WriteStartDocument()
        writer.WriteStartElement("INPUT")
        writer.WriteStartElement("LOGIN")
        writer.WriteStartElement("API_CREDENTIALS")
        writer.WriteAttributeString("api_user_id", oConfig.APIUserLender)
        writer.WriteAttributeString("api_password", oConfig.APIPasswordLender)
        writer.WriteEndElement() 'end API_CREDENTIALS 
        writer.WriteEndElement() 'end login
        writer.WriteStartElement("REQUEST")
        writer.WriteAttributeString("integration_type", sIntegrationType)
        writer.WriteStartElement("SEARCHTYPE")
        writer.WriteString(sSearchType) '' this value could be SSN/CIF
        writer.WriteEndElement() 'end SearchType
        writer.WriteStartElement("SEARCHVALUE")
        writer.WriteString(sLookupNumber) ' this value could be SSN/MemberNumber
        writer.WriteEndElement() 'end SEARCHVALUE
        writer.WriteEndElement() 'end REQUEST
        writer.WriteEndElement() ' end input
        writer.Flush()
        writer.Close()
        Return sb.ToString()
    End Function
    Public Function GetSelectedAttrValue(ByVal poRequestXMLStr As String, ByVal psSelectedAttr As String) As String
        Dim sValue As String = ""
        Dim loanRequest As XmlDocument = New XmlDocument()
        loanRequest.LoadXml(poRequestXMLStr)
        Dim innerXMLStr As String = ""
        innerXMLStr = loanRequest.InnerText
        Dim loanXML As XmlDocument = New XmlDocument
        loanXML.LoadXml(innerXMLStr)
        Dim applicantNodes As XmlNodeList = loanXML.GetElementsByTagName("APPLICANT")
        ''get primary SSN
        If applicantNodes IsNot Nothing AndAlso applicantNodes.Count > 0 Then
            Dim selectedAttr = CType(applicantNodes(0), XmlElement).Attributes(psSelectedAttr)
            If selectedAttr IsNot Nothing Then
                sValue = Common.SafeString(selectedAttr.InnerXml)
            End If
        End If
        Return sValue
    End Function
    Public Sub UpdateLoan(ByVal psLoanID As String, ByVal psStatus As String, ByVal psVerifyIdStatus As String, ByVal psMemberNumber As String)
        If psLoanID = "" Then Return ' this is used for loan only
        Dim sComment As String = ""
        Dim sStatus = psStatus.ToUpper
        Dim url As String = oConfig.BaseSubmitLoanUrl + "/GetLoans/GetLoans.aspx"
        Dim getLoansXMLRequestStr As String = LoanRetrieval.BuildGetLoansXML(psLoanID, oConfig.APIUser, oConfig.APIPassword)
        Dim req As WebRequest = WebRequest.Create(url)
        req.Method = "POST"
        req.ContentType = "text/xml"
        CPBLogger.logInfo("Preparing to POST GetLoans Request: " & CSecureStringFormatter.MaskSensitiveXMLData(getLoansXMLRequestStr))
        Dim writer As New StreamWriter(req.GetRequestStream())
        writer.Write(getLoansXMLRequestStr)
        writer.Close()
        Dim res As WebResponse = req.GetResponse()
        Dim reader As New StreamReader(res.GetResponseStream())
        Dim sXml As String = reader.ReadToEnd()
        _log.Info("GetLoans Response: " & CSecureStringFormatter.MaskSensitiveXMLData(sXml))

        ' Extract the inner xml and update the status of the loans 
        Dim responseXMLDoc As XmlDocument = New XmlDocument()
        responseXMLDoc.LoadXml(sXml)
        Dim innerXMLStr As String = ""
        innerXMLStr = responseXMLDoc.InnerText
        Dim innerXMLDoc As XmlDocument = New XmlDocument()
        innerXMLDoc.LoadXml(innerXMLStr)
        ''update status
        Dim loanStatusNodes As XmlNodeList = innerXMLDoc.GetElementsByTagName("LOAN_STATUS")
        Dim loanStatusNode As XmlElement = loanStatusNodes(0)
        Dim applicantNodes As XmlNodeList = innerXMLDoc.GetElementsByTagName("APPLICANT")
        Dim applicantNode As XmlElement = applicantNodes(0)
        If workFlowMode.ToUpper.Contains("REDSTONE") Then
            ''booking failed Change Status to Referred
            If sStatus = "API_CIS_FAILED" Or sStatus = "CMC_FAILED" Or sStatus = "TMG_FAILED" Or sStatus = "IDENTITY_FAILED" Then
                loanStatusNode.SetAttribute("loan_status", "REF")
            ElseIf sStatus = "UPDATE_MEMBERNUMBER" Then
                ''update member number            
                applicantNode.SetAttribute("member_number", psMemberNumber)
            End If
            'dont want to alter the time
            loanStatusNode.RemoveAttribute("response_date")
            loanStatusNode.RemoveAttribute("app_receive_date")
            ''update comment
            If sStatus = "API_CIS_FAILED" Then
                sComment = "Update loan status from Instant Approved to Referred because API CIS service is failed to search customer information."
            ElseIf sStatus = "CMC_FAILED" Then
                sComment = "Update loan status from Instant Approved to Referred because Booking Membership(CMC) is not successfully. "
            ElseIf sStatus = "BOOKING_TMG_FAILED" Then
                sComment = "Update loan status from Approved to Referred because Booking App(TMG) is not successfully. "
            ElseIf sStatus = "IDENTITY_FAILED" Then
                If psVerifyIdStatus.ToLower = "none" Then
                    sComment = "Update loan status from Approved to Referred because there is no docusign for ida verification. "
                ElseIf (psVerifyIdStatus.ToLower = "id_check_failed") Then
                    sComment = "Update loan status from Approved to Referred because Consumer identity could not be verified through Docusign. "
                ElseIf (psVerifyIdStatus.ToLower = "decline") Then
                    sComment = "Update loan status from Approved to Referred because the applicant selected the decline to sign selection. "
                Else
                    sComment = "Update loan status from Approved to Referred because the docusign is not completed. "
                End If
            ElseIf sStatus = "UPDATE_MEMBERNUMBER" Then
                sComment = "Update member number before booking(TMG)."
            ElseIf sStatus = "NO_TMG_BOOKING" Then
                sComment = "No booking(TMG) because the is no existing membership. "
                End If
                If Not String.IsNullOrEmpty(psVerifyIdStatus) Then
                    sComment &= "(Docusign Status: " & psVerifyIdStatus & ")"
                End If
            ElseIf WorkFlowMode.ToUpper.Contains("PACIFIC") Then
                If sStatus = "BOOKING_FAILED" Then ''change status to refer
                loanStatusNode.SetAttribute("loan_status", "REF")
                sComment = "Update loan status from Instant Approved to Referred because Corelation booking is not successfully. "
            ElseIf sStatus = "UPDATE_MEMBERNUMBER" Then
                ''update member number            
                applicantNode.SetAttribute("member_number", psMemberNumber)
                sComment = "Update member number " & psMemberNumber & " before do the booking. "
            ElseIf sStatus = "MEMBERNUMBER_NOTFOUND" Or sStatus = "MANY_MEMBERNUMBER" Then
                ''update status to pending
                loanStatusNode.SetAttribute("loan_status", "PEN")
                If sStatus = "MANY_MEMBERNUMBER" Then
                    sComment = "Update loan status from Instant Approved to Pending because the applicant has more than one member number. "
                Else
                    sComment = "Update loan status from instant Approved to Pending because the member number is not found."
                End If
            End If
            'dont want to alter the time
            loanStatusNode.RemoveAttribute("response_date")
            loanStatusNode.RemoveAttribute("app_receive_date")
        End If
        If sComment <> "" Then
            sComment = "SYSTEM (" & Now.ToShortDateString & "  " & Now.ToShortTimeString & "): " & sComment
            sComment &= Environment.NewLine
            sComment &= Environment.NewLine
        End If
        Dim decisionCommentsNodes As XmlNodeList = innerXMLDoc.GetElementsByTagName("DECISION_COMMENTS")
        Dim decisionCommentsNode As XmlElement = decisionCommentsNodes(0)
        decisionCommentsNode.InnerText = sComment & decisionCommentsNode.InnerText

        'buil xml clf for posting
        Dim UpdateLoanRequestXmlDoc As XmlDocument = LoanSubmit.BuildUpdateRequestXmlDoc(oConfig, innerXMLDoc)
        _log.Info("Preparing to Update loan data: " & CSecureStringFormatter.MaskSensitiveXMLData(UpdateLoanRequestXmlDoc.InnerXml))

        Dim submitLoanUrl As String = oConfig.BaseSubmitLoanUrl + "/SubmitLoan/SubmitLoan.aspx"
        _log.Info("start request to " & submitLoanUrl)
        ' Now send the loan request back with the updated "status, 
        req = WebRequest.Create(submitLoanUrl)
        req.Method = "POST"
        req.ContentType = "text/xml"
        writer = New StreamWriter(req.GetRequestStream())
        writer.Write(UpdateLoanRequestXmlDoc.InnerXml)
        writer.Close()

        res = req.GetResponse()

        reader = New StreamReader(res.GetResponseStream())
        sXml = reader.ReadToEnd()
        _log.Info("Update loan response: " + sXml)
    End Sub

End Class
