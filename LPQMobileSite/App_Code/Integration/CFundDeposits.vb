﻿Imports Microsoft.VisualBasic
Imports System.Xml
Imports System.Net
Imports LPQMobile.BO
Imports LPQMobile.Utils
Imports System.Text

Public Class CFundDeposits
	Private _log As log4net.ILog = log4net.LogManager.GetLogger(GetType(CFundDeposits))
	Public Sub New()
		
	End Sub

	Public Function Execute(ByVal psLoanID As String, poConfig As CWebsiteConfig, ByVal psUrl As String) As String
		Dim sErrorMessage As String = ""  'SUCCESS = success

		''================================
		'' Build XML request
		Dim sb As New StringBuilder()
		Dim writer As XmlWriter = XmlTextWriter.Create(sb)
		writer.WriteStartDocument()

		' begin input
		writer.WriteStartElement("INPUT")

		writer.WriteStartElement("LOGIN")
		writer.WriteStartElement("API_CREDENTIALS")
		writer.WriteAttributeString("api_user_id", poConfig.APIUser)
		writer.WriteAttributeString("api_password", poConfig.APIPassword)
		writer.WriteEndElement() 'end API_CREDENTIALS
		writer.WriteEndElement() 'end login

		writer.WriteStartElement("REQUEST")
		writer.WriteStartElement("LOAN_ID")
		writer.WriteString(psLoanID)
		writer.WriteEndElement() 'end LOAN_ID
		writer.WriteEndElement() 'end REQUEST

		' end input
		writer.WriteEndElement()
		writer.Flush()
		writer.Close()

		Dim RequestXMLString = sb.ToString()


		''==================
		''Begin post
		Try


			Dim req As WebRequest = WebRequest.Create(psUrl)

			req.Method = "POST"
			req.ContentType = "text/xml"

			_log.Info(String.Format("Preparing to POST FundDeposit Url: {0}", psUrl))
			_log.Info(String.Format("Preparing to POST FundDeposit Data: {0}", CSecureStringFormatter.MaskSensitiveXMLData(RequestXMLString)))

			Dim streamWriter As New System.IO.StreamWriter(req.GetRequestStream())
			streamWriter.Write(RequestXMLString)
			streamWriter.Close()

			Dim res As WebResponse = req.GetResponse()

			Dim reader As New System.IO.StreamReader(res.GetResponseStream())
			Dim sXml As String = reader.ReadToEnd()

			_log.Info("XA: Response from server(FundDeposit): " + sXml)

			reader.Close()
			res.GetResponseStream().Close()
			req.GetRequestStream().Close()

			sErrorMessage = GetErrorMessage(sXml)
		Catch ex As Exception
			_log.Error("XA: FundDeposit ERROR): " & ex.Message, ex)
		End Try

		Return sErrorMessage

	End Function

	Protected Function GetErrorMessage(ByVal psResponseXml As String) As String
		Dim xmlDoc As New XmlDocument()

		xmlDoc.LoadXml(psResponseXml)
		'check the response is nothing
		If (xmlDoc.GetElementsByTagName("RESPONSE").Count = 0) Then
			_log.Error("XA: No response for FundDeposit", Nothing)
			Return "Not response for FundDeposit"
		ElseIf xmlDoc.GetElementsByTagName("RESPONSE").Count > 0 Then
			Dim oNode As XmlElement = CType(xmlDoc.GetElementsByTagName("RESPONSE")(0), XmlElement)
			If Common.SafeString(oNode.GetAttribute("status")) <> "SUCCESS" Then
				Dim sErrMessage = xmlDoc.GetElementsByTagName("ERROR")(0).InnerText	 'error message
				_log.Error(sErrMessage, Nothing)
				Return sErrMessage
			End If
		End If

		Return "SUCCESS" '
	End Function
End Class
