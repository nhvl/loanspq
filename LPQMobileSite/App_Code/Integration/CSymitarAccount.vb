﻿Imports System.Xml
Imports System.Net
Imports LPQMobile.BO
Imports LPQMobile.Utils
Public Class CSymitarAccount

	'Symitar repgen definition
	'00/0060001940/A1/TAMRA/LYNN/WILSON/457594859//8544 SOUTH FM 730/BOYD/TX/76023/11041966/11545119/11042018/002/817-903-1044/817-903~JRGLINE=-1044/BROKER/REALTY TEXAS/1//TAMMYSIMSWILSON@YAHOO.COM//817-903-1044//0.00/TX STATE DRIVERS LICENSE/12202012/000///--------/------~JRGLINE=--/000///--------/--------/12112013
	'c:\LoansPQ2\Website\Lender\App_Code\Integration\Symitar\CSymitarBase\parseRepGenNameRecordResponse

	Private _AccountNumber As String
	Private _NameUSPersonFlag As Char
	Private _FName As String
	Private _MName As String
	Private _LName As String
	Private _Ssn As String
	Private _Dob As String '(MMDDYYYY)    'DOB has to be in yyy-MM-dd format for lpq
	Private _ExtraAddress As String

	Private _Street As String
	Private _City As String
	Private _State As String
	Private _Zip As String
	Private _IdType As String
	Private _IdDescription As String
	Private _IdNumber As String
	Private _IdEpxirationDate As String	' (MMDDYYYY)    'DOB has to be in yyy-MM-dd format for lpq
	Private _IdIssuedDate As String	' (MMDDYYYY) 
	Private _Id2Type As String
	Private _Id2Description As String
	Private _Id2Number As String
	Private _Id2EpxirationDate As String	' (MMDDYYYY)    'DOB has to be in yyy-MM-dd format for lpq
	Private _Id2IssuedDate As String	' (MMDDYYYY)
	Private _Id3Type As String
	Private _Id3Description As String
	Private _Id3Number As String
	Private _Id3EpxirationDate As String	' (MMDDYYYY)    'DOB has to be in yyy-MM-dd format for lpq
	Private _Id3IssuedDate As String	' (MMDDYYYY)
	Private _Employer As String
	Private _Occupation As String
	Private _HomePhone As String
	Private _WorkPhone As String
	Private _MobilePhone As String
    Private _Email As String
    Private _Status As String

    Public Sub New()

	End Sub

    Private _NameType As String '0 for primary
    Public Overridable Property NameType() As String
		Get
			Return _NameType
		End Get
		Set(ByVal value As String)
			_NameType = value
		End Set
	End Property

	Public Overridable Property AccountNumber() As String
		Get
			Return _AccountNumber
		End Get
		Set(ByVal value As String)
			_AccountNumber = value
		End Set
	End Property

	Public Overridable Property NameUSPersonFlag() As Char
		Get
			Return _NameUSPersonFlag
		End Get
		Set(ByVal value As Char)
			_NameUSPersonFlag = value
		End Set
	End Property

	Public Overridable Property FName() As String
		Get
			Return _FName
		End Get
		Set(ByVal value As String)
			_FName = value
		End Set
	End Property

	Public Overridable Property MName() As String
		Get
			Return _MName
		End Get
		Set(ByVal value As String)
			_MName = value
		End Set
	End Property
	Public Overridable Property LName() As String
		Get
			Return _LName
		End Get
		Set(ByVal value As String)
			_LName = value
		End Set
	End Property

	Public Overridable Property Ssn() As String
		Get
			Return _Ssn
		End Get
		Set(ByVal value As String)
			_Ssn = value
		End Set
	End Property

	Public Overridable Property Dob() As String
		Get
			Return _Dob
		End Get
		Set(ByVal value As String)
			_Dob = value
		End Set
	End Property

	Public Overridable Property ExtraAddress() As String
		Get
			Return _ExtraAddress
		End Get
		Set(ByVal value As String)
			_ExtraAddress = value
		End Set
	End Property
	Public Overridable Property Street() As String
		Get
			Return _Street
		End Get
		Set(ByVal value As String)
			_Street = value
		End Set
	End Property
	Public Overridable Property City() As String
		Get
			Return _City
		End Get
		Set(ByVal value As String)
			_City = value
		End Set
	End Property
	Public Overridable Property State() As String
		Get
			Return _State
		End Get
		Set(ByVal value As String)
			_State = value
		End Set
	End Property
	Public Overridable Property Zip() As String
		Get
			Return _Zip
		End Get
		Set(ByVal value As String)
			_Zip = value
		End Set
	End Property
	'---
	Public Overridable Property IdType() As String	'002=driver lices
		Get
			Return _IdType
		End Get
		Set(ByVal value As String)
			_IdType = value
		End Set
	End Property

	Public Overridable Property IdDescription() As String
		Get
			Return _IdDescription
		End Get
		Set(ByVal value As String)
			_IdDescription = value
		End Set
	End Property

	Public Overridable Property IdNumber() As String
		Get
			Return _IdNumber
		End Get
		Set(ByVal value As String)
			_IdNumber = value
		End Set
	End Property
	Public Overridable Property IdEpxirationDate() As String
		Get
			Return _IdEpxirationDate
		End Get
		Set(ByVal value As String)
			_IdEpxirationDate = value
		End Set
	End Property
	Public Overridable Property IdIssuedDate() As String
		Get
			Return _IdIssuedDate
		End Get
		Set(ByVal value As String)
			_IdIssuedDate = value
		End Set
	End Property

	'---

	Public Overridable Property Id2Type() As String	'002=driver lices
		Get
			Return _Id2Type
		End Get
		Set(ByVal value As String)
			_Id2Type = value
		End Set
	End Property

	Public Overridable Property Id2Description() As String
		Get
			Return _Id2Description
		End Get
		Set(ByVal value As String)
			_Id2Description = value
		End Set
	End Property

	Public Overridable Property Id2Number() As String
		Get
			Return _Id2Number
		End Get
		Set(ByVal value As String)
			_Id2Number = value
		End Set
	End Property
	Public Overridable Property Id2EpxirationDate() As String
		Get
			Return _Id2EpxirationDate
		End Get
		Set(ByVal value As String)
			_Id2EpxirationDate = value
		End Set
	End Property
	Public Overridable Property Id2IssuedDate() As String
		Get
			Return _Id2IssuedDate
		End Get
		Set(ByVal value As String)
			_Id2IssuedDate = value
		End Set
	End Property

	'----

	Public Overridable Property Id3Type() As String	'002=driver lices
		Get
			Return _Id3Type
		End Get
		Set(ByVal value As String)
			_Id3Type = value
		End Set
	End Property

	Public Overridable Property Id3Description() As String
		Get
			Return _Id3Description
		End Get
		Set(ByVal value As String)
			_Id3Description = value
		End Set
	End Property

	Public Overridable Property Id3Number() As String
		Get
			Return _Id3Number
		End Get
		Set(ByVal value As String)
			_Id3Number = value
		End Set
	End Property
	Public Overridable Property Id3EpxirationDate() As String
		Get
			Return _Id3EpxirationDate
		End Get
		Set(ByVal value As String)
			_Id3EpxirationDate = value
		End Set
	End Property
	Public Overridable Property Id3IssuedDate() As String
		Get
			Return _Id3IssuedDate
		End Get
		Set(ByVal value As String)
			_Id3IssuedDate = value
		End Set
	End Property

	Public Overridable Property Employer() As String
		Get
			Return _Employer
		End Get
		Set(ByVal value As String)
			_Employer = value
		End Set
	End Property
	
	Public Overridable Property Occupation() As String
		Get
			Return _Occupation
		End Get
		Set(ByVal value As String)
			_Occupation = value
		End Set
	End Property

	Public Overridable Property HomePhone() As String
		Get
			Return _HomePhone
		End Get
		Set(ByVal value As String)
			_HomePhone = value
		End Set
	End Property

	Public Overridable Property WorkPhone() As String
		Get
			Return _WorkPhone
		End Get
		Set(ByVal value As String)
			_WorkPhone = value
		End Set
	End Property

	Public Overridable Property MobilePhone() As String
		Get
			Return _MobilePhone
		End Get
		Set(ByVal value As String)
			_MobilePhone = value
		End Set
	End Property

	Public Overridable Property Email() As String
		Get
			Return _Email
		End Get
		Set(ByVal value As String)
			_Email = value
		End Set
	End Property

	

	Public Overridable Property Status() As String
		Get
			Return _Status
		End Get
		Set(ByVal value As String)
			_Status = value
		End Set
	End Property

    Public Sub parseFields(ByVal arrayFields As String())
        NameType = Common.SafeString(arrayFields(0))
        AccountNumber = Common.SafeString(Common.SafeInteger(arrayFields(1)))
        If arrayFields(2).Length = 2 Then
            NameUSPersonFlag = Right(arrayFields(2), 1)
        End If
        FName = Common.SafeString(arrayFields(3))
        MName = Common.SafeString(arrayFields(4))
        LName = Common.SafeString(arrayFields(5))
        Ssn = Common.SafeString(arrayFields(6))
        ExtraAddress = Common.SafeString(arrayFields(7))
        Street = Common.SafeString(arrayFields(8))
        City = Common.SafeString(arrayFields(9))
        State = Common.SafeString(arrayFields(10))
        Zip = Common.SafeString(arrayFields(11))
        Dob = Common.SafeString(arrayFields(12))

        IdNumber = Common.SafeString(arrayFields(13))
        IdEpxirationDate = Common.SafeString(arrayFields(14))
        IdIssuedDate = Common.SafeString(arrayFields(28))
        IdType = Common.SafeString(arrayFields(15)) 'TODO: need to do the conversion
        IdDescription = Common.SafeString(arrayFields(27))

        Id2Number = Common.SafeString(arrayFields(30))
        Id2EpxirationDate = Common.SafeString(arrayFields(33))
        Id2IssuedDate = Common.SafeString(arrayFields(32))
        Id2Type = Common.SafeString(arrayFields(29)) 'TODO: need to do the conversion
        Id2Description = Common.SafeString(arrayFields(31))

        Id3Number = Common.SafeString(arrayFields(35))
        Id3EpxirationDate = Common.SafeString(arrayFields(38))
        Id3IssuedDate = Common.SafeString(arrayFields(37))
        Id3Type = Common.SafeString(arrayFields(34)) 'TODO: need to do the conversion
        Id3Description = Common.SafeString(arrayFields(36))

        HomePhone = Common.SafeString(arrayFields(16))
        WorkPhone = Common.SafeString(arrayFields(17))
        Occupation = Common.SafeString(arrayFields(18))
        Employer = Common.SafeString(arrayFields(19))

        Email = Common.SafeString(arrayFields(22))
        MobilePhone = Common.SafeString(arrayFields(24))
    End Sub
End Class


