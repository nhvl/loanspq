﻿Imports System.Text
Imports Microsoft.VisualBasic
Imports System.Collections
Imports System.IO
Imports System.Xml
Imports System.Net
Imports LPQMobile.BO
Imports LPQMobile.Utils
Public Class CSymitarLookup
    Private Shared _IsTimeOut As Boolean = False
    Private Shared _log As log4net.ILog = log4net.LogManager.GetLogger(GetType(CSymitarLookup))
    ''using CIS service to get Customer infor
    Private Shared Function BuildSymitarRequestXML(ByVal oConfig As CWebsiteConfig, ByVal sLookupNumber As String) As String
        Dim sb As New StringBuilder()
        Dim writer As XmlWriter = XmlTextWriter.Create(sb)
        writer.WriteStartDocument()
        writer.WriteStartElement("INPUT")
        writer.WriteStartElement("LOGIN")
        writer.WriteStartElement("API_CREDENTIALS")
        writer.WriteAttributeString("api_user_id", oConfig.APIUser)
        writer.WriteAttributeString("api_password", oConfig.APIPassword)
        writer.WriteEndElement() 'end API_CREDENTIALS 
        writer.WriteEndElement() 'end login
        writer.WriteStartElement("REQUEST")
        writer.WriteAttributeString("integration_type", "SYMITAR")
        writer.WriteAttributeString("lender_code", oConfig.LenderCode)
        writer.WriteStartElement("SEARCHTYPE")
        writer.WriteString("MEMBERNUMBER")
        writer.WriteEndElement() 'end SearchType
        writer.WriteStartElement("SEARCHVALUE")
        writer.WriteString(sLookupNumber) 'MemberNumber
        writer.WriteEndElement() 'end SEARCHVALUE
        writer.WriteEndElement() 'end REQUEST
        writer.WriteEndElement() ' end input
        writer.Flush()
        writer.Close()
        Return sb.ToString()
    End Function
    Public Shared Function getSymitarResponse(ByVal oConfig As CWebsiteConfig, ByVal sLookupNumber As String, ByVal sUrl As String) As String
        Dim sXml As String = ""
        Dim sRequestXML = BuildSymitarRequestXML(oConfig, sLookupNumber)
        ''Begin post
        Try
            Dim req As WebRequest = WebRequest.Create(sUrl)
            req.Method = "POST"
            req.ContentType = "text/xml"
            _log.InfoFormat("Preparing to POST Data To: {0}", sUrl)
            _log.InfoFormat("Preparing to POST Searching Data: {0}", CSecureStringFormatter.MaskSensitiveXMLData(sRequestXML))
            Dim streamWriter As New System.IO.StreamWriter(req.GetRequestStream())
            streamWriter.Write(sRequestXML)
            streamWriter.Close()
            Dim res As WebResponse = req.GetResponse()
            Dim reader As New System.IO.StreamReader(res.GetResponseStream())
            sXml = reader.ReadToEnd()
            _log.DebugFormat("Searching response from Core: {0}", sXml)
            reader.Close()
            res.GetResponseStream().Close()
            req.GetRequestStream().Close()
        Catch ex As Exception
            _log.Error("Searching error: " & ex.Message, ex)
        End Try
        Return sXml
    End Function
    ''comment out unused legacy code for symitar lookup
    ''Private Shared Function CallWebRequest(ByVal xmlRequest As String, ByVal sUri As String) As String
    ''    CPBLogger.logInfo("Posting to Symitar: " & xmlRequest)
    ''    CPBLogger.logInfo("Posting to Symitar URl: " & sUri)
    ''    Dim oReq As WebRequest = Nothing
    ''    Dim oRsp As WebResponse = Nothing
    ''    Dim sResult As String = String.Empty
    ''    Try
    ''        ' create a web request
    ''        oReq = WebRequest.Create(sUri)
    ''        oReq.Method = "POST"
    ''        oReq.ContentType = "text/xml"
    ''        ' prepqre xml for request
    ''        Dim oRequestStream As Stream = oReq.GetRequestStream()
    ''        Dim oWriter As New StreamWriter(oRequestStream)
    ''        oWriter.WriteLine(xmlRequest)
    ''        oWriter.Close()
    ''        ' send the data to the webserver
    ''        oRsp = oReq.GetResponse()
    ''        ' read result
    ''        Dim oDataStream As Stream = oRsp.GetResponseStream()
    ''        Dim oReader As New StreamReader(oDataStream)
    ''        sResult = oReader.ReadToEnd()
    ''        If oRequestStream IsNot Nothing Then
    ''            oRequestStream.Close()
    ''        End If
    ''        If oDataStream IsNot Nothing Then
    ''            oDataStream.Close()
    ''        End If
    ''    Catch ex As Exception
    ''        _IsTimeOut = True
    ''        _log.Error("Can not call a web request with error: " & ex.Message, ex)
    ''    End Try
    ''    _log.Info("Result: " & sResult)
    ''    Return sResult
    ''End Function
    ''Private Shared Function GenerateRequestSymitarXml(ByVal sUserId As String, ByVal sPassword As String, ByVal sMemberNumber As String) As String
    ''    ''using userId and password lender level for symitar
    ''    Dim xmlDoc As New XmlDocument
    ''    'Create root Element <INPUT>
    ''    Dim rootNode As XmlElement = xmlDoc.CreateElement("INPUT")
    ''    xmlDoc.AppendChild(rootNode)
    ''    'Create child Element <LOGIN>
    ''    Dim loginNode As XmlElement = xmlDoc.CreateElement("LOGIN")
    ''    loginNode.SetAttribute("api_user_id", sUserId)
    ''    loginNode.SetAttribute("api_password", sPassword)
    ''    rootNode.AppendChild(loginNode)
    ''    'Create child Element <REQUEST>
    ''    Dim requestNode As XmlElement = xmlDoc.CreateElement("REQUEST")
    ''    requestNode.SetAttribute("member_number", sMemberNumber)
    ''    requestNode.SetAttribute("message_type", "RG")
    ''    requestNode.SetAttribute("repgen_name", "SYC.ACCTNAMES.MERIDIAN")
    ''    Dim sNote As String = "~JRGSESSION=33"
    ''    requestNode.InnerText = sNote
    ''    rootNode.AppendChild(requestNode)
    ''    Return xmlDoc.InnerXml()
    ''End Function
    ''Public Shared Function GetSymitarAccount(ByVal sLogin As String, ByVal sPass As String, ByVal piMemberNumber As String, ByVal sURL As String) As List(Of CSymitarAccount)
    ''    _log.Info("start generate request symintar account.")
    ''    Dim oSymitarAccounts As New List(Of CSymitarAccount)
    ''    ' init xml request
    ''    Dim xmlRequest As String = GenerateRequestSymitarXml(sLogin, sPass, piMemberNumber)
    ''    ' call web service
    ''    Dim xmlResult As String = CallWebRequest(xmlRequest, sURL)
    ''    If _IsTimeOut Then
    ''        Return oSymitarAccounts ' see log for error   name=""
    ''    End If
    ''    'symitar is busy
    ''    '<OUTPUT version="1.0"><RESPONSE status="SUCCESSFUL">RSRG~11111~K1004:SPECFILE EXECUTED WHILE IN MEMO POSTING MODE~JRGDATATYPE=9~JRGUSERCHR2=000000001</RESPONSE></OUTPUT>
    ''    'request
    ''    'RG~11111~A0~BMERIDIAN~DCARD~F1051631840000000114~GSYC.SSNNAMES.MERIDIAN~JRGSESSION=33~JRGUSERCHR2=109468100                                                   
    ''    'response with joint (member number =2618)
    ''    '' xmlResult = "<OUTPUT version=""1.0""><RESPONSE status=""SUCCESSFUL"">RSRG~11111~K0~JRGLINE=|00/0000002618/A0/RAUL//AZPIAZU/109468100//922 LIME HILL RD/WYALUSING/PA/18853/08021953//--------/000/570-746-1796/570-265-2100//C~JRGLINE=ENTURY 21 JACKSON REAL ESTATE/1//R18853A@EPIX.NET//570-540-5876//0.00//--------/002/16149229/DRIVERS LICENSE NUMBER/--------/08022~JRGLINE=013/000///--------/--------/09022009~JRGDATATYPE=9~JRGUSERCHR2=109468100</RESPONSE></OUTPUT>"
    ''    ''Result: <OUTPUT version="1.0"><RESPONSE status="SUCCESSFUL">RSRG~11111~K0~JRGLINE=|0000/0022265104/A1/ROBERTO//VILLALPANDO/636845060//5421 VOLDER DR/FORT WORTH/TX/76114-4523/07141977/03434741/07142021/002/817-820~JRGLINE=-9677/817-614-8398/LEAD/MORGAN CONCRETE/1//////0.00/TX STATE DRIVERS LICENSE/12292016/000///--------/--------/000///--------/-----~JRGLINE=---/08292018|2000/0022265104/L1/ROCIO//VILLALPANDO/636265486//5421 VOLDER DR/FORT WORTH/TX/76114-4523/06041979//--------/000/817-4~JRGLINE=20-2008/817-392-6392/ADJUSTER/YORK RISK SERVICES/8//ROCIORUIZ34@YAHOO.COM////0.00//--------/000///--------/--------/000///--------/~JRGLINE=--------/08292018~JRGDATATYPE=9</RESPONSE></OUTPUT>

    ''    If Not String.IsNullOrEmpty(xmlResult) Then
    ''        Try
    ''            Dim xmlDoc As New XmlDocument()
    ''            xmlDoc.LoadXml(xmlResult)
    ''            Dim xMainNode As XmlNode = xmlDoc.DocumentElement.SelectSingleNode("//RESPONSE")
    ''            If xMainNode IsNot Nothing Then
    ''                If Not Common.SafeString(xMainNode.Attributes("status").Value) = "SUCCESSFUL" Then
    ''                    ''oSymitarAccount.Status = "UN-SUCCESSFUL"    'TODO: is this not found or connection error
    ''                    _log.Warn("UN-SUCCESSFUL: not found or connection error")
    ''                    Return oSymitarAccounts
    ''                End If
    ''                'Don't need to do anything.
    ''                'logged in CallWebRequest
    ''                Dim sSymitarMemberinfo = xMainNode.InnerText
    ''                If sSymitarMemberinfo.Contains("MEMO POSTING MODE") Then
    ''                    _log.Warn("MEMO POSTING MODE: Busy")
    ''                    Return oSymitarAccounts
    ''                End If
    ''                '' get records for primary and joint if the records existing joint
    ''                Dim records As String() = sSymitarMemberinfo.Split("|"c)
    ''                If records.Length > 1 Then
    ''                    For nIndex As Integer = 0 To records.Length - 1
    ''                        If nIndex = 0 Then Continue For '' skip records(0) = RSRG~11111~K0~JRGLINE=
    ''                        Dim oAccount As New CSymitarAccount
    ''                        Dim record = records(nIndex).Replace("~JRGLINE=", "")  'rmeove line breaker
    ''                        Dim arrayFields As String() = record.Split("/"c)
    ''                        Dim NameType = Common.SafeString(arrayFields(0))
    ''                        ''
    ''                        If (NameType = "0000" Or NameType = "0100") Then '' "0000" for primary, "0100" for spouse,  "2000" for previous spouse
    ''                            oAccount.parseFields(arrayFields) ''parse data for each applicant
    ''                            oSymitarAccounts.Add(oAccount)
    ''                        End If
    ''                    Next
    ''                End If
    ''            End If
    ''        Catch ex As Exception
    ''            _log.Error("An error occur when parse xml GET result: " & ex.Message, ex)
    ''        End Try
    ''    End If
    ''    Return oSymitarAccounts
    ''End Function
End Class
