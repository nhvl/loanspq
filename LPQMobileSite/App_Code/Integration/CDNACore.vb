﻿Imports Microsoft.VisualBasic
Imports System.Xml
Imports System.Net
Imports LPQMobile.BO
Imports LPQMobile.Utils
Imports System.Text

Public Class CDNACore
	Private _log As log4net.ILog = log4net.LogManager.GetLogger(GetType(CDNACore))
	Public Sub New()

	End Sub

	Public Function Book(ByVal psLoanID As String, poConfig As CWebsiteConfig, ByVal psUrl As String) As String
		Dim sAccountNumber As String = ""

		''================================
		'' Build XML request
		Dim sb As New StringBuilder()
		Dim writer As XmlWriter = XmlTextWriter.Create(sb)
		writer.WriteStartDocument()

		' begin input
		writer.WriteStartElement("INPUT")

		writer.WriteStartElement("LOGIN")
		writer.WriteStartElement("API_CREDENTIALS")
		writer.WriteAttributeString("api_user_id", poConfig.APIUser)
		writer.WriteAttributeString("api_password", poConfig.APIPassword)
		writer.WriteEndElement() 'end API_CREDENTIALS 
		writer.WriteEndElement() 'end login

		writer.WriteStartElement("REQUEST")
		writer.WriteAttributeString("integration_type", "CMC")
		writer.WriteStartElement("BOOK_CMC_XA_APP")

		writer.WriteStartElement("APPLICATION")
		writer.WriteStartElement("EXISTING_APP")
		writer.WriteAttributeString("app_id", psLoanID)
		writer.WriteAttributeString("app_type", "XA")
		writer.WriteEndElement() 'end EXISTING_APP
		writer.WriteEndElement() 'end application

		writer.WriteEndElement() 'end BOOK
		writer.WriteEndElement() 'end REQUEST

		writer.WriteEndElement() ' end input
		writer.Flush()
		writer.Close()

		Dim RequestXMLString = sb.ToString()


		''==================
		''Begin post
		Try


			Dim req As WebRequest = WebRequest.Create(psUrl)

			req.Method = "POST"
			req.ContentType = "text/xml"

			_log.Info(String.Format("Preparing to POST DNA booking Url: {0}", psUrl))
			_log.Info(String.Format("Preparing to POST DNA booking Data: {0}", CSecureStringFormatter.MaskSensitiveXMLData(RequestXMLString)))

			Dim streamWriter As New System.IO.StreamWriter(req.GetRequestStream())
			streamWriter.Write(RequestXMLString)
			streamWriter.Close()

			Dim res As WebResponse = req.GetResponse()

			Dim reader As New System.IO.StreamReader(res.GetResponseStream())
			Dim sXml As String = reader.ReadToEnd()

			_log.Debug("Booking response from core (DNA): " + sXml)

			reader.Close()
			res.GetResponseStream().Close()
			req.GetRequestStream().Close()

			sAccountNumber = GetAccountNumber(sXml)
		Catch ex As Exception
			_log.Error("Booking error: " & ex.Message, ex)
		End Try

		Return sAccountNumber

	End Function

	Protected Function GetAccountNumber(ByVal psResponseXml As String) As String
		Dim xmlDoc As New XmlDocument()

		xmlDoc.LoadXml(psResponseXml)
		If (xmlDoc.GetElementsByTagName("RESPONSE").Count > 0 AndAlso xmlDoc.GetElementsByTagName("RESPONSE")(0).Attributes("status").InnerXml.NullSafeToUpper_ <> "SUCCESS") Then
			_log.Warn("Booking error: status is not success", Nothing)
			Return ""
		End If

        If xmlDoc.GetElementsByTagName("MemberNumber").Count > 0 AndAlso xmlDoc.GetElementsByTagName("MemberNumber")(0).InnerText.Length > 0 Then
            Return xmlDoc.GetElementsByTagName("MemberNumber")(0).InnerText  'pass
        ElseIf (xmlDoc.GetElementsByTagName("RESPONSE").Count > 0 AndAlso xmlDoc.GetElementsByTagName("RESPONSE")(0).Attributes("status").InnerXml.NullSafeToUpper_ = "SUCCESS") Then
            Return "N/A"  'certain FI's core doesn't return memberNumber. We just need a string here so it will not trigger error upstream
        Else
            _log.Warn("Booking error: no member number", Nothing)
            Return "" 'false
        End If

	End Function
End Class
