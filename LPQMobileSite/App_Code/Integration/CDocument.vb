﻿Imports Microsoft.VisualBasic
Imports System.Xml
Imports System.Net
Imports LPQMobile.BO
Imports LPQMobile.Utils
Imports System.Text
Public Class CDocument
    Protected Const constGeneratePDFURL As String = "/Docs/GeneratePDFs/GeneratePDFs.aspx"
    Public Shared Function BuildGeneratePDFRequestXML(ByVal poConfig As CWebsiteConfig, ByVal sLoanNumber As String, ByVal sLoanType As String, ByVal oPDFCodes As String(), ByVal sSaveToSection As String) As String
        Dim sb As New StringBuilder()
        ''prevent overriding the default encoding attribute
        Dim settings = New XmlWriterSettings With {
              .OmitXmlDeclaration = True
              }
        Dim writer As XmlWriter = XmlTextWriter.Create(sb, settings)
        writer.WriteStartDocument()
        writer.WriteStartElement("INPUT")
        writer.WriteStartElement("LOGIN")
        writer.WriteStartElement("API_CREDENTIALS")
        writer.WriteAttributeString("api_user_id", poConfig.APIUserLender)
        writer.WriteAttributeString("api_password", poConfig.APIPasswordLender)
        writer.WriteEndElement() 'end API_CREDENTIALS 
        writer.WriteEndElement() 'end login
        writer.WriteStartElement("REQUEST")
        writer.WriteAttributeString("save_to", sSaveToSection)
        writer.WriteAttributeString("return_pdf", "false")
        writer.WriteStartElement("APP_INFO")
        writer.WriteStartElement("APP_NUMBER")
        writer.WriteAttributeString("app_type", sLoanType)
        writer.WriteString(sLoanNumber)
        writer.WriteEndElement() 'end APP_NUMBER
        writer.WriteEndElement() 'end APP_INFO
        For Each pdfCode In oPDFCodes
            writer.WriteStartElement("DOCUMENT")
            writer.WriteAttributeString("pdf_code", pdfCode)
            writer.WriteEndElement() ''end document
        Next
        writer.WriteEndElement() 'end REQUEST
        writer.WriteEndElement() ' end input
        writer.Flush()
        writer.Close()
        Return sb.ToString()
    End Function
    Public Shared Function GeneratePDFs(poConfig As CWebsiteConfig, ByVal psLoanNumber As String, ByVal psAppType As String, ByVal sSaveToSection As String, ByVal sAutoCreatePDFCode As String) As String
        Dim PDFCodeList = sAutoCreatePDFCode.Split(",")
        If PDFCodeList.Count = 1 AndAlso PDFCodeList(0) = "" Then Return "" ''no pdf code
        Dim RequestXMLString = BuildGeneratePDFRequestXML(poConfig, psLoanNumber, psAppType, PDFCodeList, sSaveToSection)
        Dim sURL As String = poConfig.BaseSubmitLoanUrl + constGeneratePDFURL
        Dim requestXmLDocs As New XmlDocument
        requestXmLDocs.LoadXml(RequestXMLString)
        Return Common.WebPost(requestXmLDocs, sURL)
    End Function
End Class
