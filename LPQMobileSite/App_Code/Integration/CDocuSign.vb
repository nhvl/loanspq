﻿Imports Microsoft.VisualBasic
Imports System.Xml
Imports System.Net
Imports LPQMobile.BO
Imports LPQMobile.Utils
'Imports LPQMobileSite.UserControls
Imports System.Web
Imports System.Text

''' <summary>
''' 
''' </summary>
''' <remarks></remarks>
Public Class CDocuSign

	Private Shared _log As log4net.ILog = log4net.LogManager.GetLogger(GetType(CDocuSign))
    'Private 

    Protected Const constDocuSignURL As String = "/DocuSign/GenerateDocuSign/GenerateDocuSign.aspx"
    Public Shared Function GetDocuSign(poConfig As CWebsiteConfig, ByVal psLoanNumber As String, ByVal psAppType As String, httpRequest As HttpRequest, Optional ByVal productList As List(Of CProduct) = Nothing, Optional ByVal bDocusignWithIda As Boolean = False) As String
        ''===============
        ''Expected Input Sample
        '		<INPUT>
        '  <LOGIN>
        '    <API_CREDENTIALS api_user_id="ded48872-6b65-4085-a41d-4261b2457019" api_password="Og2TuvTuv20,nUZE,XC,#HPEuCpCHvLt5MNmP" />
        '  </LOGIN>
        '  <REQUEST>
        '    <APP_INFO>
        '      <APP_NUMBER app_type="VL">3535</APP_NUMBER>
        '    </APP_INFO>
        '    <CLIENT_URLS sign_complete_url="https://demo.loanspq.com/" access_code_failed_url="https://beta.loanspq.com/" cancel_url="https://beta.loanspq.com/" 
        '		decline_url="https://beta.loanspq.com/" exception_url="https://beta.loanspq.com/" fax_pending_url="https://beta.loanspq.com/" id_check_failed_url="https://beta.loanspq.com/" timeout_url="https://beta.loanspq.com/" 
        '	expired_url="https://beta.loanspq.com/" view_complete_url="https://beta.loanspq.com/"></CLIENT_URLS>
        '    <DOCUMENT pdf_group="AUTO_CONSUMER_DOCUSIGN"></DOCUMENT>
        '  </REQUEST>
        '</INPUT>

        Dim port As String
        If (httpRequest.Url.Port = 80) Then
            port = ""
        Else
            port = ":" + httpRequest.Url.Port.ToString()
        End If

        Dim sPDF_GROUP As String = Common.getNodeAttributes(poConfig, Common.GetConfigLoanTypeFromShort(psAppType) & "/VISIBLE", "in_session_docu_sign_pdf_group_override")
        If sPDF_GROUP = "" Then
            sPDF_GROUP = "AUTO_CONSUMER_DOCUSIGN"
        End If

        Dim serverRoot As String = httpRequest.Url.Scheme + "://" + httpRequest.Url.Host + port
        Dim sPDocusignWithIdaPara = ""
        If bDocusignWithIda Then
            sPDocusignWithIdaPara = "&DocusignWithIda=Y"
        End If

        Dim sResult As String = ""
        ''================================
        '' Build XML request
        Dim sb As New StringBuilder()
        Dim writer As XmlWriter = XmlTextWriter.Create(sb)
        writer.WriteStartDocument()

        ' begin input
        writer.WriteStartElement("INPUT")
        writer.WriteStartElement("LOGIN")
        writer.WriteStartElement("API_CREDENTIALS")
        writer.WriteAttributeString("api_user_id", poConfig.APIUser)
        writer.WriteAttributeString("api_password", poConfig.APIPassword)
        writer.WriteEndElement() 'end API_CREDENTIALS
        writer.WriteEndElement() 'end login

        writer.WriteStartElement("REQUEST")
        writer.WriteStartElement("APP_INFO")
        writer.WriteStartElement("APP_NUMBER")
        writer.WriteAttributeString("app_type", psAppType)
        writer.WriteString(psLoanNumber)
        writer.WriteEndElement() 'end APP_NUMBER
        writer.WriteEndElement() 'end APP_INFO

        writer.WriteStartElement("CLIENT_URLS")
        writer.WriteAttributeString("sign_complete_url", serverRoot & "/handler/docusigntracker.aspx?status=sign_complete&url=https://apptest.loanspq.com/apply.aspx?lenderref=lpquniversity_test" & sPDocusignWithIdaPara)
        writer.WriteAttributeString("access_code_failed_url", serverRoot & "/handler/docusigntracker.aspx?status=access_code_failed&url=https://apptest.loanspq.com/apply.aspx?lenderref=lpquniversity_test" & sPDocusignWithIdaPara)
        writer.WriteAttributeString("cancel_url", serverRoot & "/handler/docusigntracker.aspx?status=cancel&url=https://apptest.loanspq.com/apply.aspx?lenderref=lpquniversity_test" & sPDocusignWithIdaPara)
        writer.WriteAttributeString("decline_url", serverRoot & "/handler/docusigntracker.aspx?status=decline&url=https://apptest.loanspq.com/apply.aspx?lenderref=lpquniversity_test" & sPDocusignWithIdaPara)
        writer.WriteAttributeString("exception_url", serverRoot & "/handler/docusigntracker.aspx?status=exception&url=https://apptest.loanspq.com/apply.aspx?lenderref=lpquniversity_test" & sPDocusignWithIdaPara)
        writer.WriteAttributeString("fax_pending_url", serverRoot & "/handler/docusigntracker.aspx?status=fax_pending&url=https://apptest.loanspq.com/apply.aspx?lenderref=lpquniversity_test" & sPDocusignWithIdaPara)
        writer.WriteAttributeString("id_check_failed_url", serverRoot & "/handler/docusigntracker.aspx?status=id_check_failed&url=https://apptest.loanspq.com/apply.aspx?lenderref=lpquniversity_test" & sPDocusignWithIdaPara)
        writer.WriteAttributeString("timeout_url", serverRoot & "/handler/docusigntracker.aspx?status=timeout&url=https://apptest.loanspq.com/apply.aspx?lenderref=lpquniversity_test" & sPDocusignWithIdaPara)
        writer.WriteAttributeString("expired_url", serverRoot & "/handler/docusigntracker.aspx?status=expired&url=https://apptest.loanspq.com/apply.aspx?lenderref=lpquniversity_test" & sPDocusignWithIdaPara)
        writer.WriteAttributeString("view_complete_url", serverRoot & "/handler/docusigntracker.aspx?status=view_complete&url=https://apptest.loanspq.com/apply.aspx?lenderref=lpquniversity_test" & sPDocusignWithIdaPara)
        writer.WriteEndElement() 'end CLIENT_URLS
        'TODO: remove this by end of 2019
        Dim IsLegacyMode As Boolean = Common.getNodeAttributes(poConfig, "BEHAVIOR", "legacy_docusign_mode") = "Y"

        If psAppType = "XA" Then ''for xa
            If productList IsNot Nothing Then
                writer.WriteStartElement("DOCUMENT")

                writer.WriteAttributeString("pdf_group", sPDF_GROUP)
                'TODO: remove "IsLegacyMode" by end of 2019
                'TODO:temporary default back to legacy mode unitl we fix the issue with the API
                If Not IsLegacyMode Then
                    writer.WriteAttributeString("use_filtering", "Y")
                    writer.WriteAttributeString("skip_if_missing", "Y")
                End If

                writer.WriteEndElement() 'end DOCUMENT
                ''get pdfs account level by title and target_account_code(no pdf_group)
                'Dim pdfList = getPDFTitleList(poConfig)
                For Each oProduct In productList
                    ''If hasPDF(pdfList, oProduct.ProductCode) Then
                    ''    Dim selectedPDF = selectedPDFTitles(pdfList, oProduct.ProductCode)
                    ''    For i = 0 To selectedPDF.Count - 1
                    ''        writer.WriteStartElement("DOCUMENT")
                    ''        'writer.WriteAttributeString("pdf_group", sPDF_GROUP)
                    ''        writer.WriteAttributeString("target_account_code", oProduct.ProductCode)
                    ''        writer.WriteAttributeString("title", selectedPDF(i))
                    ''        writer.WriteEndElement() 'end DOCUMENT
                    ''    Next
                    ''End If
                    Dim oPDFAssociations As List(Of String) = oProduct.pdfAssociations
                    If oPDFAssociations.Count > 0 Then
                        For i = 0 To oPDFAssociations.Count - 1
                            writer.WriteStartElement("DOCUMENT")
                            writer.WriteAttributeString("target_account_code", oProduct.ProductCode)
                            writer.WriteAttributeString("title", oPDFAssociations(i))
                            'TODO:temporary default back to legacy mode unitl we fix the issue with the API
                            If Not IsLegacyMode Then
                                writer.WriteAttributeString("use_filtering", "Y")
                                writer.WriteAttributeString("skip_if_missing", "Y")
                            End If
                            writer.WriteEndElement() 'end DOCUMENT
                        Next
                    End If
                Next
            End If
        Else 'for loans
            writer.WriteStartElement("DOCUMENT")
            writer.WriteAttributeString("pdf_group", sPDF_GROUP)
            'TODO: remove IsLegacyMode by end of 2019
            'TODO:uncomment below once the new docusign api is fixed
            If Not IsLegacyMode Then
                writer.WriteAttributeString("use_filtering", "Y")
                writer.WriteAttributeString("skip_if_missing", "Y")
            End If
            writer.WriteEndElement() 'end DOCUMENT
            'using docusign to run ida
            If bDocusignWithIda Then
                writer.WriteStartElement("ENVELOPE_SECURITY")
                writer.WriteAttributeString("use_id_check_for_all", "Y")
                writer.WriteEndElement() 'end Envelope security
            End If
        End If

        writer.WriteEndElement() 'end REQUEST
        writer.WriteEndElement() ' end input
        writer.Flush()
        writer.Close()

        Dim RequestXMLString = sb.ToString()

        ''==================
        ''Begin post
        Try
            Dim baseSubmitURL = poConfig.BaseSubmitLoanUrl
            Dim docuSignURL As String = baseSubmitURL + constDocuSignURL

			Dim req As WebRequest = WebRequest.Create(docuSignURL)
			req.Timeout = 300000 ' AP-1126: Increase Timeout for in-session DocuSign from 1.6 min to 5 min

			req.Method = "POST"
            req.ContentType = "text/xml"

            _log.InfoFormat("Preparing to POST to docuSign Url: {0}", docuSignURL)
            _log.InfoFormat("Preparing to POST docuSign Data: {0}", CSecureStringFormatter.MaskSensitiveXMLData(RequestXMLString))

            Dim streamWriter As New System.IO.StreamWriter(req.GetRequestStream())
            streamWriter.Write(RequestXMLString)
            streamWriter.Close()

            Dim res As WebResponse = req.GetResponse()

            Dim reader As New System.IO.StreamReader(res.GetResponseStream())
            Dim sXml As String = reader.ReadToEnd()
            _log.DebugFormat("Response from server : {0}", sXml)
            ''===Expected output sampel
            'sXml will have response similar to this
            '<OUTPUT version="1.0"><RESPONSE><URL>https://demo.docusign.net/Signing/startinsession.aspx?t=43d52983-bdda-41b3-a95d-97d358d975ea</URL></RESPONSE></OUTPUT>
            'or
            '<OUTPUT version="1.0"><ERROR type="INTERNAL_ERROR">Unable to find PDFs for PDF Group: AUTO_CONSUMER_DOCUSIGN2</ERROR></OUTPUT>
            Dim xmlDoc As New XmlDocument()
            xmlDoc.LoadXml(sXml)
            Dim urlNode As XmlNode = xmlDoc.SelectSingleNode("/OUTPUT/RESPONSE/URL")
            Dim errorNode As XmlNode = xmlDoc.SelectSingleNode("/OUTPUT/ERROR")
            If urlNode IsNot Nothing Then
                sResult = urlNode.InnerText
            ElseIf errorNode IsNot Nothing AndAlso errorNode.InnerText.Contains("no PDFs to process") Then
                _log.Info("Inbranch response: There are no PDFs to process")
            Else
                _log.Warn("Bad Docusign Response")
            End If
            reader.Close()
            res.GetResponseStream().Close()
            req.GetRequestStream().Close()

        Catch ex As Exception
            _log.Error("DocuSign error: " & ex.Message, ex)
        End Try

        Return sResult

    End Function

    'Private Shared Function getPDFTitleList(ByVal poConfig As CWebsiteConfig) As Dictionary(Of String, IList(Of String))
    '    Dim PdfDic As New Dictionary(Of String, IList(Of String))
    '    ''testing product
    '    Dim xmlDoc As XmlDocument = CDownloadedSettings.GetXAProducts(poConfig)
    '    Dim productCodeNodes As XmlNodeList = xmlDoc.GetElementsByTagName("PRODUCT")
    '    If productCodeNodes.Count = 0 Then
    '        Return PdfDic
    '    End If
    '    For Each oProduct As XmlElement In productCodeNodes
    '        Dim productCode = Common.SafeString(oProduct.GetElementsByTagName("PRODUCT_CODE")(0).InnerText)
    '        If productCode <> "" Then
    '            Dim titleList As New List(Of String)
    '            Dim pdfAssociation As XmlNodeList = oProduct.GetElementsByTagName("PDF_ASSOCIATION")
    '            If pdfAssociation.Count > 0 Then
    '                For Each oPdf As XmlElement In pdfAssociation
    '                    titleList.Add(Common.SafeString(oPdf.InnerText))
    '                Next
    '            End If
    '            If Not PdfDic.ContainsKey(productCode) Then
    '                If titleList.Count > 0 Then
    '                    PdfDic.Add(productCode, titleList)
    '                End If
    '            End If
    '        End If
    '    Next
    '    Return PdfDic
    'End Function

    'Private Shared Function hasPDF(ByVal oPDFDic As Dictionary(Of String, IList(Of String)), ByVal sProductCode As String) As Boolean
    '    For Each oItem As KeyValuePair(Of String, IList(Of String)) In oPDFDic
    '        If oItem.Key = sProductCode And oItem.Value.Count > 0 Then
    '            Return True
    '        End If
    '    Next
    '    Return False
    'End Function
    'Private Shared Function selectedPDFTitles(ByVal sPDFDic As Dictionary(Of String, IList(Of String)), ByVal sProductCode As String) As List(Of String)
    '    For Each sPDF As KeyValuePair(Of String, IList(Of String)) In sPDFDic
    '        If sPDF.Key = sProductCode Then
    '            Return sPDF.Value
    '        End If
    '    Next
    '    Return New List(Of String)
    'End Function
End Class
