﻿Imports Microsoft.VisualBasic
Imports PayPal.Api
Imports System.Linq
Imports System.Web

''Deprecated, replace with paypal adaptive
Public Class CPayPal

    Private ReadOnly _log As log4net.ILog = log4net.LogManager.GetLogger(Me.GetType())
    Private ReadOnly _apiContext As APIContext
    Private ReadOnly _httpContext As HttpContext

    Public Sub New(httpContext As HttpContext)
        _apiContext = CPayPalConfig.GetAPIContext()
        _httpContext = httpContext
    End Sub

    ''' <summary>
    ''' Create a PayPal payment, return PayPal login page for buyer to login and process payment
    ''' </summary>
    ''' <param name="items"></param>
    ''' <param name="callbackURL">call back url after buyer approved or canceled payment</param>
    ''' <returns>PayPal Login URL</returns>
    ''' <remarks></remarks>
    Public Function CreatePayment(items As List(Of Item), callbackURL As String) As String

        Dim itemList As New ItemList()
        itemList.items = items
        Dim totalAmount As Decimal = items.Select(Function(x) Decimal.Parse(x.price)).Sum()
        Dim amount As New Amount() With {.currency = "USD", .total = totalAmount.ToString()}
        Dim payer As New Payer() With {.payment_method = "paypal"}

        '' generate a guid
        Dim paymentKey = Guid.NewGuid().ToString()
		Dim sInvoiceNumber = Guid.NewGuid().ToString()

        '' build redirect URLs
        '' check url has ? yet (first param)
        If callbackURL.IndexOf("?") < 0 Then
            callbackURL = callbackURL + "?guid=" + paymentKey
        Else '' url already had first param
            callbackURL = callbackURL + "&guid=" + paymentKey
        End If
        Dim redirUrls As New RedirectUrls() With {.return_url = callbackURL, .cancel_url = callbackURL + "&cancel=true"}

        Dim transList As New List(Of Transaction)
		transList.Add(New Transaction() With {.description = "Transaction description.", .invoice_number = sInvoiceNumber, .amount = amount, .item_list = itemList})

        Dim payment As New Payment() With {.intent = "sale", .payer = payer, .transactions = transList, .redirect_urls = redirUrls}
        Dim createdPayment = payment.Create(_apiContext)

        '' store payment id in session
        _httpContext.Session.Add(paymentKey, createdPayment.id)

        Dim ppLink = createdPayment.links.Single(Function(x) x.rel.ToLower().Trim() = "approval_url")

        Return ppLink.href

    End Function

    ''' <summary>
    ''' Ask PayPal to complete the payment (transfer buyer's $ into seller account)
    ''' </summary>
    ''' <param name="paymentKey"></param>
    ''' <param name="payerID"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function ExecutePayment(paymentKey As String, payerID As String) As Boolean
        '' get back payment id from session
        Dim paymentID As String = CType(_httpContext.Session(paymentKey), String)

        If String.IsNullOrEmpty(paymentID) Then
            Throw New Exception("Could not found PaymentID in Session with key = " + paymentKey)
        End If

        Dim paymentExecution As New PaymentExecution() With {.payer_id = payerID}

        Dim payment As New Payment() With {.id = paymentID}
        Dim executedPayment = payment.Execute(_apiContext, paymentExecution)

        Return executedPayment.state = "approved"

    End Function

End Class

''' <summary>
''' Use to render paypal receipt
''' </summary>
''' <remarks></remarks>
<Serializable()> _
Public Class CPayPalReceiptItem
	Public ProductName As String
	Public ProductCode As String
    Public DepositAmount As String
	Public ProductDescription As String

End Class