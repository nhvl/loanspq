﻿Imports Microsoft.VisualBasic
Imports LPQMobile.BO
Imports LPQMobile.Utils
Imports System.Xml
Imports System.Xml.Serialization
Imports System.Threading
Imports System.IO
Imports System.Net
Imports System.Web.Script.Serialization
Imports LPQMobile.Utils.Common


Public Class CBookingManager
	Protected Shared log As log4net.ILog = log4net.LogManager.GetLogger(GetType(CBookingManager))


	Protected Const constSymitarBookingURL As String = "/SymitarBookService/XABook.aspx"
    'symitar booking
    Protected Const constBookingURL As String = "/bookapp/bookapp.aspx"
    Protected Const constPathwayBookingURL As String = "/Integration/Pathway/BookAccount.aspx" 'TODO: this should be maped so it can used the constBookingURL
    'This same url if used for DNA, Cohesion / FIS Miser, Apex/XP2
    'Actual folder
    '/Integration/Cohesion/BookXAApp/BookXAApp.aspx"	 'Cohesion / FIS Miser
    '/Integration/XP/BookXAApp/BookXAApp.aspx"	 'XP

    Public Shared Function Book(ByVal poConifg As CWebsiteConfig, ByVal psLoanID As String, ByVal psAppType As String) As String

        ' Prepare the various URLs
        Dim baseSubmitURL = poConifg.BaseSubmitLoanUrl
        Dim symitarBookingURL As String = baseSubmitURL + constSymitarBookingURL
        Dim BookingURL As String = baseSubmitURL + constBookingURL
        Dim PathWayBookingURL As String = baseSubmitURL + constPathwayBookingURL
        Dim sAccountNumber As String = ""
        If psAppType = "XA" Or psAppType.Contains("COMBO") Then 'this loop only for xa    
            Select Case poConifg.Booking.ToUpper
                Case "SYMITAR" 'ok
                    Dim oSymitar As CSymitar = New CSymitar
                    sAccountNumber = oSymitar.Book(psLoanID, poConifg, symitarBookingURL)
                Case "DNA" 'GTE ok
                    Dim oDNA As CDNACore = New CDNACore
                    sAccountNumber = oDNA.Book(psLoanID, poConifg, BookingURL)
                Case Else
                    If String.IsNullOrWhiteSpace(poConifg.Booking) Then
                        log.Warn("Booking was not performed after funding, because booking is not setup for the portal configuration")
                        Return ""
                    End If
                    Dim sIntegrationType As String = CEnum.XABookingList.FirstOrDefault(Function(b) b.ToUpper = Common.SafeString(poConifg.Booking).ToUpper)
                    If String.IsNullOrWhiteSpace(sIntegrationType) Then
                        log.Warn("Invalid xa booking: " & poConifg.Booking)
                        Return ""
                    End If
                    Dim oBookCore As CBookCore = New CBookCore
                    '666-55-4321(XP(1st united require unique ssn, so change alter the last 4),  
                    'CONNECTIT(Alliance CU Sanjose case#204134, account number for internal transfer will be chech again the core so use other transfer method for testing
                    '"PATHWAY" SUMMIT tropical fb234003/236480
                    'Currently using HORIZONXCHANGE, JXCHANGE, OPENPATH, COHESION, XP, CONNECTIT, PATHWAY, SERVICEBUS for XA
                    sAccountNumber = oBookCore.Book(psLoanID, poConifg, BookingURL, "XA")
            End Select
        Else 'book loan
            Dim sLongLoantype = Common.GetConfigLoanTypeFromShort(psAppType)
            Dim sBookType As String = Common.getNodeAttributes(poConifg, sLongLoantype, "loan_booking")
            If sBookType = "" Then Return sAccountNumber
            Dim sIntegrationType As String = CEnum.LoanBookingList.FirstOrDefault(Function(b) b.ToUpper = sBookType.ToUpper)
            If String.IsNullOrWhiteSpace(sIntegrationType) Then
                log.Warn("Invalid loan booking: " & sBookType)
                Return ""
            End If
            Dim oBookCore As CBookCore = New CBookCore
            ''inspirus FB#219309 need to be Washington resi, require mem#=182429 on demo
            ''Currently using SYMITAR, TMG, and CORELATION for loans
            sAccountNumber = oBookCore.Book(psLoanID, poConifg, BookingURL, psAppType)
        End If
        Return sAccountNumber
    End Function
    ''' <summary>
    ''' This is similar to book but for use by clients who want to create membership via loan without using xa.
    ''' It reuses the xa booking attribute : <WEBSITE book="xxx"></WEBSITE>
    ''' </summary>
    ''' <param name="poConifg"></param>
    ''' <param name="psLoanID"></param>
    ''' <returns></returns>
    Public Shared Function CreatMembershipNoXA(ByVal poConifg As CWebsiteConfig, ByVal psLoanID As String, ByVal psAppType As String) As String
        ' Prepare the various URLs
        Dim baseSubmitURL = poConifg.BaseSubmitLoanUrl
        Dim BookingURL As String = baseSubmitURL + constBookingURL

        Dim sMemberNumber As String = ""
        Select Case poConifg.Booking.ToUpper
            Case "CMC"
                Dim oCoreBookingNoXa As CCoreBookingNoXa = New CCoreBookingNoXa
                sMemberNumber = oCoreBookingNoXa.Book(psLoanID, poConifg, BookingURL, psAppType)
        End Select
        Return sMemberNumber
    End Function
End Class
