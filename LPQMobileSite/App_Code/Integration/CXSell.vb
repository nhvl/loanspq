﻿Imports Microsoft.VisualBasic
Imports System.Xml
Imports System.Net
Imports LPQMobile.BO
Imports LPQMobile.Utils
'Imports LPQMobileSite.UserControls
Imports System.Text

''' <summary>
''' 
''' </summary>
''' <remarks></remarks>
Public Class CXSell

	Private Shared _log As log4net.ILog = log4net.LogManager.GetLogger(GetType(CXSell))
	'Private 

	Protected Const constXsellURL As String = "/GetCrossQualification/GetCrossQualification.aspx"

	Public Shared Function GetCrossQualification(poConfig As CWebsiteConfig, ByVal psLoanNumber As String, ByVal psAppType As String, sessionId As String) As String
		Dim sResult As String = ""
		''================================
		'' Build XML request
		Dim sb As New StringBuilder()
		Dim writer As XmlWriter = XmlTextWriter.Create(sb)
		writer.WriteStartDocument()

		' begin input
		writer.WriteStartElement("INPUT")

        writer.WriteStartElement("LOGIN")
        If poConfig.APIUserLender <> "" Then
            writer.WriteAttributeString("api_user_id", poConfig.APIUserLender)  'neeed to be at lender level
            writer.WriteAttributeString("api_password", poConfig.APIPasswordLender)
        Else
            writer.WriteAttributeString("api_user_id", poConfig.APIUser)
            writer.WriteAttributeString("api_password", poConfig.APIPassword)
        End If

        writer.WriteEndElement() 'end login

		writer.WriteStartElement("REQUEST")
		writer.WriteAttributeString("app_number", psLoanNumber)
		writer.WriteAttributeString("app_type", psAppType)
		writer.WriteEndElement() 'end REQUEST

		writer.WriteEndElement() ' end input
		writer.Flush()
		writer.Close()

		Dim RequestXMLString = sb.ToString()


		''==================
		''Begin post
		Try
			Dim baseSubmitURL = poConfig.BaseSubmitLoanUrl
            Dim xsellURL As String = baseSubmitURL + constXsellURL

            If poConfig.SubmitLoanUrl_TPV <> "" Then
                xsellURL = String.Format(xsellURL & "?OrgCode2={0}&LenderCode2={1}", poConfig.OrgCode2, poConfig.LenderCode2)
            End If

            Dim req As WebRequest = WebRequest.Create(xsellURL)

			req.Method = "POST"
			req.ContentType = "text/xml"

			_log.InfoFormat("Preparing to POST to xsell Url: {0}", xsellURL)
			_log.InfoFormat("Preparing to POST xsell Data: {0}", CSecureStringFormatter.MaskSensitiveXMLData(RequestXMLString))

			Dim streamWriter As New System.IO.StreamWriter(req.GetRequestStream())
			streamWriter.Write(RequestXMLString)
			streamWriter.Close()

			Dim res As WebResponse = req.GetResponse()

			Dim reader As New System.IO.StreamReader(res.GetResponseStream())
			Dim sXml As String = reader.ReadToEnd()

			_log.DebugFormat("Response from server : {0}", sXml)

			reader.Close()
			res.GetResponseStream().Close()
            req.GetRequestStream().Close()
            ''filter cross sell products: remove products that belong to current apptype        
            sXml = filterCrossSellProducts(sXml, psAppType)
            sResult = GetXSellUI(sXml, poConfig, sessionId)
        Catch ex As Exception
			_log.Error("Xsell error: " & ex.Message, ex)
		End Try

		Return sResult

	End Function

    Protected Shared Function GetXSellUI(ByVal psResponseXml As String, poConfig As CWebsiteConfig, sessionId As String) As String
        Dim xmlDoc As New XmlDocument()

        'TODO: remove psResponseXml override for live
        'TODO: implement test mode to use this override
        'psResponseXml = "<OUTPUT version=""1.0""><RESPONSE><APPLICANTS><APPLICANT first_name=""Marisol"" last_name=""Testcase""><VEHICLE_LOAN_SUMMARY><REFINANCE_GRADES><REFINANCE_GRADE creditor_name=""USAA FEDERAL SAVINGS B"" rate=""2.74"" balance=""28626"" old_monthly_payment=""533"" new_monthly_payment=""519.77"" max_amount_qualified=""100000"" monthly_savings=""13.23"" /></REFINANCE_GRADES><NEW_GRADE rate=""2.74"" monthly_payment=""357.49"" number_of_payments=""60"" loan_amount=""20000"" max_amount_qualified=""100000"" /></VEHICLE_LOAN_SUMMARY></APPLICANT></APPLICANTS></RESPONSE></OUTPUT>"

        'Uncomment this for testing purpose
        'pl#10572 has vl and cc
        'psResponseXml = "<OUTPUT version=""1.0""><RESPONSE><APPLICANTS><APPLICANT first_name=""MARISOL"" last_name=""TESTCASE"">" & _
        '				"<CREDIT_CARD_SUMMARY><CREDIT_CARDS><CREDIT_CARD card_type=""CREDIT"" card_name=""MasterCard 1"" interest_rate=""8.74"" credit_limit=""30000"" /><CREDIT_CARD card_type=""CREDIT"" card_name=""World MasterCard"" interest_rate=""8.74"" credit_limit=""30000"" /></CREDIT_CARDS></CREDIT_CARD_SUMMARY>" & _
        '				"<VEHICLE_LOAN_SUMMARY><REFINANCE_GRADES><REFINANCE_GRADE creditor_name=""USAA FEDERAL SAVINGS B"" rate=""2.24"" balance=""28626"" old_monthly_payment=""533"" new_monthly_payment=""505.24"" max_amount_qualified=""50000"" monthly_savings=""27.76"" />" & _
        '				"</REFINANCE_GRADES><NEW_GRADE rate=""3.74"" monthly_payment=""1166.34"" number_of_payments=""72"" loan_amount=""75000"" max_amount_qualified=""100000"" /></VEHICLE_LOAN_SUMMARY>" & _
        '				"<HOME_EQUITY_SUMMARY><LOAN_PROGRAMS><LOAN_PROGRAM program_name=""test heloc"" apr=""3.785"" interest_rate=""3.785"" monthly_payment=""320"" monthly_savings=""320"" max_amount_qualified=""123320"" /></LOAN_PROGRAMS><ASSUMPTIONS assumed_amount=""50000"" assumed_term=""120"" /></HOME_EQUITY_SUMMARY>" & _
        '				"</APPLICANT></APPLICANTS></RESPONSE></OUTPUT>"

        'cc#10453 has pl, HE


        xmlDoc.LoadXml(psResponseXml)


        ''Vehicle
        Dim VlrefinanceGrades As New List(Of CXSellRefinanceGrade)
        Dim vlGrade As CXSellNewGrade = Nothing
        Dim vlSummaryNode As XmlNode = xmlDoc.SelectSingleNode("/OUTPUT/RESPONSE/APPLICANTS/APPLICANT/VEHICLE_LOAN_SUMMARY")
        If vlSummaryNode IsNot Nothing Then
            Dim refinanceGradeNodes = vlSummaryNode.SelectNodes("REFINANCE_GRADES/REFINANCE_GRADE")
            If refinanceGradeNodes IsNot Nothing AndAlso refinanceGradeNodes.Count > 0 Then
                For Each node As XmlNode In refinanceGradeNodes
                    Dim item As New CXSellRefinanceGrade()
                    item.Balance = CDec(node.Attributes("balance").Value)
                    item.CreatorName = node.Attributes("creditor_name").Value
                    item.Rate = CDec(node.Attributes("rate").Value)
                    item.OldMonthlyPayment = CDec(node.Attributes("old_monthly_payment").Value)
                    item.NewMonthlyPayment = CDec(node.Attributes("new_monthly_payment").Value)
                    item.MaxAmountQualified = CDec(node.Attributes("max_amount_qualified").Value)
                    item.MonthlySavings = CDec(node.Attributes("monthly_savings").Value)
                    VlrefinanceGrades.Add(item)
                Next
            End If
            Dim newGradeNode = vlSummaryNode.SelectSingleNode("NEW_GRADE")
            If newGradeNode IsNot Nothing Then
                vlGrade = New CXSellNewGrade()
                vlGrade.LoanAmount = CDec(newGradeNode.Attributes("loan_amount").Value)
                vlGrade.MaxAmountQualified = CDec(newGradeNode.Attributes("max_amount_qualified").Value)
                vlGrade.MonthlyPayment = CDec(newGradeNode.Attributes("monthly_payment").Value)
                vlGrade.Rate = CDec(newGradeNode.Attributes("rate").Value)
                vlGrade.NumberOfPayments = CInt(newGradeNode.Attributes("number_of_payments").Value)
            End If
        End If

        'HE
        Dim heRefinanceGrades As New List(Of CXSellRefinanceGrade)
        Dim heSummaryNode As XmlNode = xmlDoc.SelectSingleNode("/OUTPUT/RESPONSE/APPLICANTS/APPLICANT/HOME_EQUITY_SUMMARY")
        If heSummaryNode IsNot Nothing Then
            Dim assumptionNode = heSummaryNode.SelectSingleNode("ASSUMPTIONS")
            Dim assumedAmount As Decimal = 0
            Dim assumedTerm As Integer = 0
            If assumptionNode IsNot Nothing Then
                assumedAmount = Common.SafeDouble(assumptionNode.Attributes("assumed_amount").Value)
                assumedTerm = Common.SafeInteger(assumptionNode.Attributes("assumed_term").Value)
            End If
            Dim refinanceGradeNodes = heSummaryNode.SelectNodes("LOAN_PROGRAMS/LOAN_PROGRAM")
            If refinanceGradeNodes IsNot Nothing AndAlso refinanceGradeNodes.Count > 0 Then
                For Each node As XmlNode In refinanceGradeNodes
                    Dim item As New CXSellRefinanceGrade()
                    'item.Balance = CDec(node.Attributes("balance").Value)
                    item.ProgramName = Common.SafeString(node.Attributes("program_name").Value)
                    item.Rate = CDec(node.Attributes("interest_rate").Value)
                    item.APR = CDec(node.Attributes("apr").Value)
                    'item.OldMonthlyPayment = CDec(node.Attributes("old_monthly_payment").Value)
                    If node.Attributes("monthly_payment") IsNot Nothing Then
                        item.NewMonthlyPayment = CDec(node.Attributes("monthly_payment").Value)
                    End If
                    If node.Attributes("max_amount_qualified") IsNot Nothing Then
                        item.MaxAmountQualified = CDec(node.Attributes("max_amount_qualified").Value)
                    End If
                    If node.Attributes("monthly_savings") IsNot Nothing Then
                        item.MonthlySavings = CDec(node.Attributes("monthly_savings").Value)
                    End If

                    item.AssumedAmount = assumedAmount
                    item.AssumedTerm = assumedTerm
                    heRefinanceGrades.Add(item)
                Next
            End If
        End If

        'credit card
        Dim newCreditCardGrades As New List(Of CXSellNewGrade)
        Dim ccSummaryNode As XmlNode = xmlDoc.SelectSingleNode("/OUTPUT/RESPONSE/APPLICANTS/APPLICANT/CREDIT_CARD_SUMMARY")
        If ccSummaryNode IsNot Nothing Then
            Dim ccNodes = ccSummaryNode.SelectNodes("CREDIT_CARDS/CREDIT_CARD")
            If ccNodes IsNot Nothing AndAlso ccNodes.Count > 0 Then
                For Each node As XmlNode In ccNodes
                    Dim item As New CXSellNewGrade()
                    item.CreditCardName = Common.SafeString(node.Attributes("card_name").Value)
                    item.CreditCardType = Common.SafeString(node.Attributes("card_type").Value)
                    item.Rate = Common.SafeDouble(node.Attributes("interest_rate").Value)
                    item.MaxAmountQualified = Common.SafeString(node.Attributes("credit_limit").Value)
                    newCreditCardGrades.Add(item)
                Next
            End If
        End If


        Dim control = UserControlUtils.LoadControl(Of CBaseXSellUserControl)("~/Inc/MainApp/XSellSelection.ascx")
        control.VlRefinanceGrades = VlrefinanceGrades
        control.VlGrade = vlGrade
        control.HeRefinanceGrades = heRefinanceGrades
        control.CreditCardGrades = newCreditCardGrades
        control.LenderRef = poConfig.LenderRef
        control.SessionId = sessionId
        Return control.RenderHtml_()

        Return ""
    End Function
    Private Shared Function filterCrossSellProducts(ByVal psResponseXML As String, ByVal sLoantype As String) As String
        Dim sResult As String = ""
        Dim xmlResponse = New XmlDocument()
        xmlResponse.LoadXml(psResponseXML)
        Dim applicantNodes As XmlNodeList = xmlResponse.GetElementsByTagName("APPLICANT")
        If applicantNodes.Count > 0 Then
            For Each oApplicant As XmlElement In applicantNodes
                For Each oLoanNode As XmlElement In oApplicant.ChildNodes
                    Select Case sLoantype.ToUpper
                        Case "VL"
                            If oLoanNode.Name = "VEHICLE_LOAN_SUMMARY" Then
                                oLoanNode.ParentNode.RemoveChild(oLoanNode)
                                '' _log.Info(sLoantype & "--remove product: " & oLoanNode.OuterXml)
                            End If
                        Case "CC"
                            If oLoanNode.Name = "CREDIT_CARD_SUMMARY" Then
                                oLoanNode.ParentNode.RemoveChild(oLoanNode)
                                '' _log.Info(sLoantype & "--remove product: " & oLoanNode.OuterXml)
                            End If
                        Case "HE"
                            If oLoanNode.Name = "HOME_EQUITY_SUMMARY" Then
                                oLoanNode.ParentNode.RemoveChild(oLoanNode)
                                ''_log.Info(sLoantype & "--remove product: " & oLoanNode.OuterXml)
                            End If
                    End Select
                Next
            Next
            sResult = xmlResponse.OuterXml
        End If
        '' _log.Info("filter products: " & sResult)
        Return sResult
    End Function
End Class
