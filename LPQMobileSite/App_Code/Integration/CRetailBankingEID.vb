﻿Imports Microsoft.VisualBasic
Imports System.Xml
Imports System.Net
Imports System.IO
Imports LPQMobile.BO
Imports LPQMobile.Utils
Imports System.Text

Public Class CRetailBankingEID
    Private log As log4net.ILog = log4net.LogManager.GetLogger(GetType(CRetailBankingEID))
    Public Sub New()
    End Sub

    Public Function isRBankingSuccessful(ByVal responseXMLStr As String) As Boolean
        Dim responseXMLDoc As XmlDocument = New XmlDocument()
        responseXMLDoc.LoadXml(responseXMLStr)
        ''get the attribute from the xml response
        Dim responseAttribute As String = ""
        Dim responseAttributeNode As XmlNode = responseXMLDoc.SelectSingleNode("//@decision_code")
        If responseAttributeNode Is Nothing Then Return False
        responseAttribute = responseAttributeNode.InnerXml
        ' Compare the result with the proper success result
        If responseAttribute.ToUpper() = "APPROVED" Then
            Return True
        Else
            Return False
        End If
    End Function

    ''------------------ build eFund for minor--------------------
    Public Function Execute_minor(ByVal psLoanID As String, ByVal psIsJoint As Boolean, ByVal poConfig As CWebsiteConfig, ByVal psUrl As String, ByVal sMinorDebitEnable As String, ByVal onlyMinorApp As Boolean) As Boolean

        Try
            ''run debit for minor if it 's enabled
            If sMinorDebitEnable = "Y" Then
                Dim minor_eFundsXMLRequest As String = BuildRetailBankingXMLRequest_minor(psLoanID, poConfig, False, "0")
                Dim minor_req As WebRequest = WebRequest.Create(psUrl)

                minor_req.Method = "POST"
                minor_req.ContentType = "text/xml"

                log.Info("XA-MINOR: Preparing to POST RetailBanking Data: " & CSecureStringFormatter.MaskSensitiveXMLData(minor_eFundsXMLRequest))

                Dim minor_writer As New StreamWriter(minor_req.GetRequestStream())
                minor_writer.Write(minor_eFundsXMLRequest)
                minor_writer.Close()

                Dim minor_res As WebResponse = minor_req.GetResponse()

                Dim minor_reader As New StreamReader(minor_res.GetResponseStream())
                Dim minor_sXml As String = minor_reader.ReadToEnd()

                log.Info("XA-MINOR: Response from server(RetailBanking): " + minor_sXml)

                minor_reader.Close()
                minor_res.GetResponseStream().Close()
                minor_req.GetRequestStream().Close()

                If Not isRBankingSuccessful(minor_sXml) Then
                    Return False
                End If
            End If
            If Not onlyMinorApp Then
                ''====================run for Parent =================================================
                Dim eFundsXMLRequest As String = BuildRetailBankingXMLRequest_minor(psLoanID, poConfig, False, "1")
                Dim req As WebRequest = WebRequest.Create(psUrl)

                req.Method = "POST"
                req.ContentType = "text/xml"

                log.Info("XA-Custodian: Preparing to POST RetailBanking Data: " & CSecureStringFormatter.MaskSensitiveXMLData(eFundsXMLRequest))

                Dim writer As New StreamWriter(req.GetRequestStream())
                writer.Write(eFundsXMLRequest)
                writer.Close()

                Dim res As WebResponse = req.GetResponse()

                Dim reader As New StreamReader(res.GetResponseStream())
                Dim sXml As String = reader.ReadToEnd()

                log.Info("XA-Custodian: Response from server(RetailBanking): " + sXml)

                reader.Close()
                res.GetResponseStream().Close()
                req.GetRequestStream().Close()

                If Not isRBankingSuccessful(sXml) Then
                    Return False
                End If

                ''====================end run for parent ==============================================
                ' ''======Run for joint==========
                If Not psIsJoint Then Return True

                Dim joint_eFundsXMLRequest As String = BuildRetailBankingXMLRequest_minor(psLoanID, poConfig, True, "1")
                Dim joint_req As WebRequest = WebRequest.Create(psUrl)

                joint_req.Method = "POST"
                joint_req.ContentType = "text/xml"

                log.Info("XA-Custodian Joint: Preparing to POST RetailBanking Data: " & CSecureStringFormatter.MaskSensitiveXMLData(joint_eFundsXMLRequest))

                Dim joint_writer As New StreamWriter(joint_req.GetRequestStream())
                joint_writer.Write(joint_eFundsXMLRequest)
                joint_writer.Close()

                Dim joint_res As WebResponse = joint_req.GetResponse()

                Dim joint_reader As New StreamReader(joint_res.GetResponseStream())
                Dim joint_sXml As String = joint_reader.ReadToEnd()

                log.Info("XA-Custodian Joint: Response from server(RetailBanking): " + joint_sXml)

                joint_reader.Close()
                joint_res.GetResponseStream().Close()
                joint_req.GetRequestStream().Close()

                If Not isRBankingSuccessful(joint_sXml) Then   'reserve 10/2
                    Return False '' exit 
                End If
            End If
        Catch ex As Exception
            log.Error("XA: Minor Retainl Banking Detect error): " & ex.Message, ex)
            Return False
        End Try
        Return True

    End Function
    ''-------------------build retail banking for minor --------------------
    Protected Function BuildRetailBankingXMLRequest_minor(ByVal psLoanID As String, ByVal poConfig As CWebsiteConfig, ByVal isJoint As Boolean, ByVal sApplicantIndex As String) As String
        ' Now construct the XML response
        Dim sb As New StringBuilder()
        Dim writer As XmlWriter = XmlTextWriter.Create(sb)
        writer.WriteStartDocument()
        ' begin input
        writer.WriteStartElement("INPUT")
        writer.WriteStartElement("LOGIN")
        writer.WriteAttributeString("api_user_id", poConfig.APIUser)
        writer.WriteAttributeString("api_password", poConfig.APIPassword)
        writer.WriteEndElement()
        writer.WriteStartElement("REQUEST")
        writer.WriteAttributeString("xpressappid", psLoanID)
        writer.WriteAttributeString("applicant_index", sApplicantIndex)
        If isJoint Then
            writer.WriteAttributeString("applicant_type", "JOINT")
        Else
            writer.WriteAttributeString("applicant_type", "PRIMARY")
        End If

        writer.WriteEndElement()
        ' end input
        writer.WriteEndElement()
        writer.Flush()
        writer.Close()
        Dim returnStr = sb.ToString()
        Return returnStr
    End Function


End Class
