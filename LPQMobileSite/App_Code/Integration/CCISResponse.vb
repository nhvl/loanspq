﻿Imports Microsoft.VisualBasic
Imports System.Xml
Imports System.Net
Imports LPQMobile.BO
Imports LPQMobile.Utils
Imports System.Web.Script.Serialization
Public Class CCISResponse
    Public CustomerInfoList As New List(Of CCISCustomerInfo)
    Private _log As log4net.ILog = log4net.LogManager.GetLogger(GetType(CCISResponse))
    Sub New(ByVal sResponseXML As String)
        CustomerInfoList = ParsedCustomerInfoResponse(sResponseXML)
    End Sub
    Public Function ParsedCustomerInfoResponse(ByVal sResponseXML As String) As List(Of CCISCustomerInfo)
        Dim xmlDoc As New XmlDocument
        Dim sResult As New List(Of CCISCustomerInfo)
        xmlDoc.LoadXml(sResponseXML)
        ''CIS Response mapping base on the CONSUMER_INFO, and CUSTOMER_INFO nodes
        ''new CIS service using CUSTOMER_INFO node.
        Dim IsNewCISVersion As Boolean = False
        Dim oCustomerInfoNode = xmlDoc.GetElementsByTagName("CUSTOMER_INFO")
        Dim oConsumerInfoNodes As XmlNodeList = xmlDoc.GetElementsByTagName("CONSUMER_INFO")
        If oCustomerInfoNode IsNot Nothing AndAlso oCustomerInfoNode.Count > 0 Then
            IsNewCISVersion = True
            oConsumerInfoNodes = oCustomerInfoNode
        End If
        If oConsumerInfoNodes.Count = 0 Then Return sResult
        Dim oAccountList As New List(Of CCISCustomerAccount)
        Dim oIDCardList As New List(Of CCISCustomerIDCard)
        Try
            For Each oConsumer As XmlElement In oConsumerInfoNodes
                Dim addressNodes As XmlNodeList = oConsumer.GetElementsByTagName("ADDRESS")
                Dim sStreet = "", sCity = "", sState = "", sZip = "", sHomePhone = "", sHomeMobilePhone = "", sWorkMobilePhone = "", sWorkPhone = "", sExtension = ""
                If addressNodes.Count > 0 Then
                    Dim addressNode = addressNodes(0)
                    If addressNode.Attributes("street_address_1") IsNot Nothing Then
                        sStreet = Common.SafeString(addressNode.Attributes("street_address_1").InnerText)
                    End If
                    If addressNode.Attributes("city") IsNot Nothing Then
                        sCity = Common.SafeString(addressNode.Attributes("city").InnerText)
                    End If
                    If addressNode.Attributes("zip") IsNot Nothing Then
                        sZip = Common.SafeString(addressNode.Attributes("zip").InnerText)
                    End If
                    If addressNode.Attributes("state") IsNot Nothing Then
                        sState = Common.SafeString(addressNode.Attributes("state").InnerText)
                    End If
                End If
                Dim phoneNodes As XmlNodeList = oConsumer.GetElementsByTagName("PHONE")
                If phoneNodes.Count > 0 Then
                    For Each oPhone As XmlElement In phoneNodes
                        If oPhone.Attributes("phone_type") IsNot Nothing And oPhone.Attributes("phone_number") IsNot Nothing Then
                            Dim sPhoneType = Common.SafeString(oPhone.Attributes("phone_type").InnerText).ToUpper
                            Dim sPhoneNumber = Common.SafeString(oPhone.Attributes("phone_number").InnerText)
                            If sPhoneType = "HOME" Or sPhoneType = "HOME PHONE" Then
                                sHomePhone = sPhoneNumber
                            ElseIf sPhoneType.Contains("UNKNOWN") Then
                                sHomeMobilePhone = sPhoneNumber
                            ElseIf sPhoneType.Contains("CELL") Then
                                sWorkMobilePhone = sPhoneNumber
                            ElseIf sPhoneType = "WORK" Or sPhoneType = "BUSINESS PHONE" Or sPhoneType = "WORK PHONE" Then
                                sWorkPhone = sPhoneNumber
                                If oPhone.Attributes("extension") IsNot Nothing Then
                                    sExtension = Common.SafeString(oPhone.Attributes("extension").InnerText)
                                End If
                            End If
                        End If
                    Next
                End If
                Dim accountNodes As XmlNodeList = oConsumer.GetElementsByTagName("ACCOUNT")
                If accountNodes.Count > 0 Then
                    For Each oAccountNode As XmlElement In accountNodes
                        Dim sAccountNumber = "", sDescription = "", sProductCode = "", sRate = "", sBalance = ""
                        If oAccountNode.Attributes("account_number") IsNot Nothing AndAlso oAccountNode.Attributes("account_number").InnerText <> "" Then
                            sAccountNumber = Common.SafeString(oAccountNode.Attributes("account_number").InnerText)
                            If oAccountNode.Attributes("description") IsNot Nothing Then
                                sDescription = Common.SafeString(oAccountNode.Attributes("description").InnerText)
                            End If
                            Dim productCodeNodes As XmlNodeList = oAccountNode.GetElementsByTagName("CORE_FIELDS")
                            If productCodeNodes.Count > 0 AndAlso productCodeNodes(0).Attributes("prodCode") IsNot Nothing Then
                                sProductCode = Common.SafeString(productCodeNodes(0).Attributes("prodCode").InnerText)
                            End If
                            If oAccountNode.Attributes("rate") IsNot Nothing Then
                                sRate = Common.SafeString(oAccountNode.Attributes("rate").InnerText)
                            End If
                            If oAccountNode.Attributes("balance") IsNot Nothing Then
                                sBalance = Common.SafeString(oAccountNode.Attributes("balance").InnerText)
                            End If
                            Dim oAccount = New CCISCustomerAccount With {
                             .AccountNumber = sAccountNumber,
                             .Description = sDescription,
                             .ProductCode = sProductCode,
                             .Rate = sRate,
                             .Balance = sBalance
                            }
                            oAccountList.Add(oAccount)
                        End If
                    Next
                End If
                ''jXChange doesn't return ID information, so unfortunately, it's not information that can be imported.
                Dim idCardNodes As XmlNodeList = oConsumer.GetElementsByTagName("ID_CARD")
                ''only get primary id
                If idCardNodes.Count > 0 Then
                    Dim idCardNode = idCardNodes(0), sIdNumber = "", sIdType = "", sIdState = "", sIdCountry = "", sIdIssuedDate = "", sIdExpirationDate = ""
                    If idCardNode.Attributes("card_number") IsNot Nothing Then
                        sIdNumber = Common.SafeString(idCardNode.Attributes("card_number").InnerText)
                    End If
                    If idCardNode.Attributes("card_type") IsNot Nothing Then
                        sIdType = Common.SafeString(idCardNode.Attributes("card_type").InnerText)
                    End If
                    ''if state attribute is nothing then looking for id state in the FIELD node
                    ''sample xml for ID_CARD without state attribute
                    ''<ID_CARD card_type="DRIVERS LICENSE" card_type_core="002" card_number="8645923" date_issued="4/4/2019" exp_date="4/4/2024">
                    ''	<CORE_FIELDS>
                    ''		<DATA_FIELDS>
                    ''			<FIELD name = "description" type="STRING" description="ID Description">FL STATE DRIVERS LICENSE</FIELD>
                    ''		</DATA_FIELDS>
                    ''	</CORE_FIELDS>
                    ''	<CLIENT_CUSTOMIZATIONS />
                    ''</ID_CARD>
                    If idCardNode.Attributes("state") IsNot Nothing Then
                        sIdState = Common.SafeString(idCardNode.Attributes("state").InnerText)
                    Else
                        Dim fieldNodes As XmlNodeList = CType(idCardNodes(0), XmlElement).GetElementsByTagName("FIELD")
                        If fieldNodes IsNot Nothing AndAlso fieldNodes.Count > 0 Then
                            For Each oField As XmlElement In fieldNodes
                                If oField.Attributes("description") IsNot Nothing AndAlso Common.SafeString(oField.Attributes("description").InnerText).ToUpper = "ID DESCRIPTION" Then
                                    Dim sIdDescription = Common.SafeString(oField.InnerText).ToUpper
                                    If sIdDescription.Contains("STATE DRIVERS") Or sIdDescription.Contains("STATE ID") Then
                                        sIdState = sIdDescription.Substring(0, 2)
                                        Exit For
                                    End If
                                End If
                            Next
                        End If
                    End If
                    If idCardNode.Attributes("country") IsNot Nothing Then
                        sIdCountry = Common.SafeString(idCardNode.Attributes("country").InnerText)
                    End If
                    If idCardNode.Attributes("date_issued") IsNot Nothing Then
                        sIdIssuedDate = Common.SafeString(idCardNode.Attributes("date_issued").InnerText)
                        If sIdIssuedDate <> "" Then
                            Dim dIdIssuedDate As DateTime = DateTime.ParseExact(sIdIssuedDate, "M/d/yyyy", System.Globalization.CultureInfo.InvariantCulture)
                            sIdIssuedDate = dIdIssuedDate.ToString("MMddyyyy", System.Globalization.CultureInfo.InvariantCulture)
                        End If
                    End If
                    If idCardNode.Attributes("exp_date") IsNot Nothing Then
                        sIdExpirationDate = Common.SafeString(idCardNode.Attributes("exp_date").InnerText)
                        If sIdExpirationDate <> "" Then
                            Dim dIdExpirationDate As DateTime = DateTime.ParseExact(sIdExpirationDate, "M/d/yyyy", System.Globalization.CultureInfo.InvariantCulture)
                            sIdExpirationDate = dIdExpirationDate.ToString("MMddyyyy", System.Globalization.CultureInfo.InvariantCulture)
                        End If
                    End If
                    If sIdNumber <> "" Then
                        Dim oIDCard As New CCISCustomerIDCard With {
                         .IdNumber = sIdNumber,
                         .IdType = sIdType,
                         .IdState = sIdState,
                         .IdCountry = sIdCountry,
                         .IdIssuedDate = sIdIssuedDate,
                         .IdExpirationDate = sIdExpirationDate
                        }
                        oIDCardList.Add(oIDCard)
                    End If
                End If
                Dim sMemberNumber = "", sFName = "", sLName = "", sMName = "", sSSN = "", sDOB = "", sEmail = "",
                    sMotherMaidenName = "", sEmployer = "", sOccupation = "", sMonthlyIncome = ""

                If oConsumer.Attributes("first_name") IsNot Nothing Then
                    sFName = Common.SafeString(oConsumer.Attributes("first_name").InnerText)
                End If
                If oConsumer.Attributes("last_name") IsNot Nothing Then
                    sLName = Common.SafeString(oConsumer.Attributes("last_name").InnerText)
                End If
                If oConsumer.Attributes("middle_name") IsNot Nothing Then
                    sMName = Common.SafeString(oConsumer.Attributes("middle_name").InnerText)
                End If
                If oConsumer.Attributes("dob") IsNot Nothing Then
                    sDOB = Common.SafeString(oConsumer.Attributes("dob").InnerText)
                End If
                If oConsumer.Attributes("email") IsNot Nothing Then
                    sEmail = Common.SafeString(oConsumer.Attributes("email").InnerText)
                End If
                If IsNewCISVersion Then
                    '' new CIS version using tax_id, and  custom_number instead of ssn, and member_number
                    If oConsumer.Attributes("tax_id") IsNot Nothing Then
                        sSSN = Common.SafeString(oConsumer.Attributes("tax_id").InnerText)
                    End If
                    If oConsumer.Attributes("customer_number") IsNot Nothing Then
                        sMemberNumber = Common.SafeString(oConsumer.Attributes("customer_number").InnerText)
                    End If
                    If oConsumer.Attributes("mother_maiden_name") IsNot Nothing Then
                        sMotherMaidenName = Common.SafeString(oConsumer.Attributes("mother_maiden_name").InnerText)
                    End If
                    If oConsumer.Attributes("employer") IsNot Nothing Then
                        sEmployer = Common.SafeString(oConsumer.Attributes("employer").InnerText)
                    End If
					If oConsumer.Attributes("occupation") IsNot Nothing Then
						sOccupation = Common.SafeString(oConsumer.Attributes("occupation").InnerText)
					End If
					If oConsumer.Attributes("monthly_income") IsNot Nothing Then
						sMonthlyIncome = Common.SafeString(oConsumer.Attributes("monthly_income").InnerText)
					End If
				Else
                    If oConsumer.Attributes("ssn") IsNot Nothing Then
                        sSSN = Common.SafeString(oConsumer.Attributes("ssn").InnerText)
                    End If
                    If oConsumer.Attributes("member_number") IsNot Nothing Then
                        sMemberNumber = Common.SafeString(oConsumer.Attributes("member_number").InnerText)
                    End If
                End If
                Dim oAccountInfo As New CCISCustomerInfo With {
                    .MemberNumber = sMemberNumber,
                    .FName = sFName,
                    .LName = sLName,
                    .MName = sMName,
                    .SSN = sSSN,
                    .DOB = sDOB,
                    .Email = sEmail,
                    .Street = sStreet,
                    .City = sCity,
                    .State = sState,
                    .Zip = sZip,
                    .MobilePhone = If(Not String.IsNullOrEmpty(sHomeMobilePhone), sHomeMobilePhone, sWorkMobilePhone),
                    .HomePhone = sHomePhone,
                    .WorkPhone = sWorkPhone,
                    .Extension = sExtension,
                    .AccountList = oAccountList,
                    .IDCardList = oIDCardList,
                    .MotherMaidenName = sMotherMaidenName,
                    .Employer = sEmployer,
                    .Occupation = sOccupation,
                    .MonthlyIncome = sMonthlyIncome
                }
                sResult.Add(oAccountInfo)
            Next
        Catch ex As Exception
            _log.Error("CIS error: " & ex.Message, ex)
        End Try

        Return sResult
    End Function

End Class
