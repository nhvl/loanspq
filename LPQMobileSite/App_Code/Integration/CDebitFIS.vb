﻿Imports Microsoft.VisualBasic
Imports System.Xml
Imports System.Net
Imports System.IO
Imports LPQMobile.BO
Imports LPQMobile.Utils
Imports System.Text
Public Class CDebitFIS
    Private log As log4net.ILog = log4net.LogManager.GetLogger(GetType(CDebitFIS))
    Public Sub New()
    End Sub
    ''------------------end execute eFund for minor----------------

    Public Function isEFundsSuccessful(ByVal responseXMLStr As String) As Boolean
        ' Return true if eFunds was successful
        Dim responseXMLDoc As XmlDocument = New XmlDocument()
        responseXMLDoc.LoadXml(responseXMLStr)
        ' Get the attribute from the XML response
        Dim responseAttribute As String = ""
        Dim passedIdentityManager As String = ""
        Dim passedOFAC As String = ""

        Dim responseAttributeNode As XmlNode = responseXMLDoc.SelectSingleNode("//@qualifile_decision")
        Dim resIdentityManagerNode As XmlNode = responseXMLDoc.SelectSingleNode("//@passed_identity_manager")
        Dim resOFACNode As XmlNode = responseXMLDoc.SelectSingleNode("//@passed_ofac")

        '' check qualify_decision
        If responseAttributeNode Is Nothing Then Return False

        responseAttribute = responseAttributeNode.InnerXml.ToUpper
        Dim qualifileDecision As Boolean = If(responseAttribute = "ACCEPT" Or responseAttribute = "APPROVE" Or responseAttribute = "APPROVED", True, False)

        ''exist quailifeDecision, identiyManagerNode and OFACNode 
        If resIdentityManagerNode IsNot Nothing And resOFACNode IsNot Nothing Then
            passedIdentityManager = resIdentityManagerNode.InnerXml.ToUpper
            passedOFAC = resOFACNode.InnerXml.ToUpper
            If qualifileDecision And passedIdentityManager = "Y" And passedOFAC = "Y" Then
                Return True ''success result
            Else
                Return False
            End If
        ElseIf resIdentityManagerNode IsNot Nothing And resOFACNode Is Nothing Then ''exist qualifileDecision and IdentityMangager
            passedIdentityManager = resIdentityManagerNode.InnerXml.ToUpper
            If qualifileDecision And passedIdentityManager = "Y" Then
                Return True ''success result
            Else
                Return False
            End If
        ElseIf resIdentityManagerNode Is Nothing And resOFACNode IsNot Nothing Then ''exist qualifile decision and OFAC
            passedOFAC = resOFACNode.InnerXml.ToUpper
            If qualifileDecision And passedOFAC = "Y" Then
                Return True ''success result
            Else
                Return False
            End If
        End If

        ''by default exist only qualifile decision
        If qualifileDecision Then
            Return True
        Else
            Return False
        End If
    End Function
    ''------------------ build eFund for minor--------------------
    Public Function Execute_minor(ByVal psLoanID As String, ByVal psIsJoint As Boolean, ByVal poConfig As CWebsiteConfig, ByVal eFundsUrl As String, ByVal sMinorDebitEnable As String, ByVal onlyMinorApp As Boolean) As Boolean
        Try
            ''run debit for minor if it 's enabled
            If sMinorDebitEnable = "Y" Then
                Dim minor_eFundsXMLRequest As String = BuildEFundsXMLRequest_minor(psLoanID, poConfig, False, "0")
                Dim minor_req As WebRequest = WebRequest.Create(eFundsUrl)

                minor_req.Method = "POST"
                minor_req.ContentType = "text/xml"

                log.Info("XA-MINOR: Preparing to POST eFunds Data: " & CSecureStringFormatter.MaskSensitiveXMLData(minor_eFundsXMLRequest))

                Dim minor_writer As New StreamWriter(minor_req.GetRequestStream())
                minor_writer.Write(minor_eFundsXMLRequest)
                minor_writer.Close()

                Dim minor_res As WebResponse = minor_req.GetResponse()

                Dim minor_reader As New StreamReader(minor_res.GetResponseStream())
                Dim minor_sXml As String = minor_reader.ReadToEnd()

                log.Info("XA-MINOR: Response from server(eFund): " + minor_sXml)

                minor_reader.Close()
                minor_res.GetResponseStream().Close()
                minor_req.GetRequestStream().Close()

                If Not isEFundsSuccessful(minor_sXml) Then
                    Return False
                End If
            End If
            If Not onlyMinorAPP Then
                ''====================run for Parent =================================================
                Dim eFundsXMLRequest As String = BuildEFundsXMLRequest_minor(psLoanID, poConfig, False, "1")
                Dim req As WebRequest = WebRequest.Create(eFundsUrl)

                req.Method = "POST"
                req.ContentType = "text/xml"

                log.Info("XA-Custodian: Preparing to POST eFunds Data: " & CSecureStringFormatter.MaskSensitiveXMLData(eFundsXMLRequest))

                Dim writer As New StreamWriter(req.GetRequestStream())
                writer.Write(eFundsXMLRequest)
                writer.Close()

                Dim res As WebResponse = req.GetResponse()

                Dim reader As New StreamReader(res.GetResponseStream())
                Dim sXml As String = reader.ReadToEnd()

                log.Info("XA-Custodian: Response from server(eFund): " + sXml)

                reader.Close()
                res.GetResponseStream().Close()
                req.GetRequestStream().Close()

                If Not isEFundsSuccessful(sXml) Then
                    Return False
                End If

                ''====================end run for parent ==============================================
                ' ''======Run for joint parent==========

                If Not psIsJoint Then Return True

                Dim joint_eFundsXMLRequest As String = BuildEFundsXMLRequest_minor(psLoanID, poConfig, True, "1")
                Dim joint_req As WebRequest = WebRequest.Create(eFundsUrl)

                joint_req.Method = "POST"
                joint_req.ContentType = "text/xml"

                log.Info("XA-Custodian Joint: Preparing to POST eFunds Data: " & CSecureStringFormatter.MaskSensitiveXMLData(joint_eFundsXMLRequest))

                Dim joint_writer As New StreamWriter(joint_req.GetRequestStream())
                joint_writer.Write(joint_eFundsXMLRequest)
                joint_writer.Close()

                Dim joint_res As WebResponse = joint_req.GetResponse()

                Dim joint_reader As New StreamReader(joint_res.GetResponseStream())
                Dim joint_sXml As String = joint_reader.ReadToEnd()

                log.Info("XA-Custodian Joint: Response from server(eFund): " + joint_sXml)

                joint_reader.Close()
                joint_res.GetResponseStream().Close()
                joint_req.GetRequestStream().Close()

                If Not isEFundsSuccessful(joint_sXml) Then
                    Return False '' exit 
                End If
            End If
        Catch ex As Exception
            log.Error("XA: Minor Efund Detect error): " & ex.Message, ex)
            Return False
        End Try
        Return True

    End Function
    ''-------------------build eFund for minor --------------------
    Protected Function BuildEFundsXMLRequest_minor(ByVal psLoanID As String, ByVal poConfig As CWebsiteConfig, ByVal isJoint As Boolean, ByVal sApplicantIndex As String) As String
        ' Now construct the XML response
        Dim sb As New StringBuilder()
        Dim writer As XmlWriter = XmlTextWriter.Create(sb)
        writer.WriteStartDocument()
        ' begin input
        writer.WriteStartElement("INPUT")
        writer.WriteStartElement("LOGIN")
        writer.WriteAttributeString("api_user_id", poConfig.APIUser)
        writer.WriteAttributeString("api_password", poConfig.APIPassword)
        writer.WriteEndElement()
        writer.WriteStartElement("REQUEST")
        writer.WriteAttributeString("xpressappid", psLoanID)
        writer.WriteAttributeString("applicant_index", sApplicantIndex)
        If isJoint Then
            writer.WriteAttributeString("applicant_type", "JOINT")
        Else
            writer.WriteAttributeString("applicant_type", "PRIMARY")
        End If

        writer.WriteEndElement()
        ' end input
        writer.WriteEndElement()
        writer.Flush()
        writer.Close()
        Dim returnStr = sb.ToString()
        Return returnStr
    End Function

End Class
