﻿Imports Microsoft.VisualBasic
Imports System.Xml
Imports System.Net
Imports LPQMobile.BO
Imports LPQMobile.Utils
Imports System.Text

Public Class CDeluxe
	Private _log As log4net.ILog = log4net.LogManager.GetLogger(GetType(CDeluxe))
	Public Sub New()
	End Sub

	'to get "Proceed
	'001010031	Buggs	Bunny	02/25/1951
	'phone 7147573456, driver a4455606
	Public Function Execute(ByVal psLoanID As String, ByVal psHasJoint As Boolean, ByVal poConfig As CWebsiteConfig, ByVal psUrl As String) As Boolean
		Try
			''Begin post for primary applicant
			Dim RequestXMLString As String = BuildDELUXEXMLRequest(psLoanID, poConfig, False, "0")
			Dim req As WebRequest = WebRequest.Create(psUrl)

			req.Method = "POST"
			req.ContentType = "text/xml"

			_log.Info(String.Format("Preparing to POST Deluxe detect Url: {0}", psUrl))
			_log.Info(String.Format("Preparing to POST Deluxe detect Data: {0}", CSecureStringFormatter.MaskSensitiveXMLData(RequestXMLString)))

			Dim streamWriter As New System.IO.StreamWriter(req.GetRequestStream())
			streamWriter.Write(RequestXMLString)
			streamWriter.Close()

			Dim res As WebResponse = req.GetResponse()

			Dim reader As New System.IO.StreamReader(res.GetResponseStream())
			Dim sXml As String = reader.ReadToEnd()

			_log.Info("XA: Response from server(Deluxe detect): " & CSecureStringFormatter.MaskSensitiveXMLData(sXml))

			reader.Close()
			res.GetResponseStream().Close()
			req.GetRequestStream().Close()
			If Not isDELUXESuccessful(sXml) Then
				Return False ''fail ->exit
			End If
			If Not psHasJoint Then Return True '' there is no joint, just return true->the process is completed
			''======Run for joint==========
			Dim joint_RequestXMLString As String = BuildDELUXEXMLRequest(psLoanID, poConfig, True, "0")
			Dim joint_req As WebRequest = WebRequest.Create(psUrl)

			joint_req.Method = "POST"
			joint_req.ContentType = "text/xml"

			_log.Info(String.Format("Joint Preparing to POST Deluxe detect Url: {0}", psUrl))
			_log.Info(String.Format("Joint Preparing to POST Deluxe detect Data: {0}", CSecureStringFormatter.MaskSensitiveXMLData(joint_RequestXMLString)))

			Dim joint_streamWriter As New System.IO.StreamWriter(joint_req.GetRequestStream())
			joint_streamWriter.Write(joint_RequestXMLString)
			joint_streamWriter.Close()

			Dim joint_res As WebResponse = joint_req.GetResponse()

			Dim joint_reader As New System.IO.StreamReader(joint_res.GetResponseStream())
			Dim joint_sXml As String = joint_reader.ReadToEnd()

			_log.Info("XA:Joint Response from server(Deluxe): " & CSecureStringFormatter.MaskSensitiveXMLData(joint_sXml))
			joint_reader.Close()
			joint_res.GetResponseStream().Close()
			joint_req.GetRequestStream().Close()
			If Not isDELUXESuccessful(joint_sXml) Then
				Return False ''fail ->exit
			End If
		Catch ex As Exception
			_log.Error("XA: Deluxe Detect error): " & ex.Message, ex)
			Return False
		End Try

		Return True	 ''complete for both primary and joint

	End Function
    Public Function isDELUXESuccessful(ByVal responseXMLStr As String) As Boolean
        ' Return true if eFunds was successful
        Dim responseXMLDoc As XmlDocument = New XmlDocument()
        responseXMLDoc.LoadXml(responseXMLStr)
        ' Get the attribute from the XML response
        Dim responseAttribute As String = ""
        Dim responseAttributeNode As XmlNode = responseXMLDoc.SelectSingleNode("//@decision")
        If responseAttributeNode Is Nothing Then Return False
        responseAttribute = responseAttributeNode.InnerXml
        ' Compare the result with the proper success result
        If responseAttribute.ToUpper() = "PROCEED" Then
            Return True
        Else
            Return False
        End If
    End Function
    Protected Function BuildDELUXEXMLRequest(ByVal psLoanID As String, ByVal poConfig As CWebsiteConfig, ByVal psIsJoint As Boolean, ByVal psApplicantIndex As String) As String
        '' Build XML request
        Dim sb As New StringBuilder()
        Dim writer As XmlWriter = XmlTextWriter.Create(sb)
        writer.WriteStartDocument()

        ' begin input
        writer.WriteStartElement("INPUT")

        writer.WriteStartElement("LOGIN")
        writer.WriteAttributeString("api_user_id", poConfig.APIUser)
        writer.WriteAttributeString("api_password", poConfig.APIPassword)
        writer.WriteEndElement() 'end login

        writer.WriteStartElement("REQUEST")
        writer.WriteAttributeString("xpressappid", psLoanID)
        writer.WriteAttributeString("applicant_index", psApplicantIndex)
        If psIsJoint = False Then
            writer.WriteAttributeString("applicant_type", "PRIMARY")
        Else
            writer.WriteAttributeString("applicant_type", "JOINT")
        End If
        writer.WriteEndElement() 'end requst

        ' end input
        writer.WriteEndElement()
        writer.Flush()
        writer.Close()
        Dim returnStr = sb.ToString()
        Return returnStr
    End Function
    ''-----------------implement Deluxe for minor --------------------------------------------
    Public Function Execute_minor(ByVal psLoanID As String, ByVal psIsJoint As Boolean, ByVal poConfig As CWebsiteConfig, ByVal psUrl As String, ByVal sMinorDebitEnable As String, ByVal onlyMinorApp As Boolean) As Boolean
        Try
            ''run debit for minor if it 's enabled
            If sMinorDebitEnable = "Y" Then
                ''Begin post for minor applicant
                Dim minor_RequestXMLString As String = BuildDELUXEXMLRequest(psLoanID, poConfig, False, "0")
                Dim minor_req As WebRequest = WebRequest.Create(psUrl)

                minor_req.Method = "POST"
                minor_req.ContentType = "text/xml"

                _log.Info(String.Format("Minor Preparing to POST Deluxe detect Url: {0}", psUrl))
                _log.Info(String.Format("Minor Preparing to POST Deluxe detect Data: {0}", CSecureStringFormatter.MaskSensitiveXMLData(minor_RequestXMLString)))

                Dim minor_streamWriter As New System.IO.StreamWriter(minor_req.GetRequestStream())
                minor_streamWriter.Write(minor_RequestXMLString)
                minor_streamWriter.Close()

                Dim minor_res As WebResponse = minor_req.GetResponse()

                Dim minor_reader As New System.IO.StreamReader(minor_res.GetResponseStream())
                Dim minor_sXml As String = minor_reader.ReadToEnd()

                _log.Info("XA Minor: Response from server(Deluxe detect): " & CSecureStringFormatter.MaskSensitiveXMLData(minor_sXml))

                minor_reader.Close()
                minor_res.GetResponseStream().Close()
                minor_req.GetRequestStream().Close()
                If Not isDELUXESuccessful(minor_sXml) Then
                    Return False ''fail ->exit
                End If
            End If
            If Not onlyMinorApp Then
                ''Begin post for custodian applicant
                Dim RequestXMLString As String = BuildDELUXEXMLRequest(psLoanID, poConfig, False, "1")
                Dim req As WebRequest = WebRequest.Create(psUrl)

                req.Method = "POST"
                req.ContentType = "text/xml"

                _log.Info(String.Format("Custodian Preparing to POST Deluxe detect Url: {0}", psUrl))
                _log.Info(String.Format("Custodian Preparing to POST Deluxe detect Data: {0}", CSecureStringFormatter.MaskSensitiveXMLData(RequestXMLString)))

                Dim streamWriter As New System.IO.StreamWriter(req.GetRequestStream())
                streamWriter.Write(RequestXMLString)
                streamWriter.Close()

                Dim res As WebResponse = req.GetResponse()

                Dim reader As New System.IO.StreamReader(res.GetResponseStream())
                Dim sXml As String = reader.ReadToEnd()

                _log.Info("XA Custodian: Response from server(Deluxe detect): " & CSecureStringFormatter.MaskSensitiveXMLData(sXml))

                reader.Close()
                res.GetResponseStream().Close()
                req.GetRequestStream().Close()
                If Not isDELUXESuccessful(sXml) Then
                    Return False ''fail ->exit
                End If
                If Not psIsJoint Then Return True '' there is no joint, just return true->the process is completed
                ''======Run for custodian joint==========
                Dim joint_RequestXMLString As String = BuildDELUXEXMLRequest(psLoanID, poConfig, True, "1")
                Dim joint_req As WebRequest = WebRequest.Create(psUrl)

                joint_req.Method = "POST"
                joint_req.ContentType = "text/xml"

                _log.Info(String.Format("Custodian Joint Preparing to POST Deluxe detect Url: {0}", psUrl))
                _log.Info(String.Format(" Custodian Joint Preparing to POST Deluxe detect Data: {0}", CSecureStringFormatter.MaskSensitiveXMLData(joint_RequestXMLString)))

                Dim joint_streamWriter As New System.IO.StreamWriter(joint_req.GetRequestStream())
                joint_streamWriter.Write(joint_RequestXMLString)
                joint_streamWriter.Close()

                Dim joint_res As WebResponse = joint_req.GetResponse()

                Dim joint_reader As New System.IO.StreamReader(joint_res.GetResponseStream())
                Dim joint_sXml As String = joint_reader.ReadToEnd()

                _log.Info("XA Custodian:Joint Response from server(Deluxe): " & CSecureStringFormatter.MaskSensitiveXMLData(joint_sXml))
                joint_reader.Close()
                joint_res.GetResponseStream().Close()
                joint_req.GetRequestStream().Close()
                If Not isDELUXESuccessful(joint_sXml) Then
                    Return False ''fail ->exit
                End If
            End If
        Catch ex As Exception
            _log.Error("XA: Deluxe Detect error): " & ex.Message, ex)
            Return False
        End Try

        Return True  ''complete for both primary and joint

    End Function
    ''-----------------end implement Deluxe for minor --------------------------------------------

End Class
