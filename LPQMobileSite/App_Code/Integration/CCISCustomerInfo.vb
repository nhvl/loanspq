﻿Imports Microsoft.VisualBasic
Imports System.IO
Imports System.Xml
Imports System.Net
Public Class CCISCustomerInfo
    Public Property AccountList As New List(Of CCISCustomerAccount)
    Public Property IDCardList As New List(Of CCISCustomerIDCard)
    Public Property MemberNumber As String
    Public Property FName As String
    Public Property LName As String
    Public Property MName As String
    Public Property SSN As String
    Public Property DOB As String
    Public Property Email As String
    Public Property HomePhone As String
    Public Property MobilePhone As String
    Public Property WorkPhone As String
    Public Property Extension As String
    Public Property Street As String
    Public Property City As String
    Public Property Zip As String
    Public Property State As String
    Public Property MotherMaidenName As String
    Public Property Employer As String
    Public Property Occupation As String
    Public Property MonthlyIncome As String
End Class
Public Class CCISCustomerIDCard
    Public Property IdType As String
    Public Property IdNumber As String
    Public Property IdState As String
    Public Property IdCountry As String
    Public Property IdIssuedDate As String
    Public Property IdExpirationDate As String
End Class
Public Class CCISCustomerAccount
    Public Property AccountNumber As String
    Public Property Description As String
    Public Property Rate As String
    Public Property Balance As String
    Public Property ProductCode As String
End Class