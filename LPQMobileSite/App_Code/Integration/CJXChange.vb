﻿Imports Microsoft.VisualBasic
Imports System.Xml
Imports System.Net
Imports LPQMobile.BO
Imports LPQMobile.Utils
Imports System.Web.Script.Serialization
Imports System.Text
Public Class CJXChange
    Private _log As log4net.ILog = log4net.LogManager.GetLogger(GetType(CJXChange))

    Public Function BuildJXChangeRequestXML(ByVal oConfig As CWebsiteConfig, ByVal sNetTellerId As String) As String
        Dim sb As New StringBuilder()
        Dim writer As XmlWriter = XmlTextWriter.Create(sb)
        writer.WriteStartDocument()
        writer.WriteStartElement("INPUT")
        writer.WriteStartElement("LOGIN")
        writer.WriteStartElement("API_CREDENTIALS")
        writer.WriteAttributeString("api_user_id", oConfig.APIUser)
        writer.WriteAttributeString("api_password", oConfig.APIPassword)
        writer.WriteEndElement() ''end api_credential
        writer.WriteEndElement() 'end login
        writer.WriteStartElement("REQUEST")
        writer.WriteAttributeString("integration_type", "JXCHANGE")
        writer.WriteAttributeString("lender_code", oConfig.LenderCode)
        writer.WriteStartElement("SEARCHTYPE")
        writer.WriteValue("NETTELLER")
        writer.WriteEndElement() 'end searchtype
        writer.WriteStartElement("SEARCHVALUE")
        writer.WriteValue(sNetTellerId)
        writer.WriteEndElement() 'end searchvalue
        writer.WriteEndElement() 'end request
        ' end input
        writer.WriteEndElement()
        writer.Flush()
        writer.Close()
        Return sb.ToString()
    End Function
    Public Function getJXChangeResponse(ByVal oConfig As CWebsiteConfig, ByVal sNetTellerId As String, ByVal sUrl As String) As String
        ''testing purpose
        ''  Dim sJXChangeResponse = "<OUTPUT version="" 1.0""><RESULT status="" SUCCESS""/><RESPONSE><CONSUMER_INFOS><CONSUMER_INFO member_number="" SAA3456"" first_name="" TEST"" middle_name="" M"" last_name="" PERSON"" ssn="" 000000001"" dob="" 01/10/1990"" email=""test@test.com""><CORE_FIELDS emplName="" MERIDIANLINK"" occType="" ENGINEER"" comName="" TEST M PERSON""/><ADDRESSES><ADDRESS street_address_1="" 1234 TEST ST"" city="" COSTA MESA"" state="" CA"" zip="" 92626""><CORE_FIELDS/></ADDRESS></ADDRESSES><PHONES><PHONE phone_type="" Home Phone"" phone_number="" 4238675309""><CORE_FIELDS/></PHONE><PHONE phone_type="" Home Cell Phone"" phone_number="" 6261234567""><CORE_FIELDS/></PHONE><PHONE phone_type="" Business Fax Number"" phone_number="" 5551239875""><CORE_FIELDS/></PHONE></PHONES><ID_CARDS><ID_CARD card_type=""DRIVER_LICENSE"" card_number=""1111111111"" state=""CA"" country=""USA"" date_issued=""12/12/1999"" exp_date=""12/21/2020"" /></ID_CARDS><ACCOUNTS><ACCOUNT account_number="" 1234567890"" product_type="" D"" description="" Test Checking"" balance="" 229.82"" rate="" 0.00000000"" open_date="" 11/19/2012""><CORE_FIELDS prodCode="" L""/></ACCOUNT></ACCOUNTS><LOANS><LOAN account_number="" 9876543210"" product_type="" L"" description="" Test Loan"" original_balance="" 0.00"" balance="" 0.00"" open_date="" 11/21/2012""><CORE_FIELDS prodCode="" 3E""/></LOAN></LOANS></CONSUMER_INFO></CONSUMER_INFOS></RESPONSE></OUTPUT>"
        '' Return sJXChangeResponse
        Dim sXml As String = ""
        Try
            Dim sRequestXML = BuildJXChangeRequestXML(oConfig, sNetTellerId)
            Dim req As WebRequest = WebRequest.Create(sUrl)
            req.Method = "POST"
            req.ContentType = "text/xml"

            _log.Info(String.Format("Preparing to POST JXChangeCIS Url: {0}", sUrl))
            _log.Info(String.Format("Preparing to POST JXChangeCIS Data: {0}", CSecureStringFormatter.MaskSensitiveXMLData(sRequestXML)))

            Dim streamWriter As New System.IO.StreamWriter(req.GetRequestStream())
            streamWriter.Write(sRequestXML)
            streamWriter.Close()

            Dim res As WebResponse = req.GetResponse()
            Dim reader As New System.IO.StreamReader(res.GetResponseStream())
            sXml = reader.ReadToEnd()
            _log.Debug("JXChangeCIS response from server: " + sXml)
            ''_log.Info(String.Format("Response Data from JXChangeCIS: {0}", CSecureStringFormatter.MaskSensitiveXMLData(sXml)))
            reader.Close()
            res.GetResponseStream().Close()
            req.GetRequestStream().Close()
        Catch ex As Exception
            _log.Error("JXChangeCIS error: " & ex.Message, ex)
        End Try
        Return sXml
    End Function


End Class
