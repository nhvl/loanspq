﻿Imports Microsoft.VisualBasic
Imports System.Xml
Imports System.Net
Imports LPQMobile.BO
Imports System.IO
Imports LPQMobile.Utils
Imports System.Text
Public Class CBSADebit
    Private ReadOnly _log As log4net.ILog = log4net.LogManager.GetLogger(GetType(CBSADebit))
    Public SAApplicants As List(Of CSAApplicantInfo)
    Public BAApplicants As List(Of CBAApplicantInfo)
    Public oConfig As CWebsiteConfig
    Public RequestDebitUrl As String
    ''execute special account debit
    Public Function ExecuteSADebit(ByVal sLoanID As String) As Boolean
        Dim bExeDebitSuccess = True
        Try
            Dim sApplicantIndex As Integer = 0
            For Each sa In SAApplicants
                ''keep track applicant index(not joint applicant)
                If Common.SafeString(sa.IsJoint).ToUpper <> "Y" Then
                    sApplicantIndex += 1
                End If
                If sa.AllowDebit Then
                    Dim DebitXMLRequest As String = BuildDebitXMLRequest(sLoanID, sa.IsJoint, (sApplicantIndex - 1).ToString)
                    Dim sDebitXmlResponse = postDebit(DebitXMLRequest)
                    bExeDebitSuccess = IsExeDebitSuccess(sDebitXmlResponse)
                    If Not bExeDebitSuccess Then
                        Return bExeDebitSuccess ''stop running debit for other applicants
                    End If
                End If
            Next
        Catch ex As Exception
            _log.Error("XA: Debit): " & ex.Message, ex)
            Return False
        End Try
        Return bExeDebitSuccess
    End Function
    ''execute business xa debit
    Public Function ExecuteBADebit(ByVal sLoanID As String) As Boolean
        Dim bExeDebitSuccess = True
        Try
            Dim sApplicantIndex As Integer = 0
            For Each ba In BAApplicants
                ''keep track applicant index(not joint applicant)
                If Common.SafeString(ba.IsJoint).ToUpper <> "Y" Then
                    sApplicantIndex += 1
                End If
                If ba.AllowDebit Then
                    Dim DebitXMLRequest As String = BuildDebitXMLRequest(sLoanID, ba.IsJoint, (sApplicantIndex - 1).ToString)
                    Dim sDebitXmlResponse = postDebit(DebitXMLRequest)
                    bExeDebitSuccess = IsExeDebitSuccess(sDebitXmlResponse)
                    If Not bExeDebitSuccess Then
                        Return bExeDebitSuccess ''stop running debit for other applicants
                    End If
                End If
            Next
        Catch ex As Exception
            _log.Error("XA: Debit): " & ex.Message, ex)
            Return False
        End Try
        Return bExeDebitSuccess
    End Function
    Public Function postDebit(ByVal sDebitXmlRequest As String) As String
        Dim sXML As String = ""
        Try
            Dim req As WebRequest = WebRequest.Create(RequestDebitUrl)
            req.Method = "POST"
            req.ContentType = "text/xml"
            _log.Info(String.Format("Preparing to POST Debit({1}) Url: {0}", RequestDebitUrl, oConfig.DebitType))
            _log.Info(String.Format("Preparing to POST Debit({1}) Data: {0}", CSecureStringFormatter.MaskSensitiveXMLData(sDebitXmlRequest), oConfig.DebitType))

            Dim streamWriter As New System.IO.StreamWriter(req.GetRequestStream())
            streamWriter.Write(sDebitXmlRequest)
            streamWriter.Close()
            Dim res As WebResponse = req.GetResponse()
            Dim reader As New System.IO.StreamReader(res.GetResponseStream())
            sXML = reader.ReadToEnd()
            _log.Info(String.Format("XA: Response from server Debit({1}): {0}", CSecureStringFormatter.MaskSensitiveXMLData(sXML), oConfig.DebitType))
            reader.Close()
            res.GetResponseStream().Close()
            req.GetRequestStream().Close()
        Catch ex As Exception
            _log.Error("XA: Debit): " & ex.Message, ex)
            Return ""
        End Try

        Return sXML
    End Function
    Public Function BuildDebitXMLRequest(ByVal sLoanID As String, ByVal sIsJoint As String, ByVal sAppliantIndex As String) As String
        '' Build XML request
        Dim sb As New StringBuilder()
        Dim writer As XmlWriter = XmlTextWriter.Create(sb)
        writer.WriteStartDocument()
        ' begin input
        writer.WriteStartElement("INPUT")

        writer.WriteStartElement("LOGIN")
        writer.WriteAttributeString("api_user_id", oConfig.APIUser)
        writer.WriteAttributeString("api_password", oConfig.APIPassword)
        writer.WriteEndElement() 'end login
        writer.WriteStartElement("REQUEST")
        writer.WriteAttributeString("applicant_index", sAppliantIndex)
        If oConfig.DebitType = "EWS" Then ''Experian
            writer.WriteAttributeString("app_id", sLoanID)
            If sIsJoint = "Y" Then
                writer.WriteAttributeString("is_joint", "true")
            Else
                writer.WriteAttributeString("is_joint", "false")
            End If
        ElseIf oConfig.DebitType = "FIS" Or oConfig.DebitType = "DID" Or oConfig.DebitType = "EID" Then
            writer.WriteAttributeString("xpressappid", sLoanID)
            If sIsJoint = "Y" Then
                writer.WriteAttributeString("applicant_type", "JOINT")
            Else
                writer.WriteAttributeString("applicant_type", "PRIMARY")
            End If
        Else
            _log.Warn("Invalid Debit Type: " & oConfig.DebitType)
            Return ""
        End If
        writer.WriteEndElement() 'end requst
        ' end input
        writer.WriteEndElement()
        writer.Flush()
        writer.Close()
        Dim returnStr = sb.ToString()
        Return returnStr
    End Function
    Public Function IsExeDebitSuccess(ByVal responseXMLstr As String) As Boolean
        Select Case oConfig.DebitType.ToUpper
            Case "FIS" '' -->> for efund
                Dim oEFund As CDebitFIS = New CDebitFIS()
                Return oEFund.isEFundsSuccessful(responseXMLstr)
            Case "EID"  '' --> for retailbanking
                Dim oRetailBanking As CRetailBankingEID = New CRetailBankingEID()
                Return oRetailBanking.isRBankingSuccessful(responseXMLstr)
            Case "DID"   '' --> for Deluxe detec
                Dim oDeluxe As CDeluxe = New CDeluxe()
                Return oDeluxe.isDELUXESuccessful(responseXMLstr)
            Case "EWS"   '' --> for Experian Precise EWS
                Dim oExperianEWS As CExperianEWS = New CExperianEWS()
                Return oExperianEWS.isEWSSuccessful(responseXMLstr)
        End Select
        Return False
    End Function

End Class
