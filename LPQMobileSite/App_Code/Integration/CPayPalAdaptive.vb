﻿Imports Microsoft.VisualBasic
Imports PayPal.AdaptivePayments
Imports PayPal.AdaptivePayments.Model
Imports PayPal.Api

Public Class CPayPalAdaptive
	Private _log As log4net.ILog = log4net.LogManager.GetLogger(GetType(CPayPalAdaptive))
	Public Function CreatePayment(items As List(Of Item), senderEmail As String, receiverEmail As String, callbackURL As String, psDescription As String) As CJsonResponse

		Dim totalAmount As Decimal = items.Select(Function(x) Decimal.Parse(x.price)).Sum()

		Dim receiverList As New PayPal.AdaptivePayments.Model.ReceiverList()

		receiverList.receiver = New List(Of Receiver)()
		Dim receiver As New Receiver(totalAmount)
		receiver.email = receiverEmail
		receiverList.receiver.Add(receiver)

		Dim returnUrl As String = ""
		Dim cancelUrl As String = ""

		'' build redirect URLs
		'' check url has ? yet (first param)
		If callbackURL.IndexOf("?") < 0 Then
			returnUrl = callbackURL + "?success=true"
			cancelUrl = callbackURL + "?cancel=true"
		Else '' url already had first param
			returnUrl = callbackURL + "&success=true"
			cancelUrl = callbackURL + "&cancel=true"
		End If

		Dim payRequest = New PayRequest(New RequestEnvelope("en_US"), "PAY", cancelUrl, "USD", receiverList, returnUrl)
		payRequest.senderEmail = senderEmail
		'for email feature, 
		payRequest.sender = New SenderIdentifier()
		'payRequest.sender.email = senderEmail
		Dim sMemo As String = "Deposit for " 'only visible in detail payment view on payer side, and review payment page
		For i = 0 To items.Count - 1
			sMemo += items(i).name + " $" + items(i).price + "; "
		Next
		payRequest.memo = sMemo

		Dim configXML As Dictionary(Of String, String) = CPayPalConfig.GetConfig()
		Dim sdkConfig As New Dictionary(Of String, String)()

		sdkConfig.Add("mode", configXML("mode"))
		sdkConfig.Add("account1.apiUsername", configXML("apiUsername"))
		sdkConfig.Add("account1.apiPassword", configXML("apiPassword"))
		sdkConfig.Add("account1.apiSignature", configXML("apiSignature"))
		sdkConfig.Add("account1.applicationId", configXML("applicationId"))

		Dim adaptivePaymentsService As New AdaptivePaymentsService(sdkConfig)
		'This is reuqired, PayPal have started to drop support for SSLv3
		Net.ServicePointManager.SecurityProtocol = Net.SecurityProtocolType.Ssl3 Or Net.SecurityProtocolType.Tls12
		Dim payResponse As PayResponse = adaptivePaymentsService.Pay(payRequest)

		'http://stackoverflow.com/questions/18463476/paypal-payment-summery-doesnt-show-items-set-by-setpaymentoptions
		'add items for detail/invoice, doesn't display in payment page, display in payment detail page.
		If payResponse IsNot Nothing And payResponse.responseEnvelope.ack.ToString().Trim().ToUpper().Equals("SUCCESS") Then
			Dim payOptionsReq As New SetPaymentOptionsRequest(New RequestEnvelope("en_US"), payResponse.payKey)
			'payOptionsReq.displayOptions = New DisplayOptions() With {.businessName = "test Business Name"}	'paypal can derive from email
			payOptionsReq.payKey = payResponse.payKey
			payOptionsReq.requestEnvelope = New RequestEnvelope("en_US")
			Dim oInvoiceItems As New List(Of Model.InvoiceItem)
			For i As Integer = 0 To items.Count - 1
				Dim oInvoiceItem As New Model.InvoiceItem()
				oInvoiceItem.identifier = items(i).name
				oInvoiceItem.name = i + 1
				oInvoiceItem.itemCount = items(i).quantity
				oInvoiceItem.itemPrice = items(i).price
				oInvoiceItem.price = items(i).price
				oInvoiceItem.name = items(i).name
				oInvoiceItems.Add(oInvoiceItem)
			Next
			'description is displayed in note on seller side
			payOptionsReq.receiverOptions = New List(Of ReceiverOptions)() From { _
				New ReceiverOptions() With {.receiver = New ReceiverIdentifier() With {.email = receiverEmail}, _
				.description = psDescription, _
				.invoiceData = New InvoiceData() With {.item = oInvoiceItems, .totalShipping = 0, .totalTax = 0} _
				} _
			}
			Dim r As SetPaymentOptionsResponse = adaptivePaymentsService.SetPaymentOptions(payOptionsReq)
			If r.error.Count > 0 Then
				_log.Warn("SetPaymentOptionsRequest Failed: " & r.error(0).message)
			End If
		End If

		' The status of the payment. Possible values are:
		' CREATED – The payment request was received; funds will be transferred once the payment is approved
		' COMPLETED – The payment was successful
		' INCOMPLETE – Some transfers succeeded and some failed for a parallel payment or, for a delayed chained payment, secondary receivers have not been paid
		' ERROR – The payment failed and all attempted transfers failed or all completed transfers were successfully reversed
		' REVERSALERROR – One or more transfers failed when attempting to reverse a payment
		' PROCESSING – The payment is in progress
		' PENDING – The payment is awaiting processing
		If payResponse.paymentExecStatus = "CREATED" Then
			Return New CJsonResponse(True, "MLPAYPAL_CREATED", New With {.URL = configXML("authorizeURL") + payResponse.payKey}) 'MLPAYPAL keyword is used by xa/script.js saveUserInfo or submitWalletAnswers
		Else
			_log.Warn("Create Paypal Payment Failed: " & payResponse.error(0).message)
			Return New CJsonResponse(False, "MLPAYPAL_FAILED", New With {.STATUS = payResponse.paymentExecStatus, .APPROVED_MESSAGE = payResponse.error(0).message})
		End If
	End Function

End Class
