﻿Imports Microsoft.VisualBasic
Imports System.Xml
Imports System.Net
Imports LPQMobile.BO
Imports LPQMobile.Utils
Imports System.Text
''' <summary>
''' This is a generic classs that will be use for booking to various core from now
''' </summary>
''' <remarks></remarks>
Public Class CBookCore
	Private _log As log4net.ILog = log4net.LogManager.GetLogger(GetType(CBookCore))
	Public Sub New()

	End Sub

    Public Function Book(ByVal psLoanID As String, poConfig As CWebsiteConfig, ByVal psUrl As String, ByVal psAppType As String) As String
        Dim sAccountNumber As String = ""
        Dim sIntegrationType = ""
        ''================================
        '' Build XML request
        '		<INPUT>
        '	<LOGIN>
        '		<API_CREDENTIALS api_user_id="userid" api_password="password" />
        '	</LOGIN>
        '	<REQUEST integration_type="PATHWAY">
        '		<BOOK_XA_APP>
        '			<APPLICATION>
        '				<EXISTING_APP app_id="1234567890123456789012" app_type="XA" />
        '			</APPLICATION>
        '		</BOOK_XA_APP>
        '	</REQUEST>
        '</INPUT>
        Dim sb As New StringBuilder()
        Dim writer As XmlWriter = XmlTextWriter.Create(sb)
        writer.WriteStartDocument()

        ' begin input
        writer.WriteStartElement("INPUT")

        writer.WriteStartElement("LOGIN")
        writer.WriteStartElement("API_CREDENTIALS")
        writer.WriteAttributeString("api_user_id", poConfig.APIUser)
        writer.WriteAttributeString("api_password", poConfig.APIPassword)
        writer.WriteEndElement() 'end API_CREDENTIALS 
        writer.WriteEndElement() 'end login
        writer.WriteStartElement("REQUEST")
        If psAppType = "XA" Or psAppType.Contains("COMBO") Then
            writer.WriteAttributeString("integration_type", poConfig.Booking)
            ''if booking type is "CMC" then using "BOOK_CMC_XA_APP" instead of "BOOK_XA_APP"
            Dim sBookXAApp = "BOOK_XA_APP"
            If poConfig.Booking = "CMC" Then sBookXAApp = "BOOK_CMC_XA_APP"
            writer.WriteStartElement(sBookXAApp)
        Else 'loan booking
            Dim sLongLoantype = Common.GetConfigLoanTypeFromShort(psAppType)
            sIntegrationType = Common.getNodeAttributes(poConfig, sLongLoantype, "loan_booking")

            writer.WriteAttributeString("integration_type", sIntegrationType)
            writer.WriteStartElement("BOOK_LPQ_APP")

        End If

        writer.WriteStartElement("APPLICATION")
        writer.WriteStartElement("EXISTING_APP")
        writer.WriteAttributeString("app_id", psLoanID)
        writer.WriteAttributeString("app_type", psAppType)
        writer.WriteEndElement() 'end EXISTING_APP
        writer.WriteEndElement() 'end application

        If psAppType <> "XA" And Not psAppType.Contains("COMBO") Then
            If sIntegrationType <> "TMG" Then
                writer.WriteStartElement("BOOKING_FIELDS")
                writer.WriteEndElement() 'end BOOKING_FIELDS
            End If
        End If

        writer.WriteEndElement() 'end BOOK
        writer.WriteEndElement() 'end REQUEST

        writer.WriteEndElement() ' end input
        writer.Flush()
        writer.Close()

        Dim RequestXMLString = sb.ToString()
        ''==================
        ''Begin post
        Try
            Dim req As WebRequest = WebRequest.Create(psUrl)
            req.Timeout = 260000 '4.3min OPENPATH takes as least 2min

            req.Method = "POST"
            req.ContentType = "text/xml"

            _log.InfoFormat("Preparing to POST to core ({1}) booking Url: {0}", psUrl, sIntegrationType)
            _log.InfoFormat("Preparing to POST booking Data: {0}", CSecureStringFormatter.MaskSensitiveXMLData(RequestXMLString))

            Dim streamWriter As New System.IO.StreamWriter(req.GetRequestStream())
            streamWriter.Write(RequestXMLString)
            streamWriter.Close()

            Dim res As WebResponse = req.GetResponse()

            Dim reader As New System.IO.StreamReader(res.GetResponseStream())
            Dim sXml As String = reader.ReadToEnd()

            '==Expect response
            '<OUTPUT version="1.0"><RESPONSE status="SUCCESS"><MemberNumber>182429</MemberNumber><Suffix>28</Suffix></RESPONSE></OUTPUT>
            '<OUTPUT version="1.0"><ERRORS><ERROR type="INTERNAL_ERROR">Unable to book app due to the following pre-booking validation errors: Validation Error</ERROR></ERRORS><RESPONSE status="FAILURE" /></OUTPUT>
            _log.DebugFormat("Booking response from Core {0}: {1}", sIntegrationType, sXml)

            reader.Close()
            res.GetResponseStream().Close()
            req.GetRequestStream().Close()
            sAccountNumber = GetAccountNumber(sXml, poConfig, sIntegrationType)
        Catch ex As Exception
            _log.Error("Booking error: " & ex.Message, ex)
        End Try

        Return sAccountNumber

    End Function

    Protected Function GetAccountNumber(ByVal psResponseXml As String, ByVal poConfig As CWebsiteConfig, ByVal sIntegrationType As String) As String
        Dim xmlDoc As New XmlDocument()
        Dim sElementName = "MemberNumber"
        If sIntegrationType = "TMG" Then
            ''sample response format for Booking(TMG) "<OUTPUT version=""1.0""><RESPONSE status=""SUCCESS""><AccountNumber>123456789</AccountNumber></RESPONSE></OUTPUT>"
            sElementName = "AccountNumber"
        End If
        xmlDoc.LoadXml(psResponseXml)
        If (xmlDoc.GetElementsByTagName("RESPONSE").Count > 0 AndAlso xmlDoc.GetElementsByTagName("RESPONSE")(0).Attributes("status").InnerXml.NullSafeToUpper_ <> "SUCCESS") Then
            _log.Warn("Booking error: status is not success", Nothing)
            Return ""
        End If

        If xmlDoc.GetElementsByTagName(sElementName).Count > 0 AndAlso xmlDoc.GetElementsByTagName(sElementName)(0).InnerText.Length > 0 Then
            Return xmlDoc.GetElementsByTagName(sElementName)(0).InnerText  'pass

        ElseIf poConfig.Booking = "SERVICEBUS" Then  'TODO: need to change to enum later
            Return "bogus12333"  'SERVICEBUS only return "Success" status.  we put a bogus# here so we don't have to change upstream code 
        Else
            _log.Warn("Booking error: no " & sElementName, Nothing)
            Return "" 'false
        End If

    End Function
End Class
