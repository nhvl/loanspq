﻿Imports Microsoft.VisualBasic
Imports System.Xml
Imports System.Net
Imports LPQMobile.BO
Imports LPQMobile.Utils
Imports System.Text

Public Class CSymitar
	Private _log As log4net.ILog = log4net.LogManager.GetLogger(GetType(CSymitar))
	Public Sub New()
		
	End Sub

	Public Function Book(ByVal psLoanID As String, poConfig As CWebsiteConfig, ByVal psUrl As String) As String
		Dim sAccountNumber As String = ""

		''================================
		'' Build XML request
		Dim sb As New StringBuilder()
		Dim writer As XmlWriter = XmlTextWriter.Create(sb)
		writer.WriteStartDocument()

		' begin input
		writer.WriteStartElement("INPUT")

		writer.WriteStartElement("LOGIN")
		writer.WriteAttributeString("api_user_id", poConfig.APIUser)
		writer.WriteAttributeString("api_password", poConfig.APIPassword)
		writer.WriteEndElement() 'end login

		writer.WriteStartElement("XML")
		writer.WriteStartElement("APPLICATION")
		writer.WriteStartElement("MeridianLinkAppID")
		writer.WriteString(psLoanID)

		writer.WriteEndElement() 'end MeridianLinkAppID
		writer.WriteEndElement() 'end application
		writer.WriteEndElement() 'end xml

		' end input
		writer.WriteEndElement()
		writer.Flush()
		writer.Close()

		Dim RequestXMLString = sb.ToString()


		''==================
		''Begin post
		Try

		
		Dim req As WebRequest = WebRequest.Create(psUrl)

		req.Method = "POST"
		req.ContentType = "text/xml"

			_log.Info(String.Format("Preparing to POST Symitar booking Url: {0}", psUrl))
			_log.Info(String.Format("Preparing to POST Symitar booking Data: {0}", CSecureStringFormatter.MaskSensitiveXMLData(RequestXMLString)))

		Dim streamWriter As New System.IO.StreamWriter(req.GetRequestStream())
		streamWriter.Write(RequestXMLString)
		streamWriter.Close()

		Dim res As WebResponse = req.GetResponse()

		Dim reader As New System.IO.StreamReader(res.GetResponseStream())
		Dim sXml As String = reader.ReadToEnd()

			_log.Debug("Booking response from server(Symitar): " + sXml)

		reader.Close()
		res.GetResponseStream().Close()
			req.GetRequestStream().Close()

			sAccountNumber = GetAccountNumber(sXml)
		Catch ex As Exception
			_log.Error("Booking error: " & ex.Message, ex)
		End Try

		Return sAccountNumber

	End Function

	Protected Function GetAccountNumber(ByVal psResponseXml As String) As String
		Dim xmlDoc As New XmlDocument()

		xmlDoc.LoadXml(psResponseXml)
		If (xmlDoc.GetElementsByTagName("Result").Count > 0 AndAlso xmlDoc.GetElementsByTagName("Result")(0).InnerText.NullSafeToUpper_ <> "SUCCESS") Then
			_log.Debug("Booking error: Result is not success", Nothing)
			Return ""
		End If

		If xmlDoc.GetElementsByTagName("AccountNumber").Count > 0 andalso xmlDoc.GetElementsByTagName("AccountNumber")(0).InnerText.Length > 0 Then
			Return xmlDoc.GetElementsByTagName("AccountNumber")(0).InnerText  'pass
		Else
			_log.Warn("Booking error: no account number", Nothing)
			Return "" 'false
		End If

	End Function
End Class
