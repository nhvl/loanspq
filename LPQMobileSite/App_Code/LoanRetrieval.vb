﻿Imports LPQMobile.BO
Imports LPQMobile.Utils
Imports System.Xml
Imports System.Xml.Serialization
Imports System.Threading
Imports System.IO
Imports System.Net
Imports System.Text

Public Class LoanRetrieval
    Public Shared Function BuildGetLoansXML(ByVal loanID As String, ByVal apiUser As String, ByVal apiPassword As String) As String
        Dim sb As New StringBuilder
        Dim writer As XmlWriter = XmlTextWriter.Create(sb)

        writer.WriteStartDocument()
        writer.WriteStartElement("INPUT")

        'begin login
        writer.WriteStartElement("LOGIN")
        writer.WriteAttributeString("api_password", apiPassword)
        writer.WriteAttributeString("api_user_id", apiUser)
        writer.WriteEndElement()

        '' begin REQUEST
        writer.WriteStartElement("REQUEST")

        ' Begin LOAN
        writer.WriteStartElement("LOAN")
        writer.WriteAttributeString("loan_id", loanID)
        ' end LOAN
        writer.WriteEndElement()

        ' end REQUEST
        writer.WriteEndElement()

        ' End INPUT
        writer.WriteEndElement()
        writer.Flush()
        writer.Close()

        Dim xmlStr As String = ""

        xmlStr = sb.ToString()

        Return xmlStr
    End Function
End Class
