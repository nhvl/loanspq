﻿Imports Microsoft.VisualBasic

Public Interface IBuildWalletAnswers
	Function BuildWalletAnswerXMLEquifax(answerIDList As List(Of String)) As String
	Function BuildWalletAnswerXMLRSA(questionSetID As String, answerIDList As List(Of String)) As String
	Function BuildWalletAnswerXMLPID(answerIDList As List(Of String)) As String

	Function BuildWalletAnswerXMLFIS(questionSetID As String, answerIDList As List(Of String)) As String
	Function BuildWalletAnswerXMLDID(ByVal coAppFollowOnQuestion As Boolean, requestAppID As String, requestClientID As String, requestSequenceID As String, answerIDList As List(Of String)) As String
	Function BuildWalletAnswerXMLTID(referenceNumber As String, answerIDList As List(Of String)) As String
End Interface
