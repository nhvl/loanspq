﻿Imports System.Runtime.CompilerServices
Imports System.IO
Imports Microsoft.VisualBasic
Imports LPQMobile.Utils
Imports System.Xml
Imports System.Xml.Linq
Imports System.Web.Security.AntiXss
Imports System.Text.RegularExpressions
Imports System.Web

Public Module ObjectExtensions

	'http://www.extensionmethod.net/vb


	<Extension()> _
	Public Function NullSafeToUpper_(obj As Object) As String
		Return Common.SafeString(obj).ToUpper
	End Function

	<Extension()> _
	Public Function NullSafeToLower_(obj As Object) As String
		Return Common.SafeString(obj).ToLower
	End Function

	<Extension()>
	Public Function StripHTML(ByVal input As String) As String
		Return Regex.Replace(input, "<.*?>", String.Empty)
	End Function
	<Extension()>
	Public Function Truncate(ByVal input As String, length As Integer, Optional suffix As String = "...") As String
		If Not String.IsNullOrEmpty(input) AndAlso input.Length > length Then
			input = input.Substring(0, length - suffix.Length) & suffix
		End If
		Return input
	End Function


	''' <summary>
	''' Return query string that is encoded to prevent XSS
	''' Input can be a single query parameter, or list of parameters
	''' 
	''' </summary>
	''' <param name="psQueryString"></param>
	''' <param name="pbCaseSensitive"></param>
	''' <returns></returns>
	''' <remarks> on mouseover test case: GET /apply.aspx?lenderref=LPQPenTest&x86f9'onmouseover='alert(String.fromCharCode(88,83,83))'bygfk=1 – this produce the xss </remarks>
	<Extension()>
	Public Function safeQueryString(ByVal psQueryString As String, Optional ByVal pbCaseSensitive As Boolean = False) As String

		Dim sProcessedQueryString As String = ""
		Dim oComparer As StringComparer = CURLBuilder.IIfStrict(pbCaseSensitive, StringComparer.Ordinal, StringComparer.OrdinalIgnoreCase)
		Dim oQueryVars As New Generic.Dictionary(Of String, IList(Of String))(oComparer)

		If psQueryString.StartsWith("?") Then
			psQueryString = Right(psQueryString, psQueryString.Length - 1)
		End If

		Dim sQueryVars As String() = psQueryString.Split("&"c)
		For Each sQueryVar As String In sQueryVars
			Dim sQueryVarPair As String() = sQueryVar.Split("="c)
			Dim sVar As String = HttpUtility.UrlDecode(sQueryVarPair(0)).StripHTML
			sVar = AntiXssEncoder.HtmlEncode(sVar, True)
			Dim sValue As String = String.Empty
			If sQueryVarPair.Length > 1 Then 'Check in case "=" is missing
				sValue = HttpUtility.UrlDecode(sQueryVarPair(1)).StripHTML
				sValue = AntiXssEncoder.HtmlEncode(sValue, True)
			End If
			sProcessedQueryString &= "&" & sVar & "=" & sValue
		Next

		Return Right(sProcessedQueryString, sProcessedQueryString.Length - 1)
	End Function



#Region "XML Utilities"
	''' <summary>
	''' Returns a System.XML.XmlDocument with the same content as this System.Xml.Linq.XDocument
	''' </summary>
	''' <remarks>
	''' This conversion requires re-parsing the document, which may have performance implications.
	''' </remarks>
	<Extension()> _
	Public Function ToXmlDocument_(ByVal pXDocument As XDocument) As XmlDocument
		Dim xmlDocument As New XmlDocument()

		Using reader As XmlReader = pXDocument.CreateReader()
			xmlDocument.Load(reader)
		End Using

		Return xmlDocument
	End Function
	''' <summary>
	''' Returns a System.XML.XmlDocument with the same content as this System.Xml.Linq.XDocument
	''' </summary>
	''' <remarks>
	''' This conversion requires re-parsing the document, which may have performance implications.
	''' </remarks>
	<Extension()> _
	Public Function ToXmlDocument_(ByVal pXDocument As XElement) As XmlDocument
		Dim xmlDocument As New XmlDocument()

		Using reader As XmlReader = pXDocument.CreateReader()
			xmlDocument.Load(reader)
		End Using

		Return xmlDocument
	End Function

	''' <summary>
	''' Returns a System.Xml.Linq.XDocument with the same content as this System.XML.XmlDocument
	''' </summary>
	''' <remarks>
	''' This conversion requires re-parsing the document, which may have performance implications.
	''' </remarks>
	<Extension()> _
	Public Function ToXDocument_(ByVal pXmlDocument As XmlDocument) As XDocument
		Using nodeReader As New XmlNodeReader(pXmlDocument)
			nodeReader.MoveToContent()
			Return XDocument.Load(nodeReader)
		End Using
	End Function

	<Extension()> _
	Public Function GetAttributeValue_(pXElement As XElement, pAttributeName As String) As String
		If pXElement Is Nothing Then
			Return ""
		End If
		If pXElement.Attribute(pAttributeName) Is Nothing Then
			Return ""
		End If
		Return Common.SafeString(pXElement.Attribute(pAttributeName).Value)
	End Function

	<Extension()> _
	Public Function GetElementValue_(pXElement As XElement) As String
		If pXElement Is Nothing Then
			Return ""
		End If

		Return Common.SafeString(pXElement.Value)
	End Function
#End Region
End Module
