﻿Imports System.Runtime.CompilerServices
Imports System.IO
Imports System.Web
Imports System.Web.UI
Imports Microsoft.VisualBasic
Public Module UserControlExtensions

	<Extension()> _
	Public Function RenderHtml_(control As UserControl) As String
		Using sw As New StringWriter
			Dim page As New Page()
			page.Controls.Add(control)
			HttpContext.Current.Server.Execute(page, sw, False)
			Return sw.ToString()
		End Using
	End Function
End Module
