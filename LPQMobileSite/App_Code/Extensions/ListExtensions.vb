﻿Imports Microsoft.VisualBasic
Imports System.Runtime.CompilerServices



Public Module ListExtensions

	'http://www.extensionmethod.net/vb

	<Extension()>
	Public Function ToSerializableList_(Of T)(lst As IOrderedEnumerable(Of T)) As SerializableList(Of T)
		If lst Is Nothing Then Return Nothing
		Dim result As New SerializableList(Of T)
		result.AddRange(lst)
		Return result
	End Function

	Public Structure IndexedItem(Of T)
		Public Item As T
		Public Index As Integer
	End Structure

	<Extension()>
	Public Function IndexedItems(Of T)(lst As IEnumerable(Of T)) As IEnumerable(Of IndexedItem(Of T))
		If lst Is Nothing Then Return New List(Of IndexedItem(Of T))(0)

		Return lst.Select(Function(item, index) New IndexedItem(Of T) With {
							  .Item = item,
							  .Index = index
							  })
	End Function
End Module
