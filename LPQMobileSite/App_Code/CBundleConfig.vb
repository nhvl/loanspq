﻿Imports System.Web.Optimization
Imports System.Configuration

Public Class CBundleConfig
	Public Shared Sub RegisterBundles(bundles As BundleCollection)
		Dim useMinified As Boolean = True
        ''this line of code cause error "Request is not available in this context". Don't know why it's placed here
        'Dim useMinified As Boolean = HttpContext.Current.Request.QueryString("IsMini") <> "N"
        Dim sEnv As String = ConfigurationManager.AppSettings.Get("ENVIRONMENT")  'TODO: convert to enum
        If (sEnv = "LIVE" Or sEnv = "TEST" Or sEnv = "STAGE") And useMinified Then
			BundleTable.EnableOptimizations = True
		End If

        bundles.Add(New CCustomStyleBundle("~/css/thirdparty/bootstrap").Include(
			"~/css/ThirdParty/bootstrap-fileupload.css",
			"~/css/ThirdParty/bootstrap.css"))

		bundles.Add(New CCustomStyleBundle("~/css/thirdparty/custom").Include(
			"~/css/ThirdParty/custom.css"))

        bundles.Add(New CCustomStyleBundle("~/css/thirdparty/misc").Include(
            "~/css/ThirdParty/intlTelInput.css",
            "~/css/font-awesome/css/font-awesome.min.css",
            "~/css/text-security-font/text-security.min.css",
            "~/js/noty/animate.css",
            "~/css/ThirdParty/jquery-ui.min.css"))

        bundles.Add(New CCustomStyleBundle("~/css/default").Include(
            "~/css/DefaultStyle.css"))

        'virtual dir need to be css or .net will not generate optimize file
        bundles.Add(New CCustomStyleBundle("~/css/smcss").Include(
                "~/sm/content/bootstrap/css/bootstrap.css",
                "~/sm/content/bootstrap/css/glyphicons-font.css",
                "~/sm/content/bootstrap/css/bootstrap-theme.css",
                "~/sm/content/bootstrap/css/bootstrap-dialog.min.css",
                "~/sm/content/bootstrap/css/bootstrap-datepicker.min.css",
                "~/sm/content/font-awesome/css/font-awesome.css",
                "~/sm/js/dropzone/dropzone.css",
                "~/sm/js/noty/animate.css",
                "~/sm/js/tagsinput/bootstrap.tagsinput.css",
                "~/sm/js/querybuilder/query-builder.default.css",
                "~/sm/content/plugins.css",
                "~/sm/content/menu.css",
                "~/sm/content/page.css",
                "~/sm/content/main.css",
                "~/sm/content/bootstrap3Migration.css"))

        bundles.Add(New CCustomStyleBundle("~/css/lpcss").Include(
                "~/sm/content/bootstrap/css/bootstrap.css",
                "~/sm/content/bootstrap/css/glyphicons-font.css",
                "~/sm/content/bootstrap/css/bootstrap-theme.css",
                "~/sm/content/bootstrap/css/bootstrap-dialog.min.css",
                "~/Sm/content/vendors/grapesjs/css/grapes.min.css",
                "~/Sm/content/vendors/grapesjs/css/grapesjs-preset-webpage.min.css"
            ))



        bundles.Add(New CCustomStyleBundle("~/css/agentcss").Include(
                "~/agent/content/bootstrap/css/bootstrap.css",
                "~/agent/content/bootstrap/css/bootstrap-theme.css",
                "~/agent/content/bootstrap/css/bootstrap-dialog.min.css",
                "~/agent/content/bootstrap/css/bootstrap-datepicker.min.css",
                "~/agent/content/font-awesome/css/font-awesome.css",
                "~/agent/js/dropzone/dropzone.css",
                "~/agent/js/noty/animate.css",
                "~/agent/content/plugins.css",
                "~/agent/content/page.css",
                "~/agent/content/main.css"
            ))

        bundles.Add(New CCustomScriptBundle("~/js/lib").Include(
            "~/js/jquery-1.12.4.js",
            "~/js/BrowserError.js",
            "~/js/pageheader.js",
            "~/js/jquery.numeric.js",
            "~/js/jquery.card.js",
            "~/js/jquery.alphanum.js",
            "~/js/notify.js",
            "~/js/noty/jquery.noty.packaged.min.js",
            "~/js/lpq-validator.js",
            "~/js/jquery.mobile-1.4.5.js",
            "~/js/jquery-ui.min.js",
            "~/js/moment.min.js",
            "~/js/jquery.inputmask.bundle.js",
            "~/js/jquery.watermark.min.js",
            "~/js/lodash.js",
            "~/js/jquery.maskedinput-merged.js",
            "~/js/json2.js",
            "~/js/common.js",
            "~/js/employment.logic.js",
            "~/js/jquery.ap-smart-money-input.js"))


        bundles.Add(New CCustomScriptBundle("~/accu/script").Include(
			"~/accu/script.js"))
		bundles.Add(New CCustomScriptBundle("~/cc/script").Include(
			"~/cc/script.js"))
		bundles.Add(New CCustomScriptBundle("~/cu/script").Include(
			"~/cu/script.js"))
		bundles.Add(New CCustomScriptBundle("~/he/script").Include(
			"~/he/script.js"))
		bundles.Add(New CCustomScriptBundle("~/pl/script").Include(
			"~/pl/script.js"))
		bundles.Add(New CCustomScriptBundle("~/vl/script").Include(
			"~/vl/script.js"))
		bundles.Add(New CCustomScriptBundle("~/xa/script").Include(
			"~/xa/script.js"))
		bundles.Add(New CCustomScriptBundle("~/zp/script").Include(
			"~/zp/script.js"))
        bundles.Add(New CCustomScriptBundle("~/bl/script").Include(
        "~/bl/script.js"))

		bundles.Add(New CCustomScriptBundle("~/js/pagefooter").Include(
			"~/js/ThirdParty/scandit-sdk/build/browser/index.min.js",
			"~/js/pagefooter.js"))
		bundles.Add(New CCustomScriptBundle("~/js/thirdparty/funding").Include(
			"~/js/thirdparty/funding_controller.js"))
		bundles.Add(New CCustomScriptBundle("~/js/thirdparty/product").Include(
			"~/js/thirdparty/product_selection_controller.js"))

		bundles.Add(New CCustomScriptBundle("~/xa/product").Include(
			"~/xa/xa_funding_controller.js",
			"~/xa/xa_product_selection_controller.js"))

		bundles.Add(New CCustomScriptBundle("~/js/newdoccapture").Include(
			"~/js/ThirdParty/newDocCapture.js"))

		bundles.Add(New CCustomScriptBundle("~/js/apply").Include(
				"~/js/jquery.mobile-1.4.5.js",
				"~/js/notify.js",
				"~/js/lodash.js",
				"~/js/lpq-validator.js"))

		bundles.Add(New CCustomScriptBundle("~/js/newdocumentscan").Include(
			"~/js/ThirdParty/combined_third_party.js",
			"~/js/ThirdParty/newDocumentScan.js"))

		bundles.Add(New CCustomScriptBundle("~/js/thirdparty/intltelinput/script").Include(
			"~/js/ThirdParty/intlTelInput/intlTelInput.js"))

		bundles.Add(New CCustomScriptBundle("~/js/smscript").Include(
				"~/sm/js/jquery-3.4.1.min.js",
				"~/js/BrowserError.js",
				"~/sm/js/jquery.serializejson.min.js",
                "~/sm/js/bootstrap/bootstrap.bundle.min.js",
				"~/sm/js/bootstrap/bootstrap-dialog.min.js",
				"~/sm/js/bootstrap/bootstrap-datepicker.js",
				"~/sm/js/bootstrap/bootstrap-typeahead.js",
				"~/sm/js/noty/jquery.noty.packaged.min.js",
				"~/sm/js/dropzone/dropzone.js",
				"~/sm/js/notify/notify.min.js",
				"~/sm/js/notify/smvalidator.js",
				"~/sm/js/lodash.js",
				"~/sm/js/pace/pace.min.js",
				"~/sm/js/bootpag/jquery.bootpag.min.js",
				"~/sm/js/tagsinput/bootstrap.tagsinput.js",
				"~/sm/js/jquery.scrollbar.js",
				"~/sm/js/moment.min.js",
				"~/sm/js/common.js",
				"~/sm/js/main.js"
			))

        bundles.Add(New CCustomScriptBundle("~/js/lpscript").Include(
                "~/Sm/js/jquery-1.12.4.js",
                "~/sm/js/bootstrap/bootstrap.bundle.min.js",
                "~/sm/js/bootstrap/bootstrap-dialog.min.js",
                "~/Sm/js/common.js",
                "~/Sm/js/grapesjs/grapes.min.js",
                "~/Sm/js/grapesjs/plugins/grapesjs-blocks-basic.min.js",
                "~/Sm/js/grapesjs/plugins/grapesjs-navbar.min.js",
                "~/Sm/js/grapesjs/plugins/grapesjs-lory-slider.min.js",
                "~/Sm/js/grapesjs/plugins/grapesjs-preset-webpage.min.js"
            ))


		bundles.Add(New CCustomScriptBundle("~/js/agentscript").Include(
				"~/agent/js/jquery-1.12.4.js",
				"~/js/BrowserError.js",
				"~/agent/js/jquery.serializejson.min.js",
				"~/agent/js/bootstrap/bootstrap.min.js",
				"~/agent/js/bootstrap/bootstrap-dialog.min.js",
				"~/agent/js/bootstrap/bootstrap-datepicker.js",
				"~/agent/js/bootstrap/bootstrap-typeahead.js",
				"~/agent/js/noty/jquery.noty.packaged.min.js",
				"~/agent/js/dropzone/dropzone.js",
				"~/agent/js/notify/notify.min.js",
				"~/agent/js/notify/smvalidator.js",
				"~/agent/js/pace/pace.min.js",
				"~/agent/js/bootpag/jquery.bootpag.min.js",
				"~/agent/js/jquery.scrollbar.js",
				"~/agent/js/common.js",
				"~/agent/js/main.js"
			))
	End Sub
End Class
