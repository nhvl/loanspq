﻿Imports Microsoft.VisualBasic
Imports System.Xml
Imports System.Web.Script.Serialization
Namespace LPQMobile.BO
	<Serializable()> _
	Public Class CCustomQuestion
		Public Name As String
		Public Text As String
		Public Value As String
		Public UIType As QuestionUIType
		Public isRequired As String
        Public Conditioning As String
        Public RegExpression As String
        Public maxValue As String
        Public minValue As String
        Public maxLength As String
        Public minLength As String
        Public errorMessage As String
        Public DataType As String
        Public ConfirmationText As String
		'' lazy init
		Private _Answers As List(Of String)
		''----create customQuestionOption list
		Private _CustomQuestionOptions As SerializableList(Of CCustomQuestionOption)
		Public Property CustomQuestionOptions() As SerializableList(Of CCustomQuestionOption)
			Get
				If _CustomQuestionOptions Is Nothing Then
					_CustomQuestionOptions = New SerializableList(Of CCustomQuestionOption)
				End If
				Return _CustomQuestionOptions
			End Get
			Set(ByVal value As SerializableList(Of CCustomQuestionOption))
				_CustomQuestionOptions = value
			End Set
		End Property

		Public Property Answers() As List(Of String)
			Get
				If _Answers Is Nothing Then
					_Answers = New List(Of String)
				End If
				Return _Answers
			End Get
			Set(ByVal value As List(Of String))
				_Answers = value
			End Set
		End Property

		Public Sub New()

		End Sub


        Public Sub New(ByVal oQuestionNode As XmlElement)
            Me.Name = oQuestionNode.GetAttribute("name")
            Me.Text = oQuestionNode.GetAttribute("text")
            Me.isRequired = oQuestionNode.GetAttribute("is_required")
            '' value attribute only available for HIDDEN question (for now)
            Me.Value = oQuestionNode.GetAttribute("value")
            Me.Conditioning = LPQMobile.Utils.Common.SafeString(oQuestionNode.GetAttribute("conditioning"))
            Me.RegExpression = LPQMobile.Utils.Common.SafeString(oQuestionNode.GetAttribute("reg_exp"))
            If Not String.IsNullOrEmpty(Me.RegExpression) Then
                Me.RegExpression = New JavaScriptSerializer().Serialize(Me.RegExpression)
            End If
            Me.maxValue = LPQMobile.Utils.Common.SafeString(oQuestionNode.GetAttribute("max_value"))
            Me.minValue = LPQMobile.Utils.Common.SafeString(oQuestionNode.GetAttribute("min_value"))
            Me.DataType = LPQMobile.Utils.Common.SafeString(oQuestionNode.GetAttribute("data_type"))
            Me.ConfirmationText = oQuestionNode.GetAttribute("confirmation_text")
            Dim sMaxLength As String = ""
            Dim sMinLength As String = ""
            ''use maxLength and minLength only when there is no regular expression(to avoid conflict)
            If String.IsNullOrEmpty(LPQMobile.Utils.Common.SafeString(oQuestionNode.GetAttribute("reg_exp"))) Then
                sMaxLength = LPQMobile.Utils.Common.SafeString(oQuestionNode.GetAttribute("max_length"))
                sMinLength = LPQMobile.Utils.Common.SafeString(oQuestionNode.GetAttribute("min_length"))
            End If
            Me.errorMessage = LPQMobile.Utils.Common.SafeString(oQuestionNode.GetAttribute("error_message"))
            Me.maxLength = sMaxLength
            Me.minLength = sMinLength
            Select Case oQuestionNode.GetAttribute("type")
                Case "text"
                    Me.UIType = QuestionUIType.TEXT
                Case "dropdown"
                    Me.UIType = QuestionUIType.DROPDOWN
                Case "check"
                    Me.UIType = QuestionUIType.CHECK
                Case "radio"
                    Me.UIType = QuestionUIType.RADIO
                Case "hidden"
                    Me.UIType = QuestionUIType.HIDDEN
            End Select
            Dim ans As XmlNodeList = oQuestionNode.SelectNodes("ANSWER")
            For Each oItem As XmlElement In ans
                Me.Answers.Add(oItem.GetAttribute("text"))
            Next
            ''-----add value and text 
			Me.CustomQuestionOptions = New SerializableList(Of CCustomQuestionOption)
            For Each oOptNode As XmlElement In oQuestionNode.SelectNodes("ANSWER")
                Dim oOpt As CCustomQuestionOption = New CCustomQuestionOption(oOptNode)
                If Not String.IsNullOrEmpty(oOpt.Text) AndAlso Not String.IsNullOrEmpty(oOpt.Value) Then
                    Me.CustomQuestionOptions.Add(oOpt)
                ElseIf Not String.IsNullOrEmpty(oOpt.Text) AndAlso String.IsNullOrEmpty(oOpt.Value) Then
                    oOpt.Value = oOpt.Text
                    Me.CustomQuestionOptions.Add(oOpt)
                ElseIf String.IsNullOrEmpty(oOpt.Text) Then 'empty default case
                    Me.CustomQuestionOptions.Add(oOpt)
                End If
            Next
            ''----------
        End Sub

    End Class

End Namespace

