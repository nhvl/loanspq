﻿Imports Microsoft.VisualBasic
Imports System.Xml
Imports LPQMobile.Utils.Common
Imports LPQMobile.Utils

Namespace LPQMobile.BO

	Public Class CCreditCard
		Inherits CBaseLoanApp
		Public CardType As String
		Public CardName As String
		Public RequestAmount As Double
		Public eCardNumber As String
        Public CCNameComment As String
        Public Overrides ReadOnly Property LoanType() As String
			Get
				Return "CC"
			End Get
		End Property

		Public Sub New(ByVal psOrganizationId As String, ByVal psLenderId As String)
			MyBase.New(psOrganizationId, psLenderId)
		End Sub

		Public Overrides Function GetXml(oConfig As CWebsiteConfig) As XmlDocument
			Dim oDoc As New XmlDocument()
            oDoc.LoadXml("<CREDITCARD_LOAN xmlns=""" & NAMESPACE_URI &
                         """ xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" " &
                         "xsi:schemaLocation=""http://www.meridianlink.com/CLF personal_loan.xsd http://www.meridianlink.com/CLF Integration\SSFCU.xsd"" " &
                         "version=""5.181"" />") ''5.203, 5.070   

            AddApplicantsXml(oDoc, oConfig)
			AddLoanInfoXml(oDoc)
			AddLoanStatusXml(oDoc)
			AddFundingXml(oDoc)
			AddCommentsXml(oDoc)
            AddCustomQuestionsXml(oDoc)
            AddContactsXml(oDoc)
            AddSystemXml(oDoc)

            Return oDoc
		End Function

		Private Sub AddLoanInfoXml(ByVal poDoc As XmlDocument)
			Dim oxmlLoanInfo As XmlElement = CType(poDoc.DocumentElement.AppendChild(poDoc.CreateElement("LOAN_INFO", NAMESPACE_URI)), XmlElement)
			oxmlLoanInfo.SetAttribute("card_type", CardType)
			oxmlLoanInfo.SetAttribute("requested_card_name", CardName)
			oxmlLoanInfo.SetAttribute("purpose_type", LoanPurpose)
			oxmlLoanInfo.SetAttribute("requested_credit_limit", RequestAmount)
			oxmlLoanInfo.SetAttribute("current_card_number", eCardNumber.Replace("-", ""))
			''don't know what first_card_holder_order_card use for, it does not exist in current version 
			''oxmlLoanInfo.SetAttribute("first_card_holder_order_card", "Y") 'founder fcu request

			'may not require for credit card
			'If Not String.IsNullOrEmpty(VendorID) Then
			'	oxmlLoanInfo.SetAttribute("is_indirect_loan", "Y")
			'End If
		End Sub

		

		Public Sub AddFundingXml(ByVal poDoc As XmlDocument)
			Dim oxmlFunding As XmlElement = CType(poDoc.DocumentElement.AppendChild(poDoc.CreateElement("FUNDING", NAMESPACE_URI)), XmlElement)
			oxmlFunding.SetAttribute("payment_method", "MIN PAYMENT")
			oxmlFunding.AppendChild(poDoc.CreateElement("INSURANCE_OPTIONS", NAMESPACE_URI))
		End Sub

		Public Sub AddCommentsXml(ByVal poDoc As XmlDocument)
			Dim oxmlComments As XmlElement = CType(poDoc.DocumentElement.AppendChild(poDoc.CreateElement("COMMENTS", NAMESPACE_URI)), XmlElement)
			Dim oxmlDecisionComments As XmlElement = CType(oxmlComments.AppendChild(poDoc.CreateElement("DECISION_COMMENTS", NAMESPACE_URI)), XmlElement)
			Dim oxmlExternalComments As XmlElement = oxmlComments.AppendChild(poDoc.CreateElement("EXTERNAL_COMMENTS", NAMESPACE_URI))
			Dim oxmlInternalComments As XmlElement = CType(oxmlComments.AppendChild(poDoc.CreateElement("INTERNAL_COMMENTS", NAMESPACE_URI)), XmlElement)
			If HasDebtCancellation Then
				oxmlExternalComments.InnerText = HasDebtCancellationText
			End If
			If CardName <> "" Then
				''add credit card name to internal comment
				oxmlInternalComments.InnerText += "SYSTEM (" + DateTime.Now + "): Selected Credit Card: " & CardName
				oxmlInternalComments.InnerText += Environment.NewLine
			End If

			''''write internal comment element:  disclosures and originating ip address
			oxmlInternalComments.InnerText += CurrentEmail + " (" + DateTime.Now + "): Disclosure(s) Checked: " + Disclosure
			oxmlInternalComments.InnerText += Environment.NewLine
			oxmlInternalComments.InnerText += "SYSTEM (" + DateTime.Now + "): Originating IP Address: " + IPAddress
			oxmlInternalComments.InnerText += Environment.NewLine
			oxmlInternalComments.InnerText += "SYSTEM (" + DateTime.Now + "): USER AGENT: " + UserAgent
			oxmlInternalComments.InnerText += Environment.NewLine

			''add cuna question answers in the internal comment if it exists
			If cunaQuestionAnswers.Count > 0 Then
				Dim i As Integer
				oxmlInternalComments.InnerText += Environment.NewLine
				oxmlInternalComments.InnerText += Environment.NewLine
				oxmlInternalComments.InnerText += "SYSTEM (" + DateTime.Now + "): PROTECTION INFORMATION: "
				oxmlInternalComments.InnerText += Environment.NewLine
				oxmlInternalComments.InnerText += Environment.NewLine
				For i = 0 To cunaQuestionAnswers.Count - 1
					oxmlInternalComments.InnerText += cunaQuestionAnswers(i)
					oxmlInternalComments.InnerText += Environment.NewLine
				Next
			End If
			''add xSell comment message to internal comment if it exist
			If xSellCommentMessage <> "" Then
				oxmlInternalComments.InnerText += "SYSTEM (" + DateTime.Now + "): " & xSellCommentMessage
				oxmlInternalComments.InnerText += Environment.NewLine
			End If
			''add instaTouch Prefill comment if it exist
			If InstaTouchPrefillComment <> "" Then
				oxmlInternalComments.InnerText += "SYSTEM (" + DateTime.Now + "): " & InstaTouchPrefillComment
				oxmlInternalComments.InnerText += Environment.NewLine
			End If
			''add credit card name comment if it exist
			If Not String.IsNullOrWhiteSpace(CCNameComment) Then
				oxmlInternalComments.InnerText += "SYSTEM (" + DateTime.Now + "): " & CCNameComment
				oxmlInternalComments.InnerText += Environment.NewLine
			End If

			'TODO: add comment for app from vendor
			'App originated by userName@Vendor
		End Sub
	End Class

End Namespace