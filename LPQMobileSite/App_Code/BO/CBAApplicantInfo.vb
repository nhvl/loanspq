﻿''' <summary>
''' Business Account (XA business)
''' </summary>
<Serializable()>
Public Class CBAApplicantInfo
    Public Property AllowDebit As Boolean
    Public Property AllowOfac As Boolean
    Public Property Prefix As String
    Public Property AccountName As String
    Public Property IsJoint As String
    Public Property RoleType As String
    Public Property ControlTitle As String
    Public Property MemberNumber As String
    Public Property SSN As String
    Public Property FirstName As String
    Public Property MiddleName As String
    Public Property LastName As String
    Public Property NameSuffix As String
    Public Property DOB As Date
    Public Property Gender As String
    Public Property MotherMaidenName As String
    Public Property CitizenshipStatus As String
    Public Property MaritalStatus As String
    Public Property EmployeeOfLender As String
    Public Property RelationToPrimaryApplicant As String
    Public Property Beneficiaries As List(Of CBeneficiaryInfo)

    Public Property AddressStreet As String
    Public Property AddressStreet2 As String
    Public Property AddressCity As String
    Public Property AddressZip As String
    Public Property AddressState As String
    Public Property AddressCountry As String
    Public Property HasMailingAddress As String
    Public Property MailingAddressStreet As String
    Public Property MailingAddressStreet2 As String
    Public Property MailingAddressCity As String
    Public Property MailingAddressZip As String
    Public Property MailingAddressCountry As String
    Public Property MailingAddressState As String

    Public Property HasPreviousAddress As String
    Public Property PreviousAddressStreet As String
    Public Property PreviousAddressCity As String
    Public Property PreviousAddressZip As String
    Public Property PreviousAddressCountry As String
    Public Property PreviousAddressState As String
    Public Property HousingPayment As String
    Public Property OccupyingLocation As String
    Public Property OccupancyDuration As Integer
    Public Property OccupancyDescription As String

    Public Property ContactMethod As String
    Public Property ContactEmail As String
    Public Property ContactHomePhone As String
    Public Property ContactHomePhoneCountry As String
    Public Property ContactMobilePhone As String
    Public Property ContactMobilePhoneCountry As String
    Public Property ContactWorkPhone As String
    Public Property ContactWorkPhoneCountry As String
    Public Property ContactWorkPhoneExt As String
    Public Property RelativeFirstName As String
    Public Property RelativeLastName As String
    Public Property RelativePhone As String
    Public Property RelativeEmail As String
    Public Property RelativeRelationship As String
    Public Property RelativeAddress As String
    Public Property RelativeZip As String
    Public Property RelativeCity As String
    Public Property RelativeState As String
    Public Property IDState As String
    Public Property IDCountry As String
    Public Property IDCardType As String
    Public Property IDCardNumber As String
    Public Property IDDateIssued As String
    Public Property IDDateExpire As String

    Public Property EmploymentStatus As String
    Public Property EmploymentDescription As String
    Public Property GrossMonthlyIncome As Double
    Public Property EnlistmentDate As String

    Public Property EmployedDurationMonth As Integer
    Public Property EmployedDurationYear As Integer
    Public Property Employer As String
    Public Property JobTitle As String
    Public Property BusinessType As String
    Public Property EmploymentStartDate As String
    Public Property ETS As String
    Public Property ProfessionDurationMonth As Integer
    Public Property ProfessionDurationYear As Integer
    Public Property SupervisorName As String
    Public Property BranchOfService As String
    Public Property PayGrade As String
    Public Property HasPreviousEmployment As String
    Public Property PrevEmploymentStartDate As String
    Public Property PrevEmploymentStatus As String
    Public Property PrevGrossMonthlyIncome As String
    Public Property PrevEmployedDurationMonth As String
    Public Property PrevEmployedDurationYear As String
    Public Property PrevJobTitle As String
    Public Property PrevEmployer As String
    Public Property PrevBusinessType As String
    Public Property PrevEnlistmentDate As String
    Public Property PrevBranchOfService As String
    Public Property PrevPayGrade As String
    Public Property PrevETS As String
	Public Property ApplicantQuestionAnswers As List(Of CValidatedQuestionAnswers)

	Public Property BusinessOwned As Double
    Public Property HasControl As String
    ''business loan
    Public Property TotalMonthlyHousingExpense As Double
    Public Property OtherMonthlyIncome As Double
    Public Property OtherMonthlyIncomeDescription As String
    Public Property GrossMonthlyInComeTaxExempt As String
    Public Property OtherMonthlyIncomeTaxExempt As String
    ''business loan - reference information
    Public Property PriRefFirstName As String
    Public Property PriRefLastName As String
    Public Property PriRefEmail As String
    Public Property PriRefPhone As String
    Public Property PriRefRelationship As String
    Public Property PriRefStreet As String
    Public Property PriRefZip As String
    Public Property PriRefCity As String
    Public Property PriRefState As String
    Public Property SecRefFirstName As String
    Public Property SecRefLastName As String
    Public Property SecRefEmail As String
    Public Property SecRefPhone As String
    Public Property SecRefStreet As String
    Public Property SecRefZip As String
    Public Property SecRefRelationship As String
    Public Property SecRefCity As String
    Public Property SecRefState As String
    Public Property HasReference As String
    ''secondary id 
    Public Property SecondaryIDState As String
    Public Property SecondaryIDCountry As String
    Public Property SecondaryIDCardType As String
    Public Property SecondaryIDCardNumber As String
    Public Property SecondaryIDDateIssued As String
    Public Property SecondaryIDDateExpire As String
End Class
