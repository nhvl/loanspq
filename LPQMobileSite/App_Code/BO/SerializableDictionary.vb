﻿Imports Microsoft.VisualBasic
Imports System.Xml.Serialization
Imports System.Xml

<XmlRoot("dictionary")>
Public Class SerializableDictionary(Of TKey, TValue)
	Inherits Dictionary(Of TKey, TValue)
	Implements IXmlSerializable

	Public Sub New()

	End Sub
	Public Sub New(dictionary As IDictionary(Of TKey, TValue))
		MyBase.New(dictionary)
	End Sub

	Public Sub New(dictionary As IDictionary(Of TKey, TValue), comparer As IEqualityComparer(Of TKey))
		MyBase.New(dictionary, comparer)
	End Sub

	Public Sub New(comparer As IEqualityComparer(Of TKey))
		MyBase.New(comparer)
	End Sub

	Public Sub New(capacity As Integer)
		MyBase.New(capacity)
	End Sub

	Public Sub New(capacity As Integer, comparer As IEqualityComparer(Of TKey))
		MyBase.New(capacity, comparer)
	End Sub



	Public Function GetSchema() As System.Xml.Schema.XmlSchema Implements IXmlSerializable.GetSchema
		Return Nothing
	End Function

	Public Sub ReadXml(reader As System.Xml.XmlReader) Implements IXmlSerializable.ReadXml
		Dim keySerializer As New XmlSerializer(GetType(TKey))
		Dim valueSerializer As New XmlSerializer(GetType(TValue))
		Dim wasEmpty = reader.IsEmptyElement
		reader.Read()
		If wasEmpty Then
			Return
		End If
		While reader.NodeType <> XmlNodeType.EndElement
			reader.ReadStartElement("item")

			reader.ReadStartElement("key")
			Dim key As TKey = CType(keySerializer.Deserialize(reader), TKey)
			reader.ReadEndElement()

			reader.ReadStartElement("value")
			Dim value As TValue = CType(valueSerializer.Deserialize(reader), TValue)
			reader.ReadEndElement()
			Add(key, value)
			reader.ReadEndElement()
			reader.MoveToContent()

		End While
		reader.ReadEndElement()
	End Sub

	Public Sub WriteXml(writer As System.Xml.XmlWriter) Implements IXmlSerializable.WriteXml

		Dim keySerializer As New XmlSerializer(GetType(TKey))
		Dim valueSerializer As New XmlSerializer(GetType(TValue))
		For Each key As TKey In Keys
			writer.WriteStartElement("item")
			writer.WriteStartElement("key")
			keySerializer.Serialize(writer, key)
			writer.WriteEndElement()

			writer.WriteStartElement("value")
			Dim value As TValue = Me(key)
			valueSerializer.Serialize(writer, value)
			writer.WriteEndElement()
			writer.WriteEndElement()
		Next
	End Sub
End Class
