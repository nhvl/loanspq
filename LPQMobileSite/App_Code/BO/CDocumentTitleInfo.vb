﻿Imports CustomList
Imports Microsoft.VisualBasic

Public Class CDocumentTitleInfo
	Inherits CCustomListRuleBaseItem
	Public Property Title As String
	Public Property Group As String
	Public Property Code As String
	''' <summary>
	''' If blank, the uploaded doc will be sent to all applicable loan types.
	''' For Combo Apps, this means the doc is sent to both the XA and the Loan apps.
	''' For single apps, it would be sent normally.
	''' However, if this is specified, then it will only get sent to the specified
	''' loan app type, which is useful in Combo Apps.
	''' </summary>
	''' <remarks>From AP-1843</remarks>
	Public Property LoanType As String
End Class
