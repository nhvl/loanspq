﻿Imports Microsoft.VisualBasic
Imports System.Xml
Imports LPQMobile.Utils.Common

<Serializable()> _
Public Class CCustomQuestionOption
	Public Position As Integer
	Public Text As String
	Public Value As String

	Public Sub New() ' this should be here, don't remove

	End Sub

	Sub New(ByVal oNode As XmlElement)
		If oNode.GetAttribute("position") = "" Then
			' Generate random value between 1 and 30. 
			Dim value As Integer = New CSecureRandom().GetValue(1, 30)
			Me.Position = value
		Else
			Me.Position = SafeInteger(oNode.GetAttribute("position"))
		End If


		Me.Text = oNode.GetAttribute("text")
		Me.Value = oNode.GetAttribute("value")
	End Sub
End Class
