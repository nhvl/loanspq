﻿Imports Microsoft.VisualBasic
''' <summary>
''' object for storing response fron xsell(new object: vl and cc)
''' </summary>
''' <remarks></remarks>
Public Class CXSellNewGrade
	Public Property Rate() As Decimal
	Public Property MonthlyPayment() As Decimal
	Public Property NumberOfPayments() As Integer

	Public Property LoanAmount() As Decimal

	Public Property MaxAmountQualified() As Decimal	' also use as credit limit

	'additional property for credit card
	Public Property CreditCardName() As String
	Public Property CreditCardType() As String
End Class
