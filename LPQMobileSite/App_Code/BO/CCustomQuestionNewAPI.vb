﻿Imports Microsoft.VisualBasic
Imports System.Xml
Imports System.Linq
Imports System.Net
Imports System.IO
Imports System.Web.Script.Serialization

Imports LPQMobile.Utils
Imports LPQMobile.Utils.Common

Namespace LPQMobile.BO

    <Serializable()> _
 Public Class CCustomQuestionNewAPI
        Private Shared _log As log4net.ILog = log4net.LogManager.GetLogger(GetType(CCustomQuestionNewAPI))

        Public name As String
        Public text As String
        Public position As Integer
        Public answerType As String
        Public isRequired As Boolean
        Public isActive As Boolean
        Public isAvailableToConsumer As Boolean
        Public dataType As String = ""
        Public minLength As String = ""
        Public maxLength As String = ""
        Public minValue As String = ""
        Public maxValue As String = ""
        Public regExpression As String = ""
        Public minDate As String = ""
        Public maxDate As String = ""
        Public errorMessage As String = ""
        Public confirmationText As String = ""
		Private _CustomQuestionOptions As SerializableList(Of CCustomQuestionOption)

        Public availAccountType As String = ""
        Public appTypes As List(Of String)
        Public entityTypes As List(Of String)
        Public specialAccountTypes As List(Of String)

		Public Property CustomQuestionOptions() As SerializableList(Of CCustomQuestionOption)
			Get
				If _CustomQuestionOptions Is Nothing Then
					_CustomQuestionOptions = New SerializableList(Of CCustomQuestionOption)
				End If
				Return _CustomQuestionOptions
			End Get
			Set(ByVal value As SerializableList(Of CCustomQuestionOption))
				_CustomQuestionOptions = value
			End Set
		End Property
        Public Sub New(ByVal oNode As XmlElement)

            Me.name = SafeString(oNode.SelectSingleNode("NAME").InnerText)
            Me.text = SafeString(oNode.SelectSingleNode("TEXT").InnerText)
            Me.position = SafeInteger(oNode.SelectSingleNode("POSITION").InnerText)
            Me.answerType = SafeString(oNode.SelectSingleNode("ANSWER_TYPE").InnerText)
            Me.isRequired = Convert.ToBoolean(SafeString(oNode.GetAttribute("is_required")))
            Me.isActive = Convert.ToBoolean(SafeString(oNode.GetAttribute("is_active")))
            Dim appSource As String = SafeString(oNode.GetAttribute("app_source"))
            Me.isAvailableToConsumer = appSource = "consumer_lender" Or appSource = "consumer_lender_conditional"

            If Me.answerType = "TEXTBOX" Then
                If oNode.SelectSingleNode("FREEFORM_TEXTBOX") IsNot Nothing Then
                    Dim sDataType = getDataType(SafeString(oNode.SelectSingleNode("FREEFORM_TEXTBOX").Attributes("type").InnerXml))
                    Dim sStringOptionPath As String = "FREEFORM_TEXTBOX/STRING_OPTIONS/"
                    Dim sPasswordOptionPath As String = "FREEFORM_TEXTBOX/PASSWORD_OPTIONS/"
                    Dim sNumberOptionPath As String = "FREEFORM_TEXTBOX/NUMBER_OPTIONS/"
                    Me.dataType = sDataType
                    If sDataType = "S" Then '' string type
                        If oNode.SelectSingleNode(sStringOptionPath & "REGEX") IsNot Nothing Then
                            Me.regExpression = SafeString(oNode.SelectSingleNode(sStringOptionPath & "REGEX").InnerText)
                            If Not String.IsNullOrEmpty(Me.regExpression) Then
                                Me.regExpression = New JavaScriptSerializer().Serialize(Me.regExpression)
                            End If
                        End If
                        If oNode.SelectSingleNode(sStringOptionPath & "MIN_LENGTH") IsNot Nothing Then
                            Me.minLength = SafeString(oNode.SelectSingleNode(sStringOptionPath & "MIN_LENGTH").InnerText)
                        End If
                        If oNode.SelectSingleNode(sStringOptionPath & "MAX_LENGTH") IsNot Nothing Then
                            Me.maxLength = SafeString(oNode.SelectSingleNode(sStringOptionPath & "MAX_LENGTH").InnerText)
                        End If
                    ElseIf sDataType = "P" Then
                        If oNode.SelectSingleNode(sPasswordOptionPath & "REGEX") IsNot Nothing Then
                            Me.regExpression = SafeString(oNode.SelectSingleNode(sPasswordOptionPath & "REGEX").InnerText)
                            If Not String.IsNullOrEmpty(Me.regExpression) Then
                                Me.regExpression = New JavaScriptSerializer().Serialize(Me.regExpression)
                            End If
                        End If
                        If oNode.SelectSingleNode(sPasswordOptionPath & "MIN_LENGTH") IsNot Nothing Then
                            Me.minLength = SafeString(oNode.SelectSingleNode(sPasswordOptionPath & "MIN_LENGTH").InnerText)
                        End If
                        If oNode.SelectSingleNode(sPasswordOptionPath & "MAX_LENGTH") IsNot Nothing Then
                            Me.maxLength = SafeString(oNode.SelectSingleNode(sPasswordOptionPath & "MAX_LENGTH").InnerText)
                        End If
                    ElseIf sDataType = "N" Or sDataType = "C" Then ''number type 

                        If oNode.SelectSingleNode(sNumberOptionPath & "MIN_VALUE") IsNot Nothing Then
                            Me.minValue = SafeString(oNode.SelectSingleNode(sNumberOptionPath & "MIN_VALUE").InnerText)
                        End If

                        If oNode.SelectSingleNode(sNumberOptionPath & "MAX_VALUE") IsNot Nothing Then
                            Me.maxValue = SafeString(oNode.SelectSingleNode(sNumberOptionPath & "MAX_VALUE").InnerText)
                        End If

                    ElseIf sDataType = "D" Then ''date type
                        If oNode.SelectSingleNode(sStringOptionPath & "MIN_DATE") IsNot Nothing Then
                            Dim sMinDate = SafeString(oNode.SelectSingleNode(sStringOptionPath & "MIN_DATE").InnerText)
                            Me.minDate = convertDateFormat(sMinDate)
                        End If

                        If oNode.SelectSingleNode(sStringOptionPath & "MAX_DATE") IsNot Nothing Then
                            Dim sMaxDate = SafeString(oNode.SelectSingleNode(sStringOptionPath & "MAX_DATE").InnerText)
                            Me.maxDate = convertDateFormat(sMaxDate)
                        End If
                    End If
                End If
            End If

            If oNode.SelectSingleNode("VALIDATION_ERROR_MSG") IsNot Nothing Then
                Me.errorMessage = SafeString(oNode.SelectSingleNode("VALIDATION_ERROR_MSG").InnerText)
            End If

            If oNode.SelectSingleNode("XA_OPTIONS/AVAIL_FOR_ACCOUNT_TYPE") IsNot Nothing Then
                Me.availAccountType = SafeString(oNode.SelectSingleNode("XA_OPTIONS/AVAIL_FOR_ACCOUNT_TYPE").InnerText)
            End If

            Me.appTypes = New List(Of String)
            Dim oAppTypeNodes As XmlNodeList = oNode.SelectNodes("APP_TYPES_SUPPORTED/APP_TYPE")
            If oAppTypeNodes.Count > 0 Then
                For Each oAppTypeNode As XmlElement In oAppTypeNodes
                    Dim strAppType = SafeString(oAppTypeNode.InnerText)
                    If strAppType <> "" Then
                        Me.appTypes.Add(strAppType)
                    End If
                Next
            End If

            Me.entityTypes = New List(Of String)
            Dim oEntityTypeNodes As XmlNodeList = oNode.SelectNodes("XA_OPTIONS/ENTITY_TYPES_SUPPORTED/ENTITY_TYPE")
            If oEntityTypeNodes.Count > 0 Then
                For Each oEntityTypeNode As XmlElement In oEntityTypeNodes
                    Dim strEntityType = SafeString(oEntityTypeNode.InnerText)
                    If strEntityType <> "" Then
                        Me.entityTypes.Add(strEntityType)
                    End If
                Next
            End If
			Me.specialAccountTypes = New SerializableList(Of String)
            Dim oSpecialAccountTypeNodes As XmlNodeList = oNode.SelectNodes("XA_OPTIONS/SPECIAL_ACCOUNT_TYPES_SUPPORTED/SPECIAL_ACCOUNT_TYPE")
            If oSpecialAccountTypeNodes.Count > 0 Then
                For Each oSpecialAccountType As XmlElement In oSpecialAccountTypeNodes
                    Dim strSpecialAccountType = SafeString(oSpecialAccountType.InnerText)
                    If strSpecialAccountType <> "" Then
                        specialAccountTypes.Add(strSpecialAccountType)
                    End If
                Next
            End If
			Me.CustomQuestionOptions = New SerializableList(Of CCustomQuestionOption)
            Dim oOptNodes As XmlNodeList = oNode.SelectNodes(answerType & "/RESTRICTED_ANSWERS/RESTRICTED_ANSWER")

            If oOptNodes.Count > 0 Then
                For Each oOptNode As XmlElement In oOptNodes
                    Dim oOpt As CCustomQuestionOption = New CCustomQuestionOption(oOptNode)
                    If Not String.IsNullOrEmpty(oOpt.Text) AndAlso Not String.IsNullOrEmpty(oOpt.Value) Then
                        Me.CustomQuestionOptions.Add(oOpt)
                    ElseIf oOpt.Position = 0 AndAlso String.IsNullOrEmpty(oOpt.Text) Then 'empty default case
                        Me.CustomQuestionOptions.Add(oOpt)
                    End If
				Next
				Me.CustomQuestionOptions = Me.CustomQuestionOptions.OrderBy(Function(o) o.Position).ToSerializableList_()
            End If
        End Sub

        Private Shared Function convertDateFormat(ByVal sDate As String) As String
            If sDate = "" Then
                Return ""
            End If
            Dim sDay As String = sDate.Substring(8, 2)
            Dim sMonth As String = sDate.Substring(6, 2)
            Dim sYear As String = sDate.Substring(0, 4)

            Return sMonth + "/" + sDate + "/" + sYear

        End Function

        Private Shared Function getDataType(ByVal sType As String) As String
            If sType = "number" Then
                Return "N"
            ElseIf sType = "number_commas" Then
                Return "C"
            ElseIf sType = "date" Then
                Return "D"
            ElseIf sType = "password" Then
                Return "P"
            Else
                Return "S"
            End If
        End Function

        Private Shared Function isSupportedAppType(ByVal sloanType As String, ByVal sAppType As List(Of String)) As Boolean
            If sAppType.Count = 0 Then
                Return False
            End If

            For i = 0 To sAppType.Count - 1
                If (sloanType = sAppType(i)) Then
                    Return True
                End If
            Next
            Return False
        End Function
        Private Shared Function getAvailableAccountType(ByVal sAvailability As String) As String
            Dim sAvailableAccountType As String = ""
            ''3/3a for business
            If (sAvailability = "2a" Or sAvailability = "3a" Or sAvailability = "2") Then
                Return "SECONDARY"
            End If
            Return "PRIMARY"
        End Function
        Private Shared Function hasEntityType(ByVal sEntityType As String, ByVal sEntityTypes As List(Of String)) As Boolean
            If sEntityTypes.Count = 0 Then
                Return False
            End If

            For i = 0 To sEntityTypes.Count - 1
                If sEntityTypes(i) = sEntityType Then
                    Return True
                End If
            Next
            Return False
        End Function
        Public Shared Function ParsedALLDownloadingCustomQuestions(ByVal xmlDoc As XmlDocument, ByVal bIsUrlParaCustomQuestion As Boolean) As SerializableList(Of CCustomQuestionNewAPI)
            Dim oCustomQuestionList As New SerializableList(Of CCustomQuestionNewAPI)
            Dim xmlNodeList As XmlNodeList = xmlDoc.SelectNodes("//OUTPUT/CUSTOM_QUESTIONS/CUSTOM_QUESTION")
            If xmlNodeList.Count = 0 Then
                Return oCustomQuestionList
            End If
            ' parse product question list
            Dim oQuestion As CCustomQuestionNewAPI
            For Each oItem As XmlElement In xmlNodeList
                oQuestion = New CCustomQuestionNewAPI(oItem)
                If oQuestion.answerType = "HEADER" Then
                    Continue For
                End If
                If bIsUrlParaCustomQuestion Then
                    ''get all active custom questions including lender questions          
                    If Convert.ToBoolean(oItem.GetAttribute("is_active")) And (SafeString(oItem.GetAttribute("app_source")) = "consumer_lender" Or SafeString(oItem.GetAttribute("app_source")) = "consumer_lender_conditional" Or SafeString(oItem.GetAttribute("app_source")) = "lender") Then
                        oCustomQuestionList.Add(oQuestion)
                    End If
                Else
                    ''available question for consumer_lender or consumer_lender_conditional            
                    If Convert.ToBoolean(oItem.GetAttribute("is_active")) And (SafeString(oItem.GetAttribute("app_source")) = "consumer_lender" Or SafeString(oItem.GetAttribute("app_source")) = "consumer_lender_conditional") Then
                        oCustomQuestionList.Add(oQuestion)
                    End If
                End If
            Next
            Return oCustomQuestionList
        End Function

        ''Public Shared Function ParsedCustomQuestions(ByVal xmlDoc As XmlDocument, ByVal sLoanType As String, Optional ByVal sAvailability As String = "", Optional ByVal isSpecialAccount As Boolean = False) As SerializableList(Of CCustomQuestionNewAPI)
        ''    Dim oResultList As New SerializableList(Of CCustomQuestionNewAPI)
        ''    Dim oCustomQuestionList = ParsedALLDownloadingCustomQuestions(xmlDoc)
        ''    If sLoanType = "ALL" Then
        ''        Return oCustomQuestionList
        ''    End If
        ''    For i = 0 To oCustomQuestionList.Count - 1
        ''        If isSupportedAppType(sLoanType, oCustomQuestionList(i).appTypes) Then
        ''            ''xa: entitytype =personal/business, application/applicant questions
        ''            If sLoanType = "XA" Then
        ''                If isSpecialAccount Then ''for special account
        ''                    ''get custom questions if the entity type is SPECIAL
        ''                    If hasEntityType("SPECIAL", oCustomQuestionList(i).entityTypes) Then
        ''                        ''get custom questions based on availability
        ''                        If oCustomQuestionList(i).availAccountType = "PRIMARY_SECONDARY" Then 'for both primary and secondary
        ''                            oResultList.Add(oCustomQuestionList(i))
        ''                        Else
        ''                            If getAvailableAccountType(sAvailability) = oCustomQuestionList(i).availAccountType Then
        ''                                oResultList.Add(oCustomQuestionList(i))
        ''                            End If
        ''                        End If
        ''                    End If
        ''                Else ''for personal
        ''                    ''get custom questions if the entity type is personal
        ''                    If hasEntityType("PERSONAL", oCustomQuestionList(i).entityTypes) Then
        ''                        ''get custom questions based on availability
        ''                        If oCustomQuestionList(i).availAccountType = "PRIMARY_SECONDARY" Then 'for both primary and secondary
        ''                            oResultList.Add(oCustomQuestionList(i))
        ''                        Else
        ''                            If getAvailableAccountType(sAvailability) = oCustomQuestionList(i).availAccountType Then
        ''                                oResultList.Add(oCustomQuestionList(i))
        ''                            End If
        ''                        End If
        ''                    End If
        ''                End If
        ''            Else ''for loans - entityType = personal/business

        ''                oResultList.Add(oCustomQuestionList(i))
        ''            End If
        ''        End If
        ''    Next

        ''    oResultList = oResultList.OrderBy(Function(o) o.position).ToList()

        ''    Return oResultList
        ''End Function    
        Public Shared Function ParsedCustomQuestions(ByVal xmlDoc As XmlDocument, ByVal sLoanType As String, Optional ByVal sAvailability As String = "", Optional ByVal bIsUrlParaCustomQuestion As Boolean = False) As SerializableList(Of CCustomQuestionNewAPI)
            Dim oResultList As New SerializableList(Of CCustomQuestionNewAPI)
            ''xa options
            Dim oAllXACQs As New SerializableList(Of CCustomQuestionNewAPI)
            Dim oSpecialPrimaryCQs As New SerializableList(Of CCustomQuestionNewAPI)
            Dim oSpecialSecondaryCQs As New SerializableList(Of CCustomQuestionNewAPI)
            Dim oPersonalPrimaryCQs As New SerializableList(Of CCustomQuestionNewAPI)
            Dim oPersonalSecondaryCQs As New SerializableList(Of CCustomQuestionNewAPI)
            Dim oBusinessPrimaryCQs As New SerializableList(Of CCustomQuestionNewAPI)
            Dim oBusinessSecondaryCQs As New SerializableList(Of CCustomQuestionNewAPI)
            ''Loans VL,PL,HE,CC, and BL
            Dim oPLCQs As New SerializableList(Of CCustomQuestionNewAPI)
            Dim oVLCQs As New SerializableList(Of CCustomQuestionNewAPI)
            Dim oCCCQs As New SerializableList(Of CCustomQuestionNewAPI)
            Dim oHECQs As New SerializableList(Of CCustomQuestionNewAPI)
            Dim oBLCQs As New SerializableList(Of CCustomQuestionNewAPI)
            Dim oCustomQuestionList = ParsedALLDownloadingCustomQuestions(xmlDoc, bIsUrlParaCustomQuestion)
            'If sLoanType = "ALL" Then
            '    Return oCustomQuestionList
            'End If
            For i = 0 To oCustomQuestionList.Count - 1
                If isSupportedAppType(sLoanType, oCustomQuestionList(i).appTypes) Then
                    ''xa: entitytype =personal/business, application/applicant questions
                    Select Case sLoanType
                        Case "XA"
                            oAllXACQs.Add(oCustomQuestionList(i))
                            Select Case oCustomQuestionList(i).availAccountType
                                Case "PRIMARY"
                                    If hasEntityType("SPECIAL", oCustomQuestionList(i).entityTypes) Then
                                        oSpecialPrimaryCQs.Add(oCustomQuestionList(i))
                                    End If
                                    If hasEntityType("PERSONAL", oCustomQuestionList(i).entityTypes) Then
                                        oPersonalPrimaryCQs.Add(oCustomQuestionList(i))
                                    End If
                                    If hasEntityType("BUSINESS", oCustomQuestionList(i).entityTypes) Then
                                        oBusinessPrimaryCQs.Add(oCustomQuestionList(i))
                                    End If
                                Case "SECONDARY"
                                    If hasEntityType("SPECIAL", oCustomQuestionList(i).entityTypes) Then
                                        oSpecialSecondaryCQs.Add(oCustomQuestionList(i))
                                    End If
                                    If hasEntityType("PERSONAL", oCustomQuestionList(i).entityTypes) Then
                                        oPersonalSecondaryCQs.Add(oCustomQuestionList(i))
                                    End If
                                    If hasEntityType("BUSINESS", oCustomQuestionList(i).entityTypes) Then
                                        oBusinessSecondaryCQs.Add(oCustomQuestionList(i))
                                    End If
                                Case "PRIMARY_SECONDARY"
                                    If hasEntityType("SPECIAL", oCustomQuestionList(i).entityTypes) Then
                                        oSpecialPrimaryCQs.Add(oCustomQuestionList(i))
                                        oSpecialSecondaryCQs.Add(oCustomQuestionList(i))
                                    End If
                                    If hasEntityType("PERSONAL", oCustomQuestionList(i).entityTypes) Then
                                        oPersonalPrimaryCQs.Add(oCustomQuestionList(i))
                                        oPersonalSecondaryCQs.Add(oCustomQuestionList(i))
                                    End If
                                    If hasEntityType("BUSINESS", oCustomQuestionList(i).entityTypes) Then
                                        oBusinessPrimaryCQs.Add(oCustomQuestionList(i))
                                        oBusinessSecondaryCQs.Add(oCustomQuestionList(i))
                                    End If
                            End Select
                        Case "PL"
                            oPLCQs.Add(oCustomQuestionList(i))
                        Case "CC"
                            oCCCQs.Add(oCustomQuestionList(i))
                        Case "VL"
                            oVLCQs.Add(oCustomQuestionList(i))
                        Case "HE"
                            oHECQs.Add(oCustomQuestionList(i))
                        Case "BL"
                            oBLCQs.Add(oCustomQuestionList(i))
                    End Select
                End If
            Next
            Select Case sLoanType
                Case "XA"
                    Select Case sAvailability.ToLower
                        Case "1"
                            oResultList = oPersonalPrimaryCQs
                        Case "2"
                            oResultList = oPersonalSecondaryCQs
                        Case "1a"
                            oResultList = oSpecialPrimaryCQs
                        Case "2a"
                            oResultList = oSpecialSecondaryCQs
                        Case "1s"
                            oResultList = oSpecialPrimaryCQs
                        Case "2s"
                            oResultList = oSpecialSecondaryCQs
                        Case "1b"
                            oResultList = oBusinessPrimaryCQs
                        Case "2b"
                            oResultList = oBusinessSecondaryCQs
                        Case Else
                            oResultList = oAllXACQs
                    End Select
                Case "PL"
                    oResultList = oPLCQs
                Case "CC"
                    oResultList = oCCCQs
                Case "VL"
                    oResultList = oVLCQs
                Case "HE"
                    oResultList = oHECQs
                Case "BL"
                    oResultList = oBLCQs
            End Select
            ''oResultList = oResultList.OrderBy(Function(o) o.position).ToList()
            ''_log.Info(oResultList.Count.ToString & " filter custom question for :" & sLoanType & " : " & New JavaScriptSerializer().Serialize(oResultList))
            ''_log.Info(oCustomQuestionList.Count.ToString & " downloaded custom questions :" & New JavaScriptSerializer().Serialize(oCustomQuestionList))
            Return oResultList
        End Function
        Public Shared Function getDownloadedCustomQuestions(ByVal oConfig As CWebsiteConfig, ByVal isApplicant As Boolean, ByVal sLoanType As String, Optional bIsUrlParaCustomQuestion As Boolean = False) As List(Of CCustomQuestion)

            Dim xmlDoc As New XmlDocument
            Dim outputTesting As String = " APPLICATION "
            If isApplicant Then
                xmlDoc = CDownloadedSettings.CustomApplicantQuestions(oConfig)
                outputTesting = " APPLICANT "
            Else
                xmlDoc = CDownloadedSettings.CustomApplicationQuestions(oConfig)
            End If
            If sLoanType = "HELOC" Then
                sLoanType = "HE"
            End If

            Dim oCQNewAPI = ParsedCustomQuestions(xmlDoc, sLoanType, "", bIsUrlParaCustomQuestion)

            Dim oCQ As New List(Of CCustomQuestion)
            If oCQNewAPI.Count = 0 Then
                Return oCQ
            End If
            For Each oQuestion In oCQNewAPI
                Dim cq As New CCustomQuestion()
                cq.Name = oQuestion.name
                cq.Text = oQuestion.text
                Select Case oQuestion.answerType
                    Case "TEXTBOX"
                        cq.UIType = QuestionUIType.TEXT
                    Case "DROPDOWN"
                        cq.UIType = QuestionUIType.DROPDOWN
                    Case "CHECKBOX"
                        cq.UIType = QuestionUIType.CHECK
                    Case "RADIO"
                        cq.UIType = QuestionUIType.RADIO
                    Case "HIDDEN"
                        cq.UIType = QuestionUIType.HIDDEN
                End Select
                cq.isRequired = IIf(oQuestion.isRequired, "Y", "")
                cq.errorMessage = oQuestion.errorMessage
                cq.CustomQuestionOptions = oQuestion.CustomQuestionOptions
                cq.RegExpression = oQuestion.regExpression
                cq.DataType = oQuestion.dataType
                cq.ConfirmationText = oQuestion.confirmationText
                cq.maxLength = oQuestion.maxLength
                cq.minLength = oQuestion.minLength
                cq.maxValue = oQuestion.maxValue
                cq.minValue = oQuestion.minValue
                oCQ.Add(cq)
            Next
            '' testing data
            ''_log.Info("download" & outputTesting & sLoanType & "Custom Questions XML --------------" & xmlDoc.InnerXml)
            ''_log.Info(outputTesting & sLoanType & "Custom Questions New API List:" & New JavaScriptSerializer().Serialize(oCQNewAPI))
            ''_log.Info(outputTesting & "Custom Questinon List: " & New JavaScriptSerializer().Serialize(oCQ))

            Return oCQ
        End Function

		Public Shared Function getDownloadedXAComboCustomQuestions(ByVal oConfig As CWebsiteConfig, ByVal isApplicantCQ As Boolean, ByVal sLoanType As String, Optional ByVal sAvailability As String = "") As List(Of CCustomQuestionXA)
			Dim oComboCQXAs As List(Of CCustomQuestionXA) = getDownloadedXACustomQuestions(oConfig, isApplicantCQ, "XA", sAvailability)
			Dim oDownloadedCQs As List(Of CCustomQuestion) = getDownloadedCustomQuestions(oConfig, isApplicantCQ, sLoanType)
			Dim oCQs As List(Of CCustomQuestionXA) = convertToCCustomQuestionXA(oDownloadedCQs)
			If oCQs.Count > 0 Then
				''merge custom questions
				For Each oCQ As CCustomQuestionXA In oCQs
					If oComboCQXAs.Any(Function(comboCQXA As CCustomQuestionXA) comboCQXA.Name = oCQ.Name) Then
						Continue For
					End If
					oComboCQXAs.Add(oCQ)
				Next
			End If
			Return oComboCQXAs
		End Function
		Public Shared Function getAllDownloadedXACustomQuestions(ByVal oConfig As CWebsiteConfig, ByVal sLoanType As String, Optional ByVal sAvailability As String = "", Optional ByVal bIsUrlParaCustomQuestion As Boolean = False) As List(Of CCustomQuestionXA)
			Dim lstCQs As New List(Of CCustomQuestionXA)
			lstCQs.AddRange(getDownloadedXACustomQuestions(oConfig, False, sLoanType, sAvailability, bIsUrlParaCustomQuestion))
			lstCQs.AddRange(getDownloadedXACustomQuestions(oConfig, True, sLoanType, sAvailability, bIsUrlParaCustomQuestion))
			Return lstCQs
		End Function
		Public Shared Function getDownloadedXACustomQuestions(ByVal oConfig As CWebsiteConfig, ByVal isApplicantCQ As Boolean, ByVal sLoanType As String, Optional ByVal sAvailability As String = "", Optional ByVal bIsUrlParaCustomQuestion As Boolean = False) As List(Of CCustomQuestionXA)

            Dim xmlDoc As New XmlDocument
            Dim outputTesting As String = " APPLICATION "
            If isApplicantCQ Then
                xmlDoc = CDownloadedSettings.CustomApplicantQuestions(oConfig)
                outputTesting = " APPLICANT "
            Else
                xmlDoc = CDownloadedSettings.CustomApplicationQuestions(oConfig)
            End If
            Dim oCQNewAPI = ParsedCustomQuestions(xmlDoc, sLoanType, sAvailability, bIsUrlParaCustomQuestion)

            Dim oCQXA As New List(Of CCustomQuestionXA)
            If oCQNewAPI.Count = 0 Then
                Return oCQXA
            End If
            For Each oQuestion In oCQNewAPI
                Dim cqXA As New CCustomQuestionXA()
                cqXA.Position = oQuestion.position
                cqXA.Name = oQuestion.name
                cqXA.Text = oQuestion.text
                cqXA.AnswerType = oQuestion.answerType
                cqXA.IsRequired = oQuestion.isRequired
                cqXA.IsAvailableToConsumer = oQuestion.isAvailableToConsumer
				cqXA.IsApplicant = isApplicantCQ
				cqXA.Identifer = New QuestionIdentifier(cqXA.Name, IIf(cqXA.IsApplicant, QuestionRole.Applicant, QuestionRole.Application))
				cqXA.IsActive = oQuestion.isActive
				cqXA.ErrorMessage = oQuestion.errorMessage
                cqXA.CustomQuestionOptions = oQuestion.CustomQuestionOptions
                cqXA.minLength = oQuestion.minLength
                cqXA.maxLength = oQuestion.maxLength
                cqXA.minValue = oQuestion.minValue
                cqXA.maxValue = oQuestion.maxValue
                cqXA.DataType = oQuestion.dataType
                cqXA.reg_expression = oQuestion.regExpression
                cqXA.ConfirmationText = oQuestion.confirmationText
                cqXA.specialAccountTypes = oQuestion.specialAccountTypes
                cqXA.AccountPositionScope = ""
				If oQuestion.availAccountType = "PRIMARY" Then
					cqXA.AccountPositionScope = "P"
				ElseIf oQuestion.availAccountType = "SECONDARY" Then
					cqXA.AccountPositionScope = "S"
				End If
				oCQXA.Add(cqXA)
			Next
            ''_log.Info("download" & outputTesting & "XA Custom Questions XML --------------" & xmlDoc.InnerXml)
            ''_log.Info(outputTesting & " XA Custom Questions_NEWAPI List=====================" & New JavaScriptSerializer().Serialize(oCQNewAPI))
            ''_log.Info(outputTesting & "XA Custom Questions List: " & New JavaScriptSerializer().Serialize(oCQXA))
            Return oCQXA
        End Function

        Public Shared Function convertToCCustomQuestionXA(ByVal oAQs As List(Of CCustomQuestion)) As List(Of CCustomQuestionXA)
            Dim xaAQs As New List(Of CCustomQuestionXA)
            If oAQs.Count = 0 Then
                Return xaAQs
            End If
            For Each oQuestion In oAQs
                Dim cqXA As New CCustomQuestionXA()
                cqXA.Name = oQuestion.Name
                'cqXA.Position = oQuestion.position
                cqXA.Text = oQuestion.Text
                Dim answerType As String = ""
                Select Case oQuestion.UIType
                    Case QuestionUIType.TEXT
                        answerType = "TEXTBOX"
                    Case QuestionUIType.CHECK
                        answerType = "CHECKBOX"
                    Case QuestionUIType.DROPDOWN
                        answerType = "DROPDOWN"
                    Case QuestionUIType.HIDDEN
                        answerType = "HIDDEN"
                    Case QuestionUIType.RADIO
                        answerType = "RADIO"
                End Select

                cqXA.AnswerType = answerType
                cqXA.IsRequired = oQuestion.isRequired = "Y"

                cqXA.ErrorMessage = oQuestion.errorMessage
                cqXA.CustomQuestionOptions = oQuestion.CustomQuestionOptions
                cqXA.minLength = oQuestion.minLength
                cqXA.maxLength = oQuestion.maxLength
                cqXA.minValue = oQuestion.minValue
                cqXA.maxValue = oQuestion.maxValue
                cqXA.DataType = oQuestion.DataType
                cqXA.reg_expression = oQuestion.RegExpression
                cqXA.ConfirmationText = oQuestion.ConfirmationText
                xaAQs.Add(cqXA)
            Next
            Return xaAQs
        End Function

		Public Shared Function getAllowedCustomQuestionNames(ByVal oConfig As CWebsiteConfig, ByVal sLoanType As String, ByVal isApplicant As Boolean, ByVal isConditionQuestion As Boolean) As List(Of String)
			Dim ProductNames As New List(Of String)


			sLoanType = Common.GetConfigLoanTypeFromShort(sLoanType)

			Dim xPathCQ As String = sLoanType & "/CUSTOM_QUESTIONS/QUESTION"
			Dim xPathConCQ As String = sLoanType & "/CUSTOM_CONDITIONED_QUESTIONS/QUESTION"
			If isApplicant Then ''custom applicant questions
				xPathCQ = sLoanType & "/CUSTOM_APPLICANT_QUESTIONS/QUESTION"
				xPathConCQ = sLoanType & "/CUSTOM_APPLICANT_CONDITIONED_QUESTIONS/QUESTION"
			End If

			Dim xPath As String = xPathCQ
			If isConditionQuestion Then
				xPath = xPathConCQ
			End If

			Dim oNodes As XmlNodeList = oConfig._webConfigXML.SelectNodes(xPath)
			If oNodes.Count < 1 Then
				Return ProductNames
			End If

			For Each child As XmlNode In oNodes
				If child.Attributes("name").InnerXml <> "" Then
					ProductNames.Add(child.Attributes("name").InnerXml.ToLower)
				End If
			Next
			Return ProductNames
		End Function

		Public Shared Function getAllowedCustomQuestions(ByVal oConfig As CWebsiteConfig, ByVal sLoanType As String, ByVal isApplicant As Boolean) As List(Of CCustomQuestion)
			Dim customQuestions As New List(Of CCustomQuestion)
			Dim oCQs = getDownloadedCustomQuestions(oConfig, isApplicant, sLoanType)
			Dim sAllowedCQNames As List(Of String) = getAllowedCustomQuestionNames(oConfig, sLoanType, isApplicant, False)
			If oCQs.Count = 0 Then
				Return customQuestions
			End If

			If sAllowedCQNames.Count > 0 Then
				For Each oCQ As CCustomQuestion In oCQs
					If Not sAllowedCQNames.Contains(oCQ.Name.ToLower) Then
						Continue For
					End If
					customQuestions.Add(oCQ)
				Next
			Else
				Return oCQs
			End If

			Return customQuestions
		End Function

		Public Shared Function getConditionedQuestions(ByVal oConfig As CWebsiteConfig, ByVal sLoanType As String, ByVal isApplicant As Boolean, ByVal oConditionQuestions As List(Of CCustomQuestion)) As List(Of CCustomQuestion)
            Dim conQuestions As New List(Of CCustomQuestion)
            Dim oCQs = getDownloadedCustomQuestions(oConfig, isApplicant, sLoanType)
            If oConditionQuestions.Count = 0 Then
                Return conQuestions
            End If

            For Each conCQ In oConditionQuestions
                For Each oCQ In oCQs
                    If conCQ.Name.ToLower = oCQ.Name.ToLower Then
                        oCQ.Conditioning = conCQ.Conditioning
                        conQuestions.Add(oCQ)
                        Exit For
                    End If
                Next
            Next

            Return conQuestions
        End Function

		'     Public Shared Function getFilterApplicantQuestions(ByVal oFilterAQs As List(Of String), ByVal oAQNames As List(Of CApplicantQA)) As List(Of CApplicantQA)
		'         Dim currentAQs As New List(Of CApplicantQA)
		'         For Each oAQ In oAQNames
		'	If oFilterAQs.Any(Function(Name As String) Name.ToLower = oAQ.CQName.ToLower) Then
		'		currentAQs.Add(oAQ)
		'	End If
		'Next
		'         Return currentAQs
		'     End Function
		'     Public Shared Function getFilterCustomQuestionAnswerList(ByVal oFilterCQs As List(Of String), ByVal oCQAnswers As List(Of CCustomQuestionAnswer)) As CCustomQuestionAnswerList
		'         Dim cqAnswers As New CCustomQuestionAnswerList()
		'         If oFilterCQs.Count = 0 Or oCQAnswers.Count = 0 Then
		'             Return cqAnswers
		'         End If
		'         For Each oCQAnswer As CCustomQuestionAnswer In oCQAnswers
		'             ''decode single quote '
		'             oCQAnswer.QuestionName = oCQAnswer.QuestionName.Replace("&#39;", "'")
		'             If oFilterCQs.Any(Function(Name As String) Name.ToLower = oCQAnswer.QuestionName.ToLower) Then
		'                 cqAnswers.Add(oCQAnswer)
		'             End If
		'         Next
		'         Return cqAnswers
		'     End Function

		'Public Shared Function getFilterCustomQuestionNames(ByVal oConfig As CWebsiteConfig, ByVal sLoanType As String, ByVal isApplicant As Boolean) As List(Of String)
		'	Dim oCustomQuestionNames As New List(Of String)
		'	Dim oAllowedCQNames As List(Of String) = getAllowedCustomQuestionNames(oConfig, sLoanType, isApplicant, False)
		'	'Dim oAllowedConCQNames As List(Of String) = getAllowedCustomQuestionNames(oConfig, sLoanType, isApplicant, True)

		'	If oAllowedCQNames.Count = 0 Then
		'		''get all available CQ names from download
		'		If sLoanType = "XA" Then
		'			Dim oXACQs = getDownloadedXACustomQuestions(oConfig, isApplicant, "XA")
		'			For Each oXACQ In oXACQs
		'				oAllowedCQNames.Add(oXACQ.Name)
		'			Next
		'		Else
		'			Dim oCQs = getDownloadedCustomQuestions(oConfig, isApplicant, sLoanType)
		'			For Each oCQ In oCQs
		'				oAllowedCQNames.Add(oCQ.Name)
		'			Next
		'		End If
		'	End If

		'	oCustomQuestionNames = oAllowedCQNames
		'	'If oAllowedConCQNames.Count = 0 Then
		'	'    Return oCustomQuestionNames
		'	'End If
		'	'''add conditioned questions to oCustomQuestion
		'	'For Each oCondCQ In oAllowedConCQNames
		'	'    ''filter custom question names
		'	'    If oCustomQuestionNames.Any(Function(Name As String) Name = oCondCQ) Then
		'	'        Continue For
		'	'    End If
		'	'    oCustomQuestionNames.Add(oCondCQ)
		'	'Next

		'	Return oCustomQuestionNames
		'End Function

		''' <summary>
		''' Given a list of answers from the Request, returns the Question paired with the answers if it's available in the configuration.
		''' For URL answers, it also returns the answers if the question exists (but not necessarily enabled).
		''' </summary>
		''' <param name="oConfig"></param>
		''' <param name="sLoanType"></param>
		''' <param name="sAvailability"></param>
		''' <param name="eRole"></param>
		''' <param name="lstUserAnswers">Answers from the GUI, answered by the user</param>
		''' <param name="lstUrlParaAnswers">Answers from the URL Parameters, not answers by the user</param>
		''' <returns></returns>
		Public Shared Function getValidatedCustomQuestionAnswers(ByVal oConfig As CWebsiteConfig,
																 ByVal sLoanType As String,
																 ByVal eRole As QuestionRole,
																 ByVal sAvailability As String,
																 ByVal lstUserAnswers As IEnumerable(Of CApplicantQA),
																 ByVal lstUrlParaAnswers As IEnumerable(Of CApplicantQA)) As List(Of CValidatedQuestionAnswers)

			If lstUserAnswers Is Nothing Then lstUserAnswers = New List(Of CApplicantQA)(0)
			If lstUrlParaAnswers Is Nothing Then lstUrlParaAnswers = New List(Of CApplicantQA)(0)

			Dim cqInfoList = getAllowedCQList(oConfig, sLoanType, eRole)

			Dim allowedCQs As List(Of CCustomQuestionXA)
			Dim isNewCQApi As Boolean = Common.getVisibleAttribute(oConfig, "custom_question_new_api") = "Y"
			If isNewCQApi Then
				allowedCQs = getDownloadedXACustomQuestions(oConfig, eRole = QuestionRole.Applicant, sLoanType, sAvailability:=sAvailability, bIsUrlParaCustomQuestion:=False).
					Where(Function(cq) cqInfoList.IsAllowed(cq)).ToList()
			Else
				If eRole = QuestionRole.Application Then
					allowedCQs = convertToCCustomQuestionXA(oConfig.GetCustomQuestions(Common.GetConfigLoanTypeFromShort(sLoanType)))
				Else
					allowedCQs = CCustomQuestionXA.CurrentXAApplicantQuestions(oConfig, sAvailability)
				End If
			End If

			Dim validatedCQAnswers As List(Of CValidatedQuestionAnswers) = CValidatedQuestionAnswers.CollectValidatedQuestionAnswers(allowedCQs, lstUserAnswers)

			If isNewCQApi AndAlso lstUrlParaAnswers.Any() Then
				' For URL Parameter answers, don't check against allows CQs. Just if it exists.
				Dim allCQs As List(Of CCustomQuestionXA) = getDownloadedXACustomQuestions(oConfig, eRole = QuestionRole.Applicant, sLoanType, sAvailability:=sAvailability, bIsUrlParaCustomQuestion:=True)
				validatedCQAnswers.AddRange(CValidatedQuestionAnswers.CollectValidatedQuestionAnswers(allCQs, lstUrlParaAnswers))
			End If

			Return validatedCQAnswers
		End Function

		Public Shared Function getAllowedCQList(oConfig As CWebsiteConfig, sLoanType As String, eRole As QuestionRole) As CQInformationList
			sLoanType = Common.GetConfigLoanTypeFromShort(sLoanType)

			Dim xPathCQ As String = sLoanType & "/CUSTOM_QUESTIONS/QUESTION"
			If eRole = QuestionRole.Applicant Then ''custom applicant questions
				xPathCQ = sLoanType & "/CUSTOM_APPLICANT_QUESTIONS/QUESTION"
			End If

			Dim oNodes As XmlNodeList = oConfig._webConfigXML.SelectNodes(xPathCQ)
			Dim lstCQInfo As New List(Of CQInformation)
			For Each xEl As XmlElement In oNodes
				lstCQInfo.Add(New CQInformation(xEl, eRole))
			Next

			Return New CQInformationList(lstCQInfo)
		End Function
	End Class

End Namespace