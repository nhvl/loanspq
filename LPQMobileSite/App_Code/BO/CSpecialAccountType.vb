﻿
Public Class CSpecialAccountType
	Public Property AccountName As String
	Public Property AccountCode As String
	Public Property IsActive As Boolean
	Public Property ShowSpecialInfo As Boolean
	Public Property AutoCreateApplicants As Boolean
	Public Property HideBeneficiaryPage As Boolean
	Public Property EnableOnConsumer As Boolean
	Public Property Roles As List(Of CSpecialAccountRole)
End Class
