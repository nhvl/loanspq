﻿Imports Microsoft.VisualBasic
Imports System.Xml
Imports LPQMobile.Utils
Imports System.Web.Script.Serialization
<Serializable()> _
Public Class CFOMQuestionField
	Public Type As String
    Public labelText As String '' for FOM_V2
    Private _Answers As List(Of CFOMQuestionAnswer)
    Public CustomValidations As String
	Public Property Answers() As List(Of CFOMQuestionAnswer)
		Get
			If _Answers Is Nothing Then
				_Answers = New List(Of CFOMQuestionAnswer)
			End If
			Return _Answers
		End Get
		Set(ByVal value As List(Of CFOMQuestionAnswer))
			_Answers = value
		End Set
	End Property

	Public Sub New()

	End Sub

	Public Sub New(ByVal oNode As XmlElement)
		Me.Type = oNode.GetAttribute("type")
        Me.labelText = Common.SafeString(oNode.GetAttribute("label_text")) '' for FOM_V2

		For Each ansNode As XmlElement In oNode.SelectNodes("answers/answer")
			Dim anw As New CFOMQuestionAnswer(ansNode)
			Me.Answers.Add(anw)
        Next
        Dim customValidationDic = New Dictionary(Of String, String)
        If Me.Type.ToLower = "textbox" Then
            Dim customValidationNode As XmlNodeList = oNode.GetElementsByTagName("custom_validation")
            If customValidationNode.Count > 0 Then
                For Each oCVNode As XmlElement In customValidationNode
                    Dim sType = Common.SafeString(oCVNode.Attributes("type").InnerXml)
                    Dim sErrorMessage = Common.SafeString(oCVNode.Attributes("error_message").InnerXml)
                    If Not customValidationDic.ContainsKey(sType) And sType <> "" Then
                        customValidationDic.Add(sType, sErrorMessage)
                    End If
                Next
            End If
        End If
        Dim strValidation As String = ""
        If customValidationDic.Count > 0 Then
            strValidation = New JavaScriptSerializer().Serialize(customValidationDic)
        End If
        Me.CustomValidations = strValidation
	End Sub
End Class
