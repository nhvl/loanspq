﻿<Serializable()>
Public Class CBABusinessInfo
	Public Property BusinessType As String
	Public Property AccountCode As String
	Public Property CompanyName As String
	Public Property TaxID As String
	Public Property Industry As String
	Public Property EstablishDate As Date
	Public Property BusinessDescription As String
	Public Property StateRegistered As String
	Public Property BusinessStatus As String
	Public Property AnnualRevenue As Double
	Public Property NumberOfEmployees As Integer
	Public Property PrimaryBank As String
	Public Property DoingBusinessAs As String
	Public Property EmailAddr As String
	Public Property MobilePhone As String
	Public Property MobilePhoneCountry As String
	Public Property HomePhone As String
	Public Property HomePhoneCountry As String
	Public Property Fax As String
	Public Property WebsiteURL As String
	Public Property FaxCountry As String
	Public Property AddressStreet As String
	Public Property AddressZip As String
	Public Property AddressCity As String
	Public Property AddressState As String
	Public Property AddressCountry As String
	Public Property HasMailingAddress As String
	Public Property MailingAddressStreet As String
	Public Property MailingAddressZip As String
	Public Property MailingAddressCity As String
	Public Property MailingAddressState As String
	Public Property MailingAddressCountry As String

	Public Property HasPreviousAddress As Boolean
	Public Property PreviousAddressStreet As String
	Public Property PreviousAddressZip As String
	Public Property PreviousAddressCity As String
	Public Property PreviousAddressState As String
	Public Property PreviousAddressCountry As String
	Public Property OccupyingLocation As String
	Public Property OccupancyDuration As String
	Public Property OccupancyDescription As String


End Class
