﻿Imports Microsoft.VisualBasic
Imports System.Xml
Imports System.Linq
Imports System.Net
Imports System.IO
Imports System.Web.Script.Serialization
Imports LPQMobile.Utils
Imports System.Text
Public Class CWalletQuestionsRendering
        Inherits CBasePage

        ' Public _CurrentWebsiteConfig As CWebsiteConfig

        Dim walletXMLDoc As XmlDocument
        Public questionsList As List(Of questionDataClass)
    Dim isPopulated As Boolean
    Public idaMethodType As String
        <Serializable()> _
        Public Class questionDataClass
            Public questionText As String
            Public questionID As String
            Public choicesList As List(Of choiceClass)
        End Class
        <Serializable()> _
        Public Class choiceClass
            Public choiceText As String
            Public choiceID As String
        End Class

    Sub New(ByVal walletXML As String, ByVal sIDAMethodType As String)
        ' Get the wallet questions XML
        walletXMLDoc = New XmlDocument()
        walletXMLDoc.LoadXml(walletXML)
        questionsList = New List(Of questionDataClass)
        isPopulated = False
        idaMethodType = sIDAMethodType.ToUpper()
    End Sub

        Public ReadOnly Property NumQuestions() As Integer
            Get
                Return questionsList.Count
            End Get
        End Property
        ' Read the XML data and strip out the relevant pieces of data
        Public Sub populateQuestions(ByRef transaction_ID As String, ByRef question_set_ID As String, ByRef request_app_ID As String, _
                                     ByRef request_client_ID As String, ByRef request_sequence_ID As String, ByRef reference_number As String)
            ' We need to figure out which type of authentication to use
        Select Case idaMethodType
            Case "RSA"
                'easier to have all the parameters)
                populateQuestionsRSA(transaction_ID, question_set_ID, request_app_ID, request_client_ID, request_sequence_ID)
            Case "PID"
                populateQuestionsPID(transaction_ID, question_set_ID, request_app_ID, request_client_ID, request_sequence_ID)
            Case "FIS"
                populateQuestionsFIS(transaction_ID, question_set_ID, request_app_ID, request_client_ID, request_sequence_ID)
            Case "DID"
                populateQuestionsDID(transaction_ID, question_set_ID, request_app_ID, request_client_ID, request_sequence_ID)
                'equifax
            Case "EID"
                populateQuestionsEquifax(transaction_ID, question_set_ID, request_app_ID, request_client_ID, request_sequence_ID)
            Case "TID"
                populateQuestionsTID(transaction_ID, question_set_ID, request_app_ID, request_client_ID, request_sequence_ID, reference_number)
        End Select
        End Sub

        Protected Sub populateQuestionsPID(ByRef transaction_ID As String, ByRef question_set_ID As String, ByRef request_app_ID As String, _
                                   ByRef request_client_ID As String, ByRef request_sequence_ID As String)
            ' This is used for the PID authentication method.  
            ' Also need to get the transaction_id and the question_set_id
            Dim request_node As XmlNode = walletXMLDoc.SelectSingleNode("/OUTPUT/RESPONSE")
            transaction_ID = request_node.Attributes("session_id").InnerXml

            ' Get all the nodes that have the QUESTION element
            Dim child_nodes As XmlNodeList = walletXMLDoc.GetElementsByTagName("QUESTION")
            questionsList.Clear()

            If child_nodes.Count < 1 Then
                ' Raise an exception and log the error
            End If

            ' Now we know how many questions there are
            Dim numQuestions As Integer = child_nodes.Count
            Dim valueStr As String = ""

            Dim tempQuestion As questionDataClass
            Dim tempChoice As choiceClass
            Dim questionIndex As Integer = 1
            Dim answerIndex As Integer = 1
            For Each child As XmlNode In child_nodes
                ' What we need to do is grab the text for each question along with the "answers"
                ' We also need to save the question_id for each question and the choice_id for each choice
                tempQuestion = New questionDataClass
                tempQuestion.questionText = child.Attributes("question_text").InnerXml
                ' PID doesn't use question ID's, so we'll set this to zero
                tempQuestion.questionID = questionIndex
                tempQuestion.choicesList = New List(Of choiceClass)

                questionIndex = questionIndex + 1

                ' Now we have to grab all the choices
                Dim choicesXML As New XmlDocument
                choicesXML.LoadXml(child.OuterXml)
                Dim choiceNodes As XmlNodeList = choicesXML.GetElementsByTagName("ANSWER")
                ' Loop through all the elements with the "CHOICE" tag in them and get the choice_id and text
                For Each choice As XmlNode In choiceNodes
                    tempChoice = New choiceClass
                    tempChoice.choiceText = choice.Attributes("answer_text").InnerXml
                    tempChoice.choiceID = answerIndex
                    ' Add the choice to the list of choices
                    tempQuestion.choicesList.Add(tempChoice)

                    answerIndex = answerIndex + 1
                Next
                ' Now that the question and choices are grouped together, add them to the main questions list
                questionsList.Add(tempQuestion)
                ' Reset the answerIndex for the next set of answers
                answerIndex = 1
            Next

            ' So we now have all the questions and their choices.
            isPopulated = True
    End Sub
    'Equifax
    Protected Sub populateQuestionsEquifax(ByRef transaction_ID As String, ByRef question_set_ID As String, ByRef request_app_ID As String, _
                                 ByRef request_client_ID As String, ByRef request_sequence_ID As String)
        'this is used for the Equifax authentication method
        'also need to get the transaction_id and 
        Dim request_node As XmlNode = walletXMLDoc.SelectSingleNode("/OUTPUT/RESPONSE")
        If request_node IsNot Nothing Then
            transaction_ID = request_node.Attributes("transaction_id").InnerXml
        End If
        Dim child_nodes As XmlNodeList = walletXMLDoc.GetElementsByTagName("QUESTION")
        questionsList.Clear()

        If (child_nodes.Count < 1) Then
            'raised an exception and log the error
        End If
        'number of question
        Dim numQuestions As Integer = child_nodes.Count
        Dim valuesStr As String = ""
        Dim tempQuestion As questionDataClass
        Dim tempChoice As choiceClass
        For Each child As XmlNode In child_nodes
            'we need to grab each question along with the choices, and also we need to save 
            'the question_number and the choice_number
            tempQuestion = New questionDataClass
            tempQuestion.questionText = child.Attributes("question_text").InnerXml
            tempQuestion.questionID = child.Attributes("question_number").InnerXml
            tempQuestion.choicesList = New List(Of choiceClass)
            'for each question grap all the choices
            Dim choicesXML As New XmlDocument
            choicesXML.LoadXml(child.OuterXml)
            Dim choiceNodes As XmlNodeList = choicesXML.GetElementsByTagName("CHOICE")
            ' Loop through all the elements with the "CHOICE" tag in them and get the choice_id and text
            For Each choice As XmlNode In choiceNodes
                tempChoice = New choiceClass
                tempChoice.choiceText = choice.Attributes("choice_text").InnerXml
                tempChoice.choiceID = choice.Attributes("choice_number").InnerXml
                ' Add the choice to the list of choices
                tempQuestion.choicesList.Add(tempChoice)
            Next
            ' Now that the question and choices are grouped together, add them to the main questions list
            questionsList.Add(tempQuestion)
        Next
        ' So we now have all the questions and their choices.
        isPopulated = True
    End Sub

    Protected Sub populateQuestionsRSA(ByRef transaction_ID As String, ByRef question_set_ID As String, ByRef request_app_ID As String, _
                                 ByRef request_client_ID As String, ByRef request_sequence_ID As String)
        ' This is used for the RSA authentication method.  
        ' Also need to get the transaction_id and the question_set_id
        Dim request_node As XmlNode = walletXMLDoc.SelectSingleNode("/OUTPUT/RESPONSE")
        transaction_ID = request_node.Attributes("transaction_id").InnerXml
        question_set_ID = request_node.Attributes("question_set_id").InnerXml

        Dim child_nodes As XmlNodeList = walletXMLDoc.GetElementsByTagName("QUESTION")
        questionsList.Clear()

        If child_nodes.Count < 1 Then
            ' Raise an exception and log the error
        End If

        ' Now we know how many questions there are
        Dim numQuestions As Integer = child_nodes.Count
        Dim valueStr As String = ""

        Dim tempQuestion As questionDataClass
        Dim tempChoice As choiceClass
        For Each child As XmlNode In child_nodes
            ' What we need to do is grab the text for each question along with the choices
            ' We also need to save the question_id for each question and the choice_id for each choice
            tempQuestion = New questionDataClass
            tempQuestion.questionText = child.Attributes("text").InnerXml
            tempQuestion.questionID = child.Attributes("question_id").InnerXml
            tempQuestion.choicesList = New List(Of choiceClass)

            ' Now we have to grab all the choices
            Dim choicesXML As New XmlDocument
            choicesXML.LoadXml(child.OuterXml)
            Dim choiceNodes As XmlNodeList = choicesXML.GetElementsByTagName("CHOICE")
            ' Loop through all the elements with the "CHOICE" tag in them and get the choice_id and text
            For Each choice As XmlNode In choiceNodes
                tempChoice = New choiceClass
                tempChoice.choiceText = choice.Attributes("text").InnerXml
                tempChoice.choiceID = choice.Attributes("choice_id").InnerXml
                ' Add the choice to the list of choices
                tempQuestion.choicesList.Add(tempChoice)
            Next
            ' Now that the question and choices are grouped together, add them to the main questions list
            questionsList.Add(tempQuestion)
        Next

        ' So we now have all the questions and their choices.
        isPopulated = True
    End Sub

    Protected Sub populateQuestionsFIS(ByRef transaction_ID As String, ByRef question_set_ID As String, ByRef request_app_ID As String, _
                                 ByRef request_client_ID As String, ByRef request_sequence_ID As String)
        ' This is used for the PID authentication method.  
        ' Also need to get the transaction_id and the question_set_id
        Dim request_node As XmlNode = walletXMLDoc.SelectSingleNode("/OUTPUT/RESPONSE")
        question_set_ID = request_node.Attributes("quiz_id").InnerXml
        transaction_ID = request_node.Attributes("transaction_id").InnerXml

        ' Get all the nodes that have the QUESTION element
        Dim child_nodes As XmlNodeList = walletXMLDoc.GetElementsByTagName("QUESTION")
        questionsList.Clear()

        If child_nodes.Count < 1 Then
            ' Raise an exception and log the error
        End If

        ' Now we know how many questions there are
        Dim numQuestions As Integer = child_nodes.Count
        Dim valueStr As String = ""

        Dim tempQuestion As questionDataClass
        Dim tempChoice As choiceClass
        Dim questionIndex As Integer = 1
        Dim answerIndex As Integer = 1
        For Each child As XmlNode In child_nodes
            ' What we need to do is grab the text for each question along with the "answers"
            ' We also need to save the question_id for each question and the choice_id for each choice
            tempQuestion = New questionDataClass
            tempQuestion.questionID = child.Attributes("id").InnerXml
            tempQuestion.questionText = child.Attributes("question_text").InnerXml
            tempQuestion.choicesList = New List(Of choiceClass)

            questionIndex = questionIndex + 1

            ' Now we have to grab all the choices
            Dim choicesXML As New XmlDocument
            choicesXML.LoadXml(child.OuterXml)
            Dim choiceNodes As XmlNodeList = choicesXML.GetElementsByTagName("ANSWER")
            ' Loop through all the elements with the "CHOICE" tag in them and get the choice_id and text
            For Each choice As XmlNode In choiceNodes
                tempChoice = New choiceClass
                tempChoice.choiceID = choice.Attributes("id").InnerXml
                tempChoice.choiceText = choice.Attributes("text").InnerXml
                ' Add the choice to the list of choices
                tempQuestion.choicesList.Add(tempChoice)

                answerIndex = answerIndex + 1
            Next
            ' Now that the question and choices are grouped together, add them to the main questions list
            questionsList.Add(tempQuestion)
            ' Reset the answerIndex for the next set of answers
            answerIndex = 1
        Next

        ' So we now have all the questions and their choices.
        isPopulated = True
    End Sub

    Protected Sub populateQuestionsDID(ByRef transaction_ID As String, ByRef question_set_ID As String, ByRef request_app_ID As String, _
                                 ByRef request_client_ID As String, ByRef request_sequence_ID As String)
        ' This is used for the PID authentication method.  
        ' Also need to get the transaction_id and the question_set_id
        Dim request_node As XmlNode = walletXMLDoc.SelectSingleNode("/OUTPUT/RESPONSE")
        transaction_ID = request_node.Attributes("session_id").InnerXml
        request_app_ID = request_node.Attributes("request_app_id").InnerXml
        request_client_ID = request_node.Attributes("request_client_id").InnerXml
        request_sequence_ID = request_node.Attributes("request_sequence_id").InnerXml

        ' Get all the nodes that have the QUESTION element
        Dim child_nodes As XmlNodeList = walletXMLDoc.GetElementsByTagName("QUESTION")
        questionsList.Clear()

        If child_nodes.Count < 1 Then
            ' Raise an exception and log the error
        End If

        ' Now we know how many questions there are
        Dim numQuestions As Integer = child_nodes.Count
        Dim valueStr As String = ""

        Dim tempQuestion As questionDataClass
        Dim tempChoice As choiceClass
        Dim questionIndex As Integer = 1
        Dim answerIndex As Integer = 1
        For Each child As XmlNode In child_nodes
            ' What we need to do is grab the text for each question along with the "answers"
            ' We also need to save the question_id for each question and the choice_id for each choice
            tempQuestion = New questionDataClass
            tempQuestion.questionID = child.Attributes("question_number").InnerXml
            tempQuestion.questionText = child.Attributes("question_text").InnerXml
            tempQuestion.choicesList = New List(Of choiceClass)

            questionIndex = questionIndex + 1

            ' Now we have to grab all the choices
            Dim choicesXML As New XmlDocument
            choicesXML.LoadXml(child.OuterXml)
            Dim choiceNodes As XmlNodeList = choicesXML.GetElementsByTagName("ANSWER")
            ' Loop through all the elements with the "CHOICE" tag in them and get the choice_id and text
            For Each choice As XmlNode In choiceNodes
                tempChoice = New choiceClass
                tempChoice.choiceID = answerIndex
                tempChoice.choiceText = choice.Attributes("answer_text").InnerXml
                ' Add the choice to the list of choices
                tempQuestion.choicesList.Add(tempChoice)

                answerIndex = answerIndex + 1
            Next
            ' Now that the question and choices are grouped together, add them to the main questions list
            questionsList.Add(tempQuestion)
            ' Reset the answerIndex for the next set of answers
            answerIndex = 1
        Next

        ' So we now have all the questions and their choices.
        isPopulated = True
    End Sub
    'TransUnion IDMA
    Protected Sub populateQuestionsTID(ByRef transaction_ID As String, ByRef question_set_ID As String, ByRef request_app_ID As String, _
                                 ByRef request_client_ID As String, ByRef request_sequence_ID As String, ByRef reference_number As String)
        'this is used for the Equifax authentication method
        'also need to get the transaction_id and 
        Dim request_node As XmlNode = walletXMLDoc.SelectSingleNode("/OUTPUT/RESPONSE")
        If request_node IsNot Nothing Then
            reference_number = request_node.Attributes("reference_number").InnerXml
        End If
        Dim child_nodes As XmlNodeList = walletXMLDoc.GetElementsByTagName("QUESTION")
        questionsList.Clear()

        If (child_nodes.Count < 1) Then
            'raised an exception and log the error
        End If
        'number of question
        Dim numQuestions As Integer = child_nodes.Count
        Dim valuesStr As String = ""
        Dim tempQuestion As questionDataClass
        Dim tempChoice As choiceClass
        For Each child As XmlNode In child_nodes
            'we need to grab each question along with the choices, and also we need to save 
            'the display_name and the name
            tempQuestion = New questionDataClass
            tempQuestion.questionText = child.Attributes("display_name").InnerXml
            tempQuestion.questionID = child.Attributes("name").InnerXml
            tempQuestion.choicesList = New List(Of choiceClass)
            'for each question grap all the choices
            Dim choicesXML As New XmlDocument
            choicesXML.LoadXml(child.OuterXml)
            Dim choiceNodes As XmlNodeList = choicesXML.GetElementsByTagName("CHOICE")
            ' Loop through all the elements with the "CHOICE" tag in them and get the choice_id and text
            For Each choice As XmlNode In choiceNodes
                tempChoice = New choiceClass
                tempChoice.choiceText = choice.Attributes("display").InnerXml
                tempChoice.choiceID = choice.Attributes("key").InnerXml
                ' Add the choice to the list of choices
                tempQuestion.choicesList.Add(tempChoice)
            Next
            ' Now that the question and choices are grouped together, add them to the main questions list
            questionsList.Add(tempQuestion)
        Next
        ' So we now have all the questions and their choices.
        isPopulated = True
    End Sub

    Public Function RenderWalletQuestions(ByVal psFirstName As String, ByRef transaction_ID As String, ByRef question_set_ID As String, ByRef request_app_ID As String, _
                                     ByRef request_client_ID As String, ByRef request_sequence_ID As String, ByRef reference_number As String) As String
        ' If the questions have not been populated, then go and populate them first
        If Not isPopulated Then
            populateQuestions(transaction_ID, question_set_ID, request_app_ID, request_client_ID, request_sequence_ID, reference_number)
        End If
        ' The questions and choices are eseentially going to be rendered as radio buttons
        Dim I As Integer
        Dim numQuestions As Integer = questionsList.Count
        Dim sb As New StringBuilder
        sb.AppendLine(HeaderUtils.RenderPageTitle(0, psFirstName & ", please answer the following questions to help us verify your identity:", True))
        For I = 0 To numQuestions - 1
            'returnStr += "<fieldset data-role='controlgroup'>"
            '' Render the question text
            'returnStr += "<legend>"
            'returnStr += questionsList(I).questionText
            'returnStr += "</legend>"
            sb.AppendLine("<div>")
            sb.Append("<b style='margin-right:2px;'>" & I + 1 & ".</b>")
            sb.AppendLine(questionsList(I).questionText)
            sb.AppendLine("</div>")
            ' Render the choices
            Dim j As Integer
            Dim numChoices As Integer = questionsList(I).choicesList.Count
            For j = 0 To numChoices - 1
                ''sb.AppendLine("<input type='radio' name='walletquestion-radio-" & (I + 1).ToString() & "' class='walletquestion-radio-" + (I + 1).ToString())
                ''sb.Append("' id='rq" + (I + 1).ToString() + "_" + questionsList(I).choicesList(j).choiceID)
                ''sb.Append("' />")

                ''sb.AppendLine("<label for='rq" + (I + 1).ToString() + "_" + questionsList(I).choicesList(j).choiceID + "'>")
                ''sb.Append(questionsList(I).choicesList(j).choiceText)
                ''sb.Append("</label>")
                sb.AppendLine("<input type='radio' name='walletquestion-radio-" & (I + 1).ToString() & "' class='walletquestion-radio-" + (I + 1).ToString())
                sb.Append("' id='rq" + (I + 1).ToString() + "_" + (j + 1).ToString + "'")
                sb.Append("data-rq-value ='" + New JavaScriptSerializer().Serialize(questionsList(I).choicesList(j).choiceID) + "'")
                sb.Append("data-rq-key='" + questionsList(I).questionID + "'")
                sb.Append(" />")
                sb.Append("<label for='rq" + (I + 1).ToString() + "_" + (j + 1).ToString() + "'>")
                sb.Append(questionsList(I).choicesList(j).choiceText)
                sb.Append("</label>")
            Next
            'returnStr += "</fieldset></br>"
        Next
        Return sb.ToString()
    End Function
End Class

