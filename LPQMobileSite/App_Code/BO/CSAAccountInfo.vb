﻿<Serializable()>
Public Class CSAAccountInfo
	Public Property AccountTypeCode As String
	Public Property AccountName As String
	Public Property EstablishDate As Date
	Public Property SSN As String
	Public Property AddressStreet As String
	Public Property AddressCity As String
	Public Property AddressZip As String
	Public Property AddressState As String
	Public Property HasMailingAddress As String
	Public Property MailingAddressStreet As String
	Public Property MailingAddressCity As String
	Public Property MailingAddressZip As String
	Public Property MailingAddressState As String
	Public Property EmailAddr As String
	Public Property HomePhone As String
	Public Property HomePhoneCountry As String
	Public Property MobilePhone As String
	Public Property MobilePhoneCountry As String
	Public Property Fax As String
	Public Property FaxCountry As String
End Class
