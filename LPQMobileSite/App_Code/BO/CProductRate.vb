﻿Imports Microsoft.VisualBasic
<Serializable()> _
Public Class CProductRate
	Public Property RateCode() As String
	Public Property RateString() As String
	Public Property RateDecimal() As Decimal
	Public Property Apy() As Decimal
	Public Property MinBalance() As Decimal
    Public Property MinDeposit() As Decimal
	Public Property MaxDeposit() As Decimal
	Public Property MinTerm As Integer
	Public Property MaxTerm As Integer
	Public Property Term() As Integer
	Private _id As String
	Public ReadOnly Property ID As String
		Get
			Return _id
		End Get
	End Property
	Public Sub New()
		_id = Guid.NewGuid().ToString().Replace("-", "").Substring(0, 12)
	End Sub

	Public Property LocationPool() As String
	Public Property ZipCodeFilterType As String
End Class
