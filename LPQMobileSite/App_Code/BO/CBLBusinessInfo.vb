﻿Imports Microsoft.VisualBasic

Public Class CBLBusinessInfo
    Inherits CBABusinessInfo

    ''extra properties for business loans(vehicle,credit card and installment)
    Public Property MemberNumber As String
    Public Property TotalExistingLoanBalance As Double
    Public Property TotalExistingMonthlyPayment As Double
    Public Property TotalCheckingAccountBalance As Double
    Public Property GrossMonthlyIncome As Double
    Public Property NetMonthlyIncome As Double
    Public Property OtherBusinessDescription As String
    Public Property BusinessEntity As String

End Class
