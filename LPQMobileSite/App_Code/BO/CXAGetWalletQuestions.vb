﻿Imports Microsoft.VisualBasic
Imports System.Xml
Imports LPQMobile.Utils
Imports System.Text

Public Class CXAGetWalletQuestions

	Private websiteConfig As CWebsiteConfig
	Private isJoint As Boolean
	Private loanID As String = ""
	Public Sub New(websiteConfig As CWebsiteConfig, isJoint As Boolean, loanID As String)
		Me.websiteConfig = websiteConfig
		Me.isJoint = isJoint
		Me.loanID = loanID
	End Sub

	Public Function GetOutOfWalletQuestionsRSA() As String
		If (loanID = "") Then Return ""

		' Now construct the XML response
		Dim sb As New StringBuilder()
		Dim writer As XmlWriter = XmlTextWriter.Create(sb)
		writer.WriteStartDocument()

		' begin input
		writer.WriteStartElement("INPUT")

		writer.WriteStartElement("LOGIN")
		writer.WriteAttributeString("api_user_id", websiteConfig.APIUser)
		writer.WriteAttributeString("api_password", websiteConfig.APIPassword)
		writer.WriteEndElement()

		writer.WriteStartElement("REQUEST")
		writer.WriteAttributeString("app_id", loanID)
		writer.WriteAttributeString("applicant_index", "0")
		writer.WriteAttributeString("is_joint", IIf(isJoint, "true", "false").ToString())
		writer.WriteEndElement()

		' end input
		writer.WriteEndElement()
		writer.Flush()
		writer.Close()

		Dim returnStr = sb.ToString()
		Return returnStr
	End Function

	Public Function GetOutOfWalletQuestionsPID() As String
		If (loanID = "") Then Return ""

		' Now construct the XML response
		Dim sb As New StringBuilder()
		Dim writer As XmlWriter = XmlTextWriter.Create(sb)
		writer.WriteStartDocument()

		' begin input
		writer.WriteStartElement("INPUT")

		writer.WriteStartElement("LOGIN")
		writer.WriteAttributeString("api_user_id", websiteConfig.APIUser)
		writer.WriteAttributeString("api_password", websiteConfig.APIPassword)

		writer.WriteEndElement()

		writer.WriteStartElement("REQUEST")
		writer.WriteAttributeString("app_id", loanID)
		writer.WriteAttributeString("applicant_index", "0")
		writer.WriteAttributeString("is_joint", IIf(isJoint, "true", "false").ToString())
		writer.WriteEndElement()

		' end input
		writer.WriteEndElement()
		writer.Flush()
		writer.Close()

		Dim returnStr = sb.ToString()
		Return returnStr
	End Function

	Public Function GetOutOfWalletQuestionsFIS() As String
		If (loanID = "") Then Return ""

		' Now construct the XML response
		Dim sb As New StringBuilder()
		Dim writer As XmlWriter = XmlTextWriter.Create(sb)
		writer.WriteStartDocument()

		' begin input
		writer.WriteStartElement("INPUT")

		writer.WriteStartElement("LOGIN")

		'FIS IDA ONLY work with lender level user
		If websiteConfig.APIUserLender <> "" Then
			writer.WriteAttributeString("api_user_id", websiteConfig.APIUserLender)
			writer.WriteAttributeString("api_password", websiteConfig.APIPasswordLender)
		Else
			writer.WriteAttributeString("api_user_id", websiteConfig.APIUser)
			writer.WriteAttributeString("api_password", websiteConfig.APIPassword)
		End If
		writer.WriteEndElement()
		writer.WriteStartElement("REQUEST")
		writer.WriteAttributeString("xpressappid", loanID)
		
		writer.WriteAttributeString("applicant_index", "0")
		writer.WriteAttributeString("applicant_type", IIf(isJoint, "JOINT", "PRIMARY").ToString()) 'primary is used for both primary and coborrower, there is no spouse     

		writer.WriteEndElement()

		' end input
		writer.WriteEndElement()
		writer.Flush()
		writer.Close()

		Dim returnStr = sb.ToString()
		Return returnStr
	End Function

	Public Function GetOutOfWalletQuestionsDID() As String
		If (loanID = "") Then Return ""

		' Now construct the XML response
		Dim sb As New StringBuilder()
		Dim writer As XmlWriter = XmlTextWriter.Create(sb)
		writer.WriteStartDocument()

		' begin input
		writer.WriteStartElement("INPUT")

		writer.WriteStartElement("LOGIN")
		writer.WriteAttributeString("api_user_id", websiteConfig.APIUser)
		writer.WriteAttributeString("api_password", websiteConfig.APIPassword)

		writer.WriteEndElement()

		writer.WriteStartElement("REQUEST")
		writer.WriteAttributeString("xpress_app_id", loanID)

		writer.WriteAttributeString("applicant_index", "0")
		writer.WriteAttributeString("is_joint", IIf(isJoint, "true", "false").ToString())
		writer.WriteEndElement()

		' end input
		writer.WriteEndElement()
		writer.Flush()
		writer.Close()

		Dim returnStr = sb.ToString()
		Return returnStr
	End Function

	'Equifax
	Public Function GetOutOfWalletQuestionsEquifax() As String
		If (loanID = "") Then Return ""

		' Now construct the XML response
		Dim sb As New StringBuilder()
		Dim writer As XmlWriter = XmlTextWriter.Create(sb)
		writer.WriteStartDocument()

		' begin input
		writer.WriteStartElement("INPUT")

		writer.WriteStartElement("LOGIN")
		writer.WriteAttributeString("api_user_id", websiteConfig.APIUser)
		writer.WriteAttributeString("api_password", websiteConfig.APIPassword)
		writer.WriteEndElement()

		writer.WriteStartElement("REQUEST")
		writer.WriteAttributeString("app_id", loanID)
		writer.WriteAttributeString("applicant_index", "0")
		writer.WriteAttributeString("is_joint", IIf(isJoint, "true", "false").ToString())
		writer.WriteEndElement()

		' end input
		writer.WriteEndElement()
		writer.Flush()
		writer.Close()

		Dim returnStr = sb.ToString()
		Return returnStr
	End Function
	''TransUnionIDMA
	Public Function GetOutOfWalletQuestionsTID() As String
		If (loanID = "") Then Return ""

		' Now construct the XML response
		Dim sb As New StringBuilder()
		Dim writer As XmlWriter = XmlTextWriter.Create(sb)
		writer.WriteStartDocument()

		' begin input
		writer.WriteStartElement("INPUT")

		writer.WriteStartElement("LOGIN")
		writer.WriteAttributeString("api_user_id", websiteConfig.APIUser)
		writer.WriteAttributeString("api_password", websiteConfig.APIPassword)
		writer.WriteEndElement()

		writer.WriteStartElement("REQUEST")
		writer.WriteAttributeString("xpressappid", loanID)
		writer.WriteAttributeString("mode", "IDA")
		writer.WriteAttributeString("applicant_index", "0")
		writer.WriteAttributeString("applicant_type", IIf(isJoint, "JOINT", "PRIMARY").ToString())
		writer.WriteEndElement()

		' end input
		writer.WriteEndElement()
		writer.Flush()
		writer.Close()

		Dim returnStr = sb.ToString()
		Return returnStr
	End Function

End Class
