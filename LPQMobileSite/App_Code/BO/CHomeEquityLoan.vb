﻿Imports Microsoft.VisualBasic
Imports System.Xml
Imports LPQMobile.Utils.Common
Imports LPQMobile.Utils

Namespace LPQMobile.BO

    Public Class CHomeEquityLoan
        Inherits CBaseLoanApp

        Public LoanTerm As Integer = 0
        Public MortgageLoanType As String
        Public MapHELOANPurposeToValue As String
        Public RequestedLoanAmount As Double
        ''EstimatePropertyValue input field is not shown to the consumer, however it is required in 
        '' loanpq(just initialize it to 1.0)
        Public EstimatedPropertyValue As Double
        Public FirstMortgageBalance As Double
        Public SecondMortgageBalance As Double
        Public PropertyOccupancyType As String
        Public DocType As String

        Public HazardInsurance As Double
        Public PropertyTaxes As Double
        Public MortgageInsurance As Double
        Public AssociationDues As Double
        '' Public OtherLoanPrincipleAndInterest As Double

        Public PropertyType As String
        Public PropertyAddress As String
        Public PropertyZip As String
        Public PropertyCity As String
        Public PropertyState As String

        'Public HasOutstandingJudgement As Boolean
        'Public HasBankruptcy As Boolean
        'Public HasBadLoan As Boolean
        'Public IsInLawsuit As Boolean
        'Public HasAlimony As Boolean
        'Public IsEndorserOnNote As Boolean
        'Public IsPropertyPrimaryResidence As Boolean
        'Public HasOwnershipInterest As Boolean
        'Public HasForeclosure As Boolean
        'Public HasPastDueBills As Boolean
        'Public HasCoMaker As Boolean
        'Public HasIncomeDecline As Boolean

		Public Property IsLineOfCredit As Boolean

        Public Overrides ReadOnly Property LoanType() As String
            Get
                Return "HE"
            End Get
        End Property

		Public Sub New(ByVal psOrganizationId As String, ByVal psLenderId As String)
			MyBase.New(psOrganizationId, psLenderId)
		End Sub
		Public Overrides Function GetXml(oConfig As CWebsiteConfig) As XmlDocument
			Dim oDoc As New XmlDocument()
            oDoc.LoadXml("<MORTGAGE_LOAN xmlns=""" & NAMESPACE_URI &
                         """ xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" " &
                         "xsi:schemaLocation=""http://www.meridianlink.com/CLF MORTGAGE_LOAN.xsd"" " &
                         "version=""5.181"" />") ''5.203, 5.070

            AddApplicantsXml(oDoc, oConfig)
			AddPropertyInfoXml(oDoc)
			AddLoanInfoXml(oDoc)
			AddLoanStatusXml(oDoc)
			AddDetailedTransactionXml(oDoc)
			AddFundingXml(oDoc)
			AddCommentsXml(oDoc)
            AddCustomQuestionsXml(oDoc)
            AddContactsXml(oDoc)
            AddSystemXml(oDoc)
			AddHDMA(oDoc)

            Return oDoc
		End Function


        Private Sub AddLoanInfoXml(ByVal poDoc As XmlDocument)
            Dim oxmlLoanInfo As XmlElement = CType(poDoc.DocumentElement.AppendChild(poDoc.CreateElement("LOAN_INFO", NAMESPACE_URI)), XmlElement)
            oxmlLoanInfo.SetAttribute("mortgage_loan_type", "OTHER")
            oxmlLoanInfo.SetAttribute("is_home_equity", "Y")
            oxmlLoanInfo.SetAttribute("refinance_purpose", LoanPurpose) ' map to LPQmobile reason

            If EstimatedPropertyValue > 0 Then
                oxmlLoanInfo.SetAttribute("estimated_property_value", EstimatedPropertyValue)
            End If
            oxmlLoanInfo.SetAttribute("amount_requested", RequestedLoanAmount)
            oxmlLoanInfo.SetAttribute("loan_term", LoanTerm.ToString())
            oxmlLoanInfo.SetAttribute("document_type", CEnum.MapDocType(DocType))
            ''add interview method(version 5.072)
            oxmlLoanInfo.SetAttribute("interview_method", "INTERNET")

            Dim oxmlHousingExpense As XmlElement = CType(oxmlLoanInfo.AppendChild(poDoc.CreateElement("PROPOSED_HOUSING_EXPENSE", NAMESPACE_URI)), XmlElement)
            '' oxmlHousingExpense.SetAttribute("other_pi", OtherLoanPrincipleAndInterest)
            oxmlHousingExpense.SetAttribute("hazard_insurance", HazardInsurance)
            oxmlHousingExpense.SetAttribute("real_estate_tax", PropertyTaxes)
            oxmlHousingExpense.SetAttribute("mortgage_insurance", MortgageInsurance)
            oxmlHousingExpense.SetAttribute("hoa_due", AssociationDues)

            Dim oxmlTD1 As XmlElement = CType(oxmlLoanInfo.AppendChild(poDoc.CreateElement("CURRENT_TRUST_DEED", NAMESPACE_URI)), XmlElement)
            oxmlTD1.SetAttribute("balance", FirstMortgageBalance)
            oxmlTD1.SetAttribute("position", "1")

            Dim oxmlTD2 As XmlElement = CType(oxmlLoanInfo.AppendChild(poDoc.CreateElement("CURRENT_TRUST_DEED", NAMESPACE_URI)), XmlElement)
            oxmlTD2.SetAttribute("balance", SecondMortgageBalance)
            oxmlTD2.SetAttribute("position", "2")

            Dim oxmlHEData As XmlElement = CType(oxmlLoanInfo.AppendChild(poDoc.CreateElement("HOME_EQUITY_DATA", NAMESPACE_URI)), XmlElement)
            ''home_equity_purpose_id is auto generated on lender site
            ''  oxmlHEData.SetAttribute("home_equity_purpose_id", MapHELOANPurposeToValue)
            oxmlHEData.SetAttribute("purpose_name", MortgageLoanType) ' map to LPQmobile Loan purpose



        End Sub




        Private Sub AddPropertyInfoXml(ByVal poDoc As XmlDocument)

            Dim oxmlPropertyInfo As XmlElement = CType(poDoc.DocumentElement.AppendChild(poDoc.CreateElement("PROPERTY_INFO", NAMESPACE_URI)), XmlElement)
            oxmlPropertyInfo.SetAttribute("occupancy_status", CEnum.MapPropertyOccupancyTypeToValue(PropertyOccupancyType))
            oxmlPropertyInfo.SetAttribute("property_type", PropertyType)
            oxmlPropertyInfo.SetAttribute("address", PropertyAddress.Trim().Replace(",", "").Replace(".", ""))
            oxmlPropertyInfo.SetAttribute("city", PropertyCity.Trim().Replace(",", "").Replace(".", ""))
            oxmlPropertyInfo.SetAttribute("state", PropertyState)
            oxmlPropertyInfo.SetAttribute("zip", PropertyZip)
            oxmlPropertyInfo.SetAttribute("business_commercial_purpose", "N")
        End Sub

        Private Sub AddDetailedTransactionXml(ByVal poDoc As XmlDocument)
            Dim oxmlDetailTransaction As XmlElement = CType(poDoc.DocumentElement.AppendChild(poDoc.CreateElement("DETAIL_TRANSACTION", NAMESPACE_URI)), XmlElement)

        End Sub

        Private Sub AddFundingXml(ByVal poDoc As XmlDocument)
            Dim oxmlFunding As XmlElement = CType(poDoc.DocumentElement.AppendChild(poDoc.CreateElement("FUNDING", NAMESPACE_URI)), XmlElement)
			Dim oxmlInsuranceOptions As XmlElement = CType(oxmlFunding.AppendChild(poDoc.CreateElement("INSURANCE_OPTIONS", NAMESPACE_URI)), XmlElement)


			If (SafeString(MortgageLoanType).ToUpper.IndexOf("HELOC") = -1 And SafeString(MortgageLoanType).ToUpper.IndexOf("LINE OF CREDIT") = -1) Then
				oxmlInsuranceOptions.SetAttribute("loan_class", "C")	 'need to set Loan System to closed-end 
			End If

			Dim oxmlInsuranceOption As XmlElement = CType(oxmlInsuranceOptions.AppendChild(poDoc.CreateElement("INSURANCE_OPTION", NAMESPACE_URI)), XmlElement)
			oxmlInsuranceOption.SetAttribute("description", "NO INSURANCE")

		End Sub

        Private Sub AddCommentsXml(ByVal poDoc As XmlDocument)
            Dim oxmlComments As XmlElement = CType(poDoc.DocumentElement.AppendChild(poDoc.CreateElement("COMMENTS", NAMESPACE_URI)), XmlElement)
            Dim oxmlDecisionComments As XmlElement = CType(oxmlComments.AppendChild(poDoc.CreateElement("DECISION_COMMENTS", NAMESPACE_URI)), XmlElement)
            Dim oxmlExternalComments As XmlElement = oxmlComments.AppendChild(poDoc.CreateElement("EXTERNAL_COMMENTS", NAMESPACE_URI))
            Dim oxmlInternalComments As XmlElement = CType(oxmlComments.AppendChild(poDoc.CreateElement("INTERNAL_COMMENTS", NAMESPACE_URI)), XmlElement)
            If HasDebtCancellation Then
                oxmlExternalComments.InnerText = HasDebtCancellationText
            End If
            ''''write internal comment element:  disclosures and originating ip address
			oxmlInternalComments.InnerText += CurrentEmail + " (" + DateTime.Now + "): Disclosure(s) Checked: " + Disclosure
            oxmlInternalComments.InnerText += Environment.NewLine
            oxmlInternalComments.InnerText += "SYSTEM (" + DateTime.Now + "): Originating IP Address: " + IPAddress
            oxmlInternalComments.InnerText += Environment.NewLine
            oxmlInternalComments.InnerText += "SYSTEM (" + DateTime.Now + "): USER AGENT: " + UserAgent
            oxmlInternalComments.InnerText += Environment.NewLine

            ''add cuna question answers in the internal comment if it exists
            If cunaQuestionAnswers.Count > 0 Then
                Dim i As Integer
                oxmlInternalComments.InnerText += Environment.NewLine
                oxmlInternalComments.InnerText += Environment.NewLine
                oxmlInternalComments.InnerText += "SYSTEM (" + DateTime.Now + "): PROTECTION INFORMATION: "
                oxmlInternalComments.InnerText += Environment.NewLine
                oxmlInternalComments.InnerText += Environment.NewLine
                For i = 0 To cunaQuestionAnswers.Count - 1
                    oxmlInternalComments.InnerText += cunaQuestionAnswers(i)
                    oxmlInternalComments.InnerText += Environment.NewLine
                Next
            End If
            ''add xSell comment message to internal comment if it exist
            If xSellCommentMessage <> "" Then
                oxmlInternalComments.InnerText += "SYSTEM (" + DateTime.Now + "): " & xSellCommentMessage
                oxmlInternalComments.InnerText += Environment.NewLine
            End If
            ''add instaTouch Prefill comment if it exist
            If InstaTouchPrefillComment <> "" Then
                oxmlInternalComments.InnerText += "SYSTEM (" + DateTime.Now + "): " & InstaTouchPrefillComment
                oxmlInternalComments.InnerText += Environment.NewLine
            End If
        End Sub

    End Class

End Namespace
