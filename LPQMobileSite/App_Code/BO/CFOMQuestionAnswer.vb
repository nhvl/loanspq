﻿Imports Microsoft.VisualBasic
Imports System.Xml

<Serializable()> _
Public Class CFOMQuestionAnswer
	Public Text As String
    Public Value As String
    Public NextQuestion As String

	Public Sub New()

	End Sub

	Sub New(ByVal oNode As XmlElement)
		Me.Text = oNode.GetAttribute("text")
        Me.Value = oNode.GetAttribute("value")
        Me.NextQuestion = oNode.GetAttribute("next_question")
	End Sub
End Class
