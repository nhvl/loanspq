﻿Imports Microsoft.VisualBasic
<Serializable()> _
Public Class CProductService

    ''' <summary>
    ''' Name of the service
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Property ServiceType As String
    Public Property ServiceCode As String
    Public Property Description As String
    Public Property DescriptionURL As String
    Public Property IsActive As Boolean
    Public Property IsPreSelection As Boolean

    Public Property IsRequired As Boolean

End Class
