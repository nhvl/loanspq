﻿<Serializable()>
Public Class CBeneficiaryInfo
	Public Property firstName As String
	Public Property lastName As String
	Public Property relationship As String
	Public Property percentShare As String
	Public Property isTrust As String
	Public Property dob As String
	Public Property ssn As String
	Public Property phone As String
	Public Property address As String
	Public Property zip As String
	Public Property city As String
	Public Property state As String

End Class