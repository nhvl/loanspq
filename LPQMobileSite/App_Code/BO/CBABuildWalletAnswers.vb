﻿Imports Microsoft.VisualBasic
Imports LPQMobile.Utils

Public Class CBABuildWalletAnswers
	Inherits CSABuildWalletAnswers
	Public Sub New(websiteConfig As CWebsiteConfig, isJoint As Boolean, loanID As String, transactionID As String, questionIDList As List(Of String), applicantIndex As Integer)
		MyBase.New(websiteConfig, isJoint, loanID, transactionID, questionIDList, applicantIndex)
		'for now, there is no different between BA and SA. This class is for later use. To prevent duplated code, I make CBAGetWalletQuestions inherits from CSAGetWalletQuestions
	End Sub
End Class
