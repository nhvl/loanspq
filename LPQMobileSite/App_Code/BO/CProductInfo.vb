﻿
Imports System.Text.RegularExpressions

Public Class ProductInfo

	Private _log As log4net.ILog = log4net.LogManager.GetLogger(Me.GetType)
	Public Property ProductName As String
	Public Property ProductMinDeposit As String
	Public Property ProductMaxDeposit As String
	Public Property ProductAPY As String
	Public Property ProductIsRequired As String
	Public Property ProductIsPreselected As String
	Public Property ProductCode As String
	Public Property AccountType As String
	Public Property OriginalAccountType As String
	Public Property ProductDescription As String = ""
	Public Property ProductServices As List(Of CProductService)
	Public Property MinorAccountCode As String
	Public Property CustomQuestions As List(Of CProductCustomQuestion)
	Public Property PreSelectedValue As String
	Public Property EntityType As String
	Public Property ZipCodeFilterType As String
	Public Property ZipCodePoolId As String
	Public Property ProductRates As List(Of CProductRate)
	Public Property NoFund As Boolean
	Public Property Position As Integer
	Public Property IsAutoCalculateTier As Boolean
	Public Property IsApyAutoCalculated As Boolean
	Public Property TermType As String


	''' <summary>
	''' Return "Show more..."" if description is long and product has service or question
	''' </summary>
	''' <returns></returns>
	''' <remarks></remarks>
	Public Function GetFilteredProductDescription() As String
		If ProductServices.Count > 0 Or CustomQuestions.Count > 0 Then

			'attributes in links are not part of the description total lenght so need to subtract them from the total
			'this regex is to remove anchor tags from ProductDescription. Because html tags will be rendered and shouldn't be count from the description length
			Dim regEx3 As New Regex("<\/?a((\s+(\w|\w[\w-]*\w)(\s*=\s*(?:\"".*?\""|'.*?'|[^'\"">\s]+))?)+\s*|\s*)\/?>")
			Dim tempStr As String = regEx3.Replace(ProductDescription, "")
			If tempStr.Length > 300 Then Return "<a href='#' data-mode='self-handle-event' data-command='showDetail'>Show more...</a>"
		End If
		Return ProductDescription
	End Function

End Class


''create a selectproducts class for new format(remove 3 limit accounts)
<Serializable()> _
Public Class SelectProducts
    Public productName As String
    Public depositAmount As String
    Public productQuestions As List(Of CProductCustomQuestion)
	Public productServices As List(Of String)
	Public productRate As String
	Public Property term As String
	Public Property apy As String
End Class