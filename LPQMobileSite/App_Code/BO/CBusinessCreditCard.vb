﻿Imports Microsoft.VisualBasic
Imports System.Xml
Imports LPQMobile.Utils.Common
Imports LPQMobile.Utils
Imports System.Web.Script.Serialization
Namespace LPQMobile.BO
    Public Class CBusinessCreditCard
        Inherits CCreditCard
        Public Overrides ReadOnly Property LoanType() As String
            Get
                Return "BL"
            End Get
        End Property

        Public Sub New(ByVal psOrganizationId As String, ByVal psLenderId As String)
            MyBase.New(psOrganizationId, psLenderId)
        End Sub

        Public Overrides Function GetXml(oConfig As CWebsiteConfig) As XmlDocument
            Dim oDoc As New XmlDocument()
            oDoc.LoadXml("<BUSINESS_LOAN  xmlns=""http://www.meridianlink.com/CLF"" version=""5.206"" />") ''5.070

            AddBusinessApplicantsXml(oDoc, oConfig)
            ''company infor
            AddBusinessInfoXml(oDoc)
            '' guarantors
            AddBusinessCreditCardXml(oDoc)
            AddLoanStatusXml(oDoc)
            AddFundingXml(oDoc)
            AddCommentsXml(oDoc)
            AddBusinessCustomQuestionsXml(oDoc, oConfig)
            AddSystemXml(oDoc)
            AddOtherSystemXml(oDoc)
            ''AddHDMA(oDoc)
            AddBeneficialOwnersXml(oDoc)
            Return oDoc
        End Function

        Protected Sub AddBusinessCreditCardXml(ByVal poDoc As XmlDocument)

            Dim oxmlLoanInfo As XmlElement = CType(poDoc.DocumentElement.AppendChild(poDoc.CreateElement("LOAN_INFO", NAMESPACE_URI)), XmlElement)
            '' <LOAN_INFO rate_code="" payment_due_day="15" max_underwrite_amount="0" epl_fees_total="0.00" is_claimed="N" is_complete_consumer="N"
            ' require_product_refresh="N" is_addon="N" is_amendment="N" solve_for="TERM" is_possible_dupe="N" is_workout_loan="N" is_solvefor_manual="N" tier="0"
            ' tier_previous="0" is_prequalification_applied="N" is_ofac="N" is_high_risk_consumer="N" purchase_real_estate_amount="0.00" purchase_business_amount="0.00" 
            'refinance_amount="0.00" renovate_amount="0.00" working_capital_amount="0.00" purchase_equipment_amount="0.00" other_amount="0.00" borrower_contribution="0.00" 
            'amount_requested="0.00" rate="0.000" loan_term="0" amount_approved="0.00" monthly_payment="0.00" number_of_payments="0" business_sub_type="CC" apr="0.000" 
            'intro_apr="0.000" minimum_payment="0.00" payment_percent="0.000" is_indirect_loan="N" is_balloon="N" loc_amount="0" is_LOC="Y" 
            'amount_approved_variance="0.00" use_of_proceeds="DSFDDS" />

            oxmlLoanInfo.SetAttribute("business_sub_type", "CC")
            oxmlLoanInfo.SetAttribute("business_purpose_type", LoanPurpose)
            '<CREDIT_CARD_INFO card_type="CREDIT" card_name_requested="Business Visa" has_balance_transfer="N" print_cards_onsite="N"
            ' apr_balance_transfer="0.000" intro_apr_balance_transfer="0.000" apr_cash_advances="0.000" intro_apr_cash_advances="0.000" apr_other="0.000"
            ' intro_apr_other="0.000" current_credit_limit="0.00">
            Dim oxmlCreditCardInfo As XmlElement = CType(poDoc.DocumentElement.AppendChild(poDoc.CreateElement("CREDIT_CARD_INFO", NAMESPACE_URI)), XmlElement)
			If LoanPurpose.ToUpper.Contains("INCREASE") Or LoanPurpose.ToUpper.Contains("LOWER") Then ''line increase
				oxmlCreditCardInfo.SetAttribute("card_type", "CREDIT")
				oxmlCreditCardInfo.SetAttribute("card_number", eCardNumber.Replace("-", ""))
			Else ''new card
				oxmlCreditCardInfo.SetAttribute("card_name_requested", CardName)
				''card_type is required, so if CardType is empty then set it to "CREDIT"
				If CardType = "" Then
					CardType = "CREDIT"
				End If
				oxmlCreditCardInfo.SetAttribute("card_type", CardType)
			End If
			If RequestAmount > 0 Then
				oxmlLoanInfo.SetAttribute("amount_requested", RequestAmount)
			End If
		End Sub
       
    End Class
End Namespace