﻿Imports Microsoft.VisualBasic
Imports System.Xml
Imports LPQMobile.Utils.Common
Imports LPQMobile.Utils
Imports System.Web.Script.Serialization
Namespace LPQMobile.BO
    Public Class CBusinessOtherLoan
        Inherits CPersonalLoan
        Public Overrides ReadOnly Property LoanType() As String
            Get
                Return "BL"
            End Get
        End Property

        Public Sub New(ByVal psOrganizationId As String, ByVal psLenderId As String)
            MyBase.New(psOrganizationId, psLenderId)
        End Sub
        Public Overrides Function GetXml(oConfig As CWebsiteConfig) As XmlDocument
            Dim oDoc As New XmlDocument()
            oDoc.LoadXml("<BUSINESS_LOAN  xmlns=""http://www.meridianlink.com/CLF"" version=""5.206"" />") ''5.070

            AddBusinessApplicantsXml(oDoc, oConfig)
            ''company infor
            AddBusinessInfoXml(oDoc)
            '' guarantors
            AddBusinessLoanInfoXml(oDoc)
            AddLoanStatusXml(oDoc)
            AddFundingXml(oDoc)
            AddCommentsXml(oDoc)
            AddBusinessCustomQuestionsXml(oDoc, oConfig)
            AddSystemXml(oDoc)
            AddOtherSystemXml(oDoc)
            ''AddHDMA(oDoc)

            AddBeneficialOwnersXml(oDoc)
            Return oDoc
        End Function

        Protected Sub AddBusinessLoanInfoXml(ByVal poDoc As XmlDocument)
            Dim oxmlLoanInfo As XmlElement = CType(poDoc.DocumentElement.AppendChild(poDoc.CreateElement("LOAN_INFO", NAMESPACE_URI)), XmlElement)
            oxmlLoanInfo.SetAttribute("business_sub_type", "OT")
            oxmlLoanInfo.SetAttribute("business_purpose_type", LoanPurpose)
            oxmlLoanInfo.SetAttribute("is_LOC", IIf(IsLOC, "Y", "N").ToString())
            If LoanTypeCategory <> "" Then
                oxmlLoanInfo.SetAttribute("security_option", LoanTypeCategory)
            End If
            If Not IsLOC Then
                oxmlLoanInfo.SetAttribute("loan_term", LoanTerm)
            End If
            oxmlLoanInfo.SetAttribute("amount_requested", LoanAmount)
        End Sub
    End Class
End Namespace