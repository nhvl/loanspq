﻿Imports Microsoft.VisualBasic

Namespace LPQMobile.BO

    Public Class CApplicant
        Public Spouse As CApplicant()
        Public EmployeeOfLender As String
        Public HasSpouse As Boolean = False
        Public FirstName As String
        Public MiddleInitial As String
        Public LastName As String
        Public Suffix As String
        Public SSN As String
		Public DOB As String
		Public MotherMaidenName As String
		''Identification info
		Public IDNumber As String
		Public IDType As String
		Public IDCountry As String
		Public IDState As String
		Public IDDateIssued As String
		Public IDExpirationDate As String

		Public MaritalStatus As String
		Public MembershipLengthMonths As String
		Public CitizenshipStatus As String
        Public HomePhone As String
        Public HomePhoneCountry As String
        Public WorkPhone As String
        Public WorkPhoneCountry As String
        Public WorkPhoneEXT As String
        Public CellPhone As String
        Public CellPhoneCountry As String
        Public Email As String
        Public PreferredContactMethod As String
        Public DriversLicenseNumber As String
        Public DriversLicenseState As String
		Public CurrentAddress As String
		Public CurrentAddress2 As String
        Public CurrentZip As String
        Public CurrentCity As String
        Public CurrentState As String
        Public CurrentCountry As String
		Public OccupancyType As String
		Public OccupancyDescription As String
        Public OccupancyDuration As Integer
        ''previous Address
        Public HasPreviousAddress As String
        Public PreviousAddress As String
        Public PreviousZip As String
        Public PreviousCity As String
		Public PreviousState As String
		Public PreviousCountry As String

		''Mailing Address
		Public HasMailingAddress As String
		Public MailingAddress As String
		Public MailingAddress2 As String
		Public MailingZip As String
		Public MailingCity As String
		Public MailingState As String
		Public MailingCountry As String

		Public EmploymentStatus As String
		Public EmploymentDescription As String
        'Public Employer As String
        'Public LengthOfEmployment As Integer
		Public GrossMonthlyIncome As Double
		Public GrossMonthlyIncomeOther As Double
		Public MonthlyIncomeOtherDescription As String
        Public TotalMonthlyHousingExpense As Double
		Public MemberNumber As String
		Public RelationshipToPrimary As String
        Public GrossMonthlyIncomeTaxExempt As String
        Public OtherMonthlyIncomeTaxExempt As String
        'Public Profession As String

        Public txtJobTitle As String
        Public ddlBranchOfService As String
        Public txtEmployer As String
        Public ddlPayGrade As String
        Public txtSupervisorName As String
        Public txtBusinessType As String
        Public txtEmploymentStartDate As String
        Public txtEmployedDuration_year As String
        Public txtEmployedDuration_month As String
        Public txtProfessionDuration_year As String
        Public txtProfessionDuration_month As String
        Public txtETS As String
        ''previous employment information
        Public hasPrevEmployment As String
        Public prev_GrossMonthlyIncome As Double
        Public prev_EmploymentStatus As String
        Public prev_txtJobTitle As String
        Public prev_ddlBranchOfService As String
        Public prev_txtEmployer As String
        Public prev_ddlPayGrade As String
        Public prev_txtBusinessType As String
        Public prev_txtEmploymentStartDate As String
        Public prev_txtEmployedDuration_year As String
        Public prev_txtEmployedDuration_month As String
        Public prev_txtETS As String
		'declaration
		Public Declarations As List(Of KeyValuePair(Of String, Boolean))
		Public AdditionalDeclarations As Dictionary(Of String, String)
		'Government Monitoring Information
        Public HasGMI As String
		'      Public Gender As String
		'      Public Ethnicity As String
		'      Public Race As String
		'Public DeclinedAnswerRaceGender As String
		Public SexNotProvided As String
		Public Sex As String
		Public EthnicityNotProvided As String
		Public EthnicityIsHispanic As String
		Public EthnicityIsNotHispanic As String
		Public EthnicityHispanic As String
		Public EthnicityHispanicOther As String
		Public RaceNotProvided As String
		Public RaceBase As String
		Public RaceTribeName As String
		Public RaceAsian As String
		Public RaceAsianOther As String
		Public RacePacificIslander As String
        Public RacePacificIslanderOther As String

        Public Assets As New List(Of CAssetInfo)

		''' <summary>
		''' Applicant question list for xaCombo
		''' </summary>
		Public ApplicantQAList As New List(Of CValidatedQuestionAnswers)

		''' <summary>
		''' Applicant questions answers for loans 
		''' </summary>
		Public ApplicantQuestionAnswers As New List(Of CValidatedQuestionAnswers)

		''reference information
		Public referenceInfor As New List(Of String)
    End Class

End Namespace
