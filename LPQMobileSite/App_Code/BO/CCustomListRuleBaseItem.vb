﻿Namespace CustomList
	Public Class CCustomListRuleBaseItem
		Public Property ID As String
		Public Property Rule As Object
		Public Property Expression As CCustomListRuleExpression
		Public Property Expression2 As CCustomListRuleExpression
		Public Property ParamInfo As List(Of KeyValuePair(Of String, String)) 'name, data type
		Public Property Enable As Boolean
	End Class
	Public Class CCustomListRuleExpression
		Public Property js As String
		Public Property vb As String
		Public Property params As Object

	End Class
	Public Class CBookingValidation
		Inherits CCustomListRuleBaseItem
		Public Property Name As String
	End Class
	Public Class CApplicantBlockRule
		Inherits CCustomListRuleBaseItem
		Public Property Name As String
	End Class
End Namespace
