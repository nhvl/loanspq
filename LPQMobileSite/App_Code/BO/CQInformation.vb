﻿Imports Microsoft.VisualBasic
Imports System.Xml

Imports LPQMobile.BO
Imports LPQMobile.Utils

Public Class CQInformation
	Public Property QuestionName As String
	Public Property IsEnable As Boolean
	Public Property DisplayPage As CustomQuestionLocation
	Public Property IsRequired As Boolean?

	Public Sub New()
	End Sub

	Public Sub New(xEl As XmlElement, eQuestionRole As QuestionRole)
		If xEl.Name <> "QUESTION" Then Throw New ArgumentException("Invalid QUESTION XmlElement input. Name does not match")

		QuestionName = xEl.GetAttribute("name")

		' The presence of is_required will override the existing required-ness of the CQ.
		' No is_required will use the default.
		IsRequired = Nothing
		If xEl.GetAttribute("is_required") = "Y" Then IsRequired = True
		If xEl.GetAttribute("is_required") = "N" Then IsRequired = False

		' Previously, the presence of QUESTION elements indicated that they were enabled.
		' Now, it is done by a switch. For backwards compatibility, IsEnable is true if set to Y or is empty.
		IsEnable = xEl.GetAttribute("is_enable") <> "N"

		Dim sDisplayPage = xEl.GetAttribute("display_page")
		Dim eLocation As CustomQuestionLocation
		Select Case eQuestionRole
			Case QuestionRole.Applicant
				If String.IsNullOrEmpty(sDisplayPage) OrElse sDisplayPage = SmCQInformation.DefaultDisplayPage Then
					eLocation = CustomQuestionLocation.ApplicantPage
				ElseIf sDisplayPage = SmCQInformation.FirstDisplayPage Then
					eLocation = CustomQuestionLocation.LoanPage
				Else
					Throw New ArgumentException("Invalid QUESTION XmlElement input. The display_page '" + sDisplayPage + "' is invalid.")
				End If

			Case QuestionRole.Application
				If String.IsNullOrEmpty(sDisplayPage) OrElse sDisplayPage = SmCQInformation.DefaultDisplayPage Then
					eLocation = CustomQuestionLocation.ReviewPage
				ElseIf sDisplayPage = SmCQInformation.FirstDisplayPage Then
					eLocation = CustomQuestionLocation.LoanPage
				Else
					Throw New ArgumentException("Invalid QUESTION XmlElement input. The display_page '" + sDisplayPage + "' is invalid.")
				End If
		End Select
		DisplayPage = eLocation
	End Sub

	Public Shared Function ReadCQInfoFromConfig(oConfig As CWebsiteConfig, xPath As String, eQuestionRole As QuestionRole) As List(Of CQInformation)
		Dim cqNodeList As XmlNodeList = oConfig._webConfigXML.SelectNodes(xPath)
		Dim lst As New List(Of CQInformation)
		If cqNodeList IsNot Nothing Then
			For Each node As XmlElement In cqNodeList
				lst.Add(New CQInformation(node, eQuestionRole))
			Next
		End If
		Return lst
	End Function


End Class

Public Class CQInformationList
	Private ReadOnly _dctCQInfo As Dictionary(Of String, CQInformation)
	Private ReadOnly _isHideAll As Boolean

	Public Sub New(ByVal lstCQInfo As List(Of CQInformation))
		If lstCQInfo Is Nothing Then Throw New ArgumentNullException("lstCQInfo")
		_dctCQInfo = lstCQInfo.ToDictionary(Function(cqi) cqi.QuestionName)
		_isHideAll = IsHideAll()
	End Sub

	''' <summary>
	''' Show All: no items
	''' Hide All: question has name "bogus_for_hiding_all_question"
	''' Other cases: questions may have is_enable true or false, 
	''' </summary>
	''' <returns></returns>
	Public Function IsShowAll() As Boolean
		Return Not _dctCQInfo.Any()
	End Function

	Public Function IsHideAll() As Boolean
		Return _dctCQInfo.ContainsKey(SmCQInformation.HideAllQuestionKey)
	End Function

	Public Function IsAllowed(cq As CCustomQuestionXA) As Boolean
		If _isHideAll Then Return False
		If IsShowAll() Then Return True

		Dim info As CQInformation = Nothing
		If _dctCQInfo.TryGetValue(cq.Name, info) Then
			Return info.IsEnable
		End If

		Return False
	End Function
End Class
