﻿Imports Microsoft.VisualBasic
Imports System.Xml
Imports System.Linq
Imports System.Net
Imports System.IO
Imports System.Web.Script.Serialization
Imports System.Text.RegularExpressions

Imports LPQMobile.Utils

<Serializable()> _
Public Class CFOMQuestion
	Private Shared log As log4net.ILog = log4net.LogManager.GetLogger(GetType(CFOMQuestion))


	Public Property Name() As String

	Public Property TextTemplate() As String

	Public Property NextQuestion() As String
    Public Property for_business() As String
    Public Property for_personal() As String
        
    Public Property for_special() As String
        
    Public _supportedSpecialAccountTypes As List(Of String)
    Public Property supportedSpecialAccountTypes() As List(Of String)
        Get
            If _supportedSpecialAccountTypes Is Nothing Then
                Return New List(Of String)
            End If
            Return _supportedSpecialAccountTypes
        End Get
        Set(value As List(Of String))
            _supportedSpecialAccountTypes = value
        End Set
    End Property
	Public ReadOnly Property title() As String
        Get
            If Me.TextTemplate.IndexOf("href") > 0 And Me.TextTemplate.IndexOf("{") < 0 Then
                Return TextTemplate
            End If

            If String.IsNullOrEmpty(Me.TextTemplate) OrElse Me.TextTemplate.IndexOf("{") < 0 Then
                Return Regex.Replace(TextTemplate, "<.*?>", String.Empty)
            End If
            Dim index As Integer = Me.TextTemplate.IndexOf("{")
            Dim strTitle As String = Me.TextTemplate.Substring(0, index)

            If (Me.TextTemplate.IndexOf("<table>") > 0) Then
                index = Me.TextTemplate.IndexOf("<table>")
            ElseIf (strTitle.LastIndexOf(":") > -1 And strTitle.LastIndexOf(":") < index) Then
                ''skip to check ":" if the title contains a link
                If strTitle.IndexOf("<a") = -1 Then
                    '' check no character between the last ":" and "{"
                    Dim strChar As String = strTitle.Substring(strTitle.LastIndexOf(":"), index - strTitle.LastIndexOf(":")).Trim()
                    If Not String.IsNullOrEmpty(strChar) Then
                        index = strTitle.LastIndexOf(":") + 1
                    End If
                    ''contain first input title
                    If (strTitle.LastIndexOf(".") > -1 And strTitle.LastIndexOf(".") < strTitle.LastIndexOf(":")) Then
                        Dim length As Integer = strTitle.LastIndexOf(":") - strTitle.LastIndexOf(".")
                        Dim strFirstInput As String = strTitle.Substring(strTitle.LastIndexOf("."), length)
                        If strFirstInput.ToUpper.IndexOf("NAME") > -1 Or strFirstInput.ToUpper.IndexOf("MEMBER") > -1 Or strFirstInput.ToUpper.IndexOf("RELATIONSHIP") > -1 Then
                            If (strTitle.LastIndexOf(".") > -1) Then
                                index = strTitle.LastIndexOf(".") + 1
                            End If
                        End If
                    End If
                End If
            End If

            Return Regex.Replace(Me.TextTemplate.Substring(0, index), "<(?!a|/a).*?>", String.Empty)

        End Get
    End Property
    Public ReadOnly Property title_V2() As String
        Get
            Dim startIndex = Me.TextTemplate.IndexOf("{")
            Dim endIndex = Me.TextTemplate.IndexOf("}")
            ''remove all label text parameters in the textTemplate if it exists
            If (startIndex > -1 And endIndex > startIndex) Then
                If Not IsNumeric(Me.TextTemplate.Substring(startIndex + 1, endIndex - startIndex - 1)) Then
                    Dim sLabelTextParas As String = Me.TextTemplate.Substring(startIndex, endIndex + 1 - startIndex)
                    Return Me.TextTemplate.Replace(sLabelTextParas, "")
                Else
                    Return Me.TextTemplate.Substring(0, startIndex)
                End If

            End If
            Return TextTemplate
        End Get
    End Property
 
	Public isSelected As Boolean

	Private _Fields As List(Of CFOMQuestionField)
	Public Property Fields() As List(Of CFOMQuestionField)
		Get
			If _Fields Is Nothing Then
				_Fields = New List(Of CFOMQuestionField)
			End If
			Return _Fields
		End Get
		Set(ByVal value As List(Of CFOMQuestionField))
			_Fields = value
		End Set
    End Property

    Public Sub New()

    End Sub

    Public Sub New(ByVal oNode As XmlElement)
        isSelected = False

        Me.Name = oNode.GetAttribute("name")
        Me.TextTemplate = oNode.GetAttribute("text_template")
        'Me.TextTemplate = Regex.Replace(Me.TextTemplate, "<.*?>", String.Empty)
        Me.TextTemplate = Regex.Replace(Me.TextTemplate, "<div.*?>", String.Empty).Replace("</div>", String.Empty)
        Me.TextTemplate = Regex.Replace(Me.TextTemplate, "<font.*?>", String.Empty).Replace("</font>", String.Empty)
        ''remove <img> tag if it exist in texttemplate
		If Me.TextTemplate.Contains("<img") Then
			Me.TextTemplate = Regex.Replace(Me.TextTemplate, "<img.*?>", String.Empty)
		End If
        Me.NextQuestion = oNode.GetAttribute("next_question")
        For Each ansNode As XmlElement In oNode.SelectNodes("fields/field")
            Dim field As New CFOMQuestionField(ansNode)
            Me.Fields.Add(field)
        Next
        Me.for_business = Common.SafeString(oNode.GetAttribute("for_business"))
        Me.for_personal = Common.SafeString(oNode.GetAttribute("for_personal"))
        Me.for_special = Common.SafeString(oNode.GetAttribute("for_special"))
        Dim supportedSpecialAccountTypeNodes As XmlNodeList = oNode.GetElementsByTagName("supported_special_account_type")
        Me.supportedSpecialAccountTypes = New List(Of String)
        If supportedSpecialAccountTypeNodes.Count > 0 Then
            For Each oSpecialAccountType As XmlElement In supportedSpecialAccountTypeNodes
                If Common.SafeString(oSpecialAccountType.InnerText) <> "" Then
                    Me.supportedSpecialAccountTypes.Add(Common.SafeString(oSpecialAccountType.InnerText))
                End If
            Next
        End If
    End Sub
  
    ''deprecate - not using anywhere
	Public Shared Function CurrentJsonPackage(websiteConfig As CWebsiteConfig) As String
		Dim serializer As New JavaScriptSerializer()
		Return serializer.Serialize(CurrentFOMQuestions(websiteConfig))
	End Function

	Public Shared Function CurrentFOMQuestions(websiteConfig As CWebsiteConfig) As List(Of CFOMQuestion)
		Dim strFOMResponse As String = CDownloadedSettings.DownloadFomQuestion(websiteConfig)
		Dim oFOMQuestionList As New List(Of CFOMQuestion)
		Dim responseXML As XmlDocument = New XmlDocument()
		responseXML.LoadXml(strFOMResponse)
		' parse starting question list
		For Each oItem As XmlElement In responseXML.SelectNodes("//fom/starting_questions/question")
			'only attribute that are false are available
			If Convert.ToBoolean(oItem.GetAttribute("is_available_to_consumer") = "false") Then	'exclude special account
				Continue For
			End If
			''If Convert.ToBoolean(oItem.GetAttribute("for_personal") = "false") Then 'exclude business account
			''    Continue For
			''End If
			oFOMQuestionList.Add(New CFOMQuestion(oItem))
		Next
		Return oFOMQuestionList
	End Function

    ''download fom next questions
	Public Shared Function DownloadFomNextQuestions(websiteConfig As CWebsiteConfig) As List(Of CFOMQuestion)
		Dim fomData As String = CDownloadedSettings.DownloadFomQuestion(websiteConfig)
		Dim oFOMQuestionList As New List(Of CFOMQuestion)
		Dim responseXML As XmlDocument = New XmlDocument()
		responseXML.LoadXml(fomData)
		' parse product question list
		For Each oItem As XmlElement In responseXML.SelectNodes("//fom/questions/question")
			Dim oQuestion As New CFOMQuestion(oItem)
			oFOMQuestionList.Add(oQuestion)
		Next
		Return oFOMQuestionList
	End Function
    'download ID_card_types  function
    Public Shared Function DownLoadIDCardTypes(websiteConfig As CWebsiteConfig) As Dictionary(Of String, String)
        Dim result As New Dictionary(Of String, String)

        'ACTion LOS doesn't have ID card section so skip this.  This is used by APM>Desgin>Visibility
        If websiteConfig.SubmitLoanUrl_TPV <> "" AndAlso websiteConfig.SubmitLoanUrl_TPV = websiteConfig.SubmitLoanUrl Then
            Return result
        End If

        Dim fomData As String = CDownloadedSettings.DownloadFomQuestion(websiteConfig)
        Try
            Dim responseXML As XmlDocument = New XmlDocument()
            responseXML.LoadXml(fomData)
            'parse List_Item  -> ID card types, add all the items to oIDCardTypes dictionary
            For Each oFieldList As XmlElement In responseXML.SelectNodes("//FIELD_LISTS/FIELD_LIST")
                If (oFieldList.GetAttribute("name").Trim() = "ID Card Types") Then
                    Dim oListItems As XmlNodeList = oFieldList.GetElementsByTagName("LIST_ITEM")
                    For Each oItemNode As XmlElement In oListItems
                        Dim key = oItemNode.GetAttribute("value")
                        If Not result.ContainsKey(key) Then
                            result.Add(key, oItemNode.GetAttribute("text"))
                        End If
                    Next
                End If
            Next
        Catch ex As Exception
            log.Warn("Issue with DownLoadIDCardTypes ", ex)
        End Try
        Return result
    End Function

    ''deprecate - not using anywhere
    'Public Shared Function DownLoadCitizenships(websiteConfig As CWebsiteConfig) As Dictionary(Of String, String)
    '	Dim DownloadFom As String
    '	Dim sCacheKey As String = "CURRENT_FOM_QUESTIONS_" & websiteConfig.LenderRef
    '	DownloadFom = CType(HttpContext.Current.Cache.Get(sCacheKey), String)
    '	Try
    '		If DownloadFom IsNot Nothing Then
    '			'_log.Info("Object " & sCacheKey & " was retrieved from cache. The cache " & _
    '			'	"was populated at " & HttpContext.Current.Application(sCacheKey).ToString())
    '			''get citizenship from cache
    '			Return ParsedCitizenship(DownloadFom)
    '			'Exit Try
    '		End If
    '		SyncLock _CacheLock_CURRENT_FOM_QUESTIONS
    '			' Ensure that the data was not loaded by a concurrent thread 
    '			' while waiting for lock.
    '			DownloadFom = CType(HttpContext.Current.Cache.Get(sCacheKey), String)
    '			If DownloadFom IsNot Nothing Then
    '				'_log.Info("Object " & sCacheKey & " was retrieved from cache. The cache " & _
    '				'"was populated at " & HttpContext.Current.Application(sCacheKey).ToString())
    '				''get citizenship from cache
    '				Return ParsedCitizenship(DownloadFom)
    '				' Exit Try
    '			End If
    '			DownloadFom = CFOMQuestion.DownloadFOM(websiteConfig)
    '			If ParsedCitizenship(DownloadFom).Count > 0 Then ' count less than 0 mean there is probably error in download so don't wnat to store in session
    '				HttpContext.Current.Cache.Insert(sCacheKey, DownloadFom, Nothing, System.Web.Caching.Cache.NoAbsoluteExpiration, New TimeSpan(22, 0, 0))   '12hour&45sec
    '				HttpContext.Current.Application(sCacheKey) = DateTime.Now
    '			End If
    '		End SyncLock
    '	Catch ex As Exception
    '		_log.Error("Can't get " & sCacheKey)
    '	End Try
    '	Return ParsedCitizenship(DownloadFom)
    'End Function
    ''------------DownloadFOM contains  questions, next questions, and other item lists such as Id card type, citizenship ... 

    Public Shared Function hasFOM_V2(websiteConfig As CWebsiteConfig) As Boolean
		Dim strFomResponse As String = CDownloadedSettings.DownloadFomQuestion(websiteConfig)
		Dim responseXML As XmlDocument = New XmlDocument()
		Dim textTemplate As String = ""
		Dim startIndex As Integer
		Dim endIndex As Integer
		Dim sPara As String = ""
		responseXML.LoadXml(strFomResponse)
		' parse product question list
		For Each oItem As XmlElement In responseXML.SelectNodes("//fom/starting_questions/question")
			If oItem.HasAttribute("header_text") Then
				Dim fields As XmlNodeList = oItem.SelectNodes("fields/field")
				For Each oField As XmlElement In fields
					If oField.HasAttribute("label_text") Then
						Return True	'' this FOM using version2
					End If
				Next
			End If

			textTemplate = Common.SafeString(oItem.GetAttribute("text_template"))
			startIndex = textTemplate.LastIndexOf("{", System.StringComparison.Ordinal)
			endIndex = textTemplate.LastIndexOf("}", System.StringComparison.Ordinal)
			If startIndex > -1 And endIndex > startIndex Then
				sPara = textTemplate.Substring(startIndex + 1, endIndex - startIndex - 1)
				''text_template ="...{0} ..{1} for version 1
				''text_template="...{label text; label text;..} for version 2
				If Not IsNumeric(sPara) Then
					Return True	'' this FOM using version2
				End If
			End If

		Next
		Return False
	End Function

	

End Class
