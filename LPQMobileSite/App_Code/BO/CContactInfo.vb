﻿<Serializable()>
Public Class CContactInfo
	Public Email As String
	Public HomePhone As String
	Public HomePhoneCountry As String
	Public CellPhone As String
	Public CellPhoneCountry As String
	Public WorkPhone As String
	Public WorkPhoneCountry As String
	Public WorkPhoneExt As String
	Public PreferredContactMethod As String
End Class