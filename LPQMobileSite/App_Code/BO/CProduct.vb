﻿Imports Microsoft.VisualBasic
Imports System.Xml
Imports System.Linq
Imports System.Net
Imports System.IO
Imports System.Web.Script.Serialization

Imports LPQMobile.Utils

<Serializable()> _
Public Class CProduct
	'Private _log As log4net.ILog = log4net.LogManager.GetLogger(GetType(CProduct))
	Private Shared ReadOnly _log As log4net.ILog = log4net.LogManager.GetLogger(GetType(CProduct))

    Protected Const constProductRetrievalURL As String = "/ssfcu/ProductRetrieval.aspx"

    Public IsActive As Boolean 'is_active'
	'Public PreSelection As Boolean 'pre_selection'
	Public IsPreSelection As Boolean 'pre_selection'
	Public IsRequired As Boolean	'pre_selection'
    Public AutoPullIDAuthenticationConsumerPrimary As Boolean 'auto_pull_id_authentication_consumer_primary'
    Public AutoPullDebitConsumerPrimary As Boolean  '  'auto_pull_debit_consumer_primary
    Public AutoPullCreditsConsumerPrimary As Boolean '  'auto_pull_credit_consumer_primary
    Public AutoPullIDAuthenticationConsumerSecondary As Boolean 'auto_pull_id_authentication_consumer_Secondary'
    Public AutoPullDebitConsumerSecondary As Boolean    '  'auto_pull_debit_consumer_Secondary
    Public AutoPullCreditsConsumerSecondary As Boolean '  'auto_pull_credit_consumer_Secondary
    Public MinimumDeposit As Decimal 'min_deposit'
    Public MaxDeposit As Decimal 'max_deposit
	Public DepositeAmount As Decimal
    Public Rate As String 'rate'
    Public Apy As String 'apy'
    Public Term As String 'term'
    Public InterestRateType As String

    Public ProductCode As String
	Public AccountType As String  '"OTHER, SAVING, CHECKING
    Public AccountName As String
    Public Availability As String 'PRIMARY, SECONDARY, BOTH
    Public ProductDescription As String
    Public Position As Integer
    Public strPreSelectedValue As String
    Public EntityType As String ''personal/special =P, special=S, and business=B
    Public MinorAccountCode As String
    Private _CustomQuestions As List(Of CProductCustomQuestion)
    Public ZipCodeFilterType As String
    Public ZipCodePoolId As String
    Public SelectedRate As String

    ''add more properties for using New API
    Public cannotBeFunded As String = ""
    Public pdfAssociations As New List(Of String)
	Public specialAccountTypes As New List(Of String)
	Public Property IsAutoCalculateTier As Boolean
	Public Property TermType As String
	Public Property IsApyAutoCalculated As Boolean

    Public Property CustomQuestions() As List(Of CProductCustomQuestion)
        Get
            If _CustomQuestions Is Nothing Then
                _CustomQuestions = New List(Of CProductCustomQuestion)
            End If
            Return _CustomQuestions
        End Get
        Set(ByVal value As List(Of CProductCustomQuestion))
            _CustomQuestions = value
        End Set
    End Property

    Private _Services As List(Of CProductService)
    Public Property Services() As List(Of CProductService)
        Get
            If _Services Is Nothing Then
                _Services = New List(Of CProductService)
            End If
            Return _Services
        End Get
        Set(ByVal value As List(Of CProductService))
            _Services = value
        End Set
    End Property

    Private _Rates As List(Of CProductRate)

    Public Property AvailableRates() As List(Of CProductRate)
        Get
            If _Rates Is Nothing Then
                _Rates = New List(Of CProductRate)
            End If
            Return _Rates
        End Get
        Set(ByVal value As List(Of CProductRate))
            _Rates = value
        End Set
    End Property
    Public Function Clone() As CProduct
        Return DirectCast(Me.MemberwiseClone(), CProduct)
    End Function

    ''' <summary>
    ''' Get available Products. 
    ''' Products are downloaded from server then filtered by settings in config.xml like AllowedProductCodes and AllowedCustomQuestions and psAvailability
    ''' </summary>
	''' <param name="oConfig"></param>
    ''' <param name="psAvailability"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function GetProducts(oConfig As CWebsiteConfig, psAvailability As String) As List(Of CProduct)    
		Dim oDowloadedProducts = FilterProductsFromDownload(oConfig, psAvailability)
		Dim oFilterProducts As List(Of CProduct) = FilterProducts(oDowloadedProducts, oConfig, psAvailability)
		Return oFilterProducts
	End Function

	Public Shared Function GetProductsForLoans(oConfig As CWebsiteConfig) As List(Of CProduct)
		Return FilterProductsFromDownload(oConfig, "1")
	End Function

	''' <summary>
	''' Get all available Products regardless restriction settings in config.xml, filter by type
	''' </summary>
	''' <returns></returns>
	''' <remarks></remarks>
	Public Shared Function GetAllProducts(oConfig As CWebsiteConfig, ByVal psAvailability As String) As List(Of CProduct)
		'' download all product
		''Return getDownloadedProducts(oConfig, psAvailability)
		Return FilterProductsFromDownload(oConfig, psAvailability)
	End Function

	''' <summary>
	''' Get all available Products(consumer, special, business)
	''' </summary>
	Public Shared Function GetAllProducts(oConfig As CWebsiteConfig) As List(Of CProduct)
		'' download all product
		''Return getDownloadedProducts(oConfig, psAvailability)
		Dim oDownloadedProducts = CDownloadedSettings.GetDownloadedProducts(oConfig)
		Dim oProductList As New List(Of CProduct)
		Dim oCombinedProductList As New List(Of CProduct)
		oProductList = FilterProductsFromDownload(oDownloadedProducts, "1")
		oCombinedProductList.AddRange(oProductList)
		oProductList = FilterProductsFromDownload(oDownloadedProducts, "2")
		oCombinedProductList.AddRange(oProductList)
		oProductList = FilterProductsFromDownload(oDownloadedProducts, "1s")
		oCombinedProductList.AddRange(oProductList)
		oProductList = FilterProductsFromDownload(oDownloadedProducts, "2s")
		oCombinedProductList.AddRange(oProductList)
		oProductList = FilterProductsFromDownload(oDownloadedProducts, "1b")
		oCombinedProductList.AddRange(oProductList)
		oProductList = FilterProductsFromDownload(oDownloadedProducts, "2b")
		oCombinedProductList.AddRange(oProductList)

		Return oCombinedProductList.Distinct().ToList()
	End Function

	Public Shared Function GetAllProductsGroupByAvailability(oConfig As CWebsiteConfig) As Dictionary(Of String, List(Of CProduct))
		Dim oProductList As New List(Of CProduct)
		Dim result As New Dictionary(Of String, List(Of CProduct))
		oProductList = FilterProductsFromDownload(oConfig, "1")
		result.Add("1", oProductList)
		oProductList = FilterProductsFromDownload(oConfig, "2")
		result.Add("2", oProductList)
		oProductList = FilterProductsFromDownload(oConfig, "1s")
		result.Add("1s", oProductList)
		oProductList = FilterProductsFromDownload(oConfig, "2s")
		result.Add("2s", oProductList)
		oProductList = FilterProductsFromDownload(oConfig, "1b")
		result.Add("1b", oProductList)
		oProductList = FilterProductsFromDownload(oConfig, "2b")
		result.Add("2b", oProductList)
		Return result
	End Function

	Public Shared Function GetJsonProductsPackage(oconfig As CWebsiteConfig, psAvailability As String) As String
		Dim serializer As New JavaScriptSerializer()
		Return serializer.Serialize(GetProducts(oconfig, psAvailability))
	End Function

	Private Shared Function FilterProducts(prods As List(Of CProduct), config As CWebsiteConfig, psAvailability As String) As List(Of CProduct)
		If prods Is Nothing OrElse prods.Count = 0 Then
			Return New List(Of CProduct)
		End If
		'' filter by allowed products in config
		Dim allowedProductCodes As List(Of String) = GetAllowedProductCodes(config, psAvailability)
		'get allowed product code for minor
		Dim specialAccountList As New Dictionary(Of String, String)

		If psAvailability = "1a" Or psAvailability = "2a" Then
			If allowedProductCodes.Count > 0 Then
				specialAccountList = Common.getItemsFromConfig(config, "XA_LOAN/SPECIAL_ACCOUNTS/ACCOUNT", "product_code", "minor_account_code")
			Else
				specialAccountList = getMinorAccountCodesFromDownload(prods, Common.ParsedMinorAccountType(config))
			End If
			'ElseIf psAvailability = "1b" Or psAvailability = "2b" Then ''for business

		End If

		If allowedProductCodes IsNot Nothing AndAlso allowedProductCodes.Count > 0 Then
			prods = prods.Where(Function(x) allowedProductCodes.Contains(x.ProductCode)).ToList()
		End If

		'' filter by account type
		Dim finalProds As New List(Of CProduct)
		For Each f In prods
			''make sure to get IsRequired and IsPreselection for both from download and from the Cache based on the strSelectedValue
			If f.strPreSelectedValue = "R" Then
				f.IsRequired = True
			ElseIf f.strPreSelectedValue = "P" Then
				f.IsPreSelection = True
			End If
			'  add minor account code to the product if it exist
			For Each mProd As KeyValuePair(Of String, String) In specialAccountList
				If mProd.Key = f.ProductCode Then
					f.MinorAccountCode = specialAccountList.Item(f.ProductCode)
					Exit For
				Else
					f.MinorAccountCode = ""
				End If
			Next
			finalProds.Add(f)
		Next

		'' filter by allowed questions in config
		Dim allowedProductQuestions = GetAllowedProductQuestions(config)
		If allowedProductQuestions IsNot Nothing AndAlso allowedProductQuestions.Count > 0 Then
			For Each item In finalProds
				Dim allowedQuestions = item.CustomQuestions.Where(Function(x) allowedProductQuestions.Contains(x.QuestionName)).ToList()
				item.CustomQuestions = allowedQuestions
			Next
		End If

		Return finalProds
	End Function



	Private Shared Function GetAllowedProductQuestions(config As CWebsiteConfig) As List(Of String)
		Dim productQuestions As New List(Of String)
		Dim oNodes As XmlNodeList = config._webConfigXML.SelectNodes("XA_LOAN/ACCOUNT_QUESTIONS/QUESTION")
		If oNodes IsNot Nothing AndAlso oNodes.Count > 0 Then
			For Each child As XmlNode In oNodes
				If child.Attributes("question_name") Is Nothing Then
					_log.Error("question_name is not correct.", Nothing)
				End If
				If Not String.IsNullOrEmpty(child.Attributes("question_name").InnerXml) Then
					productQuestions.Add(child.Attributes("question_name").InnerXml)
				End If
			Next
		End If
		Return productQuestions
	End Function

	Private Shared Function GetAllowedProductCodes(config As CWebsiteConfig, ByVal sAvailability As String) As List(Of String)
		Dim productCodes As New List(Of String)
		Dim nodeName As String = "XA_LOAN/ACCOUNTS/ACCOUNT"
		Dim minorPrefix As String = ""
		If sAvailability = "1a" Or sAvailability = "2a" Then
			nodeName = "XA_LOAN/SPECIAL_ACCOUNTS/ACCOUNT"
			minorPrefix = "Minor"
		End If
		Dim oNodes As XmlNodeList = config._webConfigXML.SelectNodes(nodeName)

		If oNodes IsNot Nothing AndAlso oNodes.Count > 0 Then
			For Each child As XmlNode In oNodes
				If child.Attributes("product_code") Is Nothing Then
					_log.Error(minorPrefix + " product_code is not correct.", Nothing)
				End If
				If child.Attributes("product_code").InnerXml <> "" Then
					productCodes.Add(child.Attributes("product_code").InnerXml)
				End If
			Next
		End If
		Return productCodes
	End Function
#Region "download Products with New API"
	Public Shared Function FilterProductsFromDownload(oDownloadedProducts As List(Of CProduct), sAvailability As String) As List(Of CProduct)
		Dim oCurrentProducts As New List(Of CProduct)

		Dim filter As Func(Of CProduct, Boolean) = Function(oProduct)
															 Select Case sAvailability.ToLower
																 Case "2"
																	 If (oProduct.Availability = "SECONDARY" OrElse oProduct.Availability = "BOTH") AndAlso oProduct.EntityType = "P" Then ' PersonalSecondaryProducts
																		 Return True
																	 End If
																 Case "1a", "1s"
																	 If (oProduct.Availability = "PRIMARY" OrElse oProduct.Availability = "BOTH") AndAlso (oProduct.EntityType = "S" Or (oProduct.EntityType = "P" And oProduct.specialAccountTypes.Count > 0)) Then ' SpecialPrimaryProducts
																		 Return True
																	 End If
																 Case "2a", "2s"
																	 If (oProduct.Availability = "SECONDARY" OrElse oProduct.Availability = "BOTH") AndAlso (oProduct.EntityType = "S" Or (oProduct.EntityType = "P" And oProduct.specialAccountTypes.Count > 0)) Then ' SpecialSecondaryProducts
																		 Return True
																	 End If
																 Case "1b"
																	 If (oProduct.Availability = "PRIMARY" OrElse oProduct.Availability = "BOTH") AndAlso oProduct.EntityType = "B" Then ' BusinessPrimaryProducts
																		 Return True
																	 End If
																 Case "2b"
																	 If (oProduct.Availability = "SECONDARY" OrElse oProduct.Availability = "BOTH") AndAlso oProduct.EntityType = "B" Then ' BusinessPrimaryProducts
																		 Return True
																	 End If
																 Case "1"
																	 If (oProduct.Availability = "PRIMARY" OrElse oProduct.Availability = "BOTH") AndAlso oProduct.EntityType = "P" Then ' PersonalPrimaryProducts '' by default
																		 Return True
																	 End If
																 Case Else
																	 If (oProduct.Availability = "PRIMARY" OrElse oProduct.Availability = "BOTH") AndAlso oProduct.EntityType = "P" Then ' PersonalPrimaryProducts '' by default
																		 Return True
																	 End If
															 End Select
															 Return False
														 End Function

		If oDownloadedProducts IsNot Nothing AndAlso oDownloadedProducts.Count > 0 Then
			oCurrentProducts = oDownloadedProducts.Where(filter).ToList()
		End If

		Return oCurrentProducts
	End Function

	Public Shared Function FilterProductsFromDownload(oConfig As CWebsiteConfig, sAvailability As String) As List(Of CProduct)
		Dim oDownloadedProducts = CDownloadedSettings.GetDownloadedProducts(oConfig)
		Return FilterProductsFromDownload(oDownloadedProducts, sAvailability)
	End Function
	Public Shared Function getDownloadedProducts_NewAPI(oConfig As CWebsiteConfig) As List(Of CProduct)
		Dim oDownLoadProducts As New List(Of CProduct)
		If oConfig Is Nothing Or (oConfig IsNot Nothing And (oConfig.OrgCode = "" Or oConfig.LenderCode = "")) Then
			Return oDownLoadProducts
		End If
		Dim productsXML As XmlDocument = CDownloadedSettings.GetXAProducts(oConfig)
		If productsXML.ChildNodes.Count = 0 Then
			Return oDownLoadProducts
		End If
		Dim productNodes As XmlNodeList = productsXML.GetElementsByTagName("PRODUCT")
		If productNodes.Count = 0 Then
			Return oDownLoadProducts
		End If
		Dim getFieldValue As Func(Of XmlElement, String) = Function(x) If(x IsNot Nothing, Common.SafeString(x.InnerText), "")


		For Each oNode As XmlElement In productNodes
			Dim newProduct As New CProduct
			''------parse GENERAL_INFORMATION Node------
			Dim oGenInfo As XmlElement = CType(oNode.SelectSingleNode("GENERAL_INFORMATION"), XmlElement)
			Dim oAvaiInfo As XmlElement = CType(oNode.SelectSingleNode("AVAILABILITY_INFORMATION"), XmlElement)
			Dim oRateAndTerm As XmlElement = CType(oNode.SelectSingleNode("RATES_AND_TERMS"), XmlElement)
			'Dim oDisbFunding As XmlElement = oNode.SelectSingleNode("DISBURSEMENT_AND_ADDITIONAL_FUNDING")
			'Dim oSwitchKit As XmlElement = oNode.SelectSingleNode("SWITCH_KIT")
			'Dim oProductCQs As XmlElement = CType(oNode.SelectSingleNode("PRODUCT_CUSTOM_QUESTIONS"), XmlElement)

			Dim oIntegration As XmlElement = CType(oNode.SelectSingleNode("INTEGRATION"), XmlElement)
			Dim sIsActive As String = getFieldValue(CType(oGenInfo.SelectSingleNode("IS_ACTIVE"), XmlElement)).ToUpper
			Dim sSiteEntity As String = getFieldValue(CType(oAvaiInfo.SelectSingleNode("SITE_ENTITY"), XmlElement)).ToUpper
			Dim sEntityType As String = getFieldValue(CType(oAvaiInfo.SelectSingleNode("ENTITY_TYPE"), XmlElement)).ToUpper
			''get special account types 
			Dim specialAccountNode As XmlNodeList = oAvaiInfo.GetElementsByTagName("SPECIAL_ACCOUNT")
			Dim oSpecialAccountTypes As New List(Of String)
			If specialAccountNode.Count > 0 Then
				oSpecialAccountTypes.AddRange(From oSpecialAccount As XmlElement In specialAccountNode Select accountType = getFieldValue(oSpecialAccount) Where accountType <> "")
			End If
			newProduct.specialAccountTypes = oSpecialAccountTypes

			newProduct.IsActive = sIsActive = "Y"
			''exclude not active product, exclude not available to consumer
			If Not newProduct.IsActive Or sSiteEntity <> "ALL" Then
				Continue For
			End If
			''PDF association
			Dim pdfAssociations = oNode.SelectNodes("PDF_ASSOCIATIONS/PDF_ASSOCIATION")
			Dim oPDFs As New List(Of String)
			If pdfAssociations IsNot Nothing AndAlso pdfAssociations.Count > 0 Then
				oPDFs.AddRange(From pdfNode As XmlElement In pdfAssociations Where Not String.IsNullOrEmpty(Common.SafeString(pdfNode.InnerText)) Select pdfNode.InnerText)
			End If
			newProduct.pdfAssociations = oPDFs

			''Dim tierNodes = oRateAndTerm.SelectNodes("TIERS/TIER")
			' '' '' pre_selection has 3 values, for my best guess: P (pre selected) R (required) O (who knows)
			''If tierNodes IsNot Nothing AndAlso tierNodes.Count > 0 Then
			'' ''general infor
			newProduct.cannotBeFunded = getFieldValue(CType(oGenInfo.SelectSingleNode("CANNOT_BE_FUNDED"), XmlElement))
			newProduct.ProductCode = getFieldValue(CType(oGenInfo.SelectSingleNode("PRODUCT_CODE"), XmlElement))
			newProduct.AccountType = getFieldValue(CType(oGenInfo.SelectSingleNode("ACCOUNT_TYPE"), XmlElement))
			newProduct.AccountName = getFieldValue(CType(oGenInfo.SelectSingleNode("PRODUCT_NAME"), XmlElement))
			newProduct.ProductDescription = getFieldValue(CType(oGenInfo.SelectSingleNode("PRODUCT_DESCRIPTION"), XmlElement))
			newProduct.strPreSelectedValue = getFieldValue(CType(oGenInfo.SelectSingleNode("PRE_SELECTION"), XmlElement))
			newProduct.IsRequired = newProduct.strPreSelectedValue = "R"
			newProduct.IsPreSelection = newProduct.strPreSelectedValue = "P"
			''availability infor
			newProduct.Availability = getFieldValue(CType(oAvaiInfo.SelectSingleNode("AVAILABILITY"), XmlElement))
			newProduct.EntityType = sEntityType
			newProduct.ZipCodeFilterType = getFieldValue(CType(oAvaiInfo.SelectSingleNode("ZIP_CODE_FILTER_TYPE"), XmlElement))
			newProduct.ZipCodePoolId = getFieldValue(CType(oAvaiInfo.SelectSingleNode("ZIP_CODE_POOL_ID"), XmlElement))

			''Integration - Authentication -Primary
			newProduct.AutoPullIDAuthenticationConsumerPrimary = getFieldValue(CType(oIntegration.SelectSingleNode("AUTO_PULL_ID_AUTHENTICATION_CONSUMER_PRIMARY"), XmlElement)) = "Y"
			newProduct.AutoPullDebitConsumerPrimary = getFieldValue(CType(oIntegration.SelectSingleNode("AUTO_PULL_DEBIT_CONSUMER_PRIMARY"), XmlElement)) = "Y"
			newProduct.AutoPullCreditsConsumerPrimary = getFieldValue(CType(oIntegration.SelectSingleNode("AUTO_PULL_CREDIT_CONSUMER_PRIMARY"), XmlElement)) = "Y"
			''Integration - Authentication -Secondary
			newProduct.AutoPullIDAuthenticationConsumerSecondary = getFieldValue(CType(oIntegration.SelectSingleNode("AUTO_PULL_ID_AUTHENTICATION_CONSUMER_SECONDARY"), XmlElement)) = "Y"
			newProduct.AutoPullDebitConsumerSecondary = getFieldValue(CType(oIntegration.SelectSingleNode("AUTO_PULL_DEBIT_CONSUMER_SECONDARY"), XmlElement)) = "Y"
			newProduct.AutoPullCreditsConsumerSecondary = getFieldValue(CType(oIntegration.SelectSingleNode("AUTO_PULL_CREDIT_CONSUMER_SECONDARY"), XmlElement)) = "Y"

			Dim sPosition As String = getFieldValue(CType(oGenInfo.SelectSingleNode("DISPLAY_POSITION"), XmlElement))

			If sPosition = "" Then
				' Generate random value between 1 and 30.
				Dim value As Integer = New CSecureRandom().GetValue(1, 30)
				newProduct.Position = value
			Else
				newProduct.Position = CInt(sPosition)
			End If

			''rate and term
			newProduct.Rate = getFieldValue(CType(oRateAndTerm.SelectSingleNode("RATE"), XmlElement))
			''there no TERM attribute so get MIN_TERM
			newProduct.Term = getFieldValue(CType(oRateAndTerm.SelectSingleNode("MIN_TERM"), XmlElement))
			If Not String.IsNullOrEmpty(getFieldValue(CType(oRateAndTerm.SelectSingleNode("MIN_DEPOSIT"), XmlElement))) Then
				newProduct.MinimumDeposit = Convert.ToDecimal(getFieldValue(CType(oRateAndTerm.SelectSingleNode("MIN_DEPOSIT"), XmlElement)))
			End If

			If Not String.IsNullOrEmpty(getFieldValue(CType(oRateAndTerm.SelectSingleNode("MAX_DEPOSIT"), XmlElement))) Then
				newProduct.MaxDeposit = Convert.ToDecimal(getFieldValue(CType(oRateAndTerm.SelectSingleNode("MAX_DEPOSIT"), XmlElement)))
			End If

			newProduct.Apy = getFieldValue(CType(oRateAndTerm.SelectSingleNode("APY"), XmlElement))

			If Not String.IsNullOrEmpty(getFieldValue(CType(oRateAndTerm.SelectSingleNode("INTEREST_RATE_TYPE"), XmlElement))) Then
				newProduct.InterestRateType = getFieldValue(CType(oRateAndTerm.SelectSingleNode("INTEREST_RATE_TYPE"), XmlElement))
			End If
			'INTEREST_RATE_TYPE is not available in TIERS node

			newProduct.IsAutoCalculateTier = getFieldValue(CType(oRateAndTerm.SelectSingleNode("IS_AUTO_CALCULATE_TIER"), XmlElement)) = "Y"
			newProduct.IsApyAutoCalculated = getFieldValue(CType(oRateAndTerm.SelectSingleNode("IS_APY_AUTO_CALCULATED"), XmlElement)) = "Y"
			newProduct.TermType = getFieldValue(CType(oRateAndTerm.SelectSingleNode("TERM_TYPE"), XmlElement))
			''Extract the max APY and min Deposit from TIERS
			Dim tierNodes = oRateAndTerm.SelectNodes("TIERS/TIER")
			' '' pre_selection has 3 values, for my best guess: P (pre selected) R (required) O (who knows)
			If tierNodes IsNot Nothing AndAlso tierNodes.Count > 0 Then
				Dim maxApy As Decimal = 0D
				Dim minDeposit As Decimal = 1000000D
				Dim maxDeposit As Decimal = 0D
				For Each tn As XmlElement In tierNodes
					Dim rateCode = getFieldValue(CType(tn.SelectSingleNode("RATE_CODE"), XmlElement))
					Dim locationPool = getFieldValue(CType(tn.SelectSingleNode("LOCATION_POOL"), XmlElement))
					Dim zipCodeFilterType = getFieldValue(CType(tn.SelectSingleNode("AVAILABLE_ZIP_CODE_FILTER_TYPE"), XmlElement))
					Dim currMaxDeposit As Decimal = 0D
					If newProduct.AvailableRates.Any(Function(r) r.RateCode = rateCode) Then Continue For
					Dim apy As Decimal = 0D

					Decimal.TryParse(getFieldValue(CType(tn.SelectSingleNode("APY"), XmlElement)), apy)
					If apy > maxApy Then
						maxApy = apy
					End If
					Dim deposit As Decimal = 0D
					Decimal.TryParse(getFieldValue(CType(tn.SelectSingleNode("MIN_DEPOSIT"), XmlElement)), deposit)
					Decimal.TryParse(getFieldValue(CType(tn.SelectSingleNode("MAX_DEPOSIT"), XmlElement)), currMaxDeposit)
					If newProduct.IsAutoCalculateTier Then  ''use min deposit from auto tier rate
						deposit = newProduct.MinimumDeposit
						currMaxDeposit = newProduct.MaxDeposit
					End If
					If currMaxDeposit >= 0 And currMaxDeposit > maxDeposit Then
						maxDeposit = currMaxDeposit
					End If
					'fix null max deposit amount in tier rate
					If currMaxDeposit < 0 Then
						currMaxDeposit = 0D
					End If

					If deposit >= 0 AndAlso deposit < minDeposit Then
						minDeposit = deposit
					End If

					'fix null min deposit amount in tier rate
					If deposit < 0 Then
						deposit = 0D
					End If

					Dim sRate As String = getFieldValue(CType(tn.SelectSingleNode("RATE"), XmlElement))

					Dim rate As Decimal = 0D
					Decimal.TryParse(sRate, rate)
					Dim balance As Decimal = 0D
					Decimal.TryParse(getFieldValue(CType(tn.SelectSingleNode("MIN_BALANCE"), XmlElement)), balance)
					Dim minTerm As Integer = -1
					Integer.TryParse(getFieldValue(CType(tn.SelectSingleNode("MIN_TERM"), XmlElement)), minTerm)
					Dim maxTerm As Integer = -1
					Integer.TryParse(getFieldValue(CType(tn.SelectSingleNode("MAX_TERM"), XmlElement)), maxTerm)
					newProduct.AvailableRates.Add(New CProductRate With {.MaxDeposit = currMaxDeposit, .Apy = apy, .MinBalance = balance, .MinDeposit = deposit, .RateString = sRate, .RateDecimal = rate, .RateCode = rateCode, .Term = minTerm, .LocationPool = locationPool, .ZipCodeFilterType = zipCodeFilterType, .MinTerm = minTerm, .MaxTerm = maxTerm})
				Next
				' AP-3280 - Order tiered rates by Rate, MinBalance, MinDeposit, Term
				newProduct.AvailableRates = newProduct.AvailableRates.OrderBy(Function(o) o.RateDecimal).ThenBy(Function(o) o.MinBalance).ThenBy(Function(o) o.MinDeposit).ThenBy(Function(o) o.Term).ToList()
				newProduct.Apy = maxApy.ToString()
				newProduct.MinimumDeposit = If(minDeposit < 1000000D And minDeposit > 0D, minDeposit, 0)
				newProduct.MaxDeposit = maxDeposit
			End If
			'' Each Product can contain Services, let's find out
			Dim serviceNodes = oNode.SelectNodes("ACCOUNT_SERVICES/SERVICE")
			'' pre_selection has 3 values, for my best guess: P (pre selected) R (required) O (who knows)
			If serviceNodes IsNot Nothing AndAlso serviceNodes.Count > 0 Then
				For Each sn As XmlElement In serviceNodes
					Dim isActive = getFieldValue(CType(sn.SelectSingleNode("IS_ACTIVE"), XmlElement)) = "Y"
					If isActive Then
						Dim ps As New CProductService() With
					   {
							.ServiceType = getFieldValue(CType(sn.SelectSingleNode("SERVICE_TYPE"), XmlElement)),
							.ServiceCode = getFieldValue(CType(sn.SelectSingleNode("SERVICE_CODE"), XmlElement)),
							.Description = getFieldValue(CType(sn.SelectSingleNode("DESCRIPTION"), XmlElement)),
							.DescriptionURL = Common.SafeString(getFieldValue(CType(sn.SelectSingleNode("LINK_DESCRIPTION"), XmlElement))),
						 .IsActive = True,
						 .IsPreSelection = getFieldValue(CType(sn.SelectSingleNode("PRE_SELECTION"), XmlElement)) = "P",
						.IsRequired = getFieldValue(CType(sn.SelectSingleNode("PRE_SELECTION"), XmlElement)) = "R"
						}
						''make sure sevice code is not empty
						If Not String.IsNullOrEmpty(ps.ServiceCode) Then
							newProduct.Services.Add(ps)
						End If
					End If
				Next
			End If
			oDownLoadProducts.Add(newProduct)
		Next
		oDownLoadProducts = oDownLoadProducts.OrderBy(Function(o) o.Position).ToList()
		Return oDownLoadProducts
	End Function

	Public Shared Function GetMinorAccountProductsFromDownload(ByVal oProducts As List(Of CProduct), ByVal oMinorAccountTypes As List(Of CMinorAccount)) As List(Of MinorAccountProduct)
		Dim mCurrentAccountProducts As New List(Of MinorAccountProduct)
		Dim mCurrentProducts As New List(Of CProduct)
		''convert special account type to upper case
		For Each oProd In oProducts
			Dim specialAccounTypes = oProd.specialAccountTypes
			If specialAccounTypes.Count > 0 Then
				oProd.specialAccountTypes = specialAccounTypes.ConvertAll(Function(up) up.ToUpperInvariant())
			End If
			mCurrentProducts.Add(oProd)
		Next
		For Each oMAType In oMinorAccountTypes
			Dim mAccountProduct As New MinorAccountProduct()
			Dim mAccountTypes As New List(Of String)
			Dim mProducts = mCurrentProducts.Where(Function(x) x.specialAccountTypes.Contains(oMAType.minorTitle.ToUpper)).ToList
			If mProducts IsNot Nothing AndAlso mProducts.Count > 0 Then
				mAccountTypes.AddRange(From mProduct In mProducts Select mProduct.ProductCode)
			End If
			mAccountProduct.minorAccountCode = oMAType.minorAccountCode
			mAccountProduct.minorAccountProductList = mAccountTypes
			mCurrentAccountProducts.Add(mAccountProduct)
		Next
		Return mCurrentAccountProducts
	End Function
	Private Shared Function GetMinorAccountCodesFromDownload(ByVal oProducts As List(Of CProduct), ByVal oMinorAccountTypes As List(Of CMinorAccount)) As Dictionary(Of String, String)
		Dim mCurrentAccountProducts = GetMinorAccountProductsFromDownload(oProducts, oMinorAccountTypes)
		Dim mCurrentAccountCodes As New Dictionary(Of String, String)
		Dim mProducts As New List(Of String)
		For Each mAccountProduct In mCurrentAccountProducts
			mProducts.AddRange(mAccountProduct.minorAccountProductList)
		Next
		mProducts = mProducts.Distinct().ToList
		For Each mProduct In mProducts
			Dim mProds As New List(Of String)
			Dim mCurrentProducts = mCurrentAccountProducts.Where(Function(x) x.minorAccountProductList.Contains(mProduct)).Distinct().ToList
			If mCurrentProducts IsNot Nothing AndAlso mCurrentProducts.Count > 0 Then
				mProds.AddRange(From oProd In mCurrentProducts Select oProd.minorAccountCode)
			End If
			If Not mCurrentAccountCodes.ContainsKey(mProduct) And mProds.Count > 0 Then
				If mProds.Count = 1 Then
					mCurrentAccountCodes.Add(mProduct, mProds(0))
				Else
					mCurrentAccountCodes.Add(mProduct, String.Join("|", mProds))
				End If

			End If
		Next
		Return mCurrentAccountCodes
	End Function
#End Region
End Class
