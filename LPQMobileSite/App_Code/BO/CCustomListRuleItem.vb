﻿Imports Newtonsoft.Json
Imports Microsoft.VisualBasic
Imports System.Web.Script.Serialization
Namespace CustomList

Public Class CCustomListRuleItem
	Public Property Name As String
	Public Property Code As String
	Public Property Description As String
	Public Property Enable As Boolean
	Public Property Columns As List(Of CCustomListRuleColumn)
	Public Property FilterIDs As List(Of String)
	Public Property List As Object
	Public Property DialogTitle As String
	Public Property DialogMessage As String

	Public Function GetList(Of T As CCustomListRuleBaseItem)() As List(Of T)
		Dim result As List(Of T)
		If List IsNot Nothing Then
			result = JsonConvert.DeserializeObject(Of List(Of T))(List.ToString())
			'result = New JavaScriptSerializer().Deserialize(Of List(Of T))(List.ToString())
		End If
		Return result
	End Function
	End Class

	Public Class CCustomListRuleColumn
		Public Property Name As String
		Public Property DisplayText As String
		Public Property Type As String
	End Class
End Namespace
