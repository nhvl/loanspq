﻿Imports Microsoft.VisualBasic
Imports LPQMobile.Utils
Imports System.Xml

Public Class CProductRendering

    Private ReadOnly _log As log4net.ILog = log4net.LogManager.GetLogger(Me.GetType())
    Private ReadOnly _ProductsList As New List(Of ProductInfo)
    Private _NotFundedProductCodes As New List(Of String)

    Sub New(productList As List(Of CProduct))
        populateProducts(productList)
    End Sub


    Private Sub populateProducts(ByVal listProducts As List(Of CProduct))
        Dim valueStr As String = ""
        _ProductsList.Clear()
        Dim tempAccountInfo As ProductInfo
        For Each prod As CProduct In listProducts

            tempAccountInfo = New ProductInfo
            tempAccountInfo.ProductName = prod.AccountName
            tempAccountInfo.ProductMinDeposit = prod.MinimumDeposit.ToString()
            tempAccountInfo.ProductMaxDeposit = prod.MaxDeposit.ToString()
            tempAccountInfo.ProductIsRequired = If(prod.IsRequired, "Y", "N")
            tempAccountInfo.ProductIsPreselected = If(prod.IsPreSelection, "Y", "N")
            tempAccountInfo.ProductCode = prod.ProductCode
            tempAccountInfo.AccountType = prod.AccountType
            tempAccountInfo.ProductDescription = prod.ProductDescription
            '' set product services from outer object CProduct
            tempAccountInfo.ProductServices = prod.Services
            tempAccountInfo.MinorAccountCode = prod.MinorAccountCode
            If prod.cannotBeFunded = "Y" Then
                _NotFundedProductCodes.Add(prod.ProductCode)
            End If
            _ProductsList.Add(tempAccountInfo)
        Next

        ' To make it look nice, we can move the required accounts to the top of the list before we render them.
        moveRequiredToTop()
        ' Set the hidden fields so that Javascript code can access it
    End Sub

    Private Sub moveRequiredToTop()
        Dim requiredProductsList As New List(Of ProductInfo)
        Dim optionalProductsList As New List(Of ProductInfo)
        ' Now go through each account type and write it to the list if it is required
        For Each anAccount As ProductInfo In _ProductsList
            If (anAccount.ProductIsRequired.ToUpper() = "Y") Then
                ' Required account found
                requiredProductsList.Add(anAccount)
            Else
                optionalProductsList.Add(anAccount)
            End If
        Next

        ' Now put the two lists back together with the required ones going first
        _ProductsList.Clear()
        For Each requiredAccount As ProductInfo In requiredProductsList
            _ProductsList.Add(requiredAccount)
        Next

        For Each optionalAccount As ProductInfo In optionalProductsList
            _ProductsList.Add(optionalAccount)
        Next
    End Sub



    Public Function RenderHiddenProducts() As String
        ' Create some hidden fields to hold the minimum deposit amount for each Product
        ' 
        Dim i As Integer
        Dim returnStr As String
        returnStr = ""

        For i = 1 To _ProductsList.Count
            returnStr += "<input type='hidden' runat='server' id='hdProduct"
            returnStr += i.ToString() + "'"
            returnStr += " value='" + _ProductsList(i - 1).ProductCode + "' />"
            returnStr += System.Environment.NewLine
        Next

        Return returnStr
    End Function

    Public Function RenderHiddenRequiredProducts() As String
        ' Create some hidden fields to hold the minimum deposit amount for each Product
        ' 
        Dim i As Integer
        Dim returnStr As String
        returnStr = ""

        For i = 1 To _ProductsList.Count
            returnStr += "<input type='hidden' runat='server' id='hdRequiredProduct"
            returnStr += i.ToString() + "'"
            'returnStr += _ProductsList(i - 1).ProductType + "'"
            returnStr += " value='"
            returnStr += _ProductsList(i - 1).ProductIsRequired.ToUpper()
            returnStr += "' />"
            returnStr += System.Environment.NewLine
        Next

        Return returnStr
    End Function

    Public Function RenderHiddenMinDeposits() As String
        ' Create some hidden fields to hold the minimum deposit amount for each Product
        ' 
        Dim i As Integer
        Dim returnStr As String
        returnStr = ""

        For i = 1 To _ProductsList.Count
            returnStr += "<input type='hidden' runat='server' id='hdMinAmount"
            returnStr += i.ToString() + "'"
            'returnStr += _ProductsList(i - 1).ProductType + "'"
            returnStr += " value='" + _ProductsList(i - 1).ProductMinDeposit + "' />"
            returnStr += System.Environment.NewLine
        Next

        Return returnStr
    End Function

    Public Function RenderHiddenNumProducts() As String
        Dim returnStr As String
        returnStr = "<input type='hidden' runat='server' id='hdNumProducts' value = '"
        returnStr += _ProductsList.Count.ToString()
        returnStr += "'>"
        Return returnStr
    End Function

    '''****** display all initial deposit text fields
    Public Function RenderInitialDepositProduct(ByVal inputType As String) As String
        Dim index As Integer
        Dim returnStr As String = ""
        For index = 0 To _ProductsList.Count - 1
            ''no render initial product if product isnot funded
            Dim cannotFunded As Boolean = False
            If _NotFundedProductCodes.Count > 0 Then
                For Each item In _NotFundedProductCodes
                    If (_ProductsList(index).ProductCode = item) Then
                        cannotFunded = True
                        Exit For
                    End If
                Next
            End If
            If cannotFunded Then
                Continue For
            End If
            returnStr += BuildInitialDepositProduct(index, inputType)
        Next
        Return returnStr
    End Function

    Public Function BuildInitialDepositProduct(ByVal index As Integer, ByVal inputType As String) As String
        Dim strDepositProduct As String = ""
        'Dim productName As String = "<b>" & _ProductsList(index).ProductName & "</b>"
        Dim productName As String = _ProductsList(index).ProductName & "<span data-name='depositamount'>"
        Dim minAmount = Common.SafeDouble(_ProductsList(index).ProductMinDeposit)
        Dim maxAmount = Common.SafeDouble(_ProductsList(index).ProductMaxDeposit)
        Dim strMaxAmountHtml As String = ""
        Dim requiredStar As String = ""
        If minAmount < 0 Then
            minAmount = 0.0
        End If
        If maxAmount > 0 Then
            strMaxAmountHtml = "<span data-name='maxdeposit'>, $" & CType(maxAmount, Decimal).ToString("0.00") & " max</span>"
        End If
        Dim reqStar As String = ""
        ''need to display amount min deposit with the title field
        If minAmount > 0 Then
            productName = productName & "(<span data-name='mindeposit'>$" & CType(minAmount, Decimal).ToString("0.00") & " min</span>" & strMaxAmountHtml & ")"
            reqStar = "*"
		ElseIf minAmount = 0 And maxAmount > 0 Then	''no min Deposit, and only max deposit 
			productName = productName & "(<span data-name='mindeposit'></span><span data-name='maxdeposit'>$" & CType(maxAmount, Decimal).ToString("0.00") & " max</span>)"
        End If
        requiredStar = "<span id ='spDepositAmount_chk" & _ProductsList(index).ProductCode & "' style='display: none;' class='require-span'>" & reqStar & "</span>"
        productName = productName & requiredStar & "</span>"
		strDepositProduct += "<div style='display: none' id ='divDepositAmount_chk" & _ProductsList(index).ProductCode & "' ><div data-role='fieldcontain'><label>" & productName & "</label>"
        strDepositProduct += "<input aria-labelledby='divDepositAmount_chk" & _ProductsList(index).ProductCode & "' type ='" & inputType & "' id ='txtDepositAmount_chk" & _ProductsList(index).ProductCode & "' value ='"
        strDepositProduct += FormatCurrency(minAmount) & "' data-field='" & _ProductsList(index).ProductCode & "' class='money'/></div></div>"
        Return strDepositProduct
    End Function

	Public Function BuildProductQuestions(oProducts As List(Of CProduct), ByVal inputType As String, Optional DDLInsteadOfYN As Boolean = False) As String
		Dim strQuestions As String = ""
		Dim isRequired As String = ""
		Dim answerType As String = ""
		Dim questionText As String = ""
		Dim questionName As String = ""
		Dim productCode As String = ""
		For Each oProductNode As CProduct In oProducts
			Dim i As Integer = 1
			Dim qIndex As Integer = 1
			productCode = "chk" & oProductNode.ProductCode
			Dim questionCount As Integer = oProductNode.CustomQuestions.Count
			If questionCount = 1 Then
				''reset question to 0 if answerType is Header
				If oProductNode.CustomQuestions(0).AnswerType = "HEADER" Then
					questionCount = 0
				End If
			End If
			strQuestions += "<div class='ProductQuestionWidth LineSpace product-questions' style ='display: none;' data-eid ='divProductQuestion_" & productCode & "' question_count ='" & questionCount & "' >"
			'strQuestions += "<span class='panel-title-font labelText-bold'>" & oProductNode.AccountName & "</span>"
			For Each oCustomQuestion As CProductCustomQuestion In oProductNode.CustomQuestions
				Dim isHeaderType As Boolean = False
				answerType = oCustomQuestion.AnswerType
				''remove class or css if it exist in <p>
				Dim strMaxLength As String = ""
				If Not String.IsNullOrEmpty(oCustomQuestion.max_value) Then
					strMaxLength = " maxlength='" & oCustomQuestion.max_value & "'"
				Else
					strMaxLength = " maxlength='100'" ''default
				End If
				questionText = Common.CleanupQuestionText(oCustomQuestion.QuestionText)
				questionName = oCustomQuestion.QuestionName
				If oCustomQuestion.IsRequired Then
					isRequired = "Y"
				Else
					isRequired = "N"
				End If
				Dim randomId As String = Guid.NewGuid().ToString().Replace("-", "").Substring(0, 8).ToLower()
				''add reqular expression and error message attributes
				Dim strExtraData = "reg_expression='" & Common.SafeString(oCustomQuestion.reg_expression) & "' error_message ='" & Common.SafeString(oCustomQuestion.errorMessage) & "'"
                '' add id for each label question -> display question text in review product question
                strQuestions += String.Format("<div class='pq-wrapper' qname='{0}' id='{1}'>", questionName, "pq_" & qIndex & "_wrapper_" & oProductNode.ProductCode)
                Select Case answerType
					Case "PASSWORD"
						strQuestions += "<div class='LineSpace labeltext-bold' data-eid='questionName_" & i & "_" & productCode & "' id='questionName_" & i & "_" & productCode & "_" & randomId & "'>" & questionText & "<span data-eid ='reqQuestion_" & i & "_" & productCode & "' id ='reqQuestion_" & i & "_" & productCode & "_" & randomId & "' style ='display: none;' class='require-span' >*</span></div>"
						strQuestions += "<input data-role='none' aria-labelledby='questionName_" & i & "_" & productCode & "_" & randomId & "' type ='password'" & strMaxLength & " name='question_" & i & "_" & productCode & "' iname='" & questionName & "' data-eid ='question_" & i & "_" & productCode & "' id ='question_" & i & "_" & productCode & "_" & randomId & "' question_required ='" & isRequired & "' class='ProductQuestion' " & strExtraData & " confirmation_required ='" & oCustomQuestion.RequireConfirmation & "' confirmation_text='" & oCustomQuestion.ConfirmationText & "'/>"
						If oCustomQuestion.RequireConfirmation Then
							strQuestions += "<div class='LineSpace labeltext-bold' data-eid='re_questionName_" & i & "_" & productCode & "' id='re_questionName_" & i & "_" & productCode & "_" & randomId & "'> Re-enter " & questionText & "<span data-eid ='re_reqQuestion_" & i & "_" & productCode & "' id ='re_reqQuestion_" & i & "_" & productCode & "_" & randomId & "' style ='display: none;' class='require-span' >*</span></div>"
							strQuestions += "<input data-role='none' aria-labelledby='questionName_" & i & "_" & productCode & "_" & randomId & "' type ='password'" & strMaxLength & " name='question_" & i & "_" & productCode & "' iname='" & questionName & "' data-eid ='re_question_" & i & "_" & productCode & "' id ='re_question_" & i & "_" & productCode & "_" & randomId & "' question_required ='" & isRequired & "' class='ProductQuestion' />"
						End If
					Case "TEXTBOX"
						strQuestions += "<div class='LineSpace labeltext-bold' data-eid='questionName_" & i & "_" & productCode & "' id='questionName_" & i & "_" & productCode & "_" & randomId & "'>" & questionText & "<span data-eid ='reqQuestion_" & i & "_" & productCode & "' id ='reqQuestion_" & i & "_" & productCode & "_" & randomId & "' style ='display: none;' class='require-span' >*</span></div>"
						strQuestions += "<input data-role='none' aria-labelledby='questionName_" & i & "_" & productCode & "_" & randomId & "' type ='text'" & strMaxLength & " name='question_" & i & "_" & productCode & "' iname='" & questionName & "' data-eid = 'question_" & i & "_" & productCode & "' id = 'question_" & i & "_" & productCode & "_" & randomId & "' question_required ='" & isRequired & "' class='ProductQuestion' " & strExtraData & "/>"
					Case "DROPDOWN"
						If (oCustomQuestion.RestrictedAnswers.Count = 2 Or oCustomQuestion.RestrictedAnswers.Count = 3) And oCustomQuestion.RestrictedAnswers.Exists(Function(answer As CProductRestrictedAnswer) answer.Value.ToUpper() = "YES" And oCustomQuestion.RestrictedAnswers.Exists(Function(answer1 As CProductRestrictedAnswer) answer1.Value.ToUpper() = "NO")) Then
							strQuestions += "<div class='LineSpace labeltext-bold' data-eid='questionName_" & i & "_" & productCode & "' id='questionName_" & i & "_" & productCode & "_" & randomId & "'>" & questionText & "<span data-eid ='reqQuestion_" & i & "_" & productCode & "' d ='reqQuestion_" & i & "_" & productCode & "_" & randomId & "' style ='display: none;' class='require-span' >*</span></div>"
							If DDLInsteadOfYN = False Or (oCustomQuestion.RestrictedAnswers.Exists(Function(answer As CProductRestrictedAnswer) answer.Text.Trim().ToUpper() = "YES") And oCustomQuestion.RestrictedAnswers.Exists(Function(answer As CProductRestrictedAnswer) answer.Text.Trim().ToUpper() = "NO")) Then
								strQuestions += "<div class='yesno-group' iname='" & questionName & "' data-value='' name='question_" & i & "_" & productCode & "' data-eid ='question_" & i & "_" & productCode & "' id ='question_" & i & "_" & productCode & "_" & randomId & "' question_required ='" & isRequired & "'><div class='yes-opt' tabindex='0' data-value='yes'>Yes</div><div class='no-opt' tabindex='0' data-value='no'>No</div></div><div style='clear:both;'></div>"
							Else
								strQuestions += "<select data-role='none' iname='" & questionName & "' aria-labelledby='questionName_" & i & "_" & productCode & "_" & randomId & "' name='question_" & i & "_" & productCode & "' data-eid ='question_" & i & "_" & productCode & "' id ='question_" & i & "_" & productCode & "_" & randomId & "' question_required ='" & isRequired & "' class='ProductQuestion'/>"
								For Each oRestrictAns As CProductRestrictedAnswer In oCustomQuestion.RestrictedAnswers
									''If oRestrictAns.Text.Trim() = "" Then Continue For -->this case is for default selection
									strQuestions += "<option value=""" & oRestrictAns.Value & """>" & oRestrictAns.Text & "</option>"
								Next
								strQuestions += "</select>"
							End If
						Else
							strQuestions += "<div class='LineSpace labeltext-bold' data-eid='questionName_" & i & "_" & productCode & "' id='questionName_" & i & "_" & productCode & "_" & randomId & "'>" & questionText & "<span data-eid ='reqQuestion_" & i & "_" & productCode & "'style ='display: none;' class='require-span' >*</span></div>"
							strQuestions += "<select data-role='none' iname='" & questionName & "' aria-labelledby='questionName_" & i & "_" & productCode & "_" & randomId & "' name='question_" & i & "_" & productCode & "'  data-eid ='question_" & i & "_" & productCode & "' id ='question_" & i & "_" & productCode & "_" & randomId & "' question_required ='" & isRequired & "' class='ProductQuestion'/>"
							For Each oRestrictAns As CProductRestrictedAnswer In oCustomQuestion.RestrictedAnswers
								strQuestions += "<option value=""" & oRestrictAns.Value & """>" & oRestrictAns.Text & "</option>"
							Next
							strQuestions += "</select>"
						End If
					Case "CHECKBOX"
						strQuestions += "<div class='LineSpace labeltext-bold' data-eid='questionName_" & i & "_" & productCode & "' id='questionName_" & i & "_" & productCode & "_" & randomId & "'>" & questionText & "<span data-eid ='reqQuestion_" & i & "_" & productCode & "' id ='reqQuestion_" & i & "_" & productCode & "_" & randomId & "' style ='display: none;' class='require-span' >*</span></div>"
						strQuestions += "<div chknum='" & oCustomQuestion.RestrictedAnswers.Count & "' data-eid ='question_" & i & "_" & productCode & "' id ='question_" & i & "_" & productCode & "_" & randomId & "' question_required ='" & isRequired & "' > </div>"
						strQuestions += "<fieldset data-role='controlgroup' class='ProductQuestion'>"
						Dim j As Integer = 0
						For Each oRestrictAns As CProductRestrictedAnswer In oCustomQuestion.RestrictedAnswers
							'strQuestions += "<input data-role='none' iname='" & questionName & "' type ='checkbox' name='question_" & i & "_" & productCode & "_" & j & "' data-eid ='question_" & i & "_" & productCode & "_" & j & "' id='question_" & i & "_" & productCode & "_" & j & "_" & randomId & "' value ='" & oRestrictAns.Value & "'/>"
							strQuestions += "<label for ='question_" & i & "_" & productCode & "_" & j & "_" & randomId & "' data-eid='chkName_" & i & "_" & j & "' id='chkName_" & i & "_" & j & "_" & randomId & "'>" & oRestrictAns.Text & "<input data-role='none' iname='" & questionName & "' type ='checkbox' name='question_" & i & "_" & productCode & "_" & j & "' data-eid ='question_" & i & "_" & productCode & "_" & j & "' id='question_" & i & "_" & productCode & "_" & j & "_" & randomId & "' value ='" & oRestrictAns.Value & "'/>" & "</label>"
							j += 1
						Next
						strQuestions += "</fieldset>"
					Case "DATE"
						strQuestions += "<div class='LineSpace labeltext-bold' data-eid='questionName_" & i & "_" & productCode & "' id='questionName_" & i & "_" & productCode & "_" & randomId & "'>" & questionText & "<span data-eid ='reqQuestion_" & i & "_" & productCode & "' id ='reqQuestion_" & i & "_" & productCode & "_" & randomId & "' style ='display: none;' class='require-span' >*</span></div>"
						strQuestions += "<input data-role='none' iname='" & questionName & "' aria-labelledby='questionName_" & i & "_" & productCode & "_" & randomId & "' type='" & inputType & "' name='question_" & i & "_" & productCode & "' data-eid ='question_" & i & "_" & productCode & "' id ='question_" & i & "_" & productCode & "_" & randomId & "' class ='indate' pattern='[0-9]*' question_required ='" & isRequired & "' class='ProductQuestion' " & strExtraData & "/>"
					Case "HEADER" ''not available for rendering
						isHeaderType = True
				End Select
				strQuestions += "</div>"
				If Not isHeaderType Then
					qIndex += 1
				End If
				i += 1
			Next
			strQuestions += "<br/></div>"
		Next
		Return strQuestions
	End Function


End Class