﻿Imports Microsoft.VisualBasic
Imports System.Xml
Imports System.Net
Imports System.IO
Imports LPQMobile.BO
Imports LPQMobile.Utils
Imports System.Text
Public Class CMinorGetWalletQuestions
    Private log As log4net.ILog = log4net.LogManager.GetLogger(GetType(CMinorGetWalletQuestions))
    Public Sub New()
    End Sub

    Public Function GetOutOfWalletQuestionsRSA(ByVal responseStr As String, ByVal loanID As String, ByVal oConfig As CWebsiteConfig, ByVal sApplicantIndex As String, ByVal isJoint As Boolean) As String
        Return GetOutOfWalletQuestions(responseStr, loanID, oConfig, sApplicantIndex, isJoint, "RSA")
    End Function

    Public Function GetOutOfWalletQuestionsPID(ByVal responseStr As String, ByVal loanID As String, ByVal oConfig As CWebsiteConfig, ByVal sApplicantIndex As String, ByVal isJoint As Boolean) As String
        Return GetOutOfWalletQuestions(responseStr, loanID, oConfig, sApplicantIndex, isJoint, "PID")
    End Function

    Public Function GetOutOfWalletQuestionsEquifax(ByVal responseStr As String, ByVal loanID As String, ByVal oConfig As CWebsiteConfig, ByVal sApplicantIndex As String, ByVal isJoint As Boolean) As String
        Return GetOutOfWalletQuestions(responseStr, loanID, oConfig, sApplicantIndex, isJoint, "EID")
    End Function

    Public Function GetOutOfWalletQuestionsFIS(ByVal responseStr As String, ByVal loanID As String, ByVal oConfig As CWebsiteConfig, ByVal sApplicantIndex As String, ByVal isJoint As Boolean) As String
        Return GetOutOfWalletQuestions(responseStr, loanID, oConfig, sApplicantIndex, isJoint, "FIS")
    End Function

    Public Function GetOutOfWalletQuestionsDID(ByVal responseStr As String, ByVal loanID As String, ByVal oConfig As CWebsiteConfig, ByVal sApplicantIndex As String, ByVal isJoint As Boolean) As String
        Return GetOutOfWalletQuestions(responseStr, loanID, oConfig, sApplicantIndex, isJoint, "DID")
    End Function

    Public Function GetOutOfWalletQuestionsTID(ByVal responseStr As String, ByVal loanID As String, ByVal oConfig As CWebsiteConfig, ByVal sApplicantIndex As String, ByVal isJoint As Boolean) As String
        Return GetOutOfWalletQuestions(responseStr, loanID, oConfig, sApplicantIndex, isJoint, "TID")
    End Function


    Public Function GetOutOfWalletQuestions(ByVal responseStr As String, ByVal loanID As String, ByVal oConfig As CWebsiteConfig, ByVal sApplicantIndex As String, ByVal isJoint As Boolean, ByVal sIDAType As String) As String
        ' Build the XML to submit for getting the Out of Wallet Questions
        ' Build an XML object from the response string 
        Dim myDocument As New XmlDocument
        myDocument.LoadXml(responseStr)

        Dim tempNodes As XmlNodeList
        loanID = ""
        Dim loanIDXMLStr As String
        tempNodes = myDocument.GetElementsByTagName("RESPONSE")
        For Each childNode As XmlNode In tempNodes
            loanIDXMLStr = childNode.Attributes("loan_id").InnerXml
            If (Not String.IsNullOrEmpty(loanIDXMLStr)) Then
                loanID = loanIDXMLStr
            End If
        Next

        If (loanID = "") Then
            Return ""
        End If

        ' Now construct the XML response
        Dim sb As New StringBuilder()
        Dim writer As XmlWriter = XmlTextWriter.Create(sb)
        writer.WriteStartDocument()

        ' begin input
        writer.WriteStartElement("INPUT")

        writer.WriteStartElement("LOGIN")
        If sIDAType = "FIS" Then
            'FIS IDA ONLY work with lender level user
            If oConfig.APIUserLender <> "" Then
                writer.WriteAttributeString("api_user_id", oConfig.APIUserLender)
                writer.WriteAttributeString("api_password", oConfig.APIPasswordLender)
            Else
                writer.WriteAttributeString("api_user_id", oConfig.APIUser)
                writer.WriteAttributeString("api_password", oConfig.APIPassword)
            End If
        Else
            writer.WriteAttributeString("api_user_id", oConfig.APIUser)
            writer.WriteAttributeString("api_password", oConfig.APIPassword)
        End If

        writer.WriteEndElement()

        writer.WriteStartElement("REQUEST")
        If sIDAType = "RSA" Or sIDAType = "EID" Or sIDAType = "PID" Or sIDAType = "DID" Then
            If sIDAType = "DID" Then
                writer.WriteAttributeString("xpress_app_id", loanID)
            Else
                writer.WriteAttributeString("app_id", loanID)
            End If
            writer.WriteAttributeString("applicant_index", sApplicantIndex)
            If (isJoint) Then
                writer.WriteAttributeString("is_joint", "true")
            Else
                writer.WriteAttributeString("is_joint", "false")
            End If
        ElseIf sIDAType = "FIS" Or sIDAType = "TID" Then
            writer.WriteAttributeString("xpressappid", loanID)
            If sIDAType = "TID" Then
                writer.WriteAttributeString("mode", "IDA")
            End If
            writer.WriteAttributeString("applicant_index", sApplicantIndex)
            If (isJoint) Then
                writer.WriteAttributeString("applicant_type", "JOINT")
            Else
                writer.WriteAttributeString("applicant_type", "PRIMARY")

            End If
        End If

        writer.WriteEndElement()
        ' end input
        writer.WriteEndElement()
        writer.Flush()
        writer.Close()

        Dim returnStr = sb.ToString()
        Return returnStr
    End Function

End Class
