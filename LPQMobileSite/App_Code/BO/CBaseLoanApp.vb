﻿Imports Microsoft.VisualBasic
Imports System.Collections
Imports System.Xml
Imports System.Web
Imports LPQMobile.Utils.Common

Imports log4net
Imports LPQMobile.Utils

Namespace LPQMobile.BO

    Public MustInherit Class CBaseLoanApp
        Public MustOverride ReadOnly Property LoanType() As String

        Public log As ILog = LogManager.GetLogger(Me.GetType)
        ''business loans
        Public BLBusinessInfo As CBLBusinessInfo ' business info
        Public BLApplicants As List(Of CBLApplicantInfo)
        Public BLBeneficialOwners As New List(Of CBeneficialOwnerInfo)
        Public BLDeclarationDic As Dictionary(Of String, CBLDeclarationInfo)
		Public BLCustomQuestionAnswers As New List(Of CValidatedQuestionAnswers)
		Public IsUrlParaCustomQuestion As Boolean = False
		Public SelectedLocationPool As List(Of String)

		Public isBusinessLoan As Boolean = False
		''
		Public LoanPurpose As String
		Public Applicants As List(Of CApplicant)
		Public CoApplicantType As String
		Public CustomQuestionAnswers As New List(Of CValidatedQuestionAnswers)()
		''new api
		Public isCQNewAPI As Boolean = False
		Public DownloadedAQs As New List(Of CCustomQuestion)
		Public DownloadedXAAQs As New List(Of CCustomQuestionXA)

		Protected OrganizationID As String
		Protected LenderID As String
		Public BranchID As String
		Public LoanOfficerID As String

		Public ReferralSource As String
		Public IsSSO As Boolean = False	 'single sign on from another homebankin where member info is prefill
		Public IsComboMode As Boolean = False	'both loan and xa are created for new member
		Public isDisagreeSelect As String
		'' --disclosure, current email, ips, user agents for internal comment 

		Public Disclosure As String
		Public CurrentEmail As String
		Public IPAddress As String
		Public UserAgent As String
		Public HasDebtCancellation As Boolean
		Protected Const HasDebtCancellationText As String = "INTERESTED IN DEBT CANCELLATION" 'value to send in loan XML

		Public DocBase64Dic As New Dictionary(Of String, String) 'title, dada
		Public UploadDocumentInfoList As List(Of uploadDocumentInfo) ' doc_title,doc group
		Public OtherLoanNumber As String = "" 'this can be mapped to LPQ reference# which can be map to Lending Tree QFName(app#)
		Public BadMemberComment As String = ""
		Public InstaTouchPrefillComment As String = ""
		Public ClinicName As String	'this if for clinic app
		Public ClinicID As String	'this if for clinic app
		Public ClinicWorkerID As String	'this if for clinic app
		''cuna question Answers
		Public cunaQuestionAnswers As New List(Of String)
		''xSell Comment message
		Public xSellCommentMessage As String = ""

		'internal to vendor dashboard
		Public VendorID As String
		Public UserID As Guid
		Public VendorName As String
		'using to idenitify in-completed loan saving in Loans table.
		Public LoanSavingID As Guid
		Public LoanRawData As String
		Public Property AppNSS As CNSSInfo
		Public Property CoAppNSS As CNSSInfo

		'LPQ inbranch setting
		Public LPQVendorID As String  'This can be lpqDealerNumer for Vl or clinicID for merchant
		Public LPQWorkerID As String   'lpqworkerID, this is not necessary if client doesn't use inbranch clinic but is is required by decisionLoan schema
		Public Sub New(ByVal psOrganizationId As String, ByVal psLenderId As String)
			Me.OrganizationID = psOrganizationId
			Me.LenderID = psLenderId
		End Sub

		Public MustOverride Function GetXml(oConfig As CWebsiteConfig) As XmlDocument

		Protected Sub AddApplicantsXml(ByVal poDoc As XmlDocument, oConfig As CWebsiteConfig)
			Dim oxmlApplicants As XmlElement = CType(poDoc.DocumentElement.AppendChild(poDoc.CreateElement("APPLICANTS", NAMESPACE_URI)), XmlElement)

			Dim oApplicant As CApplicant
			oApplicant = Applicants(0)
			Dim oxmlApplicant As XmlElement
			If TypeOf Me Is CXpressApp Then
				oxmlApplicant = AddXAApplicantXml(oxmlApplicants, oApplicant, IIf(oApplicant Is Applicants(0), "P", "C").ToString(), "APPLICANT", oConfig)
			Else
				oxmlApplicant = AddApplicantXml(oxmlApplicants, oApplicant, IIf(oApplicant Is Applicants(0), "P", "C").ToString(), "APPLICANT")
			End If

			If oApplicant.HasSpouse Then
				oApplicant = Applicants(1)
				Dim sCoApplicantType As String = "C" 'CO-BORROWER is the default
				If CoApplicantType = "CO-SIGNER" Then
					sCoApplicantType = "S"
				End If
				If CoApplicantType = "GUARANTOR" Then
					sCoApplicantType = "G"
				End If

				If TypeOf Me Is CXpressApp Then
					AddXAApplicantXml(oxmlApplicant, oApplicant, IIf(oApplicant Is Applicants(0), "P", "C").ToString(), "SPOUSE", oConfig)
				Else
					AddApplicantXml(oxmlApplicant, oApplicant, IIf(oApplicant Is Applicants(0), "P", sCoApplicantType).ToString(), "SPOUSE")
				End If

			End If
			''

			If TypeOf Me Is CHomeEquityLoan Then

				Dim oHELoan As CHomeEquityLoan = CType(Me, CHomeEquityLoan)

				Dim oxmlPresentHousingExpense As XmlElement = CType(oxmlApplicant.AppendChild(oxmlApplicant.OwnerDocument.CreateElement("PRESENT_HOUSING_EXPENSE", NAMESPACE_URI)), XmlElement)
				''   oxmlPresentHousingExpense.SetAttribute("other_pi", oHELoan.OtherLoanPrincipleAndInterest)
				oxmlPresentHousingExpense.SetAttribute("hazard_insurance", oHELoan.HazardInsurance)
				oxmlPresentHousingExpense.SetAttribute("real_estate_tax", oHELoan.PropertyTaxes)
				oxmlPresentHousingExpense.SetAttribute("mortgage_insurance", oHELoan.MortgageInsurance)
				oxmlPresentHousingExpense.SetAttribute("hoa_due", oHELoan.AssociationDues)
			End If

		End Sub
		Protected Function AddApplicantXml(ByVal poParent As XmlElement, ByVal poApplicant As CApplicant, ByVal psApplicantType As String, ByVal psApplicantNodeName As String) As XmlElement
			Dim oxmlApplicant As XmlElement = CType(poParent.AppendChild(poParent.OwnerDocument.CreateElement(psApplicantNodeName, NAMESPACE_URI)), XmlElement)
			With poApplicant

				oxmlApplicant.SetAttribute("first_name", .FirstName.NullSafeToUpper_)
				oxmlApplicant.SetAttribute("last_name", .LastName.NullSafeToUpper_)
				oxmlApplicant.SetAttribute("suffix", .Suffix.NullSafeToUpper_)
				oxmlApplicant.SetAttribute("middle_name", .MiddleInitial.NullSafeToUpper_)
				oxmlApplicant.SetAttribute("ssn", .SSN)
				Dim sDOB As String = SafeDateXml(.DOB)
				If sDOB <> "" Then
					oxmlApplicant.SetAttribute("dob", sDOB)	'1967-08-dd
				End If
				oxmlApplicant.SetAttribute("citizenship", CEnum.MapCitizenshipStatusToValue(.CitizenshipStatus))
				oxmlApplicant.SetAttribute("member_number", SafeString(.MemberNumber).Replace("n/a", "").Replace("N/A", "")) 'not sure why this value is passed to lenderside bc there is no number format along this path
				If Not String.IsNullOrEmpty(SafeString(.RelationshipToPrimary)) Then
					oxmlApplicant.SetAttribute("relation_to_primary", SafeString(.RelationshipToPrimary))
				End If
				oxmlApplicant.SetAttribute("marital_status", CEnum.MapMaritalStatusToValue(.MaritalStatus))
				If Not String.IsNullOrEmpty(SafeString(.MembershipLengthMonths)) Then
					oxmlApplicant.SetAttribute("membership_months", SafeString(.MembershipLengthMonths))
				End If
				oxmlApplicant.SetAttribute("applicant_type", psApplicantType)
				''add interview method(version 5.203)
				''If TypeOf Me Is CHomeEquityLoan Then
				''    oxmlApplicant.SetAttribute("interview_method", "INTERNET")
				''End If

				' Foreign contact, moved to contact node
				'oxmlApplicant.SetAttribute("home_phone", .HomePhone).Replace("-", "").Replace("(", "").Replace(")", "").Replace(".", "").Replace(" ", ""))
				'oxmlApplicant.SetAttribute("is_home_phone_foreign", "Y")
				'oxmlApplicant.SetAttribute("work_phone", .WorkPhone).Replace("-", "").Replace("(", "").Replace(")", "").Replace(".", "").Replace(" ", ""))
				'oxmlApplicant.SetAttribute("is_work_phone_foreign", "Y")
				'oxmlApplicant.SetAttribute("cell_phone", .CellPhone).Replace("-", "").Replace("(", "").Replace(")", "").Replace(".", "").Replace(" ", ""))
				'oxmlApplicant.SetAttribute("is_cell_phone_foreign", "Y")

				'Dim oxmlIDCard As XmlElement = CType(oxmlApplicant.AppendChild(oxmlApplicant.OwnerDocument.CreateElement("ID_CARD", NAMESPACE_URI)), XmlElement)
				'oxmlIDCard.SetAttribute("card_number", .DriversLicenseNumber)
				'oxmlIDCard.SetAttribute("card_type", "DRIVERS_LICENSE")
				'oxmlIDCard.SetAttribute("country", "USA")
				'oxmlIDCard.SetAttribute("state", .DriversLicenseState)
				If Not String.IsNullOrEmpty(.IDNumber) Then
					Dim oxmlIDCard As XmlElement = CType(oxmlApplicant.AppendChild(oxmlApplicant.OwnerDocument.CreateElement("ID_CARD", NAMESPACE_URI)), XmlElement)
					oxmlIDCard.SetAttribute("card_type", .IDType)
					oxmlIDCard.SetAttribute("card_number", .IDNumber)
					oxmlIDCard.SetAttribute("country", .IDCountry)
					oxmlIDCard.SetAttribute("state", .IDState)
					If Not String.IsNullOrEmpty(SafeDateXml(.IDDateIssued)) Then
						oxmlIDCard.SetAttribute("date_issued", SafeDateXml(.IDDateIssued))
					End If
					If Not String.IsNullOrEmpty(SafeDateXml(.IDExpirationDate)) Then
						oxmlIDCard.SetAttribute("exp_date", SafeDateXml(.IDExpirationDate))
					End If
				End If
				''''' get current email
				CurrentEmail = .Email
				If isDisagreeSelect = "Y" Then
					oxmlApplicant.SetAttribute("is_declined", "Y")

					Dim oxmlApprovalDenialReason As XmlElement = CType(oxmlApplicant.AppendChild(oxmlApplicant.OwnerDocument.CreateElement("APPROVAL_DENIAL_REASON", NAMESPACE_URI)), XmlElement)
					oxmlApprovalDenialReason.InnerText = "Applicant Declined to Pull Credit"
				End If

				'' Applicant Questions 
				AddApplicantQuestionsXML(oxmlApplicant, poApplicant)

				Dim oxmlCurrentAddress As XmlElement = CType(oxmlApplicant.AppendChild(oxmlApplicant.OwnerDocument.CreateElement("CURRENT_ADDRESS", NAMESPACE_URI)), XmlElement)
				oxmlCurrentAddress.SetAttribute("occupancy_status", CEnum.MapOccupancyTypeToValue(.OccupancyType))
				oxmlCurrentAddress.SetAttribute("occupancy_duration", .OccupancyDuration)
				If .OccupancyDescription IsNot Nothing AndAlso .OccupancyDescription <> "" Then
					oxmlCurrentAddress.SetAttribute("other_occupancy_description", .OccupancyDescription)
				End If
				''*************
				' Address - use either loose or three line format
				' Foreign address
				'replace . and , if it exist in address and city to prevent errors 
				.CurrentAddress = .CurrentAddress.NullSafeToUpper_.Replace(",", "").Replace(".", "")
				.CurrentAddress2 = .CurrentAddress2.NullSafeToUpper_.Replace(",", "").Replace(".", "")
				.CurrentCity = .CurrentCity.Trim().Replace(",", "").Replace(".", "")

				If .CurrentCountry <> "" And .CurrentCountry <> "USA" Then
					Dim oxmlCurrentThreeLineAddress As XmlElement = CType(oxmlCurrentAddress.AppendChild(oxmlApplicant.OwnerDocument.CreateElement("THREE_LINE_ADDRESS", NAMESPACE_URI)), XmlElement)
					oxmlCurrentThreeLineAddress.SetAttribute("street_address_1", .CurrentAddress.NullSafeToUpper_)
					oxmlCurrentThreeLineAddress.SetAttribute("street_address_2", .CurrentAddress2.NullSafeToUpper_)
					'oxmlCurrentThreeLineAddress.SetAttribute("street_address_2", .CurrentCity & ", " & .CurrentZip)
					'oxmlCurrentThreeLineAddress.SetAttribute("street_address_3", .CurrentCountry)
					oxmlCurrentThreeLineAddress.SetAttribute("city", .CurrentCity.NullSafeToUpper_)
					oxmlCurrentThreeLineAddress.SetAttribute("country", .CurrentCountry.NullSafeToUpper_)
					oxmlCurrentThreeLineAddress.SetAttribute("zip", .CurrentZip)
				Else
					'domestic
					Dim oxmlLooseAddress As XmlElement = CType(oxmlCurrentAddress.AppendChild(oxmlApplicant.OwnerDocument.CreateElement("LOOSE_ADDRESS", NAMESPACE_URI)), XmlElement)
					oxmlLooseAddress.SetAttribute("street_address_1", .CurrentAddress.NullSafeToUpper_)
					oxmlLooseAddress.SetAttribute("city", .CurrentCity.NullSafeToUpper_)
					oxmlLooseAddress.SetAttribute("state", .CurrentState.NullSafeToUpper_)
					oxmlLooseAddress.SetAttribute("zip", .CurrentZip)
				End If

				''Previous Address
				If .HasPreviousAddress = "Y" Then

					.PreviousAddress = .PreviousAddress.Replace(",", "").Replace(".", "")
					.PreviousCity = .PreviousCity.Replace(",", "").Replace(".", "")
					Dim oxmlPreviousAddress As XmlElement = CType(oxmlApplicant.AppendChild(oxmlApplicant.OwnerDocument.CreateElement("PREVIOUS_ADDRESS", NAMESPACE_URI)), XmlElement)
					oxmlPreviousAddress.SetAttribute("street_address_1", .PreviousAddress.NullSafeToUpper_)
					oxmlPreviousAddress.SetAttribute("city", .PreviousCity.NullSafeToUpper_)
					oxmlPreviousAddress.SetAttribute("state", .PreviousState.NullSafeToUpper_)
					oxmlPreviousAddress.SetAttribute("zip", .PreviousZip.NullSafeToUpper_)
					oxmlPreviousAddress.SetAttribute("country", .PreviousCountry.NullSafeToUpper_)
				End If
				''end Previous Address

				''**************
				If Not TypeOf Me Is CHomeEquityLoan Then
					'  Mailing Address
					Dim oxmlMailingAddress As XmlElement = CType(oxmlApplicant.AppendChild(oxmlApplicant.OwnerDocument.CreateElement("MAILING_ADDRESS", NAMESPACE_URI)), XmlElement)
					If .HasMailingAddress = "Y" Then
						oxmlMailingAddress.SetAttribute("street_address_1", .MailingAddress.NullSafeToUpper_)
						oxmlMailingAddress.SetAttribute("street_address_2", .MailingAddress2.NullSafeToUpper_)
						oxmlMailingAddress.SetAttribute("city", .MailingCity.NullSafeToUpper_)
						oxmlMailingAddress.SetAttribute("state", .MailingState.NullSafeToUpper_)
						oxmlMailingAddress.SetAttribute("zip", .MailingZip)
						oxmlMailingAddress.SetAttribute("country", .MailingCountry.NullSafeToUpper_)
						oxmlMailingAddress.SetAttribute("is_current", "N")
					Else
						oxmlMailingAddress.SetAttribute("is_current", "Y")
					End If
				End If

				Dim oxmlFinancialInfo As XmlElement = CType(oxmlApplicant.AppendChild(oxmlApplicant.OwnerDocument.CreateElement("FINANCIAL_INFO", NAMESPACE_URI)), XmlElement)
				Dim oxmlCurrentEmployment As XmlElement = CType(oxmlFinancialInfo.AppendChild(oxmlApplicant.OwnerDocument.CreateElement("CURRENT_EMPLOYMENT", NAMESPACE_URI)), XmlElement)
				If Not String.IsNullOrEmpty(.EmployeeOfLender) And .EmployeeOfLender <> "NONE" Then
					oxmlCurrentEmployment.SetAttribute("employee_of_lender_type", .EmployeeOfLender)
				End If
				' new employment logic
				If .EmploymentStatus.Contains("MILITARY") Then
					oxmlCurrentEmployment.SetAttribute("employer", CEnum.MapBranchOfServiceToValue(.ddlBranchOfService))
				Else
					If .EmploymentStatus = "HOMEMAKER" Or .EmploymentStatus = "STUDENT" Or .EmploymentStatus = "UNEMPLOYED" Or .EmploymentStatus.Contains("RETIRED") Then
						.txtEmployer = "" ''no employer 
					End If
					oxmlCurrentEmployment.SetAttribute("employer", .txtEmployer.NullSafeToUpper_)
				End If
				Dim employed_months As Integer
				employed_months = SafeInteger(.txtEmployedDuration_year) * 12 + SafeInteger(.txtEmployedDuration_month)
				Dim profession_months As Integer
				profession_months = SafeInteger(.txtProfessionDuration_year) * 12 + SafeInteger(.txtProfessionDuration_month)
				''no employment duration for student and homemaker
				If .EmploymentStatus <> "STUDENT" And .EmploymentStatus <> "HOMEMAKER" Then
					oxmlCurrentEmployment.SetAttribute("employed_months", employed_months.ToString())
					If profession_months = 0 Then
						oxmlCurrentEmployment.SetAttribute("profession_months", employed_months.ToString())
					Else
						oxmlCurrentEmployment.SetAttribute("profession_months", profession_months.ToString())
					End If
				End If
				oxmlCurrentEmployment.SetAttribute("occupation", .txtJobTitle.NullSafeToUpper_)
				oxmlCurrentEmployment.SetAttribute("employment_business_type", .txtBusinessType.NullSafeToUpper_)
				oxmlCurrentEmployment.SetAttribute("supervisor_name", .txtSupervisorName.NullSafeToUpper_)
				oxmlCurrentEmployment.SetAttribute("pay_grade", .ddlPayGrade)
				Dim employmentStatus = CEnum.MapEmploymentStatusToValue(.EmploymentStatus)
				'' to add employement_start_date attribute: make sure employment status is Military active 
				If employmentStatus = "MI" Then
					If Not String.IsNullOrEmpty(SafeDateXml(.txtEmploymentStartDate)) Then
						oxmlCurrentEmployment.SetAttribute("employment_start_date", SafeDateXml(.txtEmploymentStartDate))
					End If
				End If
				If Not String.IsNullOrEmpty(.txtETS) AndAlso Not String.IsNullOrEmpty(SafeDateXml(.txtETS)) Then
					oxmlCurrentEmployment.SetAttribute("ets", SafeDateXml(.txtETS))
				End If
				' end new employment logic

				oxmlCurrentEmployment.SetAttribute("employment_status", CEnum.MapEmploymentStatusToValue(.EmploymentStatus))
				oxmlCurrentEmployment.SetAttribute("other_employment_description", .EmploymentDescription.NullSafeToUpper_)
				''previous employment information 
				If .hasPrevEmployment = "Y" Then
					Dim oxmlPreviousEmployment As XmlElement = CType(oxmlFinancialInfo.AppendChild(oxmlApplicant.OwnerDocument.CreateElement("PREVIOUS_EMPLOYMENT", NAMESPACE_URI)), XmlElement)
					If .prev_EmploymentStatus.Contains("MILITARY") Then
						oxmlPreviousEmployment.SetAttribute("employer", CEnum.MapBranchOfServiceToValue(.prev_ddlBranchOfService))
					Else
						If .prev_EmploymentStatus = "HOMEMAKER" Or .prev_EmploymentStatus = "STUDENT" Or .prev_EmploymentStatus = "UNEMPLOYED" Or .prev_EmploymentStatus.Contains("RETIRED") Then
							.prev_txtEmployer = "" ''no employer 
						End If
						oxmlPreviousEmployment.SetAttribute("employer", .prev_txtEmployer.NullSafeToUpper_)
					End If
					Dim pre_employed_month As Integer = SafeInteger(.prev_txtEmployedDuration_year) * 12 + SafeInteger(.prev_txtEmployedDuration_month)

					If .prev_EmploymentStatus = "STUDENT" Or .prev_EmploymentStatus = "HOMEMAKER" Then
						pre_employed_month = 0
					End If
					oxmlPreviousEmployment.SetAttribute("employed_months", pre_employed_month.ToString())
					oxmlPreviousEmployment.SetAttribute("occupation", .prev_txtJobTitle.NullSafeToUpper_)
					oxmlPreviousEmployment.SetAttribute("employment_business_type", .prev_txtBusinessType.NullSafeToUpper_)
					oxmlPreviousEmployment.SetAttribute("pay_grade", .prev_ddlPayGrade)
					oxmlPreviousEmployment.SetAttribute("monthly_income", .prev_GrossMonthlyIncome)
					Dim prev_employmentStatus = CEnum.MapEmploymentStatusToValue(.prev_EmploymentStatus)
					If prev_employmentStatus = "MI" Then
						If Not String.IsNullOrEmpty(SafeDateXml(.prev_txtEmploymentStartDate)) Then
							oxmlPreviousEmployment.SetAttribute("employment_start_date", SafeDateXml(.prev_txtEmploymentStartDate))
						End If
					End If
					If Not String.IsNullOrEmpty(.prev_txtETS) AndAlso Not String.IsNullOrEmpty(SafeDateXml(.prev_txtETS)) Then
						oxmlPreviousEmployment.SetAttribute("ets", SafeDateXml(.prev_txtETS))
					End If
					oxmlPreviousEmployment.SetAttribute("employment_status", CEnum.MapEmploymentStatusToValue(.prev_EmploymentStatus))
				End If
				''end previous employment information

				Dim oxmlMonthlyIncome As XmlElement = CType(oxmlFinancialInfo.AppendChild(oxmlApplicant.OwnerDocument.CreateElement("MONTHLY_INCOME", NAMESPACE_URI)), XmlElement)
				If Common.SafeString(.GrossMonthlyIncome) <> "" Then
					oxmlMonthlyIncome.SetAttribute("monthly_income_base_salary", .GrossMonthlyIncome)
				End If

				'respect inbranch auto calculated military income
				If employmentStatus = "MI" Then
					oxmlMonthlyIncome.SetAttribute("is_monthly_income_base_salary_manual", "N")
				End If

				' oxmlMonthlyIncome.SetAttribute("monthly_income_other_1", .GrossMonthlyIncomeOther)
				'oxmlMonthlyIncome.SetAttribute("monthly_income_other_description_1", .MonthlyIncomeOtherDescription.NullSafeToUpper_)
				oxmlMonthlyIncome.SetAttribute("is_tax_exempt_monthly_income_base_salary", .GrossMonthlyIncomeTaxExempt)
				' oxmlMonthlyIncome.SetAttribute("is_tax_exempt_income_other_1", .OtherMonthlyIncomeTaxExempt)
				''the old code for clf version < 5.188
				If .MonthlyIncomeOtherDescription.NullSafeToUpper_ <> "" OrElse Common.SafeDouble(.GrossMonthlyIncomeOther) > 0 Then
					oxmlMonthlyIncome.SetAttribute("monthly_income_other_1", .GrossMonthlyIncomeOther)
					oxmlMonthlyIncome.SetAttribute("monthly_income_other_description_1", .MonthlyIncomeOtherDescription.NullSafeToUpper_)
					oxmlMonthlyIncome.SetAttribute("is_tax_exempt_income_other_1", .OtherMonthlyIncomeTaxExempt)
				End If

				''this is for  new clf version, we have to go back to use the old code for clf 5.181
				''If .MonthlyIncomeOtherDescription.NullSafeToUpper_ <> "" OrElse Common.SafeDouble(.GrossMonthlyIncomeOther) > 0 Then
				''    Dim oxmlOtherMonthlyIncome As XmlElement = CType(oxmlMonthlyIncome.AppendChild(oxmlApplicant.OwnerDocument.CreateElement("OTHER_INCOME", NAMESPACE_URI)), XmlElement)
				''    oxmlOtherMonthlyIncome.SetAttribute("monthly_income", .GrossMonthlyIncomeOther)
				''    oxmlOtherMonthlyIncome.SetAttribute("monthly_income_description", .MonthlyIncomeOtherDescription.NullSafeToUpper_)
				''    oxmlOtherMonthlyIncome.SetAttribute("is_tax_exempt", .OtherMonthlyIncomeTaxExempt)
				''End If

				Dim oxmlMonthlyDebt As XmlElement = CType(oxmlFinancialInfo.AppendChild(oxmlApplicant.OwnerDocument.CreateElement("MONTHLY_DEBT", NAMESPACE_URI)), XmlElement)

				If CEnum.MapOccupancyTypeToValue(.OccupancyType) = "BUYING" Or CEnum.MapOccupancyTypeToValue(.OccupancyType) = "OWN" Then
					oxmlMonthlyDebt.SetAttribute("monthly_mortgage_payment", .TotalMonthlyHousingExpense)
				Else
					oxmlMonthlyDebt.SetAttribute("monthly_housing_cost", .TotalMonthlyHousingExpense)
				End If
				''reference information
				If .referenceInfor IsNot Nothing Then
					Dim oxmlReferenceInfo As XmlElement = CType(oxmlApplicant.AppendChild(oxmlApplicant.OwnerDocument.CreateElement("REFERENCE", NAMESPACE_URI)), XmlElement)
					If .referenceInfor.Count > 0 Then
						For Each oItem In .referenceInfor
							Dim sName = oItem.Split("|"c)(0)
							Dim sValue = oItem.Split("|"c)(1)
							If Not String.IsNullOrEmpty(sValue) Then
								If sName = "ref_first_name" Then
									oxmlReferenceInfo.SetAttribute("first_name", sValue)
								ElseIf sName = "ref_last_name" Then
									oxmlReferenceInfo.SetAttribute("last_name", sValue)
								ElseIf sName = "ref_phone" Then
									oxmlReferenceInfo.SetAttribute("phone", sValue)
								ElseIf sName = "ref_email" Then
									oxmlReferenceInfo.SetAttribute("email", sValue)
								ElseIf sName = "ref_relationship" Then
									oxmlReferenceInfo.SetAttribute("relationship", sValue)
								End If
							End If
						Next
					End If
				End If ''end reference information

				Dim oxmlContactInfo As XmlElement = CType(oxmlApplicant.AppendChild(oxmlApplicant.OwnerDocument.CreateElement("CONTACT_INFO", NAMESPACE_URI)), XmlElement)

				oxmlContactInfo.SetAttribute("home_phone", .HomePhone)
				If .HomePhoneCountry <> "" And .HomePhoneCountry.NullSafeToUpper_ <> "US" Then
					oxmlContactInfo.SetAttribute("home_phone_country", .HomePhoneCountry.NullSafeToUpper_)
					'oxmlContactInfo.SetAttribute("is_home_phone_foreign", "Y")
				End If

				oxmlContactInfo.SetAttribute("work_phone", .WorkPhone)
				If .WorkPhoneCountry <> "" And .WorkPhoneCountry.NullSafeToUpper_ <> "US" Then
					oxmlContactInfo.SetAttribute("work_phone_country", .WorkPhoneCountry.NullSafeToUpper_)
					'oxmlContactInfo.SetAttribute("is_work_phone_foreign", "Y")
				End If
				oxmlContactInfo.SetAttribute("work_phone_extension", .WorkPhoneEXT)

				oxmlContactInfo.SetAttribute("cell_phone", .CellPhone)
				If .CellPhoneCountry <> "" And .CellPhoneCountry.NullSafeToUpper_ <> "US" Then
					oxmlContactInfo.SetAttribute("cell_phone_country", .CellPhoneCountry.NullSafeToUpper_)
					'oxmlContactInfo.SetAttribute("is_cell_phone_foreign", "Y")
				End If
				oxmlContactInfo.SetAttribute("email", .Email.NullSafeToUpper_)
				If .PreferredContactMethod <> "" Then oxmlContactInfo.SetAttribute("preferred_contact_method", .PreferredContactMethod)

				AddAssetsXml(oxmlApplicant, .Assets)

				If (poApplicant.Declarations IsNot Nothing AndAlso poApplicant.Declarations.Count > 0) Then
					Dim sDeclarationNode = "DECLARATIONS" ''for CC, PL, and VL
					If TypeOf Me Is CHomeEquityLoan Then
						sDeclarationNode = "DECLARATION" ''only for HE
					End If
					Dim oxmlMortgageDeclaration As XmlElement = CType(oxmlApplicant.AppendChild(oxmlApplicant.OwnerDocument.CreateElement(sDeclarationNode, NAMESPACE_URI)), XmlElement)
					For Each item In poApplicant.Declarations
						oxmlMortgageDeclaration.SetAttribute(item.Key, IIf(item.Value, "Y", "N").ToString())
						If item.Value AndAlso poApplicant.AdditionalDeclarations IsNot Nothing AndAlso poApplicant.AdditionalDeclarations.Count > 0 Then
							If item.Key = "has_co_maker" Then
								If poApplicant.AdditionalDeclarations.ContainsKey("co_maker_primary_name") Then
									oxmlMortgageDeclaration.SetAttribute("co_maker_primary_name", SafeStripHtmlString(poApplicant.AdditionalDeclarations("co_maker_primary_name")))
								End If
								If poApplicant.AdditionalDeclarations.ContainsKey("co_maker_creditor") Then
									oxmlMortgageDeclaration.SetAttribute("co_maker_creditor", SafeStripHtmlString(poApplicant.AdditionalDeclarations("co_maker_creditor")))
								End If
								If poApplicant.AdditionalDeclarations.ContainsKey("co_maker_amount") Then
									''remove "$" or "," if it exist and then convert to double
									Dim coMakerAmount = SafeDouble(SafeStripHtmlString(poApplicant.AdditionalDeclarations("co_maker_amount")).Replace("$", "").Replace(",", ""))
									If coMakerAmount > 0 Then
										oxmlMortgageDeclaration.SetAttribute("co_maker_amount", coMakerAmount)
									End If
								End If
							ElseIf item.Key = "has_alias" Then
								If poApplicant.AdditionalDeclarations.ContainsKey("alias") Then
									oxmlMortgageDeclaration.SetAttribute("alias", SafeStripHtmlString(poApplicant.AdditionalDeclarations("alias")))
								End If
							ElseIf item.Key = "has_alimony" Then
								If poApplicant.AdditionalDeclarations.ContainsKey("alimony_recipient") Then
									oxmlMortgageDeclaration.SetAttribute("alimony_recipient", SafeStripHtmlString(poApplicant.AdditionalDeclarations("alimony_recipient")))
								End If
								If poApplicant.AdditionalDeclarations.ContainsKey("alimony_recipient_address") Then
									oxmlMortgageDeclaration.SetAttribute("alimony_recipient_address", SafeStripHtmlString(poApplicant.AdditionalDeclarations("alimony_recipient_address")))
								End If
								If poApplicant.AdditionalDeclarations.ContainsKey("alimony") Then
									''remove "$" or "," if it exist and then convert to double
									Dim alimonyAmount = SafeDouble(SafeStripHtmlString(poApplicant.AdditionalDeclarations("alimony")).Replace("$", "").Replace(",", ""))
									If alimonyAmount > 0 Then
										oxmlMonthlyDebt.SetAttribute("alimony", alimonyAmount)
									End If
								End If
								If poApplicant.AdditionalDeclarations.ContainsKey("separate_maintenance_expense") Then
									''remove "$" or "," if it exist and then convert to double
									Dim seperateExpenseAmount = SafeDouble(SafeStripHtmlString(poApplicant.AdditionalDeclarations("separate_maintenance_expense")).Replace("$", "").Replace(",", ""))
									If seperateExpenseAmount > 0 Then
										oxmlMonthlyDebt.SetAttribute("separate_maintenance_expense", seperateExpenseAmount)
									End If
								End If
								If poApplicant.AdditionalDeclarations.ContainsKey("child_support") Then
									''remove "$" or "," if it exist and then convert to double
									Dim childSupportAmount = SafeDouble(SafeStripHtmlString(poApplicant.AdditionalDeclarations("child_support")).Replace("$", "").Replace(",", ""))
									If childSupportAmount > 0 Then
										oxmlMonthlyDebt.SetAttribute("child_support", childSupportAmount)
									End If
								End If
							End If
						End If
					Next
				End If

				If TypeOf Me Is CHomeEquityLoan Then
					If poApplicant.Declarations Is Nothing OrElse poApplicant.Declarations.Count = 0 Then  ''prevent xml validation error, add empty DECLARATION node for home equity loan
						oxmlApplicant.AppendChild(oxmlApplicant.OwnerDocument.CreateElement("DECLARATION", NAMESPACE_URI))
					End If
					'' "MAILING_ADDRESS" NODE is after "DECLARATION" NODE
					'  Mailing Address
					Dim oxmlMailingAddress As XmlElement = CType(oxmlApplicant.AppendChild(oxmlApplicant.OwnerDocument.CreateElement("MAILING_ADDRESS", NAMESPACE_URI)), XmlElement)
					If .HasMailingAddress = "Y" Then
						oxmlMailingAddress.SetAttribute("street_address_1", .MailingAddress.NullSafeToUpper_)
						oxmlMailingAddress.SetAttribute("street_address_2", .MailingAddress2.NullSafeToUpper_)
						oxmlMailingAddress.SetAttribute("city", .MailingCity.NullSafeToUpper_)
						oxmlMailingAddress.SetAttribute("state", .MailingState.NullSafeToUpper_)
						oxmlMailingAddress.SetAttribute("zip", .MailingZip)
						oxmlMailingAddress.SetAttribute("country", .MailingCountry.NullSafeToUpper_)
						oxmlMailingAddress.SetAttribute("is_current", "N")
					Else
						oxmlMailingAddress.SetAttribute("is_current", "Y")
					End If
				End If
			End With
			Return oxmlApplicant

		End Function
		Protected Function AddXAApplicantXml(ByVal poParent As XmlElement, ByVal poApplicant As CApplicant, ByVal psApplicantType As String, ByVal psApplicantNodeName As String, oConfig As CWebsiteConfig) As XmlElement
			Dim oxmlApplicant As XmlElement = CType(poParent.AppendChild(poParent.OwnerDocument.CreateElement(psApplicantNodeName, NAMESPACE_URI)), XmlElement)

			With poApplicant

				oxmlApplicant.SetAttribute("first_name", .FirstName.NullSafeToUpper_)
				oxmlApplicant.SetAttribute("last_name", .LastName.NullSafeToUpper_)
				oxmlApplicant.SetAttribute("suffix", .Suffix.NullSafeToUpper_)
				oxmlApplicant.SetAttribute("middle_name", .MiddleInitial.NullSafeToUpper_)
				oxmlApplicant.SetAttribute("ssn", .SSN)
				If .SSN.StartsWith("9") Then
					oxmlApplicant.SetAttribute("is_ssn_tax_id", "Y")
				End If
				Dim sDOB As String = SafeDateXml(.DOB)
				If sDOB <> "" Then
					oxmlApplicant.SetAttribute("dob", sDOB)	'1967-08-dd
				End If
				oxmlApplicant.SetAttribute("mother_maiden_name", .MotherMaidenName)	'TODO:
				oxmlApplicant.SetAttribute("citizenship", CEnum.MapCitizenshipStatusToValue(.CitizenshipStatus))
				oxmlApplicant.SetAttribute("member_number", .MemberNumber)
				If Not String.IsNullOrEmpty(SafeString(.RelationshipToPrimary)) Then
					oxmlApplicant.SetAttribute("relation_to_primary", SafeString(.RelationshipToPrimary))
				End If
				oxmlApplicant.SetAttribute("marital_status", CEnum.MapMaritalStatusToValue(.MaritalStatus))
				If Not String.IsNullOrEmpty(SafeString(.MembershipLengthMonths)) Then
					oxmlApplicant.SetAttribute("membership_months", SafeString(.MembershipLengthMonths))
				End If

				'oxmlApplicant.SetAttribute("applicant_type", psApplicantType)  '?

				oxmlApplicant.SetAttribute("home_phone", .HomePhone)
				If .HomePhoneCountry <> "" And .HomePhoneCountry.NullSafeToUpper_ <> "US" Then
					oxmlApplicant.SetAttribute("home_phone_country", .HomePhoneCountry.NullSafeToUpper_)
					'oxmlApplicant.SetAttribute("is_home_phone_foreign", "Y")
				End If

				oxmlApplicant.SetAttribute("work_phone", .WorkPhone)
				If .WorkPhoneCountry <> "" And .WorkPhoneCountry.NullSafeToUpper_ <> "US" Then
					oxmlApplicant.SetAttribute("work_phone_country", .WorkPhoneCountry.NullSafeToUpper_)
					'oxmlApplicant.SetAttribute("is_work_phone_foreign", "Y")
				End If
				oxmlApplicant.SetAttribute("work_phone_extension", .WorkPhoneEXT)

				oxmlApplicant.SetAttribute("cell_phone", .CellPhone)
				If .CellPhoneCountry <> "" And .CellPhoneCountry.NullSafeToUpper_ <> "US" Then
					oxmlApplicant.SetAttribute("cell_phone_country", .CellPhoneCountry.NullSafeToUpper_)
					'oxmlApplicant.SetAttribute("is_cell_phone_foreign", "Y")
				End If
				oxmlApplicant.SetAttribute("email", .Email)
				If .PreferredContactMethod <> "" Then oxmlApplicant.SetAttribute("preferred_contact_method", .PreferredContactMethod)

				Dim oxmlCurrentAddress As XmlElement = CType(oxmlApplicant.AppendChild(oxmlApplicant.OwnerDocument.CreateElement("CURRENT_ADDRESS", NAMESPACE_URI)), XmlElement)
				oxmlCurrentAddress.SetAttribute("occupancy_status", CEnum.MapOccupancyTypeToValue(.OccupancyType))
				oxmlCurrentAddress.SetAttribute("occupancy_duration", .OccupancyDuration)
				If .OccupancyDescription IsNot Nothing AndAlso .OccupancyDescription <> "" Then
					oxmlCurrentAddress.SetAttribute("other_occupancy_description", .OccupancyDescription)
				End If

				''*************
				' Address - use either loose or three line format
				' Foreign address
				'replace . and , if it exist in address and city to prevent errors 
				.CurrentAddress = .CurrentAddress.NullSafeToUpper_.Replace(",", "").Replace(".", "")
				.CurrentAddress2 = .CurrentAddress2.NullSafeToUpper_.Replace(",", "").Replace(".", "")
				.CurrentCity = .CurrentCity.NullSafeToUpper_.Replace(",", "").Replace(".", "")

				If .CurrentCountry <> "" And .CurrentCountry <> "USA" Then
					Dim oxmlCurrentThreeLineAddress As XmlElement = CType(oxmlCurrentAddress.AppendChild(oxmlApplicant.OwnerDocument.CreateElement("THREE_LINE_ADDRESS", NAMESPACE_URI)), XmlElement)
					oxmlCurrentThreeLineAddress.SetAttribute("street_address_1", .CurrentAddress.NullSafeToUpper_)
					oxmlCurrentThreeLineAddress.SetAttribute("street_address_2", .CurrentAddress2.NullSafeToUpper_)
					'oxmlCurrentThreeLineAddress.SetAttribute("street_address_2", .CurrentCity & ", " & .CurrentZip)
					'oxmlCurrentThreeLineAddress.SetAttribute("street_address_3", .CurrentCountry)
					oxmlCurrentThreeLineAddress.SetAttribute("city", .CurrentCity.NullSafeToUpper_)
					oxmlCurrentThreeLineAddress.SetAttribute("country", .CurrentCountry.NullSafeToUpper_)
					oxmlCurrentThreeLineAddress.SetAttribute("zip", .CurrentZip)
				Else
					'domestic
					Dim oxmlLooseAddress As XmlElement = CType(oxmlCurrentAddress.AppendChild(oxmlApplicant.OwnerDocument.CreateElement("LOOSE_ADDRESS", NAMESPACE_URI)), XmlElement)
					oxmlLooseAddress.SetAttribute("street_address_1", .CurrentAddress.NullSafeToUpper_)
					oxmlLooseAddress.SetAttribute("city", .CurrentCity.NullSafeToUpper_)
					oxmlLooseAddress.SetAttribute("state", .CurrentState.NullSafeToUpper_)
					oxmlLooseAddress.SetAttribute("zip", .CurrentZip)
				End If

				''Previous Address
				If .HasPreviousAddress = "Y" Then
					'replace . and , if it exist in address and city to prevent errors 
					.PreviousAddress = .PreviousAddress.Replace(",", "").Replace(".", "")
					.PreviousCity = .PreviousCity.Replace(",", "").Replace(".", "")

					Dim oxmlPreviousAddress As XmlElement = CType(oxmlApplicant.AppendChild(oxmlApplicant.OwnerDocument.CreateElement("PREVIOUS_ADDRESS", NAMESPACE_URI)), XmlElement)
					oxmlPreviousAddress.SetAttribute("street_address_1", .PreviousAddress.NullSafeToUpper_)
					oxmlPreviousAddress.SetAttribute("city", .PreviousCity.NullSafeToUpper_)
					oxmlPreviousAddress.SetAttribute("state", .PreviousState.NullSafeToUpper_)
					oxmlPreviousAddress.SetAttribute("zip", .PreviousZip)
				End If
				''end Previous Address

				''**************
				'  Mailing Address
				Dim oxmlMailingAddress As XmlElement = CType(oxmlApplicant.AppendChild(oxmlApplicant.OwnerDocument.CreateElement("MAILING_ADDRESS", NAMESPACE_URI)), XmlElement)
				If .HasMailingAddress = "Y" Then
					'replace . and , if it exist in address and city to prevent errors 
					.MailingAddress = .MailingAddress.Replace(",", "").Replace(".", "")
					.MailingCity = .MailingCity.Replace(",", "").Replace(".", "")

					oxmlMailingAddress.SetAttribute("street_address_1", .MailingAddress.NullSafeToUpper_)
					oxmlMailingAddress.SetAttribute("street_address_2", .MailingAddress2.NullSafeToUpper_)
					oxmlMailingAddress.SetAttribute("city", .MailingCity.NullSafeToUpper_)
					oxmlMailingAddress.SetAttribute("state", .MailingState.NullSafeToUpper_)
					oxmlMailingAddress.SetAttribute("zip", .MailingZip)
					oxmlMailingAddress.SetAttribute("country", .MailingCountry.NullSafeToUpper_)
					oxmlMailingAddress.SetAttribute("is_current", "N")
				Else
					oxmlMailingAddress.SetAttribute("is_current", "Y")
				End If



				' ''''' get current email
				'CurrentEmail = .Email
				'If isDisagreeSelect = "Y" Then
				'	oxmlApplicant.SetAttribute("is_declined", "Y")

				'	Dim oxmlApprovalDenialReason As XmlElement = CType(oxmlApplicant.AppendChild(oxmlApplicant.OwnerDocument.CreateElement("APPROVAL_DENIAL_REASON", NAMESPACE_URI)), XmlElement)
				'	oxmlApprovalDenialReason.InnerText = "Applicant Declined to Pull Credit"
				'End If

				Dim oxmlFinancialInfo As XmlElement = CType(oxmlApplicant.AppendChild(oxmlApplicant.OwnerDocument.CreateElement("FINANCIAL_INFO", NAMESPACE_URI)), XmlElement)
				If Not String.IsNullOrEmpty(.EmployeeOfLender) And .EmployeeOfLender <> "NONE" Then
					oxmlFinancialInfo.SetAttribute("employee_of_lender_type", .EmployeeOfLender)
				End If
				' new employment logic
				If .EmploymentStatus.Contains("MILITARY") Then
					oxmlFinancialInfo.SetAttribute("employer", CEnum.MapBranchOfServiceToValue(.ddlBranchOfService))
				Else
					If .EmploymentStatus = "HOMEMAKER" Or .EmploymentStatus = "STUDENT" Or .EmploymentStatus = "UNEMPLOYED" Or .EmploymentStatus.Contains("RETIRED") Then
						.txtEmployer = "" ''no employer 
					End If
					oxmlFinancialInfo.SetAttribute("employer", .txtEmployer)
				End If
				Dim employed_months As Integer
				employed_months = SafeInteger(.txtEmployedDuration_year) * 12 + SafeInteger(.txtEmployedDuration_month)
				Dim profession_months As Integer
				profession_months = SafeInteger(.txtProfessionDuration_year) * 12 + SafeInteger(.txtProfessionDuration_month)
				''no employment duration for student and homemaker
				If .EmploymentStatus <> "STUDENT" And .EmploymentStatus <> "HOMEMAKER" Then
					oxmlFinancialInfo.SetAttribute("employed_months", employed_months.ToString())
					If profession_months = 0 Then
						oxmlFinancialInfo.SetAttribute("profession_months", employed_months.ToString())
					Else
						oxmlFinancialInfo.SetAttribute("profession_months", profession_months.ToString())
					End If
				End If

				oxmlFinancialInfo.SetAttribute("occupation", .txtJobTitle)
				oxmlFinancialInfo.SetAttribute("employment_business_type", .txtBusinessType.NullSafeToUpper_)
				oxmlFinancialInfo.SetAttribute("supervisor_name", .txtSupervisorName.NullSafeToUpper_)
				oxmlFinancialInfo.SetAttribute("pay_grade", .ddlPayGrade)
				Dim employmentStatus = CEnum.MapEmploymentStatusToValue(.EmploymentStatus)
				'' to add employement_start_date attribute: make sure employment status is Military active 
				If employmentStatus = "MI" Then
					If Not String.IsNullOrEmpty(SafeDateXml(.txtEmploymentStartDate)) Then
						oxmlFinancialInfo.SetAttribute("employment_start_date", SafeDateXml(.txtEmploymentStartDate))
					End If
				End If
				If Not String.IsNullOrEmpty(.txtETS) AndAlso Not String.IsNullOrEmpty(SafeDateXml(.txtETS)) Then
					oxmlFinancialInfo.SetAttribute("ets", SafeDateXml(.txtETS))
				End If
				oxmlFinancialInfo.SetAttribute("employment_status", CEnum.MapEmploymentStatusToValue(.EmploymentStatus))
				oxmlFinancialInfo.SetAttribute("other_employment_description", .EmploymentDescription.NullSafeToUpper_)
				If Common.SafeString(.GrossMonthlyIncome) <> "" Then
					oxmlFinancialInfo.SetAttribute("monthly_income_base_salary", .GrossMonthlyIncome)
				End If
				oxmlFinancialInfo.SetAttribute("monthly_rent", .TotalMonthlyHousingExpense)
				' end new employment logic


				''previous employment information 
				'If .hasPrevEmployment = "Y" Then
				'	Dim oxmlPreviousEmployment As XmlElement = CType(oxmlFinancialInfo.AppendChild(oxmlApplicant.OwnerDocument.CreateElement("PREVIOUS_EMPLOYMENT", NAMESPACE_URI)), XmlElement)
				'	If .prev_EmploymentStatus.Contains("MILITARY") Then
				'		oxmlPreviousEmployment.SetAttribute("employer", CEnum.MapBranchOfServiceToValue(.prev_ddlBranchOfService))
				'	Else
				'		oxmlPreviousEmployment.SetAttribute("employer", .prev_txtEmployer)
				'	End If
				'	Dim pre_employed_month As Integer = SafeInteger(.prev_txtEmployedDuration_year) * 12 + SafeInteger(.prev_txtEmployedDuration_month)
				'	oxmlPreviousEmployment.SetAttribute("employed_months", pre_employed_month)
				'	oxmlPreviousEmployment.SetAttribute("occupation", .prev_txtJobTitle)
				'	oxmlPreviousEmployment.SetAttribute("employment_business_type", .prev_txtBusinessType)
				'	oxmlPreviousEmployment.SetAttribute("pay_grade", .prev_ddlPayGrade)
				'	oxmlPreviousEmployment.SetAttribute("monthly_income", .prev_GrossMonthlyIncome)
				'	Dim prev_employmentStatus = CEnum.MapEmploymentStatusToValue(.prev_EmploymentStatus)
				'	If prev_employmentStatus = "MI" Then
				'		If Not String.IsNullOrEmpty(SafeDateXml(.prev_txtEmploymentStartDate)) Then
				'			oxmlPreviousEmployment.SetAttribute("employment_start_date", SafeDateXml(.prev_txtEmploymentStartDate))
				'		End If
				'	End If
				'	If Not String.IsNullOrEmpty(.prev_txtETS) Then
				'		oxmlPreviousEmployment.SetAttribute("ets", SafeDateXml(.prev_txtETS))
				'	End If
				'	oxmlPreviousEmployment.SetAttribute("employment_status", CEnum.MapEmploymentStatusToValue(.prev_EmploymentStatus))
				'End If
				''end previous employment information

				'Dim oxmlMonthlyIncome As XmlElement = CType(oxmlFinancialInfo.AppendChild(oxmlApplicant.OwnerDocument.CreateElement("MONTHLY_INCOME", NAMESPACE_URI)), XmlElement)
				'oxmlMonthlyIncome.SetAttribute("monthly_income_base_salary", .GrossMonthlyIncome)
				'oxmlMonthlyIncome.SetAttribute("monthly_income_other_1", .GrossMonthlyIncomeOther)
				'oxmlMonthlyIncome.SetAttribute("monthly_income_other_description_1", .MonthlyIncomeOtherDescription)


				'Dim oxmlMonthlyDebt As XmlElement = CType(oxmlFinancialInfo.AppendChild(oxmlApplicant.OwnerDocument.CreateElement("MONTHLY_DEBT", NAMESPACE_URI)), XmlElement)

				'If CEnum.MapOccupancyTypeToValue(.OccupancyType) = "BUYING" Or CEnum.MapOccupancyTypeToValue(.OccupancyType) = "OWN" Then
				'	oxmlMonthlyDebt.SetAttribute("monthly_mortgage_payment", .TotalMonthlyHousingExpense)
				'Else
				'	oxmlMonthlyDebt.SetAttribute("monthly_housing_cost", .TotalMonthlyHousingExpense)
				'End If

				'If Not String.IsNullOrEmpty(.DLNumber) Then
				Dim oxmlIDCard As XmlElement = CType(oxmlApplicant.AppendChild(oxmlApplicant.OwnerDocument.CreateElement("ID_CARD", NAMESPACE_URI)), XmlElement)
				oxmlIDCard.SetAttribute("card_type", .IDType)
				oxmlIDCard.SetAttribute("card_number", .IDNumber)
				oxmlIDCard.SetAttribute("country", .IDCountry)
				oxmlIDCard.SetAttribute("state", .IDState)
				If Not String.IsNullOrEmpty(SafeDateXml(.IDDateIssued)) Then
					oxmlIDCard.SetAttribute("date_issued", SafeDateXml(.IDDateIssued))
				End If
				If Not String.IsNullOrEmpty(SafeDateXml(.IDExpirationDate)) Then
					oxmlIDCard.SetAttribute("exp_date", SafeDateXml(.IDExpirationDate))
				End If

				''**************************************
				'' Applicant Question mapping for xa in combo mode
				If .ApplicantQAList.Count > 0 Then	 ''make sure applicant question answer is not empty

					Dim oxmlApplicantQuestions As XmlElement = CType(oxmlApplicant.AppendChild(oxmlApplicant.OwnerDocument.CreateElement("APPLICANT_QUESTIONS", NAMESPACE_URI)), XmlElement)
					For Each oQuestionAnswer As CValidatedQuestionAnswers In .ApplicantQAList
						If Not oQuestionAnswer.Answers.Any() Then Continue For

						Dim oxmlApplicantQuestion As XmlElement = CType(oxmlApplicantQuestions.AppendChild(oxmlApplicantQuestions.OwnerDocument.CreateElement("APPLICANT_QUESTION", NAMESPACE_URI)), XmlElement)
						oxmlApplicantQuestion.SetAttribute("question_name", oQuestionAnswer.Question.Name)
						oxmlApplicantQuestion.SetAttribute("question_type", oQuestionAnswer.Question.AnswerType)
						For Each oAnswerItem In oQuestionAnswer.Answers
							Dim oxmlQuestionAnswer As XmlElement = CType(oxmlApplicantQuestion.AppendChild(oxmlApplicant.OwnerDocument.CreateElement("APPLICANT_QUESTION_ANSWER", NAMESPACE_URI)), XmlElement)
							oxmlQuestionAnswer.SetAttribute("answer_text", oAnswerItem.AnswerText)
							oxmlQuestionAnswer.SetAttribute("answer_value", oAnswerItem.AnswerValue)
						Next
					Next
				End If

			End With

			Return oxmlApplicant
		End Function

		Protected Sub AddContactsXml(ByVal poDoc As XmlDocument)
			If AppNSS Is Nothing AndAlso CoAppNSS Is Nothing Then Exit Sub
			Dim oxmlContacts As XmlElement = CType(poDoc.DocumentElement.AppendChild(poDoc.CreateElement("CONTACTS", NAMESPACE_URI)), XmlElement)
			If AppNSS IsNot Nothing Then
				AddContactInfoXml(oxmlContacts, AppNSS)
			End If
			If CoAppNSS IsNot Nothing Then
				AddContactInfoXml(oxmlContacts, CoAppNSS)
			End If
		End Sub
		Private Sub AddContactInfoXml(poParent As XmlElement, nssInfo As CNSSInfo)
			Dim oxmlContactInfo As XmlElement = CType(poParent.AppendChild(poParent.OwnerDocument.CreateElement("CONTACT_INFO", NAMESPACE_URI)), XmlElement)
			With nssInfo
				oxmlContactInfo.SetAttribute("contact_type", "NSS")
				oxmlContactInfo.SetAttribute("first_name", .FirstName.NullSafeToUpper_)
				oxmlContactInfo.SetAttribute("last_name", .LastName.NullSafeToUpper_)
				oxmlContactInfo.SetAttribute("suffix", .Suffix.NullSafeToUpper_)
				oxmlContactInfo.SetAttribute("middle_name", .MiddleName.NullSafeToUpper_)
				oxmlContactInfo.SetAttribute("ssn", .SSN)
				'oxmlContactInfo.SetAttribute("is_ssn_tax_id", IIf(.SSN.StartsWith("9"), "Y", "N"))
				If Not String.IsNullOrEmpty(.Email) Then
					oxmlContactInfo.SetAttribute("email", .Email)
				End If
				If Not String.IsNullOrEmpty(.CellPhone) Then
					oxmlContactInfo.SetAttribute("cell", .CellPhone)
				End If

				'If .CellPhoneCountry <> "" And .CellPhoneCountry.NullSafeToUpper_ <> "US" Then
				'    oxmlContactInfo.SetAttribute("cell_phone_country", .CellPhoneCountry.NullSafeToUpper_)
				'End If
				If Not String.IsNullOrEmpty(.HomePhone) Then
					oxmlContactInfo.SetAttribute("phone", .HomePhone)
				End If
				If .HomePhoneCountry <> "" And .HomePhoneCountry.NullSafeToUpper_ <> "US" Then
					oxmlContactInfo.SetAttribute("country", .HomePhoneCountry.NullSafeToUpper_)
				End If
				'replace . and , if it exist in address and city to prevent errors 
				If .Country <> "" AndAlso .Country <> "USA" Then
					oxmlContactInfo.SetAttribute("address", .Address.NullSafeToUpper_.Replace(",", "").Replace(".", ""))
					oxmlContactInfo.SetAttribute("address_2", .Address2.NullSafeToUpper_.Replace(",", "").Replace(".", ""))
					oxmlContactInfo.SetAttribute("city", .City.NullSafeToUpper_.Replace(",", "").Replace(".", ""))
					oxmlContactInfo.SetAttribute("country", .Country.NullSafeToUpper_)
					oxmlContactInfo.SetAttribute("zip", .Zip)
				Else
					'domestic
					oxmlContactInfo.SetAttribute("address", .Address.NullSafeToUpper_.Replace(",", "").Replace(".", ""))
					oxmlContactInfo.SetAttribute("city", .City.NullSafeToUpper_.Replace(",", "").Replace(".", ""))
					oxmlContactInfo.SetAttribute("state", .State.NullSafeToUpper_)
					oxmlContactInfo.SetAttribute("zip", .Zip)
				End If
				oxmlContactInfo.SetAttribute("notes", "This person is married to " & .MarriedTo)
			End With
		End Sub

		Protected Sub AddAssetsXml(ByVal applicantDoc As XmlElement, ByVal AssetInfoList As List(Of CAssetInfo))
			If (AssetInfoList Is Nothing OrElse AssetInfoList.Count = 0) Then Exit Sub
			Dim oxmlAssets As XmlElement = CType(applicantDoc.AppendChild(applicantDoc.OwnerDocument.CreateElement("ASSETS", NAMESPACE_URI)), XmlElement)
			For Each asset In AssetInfoList
				AddAssetInfoXml(oxmlAssets, asset)
			Next
		End Sub
		Private Sub AddAssetInfoXml(poParent As XmlElement, assetInfo As CAssetInfo)
			Dim oxmlAssetInfo As XmlElement
			With assetInfo

				'Create  asset element 
				'inbranch quirk, some use ASSET while other use custom name as element name
				If .asset_type = AssetType.SAVINGS_ACCOUNT.ToString() OrElse .asset_type = AssetType.CERTIFICATE.ToString() Then
					oxmlAssetInfo = CType(poParent.AppendChild(poParent.OwnerDocument.CreateElement(assetInfo.asset_type, NAMESPACE_URI)), XmlElement)
				ElseIf .asset_type = AssetType.SHARE_BACK.ToString() Then
					oxmlAssetInfo = CType(poParent.AppendChild(poParent.OwnerDocument.CreateElement("SHARES", NAMESPACE_URI)), XmlElement)
				ElseIf .asset_type = AssetType.REALESTATE.ToString() OrElse .asset_type = AssetType.REAL_ESTATE_CONST.ToString() Then
					oxmlAssetInfo = CType(poParent.AppendChild(poParent.OwnerDocument.CreateElement("REAL_ESTATE", NAMESPACE_URI)), XmlElement)
				ElseIf .asset_type = AssetType.AUTOMOBILE.ToString() OrElse .asset_type = AssetType.DOC_VESSEL.ToString() OrElse .asset_type = AssetType.FARM_EQUIP.ToString() OrElse .asset_type = AssetType.WATERCRAFT.ToString() Then
					oxmlAssetInfo = CType(poParent.AppendChild(poParent.OwnerDocument.CreateElement("VEHICLE", NAMESPACE_URI)), XmlElement)
				ElseIf .asset_type = AssetType.INSTRUMENTS.ToString() Then
					oxmlAssetInfo = CType(poParent.AppendChild(poParent.OwnerDocument.CreateElement("CERTIFICATE", NAMESPACE_URI)), XmlElement)
				Else
					oxmlAssetInfo = CType(poParent.AppendChild(poParent.OwnerDocument.CreateElement("ASSET", NAMESPACE_URI)), XmlElement)
				End If

				oxmlAssetInfo.SetAttribute("asset_type", .asset_type.NullSafeToUpper_)
				oxmlAssetInfo.SetAttribute("asset_value", Common.SafeDouble(.asset_value))
				oxmlAssetInfo.SetAttribute("verification_status", "N/A")
				oxmlAssetInfo.SetAttribute("property_address", String.Empty)
				oxmlAssetInfo.SetAttribute("property_state", String.Empty)
				oxmlAssetInfo.SetAttribute("property_city", String.Empty)
				oxmlAssetInfo.SetAttribute("existing_loan_amount", "0.00")
				oxmlAssetInfo.SetAttribute("description", .description)
				oxmlAssetInfo.SetAttribute("account_number", String.Empty)
				oxmlAssetInfo.SetAttribute("bank_name", String.Empty)
				oxmlAssetInfo.SetAttribute("is_collateral", "Y")
				oxmlAssetInfo.SetAttribute("ownership_type", "B")
				oxmlAssetInfo.SetAttribute("other_owner_name", String.Empty)
				oxmlAssetInfo.SetAttribute("other_owner_name2", String.Empty)
				oxmlAssetInfo.SetAttribute("available_account_info", String.Empty)
				oxmlAssetInfo.SetAttribute("stock_bond_mutualfund_share_count", "0")

				'LPQ CLF import doessn't support rate for ira and money market 
				If .asset_type = AssetType.CERTIFICATE.ToString() OrElse .asset_type = AssetType.SHARE_BACK.ToString() Then
					'oxmlAssetInfo.SetAttribute("certificate_number", String.Empty)
					'oxmlAssetInfo.SetAttribute("pledge_type", String.Empty)
					oxmlAssetInfo.SetAttribute("rate", Common.SafeDouble(.rate))
					'oxmlAssetInfo.SetAttribute("pledge_value_type", "AMOUNT")
					'oxmlAssetInfo.SetAttribute("pledge_value", "0.00")
				End If
			End With
		End Sub
		Protected Function AddSystemXml(ByVal poDoc As XmlDocument) As XmlElement
			Dim oxmlSystem As XmlElement = CType(poDoc.DocumentElement.AppendChild(poDoc.CreateElement("SYSTEM", NAMESPACE_URI)), XmlElement)
			Dim sInternalUseURI = "http://www.meridianlink.com/InternalUse"
			oxmlSystem.SetAttribute("xmlns:d2p1", sInternalUseURI)
			oxmlSystem.SetAttribute("source", "GATEWAY")
			oxmlSystem.SetAttribute("type", "LPQ")

			'TODO: enable this for all lender in mid 2016 when all lpq code is updated, not much effective since app is running on mobile device
			oxmlSystem.SetAttribute("origination_ip", IPAddress)

			Dim sExternalSource As String = "MOBILE WEBSITE"
			If ReferralSource <> "" Then
				sExternalSource = Right(ReferralSource, 20)
			ElseIf IsSSO Then
				sExternalSource = "MOBILE WEBSITE SSO"
			ElseIf IsComboMode Then
				sExternalSource = "MOBILE WEBSITE COMBO"
			ElseIf ClinicID <> "" Then
				sExternalSource = Left(ClinicName, 16) & " - M"
			ElseIf VendorName <> "" Then
				sExternalSource = Left(VendorName, 16) & " - M"
			Else
				sExternalSource = "MOBILE WEBSITE"
			End If

			oxmlSystem.SetAttribute("external_source", sExternalSource)
			''for clf - version 5.070
			'If LoanOfficerID <> "" Then
			'    Dim oxmlLoanOfficer As XmlElement = CType(oxmlSystem.AppendChild(oxmlSystem.OwnerDocument.CreateElement("LOAN_OFFICER", NAMESPACE_URI)), XmlElement)
			'    oxmlLoanOfficer.SetAttribute("id", LoanOfficerID)
			'End If

			'If BranchID <> "" Then
			'    Dim oxmlBranch As XmlElement = CType(oxmlSystem.AppendChild(oxmlSystem.OwnerDocument.CreateElement("BRANCH", NAMESPACE_URI)), XmlElement)
			'    oxmlBranch.SetAttribute("id", BranchID)
			'End If

			'Dim oxmlLender As XmlElement = CType(oxmlSystem.AppendChild(oxmlSystem.OwnerDocument.CreateElement("LENDER", NAMESPACE_URI)), XmlElement)
			'oxmlLender.SetAttribute("id", LenderID)

			'Dim oxmlOrganization As XmlElement = CType(oxmlSystem.AppendChild(oxmlSystem.OwnerDocument.CreateElement("ORGANIZATION", NAMESPACE_URI)), XmlElement)
			'oxmlOrganization.SetAttribute("id", OrganizationID)

			'If ClinicID <> "" Then
			'    Dim oxmlClinic As XmlElement = CType(oxmlSystem.AppendChild(oxmlSystem.OwnerDocument.CreateElement("CLINIC", NAMESPACE_URI)), XmlElement)
			'    oxmlClinic.SetAttribute("id", ClinicID)

			'    Dim oxmlClinicWorker As XmlElement = CType(oxmlSystem.AppendChild(oxmlSystem.OwnerDocument.CreateElement("CLINIC_WORKER", NAMESPACE_URI)), XmlElement)
			'    oxmlClinicWorker.SetAttribute("id", ClinicWorkerID)
			'End If

			''TODO
			'If Not String.IsNullOrEmpty(LPQVendorID) AndAlso Not String.IsNullOrEmpty(LPQWorkerID) Then ' 
			'    Dim oxmlClinic As XmlElement = CType(oxmlSystem.AppendChild(oxmlSystem.OwnerDocument.CreateElement("CLINIC", NAMESPACE_URI)), XmlElement)
			'    oxmlClinic.SetAttribute("id", LPQVendorID)

			'    '*********************************
			'    'TODO remove this after we fix inbranch schema validation
			'    Dim oxmlClinicWorker As XmlElement = CType(oxmlSystem.AppendChild(oxmlSystem.OwnerDocument.CreateElement("CLINIC_WORKER", NAMESPACE_URI)), XmlElement)
			'    'oxmlClinicWorker.SetAttribute("id", "dd1ac16b-0bbf-4d14-a4a1-9a383d9428a3")
			'    oxmlClinicWorker.SetAttribute("id", LPQWorkerID)
			'End If

			''for clf verions =5.203
			If LoanOfficerID <> "" Then
				Dim oxmlLoanOfficer As XmlElement = CType(oxmlSystem.AppendChild(oxmlSystem.OwnerDocument.CreateElement("LOAN_OFFICER", NAMESPACE_URI)), XmlElement)
				oxmlLoanOfficer.SetAttribute("internal_id", sInternalUseURI, LoanOfficerID)
			End If
			If BranchID <> "" Then
				Dim oxmlBranch As XmlElement = CType(oxmlSystem.AppendChild(oxmlSystem.OwnerDocument.CreateElement("BRANCH", NAMESPACE_URI)), XmlElement)
				oxmlBranch.SetAttribute("internal_id", sInternalUseURI, BranchID)
			End If

			Dim oxmlLender As XmlElement = CType(oxmlSystem.AppendChild(oxmlSystem.OwnerDocument.CreateElement("LENDER", NAMESPACE_URI)), XmlElement)
			oxmlLender.SetAttribute("internal_id", sInternalUseURI, LenderID)

			Dim oxmlOrganization As XmlElement = CType(oxmlSystem.AppendChild(oxmlSystem.OwnerDocument.CreateElement("ORGANIZATION", NAMESPACE_URI)), XmlElement)
			oxmlOrganization.SetAttribute("internal_id", sInternalUseURI, OrganizationID)

			If ClinicID <> "" Then
				Dim oxmlClinic As XmlElement = CType(oxmlSystem.AppendChild(oxmlSystem.OwnerDocument.CreateElement("CLINIC", NAMESPACE_URI)), XmlElement)
				oxmlClinic.SetAttribute("internal_id", sInternalUseURI, ClinicID)

				Dim oxmlClinicWorker As XmlElement = CType(oxmlSystem.AppendChild(oxmlSystem.OwnerDocument.CreateElement("CLINIC_WORKER", NAMESPACE_URI)), XmlElement)
				oxmlClinicWorker.SetAttribute("internal_id", sInternalUseURI, ClinicWorkerID)
			End If

			'TODO
			If Not String.IsNullOrEmpty(LPQVendorID) AndAlso Not String.IsNullOrEmpty(LPQWorkerID) Then	' 
				Dim oxmlClinic As XmlElement = CType(oxmlSystem.AppendChild(oxmlSystem.OwnerDocument.CreateElement("CLINIC", NAMESPACE_URI)), XmlElement)
				oxmlClinic.SetAttribute("internal_id", sInternalUseURI, LPQVendorID)

				'*********************************
				'TODO remove this after we fix inbranch schema validation
				Dim oxmlClinicWorker As XmlElement = CType(oxmlSystem.AppendChild(oxmlSystem.OwnerDocument.CreateElement("CLINIC_WORKER", NAMESPACE_URI)), XmlElement)
				'oxmlClinicWorker.SetAttribute("id", "dd1ac16b-0bbf-4d14-a4a1-9a383d9428a3")
				oxmlClinicWorker.SetAttribute("internal_id", sInternalUseURI, LPQWorkerID)
			End If
			Return oxmlSystem
		End Function

		Protected Function AddOtherSystemXml(ByVal poDoc As XmlDocument) As XmlElement
			If OtherLoanNumber = "" Then Return Nothing
			Dim oxmlSystem As XmlElement = CType(poDoc.DocumentElement.AppendChild(poDoc.CreateElement("SYSTEM", NAMESPACE_URI)), XmlElement)
			oxmlSystem.SetAttribute("loan_number", OtherLoanNumber)
			oxmlSystem.SetAttribute("type", "OTHER")

			Dim oxmlOrganization As XmlElement = CType(oxmlSystem.AppendChild(oxmlSystem.OwnerDocument.CreateElement("ORGANIZATION", NAMESPACE_URI)), XmlElement)
			'oxmlOrganization.SetAttribute("id", "")

			Return oxmlSystem
		End Function

		Protected Sub AddLoanStatusXml(ByVal poDoc As XmlDocument)
			'Dim sDate As String = SafeDateTimeXml(Date.Now)
			Dim oxmlLoanStatus As XmlElement = CType(poDoc.DocumentElement.AppendChild(poDoc.CreateElement("LOAN_STATUS", NAMESPACE_URI)), XmlElement)
			If isDisagreeSelect = "Y" Then
				oxmlLoanStatus.SetAttribute("loan_status", "MEMDC")
			Else
				oxmlLoanStatus.SetAttribute("loan_status", "PEN")
			End If
			oxmlLoanStatus.SetAttribute("initial_entry_timestamp", XmlConvert.ToString(HttpContext.Current.Session("StartedDate"), XmlDateTimeSerializationMode.Local))
			'oxmlLoanStatus.SetAttribute("last_modified", sDate)
			oxmlLoanStatus.SetAttribute("is_locked", "N")

		End Sub

		Protected Sub AddCustomQuestionsXml(ByVal poDoc As XmlDocument)
			If CustomQuestionAnswers.Any() Then
				Dim oxmlCustomQuestions As XmlElement = CType(poDoc.DocumentElement.AppendChild(poDoc.CreateElement("CUSTOM_QUESTIONS", NAMESPACE_URI)), XmlElement)
				Dim oXmlCustomQuestion, oxmlQuestionAnswer As XmlElement

				For Each oQuestionAnswer As CValidatedQuestionAnswers In CustomQuestionAnswers
					If Not oQuestionAnswer.Answers.Any() Then Continue For

					oXmlCustomQuestion = getCustomQuestionXmlNode(oQuestionAnswer.Question.Name, oxmlCustomQuestions)
					oXmlCustomQuestion.SetAttribute("question_type", oQuestionAnswer.Question.AnswerType.NullSafeToUpper_)
					For Each oAnswerItem In oQuestionAnswer.Answers
						oxmlQuestionAnswer = CType(oXmlCustomQuestion.AppendChild(poDoc.CreateElement("CUSTOM_QUESTION_ANSWER", NAMESPACE_URI)), XmlElement)
						oxmlQuestionAnswer.SetAttribute("answer_text", oAnswerItem.AnswerText)
						oxmlQuestionAnswer.SetAttribute("answer_value", oAnswerItem.AnswerValue)
					Next
				Next
			End If
		End Sub


		''' <summary>
		''' This searches the list of custom question answers already added. if an existing name is found
		''' then we add the answer to that question (checkbox list questions can have multiple answers)
		''' </summary>
		Private Function getCustomQuestionXmlNode(ByVal psCustomQuestionName As String, ByVal poxmlCustomQuestions As XmlElement) As XmlElement
			Dim oXmlCustomQuestion As XmlNode = poxmlCustomQuestions.SelectSingleNode("CUSTOM_QUESTION[@question_name=""" & psCustomQuestionName & """]")
			If oXmlCustomQuestion Is Nothing Then
				oXmlCustomQuestion = poxmlCustomQuestions.AppendChild(poxmlCustomQuestions.OwnerDocument.CreateElement("CUSTOM_QUESTION", NAMESPACE_URI))
				CType(oXmlCustomQuestion, XmlElement).SetAttribute("question_name", psCustomQuestionName.Replace("&#39;", "'"))
			End If

			Return CType(oXmlCustomQuestion, XmlElement)
		End Function

		''add applicant questionsXML for Loan using New api
		Private Sub AddApplicantQuestionsXML(ByVal oxmlApplicant As XmlElement, ByVal poApplicant As CApplicant)

			If poApplicant.ApplicantQuestionAnswers.Any() Then
				Dim oxmlApplicantQuestions As XmlElement = CType(oxmlApplicant.AppendChild(oxmlApplicant.OwnerDocument.CreateElement("APPLICANT_QUESTIONS", NAMESPACE_URI)), XmlElement)

				For Each oQuestionAnswers As CValidatedQuestionAnswers In poApplicant.ApplicantQuestionAnswers
					If Not oQuestionAnswers.Answers.Any() Then Continue For

					Dim oxmlApplicantQuestion As XmlElement = CType(oxmlApplicantQuestions.AppendChild(oxmlApplicantQuestions.OwnerDocument.CreateElement("APPLICANT_QUESTION", NAMESPACE_URI)), XmlElement)
					oxmlApplicantQuestion.SetAttribute("question_name", oQuestionAnswers.Question.Name)
					oxmlApplicantQuestion.SetAttribute("question_type", oQuestionAnswers.Question.AnswerType)
					For Each oAnswer In oQuestionAnswers.Answers
						Dim oxmlApplicantQuestionAnswer As XmlElement = CType(oxmlApplicantQuestion.AppendChild(oxmlApplicantQuestion.OwnerDocument.CreateElement("APPLICANT_QUESTION_ANSWER", NAMESPACE_URI)), XmlElement)
						oxmlApplicantQuestionAnswer.SetAttribute("answer_text", oAnswer.AnswerText)
						oxmlApplicantQuestionAnswer.SetAttribute("answer_value", oAnswer.AnswerValue)
					Next
				Next
			End If
		End Sub

		Protected Function AddHDMA(ByVal poDoc As XmlDocument) As XmlElement
			Dim oApplicant As CApplicant
			oApplicant = Applicants(0)
			If Not oApplicant.HasGMI <> "N" Then Return Nothing

			Dim oxmlHMDA As XmlElement = CType(poDoc.DocumentElement.AppendChild(poDoc.CreateElement("HMDA_XML", NAMESPACE_URI)), XmlElement)

			Dim applicant_number As String = "applicant_0_"
			Dim dicHDMA0 = New Dictionary(Of String, String) From {{applicant_number & "sex_not_provided", oApplicant.SexNotProvided}, {applicant_number & "sex", oApplicant.Sex},
																   {applicant_number & "ethnicity_not_provided", oApplicant.EthnicityNotProvided}, {applicant_number & "ethnicity_is_not_hispanic", oApplicant.EthnicityIsNotHispanic},
																   {applicant_number & "ethnicity_is_hispanic", oApplicant.EthnicityIsHispanic}, {applicant_number & "ethnicity_hispanic_other", oApplicant.EthnicityHispanicOther},
																   {applicant_number & "ethnicity_hispanic", oApplicant.EthnicityHispanic}, {applicant_number & "race_asian", oApplicant.RaceAsian},
																   {applicant_number & "race_asian_other", oApplicant.RaceAsianOther}, {applicant_number & "race_base", oApplicant.RaceBase},
																   {applicant_number & "race_not_provided", oApplicant.RaceNotProvided}, {applicant_number & "race_pacificislander", oApplicant.RacePacificIslander},
																   {applicant_number & "race_pacificislander_other", oApplicant.RacePacificIslanderOther}, {applicant_number & "tribe_name", oApplicant.RaceTribeName}
																  }
			Dim dicHDMA1 As New Dictionary(Of String, String)
			If oApplicant.HasSpouse Then
				oApplicant = Applicants(1)
				applicant_number = "applicant_1_"
				dicHDMA1 = New Dictionary(Of String, String) From {{applicant_number & "sex_not_provided", oApplicant.SexNotProvided}, {applicant_number & "sex", oApplicant.Sex},
																   {applicant_number & "ethnicity_not_provided", oApplicant.EthnicityNotProvided}, {applicant_number & "ethnicity_is_not_hispanic", oApplicant.EthnicityIsNotHispanic},
																   {applicant_number & "ethnicity_is_hispanic", oApplicant.EthnicityIsHispanic}, {applicant_number & "ethnicity_hispanic_other", oApplicant.EthnicityHispanicOther},
																   {applicant_number & "ethnicity_hispanic", oApplicant.EthnicityHispanic}, {applicant_number & "race_asian", oApplicant.RaceAsian},
																   {applicant_number & "race_asian_other", oApplicant.RaceAsianOther}, {applicant_number & "race_base", oApplicant.RaceBase},
																   {applicant_number & "race_not_provided", oApplicant.RaceNotProvided}, {applicant_number & "race_pacificislander", oApplicant.RacePacificIslander},
																   {applicant_number & "race_pacificislander_other", oApplicant.RacePacificIslanderOther}, {applicant_number & "tribe_name", oApplicant.RaceTribeName}
																  }
			End If

			For Each kvp As KeyValuePair(Of String, String) In dicHDMA0
				Dim oxmlITEM = CType(oxmlHMDA.AppendChild(poDoc.CreateElement("ITEM", NAMESPACE_URI)), XmlElement)
				oxmlITEM.SetAttribute("key", kvp.Key)
				oxmlITEM.SetAttribute("value", kvp.Value)
			Next

			For Each kvp As KeyValuePair(Of String, String) In dicHDMA1
				Dim oxmlITEM = CType(oxmlHMDA.AppendChild(poDoc.CreateElement("ITEM", NAMESPACE_URI)), XmlElement)
				oxmlITEM.SetAttribute("key", kvp.Key)
				oxmlITEM.SetAttribute("value", kvp.Value)
			Next
			Return oxmlHMDA
		End Function
		''business loan 
#Region "Business Loan"
		Protected Sub AddBusinessInfoXml(ByVal poDoc As XmlDocument)
			Dim oxmlCompanyInfo As XmlElement = CType(poDoc.DocumentElement.AppendChild(poDoc.CreateElement("COMPANY_INFO", NAMESPACE_URI)), XmlElement)
			oxmlCompanyInfo.SetAttribute("company_name", BLBusinessInfo.CompanyName)
			oxmlCompanyInfo.SetAttribute("business_tax_id", BLBusinessInfo.TaxID)
			oxmlCompanyInfo.SetAttribute("phone", BLBusinessInfo.HomePhone)
			If BLBusinessInfo.MobilePhone <> "" Then
				oxmlCompanyInfo.SetAttribute("cell_phone", BLBusinessInfo.MobilePhone)
			End If
			If BLBusinessInfo.Fax <> "" Then
				oxmlCompanyInfo.SetAttribute("fax", BLBusinessInfo.Fax)
			End If
			If BLBusinessInfo.EmailAddr <> "" Then
				oxmlCompanyInfo.SetAttribute("email", BLBusinessInfo.EmailAddr)
			End If
			If BLBusinessInfo.WebsiteURL <> "" Then
				oxmlCompanyInfo.SetAttribute("website_url", BLBusinessInfo.WebsiteURL)
			End If
			' oxmlCompanyInfo.SetAttribute("months_in_business", "")

			If BLBusinessInfo.BusinessStatus <> "" Then
				oxmlCompanyInfo.SetAttribute("business_status", BLBusinessInfo.BusinessStatus)
			End If

			If BLBusinessInfo.StateRegistered <> "" Then
				oxmlCompanyInfo.SetAttribute("state_registered", BLBusinessInfo.StateRegistered)
			End If

			If BLBusinessInfo.MemberNumber <> "" Then
				oxmlCompanyInfo.SetAttribute("member_number", BLBusinessInfo.MemberNumber)
			End If

			oxmlCompanyInfo.SetAttribute("business_industry", BLBusinessInfo.Industry)
			oxmlCompanyInfo.SetAttribute("business_description", BLBusinessInfo.BusinessDescription)
			oxmlCompanyInfo.SetAttribute("curr_monthly_gross_income", BLBusinessInfo.GrossMonthlyIncome)
			If Common.SafeString(BLBusinessInfo.EstablishDate) <> "" Then
				oxmlCompanyInfo.SetAttribute("establish_date", SafeDateXml(BLBusinessInfo.EstablishDate))
			End If

			oxmlCompanyInfo.SetAttribute("curr_monthly_net_income", BLBusinessInfo.NetMonthlyIncome)
			oxmlCompanyInfo.SetAttribute("num_employees_and_owners", BLBusinessInfo.NumberOfEmployees)
			oxmlCompanyInfo.SetAttribute("primary_bank", BLBusinessInfo.PrimaryBank)
			oxmlCompanyInfo.SetAttribute("total_existing_loan_balance", BLBusinessInfo.TotalExistingLoanBalance)
			oxmlCompanyInfo.SetAttribute("total_existing_loan_payment", BLBusinessInfo.TotalExistingMonthlyPayment)
			oxmlCompanyInfo.SetAttribute("total_checking_balance", BLBusinessInfo.TotalCheckingAccountBalance)
			oxmlCompanyInfo.SetAttribute("entity_type", BLBusinessInfo.BusinessEntity)
			''<YEAR_TO_DATE_HISTORICAL_DATA total_sales="0.00" net_income="0.00" depreciation_amortization="0.00" interest_paid="0.00" owner_salary="0.00" non_recurring_expenses="0.00" rent="0.00" />
			''<CURRENT_HISTORICAL_DATA total_sales="0.00" net_income="0.00" depreciation_amortization="0.00" interest_paid="0.00" owner_salary="0.00" non_recurring_expenses="0.00" rent="0.00" />
			''<PREVIOUS_HISTORICAL_DATA total_sales="0.00" net_income="0.00" depreciation_amortization="0.00" interest_paid="0.00" owner_salary="0.00" non_recurring_expenses="0.00" rent="0.00" />
			''YEAR_TO_DATE_HISTORICAL_DATA
			Dim oxmlYearToDateHistoricalData As XmlElement = CType(oxmlCompanyInfo.AppendChild(poDoc.CreateElement("YEAR_TO_DATE_HISTORICAL_DATA", NAMESPACE_URI)), XmlElement)
			Dim oxmlCurrentHistoricalData As XmlElement = CType(oxmlCompanyInfo.AppendChild(poDoc.CreateElement("CURRENT_HISTORICAL_DATA", NAMESPACE_URI)), XmlElement)
			Dim oxmlPreviousHistoricalData As XmlElement = CType(oxmlCompanyInfo.AppendChild(poDoc.CreateElement("PREVIOUS_HISTORICAL_DATA", NAMESPACE_URI)), XmlElement)

			Dim oxmlCurrentAddress As XmlElement = CType(oxmlCompanyInfo.AppendChild(poDoc.CreateElement("CURRENT_ADDRESS", NAMESPACE_URI)), XmlElement)
			oxmlCurrentAddress.SetAttribute("occupancy_status", CEnum.MapOccupancyTypeToValue(BLBusinessInfo.OccupyingLocation))
			oxmlCurrentAddress.SetAttribute("occupancy_duration", BLBusinessInfo.OccupancyDuration)
			If BLBusinessInfo.OccupancyDescription IsNot Nothing AndAlso BLBusinessInfo.OccupancyDescription <> "" Then
				oxmlCurrentAddress.SetAttribute("other_occupancy_description", BLBusinessInfo.OccupancyDescription)
			End If
			Dim oxmlLooseAddress As XmlElement = CType(oxmlCurrentAddress.AppendChild(poDoc.CreateElement("LOOSE_ADDRESS", NAMESPACE_URI)), XmlElement)
			oxmlLooseAddress.SetAttribute("street_address_1", BLBusinessInfo.AddressStreet)
			oxmlLooseAddress.SetAttribute("city", BLBusinessInfo.AddressCity)
			oxmlLooseAddress.SetAttribute("state", BLBusinessInfo.AddressState)
			oxmlLooseAddress.SetAttribute("zip", BLBusinessInfo.AddressZip)
			''mailing address
			Dim oxmlMailingAddress As XmlElement = CType(oxmlCompanyInfo.AppendChild(poDoc.CreateElement("MAILING_ADDRESS", NAMESPACE_URI)), XmlElement)
			If BLBusinessInfo.HasMailingAddress = "Y" Then
				oxmlMailingAddress.SetAttribute("street_address_1", BLBusinessInfo.MailingAddressStreet)
				oxmlMailingAddress.SetAttribute("city", BLBusinessInfo.MailingAddressCity)
				oxmlMailingAddress.SetAttribute("state", BLBusinessInfo.MailingAddressState)
				oxmlMailingAddress.SetAttribute("zip", BLBusinessInfo.MailingAddressZip)
				oxmlMailingAddress.SetAttribute("is_current", "N")
			Else
				oxmlMailingAddress.SetAttribute("is_current", "Y")
			End If
		End Sub


		Protected Sub AddBeneficialOwnersXml(ByVal poDoc As XmlDocument)
			Dim oxmlBeneficialOwners As XmlElement = CType(poDoc.DocumentElement.AppendChild(poDoc.CreateElement("BENEFICIAL_OWNERS", NAMESPACE_URI)), XmlElement)
			If BLApplicants.Count > 0 Then
				For Each oItem In BLApplicants
					Dim oxmlBeneOwner As XmlElement = CType(oxmlBeneficialOwners.AppendChild(poDoc.CreateElement("BENEFICIAL_OWNER", NAMESPACE_URI)), XmlElement)
					oxmlBeneOwner.SetAttribute("first_name", oItem.FirstName)
					oxmlBeneOwner.SetAttribute("last_name", oItem.LastName)
					oxmlBeneOwner.SetAttribute("middle_name", oItem.MiddleName)
					If Common.SafeStringNoTrim(oItem.DOB) <> "" Then
						oxmlBeneOwner.SetAttribute("dob", SafeDateXml(oItem.DOB))
					End If
					oxmlBeneOwner.SetAttribute("is_tin_ssn", IIf(oItem.SSN.StartsWith("9"), "Y", "N").ToString())
					oxmlBeneOwner.SetAttribute("ssn", oItem.SSN)
					oxmlBeneOwner.SetAttribute("address", oItem.AddressStreet)
					oxmlBeneOwner.SetAttribute("city", oItem.AddressCity)
					oxmlBeneOwner.SetAttribute("zip", oItem.AddressZip)
					oxmlBeneOwner.SetAttribute("state", oItem.AddressState)
					'' oxmlBeneOwner.SetAttribute("country", beneAddress.Country)
					oxmlBeneOwner.SetAttribute("percent_business_owned", oItem.BusinessOwned)
					Dim isBeneficialOwner = "N"
					If oItem.HasControl.ToUpper() = "YES" Then
						isBeneficialOwner = "Y"
					Else
						If oItem.BusinessOwned >= 25 Then
							isBeneficialOwner = "Y"
						End If
					End If
					oxmlBeneOwner.SetAttribute("is_beneficial_owner", isBeneficialOwner)
					oxmlBeneOwner.SetAttribute("is_control", IIf(oItem.HasControl.ToUpper() = "YES", "Y", "N").ToString())
					oxmlBeneOwner.SetAttribute("control_title", CEnum.MapRoleNameToValue(oItem.ControlTitle).ToUpper())
					Dim oxmlBeneficialID As XmlElement = CType(oxmlBeneOwner.AppendChild(poDoc.CreateElement("ID_CARD", NAMESPACE_URI)), XmlElement)
					oxmlBeneficialID.SetAttribute("card_type", oItem.IDCardType)
					oxmlBeneficialID.SetAttribute("card_number", oItem.IDCardNumber)
					oxmlBeneficialID.SetAttribute("state", oItem.IDState)
					oxmlBeneficialID.SetAttribute("country", oItem.IDCountry)
					If (Common.SafeString(oItem.IDDateExpire) <> "") Then
						oxmlBeneficialID.SetAttribute("exp_date", Common.SafeDateXml(oItem.IDDateExpire))
					End If
					If (Common.SafeString(oItem.IDDateIssued) <> "") Then
						oxmlBeneficialID.SetAttribute("date_issued", Common.SafeDateXml(oItem.IDDateIssued))
					End If
				Next
			End If

			If BLBeneficialOwners.Count > 0 Then
				For Each oItem In BLBeneficialOwners
					Dim oxmlBeneOwner As XmlElement = CType(oxmlBeneficialOwners.AppendChild(poDoc.CreateElement("BENEFICIAL_OWNER", NAMESPACE_URI)), XmlElement)
					oxmlBeneOwner.SetAttribute("first_name", oItem.FirstName)
					oxmlBeneOwner.SetAttribute("last_name", oItem.LastName)
					oxmlBeneOwner.SetAttribute("middle_name", oItem.MiddleName)
					If Common.SafeString(oItem.Dob) <> "" Then
						oxmlBeneOwner.SetAttribute("dob", SafeDateXml(oItem.Dob))
					End If
					oxmlBeneOwner.SetAttribute("is_tin_ssn", IIf(oItem.Ssn.StartsWith("9"), "Y", "N").ToString())
					oxmlBeneOwner.SetAttribute("ssn", oItem.Ssn)
					oxmlBeneOwner.SetAttribute("address", oItem.AddressStreet)
					oxmlBeneOwner.SetAttribute("city", oItem.AddressCity)
					oxmlBeneOwner.SetAttribute("zip", oItem.AddressZip)
					oxmlBeneOwner.SetAttribute("state", oItem.AddressState)
					'' oxmlBeneOwner.SetAttribute("country", beneAddress.Country)
					oxmlBeneOwner.SetAttribute("percent_business_owned", oItem.BusinessOwned)
					Dim isBeneficialOwner = "N"
					If oItem.HasControl.ToUpper() = "YES" Then
						isBeneficialOwner = "Y"
					Else
						If oItem.BusinessOwned >= 25 Then
							isBeneficialOwner = "Y"
						End If
					End If
					oxmlBeneOwner.SetAttribute("is_beneficial_owner", isBeneficialOwner)
					oxmlBeneOwner.SetAttribute("is_control", IIf(oItem.HasControl.ToUpper() = "YES", "Y", "N").ToString())
					oxmlBeneOwner.SetAttribute("control_title", CEnum.MapRoleNameToValue(oItem.ControlTitle).ToUpper())
					Dim oxmlBeneficialID As XmlElement = CType(oxmlBeneOwner.AppendChild(poDoc.CreateElement("ID_CARD", NAMESPACE_URI)), XmlElement)
					''oxmlBeneficialID.SetAttribute("card_type", "")
					''oxmlBeneficialID.SetAttribute("card_number", "")
					''oxmlBeneficialID.SetAttribute("state", "")
					''oxmlBeneficialID.SetAttribute("country", "")
					''oxmlBeneficialID.SetAttribute("exp_date", "")
					''oxmlBeneficialID.SetAttribute("date_issued", "")
				Next
			End If
		End Sub


		Protected Sub AddBusinessApplicantsXml(ByVal poDoc As XmlDocument, oConfig As CWebsiteConfig)
			Dim oxmlApplicants As XmlElement = CType(poDoc.DocumentElement.AppendChild(poDoc.CreateElement("APPLICANTS", NAMESPACE_URI)), XmlElement)
			If BLApplicants IsNot Nothing AndAlso BLApplicants.Any() Then
				For Each app As CBLApplicantInfo In BLApplicants
					Dim oBADeclaration = BLDeclarationDic.Item(app.selectedApplicant.IDPrefix)
					If app.IsJoint.ToUpper() = "Y" Then Continue For
					Dim oxmlApplicant = AddBusinessApplicantXml(oxmlApplicants, app, "APPLICANT", oConfig, oBADeclaration)
					If app IsNot BLApplicants.Last() AndAlso BLApplicants(BLApplicants.IndexOf(app) + 1).IsJoint.ToUpper() = "Y" Then
						AddBusinessApplicantXml(oxmlApplicant, BLApplicants(BLApplicants.IndexOf(app) + 1), "SPOUSE", oConfig, oBADeclaration)
					End If
				Next
			End If
		End Sub
		Protected Function AddBusinessApplicantXml(ByVal poParent As XmlElement, app As CBLApplicantInfo, ByVal psApplicantNodeName As String, ByVal oConfig As CWebsiteConfig, ByVal BADeclaration As CBLDeclarationInfo) As XmlElement
			Dim oxmlApplicant As XmlElement = CType(poParent.AppendChild(poParent.OwnerDocument.CreateElement(psApplicantNodeName, NAMESPACE_URI)), XmlElement)
			With app
				oxmlApplicant.SetAttribute("first_name", .FirstName.NullSafeToUpper_)
				oxmlApplicant.SetAttribute("last_name", .LastName.NullSafeToUpper_)
				oxmlApplicant.SetAttribute("suffix", .NameSuffix.NullSafeToUpper_)
				oxmlApplicant.SetAttribute("middle_name", .MiddleName.NullSafeToUpper_)
				oxmlApplicant.SetAttribute("ssn", .SSN)
				Dim sDOB As String = SafeDateXml(.DOB)
				If sDOB <> "" Then
					oxmlApplicant.SetAttribute("dob", sDOB)	'1967-08-dd
				End If
				oxmlApplicant.SetAttribute("citizenship", CEnum.MapCitizenshipStatusToValue(.CitizenshipStatus))
				oxmlApplicant.SetAttribute("member_number", SafeString(.MemberNumber).Replace("n/a", "").Replace("N/A", "")) 'not sure why this value is passed to lenderside bc there is no number format along this path

				oxmlApplicant.SetAttribute("marital_status", CEnum.MapMaritalStatusToValue(.MaritalStatus))
				oxmlApplicant.SetAttribute("applicant_type", app.selectedApplicant.appTypeValue)
				' Foreign contact, moved to contact node
				'oxmlApplicant.SetAttribute("home_phone", .HomePhone).Replace("-", "").Replace("(", "").Replace(")", "").Replace(".", "").Replace(" ", ""))
				'oxmlApplicant.SetAttribute("is_home_phone_foreign", "Y")
				'oxmlApplicant.SetAttribute("work_phone", .WorkPhone).Replace("-", "").Replace("(", "").Replace(")", "").Replace(".", "").Replace(" ", ""))
				'oxmlApplicant.SetAttribute("is_work_phone_foreign", "Y")
				'oxmlApplicant.SetAttribute("cell_phone", .CellPhone).Replace("-", "").Replace("(", "").Replace(")", "").Replace(".", "").Replace(" ", ""))
				'oxmlApplicant.SetAttribute("is_cell_phone_foreign", "Y")

				'Dim oxmlIDCard As XmlElement = CType(oxmlApplicant.AppendChild(oxmlApplicant.OwnerDocument.CreateElement("ID_CARD", NAMESPACE_URI)), XmlElement)
				'oxmlIDCard.SetAttribute("card_number", .DriversLicenseNumber)
				'oxmlIDCard.SetAttribute("card_type", "DRIVERS_LICENSE")
				'oxmlIDCard.SetAttribute("country", "USA")
				'oxmlIDCard.SetAttribute("state", .DriversLicenseState)
				If Not String.IsNullOrEmpty(.IDCardNumber) Then
					Dim oxmlIDCard As XmlElement = CType(oxmlApplicant.AppendChild(oxmlApplicant.OwnerDocument.CreateElement("ID_CARD", NAMESPACE_URI)), XmlElement)
					oxmlIDCard.SetAttribute("card_type", .IDCardType)
					oxmlIDCard.SetAttribute("card_number", .IDCardNumber)
					oxmlIDCard.SetAttribute("country", .IDCountry)
					oxmlIDCard.SetAttribute("state", .IDState)
					If Not String.IsNullOrEmpty(SafeDateXml(.IDDateIssued)) Then
						oxmlIDCard.SetAttribute("date_issued", SafeDateXml(.IDDateIssued))
					End If
					If Not String.IsNullOrEmpty(SafeDateXml(.IDDateExpire)) Then
						oxmlIDCard.SetAttribute("exp_date", SafeDateXml(.IDDateExpire))
					End If
				End If
				''secondary id card
				If Not String.IsNullOrEmpty(.SecondaryIDCardNumber) Then
					Dim oxmlSecondaryIDCard As XmlElement = CType(oxmlApplicant.AppendChild(oxmlApplicant.OwnerDocument.CreateElement("ID_CARD2", NAMESPACE_URI)), XmlElement)
					oxmlSecondaryIDCard.SetAttribute("card_type", .SecondaryIDCardType)
					oxmlSecondaryIDCard.SetAttribute("card_number", .SecondaryIDCardNumber)
					oxmlSecondaryIDCard.SetAttribute("country", .SecondaryIDCountry)
					oxmlSecondaryIDCard.SetAttribute("state", .SecondaryIDState)
					If Not String.IsNullOrEmpty(SafeDateXml(.SecondaryIDDateIssued)) Then
						oxmlSecondaryIDCard.SetAttribute("date_issued", SafeDateXml(.SecondaryIDDateIssued))
					End If
					If Not String.IsNullOrEmpty(SafeDateXml(.SecondaryIDDateExpire)) Then
						oxmlSecondaryIDCard.SetAttribute("exp_date", SafeDateXml(.SecondaryIDDateExpire))
					End If
				End If
				''''' get current email
				'' CurrentEmail = .ContactEmail

				'If isDisagreeSelect = "Y" Then
				'    oxmlApplicant.SetAttribute("is_declined", "Y")
				'    Dim oxmlApprovalDenialReason As XmlElement = CType(oxmlApplicant.AppendChild(oxmlApplicant.OwnerDocument.CreateElement("APPROVAL_DENIAL_REASON", NAMESPACE_URI)), XmlElement)
				'    oxmlApprovalDenialReason.InnerText = "Applicant Declined to Pull Credit"
				'End If

				''Applicant Questions
				''AddApplicantQuestionsXML(oxmlApplicant, poApplicant)
				''**************************************
				'''' Applicant Question mapping for xa in combo mode
				If .ApplicantQuestionAnswers.Any() Then
					Dim oxmlApplicantQuestions As XmlElement = CType(oxmlApplicant.AppendChild(oxmlApplicant.OwnerDocument.CreateElement("APPLICANT_QUESTIONS", NAMESPACE_URI)), XmlElement)

					For Each oQuestionAnswers As CValidatedQuestionAnswers In .ApplicantQuestionAnswers
						If Not oQuestionAnswers.Answers.Any() Then Continue For

						Dim oxmlApplicantQuestion As XmlElement = CType(oxmlApplicantQuestions.AppendChild(oxmlApplicantQuestions.OwnerDocument.CreateElement("APPLICANT_QUESTION", NAMESPACE_URI)), XmlElement)
						oxmlApplicantQuestion.SetAttribute("question_name", oQuestionAnswers.Question.Name)
						oxmlApplicantQuestion.SetAttribute("question_type", oQuestionAnswers.Question.AnswerType)
						For Each oAnswer In oQuestionAnswers.Answers
							Dim oxmlApplicantQuestionAnswer As XmlElement = CType(oxmlApplicantQuestion.AppendChild(oxmlApplicantQuestion.OwnerDocument.CreateElement("APPLICANT_QUESTION_ANSWER", NAMESPACE_URI)), XmlElement)
							oxmlApplicantQuestionAnswer.SetAttribute("answer_text", oAnswer.AnswerText)
							oxmlApplicantQuestionAnswer.SetAttribute("answer_value", oAnswer.AnswerValue)
						Next
					Next
				End If

				Dim oxmlCurrentAddress As XmlElement = CType(oxmlApplicant.AppendChild(oxmlApplicant.OwnerDocument.CreateElement("CURRENT_ADDRESS", NAMESPACE_URI)), XmlElement)
				oxmlCurrentAddress.SetAttribute("occupancy_status", CEnum.MapOccupancyTypeToValue(.OccupyingLocation))
				oxmlCurrentAddress.SetAttribute("occupancy_duration", .OccupancyDuration)
				If .OccupancyDescription IsNot Nothing AndAlso .OccupancyDescription <> "" Then
					oxmlCurrentAddress.SetAttribute("other_occupancy_description", .OccupancyDescription)
				End If
				''*************
				' Address - use either loose or three line format
				' Foreign address
				'replace . and , if it exist in address and city to prevent errors 
				.AddressStreet = .AddressStreet.NullSafeToUpper_.Replace(",", "").Replace(".", "")
				.AddressStreet2 = .AddressStreet2.NullSafeToUpper_.Replace(",", "").Replace(".", "")
				.AddressCity = .AddressCity.Trim().Replace(",", "").Replace(".", "")

				If .AddressCountry <> "" And .AddressCountry <> "USA" Then
					Dim oxmlCurrentThreeLineAddress As XmlElement = CType(oxmlCurrentAddress.AppendChild(oxmlApplicant.OwnerDocument.CreateElement("THREE_LINE_ADDRESS", NAMESPACE_URI)), XmlElement)
					oxmlCurrentThreeLineAddress.SetAttribute("street_address_1", .AddressStreet.NullSafeToUpper_)
					oxmlCurrentThreeLineAddress.SetAttribute("street_address_2", .AddressStreet2.NullSafeToUpper_)
					'oxmlCurrentThreeLineAddress.SetAttribute("street_address_2", .CurrentCity & ", " & .CurrentZip)
					'oxmlCurrentThreeLineAddress.SetAttribute("street_address_3", .CurrentCountry)
					oxmlCurrentThreeLineAddress.SetAttribute("city", .AddressCity.NullSafeToUpper_)
					oxmlCurrentThreeLineAddress.SetAttribute("country", .AddressCountry.NullSafeToUpper_)
					oxmlCurrentThreeLineAddress.SetAttribute("zip", .AddressZip)
				Else
					'domestic
					Dim oxmlLooseAddress As XmlElement = CType(oxmlCurrentAddress.AppendChild(oxmlApplicant.OwnerDocument.CreateElement("LOOSE_ADDRESS", NAMESPACE_URI)), XmlElement)
					oxmlLooseAddress.SetAttribute("street_address_1", .AddressStreet.NullSafeToUpper_)
					oxmlLooseAddress.SetAttribute("city", .AddressCity.NullSafeToUpper_)
					oxmlLooseAddress.SetAttribute("state", .AddressState.NullSafeToUpper_)
					oxmlLooseAddress.SetAttribute("zip", .AddressZip)
				End If

				''Previous Address
				If .HasPreviousAddress = "Y" Then

					.PreviousAddressStreet = .PreviousAddressStreet.Replace(",", "").Replace(".", "")
					.PreviousAddressCity = .PreviousAddressCity.Replace(",", "").Replace(".", "")
					Dim oxmlPreviousAddress As XmlElement = CType(oxmlApplicant.AppendChild(oxmlApplicant.OwnerDocument.CreateElement("PREVIOUS_ADDRESS", NAMESPACE_URI)), XmlElement)
					oxmlPreviousAddress.SetAttribute("street_address_1", .PreviousAddressStreet.NullSafeToUpper_)
					oxmlPreviousAddress.SetAttribute("city", .PreviousAddressCity.NullSafeToUpper_)
					oxmlPreviousAddress.SetAttribute("state", .PreviousAddressState.NullSafeToUpper_)
					oxmlPreviousAddress.SetAttribute("zip", .PreviousAddressZip.NullSafeToUpper_)
					oxmlPreviousAddress.SetAttribute("country", .PreviousAddressCountry.NullSafeToUpper_)
				End If
				''end Previous Address

				''**************

				'  Mailing Address
				Dim oxmlMailingAddress As XmlElement = CType(oxmlApplicant.AppendChild(oxmlApplicant.OwnerDocument.CreateElement("MAILING_ADDRESS", NAMESPACE_URI)), XmlElement)
				If .HasMailingAddress = "Y" Then
					oxmlMailingAddress.SetAttribute("street_address_1", .MailingAddressStreet.NullSafeToUpper_)
					oxmlMailingAddress.SetAttribute("street_address_2", .MailingAddressStreet2.NullSafeToUpper_)
					oxmlMailingAddress.SetAttribute("city", .MailingAddressCity.NullSafeToUpper_)
					oxmlMailingAddress.SetAttribute("state", .MailingAddressState.NullSafeToUpper_)
					oxmlMailingAddress.SetAttribute("zip", .MailingAddressZip)
					oxmlMailingAddress.SetAttribute("country", .MailingAddressCountry.NullSafeToUpper_)
					oxmlMailingAddress.SetAttribute("is_current", "N")
				Else
					oxmlMailingAddress.SetAttribute("is_current", "Y")
				End If


				Dim oxmlFinancialInfo As XmlElement = CType(oxmlApplicant.AppendChild(oxmlApplicant.OwnerDocument.CreateElement("FINANCIAL_INFO", NAMESPACE_URI)), XmlElement)
				Dim oxmlCurrentEmployment As XmlElement = CType(oxmlFinancialInfo.AppendChild(oxmlApplicant.OwnerDocument.CreateElement("CURRENT_EMPLOYMENT", NAMESPACE_URI)), XmlElement)
				' new employment logic
				If .EmploymentStatus.Contains("MILITARY") Then
					oxmlCurrentEmployment.SetAttribute("employer", CEnum.MapBranchOfServiceToValue(.BranchOfService))
				Else
					If .EmploymentStatus = "HOMEMAKER" Or .EmploymentStatus = "STUDENT" Or .EmploymentStatus = "UNEMPLOYED" Or .EmploymentStatus.Contains("RETIRED") Then
						.Employer = "" ''no employer 
					End If
					oxmlCurrentEmployment.SetAttribute("employer", .Employer.NullSafeToUpper_)
				End If
				Dim employed_months As Integer
				employed_months = SafeInteger(.EmployedDurationYear) * 12 + SafeInteger(.EmployedDurationMonth)
				'Dim profession_months As Integer
				''  profession_months = SafeInteger(.txtProfessionDuration_year) * 12 + SafeInteger(.txtProfessionDuration_month)
				''no employment duration for student and homemaker
				If .EmploymentStatus <> "STUDENT" And .EmploymentStatus <> "HOMEMAKER" Then
					oxmlCurrentEmployment.SetAttribute("employed_months", employed_months.ToString())
					'If profession_months = 0 Then
					oxmlCurrentEmployment.SetAttribute("profession_months", employed_months.ToString())
					'Else
					'    oxmlCurrentEmployment.SetAttribute("profession_months", profession_months.ToString())
					'End If
				End If

				oxmlCurrentEmployment.SetAttribute("occupation", .JobTitle.NullSafeToUpper_)
				oxmlCurrentEmployment.SetAttribute("employment_business_type", .BusinessType.NullSafeToUpper_)
				oxmlCurrentEmployment.SetAttribute("supervisor_name", .SupervisorName.NullSafeToUpper_)
				oxmlCurrentEmployment.SetAttribute("pay_grade", .PayGrade)
				Dim employmentStatus = CEnum.MapEmploymentStatusToValue(.EmploymentStatus)
				'' to add employement_start_date attribute: make sure employment status is Military active 
				If employmentStatus = "MI" Then
					If Not String.IsNullOrEmpty(SafeDateXml(.EmploymentStartDate)) Then
						oxmlCurrentEmployment.SetAttribute("employment_start_date", SafeDateXml(.EmploymentStartDate))
					End If
				End If
				If Not String.IsNullOrEmpty(.ETS) AndAlso Not String.IsNullOrEmpty(SafeDateXml(.ETS)) Then
					oxmlCurrentEmployment.SetAttribute("ets", SafeDateXml(.ETS))
				End If
				' end new employment logic

				oxmlCurrentEmployment.SetAttribute("employment_status", CEnum.MapEmploymentStatusToValue(.EmploymentStatus))
				oxmlCurrentEmployment.SetAttribute("other_employment_description", .EmploymentDescription.NullSafeToUpper_)
				''previous employment information 
				If .HasPreviousEmployment = "Y" Then
					Dim oxmlPreviousEmployment As XmlElement = CType(oxmlFinancialInfo.AppendChild(oxmlApplicant.OwnerDocument.CreateElement("PREVIOUS_EMPLOYMENT", NAMESPACE_URI)), XmlElement)
					If .PrevEmploymentStatus.Contains("MILITARY") Then
						oxmlPreviousEmployment.SetAttribute("employer", CEnum.MapBranchOfServiceToValue(.PrevBranchOfService))
					Else
						If .PrevEmploymentStatus = "HOMEMAKER" Or .PrevEmploymentStatus = "STUDENT" Or .PrevEmploymentStatus = "UNEMPLOYED" Or .PrevEmploymentStatus.Contains("RETIRED") Then
							.PrevEmployer = "" ''no employer 
						End If
						oxmlPreviousEmployment.SetAttribute("employer", .PrevEmployer.NullSafeToUpper_)
					End If
					Dim pre_employed_month As Integer = SafeInteger(.PrevEmployedDurationYear) * 12 + SafeInteger(.PrevEmployedDurationMonth)

					If .PrevEmploymentStatus = "STUDENT" Or .PrevEmploymentStatus = "HOMEMAKER" Then
						pre_employed_month = 0
					End If
					oxmlPreviousEmployment.SetAttribute("employed_months", pre_employed_month.ToString())
					oxmlPreviousEmployment.SetAttribute("occupation", .PrevJobTitle.NullSafeToUpper_)
					oxmlPreviousEmployment.SetAttribute("employment_business_type", .PrevBusinessType.NullSafeToUpper_)
					oxmlPreviousEmployment.SetAttribute("pay_grade", .PrevPayGrade)
					oxmlPreviousEmployment.SetAttribute("monthly_income", .PrevGrossMonthlyIncome)
					Dim prev_employmentStatus = CEnum.MapEmploymentStatusToValue(.PrevEmploymentStatus)
					If prev_employmentStatus = "MI" Then
						If Not String.IsNullOrEmpty(SafeDateXml(.PrevEmploymentStartDate)) Then
							oxmlPreviousEmployment.SetAttribute("employment_start_date", SafeDateXml(.PrevEmploymentStartDate))
						End If
					End If
					If Not String.IsNullOrEmpty(.PrevETS) AndAlso Not String.IsNullOrEmpty(SafeDateXml(.PrevETS)) Then
						oxmlPreviousEmployment.SetAttribute("ets", SafeDateXml(.PrevETS))
					End If
					oxmlPreviousEmployment.SetAttribute("employment_status", CEnum.MapEmploymentStatusToValue(.PrevEmploymentStatus))
				End If
				''end previous employment information

				Dim oxmlMonthlyIncome As XmlElement = CType(oxmlFinancialInfo.AppendChild(oxmlApplicant.OwnerDocument.CreateElement("MONTHLY_INCOME", NAMESPACE_URI)), XmlElement)
				If Common.SafeString(.GrossMonthlyIncome) <> "" Then
					oxmlMonthlyIncome.SetAttribute("monthly_income_base_salary", .GrossMonthlyIncome)
				End If
				' oxmlMonthlyIncome.SetAttribute("monthly_income_other_1", .GrossMonthlyIncomeOther)
				'oxmlMonthlyIncome.SetAttribute("monthly_income_other_description_1", .MonthlyIncomeOtherDescription.NullSafeToUpper_)
				'   oxmlMonthlyIncome.SetAttribute("is_tax_exempt_monthly_income_base_salary", .GrossMonthlyIncomeTaxExempt)
				' oxmlMonthlyIncome.SetAttribute("is_tax_exempt_income_other_1", .OtherMonthlyIncomeTaxExempt)

				If .OtherMonthlyIncomeDescription <> "" Then
					Dim oxmlOtherMonthlyIncome As XmlElement = CType(oxmlMonthlyIncome.AppendChild(oxmlApplicant.OwnerDocument.CreateElement("OTHER_INCOME", NAMESPACE_URI)), XmlElement)
					oxmlOtherMonthlyIncome.SetAttribute("monthly_income", .OtherMonthlyIncome)
					oxmlOtherMonthlyIncome.SetAttribute("monthly_income_description", .OtherMonthlyIncomeDescription)
					oxmlOtherMonthlyIncome.SetAttribute("is_tax_exempt", .OtherMonthlyIncomeTaxExempt)
				End If

				Dim oxmlMonthlyDebt As XmlElement = CType(oxmlFinancialInfo.AppendChild(oxmlApplicant.OwnerDocument.CreateElement("MONTHLY_DEBT", NAMESPACE_URI)), XmlElement)
				'    <MONTHLY_DEBT monthly_liability="803.58" monthly_housing_cost="1500.00" monthly_rent="1500.00" job_expense="0.00" alimony="3333.00" child_support="111.00" separate_maintenance_expense="111.00" tax_expense="0.00" other_expense_1="0.00" other_expense_2="0.00" monthly_mortgage_payment="0.00" aggregate_amount_with_lender="0.00" aggregate_amount_with_lender_secured_no_mortgage="0.00" />
				If BADeclaration IsNot Nothing AndAlso BADeclaration.HasAlimony = "Y" Then
					oxmlMonthlyDebt.SetAttribute("alimony", BADeclaration.AlimonyAmount)
					oxmlMonthlyDebt.SetAttribute("separate_maintenance_expense", BADeclaration.SeperateAmount)
					oxmlMonthlyDebt.SetAttribute("child_support", BADeclaration.ChildSupportAmount)
				End If

				If CEnum.MapOccupancyTypeToValue(.OccupyingLocation) = "BUYING" Or CEnum.MapOccupancyTypeToValue(.OccupyingLocation) = "OWN" Then
					oxmlMonthlyDebt.SetAttribute("monthly_mortgage_payment", .TotalMonthlyHousingExpense)
				Else
					oxmlMonthlyDebt.SetAttribute("monthly_housing_cost", .TotalMonthlyHousingExpense)
				End If
				If .HasReference = "Y" Then
					Dim oxmlPrimaryReferenceInfo As XmlElement = CType(oxmlApplicant.AppendChild(oxmlApplicant.OwnerDocument.CreateElement("REFERENCE", NAMESPACE_URI)), XmlElement)
					oxmlPrimaryReferenceInfo.SetAttribute("first_name", .PriRefFirstName)
					oxmlPrimaryReferenceInfo.SetAttribute("last_name", .PriRefLastName)
					oxmlPrimaryReferenceInfo.SetAttribute("phone", .PriRefPhone)
					oxmlPrimaryReferenceInfo.SetAttribute("email", .PriRefEmail)
					oxmlPrimaryReferenceInfo.SetAttribute("relationship", .PriRefRelationship)
					If .PriRefStreet <> "" Then
						oxmlPrimaryReferenceInfo.SetAttribute("address", .PriRefStreet)
						oxmlPrimaryReferenceInfo.SetAttribute("city", .PriRefCity)
						oxmlPrimaryReferenceInfo.SetAttribute("state", .PriRefState)
						oxmlPrimaryReferenceInfo.SetAttribute("zip", .PriRefZip)
					End If
					Dim oxmlSecondaryReferenceInfo As XmlElement = CType(oxmlApplicant.AppendChild(oxmlApplicant.OwnerDocument.CreateElement("REFERENCE", NAMESPACE_URI)), XmlElement)
					oxmlSecondaryReferenceInfo.SetAttribute("first_name", .SecRefFirstName)
					oxmlSecondaryReferenceInfo.SetAttribute("last_name", .SecRefLastName)
					oxmlSecondaryReferenceInfo.SetAttribute("phone", .SecRefPhone)
					oxmlSecondaryReferenceInfo.SetAttribute("email", .SecRefEmail)
					oxmlSecondaryReferenceInfo.SetAttribute("relationship", .SecRefRelationship)
					If .SecRefStreet <> "" Then
						oxmlSecondaryReferenceInfo.SetAttribute("address", .SecRefStreet)
						oxmlSecondaryReferenceInfo.SetAttribute("city", .SecRefCity)
						oxmlSecondaryReferenceInfo.SetAttribute("state", .SecRefState)
						oxmlSecondaryReferenceInfo.SetAttribute("zip", .SecRefZip)
					End If
				End If

				Dim oxmlContactInfo As XmlElement = CType(oxmlApplicant.AppendChild(oxmlApplicant.OwnerDocument.CreateElement("CONTACT_INFO", NAMESPACE_URI)), XmlElement)

				oxmlContactInfo.SetAttribute("home_phone", .ContactHomePhone)
				If .ContactHomePhoneCountry <> "" And .ContactHomePhoneCountry.NullSafeToUpper_ <> "US" Then
					oxmlContactInfo.SetAttribute("home_phone_country", .ContactHomePhoneCountry.NullSafeToUpper_)
					'oxmlContactInfo.SetAttribute("is_home_phone_foreign", "Y")
				End If

				oxmlContactInfo.SetAttribute("work_phone", .ContactWorkPhone)
				If .ContactWorkPhoneCountry <> "" And .ContactWorkPhoneCountry.NullSafeToUpper_ <> "US" Then
					oxmlContactInfo.SetAttribute("work_phone_country", .ContactWorkPhoneCountry.NullSafeToUpper_)
					'oxmlContactInfo.SetAttribute("is_work_phone_foreign", "Y")
				End If
				oxmlContactInfo.SetAttribute("work_phone_extension", .ContactWorkPhoneExt)

				oxmlContactInfo.SetAttribute("cell_phone", .ContactMobilePhone)
				If .ContactMobilePhoneCountry <> "" And .ContactMobilePhoneCountry.NullSafeToUpper_ <> "US" Then
					oxmlContactInfo.SetAttribute("cell_phone_country", .ContactMobilePhone.NullSafeToUpper_)
					'oxmlContactInfo.SetAttribute("is_cell_phone_foreign", "Y")
				End If
				oxmlContactInfo.SetAttribute("email", .ContactEmail.NullSafeToUpper_)
				''If .PreferredContactMethod <> "" Then oxmlContactInfo.SetAttribute("preferred_contact_method", .PreferredContactMethod)
				Dim oxmlAssets As XmlElement = CType(oxmlApplicant.AppendChild(oxmlApplicant.OwnerDocument.CreateElement("ASSETS", NAMESPACE_URI)), XmlElement)
				''declaration

				If BADeclaration IsNot Nothing Then
					Dim oxmlDeclaration As XmlElement = CType(oxmlApplicant.AppendChild(oxmlApplicant.OwnerDocument.CreateElement("DECLARATIONS", NAMESPACE_URI)), XmlElement)
					oxmlDeclaration.SetAttribute("has_judge_bankrupt_foreclosure", BADeclaration.HasBankruptForeclosure)
					oxmlDeclaration.SetAttribute("has_declare_bankrupt", BADeclaration.HasDeclaredBankruptcy)
					oxmlDeclaration.SetAttribute("has_chapter_13", BADeclaration.HasChapter13)
					oxmlDeclaration.SetAttribute("has_lawsuit_party", BADeclaration.HasLawsuitParty)
					oxmlDeclaration.SetAttribute("has_judgement", BADeclaration.HasJudgment)
					oxmlDeclaration.SetAttribute("has_reposession", BADeclaration.HasRepossession)
					oxmlDeclaration.SetAttribute("has_other_obligation", BADeclaration.HasOtherObligation)
					oxmlDeclaration.SetAttribute("has_past_due_bills", BADeclaration.HasPastDueBills)

					oxmlDeclaration.SetAttribute("has_co_maker", BADeclaration.HasCoMaker)
					If BADeclaration.HasCoMaker = "Y" Then
						oxmlDeclaration.SetAttribute("co_maker_primary_name", BADeclaration.PrimaryName)
						oxmlDeclaration.SetAttribute("co_maker_creditor", BADeclaration.CreditorName)
						oxmlDeclaration.SetAttribute("co_maker_amount", BADeclaration.Amount)
					End If
					oxmlDeclaration.SetAttribute("has_alias", BADeclaration.HasOtherCreditName)
					If BADeclaration.HasOtherCreditName = "Y" Then
						oxmlDeclaration.SetAttribute("alias", BADeclaration.OtherCreditName)
					End If
					oxmlDeclaration.SetAttribute("has_suits_pending", BADeclaration.HasSuitsPending)
					oxmlDeclaration.SetAttribute("has_income_decline", BADeclaration.HasIncomeDecline)
					oxmlDeclaration.SetAttribute("has_alimony", BADeclaration.HasAlimony)
					If BADeclaration.HasAlimony = "Y" Then
						oxmlDeclaration.SetAttribute("alimony_recipient", BADeclaration.RecipientName)
						oxmlDeclaration.SetAttribute("alimony_recipient_address", BADeclaration.RecipientAddress)
					End If
				End If
			End With
			Return oxmlApplicant
		End Function

		Protected Sub AddBusinessCustomQuestionsXml(ByVal poDoc As XmlDocument, ByVal oConfig As CWebsiteConfig)
			If BLCustomQuestionAnswers.Any() Then
				Dim oxmlCustomQuestions As XmlElement = CType(poDoc.DocumentElement.AppendChild(poDoc.CreateElement("CUSTOM_QUESTIONS", NAMESPACE_URI)), XmlElement)
				Dim oXmlCustomQuestion, oxmlQuestionAnswer As XmlElement

				For Each oQuestionAnswer As CValidatedQuestionAnswers In BLCustomQuestionAnswers
					If Not oQuestionAnswer.Answers.Any() Then Continue For

					oXmlCustomQuestion = getCustomQuestionXmlNode(oQuestionAnswer.Question.Name, oxmlCustomQuestions)
					oXmlCustomQuestion.SetAttribute("question_type", oQuestionAnswer.Question.AnswerType.NullSafeToUpper_)
					For Each oAnswerItem In oQuestionAnswer.Answers
						oxmlQuestionAnswer = CType(oXmlCustomQuestion.AppendChild(poDoc.CreateElement("CUSTOM_QUESTION_ANSWER", NAMESPACE_URI)), XmlElement)
						oxmlQuestionAnswer.SetAttribute("answer_text", oAnswerItem.AnswerText)
						oxmlQuestionAnswer.SetAttribute("answer_value", oAnswerItem.AnswerValue)
					Next
				Next
			End If
		End Sub

#End Region


	End Class
	<Serializable()>
	Public Class uploadDocumentInfo
		Public docTitle As String
		Public docGroup As String
		Public docCode As String
		Public docLoanType As String
		Public docFileName As String
	End Class
End Namespace