﻿Imports Microsoft.VisualBasic
Imports System.Xml

Public Structure CQualifiedProduct_CC
	''' <summary>
	''' The name of the qualified credit card
	''' </summary>
	Public CardName As String
	''' <summary>
	''' The default APR (or rate)
	''' </summary>
	Public DefaultApr As String
	''' <summary>
	''' The maximum amount the qualified card is approved for (or credit limit)
	''' </summary>
	Public MaxAmountApproved As String
	''' <summary>
	''' Used only in DecisionLoan 2.0
	''' </summary>
	Public UnderwritingServiceResultsIndex As String
	Public IsPrequalified As Boolean

	Public Shared Function CreateFrom_1_0(xEl As XmlElement) As CQualifiedProduct_CC
		Return New CQualifiedProduct_CC With {
			.CardName = xEl.GetAttribute("name"),
			.DefaultApr = xEl.GetAttribute("rate"),
			.MaxAmountApproved = xEl.GetAttribute("credit_limit"),
			.UnderwritingServiceResultsIndex = "",
			.IsPrequalified = False
		}
	End Function

	Public Shared Function CreateFrom_2_0(xEl As XmlElement, isPreQual As Boolean) As CQualifiedProduct_CC
		Return New CQualifiedProduct_CC With {
			.CardName = xEl.GetAttribute("card_name"),
			.DefaultApr = xEl.GetAttribute("default_apr"),
			.MaxAmountApproved = xEl.GetAttribute("max_amount_approved"),
			.UnderwritingServiceResultsIndex = xEl.GetAttribute("underwriting_service_results_index"),
			.IsPrequalified = isPreQual
		}
	End Function
End Structure

Public Structure CQualifiedProduct_HE_1_0

End Structure

Public Structure CQualifiedProduct_HE_2_0

End Structure

Public Structure CQualifiedProduct_PL_1_0

End Structure

Public Structure CQualifiedProduct_PL_2_0

End Structure

Public Structure CQualifiedProduct_VL_1_0

End Structure

Public Structure CQualifiedProduct_VL_2_0

End Structure