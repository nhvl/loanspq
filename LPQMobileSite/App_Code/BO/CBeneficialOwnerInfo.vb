﻿<Serializable()>
Public Class CBeneficialOwnerInfo
	Public Property BusinessOwned As Double
	Public Property HasControl As String
	Public Property RoleType As String
	Public Property FirstName As String
	Public Property MiddleName As String
	Public Property LastName As String
	Public Property NameSuffix As String
	Public Property CitizenshipStatus As String
	Public Property Ssn As String
	Public Property Dob As Date
	Public Property AddressStreet As String
	Public Property AddressZip As String
	Public Property AddressCity As String
    Public Property AddressState As String
    Public Property ControlTitle As String
End Class
