﻿
Imports LPQMobile.Utils
Imports System.Web
Imports System.IO
Imports System.Linq

Namespace LPQMobile.BO
	'deprecated, use verify address
    Public Class CZipCode
        Public ZipCode As Integer
        Public City As String
        Public State As String

        Public Sub New()            
        End Sub

        Private Shared ZipCodeData As New List(Of CZipCode)

        Private Sub InitialData()
            If ZipCodeData.Count = 0 Then
                Dim sPath As String = HttpContext.Current.Server.MapPath("~/doc/zipcode.txt")
                Dim oFile As String() = File.ReadAllLines(sPath)

                For Each row As String In oFile
                    Dim codeData As String() = row.Split("|".ToCharArray())
                    Dim oZipCode As New CZipCode()
                    oZipCode.ZipCode = Common.SafeInteger(codeData(0))
                    oZipCode.City = Common.SafeString(codeData(1))
                    oZipCode.State = Common.SafeString(codeData(2))

                    ZipCodeData.Add(oZipCode)
                Next

            End If
        End Sub

        Public Function LookupByZip(ByVal zip As Integer) As CZipCode
            If ZipCodeData.Count = 0 Then
                InitialData()
            End If
            Dim q = From item In ZipCodeData Where item.ZipCode.Equals(zip) Select item
            Return q.FirstOrDefault()
        End Function

    End Class

End Namespace

