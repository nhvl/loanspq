﻿Imports Microsoft.VisualBasic

Public Class CSSOCustomApprovalLimit
	Public Property CurrentAppType As String
	Public Property CurrentAppTypeCategory As String
	Public Property CustomApprovalLimitsValidationKeySuffix As String
	Public Property CustomApprovalLimitsValidationResult As String
	Public Property CustomApprovalTargetColumns As List(Of String)
	Public Property CustomApprovalApplicableColumns As Object
	Public Property CustomApprovalFailureReasons As List(Of String)
End Class
