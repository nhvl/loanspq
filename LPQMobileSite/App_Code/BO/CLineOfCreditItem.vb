﻿Imports Newtonsoft.Json.Converters
Imports Newtonsoft.Json

Public Class CLineOfCreditItem
	Public Property Text As String
	Public Property Value As String
	Public Property Category As String
	<JsonConverter(GetType(StringEnumConverter))>
	Public Property OverDraftOption As CEnum.LocOverDraftOption
End Class
