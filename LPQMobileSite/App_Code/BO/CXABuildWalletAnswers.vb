﻿Imports Microsoft.VisualBasic
Imports System.Xml
Imports LPQMobile.Utils
Imports System.Text

Public Class CXABuildWalletAnswers
	Implements IBuildWalletAnswers

	Private websiteConfig As CWebsiteConfig
	Private isJoint As Boolean
	Private loanID As String = ""
	Private transactionID As String = ""
	Private questionIDList As List(Of String)

	Public Sub New(websiteConfig As CWebsiteConfig, isJoint As Boolean, loanID As String, transactionID As String, questionIDList As List(Of String))
		Me.websiteConfig = websiteConfig
		Me.isJoint = isJoint
		Me.transactionID = transactionID
		Me.questionIDList = questionIDList
		Me.loanID = loanID
	End Sub

	Public Function buildWalletAnswerXMLEquifax(answerIDList As List(Of String)) As String Implements IBuildWalletAnswers.BuildWalletAnswerXMLEquifax
		Dim sb As New StringBuilder()
		Dim writer As XmlWriter = XmlTextWriter.Create(sb)
		writer.WriteStartDocument()

		' begin input
		writer.WriteStartElement("INPUT")

		writer.WriteStartElement("LOGIN")
		'      writer.WriteAttributeString("login", "quanntest")
		'writer.WriteAttributeString("password", "lpq123")
		writer.WriteAttributeString("api_user_id", websiteConfig.APIUser)
		writer.WriteAttributeString("api_password", websiteConfig.APIPassword)
		writer.WriteEndElement()
		writer.WriteStartElement("REQUEST")
		writer.WriteAttributeString("transaction_id", transactionID)
		writer.WriteAttributeString("app_id", loanID)
		writer.WriteAttributeString("applicant_index", "0")
		writer.WriteAttributeString("is_joint", IIf(isJoint, "true", "false").ToString())
		Dim numQuestions As Integer = questionIDList.Count
		writer.WriteStartElement("ANSWERS")
		For i As Integer = 0 To numQuestions - 1
			writer.WriteStartElement("ANSWER")
			writer.WriteAttributeString("question_number", questionIDList(i))
			writer.WriteAttributeString("answer_number", answerIDList(i))
			writer.WriteEndElement()
		Next
		' END </ANSWERS>
		writer.WriteEndElement()

		' END </REQUEST>
		writer.WriteEndElement()

		' END </INPUT>
		writer.WriteEndElement()
		writer.Flush()
		writer.Close()

		Dim returnStr As String = sb.ToString()

		Return returnStr
	End Function
	'
	Public Function buildWalletAnswerXMLRSA(questionSetID As String, answerIDList As List(Of String)) As String Implements IBuildWalletAnswers.BuildWalletAnswerXMLRSA
		Dim sb As New StringBuilder()
		Dim writer As XmlWriter = XmlTextWriter.Create(sb)
		writer.WriteStartDocument()
		' begin input
		writer.WriteStartElement("INPUT")
		writer.WriteStartElement("LOGIN")
		writer.WriteAttributeString("api_user_id", websiteConfig.APIUser)
		writer.WriteAttributeString("api_password", websiteConfig.APIPassword)
		writer.WriteEndElement()


		writer.WriteStartElement("REQUEST")
		writer.WriteAttributeString("transaction_id", transactionID)
		writer.WriteAttributeString("question_set_id", questionSetID)
		writer.WriteAttributeString("app_id", loanID)
		writer.WriteAttributeString("applicant_index", "0")
		writer.WriteAttributeString("is_joint", IIf(isJoint, "true", "false").ToString())
		Dim numQuestions As Integer = questionIDList.Count
		writer.WriteStartElement("ANSWERS")
		For i As Integer = 0 To numQuestions - 1
			writer.WriteStartElement("ANSWER")
			writer.WriteAttributeString("question_id", questionIDList(i))
			writer.WriteAttributeString("choice_id", answerIDList(i))
			writer.WriteEndElement()
		Next
		' END </ANSWERS>
		writer.WriteEndElement()

		' END </REQUEST>
		writer.WriteEndElement()

		' END </INPUT>
		writer.WriteEndElement()
		writer.Flush()
		writer.Close()

		Dim returnStr As String = sb.ToString()

		Return returnStr
	End Function

	Public Function buildWalletAnswerXMLPID(answerIDList As List(Of String)) As String Implements IBuildWalletAnswers.BuildWalletAnswerXMLPID
		Dim sb As New StringBuilder()
		Dim writer As XmlWriter = XmlTextWriter.Create(sb)
		writer.WriteStartDocument()

		' begin input
		writer.WriteStartElement("INPUT")

		writer.WriteStartElement("LOGIN")
		writer.WriteAttributeString("api_user_id", websiteConfig.APIUser)
		writer.WriteAttributeString("api_password", websiteConfig.APIPassword)
		writer.WriteEndElement()


		writer.WriteStartElement("REQUEST")
		writer.WriteAttributeString("app_id", loanID)

		writer.WriteAttributeString("applicant_index", "0")
		writer.WriteAttributeString("is_joint", IIf(isJoint, "true", "false").ToString())

		writer.WriteAttributeString("session_id", transactionID)

		Dim numQuestions As Integer = questionIDList.Count
		writer.WriteStartElement("ANSWERS")
		For i As Integer = 0 To numQuestions - 1
			writer.WriteStartElement("ANSWER")
			writer.WriteAttributeString("answer_text", answerIDList(i))
			writer.WriteEndElement()
		Next
		' END </ANSWERS>
		writer.WriteEndElement()

		' END </REQUEST>
		writer.WriteEndElement()

		' END </INPUT>
		writer.WriteEndElement()
		writer.Flush()
		writer.Close()

		Dim returnStr As String = sb.ToString()

		Return returnStr
	End Function

	Public Function buildWalletAnswerXMLFIS(questionSetID As String, answerIDList As List(Of String)) As String Implements IBuildWalletAnswers.BuildWalletAnswerXMLFIS
		Dim sb As New StringBuilder()
		Dim writer As XmlWriter = XmlTextWriter.Create(sb)
		writer.WriteStartDocument()

		' begin input
		writer.WriteStartElement("INPUT")

		writer.WriteStartElement("LOGIN")

		'FIS IDA ONLY work with lender level user
		If websiteConfig.APIUserLender <> "" Then
			writer.WriteAttributeString("api_user_id", websiteConfig.APIUserLender)
			writer.WriteAttributeString("api_password", websiteConfig.APIPasswordLender)
		Else
			writer.WriteAttributeString("api_user_id", websiteConfig.APIUser)
			writer.WriteAttributeString("api_password", websiteConfig.APIPassword)
		End If

		writer.WriteEndElement()


		writer.WriteStartElement("REQUEST")
		writer.WriteAttributeString("xpressappid", loanID)
		writer.WriteAttributeString("applicant_index", "0")
		writer.WriteAttributeString("applicant_type", IIf(isJoint, "JOINT", "PRIMARY").ToString())

		writer.WriteAttributeString("quiz_id", questionSetID)
		writer.WriteAttributeString("transaction_id", transactionID)

		Dim numQuestions As Integer = questionIDList.Count
		writer.WriteStartElement("ANSWERS")
		For i As Integer = 0 To numQuestions - 1
			writer.WriteStartElement("ANSWER")
			writer.WriteAttributeString("question_id", questionIDList(i))
			writer.WriteAttributeString("answer_id", answerIDList(i))
			writer.WriteEndElement()
		Next
		' END </ANSWERS>
		writer.WriteEndElement()

		' END </REQUEST>
		writer.WriteEndElement()

		' END </INPUT>
		writer.WriteEndElement()
		writer.Flush()
		writer.Close()

		Dim returnStr As String = sb.ToString()

		Return returnStr
	End Function

	Public Function buildWalletAnswerXMLDID(ByVal coAppFollowOnQuestion As Boolean, requestAppID As String, requestClientID As String, requestSequenceID As String, answerIDList As List(Of String)) As String Implements IBuildWalletAnswers.BuildWalletAnswerXMLDID
		Dim sb As New StringBuilder()
		Dim writer As XmlWriter = XmlTextWriter.Create(sb)
		writer.WriteStartDocument()

		' begin input
		writer.WriteStartElement("INPUT")

		writer.WriteStartElement("LOGIN")
		writer.WriteAttributeString("api_user_id", websiteConfig.APIUser)
		writer.WriteAttributeString("api_password", websiteConfig.APIPassword)
		writer.WriteEndElement()

		writer.WriteStartElement("REQUEST")
		writer.WriteAttributeString("xpress_app_id", loanID)
		writer.WriteAttributeString("applicant_index", "0")
		writer.WriteAttributeString("is_joint", IIf(isJoint, "true", "false").ToString())

		writer.WriteAttributeString("session_id", transactionID)
		writer.WriteAttributeString("request_app_id", requestAppID)
		writer.WriteAttributeString("request_client_id", requestClientID)
		writer.WriteAttributeString("request_sequence_id", requestSequenceID)

		Dim numQuestions As Integer = questionIDList.Count
		writer.WriteStartElement("ANSWERS")
		For i As Integer = 0 To numQuestions - 1
			writer.WriteStartElement("ANSWER")
			writer.WriteAttributeString("question_number", questionIDList(i))
			writer.WriteAttributeString("answer_text", answerIDList(i))
			writer.WriteEndElement()
		Next
		' END </ANSWERS>
		writer.WriteEndElement()

		' END </REQUEST>
		writer.WriteEndElement()

		' END </INPUT>
		writer.WriteEndElement()
		writer.Flush()
		writer.Close()

		Dim returnStr As String = sb.ToString()

		Return returnStr
	End Function

	''TransUnion build wallet answer
	Public Function buildWalletAnswerXMLTID(referenceNumber As String, answerIDList As List(Of String)) As String Implements IBuildWalletAnswers.BuildWalletAnswerXMLTID
		Dim sb As New StringBuilder()
		Dim writer As XmlWriter = XmlTextWriter.Create(sb)
		writer.WriteStartDocument()

		' begin input
		writer.WriteStartElement("INPUT")

		writer.WriteStartElement("LOGIN")
		writer.WriteAttributeString("api_user_id", websiteConfig.APIUser)
		writer.WriteAttributeString("api_password", websiteConfig.APIPassword)
		writer.WriteEndElement()
		writer.WriteStartElement("REQUEST")
		writer.WriteAttributeString("xpressappid", loanID)
		writer.WriteAttributeString("applicant_index", "0")
		writer.WriteAttributeString("mode", "IDA")
		writer.WriteAttributeString("reference_number", referenceNumber)
		writer.WriteAttributeString("applicant_type", IIf(isJoint, "JOINT", "PRIMARY").ToString())

		Dim numQuestions As Integer = questionIDList.Count
		writer.WriteStartElement("ANSWERS")
		For i As Integer = 0 To numQuestions - 1
			writer.WriteStartElement("ANSWER")
			writer.WriteAttributeString("question_name", questionIDList(i))
			writer.WriteAttributeString("choice_key", answerIDList(i))
			writer.WriteEndElement()
		Next
		' END </ANSWERS>
		writer.WriteEndElement()

		' END </REQUEST>
		writer.WriteEndElement()

		' END </INPUT>
		writer.WriteEndElement()
		writer.Flush()
		writer.Close()

		Dim returnStr As String = sb.ToString()

		Return returnStr
	End Function
End Class
