﻿Imports Microsoft.VisualBasic
Imports LPQMobile.Utils

Public Class CBAGetWalletQuestions
	Inherits CSAGetWalletQuestions

	Public Sub New(authenticationMethod As String, websiteConfig As CWebsiteConfig, isJoint As Boolean, loanID As String, applicantIndex As Integer)
		MyBase.New(authenticationMethod, websiteConfig, isJoint, loanID, applicantIndex)
		'for now, there is no different between BA and SA. This class is for later use. To prevent duplated code, I make CBAGetWalletQuestions inherits from CSAGetWalletQuestions
	End Sub
End Class
