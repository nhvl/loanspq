﻿Imports Microsoft.VisualBasic
Imports System.Xml
Imports System.Linq
Imports LPQMobile.Utils.Common
Imports System.Web.Script.Serialization
<Serializable()> _
Public Class CProductCustomQuestion
	Public AnswerType As String
	Public AppSource As String
	Public IsActive As Boolean
	Public IsAvailableToConsumer As Boolean
	Public IsRequired As Boolean

	Public IsViewableUntilLocked As Boolean
	Public Position As Integer
	Public QuestionName As String
	Public QuestionText As String
    Public RequireConfirmation As Boolean
    Public ConfirmationText As String
    Public max_value As String
    Public min_value As String
    Public reg_expression As String
    Public errorMessage As String
	Public XAProductQuestionID As String
	Private _RestrictedAnswers As List(Of CProductRestrictedAnswer)
	Public Property RestrictedAnswers() As List(Of CProductRestrictedAnswer)
		Get
			If _RestrictedAnswers Is Nothing Then
				_RestrictedAnswers = New List(Of CProductRestrictedAnswer)
			End If
			Return _RestrictedAnswers
		End Get
		Set(ByVal value As List(Of CProductRestrictedAnswer))
			_RestrictedAnswers = value
		End Set
	End Property

	Public productAnswerText As String
	Public numberIdx As Integer

	Public Sub New()

	End Sub

	Public Sub New(ByVal oQuestionNode As XmlElement)
		Me.AnswerType = oQuestionNode.GetAttribute("answer_type")
		Me.AppSource = oQuestionNode.GetAttribute("app_source")
		Me.IsActive = Convert.ToBoolean(oQuestionNode.GetAttribute("is_active"))
		Me.IsAvailableToConsumer = Convert.ToBoolean(oQuestionNode.GetAttribute("is_available_to_consumer"))
		Me.IsRequired = Convert.ToBoolean(oQuestionNode.GetAttribute("is_required"))

		Me.IsViewableUntilLocked = Convert.ToBoolean(oQuestionNode.GetAttribute("is_viewable_until_locked"))
		Me.Position = SafeInteger(oQuestionNode.GetAttribute("position"))
        Me.QuestionName = oQuestionNode.GetAttribute("question_name")
        Me.QuestionText = oQuestionNode.GetAttribute("question_text")
		Me.RequireConfirmation = Convert.ToBoolean(oQuestionNode.GetAttribute("require_confirmation"))
        Me.ConfirmationText = oQuestionNode.GetAttribute("confirmation_text")
        Me.XAProductQuestionID = oQuestionNode.GetAttribute("xa_product_question_id")
		Me.productAnswerText = ""
		Me.numberIdx = 0
        Me.max_value = oQuestionNode.GetAttribute("max_value")
        Me.min_value = oQuestionNode.GetAttribute("min_value")
        Me.reg_expression = oQuestionNode.GetAttribute("regular_expression")
        If Not String.IsNullOrEmpty(Me.reg_expression) Then
            Me.reg_expression = New JavaScriptSerializer().Serialize(Me.reg_expression)
        End If
        Me.errorMessage = oQuestionNode.GetAttribute("error_message")
		For Each oItem As XmlElement In oQuestionNode.SelectNodes("RESTRICTED_ANSWERS/RESTRICTED_ANSWER")
			Me.RestrictedAnswers.Add(New CProductRestrictedAnswer(oItem))
		Next
		Me.RestrictedAnswers = Me.RestrictedAnswers.OrderBy(Function(o) o.Position).ToList()

	End Sub


End Class
