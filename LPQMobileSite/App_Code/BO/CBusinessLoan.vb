﻿Public Class CBusinessLoan
    Public VehiclePreApprovedMessage As String
    Public CardPreApprovedMessage As String
    Public OtherPreApprovedMessage As String
    Public VehicleSubmitteddMessage As String
    Public CardSubmittedMessage As String
    Public OtherSubmittedMessage As String
    Public VehicleDisclosures As List(Of String)
    Public CardDisclosures As List(Of String)
    Public OtherDisclosures As List(Of String)
    Public VehiclePurposes As Dictionary(Of String, String)
    Public CardPurposes As Dictionary(Of String, String)
    Public OtherPurposes As Dictionary(Of String, CTextValueCatItem)
    Public CreditCardTypes As Dictionary(Of String, String)
    Public CreditCardNames As New List(Of Tuple(Of String, String, String, String))
    Public EnableCCDeclaration As Boolean
    Public EnableOTDeclaration As Boolean
    Public EnableVLDeclaration As Boolean
End Class
