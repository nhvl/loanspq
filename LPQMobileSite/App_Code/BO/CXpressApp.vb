﻿Imports Microsoft.VisualBasic
Imports System.Xml
Imports LPQMobile.Utils.Common
Imports LPQMobile.Utils
Imports System.Web.Script.Serialization
Imports System.Web

Namespace LPQMobile.BO

	'TODO: use this for xa/callback
	Public Class CXpressApp
		Inherits CBaseLoanApp

		Public products As List(Of CProduct)
		Public IsDecisionXAEnable As Boolean = False
		Public Funding As CFundingSourceInfo
		Public FundType As String
		'Public DepositAmount As String 'move to product level
		Public ComboComment As String 'loan type & loan number that this xa is for + if consumer want to process with the app

		''XA specific
		'Public oAccountsList As List(Of AccountInfo)

        'FOM
        Public FOMQName As String
		Public FOMIdx As String
        Public FOMQuestionsAndAnswers As String
        Public FOMAnswers As String
        ''Public FOMNQAnswers As String
        ''Public FOMNQItems As String
		'END FOM

		
		Public Overrides ReadOnly Property LoanType() As String
			Get
				Return "XA"
			End Get
		End Property

		Public Sub New(ByVal psOrganizationId As String, ByVal psLenderId As String)
			MyBase.New(psOrganizationId, psLenderId)
		End Sub

		Public Overrides Function GetXml(oConfig As CWebsiteConfig) As XmlDocument
			Dim oDoc As New XmlDocument()
            oDoc.LoadXml("<XPRESS_LOAN xmlns=""" & NAMESPACE_URI &
                         """ xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" " &
                         "xsi:schemaLocation=""http://www.meridianlink.com/CLF personal_loan.xsd http://www.meridianlink.com/CLF Integration\SSFCU.xsd"" " &
                         "version=""5.181"" />") ''5.203, 5.070

            AddApplicantsXml(oDoc, oConfig)

			'AddLoanStatusXml(oDoc)

			AddCommentsXml(oDoc)
			AddLoanInfoXml(oDoc)
			AddCustomQuestionsXml(oDoc)
            AddSystemXml(oDoc)
			AddAccountXml(oDoc, oConfig)
			'TODO:
            AddFundingSourceXml(oDoc) 'only for new member
            AddFOMFeesXML(oDoc, oConfig)
			AddFomXml(oDoc, oConfig)


			Return oDoc
		End Function

		Private Sub AddLoanInfoXml(ByVal poDoc As XmlDocument)
			Dim oxmlLoanInfo As XmlElement = CType(poDoc.DocumentElement.AppendChild(poDoc.CreateElement("LOAN_INFO", NAMESPACE_URI)), XmlElement)
			oxmlLoanInfo.SetAttribute("status", "REFERRED")
			'writer.WriteValue(HttpContext.Current.Session("StartedDate"))
			'oxmlLoanInfo.SetAttribute("create_date", XmlConvert.ToString(Date.Now, XmlDateTimeSerializationMode.Local))
			oxmlLoanInfo.SetAttribute("create_date", XmlConvert.ToString(HttpContext.Current.Session("StartedDate"), XmlDateTimeSerializationMode.Local))
			oxmlLoanInfo.SetAttribute("submit_date", XmlConvert.ToString(Date.Now, XmlDateTimeSerializationMode.Local))
            oxmlLoanInfo.SetAttribute("account_position", "1") 'not a member
            oxmlLoanInfo.SetAttribute("is_new_customer", "Y")
		End Sub

		Private Sub AddFundingXml(ByVal poDoc As XmlDocument)
			Dim oxmlFunding As XmlElement = CType(poDoc.DocumentElement.AppendChild(poDoc.CreateElement("FUNDING", NAMESPACE_URI)), XmlElement)
			oxmlFunding.SetAttribute("payment_method", "MIN PAYMENT")
			oxmlFunding.AppendChild(poDoc.CreateElement("INSURANCE_OPTIONS", NAMESPACE_URI))
        End Sub

        Private Sub AddFOMFeesXML(ByVal poDoc As XmlDocument, oConfig As CWebsiteConfig)
            Dim customListFomFees As Dictionary(Of String, String) = Common.GetFomFeesFromCustomList(oConfig)
            If Common.SafeDouble(oConfig.MembershipFee) > 0 Or customListFomFees.Count > 0 Then
                Dim oxmlMembershipFee As XmlElement = CType(poDoc.DocumentElement.AppendChild(poDoc.CreateElement("MEMBERSHIP_FEE", NAMESPACE_URI)), XmlElement)
                Dim fundingFOMFee As Double = 0D
                Dim fundingBaseFee As Double = Common.SafeDouble(oConfig.MembershipFee)
                If Not String.IsNullOrEmpty(FOMQName) Then
                    If customListFomFees.ContainsKey(FOMQName) Then
                        fundingFOMFee = Common.SafeDouble(customListFomFees(FOMQName))
                    End If
                End If
                Dim totalFundingAmount As Double = fundingBaseFee + fundingFOMFee
                ''adding membership feed
                oxmlMembershipFee.SetAttribute("membership_fee_funding_manual_fee", totalFundingAmount)
                oxmlMembershipFee.SetAttribute("membership_fee_funding_base_fee", fundingBaseFee)
                oxmlMembershipFee.SetAttribute("membership_fee_funding_fom_fee", fundingFOMFee)
                oxmlMembershipFee.SetAttribute("is_manual_membership_fee", "Y")
                Dim sFundingType = Funding.ValueFromType(Funding.fsFundingType)
                If sFundingType <> "" Then
                    If sFundingType = "CASH" Then
                        oxmlMembershipFee.SetAttribute("membership_fee_funding_source_id", "0")
                    ElseIf sFundingType = "MAIL" Then
                        oxmlMembershipFee.SetAttribute("membership_fee_funding_source_id", "1")
                    ElseIf sFundingType = "NOT_FUNDING" Then
                        oxmlMembershipFee.SetAttribute("membership_fee_funding_source_id", "2")

                        'source_id=3 will be used for all these types, only one will have source_id, other will have null for source id
                    ElseIf sFundingType = "PAYPAL" Then
                        oxmlMembershipFee.SetAttribute("membership_fee_funding_source_id", "3") '??
                    ElseIf sFundingType = "BANK" Then
                        oxmlMembershipFee.SetAttribute("membership_fee_funding_source_id", "3") ' ACH 
                    ElseIf sFundingType = "INTERNALTRANSFER" Then
                        oxmlMembershipFee.SetAttribute("membership_fee_funding_source_id", "3") ''this doesn't work , probably doesn't matter bc new member has no internal accoutn
                    ElseIf sFundingType = "CREDITCARD" Then
                        oxmlMembershipFee.SetAttribute("membership_fee_funding_source_id", "3")
                    Else
                        oxmlMembershipFee.SetAttribute("membership_fee_funding_source_id", "10")  '??
                    End If
                End If
            End If
        End Sub
		Private Sub AddFundingSourceXml(ByVal poDoc As XmlDocument)

			Dim oxmlFundingSources As XmlElement = CType(poDoc.DocumentElement.AppendChild(poDoc.CreateElement("FUNDING_SOURCES", NAMESPACE_URI)), XmlElement)
			If Funding.fsFundingType <> "" Then
				Dim oxmlFundingSource As XmlElement = CType(oxmlFundingSources.AppendChild(poDoc.CreateElement("FUNDING_SOURCE", NAMESPACE_URI)), XmlElement)
				If Funding.ValueFromType(Funding.fsFundingType) = "CASH" Then
					oxmlFundingSource.SetAttribute("clf_funding_source_id", "0")
				ElseIf Funding.ValueFromType(Funding.fsFundingType) = "MAIL" Then
					oxmlFundingSource.SetAttribute("clf_funding_source_id", "1")
				ElseIf Funding.ValueFromType(Funding.fsFundingType) = "NOT_FUNDING" Then
					oxmlFundingSource.SetAttribute("clf_funding_source_id", "2")
				ElseIf Funding.ValueFromType(Funding.fsFundingType) = "INTERNALTRANSFER" Then
					oxmlFundingSource.SetAttribute("clf_funding_source_id", "3")
				ElseIf Funding.ValueFromType(Funding.fsFundingType) = "BANK" Then
					oxmlFundingSource.SetAttribute("clf_funding_source_id", "3")
				ElseIf Funding.ValueFromType(Funding.fsFundingType) = "CREDITCARD" Then
					oxmlFundingSource.SetAttribute("clf_funding_source_id", "3")
				ElseIf Funding.ValueFromType(Funding.fsFundingType) = "PAYPAL" Then
					oxmlFundingSource.SetAttribute("clf_funding_source_id", "3")
				Else
					oxmlFundingSource.SetAttribute("clf_funding_source_id", "10")
				End If

				oxmlFundingSource.SetAttribute("funding_type", Funding.ValueFromType(Funding.fsFundingType))


				Select Case Funding.fsFundingType
					Case "CREDIT CARD"
						oxmlFundingSource.SetAttribute("name_on_card", Funding.cNameOnCard)
						oxmlFundingSource.SetAttribute("cc_card_type", Funding.cCardType)
						oxmlFundingSource.SetAttribute("cc_card_number", Funding.cCreditCardNumber)
						oxmlFundingSource.SetAttribute("cc_card_exp_date", String.Format("{0}-{1}-01", Funding.cExpirationDate.Substring(3), Funding.cExpirationDate.Substring(0, 2)))
                        ''cc_cvn_number does not exist in new clf version
                        '' oxmlFundingSource.SetAttribute("cc_cvn_number", Funding.cCVNNumber)
						oxmlFundingSource.SetAttribute("billing_street_address", Funding.cBillingAddress)
						oxmlFundingSource.SetAttribute("billing_city", Funding.cBillingCity)
						oxmlFundingSource.SetAttribute("billing_state", Funding.cBillingState)
						oxmlFundingSource.SetAttribute("billing_zip", Funding.cBillingZip)
					Case "TRANSFER FROM ANOTHER FINANCIAL INSTITUTION"
						oxmlFundingSource.SetAttribute("routing_number", Funding.bRoutingNumber)
						oxmlFundingSource.SetAttribute("bank_name_on_card", Funding.bNameOnCard)
						oxmlFundingSource.SetAttribute("bank_account_number", Funding.bAccountNumber)
						oxmlFundingSource.SetAttribute("bank_bankstate", Funding.bBankState)
						oxmlFundingSource.SetAttribute("bank_bankname", Funding.bBankName)
						oxmlFundingSource.SetAttribute("bank_account_type", Funding.bAccountType)

					Case "INTERNAL TRANSFER"
						oxmlFundingSource.SetAttribute("transfer_account_number", Funding.tAccountNumber)
                        oxmlFundingSource.SetAttribute("transfer_account_type", Funding.ValueFromTransferType(Funding.tAccountType))
                End Select

			End If

		End Sub


		Private Sub AddAccountXml(ByVal poDoc As XmlDocument, oConfig As CWebsiteConfig)
			'decisionXA will underwrite and auto add approved account
			If Not IsDecisionXAEnable Then
				Dim oxmlApprovedAccounts As XmlElement = CType(poDoc.DocumentElement.AppendChild(poDoc.CreateElement("APPROVED_ACCOUNTS", NAMESPACE_URI)), XmlElement)
				If products IsNot Nothing And products.Count > 0 Then
					For Each prod As CProduct In products
						Dim oxmlApprovedAccount As XmlElement = CType(oxmlApprovedAccounts.AppendChild(poDoc.CreateElement("ACCOUNT_TYPE", NAMESPACE_URI)), XmlElement)
						AddAccount(poDoc, oxmlApprovedAccount, True, prod, oConfig)
					Next
				End If
			End If
			'interested account
			Dim oxmlInterestedAccounts As XmlElement = CType(poDoc.DocumentElement.AppendChild(poDoc.CreateElement("INTERESTED_ACCOUNTS", NAMESPACE_URI)), XmlElement)
			If products IsNot Nothing And products.Count > 0 Then
				For Each prod As CProduct In products
					Dim oxmlInterestedAccount As XmlElement = CType(oxmlInterestedAccounts.AppendChild(poDoc.CreateElement("ACCOUNT_TYPE", NAMESPACE_URI)), XmlElement)
					AddAccount(poDoc, oxmlInterestedAccount, False, prod, oConfig)
				Next
			End If
		End Sub
		Private Sub AddAccount(ByVal poDoc As XmlDocument, ByVal poAccounts As XmlElement, ByVal pbIsApprovedAccount As Boolean, Product As CProduct, websiteConfig As CWebsiteConfig)

			poAccounts.SetAttribute("account_name", Product.AccountName)
			poAccounts.SetAttribute("product_code", Product.ProductCode)
			poAccounts.SetAttribute("account_type", Product.AccountType)
			poAccounts.SetAttribute("amount_deposit", Product.DepositeAmount)


            If FundType <> "" And pbIsApprovedAccount And Product.cannotBeFunded <> "Y" Then  'only need this for new member , non secured card
                If FundType = "CASH" Then
                    poAccounts.SetAttribute("clf_funding_source_id", "0")
                ElseIf FundType = "MAIL" Then
                    poAccounts.SetAttribute("clf_funding_source_id", "1")
                ElseIf FundType = "NOT_FUNDING" Then
                    poAccounts.SetAttribute("clf_funding_source_id", "2")
                ElseIf FundType = "PAYPAL" Then
                    poAccounts.SetAttribute("clf_funding_source_id", "3")
                ElseIf FundType = "INTERNALTRANSFER" Then
                    poAccounts.SetAttribute("clf_funding_source_id", "3")
                ElseIf FundType = "BANK" Then
                    poAccounts.SetAttribute("clf_funding_source_id", "3")
                ElseIf FundType = "CREDITCARD" Then
                    poAccounts.SetAttribute("clf_funding_source_id", "3")
                Else
                    poAccounts.SetAttribute("clf_funding_source_id", "10")
                End If
            End If

			'writer.WriteAttributeString("IS_REQUIRED", matchingAccountNode.Attributes("is_required").InnerXml)
			Dim selectedRate As CProductRate = Nothing
			If Product.AvailableRates IsNot Nothing AndAlso Product.AvailableRates.Any() Then
				selectedRate = Product.AvailableRates.FirstOrDefault(Function(r) r.RateCode = Product.SelectedRate)
			End If
			If selectedRate Is Nothing Then
				Dim writingRate As String = "-999999"
				Dim writingTerm As String = "-16959"
				Dim writingApy As String = "-999999"
				If Not String.IsNullOrEmpty(Product.Rate) AndAlso Product.Rate <> "-999999" Then
					writingRate = FormatNumber(Product.Rate, 4).Replace(",", "")
				End If

				If Not String.IsNullOrEmpty(Product.Apy) AndAlso Product.Apy <> "-999999" Then
					writingApy = FormatNumber(Product.Apy, 4).Replace(",", "")
				End If
				If Not String.IsNullOrEmpty(Product.Term) Then writingTerm = Product.Term
				poAccounts.SetAttribute("rate", writingRate)
				poAccounts.SetAttribute("apy", writingApy)
				poAccounts.SetAttribute("term", writingTerm)
			Else
				Dim writingRate As String = "0"
				Dim writingTerm As String = "-16959"
				' If the RateString is empty, send a 0 instead to avoid CLF errors.
				If Not String.IsNullOrEmpty(selectedRate.RateString) Then
					writingRate = selectedRate.RateString
				End If
				poAccounts.SetAttribute("rate", writingRate)
				poAccounts.SetAttribute("rate_code", selectedRate.RateCode)
				If Not pbIsApprovedAccount Then
					poAccounts.SetAttribute("min_deposit", Common.SafeDouble(selectedRate.MinDeposit).ToString())
				End If
				poAccounts.SetAttribute("apy", FormatNumber(Product.Apy, 4).Replace(",", ""))
				poAccounts.SetAttribute("term_type", Product.TermType)
				If Not String.IsNullOrEmpty(Product.Term) Then
					writingTerm = Product.Term
				End If
				poAccounts.SetAttribute("term", writingTerm)
			End If
			If Product.Services IsNot Nothing AndAlso Product.Services.Count > 0 Then
				Dim oxmlServices As XmlElement = CType(poAccounts.AppendChild(poDoc.CreateElement("SERVICES", NAMESPACE_URI)), XmlElement)
				For Each serv In Product.Services
					Dim oxmlService As XmlElement = CType(oxmlServices.AppendChild(poDoc.CreateElement("SERVICE", NAMESPACE_URI)), XmlElement)
					oxmlService.SetAttribute("description", serv.Description)
					oxmlService.SetAttribute("service_code", serv.ServiceCode)
					oxmlService.SetAttribute("service_type", serv.ServiceType)
				Next
			End If

			'' if has Joint Applicant and product level ownership-> add acount_type_relations in account type
			Dim hasOnwershipLevel As String = Common.hasOwnerShipLevel(websiteConfig)
			Dim sCoSSN As String = ""
			If Applicants(0).HasSpouse Then
				sCoSSN = Applicants(1).SSN
			End If
			If sCoSSN <> "" And pbIsApprovedAccount And hasOnwershipLevel = "Y" Then
				Dim oxmlAccountTypeRelation As XmlElement = CType(poAccounts.AppendChild(poDoc.CreateElement("ACCOUNT_TYPE_RELATIONS", NAMESPACE_URI)), XmlElement)
				Dim oxmlRelation As XmlElement = CType(oxmlAccountTypeRelation.AppendChild(poDoc.CreateElement("RELATION", NAMESPACE_URI)), XmlElement)
				oxmlRelation.SetAttribute("ssn", sCoSSN)
				If hasOnwershipLevel = "Y" Then
					oxmlRelation.SetAttribute("benefactor_type", "J")
				End If
			End If

			If Product.CustomQuestions IsNot Nothing AndAlso Product.CustomQuestions.Count > 0 Then
				Dim oxmlProductCustomQuestions As XmlElement
				If pbIsApprovedAccount Then
					oxmlProductCustomQuestions = CType(poAccounts.AppendChild(poDoc.CreateElement("PRODUCT_CUSTOM_QUESTIONS_APPROVED", NAMESPACE_URI)), XmlElement)
				Else
					oxmlProductCustomQuestions = CType(poAccounts.AppendChild(poDoc.CreateElement("PRODUCT_CUSTOM_QUESTIONS_INTERESTED", NAMESPACE_URI)), XmlElement)
				End If

				For Each question As CProductCustomQuestion In Product.CustomQuestions
					Dim oxmlProductCustomQuestion As XmlElement = CType(oxmlProductCustomQuestions.AppendChild(poDoc.CreateElement("CUSTOM_QUESTION", NAMESPACE_URI)), XmlElement)
					oxmlProductCustomQuestion.SetAttribute("question_name", question.QuestionName)
					oxmlProductCustomQuestion.SetAttribute("question_type", question.AnswerType)
					If String.Equals(question.AnswerType, "CHECKBOX") OrElse String.Equals(question.AnswerType, "DROPDOWN") Then
						For Each ans As CProductRestrictedAnswer In question.RestrictedAnswers
							If ans.isSelected Then
								Dim oxmlProductCustomQuestionAnswwer As XmlElement = CType(oxmlProductCustomQuestion.AppendChild(poDoc.CreateElement("CUSTOM_QUESTION_ANSWER", NAMESPACE_URI)), XmlElement)
								oxmlProductCustomQuestionAnswwer.SetAttribute("answer_text", ans.Text)
								oxmlProductCustomQuestionAnswwer.SetAttribute("answer_value", ans.Value)
								'' check box can have one or more answer -> need to check all the answers
								'' Exit For  ->correct only for dropdown (not for checkbox)
							End If
						Next
					Else
						Dim oxmlProductCustomQuestionAnswwer As XmlElement = CType(oxmlProductCustomQuestion.AppendChild(poDoc.CreateElement("CUSTOM_QUESTION_ANSWER", NAMESPACE_URI)), XmlElement)
						oxmlProductCustomQuestionAnswwer.SetAttribute("answer_text", question.productAnswerText)
						oxmlProductCustomQuestionAnswwer.SetAttribute("answer_value", question.productAnswerText)
					End If
				Next
			End If

		End Sub

		Private Sub AddCommentsXml(ByVal poDoc As XmlDocument)
			Dim oxmlComments As XmlElement = CType(poDoc.DocumentElement.AppendChild(poDoc.CreateElement("COMMENTS", NAMESPACE_URI)), XmlElement)
			Dim oxmlDecisionComments As XmlElement = CType(oxmlComments.AppendChild(poDoc.CreateElement("DECISION_COMMENTS", NAMESPACE_URI)), XmlElement)
			Dim oxmlExternalComments As XmlElement = oxmlComments.AppendChild(poDoc.CreateElement("EXTERNAL_COMMENTS", NAMESPACE_URI))
			Dim oxmlInternalComments As XmlElement = CType(oxmlComments.AppendChild(poDoc.CreateElement("INTERNAL_COMMENTS", NAMESPACE_URI)), XmlElement)
			If HasDebtCancellation Then
				oxmlExternalComments.InnerText = HasDebtCancellationText
			End If
			''''write internal comment element:  disclosures and originating ip address
			oxmlInternalComments.InnerText += CurrentEmail + " (" + DateTime.Now + "): Disclosure(s) Checked: " + Disclosure
			oxmlInternalComments.InnerText += Environment.NewLine
			oxmlInternalComments.InnerText += "SYSTEM (" + DateTime.Now + "): Originating IP Address: " + IPAddress
			oxmlInternalComments.InnerText += Environment.NewLine
			oxmlInternalComments.InnerText += "SYSTEM (" + DateTime.Now + "): USER AGENT: " + UserAgent
			oxmlInternalComments.InnerText += Environment.NewLine
			If ComboComment <> "" Then
				oxmlInternalComments.InnerText += "SYSTEM (" + DateTime.Now + "): Combo Application for " & ComboComment
			Else
				oxmlInternalComments.InnerText += "SYSTEM (" + DateTime.Now + "): Combo Application "
            End If
            oxmlInternalComments.InnerText += Environment.NewLine
            ''add highRiskConsumerMessage to internal comment for xaCombo
            If BadMemberComment <> "" Then
                oxmlInternalComments.InnerText += "SYSTEM (" + DateTime.Now + "): " + BadMemberComment
            End If
			oxmlInternalComments.InnerText += Environment.NewLine
		End Sub
        Private Sub AddFomXml(ByVal poDoc As XmlDocument, websiteConfig As CWebsiteConfig)  'TODO:map fom from client
            Dim oFOMQAs As New List(Of Dictionary(Of String, String))
            If Not String.IsNullOrEmpty(FOMQuestionsAndAnswers) Then
                oFOMQAs = (New JavaScriptSerializer()).Deserialize(Of List(Of Dictionary(Of String, String)))(FOMQuestionsAndAnswers)
            End If
			If oFOMQAs IsNot Nothing AndAlso oFOMQAs.Any() Then
				'' begin fom question section
				Dim oxmlFOMAnswerQuestions As XmlElement = CType(poDoc.DocumentElement.AppendChild(poDoc.CreateElement("FOM_ANSWERED_QUESTIONS", NAMESPACE_URI)), XmlElement)
				For Each FOMQAIndexed As IndexedItem(Of Dictionary(Of String, String)) In oFOMQAs.IndexedItems()
					Dim FOMQA As Dictionary(Of String, String) = FOMQAIndexed.Item
					Dim oFQs As New List(Of CFOMQuestion)
					Dim sFQName = FOMQA.Item("FOMName")
					Dim listFomAnswers = (New JavaScriptSerializer()).Deserialize(Of List(Of String))(FOMQA.Item("FOMAnswers"))
					If FOMQAIndexed.Index = 0 Then ''start question
						oFQs = CFOMQuestion.CurrentFOMQuestions(websiteConfig).Where(Function(FQ) FQ.Name IsNot Nothing AndAlso FQ.Name.Equals(sFQName, StringComparison.OrdinalIgnoreCase)).ToList()
					Else ''next question
						oFQs = CFOMQuestion.DownloadFomNextQuestions(websiteConfig).Where(Function(FQ) FQ.Name IsNot Nothing AndAlso FQ.Name.Equals(sFQName, StringComparison.OrdinalIgnoreCase)).ToList()
					End If

					Dim oFQ As CFOMQuestion = oFQs.FirstOrDefault()
					If oFQ Is Nothing Then
						Exit For
					End If

					Dim oxmlFOMAnswerQuestion As XmlElement = CType(oxmlFOMAnswerQuestions.AppendChild(poDoc.CreateElement("FOM_ANSWERED_QUESTION", NAMESPACE_URI)), XmlElement)
					oxmlFOMAnswerQuestion.SetAttribute("name", oFQ.Name)
					Dim oxmlTextTemplate As XmlElement = CType(oxmlFOMAnswerQuestion.AppendChild(poDoc.CreateElement("text_template", NAMESPACE_URI)), XmlElement)
					oxmlTextTemplate.InnerXml = HttpUtility.HtmlEncode(oFQ.TextTemplate)
					Dim oxmlFOMAnswers As XmlElement = CType(oxmlFOMAnswerQuestion.AppendChild(poDoc.CreateElement("FOM_ANSWERS", NAMESPACE_URI)), XmlElement)
					For Each _fieldIndexed As IndexedItem(Of CFOMQuestionField) In oFQ.Fields.IndexedItems()
						Dim _field As CFOMQuestionField = _fieldIndexed.Item
						Dim oxmlFOMAnswer As XmlElement = CType(oxmlFOMAnswers.AppendChild(poDoc.CreateElement("FOM_ANSWER", NAMESPACE_URI)), XmlElement)
						oxmlFOMAnswer.SetAttribute("field_type", _field.Type.ToUpper())
						Dim _val As String = IIf(listFomAnswers.Count > _fieldIndexed.Index, listFomAnswers(_fieldIndexed.Index), "")
						Dim _text As String = _val
						Dim oxmlValue As XmlElement = CType(oxmlFOMAnswer.AppendChild(poDoc.CreateElement("value", NAMESPACE_URI)), XmlElement)
						oxmlValue.InnerXml = _val
						If "DROPDOWN".Equals(_field.Type, StringComparison.OrdinalIgnoreCase) AndAlso _field.Answers.Any() Then
							For Each _ans As CFOMQuestionAnswer In _field.Answers
								If _ans.Value.Equals(_val) Then
									_text = _ans.Text
									Exit For
								End If
							Next
						End If

						Dim oxmlText As XmlElement = CType(oxmlFOMAnswer.AppendChild(poDoc.CreateElement("text", NAMESPACE_URI)), XmlElement)
						oxmlText.InnerText = _text ' Set InnerText instead of InnerXml, since it would cause errors if _text contains & (ampersand_ character)
					Next
				Next
			End If
		End Sub
     End Class

End Namespace