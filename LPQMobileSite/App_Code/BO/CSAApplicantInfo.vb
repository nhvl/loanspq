﻿
<Serializable()>
Public Class CSAApplicantInfo
    Public Property AllowDebit As Boolean
    Public Property AllowOfac As Boolean
    Public Property Prefix As String
    Public Property IsJoint As String
    Public Property RoleType As String
    Public Property MemberNumber As String
    Public Property SSN As String
    Public Property FirstName As String
    Public Property MiddleName As String
    Public Property LastName As String
    Public Property NameSuffix As String
    Public Property DOB As Date
    Public Property Gender As String
    Public Property MotherMaidenName As String
    Public Property CitizenshipStatus As String
    Public Property MaritalStatus As String
    Public Property EmployeeOfLender As String
    Public Property RelationToPrimaryApplicant As String
    Public Property Beneficiaries As List(Of CBeneficiaryInfo)

    Public Property AddressStreet As String
    Public Property AddressStreet2 As String
    Public Property AddressCity As String
    Public Property AddressZip As String
    Public Property AddressState As String
    Public Property AddressCountry As String
    Public Property HasMailingAddress As String
    Public Property MailingAddressStreet As String
    Public Property MailingAddressStreet2 As String
    Public Property MailingAddressCity As String
    Public Property MailingAddressZip As String
    Public Property MailingAddressCountry As String
    Public Property MailingAddressState As String

    Public Property HasPreviousAddress As String
    Public Property PreviousAddressStreet As String
    Public Property PreviousAddressCity As String
    Public Property PreviousAddressZip As String
    Public Property PreviousAddressCountry As String
    Public Property PreviousAddressState As String
    Public Property HousingPayment As String
    Public Property OccupyingLocation As String
    Public Property OccupancyDuration As Integer
    Public Property OccupancyDescription As String

    Public Property ContactMethod As String
    Public Property ContactEmail As String
    Public Property ContactHomePhone As String
    Public Property ContactMobilePhone As String
    Public Property RelativeFirstName As String
    Public Property RelativeLastName As String
    Public Property RelativePhone As String
    Public Property RelativeEmail As String
    Public Property RelativeRelationship As String
    Public Property RelativeAddress As String
    Public Property RelativeZip As String
    Public Property RelativeCity As String
    Public Property RelativeState As String
    Public Property IDState As String
    Public Property IDCountry As String
    Public Property IDCardType As String
    Public Property IDCardNumber As String
    Public Property IDDateIssued As String
    Public Property IDDateExpire As String

    Public Property EmploymentStatus As String
    Public Property EmploymentDescription As String
    Public Property GrossMonthlyIncome As Double
    Public Property EnlistmentDate As String

    Public Property EmployedDurationMonth As Integer
    Public Property EmployedDurationYear As Integer
    Public Property Employer As String
    Public Property JobTitle As String
    Public Property BusinessType As String
    Public Property EmploymentStartDate As String
    Public Property ETS As String
    Public Property ProfessionDurationMonth As Integer
    Public Property ProfessionDurationYear As Integer
    Public Property SupervisorName As String
    Public Property BranchOfService As String
    Public Property PayGrade As String
    Public Property HasPreviousEmployment As String
    Public Property PrevEmploymentStartDate As String
    Public Property PrevEmploymentStatus As String
    Public Property PrevGrossMonthlyIncome As String
    Public Property PrevEmployedDurationMonth As String
    Public Property PrevEmployedDurationYear As String
    Public Property PrevJobTitle As String
    Public Property PrevEmployer As String
    Public Property PrevBusinessType As String
    Public Property PrevEnlistmentDate As String
    Public Property PrevBranchOfService As String
    Public Property PrevPayGrade As String
    Public Property PrevETS As String
	Public Property ApplicantQuestionAnswers As List(Of CValidatedQuestionAnswers)
End Class
