﻿Imports Microsoft.VisualBasic

Public Class CLenderConfigLog
	Public Property LenderConfigLogID As Guid
	Public Property OrganizationID As Guid
	Public Property LenderID As Guid
	Public Property LenderRef As String
	Public Property ReturnURL As String
	Public Property LPQLogin As String
	Public Property LPQPW As String
	Public Property LPQURL As String
	Public Property ConfigData As String
	Public Property LogTime As DateTime
	Public Property RemoteIP As String
	Public Property LastModifiedByUserID As Guid
	Public Property LastModifiedByUserName As String
	Public Property Note As String
	Public Property Comment As String
	Public Property LenderConfigID As Guid
	Public Property AuditChanges As String
	Public Property RevisionID As Integer
End Class

