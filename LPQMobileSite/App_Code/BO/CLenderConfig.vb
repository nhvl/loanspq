﻿Imports Microsoft.VisualBasic
Imports System.Xml
Imports LPQMobile.Utils

Namespace LPQMobile.BO
	<Serializable()> _
 Public Class CLenderConfig
		Public ID As Guid	'unique within mobile platform
		Public OrganizationId As Guid 'unique within lpq platform in the same host
		Public LenderId As Guid	'unique within lpq platform in the same host
		Public LenderRef As String 'unique within mobile platform, but changeable by admin
		Public ReturnURL As String
		Public LPQLogin As String
		Public LPQPW As String
		Public LPQURL As String
		Public ConfigData As String
		Public Note As String = ""
		Public LastModifiedByUserID As Guid
		Public LastModifiedByUserFullName As String
		Public LastModifiedDate As DateTime
		'The below Dims are used by legacy config
		Public LastOpenedByUserID As Guid = Guid.Empty
		Public LastOpenedByUserFullName As String
		Public LastOpenedDate As DateTime
		Public IterationID As Integer
		Public RevisionID As Integer
		Public ReadOnly Property CacheIDSuffix() As String
			Get
				Return LenderId.ToString().Replace("-", "").ToLower() & "_" & SmUtil.ExtractHostFromUrl(LPQURL)
			End Get
		End Property

		Public Sub New()

		End Sub

		Public Sub New(ByVal oNode As XmlElement)

			Me.OrganizationId = Common.SafeGUID(oNode.GetAttribute("organization_id"))
			Me.LenderId = Common.SafeGUID(oNode.GetAttribute("lender_id"))
			Me.LenderRef = Common.SafeString(oNode.GetAttribute("lender_ref"))
			Me.ReturnURL = oNode.GetAttribute("finished_url")
			Me.LPQLogin = oNode.GetAttribute("user")
			Me.LPQPW = oNode.GetAttribute("password")
			Me.LPQURL = oNode.GetAttribute("loan_submit_url")
			Me.ConfigData = oNode.OuterXml
		End Sub
	End Class

End Namespace

