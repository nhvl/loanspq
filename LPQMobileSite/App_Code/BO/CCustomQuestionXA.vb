﻿Imports System.Xml
Imports System.Linq
Imports System.Net
Imports System.IO
Imports System.Web.Script.Serialization
Imports Newtonsoft.Json
Imports Newtonsoft.Json.Converters
Imports LPQMobile.Utils
Imports LPQMobile.Utils.Common

Namespace LPQMobile.BO

	<Serializable()>
	Public Class CCustomQuestionXA
		Private Shared _log As log4net.ILog = log4net.LogManager.GetLogger(GetType(CCustomQuestionXA))


		Public Position As Integer
		Public Identifer As QuestionIdentifier
		Public Name As String
		Public Text As String
		Public AnswerType As String
		Public IsRequired As Boolean
		Public DataType As String
		Public IsAvailableToConsumer As Boolean
		Public IsApplicant As Boolean
		Public IsActive As Boolean
		Public IsHeader As Boolean
		Public minLength As String = ""
		Public maxLength As String = ""
		Public minValue As String = ""
		Public maxValue As String = ""
		Public IsPersonalEntity As Boolean
		Public IsBusinessEntity As Boolean
		Public reg_expression As String
		Public AccountPositionScope As String
		Public ErrorMessage As String
		Public ConfirmationText As String
		Public specialAccountTypes As New List(Of String)
		Private _CustomQuestionOptions As SerializableList(Of CCustomQuestionOption)

		Public Property CustomQuestionOptions() As SerializableList(Of CCustomQuestionOption)
			Get
				If _CustomQuestionOptions Is Nothing Then
					_CustomQuestionOptions = New SerializableList(Of CCustomQuestionOption)
				End If
				Return _CustomQuestionOptions
			End Get
			Set(ByVal value As SerializableList(Of CCustomQuestionOption))
				_CustomQuestionOptions = value
			End Set
		End Property
		Public Sub New()

		End Sub
		'not being used for XA
		Public Sub New(ByVal oNode As XmlElement)
			Me.Position = SafeInteger(oNode.GetAttribute("position"))
			Me.Name = oNode.GetAttribute("name")
			Me.Text = oNode.GetAttribute("text")
			Me.AnswerType = oNode.GetAttribute("answer_type")
			Me.IsRequired = Convert.ToBoolean(oNode.GetAttribute("is_required"))

			Me.DataType = oNode.GetAttribute("data_type")
			Me.IsAvailableToConsumer = Convert.ToBoolean(oNode.GetAttribute("is_available_to_consumer"))
			Me.IsApplicant = Convert.ToBoolean(oNode.GetAttribute("is_applicant"))
			Me.Identifer = New QuestionIdentifier(Me.Name, IIf(Me.IsApplicant, QuestionRole.Applicant, QuestionRole.Application))
			Me.IsActive = Convert.ToBoolean(oNode.GetAttribute("is_active"))
			Me.IsHeader = Convert.ToBoolean(oNode.GetAttribute("is_header"))
			Me.IsPersonalEntity = Convert.ToBoolean(oNode.GetAttribute("is_xa_entity_personal"))
			Me.IsBusinessEntity = Convert.ToBoolean(oNode.GetAttribute("is_xa_entity_business"))
			If Me.DataType = "N" Or Me.DataType = "C" Then
				Me.minValue = SafeString(oNode.GetAttribute("min_value"))
				Me.maxValue = SafeString(oNode.GetAttribute("max_value"))
			ElseIf Me.DataType = "S" Or Me.DataType = "P" Then
				Me.minLength = SafeString(oNode.GetAttribute("min_value"))
				Me.maxLength = SafeString(oNode.GetAttribute("max_value"))
			End If

			Me.AccountPositionScope = SafeString(oNode.GetAttribute("xa_account_position_scope"))
			Me.reg_expression = SafeString(oNode.GetAttribute("regular_expression"))
			If Not String.IsNullOrEmpty(Me.reg_expression) Then
				Me.reg_expression = New JavaScriptSerializer().Serialize(Me.reg_expression)
			End If
			Me.ErrorMessage = SafeString(oNode.GetAttribute("error_message"))
			Me.ConfirmationText = SafeString(oNode.GetAttribute("confirmation_text"))
			Me.CustomQuestionOptions = New SerializableList(Of CCustomQuestionOption)
			For Each oOptNode As XmlElement In oNode.SelectNodes("RESTRICTED_ANSWERS/RESTRICTED_ANSWER")
				Dim oOpt As CCustomQuestionOption = New CCustomQuestionOption(oOptNode)
				If Not String.IsNullOrEmpty(oOpt.Text) AndAlso Not String.IsNullOrEmpty(oOpt.Value) Then
					Me.CustomQuestionOptions.Add(oOpt)
				ElseIf oOpt.Position = 0 AndAlso String.IsNullOrEmpty(oOpt.Text) Then 'empty default case
					Me.CustomQuestionOptions.Add(oOpt)
				End If
			Next
			Me.CustomQuestionOptions = Me.CustomQuestionOptions.OrderBy(Function(o) o.Position).ToSerializableList_()
		End Sub

		Public Shared Function CurrentXACustomQuestions(oConfig As CWebsiteConfig, Optional sAvailability As String = "") As List(Of CCustomQuestionXA)
			Dim questionList As List(Of CCustomQuestionXA) = CDownloadedSettings.DownloadQuestions(oConfig)
			If questionList IsNot Nothing AndAlso questionList.Count > 0 Then
				If sAvailability = "1b" Or sAvailability = "2b" Then ''get business xa application questions
					questionList = questionList.Where(Function(q) Not q.IsApplicant AndAlso q.IsBusinessEntity).OrderBy(Function(o) o.Position).ToList()
				Else ''get personal xa application questions
					questionList = questionList.Where(Function(q) Not q.IsApplicant AndAlso q.IsPersonalEntity).OrderBy(Function(o) o.Position).ToList()
				End If
			End If
			Return questionList
		End Function

		Public Shared Function CurrentXAApplicantQuestions(oConfig As CWebsiteConfig, Optional sAvailability As String = "") As List(Of CCustomQuestionXA)
			Dim questionList As List(Of CCustomQuestionXA) = CDownloadedSettings.DownloadQuestions(oConfig)
			If questionList IsNot Nothing AndAlso questionList.Count > 0 Then
				If sAvailability = "1b" Or sAvailability = "2b" Then ''get business xa applicant questions
					questionList = questionList.Where(Function(q) q.IsApplicant AndAlso q.IsBusinessEntity).OrderBy(Function(o) o.Position).ToList()
				Else ''get personal xa applicant questions
					questionList = questionList.Where(Function(q) q.IsApplicant AndAlso q.IsPersonalEntity).OrderBy(Function(o) o.Position).ToList()
				End If
			End If
			Return questionList
		End Function
	End Class

	<Serializable()>
	Public Enum QuestionRole
		<JsonConverter(GetType(StringEnumConverter))>
		Application
		<JsonConverter(GetType(StringEnumConverter))>
		Applicant
	End Enum
	<Serializable()>
	Public Structure QuestionIdentifier
		Implements IEquatable(Of QuestionIdentifier)

		Public ReadOnly Name As String
		Public ReadOnly Role As QuestionRole

		Public Sub New(ByVal sName As String, ByVal eRole As QuestionRole)
			If String.IsNullOrEmpty(sName) Then Throw New ArgumentNullException(sName)
			Name = sName
			Role = eRole
		End Sub

#Region "IEquatable"
		Public Overloads Function Equals(other As QuestionIdentifier) As Boolean Implements IEquatable(Of QuestionIdentifier).Equals
			Return other.Name = Me.Name AndAlso other.Role = Me.Role
		End Function

		Public Overrides Function Equals(obj As Object) As Boolean
			If obj Is Nothing Then Return False

			If TypeOf obj Is QuestionIdentifier Then Return CType(obj, QuestionIdentifier).Equals(Me)
			Return False
		End Function

		Public Overrides Function GetHashCode() As Integer
			Return ShiftAndWrap(Me.Name.GetHashCode(), 2) Xor Me.Role.GetHashCode()
		End Function

		Private Function ShiftAndWrap(value As Integer, positions As Integer) As Integer
			positions = positions And &H1F

			' Save the existing bit pattern, but interpret it as an unsigned integer.
			Dim number As UInteger = BitConverter.ToUInt32(BitConverter.GetBytes(value), 0)
			' Preserve the bits to be discarded.
			Dim wrapped As UInteger = number >> (32 - positions)
			' Shift and wrap the discarded bits.
			Return BitConverter.ToInt32(BitConverter.GetBytes((number << positions) Or wrapped), 0)
		End Function

		Public Shared Operator =(cq1 As QuestionIdentifier, cq2 As QuestionIdentifier) As Boolean
			Return cq1.Equals(cq2)
		End Operator

		Public Shared Operator <>(cq1 As QuestionIdentifier, cq2 As QuestionIdentifier) As Boolean
			Return Not cq1.Equals(cq2)
		End Operator
#End Region	' IEquatable

	End Structure

End Namespace

