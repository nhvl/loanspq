﻿Imports Microsoft.VisualBasic

Public Class CSpecialAccountRole
	Public Property RoleType As String
	Public ReadOnly Property DisplayRoleName As String
		Get
            Return CEnum.MapSpecialRoleValueToDisplayName(RoleType)
        End Get
	End Property
	Public Property InstanceMin As Integer
	Public Property InstanceMax As Integer
	Public Property AllowIDAuthentication As Boolean
	Public Property AllowIDVerification As Boolean
	Public Property AllowDebit As Boolean
	Public Property AllowCreditReport As Boolean
	Public Property AllowOfac As Boolean
	Public Property ShowJointOption As Boolean
	Public Property ShowAddress As Boolean
	Public Property ShowEmployment As Boolean
	Public Property ShowContactRelative As Boolean
	Public Property IsActive As Boolean
	Public Property ShowIDCard As Boolean
	Public Property ShowRelationship As Boolean
	Public Property RoleIndex As Integer
	Public Property DisplayForAtmDebitCard As Boolean
	Public Property DisplayForYellowHammer As Boolean
	Public Property ShowApplicantContactInfo As Boolean
	Public Property MinAge As Integer
	Public Property MaxAge As Integer
	Public Property RequirePrimaryIDCard As Boolean
	Public Property RequireSecondaryIDCard As Boolean
	Public Property ShowOccupancyStatus As Boolean
	Public Property ShowMaritalStatus As Boolean
End Class
