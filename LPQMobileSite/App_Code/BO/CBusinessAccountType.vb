﻿
Public Class CBusinessAccountType
	Public Property BusinessType As String
	Public Property AccountCode As String
	Public Property IsActive As Boolean
	Public Property EnableOnConsumer As Boolean
	Public Property AutoCreateApplicants As Boolean
	Public Property ShowDoingBusinessAs As Boolean
	Public Property Roles As List(Of CBusinessAccountRole)
End Class
