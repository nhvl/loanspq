﻿Imports Microsoft.VisualBasic


''' <summary>
''' object for storing response fron xsell(refinance object: vehicle and he)
''' </summary>
''' <remarks></remarks>
Public Class CXSellRefinanceGrade
	Public Property CreatorName() As String
	Public Property Rate() As Decimal
	Public Property Balance() As Decimal
	Public Property OldMonthlyPayment() As Decimal
	Public Property NewMonthlyPayment() As Decimal
	Public Property MaxAmountQualified() As Decimal
	Public Property MonthlySavings() As Decimal

	''''''
	'additional property for HE
	Public Property ProgramName() As String
	Public Property APR() As Decimal
	'assumption
	Public Property AssumedAmount() As Decimal
	Public Property AssumedTerm As Integer


End Class
