﻿Imports Microsoft.VisualBasic

Namespace LPQMobile.BO
	<Serializable()> _
 Public Class CCustomQuestionAnswer

		Public QuestionName As String
		Public QuestionAnswer As String
		''----create question answer text and type
		Public QuestionAnswer_text As String
		Public QuestionType As String
		''----add answer_text parameter to the sub New
		'Public Sub New(ByVal psQuestionName As String, ByVal psAnswer As String)
		Public Sub New(ByVal psQuestionName As String, ByVal psAnswer As String, ByVal psAnswer_text As String, ByVal psAnswer_type As String)
			QuestionName = psQuestionName
			QuestionAnswer = psAnswer
			QuestionAnswer_text = psAnswer_text
			QuestionType = psAnswer_type
		End Sub
	End Class

    Public Class CCustomQuestionAnswerList
        Inherits System.Collections.Generic.List(Of CCustomQuestionAnswer)
        ''----add answer_text parameter to the function
        'Public Sub AddCustomQuestionAnswer(ByVal psQuestionName As String, ByVal psAnswer As String)
        Public Sub AddCustomQuestionAnswer(ByVal psQuestionName As String, ByVal psAnswer As String, ByVal psAnswer_text As String, ByVal psAnswer_type As String)
            Me.Add(New CCustomQuestionAnswer(psQuestionName, psAnswer, psAnswer_text, psAnswer_type))
        End Sub
    End Class


End Namespace