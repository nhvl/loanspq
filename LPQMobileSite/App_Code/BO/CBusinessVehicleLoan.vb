﻿Imports Microsoft.VisualBasic
Imports System.Xml
Imports LPQMobile.Utils.Common
Imports LPQMobile.Utils
Imports System.Web.Script.Serialization

Namespace LPQMobile.BO
    Public Class CBusinessVehicleLoan
        Inherits CVehicleLoan
        Public Overrides ReadOnly Property LoanType() As String
            Get
                Return "BL"
            End Get
        End Property

        Public Sub New(ByVal psOrganizationId As String, ByVal psLenderId As String)
            MyBase.New(psOrganizationId, psLenderId)
        End Sub

        Public Overrides Function GetXml(oConfig As CWebsiteConfig) As XmlDocument
            Dim oDoc As New XmlDocument()
            oDoc.LoadXml("<BUSINESS_LOAN  xmlns=""http://www.meridianlink.com/CLF"" version=""5.206"" />") ''5.070

            AddBusinessApplicantsXml(oDoc, oConfig)
            ''company infor
            AddBusinessInfoXml(oDoc)
            '' guarantors
            AddBusinessLoanInfoXml(oDoc)
            AddBusinessVehicleXml(oDoc)
            AddLoanStatusXml(oDoc)
            AddFundingXml(oDoc)
            AddCommentsXml(oDoc)
            AddBusinessCustomQuestionsXml(oDoc, oConfig)
            AddSystemXml(oDoc)
            AddOtherSystemXml(oDoc)
            ''AddHDMA(oDoc)
            AddBeneficialOwnersXml(oDoc)
            Return oDoc
        End Function

        Protected Sub AddBusinessVehicleXml(ByVal poDoc As XmlDocument)
            Dim oxmlVehicles As XmlElement
            Dim oxmlVehicleInfo As XmlElement = CType(poDoc.DocumentElement.AppendChild(poDoc.CreateElement("VEHICLE_INFO", NAMESPACE_URI)), XmlElement)
            oxmlVehicleInfo.SetAttribute("total_vehicle_value", AmountRequested)
            oxmlVehicleInfo.SetAttribute("total_sales_price", AmountRequested)
            oxmlVehicleInfo.SetAttribute("down_payment", DownPayment)
            'oxmlVehicleInfo.SetAttribute("", "")
            'oxmlVehicleInfo.SetAttribute("", "")
            'oxmlVehicleInfo.SetAttribute("", "")
            'oxmlVehicleInfo.SetAttribute("", "")
            'oxmlVehicleInfo.SetAttribute("", "")
            'oxmlVehicleInfo.SetAttribute("", "")
            oxmlVehicles = CType(oxmlVehicleInfo.AppendChild(poDoc.CreateElement("VEHICLES", NAMESPACE_URI)), XmlElement)
            Dim oxmlVehicle As XmlElement = CType(oxmlVehicles.AppendChild(poDoc.CreateElement("VEHICLE", NAMESPACE_URI)), XmlElement)
            oxmlVehicle.SetAttribute("mileage", VehicleMileage)
            oxmlVehicle.SetAttribute("is_new_vehicle", IIf(IsNewVehicle, "Y", "N").ToString())
            oxmlVehicle.SetAttribute("vehicle_value", VehicleValue)
            oxmlVehicle.SetAttribute("vehicle_sales_price", VehicleValue)
            oxmlVehicle.SetAttribute("vehicle_type", IIf(VehicleType = "AUTO/PICKUP-TRUCK", "CAR", VehicleType).ToString())
            oxmlVehicle.SetAttribute("year", VehicleYear)
            oxmlVehicle.SetAttribute("vin", VehicleVin.NullSafeToUpper_)
            oxmlVehicle.SetAttribute("make", IIf(VehicleMake = "", "UNDECIDED", VehicleMake).ToString())
            oxmlVehicle.SetAttribute("model", VehicleModel)
            If HasTradeIn = True Then
                Dim oxmlTradeIn As XmlElement = CType(oxmlVehicles.AppendChild(poDoc.CreateElement("TRADE_IN", NAMESPACE_URI)), XmlElement)
                oxmlTradeIn.SetAttribute("trade_value", TradeValue)
                oxmlTradeIn.SetAttribute("trade_payoff", TradePayOff)
                oxmlTradeIn.SetAttribute("trade_payment", TradePayment)
                oxmlTradeIn.SetAttribute("vehicle_type", TradeType)
                oxmlTradeIn.SetAttribute("year", TradeYear)
                oxmlTradeIn.SetAttribute("make", TradeMake)
                oxmlTradeIn.SetAttribute("model", TradeModel)
            End If
            If Not String.IsNullOrEmpty(VendorID) Then
                Dim smBL As New SmBL()
                Dim vendor As SmLenderVendor = smBL.GetLenderVendorByVendorID(VendorID)
                If vendor IsNot Nothing Then
                    Dim oxmlDealershipProcessing As XmlElement = CType(oxmlVehicle.AppendChild(poDoc.CreateElement("DEALERSHIP_PROCESSING", NAMESPACE_URI)), XmlElement)
                    oxmlDealershipProcessing.SetAttribute("name", vendor.VendorName)
                    oxmlDealershipProcessing.SetAttribute("dealer_number", vendor.DealerNumberLpq)
                    oxmlDealershipProcessing.SetAttribute("address", vendor.Address)
                    oxmlDealershipProcessing.SetAttribute("zip", vendor.Zip)
                    oxmlDealershipProcessing.SetAttribute("city", vendor.City)
                    oxmlDealershipProcessing.SetAttribute("state", vendor.State)
                    oxmlDealershipProcessing.SetAttribute("phone", vendor.Phone)
                    oxmlDealershipProcessing.SetAttribute("fax", vendor.Fax)
                End If
            End If
        End Sub
        Protected Sub AddBusinessLoanInfoXml(ByVal poDoc As XmlDocument)
            Dim oxmlLoanInfo As XmlElement = CType(poDoc.DocumentElement.AppendChild(poDoc.CreateElement("LOAN_INFO", NAMESPACE_URI)), XmlElement)
            ' <LOAN_INFO rate_code="" payment_due_day="15" max_underwrite_amount="0" epl_fees_total="0.00" is_claimed="N" is_complete_consumer="N" 
            'require_product_refresh="N" is_addon="N" is_amendment="N" solve_for="PAYMENT" is_possible_dupe="N" is_workout_loan="N" is_solvefor_manual="N" 
            'tier="0" tier_previous="0" is_prequalification_applied="N" is_ofac="N" is_high_risk_consumer="N" purchase_real_estate_amount="0.00" 
            'purchase_business_amount="0.00" refinance_amount="0.00" renovate_amount="0.00" working_capital_amount="0.00" purchase_equipment_amount="0.00"
            ' other_amount="0.00" borrower_contribution="0.00" amount_requested="12345.00" rate="0.000" loan_term="222" amount_approved="12345.00" monthly_payment="0.00" 
            'number_of_payments="0" business_sub_type="VL" business_purpose_type="Purchase" apr="0.000" intro_apr="0.000" minimum_payment="0.00" payment_percent="0.000" 
            'is_indirect_loan="N" is_balloon="N" loc_amount="0" is_LOC="N" amount_approved_variance="0.00" use_of_proceeds="FSSFDD" />
            oxmlLoanInfo.SetAttribute("business_sub_type", "VL")
            oxmlLoanInfo.SetAttribute("loan_term", LoanTerm)
            oxmlLoanInfo.SetAttribute("business_purpose_type", LoanPurpose)
            oxmlLoanInfo.SetAttribute("amount_requested", AmountRequested)
        End Sub
    End Class
End Namespace
