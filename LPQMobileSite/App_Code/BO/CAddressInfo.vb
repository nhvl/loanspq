﻿<Serializable()>
Public Class CAddressInfo
	Public Address As String
	Public Address2 As String
	Public Zip As String
	Public City As String
	Public State As String
	Public Country As String
	Public OccupancyDuration As Integer
	Public OccupancyStatus As String
	Public OccupancyDescription As String
End Class