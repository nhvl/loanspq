﻿

Public Class CAdvancedLogicItem
	Public Property TargetItem As String
	Public Property TargetType As String
	Public Property LoanType As String
	Public Property Expression As String
	Public Property Conditions As List(Of CConditionItem)
End Class
