﻿
Imports Newtonsoft.Json.Converters
Imports Newtonsoft.Json

Namespace CustomApplicationScenarios
	Public Class CCustomApplicationScenarioItem
		Public Property ID As String
		Public Property Name As String
		Public Property Description As String
		<JsonConverter(GetType(StringEnumConverter))>
		Public Property AccountType As AccountType
		Public Property SpecialAccountType As String
		Public Property IsSecondaryAccount As Boolean = False
		Public Property RequiredProducts As HashSet(Of String)
		Public Property UseDefaultRequiredProducts As Boolean = True
		Public Property AvailableProducts As HashSet(Of String)
		Public Property UseDefaultAvailableProducts As Boolean = True
		Public Property PreselectedProducts As HashSet(Of String)
		Public Property UseDefaultPreselectedProducts As Boolean = True
		Public Property Enabled As Boolean = True
		Public Property Url As String
	End Class
	Public Enum AccountType
		Personal
		Business
		Special
	End Enum
End Namespace
