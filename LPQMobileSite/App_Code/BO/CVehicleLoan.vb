﻿Imports Microsoft.VisualBasic
Imports System.Xml
Imports LPQMobile.Utils.Common
Imports LPQMobile.Utils

Namespace LPQMobile.BO

    Public Class CVehicleLoan
        Inherits CBaseLoanApp

        Public VehicleType As String
		Public KnowVehicleMake As Boolean
		Public VehicleVin As String
        Public VehicleMake As String
        Public VehicleModel As String
		Public VehicleYear As Integer
        Public IsNewVehicle As Boolean
		Public VehicleValue As Double
		Public LoanTerm As Integer
        Public VehicleMileage As Integer
        Public AmountRequested As Double
        Public UnderstandsReconstructedTitle As Boolean
		Public DownPayment As Double 'validate on client side for greater than 0
		Public DocStampsFeeIsManual As String '"Y"/"N"

		Public TradeValue As Double
		Public TradePayOff As Double
		Public TradePayment As Double
		Public TradeType As String
		Public TradeYear As Integer
		Public TradeModel As String
		Public TradeMake As String
		Public HasTradeIn As Boolean
		


        Public Overrides ReadOnly Property LoanType() As String
            Get
                Return "VL"
            End Get
        End Property

		Public Sub New(ByVal psOrganizationId As String, ByVal psLenderId As String)
			MyBase.New(psOrganizationId, psLenderId)
		End Sub

		Public Overrides Function GetXml(oConfig As CWebsiteConfig) As XmlDocument
			Dim oDoc As New XmlDocument()
            oDoc.LoadXml("<VEHICLE_LOAN xmlns=""" & NAMESPACE_URI &
                         """ xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" " &
                         "xsi:schemaLocation=""http://www.meridianlink.com/CLF VEHICLE_LOAN.xsd http://www.meridianlink.com/CLF Integration\SSFCU.xsd"" " &
                         "version=""5.181"" />") ''5.203, 5.070

            AddApplicantsXml(oDoc, oConfig)
			AddLoanInfoXml(oDoc)
			AddLoanStatusXml(oDoc)
			AddFundingXml(oDoc)
			AddCommentsXml(oDoc)
			AddVehicleXml(oDoc)
            AddCustomQuestionsXml(oDoc) 'need to come after vehicle
            AddContactsXml(oDoc)
            AddSystemXml(oDoc)
            AddOtherSystemXml(oDoc)
			AddHDMA(oDoc)
			'log.Info(oDoc.InnerXml)
			Return oDoc
		End Function

        Public Sub AddLoanInfoXml(ByVal poDoc As XmlDocument)
            Dim oxmlLoanInfo As XmlElement = CType(poDoc.DocumentElement.AppendChild(poDoc.CreateElement("LOAN_INFO", NAMESPACE_URI)), XmlElement)
            oxmlLoanInfo.SetAttribute("loan_term", LoanTerm)
            ''oxmlLoanInfo.SetAttribute("request_type", LoanPurpose) ''for clf version < 5.107
            oxmlLoanInfo.SetAttribute("purpose_type", LoanPurpose)
            'assumption
            oxmlLoanInfo.SetAttribute("total_vehicle_value", AmountRequested)
            oxmlLoanInfo.SetAttribute("total_sales_price", AmountRequested)

            oxmlLoanInfo.SetAttribute("amount_requested", AmountRequested)
            oxmlLoanInfo.SetAttribute("down_payment", DownPayment)
            If Not String.IsNullOrEmpty(VendorID) Then
                oxmlLoanInfo.SetAttribute("is_indirect_loan", "Y")
            End If
        End Sub

        Public Sub AddFundingXml(ByVal poDoc As XmlDocument)
            Dim oxmlFunding As XmlElement = CType(poDoc.DocumentElement.AppendChild(poDoc.CreateElement("FUNDING", NAMESPACE_URI)), XmlElement)
            If DocStampsFeeIsManual <> "" Then
                oxmlFunding.SetAttribute("doc_stamps_fee_is_manual", DocStampsFeeIsManual)   'Founder requested for this so that doc filling fee will not be added automatically. 8/5/16, shouldn't need override bc LPQ side default to Y
            End If
            Dim oxmlInsuranceOption As XmlElement = CType(oxmlFunding.AppendChild(poDoc.CreateElement("INSURANCE_OPTIONS", NAMESPACE_URI)), XmlElement)
            oxmlInsuranceOption.SetAttribute("loan_class", "C")  'need to set Loan System to closed-end so "Owed To Lender" and APY fields are visible inFUnding page for booking
        End Sub

        Public Sub AddCommentsXml(ByVal poDoc As XmlDocument)
            Dim oxmlComments As XmlElement = CType(poDoc.DocumentElement.AppendChild(poDoc.CreateElement("COMMENTS", NAMESPACE_URI)), XmlElement)
            Dim oxmlDecisionComments As XmlElement = CType(oxmlComments.AppendChild(poDoc.CreateElement("DECISION_COMMENTS", NAMESPACE_URI)), XmlElement)
            Dim oxmlExternalComments As XmlElement = CType(oxmlComments.AppendChild(poDoc.CreateElement("EXTERNAL_COMMENTS", NAMESPACE_URI)), XmlElement)
            Dim oxmlInternalComments As XmlElement = CType(oxmlComments.AppendChild(poDoc.CreateElement("INTERNAL_COMMENTS", NAMESPACE_URI)), XmlElement)
            If HasDebtCancellation Then
                oxmlExternalComments.InnerText = HasDebtCancellationText
            End If
            ''''write internal comment element:  disclosures and originating ip address
            oxmlInternalComments.InnerText += CurrentEmail + " (" + DateTime.Now + "): Disclosure(s) Checked: " + Disclosure
            oxmlInternalComments.InnerText += Environment.NewLine
            oxmlInternalComments.InnerText += "SYSTEM (" + DateTime.Now + "): Originating IP Address: " + IPAddress
            oxmlInternalComments.InnerText += Environment.NewLine
            oxmlInternalComments.InnerText += "SYSTEM (" + DateTime.Now + "): USER AGENT: " + UserAgent
            oxmlInternalComments.InnerText += Environment.NewLine

            ''add cuna question answers in the internal comment if it exists
            If cunaQuestionAnswers.Count > 0 Then
                Dim i As Integer
                oxmlInternalComments.InnerText += Environment.NewLine
                oxmlInternalComments.InnerText += Environment.NewLine
                oxmlInternalComments.InnerText += "SYSTEM (" + DateTime.Now + "): PROTECTION INFORMATION: "
                oxmlInternalComments.InnerText += Environment.NewLine
                oxmlInternalComments.InnerText += Environment.NewLine
                For i = 0 To cunaQuestionAnswers.Count - 1
                    oxmlInternalComments.InnerText += cunaQuestionAnswers(i)
                    oxmlInternalComments.InnerText += Environment.NewLine
                Next
            End If
            ''add xSell comment message to internal comment if it exist
            If xSellCommentMessage <> "" Then
                oxmlInternalComments.InnerText += "SYSTEM (" + DateTime.Now + "): " & xSellCommentMessage
                oxmlInternalComments.InnerText += Environment.NewLine
            End If
            ''add instaTouch Prefill comment if it exist
            If InstaTouchPrefillComment <> "" Then
                oxmlInternalComments.InnerText += "SYSTEM (" + DateTime.Now + "): " & InstaTouchPrefillComment
                oxmlInternalComments.InnerText += Environment.NewLine
            End If
        End Sub

        Public Sub AddVehicleXml(ByVal poDoc As XmlDocument)
            Dim oxmlVehicles As XmlElement = CType(poDoc.DocumentElement.AppendChild(poDoc.CreateElement("VEHICLES", NAMESPACE_URI)), XmlElement)
            Dim oxmlVehicle As XmlElement = CType(oxmlVehicles.AppendChild(poDoc.CreateElement("VEHICLE", NAMESPACE_URI)), XmlElement)
            oxmlVehicle.SetAttribute("mileage", VehicleMileage)
            oxmlVehicle.SetAttribute("is_new_vehicle", IIf(IsNewVehicle, "Y", "N").ToString())
            oxmlVehicle.SetAttribute("vehicle_value", VehicleValue)
            oxmlVehicle.SetAttribute("vehicle_sales_price", VehicleValue)
            oxmlVehicle.SetAttribute("vehicle_type", IIf(VehicleType = "AUTO/PICKUP-TRUCK", "CAR", VehicleType).ToString())
            oxmlVehicle.SetAttribute("year", VehicleYear)
            oxmlVehicle.SetAttribute("vin", VehicleVin.NullSafeToUpper_)
            oxmlVehicle.SetAttribute("make", IIf(VehicleMake = "", "UNDECIDED", VehicleMake).ToString())
            oxmlVehicle.SetAttribute("model", VehicleModel)
            If HasTradeIn = True Then
                Dim oxmlTradeIn As XmlElement = CType(oxmlVehicles.AppendChild(poDoc.CreateElement("TRADE_IN", NAMESPACE_URI)), XmlElement)
                oxmlTradeIn.SetAttribute("trade_value", TradeValue)
                oxmlTradeIn.SetAttribute("trade_payoff", TradePayOff)
                oxmlTradeIn.SetAttribute("trade_payment", TradePayment)
                oxmlTradeIn.SetAttribute("vehicle_type", TradeType)
                oxmlTradeIn.SetAttribute("year", TradeYear)
                oxmlTradeIn.SetAttribute("make", TradeMake)
                oxmlTradeIn.SetAttribute("model", TradeModel)
            End If
            If Not String.IsNullOrEmpty(VendorID) Then
                Dim smBL As New SmBL()
                Dim vendor As SmLenderVendor = smBL.GetLenderVendorByVendorID(VendorID)
                If vendor IsNot Nothing Then
                    Dim oxmlDealershipProcessing As XmlElement = CType(oxmlVehicle.AppendChild(poDoc.CreateElement("DEALERSHIP_PROCESSING", NAMESPACE_URI)), XmlElement)
                    oxmlDealershipProcessing.SetAttribute("name", vendor.VendorName)
                    oxmlDealershipProcessing.SetAttribute("dealer_number", vendor.DealerNumberLpq)
                    oxmlDealershipProcessing.SetAttribute("address", vendor.Address)
                    oxmlDealershipProcessing.SetAttribute("zip", vendor.Zip)
                    oxmlDealershipProcessing.SetAttribute("city", vendor.City)
                    oxmlDealershipProcessing.SetAttribute("state", vendor.State)
                    oxmlDealershipProcessing.SetAttribute("phone", vendor.Phone)
                    oxmlDealershipProcessing.SetAttribute("fax", vendor.Fax)
                End If
            End If
        End Sub

	End Class

End Namespace