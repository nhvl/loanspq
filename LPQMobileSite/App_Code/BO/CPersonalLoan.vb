﻿Imports Microsoft.VisualBasic
Imports System.Xml
Imports LPQMobile.Utils.Common
Imports LPQMobile.Utils

Namespace LPQMobile.BO

    Public Class CPersonalLoan
        Inherits CBaseLoanApp

        Public IsLOC As Boolean = False
        Public LoanAmount As Double
		Public LoanTerm As Integer
		Public LoanTypeCategory As String
        Public Description As String
		Public DocStampsFeeIsManual As String
		Public Overrides ReadOnly Property LoanType() As String
			Get
				Return "PL"
			End Get
		End Property

		Public Sub New(ByVal psOrganizationId As String, ByVal psLenderId As String)
			MyBase.New(psOrganizationId, psLenderId)
		End Sub

		Public Overrides Function GetXml(oConfig As CWebsiteConfig) As XmlDocument
			Dim oDoc As New XmlDocument()
            oDoc.LoadXml("<PERSONAL_LOAN xmlns=""" & NAMESPACE_URI &
                         """ xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" " &
                         "xsi:schemaLocation=""http://www.meridianlink.com/CLF personal_loan.xsd http://www.meridianlink.com/CLF Integration\SSFCU.xsd"" " &
                         "version=""5.181"" />") ''5.203, 5.070
            AddApplicantsXml(oDoc, oConfig)
			AddLoanInfoXml(oDoc)
			AddLoanStatusXml(oDoc)
			AddFundingXml(oDoc)
			AddCommentsXml(oDoc)
            AddCustomQuestionsXml(oDoc)
            AddContactsXml(oDoc)
            AddSystemXml(oDoc)

            Return oDoc
		End Function

		

        Private Sub AddLoanInfoXml(ByVal poDoc As XmlDocument)
			Dim oxmlLoanInfo As XmlElement = CType(poDoc.DocumentElement.AppendChild(poDoc.CreateElement("LOAN_INFO", NAMESPACE_URI)), XmlElement)
			If String.IsNullOrEmpty(LoanTypeCategory) Then
				LoanTypeCategory = "UNSECURE"
			End If
			oxmlLoanInfo.SetAttribute("personal_loan_type", LoanTypeCategory)
            oxmlLoanInfo.SetAttribute("purpose_type", LoanPurpose)
            oxmlLoanInfo.SetAttribute("amount_requested", LoanAmount)
			oxmlLoanInfo.SetAttribute("is_LOC", IIf(IsLOC, "Y", "N").ToString())
            oxmlLoanInfo.SetAttribute("purpose_description", Description)
			oxmlLoanInfo.SetAttribute("loan_term_requested", LoanTerm)

			If ClinicID <> "" Then
				oxmlLoanInfo.SetAttribute("is_indirect_loan", "Y")
			End If

            If Not String.IsNullOrEmpty(VendorID) Then
                oxmlLoanInfo.SetAttribute("is_indirect_loan", "Y")
            End If

            'custom for Founder but apply it to all
			If SafeString(LoanPurpose).ToUpper.Contains("OVERDRAFT") Then oxmlLoanInfo.SetAttribute("is_overdraft", "Y")

		End Sub

        Public Sub AddFundingXml(ByVal poDoc As XmlDocument)
            Dim oxmlFunding As XmlElement = CType(poDoc.DocumentElement.AppendChild(poDoc.CreateElement("FUNDING", NAMESPACE_URI)), XmlElement)
            If DocStampsFeeIsManual <> "" Then
                oxmlFunding.SetAttribute("doc_stamps_fee_is_manual", DocStampsFeeIsManual)   'Founder requested for this so that doc filling fee will not be added automatically. 8/5/16, shouldn't need override bc LPQ side default to Y
            End If
            Dim oxmlInsuranceOption As XmlElement = CType(oxmlFunding.AppendChild(poDoc.CreateElement("INSURANCE_OPTIONS", NAMESPACE_URI)), XmlElement)
            oxmlInsuranceOption.SetAttribute("loan_class", "C")  'need to set Loan System to closed-end so "Owed To Lender" and APY fields are visible inFUnding page for booking
        End Sub

        Public Sub AddCommentsXml(ByVal poDoc As XmlDocument)
            Dim oxmlComments As XmlElement = CType(poDoc.DocumentElement.AppendChild(poDoc.CreateElement("COMMENTS", NAMESPACE_URI)), XmlElement)
            Dim oxmlDecisionComments As XmlElement = CType(oxmlComments.AppendChild(poDoc.CreateElement("DECISION_COMMENTS", NAMESPACE_URI)), XmlElement)
            Dim oxmlExternalComments As XmlElement = oxmlComments.AppendChild(poDoc.CreateElement("EXTERNAL_COMMENTS", NAMESPACE_URI))
            Dim oxmlInternalComments As XmlElement = CType(oxmlComments.AppendChild(poDoc.CreateElement("INTERNAL_COMMENTS", NAMESPACE_URI)), XmlElement)
            If HasDebtCancellation Then
                oxmlExternalComments.InnerText = HasDebtCancellationText
            End If
            ''''write internal comment element:  disclosures and originating ip address, user agent
            oxmlInternalComments.InnerText += CurrentEmail + " (" + DateTime.Now + "): Disclosure(s) Checked: " + Disclosure
            oxmlInternalComments.InnerText += Environment.NewLine
            oxmlInternalComments.InnerText += "SYSTEM (" + DateTime.Now + "): Originating IP Address: " + IPAddress
            oxmlInternalComments.InnerText += Environment.NewLine
            oxmlInternalComments.InnerText += "SYSTEM (" + DateTime.Now + "): USER AGENT: " + UserAgent
            oxmlInternalComments.InnerText += Environment.NewLine

            ''add cuna question answers in the internal comment if it exists
            If cunaQuestionAnswers.Count > 0 Then
                Dim i As Integer
                oxmlInternalComments.InnerText += Environment.NewLine
                oxmlInternalComments.InnerText += Environment.NewLine
                oxmlInternalComments.InnerText += "SYSTEM (" + DateTime.Now + "): PROTECTION INFORMATION: "
                oxmlInternalComments.InnerText += Environment.NewLine
                oxmlInternalComments.InnerText += Environment.NewLine
                For i = 0 To cunaQuestionAnswers.Count - 1
                    oxmlInternalComments.InnerText += cunaQuestionAnswers(i)
                    oxmlInternalComments.InnerText += Environment.NewLine
                Next
            End If
            ''add instaTouch Prefill comment if it exist
            If InstaTouchPrefillComment <> "" Then
                oxmlInternalComments.InnerText += "SYSTEM (" + DateTime.Now + "): " & InstaTouchPrefillComment
                oxmlInternalComments.InnerText += Environment.NewLine
            End If
        End Sub


    End Class

End Namespace
