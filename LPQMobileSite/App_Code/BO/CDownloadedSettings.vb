﻿Imports System.Data
Imports Microsoft.VisualBasic.CompilerServices
Imports LPQMobile.BO
Imports DBUtils
Imports Microsoft.VisualBasic
Imports LPQMobile.Utils
Imports System.Xml
Imports System.Xml.Serialization
Imports DownloadedSettings
Imports System.Net
Imports System.IO
Imports System.Security.Cryptography
Imports System.Configuration
Imports System.Text
Imports System.Web

<Serializable()>
Public Class CDownloadSettingData
	Public _VehicleTypes As List(Of CListItem)
	Public _MotorCycleMakes As List(Of CListItem)
	Public _CarMakes As List(Of CListItem)
	Public _PersonalLoanPurposes As List(Of CListItem)
	Public _VehicleLoanPurposes As List(Of CListItem)
	Public _CreditCardPurposes As List(Of CListItem)
	Public _HomeEquityPurposes As List(Of CListItem)
	Public _BusinessPurposes As List(Of CListItem)

	Public _IDCards As List(Of CIDCardListItem)
	Public _Branches As List(Of CBranch)

	''ned property type


	Public _CustomApplicationQuestions As XmlDocument
	Public _CustomApplicantQuestions As XmlDocument

	Public _XAProducts As XmlDocument

	Public _XAPreferences As XmlDocument

	Public _HasVehicleTypes As String = String.Empty
	Public _HasMotorCycleMakes As String = String.Empty
	Public _HasCarMakes As String = String.Empty
	Public _HasPersonalLoanPurposes As String = String.Empty
	Public _HasVehicleLoanPurposes As String = String.Empty
	Public _HasCreditCardPurposes As String = String.Empty
	Public _HasHomeEquityPurposes As String = String.Empty
	Public _HasBusinessPurposes As String = String.Empty
	Public _HasIDCards As String = String.Empty
	Public _HasApplicantQuestions As String = String.Empty
	Public _HasApplicationQuestions As String = String.Empty
	Public _HasXAProducts As String = String.Empty
	Public _HasBranches As String = String.Empty
	Public _HasXAReferences As String = String.Empty
	Public _XABusinessAccountTypes As XmlDocument
	Public _XASpecialAccountTypes As XmlDocument
	Public _HasXABusinessAccountTypes As String = String.Empty
	Public _HasXASpecialAccountTypes As String = String.Empty
	Public _CreditCardProducts As List(Of CCreditCardProduct)
	Public _HasCreditCardProducts As String = ""
	Public _HomeEquityLoanProducts As List(Of CHomeEquityLoanProduct)
	Public _HasHomeEquityLoanProducts As String = ""
	Public _PersonalLoanProducts As List(Of CPersonalLoanProduct)
	Public _HasPersonalLoanProducts As String = ""
	Public _VehicleLoanProducts As List(Of CVehicleLoanProduct)
	Public _HasVehicleLoanProducts As String = ""
End Class



Public Class CDownloadedSettings
	Private Shared _log As log4net.ILog = log4net.LogManager.GetLogger(GetType(CDownloadedSettings))

	''Digital Certificate
	'setup use LPQMobileLib.ddl(.NET 4.6.1) from Bin
	'TODO: move this to web.config
	Private Shared _CertPath As String = "c:\LPQMobileWebsiteConfig\cert\apm_selfsigned_2018.pfx"
	Private Shared _CertPW As String = "tonyapm"

	''' <remarks>
	''' Note: This method of synchronization will only work for a single-server environment.
	''' With multiple webservers in a farm, each one will have their own locks. If two requests come in for the same lender,
	''' each server will still work in parallel, and potentially overwrite each other in the database cache, losing the 
	''' results of an API call.
	''' 
	''' Possibilities for future improvement for farm-readiness:
	''' - Implement a central semaphore server, so that webservers (if absolutely needed) can use an over-the-network semaphore.
	'''   - Network semaphores may be slow, and may lead to a central point of failure, and may not had 100% integrity.
	''' - Dedicate a particular machine to be the one making API requests to fill the database cache. This single server would lock within itself on multiple requests.
	'''   - Still a central point of failure, though webservers could use shared code to fallback to local retrieval of settings.
	''' - Separate out cached items enough so that the problem is isolated to double pulls (because the double-lock paradigm would be lost).
	'''   - This may not work well for larger cache items like applicant custom questions where a single pull may take very long. See AP-2907.
	''' </remarks>
	Private Shared _LenderLockManager As New CLenderLockCollectionManager(Of CDownloadSettingsLenderLockCollection)

	Private Shared LPQMOBILE_CONNECTIONSTRING As String = System.Web.Configuration.WebConfigurationManager.ConnectionStrings("LPQMOBILE_CONNECTIONSTRING").ConnectionString
	Public Shared Function CAnyType(Of T)(ByRef uto As Object) As T
		Return CType(uto, T)
	End Function

	Private Shared Sub SetCache(Of T)(sCacheKey As String, data As T)
		HttpContext.Current.Cache.Insert(sCacheKey, data, Nothing, Caching.Cache.NoAbsoluteExpiration, New TimeSpan(22, 0, 0))	'12hour&45sec
		HttpContext.Current.Application(sCacheKey) = DateTime.Now
	End Sub
	Public Shared Sub Recycle(cacheSuffix As String, Optional clearCacheFromDb As Boolean = True)
		_log.Debug("delete in-memory cached data")
		HttpContext.Current.Cache.Remove("CURRENT_XA_QUESTIONS_" & cacheSuffix)
		HttpContext.Current.Cache.Remove("CURRENT_FOM_QUESTIONS_" & cacheSuffix)
		HttpContext.Current.Cache.Remove("CURRENT_LENDER_SERVICE_CONFIG_INFO_" & cacheSuffix)
		HttpContext.Current.Cache.Remove("CURRENT_LIST_PRODUCT_" & cacheSuffix)
		HttpContext.Current.Cache.Remove("DOWNLOADED_SETTINS_" & cacheSuffix)
		If clearCacheFromDb Then
			_log.Debug("delete in-database cached data")
			ClearCacheDataFromDatabase("CURRENT_XA_QUESTIONS_" & cacheSuffix)
			ClearCacheDataFromDatabase("CURRENT_FOM_QUESTIONS_" & cacheSuffix)
			ClearCacheDataFromDatabase("CURRENT_LENDER_SERVICE_CONFIG_INFO_" & cacheSuffix)
			ClearCacheDataFromDatabase("CURRENT_LIST_PRODUCT_" & cacheSuffix)
			ClearCacheDataFromDatabase("DOWNLOADED_SETTINS_" & cacheSuffix)
		End If
	End Sub

	Private Shared Sub ClearCacheDataFromDatabase(sCacheKey As String)
		Dim oDB As New CSQLDBUtils(LPQMOBILE_CONNECTIONSTRING)
		Try
			Dim oWhere = New CSQLWhereStringBuilder()
			oWhere.AppendAndCondition("CachedKey={0}", New CSQLParamValue(sCacheKey))
			Dim oDelete As New CSQLDeleteStringBuilder("CachedConfigs", oWhere)
			oDB.executeNonQuery(oDelete.SQL)
		Catch ex As Exception
			_log.Error("Error while ClearCacheDataFromDatabase", ex)
		Finally
			oDB.closeConnection()
		End Try
	End Sub

	Private Shared Function GetLenderLockCollection(ByVal poConfig As CWebsiteConfig) As CDownloadSettingsLenderLockCollection
		Return _LenderLockManager.GetLenderLockCollection(poConfig)
	End Function

	Private Shared Function ReadCacheDataFromDatabase(Of T)(sCacheKey As String) As T
		Dim result As T
		Dim oData As DataTable = New DataTable()
		Dim sSQL As String = "Select top 1 * From CachedConfigs"
		Dim oWhere As New CSQLWhereStringBuilder()
		oWhere.AppendAndCondition("CachedKey={0}", New CSQLParamValue(sCacheKey))
		Dim oDB As New CSQLDBUtils(LPQMOBILE_CONNECTIONSTRING)
		Try
			oData = oDB.getDataTable(sSQL & oWhere.SQL & " ORDER BY CreatedDate DESC")
		Catch ex As Exception
			_log.Error("Error while GetCachedDataFromDatabase", ex)
		Finally
			oDB.closeConnection()
		End Try
		If oData IsNot Nothing AndAlso oData.Rows.Count > 0 Then
			Try
				Dim serializer As New XmlSerializer(GetType(T))
				Using reader As TextReader = New StringReader(Common.SafeString(oData.Rows(0)("Config")))
					result = CType(serializer.Deserialize(reader), T)
				End Using
				Return result
			Catch ex As Exception
				_log.Error("Cannot read cached data from database", ex)
			End Try
		End If
		Return Nothing
	End Function

	Private Shared Sub SaveCacheDataToDatabase(Of T)(sCacheKey As String, data As T)
		Dim oDB As New CSQLDBUtils(LPQMOBILE_CONNECTIONSTRING)
		Try
			ClearCacheDataFromDatabase(sCacheKey)
			Dim serializer As New XmlSerializer(GetType(T))
			Dim sw As New StringWriter
			serializer.Serialize(sw, data)
			Dim oInsert As New cSQLInsertStringBuilder("CachedConfigs")
			oInsert.AppendValue("CachedKey", New CSQLParamValue(sCacheKey))
			oInsert.AppendValue("Config", New CSQLParamValue(sw.ToString()))
			oInsert.AppendValue("CreatedDate", New CSQLParamValue(Now))
			oDB.executeNonQuery(oInsert.SQL)
		Catch ex As Exception
			_log.Error("Error while SaveDownloadedDataToDatabase", ex)
		Finally
			oDB.closeConnection()
		End Try
	End Sub



	Private Shared Function DownloadSettings(Of T)(ByVal poConfig As CWebsiteConfig, propName As String, sUrl As String, getData As Func(Of CDownloadSettingData, List(Of T)), setData As Func(Of CDownloadSettingData, List(Of T), CDownloadSettingData), getDataFlag As Func(Of CDownloadSettingData, String), setDataFlag As Func(Of CDownloadSettingData, String, CDownloadSettingData), Optional deserialType As String = "") As List(Of T)
		Dim oResult As New List(Of T)
		If poConfig.OrgCode = "" Or poConfig.LenderCode = "" Then
			_log.Error(poConfig.LenderId & " doesn't have org code or lender code")
			Return oResult
		End If

		Dim downloadedSettingData As CDownloadSettingData
		Dim sCacheKey As String = "DOWNLOADED_SETTINS_" & poConfig.CacheIDSuffix
		downloadedSettingData = CType(HttpContext.Current.Cache.Get(sCacheKey), CDownloadSettingData)
		If downloadedSettingData Is Nothing OrElse getData(downloadedSettingData) Is Nothing Then
			Try
				SyncLock GetLenderLockCollection(poConfig).DownloadSettingsLock
					' Ensure that the data was not loaded by a concurrent thread 
					' while waiting for lock.
					downloadedSettingData = CType(HttpContext.Current.Cache.Get(sCacheKey), CDownloadSettingData)
					If downloadedSettingData IsNot Nothing AndAlso getData(downloadedSettingData) IsNot Nothing Then
						_log.Info(propName & " Object " & sCacheKey & " was retrieved from cache. The cache was populated at " & HttpContext.Current.Application(sCacheKey).ToString())
						oResult = getData(downloadedSettingData)
					Else
						If downloadedSettingData IsNot Nothing AndAlso getDataFlag(downloadedSettingData) = "NO" Then
							oResult = New List(Of T)
							Exit Try
						End If

						If downloadedSettingData Is Nothing Then
							downloadedSettingData = New CDownloadSettingData
						End If

						'TODO: check and get cached data from DB
						Dim downloadedSettingsFromDb = ReadCacheDataFromDatabase(Of CDownloadSettingData)(sCacheKey)
						If downloadedSettingsFromDb IsNot Nothing AndAlso Not String.IsNullOrEmpty(getDataFlag(downloadedSettingsFromDb)) Then
							If getData(downloadedSettingsFromDb) IsNot Nothing AndAlso getData(downloadedSettingsFromDb).Count > 0 Then
								oResult = getData(downloadedSettingsFromDb)
								downloadedSettingData = setData(downloadedSettingData, oResult)
								downloadedSettingData = setDataFlag(downloadedSettingData, "YES")
							Else
								oResult = New List(Of T)
								downloadedSettingData = setDataFlag(downloadedSettingData, "NO")
							End If
						Else
							Dim sReponse = Common.SubmitRestApiGet(poConfig, sUrl)
							Dim oDeserializedOutput As Object
							If deserialType = "CIDCardOutput" Then
								Dim serializer As New XmlSerializer(GetType(CIDCardOutput))
								Using reader As TextReader = New StringReader(sReponse)
									oDeserializedOutput = CType(serializer.Deserialize(reader), CIDCardOutput)
								End Using
							Else
								Dim serializer As New XmlSerializer(GetType(COutput))
								Using reader As TextReader = New StringReader(sReponse)
									oDeserializedOutput = CType(serializer.Deserialize(reader), COutput)
								End Using
							End If

							If oDeserializedOutput IsNot Nothing AndAlso oDeserializedOutput.Response.Field_list.ListItems.Count > 0 Then
								oResult = CAnyType(Of List(Of T))(oDeserializedOutput.Response.Field_list.ListItems)
								downloadedSettingData = setData(downloadedSettingData, oResult)
								downloadedSettingData = setDataFlag(downloadedSettingData, "YES")
							Else
								oResult = New List(Of T)
								downloadedSettingData = setDataFlag(downloadedSettingData, "NO")
							End If
							SaveCacheDataToDatabase(sCacheKey, downloadedSettingData)
						End If
						SetCache(sCacheKey, downloadedSettingData)
					End If
				End SyncLock
			Catch ex As Exception
				_log.Error("Can't get " & propName, ex)
			End Try
		Else
			oResult = getData(downloadedSettingData)
		End If
		Return oResult
	End Function





    ''' <summary>
    ''' return vehicle types(selected on lenderside) available for consumer
    ''' </summary>
    ''' <param name="poConfig"></param>
    ''' <returns></returns>
    ''' <remarks>Usage:Dim VehicleTypes As List(Of DownloadedSettings.CListItem) = CDownloadSettingData.VehicleTypes(_CurrentWebsiteConfig)
    '''</remarks>
    Public Shared Function VehicleTypes(ByVal poConfig As CWebsiteConfig) As List(Of CListItem)
        Dim sUrl = poConfig.BaseSubmitLoanUrl & "/resources.ashx/orgs/" & poConfig.OrgCode & "/fis/" & poConfig.LenderCode & "/settings/vl/lists/vehicle-types"
        If poConfig.BaseSubmitLoanUrl_TPV <> "" Then
            sUrl = poConfig.BaseSubmitLoanUrl_TPV
            sUrl = String.Format(sUrl & "/resources.ashx/orgs/" & poConfig.OrgCode & "/fis/" & poConfig.LenderCode & "/settings/vl/lists/vehicle-types?OrgCode2={0}&LenderCode2={1}", poConfig.OrgCode2, poConfig.LenderCode2)
        End If

        Dim getData As Func(Of CDownloadSettingData, List(Of CListItem)) = Function(x) x._VehicleTypes
        Dim setData As Func(Of CDownloadSettingData, List(Of CListItem), CDownloadSettingData) = Function(x, data)
                                                                                                     x._VehicleTypes = data
                                                                                                     Return x
                                                                                                 End Function
        Dim getDataFlag As Func(Of CDownloadSettingData, String) = Function(x) x._HasVehicleTypes
        Dim setDataFlag As Func(Of CDownloadSettingData, String, CDownloadSettingData) = Function(x, data)
                                                                                             x._HasVehicleTypes = data
                                                                                             Return x
                                                                                         End Function
        Return DownloadSettings(poConfig, "VehicleTypes", sUrl, getData, setData, getDataFlag, setDataFlag)

    End Function

    ''' <summary>
    ''' return motorcycle makes object
    ''' </summary>
    ''' <param name="poConfig"></param>
    ''' <returns></returns>
    ''' <remarks>Usage:Dim MotorCycleMakes As List(Of DownloadedSettings.CListItem) = CDownloadSettingData.MotorCycleMakes(_CurrentWebsiteConfig)
    '''</remarks>
    Public Shared Function MotorCycleMakes(ByVal poConfig As CWebsiteConfig) As List(Of CListItem)
        Dim sUrl = poConfig.BaseSubmitLoanUrl & "/resources.ashx/orgs/" & poConfig.OrgCode & "/fis/" & poConfig.LenderCode & "/settings/vl/lists/motorcycle-makes"
        If poConfig.BaseSubmitLoanUrl_TPV <> "" Then
            sUrl = poConfig.BaseSubmitLoanUrl_TPV
            sUrl = String.Format(sUrl & "/resources.ashx/orgs/" & poConfig.OrgCode & "/fis/" & poConfig.LenderCode & "/settings/vl/lists/motorcycle-makes?OrgCode2={0}&LenderCode2={1}", poConfig.OrgCode2, poConfig.LenderCode2)
        End If

        Dim getData As Func(Of CDownloadSettingData, List(Of CListItem)) = Function(x) x._MotorCycleMakes
        Dim setData As Func(Of CDownloadSettingData, List(Of CListItem), CDownloadSettingData) = Function(x, data)
                                                                                                     x._MotorCycleMakes = data
                                                                                                     Return x
                                                                                                 End Function
        Dim getDataFlag As Func(Of CDownloadSettingData, String) = Function(x) x._HasMotorCycleMakes
        Dim setDataFlag As Func(Of CDownloadSettingData, String, CDownloadSettingData) = Function(x, data)
                                                                                             x._HasMotorCycleMakes = data
                                                                                             Return x
                                                                                         End Function
        Return DownloadSettings(poConfig, "MotorCycleMakes", sUrl, getData, setData, getDataFlag, setDataFlag)

    End Function

    ''' <summary>
    ''' return car/truck makes
    ''' </summary>
    ''' <param name="poConfig"></param>
    ''' <returns></returns>
    ''' <remarks>Usage:Dim MotorCycleMakes As List(Of DownloadedSettings.CListItem) = CDownloadSettingData.MotorCycleMakes(_CurrentWebsiteConfig)
    '''</remarks>
    Public Shared Function CarMakes(ByVal poConfig As CWebsiteConfig) As List(Of CListItem)
        Dim sUrl = poConfig.BaseSubmitLoanUrl & "/resources.ashx/orgs/" & poConfig.OrgCode & "/fis/" & poConfig.LenderCode & "/settings/vl/lists/car-makes"
        If poConfig.BaseSubmitLoanUrl_TPV <> "" Then
            sUrl = poConfig.BaseSubmitLoanUrl_TPV
            sUrl = String.Format(sUrl & "/resources.ashx/orgs/" & poConfig.OrgCode & "/fis/" & poConfig.LenderCode & "/settings/vl/lists/car-makes?OrgCode2={0}&LenderCode2={1}", poConfig.OrgCode2, poConfig.LenderCode2)
        End If
        Dim getData As Func(Of CDownloadSettingData, List(Of CListItem)) = Function(x) x._CarMakes
        Dim setData As Func(Of CDownloadSettingData, List(Of CListItem), CDownloadSettingData) = Function(x, data)
                                                                                                     x._CarMakes = data
                                                                                                     Return x
                                                                                                 End Function
        Dim getDataFlag As Func(Of CDownloadSettingData, String) = Function(x) x._HasCarMakes
        Dim setDataFlag As Func(Of CDownloadSettingData, String, CDownloadSettingData) = Function(x, data)
                                                                                             x._HasCarMakes = data
                                                                                             Return x
                                                                                         End Function
        Return DownloadSettings(poConfig, "CarMakes", sUrl, getData, setData, getDataFlag, setDataFlag)
    End Function

    ''' <summary>
    ''' return ID cards
    ''' </summary>
    ''' <param name="poConfig"></param>
    ''' <returns></returns>
    ''' <remarks>Usage:Dim IDCards As List(Of DownloadedSettings.CIDCardListItem) = CDownloadSettingData.IDCards(_CurrentWebsiteConfig)
    '''</remarks>
    Public Shared Function IDCards(ByVal poConfig As CWebsiteConfig) As List(Of CIDCardListItem)
        Dim sUrl = poConfig.BaseSubmitLoanUrl & "/resources.ashx/orgs/" & poConfig.OrgCode & "/fis/" & poConfig.LenderCode & "/settings/vl/lists/id-card-types"
        Dim getData As Func(Of CDownloadSettingData, List(Of CIDCardListItem)) = Function(x) x._IDCards
        Dim setData As Func(Of CDownloadSettingData, List(Of CIDCardListItem), CDownloadSettingData) = Function(x, data)
                                                                                                           x._IDCards = data
                                                                                                           Return x
                                                                                                       End Function
        Dim getDataFlag As Func(Of CDownloadSettingData, String) = Function(x) x._HasIDCards
        Dim setDataFlag As Func(Of CDownloadSettingData, String, CDownloadSettingData) = Function(x, data)
                                                                                             x._HasIDCards = data
                                                                                             Return x
                                                                                         End Function
        Return DownloadSettings(poConfig, "IDCards", sUrl, getData, setData, getDataFlag, setDataFlag, "CIDCardOutput")


    End Function


    Public Shared Function PersonalLoanPurposes(ByVal poConfig As CWebsiteConfig) As List(Of CListItem)
        Dim sUrl = poConfig.BaseSubmitLoanUrl & "/resources.ashx/orgs/" & poConfig.OrgCode & "/fis/" & poConfig.LenderCode & "/settings/pl/lists/loan-purposes"
        If poConfig.BaseSubmitLoanUrl_TPV <> "" Then
            sUrl = poConfig.BaseSubmitLoanUrl_TPV
            sUrl = String.Format(sUrl & "/resources.ashx/orgs/" & poConfig.OrgCode & "/fis/" & poConfig.LenderCode & "/settings/pl/lists/loan-purposes?OrgCode2={0}&LenderCode2={1}", poConfig.OrgCode2, poConfig.LenderCode2)
        End If

        Dim getData As Func(Of CDownloadSettingData, List(Of CListItem)) = Function(x) x._PersonalLoanPurposes
		Dim setData As Func(Of CDownloadSettingData, List(Of CListItem), CDownloadSettingData) = Function(x, data)
																										   x._PersonalLoanPurposes = data
																										   Return x
																									   End Function
		Dim getDataFlag As Func(Of CDownloadSettingData, String) = Function(x) x._HasPersonalLoanPurposes
		Dim setDataFlag As Func(Of CDownloadSettingData, String, CDownloadSettingData) = Function(x, data)
																								   x._HasPersonalLoanPurposes = data
																								   Return x
																							   End Function
		Return DownloadSettings(poConfig, "PersonalLoanPurposes", sUrl, getData, setData, getDataFlag, setDataFlag)
	End Function





    Public Shared Function VehicleLoanPurposes(ByVal poConfig As CWebsiteConfig) As List(Of CListItem)
        Dim sUrl = poConfig.BaseSubmitLoanUrl & "/resources.ashx/orgs/" & poConfig.OrgCode & "/fis/" & poConfig.LenderCode & "/settings/vl/lists/loan-purposes"
        If poConfig.BaseSubmitLoanUrl_TPV <> "" Then
            sUrl = poConfig.BaseSubmitLoanUrl_TPV
            sUrl = String.Format(sUrl & "/resources.ashx/orgs/" & poConfig.OrgCode & "/fis/" & poConfig.LenderCode & "/settings/vl/lists/loan-purposes?OrgCode2={0}&LenderCode2={1}", poConfig.OrgCode2, poConfig.LenderCode2)
        End If
        Dim getData As Func(Of CDownloadSettingData, List(Of CListItem)) = Function(x) x._VehicleLoanPurposes
        Dim setData As Func(Of CDownloadSettingData, List(Of CListItem), CDownloadSettingData) = Function(x, data)
                                                                                                     x._VehicleLoanPurposes = data
                                                                                                     Return x
                                                                                                 End Function
        Dim getDataFlag As Func(Of CDownloadSettingData, String) = Function(x) x._HasVehicleLoanPurposes
        Dim setDataFlag As Func(Of CDownloadSettingData, String, CDownloadSettingData) = Function(x, data)
                                                                                             x._HasVehicleLoanPurposes = data
                                                                                             Return x
                                                                                         End Function
        Return DownloadSettings(poConfig, "VehicleLoanPurposes", sUrl, getData, setData, getDataFlag, setDataFlag)

    End Function


    Public Shared Function CreditCardPurposes(ByVal poConfig As CWebsiteConfig) As List(Of CListItem)
        Dim sUrl = poConfig.BaseSubmitLoanUrl & "/resources.ashx/orgs/" & poConfig.OrgCode & "/fis/" & poConfig.LenderCode & "/settings/cc/lists/loan-purposes"
        If poConfig.BaseSubmitLoanUrl_TPV <> "" Then
            sUrl = poConfig.BaseSubmitLoanUrl_TPV
            sUrl = String.Format(sUrl & "/resources.ashx/orgs/" & poConfig.OrgCode & "/fis/" & poConfig.LenderCode & "/settings/cc/lists/loan-purposes?OrgCode2={0}&LenderCode2={1}", poConfig.OrgCode2, poConfig.LenderCode2)
        End If
        Dim getData As Func(Of CDownloadSettingData, List(Of CListItem)) = Function(x) x._CreditCardPurposes
        Dim setData As Func(Of CDownloadSettingData, List(Of CListItem), CDownloadSettingData) = Function(x, data)
                                                                                                     x._CreditCardPurposes = data
                                                                                                     Return x
                                                                                                 End Function
        Dim getDataFlag As Func(Of CDownloadSettingData, String) = Function(x) x._HasCreditCardPurposes
        Dim setDataFlag As Func(Of CDownloadSettingData, String, CDownloadSettingData) = Function(x, data)
                                                                                             x._HasCreditCardPurposes = data
                                                                                             Return x
                                                                                         End Function
        Return DownloadSettings(poConfig, "CreditCardPurposes", sUrl, getData, setData, getDataFlag, setDataFlag)

    End Function

    Public Shared Function HomeEquityPurposes(ByVal poConfig As CWebsiteConfig) As List(Of CListItem)
        Dim sUrl = poConfig.BaseSubmitLoanUrl & "/resources.ashx/orgs/" & poConfig.OrgCode & "/fis/" & poConfig.LenderCode & "/settings/he/lists/loan-purposes"
        If poConfig.BaseSubmitLoanUrl_TPV <> "" Then
            sUrl = poConfig.BaseSubmitLoanUrl_TPV
            sUrl = String.Format(sUrl & "/resources.ashx/orgs/" & poConfig.OrgCode & "/fis/" & poConfig.LenderCode & "/settings/he/lists/loan-purposes?OrgCode2={0}&LenderCode2={1}", poConfig.OrgCode2, poConfig.LenderCode2)
        End If
        Dim getData As Func(Of CDownloadSettingData, List(Of CListItem)) = Function(x) x._HomeEquityPurposes
        Dim setData As Func(Of CDownloadSettingData, List(Of CListItem), CDownloadSettingData) = Function(x, data)
                                                                                                     x._HomeEquityPurposes = data
                                                                                                     Return x
                                                                                                 End Function
        Dim getDataFlag As Func(Of CDownloadSettingData, String) = Function(x) x._HasHomeEquityPurposes
        Dim setDataFlag As Func(Of CDownloadSettingData, String, CDownloadSettingData) = Function(x, data)
                                                                                             x._HasHomeEquityPurposes = data
                                                                                             Return x
                                                                                         End Function
        Return DownloadSettings(poConfig, "HomeEquityPurposes", sUrl, getData, setData, getDataFlag, setDataFlag)
    End Function

    Public Shared Function BusinessPurposes(ByVal poConfig As CWebsiteConfig) As List(Of CListItem)
        Dim sUrl = poConfig.BaseSubmitLoanUrl & "/resources.ashx/orgs/" & poConfig.OrgCode & "/fis/" & poConfig.LenderCode & "/settings/bl/lists/loan-purposes"
        Dim getData As Func(Of CDownloadSettingData, List(Of CListItem)) = Function(x) x._BusinessPurposes
        Dim setData As Func(Of CDownloadSettingData, List(Of CListItem), CDownloadSettingData) = Function(x, data)
                                                                                                     x._BusinessPurposes = data
                                                                                                     Return x
                                                                                                 End Function
        Dim getDataFlag As Func(Of CDownloadSettingData, String) = Function(x) x._HasBusinessPurposes
        Dim setDataFlag As Func(Of CDownloadSettingData, String, CDownloadSettingData) = Function(x, data)
                                                                                             x._HasBusinessPurposes = data
                                                                                             Return x
                                                                                         End Function
        Return DownloadSettings(poConfig, "BusinessPurposes", sUrl, getData, setData, getDataFlag, setDataFlag)
    End Function


    ''' <summary>
    ''' return branches
    ''' </summary>
    ''' <param name="poConfig"></param>
    ''' <returns></returns>
    ''' <remarks>Usage:Dim Branches As List(Of DownloadedSettings.CBranch) = CDownloadSettingData.Branches(_CurrentWebsiteConfig)
    ''' in-branch setting, each branch should have a branchcode
    '''</remarks>
    Public Shared Function Branches(ByVal poConfig As CWebsiteConfig) As List(Of CBranch)
		Dim oResult As New List(Of CBranch)
		If poConfig.OrgCode = "" Or poConfig.LenderCode = "" Then
			_log.Error(poConfig.LenderId & " doesn't have org code or lender code")
			Return oResult
		End If

		Dim downloadSettingData As CDownloadSettingData
		Dim sCacheKey As String = "DOWNLOADED_SETTINS_" & poConfig.CacheIDSuffix
		downloadSettingData = CType(HttpContext.Current.Cache.Get(sCacheKey), CDownloadSettingData)

		Try
			If downloadSettingData IsNot Nothing AndAlso downloadSettingData._Branches IsNot Nothing Then
				_log.Info("_Branches Object " & sCacheKey & " was retrieved from cache. The cache " & "was populated at " & HttpContext.Current.Application(sCacheKey).ToString())
				oResult = downloadSettingData._Branches
				Exit Try
			End If
			SyncLock GetLenderLockCollection(poConfig).DownloadSettingsLock
				' Ensure that the data was not loaded by a concurrent thread 
				' while waiting for lock.
				downloadSettingData = CType(HttpContext.Current.Cache.Get(sCacheKey), CDownloadSettingData)
				If downloadSettingData IsNot Nothing AndAlso downloadSettingData._Branches IsNot Nothing Then
					_log.Info("_Branches Object " & sCacheKey & " was retrieved from cache. The cache " & "was populated at " & HttpContext.Current.Application(sCacheKey).ToString())
					oResult = downloadSettingData._Branches
				Else
					If downloadSettingData IsNot Nothing AndAlso downloadSettingData._HasBranches = "NO" Then
						oResult = New List(Of CBranch)
						Exit Try
					End If
					If downloadSettingData Is Nothing Then
						downloadSettingData = New CDownloadSettingData
					End If

					'TODO: check and get cached data from DB
					Dim downloadedSettingsFromDb = ReadCacheDataFromDatabase(Of CDownloadSettingData)(sCacheKey)
					If downloadedSettingsFromDb IsNot Nothing AndAlso Not String.IsNullOrEmpty(downloadedSettingsFromDb._HasBranches) Then
						If downloadedSettingsFromDb._Branches IsNot Nothing AndAlso downloadedSettingsFromDb._Branches.Count > 0 Then
							oResult = downloadedSettingsFromDb._Branches
							downloadSettingData._Branches = oResult
							downloadSettingData._HasBranches = "YES"
						Else
							oResult = New List(Of CBranch)
							downloadSettingData._HasBranches = "NO"
						End If
					Else

                        'Get summary for all branches 				
                        Dim sUrl = poConfig.BaseSubmitLoanUrl & "/resources.ashx/orgs/" & poConfig.OrgCode & "/fis/" & poConfig.LenderCode & "/branches"
                        Dim sURL_GET As String = sUrl
						If poConfig.SubmitLoanUrl_TPV <> "" AndAlso poConfig.SubmitLoanUrl_TPV = poConfig.SubmitLoanUrl Then
							sURL_GET = String.Format(sURL_GET & "?OrgCode2={0}&LenderCode2={1}", poConfig.OrgCode2, poConfig.LenderCode2)
						End If
						Dim sReponse = Common.SubmitRestApiGet(poConfig, sURL_GET)

						'Get detail for each branch
						Dim xmlDoc As New XmlDocument
						xmlDoc.LoadXml(sReponse)
						Dim branchNodes = xmlDoc.GetElementsByTagName("BRANCH")
						If branchNodes IsNot Nothing AndAlso branchNodes.Count > 0 Then
							Dim oDeserializedOutput As CBranch
							Dim serializer As New XmlSerializer(GetType(CBranch))

							For Each branch As XmlNode In branchNodes
								Dim sBranchCode As String = branch.SelectSingleNode("BRANCH_CODE").InnerText
								If sBranchCode = "" Then Continue For
								sURL_GET = sUrl & "/" & sBranchCode & "/settings"
								If poConfig.SubmitLoanUrl_TPV <> "" AndAlso poConfig.SubmitLoanUrl_TPV = poConfig.SubmitLoanUrl Then
									sURL_GET = String.Format(sURL_GET & "?OrgCode2={0}&LenderCode2={1}", poConfig.OrgCode2, poConfig.LenderCode2)
								End If
								sReponse = Common.SubmitRestApiGet(poConfig, sURL_GET)

								If String.IsNullOrEmpty(sReponse) Then	''this is need so this process can continue and cache other questions to cache.  Don't wnat to tram the traffic if this process is run many times
									_log.WarnFormat("Can't download question branch:{0}.  branch name on backoffice may need to be fixed by removing any special characters", sBranchCode)
									Continue For
								End If

								Using reader As TextReader = New StringReader(sReponse)
									oDeserializedOutput = CType(serializer.Deserialize(reader), CBranch)
								End Using
								oResult.Add(oDeserializedOutput)
							Next

							If oResult IsNot Nothing AndAlso oResult.Count > 0 Then	  ' count less than 0 mean there is probably error in download so don't wnat to store in session
								_log.Info("_Branches Object " & sCacheKey & " was created. ")
								downloadSettingData._Branches = oResult
								downloadSettingData._HasBranches = "YES"
							Else
								oResult = New List(Of CBranch)
								downloadSettingData._HasBranches = "NO"
							End If
						Else
							oResult = New List(Of CBranch)
							downloadSettingData._HasBranches = "NO"
						End If
						SaveCacheDataToDatabase(sCacheKey, downloadSettingData)
					End If
					SetCache(sCacheKey, downloadSettingData)
				End If

			End SyncLock
		Catch ex As Exception
			_log.Error("Can't get _Branches", ex)
		End Try
		Return oResult
	End Function

	'----Custom Question XML sample
	'	<OUTPUT xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
	'	<CUSTOM_QUESTIONS>
	'		<!--posible value for app_source: consumer_lender, consumer_lender_conditional, consumer, lender-->
	'		<CUSTOM_QUESTION is_active="true" is_required="true" app_source="consumer_lender">
	'			<NAME>Fee 150.00</NAME>
	'			<POSITION>2</POSITION>
	'			<TEXT>I agree to accept a $150.00 processing fee for this loan.</TEXT>
	'			<APP_TYPES_SUPPORTED>
	'				<APP_TYPE>HE</APP_TYPE>
	'			</APP_TYPES_SUPPORTED>
	'			<ANSWER_TYPE>CHECKBOX</ANSWER_TYPE>
	'			<VALIDATION_ERROR_MSG/>
	'			<CHECKBOX list_columns="2">
	'				<RESTRICTED_ANSWERS>
	'					<RESTRICTED_ANSWER text="Accept" value="Accept" position="0"/>
	'				</RESTRICTED_ANSWERS>
	'			</CHECKBOX>
	'		</CUSTOM_QUESTION>

	'		<CUSTOM_QUESTION is_active="true" is_required="true" app_source="lender">
	'			<NAME>MSR Info</NAME>
	'			<POSITION>2</POSITION>
	'			<TEXT>MSR Initials and Teller Number</TEXT>
	'			<APP_TYPES_SUPPORTED>
	'				<APP_TYPE>CC</APP_TYPE>
	'				<APP_TYPE>HE</APP_TYPE>
	'				<APP_TYPE>PL</APP_TYPE>
	'				<APP_TYPE>VL</APP_TYPE>
	'			</APP_TYPES_SUPPORTED>
	'			<ANSWER_TYPE>TEXTBOX</ANSWER_TYPE>
	'			<VALIDATION_ERROR_MSG/>
	'		</CUSTOM_QUESTION>

	'		<CUSTOM_QUESTION is_active="true" is_required="false" app_source="consumer_lender">
	'			<NAME>Source of Deposit</NAME>
	'			<POSITION>4</POSITION>
	'			<TEXT>Source of Funds of initial deposit:</TEXT>
	'			<APP_TYPES_SUPPORTED>
	'				<APP_TYPE>CC</APP_TYPE>
	'				<APP_TYPE>XA</APP_TYPE>
	'			</APP_TYPES_SUPPORTED>
	'			<XA_OPTIONS>
	'			   <!--PRIMARY, SECONDARY, PRIMARY_SECONDARY-->
	'				<AVAIL_FOR_ACCOUNT_TYPE>PRIMARY</AVAIL_FOR_ACCOUNT_TYPE>
	'				<ENTITY_TYPES_SUPPORTED>
	'					<ENTITY_TYPE>PERSONAL</ENTITY_TYPE>
	'				</ENTITY_TYPES_SUPPORTED>
	'				<SPECIAL_ACCOUNT_TYPES_SUPPORTED xsi:nil="true"/>
	'			</XA_OPTIONS>
	'			<ANSWER_TYPE>DROPDOWN</ANSWER_TYPE>
	'			<VALIDATION_ERROR_MSG/>
	'			<DROPDOWN>
	'				<RESTRICTED_ANSWERS>
	'					<RESTRICTED_ANSWER position="0"/>
	'					<RESTRICTED_ANSWER text="Check" value="Check" position="1"/>
	'					<RESTRICTED_ANSWER text="Wire" value="Wire" position="2"/>
	'					<RESTRICTED_ANSWER text="Payroll" value="Payroll" position="3"/>
	'					<RESTRICTED_ANSWER text="Cash" value="Cash" position="4"/>
	'					<RESTRICTED_ANSWER text="ACH" value="ACH" position="5"/>
	'					<RESTRICTED_ANSWER text="Combination" value="Combination" position="6"/>
	'				</RESTRICTED_ANSWERS>
	'			</DROPDOWN>
	'		</CUSTOM_QUESTION>
	'	</CUSTOM_QUESTIONS>			
	'</OUTPUT>

	Private Shared Function DownloadCqQuestions(ByVal poConfig As CWebsiteConfig, propName As String, sUrl As String, getData As Func(Of CDownloadSettingData, XmlDocument), setData As Func(Of CDownloadSettingData, XmlDocument, CDownloadSettingData), getDataFlag As Func(Of CDownloadSettingData, String), setDataFlag As Func(Of CDownloadSettingData, String, CDownloadSettingData)) As XmlDocument
		Dim oResult As New XmlDocument
		If Common.getVisibleAttribute(poConfig, "custom_question_new_api") <> "Y" Then
			Return oResult
		End If

		If poConfig.OrgCode = "" Or poConfig.LenderCode = "" Then
			_log.Error(poConfig.LenderId & " doesn't have org code or lender code")
			Return oResult
		End If

		Dim downloadedSettings As CDownloadSettingData
		Dim sCacheKey As String = "DOWNLOADED_SETTINS_" & poConfig.CacheIDSuffix
		downloadedSettings = CType(HttpContext.Current.Cache.Get(sCacheKey), CDownloadSettingData)

		Try
			If downloadedSettings IsNot Nothing AndAlso getData(downloadedSettings) IsNot Nothing Then
				'_log.Info(propName &" Object " & sCacheKey & " was retrieved from cache. The cache was populated at " & HttpContext.Current.Application(sCacheKey).ToString())
				oResult = getData(downloadedSettings)
				Exit Try
			End If
			SyncLock GetLenderLockCollection(poConfig).DownloadSettingsLock
				' Ensure that the data was not loaded by a concurrent thread 
				' while waiting for lock.
				downloadedSettings = CType(HttpContext.Current.Cache.Get(sCacheKey), CDownloadSettingData)
				If downloadedSettings IsNot Nothing AndAlso getData(downloadedSettings) IsNot Nothing Then
					'_log.Info(propName &" Object " & sCacheKey & " was retrieved from cache. The cache was populated at " & HttpContext.Current.Application(sCacheKey).ToString())
					oResult = getData(downloadedSettings)
				Else
					If downloadedSettings IsNot Nothing AndAlso getDataFlag(downloadedSettings) = "NO" Then
						oResult = New XmlDocument
						Exit Try
					End If
					If downloadedSettings Is Nothing Then
						downloadedSettings = New CDownloadSettingData
					End If


					'TODO: check and get cached data from DB
					Dim downloadedSettingsFromDb = ReadCacheDataFromDatabase(Of CDownloadSettingData)(sCacheKey)
					If downloadedSettingsFromDb IsNot Nothing AndAlso Not String.IsNullOrEmpty(getDataFlag(downloadedSettingsFromDb)) Then
						If getData(downloadedSettingsFromDb) IsNot Nothing Then
							oResult = getData(downloadedSettingsFromDb)
							downloadedSettings = setData(downloadedSettings, oResult)
							downloadedSettings = setDataFlag(downloadedSettings, "YES")
						Else
							oResult = New XmlDocument()
							downloadedSettings = setDataFlag(downloadedSettings, "NO")
						End If
					Else
						'step 1 - get all application questions
						Dim sURL_GET As String = sUrl
						If poConfig.BaseSubmitLoanUrl_TPV <> "" AndAlso poConfig.BaseSubmitLoanUrl_TPV = poConfig.BaseSubmitLoanUrl Then 'only loan module is enabled and not using LPQ LOS
							sURL_GET = String.Format(sURL_GET & "?OrgCode2={0}&LenderCode2={1}", poConfig.OrgCode2, poConfig.LenderCode2)
						End If
						Dim sReponse = Common.SubmitRestApiGet(poConfig, sURL_GET)

						Dim xmlDoc As New XmlDocument
						xmlDoc.LoadXml(sReponse)
						Dim customQuestionNodes = xmlDoc.GetElementsByTagName("CUSTOM_QUESTION")
						If customQuestionNodes IsNot Nothing AndAlso customQuestionNodes.Count > 0 Then
							'2 - get detail for each question to extract VALIDATION_ERROR_MSG and ANSWER(DROPDOWN/CHECKBOX/TEXT) and append it to the custom question
							'should skip when <ANSWER_TYPE>HEADER</ANSWER_TYPE>
							For Each question As XmlNode In customQuestionNodes
								Dim validationErrorMsgNodes As XmlNode = question.SelectSingleNode("VALIDATION_ERROR_MSG")
								If validationErrorMsgNodes IsNot Nothing Then
									Exit For ''the webservice has been enhanced to include the detail so don't need to go throught this loop
								End If

								If CType(question, XmlElement).GetAttribute("is_active") = "false" Then Continue For

								Dim sQuestionName As String = question.SelectSingleNode("NAME").InnerText

								''need to fixed this issue in the lender side in the future
								If sQuestionName.Contains("?") Or sQuestionName.Contains("/") Or sQuestionName.Contains("&") Then
									'sQuestionName = HttpContext.Current.Server.UrlEncode(sQuestionName)
									' sQuestionName = sQuestionName.Replace("?", "%3f")
									Continue For
								End If

								Dim sAnswerType As String = question.SelectSingleNode("ANSWER_TYPE").InnerText
								If sAnswerType = "HEADER" Then Continue For

								Dim sURL_GET2 As String = sUrl & "/" & sQuestionName
								If poConfig.BaseSubmitLoanUrl_TPV <> "" AndAlso poConfig.BaseSubmitLoanUrl_TPV = poConfig.BaseSubmitLoanUrl Then 'only loan module is enabled and not using LPQ LOS
									sURL_GET2 = String.Format(sURL_GET2 & "?OrgCode2={0}&LenderCode2={1}", poConfig.OrgCode2, poConfig.LenderCode2)
								End If

								sReponse = Common.SubmitRestApiGet(poConfig, sURL_GET2)

								If String.IsNullOrEmpty(sReponse) Then	''this is need so this process can continue and cache other questions to cache.  Don't wnat to tram the traffic if this process is run many times
									_log.DebugFormat("Can't download question name:{0}.  question name on backoffice may need to be fixed by removing any special characters", sQuestionName)
									Continue For
								End If

								Dim xmlDetailDoc As New XmlDocument
								xmlDetailDoc.LoadXml(sReponse)
								Dim validationErrorMessage As XmlNodeList = xmlDetailDoc.GetElementsByTagName("VALIDATION_ERROR_MSG")
								Dim newValidationErrorMessageNode As XmlNode = xmlDoc.ImportNode(validationErrorMessage(0), True)
								question.AppendChild(newValidationErrorMessageNode)
								If sAnswerType = "TEXTBOX" Then sAnswerType = "FREEFORM_TEXTBOX"
								Dim answerNodes As XmlNodeList = xmlDetailDoc.GetElementsByTagName(sAnswerType)
								If answerNodes IsNot Nothing AndAlso answerNodes(0) IsNot Nothing Then
									question.AppendChild(xmlDoc.ImportNode(answerNodes(0), True))
								End If
							Next
							oResult = xmlDoc


							If Not String.IsNullOrEmpty(oResult.OuterXml) Then	  '
								_log.Info(propName & " Object " & sCacheKey & " was created.  " & oResult.OuterXml)
								downloadedSettings = setData(downloadedSettings, oResult)
								downloadedSettings = setDataFlag(downloadedSettings, "YES")
							Else
								oResult = New XmlDocument()
								downloadedSettings = setDataFlag(downloadedSettings, "NO")
							End If
						Else
							oResult = New XmlDocument()
							downloadedSettings = setDataFlag(downloadedSettings, "NO")
						End If
						SaveCacheDataToDatabase(sCacheKey, downloadedSettings)
					End If
					SetCache(sCacheKey, downloadedSettings)
				End If
			End SyncLock
		Catch ex As Exception
			_log.Error("Can't get " & propName, ex)
		End Try
		Return oResult
	End Function





    ''' <summary>
    ''' return custom application questions
    ''' </summary>
    ''' <param name="poConfig"></param>
    ''' <returns></returns>
    ''' <remarks>Usage: Dim CustomApplicationQuestions As xmlDocument = CDownloadSettingData.CustomApplicationQuestions(_CurrentWebsiteConfig)
    '''</remarks>
    Public Shared Function CustomApplicationQuestions(ByVal poConfig As CWebsiteConfig) As XmlDocument
        Dim sUrl = poConfig.BaseSubmitLoanUrl & "/resources.ashx/orgs/" & poConfig.OrgCode & "/fis/" & poConfig.LenderCode & "/settings/custom-questions/application"
        Dim getData As Func(Of CDownloadSettingData, XmlDocument) = Function(x) x._CustomApplicationQuestions
        Dim setData As Func(Of CDownloadSettingData, XmlDocument, CDownloadSettingData) = Function(x, data)
                                                                                              x._CustomApplicationQuestions = data
                                                                                              Return x
                                                                                          End Function
        Dim getDataFlag As Func(Of CDownloadSettingData, String) = Function(x) x._HasApplicationQuestions
        Dim setDataFlag As Func(Of CDownloadSettingData, String, CDownloadSettingData) = Function(x, data)
                                                                                             x._HasApplicationQuestions = data
                                                                                             Return x
                                                                                         End Function
        Return DownloadCqQuestions(poConfig, "CustomApplicationQuestions", sUrl, getData, setData, getDataFlag, setDataFlag)
    End Function


    ''' <summary>
    ''' return custom applicant questions
    ''' </summary>
    ''' <param name="poConfig"></param>
    ''' <returns></returns>
    ''' <remarks>Usage: Dim CustomApplicantQuestions As xmlDocument = CDownloadSettingData.CustomApplicantQuestions(_CurrentWebsiteConfig)
    '''</remarks>
    Public Shared Function CustomApplicantQuestions(ByVal poConfig As CWebsiteConfig) As XmlDocument
        Dim sUrl = poConfig.BaseSubmitLoanUrl & "/resources.ashx/orgs/" & poConfig.OrgCode & "/fis/" & poConfig.LenderCode & "/settings/custom-questions/applicant"
        Dim getData As Func(Of CDownloadSettingData, XmlDocument) = Function(x) x._CustomApplicantQuestions
        Dim setData As Func(Of CDownloadSettingData, XmlDocument, CDownloadSettingData) = Function(x, data)
                                                                                              x._CustomApplicantQuestions = data
                                                                                              Return x
                                                                                          End Function
        Dim getDataFlag As Func(Of CDownloadSettingData, String) = Function(x) x._HasApplicantQuestions
        Dim setDataFlag As Func(Of CDownloadSettingData, String, CDownloadSettingData) = Function(x, data)
                                                                                             x._HasApplicantQuestions = data
                                                                                             Return x
                                                                                         End Function
        Return DownloadCqQuestions(poConfig, "CustomApplicantQuestions", sUrl, getData, setData, getDataFlag, setDataFlag)
    End Function


    Public Shared Function GetXAProducts(ByVal poConfig As CWebsiteConfig) As XmlDocument
		Dim oResult As New XmlDocument
		Dim docProductList As New XmlDocument
		Dim docProducts As New XmlDocument
		docProducts.LoadXml("<PRODUCTS></PRODUCTS>")
		If poConfig.OrgCode = "" Or poConfig.LenderCode = "" Then
			_log.Warn(poConfig.LenderId & " doesn't have org code or lender code")
			Return oResult
		End If
		Dim downloadSettingData As CDownloadSettingData
		Dim sCacheKey As String = "DOWNLOADED_SETTINS_" & poConfig.CacheIDSuffix
		downloadSettingData = CType(HttpContext.Current.Cache.Get(sCacheKey), CDownloadSettingData)

		Try
			If downloadSettingData IsNot Nothing AndAlso downloadSettingData._XAProducts IsNot Nothing Then
				'_log.Info("_CustomApplicantQuestions Object " & sCacheKey & " was retrieved from cache. The cache  was populated at " & HttpContext.Current.Application(sCacheKey).ToString())
				oResult = downloadSettingData._XAProducts
				Exit Try
			End If
			SyncLock GetLenderLockCollection(poConfig).DownloadSettingsLock
				' Ensure that the data was not loaded by a concurrent thread 
				' while waiting for lock.
				downloadSettingData = CType(HttpContext.Current.Cache.Get(sCacheKey), CDownloadSettingData)
				If downloadSettingData IsNot Nothing AndAlso downloadSettingData._XAProducts IsNot Nothing Then
					'_log.Info("XAProducts Object " & sCacheKey & " was retrieved from cache. The cache was populated at " & HttpContext.Current.Application(sCacheKey).ToString())
					oResult = downloadSettingData._XAProducts
				Else
					If downloadSettingData IsNot Nothing AndAlso downloadSettingData._HasXAProducts = "NO" Then
						oResult = New XmlDocument()
						Exit Try
					End If
					If downloadSettingData Is Nothing Then
						downloadSettingData = New CDownloadSettingData
					End If
					'TODO: check and get cached data from DB
					Dim downloadedSettingsFromDb = ReadCacheDataFromDatabase(Of CDownloadSettingData)(sCacheKey)
                    If downloadedSettingsFromDb IsNot Nothing AndAlso Not String.IsNullOrEmpty(downloadedSettingsFromDb._HasXAProducts) Then
                        If downloadedSettingsFromDb._XAProducts IsNot Nothing Then
                            oResult = downloadedSettingsFromDb._XAProducts
                            downloadSettingData._XAProducts = oResult
                            downloadSettingData._HasXAProducts = "YES"
                        Else
                            oResult = New XmlDocument()
                            downloadSettingData._HasXAProducts = "NO"
                        End If
                    Else
                        'use this for loan product
                        '<LPQRespondsTo("GET", "orgs/{orgcode}/fis/{lendercode}/settings/{apptype}/products", LpqAuthType.Officer, "Returns summay list of products", "", "~/Settings/Products/ProductList.xsd")>

                        Dim sUrl = poConfig.BaseSubmitLoanUrl & "/resources.ashx/orgs/" & poConfig.OrgCode & "/fis/" & poConfig.LenderCode & "/settings/xa/products"
                        Dim sReponse = Common.SubmitRestApiGet(poConfig, sUrl)

						docProductList.LoadXml(sReponse)
						Dim productNodes = docProductList.GetElementsByTagName("PRODUCT")
						If productNodes IsNot Nothing AndAlso productNodes.Count > 0 Then
							For Each node As XmlNode In productNodes
								Dim sProductCode As String = node.SelectSingleNode("PRODUCT_CODE").InnerText

								''need to fixed this issue in the lender side in the future
								If sProductCode.Contains("?") Or sProductCode.Contains("/") Or sProductCode.Contains("&") Then
									'sQuestionName = HttpContext.Current.Server.UrlEncode(sQuestionName)
									' sQuestionName = sQuestionName.Replace("?", "%3f")
									Continue For
								End If
								sReponse = Common.SubmitRestApiGet(poConfig, sUrl & "/" & sProductCode)

								If String.IsNullOrEmpty(sReponse) Then	''this is need so this process can continue and cache other questions to cache.  Don't wnat to tram the traffic if this process is run many times
									_log.DebugFormat("Can't download sProductCode :{0}.  question name on backoffice may need to be fixed by removing any special characters", sProductCode)
									Continue For
								End If

								Dim xmlDetailDoc As New XmlDocument
								xmlDetailDoc.LoadXml(sReponse)

								Dim productDetailNodes As XmlNodeList = xmlDetailDoc.GetElementsByTagName("PRODUCT")
								If productDetailNodes IsNot Nothing AndAlso productDetailNodes(0) IsNot Nothing Then
									Dim productsNode = docProducts.SelectSingleNode("PRODUCTS")
									productsNode.AppendChild(docProducts.ImportNode(productDetailNodes(0), True))
								End If
							Next
							oResult = docProducts

							If Not String.IsNullOrEmpty(oResult.OuterXml) Then	'
								_log.Info("_XAProduct Object " & sCacheKey & " was created.  " & oResult.OuterXml)
								downloadSettingData._XAProducts = oResult
								downloadSettingData._HasXAProducts = "YES"
							Else
								oResult = New XmlDocument()
								downloadSettingData._HasXAProducts = "NO"
							End If
						Else
							oResult = New XmlDocument()
							downloadSettingData._HasXAProducts = "NO"
						End If
						SaveCacheDataToDatabase(sCacheKey, downloadSettingData)
					End If
					SetCache(sCacheKey, downloadSettingData)
				End If
			End SyncLock
		Catch ex As Exception
			_log.Error("Can't get _XAProduct", ex)
		End Try
		Return oResult
	End Function


	Public Shared Function GetXAPreferences(ByVal poConfig As CWebsiteConfig) As XmlDocument
		Dim oResult As New XmlDocument
		If poConfig.OrgCode = "" Or poConfig.LenderCode = "" Then
			_log.Warn(poConfig.LenderId & " doesn't have org code or lender code")
			Return oResult
		End If

		Dim downloadSettingData As CDownloadSettingData
		Dim sCacheKey As String = "DOWNLOADED_SETTINS_" & poConfig.CacheIDSuffix
		downloadSettingData = CType(HttpContext.Current.Cache.Get(sCacheKey), CDownloadSettingData)

		Try
			If downloadSettingData IsNot Nothing AndAlso downloadSettingData._XAPreferences IsNot Nothing Then
				'_log.Info("_CustomApplicantQuestions Object " & sCacheKey & " was retrieved from cache. The cache was populated at " & HttpContext.Current.Application(sCacheKey).ToString())
				oResult = downloadSettingData._XAPreferences
				Exit Try
			End If
			SyncLock GetLenderLockCollection(poConfig).DownloadSettingsLock
				' Ensure that the data was not loaded by a concurrent thread 
				' while waiting for lock.
				downloadSettingData = CType(HttpContext.Current.Cache.Get(sCacheKey), CDownloadSettingData)
				If downloadSettingData IsNot Nothing AndAlso downloadSettingData._XAPreferences IsNot Nothing Then
					'_log.Info("_CustomApplicantQuestions Object " & sCacheKey & " was retrieved from cache. The cache was populated at " & HttpContext.Current.Application(sCacheKey).ToString())
					oResult = downloadSettingData._XAPreferences
				Else
					If downloadSettingData IsNot Nothing AndAlso downloadSettingData._HasXAReferences = "NO" Then
						oResult = New XmlDocument()
						Exit Try
					End If
					If downloadSettingData Is Nothing Then
						downloadSettingData = New CDownloadSettingData
					End If

					'TODO: check and get cached data from DB
					Dim downloadedSettingsFromDb = ReadCacheDataFromDatabase(Of CDownloadSettingData)(sCacheKey)
                    If downloadedSettingsFromDb IsNot Nothing AndAlso Not String.IsNullOrEmpty(downloadedSettingsFromDb._HasXAReferences) Then
                        If downloadedSettingsFromDb._XAProducts IsNot Nothing Then
                            oResult = downloadedSettingsFromDb._XAPreferences
                            downloadSettingData._XAPreferences = oResult
                            downloadSettingData._HasXAReferences = "YES"
                        Else
                            oResult = New XmlDocument()
                            downloadSettingData._HasXAReferences = "NO"
                        End If
                    Else
                        Dim sUrl = poConfig.BaseSubmitLoanUrl & "/resources.ashx/orgs/" & poConfig.OrgCode & "/fis/" & poConfig.LenderCode & "/settings/xa/preferences"
                        Dim sReponse = Common.SubmitRestApiGet(poConfig, sUrl)
						oResult.LoadXml(sReponse)
						Dim preferenceNodes = oResult.GetElementsByTagName("PREFERENCES")
						If preferenceNodes IsNot Nothing AndAlso preferenceNodes.Count > 0 Then
							If Not String.IsNullOrEmpty(oResult.OuterXml) Then	  '
								_log.Info("_XAPreferences Object" & sCacheKey & " was created.  " & oResult.OuterXml)
								downloadSettingData._XAPreferences = oResult
								downloadSettingData._HasXAReferences = "YES"
							Else
								oResult = New XmlDocument()
								downloadSettingData._HasXAReferences = "NO"
							End If
						Else
							oResult = New XmlDocument()
							downloadSettingData._HasXAReferences = "NO"
						End If
						SaveCacheDataToDatabase(sCacheKey, downloadSettingData)
					End If
					SetCache(sCacheKey, downloadSettingData)
				End If
			End SyncLock
		Catch ex As Exception
			_log.Error("Can't get _XAPreferences", ex)
		End Try
		Return oResult
	End Function


	'TODO: need to get all loan or refactor to support reflect
	'Public Shared Function GetHEPreferences(ByVal poConfig As CWebsiteConfig) As XmlDocument
	'	Dim oResult As New XmlDocument
	'	If poConfig.OrgCode = "" Or poConfig.LenderCode = "" Then
	'		_log.Error(poConfig.LenderRef & " doesn't have org code or lender code")
	'		Return oResult
	'	End If

	'	Dim DownloadedSettings As CDownloadedSettings
	'	Dim sCacheKey As String = "DOWNLOADED_SETTINS_" & poConfig.LenderRef
	'	DownloadedSettings = CType(HttpContext.Current.Cache.Get(sCacheKey), CDownloadedSettings)

	'	Try
	'		If DownloadedSettings IsNot Nothing AndAlso DownloadedSettings._HEPreferences IsNot Nothing Then
	'			'_log.Info("_CustomApplicantQuestions Object " & sCacheKey & " was retrieved from cache. The cache " & _
	'			'	"was populated at " & HttpContext.Current.Application(sCacheKey).ToString())
	'			oResult = DownloadedSettings._HEPreferences
	'			Exit Try
	'		End If
	'		SyncLock _CacheLock_DOWNLOADED_SETTINS
	'			' Ensure that the data was not loaded by a concurrent thread 
	'			' while waiting for lock.
	'			DownloadedSettings = CType(HttpContext.Current.Cache.Get(sCacheKey), CDownloadedSettings)
	'			If DownloadedSettings IsNot Nothing AndAlso DownloadedSettings._HEPreferences IsNot Nothing Then
	'				'_log.Info("_CustomApplicantQuestions Object " & sCacheKey & " was retrieved from cache. The cache " & _
	'				'"was populated at " & HttpContext.Current.Application(sCacheKey).ToString())
	'				oResult = DownloadedSettings._HEPreferences
	'				Exit Try
	'			End If
	'			If DownloadedSettings IsNot Nothing AndAlso Not DownloadedSettings._HasHEPreferences Then
	'				Return oResult
	'			End If
	'			Dim sUrl As String = poConfig.SubmitLoanUrl

	'			sUrl = sUrl.Substring(0, sUrl.IndexOf("/decisionloan/"))

	'			sUrl = sUrl & "/resources.ashx/orgs/" & poConfig.OrgCode & "/fis/" & poConfig.LenderCode & "/settings/he/preferences"
	'			Dim sReponse = Common.SubmitRestApiGet(poConfig, sUrl)

	'			oResult.LoadXml(sReponse)
	'			Dim PreferenceNodes = oResult.GetElementsByTagName("PREFERENCES")
	'			If PreferenceNodes Is Nothing OrElse PreferenceNodes.Count = 0 Then
	'				If DownloadedSettings Is Nothing Then
	'					DownloadedSettings = New CDownloadedSettings
	'				End If
	'				DownloadedSettings._HasHEPreferences = False
	'				HttpContext.Current.Cache.Insert(sCacheKey, DownloadedSettings, Nothing, System.Web.Caching.Cache.NoAbsoluteExpiration, New TimeSpan(22, 0, 0))	'12hour&45sec
	'				HttpContext.Current.Application(sCacheKey) = DateTime.Now
	'				Return New XmlDocument
	'			End If
	'			If oResult.OuterXml <> "" Then	  '
	'				If DownloadedSettings Is Nothing Then
	'					DownloadedSettings = New CDownloadedSettings
	'				End If
	'				_log.Info("_HEPreferences Object " & sCacheKey & " was created.  " & oResult.OuterXml)
	'				DownloadedSettings._HEPreferences = oResult
	'				HttpContext.Current.Cache.Insert(sCacheKey, DownloadedSettings, Nothing, System.Web.Caching.Cache.NoAbsoluteExpiration, New TimeSpan(22, 0, 0))	'12hour&45sec
	'				HttpContext.Current.Application(sCacheKey) = DateTime.Now
	'			End If
	'		End SyncLock
	'	Catch ex As Exception
	'		_log.Error("Can't get _HEPreferences", ex)
	'	End Try
	'	Return oResult
	'End Function

	Private Shared Function DownloadAccountTypes(ByVal poConfig As CWebsiteConfig, propName As String, getResponse As Func(Of String), getData As Func(Of CDownloadSettingData, XmlDocument), setData As Func(Of CDownloadSettingData, XmlDocument, CDownloadSettingData), getDataFlag As Func(Of CDownloadSettingData, String), setDataFlag As Func(Of CDownloadSettingData, String, CDownloadSettingData)) As XmlDocument
		Dim oResult As New XmlDocument
		If poConfig.OrgCode = "" Or poConfig.LenderCode = "" Then
			_log.Warn(poConfig.LenderId & " doesn't have org code or lender code")
			Return oResult
		End If
		Dim downloadSettingData As CDownloadSettingData
		Dim sCacheKey As String = "DOWNLOADED_SETTINS_" & poConfig.CacheIDSuffix
		downloadSettingData = CType(HttpContext.Current.Cache.Get(sCacheKey), CDownloadSettingData)

		Try
			If downloadSettingData IsNot Nothing AndAlso getData(downloadSettingData) IsNot Nothing Then
				'_log.Info(propName & " Object " & sCacheKey & " was retrieved from cache. The cache was populated at " & HttpContext.Current.Application(sCacheKey).ToString())
				oResult = getData(downloadSettingData)
				Exit Try
			End If
			SyncLock GetLenderLockCollection(poConfig).DownloadSettingsLock
				' Ensure that the data was not loaded by a concurrent thread 
				' while waiting for lock.
				downloadSettingData = CType(HttpContext.Current.Cache.Get(sCacheKey), CDownloadSettingData)
				If downloadSettingData IsNot Nothing AndAlso getData(downloadSettingData) IsNot Nothing Then
					'_log.Info(propName & " Object " & sCacheKey & " was retrieved from cache. The cache was populated at " & HttpContext.Current.Application(sCacheKey).ToString())
					oResult = getData(downloadSettingData)
				Else
					If downloadSettingData IsNot Nothing AndAlso getDataFlag(downloadSettingData) = "NO" Then
						oResult = New XmlDocument()
						Exit Try
					End If
					If downloadSettingData Is Nothing Then
						downloadSettingData = New CDownloadSettingData
					End If

					'TODO: check and get cached data from DB
					Dim downloadedSettingsFromDb = ReadCacheDataFromDatabase(Of CDownloadSettingData)(sCacheKey)
					If downloadedSettingsFromDb IsNot Nothing AndAlso Not String.IsNullOrEmpty(getDataFlag(downloadedSettingsFromDb)) Then
						If getData(downloadedSettingsFromDb) IsNot Nothing Then
							oResult = getData(downloadedSettingsFromDb)
							downloadSettingData = setData(downloadSettingData, oResult)
							downloadSettingData = setDataFlag(downloadSettingData, "YES")
						Else
							oResult = New XmlDocument()
							downloadSettingData = setDataFlag(downloadSettingData, "NO")
						End If
					Else
						Dim sReponse = getResponse()
						If Not String.IsNullOrEmpty(sReponse) Then
							oResult.LoadXml(sReponse)
						End If
						Dim settingExportResultNode = oResult.GetElementsByTagName("SETTINGS_EXPORT_RESULT")
						If settingExportResultNode IsNot Nothing AndAlso settingExportResultNode.Count > 0 Then
							_log.Info(propName & " Object" & sCacheKey & " was created.  " & oResult.OuterXml)
							downloadSettingData = setData(downloadSettingData, oResult)
							downloadSettingData = setDataFlag(downloadSettingData, "YES")
						Else
							downloadSettingData = setDataFlag(downloadSettingData, "NO")
						End If
						SaveCacheDataToDatabase(sCacheKey, downloadSettingData)
					End If
					SetCache(sCacheKey, downloadSettingData)
				End If
			End SyncLock
		Catch ex As Exception
			_log.Error("Can't get " & propName, ex)
		End Try
		Return oResult

	End Function


    Public Shared Function GetXABusinessAccountTypes(ByVal poConfig As CWebsiteConfig) As XmlDocument
        'step 1 - get all application questions
        Dim sUrl = poConfig.BaseSubmitLoanUrl & "/SettingsTransfer/SettingsExport.aspx"
        Dim sXML As String = String.Format("<SETTINGS_EXPORT_REQUEST module_name=""{0}"" request_id=""{1}"" entity_id=""{2}"" version=""1.0""/>", "XABusinessAccountTypes", System.Guid.NewGuid(), Common.SafeGUID(poConfig.LenderId))
        Dim getResponse As Func(Of String) = Function() PostSettingExportRequest(sXML, sUrl)
        'Response
        '<SETTINGS_EXPORT_RESULT request_id="b4bb5bb3-0165-4d78-8997-a05c361b89e6" entity_id="3eaf8f2f-5e5e-419d-8770-54a59738d48e" module_name="XABusinessAccountTypes">
        ' <Data>
        ' <XA_BUSINESS_ACCOUNT_TYPES version = "1.0" org_name="LPQ University" lender_name="LPQ University">
        '<XA_BUSINESS_ACCOUNT_TYPE>
        ' <BUSINESSTYPE type = "System.String" > CORP</BUSINESSTYPE>
        '	<ACCOUNTCODE type = "System.String" > corp</ACCOUNTCODE>
        '	<ISACTIVE type = "System.String" > Y</ISACTIVE>
        '	<ENABLEONCONSUMER type = "System.String" > N</ENABLEONCONSUMER>
        '	<AUTOCREATEAPPLICANTS type = "System.String" > N</AUTOCREATEAPPLICANTS>
        '	<SHOWDOINGBUSINESSAS type = "System.String" > N</SHOWDOINGBUSINESSAS>
        '	<XA_BUSINESS_ACCOUNT_ROLE>
        ' <ROLETYPE type = "System.String" > CORPORATE_REPRESENTATIVE</ROLETYPE>
        '		<INSTANCEMIN type = "System.Byte" > 1</INSTANCEMIN>
        '		<INSTANCEMAX type = "System.Byte" > 1</INSTANCEMAX>
        Dim getData As Func(Of CDownloadSettingData, XmlDocument) = Function(x) x._XABusinessAccountTypes
        Dim setData As Func(Of CDownloadSettingData, XmlDocument, CDownloadSettingData) = Function(x, data)
                                                                                              x._XABusinessAccountTypes = data
                                                                                              Return x
                                                                                          End Function
        Dim getDataFlag As Func(Of CDownloadSettingData, String) = Function(x) x._HasXABusinessAccountTypes
        Dim setDataFlag As Func(Of CDownloadSettingData, String, CDownloadSettingData) = Function(x, data)
                                                                                             x._HasXABusinessAccountTypes = data
                                                                                             Return x
                                                                                         End Function
        Return DownloadAccountTypes(poConfig, "XABusinessAccountTypes", getResponse, getData, setData, getDataFlag, setDataFlag)



    End Function

    Public Shared Function GetXASpecialAccountTypes(ByVal poConfig As CWebsiteConfig) As XmlDocument
        'step 1 - get all application questions 
        Dim sUrl = poConfig.BaseSubmitLoanUrl & "/SettingsTransfer/SettingsExport.aspx"
        Dim sXML As String = String.Format("<SETTINGS_EXPORT_REQUEST module_name=""{0}"" request_id=""{1}"" entity_id=""{2}"" version=""1.0""/>", "SpecialAccountTypes", System.Guid.NewGuid(), Common.SafeGUID(poConfig.LenderId))
        Dim getResponse As Func(Of String) = Function() PostSettingExportRequest(sXML, sUrl)
        'Response
        '<SETTINGS_EXPORT_RESULT request_id = "060de5de-5ab3-4b6b-8c53-f2eb70431b55" entity_id="3eaf8f2f-5e5e-419d-8770-54a59738d48e" module_name="SpecialAccountTypes">
        '<DATA>
        '	<SPECIAL_ACCOUNT_TYPES version = "1.0" org_name="LPQ University" lender_name="LPQ University">
        '		<SPECIAL_ACCOUNT_TYPE>
        '			<ACCOUNTNAME type = "System.String" > Association Club</ACCOUNTNAME>
        '			<ACCOUNTCODE type = "System.String" > ASSOCIATION_CLUB</ACCOUNTCODE>
        '			<ISACTIVE type = "System.String" > N</ISACTIVE>
        '			<SHOWSPECIALINFO type = "System.String" > Y</SHOWSPECIALINFO>
        '			<AUTOCREATEAPPLICANTS type = "System.String" > N</AUTOCREATEAPPLICANTS>
        '			<HIDEBENEFICIARYPAGE type = "System.String" > N</HIDEBENEFICIARYPAGE>
        '			<ENABLEONCONSUMER type = "System.String" > N</ENABLEONCONSUMER>
        '		</SPECIAL_ACCOUNT_TYPE>
        '		<SPECIAL_ACCOUNT_TYPE>
        '			<ACCOUNTNAME type = "System.String" > Business</ACCOUNTNAME>
        '			<ACCOUNTCODE type = "System.String" > BUS</ACCOUNTCODE>
        '			<ISACTIVE type = "System.String" > N</ISACTIVE>
        '			<SHOWSPECIALINFO type = "System.String" > Y</SHOWSPECIALINFO>
        '			<AUTOCREATEAPPLICANTS type = "System.String" > N</AUTOCREATEAPPLICANTS>
        '			<HIDEBENEFICIARYPAGE type = "System.String" > Y</HIDEBENEFICIARYPAGE>
        '			<ENABLEONCONSUMER type = "System.String" > N</ENABLEONCONSUMER>
        '			<SPECIAL_ACCOUNT_ROLE>
        '				<ROLETYPE type = "System.String" > OWNER</ROLETYPE>
        '				<INSTANCEMIN type = "System.Int32" > 1</INSTANCEMIN>
        '				<INSTANCEMAX type = "System.Int32" > 10</INSTANCEMAX>
        Dim getData As Func(Of CDownloadSettingData, XmlDocument) = Function(x) x._XASpecialAccountTypes
        Dim setData As Func(Of CDownloadSettingData, XmlDocument, CDownloadSettingData) = Function(x, data)
                                                                                              x._XASpecialAccountTypes = data
                                                                                              Return x
                                                                                          End Function
        Dim getDataFlag As Func(Of CDownloadSettingData, String) = Function(x) x._HasXASpecialAccountTypes
        Dim setDataFlag As Func(Of CDownloadSettingData, String, CDownloadSettingData) = Function(x, data)
                                                                                             x._HasXASpecialAccountTypes = data
                                                                                             Return x
                                                                                         End Function
        Return DownloadAccountTypes(poConfig, "XASpecialAccountTypes", getResponse, getData, setData, getDataFlag, setDataFlag)
    End Function


    Private Shared Function PostSettingExportRequest(ByVal psXml As String, ByVal psURL As String) As String
		_log.InfoFormat("Prepare data to POST {0}", CSecureStringFormatter.MaskSensitiveXMLData(psXml))	'use MaskSensitiveData for incomplete xml doc
		'_log.info("Preparing to POST data: " + Left(poDoc.OuterXml, 4000))

		Dim oRequest As HttpWebRequest = CType(WebRequest.Create(psURL), HttpWebRequest)
		Dim oBytes As Byte() = System.Text.Encoding.UTF8.GetBytes(psXml)
		oRequest.Method = "POST"
		oRequest.ContentLength = oBytes.Length
		oRequest.ContentType = "text/xml; encoding='utf-8'"

		Dim Cert As New X509Certificates.X509Certificate2()
		Try
			'Cert.Import()

			If ConfigurationManager.AppSettings("CertPath") IsNot Nothing AndAlso ConfigurationManager.AppSettings("CertPath") <> "" Then
				_CertPath = ConfigurationManager.AppSettings("CertPath")
			End If

			If ConfigurationManager.AppSettings("CertPassword") IsNot Nothing AndAlso ConfigurationManager.AppSettings("CertPassword") <> "" Then
				_CertPW = ConfigurationManager.AppSettings("CertPassword")
			End If

			'AP apptest can post to either test or prod LPQ so AP needs to use the correct certificate based on LPQ domain
			'swith to live certifate if the domain is not a test env
			'https://apply-sta.golden1.com G1 stage env
			If Not (psURL.Contains("beta") Or psURL.Contains("demo") Or psURL.Contains("jplqa") Or psURL.Contains("apply-sta")) Then
				If ConfigurationManager.AppSettings("CertPath_live") IsNot Nothing AndAlso ConfigurationManager.AppSettings("CertPath_live") <> "" Then
					_CertPath = ConfigurationManager.AppSettings("CertPath_live")
				End If

				If ConfigurationManager.AppSettings("CertPassword_live") IsNot Nothing AndAlso ConfigurationManager.AppSettings("CertPassword_live") <> "" Then
					_CertPW = ConfigurationManager.AppSettings("CertPassword_live")
				End If
			End If

			Cert.Import(_CertPath, _CertPW, X509Certificates.X509KeyStorageFlags.UserKeySet)
		Catch ex As Exception
			_log.Error(String.Format("ERROR. Unable to load certificate {0}: {1}", _CertPath, ex.Message), ex)
		End Try

		Dim signatureService As New LPQMobileLib.CXmlDigitalSignatureService()
		signatureService.AttachSignature(oRequest, psXml, Cert)

		Dim oRequestStream As Stream = oRequest.GetRequestStream()
		oRequestStream.Write(oBytes, 0, oBytes.Length)
		oRequestStream.Close()
		oRequestStream.Dispose()
		Dim streamBuilder As New StringBuilder()
		Dim responseXML As String = ""
		Try
			_log.Info("Begin request to: " & psURL)
			'_log.info("Start the request to " + psURL)

			Dim oResponse As HttpWebResponse = CType(oRequest.GetResponse(), HttpWebResponse)
			''status = oResponse.StatusCode = HttpStatusCode.OK
			' _Log.Info("Request successfully!")
			'  _log.info("Request was successful")
			Dim receiveStream As Stream = oResponse.GetResponseStream()
			Dim encode As Encoding = System.Text.Encoding.GetEncoding("utf-8")
			Dim readStream As New StreamReader(receiveStream, encode)
			Dim read(256) As [Char]
			' Reads 256 characters at a time.    
			Dim count As Integer = readStream.Read(read, 0, 256)
			While count > 0
				Dim str As New [String](read, 0, count)
				streamBuilder.Append(str)
				count = readStream.Read(read, 0, 256)
			End While
			' Releases the resources of the Stream.
			readStream.Close()
			' Releases the resources of the response.

			oResponse.Close()

			responseXML = streamBuilder.ToString()

			' _Log.Info("Response status Code: " & oResponse.StatusCode.ToString())
			'_log.info("Response status Code: " + oResponse.StatusCode.ToString())

			_log.Info("Response string: " & responseXML)
			'_log.info("Response string: " + responseXML)

		Catch ex As Exception
			_log.Error("Unable to get response from: " & psURL, ex)
			'_log.Error("Unable to get response from: " + psURL, ex)
		End Try
		Return responseXML
	End Function


	Public Shared Function DownloadQuestions(oConfig As CWebsiteConfig) As List(Of CCustomQuestionXA)
		Dim questionList As List(Of CCustomQuestionXA)
		Dim sCacheKey As String = "CURRENT_XA_QUESTIONS_" & oConfig.CacheIDSuffix
		questionList = CType(HttpContext.Current.Cache.Get(sCacheKey), List(Of CCustomQuestionXA))
		If questionList Is Nothing Then
			Try
				SyncLock GetLenderLockCollection(oConfig).XAQuestionsLock
					' Ensure that the data was not loaded by a concurrent thread 
					' while waiting for lock.
					questionList = CType(HttpContext.Current.Cache.Get(sCacheKey), List(Of CCustomQuestionXA))
					If questionList Is Nothing Then
						questionList = New List(Of CCustomQuestionXA)
						'TODO: check and get cached data from DB
						Dim cacheDataFromDb = ReadCacheDataFromDatabase(Of List(Of CCustomQuestionXA))(sCacheKey)
						If cacheDataFromDb IsNot Nothing Then
							questionList = cacheDataFromDb
						Else
							' TODO: move these codes to another place
							Dim req As WebRequest = WebRequest.Create(oConfig.BaseSubmitLoanUrl & "/ssfcu/CustomQuestionRetrieval.aspx")
							req.Method = "POST"
							req.ContentType = "text/xml"

							Using requestStream = req.GetRequestStream()
								Dim writer As New StreamWriter(requestStream)
								writer.Write(String.Format("<?xml version='1.0' encoding='UTF-8'?><INPUT><LOGIN api_user_id='{0}' api_password='{1}'/><REQUEST lender_id='{2}' app_type='XA'/></INPUT>" _
								 , oConfig.APIUser, oConfig.APIPassword, oConfig.LenderId))
								writer.Close()

								Using res As WebResponse = req.GetResponse()
									Using stream As Stream = res.GetResponseStream()
										Dim reader As New StreamReader(stream)
										Dim sXml As String = reader.ReadToEnd()
										_log.Debug("XA: Response from server (questions retrieval): " + sXml)
										reader.Close()

										Dim responseXML As XmlDocument = New XmlDocument()
										responseXML.LoadXml(sXml)

										' parse product question list
										For Each oItem As XmlElement In responseXML.SelectNodes("//RESPONSE/CUSTOM_QUESTIONS/CUSTOM_QUESTION")
											Dim oQuestion As New CCustomQuestionXA(oItem)
											''filtering custom questions in CCustomQuestionXA class
											''If Convert.ToBoolean(oItem.GetAttribute("is_xa_entity_personal") = "false") Then	'exclude when personal is not checked
											''	Continue For
											''End If
											If oQuestion.IsActive AndAlso oQuestion.IsAvailableToConsumer AndAlso (oQuestion.CustomQuestionOptions.Count > 0 OrElse oQuestion.AnswerType.Equals("TEXTBOX")) Then
												questionList.Add(oQuestion)
											End If
										Next

										questionList = questionList.ToList()
									End Using

									res.Close()
								End Using
								requestStream.Close()
							End Using
							SaveCacheDataToDatabase(sCacheKey, questionList)
						End If
						'If DownloadApplicantQuestions.Count > 0 Then ' count less than 0 mean there is probably error in download so don't wnat to store
						'DownloadApplicantQuestions.Count= 0 happen alot, do this to avoid  lot of web request
						SetCache(sCacheKey, questionList)
						'End If
					End If
				End SyncLock
			Catch ex As Exception
				_log.Error("Can't get " & sCacheKey, ex)
			End Try
		End If
		Return questionList
	End Function

	Public Shared Function DownloadFomQuestion(oConfig As CWebsiteConfig) As String
		Dim fomData As String
		Dim sCacheKey As String = "CURRENT_FOM_QUESTIONS_" & oConfig.CacheIDSuffix
		fomData = CType(HttpContext.Current.Cache.Get(sCacheKey), String)
		If String.IsNullOrEmpty(fomData) Then
			Try
				SyncLock GetLenderLockCollection(oConfig).FOMQuestionsLock
					' Ensure that the data was not loaded by a concurrent thread 
					' while waiting for lock.
					fomData = CType(HttpContext.Current.Cache.Get(sCacheKey), String)
					If String.IsNullOrEmpty(fomData) Then
						'TODO: check and get cached data from DB
						Dim cacheDataFromDb = ReadCacheDataFromDatabase(Of String)(sCacheKey)
						If cacheDataFromDb IsNot Nothing Then
							fomData = cacheDataFromDb
						Else
							Dim req As WebRequest = WebRequest.Create(oConfig.BaseSubmitLoanUrl & "/ssfcu/FieldListRetrieval.aspx")
							req.Method = "POST"
							req.ContentType = "text/xml"

							Using requestStream = req.GetRequestStream()
								Dim writer As New StreamWriter(requestStream)
								writer.Write(String.Format("<?xml version='1.0' encoding='UTF-8'?><INPUT><LOGIN api_user_id='{0}' api_password='{1}'/><REQUEST lender_id='{2}' app_type='XA'/></INPUT>" _
								 , oConfig.APIUser, oConfig.APIPassword, oConfig.LenderId))
								writer.Close()
								Using res As WebResponse = req.GetResponse()
									Using stream As Stream = res.GetResponseStream()
										Dim reader As New StreamReader(stream)
										fomData = reader.ReadToEnd()
										_log.Debug("XA: Response from server (FOM question, ID card type ... retrieval): " + fomData)
										reader.Close()
									End Using
									res.Close()
								End Using
								requestStream.Close()
							End Using
							SaveCacheDataToDatabase(sCacheKey, fomData)
						End If
						If Not String.IsNullOrEmpty(fomData) Then
							Dim responseXml As XmlDocument = New XmlDocument()
							responseXml.LoadXml(fomData)
							If responseXml.SelectNodes("//fom/starting_questions").Count > 0 Then ' count less than 0 mean there is probably error in download so don't wnat to store in session
								SetCache(sCacheKey, fomData)
							End If
						End If
					End If
				End SyncLock
			Catch ex As Exception
				_log.Error("Can't get " & sCacheKey)
			End Try
		End If
		Return fomData
	End Function


	Public Shared Function DownloadLenderServiceConfigInfo(oConfig As CWebsiteConfig) As CLenderServiceConfigInfo
		Dim download As CLenderServiceConfigInfo
		Dim sCacheKey As String = "CURRENT_LENDER_SERVICE_CONFIG_INFO_" & oConfig.CacheIDSuffix
		download = CType(HttpContext.Current.Cache.Get(sCacheKey), CLenderServiceConfigInfo)
		If download Is Nothing Then
			Dim sUrl As String = oConfig.BaseSubmitLoanUrl & "/ssfcu/SystemInformationRetrieval.aspx"
			If oConfig.SubmitLoanUrl_TPV <> "" AndAlso oConfig.SubmitLoanUrl_TPV = oConfig.SubmitLoanUrl Then 'if only interface to ACTIon then use ACTion
				sUrl = String.Format(oConfig.BaseSubmitLoanUrl_TPV & "/ssfcu/SystemInformationRetrieval.aspx?OrgCode2={0}&LenderCode2={1}", oConfig.OrgCode2, oConfig.LenderCode2)
			End If
			Try
				SyncLock GetLenderLockCollection(oConfig).LenderServiceConfigInfoLock
					' Ensure that the data was not loaded by a concurrent thread 
					' while waiting for lock.
					download = CType(HttpContext.Current.Cache.Get(sCacheKey), CLenderServiceConfigInfo)
					If download Is Nothing Then

						'TODO: check and get cached data from DB
						Dim cacheDataFromDb = ReadCacheDataFromDatabase(Of CLenderServiceConfigInfo)(sCacheKey)
						If cacheDataFromDb IsNot Nothing Then
							download = cacheDataFromDb
						Else

							Dim req As WebRequest = WebRequest.Create(sUrl)
							req.Method = "POST"
							req.ContentType = "text/xml"

							Using requestStream = req.GetRequestStream()
								Dim writer As New StreamWriter(requestStream)
								Dim xmlRequest As String = String.Format("<?xml version='1.0' encoding='UTF-8'?><INPUT><LOGIN api_user_id='{0}' api_password='{1}'/><REQUEST organization_id='{2}'/></INPUT>", oConfig.APIUser, oConfig.APIPassword, oConfig.OrganizationId)
								_log.Debug(String.Format("Request to {0} : {1}", sUrl, CSecureStringFormatter.MaskSensitiveXMLData(xmlRequest)))
								writer.Write(xmlRequest)
								writer.Close()

								Using res As WebResponse = req.GetResponse()
									Using stream As Stream = res.GetResponseStream()
										Dim reader As New StreamReader(stream)
										Dim sXml As String = reader.ReadToEnd()

										_log.Debug(String.Format("Response(system service retrieval) from {0} : {1}", sUrl, sXml))
										reader.Close()

										Dim responseXML As XmlDocument = New XmlDocument()
										responseXML.LoadXml(sXml)

										' parse product question list
										Dim xpath As String = String.Format("//ORGANIZATION[@organization_id='{0}']/LENDERS/LENDER[@lender_id='{1}']", oConfig.OrganizationId, oConfig.LenderId)
										Dim IsNotLPQ As Boolean = oConfig.SubmitLoanUrl = oConfig.SubmitLoanUrl_TPV
										If IsNotLPQ Then
											xpath = String.Format("//ORGANIZATION/LENDERS/LENDER")
										End If

										Dim lenderNode As XmlElement = CType(responseXML.SelectSingleNode(xpath), XmlElement)
										If lenderNode IsNot Nothing Then
											download = New CLenderServiceConfigInfo(oConfig, lenderNode)
										Else
											download = Nothing
										End If
									End Using

									res.Close()
								End Using
								requestStream.Close()
							End Using
							If download IsNot Nothing Then	' nothing mean there is probably error in download so don't wnat to store
								SaveCacheDataToDatabase(sCacheKey, download)
							End If
						End If
						If download IsNot Nothing Then	' nothing mean there is probably error in download so don't wnat to store
							SetCache(sCacheKey, download)
						Else
							_log.Warn(String.Format("can't download LenderServiceConfigInfo from {0}", sUrl))
							download = New CLenderServiceConfigInfo
						End If
					End If
				End SyncLock
			Catch ex As Exception
				_log.Error(String.Format("Can't get " & sCacheKey & " from {0}.", sUrl), ex)
			End Try
		End If
		Return download

	End Function


	Public Shared Function GetDownloadedProducts(oConfig As CWebsiteConfig) As List(Of CProduct)
		Dim sCacheKey As String = "CURRENT_LIST_PRODUCT_" & oConfig.CacheIDSuffix
		Dim products As List(Of CProduct) = CType(HttpContext.Current.Cache.Get(sCacheKey), List(Of CProduct))
		If products Is Nothing Then
			Try
				SyncLock GetLenderLockCollection(oConfig).ListProductLock
					' Ensure that the data was not loaded by a concurrent thread 
					' while waiting for lock.
					products = CType(HttpContext.Current.Cache.Get(sCacheKey), List(Of CProduct))
					If products Is Nothing Then
						'TODO: check and get cached data from DB
						Dim cacheDataFromDb = ReadCacheDataFromDatabase(Of List(Of CProduct))(sCacheKey)
						If cacheDataFromDb IsNot Nothing Then
							products = cacheDataFromDb
						Else
							products = DownloadAllProducts(oConfig)
							SaveCacheDataToDatabase(sCacheKey, products)
						End If
						If products.Count > 0 Then ' count less than 0 mean there is probably error in download so don't want to store
							SetCache(sCacheKey, products)
						End If
					End If
				End SyncLock
			Catch ex As Exception
				_log.Error("Can't get " & sCacheKey)
			End Try
		End If

		'Merge some attribute from new API that are not available in legacy service to Products
		Dim oProducts_NewAPI = CProduct.getDownloadedProducts_NewAPI(oConfig)
		If oProducts_NewAPI.Count > 0 Then
			For Each oProduct In products
				Dim selectedProd_NewApi As CProduct = oProducts_NewAPI.FirstOrDefault(Function(x) oProduct.ProductCode = x.ProductCode)
				If selectedProd_NewApi IsNot Nothing AndAlso selectedProd_NewApi.ProductCode <> "" Then
					oProduct.cannotBeFunded = selectedProd_NewApi.cannotBeFunded
					oProduct.pdfAssociations = selectedProd_NewApi.pdfAssociations
					oProduct.specialAccountTypes = selectedProd_NewApi.specialAccountTypes
					oProduct.InterestRateType = selectedProd_NewApi.InterestRateType
					oProduct.IsAutoCalculateTier = selectedProd_NewApi.IsAutoCalculateTier
					oProduct.IsApyAutoCalculated = selectedProd_NewApi.IsApyAutoCalculated
					oProduct.TermType = selectedProd_NewApi.TermType
					If oProduct.AvailableRates IsNot Nothing AndAlso oProduct.AvailableRates.Count > 0 AndAlso selectedProd_NewApi.AvailableRates IsNot Nothing AndAlso selectedProd_NewApi.AvailableRates.Count > 0 Then
						For Each newRate As CProductRate In selectedProd_NewApi.AvailableRates
							Dim rate = oProduct.AvailableRates.FirstOrDefault(Function(x) newRate.RateCode = x.RateCode)
							If rate IsNot Nothing Then
								rate.LocationPool = newRate.LocationPool
								rate.ZipCodeFilterType = newRate.ZipCodeFilterType
								rate.MinTerm = newRate.MinTerm
								rate.MaxTerm = newRate.MaxTerm
							End If
						Next
					End If
				End If
			Next
		End If
		Return products
	End Function

	Private Shared Function DownloadAllProducts(oConfig As CWebsiteConfig) As List(Of CProduct)
		Dim products As New List(Of CProduct)
		Dim req As WebRequest = WebRequest.Create(oConfig.BaseSubmitLoanUrl & "/ssfcu/ProductRetrieval.aspx")
		req.Method = "POST"
		req.ContentType = "text/xml"
		Using requestStream = req.GetRequestStream()
			Dim writer As New StreamWriter(requestStream)
			writer.Write(String.Format("<?xml version='1.0' encoding='UTF-8'?><INPUT><LOGIN api_user_id='{0}' api_password='{1}'/><REQUEST lender_id='{2}'/></INPUT>", oConfig.APIUser, oConfig.APIPassword, oConfig.LenderId))
			writer.Close()

			Using res As WebResponse = req.GetResponse()
				Using stream As Stream = res.GetResponseStream()
					Dim reader As New StreamReader(stream)
					Dim sXml As String = reader.ReadToEnd()

					_log.Info("Product Response: " + sXml)
					reader.Close()

					Dim responseXML As XmlDocument = New XmlDocument()
					responseXML.LoadXml(sXml)

					' parse product question list
					Dim oTempQuestionList As List(Of CProductCustomQuestion) = (From oItem As XmlElement In responseXML.SelectNodes("//RESPONSE/QUESTIONS/QUESTION") Select New CProductCustomQuestion(oItem)).ToList()
					oTempQuestionList = oTempQuestionList.OrderBy(Function(o) o.Position).ToList()

					' parse product basic info -- product_code
					For Each oProductNode As XmlElement In responseXML.SelectNodes("//PRODUCTS/PRODUCT")
						Dim newProduct As New CProduct
						newProduct.IsActive = Convert.ToBoolean(oProductNode.GetAttribute("is_active"))
						If Not newProduct.IsActive Then
							Continue For
						End If
						If Convert.ToBoolean(oProductNode.GetAttribute("site_entity") <> "ALL") Then 'available to consumer
							Continue For
						End If
						newProduct.EntityType = oProductNode.GetAttribute("entity_type")
						newProduct.Availability = oProductNode.GetAttribute("availability")
						newProduct.ProductCode = oProductNode.GetAttribute("product_code")
						newProduct.AccountType = oProductNode.GetAttribute("account_type")
						newProduct.AccountName = oProductNode.GetAttribute("name")
						newProduct.Rate = oProductNode.GetAttribute("rate")
						newProduct.Term = oProductNode.GetAttribute("term")
						newProduct.ProductDescription = oProductNode.GetAttribute("product_description") '' ***product description
						If newProduct.Term = "" Or newProduct.Term = "-999999" Then
							newProduct.Term = oProductNode.GetAttribute("min_term")
						End If
						newProduct.strPreSelectedValue = oProductNode.GetAttribute("pre_selection")
						newProduct.IsRequired = Convert.ToBoolean(String.Equals(oProductNode.GetAttribute("pre_selection"), "R"))
						newProduct.IsPreSelection = Convert.ToBoolean(String.Equals(oProductNode.GetAttribute("pre_selection"), "P"))
						newProduct.AutoPullIDAuthenticationConsumerPrimary = Convert.ToBoolean(oProductNode.GetAttribute("auto_pull_id_authentication_consumer_primary"))
						newProduct.AutoPullDebitConsumerPrimary = Convert.ToBoolean(oProductNode.GetAttribute("auto_pull_debit_consumer_primary"))
						newProduct.AutoPullCreditsConsumerPrimary = Convert.ToBoolean(oProductNode.GetAttribute("auto_pull_credit_consumer_primary"))
						newProduct.AutoPullIDAuthenticationConsumerSecondary = Convert.ToBoolean(oProductNode.GetAttribute("auto_pull_id_authentication_consumer_secondary"))
						newProduct.AutoPullDebitConsumerSecondary = Convert.ToBoolean(oProductNode.GetAttribute("auto_pull_debit_consumer_secondary"))
						newProduct.AutoPullCreditsConsumerSecondary = Convert.ToBoolean(oProductNode.GetAttribute("auto_pull_credit_consumer_secondary"))
						newProduct.ZipCodeFilterType = oProductNode.GetAttribute("zip_code_filter_type")
						newProduct.ZipCodePoolId = oProductNode.GetAttribute("zip_code_pool_id")
						If oProductNode.GetAttribute("display_position") = "" Then
							' Generate random value between 1 and 30. 
							Dim value As Integer = New CSecureRandom().GetValue(1, 30)
							newProduct.Position = value
						Else
							newProduct.Position = Convert.ToInt32(oProductNode.GetAttribute("display_position"))
						End If

						newProduct.MinimumDeposit = Convert.ToDecimal(oProductNode.GetAttribute("min_deposit"))
						newProduct.MaxDeposit = Convert.ToDecimal(oProductNode.GetAttribute("max_deposit"))
						newProduct.Apy = oProductNode.GetAttribute("apy")
						Dim isAutoCalculateTier As Boolean = Convert.ToBoolean(oProductNode.GetAttribute("is_auto_calculate_tier"))
						''Extract the max APY and min Deposit from TIERS
						Dim tierNodes = oProductNode.SelectNodes("TIERS/TIER")
						'' pre_selection has 3 values, for my best guess: P (pre selected) R (required) O (who knows)
						If tierNodes IsNot Nothing AndAlso tierNodes.Count > 0 Then
							Dim maxApy As Decimal = 0D
							Dim minDeposit As Decimal = 1000000D
							Dim maxDeposit As Decimal = 0D
							For Each tn As XmlElement In tierNodes
								Dim rateCode = tn.GetAttribute("rate_code")
								Dim currMaxDeposit As Decimal = 0D
								If newProduct.AvailableRates.Any(Function(r) r.RateCode = rateCode) Then Continue For
								Dim apy As Decimal = 0D
								Decimal.TryParse(tn.GetAttribute("apy"), apy)
								If apy > maxApy Then
									maxApy = apy
								End If
								Dim deposit As Decimal = 0D
								Decimal.TryParse(tn.GetAttribute("min_deposit"), deposit)
								Decimal.TryParse(tn.GetAttribute("max_deposit"), currMaxDeposit)
								Dim minTerm As Integer = 0
								Integer.TryParse(tn.GetAttribute("min_term"), minTerm)
								If isAutoCalculateTier Then	''use min deposit and min term from auto tier rate 
									If newProduct.MinimumDeposit >= 0 Then
										deposit = newProduct.MinimumDeposit
									End If
									If newProduct.MaxDeposit >= 0 Then
										currMaxDeposit = newProduct.MaxDeposit
									End If
									If Not String.IsNullOrEmpty(newProduct.Term) AndAlso Convert.ToInt32(newProduct.Term) >= 0 Then
										minTerm = Convert.ToInt32(newProduct.Term)
									End If
								End If

								If currMaxDeposit >= 0 And currMaxDeposit > maxDeposit Then
									maxDeposit = currMaxDeposit
								End If
								'fix null max deposit amount in tier rate
								If currMaxDeposit < 0 Then
									currMaxDeposit = 0D
								End If

								If deposit >= 0 AndAlso deposit < minDeposit Then
									minDeposit = deposit
								End If

								'fix null min deposit amount in tier rate
								If deposit < 0 Then
									deposit = 0D
								End If

								Dim sRate As String = tn.GetAttribute("rate")
								Dim rate As Decimal = 0D
								Decimal.TryParse(sRate, rate)
								Dim balance As Decimal = 0D
								Decimal.TryParse(tn.GetAttribute("min_balance"), balance)
								newProduct.AvailableRates.Add(New CProductRate With {.MaxDeposit = currMaxDeposit, .Apy = apy, .MinBalance = balance, .MinDeposit = deposit, .RateString = sRate, .RateDecimal = rate, .RateCode = rateCode, .Term = minTerm})
							Next
							' AP-3280 - Order tiered rates by Rate, MinBalance, MinDeposit, Term
							newProduct.AvailableRates = newProduct.AvailableRates.OrderBy(Function(o) o.RateDecimal).ThenBy(Function(o) o.MinBalance).ThenBy(Function(o) o.MinDeposit).ThenBy(Function(o) o.Term).ToList()
							newProduct.Apy = maxApy.ToString()
							newProduct.MinimumDeposit = If(minDeposit < 1000000D And minDeposit > 0D, minDeposit, 0)
							newProduct.MaxDeposit = maxDeposit
						End If

						'' Each Product can contain Services, let's find out
						Dim serviceNodes = oProductNode.SelectNodes("SERVICES/SERVICE")
						'' pre_selection has 3 values, for my best guess: P (pre selected) R (required) O (who knows)
						If serviceNodes IsNot Nothing AndAlso serviceNodes.Count > 0 Then
							For Each sn As XmlElement In serviceNodes
								Dim isActive = sn.GetAttribute("is_active") = "true"
								If isActive Then
									Dim ps As New CProductService() With
									{
										.ServiceType = sn.GetAttribute("service_type"),
										.ServiceCode = sn.GetAttribute("service_code"),
										.Description = sn.GetAttribute("description"),
										.DescriptionURL = Common.SafeString(sn.GetAttribute("link_description")),
										.IsActive = True,
										.IsPreSelection = sn.GetAttribute("pre_selection") = "P",
										.IsRequired = sn.GetAttribute("pre_selection") = "R"
									}
									''make sure sevice code is not empty
									If Not String.IsNullOrEmpty(ps.ServiceCode) Then
										newProduct.Services.Add(ps)
									End If
								End If
							Next
						End If

						Dim oTempProductQuestions As New List(Of CProductCustomQuestion)
						For Each oQuesitonNode As XmlElement In oProductNode.SelectNodes("QUESTIONS/QUESTION")
							Dim cq As CProductCustomQuestion = oTempQuestionList.FirstOrDefault(Function(o) String.Equals(o.XAProductQuestionID, oQuesitonNode.GetAttribute("xa_product_question_id")))
							If cq IsNot Nothing AndAlso cq.IsActive Then
								If cq.IsAvailableToConsumer = False Then Continue For 'only want consumer
								If cq.AnswerType = "HEADER" Then Continue For
								oTempProductQuestions.Add(cq)
							End If
						Next

						oTempProductQuestions = oTempProductQuestions.OrderBy(Function(o) o.Position).ToList
						For Each oItem As CProductCustomQuestion In oTempProductQuestions
							newProduct.CustomQuestions.Add(oItem)
						Next
						products.Add(newProduct)
					Next

				End Using

				res.Close()
			End Using
			requestStream.Close()
		End Using
		products = products.OrderBy(Function(o) o.Position).ToList()
		Return products
	End Function

	Public Shared Function CreditCardProducts(ByVal poConfig As CWebsiteConfig) As List(Of CCreditCardProduct)
		Dim oResult As New List(Of CCreditCardProduct)
		If poConfig.OrgCode = "" Or poConfig.LenderCode = "" Then
			_log.Error(poConfig.LenderId & " doesn't have org code or lender code")
			Return oResult
		End If

		Dim downloadSettingData As CDownloadSettingData
		Dim sCacheKey As String = "DOWNLOADED_SETTINS_" & poConfig.CacheIDSuffix
		downloadSettingData = CType(HttpContext.Current.Cache.Get(sCacheKey), CDownloadSettingData)

		Try
			If downloadSettingData IsNot Nothing AndAlso downloadSettingData._CreditCardProducts IsNot Nothing Then
				_log.Info("_CreditCardProduct Object " & sCacheKey & " was retrieved from cache. The cache " & "was populated at " & HttpContext.Current.Application(sCacheKey).ToString())
				oResult = downloadSettingData._CreditCardProducts
				Exit Try
			End If
			SyncLock GetLenderLockCollection(poConfig).DownloadSettingsLock
				' Ensure that the data was not loaded by a concurrent thread 
				' while waiting for lock.
				downloadSettingData = CType(HttpContext.Current.Cache.Get(sCacheKey), CDownloadSettingData)
				If downloadSettingData IsNot Nothing AndAlso downloadSettingData._CreditCardProducts IsNot Nothing Then
					_log.Info("_CreditCardProduct Object " & sCacheKey & " was retrieved from cache. The cache " & "was populated at " & HttpContext.Current.Application(sCacheKey).ToString())
					oResult = downloadSettingData._CreditCardProducts
				Else
					If downloadSettingData IsNot Nothing AndAlso downloadSettingData._HasCreditCardProducts = "NO" Then
						oResult = New List(Of CCreditCardProduct)
						Exit Try
					End If
					If downloadSettingData Is Nothing Then
						downloadSettingData = New CDownloadSettingData
					End If

					'TODO: check and get cached data from DB
					Dim downloadedSettingsFromDb = ReadCacheDataFromDatabase(Of CDownloadSettingData)(sCacheKey)
                    If downloadedSettingsFromDb IsNot Nothing AndAlso Not String.IsNullOrEmpty(downloadedSettingsFromDb._HasCreditCardProducts) Then
                        If downloadedSettingsFromDb._CreditCardProducts IsNot Nothing AndAlso downloadedSettingsFromDb._CreditCardProducts.Count > 0 Then
                            oResult = downloadedSettingsFromDb._CreditCardProducts
                            downloadSettingData._CreditCardProducts = oResult
                            downloadSettingData._HasCreditCardProducts = "YES"
                        Else
                            oResult = New List(Of CCreditCardProduct)
                            downloadSettingData._HasCreditCardProducts = "NO"
                        End If
                    Else
                        Dim sUrl = poConfig.BaseSubmitLoanUrl & "/resources.ashx/orgs/" & poConfig.OrgCode & "/fis/" & poConfig.LenderCode & "/settings/cc/products"
                        Dim sReponse = Common.SubmitRestApiGet(poConfig, sUrl)
						'Get detail for products
						Dim xmlDoc As New XmlDocument
						xmlDoc.LoadXml(sReponse)
						Dim productNodes = xmlDoc.GetElementsByTagName("PRODUCT")
						If productNodes IsNot Nothing AndAlso productNodes.Count > 0 Then
							Dim oDeserializedOutput As CCreditCardProduct
							Dim serializer As New XmlSerializer(GetType(CCreditCardProduct))
							For Each product As XmlNode In productNodes
								Dim sCardName As String = product.SelectSingleNode("CARD_NAME").InnerText
								If sCardName = "" Then Continue For
								sReponse = Common.SubmitRestApiGet(poConfig, sUrl & "/" & sCardName)

								If String.IsNullOrEmpty(sReponse) Then	''this is need so this process can continue and cache other questions to cache.  Don't wnat to tram the traffic if this process is run many times
									_log.WarnFormat("Can't download _CreditCardProduct:{0}.  _CreditCardProduct name on backoffice may need to be fixed by removing any special characters", sCardName)
									Continue For
								End If

								Using reader As TextReader = New StringReader(sReponse)
									oDeserializedOutput = CType(serializer.Deserialize(reader), CCreditCardProduct)
								End Using
								oResult.Add(oDeserializedOutput)
							Next

							If oResult IsNot Nothing AndAlso oResult.Count > 0 Then	  ' count less than 0 mean there is probably error in download so don't wnat to store in session
								_log.Info("_CreditCardProduct Object " & sCacheKey & " was created. ")
								downloadSettingData._CreditCardProducts = oResult
								downloadSettingData._HasCreditCardProducts = "YES"
							Else
								oResult = New List(Of CCreditCardProduct)
								downloadSettingData._HasCreditCardProducts = "NO"
							End If
						Else
							oResult = New List(Of CCreditCardProduct)
							downloadSettingData._HasCreditCardProducts = "NO"
						End If
						SaveCacheDataToDatabase(sCacheKey, downloadSettingData)
					End If
					SetCache(sCacheKey, downloadSettingData)
				End If

			End SyncLock
		Catch ex As Exception
			_log.Error("Can't get _CreditCardProduct", ex)
		End Try
		Return oResult
	End Function
	Public Shared Function HomeEquityLoanProducts(ByVal poConfig As CWebsiteConfig) As List(Of CHomeEquityLoanProduct)
		Dim oResult As New List(Of CHomeEquityLoanProduct)
		If poConfig.OrgCode = "" Or poConfig.LenderCode = "" Then
			_log.Error(poConfig.LenderId & " doesn't have org code or lender code")
			Return oResult
		End If

		Dim downloadSettingData As CDownloadSettingData
		Dim sCacheKey As String = "DOWNLOADED_SETTINS_" & poConfig.CacheIDSuffix
		downloadSettingData = CType(HttpContext.Current.Cache.Get(sCacheKey), CDownloadSettingData)

		Try
			If downloadSettingData IsNot Nothing AndAlso downloadSettingData._HomeEquityLoanProducts IsNot Nothing Then
				_log.Info("_HomeEquityLoanProduct Object " & sCacheKey & " was retrieved from cache. The cache " & "was populated at " & HttpContext.Current.Application(sCacheKey).ToString())
				oResult = downloadSettingData._HomeEquityLoanProducts
				Exit Try
			End If
			SyncLock GetLenderLockCollection(poConfig).DownloadSettingsLock
				' Ensure that the data was not loaded by a concurrent thread 
				' while waiting for lock.
				downloadSettingData = CType(HttpContext.Current.Cache.Get(sCacheKey), CDownloadSettingData)
				If downloadSettingData IsNot Nothing AndAlso downloadSettingData._HomeEquityLoanProducts IsNot Nothing Then
					_log.Info("_HomeEquityLoanProduct Object " & sCacheKey & " was retrieved from cache. The cache " & "was populated at " & HttpContext.Current.Application(sCacheKey).ToString())
					oResult = downloadSettingData._HomeEquityLoanProducts
				Else
					If downloadSettingData IsNot Nothing AndAlso downloadSettingData._HasHomeEquityLoanProducts = "NO" Then
						oResult = New List(Of CHomeEquityLoanProduct)
						Exit Try
					End If
					If downloadSettingData Is Nothing Then
						downloadSettingData = New CDownloadSettingData
					End If

					'TODO: check and get cached data from DB
					Dim downloadedSettingsFromDb = ReadCacheDataFromDatabase(Of CDownloadSettingData)(sCacheKey)
                    If downloadedSettingsFromDb IsNot Nothing AndAlso Not String.IsNullOrEmpty(downloadedSettingsFromDb._HasHomeEquityLoanProducts) Then
                        If downloadedSettingsFromDb._HomeEquityLoanProducts IsNot Nothing AndAlso downloadedSettingsFromDb._HomeEquityLoanProducts.Count > 0 Then
                            oResult = downloadedSettingsFromDb._HomeEquityLoanProducts
                            downloadSettingData._HomeEquityLoanProducts = oResult
                            downloadSettingData._HasHomeEquityLoanProducts = "YES"
                        Else
                            oResult = New List(Of CHomeEquityLoanProduct)
                            downloadSettingData._HasHomeEquityLoanProducts = "NO"
                        End If
                    Else
                        Dim sUrl = poConfig.BaseSubmitLoanUrl & "/resources.ashx/orgs/" & poConfig.OrgCode & "/fis/" & poConfig.LenderCode & "/settings/he/products"
                        Dim sReponse = Common.SubmitRestApiGet(poConfig, sUrl)
						'Get detail for products
						Dim xmlDoc As New XmlDocument
						xmlDoc.LoadXml(sReponse)
						Dim productNodes = xmlDoc.GetElementsByTagName("PRODUCT")
						If productNodes IsNot Nothing AndAlso productNodes.Count > 0 Then
							Dim oDeserializedOutput As CHomeEquityLoanProduct
							Dim serializer As New XmlSerializer(GetType(CHomeEquityLoanProduct))
							For Each product As XmlNode In productNodes
								Dim sProgramName As String = product.SelectSingleNode("PROGRAM_NAME").InnerText
								If sProgramName = "" Then Continue For
								sReponse = Common.SubmitRestApiGet(poConfig, sUrl & "/" & sProgramName)

								If String.IsNullOrEmpty(sReponse) Then	''this is need so this process can continue and cache other questions to cache.  Don't wnat to tram the traffic if this process is run many times
									_log.WarnFormat("Can't download HE Products:{0}.  product name on backoffice may need to be fixed by removing any special characters", sProgramName)
									Continue For
								End If

								Using reader As TextReader = New StringReader(sReponse)
									oDeserializedOutput = CType(serializer.Deserialize(reader), CHomeEquityLoanProduct)
								End Using
								oResult.Add(oDeserializedOutput)
							Next

							If oResult IsNot Nothing AndAlso oResult.Count > 0 Then	  ' count less than 0 mean there is probably error in download so don't wnat to store in session
								_log.Info("_HomeEquityLoanProduct Object " & sCacheKey & " was created. ")
								downloadSettingData._HomeEquityLoanProducts = oResult
								downloadSettingData._HasHomeEquityLoanProducts = "YES"
							Else
								oResult = New List(Of CHomeEquityLoanProduct)
								downloadSettingData._HasHomeEquityLoanProducts = "NO"
							End If
						Else
							oResult = New List(Of CHomeEquityLoanProduct)
							downloadSettingData._HasHomeEquityLoanProducts = "NO"
						End If
						SaveCacheDataToDatabase(sCacheKey, downloadSettingData)
					End If
					SetCache(sCacheKey, downloadSettingData)
				End If

			End SyncLock
		Catch ex As Exception
			_log.Error("Can't get _HomeEquityLoanProduct", ex)
		End Try
		Return oResult
	End Function
	Public Shared Function PersonalLoanProducts(ByVal poConfig As CWebsiteConfig) As List(Of CPersonalLoanProduct)
		Dim oResult As New List(Of CPersonalLoanProduct)
		If poConfig.OrgCode = "" Or poConfig.LenderCode = "" Then
			_log.Error(poConfig.LenderId & " doesn't have org code or lender code")
			Return oResult
		End If

		Dim downloadSettingData As CDownloadSettingData
		Dim sCacheKey As String = "DOWNLOADED_SETTINS_" & poConfig.CacheIDSuffix
		downloadSettingData = CType(HttpContext.Current.Cache.Get(sCacheKey), CDownloadSettingData)

		Try
			If downloadSettingData IsNot Nothing AndAlso downloadSettingData._PersonalLoanProducts IsNot Nothing Then
				_log.Info("_PersonalLoanProduct Object " & sCacheKey & " was retrieved from cache. The cache " & "was populated at " & HttpContext.Current.Application(sCacheKey).ToString())
				oResult = downloadSettingData._PersonalLoanProducts
				Exit Try
			End If
			SyncLock GetLenderLockCollection(poConfig).DownloadSettingsLock
				' Ensure that the data was not loaded by a concurrent thread 
				' while waiting for lock.
				downloadSettingData = CType(HttpContext.Current.Cache.Get(sCacheKey), CDownloadSettingData)
				If downloadSettingData IsNot Nothing AndAlso downloadSettingData._PersonalLoanProducts IsNot Nothing Then
					_log.Info("_PersonalLoanProduct Object " & sCacheKey & " was retrieved from cache. The cache " & "was populated at " & HttpContext.Current.Application(sCacheKey).ToString())
					oResult = downloadSettingData._PersonalLoanProducts
				Else
					If downloadSettingData IsNot Nothing AndAlso downloadSettingData._HasPersonalLoanProducts = "NO" Then
						oResult = New List(Of CPersonalLoanProduct)
						Exit Try
					End If
					If downloadSettingData Is Nothing Then
						downloadSettingData = New CDownloadSettingData
					End If

					'TODO: check and get cached data from DB
					Dim downloadedSettingsFromDb = ReadCacheDataFromDatabase(Of CDownloadSettingData)(sCacheKey)
                    If downloadedSettingsFromDb IsNot Nothing AndAlso Not String.IsNullOrEmpty(downloadedSettingsFromDb._HasPersonalLoanProducts) Then
                        If downloadedSettingsFromDb._PersonalLoanProducts IsNot Nothing AndAlso downloadedSettingsFromDb._PersonalLoanProducts.Count > 0 Then
                            oResult = downloadedSettingsFromDb._PersonalLoanProducts
                            downloadSettingData._PersonalLoanProducts = oResult
                            downloadSettingData._HasPersonalLoanProducts = "YES"
                        Else
                            oResult = New List(Of CPersonalLoanProduct)
                            downloadSettingData._HasPersonalLoanProducts = "NO"
                        End If
                    Else
                        Dim sUrl = poConfig.BaseSubmitLoanUrl & "/resources.ashx/orgs/" & poConfig.OrgCode & "/fis/" & poConfig.LenderCode & "/settings/pl/products"
                        Dim sReponse = Common.SubmitRestApiGet(poConfig, sUrl)
						'Get detail for products
						Dim xmlDoc As New XmlDocument
						xmlDoc.LoadXml(sReponse)
						Dim productNodes = xmlDoc.GetElementsByTagName("PRODUCT")
						If productNodes IsNot Nothing AndAlso productNodes.Count > 0 Then
							Dim oDeserializedOutput As CPersonalLoanProduct
							Dim serializer As New XmlSerializer(GetType(CPersonalLoanProduct))
							For Each product As XmlNode In productNodes
								Dim sProductCode As String = product.SelectSingleNode("PRODUCT_CODE").InnerText
								If sProductCode = "" Then Continue For
								sReponse = Common.SubmitRestApiGet(poConfig, sUrl & "/" & sProductCode)

								If String.IsNullOrEmpty(sReponse) Then	''this is need so this process can continue and cache other questions to cache.  Don't wnat to tram the traffic if this process is run many times
									_log.WarnFormat("Can't download PL Products:{0}.  product name on backoffice may need to be fixed by removing any special characters", sProductCode)
									Continue For
								End If

								Using reader As TextReader = New StringReader(sReponse)
									oDeserializedOutput = CType(serializer.Deserialize(reader), CPersonalLoanProduct)
								End Using
								oResult.Add(oDeserializedOutput)
							Next

							If oResult IsNot Nothing AndAlso oResult.Count > 0 Then	  ' count less than 0 mean there is probably error in download so don't wnat to store in session
								_log.Info("_PersonalLoanProduct Object " & sCacheKey & " was created. ")
								downloadSettingData._PersonalLoanProducts = oResult
								downloadSettingData._HasPersonalLoanProducts = "YES"
							Else
								oResult = New List(Of CPersonalLoanProduct)
								downloadSettingData._HasPersonalLoanProducts = "NO"
							End If
						Else
							oResult = New List(Of CPersonalLoanProduct)
							downloadSettingData._HasPersonalLoanProducts = "NO"
						End If
						SaveCacheDataToDatabase(sCacheKey, downloadSettingData)
					End If
					SetCache(sCacheKey, downloadSettingData)
				End If

			End SyncLock
		Catch ex As Exception
			_log.Error("Can't get _PersonalLoanProduct", ex)
		End Try
		Return oResult
	End Function
	Public Shared Function VehicleLoanProducts(ByVal poConfig As CWebsiteConfig) As List(Of CVehicleLoanProduct)
		Dim oResult As New List(Of CVehicleLoanProduct)
		If poConfig.OrgCode = "" Or poConfig.LenderCode = "" Then
			_log.Error(poConfig.LenderId & " doesn't have org code or lender code")
			Return oResult
		End If

		Dim downloadSettingData As CDownloadSettingData
		Dim sCacheKey As String = "DOWNLOADED_SETTINS_" & poConfig.CacheIDSuffix
		downloadSettingData = CType(HttpContext.Current.Cache.Get(sCacheKey), CDownloadSettingData)

		Try
			If downloadSettingData IsNot Nothing AndAlso downloadSettingData._VehicleLoanProducts IsNot Nothing Then
				_log.Info("_CVehicleLoanProduct Object " & sCacheKey & " was retrieved from cache. The cache " & "was populated at " & HttpContext.Current.Application(sCacheKey).ToString())
				oResult = downloadSettingData._VehicleLoanProducts
				Exit Try
			End If
			SyncLock GetLenderLockCollection(poConfig).DownloadSettingsLock
				' Ensure that the data was not loaded by a concurrent thread 
				' while waiting for lock.
				downloadSettingData = CType(HttpContext.Current.Cache.Get(sCacheKey), CDownloadSettingData)
				If downloadSettingData IsNot Nothing AndAlso downloadSettingData._VehicleLoanProducts IsNot Nothing Then
					_log.Info("_CVehicleLoanProduct Object " & sCacheKey & " was retrieved from cache. The cache " & "was populated at " & HttpContext.Current.Application(sCacheKey).ToString())
					oResult = downloadSettingData._VehicleLoanProducts
				Else
					If downloadSettingData IsNot Nothing AndAlso downloadSettingData._HasVehicleLoanProducts = "NO" Then
						oResult = New List(Of CVehicleLoanProduct)
						Exit Try
					End If
					If downloadSettingData Is Nothing Then
						downloadSettingData = New CDownloadSettingData
					End If

					'TODO: check and get cached data from DB
					Dim downloadedSettingsFromDb = ReadCacheDataFromDatabase(Of CDownloadSettingData)(sCacheKey)
                    If downloadedSettingsFromDb IsNot Nothing AndAlso Not String.IsNullOrEmpty(downloadedSettingsFromDb._HasVehicleLoanProducts) Then
                        If downloadedSettingsFromDb._VehicleLoanProducts IsNot Nothing AndAlso downloadedSettingsFromDb._VehicleLoanProducts.Count > 0 Then
                            oResult = downloadedSettingsFromDb._VehicleLoanProducts
                            downloadSettingData._VehicleLoanProducts = oResult
                            downloadSettingData._HasVehicleLoanProducts = "YES"
                        Else
                            oResult = New List(Of CVehicleLoanProduct)
                            downloadSettingData._HasVehicleLoanProducts = "NO"
                        End If
                    Else
                        Dim sUrl = poConfig.BaseSubmitLoanUrl & "/resources.ashx/orgs/" & poConfig.OrgCode & "/fis/" & poConfig.LenderCode & "/settings/vl/products"
                        Dim sReponse = Common.SubmitRestApiGet(poConfig, sUrl)
						'Get detail for products
						Dim xmlDoc As New XmlDocument
						xmlDoc.LoadXml(sReponse)
						Dim productNodes = xmlDoc.GetElementsByTagName("PRODUCT")
						If productNodes IsNot Nothing AndAlso productNodes.Count > 0 Then
							Dim oDeserializedOutput As CVehicleLoanProduct
							Dim serializer As New XmlSerializer(GetType(CVehicleLoanProduct))
							For Each product As XmlNode In productNodes
								Dim sProductCode As String = product.SelectSingleNode("PRODUCT_CODE").InnerText
								If sProductCode = "" Then Continue For
								sReponse = Common.SubmitRestApiGet(poConfig, sUrl & "/" & sProductCode)

								If String.IsNullOrEmpty(sReponse) Then	''this is need so this process can continue and cache other questions to cache.  Don't wnat to tram the traffic if this process is run many times
									_log.WarnFormat("Can't download _CVehicleLoanProduct:{0}.  _CVehicleLoanProduct name on backoffice may need to be fixed by removing any special characters", sProductCode)
									Continue For
								End If

								Using reader As TextReader = New StringReader(sReponse)
									oDeserializedOutput = CType(serializer.Deserialize(reader), CVehicleLoanProduct)
								End Using
								oResult.Add(oDeserializedOutput)
							Next

							If oResult IsNot Nothing AndAlso oResult.Count > 0 Then	  ' count less than 0 mean there is probably error in download so don't wnat to store in session
								_log.Info("_CVehicleLoanProduct Object " & sCacheKey & " was created. ")
								downloadSettingData._VehicleLoanProducts = oResult
								downloadSettingData._HasVehicleLoanProducts = "YES"
							Else
								oResult = New List(Of CVehicleLoanProduct)
								downloadSettingData._HasVehicleLoanProducts = "NO"
							End If
						Else
							oResult = New List(Of CVehicleLoanProduct)
							downloadSettingData._HasVehicleLoanProducts = "NO"
						End If
						SaveCacheDataToDatabase(sCacheKey, downloadSettingData)
					End If
					SetCache(sCacheKey, downloadSettingData)
				End If

			End SyncLock
		Catch ex As Exception
			_log.Error("Can't get _CVehicleLoanProduct", ex)
		End Try
		Return oResult
	End Function
End Class




Namespace DownloadedSettings



#Region "basic Class object, target for deserialization"

    '----these top xml elements are needed to mimic xml output from service, these will not be final object
    <Serializable(), XmlRoot("OUTPUT")>
    Public Class COutput
        <XmlElement("RESPONSE")>
        Property Response As CResponse
    End Class

    <Serializable(), XmlRoot("RESPONSE")>
    Public Class CResponse
        <XmlElement("FIELD_LIST")>
        Property Field_list As CFieldList
    End Class
    '---

    <Serializable(), XmlRoot("FIELD_LIST")>
    Public Class CFieldList
        <XmlElement("LIST_ITEM")>
        Property ListItems As New List(Of CListItem)
    End Class

    <Serializable()>
    Public Class CListItem
        <XmlAttribute("text")>
        Public Text As String
        <XmlAttribute("value")>
        Public Value As String
    End Class


#End Region

#Region "ID card  object, target for deserialization"

    <Serializable(), XmlRoot("OUTPUT")>
    Public Class CIDCardOutput
        <XmlElement("RESPONSE")>
        Property Response As CIDCardResponse
    End Class

    <Serializable(), XmlRoot("RESPONSE")>
    Public Class CIDCardResponse
        <XmlElement("FIELD_LIST")>
        Property Field_list As CIDCardFieldList
    End Class

    <Serializable(), XmlRoot("FIELD_LIST")>
    Public Class CIDCardFieldList
        <XmlAttribute("name")>
        Public name As String
        <XmlElement("LIST_ITEM")>
        Property ListItems As New List(Of CIDCardListItem)
    End Class

    '<LIST_ITEM text="AUTO INSURANCE CARD" value="AUTO_INSURANCE">
    '			<FIELD_NAMES>
    '				<FIELD_NAME text="IsCountryVisible" value="False"/>
    '				<FIELD_NAME text="IsCountryRequired" value="False"/>
    '				<FIELD_NAME text="IsCardNumberVisible" value="True"/>
    '				<FIELD_NAME text="IsCardNumberRequired" value="True"/>
    '				<FIELD_NAME text="IsDateIssuedVisible" value="False"/>
    '				<FIELD_NAME text="IsDateIssuedRequired" value="False"/>
    '				<FIELD_NAME text="IsDateExpireVisible" value="False"/>
    '				<FIELD_NAME text="IsDateExpireRequired" value="False"/>
    '				<FIELD_NAME text="IsStateVisible" value="False"/>
    '				<FIELD_NAME text="IsStateRequired" value="False"/>
    '			</FIELD_NAMES>
    '			<APPLICANT_TYPES>
    '				<APPLICANT_TYPE text="IsLoanApplicantsPrimaryID" value="False"/>
    '				<APPLICANT_TYPE text="IsLoanApplicantsSecondaryID" value="False"/>
    '				<APPLICANT_TYPE text="IsXAApplicantsPrimaryID" value="False"/>
    '				<APPLICANT_TYPE text="IsXAApplicantsSecondaryID" value="False"/>
    '			</APPLICANT_TYPES>
    '		</LIST_ITEM>

    <Serializable()>
    Public Class CIDCardListItem
        <XmlAttribute("text")>
        Public Text As String
        <XmlAttribute("value")>
        Public Value As String
        <XmlElement("FIELD_NAMES")>
        Public FieldNames As CIDFiledNames
        <XmlElement("APPLICANT_TYPES")>
        Public ApplicatTypes As CIDCardAppTypes
    End Class

    Public Class CIDFiledNames
        <XmlElement("FIELD_NAME")>
        Property FieldNames As New List(Of CIDCardFeldName)
    End Class

    Public Class CIDCardAppTypes
        <XmlElement("APPLICANT_TYPE")>
        Property ApplicantType As New List(Of CIDCardAppType)
    End Class

    <Serializable()>
    Public Class CIDCardFeldName
        <XmlAttribute("text")>
        Public Text As String
        <XmlAttribute("value")>
        Public Value As String
    End Class

    <Serializable()>
    Public Class CIDCardAppType
        <XmlAttribute("text")>
        Public Text As String
        <XmlAttribute("value")>
        Public Value As String
    End Class

#End Region

#Region "Banch object "

    '****only active branches are returned
    '	<BRANCH>
    '	<BRANCH_CODE>1e2505ef734446e2b58000ff029b78ba</BRANCH_CODE>
    '	<BRANCH_NAME>Anaheim</BRANCH_NAME>
    '	<ADDRESS>111 Magnolia</ADDRESS>
    '	<CITY>Anaheim</CITY>
    '	<STATE>CA</STATE>
    '	<ZIP>92804</ZIP>
    '	<PHONE>(714) 555-8888</PHONE>
    '	<FAX>7145558889</FAX>
    '	<EMAIL/>
    '	<MANAGER_NAME/>
    '	<WEBSITE/>
    '	<ZIP_CODE_POOL_ID>TODO</ZIP_CODE_POOL_ID>
    '	<IS_BUSINESS_BRANCH>N</IS_BUSINESS_BRANCH>
    '	<IS_CREDIT_CARD_BRANCH>Y</IS_CREDIT_CARD_BRANCH>
    '	<IS_HOME_EQUITY_BRANCH>Y</IS_HOME_EQUITY_BRANCH>
    '	<IS_MORTGAGE_BRANCH>Y</IS_MORTGAGE_BRANCH>
    '	<IS_PERSONAL_BRANCH>Y</IS_PERSONAL_BRANCH>
    '	<IS_VEHICLE_BRANCH>Y</IS_VEHICLE_BRANCH>
    '	<IS_XPRESS_BRANCH>Y</IS_XPRESS_BRANCH>
    '	<IS_AVAILABLE_TO_CONSUMER_LOANS>Y</IS_AVAILABLE_TO_CONSUMER_LOANS>
    '	<IS_AVAILABLE_TO_CONSUMER_XA>Y</IS_AVAILABLE_TO_CONSUMER_XA>
    '	<BRANCH_REFERENCE_ID>35</BRANCH_REFERENCE_ID>
    '	<BRANCH_REFERENCE_ID_2/>
    '	<E_FUNDS_BRANCH_ID/>
    '	<FRE_BRANCH_ID/>
    '	<DELUXE_ORG_REPORTING_UNIT/>
    '	<EWS_ORG_REPORTING_UNIT/>
    '	<TELE_CHECK_SUBSCRIBER_NUMBER/>
    '	<TELE_CHECK_T_ID/>
    '	<TELE_CHECK_D_ID/>
    '	<CONSUMER_PULL_EXP>N</CONSUMER_PULL_EXP>
    '	<CONSUMER_PULL_TUC>N</CONSUMER_PULL_TUC>
    '	<CONSUMER_PULL_EQF>N</CONSUMER_PULL_EQF>
    '</BRANCH>

    <Serializable(), XmlRoot("BRANCH")>
    Public Class CBranch
        <XmlElement("BRANCH_NAME")>
        Public BranchName As String
        <XmlElement("BRANCH_CODE")>
        Public BranchCode As String
        <XmlElement("ADDRESS")>
        Public BranchAddress As String
        <XmlElement("CITY")>
        Public BranchCity As String
        <XmlElement("STATE")>
        Public BranchState As String
        <XmlElement("ZIP")>
        Public BranchZip As String
        <XmlElement("PHONE")>
        Public BranchPhone As String
        <XmlElement("EMAIL")>
        Public BranchEmail As String
        <XmlElement("IS_CREDIT_CARD_BRANCH")>
        Public IsCredit As String
        <XmlElement("IS_HOME_EQUITY_BRANCH")>
        Public IsHE As String
        <XmlElement("IS_MORTGAGE_BRANCH")>
        Public IsMortgage As String
        <XmlElement("IS_PERSONAL_BRANCH")>
        Public IsPL As String
        <XmlElement("IS_VEHICLE_BRANCH")>
        Public IsVL As String
        <XmlElement("IS_XPRESS_BRANCH")>
        Public IsXA As String
        <XmlElement("IS_AVAILABLE_TO_CONSUMER_LOANS")>
        Public IsAvailableToConsumerLoan As String
        <XmlElement("IS_AVAILABLE_TO_CONSUMER_XA")>
        Public IsAvailableToConsumerXA As String
        <XmlElement("BRANCH_REFERENCE_ID")>
        Public BranchReferenceID As String
        <XmlElement("ZIP_CODE_POOL_NAME")>
        Public ZipCodePoolName As String
    End Class
    <Serializable(), XmlRoot("PRODUCT")>
    Public Class CCreditCardProduct
        <XmlElement("IS_ACTIVE")>
        Public IsActive As String
        <XmlElement("LOCATION_POOL")>
        Public ZipCodePoolName As String
        <XmlElement("AVAILABLE_ZIP_CODE_FILTER_TYPE")>
        Public ZipCodeFilterType As String
        <XmlElement("CARD_TYPE")>
        Public CreditCardType As String
        <XmlElement("CARD_NAME")>
        Public CreditCardName As String
    End Class
    <Serializable(), XmlRoot("PRODUCT")>
    Public Class CVehicleLoanProduct
        <XmlElement("IS_ACTIVE")>
        Public IsActive As String
        <XmlElement("LOCATION_POOL")>
        Public ZipCodePoolName As String
        <XmlElement("AVAILABLE_ZIP_CODE_FILTER_TYPE")>
        Public ZipCodeFilterType As String
        <XmlElement("ACCEPTABLE_PURPOSE_TYPES")>
        Public AcceptablePurposeTypes As String
        <XmlElement("VEHICLE_TYPES")>
        Public VehicleType As String
        <XmlElement("PRODUCT_CODE")>
        Public ProductCode As String
    End Class
    <Serializable(), XmlRoot("PRODUCT")>
    Public Class CPersonalLoanProduct
        <XmlElement("IS_ACTIVE")>
        Public IsActive As String
        <XmlElement("LOCATION_POOL")>
        Public ZipCodePoolName As String
        <XmlElement("AVAILABLE_ZIP_CODE_FILTER_TYPE")>
        Public ZipCodeFilterType As String
        <XmlElement("ACCEPTABLE_PURPOSE_TYPES")>
        Public AcceptablePurposeTypes As String
        <XmlElement("PRODUCT_CODE")>
        Public ProductCode As String
    End Class
    <Serializable(), XmlRoot("PRODUCT")>
    Public Class CHomeEquityLoanProduct
        <XmlElement("IS_ACTIVE")>
        Public IsActive As String
        <XmlElement("LOCATION_POOL")>
        Public ZipCodePoolName As String
        <XmlElement("AVAILABLE_ZIP_CODE_FILTER_TYPE")>
        Public ZipCodeFilterType As String
        <XmlElement("PRODUCT_CODE")>
        Public ProductCode As String
        <XmlElement("PROGRAM_NAME")>
        Public ProgramName As String
    End Class

#End Region

End Namespace