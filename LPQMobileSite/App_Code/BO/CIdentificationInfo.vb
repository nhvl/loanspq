﻿<Serializable()>
Public Class CIdentificationInfo
	Public CardType As String
	Public CardNumber As String
	Public DateIssued As String
	Public ExpirationDate As String
	Public State As String
	Public Country As String
End Class