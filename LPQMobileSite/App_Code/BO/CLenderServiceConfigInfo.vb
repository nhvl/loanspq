﻿
Imports System.Xml
Imports System.Net
Imports System.IO
Imports System.Web
Imports LPQMobile.Utils

<Serializable()> _
Public Class CLenderServiceConfigInfo
	Private Shared _log As log4net.ILog = log4net.LogManager.GetLogger(GetType(CLenderServiceConfigInfo))

	Public IsSupportMailFunding As Boolean
	Public IsSupportCC As Boolean
	Public IsSupportACHFunding As Boolean
	Public IsSupportInternalTransfer As Boolean
	Public IsSupportNotFunding As Boolean = False

	Public LenderName As String = ""
    Public LenderPhone As String = ""
    Public LenderEmail As String = ""
    Public zipCodeList As New List(Of String)
	Public BranchNamesDic As New SerializableDictionary(Of String, String)
	Public ZipCodePool As New SerializableDictionary(Of String, List(Of String))
	Public ZipCodePoolName As New SerializableDictionary(Of String, String)
    Public Sub New()

	End Sub

	Sub New(ByVal poCurrentConfig As CWebsiteConfig, ByVal oNode As XmlElement)
        Dim oDepositNode As XmlElement = CType(oNode.SelectSingleNode("DEPOSIT_FUNDING_OPTIONS"), XmlElement)
        If oDepositNode IsNot Nothing AndAlso oDepositNode.GetAttribute("is_support_ach_funding") <> "" Then
            Me.IsSupportMailFunding = Convert.ToBoolean(oDepositNode.GetAttribute("is_support_mail_check_funding"))
            Me.IsSupportCC = Convert.ToBoolean(oDepositNode.GetAttribute("is_support_cc_funding"))
            Me.IsSupportACHFunding = Convert.ToBoolean(oDepositNode.GetAttribute("is_support_ach_funding"))
            Me.IsSupportInternalTransfer = Convert.ToBoolean(oDepositNode.GetAttribute("is_support_internal_transfer_funding"))
        End If

        LenderName = oNode.GetAttribute("name")
		If LenderName = "" Then LenderName = "lender"

		''get lender phone, email from download
		If oNode.GetAttribute("phone") IsNot Nothing Then
			Me.LenderPhone = oNode.GetAttribute("phone")
		End If
		If oNode.GetAttribute("email") IsNot Nothing Then
			Me.LenderEmail = oNode.GetAttribute("email")
		End If
		''get all branch names and branch reference id
		Dim oBranchNodes As XmlNodeList = oNode.SelectNodes("BRANCHES/BRANCH")
		If oBranchNodes IsNot Nothing Then
			For Each oBrandNode As XmlNode In oBranchNodes
				Dim branchName As String = Common.SafeString(oBrandNode.Attributes("name").InnerXml)
				Dim branchId As String = Common.SafeString(oBrandNode.Attributes("branch_id").InnerXml)
				If Not String.IsNullOrEmpty(branchName) Then
					If Not BranchNamesDic.ContainsKey(branchId) Then
						BranchNamesDic.Add(branchId, branchName)
					End If
				End If
			Next
		End If

		' ------
		IsSupportNotFunding = True

		For Each oZipCodePool As XmlElement In oNode.SelectNodes("ZIP_CODE_POOLS/ZIP_CODE_POOL")
			Dim zcpId As String = oZipCodePool.GetAttribute("zip_code_pool_id")
			Dim oZipNodes As XmlNodeList = oZipCodePool.GetElementsByTagName("ZIP_CODE")
			If oZipNodes IsNot Nothing AndAlso oZipNodes.Count > 0 Then
				For Each oZipNode As XmlNode In oZipNodes
					Dim zipCode As String = ""
					If oZipNode.Attributes("zip_code") IsNot Nothing Then
						zipCode = oZipNode.Attributes("zip_code").InnerXml
					End If
					If Not String.IsNullOrEmpty(zipCode) Then
						If ZipCodePool.ContainsKey(zipCode) Then
							ZipCodePool(zipCode).Add(zcpId)
						Else
							ZipCodePool.Add(zipCode, New List(Of String)() From {zcpId})
						End If
					End If
				Next
				If zcpId <> "" AndAlso Not ZipCodePoolName.ContainsKey(zcpId) Then
					ZipCodePoolName.Add(zcpId, Common.SafeString(oZipCodePool.GetAttribute("pool_name")))
				End If
			End If
		Next
	End Sub

	


End Class
