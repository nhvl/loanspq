﻿Imports Microsoft.VisualBasic
Imports System.Xml.Serialization
Imports System.Xml.Schema
Imports System.Xml


Public Class SerializableList(Of T)
	Inherits List(Of T)
	Implements IXmlSerializable

	Public Sub New()

	End Sub
	Public Sub New(list As IList(Of T))
		MyBase.New(list)
	End Sub

	
	Public Sub New(capacity As Integer)
		MyBase.New(capacity)
	End Sub

	Public Function GetSchema() As XmlSchema Implements IXmlSerializable.GetSchema
		Return Nothing
	End Function

	Public Sub ReadXml(ByVal reader As XmlReader) Implements IXmlSerializable.ReadXml
		Dim serializer As New XmlSerializer(GetType(T))
		Dim wasEmpty = reader.IsEmptyElement
		reader.Read()
		If wasEmpty Then
			Return
		End If
		While reader.NodeType <> XmlNodeType.EndElement
			reader.ReadStartElement("item")
			Dim value As T = CType(serializer.Deserialize(reader), T)
			reader.ReadEndElement()
			Add(value)
			reader.MoveToContent()

		End While
		reader.ReadEndElement()
	End Sub

	Public Sub WriteXml(ByVal writer As XmlWriter) Implements IXmlSerializable.WriteXml
		Dim serializer As New XmlSerializer(GetType(T))
		For i As Integer = 0 To Count - 1
			writer.WriteStartElement("item")
			Dim value As T = Item(i)
			serializer.Serialize(writer, value)
			writer.WriteEndElement()
		Next
	End Sub

End Class
