﻿<Serializable()>
Public Class CXAPersonalInfo
	Public EmployeeOfLender As String
	Public MemberNumber As String
	Public RelationshipToPrimary As String
	Public FirstName As String
	Public MiddleName As String
	Public LastName As String
	Public Suffix As String
	Public Gender As String
	Public DateOfBirth As String
	Public MotherMaidenName As String
	Public Citizenship As String
	Public MaritalStatus As String
	Public SSN As String
	Public EmploymentSectionEnabled As Boolean = False
	Public EmploymentStatus As String
	Public EmploymentDescription As String

	Public txtJobTitle As String
	Public ddlBranchOfService As String
	Public txtEmployer As String
	Public ddlPayGrade As String
	Public txtSupervisorName As String
	Public txtBusinessType As String
	Public txtEmploymentStartDate As String
	Public txtEmployedDuration_year As String
	Public txtEmployedDuration_month As String
	Public txtProfessionDuration_year As String
	Public txtProfessionDuration_month As String
	Public txtETS As String
	Public txtGrossMonthlyIncome As Double
	Public HousingPayment As Double = 0.0

	''previous employment information
	Public hasPrevEmployment As String
	Public prev_EmploymentStatus As String
	Public prev_txtJobTitle As String
	Public prev_ddlBranchOfService As String
	Public prev_txtEmployer As String
	Public prev_ddlPayGrade As String
	Public prev_txtBusinessType As String
	Public prev_txtEmploymentStartDate As String
	Public prev_txtEmployedDuration_year As String
	Public prev_txtEmployedDuration_month As String
	Public prev_txtETS As String
	Public prev_GrossMonthlyIncome As Double

	''hasMinor
	Public HasMinor As String

End Class