﻿Imports LPQMobile.Utils
Imports System.Xml
Imports System.Text

<Serializable()>
Public Class AccountInfo

	Private ReadOnly _log As log4net.ILog = log4net.LogManager.GetLogger(Me.GetType())

	Private oConfig As CWebsiteConfig
	Public Sub New(ByVal poConfig As CWebsiteConfig)
		Me.oConfig = poConfig
	End Sub

	Public Amount As Double
	Public Product As String	' saving product code

	Public ProductServices As List(Of String)

	Public ProductRate As String

	Public Property Term As String
	Public Property TermType As String
	Public Property Apy As String

	Public CustomQuestions As List(Of CProductCustomQuestion)
	'' update toxmlString function by adding coSSN parameter to the function
	Public Function ToXmlString(ByVal listProducts As List(Of CProduct), Optional ByVal pbIsApproved As Boolean = False, Optional ByVal psFundType As String = "", Optional ByVal psCoSSN As String = "", Optional ByVal psGuardianSSN As String = "", Optional ByVal oCoAppSSNDic As Dictionary(Of String, String) = Nothing, Optional oBeneficiaryList As List(Of CBeneficiaryInfo) = Nothing) As String
		' This function will match the selected account product with the one in the XML config file.
		' It will then fill in the initial deposit amount and construct the XML
		Dim prod As CProduct = listProducts.FirstOrDefault(Function(p) p.ProductCode = Product)
		If prod Is Nothing Then Return ""
		Dim sb As New StringBuilder
		Dim settings As New XmlWriterSettings()
		settings.Indent = True
		settings.OmitXmlDeclaration = True
		Dim writer As XmlWriter = XmlTextWriter.Create(sb, settings)

		Dim selectedRate As CProductRate = Nothing
		Dim matchingProduct = prod.Clone() 'do a clone so we don't modify the original cached copy
		matchingProduct.Services = matchingProduct.Services.Where(Function(x) Me.ProductServices.Contains(x.ServiceCode)).ToList()
		If prod.AvailableRates IsNot Nothing AndAlso prod.AvailableRates.Any() Then
			selectedRate = prod.AvailableRates.FirstOrDefault(Function(r) r.RateCode = ProductRate)
		End If

		' Write out the XML 
		writer.WriteStartElement("ACCOUNT_TYPE")
		writer.WriteAttributeString("account_name", matchingProduct.AccountName)

		If selectedRate Is Nothing Then
			Dim writingRate As String = "-999999"
			Dim writingTerm As String = "-16959"
			Dim writingApy As String = "-999999"
			If Not String.IsNullOrEmpty(matchingProduct.Rate) AndAlso matchingProduct.Rate <> "-999999" Then
				writingRate = FormatNumber(matchingProduct.Rate, 4).Replace(",", "")
			End If

			If Not String.IsNullOrEmpty(matchingProduct.Apy) AndAlso matchingProduct.Apy <> "-999999" Then
				writingApy = FormatNumber(matchingProduct.Apy, 4).Replace(",", "")
			End If
			If Not String.IsNullOrEmpty(matchingProduct.Term) Then writingTerm = matchingProduct.Term
			writer.WriteAttributeString("rate", writingRate)
			writer.WriteAttributeString("apy", writingApy)
			writer.WriteAttributeString("term", writingTerm)
		Else
			Dim writingRate As String = "0"
			Dim writingTerm As String = "-16959"
			' If the RateString is empty, send a 0 instead to avoid CLF errors.
			If Not String.IsNullOrEmpty(selectedRate.RateString) Then
				writingRate = selectedRate.RateString
			End If
			writer.WriteAttributeString("rate", writingRate)
			writer.WriteAttributeString("rate_code", selectedRate.RateCode)
			If Not pbIsApproved Then
				writer.WriteAttributeString("min_deposit", Common.SafeDouble(selectedRate.MinDeposit).ToString())
			End If
			writer.WriteAttributeString("apy", FormatNumber(Apy, 4).Replace(",", ""))
			writer.WriteAttributeString("term_type", matchingProduct.TermType)
			If Not String.IsNullOrEmpty(Term) Then
				writingTerm = Term
			End If
			writer.WriteAttributeString("term", writingTerm)
		End If
		writer.WriteAttributeString("product_code", matchingProduct.ProductCode)
		writer.WriteAttributeString("account_type", matchingProduct.AccountType)
		writer.WriteAttributeString("amount_deposit", Amount)

		'this is required for PDF mapping
		If Not String.IsNullOrEmpty(matchingProduct.InterestRateType) Then
			writer.WriteAttributeString("interest_rate_type", matchingProduct.InterestRateType)
		End If

		'schema only allow funding source for approved account
		If psFundType <> "" And pbIsApproved And matchingProduct.cannotBeFunded <> "Y" Then
			If psFundType = "CASH" Then
				writer.WriteAttributeString("clf_funding_source_id", "0")
			ElseIf psFundType = "MAIL" Then
				writer.WriteAttributeString("clf_funding_source_id", "1")
			ElseIf psFundType = "NOT_FUNDING" Then
				writer.WriteAttributeString("clf_funding_source_id", "2")
			ElseIf psFundType = "PAYPAL" Then
				writer.WriteAttributeString("clf_funding_source_id", "3")
			ElseIf psFundType = "INTERNALTRANSFER" Then
				writer.WriteAttributeString("clf_funding_source_id", "3")
			ElseIf psFundType = "BANK" Then
				writer.WriteAttributeString("clf_funding_source_id", "3")
			ElseIf psFundType = "CREDITCARD" Then
				writer.WriteAttributeString("clf_funding_source_id", "3")
			Else
				writer.WriteAttributeString("clf_funding_source_id", "10")
			End If
		End If

		'writer.WriteAttributeString("IS_REQUIRED", matchingAccountNode.Attributes("is_required").InnerXml)



		'' write selected SERVICES node if available from LPQ as TODO required
		If matchingProduct.Services IsNot Nothing AndAlso matchingProduct.Services.Count > 0 Then
			writer.WriteStartElement("SERVICES")
			For Each serv In matchingProduct.Services
				writer.WriteStartElement("SERVICE")
				writer.WriteAttributeString("description", serv.Description)
				writer.WriteAttributeString("service_code", serv.ServiceCode)
				writer.WriteAttributeString("service_type", serv.ServiceType)
				writer.WriteEndElement()
			Next
			writer.WriteEndElement()
		End If

		'' if has Joint Applicant and product level ownership-> add acount_type_relations in account type
		Dim hasOnwershipLevel As String = Common.hasOwnerShipLevel(oConfig)

		If pbIsApproved Then
			writer.WriteStartElement("ACCOUNT_TYPE_RELATIONS")
			If hasOnwershipLevel = "Y" Then
				If Not String.IsNullOrWhiteSpace(psGuardianSSN) Then	''for minor account(type=1a/2a)
					writer.WriteStartElement("RELATION")
					writer.WriteAttributeString("ssn", psGuardianSSN)
					writer.WriteAttributeString("benefactor_type", "C")
					writer.WriteEndElement()  'end  RELATION
					If Not String.IsNullOrWhiteSpace(psCoSSN) Then
						writer.WriteStartElement("RELATION")
						writer.WriteAttributeString("ssn", psCoSSN)
						writer.WriteAttributeString("benefactor_type", "J")
						writer.WriteEndElement()  'end  RELATION
					End If
				ElseIf oCoAppSSNDic IsNot Nothing AndAlso oCoAppSSNDic.Count > 0 Then	'' for Business/Special XA
					For Each oSSN As KeyValuePair(Of String, String) In oCoAppSSNDic
						writer.WriteStartElement("RELATION")
						writer.WriteAttributeString("ssn", oSSN.Key)
						writer.WriteAttributeString("benefactor_type", IIf(oSSN.Value.ToUpper = "Y", "J", "C"))
						writer.WriteEndElement()  'end  RELATION
					Next
				ElseIf Not String.IsNullOrWhiteSpace(psCoSSN) Then
					writer.WriteStartElement("RELATION")
					writer.WriteAttributeString("ssn", psCoSSN)
					'If hasOnwershipLevel = "Y" Then
					writer.WriteAttributeString("benefactor_type", "J")
					'End If
					writer.WriteEndElement()  'end  RELATION
				End If
			End If
			If oBeneficiaryList IsNot Nothing AndAlso oBeneficiaryList.Any() Then
				For Each item As CBeneficiaryInfo In oBeneficiaryList
					writer.WriteStartElement("RELATION")
					writer.WriteAttributeString("ssn", item.ssn)
					writer.WriteAttributeString("beneficiary_percent_share", item.percentShare)
					writer.WriteAttributeString("beneficiary_priority", "P")
					writer.WriteAttributeString("benefactor_type", "P")
					writer.WriteEndElement()  'end  RELATION
				Next
			End If
			writer.WriteEndElement()  'ACCOUNT_TYPE_RELATIONS
		End If
		' begin add product custom question '
		If Me.CustomQuestions IsNot Nothing AndAlso Me.CustomQuestions.Count > 0 Then
			If pbIsApproved Then
				writer.WriteStartElement("PRODUCT_CUSTOM_QUESTIONS_APPROVED")
			Else
				writer.WriteStartElement("PRODUCT_CUSTOM_QUESTIONS_INTERESTED")
			End If

			For Each question As CProductCustomQuestion In Me.CustomQuestions
				writer.WriteStartElement("CUSTOM_QUESTION")
				writer.WriteAttributeString("question_name", question.QuestionName)
				writer.WriteAttributeString("question_type", question.AnswerType)
				If String.Equals(question.AnswerType, "CHECKBOX") OrElse String.Equals(question.AnswerType, "DROPDOWN") Then
					For Each ans As CProductRestrictedAnswer In question.RestrictedAnswers
						If ans.isSelected Then
							writer.WriteStartElement("CUSTOM_QUESTION_ANSWER")
							writer.WriteAttributeString("answer_text", ans.Text)
							writer.WriteAttributeString("answer_value", ans.Value)
							writer.WriteEndElement() 'end CUSTOM_QUESTION_ANSWER
							'' check box can have one or more answer -> need to check all the answers
							'' Exit For  ->correct only for dropdown (not for checkbox)
						End If
					Next
				Else
					writer.WriteStartElement("CUSTOM_QUESTION_ANSWER")
					writer.WriteAttributeString("answer_text", question.productAnswerText)
					writer.WriteAttributeString("answer_value", question.productAnswerText)
					writer.WriteEndElement() 'end CUSTOM_QUESTION_ANSWER
				End If
				writer.WriteEndElement()  'end CUSTOM_QUESTION
			Next
			writer.WriteEndElement() 'end PRODUCT_CUSTOM_QUESTIONS_APPROVED
		End If
		' end add product custom question '

		'writer.WriteStartAttribute("create_date")
		'writer.WriteValue(DateTime.Now)
		'writer.WriteEndAttribute()
		'' get the joint CoApp
		writer.WriteEndElement() 'end ACCOUNT_TYPE
		writer.Flush()
		writer.Close()

		Console.WriteLine(sb.ToString())

		Dim xmlStr As String
		xmlStr = sb.ToString()

		Console.WriteLine(xmlStr)

		'_log.Info("AccountInfoXML: " + xmlStr)

		Return xmlStr



	End Function
End Class
