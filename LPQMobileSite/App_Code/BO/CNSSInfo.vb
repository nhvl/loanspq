﻿Imports Microsoft.VisualBasic

Public Class CNSSInfo
	Public Property FirstName As String
	Public Property LastName As String
	Public Property MiddleName As String
	Public Property Suffix As String
	Public Property SSN As String
	Public Property Email As String
	Public Property HomePhone As String
	Public Property HomePhoneCountry As String
	Public Property CellPhone As String
	Public Property CellPhoneCountry As String
	Public Property Address As String
	Public Property Address2 As String
	Public Property City As String
	Public Property State As String
	Public Property Zip As String
	Public Property Country As String
	Public Property MarriedTo As String
End Class
