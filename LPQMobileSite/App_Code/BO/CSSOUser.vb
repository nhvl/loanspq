﻿Imports Microsoft.VisualBasic

Public Class CSSOUser
	Public Property OrganizationCode As String
	Public Property LoanOfficerCode As String
	Public Property Login As String
	Public Property LName As String
	Public Property MName As String
	Public Property FName As String
	Public Property Email As String
	Public Property Phone As String
	Public Property LenderCode As String
	Public Property AccessRights As CSSOAccessRight
End Class
