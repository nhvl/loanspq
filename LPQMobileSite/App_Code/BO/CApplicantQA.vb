﻿Imports LPQMobile.BO
<Serializable()>
Public Class CApplicantQA
	Public CQName As String	'question
	Public CQAnswer As String	 'answer
	Public CQRole As QuestionRole
	Public CQLocation As CustomQuestionLocation
	Public CQApplicantPrefix As String
End Class
<Serializable()>
Public Structure CValidatedQuestionAnswer
	Public AnswerValue As String
	Public AnswerText As String

End Structure
<Serializable()>
Public Structure CValidatedQuestionAnswers
	Public Question As CCustomQuestionXA
	Public Answers As List(Of CValidatedQuestionAnswer)

	Public Shared Function CollectValidatedQuestionAnswers(ByVal availableQuestions As IEnumerable(Of CCustomQuestionXA), ByVal givenAnswers As IEnumerable(Of CApplicantQA)) As List(Of CValidatedQuestionAnswers)
		If availableQuestions Is Nothing OrElse Not availableQuestions.Any() Then Return New List(Of CValidatedQuestionAnswers)(0)
		If givenAnswers Is Nothing OrElse Not givenAnswers.Any() Then Return New List(Of CValidatedQuestionAnswers)(0)

		Dim answerLookup As ILookup(Of QuestionIdentifier, CApplicantQA) = givenAnswers.ToLookup(Function(cqa) New QuestionIdentifier(cqa.CQName, cqa.CQRole))
		Dim results As New List(Of CValidatedQuestionAnswers)

		For Each cq As CCustomQuestionXA In availableQuestions
			If Not answerLookup.Contains(cq.Identifer) Then Continue For

			Dim rawAnswers As List(Of CApplicantQA) = answerLookup.Item(cq.Identifer).ToList()
			If Not rawAnswers.Any() Then Continue For

			Dim answers As New List(Of CValidatedQuestionAnswer)
			If cq.AnswerType = "TEXTBOX" Then
				Dim rawAnswer As CApplicantQA = rawAnswers.First()
				answers.Add(New CValidatedQuestionAnswer With {
							.AnswerValue = rawAnswer.CQAnswer,
							.AnswerText = rawAnswer.CQAnswer
							})
			ElseIf cq.AnswerType = "DROPDOWN" Then
				Dim rawAnswer As CApplicantQA = rawAnswers.First()
				Dim opt As CCustomQuestionOption = cq.CustomQuestionOptions.FirstOrDefault(Function(o) o.Value = rawAnswer.CQAnswer)
				If opt Is Nothing Then Throw New Exception("The answer value '" + rawAnswer.CQAnswer +
														   "' is not a valid answer for the Custom Question '" + cq.Name +
														   "'. Available answers are: " + String.Join(", ", cq.CustomQuestionOptions.Select(Function(o) "'" + o.Value + "'")))
				answers.Add(New CValidatedQuestionAnswer With {
							.AnswerValue = opt.Value,
							.AnswerText = opt.Text
							})
			ElseIf cq.AnswerType = "CHECKBOX" Then
				For Each rawAnswer As CApplicantQA In rawAnswers
					Dim opt As CCustomQuestionOption = cq.CustomQuestionOptions.FirstOrDefault(Function(o) o.Value = rawAnswer.CQAnswer)
					If opt Is Nothing Then Throw New Exception("The answer value '" + rawAnswer.CQAnswer +
															   "' is not a valid answer for the Custom Question '" + cq.Name +
															   "'. Available answers are: " + String.Join(", ", cq.CustomQuestionOptions.Select(Function(o) "'" + o.Value + "'")))
					answers.Add(New CValidatedQuestionAnswer With {
								.AnswerValue = opt.Value,
								.AnswerText = opt.Text
								})
				Next
			Else
				Throw New Exception("Error validating custom question answer. Custom Question with the type '" + cq.AnswerType + "' is unsupported.")
			End If

			results.Add(New CValidatedQuestionAnswers With {
						.Question = cq,
						.Answers = answers
						})
		Next

		Return results
	End Function
End Structure