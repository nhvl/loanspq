﻿Imports Microsoft.VisualBasic
Imports System.Xml
Imports LPQMobile.Utils.Common
<Serializable()> _
Public Class CProductRestrictedAnswer
	Public Text As String
	Public Value As String
	Public Position As Integer

	Public isSelected As Boolean

	Public Sub New()

	End Sub

	Public Sub New(ByVal oAnswerNode As XmlElement)
		Me.Text = oAnswerNode.GetAttribute("text")
		Me.Value = oAnswerNode.GetAttribute("value")
		Me.Position = SafeInteger(oAnswerNode.GetAttribute("position"))

		Me.isSelected = False
	End Sub
End Class
