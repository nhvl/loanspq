﻿Imports Microsoft.VisualBasic
Imports System.Web.Script.Serialization
<Serializable()> _
Public Class CMinorAccount
    Public minorAccountCode As String
    Public minorTitle As String
    Public minorRoleType As String
    Public secondRoleType As String
    Public minorMinAge As String
    Public minorMaxAge As String
    Public employment As String

    Public minorIDAEnable As String
    Public minorDebitEnable As String
    Public decisionXAEnable As String
    Public creditPull As String
    Public debitType As String
    Public authenticationType As String
End Class
<Serializable()>
Public Class MinorAccountProduct
	Public minorAccountCode As String
	Public minorAccountProductList As List(Of String)
End Class


