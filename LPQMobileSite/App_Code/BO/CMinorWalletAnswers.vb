﻿Imports Microsoft.VisualBasic
Imports System.Xml
Imports System.Net
Imports System.IO
Imports LPQMobile.BO
Imports LPQMobile.Utils
Imports System.Text

Public Class CMinorWalletAnswers
    Private log As log4net.ILog = log4net.LogManager.GetLogger(GetType(CMinorWalletAnswers))
    Public Sub New()
    End Sub

    Public Function buildWalletAnswerXML(ByVal questionIDList As List(Of String), ByVal answerIDList As List(Of String), ByVal answerTextList As List(Of String), ByVal sAuthMethod As String, ByVal loanID As String, ByVal oConfig As CWebsiteConfig, ByVal sApplicantIndex As String, ByVal isJoint As Boolean, ByVal question_set_ID As String, ByVal transaction_ID As String, ByVal request_app_ID As String, ByVal request_client_ID As String, ByVal request_sequence_ID As String, ByVal reference_number As String) As String
        Select Case sAuthMethod.ToUpper
            Case "RSA"
                Return buildWalletAnswerXMLRSA(questionIDList, answerIDList, loanID, oConfig, sApplicantIndex, isJoint, question_set_ID, transaction_ID)
            Case "PID"
                Return buildWalletAnswerXMLPID(questionIDList, answerIDList, loanID, oConfig, sApplicantIndex, isJoint, transaction_ID)
            Case "FIS"
                Return buildWalletAnswerXMLFIS(questionIDList, answerIDList, loanID, oConfig, sApplicantIndex, isJoint, question_set_ID, transaction_ID)
            Case "DID"
                Return buildWalletAnswerXMLDID(questionIDList, answerTextList, loanID, oConfig, sApplicantIndex, isJoint, transaction_ID, request_app_ID, request_client_ID, request_sequence_ID)
                'Equifax
            Case "EID"
                Return buildWalletAnswerXMLEquifax(questionIDList, answerIDList, loanID, oConfig, sApplicantIndex, isJoint, transaction_ID)
            Case "TID"
                Return buildWalletAnswerXMLTID(questionIDList, answerIDList, loanID, oConfig, sApplicantIndex, isJoint, reference_number)
        End Select

        Return Nothing

    End Function
    Public Function buildWalletAnswerXMLEquifax(ByVal questionIDList As List(Of String), ByVal answerIDList As List(Of String), ByVal loanID As String, ByVal oConfig As CWebsiteConfig, ByVal sApplicantIndex As String, ByVal isJoint As Boolean, ByVal transaction_ID As String) As String
        Dim sb As New StringBuilder()
        Dim writer As XmlWriter = XmlTextWriter.Create(sb)
        writer.WriteStartDocument()

        ' begin input
        writer.WriteStartElement("INPUT")

        writer.WriteStartElement("LOGIN")

        writer.WriteAttributeString("api_user_id", oConfig.APIUser)
        writer.WriteAttributeString("api_password", oConfig.APIPassword)
        writer.WriteEndElement()
        writer.WriteStartElement("REQUEST")
        writer.WriteAttributeString("transaction_id", transaction_ID)
        writer.WriteAttributeString("app_id", loanID)

        writer.WriteAttributeString("applicant_index", sApplicantIndex)
        If isJoint Then
            writer.WriteAttributeString("is_joint", "true")
        Else
            writer.WriteAttributeString("is_joint", "false")
        End If

        Dim numQuestions As Integer = questionIDList.Count
        writer.WriteStartElement("ANSWERS")
        For i As Integer = 0 To numQuestions - 1
            writer.WriteStartElement("ANSWER")
            writer.WriteAttributeString("question_number", questionIDList(i))
            writer.WriteAttributeString("answer_number", answerIDList(i))
            writer.WriteEndElement()
        Next
        ' END </ANSWERS>
        writer.WriteEndElement()

        ' END </REQUEST>
        writer.WriteEndElement()

        ' END </INPUT>
        writer.WriteEndElement()
        writer.Flush()
        writer.Close()

        Dim returnStr As String = sb.ToString()

        Return returnStr
    End Function
    Protected Function buildWalletAnswerXMLRSA(ByVal questionIDList As List(Of String), ByVal answerIDList As List(Of String), ByVal loanID As String, ByVal oConfig As CWebsiteConfig, ByVal sApplicantIndex As String, ByVal isJoint As Boolean, ByVal question_set_ID As String, ByVal transaction_ID As String) As String
        Dim sb As New StringBuilder()
        Dim writer As XmlWriter = XmlTextWriter.Create(sb)
        writer.WriteStartDocument()

        ' begin input
        writer.WriteStartElement("INPUT")

        writer.WriteStartElement("LOGIN")
    
        writer.WriteAttributeString("api_user_id", oConfig.APIUser)
        writer.WriteAttributeString("api_password", oConfig.APIPassword)
        writer.WriteEndElement()


        writer.WriteStartElement("REQUEST")
        writer.WriteAttributeString("transaction_id", transaction_ID)
        writer.WriteAttributeString("question_set_id", question_set_ID)
        writer.WriteAttributeString("app_id", loanID)
        writer.WriteAttributeString("applicant_index", sApplicantIndex)
       
        If (isJoint) Then
            writer.WriteAttributeString("is_joint", "true")
        Else
            writer.WriteAttributeString("is_joint", "false")
        End If

        Dim numQuestions As Integer = questionIDList.Count
        writer.WriteStartElement("ANSWERS")
        For i As Integer = 0 To numQuestions - 1
            writer.WriteStartElement("ANSWER")
            writer.WriteAttributeString("question_id", questionIDList(i))
            writer.WriteAttributeString("choice_id", answerIDList(i))
            writer.WriteEndElement()
        Next
        ' END </ANSWERS>
        writer.WriteEndElement()

        ' END </REQUEST>
        writer.WriteEndElement()

        ' END </INPUT>
        writer.WriteEndElement()
        writer.Flush()
        writer.Close()

        Dim returnStr As String = sb.ToString()

        Return returnStr
    End Function
    '
    Protected Function buildWalletAnswerXMLPID(ByVal questionIDList As List(Of String), ByVal answerIDList As List(Of String), ByVal loanID As String, ByVal oConfig As CWebsiteConfig, ByVal sApplicantIndex As String, ByVal isJoint As Boolean, ByVal transaction_ID As String) As String
        Dim sb As New StringBuilder()
        Dim writer As XmlWriter = XmlTextWriter.Create(sb)
        writer.WriteStartDocument()

        ' begin input
        writer.WriteStartElement("INPUT")

        writer.WriteStartElement("LOGIN")
        writer.WriteAttributeString("api_user_id", oConfig.APIUser)
        writer.WriteAttributeString("api_password", oConfig.APIPassword)
        writer.WriteEndElement()


        writer.WriteStartElement("REQUEST")
        writer.WriteAttributeString("app_id", loanID)

        writer.WriteAttributeString("applicant_index", sApplicantIndex)

        If isJoint Then
            writer.WriteAttributeString("is_joint", "true")
        Else
            writer.WriteAttributeString("is_joint", "false")
        End If
        writer.WriteAttributeString("session_id", transaction_ID)

        Dim numQuestions As Integer = questionIDList.Count
        writer.WriteStartElement("ANSWERS")
        For i As Integer = 0 To numQuestions - 1
            writer.WriteStartElement("ANSWER")
            writer.WriteAttributeString("answer_text", answerIDList(i))
            writer.WriteEndElement()
        Next
        ' END </ANSWERS>
        writer.WriteEndElement()

        ' END </REQUEST>
        writer.WriteEndElement()

        ' END </INPUT>
        writer.WriteEndElement()
        writer.Flush()
        writer.Close()

        Dim returnStr As String = sb.ToString()

        Return returnStr
    End Function

    Public Function buildWalletAnswerXMLFIS(ByVal questionIDList As List(Of String), ByVal answerIDList As List(Of String), ByVal loanID As String, ByVal oConfig As CWebsiteConfig, ByVal sApplicantIndex As String, ByVal isJoint As Boolean, ByVal question_set_ID As String, ByVal transaction_ID As String) As String
        Dim sb As New StringBuilder()
        Dim writer As XmlWriter = XmlTextWriter.Create(sb)
        writer.WriteStartDocument()
        ' begin input
        writer.WriteStartElement("INPUT")

        writer.WriteStartElement("LOGIN")

        'FIS IDA ONLY work with lender level user
        If oConfig.APIUserLender <> "" Then
            writer.WriteAttributeString("api_user_id", oConfig.APIUserLender)
            writer.WriteAttributeString("api_password", oConfig.APIPasswordLender)
        Else
            writer.WriteAttributeString("api_user_id", oConfig.APIUser)
            writer.WriteAttributeString("api_password", oConfig.APIPassword)
        End If

        writer.WriteEndElement()

        writer.WriteStartElement("REQUEST")
        writer.WriteAttributeString("xpressappid", loanID)
        writer.WriteAttributeString("applicant_index", sApplicantIndex)
        If (isJoint) Then
            writer.WriteAttributeString("applicant_type", "JOINT")
        Else
            writer.WriteAttributeString("applicant_type", "PRIMARY")
        End If


        writer.WriteAttributeString("quiz_id", question_set_ID)
        writer.WriteAttributeString("transaction_id", transaction_ID)

        Dim numQuestions As Integer = questionIDList.Count
        writer.WriteStartElement("ANSWERS")
        For i As Integer = 0 To numQuestions - 1
            writer.WriteStartElement("ANSWER")
            writer.WriteAttributeString("question_id", questionIDList(i))
            writer.WriteAttributeString("answer_id", answerIDList(i))
            writer.WriteEndElement()
        Next
        ' END </ANSWERS>
        writer.WriteEndElement()

        ' END </REQUEST>
        writer.WriteEndElement()

        ' END </INPUT>
        writer.WriteEndElement()
        writer.Flush()
        writer.Close()

        Dim returnStr As String = sb.ToString()

        Return returnStr
    End Function
    Protected Function buildWalletAnswerXMLDID(ByVal questionIDList As List(Of String), ByVal answerIDList As List(Of String), ByVal loanID As String, ByVal oConfig As CWebsiteConfig, ByVal sApplicantIndex As String, ByVal isJoint As Boolean, ByVal transaction_ID As String, ByVal request_app_ID As String, ByVal request_client_ID As String, ByVal request_sequence_ID As String) As String
        Dim sb As New StringBuilder()
        Dim writer As XmlWriter = XmlTextWriter.Create(sb)
        writer.WriteStartDocument()

        ' begin input
        writer.WriteStartElement("INPUT")

        writer.WriteStartElement("LOGIN")
        writer.WriteAttributeString("api_user_id", oConfig.APIUser)
        writer.WriteAttributeString("api_password", oConfig.APIPassword)
        writer.WriteEndElement()

        writer.WriteStartElement("REQUEST")
        writer.WriteAttributeString("xpress_app_id", loanID)
        writer.WriteAttributeString("applicant_index", sApplicantIndex)
        If isJoint Then
            writer.WriteAttributeString("is_joint", "true")
        Else
            writer.WriteAttributeString("is_joint", "false")
        End If

        writer.WriteAttributeString("session_id", transaction_ID)
        writer.WriteAttributeString("request_app_id", request_app_ID)
        writer.WriteAttributeString("request_client_id", request_client_ID)
        writer.WriteAttributeString("request_sequence_id", request_sequence_ID)

        Dim numQuestions As Integer = questionIDList.Count
        writer.WriteStartElement("ANSWERS")
        For i As Integer = 0 To numQuestions - 1
            writer.WriteStartElement("ANSWER")
            writer.WriteAttributeString("question_number", questionIDList(i))
            writer.WriteAttributeString("answer_text", answerIDList(i))
            writer.WriteEndElement()
        Next
        ' END </ANSWERS>
        writer.WriteEndElement()

        ' END </REQUEST>
        writer.WriteEndElement()

        ' END </INPUT>
        writer.WriteEndElement()
        writer.Flush()
        writer.Close()

        Dim returnStr As String = sb.ToString()

        Return returnStr
    End Function

    ''TransUnion build wallet answer
    Protected Function buildWalletAnswerXMLTID(ByVal questionIDList As List(Of String), ByVal answerIDList As List(Of String), ByVal loanID As String, ByVal oConfig As CWebsiteConfig, ByVal sApplicantIndex As String, ByVal isJoint As Boolean, ByVal reference_number As String) As String
        Dim sb As New StringBuilder()
        Dim writer As XmlWriter = XmlTextWriter.Create(sb)
        writer.WriteStartDocument()

        ' begin input
        writer.WriteStartElement("INPUT")

        writer.WriteStartElement("LOGIN")
        writer.WriteAttributeString("api_user_id", oConfig.APIUser)
        writer.WriteAttributeString("api_password", oConfig.APIPassword)
        writer.WriteEndElement()
        writer.WriteStartElement("REQUEST")
        writer.WriteAttributeString("xpressappid", loanID)
        writer.WriteAttributeString("applicant_index", sApplicantIndex)
        writer.WriteAttributeString("mode", "IDA")
        writer.WriteAttributeString("reference_number", reference_number)
        If isJoint Then
            writer.WriteAttributeString("applicant_type", "JOINT")
        Else
            writer.WriteAttributeString("applicant_type", "PRIMARY")
        End If
        Dim numQuestions As Integer = questionIDList.Count
        writer.WriteStartElement("ANSWERS")
        For i As Integer = 0 To numQuestions - 1
            writer.WriteStartElement("ANSWER")
            writer.WriteAttributeString("question_name", questionIDList(i))
            writer.WriteAttributeString("choice_key", answerIDList(i))
            writer.WriteEndElement()
        Next
        ' END </ANSWERS>
        writer.WriteEndElement()

        ' END </REQUEST>
        writer.WriteEndElement()

        ' END </INPUT>
        writer.WriteEndElement()
        writer.Flush()
        writer.Close()

        Dim returnStr As String = sb.ToString()

        Return returnStr
    End Function
End Class
