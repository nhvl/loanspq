﻿

Namespace LPQMobile.BO
	Public Class CArchivedDocument


		Public Property ApprovedAccountId() As String
		Public Property ApplicantId() As String
		Public Property IsJointApplicant() As Boolean
		Public Property MergeSetId() As String
		Public Property FileContentBase64() As String
		Public Property Title As String
		Public Property DocCode As String
		Public Property DocGroup As String
		Public Property CreateDate As DateTime
		Public Property CreateBy As String
		Public Property PdfId As String
		Public Property PageCount As Integer
	End Class
End Namespace

