﻿Imports LPQMobile.Utils

<Serializable()> _
Public Class CFundingSourceInfo
	Public fsFundingType As String

	Public ListTypes As List(Of String)

	Public cNameOnCard As String
	Public cCardType As String ' VISA or MASTER
	Public cCreditCardNumber As String ' number only 16 digits
	Public cExpirationDate As String ' 02 - 2014
	Public cCVNNumber As String
	Public cBillingAddress As String
	Public cBillingZip As String
	Public cBillingCity As String
	Public cBillingState As String

	Public bAccountType As String ' CHECKIGN or SAVINGS
	Public bNameOnCard As String
	Public bAccountNumber As String
	Public bRoutingNumber As String	' 9 digits
	Public bBankName As String
	Public bBankState As String

	Public tAccountNumber As String
	Public tAccountType As String

	Public Shared Function CurrentJsonFundingPackage(websiteConfig As CWebsiteConfig) As CFundingSourceInfo
		Return New CFundingSourceInfo(CDownloadedSettings.DownloadLenderServiceConfigInfo(websiteConfig))
	End Function

	Public Sub New()
		ListTypes = New List(Of String)
		ListTypes.Add("NOT FUNDING AT THIS TIME")
		'ListTypes.Add("CASH")
	End Sub

	Public Function ValueFromTransferType(ByVal text As String) As String
        Select Case text.ToUpper
            Case "MONEY MARKET"
                Return "MONEY_MARKET"
            Case "SAVINGS"
                Return "SAVINGS"
            Case "CHECKING"
                Return "CHECKING"
        End Select
        Return text
    End Function

	Public Function ValueFromType(ByVal text As String) As String
		Select Case text
			Case "NOT FUNDING AT THIS TIME"
				Return "NOT_FUNDING"
			Case "CASH"
				Return "CASH"
			Case "MAIL A CHECK"
				Return "MAIL"
			Case "CREDIT CARD"
				Return "CREDITCARD"
			Case "TRANSFER FROM ANOTHER FINANCIAL INSTITUTION"
				Return "BANK"
			Case "INTERNAL TRANSFER"
				Return "INTERNALTRANSFER"
			Case "PAYPAL"
				Return "PAYPAL"
		End Select

		Return "NOT_FUNDING"
	End Function

	Public Sub New(ByVal pConfig As CLenderServiceConfigInfo)
		ListTypes = New List(Of String)

		If pConfig.IsSupportNotFunding Then
			ListTypes.Add("NOT FUNDING AT THIS TIME")
		End If
		If pConfig.IsSupportCC Then
			ListTypes.Add("CREDIT CARD")
		End If

		If pConfig.IsSupportACHFunding Then
			ListTypes.Add("TRANSFER FROM ANOTHER FINANCIAL INSTITUTION")
		End If

		If pConfig.IsSupportMailFunding Then
			ListTypes.Add("MAIL A CHECK")
		End If

		If pConfig.IsSupportInternalTransfer Then
			ListTypes.Add("INTERNAL TRANSFER")
		End If

		' ListTypes.Add("CASH")

	End Sub
End Class
