﻿Imports Microsoft.VisualBasic

Public Class CBLDeclarationInfo
    Public Property selectedApplicant As CSelectedApplicant
    Public Property HasBankruptForeclosure As String
    Public Property HasDeclaredBankruptcy As String
    Public Property HasChapter13 As String
    Public Property HasJudgment As String
    Public Property HasLawsuitParty As String
    Public Property HasRepossession As String

    Public Property HasOtherObligation As String
    Public Property HasPastDueBills As String
    Public Property HasCoMaker As String
    Public Property HasOtherCreditName As String
    Public Property HasSuitsPending As String
    Public Property HasIncomeDecline As String
    Public Property HasAlimony As String
    Public Property PrimaryName As String
    Public Property CreditorName As String
    Public Property Amount As Double
    Public Property OtherCreditName As String
    Public Property RecipientName As String
    Public Property RecipientAddress As String
    Public Property AlimonyAmount As Double
    Public Property SeperateAmount As Double
    Public Property ChildSupportAmount As Double

End Class
