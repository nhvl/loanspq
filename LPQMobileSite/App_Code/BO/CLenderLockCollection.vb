﻿Imports Microsoft.VisualBasic
Imports LPQMobile.Utils

Public Interface ILenderLockCollection

End Interface

Public Class CDownloadSettingsLenderLockCollection
	Implements ILenderLockCollection

	Public DownloadSettingsLock As Object
	Public XAQuestionsLock As Object
	Public FOMQuestionsLock As Object
	Public LenderServiceConfigInfoLock As Object
	Public ListProductLock As Object

	Public Sub New()
		DownloadSettingsLock = New Object()
		XAQuestionsLock = New Object()
		FOMQuestionsLock = New Object()
		LenderServiceConfigInfoLock = New Object()
		ListProductLock = New Object()
	End Sub
End Class

Public Class CLenderLockCollectionManager(Of TLenderLockCollection As {New, ILenderLockCollection})

	''' <summary>
	''' This global lock is for this particular instance of the manager. It is important
	''' to only use one instance of a manager at a time, for applicable scenarios.
	''' </summary>
	Private _GlobalLock As New Object()

	''' <summary>
	''' A mapping of the lender identification to its collection of locks
	''' </summary>
	Private _LenderCacheLocks As New Dictionary(Of String, TLenderLockCollection)

	''' <summary>
	''' Gets the collection of sync locks for use by CDownloadSettings to synchronize
	''' the retrieval of certain types of cached data. The locks are necessary within the
	''' lender to prevent race conditions dropping the data of a request.
	''' </summary>
	''' <param name="poConfig"></param>
	''' <returns></returns>
	Public Function GetLenderLockCollection(ByVal poConfig As CWebsiteConfig) As TLenderLockCollection
		Dim sKey = poConfig.CacheIDSuffix
		Dim lenderLock As Object = Nothing

		' Get existing lock object
		If _LenderCacheLocks.TryGetValue(sKey, lenderLock) AndAlso lenderLock IsNot Nothing Then
			Return lenderLock
		End If

		' If the lock object does not yet exist, create a new one. However, we don't want to accidentally create
		' two in the event of a race condition. Thus, use a global lock only for creating lender locks.
		SyncLock _GlobalLock
			' It's possible another thread created the lock between the normal execution and this instruction.
			' If so, return that.
			If _LenderCacheLocks.TryGetValue(sKey, lenderLock) AndAlso lenderLock IsNot Nothing Then
				Return lenderLock
			End If

			' Create a new object, add to the dictionary
			lenderLock = New TLenderLockCollection()
			_LenderCacheLocks.Add(sKey, lenderLock)
		End SyncLock

		Return lenderLock
	End Function
End Class
