﻿Imports System.IO
Imports Microsoft.VisualBasic
Imports System.Web.Optimization
Imports Microsoft.Ajax.Utilities
Imports System.Text
Imports System.Configuration
Imports System.Web

Public Class CCustomScriptBundleBuilder
	Implements IBundleBuilder

	Shared Sub AddMapBundle(context As BundleContext, mapVirtualPath As String, content As String)
		Dim bundle As New Bundle(mapVirtualPath)
		bundle.Builder = New CMapBundleBuilder(content)
		context.BundleCollection.Add(bundle)
	End Sub

	Shared Function GenerateErrorResponse(errors As IEnumerable(Of Object)) As String
		Dim sb As New StringBuilder
		sb.Append("/* ")
		sb.Append("An errror occured during minification")
		sb.Append(ControlChars.NewLine & Environment.NewLine)
		For Each err As Object In errors
			sb.Append(err).Append(ControlChars.NewLine & Environment.NewLine)
		Next
		sb.Append("*/" & ControlChars.NewLine & Environment.NewLine)
		Return sb.ToString()
	End Function

	Public Function BuildBundleContent(bundle As Bundle, context As BundleContext, files As IEnumerable(Of BundleFile)) As String Implements IBundleBuilder.BuildBundleContent
		If files Is Nothing Then Return String.Empty
		If context Is Nothing Then Throw New ArgumentNullException("context")
		If bundle Is Nothing Then Throw New ArgumentNullException("bundle")
		Dim concatenationToken As String = CType(Nothing, String)
		If String.IsNullOrEmpty(bundle.ConcatenationToken) = False Then
			concatenationToken = bundle.ConcatenationToken
		End If
		If concatenationToken Is Nothing Or context.EnableInstrumentation Then
			concatenationToken = String.Concat(";", Environment.NewLine)
		End If
		Dim sourceAbsolutePath = VirtualPathUtility.ToAbsolute(context.BundleVirtualPath)
		Dim mapVirtualPath = context.BundleVirtualPath & "map"
		Dim mapAbsolutePath = VirtualPathUtility.ToAbsolute(mapVirtualPath)
		Dim bundleContentBuilder As New StringBuilder
		Dim mapBuilder As New StringBuilder
		Dim minifier As New Microsoft.Ajax.Utilities.Minifier()
		Using bundleContentWriter As New StringWriter(bundleContentBuilder)
			Using mapWriter As New StringWriter(mapBuilder)
				Using sourceMap As New V3SourceMap(mapWriter)
					sourceMap.StartPackage(sourceAbsolutePath, mapAbsolutePath)
					Dim settings As New CodeSettings With {
						.EvalTreatment = EvalTreatment.MakeImmediateSafe, .PreserveImportantComments = False, .SymbolsMap = sourceMap}
					For Each file As BundleFile In files
						minifier.FileName = VirtualPathUtility.ToAbsolute(file.IncludedVirtualPath)
						Dim input As String = file.ApplyTransforms()
						Dim minifiedFile = minifier.MinifyJavaScript(input, settings)
						bundleContentWriter.Write(minifiedFile)
						bundleContentWriter.Write(concatenationToken)
					Next
				End Using
			End Using
		End Using
		If minifier.ErrorList.Any() Then
			Return GenerateErrorResponse(CType(minifier.ErrorList, IEnumerable(Of Object)))
		End If
		Dim mapFile = mapBuilder.ToString()
		AddMapBundle(context, mapVirtualPath, mapFile)

		'TODO: For security reason, only allow to show source map if we need to reproduce error on production
		If ConfigurationManager.AppSettings.Get("UseSourceMap") = "Y" Then 'default not to use source map
			bundleContentBuilder.Append(String.Format(ControlChars.NewLine & Environment.NewLine & "//# sourceMappingURL={0}", mapAbsolutePath))
		End If

		Dim output = bundleContentBuilder.ToString()
		Return output
	End Function
End Class
