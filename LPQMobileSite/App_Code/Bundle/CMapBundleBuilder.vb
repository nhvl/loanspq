﻿
Imports System.Web.Optimization

Public Class CMapBundleBuilder
	Implements IBundleBuilder
	Private bundleContent As String
	Sub New(_bundleContent As String)
		Me.bundleContent = _bundleContent
	End Sub

	Public Function BuildBundleContent(ByVal bundle As Bundle, ByVal context As BundleContext, ByVal files As IEnumerable(Of BundleFile)) As String Implements IBundleBuilder.BuildBundleContent
		Return Me.bundleContent
	End Function
End Class
