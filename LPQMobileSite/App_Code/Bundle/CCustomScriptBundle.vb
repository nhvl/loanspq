﻿Imports Microsoft.VisualBasic
Imports System.Web.Optimization

Public Class CCustomScriptBundle
	Inherits Bundle

	Public Sub New(virtualPath As String)
		Me.New(virtualPath, CType(Nothing, String))
	End Sub

	Public Sub New(virtualPath As String, cdnPath As String)
		MyBase.New(virtualPath, cdnPath, New IBundleTransform() {})
		Builder = New CCustomScriptBundleBuilder()
		ConcatenationToken = String.Concat(";", Environment.NewLine)
	End Sub
End Class
