﻿Imports Microsoft.VisualBasic
Imports System.Web.Optimization
Imports System.IO
Imports Microsoft.Ajax.Utilities
Imports System.Text
Imports System.Text.RegularExpressions
Imports System.Configuration
Imports System.Web

Public Class CCustomStyleBundleBuilder
	Implements IBundleBuilder

	Shared Sub AddMapBundle(context As BundleContext, mapVirtualPath As String, content As String)
		Dim bundle As New Bundle(mapVirtualPath)
		bundle.Builder = New CMapBundleBuilder(content)
		context.BundleCollection.Add(bundle)
	End Sub

	Shared Function GenerateErrorResponse(errors As IEnumerable(Of Object)) As String
		Dim sb As New StringBuilder
		sb.Append("/* ")
		sb.Append("An errror occured during minification")
		sb.Append(ControlChars.NewLine & Environment.NewLine)
		For Each err As Object In errors
			sb.Append(err).Append(ControlChars.NewLine & Environment.NewLine)
		Next
		sb.Append("*/" & ControlChars.NewLine & Environment.NewLine)
		Return sb.ToString()
	End Function

	Public Function BuildBundleContent(bundle As Bundle, context As BundleContext, files As IEnumerable(Of BundleFile)) As String Implements IBundleBuilder.BuildBundleContent
		If files Is Nothing Then Return String.Empty
		If context Is Nothing Then Throw New ArgumentNullException("context")
		If bundle Is Nothing Then Throw New ArgumentNullException("bundle")
		Dim concatenationToken As String = CType(Nothing, String)
		If String.IsNullOrEmpty(bundle.ConcatenationToken) = False Then
			concatenationToken = bundle.ConcatenationToken
		End If
		If concatenationToken Is Nothing Or context.EnableInstrumentation Then
			concatenationToken = String.Concat(" ", Environment.NewLine)
		End If
		Dim sourceAbsolutePath = VirtualPathUtility.ToAbsolute(context.BundleVirtualPath)
		Dim mapVirtualPath = context.BundleVirtualPath & "map"
		Dim mapAbsolutePath = VirtualPathUtility.ToAbsolute(mapVirtualPath)
		Dim bundleContentBuilder As New StringBuilder
		Dim mapBuilder As New StringBuilder
		Dim minifier As New Microsoft.Ajax.Utilities.Minifier()
		Using bundleContentWriter As New StringWriter(bundleContentBuilder)
			Using mapWriter As New StringWriter(mapBuilder)
				Using sourceMap As New V3SourceMap(mapWriter)
					sourceMap.StartPackage(sourceAbsolutePath, mapAbsolutePath)
					Dim settings As New CssSettings With {
						.OutputMode = OutputMode.SingleLine}
					Dim codeSettings As New CodeSettings With {
						.EvalTreatment = EvalTreatment.MakeImmediateSafe, .PreserveImportantComments = False, .SymbolsMap = sourceMap}
					For Each file As BundleFile In files
						minifier.FileName = VirtualPathUtility.ToAbsolute(file.IncludedVirtualPath)
						file.Transforms.Add(New CssRewriteUrlTransformIgnoringDataUri())
						Dim input As String = file.ApplyTransforms()
						Dim minifiedFile = minifier.MinifyStyleSheet(input, settings, codeSettings)
						bundleContentWriter.Write(minifiedFile)
						bundleContentWriter.Write(concatenationToken)
					Next
				End Using
			End Using
		End Using
		If minifier.ErrorList.Any() Then
			Return GenerateErrorResponse(CType(minifier.ErrorList, IEnumerable(Of Object)))
		End If
		Dim mapFile = mapBuilder.ToString()
		AddMapBundle(context, mapVirtualPath, mapFile)
		'TODO: For security reason, only allow to show source map if we need to reproduce error on production
		If ConfigurationManager.AppSettings.Get("UseSourceMap") = "Y" Then
			bundleContentBuilder.Append(String.Format(ControlChars.NewLine & Environment.NewLine & "/*# sourceMappingURL={0} */", mapAbsolutePath))
		End If
		Dim output = bundleContentBuilder.ToString()
		Return output
	End Function
End Class

''' <summary>
''' Pulled from https://gist.github.com/janv8000/fa69b2ab6886f635e3df with minor modifications
''' </summary>
Public Class CssRewriteUrlTransformIgnoringDataUri
	Implements IItemTransform

	Friend Shared Function RebaseUrlToAbsolute(ByVal baseUrl As String, ByVal url As String) As String
		If String.IsNullOrWhiteSpace(url) OrElse String.IsNullOrWhiteSpace(baseUrl) OrElse url.StartsWith("/", StringComparison.OrdinalIgnoreCase) Then Return url
		If Not baseUrl.EndsWith("/", StringComparison.OrdinalIgnoreCase) Then baseUrl = baseUrl & "/"
		Return VirtualPathUtility.ToAbsolute(baseUrl & url)
	End Function

	Friend Shared Function ConvertUrlsToAbsolute(ByVal baseUrl As String, ByVal content As String) As String
		If String.IsNullOrWhiteSpace(content) Then
			Return content
		End If

		Return New Regex("url\(['""]?(?<url>[^)]+?)['""]?\)").Replace(content, Function(match)
																				   Dim format = match.Groups("url").Value

																				   If format.StartsWith("data:image", StringComparison.CurrentCultureIgnoreCase) Then
																					   Return "url(" & format & ")"
																				   End If

																				   Return "url(" & RebaseUrlToAbsolute(baseUrl, format) & ")"
																			   End Function)
	End Function

	Public Function Process(includedVirtualPath As String, input As String) As String Implements IItemTransform.Process
		If includedVirtualPath Is Nothing Then
			Throw New ArgumentNullException("includedVirtualPath")
		End If

		Return ConvertUrlsToAbsolute(VirtualPathUtility.GetDirectory(includedVirtualPath.Substring(1)), input)
	End Function
End Class