﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="GenericErrorPage.aspx.vb" Inherits="GenericErrorPage" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Generic Error Page</title>
</head>
<body>
    <form id="form1" runat="server">
    
			<div>
					<p><font style="FONT: 8pt/11pt verdana; COLOR: #000000">
			There is a problem with the page you are trying to reach and it cannot be displayed.			
									<br/>
									<br/>
									You may want to close your browser and retry again.&nbsp;&nbsp;

            </font></p>
		  </div>
  
    </form>
</body>
</html>
