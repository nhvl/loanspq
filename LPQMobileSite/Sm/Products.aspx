﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="Products.aspx.vb" Inherits="Sm_Products" MasterPageFile="SiteManager.master" %>

<asp:Content runat="server" ContentPlaceHolderID="plBody">
	<div id="divProductsPage">
		<h1 class="page-header">Product</h1>
		<p>Configure products that are available in your Application Portal</p>
		<section>
			<ul class="nav nav-tabs" id="mainTabs" role="tablist">
				<%If CurrentModule = SmSettings.ModuleName.XA Then%>
				<li role="presentation" class="active"><a href="#" aria-controls="tabProd" role="tab">XA</a></li>
				<%Else %>
				<li role="presentation" <%=IIf(EnableXA, "", "class='hidden'")%>><a href="<%=BuildUrl("/sm/products.aspx", New KeyValuePair(Of String, String)("ptab", "xa"))%>" role="tab">XA</a></li>
				<%End If%>
				<%If CurrentModule = SmSettings.ModuleName.CC_COMBO Then%>
				<li role="presentation" class="active"><a href="#" aria-controls="tabCQ" role="tab">CC Combo</a></li>
				<%Else %>
				<li role="presentation" <%=IIf(CheckEnabledModule(SmSettings.ModuleName.CC_COMBO), "", "class='hidden'")%>><a href="<%=BuildUrl("/sm/products.aspx", New KeyValuePair(Of String, String)("ptab", "cc"))%>" role="tab">CC Combo</a></li>
				<%End If%>
				<%If CurrentModule = SmSettings.ModuleName.HE_COMBO Then%>
				<li role="presentation" class="active"><a href="#" aria-controls="tabCQ" role="tab">HE Combo</a></li>
				<%Else %>
				<li role="presentation" <%=IIf(CheckEnabledModule(SmSettings.ModuleName.HE_COMBO), "", "class='hidden'")%>><a href="<%=BuildUrl("/sm/products.aspx", New KeyValuePair(Of String, String)("ptab", "he"))%>" role="tab">HE Combo</a></li>
				<%End If%>
				<%If CurrentModule = SmSettings.ModuleName.PL_COMBO Then%>
				<li role="presentation" class="active"><a href="#" aria-controls="tabCQ" role="tab">PL Combo</a></li>
				<%Else %>
				<li role="presentation" <%=IIf(CheckEnabledModule(SmSettings.ModuleName.PL_COMBO), "", "class='hidden'")%>><a href="<%=BuildUrl("/sm/products.aspx", New KeyValuePair(Of String, String)("ptab", "pl"))%>" role="tab">PL Combo</a></li>
				<%End If%>
				<%If CurrentModule = SmSettings.ModuleName.VL_COMBO Then%>
				<li role="presentation" class="active"><a href="#" aria-controls="tabCQ" role="tab">VL Combo</a></li>
				<%Else %>
				<li role="presentation" <%=IIf(CheckEnabledModule(SmSettings.ModuleName.VL_COMBO), "", "class='hidden'")%>><a href="<%=BuildUrl("/sm/products.aspx", New KeyValuePair(Of String, String)("ptab", "vl"))%>" role="tab">VL combo</a></li>
				<%End If%>

			</ul>
			<div class="tab-content">
				<div role="tabpanel" class="tab-pane active" id="tabProd">
					<h2 class="section-title"><%=TabTitle%></h2>
					<p>Product names and applicable settings are configured in the in-branch settings. Please make the update in-branch prior to updating here. If only one product is selected, the product selection section will be hidden in your Application Portal.</p>
					<div class="checkbox-bypass">
						<div class="checkbox checkbox-success checkbox-inline" data-toggle="tooltip" data-placement="bottom" title="This is the bypass mode so all products from in-branch that are visible to consumer will show up on Application Portal.">
							<input type="checkbox" id="chkProductShowAll" data-command="showAll" <%=BinShowAllCheckbox(EnabledProducts)%>/>
							<label for="chkProductShowAll"> Show All (all products from in-branch are visible) </label>
						</div>
					</div>
					<%If ProductList IsNot Nothing AndAlso ProductList.Any() Then
							For Each item As KeyValuePair(Of String, List(Of CProduct)) In ProductList.OrderBy(Function(c) c.Key)
								If item.Value IsNot Nothing AndAlso item.Value.Any() Then%>
						<div class="checkbox-group">
							<p><i class="fa fa-caret-right" aria-hidden="true"></i><span><%=item.Key%></span></p>
							<%For Each product As CProduct In item.Value%>
							<div class="checkbox-btn-wrapper">
								<span><%=product.AccountName%></span>
								<label class="toggle-btn pull-right" data-on="ON" data-off="OFF">
									<input type="checkbox" data-text="<%=product.AccountName%>" value="<%=product.ProductCode%>" <%=BindCheckbox(product.ProductCode, EnabledProducts)%>/>
									<span class="button-checkbox"></span>
								</label>
							</div>			
							<%Next%>
						</div>
					<%End If
					Next
						  End If%>
				</div>
			</div>
		</section>
	</div>
</asp:Content>
<asp:Content runat="server" ContentPlaceHolderID="plScripts">
	<script type="text/javascript">
		$(function () {
			$("input:checkbox", "#divProductsPage").on("change", function () {
				master.FACTORY.documentChanged(this);
			});
			$("input:checkbox[data-command='showAll']", "#divProductsPage").on("change", function () {
				var $self = $(this);
				if ($self.is(":checked")) {
					$("label.toggle-btn>input:checkbox", $self.closest("div.tab-pane")).each(function (idx, ele) {
						$(ele).prop("checked", false);
					});
				}
				if ($self.is(":checked") == false) {  //disable uncheck  via user click
					$self.prop("checked", true);
				}
			});
			$("label.toggle-btn>input:checkbox", "#divProductsPage").on("change", function () {
				var $self = $(this);
				if ($self.is(":checked")) {
					$self.closest("div.tab-pane").find("input:checkbox[data-command='showAll']").prop("checked", false);
				}
			});
		});
		(function (master, $, undefined) {
			master.FACTORY.SaveDraft = function (revisionId) {
				var dataObj = collectSubmitData();
				dataObj.command = 'saveProductConfigure';
				dataObj.revisionId = revisionId;
				$.ajax({
					url: "/sm/smhandler.aspx",
					async: true,
					cache: false,
					type: 'POST',
					dataType: 'html',
					data: dataObj,
					success: function (responseText) {
						var response = $.parseJSON(responseText);
						if (response.IsSuccess) {
							master.FACTORY.onSaveDraftSuccess();
						} else {
							_COMMON.noty("error", "Error", 500);
						}
					}
				});
			};
			master.FACTORY.Preview = function (revisionId) {
				var dataObj = collectSubmitData();
				dataObj.command = 'previewProductConfigure';
				dataObj.revisionId = revisionId;
				$.ajax({
					url: "/sm/smhandler.aspx",
					async: true,
					cache: false,
					type: 'POST',
					dataType: 'html',
					data: dataObj,
					success: function (responseText) {
						var response = $.parseJSON(responseText);
						if (response.IsSuccess) {
							master.FACTORY.onPreviewSuccess();
							_COMMON.openInNewTab(response.Info.previewurl);
						} else {
							_COMMON.noty("error", "Error", 500);
						}
					}
				});
			};
			master.FACTORY.Publish = function (comment, publishAll) {
				var dataObj = collectSubmitData();
				dataObj.command = 'publishProductConfigure';
				dataObj.comment = comment;
				dataObj.publishAll = publishAll;
				$.ajax({
					url: "/sm/smhandler.aspx",
					async: true,
					cache: false,
					type: 'POST',
					dataType: 'html',
					data: dataObj,
					success: function (responseText) {
						var response = $.parseJSON(responseText);
						if (response.IsSuccess) {
							master.FACTORY.onPublishSuccess();
						} else {
							_COMMON.noty("error", "Error", 500);
						}
					}
				});
			};
		}(window.master = window.master || {}, jQuery));

		function collectSubmitData() {
			var result = {};
			result.lenderconfigid = '<%=LenderConfigID%>';
			result.tab = '<%=CurrentTab%>';
			result.selectedProducts = JSON.stringify(_COMMON.collectCheckboxGroupData($("#tabProd")));
			return result;
		}
	</script>
</asp:Content>
<asp:Content runat="server" ContentPlaceHolderID="plActionButtons"></asp:Content>