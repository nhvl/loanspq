﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="EquifaxInstaTouchReport.aspx.vb" Inherits="Sm_EquifaxInstaTouchReport" MasterPageFile="SiteManager.master" %>
<asp:Content runat="server" ContentPlaceHolderID="plBody">
	<div>
		<h1 class="page-header">Equifax InstaTouch</h1>
		<p>This report will display usage of this feature and will generate as a CSV. Specify the date range for the report.</p>
		<div class="row">
			<div class="col-sm-4 form-group">
				<label for="txtToDate">Start Date</label>
				<div class='input-group date' id="txtFromDate">
                    <input type='text' class="form-control" placeholder="From Date"/>
                    <span class="input-group-addon">
                    <span class="glyphicon glyphicon-calendar"></span>
                    </span>
                </div>
			</div>
			<div class="col-sm-4 form-group">
				<label for="txtToDate">End Date</label>
				<div class='input-group date' id="txtToDate">
                    <input type='text' class="form-control" placeholder="To Date"/>
                    <span class="input-group-addon">
                    <span class="glyphicon glyphicon-calendar"></span>
                    </span>
                </div>
			</div>
		</div>
		<div class="row">
			<div class="col-sm-12">
				<button id="btnRunReport" class="btn btn-primary mb10 mr5">Generate Report</button>
			</div>
		</div>
	</div>
</asp:Content>
<asp:Content runat="server" ContentPlaceHolderID="plScripts">
	<script type="text/javascript">
		$(function () {
			$('#txtFromDate, #txtToDate').datepicker().on("hide", function (e) {
				$(e.target).trigger("change");
			});
			registerDataValidator();
			$("#btnRunReport").on("click", function() {
				if (!$.smValidate("ValidateData")) return false;
				var downloadExcelPage = window.open('/sm/loading.aspx');
				$.ajax({
					url: "/sm/smhandler.aspx",
					async: true,
					cache: false,
					type: 'POST',
					dataType: 'json',
					data: { start: $("#txtFromDate>input").val(), end: $("#txtToDate>input").val(), lenderRef: '<%=LenderRef%>', lenderID: '<%=LenderId%>', command: 'runinstatouchreport' },
					success: function (response) {
						if (response.IsSuccess) {
							downloadExcelPage.location.href = response.Message;
						} else {
							downloadExcelPage.close();
							_COMMON.noty("error", "Error", 500);
						}
					}
				});
			});
		});
		function registerDataValidator() {
			$.smValidate.removeValidationGroup("ValidateData");
			$("#txtFromDate").observer({
				validators: [
					function (partial) {
						var $self = $(this).find(">input");
						var startDate = moment($self.val(), "MM-DD-YYYY");

						if (!_COMMON.ValidateDate($self.val())) {
							return "Please enter a valid date";
						} else if (!startDate.isValid()) {
							return "Please enter a valid date";
						} else if (!startDate.isBefore(moment())) {
							return "Start date must be before current date";
						}
						var endDate = moment($("#txtToDate>input").val(), "MM-DD-YYYY");
						if (_COMMON.ValidateDate($("#txtToDate>input").val()) && endDate.isValid() && endDate.isBefore(startDate)) {
							return "Start date must be before end date";
						}
						return "";
					}
				],
				validateOnBlur: true,
				group: "ValidateData"
			});
			$("#txtToDate").observer({
				validators: [
					function (partial) {
						var $self = $(this).find(">input");
						var endDate = moment($self.val(), "MM-DD-YYYY");
						if (!_COMMON.ValidateDate($self.val())) {
							return "Please enter a valid date";
						} else if (!endDate.isValid()) {
							return "Please enter a valid date";
						//} else if (!endDate.isBefore(moment())) {
						//	return "Start date must be before current date";
						}
						var startDate = moment($("#txtFromDate>input").val(), "MM-DD-YYYY");
						if (_COMMON.ValidateDate($("#txtFromDate>input").val()) && startDate.isValid() && endDate.isBefore(startDate)) {
							return "End date must be after start date";
						}
						return "";
					}
				],
				validateOnBlur: true,
				group: "ValidateData"
			});
		}
	</script>
</asp:Content>
<asp:Content runat="server" ContentPlaceHolderID="plActionButtons"></asp:Content>