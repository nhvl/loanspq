﻿Imports Sm
Imports System.Xml

Partial Class Sm_LoansSettings
	Inherits SmApmBasePage

	Protected Property UploadDocEnable As Boolean = True
	Protected Property UploadDocMsg As String


	Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
		If Not EnableCC AndAlso Not EnableHE AndAlso Not EnablePL AndAlso Not EnableVL Then
			Response.Redirect(BuildUrl("/sm/generalsettings.aspx"), True)
		End If
		If Not Page.IsPostBack Then
			Dim smMasterPage = CType(Me.Master, Sm_SiteManager)
			smMasterPage.LenderConfigXml = LenderConfigXml
			smMasterPage.RevisionID = LenderConfig.RevisionID
			smMasterPage.LastModifiedBy = LenderConfig.LastModifiedByUserFullName
			smMasterPage.LastModifiedByUserID = LenderConfig.LastModifiedByUserID
			smMasterPage.SelectedMainMenuItem = "config"
			smMasterPage.SelectedSubMenuItem = "settings"
			smMasterPage.CurrentNodeGroup = SmSettings.NodeGroup.AllLoansSettings
			smMasterPage.BreadCrumb = "Configure / Settings / All loans"
			Dim loanUploadDocEnableNode As XmlNode = GetNode(UPLOAD_DOC_ENABLE())
			If loanUploadDocEnableNode IsNot Nothing Then
				UploadDocEnable = Not loanUploadDocEnableNode.Value.ToUpper().Equals("N")
			End If

			Dim loanUploadDocNode As XmlNode = GetNode(UPLOAD_DOC_MESSAGE())
			If loanUploadDocNode IsNot Nothing Then
				UploadDocMsg = loanUploadDocNode.InnerText.Trim()
			End If
		End If
	End Sub

End Class

