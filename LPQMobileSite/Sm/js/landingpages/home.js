//------------- Home page.js -------------//
$(document).ready(function() {
    //Customers carousel
    $("#customers-carousel").owlCarousel({
        items: 4,
        lazyLoad : true,
        navigation : false,
        slideSpeed : 500,
        paginationSpeed : 1000,
        autoPlay: 7000,
        stopOnHover: true
    }); 

	$('.js-slick').slick({
		autoplay: true,
		autoplaySpeed: 2000,
		dots: true,
		draggable: true,
		fade: true,
		speed: 1000
	});

	$('.js-slick').on('beforeChange', function(event, slick, currentSlide, nextSlide) {
		$(slick.$slides).removeClass('is-animating');
	});

	$('.js-slick').on('afterChange', function(event, slick, currentSlide, nextSlide) {
		$(slick.$slides.get(currentSlide)).addClass('is-animating');
	});

    createInlineSvg();
});

function openNav() {
	$('#fly_menu').css('width', '300px');
	$('#fly_menu a').addClass('menu-visible');
}

function closeNav() {				
	$('#fly_menu').css('width', '0');
	$('#fly_menu a').removeClass('menu-visible');
}

function createInlineSvg() {
    $('img.inline-svg').each(function () {
        var $img = $(this);
        var imgID = $img.attr('id');
        var imgClass = $img.attr('class');
        var imgURL = $img.attr('src');

        $.get(imgURL, function (data) {
            // Get the SVG tag, ignore the rest
            var $svg = $(data).find('svg');

            // Add replaced image's ID to the new SVG
            if (typeof imgID !== 'undefined') {
                $svg = $svg.attr('id', imgID);
            }
            // Add replaced image's classes to the new SVG
            if (typeof imgClass !== 'undefined') {
                $svg = $svg.attr('class', imgClass + ' replaced-svg');
            }

            // Remove any invalid XML tags as per http://validator.w3.org
            $svg = $svg.removeAttr('xmlns:a');

            // Check if the viewport is set, else we gonna set it if we can.
            if (!$svg.attr('viewBox') && $svg.attr('height') && $svg.attr('width')) {
                $svg.attr('viewBox', '0 0 ' + $svg.attr('height') + ' ' + $svg.attr('width'));
            }

            // Replace image with new SVG
            $img.replaceWith($svg);

        }, 'xml');
    });
}