﻿//function initNewUploadUserAvatar() {
//	$("#divAvatar").dropzone({
//		url: "/sm/smhandler.aspx",
//		dictDefaultMessage: "<p>Drop image file here or click to select image from your computer</p><p>Compatible file types: JPG, PNG. Recommended approximate dimension: 200 x 200.</p><p>Recommended file size: 10KB.</p>",
//		maxFilesize: 2,
//		acceptedFiles: "image/png,image/jpeg",
//		params: { "command": "uploadavatar" },
//		maxFiles: 1,
//		addRemoveLinks: true,
//		dictRemoveFile: "Remove",
//		maxfilesexceeded: function (file) {
//			this.removeAllFiles();
//			this.addFile(file);
//		},
//		thumbnailWidth: 200,
//		thumbnailHeight: null,
//		init: function () {
//			this.on("addedfile", function (file) {

//			});
//			this.on("success", function (file, response) {
//				$("#hdAvatar").val(response);
//				$.smValidate.hideValidation($("#hdAvatar").closest(".form-group"));
//			});
//			this.on("removedfile", function (file) {
//				var fileurl = file.xhr.responseText;
//				$.ajax({
//					url: "/sm/smhandler.aspx",
//					async: true,
//					cache: false,
//					type: 'POST',
//					dataType: 'html',
//					data: { command: 'deleteavatar', fileurl: fileurl },
//					success: function (responseText) {
//						$("#hdAvatar").val("");
//					}
//				});
//			});
//		}
//	});
//}

var currentAvatarFile = null;
function initEditUploadUserAvatar() {
	$("#divAvatar").dropzone({
		url: "/sm/smhandler.aspx",
		dictDefaultMessage: "<p>Drop image file here or click to select image from your computer</p><p>Compatible file types: JPG, PNG. Recommended approximate dimension: 200 x 200.</p><p>Recommended file size: 10KB.</p>",
		maxFilesize: 2,
		acceptedFiles: "image/png,image/jpeg",
		params: { "command": "uploadavatar" },
		maxFiles: 1,
		addRemoveLinks: true,
		dictRemoveFile: "Remove",
		maxfilesexceeded: function (file) {
			this.removeAllFiles();
			this.addFile(file);
		},
		thumbnailWidth: 200,
		thumbnailHeight: null,
		init: function () {
			var $hdLogo = $("#hdAvatar");
			currentAvatarFile = null;
			if ($hdLogo.val() != "") {
				var mockFile = { name: $hdLogo.data("file-name"), size: $hdLogo.data("file-size") /*, type: 'image/png'*/, url: $("#hdAvatar").val() };
				this.options.addedfile.call(this, mockFile);
				this.options.thumbnail.call(this, mockFile, $("#hdAvatar").val());
				mockFile.previewElement.classList.add('dz-success');
				mockFile.previewElement.classList.add('dz-complete');
				$(mockFile.previewElement).find("img").attr("width", 200);
				currentAvatarFile = mockFile;
			}
			this.on("addedfile", function (file) {
				if (currentAvatarFile) {
					this.removeFile(currentAvatarFile);
				}
				currentAvatarFile = file;
			});
			this.on("success", function (file, response) {
				$("#hdAvatar").val(response);
				$.smValidate.hideValidation($("#hdAvatar").closest(".form-group"));
				currentAvatarFile = file;
			});
			this.on("removedfile", function (file) {
				currentAvatarFile = null;
				if (file.xhr) {
					$.ajax({
						url: "/sm/smhandler.aspx",
						async: true,
						cache: false,
						type: 'POST',
						dataType: 'html',
						data: { command: 'deleteavatar', fileurl: file.xhr.responseText },
						success: function(responseText) {
							$("#hdAvatar").val("");
						}
					});
				} else {
					$("#hdAvatar").val("");
				}

			});
		}
	});
}









