﻿CKEDITOR.dialog.add("dynamicKeysDialog",function(editor) {
	return {
		title: "Dynamic Keys",
		width: 600,
		height: 400,
		buttons: [CKEDITOR.dialog.cancelButton],
		contents: [
			{
				elements: [{
					type: "html",
					html: "This dialog lists all dynamic keys with search and filter feature. This dialog will be implemented when the number of dynamic keys grow to more than 20."
				}]
			}
		]
	};
});