/**
 * @license Copyright (c) 2003-2019, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see https://ckeditor.com/legal/ckeditor-oss-license
 */

CKEDITOR.stylesSet.add('apm-styles', [
	{ name: "Sub Section Header", element: "div", attributes: { 'class': 'sub_section_header header_theme' } },
	{ name: "Section Title", element: "div", attributes: { 'class': 'section-title' } },
	{ name: "Check Mark", element: "div", attributes: { 'class': 'CheckMark MemLabel-Indent' } }
]);


CKEDITOR.editorConfig = function( config ) {
	// Define changes to default configuration here.
	// For complete reference see:
	// http://docs.ckeditor.com/#!/api/CKEDITOR.config

	// The toolbar groups arrangement, optimized for two toolbar rows.
	//config.toolbarGroups = [
	//	{ name: 'clipboard',   groups: [ 'clipboard', 'undo' ] },
	//	{ name: 'editing',     groups: [ 'find', 'selection', 'spellchecker' ] },
	//	{ name: 'links' },
	//	{ name: 'insert' },
	//	{ name: 'forms' },
	//	{ name: 'tools' },
	//	{ name: 'document',	   groups: [ 'mode', 'document', 'doctools' ] },
	//	{ name: 'others' },
	//	'/',
	//	{ name: 'basicstyles', groups: [ 'basicstyles', 'cleanup' ] },
	//	{ name: 'paragraph',   groups: [ 'list', 'indent', 'blocks', 'align', 'bidi' ] },
	//	{ name: 'styles' },
	//	{ name: 'colors' },
	//	{ name: 'about' }
	//];
	config.toolbar = [
		{ name: "links", items: ["Link", "Unlink", "Anchor"] },
		{ name: "inserts", items: ["Image", "Table", "DynamicKeys"] },
	{ name: "paragraph", items: ["NumberedList", "BulletedList", "-", "Outdent", "Indent", "Blockquote", "CreateDiv"] },
		{name: "editing", items: ["Undo", "Redo", "SelectAll", "RemoveFormat", "Sourcedialog"]},
		"/",
		{ name: "basicstyles", items: ["Bold", "Italic", "Underline", "Strike", "-", "Subscript", "Superscript"] },
		{ name: "styles", items: ["Styles", "Format", "Font", "FontSize"] }
	];

	// Remove some buttons provided by the standard plugins, which are
	// not needed in the Standard(s) toolbar.
	//config.removeButtons = 'Underline,Subscript,Superscript';
	config.removeButtons = 'Cut,Copy,Paste,PasteText,PasteFromWord,SpellChecker,Scayt,HorizontalRule,SpecialChar,About';

	// Set the most common block elements.
	config.format_tags = 'p;h1;h2;h3;pre;div';
	config.extraAllowedContent = '*(*);*{*}';
	// Simplify the dialog windows.
	config.removeDialogTabs = 'image:advanced;link:advanced';

	config.extraPlugins = "dynamickeys";
	config.stylesSet = 'apm-styles';
	config.enterMode = CKEDITOR.ENTER_BR;
	config.removePlugins = 'magicline';
};
CKEDITOR.on("dialogDefinition", function(e) {
	e.data.definition.resizable = CKEDITOR.DIALOG_RESIZE_NONE;
	var dialog = e.data.definition.dialog;
	if (dialog.getName() == "dynamicKeysDialog") {
		dialog.on("show", function(e) {
			var bodys = document.querySelectorAll(".cke_dialog_contents_body");
			if (bodys.length > 0) {
				bodys[bodys.length - 1].style.padding = "20px;";
			}
			var children = document.querySelectorAll(".ck_dialog_ui_vbox_child");
			if (children.length > 0) {
				children[children.length - 1].style.padding = "10px";
			}
			e.removeListener();
		});
	}
});
