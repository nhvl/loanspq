﻿// ensure there is only 1 instance of noty through the application
var _NOTY = null;
var _COMMON_SORT_COLUMN = '';
var _COMMON_SORT_TYPE = 'sorting_desc';
var _COMMON_PAGE_INDEX = 1;
var _COMMON_PAGE_SIZE = 5;
var _COMMON_IS_SORTING = false;
var _COMMON_CURRENT_SORT_TYPE = '';
var _COMMON_FILTER_DATE_TIME_SPAN = 30;//Days (AuditLogs.ascx)
var _COMMON_FILTER_MAX_TIME_SPAN = 1;//Year (AuditLogs.ascx)

var _COMMON = {
	noty: function(type, text, timeout) {
		if (_NOTY != null) {
			_NOTY.closeCleanUp();
		}

		var noTimeOut = typeof timeout === 'undefined' || timeout == null;
		_NOTY = noty({
			text: text,
			type: type, // "alert", "success", "error", "warning", "information", "confirm"
			layout: 'topRight', //top, topLeft, topCenter, topRight, center, centerLeft, centerRight, bottom, bottomLeft, bottomCenter, bottomRight
			container: '.dasdsdasd',
			theme: 'bootstrapTheme',
			killer: false,
			timeout: '800', //noTimeOut ? false : timeout,
			animation: {
				open: 'animated fadeInDown', // flipInX
				close: 'animated fadeOutUp' // flipOutX                                   
			}
		});
		_NOTY.setTimeout(2000);
	},
	insertAtCursor: function(field, value) {
		//support TEXTAREA only
		//IE support
		if (document.selection) {
			field.focus();
			sel = document.selection.createRange();
			sel.text = value;
		}
		//MOZILLA and others
		else if (field.selectionStart || field.selectionStart == '0') {
			var startPos = field.selectionStart;
			var endPos = field.selectionEnd;
			field.value = field.value.substring(0, startPos)
				+ value
				+ field.value.substring(endPos, field.value.length);
			field.selectionStart = startPos + value.length;
			field.selectionEnd = startPos + value.length;
		} else {
			field.value += value;
		}
	},
	pasteHtmlAtCaret: function(field, html, selectPastedContent) {
		//use this for div[contenteditable=true]
		if ($(field).is(":focus") === false) $(field).focus();
		var sel, range;
		if (window.getSelection) {
			// IE9 and non-IE
			sel = window.getSelection();
			if (sel.getRangeAt && sel.rangeCount) {
				range = sel.getRangeAt(0);
				range.deleteContents();

				// Range.createContextualFragment() would be useful here but is
				// only relatively recently standardized and is not supported in
				// some browsers (IE9, for one)
				var el = document.createElement("div");
				el.innerHTML = html;
				var frag = document.createDocumentFragment(), node, lastNode;
				while ((node = el.firstChild)) {
					lastNode = frag.appendChild(node);
				}
				var firstNode = frag.firstChild;
				range.insertNode(frag);

				// Preserve the selection
				if (lastNode) {
					range = range.cloneRange();
					range.setStartAfter(lastNode);
					if (selectPastedContent) {
						range.setStartBefore(firstNode);
					} else {
						range.collapse(true);
					}
					sel.removeAllRanges();
					sel.addRange(range);
				}
			}
		} else if ((sel = document.selection) && sel.type != "Control") {
			// IE < 9
			var originalRange = sel.createRange();
			originalRange.collapse(true);
			sel.createRange().pasteHTML(html);
			if (selectPastedContent) {
				range = sel.createRange();
				range.setEndPoint("StartToStart", originalRange);
				range.select();
			}
		}
	},
	collectDisclosureData: function(container) {
		var $container = $(container);
		var result = [];
		$(".html-editor", $container).each(function(idx, ele) {
			var $ele = $(ele);
			var content = CKEDITOR.instances[$ele.attr("id")].getData();
			if ($.trim(content) !== "") {
				result[idx] = content;
			}
		});
		return result;
	},
	collectCheckboxGroupData: function(container) {
		var $container = $(container);
		var result = [];
		$(".checkbox-btn-wrapper input[type='checkbox']:checked", $container).each(function(idx, ele) {
			var $ele = $(ele);
			var item = {}
			item.text = $ele.closest("div.checkbox-btn-wrapper").find("span").text();
			item.value = $ele.val();
			result.push(item);
		});
		return result;
	},
	collectCreditCardNameCheckboxGroupData: function(container) {
		var $container = $(container);
		var result = [];
		$(".checkbox-btn-wrapper input[type='checkbox']:checked", $container).each(function(idx, ele) {
			var $ele = $(ele);
			var item = {}
			item.credit_card_name = $ele.val();
			item.credit_card_type = $ele.data("card-type");
			item.image_url = $ele.data("image-url");
			result.push(item);
		});
		return result;
	},
	collectTextValueCatCheckboxGroupData: function(container) {
		var $container = $(container);
		var result = [];
		$(".checkbox-btn-wrapper input[type='checkbox']:checked", $container).each(function(idx, ele) {
			var $ele = $(ele);
			var item = {}
			item.text = $ele.data("text");
			item.value = $ele.val();
			item.category = $ele.closest(".checkbox-btn-wrapper").find("select").val();
			result.push(item);
		});
		return result;
	},
	getEditorValue: function(id) {
		return CKEDITOR.instances[id].getData();
	},
	openInNewTab: function(url) {
		var w = window.open(url, "_blank");
		w.focus();
	},
	showDialogSaveClose: function (title, dataId, containerId, htmlContent, getContentUrl, postData, onSaveCallback, onInitCallback, onClosingCallback, size, closable, saveButtonLabel, closeButtonLabel, closeByBackdrop, closeByKeyboard) {
		if (typeof size == "undefined") size = BootstrapDialog.SIZE_NORMAL;
		if (typeof closable == "undefined") closable = true;
		if (typeof closeByBackdrop == "undefined") closable = true;
		if (typeof closeByKeyboard == "undefined") closable = true;
		if (typeof saveButtonLabel == "undefined" || saveButtonLabel == null || saveButtonLabel == "") saveButtonLabel = "Save";
		if (typeof closeButtonLabel == "undefined" || closeButtonLabel == null || closeButtonLabel == "") closeButtonLabel = "Close";
		window.BootstrapDialog.show({
			title: title,
			size: size,
			closable: closable,
			cssClass: (closable ? "closable-dialog" : ""),
			closeByBackdrop: closeByBackdrop,
			closeByKeyboard: closeByKeyboard,
			message: "<div class='row d-block' data-id='" + dataId + "' id='" + containerId + "'></div>",
			buttons: [
				{
					label: saveButtonLabel,
					cssClass: 'btn-primary',
					action: function(dialogItself) {
						if ($.isFunction(onSaveCallback))
							onSaveCallback({ container: $("#" + containerId), dialog: dialogItself });
					}
				},
				{
					label: closeButtonLabel,
					cssClass: 'btn-default',
					action: function(dialogItself) {
						dialogItself.close();
					}
				}
			],
			onshown: function() {
				if (getContentUrl == '' || getContentUrl == 'undefined') {
					$('#' + containerId).append(htmlContent);
					if ($.isFunction(onInitCallback))
						onInitCallback($("#" + containerId));
				} else {
					// prepare data to post
					$.post(getContentUrl, postData, "html").done(function(responseHTML) {
						$('#' + containerId).html(responseHTML);
						if ($.isFunction(onInitCallback))
							onInitCallback($("#" + containerId));
					});
				}

			},
			onhide: function() {
				if ($.isFunction(onClosingCallback))
					onClosingCallback();
			}
		});
	},
	showDialog: function (title, dataId, containerId, htmlContent, getContentUrl, postData, onInitCallback, onClosingCallback, size, closable, hasCloseButton) {
		if (typeof size == "undefined") size = BootstrapDialog.SIZE_NORMAL;
		if (typeof closable == "undefined") closable = true;
		window.BootstrapDialog.show({
			title: title,
			size: size,
			closable: closable,
			message: "<div class='row dialog-wrapper' data-id='" + dataId + "' id='" + containerId + "'></div>",
			buttons: hasCloseButton ? [
				{
					label: 'Close',
					cssClass: 'btn-default',
					action: function(dialogItself) {
						dialogItself.close();
					}
				}
			] : [],
			onshown: function() {
				$(".bootstrap-dialog-close-button").show();
				if (getContentUrl == '' || getContentUrl == 'undefined') {
					$('#' + containerId).html(htmlContent);
					if ($.isFunction(onInitCallback))
						onInitCallback($("#" + containerId));
				} else {
					// prepare data to post
					$.post(getContentUrl, postData, "html").done(function(responseHTML) {
						$('#' + containerId).html(responseHTML);
						if ($.isFunction(onInitCallback))
							onInitCallback($("#" + containerId));
					});
				}

			},
			onhide: function() {
				if ($.isFunction(onClosingCallback))
					onClosingCallback();
			}
		});
	},
	showDialogClose: function (title, dataId, containerId, htmlContent, getContentUrl, postData, onInitCallback, onClosingCallback, size, closable) {
		if (typeof size == "undefined") size = BootstrapDialog.SIZE_NORMAL;
		if (typeof closable == "undefined") closable = true;
		window.BootstrapDialog.show({
			title: title,
			size: size,
			closable: closable,
			message: "<div class='row dialog-wrapper' data-id='" + dataId + "' id='" + containerId + "'></div>",
			buttons: [
				{
					label: 'Close',
					cssClass: 'btn-default',
					action: function(dialogItself) {
						dialogItself.close();
					}
				}
			],
			onshown: function() {
				if (getContentUrl == '' || getContentUrl == 'undefined') {
					$('#' + containerId).append(htmlContent);
					if ($.isFunction(onInitCallback))
						onInitCallback($("#" + containerId));
				} else {
					// prepare data to post
					$.post(getContentUrl, postData, "html").done(function(responseHTML) {
						$('#' + containerId).html(responseHTML);
						if ($.isFunction(onInitCallback))
							onInitCallback($("#" + containerId));
					});
				}

			},
			onhide: function() {
				if ($.isFunction(onClosingCallback))
					onClosingCallback();
			}
		});
	},
	showConfirmDialog: function (htmlMessage, onCallbackYes, onCallbackNo, yesButtonLabel, noButtonLabel) {
		yesButtonLabel = yesButtonLabel || "OK";
		noButtonLabel = noButtonLabel || "Cancel";
		window.BootstrapDialog.confirm({
			title: 'WARNING',
			message: htmlMessage,
			type: BootstrapDialog.TYPE_WARNING, // BootstrapDialog.TYPE_PRIMARY
			closable: false,
			draggable: false,
			btnCancelLabel: noButtonLabel,
			btnOKLabel: yesButtonLabel,
			btnOKClass: 'btn-warning', 
			callback: function (result) {
				if (result) {
					if ($.isFunction(onCallbackYes)) {
						onCallbackYes();
					}
				} else {
					if ($.isFunction(onCallbackNo)) {
						onCallbackNo();
					}
				}
			}
		});
		/*window.BootstrapDialog.confirm(htmlMessage, function(result) {
			if (result) {
				if ($.isFunction(onCallbackYes)) {
					onCallbackYes();
				}
			} else {
				if ($.isFunction(onCallbackNo)) {
					onCallbackNo();
				}
			}
		});*/
	},
	showAlertDialog: function (message, title, callbackFunc) {
		if (typeof title == "undefined") {
			title = 'WARNING';
		}
			
		window.BootstrapDialog.alert({
			title: title,
			message: message,
			type: BootstrapDialog.TYPE_WARNING,
			closable: false,
			draggable: true,
			callback: function (result) {
				if (typeof callbackFunc == "function" && $.isFunction(callbackFunc)) {
					callbackFunc();
				}
			}
		});
	},
	showSuccessDialog: function (message, title) {
		if (typeof title == "undefined") {
			title = 'SUCCESS';
		}

		window.BootstrapDialog.alert({
			title: title,
			message: message,
			type: BootstrapDialog.TYPE_SUCCESS,
			closable: false,
			draggable: true
		});
	},
	showInfoDialog: function (message, title) {
		if (typeof title == "undefined") {
			title = 'INFO';
		}

		window.BootstrapDialog.alert({
			title: title,
			message: message,
			type: BootstrapDialog.TYPE_INFO,
			closable: false,
			draggable: true
		});
	},
	showWarningDialog: function (message, yesLabel, noLabel, onCallbackYes, onCallbackNo) {
		window.BootstrapDialog.confirm({
			title: 'WARNING',
			message: message,
			type: BootstrapDialog.TYPE_WARNING,
			closable: false,
			draggable: true,
			btnCancelLabel: noLabel,
			btnOKLabel: yesLabel,
			btnOKClass: 'btn-warning',
			callback: function (result) {
				console.log(result);
				if (result) {
					if ($.isFunction(onCallbackYes)) {
						onCallbackYes();
					}
				} else {
					if ($.isFunction(onCallbackNo)) {
						onCallbackNo();
					}
				}
			}
		});
	},
	ValidatePhone: function (phone) {
		if ($.trim(phone) == '')
			return false;

		var regexObj = /^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$/;
		if (!phone.match(regexObj))
			return false;
		return true;
	},
	ValidateEmail: function (email) {
		var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
		return re.test(email);
	},
	ValidateDate: function (date) {
		// Original JavaScript code by Chirp Internet: www.chirp.com.au
		var minYear = 1902;
		// The original code limits the maxYear to the current year.
		// This will cause future dates like expiration dates to fail.
		// I pushed the limit 20 years into the future.
		var maxYear = (new Date()).getFullYear() + 100; //add 100 instead 20
		date = date.toString();
		if ($.trim(date) == '') return false;
		//make sure date has format mm/dd/yyyy
		if (date.length == 8 && date.indexOf('/') == -1) {
			var getMonth = date.substring(0, 2);
			var getDay = date.substring(2, 4);
			var getYear = date.substring(4, 8);
			//add forward slash to the date
			date = getMonth + "/" + getDay + "/" + getYear;
		}
		if (date != '') {
			if (regs = date.match(/^(\d{1,2})\/(\d{1,2})\/(\d{4})$/)) {
				if (regs[2] < 1 || regs[2] > 31) {
					return false;
				} else if (regs[1] < 1 || regs[1] > 12) {
					return false;
				} else if (regs[3] < minYear || regs[3] > maxYear) {
					return false;
				}
			} else {
				return false;
			}
		}
		return true;
	},
	toJSON: function (obj) {
		return window.JSON ? JSON.stringify(obj) : $.toJSON(obj);
	},
	validatePassword: function(password, userName) {
		if (password.length < 8) return "Password must have at least 8 characters.";
		if (/[0-9]{1,}/.test(password) == false) return "Password must contain at least 1 digit.";
		if (/[a-zA-Z]{1,}/.test(password) == false) return "Password must contain at least 1 letter.";
		if (/[\*&'"<>`]/.test(password) == true) return "Password should not contain special characters * & ' \" < >";
		if (/[0-9]{1,}/.test(password) == false) return "Password must have at least 8 characters.";
		if (typeof userName == "string" && password.toLowerCase().indexOf(userName.toLowerCase()) != -1) return "Password should not contain user name.";
		return "";

	},
	HtmlEncode: function(value) {
		//create a in-memory div, set it's inner text(which jQuery automatically encodes)
		//then grab the encoded contents back out.  The div never exists on the page.
		if ((value != '' && value != undefined)) {
			return $('<div/>').text(value).html();
		}
		return value;
	},
	HtmlDecode: function (value) {
		return $('<div/>').html(value).text();
	}
};
_COMMON.Grid = (function () {
	var getDataUrl = '';
	var targetObject;
	var gridObject;
	var afterLoad;
	var postData;

	function Grid(url, instanceName, containerId, targetObj, afterLoadCallback, datapost) {
		var self = this;
		getDataUrl = url;
		gridObject = LPQGrid(instanceName, containerId);
		targetObject = targetObj;
		self.GridObject = gridObject;
		afterLoad = afterLoadCallback;
		postData = datapost;
	}

	Grid.prototype.GridObject = gridObject;
	Grid.prototype.AfterLoad = afterLoad;
	Grid.prototype.init = function(loadOnInit) {
		if (targetObject != null && typeof targetObject != typeof undefined) {
			gridObject.setFilter(targetObject.getFilterInfo());
		}
		var g = gridObject.defineLoad(getDataUrl, postData, null, afterLoad);
		if (loadOnInit != false) {
			g.load();
		}
	};
	return Grid;
})();
(function (factory) { factory(jQuery); }(function ($) {
	$.fn.extend({
		dynamicKey: function (options) {
			if ($(this).length === 0) return false;
			return this.each(function() {
				var defaultOptions = {
					inputCtrl: $(this).prev("div.html-editor"),
					pattern: "{{KEY}}"
				};
				var settings = $.extend(true, defaultOptions, $.isPlainObject(options) ? options : {});
				$(this).find(">span").each(function (idx, ele) {
					$(ele).on("click", function () {
						//_COMMON.insertAtCursor(settings.inputCtrl[0], settings.pattern.replace("KEY", $(this).data("key")));
						_COMMON.pasteHtmlAtCaret(settings.inputCtrl[0], settings.pattern.replace("KEY", $(this).data("key")), true);
						$.smValidate.hideValidation(settings.inputCtrl);
					});
				});
			});
		}
	});
	$.fn.extend({
		appFieldMandatoryControl: function (options) {
			if ($(this).length === 0) return false;
			return this.each(function () {
				var defaultOptions = {};
				var settings = $.extend(true, defaultOptions, $.isPlainObject(options) ? options : {});
				$(this).on("change", function() {
					var val = $(this).val();
					$(this).closest(".app-field-item").removeClass("hidden-field").removeClass("required-field");
					if (val === "REQUIRED") {
						$(this).closest(".app-field-item").addClass("required-field");
					}else if (val === "HIDDEN") {
						$(this).closest(".app-field-item").addClass("hidden-field");
					}
				}).trigger("change");
			});
		}
	});
	$.fn.extend({
		selectSwitchPane: function (options) {
			if ($(this).length === 0) return false;
			return this.each(function () {
				var defaultOptions = {};
				var settings = $.extend(true, defaultOptions, $.isPlainObject(options) ? options : {});
				$(this).on("change", function () {
					$(this).find("option:not(:selected)").each(function (idx, opt) {
						$("#" + $(opt).attr("aria-controls")).removeClass("active");
					});
					var $curOpt = $(this).find("option:selected");
					$("#" + $curOpt.attr("aria-controls")).addClass("active");
				}).trigger("change");
			});
		},
		counter: function () {
			var defaults = {};
			var options = $.extend({}, defaults);
			var flag = false;
			return this.each(function () {
				var $this = $(this);
				var $label = $("#" + $this.attr("display-counter-control"));
				var maxlength = $this.attr("maxlength");
				$label.text($(this).val().length + "/" + maxlength);
				$this.bind('keyup click blur focus change paste', function () {
					var newLength = $(this).val().length;
					if (newLength > maxlength) {
						$this.val($this.val().substring(0, maxlength));
						newLength = maxlength;
						flag = true;
					}
					$this.off("keydown").keydown(function (event) {
						if (flag) {
							this.focus();
							if (event.keyCode !== 46 && event.keyCode !== 8 && $(this).val().length > maxlength) {
								$(this).val($(this).val().substring(0, maxlength));
								return false;
							} else {
								flag = false;
								return true;
							}
						}
					});
					$label.text(newLength + "/" + maxlength);
				});
			});
		}
	});
	$.fn.extend({
		ckEditor: function(options) {
			if ($(this).length === 0) return false;
			var $disclosureSection = $(this).closest("[data-section-type='disclosure']");
			return this.each(function (idx, ele) {
				var defaultOptions = {};
				var settings = $.extend(true, defaultOptions, $.isPlainObject(options) ? options : {});
				CKEDITOR.inline($(ele).attr("id"));
				CKEDITOR.instances[$(ele).attr("id")].on("change", function() {
					if (settings.onContentChanged !== null && typeof settings.onContentChanged == "function") {
						settings.onContentChanged(ele);
					}
				});
				var $commandPanel = $(ele).next(".disclosure-commands");
				
				$("button.confirmable-remove-btn", $commandPanel).on("click", function () {
					var $self = $(this);
					_COMMON.showConfirmDialog("Are you sure you want to remove this disclosure item?", function () {
						//yes
						var $wrapper = $self.closest(".disclosure-block");
						$wrapper.find(".html-editor").each(function () {
							var editorId = $(this).attr("id");
							if (CKEDITOR.instances[editorId]) {
								delete CKEDITOR.instances[editorId];
								$disclosureSection.trigger("removeItem", [editorId]);
							}
						});
						$wrapper.remove();
						if (settings.onCommandClicked !== null && typeof settings.onCommandClicked == "function") {
							settings.onCommandClicked(ele, $self);
						}
						$disclosureSection.trigger("change").trigger("removeItem");
					}, function () {
						//no
					}, "YES", "NO");
				});
				$("button.remove-btn", $commandPanel).on("click", function () {
					var $self = $(this);
					var $wrapper = $self.closest(".disclosure-block");
					$wrapper.find(".html-editor").each(function () {
						var editorId = $(this).attr("id");
						if (CKEDITOR.instances[editorId]) {
							delete CKEDITOR.instances[editorId];
							$disclosureSection.trigger("removeItem", [editorId]);
						}
					});
					$wrapper.remove();
					if (settings.onCommandClicked !== null && typeof settings.onCommandClicked == "function") {
						settings.onCommandClicked(ele, $self);
					}
					$disclosureSection.trigger("change");
				});
				$("button.moveup-btn", $commandPanel).on("click", function () {
					var $preItem = $(this).closest(".disclosure-block").prev(".disclosure-block");
					if ($preItem.length > 0) {
						$(this).closest(".disclosure-block").after($preItem);
					}
					if (settings.onCommandClicked !== null && typeof settings.onCommandClicked == "function") {
						settings.onCommandClicked(ele, this);
					}
					$disclosureSection.trigger("change");
				});
				$("button.movedown-btn", $commandPanel).on("click", function () {
					var $nextItem = $(this).closest(".disclosure-block").next(".disclosure-block");
					if ($nextItem.length > 0) {
						$(this).closest(".disclosure-block").before($nextItem);
					}
					if (settings.onCommandClicked !== null && typeof settings.onCommandClicked == "function") {
						settings.onCommandClicked(ele, this);
					}
					$disclosureSection.trigger("change");
				});
			});
		}
	});
	$.fn.extend({
		colorPicker: function (options) {
			if ($(this).length === 0) return false;
			return this.each(function () {
				var defaultOptions = {
					button: $(this).next("input.picker-btn"),
					callback: null
				};
				var settings = $.extend(true, defaultOptions, $.isPlainObject(options) ? options : {});
				var $self = $(this);
				settings.button.spectrum({
					color: "#f00",
					move: function (color) {
						var hexCode = color.toHexString();
						$(this).prev("input:text").val(hexCode).css("color", hexCode);
						if (settings.callback !== null && typeof settings.callback == "function") {
							settings.callback(hexCode);
						}
					},
					change: function (color) {
						$.smValidate.hideValidation($(this).prev("input:text"));
					}
				});
				$self.on("changeValue", function () {
					var hexCode = $(this).val();
					settings.button.spectrum("set", hexCode);
					$(this).css("color", hexCode);
					if (settings.callback !== null && typeof settings.callback == "function") {
						settings.callback(hexCode);
					}
				}).trigger("changeValue");
				$self.on("blur", function() {
					$(this).trigger("changeValue");
				});
			});
		}
	});
}));
/**
    * @param {String} instanceName - current instance name of this lib
    * @param {String} containerID - the container id that contains the gird
    * @param {Object} filterModel - filter model need to pass to server to filter data
*/
(function (global, $) {
	var Grid = function (instanceName, containerID) {
		return new Grid.init(instanceName, containerID);
	}

	function getNextSortDirection(sortDir) {
		switch (sortDir) {
			case 'NONE':
				return 'ASC';
			case 'ASC':
				return 'DESC';
			default:
				return '';
		}
	}

	function calculateSortDirection(newSortCol, currentSortCol, currentDir) {
		// check if we are going to sort the same column
		if (newSortCol === currentSortCol) {
			return getNextSortDirection(currentDir);
		} else {
			// since default sort for all is NONE -> we return next sort dir: ASC
			return 'ASC';
		}
	}

	Grid.prototype = {
		url: '',
		postData: null,
		$beforeLoad: null,
		$afterLoad: null,
		defineLoad: function (requestURL, dataToPost, fnBeforeLoad, fnAfterLoad) {
			this.url = requestURL;
			this.postData = dataToPost;
			if ($.isFunction(fnBeforeLoad)) {
				this.$beforeLoad = fnBeforeLoad;
			}
			if ($.isFunction(fnAfterLoad)) {
				this.$afterLoad = fnAfterLoad;
			}

			return this;
		},

		load: function () {

			// show loading
			//_COMMON.showLoadingPartial(this.PagingInfo.ContainerID);

			// execute beforeLoad func
			if ($.isFunction(this.$beforeLoad)) {
				this.$beforeLoad();
			}

			// gather data to post
			if (typeof this.postData === 'undefined' || this.postData === null) {
				this.postData = {};
			}

			// prepend primary data
			this.postData.paging_info = global.JSON ? JSON.stringify(this.PagingInfo) : $.toJSON(this.PagingInfo);
			this.postData.filter_info = global.JSON ? JSON.stringify(this.FilterInfo) : $.toJSON(this.FilterInfo);


			var self = this;
			// do post
			$.post(this.url, this.postData, "html").done(function (responseHTML) {
				var container = $('#' + self.PagingInfo.ContainerID);
				container.html(responseHTML);
				var wrapper = container.find('tfoot div.pagination-wrapper');
				wrapper.bootpag(self.getPaginationSetting(wrapper.attr('total-page'))).on('page', function (evt, pageNum) {
					self.setPageIndex(pageNum).load();
				});
				// execute afterLoad func
				if ($.isFunction(self.$afterLoad)) {
					self.$afterLoad(container);
				}
			});
		},

		init: function (fn) {
			if (!$.isFunction(fn)) {
				throw "init needs a parameter as function";
			}
			// do some init before calling load
			fn.bind(this)();
			return this;
		},
		resetPaging: function () {
			this.PagingInfo.PageIndex = 1;
			this.PagingInfo.PageSize = _COMMON_PAGE_SIZE;
			this.PagingInfo.SortColumn = '';
			this.PagingInfo.SortDirection = '';
			return this;
		},

		setFilter: function (data) {
			this.resetPaging();
			this.FilterInfo = data;
			return this;
		},

		setPageIndex: function (index) {
			this.PagingInfo.PageIndex = index;
			return this;
		},

		setPageSize: function (size) { //setFilter will call resetPaging so setPaging need to be invoke after setFilterc
			this.PagingInfo.PageSize = size;
			// reset page index to 1
			this.PagingInfo.PageIndex = 1;
			return this;
		},

		setSortColumn: function (colName) {
			this.PagingInfo.SortDirection = calculateSortDirection(
                colName,
                this.PagingInfo.SortColumn,
                this.PagingInfo.SortDirection
            );
			this.PagingInfo.SortColumn = colName;
			// reset page index to 1
			this.PagingInfo.PageIndex = 1;

			return this;
		},

		getPaginationSetting: function (totalPage) {
			return {
				total: totalPage,
				page: this.PagingInfo.PageIndex,
				maxVisible: 5,
				leaps: true,
				firstLastUse: true,
				first: '←',
				last: '→',
				wrapClass: 'pagination',
				activeClass: 'active',
				disabledClass: 'disabled',
				nextClass: 'next',
				prevClass: 'prev',
				lastClass: 'last',
				firstClass: 'first'
			};
		},
		log: function () {
			console.log(this);
		}
	};

	Grid.init = function (instanceName, containerID) {

		// validate params
		if (!instanceName) {
			throw 'Missing first parameter instanceName';
		}

		if (!containerID) {
			throw 'Missing second parameter containerID param';
		}

		// create public properties               
		this.PagingInfo = {
			ContainerID: containerID,
			GridInstanceName: instanceName,
			PageIndex: 1,
			PageSize: _COMMON_PAGE_SIZE,
			SortColumn: '',
			SortDirection: ''
		};
		this.FilterInfo = {};

		//this.log();
	}

	Grid.init.prototype = Grid.prototype;
	global.LPQGrid = Grid;

}(window, jQuery));