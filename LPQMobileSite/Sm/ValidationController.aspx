﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="ValidationController.aspx.vb" Inherits="Sm_ValidationController"  MasterPageFile="SiteManager.master" %>
<%@ Import Namespace="Newtonsoft.Json" %>

<asp:Content runat="server" ContentPlaceHolderID="plBody">
	<section class="design-theme-section two-row-button">
		<div class="collapse-able switch-btn-panel <%=IIf(PreviewUrlList.Count < 2, "hidden", "")%>">
			<div class="preview-url-ddl">
				<span>Select application type:</span>
			<select class="form-control inline-select" id="ddlPreviewList"><%	For Each item As KeyValuePair(Of String, String) In PreviewUrlList%>
				<option data-url="<%=GetNavigateUrl(item.Key)%>" value="<%=item.Key%>"><%=GetModuleName(item.Key)%></option>
				<%Next%></select>
			</div>
			<div>
				<div class="btn-group" role="group" aria-label="...">
					<%	For Each item As KeyValuePair(Of String, String) In PreviewUrlList.Where(Function(p) Not p.Key.StartsWith("XA") And Not p.Key.StartsWith("SA"))%>
					<a href="<%=GetNavigateUrl(item.Key)%>" data-id="<%=item.Key%>" class="btn btn-default"><%=GetModuleName(item.Key)%></a>
					<%Next%>
				</div>
			</div>
			<div>
				<div class="btn-group" role="group" aria-label="...">
					<%	For Each item As KeyValuePair(Of String, String) In PreviewUrlList.Where(Function(p) p.Key.StartsWith("XA") OrElse p.Key.StartsWith("SA"))%>
					<a href="<%=GetNavigateUrl(item.Key)%>" data-id="<%=item.Key%>" class="btn btn-default"><%=GetModuleName(item.Key)%></a>
					<%Next%>
				</div>	
			</div>
		</div>
		<div class="preview-panel">
			<div class="frame-wrapper">
				<div class="spinner">
					<div class="rect1"></div>
					<div class="rect2"></div>
					<div class="rect3"></div>
					<div class="rect4"></div>
					<div class="rect5"></div>
				</div>
				<p>Loading...</p>
		    </div>
			<div id="divPrevPage" class="editor-nav-button hidden">
				<a href="javascript:void(0);">
					<i class="fa fa-chevron-circle-left"></i>
					<span>Previous Page</span>
				</a>
			</div>
			<div id="iframeContainer" style="width: 100%; height: 100%">

			</div>
			<div id="divNextPage" class="editor-nav-button hidden">
				<a href="javascript:void(0);">
					<i class="fa fa-chevron-circle-right"></i>
					<span>Next Page</span>
				</a>
			</div>
		</div>
		<div class="design-panel mt50">
			<h1 class="page-header">Validation</h1>
			<p>To control validation (required/not required) for a field via UI</p>
			<div id="divValidatorContainer"></div> 
		</div>
	</section>
	<div></div>
</asp:Content>
<asp:Content runat="server" ContentPlaceHolderID="head">
</asp:Content>
<asp:Content runat="server" ContentPlaceHolderID="plScripts">
	<script type="text/javascript" src="js/easyxdm/easyXDM.debug.js"></script>
	<script type="text/javascript">
		var previewUrlArr = <%=JsonConvert.SerializeObject(PreviewUrlList)%>;
		var frameReadyCalled = false;

		var initFrame = function(id) {
			$(".preview-panel .frame-wrapper").show();
			$("#iframeContainer iframe").remove();
			$("a.btn.active", ".switch-btn-panel").removeClass("active");
			$("a[data-id='" + id + "']", ".switch-btn-panel").addClass("active");
			$("#ddlPreviewList").val(id);

			if (id == "LQB_MAIN") {
				$("#divPrevPage, #divNextPage").removeClass("hidden");
				$(".preview-panel, .design-panel").addClass("expanded-navigation-buttons");
			} else {
				$("#divPrevPage, #divNextPage").addClass("hidden");
				$(".preview-panel, .design-panel").removeClass("expanded-navigation-buttons");
			}

			frameReadyCalled = false;

			var rpcSetup = {
				remote: previewUrlArr[id],
				container: document.getElementById("iframeContainer"), // the path to the provider
			};
			var _rpc = new easyXDM.Rpc(rpcSetup,
			{
				local: {
					frameReady: function(data) {
						rpcSetup.container.getElementsByTagName("iframe")[0].width = "100%";
						rpcSetup.container.getElementsByTagName("iframe")[0].height = "100%";
						$(".preview-panel .frame-wrapper").hide();
						if (id != "XA_BUSINESS" && id != "SA_BUSINESS" && id != "XA_SPECIAL" && id != "SA_SPECIAL" && id!="BL" || id == "LQB_MAIN") {
							rpc.initValidationItem();
						}

						if (!frameReadyCalled) {
							// Reload the item list for the first time the frame is ready (i.e., on initial APM page load or switching the tabs)
							loadValidatorItemList(id);
							frameReadyCalled = true;
						} else {
							// Else, don't reload the rename item list, but do tell the page what the update list is, to refresh the frame.
							$("div.checkbox-btn-wrapper", "#divValidatorContainer").each(function(idx, row) {
								var $chk = $(row).find(".toggle-btn input:checkbox");
								var state;
								if ($(row).hasClass("hidden")) {
									state = "D";
								} else {
									state = $chk.is(":checked") ? "Y" : "N";;
								}
								rpc.updateValidationItem({controllerId: $chk.val(), isRequire: state});	
							});
						}
					},
					updateValidationItemList: function(data) {
						master.FACTORY.updateValidationItemList(data);
					},
					submitForm: function(data) {
						rpc = submitFrame(data, id);
					},
					triggerUpdateValidationItem: function(data) {
						var $rowItem = $("div.checkbox-btn-wrapper[data-controller-id='" + data.controllerId + "']", "#divValidatorContainer");
						if ($rowItem.length > 0) {
							var $chk = $rowItem.find(".toggle-btn input:checkbox");
							if ($rowItem.hasClass("hidden")) {
								data.isRequire = "D";
							} else {
								data.isRequire = $chk.is(":checked") ? "Y" : "N";;
							}
							
							rpc.updateValidationItem(data);
						}
					},
					canGoToPreviousQuickNavPage: function (bValue) {
						if (bValue == true) {
							$("#divPrevPage a").removeClass("disabled");
						} else {
							$("#divPrevPage a").addClass("disabled");
						}
						
					},
					canGoToNextQuickNavPage: function (bValue) {
						if (bValue == true) {
							$("#divNextPage a").removeClass("disabled");
						} else {
							$("#divNextPage a").addClass("disabled");
						}
					},
				},
				remote: {
					initValidationItem: {},
					updateValidationItem: {},
					goToPreviousQuickNavPage: {},
					goToNextQuickNavPage: {},
				}
			});
			return _rpc;
		}
		var submitFrame = function(url, id) {
			$(".preview-panel .frame-wrapper").show();
			$("#iframeContainer iframe").remove();
			var rpcSetup = {
				remote: url,
				container: document.getElementById("iframeContainer"), // the path to the provider
			};
			var _rpc = new easyXDM.Rpc(rpcSetup,
			{
				local: {
					frameReady: function(data) {
						rpcSetup.container.getElementsByTagName("iframe")[0].width = "100%";
						rpcSetup.container.getElementsByTagName("iframe")[0].height = "100%";
						$(".preview-panel .frame-wrapper").hide();
						if (id == "XA_BUSINESS" || id == "SA_BUSINESS" || id != "XA_SPECIAL" || id != "SA_SPECIAL" || id=="BL" || id == "LQB_MAIN") {
							rpc.initValidationItem();
							$("div.checkbox-btn-wrapper", "#divValidatorContainer").each(function(idx, row) {
								initValidationItemListItem(row);
								var $chk = $(row).find(".toggle-btn input:checkbox");
								var state;
								if ($(row).hasClass("hidden")) {
									state = "D";
								} else {
									state = $chk.is(":checked") ? "Y" : "N";;
								}
								rpc.updateValidationItem({controllerId: $chk.val(), isRequire: state});	
							});
						}
					},
					updateValidationItemList: function(data) {
						master.FACTORY.updateValidationItemList(data);
					},
					submitForm: function(data) {
						rpc = submitFrame(data, id);
					},
					triggerUpdateValidationItem: function(data) {
						var $rowItem = $("div.checkbox-btn-wrapper[data-controller-id='" + data.controllerId + "']", "#divValidatorContainer");
						if ($rowItem.length > 0) {
							var $chk = $rowItem.find(".toggle-btn input:checkbox");
							if ($rowItem.hasClass("hidden")) {
								data.isRequire = "D";
							} else {
								data.isRequire = $chk.is(":checked") ? "Y" : "N";;
							}
							
							rpc.updateValidationItem(data);
						}
					},
					canGoToPreviousQuickNavPage: function (bValue) {
						if (bValue == true) {
							$("#divPrevPage a").removeClass("disabled");
						} else {
							$("#divPrevPage a").addClass("disabled");
						}
						
					},
					canGoToNextQuickNavPage: function (bValue) {
						if (bValue == true) {
							$("#divNextPage a").removeClass("disabled");
						} else {
							$("#divNextPage a").addClass("disabled");
						}
					},
				},
				remote: {
					initValidationItem: {},
					updateValidationItem: {},
					goToPreviousQuickNavPage: {},
					goToNextQuickNavPage: {},
				}
			});
			return _rpc;
		}
		var initItem = master.FACTORY.getParaByName("loantype");
		if (initItem == "") {
			initItem = "<%=DefaultLoanType%>";
	}
	var rpc = initFrame(initItem);

	$(function () {
		var lastPreviewUrlSelected; 
		$("#ddlPreviewList").on("click", function() {
			lastPreviewUrlSelected = $(this).val();
		}).on("change", function() {
			if ($("body").data("hasChanged") == true) {
				if (confirm("The document contains unsaved changes. Do you want to leave?") == false) {
					$(this).val(lastPreviewUrlSelected);

				} else {
					$("body").data("hasChanged", false);
					window.location = $(this).find("option:selected").data("url");
				}
			} else {
				window.location = $(this).find("option:selected").data("url");
			}
		});
		$("#divPrevPage").on("click", function () {
			$("#divPrevPage, #divPrevPage *").blur();
			rpc.goToPreviousQuickNavPage();
		});
		$("#divNextPage").on("click", function () {
			$("#divNextPage, #divNextPage *").blur();
			rpc.goToNextQuickNavPage();
		});
	});
	(function (master, $, undefined) {
		master.FACTORY.SaveDraft = function (revisionId) {
			//if ($.smValidate("ValidateData") == false) return;
			var dataObj = collectSubmitData();
			dataObj.revisionId = revisionId;
			dataObj.command = "saveValidationItem";
			$.ajax({
				url: "/sm/smhandler.aspx",
				async: true,
				cache: false,
				type: 'POST',
				dataType: 'html',
				data: dataObj,
				success: function (responseText) {
					var response = $.parseJSON(responseText);
					if (response.IsSuccess) {
						master.FACTORY.onSaveDraftSuccess();
					} else {
						_COMMON.noty("error", "Error", 500);
					}
				}
			});
		};
		master.FACTORY.Publish = function (comment, publishAll) {
			//if ($.smValidate("ValidateData") == false) return;
			var dataObj = collectSubmitData();
			dataObj.comment = comment; 
			dataObj.publishAll = publishAll;
			dataObj.command = "publishValidationItem";
			$.ajax({
				url: "/sm/smhandler.aspx",
				async: true,
				cache: false,
				type: 'POST',
				dataType: 'html',
				data: dataObj,
				success: function (responseText) {
					var response = $.parseJSON(responseText);
					if (response.IsSuccess) {
						master.FACTORY.onPublishSuccess();
					} else {
						_COMMON.noty("error", "Error", 500);
					}
				}
			});
		};
		master.FACTORY.updateValidationItemList = function(data) {
			var $container = $("#divValidatorContainer");
			var foundedItem = null;
			master.FACTORY.documentChanged();
			$("div.checkbox-btn-wrapper", $container).each(function(idx, row) {
				if ($(row).data("controller-id") == data.controllerId/* || $(row).data("controller-name") == data.controllerName*/) {
					foundedItem = $(row);
					return false;
				}
			});
			if (foundedItem) {
				if (data.isRequire == "Y") {
					foundedItem.removeClass("hidden");
					foundedItem.find("input:checkbox").prop("checked", true);	
				}else if (data.isRequire == "N") {
					foundedItem.removeClass("hidden");
					foundedItem.find("input:checkbox").prop("checked", false);	
				} else {
					//foundedItem.remove();	
					//default state
					foundedItem.addClass("hidden");
				}
			} else {
				if (data.isRequire != "D") {
					var $item = $('<div class="checkbox-btn-wrapper" data-controller-id="' + data.controllerId + '" data-controller-name="' + data.controllerName + '"><span>' + data.controllerName + '</span><label class="toggle-btn pull-right" data-on="YES" data-off="NO"><input type="checkbox" data-text="' + data.controllerName + '" value="' + data.controllerId + '" ' + (data.isRequire == "Y" ? "checked" : "") + ' /><span class="button-checkbox"></span></label></div>').appendTo($container);
					initValidationItemListItem($item);
				} else {
					//default state
					var $item1 = $('<div class="checkbox-btn-wrapper hidden" data-controller-id="' + data.controllerId + '" data-controller-name="' + data.controllerName + '"><span>' + data.controllerName + '</span><label class="toggle-btn pull-right" data-on="YES" data-off="NO"><input type="checkbox" data-text="' + data.controllerName + '" value="' + data.controllerId + '" /><span class="button-checkbox"></span></label></div>').appendTo($container);
					initValidationItemListItem($item1);
				}
			}
			rpc.updateValidationItem(data);
		};

	}(window.master = window.master || {}, jQuery));
	function collectSubmitData() {
		var result = {};
		result.lenderconfigid = '<%=LenderConfigID%>';
		result.loantype = $("a.btn.active", ".switch-btn-panel").data("id").replace("#", "");
		result.requireFieldItemList = _COMMON.toJSON(collectRequireFieldItem());
		return result;
	}
		function collectRequireFieldItem() {
		var result = [];
		$(".toggle-btn input:checkbox", "#divValidatorContainer").each(function(idx, ele) {
			var $ele = $(ele);
			if ($ele.closest("div.checkbox-btn-wrapper").hasClass("hidden")) {
				//default state
				result.push({ ControllerId: $ele.val(), ControllerName: $ele.data("text"), isRequire: "D" }); //default
			} else {
				result.push({ ControllerId: $ele.val(), ControllerName: $ele.data("text"), isRequire: $ele.is(":checked") ? "Y" : "N" });
			}
		});
		return result;
	}
	function loadValidatorItemList(type) {
		$.ajax({
			url: "/sm/subpages/validatoritemlist.aspx?lenderconfigid=<%=LenderConfigID%>",
			async: true,
			cache: false,
			type: 'POST',
			dataType: 'html',
			data: {type: type},
			success: function (response) {
				$("#divValidatorContainer").html(response);
				$("div.checkbox-btn-wrapper", "#divValidatorContainer").each(function(idx, row) {
					initValidationItemListItem(row);
					var $chk = $(row).find(".toggle-btn input:checkbox");
					var state;
					if ($(row).hasClass("hidden")) {
						state = "D";
					} else {
						state = $chk.is(":checked") ? "Y" : "N";;
					}
					rpc.updateValidationItem({controllerId: $chk.val(), isRequire: state});	
					/*if ($chk.data("text") == "Beneficiary SSN") {
						rpc.setBeneficiarySsnInitialState(state);
					}
					if ($chk.data("text") == "Beneficiary Trust") {
						rpc.setBeneficiaryTrustInitialState(state);
					}*/
				});
			}
		});
	}
	function initValidationItemListItem(item) {
		var $item = $(item);
		$("input:checkbox", $item).on("change", function() {
			var $self = $(this);
			master.FACTORY.documentChanged();
			var state = $self.is(":checked") ? "Y" : "N";
			rpc.updateValidationItem({controllerId: $item.data("controller-id"), isRequire: state});
			/*if ($self.data("text") == "Beneficiary SSN") {
				rpc.setBeneficiarySsnInitialState(state);
			}
			if ($self.data("text") == "Beneficiary Trust") {
				rpc.setBeneficiaryTrustInitialState(state);
			}*/
		});
	}
</script>
</asp:Content>
<asp:Content runat="server" ContentPlaceHolderID="plActionButtons">
</asp:Content>