﻿
Imports System.Xml
Imports Sm
Imports LPQMobile.Utils

Partial Class Sm_ThemeEditor
	Inherits SmApmBasePage

	Protected Model As New SmThemeEditorModel

	Protected PreviewUrlList As New Dictionary(Of String, String)

	Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
		If Not Page.IsPostBack Then
			Dim smMasterPage = CType(Me.Master, Sm_SiteManager)
			smMasterPage.LenderConfigXml = LenderConfigXml
			smMasterPage.RevisionID = LenderConfig.RevisionID
			smMasterPage.LastModifiedBy = LenderConfig.LastModifiedByUserFullName
			smMasterPage.LastModifiedByUserID = LenderConfig.LastModifiedByUserID
			smMasterPage.SelectedMainMenuItem = "design"
			smMasterPage.SelectedSubMenuItem = "theme"
			smMasterPage.ShowPreviewButton = False
			smMasterPage.BreadCrumb = "Design / Themes"
			smMasterPage.CurrentNodeGroup = SmSettings.NodeGroup.DesignTheme

			Dim logoUrlNode As XmlNode = GetNode(LOGO_URL())
			If logoUrlNode IsNot Nothing Then
				Model.LogoUrl = Common.CompileLogoUrl(logoUrlNode.Value)
			End If

			Dim favIcoNode As XmlNode = GetNode(FAV_ICO())
			If favIcoNode IsNot Nothing Then
				Model.FavIco = Common.CompileLogoUrl(favIcoNode.Value)
			End If

			Dim footerThemeNode As XmlNode = GetNode(FOOTER_THEME())
			If footerThemeNode IsNot Nothing AndAlso Regex.IsMatch(footerThemeNode.Value, "^#[A-Za-z0-9]{6}$") Then
				Model.FooterColor = footerThemeNode.Value
			End If

			Dim footerFontThemeNode As XmlNode = GetNode(FOOTER_FONT_THEME())
			If footerFontThemeNode IsNot Nothing AndAlso Regex.IsMatch(footerFontThemeNode.Value, "^#[A-Za-z0-9]{6}$") Then
				Model.FooterFontColor = footerFontThemeNode.Value
			End If

			Dim buttonThemeNode As XmlNode = GetNode(BUTTON_THEME())
			If buttonThemeNode IsNot Nothing AndAlso Regex.IsMatch(buttonThemeNode.Value, "^#[A-Za-z0-9]{6}$") Then
				Model.ButtonColor = buttonThemeNode.Value
			End If

			Dim buttonFontThemeNode As XmlNode = GetNode(BUTTON_FONT_THEME())
			If buttonFontThemeNode IsNot Nothing AndAlso Regex.IsMatch(buttonFontThemeNode.Value, "^#[A-Za-z0-9]{6}$") Then
				Model.ButtonFontColor = buttonFontThemeNode.Value
			End If

			Dim headerThemeNode As XmlNode = GetNode(HEADER_THEME())
			If headerThemeNode IsNot Nothing AndAlso Regex.IsMatch(headerThemeNode.Value, "^#[A-Za-z0-9]{6}$") Then
				Model.HeaderColor = headerThemeNode.Value
			End If

			Dim header2ThemeNode As XmlNode = GetNode(HEADER2_THEME())
			If header2ThemeNode IsNot Nothing AndAlso Regex.IsMatch(header2ThemeNode.Value, "^#[A-Za-z0-9]{6}$") Then
				Model.Header2Color = header2ThemeNode.Value
			End If

			Dim backgroundThemeNode As XmlNode = GetNode(BACKGROUND_THEME())
			If backgroundThemeNode IsNot Nothing AndAlso Regex.IsMatch(backgroundThemeNode.Value, "^#[A-Za-z0-9]{6}$") Then
				Model.BackgroundColor = backgroundThemeNode.Value
			End If

			Dim contentThemeNode As XmlNode = GetNode(CONTENT_THEME())
			If contentThemeNode IsNot Nothing AndAlso Regex.IsMatch(contentThemeNode.Value, "^#[A-Za-z0-9]{6}$") Then
				Model.ContentColor = contentThemeNode.Value
			End If
			If CheckEnabledModule(SmSettings.ModuleName.CC) Then
				PreviewUrlList.Add("CC", String.Format("{0}/cc/creditcard.aspx?lenderref={1}&autofill=true&mode=777&noencrypt=true", ServerRoot, LenderRef))
			End If
			If CheckEnabledModule(SmSettings.ModuleName.HE) Then
				PreviewUrlList.Add("HE", String.Format("{0}/he/homeequityloan.aspx?lenderref={1}&autofill=true&mode=777&noencrypt=true", ServerRoot, LenderRef))
			End If
			If CheckEnabledModule(SmSettings.ModuleName.PL) Then
				PreviewUrlList.Add("PL", String.Format("{0}/pl/personalloan.aspx?lenderref={1}&autofill=true&mode=777&noencrypt=true", ServerRoot, LenderRef))
			End If
			If CheckEnabledModule(SmSettings.ModuleName.VL) Then
				PreviewUrlList.Add("VL", String.Format("{0}/vl/vehicleloan.aspx?lenderref={1}&autofill=true&mode=777&noencrypt=true", ServerRoot, LenderRef))
			End If
			If CheckEnabledModule(SmSettings.ModuleName.XA) Then
				PreviewUrlList.Add("XA", String.Format("{0}/xa/xpressapp.aspx?lenderref={1}&autofill=true&mode=777&noencrypt=true", ServerRoot, LenderRef))
			End If
			If CheckEnabledModule(SmSettings.ModuleName.LQB) Then
				PreviewUrlList.Add("LQB", String.Format("{0}/Account/Login?lenderref={1}&mode=777&apm=theme&quicknav=true&apmstub=true", Common.LQB_WEBSITE, LenderRef))
			End If
		End If
	End Sub
End Class
