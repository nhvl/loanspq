﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="PortalDashboard.aspx.vb" Inherits="Sm_PortalDashboard" MasterPageFile="MasterPage.master" %>

<asp:Content runat="server" ContentPlaceHolderID="plBody">
	<div>
		<h1 class="page-header">Portal Dashboard - <%=Portal.LenderRef %></h1>
		<p>Manage information for this portal.</p>
		<section id="dashboard">
			<ul class="nav nav-tabs" role="tablist">
				<%If SmUtil.CheckRoleAccess(HttpContext.Current, SmSettings.Role.SuperOperator, SmSettings.Role.LegacyOperator) Then%>
					<li role="presentation" ><a href="#info" aria-controls="info" role="tab" data-toggle="tab">Profile</a></li>
				<%End If%>
				<%If LenderAdminAllowed AndAlso CanManageUsers Then%>
					<li role="presentation"><a href="#users" aria-controls="users" role="tab" data-toggle="tab">Users</a></li>
				<%End If%>

                <%--//TODO: remove IsInEnvironment for deployment--%>
				<%If CanManageUsers And IsInEnvironment("DEV", "STAGE") Then%>
				    <%If LenderAdminAllowed OrElse PortalAdminAllowed Then%>
				    <li role="presentation"><a href="#vendorGroupAdmins" aria-controls="vendorGroupAdmins" role="tab" data-toggle="tab">Vendor Group Administrators</a></li>
				    <%End If%>
				   
				    <li role="presentation"><a href="#vendorUsers" aria-controls="vendorUsers" role="tab" data-toggle="tab">Vendor Administrators</a></li>
				    
				<%End If%>
				
				<%--<li role="presentation"><a href="#history" aria-controls="history" role="tab" data-toggle="tab">History</a></li>--%>
			</ul>
			<div class="tab-content">
				<%If SmUtil.CheckRoleAccess(HttpContext.Current, SmSettings.Role.SuperOperator, SmSettings.Role.LegacyOperator) Then%>
					<div role="tabpanel" class="tab-pane" id="info">
					<%--<h2 class="section-title">Edit profile</h2>--%>
						<form id="frmPortalInfo">
							<div class="row">
								<div class="col-sm-6 col-lg-4">
									<div class="form-group">
										This panel is only visible for legacy operator. <br /><br />
										<label for="txtPortalName" class="required-field">Notes</label>
										<input type="text" name="Note" data-message-require="Please fill out this field" data-required="true" class="form-control" value="<%=Portal.Note%>" id="txtPortalName" placeholder="Enter portal name">
										<input type="hidden" name="LenderConfigID" value="<%=Portal.LenderConfigID%>"/>
									</div>
								
									<div class="form-group">								
										<label for="txtLenderRef" class="required-field">Lender Ref (Caution: This may be linked to other feature) </label>
										<input type="text" name="LenderRef" data-message-require="Please fill out this field" data-required="true" class="form-control" value="<%=Portal.LenderRef%>" id="txtLenderRef" placeholder="Enter Lender Ref">
									</div>
									<%If LenderAdminAllowed Then%>
									<div>
										<button type="button" id="btnSavePortalProfile" class="btn btn-primary pull-right">Save changes</button>
									</div>
									<%End If%>
								</div>
							</div>
						</form>
					</div>
				<%End If%>
				<%If LenderAdminAllowed AndAlso CanManageUsers Then%>
				<div role="tabpanel" class="tab-pane active" id="users">
					<h2 class="section-title">List of users who have permission to setup this portal.  This panel is only visible to user with privileges greater or equal to lender admin.</h2>
					<div>
						<div class="pull-left search-bar">
							<div class="input-group">
								<input id="txtSearch" type="text" class="form-control" placeholder="Search for..." />
								<span class="input-group-btn"><button class="btn btn-secondary" type="button" id="btnSearch">Search</button></span>
							</div>	
						</div>
						<div class="pull-right add-panel">
							<div class="input-group">
								<input id="txtAddPortalUser" type="text" class="form-control" placeholder="Email" />
								<span class="input-group-btn"><button class="btn btn-secondary" type="button" id="btnAddPortalUser">Add</button></span>
							</div>
						</div>
					</div>
					
					<div class="row grid" id="divPortalUserRoleList">
						<div class="spinner">
							<div class="rect1"></div>
							<div class="rect2"></div>
							<div class="rect3"></div>
							<div class="rect4"></div>
							<div class="rect5"></div>
						</div>
						<p>Loading...</p>
					</div>
				</div>
				<%End If%>

                <%--TODO: remove IsInEnvironment for deployment--%>
				<%If CanManageUsers And IsInEnvironment("DEV", "STAGE") Then%>
				<%If LenderAdminAllowed OrElse PortalAdminAllowed Then%>
				<div role="tabpanel" class="tab-pane" id="vendorGroupAdmins">
					<h2 class="section-title">List of vendor group administrators who have permission to manage vendor adminstrators of assigned vendors. </h2>
                    This pannel in only visible to user with priviledge greater or equal to portal admin. (dev phase, only available on appstage).
					<div>
						<div class="pull-left search-bar">
							<div class="input-group">
								<input id="txtSearchVendorGroupAdmin" type="text" class="form-control" placeholder="Search for..." />
								<span class="input-group-btn"><button class="btn btn-secondary" type="button" id="btnSearchVendorGroupAdmin">Search</button></span>
							</div>	
						</div>
						<div class="pull-right add-panel">
							<button class="btn btn-secondary" type="button" id="btnAddVendorGroupAdmin">Add</button>
						</div>
					</div>
					
					<div class="row grid" id="divVendorGroupAdminList">
						<div class="spinner">
							<div class="rect1"></div>
							<div class="rect2"></div>
							<div class="rect3"></div>
							<div class="rect4"></div>
							<div class="rect5"></div>
						</div>
						<p>Loading...</p>
					</div>
				</div>
				<%End If%>
				<div role="tabpanel" class="tab-pane" id="vendorUsers">
					<h2 class="section-title">List of vendor administrators who have permission to manage vendor agents of the assigned vendor. </h2>
					Don't use this if you don't have vendor setup in lender dashboard. This pannel is only visible to users with priviledge greater or equal to vendor group admin. (dev phase, only available on appstage)<br /><br />
					<div>
						<div class="pull-left search-bar">
							<div class="input-group">
								<input id="txtSearchVendorUser" type="text" class="form-control" placeholder="Search for..." />
								<span class="input-group-btn"><button class="btn btn-secondary" type="button" id="btnSearchVendorUser">Search</button></span>
							</div>	
						</div>
						<div class="pull-right add-panel">
							<button class="btn btn-secondary" type="button" id="btnAddVendorUser">Add</button>
						</div>
					</div>
					<div class="row grid" id="divVendorUserList">
						<div class="spinner">
							<div class="rect1"></div>
							<div class="rect2"></div>
							<div class="rect3"></div>
							<div class="rect4"></div>
							<div class="rect5"></div>
						</div>
						<p>Loading...</p>
					</div>
				</div>
				<%End If%>
				<%--<div role="tabpanel" class="tab-pane fade" id="history">
					<h2 class="section-title">The activity logs list actions performed on current lender</h2>
				</div>--%>
			</div>
		</section>
	</div>
</asp:Content>
<asp:Content runat="server" ContentPlaceHolderID="plScripts">
	<script type="text/javascript">
		_COMMON_PAGE_SIZE = 10;
		$(function () {
			$('.nav-tabs a').on('shown.bs.tab', function (e) {
				<%--if (e.target.hash == "#info") {
					$(".save-changes-btn").show();
				} else {
					$(".save-changes-btn").hide();
				}--%>
				window.location.hash = e.target.hash;
				window.scrollTo(0, 0);
			});
			<%If LenderAdminAllowed Then%>
			var portalUserRoleGrid;
			portalUserRoleGrid = new _COMMON.Grid("/sm/smhandler.aspx", "portalUserRoleGrid", "divPortalUserRoleList", managePortalUserRoles.FACTORY, managePortalUserRoles.FACTORY.onLoaded, { command: "loadPortalUserRoleGrid", lenderConfigID: '<%=LenderConfigID%>' });
			portalUserRoleGrid.init();
			managePortalUserRoles.FACTORY.init(portalUserRoleGrid.GridObject);

			<%If SmUtil.CheckRoleAccess(HttpContext.current, SmSettings.Role.SuperUser) Then %>
			$('#txtAddPortalUser').typeahead({
				ajax: {
					url: '/sm/smhandler.aspx',
					method: 'POST',
					displayField: "name",
					valueField: "id",
					dataType: 'json',
					preDispatch: function () {
						return { query: $('#txtAddPortalUser').val(), command: "getAllLenderUsersForSuggest", lenderID: '<%=LenderID%>' };
					},
					preProcess: function (data) {
						return data;
					}
				},
				scrollBar: true,
				onSelect: function (item) {
				}
			});
			<%End If%>
			$('#txtAddPortalUser').on("focus", function () {
				$.smValidate.hideValidation($(this));
			});
			$('#btnAddPortalUser').on("click", function () {
				var email = $.trim($("#txtAddPortalUser").val());
				if (_COMMON.ValidateEmail(email) == true) {
					managePortalUserRoles.FACTORY.AddPortalUser(email);
				} else {
					$.smValidate.showValidation($("#txtAddPortalUser"), "Invalid email");
				}
			});

			$("#btnSearch").on("click", managePortalUserRoles.FACTORY.search);
			$('#txtSearch').on("keypress", function (e) {
				var code = (e.keyCode ? e.keyCode : e.which);
				if (code == 13) {
					managePortalUserRoles.FACTORY.search();
				}
			});

			registerDataValidator($("#frmPortalInfo"), "portal_info");
			//$(".save-changes-btn").on("click", managePortalUserRoles.FACTORY.SavePortalInfo);
			$("#btnSavePortalProfile").on("click", managePortalUserRoles.FACTORY.SavePortalInfo);
			<%End If%>
			<%If CanManageUsers And IsInEnvironment("DEV", "STAGE") Then%>
			var vendorUserGrid;
			vendorUserGrid = new _COMMON.Grid("/sm/smhandler.aspx", "vendorUserGrid", "divVendorUserList", manageVendorUsers.FACTORY, manageVendorUsers.FACTORY.onLoaded, { command: "loadVendorUserGrid", lenderConfigID: '<%=LenderConfigID%>' });
			vendorUserGrid.init();
			manageVendorUsers.FACTORY.init(vendorUserGrid.GridObject);

			$('#btnAddVendorUser').on("click", function () {
				manageVendorUsers.FACTORY.loadAddVendorUserForm();
			});

			$("#btnSearchVendorUser").on("click", manageVendorUsers.FACTORY.search);

			$('#txtSearchVendorUser').on("keypress", function (e) {
				var code = (e.keyCode ? e.keyCode : e.which);
				if (code == 13) {
					manageVendorUsers.FACTORY.search();
				}
			});

			<%If LenderAdminAllowed OrElse PortalAdminAllowed Then%>
			var vendorGroupAdminGrid;
			vendorGroupAdminGrid = new _COMMON.Grid("/sm/smhandler.aspx", "vendorGroupAdminGrid", "divVendorGroupAdminList", manageVendorGroupAdmin.FACTORY, manageVendorGroupAdmin.FACTORY.onLoaded, { command: "loadVendorGroupAdminGrid", lenderConfigID: '<%=LenderConfigID%>' });
			vendorGroupAdminGrid.init();
			manageVendorGroupAdmin.FACTORY.init(vendorGroupAdminGrid.GridObject);

			$('#btnAddVendorGroupAdmin').on("click", function () {
				manageVendorGroupAdmin.FACTORY.loadAddVendorGroupAdminForm();
			});

			$("#btnSearchVendorGroupAdmin").on("click", manageVendorGroupAdmin.FACTORY.search);

			$('#txtSearchVendorGroupAdmin').on("keypress", function (e) {
				var code = (e.keyCode ? e.keyCode : e.which);
				if (code == 13) {
					manageVendorGroupAdmin.FACTORY.search();
				}
			});
			<%End If%>
			<%End If%>
			var url = document.location.toString();
			if (url.match('#') && $.trim(url.split('#')[1]) != "") {
				$('.nav-tabs a[href="#' + url.split('#')[1] + '"]').tab('show');
			} else {
				var tab = $("div.tab-content >.tab-pane:first-child").attr("id");
				$('.nav-tabs a[href="#' + tab + '"]').tab('show');
			}
			
		});
		<%If LenderAdminAllowed Then%>
		(function (managePortalUserRoles, $, undefined) {
			managePortalUserRoles.DATA = {};
			managePortalUserRoles.FACTORY = {};
			managePortalUserRoles.FACTORY.init = function (gridInstance) {
				var self = this;
				managePortalUserRoles.DATA.grid = gridInstance;
			};
			managePortalUserRoles.FACTORY.getFilterInfo = function () {
				//implement this later
				return { Keyword: $.trim($("#txtSearch").val()) };
			};
			managePortalUserRoles.FACTORY.onLoaded = function (container) {
				$("input:checkbox[data-command='enable-btn']", $("#divPortalUserRoleList")).each(function (idx, ele) {
					var $self = $(ele);
					$self.on("change", function () {
						managePortalUserRoles.FACTORY.ShowChangeStateDialog($self.closest("tr").data("id"), $self);
					});
				});
				$("a[data-command='delete']", $("#divPortalUserRoleList")).each(function (idx, ele) {
					var $self = $(ele);
					$self.on("click", function () {
						managePortalUserRoles.FACTORY.ShowRemoveDialog($self.closest("tr").data("id"), $self);
					});
				});
				$("a[data-command='unlock']", $("#divPortalUserRoleList")).each(function (idx, ele) {
					var $self = $(ele);
					$self.on("click", function () {
						managePortalUserRoles.FACTORY.UnlockUser($self.closest("td").data("id"), $self);
					});
				});
			};
			managePortalUserRoles.FACTORY.ShowChangeStateDialog = function (id, srcEle) {
				_COMMON.showDialogSaveClose("Leave a comment", 0, "takenote_dialog_0", '<div style="margin:10px 20px;"><textarea data-id="txtComment" class="form-control required" rows="3" data-message-require="Comment cannot be empty" placeholder="Write a comment" maxlength="1000" display-counter-control="spanCommentCounter"></textarea><div class="text-right char-count"><span id="spanCommentCounter"></span></div><label for="txtComment" class="has-error" style="display:none"></label></div>', "", null, function (container) {
					var comment = $("textarea[data-id='txtComment']", $("#takenote_dialog_0")).val();
					managePortalUserRoles.FACTORY.ChangeState(id, $(srcEle).is(":checked"), srcEle, comment, container);
				}, function (container) {
					$("textarea[data-id='txtComment']", $(container)).counter();
				}, function () {
					//reload grid when dialog closed
					var pageIndex = managePortalUserRoles.DATA.grid.getPaginationSetting().page;
					managePortalUserRoles.DATA.grid.setFilter(managePortalUserRoles.FACTORY.getFilterInfo()).setPageIndex(pageIndex).load();
				});
			};
			managePortalUserRoles.FACTORY.UnlockUser = function (id, sourceEle) {
				var dataObj = { userid: id, command: "unlockUser" }
				$.ajax({
					url: "/sm/smhandler.aspx",
					async: true,
					cache: false,
					type: 'POST',
					dataType: 'html',
					data: dataObj,
					success: function (responseText) {
						var response = $.parseJSON(responseText);
						if (response.IsSuccess) {
							_COMMON.noty("success", "Success", 500);
							var pageIndex = managePortalUserRoles.DATA.grid.getPaginationSetting().page;
							managePortalUserRoles.DATA.grid.setFilter(managePortalUserRoles.FACTORY.getFilterInfo()).setPageIndex(pageIndex).load();
						} else {
							_COMMON.noty("error", "Error", 500);
							$(sourceEle).prop("checked", !isEnabled);
						}
					}
				});
			};
			managePortalUserRoles.FACTORY.ChangeState = function (id, isEnabled, sourceEle, comment, container) {
				var dataObj = { userRoleID: id, comment: comment, lenderID: '<%=LenderID%>', lenderConfigID: '<%=LenderConfigID%>', enable: isEnabled ? "Y" : "N", command: "changePortalUserRoleState" }
				$.ajax({
					url: "/sm/smhandler.aspx",
					async: true,
					cache: false,
					type: 'POST',
					dataType: 'html',
					data: dataObj,
					success: function (responseText) {
						var response = $.parseJSON(responseText);
						if (response.IsSuccess) {
							_COMMON.noty("success", "Success", 500);
							if (response.Info == "RELOAD") {
								window.location = window.location.href.replace(/#\w*$/, "");
							}
							var pageIndex = managePortalUserRoles.DATA.grid.getPaginationSetting().page;
							managePortalUserRoles.DATA.grid.setFilter(managePortalUserRoles.FACTORY.getFilterInfo()).setPageIndex(pageIndex).load();
							container.dialog.close();
						} else {
							_COMMON.noty("error", "Error", 500);
							$(sourceEle).prop("checked", !isEnabled);
						}
					}
				});
			};
			managePortalUserRoles.FACTORY.ShowRemoveDialog = function (id, srcEle) {
				_COMMON.showDialogSaveClose("Leave a comment", 0, "takenote_dialog_0", '<div style="margin:10px 20px;"><textarea data-id="txtComment" class="form-control required" rows="3" data-message-require="Comment cannot be empty" placeholder="Write a comment" maxlength="1000" display-counter-control="spanCommentCounter"></textarea><div class="text-right char-count"><span id="spanCommentCounter"></span></div><label for="txtComment" class="has-error" style="display:none"></label></div>', "", null, function (container) {
					var comment = $("textarea[data-id='txtComment']", $("#takenote_dialog_0")).val();
					managePortalUserRoles.FACTORY.DeleteUser(id, srcEle, comment, container);
				}, function (container) {
					$("textarea[data-id='txtComment']", $(container)).counter();
				}, function () {
					//reload grid when dialog closed
					var pageIndex = managePortalUserRoles.DATA.grid.getPaginationSetting().page;
					managePortalUserRoles.DATA.grid.setFilter(managePortalUserRoles.FACTORY.getFilterInfo()).setPageIndex(pageIndex).load();
				});
			};
			managePortalUserRoles.FACTORY.DeleteUser = function (id, sourceEle, comment, container) {
				var dataObj = { userRoleID: id, comment: comment, lenderID: '<%=LenderID%>', lenderConfigID: '<%=LenderConfigID%>', command: "removePortalUserRole" }
				$.ajax({
					url: "/sm/smhandler.aspx",
					async: true,
					cache: false,
					type: 'POST',
					dataType: 'html',
					data: dataObj,
					success: function (responseText) {
						var response = $.parseJSON(responseText);
						if (response.IsSuccess) {
							_COMMON.noty("success", "Success", 500);
							if (response.Info == "RELOAD") {
								window.location = window.location.href.replace(/#\w*$/, "");
							}
							var pageIndex = managePortalUserRoles.DATA.grid.getPaginationSetting().page;
							managePortalUserRoles.DATA.grid.setFilter(managePortalUserRoles.FACTORY.getFilterInfo()).setPageIndex(pageIndex).load();
							container.dialog.close();
						} else {
							_COMMON.noty("error", "Error", 500);
						}
					}
				});
			};
			managePortalUserRoles.FACTORY.AddPortalUser = function (email) {
				$("#btnAddPortalUser").button("loading");
				var dataObj = { email: email, lenderID: '<%=LenderID%>', lenderConfigID: '<%=LenderConfigID%>', command: "addPortalUser" }
				$.ajax({
					url: "/sm/smhandler.aspx",
					async: true,
					cache: false,
					type: 'POST',
					dataType: 'html',
					data: dataObj,
					success: function (responseText) {
						$("#btnAddPortalUser").button("reset");
						var response = $.parseJSON(responseText);
						if (response.IsSuccess) {
							_COMMON.noty("success", "Success", 500);
							var pageIndex = managePortalUserRoles.DATA.grid.getPaginationSetting().page;
							managePortalUserRoles.DATA.grid.setFilter(managePortalUserRoles.FACTORY.getFilterInfo()).setPageIndex(pageIndex).load();
							$("#txtAddPortalUser").val("");
						} else if (response.Info == "ALREADY") {
							_COMMON.showAlertDialog(response.Message);
						} else if (response.Info == "UPGRADE") {
							//show confirm popup
							_COMMON.showConfirmDialog(response.Message, function () {
								//yes
								upgradeToPortalAdmin(email);
							}, function () {
								//no
							}, "YES", "NO");
						} else if (response.Info == "DOWNGRADE") {
							//show confirm popup
							_COMMON.showConfirmDialog(response.Message, function () {
								//yes
								downgradeToPortalAdmin(email);
							}, function () {
								//no
							}, "YES", "NO");
						} else {
							_COMMON.noty("error", response.Message, 500);
						}
					}
				});
			};

			function upgradeToPortalAdmin(email) {
				$.ajax({
					url: "/sm/smhandler.aspx",
					async: true,
					cache: false,
					type: 'POST',
					dataType: 'html',
					data: { email: email, command: 'upgradeToPortalAdmin', lenderID: '<%=LenderID%>', lenderConfigID: '<%=LenderConfigID%>' },
					success: function (respText) {
						var resp = $.parseJSON(respText);
						if (resp.IsSuccess) {
							_COMMON.noty("success", "Success.", 500);
							var pIndex = managePortalUserRoles.DATA.grid.getPaginationSetting().page;
							managePortalUserRoles.DATA.grid.setFilter(managePortalUserRoles.FACTORY.getFilterInfo()).setPageIndex(pIndex).load();
							var vIndex = manageVendorUsers.DATA.grid.getPaginationSetting().page;
							manageVendorUsers.DATA.grid.setFilter(manageVendorUsers.FACTORY.getFilterInfo()).setPageIndex(vIndex).load();
							$("#txtAddPortalUser").val("");
						} else {
							_COMMON.noty("error", "Error", 500);
						}
					}
				});
			}
			function downgradeToPortalAdmin(email) {
				$.ajax({
					url: "/sm/smhandler.aspx",
					async: true,
					cache: false,
					type: 'POST',
					dataType: 'html',
					data: { email: email, command: 'downgradeToPortalAdmin', lenderID: '<%=LenderID%>', lenderConfigID: '<%=LenderConfigID%>' },
					success: function (respText) {
						var resp = $.parseJSON(respText);
						if (resp.IsSuccess) {
							_COMMON.noty("success", "Success.", 500);
							if (resp.Info == "RELOAD") {
								window.location = window.location.href.replace(/#\w*$/, '');
							} else {
								var pIndex = managePortalUserRoles.DATA.grid.getPaginationSetting().page;
								managePortalUserRoles.DATA.grid.setFilter(managePortalUserRoles.FACTORY.getFilterInfo()).setPageIndex(pIndex).load();
								var vIndex = manageVendorUsers.DATA.grid.getPaginationSetting().page;
								manageVendorUsers.DATA.grid.setFilter(manageVendorUsers.FACTORY.getFilterInfo()).setPageIndex(vIndex).load();
								$("#txtAddPortalUser").val("");
							}
						} else {
							_COMMON.noty("error", "Error", 500);
						}
					}
				});
			}

			managePortalUserRoles.FACTORY.search = function () {
				managePortalUserRoles.DATA.grid.setFilter(managePortalUserRoles.FACTORY.getFilterInfo()).setPageIndex(1).load();
			};
			managePortalUserRoles.FACTORY.SavePortalInfo = function () {
				if ($.smValidate("portal_info") == false) return;
				var portalData = $("#frmPortalInfo").serializeJSON({ parseAll: true, checkboxUncheckedValue: "false" });
				//update data
				var postData = {
					command: "savePortalInfo",
					portal_data: _COMMON.toJSON(portalData)
				};
				$.ajax({
					url: "/sm/smhandler.aspx",
					async: true,
					cache: false,
					type: 'POST',
					dataType: 'html',
					data: postData,
					success: function (responseText) {
						var response = $.parseJSON(responseText);
						if (response.IsSuccess) {
							_COMMON.noty("success", "Success", 500);
							$("h1.page-header").text("Portal Dashboard - " + response.Info.name + "(" + response.Info.ref + ")");
							$("#spLenderRef").text(response.Info.ref);
						}else if(response.Info == "EXISTED"){
							_COMMON.showAlertDialog(response.Message);
						} else {
							var errorMsg = response.Message !== "" ? response.Message : "Error";
							_COMMON.noty("error", errorMsg, 500);
						}
					}
				});
			};
		}(window.managePortalUserRoles = window.managePortalUserRoles || {}, jQuery));
		<%End If%>
		<%If CanManageUsers And IsInEnvironment("DEV", "STAGE") then%>
		(function (manageVendorUsers, $, undefined) {
			manageVendorUsers.DATA = {};
			manageVendorUsers.FACTORY = {};
			manageVendorUsers.FACTORY.init = function (gridInstance) {
				var self = this;
				manageVendorUsers.DATA.grid = gridInstance;
			};
			manageVendorUsers.FACTORY.getFilterInfo = function () {
				//implement this later
				return { Keyword: $.trim($("#txtSearchVendorUser").val()) };
			};
			manageVendorUsers.FACTORY.onLoaded = function (container) {
				$("input:checkbox[data-command='enable-btn']", $("#divVendorUserList")).each(function (idx, ele) {
					var $self = $(ele);
					$self.on("change", function () {
						manageVendorUsers.FACTORY.ShowChangeStateDialog($self.closest("tr").data("id"), $self);
					});
				});
				$("a[data-command='delete']", $("#divVendorUserList")).each(function (idx, ele) {
					var $self = $(ele);
					$self.on("click", function () {
						manageVendorUsers.FACTORY.ShowRemoveDialog($self.closest("tr").data("id"), $self);
					});
				});
				$("a[data-command='unlock']", $("#divVendorUserList")).each(function (idx, ele) {
					var $self = $(ele);
					$self.on("click", function () {
						manageVendorUsers.FACTORY.UnlockUser($self.closest("td").data("id"), $self);
					});
				});
			};
			manageVendorUsers.FACTORY.UnlockUser = function (id, sourceEle) {
				var dataObj = { userid: id, command: "unlockUser" }
				$.ajax({
					url: "/sm/smhandler.aspx",
					async: true,
					cache: false,
					type: 'POST',
					dataType: 'html',
					data: dataObj,
					success: function (responseText) {
						var response = $.parseJSON(responseText);
						if (response.IsSuccess) {
							_COMMON.noty("success", "Success", 500);
							var pageIndex = manageVendorUsers.DATA.grid.getPaginationSetting().page;
							manageVendorUsers.DATA.grid.setFilter(manageVendorUsers.FACTORY.getFilterInfo()).setPageIndex(pageIndex).load();
						} else {
							_COMMON.noty("error", "Error", 500);
							$(sourceEle).prop("checked", !isEnabled);
						}
					}
				});
			};
			manageVendorUsers.FACTORY.ShowRemoveDialog = function (id, srcEle) {
				_COMMON.showDialogSaveClose("Leave a comment", 0, "takenote_dialog_0", '<div style="margin:10px 20px;"><textarea data-id="txtComment" class="form-control required" rows="3" data-message-require="Comment cannot be empty" placeholder="Write a comment" maxlength="1000" display-counter-control="spanCommentCounter"></textarea><div class="text-right char-count"><span id="spanCommentCounter"></span></div><label for="txtComment" class="has-error" style="display:none"></label></div>', "", null, function (container) {
					var comment = $("textarea[data-id='txtComment']", $("#takenote_dialog_0")).val();
					manageVendorUsers.FACTORY.DeleteUserRole(id, srcEle, comment, container);
				}, function (container) {
					$("textarea[data-id='txtComment']", $(container)).counter();
				}, function () {
					//reload grid when dialog closed
					var pageIndex = manageVendorUsers.DATA.grid.getPaginationSetting().page;
					manageVendorUsers.DATA.grid.setFilter(manageVendorUsers.FACTORY.getFilterInfo()).setPageIndex(pageIndex).load();
				});
			};
			manageVendorUsers.FACTORY.DeleteUserRole = function (id, sourceEle, comment, container) {
				var dataObj = { userRoleID: id, comment: comment, lenderID: '<%=LenderID%>', lenderConfigID: '<%=LenderConfigID%>', command: "removeVendorUser" }
				$.ajax({
					url: "/sm/smhandler.aspx",
					async: true,
					cache: false,
					type: 'POST',
					dataType: 'html',
					data: dataObj,
					success: function (responseText) {
						var response = $.parseJSON(responseText);
						if (response.IsSuccess) {
							_COMMON.noty("success", "Success", 500);
							var pageIndex = manageVendorUsers.DATA.grid.getPaginationSetting().page;
							manageVendorUsers.DATA.grid.setFilter(manageVendorUsers.FACTORY.getFilterInfo()).setPageIndex(pageIndex).load();
							container.dialog.close();
						} else {
							_COMMON.noty("error", "Error", 500);
						}
					}
				});
			};
			manageVendorUsers.FACTORY.ShowChangeStateDialog = function (id, srcEle) {
				_COMMON.showDialogSaveClose("Leave a comment", 0, "takenote_dialog_0", '<div style="margin:10px 20px;"><textarea data-id="txtComment" class="form-control required" rows="3" data-message-require="Comment cannot be empty" placeholder="Write a comment" maxlength="1000" display-counter-control="spanCommentCounter"></textarea><div class="text-right char-count"><span id="spanCommentCounter"></span></div><label for="txtComment" class="has-error" style="display:none"></label></div>', "", null, function (container) {
					var comment = $("textarea[data-id='txtComment']", $("#takenote_dialog_0")).val();
					manageVendorUsers.FACTORY.ChangeState(id, $(srcEle).is(":checked"), srcEle, comment, container);
				}, function (container) {
					$("textarea[data-id='txtComment']", $(container)).counter();
				}, function () {
					//reload grid when dialog closed
					var pageIndex = manageVendorUsers.DATA.grid.getPaginationSetting().page;
					manageVendorUsers.DATA.grid.setFilter(manageVendorUsers.FACTORY.getFilterInfo()).setPageIndex(pageIndex).load();
				});
			};

			manageVendorUsers.FACTORY.ChangeState = function (id, isEnabled, sourceEle, comment, container) {
				var dataObj = { userRoleID: id, comment: comment, lenderConfigID: '<%=LenderConfigID%>', lenderID: '<%=LenderID%>', enable: isEnabled ? "Y" : "N", command: "changeVendorUserState" }
				$.ajax({
					url: "/sm/smhandler.aspx",
					async: true,
					cache: false,
					type: 'POST',
					dataType: 'html',
					data: dataObj,
					success: function (responseText) {
						var response = $.parseJSON(responseText);
						if (response.IsSuccess) {
							_COMMON.noty("success", "Success", 500);
							var pageIndex = manageVendorUsers.DATA.grid.getPaginationSetting().page;
							manageVendorUsers.DATA.grid.setFilter(manageVendorUsers.FACTORY.getFilterInfo()).setPageIndex(pageIndex).load();
							container.dialog.close();
						} else {
							_COMMON.noty("error", "Error", 500);
							$(sourceEle).prop("checked", !isEnabled);
						}
					}
				});
			};
			manageVendorUsers.FACTORY.loadAddVendorUserForm = function () {
				_COMMON.showDialogSaveClose("Add Vendor Administrator", 0, "addvendoruser_dialog_0", "", "/sm/smhandler.aspx", { command: "loadAddVendorUserForm", lenderConfigID: '<%=LenderConfigID%>'}, manageVendorUsers.FACTORY.addVendorUser, function (container) {
					//add validator
					registerDataValidator($("#addvendoruser_dialog_0"), "addvendoruser_dialog_0");
					$("#addvendoruser_dialog_0 #btnAddNewVendor").off("click").on("click", function () {
						_COMMON.showDialogSaveClose("Add New Vendor", 0, "addvendor_dialog_0", "", "/sm/smhandler.aspx", { command: "loadAddVendorForm" }, function (obj) {
							//saving
							if ($("#hdLogo").val() == "") {
								$.smValidate.showValidation($("#hdLogo").closest(".form-group"), "Please upload logo");
							} else {
								$.smValidate.hideValidation($("#hdLogo").closest(".form-group"));
							}
							if ($.smValidate("addvendor_dialog_0") == false || $("#hdLogo").val() == "") return;
							var vendorData = obj.container.find("form").serializeJSON({ parseAll: true });
							//update data
							var postData = {
								command: "saveNewLenderVendor",
								vendor_data: _COMMON.toJSON(vendorData),
								lenderID: '<%=LenderID%>',
								bid: '<%=BackOfficeLender.BackOfficeLenderID%>'
							};
							$.ajax({
								url: "/sm/smhandler.aspx",
								async: true,
								cache: false,
								type: 'POST',
								dataType: 'html',
								data: postData,
								success: function (responseText) {
									var response = $.parseJSON(responseText);
									if (response.IsSuccess) {
										//console.log(response);
										var $ddlVendor = $("#ddlVendor", container);
										$ddlVendor.append($('<option>', { value: response.Info.id, text: response.Info.name }));
										$ddlVendor.val(response.Info.id);
										$.smValidate.hideValidation($ddlVendor);
										obj.dialog.close();
									} else {
										var errorMsg = response.Message !== "" ? response.Message : "Error";
										_COMMON.noty("error", errorMsg, 500);
									}
								}
							});
						}, function (container) {
							//on loaded
							//add validator
							registerDataValidator($("#addvendor_dialog_0"), "addvendor_dialog_0");
							$("#divVendorLogo").dropzone({
								url: "/sm/smhandler.aspx",
								dictDefaultMessage: "<p>Drop logo file here or click to select image from your computer</p><p>Compatible file types: JPG, PNG. Recommended approximate dimension: 350 x 200.</p><p>Recommended file size: 10KB.</p>",
								maxFilesize: 2,
								acceptedFiles: "image/png,image/jpeg",
								params: { "command": "uploadlogo" },
								maxFiles: 1,
								addRemoveLinks: true,
								dictRemoveFile: "Remove",
								maxfilesexceeded: function (file) {
									this.removeAllFiles();
									this.addFile(file);
								},
								thumbnailWidth: 200,
								thumbnailHeight: null,
								init: function () {
									manageVendorUsers.DATA.currentLogoFile = null;
									this.on("addedfile", function (file) {
										if (manageVendorUsers.DATA.currentLogoFile) {
											this.removeFile(manageVendorUsers.DATA.currentLogoFile);
										}
										manageVendorUsers.DATA.currentLogoFile = file;
									});
									this.on("success", function (file, response) {
										$("#hdLogo").val(response);
										$.smValidate.hideValidation($("#hdLogo").closest(".form-group"));
										manageVendorUsers.DATA.currentLogoFile = file;
									});
									this.on("removedfile", function (file) {
										manageVendorUsers.DATA.currentLogoFile = null;
										if (file.xhr) {
											$.ajax({
												url: "/sm/smhandler.aspx",
												async: true,
												cache: false,
												type: 'POST',
												dataType: 'html',
												data: { command: 'deletelogo', logourl: file.xhr.responseText },
												success: function(responseText) {
													$("#hdLogo").val("");
												}
											});
										} else {
											$("#hdLogo").val("");
										}
										
									});
								}
							});
							$("#addvendor_dialog_0 input[data-role=tagsinput]").tagsinput({
								trimValue: true,
								typeahead: {
									ajax: {
										url: '/sm/smhandler.aspx',
										method: 'POST',
										displayField: "name",
										valueField: "id",
										dataType: 'json',
										preDispatch: function () {
											return { query: "", command: "getAvailableTags", lenderID: '<%=LenderID%>' };
								},
								preProcess: function (data) {
									return data;
								}
							}
						}
							});
						}, function () {
							//reload grid when dialog closed
						});
					});
				}, function () {
					//reload grid when dialog closed
					var pageIndex = manageVendorUsers.DATA.grid.getPaginationSetting().page;
					manageVendorUsers.DATA.grid.setFilter(manageVendorUsers.FACTORY.getFilterInfo()).setPageIndex(pageIndex).load();
				});
			};

			manageVendorUsers.FACTORY.addVendorUser = function (obj, force) {
				if (typeof(force) == "undefined") force = false;
				if ($.smValidate("addvendoruser_dialog_0") == false) return;
				var vendorUserData = obj.container.find("form").serializeJSON({ parseAll: true, checkboxUncheckedValue: "false" });
				//update data
				var postData = {
					command: "saveNewVendorUser",
					email: vendorUserData.Email,
					lenderID: '<%=LenderID%>',
					lenderConfigID: '<%=LenderConfigID%>',
					vendorID: vendorUserData.VendorID,
					role: vendorUserData.Role,
					force: force
				};
				$.ajax({
					url: "/sm/smhandler.aspx",
					async: true,
					cache: false,
					type: 'POST',
					dataType: 'html',
					data: postData,
					success: function (responseText) {
						var response = $.parseJSON(responseText);
						if (response.IsSuccess) {
							_COMMON.noty("success", "Success", 500);
							obj.dialog.close();
						} else if (response.Info == "ALREADY") {
							_COMMON.showAlertDialog(response.Message);
						} else if (response.Info == "UPGRADE" || response.Info == "DOWNGRADE") {
							_COMMON.showConfirmDialog(response.Message, function () {
								manageVendorUsers.FACTORY.addVendorUser(obj, true);
							}, function () {
								//no
								//obj.dialog.close();
							}, "YES", "NO");
						} else {
							var errorMsg = response.Message !== "" ? response.Message : "Error";
							_COMMON.noty("error", errorMsg, 500);
						}
					}
				});
			};
			manageVendorUsers.FACTORY.search = function () {
				manageVendorUsers.DATA.grid.setFilter(manageVendorUsers.FACTORY.getFilterInfo()).setPageIndex(1).load();
			};
		}(window.manageVendorUsers = window.manageVendorUsers || {}, jQuery));






		<%If LenderAdminAllowed OrElse PortalAdminAllowed Then%>

		(function (manageVendorGroupAdmin, $, undefined) {
			manageVendorGroupAdmin.DATA = {};
			manageVendorGroupAdmin.FACTORY = {};
			manageVendorGroupAdmin.FACTORY.init = function (gridInstance) {
				var self = this;
				manageVendorGroupAdmin.DATA.grid = gridInstance;
			};
			manageVendorGroupAdmin.FACTORY.getFilterInfo = function () {
				//implement this later
				return { Keyword: $.trim($("#txtSearchVendorGroupAdmin").val()) };
			};
			manageVendorGroupAdmin.FACTORY.onLoaded = function (container) {
				$("input:checkbox[data-command='enable-btn']", $("#divVendorGroupAdminList")).each(function (idx, ele) {
					var $self = $(ele);
					$self.on("change", function () {
						manageVendorGroupAdmin.FACTORY.ShowChangeStateDialog($self.closest("tr").data("id"), $self);
					});
				});
				$("a[data-command='delete']", $("#divVendorGroupAdminList")).each(function (idx, ele) {
					var $self = $(ele);
					$self.on("click", function () {
						manageVendorGroupAdmin.FACTORY.ShowRemoveDialog($self.closest("tr").data("id"), $self);
					});
				});
				$("a[data-command='unlock']", $("#divVendorGroupAdminList")).each(function (idx, ele) {
					var $self = $(ele);
					$self.on("click", function () {
						manageVendorGroupAdmin.FACTORY.UnlockUser($self.closest("td").data("id"), $self);
					});
				});
				$("a[data-command='edittags']", $("#divVendorGroupAdminList")).each(function (idx, ele) {
					var $self = $(ele);
					$self.on("click", function () {
						manageVendorGroupAdmin.FACTORY.loadEditTagsForm($self.closest("tr").data("id"), $self);
					});
				});
			};
			manageVendorGroupAdmin.FACTORY.UnlockUser = function (id, sourceEle) {
				var dataObj = { userid: id, command: "unlockUser" }
				$.ajax({
					url: "/sm/smhandler.aspx",
					async: true,
					cache: false,
					type: 'POST',
					dataType: 'html',
					data: dataObj,
					success: function (responseText) {
						var response = $.parseJSON(responseText);
						if (response.IsSuccess) {
							_COMMON.noty("success", "Success", 500);
							var pageIndex = manageVendorGroupAdmin.DATA.grid.getPaginationSetting().page;
							manageVendorGroupAdmin.DATA.grid.setFilter(manageVendorGroupAdmin.FACTORY.getFilterInfo()).setPageIndex(pageIndex).load();
						} else {
							_COMMON.noty("error", "Error", 500);
							$(sourceEle).prop("checked", !isEnabled);
						}
					}
				});
			};
			manageVendorGroupAdmin.FACTORY.ShowRemoveDialog = function (id, srcEle) {
				_COMMON.showDialogSaveClose("Leave a comment", 0, "takenote_dialog_0", '<div style="margin:10px 20px;"><textarea data-id="txtComment" class="form-control required" rows="3" data-message-require="Comment cannot be empty" placeholder="Write a comment" maxlength="1000" display-counter-control="spanCommentCounter"></textarea><div class="text-right char-count"><span id="spanCommentCounter"></span></div><label for="txtComment" class="has-error" style="display:none"></label></div>', "", null, function (container) {
					var comment = $("textarea[data-id='txtComment']", $("#takenote_dialog_0")).val();
					manageVendorGroupAdmin.FACTORY.DeleteUserRole(id, srcEle, comment, container);
				}, function (container) {
					$("textarea[data-id='txtComment']", $(container)).counter();
				}, function () {
					//reload grid when dialog closed
					var pageIndex = manageVendorGroupAdmin.DATA.grid.getPaginationSetting().page;
					manageVendorGroupAdmin.DATA.grid.setFilter(manageVendorGroupAdmin.FACTORY.getFilterInfo()).setPageIndex(pageIndex).load();
				});
			};
			manageVendorGroupAdmin.FACTORY.DeleteUserRole = function (id, sourceEle, comment, container) {
				var dataObj = { userRoleID: id, comment: comment, lenderID: '<%=LenderID%>', lenderConfigID: '<%=LenderConfigID%>', command: "removeVendorGroupAdmin" }
				$.ajax({
					url: "/sm/smhandler.aspx",
					async: true,
					cache: false,
					type: 'POST',
					dataType: 'html',
					data: dataObj,
					success: function (responseText) {
						var response = $.parseJSON(responseText);
						if (response.IsSuccess) {
							_COMMON.noty("success", "Success", 500);
							var pageIndex = manageVendorGroupAdmin.DATA.grid.getPaginationSetting().page;
							manageVendorGroupAdmin.DATA.grid.setFilter(manageVendorGroupAdmin.FACTORY.getFilterInfo()).setPageIndex(pageIndex).load();
							container.dialog.close();
						} else {
							_COMMON.noty("error", "Error", 500);
						}
					}
				});
			};
			manageVendorGroupAdmin.FACTORY.ShowChangeStateDialog = function (id, srcEle) {
				_COMMON.showDialogSaveClose("Leave a comment", 0, "takenote_dialog_0", '<div style="margin:10px 20px;"><textarea data-id="txtComment" class="form-control required" rows="3" data-message-require="Comment cannot be empty" placeholder="Write a comment" maxlength="1000" display-counter-control="spanCommentCounter"></textarea><div class="text-right char-count"><span id="spanCommentCounter"></span></div><label for="txtComment" class="has-error" style="display:none"></label></div>', "", null, function (container) {
					var comment = $("textarea[data-id='txtComment']", $("#takenote_dialog_0")).val();
					manageVendorGroupAdmin.FACTORY.ChangeState(id, $(srcEle).is(":checked"), srcEle, comment, container);
				}, function (container) {
					$("textarea[data-id='txtComment']", $(container)).counter();
				}, function () {
					//reload grid when dialog closed
					var pageIndex = manageVendorGroupAdmin.DATA.grid.getPaginationSetting().page;
					manageVendorGroupAdmin.DATA.grid.setFilter(manageVendorGroupAdmin.FACTORY.getFilterInfo()).setPageIndex(pageIndex).load();
				});
			};

			manageVendorGroupAdmin.FACTORY.ChangeState = function (id, isEnabled, sourceEle, comment, container) {
				var dataObj = { userRoleID: id, comment: comment, lenderConfigID: '<%=LenderConfigID%>', lenderID: '<%=LenderID%>', enable: isEnabled ? "Y" : "N", command: "changeVendorGroupAdminState" }
				$.ajax({
					url: "/sm/smhandler.aspx",
					async: true,
					cache: false,
					type: 'POST',
					dataType: 'html',
					data: dataObj,
					success: function (responseText) {
						var response = $.parseJSON(responseText);
						if (response.IsSuccess) {
							_COMMON.noty("success", "Success", 500);
							var pageIndex = manageVendorGroupAdmin.DATA.grid.getPaginationSetting().page;
							manageVendorGroupAdmin.DATA.grid.setFilter(manageVendorGroupAdmin.FACTORY.getFilterInfo()).setPageIndex(pageIndex).load();
							container.dialog.close();
						} else {
							_COMMON.noty("error", "Error", 500);
							$(sourceEle).prop("checked", !isEnabled);
						}
					}
				});
			};
			manageVendorGroupAdmin.FACTORY.loadAddVendorGroupAdminForm = function () {
				_COMMON.showDialogSaveClose("Add Vendor Group Administrator", 0, "addvendorgroupadmin_dialog_0", "", "/sm/smhandler.aspx", { command: "loadAddVendorGroupAdminForm", lenderConfigID: '<%=LenderConfigID%>' }, manageVendorGroupAdmin.FACTORY.addVendorGroupAdmin, function (container) {
					//add validator
					registerDataValidator($("#addvendorgroupadmin_dialog_0"), "addvendorgroupadmin_dialog_0");
					$("#addvendorgroupadmin_dialog_0 input[data-role=tagsinput]").tagsinput({
						trimValue: true,
						typeahead: {
							ajax: {
								url: '/sm/smhandler.aspx',
								method: 'POST',
								displayField: "name",
								valueField: "id",
								dataType: 'json',
								preDispatch: function() {
									return { query: "", command: "getAvailableTags", lenderID: '<%=LenderID%>' };
								},
								preProcess: function(data) {
									return data;
								}
							}
						}
					});
				}, function () {
					//reload grid when dialog closed
					var pageIndex = manageVendorGroupAdmin.DATA.grid.getPaginationSetting().page;
					manageVendorGroupAdmin.DATA.grid.setFilter(manageVendorGroupAdmin.FACTORY.getFilterInfo()).setPageIndex(pageIndex).load();
				});
			};

			manageVendorGroupAdmin.FACTORY.addVendorGroupAdmin = function (obj, force) {
				if (typeof (force) == "undefined") force = false;
				if ($.smValidate("addvendorgroupadmin_dialog_0") == false) return;
				var vendorGroupAdminData = obj.container.find("form").serializeJSON({ parseAll: true, checkboxUncheckedValue: "false" });
				//update data
				var postData = {
					command: "saveNewVendorGroupAdmin",
					email: vendorGroupAdminData.Email,
					lenderID: '<%=LenderID%>',
					lenderConfigID: '<%=LenderConfigID%>',
					tags: vendorGroupAdminData.Tags,
					role: vendorGroupAdminData.Role,
					force: force
				};
				$.ajax({
					url: "/sm/smhandler.aspx",
					async: true,
					cache: false,
					type: 'POST',
					dataType: 'html',
					data: postData,
					success: function (responseText) {
						var response = $.parseJSON(responseText);
						if (response.IsSuccess) {
							_COMMON.noty("success", "Success", 500);
							obj.dialog.close();
						} else if (response.Info == "ALREADY") {
							_COMMON.showAlertDialog(response.Message);
						} else if (response.Info == "UPGRADE") {
							//show confirm popup
							_COMMON.showConfirmDialog(response.Message, function () {
								obj.dialog.close();
								//yes
								manageVendorGroupAdmin.FACTORY.addVendorGroupAdmin(obj, true);
							}, function () {
								//no
							}, "YES", "NO");
						} else if (response.Info == "DOWNGRADE") {
							//show confirm popup
							_COMMON.showConfirmDialog(response.Message, function () {
								obj.dialog.close();
								//yes
								downgradeToVendorGroupAdmin(vendorGroupAdminData.Email, vendorGroupAdminData.Tags);
							}, function () {
								//no
							}, "YES", "NO");
						} else {
							var errorMsg = response.Message !== "" ? response.Message : "Error";
							_COMMON.noty("error", errorMsg, 500);
						}
					}
				});
			};

			manageVendorGroupAdmin.FACTORY.loadEditTagsForm = function (id, sourceEle) {
				_COMMON.showDialogSaveClose("Edit Assigned Groups", 0, "editassignedgroups_dialog_0", "", "/sm/smhandler.aspx", { command: "loadEditVendorGroupAdminForm", lenderConfigID: '<%=LenderConfigID%>', userRoleID: id }, manageVendorGroupAdmin.FACTORY.updateVendorGroupAdmin, function (container) {
					//add validator
					registerDataValidator($("#editassignedgroups_dialog_0"), "editassignedgroups_dialog_0");
					$("#editassignedgroups_dialog_0 input[data-role=tagsinput]").tagsinput({
						trimValue: true,
						typeahead: {
							ajax: {
								url: '/sm/smhandler.aspx',
								method: 'POST',
								displayField: "name",
								valueField: "id",
								dataType: 'json',
								preDispatch: function () {
									return { query: "", command: "getAvailableTags", lenderID: '<%=LenderID%>' };
								},
								preProcess: function (data) {
									return data;
								}
							}
						}
					});
				}, function () {
					//reload grid when dialog closed
					var pageIndex = manageVendorGroupAdmin.DATA.grid.getPaginationSetting().page;
					manageVendorGroupAdmin.DATA.grid.setFilter(manageVendorGroupAdmin.FACTORY.getFilterInfo()).setPageIndex(pageIndex).load();
				});
			};
			manageVendorGroupAdmin.FACTORY.updateVendorGroupAdmin = function (obj) {
				if ($.smValidate("editassignedgroups_dialog_0") == false) return;
				var vendorGroupAdminData = obj.container.find("form").serializeJSON({ parseAll: true, checkboxUncheckedValue: "false" });
				//update data
				var postData = {
					command: "updateVendorGroupAdmin",
					email: vendorGroupAdminData.Email,
					lenderID: '<%=LenderID%>',
					lenderConfigID: '<%=LenderConfigID%>',
					tags: vendorGroupAdminData.Tags,
					role: vendorGroupAdminData.Role,
					userRoleID: vendorGroupAdminData.UserRoleID
				};
				$.ajax({
					url: "/sm/smhandler.aspx",
					async: true,
					cache: false,
					type: 'POST',
					dataType: 'html',
					data: postData,
					success: function (responseText) {
						var response = $.parseJSON(responseText);
						if (response.IsSuccess) {
							_COMMON.noty("success", "Success", 500);
							obj.dialog.close();
						} else {
							var errorMsg = response.Message !== "" ? response.Message : "Error";
							_COMMON.noty("error", errorMsg, 500);
						}
					}
				});
			};
			function downgradeToVendorGroupAdmin(email, tags) {
				$.ajax({
					url: "/sm/smhandler.aspx",
					async: true,
					cache: false,
					type: 'POST',
					dataType: 'html',
					data: { email: email, tags: tags, command: 'downgradeToVendorGroupAdmin', lenderID: '<%=LenderID%>', lenderConfigID: '<%=LenderConfigID%>' },
					success: function (respText) {
						var resp = $.parseJSON(respText);
						if (resp.IsSuccess) {
							_COMMON.noty("success", "Success.", 500);
							if (resp.Info == "RELOAD") {
								window.location = window.location.href.replace(/#\w*$/, '');
							} else {
								var pIndex = manageVendorGroupAdmin.DATA.grid.getPaginationSetting().page;
								manageVendorGroupAdmin.DATA.grid.setFilter(manageVendorGroupAdmin.FACTORY.getFilterInfo()).setPageIndex(pIndex).load();
								var vIndex = managePortalUserRoles.DATA.grid.getPaginationSetting().page;
								managePortalUserRoles.DATA.grid.setFilter(managePortalUserRoles.FACTORY.getFilterInfo()).setPageIndex(vIndex).load();
							}
						} else {
							_COMMON.noty("error", "Error", 500);
						}
					}
				});
			}

			manageVendorGroupAdmin.FACTORY.search = function () {
				manageVendorGroupAdmin.DATA.grid.setFilter(manageVendorGroupAdmin.FACTORY.getFilterInfo()).setPageIndex(1).load();
			};
		}(window.manageVendorGroupAdmin = window.manageVendorGroupAdmin || {}, jQuery));

		<%End If%>
		<%End If%>
		function registerDataValidator(container, groupName) {
			var $container = $(container);
			$.smValidate.removeValidationGroup(groupName);
			$("[data-required='true'],[data-format]", $container).each(function (idx, ele) {
				$(ele).observer({
					validators: [
						function (partial) {
							var $self = $(this);
							if ($self.data("required") && $.trim($self.val()) === "") {
								if ($self.data("message-require")) {
									return $self.data("message-require");
								} else {
									return "This field is required";
								}
							}
							var format = $self.data("format");
							if (format) {
								var msg = "Invalid format";
								if ($self.data("message-invalid-data")) {
									msg = $self.data("message-invalid-data");
								}
								if (format === "email") {
									if (_COMMON.ValidateEmail($self.val()) === false) {
										return msg;
									}
								} else if (format === "phone") {
									if (_COMMON.ValidatePhone($self.val()) === false) {
										return msg;
									}
								} else if ($self.val() != "" && format === "positive-number") {
									if ((parseInt($self.val()) > 0) == false) {
										return msg;
									}
								}
							}
							return "";
						}
					],
					validateOnBlur: true,
					group: groupName
				});
			});
		}
	</script>
	
</asp:Content>
<asp:Content runat="server" ContentPlaceHolderID="plActionButtons">
	<%--<%If LenderAdminAllowed Then%>
	<div class="top-action-button save-changes-btn"><span>Save Changes</span></div>
	<%End If%>--%>
</asp:Content>