﻿
Partial Class Sm_Index
	Inherits SmApmBasePage
	Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
		If Not Page.IsPostBack Then
			Dim smMasterPage = CType(Me.Master, Sm_SiteManager)
			smMasterPage.LenderConfigXml = LenderConfigXml
			smMasterPage.SelectedMainMenuItem = "home"
			smMasterPage.ShowPreviewButton = False
			smMasterPage.ShowSaveDraftButton = False
			smMasterPage.ShowPublishButton = False
			smMasterPage.HideSubMenuItem = True
		End If
	End Sub

	Protected Function GetPageUrl(pageName As String) As String
		Return String.Format("{0}{1}", pageName, System.Web.Security.AntiXss.AntiXssEncoder.HtmlEncode(Request.Url.Query, True))
	End Function
End Class
