﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="ManageBL.aspx.vb" Inherits="Sm_ManageBL" MasterPageFile="SiteManager.master" %>

<asp:Content runat="server" ContentPlaceHolderID="plBody">
	<div>
		<h1 class="page-header">Manage Business Loan Settings</h1>
		<p>Use this page to manage what’s available for your Application Portal including Purpose Types & Decision Messages, and Disclosures</p>
		<section data-name="CreditCardTypes">
			<h2 class="section-title">Credit Card Types</h2>
			<p>Select which credit card types you want to make available. These types are configured in LoanPQ.</p>
			<div class="checkbox-btn-wrapper">
				<span>CREDIT CARD</span>
				<label class="toggle-btn pull-right" data-on="ON" data-off="OFF">
                    <input type="checkbox" value="CREDIT" <%=BindCheckbox("CREDIT", CardTypeList)%>/>
                    <span class="button-checkbox"></span>
                </label>
			</div>
			<div class="checkbox-btn-wrapper">
				<span>SECURE CREDIT CARD</span>
				<label class="toggle-btn pull-right" data-on="ON" data-off="OFF">
                    <input type="checkbox" value="SECUREDCREDIT" <%=BindCheckbox("SECUREDCREDIT", CardTypeList)%>/>
                    <span class="button-checkbox"></span>
                </label>
			</div>
		</section>
		<section data-name="CreditCardLoanPurposes">
			<h2 class="section-title">Credit Card Purposes</h2>
			<p>Select which purposes you want to make available. Values for these purposes are configured in your In-Branch Configure Site settings.</p>
			<%If BusinessPurposeList IsNot Nothing AndAlso BusinessPurposeList.Any() Then%>
			<%	 For Each item As KeyValuePair(Of String, SmTextValueCatItem) In BusinessPurposeList.OrderBy(Function(p) p.Value.value)%>
			<div class="checkbox-btn-wrapper category-included">
				<span><%=item.Value.text%></span>
				<select class="form-control inline-select <%=IIf(BindCheckbox(item.Value.value, CreditCardEnabledPurposeList) = "checked", "", "hidden")%>">
					<option <%=BindSelectbox(item.Value.category, "")%> value=""></option>
					<option <%=BindSelectbox(item.Value.category, "NEW_CARD")%> value="NEW_CARD">NEW_CARD</option>
					<option <%=BindSelectbox(item.Value.category, "LINE INCREASE")%> value="LINE INCREASE">LINE INCREASE</option>
					<option <%=BindSelectbox(item.Value.category, "CARD UPGRADE")%> value="CARD UPGRADE">CARD UPGRADE</option>
					<option <%=BindSelectbox(item.Value.category, "NEW_CARD | LIMIT")%> value="NEW_CARD | LIMIT">NEW_CARD | LIMIT</option>
				</select>
				<label class="toggle-btn pull-right" data-on="ON" data-off="OFF">
                    <input type="checkbox" data-text="<%=item.Value.text%>" value="<%=item.Value.value%>" <%=BindCheckbox(item.Value.value, CreditCardEnabledPurposeList)%>/>
                    <span class="button-checkbox"></span>
                </label>
			</div>
			<%Next%>
			<%End If%>
		</section>
		<section data-name="VehicleLoanPurposes">
			<h2 class="section-title">Vehicle Loan Purposes</h2>
			<p>Select which purposes you want to make available. Values for these purposes are configured in your In-Branch Configure Site settings.</p>
			<%If BusinessPurposeList IsNot Nothing AndAlso BusinessPurposeList.Any() Then%>
			<%	 For Each item As KeyValuePair(Of String, SmTextValueCatItem) In BusinessPurposeList.OrderBy(Function(p) p.Value.value)%>
			<div class="checkbox-btn-wrapper category-included">
				<span><%=item.Value.text%></span>
				<select class="form-control inline-select <%=IIf(BindCheckbox(item.Value.value, VehicleEnabledPurposeList) = "checked", "", "hidden")%>">
					<option <%=BindSelectbox(item.Value.category, "")%> value=""></option>
					<option <%=BindSelectbox(item.Value.category, "PURCHASE")%> value="PURCHASE">PURCHASE</option>
					<option <%=BindSelectbox(item.Value.category, "REFINANCE")%> value="REFINANCE">REFINANCE</option>
					<option <%=BindSelectbox(item.Value.category, "LEASE")%> value="">LEASE</option>
				</select>
				<label class="toggle-btn pull-right" data-on="ON" data-off="OFF">
                    <input type="checkbox" data-text="<%=item.Value.text%>" value="<%=item.Value.value%>" <%=BindCheckbox(item.Value.value, VehicleEnabledPurposeList)%>/>
                    <span class="button-checkbox"></span>
                </label>
			</div>
			<%Next%>
			<%End If%>
		</section>
		<section data-name="OtherLoanPurposes">
			<h2 class="section-title">Other Loan Purposes</h2>
			<p>Select which purposes you want to make available. Values for these purposes are configured in your In-Branch Configure Site settings.</p>
			<%If BusinessPurposeList IsNot Nothing AndAlso BusinessPurposeList.Any() Then%>
			<%	 For Each item As KeyValuePair(Of String, SmTextValueCatItem) In BusinessPurposeList.OrderBy(Function(p) p.Value.value)%>
			<div class="checkbox-btn-wrapper category-included">
				<span><%=item.Value.text%></span>
				<select class="form-control inline-select <%=IIf(BindCheckbox(item.Value.value, OtherEnabledPurposeList) = "checked", "", "hidden")%>">
					<option <%=BindSelectbox(item.Value.category, "")%> value=""></option>
					<option <%=BindSelectbox(item.Value.category, "UNSECURE")%> value="UNSECURE">UNSECURE</option>
					<option <%=BindSelectbox(item.Value.category, "SECURE")%> value="SECURE">SECURE</option>
				</select>
				<label class="toggle-btn pull-right" data-on="ON" data-off="OFF">
                    <input type="checkbox" data-text="<%=item.Value.text%>" value="<%=item.Value.value%>" <%=BindCheckbox(item.Value.value, OtherEnabledPurposeList)%>/>
                    <span class="button-checkbox"></span>
                </label>
			</div>
			<%Next%>
			<%End If%>
		</section>
		<section>
			<h2 class="section-title">Credit Card Messages</h2>
			<p>These are the messages that display depending on the applicant’s status.</p>
			<div>
				<h3 class="property-title">Approved Message</h3>
				<div class="html-editor" contenteditable="true" data-required="true" placeholder="Enter your approved Message..." id="txtCreditCardLoanPreapprovedMsg"><%=CreditCardPreapprovedMessage%></div>
			</div>
			<div>
				<h3 class="property-title">Referred Message</h3>
				<div class="html-editor" contenteditable="true" data-required="true" placeholder="Enter your referred Message..." id="txtCreditCardLoanSubmittedMsg"><%=CreditCardSubmittedMessage%></div>
			</div>
		</section>
		<section>
			<h2 class="section-title">Vehicle Messages</h2>
			<p>These are the messages that display depending on the applicant’s status.</p>
			<div>
				<h3 class="property-title">Approved Message</h3>
				<div class="html-editor" contenteditable="true" data-required="true" placeholder="Enter your approved Message..." id="txtVehicleLoanPreapprovedMsg"><%=VehiclePreapprovedMessage%></div>
			</div>
			<div>
				<h3 class="property-title">Referred Message</h3>
				<div class="html-editor" contenteditable="true" data-required="true" placeholder="Enter your referred Message..." id="txtVehicleLoanSubmittedMsg"><%=VehicleSubmittedMessage%></div>
			</div>
		</section>
		<section>
			<h2 class="section-title">Other Loan Messages</h2>
			<p>These are the messages that display depending on the applicant’s status.</p>
			<div>
				<h3 class="property-title">Approved Message</h3>
				<div class="html-editor" contenteditable="true" data-required="true" placeholder="Enter your approved Message..." id="txtOtherLoanPreapprovedMsg"><%=OtherPreapprovedMessage%></div>
			</div>
			<div>
				<h3 class="property-title">Referred Message</h3>
				<div class="html-editor" contenteditable="true" data-required="true" placeholder="Enter your referred Message..." id="txtOtherLoanSubmittedMsg"><%=OtherSubmittedMessage%></div>
			</div>
		</section>
		<br />
		<section data-name="ccDisclosures" data-section-type="disclosure">
			<div class="section-heading-btn clearfix">
				<div class="pull-left">
					<h2 class="section-title">Credit Card Disclosures</h2>
					<p>These are the messages that display in Submit & Sign section and require applicant's consent via check boxes. Any stand-alone link, link without text before or after will be displayed as a button in your Application Portal.</p>
				</div>
				<div class="pull-right buttons">
					<button type="button" class="btn btn-primary add-disclosure mb10"><i class="fa fa-plus" aria-hidden="true"></i>Add Disclosure</button>
				</div>
			</div>			
			<%If CreditCardDisclosureList IsNot Nothing AndAlso CreditCardDisclosureList.Any() Then
					Dim idx As Integer = 0%>
			<%For Each disclosure As String In CreditCardDisclosureList%>
			<div class="disclosure-block">
				<div id="ccDisclosureEditor<%=idx%>" data-required="true" class="html-editor" contenteditable="true" placeholder="Enter your disclosure..."><%=disclosure%></div>
				<div class="disclosure-commands">
					<button type="button" class="btn btn-default remove-btn"><i class="fa fa-trash-o" aria-hidden="true"></i>Remove</button>
					<button type="button" class="btn btn-default moveup-btn"><i class="fa fa-chevron-circle-up" aria-hidden="true"></i>Move up</button>
					<button type="button" class="btn btn-default movedown-btn"><i class="fa fa-chevron-circle-down" aria-hidden="true"></i>Move down</button>
				</div>		
			</div>
			<% idx = idx + 1
				Next%>
			<%End If%>
			<div class="no-item <%=IIf(CreditCardDisclosureList Is Nothing OrElse Not CreditCardDisclosureList.Any(), "hidden", "")%>">
				<div>
					<i class="fa fa-file-o" aria-hidden="true"></i>
					<p>These are currently no disclosures added for this loan type.</p>
					<a href="#" class="add-disclosure">Add Disclosure</a>
				</div>
			</div>
		</section>
		<section data-name="vlDisclosures" data-section-type="disclosure">
			<div class="section-heading-btn clearfix">
				<div class="pull-left">
					<h2 class="section-title">Vehicle Loan Disclosures</h2>
					<p>These are the messages that display in Submit & Sign section and require applicant's consent via check boxes. Any stand-alone link, link without text before or after will be displayed as a button in your Application Portal.</p>
				</div>
				<div class="pull-right buttons">
					<button type="button" class="btn btn-primary add-disclosure mb10"><i class="fa fa-plus" aria-hidden="true"></i>Add Disclosure</button>
				</div>
			</div>			
			<%If VehicleDisclosureList IsNot Nothing AndAlso VehicleDisclosureList.Any() Then
					Dim idx As Integer = 0%>
			<%For Each disclosure As String In VehicleDisclosureList%>
			<div class="disclosure-block">
				<div id="vlDisclosureEditor<%=idx%>" data-required="true" class="html-editor" contenteditable="true" placeholder="Enter your disclosure..."><%=disclosure%></div>
				<div class="disclosure-commands">
					<button type="button" class="btn btn-default remove-btn"><i class="fa fa-trash-o" aria-hidden="true"></i>Remove</button>
					<button type="button" class="btn btn-default moveup-btn"><i class="fa fa-chevron-circle-up" aria-hidden="true"></i>Move up</button>
					<button type="button" class="btn btn-default movedown-btn"><i class="fa fa-chevron-circle-down" aria-hidden="true"></i>Move down</button>
				</div>		
			</div>
			<% idx = idx + 1
				Next%>
			<%End If%>
			<div class="no-item <%=IIf(VehicleDisclosureList Is Nothing OrElse Not VehicleDisclosureList.Any(), "hidden", "")%>">
				<div>
					<i class="fa fa-file-o" aria-hidden="true"></i>
					<p>These are currently no disclosures added for this loan type.</p>
					<a href="#" class="add-disclosure">Add Disclosure</a>
				</div>
			</div>
		</section>
		<section data-name="otherDisclosures" data-section-type="disclosure">
			<div class="section-heading-btn clearfix">
				<div class="pull-left">
					<h2 class="section-title">Other Loan Disclosures</h2>
					<p>These are the messages that display in Submit & Sign section and require applicant's consent via check boxes. Any stand-alone link, link without text before or after will be displayed as a button in your Application Portal.</p>
				</div>
				<div class="pull-right buttons">
					<button type="button" class="btn btn-primary add-disclosure mb10"><i class="fa fa-plus" aria-hidden="true"></i>Add Disclosure</button>
				</div>
			</div>			
			<%If OtherDisclosureList IsNot Nothing AndAlso OtherDisclosureList.Any() Then
					Dim idx As Integer = 0%>
			<%For Each disclosure As String In OtherDisclosureList%>
			<div class="disclosure-block">
				<div id="otherDisclosureEditor<%=idx%>" data-required="true" class="html-editor" contenteditable="true" placeholder="Enter your disclosure..."><%=disclosure%></div>
				<div class="disclosure-commands">
					<button type="button" class="btn btn-default remove-btn"><i class="fa fa-trash-o" aria-hidden="true"></i>Remove</button>
					<button type="button" class="btn btn-default moveup-btn"><i class="fa fa-chevron-circle-up" aria-hidden="true"></i>Move up</button>
					<button type="button" class="btn btn-default movedown-btn"><i class="fa fa-chevron-circle-down" aria-hidden="true"></i>Move down</button>
				</div>		
			</div>
			<% idx = idx + 1
				Next%>
			<%End If%>
			<div class="no-item <%=IIf(OtherDisclosureList Is Nothing OrElse Not OtherDisclosureList.Any(), "hidden", "")%>">
				<div>
					<i class="fa fa-file-o" aria-hidden="true"></i>
					<p>These are currently no disclosures added for this loan type.</p>
					<a href="#" class="add-disclosure">Add Disclosure</a>
				</div>
			</div>
		</section>
	</div>
</asp:Content>
<asp:Content runat="server" ContentPlaceHolderID="plScripts">
	<script type="text/javascript" src="js/ckeditor/ckeditor.js"></script>
	<script type="text/javascript">
		CKEDITOR.disableAutoInline = true;
		$(function () {
			$("section[data-name=vlDisclosures],section[data-name=ccDisclosures],section[data-name=otherDisclosures]").on("change", function () {
				$(this).find(".disclosure-commands button").show();
				var $firstBlock = $(this).find(".disclosure-block:first");
				$firstBlock.find("button.moveup-btn").hide();
				var $lastBlock = $(this).find(".disclosure-block:last");
				$lastBlock.find("button.movedown-btn").hide();
				if ($(this).find(".disclosure-block").length == 0) {
					$(this).find(".no-item").removeClass("hidden");
				} else {
					$(this).find(".no-item").addClass("hidden");
				}
			}).trigger("change");
			$(".html-editor").ckEditor({
				onContentChanged: function (editor) { master.FACTORY.documentChanged(editor); },
				onCommandClicked: function (editor, commandBtn) { master.FACTORY.documentChanged(editor); }
			});

			$(".add-disclosure").click(function (e) {
				var $section = $(this).closest("section");
				var htmlStr = '<div class="disclosure-block"><div id="disclosureEditor' + new Date().getTime() + '" class="html-editor" data-required="true" contenteditable="true" placeholder="Enter your disclosure..."></div><div class="disclosure-commands"><button type="button" class="btn btn-default remove-btn"><i class="fa fa-trash-o" aria-hidden="true"></i>Remove</button><button type="button" class="btn btn-default moveup-btn"><i class="fa fa-chevron-circle-up" aria-hidden="true"></i>Move up</button><button type="button" class="btn btn-default movedown-btn"><i class="fa fa-chevron-circle-down" aria-hidden="true"></i>Move down</button></div></div>';
				var $item = $(htmlStr).insertBefore($(".no-item", $section));
				$item.find(".html-editor").ckEditor({
					onContentChanged: function (editor) { master.FACTORY.documentChanged(editor); },
					onCommandClicked: function (editor, commandBtn) { master.FACTORY.documentChanged(editor); }
				});
				$(this).closest("section").trigger("change");
				$('html, body').scrollTop($(this).closest("section").offset().top - 40);
				setTimeout(function() {
					$item.find(".html-editor").focus().trigger("focus");
				}, 200);
				e.preventDefault();
				registerDataValidator();
			});
			registerDataValidator();
			$("select,input:checkbox", "div.checkbox-btn-wrapper").on("change", function () {
				var $self = $(this);
				master.FACTORY.documentChanged(this);
				if ($self.is(":checkbox")) {
					if ($self.is(":checked")) {
						$self.closest("div.checkbox-btn-wrapper").find("select.inline-select").removeClass("hidden");
					} else {
						$self.closest("div.checkbox-btn-wrapper").find("select.inline-select").addClass("hidden");
					}
				}
			});
		});
		(function (master, $, undefined) {
			master.FACTORY.SaveDraft = function (revisionId) {
				if ($.smValidate("ValidateData") == false) return;
				var dataObj = collectSubmitData();
				dataObj.command = "savemanagebl";
				dataObj.revisionId = revisionId;
				$.ajax({
					url: "/sm/smhandler.aspx",
					async: true,
					cache: false,
					type: 'POST',
					dataType: 'html',
					data: dataObj,
					success: function (responseText) {
						var response = $.parseJSON(responseText);
						if (response.IsSuccess) {
							master.FACTORY.onSaveDraftSuccess();
						} else {
							_COMMON.noty("error", "Error", 500);
						}
					}
				});
			};
			master.FACTORY.Preview = function (revisionId) {
				if ($.smValidate("ValidateData") == false) return;
				var dataObj = collectSubmitData();
				dataObj.command = "previewmanagebl";
				dataObj.revisionId = revisionId;
				$.ajax({
					url: "/sm/smhandler.aspx",
					async: true,
					cache: false,
					type: 'POST',
					dataType: 'html',
					data: dataObj,
					success: function (responseText) {
						var response = $.parseJSON(responseText);
						if (response.IsSuccess) {
							master.FACTORY.onPreviewSuccess();
							_COMMON.openInNewTab(response.Info.previewurl);
						} else {
							_COMMON.noty("error", "Error", 500);
						}
					}
				});
			};
			master.FACTORY.validateBeforePublish = function () {
				return $.smValidate("ValidateData");
			}
			master.FACTORY.Publish = function (comment, publishAll) {
				if ($.smValidate("ValidateData") == false) return;
				var dataObj = collectSubmitData();
				dataObj.command = 'publishmanagebl';
				dataObj.comment = comment;
				dataObj.publishAll = publishAll;
				$.ajax({
					url: "/sm/smhandler.aspx",
					async: true,
					cache: false,
					type: 'POST',
					dataType: 'html',
					data: dataObj,
					success: function (responseText) {
						var response = $.parseJSON(responseText);
						if (response.IsSuccess) {
							master.FACTORY.onPublishSuccess();
						} else {
							_COMMON.noty("error", "Error", 500);
						}
					}
				});
			};
		}(window.master = window.master || {}, jQuery));
		function registerDataValidator() {
			$.smValidate.removeValidationGroup("ValidateData");
			$("[data-required='true']").each(function (idx, ele) {
				$(ele).observer({
					validators: [
						function (partial) {
							var $self = $(this);
							var content = "";
							if ($self.hasClass("html-editor")) {
								content = CKEDITOR.instances[$self.attr("id")].getData();
								if (content == $self.attr("placeholder")) content = "";
							} else if ($self.hasClass("sm-textarea")) {
								content = $self.val();
							}
							if ($.trim(content) === "") return "This field is required";
							return "";
						}
					],
					validateOnBlur: true,
					group: "ValidateData"
				});
			});
		}
		function collectSubmitData() {
			var result = {};
			result.vlPreapprovedMsg = _COMMON.getEditorValue("txtVehicleLoanPreapprovedMsg");
			result.vlSubmittedMsg = _COMMON.getEditorValue("txtVehicleLoanSubmittedMsg");
			result.ccPreapprovedMsg = _COMMON.getEditorValue("txtCreditCardLoanPreapprovedMsg");
			result.ccSubmittedMsg = _COMMON.getEditorValue("txtCreditCardLoanSubmittedMsg");
			result.otherPreapprovedMsg = _COMMON.getEditorValue("txtOtherLoanPreapprovedMsg");
			result.otherSubmittedMsg = _COMMON.getEditorValue("txtOtherLoanSubmittedMsg");
			result.ccDisclosures = JSON.stringify(_COMMON.collectDisclosureData($("section[data-name=ccDisclosures]")));
			result.vlDisclosures = JSON.stringify(_COMMON.collectDisclosureData($("section[data-name=vlDisclosures]")));
			result.otherDisclosures = JSON.stringify(_COMMON.collectDisclosureData($("section[data-name=otherDisclosures]")));
			result.lenderconfigid = '<%=LenderConfigID%>';
			result.cardTypes = JSON.stringify(_COMMON.collectCheckboxGroupData($("section[data-name='CreditCardTypes']")));
			result.ccPurposes = JSON.stringify(_COMMON.collectTextValueCatCheckboxGroupData($("section[data-name='CreditCardLoanPurposes']")));
			result.vlPurposes = JSON.stringify(_COMMON.collectTextValueCatCheckboxGroupData($("section[data-name='VehicleLoanPurposes']")));
			result.otherPurposes = JSON.stringify(_COMMON.collectTextValueCatCheckboxGroupData($("section[data-name='OtherLoanPurposes']")));
			return result;
		}
	</script>
</asp:Content>
<asp:Content runat="server" ContentPlaceHolderID="plActionButtons"></asp:Content>