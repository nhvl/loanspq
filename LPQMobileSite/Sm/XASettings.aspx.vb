﻿Imports Sm
Imports System.Xml
Imports LPQMobile.Utils
Partial Class Sm_XASettings
	Inherits SmApmBasePage

    Protected Property XAUploadDocEnable As Boolean = True  'set this to true for backward compatible, featue only visible if both XAUploadDocEnable and XAUploadDocMsg <> ""
    Protected Property XASpecialUploadDocEnable As Boolean = False
    Protected Property XABusinessUploadDocEnable As Boolean = False
    Protected Property XAUploadDocMsg As String
    Protected Property XASpecialUploadDocMsg As String
    Protected Property XABusinessUploadDocMsg As String
    Protected Property XAJointEnable As Boolean = True
	Protected Property XACollectJobTitleOnly As Boolean = False
	Protected Property SAJointEnable As Boolean = True
	Protected Property XALocationPool As Boolean = False
	Protected Property Debit As String
	Protected Property Ida As String
	Protected Property CreditPull As Boolean = False
	Protected Property DecisionXAEnabled As Boolean = False

	''TODO: get config from xml
	Protected Property XALoanStatus As String
	Protected Property XAFunding As Boolean = False
	Protected Property XAFundingAccountNumberFreeForm As Boolean = False
	Protected Property XABooking As String

	'Protected Property XAXSell As Boolean = False
	Protected Property XADocuSign As Boolean = False
	Protected Property XADocuSignPDFGroup As String
    Protected Property XAPreviousAddressEnable As Boolean = False
    Protected Property XAPreviousAddressThreshold As String = ""
    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
		If Not EnableXA Then
			Response.Redirect(BuildUrl("/sm/generalsettings.aspx"), True)
		End If
		If Not Page.IsPostBack Then
			Dim smMasterPage = CType(Me.Master, Sm_SiteManager)
			smMasterPage.LenderConfigXml = LenderConfigXml
			smMasterPage.RevisionID = LenderConfig.RevisionID
			smMasterPage.LastModifiedBy = LenderConfig.LastModifiedByUserFullName
			smMasterPage.LastModifiedByUserID = LenderConfig.LastModifiedByUserID
			smMasterPage.SelectedMainMenuItem = "config"
			smMasterPage.SelectedSubMenuItem = "settings"
			smMasterPage.CurrentNodeGroup = SmSettings.NodeGroup.XaSettings
			smMasterPage.BreadCrumb = "Configure / Settings / XA"

			Dim xaUploadDocEnableNode As XmlNode = GetNode(XA_UPLOAD_DOC_ENABLE())
            If xaUploadDocEnableNode IsNot Nothing Then
                XAUploadDocEnable = Not xaUploadDocEnableNode.Value.ToUpper().Equals("N")
            End If
            Dim xaUploadDocNode As XmlNode = GetNode(XA_UPLOAD_DOC_MESSAGE())
            If xaUploadDocNode IsNot Nothing Then
                XAUploadDocMsg = HttpUtility.JavaScriptStringEncode(xaUploadDocNode.InnerText.Trim())
            End If

            Dim xaSpecialUploadDocEnableNode As XmlNode = GetNode(XA_SPECIAL_UPLOAD_DOC_ENABLE())
            If xaSpecialUploadDocEnableNode IsNot Nothing Then
                XASpecialUploadDocEnable = Not xaSpecialUploadDocEnableNode.Value.ToUpper().Equals("N")
            End If
            Dim xaSpecialUploadDocNode As XmlNode = GetNode(XA_SPECIAL_UPLOAD_DOC_MESSAGE())
            If xaSpecialUploadDocNode IsNot Nothing Then
                XASpecialUploadDocMsg = HttpUtility.JavaScriptStringEncode(xaSpecialUploadDocNode.InnerText.Trim())
            End If

            Dim xaBusinessUploadDocEnableNode As XmlNode = GetNode(XA_BUSINESS_UPLOAD_DOC_ENABLE())
            If xaBusinessUploadDocEnableNode IsNot Nothing Then
                XABusinessUploadDocEnable = Not xaBusinessUploadDocEnableNode.Value.ToUpper().Equals("N")
            End If
            Dim xaBusinessUploadDocNode As XmlNode = GetNode(XA_BUSINESS_UPLOAD_DOC_MESSAGE())
            If xaBusinessUploadDocNode IsNot Nothing Then
                XABusinessUploadDocMsg = HttpUtility.JavaScriptStringEncode(xaBusinessUploadDocNode.InnerText.Trim())
            End If



            Dim xaJointEnableNode As XmlNode = GetNode(XA_JOINT_ENABLE())
			If xaJointEnableNode IsNot Nothing Then
				XAJointEnable = Not xaJointEnableNode.Value.ToUpper().Equals("N")
			End If

			Dim saJointEnableNode As XmlNode = GetNode(SA_JOINT_ENABLE())
			If saJointEnableNode IsNot Nothing Then
				SAJointEnable = Not saJointEnableNode.Value.ToUpper().Equals("N")
			Else
				'Note: if there is no joint_enable_sa the App Portal would also read from joint_enable to see if secondary accounts should allow joint
				SAJointEnable = XAJointEnable
			End If

			Dim xaCollectJobTitleOnlyNode As XmlNode = GetNode(XA_COLLECT_JOB_TITLE_ONLY)
			If xaCollectJobTitleOnlyNode IsNot Nothing Then
				XACollectJobTitleOnly = xaCollectJobTitleOnlyNode.Value.ToUpper().Equals("Y")
			End If

			Dim xaLocationPoolNode As XmlNode = GetNode(XA_LOCATION_POOL())
			If xaLocationPoolNode IsNot Nothing Then
				XALocationPool = Not xaLocationPoolNode.Value.ToUpper().Equals("N")
			End If

			Dim debitNode As XmlNode = GetNode(Sm.DEBIT())
			If debitNode IsNot Nothing Then
				Debit = debitNode.Value
			End If
			Dim idaNode As XmlNode = GetNode(Sm.IDA())
			If idaNode IsNot Nothing Then
				Ida = idaNode.Value
			End If

			Dim creditPullNode As XmlNode = GetNode(CREDIT_PULL())
			If creditPullNode IsNot Nothing Then
				CreditPull = creditPullNode.Value.ToUpper().Equals("Y")
			End If

			Dim decisionXAEnabledNode As XmlNode = GetNode(DECISIONXA_ENABLE())
			If decisionXAEnabledNode IsNot Nothing Then
				DecisionXAEnabled = decisionXAEnabledNode.Value.ToUpper().Equals("Y")
			End If

			'---
			Dim XAStatusNode As XmlNode = GetNode(XA_STATUS())
			If XAStatusNode IsNot Nothing Then
				XALoanStatus = XAStatusNode.Value
			End If

			Dim fundingXAEnabledNode As XmlNode = GetNode(XA_FUNDING_ENABLE())
			If fundingXAEnabledNode IsNot Nothing Then
				XAFunding = fundingXAEnabledNode.Value.ToUpper().Equals("Y")
			End If

			Dim fundingXAAccountNumberFreeFormEnabledNode As XmlNode = GetNode(XA_FUNDING_ACCOUNT_NUMBER_FREE_FORM())
			If fundingXAAccountNumberFreeFormEnabledNode IsNot Nothing Then
				XAFundingAccountNumberFreeForm = fundingXAAccountNumberFreeFormEnabledNode.Value.ToUpper().Equals("Y")
			End If

			Dim XABookingNode As XmlNode = GetNode(XA_BOOKING())
			If XABookingNode IsNot Nothing Then
				XABooking = XABookingNode.Value
			End If
			'Dim xsellXAEnabledNode As XmlNode = GetNode(XSELL("XA_LOAN"))
			'If xsellXAEnabledNode IsNot Nothing Then
			'	XAXSell = xsellXAEnabledNode.Value.ToUpper().Equals("Y")
			'End If

			Dim docusignXAEnabledNode As XmlNode = GetNode(DOCU_SIGN("XA_LOAN"))
			If docusignXAEnabledNode IsNot Nothing Then
				XADocuSign = docusignXAEnabledNode.Value.ToUpper().Equals("Y")
			End If

			Dim docusignPDFGroupNode As XmlNode = GetNode(DOCU_SIGN_PDF_GROUP("XA_LOAN"))
            If docusignPDFGroupNode IsNot Nothing Then
                XADocuSignPDFGroup = docusignPDFGroupNode.Value.Trim()
            End If

            Dim xaPreviousAddressThresholdNode As XmlNode = GetNode(PREVIOUS_ADDRESS_THRESHOLD("XA_LOAN"))
            If xaPreviousAddressThresholdNode IsNot Nothing AndAlso Common.SafeString(xaPreviousAddressThresholdNode.Value) <> "" Then
                XAPreviousAddressThreshold = Common.SafeString(xaPreviousAddressThresholdNode.Value)
                XAPreviousAddressEnable = True
            End If
        End If
	End Sub

End Class
