﻿Imports Sm
Imports System.Xml
Imports LPQMobile.Utils

Partial Class Sm_HESettings
	Inherits SmApmBasePage
	Private Const LOAN_NODE_NAME As String = "HOME_EQUITY_LOAN"
	Protected Property HEJointEnable As Boolean = True
	Protected Property HEBooking As String
	Protected Property HEXSell As Boolean = False
	Protected Property HEDocuSign As Boolean = False
	Protected Property HEDocuSignPDFGroup As String
	Protected Property HEInterestRate As String = "2.5"
	Protected Property HELocationPool As Boolean = False
    Protected Property HEPreviousAddressEnable As Boolean = False
    Protected Property HEPreviousEmploymentEnable As Boolean = False
    Protected Property HEPreviousAddressThreshold As String = ""
    Protected Property HEPreviousEmploymentThreshold As String = ""
    Protected Property DeclarationList As Dictionary(Of String, SmDeclarationItem)

	Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
		If Not EnableHE Then
			Response.Redirect(BuildUrl("/sm/generalsettings.aspx"), True)
		End If
		If Not Page.IsPostBack Then
			Dim smMasterPage = CType(Me.Master, Sm_SiteManager)
			smMasterPage.LenderConfigXml = LenderConfigXml
			smMasterPage.RevisionID = LenderConfig.RevisionID
			smMasterPage.LastModifiedBy = LenderConfig.LastModifiedByUserFullName
			smMasterPage.LastModifiedByUserID = LenderConfig.LastModifiedByUserID
			smMasterPage.SelectedMainMenuItem = "config"
			smMasterPage.SelectedSubMenuItem = "settings"
			smMasterPage.CurrentNodeGroup = SmSettings.NodeGroup.HESettings
			smMasterPage.BreadCrumb = "Configure / Settings / Home Equity Loan"
			Dim HEBookingNode As XmlNode = GetNode(Sm.LOAN_BOOKING(LOAN_NODE_NAME))
			If HEBookingNode IsNot Nothing Then
				HEBooking = HEBookingNode.Value
			End If

			Dim HExsellEnabledNode As XmlNode = GetNode(XSELL(LOAN_NODE_NAME))
			If HExsellEnabledNode IsNot Nothing Then
				HEXSell = HExsellEnabledNode.Value.ToUpper().Equals("Y")
			End If

			Dim HEdocusignEnabledNode As XmlNode = GetNode(DOCU_SIGN(LOAN_NODE_NAME))
			If HEdocusignEnabledNode IsNot Nothing Then
				HEDocuSign = HEdocusignEnabledNode.Value.ToUpper().Equals("Y")
			End If

			Dim HEdocusignPDFGroupNode As XmlNode = GetNode(DOCU_SIGN_PDF_GROUP(LOAN_NODE_NAME))
			If HEdocusignPDFGroupNode IsNot Nothing Then
				HEDocuSignPDFGroup = HEdocusignPDFGroupNode.Value.Trim()
			End If
			Dim customListRateNode As XmlNode = GetNode(CUSTOM_LIST_RATES("he"))
			If customListRateNode IsNot Nothing Then
				If customListRateNode.Name = "RATES" Then
					customListRateNode = customListRateNode.SelectSingleNode("ITEM[@loan_type='he']")
				End If
				Dim heRateNode As XmlNode = customListRateNode.Attributes("rate")
				If heRateNode IsNot Nothing Then
					HEInterestRate = heRateNode.Value
				End If
			End If
			Dim heJointEnableNode As XmlNode = GetNode(HE_JOINT_ENABLE())
			If heJointEnableNode IsNot Nothing Then
				HEJointEnable = Not heJointEnableNode.Value.ToUpper().Equals("N")
			End If
			Dim HELocationPoolNode As XmlNode = GetNode(LOAN_LOCATION_POOL(LOAN_NODE_NAME))
			If HELocationPoolNode IsNot Nothing Then
				HELocationPool = HELocationPoolNode.Value.ToUpper().Equals("Y")
			End If

			Dim loanDeclarationList = Common.GetAllDeclarations(LOAN_NODE_NAME)
            If loanDeclarationList IsNot Nothing AndAlso loanDeclarationList.Count > 0 Then
                DeclarationList = (From item In loanDeclarationList Select New SmDeclarationItem() With {.Key = item.Key, .DisplayText = item.Value, .Active = False}).ToDictionary(Function(p) p.Key, Function(p) p)
                Dim declarationsDraftNode As XmlNode = GetNode(LOAN_DECLARATIONS(LOAN_NODE_NAME))
                If declarationsDraftNode IsNot Nothing Then
                    Dim declarationDraftNodes = declarationsDraftNode.SelectNodes("DECLARATION")
                    If declarationDraftNodes IsNot Nothing AndAlso declarationDraftNodes.Count > 0 Then
                        For Each node As XmlNode In declarationDraftNodes
                            Dim key As String = node.Attributes("key").Value
                            If DeclarationList.ContainsKey(key) Then
                                DeclarationList.Item(key).Active = True
                            End If
                        Next
                    End If
                End If
            End If

            Dim hePreviousEmploymentThresholdNode As XmlNode = GetNode(PREVIOUS_EMPLOYMENT_THRESHOLD("HOME_EQUITY_LOAN"))
            If hePreviousEmploymentThresholdNode IsNot Nothing AndAlso Common.SafeString(hePreviousEmploymentThresholdNode.Value) <> "" Then
                HEPreviousEmploymentThreshold = Common.SafeString(hePreviousEmploymentThresholdNode.Value)
                HEPreviousEmploymentEnable = True
            End If
            Dim hePreviousAddressThresholdNode As XmlNode = GetNode(PREVIOUS_ADDRESS_THRESHOLD("HOME_EQUITY_LOAN"))
            If hePreviousAddressThresholdNode IsNot Nothing AndAlso Common.SafeString(hePreviousAddressThresholdNode.Value) <> "" Then
                HEPreviousAddressThreshold = Common.SafeString(hePreviousAddressThresholdNode.Value)
                HEPreviousAddressEnable = True
            End If
        End If
	End Sub

End Class
