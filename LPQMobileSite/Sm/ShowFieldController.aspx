﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="ShowFieldController.aspx.vb" Inherits="Sm_ShowFieldController"  MasterPageFile="SiteManager.master" %>
<%@ Import Namespace="Newtonsoft.Json" %>

<asp:Content runat="server" ContentPlaceHolderID="plBody">
	<section class="design-theme-section two-row-button">
		<div class="collapse-able switch-btn-panel <%=IIf(PreviewUrlList.Count < 2, "hidden", "")%>">
			<div class="preview-url-ddl">
				<span>Select application type:</span>
			<select class="form-control inline-select" id="ddlPreviewList"><%	For Each item As KeyValuePair(Of String, String) In PreviewUrlList%>
				<option data-url="<%=GetNavigateUrl(item.Key)%>" value="<%=item.Key%>"><%=GetModuleName(item.Key)%></option>
				<%Next%></select>
			</div>
			<div>
				<div class="btn-group" role="group" aria-label="...">
					<%	For Each item As KeyValuePair(Of String, String) In PreviewUrlList.Where(Function(p) Not p.Key.StartsWith("XA") And Not p.Key.StartsWith("SA"))%>
					<a href="<%=GetNavigateUrl(item.Key)%>" data-id="<%=item.Key%>" class="btn btn-default"><%=GetModuleName(item.Key)%></a>
					<%Next%>
				</div>
			</div>
			<div>
				<div class="btn-group" role="group" aria-label="...">
					<%	For Each item As KeyValuePair(Of String, String) In PreviewUrlList.Where(Function(p) p.Key.StartsWith("XA") OrElse p.Key.StartsWith("SA"))%>
					<a href="<%=GetNavigateUrl(item.Key)%>" data-id="<%=item.Key%>" class="btn btn-default"><%=GetModuleName(item.Key)%></a>
					<%Next%>
				</div>	
			</div>
		</div>
		<div class="preview-panel">
			<div class="frame-wrapper">
				<div class="spinner">
					<div class="rect1"></div>
					<div class="rect2"></div>
					<div class="rect3"></div>
					<div class="rect4"></div>
					<div class="rect5"></div>
				</div>
				<p>Loading...</p>
		    </div>
			<div id="divPrevPage" class="editor-nav-button hidden">
				<a href="javascript:void(0);">
					<i class="fa fa-chevron-circle-left"></i>
					<span>Previous Page</span>
				</a>
			</div>
			<div id="iframeContainer" style="width: 100%; height: 100%">

			</div>
			<div id="divNextPage" class="editor-nav-button hidden">
				<a href="javascript:void(0);">
					<i class="fa fa-chevron-circle-right"></i>
					<span>Next Page</span>
				</a>
			</div>
		</div>
		<div class="design-panel mt50">
			<h1 class="page-header">Visibility</h1>
			<p>To show or hide the specified section, click the eye icon.  If both eye icons are gray, this means the system is using default visibility, which is depicted by the plug icon when mouse hover.  Once you're ready to publish, hit the Publish button, or click "Save Draft" to save your work.  
				<br/>The below column is a summary and only shows fields that have been overridden with user selection. </p>
			<div id="divShowFieldContainer"></div> 
		</div>
	</section>
	<div></div>
</asp:Content>
<asp:Content runat="server" ContentPlaceHolderID="head">
</asp:Content>
<asp:Content runat="server" ContentPlaceHolderID="plScripts">
	<script type="text/javascript" src="js/easyxdm/easyXDM.debug.js"></script>
	<script type="text/javascript">
		var previewUrlArr = <%=JsonConvert.SerializeObject(PreviewUrlList)%>;
		var visibilityWarnings = <%=JsonConvert.SerializeObject(VisibilityWarnings)%>;
		var frameReadyCalled = false;

        var initFrame = function (id) {
			$(".preview-panel .frame-wrapper").show();
			$("#iframeContainer iframe").remove();
			$("a.btn.active", ".switch-btn-panel").removeClass("active");
			$("a[data-id='" + id + "']", ".switch-btn-panel").addClass("active");
			$("#ddlPreviewList").val(id);

			if (id == "LQB_MAIN") {
				$("#divPrevPage, #divNextPage").removeClass("hidden");
				$(".preview-panel, .design-panel").addClass("expanded-navigation-buttons");
			} else {
				$("#divPrevPage, #divNextPage").addClass("hidden");
				$(".preview-panel, .design-panel").removeClass("expanded-navigation-buttons");
			}

			frameReadyCalled = false;

			var rpcSetup = {
				remote: previewUrlArr[id],
				container: document.getElementById("iframeContainer"), // the path to the provider
			};
			var _rpc = new easyXDM.Rpc(rpcSetup,
			{
				local: {
					frameReady: function(data) {
						rpcSetup.container.getElementsByTagName("iframe")[0].width = "100%";
						rpcSetup.container.getElementsByTagName("iframe")[0].height = "100%";
                        $(".preview-panel .frame-wrapper").hide();                   
						if (id != "XA_BUSINESS" && id != "SA_BUSINESS" && id != "XA_SPECIAL" && id != "SA_SPECIAL" || id == "LQB_MAIN") {
							rpc.initShowField();
						}

						if (!frameReadyCalled) {
							// Reload the item list for the first time the frame is ready (i.e., on initial APM page load or switching the tabs)
							loadShowFieldItemList(id);
							frameReadyCalled = true;
						} else {
							// Else, don't reload the rename item list, but do tell the page what the update list is, to refresh the frame.
							$("div.checkbox-btn-wrapper", "#divShowFieldContainer").each(function(idx, row) {
								var $chk = $(row).find(".toggle-btn input:checkbox");
								var state;
								if ($(row).hasClass("hidden")) {
									state = null;
								} else {
									state = $chk.is(":checked");
								}                  
								rpc.updateShowField({controllerId: $chk.val(), isVisible: state});	
							});
						}
					},
					updateShowFieldList: function(data) {
						master.FACTORY.updateShowFieldList(data);
					},
					submitForm: function(data) {
						console.log(data);
						rpc = submitFrame(data, id);
					},
					triggerUpdateShowField: function(data) {
						var $rowItem = $("div.checkbox-btn-wrapper[data-controller-id='" + data.controllerId + "']", "#divShowFieldContainer");
						if ($rowItem.length > 0) {
							var $chk = $rowItem.find(".toggle-btn input:checkbox");
							if ($rowItem.hasClass("hidden")) {
								data.isVisible = null;
							} else {
								data.isVisible = $chk.is(":checked");
							}
							
							rpc.updateShowField(data);
						}
					},
					canGoToPreviousQuickNavPage: function (bValue) {
						if (bValue == true) {
							$("#divPrevPage a").removeClass("disabled");
						} else {
							$("#divPrevPage a").addClass("disabled");
						}
						
					},
					canGoToNextQuickNavPage: function (bValue) {
						if (bValue == true) {
							$("#divNextPage a").removeClass("disabled");
						} else {
							$("#divNextPage a").addClass("disabled");
						}
					},
				},
				remote: {
					initShowField:{},
					updateShowField:{},
					/*setBeneficiarySsnInitialState: {},
					setBeneficiaryTrustInitialState: {}*/
					goToPreviousQuickNavPage: {},
					goToNextQuickNavPage: {},
				}
			});
			return _rpc;
		}
		var submitFrame = function(url, id) {
			$(".preview-panel .frame-wrapper").show();
			$("#iframeContainer iframe").remove();
			var rpcSetup = {
				remote: url,
				container: document.getElementById("iframeContainer"), // the path to the provider
            };       
			var _rpc = new easyXDM.Rpc(rpcSetup,
			{
				local: {
					frameReady: function(data) {
						rpcSetup.container.getElementsByTagName("iframe")[0].width = "100%";
						rpcSetup.container.getElementsByTagName("iframe")[0].height = "100%";
                        $(".preview-panel .frame-wrapper").hide();                 
						if (id == "XA_BUSINESS" || id == "SA_BUSINESS" || id != "XA_SPECIAL" || id != "SA_SPECIAL" || id=="BL" || id == "LQB_MAIN") {
							rpc.initShowField();
							$("div.checkbox-btn-wrapper", "#divShowFieldContainer").each(function(idx, row) {
								initShowFieldListItem(row);
								var $chk = $(row).find(".toggle-btn input:checkbox");
								var state;
								if ($(row).hasClass("hidden")) {
									state = null;
								} else {
									state = $chk.is(":checked");
								}
								rpc.updateShowField({controllerId: $chk.val(), isVisible: state});	
							});
						}
					},
					updateShowFieldList: function(data) {
						master.FACTORY.updateShowFieldList(data);
					},
					submitForm: function(data) {			
						rpc = submitFrame(data, id);
					},
					triggerUpdateShowField: function(data) {
						var $rowItem = $("div.checkbox-btn-wrapper[data-controller-id='" + data.controllerId + "']", "#divShowFieldContainer");
						if ($rowItem.length > 0) {
							var $chk = $rowItem.find(".toggle-btn input:checkbox");
							if ($rowItem.hasClass("hidden")) {
								data.isVisible = null;
							} else {
								data.isVisible = $chk.is(":checked");
							}
							
							rpc.updateShowField(data);
						}
					},
					canGoToPreviousQuickNavPage: function (bValue) {
						if (bValue == true) {
							$("#divPrevPage a").removeClass("disabled");
						} else {
							$("#divPrevPage a").addClass("disabled");
						}
						
					},
					canGoToNextQuickNavPage: function (bValue) {
						if (bValue == true) {
							$("#divNextPage a").removeClass("disabled");
						} else {
							$("#divNextPage a").addClass("disabled");
						}
					},
				},
				remote: {
					initShowField:{},
					updateShowField:{},
					/*setBeneficiarySsnInitialState: {},
					setBeneficiaryTrustInitialState: {}*/
					goToPreviousQuickNavPage: {},
					goToNextQuickNavPage: {},
				}
			});
			return _rpc;
		}
		var initItem = master.FACTORY.getParaByName("loantype");
		if (initItem == "") {
			initItem = "<%=DefaultLoanType%>";
		}
	var rpc = initFrame(initItem);

	$(function () {
		//$("a.btn", ".switch-btn-panel").each(function(idx, ele) {
		//	var $self = $(ele);
		//	$self.on("click", function(e) {
		//		if ($("body").data("hasChanged") == true) {
		//			if (confirm("The document contains unsaved changes. Do you want to leave?") == false) {
		//				e.preventDefault();
		//				return false;
		//			} else {
		//				$("body").data("hasChanged", false);
		//			}
		//		}
		//		//window.location.reload();
		//		//var key = $self.attr("href").replace("#", "");
		//		//rpc = initFrame(key.toUpperCase());
		//	});
		//});
		var lastPreviewUrlSelected; 
		$("#ddlPreviewList").on("click", function() {
			lastPreviewUrlSelected = $(this).val();
		}).on("change", function() {
			if ($("body").data("hasChanged") == true) {
				if (confirm("The document contains unsaved changes. Do you want to leave?") == false) {
					$(this).val(lastPreviewUrlSelected);

				} else {
					$("body").data("hasChanged", false);
					window.location = $(this).find("option:selected").data("url");
				}
			} else {
				window.location = $(this).find("option:selected").data("url");
			}
		});
		$("#divPrevPage").on("click", function () {
			$("#divPrevPage, #divPrevPage *").blur();
			rpc.goToPreviousQuickNavPage();
		});
		$("#divNextPage").on("click", function () {
			$("#divNextPage, #divNextPage *").blur();
			rpc.goToNextQuickNavPage();
		});
	});
	(function (master, $, undefined) {
		master.FACTORY.SaveDraft = function (revisionId) {
			//if ($.smValidate("ValidateData") == false) return;
			var dataObj = collectSubmitData();
			dataObj.revisionId = revisionId;
			dataObj.command = "saveShowFieldItem";
			$.ajax({
				url: "/sm/smhandler.aspx",
				async: true,
				cache: false,
				type: 'POST',
				dataType: 'html',
				data: dataObj,
				success: function (responseText) {
					var response = $.parseJSON(responseText);
					if (response.IsSuccess) {
						master.FACTORY.onSaveDraftSuccess();
					} else {
						_COMMON.noty("error", "Error", 500);
					}
				}
			});
		};
		master.FACTORY.Publish = function (comment, publishAll) {
			//if ($.smValidate("ValidateData") == false) return;
			var dataObj = collectSubmitData();
			dataObj.comment = comment; 
			dataObj.publishAll = publishAll;
			dataObj.command = "publishShowFieldItem";
			$.ajax({
				url: "/sm/smhandler.aspx",
				async: true,
				cache: false,
				type: 'POST',
				dataType: 'html',
				data: dataObj,
				success: function (responseText) {
					var response = $.parseJSON(responseText);
					if (response.IsSuccess) {
						master.FACTORY.onPublishSuccess();
					} else {
						_COMMON.noty("error", "Error", 500);
					}
				}
			});
		};
		master.FACTORY.updateShowFieldList = function(data) {
			var $container = $("#divShowFieldContainer");
			var foundedItem = null;
			master.FACTORY.documentChanged();
			$("div.checkbox-btn-wrapper", $container).each(function(idx, row) {
				if ($(row).data("controller-id") == data.controllerId/* || $(row).data("controller-name") == data.controllerName*/) {
					foundedItem = $(row);
					return false;
				}
			});

			const warning = visibilityWarnings[data.controllerId];
			var warningHtml = "";
			if (warning != null) {
				warningHtml = '<a href="#" class="visibility-warning" onclick="showVisibilityWarning(this)" style="display: none;"><i class="fa fa-warning" title="' + warning.WarningText + '"></i></a>&nbsp;';
			}

			if (foundedItem) {
				if (data.isVisible == true) {
					foundedItem.removeClass("hidden");
					foundedItem.find("input:checkbox").prop("checked", true);	
				}else if (data.isVisible == false) {
					foundedItem.removeClass("hidden");
					foundedItem.find("input:checkbox").prop("checked", false);	
				} else {
					//foundedItem.remove();	
					//default state
					foundedItem.addClass("hidden");
				}
				refreshWarnings(foundedItem, data);
			} else {
				var $item;
				if (data.isVisible != null) {
					$item = $('<div class="checkbox-btn-wrapper" data-controller-id="' + data.controllerId + '" data-controller-name="' + data.controllerName + '">' + warningHtml + '<span>' + data.controllerName + '</span><label class="toggle-btn pull-right" data-on="YES" data-off="NO"><input type="checkbox" data-text="' + data.controllerName + '" value="' + data.controllerId + '" ' + (data.isVisible ? "checked" : "") + ' /><span class="button-checkbox"></span></label></div>').appendTo($container);
				} else {
					//default state
					$item1 = $('<div class="checkbox-btn-wrapper hidden" data-controller-id="' + data.controllerId + '" data-controller-name="' + data.controllerName + '">' + warningHtml + '<span>' + data.controllerName + '</span><label class="toggle-btn pull-right" data-on="YES" data-off="NO"><input type="checkbox" data-text="' + data.controllerName + '" value="' + data.controllerId + '" /><span class="button-checkbox"></span></label></div>').appendTo($container);
				}
				initShowFieldListItem($item);
				refreshWarnings($item, data);
			}
			rpc.updateShowField(data);
		};

	}(window.master = window.master || {}, jQuery));
	function collectSubmitData() {
		var result = {};
		result.lenderconfigid = '<%=LenderConfigID%>';
			result.loantype = $("a.btn.active", ".switch-btn-panel").data("id").replace("#", "");
			result.showFieldItemList = _COMMON.toJSON(collectShowFieldItem());
			return result;
	}
	function collectShowFieldItem() {
		var result = [];
		$(".toggle-btn input:checkbox", "#divShowFieldContainer").each(function(idx, ele) {
			var $ele = $(ele);
			if ($ele.closest("div.checkbox-btn-wrapper").hasClass("hidden")) {
				//default state
				result.push({ ControllerId: $ele.val(), ControllerName: $ele.data("text"), IsVisible: "D" }); //default
			} else {
				result.push({ ControllerId: $ele.val(), ControllerName: $ele.data("text"), IsVisible: $ele.is(":checked") ? "Y" : "N" });
			}
		});
		return result;
	}
	function loadShowFieldItemList(type) {
		$.ajax({
			url: "/sm/subpages/showfielditemlist.aspx?lenderconfigid=<%=LenderConfigID%>",
			async: true,
			cache: false,
			type: 'POST',
			dataType: 'html',
			data: {type: type},
			success: function (response) {
				$("#divShowFieldContainer").html(response);
				$("div.checkbox-btn-wrapper", "#divShowFieldContainer").each(function(idx, row) {
					initShowFieldListItem(row);
					var $chk = $(row).find(".toggle-btn input:checkbox");
					var state;
					if ($(row).hasClass("hidden")) {
						state = null;
					} else {
						state = $chk.is(":checked");
                    }                  
					rpc.updateShowField({ controllerId: $chk.val(), isVisible: state });
					refreshWarnings($(row), { controllerId: $chk.val(), isVisible: state, ignoreDialog: true });
				});
			}
		});
	}
	function initShowFieldListItem(item, data) {
		var $item = $(item);
		$("input:checkbox", $item).on("change", function() {
			var $self = $(this);
			master.FACTORY.documentChanged();
			var state = $self.is(":checked");
			rpc.updateShowField({ controllerId: $item.data("controller-id"), isVisible: state });
			refreshWarnings($item, { isVisible: state, controllerId: $item.data("controller-id") })
		});
	}
	function refreshWarnings(item, data) {
		const $item = $(item);
		const warning = visibilityWarnings[data.controllerId];
		if (warning != null) {
			var showWarning = false;
			if (data.isVisible === true)
				showWarning = warning.Trigger == "Any" || warning.Trigger == "Visible";
			else if (data.isVisible === false)
				showWarning = warning.Trigger == "Any" || warning.Trigger == "Hidden";

			if (showWarning && !data.ignoreDialog) {
				// Also show a warning as they click it
				setTimeout(function () {
					_COMMON.showAlertDialog(warning.WarningText);
				});
			}
		}

		if (showWarning) {
			$item.find("a.visibility-warning").show();
		} else {
			$item.find("a.visibility-warning").hide();
		}
	}
	function showVisibilityWarning(anchor) {
		_COMMON.showAlertDialog($(anchor).find("i").attr("title"));
	}
</script>
</asp:Content>
<asp:Content runat="server" ContentPlaceHolderID="plActionButtons">
</asp:Content>