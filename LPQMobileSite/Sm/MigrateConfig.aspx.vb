﻿
Imports System.Xml
Imports Newtonsoft.Json
Imports Sm.BO
Imports LPQMobile.Utils

Partial Class Sm_MigrateConfig
	Inherits SmApmBasePage

	Protected Property XaBranchSelectionDropdown As String = ""

	Protected Property XaMotherMaidenName As String = ""
	Protected Property XaGender As String = ""
	Protected Property XaCitizenship As String = ""
	Protected Property XaEmployment As String = ""

	Protected Property XaMaritalStatus As String = ""

	Protected Property XaMotherMaidenNameSecondary As String = ""
	Protected Property XaCitizenshipSecondary As String = ""
	Protected Property XaEmploymentSecondary As String = ""
	Protected Property XaGrossIncomeSecondary As String = ""
	Protected Property XaIDPageSecondary As String = ""
	Protected Property XaOccupancySecondary As String = ""
	Protected Property XaOccupancy As String = ""
	Protected Property XaDisclosuresSecondaryEnable As String = ""
	Protected Property XaEmploymentMinor As String = ""

	Protected Property XaEnableMemberNumber As String = ""

	Protected Property LoanBranchSelectionDropdown As String = ""
	Protected Property CCIdentificationSection As String = ""
	Protected Property HEIdentificationSection As String = ""
	Protected Property PLIdentificationSection As String = ""
	Protected Property VLIdentificationSection As String = ""

	Protected Property SAShowFieldDisclosure As XmlNode

	Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
		If Not Page.IsPostBack Then
			If Request.Params("command") = "runmigratebatch" Then
				Dim res As CJsonResponse
				Try

					'SAShowFieldDisclosure = LenderConfigXml.SelectSingleNode("WEBSITE/CUSTOM_LIST/SHOW_FIELDS/ITEM[@controller_id='divDisclosureSection']")

					Dim xaBranchRequired As String = GetData("WEBSITE/XA_LOAN/BEHAVIOR/@branch_selection_dropdown_required")
					If xaBranchRequired = "Y" Then
						XaBranchSelectionDropdown = "Y"
					Else
						XaBranchSelectionDropdown = GetData("WEBSITE/XA_LOAN/VISIBLE/@branch_selection_dropdown")
					End If

					XaMotherMaidenName = GetData("WEBSITE/XA_LOAN/VISIBLE/@mother_maiden_name")
					XaGender = GetData("WEBSITE/XA_LOAN/VISIBLE/@gender")
					XaMaritalStatus = GetData("WEBSITE/XA_LOAN/VISIBLE/@marital_status")
					XaCitizenship = GetData("WEBSITE/XA_LOAN/VISIBLE/@citizenship")
					XaMotherMaidenNameSecondary = GetData("WEBSITE/XA_LOAN/VISIBLE/@mother_maiden_name_secondary")
					XaEmployment = GetData("WEBSITE/XA_LOAN/VISIBLE/@employment")

					XaCitizenshipSecondary = GetData("WEBSITE/XA_LOAN/VISIBLE/@citizenship_secondary")
					XaEmploymentSecondary = GetData("WEBSITE/XA_LOAN/VISIBLE/@employment_secondary")
					XaGrossIncomeSecondary = GetData("WEBSITE/XA_LOAN/VISIBLE/@gross_income_secondary")
					XaIDPageSecondary = GetData("WEBSITE/XA_LOAN/VISIBLE/@id_page_secondary")
					XaOccupancySecondary = GetData("WEBSITE/XA_LOAN/VISIBLE/@occupancy_secondary")
					XaDisclosuresSecondaryEnable = GetData("WEBSITE/XA_LOAN/VISIBLE/@disclosures_secondary_enable")

					XaEmploymentMinor = GetData("WEBSITE/XA_LOAN/VISIBLE/@minor_employment")


					XaEnableMemberNumber = GetData("WEBSITE/XA_LOAN/VISIBLE/@mem_number_secondary")

					XaOccupancy = GetData("WEBSITE/XA_LOAN/VISIBLE/@occupancy_status")

					Dim loanBranchRequired As String = GetData("WEBSITE/BEHAVIOR/@branch_selection_dropdown_required")
					If loanBranchRequired = "Y" Then
						LoanBranchSelectionDropdown = "Y"
					Else
						LoanBranchSelectionDropdown = GetData("WEBSITE/VISIBLE/@branch_selection_dropdown")
					End If

					CCIdentificationSection = GetData("WEBSITE/CREDIT_CARD_LOAN/VISIBLE/@identification_section")
					HEIdentificationSection = GetData("WEBSITE/HOME_EQUITY_LOAN/VISIBLE/@identification_section")
					PLIdentificationSection = GetData("WEBSITE/PERSONAL_LOAN/VISIBLE/@identification_section")
					VLIdentificationSection = GetData("WEBSITE/VEHICLE_LOAN/VISIBLE/@identification_section")

					'----
					Dim showFieldList As New List(Of SmShowFieldItemModel)
					If Not String.IsNullOrEmpty(XaBranchSelectionDropdown) Then
						showFieldList.Add(New SmShowFieldItemModel() With {.ControllerId = "divBranchSection", .ControllerName = "Branch", .LoanType = "XA", .IsVisible = IIf(XaBranchSelectionDropdown = "Y", "Y", "N").ToString()})
						showFieldList.Add(New SmShowFieldItemModel() With {.ControllerId = "divBranchSection", .ControllerName = "Branch", .LoanType = "SA", .IsVisible = IIf(XaBranchSelectionDropdown = "Y", "Y", "N").ToString()})
						showFieldList.Add(New SmShowFieldItemModel() With {.ControllerId = "divBranchSection", .ControllerName = "Branch", .LoanType = "XA_SPECIAL", .IsVisible = IIf(XaBranchSelectionDropdown = "Y", "Y", "N").ToString()})
                        ''sa_special
                        showFieldList.Add(New SmShowFieldItemModel() With {.ControllerId = "divBranchSection", .ControllerName = "Branch", .LoanType = "SA_SPECIAL", .IsVisible = IIf(XaBranchSelectionDropdown = "Y", "Y", "N").ToString()})
                    End If

					If Not String.IsNullOrEmpty(XaMotherMaidenName) Then
						showFieldList.Add(New SmShowFieldItemModel() With {.ControllerId = "divMotherMaidenName", .ControllerName = "Mother Maiden Name", .LoanType = "XA", .IsVisible = IIf(XaMotherMaidenName = "Y", "Y", "N").ToString()})
						showFieldList.Add(New SmShowFieldItemModel() With {.ControllerId = "co_divMotherMaidenName", .ControllerName = "Co-App Mother Maiden Name", .LoanType = "XA", .IsVisible = IIf(XaMotherMaidenName = "Y", "Y", "N").ToString()})
						showFieldList.Add(New SmShowFieldItemModel() With {.ControllerId = "divMotherMaidenName", .ControllerName = "Mother Maiden Name", .LoanType = "XA_SPECIAL", .IsVisible = IIf(XaMotherMaidenName = "Y", "Y", "N").ToString()})
						showFieldList.Add(New SmShowFieldItemModel() With {.ControllerId = "co_divMotherMaidenName", .ControllerName = "Co-App Mother Maiden Name", .LoanType = "XA_SPECIAL", .IsVisible = IIf(XaMotherMaidenName = "Y", "Y", "N").ToString()})
						showFieldList.Add(New SmShowFieldItemModel() With {.ControllerId = "m_divMotherMaidenName", .ControllerName = "Minor Mother Maiden Name", .LoanType = "XA_SPECIAL", .IsVisible = IIf(XaMotherMaidenName = "Y", "Y", "N").ToString()})        
                    End If

					If Not String.IsNullOrEmpty(XaGender) Then
						showFieldList.Add(New SmShowFieldItemModel() With {.ControllerId = "divGender", .ControllerName = "Gender", .LoanType = "XA", .IsVisible = IIf(XaGender = "Y", "Y", "N").ToString()})
						showFieldList.Add(New SmShowFieldItemModel() With {.ControllerId = "co_divGender", .ControllerName = "Co-App Gender", .LoanType = "XA", .IsVisible = IIf(XaGender = "Y", "Y", "N").ToString()})
						showFieldList.Add(New SmShowFieldItemModel() With {.ControllerId = "divGender", .ControllerName = "Gender", .LoanType = "XA_SPECIAL", .IsVisible = IIf(XaGender = "Y", "Y", "N").ToString()})
						showFieldList.Add(New SmShowFieldItemModel() With {.ControllerId = "co_divGender", .ControllerName = "Co-App Gender", .LoanType = "XA_SPECIAL", .IsVisible = IIf(XaGender = "Y", "Y", "N").ToString()})
						showFieldList.Add(New SmShowFieldItemModel() With {.ControllerId = "m_divGender", .ControllerName = "Minor Gender", .LoanType = "XA_SPECIAL", .IsVisible = IIf(XaGender = "Y", "Y", "N").ToString()})
                    End If

					If Not String.IsNullOrEmpty(XaEnableMemberNumber) Then
                        showFieldList.Add(New SmShowFieldItemModel() With {.ControllerId = "divMemberNumber", .ControllerName = "Member Number", .LoanType = "SA", .IsVisible = IIf(XaEnableMemberNumber = "Y", "Y", "N").ToString()})
                        showFieldList.Add(New SmShowFieldItemModel() With {.ControllerId = "co_divMemberNumber", .ControllerName = "Co-App Member Number", .LoanType = "SA", .IsVisible = IIf(XaEnableMemberNumber = "Y", "Y", "N").ToString()})
                        ''sa_special
                        showFieldList.Add(New SmShowFieldItemModel() With {.ControllerId = "m_divMemberNumber", .ControllerName = "Minor Member Number", .LoanType = "SA_SPECIAL", .IsVisible = IIf(XaEnableMemberNumber = "Y", "Y", "N").ToString()})
                    End If

					If Not String.IsNullOrEmpty(XaMaritalStatus) Then
						showFieldList.Add(New SmShowFieldItemModel() With {.ControllerId = "divMaritalStatus", .ControllerName = "Marital Status", .LoanType = "XA", .IsVisible = IIf(XaMaritalStatus = "Y", "Y", "N").ToString()})
						showFieldList.Add(New SmShowFieldItemModel() With {.ControllerId = "co_divMaritalStatus", .ControllerName = "Co-App Marital Status", .LoanType = "XA", .IsVisible = IIf(XaMaritalStatus = "Y", "Y", "N").ToString()})
						showFieldList.Add(New SmShowFieldItemModel() With {.ControllerId = "divMaritalStatus", .ControllerName = "Marital Status", .LoanType = "SA", .IsVisible = IIf(XaMaritalStatus = "Y", "Y", "N").ToString()})
						showFieldList.Add(New SmShowFieldItemModel() With {.ControllerId = "co_divMaritalStatus", .ControllerName = "Co-App Marital Status", .LoanType = "SA", .IsVisible = IIf(XaMaritalStatus = "Y", "Y", "N").ToString()})
						showFieldList.Add(New SmShowFieldItemModel() With {.ControllerId = "divMaritalStatus", .ControllerName = "Marital Status", .LoanType = "XA_SPECIAL", .IsVisible = IIf(XaMaritalStatus = "Y", "Y", "N").ToString()})
						showFieldList.Add(New SmShowFieldItemModel() With {.ControllerId = "co_divMaritalStatus", .ControllerName = "Co-App Marital Status", .LoanType = "XA_SPECIAL", .IsVisible = IIf(XaMaritalStatus = "Y", "Y", "N").ToString()})
						showFieldList.Add(New SmShowFieldItemModel() With {.ControllerId = "m_divMaritalStatus", .ControllerName = "Minor Marital Status", .LoanType = "XA_SPECIAL", .IsVisible = IIf(XaMaritalStatus = "Y", "Y", "N").ToString()})
                        ''sa_special
                        showFieldList.Add(New SmShowFieldItemModel() With {.ControllerId = "divMaritalStatus", .ControllerName = "Marital Status", .LoanType = "SA_SPECIAL", .IsVisible = IIf(XaMaritalStatus = "Y", "Y", "N").ToString()})
                        showFieldList.Add(New SmShowFieldItemModel() With {.ControllerId = "co_divMaritalStatus", .ControllerName = "Co-App Marital Status", .LoanType = "SA_SPECIAL", .IsVisible = IIf(XaMaritalStatus = "Y", "Y", "N").ToString()})
                        showFieldList.Add(New SmShowFieldItemModel() With {.ControllerId = "m_divMaritalStatus", .ControllerName = "Minor Marital Status", .LoanType = "SA_SPECIAL", .IsVisible = IIf(XaMaritalStatus = "Y", "Y", "N").ToString()})

                    End If


					If Not String.IsNullOrEmpty(XaCitizenship) Then
						showFieldList.Add(New SmShowFieldItemModel() With {.ControllerId = "divCitizenshipStatus", .ControllerName = "Citizenship Status", .LoanType = "XA", .IsVisible = IIf(XaCitizenship = "Y", "Y", "N").ToString()})
						showFieldList.Add(New SmShowFieldItemModel() With {.ControllerId = "co_divCitizenshipStatus", .ControllerName = "Co-App Citizenship Status", .LoanType = "XA", .IsVisible = IIf(XaCitizenship = "Y", "Y", "N").ToString()})
						showFieldList.Add(New SmShowFieldItemModel() With {.ControllerId = "divCitizenshipStatus", .ControllerName = "Citizenship Status", .LoanType = "XA_SPECIAL", .IsVisible = IIf(XaCitizenship = "Y", "Y", "N").ToString()})
						showFieldList.Add(New SmShowFieldItemModel() With {.ControllerId = "co_divCitizenshipStatus", .ControllerName = "Co-App Citizenship Status", .LoanType = "XA_SPECIAL", .IsVisible = IIf(XaCitizenship = "Y", "Y", "N").ToString()})
						showFieldList.Add(New SmShowFieldItemModel() With {.ControllerId = "m_divCitizenshipStatus", .ControllerName = "Minor Citizenship Status", .LoanType = "XA_SPECIAL", .IsVisible = IIf(XaCitizenship = "Y", "Y", "N").ToString()})
					End If

					If Not String.IsNullOrEmpty(XaMotherMaidenNameSecondary) Then
						showFieldList.Add(New SmShowFieldItemModel() With {.ControllerId = "divMotherMaidenName", .ControllerName = "Mother Maiden Name", .LoanType = "SA", .IsVisible = IIf(XaMotherMaidenNameSecondary = "Y", "Y", "N").ToString()})
						showFieldList.Add(New SmShowFieldItemModel() With {.ControllerId = "co_divMotherMaidenName", .ControllerName = "Co-App Mother Maiden Name", .LoanType = "SA", .IsVisible = IIf(XaMotherMaidenNameSecondary = "Y", "Y", "N").ToString()})
                        ''sa_special
                        showFieldList.Add(New SmShowFieldItemModel() With {.ControllerId = "divMotherMaidenName", .ControllerName = "Mother Maiden Name", .LoanType = "SA_SPECIAL", .IsVisible = IIf(XaMotherMaidenNameSecondary = "Y", "Y", "N").ToString()})
                        showFieldList.Add(New SmShowFieldItemModel() With {.ControllerId = "co_divMotherMaidenName", .ControllerName = "Co-App Mother Maiden Name", .LoanType = "SA_SPECIAL", .IsVisible = IIf(XaMotherMaidenNameSecondary = "Y", "Y", "N").ToString()})
                        showFieldList.Add(New SmShowFieldItemModel() With {.ControllerId = "m_divMotherMaidenName", .ControllerName = "Co-App Mother Maiden Name", .LoanType = "SA", .IsVisible = IIf(XaMotherMaidenNameSecondary = "Y", "Y", "N").ToString()})
                    End If

					If Not String.IsNullOrEmpty(XaCitizenshipSecondary) Then
						showFieldList.Add(New SmShowFieldItemModel() With {.ControllerId = "divCitizenshipStatus", .ControllerName = "Citizenship Status", .LoanType = "SA", .IsVisible = IIf(XaCitizenshipSecondary = "Y", "Y", "N").ToString()})
						showFieldList.Add(New SmShowFieldItemModel() With {.ControllerId = "co_divCitizenshipStatus", .ControllerName = "Co-App Citizenship Status", .LoanType = "SA", .IsVisible = IIf(XaCitizenshipSecondary = "Y", "Y", "N").ToString()})
                        ''sa_special
                        showFieldList.Add(New SmShowFieldItemModel() With {.ControllerId = "divCitizenshipStatus", .ControllerName = "Citizenship Status", .LoanType = "SA_SPECIAL", .IsVisible = IIf(XaCitizenshipSecondary = "Y", "Y", "N").ToString()})
                        showFieldList.Add(New SmShowFieldItemModel() With {.ControllerId = "co_divCitizenshipStatus", .ControllerName = "Co-App Citizenship Status", .LoanType = "SA_SPECIAL", .IsVisible = IIf(XaCitizenshipSecondary = "Y", "Y", "N").ToString()})
                        showFieldList.Add(New SmShowFieldItemModel() With {.ControllerId = "m_divCitizenshipStatus", .ControllerName = "Co-App Citizenship Status", .LoanType = "SA_SPECIAL", .IsVisible = IIf(XaCitizenshipSecondary = "Y", "Y", "N").ToString()})
                    End If

					If Not String.IsNullOrEmpty(XaEmployment) Then
						showFieldList.Add(New SmShowFieldItemModel() With {.ControllerId = "divEmploymentSection", .ControllerName = "Employment", .LoanType = "XA", .IsVisible = IIf(XaEmployment = "Y", "Y", "N").ToString()})
						showFieldList.Add(New SmShowFieldItemModel() With {.ControllerId = "co_divEmploymentSection", .ControllerName = "Co-App Employment", .LoanType = "XA", .IsVisible = IIf(XaEmployment = "Y", "Y", "N").ToString()})
					End If

					If Not String.IsNullOrEmpty(XaEmploymentSecondary) Then
						showFieldList.Add(New SmShowFieldItemModel() With {.ControllerId = "divEmploymentSection", .ControllerName = "Employment", .LoanType = "SA", .IsVisible = IIf(XaEmploymentSecondary = "Y", "Y", "N").ToString()})
						showFieldList.Add(New SmShowFieldItemModel() With {.ControllerId = "co_divEmploymentSection", .ControllerName = "Co-App Employment", .LoanType = "SA", .IsVisible = IIf(XaEmploymentSecondary = "Y", "Y", "N").ToString()})
					End If

					If Not String.IsNullOrEmpty(XaEmploymentMinor) Then	 'default is no
						showFieldList.Add(New SmShowFieldItemModel() With {.ControllerId = "divEmploymentSection", .ControllerName = "Employment", .LoanType = "XA_SPECIAL", .IsVisible = IIf(XaEmploymentMinor = "Y", "Y", "N").ToString()})
						showFieldList.Add(New SmShowFieldItemModel() With {.ControllerId = "co_divEmploymentSection", .ControllerName = "Co-App Employment", .LoanType = "XA_SPECIAL", .IsVisible = IIf(XaEmploymentMinor = "Y", "Y", "N").ToString()})
                    End If

					If Not String.IsNullOrEmpty(XaGrossIncomeSecondary) Then
						showFieldList.Add(New SmShowFieldItemModel() With {.ControllerId = "divGrossMonthlyIncome", .ControllerName = "Gross Monthly Income", .LoanType = "SA", .IsVisible = IIf(XaGrossIncomeSecondary = "Y", "Y", "N").ToString()})
						showFieldList.Add(New SmShowFieldItemModel() With {.ControllerId = "co_divGrossMonthlyIncome", .ControllerName = "Co-App Gross Monthly Income", .LoanType = "SA", .IsVisible = IIf(XaGrossIncomeSecondary = "Y", "Y", "N").ToString()})
					End If

					If Not String.IsNullOrEmpty(XaIDPageSecondary) Then
						showFieldList.Add(New SmShowFieldItemModel() With {.ControllerId = "divApplicantIdentification", .ControllerName = "Applicant Identification", .LoanType = "SA", .IsVisible = IIf(XaIDPageSecondary = "Y", "Y", "N").ToString()})
						showFieldList.Add(New SmShowFieldItemModel() With {.ControllerId = "co_divApplicantIdentification", .ControllerName = "Co-App Applicant Identification", .LoanType = "SA", .IsVisible = IIf(XaIDPageSecondary = "Y", "Y", "N").ToString()})
					End If

					If Not String.IsNullOrEmpty(XaOccupancySecondary) Then
						showFieldList.Add(New SmShowFieldItemModel() With {.ControllerId = "divOccupancyStatusSection", .ControllerName = "Occupancy Status", .LoanType = "SA", .IsVisible = IIf(XaOccupancySecondary = "Y", "Y", "N").ToString()})
						showFieldList.Add(New SmShowFieldItemModel() With {.ControllerId = "co_divOccupancyStatusSection", .ControllerName = "Co-App Occupancy Status", .LoanType = "SA", .IsVisible = IIf(XaOccupancySecondary = "Y", "Y", "N").ToString()})
					End If

					If Not String.IsNullOrEmpty(XaOccupancy) Then
						showFieldList.Add(New SmShowFieldItemModel() With {.ControllerId = "divOccupancyStatusSection", .ControllerName = "Occupancy Status", .LoanType = "XA", .IsVisible = IIf(XaOccupancySecondary = "Y", "Y", "N").ToString()})
						showFieldList.Add(New SmShowFieldItemModel() With {.ControllerId = "co_divOccupancyStatusSection", .ControllerName = "Co-App Occupancy Status", .LoanType = "XA", .IsVisible = IIf(XaOccupancySecondary = "Y", "Y", "N").ToString()})
					End If


					'If Not String.IsNullOrEmpty(XaDisclosuresSecondaryEnable) Then
					'	showFieldList.Add(New SmShowFieldItemModel() With {.ControllerId = "divDisclosureSection", .ControllerName = "Disclosure", .LoanType = "SA", .IsVisible = IIf(XaDisclosuresSecondaryEnable = "Y", "Y", "N").ToString()})
					'End If


					If Not String.IsNullOrEmpty(LoanBranchSelectionDropdown) Then
						showFieldList.Add(New SmShowFieldItemModel() With {.ControllerId = "divBranchSection", .ControllerName = "Branch", .LoanType = "CC", .IsVisible = IIf(LoanBranchSelectionDropdown = "Y", "Y", "N").ToString()})
						showFieldList.Add(New SmShowFieldItemModel() With {.ControllerId = "divBranchSection", .ControllerName = "Branch", .LoanType = "CC_COMBO", .IsVisible = IIf(LoanBranchSelectionDropdown = "Y", "Y", "N").ToString()})
						showFieldList.Add(New SmShowFieldItemModel() With {.ControllerId = "divBranchSection", .ControllerName = "Branch", .LoanType = "HE", .IsVisible = IIf(LoanBranchSelectionDropdown = "Y", "Y", "N").ToString()})
						showFieldList.Add(New SmShowFieldItemModel() With {.ControllerId = "divBranchSection", .ControllerName = "Branch", .LoanType = "HE_COMBO", .IsVisible = IIf(LoanBranchSelectionDropdown = "Y", "Y", "N").ToString()})
						showFieldList.Add(New SmShowFieldItemModel() With {.ControllerId = "divBranchSection", .ControllerName = "Branch", .LoanType = "PL", .IsVisible = IIf(LoanBranchSelectionDropdown = "Y", "Y", "N").ToString()})
						showFieldList.Add(New SmShowFieldItemModel() With {.ControllerId = "divBranchSection", .ControllerName = "Branch", .LoanType = "PL_COMBO", .IsVisible = IIf(LoanBranchSelectionDropdown = "Y", "Y", "N").ToString()})
						showFieldList.Add(New SmShowFieldItemModel() With {.ControllerId = "divBranchSection", .ControllerName = "Branch", .LoanType = "VL", .IsVisible = IIf(LoanBranchSelectionDropdown = "Y", "Y", "N").ToString()})
						showFieldList.Add(New SmShowFieldItemModel() With {.ControllerId = "divBranchSection", .ControllerName = "Branch", .LoanType = "VL_COMBO", .IsVisible = IIf(LoanBranchSelectionDropdown = "Y", "Y", "N").ToString()})
					End If

					If Not String.IsNullOrEmpty(CCIdentificationSection) Then
						showFieldList.Add(New SmShowFieldItemModel() With {.ControllerId = "divApplicantIdentification", .ControllerName = "Applicant Identification", .LoanType = "CC", .IsVisible = IIf(CCIdentificationSection = "Y", "Y", "N").ToString()})
						showFieldList.Add(New SmShowFieldItemModel() With {.ControllerId = "co_divApplicantIdentification", .ControllerName = "Co-App Applicant Identification", .LoanType = "CC", .IsVisible = IIf(CCIdentificationSection = "Y", "Y", "N").ToString()})
						showFieldList.Add(New SmShowFieldItemModel() With {.ControllerId = "divApplicantIdentification", .ControllerName = "Applicant Identification", .LoanType = "CC_COMBO", .IsVisible = IIf(CCIdentificationSection = "Y", "Y", "N").ToString()})
						showFieldList.Add(New SmShowFieldItemModel() With {.ControllerId = "co_divApplicantIdentification", .ControllerName = "Co-App Applicant Identification", .LoanType = "CC_COMBO", .IsVisible = IIf(CCIdentificationSection = "Y", "Y", "N").ToString()})
					End If

					If Not String.IsNullOrEmpty(HEIdentificationSection) Then
						showFieldList.Add(New SmShowFieldItemModel() With {.ControllerId = "divApplicantIdentification", .ControllerName = "Applicant Identification", .LoanType = "HE", .IsVisible = IIf(HEIdentificationSection = "Y", "Y", "N").ToString()})
						showFieldList.Add(New SmShowFieldItemModel() With {.ControllerId = "co_divApplicantIdentification", .ControllerName = "Co-App Applicant Identification", .LoanType = "HE", .IsVisible = IIf(HEIdentificationSection = "Y", "Y", "N").ToString()})
						showFieldList.Add(New SmShowFieldItemModel() With {.ControllerId = "divApplicantIdentification", .ControllerName = "Applicant Identification", .LoanType = "HE_COMBO", .IsVisible = IIf(HEIdentificationSection = "Y", "Y", "N").ToString()})
						showFieldList.Add(New SmShowFieldItemModel() With {.ControllerId = "co_divApplicantIdentification", .ControllerName = "Co-App Applicant Identification", .LoanType = "HE_COMBO", .IsVisible = IIf(HEIdentificationSection = "Y", "Y", "N").ToString()})
					End If

					If Not String.IsNullOrEmpty(PLIdentificationSection) Then
						showFieldList.Add(New SmShowFieldItemModel() With {.ControllerId = "divApplicantIdentification", .ControllerName = "Applicant Identification", .LoanType = "PL", .IsVisible = IIf(PLIdentificationSection = "Y", "Y", "N").ToString()})
						showFieldList.Add(New SmShowFieldItemModel() With {.ControllerId = "co_divApplicantIdentification", .ControllerName = "Co-App Applicant Identification", .LoanType = "PL", .IsVisible = IIf(PLIdentificationSection = "Y", "Y", "N").ToString()})
						showFieldList.Add(New SmShowFieldItemModel() With {.ControllerId = "divApplicantIdentification", .ControllerName = "Applicant Identification", .LoanType = "PL_COMBO", .IsVisible = IIf(PLIdentificationSection = "Y", "Y", "N").ToString()})
						showFieldList.Add(New SmShowFieldItemModel() With {.ControllerId = "co_divApplicantIdentification", .ControllerName = "Co-App Applicant Identification", .LoanType = "PL_COMBO", .IsVisible = IIf(PLIdentificationSection = "Y", "Y", "N").ToString()})
					End If

					If Not String.IsNullOrEmpty(VLIdentificationSection) Then
						showFieldList.Add(New SmShowFieldItemModel() With {.ControllerId = "divApplicantIdentification", .ControllerName = "Applicant Identification", .LoanType = "VL", .IsVisible = IIf(VLIdentificationSection = "Y", "Y", "N").ToString()})
						showFieldList.Add(New SmShowFieldItemModel() With {.ControllerId = "co_divApplicantIdentification", .ControllerName = "Co-App Applicant Identification", .LoanType = "VL", .IsVisible = IIf(VLIdentificationSection = "Y", "Y", "N").ToString()})
						showFieldList.Add(New SmShowFieldItemModel() With {.ControllerId = "divApplicantIdentification", .ControllerName = "Applicant Identification", .LoanType = "VL_COMBO", .IsVisible = IIf(VLIdentificationSection = "Y", "Y", "N").ToString()})
						showFieldList.Add(New SmShowFieldItemModel() With {.ControllerId = "co_divApplicantIdentification", .ControllerName = "Co-App Applicant Identification", .LoanType = "VL_COMBO", .IsVisible = IIf(VLIdentificationSection = "Y", "Y", "N").ToString()})
					End If
					Dim result = True
					If showFieldList IsNot Nothing AndAlso showFieldList.Any() Then
                        Dim loanTypeList As New List(Of String)(New String() {"XA", "SA", "XA_SPECIAL", "SA_SPECIAL", "CC", "CC_COMBO", "HE", "HE_COMBO", "PL", "PL_COMBO", "VL", "VL_COMBO"})
						For Each loanType As String In loanTypeList
							Dim nodeBatch As New SmLenderConfigDraft() With {
								.LenderConfigID = LenderConfigID,
								.CreatedByUserID = UserInfo.UserID,
								.NodePath = Sm.SHOW_FIELDS_ITEMS(loanType),
								.NodeContent = SmUtil.BuildShowFieldNodeXml("SHOW_FIELDS", loanType, showFieldList.Where(Function(p) p.LoanType = loanType).ToList())}
							result = smBL.MigrateNodes(LenderConfigID, Guid.Empty, Request.UserHostAddress, "migrate configurations", AddressOf SmUtil.MergeShowFieldItems, nodeBatch)
						Next
						'remove legacy attributes after migrated
						smBL.MigrateNodes(LenderConfigID, Guid.Empty, Request.UserHostAddress, "delete legacy attributes after migrated", AddressOf RemoveLegacyAttributes, New SmLenderConfigDraft() With {
								.LenderConfigID = LenderConfigID,
								.CreatedByUserID = UserInfo.UserID,
								.NodePath = String.Empty,
								.NodeContent = String.Empty})
					End If

					'code for remove certain item from showfield, need to comment out code from the showFieldList loop first
					'If SAShowFieldDisclosure IsNot Nothing Then
					'	smBL.MigrateNodes(LenderConfigID, "system", Request.UserHostAddress, "delete nodes that are wrong created after migrated", AddressOf RemoveShowField, New SmLenderConfigDraft() With {
					'			.LenderConfigID = LenderConfigID,
					'			.CreatedByUserID = UserInfo.UserID,
					'			.NodePath = String.Empty,
					'			.NodeContent = String.Empty})
					'End If


					If result Then
						res = New CJsonResponse(True, "", "OK")
					Else
						res = New CJsonResponse(False, "", "FAILED")
					End If

					'removed old node

				Catch ex As Exception
					log.Error("Could not run batch to migrate the show field configuration data.", ex)
					res = New CJsonResponse(False, "Could not run batch. Please try again", True)
                End Try
                Response.Write(JsonConvert.SerializeObject(res))
				Response.End()
				Return
			Else
				Try
					Dim smMasterPage = CType(Me.Master, Sm_SiteManager)
					smMasterPage.LenderConfigXml = LenderConfigXml
					smMasterPage.SelectedMainMenuItem = "config"
					smMasterPage.SelectedSubMenuItem = "migrateconfig"
					smMasterPage.ShowPreviewButton = False
					smMasterPage.ShowSaveDraftButton = False
					smMasterPage.ShowPublishButton = False


					Dim xaBranchRequired As String = GetData("WEBSITE/XA_LOAN/BEHAVIOR/@branch_selection_dropdown_required")
					If xaBranchRequired = "Y" Then
						XaBranchSelectionDropdown = "Y"
					Else
						XaBranchSelectionDropdown = GetData("WEBSITE/XA_LOAN/VISIBLE/@branch_selection_dropdown")
					End If

					XaMotherMaidenName = GetData("WEBSITE/XA_LOAN/VISIBLE/@mother_maiden_name")
					XaGender = GetData("WEBSITE/XA_LOAN/VISIBLE/@gender")
					XaMaritalStatus = GetData("WEBSITE/XA_LOAN/VISIBLE/@marital_status")
					XaCitizenship = GetData("WEBSITE/XA_LOAN/VISIBLE/@citizenship")
					XaMotherMaidenNameSecondary = GetData("WEBSITE/XA_LOAN/VISIBLE/@mother_maiden_name_secondary")
					XaCitizenshipSecondary = GetData("WEBSITE/XA_LOAN/VISIBLE/@citizenship_secondary")
					XaEmploymentSecondary = GetData("WEBSITE/XA_LOAN/VISIBLE/@employment_secondary")
					XaGrossIncomeSecondary = GetData("WEBSITE/XA_LOAN/VISIBLE/@gross_income_secondary")
					XaIDPageSecondary = GetData("WEBSITE/XA_LOAN/VISIBLE/@id_page_secondary")
					XaOccupancySecondary = GetData("WEBSITE/XA_LOAN/VISIBLE/@occupancy_secondary")
					XaDisclosuresSecondaryEnable = GetData("WEBSITE/XA_LOAN/VISIBLE/@disclosures_secondary_enable")
					XaEnableMemberNumber = GetData("WEBSITE/XA_LOAN/VISIBLE/@mem_number_secondary")

					Dim loanBranchRequired As String = GetData("WEBSITE/BEHAVIOR/@branch_selection_dropdown_required")
					If loanBranchRequired = "Y" Then
						LoanBranchSelectionDropdown = "Y"
					Else
						LoanBranchSelectionDropdown = GetData("WEBSITE/VISIBLE/@branch_selection_dropdown")
					End If

					CCIdentificationSection = GetData("WEBSITE/CREDIT_CARD_LOAN/VISIBLE/@identification_section")
					HEIdentificationSection = GetData("WEBSITE/HOME_EQUITY_LOAN/VISIBLE/@identification_section")
					PLIdentificationSection = GetData("WEBSITE/PERSONAL_LOAN/VISIBLE/@identification_section")
					VLIdentificationSection = GetData("WEBSITE/VEHICLE_LOAN/VISIBLE/@identification_section")
				Catch ex As Exception
					log.Error("Could not load MigrateConfig Page", ex)
				End Try
			End If

		End If
	End Sub


	Private Function GetData(path As String) As String
		Dim node As XmlNode = LenderConfigXml.SelectSingleNode(path)
		If node IsNot Nothing Then
			Return node.Value.ToUpper()
		End If
		Return String.Empty
	End Function

	Private Function RemoveLegacyAttributes(liveConfigXmlDoc As XmlDocument, nodeBatch As SmLenderConfigDraft) As XmlDocument
		'Dim xaBranchSelectionDropdownNode = CType(liveConfigXmlDoc.SelectSingleNode("WEBSITE/XA_LOAN/VISIBLE/@branch_selection_dropdown"), XmlAttribute)
		'If xaBranchSelectionDropdownNode IsNot Nothing Then
		'	xaBranchSelectionDropdownNode.OwnerElement.RemoveAttribute("branch_selection_dropdown")
		'End If

		Dim xaVisibleNode = liveConfigXmlDoc.SelectSingleNode("WEBSITE/XA_LOAN/VISIBLE")
		If xaVisibleNode IsNot Nothing Then
			xaVisibleNode.Attributes.RemoveNamedItem("branch_selection_dropdown")
			xaVisibleNode.Attributes.RemoveNamedItem("mother_maiden_name")
			xaVisibleNode.Attributes.RemoveNamedItem("gender")
			xaVisibleNode.Attributes.RemoveNamedItem("gender_secondary")
			xaVisibleNode.Attributes.RemoveNamedItem("citizenship")
			xaVisibleNode.Attributes.RemoveNamedItem("employment")
			xaVisibleNode.Attributes.RemoveNamedItem("mother_maiden_name_secondary")
			xaVisibleNode.Attributes.RemoveNamedItem("citizenship_secondary")
			xaVisibleNode.Attributes.RemoveNamedItem("employment_secondary")
			xaVisibleNode.Attributes.RemoveNamedItem("gross_income_secondary")
			xaVisibleNode.Attributes.RemoveNamedItem("id_page_secondary")
			xaVisibleNode.Attributes.RemoveNamedItem("occupancy_secondary")
			xaVisibleNode.Attributes.RemoveNamedItem("disclosures_secondary_enable")
			xaVisibleNode.Attributes.RemoveNamedItem("minor_employment")
			xaVisibleNode.Attributes.RemoveNamedItem("marital_status")
			xaVisibleNode.Attributes.RemoveNamedItem("mem_number_secondary")
			xaVisibleNode.Attributes.RemoveNamedItem("occupancy_status")
		End If

		Dim websiteVisibleNode = liveConfigXmlDoc.SelectSingleNode("WEBSITE/VISIBLE")
		If websiteVisibleNode IsNot Nothing Then
			websiteVisibleNode.Attributes.RemoveNamedItem("branch_selection_dropdown")
			websiteVisibleNode.Attributes.RemoveNamedItem("citizenship")
		End If

		Dim ccVisibleNode = liveConfigXmlDoc.SelectSingleNode("WEBSITE/CREDIT_CARD_LOAN/VISIBLE")
		If ccVisibleNode IsNot Nothing Then
			ccVisibleNode.Attributes.RemoveNamedItem("identification_section")
		End If

		Dim heVisibleNode = liveConfigXmlDoc.SelectSingleNode("WEBSITE/HOME_EQUITY_LOAN/VISIBLE")
		If heVisibleNode IsNot Nothing Then
			heVisibleNode.Attributes.RemoveNamedItem("identification_section")
		End If

		'Dim helocVisibleNode = liveConfigXmlDoc.SelectSingleNode("WEBSITE/HOME_EQUITY_LOC/VISIBLE")
		'If helocVisibleNode IsNot Nothing Then
		'	helocVisibleNode.Attributes.RemoveNamedItem("identification_section")
		'End If

		Dim plVisibleNode = liveConfigXmlDoc.SelectSingleNode("WEBSITE/PERSONAL_LOAN/VISIBLE")
		If plVisibleNode IsNot Nothing Then
			plVisibleNode.Attributes.RemoveNamedItem("identification_section")
		End If

		Dim vlVisibleNode = liveConfigXmlDoc.SelectSingleNode("WEBSITE/VEHICLE_LOAN/VISIBLE")
		If vlVisibleNode IsNot Nothing Then
			vlVisibleNode.Attributes.RemoveNamedItem("identification_section")
		End If
		Return liveConfigXmlDoc
	End Function

	Private Function RemoveShowField(liveConfigXmlDoc As XmlDocument, nodeBatch As SmLenderConfigDraft) As XmlDocument
		Dim SAShowFieldDisclosure = liveConfigXmlDoc.SelectSingleNode("WEBSITE/CUSTOM_LIST/SHOW_FIELDS/ITEM[@controller_id='divDisclosureSection']")
		If SAShowFieldDisclosure IsNot Nothing Then
			SAShowFieldDisclosure.ParentNode.RemoveChild(SAShowFieldDisclosure)
		End If
		Return liveConfigXmlDoc
	End Function

End Class
