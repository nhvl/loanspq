﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="ManageML.aspx.vb" Inherits="Sm_ManageML" MasterPageFile="SiteManager.master" %>

<asp:Content runat="server" ContentPlaceHolderID="plBody">
	<div>
		<h1 class="page-header">Manage Mortgage Loan Settings</h1>
		<p>These are the messages that may be displayed in application</p>
		<section class="section-wrapper">
			<p>GENERAL</p>
			<div>
				<h3 class="property-title">Contact Info</h3>
				<p>This information will display when an error occurs and the applicant needs your institution's contact information.</p>
				<div class="html-editor" contenteditable="true" data-required="true" placeholder="Enter your message..." id="txtContactInfoMsg"><%=ContactInfo%></div>
			</div>
			<div>
				<h3 class="property-title">Online Application Privacy Policy</h3>
				<div class="html-editor" contenteditable="true" data-required="true" placeholder="Enter your message..." id="txtOnlineAppPrivacyPolicyMsg"><%=OnlineApplicationPrivacyPolicy%></div>
			</div>
			<div>
				<h3 class="property-title">Full Application Submission Confirmation</h3>
				<div class="html-editor" contenteditable="true" data-required="true" placeholder="Enter your message..." id="txtFullAppSubmitConfirmationMsg"><%=FullApplicationSubmissionConfirmation%></div>
			</div>
			<div>
				<h3 class="property-title">Sign-Out Page</h3>
				<div class="html-editor" contenteditable="true" data-required="true" placeholder="Enter your message..." id="txtSignedOutPageMsg"><%=SignedOutPageMessage%></div>
			</div>
		</section>
		<section class="section-wrapper">
			<p>DISCLAIMERS</p>
			<div>
				<h3 class="property-title">Get Rates Page Disclaimer</h3>
				<p>This message will display at the beginning of Quick Pricer.</p>
				<div class="html-editor" contenteditable="true" data-required="true" placeholder="Enter your message..." id="txtGetRatesPageDisClaimerMsg"><%=GetRatesPageDisclaimer%></div>
			</div>
			<div>
				<h3 class="property-title">Eligible Loan Program Disclaimer</h3>
				<p>This message will display on the Details model window in the Quick Pricer if an eligible loan program is found.</p>
				<div class="html-editor" contenteditable="true" data-required="true" placeholder="Enter your message..." id="txtEligibleLoanProgramDisclaimerMsg"><%=EligibleLoanProgramDisclaimer%></div>
			</div>
			<div>
				<h3 class="property-title">No Eligible Loan Program Disclaimer</h3>
				<p>This message will display on the Details model window in the Quick Pricer if no eligible loan program is found.</p>
				<div class="html-editor" contenteditable="true" data-required="true" placeholder="Enter your message..." id="txtNoEligibleLoanProgramDisclaimerMsg"><%=NoEligibleLoanProgramDisclaimer%></div>
			</div>
		</section>
	</div>
</asp:Content>
<asp:Content runat="server" ContentPlaceHolderID="plScripts">
	<script type="text/javascript" src="js/ckeditor/ckeditor.js"></script>
	<script type="text/javascript">
		CKEDITOR.disableAutoInline = true;
		$(function () {
			$(".html-editor").ckEditor({
				onContentChanged: function (editor) { master.FACTORY.documentChanged(editor); },
				onCommandClicked: function (editor, commandBtn) { master.FACTORY.documentChanged(editor); }
			});
			
			registerDataValidator();
			
		});
		(function (master, $, undefined) {
			master.FACTORY.SaveDraft = function (revisionId) {
				if ($.smValidate("ValidateData") == false) return;
				var dataObj = collectSubmitData();
				dataObj.command = "savemanagelqb";
				dataObj.revisionId = revisionId;
				$.ajax({
					url: "/sm/smhandler.aspx",
					async: true,
					cache: false,
					type: 'POST',
					dataType: 'html',
					data: dataObj,
					success: function (responseText) {
						var response = $.parseJSON(responseText);
						if (response.IsSuccess) {
							master.FACTORY.onSaveDraftSuccess();
						} else {
							_COMMON.noty("error", "Error", 500);
						}
					}
				});
			};
			master.FACTORY.Preview = function (revisionId) {
				if ($.smValidate("ValidateData") == false) return;
				var dataObj = collectSubmitData();
				dataObj.command = "previewmanagelqb";
				dataObj.revisionId = revisionId;
				$.ajax({
					url: "/sm/smhandler.aspx",
					async: true,
					cache: false,
					type: 'POST',
					dataType: 'html',
					data: dataObj,
					success: function (responseText) {
						var response = $.parseJSON(responseText);
						if (response.IsSuccess) {
							master.FACTORY.onPreviewSuccess();
							_COMMON.openInNewTab(response.Info.previewurl);
						} else {
							_COMMON.noty("error", "Error", 500);
						}
					}
				});
			};
			master.FACTORY.validateBeforePublish = function () {
				return $.smValidate("ValidateData");
			}
			master.FACTORY.Publish = function (comment, publishAll) {
				if ($.smValidate("ValidateData") == false) return;
				var dataObj = collectSubmitData();
				dataObj.command = "publishmanagelqb";
				dataObj.comment = comment;
				dataObj.publishAll = publishAll;
				$.ajax({
					url: "/sm/smhandler.aspx",
					async: true,
					cache: false,
					type: 'POST',
					dataType: 'html',
					data: dataObj,
					success: function (responseText) {
						var response = $.parseJSON(responseText);
						if (response.IsSuccess) {
							master.FACTORY.onPublishSuccess();
						} else {
							_COMMON.noty("error", "Error", 500);
						}
					}
				});
			};
		}(window.master = window.master || {}, jQuery));
		function registerDataValidator() {
			$.smValidate.removeValidationGroup("ValidateData");
			$("[data-required='true']").each(function (idx, ele) {
				$(ele).observer({
					validators: [
						function (partial) {
							var $self = $(this);
							var content = "";
							if ($self.hasClass("html-editor")) {
								content = CKEDITOR.instances[$self.attr("id")].getData();
								if (content == $self.attr("placeholder")) content = "";
							} else if ($self.hasClass("sm-textarea")) {
								content = $self.val();
							}
							if ($.trim(content) === "") return "This field is required";
							return "";
						}
					],
					validateOnBlur: true,
					group: "ValidateData"
				});
			});
		}
		function collectSubmitData() {
			var result = {};
			result.contactInfoMsg = _COMMON.getEditorValue("txtContactInfoMsg");
			result.onlineAppPrivacyPolicyMsg = _COMMON.getEditorValue("txtOnlineAppPrivacyPolicyMsg");
			result.fullAppSubmitConfirmationMsg = _COMMON.getEditorValue("txtFullAppSubmitConfirmationMsg");
			result.signedOutPageMsg = _COMMON.getEditorValue("txtSignedOutPageMsg");
			result.getRatesPageDisClaimerMsg = _COMMON.getEditorValue("txtGetRatesPageDisClaimerMsg");
			result.eligibleLoanProgramDisclaimerMsg = _COMMON.getEditorValue("txtEligibleLoanProgramDisclaimerMsg");
			result.noEligibleLoanProgramDisclaimerMsg = _COMMON.getEditorValue("txtNoEligibleLoanProgramDisclaimerMsg");
			result.lenderconfigid = '<%=LenderConfigID%>';
			return result;
		}
	</script>
</asp:Content>
<asp:Content runat="server" ContentPlaceHolderID="plActionButtons"></asp:Content>