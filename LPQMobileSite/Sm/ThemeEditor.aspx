﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="ThemeEditor.aspx.vb" Inherits="Sm_ThemeEditor" MasterPageFile="SiteManager.master" %>
<%@ Import Namespace="Newtonsoft.Json" %>

<asp:Content runat="server" ContentPlaceHolderID="plBody">
	<section class="design-theme-section">
		<div class="preview-panel">
			<div class="switch-btn-panel <%=IIf(PreviewUrlList.Count < 2, "hidden", "")%>">
				<div class="btn-group" role="group" aria-label="...">
					<%	For Each item As KeyValuePair(Of String, String) In PreviewUrlList%>
					<a href="#<%=item.Key%>" class="btn btn-default"><%=GetModuleName(item.Key)%></a>
					<%Next%>
				</div>
			</div>
			<div class="frame-wrapper">
				<div class="spinner">
					<div class="rect1"></div>
					<div class="rect2"></div>
					<div class="rect3"></div>
					<div class="rect4"></div>
					<div class="rect5"></div>
				</div>
				<p>Loading...</p>
		    </div>
			<div id="divPrevPage" class="editor-nav-button hidden">
				<a href="javascript:void(0);">
					<i class="fa fa-chevron-circle-left"></i>
					<span>Previous Page</span>
				</a>
			</div>
			<div id="iframeContainer" style="width: 100%; height: 100%">

			</div>
			<div id="divNextPage" class="editor-nav-button hidden">
				<a href="javascript:void(0);">
					<i class="fa fa-chevron-circle-right"></i>
					<span>Next Page</span>
				</a>
			</div>
		</div>
		<div class="design-panel">
			<form id="frmThemeEditor">
			<h1 class="page-header">Theme</h1>
			<p>Design the appearance of your Application Portal</p>
			<h2 class="section-title">Logo</h2>
			<p>Upload a logo to be displayed on your Application Portal</p>
			<div class="logo-upload-zone dropzone" id="divUploadLogo" data-current-logo="<%=Model.LogoUrl%>"></div>
			<div class="top30">
				<h2 class="section-title">Favicon	</h2>
				<p>Upload a favicon to be displayed on your Application Portal</p>
				<div class="logo-upload-zone dropzone" id="divFavIco" data-current-ico="<%=Model.FavIco%>"></div>
			</div>
			<div class="top30">
				<h2 class="section-title">Colors</h2>
				<p>These are the colors that will appear on your Application Portal.</p>	
				<div class="color-picker-block" >
					<label>Header & Heading</label>
					<div>
						<input type="text" name="HeaderColor" placeholder="Enter HEX code..." class="form-control" id="txtHeaderColor" value="<%=Model.HeaderColor%>" />
						<input type="text" class="picker-btn"/>
					</div>
				</div>
				
				<div class="color-picker-block">
					<label>Selection Button & Link</label>
					<div>
						<input type="text" name="Header2Color" placeholder="Enter HEX code..." class="form-control" id="txtHeader2Color" value="<%=Model.Header2Color%>"/>
						<input type="text" class="picker-btn"/>
					</div>
				</div>
					
				<div class="color-picker-block">
					<label>Action Button</label>
					<div>
						<input type="text" name="ButtonColor" placeholder="Enter HEX code..." class="form-control" id="txtButtonColor" value="<%=Model.ButtonColor%>"/>
						<input type="text" class="picker-btn"/>
					</div>
				</div>
				<div class="color-picker-block" >
					<label>Action Button Font</label>
					<div>
						<input type="text" name="ButtonFontColor" placeholder="Enter HEX code..." class="form-control" id="txtButtonTextColor" value="<%=Model.ButtonFontColor%>"/>
						<input type="text" class="picker-btn"/>
					</div>
				</div>				
				<div class="color-picker-block">
					<label>Content Background</label>
					<div>
						<input type="text" name="ContentColor" placeholder="Enter HEX code..." class="form-control" id="txtContentColor" value="<%=Model.ContentColor%>"/>
						<input type="text" class="picker-btn"/>
					</div>
				</div>
				<div class="color-picker-block">
					<label>Footer Background</label>
					<div>
						<input type="text" name="FooterColor" placeholder="Enter HEX code..." class="form-control" id="txtFooterColor" value="<%=Model.FooterColor%>"/>
						<input type="text" class="picker-btn"/>
					</div>
				</div>
				<div class="color-picker-block">
					<label>Footer Font</label>
					<div>
						<input type="text" name="FooterFontColor" placeholder="Enter HEX code..." class="form-control" id="txtFooterFontColor" value="<%=Model.FooterFontColor%>"/>
						<input type="text" class="picker-btn"/>
					</div>
				</div>				
				<div class="color-picker-block">
					<label>Outside Background</label>
					<div>
						<input type="text" name="BackgroundColor" placeholder="Enter HEX code..." class="form-control" id="txtBackgroundColor" value="<%=Model.BackgroundColor%>"/>
						<input type="text" class="picker-btn"/>
					</div>
				</div>
			</div>
			<div class="top30">
				<h2 class="section-title">Resources for ADA Compliance </h2>
				<p>These are some resources that you can use to validate your Application Portal for meeting W3C Web Content Accessibility Guidelines (WCAG 2.0 AA).</p>
				<div><b>Website</b>: "http://webaim.org/resources/contrastchecker" </div><br />
				<div><b>Chrome browser plugin (WAVE)</b>: "https://chrome.google.com/webstore/detail/wave-evaluation-tool/jbbplnpkjmmeebjpijfedlgcdilocofh?hl=en-US" </div>	<br />
				<div><b>WCAG 2.0 Guidelines:</b>: "http://www.interactiveaccessibility.com/services/wcag-20-compliance" </div><br />		
			</div>
			</form>
		</div>
	</section>
	<div></div>
</asp:Content>
<asp:Content runat="server" ContentPlaceHolderID="head">
	<link rel="stylesheet" type="text/css" href="js/spectrum/spectrum.min.css" />
	</asp:Content>
<asp:Content runat="server" ContentPlaceHolderID="plScripts">
	<script src="js/spectrum/spectrum.min.js"></script>
	<script type="text/javascript" src="js/easyxdm/easyXDM.debug.js"></script>
	<script type="text/javascript">
		var previewUrlArr = <%=JsonConvert.SerializeObject(PreviewUrlList)%>;
		var initFrame = function(id) {
			$(".preview-panel .frame-wrapper").show();
			$("#iframeContainer iframe").remove();
			$("a.btn.active", ".switch-btn-panel").removeClass("active");
			$("a[href='#" + id + "']", ".switch-btn-panel").addClass("active");

			if (id == "LQB") {
				$("#divPrevPage, #divNextPage").removeClass("hidden");
				$(".preview-panel, .design-panel").addClass("expanded-navigation-buttons");
			} else {
				$("#divPrevPage, #divNextPage").addClass("hidden");
				$(".preview-panel, .design-panel").removeClass("expanded-navigation-buttons");
			}

			colorPickerBlockShowHideController(id);
			var rpcSetup = {
				remote: previewUrlArr[id],
				container: document.getElementById("iframeContainer"), // the path to the provider
			};
			var _rpc = new easyXDM.Rpc(rpcSetup,
			{
				local: {
					frameReady: function(data) {
						rpcSetup.container.getElementsByTagName("iframe")[0].width = "100%";
						rpcSetup.container.getElementsByTagName("iframe")[0].height = "100%";
						var preset = $.parseJSON(collectSubmitData().themeData);
						rpc.setLogoUrl(preset.LogoUrl);
						$(".preview-panel .frame-wrapper").hide();
						if ($.trim(preset.ButtonColor) !== "") {
							rpc.setButtonColor(preset.ButtonColor);
						}
						if ($.trim(preset.ButtonFontColor) !== "") {
							rpc.setButtonTextColor(preset.ButtonFontColor);
						}
						if ($.trim(preset.FooterColor) !== "") {
							rpc.setFooterColor(preset.FooterColor);
						}
						if (preset.FooterFontColor != null && $.trim(preset.FooterFontColor) !== "") {
							rpc.setFooterFontColor(preset.FooterFontColor);
						} else if(preset.FooterColor != null && $.trim(preset.FooterColor) !== "") {
							rpc.setFooterFontColor(getContrastYIQ(preset.FooterColor));
							$("#txtFooterFontColor").val(getContrastYIQ(preset.FooterColor)).trigger("blur");
						}
						if ($.trim(preset.HeaderColor) !== "") {
							rpc.setHeaderColor(preset.HeaderColor);
						}
						if ($.trim(preset.Header2Color) !== "") {
							rpc.setHeader2Color(preset.Header2Color);
						}
						if ($.trim(preset.BackgroundColor) !== "") {
							rpc.setBackgroundColor(preset.BackgroundColor);
						}
						if ($.trim(preset.ContentColor) !== "") {
							rpc.setContentColor(preset.ContentColor);
						}						
					},
					submitForm: function(data) {
						rpc = submitFrame(data, id, $.parseJSON(collectSubmitData().themeData));
					},
					canGoToPreviousQuickNavPage: function (bValue) {
						if (bValue == true) {
							$("#divPrevPage a").removeClass("disabled");
						} else {
							$("#divPrevPage a").addClass("disabled");
						}
						
					},
					canGoToNextQuickNavPage: function (bValue) {
						if (bValue == true) {
							$("#divNextPage a").removeClass("disabled");
						} else {
							$("#divNextPage a").addClass("disabled");
						}
					},
				},
				remote: {
					setLogoUrl: {},
					setFooterColor: {},
					setFooterFontColor: {},
					setButtonColor: {},
					setHeaderColor: {},
					setHeader2Color: {},
					setContentColor: {},
					setBackgroundColor: {},
					setButtonTextColor: {},
					goToPreviousQuickNavPage: {},
					goToNextQuickNavPage: {},
				}
			});
			return _rpc;
		}
		var submitFrame = function(url, id, preset) {
			$(".preview-panel .frame-wrapper").show();
			$("#iframeContainer iframe").remove();
			colorPickerBlockShowHideController(id);
			var rpcSetup = {
				remote: url,
				container: document.getElementById("iframeContainer"), // the path to the provider
			};
			var _rpc = new easyXDM.Rpc(rpcSetup,
			{
				local: {
					frameReady: function(data) {
						rpcSetup.container.getElementsByTagName("iframe")[0].width = "100%";
						rpcSetup.container.getElementsByTagName("iframe")[0].height = "100%";
						$(".preview-panel .frame-wrapper").hide();
						console.log(preset);
						rpc.setLogoUrl(preset.LogoUrl);
						if ($.trim(preset.ButtonColor) !== "") {
							rpc.setButtonColor(preset.ButtonColor);
						}
						if ($.trim(preset.ButtonFontColor) !== "") {
							rpc.setButtonTextColor(preset.ButtonFontColor);
						}
						if ($.trim(preset.FooterColor) !== "") {
							rpc.setFooterColor(preset.FooterColor);
						}

						if (preset.FooterFontColor != null && $.trim(preset.FooterFontColor) !== "") {
							rpc.setFooterFontColor(preset.FooterFontColor);
						} else if(preset.FooterColor != null && $.trim(preset.FooterColor) !== "") {
							rpc.setFooterFontColor(getContrastYIQ(preset.FooterColor));
							$("#txtFooterFontColor").val(getContrastYIQ(preset.FooterColor)).trigger("blur");
						}

						if ($.trim(preset.HeaderColor) !== "") {
							rpc.setHeaderColor(preset.HeaderColor);
						}
						if ($.trim(preset.Header2Color) !== "") {
							rpc.setHeader2Color(preset.Header2Color);
						}
						if ($.trim(preset.BackgroundColor) !== "") {
							rpc.setBackgroundColor(preset.BackgroundColor);
						}
						if ($.trim(preset.ContentColor) !== "") {
							rpc.setContentColor(preset.ContentColor);
						}						
						
					},
					submitForm: function(data) {
						rpc = submitFrame(data, id, $.parseJSON(collectSubmitData().themeData));
					},
					canGoToPreviousQuickNavPage: function () {
						if (bValue == true) {
							$("#divPrevPage a").removeClass("disabled");
						} else {
							$("#divPrevPage a").addClass("disabled");
						}
						
					},
					canGoToNextQuickNavPage: function () {
						if (bValue == true) {
							$("#divNextPage a").removeClass("disabled");
						} else {
							$("#divNextPage a").addClass("disabled");
						}
					},
				},
				remote: {
					setLogoUrl: {},
					setFooterColor: {},
					setFooterFontColor: {},
					setButtonColor: {},
					setHeaderColor: {},
					setHeader2Color: {},
					setContentColor: {},
					setBackgroundColor: {},
					setButtonTextColor: {},
					goToPreviousQuickNavPage: {},
					goToNextQuickNavPage: {},
				}
			});
			return _rpc;
		}
		var initItem = $("a.btn", ".switch-btn-panel").first().attr("href").replace("#", "");
		if (window.location.hash !== "") {
			initItem = window.location.hash.replace("#", "");
		}
		var rpc = initFrame(initItem);
		var currentLogoFile = null;
		var currentFavIcoFile = null;
		Dropzone.autoDiscover = false;
		$(function () {
			registerDataValidator();
			$("#divUploadLogo").dropzone({
				url: "/sm/smhandler.aspx",
				dictDefaultMessage: "<p>Drop logo file here or click to select image from your computer</p><p>Compatible file types: JPG, PNG. Recommended approximate dimension: 350 x 200.</p><p>Recommended file size: 10KB.</p>",
				maxFilesize: 2,
				acceptedFiles: "image/png,image/jpeg",
				params: { "command": "uploadlogo" },
				maxFiles: 1,
				addRemoveLinks: true,
				dictRemoveFile: "Remove",
				maxfilesexceeded: function (file) {
					this.removeAllFiles();
					this.addFile(file);
				},
				thumbnailWidth: 200,
				thumbnailHeight: null,
				init: function () {
					currentLogoFile = null;
					this.on("addedfile", function(file) {
						if (currentLogoFile) {
							this.removeFile(currentLogoFile);
						}
						currentLogoFile = file;
					});
					this.on("success", function(file, response) {
						//var container = this.element;
						rpc.setLogoUrl(response);
						master.FACTORY.documentChanged($("#divUploadLogo"));
						$("#divUploadLogo").data("new-logo", response);
						currentLogoFile = file;
					});
					this.on("removedfile", function (file) {
						currentLogoFile = null;
						if (file.xhr) {
							$.ajax({
								url: "/sm/smhandler.aspx",
								async: true,
								cache: false,
								type: 'POST',
								dataType: 'html',
								data: { command: 'deletelogo', logourl: file.xhr.responseText },
								success: function(responseText) {
									rpc.setLogoUrl($("#divUploadLogo").data("current-logo"));
									//$("#divUploadLogo").data("new-logo", $("#divUploadLogo").data("current-logo"));
									master.FACTORY.documentChanged($("#divUploadLogo"));
									$("#divUploadLogo").removeData("new-logo");
								}
							});
						} else {
							$("#divUploadLogo").removeData("new-logo");
						}
						
					});
				}
			});

			$("#divFavIco").dropzone({
				url: "/sm/smhandler.aspx",
				dictDefaultMessage: "<p>Drop favicon file here or click to select image from your computer</p><p>Compatible file types: ICO, PNG.</p><p>Recommended file size: 10KB.</p>",
				maxFilesize: 2,
				acceptedFiles: "image/x-icon,image/png",
				params: { "command": "uploadfavico" },
				maxFiles: 1,
				addRemoveLinks: true,
				dictRemoveFile: "Remove",
				maxfilesexceeded: function (file) {
					this.removeAllFiles();
					this.addFile(file);
				},
				thumbnailWidth: 200,
				thumbnailHeight: null,
				init: function () {
					currentFavIcoFile = null;
					this.on("addedfile", function(file) {
						if (currentFavIcoFile) {
							this.removeFile(currentFavIcoFile);
						}
						currentFavIcoFile = file;
					});
					this.on("success", function(file, response) {
						//var container = this.element;
						//rpc.setFavico(response);
						master.FACTORY.documentChanged($("#divFavIco"));
						$("#divFavIco").data("new-ico", response);
						currentFavIcoFile = file;
					});
					this.on("removedfile", function (file) {
						currentFavIcoFile = null;
						if (file.xhr) {
							$.ajax({
								url: "/sm/smhandler.aspx",
								async: true,
								cache: false,
								type: 'POST',
								dataType: 'html',
								data: { command: 'deletefavico', favico: file.xhr.responseText },
								success: function(responseText) {
									//$("#divFavIco").data("new-ico", $("#divFavIco").data("current-ico"));
									master.FACTORY.documentChanged($("#divFavIco"));
									$("#divFavIco").removeData("new-ico");
								}
							});
						} else {
							$("#divFavIco").removeData("new-ico");
						}
						
					});
				}
			});

			var bIsInitializingColorPickers = true;
			$("#txtButtonColor").colorPicker({ callback: function (hexCode) { rpc.setButtonColor(hexCode); master.FACTORY.documentChanged($("#txtButtonColor"));} });
			$("#txtButtonTextColor").colorPicker({ callback: function (hexCode) { rpc.setButtonTextColor(hexCode); master.FACTORY.documentChanged($("#txtButtonTextColor")); } });
			$("#txtFooterColor").colorPicker({ callback: function(hexCode) {
				rpc.setFooterColor(hexCode); 
				if (!bIsInitializingColorPickers) {
					rpc.setFooterFontColor(getContrastYIQ(hexCode));
					$("#txtFooterFontColor").val(getContrastYIQ(hexCode)).trigger("blur");
				}
				master.FACTORY.documentChanged($("#txtFooterColor"));
			} });
			$("#txtFooterFontColor").colorPicker({ callback: function (hexCode) { rpc.setFooterFontColor(hexCode); master.FACTORY.documentChanged($("#txtFooterFontColor"));} });
			$("#txtHeaderColor").colorPicker({ callback: function (hexCode) { rpc.setHeaderColor(hexCode); master.FACTORY.documentChanged($("#txtHeaderColor"));} });
			$("#txtHeader2Color").colorPicker({ callback: function (hexCode) { rpc.setHeader2Color(hexCode); master.FACTORY.documentChanged($("#txtHeader2Color"));} });
			$("#txtBackgroundColor").colorPicker({ callback: function (hexCode) { rpc.setBackgroundColor(hexCode); master.FACTORY.documentChanged($("#txtBackgroundColor"));} });
			$("#txtContentColor").colorPicker({ callback: function (hexCode) { rpc.setContentColor(hexCode); master.FACTORY.documentChanged($("#txtContentColor")); } });
			bIsInitializingColorPickers = false;
			$("body").data("hasChanged", false); //reset the flag after colorPickers initial
			$("a.btn", ".switch-btn-panel").each(function(idx, ele) {
				var $self = $(ele);
				$self.on("click", function() {
					var key = $self.attr("href").replace("#", "");
					rpc = initFrame(key);
				});
			});
			$(".color-picker-block input:text:not(.picker-btn)").on("keyup paste", function() {
				var $self = $(this);
				var hexCode = $self.val();
				if (/^#[A-Za-z0-9]{6}$/.test($self.val())) {
					$self.trigger("changeValue");
				}
			});

			$("#divPrevPage").on("click", function () {
				$("#divPrevPage, #divPrevPage *").blur();
				rpc.goToPreviousQuickNavPage();
			});
			$("#divNextPage").on("click", function () {
				$("#divNextPage, #divNextPage *").blur();
				rpc.goToNextQuickNavPage();
			});
		});
		(function (master, $, undefined) {
			master.FACTORY.SaveDraft = function (revisionId) {
				if ($.smValidate("ValidateData") == false) return;
				var dataObj = collectSubmitData();
				dataObj.command = "savedesigntheme";
				dataObj.revisionId = revisionId;
				$.ajax({
					url: "/sm/smhandler.aspx",
					async: true,
					cache: false,
					type: 'POST',
					dataType: 'html',
					data: dataObj,
					success: function (responseText) {
						var response = $.parseJSON(responseText);
						if (response.IsSuccess) {
							master.FACTORY.onSaveDraftSuccess();
						} else {
							_COMMON.noty("error", "Error", 500);
						}
					}
				});
			};
			master.FACTORY.validateBeforePublish = function () {
				return $.smValidate("ValidateData");
			}
			master.FACTORY.Publish = function (comment, publishAll) {
				if ($.smValidate("ValidateData") == false) return;
				var dataObj = collectSubmitData();
				dataObj.command = "publishdesigntheme";
				dataObj.comment = comment; 
				dataObj.publishAll = publishAll;
				$.ajax({
					url: "/sm/smhandler.aspx",
					async: true,
					cache: false,
					type: 'POST',
					dataType: 'html',
					data: dataObj,
					success: function (responseText) {
						var response = $.parseJSON(responseText);
						if (response.IsSuccess) {
							master.FACTORY.onPublishSuccess();
						} else {
							_COMMON.noty("error", "Error", 500);
						}
					}
				});
			};
		}(window.master = window.master || {}, jQuery));
		function registerDataValidator() {
			$.smValidate.removeValidationGroup("ValidateData");
			$(".color-picker-block input.form-control").each(function (idx, ele) {
				$(ele).observer({
					validators: [
						function (partial) {
							var $self = $(this);
							if (/^#[A-Za-z0-9]{6}$/.test($self.val()) || $self.val() === '') {
								return "";
							}
							return "Invalid color";
						}
					],
					validateOnBlur: true,
					group: "ValidateData"
				});
			});
		}
		function collectSubmitData() {
			var result = {};
			result.lenderconfigid = '<%=LenderConfigID%>';
			var themeData = {};
			themeData = $("#frmThemeEditor").serializeJSON({ parseAll: true, checkboxUncheckedValue: "false" });
			if (typeof $("#divUploadLogo").data("new-logo") == "undefined") {
				themeData.LogoUrl = $("#divUploadLogo").data("current-logo");
			} else {
				themeData.LogoUrl = $("#divUploadLogo").data("new-logo");
			}
			if (typeof $("#divFavIco").data("new-ico") == "undefined") {
				themeData.FavIco = $("#divFavIco").data("current-ico");
			} else {
				themeData.FavIco = $("#divFavIco").data("new-ico");
			}
			result.themeData = _COMMON.toJSON(themeData);
			return result;
		}
		function colorPickerBlockShowHideController(id) {
			if (id == "LQB") {
				$("#txtContentColor").closest("div.color-picker-block").addClass("hidden");
			} else {
				$("#txtContentColor").closest("div.color-picker-block").removeClass("hidden");
			}
		}
		function getContrastYIQ(bgColor) {
			bgColor = bgColor.replace("#", "");
			var r = parseInt(bgColor.substr(0, 2), 16);
			var g = parseInt(bgColor.substr(2, 2), 16);
			var b = parseInt(bgColor.substr(4, 2), 16);
			var yiq = ((r * 299) + (g * 587) + (b * 114)) / 1000;
			//yiq max =255, min =0
			//179 = 70% of max, 128 =50% of max
			return (yiq >= 179) ? '#000000' : '#ffffff';
		}
	</script>
</asp:Content>
<asp:Content runat="server" ContentPlaceHolderID="plActionButtons"></asp:Content>