﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="Profile.aspx.vb" Inherits="Sm_Profile" MasterPageFile="MasterPage.master" %>

<asp:Content runat="server" ContentPlaceHolderID="plBody">
	<div>
		<h1 class="page-header">Profile</h1>
		<p>Manage all your information and personal settings.</p>
		<section id="profiles">
			<ul class="nav nav-tabs" role="tablist">
				<li role="presentation"><a href="#profile" aria-controls="profile" role="tab" data-toggle="tab">Profile</a></li>
				<li role="presentation"><a href="#password" aria-controls="password" role="tab" data-toggle="tab">Change Password</a></li>
			</ul>
			<div class="tab-content">
				<div role="tabpanel" class="tab-pane active" id="profile">
					<form id="frmProfile">
						<div class="row">
							<div class="col-sm-6 col-lg-4">
								<div class="form-group">
									<label class="required-field">First Name</label>
									<input type="text" class="form-control" name="FirstName" id="txtFirstName" value="<%=profileData.FirstName%>" placeholder="First Name" data-message-require="Please fill out this field" data-required="true" maxlength="50" />
								</div>
								<div class="form-group">
									<label class="required-field">Last Name</label>
									<input type="text" class="form-control" name="LastName" id="txtLastName" placeholder="Last Name" value="<%=profileData.LastName%>" data-message-require="Please fill out this field" data-required="true" maxlength="50" />
								</div>
								<%--Todo: next phase, allow user change their email. After we have done with action logs--%>
								<%--<div class="form-group">
									<label class="required-field">Email</label>
									<input type="text" class="form-control" name="Email" id="txtEmail" value="<%=profileData.Email%>" placeholder="First Name" data-message-require="Please fill out this field" data-required="true" maxlength="50" />
								</div>--%>
								<div class="form-group">
									<label>Avatar</label>
									<div class="logo-upload-zone dropzone" id="divAvatar" data-current-logo=""></div>
									<%If AvatarFileInfo IsNot Nothing Then%>
									<input type="hidden" value="<%=profileData.Avatar%>" name="Avatar" id="hdAvatar" data-file-ext="<%=AvatarFileInfo.Extension%>" data-file-name="<%=AvatarFileInfo.Name%>" data-file-size="<%=AvatarFileInfo.Length%>"/>
									<%Else%>
									<input type="hidden" value="<%=profileData.Avatar%>" name="Avatar" id="hdAvatar" data-file-ext="" data-file-name="filename" data-file-size="0"/>
									<%End If%>
								</div>
								<div class="form-group">
									<label class="required-field">Phone Number</label>
									<input type="text" class="form-control" name="Phone" id="txtPhone" value="<%=profileData.Phone%>" data-message-invalid-data="Valid phone number is required" data-format="phone" placeholder="Phone Number" maxlength="20" />
								</div>
								<div>
									<button type="button" id="btnSaveProfile" class="btn btn-primary pull-right">Save changes</button>
								</div>
							</div>
						</div>
					</form>
				</div>
				<div role="tabpanel" class="tab-pane" id="password">
					<form id="frmChangePassword">
						<div class="row">
							<div class="col-sm-6 col-lg-4">
								<div class="form-group">
									<label class="required-field">Current Password</label>
									<input type="password" class="form-control" name="CurrentPassword" id="txtCurrentPassword" data-message-require="Please fill out this field" data-required="true" placeholder="Password" maxlength="50" />
								</div>
								<div class="form-group">
									<label class="required-field">New Password</label>
									<input type="password" class="form-control" name="NewPassword" id="txtPassword" data-format="password" data-message-require="Please fill out this field" data-required="true" placeholder="Password" maxlength="50" />
								</div>
								<div class="form-group">
									<label class="required-field">Confirm New Password</label>
									<input type="password" class="form-control" name="ConfirmNewPassword" data-format="confirmPassword" id="txtConfirmPassword" data-password-not-match="Confirm password is not match." data-message-require="Please fill out this field" data-required="true" placeholder="Confirm Password" maxlength="50" />
								</div>
								<div>
									<button type="button" id="btnChangePassword" class="btn btn-primary pull-right">Save changes</button>
								</div>
							</div>
						</div>
					</form>
				</div>
			</div>
		</section>
	</div>
</asp:Content>
<asp:Content runat="server" ContentPlaceHolderID="plScripts">
	<script type="text/javascript">

		$(function () {
			$('.nav-tabs a').on('shown.bs.tab', function (e) {
				window.location.hash = e.target.hash;
				window.scrollTo(0, 0);
			});
			var url = document.location.toString();
			if (url.match('#') && $.trim(url.split('#')[1]) != "") {
				$('.nav-tabs a[href="#' + url.split('#')[1] + '"]').tab('show');
			} else {
				var tab = $("div.tab-content >.tab-pane:first-child").attr("id");
				$('.nav-tabs a[href="#' + tab + '"]').tab('show');
			}
			registerDataValidator($("#frmProfile"), "validateProfile");
			registerDataValidator($("#frmChangePassword"), "validatePassword");
			$("#btnSaveProfile").on("click", manageProfile.FACTORY.SaveProfile);
			$("#btnChangePassword").on("click", manageProfile.FACTORY.ChangePassword);

			var currentLogoFile = null;
			$("#divAvatar").dropzone({
				url: "/sm/smhandler.aspx",
				dictDefaultMessage: "<p>Drop image file here or click to select image from your computer</p><p>Compatible file types: JPG, PNG. Recommended approximate dimension: 200 x 200.</p><p>Recommended file size: 10KB.</p>",
				maxFilesize: 2,
				acceptedFiles: "image/png,image/jpeg",
				params: { "command": "uploadavatar" },
				maxFiles: 1,
				addRemoveLinks: true,
				dictRemoveFile: "Remove",
				maxfilesexceeded: function (file) {
					this.removeAllFiles();
					this.addFile(file);
				},
				thumbnailWidth: 200,
				thumbnailHeight: null,
				init: function () {
					var $hdLogo = $("#hdAvatar");
					currentLogoFile = null;
					if ($hdLogo.val() != "") {
						var mockFile = { name: $hdLogo.data("file-name"), size: $hdLogo.data("file-size") /*, type: 'image/png'*/, url: $("#hdAvatar").val() };
						this.options.addedfile.call(this, mockFile);
						this.options.thumbnail.call(this, mockFile, $("#hdAvatar").val());
						mockFile.previewElement.classList.add('dz-success');
						mockFile.previewElement.classList.add('dz-complete');
						$(mockFile.previewElement).find("img").attr("width", 200);
						currentLogoFile = mockFile;
					}
					this.on("addedfile", function (file) {
						if (currentLogoFile) {
							this.removeFile(currentLogoFile);
						}
						currentLogoFile = file;
					});
					this.on("success", function (file, response) {
						$("#hdAvatar").val(response);
						$.smValidate.hideValidation($("#hdAvatar").closest(".form-group"));
						currentLogoFile = file;
					});
					this.on("removedfile", function (file) {
						currentLogoFile = null;
						if (file.xhr) {
							$.ajax({
								url: "/sm/smhandler.aspx",
								async: true,
								cache: false,
								type: 'POST',
								dataType: 'html',
								data: { command: 'deleteavatar', fileurl: file.xhr.responseText },
								success: function (responseText) {
									$("#hdAvatar").val("");
								}
							});
						} else {
							$("#hdAvatar").val("");
						}

					});
				}
			});
		});
		(function (manageProfile, $, undefined) {
			manageProfile.DATA = {};
			manageProfile.FACTORY = {};
			manageProfile.FACTORY.SaveProfile = function () {
				if ($.smValidate("validateProfile") == false) return;
				var profileData = $("#frmProfile").serializeJSON({ parseAll: true, checkboxUncheckedValue: "false" });
				//update data
				var postData = {
					command: "saveProfile",
					profile_data: _COMMON.toJSON(profileData)
				};
				$.ajax({
					url: "/sm/smhandler.aspx",
					async: true,
					cache: false,
					type: 'POST',
					dataType: 'html',
					data: postData,
					success: function (responseText) {
						var response = $.parseJSON(responseText);
						if (response.IsSuccess) {
							_COMMON.noty("success", "Success", 500);
							$(".sm-header .profile-block span.avatar").css("background-image", "url('" + response.Info.avatar + "')");
							$(".sm-header .profile-block span.js-profile-firstname").text(response.Info.firstName);

						} else {
							var errorMsg = response.Message !== "" ? response.Message : "Error";
							_COMMON.noty("error", errorMsg, 500);
						}
					}
				});
			};
			manageProfile.FACTORY.ChangePassword = function () {
				if ($.smValidate("validatePassword") == false) return;
				var passwordData = $("#frmChangePassword").serializeJSON({ parseAll: true, checkboxUncheckedValue: "false" });
				//update data
				var postData = {
					command: "changePassword",
					password_data: _COMMON.toJSON(passwordData)
				};
				$.ajax({
					url: "/sm/smhandler.aspx",
					async: true,
					cache: false,
					type: 'POST',
					dataType: 'html',
					data: postData,
					success: function (responseText) {
						var response = $.parseJSON(responseText);
						if (response.IsSuccess) {
							_COMMON.noty("success", "Success", 500);
							$("#frmChangePassword")[0].reset();
						} else if (response.Info == "INCORRECT") {
							$.smValidate.showValidation($("#txtCurrentPassword"), response.Message);
						} else if (response.Info == "INVALID") {
							$.smValidate.showValidation($("#txtPassword"), response.Message);
						} else {
							var errorMsg = response.Message !== "" ? response.Message : "Error";
							_COMMON.noty("error", errorMsg, 500);
						}
					}
				});
			};
		}(window.manageProfile = window.manageProfile || {}, jQuery));
		function registerDataValidator(container, groupName) {
			var $container = $(container);
			$.smValidate.removeValidationGroup(groupName);
			$("[data-required='true'],[data-format]", $container).each(function (idx, ele) {
				$(ele).observer({
					validators: [
						function (partial) {
							var $self = $(this);
							if ($self.data("required") && $.trim($self.val()) === "") {
								if ($self.data("message-require")) {
									return $self.data("message-require");
								} else {
									return "This field is required";
								}
							}
							var format = $self.data("format");
							if (format) {
								var msg = "Invalid format";
								if ($self.data("message-invalid-data")) {
									msg = $self.data("message-invalid-data");
								}
								if (format === "email") {
									if (_COMMON.ValidateEmail($self.val()) === false) {
										return msg;
									}
								} else if (format === "phone") {
									if (_COMMON.ValidatePhone($self.val()) === false) {
										return msg;
									}
								} else if (format === "password") {
									var result = _COMMON.validatePassword($self.val());
									if ( result != "") {
										return result;
									}
								} else if (format === "confirmPassword") {
									if ($self.val() !== $("#txtPassword").val()) {
										return $self.data("password-not-match");
									}
								} else if ($self.val() != "" && format === "positive-number") {
									if ((parseInt($self.val()) > 0) == false) {
										return msg;
									}
								}
							}

							return "";
						}
					],
					validateOnBlur: true,
					group: groupName
				});
			});
		}
	</script>
</asp:Content>
<asp:Content runat="server" ContentPlaceHolderID="plActionButtons">
</asp:Content>