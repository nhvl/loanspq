﻿

Imports CustomApplicationScenarios
Imports System.Globalization
Imports Newtonsoft.Json
Imports LPQMobile.Utils
Imports System.Xml
Imports Microsoft.Ajax.Utilities

Partial Class Sm_CustomApplicationScenarios
	Inherits SmApmBasePage

	Protected Property CurrentTab As String = ""
	Protected Property CurrentModule As SmSettings.ModuleName
	Protected Property TabTitle As String = ""
	Protected Property CustomApplicationScenariosListJson As String = "[]"
	Protected Property BusinessAccountTypeDic As New Dictionary(Of String, String)
	Protected Property SpecialAccountTypeDic As New Dictionary(Of String, String)
	Protected Property AllProductListJson As String = "[]"
	Protected Property ProductsByAvaibilityJson() As String = "[]"

	Private Sub LoadModule(smMasterPage As Sm_SiteManager, ByRef loanNodeName As String, ByRef tabName As String)
		Select Case CurrentTab
			Case "XA"
				CurrentTab = "XA"
				CurrentModule = SmSettings.ModuleName.XA
				loanNodeName = "XA_LOAN"
				smMasterPage.CurrentNodeGroup = SmSettings.NodeGroup.XACustomApplicationScenarios
				tabName = "XA"
				TabTitle = "Xpress Account Application Scenarios"
			Case Else
				Throw New Exception(String.Format("Unknown tab value '{0}'", TAB))
		End Select
	End Sub

	Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
		If Not Page.IsPostBack Then
			Dim smMasterPage = CType(Me.Master, Sm_SiteManager)
			CurrentTab = Common.SafeEncodeString(Request.QueryString("castab")).ToUpper()
			'loanNodeName will be used in the future when we implement Custom Application Scenarios for another application types
			Dim loanNodeName As String = ""
			Dim tabName As String = ""
			Select Case CurrentTab
				' If a specific module was specified, load that
				Case "XA"
					LoadModule(smMasterPage, loanNodeName, tabName)
				Case Else
					' Else, a specific was not specified, and we should try defaulting to the first available enabled module
					If EnableXA Then
						CurrentTab = "XA"
					End If

					If Not String.IsNullOrEmpty(CurrentTab) Then
						LoadModule(smMasterPage, loanNodeName, tabName)
					End If
			End Select

			If String.IsNullOrEmpty(CurrentTab) OrElse Not CheckEnabledModule(CurrentModule) Then
				'terminate current page
				Response.Redirect(BuildUrl("/sm/index.aspx", New List(Of String) From {"castab"}), True)
			End If


			smMasterPage.LenderConfigXml = LenderConfigXml
			smMasterPage.SelectedMainMenuItem = "config"
			smMasterPage.RevisionID = LenderConfig.RevisionID
			smMasterPage.LastModifiedBy = LenderConfig.LastModifiedByUserFullName
			smMasterPage.LastModifiedByUserID = LenderConfig.LastModifiedByUserID
			smMasterPage.SelectedSubMenuItem = "customapplicationscenarios"
			smMasterPage.BreadCrumb = "Configure / Custom Application Scenarios/ " & tabName
			Try
				Dim customApplicationScenariosNode As XmlNode = GetNode(Sm.CUSTOM_APPLICATION_SCENARIOS(loanNodeName))
				If customApplicationScenariosNode IsNot Nothing Then
					CustomApplicationScenariosListJson = customApplicationScenariosNode.InnerText
				End If
				Dim businessAccountTypeList = Common.GetBusinessAccountTypeList(WebsiteConfig)
				If businessAccountTypeList IsNot Nothing AndAlso businessAccountTypeList.Count > 0 Then
					For Each accTypeItem In businessAccountTypeList
						If Not BusinessAccountTypeDic.ContainsKey(accTypeItem.AccountCode) Then
							BusinessAccountTypeDic.Add(accTypeItem.AccountCode, CEnum.MapBusinessTypeName(accTypeItem.BusinessType))
						End If
					Next
				End If
				Dim specialAccountTypeList = Common.GetSpecialAccountTypeList(WebsiteConfig)
				If specialAccountTypeList IsNot Nothing AndAlso specialAccountTypeList.Count > 0 Then
					For Each accTypeItem In specialAccountTypeList
						If Not SpecialAccountTypeDic.ContainsKey(accTypeItem.AccountCode) Then
							SpecialAccountTypeDic.Add(accTypeItem.AccountCode, accTypeItem.AccountName)
						End If
					Next
				End If
				Dim allProducts As New List(Of ProductInfo)
				Dim allProductsGroupByAvaibility As New Dictionary(Of String, List(Of ProductInfo))
				Dim tempAccountInfo As ProductInfo
				For Each item In CProduct.GetAllProductsGroupByAvailability(WebsiteConfig)
					Dim pList As New List(Of ProductInfo)
					If item.Value IsNot Nothing AndAlso item.Value.Count > 0 Then
						For Each prod In item.Value
							tempAccountInfo = New ProductInfo
							tempAccountInfo.Name = prod.AccountName
							tempAccountInfo.IsRequired = prod.IsRequired
							tempAccountInfo.IsPreselected = prod.IsPreSelection
							tempAccountInfo.Code = prod.ProductCode
							tempAccountInfo.Type = prod.AccountType
							allProducts.Add(tempAccountInfo)
							pList.Add(tempAccountInfo)
						Next
					End If
					allProductsGroupByAvaibility.Add(item.Key, pList.OrderBy(Function(p) p.Name).ToList())
				Next
				AllProductListJson = JsonConvert.SerializeObject(allProducts.DistinctBy(Function(p) p.Name))
				ProductsByAvaibilityJson = JsonConvert.SerializeObject(allProductsGroupByAvaibility)
			Catch ex As Exception
				log.Error("Could not load Custom Application Scenarios page", ex)
			End Try
		End If
	End Sub
	Public Class ProductInfo
		Public Property Name As String
		Public Property Code As String
		Public Property IsRequired As Boolean
		Public Property Type As String
		Public ReadOnly Property MappedTypeName As String
			Get
				Return Common.MapProductType2String(Type)
			End Get
		End Property
		Public Property IsPreselected As Boolean

	End Class
End Class