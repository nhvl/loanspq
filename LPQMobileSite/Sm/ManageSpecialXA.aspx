﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="ManageSpecialXA.aspx.vb" Inherits="Sm_ManageSpecialXA" MasterPageFile="SiteManager.master" ValidateRequest="false"%>
<%@ Import Namespace="LPQMobile.Utils" %>

<asp:Content runat="server" ContentPlaceHolderID="plBody">
	<div>
		<h1 class="page-header">Manage Xpress Account Application Settings</h1>
		<p>Use this page to manage what is available for your Application Portal, including Purpose Types, Decision Messages, and Disclosures</p>
		<section>
			<ul class="nav nav-tabs" id="mainTabs" role="tablist">
				<li role="presentation"><a href="<%=BuildUrl("/sm/managestandardxa.aspx")%>" aria-controls="xa" role="tab">Personal XA</a></li>
				<li role="presentation" class="active"><a href="#" role="tab">Special XA</a></li>
				<li role="presentation"><a href="<%=BuildUrl("/sm/managebusinessxa.aspx")%>" aria-controls="xa" role="tab">Business XA</a></li>
			</ul>
			<div class="tab-content">
				<div role="tabpanel" class="tab-pane active" id="xa">
					<div class="bottom30">
						<h2 class="section-title">HTML</h2>
						<p><%:IIf(IsBankPortal, "This text will be displayed on the first page of the New Account application.", "This text will be displayed in Eligibility page of the New Membership application.")%></p>
						<div>
							<h3 class="property-title"><%:IIf(IsBankPortal, "Getting Started Text", "Eligibility Text")%></h3>
							<p>System will use default text template if this area is empty.  System will not display <%:IIf(IsBankPortal, "Getting Started Text", "Eligibility Text")%> if this area has at least one empty space.  Use class from the default text template for responsive text.</p>
							<div class="html-editor" contenteditable="true" data-required="false" id="EligibilityMessage"><%=EligibilityMessage%></div>
							<div class="pull-right buttons mt5">
								<button type="button" class="btn btn-primary" id="btnLoadDefaultEligibilityMessage"><i class="fa fa-plus" aria-hidden="true"></i>Load Default Template</button>
								<%--<button class="sm-btn" data-toggle="modal" data-target="#mdlAddDisclosure"><i class="fa fa-plus" aria-hidden="true"></i>Add Disclosure</button>--%>
							</div>
							<div id="divDefaultEligibilityMessage" class="hidden"><%=Common.GenerateDefaultEligibilityMessage()%></div>
						</div>
					</div>
					<br/>
					<section>
						<h2 class="section-title">Messages</h2>
						<p>These are the messages that display depending on the applicant’s status</p>
						<div>
							<h3 class="property-title">Approved Message</h3>
							<div class="html-editor" contenteditable="true" data-required="true" placeholder="Enter your approved message..." id="txtApprovalMsg"><%=ApprovalMessage%></div>
							<%--<div class="dynamic-key-panel">
								Add a dynamic key:
								<span data-key="MEMBER_NUMBER"><i class="fa fa-plus" aria-hidden="true"></i>Application Number</span>
								<span data-key="FIRST_NAME"><i class="fa fa-plus" aria-hidden="true"></i>Application First Name</span>
								<span data-key="FULL_NAME"><i class="fa fa-plus" aria-hidden="true"></i>Application Full Name</span>
							</div>--%>
						</div>
						<div>
							<h3 class="property-title">Referred Message</h3>
							<div class="html-editor" contenteditable="true" data-required="true" placeholder="Enter your referral message..." id="txtReferredMsg"><%=ReferredMessage%></div>
							<%--<div class="dynamic-key-panel">
								Add a dynamic key:
								<span data-key="MEMBER_NUMBER"><i class="fa fa-plus" aria-hidden="true"></i>Application Number</span>
								<span data-key="FIRST_NAME"><i class="fa fa-plus" aria-hidden="true"></i>Application First Name</span>
								<span data-key="FULL_NAME"><i class="fa fa-plus" aria-hidden="true"></i>Application Full Name</span>
							</div>--%>
						</div>
					</section>
					<section>
						<h2 class="section-title">Disclosures</h2>
						<div data-name="fundingDisclosure" data-section-type="disclosure">
							<div class="section-heading-btn clearfix">
								<div class="pull-left">
									<h3 class="section-title property-title">Funding Disclosure</h3>
									<p>This is the message that displays on the funding page and requires the applicant's consent via a checkbox. Any stand-alone link, a link without text before or after, will be displayed as a button in your Application Portal.</p>
								</div>
								<div id="divRestoreDefaultFundingDisclosureText" class="pull-right buttons mb10 <%=IIf(String.IsNullOrEmpty(FundingDislosure), " hidden", "")%>">
									<div class="link-btn">Restore default text</div>
								</div>
							</div>
							<%If Not String.IsNullOrEmpty(FundingDislosure) Then%>
							<div class="disclosure-block">
								<div id="fundingDisclosureEditor" data-required="true" class="html-editor" contenteditable="true" placeholder="Enter your disclosure..."><%=FundingDislosure%></div>
								<div class="disclosure-commands">
									<button type="button" class="btn btn-default confirmable-remove-btn"><i class="fa fa-trash-o" aria-hidden="true"></i>Remove</button>
								</div>		
							</div>
							<%End If%>
							<div class="no-item <%=IIf(String.IsNullOrEmpty(FundingDislosure), "hidden", "")%>">
								<div>
									<i class="fa fa-file-o" aria-hidden="true"></i>
									<p>These is currently no funding disclosure added.</p>
									<a href="#" class="add-funding-disclosure">Add Funding Disclosure</a>
								</div>
							</div>
						</div>
						<br/>
						<br/>
						<div data-name="additionalDisclosures" data-section-type="disclosure">
							<div class="section-heading-btn clearfix">
								<div class="pull-left">
									<h3 class="section-title property-title">Additional Disclosures</h3>
									<p>These are the messages that display in Submit & Sign section and require applicant's consent via checkboxes. Any stand-alone link, a link without text before or after will be displayed as a button in your Application Portal.</p>
								</div>
								<div class="pull-right buttons">
									<button type="button" class="btn btn-primary add-additional-disclosure mb10"><i class="fa fa-plus" aria-hidden="true"></i>Add Disclosure</button>
									<%--<button class="sm-btn" data-toggle="modal" data-target="#mdlAddDisclosure"><i class="fa fa-plus" aria-hidden="true"></i>Add Disclosure</button>--%>
								</div>
							</div>
			
							<%If AdditionalDisclosureList IsNot Nothing AndAlso AdditionalDisclosureList.Any() Then
									Dim idx As Integer = 0%>
							<%For each disclosure as string in AdditionalDisclosureList %>
							<div class="disclosure-block">
								<div id="disclosureEditor<%=idx%>" data-required="true" class="html-editor" contenteditable="true" placeholder="Enter your disclosure..."><%=disclosure%></div>
								<div class="disclosure-commands">
									<button type="button" class="btn btn-default remove-btn"><i class="fa fa-trash-o" aria-hidden="true"></i>Remove</button>
									<button type="button" class="btn btn-default moveup-btn"><i class="fa fa-chevron-circle-up" aria-hidden="true"></i>Move up</button>
									<button type="button" class="btn btn-default movedown-btn"><i class="fa fa-chevron-circle-down" aria-hidden="true"></i>Move down</button>
								</div>		
							</div>
							<% idx = idx + 1
								Next%>
							<%End If%>
							<div class="no-item <%=IIf(AdditionalDisclosureList Is Nothing OrElse Not AdditionalDisclosureList.Any(), "hidden", "")%>">
								<div>
									<i class="fa fa-file-o" aria-hidden="true"></i>
									<p>These are currently no disclosures added for this loan type.</p>
									<a href="#" class="add-additional-disclosure">Add Disclosure</a>
								</div>
							</div>
							<%--<div class="modal fade" id="mdlAddDisclosure" tabindex="-1" role="dialog">
								<div class="modal-dialog modal-lg" role="document">
									<div class="modal-content">
										<div class="modal-header">
											<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
											<h4 class="modal-title">Add/Edit Disclosure</h4>
										</div>
										<div class="modal-body">
											<textarea rows="10" class="sm-textarea"></textarea>
										</div>
										<div class="modal-footer">
											<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
											<button type="button" class="btn btn-primary">Save changes</button>
										</div>
									</div>
								</div>
							</div>--%>
							</div>
					</section>
				</div>
			</div>
		</section>
	</div>
</asp:Content>
<asp:Content runat="server" ContentPlaceHolderID="plScripts">
	<script type="text/javascript" src="js/ckeditor/ckeditor.js"></script>

	<script type="text/javascript">
		CKEDITOR.disableAutoInline = true;
		$(function () {
			//var modelAddDisclosure = $("#mdlAddDisclosure");
			//modelAddDisclosure.on('hidden.bs.modal', function () {
			//	//to sonething here
			//	console.log(new Date());
			//});
			//$(".dynamic-key-panel").dynamicKey();
			$("div[data-section-type='disclosure']").on("change", function () {
				var $self = $(this);
				$self.find(".disclosure-commands button").show();
				var $firstBlock = $self.find(".disclosure-block:first");
				$firstBlock.find("button.moveup-btn").hide();
				var $lastBlock = $self.find(".disclosure-block:last");
				$lastBlock.find("button.movedown-btn").hide();
				if ($self.find(".disclosure-block").length == 0) {
					$self.find(".no-item").removeClass("hidden");
				} else {
					$self.find(".no-item").addClass("hidden");
				}
			}).on("removeItem", function (e, editorId) {
				$.smValidate.removeValidationItem("ValidateData", editorId);
			}).trigger("change");
			$("div[data-name='fundingDisclosure']").on("change", function () {
				var $self = $(this);
				if ($self.find(".disclosure-block").length == 0) {
					$("#divRestoreDefaultFundingDisclosureText").addClass("hidden");
				} else {
					$("#divRestoreDefaultFundingDisclosureText").removeClass("hidden");
				}
			}).on("removeItem", function (e, editorId) {
				$.smValidate.removeValidationItem("ValidateData", editorId);
			});
			$("#divRestoreDefaultFundingDisclosureText").on("click", function () {
				CKEDITOR.instances["fundingDisclosureEditor"].setData("I agree to fund the account(s) with the method selected");
				$.smValidate.hideValidation("#fundingDisclosureEditor");
			});
			$(".html-editor").ckEditor({
				onContentChanged: function (editor) { master.FACTORY.documentChanged(editor); },
				onCommandClicked: function (editor, commandBtn) { master.FACTORY.documentChanged(editor); }
			});
			$(".add-funding-disclosure").click(function (e) {
				var $wrapper = $(this).closest("div[data-name='fundingDisclosure']");
				var $html = $("<div/>", { "class": "disclosure-block" }).append(
						$("<div/>", { "id": "fundingDisclosureEditor", "class": "html-editor", "data-required": true, "contenteditable": true, "placeholder": "Enter your funding disclosure..." }).html("I agree to fund the account(s) with the method selected"),
						$("<div/>", { "class": "disclosure-commands" }).append(
							$("<button/>", { "type": "button", "class": "btn btn-default confirmable-remove-btn", }).append(
								$("<i/>", { "class": "fa fa-trash-o", "aria-hidden": true }),
								document.createTextNode('Remove')
							)
						)
					);
				var $item = $html.insertBefore($(".no-item", $wrapper));
				$item.find(".html-editor").ckEditor({
					onContentChanged: function (editor) { master.FACTORY.documentChanged(editor); },
					onCommandClicked: function (editor, commandBtn) { master.FACTORY.documentChanged(editor); }
				});
				$wrapper.trigger("change");
				$('html, body').scrollTop($(this).closest("section").offset().top - 40);
				setTimeout(function () {
					$item.find(".html-editor").focus().trigger("focus");
				}, 200);
				e.preventDefault();
				registerDataValidator();
			});
			$(".add-additional-disclosure").click(function (e) {
				var $wrapper = $(this).closest("div[data-name='additionalDisclosures']");
				var $html = $("<div/>", { "class": "disclosure-block" }).append(
						$("<div/>", { "id": "disclosureEditor" + new Date().getTime(), "class": "html-editor", "data-required": true, "contenteditable": true, "placeholder": "Enter your additional disclosure..." }),
						$("<div/>", { "class": "disclosure-commands" }).append(
							$("<button/>", { "type": "button", "class": "btn btn-default remove-btn" }).append(
								$("<i/>", { "class": "fa fa-chevron-circle-up", "aria-hidden": true }),
								document.createTextNode('Remove')
							),
							$("<button/>", { "type": "button", "class": "btn btn-default moveup-btn" }).append(
								$("<i/>", { "class": "fa fa-trash-o", "aria-hidden": true }),
								document.createTextNode('Move up')
							),
							$("<button/>", { "type": "button", "class": "btn btn-default movedown-btn" }).append(
								$("<i/>", { "class": "fa fa-chevron-circle-down", "aria-hidden": true }),
								document.createTextNode('Move down')
							)
						)
					);
				var $item = $html.insertBefore($(".no-item", $wrapper));
				$item.find(".html-editor").ckEditor({
					onContentChanged: function (editor) { master.FACTORY.documentChanged(editor); },
					onCommandClicked: function (editor, commandBtn) { master.FACTORY.documentChanged(editor); }
				});
				$wrapper.trigger("change");
				$('html, body').scrollTop($(this).closest("section").offset().top - 40);
				setTimeout(function () {
					$item.find(".html-editor").focus().trigger("focus");
				}, 200);
				e.preventDefault();
				registerDataValidator();
			});
			$("#btnLoadDefaultEligibilityMessage").on("click", function () {
				CKEDITOR.instances["EligibilityMessage"].setData($("#divDefaultEligibilityMessage").html());
			});
			registerDataValidator();
		});
		(function (master, $, undefined) {
			master.FACTORY.SaveDraft = function (revisionId) {
				if ($.smValidate("ValidateData") == false) return;
				var dataObj = collectSubmitData();
				dataObj.command = 'savemanagespecialxa';
				dataObj.revisionId = revisionId;
				$.ajax({
					url: "/sm/smhandler.aspx",
					async: true,
					cache: false,
					type: 'POST',
					dataType: 'html',
					data: dataObj,
					success: function (responseText) {
						var response = $.parseJSON(responseText);
						if (response.IsSuccess) {
							master.FACTORY.onSaveDraftSuccess();
						} else {
							_COMMON.noty("error", "Error", 500);
						}
					}
				});
			};
			master.FACTORY.Preview = function (revisionId) {
				if ($.smValidate("ValidateData") == false) return;
				var dataObj = collectSubmitData();
				dataObj.command = 'previewmanagespecialxa';
				dataObj.revisionId = revisionId;
				$.ajax({
					url: "/sm/smhandler.aspx",
					async: true,
					cache: false,
					type: 'POST',
					dataType: 'html',
					data: dataObj,
					success: function (responseText) {
						var response = $.parseJSON(responseText);
						if (response.IsSuccess) {
							master.FACTORY.onPreviewSuccess();
							_COMMON.openInNewTab(response.Info.previewurl);
						} else {
							_COMMON.noty("error", "Error", 500);
						}
					}
				});
			};
			master.FACTORY.validateBeforePublish = function () {
				return $.smValidate("ValidateData");
			}
			master.FACTORY.Publish = function (comment, publishAll) {
				if ($.smValidate("ValidateData") == false) return;
				var dataObj = collectSubmitData();
				dataObj.command = 'publishmanagespecialxa';
				dataObj.comment = comment;
				dataObj.publishAll = publishAll;
				$.ajax({
					url: "/sm/smhandler.aspx",
					async: true,
					cache: false,
					type: 'POST',
					dataType: 'html',
					data: dataObj,
					success: function (responseText) {
						var response = $.parseJSON(responseText);
						if (response.IsSuccess) {
							master.FACTORY.onPublishSuccess();
						} else {
							_COMMON.noty("error", "Error", 500);
						}
					}
				});
			};
		}(window.master = window.master || {}, jQuery));

		function registerDataValidator() {
			$.smValidate.removeValidationGroup("ValidateData");
			$("[data-required='true']").each(function (idx, ele) {
				$(ele).observer({
					validators: [
						function (partial) {
							var $self = $(this);
							var content = "";
							if ($self.hasClass("html-editor")) {
								content = CKEDITOR.instances[$self.attr("id")].getData();
								if (content == $self.attr("placeholder")) content = "";
							} else if ($self.hasClass("sm-textarea")) {
								content = $self.val();
							}

							if ($.trim(content) === "") return "This field is required";
							return "";
						}
					],
					validateOnBlur: true,
					group: "ValidateData"
				});
			});

		}
		function collectSubmitData() {
			var result = {};
			result.approvalMsg = _COMMON.getEditorValue("txtApprovalMsg");
			result.referredMsg = _COMMON.getEditorValue("txtReferredMsg");
			result.eligibilityMsg = _COMMON.getEditorValue("EligibilityMessage");
			result.additionalDisclosures = JSON.stringify(_COMMON.collectDisclosureData($("div[data-name='additionalDisclosures']")));
			if (CKEDITOR.instances["fundingDisclosureEditor"]) {
				result.fundingDisclosure = CKEDITOR.instances["fundingDisclosureEditor"].getData();
			}
			result.lenderconfigid = '<%=LenderConfigID%>';
			return result;
		}
	</script>
</asp:Content>
<asp:Content runat="server" ContentPlaceHolderID="plActionButtons"></asp:Content>