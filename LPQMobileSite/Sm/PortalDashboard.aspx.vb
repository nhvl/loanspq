﻿
Imports LPQMobile.Utils

Partial Class Sm_PortalDashboard
	Inherits SmBasePage

	Protected LenderConfigID As Guid
	Protected LenderID As Guid
	Protected Portal As SmLenderConfigBasicInfo
	Protected BackOfficeLender As SmBackOfficeLender
	Protected LenderAdminAllowed As Boolean = False
	Protected PortalAdminAllowed As Boolean = False
	Protected CanManageUsers As Boolean = True
	Protected Sub Page_PreLoad(sender As Object, e As EventArgs) Handles Me.PreLoad
		Dim smMasterPage = CType(Me.Master, Sm_MasterPage)
		smMasterPage.SelectedMainMenuItem = "lender"
		LenderConfigID = Common.SafeGUID(Request.Params("lenderConfigId"))
		If LenderConfigID = Guid.Empty Then
			Response.Redirect("/Sm/ManageLenders.aspx", True)
		End If
		Dim smBL As New SmBL()
		Portal = smBL.GetPortalBasicInfoByLenderConfigID(LenderConfigID)
		If Portal Is Nothing Then
			Response.Redirect("/Sm/ManageLenders.aspx", True)
		End If
		BackOfficeLender = smBL.GetBackOfficeLenderByLenderConfigID(Portal.LenderConfigID)
		If BackOfficeLender Is Nothing Then
			Response.Redirect("/Sm/ManageLenders.aspx", True)
		End If
		LenderID = Portal.LenderID
		smMasterPage.LenderInfo = "(<span id='spLenderName'>" & BackOfficeLender.LenderName & "</span> - <span id='spLenderRef'>" & Portal.LenderRef & "</span>)"

		AllowedRoles.Add(SmSettings.Role.SuperUser.ToString())
		AllowedRoles.Add(String.Format("PortalScope${0}${1}${2}", SmSettings.Role.PortalAdmin.ToString(), LenderID.ToString(), LenderConfigID.ToString()))
		AllowedRoles.Add(String.Format("PortalScope${0}${1}${2}", SmSettings.Role.VendorGroupAdmin.ToString(), LenderID.ToString(), LenderConfigID.ToString()))
		AllowedRoles.Add(String.Format("LenderScope${0}${1}", SmSettings.Role.LenderAdmin.ToString(), LenderID.ToString()))
		NotAllowedAccessRedirectUrl = "/Sm/ManageLenders.aspx"
		LenderAdminAllowed = SmUtil.CheckRoleAccess(HttpContext.Current, SmSettings.Role.SuperUser.ToString(), String.Format("LenderScope${0}${1}", SmSettings.Role.LenderAdmin.ToString(), LenderID.ToString()))
		PortalAdminAllowed = SmUtil.CheckRoleAccess(HttpContext.Current, SmSettings.Role.SuperUser.ToString(), String.Format("PortalScope${0}${1}${2}", SmSettings.Role.PortalAdmin.ToString(), LenderID.ToString(), LenderConfigID.ToString()))
		If UserInfo.SSOAccessRights IsNot Nothing Then
			CanManageUsers = UserInfo.SSOAccessRights.CanManageUsers
		End If
	End Sub
End Class
