﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="ManageHE.aspx.vb" Inherits="Sm_ManageHE" MasterPageFile="SiteManager.master" %>

<asp:Content runat="server" ContentPlaceHolderID="plBody">
	<div>
		<h1 class="page-header">Manage Home Equity Loan Settings</h1>
		<p>Use this page to manage what’s available for your Application Portal including Purpose Types & Decision Messages, and Disclosures</p>
		<section data-name="HomeEquityLoanPurposes">
			<h2 class="section-title">Purpose Types</h2>
			<p>Select which purposes you want to make available. Values for these purposes are configured in your In-Branch Configure Site settings.</p>
			<%If PurposeList IsNot Nothing AndAlso PurposeList.Any() Then%>
			<%For Each item As KeyValuePair(Of String, SmTextValuePair) In PurposeList.OrderBy(Function(p) p.Value.Value)%>
			<div class="checkbox-btn-wrapper">
				<span><%=item.Value.text%></span>
				<label class="toggle-btn pull-right" data-on="ON" data-off="OFF">
                    <input type="checkbox" data-text="<%=item.Value.text%>" value="<%=item.Value.value%>" <%=BindCheckbox(item.Value.value, EnabledPurposeList)%>/>
                    <span class="button-checkbox"></span>
                </label>
			</div>
			<%Next%>
			<%End If%>
		</section>
		<section id="secHomeEquityReasons" data-name="HomeEquityReasons">
			<h2 class="section-title">Home Equity Reasons</h2>
			<p>Select which reasons you want to make available. At least 1 reason is required.</p>
			
			
			<div class="checkbox-btn-wrapper">
				<span>CASH</span>
				<label class="toggle-btn pull-right" data-on="ON" data-off="OFF">
                    <input type="checkbox" value="CASH" <%=BindCheckbox("CASH", ReasonList)%>/>
                    <span class="button-checkbox"></span>
                </label>
			</div>
			<div class="checkbox-btn-wrapper">
				<span>DEBT CONSOLIDATION</span>
				<label class="toggle-btn pull-right" data-on="ON" data-off="OFF">
                    <input type="checkbox" value="DEBT CONSOLIDATION" <%=BindCheckbox("DEBT CONSOLIDATION", ReasonList)%>/>
                    <span class="button-checkbox"></span>
                </label>
			</div>
			<div class="checkbox-btn-wrapper">
				<span>HOME IMPROVEMENT</span>
				<label class="toggle-btn pull-right" data-on="ON" data-off="OFF">
                    <input type="checkbox" value="HOME IMPROVEMENT" <%=BindCheckbox("HOME IMPROVEMENT", ReasonList)%>/>
                    <span class="button-checkbox"></span>
                </label>
			</div>
			<div class="checkbox-btn-wrapper">
				<span>PURCHASE</span>
				<label class="toggle-btn pull-right" data-on="ON" data-off="OFF">
                    <input type="checkbox" value="MANUFACTURED HOME PURCHASE" <%=BindCheckbox("MANUFACTURED HOME PURCHASE", ReasonList)%>/>
                    <span class="button-checkbox"></span>
                </label>
			</div>
			<div class="checkbox-btn-wrapper">
				<span>OTHER</span>
				<label class="toggle-btn pull-right" data-on="ON" data-off="OFF">
                    <input type="checkbox" value="OTHER" <%=BindCheckbox("OTHER", ReasonList)%>/>
                    <span class="button-checkbox"></span>
                </label>
			</div>
			<div class="checkbox-btn-wrapper">
				<span>PERSONAL</span>
				<label class="toggle-btn pull-right" data-on="ON" data-off="OFF">
                    <input type="checkbox" value="PERSONAL" <%=BindCheckbox("PERSONAL", ReasonList)%>/>
                    <span class="button-checkbox"></span>
                </label>
			</div>
			<div class="checkbox-btn-wrapper">
				<span>REFINANCE HOME LOAN</span>
				<label class="toggle-btn pull-right" data-on="ON" data-off="OFF">
                    <input type="checkbox" value="REFINANCE" <%=BindCheckbox("REFINANCE", ReasonList)%>/>
                    <span class="button-checkbox"></span>
                </label>
			</div>
			<div class="checkbox-btn-wrapper">
				<span>REFINANCE CASHOUT</span>
				<label class="toggle-btn pull-right" data-on="ON" data-off="OFF">
                    <input type="checkbox" value="REFINANCE CASHOUT" <%=BindCheckbox("REFINANCE CASHOUT", ReasonList)%>/>
                    <span class="button-checkbox"></span>
                </label>
			</div>
		</section>
		<section id="secHomeEquityProperties" data-name="HomeEquityProperties">
			<h2 class="section-title">Home Equity Properties</h2>
			<p>Select which properties you want to make available.  At least 1 property type is required.</p>
			<div class="checkbox-btn-wrapper">
				<span>SINGLE FAMILY RESIDENCE</span>
				<label class="toggle-btn pull-right" data-on="ON" data-off="OFF">
                    <input type="checkbox" value="SFR" <%=BindCheckbox("SFR", PropertyList)%>/>
                    <span class="button-checkbox"></span>
                </label>
			</div>
			<div class="checkbox-btn-wrapper">
				<span>2 UNIT</span>
				<label class="toggle-btn pull-right" data-on="ON" data-off="OFF">
                    <input type="checkbox" value="2 UNIT" <%=BindCheckbox("2 UNIT", PropertyList)%>/>
                    <span class="button-checkbox"></span>
                </label>
			</div>
			<div class="checkbox-btn-wrapper">
				<span>3 UNIT</span>
				<label class="toggle-btn pull-right" data-on="ON" data-off="OFF">
                    <input type="checkbox" value="3 UNIT" <%=BindCheckbox("3 UNIT", PropertyList)%>/>
                    <span class="button-checkbox"></span>
                </label>
			</div>
			<div class="checkbox-btn-wrapper">
				<span>4 UNIT</span>
				<label class="toggle-btn pull-right" data-on="ON" data-off="OFF">
                    <input type="checkbox" value="4 UNIT" <%=BindCheckbox("4 UNIT", PropertyList)%>/>
                    <span class="button-checkbox"></span>
                </label>
			</div>
			<div class="checkbox-btn-wrapper">
				<span>5+ UNIT</span>
				<label class="toggle-btn pull-right" data-on="ON" data-off="OFF">
                    <input type="checkbox" value="5+ UNIT" <%=BindCheckbox("5+ UNIT", PropertyList)%>/>
                    <span class="button-checkbox"></span>
                </label>
			</div>
			<div class="checkbox-btn-wrapper">
				<span>TOWNHOUSE</span>
				<label class="toggle-btn pull-right" data-on="ON" data-off="OFF">
                    <input type="checkbox" value="TOWNHOUSE" <%=BindCheckbox("TOWNHOUSE", PropertyList)%>/>
                    <span class="button-checkbox"></span>
                </label>
			</div>
			<div class="checkbox-btn-wrapper">
				<span>HIGH RISE CONDO</span>
				<label class="toggle-btn pull-right" data-on="ON" data-off="OFF">
                    <input type="checkbox" value="HIGH RISE CONDO" <%=BindCheckbox("HIGH RISE CONDO", PropertyList)%>/>
                    <span class="button-checkbox"></span>
                </label>
			</div>
			<div class="checkbox-btn-wrapper">
				<span>LOW RISE CONDO</span>
				<label class="toggle-btn pull-right" data-on="ON" data-off="OFF">
                    <input type="checkbox" value="LOW RISE CONDO" <%=BindCheckbox("LOW RISE CONDO", PropertyList)%>/>
                    <span class="button-checkbox"></span>
                </label>
			</div>
			<div class="checkbox-btn-wrapper">
				<span>PUD</span>
				<label class="toggle-btn pull-right" data-on="ON" data-off="OFF">
                    <input type="checkbox" value="PUD" <%=BindCheckbox("PUD", PropertyList)%>/>
                    <span class="button-checkbox"></span>
                </label>
			</div>
			<div class="checkbox-btn-wrapper">
				<span>MOBILE HOME</span>
				<label class="toggle-btn pull-right" data-on="ON" data-off="OFF">
                    <input type="checkbox" value="MOBILE HOME" <%=BindCheckbox("MOBILE HOME", PropertyList)%>/>
                    <span class="button-checkbox"></span>
                </label>
			</div>
			<div class="checkbox-btn-wrapper">
				<span>MANUFACTURED HOME</span>
				<label class="toggle-btn pull-right" data-on="ON" data-off="OFF">
                    <input type="checkbox" value="MANUFACTURED HOME" <%=BindCheckbox("MANUFACTURED HOME", PropertyList)%>/>
                    <span class="button-checkbox"></span>
                </label>
			</div>
			<div class="checkbox-btn-wrapper">
				<span>MIXED USE</span>
				<label class="toggle-btn pull-right" data-on="ON" data-off="OFF">
                    <input type="checkbox" value=">MIXED USE" <%=BindCheckbox(">MIXED USE", PropertyList)%>/>
                    <span class="button-checkbox"></span>
                </label>
			</div>
			<div class="checkbox-btn-wrapper">
				<span>CONDOTEL</span>
				<label class="toggle-btn pull-right" data-on="ON" data-off="OFF">
                    <input type="checkbox" value="CONDOTEL" <%=BindCheckbox("CONDOTEL", PropertyList)%>/>
                    <span class="button-checkbox"></span>
                </label>
			</div>
			<div class="checkbox-btn-wrapper">
				<span>COOP</span>
				<label class="toggle-btn pull-right" data-on="ON" data-off="OFF">
                    <input type="checkbox" value="COOP" <%=BindCheckbox("COOP", PropertyList)%>/>
                    <span class="button-checkbox"></span>
                </label>
			</div>
			<div class="checkbox-btn-wrapper">
				<span>COMMERCIAL</span>
				<label class="toggle-btn pull-right" data-on="ON" data-off="OFF">
                    <input type="checkbox" value="COMMERCIAL" <%=BindCheckbox("COMMERCIAL", PropertyList)%>/>
                    <span class="button-checkbox"></span>
                </label>
			</div>
			<div class="checkbox-btn-wrapper">
				<span>FARM</span>
				<label class="toggle-btn pull-right" data-on="ON" data-off="OFF">
                    <input type="checkbox" value="FARM" <%=BindCheckbox("FARM", PropertyList)%>/>
                    <span class="button-checkbox"></span>
                </label>
			</div>
			<div class="checkbox-btn-wrapper">
				<span>HOME & BUSINNES</span>
				<label class="toggle-btn pull-right" data-on="ON" data-off="OFF">
                    <input type="checkbox" value="HOME BUSINNES" <%=BindCheckbox("HOME BUSINNES", PropertyList)%>/>
                    <span class="button-checkbox"></span>
                </label>
			</div>
			<div class="checkbox-btn-wrapper">
				<span>LAND</span>
				<label class="toggle-btn pull-right" data-on="ON" data-off="OFF">
                    <input type="checkbox" value="LAND" <%=BindCheckbox("LAND", PropertyList)%>/>
                    <span class="button-checkbox"></span>
                </label>
			</div>
			<div class="checkbox-btn-wrapper">
				<span>OTHER</span>
				<label class="toggle-btn pull-right" data-on="ON" data-off="OFF">
                    <input type="checkbox" value="OTHER" <%=BindCheckbox("OTHER", PropertyList)%>/>
                    <span class="button-checkbox"></span>
                </label>
			</div>
			<div class="checkbox-btn-wrapper">
				<span>MANUFACTURED HOME - NO LAND</span>
				<label class="toggle-btn pull-right" data-on="ON" data-off="OFF">
                    <input type="checkbox" value="MNL" <%=BindCheckbox("MNL", PropertyList)%>/>
                    <span class="button-checkbox"></span>
                </label>
			</div>
		</section>
		<section>
			<h2 class="section-title">Messages</h2>
			<p>These are the messages that display depending on the applicant’s status.</p>
			<div>
				<h3 class="property-title">Approved Message</h3>
				<div class="html-editor" contenteditable="true" data-required="true" placeholder="Enter your approved message..." id="txtApprovalMsg"><%=ApprovalMessage%></div>
				<%--<div class="dynamic-key-panel">
					Add a dynamic key:
					<span data-key="MEMBER_NUMBER"><i class="fa fa-plus" aria-hidden="true"></i>Application Number</span>
					<span data-key="FIRST_NAME"><i class="fa fa-plus" aria-hidden="true"></i>Application First Name</span>
					<span data-key="FULL_NAME"><i class="fa fa-plus" aria-hidden="true"></i>Application Full Name</span>
				</div>--%>
			</div>
			<div>
				<h3 class="property-title">Referred Message</h3>
				<div class="html-editor" contenteditable="true" data-required="true" placeholder="Enter your referral message..." id="txtReferredMsg"><%=ReferredMessage%></div>
				<%--<div class="dynamic-key-panel">
					Add a dynamic key:
					<span data-key="MEMBER_NUMBER"><i class="fa fa-plus" aria-hidden="true"></i>Application Number</span>
					<span data-key="FIRST_NAME"><i class="fa fa-plus" aria-hidden="true"></i>Application First Name</span>
					<span data-key="FULL_NAME"><i class="fa fa-plus" aria-hidden="true"></i>Application Full Name</span>
				</div>--%>
			</div>
			<div>
				<h3 class="property-title">Declined Message</h3>
				<div class="html-editor" contenteditable="true" data-required="false"  id="txtDeclinedMsg"><%=DeclinedMessage%></div>
				<%--<div class="dynamic-key-panel">
					Add a dynamic key:
					<span data-key="MEMBER_NUMBER"><i class="fa fa-plus" aria-hidden="true"></i>Application Number</span>
					<span data-key="FIRST_NAME"><i class="fa fa-plus" aria-hidden="true"></i>Application First Name</span>
					<span data-key="FULL_NAME"><i class="fa fa-plus" aria-hidden="true"></i>Application Full Name</span>
				</div>--%>
			</div>
            <div>
				<h3 class="property-title">Pre-qualified Message</h3>
				<div class="html-editor" contenteditable="true" data-required="false"  id="txtPreQualifiedMsg"><%=PreQualifiedMessage%></div>			
			</div>
		</section>
		<section id="demographics">
			<h2>Demographic Information</h2>
			<p>
				These are the messages that are related to the collection of HMDA demographic information for race and ethnicity. The default text is compliant with federal regulations.
				You may customize the text, but you are responsible for ensuring that it remains within compliance.
			</p>

			<div>
				<div class="flex-row flex-align-baseline">
					<h3 class="property-title">Demographic Description</h3>
					<div class="flex-spacer"></div>
					<a href="javascript:void(0);" id="aRestoreDemographicDescription">Restore default text</a>
				</div>
				<div class="html-editor" contenteditable="true" data-required="true" id="txtDemographicDescription"><%=DemographicDescription%></div>
			</div>

			<div>
				<div class="flex-row flex-align-baseline">
					<h3 class="property-title">Demographic Disclosure</h3>
					<div class="flex-spacer"></div>
					<a href="javascript:void(0);" id="aRestoreDemographicDisclosure">Restore default text</a>
				</div>
				<div class="html-editor" contenteditable="true" data-required="true" id="txtDemographicDisclosure"><%=DemographicDisclosure%></div>
			</div>
		</section>
		<br />
		<section data-name="disclosures" data-section-type="disclosure">
			<div class="section-heading-btn clearfix">
				<div class="pull-left">
					<h2 class="section-title">Disclosures</h2>
					<p>These are the messages that display in Submit & Sign section and require applicant's consent via check boxes. Any stand-alone link, link without text before or after will be displayed as a button in your Application Portal.</p>
				</div>
				<div class="pull-right buttons">
					<button type="button" class="btn btn-primary add-disclosure mb10"><i class="fa fa-plus" aria-hidden="true"></i>Add Disclosure</button>
					<%--<button class="sm-btn" data-toggle="modal" data-target="#mdlAddDisclosure"><i class="fa fa-plus" aria-hidden="true"></i>Add Disclosure</button>--%>
				</div>
			</div>			
			<%If DisclosureList IsNot Nothing AndAlso DisclosureList.Any() Then
					Dim idx As Integer = 0%>
			<%For each disclosure as string in DisclosureList %>
			<div class="disclosure-block">
				<div id="disclosureEditor<%=idx%>" data-required="true" class="html-editor" contenteditable="true" placeholder="Enter your disclosure..."><%=disclosure%></div>
				<div class="disclosure-commands">
					<button type="button" class="btn btn-default remove-btn"><i class="fa fa-trash-o" aria-hidden="true"></i>Remove</button>
					<button type="button" class="btn btn-default moveup-btn"><i class="fa fa-chevron-circle-up" aria-hidden="true"></i>Move up</button>
					<button type="button" class="btn btn-default movedown-btn"><i class="fa fa-chevron-circle-down" aria-hidden="true"></i>Move down</button>
				</div>		
			</div>
			<% idx = idx + 1
				Next%>
			<%End If%>
			<div class="no-item <%=IIf(DisclosureList Is Nothing OrElse Not DisclosureList.Any(), "hidden", "")%>">
				<div>
					<i class="fa fa-file-o" aria-hidden="true"></i>
					<p>These are currently no disclosures added for this loan type.</p>
					<a href="#" class="add-disclosure">Add Disclosure</a>
				</div>
			</div>
			<%--<div class="modal fade" id="mdlAddDisclosure" tabindex="-1" role="dialog">
				<div class="modal-dialog modal-lg" role="document">
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
							<h4 class="modal-title">Add/Edit Disclosure</h4>
						</div>
						<div class="modal-body">
							<textarea rows="10" class="sm-textarea"></textarea>
						</div>
						<div class="modal-footer">
							<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
							<button type="button" class="btn btn-primary">Save changes</button>
						</div>
					</div>
				</div>
			</div>--%>
		</section>
		<br />
		<section data-name="disclosures_loc" data-section-type="disclosure">
			<div class="section-heading-btn clearfix">
				<div class="pull-left">
					<h2 class="section-title">Disclosures LOC</h2>
					<p>These are the messages that display in Submit & Sign section and require applicant's consent via check boxes. Any stand-alone link, link without text before or after will be displayed as a button in your Application Portal.</p>
				</div>
				<div class="pull-right buttons">
					<button type="button" class="btn btn-primary add-disclosure-loc mb10"><i class="fa fa-plus" aria-hidden="true"></i>Add Disclosure</button>
					<%--<button class="sm-btn" data-toggle="modal" data-target="#mdlAddDisclosure"><i class="fa fa-plus" aria-hidden="true"></i>Add Disclosure</button>--%>
				</div>
			</div>			
			<%If DisclosureLocList IsNot Nothing AndAlso DisclosureLocList.Any() Then
					Dim idx As Integer = 0%>
			<%For Each disclosure As String In DisclosureLocList%>
			<div class="disclosure-block">
				<div id="disclosureLocEditor<%=idx%>" data-required="true" class="html-editor" contenteditable="true" placeholder="Enter your disclosure..."><%=disclosure%></div>
				<div class="disclosure-commands">
					<button type="button" class="btn btn-default remove-btn"><i class="fa fa-trash-o" aria-hidden="true"></i>Remove</button>
					<button type="button" class="btn btn-default moveup-btn"><i class="fa fa-chevron-circle-up" aria-hidden="true"></i>Move up</button>
					<button type="button" class="btn btn-default movedown-btn"><i class="fa fa-chevron-circle-down" aria-hidden="true"></i>Move down</button>
				</div>		
			</div>
			<% idx = idx + 1
				Next%>
			<%End If%>
			<div class="no-item <%=IIf(DisclosureLocList Is Nothing OrElse Not DisclosureLocList.Any(), "hidden", "")%>">
				<div>
					<i class="fa fa-file-o" aria-hidden="true"></i>
					<p>These are currently no disclosures added for this loan type.</p>
					<a href="#" class="add-disclosure-loc">Add Disclosure</a>
				</div>
			</div>
		</section>
	</div>
</asp:Content>
<asp:Content runat="server" ContentPlaceHolderID="plScripts">
	<script type="text/javascript" src="js/ckeditor/ckeditor.js"></script>
	<script type="text/javascript">
		CKEDITOR.disableAutoInline = true;
		$(function () {
			//var modelAddDisclosure = $("#mdlAddDisclosure");
			//modelAddDisclosure.on('hidden.bs.modal', function () {
			//	//to sonething here
			//	console.log(new Date());
			//});
			//$(".dynamic-key-panel").dynamicKey();
			$("section[data-name=disclosures]").on("change", function () {
				$(this).find(".disclosure-commands button").show();
				var $firstBlock = $(this).find(".disclosure-block:first");
				$firstBlock.find("button.moveup-btn").hide();
				var $lastBlock = $(this).find(".disclosure-block:last");
				$lastBlock.find("button.movedown-btn").hide();
				if ($(this).find(".disclosure-block").length == 0) {
					$(this).find(".no-item").removeClass("hidden");
				} else {
					$(this).find(".no-item").addClass("hidden");
				}
			}).trigger("change");

			$("section[data-name=disclosures_loc]").on("change", function () {
				$(this).find(".disclosure-commands button").show();
				var $firstBlock = $(this).find(".disclosure-block:first");
				$firstBlock.find("button.moveup-btn").hide();
				var $lastBlock = $(this).find(".disclosure-block:last");
				$lastBlock.find("button.movedown-btn").hide();
				if ($(this).find(".disclosure-block").length == 0) {
					$(this).find(".no-item").removeClass("hidden");
				} else {
					$(this).find(".no-item").addClass("hidden");
				}
			}).trigger("change");
			$(".html-editor").ckEditor({
				onContentChanged: function (editor) { master.FACTORY.documentChanged(editor); },
				onCommandClicked: function (editor, commandBtn) { master.FACTORY.documentChanged(editor); },
				placeholder: "Type here..."
			});

			$(".add-disclosure").click(function (e) {
				var $section = $(this).closest("section");
				var htmlStr = '<div class="disclosure-block"><div id="disclosureEditor' + new Date().getTime() + '" class="html-editor" data-required="true" contenteditable="true" placeholder="Enter your disclosure..."></div><div class="disclosure-commands"><button type="button" class="btn btn-default remove-btn"><i class="fa fa-trash-o" aria-hidden="true"></i>Remove</button><button type="button" class="btn btn-default moveup-btn"><i class="fa fa-chevron-circle-up" aria-hidden="true"></i>Move up</button><button type="button" class="btn btn-default movedown-btn"><i class="fa fa-chevron-circle-down" aria-hidden="true"></i>Move down</button></div></div>';
				var $item = $(htmlStr).insertBefore($(".no-item", $section));
				$item.find(".html-editor").ckEditor({
					onContentChanged: function (editor) { master.FACTORY.documentChanged(editor); },
					onCommandClicked: function (editor, commandBtn) { master.FACTORY.documentChanged(editor); }
				});
				$(this).closest("section").trigger("change");
				$('html, body').scrollTop($(this).closest("section").offset().top - 40);
				setTimeout(function () {
					$item.find(".html-editor").focus().trigger("focus");
				}, 200);
				e.preventDefault();
				registerDataValidator();
			});
			$(".add-disclosure-loc").click(function (e) {
				var $section = $(this).closest("section");
				var htmlStr = '<div class="disclosure-block"><div id="disclosureEditor' + new Date().getTime() + '" class="html-editor" data-required="true" contenteditable="true" placeholder="Enter your disclosure..."></div><div class="disclosure-commands"><button type="button" class="btn btn-default remove-btn"><i class="fa fa-trash-o" aria-hidden="true"></i>Remove</button><button type="button" class="btn btn-default moveup-btn"><i class="fa fa-chevron-circle-up" aria-hidden="true"></i>Move up</button><button type="button" class="btn btn-default movedown-btn"><i class="fa fa-chevron-circle-down" aria-hidden="true"></i>Move down</button></div></div>';
				var $item = $(htmlStr).insertBefore($(".no-item", $section));
				$('html, body').scrollTop($item.find(".html-editor").focus().offset().top - 40);
				$item.find(".html-editor").ckEditor({
					onContentChanged: function (editor) { master.FACTORY.documentChanged(editor); },
					onCommandClicked: function (editor, commandBtn) { master.FACTORY.documentChanged(editor); }
				});
				$(this).closest("section").trigger("change");
				$('html, body').scrollTop($(this).closest("section").offset().top - 40);
				setTimeout(function () {
					$item.find(".html-editor").focus().trigger("focus");
				}, 200);
				e.preventDefault();
				registerDataValidator();
			});
			$("select,input:checkbox", "div.checkbox-btn-wrapper").on("change", function () {
				master.FACTORY.documentChanged(this);
			});
			$("#aRestoreDemographicDescription").on("click", function () {
				CKEDITOR.instances["txtDemographicDescription"].setData("<%=HttpUtility.JavaScriptStringEncode(DefaultDemographicDescription)%>");
				$("#txtDemographicDescription").focus();
			});
			$("#aRestoreDemographicDisclosure").on("click", function () {
				CKEDITOR.instances["txtDemographicDisclosure"].setData("<%=HttpUtility.JavaScriptStringEncode(DefaultDemographicDisclosure)%>");
				$("#txtDemographicDisclosure").focus();
			});
			registerDataValidator();
		});
		(function (master, $, undefined) {
			master.FACTORY.SaveDraft = function (revisionId) {
				if ($.smValidate("ValidateData") == false) return;
				var dataObj = collectSubmitData();
				dataObj.command = "savemanagehe";
				dataObj.revisionId = revisionId;
				$.ajax({
					url: "/sm/smhandler.aspx",
					async: true,
					cache: false,
					type: 'POST',
					dataType: 'html',
					data: dataObj,
					success: function (responseText) {
						var response = $.parseJSON(responseText);
						if (response.IsSuccess) {
							master.FACTORY.onSaveDraftSuccess();
						} else {
							_COMMON.noty("error", "Error", 500);
						}
					}
				});
			};
			master.FACTORY.Preview = function (revisionId) {
				if ($.smValidate("ValidateData") == false) return;
				var dataObj = collectSubmitData();
				dataObj.command = "previewmanagehe";
				dataObj.revisionId = revisionId;
				$.ajax({
					url: "/sm/smhandler.aspx",
					async: true,
					cache: false,
					type: 'POST',
					dataType: 'html',
					data: dataObj,
					success: function (responseText) {
						var response = $.parseJSON(responseText);
						if (response.IsSuccess) {
							master.FACTORY.onPreviewSuccess();
							_COMMON.openInNewTab(response.Info.previewurl);
						} else {
							_COMMON.noty("error", "Error", 500);
						}
					}
				});
			};
			master.FACTORY.validateBeforePublish = function() {
				return $.smValidate("ValidateData");
			}
			master.FACTORY.Publish = function (comment, publishAll) {
				if ($.smValidate("ValidateData") == false) return;
				var dataObj = collectSubmitData();
				dataObj.command = "publishmanagehe";
				dataObj.comment = comment;
				dataObj.publishAll = publishAll;
				$.ajax({
					url: "/sm/smhandler.aspx",
					async: true,
					cache: false,
					type: 'POST',
					dataType: 'html',
					data: dataObj,
					success: function (responseText) {
						var response = $.parseJSON(responseText);
						if (response.IsSuccess) {
							master.FACTORY.onPublishSuccess();
						} else {
							_COMMON.noty("error", "Error", 500);
						}
					}
				});
			};
		}(window.master = window.master || {}, jQuery));
		function registerDataValidator() {
			$.smValidate.removeValidationGroup("ValidateData");
			$("[data-required='true']").each(function (idx, ele) {
				$(ele).observer({
					validators: [
						function (partial) {
							var $self = $(this);
							var content = "";
							if ($self.hasClass("html-editor")) {
								content = CKEDITOR.instances[$self.attr("id")].getData();
								if (content == $self.attr("placeholder")) content = "";
							} else if ($self.hasClass("sm-textarea")) {
								content = $self.val();
							}
							if ($.trim(content) === "") return "This field is required";
							return "";
						}
					],
					validateOnBlur: true,
					group: "ValidateData"
				});
			});
			$("#secHomeEquityReasons").observer({
				validators: [
					function (partial) {
						if (_COMMON.collectCheckboxGroupData($("#secHomeEquityReasons")).length == 0) {
							return "Please enable at least 1 reason";
						}
						return "";
					}
				],
				validateOnBlur: true,
				group: "ValidateData",
				bindingEvent: "change",
				groupType: "checkboxGroup"
			});
			$("#secHomeEquityProperties").observer({
				validators: [
					function (partial) {
						if (_COMMON.collectCheckboxGroupData($("#secHomeEquityProperties")).length == 0) {
							return "Please enable at least 1 property type";
						}
						return "";
					}
				],
				validateOnBlur: true,
				group: "ValidateData",
				bindingEvent: "change",
				groupType: "checkboxGroup"
			});
			$("#txtDemographicDescription").observer({
				validators: [
					function () {
						var txtDemoDesc = $("#txtDemographicDescription").text().trim();
						if (txtDemoDesc == "") {
							return "This field is required";
						}
						if (txtDemoDesc.length > 400) {
							return "This field is too long. " + txtDemoDesc.length + " out of 400 characters";
						}
					}
				],
				validateOnBlur: true,
				group: "ValidateData",
				bindingEvent: "change",
			});
			$("#txtDemographicDisclosure").observer({
				validators: [
					function () {
						var txtDemoDisc = $("#txtDemographicDisclosure").text().trim();
						if (txtDemoDisc == "") {
							return "This field is required";
						}
						if (txtDemoDisc.length > 5000) {
							return "This field is too long. " + txtDemoDisc.length + " out of 5,000 characters";
						}
					}
				],
				validateOnBlur: true,
				group: "ValidateData",
				bindingEvent: "change",
			});
		}
		function collectSubmitData() {
			var result = {};
			result.approvalMsg = _COMMON.getEditorValue("txtApprovalMsg");
			result.referredMsg = _COMMON.getEditorValue("txtReferredMsg");
            result.declinedMsg = _COMMON.getEditorValue("txtDeclinedMsg");
			result.preQualifiedMsg = _COMMON.getEditorValue("txtPreQualifiedMsg"); 
			result.demographicDescription = _COMMON.getEditorValue("txtDemographicDescription");
			result.demographicDisclosure = _COMMON.getEditorValue("txtDemographicDisclosure");
			result.disclosures = JSON.stringify(_COMMON.collectDisclosureData($("section[data-name=disclosures]")));
			result.disclosuresLoc = JSON.stringify(_COMMON.collectDisclosureData($("section[data-name=disclosures_loc]")));
			result.lenderconfigid = '<%=LenderConfigID%>';
			result.loanReasons = JSON.stringify(_COMMON.collectCheckboxGroupData($("section[data-name='HomeEquityReasons']")));
			result.loanPurposes = JSON.stringify(_COMMON.collectCheckboxGroupData($("section[data-name='HomeEquityLoanPurposes']")));
			result.loanProperties = JSON.stringify(_COMMON.collectCheckboxGroupData($("section[data-name='HomeEquityProperties']")));
			return result;
		}
	</script>
</asp:Content>
<asp:Content runat="server" ContentPlaceHolderID="plActionButtons"></asp:Content>