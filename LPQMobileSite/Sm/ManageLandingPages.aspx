﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="ManageLandingPages.aspx.vb" Inherits="Sm_ManageLandingPages"  MasterPageFile="SiteManager.master" %>

<asp:Content runat="server" ContentPlaceHolderID="plBody">
	<div>
		<h1>Homepages</h1>
		<p>Create new or select existing one for edit with drag & drop</p>
		<section data-name="landing-pages">
			<div>
				<div class="pull-left search-bar">
					<div class="input-group">
						<input id="txtSearch" type="text" class="form-control" placeholder="Search for..." />
						<span class="input-group-btn"><button class="btn btn-secondary" type="button" id="btnSearch">Search</button></span>
					</div>	
				</div>
				<div class="pull-right add-panel">
					<button class="btn btn-secondary" type="button" id="btnAddLandingPage">Add Landing Page</button>
				</div>
			</div>
			<div class="grid" id="divLandingPageList">
				<div class="spinner">
					<div class="rect1"></div>
					<div class="rect2"></div>
					<div class="rect3"></div>
					<div class="rect4"></div>
					<div class="rect5"></div>
				</div>
				<p>Loading...</p>
			</div>
		</section>
	</div>
</asp:Content>
<asp:Content runat="server" ContentPlaceHolderID="plScripts">
	<script type="text/javascript" src="/Sm/js/bootstrap/bootstrap-typeahead.js"></script>
	<script type="text/javascript">
		_COMMON_PAGE_SIZE = 10;

		$(function () {
			var landingPageGrid;
			landingPageGrid = new _COMMON.Grid("/sm/smhandler.aspx", "landingPageGrid", "divLandingPageList", manageLandingPages.FACTORY, manageLandingPages.FACTORY.onLoaded, { command: "loadLandingPageGrid", lenderConfigID: '<%=LenderConfigID%>' });
			landingPageGrid.init();
			manageLandingPages.FACTORY.init(landingPageGrid.GridObject);
			$("#btnSearch").on("click", manageLandingPages.FACTORY.search);
			$("#btnAddLandingPage").on("click", manageLandingPages.FACTORY.showAddLandingPageForm);
		});
		(function (manageLandingPages, $, undefined) {
			manageLandingPages.DATA = {};
			manageLandingPages.FACTORY = {};
			manageLandingPages.FACTORY.init = function (gridInstance) {
				var self = this;
				manageLandingPages.DATA.grid = gridInstance;
			};
			manageLandingPages.FACTORY.getFilterInfo = function () {
				//implement this later
				return { Keyword: $.trim($("#txtSearch").val()) };
			};
			manageLandingPages.FACTORY.onLoaded = function () {
				//$("a[data-command='audit-logs']", "#divLandingPageList").each(function (idx, ele) {
				//	var $self = $(ele);
				//	$self.on("click", function () {
				//		//title, dataId, containerId, htmlContent, getContentUrl, postData, onInitCallback, onClosingCallback
				//		var landingPageId = $self.closest("tr").data("id");
				//		_COMMON.showDialogClose("Audit Logs", landingPageId, "loadlandingpageauditlogs_dialog", "", "", null, function (container) {
				//			//init grid
				//			$("#loadlandingpageauditlogs_dialog").closest("div.modal-dialog").css({ "width": "80%" });
				//			var landingPageAuditLogsGrid = new _COMMON.Grid("/sm/smhandler.aspx", "landingPageAuditLogsGrid", "loadlandingpageauditlogs_dialog", null, null, { command: "loadLandingPageAuditLogs", landingPageID: landingPageId });
				//			landingPageAuditLogsGrid.init();
				//		}, null);
				//	});
				//});
				$("a[data-command='clone']", "#divLandingPageList").each(function (idx, ele) {
					var $self = $(ele);
					$self.on("click", function () {
						//title, dataId, containerId, htmlContent, getContentUrl, postData, onInitCallback, onClosingCallback
						var landingPageId = $self.closest("tr").data("id");
						_COMMON.showDialogSaveClose("Clone landing page", 0, "clonelandingpage_dialog", "", "/sm/smhandler.aspx", { command: "loadCloneLandingPageForm", landingPageId: landingPageId }, manageLandingPages.FACTORY.cloneLandingPage, function (container) {
							//add validator
							registerDataValidator($("#clonelandingpage_dialog"), "clonelandingpage_dialog");

						}, function () {
							//reload grid when dialog closed
							var pageIndex = manageLandingPages.DATA.grid.getPaginationSetting().page;
							manageLandingPages.DATA.grid.setFilter(manageLandingPages.FACTORY.getFilterInfo()).setPageIndex(pageIndex).load();
						});
					});
				});
				$("a[data-command='edit']", "#divLandingPageList").each(function (idx, ele) {
					var $self = $(ele);
					$self.on("click", function () {
						//title, dataId, containerId, htmlContent, getContentUrl, postData, onInitCallback, onClosingCallback
						manageLandingPages.FACTORY.showEditLandingPageForm($self.closest("tr").data("id"));
					});
				});
				$("input:checkbox[data-command='enable-btn']", $("#divLandingPageList")).each(function (idx, ele) {
					var $self = $(ele);
					$self.on("change", function () {
						manageLandingPages.FACTORY.ShowChangeStateDialog($self.closest("tr").data("id"), $self);
					});
				});
				$("a[data-command='delete']", "#divLandingPageList").each(function (idx, ele) {
					var $self = $(ele);
					$self.on("click", function () {
						//title, dataId, containerId, htmlContent, getContentUrl, postData, onInitCallback, onClosingCallback
						manageLandingPages.FACTORY.ShowDeleteDialog($self.closest("tr").data("id"));
					});
				});
			};
			manageLandingPages.FACTORY.ShowDeleteDialog = function (landingPageId) {
				_COMMON.showDialogSaveClose("Leave a comment", 0, "takenote_dialog_1", '<div style="margin:10px 20px;"><textarea data-id="txtComment" class="form-control required" rows="3" data-message-require="Comment cannot be empty" placeholder="Write a comment" maxlength="1000" display-counter-control="spanCommentCounter"></textarea><div class="text-right char-count"><span id="spanCommentCounter"></span></div><label for="txtComment" class="has-error" style="display:none"></label></div>', "", null, function (container) {
					var comment = $("textarea[data-id='txtComment']", $("#takenote_dialog_1")).val();
					manageLandingPages.FACTORY.DeleteLandingPage(landingPageId, comment, container);
				}, function (container) {
					$("textarea[data-id='txtComment']", $(container)).counter();
				}, function () {
					//reload grid when dialog closed
					var pageIndex = manageLandingPages.DATA.grid.getPaginationSetting().page;
					manageLandingPages.DATA.grid.setFilter(manageLandingPages.FACTORY.getFilterInfo()).setPageIndex(pageIndex).load();
				});
			};
			manageLandingPages.FACTORY.cloneLandingPage = function (obj) {
				if ($.smValidate("clonelandingpage_dialog") == false) return;
				var data = obj.container.find("form").serializeJSON({ parseAll: true, checkboxUncheckedValue: "false" });
				//update data
				var postData = {
					command: "cloneLandingPage",
					post_data: _COMMON.toJSON(data)
				};
				$.ajax({
					url: "/sm/smhandler.aspx",
					async: true,
					cache: false,
					type: 'POST',
					dataType: 'html',
					data: postData,
					success: function (responseText) {
						var response = $.parseJSON(responseText);
						if (response.IsSuccess) {
							_COMMON.noty("success", "Success", 500);
							obj.dialog.close();
						} else if (response.Info == "EXISTED") {
							_COMMON.showAlertDialog(response.Message);
						} else {
							var errorMsg = response.Message !== "" ? response.Message : "Error";
							_COMMON.noty("error", errorMsg, 500);
						}
					}
				});
			}
			manageLandingPages.FACTORY.ShowChangeStateDialog = function (landingPageId, srcEle) {
				_COMMON.showDialogSaveClose("Leave a comment", 0, "takenote_dialog_0", '<div style="margin:10px 20px;"><textarea data-id="txtComment" class="form-control required" rows="3" data-message-require="Comment cannot be empty" placeholder="Write a comment" maxlength="1000" display-counter-control="spanCommentCounter"></textarea><div class="text-right char-count"><span id="spanCommentCounter"></span></div><label for="txtComment" class="has-error" style="display:none"></label></div>', "", null, function (container) {
					var comment = $("textarea[data-id='txtComment']", $("#takenote_dialog_0")).val();
					manageLandingPages.FACTORY.ChangeState(landingPageId, $(srcEle).is(":checked"), srcEle, comment, container);
				}, function (container) {
					$("textarea[data-id='txtComment']", $(container)).counter();
				}, function () {
					//reload grid when dialog closed
					var pageIndex = manageLandingPages.DATA.grid.getPaginationSetting().page;
					manageLandingPages.DATA.grid.setFilter(manageLandingPages.FACTORY.getFilterInfo()).setPageIndex(pageIndex).load();
				});
			};
			manageLandingPages.FACTORY.DeleteLandingPage = function (landingPageId, comment, container) {
				var dataObj = { landingPageID: landingPageId, comment: comment, command: "deleteLandingPage" }
				$.ajax({
					url: "/sm/smhandler.aspx",
					async: true,
					cache: false,
					type: 'POST',
					dataType: 'html',
					data: dataObj,
					success: function (responseText) {
						var response = $.parseJSON(responseText);
						if (response.IsSuccess) {
							_COMMON.noty("success", "Success", 500);
							var pageIndex = manageLandingPages.DATA.grid.getPaginationSetting().page;
							manageLandingPages.DATA.grid.setFilter(manageLandingPages.FACTORY.getFilterInfo()).setPageIndex(pageIndex).load();
							container.dialog.close();
						} else {
							_COMMON.noty("error", "Error", 500);
							$(sourceEle).prop("checked", !isEnabled);
						}
					}
				});
			};
			manageLandingPages.FACTORY.ChangeState = function (landingPageId, isEnabled, sourceEle, comment, container) {
				var dataObj = { landingPageID: landingPageId, comment: comment, enable: isEnabled ? "Y" : "N", command: "changeLandingPageStatus" }
				$.ajax({
					url: "/sm/smhandler.aspx",
					async: true,
					cache: false,
					type: 'POST',
					dataType: 'html',
					data: dataObj,
					success: function (responseText) {
						var response = $.parseJSON(responseText);
						if (response.IsSuccess) {
							_COMMON.noty("success", "Success", 500);
							var pageIndex = manageLandingPages.DATA.grid.getPaginationSetting().page;
							manageLandingPages.DATA.grid.setFilter(manageLandingPages.FACTORY.getFilterInfo()).setPageIndex(pageIndex).load();
							container.dialog.close();
						} else {
							_COMMON.noty("error", "Error", 500);
							$(sourceEle).prop("checked", !isEnabled);
						}
					}
				});
			};
			manageLandingPages.FACTORY.clone = function (obj) {
				if ($.smValidate("clonelandingpage_dialog") == false) return;
				var data = obj.container.find("form").serializeJSON({ parseAll: true, checkboxUncheckedValue: "false" });
				//update data
				var postData = {
					command: "cloneLandingPage",
					post_data: _COMMON.toJSON(data)
				};
				$.ajax({
					url: "/sm/smhandler.aspx",
					async: true,
					cache: false,
					type: 'POST',
					dataType: 'html',
					data: postData,
					success: function (responseText) {
						var response = $.parseJSON(responseText);
						if (response.IsSuccess) {
							_COMMON.noty("success", "Success", 500);
							obj.dialog.close();
						} else if (response.Info == "EXISTED") {
							_COMMON.showAlertDialog(response.Message);
						} else {
							var errorMsg = response.Message !== "" ? response.Message : "Error";
							_COMMON.noty("error", errorMsg, 500);
						}
					}
				});
			}
			manageLandingPages.FACTORY.search = function () {
				manageLandingPages.DATA.grid.setFilter(manageLandingPages.FACTORY.getFilterInfo()).setPageIndex(1).load();
			};
			manageLandingPages.FACTORY.showAddLandingPageForm = function () {
				_COMMON.showDialogSaveClose("Add New Landing Page", 0, "addlandingpage_dialog_0", "", "/sm/smhandler.aspx", { command: "loadAddLandingPageForm", lenderConfigID: '<%=LenderConfigID%>' }, manageLandingPages.FACTORY.saveNewLandingPage, function (container) {
					//add validator
					registerDataValidator($("#addlandingpage_dialog_0"), "addlandingpage_dialog_0");
				}, function () {
					//reload grid when dialog closed
					var pageIndex = manageLandingPages.DATA.grid.getPaginationSetting().page;
					manageLandingPages.DATA.grid.setFilter(manageLandingPages.FACTORY.getFilterInfo()).setPageIndex(pageIndex).load();
				});
			};
			manageLandingPages.FACTORY.saveNewLandingPage = function (obj) {
				if ($.smValidate("addlandingpage_dialog_0") == false) return;
				var landingPageData = obj.container.find("form").serializeJSON({ parseAll: true, checkboxUncheckedValue: "false" });
				//update data
				var postData = {
					command: "saveNewLandingPage",
					landingpage_data: _COMMON.toJSON(landingPageData),
					template: $("#ddlTemplate").val()
				};
				$.ajax({
					url: "/sm/smhandler.aspx",
					async: true,
					cache: false,
					type: 'POST',
					dataType: 'html',
					data: postData,
					success: function (responseText) {
						var response = $.parseJSON(responseText);
						if (response.IsSuccess) {
							_COMMON.noty("success", "Success", 500);
							obj.dialog.close();
						} else if (response.Info == "EXISTED") {
							_COMMON.showAlertDialog(response.Message);
						} else {
							var errorMsg = response.Message !== "" ? response.Message : "Error";
							_COMMON.noty("error", errorMsg, 500);
						}
					}
				});
			};

			manageLandingPages.FACTORY.showEditLandingPageForm = function (id) {
				_COMMON.showDialogSaveClose("Edit Landing Page", 0, "editlandingpage_dialog", "", "/sm/smhandler.aspx", { command: "loadEditLandingPageForm", landingPageId: id }, manageLandingPages.FACTORY.updateLandingPage, function (container) {
					//add validator
					registerDataValidator($("#editlandingpage_dialog"), "editlandingpage_dialog");
				}, function () {
					//reload grid when dialog closed
					var pageIndex = manageLandingPages.DATA.grid.getPaginationSetting().page;
					manageLandingPages.DATA.grid.setFilter(manageLandingPages.FACTORY.getFilterInfo()).setPageIndex(pageIndex).load();
				});
			};
			manageLandingPages.FACTORY.updateLandingPage = function (obj) {
				if ($.smValidate("editlandingpage_dialog") == false) return;
				var landingPageData = obj.container.find("form").serializeJSON({ parseAll: true, checkboxUncheckedValue: "false" });
				//update data
				var postData = {
					command: "updateLandingPage",
					landingpage_data: _COMMON.toJSON(landingPageData)
				};
				$.ajax({
					url: "/sm/smhandler.aspx",
					async: true,
					cache: false,
					type: 'POST',
					dataType: 'html',
					data: postData,
					success: function (responseText) {
						var response = $.parseJSON(responseText);
						if (response.IsSuccess) {
							_COMMON.noty("success", "Success", 500);
							obj.dialog.close();
						} else if (response.Info == "EXISTED") {
							_COMMON.showAlertDialog(response.Message);
						} else {
							var errorMsg = response.Message !== "" ? response.Message : "Error";
							_COMMON.noty("error", errorMsg, 500);
						}
					}
				});
			};
		}(window.manageLandingPages = window.manageLandingPages || {}, jQuery));
		function registerDataValidator(container, groupName) {
			var $container = $(container);
			$.smValidate.removeValidationGroup(groupName);
			$("[data-required='true'],[data-format]", $container).each(function (idx, ele) {
				$(ele).observer({
					validators: [
						function (partial) {
							var $self = $(this);
							if ($self.data("required") && $.trim($self.val()) === "") {
								if ($self.data("message-require")) {
									return $self.data("message-require");
								} else {
									return "This field is required";
								}
							}
							var format = $self.data("format");
							if (format) {
								var msg = "Invalid format";
								if ($self.data("message-invalid-data")) {
									msg = $self.data("message-invalid-data");
								}
								if (format === "email") {
									if (_COMMON.ValidateEmail($self.val()) === false) {
										return msg;
									}
								} else if (format === "phone") {
									if (_COMMON.ValidatePhone($self.val()) === false) {
										return msg;
									}
								} else if ($self.val() != "" && format === "positive-number") {
									if ((parseInt($self.val()) > 0) == false) {
										return msg;
									}
								} else if (format === "guid") {
									if (/^[0-9A-Za-z]{8}-[0-9A-Za-z]{4}-[0-9A-Za-z]{4}-[0-9A-Za-z]{4}-[0-9A-Za-z]{12}$/.test($self.val()) == false) {
										return msg;
									}
								}
							}
							return "";
						}
					],
					validateOnBlur: true,
					group: groupName
				});
			});
		}
	</script>
</asp:Content>
