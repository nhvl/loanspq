﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="DefaultManage.aspx.vb" Inherits="Sm_DefaultManage" MasterPageFile="SiteManager.master" %>

<asp:Content runat="server" ContentPlaceHolderID="plBody">
	<div>
		<h1>Application Portal Utilities</h1>
		<p>Tools for synchronizing and updating your Application Portal with lender settings (back-office)</p>
		<section data-name="System Caches">
			<%--<h2 class="section-title">System Caches</h2>
			<p>This system stores data so future requests for that data can be served faster; the data stored in a cache might be the result of an earlier computation, or the duplicate of data stored elsewhere.</p>
				--%>

			<div class="bottom30">
				<div class="input-block">
					<button id="btnClearConfig" class="btn btn-warning mb10 mr5"><i class="fa fa-refresh" aria-hidden="true"></i>Sync Settings</button>
					<label style="display: none;">Portal configuration items have been successfully synced with back office.</label>
					<p>A few settings on lender side (back-office) carry over to the Application Portal automatically overnight. Click the Sync Settings button to immediately update the Application Portal with current lender side settings such as Purpose Types, Credit Card Types, Home Equity Properties, and more.</p>
					<p>NOTE: </p>
					<p>1) This feature does not carry over any disclosures or approval messages.</p>
					<p>2) If you just made a change on lender side, you should wait about 2 minutes before using the  Sync Settings button.</p>
				</div>
			</div>
		
			<div class="bottom30">
				<div class="input-block">
					<button id="btnClearPdf" class="btn btn-warning mb10 mr5"><i class="fa fa-refresh" aria-hidden="true"></i>Sync PDF Files</button>
					<label style="display: none;">PDF files have been successfully synced.</label>
					<p>This system stores PDF files so future requests for that data can be served faster; the data stored might be the result of an earlier computation, or the duplicate of data stored elsewhere.  Click Sync PDF Files button to immediately sync PDF files which are automatically synced every other day.</p>
					
				</div>
			</div>
		</section>
	</div>
</asp:Content>
<asp:Content runat="server" ContentPlaceHolderID="plScripts">
	<script type="text/javascript">
		$(function () {
			$("#btnClearConfig").on("click", function () {
				var $self = $(this);
				var dataObj = {};
				dataObj.command = "clearCache";
				dataObj.lenderconfigid = '<%=LenderConfigID%>';
				$.ajax({
					url: "/sm/smhandler.aspx",
					async: true,
					cache: false,
					type: 'POST',
					dataType: 'html',
					data: dataObj,
					success: function (responseText) {
						var response = $.parseJSON(responseText);
						var $lbl = $self.next("label");
						if (response.IsSuccess) {
							$lbl.text("Portal configuration items have been successfully synced with back office.");
							$lbl.removeClass("error-msg").addClass("success-msg").show();
							setTimeout(function() {
								$lbl.fadeOut(1000);
							}, 3000);
						} else {
							$self.next("label").text("Error");
							$lbl.removeClass("success-msg").addClass("error-msg").show();
						}
					}
				});
			});
			$("#btnClearPdf").on("click", function () {
				var $self = $(this);
				var dataObj = {};
				dataObj.command = "clearPDF";
				dataObj.lenderconfigid = '<%=LenderConfigID%>';
				$.ajax({
					url: "/sm/smhandler.aspx",
					async: true,
					cache: false,
					type: 'POST',
					dataType: 'html',
					data: dataObj,
					success: function (responseText) {
						var response = $.parseJSON(responseText);
						var $lbl = $self.next("label");
						if (response.IsSuccess) {
							$lbl.text("PDF files have been successfully synced.");
							$lbl.removeClass("error-msg").addClass("success-msg").show();
							setTimeout(function () {
								$lbl.fadeOut(1000);
							}, 3000);
						} else {
							$self.next("label").text("Error");
							$lbl.removeClass("success-msg").addClass("error-msg").show();
						}
					}
				});
			});
		});
	</script>
</asp:Content>