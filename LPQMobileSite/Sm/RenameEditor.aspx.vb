﻿
Imports System.Xml
Imports Sm
Imports LPQMobile.Utils
Imports System.Activities.Statements

Partial Class Sm_RenameEditor
	Inherits SmApmBasePage

	Protected PreviewUrlList As New Dictionary(Of String, String)
	Protected DefaultLoanType As String = ""
	Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
		If Not Page.IsPostBack Then
			Dim smMasterPage = CType(Me.Master, Sm_SiteManager)
			smMasterPage.LenderConfigXml = LenderConfigXml
			smMasterPage.RevisionID = LenderConfig.RevisionID
			smMasterPage.LastModifiedBy = LenderConfig.LastModifiedByUserFullName
			smMasterPage.LastModifiedByUserID = LenderConfig.LastModifiedByUserID
			smMasterPage.SelectedMainMenuItem = "design"
			smMasterPage.SelectedSubMenuItem = "rename"
			smMasterPage.ShowPreviewButton = False
			smMasterPage.RemoveQueryStringList.Add("loantype")
			DefaultLoanType = Common.SafeString(Request.QueryString("loantype")).ToUpper()
			If String.IsNullOrEmpty(DefaultLoanType) Then
				If EnablePL Then
					DefaultLoanType = "PL"
				ElseIf EnableCC Then
					DefaultLoanType = "CC"
				ElseIf EnableVL Then
					DefaultLoanType = "VL"
				ElseIf EnableHE Then
					DefaultLoanType = "HE"
				ElseIf EnableBL Then
					DefaultLoanType = "BL"
				ElseIf EnableXA Then
					DefaultLoanType = "XA"
				ElseIf EnableLQB Then
					DefaultLoanType = "LQB_MAIN"
				End If
			End If
			smMasterPage.BreadCrumb = "Design / Verbiage / " & GetModuleName(DefaultLoanType)

			If Not Regex.IsMatch(DefaultLoanType.Trim(), "^(XA|SA|XA_SPECIAL|SA_SPECIAL|CC|CC_COMBO|HE|HE_COMBO|PL|PL_COMBO|VL|VL_COMBO|LP|ST|BL|XA_BUSINESS|SA_BUSINESS|LQB_MAIN|XA_MINOR|SA_MINOR)$") Then
				Response.Redirect("~/NoAccess.aspx", True)
			ElseIf ApmComboEnable = False AndAlso Regex.IsMatch(DefaultLoanType.Trim(), "^(CC_COMBO|HE_COMBO|PL_COMBO|VL_COMBO)$") Then
				Response.Redirect(String.Format("~/Sm/Index.aspx?lenderconfigid={0}", LenderConfigID), True)
			End If

            Dim nodeGroup As SmSettings.NodeGroup
			Select Case DefaultLoanType
				Case "XA"
					nodeGroup = SmSettings.NodeGroup.ICERenameXA
				Case "CC"
					nodeGroup = SmSettings.NodeGroup.ICERenameCC
				Case "HE"
					nodeGroup = SmSettings.NodeGroup.ICERenameHE
				Case "VL"
					nodeGroup = SmSettings.NodeGroup.ICERenameVL
				Case "PL"
					nodeGroup = SmSettings.NodeGroup.ICERenamePL
				Case "BL"
					nodeGroup = SmSettings.NodeGroup.ICERenameBL
				Case "SA"
					nodeGroup = SmSettings.NodeGroup.ICERenameSA
				Case "CC_COMBO"
					nodeGroup = SmSettings.NodeGroup.ICERenameCCCombo
				Case "HE_COMBO"
					nodeGroup = SmSettings.NodeGroup.ICERenameHECombo
				Case "VL_COMBO"
					nodeGroup = SmSettings.NodeGroup.ICERenameVLCombo
				Case "PL_COMBO"
					nodeGroup = SmSettings.NodeGroup.ICERenamePLCombo
				Case "LP"
					nodeGroup = SmSettings.NodeGroup.ICERenameLanding
				Case "XA_SPECIAL"
					nodeGroup = SmSettings.NodeGroup.ICERenameXASpecial
				Case "SA_SPECIAL"
					nodeGroup = SmSettings.NodeGroup.ICERenameSASpecial
				Case "XA_BUSINESS"
					nodeGroup = SmSettings.NodeGroup.ICERenameXABusiness
				Case "SA_BUSINESS"
					nodeGroup = SmSettings.NodeGroup.ICERenameSABusiness
				Case "XA_MINOR"
					'For backward compatibility and avoid the need for migration script, use values “xa_special” and “sa_special” for loan_type attribute for both XA Special and XA Minor - don't use values “xa_minor” or “sa_minor”
					'This may have the side effect that a change in XA Special may leak to XA Minor. We would have to ask client to use as-is.
					nodeGroup = SmSettings.NodeGroup.ICERenameXASpecial
				Case "SA_MINOR"
					'For backward compatibility and avoid the need for migration script, use values “xa_special” and “sa_special” for loan_type attribute for both XA Special and XA Minor - don't use values “xa_minor” or “sa_minor”
					'This may have the side effect that a change in XA Special may leak to XA Minor. We would have to ask client to use as-is.
					nodeGroup = SmSettings.NodeGroup.ICERenameSASpecial
				Case "ST"
					nodeGroup = SmSettings.NodeGroup.ICERenameCheckStatus
				Case "LQB_MAIN"
					nodeGroup = SmSettings.NodeGroup.ICERenameLQBMain
			End Select
			smMasterPage.CurrentNodeGroup = nodeGroup

			If CheckEnabledModule(SmSettings.ModuleName.XA) _
				OrElse CheckEnabledModule(SmSettings.ModuleName.CC) _
				OrElse CheckEnabledModule(SmSettings.ModuleName.CC_COMBO) _
				OrElse CheckEnabledModule(SmSettings.ModuleName.HE) _
				OrElse CheckEnabledModule(SmSettings.ModuleName.HE_COMBO) _
				OrElse CheckEnabledModule(SmSettings.ModuleName.PL) _
				OrElse CheckEnabledModule(SmSettings.ModuleName.PL_COMBO) _
				OrElse CheckEnabledModule(SmSettings.ModuleName.VL) _
				OrElse CheckEnabledModule(SmSettings.ModuleName.VL_COMBO) _
				OrElse CheckEnabledModule(SmSettings.ModuleName.BL) Then
				PreviewUrlList.Add("LP", String.Format("{0}/apply.aspx?lenderref={1}&mode=777&noencrypt=true", ServerRoot, LenderRef))
				PreviewUrlList.Add("ST", String.Format("{0}/cu/ViewSubmittedLoans.aspx?lenderref={1}&mode=777&noencrypt=true&feature=rename", ServerRoot, LenderRef))
			End If
			
            If CheckEnabledModule(SmSettings.ModuleName.BL) Then
				PreviewUrlList.Add("BL", String.Format("{0}/apply.aspx?lenderref={1}&autofill=true&mode=777&noencrypt=true&list=bl&feature=rename", ServerRoot, LenderRef))
			End If
			If CheckEnabledModule(SmSettings.ModuleName.CC) Then
				PreviewUrlList.Add("CC", String.Format("{0}/cc/creditcard.aspx?lenderref={1}&autofill=true&mode=777&noencrypt=true&feature=rename", ServerRoot, LenderRef))
			End If
            If CheckEnabledModule(SmSettings.ModuleName.HE) Then
				PreviewUrlList.Add("HE", String.Format("{0}/he/homeequityloan.aspx?lenderref={1}&autofill=true&mode=777&noencrypt=true&feature=rename", ServerRoot, LenderRef))
            End If
            If CheckEnabledModule(SmSettings.ModuleName.PL) Then
				PreviewUrlList.Add("PL", String.Format("{0}/pl/personalloan.aspx?lenderref={1}&autofill=true&mode=777&noencrypt=true&feature=rename", ServerRoot, LenderRef))
            End If
            If CheckEnabledModule(SmSettings.ModuleName.VL) Then
				PreviewUrlList.Add("VL", String.Format("{0}/vl/vehicleloan.aspx?lenderref={1}&autofill=true&mode=777&noencrypt=true&feature=rename", ServerRoot, LenderRef))
            End If

			If CheckEnabledModule(SmSettings.ModuleName.CC_COMBO) Then
				PreviewUrlList.Add("CC_COMBO", String.Format("{0}/cc/creditcard.aspx?lenderref={1}&autofill=true&mode=777&noencrypt=true&type=1&feature=rename", ServerRoot, LenderRef))
			End If
			If CheckEnabledModule(SmSettings.ModuleName.HE_COMBO) Then
				PreviewUrlList.Add("HE_COMBO", String.Format("{0}/he/homeequityloan.aspx?lenderref={1}&autofill=true&mode=777&noencrypt=true&type=1&feature=rename", ServerRoot, LenderRef))
			End If
			If CheckEnabledModule(SmSettings.ModuleName.PL_COMBO) Then
				PreviewUrlList.Add("PL_COMBO", String.Format("{0}/pl/personalloan.aspx?lenderref={1}&autofill=true&mode=777&noencrypt=true&type=1&feature=rename", ServerRoot, LenderRef))
			End If
			If CheckEnabledModule(SmSettings.ModuleName.VL_COMBO) Then
				PreviewUrlList.Add("VL_COMBO", String.Format("{0}/vl/vehicleloan.aspx?lenderref={1}&autofill=true&mode=777&noencrypt=true&type=1&feature=rename", ServerRoot, LenderRef))
			End If

			If CheckEnabledModule(SmSettings.ModuleName.LQB) Then
				PreviewUrlList.Add("LQB_MAIN", String.Format("{0}/Account/Login?lenderref={1}&mode=777&apm=verbiage&quicknav=true&apmstub=true", Common.LQB_WEBSITE, LenderRef))
			End If

			If CheckEnabledModule(SmSettings.ModuleName.XA) Then
				PreviewUrlList.Add("XA", String.Format("{0}/xa/xpressapp.aspx?lenderref={1}&autofill=true&mode=777&noencrypt=true&feature=rename", ServerRoot, LenderRef))
                PreviewUrlList.Add("SA", String.Format("{0}/xa/xpressapp.aspx?lenderref={1}&autofill=true&mode=777&noencrypt=true&type=2&feature=rename", ServerRoot, LenderRef))

                Dim minorAccountType As Dictionary(Of String, String) = Common.getItemsFromConfig(WebsiteConfig, "XA_LOAN/SPECIAL_ACCOUNT_TYPE/ITEM", "account_code", "title")
                Dim minorProductDic As Dictionary(Of String, String) = Common.getItemsFromConfig(WebsiteConfig, "XA_LOAN/SPECIAL_ACCOUNTS/ACCOUNT", "product_code", "minor_account_code")
                If minorAccountType IsNot Nothing AndAlso minorProductDic IsNot Nothing AndAlso minorAccountType.Count > 0 AndAlso minorProductDic.Count > 0 Then
                    PreviewUrlList.Add("XA_MINOR", String.Format("{0}/xa/xpressapp.aspx?lenderref={1}&autofill=true&mode=777&noencrypt=true&type=1a&feature=rename", ServerRoot, LenderRef))
                    PreviewUrlList.Add("SA_MINOR", String.Format("{0}/xa/xpressapp.aspx?lenderref={1}&autofill=true&mode=777&noencrypt=true&type=2a&feature=rename", ServerRoot, LenderRef))
                End If
				Dim specialAccountTypeList = Common.GetSpecialAccountTypeList(WebsiteConfig)
                If specialAccountTypeList IsNot Nothing AndAlso specialAccountTypeList.Count > 0 AndAlso specialAccountTypeList.Any(Function(p) p.Roles IsNot Nothing AndAlso p.Roles.Count > 0) Then
                    PreviewUrlList.Add("XA_SPECIAL", String.Format("{0}/apply.aspx?lenderref={1}&autofill=true&mode=777&noencrypt=true&list=1s&feature=rename", ServerRoot, LenderRef))
                    PreviewUrlList.Add("SA_SPECIAL", String.Format("{0}/apply.aspx?lenderref={1}&autofill=true&mode=777&noencrypt=true&list=2s&feature=rename", ServerRoot, LenderRef))
                End If
				Dim businessAccountTypeList = Common.GetBusinessAccountTypeList(WebsiteConfig)
                If businessAccountTypeList IsNot Nothing AndAlso businessAccountTypeList.Count > 0 AndAlso businessAccountTypeList.Any(Function(p) p.Roles IsNot Nothing AndAlso p.Roles.Count > 0) Then
                    PreviewUrlList.Add("XA_BUSINESS", String.Format("{0}/apply.aspx?lenderref={1}&autofill=true&mode=777&noencrypt=true&list=1b&feature=rename", ServerRoot, LenderRef))
                    PreviewUrlList.Add("SA_BUSINESS", String.Format("{0}/apply.aspx?lenderref={1}&autofill=true&mode=777&noencrypt=true&list=2b&feature=rename", ServerRoot, LenderRef))
                End If

            End If
        End If
	End Sub

	Protected Function GetNavigateUrl(type As String) As String
		Return String.Format("{0}?lenderconfigid={1}&loantype={2}", Request.Path, Request.QueryString("lenderconfigid"), type)
	End Function

	
End Class

