﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="CustomQuestions.aspx.vb" Inherits="Sm_CustomQuestions" MasterPageFile="SiteManager.master" %>


<asp:Content runat="server" ContentPlaceHolderID="plBody">
	<div id="divCustomQuestionsPage">
		<h1 class="page-header">Custom Questions</h1>
		<p>Configure questions that are visible in your Application Portal</p>
		<section>
            <%-- For other devs: Do not remove below line. --%>
            <%="" %>
			<ul class="nav nav-tabs" id="mainTabs" role="tablist">
				<%If CurrentModule = SmSettings.ModuleName.XA Then%>
				<li role="presentation" class="active"><a href="#" aria-controls="tabCQ" role="tab">XA</a></li>
				<%Else %>
				<li role="presentation" <%=IIf(EnableXA, "", "class='hidden'")%>><a href="<%=BuildUrl("/sm/customquestions.aspx", New KeyValuePair(Of String, String)("cqtab", "xa"))%>" role="tab">XA</a></li>
				<%End If%>
				<%If CurrentModule = SmSettings.ModuleName.CC Then%>
				<li role="presentation" class="active"><a href="#" aria-controls="tabCQ" role="tab">Credit Card</a></li>
				<%Else %>
				<li role="presentation" <%=IIf(EnableCC, "", "class='hidden'")%>><a href="<%=BuildUrl("/sm/customquestions.aspx", New KeyValuePair(Of String, String)("cqtab", "cc"))%>" role="tab">Credit Card</a></li>
				<%End If%>
				<%If CurrentModule = SmSettings.ModuleName.HE Then%>
				<li role="presentation" class="active"><a href="#" aria-controls="tabCQ" role="tab">Home Equity Loan</a></li>
				<%Else %>
				<li role="presentation" <%=IIf(EnableHE, "", "class='hidden'")%>><a href="<%=BuildUrl("/sm/customquestions.aspx", New KeyValuePair(Of String, String)("cqtab", "he"))%>" role="tab">Home Equity Loan</a></li>
				<%End If%>
				<%If CurrentModule = SmSettings.ModuleName.PL Then%>
				<li role="presentation" class="active"><a href="#" aria-controls="tabCQ" role="tab">Personal Loan</a></li>
				<%Else %>
				<li role="presentation" <%=IIf(EnablePL, "", "class='hidden'")%>><a href="<%=BuildUrl("/sm/customquestions.aspx", New KeyValuePair(Of String, String)("cqtab", "pl"))%>" role="tab">Personal Loan</a></li>
				<%End If%>
				<%If CurrentModule = SmSettings.ModuleName.VL Then%>
				<li role="presentation" class="active"><a href="#" aria-controls="tabCQ" role="tab">Vehicle Loan</a></li>
				<%Else %>
				<li role="presentation" <%=IIf(EnableVL, "", "class='hidden'")%>><a href="<%=BuildUrl("/sm/customquestions.aspx", New KeyValuePair(Of String, String)("cqtab", "vl"))%>" role="tab">Vehicle Loan</a></li>
				<%End If%>
				<%If CurrentModule = SmSettings.ModuleName.BL Then%>
				<li role="presentation" class="active"><a href="#" aria-controls="tabCQ" role="tab">Business Loan</a></li>
				<%Else %>
				<li role="presentation" <%=IIf(EnableBL, "", "class='hidden'")%>><a href="<%=BuildUrl("/sm/customquestions.aspx", New KeyValuePair(Of String, String)("cqtab", "bl"))%>" role="tab">Business Loan</a></li>
				<%End If%>
			</ul>
			<div class="tab-content">
				<div role="tabpanel" class="tab-pane active" id="tabCQ">
					<h2 class="section-title"><%=TabTitle%></h2>
					<p>Custom Questions are configured through the in-branch settings. Please make in-branch updates prior to making updates here. If use Advanced Conditioning features, you will need to replicate those conditions via the Business Rules page.</p>
					<p> To avoid issues, in-branch Custom Question names should not contain the following special characters: <br />
$    ;    /    ?    :     @   =    &    “    <    >    #    %    {    }    |    \    ^    ~    [    ]    ‘</p>
                    <%--<ul class="sm-tab-strip">
						<li><a>APPLICANT QUESTIONS</a></li>
						<li><a>APPLICATION QUESTIONS</a></li>
					</ul>--%>
					<%If CheckEnabledModule(CurrentModule) Then%>
						<div class="applicant-cq">
							<div class="checkbox-group">
								<p><i class="fa fa-caret-right" aria-hidden="true"></i><span>APPLICANT QUESTIONS</span></p>
								<div class="checkbox-group-header">
									<div class="checkbox checkbox-success checkbox-inline" data-toggle="tooltip" data-placement="bottom" title="This is bypass mode so all questions from in-branch that are visible for consumer will show up on Application Portal.">
										<input type="checkbox" data-command="showAll" id="chkXAApplicantShowAll" <%=BindShowAllCheckbox(True)%>/>
										<label for="chkXAApplicantShowAll"> Show All</label>
									</div>
									<div class="checkbox checkbox-success checkbox-inline">
										<input type="checkbox" data-command="hideAll" id="chkCQApplicantHideAll" <%=BindHideAllCheckbox(True)%>/>
										<label for="chkCQApplicantHideAll"> Hide All </label>
									</div>
								</div>
								<%If CustomApplicantQuestions IsNot Nothing AndAlso CustomApplicantQuestions.Any() Then%>
								<div class="checkbox-btn-wrapper custom-question">
									<span></span><span>Display on the following page</span>
								</div>
								<%For Each question As String In CustomApplicantQuestions%>
								<div class="checkbox-btn-wrapper custom-question">
									<span><%=question%></span>
									<select class="display-page-ddl form-control">
                                        <% For Each item As KeyValuePair(Of String, String) In DisplayPageApplicantOptions%>
								            <option <%=BindSelectbox(GetPageDisplayCQs(question, True), item.Key)%> value="<%=item.Key%>"><%=item.Value%></option>
								        <%Next%>
									</select>
									<label class="toggle-btn pull-right" data-on="ON" data-off="OFF">
										<input type="checkbox" data-text="<%=question%>" value="<%=question%>" <%=BindCheckbox(GetEnableCQs(question, True))%>/>
										<span class="button-checkbox"></span>
									</label>
								</div>			
								<%Next%>
								<%End If%>
							</div>
						</div>
						<div class="application-cq">
							<div class="checkbox-group">
								<p><i class="fa fa-caret-right" aria-hidden="true"></i><span>APPLICATION QUESTIONS</span></p>
								<div class="checkbox-group-header">
									<div class="checkbox checkbox-success checkbox-inline" data-toggle="tooltip" data-placement="bottom" title="This is bypass mode so all questions from in-branch that are visible for consumer will show up on Application Portal.">
										<input type="checkbox" data-command="showAll" id="chkXAApplicationShowAll" <%=BindShowAllCheckbox(False)%>/>
										<label for="chkXAApplicationShowAll"> Show All</label>
									</div>
									<div class="checkbox checkbox-success checkbox-inline">
										<input type="checkbox" data-command="hideAll" id="chkCQApplicationHideAll" <%=BindHideAllCheckbox(False)%>/>
										<label for="chkCQApplicationHideAll"> Hide All </label>
									</div>
								</div>
								<%If CustomQuestions IsNot Nothing AndAlso CustomQuestions.Any() Then%>
								<div class="checkbox-btn-wrapper custom-question">
									<span></span><span>Display on the following page</span>
								</div>
                                <%For Each question As String In CustomQuestions%>
								<div class="checkbox-btn-wrapper custom-question">
									<span><%=question%></span>
									<select class="display-page-ddl form-control">
                                        <% For Each item As KeyValuePair(Of String, String) In DisplayPageOptions%>
								            <option <%=BindSelectbox(GetPageDisplayCQs(question, False), item.Key)%> value="<%=item.Key%>"><%=item.Value%></option>
								        <%Next%>
									</select>
									<label class="toggle-btn pull-right" data-on="ON" data-off="OFF">
										<input type="checkbox" data-text="<%=question%>" value="<%=question%>" <%=BindCheckbox(GetEnableCQs(question, False))%>/>
										<span class="button-checkbox"></span>
									</label>
								</div>			
								<%Next%>
								<%End If%>
							</div>
						</div>
					<%End If%>
				</div>
			</div>
		</section>
	</div>
</asp:Content>
<asp:Content runat="server" ContentPlaceHolderID="plScripts">
	<script type="text/javascript">
		$(function () {
			$("input:checkbox", "#divCustomQuestionsPage").on("change", function () {
				master.FACTORY.documentChanged(this);
				toggleDisplayPageDdl(this);
			});
			$("input:checkbox[data-command='showAll']", "#divCustomQuestionsPage").on("change", function () {
				var $self = $(this);
				if ($self.is(":checked")) {
					$self.closest(".checkbox-group-header").find("input:checkbox[data-command='hideAll']").prop("checked", false);
					$("label.toggle-btn>input:checkbox", $self.closest(".checkbox-group")).each(function (idx, ele) {
						$(ele).prop("checked", false);
					});
				}
				if ($self.is(":checked") == false) {  //disable uncheck  via click
					$self.prop("checked", true);
				}

			});
			$("input:checkbox[data-command='hideAll']", "#divCustomQuestionsPage").on("change", function () {
				var $self = $(this);
				if ($self.is(":checked")) {
					$self.closest(".checkbox-group-header").find("input:checkbox[data-command='showAll']").prop("checked", false);
					$("label.toggle-btn>input:checkbox", $self.closest(".checkbox-group")).each(function (idx, ele) {
						$(ele).prop("checked", false);
					});
				}
				if ($self.is(":checked") == false) {  //disable uncheck  via click
					$self.prop("checked", true);
				}
			});
			$("label.toggle-btn>input:checkbox", "#divCustomQuestionsPage").on("change", function () {
				var $self = $(this);
				if ($self.is(":checked")) {
					var showAllElem = $self.closest(".checkbox-group").find("input:checkbox[data-command='showAll']");
					if(showAllElem.is(":checked")) {
						showAllElem.prop("checked", false);
						$("label.toggle-btn>input:checkbox", $self.closest(".checkbox-group")).each(function (idx, ele) {
							if(!$(ele).is($self)){
								$(ele).closest(".checkbox-btn-wrapper").find("select").toggleClass("invisible", true);
							}
						});
					}
					else {
						showAllElem.prop("checked", false);
					}
					$self.closest(".checkbox-group").find("input:checkbox[data-command='hideAll']").prop("checked", false);
				}
				else {
					var atLeastOneIsChecked = $("label.toggle-btn>input:checkbox:checked", $self.closest(".checkbox-group")).length > 0;
					if(!atLeastOneIsChecked){
						$self.closest(".checkbox-group").find("input:checkbox[data-command='hideAll']").prop("checked", true);
					}
				}
			});
			function setVisibilityDisplayPageDdl(){
				$("input:checkbox[data-command='showAll']", "#divCustomQuestionsPage").each(function (idx, ele) {
						var $self = $(ele);
						if($self.is(":checked")) {
							toggleDisplayPageDdl($self);
						}
						else {
							var hideAllcheckbox = $self.closest(".checkbox-group-header").find("input:checkbox[data-command='hideAll']");
							if(hideAllcheckbox.is(":checked")){
								toggleDisplayPageDdl(hideAllcheckbox);
							}
							else{
								$("label.toggle-btn>input:checkbox", $self.closest(".checkbox-group")).each(function (idx, ele) {
									toggleDisplayPageDdl($(ele));
								});
							}
						}
				});
			}
			function toggleDisplayPageDdl(element){
				var $self = $(element);
				if($self.attr("data-command") == null){
					$self.closest(".checkbox-btn-wrapper").find("select").toggleClass("invisible", !$self.is(":checked"));
				}
				else{
					$self.closest(".checkbox-group").find("select").toggleClass("invisible", ($self.attr("data-command") != "showAll"));
				}
            }
			setVisibilityDisplayPageDdl();
		});
		(function (master, $, undefined) {
			master.FACTORY.SaveDraft = function (revisionId) {
				var dataObj = collectSubmitData();
				dataObj.command = 'saveconfigurecq';
				dataObj.revisionId = revisionId;
				$.ajax({
					url: "/sm/smhandler.aspx",
					async: true,
					cache: false,
					type: 'POST',
					dataType: 'html',
					data: dataObj,
					success: function (responseText) {
						var response = $.parseJSON(responseText);
						if (response.IsSuccess) {
							master.FACTORY.onSaveDraftSuccess();
						} else {
							_COMMON.noty("error", "Error", 500);
						}
					}
				});
			};
			master.FACTORY.Preview = function (revisionId) {
				var dataObj = collectSubmitData();
				dataObj.command = 'previewconfigurecq';
				dataObj.revisionId = revisionId;
				$.ajax({
					url: "/sm/smhandler.aspx",
					async: true,
					cache: false,
					type: 'POST',
					dataType: 'html',
					data: dataObj,
					success: function (responseText) {
						var response = $.parseJSON(responseText);
						if (response.IsSuccess) {
							master.FACTORY.onPreviewSuccess();
							_COMMON.openInNewTab(response.Info.previewurl);
						} else {
							_COMMON.noty("error", "Error", 500);
						}
					}
				});
			};
			master.FACTORY.Publish = function (comment, publishAll) {
				var dataObj = collectSubmitData();
				dataObj.command = 'publishconfigurecq';
				dataObj.comment = comment;
				dataObj.publishAll = publishAll;
				$.ajax({
					url: "/sm/smhandler.aspx",
					async: true,
					cache: false,
					type: 'POST',
					dataType: 'html',
					data: dataObj,
					success: function (responseText) {
						var response = $.parseJSON(responseText);
						if (response.IsSuccess) {
							master.FACTORY.onPublishSuccess();
						} else {
							_COMMON.noty("error", "Error", 500);
						}
					}
				});
			};
		}(window.master = window.master || {}, jQuery));

		function collectSubmitData() {
			var result = {};
			result.lenderconfigid = '<%=LenderConfigID%>';
			result.tab = '<%=CurrentTab%>';
			result.ApplicantCQ = JSON.stringify(collectCheckboxGroupData($("#tabCQ .applicant-cq")));
			result.CQ = JSON.stringify(collectCheckboxGroupData($("#tabCQ .application-cq")));
			result.ApplicantHideAll = $("#chkCQApplicantHideAll").prop("checked");
			result.ApplicationHideAll = $("#chkCQApplicationHideAll").prop("checked");


			return result;
		}

		function collectCheckboxGroupData(container) {
			var $container = $(container);
			var result = [];
			$(".checkbox-btn-wrapper input[type='checkbox']", $container).each(function(idx, ele) {
				var $ele = $(ele);
				var item = {}
				item.Question = $ele.closest("div.checkbox-btn-wrapper").find("span").text();
				item.IsEnable = $ele.is(":checked");
				item.DisplayPage = $ele.closest("div.checkbox-btn-wrapper").find("select").val();
				result.push(item);
			});
			return result;
		}
	</script>
</asp:Content>
<asp:Content runat="server" ContentPlaceHolderID="plActionButtons"></asp:Content>