﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="MLSettings.aspx.vb" Inherits="Sm_MLSettings" MasterPageFile="SiteManager.master"  %>

<asp:Content runat="server" ContentPlaceHolderID="plBody">
	<div>
		<h1 class="page-header">Settings</h1>
		<p>Configure settings for your Application Portal</p>
		<section>
			<ul class="nav nav-tabs" id="mainTabs" role="tablist">
				<%If EnableCC Or EnableHE Or EnablePL Or EnableVL Or EnableBL Or EnableXA Then %>
				<li role="presentation"><a href="<%=BuildUrl("/sm/generalsettings.aspx")%>" role="tab">General</a></li>
				<% End If %>
				<li role="presentation" <%=IIf(EnableXA, "", "class='hidden'")%>><a href="<%=BuildUrl("/sm/xasettings.aspx")%>" role="tab">XA</a></li>
				<li role="presentation" <%=IIf(EnableCC Or EnableHE Or EnablePL Or EnableVL, "", "class='hidden'")%>><a href="<%=BuildUrl("/sm/loanssettings.aspx")%>" role="tab">All Loans</a></li>
				<li role="presentation" <%=IIf(EnableCC, "", "class='hidden'")%>><a href="<%=BuildUrl("/sm/ccsettings.aspx")%>" role="tab">Credit Card Loan</a></li>
				<li role="presentation" <%=IIf(EnableHE, "", "class='hidden'")%>><a href="<%=BuildUrl("/sm/hesettings.aspx")%>" role="tab">Home Equity Loan</a></li>
				<li role="presentation" class="active"><a href="#" aria-controls="mlloans" role="tab">Mortgage Loan</a></li>
				<li role="presentation" <%=IIf(EnablePL, "", "class='hidden'")%>><a href="<%=BuildUrl("/sm/plsettings.aspx")%>" role="tab">Personal Loan</a></li>
				<li role="presentation" <%=IIf(EnableVL, "", "class='hidden'")%>><a href="<%=BuildUrl("/sm/vlsettings.aspx")%>" role="tab">Vehicle Loan</a></li>
				<li role="presentation" <%=IIf(EnableBL, "", "class='hidden'")%>><a href="<%=BuildUrl("/sm/blsettings.aspx")%>" role="tab">Business Loan</a></li>
			</ul>
			<div class="tab-content">
				<div role="tabpanel" class="tab-pane active" id="plloans">
					<h2 class="section-title">Mortgage Loans</h2>
					<p>These settings will only affect Mortgage Loan applications.</p>
					<div class="bottom30">
						<div class="input-block">
							<h3 class="property-title">Application Default Loan Template</h3>
							<input type="text" class="form-control" id="txtDefaultLoanTemplate" value="<%=DefaultLoanTemplate%>" />
						</div>
						<div class="input-block">
							<h3 class="property-title">Default Loan Officer</h3>
							<input type="text" class="form-control" id="txtDefaultLoanOfficer" value="<%=LoanOfficer%>" />
						</div>
						<div class="input-block">
							<h3 class="property-title">Credit Reporting Agency</h3>
							<select id="txtCreditReportingAgency" class="form-control inline-select">
								<option value="" <%=BindSelectbox(CreditReportingAgency.ToString(), "")%>>Not set</option>
								<% For Each item As KeyValuePair(Of String, String) In ListCRAs %>
								<option <%=BindSelectbox(CreditReportingAgency.ToString(), item.Key)%> value="<%=item.Key%>"><%=item.Value%></option>
								<%Next%>
							</select>
						</div>
					</div>
					<div class="bottom30">
						<div class="group-heading">IN-BRANCH CONFIGURE SITE</div>						
						<div class="input-block">
							<h3 class="property-title">Saving Completed Application (Full)</h3>
							<p>How should the loan file should be saved as when all loan applicants on a full loan application have submitted their applications?</p>
							<div style="margin-top: -10px;">
								<div class="radio radio-success radio-md" style="display: inline-block; margin-right: 20px;">
									<input type="radio" id="radSaveFullAppAsLead" name="radSaveFullAppAs" <%=IIf(SaveFullAppAs.ToUpper() = "LEAD", "checked='checked'", "")%> value="lead"/>
									<label for="radSaveFullAppAsLead"> Lead</label>
								</div>
								<div class="radio radio-success radio-md" style="display: inline-block">
									<input type="radio" id="radSaveFullAppAsLoan" name="radSaveFullAppAs" <%=IIf(SaveFullAppAs.ToUpper() = "LOAN", "checked='checked'", "")%> value="loan"/>
									<label for="radSaveFullAppAsLoan"> Loan</label>
								</div>
							</div>
						</div>
					</div>
					<div class="bottom30">
						<div class="group-heading">WORKFLOWS</div>
						<div class="input-block">
							<h3 class="property-title">Create New Applications</h3>
							<p>This feature allows any applicant to create new applications. If turned off, applicants will only be able to access applications created for them in-branch.</p>
							<label class="toggle-btn" data-on="ON" data-off="OFF">
								<input type="checkbox" id="chkAllowComsumerToCreateApps" value="" <%=BindCheckbox(AllowComsumerToCreateApps)%>/>
								<span class="button-checkbox"></span>
							</label>
						</div>
						<div class="input-block">
							<h3 class="property-title">Present Quick Pricer in Application</h3>
							<p>This feature enables the Quick Pricer to be displayed within the application to display any eligible programs.</p>
							<label class="toggle-btn" data-on="ON" data-off="OFF">
								<input type="checkbox" id="chkShowPricerAtEnd" value="" <%=BindCheckbox(ShowPricerAtEnd)%>/>
								<span class="button-checkbox"></span>
							</label>
						</div>
						<div class="input-block">
							<h3 class="property-title">Credential</h3>
							<p>User</p>
							<input type="text" class="form-control" id="txtLQBUser" value="<%=LQBUser%>" />
							<br/>
							<p>Password</p>
							<input type="password" class="form-control" data-format="password" id="txtLQBPassword" value="<%=LQBPassword%>" />
							<br/>
							<p>Confirm Password</p>
							<input type="password" data-format="confirmPassword" data-password-not-match="Confirm password is not match." class="form-control" id="txtConfirmLQBPassword" value="<%=LQBPassword%>" />
						</div>
					</div>
					<div class="bottom30">
						<div class="group-heading">INTEGRATIONS</div>
						<div class="input-block">
							<h3 class="property-title">Site Analytics</h3>
							<p>This feature enables site analytics for your Application Portal.</p>
							<p>MeridianLink Analytics ID</p>
							<input type="text" class="form-control" id="txtPiwikSiteId" value="<%=PiwikSiteId%>" />
							<br/>
							<p>Google Tag Manager ID</p>
							<input type="text" class="form-control" id="txtGoogleTagManagerId" value="<%=GoogleTagManagerId%>" />
						</div>
						<%If ApmShowInstaTouchSwitchButton Then%>
						<div class="input-block">
							<h3 class="property-title">InstaTouch</h3>
							<p>This feature allows applicants to prefill their application using Equifax InstaTouch.</p>
							<label class="toggle-btn" data-on="ON" data-off="OFF">
								<input type="checkbox" id="chkInstaTouchEnabled" value="" <%=BindCheckbox(InstaTouchEnabled)%>/>
								<span class="button-checkbox"></span>
							</label>
						</div>
						<%End If%>
						<div class="input-block">
							<h3 class="property-title">Verification of Assets</h3>
							<p>This feature allows applicants to prefill their assets in the application using VOA.</p>
							<label class="toggle-btn" data-on="ON" data-off="OFF">
								<input type="checkbox" id="chkVOAPrefillEnabled" value="" <%=BindCheckbox(VOAPrefillEnabled)%>/>
								<span class="button-checkbox"></span>
							</label>
						</div>
						<div class="input-block">
							<h3 class="property-title">Credit Pull</h3>
							<p>This feature controls whether applicant submissions will automatically have credit pulled.</p>
							<label class="toggle-btn" data-on="ON" data-off="OFF">
								<input type="checkbox" id="chkCreditPull" value="" <%=BindCheckbox(CreditPull)%>/>
								<span class="button-checkbox"></span>
							</label>
						</div>
					</div>
				</div>
			</div>
		</section>
	</div>
</asp:Content>
<asp:Content runat="server" ContentPlaceHolderID="plScripts">
	<script type="text/javascript">
		$(function () {
			registerDataValidator("#plloans", "ValidateData");
			$("select,input:checkbox").on("change", function () {
				master.FACTORY.documentChanged(this);
			});
			
			$("input:text").each(function (idx, ele) {
				$(ele).on({
					keypress: function () { master.FACTORY.documentChanged(ele); },
					paste: function () { master.FACTORY.documentChanged(ele); },
					cut: function () { master.FACTORY.documentChanged(ele); }
				});
			});
		});
		(function (master, $, undefined) {
			master.FACTORY.SaveDraft = function (revisionId) {
				if ($.smValidate("ValidateData") == false) {
					_COMMON.noty("error", "Error: please check all fields.", 500);
					return;
				}

				var dataObj = collectSubmitData();
				dataObj.revisionId = revisionId;
				dataObj.command = 'savelqbsettings';
				$.ajax({
					url: "/sm/smhandler.aspx",
					async: true,
					cache: false,
					type: 'POST',
					dataType: 'html',
					data: dataObj,
					success: function (responseText) {
						var response = $.parseJSON(responseText);
						if (response.IsSuccess) {
							master.FACTORY.onSaveDraftSuccess();
						} else {
							_COMMON.noty("error", "Error", 500);
						}
					}
				});
			};
			master.FACTORY.Preview = function (revisionId) {
				if ($.smValidate("ValidateData") == false) {
					_COMMON.noty("error", "Error: please check all fields.", 500);
					return;
				}
				var dataObj = collectSubmitData();
				dataObj.revisionId = revisionId;
				dataObj.command = 'previewlqbsettings';
				$.ajax({
					url: "/sm/smhandler.aspx",
					async: true,
					cache: false,
					type: 'POST',
					dataType: 'html',
					data: dataObj,
					success: function (responseText) {
						var response = $.parseJSON(responseText);
						if (response.IsSuccess) {
							master.FACTORY.onPreviewSuccess();
							_COMMON.openInNewTab(response.Info.previewurl);
						} else {
							_COMMON.noty("error", "Error", 500);
						}
					}
				});
			};
			master.FACTORY.validateBeforePublish = function () {
				var result = $.smValidate("ValidateData");
				if (result == false) {
					_COMMON.noty("error", "Error: please check all fields.", 500);
				}
				return result;
			}
			master.FACTORY.Publish = function (comment, publishAll) {
				if ($.smValidate("ValidateData") == false) {
					_COMMON.noty("error", "Error: please check all fields.", 500);
					return;
				}
				var dataObj = collectSubmitData();
				dataObj.command = 'publishlqbsettings';
				dataObj.comment = comment;
				dataObj.publishAll = publishAll;
				$.ajax({
					url: "/sm/smhandler.aspx",
					async: true,
					cache: false,
					type: 'POST',
					dataType: 'html',
					data: dataObj,
					success: function (responseText) {
						var response = $.parseJSON(responseText);
						if (response.IsSuccess) {
							master.FACTORY.onPublishSuccess();
						} else {
							_COMMON.noty("error", "Error", 500);
						}
					}
				});
			};
		}(window.master = window.master || {}, jQuery));
		function registerDataValidator(container, groupName) {
			var $container = $(container);
			$.smValidate.removeValidationGroup(groupName);
			$("[data-required='true'],[data-format]", $container).each(function (idx, ele) {
				$(ele).observer({
					validators: [
						function (partial) {
							var $self = $(this);
							if ($self.data("required") && $.trim($self.val()) === "") {
								if ($self.data("message-require")) {
									return $self.data("message-require");
								} else {
									return "This field is required";
								}
							}
							var format = $self.data("format");
							if (format) {
								var msg = "Invalid format";
								if ($self.data("message-invalid-data")) {
									msg = $self.data("message-invalid-data");
								}
								if (format === "email") {
									if (_COMMON.ValidateEmail($self.val()) === false) {
										return msg;
									}
								} else if (format === "phone") {
									if (_COMMON.ValidatePhone($self.val()) === false) {
										return msg;
									}
								} else if (format === "confirmPassword") {
									var pwd = $("#txtLQBPassword").val();
									if ($self.val() !== pwd) {
										return $self.data("password-not-match");
									}
								} else if ($self.val() != "" && format === "positive-number") {
									if ((parseInt($self.val()) > 0) == false) {
										return msg;
									}
								} else if ($self.val() != "" && format === "guid") {
									if (!/^[0-9a-f]{8}-[0-9a-f]{4}-[1-5][0-9a-f]{3}-[89ab][0-9a-f]{3}-[0-9a-f]{12}$/i.test($self.val())) {
										return msg;
									}
								}

							}

							return "";
						}
					],
					validateOnBlur: true,
					group: groupName
				});
			});
		}
		function collectSubmitData() {
			var result = {};
			result.lenderconfigid = '<%=LenderConfigID%>';
			result.defaultLoanTemplate = $("#txtDefaultLoanTemplate").val();
			result.defaultLoanOfficer = $("#txtDefaultLoanOfficer").val();
			result.creditReportingAgency = $("#txtCreditReportingAgency").val();
			result.saveFullAppAs = $("input:radio[name='radSaveFullAppAs']:checked").val();
			result.allowComsumerToCreateApps = $("#chkAllowComsumerToCreateApps").is(":checked");
			result.showPricerAtEnd = $("#chkShowPricerAtEnd").is(":checked");
			result.lqbUser = $("#txtLQBUser").val();
			result.lqbPassword = $("#txtLQBPassword").val();
			result.piwikSiteId = $("#txtPiwikSiteId").val();
			result.googleTagManagerId = $("#txtGoogleTagManagerId").val();
			<%If ApmShowInstaTouchSwitchButton Then%>
			result.instaTouchEnabled = $("#chkInstaTouchEnabled").is(":checked");
			<%End If%>
			result.voaPrefillEnabled = $("#chkVOAPrefillEnabled").is(":checked");
			result.creditPull = $("#chkCreditPull").is(":checked");
			return result;
		}
	</script>
</asp:Content>
<asp:Content runat="server" ContentPlaceHolderID="plActionButtons"></asp:Content>