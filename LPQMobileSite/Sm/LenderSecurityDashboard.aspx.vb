﻿Imports Newtonsoft.Json
Imports LPQMobile.Utils

Partial Class Sm_LenderSecurityDashboard
	Inherits SmBasePage
	Protected Property BackOfficeLenderID As Guid
	Protected Property LenderID As Guid
	Protected Property BackOfficeLender As SmBackOfficeLender
	Protected Property LenderAdminAllowed As Boolean = False
	Protected Property CanManageUsers As Boolean = True
	Private countriesAllowed As List(Of String)
	Protected Property ExcludedIPs As List(Of SmGlobalConfigIPItem)
	Protected Property SubmissionRateLimiterIPs As List(Of SmGlobalConfigIPItem)
	Protected Property InstaTouchRestrictionIPs As List(Of SmGlobalConfigIPItem)
	Protected Property ExcludedIPMessage As String
	Protected Property MaxNumberOfAppBeforeNotification As Integer
	Protected Property MaxNumberOfAppBeforeBlocking As Integer
	Protected Property InternalEmailAddresses As String

	Protected Property InstaTouchMaxTimesBeforeNotification As Integer
	Protected Property InstaTouchMaxTimeBeforeBlocking As Integer
	Protected Property InstaTouchInternalEmailAddresses As String
	Protected Property RevisionList As Dictionary(Of String, Integer)

	Protected Sub Page_PreLoad(sender As Object, e As EventArgs) Handles Me.PreLoad
		Dim smMasterPage = CType(Me.Master, Sm_MasterPage)
		smMasterPage.SelectedMainMenuItem = "lender"
		BackOfficeLenderID = Common.SafeGUID(Request.Params("id"))
		If BackOfficeLenderID = Guid.Empty Then
			Response.Redirect("/Sm/ManageLenders.aspx", True)
		End If
		Dim smBL As New SmBL()
		BackOfficeLender = smBL.GetBackOfficeLenderByID(BackOfficeLenderID)
		If BackOfficeLender Is Nothing Then
			Response.Redirect("/Sm/ManageLenders.aspx", True)
		End If
		LenderID = BackOfficeLender.LenderID
		AllowedRoles.Add(SmSettings.Role.SuperUser.ToString())
		AllowedRoles.Add(String.Format("LenderScope${0}${1}", SmSettings.Role.LenderAdmin.ToString(), LenderID.ToString()))
		NotAllowedAccessRedirectUrl = "/Sm/ManageLenders.aspx"
		smMasterPage.LenderInfo = "(<span id='spLenderName'>" & BackOfficeLender.LenderName & "</span>)"

		LenderAdminAllowed = SmUtil.CheckRoleAccess(HttpContext.Current, SmSettings.Role.SuperUser.ToString(), String.Format("LenderScope${0}${1}", SmSettings.Role.LenderAdmin.ToString(), LenderID.ToString()))
		If UserInfo.SSOAccessRights IsNot Nothing Then
			CanManageUsers = UserInfo.SSOAccessRights.CanManageUsers
		End If

		Dim configNames As New List(Of String)
		configNames.Add(GlobalConfigDictionary.GENERAL_COUNTRIES_ALLOWED_ACCESS)
		configNames.Add(GlobalConfigDictionary.GENERAL_EXCLUDED_IP)
		configNames.Add(GlobalConfigDictionary.GENERAL_INSTATOUCH)
		configNames.Add(GlobalConfigDictionary.GENERAL_SUBMISSION)
		Dim configData = smBL.ReadGlobalConfigItems(configNames, LenderID)
		RevisionList = New Dictionary(Of String, Integer)


		RevisionList.Add(GlobalConfigDictionary.GENERAL_COUNTRIES_ALLOWED_ACCESS, 0)
		If configData IsNot Nothing AndAlso configData.ContainsKey(GlobalConfigDictionary.GENERAL_COUNTRIES_ALLOWED_ACCESS) Then
			countriesAllowed = JsonConvert.DeserializeObject(Of List(Of String))(configData(GlobalConfigDictionary.GENERAL_COUNTRIES_ALLOWED_ACCESS).ConfigJson)
			RevisionList(GlobalConfigDictionary.GENERAL_COUNTRIES_ALLOWED_ACCESS) = configData(GlobalConfigDictionary.GENERAL_COUNTRIES_ALLOWED_ACCESS).RevisionID
		Else
			countriesAllowed = Common.CountryList.Where(Function(p) p.Value.Status = 1).Select(Function(c) c.Value.Code2).ToList()	'default values
		End If

		ExcludedIPs = New List(Of SmGlobalConfigIPItem)	'default values
		ExcludedIPMessage = ""
		RevisionList.Add(GlobalConfigDictionary.GENERAL_EXCLUDED_IP, 0)
		If configData IsNot Nothing AndAlso configData.ContainsKey(GlobalConfigDictionary.GENERAL_EXCLUDED_IP) Then
			Dim obj = New With {.ExcludedIPMessage = "", .ExcludedIPs = New List(Of SmGlobalConfigIPItem)}
			obj = JsonConvert.DeserializeAnonymousType(configData(GlobalConfigDictionary.GENERAL_EXCLUDED_IP).ConfigJson, obj)
			If obj IsNot Nothing Then
				ExcludedIPs = obj.ExcludedIPs
				ExcludedIPMessage = obj.ExcludedIPMessage
			End If
			RevisionList(GlobalConfigDictionary.GENERAL_EXCLUDED_IP) = configData(GlobalConfigDictionary.GENERAL_EXCLUDED_IP).RevisionID
		End If

		InstaTouchInternalEmailAddresses = ""
		InstaTouchMaxTimeBeforeBlocking = -1
		InstaTouchMaxTimesBeforeNotification = -1
		InstaTouchRestrictionIPs = New List(Of SmGlobalConfigIPItem)
		RevisionList.Add(GlobalConfigDictionary.GENERAL_INSTATOUCH, 0)
		If configData IsNot Nothing AndAlso configData.ContainsKey(GlobalConfigDictionary.GENERAL_INSTATOUCH) Then
			Dim obj = New With {
				.InstaTouchInternalEmailAddresses = "",
				.InstaTouchRestrictionIPs = New List(Of SmGlobalConfigIPItem),
				.InstaTouchMaxTimesBeforeNotification = -1,
				.InstaTouchMaxTimesBeforeBlocking = -1
			}
			obj = JsonConvert.DeserializeAnonymousType(configData(GlobalConfigDictionary.GENERAL_INSTATOUCH).ConfigJson, obj)
			If obj IsNot Nothing Then
				InstaTouchInternalEmailAddresses = obj.InstaTouchInternalEmailAddresses
				InstaTouchMaxTimeBeforeBlocking = obj.InstaTouchMaxTimesBeforeBlocking
				InstaTouchMaxTimesBeforeNotification = obj.InstaTouchMaxTimesBeforeNotification
				InstaTouchRestrictionIPs = obj.InstaTouchRestrictionIPs
			End If
			RevisionList(GlobalConfigDictionary.GENERAL_INSTATOUCH) = configData(GlobalConfigDictionary.GENERAL_INSTATOUCH).RevisionID
		End If


		InternalEmailAddresses = ""
		MaxNumberOfAppBeforeNotification = -1
		If IsInEnvironment("LIVE") Then
			MaxNumberOfAppBeforeBlocking = 25
		Else
			MaxNumberOfAppBeforeBlocking = 250
		End If
		SubmissionRateLimiterIPs = New List(Of SmGlobalConfigIPItem)
		RevisionList.Add(GlobalConfigDictionary.GENERAL_SUBMISSION, 0)
		If configData IsNot Nothing AndAlso configData.ContainsKey(GlobalConfigDictionary.GENERAL_SUBMISSION) Then
			Dim obj = New With {
				.InternalEmailAddresses = "",
				.SubmissionRateLimiterIPs = New List(Of SmGlobalConfigIPItem),
				.MaxAppBeforeNotification = -1,
				.MaxAppBeforeBlocking = -1
			}
			obj = JsonConvert.DeserializeAnonymousType(configData(GlobalConfigDictionary.GENERAL_SUBMISSION).ConfigJson, obj)
			If obj IsNot Nothing Then
				InternalEmailAddresses = obj.InternalEmailAddresses
				MaxNumberOfAppBeforeNotification = obj.MaxAppBeforeNotification
				MaxNumberOfAppBeforeBlocking = obj.MaxAppBeforeBlocking
				SubmissionRateLimiterIPs = obj.SubmissionRateLimiterIPs
			End If
			RevisionList(GlobalConfigDictionary.GENERAL_SUBMISSION) = configData(GlobalConfigDictionary.GENERAL_SUBMISSION).RevisionID
		End If


		'mock data
		'countriesAllowed = New List(Of String) From {"VN", "US", "DZ"}
		'ExcludedIPs = New List(Of KeyValuePair(Of String, String)) From {New KeyValuePair(Of String, String)("12.12.34.2", "dfss dsd dsd sd")}
		'ExcludedIPs.Add(New KeyValuePair(Of String, String)("12.12.34.2", "dfss dsd dsd sd"))
		'ExcludedIPs.Add(New KeyValuePair(Of String, String)("12.12.34.2", "dfss dsd dsd sd"))
		'ExcludedIPs.Add(New KeyValuePair(Of String, String)("12.12.34.2", "dfss dsd dsd sd"))
		'ExcludedIPs.Add(New KeyValuePair(Of String, String)("12.12.34.2", "dfss dsd dsd sd"))
		'ExcludedIPs.Add(New KeyValuePair(Of String, String)("12.12.34.2", "dfss dsd dsd sd"))
		'ExcludedIPs.Add(New KeyValuePair(Of String, String)("12.12.34.2", "dfss dsd dsd sd"))

	End Sub

	Protected Function BindCountriesAllowed(code As String) As String
		If countriesAllowed IsNot Nothing AndAlso countriesAllowed.Contains(code) Then Return "checked=""checked"""
		Return ""
	End Function
End Class
