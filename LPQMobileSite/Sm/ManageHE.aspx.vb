﻿Imports System.Xml
Imports DownloadedSettings
Imports Sm
Partial Class Sm_ManageHE
	Inherits SmApmBasePage
	Const LOANTYPE As String = "HOME_EQUITY_LOAN"
	Protected Property ApprovalMessage As String
	Protected Property ReferredMessage As String
    Protected Property DeclinedMessage As String
	Protected Property PreQualifiedMessage As String
	Protected Property DemographicDescription As String
	Protected Property DefaultDemographicDescription As String = LPQMobile.Utils.Common.DEFAULT_DEMOGRAPHIC_DESCRIPTION_MESSAGE
	Protected Property DemographicDisclosure As String
	Protected Property DefaultDemographicDisclosure As String = LPQMobile.Utils.Common.DEFAULT_DEMOGRAPHIC_DISCLOSURE_MESSAGE
	Protected Property DisclosureList As New List(Of String)

	Protected Property DisclosureLocList As New List(Of String)

	Protected Property EnabledPurposeList As New List(Of String)(System.StringComparison.OrdinalIgnoreCase)

	Protected Property PurposeList As New Dictionary(Of String, SmTextValuePair)(System.StringComparison.OrdinalIgnoreCase)
	Protected Property ReasonList As New List(Of String)
	Protected Property PropertyList As New List(Of String)

	Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
		If Not Page.IsPostBack Then
			Dim smMasterPage = CType(Me.Master, Sm_SiteManager)
			smMasterPage.LenderConfigXml = LenderConfigXml
			smMasterPage.RevisionID = LenderConfig.RevisionID
			smMasterPage.LastModifiedBy = LenderConfig.LastModifiedByUserFullName
			smMasterPage.LastModifiedByUserID = LenderConfig.LastModifiedByUserID
			smMasterPage.SelectedMainMenuItem = "manage"
			smMasterPage.SelectedSubMenuItem = "he"
			smMasterPage.BreadCrumb = "Manage / Home Equity Loan"
			smMasterPage.CurrentNodeGroup = SmSettings.NodeGroup.ManageHE

			Dim preapprovedMessageNode As XmlNode = GetNode(PREAPPROVED_MESSAGE(LOANTYPE))
			If preapprovedMessageNode IsNot Nothing AndAlso preapprovedMessageNode.NodeType <> XmlNodeType.Comment Then
				ApprovalMessage = SmUtil.MergeMessageKey(preapprovedMessageNode.Attributes("message").InnerText, preapprovedMessageNode)
			End If

			Dim submittedMessageNode As XmlNode = GetNode(SUBMITTED_MESSAGE(LOANTYPE))
			If submittedMessageNode IsNot Nothing AndAlso submittedMessageNode.NodeType <> XmlNodeType.Comment Then
				ReferredMessage = SmUtil.MergeMessageKey(submittedMessageNode.Attributes("message").InnerText, submittedMessageNode)
			End If

			Dim declinedMessageNode As XmlNode = GetNode(DECLINED_MESSAGE(LOANTYPE))
			If declinedMessageNode IsNot Nothing AndAlso declinedMessageNode.NodeType <> XmlNodeType.Comment Then
				DeclinedMessage = SmUtil.MergeMessageKey(declinedMessageNode.Attributes("message").InnerText, declinedMessageNode)
			End If
            Dim preQualifiedMessageNode As XmlNode = GetNode(PREQUALIFIED_MESSAGE(LOANTYPE))
			If preQualifiedMessageNode IsNot Nothing AndAlso preQualifiedMessageNode.NodeType <> XmlNodeType.Comment Then
				PreQualifiedMessage = SmUtil.MergeMessageKey(preQualifiedMessageNode.Attributes("message").InnerText, preQualifiedMessageNode)
			End If

			Dim demographicDescriptionNode As XmlNode = GetNode(DEMOGRAPHIC_DESCRIPTION_MESSAGE(LOANTYPE))
			DemographicDescription = LPQMobile.Utils.Common.GetDemographicDescriptionMessage(demographicDescriptionNode)

			Dim demographicDisclosureNode As XmlNode = GetNode(DEMOGRAPHIC_DISCLOSURE_MESSAGE(LOANTYPE))
			DemographicDisclosure = LPQMobile.Utils.Common.GetDemographicDisclosureMessage(demographicDisclosureNode)

			Dim disclosuresNode As XmlNode = GetNode(DISCLOSURES(LOANTYPE))
			If disclosuresNode IsNot Nothing AndAlso disclosuresNode.HasChildNodes Then
				For Each node As XmlNode In disclosuresNode.ChildNodes
					If node.NodeType = XmlNodeType.Comment Then Continue For
					DisclosureList.Add(node.InnerText.Trim())
				Next
			End If

			Dim disclosuresLocNode As XmlNode = GetNode(DISCLOSURES_LOC(LOANTYPE))
			If disclosuresLocNode IsNot Nothing AndAlso disclosuresLocNode.HasChildNodes Then
				For Each node As XmlNode In disclosuresLocNode.ChildNodes
					If node.NodeType = XmlNodeType.Comment Then Continue For
					DisclosureLocList.Add(node.InnerText.Trim())
				Next
			End If

			''*******
			''Purpose
			Dim downloadHEPurposes As List(Of DownloadedSettings.CListItem)
			downloadHEPurposes = CDownloadedSettings.HomeEquityPurposes(WebsiteConfig)
			'<OUTPUT><RESPONSE><FIELD_LIST name="Loan Purposes"><LIST_ITEM text="HELOC Fixed Rate" value="HELOC Fixed Rate" /><LIST_ITEM text="Vacation Home" value="Vacation Home" /><LIST_ITEM text="Home Equity Line of Credit" value="Home Equity Line of Credit" /><LIST_ITEM text="Closed End Home Equity Loan" value="Closed End Home Equity Loan" /><LIST_ITEM text="Fixed Rate" value="Fixed Rate" /><LIST_ITEM text="Home Equity Line of Credit" value="Home Equity Line of Credit" /></FIELD_LIST></RESPONSE></OUTPUT>
			If downloadHEPurposes IsNot Nothing AndAlso downloadHEPurposes.Any() Then
                For Each item As CListItem In downloadHEPurposes
                    If item.Value Is Nothing OrElse item.Value = "" Then Continue For
                    If PurposeList.ContainsKey(item.Value) Then Continue For
                    PurposeList.Add(item.Value, New SmTextValuePair() With {.Text = item.Text, .Value = item.Value})
                Next
            End If
			Dim purposesNode As XmlNode = GetNode(HOME_EQUITY_LOAN_PURPOSES())
			If purposesNode IsNot Nothing AndAlso purposesNode.HasChildNodes Then
				For Each node As XmlNode In purposesNode.ChildNodes
					If node.NodeType = XmlNodeType.Comment Then Continue For
					Dim value As String = node.Attributes("value").Value
					If Not EnabledPurposeList.Contains(value) Then
						EnabledPurposeList.Add(value)
					End If
				Next
			End If

			Dim reasonsNode As XmlNode = GetNode(HOME_EQUITY_LOAN_REASONS())
			If reasonsNode IsNot Nothing AndAlso reasonsNode.HasChildNodes Then
				For Each node As XmlNode In reasonsNode.ChildNodes
					If node.NodeType = XmlNodeType.Comment Then Continue For
					ReasonList.Add(node.Attributes("value").InnerText)
				Next
			End If

			Dim propertiesNode As XmlNode = GetNode(PROPERTY_TYPES)
			If propertiesNode IsNot Nothing AndAlso propertiesNode.HasChildNodes Then
				For Each node As XmlNode In propertiesNode.ChildNodes
					If node.NodeType = XmlNodeType.Comment Then Continue For
					PropertyList.Add(node.Attributes("value").InnerText)
				Next
			End If
		End If
	End Sub

End Class
