﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="Index.aspx.vb" Inherits="Sm_Index" MasterPageFile="SiteManager.master" %>

<asp:Content runat="server" ContentPlaceHolderID="plBody">
	<div class="homepage">
		<div class="top-section">
			<div>
				<%--<img src="/Sm/content/images/profile_ico.png"/>--%>
				<img src="/Sm/content/images/logo.png"/>
				<h1>Welcome to the Application Portal Manager</h1>
				<p>Manage the design and configuration of your Application Portal (aka Mobile Optimized Website)</p>
			</div>
		</div>
		<div class="main-section-wrapper">	
			<div class="main-section">

                <%--temporary hide this bc we will have a new design for this section--%>
				<div class="section-title" style="display:none;">
					<h3 class="wizard-ico">Quick links</h3>
					<%--<p>Looking to get in and get out? Here are some of our most popular actions.</p>--%>
				</div>

                <%--temporary hide this bc we will have a new design for this section--%>
				<div class="quick-link-container row" style="display:none;">
					<a href="<%=GetPageUrl("RenameEditor.aspx")%>">
						<p class="edit-ico">Edit Text</p>
					</a>
					<a href="<%=GetPageUrl("ShowFieldController.aspx")%>">
						<p class="eyeball-ico">Edit Visibility</p>
					</a>
					<a href="<%=GetPageUrl("DefaultManage.aspx")%>">
						<p class="megaphone-ico">Edit Disclosures</p>
					</a>
					<a href="<%=GetPageUrl("ThemeEditor.aspx")%>">
						<p class="paintroller-ico">Edit Theme</p>
					</a>
					<%--<a href="<%=GetPageUrl("DefaultManage.aspx")%>">
						<p>Manage - Disclosures</p>
						<p>Manage and update your disclosures for your Application Portal</p>
					</a>
					<a href="<%=GetPageUrl("ThemeEditor.aspx")%>">
						<p>Design - Theme</p>
						<p>Change the logo and theme colors for your Application Portal</p>
					</a>--%>
				</div>
				<div class="faq-container row">
					<div class="col-sm-4">
						<h3 class="video-ico">Videos</h3>
						<a href="https://cms.assistdirect.com/pages/viewpage.action?pageId=40534693#ApplicationPortal/ApplicationPortalManagerVideos-AP/APMIntroandDemo%E2%80%9314:14" target="_help">Application Portal and Application Portal Manager Intro and Demo</a>
						<a href="https://cms.assistdirect.com/pages/viewpage.action?pageId=40534693#ApplicationPortal/ApplicationPortalManagerVideos-APMThemeConfiguration%E2%80%9307:09" target="_help">Theme Configuration</a>
						<a href="https://cms.assistdirect.com/pages/viewpage.action?pageId=40534693#ApplicationPortal/ApplicationPortalManagerVideos-APMMessages%E2%80%9306:06" target="_help">Status and Disclosure Messages</a>
						<a href="https://cms.assistdirect.com/pages/viewpage.action?pageId=40534693#ApplicationPortal/ApplicationPortalManagerVideos-APMComboApp%E2%80%9304:41" target="_help">Combo Applications</a>
						<a href="https://cms.assistdirect.com/pages/viewpage.action?pageId=40534693#ApplicationPortal/ApplicationPortalManagerVideos-APMBusinessRulesandCustomQuestions%E2%80%9308:47" target="_help">Business Rules and Custom Questions</a>
						<a href="https://cms.assistdirect.com/pages/viewpage.action?pageId=40534693#ApplicationPortal/ApplicationPortalManagerVideos-APMConfiguringLendingVisibilityandOtherSettings%E2%80%9310:38" target="_help">Lending Settings</a>
						<a href="https://cms.assistdirect.com/pages/viewpage.action?pageId=40534693#ApplicationPortal/ApplicationPortalManagerVideos-APMConfiguringXpressAccountsVisibilityandOtherSettings%E2%80%9309:03" target="_help">XpressAccounts Settings</a>
					</div>
					<div class="col-sm-4">
						<h3 class="howto-ico">How to</h3>
						<a href="https://cms.assistdirect.com/x/nYFqAg" target="_blank">Save and Publish Changes</a>
						<a href="https://cms.assistdirect.com/x/pIFqAg" target="_blank">Edit Themes</a>
						<a href="https://cms.assistdirect.com/x/qIFqAg" target="_blank">Customize Text</a>
						<a href="https://cms.assistdirect.com/x/r4FqAg" target="_blank">Control Visibility</a>
						<a href="https://cms.assistdirect.com/x/yIFqAg" target="_blank">Configure Products</a>
					</div>
					<div class="col-sm-4">
						<h3 class="info-ico">Info</h3>
						<a href="https://cms.assistdirect.com/x/lIFqAg" target="_blank">Application Portal Manager Administrator's Manual</a>
						<a href="https://cms.assistdirect.com/x/bZ66" target="_blank">Application Portal Information</a>
					</div>
				</div>
				<%--<div class="section-title">
					<h3>Questions</h3>
					<p>Here are some frequently asked questions.</p>
				</div>
				<div class="faq-container">
					<div>
						<label class="question-line">
							<span>What is the Application Portal Manager (APM)?</span>
							<input type="checkbox"/>
							<span></span>
						</label>
						<p>
							The Application Portal Manager (APM) is a tool for LoansPQ admins to manage and configure the consumer facing Application Portal (AP)). This tool allows the admin to configure branding themes, message templates and disclosures, fields and functionality settings, and much more. To review the user manual, please click here: <a href='https://cms.assistdirect.com/display/KB/Application+Portal+Manager+%28APM%29+Admin+Manual' target='_blank'>Application Portal Manager User Guide</a>
						</p>
					</div>
					<div>
						<label class="question-line">
							<span>How do I save my changes and publish my changes to my live sites?</span>
							<input type="checkbox"/>
							<span></span>
						</label>
						<p>
							To save your changes in the Application Portal Manager, simply click <b>Save Draft</b> at the top right of the page. You will also receive a message reminding you to save the changes you've made prior to leaving the page. To publish the saved changes, click <b>Publish</b>.
						</p>
					</div>
					<div>
						<label class="question-line">
							<span>What if the changes I want to make are not listed in the Application Portal Manager?</span>
							<input type="checkbox"/>
							<span></span>
						</label>
						<p>
							Unavailable options, such as Custom Questions, will be available in future releases. For changes to the Application Portal that are not listed in the Application Portal Manager, please consult with your Mobile Implementation Consultant or our LoansPQ Support Team. Please note that not all change requests will be fullfilled as the Application Portal contains hard coded elements.
						</p>
					</div>
				</div>--%>
			</div>
		</div>
	</div>
	
	
	
<%--	
	<div class="homepage">
		<h1 class="page-header">Dashboard</h1>
		<p>This will be updated to show the default homepage, same for every one.</p>
		<div class="row">
			<div class="col-12 quick-access-panel">
				<%If SmUtil.CheckRoleAccess(HttpContext.Current, SmSettings.Role.LegacyOperator, SmSettings.Role.SuperOperator) Then%>
				<a href="/migrateConfig/editConfig.aspx" target="_blank">
					<img src="/Sm/content/images/duplicate_ico_g.png"/>
					<p>Legacy</p>
				</a>
				<%End If%>
				<a href="/sm/ManageLenders.aspx">
					<img src="/Sm/content/images/lenderlist_ico_g.png"/>
					<p>Lenders</p>
				</a>
				<%If SmUtil.CheckRoleAccess(HttpContext.Current, SmSettings.Role.SuperOperator) Then%>
				<a href="/sm/so/ManageUsers.aspx">
					<img src="/Sm/content/images/user_ico_g.png"/>
					<p>Users</p>
				</a>
				<%End If %>
				<a href="/sm/so/ManageLogs.aspx">
					<img src="/Sm/content/images/history_ico_g.png"/>
					<p>History</p>
				</a>
			</div>
		</div>
	</div>--%>
</asp:Content>
<asp:Content runat="server" ContentPlaceHolderID="plScripts">
	<script type="text/javascript">
		$(function() {
			$("label.question-line>input:checkbox", ".homepage").each(function(idx, ele) {
				var $self = $(ele);
				$self.on("change", function() {
					if ($(this).is(":checked")) {
						$self.closest("label.question-line").addClass("collapsed-question");
					} else {
						$self.closest("label.question-line").removeClass("collapsed-question");
					}
				});
			});
		});
	</script>
</asp:Content>