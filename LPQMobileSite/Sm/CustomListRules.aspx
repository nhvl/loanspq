﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="CustomListRules.aspx.vb" Inherits="Sm_CustomListRules" MasterPageFile="SiteManager.master" %>
<%@ Import Namespace="Newtonsoft.Json" %>


<asp:Content runat="server" ContentPlaceHolderID="plBody">
	<div id="divCustomListRulesPage">
		<h1 class="page-header">Custom List Rules</h1>
		<p>Build rules for custom list</p>
		<section>
            <%-- For other devs: Do not remove below line. --%>
            <%="" %>
			<ul class="nav nav-tabs" id="mainTabs" role="tablist">
				<%If CurrentModule = SmSettings.ModuleName.XA Then%>
				<li role="presentation" class="active"><a href="#" aria-controls="tabCLR" role="tab">XA</a></li>
				<%Else %>
				<li role="presentation" <%=IIf(EnableXA, "", "class='hidden'")%>><a href="<%=BuildUrl("/sm/customlistrules.aspx", New KeyValuePair(Of String, String)("clrtab", "xa"))%>" role="tab">XA</a></li>
				<%End If%>
				<%If CurrentModule = SmSettings.ModuleName.BL Then%>
				<li role="presentation" class="active"><a href="#" aria-controls="tabCLR" role="tab">BL</a></li>
				<%Else %>
				<li role="presentation" <%=IIf(EnableBL, "", "class='hidden'")%>><a href="<%=BuildUrl("/sm/customlistrules.aspx", New KeyValuePair(Of String, String)("clrtab", "bl"))%>" role="tab">BL</a></li>
				<%End If%>
				<%If CurrentModule = SmSettings.ModuleName.CC Then%>
				<li role="presentation" class="active"><a href="#" aria-controls="tabCLR" role="tab">CC</a></li>
				<%Else %>
				<li role="presentation" <%=IIf(EnableCC, "", "class='hidden'")%>><a href="<%=BuildUrl("/sm/customlistrules.aspx", New KeyValuePair(Of String, String)("clrtab", "cc"))%>" role="tab">CC</a></li>
				<%End If%>
				<%If CurrentModule = SmSettings.ModuleName.HE Then%>
				<li role="presentation" class="active"><a href="#" aria-controls="tabCLR" role="tab">HE</a></li>
				<%Else %>
				<li role="presentation" <%=IIf(EnableHE, "", "class='hidden'")%>><a href="<%=BuildUrl("/sm/customlistrules.aspx", New KeyValuePair(Of String, String)("clrtab", "he"))%>" role="tab">HE</a></li>
				<%End If%>
				<%If CurrentModule = SmSettings.ModuleName.PL Then%>
				<li role="presentation" class="active"><a href="#" aria-controls="tabCLR" role="tab">PL</a></li>
				<%Else %>
				<li role="presentation" <%=IIf(EnablePL, "", "class='hidden'")%>><a href="<%=BuildUrl("/sm/customlistrules.aspx", New KeyValuePair(Of String, String)("clrtab", "pl"))%>" role="tab">PL</a></li>
				<%End If%>
				<%If CurrentModule = SmSettings.ModuleName.VL Then%>
				<li role="presentation" class="active"><a href="#" aria-controls="tabCLR" role="tab">VL</a></li>
				<%Else %>
				<li role="presentation" <%=IIf(EnableVL, "", "class='hidden'")%>><a href="<%=BuildUrl("/sm/customlistrules.aspx", New KeyValuePair(Of String, String)("clrtab", "vl"))%>" role="tab">VL</a></li>
				<%End If%>
			</ul>
			<div class="tab-content">
				<div role="tabpanel" class="tab-pane active" id="tabCLR">
					<h2 class="section-title"><%=TabTitle%></h2>
					<p>Click "Add List" to view the available custom list features.</p>
					<div class="text-right">
						<button type="button" data-command="add-item" onclick="master.FACTORY.showPickTemplatePopup()" class="btn btn-primary mb10"><i class="fa fa-plus" aria-hidden="true"></i>Add List</button>
					</div>
						<hr/>
					<div class="no-data">
						<p>No lists have been defined.</p>
					</div>
					<div class="custom-list-rules-wrapper">
					</div>
				</div>
			</div>
		</section>
	</div>
</asp:Content>
<asp:Content runat="server" ContentPlaceHolderID="plScripts">
	<script type="text/javascript" src="js/querybuilder/query-builder.standalone.js"></script>
	<script type="text/javascript">
		$(function () {
			$("input:checkbox", "#divCustomListRulesPage").on("change", function () {
				master.FACTORY.documentChanged(this);
			});
			<%If not string.isnullorempty(CustomListRulesJson) then%>
			master.DATA.CustomListRules = <%=CustomListRulesJson%>;
			master.FACTORY.bindingCustomListRules();
			<%End If%>
			$("input[id$='_DialogTitle'], textarea[id$='_DialogMessage']").on("change", function () {
				master.FACTORY.documentChanged(this);
			});
		});
		(function (master, $, undefined) {
			var defaultRule = {
				condition: 'AND',
				rules: [{ empty: true }]
			};
			master.DATA.TemplateList = [
				/*{
					Name: "Default",
					Code: "DEFAULT_LIST",
					Description: "Default list with two columns TEXT, VALUE. All filters is available.",
					Enable: false
				},*/
				{
					Name: "Applicant Block Rules",
					Code: "APPLICANT_BLOCK_RULE",
					Description: "<div>Define the rules that will prevent an applicant from submitting an application.</div><div>Each rule consists of one or more conditions and descriptive text that is displayed to the consumer when the conditions are met.</div>",
					Enable: true,
					FilterIDs: null,
					Columns: [
						{
							DisplayText: "Rule",
							Name: "Name",
							Maxlength: 60,
							Type: "string"
						}
					]
				},
				{
					Name: "Document Titles",
					Code: "DOCUMENT_TITLES",
					Description: "Create a list of the document types you want the consumer to upload for an application.",
					Enable: true,
					//FilterIDs: ["#age", "#citizens"],
					FilterIDs: [],
					Columns: [
						{
							Name: "Title",
							DisplayText: "Document Title",
							Type: "string",
							Maxlength: 100
						}, {
							Name: "Code",
							DisplayText: "Doc Code",
							Type: "string",
							Maxlength: 60,
							IsRequired: false
						}, {
							Name: "Group",
							DisplayText: "Doc Group",
							Type: "string",
							Maxlength: 60,
							IsRequired: false
						}
					]/*,
					List: [
						{
							title: "--Please Select Title--",
							doc_group: "",
							doc_code: "",
							Enable: true
						},{
							title: "driver license",
							doc_group: "idcard",
							doc_code: "345",
							Enable: true
						},{
							title: "pay stud loan",
							doc_group: "income",
							doc_code: "12313",
							Enable: true
						}
					]*/	
				}/*,
				{
					Name: "Additional Card Names",
					Code: "CREDIT_CARD_NAMES",
					Description: "This is to configure the list of credit card names",
					Enable: true,
					FilterIDs: [],
					Columns: [
						{
							Name: "credit_card_name",
							DisplayText: "Card Name",
							Type: "string"
						}, {
							Name: "credit_card_type",
							DisplayText: "Card Type",
							Type: "string"
						}, {
							Name: "category",
							DisplayText: "Category",
							Type: "string"
						}, {
							Name: "image_url",
							DisplayText: "Image Url",
							Type: "string"
						}
					]
				}*/
			];
			<%--we don't run booking for BL so hide BOOKING_VALIDATION--%>
			<%If Not String.IsNullOrEmpty(WebsiteConfig.Booking) AndAlso CurrentTab <> "BL" Then%>
			master.DATA.TemplateList.push({
				Name: "Booking Validation Rules",
				Code: "BOOKING_VALIDATION",
				Description: "Define the rules that will prevent an instant-approved application from being automatically booked. An application that is stopped by a validation rule will have its status changed to referred.",
				Enable: true,
				Columns: [
					{
						Name: "Name",
						DisplayText: "Rule",
						Maxlength: 200,
						Type: "string"
					}
				],
				HiddenColumns:[
				{
					Name: "ErrorCode",
					DisplayText: "Error Code",
					Type: "string"
				}]
			});
			<%End If%>
			var operatorDic = {
				"dropdown": ['equal', 'not_equal'],
				"checkbox": ['equal_array', 'not_equal_array', 'is_a_subset_of', 'is_a_superset_of', 'is_not_a_subset_of', 'is_not_a_superset_of', 'intersects', 'does_not_intersect'],
				"numeric": ['equal', 'not_equal', 'greater', 'greater_or_equal', 'less', 'less_or_equal', 'between', 'not_between'],
				"date": ['equal', 'not_equal', 'greater', 'greater_or_equal', 'less', 'less_or_equal', 'is_defined', 'is_not_defined', 'is_exactly_X_days_before_today', 'is_older_than_X_days_before_today', 'is_within_X_days_before_today'],
				"string": ['equal', 'not_equal', 'contains', 'not_contains', 'begins_with', 'not_begins_with', 'ends_with', 'not_ends_with', 'is_defined', 'is_not_defined']
			}
			var filters = [];
			var generalFilters = [
				{
					id: '#age',
					label: 'Age',
					type: 'integer',
					validation: {
						min: 0,
						max: 100
					},
					operators: operatorDic['numeric'],
					optgroup: "general"
				},
				{
					id: '#citizenship',
					label: 'Citizenship',
					type: 'string',
					input: 'select',
					values: <%=CitizenshipJson%>,
					operators: operatorDic['dropdown'],
					optgroup: "general"
				},
				{
					id: '#employeeOfLender',
					label: 'Employee of Lender',
					type: 'string',
					input: 'select',
					values: <%=EmployeeOfLenderJson%>,
					operators: operatorDic['dropdown'],
					optgroup: "general"
				},
				{
					id: '#employmentLength',
					label: 'Employment Length (years)',
					type: 'double',
					validation: {
						min: 0,
						max: 100
					},
					operators: operatorDic['numeric'],
					optgroup: "general"
				},
				{
					id: '#employmentStatus',
					label: 'Employment Status',
					type: 'string',
					input: 'select',
					values: {
						"ACTIVE MILITARY": "Active Military",
						"EMPLOYED": "Employed",
						"GOVERNMENT/DOD": "Government/DoD",
						"HOMEMAKER": "Homemaker",
						"OTHER": "Other",
						"OWNER": "Owner",
						"RETIRED": "Retired",
						"RETIRED MILITARY": "Retired Military",
						"SELF EMPLOYED": "Self Employed",
						"STUDENT": "Student",
						"UNEMPLOYED": "Unemployed"
					},
					operators: operatorDic['dropdown'],
					optgroup: "general"
				},
				{
					id: '#isJoint',
					label: 'Is Joint?',
					type: 'boolean',
					input: 'select',
					values: {
						"true": "Yes",
						"false": "No"
					},
					operators: operatorDic['dropdown'],
					optgroup: "general"
				},
				{
					id: '#locationPool',
					label: 'Location Pool',
					type: 'string',
					input: 'checkbox',
					values: <%=LocationPoolJson%>,
					operators: operatorDic['checkbox'],
					optgroup: "general"
				},
				{
					id: '#memberNumber',
					label: 'Member Number',
					type: 'string',
					operators: operatorDic['string'],
					optgroup: "general"
				},
				{
					id: '#occupancyLength',
					label: 'Occupancy Length (years)',
					type: 'double',
					validation: {
						min: 0,
						max: 100
					},
					operators: operatorDic['numeric'],
					optgroup: "general"
				},
				{
					id: '#occupancyStatus',
					label: 'Occupancy Status',
					type: 'string',
					input: 'select',
					values: <%=OccupancyStatusJson%>,
					operators: operatorDic['dropdown'],
					optgroup: "general"
				}

			];

			<%If CurrentTab = "BL" Then%>
			filters = _.concat(generalFilters, [
					{
						id: '#amountRequested',
						label: 'Amount Requested',
						type: 'double',
						operators: operatorDic['numeric'],
						optgroup: "loans"
					},
					{
						id: '#businessType',
						label: 'Business Type',
						type: 'string',
						input: 'checkbox',
						//multiple: true,
						values: <%=BLBusinessAccountTypesJson%>,
						operators: operatorDic['checkbox'],
						optgroup: "loans"
					},
					{
						id: '#creditCardPurpose',
						label: 'Credit Card Purposes',
						type: 'string',
						input: 'checkbox',
						//multiple: true,
						values: <%=CreditCardPurposesJson%>,
						operators: operatorDic['checkbox'],
						optgroup: "loans"
					},
					{
						id: '#cardType',
						label: 'Card Type',
						type: 'string',
						input: 'checkbox',
						//multiple: true,
						values: <%=CreditCardTypesJson%>,
						operators: operatorDic['checkbox'],
						optgroup: "loans"
					},
					{
						id: '#otherLoanPurpose',
						label: 'Other Loan Purposes',
						type: 'string',
						input: 'checkbox',
						//multiple: true,
						values: <%=OtherLoanPurposesJson%>,
						operators: operatorDic['checkbox'],
						optgroup: "loans"
					},
					{
						id: '#vehicleLoanPurpose',
						label: 'Vehicle Loan Purposes',
						type: 'string',
						input: 'checkbox',
						//multiple: true,
						values: <%=VehicleLoanPurposesJson%>,
						operators: operatorDic['checkbox'],
						optgroup: "loans"
					}
			]);
			<%ElseIf CurrentTab = "CC" Then%>
			filters = _.concat(generalFilters, [
					{
						id: '#amountRequested',
						label: 'Amount Requested',
						type: 'double',
						operators: operatorDic['numeric'],
						optgroup: "loans"
					},
					{
						id: '#creditCardPurpose',
						label: 'Credit Card Purpose',
						type: 'string',
						input: 'checkbox',
						//multiple: true,
						values: <%=CreditCardPurposesJson%>,
						operators: operatorDic['checkbox'],
						optgroup: "loans"
					},
					{
						id: '#creditCardType',
						label: 'Card Type',
						type: 'string',
						input: 'checkbox',
						//multiple: true,
						values: <%=CreditCardTypesJson%>,
						operators: operatorDic['checkbox'],
						optgroup: "loans"
					}
			]);
			<%ElseIf CurrentTab = "HE" Then%>
			filters = _.concat(generalFilters, [
					{
						id: '#amountRequested',
						label: 'Amount Requested',
						type: 'double',
						operators: operatorDic['numeric'],
						optgroup: "loans"
					},
					{
						id: '#isLineOfCredit',
						label: 'Is Line of Credit?',
						type: 'boolean',
						input: 'select',
						values: {
							"true": "Yes",
							"false": "No"
						},
						operators: operatorDic['dropdown'],
						optgroup: "loans"
					},
					{
						id: '#homeEquityProperty',
						label: 'Home Equity Properties',
						type: 'string',
						input: 'checkbox',
						//multiple: true,
						values: <%=HomeEquityPropertiesJson%>,
						operators: operatorDic['checkbox'],
						optgroup: "loans"
					},
					{
						id: '#homeEquityPurpose',
						label: 'Home Equity Purposes',
						type: 'string',
						input: 'checkbox',
						//multiple: true,
						values: <%=HomeEquityPurposesJson%>,
						operators: operatorDic['checkbox'],
						optgroup: "loans"
					},
					{
						id: '#homeEquityReason',
						label: 'Home Equity Reasons',
						type: 'string',
						input: 'checkbox',
						//multiple: true,
						values: <%=HomeEquityReasonsJson%>,
						operators: operatorDic['checkbox'],
						optgroup: "loans"
					},
					{
						id: '#occupyStatus',
						label: 'Occupy Status',
						type: 'string',
						input: 'checkbox',
						//multiple: true,
						values: {
							"PRIMARY RESIDENCE": "Primary Residence",
							"SECOND HOME": "Second Home",
							"RENTAL/INVESTMENT": "Rental/Investment"
						},
						operators: operatorDic['checkbox'],
						optgroup: "loans"
					},
					{
						id: '#propertyState',
						label: 'Property State',
						type: 'string',
						input: 'checkbox',
						//multiple: true,
						values: <%=StatesJson%>,
						operators: operatorDic['checkbox'],
						optgroup: "loans"
					}
			]);
			<%ElseIf CurrentTab = "PL" Then%>
			filters = _.concat(generalFilters, [
					{
						id: '#amountRequested',
						label: 'Amount Requested',
						type: 'double',
						operators: operatorDic['numeric'],
						optgroup: "loans"
					},
					{
						id: '#isLineOfCredit',
						label: 'Is Line of Credit?',
						type: 'boolean',
						input: 'select',
						values: {
							"true": "Yes",
							"false": "No"
						},
						operators: operatorDic['dropdown'],
						optgroup: "loans"
					},
					{
						id: '#loanPurposeLoc',
						label: 'Personal Loan Purposes (LOC)',
						type: 'string',
						input: 'checkbox',
						//multiple: true,
						values: <%=PersonalLineOfCreditsJson%>,
						operators: operatorDic['checkbox'],
						optgroup: "loans"
					},
					{
						id: '#loanPurposeNonLoc',
						label: 'Personal Loan Purposes (non-LOC)',
						type: 'string',
						input: 'checkbox',
						//multiple: true,
						values: <%=PersonalLoanPurposesJson%>,
						operators: operatorDic['checkbox'],
						optgroup: "loans"
					}
			]);
			<%ElseIf CurrentTab = "VL" Then%>
			filters = _.concat(generalFilters, [
					{
						id: '#amountRequested',
						label: 'Amount Requested',
						type: 'double',
						operators: operatorDic['numeric'],
						optgroup: "loans"
					},
					{
						id: '#vehicleLoanPurpose',
						label: 'Vehicle Loan Purposes',
						type: 'string',
						input: 'checkbox',
						//multiple: true,
						values: <%=VehicleLoanPurposesJson%>,
						operators: operatorDic['checkbox'],
						optgroup: "loans"
					},
					{
						id: '#vehicleType',
						label: 'Vehicle Type',
						type: 'string',
						input: 'checkbox',
						//multiple: true,
						values: <%=VehicleTypesJson%>,
						operators: operatorDic['checkbox'],
						optgroup: "loans"
					}
			]);
			<%ElseIf CurrentTab = "XA" Then%>
			filters = _.concat(generalFilters, [
				{
					id: '#accountPosition',
					label: 'Account Position',
					type: 'string',
					input: 'select',
					values: {
						"1": "Primary",
						"2": "Secondary"
					},
					operators: operatorDic['dropdown'],
					optgroup: "xa"
				},
				{
					id: '#accountType',
					label: 'Account Type',
					type: 'string',
					input: 'select',
					values: {
						"b": "Business",
						" ": "Personal",
						"s": "Special"
					},
					/*validation: {
						allow_empty_value: true
					},*/
					operators: operatorDic['dropdown'],
					optgroup: "xa"
				},
				{
					id: '#businessAccountType',
					label: 'Business Account Type',
					type: 'string',
					input: 'checkbox',
					//multiple: true,
					values: <%=XABusinessAccountTypesJson%>,
					operators: operatorDic['checkbox'],
					optgroup: "xa"
				},
				{
					id: '#productCode',
					label: 'Product Code',
					type: 'string',
					input: 'checkbox',
					//multiple: true,
					values: <%=ProductCodesJson%>,
					operators: operatorDic['checkbox'],
					optgroup: "xa"
				},
				{
					id: '#specialAccountType',
					label: 'Special Account Type',
					type: 'string',
					input: 'checkbox',
					//multiple: true,
					values: <%=SpecialAccountTypesJson%>,
					operators: operatorDic['checkbox'],
					optgroup: "xa"
				}
			]);
			<%Else%>
			$("button.btn[data-command='add-item']", "#tabCLR").remove();
			$("#tabCLR .no-data").addClass("hidden");
			<%End If%>
			
			<%If ApplicationCQFilterItems IsNot Nothing AndAlso ApplicationCQFilterItems.Count > 0 Then
			For Each item In ApplicationCQFilterItems
			%>
			filters.push({
				id: "<%=HttpUtility.JavaScriptStringEncode(item.id)%>",
				label: "<%=HttpUtility.JavaScriptStringEncode(item.label)%>",
				type: '<%=item.type%>',
				input: '<%=item.input%>',
				//multiple: <%=item.multiple%>,
				values: <%=JsonConvert.SerializeObject(item.values)%>,
				operators: operatorDic['<%=item.operators%>'],
				optgroup: "customquestions"
				});
			<%
	Next
End If%>
			<%If ApplicantCQFilterItems IsNot Nothing AndAlso ApplicantCQFilterItems.Count > 0 Then
			For Each item In ApplicantCQFilterItems
			%>
			filters.push({
				id: "<%=HttpUtility.JavaScriptStringEncode(item.id)%>",
				label: "<%=HttpUtility.JavaScriptStringEncode(item.label)%>",
				type: '<%=item.type%>',
				input: '<%=item.input%>',
				//multiple: <%=item.multiple%>,
				values: <%=JsonConvert.SerializeObject(item.values)%>,
				operators: operatorDic['<%=item.operators%>'],
				optgroup: "applicantquestions"
			});
			<%
	Next
End If%>
			<%If ProductQuestionsFilterItems IsNot Nothing AndAlso ProductQuestionsFilterItems.Count > 0 Then
			For Each item In ProductQuestionsFilterItems
			%>
			filters.push({
				id: "<%=HttpUtility.JavaScriptStringEncode(item.id)%>",
				label: "<%=HttpUtility.JavaScriptStringEncode(item.label)%>",
				type: '<%=item.type%>',
				//multiple: <%=item.multiple%>,
				<%If item.type = "date"%>
				validation: {
					format: 'MM/DD/YYYY'
				},
				<%Else%>
				input: '<%=item.input%>',
				<%End If%>
				values: <%=JsonConvert.SerializeObject(item.values)%>,
				operators: operatorDic['<%=item.operators%>'],
				optgroup: "products"
			});
			<%
	Next
End If%>
			master.FACTORY.showPickTemplatePopup = function() {
				var htmlContent = "";
				_.forEach(master.DATA.TemplateList, function(t) {
					var addBtnState = "";
					if (_.find(master.DATA.CustomListRules, { Code: t.Code })) {
						addBtnState = 'disabled="disabled"';
					}

					htmlContent += '<div class="row">';
					htmlContent += '	<div class="col-10">';
					htmlContent += '		<p style="font-weight: bold; margin-bottom: 0;">' + t.Name + '</p>';
					htmlContent += '		<div>' + t.Description + '</div>';
					if (t.List && t.List.length > 0) {
						htmlContent += '	<a href="javascript:;" class="clr-toggle-list-btn">Items to Add</a>';
						htmlContent += '	<table>';
						htmlContent += '	<thead>';
						_.forEach(t.Columns, function(c) {
							htmlContent += '	<th>' + (c.DisplayText || c.Name) + '</th>';
						});
						htmlContent += '	</thead>';
						htmlContent += '	<tbody>';
						_.forEach(t.List, function(lstItem) {
							htmlContent += '	<tr>';
							_.forEach(t.Columns, function(attr) {
								htmlContent += '	<td>' + lstItem[attr.Name] + '</td>';
							});
							htmlContent += '	</tr>';
						});
						htmlContent += '	</tbody>';
						htmlContent += '	</table>';
					}
					htmlContent += '	</div>';
					htmlContent += '	<div class="col-2">';
					htmlContent += '		<button class="btn btn-primary" ' + addBtnState + ' onclick="master.FACTORY.pickTemplate(\'' + t.Code + '\')">Add</button>';
					htmlContent += '	</div>';
					htmlContent += '</div>';
				});
				htmlContent = '<div style="padding: 20px; overflow: auto; max-height: 500px;" class="clr-template-list">' + htmlContent + '</div>';
				_COMMON.showDialogClose("Available Lists", 0, "pick_template_dialog_0", htmlContent, "", null, function (container) {
					//ready
				}, function () {
					//onclose
				}, BootstrapDialog.SIZE_WIDE, false);
			}
			master.FACTORY.pickTemplate = function(code) {
				var template = _.find(master.DATA.TemplateList, { Code: code });
				if (template) {
					//showAddItemPopup(template);
					var data = {};
					data.ID = "clr_" + Math.random().toString(36).substr(2, 16);
					data.Name = template.Name;
					data.Code = template.Code;
					data.Description = template.Description;
					data.Enable = true;
					if (template.Code == "APPLICANT_BLOCK_RULE") {
						data.DialogTitle = "We have encountered an issue";
						data.DialogMessage = "Thank you for considering us for your financial services needs. We are sorry that we cannot continue with your application for the following reasons:";	
					}
					data.Columns = template.Columns;
					data.HiddenColumns = template.HiddenColumns;
					data.FilterIDs = template.FilterIDs;
					if (!data.Columns) {
						data.Columns = [{ Name: "Text", DisplayText: "Text", Type: "string" }, { Name: "Value", DisplayText: "Value", Type: "string" }];
					}
					data.List = _.map(template.List, function(item) {
						item.Rule = null;
						item.Expression = null;
						item.Expression2 = null;
						item.ID = "clr_lst_item" + Math.random().toString(36).substr(2, 16);
						return item;
					});
					addItem(data);
					$("#pick_template_dialog_0").closest(".modal").modal("hide");
					$('html, body').stop().animate({ scrollTop: $("#" + data.ID).offset().top - 100 }, '500', 'swing');
				}
			}
			/*function showAddItemPopup(template) {
				var htmlContent = "";
				htmlContent += '<div class="row">';
				htmlContent += '	<div class="col-12">';
				htmlContent += '		<div class="form-group">';
				htmlContent += '			<label class="required-field">Custom List Name</label>';
				htmlContent += '			<input type="text" class="form-control" value="' + template.Name + '" data-field="txtCustomListName" data-format="pattern" data-pattern="^[a-zA-Z0-9_\\s]*$" data-pattern-msg="Allowed characters: letters A-Z, 0-9, dashes, spaces." data-required="true" id="txtCustomListName"/>';
				htmlContent += '		</div>';
				htmlContent += '	</div>';
				htmlContent += '</div>';
				htmlContent += '<div class="row">';
				htmlContent += '	<div class="col-12">';
				htmlContent += '		<div class="form-group">';
				htmlContent += '			<label class="required-field">Custom List Code</label>';
				htmlContent += '			<input type="text" class="form-control" value="' + template.Code + '" data-field="txtCustomListCode" data-format="pattern" data-pattern="^[a-zA-Z0-9_]*$" data-pattern-msg="Allowed characters: letters A-Z, 0-9, dashes." data-required="true" id="txtCustomListCode"/>';
				htmlContent += '		</div>';
				htmlContent += '	</div>';
				htmlContent += '</div>';
				htmlContent += '<div class="row">';
				htmlContent += '	<div class="col-12">';
				htmlContent += '		<div class="form-group">';
				htmlContent += '			<label>Description</label>';
				htmlContent += '			<textarea class="form-control" rows="5" data-field="txtCustomListDescription" id="txtCustomListDescription">' + template.Description + '</textarea>';
				htmlContent += '		</div>';
				htmlContent += '	</div>';
				htmlContent += '</div>';
				htmlContent += '<div class="row">';
				htmlContent += '	<div class="col-12">';
				htmlContent += '		<div class="form-group">';
				htmlContent += '			<label>Is Active</label>';
				htmlContent += '			<div><label class="toggle-btn" data-on="ON" data-off="OFF"><input type="checkbox" value="" id="chkCustomListEnabled" ' + (template.Enable == true ? 'checked="checked"' : '') + ' /><span class="button-checkbox"></span></label></div>';
				htmlContent += '		</div>';
				htmlContent += '	</div>';
				htmlContent += '</div>';
				htmlContent += '<div class="row"><div class="col-12"></div></div>';
				htmlContent = '<div style="padding: 20px; overflow: auto; max-height: 500px;">' + htmlContent + '</div>';
				_COMMON.showDialogSaveClose("Add Item", 0, "additem_dialog_0", htmlContent, "", null, function (dlg) {
					//on save
					if (!$.smValidate("additem_dialog_0")) return;
					var data = {};
					data.ID = "clr_" + Math.random().toString(36).substr(2, 16);
					data.Name = $("#txtCustomListName", "#additem_dialog_0").val();
					data.Code = $("#txtCustomListCode", "#additem_dialog_0").val();
					data.Description = $("#txtCustomListDescription", "#additem_dialog_0").val();
					data.Columns = template.Columns;
					data.FilterIDs = template.FilterIDs;
					if (!data.Columns) {
						data.Columns = [{ Name: "Text", DisplayText: "Text", Type: "string" }, { Name: "Value", DisplayText: "Value", Type: "string" }];
					}
					data.List = _.map(template.List, function(item) {
						item.Rule = null;
						item.Expression = null;
						item.Expression2 = null;
						item.ID = "clr_lst_item" + Math.random().toString(36).substr(2, 16);
						return item;
					});
					data.Enable = $("#chkCustomListEnabled", "#additem_dialog_0").is(":checked");
					addItem(data);
					var $container = dlg.container;
					dlg.dialog.close();
				}, function (container) {
					$("#txtCustomListName").on("blur", function() {
						if ($("#txtCustomListCode").val() == "" && $.smValidate.validate("additem_dialog_0", this)) {
							$("#txtCustomListCode").val($.trim($(this).val()).replace(/\s+/g, "_").toUpperCase());
						}
					});
					$("#txtCustomListCode").on("blur", function() {
						if ($.smValidate.validate("additem_dialog_0", this)) {
							$(this).val($.trim($(this).val()).replace(/\s+/g, "_").toUpperCase());
						}
					});
					registerDataValidator($("#additem_dialog_0"), "additem_dialog_0");
				}, function () {
					//onclose
					$.smValidate.cleanGroup("additem_dialog_0");
				}, BootstrapDialog.SIZE_NORMAL, false);
			};*/
			function lookupFieldName(id, paramInfo) {
				var ret = _.find(filters, { id: id });
				if (typeof ret != "undefined" && ret != null) {
					return "<span>" + ret.label + "</span>";
				} else if(paramInfo != null && paramInfo.length > 0) {

					var re = /^%(\w+)%$/.exec(id);
					if (re == null) {
						re = /^#(\w+)$/.exec(id);
					}
					if (re != null) {
						var info = _.find(paramInfo, {Key: re[1]});
						if (info) {
							info.IsError = true;
							info.OriginalId = id;
							return "<span class='red'>" + info.Text + "</span>";
						}
					}
					
				}
				return "<span class='red'>" + id.replace("#", "") + "</span>";
			}
			function translateOperator(op, type, value) {
				var ret = "";
				switch(op) {
					case "equal":
						ret =  "is " + value;
						break;
					case "not_equal":
						ret = "is not " + value;
						break;
					case "greater":
						ret = (type == "date" ? "is after " : "is greater than ") + value;
						break;
					case "greater_or_equal":
						ret = (type == "date" ? "is the same or after " : "is greater than or equal to ") + value;
						break;
					case "less":
						ret = (type == "date" ? "is before " : "is less than ") + value;
						break;
					case "less_or_equal":
						ret = (type == "date" ? "is the same or before " : "is less than or equal to ") + value;
						break;
					case "between":
						ret = " is between " + value[0] + " and " + value[1];
						break;
					case "not_between":
						ret = "is not between " + value[0] + " and " + value[1];
						break;
					case "begins_with":
						ret = "starts with " + value;
						break;
					case "not_begins_with":
						ret = "does not start with " + value;
						break;
					case "contains":
						ret = "contains " + value;
						break;
					case "not_contains":
						ret = "does not contains " + value;
						break;
					case "ends_with":
						ret = "ends with " + value;
						break;
					case "not_ends_with":
						ret = "does not end with" + value;
						break;
					case "is_defined":
						ret = "is defined";
						break;
					case "is_not_defined":
						ret = "is not defined";
						break;
					case "is_exactly_X_days_before_today":
						ret = "is exactly " + value + " day" + (value > 1 ? "s": "") + " before today";
						break;
					case "is_older_than_X_days_before_today":
						ret = "is older than " + value + " day" + (value > 1 ? "s": "") + " before today";
						break;
					case "is_within_X_days_before_today":
						ret = "is within " + value + " day" + (value > 1 ? "s": "") + " before today";
						break;
					case "is_a_subset_of":
						ret = "is a subset of [" + _.join(value, ", ") + "]";
						break;
					case "is_not_a_subset_of":
						ret = "is not a subset of [" + _.join(value, ", ") + "]";
						break;
					case "is_a_superset_of":
						ret = "is a superset of [" + _.join(value, ", ") + "]";
						break;
					case "is_not_a_superset_of":
						ret = "is not a superset of [" + _.join(value, ", ") + "]";
						break;
					case "intersects":
						ret = "intersects with [" + _.join(value, ", ") + "]" ;
						break;
					case "does_not_intersect":
						ret = "does not intersect with [" + _.join(value, ", ") + "]";
						break;
					case "equal_array":
						ret = "is [" + _.join(value, ", ") + "]";
						break;
					case "not_equal_array":
						ret = "is not [" + _.join(value, ", ") + "]";
						break;
					case "is_one_of":
						ret = "is one of [" + _.join(value, ", ") + "]";
						break;
					case "is_not_one_of":
						ret = "is not one of [" + _.join(value, ", ") + "]";
						break;
					default:
						ret = op + " " + value;
						break;
				}
				return ret;
			}
			master.FACTORY.bindingCustomListRules = function() {
				if (master.DATA.CustomListRules != null && master.DATA.CustomListRules.length > 0) {
					$("#tabCLR .no-data").addClass("hidden");
					_.forEach(master.DATA.CustomListRules, function(data) {
						$("#tabCLR .custom-list-rules-wrapper").append(buildItem(data));
					});	
				}
				
			}
			master.FACTORY.changeListItemStatus = function(srcEle, id, itemId) {
				var ret = _.find(master.DATA.CustomListRules, { ID: id });
				if (typeof ret != "undefined" && ret != null && ret.List && ret.List.length > 0) {
					var item =_.find(ret.List, { ID: itemId });
					if (item) {
						item.Enable = $(srcEle).is(":checked");
						master.FACTORY.documentChanged();
					}
				}
			}
			master.FACTORY.deleteListItem = function(srcEle, id, itemId) {
				_COMMON.showConfirmDialog("Are you sure you want to delete this rule?", function() {
					//yes
					var ret = _.find(master.DATA.CustomListRules, { ID: id });
					if (typeof ret != "undefined" && ret != null && ret.List && ret.List.length > 0) {
						_.remove(ret.List, { ID: itemId });
						if (ret.List.length == 0) {
							$(srcEle).closest("tbody").append($('<tr><td colspan="' + (ret.Columns.length + 3) + '" class="text-center">The list is empty.</td></tr>'));
						}
						$(srcEle).closest("tr").remove();
						master.FACTORY.documentChanged();
					}
				}, function() {
					//no
				}, "Yes", "No");


				
			}
			/*master.FACTORY.addNewListItem = function(id) {
				
				var ret = _.find(master.DATA.CustomListRules, { ID: id });
				if (typeof ret != "undefined" && ret != null) {
					var newListItem = {};
					_.forEach(ret.Columns, function(c) {
						var $txt = $("#txtAddNew_" + id + "_" + c.Name);
						newListItem[c.Name] = _COMMON.HtmlEncode($txt.val());
						$txt.val("");
					});
					
					newListItem.ID = "clr_lst_item" + Math.random().toString(36).substr(2, 16);
					newListItem.Rule = null;
					newListItem.Expression = null;
					newListItem.Expression2 = null;
					newListItem.Enable = $("#chkAddNew_" + id + "_IsActive").is(":checked");

					ret.List = ret.List || [];
					if (ret.List.length == 0) {
						$('#plhListItems_' + id).html("");
					}
					ret.List.push(newListItem);
					$('#plhListItems_' + id).append(buildListItem(id, ret.Columns, newListItem, ret.FilterIDs));
					master.FACTORY.documentChanged();
				}
			}*/
			
			function addItem(data) {
				master.DATA.CustomListRules = master.DATA.CustomListRules || [];
				if (master.DATA.CustomListRules.length == 0) {
					$("#tabCLR .no-data").addClass("hidden");
				}
				master.DATA.CustomListRules.push(data);
				$("#tabCLR .custom-list-rules-wrapper").append(buildItem(data));
				master.FACTORY.documentChanged();
				$.smValidate.cleanGroup("customlistvalidator");
				registerDataValidator($("#tabCLR"), "customlistvalidator");
			}
			/*function updateItem(data) {
				var ret = _.find(master.DATA.CustomListRules, { ID: data.ID });
				if (typeof ret != "undefined" && ret != null) {
					ret = $.extend(ret, data);
					$("#" + data.ID, "#tabCLR").replaceWith(buildItem(ret));
					master.FACTORY.documentChanged();
				}
			}*/
			function updateListItem(srcEle, id, wItem) {
				var ret = _.find(master.DATA.CustomListRules, { ID: id });
				if (typeof ret != "undefined" && ret != null) {
					var item =_.find(ret.List, { ID: wItem.ID });
					item = $.extend(item, wItem);
					if (srcEle) {
						$(srcEle).closest("tr").replaceWith(buildListItem(id, ret.Columns, item, ret.FilterIDs));

					} else {
						$('#plhListItems_' + id).append(buildListItem(id, ret.Columns, wItem, ret.FilterIDs));
					}
					master.FACTORY.documentChanged();
				}
			}
			master.FACTORY.deleteItem = function(id) {
				_COMMON.showConfirmDialog("Are you sure you want to delete this list?", function() {
					//yes
					_.remove(master.DATA.CustomListRules, function(p) {
						return p.ID == id;
					});
					master.FACTORY.documentChanged();
					if (master.DATA.CustomListRules.length == 0) {
						$("#tabCLR .no-data").removeClass("hidden");
					}
					$("#" + id, "#tabCLR").remove();	
					$.smValidate.cleanGroup("customlistvalidator");
					registerDataValidator($("#tabCLR"), "customlistvalidator");
				}, function() {
					//no
				}, "Yes", "No");
			}
			master.FACTORY.changeItemStatus = function(srcEle, id) {
				var ret = _.find(master.DATA.CustomListRules, { ID: id });
				if (typeof ret != "undefined" && ret != null) {
					ret.Enable = $(srcEle).is(":checked");
					master.FACTORY.documentChanged();
				}
			}
			function buildItem(data) {
				var html = '';
				html += '<div class="custom-list-rule-item" id="' + data.ID + '">';
				html += '	<div>';
				html += '		<span class="list-name">' + data.Name + '</span>';
				html += '		<div class="custom-list-rule-btn">';
				//html += '			<div onclick="master.FACTORY.showEditItemDialog(\'' + data.ID + '\')">Edit</div>';
				html += '			<div class="link-btn" onclick="master.FACTORY.deleteItem(\'' + data.ID + '\')">Delete</div>';
				html += '			<label class="toggle-btn" data-on="ON" data-off="OFF"><input type="checkbox" ' + (data.Enable ? 'checked="checked"' : '') + ' value="" onchange="master.FACTORY.changeItemStatus(this, \'' + data.ID + '\')"/><span class="button-checkbox"></span></label>';
				html += '		</div>';
				html += '	</div>';
				html += '	<div>' + data.Description + '</div>';
				var type = "Rule";
				if (data.Code == "APPLICANT_BLOCK_RULE") {
					html += '<br/>';
					html += '<div>';
					html += '	<div class="form-group row">';
					html += '		<label class="col-md-3 col-form-label"><span class="require-ico">Dialog Title</span>: </label>';
					html += '		<div class="col-md-9"><input type="text" maxlength="60" data-required="true" value="' + data.DialogTitle + '" placeholder="Dialog Title" class="form-control" id="txt_' + data.ID + '_DialogTitle"/></div>';
					html += '	</div>';
					html += '	<div class="form-group row">';
					html += '		<label class="col-md-3 col-form-label"><span class="require-ico">Dialog Message</span>: </label>';
					html += '		<div class="col-md-9"><textarea class="form-control" maxlength="180" data-required="true" placeholder="Dialog Message" id="txt_' + data.ID + '_DialogMessage" rows="3">' + data.DialogMessage + '</textarea></div>';
					html += '	</div>';
					html += '</div>';
				}else if (data.Code == "DOCUMENT_TITLES") {
					type = "Title";
				}

				html += '	<table border="1">';
				html += '		<thead>';
				html += '			<tr>';
				_.forEach(data.Columns, function(c) {
					html += '			<th>' + (c.DisplayText || c.Name) + '</th>';
				});
				if (!data.FilterIDs || data.FilterIDs.length > 0) {
					html += '				<th>Condition</th>';
				}
				html += '				<th style="width: 100px;">Active</th>';
				html += '				<th style="width: 100px;">Action</th>';
				html += '			</tr>';
				html += '		</thead>';
				html += '		<tbody id="plhListItems_' + data.ID + '">';
				if (data.List && data.List.length > 0) {
					_.forEach(data.List, function(item) {
						html += buildListItem(data.ID, data.Columns, item, data.FilterIDs);
					});
				} else {
					html += '		<tr>';
					html += '			<td colspan="' + (data.Columns.length + (!data.FilterIDs || data.FilterIDs.length > 0 ? 3 : 2)) + '" class="text-center">The list is empty.</td>';
					html += '		</tr>';
				}
				html += '		</tbody>';
				html += '	</table>';
				html += '	<div style="text-align: right; padding-right: 0;">';
				html += '		<div class="form-group">';
				html += '			<button class="btn btn-primary" onclick="master.FACTORY.showAddEditListItemDialog(null, \'' + data.ID + '\', null)">Add ' + type + '</button>';
				html += '		</div>';
				html += '	</div>';
				/*html += '	<div>';
				_.forEach(data.Columns, function(c) {
					html += '	<div class="form-group">';
					html += '		<label>' + (c.DisplayText || c.Name) + ': </label>';
					html += '		<input type="text" class="form-control" placeholder="' + (c.DisplayText || c.Name) + '" id="txtAddNew_' + data.ID + '_' + c.Name + '">';
					html += '	</div>';
				});
				html += '		<div class="form-group">';
				html += '			<label>Is Active:</label>';
				html += '			<div class="checkbox checkbox-success checkbox-inline" style="margin-top: 6px;padding-left: 4px;">';
				html += '				<input type="checkbox" id="chkAddNew_' + data.ID + '_IsActive" checked="checked"/>';
				html += '				<label for="chkAddNew_' + data.ID + '_IsActive"></label>';
				html += '			</div>';
				html += '		</div>';
				html += '		<div class="form-group">';
				html += '			<button class="btn btn-primary" onclick="master.FACTORY.addNewListItem(\'' + data.ID + '\')">Add</button>';
				html += '		</div>';
				html += '	</div>';*/
				html += '</div>';
				return html;
			}
			function buildListItem(id, columns, item, filterIds) {
				var html = "";
				html += '	<tr>';
				_.forEach(columns, function(c) {
					html += '	<td>' + _COMMON.HtmlEncode(item[c.Name]) + '</td>';
				});
				var showExclamation = false;
				if (!filterIds || filterIds.length > 0) {
					var logic = '';
					if (item.Rule) {
						logic = buildSummaryOfRule(item.Rule, item.ParamInfo);
						showExclamation = _.find(item.ParamInfo, { IsError: true });
					}
					html += '		<td>' + logic + '</td>';
				}
				html += '		<td>';
				html += '			<i class="' + (showExclamation ? "" : "hidden") + ' fa fa-exclamation-circle red" aria-hidden="true" style="margin-left: 34px; line-height: 1px;"></i>';
				html += '			<div class="' + (showExclamation ? "hidden" : "") + ' checkbox checkbox-success checkbox-inline" style="margin-left: 32px; line-height: 1px;">';
				html += '				<input type="checkbox" id="chk_' + id + "_" + item.ID + '" ' + (item.Enable ? 'checked="checked"' : '') + ' onchange="master.FACTORY.changeListItemStatus(this, \'' + id + '\', \'' + item.ID + '\')"/>';
				html += '				<label for="chk_' + id + "_" + item.ID + '"></label>';
				html += '			</div>';
				html += '		</td>';
				html += '		<td class="text-center">';
				html += '			<a href="javascript:;" onclick="master.FACTORY.showAddEditListItemDialog(this, \'' + id + '\', \'' + item.ID + '\')"><span>Edit<span></a>';
				html += '			<span>|</span>';
				html += '			<a href="javascript:;" onclick="master.FACTORY.deleteListItem(this, \'' + id + '\', \'' + item.ID + '\')"><span>Delete</span></a>';
				html += '		</td>';
				html += '	</tr>';
				return html;
			}
			function buildSummaryOfRule(rule, paramInfo) {
				function translate(condition, rules) {
					var x = "";
					_.forEach(rules, function(r) {
						if (typeof r.condition != "undefined") {
							x += (x == "" ? "" : "<strong class='text-uppercase'>" + condition + "</strong> ") + "(" + translate(r.condition, r.rules) + ") ";
						} else {
							var value;
							if (r.input == "select" || r.input == "radio") {
								var filter = _.find(filters, { id: r.id });
								if (filter) {
									value = filter.values[r.value];
								}
							}else if (r.input == "checkbox") {
								var filter = _.find(filters, { id: r.id });
								value = [];
								if (filter) {
									_.forEach(r.value, function (v) {
										value.push(filter.values[v]);
									});
								}
							} else {
								value = r.value;
							}
							x += (x == "" ? "" : "<strong class='text-uppercase'>" + condition + "</strong> ") + lookupFieldName(r.id, paramInfo) + " " + translateOperator(r.operator, r.type, value) + " ";
						}
					});
					return x;
				}

				return translate(rule.condition, rule.rules);

				//return '<p>Personal Loan Type is Personal or Purpose Type is Overdraft</p><p><strong>and</strong> (Applicant Total Monthly Income is less than or equal to 0 and Age is less than 18)</p>';
			}
			function initEmptyRule(code) {
				var rule = {};
				switch (code) {
					case "APPLICANT_BLOCK_RULE":
						rule = {
							Name: ""
						};
						break;
					case "BOOKING_VALIDATION":
						rule = {
							Name: "",
							ErrorCode: ""
						};
						break;
					case "DOCUMENT_TITLES":
						rule = {
							Title: "",
							Code: "",
							Group: ""
						};
						break;
					default:
						break;
				}
				return rule;
			}
			master.FACTORY.showAddEditListItemDialog = function(srcEle, id, itemId) {
				var ret = _.find(master.DATA.CustomListRules, { ID: id });
				if (typeof ret != "undefined" && ret != null) {
					var item =_.find(ret.List|| [], { ID: itemId });
					var type = "Rule";
					var htmlContent = "";
					if (ret.Code == "DOCUMENT_TITLES") {
						htmlContent += '<div class="row bottom10">';
						htmlContent += '<div class="col-12">"Document Title" is displayed to the consumer as a choice for document uploads. "Doc Code" and "Doc Group" are optional fields used for the LoansPQ Document Archiving services.</div>';
						htmlContent += '</div>';
						type = "Document Title";
					}
					var action = "Edit";
					if (!item) {
						action = "Add";
						item = initEmptyRule(ret.Code);
					}
					_.forEach(ret.Columns, function(c) {
						htmlContent += '<div class="row">';
						htmlContent += '	<div class="col-12">';
						htmlContent += '		<div class="form-group clr-form-group">';
						htmlContent += '			<label ' + (c.IsRequired == false ? "" : "class=\"require-ico\"") + '>' + (c.DisplayText || c.Name) + '</label>';
						if (c.Maxlength && c.Maxlength > 0) {
							htmlContent += '			<div>' + c.Maxlength + ' characters limit</div>';
						}
						htmlContent += '			<input type="text" id="txt' + c.Name + Math.random().toString(36).substr(2, 16) + '" data-required="' + (c.IsRequired == false ? "false" : "true") + '" '+ (c.Maxlength && c.Maxlength > 0 ? 'maxlength="' + c.Maxlength + '"' : "") +' class="form-control" value="' + _COMMON.HtmlEncode(item[c.Name]) + '" data-field="txt' + c.Name + '"/>';
						htmlContent += '		</div>';
						htmlContent += '	</div>';
						htmlContent += '</div>';
					});
					htmlContent += '<div class="row"><div class="col-12"><div id="clrBuilder"></div></div></div>';
					htmlContent = '<div style="padding: 20px; overflow: auto; max-height: 500px;">' + htmlContent + '</div>';
					_COMMON.showDialogSaveClose(action + " " + type, 0, "editlistitem_dialog_0", htmlContent, "", null, function(dlg) {
						//on save
							
						if (!$.smValidate("editlistitem_dialog_0") || ($("#clrBuilder").length > 0 && $("#clrBuilder").queryBuilder("validate") != true)) return;
						var wItem = {};
						wItem.ID = itemId || "clr_lst_item" + Math.random().toString(36).substr(2, 16);
						
						_.forEach(ret.Columns, function(c) {
							wItem[c.Name] = $("input[data-field='txt" + c.Name + "']", "#editlistitem_dialog_0").val();
						});
						_.forEach(ret.HiddenColumns, function(c) {
							wItem[c.Name] = null;
						});
						if ($("#clrBuilder").length > 0) {
							wItem.Rule = $("#clrBuilder").queryBuilder("getRules");
							wItem.Expression = $.extend({}, $("#clrBuilder").queryBuilder("getJS", "numbered(@)"), $("#clrBuilder").queryBuilder("getVB", "numbered(@)"));
							wItem.Expression2 = $.extend({}, $("#clrBuilder").queryBuilder("getJS", false), $("#clrBuilder").queryBuilder("getVB", false));
							if (wItem.Rule) {
								wItem.ParamInfo = collectParamInfoFromRule(wItem.Rule.rules);
							}
						} else {
							wItem.Rule = null;
							wItem.Expression = null;
							wItem.Expression2 = null;
						}
						if (itemId == null) {//add new
							wItem.Enable = true;
							if (ret.List.length == 0) {
								$('#plhListItems_' + id).html("");
							}
							ret.List.push(wItem);
						}
						updateListItem(srcEle, id, wItem);
						var $container = dlg.container;
						dlg.dialog.close();
					}, function(container) {
						//onready
						if (!ret.FilterIDs || ret.FilterIDs.length > 0) {
							var fFilter = _.filter(filters, function(r) { return !ret.FilterIDs || _.findIndex(ret.FilterIDs, function(s) { return r.id == s; }) > -1; });
							var rRules = item.Rule || defaultRule;
							if (fFilter && fFilter.length > 0 && _.find(item.ParamInfo, {IsError: true})) {
								_.forEach(item.ParamInfo, function(p) {
									if (p.IsError == true) {
										var missingRule = _.find(rRules.rules, {id: p.OriginalId});
										var addInFilter = {
											id: missingRule.id,
											label: p.Text,
											type: missingRule.type,
											operators: _.concat(operatorDic["string"], missingRule.operator),
											optgroup: detectOptGroup(missingRule.id),
											values: null,
											validCondition: false
										};
										fFilter.push(addInFilter);
									}
								});
							}

							$("#clrBuilder", container).queryBuilder({
								plugins: ['bt-tooltip-errors'],
								allow_groups: 1,
								optgroups: {
									general: 'General',
									loans: 'Loans',
									xa: "XA",
									customquestions: 'Custom Questions - Application',
									applicantquestions: 'Custom Questions - Applicant',
									products: 'Custom Questions - Product'
								},
								lang: {
									"add_rule": "Add condition",
									operators: {
										"equal": "is",
										"not_equal": "is not"
									}
								},
								filters: fFilter,
								rules: rRules
							});
						} else {
							$("#clrBuilder", container).remove();
						}
						registerDataValidator($("#editlistitem_dialog_0"), "editlistitem_dialog_0");
					}, function() {
						//onclose
						$.smValidate.cleanGroup("editlistitem_dialog_0");
					}, BootstrapDialog.SIZE_WIDE, true, "Save", "Cancel", false, false);
				}
			};
			function detectOptGroup(id) {
				if (/^%AQ/.test(id)) {
					return "applicantquestions";
				}else if (/^%CQ/.test(id)) {
					return "customquestions";
				}else if (/^%PQ/.test(id)) {
					return "products";
				}
				return "general";
			}
			function collectParamInfoFromRule(rule) {
				var result = [];
				if (rule && rule.length > 0) {
					_.forEach(rule, function(r) {
						if (r.condition && r.rules.length > 0) {
							result =_.concat(result, collectParamInfoFromRule(r.rules));
						} else {
							var id = r.id;
							var re = /^%([\w-_]+)%$/.exec(r.id);
							if (re == null) {
								re = /^#([\w-_]+)$/.exec(r.id);
							}
							if (re != null) {
								id = re[1];
							}
							var type = r.type;
							if (typeof r.value == "object" && Array.isArray(r.value)) {
								type = "List(of " + type + ")";
							}
							result.push({Key: id, Text: _.find(filters, {id: r.id}).label, Value: type });
						}
					});
				}
				return _.uniqWith(result, _.isEqual);
			}
			/*master.FACTORY.showEditItemDialog = function(id) {
				var ret = _.find(master.DATA.CustomListRules, { ID: id });
				if (typeof ret != "undefined" && ret != null) {
					var htmlContent = "";
					htmlContent += '<div class="row">';
					htmlContent += '	<div class="col-12">';
					htmlContent += '		<div class="form-group">';
					htmlContent += '			<label class="required-field">Custom List Name</label>';
					htmlContent += '			<input type="text" class="form-control" value="' + ret.Name + '" data-field="txtCustomListName" data-format="pattern" data-pattern="^[a-zA-Z0-9_\\s]*$" data-pattern-msg="Allowed characters: letters A-Z, 0-9, dashes, spaces." data-required="true" id="txtCustomListName"/>';
					htmlContent += '		</div>';
					htmlContent += '	</div>';
					htmlContent += '</div>';
					htmlContent += '<div class="row">';
					htmlContent += '	<div class="col-12">';
					htmlContent += '		<div class="form-group">';
					htmlContent += '			<label class="required-field">Custom List Code</label>';
					htmlContent += '			<input type="text" class="form-control" value="' + ret.Code + '" data-field="txtCustomListCode" data-format="pattern" data-pattern="^[a-zA-Z0-9_]*$" data-pattern-msg="Allowed characters: letters A-Z, 0-9, dashes." data-required="true" id="txtCustomListCode"/>';
					htmlContent += '		</div>';
					htmlContent += '	</div>';
					htmlContent += '</div>';
					htmlContent += '<div class="row">';
					htmlContent += '	<div class="col-12">';
					htmlContent += '		<div class="form-group">';
					htmlContent += '			<label>Description</label>';
					htmlContent += '			<textarea class="form-control" rows="5" data-field="txtCustomListDescription" id="txtCustomListDescription">' + ret.Description + '</textarea>';
					htmlContent += '		</div>';
					htmlContent += '	</div>';
					htmlContent += '</div>';
					htmlContent += '<div class="row">';
					htmlContent += '	<div class="col-12">';
					htmlContent += '		<div class="form-group">';
					htmlContent += '			<label>Is Active</label>';
					htmlContent += '			<div><label class="toggle-btn" data-on="ON" data-off="OFF"><input type="checkbox" value="" ' + (ret.Enable ? 'checked="checked"' : '') + '" id="chkCustomListEnabled" /><span class="button-checkbox"></span></label></div>';
					htmlContent += '		</div>';
					htmlContent += '	</div>';
					htmlContent += '</div>';
					//htmlContent += '<div class="row"><div class="col-12"></div></div>';
					htmlContent = '<div style="padding: 20px; overflow: auto; max-height: 500px;">' + htmlContent + '</div>';
					_COMMON.showDialogSaveClose("Edit Item", 0, "edititem_dialog_0", htmlContent, "", null, function(dlg) {
						//on save
						if (!$.smValidate("edititem_dialog_0")) return;
						var data = {};
						data.ID = id;
						data.Name = $("#txtCustomListName", "#edititem_dialog_0").val();
						data.Code = $("#txtCustomListCode", "#edititem_dialog_0").val();
						data.Description = $("#txtCustomListDescription", "#edititem_dialog_0").val();
						data.Enable = $("#chkCustomListEnabled", "#edititem_dialog_0").is(":checked");
						updateItem(data);
						var $container = dlg.container;
						dlg.dialog.close();
					}, function(container) {
						//onready
						$("#txtCustomListName").on("blur", function() {
							if ($("#txtCustomListCode").val() == "" && $.smValidate.validate("edititem_dialog_0", this)) {
								$("#txtCustomListCode").val($.trim($(this).val()).replace(/\s+/g, "_").toUpperCase());
							}
						});
						$("#txtCustomListCode").on("blur", function() {
							if ($.smValidate.validate("edititem_dialog_0", this)) {
								$(this).val($.trim($(this).val()).replace(/\s+/g, "_").toUpperCase());
							}
						});
						registerDataValidator($("#edititem_dialog_0"), "edititem_dialog_0");
					}, function() {
						//onclose
						$.smValidate.cleanGroup("edititem_dialog_0");
					}, BootstrapDialog.SIZE_WIDE, false);
				}
			};*/
			master.FACTORY.SaveDraft = function (revisionId) {
				if (!$.smValidate("customlistvalidator")) return;
				var dataObj = collectSubmitData();
				dataObj.command = 'saveCustomListRules';
				dataObj.revisionId = revisionId;
				$.ajax({
					url: "/sm/smhandler.aspx",
					async: true,
					cache: false,
					type: 'POST',
					dataType: 'html',
					data: dataObj,
					success: function (responseText) {
						var response = $.parseJSON(responseText);
						if (response.IsSuccess) {
							master.FACTORY.onSaveDraftSuccess();
						} else {
							_COMMON.noty("error", "Error", 500);
						}
					}
				});
			};
			master.FACTORY.Preview = function (revisionId) {
				if (!$.smValidate("customlistvalidator")) return;
				var dataObj = collectSubmitData();
				dataObj.command = 'previewCustomListRules';
				dataObj.revisionId = revisionId;
				$.ajax({
					url: "/sm/smhandler.aspx",
					async: true,
					cache: false,
					type: 'POST',
					dataType: 'html',
					data: dataObj,
					success: function (responseText) {
						var response = $.parseJSON(responseText);
						if (response.IsSuccess) {
							master.FACTORY.onPreviewSuccess();
							_COMMON.openInNewTab(response.Info.previewurl);
						} else {
							_COMMON.noty("error", "Error", 500);
						}
					}
				});
			};
			master.FACTORY.Publish = function (comment, publishAll) {
				if (!$.smValidate("customlistvalidator")) return;
				var dataObj = collectSubmitData();
				dataObj.command = 'publishCustomListRules';
				dataObj.comment = comment;
				dataObj.publishAll = publishAll;
				$.ajax({
					url: "/sm/smhandler.aspx",
					async: true,
					cache: false,
					type: 'POST',
					dataType: 'html',
					data: dataObj,
					success: function (responseText) {
						var response = $.parseJSON(responseText);
						if (response.IsSuccess) {
							master.FACTORY.onPublishSuccess();
						} else {
							_COMMON.noty("error", "Error", 500);
						}
					}
				});
			};
		}(window.master = window.master || {}, jQuery));

		function collectSubmitData() {
			var result = {};
			result.lenderconfigid = '<%=LenderConfigID%>';
			result.tab = '<%=CurrentTab%>';
			_.forEach(master.DATA.CustomListRules, function(value) {
				value.DialogTitle = $('#txt_' + value.ID + '_DialogTitle').val();
				value.DialogMessage = $('#txt_' + value.ID + '_DialogMessage').val();
			});
			result.data = JSON.stringify(master.DATA.CustomListRules);
			return result;
		}
		function registerDataValidator(container, groupName) {
			var $container = $(container);
			$.smValidate.removeValidationGroup(groupName);
			$("[data-required='true'],[data-format]", $container).each(function (idx, ele) {
				if ($(ele).is(":radio")) {
					var $placeHolder = $(ele).closest("div");
					$(ele).observer({
						validators: [
							function (partial) {
								var $self = $(this);
								if ($self.is(":visible") == false) return "";
								if ($("input[name='" + $self.attr("name") + "']:checked", $(container)).length == 0) {
									return "Please select an option";
								}
								return "";
							}
						],
						validateOnBlur: true,
						placeHolder: $placeHolder,
						group: groupName
					});
				} else {
					$(ele).observer({
						validators: [
							function (partial) {
								var $self = $(this);
								if ($self.is(":visible") == false) return "";
								if ($self.data("required") && $.trim($self.val()) === "") {
									if ($self.data("message-require")) {
										return $self.data("message-require");
									} else {
										return "This field is required";
									}
								}
								var format = $self.data("format");
								if (format) {
									var msg = "Invalid format";
									if ($self.data("message-invalid-data")) {
										msg = $self.data("message-invalid-data");
									}
									if (format === "email") {
										if (_COMMON.ValidateEmail($self.val()) === false) {
											return msg;
										}
									} else if (format === "phone") {
										if (_COMMON.ValidatePhone($self.val()) === false) {
											return msg;
										}
									}else if (format == "pattern") {
										var re = new RegExp($self.data("pattern"), "i");
										if (!re.test($self.val())) {
											return $self.data("pattern-msg");
										}
									}
								}
								return "";
							}
						],
						validateOnBlur: true,
						group: groupName
					});
				}
			});
		}
	</script>
</asp:Content>
<asp:Content runat="server" ContentPlaceHolderID="plActionButtons"></asp:Content>