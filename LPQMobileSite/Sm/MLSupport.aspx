﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="MLSupport.aspx.vb" Inherits="Sm_MLSupport" MasterPageFile="SiteManager.master" %>
<asp:Content runat="server" ContentPlaceHolderID="plBody">
	<div>
		<h1 class="page-header">Support</h1>
		<p>Access tools to troubleshoot or test applications.</p>
		<h3>Search Mortgage Applications</h3>
		<br/>
		<div class="row">
			<div class="col-sm-2 form-group">
				<label for="txtLoanNumber">Loan Number</label>
				<input type="text" class="form-control" id="txtLoanNumber" placeholder="Loan Number"/>
			</div>
		</div>
		<div class="row">
			<div class="col-sm-12">
				<button id="btnSearchLoans" data-loading-text="<i class='fa fa-circle-o-notch fa-spin'></i> Searching..." class="btn btn-primary mb10 mr5">Search</button>
			</div>
		</div>
		<br/>
		<br/>
		<div class="row grid">
			<div class="col-sm-12" id="divSearchResult"></div>
		</div>
	</div>
	</asp:Content>
<asp:Content runat="server" ContentPlaceHolderID="head">
	<style type="text/css">
		.ml-app-table a[data-command]+a {
			display: none;
		} 
	</style>
</asp:Content>
<asp:Content runat="server" ContentPlaceHolderID="plScripts">
	<script type="text/javascript">
		_COMMON_PAGE_SIZE = 10;
		$(function () {
			registerDataValidator();
			var searchResultGrid;
			searchResultGrid = new _COMMON.Grid("/sm/smhandler.aspx", "searchResultGrid", "divSearchResult", mlSupport.FACTORY, mlSupport.FACTORY.onLoaded, { command: "searchMortgageLoans" });
			searchResultGrid.init(false);
			mlSupport.FACTORY.init(searchResultGrid.GridObject);
			$("#btnSearchLoans").on("click", mlSupport.FACTORY.search);
		});
		(function (mlSupport, $, undefined) {
			mlSupport.DATA = {};
			mlSupport.FACTORY = {};
			mlSupport.FACTORY.init = function (gridInstance) {
				var self = this;
				mlSupport.DATA.grid = gridInstance;
			};
			mlSupport.FACTORY.getFilterInfo = function () {
				//implement this later
				return { Keyword: $.trim($("#txtLoanNumber").val()), LenderRef: "<%=LenderRef%>" };
			};
			mlSupport.FACTORY.onLoaded = function () {
				$("#btnSearchLoans").button("reset");
			};
			mlSupport.FACTORY.generateUrl = function (ele, loanNum, index) {
				var $self = $(ele);
				if ($self.text == "Processing...") return;
				var postData = MORTGAGE_LOAN_SEARCH_RESULT[index];
				postData.LoanNumber = loanNum;
				$self.text("Processing...");
				$.ajax({
					url: "/sm/smhandler.aspx",
					async: true,
					cache: false,
					type: 'POST',
					dataType: 'json',
					data: { command: "generateMLSearchToken", lenderRef: '<%=LenderRef%>', data: JSON.stringify(postData) },
					success: function (response) {
						if (response.IsSuccess && response.Info != "") {
							$self.next("a[data-command='open-application']").attr("href", response.Info);
							$self.closest("tr").next("tr.app-lnk-container").find(">td").text(response.Info);
							$self.remove();
						} else {
							_COMMON.noty("error", "Error", 500);
							$self.text("Generate Url");
						}
					}
				});
				
			}
			mlSupport.FACTORY.search = function () {
				if (!$.smValidate("ValidateData")) return false;
				$("#btnSearchLoans").button("loading");
				mlSupport.DATA.grid.setFilter(mlSupport.FACTORY.getFilterInfo()).setPageIndex(1).load();
			};
		}(window.mlSupport = window.mlSupport || {}, jQuery));
		function registerDataValidator() {
			$.smValidate.removeValidationGroup("ValidateData");
			$("#txtLoanNumber").observer({
				validators: [
					function (partial) {
						var $self = $(this);
						if ($.trim($self.val()) == "") {
							return "Please enter a loan number";
						}
						return "";
					}
				],
				validateOnBlur: true,
				group: "ValidateData"
			});
			
		}
	</script>
</asp:Content>
<asp:Content runat="server" ContentPlaceHolderID="plActionButtons"></asp:Content>