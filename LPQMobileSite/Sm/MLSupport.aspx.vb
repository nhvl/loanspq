﻿
Partial Class Sm_MLSupport
	Inherits SmApmBasePage

	Protected Sub Page_Load1(sender As Object, e As EventArgs) Handles Me.Load
		If Not Page.IsPostBack Then
			Dim smMasterPage = CType(Me.Master, Sm_SiteManager)
			smMasterPage.LenderConfigXml = LenderConfigXml
			smMasterPage.RevisionID = LenderConfig.RevisionID
			smMasterPage.LastModifiedBy = LenderConfig.LastModifiedByUserFullName
			smMasterPage.LastModifiedByUserID = LenderConfig.LastModifiedByUserID
			smMasterPage.SelectedMainMenuItem = "support"
			smMasterPage.SelectedSubMenuItem = "mlsupport"
			smMasterPage.CurrentNodeGroup = SmSettings.NodeGroup.MLSupport
			smMasterPage.ShowPreviewButton = False
			smMasterPage.ShowSaveDraftButton = False
			smMasterPage.ShowPublishButton = False
		End If
	End Sub
End Class
