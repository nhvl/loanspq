﻿
Imports System.Xml
Imports Sm
Partial Class Sm_ManageStandardXASecondary
	Inherits SmApmBasePage
	Const LOANTYPE As String = "XA_LOAN"

	Protected Property ApprovedSecondaryMessage As String

	Protected Property AdditionalDisclosureList As New List(Of String)
	Protected Property FundingDislosure As String

	Private _log As log4net.ILog = log4net.LogManager.GetLogger(Me.GetType)

	Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
		If Not Page.IsPostBack Then
			Dim smMasterPage = CType(Me.Master, Sm_SiteManager)
			smMasterPage.LenderConfigXml = LenderConfigXml
			smMasterPage.RevisionID = LenderConfig.RevisionID
			smMasterPage.LastModifiedBy = LenderConfig.LastModifiedByUserFullName
			smMasterPage.LastModifiedByUserID = LenderConfig.LastModifiedByUserID
			smMasterPage.SelectedMainMenuItem = "manage"
			smMasterPage.SelectedSubMenuItem = "xa_secondary"
			smMasterPage.CurrentNodeGroup = SmSettings.NodeGroup.ManageXASecondary
			smMasterPage.BreadCrumb = "Manage / Xpress Account (existing member)"

			Dim approvedSecondaryMessageNode As XmlNode = GetNode(Sm.XA_APPROVED_SECONDARY_MESSAGE())
			If approvedSecondaryMessageNode IsNot Nothing AndAlso approvedSecondaryMessageNode.NodeType <> XmlNodeType.Comment Then
				ApprovedSecondaryMessage = SmUtil.MergeMessageKey(approvedSecondaryMessageNode.Attributes("message").InnerText, approvedSecondaryMessageNode)
			End If

			Dim disclosuresSecondaryNode As XmlNode = GetNode(DISCLOSURES_SECONDARY(LOANTYPE))
			If disclosuresSecondaryNode IsNot Nothing AndAlso disclosuresSecondaryNode.HasChildNodes Then
				For Each node As XmlNode In disclosuresSecondaryNode.ChildNodes
					If node.NodeType = XmlNodeType.Comment Then Continue For
					AdditionalDisclosureList.Add(node.InnerText.Trim())
				Next
			End If
			Dim fundingDisclosureNode As XmlNode = GetNode(FUNDING_DISCLOSURE_SECONDARY(LOANTYPE))
			If fundingDisclosureNode IsNot Nothing AndAlso fundingDisclosureNode.NodeType <> XmlNodeType.Comment Then
				FundingDislosure = fundingDisclosureNode.InnerText
			End If
		End If
	End Sub
End Class
