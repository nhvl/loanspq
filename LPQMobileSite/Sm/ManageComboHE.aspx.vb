﻿
Imports System.Xml
Imports Sm
Partial Class Sm_ManageComboHE
	Inherits SmApmBasePage

	Const LOANTYPE As String = "HOME_EQUITY_LOAN"
	Protected Property ComboBothPreApprovedMsg As String
	Protected Property ComboSubmittedMsg As String
	Protected Property ComboXAPreApprovedMsg As String
	Protected Property ComboXASubmittedMsg As String
    Protected Property ComboPreQualifiedMsg As String
    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
		If Not Page.IsPostBack Then
			Dim smMasterPage = CType(Me.Master, Sm_SiteManager)
			smMasterPage.LenderConfigXml = LenderConfigXml
			smMasterPage.RevisionID = LenderConfig.RevisionID
			smMasterPage.LastModifiedBy = LenderConfig.LastModifiedByUserFullName
			smMasterPage.LastModifiedByUserID = LenderConfig.LastModifiedByUserID
			smMasterPage.SelectedMainMenuItem = "manage"
			smMasterPage.SelectedSubMenuItem = "combohe"
			smMasterPage.CurrentNodeGroup = SmSettings.NodeGroup.ManageComboHE
			smMasterPage.BreadCrumb = "Manage / Home Equity Loan + XA"
			If ApmComboEnable = False Then
				Response.Redirect(String.Format("~/Sm/Index.aspx?lenderconfigid={0}", LenderConfigID), True)
			End If
			Dim comboBothPreapprovedMessageNode As XmlNode = GetNode(COMBO_BOTH_PREAPPROVED_MESSAGE(LOANTYPE))
			If comboBothPreapprovedMessageNode IsNot Nothing AndAlso comboBothPreapprovedMessageNode.NodeType <> XmlNodeType.Comment Then
				ComboBothPreApprovedMsg = SmUtil.MergeMessageKey(comboBothPreapprovedMessageNode.Attributes("message").InnerText, comboBothPreapprovedMessageNode)
			End If


			Dim comboSubmittedMessageNode As XmlNode = GetNode(COMBO_SUBMITTED_MESSAGE(LOANTYPE))
			If comboSubmittedMessageNode IsNot Nothing AndAlso comboSubmittedMessageNode.NodeType <> XmlNodeType.Comment Then
				ComboSubmittedMsg = SmUtil.MergeMessageKey(comboSubmittedMessageNode.Attributes("message").InnerText, comboSubmittedMessageNode)
			End If

			Dim comboXAPreapprovedMessageNode As XmlNode = GetNode(COMBO_XA_PREAPPROVED_MESSAGE(LOANTYPE))
			If comboXAPreapprovedMessageNode IsNot Nothing AndAlso comboXAPreapprovedMessageNode.NodeType <> XmlNodeType.Comment Then
				ComboXAPreApprovedMsg = SmUtil.MergeMessageKey(comboXAPreapprovedMessageNode.Attributes("message").InnerText, comboXAPreapprovedMessageNode)
			End If

			Dim comboXASubmittedMessageNode As XmlNode = GetNode(COMBO_XA_SUBMITTED_MESSAGE(LOANTYPE))
            If comboXASubmittedMessageNode IsNot Nothing AndAlso comboXASubmittedMessageNode.NodeType <> XmlNodeType.Comment Then
                ComboXASubmittedMsg = SmUtil.MergeMessageKey(comboXASubmittedMessageNode.Attributes("message").InnerText, comboXASubmittedMessageNode)
            End If
            Dim comboPreQualifiedMessageNode As XmlNode = GetNode(COMBO_PREQUALIFIED_MESSAGE(LOANTYPE))
            If comboPreQualifiedMessageNode IsNot Nothing AndAlso comboPreQualifiedMessageNode.NodeType <> XmlNodeType.Comment Then
                ComboPreQualifiedMsg = SmUtil.MergeMessageKey(comboPreQualifiedMessageNode.Attributes("message").InnerText, comboPreQualifiedMessageNode)
            End If
        End If
	End Sub

End Class