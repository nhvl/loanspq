﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="LandingPageEditor.aspx.vb" Inherits="Sm_LandingPageEditor" %>
<%@ Import Namespace="System.Web.Optimization" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>APM - Landing Page Editor</title>
    <%--<link rel="stylesheet" href="//fonts.googleapis.com/css?family=Nunito|Open+Sans" />
    <link rel="stylesheet" href="/Sm/content/bootstrap/css/bootstrap.min.css" />
    <link rel="stylesheet" href="/Sm/content/landing/custom.min.css" />
    <link rel="stylesheet" href="/Sm/content/landing/main.css" />--%>
    <%:Styles.Render("~/css/lpcss")%>
	
    <%--<link rel="stylesheet" href="/Sm/content/vendors/grapesjs/css/grapesjs-preset-newsletter.css" />--%>
    <style type="text/css">
        html {
            height: 100%;
	        font-size: inherit;
        }
        body {
            margin: 0;
        }
        .place-holder-tag {
            cursor: pointer;
            margin: 10px;
            text-decoration: none;
        }
    </style>
    <%--<%=TemplateCss%>--%>
</head>
<body id="gjs">
    
    <%=TemplateHtml%>

</body>
</html>

<%:Scripts.Render("~/js/lpscript")%>

<script type="text/javascript">
    var crc = 'create-comp';
    var mvc = 'move-comp';
    var swv = 'sw-visibility';
    var expt = 'export-template';
    var imprt = 'gjs-open-import-webpage';
    var osm = 'open-sm';
    var otm = 'open-tm';
    var ola = 'open-layers';
    var obl = 'open-blocks';
    var ful = 'fullscreen';
    var prv = 'preview';

    /* - customize href trait - */
    function showUrlEditorDialog(target, input) {
        var url = target.get('attributes').href;
        var tags = ['LPQHOST', 'LQBHOST', 'LENDERREF', 'VENDORID', 'USERID', 'QUERYSTRING'];

        var dialogContent = $('<div id="div_url_editor"></div>');
        var tagList = '';
        tags.forEach(function(t) {
            tagList += '<a class="place-holder-tag">{' + t + '}</a>';
        });
        dialogContent.append(tagList);
        dialogContent.append('<br><br>');
        dialogContent.append('<label>URL:</label>');
        dialogContent.append('<input id="txtUrlEditor" class="form-control" value="' + url + '" />');

        window.BootstrapDialog.show({
            title: 'URL Editor',
            message: dialogContent,
            onshown: function() {
                $('#div_url_editor a').click(function() {
                    insertAtCaret('txtUrlEditor', $(this).html());
                });
            },
            buttons: [
                {
                    label: 'Save',
                    cssClass: 'btn-primary',
                    action: function(dialog) {
                        var href = $('#txtUrlEditor').val();
                        $(input).val(href);
                        editor.getSelected().set('href', href);
                        target.set('href', href);

                        dialog.close();
                    }
                }, {
                    label: 'Cancel',
                    action: function(dialog) {
                        dialog.close();
                    }
                }
            ]
        });
    }

    grapesjs.plugins.add('url-helper-plugin',
        function(editor) {
            // Each new type extends the default Trait
            editor.TraitManager.addType('content',
                {
                    events: {
                        'keyup': 'onChange' // trigger parent onChange method on keyup
                    },

                    /**
                    * Returns the input element
                    * @return {HTMLElement}
                    */
                    getInputEl: function() {
                        if (!this.inputEl) {
                            var input = document.createElement('input');
                            //input.readOnly = true;

                            var target = this.target;
                            var atts = this.target.get('attributes');
                            var href = '';
                            if (atts.href) {
                                href = atts.href;
                            }
                            input.value = href;

                            input.addEventListener('click',
                                function() {
                                    showUrlEditorDialog(target, this);
                                });

                            this.inputEl = input;
                        }
                        return this.inputEl;
                    },

                    /**
                    * Triggered when the value of the model is changed
                    */
                    onValueChange: function() {
                        this.target.set('href', this.model.get('value'));
                    }
                });

            var comps = editor.DomComponents;
            var dType = comps.getType('default');
            var dModel = dType.model;

            var originalLink = comps.getType('link');

            comps.addType('link',
                {
                    model: originalLink.model.extend({
                            tagname: 'a',
                            init: function() {
                                //console.log(this.get('traits'));

                                this.listenTo(this,
                                    'change:href',
                                    function(target) {
                                        this.view.attr.href = target.changed.href;
                                        this.view.render();
                                    });
                            },

                            defaults: Object.assign({},
                                dModel.prototype.defaults,
                                {
                                    type: 'link',
                                    tagName: 'a',
                                    traits: [
                                        'title', 'target',
                                        {
                                            name: 'href',
                                            type: 'content',
                                            changeProp: 1
                                        }
                                    ]
                                }),
                            /**
                           * Returns object of attributes for HTML
                           * @return {Object}
                           * @private
                           */
                            getAttrToHTML: function() {
                                for (var _len = arguments.length, args = Array(_len), _key = 0; _key < _len; _key++) {
                                    args[_key] = arguments[_key];
                                }

                                var attr = dModel.prototype.getAttrToHTML.apply(this, args);
                                //console.log(attr);
                                delete attr.onmousedown;
                                return attr;
                            }
                        },
                        {
                            isComponent: function(el) {
                                var result = void 0;

                                if (el.tagName == 'A') {
                                    result = {
                                        type: 'link',
                                        editable: 0
                                    };

                                    // The link is editable only if, at least, one of its
                                    // children is a text node (not empty one)
                                    var children = el.childNodes;
                                    var len = children.length;
                                    if (!len) delete result.editable;

                                    for (var i = 0; i < len; i++) {
                                        var child = children[i];

                                        if (child.nodeType == 3 && child.textContent.trim() != '') {
                                            delete result.editable;
                                            break;
                                        }
                                    }
                                }

                                return result;
                            }
                        }),
                    view: originalLink.view
                });

            // add new trait for rendered component
            //console.log(comps);
            //originalLink.model.get('traits').add([{ name: 'product' }]);
        });
    /* - end customize href trait - */

    var editor = grapesjs.init({
        container: '#gjs',
        height: '100%',
        plugins: ['gjs-blocks-basic', 'url-helper-plugin' /*, 'gjs-navbar', 'grapesjs-lory-slider', 'gjs-preset-webpage'*/],
        pluginsOpts: {
            'gjs-blocks-basic': {
                //blocks: ['column1', 'column2', 'column3', 'column3-7', 'text', 'link', 'image', 'video', 'map']
                blocks: ['text', 'link', 'image']
            }
        },
        allowScripts: 0,
        showDevices: 0,
        autorender: 0, // turn off auto render, will render after add new type with traits
        canvas: {
            styles: [
                'https://fonts.googleapis.com/css?family=Quattrocento+Sans:400,700" rel="stylesheet" type="text/css">',
                'https://fonts.googleapis.com/css?family=Raleway:100,200,300,700,800,900" rel="stylesheet" type="text/css">',
                '/Sm/content/landingpages/bootstrap.css" rel="stylesheet" />',
                '/Sm/content/landingpages/font-awesome.min.css" rel="stylesheet" />',
                '/Sm/content/landingpages/main.css" rel="stylesheet" />',
                '/Sm/content/landingpages/carousel.css" rel="stylesheet" />',
                '/Sm/js/slick/slick.css" rel="stylesheet" type="text/css" />',
                '/Sm/js/slick/slick-theme.css" rel="stylesheet" type="text/css" />',
                '/Sm/content/landingpages/custom.css" rel="stylesheet" />'
            ],
            scripts: [
                'https://code.jquery.com/jquery-2.1.1.min.js',
                'https://code.jquery.com/ui/1.10.4/jquery-ui.js',
                '/Sm/js/bootstrap/bootstrap.js',
                '/Sm/js/carousel/owl.carousel.min.js',
                '/Sm/js/slick/slick.min.js',
                '/Sm/js/landingpages/editor.js'
            ]
        },
        assetManager: {
            // Upload endpoint, set 'false' to disable upload, default 'false'
            upload: '<%=ServerRoot%>/Sm/FileUploadHandler.aspx',

            // The name used in POST to pass uploaded files, default: 'files'
            uploadName: 'files'
        },
		// Disable default local storage in case you've already used GrapesJS
		storageManager: {
			autosave: false,
			type: 'remote',
			urlStore: '<%=ServerRoot%>/Sm/SmHandler.aspx',
			urlLoad: '',
			params: {
				id: '<%=LandingPage.LandingPageID%>',
				command: "saveLandingPageContent"
			}
		},
		fromElement: true,
		panels: {
			stylePrefix: 'pn-',
			defaults: [
			    {
			        id: 'devices-c',
			        buttons: [
			            {
			                id: 'deviceDesktop',
			                command: 'set-device-desktop',
			                className: 'fa fa-desktop',
			                attributes: { 'title': 'Desktop' },
			                active: 1
			            }, {
			                id: 'deviceTablet',
			                command: 'set-device-tablet',
			                className: 'fa fa-tablet',
			                attributes: { 'title': 'Tablet' }
			            }, {
			                id: 'deviceMobile',
			                command: 'set-device-mobile',
			                className: 'fa fa-mobile',
			                attributes: { 'title': 'Mobile' }
			            }
			        ]
			    },
				{
					id: 'commands',
					buttons: []
				},
				{
					id: 'options',
					buttons: [
						{
							active: true,
							id: swv,
							className: 'fa fa-square-o',
							command: swv,
							context: swv,
							attributes: { title: 'View components' }
						},
						{
							id: prv,
							className: 'fa fa-eye',
							command: prv,
							context: prv,
							attributes: { title: 'Preview' }
						},
						<%If SmUtil.CheckRoleAccess(HttpContext.Current, SmSettings.Role.SuperUser) Then%>
						{
							id: "export",
							className: 'fa fa-code',
							command: expt,
							context: expt,
							attributes: { title: 'Export Template' }
						},
						//{
						//	id: "import",
						//	className: 'fa fa-download',
						//	command: imprt,
						//	context: imprt,
						//	attributes: { title: 'Import Template' }
						//},
						<%End If%>
                        {
                            id: 'undo',
                            className: 'fa fa-undo icon-undo',
                            command: 'undo',
                            attributes: { title: 'Undo (CTRL/CMD + Z)' }
                        }, {
                            id: 'redo',
                            className: 'fa fa-repeat icon-redo',
                            command: 'redo',
                            attributes: { title: 'Redo (CTRL/CMD + SHIFT + Z)' }
                        }, {
                            id: 'save-db',
                            className: 'fa fa-floppy-o',
                            command: 'save-db',
                            attributes: { title: 'Save Changes' }
                        }
                    ]
                },
                {
                    id: 'views',
                    buttons: [
                        {
                            id: osm,
                            className: 'fa fa-paint-brush',
                            command: osm,
                            active: true,
                            attributes: { title: 'Open Style Manager' }
                        },
                        {
                            id: ola,
                            className: 'fa fa-bars',
                            command: ola,
                            active: true,
                            attributes: { title: 'Open Layer Manager' }
                        },
                        {
                            id: otm,
                            className: 'fa fa-cog',
                            command: otm,
                            attributes: { title: 'Settings' }
                        },
                        {
                            id: obl,
                            className: 'fa fa-th-large',
                            command: obl,
                            attributes: { title: 'Open Blocks' }
                        }
                    ]
                }
            ]
        }
    });

	// commands
	var cmds = editor.Commands;
	cmds.add('undo', {
		run: function (editor, sender) {
			sender.set('active', 0);
			editor.UndoManager.undo(1);
		}
	});
	cmds.add('redo', {
		run: function (editor, sender) {
			sender.set('active', 0);
			editor.UndoManager.redo(1);
		}
	});
	cmds.add('save-db', {
		run: function (editor, sender) {
			sender && sender.set('active', 0); // turn off the button
			editor.store();
			_COMMON.showInfoDialog("Landing page template has been saved.", "SUCCESS");
		}
	});
    cmds.add('set-device-desktop', {
        run: function (editor) {
            editor.setDevice('Desktop');
        }
    });
    cmds.add('set-device-tablet', {
        run: function (editor) {
            editor.setDevice('Tablet');
        }
    });
    cmds.add('set-device-mobile', {
        run: function (editor) {
            editor.setDevice('Mobile portrait');
        }
    });

	// Register Blocks
	editor.BlockManager.add('text-sect', {
		label: "Text section",
		category: "Basic",
		content: '<h1 class="heading">Insert title here</h1><p class="paragraph">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua</p>',
		attributes: { class: 'gjs-fonts gjs-f-h1p' }
	});

    editor.BlockManager.add('menuItemBlock', {
        label: 'Menu Item',
        category: "Basic",
        attributes: { class: 'fa fa-list-ul' },
        content: '<a href="#" class="menu-visible">Menu Item</a>'
    });

    // Fix style in editor
	var cssComposer = editor.CssComposer;
	cssComposer.setIdRule('slider', { height: '30em', 'margin-top': '50px' });

	function disableEdit(component) {
	    if (component.attributes.classes.length > 0) {
	        if (component.attributes.classes.models.filter(function(e) { return e.id === 'non-editable'; }).length > 0) {
	            //console.log(component);
	            component.set({
	                removable: false,

	                // Indicates if it's possible to drag the component inside other
	                // Tip: Indicate an array of selectors where it could be dropped inside
	                draggable: false,

	                // Indicates if it's possible to drop other components inside
	                // Tip: Indicate an array of selectors which could be dropped inside
	                droppable: false,

	                // Set false if don't want to see the badge (with the name) over the component
	                badgable: false,

	                // True if it's possible to style it
	                // Tip:  Indicate an array of CSS properties which is possible to style
	                stylable: false,

	                // Highlightable with 'dotted' style if true
	                highlightable: false,

	                // True if it's possible to clone the component
	                copyable: false,

	                // Indicates if it's possible to resize the component (at the moment implemented only on Image Components)
	                // It's also possible to pass an object as options for the Resizer
	                resizable: false,

	                // Allow to edit the content of the component (used on Text components)
	                editable: false,

	                // Hide the component inside Layers
	                layerable: false,

	                hoverable: false,

	                selectable: true
	            });
	        }
	    }

	    component.get('components').each(function(c) {
	        disableEdit(c);
	    });
	}

    // disable edit for non-editable components
	editor.getComponents().each(function (c) {
	    disableEdit(c);
	});

    // Get the Asset Manager module first
    const am = editor.AssetManager;
    function loadAssetsFromUploaded() {
        $.ajax({
            url: '<%=ServerRoot%>/Sm/SmHandler.aspx',
            async: true,
            cache: false,
            type: 'POST',
            dataType: 'json',
            data: {
                command: 'getUploadedAssets'
            },
            success: function (res) {
                if (res && res.data) {
                    //console.log(res);
                    am.add(res.data);
                }
            }
        });
    }

    loadAssetsFromUploaded();

    // handler iframe loaded
    window.addEventListener("message", receiveMessage, false);
    function receiveMessage(event) {
        var myMsg = event.data;
        if (myMsg === 'loaded') {
            var frames = $('iframe');
            if (frames.length > 0) {
                frames[0].contentWindow.initDocumentReady();
                //frames[0].contentWindow.createInlineSvg();
            }
        }
    }

    function insertAtCaret(areaId, text) {
        var txtarea = document.getElementById(areaId);
        if (!txtarea) {
            return;
        }

        var scrollPos = txtarea.scrollTop;
        var strPos = 0;
        var br = ((txtarea.selectionStart || txtarea.selectionStart == '0') ?
            "ff" : (document.selection ? "ie" : false));
        if (br == "ie") {
            txtarea.focus();
            var range = document.selection.createRange();
            range.moveStart('character', -txtarea.value.length);
            strPos = range.text.length;
        } else if (br == "ff") {
            strPos = txtarea.selectionStart;
        }

        var front = (txtarea.value).substring(0, strPos);
        var back = (txtarea.value).substring(strPos, txtarea.value.length);
        txtarea.value = front + text + back;
        strPos = strPos + text.length;
        if (br == "ie") {
            txtarea.focus();
            var ieRange = document.selection.createRange();
            ieRange.moveStart('character', -txtarea.value.length);
            ieRange.moveStart('character', strPos);
            ieRange.moveEnd('character', 0);
            ieRange.select();
        } else if (br == "ff") {
            txtarea.selectionStart = strPos;
            txtarea.selectionEnd = strPos;
            txtarea.focus();
        }

        txtarea.scrollTop = scrollPos;
    }

    // manual render components
    editor.render();
</script>