﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="CustomApplicationScenarios.aspx.vb" Inherits="Sm_CustomApplicationScenarios"  MasterPageFile="SiteManager.master" %>
<%@ Import Namespace="Newtonsoft.Json" %>


<asp:Content runat="server" ContentPlaceHolderID="plBody">
	<div id="divCustomApplicationScenariosPage">
		<h1 class="page-header">Custom Application Scenarios</h1>
		<p>Create a link by specifying a scenario for an application. Links are not usable until they are published.</p>
		<section>
            <%-- For other devs: Do not remove below line. --%>
            <%="" %>
			<ul class="nav nav-tabs" id="mainTabs" role="tablist">
				<%If CurrentModule = SmSettings.ModuleName.XA Then%>
				<li role="presentation" class="active"><a href="#" aria-controls="tabCAS" role="tab">XA</a></li>
				<%Else %>
				<li role="presentation" <%=IIf(EnableXA, "", "class='hidden'")%>><a href="<%=BuildUrl("/sm/customapplicationscenarios.aspx", New KeyValuePair(Of String, String)("castab", "xa"))%>" role="tab">XA</a></li>
				<%End If%>
			</ul>
			<div class="tab-content">
				<div role="tabpanel" class="tab-pane active" id="tabCAS">
					<h2 class="section-title"><%=TabTitle%></h2>
					<div class="text-right">
						<button type="button" tabindex="0" onclick="master.FACTORY.showAddEditScenarioPopup(this)" class="btn btn-primary mb10"><i class="fa fa-plus" aria-hidden="true"></i>Add Scenario</button>
					</div>
						<hr/>
					<div class="no-data">
						<p>Loading...</p>
					</div>
					<div class="custom-application-scenarios-wrapper">
						<%--<div class="custom-application-scenarios-item" id="cas_114fd">
							<div>
								<div class="scenario-name">Campain 1234<span onclick="master.FACTORY.toggleCollapseScenario(this)"></span></div>
								<div class="custom-application-scenarios-btn">
									<div onclick="master.FACTORY.deleteScenario(this)">Delete</div>
									<div onclick="master.FACTORY.editScenario(this)">Edit</div>
									<label class="toggle-btn" data-on="ON" data-off="OFF">
										<input type="checkbox" checked="checked" value="" onchange="master.FACTORY.changeScenarioStatus(this)"/>
										<span class="button-checkbox"></span>
									</label>
								</div>
							</div>
							<div>Campain to target college grads</div>
							<div class="scenario-url"><a href="http://this-is-a-placeholder-for-the-generated-url" target="_blank">http://this-is-a-placeholder-for-the-generated-url</a><span onclick="master.FACTORY.copyScenarioUrl(this)"></span></div>
							<div class="collapse-able-area">
								<div>
									<p>Account Type</p>
									<div>Special (Minor) - Existing Account</div>
								</div>
								<div>
									<p>Required Products</p>
									<div>Default required products - Checking Account</div>
								</div>
								<div>
									<p>Pre-Selected Products</p>
									<div class="text-italic">No products selected</div>
								</div>
								<div>
									<p>Available Products</p>
									<div>Checking Account, Savings</div>
								</div>
							</div>
						</div>
						<div class="custom-application-scenarios-item" id="cas_11ffd">
							<div>
								<div class="scenario-name">Campain 4567<span onclick="master.FACTORY.toggleCollapseScenario(this)"></span></div>
								<div class="custom-application-scenarios-btn">
									<div onclick="master.FACTORY.deleteScenario(this)">Delete</div>
									<div onclick="master.FACTORY.editScenario(this)">Edit</div>
									<label class="toggle-btn" data-on="ON" data-off="OFF">
										<input type="checkbox" checked="checked" value="" onchange="master.FACTORY.changeScenarioStatus(this)"/>
										<span class="button-checkbox"></span>
									</label>
								</div>
							</div>
							<div>Campain to target college grads</div>
							<div class="scenario-url"><a href="http://this-is-a-placeholder-for-the-generated-url" target="_blank">http://this-is-a-placeholder-for-the-generated-url</a><span onclick="master.FACTORY.copyScenarioUrl(this)"></span></div>
							<div class="collapse-able-area">
								<div>
									<p>Account Type</p>
									<div>Special (Minor) - Existing Account</div>
								</div>
								<div>
									<p>Required Products</p>
									<div>Default required products - Checking Account</div>
								</div>
								<div>
									<p>Pre-Selected Products</p>
									<div class="text-italic">No products selected</div>
								</div>
								<div>
									<p>Available Products</p>
									<div>Checking Account, Savings</div>
								</div>
							</div>
						</div>--%>
					</div>
				</div>
			</div>
		</section>
	</div>
</asp:Content>
<asp:Content runat="server" ContentPlaceHolderID="plScripts">
	<script type="text/javascript">
		$(function() {
			<%If Not String.IsNullOrEmpty(CustomApplicationScenariosListJson) Then%>
			master.DATA.CustomApplicationScenarios = <%=CustomApplicationScenariosListJson%>;
			<%Else%>
			master.DATA.CustomApplicationScenarios = [];
			<%End If%>	
			master.FACTORY.bindingCAS();
		});
		(function(master, $, undefined) {
			master.DATA.BusinessAccountTypeList = <%=JsonConvert.SerializeObject(BusinessAccountTypeDic)%>;
			master.DATA.SpecialAccountTypeList = <%=JsonConvert.SerializeObject(SpecialAccountTypeDic)%>;
			master.DATA.AllProducts = <%=AllProductListJson%>;
			master.DATA.ProductGroupByAvaibility = <%=ProductsByAvaibilityJson%>;
			master.FACTORY.bindingCAS = function() {
				if (master.DATA.CustomApplicationScenarios != null && master.DATA.CustomApplicationScenarios.length > 0) {
					_.forEach(master.DATA.CustomApplicationScenarios, function(data) {
						var productList = populateProductList(data.IsSecondaryAccount, data.AccountType);
						if (productList && productList.length > 0) {
							if (data.UseDefaultRequiredProducts == true) data.RequiredProducts = getDefaultSelectedProducts("require", productList);
							if (data.UseDefaultPreselectedProducts == true) data.PreselectedProducts = getDefaultSelectedProducts("preselect", productList);
							if (data.UseDefaultAvailableProducts == true) data.AvailableProducts = getDefaultSelectedProducts("available", productList);
						}
						$("#tabCAS .custom-application-scenarios-wrapper").append(buildItem(data));
					});
					$("#tabCAS .no-data").addClass("hidden");
				}
				$("#tabCAS .no-data>p").text("No scenarios have been added.");
			}
			function populateProductList(is2nd, accType) {
				var avaibility = is2nd ? "2" : "1";
				if (accType == "Business") {
					avaibility += "b";
				}else if (accType == "Special") {
					avaibility += "s";
				}
				return _.cloneDeep(master.DATA.ProductGroupByAvaibility[avaibility]);
			}
			function getDefaultSelectedProducts(type, productList) {
				var result;
				if (type == "require") {
					result = _.filter(productList, { IsRequired: true });
				} else if (type == "preselect") {
					result = _.filter(productList, { IsPreselected: true });
				} else {
					result = _.filter(productList, { IsRequired: false });
				}
				return _.map(result, function(item) {
					return item.Code;
				});
			}
			function showProductSelectionPopup(type, useDefault, productList, selectedProductList, onSaveHandler) {
				//var randomId = Math.random().toString(36).substr(2, 16);
				var desc = "Select which products to make available in the application";
				var title = "Select Available Products";
				var defaultRadioLabel = "Default available products";
				if (type == "require") {
					desc = "Select which products to require in the application";
					title = "Select Required Products";
					defaultRadioLabel = "Default required products";
				}else if (type == "preselect") {
					desc = "Select which products to pre-select in the application";
					title = "Select Pre-selected Products";
					defaultRadioLabel = "Default pre-selected products";
				}
				
				var groupedList = [];
				if (productList && productList.length > 0) {
					groupedList = _.groupBy(productList, "MappedTypeName");
				}
				var $dialogContent = $("<div/>", {"style": "padding: 10px 20px 0; margin-bottom:-15px;" }).append(
						$("<div/>", {"class": "row bottom10"}).append(
							$("<div/>", {"class": "col-12"}).text(desc)
						),
						$("<div/>", {"class": "row"}).append(
							$("<div/>", {"class": "col-12"}).append(
								$("<div/>", {"class": "form-group cas-form-group"}).append(
									$("<div/>", {"class": "inline-control-group"}).append(
										$("<label/>", {"class": "radio-inline"}).append(
											$("<input/>", {"type": "radio", "name": "radUseDefaultProducts", "id": "radUseDefaultProducts_Y", "value": true}).prop("checked", useDefault),
											document.createTextNode(defaultRadioLabel)
										),
										$("<label/>", {"class": "radio-inline"}).append(
											$("<input/>", {"type": "radio", "name": "radUseDefaultProducts", "id": "radUseDefaultProducts_N", "value": false}).prop("checked", !useDefault),
											document.createTextNode("Custom products")
										)
									)
								)
							)
						),
						$("<div/>", {"class": "row", "style": "overflow: auto; max-height: 450px;"}).append(
							$("<div/>", {"class": "col-12"}).append(
								_.map(_.sortBy(Object.keys(groupedList)), function(groupName) {
									return $("<div/>", { "class": "collapse-able-wrapper bottom20" }).append(
										$("<div/>", { "class": "collapse-btn text-bold" }).append(
											$("<strong/>").text(groupName),
											$("<span/>", {"tabindex": "0", "onclick": "master.FACTORY.toggleCollapseScenario(this, event)", "onkeypress": "master.FACTORY.toggleCollapseScenario(this, event)" })
										),
										renderProductListInModal(groupedList[groupName])
									);
								})
							)
						)
					);
				_COMMON.showDialogSaveClose(title, 0, "product_selection_dialog_0", $dialogContent, "", null, function(dlg) {
					//on save
					
					//var $container = dlg.container;
					onSaveHandler($("input[name='radUseDefaultProducts']:checked", dlg.container).val() == "true", _.map($("input:checkbox:checked", dlg.container), function(item) {
						return $(item).val();
					}));
					dlg.dialog.close();
				}, function(container) {
					//onready
					$("input[name='radUseDefaultProducts']", container).on("change", function() {
						var $this = $(this);
						if ($this.val() == "true") {
							//$("input:checkbox:not(input[name='radUseDefaultProducts'])", container).attr("disabled", "true");
							var defaultList = getDefaultSelectedProducts(type, productList);
							$("input:checkbox:not(input[name='radUseDefaultProducts'])", container).each(function(idx, ele) {
								var $ele = $(ele);
								$ele.attr("disabled", true);
								if (_.indexOf(defaultList, $ele.val())>-1) {
									$ele.prop("checked", true);
								} else {
									$ele.prop("checked", false);
								}
							});
						} else {
							$("input:checkbox:not(input[name='radUseDefaultProducts'])", container).each(function(idx, ele) {
								var $ele = $(ele);
								$ele.removeAttr("disabled", true);
								$ele.prop("checked", false);
							});
						}
					});
					if (useDefault) {
						$("#radUseDefaultProducts_Y", container).trigger("change");
					} else {
						$("#radUseDefaultProducts_N", container).trigger("change");
						$("input:checkbox:not(input[name='radUseDefaultProducts'])", container).each(function(idx, ele) {
							var $ele = $(ele);
							$ele.removeAttr("disabled");
							if (_.indexOf(selectedProductList, $ele.val())>-1) {
								$ele.prop("checked", true);
							} else {
								$ele.prop("checked", false);
							}
						});
					}

				}, function() {
				}, BootstrapDialog.SIZE_WIDE, true, "Select", "Cancel", false, false);
			}
			function renderProductListInModal(lst) {
				var result = [];
				if (lst.length > 5) {
					var lst1 = [];
					var lst2 = [];
					var pc = Math.ceil(lst.length / 2);
					for (var idx = 0; idx < pc; idx++) {
						lst1.push(renderProductItemInModal(lst[idx]));
						if (pc + idx <= lst.length - 1) {
							lst2.push(renderProductItemInModal(lst[pc + idx]));
						}
					}
					result.push($("<div/>", { "class": "row collapse-able-area" }).append(
						$("<div/>", { "class": "col-6" }).append(lst1),
						$("<div/>", { "class": "col-6" }).append(lst2)
					));

				} else {
					result.push($("<div/>", { "class": "row collapse-able-area" }).append(
						$("<div/>", { "class": "col-12" }).append(
							_.map(lst, function(prod) { return renderProductItemInModal(prod); })
						)
					));
				}
				return result;
			}
			function renderProductItemInModal(data) {
				var randomId = Math.random().toString(36).substr(2, 16);
				return $("<div/>", { "class": "checkbox" }).append(
						$("<input/>", {"id": "chk_" + randomId, "type": "checkbox", "value": data.Code}),
						$("<label/>", {"for": "chk_" + randomId}).text(data.Name)
				);
			}
			master.FACTORY.showAddEditScenarioPopup = function(scenarioId) {
				var scenarioObj;
				var existingScenarioObj	= _.find(master.DATA.CustomApplicationScenarios, { ID: scenarioId });
				var isEditing = true;
				if (!existingScenarioObj) {
					scenarioObj = initEmptyScenario();
					isEditing = false;
				} else {
					scenarioObj = _.cloneDeep(existingScenarioObj);
				}
				var productList = populateProductList(scenarioObj.IsSecondaryAccount, scenarioObj.AccountType);
				var $dialogContent = $("<div/>", {"class": "custom-application-scenarios-modal scrolling-region", "style": "padding: 20px; overflow: auto; max-height: 500px;" }).append(
						$("<div/>", {"class": "row bottom10"}).append(
							$("<div/>", {"class": "col-12"}).text("Select required, preselected, and available products for this application. If no products are selected for a section, no products will show within the application for that respective section.")
						),
						$("<div/>", {"class": "row"}).append(
							$("<div/>", {"class": "col-12"}).append(
								$("<div/>", {"class": "form-group cas-form-group"}).append(
									$("<label/>", {"class": "require-ico"}).text("Scenario Name"),
									$("<div/>").text("Set a unique scenario name to be used within created URL. Only letters, numbers, and underscores are allowed."),
									$("<input/>", {"id": "txtScenarioName_" + scenarioObj.ID, "class": "form-control", "style": "max-width:60%;", "type": "text", "maxlength": "50", "data-required": true, "data-id": scenarioObj.ID, "data-field": "scenarioName", "data-format": "pattern", "data-pattern": "^[a-zA-Z0-9_]+$", "data-message-invalid-data": "Only letters, numbers, and underscore are allowed.", "value": scenarioObj.Name})
								)
							)
						),
						$("<div/>", {"class": "row bottom20"}).append(
							$("<div/>", {"class": "col-12"}).append(
								$("<div/>", {"class": "form-group cas-form-group"}).append(
									$("<label/>").text("Description"),
									$("<div/>").text("The description will only display within the Application Portal Manager and will not be displayed to the applicant."),
									$("<input/>", {"id": "txtScenarioDesc_" + scenarioObj.ID, "class": "form-control", "type": "text", "maxlength": "150", "value": scenarioObj.Description})
								)
							)
						),
						$("<div/>", {"class": "row"}).append(
							$("<div/>", {"class": "col-12"}).append(
								$("<div/>", {"class": "form-group cas-form-group"}).append(
									$("<label/>").text("Application Account Type"),
									$("<div/>", {"class": "inline-control-group"}).append(
										$("<label/>", {"class": "radio-inline"}).append(
											$("<input/>", {"type": "radio", "name": "radAccountType_" + scenarioObj.ID, "id": "radAccountType_Personal_" + scenarioObj.ID, "value": "Personal"}).prop("checked", scenarioObj.AccountType == "Personal"),
											document.createTextNode("Personal")
										),
										$("<label/>", {"class": "radio-inline"}).append(
											$("<input/>", {"type": "radio", "name": "radAccountType_" + scenarioObj.ID, "id": "radAccountType_Business_" + scenarioObj.ID, "value": "Business"}).prop("checked", scenarioObj.AccountType == "Business"),
											document.createTextNode("Business")
										),
										$("<label/>", {"class": "radio-inline"}).append(
											$("<input/>", {"type": "radio", "name": "radAccountType_" + scenarioObj.ID, "id": "radAccountType_Special_" + scenarioObj.ID, "value": "Special"}).prop("checked", scenarioObj.AccountType == "Special"),
											document.createTextNode("Special")
										),
										$("<select/>", {"id": "ddlSpecialAccountType_" + scenarioObj.ID, "data-required": true, "data-message-require": "Select a type of account", "class": "form-control select-inline" + (scenarioObj.AccountType=="Special" ? "" : " hidden")}).append(
											$("<option/>", {"value": ""}).text("--Please select--"),
											$.map(master.DATA.SpecialAccountTypeList, function(value, key) {
												return $("<option/>", { "value": key }).text(value);
											})
										).val(scenarioObj.AccountType == "Special" ? scenarioObj.SpecialAccountType : ""),
										$("<select/>", {"id": "ddlBusinessAccountType_" + scenarioObj.ID, "data-required": true, "data-message-require": "Select a type of account", "class": "form-control select-inline" + (scenarioObj.AccountType=="Business" ? "" : " hidden")}).append(
											$("<option/>", {"value": ""}).text("--Please select--"),
											$.map(master.DATA.BusinessAccountTypeList, function(value, key) {
												return $("<option/>", { "value": key }).text(value);
											})
										).val(scenarioObj.AccountType == "Business" ? scenarioObj.SpecialAccountType : "")
									)
								)
							)
						),
						$("<div/>", {"class": "row bottom20"}).append(
							$("<div/>", {"class": "col-12"}).append(
								$("<div/>", {"class": "form-group cas-form-group"}).append(
									$("<label/>").text("New or Existing Account Type"),
									$("<div/>", {"class": "inline-control-group"}).append(
										$("<label/>", {"class": "radio-inline"}).append(
											$("<input/>", {"type": "radio", "name": "radIsSecondaryAccount_" + scenarioObj.ID, "id": "radAccountType_Personal_" + scenarioObj.ID, "value": false}).prop("checked", !scenarioObj.IsSecondaryAccount),
											document.createTextNode("New Account")
										),
										$("<label/>", {"class": "radio-inline"}).append(
											$("<input/>", {"type": "radio", "name": "radIsSecondaryAccount_" + scenarioObj.ID, "id": "radAccountType_Business_" + scenarioObj.ID, "value": true}).prop("checked", scenarioObj.IsSecondaryAccount),
											document.createTextNode("Existing Account")
										)
									)
								)
							)
						),
						buildSelectedProductsBlock("Required Products", "required-products-wrapper", function() {
							//edit handler
							showProductSelectionPopup("require", scenarioObj.UseDefaultRequiredProducts, productList, scenarioObj.RequiredProducts, function(useDefault, selectedProducts) {
									scenarioObj.UseDefaultRequiredProducts = useDefault;
									scenarioObj.RequiredProducts = selectedProducts;
									refreshSelectedProductsBlockOnModal("div.required-products-wrapper", scenarioObj.RequiredProducts, scenarioObj.UseDefaultRequiredProducts, "Default required product");
								});
							}, function() {
							//add handler
							showProductSelectionPopup("require", true, productList, getDefaultSelectedProducts("require", productList), function(useDefault, selectedProducts) {
									scenarioObj.UseDefaultRequiredProducts = useDefault;
									scenarioObj.RequiredProducts = selectedProducts;
									refreshSelectedProductsBlockOnModal("div.required-products-wrapper", scenarioObj.RequiredProducts, scenarioObj.UseDefaultRequiredProducts, "Default required product");
								});
							}, 
							scenarioObj.RequiredProducts, scenarioObj.UseDefaultRequiredProducts, "Default required products"
						),
						buildSelectedProductsBlock("Pre-Selected Products", "preselected-products-wrapper", function() {
							//edit handler
							showProductSelectionPopup("preselect", scenarioObj.UseDefaultPreselectedProducts, productList, scenarioObj.PreselectedProducts, function(useDefault, selectedProducts) {
									scenarioObj.UseDefaultPreselectedProducts = useDefault;
									scenarioObj.PreselectedProducts = selectedProducts;
									refreshSelectedProductsBlockOnModal("div.preselected-products-wrapper", scenarioObj.PreselectedProducts, scenarioObj.UseDefaultPreselectedProducts, "Default pre-selected product");
								});
							}, function() {
							//add handler
							showProductSelectionPopup("preselect", true, productList, getDefaultSelectedProducts("preselect", productList), function(useDefault, selectedProducts) {
									scenarioObj.UseDefaultPreselectedProducts = useDefault;
									scenarioObj.PreselectedProducts = selectedProducts;
									refreshSelectedProductsBlockOnModal("div.preselected-products-wrapper", scenarioObj.PreselectedProducts, scenarioObj.UseDefaultPreselectedProducts, "Default pre-selected product");
								});
							}, 
							scenarioObj.PreselectedProducts, scenarioObj.UseDefaultPreselectedProducts, "Default pre-selected products"
						),
						buildSelectedProductsBlock("Available Products", "available-products-wrapper", function() {
							//edit handler
							showProductSelectionPopup("available", scenarioObj.UseDefaultAvailableProducts, productList, scenarioObj.AvailableProducts, function(useDefault, selectedProducts) {
									scenarioObj.UseDefaultAvailableProducts = useDefault;
									scenarioObj.AvailableProducts = selectedProducts;
									refreshSelectedProductsBlockOnModal("div.available-products-wrapper", scenarioObj.AvailableProducts, scenarioObj.UseDefaultAvailableProducts, "Default available product");
								});
							}, function() {
							//add handler
							showProductSelectionPopup("available", true, productList, getDefaultSelectedProducts("available", productList), function(useDefault, selectedProducts) {
									scenarioObj.UseDefaultAvailableProducts = useDefault;
									scenarioObj.AvailableProducts = selectedProducts;
									refreshSelectedProductsBlockOnModal("div.available-products-wrapper", scenarioObj.AvailableProducts, scenarioObj.UseDefaultAvailableProducts, "Default available product");
								});
							}, 
							scenarioObj.AvailableProducts, scenarioObj.UseDefaultAvailableProducts, "Default available products"
						)
					);
				_COMMON.showDialogSaveClose((isEditing ? "Edit" : "Add") + " Scenario", 0, "add_scenario_dialog_0", $dialogContent, "", null, function(dlg) {
					//on save
					if (!$.smValidate("add_scenario_dialog_0")) return;

					if (isEditing) {
						existingScenarioObj.UseDefaultRequiredProducts = scenarioObj.UseDefaultRequiredProducts;
						existingScenarioObj.RequiredProducts = scenarioObj.RequiredProducts;
						existingScenarioObj.UseDefaultPreselectedProducts = scenarioObj.UseDefaultPreselectedProducts;
						existingScenarioObj.PreselectedProducts = scenarioObj.PreselectedProducts;
						existingScenarioObj.UseDefaultAvailableProducts = scenarioObj.UseDefaultAvailableProducts;
						existingScenarioObj.AvailableProducts = scenarioObj.AvailableProducts;
						scenarioObj = existingScenarioObj;
					} else {
						master.DATA.CustomApplicationScenarios.push(scenarioObj);
						scenarioObj.Enabled = true;
						$("#tabCAS .no-data").addClass("hidden");
					}
					scenarioObj.Name = $("#txtScenarioName_" + scenarioObj.ID).val();
					scenarioObj.Description = $("#txtScenarioDesc_" + scenarioObj.ID).val();
					scenarioObj.AccountType = $("input[name='radAccountType_" + scenarioObj.ID + "']:checked").val();
					if (scenarioObj.AccountType == "Special") {
						scenarioObj.SpecialAccountType = $("#ddlSpecialAccountType_" + scenarioObj.ID).val();
					}else if (scenarioObj.AccountType == "Business") {
						scenarioObj.SpecialAccountType = $("#ddlBusinessAccountType_" + scenarioObj.ID).val();
					} else {
						scenarioObj.SpecialAccountType = "";
					}
					scenarioObj.IsSecondaryAccount = $("input[name='radIsSecondaryAccount_" + scenarioObj.ID + "']:checked").val() == "true";
					scenarioObj.Url = buildScenarioUrl(scenarioObj);
					updateScenarioList(scenarioObj);
					var $container = dlg.container;
					dlg.dialog.close();
				}, function(container) {
					//onready
					$("input[name='radAccountType_" + scenarioObj.ID + "']").on("change", function() {
						var $this = $(this);
						var $ddlBusinessAccountType = $("#ddlBusinessAccountType_" + scenarioObj.ID);
						var $ddlSpecialAccountType = $("#ddlSpecialAccountType_" + scenarioObj.ID);
						if ($this.val() == "Business") {
							$ddlBusinessAccountType.removeClass("hidden");
							$ddlSpecialAccountType.addClass("hidden");
						}else if ($this.val() == "Special") {
							$ddlBusinessAccountType.addClass("hidden");
							$ddlSpecialAccountType.removeClass("hidden");
						} else {
							$ddlBusinessAccountType.addClass("hidden");
							$ddlSpecialAccountType.addClass("hidden");
						}
						$(container).trigger("resetSelectedProductsBlocks");
					});
					$("input[name='radIsSecondaryAccount_" + scenarioObj.ID + "']").on("change", function() {
						$(container).trigger("resetSelectedProductsBlocks");
					});
					$(container).on("resetSelectedProductsBlocks", function() {
						scenarioObj.UseDefaultRequiredProducts = false;
						scenarioObj.RequiredProducts = null;
						scenarioObj.UseDefaultPreselectedProducts = false;
						scenarioObj.PreselectedProducts = null;
						scenarioObj.UseDefaultAvailableProducts = false;
						scenarioObj.AvailableProducts = null;

						productList = populateProductList($("input[name='radIsSecondaryAccount_" + scenarioObj.ID + "']:checked").val() == "true", $("input[name='radAccountType_" + scenarioObj.ID + "']:checked").val());

						$("div.required-products-wrapper", container).replaceWith(
							buildSelectedProductsBlock("Required Products", "required-products-wrapper", function() {
								//edit handler
								showProductSelectionPopup("require", scenarioObj.UseDefaultRequiredProducts, productList, scenarioObj.RequiredProducts, function(useDefault, selectedProducts) {
									scenarioObj.UseDefaultRequiredProducts = useDefault;
									scenarioObj.RequiredProducts = selectedProducts;
									refreshSelectedProductsBlockOnModal("div.required-products-wrapper", scenarioObj.RequiredProducts, scenarioObj.UseDefaultRequiredProducts, "Default required product");
								});
							}, function() {
								//add handler
								showProductSelectionPopup("require", true, productList, getDefaultSelectedProducts("require", productList), function(useDefault, selectedProducts) {
									scenarioObj.UseDefaultRequiredProducts = useDefault;
									scenarioObj.RequiredProducts = selectedProducts;
									refreshSelectedProductsBlockOnModal("div.required-products-wrapper", scenarioObj.RequiredProducts, scenarioObj.UseDefaultRequiredProducts, "Default required product");
								});
							}, scenarioObj.RequiredProducts, scenarioObj.UseDefaultRequiredProducts, "Default required products")
						);
						$("div.preselected-products-wrapper", container).replaceWith(
							buildSelectedProductsBlock("Pre-Selected Products", "preselected-products-wrapper", function() {
								//edit handler
								showProductSelectionPopup("preselect", scenarioObj.UseDefaultPreselectedProducts, productList, scenarioObj.PreselectedProducts, function(useDefault, selectedProducts) {
									scenarioObj.UseDefaultPreselectedProducts = useDefault;
									scenarioObj.PreselectedProducts = selectedProducts;
									refreshSelectedProductsBlockOnModal("div.preselected-products-wrapper", scenarioObj.PreselectedProducts, scenarioObj.UseDefaultPreselectedProducts, "Default pre-selected product");
								});
							}, function() {
								//add handler
								showProductSelectionPopup("preselect", true, productList, getDefaultSelectedProducts("preselect", productList), function(useDefault, selectedProducts) {
									scenarioObj.UseDefaultPreselectedProducts = useDefault;
									scenarioObj.PreselectedProducts = selectedProducts;
									refreshSelectedProductsBlockOnModal("div.preselected-products-wrapper", scenarioObj.PreselectedProducts, scenarioObj.UseDefaultPreselectedProducts, "Default pre-selected product");
								});
							}, scenarioObj.PreselectedProducts, scenarioObj.UseDefaultPreselectedProducts, "Default pre-selected products")
						);
						$("div.available-products-wrapper", container).replaceWith(
							buildSelectedProductsBlock("Available Products", "available-products-wrapper", function() {
								//edit handler
								showProductSelectionPopup("available", scenarioObj.UseDefaultAvailableProducts, productList, scenarioObj.AvailableProducts, function(useDefault, selectedProducts) {
									scenarioObj.UseDefaultAvailableProducts = useDefault;
									scenarioObj.AvailableProducts = selectedProducts;
									refreshSelectedProductsBlockOnModal("div.available-products-wrapper", scenarioObj.AvailableProducts, scenarioObj.UseDefaultAvailableProducts, "Default available product");
								});
							}, function() {
								//add handler
								showProductSelectionPopup("available", true, productList, getDefaultSelectedProducts("available", productList), function(useDefault, selectedProducts) {
									scenarioObj.UseDefaultAvailableProducts = useDefault;
									scenarioObj.AvailableProducts = selectedProducts;
									refreshSelectedProductsBlockOnModal("div.available-products-wrapper", scenarioObj.AvailableProducts, scenarioObj.UseDefaultAvailableProducts, "Default available product");
								});
							}, scenarioObj.AvailableProducts, scenarioObj.UseDefaultAvailableProducts, "Default available products")
						);
					});
					registerDataValidator($("#add_scenario_dialog_0"), "add_scenario_dialog_0");

				}, function() {
					//onclose
					$.smValidate.cleanGroup("add_scenario_dialog_0");
				}, BootstrapDialog.SIZE_WIDE, true, (isEditing ? "Save" : "Add") + " Scenario", "Cancel", false, false);
			}
			function buildScenarioUrl(scenario, previewCode) {
				var result = "";
				<%If CurrentModule = SmSettings.ModuleName.XA Then%>
				if (scenario.AccountType == "Special") {
					result = "<%=ServerRoot%>/apply.aspx" + "?lenderref=<%=LenderRef%>&list=" + (scenario.IsSecondaryAccount ? "2s" : "1s") + "&scenario=" + scenario.Name + "&acctype=" + scenario.SpecialAccountType + (previewCode ? "&previewcode=" + previewCode : "");
				} else if (scenario.AccountType == "Business") {
					result = "<%=ServerRoot%>/apply.aspx" + "?lenderref=<%=LenderRef%>&list=" + (scenario.IsSecondaryAccount ? "2b" : "1b") + "&scenario=" + scenario.Name + "&acctype=" + scenario.SpecialAccountType + (previewCode ? "&previewcode=" + previewCode : "");
				} else if (scenario.AccountType == "Personal") {
					result = "<%=ServerRoot%>/xa/xpressapp.aspx" + "?lenderref=<%=LenderRef%>&type=" + (scenario.IsSecondaryAccount ? "2" : "1") + "&scenario=" + scenario.Name + (previewCode ? "&previewcode=" + previewCode : "");
				}
				<%End If%>
				return result;
			}
			function buildSelectedProductsBlock(label, wrapperClass, onEditHandler, onAddHandler, productList, useDefault, useDefaultLabel) {
				return $("<div/>", { "class": "row " + wrapperClass + (productList == null ? " no-product-selected" : "")}).append(
					$("<div/>", { "class": "col-12" }).append(
						$("<div/>", { "class": "form-group cas-form-group" }).append(
							$("<label/>").text(label),
							$("<span/>", { "class": "ml20 link-btn edit-btn", "tabindex": "0" }).text("Edit").on("click keypress", onEditHandler),
							$("<div/>", { "class": "selected-product-list" + (productList && productList.length > 0 ? "" : " text-italic") }).text(productList && productList.length > 0 ? (useDefault ? useDefaultLabel + " - " : "") + mapProductNames(productList).join(", ") : "No products selected"),
							$("<div/>", { "class": "link-btn mt5 add-btn", "tabindex": "0" }).text("Select Products").on("click keypress", onAddHandler)
						)
					)
				);
			}
			function refreshSelectedProductsBlockOnModal(container, selectedProducts, useDefault, useDefaultLabel) {
				var $container = $(container, ".custom-application-scenarios-modal");
				if (selectedProducts && selectedProducts.length > 0) {
					$container.find(".selected-product-list").removeClass("text-italic").text((useDefault ? useDefaultLabel + " - " : "") + mapProductNames(selectedProducts).join(", "));
				} else {
					$container.find(".selected-product-list").addClass("text-italic").text("No products selected");
				}
				if (useDefault == null) {
					$container.addClass("no-product-selected");
				} else {
					$container.removeClass("no-product-selected");
				}
				
			}
			function initEmptyScenario() {
				//var defaultProductList = populateProductList(false, "Personal");
				return {
					ID: Math.random().toString(36).substr(2, 16),
					Name: "",
					Description: "",
					AccountType: "Personal",
					SpecialAccountType: "",
					IsSecondaryAccount: false,
					UseDefaultRequiredProducts: false,
					RequiredProducts: null,//getDefaultSelectedProducts("require", defaultProductList),
					UseDefaultPreselectedProducts: false,
					PreselectedProducts: null,//getDefaultSelectedProducts("preselect", defaultProductList),
					UseDefaultAvailableProducts: false,
					AvailableProducts: null,//getDefaultSelectedProducts("available", defaultProductList),
					Enabled: true,
					Url: ""
				};
			}
			function updateScenarioList(scenario) {
				if ($("#cas_" + scenario.ID).length > 0) {
					$("#cas_" + scenario.ID).replaceWith(buildItem(scenario));
				} else {
					$("#tabCAS .custom-application-scenarios-wrapper").append(buildItem(scenario));
				}
				master.FACTORY.documentChanged();
			}
			function buildMinimalItemForPreviewing(scenario, previewCode) {
				var $scenarioItem;
				var scenarioUrl = buildScenarioUrl(scenario, previewCode);
				$scenarioItem = $("<div/>").append(
					$("<div/>").append(
						$("<p/>").text(scenario.Name),
						$("<div/>").text(scenario.Description)
					),
					$("<a/>", {"class": "btn btn-primary", "target": "_blank", "href": (scenarioUrl == "" ? "#" : scenarioUrl)}).text("Preview")
				);
				return $scenarioItem;
			}
			function buildItem(scenario) {
				var $scenarioItem;
				$scenarioItem = $("<div/>", { "class": "custom-application-scenarios-item collapse-able-wrapper", "id": "cas_" + scenario.ID, "data-id": scenario.ID }).append(
					$("<div/>").append(
						$("<div/>", { "class": "scenario-name collapse-btn" }).append(
							document.createTextNode(scenario.Name),
							$("<span/>", { "onclick": "master.FACTORY.toggleCollapseScenario(this, event)", "onkeypress": "master.FACTORY.toggleCollapseScenario(this, event)", "tabindex": "0" })
						),
						$("<div/>", { "class": "custom-application-scenarios-btn" }).append(
							$("<div/>", { "tabindex": "0", "onclick": "master.FACTORY.editScenario(this, event)", "onkeypress": "master.FACTORY.editScenario(this, event)", "class": "link-btn" }).text("Edit"),
							$("<div/>", { "tabindex": "0", "onclick": "master.FACTORY.deleteScenario(this, event)", "onkeypress": "master.FACTORY.deleteScenario(this, event)", "class": "link-btn" }).text("Delete"),
							$("<label/>", { "onkeypress": "master.FACTORY.fireScenarioStatusChangevent(this, event)", "tabindex": "0", "class": "toggle-btn", "data-on": "ON", "data-off": "OFF" }).append(
								$("<input/>", { "tabindex": "-1", "type": "checkbox", "value": "", "onchange": "master.FACTORY.changeScenarioStatus(this)" }).prop("checked", scenario.Enabled),
								$("<span/>", { "class": "button-checkbox" })
							)
						)
					),
					$("<div/>").text(scenario.Description),
					$("<div/>", { "class": "scenario-url" + (scenario.Url == "" ? " hidden" : "") }).append(
						$("<a/>", { "href": scenario.Url, "target": "_blank", "class": "link-btn" }).text(scenario.Url),
						$("<span/>", { "tabindex": "0", "onclick": "master.FACTORY.copyScenarioUrl(this, event)", "onkeypress": "master.FACTORY.copyScenarioUrl(this, event)" })
					),
					$("<div/>", { "class": "collapse-able-area" }).append(
						$("<div/>").append(
							$("<p/>").text("Account Type"),
							$("<div/>").text(scenario.AccountType + mapSpecialAccountTypeName(scenario) + " - " + (scenario.IsSecondaryAccount ? "Existing" : "New") + " Account")
						),
						$("<div/>").append(
							$("<p/>").text("Required Products"),
							$("<div/>", {"class": (scenario.RequiredProducts && scenario.RequiredProducts.length > 0 ? "" : "text-italic")}).text(scenario.RequiredProducts && scenario.RequiredProducts.length > 0 ? (scenario.UseDefaultRequiredProducts ? "Default required products - " : "") + mapProductNames(scenario.RequiredProducts).join(", ") : "No products selected")
						),
						$("<div/>").append(
							$("<p/>").text("Pre-Selected Products"),
							$("<div/>", {"class": (scenario.PreselectedProducts && scenario.PreselectedProducts.length > 0 ? "" : "text-italic")}).text(scenario.PreselectedProducts && scenario.PreselectedProducts.length > 0 ? (scenario.UseDefaultPreselectedProducts ? "Default pre-selected products - " : "") +  mapProductNames(scenario.PreselectedProducts).join(", ") : "No products selected")
						),
						$("<div/>").append(
							$("<p/>").text("Available Products"),
							$("<div/>", {"class": (scenario.AvailableProducts && scenario.AvailableProducts.length > 0 ? "" : "text-italic")}).text(scenario.AvailableProducts && scenario.AvailableProducts.length > 0 ? (scenario.UseDefaultAvailableProducts ? "Default available products - " : "") +   mapProductNames(scenario.AvailableProducts).join(", ") : "No products selected")
						)
					)
				);
				return $scenarioItem;
			}
			function mapSpecialAccountTypeName(scenario) {
				var result = "";
				if (scenario.AccountType == "Business") {
					result = " (" + (master.DATA.BusinessAccountTypeList[scenario.SpecialAccountType] || "N/A") + ")";
				}else if (scenario.AccountType == "Special") {
					result = " (" + (master.DATA.SpecialAccountTypeList[scenario.SpecialAccountType] || "N/A") + ")";
				}
				return result;
			}
			function mapProductNames(products) {
				var result = [];
				if (products && products.length > 0 && master.DATA.AllProducts && master.DATA.AllProducts.length > 0) {
					_.forEach(products, function(pCode) {
						var foundItem = _.find(master.DATA.AllProducts, { Code: pCode });
						if (foundItem) {
							result.push(foundItem.Name);
						} else {
							result.push("N/A");
						}
					});
				}
				return result;
			}
			master.FACTORY.toggleCollapseScenario = function(srcEle, event) {
				var $srcEle = $(srcEle);
				$srcEle.closest(".collapse-able-wrapper").toggleClass("collapsed");
			}
			master.FACTORY.deleteScenario = function(srcEle, event) {
				var $scenarioEle = $(srcEle).closest(".custom-application-scenarios-item");
				_COMMON.showConfirmDialog("Are you sure you want to delete this scenario?", function() {
					//yes
					_.remove(master.DATA.CustomApplicationScenarios, { ID: $scenarioEle.data("id") });
					master.FACTORY.documentChanged();
					if (master.DATA.CustomApplicationScenarios.length == 0) {
						$("#tabCAS .no-data").removeClass("hidden");
					}
					$scenarioEle.remove();
				}, function() {
					//no
				}, "Yes", "No");
			}
			master.FACTORY.editScenario = function(srcEle, event) {
				var $srcEle = $(srcEle);
				master.FACTORY.showAddEditScenarioPopup($srcEle.closest(".custom-application-scenarios-item").data("id"));
			}
			master.FACTORY.copyScenarioUrl = function(srcEle) {
				var $srcEle = $(srcEle);
				var $anchor = $srcEle.prev("a");
				var $el = document.createElement('textarea');
				$el.value = $anchor.attr("href");              
				$el.setAttribute('readonly', '');
				$el.style.position = 'absolute';
				$el.style.left = '-9999px';
				document.body.appendChild($el);					
				var selected = document.getSelection().rangeCount > 0 ? document.getSelection().getRangeAt(0) : false;                                   
				$el.select();
				if (document.execCommand('copy')) {
					_COMMON.noty("success", "Copied", 500);
				}
				document.body.removeChild($el);
				if (selected) {
					document.getSelection().removeAllRanges();
					document.getSelection().addRange(selected);
				}
			}
			master.FACTORY.fireScenarioStatusChangevent = function(srcEle, event) {
				$(srcEle).trigger("click");
			}
			master.FACTORY.changeScenarioStatus = function(srcEle) {
				var $scenarioEle = $(srcEle).closest(".custom-application-scenarios-item");
				var scenario = _.find(master.DATA.CustomApplicationScenarios, { ID: $scenarioEle.data("id") });
				if (typeof scenario != "undefined" && scenario != null) {
					scenario.Enabled = $(srcEle).is(":checked");
					master.FACTORY.documentChanged();
				}
			}
			master.FACTORY.SaveDraft = function (revisionId) {
				var dataObj = collectSubmitData();
				dataObj.command = 'saveCustomApplicationScenarios';
				dataObj.revisionId = revisionId;
				$.ajax({
					url: "/sm/smhandler.aspx",
					async: true,
					cache: false,
					type: 'POST',
					dataType: 'html',
					data: dataObj,
					success: function (responseText) {
						var response = $.parseJSON(responseText);
						if (response.IsSuccess) {
							master.FACTORY.onSaveDraftSuccess();
						} else {
							_COMMON.noty("error", "Error", 500);
						}
					}
				});
			};
			function showScenarioPreviewSelectionPopup(previewCode) {
				var $dialogContent = $("<div/>", { "class": "custom-application-scenarios-preview-wrapper" }).append(
					_.map(master.DATA.CustomApplicationScenarios, function(scenario) {
						return buildMinimalItemForPreviewing(scenario, previewCode);
					})
				);
				_COMMON.showDialogClose("Choose Which Scenario To Preview", 0, "choose_scenario_to_preview_dialog_0", $dialogContent, "", null, function (container) {
					//ready
				}, function () {
					//onclose
				}, BootstrapDialog.SIZE_WIDE, false);
			}
			master.FACTORY.Preview = function (revisionId) {
				var dataObj = collectSubmitData();
				dataObj.command = 'previewCustomApplicationScenarios';
				dataObj.revisionId = revisionId;
				$.ajax({
					url: "/sm/smhandler.aspx",
					async: true,
					cache: false,
					type: 'POST',
					dataType: 'html',
					data: dataObj,
					success: function (responseText) {
						var response = $.parseJSON(responseText);
						if (response.IsSuccess) {
							master.FACTORY.onPreviewSuccess();
							if (master.DATA.CustomApplicationScenarios.length > 0) {
								showScenarioPreviewSelectionPopup(response.Info.previewcode);
							}
							//_COMMON.openInNewTab(response.Info.previewurl);
						} else {
							_COMMON.noty("error", "Error", 500);
						}
					}
				});
			};
			master.FACTORY.Publish = function (comment, publishAll) {
				var dataObj = collectSubmitData();
				dataObj.command = 'publishCustomApplicationScenarios';
				dataObj.comment = comment;
				dataObj.publishAll = publishAll;
				$.ajax({
					url: "/sm/smhandler.aspx",
					async: true,
					cache: false,
					type: 'POST',
					dataType: 'html',
					data: dataObj,
					success: function (responseText) {
						var response = $.parseJSON(responseText);
						if (response.IsSuccess) {
							master.FACTORY.onPublishSuccess();
						} else {
							_COMMON.noty("error", "Error", 500);
						}
					}
				});
			};


			function collectSubmitData() {
				var result = {};
				result.lenderconfigid = '<%=LenderConfigID%>';
				result.tab = '<%=CurrentTab%>';
				var clonedObj = _.cloneDeep(master.DATA.CustomApplicationScenarios);
				_.forEach(clonedObj, function(sc) {
					if (sc.UseDefaultRequiredProducts == true) sc.RequiredProducts = [];
					if (sc.UseDefaultPreselectedProducts == true) sc.PreselectedProducts = [];
					if (sc.UseDefaultAvailableProducts == true) sc.AvailableProducts = [];
				});
				result.data = JSON.stringify(clonedObj);
				console.log(clonedObj);
				return result;
			}
			function registerDataValidator(container, groupName) {
				var $container = $(container);
				$.smValidate.removeValidationGroup(groupName);
				$("[data-required='true'],[data-format]", $container).each(function (idx, ele) {
					if ($(ele).is(":radio")) {
						var $placeHolder = $(ele).closest("div");
						$(ele).observer({
							validators: [
								function (partial) {
									var $self = $(this);
									if ($self.is(":visible") == false) return "";
									if ($("input[name='" + $self.attr("name") + "']:checked", $(container)).length == 0) {
										return "Please select an option";
									}
									return "";
								}
							],
							validateOnBlur: true,
							placeHolder: $placeHolder,
							group: groupName
						});
					} else {
						$(ele).observer({
							validators: [
								function (partial) {
									var $self = $(this);
									if ($self.is(":visible") == false) return "";
									if ($self.data("required") && $.trim($self.val()) === "") {
										if ($self.data("message-require")) {
											return $self.data("message-require");
										} else {
											return "This field is required";
										}
									}
									var format = $self.data("format");
									if (format) {
										var msg = "Invalid format";
										if ($self.data("message-invalid-data")) {
											msg = $self.data("message-invalid-data");
										}
										if (format === "email") {
											if (_COMMON.ValidateEmail($self.val()) === false) {
												return msg;
											}
										} else if (format === "phone") {
											if (_COMMON.ValidatePhone($self.val()) === false) {
												return msg;
											}
										}else if (format == "pattern") {
											var re = new RegExp($self.data("pattern"), "i");
											if (!re.test($self.val())) {
												return msg;
											}
										}
									}
									if ($self.data("field") == "scenarioName") {
										var isExisted = _.findIndex(master.DATA.CustomApplicationScenarios, function(p) {
											return p.Name == $self.val() && p.ID != $self.data("id");
										}) != -1;
										if (isExisted) {
											return "\"" + $self.val() + "\" is in used. Please choose another name.";
										}
									}
									return "";
								}
							],
							validateOnBlur: true,
							group: groupName
						});
					}
				});
			}
		}(window.master = window.master || {}, jQuery));
		
	</script>
</asp:Content>
<asp:Content runat="server" ContentPlaceHolderID="plActionButtons"></asp:Content>