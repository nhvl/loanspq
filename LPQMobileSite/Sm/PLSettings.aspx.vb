﻿Imports Sm
Imports System.Xml
Imports LPQMobile.Utils

Partial Class Sm_PLSettings
	Inherits SmApmBasePage
	Private Const LOAN_NODE_NAME As String = "PERSONAL_LOAN"
	Protected Property PLBooking As String
	Protected Property PLXSell As Boolean = False
	Protected Property PLDocuSign As Boolean = False
	Protected Property PLDocuSignPDFGroup As String
	Protected Property PLJointEnable As Boolean = True
    Protected Property PLInterestRate As String = "2.5"
	Protected Property PLLocationPool As Boolean = False
    Protected Property PLPreviousAddressEnable As Boolean = False
    Protected Property PLPreviousEmploymentEnable As Boolean = False
    Protected Property PLPreviousAddressThreshold As String = ""
    Protected Property PLPreviousEmploymentThreshold As String = ""
    Protected Property DeclarationList As Dictionary(Of String, SmDeclarationItem)
    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load

		If Not Page.IsPostBack Then
			If Not EnablePL Then
				Response.Redirect(BuildUrl("/sm/generalsettings.aspx"), True)
			End If
			Dim smMasterPage = CType(Me.Master, Sm_SiteManager)
			smMasterPage.LenderConfigXml = LenderConfigXml
			smMasterPage.RevisionID = LenderConfig.RevisionID
			smMasterPage.LastModifiedBy = LenderConfig.LastModifiedByUserFullName
			smMasterPage.LastModifiedByUserID = LenderConfig.LastModifiedByUserID
			smMasterPage.SelectedMainMenuItem = "config"
			smMasterPage.SelectedSubMenuItem = "settings"
			smMasterPage.CurrentNodeGroup = SmSettings.NodeGroup.PLSettings
			smMasterPage.BreadCrumb = "Configure / Settings / Personal Loan"

			Dim PLBookingNode As XmlNode = GetNode(LOAN_BOOKING(LOAN_NODE_NAME))
			If PLBookingNode IsNot Nothing Then
				PLBooking = PLBookingNode.Value
			End If

			Dim PLxsellEnabledNode As XmlNode = GetNode(XSELL(LOAN_NODE_NAME))
			If PLxsellEnabledNode IsNot Nothing Then
				PLXSell = PLxsellEnabledNode.Value.ToUpper().Equals("Y")
			End If

			Dim PLdocusignEnabledNode As XmlNode = GetNode(DOCU_SIGN(LOAN_NODE_NAME))
			If PLdocusignEnabledNode IsNot Nothing Then
				PLDocuSign = PLdocusignEnabledNode.Value.ToUpper().Equals("Y")
			End If

			Dim PLdocusignPDFGroupNode As XmlNode = GetNode(DOCU_SIGN_PDF_GROUP(LOAN_NODE_NAME))
			If PLdocusignPDFGroupNode IsNot Nothing Then
				PLDocuSignPDFGroup = PLdocusignPDFGroupNode.Value.Trim()
			End If

			Dim customListRateNode As XmlNode = GetNode(CUSTOM_LIST_RATES("pl"))
			If customListRateNode IsNot Nothing Then
				If customListRateNode.Name = "RATES" Then
					customListRateNode = customListRateNode.SelectSingleNode("ITEM[@loan_type='pl']")
				End If
				Dim plRateNode As XmlNode = customListRateNode.Attributes("rate")
				If plRateNode IsNot Nothing Then
					PLInterestRate = plRateNode.Value
				End If
			End If
			Dim plJointEnableNode As XmlNode = GetNode(PL_JOINT_ENABLE())
			If plJointEnableNode IsNot Nothing Then
				PLJointEnable = Not plJointEnableNode.Value.ToUpper().Equals("N")
			End If
			Dim PLLocationPoolNode As XmlNode = GetNode(LOAN_LOCATION_POOL(LOAN_NODE_NAME))
            If PLLocationPoolNode IsNot Nothing Then
                PLLocationPool = PLLocationPoolNode.Value.ToUpper().Equals("Y")
			End If
			Dim loanDeclarationList = Common.GetAllDeclarations(LOAN_NODE_NAME)
            If loanDeclarationList IsNot Nothing AndAlso loanDeclarationList.Count > 0 Then
                DeclarationList = (From item In loanDeclarationList Select New SmDeclarationItem() With {.Key = item.Key, .DisplayText = item.Value, .Active = False}).ToDictionary(Function(p) p.Key, Function(p) p)
                Dim declarationsDraftNode As XmlNode = GetNode(LOAN_DECLARATIONS(LOAN_NODE_NAME))
                If declarationsDraftNode IsNot Nothing Then
                    Dim declarationDraftNodes = declarationsDraftNode.SelectNodes("DECLARATION")
                    If declarationDraftNodes IsNot Nothing AndAlso declarationDraftNodes.Count > 0 Then
                        For Each node As XmlNode In declarationDraftNodes
                            Dim key As String = node.Attributes("key").Value
                            If DeclarationList.ContainsKey(key) Then
                                DeclarationList.Item(key).Active = True
                            End If
                        Next
                    End If
                End If
            End If

            Dim plPreviousEmploymentThresholdNode As XmlNode = GetNode(PREVIOUS_EMPLOYMENT_THRESHOLD("PERSONAL_LOAN"))
            If plPreviousEmploymentThresholdNode IsNot Nothing AndAlso Common.SafeString(plPreviousEmploymentThresholdNode.Value) <> "" Then
                PLPreviousEmploymentThreshold = Common.SafeString(plPreviousEmploymentThresholdNode.Value)
                PLPreviousEmploymentEnable = True
            End If
            Dim plPreviousAddressThresholdNode As XmlNode = GetNode(PREVIOUS_ADDRESS_THRESHOLD("PERSONAL_LOAN"))
            If plPreviousAddressThresholdNode IsNot Nothing AndAlso Common.SafeString(plPreviousAddressThresholdNode.Value) <> "" Then
                PLPreviousAddressThreshold = Common.SafeString(plPreviousAddressThresholdNode.Value)
                PLPreviousAddressEnable = True
            End If
        End If
	End Sub

End Class
