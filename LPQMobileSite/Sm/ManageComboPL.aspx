﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="ManageComboPL.aspx.vb" Inherits="Sm_ManageComboPL" MasterPageFile="SiteManager.master" %>

<asp:Content runat="server" ContentPlaceHolderID="plBody">
	<div>
		<h1 class="page-header">Manage Combo Personal Loan and XA Settings</h1>
		<p>Combo App is a feature that allows applicant(s) to fill and submit one application that generates one XA and one loan application on the lender side (back-office). Enabling Combo Apps will allow the Combo Messages to display once the applicant(s) submit an application.</p>
		<section>
			<h2 class="section-title">Messages</h2>
			<p>These are the messages that display depending on the applicant’s status.</p>
			<div>
				<h3 class="property-title">Loan Approved and XA Approved</h3>
				<div class="html-editor" contenteditable="true" data-required="true" placeholder="Enter your message..." id="txtComboBothPreApprovedMsg"><%=ComboBothPreApprovedMsg%></div>
			</div>
            <div>
				<h3 class="property-title">Loan Approved and XA Referred</h3>
				<div class="html-editor" contenteditable="true" data-required="true" placeholder="Enter your message..." id="txtComboXASubmittedMsg"><%=ComboXASubmittedMsg%></div>
			</div>
            <div>
				<h3 class="property-title">Loan Referred and  XA Approved</h3>
				<div class="html-editor" contenteditable="true" data-required="true" placeholder="Enter your message..." id="txtComboXAPreApprovedMsg"><%=ComboXAPreApprovedMsg%></div>
			</div>
			<div>
				<h3 class="property-title">Loan Referred and XA Referred</h3>
				<div class="html-editor" contenteditable="true" data-required="true" placeholder="Enter your message..." id="txtComboSubmittedMsg"><%=ComboSubmittedMsg%></div>
			</div>
			 <div>
				<h3 class="property-title">Loan Pre-qualified and XA Approved</h3>
				<div class="html-editor" contenteditable="true" data-required="true" placeholder="Enter your messages..." id="txtComboPreQualifiedMsg"><%=ComboPreQualifiedMsg%></div>
			</div>
			
		</section>
	</div>
</asp:Content>
<asp:Content runat="server" ContentPlaceHolderID="plScripts">
	<script type="text/javascript" src="js/ckeditor/ckeditor.js"></script>
	<script type="text/javascript">
		CKEDITOR.disableAutoInline = true;
		$(function () {
			$(".html-editor").ckEditor({
				onContentChanged: function (editor) { master.FACTORY.documentChanged(editor); },
				onCommandClicked: function (editor, commandBtn) { master.FACTORY.documentChanged(editor); }
			});
			registerDataValidator();
		});
		(function (master, $, undefined) {
			master.FACTORY.SaveDraft = function (revisionId) {
				if ($.smValidate("ValidateData") == false) return;
				var dataObj = collectSubmitData();
				dataObj.command = 'savemanagecombopl';
				dataObj.revisionId = revisionId;
				$.ajax({
					url: "/sm/smhandler.aspx",
					async: true,
					cache: false,
					type: 'POST',
					dataType: 'html',
					data: dataObj,
					success: function (responseText) {
						var response = $.parseJSON(responseText);
						if (response.IsSuccess) {
							master.FACTORY.onSaveDraftSuccess();
						} else {
							_COMMON.noty("error", "Error", 500);
						}
					}
				});
			};
			master.FACTORY.Preview = function (revisionId) {
				if ($.smValidate("ValidateData") == false) return;
				var dataObj = collectSubmitData();
				dataObj.command = 'previewmanagecombopl';
				dataObj.revisionId = revisionId;
				$.ajax({
					url: "/sm/smhandler.aspx",
					async: true,
					cache: false,
					type: 'POST',
					dataType: 'html',
					data: dataObj,
					success: function (responseText) {
						var response = $.parseJSON(responseText);
						if (response.IsSuccess) {
							master.FACTORY.onPreviewSuccess();
							_COMMON.openInNewTab(response.Info.previewurl);
						} else {
							_COMMON.noty("error", "Error", 500);
						}
					}
				});
			};
			master.FACTORY.validateBeforePublish = function () {
				return $.smValidate("ValidateData");
			}
			master.FACTORY.Publish = function (comment, publishAll) {
				if ($.smValidate("ValidateData") == false) return;
				var dataObj = collectSubmitData();
				dataObj.command = 'publishmanagecombopl';
				dataObj.comment = comment;
				dataObj.publishAll = publishAll;
				$.ajax({
					url: "/sm/smhandler.aspx",
					async: true,
					cache: false,
					type: 'POST',
					dataType: 'html',
					data: dataObj,
					success: function (responseText) {
						var response = $.parseJSON(responseText);
						if (response.IsSuccess) {
							master.FACTORY.onPublishSuccess();
						} else {
							_COMMON.noty("error", "Error", 500);
						}
					}
				});
			};
		}(window.master = window.master || {}, jQuery));
		function registerDataValidator() {
			$.smValidate.removeValidationGroup("ValidateData");
			$("[data-required='true']").each(function (idx, ele) {
				$(ele).observer({
					validators: [
						function (partial) {
							var $self = $(this);
							var content = "";
							if ($self.hasClass("html-editor")) {
								content = CKEDITOR.instances[$self.attr("id")].getData();
								if (content == $self.attr("placeholder")) content = "";
							} else if ($self.hasClass("sm-textarea")) {
								content = $self.val();
							}
							if ($.trim(content) === "") return "This field is required";
							return "";
						}
					],
					validateOnBlur: true,
					group: "ValidateData"
				});
			});
		}
		function collectSubmitData() {
			var result = {};
			result.comboBothPreapprovedMsg = _COMMON.getEditorValue("txtComboBothPreApprovedMsg");
			result.comboSubmittedMsg = _COMMON.getEditorValue("txtComboSubmittedMsg");
			result.comboXAPreapprovedMsg = _COMMON.getEditorValue("txtComboXAPreApprovedMsg");
            result.comboXASubmittedMsg = _COMMON.getEditorValue("txtComboXASubmittedMsg");
            result.comboPreQualifiedMsg = _COMMON.getEditorValue("txtComboPreQualifiedMsg");
			result.lenderconfigid = '<%=LenderConfigID%>';
			return result;
		}
	</script>
</asp:Content>
<asp:Content runat="server" ContentPlaceHolderID="plActionButtons"></asp:Content>