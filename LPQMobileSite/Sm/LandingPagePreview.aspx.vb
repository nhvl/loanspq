﻿Imports LPQMobile.Utils
Imports System.Xml

Partial Class Sm_LandingPagePreview
	Inherits System.Web.UI.Page

	Protected TemplateCss As String
	Protected TemplateHtml As String
    Protected ReferenceId As String
    
	Public ReadOnly Property ServerRoot() As String
		Get
			Dim port As String = ""
			If (Request.Url.Scheme = "http" AndAlso Request.Url.Port <> 80) OrElse (Request.Url.Scheme = "https" AndAlso Request.Url.Port <> 443) Then
				port = ":" + Request.Url.Port.ToString()
			End If
			Return Request.Url.Scheme + "://" + Request.Url.Host + port
		End Get
	End Property

	Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
		If Not Page.IsPostBack Then
			' '' DOTO: Test template, NO commit
			'TemplateHtml = File.ReadAllText(Server.MapPath("~/App_Data/LandingPageTemplates/default.txt"))
			'Return

			Dim smBL As New SmBL
			Dim landingPageID As Guid = Common.SafeGUID(Request.Params("id"))
			Dim landingPage = smBL.GetLandingPageByID(landingPageID)
			If landingPage IsNot Nothing Then
				Dim portal = smBL.GetPortalByLenderConfigID(landingPage.LenderConfigID, pbDecrypt:=False)
				If portal IsNot Nothing Then
					Dim config As New XmlDocument
					config.LoadXml(portal.ConfigData)
					Dim homepageEditorNode As XmlNode = config.SelectSingleNode("WEBSITE/@homepage_editor")
					If homepageEditorNode IsNot Nothing AndAlso homepageEditorNode.Value.ToUpper().Equals("Y") Then
						Dim allowRoles As New List(Of String)
						allowRoles.Add(SmSettings.Role.SuperUser.ToString())
						allowRoles.Add(String.Format("LenderScope${0}${1}", SmSettings.Role.LenderAdmin.ToString(), portal.LenderID.ToString()))
						allowRoles.Add(String.Format("PortalScope${0}${1}${2}", SmSettings.Role.PortalAdmin, portal.LenderID.ToString(), portal.LenderConfigID.ToString()))
						If Not SmUtil.CheckRoleAccess(HttpContext.Current, allowRoles) Then
							Response.Redirect("~/NoAccess.aspx", True)
                        End If

                        TemplateHtml = landingPage.PageData

                        '' Replace tags in URLs with real value
                        If (TemplateHtml.Contains("{LPQHOST}")) Then
                            TemplateHtml = TemplateHtml.Replace("{LPQHOST}", Common.LPQ_WEBSITE)
                        End If
                        If (TemplateHtml.Contains("{LQBHOST}")) Then
                            TemplateHtml = TemplateHtml.Replace("{LQBHOST}", Common.LQB_WEBSITE)
                        End If
                        If (TemplateHtml.Contains("{LENDERREF}")) Then
                            TemplateHtml = TemplateHtml.Replace("{LENDERREF}", portal.LenderRef)
                        End If
                        If (TemplateHtml.Contains("{VENDORID}")) Then
                            Dim vendorId = Common.SafeString(Request.Params("vendorid"))
                            TemplateHtml = TemplateHtml.Replace("{VENDORID}", vendorId)
                        End If
                        If (TemplateHtml.Contains("{USERID}")) Then
                            Dim userId = Common.SafeString(Request.Params("userId"))
                            TemplateHtml = TemplateHtml.Replace("{USERID}", userId)
                        End If
                        If (TemplateHtml.Contains("{QUERYSTRING}")) Then
                            Dim queryStr = ""
                            Dim parts = Request.RawUrl.Split("?"c)
                            If (parts.Length > 1) Then
                                queryStr = parts(1)
                            End If

                            TemplateHtml = TemplateHtml.Replace("{QUERYSTRING}", queryStr)
                        End If
                    Else
                        Response.Redirect("~/PageNotFound.aspx", True)
                    End If
                Else
                    Response.Redirect("~/PageNotFound.aspx", True)
                End If

            Else
                Response.Redirect("~/PageNotFound.aspx", True)
            End If
        End If
	End Sub
End Class
