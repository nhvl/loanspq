﻿
Imports System.Xml
Imports Sm
Imports LPQMobile.Utils

Partial Class Sm_Products
	Inherits SmApmBasePage
	Protected ProductList As Dictionary(Of String, List(Of CProduct))
	Protected EnabledProducts As New List(Of String)

	Protected Property CurrentTab As String = ""
	Protected Property CurrentModule As SmSettings.ModuleName
	Protected Property TabTitle As String = "Products for"
	Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
		If Not Page.IsPostBack Then
			Dim smMasterPage = CType(Me.Master, Sm_SiteManager)

			CurrentTab = Common.SafeEncodeString(Request.QueryString("ptab")).ToUpper()
			If String.IsNullOrWhiteSpace(CurrentTab) Then
				' detect default loan type in case it's not specified by query string
				If CheckEnabledModule(SmSettings.ModuleName.XA) Then
					CurrentTab = "XA"
				ElseIf CheckEnabledModule(SmSettings.ModuleName.CC_COMBO) Then
					CurrentTab = "CC"
				ElseIf CheckEnabledModule(SmSettings.ModuleName.HE_COMBO) Then
					CurrentTab = "HE"
				ElseIf CheckEnabledModule(SmSettings.ModuleName.PL_COMBO) Then
					CurrentTab = "PL"
				ElseIf CheckEnabledModule(SmSettings.ModuleName.VL_COMBO) Then
					CurrentTab = "VL"
				End If
			End If
			Dim loanNodeName As String = ""
			Dim tabName As String = ""
			Select Case CurrentTab
				Case "CC"
					CurrentModule = SmSettings.ModuleName.CC_COMBO
					loanNodeName = "CREDIT_CARD_LOAN"
					smMasterPage.CurrentNodeGroup = SmSettings.NodeGroup.CCComboProductsConfigure
					tabName = "Credit Card"
					TabTitle = TabTitle & " Credit Card Applications Combo"
				Case "HE"
					CurrentModule = SmSettings.ModuleName.HE_COMBO
					loanNodeName = "HOME_EQUITY_LOAN"
					smMasterPage.CurrentNodeGroup = SmSettings.NodeGroup.HEComboProductsConfigure
					tabName = "Home Equity Loan"
					TabTitle = TabTitle & " Home Equity Loans Combo"
				Case "PL"
					CurrentModule = SmSettings.ModuleName.PL_COMBO
					loanNodeName = "PERSONAL_LOAN"
					smMasterPage.CurrentNodeGroup = SmSettings.NodeGroup.PLComboProductsConfigure
					tabName = "Personal Loan"
					TabTitle = TabTitle & " Personal Loans Combo"
				Case "VL"
					CurrentModule = SmSettings.ModuleName.VL_COMBO
					loanNodeName = "VEHICLE_LOAN"
					smMasterPage.CurrentNodeGroup = SmSettings.NodeGroup.VLComboProductsConfigure
					tabName = "Vehicle Loan"
					TabTitle = TabTitle & " Vehicle Loans Combo"
				Case "XA"
					CurrentModule = SmSettings.ModuleName.XA
					loanNodeName = "XA_LOAN"
					smMasterPage.CurrentNodeGroup = SmSettings.NodeGroup.XAProductsConfigure
					tabName = "XA"
					TabTitle = TabTitle & " XA"
				Case Else
					CurrentModule = SmSettings.ModuleName.UNDEFINDED
			End Select
			If CurrentModule = SmSettings.ModuleName.UNDEFINDED OrElse Not CheckEnabledModule(CurrentModule) Then
				'terminate current page
				Response.Redirect(BuildUrl("/sm/index.aspx", New List(Of String) From {"ptab"}), True)
			End If
			
			smMasterPage.LenderConfigXml = LenderConfigXml
			smMasterPage.RevisionID = LenderConfig.RevisionID
			smMasterPage.LastModifiedBy = LenderConfig.LastModifiedByUserFullName
			smMasterPage.LastModifiedByUserID = LenderConfig.LastModifiedByUserID
			smMasterPage.SelectedMainMenuItem = "config"
			smMasterPage.SelectedSubMenuItem = "product"
			smMasterPage.BreadCrumb = "Configure / Products / " & tabName
            Dim allProducts As List(Of CProduct) = CProduct.GetAllProducts(WebsiteConfig)
            ProductList = allProducts.GroupBy(Function(p) p.AccountType).ToDictionary(Function(p) p.Key, Function(p) p.ToList())

			If CheckEnabledModule(CurrentModule) Then
				Dim accountsNode As XmlNode = GetNode(ACCOUNTS(loanNodeName))
				If accountsNode IsNot Nothing AndAlso accountsNode.HasChildNodes Then
					For Each node As XmlNode In accountsNode.ChildNodes
						If node.Name = "#comment" Then Continue For
						EnabledProducts.Add(node.Attributes("product_code").Value)
					Next
				End If
			Else
				Response.Redirect("~/NoAccess.aspx", True)
			End If
		End If
	End Sub

	Protected Function BinShowAllCheckbox(pList As List(Of String)) As String
		Dim result As String = ""
		If pList Is Nothing OrElse Not pList.Any() Then
			result = "checked='checked'"
		End If
		Return result
	End Function
End Class
