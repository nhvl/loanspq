﻿Imports Sm
Imports System.Xml
Imports LPQMobile.Utils
Partial Class Sm_BLSettings
	Inherits SmApmBasePage

	Protected Property UploadDocEnable As Boolean = False
	Protected Property UploadDocMsg As String
    Protected Property BLPreviousAddressEnable As Boolean = False
    Protected Property BLPreviousEmploymentEnable As Boolean = False
    Protected Property BLPreviousAddressThreshold As String = ""
    Protected Property BLPreviousEmploymentThreshold As String = ""

    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
		If Not EnableBL Then
			Response.Redirect(BuildUrl("/sm/generalsettings.aspx"), True)
		End If
		If Not Page.IsPostBack Then
			Dim smMasterPage = CType(Me.Master, Sm_SiteManager)
			smMasterPage.LenderConfigXml = LenderConfigXml
			smMasterPage.RevisionID = LenderConfig.RevisionID
			smMasterPage.LastModifiedBy = LenderConfig.LastModifiedByUserFullName
			smMasterPage.LastModifiedByUserID = LenderConfig.LastModifiedByUserID
			smMasterPage.SelectedMainMenuItem = "config"
			smMasterPage.SelectedSubMenuItem = "settings"
			smMasterPage.CurrentNodeGroup = SmSettings.NodeGroup.BLSettings
			smMasterPage.BreadCrumb = "Configure / Settings / Business Loan"
			Dim blUploadDocEnableNode As XmlNode = GetNode(BL_UPLOAD_DOC_ENABLE())
			If blUploadDocEnableNode IsNot Nothing Then
				UploadDocEnable = blUploadDocEnableNode.Value.ToUpper().Equals("Y")
			End If

			Dim blUploadDocNode As XmlNode = GetNode(BL_UPLOAD_DOC_MESSAGE())
            If blUploadDocNode IsNot Nothing Then
                UploadDocMsg = blUploadDocNode.InnerText.Trim()
            End If

            Dim blPreviousEmploymentThresholdNode As XmlNode = GetNode(PREVIOUS_EMPLOYMENT_THRESHOLD("BUSINESS_LOAN"))
            If blPreviousEmploymentThresholdNode IsNot Nothing AndAlso Common.SafeString(blPreviousEmploymentThresholdNode.Value) <> "" Then
                BLPreviousEmploymentThreshold = Common.SafeString(blPreviousEmploymentThresholdNode.Value)
                BLPreviousEmploymentEnable = True
            End If
            Dim blPreviousAddressThresholdNode As XmlNode = GetNode(PREVIOUS_ADDRESS_THRESHOLD("BUSINESS_LOAN"))
            If blPreviousAddressThresholdNode IsNot Nothing AndAlso Common.SafeString(blPreviousAddressThresholdNode.Value) <> "" Then
                BLPreviousAddressThreshold = Common.SafeString(blPreviousAddressThresholdNode.Value)
                BLPreviousAddressEnable = True
            End If
        End If
	End Sub

End Class


