﻿
Imports System.Xml
Imports DownloadedSettings
Imports Sm
Imports LPQMobile.Utils
Imports System.Activities.Statements

Partial Class Sm_AdvancedLogic
	Inherits SmApmBasePage

	Protected AdvancedLogicItems As New Dictionary(Of String, SmAdvancedLogicItem)
	Protected CustomQuestions As New List(Of SmCustomQuestionModel)
	Protected ApplicantQuestions As New List(Of SmApplicantQuestionModel)
	Protected CCLoanPurposes As New List(Of SmTextValueCatItem)
	Protected PLLoanPurposes As New List(Of SmTextValueCatItem)
	Protected VLLoanPurposes As New List(Of SmTextValueCatItem)
	Protected HELoanPurposes As New List(Of SmTextValueCatItem)
	Protected BLLoanPurposes As New List(Of SmTextValueCatItem)
	Protected ProductQuestions As New List(Of SmCustomQuestionModel)
	Protected LoanTypeList As New List(Of String)
	Protected CCTypes As New List(Of SmTextValuePair)
    Protected VLTypes As New List(Of SmTextValuePair)

	Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
		If Not Page.IsPostBack Then
			If CheckEnabledModule(SmSettings.ModuleName.XA) _
				OrElse CheckEnabledModule(SmSettings.ModuleName.CC) _
				OrElse CheckEnabledModule(SmSettings.ModuleName.CC_COMBO) _
				OrElse CheckEnabledModule(SmSettings.ModuleName.HE) _
				OrElse CheckEnabledModule(SmSettings.ModuleName.HE_COMBO) _
				OrElse CheckEnabledModule(SmSettings.ModuleName.PL) _
				OrElse CheckEnabledModule(SmSettings.ModuleName.PL_COMBO) _
				OrElse CheckEnabledModule(SmSettings.ModuleName.VL) _
				OrElse CheckEnabledModule(SmSettings.ModuleName.VL_COMBO) _
				OrElse CheckEnabledModule(SmSettings.ModuleName.BL) Then
				Dim smMasterPage = CType(Me.Master, Sm_SiteManager)
				smMasterPage.LenderConfigXml = LenderConfigXml
				smMasterPage.RevisionID = LenderConfig.RevisionID
				smMasterPage.LastModifiedBy = LenderConfig.LastModifiedByUserFullName
				smMasterPage.LastModifiedByUserID = LenderConfig.LastModifiedByUserID
				smMasterPage.SelectedMainMenuItem = "config"
				smMasterPage.SelectedSubMenuItem = "advancedlogic"
				smMasterPage.CurrentNodeGroup = SmSettings.NodeGroup.AdvancedLogics
				smMasterPage.BreadCrumb = "Configure / Business Rules / All Modules"
				Try
					CustomQuestions = SmUtil.DownloadCustomQuestion(WebsiteConfig)
					ApplicantQuestions = SmUtil.DownloadApplicantQuestion(WebsiteConfig)
					ProductQuestions = GetProductQuestions(WebsiteConfig)
					Dim advancedLogicNode As XmlNode = GetNode(ADVANCED_LOGICS)
					CCLoanPurposes = GetCCLoanPurposes()
					HELoanPurposes = GetHELoanPurposes()
					PLLoanPurposes = GetPLLoanPurposes()
					VLLoanPurposes = GetVLLoanPurposes()
					BLLoanPurposes = GetBLLoanPurposes()
					CCTypes = GetCCTypes()
					LoanTypeList = New List(Of String)({"xa", "cc", "he", "pl", "vl", "bl"})
					If CheckEnabledModule(SmSettings.ModuleName.CC_COMBO) Then
						LoanTypeList.Add("cc_combo")
					End If
					If CheckEnabledModule(SmSettings.ModuleName.HE_COMBO) Then
						LoanTypeList.Add("he_combo")
					End If
					If CheckEnabledModule(SmSettings.ModuleName.PL_COMBO) Then
						LoanTypeList.Add("pl_combo")
					End If
					If CheckEnabledModule(SmSettings.ModuleName.VL_COMBO) Then
						LoanTypeList.Add("vl_combo")
					End If
					VLTypes = GetVLTypes()
					If advancedLogicNode IsNot Nothing AndAlso advancedLogicNode.NodeType <> XmlNodeType.Comment Then
						Dim advancedLogicItemNodes As XmlNodeList = advancedLogicNode.SelectNodes("ADVANCED_LOGIC_ITEM")
						If advancedLogicItemNodes IsNot Nothing AndAlso advancedLogicItemNodes.Count > 0 Then
							For Each node As XmlNode In advancedLogicItemNodes
								Dim targetItem As String = node.Attributes("target_item").Value
								Dim targetType As String = node.Attributes("target_type").Value
								Dim loanType As String = node.Attributes("loan_type").Value.ToUpper()
								Dim targetText As String = ""
								If targetType = "CQ" Then
									'If loantype is combo then including the questions of XA
									If Not CustomQuestions.Any(Function(q) q.Name.ToUpper() = targetItem.ToUpper() And (q.LoanType = loanType Or (loanType.EndsWith("_COMBO") And (q.LoanType = loanType.Replace("_COMBO", "") Or q.LoanType = "XA")))) Then
										Continue For
									End If
									targetText = CustomQuestions.First(Function(q) q.Name.ToUpper() = targetItem.ToUpper() And (q.LoanType = loanType Or (loanType.EndsWith("_COMBO") And (q.LoanType = loanType.Replace("_COMBO", "") Or q.LoanType = "XA")))).Text
								ElseIf targetType = "AQ" Then
									If ApplicantQuestions IsNot Nothing AndAlso ApplicantQuestions.Any() Then
										Dim aq = ApplicantQuestions.FirstOrDefault(Function(q) q.LoanType = loanType And q.Name.ToUpper() = targetItem.ToUpper())
										If aq IsNot Nothing Then
											targetText = aq.Text
										End If
									End If
								ElseIf targetType = "PQ" Then
									If ProductQuestions IsNot Nothing AndAlso ProductQuestions.Any() Then
										Dim match = Regex.Match(loanType.ToUpper(), "(?<T>XA|COMBO)$")
										Dim pq = ProductQuestions.FirstOrDefault(Function(q) match.Success AndAlso q.LoanType = match.Groups("T").Value AndAlso q.Name.ToUpper() = targetItem.ToUpper())
										If pq IsNot Nothing Then
											targetText = pq.Text
										End If
									End If
								Else 'targetType=PP
									If loanType.StartsWith("CC") Then
										If CCLoanPurposes.Any(Function(p) p.value = targetItem) Then
											targetText = CCLoanPurposes.First(Function(p) p.value = targetItem).text
										End If
									ElseIf loanType.StartsWith("HE") Then
										If HELoanPurposes.Any(Function(p) p.value = targetItem) Then
											targetText = HELoanPurposes.First(Function(p) p.value = targetItem).text
										End If
									ElseIf loanType.StartsWith("PL") Then
										If PLLoanPurposes.Any(Function(p) p.value = targetItem) Then
											targetText = PLLoanPurposes.First(Function(p) p.value = targetItem).text
										End If
									ElseIf loanType.StartsWith("VL") Then
										If VLLoanPurposes.Any(Function(p) p.value = targetItem) Then
											targetText = VLLoanPurposes.First(Function(p) p.value = targetItem).text
										End If
									ElseIf loanType.StartsWith("BL") Then
										If BLLoanPurposes.Any(Function(p) p.value = targetItem) Then
											targetText = BLLoanPurposes.First(Function(p) p.value = targetItem).text
										End If
									End If
								End If
								Dim conditionItemNode As XmlNodeList = node.SelectNodes("CONDITIONS/CONDITION_ITEM")
								If conditionItemNode IsNot Nothing AndAlso conditionItemNode.Count > 0 Then
									Dim conditionItemList As New List(Of SmConditionItem)
									For Each conditionNode As XmlNode In conditionItemNode
										Dim item As New SmConditionItem()
										item.Name = conditionNode.Attributes("name").Value
										item.Type = conditionNode.Attributes("type").Value
										If item.Type = "CQ" Then
											'If loantype is combo then including the questions of XA
											If CustomQuestions.Any(Function(q) q.Name.ToUpper() = item.Name.ToUpper() And (q.LoanType = loanType Or (loanType.EndsWith("_COMBO") And (q.LoanType = loanType.Replace("_COMBO", "") Or q.LoanType = "XA")))) Then
												item.Text = CustomQuestions.First(Function(q) q.Name.ToUpper() = item.Name.ToUpper() And (q.LoanType = loanType Or (loanType.EndsWith("_COMBO") And (q.LoanType = loanType.Replace("_COMBO", "") Or q.LoanType = "XA")))).Text
											Else
												Continue For
											End If
										ElseIf item.Type = "AQ" Then
											If ApplicantQuestions IsNot Nothing AndAlso ApplicantQuestions.Any() Then
												Dim aq = ApplicantQuestions.FirstOrDefault(Function(q) q.LoanType = loanType And q.Name.ToUpper() = item.Name.ToUpper())
												If aq IsNot Nothing Then
													item.Text = aq.Text
												End If
											End If
										ElseIf item.Type = "PQ" Then
											item.Text = ""
											If ProductQuestions IsNot Nothing AndAlso ProductQuestions.Any() Then
												Dim match = Regex.Match(loanType.ToUpper(), "(?<T>XA|COMBO)$")
												Dim pq = ProductQuestions.FirstOrDefault(Function(q) match.Success AndAlso q.LoanType = match.Groups("T").Value AndAlso q.Name.ToUpper() = item.Name.ToUpper())
												If pq IsNot Nothing Then
													item.Text = pq.Text
												End If
											End If
										End If
										item.Comparer = conditionNode.Attributes("operator").Value
										item.Value = conditionNode.Attributes("value").Value
										item.LogicalOperator = conditionNode.Attributes("logical_operator").Value
										item.LogicalIndent = Common.SafeInteger(conditionNode.Attributes("indent").Value)
										conditionItemList.Add(item)
									Next
									If Not AdvancedLogicItems.ContainsKey(String.Format("{0}|{1}|{2}", loanType, targetType, targetItem)) Then
										AdvancedLogicItems.Add(String.Format("{0}|{1}|{2}", loanType, targetType, targetItem), New SmAdvancedLogicItem() With {.Conditions = conditionItemList, .TargetItem = targetItem, .TargetText = targetText, .TargetType = targetType})
									End If
								End If
							Next
						End If
					End If
				Catch ex As Exception
					log.Error("Could not load Advanced Logic page", ex)
				End Try
			Else
				Response.Redirect(String.Format("~/Sm/Index.aspx?lenderconfigid={0}", LenderConfigID), True)
			End If
		End If
	End Sub
	Private Function GetProductQuestions(config As CWebsiteConfig) As List(Of SmCustomQuestionModel)
		Dim result As New List(Of SmCustomQuestionModel)
		' _Availability: 1, 2, 1a, 2a, 1b, 2b
		Dim xaProdList As List(Of CProduct) = CProduct.GetProducts(config, "1")
		If xaProdList IsNot Nothing AndAlso xaProdList.Any() Then
			For Each prod As CProduct In xaProdList
				If prod.CustomQuestions IsNot Nothing AndAlso prod.CustomQuestions.Any() Then
					For Each question In prod.CustomQuestions
						If Not result.Any(Function(p) p.Name = question.QuestionName) Then
							result.Add(New SmCustomQuestionModel() With {.LoanType = "XA", .Name = question.QuestionName, .Text = question.QuestionText})
						End If
					Next
				End If
			Next
		End If
		xaProdList = CProduct.GetProducts(config, "2")
		If xaProdList IsNot Nothing AndAlso xaProdList.Any() Then
			For Each prod As CProduct In xaProdList
				If prod.CustomQuestions IsNot Nothing AndAlso prod.CustomQuestions.Any() Then
					For Each question In prod.CustomQuestions
						If Not result.Any(Function(p) p.Name = question.QuestionName) Then
							result.Add(New SmCustomQuestionModel() With {.LoanType = "XA", .Name = question.QuestionName, .Text = question.QuestionText})
						End If
					Next
				End If
			Next
		End If
		xaProdList = CProduct.GetProducts(config, "1a")
		If xaProdList IsNot Nothing AndAlso xaProdList.Any() Then
			For Each prod As CProduct In xaProdList
				If prod.CustomQuestions IsNot Nothing AndAlso prod.CustomQuestions.Any() Then
					For Each question In prod.CustomQuestions
						If Not result.Any(Function(p) p.Name = question.QuestionName) Then
							result.Add(New SmCustomQuestionModel() With {.LoanType = "XA", .Name = question.QuestionName, .Text = question.QuestionText})
						End If
					Next
				End If
			Next
		End If
		xaProdList = CProduct.GetProducts(config, "2a")
		If xaProdList IsNot Nothing AndAlso xaProdList.Any() Then
			For Each prod As CProduct In xaProdList
				If prod.CustomQuestions IsNot Nothing AndAlso prod.CustomQuestions.Any() Then
					For Each question In prod.CustomQuestions
						If Not result.Any(Function(p) p.Name = question.QuestionName) Then
							result.Add(New SmCustomQuestionModel() With {.LoanType = "XA", .Name = question.QuestionName, .Text = question.QuestionText})
						End If
					Next
				End If
			Next
		End If
		Dim loanProdList As List(Of CProduct) = CProduct.GetProductsForLoans(config)
		If loanProdList IsNot Nothing AndAlso loanProdList.Any() Then
			For Each prod As CProduct In loanProdList
				If prod.CustomQuestions IsNot Nothing AndAlso prod.CustomQuestions.Any() Then
					For Each question In prod.CustomQuestions
						If Not result.Any(Function(p) p.Name = question.QuestionName AndAlso p.LoanType = "COMBO") Then
							result.Add(New SmCustomQuestionModel() With {.LoanType = "COMBO", .Name = question.QuestionName, .Text = question.QuestionText})
						End If
					Next
				End If
			Next
		End If
		Return result.ToList()
	End Function

	Private Function GetCCLoanPurposes() As List(Of SmTextValueCatItem)
		Dim result As New Dictionary(Of String, SmTextValueCatItem)(System.StringComparison.OrdinalIgnoreCase)
		Dim enabledPurposesNode As XmlNode = GetNode(CREDIT_CARD_LOAN_PURPOSES())
		If enabledPurposesNode IsNot Nothing AndAlso enabledPurposesNode.HasChildNodes Then
			For Each node As XmlNode In enabledPurposesNode.ChildNodes
				If node.NodeType = XmlNodeType.Comment Then Continue For
				Dim value As String = node.Attributes("value").Value
				If result.ContainsKey(value) Then Continue For
				Dim category As String = ""
				If node.Attributes("category") IsNot Nothing Then
					category = node.Attributes("category").Value
				End If
				result.Add(value, New SmTextValueCatItem() With {.text = node.Attributes("text").Value, .value = value, .category = category})
			Next
		End If

		Dim downloadCreditCardPurposes As List(Of DownloadedSettings.CListItem)
		'<OUTPUT><RESPONSE><FIELD_LIST name="Loan Purposes"><LIST_ITEM text="NEW CARD" value="NEW CARD" /><LIST_ITEM text="CREDIT LIMIT INCREASE" value="CREDIT LIMIT INCREASE" /></FIELD_LIST></RESPONSE></OUTPUT>
		downloadCreditCardPurposes = CDownloadedSettings.CreditCardPurposes(WebsiteConfig)
		If downloadCreditCardPurposes IsNot Nothing AndAlso downloadCreditCardPurposes.Any() Then
			For Each item As CListItem In downloadCreditCardPurposes
				If result.ContainsKey(item.Value) Then Continue For
				result.Add(item.Value, New SmTextValueCatItem() With {.text = item.Text, .value = item.Value, .category = ""})
			Next
		End If
		Return result.Select(Function(q) q.Value).ToList()
	End Function
	Private Function GetBLLoanPurposes() As List(Of SmTextValueCatItem)
		Dim result As New Dictionary(Of String, SmTextValueCatItem)(System.StringComparison.OrdinalIgnoreCase)
		Dim vehiclePurposes = WebsiteConfig.GetEnumItems("BL_VEHICLE_PURPOSES")
		If vehiclePurposes IsNot Nothing AndAlso vehiclePurposes.Any() Then
			For Each purpose As KeyValuePair(Of String,String) In vehiclePurposes
				If result.ContainsKey(purpose.Key) Then Continue For
				result.Add(purpose.Key, New SmTextValueCatItem() With {.value = purpose.Key, .text = purpose.Value})
			Next
		End If
		Dim cardPurposes = WebsiteConfig.GetEnumItems("BL_CREDIT_CARD_PURPOSES")
		If cardPurposes IsNot Nothing AndAlso cardPurposes.Any() Then
			For Each purpose As KeyValuePair(Of String, String) In cardPurposes
				If result.ContainsKey(purpose.Key) Then Continue For
				result.Add(purpose.Key, New SmTextValueCatItem() With {.value = purpose.Key, .text = purpose.Value})
			Next
		End If
		Dim otherPurposes = WebsiteConfig.GetEnumWithCatItems("BL_OTHER_PURPOSES")
		If otherPurposes IsNot Nothing AndAlso otherPurposes.Any() Then
			For Each purpose In otherPurposes
				If result.ContainsKey(purpose.Key) Then Continue For
				result.Add(purpose.Key, New SmTextValueCatItem() With {.value = purpose.Value.Value, .text = purpose.Value.Text})
			Next
		End If
		Return result.Select(Function(q) q.Value).ToList()
	End Function

	Private Function GetHELoanPurposes() As List(Of SmTextValueCatItem)
		Dim result As New Dictionary(Of String, SmTextValueCatItem)(System.StringComparison.OrdinalIgnoreCase)
		Dim downloadHEPurposes As List(Of DownloadedSettings.CListItem)
		downloadHEPurposes = CDownloadedSettings.HomeEquityPurposes(WebsiteConfig)
		'<OUTPUT><RESPONSE><FIELD_LIST name="Loan Purposes"><LIST_ITEM text="HELOC Fixed Rate" value="HELOC Fixed Rate" /><LIST_ITEM text="Vacation Home" value="Vacation Home" /><LIST_ITEM text="Home Equity Line of Credit" value="Home Equity Line of Credit" /><LIST_ITEM text="Closed End Home Equity Loan" value="Closed End Home Equity Loan" /><LIST_ITEM text="Fixed Rate" value="Fixed Rate" /><LIST_ITEM text="Home Equity Line of Credit" value="Home Equity Line of Credit" /></FIELD_LIST></RESPONSE></OUTPUT>
		If downloadHEPurposes IsNot Nothing AndAlso downloadHEPurposes.Any() Then
			For Each item As CListItem In downloadHEPurposes
				If result.ContainsKey(item.Value) Then Continue For
				result.Add(item.Value, New SmTextValueCatItem() With {.text = item.Text, .value = item.Value})
			Next
		End If

		'TODO: should be using the latest from download, may need this here for sometime due to backward compartible
		Dim purposesNode As XmlNode = GetNode(HOME_EQUITY_LOAN_PURPOSES())
		If purposesNode IsNot Nothing AndAlso purposesNode.HasChildNodes Then
			For Each node As XmlNode In purposesNode.ChildNodes
				If node.NodeType = XmlNodeType.Comment Then Continue For
				Dim value As String = node.Attributes("value").Value
				If result.ContainsKey(value) Then Continue For
				result.Add(value, New SmTextValueCatItem() With {.text = node.Attributes("text").Value, .value = value})
			Next
		End If
		Return result.Select(Function(q) q.Value).ToList()
	End Function

	Private Function GetPLLoanPurposes() As List(Of SmTextValueCatItem)
		Dim result As New Dictionary(Of String, SmTextValueCatItem)(System.StringComparison.OrdinalIgnoreCase)
		Dim downloadPersonalLoanPurposes As List(Of DownloadedSettings.CListItem)
		downloadPersonalLoanPurposes = CDownloadedSettings.PersonalLoanPurposes(WebsiteConfig)
		If downloadPersonalLoanPurposes IsNot Nothing AndAlso downloadPersonalLoanPurposes.Any() Then
			For Each item As CListItem In downloadPersonalLoanPurposes
				If result.ContainsKey(item.Value) Then Continue For
				result.Add(item.Value, New SmTextValueCatItem() With {.text = item.Text, .value = item.Value})
			Next
		End If

		'TODO, may not need this, should be using the latest from download
		Dim purposesNode As XmlNode = GetNode(PERSONAL_LOAN_PURPOSES())
		If purposesNode IsNot Nothing AndAlso purposesNode.HasChildNodes Then
			For Each node As XmlNode In purposesNode.ChildNodes
				If node.NodeType = XmlNodeType.Comment Then Continue For
				Dim value As String = node.Attributes("value").Value
				If result.ContainsKey(value) Then Continue For
				result.Add(value, New SmTextValueCatItem() With {.text = node.Attributes("text").Value, .value = value})
			Next
		End If
		Return result.Select(Function(q) q.Value).ToList()
	End Function
	Private Function GetVLLoanPurposes() As List(Of SmTextValueCatItem)
		Dim result As New Dictionary(Of String, SmTextValueCatItem)(System.StringComparison.OrdinalIgnoreCase)
		
		Dim downloadVehicleLoanPurposes As List(Of DownloadedSettings.CListItem)
		downloadVehicleLoanPurposes = CDownloadedSettings.VehicleLoanPurposes(WebsiteConfig)
		'<OUTPUT><RESPONSE><FIELD_LIST name="Loan Purposes"><LIST_ITEM text="AUTO ADVANTAGE REFINANCE" value="AUTO ADVANTAGE REFINANCE" /><LIST_ITEM text="BALLOON" value="BALLOON" /><LIST_ITEM text="AUTO ADVANTAGE PURCHASE" value="AUTO ADVANTAGE PURCHASE" /><LIST_ITEM text="CONSOLIDATION" value="CONSOLIDATION" /><LIST_ITEM text="AUTO PURCHASE" value="AUTO PURCHASE" /><LIST_ITEM text="AUTO REFINANCE" value="AUTO REFINANCE" /><LIST_ITEM text="PURCHASE" value="PURCHASE" /><LIST_ITEM text="LEASE BUYOUT" value="LEASE BUYOUT" /><LIST_ITEM text="REFINANCE" value="REFINANCE" /><LIST_ITEM text="Loanstar" value="Loanstar" /></FIELD_LIST></RESPONSE></OUTPUT>
		If downloadVehicleLoanPurposes IsNot Nothing AndAlso downloadVehicleLoanPurposes.Any() Then
			For Each item As CListItem In downloadVehicleLoanPurposes
				If result.ContainsKey(item.Value) Then Continue For
				result.Add(item.Value, New SmTextValueCatItem() With {.text = item.Text, .value = item.Value, .category = ""})
			Next
		End If

		Dim enabledPurposesNode As XmlNode = GetNode(VEHICLE_LOAN_PURPOSES())
		If enabledPurposesNode IsNot Nothing AndAlso enabledPurposesNode.HasChildNodes Then
			For Each node As XmlNode In enabledPurposesNode.ChildNodes
				If node.NodeType = XmlNodeType.Comment Then Continue For
				Dim value As String = node.Attributes("value").Value
				If result.ContainsKey(value) Then Continue For
				Dim _category As String = ""
				If node.Attributes("category") IsNot Nothing Then
					_category = node.Attributes("category").Value
				End If
				result.Add(value, New SmTextValueCatItem() With {.text = node.Attributes("text").Value, .value = value, .category = _category})
			Next
		End If


		Return result.Select(Function(q) q.Value).ToList()
	End Function

	
	Private Function GetCCTypes() As List(Of SmTextValuePair)
		Dim result As New List(Of SmTextValuePair)
		Dim cardTypesNode As XmlNode = GetNode(CREDIT_CARD_TYPES())
		If cardTypesNode IsNot Nothing AndAlso cardTypesNode.HasChildNodes Then
			For Each node As XmlNode In cardTypesNode.ChildNodes
				If node.NodeType = XmlNodeType.Comment Then Continue For
				result.Add(New SmTextValuePair() With {.Value = node.Attributes("value").InnerText, .Text = node.Attributes("text").InnerText})
			Next
		End If
		Return result
	End Function
	
    Private Function GetVLTypes() As List(Of SmTextValuePair)
        Dim result As New List(Of SmTextValuePair)
        Dim vehicleTypesNode As XmlNode = GetNode(VEHICLE_TYPES())
        If vehicleTypesNode IsNot Nothing AndAlso vehicleTypesNode.HasChildNodes Then
            For Each node As XmlNode In vehicleTypesNode.ChildNodes
                If node.NodeType = XmlNodeType.Comment Then Continue For
                result.Add(New SmTextValuePair() With {.Value = node.Attributes("value").InnerText, .Text = node.Attributes("text").InnerText})
            Next
        Else
            Dim downloadVehicleTypes As List(Of DownloadedSettings.CListItem)
            downloadVehicleTypes = CDownloadedSettings.VehicleTypes(WebsiteConfig)
            For Each item As CListItem In downloadVehicleTypes
                result.Add(New SmTextValuePair() With {.Value = item.Value, .Text = item.Text})
            Next
        End If
        Return result
    End Function
End Class
