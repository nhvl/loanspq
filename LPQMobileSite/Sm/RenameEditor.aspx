﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="RenameEditor.aspx.vb" Inherits="Sm_RenameEditor" MasterPageFile="SiteManager.master" %>
<%@ Import Namespace="Newtonsoft.Json" %>

<asp:Content runat="server" ContentPlaceHolderID="plBody">
	<section class="design-theme-section two-row-button">
		<div class="collapse-able switch-btn-panel <%=IIf(PreviewUrlList.Count < 2, "hidden", "")%>">
			<div class="preview-url-ddl">
				<span>Select application type:</span>
			<select class="form-control inline-select" id="ddlPreviewList"><%	For Each item As KeyValuePair(Of String, String) In PreviewUrlList%>
				<option data-url="<%=GetNavigateUrl(item.Key)%>" value="<%=item.Key%>"><%=GetModuleName(item.Key)%></option>
				<%Next%></select>
			</div>
			<div>
				<div class="btn-group" role="group" aria-label="...">
					<%	For Each item As KeyValuePair(Of String, String) In PreviewUrlList.Where(Function(p) Not p.Key.StartsWith("XA") And Not p.Key.StartsWith("SA"))%>
					<a href="<%=GetNavigateUrl(item.Key)%>" data-id="<%=item.Key%>" class="btn btn-default"><%=GetModuleName(item.Key)%></a>
					<%Next%>
				</div>
			</div>
			<div>
				<div class="btn-group" role="group" aria-label="...">
					<%	For Each item As KeyValuePair(Of String, String) In PreviewUrlList.Where(Function(p) p.Key.StartsWith("XA") OrElse p.Key.StartsWith("SA"))%>
					<a href="<%=GetNavigateUrl(item.Key)%>" data-id="<%=item.Key%>" class="btn btn-default"><%=GetModuleName(item.Key)%></a>
					<%Next%>
				</div>	
			</div>
			
		</div>
		<div class="preview-panel">
			<div class="frame-wrapper">
				<div class="spinner">
					<div class="rect1"></div>
					<div class="rect2"></div>
					<div class="rect3"></div>
					<div class="rect4"></div>
					<div class="rect5"></div>
				</div>
				<p>Loading...</p>
		    </div>
			<div id="divPrevPage" class="editor-nav-button hidden">
				<a href="javascript:void(0);">
					<i class="fa fa-chevron-circle-left"></i>
					<span>Previous Page</span>
				</a>
			</div>
			<div id="iframeContainer" style="width: 100%; height: 100%">

			</div>
			<div id="divNextPage" class="editor-nav-button hidden">
				<a href="javascript:void(0);">
					<i class="fa fa-chevron-circle-right"></i>
					<span>Next Page</span>
				</a>
			</div>
		</div>
		<div class="design-panel">
			<h1 class="page-header">Verbiage</h1>
			<p>To edit text, click the orange pencil icon next to an available field. Save the modified changes by clicking the orange save button. Once you're ready to publish, hit the Publish button, or click "Save Draft" to save your work.</p>
			<div>
				<button type="button" class="btn btn-primary bottom10 pull-right" id="btnAddNew">Add item</button>
			</div>
			<div id="divRenameListContainer"></div> 
			
		</div>
	</section>
	<div></div>
</asp:Content>
<asp:Content runat="server" ContentPlaceHolderID="head">
</asp:Content>
<asp:Content runat="server" ContentPlaceHolderID="plScripts">
	<script type="text/javascript" src="js/easyxdm/easyXDM.debug.js"></script>
	<script type="text/javascript">
		var previewUrlArr = <%=JsonConvert.SerializeObject(PreviewUrlList)%>;
		var frameReadyCalled = false;

		var initFrame = function (id) {
			$(".preview-panel .frame-wrapper").show();
			$("#iframeContainer iframe").remove();
			$("a.btn.active", ".switch-btn-panel").removeClass("active");
			$("a[data-id='" + id + "']", ".switch-btn-panel").addClass("active");
			$("#ddlPreviewList").val(id);

			if (id == "LQB_MAIN") {
				$("#divPrevPage, #divNextPage").removeClass("hidden");
				$(".preview-panel, .design-panel").addClass("expanded-navigation-buttons");
			} else {
				$("#divPrevPage, #divNextPage").addClass("hidden");
				$(".preview-panel, .design-panel").removeClass("expanded-navigation-buttons");
			}

			frameReadyCalled = false;

			var rpcSetup = {
				remote: previewUrlArr[id],
				container: document.getElementById("iframeContainer"), // the path to the provider
			};
			var _rpc = new easyXDM.Rpc(rpcSetup,
			{
				local: {
					frameReady: function (data) {
						rpcSetup.container.getElementsByTagName("iframe")[0].width = "100%";
						rpcSetup.container.getElementsByTagName("iframe")[0].height = "100%";
						$(".preview-panel .frame-wrapper").hide();
						/*if (id == "BL") {
							//do nothing
						} else if (id == "LP") {
							rpc.initICE("#apply_container");
						} else if (/^(XA_BUSINESS|SA_BUSINESS)$/.test(id)) {

						} else if (/^(XA_SPECIAL|SA_SPECIAL)$/.test(id)) {

						} else {
							rpc.initICE(id);
						}*/
						if (id != "XA_BUSINESS" && id != "SA_BUSINESS" && id != "XA_SPECIAL" && id != "SA_SPECIAL" && id != "BL" || id == "LQB_MAIN") {
							rpc.initICE();	
						}
						//rpc.initICE(id);

						if (!frameReadyCalled) {
							// Reload the item list for the first time the frame is ready (i.e., on initial APM page load or switching the tabs)
							loadRenameItemList(id);
							frameReadyCalled = true;
						} else {
							// Else, don't reload the rename item list, but do tell the page what the update list is, to refresh the frame.
							$("table tr.renameable-item", "#divRenameListContainer").each(function(idx, row) {
								sendRenameListItemsToFrame(row);
							});
						}
					},
					openRenameDropdownDialog: function (data) {
						master.FACTORY.ShowRenameDropboxDialog(data);
					},
					updateRenameList: function(data) {
						master.FACTORY.UpdateRenameList(data);
					},
					submitForm: function(data) {
						//console.log('<%=ServerRoot%>' + data);
						console.log(data);
						rpc = submitFrame(data, id);
					},
					canGoToPreviousQuickNavPage: function (bValue) {
						if (bValue == true) {
							$("#divPrevPage a").removeClass("disabled");
						} else {
							$("#divPrevPage a").addClass("disabled");
						}
						
					},
					canGoToNextQuickNavPage: function (bValue) {
						if (bValue == true) {
							$("#divNextPage a").removeClass("disabled");
						} else {
							$("#divNextPage a").addClass("disabled");
						}
					},
				},
				remote: {
					initICE: {},
					updateICE: {},
					goToPreviousQuickNavPage: {},
					goToNextQuickNavPage: {},
				}
			});
			return _rpc;
		}
		var submitFrame = function(url, id) {
			$(".preview-panel .frame-wrapper").show();
			$("#iframeContainer iframe").remove();
			var rpcSetup = {
				remote: url,
				container: document.getElementById("iframeContainer"), // the path to the provider
			};
			var _rpc = new easyXDM.Rpc(rpcSetup,
			{
				local: {
					frameReady: function(data) {
						rpcSetup.container.getElementsByTagName("iframe")[0].width = "100%";
						rpcSetup.container.getElementsByTagName("iframe")[0].height = "100%";
						$(".preview-panel .frame-wrapper").hide();
						if (id == "XA_BUSINESS" || id == "SA_BUSINESS" || id == "XA_SPECIAL" || id == "SA_SPECIAL" || id == "BL" || id == "LQB_MAIN") {
							rpc.initICE();	
							$("table tr.renameable-item", "#divRenameListContainer").each(function(idx, row) {
								initRenameListItem(row);
							});
							if (RENAMEABLE_ABORT_LIST != null && RENAMEABLE_ABORT_LIST.length > 0) {
								$.each(RENAMEABLE_ABORT_LIST, function(idx, item) {
									rpc.updateICE({originalText: item, newText: null});
								});
							}
						}
						
					},
					openRenameDropdownDialog: function (data) {
						master.FACTORY.ShowRenameDropboxDialog(data);
					},
					updateRenameList: function(data) {
						master.FACTORY.UpdateRenameList(data);
					},
					submitForm: function(data) {
						console.log(data);
						rpc = submitFrame(data, id);
					},
					canGoToPreviousQuickNavPage: function (bValue) {
						if (bValue == true) {
							$("#divPrevPage a").removeClass("disabled");
						} else {
							$("#divPrevPage a").addClass("disabled");
						}
						
					},
					canGoToNextQuickNavPage: function (bValue) {
						if (bValue == true) {
							$("#divNextPage a").removeClass("disabled");
						} else {
							$("#divNextPage a").addClass("disabled");
						}
					},
				},
				remote: {
					initICE: {},
					updateICE: {},
					goToPreviousQuickNavPage: {},
					goToNextQuickNavPage: {},
				}
			});
			return _rpc;
		}

		var initItem = master.FACTORY.getParaByName("loantype");
		if (initItem == "") {
			initItem = "<%=DefaultLoanType%>";
		}
		var rpc = initFrame(initItem);

		$(function () {
			$("#btnAddNew").on("click", master.FACTORY.ShowAddDialog);
			//$("a.btn", ".switch-btn-panel").each(function(idx, ele) {
			//	var $self = $(ele);
			//	$self.on("click", function(e) {
			//		if ($("body").data("hasChanged") == true) {
			//			if (confirm("The document contains unsaved changes. Do you want to leave?") == false) {
			//				e.preventDefault();
			//				return false;
			//			} /*else {
			//				$("body").data("hasChanged", false);
			//			}*/
			//		}
			//		//window.location.reload();
			//		//var key = $self.attr("href").replace("#", "");
			//		//rpc = initFrame(key.toUpperCase());
			//	});
			//});
			var lastPreviewUrlSelected; 
			$("#ddlPreviewList").on("click", function() {
				lastPreviewUrlSelected = $(this).val();
			}).on("change", function() {
				if ($("body").data("hasChanged") == true) {
					if (confirm("The document contains unsaved changes. Do you want to leave?") == false) {
						$(this).val(lastPreviewUrlSelected);

					} else {
						$("body").data("hasChanged", false);
						window.location = $(this).find("option:selected").data("url");
					}
				} else {
					window.location = $(this).find("option:selected").data("url");
				}
			});
			$("#divPrevPage").on("click", function () {
				$("#divPrevPage, #divPrevPage *").blur();
				rpc.goToPreviousQuickNavPage();
			});
			$("#divNextPage").on("click", function () {
				$("#divNextPage, #divNextPage *").blur();
				rpc.goToNextQuickNavPage();
			});
		});
		(function (master, $, undefined) {
			master.FACTORY.SaveDraft = function (revisionId) {
				if ($.smValidate("ValidateData") == false) return;
				var dataObj = collectSubmitData();
				dataObj.revisionId = revisionId;
				dataObj.command = "saveRenameItem";
				$.ajax({
					url: "/sm/smhandler.aspx",
					async: true,
					cache: false,
					type: 'POST',
					dataType: 'html',
					data: dataObj,
					success: function (responseText) {
						var response = $.parseJSON(responseText);
						if (response.IsSuccess) {
							master.FACTORY.onSaveDraftSuccess();
						} else {
							_COMMON.noty("error", "Error", 500);
						}
					}
				});
			};
			master.FACTORY.Publish = function (comment, publishAll) {
				if ($.smValidate("ValidateData") == false) return;
				var dataObj = collectSubmitData();
				dataObj.comment = comment; 
				dataObj.publishAll = publishAll;
				dataObj.command = "publishRenameItem";
				$.ajax({
					url: "/sm/smhandler.aspx",
					async: true,
					cache: false,
					type: 'POST',
					dataType: 'html',
					data: dataObj,
					success: function (responseText) {
						var response = $.parseJSON(responseText);
						if (response.IsSuccess) {
							master.FACTORY.onPublishSuccess();
						} else {
							_COMMON.noty("error", "Error", 500);
						}
					}
				});
			};
			master.FACTORY.ShowEditDialog = function (row) {
				var $row = $(row);
				var originalText = $row.find("td[data-field='original']").text();
				var currentText = $row.find("td[data-field='new']").text();
				var htmlContent = '<div class="row"><div class="col-12"><div class="form-group"><label class="required-field">Original Value</label><input type="text" class="form-control" id="txtOrignalValue" placeholder="Original Value" readonly value="'+originalText+'" data-message-require="Please fill out this field" data-required="true"/></div></div><div class="col-12"><div class="form-group"><label>New Value</label><input type="text" class="form-control" id="txtNewValue" value="'+currentText+'" placeholder="New Value" data-message-require="Please fill out this field" data-required="true"/></div></div></div>';
				htmlContent = '<div class="dialog-content">'+ htmlContent+'</div>';
				_COMMON.showDialogSaveClose("Edit", 0, "rename_edit_dialog_0", htmlContent, "", null, function(container) {
					//on save
					var originalValue = $("#txtOrignalValue").val();
					var newValue = $("#txtNewValue").val();
					master.FACTORY.UpdateRenameList({ originalText: originalValue, currentText: currentText, newText: newValue });
					container.dialog.close();
				}, function(container) {
					//onready
					var $container = $(container);
					$("#txtNewValue").focus();
					registerDataValidator($container, "rename_edit_dialog_0");
				}, function() {
					//onclose
					$.smValidate.cleanGroup("rename_edit_dialog_0");
				});
			};
			master.FACTORY.ShowAddDialog = function () {
				var htmlContent = '<div class="row"><div class="col-12"><div class="form-group"><label class="required-field">Original Value</label><input type="text" class="form-control" id="txtOrignalValue" placeholder="Original Value" data-message-require="Please fill out this field" data-required="true"/></div></div><div class="col-12"><div class="form-group"><label>New Value</label><input type="text" class="form-control" id="txtNewValue" placeholder="New Value" data-message-require="Please fill out this field" data-required="true"/></div></div></div>';
				htmlContent = '<div class="dialog-content">'+ htmlContent+'</div>';
				_COMMON.showDialogSaveClose("Add", 0, "rename_add_dialog_0", htmlContent, "", null, function(container) {
					//on save
					var originalValue = $("#txtOrignalValue").val();
					var newValue = $("#txtNewValue").val();
					master.FACTORY.UpdateRenameList({ originalText: originalValue, currentText: originalValue, newText: newValue });
					container.dialog.close();
				}, function(container) {
					//onready
					var $container = $(container);
					$("#txtOrignalValue").focus();
					registerDataValidator($container, "rename_add_dialog_0");
				}, function() {
					//onclose
					$.smValidate.cleanGroup("rename_add_dialog_0");
				});
			};
			master.FACTORY.ShowRenameDropboxDialog = function (data) {
				var htmlContent = "";
				console.log(data);
				$.each(data, function(idx, item) {
					htmlContent += '<div class="btn-rename"><div><img src="/sm/content/images/pencil_ico_w.png" data-command="make-it-editable" width="18px" height="18px"/><img src="/sm/content/images/cancel_ico_w.png" width="18px" height="18px" data-command="cancel" class="hidden"/><img src="/sm/content/images/save_ico_w.png" data-command="save" class="hidden" width="18px" height="18px"/></div></div>';
					htmlContent += "<div class='option-item' data-original-text='"+ item.originalText+"'>"+item.currentText+"</div>";
				});
				htmlContent = '<div class="dialog-content">'+ htmlContent+'</div>';
				_COMMON.showDialogClose("Options", 0, "rename_dialog_0", htmlContent, "", null, function(container) {
					//onready
					var $container = $(container);
					$container.find("img[data-command='make-it-editable']").on("click", function (e) {
						var $self = $(this);
						var $ctrl = $self.closest(".btn-rename").next("div.option-item");
						$ctrl.attr("contenteditable", true);
						$ctrl.data("current-text", $ctrl.text());
						$ctrl.focus();
						$ctrl.select();
						$self.addClass("hidden");
						$self.parent().find("img[data-command]").not(this).removeClass("hidden");
					});
					$container.find("img[data-command='save']").on("click", function (e) {
						var $self = $(this);
						var $ctrl = $self.closest(".btn-rename").next("div.option-item");
						if ($ctrl.text() == "") {
							$self.closest(".btn-rename").addClass("hidden apm-hidden").next().addClass("hidden apm-hidden");
						}
						master.FACTORY.UpdateRenameList({originalText:$ctrl.data("original-text"), currentText: $ctrl.data("current-text"), newText: $ctrl.text() });

						$ctrl.attr("contenteditable", false);
						$self.addClass("hidden");
						$self.parent().find("img[data-command='cancel']").addClass("hidden");
						$self.parent().find("img[data-command='make-it-editable']").removeClass("hidden");

					});
					$container.find("img[data-command='cancel']").on("click", function (e) {
						var $self = $(this);
						var $ctrl = $self.closest(".btn-rename").next("div.option-item");
						$ctrl.attr("contenteditable", false);
						$ctrl.text($ctrl.data("current-text"));
						$self.addClass("hidden");
						$self.parent().find("img[data-command='save']").addClass("hidden");
						$self.parent().find("img[data-command='make-it-editable']").removeClass("hidden");
					});
				}, function() {
					//onclose
				});
			};
			master.FACTORY.UpdateRenameList = function(data) {
				console.log(data);
				var $container = $("#divRenameListContainer");
				var foundedRow = null;
				$("tr.no-row", $container).remove();
				//rpc.updateICE(data);
				master.FACTORY.documentChanged();
				var foundedRowArr = [];
				var bridgeFlag = false;

				$("tr.renameable-item", $container).each(function(idx, row) {
					if ($(row).find("td[data-field='original']").text() == data.originalText) {
						foundedRowArr.push({foundedRow: row, bridgeFlag: false});
					}else if ($(row).find("td[data-field='original']").text() == data.newText || $(row).find("td[data-field='new']").text() == data.originalText) {
						/*
						1. Add A1 --> B1
						2. Add B2 --> B3
						3. Edit A1 --> B2	
						==> then result should be reduced to A1 -> B3
						*/
						foundedRowArr.push({foundedRow: row, bridgeFlag: true});
						//foundedRow = row;
						//bridgeFlag = true;
						//return false;
					}
				});
				/*if (foundedRow) {
					if (bridgeFlag == false) {
						if ($(foundedRow).find("td[data-field='original']").text() == data.newText) {
							$(foundedRow).remove();
							if ($("tr.renameable-item", $container).length == 0) {
								$('<tr class="no-row"><td colspan="4" align="center">No item found</td></tr>').appendTo($("tbody", $container));
							}
						} else {
							$(foundedRow).find("td[data-field='new']").text(data.newText);
						}
						data.originalText = $(foundedRow).find("td[data-field='original']").text();
						rpc.updateICE(data);
					} else {
						var newText = $(foundedRow).find("td[data-field='new']").text();
						$("tr.renameable-item", $container).each(function(idx, row) {
							if ($(row).find("td[data-field='original']").text() == data.originalText) {
								$(row).find("td[data-field='new']").text(newText);
							}
						});
						data.newText = newText;
						rpc.updateICE(data);
					}
				} else {*/
				if (foundedRowArr.length  > 0) {
					$.each(foundedRowArr, function(ix, ele) {
						if (ele.bridgeFlag == false) {
							if ($(ele.foundedRow).find("td[data-field='original']").text() == data.newText) {
								$(ele.foundedRow).remove();
								if ($("tr.renameable-item", $container).length == 0) {
									$('<tr class="no-row"><td colspan="4" align="center">No item found</td></tr>').appendTo($("tbody", $container));
								}
							} else {
								$(ele.foundedRow).find("td[data-field='new']").text(data.newText);
							}
							data.originalText = $(ele.foundedRow).find("td[data-field='original']").text();
							rpc.updateICE(data);
						} else {
							var newText = $(ele.foundedRow).find("td[data-field='new']").text();
							data.newText = newText;
							rpc.updateICE(data);
							var updateExistedRow = false;
							$("tr.renameable-item", $container).each(function(idx, row) {
								if ($(row).find("td[data-field='original']").text() == data.originalText) {
									$(row).find("td[data-field='new']").text(newText);
									updateExistedRow = true;
								}
							});
							if (!updateExistedRow) {
								var $tr1 = $('<tr class="renameable-item"><td data-field="original">' + data.originalText + '</td><td align="center"><i class="fa fa-arrow-right" aria-hidden="true"></i></td><td data-field="new">' + data.newText + '</td><td align="center"><a href="#" data-command="edit">Edit</a><span class="separator">|</span><a href="#" data-command="delete">Delete</a></td></tr>').appendTo($("tbody", $container));
								initRenameListItem($tr1);
							}
						}
					});
				} else {
					rpc.updateICE(data);
					var $tr = $('<tr class="renameable-item"><td data-field="original">' + data.currentText + '</td><td align="center"><i class="fa fa-arrow-right" aria-hidden="true"></i></td><td data-field="new">' + data.newText + '</td><td align="center"><a href="#" data-command="edit">Edit</a><span class="separator">|</span><a href="#" data-command="delete">Delete</a></td></tr>').appendTo($("tbody", $container));
					initRenameListItem($tr);
				}
			};

		}(window.master = window.master || {}, jQuery));
		function collectSubmitData() {
			var result = {};
			result.lenderconfigid = '<%=LenderConfigID%>';
			result.loantype = $("a.btn.active", ".switch-btn-panel").data("id").replace("#", "");
			result.renameablelist = _COMMON.toJSON(collectRenameableItem());
			return result;
		}
		function collectRenameableItem() {
			var result = [];
			$("table tr.renameable-item", "#divRenameListContainer").each(function(idx, ele) {
				var $ele = $(ele);
				result.push({ OriginalText: $ele.find("td[data-field='original']").text(), NewText: $ele.find("td[data-field='new']").text() });
			});
			return result;
		}
		function loadRenameItemList(type) {
			$.ajax({
				url: "/sm/subpages/renameitemlist.aspx?lenderconfigid=<%=LenderConfigID%>",
				async: true,
				cache: false,
				type: 'POST',
				dataType: 'html',
				data: {type: type},
				success: function (response) {
					$("#divRenameListContainer").html(response);
					$("table tr.renameable-item", "#divRenameListContainer").each(function(idx, row) {
						initRenameListItem(row);
					});
					if (RENAMEABLE_ABORT_LIST != null && RENAMEABLE_ABORT_LIST.length > 0) {
						$.each(RENAMEABLE_ABORT_LIST, function(idx, item) {
							rpc.updateICE({originalText: item, newText: null});
						});
					}
				}
			});
		}
		function initRenameListItem(row) {
			var $row = $(row);
			rpc.updateICE({originalText: $row.find("td[data-field='original']").text(), newText: $row.find("td[data-field='new']").text()});
			$("a[data-command='edit']", $row).on("click", function() {
				master.FACTORY.ShowEditDialog($row);
			});
			$("a[data-command='delete']", $row).on("click", function() {
				if ($row.closest("tbody").find("tr").length == 1) {
					$('<tr class="no-row"><td colspan="4" align="center">No item found</td></tr>').appendTo($row.closest("tbody"));
				}
				$row.remove();
				master.FACTORY.documentChanged();
				rpc.updateICE({originalText: $row.find("td[data-field='original']").text(), newText: null});
			});
		}
		function sendRenameListItemsToFrame(row) {
			var $row = $(row);
			rpc.updateICE({originalText: $row.find("td[data-field='original']").text(), newText: $row.find("td[data-field='new']").text()});
		}

		function registerDataValidator(container, groupName) {
			var $container = $(container);
			$.smValidate.removeValidationGroup(groupName);
			$("[data-required='true'],[data-format]", $container).each(function(idx, ele) {
				$(ele).observer({
					validators: [
						function(partial) {
							var $self = $(this);
							if ($self.data("required") && $.trim($self.val()) === "") {
								if ($self.data("message-require")) {
									return $self.data("message-require");
								} else {
									return "This field is required";
								}
							}
							var format = $self.data("format");
							if (format) {
								var msg = "Invalid format";
								if ($self.data("message-invalid-data")) {
									msg = $self.data("message-invalid-data");
								}
								if (format === "email") {
									if (_COMMON.ValidateEmail($self.val()) === false) {
										return msg;
									}
								} else if (format === "phone") {
									if (_COMMON.ValidatePhone($self.val()) === false) {
										return msg;
									}
								}
							}
							return "";
						}
					],
					validateOnBlur: true,
					group: groupName
				});
			});
		}
	</script>
</asp:Content>
<asp:Content runat="server" ContentPlaceHolderID="plActionButtons">
</asp:Content>