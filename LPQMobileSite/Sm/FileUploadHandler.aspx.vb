﻿
Imports System.IO
Imports Newtonsoft.Json

Partial Class Sm_FileUploadHandler
	Inherits System.Web.UI.Page

	Protected _log As log4net.ILog = log4net.LogManager.GetLogger(Me.GetType)

	Public ReadOnly Property ServerRoot() As String
		Get
			Dim port As String = ""
			If (Request.Url.Scheme = "http" AndAlso Request.Url.Port <> 80) OrElse (Request.Url.Scheme = "https" AndAlso Request.Url.Port <> 443) Then
				port = ":" + Request.Url.Port.ToString()
			End If
			Return Request.Url.Scheme + "://" + Request.Url.Host + port
		End Get
	End Property

	Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
		If Not Page.IsPostBack Then
			Dim response = New AssetResponse()

			Try
				Dim postedFile = HttpContext.Current.Request.Files("files[]")

				If postedFile IsNot Nothing Then
					Dim folderPath = HttpContext.Current.Server.MapPath("~/Sm/content/images/upload/")

					' create folder if not exist
					If Not Directory.Exists(folderPath) Then
						Directory.CreateDirectory(folderPath)
					End If

					Dim fileExtension = Path.GetExtension(postedFile.FileName)
					Dim fileName = Guid.NewGuid().ToString()
					fileName = fileName + fileExtension

					' Get the complete file path
					Dim fileSavePath = Path.Combine(folderPath, fileName)

					' Save the uploaded file to "UploadedFiles" folder
					postedFile.SaveAs(fileSavePath)

					' Add url to response
					response.data.Add(String.Format("{0}/Sm/content/images/upload/{1}", ServerRoot, fileName))
				End If
			Catch ex As Exception
				_log.Error("Could not upload file", ex)
			End Try

			HttpContext.Current.Response.Write(JsonConvert.SerializeObject(response))
			HttpContext.Current.Response.End()
		End If
	End Sub
End Class
