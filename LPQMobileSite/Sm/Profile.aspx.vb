﻿
Imports System.IO
Imports LPQMobile.Utils

Partial Class Sm_Profile
	Inherits SmBasePage

	Protected profileData As SmUser
	Public Property AvatarFileInfo As FileInfo
	Protected Sub Page_PreLoad(sender As Object, e As EventArgs) Handles Me.PreLoad
		Dim smMasterPage = CType(Me.Master, Sm_MasterPage)
		smMasterPage.SelectedMainMenuItem = ""
		Dim smAuthBL As New SmAuthBL
		profileData = smAuthBL.GetUserByID(UserInfo.UserID)
		If SmUtil.CheckRoleAccess(Context, SmSettings.Role.SSOUser) OrElse profileData Is Nothing Then
			Response.Redirect("/PageNotFound.aspx", True)
		End If
		If Not String.IsNullOrEmpty(profileData.Avatar) Then
			If profileData.Avatar.StartsWith("/images/avatars/") AndAlso File.Exists(Server.MapPath(profileData.Avatar)) Then
				AvatarFileInfo = New FileInfo(Server.MapPath(profileData.Avatar))
			End If
		End If
	End Sub
End Class
