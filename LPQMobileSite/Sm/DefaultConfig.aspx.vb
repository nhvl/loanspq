﻿
Partial Class Sm_DefaultConfig
	Inherits SmApmBasePage

	Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
		If Not Page.IsPostBack Then
			Dim smMasterPage = CType(Me.Master, Sm_SiteManager)
			smMasterPage.LenderConfigXml = LenderConfigXml
			smMasterPage.SelectedMainMenuItem = "config"
			smMasterPage.ShowPreviewButton = False
			smMasterPage.ShowSaveDraftButton = False
			smMasterPage.ShowPublishButton = False
		End If
	End Sub
End Class