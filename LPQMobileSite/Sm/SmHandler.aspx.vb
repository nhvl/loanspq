﻿Imports System.IO
Imports LPQMobile.BO
Imports System.Net
Imports OfficeOpenXml
Imports Sm.BO
Imports Newtonsoft.Json
Imports LPQMobile.Utils
Imports System.Web.Script.Serialization
Imports Sm
Imports System.Xml
Imports System.Activities.Statements

Partial Class Sm_SmHandler
    Inherits SmBaseHandler

    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        Dim cmd As String = Common.SafeString(Request.Form("command"))
        _log.Debug("Recieved command: " & Common.SafeString(cmd))
        Select Case cmd
			Case "savemanagexa"
				SaveManageXADraft()
			Case "previewmanagexa"
				PreviewManageXADraft()
			Case "publishmanagexa"
				PublishManageXADraft()

			Case "savemanagespecialxa"
				SaveManageSpecialXADraft()
			Case "previewmanagespecialxa"
				PreviewManageSpecialXADraft()
			Case "publishmanagespecialxa"
				PublishManageSpecialXADraft()

			Case "savemanagebusinessxa"
				SaveManageBusinessXADraft()
			Case "previewmanagebusinessxa"
				PreviewManageBusinessXADraft()
			Case "publishmanagebusinessxa"
				PublishManageBusinessXADraft()

			Case "savemanagexasecondary"
				SaveManageXASecondaryDraft()
			Case "previewmanagexasecondary"
				PreviewManageXASecondaryDraft()
			Case "publishmanagexasecondary"
				PublishManageXASecondaryDraft()

			Case "savemanagespecialxasecondary"
				SaveManageSpecialXASecondaryDraft()
			Case "previewmanagespecialxasecondary"
				PreviewManageSpecialXASecondaryDraft()
			Case "publishmanagespecialxasecondary"
				PublishManageSpecialXASecondaryDraft()

			Case "savemanagebusinessxasecondary"
				SaveManageBusinessXASecondaryDraft()
			Case "previewmanagebusinessxasecondary"
				PreviewManageBusinessXASecondaryDraft()
			Case "publishmanagebusinessxasecondary"
				PublishManageBusinessXASecondaryDraft()

			Case "savemanagevl"
				SaveManageVLDraft()
			Case "previewmanagevl"
				PreviewManageVLDraft()
			Case "publishmanagevl"
				PublishManageVLDraft()
			Case "savemanagepl"
				SaveManagePLDraft()
			Case "previewmanagepl"
				PreviewManagePLDraft()
			Case "publishmanagepl"
				PublishManagePLDraft()
			Case "savemanagebl"
				SaveManageBLDraft()
			Case "previewmanagebl"
				PreviewManageBLDraft()
			Case "publishmanagebl"
				PublishManageBLDraft()
			Case "savemanagecc"
				SaveManageCCDraft()
			Case "previewmanagecc"
				PreviewManageCCDraft()
			Case "publishmanagecc"
				PublishManageCCDraft()
			Case "savemanagehe"
				SaveManageHEDraft()
			Case "previewmanagehe"
				PreviewManageHEDraft()
			Case "publishmanagehe"
				PublishManageHEDraft()
			Case "savemanagecombovl"
				SaveManageComboVLDraft()
			Case "previewmanagecombovl"
				PreviewManageComboVLDraft()
			Case "publishmanagecombovl"
				PublishManageComboVLDraft()
			Case "savemanagecombopl"
				SaveManageComboPLDraft()
			Case "previewmanagecombopl"
				PreviewManageComboPLDraft()
			Case "publishmanagecombopl"
				PublishManageComboPLDraft()
			Case "savemanagecombocc"
				SaveManageComboCCDraft()
			Case "previewmanagecombocc"
				PreviewManageComboCCDraft()
			Case "publishmanagecombocc"
				PublishManageComboCCDraft()
			Case "savemanagecombohe"
				SaveManageComboHEDraft()
			Case "previewmanagecombohe"
				PreviewManageComboHEDraft()
			Case "publishmanagecombohe"
				PublishManageComboHEDraft()
			Case "savemanagelqb"
				SaveManageLQBDraft()
			Case "previewmanagelqb"
				PreviewManageLQBDraft()
			Case "publishmanagelqb"
				PublishManageLQBDraft()
				'Case "savesettings"
				'	SaveSetttingsDraft()
				'Case "previewsettings"
				'	PreviewSettingsDraft()
				'Case "publishsettings"
				'	PublishSettingsDraft()
			Case "savegeneralsettings"
				SaveGeneralSetttingsDraft()
			Case "previewgeneralsettings"
				PreviewGeneralSettingsDraft()
			Case "publishgeneralsettings"
				PublishGeneralSettings()
			Case "savexasettings"
				SaveXaSetttingsDraft()
			Case "previewxasettings"
				PreviewXaSettingsDraft()
			Case "publishxasettings"
				PublishXaSettings()
			Case "saveallloanssettings"
				SaveAllLoansSetttingsDraft()
			Case "previewallloanssettings"
				PreviewAllLoansSettingsDraft()
			Case "publishallloanssettings"
				PublishAllLoansSettings()
			Case "saveblsettings"
				SaveBLSetttingsDraft()
			Case "previewblsettings"
				PreviewBLSettingsDraft()
			Case "publishblsettings"
				PublishBLSettings()
			Case "saveccsettings"
				SaveCCSetttingsDraft()
			Case "previewccsettings"
				PreviewCCSettingsDraft()
			Case "publishccsettings"
				PublishCCSettings()
			Case "savehesettings"
				SaveHESetttingsDraft()
			Case "previewhesettings"
				PreviewHESettingsDraft()
			Case "publishhesettings"
				PublishHESettings()
			Case "saveplsettings"
				SavePLSetttingsDraft()
			Case "previewplsettings"
				PreviewPLSettingsDraft()
			Case "publishplsettings"
				PublishPLSettings()
			Case "savevlsettings"
				SaveVLSetttingsDraft()
			Case "previewvlsettings"
				PreviewVLSettingsDraft()
			Case "publishvlsettings"
				PublishVLSettings()
			Case "savelqbsettings"
				SaveLQBSetttingsDraft()
			Case "previewlqbsettings"
				PreviewLQBSettingsDraft()
			Case "publishlqbsettings"
				PublishLQBSettings()
			Case "uploadlogo"
				UploadLogo()
			Case "deletelogo"
				DeleteLogo()
			Case "uploadavatar"
				UploadAvatar()
			Case "deleteavatar"
				DeleteAvatar()
			Case "uploadfavico"
				UploadFavIco()
			Case "deletefavico"
				DeleteFavIco()
			Case "savedesigntheme"
				SaveDesignThemeDraft()
				'Case "previewdesigntheme"
				'	PreviewDesignThemeDraft()
			Case "publishdesigntheme"
				PublishDesignThemeDraft()
			Case "loadUserGrid"
				LoadUserGrid()
			Case "changeUserState"
				ChangeUserState()
			Case "loadAddUserForm"
				LoadAddUserForm()
			Case "saveNewUser"
				SaveNewUser()
			Case "updateUser"
				UpdateUser()
			Case "resetLoginFailureCount"
				ResetLoginFailureCount()
			Case "deleteUser"
				DeleteUser()
			Case "loadLenderGrid"
				LoadLenderGrid()
			Case "loadActionLogGrid"
				LoadActionLogGrid()
			Case "loadRevisionGrid"
				LoadRevisionGrid()
			Case "getAllLenders"
				If HttpContext.Current.User.IsInRole(SmSettings.Role.User.ToString()) Then
					GetAllLenders(UserInfo.UserID)
				Else
					GetAllLenders()
				End If
			Case "loadLenderUserRoleGrid"
				LoadLenderUserRoleGrid()
			Case "changeLenderUserRoleState"
				ChangeLenderUserRoleState()
			Case "removeLenderUserRole"
				RemoveLenderUserRole()
			Case "getAllLenderUsersForSuggest"
				GetAllLenderUsersForSuggest()
			Case "addLenderUser"
				AddLenderUser()
				'Case "loadAddLenderConfigUserForm"
				'	LoadAddLenderConfigUserForm()
			Case "loadEditUserForm"
				LoadEditUserForm()
			Case "saveLenderInfo"
				SaveLenderInfo()
			Case "cleanConfigDraft"
				CleanConfigDraft()
			Case "saveRenameItem"
				SaveRenameItemDraft()
			Case "publishRenameItem"
				PublishRenameItemDraft()
			Case "clearCache"
				ClearCache()
			Case "clearPDF"
				ClearPDF()
			Case "loadAuditLogs"
				LoadAuditLogs()
			Case "saveProductConfigure"
				SaveProductConfigure()
			Case "previewProductConfigure"
				PreviewProductConfigure()
			Case "publishProductConfigure"
				PublishProductConfigure()
				'Case "saveCustomQuestionConfigure"
				'	SaveCustomQuestionConfigure()
				'Case "previewCustomQuestionConfigure"
				'	PreviewCustomQuestionConfigure()
				'Case "publishCustomQuestionConfigure"
				'	PublishCustomQuestionConfigure()
			Case "saveconfigurecq"
				SaveConfigureCQ()
			Case "previewconfigurecq"
				PreviewConfigureCQ()
			Case "publishconfigurecq"
				PublishConfigureCQ()
			Case "saveAdvancedLogics"
				SaveAdvancedLogicsDraft()
			Case "previewAdvancedLogics"
				PreviewAdvancedLogicsDraft()
			Case "publishAdvancedLogics"
				PublishAdvancedLogicsDraft()
			Case "checkRevision"
				CheckRevision()
			Case "getRevisionNumber"
				GetRevisionNumber()
			Case "revertToRevision"
				RevertToRevision()
			Case "showChangesInRevision"
				ShowChangesInRevision()
			Case "previewRevision"
				PreviewRevision()
			Case "saveShowFieldItem"
				SaveShowFieldItemsDraft()
			Case "publishShowFieldItem"
				PublishShowFieldItemsDraft()
			Case "saveValidationItem"
				SaveValidationItemsDraft()
			Case "publishValidationItem"
				PublishValidationItemsDraft()
			Case "loadVendorGrid"
				LoadVendorGrid()
			Case "loadAddVendorForm"
				LoadAddVendorForm()
			Case "saveNewLenderVendor"
				SaveNewLenderVendor()
			Case "changeLenderVendorState"
				ChangeLenderVendorState()
			Case "loadEditLenderVendorForm"
				LoadEditLenderVendorForm()
			Case "updateLenderVendor"
				UpdateLenderVendor()
			Case "upgradeToLenderAdmin"
				UpgradeToLenderAdmin()
			Case "loadPortalUserRoleGrid"
				LoadPortalUserRoleGrid()
			Case "savePortalInfo"
				SavePortalInfo()
			Case "updatePortalUser"
				UpdatePortalUser()
			Case "addPortalUser"
				AddPortalUser()
			Case "upgradeToPortalAdmin"
				UpgradeToPortalAdmin()
			Case "downgradeToPortalAdmin"
				DowngradeToPortalAdmin()
			Case "removePortalUserRole"
				RemovePortalUserRole()
			Case "changePortalUserRoleState"
				ChangePortalUserRoleState()
			Case "loadVendorUserGrid"
				LoadVendorUserGrid()
			Case "loadAddVendorUserForm"
				LoadAddVendorUserForm()
			Case "saveNewVendorUser"
				SaveNewVendorUser()
			Case "removeVendorUser"
				RemoveVendorUser()
			Case "changeVendorUserState"
				ChangeVendorUserState()
			Case "loadCloneLenderForm"
				LoadCloneLenderForm()
			Case "loadClonePortalForm"
				LoadClonePortalForm()
			Case "generateGuid"
				GenerateGuid()
			Case "cloneLender"
				CloneLender()
			Case "clonePortal"
				ClonePortal()
			Case "loadLandingPageGrid"
				LoadLandingPageGrid()
			Case "saveLandingPageContent"
				SaveLandingPageContent()
			Case "loadAddLandingPageForm"
				LoadAddLandingPageForm()
			Case "saveNewLandingPage"
				SaveNewLandingPage()
			Case "loadEditLandingPageForm"
				LoadEditLandingPageForm()
			Case "updateLandingPage"
				UpdateLandingPage()
			Case "changeLandingPageStatus"
				ChangeLandingPageStatus()
			Case "deleteLandingPage"
				DeleteLandingPage()
			Case "loadCloneLandingPageForm"
				LoadCloneLandingPageForm()
			Case "cloneLandingPage"
				CloneLandingPage()
			Case "getUploadedAssets"
				GetUploadedAssets()
			Case "saveProfile"
				SaveProfile()
			Case "changePassword"
				ChangePassword()
			Case "unlockUser"
				UnlockUser()
			Case "saveBroadcast"
				SaveBroadcast()
			Case "loadVendorGroupAdminGrid"
				LoadVendorGroupAdminGrid()
			Case "loadAddVendorGroupAdminForm"
				LoadAddVendorGroupAdminForm()
			Case "getAvailableTags"
				GetAvailableTags()
			Case "saveNewVendorGroupAdmin"
				SaveNewVendorGroupAdmin()
			Case "downgradeToVendorGroupAdmin"
				DowngradeToVendorGroupAdmin()
			Case "changeVendorGroupAdminState"
				ChangeVendorGroupAdminState()
			Case "loadEditVendorGroupAdminForm"
				LoadEditVendorGroupAdminForm()
			Case "updateVendorGroupAdmin"
				UpdateVendorGroupAdmin()
			Case "removeVendorGroupAdmin"
				RemoveVendorGroupAdmin()
			Case "runinstatouchreport"
				RunInstaTouchReport()
			Case "runinstatouchaccountingreport"
				RunInstaTouchAccountingReport()
			Case "searchMortgageLoans"
				SearchMortgageLoans()
			Case "generateMLSearchToken"
				GenerateMLSearchToken()
			Case "saveCustomListRules"
				SaveCustomListRules()
			Case "previewCustomListRules"
				PreviewCustomListRules()
			Case "publishCustomListRules"
				PublishCustomListRules()
			Case "saveLenderSecurityConfig"
				SaveLenderSecurityConfig()
			Case "saveCustomApplicationScenarios"
				SaveCustomApplicationScenarios()
			Case "previewCustomApplicationScenarios"
				PreviewCustomApplicationScenarios()
			Case "publishCustomApplicationScenarios"
				PublishCustomApplicationScenarios()
		End Select
	End Sub

	Private Sub GenerateGuid()
		Dim res As CJsonResponse
		Try
			res = New CJsonResponse(True, Guid.NewGuid().ToString(), "OK")
		Catch ex As Exception
			_log.Error("Could not GenerateGuid.", ex)
			res = New CJsonResponse(False, "Could not generate ID. Please try again", "FAILED")
		End Try
		Response.Write(JsonConvert.SerializeObject(res))
		Response.End()
	End Sub

	Private Sub LoadAuditLogs()
		Try
			Dim control As Sm_Uc_AuditLogs = CType(LoadControl("~/Sm/Uc/AuditLogs.ascx"), Sm_Uc_AuditLogs)
			Dim lenderConfigId As Guid = Common.SafeGUID(Request.Form("lenderConfigId"))
			control.LenderConfigId = lenderConfigId
			Me.Controls.Add(control)
		Catch ex As Exception
			_log.Error("Could not load audit logs grid", ex)
			Response.Write("<h3 style='text-align: center; color: red;'>Unable to render content. Please try again later</h3>")
		End Try
	End Sub

	Private Sub UploadLogo()
		Dim fileUrl = ""
		Try
			If Request.Files.Count > 0 Then
				Dim file As HttpPostedFile = Request.Files(0)
				If file IsNot Nothing AndAlso file.ContentLength > 0 AndAlso (file.ContentType = "image/png" Or file.ContentType = "image/jpeg") Then
					Dim fileExt As String = New FileInfo(file.FileName).Extension
					Dim fileName As String = Guid.NewGuid().ToString().Replace("-", "").Substring(0, 10) & fileExt
					If Not String.IsNullOrWhiteSpace(ConfigurationManager.AppSettings.Get("AZURE_MEDIA_ACCOUNT_NAME")) Then
						Using reader = New BinaryReader(file.InputStream)
							Dim fileContent As Byte() = reader.ReadBytes(file.ContentLength)
							Dim azureSrv As New CAzureStorageService(ConfigurationManager.AppSettings.Get("AZURE_MEDIA_ACCOUNT_NAME"), ConfigurationManager.AppSettings.Get("AZURE_MEDIA_ACCOUNT_KEY"))
							Dim logoContainerName As String = ConfigurationManager.AppSettings.Get("AZURE_LOGO_CONTAINER")
							If String.IsNullOrWhiteSpace(logoContainerName) Then logoContainerName = "logos"
							fileUrl = azureSrv.UploadBlob(logoContainerName, fileName, fileContent, file.ContentType)
						End Using
					Else
						Dim logoFolderPath As String = IO.Path.Combine(Server.MapPath("~/images"), "logos")
						If Not Directory.Exists(logoFolderPath) Then
							Directory.CreateDirectory(logoFolderPath)
						End If
						file.SaveAs(Path.Combine(logoFolderPath, fileName))
						fileUrl = "/images/logos/" & fileName
					End If
				End If
			End If
		Catch ex As Exception
			_log.Error("Error while saving logo", ex)
		End Try
		Response.Write(fileUrl)
		Response.End()
	End Sub
	Private Sub DeleteLogo()
		Dim result As String = ""
		Try
			'https://lpqhieplam.blob.core.windows.net/logo/8915a919bc.png
			If Not String.IsNullOrWhiteSpace(ConfigurationManager.AppSettings.Get("AZURE_MEDIA_ACCOUNT_NAME")) Then
				Dim logoContainerName As String = ConfigurationManager.AppSettings.Get("AZURE_LOGO_CONTAINER")
				If String.IsNullOrWhiteSpace(logoContainerName) Then logoContainerName = "logos"
				Dim m As Match = Regex.Match(Common.SafeStripHtmlString(Request.Form("logourl")), String.Format("{0}/(?<filename>\w+.(?:png|jpg|jpeg))$", logoContainerName))
				If m.Success Then
					Dim azureSrv As New CAzureStorageService(ConfigurationManager.AppSettings.Get("AZURE_MEDIA_ACCOUNT_NAME"), ConfigurationManager.AppSettings.Get("AZURE_MEDIA_ACCOUNT_KEY"))
					azureSrv.DeleteBlob(logoContainerName, m.Groups("filename").Value)
				End If
			Else
				Dim m As Match = Regex.Match(Common.SafeStripHtmlString(Request.Form("logourl")), "images/logos/(?<filename>\w+.(?:png|jpg|jpeg))$")
				If m.Success Then
					Dim logoFilePath As String = IO.Path.Combine(Server.MapPath("~/images"), "logos", m.Groups("filename").Value)
					If IO.File.Exists(logoFilePath) Then
						IO.File.Delete(logoFilePath)
					End If
				End If
			End If

			result = "done"
		Catch ex As Exception
			_log.Error("Error while deleting logo", ex)
			result = "error"
		End Try
		Response.Write(result)
		Response.End()
	End Sub

	Private Sub UploadFavIco()
		Dim fileUrl = ""
		Try
			If Request.Files.Count > 0 Then
				Dim file As HttpPostedFile = Request.Files(0)
				If file IsNot Nothing AndAlso file.ContentLength > 0 AndAlso (file.ContentType = "image/x-icon" Or file.ContentType = "image/png") Then
					Dim fileExt As String = New FileInfo(file.FileName).Extension
					Dim fileName As String = Guid.NewGuid().ToString().Replace("-", "").Substring(0, 10) & fileExt

					If Not String.IsNullOrWhiteSpace(ConfigurationManager.AppSettings.Get("AZURE_MEDIA_ACCOUNT_NAME")) Then
						Using reader = New BinaryReader(file.InputStream)
							Dim fileContent As Byte() = reader.ReadBytes(file.ContentLength)
							Dim azureSrv As New CAzureStorageService(ConfigurationManager.AppSettings.Get("AZURE_MEDIA_ACCOUNT_NAME"), ConfigurationManager.AppSettings.Get("AZURE_MEDIA_ACCOUNT_KEY"))
							Dim logoContainerName As String = ConfigurationManager.AppSettings.Get("AZURE_LOGO_CONTAINER")
							If String.IsNullOrWhiteSpace(logoContainerName) Then logoContainerName = "logos"
							fileUrl = azureSrv.UploadBlob(logoContainerName, fileName, fileContent, file.ContentType)
						End Using
					Else
						Dim logoFolderPath As String = IO.Path.Combine(Server.MapPath("~/images"), "logos")
						If Not Directory.Exists(logoFolderPath) Then
							Directory.CreateDirectory(logoFolderPath)
						End If
						file.SaveAs(Path.Combine(logoFolderPath, fileName))
						fileUrl = "/images/logos/" & fileName
					End If
				End If
			End If
		Catch ex As Exception
			_log.Error("Error while saving fav icon", ex)
		End Try
		Response.Write(fileUrl)
		Response.End()
	End Sub
	Private Sub DeleteFavIco()
		Dim result As String = ""
		Try
			'https://lpqhieplam.blob.core.windows.net/logo/8915a919bc.png
			If Not String.IsNullOrWhiteSpace(ConfigurationManager.AppSettings.Get("AZURE_MEDIA_ACCOUNT_NAME")) Then
				Dim logoContainerName As String = ConfigurationManager.AppSettings.Get("AZURE_LOGO_CONTAINER")
				If String.IsNullOrWhiteSpace(logoContainerName) Then logoContainerName = "logos"
				Dim m As Match = Regex.Match(Common.SafeStripHtmlString(Request.Form("logourl")), String.Format("{0}/(?<filename>\w+.(?:ico))$", logoContainerName))
				If m.Success Then
					Dim azureSrv As New CAzureStorageService(ConfigurationManager.AppSettings.Get("AZURE_MEDIA_ACCOUNT_NAME"), ConfigurationManager.AppSettings.Get("AZURE_MEDIA_ACCOUNT_KEY"))
					azureSrv.DeleteBlob(logoContainerName, m.Groups("filename").Value)
				End If
			Else

				Dim m As Match = Regex.Match(Common.SafeStripHtmlString(Request.Form("logourl")), "images/logos/(?<filename>\w+.(?:ico))$")
				If m.Success Then
					Dim logoFilePath As String = IO.Path.Combine(Server.MapPath("~/images"), "logos", m.Groups("filename").Value)
					If IO.File.Exists(logoFilePath) Then
						IO.File.Delete(logoFilePath)
					End If
				End If
			End If
			result = "done"
		Catch ex As Exception
			_log.Error("Error while deleting fav icon", ex)
			result = "error"
		End Try
		Response.Write(result)
		Response.End()
	End Sub

	Private Sub UploadAvatar()
		Dim fileUrl = ""
		Try
			If Request.Files.Count > 0 Then
				Dim file As HttpPostedFile = Request.Files(0)
				If file IsNot Nothing AndAlso file.ContentLength > 0 AndAlso (file.ContentType = "image/png" Or file.ContentType = "image/jpeg") Then
					Dim fileExt As String = New FileInfo(file.FileName).Extension
					Dim fileName As String = Guid.NewGuid().ToString().Replace("-", "").Substring(0, 10) & fileExt
					If Not String.IsNullOrWhiteSpace(ConfigurationManager.AppSettings.Get("AZURE_MEDIA_ACCOUNT_NAME")) Then
						Using reader = New BinaryReader(file.InputStream)
							Dim fileContent As Byte() = reader.ReadBytes(file.ContentLength)
							Dim azureSrv As New CAzureStorageService(ConfigurationManager.AppSettings.Get("AZURE_MEDIA_ACCOUNT_NAME"), ConfigurationManager.AppSettings.Get("AZURE_MEDIA_ACCOUNT_KEY"))
							Dim avatarContainerName As String = ConfigurationManager.AppSettings.Get("AZURE_AVATAR_CONTAINER")
							If String.IsNullOrWhiteSpace(avatarContainerName) Then avatarContainerName = "avatars"
							fileUrl = azureSrv.UploadBlob(avatarContainerName, fileName, fileContent, file.ContentType)
						End Using
					Else
						Dim avatarFolderPath As String = IO.Path.Combine(Server.MapPath("~/images"), "avatars")
						If Not Directory.Exists(avatarFolderPath) Then
							Directory.CreateDirectory(avatarFolderPath)
						End If
						file.SaveAs(Path.Combine(avatarFolderPath, fileName))
						fileUrl = "/images/avatars/" & fileName
					End If
				End If
			End If
		Catch ex As Exception
			_log.Error("Error while saving avatar", ex)
		End Try
		Response.Write(fileUrl)
		Response.End()
	End Sub
	Private Sub DeleteAvatar()
		Dim result As String = ""
		Try
			'https://lpqhieplam.blob.core.windows.net/avatars/8915a919bc.png
			If Not String.IsNullOrWhiteSpace(ConfigurationManager.AppSettings.Get("AZURE_MEDIA_ACCOUNT_NAME")) Then
				Dim avatarContainerName As String = ConfigurationManager.AppSettings.Get("AZURE_AVATAR_CONTAINER")
				If String.IsNullOrWhiteSpace(avatarContainerName) Then avatarContainerName = "avatars"
				Dim m As Match = Regex.Match(Common.SafeStripHtmlString(Request.Form("fileurl")), String.Format("{0}/(?<filename>\w+.(?:png|jpg|jpeg))$", avatarContainerName))
				If m.Success Then
					Dim azureSrv As New CAzureStorageService(ConfigurationManager.AppSettings.Get("AZURE_MEDIA_ACCOUNT_NAME"), ConfigurationManager.AppSettings.Get("AZURE_MEDIA_ACCOUNT_KEY"))
					azureSrv.DeleteBlob(avatarContainerName, m.Groups("filename").Value)
				End If
			Else
				Dim m As Match = Regex.Match(Common.SafeStripHtmlString(Request.Form("fileurl")), "images/avatars/(?<filename>\w+.(?:png|jpg|jpeg))$")
				If m.Success Then
					Dim logoFilePath As String = IO.Path.Combine(Server.MapPath("~/images"), "avatars", m.Groups("filename").Value)
					If IO.File.Exists(logoFilePath) Then
						IO.File.Delete(logoFilePath)
					End If
				End If
			End If
			result = "done"
		Catch ex As Exception
			_log.Error("Error while deleting avatar", ex)
			result = "error"
		End Try
		Response.Write(result)
		Response.End()
	End Sub

	Private Sub CleanConfigDraft()
		Dim res As CJsonResponse
		Try
			Dim smBL As New SmBL()
			Dim group As String = Common.SafeStripHtmlString(Request.Form("group"))
			Dim lenderConfigID As Guid = Common.SafeGUID(Request.Form("lenderconfigid"))
			smBL.CleanLenderConfigDrafts(lenderConfigID, UserInfo.UserID, CType([Enum].Parse(GetType(SmSettings.NodeGroup), group), SmSettings.NodeGroup))
			res = New CJsonResponse(True, "", "OK")
		Catch ex As Exception
			_log.Error("Could not ResetLoginFailureCount.", ex)
			res = New CJsonResponse(False, "Could not revert changes. Please try again", "FAILED")
		End Try
		Response.Write(JsonConvert.SerializeObject(res))
		Response.End()
	End Sub


	Private Sub CheckRevision()
		Dim res As CJsonResponse
		Try
			Dim smBL As New SmBL()
			Dim group As String = Common.SafeStripHtmlString(Request.Form("group"))
			Dim lenderConfigID As Guid = Common.SafeGUID(Request.Form("lenderconfigid"))
			Dim loadedRevisionID As Integer = Common.SafeInteger(Request.Form("loadedRevision"))
			Dim loadedDraftRevisionID As Integer = Common.SafeInteger(Request.Form("loadedDraftRevision"))
			Dim LenderConfig = smBL.GetLiveConfig(lenderConfigID)
			'Dim draftRevisionID As Integer = smBL.GetDraftRevisionID(CType([Enum].Parse(GetType(SmSettings.NodeGroup), group), SmSettings.NodeGroup), lenderConfigID, UserInfo.UserID)
			If IIf(loadedDraftRevisionID = 0, loadedRevisionID, loadedDraftRevisionID) <> LenderConfig.RevisionID Then
				If UserInfo.UserID = LenderConfig.LastModifiedByUserID Then
					'don't need to display this message bc what ever this user saved in previous revision doesn't include this part
					'res = New CJsonResponse(True, "", New With {.HasChanged = True, .Message = "You has changed the configuration since the last time you opened this page.", .ChangedBy = ""}) 'changed
					res = New CJsonResponse(True, "", New With {.HasChanged = False})
				Else
					res = New CJsonResponse(True, "", New With {.HasChanged = True, .Message = IIf(String.IsNullOrWhiteSpace(LenderConfig.LastModifiedByUserFullName), "Someone", LenderConfig.LastModifiedByUserFullName) & " has changed the configuration since the last time you opened.", .ChangedBy = LenderConfig.LastModifiedByUserFullName})	'changed
				End If
			Else
				res = New CJsonResponse(True, "", New With {.HasChanged = False})
			End If

		Catch ex As Exception
			_log.Error("Could not CheckRevision.", ex)
			res = New CJsonResponse(False, "Could not check revision. Please try again", True)
		End Try
		Response.Write(JsonConvert.SerializeObject(res))
		Response.End()
	End Sub
	Private Sub GetRevisionNumber()
		Dim res As CJsonResponse
		Try
			Dim smBL As New SmBL()
			Dim group As String = Common.SafeStripHtmlString(Request.Form("group"))
			Dim lenderConfigID As Guid = Common.SafeGUID(Request.Form("lenderconfigid"))
			Dim LenderConfig = smBL.GetLiveConfig(lenderConfigID)
			res = New CJsonResponse(True, "", New With {.RevisionID = LenderConfig.RevisionID, .DraftRevisionID = 0, .CreatedDate = Now})
		Catch ex As Exception
			_log.Error("Could not GetRevisionNumber.", ex)
			res = New CJsonResponse(False, "Could not get revision. Please try again", True)
		End Try
		Response.Write(JsonConvert.SerializeObject(res))
		Response.End()
	End Sub

	Private Sub RevertToRevision()
		Dim res As CJsonResponse
		Try
			Dim smBL As New SmBL()
			Dim lenderConfigLogID As Guid = Common.SafeGUID(Request.Form("lenderConfigLogId"))
			Dim comment As String = Common.SafeStripHtmlString(Request.Form("comment"))
			Dim ipAddress As String = Request.ServerVariables("REMOTE_ADDR")
			Dim result = smBL.RevertToRevision(lenderConfigLogID, comment, UserInfo.UserID, UserInfo.FullName, ipAddress)
			If result = "ok" Then
				res = New CJsonResponse(True, "", "OK")
			Else
				res = New CJsonResponse(False, "", "FAILED")
			End If
		Catch ex As Exception
			_log.Error("Could not GetRevisionNumber.", ex)
			res = New CJsonResponse(False, "Could not get revision. Please try again", True)
		End Try
		Response.Write(JsonConvert.SerializeObject(res))
		Response.End()
	End Sub

	Private Sub PreviewRevision()
		Dim res As CJsonResponse
		Try
			Dim smBL As New SmBL()
			Dim lenderConfigLogID As Guid = Common.SafeGUID(Request.Form("lenderConfigLogId"))
			Dim previewType As String = Common.SafeStripHtmlString(Request.Form("type"))
			Dim previewPage As String = ""
			Select Case previewType
				Case "cc"
					previewPage = "cc/creditcard.aspx"
				Case "cc_combo"
					previewPage = "cc/creditcard.aspx"
				Case "he"
					previewPage = "he/homeequityloan.aspx"
				Case "he_combo"
					previewPage = "he/homeequityloan.aspx"
				Case "pl"
					previewPage = "pl/personalloan.aspx"
				Case "pl_combo"
					previewPage = "pl/personalloan.aspx"
				Case "vl"
					previewPage = "vl/vehicleloan.aspx"
				Case "vl_combo"
					previewPage = "vl/vehicleloan.aspx"
				Case Else
					previewPage = "xa/xpressapp.aspx"
			End Select
			Dim result = smBL.GeneratePreviewConfig(lenderConfigLogID, Function(config, code) String.Format("{0}/{1}?lenderref={2}&autofill=true&previewCode={3}{4}", _serverRoot, previewPage, config.LenderRef, code, IIf(previewType.EndsWith("combo"), "&type=1", IIf(previewType = "sa", "&type=2", ""))))
			res = New CJsonResponse(True, "", New With {.previewurl = result})
		Catch ex As Exception
			_log.Error("Could not PreviewRevision.", ex)
			res = New CJsonResponse(False, "Could not generate preview code. Please try again", True)
		End Try
		Response.Write(JsonConvert.SerializeObject(res))
		Response.End()
	End Sub

	Private Sub ShowChangesInRevision()
		Try
			Dim control As Sm_Uc_RevisionChanges = CType(LoadControl("~/Sm/Uc/RevisionChanges.ascx"), Sm_Uc_RevisionChanges)
			Dim lenderConfigLogId As Guid = Common.SafeGUID(Request.Form("lenderConfigLogId"))
			control.LenderConfigLogId = lenderConfigLogId
			Me.Controls.Add(control)
		Catch ex As Exception
			_log.Error("Could not load changes in revision", ex)
			Response.Write("<h3 style='text-align: center; color: red;'>Unable to render content. Please try again later</h3>")
		End Try
	End Sub


	Private Sub ClearCache()
		Dim res As CJsonResponse
		Try
			Dim smBL As New SmBL()
			Dim lenderConfigID As Guid = Common.SafeGUID(Request.Form("lenderconfigid"))
			Dim oLenderConfig = smBL.GetLiveConfig(lenderConfigID)
			If oLenderConfig Is Nothing Then
				Response.StatusCode = 404 '' Forbidden
				Response.StatusDescription = "Not found"
				Response.End() '' Terminate all the rest processes
			End If
			Dim appServiceDomains = ConfigurationManager.AppSettings.Get("AZURE_APP_SERVICE_DOMAINS")
			Dim appServiceDomainList As New List(Of String)
			If Not String.IsNullOrWhiteSpace(appServiceDomains) Then
				appServiceDomainList = appServiceDomains.Split(","c).Where(Function(p) Not String.IsNullOrWhiteSpace(p)).Distinct().ToList()
			End If
			If appServiceDomainList.Count > 1 Then
				Dim idx As Integer = 0
				For Each domain As String In appServiceDomainList
					Try
						Dim req = WebRequest.Create(String.Format("{0}://{1}/handler/recycleCache.aspx?suffix={2}{3}", Request.Url.Scheme, domain, oLenderConfig.CacheIDSuffix, IIf(idx = 0, "&recycledb=y", "")))
						req.GetResponse()
					Catch ex As Exception
						_log.Warn(ex)
					End Try
					idx += 1

				Next
			Else
				CDownloadedSettings.Recycle(oLenderConfig.CacheIDSuffix)
			End If
			res = New CJsonResponse(True, "Done", "OK")
		Catch ex As Exception
			res = New CJsonResponse(False, "Error", "FAILED")
			_log.Error("Could not ClearCache", ex)
		End Try
		Response.Write(JsonConvert.SerializeObject(res))
		Response.End()
	End Sub

	Private Sub ClearPDF()
		'call CrossDomainServices/PDFFilesRefresh on another remote server
		Dim res As CJsonResponse
		Try
			Dim smBL As New SmBL()
			Dim lenderConfigID As Guid = Common.SafeGUID(Request.Form("lenderconfigid"))
			Dim oLenderConfig = smBL.GetLiveConfig(lenderConfigID)
			If oLenderConfig Is Nothing Then
				Response.StatusCode = 404 '' Forbidden
				Response.StatusDescription = "Not found"
				Response.End() '' Terminate all the rest processes
			End If
			Dim sUrl As String = ConfigurationManager.AppSettings.Get("CrossDomainServicesUrl")
			Dim sFile As String = String.Format(sUrl & "PDFFilesRefresh.aspx?lenderref={0}", oLenderConfig.LenderRef)

			Dim oRequest As System.Net.HttpWebRequest = CType(System.Net.WebRequest.Create(sFile), System.Net.HttpWebRequest)
			oRequest.Method = "GET"

			_log.Info("CLearPDF request to: " & sFile)
			Dim oResponse As System.Net.HttpWebResponse = CType(oRequest.GetResponse(), System.Net.HttpWebResponse)

			If oResponse.StatusCode = "200" Then
				_log.Info("CLearPDF executed successfully")
			End If

			If oResponse.StatusCode <> "200" Then
				_log.Warn("CLearPDF: Request NOT successful!  Status code: " & oResponse.StatusCode)
			End If

			res = New CJsonResponse(True, "Done", "OK")
		Catch ex As Exception
			res = New CJsonResponse(False, "Error", "FAILED")
			_log.Error("Could not ClearPDF", ex)
		End Try
		Response.Write(JsonConvert.SerializeObject(res))
		Response.End()

	End Sub


#Region "XA"
	Private Sub SaveManageXADraft()
		Dim res As New CJsonResponse()
		Try
			Dim smBL As New SmBL()
			Dim oSerializer As New JavaScriptSerializer()
			Dim approvalMsg As String = Common.SafeString(Request.Form("approvalMsg"))
			Dim referredMsg As String = Common.SafeString(Request.Form("referredMsg"))
			Dim eligibilityMsg As String = Common.SafeString(Request.Form("eligibilityMsg"))

			'Dim declinedMsg As String = Common.SafeString(Request.Form("declinedMsg"))
			Dim additionalDisclosures As List(Of String) = oSerializer.Deserialize(Of List(Of String))(Request.Form("additionalDisclosures"))
			Dim fundingDisclosure As String = Common.SafeString(Request.Form("fundingDisclosure"))
			Dim lenderConfigID As Guid = Common.SafeGUID(Request.Form("lenderconfigid"))
			Dim draftRecordList As New List(Of SmLenderConfigDraft)
			If Not String.IsNullOrEmpty(approvalMsg) Then
				draftRecordList.Add(New SmLenderConfigDraft() With {
					.LenderConfigID = lenderConfigID,
					.CreatedByUserID = UserInfo.UserID,
					.NodeGroup = SmSettings.NodeGroup.ManageXA,
					.NodePath = PREAPPROVED_MESSAGE("XA_LOAN"),
					.NodeContent = SmUtil.BuildSuccessMessageXml("SUCCESS_MESSAGES", "PREAPPROVED_MESSAGE", approvalMsg)})
			End If
			If Not String.IsNullOrEmpty(referredMsg) Then
				draftRecordList.Add(New SmLenderConfigDraft() With {
				.LenderConfigID = lenderConfigID,
				.CreatedByUserID = UserInfo.UserID,
				.NodePath = SUBMITTED_MESSAGE("XA_LOAN"),
				.NodeGroup = SmSettings.NodeGroup.ManageXA,
				.NodeContent = SmUtil.BuildSuccessMessageXml("SUCCESS_MESSAGES", "SUBMITTED_MESSAGE", referredMsg)})
			End If

			draftRecordList.Add(New SmLenderConfigDraft() With {
			.LenderConfigID = lenderConfigID,
			.CreatedByUserID = UserInfo.UserID,
			.NodePath = ELIGIBILITY_MESSAGE("XA_LOAN"),
			.NodeGroup = SmSettings.NodeGroup.ManageXA,
			.NodeContent = SmUtil.BuildContentNodeXml("ELIGIBILITY_MESSAGE", eligibilityMsg)})

			draftRecordList.Add(New SmLenderConfigDraft() With {
			.LenderConfigID = lenderConfigID,
			.CreatedByUserID = UserInfo.UserID,
			.NodePath = FUNDING_DISCLOSURE("XA_LOAN"),
			.NodeGroup = SmSettings.NodeGroup.ManageXA,
			.NodeContent = SmUtil.BuildContentNodeXml("FUNDING_DISCLOSURE", fundingDisclosure)})
			'If Not String.IsNullOrEmpty(declinedMsg) Then
			'	draftRecordList.Add(New SmLenderConfigDraft() With {
			'		.LenderConfigID = lenderConfigID,
			'		
			'		.CreatedBy = UserInfo.Email,
			'		.NodePath = DECLINED_MESSAGE("XA_LOAN"),
			'		.NodeGroup = SmSettings.NodeGroup.ManageXA,
			'		.NodeContent = SmUtil.BuildSuccessMessageXml("SUCCESS_MESSAGES", "DECLINED_MESSAGE", declinedMsg)})
			'End If
			draftRecordList.Add(New SmLenderConfigDraft() With {
			.LenderConfigID = lenderConfigID,
			.CreatedByUserID = UserInfo.UserID,
			.NodePath = Sm.DISCLOSURES("XA_LOAN"),
			.NodeGroup = SmSettings.NodeGroup.ManageXA,
			.NodeContent = SmUtil.BuildDisclosuresXml(additionalDisclosures)})
			Dim revisionID As Integer = Common.SafeInteger(Request.Form("revisionID"))
			Dim result = smBL.SaveNodesToDraft(revisionID, draftRecordList.ToArray())
			If result Then
				res = New CJsonResponse(True, "", "OK")
			Else
				res = New CJsonResponse(False, "", "FAILED")
			End If
		Catch ex As Exception
			_log.Error("Could not SaveManageXADraft.", ex)
			res = New CJsonResponse(False, "Unable to save draft. Please try again", "FAILED")
		End Try
		Response.Write(JsonConvert.SerializeObject(res))
		Response.End()
	End Sub
	Private Sub PreviewManageXADraft()
		Dim res As New CJsonResponse()
		Try
			Dim smBL As New SmBL()
			Dim oSerializer As New JavaScriptSerializer()
			Dim approvalMsg As String = Common.SafeString(Request.Form("approvalMsg"))
			Dim referredMsg As String = Common.SafeString(Request.Form("referredMsg"))
			Dim eligibilityMsg As String = Common.SafeString(Request.Form("eligibilityMsg"))
			'Dim declinedMsg As String = Common.SafeString(Request.Form("declinedMsg"))
			Dim additionalDisclosures As List(Of String) = oSerializer.Deserialize(Of List(Of String))(Request.Form("additionalDisclosures"))
			Dim fundingDisclosure As String = Common.SafeString(Request.Form("fundingDisclosure"))
			Dim lenderConfigID As Guid = Common.SafeGUID(Request.Form("lenderconfigid"))
			Dim draftRecordList As New List(Of SmLenderConfigDraft)
			If Not String.IsNullOrEmpty(approvalMsg) Then
				draftRecordList.Add(New SmLenderConfigDraft() With {
					.LenderConfigID = lenderConfigID,
					.CreatedByUserID = UserInfo.UserID,
					.NodePath = PREAPPROVED_MESSAGE("XA_LOAN"),
					.NodeGroup = SmSettings.NodeGroup.ManageXA,
					.NodeContent = SmUtil.BuildSuccessMessageXml("SUCCESS_MESSAGES", "PREAPPROVED_MESSAGE", approvalMsg)})
			End If
			If Not String.IsNullOrEmpty(referredMsg) Then
				draftRecordList.Add(New SmLenderConfigDraft() With {
				.LenderConfigID = lenderConfigID,
				.CreatedByUserID = UserInfo.UserID,
				.NodePath = SUBMITTED_MESSAGE("XA_LOAN"),
				.NodeGroup = SmSettings.NodeGroup.ManageXA,
				.NodeContent = SmUtil.BuildSuccessMessageXml("SUCCESS_MESSAGES", "SUBMITTED_MESSAGE", referredMsg)})
			End If
			draftRecordList.Add(New SmLenderConfigDraft() With {
			.LenderConfigID = lenderConfigID,
			.CreatedByUserID = UserInfo.UserID,
			.NodePath = ELIGIBILITY_MESSAGE("XA_LOAN"),
			.NodeGroup = SmSettings.NodeGroup.ManageXA,
			.NodeContent = SmUtil.BuildContentNodeXml("ELIGIBILITY_MESSAGE", eligibilityMsg)})
			draftRecordList.Add(New SmLenderConfigDraft() With {
			.LenderConfigID = lenderConfigID,
			.CreatedByUserID = UserInfo.UserID,
			.NodePath = FUNDING_DISCLOSURE("XA_LOAN"),
			.NodeGroup = SmSettings.NodeGroup.ManageXA,
			.NodeContent = SmUtil.BuildContentNodeXml("FUNDING_DISCLOSURE", fundingDisclosure)})
			'If Not String.IsNullOrEmpty(declinedMsg) Then
			'	draftRecordList.Add(New SmLenderConfigDraft() With {
			'		.LenderConfigID = lenderConfigID,
			'		
			'		.CreatedBy = UserInfo.Email,
			'		.NodePath = DECLINED_MESSAGE("XA_LOAN"),
			'		.NodeGroup = SmSettings.NodeGroup.ManageXA,
			'		.NodeContent = SmUtil.BuildSuccessMessageXml("SUCCESS_MESSAGES", "DECLINED_MESSAGE", declinedMsg)})
			'End If
			draftRecordList.Add(New SmLenderConfigDraft() With {
			.LenderConfigID = lenderConfigID,
			.CreatedByUserID = UserInfo.UserID,
			.NodePath = Sm.DISCLOSURES("XA_LOAN"),
			.NodeGroup = SmSettings.NodeGroup.ManageXA,
			.NodeContent = SmUtil.BuildDisclosuresXml(additionalDisclosures)})
			Dim revisionID As Integer = Common.SafeInteger(Request.Form("revisionID"))
			Dim result = smBL.SaveNodesToDraft(revisionID, draftRecordList.ToArray())
			If result Then
				Dim previewUrl As String = smBL.GeneratePreviewConfig(lenderConfigID, UserInfo.Email, Function(config, code) String.Format("{0}/xa/xpressApp.aspx?lenderref={1}&autofill=true&previewCode={2}", _serverRoot, config.LenderRef, code), draftRecordList.ToArray())
				res = New CJsonResponse(True, "", "OK")
				res.Info = New With {.previewurl = previewUrl}
			Else
			End If
		Catch ex As Exception
			_log.Error("Could not PreviewManageXADraft.", ex)
			res = New CJsonResponse(False, "Unable to save and preview draft. Please try again", "FAILED")
		End Try
		Response.Write(JsonConvert.SerializeObject(res))
		Response.End()
	End Sub
	Private Sub PublishManageXADraft()
		Dim res As New CJsonResponse()
		Try
			Dim smBL As New SmBL()
			Dim oSerializer As New JavaScriptSerializer()
			Dim approvalMsg As String = Common.SafeString(Request.Form("approvalMsg"))
			Dim referredMsg As String = Common.SafeString(Request.Form("referredMsg"))
			Dim eligibilityMsg As String = Common.SafeString(Request.Form("eligibilityMsg"))
			'Dim declinedMsg As String = Common.SafeString(Request.Form("declinedMsg"))
			Dim additionalDisclosures As List(Of String) = oSerializer.Deserialize(Of List(Of String))(Request.Form("additionalDisclosures"))
			Dim fundingDisclosure As String = Common.SafeString(Request.Form("fundingDisclosure"))
			Dim lenderConfigID As Guid = Common.SafeGUID(Request.Form("lenderconfigid"))
			Dim draftRecordList As New List(Of SmLenderConfigDraft)
			If Not String.IsNullOrEmpty(approvalMsg) Then
				draftRecordList.Add(New SmLenderConfigDraft() With {
					.LenderConfigID = lenderConfigID,
					.CreatedByUserID = UserInfo.UserID,
					.NodePath = PREAPPROVED_MESSAGE("XA_LOAN"),
					.NodeGroup = SmSettings.NodeGroup.ManageXA,
					.NodeContent = SmUtil.BuildSuccessMessageXml("SUCCESS_MESSAGES", "PREAPPROVED_MESSAGE", approvalMsg)})
			End If
			If Not String.IsNullOrEmpty(referredMsg) Then
				draftRecordList.Add(New SmLenderConfigDraft() With {
				.LenderConfigID = lenderConfigID,
				.CreatedByUserID = UserInfo.UserID,
				.NodePath = SUBMITTED_MESSAGE("XA_LOAN"),
				.NodeGroup = SmSettings.NodeGroup.ManageXA,
				.NodeContent = SmUtil.BuildSuccessMessageXml("SUCCESS_MESSAGES", "SUBMITTED_MESSAGE", referredMsg)})
			End If
			draftRecordList.Add(New SmLenderConfigDraft() With {
			.LenderConfigID = lenderConfigID,
			.CreatedByUserID = UserInfo.UserID,
			.NodePath = ELIGIBILITY_MESSAGE("XA_LOAN"),
			.NodeGroup = SmSettings.NodeGroup.ManageXA,
			.NodeContent = SmUtil.BuildContentNodeXml("ELIGIBILITY_MESSAGE", eligibilityMsg)})
			draftRecordList.Add(New SmLenderConfigDraft() With {
			.LenderConfigID = lenderConfigID,
			.CreatedByUserID = UserInfo.UserID,
			.NodePath = FUNDING_DISCLOSURE("XA_LOAN"),
			.NodeGroup = SmSettings.NodeGroup.ManageXA,
			.NodeContent = SmUtil.BuildContentNodeXml("FUNDING_DISCLOSURE", fundingDisclosure)})
			'If Not String.IsNullOrEmpty(declinedMsg) Then
			'	draftRecordList.Add(New SmLenderConfigDraft() With {
			'		.LenderConfigID = lenderConfigID,
			'		
			'		.CreatedBy = UserInfo.Email,
			'		.NodePath = DECLINED_MESSAGE("XA_LOAN"),
			'		.NodeGroup = SmSettings.NodeGroup.ManageXA,
			'		.NodeContent = SmUtil.BuildSuccessMessageXml("SUCCESS_MESSAGES", "DECLINED_MESSAGE", declinedMsg)})
			'End If
			draftRecordList.Add(New SmLenderConfigDraft() With {
			.LenderConfigID = lenderConfigID,
			.CreatedByUserID = UserInfo.UserID,
			.NodePath = Sm.DISCLOSURES("XA_LOAN"),
			.NodeGroup = SmSettings.NodeGroup.ManageXA,
			.NodeContent = SmUtil.BuildDisclosuresXml(additionalDisclosures)})

			Dim comment As String = Common.SafeStripHtmlString(Request.Form("comment"))
			Dim publishAll As Boolean = Common.SafeBoolean(Request.Form("publishAll"))
			Dim result = smBL.PublishNodes(lenderConfigID, UserInfo.UserID, Request.UserHostAddress, comment, publishAll, draftRecordList.ToArray())
			If result Then
				res = New CJsonResponse(True, "", "OK")
			Else
				res = New CJsonResponse(False, "", "FAILED")
			End If
		Catch ex As Exception
			_log.Error("Could not PublishManageXADraft.", ex)
			res = New CJsonResponse(False, "Unable to publish data. Please try again", "FAILED")
		End Try
		Response.Write(JsonConvert.SerializeObject(res))
		Response.End()
	End Sub
#End Region
#Region "XA SPECIAL"
	Private Sub SaveManageSpecialXADraft()
		Dim res As New CJsonResponse()
		Try
			Dim smBL As New SmBL()
			Dim oSerializer As New JavaScriptSerializer()
			Dim approvalMsg As String = Common.SafeString(Request.Form("approvalMsg"))
			Dim referredMsg As String = Common.SafeString(Request.Form("referredMsg"))
			Dim eligibilityMsg As String = Common.SafeString(Request.Form("eligibilityMsg"))

			Dim additionalDisclosures As List(Of String) = oSerializer.Deserialize(Of List(Of String))(Request.Form("additionalDisclosures"))
			Dim fundingDisclosure As String = Common.SafeString(Request.Form("fundingDisclosure"))
			Dim lenderConfigID As Guid = Common.SafeGUID(Request.Form("lenderconfigid"))
			Dim draftRecordList As New List(Of SmLenderConfigDraft)
			If Not String.IsNullOrEmpty(approvalMsg) Then
				draftRecordList.Add(New SmLenderConfigDraft() With {
					.LenderConfigID = lenderConfigID,
					.CreatedByUserID = UserInfo.UserID,
					.NodeGroup = SmSettings.NodeGroup.ManageSpecialXA,
					.NodePath = XA_SPECIAL_ACCOUNT_PREAPPROVED_MESSAGE(),
					.NodeContent = SmUtil.BuildSuccessMessageXml("SPECIAL_ACCOUNT_SUCCESS_MESSAGES", "PREAPPROVED_MESSAGE", approvalMsg)})
			End If
			If Not String.IsNullOrEmpty(referredMsg) Then
				draftRecordList.Add(New SmLenderConfigDraft() With {
				.LenderConfigID = lenderConfigID,
				.CreatedByUserID = UserInfo.UserID,
				.NodePath = XA_SPECIAL_ACCOUNT_SUBMITTED_MESSAGE(),
				.NodeGroup = SmSettings.NodeGroup.ManageSpecialXA,
				.NodeContent = SmUtil.BuildSuccessMessageXml("SPECIAL_ACCOUNT_SUCCESS_MESSAGES", "SUBMITTED_MESSAGE", referredMsg)})
			End If

			If Not String.IsNullOrWhiteSpace(eligibilityMsg) Then
				draftRecordList.Add(New SmLenderConfigDraft() With {
				.LenderConfigID = lenderConfigID,
				.CreatedByUserID = UserInfo.UserID,
				.NodePath = XA_SPECIAL_ACCOUNT_ELIGIBILITY_MESSAGE(),
				.NodeGroup = SmSettings.NodeGroup.ManageSpecialXA,
				.NodeContent = SmUtil.BuildContentNodeXml("SPECIAL_ACCOUNT_ELIGIBILITY_MESSAGE", eligibilityMsg)})
			End If

			draftRecordList.Add(New SmLenderConfigDraft() With {
			.LenderConfigID = lenderConfigID,
			.CreatedByUserID = UserInfo.UserID,
			.NodePath = XA_SPECIAL_ACCOUNT_FUNDING_DISCLOSURE(),
			.NodeGroup = SmSettings.NodeGroup.ManageSpecialXA,
			.NodeContent = SmUtil.BuildContentNodeXml("SPECIAL_ACCOUNT_FUNDING_DISCLOSURE", fundingDisclosure)})

			draftRecordList.Add(New SmLenderConfigDraft() With {
			.LenderConfigID = lenderConfigID,
			.CreatedByUserID = UserInfo.UserID,
			.NodePath = XA_SPECIAL_ACCOUNT_DISCLOSURES(),
			.NodeGroup = SmSettings.NodeGroup.ManageSpecialXA,
			.NodeContent = SmUtil.BuildDisclosuresXml(additionalDisclosures, "SPECIAL_ACCOUNT_DISCLOSURES")})
			Dim revisionID As Integer = Common.SafeInteger(Request.Form("revisionID"))
			Dim result = smBL.SaveNodesToDraft(revisionID, draftRecordList.ToArray())
			If result Then
				res = New CJsonResponse(True, "", "OK")
			Else
				res = New CJsonResponse(False, "", "FAILED")
			End If
		Catch ex As Exception
			_log.Error("Could not SaveManageSpecialXADraft.", ex)
			res = New CJsonResponse(False, "Unable to save draft. Please try again", "FAILED")
		End Try
		Response.Write(JsonConvert.SerializeObject(res))
		Response.End()
	End Sub
	Private Sub PreviewManageSpecialXADraft()
		Dim res As New CJsonResponse()
		Try
			Dim smBL As New SmBL()
			Dim oSerializer As New JavaScriptSerializer()
			Dim approvalMsg As String = Common.SafeString(Request.Form("approvalMsg"))
			Dim referredMsg As String = Common.SafeString(Request.Form("referredMsg"))
			Dim eligibilityMsg As String = Common.SafeString(Request.Form("eligibilityMsg"))
			Dim additionalDisclosures As List(Of String) = oSerializer.Deserialize(Of List(Of String))(Request.Form("additionalDisclosures"))
			Dim fundingDisclosure As String = Common.SafeString(Request.Form("fundingDisclosure"))
			Dim lenderConfigID As Guid = Common.SafeGUID(Request.Form("lenderconfigid"))
			Dim draftRecordList As New List(Of SmLenderConfigDraft)
			If Not String.IsNullOrEmpty(approvalMsg) Then
				draftRecordList.Add(New SmLenderConfigDraft() With {
					.LenderConfigID = lenderConfigID,
					.CreatedByUserID = UserInfo.UserID,
					.NodeGroup = SmSettings.NodeGroup.ManageSpecialXA,
					.NodePath = XA_SPECIAL_ACCOUNT_PREAPPROVED_MESSAGE(),
					.NodeContent = SmUtil.BuildSuccessMessageXml("SPECIAL_ACCOUNT_SUCCESS_MESSAGES", "PREAPPROVED_MESSAGE", approvalMsg)})
			End If
			If Not String.IsNullOrEmpty(referredMsg) Then
				draftRecordList.Add(New SmLenderConfigDraft() With {
				.LenderConfigID = lenderConfigID,
				.CreatedByUserID = UserInfo.UserID,
				.NodePath = XA_SPECIAL_ACCOUNT_SUBMITTED_MESSAGE(),
				.NodeGroup = SmSettings.NodeGroup.ManageSpecialXA,
				.NodeContent = SmUtil.BuildSuccessMessageXml("SPECIAL_ACCOUNT_SUCCESS_MESSAGES", "SUBMITTED_MESSAGE", referredMsg)})
			End If

			If Not String.IsNullOrWhiteSpace(eligibilityMsg) Then
				draftRecordList.Add(New SmLenderConfigDraft() With {
				.LenderConfigID = lenderConfigID,
				.CreatedByUserID = UserInfo.UserID,
				.NodePath = XA_SPECIAL_ACCOUNT_ELIGIBILITY_MESSAGE(),
				.NodeGroup = SmSettings.NodeGroup.ManageSpecialXA,
				.NodeContent = SmUtil.BuildContentNodeXml("SPECIAL_ACCOUNT_ELIGIBILITY_MESSAGE", eligibilityMsg)})
			End If
			draftRecordList.Add(New SmLenderConfigDraft() With {
			.LenderConfigID = lenderConfigID,
			.CreatedByUserID = UserInfo.UserID,
			.NodePath = XA_SPECIAL_ACCOUNT_FUNDING_DISCLOSURE(),
			.NodeGroup = SmSettings.NodeGroup.ManageSpecialXA,
			.NodeContent = SmUtil.BuildContentNodeXml("SPECIAL_ACCOUNT_FUNDING_DISCLOSURE", fundingDisclosure)})

			draftRecordList.Add(New SmLenderConfigDraft() With {
			.LenderConfigID = lenderConfigID,
			.CreatedByUserID = UserInfo.UserID,
			.NodePath = XA_SPECIAL_ACCOUNT_DISCLOSURES(),
			.NodeGroup = SmSettings.NodeGroup.ManageSpecialXA,
			.NodeContent = SmUtil.BuildDisclosuresXml(additionalDisclosures, "SPECIAL_ACCOUNT_DISCLOSURES")})

			Dim revisionID As Integer = Common.SafeInteger(Request.Form("revisionID"))
			Dim result = smBL.SaveNodesToDraft(revisionID, draftRecordList.ToArray())
			If result Then
				Dim previewUrl As String = smBL.GeneratePreviewConfig(lenderConfigID, UserInfo.Email, Function(config, code) String.Format("{0}/apply.aspx?lenderref={1}&list=1s&autofill=true&previewCode={2}", _serverRoot, config.LenderRef, code), draftRecordList.ToArray())
				res = New CJsonResponse(True, "", "OK")
				res.Info = New With {.previewurl = previewUrl}
			Else
			End If
		Catch ex As Exception
			_log.Error("Could not PreviewManageSpecialXADraft.", ex)
			res = New CJsonResponse(False, "Unable to save and preview draft. Please try again", "FAILED")
		End Try
		Response.Write(JsonConvert.SerializeObject(res))
		Response.End()
	End Sub
	Private Sub PublishManageSpecialXADraft()
		Dim res As New CJsonResponse()
		Try
			Dim smBL As New SmBL()
			Dim oSerializer As New JavaScriptSerializer()
			Dim approvalMsg As String = Common.SafeString(Request.Form("approvalMsg"))
			Dim referredMsg As String = Common.SafeString(Request.Form("referredMsg"))
			Dim eligibilityMsg As String = Common.SafeString(Request.Form("eligibilityMsg"))
			Dim additionalDisclosures As List(Of String) = oSerializer.Deserialize(Of List(Of String))(Request.Form("additionalDisclosures"))
			Dim fundingDisclosure As String = Common.SafeString(Request.Form("fundingDisclosure"))
			Dim lenderConfigID As Guid = Common.SafeGUID(Request.Form("lenderconfigid"))
			Dim draftRecordList As New List(Of SmLenderConfigDraft)
			If Not String.IsNullOrEmpty(approvalMsg) Then
				draftRecordList.Add(New SmLenderConfigDraft() With {
					.LenderConfigID = lenderConfigID,
					.CreatedByUserID = UserInfo.UserID,
					.NodeGroup = SmSettings.NodeGroup.ManageSpecialXA,
					.NodePath = XA_SPECIAL_ACCOUNT_PREAPPROVED_MESSAGE(),
					.NodeContent = SmUtil.BuildSuccessMessageXml("SPECIAL_ACCOUNT_SUCCESS_MESSAGES", "PREAPPROVED_MESSAGE", approvalMsg)})
			End If
			If Not String.IsNullOrEmpty(referredMsg) Then
				draftRecordList.Add(New SmLenderConfigDraft() With {
				.LenderConfigID = lenderConfigID,
				.CreatedByUserID = UserInfo.UserID,
				.NodePath = XA_SPECIAL_ACCOUNT_SUBMITTED_MESSAGE(),
				.NodeGroup = SmSettings.NodeGroup.ManageSpecialXA,
				.NodeContent = SmUtil.BuildSuccessMessageXml("SPECIAL_ACCOUNT_SUCCESS_MESSAGES", "SUBMITTED_MESSAGE", referredMsg)})
			End If

			If Not String.IsNullOrWhiteSpace(eligibilityMsg) Then
				draftRecordList.Add(New SmLenderConfigDraft() With {
				.LenderConfigID = lenderConfigID,
				.CreatedByUserID = UserInfo.UserID,
				.NodePath = XA_SPECIAL_ACCOUNT_ELIGIBILITY_MESSAGE(),
				.NodeGroup = SmSettings.NodeGroup.ManageSpecialXA,
				.NodeContent = SmUtil.BuildContentNodeXml("SPECIAL_ACCOUNT_ELIGIBILITY_MESSAGE", eligibilityMsg)})
			End If
			draftRecordList.Add(New SmLenderConfigDraft() With {
			.LenderConfigID = lenderConfigID,
			.CreatedByUserID = UserInfo.UserID,
			.NodePath = XA_SPECIAL_ACCOUNT_FUNDING_DISCLOSURE(),
			.NodeGroup = SmSettings.NodeGroup.ManageSpecialXA,
			.NodeContent = SmUtil.BuildContentNodeXml("SPECIAL_ACCOUNT_FUNDING_DISCLOSURE", fundingDisclosure)})

			draftRecordList.Add(New SmLenderConfigDraft() With {
			.LenderConfigID = lenderConfigID,
			.CreatedByUserID = UserInfo.UserID,
			.NodePath = XA_SPECIAL_ACCOUNT_DISCLOSURES(),
			.NodeGroup = SmSettings.NodeGroup.ManageSpecialXA,
			.NodeContent = SmUtil.BuildDisclosuresXml(additionalDisclosures, "SPECIAL_ACCOUNT_DISCLOSURES")})

			Dim comment As String = Common.SafeStripHtmlString(Request.Form("comment"))
			Dim publishAll As Boolean = Common.SafeBoolean(Request.Form("publishAll"))
			Dim result = smBL.PublishNodes(lenderConfigID, UserInfo.UserID, Request.UserHostAddress, comment, publishAll, draftRecordList.ToArray())
			If result Then
				res = New CJsonResponse(True, "", "OK")
			Else
				res = New CJsonResponse(False, "", "FAILED")
			End If
		Catch ex As Exception
			_log.Error("Could not PublishManageSpecialXADraft.", ex)
			res = New CJsonResponse(False, "Unable to publish data. Please try again", "FAILED")
		End Try
		Response.Write(JsonConvert.SerializeObject(res))
		Response.End()
	End Sub
#End Region
#Region "XA BUSINESS"
	Private Sub SaveManageBusinessXADraft()
		Dim res As New CJsonResponse()
		Try
			Dim smBL As New SmBL()
			Dim oSerializer As New JavaScriptSerializer()
			Dim approvalMsg As String = Common.SafeString(Request.Form("approvalMsg"))
			Dim referredMsg As String = Common.SafeString(Request.Form("referredMsg"))
			Dim eligibilityMsg As String = Common.SafeString(Request.Form("eligibilityMsg"))

			Dim additionalDisclosures As List(Of String) = oSerializer.Deserialize(Of List(Of String))(Request.Form("additionalDisclosures"))
			Dim fundingDisclosure As String = Common.SafeString(Request.Form("fundingDisclosure"))
			Dim lenderConfigID As Guid = Common.SafeGUID(Request.Form("lenderconfigid"))
			Dim draftRecordList As New List(Of SmLenderConfigDraft)
			If Not String.IsNullOrEmpty(approvalMsg) Then
				draftRecordList.Add(New SmLenderConfigDraft() With {
					.LenderConfigID = lenderConfigID,
					.CreatedByUserID = UserInfo.UserID,
					.NodeGroup = SmSettings.NodeGroup.ManageBusinessXA,
					.NodePath = XA_BUSINESS_ACCOUNT_PREAPPROVED_MESSAGE(),
					.NodeContent = SmUtil.BuildSuccessMessageXml("BUSINESS_ACCOUNT_SUCCESS_MESSAGES", "PREAPPROVED_MESSAGE", approvalMsg)})
			End If
			If Not String.IsNullOrEmpty(referredMsg) Then
				draftRecordList.Add(New SmLenderConfigDraft() With {
				.LenderConfigID = lenderConfigID,
				.CreatedByUserID = UserInfo.UserID,
				.NodePath = XA_BUSINESS_ACCOUNT_SUBMITTED_MESSAGE(),
				.NodeGroup = SmSettings.NodeGroup.ManageBusinessXA,
				.NodeContent = SmUtil.BuildSuccessMessageXml("BUSINESS_ACCOUNT_SUCCESS_MESSAGES", "SUBMITTED_MESSAGE", referredMsg)})
			End If

			If Not String.IsNullOrWhiteSpace(eligibilityMsg) Then
				draftRecordList.Add(New SmLenderConfigDraft() With {
				.LenderConfigID = lenderConfigID,
				.CreatedByUserID = UserInfo.UserID,
				.NodePath = XA_BUSINESS_ACCOUNT_ELIGIBILITY_MESSAGE(),
				.NodeGroup = SmSettings.NodeGroup.ManageBusinessXA,
				.NodeContent = SmUtil.BuildContentNodeXml("BUSINESS_ACCOUNT_ELIGIBILITY_MESSAGE", eligibilityMsg)})
			End If

			draftRecordList.Add(New SmLenderConfigDraft() With {
			.LenderConfigID = lenderConfigID,
			.CreatedByUserID = UserInfo.UserID,
			.NodePath = XA_BUSINESS_ACCOUNT_FUNDING_DISCLOSURE(),
			.NodeGroup = SmSettings.NodeGroup.ManageBusinessXA,
			.NodeContent = SmUtil.BuildContentNodeXml("BUSINESS_ACCOUNT_FUNDING_DISCLOSURE", fundingDisclosure)})

			draftRecordList.Add(New SmLenderConfigDraft() With {
			.LenderConfigID = lenderConfigID,
			.CreatedByUserID = UserInfo.UserID,
			.NodePath = XA_BUSINESS_ACCOUNT_DISCLOSURES(),
			.NodeGroup = SmSettings.NodeGroup.ManageBusinessXA,
			.NodeContent = SmUtil.BuildDisclosuresXml(additionalDisclosures, "BUSINESS_ACCOUNT_DISCLOSURES")})
			Dim revisionID As Integer = Common.SafeInteger(Request.Form("revisionID"))
			Dim result = smBL.SaveNodesToDraft(revisionID, draftRecordList.ToArray())
			If result Then
				res = New CJsonResponse(True, "", "OK")
			Else
				res = New CJsonResponse(False, "", "FAILED")
			End If
		Catch ex As Exception
			_log.Error("Could not SaveManageBusinessXADraft.", ex)
			res = New CJsonResponse(False, "Unable to save draft. Please try again", "FAILED")
		End Try
		Response.Write(JsonConvert.SerializeObject(res))
		Response.End()
	End Sub
	Private Sub PreviewManageBusinessXADraft()
		Dim res As New CJsonResponse()
		Try
			Dim smBL As New SmBL()
			Dim oSerializer As New JavaScriptSerializer()
			Dim approvalMsg As String = Common.SafeString(Request.Form("approvalMsg"))
			Dim referredMsg As String = Common.SafeString(Request.Form("referredMsg"))
			Dim eligibilityMsg As String = Common.SafeString(Request.Form("eligibilityMsg"))
			Dim additionalDisclosures As List(Of String) = oSerializer.Deserialize(Of List(Of String))(Request.Form("additionalDisclosures"))
			Dim fundingDisclosure As String = Common.SafeString(Request.Form("fundingDisclosure"))
			Dim lenderConfigID As Guid = Common.SafeGUID(Request.Form("lenderconfigid"))
			Dim draftRecordList As New List(Of SmLenderConfigDraft)
			If Not String.IsNullOrEmpty(approvalMsg) Then
				draftRecordList.Add(New SmLenderConfigDraft() With {
					.LenderConfigID = lenderConfigID,
					.CreatedByUserID = UserInfo.UserID,
					.NodeGroup = SmSettings.NodeGroup.ManageBusinessXA,
					.NodePath = XA_BUSINESS_ACCOUNT_PREAPPROVED_MESSAGE(),
					.NodeContent = SmUtil.BuildSuccessMessageXml("BUSINESS_ACCOUNT_SUCCESS_MESSAGES", "PREAPPROVED_MESSAGE", approvalMsg)})
			End If
			If Not String.IsNullOrEmpty(referredMsg) Then
				draftRecordList.Add(New SmLenderConfigDraft() With {
				.LenderConfigID = lenderConfigID,
				.CreatedByUserID = UserInfo.UserID,
				.NodePath = XA_BUSINESS_ACCOUNT_SUBMITTED_MESSAGE(),
				.NodeGroup = SmSettings.NodeGroup.ManageBusinessXA,
				.NodeContent = SmUtil.BuildSuccessMessageXml("BUSINESS_ACCOUNT_SUCCESS_MESSAGES", "SUBMITTED_MESSAGE", referredMsg)})
			End If

			If Not String.IsNullOrWhiteSpace(eligibilityMsg) Then
				draftRecordList.Add(New SmLenderConfigDraft() With {
				.LenderConfigID = lenderConfigID,
				.CreatedByUserID = UserInfo.UserID,
				.NodePath = XA_BUSINESS_ACCOUNT_ELIGIBILITY_MESSAGE(),
				.NodeGroup = SmSettings.NodeGroup.ManageBusinessXA,
				.NodeContent = SmUtil.BuildContentNodeXml("BUSINESS_ACCOUNT_ELIGIBILITY_MESSAGE", eligibilityMsg)})
			End If

			draftRecordList.Add(New SmLenderConfigDraft() With {
			.LenderConfigID = lenderConfigID,
			.CreatedByUserID = UserInfo.UserID,
			.NodePath = XA_BUSINESS_ACCOUNT_FUNDING_DISCLOSURE(),
			.NodeGroup = SmSettings.NodeGroup.ManageBusinessXA,
			.NodeContent = SmUtil.BuildContentNodeXml("BUSINESS_ACCOUNT_FUNDING_DISCLOSURE", fundingDisclosure)})

			draftRecordList.Add(New SmLenderConfigDraft() With {
			.LenderConfigID = lenderConfigID,
			.CreatedByUserID = UserInfo.UserID,
			.NodePath = XA_BUSINESS_ACCOUNT_DISCLOSURES(),
			.NodeGroup = SmSettings.NodeGroup.ManageBusinessXA,
			.NodeContent = SmUtil.BuildDisclosuresXml(additionalDisclosures, "BUSINESS_ACCOUNT_DISCLOSURES")})

			Dim revisionID As Integer = Common.SafeInteger(Request.Form("revisionID"))
			Dim result = smBL.SaveNodesToDraft(revisionID, draftRecordList.ToArray())
			If result Then
				Dim previewUrl As String = smBL.GeneratePreviewConfig(lenderConfigID, UserInfo.Email, Function(config, code) String.Format("{0}/apply.aspx?lenderref={1}&list=1b&autofill=true&previewCode={2}", _serverRoot, config.LenderRef, code), draftRecordList.ToArray())
				res = New CJsonResponse(True, "", "OK")
				res.Info = New With {.previewurl = previewUrl}
			Else
			End If
		Catch ex As Exception
			_log.Error("Could not PreviewManageBusinessXADraft.", ex)
			res = New CJsonResponse(False, "Unable to save and preview draft. Please try again", "FAILED")
		End Try
		Response.Write(JsonConvert.SerializeObject(res))
		Response.End()
	End Sub
	Private Sub PublishManageBusinessXADraft()
		Dim res As New CJsonResponse()
		Try
			Dim smBL As New SmBL()
			Dim oSerializer As New JavaScriptSerializer()
			Dim approvalMsg As String = Common.SafeString(Request.Form("approvalMsg"))
			Dim referredMsg As String = Common.SafeString(Request.Form("referredMsg"))
			Dim eligibilityMsg As String = Common.SafeString(Request.Form("eligibilityMsg"))
			Dim additionalDisclosures As List(Of String) = oSerializer.Deserialize(Of List(Of String))(Request.Form("additionalDisclosures"))
			Dim fundingDisclosure As String = Common.SafeString(Request.Form("fundingDisclosure"))
			Dim lenderConfigID As Guid = Common.SafeGUID(Request.Form("lenderconfigid"))
			Dim draftRecordList As New List(Of SmLenderConfigDraft)
			If Not String.IsNullOrEmpty(approvalMsg) Then
				draftRecordList.Add(New SmLenderConfigDraft() With {
					.LenderConfigID = lenderConfigID,
					.CreatedByUserID = UserInfo.UserID,
					.NodeGroup = SmSettings.NodeGroup.ManageBusinessXA,
					.NodePath = XA_BUSINESS_ACCOUNT_PREAPPROVED_MESSAGE(),
					.NodeContent = SmUtil.BuildSuccessMessageXml("BUSINESS_ACCOUNT_SUCCESS_MESSAGES", "PREAPPROVED_MESSAGE", approvalMsg)})
			End If
			If Not String.IsNullOrEmpty(referredMsg) Then
				draftRecordList.Add(New SmLenderConfigDraft() With {
				.LenderConfigID = lenderConfigID,
				.CreatedByUserID = UserInfo.UserID,
				.NodePath = XA_BUSINESS_ACCOUNT_SUBMITTED_MESSAGE(),
				.NodeGroup = SmSettings.NodeGroup.ManageBusinessXA,
				.NodeContent = SmUtil.BuildSuccessMessageXml("BUSINESS_ACCOUNT_SUCCESS_MESSAGES", "SUBMITTED_MESSAGE", referredMsg)})
			End If

			If Not String.IsNullOrWhiteSpace(eligibilityMsg) Then
				draftRecordList.Add(New SmLenderConfigDraft() With {
				.LenderConfigID = lenderConfigID,
				.CreatedByUserID = UserInfo.UserID,
				.NodePath = XA_BUSINESS_ACCOUNT_ELIGIBILITY_MESSAGE(),
				.NodeGroup = SmSettings.NodeGroup.ManageBusinessXA,
				.NodeContent = SmUtil.BuildContentNodeXml("BUSINESS_ACCOUNT_ELIGIBILITY_MESSAGE", eligibilityMsg)})
			End If

			draftRecordList.Add(New SmLenderConfigDraft() With {
			.LenderConfigID = lenderConfigID,
			.CreatedByUserID = UserInfo.UserID,
			.NodePath = XA_BUSINESS_ACCOUNT_FUNDING_DISCLOSURE(),
			.NodeGroup = SmSettings.NodeGroup.ManageBusinessXA,
			.NodeContent = SmUtil.BuildContentNodeXml("BUSINESS_ACCOUNT_FUNDING_DISCLOSURE", fundingDisclosure)})

			draftRecordList.Add(New SmLenderConfigDraft() With {
			.LenderConfigID = lenderConfigID,
			.CreatedByUserID = UserInfo.UserID,
			.NodePath = XA_BUSINESS_ACCOUNT_DISCLOSURES(),
			.NodeGroup = SmSettings.NodeGroup.ManageBusinessXA,
			.NodeContent = SmUtil.BuildDisclosuresXml(additionalDisclosures, "BUSINESS_ACCOUNT_DISCLOSURES")})

			Dim comment As String = Common.SafeStripHtmlString(Request.Form("comment"))
			Dim publishAll As Boolean = Common.SafeBoolean(Request.Form("publishAll"))
			Dim result = smBL.PublishNodes(lenderConfigID, UserInfo.UserID, Request.UserHostAddress, comment, publishAll, draftRecordList.ToArray())
			If result Then
				res = New CJsonResponse(True, "", "OK")
			Else
				res = New CJsonResponse(False, "", "FAILED")
			End If
		Catch ex As Exception
			_log.Error("Could not PublishManageBusinessXADraft.", ex)
			res = New CJsonResponse(False, "Unable to publish data. Please try again", "FAILED")
		End Try
		Response.Write(JsonConvert.SerializeObject(res))
		Response.End()
	End Sub
#End Region
#Region "XA Secondary"
    Private Sub SaveManageXASecondaryDraft()
        Dim res As New CJsonResponse()
        Try
            Dim smBL As New SmBL()
            Dim oSerializer As New JavaScriptSerializer()
			Dim additionalDisclosuresSecondary As List(Of String) = oSerializer.Deserialize(Of List(Of String))(Request.Form("additionalDisclosuresSecondary"))
			Dim fundingDisclosureSecondary As String = Common.SafeString(Request.Form("fundingDisclosureSecondary"))
            Dim lenderConfigID As Guid = Common.SafeGUID(Request.Form("lenderconfigid"))
            Dim draftRecordList As New List(Of SmLenderConfigDraft)

            Dim approvedSecondaryMsg As String = Common.SafeString(Request.Form("approvedSecondaryMsg"))

            If Not String.IsNullOrEmpty(approvedSecondaryMsg) Then
                draftRecordList.Add(New SmLenderConfigDraft() With {
                    .LenderConfigID = lenderConfigID,
                    .CreatedByUserID = UserInfo.UserID,
                    .NodePath = XA_APPROVED_SECONDARY_MESSAGE(),
                    .NodeGroup = SmSettings.NodeGroup.ManageXASecondary,
                    .NodeContent = SmUtil.BuildSuccessMessageXml("SUCCESS_MESSAGES", "APPROVED_SECONDARY_MESSAGE", approvedSecondaryMsg)})
            End If

			draftRecordList.Add(New SmLenderConfigDraft() With {
			.LenderConfigID = lenderConfigID,
			.CreatedByUserID = UserInfo.UserID,
			.NodePath = Sm.DISCLOSURES_SECONDARY("XA_LOAN"),
			.NodeGroup = SmSettings.NodeGroup.ManageXASecondary,
			.NodeContent = SmUtil.BuildDisclosuresXml(additionalDisclosuresSecondary)})

			draftRecordList.Add(New SmLenderConfigDraft() With {
			.LenderConfigID = lenderConfigID,
			.CreatedByUserID = UserInfo.UserID,
			.NodePath = FUNDING_DISCLOSURE_SECONDARY("XA_LOAN"),
			.NodeGroup = SmSettings.NodeGroup.ManageXASecondary,
			.NodeContent = SmUtil.BuildContentNodeXml("FUNDING_DISCLOSURE_SECONDARY", fundingDisclosureSecondary)})
            Dim revisionID As Integer = Common.SafeInteger(Request.Form("revisionID"))
            Dim result = smBL.SaveNodesToDraft(revisionID, draftRecordList.ToArray())
            If result Then
                res = New CJsonResponse(True, "", "OK")
            Else
                res = New CJsonResponse(False, "", "FAILED")
            End If
        Catch ex As Exception
            _log.Error("Could not SaveManageXASecondaryDraft.", ex)
            res = New CJsonResponse(False, "Unable to save draft. Please try again", "FAILED")
        End Try
        Response.Write(JsonConvert.SerializeObject(res))
        Response.End()
    End Sub
    Private Sub PreviewManageXASecondaryDraft()
        Dim res As New CJsonResponse()
        Try
            Dim smBL As New SmBL()
            Dim oSerializer As New JavaScriptSerializer()
			Dim additionalDisclosuresSecondary As List(Of String) = oSerializer.Deserialize(Of List(Of String))(Request.Form("additionalDisclosuresSecondary"))
			Dim fundingDisclosureSecondary As String = Common.SafeString(Request.Form("fundingDisclosureSecondary"))
            Dim lenderConfigID As Guid = Common.SafeGUID(Request.Form("lenderconfigid"))
            Dim draftRecordList As New List(Of SmLenderConfigDraft)

            Dim approvedSecondaryMsg As String = Common.SafeString(Request.Form("approvedSecondaryMsg"))
            If Not String.IsNullOrEmpty(approvedSecondaryMsg) Then
                draftRecordList.Add(New SmLenderConfigDraft() With {
                    .LenderConfigID = lenderConfigID,
                    .CreatedByUserID = UserInfo.UserID,
                    .NodePath = XA_APPROVED_SECONDARY_MESSAGE(),
                    .NodeGroup = SmSettings.NodeGroup.ManageXASecondary,
                    .NodeContent = SmUtil.BuildSuccessMessageXml("SUCCESS_MESSAGES", "APPROVED_SECONDARY_MESSAGE", approvedSecondaryMsg)})
            End If

			draftRecordList.Add(New SmLenderConfigDraft() With {
			.LenderConfigID = lenderConfigID,
			.CreatedByUserID = UserInfo.UserID,
			.NodePath = Sm.DISCLOSURES_SECONDARY("XA_LOAN"),
			.NodeGroup = SmSettings.NodeGroup.ManageXASecondary,
			.NodeContent = SmUtil.BuildDisclosuresXml(additionalDisclosuresSecondary)})

			draftRecordList.Add(New SmLenderConfigDraft() With {
			.LenderConfigID = lenderConfigID,
			.CreatedByUserID = UserInfo.UserID,
			.NodePath = FUNDING_DISCLOSURE_SECONDARY("XA_LOAN"),
			.NodeGroup = SmSettings.NodeGroup.ManageXASecondary,
			.NodeContent = SmUtil.BuildContentNodeXml("FUNDING_DISCLOSURE_SECONDARY", fundingDisclosureSecondary)})
            Dim revisionID As Integer = Common.SafeInteger(Request.Form("revisionID"))
            Dim result = smBL.SaveNodesToDraft(revisionID, draftRecordList.ToArray())
            If result Then
                Dim previewUrl As String = smBL.GeneratePreviewConfig(lenderConfigID, UserInfo.Email, Function(config, code) String.Format("{0}/xa/xpressApp.aspx?lenderref={1}&autofill=true&type=2&previewCode={2}", _serverRoot, config.LenderRef, code), draftRecordList.ToArray())
                res = New CJsonResponse(True, "", "OK")
                res.Info = New With {.previewurl = previewUrl}
            Else
            End If
        Catch ex As Exception
            _log.Error("Could not PreviewManageXASecondaryDraft.", ex)
            res = New CJsonResponse(False, "Unable to save and preview draft. Please try again", "FAILED")
        End Try
        Response.Write(JsonConvert.SerializeObject(res))
        Response.End()
    End Sub
    Private Sub PublishManageXASecondaryDraft()
        Dim res As New CJsonResponse()
        Try
            Dim smBL As New SmBL()
            Dim oSerializer As New JavaScriptSerializer()
			Dim additionalDisclosuresSecondary As List(Of String) = oSerializer.Deserialize(Of List(Of String))(Request.Form("additionalDisclosuresSecondary"))
			Dim fundingDisclosureSecondary As String = Common.SafeString(Request.Form("fundingDisclosureSecondary"))
            Dim lenderConfigID As Guid = Common.SafeGUID(Request.Form("lenderconfigid"))
            Dim draftRecordList As New List(Of SmLenderConfigDraft)

            Dim approvedSecondaryMsg As String = Common.SafeString(Request.Form("approvedSecondaryMsg"))
            If Not String.IsNullOrEmpty(approvedSecondaryMsg) Then
                draftRecordList.Add(New SmLenderConfigDraft() With {
                    .LenderConfigID = lenderConfigID,
                    .CreatedByUserID = UserInfo.UserID,
                    .NodePath = XA_APPROVED_SECONDARY_MESSAGE(),
                    .NodeGroup = SmSettings.NodeGroup.ManageXASecondary,
                    .NodeContent = SmUtil.BuildSuccessMessageXml("SUCCESS_MESSAGES", "APPROVED_SECONDARY_MESSAGE", approvedSecondaryMsg)})
            End If

			draftRecordList.Add(New SmLenderConfigDraft() With {
			.LenderConfigID = lenderConfigID,
			.CreatedByUserID = UserInfo.UserID,
			.NodePath = Sm.DISCLOSURES_SECONDARY("XA_LOAN"),
			.NodeGroup = SmSettings.NodeGroup.ManageXASecondary,
			.NodeContent = SmUtil.BuildDisclosuresXml(additionalDisclosuresSecondary)})

			draftRecordList.Add(New SmLenderConfigDraft() With {
			.LenderConfigID = lenderConfigID,
			.CreatedByUserID = UserInfo.UserID,
			.NodePath = FUNDING_DISCLOSURE_SECONDARY("XA_LOAN"),
			.NodeGroup = SmSettings.NodeGroup.ManageXASecondary,
			.NodeContent = SmUtil.BuildContentNodeXml("FUNDING_DISCLOSURE_SECONDARY", fundingDisclosureSecondary)})

            Dim comment As String = Common.SafeStripHtmlString(Request.Form("comment"))
            Dim publishAll As Boolean = Common.SafeBoolean(Request.Form("publishAll"))
            Dim result = smBL.PublishNodes(lenderConfigID, UserInfo.UserID, Request.UserHostAddress, comment, publishAll, draftRecordList.ToArray())
            If result Then
                res = New CJsonResponse(True, "", "OK")
            Else
                res = New CJsonResponse(False, "", "FAILED")
            End If
        Catch ex As Exception
            _log.Error("Could not PublishManageXASecondaryDraft.", ex)
            res = New CJsonResponse(False, "Unable to publish data. Please try again", "FAILED")
        End Try
        Response.Write(JsonConvert.SerializeObject(res))
        Response.End()
    End Sub
#End Region
#Region "XA SPECIAL Secondary"
	Private Sub SaveManageSpecialXASecondaryDraft()
		Dim res As New CJsonResponse()
		Try
			Dim smBL As New SmBL()
			Dim oSerializer As New JavaScriptSerializer()
			Dim additionalDisclosuresSecondary As List(Of String) = oSerializer.Deserialize(Of List(Of String))(Request.Form("additionalDisclosuresSecondary"))
			Dim fundingDisclosureSecondary As String = Common.SafeString(Request.Form("fundingDisclosureSecondary"))
			Dim lenderConfigID As Guid = Common.SafeGUID(Request.Form("lenderconfigid"))
			Dim draftRecordList As New List(Of SmLenderConfigDraft)

			Dim approvedSecondaryMsg As String = Common.SafeString(Request.Form("approvedSecondaryMsg"))

			If Not String.IsNullOrEmpty(approvedSecondaryMsg) Then
				draftRecordList.Add(New SmLenderConfigDraft() With {
					.LenderConfigID = lenderConfigID,
					.CreatedByUserID = UserInfo.UserID,
					.NodePath = XA_SPECIAL_ACCOUNT_APPROVED_SECONDARY_MESSAGE(),
					.NodeGroup = SmSettings.NodeGroup.ManageSpecialXASecondary,
					.NodeContent = SmUtil.BuildSuccessMessageXml("SPECIAL_ACCOUNT_SUCCESS_MESSAGES", "APPROVED_SECONDARY_MESSAGE", approvedSecondaryMsg)})
			End If

			draftRecordList.Add(New SmLenderConfigDraft() With {
			.LenderConfigID = lenderConfigID,
			.CreatedByUserID = UserInfo.UserID,
			.NodePath = XA_SPECIAL_ACCOUNT_DISCLOSURES_SECONDARY(),
			.NodeGroup = SmSettings.NodeGroup.ManageSpecialXASecondary,
			.NodeContent = SmUtil.BuildDisclosuresXml(additionalDisclosuresSecondary, "SPECIAL_ACCOUNT_DISCLOSURES_SECONDARY")})


			draftRecordList.Add(New SmLenderConfigDraft() With {
			.LenderConfigID = lenderConfigID,
			.CreatedByUserID = UserInfo.UserID,
			.NodePath = XA_SPECIAL_ACCOUNT_FUNDING_DISCLOSURE_SECONDARY(),
			.NodeGroup = SmSettings.NodeGroup.ManageSpecialXASecondary,
			.NodeContent = SmUtil.BuildContentNodeXml("SPECIAL_ACCOUNT_FUNDING_DISCLOSURE_SECONDARY", fundingDisclosureSecondary)})

			Dim revisionID As Integer = Common.SafeInteger(Request.Form("revisionID"))
			Dim result = smBL.SaveNodesToDraft(revisionID, draftRecordList.ToArray())
			If result Then
				res = New CJsonResponse(True, "", "OK")
			Else
				res = New CJsonResponse(False, "", "FAILED")
			End If
		Catch ex As Exception
			_log.Error("Could not SaveManageSpecialXASecondaryDraft.", ex)
			res = New CJsonResponse(False, "Unable to save draft. Please try again", "FAILED")
		End Try
		Response.Write(JsonConvert.SerializeObject(res))
		Response.End()
	End Sub
	Private Sub PreviewManageSpecialXASecondaryDraft()
		Dim res As New CJsonResponse()
		Try
			Dim smBL As New SmBL()
			Dim oSerializer As New JavaScriptSerializer()
			Dim additionalDisclosuresSecondary As List(Of String) = oSerializer.Deserialize(Of List(Of String))(Request.Form("additionalDisclosuresSecondary"))
			Dim fundingDisclosureSecondary As String = Common.SafeString(Request.Form("fundingDisclosureSecondary"))
			Dim lenderConfigID As Guid = Common.SafeGUID(Request.Form("lenderconfigid"))
			Dim draftRecordList As New List(Of SmLenderConfigDraft)

			Dim approvedSecondaryMsg As String = Common.SafeString(Request.Form("approvedSecondaryMsg"))
			If Not String.IsNullOrEmpty(approvedSecondaryMsg) Then
				draftRecordList.Add(New SmLenderConfigDraft() With {
					.LenderConfigID = lenderConfigID,
					.CreatedByUserID = UserInfo.UserID,
					.NodePath = XA_SPECIAL_ACCOUNT_APPROVED_SECONDARY_MESSAGE(),
					.NodeGroup = SmSettings.NodeGroup.ManageSpecialXASecondary,
					.NodeContent = SmUtil.BuildSuccessMessageXml("SPECIAL_ACCOUNT_SUCCESS_MESSAGES", "APPROVED_SECONDARY_MESSAGE", approvedSecondaryMsg)})
			End If

			draftRecordList.Add(New SmLenderConfigDraft() With {
			.LenderConfigID = lenderConfigID,
			.CreatedByUserID = UserInfo.UserID,
			.NodePath = XA_SPECIAL_ACCOUNT_DISCLOSURES_SECONDARY(),
			.NodeGroup = SmSettings.NodeGroup.ManageSpecialXASecondary,
			.NodeContent = SmUtil.BuildDisclosuresXml(additionalDisclosuresSecondary, "SPECIAL_ACCOUNT_DISCLOSURES_SECONDARY")})

			draftRecordList.Add(New SmLenderConfigDraft() With {
			.LenderConfigID = lenderConfigID,
			.CreatedByUserID = UserInfo.UserID,
			.NodePath = XA_SPECIAL_ACCOUNT_FUNDING_DISCLOSURE_SECONDARY(),
			.NodeGroup = SmSettings.NodeGroup.ManageSpecialXASecondary,
			.NodeContent = SmUtil.BuildContentNodeXml("SPECIAL_ACCOUNT_FUNDING_DISCLOSURE_SECONDARY", fundingDisclosureSecondary)})

			Dim revisionID As Integer = Common.SafeInteger(Request.Form("revisionID"))
			Dim result = smBL.SaveNodesToDraft(revisionID, draftRecordList.ToArray())
			If result Then
				Dim previewUrl As String = smBL.GeneratePreviewConfig(lenderConfigID, UserInfo.Email, Function(config, code) String.Format("{0}/apply.aspx?lenderref={1}&list=2s&autofill=true&previewCode={2}", _serverRoot, config.LenderRef, code), draftRecordList.ToArray())
				res = New CJsonResponse(True, "", "OK")
				res.Info = New With {.previewurl = previewUrl}
			Else
			End If
		Catch ex As Exception
			_log.Error("Could not PreviewSpecialManageXASecondaryDraft.", ex)
			res = New CJsonResponse(False, "Unable to save and preview draft. Please try again", "FAILED")
		End Try
		Response.Write(JsonConvert.SerializeObject(res))
		Response.End()
	End Sub
	Private Sub PublishManageSpecialXASecondaryDraft()
		Dim res As New CJsonResponse()
		Try
			Dim smBL As New SmBL()
			Dim oSerializer As New JavaScriptSerializer()
			Dim additionalDisclosuresSecondary As List(Of String) = oSerializer.Deserialize(Of List(Of String))(Request.Form("additionalDisclosuresSecondary"))
			Dim fundingDisclosureSecondary As String = Common.SafeString(Request.Form("fundingDisclosureSecondary"))
			Dim lenderConfigID As Guid = Common.SafeGUID(Request.Form("lenderconfigid"))
			Dim draftRecordList As New List(Of SmLenderConfigDraft)

			Dim approvedSecondaryMsg As String = Common.SafeString(Request.Form("approvedSecondaryMsg"))
			If Not String.IsNullOrEmpty(approvedSecondaryMsg) Then
				draftRecordList.Add(New SmLenderConfigDraft() With {
					.LenderConfigID = lenderConfigID,
					.CreatedByUserID = UserInfo.UserID,
					.NodePath = XA_SPECIAL_ACCOUNT_APPROVED_SECONDARY_MESSAGE(),
					.NodeGroup = SmSettings.NodeGroup.ManageSpecialXASecondary,
					.NodeContent = SmUtil.BuildSuccessMessageXml("SPECIAL_ACCOUNT_SUCCESS_MESSAGES", "APPROVED_SECONDARY_MESSAGE", approvedSecondaryMsg)})
			End If

			draftRecordList.Add(New SmLenderConfigDraft() With {
			.LenderConfigID = lenderConfigID,
			.CreatedByUserID = UserInfo.UserID,
			.NodePath = XA_SPECIAL_ACCOUNT_DISCLOSURES_SECONDARY(),
			.NodeGroup = SmSettings.NodeGroup.ManageSpecialXASecondary,
			.NodeContent = SmUtil.BuildDisclosuresXml(additionalDisclosuresSecondary, "SPECIAL_ACCOUNT_DISCLOSURES_SECONDARY")})

			draftRecordList.Add(New SmLenderConfigDraft() With {
			.LenderConfigID = lenderConfigID,
			.CreatedByUserID = UserInfo.UserID,
			.NodePath = XA_SPECIAL_ACCOUNT_FUNDING_DISCLOSURE_SECONDARY(),
			.NodeGroup = SmSettings.NodeGroup.ManageSpecialXASecondary,
			.NodeContent = SmUtil.BuildContentNodeXml("SPECIAL_ACCOUNT_FUNDING_DISCLOSURE_SECONDARY", fundingDisclosureSecondary)})

			Dim comment As String = Common.SafeStripHtmlString(Request.Form("comment"))
			Dim publishAll As Boolean = Common.SafeBoolean(Request.Form("publishAll"))
			Dim result = smBL.PublishNodes(lenderConfigID, UserInfo.UserID, Request.UserHostAddress, comment, publishAll, draftRecordList.ToArray())
			If result Then
				res = New CJsonResponse(True, "", "OK")
			Else
				res = New CJsonResponse(False, "", "FAILED")
			End If
		Catch ex As Exception
			_log.Error("Could not PublishManageSpecialXASecondaryDraft.", ex)
			res = New CJsonResponse(False, "Unable to publish data. Please try again", "FAILED")
		End Try
		Response.Write(JsonConvert.SerializeObject(res))
		Response.End()
	End Sub
#End Region
#Region "XA BUSINESS Secondary"
	Private Sub SaveManageBusinessXASecondaryDraft()
		Dim res As New CJsonResponse()
		Try
			Dim smBL As New SmBL()
			Dim oSerializer As New JavaScriptSerializer()
			Dim additionalDisclosuresSecondary As List(Of String) = oSerializer.Deserialize(Of List(Of String))(Request.Form("additionalDisclosuresSecondary"))
			Dim fundingDisclosureSecondary As String = Common.SafeString(Request.Form("fundingDisclosureSecondary"))
			Dim lenderConfigID As Guid = Common.SafeGUID(Request.Form("lenderconfigid"))
			Dim draftRecordList As New List(Of SmLenderConfigDraft)

			Dim approvedSecondaryMsg As String = Common.SafeString(Request.Form("approvedSecondaryMsg"))

			If Not String.IsNullOrEmpty(approvedSecondaryMsg) Then
				draftRecordList.Add(New SmLenderConfigDraft() With {
					.LenderConfigID = lenderConfigID,
					.CreatedByUserID = UserInfo.UserID,
					.NodePath = XA_BUSINESS_ACCOUNT_APPROVED_SECONDARY_MESSAGE(),
					.NodeGroup = SmSettings.NodeGroup.ManageBusinessXASecondary,
					.NodeContent = SmUtil.BuildSuccessMessageXml("BUSINESS_ACCOUNT_SUCCESS_MESSAGES", "APPROVED_SECONDARY_MESSAGE", approvedSecondaryMsg)})
			End If

			draftRecordList.Add(New SmLenderConfigDraft() With {
			.LenderConfigID = lenderConfigID,
			.CreatedByUserID = UserInfo.UserID,
			.NodePath = XA_BUSINESS_ACCOUNT_DISCLOSURES_SECONDARY(),
			.NodeGroup = SmSettings.NodeGroup.ManageBusinessXASecondary,
			.NodeContent = SmUtil.BuildDisclosuresXml(additionalDisclosuresSecondary, "BUSINESS_ACCOUNT_DISCLOSURES_SECONDARY")})

			draftRecordList.Add(New SmLenderConfigDraft() With {
			.LenderConfigID = lenderConfigID,
			.CreatedByUserID = UserInfo.UserID,
			.NodePath = XA_BUSINESS_ACCOUNT_FUNDING_DISCLOSURE_SECONDARY(),
			.NodeGroup = SmSettings.NodeGroup.ManageBusinessXASecondary,
			.NodeContent = SmUtil.BuildContentNodeXml("BUSINESS_ACCOUNT_FUNDING_DISCLOSURE_SECONDARY", fundingDisclosureSecondary)})



			Dim revisionID As Integer = Common.SafeInteger(Request.Form("revisionID"))
			Dim result = smBL.SaveNodesToDraft(revisionID, draftRecordList.ToArray())
			If result Then
				res = New CJsonResponse(True, "", "OK")
			Else
				res = New CJsonResponse(False, "", "FAILED")
			End If
		Catch ex As Exception
			_log.Error("Could not SaveManageBusinessXASecondaryDraft.", ex)
			res = New CJsonResponse(False, "Unable to save draft. Please try again", "FAILED")
		End Try
		Response.Write(JsonConvert.SerializeObject(res))
		Response.End()
	End Sub
	Private Sub PreviewManageBusinessXASecondaryDraft()
		Dim res As New CJsonResponse()
		Try
			Dim smBL As New SmBL()
			Dim oSerializer As New JavaScriptSerializer()
			Dim additionalDisclosuresSecondary As List(Of String) = oSerializer.Deserialize(Of List(Of String))(Request.Form("additionalDisclosuresSecondary"))
			Dim fundingDisclosureSecondary As String = Common.SafeString(Request.Form("fundingDisclosureSecondary"))
			Dim lenderConfigID As Guid = Common.SafeGUID(Request.Form("lenderconfigid"))
			Dim draftRecordList As New List(Of SmLenderConfigDraft)

			Dim approvedSecondaryMsg As String = Common.SafeString(Request.Form("approvedSecondaryMsg"))
			If Not String.IsNullOrEmpty(approvedSecondaryMsg) Then
				draftRecordList.Add(New SmLenderConfigDraft() With {
					.LenderConfigID = lenderConfigID,
					.CreatedByUserID = UserInfo.UserID,
					.NodePath = XA_BUSINESS_ACCOUNT_APPROVED_SECONDARY_MESSAGE(),
					.NodeGroup = SmSettings.NodeGroup.ManageBusinessXASecondary,
					.NodeContent = SmUtil.BuildSuccessMessageXml("BUSINESS_ACCOUNT_SUCCESS_MESSAGES", "APPROVED_SECONDARY_MESSAGE", approvedSecondaryMsg)})
			End If

			draftRecordList.Add(New SmLenderConfigDraft() With {
			.LenderConfigID = lenderConfigID,
			.CreatedByUserID = UserInfo.UserID,
			.NodePath = XA_BUSINESS_ACCOUNT_DISCLOSURES_SECONDARY(),
			.NodeGroup = SmSettings.NodeGroup.ManageBusinessXASecondary,
			.NodeContent = SmUtil.BuildDisclosuresXml(additionalDisclosuresSecondary, "BUSINESS_ACCOUNT_DISCLOSURES_SECONDARY")})

			draftRecordList.Add(New SmLenderConfigDraft() With {
			.LenderConfigID = lenderConfigID,
			.CreatedByUserID = UserInfo.UserID,
			.NodePath = XA_BUSINESS_ACCOUNT_FUNDING_DISCLOSURE_SECONDARY(),
			.NodeGroup = SmSettings.NodeGroup.ManageBusinessXASecondary,
			.NodeContent = SmUtil.BuildContentNodeXml("BUSINESS_ACCOUNT_FUNDING_DISCLOSURE_SECONDARY", fundingDisclosureSecondary)})

			Dim revisionID As Integer = Common.SafeInteger(Request.Form("revisionID"))
			Dim result = smBL.SaveNodesToDraft(revisionID, draftRecordList.ToArray())
			If result Then
				Dim previewUrl As String = smBL.GeneratePreviewConfig(lenderConfigID, UserInfo.Email, Function(config, code) String.Format("{0}/apply.aspx?lenderref={1}&list=2b&autofill=true&previewCode={2}", _serverRoot, config.LenderRef, code), draftRecordList.ToArray())
				res = New CJsonResponse(True, "", "OK")
				res.Info = New With {.previewurl = previewUrl}
			Else
			End If
		Catch ex As Exception
			_log.Error("Could not PreviewBusinessManageXASecondaryDraft.", ex)
			res = New CJsonResponse(False, "Unable to save and preview draft. Please try again", "FAILED")
		End Try
		Response.Write(JsonConvert.SerializeObject(res))
		Response.End()
	End Sub
	Private Sub PublishManageBusinessXASecondaryDraft()
		Dim res As New CJsonResponse()
		Try
			Dim smBL As New SmBL()
			Dim oSerializer As New JavaScriptSerializer()
			Dim additionalDisclosuresSecondary As List(Of String) = oSerializer.Deserialize(Of List(Of String))(Request.Form("additionalDisclosuresSecondary"))
			Dim fundingDisclosureSecondary As String = Common.SafeString(Request.Form("fundingDisclosureSecondary"))
			Dim lenderConfigID As Guid = Common.SafeGUID(Request.Form("lenderconfigid"))
			Dim draftRecordList As New List(Of SmLenderConfigDraft)

			Dim approvedSecondaryMsg As String = Common.SafeString(Request.Form("approvedSecondaryMsg"))
			If Not String.IsNullOrEmpty(approvedSecondaryMsg) Then
				draftRecordList.Add(New SmLenderConfigDraft() With {
					.LenderConfigID = lenderConfigID,
					.CreatedByUserID = UserInfo.UserID,
					.NodePath = XA_BUSINESS_ACCOUNT_APPROVED_SECONDARY_MESSAGE(),
					.NodeGroup = SmSettings.NodeGroup.ManageBusinessXASecondary,
					.NodeContent = SmUtil.BuildSuccessMessageXml("BUSINESS_ACCOUNT_SUCCESS_MESSAGES", "APPROVED_SECONDARY_MESSAGE", approvedSecondaryMsg)})
			End If

			draftRecordList.Add(New SmLenderConfigDraft() With {
			.LenderConfigID = lenderConfigID,
			.CreatedByUserID = UserInfo.UserID,
			.NodePath = XA_BUSINESS_ACCOUNT_DISCLOSURES_SECONDARY(),
			.NodeGroup = SmSettings.NodeGroup.ManageBusinessXASecondary,
			.NodeContent = SmUtil.BuildDisclosuresXml(additionalDisclosuresSecondary, "BUSINESS_ACCOUNT_DISCLOSURES_SECONDARY")})

			draftRecordList.Add(New SmLenderConfigDraft() With {
			.LenderConfigID = lenderConfigID,
			.CreatedByUserID = UserInfo.UserID,
			.NodePath = XA_BUSINESS_ACCOUNT_FUNDING_DISCLOSURE_SECONDARY(),
			.NodeGroup = SmSettings.NodeGroup.ManageBusinessXASecondary,
			.NodeContent = SmUtil.BuildContentNodeXml("BUSINESS_ACCOUNT_FUNDING_DISCLOSURE_SECONDARY", fundingDisclosureSecondary)})

			Dim comment As String = Common.SafeStripHtmlString(Request.Form("comment"))
			Dim publishAll As Boolean = Common.SafeBoolean(Request.Form("publishAll"))
			Dim result = smBL.PublishNodes(lenderConfigID, UserInfo.UserID, Request.UserHostAddress, comment, publishAll, draftRecordList.ToArray())
			If result Then
				res = New CJsonResponse(True, "", "OK")
			Else
				res = New CJsonResponse(False, "", "FAILED")
			End If
		Catch ex As Exception
			_log.Error("Could not PublishManageBusinessXASecondaryDraft.", ex)
			res = New CJsonResponse(False, "Unable to publish data. Please try again", "FAILED")
		End Try
		Response.Write(JsonConvert.SerializeObject(res))
		Response.End()
	End Sub
#End Region
#Region "VL"
    Private Sub SaveManageVLDraft()
        Dim res As New CJsonResponse()
        Try
            Dim smBL As New SmBL()
            Dim oSerializer As New JavaScriptSerializer()
            Dim approvalMsg As String = Common.SafeString(Request.Form("approvalMsg"))
            Dim referredMsg As String = Common.SafeString(Request.Form("referredMsg"))
            Dim declinedMsg As String = Common.SafeString(Request.Form("declinedMsg"))
            Dim preQualifiedMsg As String = Common.SafeString(Request.Form("preQualifiedMsg"))
            Dim disclosures As List(Of String) = oSerializer.Deserialize(Of List(Of String))(Request.Form("disclosures"))
            Dim lenderConfigID As Guid = Common.SafeGUID(Request.Form("lenderconfigid"))
            Dim vehicleTypes As List(Of SmTextValuePair) = oSerializer.Deserialize(Of List(Of SmTextValuePair))(Request.Form("vehicleTypes"))
            Dim vehicleLoanPurposes As List(Of SmTextValueCatItem) = oSerializer.Deserialize(Of List(Of SmTextValueCatItem))(Request.Form("vehicleLoanPurposes"))
            Dim draftRecordList As New List(Of SmLenderConfigDraft)
            If Not String.IsNullOrEmpty(approvalMsg) Then
                draftRecordList.Add(New SmLenderConfigDraft() With {
                .LenderConfigID = lenderConfigID,
                .CreatedByUserID = UserInfo.UserID,
                .NodePath = PREAPPROVED_MESSAGE("VEHICLE_LOAN"),
                .NodeGroup = SmSettings.NodeGroup.ManageVL,
                .NodeContent = SmUtil.BuildSuccessMessageXml("SUCCESS_MESSAGES", "PREAPPROVED_MESSAGE", approvalMsg)})
            End If
            If Not String.IsNullOrEmpty(referredMsg) Then
                draftRecordList.Add(New SmLenderConfigDraft() With {
                    .LenderConfigID = lenderConfigID,
                    .CreatedByUserID = UserInfo.UserID,
                    .NodePath = SUBMITTED_MESSAGE("VEHICLE_LOAN"),
                    .NodeGroup = SmSettings.NodeGroup.ManageVL,
                    .NodeContent = SmUtil.BuildSuccessMessageXml("SUCCESS_MESSAGES", "SUBMITTED_MESSAGE", referredMsg)})
            End If
            If Not String.IsNullOrEmpty(declinedMsg) Then
                draftRecordList.Add(New SmLenderConfigDraft() With {
                    .LenderConfigID = lenderConfigID,
                    .CreatedByUserID = UserInfo.UserID,
                    .NodePath = DECLINED_MESSAGE("VEHICLE_LOAN"),
                    .NodeGroup = SmSettings.NodeGroup.ManageVL,
                    .NodeContent = SmUtil.BuildSuccessMessageXml("SUCCESS_MESSAGES", "DECLINED_MESSAGE", declinedMsg)})
            End If
            If Not String.IsNullOrEmpty(preQualifiedMsg) Then
                draftRecordList.Add(New SmLenderConfigDraft() With {
                    .LenderConfigID = lenderConfigID,
                    .CreatedByUserID = UserInfo.UserID,
                    .NodePath = PREQUALIFIED_MESSAGE("VEHICLE_LOAN"),
                    .NodeGroup = SmSettings.NodeGroup.ManageVL,
                    .NodeContent = SmUtil.BuildSuccessMessageXml("SUCCESS_MESSAGES", "PREQUALIFIED_MESSAGE", preQualifiedMsg)})
            End If

            draftRecordList.Add(New SmLenderConfigDraft() With {
                            .LenderConfigID = lenderConfigID,
                            .CreatedByUserID = UserInfo.UserID,
                            .NodePath = Sm.DISCLOSURES("VEHICLE_LOAN"),
                            .NodeGroup = SmSettings.NodeGroup.ManageVL,
                            .NodeContent = SmUtil.BuildDisclosuresXml(disclosures)})
            draftRecordList.Add(New SmLenderConfigDraft() With {
            .LenderConfigID = lenderConfigID,
            .CreatedByUserID = UserInfo.UserID,
            .NodePath = Sm.VEHICLE_TYPES(),
            .NodeGroup = SmSettings.NodeGroup.ManageVL,
            .NodeContent = SmUtil.BuildTextValueXml("VEHICLE_TYPES", vehicleTypes)})
            draftRecordList.Add(New SmLenderConfigDraft() With {
            .LenderConfigID = lenderConfigID,
            .CreatedByUserID = UserInfo.UserID,
            .NodePath = Sm.VEHICLE_LOAN_PURPOSES(),
            .NodeGroup = SmSettings.NodeGroup.ManageVL,
            .NodeContent = SmUtil.BuildTextValueCatXml("VEHICLE_LOAN_PURPOSES", vehicleLoanPurposes)})
            Dim revisionID As Integer = Common.SafeInteger(Request.Form("revisionID"))
            Dim result = smBL.SaveNodesToDraft(revisionID, draftRecordList.ToArray())
            If result Then
                res = New CJsonResponse(True, "", "OK")
            Else
                res = New CJsonResponse(False, "", "FAILED")
            End If
        Catch ex As Exception
            _log.Error("Could not SaveManageVLDraft.", ex)
            res = New CJsonResponse(False, "Unable to save draft. Please try again", "FAILED")
        End Try
        Response.Write(JsonConvert.SerializeObject(res))
        Response.End()
    End Sub
    Private Sub PreviewManageVLDraft()
        Dim res As New CJsonResponse()
        Try
            Dim smBL As New SmBL()
            Dim oSerializer As New JavaScriptSerializer()
            Dim approvalMsg As String = Common.SafeString(Request.Form("approvalMsg"))
            Dim referredMsg As String = Common.SafeString(Request.Form("referredMsg"))
            Dim declinedMsg As String = Common.SafeString(Request.Form("declinedMsg"))
            Dim preQualifiedMsg As String = Common.SafeString(Request.Form("preQualifiedMsg"))
            Dim disclosures As List(Of String) = oSerializer.Deserialize(Of List(Of String))(Request.Form("disclosures"))
            Dim lenderConfigID As Guid = Common.SafeGUID(Request.Form("lenderconfigid"))
            Dim vehicleTypes As List(Of SmTextValuePair) = oSerializer.Deserialize(Of List(Of SmTextValuePair))(Request.Form("vehicleTypes"))
            Dim vehicleLoanPurposes As List(Of SmTextValueCatItem) = oSerializer.Deserialize(Of List(Of SmTextValueCatItem))(Request.Form("vehicleLoanPurposes"))
            Dim draftRecordList As New List(Of SmLenderConfigDraft)
            If Not String.IsNullOrEmpty(approvalMsg) Then
                draftRecordList.Add(New SmLenderConfigDraft() With {
                .LenderConfigID = lenderConfigID,
                .CreatedByUserID = UserInfo.UserID,
                .NodePath = PREAPPROVED_MESSAGE("VEHICLE_LOAN"),
                .NodeGroup = SmSettings.NodeGroup.ManageVL,
                .NodeContent = SmUtil.BuildSuccessMessageXml("SUCCESS_MESSAGES", "PREAPPROVED_MESSAGE", approvalMsg)})
            End If
            If Not String.IsNullOrEmpty(referredMsg) Then
                draftRecordList.Add(New SmLenderConfigDraft() With {
                    .LenderConfigID = lenderConfigID,
                    .CreatedByUserID = UserInfo.UserID,
                    .NodePath = SUBMITTED_MESSAGE("VEHICLE_LOAN"),
                    .NodeGroup = SmSettings.NodeGroup.ManageVL,
                    .NodeContent = SmUtil.BuildSuccessMessageXml("SUCCESS_MESSAGES", "SUBMITTED_MESSAGE", referredMsg)})
            End If
            draftRecordList.Add(New SmLenderConfigDraft() With {
                .LenderConfigID = lenderConfigID,
                .CreatedByUserID = UserInfo.UserID,
                .NodePath = DECLINED_MESSAGE("VEHICLE_LOAN"),
                .NodeGroup = SmSettings.NodeGroup.ManageVL,
                .NodeContent = SmUtil.BuildSuccessMessageXml("SUCCESS_MESSAGES", "DECLINED_MESSAGE", declinedMsg)})
            If Not String.IsNullOrEmpty(preQualifiedMsg) Then
                draftRecordList.Add(New SmLenderConfigDraft() With {
                    .LenderConfigID = lenderConfigID,
                    .CreatedByUserID = UserInfo.UserID,
                    .NodePath = PREQUALIFIED_MESSAGE("VEHICLE_LOAN"),
                    .NodeGroup = SmSettings.NodeGroup.ManageVL,
                    .NodeContent = SmUtil.BuildSuccessMessageXml("SUCCESS_MESSAGES", "PREQUALIFIED_MESSAGE", preQualifiedMsg)})
            End If
            draftRecordList.Add(New SmLenderConfigDraft() With {
                            .LenderConfigID = lenderConfigID,
                            .CreatedByUserID = UserInfo.UserID,
                            .NodePath = Sm.DISCLOSURES("VEHICLE_LOAN"),
                            .NodeGroup = SmSettings.NodeGroup.ManageVL,
                            .NodeContent = SmUtil.BuildDisclosuresXml(disclosures)})
            draftRecordList.Add(New SmLenderConfigDraft() With {
                .LenderConfigID = lenderConfigID,
                .CreatedByUserID = UserInfo.UserID,
                .NodePath = Sm.VEHICLE_TYPES(),
                .NodeGroup = SmSettings.NodeGroup.ManageVL,
                .NodeContent = SmUtil.BuildTextValueXml("VEHICLE_TYPES", vehicleTypes)})
            draftRecordList.Add(New SmLenderConfigDraft() With {
                .LenderConfigID = lenderConfigID,
                .CreatedByUserID = UserInfo.UserID,
                .NodePath = Sm.VEHICLE_LOAN_PURPOSES(),
                .NodeGroup = SmSettings.NodeGroup.ManageVL,
                .NodeContent = SmUtil.BuildTextValueCatXml("VEHICLE_LOAN_PURPOSES", vehicleLoanPurposes)})
            Dim revisionID As Integer = Common.SafeInteger(Request.Form("revisionID"))
            Dim result = smBL.SaveNodesToDraft(revisionID, draftRecordList.ToArray())
            If result Then
                Dim previewUrl As String = smBL.GeneratePreviewConfig(lenderConfigID, UserInfo.Email, Function(config, code) String.Format("{0}/vl/vehicleloan.aspx?lenderref={1}&autofill=true&previewCode={2}", _serverRoot, config.LenderRef, code), draftRecordList.ToArray())
                res = New CJsonResponse(True, "", "OK")
                res.Info = New With {.previewurl = previewUrl}
            Else
                res = New CJsonResponse(False, "", "FAILED")
            End If
        Catch ex As Exception
            _log.Error("Could not PreviewManageVLDraft.", ex)
            res = New CJsonResponse(False, "Unable to save and preview draft. Please try again", "FAILED")
        End Try
        Response.Write(JsonConvert.SerializeObject(res))
        Response.End()
    End Sub
    Private Sub PublishManageVLDraft()
        Dim res As New CJsonResponse()
        Try
            Dim smBL As New SmBL()
            Dim oSerializer As New JavaScriptSerializer()
            Dim approvalMsg As String = Common.SafeString(Request.Form("approvalMsg"))
            Dim referredMsg As String = Common.SafeString(Request.Form("referredMsg"))
            Dim declinedMsg As String = Common.SafeString(Request.Form("declinedMsg"))
            Dim preQualifiedMsg As String = Common.SafeString(Request.Form("preQualifiedMsg"))
            Dim disclosures As List(Of String) = oSerializer.Deserialize(Of List(Of String))(Request.Form("disclosures"))
            Dim lenderConfigID As Guid = Common.SafeGUID(Request.Form("lenderconfigid"))
            Dim vehicleTypes As List(Of SmTextValuePair) = oSerializer.Deserialize(Of List(Of SmTextValuePair))(Request.Form("vehicleTypes"))
            Dim vehicleLoanPurposes As List(Of SmTextValueCatItem) = oSerializer.Deserialize(Of List(Of SmTextValueCatItem))(Request.Form("vehicleLoanPurposes"))
            Dim draftRecordList As New List(Of SmLenderConfigDraft)
            If Not String.IsNullOrEmpty(approvalMsg) Then
                draftRecordList.Add(New SmLenderConfigDraft() With {
                .LenderConfigID = lenderConfigID,
                .CreatedByUserID = UserInfo.UserID,
                .NodePath = PREAPPROVED_MESSAGE("VEHICLE_LOAN"),
                .NodeGroup = SmSettings.NodeGroup.ManageVL,
                .NodeContent = SmUtil.BuildSuccessMessageXml("SUCCESS_MESSAGES", "PREAPPROVED_MESSAGE", approvalMsg)})
            End If
            If Not String.IsNullOrEmpty(referredMsg) Then
                draftRecordList.Add(New SmLenderConfigDraft() With {
                    .LenderConfigID = lenderConfigID,
                    .CreatedByUserID = UserInfo.UserID,
                    .NodePath = SUBMITTED_MESSAGE("VEHICLE_LOAN"),
                    .NodeGroup = SmSettings.NodeGroup.ManageVL,
                    .NodeContent = SmUtil.BuildSuccessMessageXml("SUCCESS_MESSAGES", "SUBMITTED_MESSAGE", referredMsg)})
            End If
            draftRecordList.Add(New SmLenderConfigDraft() With {
                .LenderConfigID = lenderConfigID,
                .CreatedByUserID = UserInfo.UserID,
                .NodePath = DECLINED_MESSAGE("VEHICLE_LOAN"),
                .NodeGroup = SmSettings.NodeGroup.ManageVL,
                .NodeContent = SmUtil.BuildSuccessMessageXml("SUCCESS_MESSAGES", "DECLINED_MESSAGE", declinedMsg)})
            If Not String.IsNullOrEmpty(preQualifiedMsg) Then
                draftRecordList.Add(New SmLenderConfigDraft() With {
                    .LenderConfigID = lenderConfigID,
                    .CreatedByUserID = UserInfo.UserID,
                    .NodePath = PREQUALIFIED_MESSAGE("VEHICLE_LOAN"),
                    .NodeGroup = SmSettings.NodeGroup.ManageVL,
                    .NodeContent = SmUtil.BuildSuccessMessageXml("SUCCESS_MESSAGES", "PREQUALIFIED_MESSAGE", preQualifiedMsg)})
            End If
            draftRecordList.Add(New SmLenderConfigDraft() With {
                            .LenderConfigID = lenderConfigID,
                            .CreatedByUserID = UserInfo.UserID,
                            .NodePath = Sm.DISCLOSURES("VEHICLE_LOAN"),
                            .NodeGroup = SmSettings.NodeGroup.ManageVL,
                            .NodeContent = SmUtil.BuildDisclosuresXml(disclosures)})
            draftRecordList.Add(New SmLenderConfigDraft() With {
            .LenderConfigID = lenderConfigID,
            .CreatedByUserID = UserInfo.UserID,
            .NodePath = Sm.VEHICLE_TYPES(),
            .NodeGroup = SmSettings.NodeGroup.ManageVL,
            .NodeContent = SmUtil.BuildTextValueXml("VEHICLE_TYPES", vehicleTypes)})
            draftRecordList.Add(New SmLenderConfigDraft() With {
            .LenderConfigID = lenderConfigID,
            .CreatedByUserID = UserInfo.UserID,
            .NodePath = Sm.VEHICLE_LOAN_PURPOSES(),
            .NodeGroup = SmSettings.NodeGroup.ManageVL,
            .NodeContent = SmUtil.BuildTextValueCatXml("VEHICLE_LOAN_PURPOSES", vehicleLoanPurposes)})
            Dim comment As String = Common.SafeStripHtmlString(Request.Form("comment"))
            Dim publishAll As Boolean = Common.SafeBoolean(Request.Form("publishAll"))
            Dim result = smBL.PublishNodes(lenderConfigID, UserInfo.UserID, Request.UserHostAddress, comment, publishAll, draftRecordList.ToArray())
            If result Then
                res = New CJsonResponse(True, "", "OK")
            Else
                res = New CJsonResponse(False, "", "FAILED")
            End If
        Catch ex As Exception
            _log.Error("Could not PublishManageVLDraft.", ex)
            res = New CJsonResponse(False, "Unable to publish data. Please try again", "FAILED")
        End Try
        Response.Write(JsonConvert.SerializeObject(res))
        Response.End()
    End Sub
    Private Sub SaveManageComboVLDraft()
        Dim res As New CJsonResponse()
        Try
            Dim smBL As New SmBL()
            Dim comboBothPreapprovedMsg As String = Common.SafeString(Request.Form("comboBothPreapprovedMsg"))
            Dim comboSubmittedMsg As String = Common.SafeString(Request.Form("comboSubmittedMsg"))
            Dim comboXAPreapprovedMsg As String = Common.SafeString(Request.Form("comboXAPreapprovedMsg"))
            Dim comboXASubmittedMsg As String = Common.SafeString(Request.Form("comboXASubmittedMsg"))
            Dim comboPreQualifiedMsg As String = Common.SafeString(Request.Form("comboPreQualifiedMsg"))
            Dim lenderConfigID As Guid = Common.SafeGUID(Request.Form("lenderconfigid"))
            Dim draftRecordList As New List(Of SmLenderConfigDraft)
            Dim lenderConfig = smBL.GetLiveConfig(lenderConfigID)
            If lenderConfig Is Nothing Then
                Response.StatusCode = 404 '' Forbidden
                Response.StatusDescription = "Not found"
                Response.End() '' Terminate all the rest processes
            End If
            Dim apmComboEnable As Boolean = SmUtil.GetApmComboEnable(lenderConfig.ConfigData)
            If apmComboEnable = False Then
                Response.StatusCode = 404 '' Forbidden
                Response.StatusDescription = "Not found"
                Response.End() '' Terminate all the rest processes
            End If

            If Not String.IsNullOrEmpty(comboBothPreapprovedMsg) Then
                draftRecordList.Add(New SmLenderConfigDraft() With {
                .LenderConfigID = lenderConfigID,
                .CreatedByUserID = UserInfo.UserID,
                .NodePath = COMBO_BOTH_PREAPPROVED_MESSAGE("VEHICLE_LOAN"),
                .NodeGroup = SmSettings.NodeGroup.ManageComboVL,
                .NodeContent = SmUtil.BuildSuccessMessageXml("SUCCESS_MESSAGES", "COMBO_BOTH_PREAPPROVED_MESSAGE", comboBothPreapprovedMsg)})
            End If
            If Not String.IsNullOrEmpty(comboSubmittedMsg) Then
                draftRecordList.Add(New SmLenderConfigDraft() With {
                    .LenderConfigID = lenderConfigID,
                    .CreatedByUserID = UserInfo.UserID,
                    .NodePath = COMBO_SUBMITTED_MESSAGE("VEHICLE_LOAN"),
                    .NodeGroup = SmSettings.NodeGroup.ManageComboVL,
                    .NodeContent = SmUtil.BuildSuccessMessageXml("SUCCESS_MESSAGES", "COMBO_SUBMITTED_MESSAGE", comboSubmittedMsg)})
            End If
            If Not String.IsNullOrEmpty(comboXAPreapprovedMsg) Then
                draftRecordList.Add(New SmLenderConfigDraft() With {
                    .LenderConfigID = lenderConfigID,
                    .CreatedByUserID = UserInfo.UserID,
                    .NodePath = COMBO_XA_PREAPPROVED_MESSAGE("VEHICLE_LOAN"),
                    .NodeGroup = SmSettings.NodeGroup.ManageComboVL,
                    .NodeContent = SmUtil.BuildSuccessMessageXml("SUCCESS_MESSAGES", "COMBO_XA_PREAPPROVED_MESSAGE", comboXAPreapprovedMsg)})
            End If
            If Not String.IsNullOrEmpty(comboXASubmittedMsg) Then
                draftRecordList.Add(New SmLenderConfigDraft() With {
                    .LenderConfigID = lenderConfigID,
                    .CreatedByUserID = UserInfo.UserID,
                    .NodePath = COMBO_XA_SUBMITTED_MESSAGE("VEHICLE_LOAN"),
                    .NodeGroup = SmSettings.NodeGroup.ManageComboVL,
                    .NodeContent = SmUtil.BuildSuccessMessageXml("SUCCESS_MESSAGES", "COMBO_XA_SUBMITTED_MESSAGE", comboXASubmittedMsg)})
            End If
            If Not String.IsNullOrEmpty(comboPreQualifiedMsg) Then
                draftRecordList.Add(New SmLenderConfigDraft() With {
                    .LenderConfigID = lenderConfigID,
                    .CreatedByUserID = UserInfo.UserID,
                    .NodePath = COMBO_PREQUALIFIED_MESSAGE("VEHICLE_LOAN"),
                    .NodeGroup = SmSettings.NodeGroup.ManageComboVL,
                    .NodeContent = SmUtil.BuildSuccessMessageXml("SUCCESS_MESSAGES", "COMBO_PREQUALIFIED_MESSAGE", comboPreQualifiedMsg)})
            End If
            Dim revisionID As Integer = Common.SafeInteger(Request.Form("revisionID"))
            Dim result = smBL.SaveNodesToDraft(revisionID, draftRecordList.ToArray())
            If result Then
                res = New CJsonResponse(True, "", "OK")
            Else
                res = New CJsonResponse(False, "", "FAILED")
            End If
        Catch ex As Exception
            _log.Error("Could not SaveManageComboVLDraft.", ex)
            res = New CJsonResponse(False, "Unable to save draft. Please try again", "FAILED")
        End Try
        Response.Write(JsonConvert.SerializeObject(res))
        Response.End()
    End Sub
    Private Sub PreviewManageComboVLDraft()
        Dim res As New CJsonResponse()
        Try
            Dim smBL As New SmBL()
            Dim comboBothPreapprovedMsg As String = Common.SafeString(Request.Form("comboBothPreapprovedMsg"))
            Dim comboSubmittedMsg As String = Common.SafeString(Request.Form("comboSubmittedMsg"))
            Dim comboXAPreapprovedMsg As String = Common.SafeString(Request.Form("comboXAPreapprovedMsg"))
            Dim comboXASubmittedMsg As String = Common.SafeString(Request.Form("comboXASubmittedMsg"))
            Dim comboPreQualifiedMsg As String = Common.SafeString(Request.Form("comboPreQualifiedMsg"))
            Dim lenderConfigID As Guid = Common.SafeGUID(Request.Form("lenderconfigid"))
            Dim draftRecordList As New List(Of SmLenderConfigDraft)

            Dim lenderConfig = smBL.GetLiveConfig(lenderConfigID)
            If lenderConfig Is Nothing Then
                Response.StatusCode = 404 '' Forbidden
                Response.StatusDescription = "Not found"
                Response.End() '' Terminate all the rest processes
            End If
            Dim apmComboEnable As Boolean = SmUtil.GetApmComboEnable(lenderConfig.ConfigData)
            If apmComboEnable = False Then
                Response.StatusCode = 404 '' Forbidden
                Response.StatusDescription = "Not found"
                Response.End() '' Terminate all the rest processes
            End If

            If Not String.IsNullOrEmpty(comboBothPreapprovedMsg) Then
                draftRecordList.Add(New SmLenderConfigDraft() With {
                .LenderConfigID = lenderConfigID,
                .CreatedByUserID = UserInfo.UserID,
                .NodePath = COMBO_BOTH_PREAPPROVED_MESSAGE("VEHICLE_LOAN"),
                .NodeGroup = SmSettings.NodeGroup.ManageComboVL,
                .NodeContent = SmUtil.BuildSuccessMessageXml("SUCCESS_MESSAGES", "COMBO_BOTH_PREAPPROVED_MESSAGE", comboBothPreapprovedMsg)})
            End If
            If Not String.IsNullOrEmpty(comboSubmittedMsg) Then
                draftRecordList.Add(New SmLenderConfigDraft() With {
                    .LenderConfigID = lenderConfigID,
                    .CreatedByUserID = UserInfo.UserID,
                    .NodePath = COMBO_SUBMITTED_MESSAGE("VEHICLE_LOAN"),
                    .NodeGroup = SmSettings.NodeGroup.ManageComboVL,
                    .NodeContent = SmUtil.BuildSuccessMessageXml("SUCCESS_MESSAGES", "COMBO_SUBMITTED_MESSAGE", comboSubmittedMsg)})
            End If
            If Not String.IsNullOrEmpty(comboXAPreapprovedMsg) Then
                draftRecordList.Add(New SmLenderConfigDraft() With {
                    .LenderConfigID = lenderConfigID,
                    .CreatedByUserID = UserInfo.UserID,
                    .NodePath = COMBO_XA_PREAPPROVED_MESSAGE("VEHICLE_LOAN"),
                    .NodeGroup = SmSettings.NodeGroup.ManageComboVL,
                    .NodeContent = SmUtil.BuildSuccessMessageXml("SUCCESS_MESSAGES", "COMBO_XA_PREAPPROVED_MESSAGE", comboXAPreapprovedMsg)})
            End If
            If Not String.IsNullOrEmpty(comboXASubmittedMsg) Then
                draftRecordList.Add(New SmLenderConfigDraft() With {
                    .LenderConfigID = lenderConfigID,
                    .CreatedByUserID = UserInfo.UserID,
                    .NodePath = COMBO_XA_SUBMITTED_MESSAGE("VEHICLE_LOAN"),
                    .NodeGroup = SmSettings.NodeGroup.ManageComboVL,
                    .NodeContent = SmUtil.BuildSuccessMessageXml("SUCCESS_MESSAGES", "COMBO_XA_SUBMITTED_MESSAGE", comboXASubmittedMsg)})
            End If
            If Not String.IsNullOrEmpty(comboPreQualifiedMsg) Then
                draftRecordList.Add(New SmLenderConfigDraft() With {
                    .LenderConfigID = lenderConfigID,
                    .CreatedByUserID = UserInfo.UserID,
                    .NodePath = COMBO_PREQUALIFIED_MESSAGE("VEHICLE_LOAN"),
                    .NodeGroup = SmSettings.NodeGroup.ManageComboVL,
                    .NodeContent = SmUtil.BuildSuccessMessageXml("SUCCESS_MESSAGES", "COMBO_PREQUALIFIED_MESSAGE", comboPreQualifiedMsg)})
            End If
            Dim revisionID As Integer = Common.SafeInteger(Request.Form("revisionID"))
            Dim result = smBL.SaveNodesToDraft(revisionID, draftRecordList.ToArray())
            If result Then
                Dim previewUrl As String = smBL.GeneratePreviewConfig(lenderConfigID, UserInfo.Email, Function(config, code) String.Format("{0}/vl/vehicleloan.aspx?lenderref={1}&type=1&autofill=true&previewCode={2}", _serverRoot, config.LenderRef, code), draftRecordList.ToArray())
                res = New CJsonResponse(True, "", "OK")
                res.Info = New With {.previewurl = previewUrl}
            Else
                res = New CJsonResponse(False, "", "FAILED")
            End If
        Catch ex As Exception
            _log.Error("Could not PreviewManageComboVLDraft.", ex)
            res = New CJsonResponse(False, "Unable to save and preview draft. Please try again", "FAILED")
        End Try
        Response.Write(JsonConvert.SerializeObject(res))
        Response.End()
    End Sub
    Private Sub PublishManageComboVLDraft()
        Dim res As New CJsonResponse()
        Try
            Dim smBL As New SmBL()
            Dim comboBothPreapprovedMsg As String = Common.SafeString(Request.Form("comboBothPreapprovedMsg"))
            Dim comboSubmittedMsg As String = Common.SafeString(Request.Form("comboSubmittedMsg"))
            Dim comboXAPreapprovedMsg As String = Common.SafeString(Request.Form("comboXAPreapprovedMsg"))
            Dim comboXASubmittedMsg As String = Common.SafeString(Request.Form("comboXASubmittedMsg"))
            Dim comboPreQualifiedMsg As String = Common.SafeString(Request.Form("comboPreQualifiedMsg"))
            Dim lenderConfigID As Guid = Common.SafeGUID(Request.Form("lenderconfigid"))
            Dim draftRecordList As New List(Of SmLenderConfigDraft)
            Dim lenderConfig = smBL.GetLiveConfig(lenderConfigID)
            If lenderConfig Is Nothing Then
                Response.StatusCode = 404 '' Forbidden
                Response.StatusDescription = "Not found"
                Response.End() '' Terminate all the rest processes
            End If
            Dim apmComboEnable As Boolean = SmUtil.GetApmComboEnable(lenderConfig.ConfigData)
            If apmComboEnable = False Then
                Response.StatusCode = 404 '' Forbidden
                Response.StatusDescription = "Not found"
                Response.End() '' Terminate all the rest processes
            End If
            If Not String.IsNullOrEmpty(comboBothPreapprovedMsg) Then
                draftRecordList.Add(New SmLenderConfigDraft() With {
                .LenderConfigID = lenderConfigID,
                .CreatedByUserID = UserInfo.UserID,
                .NodePath = COMBO_BOTH_PREAPPROVED_MESSAGE("VEHICLE_LOAN"),
                .NodeGroup = SmSettings.NodeGroup.ManageComboVL,
                .NodeContent = SmUtil.BuildSuccessMessageXml("SUCCESS_MESSAGES", "COMBO_BOTH_PREAPPROVED_MESSAGE", comboBothPreapprovedMsg)})
            End If
            If Not String.IsNullOrEmpty(comboSubmittedMsg) Then
                draftRecordList.Add(New SmLenderConfigDraft() With {
                    .LenderConfigID = lenderConfigID,
                    .CreatedByUserID = UserInfo.UserID,
                    .NodePath = COMBO_SUBMITTED_MESSAGE("VEHICLE_LOAN"),
                    .NodeGroup = SmSettings.NodeGroup.ManageComboVL,
                    .NodeContent = SmUtil.BuildSuccessMessageXml("SUCCESS_MESSAGES", "COMBO_SUBMITTED_MESSAGE", comboSubmittedMsg)})
            End If
            If Not String.IsNullOrEmpty(comboXAPreapprovedMsg) Then
                draftRecordList.Add(New SmLenderConfigDraft() With {
                    .LenderConfigID = lenderConfigID,
                    .CreatedByUserID = UserInfo.UserID,
                    .NodePath = COMBO_XA_PREAPPROVED_MESSAGE("VEHICLE_LOAN"),
                    .NodeGroup = SmSettings.NodeGroup.ManageComboVL,
                    .NodeContent = SmUtil.BuildSuccessMessageXml("SUCCESS_MESSAGES", "COMBO_XA_PREAPPROVED_MESSAGE", comboXAPreapprovedMsg)})
            End If
            If Not String.IsNullOrEmpty(comboXASubmittedMsg) Then
                draftRecordList.Add(New SmLenderConfigDraft() With {
                    .LenderConfigID = lenderConfigID,
                    .CreatedByUserID = UserInfo.UserID,
                    .NodePath = COMBO_XA_SUBMITTED_MESSAGE("VEHICLE_LOAN"),
                    .NodeGroup = SmSettings.NodeGroup.ManageComboVL,
                    .NodeContent = SmUtil.BuildSuccessMessageXml("SUCCESS_MESSAGES", "COMBO_XA_SUBMITTED_MESSAGE", comboXASubmittedMsg)})
            End If
            If Not String.IsNullOrEmpty(comboPreQualifiedMsg) Then
                draftRecordList.Add(New SmLenderConfigDraft() With {
                    .LenderConfigID = lenderConfigID,
                    .CreatedByUserID = UserInfo.UserID,
                    .NodePath = COMBO_PREQUALIFIED_MESSAGE("VEHICLE_LOAN"),
                    .NodeGroup = SmSettings.NodeGroup.ManageComboVL,
                    .NodeContent = SmUtil.BuildSuccessMessageXml("SUCCESS_MESSAGES", "COMBO_PREQUALIFIED_MESSAGE", comboPreQualifiedMsg)})
            End If
            Dim comment As String = Common.SafeStripHtmlString(Request.Form("comment"))
            Dim publishAll As Boolean = Common.SafeBoolean(Request.Form("publishAll"))
            Dim result = smBL.PublishNodes(lenderConfigID, UserInfo.UserID, Request.UserHostAddress, comment, publishAll, draftRecordList.ToArray())
            If result Then
                res = New CJsonResponse(True, "", "OK")
            Else
                res = New CJsonResponse(False, "", "FAILED")
            End If
        Catch ex As Exception
            _log.Error("Could not PublishManageComboVLDraft.", ex)
            res = New CJsonResponse(False, "Unable to publish data. Please try again", "FAILED")
        End Try
        Response.Write(JsonConvert.SerializeObject(res))
        Response.End()
    End Sub
#End Region
#Region "PL"
    Private Sub SaveManagePLDraft()
        Dim res As New CJsonResponse()
        Try
            Dim smBL As New SmBL()
            Dim oSerializer As New JavaScriptSerializer()
            Dim approvalMsg As String = Common.SafeString(Request.Form("approvalMsg"))
            Dim referredMsg As String = Common.SafeString(Request.Form("referredMsg"))
            Dim declinedMsg As String = Common.SafeString(Request.Form("declinedMsg"))
            Dim preQualifiedMsg As String = Common.SafeString(Request.Form("preQualifiedMsg"))
            Dim disclosures As List(Of String) = oSerializer.Deserialize(Of List(Of String))(Request.Form("disclosures"))
            Dim lenderConfigID As Guid = Common.SafeGUID(Request.Form("lenderconfigid"))
			Dim locTypes As List(Of SmLineOfCreditItem) = JsonConvert.DeserializeObject(Of List(Of SmLineOfCreditItem))(Request.Form("locTypes"))
			Dim loanPurposes As List(Of SmTextValueCatItem) = oSerializer.Deserialize(Of List(Of SmTextValueCatItem))(Request.Form("loanPurposes"))


			Dim draftRecordList As New List(Of SmLenderConfigDraft)
			If Not String.IsNullOrEmpty(approvalMsg) Then
				draftRecordList.Add(New SmLenderConfigDraft() With {
				.LenderConfigID = lenderConfigID,
				.CreatedByUserID = UserInfo.UserID,
				.NodePath = PREAPPROVED_MESSAGE("PERSONAL_LOAN"),
				.NodeGroup = SmSettings.NodeGroup.ManagePL,
				.NodeContent = SmUtil.BuildSuccessMessageXml("SUCCESS_MESSAGES", "PREAPPROVED_MESSAGE", approvalMsg)})
			End If
			If Not String.IsNullOrEmpty(referredMsg) Then
				draftRecordList.Add(New SmLenderConfigDraft() With {
					.LenderConfigID = lenderConfigID,
					.CreatedByUserID = UserInfo.UserID,
					.NodePath = SUBMITTED_MESSAGE("PERSONAL_LOAN"),
					.NodeGroup = SmSettings.NodeGroup.ManagePL,
					.NodeContent = SmUtil.BuildSuccessMessageXml("SUCCESS_MESSAGES", "SUBMITTED_MESSAGE", referredMsg)})
			End If
			draftRecordList.Add(New SmLenderConfigDraft() With {
				.LenderConfigID = lenderConfigID,
				.CreatedByUserID = UserInfo.UserID,
				.NodePath = DECLINED_MESSAGE("PERSONAL_LOAN"),
				.NodeGroup = SmSettings.NodeGroup.ManagePL,
				.NodeContent = SmUtil.BuildSuccessMessageXml("SUCCESS_MESSAGES", "DECLINED_MESSAGE", declinedMsg)})
			If Not String.IsNullOrEmpty(preQualifiedMsg) Then
				draftRecordList.Add(New SmLenderConfigDraft() With {
					.LenderConfigID = lenderConfigID,
					.CreatedByUserID = UserInfo.UserID,
					.NodePath = PREQUALIFIED_MESSAGE("PERSONAL_LOAN"),
					.NodeGroup = SmSettings.NodeGroup.ManagePL,
					.NodeContent = SmUtil.BuildSuccessMessageXml("SUCCESS_MESSAGES", "PREQUALIFIED_MESSAGE", preQualifiedMsg)})
			End If
			draftRecordList.Add(New SmLenderConfigDraft() With {
				.LenderConfigID = lenderConfigID,
				.CreatedByUserID = UserInfo.UserID,
				.NodePath = Sm.DISCLOSURES("PERSONAL_LOAN"),
				.NodeGroup = SmSettings.NodeGroup.ManagePL,
				.NodeContent = SmUtil.BuildDisclosuresXml(disclosures)})
			draftRecordList.Add(New SmLenderConfigDraft() With {
				.LenderConfigID = lenderConfigID,
				.CreatedByUserID = UserInfo.UserID,
				.NodePath = Sm.PERSONAL_LOC_TYPES(),
				.NodeGroup = SmSettings.NodeGroup.ManagePL,
				.NodeContent = SmUtil.BuildLocTypeXml("PERSONAL_LOC_PURPOSES", locTypes)})
			draftRecordList.Add(New SmLenderConfigDraft() With {
				.LenderConfigID = lenderConfigID,
				.CreatedByUserID = UserInfo.UserID,
				.NodePath = Sm.PERSONAL_LOAN_PURPOSES(),
				.NodeGroup = SmSettings.NodeGroup.ManagePL,
				.NodeContent = SmUtil.BuildTextValueCatXml("PERSONAL_LOAN_PURPOSES", loanPurposes)})
			Dim revisionID As Integer = Common.SafeInteger(Request.Form("revisionID"))
			Dim result = smBL.SaveNodesToDraft(revisionID, draftRecordList.ToArray())
			If result Then
				res = New CJsonResponse(True, "", "OK")
			Else
				res = New CJsonResponse(False, "", "FAILED")
			End If
		Catch ex As Exception
			_log.Error("Could not SaveManagePLDraft.", ex)
			res = New CJsonResponse(False, "Unable to save draft. Please try again", "FAILED")
		End Try
		Response.Write(JsonConvert.SerializeObject(res))
		Response.End()
	End Sub
	Private Sub PreviewManagePLDraft()
		Dim res As New CJsonResponse()
		Try
			Dim smBL As New SmBL()
			Dim oSerializer As New JavaScriptSerializer()
			Dim approvalMsg As String = Common.SafeString(Request.Form("approvalMsg"))
			Dim referredMsg As String = Common.SafeString(Request.Form("referredMsg"))
			Dim declinedMsg As String = Common.SafeString(Request.Form("declinedMsg"))
			Dim preQualifiedMsg As String = Common.SafeString(Request.Form("preQualifiedMsg"))
			Dim disclosures As List(Of String) = oSerializer.Deserialize(Of List(Of String))(Request.Form("disclosures"))
			Dim lenderConfigID As Guid = Common.SafeGUID(Request.Form("lenderconfigid"))
			Dim locTypes As List(Of SmLineOfCreditItem) = JsonConvert.DeserializeObject(Of List(Of SmLineOfCreditItem))(Request.Form("locTypes"))
			Dim loanPurposes As List(Of SmTextValueCatItem) = oSerializer.Deserialize(Of List(Of SmTextValueCatItem))(Request.Form("loanPurposes"))


			Dim draftRecordList As New List(Of SmLenderConfigDraft)
			If Not String.IsNullOrEmpty(approvalMsg) Then
				draftRecordList.Add(New SmLenderConfigDraft() With {
				.LenderConfigID = lenderConfigID,
				.CreatedByUserID = UserInfo.UserID,
				.NodePath = PREAPPROVED_MESSAGE("PERSONAL_LOAN"),
				.NodeGroup = SmSettings.NodeGroup.ManagePL,
				.NodeContent = SmUtil.BuildSuccessMessageXml("SUCCESS_MESSAGES", "PREAPPROVED_MESSAGE", approvalMsg)})
			End If
			If Not String.IsNullOrEmpty(referredMsg) Then
				draftRecordList.Add(New SmLenderConfigDraft() With {
					.LenderConfigID = lenderConfigID,
					.CreatedByUserID = UserInfo.UserID,
					.NodePath = SUBMITTED_MESSAGE("PERSONAL_LOAN"),
					.NodeGroup = SmSettings.NodeGroup.ManagePL,
					.NodeContent = SmUtil.BuildSuccessMessageXml("SUCCESS_MESSAGES", "SUBMITTED_MESSAGE", referredMsg)})
			End If
			draftRecordList.Add(New SmLenderConfigDraft() With {
				.LenderConfigID = lenderConfigID,
				.CreatedByUserID = UserInfo.UserID,
				.NodePath = DECLINED_MESSAGE("PERSONAL_LOAN"),
				.NodeGroup = SmSettings.NodeGroup.ManagePL,
				.NodeContent = SmUtil.BuildSuccessMessageXml("SUCCESS_MESSAGES", "DECLINED_MESSAGE", declinedMsg)})
			If Not String.IsNullOrEmpty(preQualifiedMsg) Then
				draftRecordList.Add(New SmLenderConfigDraft() With {
					.LenderConfigID = lenderConfigID,
					.CreatedByUserID = UserInfo.UserID,
					.NodePath = PREQUALIFIED_MESSAGE("PERSONAL_LOAN"),
					.NodeGroup = SmSettings.NodeGroup.ManagePL,
					.NodeContent = SmUtil.BuildSuccessMessageXml("SUCCESS_MESSAGES", "PREQUALIFIED_MESSAGE", preQualifiedMsg)})
			End If
			draftRecordList.Add(New SmLenderConfigDraft() With {
				.LenderConfigID = lenderConfigID,
				.CreatedByUserID = UserInfo.UserID,
				.NodePath = Sm.DISCLOSURES("PERSONAL_LOAN"),
				.NodeGroup = SmSettings.NodeGroup.ManagePL,
				.NodeContent = SmUtil.BuildDisclosuresXml(disclosures)})
			draftRecordList.Add(New SmLenderConfigDraft() With {
				.LenderConfigID = lenderConfigID,
				.CreatedByUserID = UserInfo.UserID,
				.NodePath = Sm.PERSONAL_LOC_TYPES(),
				.NodeGroup = SmSettings.NodeGroup.ManagePL,
				.NodeContent = SmUtil.BuildLocTypeXml("PERSONAL_LOC_PURPOSES", locTypes)})
			draftRecordList.Add(New SmLenderConfigDraft() With {
				.LenderConfigID = lenderConfigID,
				.CreatedByUserID = UserInfo.UserID,
				.NodePath = Sm.PERSONAL_LOAN_PURPOSES(),
				.NodeGroup = SmSettings.NodeGroup.ManagePL,
				.NodeContent = SmUtil.BuildTextValueCatXml("PERSONAL_LOAN_PURPOSES", loanPurposes)})
			Dim revisionID As Integer = Common.SafeInteger(Request.Form("revisionID"))
			Dim result = smBL.SaveNodesToDraft(revisionID, draftRecordList.ToArray())
			If result Then
				Dim previewUrl As String = smBL.GeneratePreviewConfig(lenderConfigID, UserInfo.Email, Function(config, code) String.Format("{0}/pl/personalloan.aspx?lenderref={1}&autofill=true&previewCode={2}", _serverRoot, config.LenderRef, code), draftRecordList.ToArray())
				res = New CJsonResponse(True, "", "OK")
				res.Info = New With {.previewurl = previewUrl}
			Else
				res = New CJsonResponse(False, "", "FAILED")
			End If
		Catch ex As Exception
			_log.Error("Could not PreviewManagePLDraft.", ex)
			res = New CJsonResponse(False, "Unable to save and preview draft. Please try again", "FAILED")
		End Try
		Response.Write(JsonConvert.SerializeObject(res))
		Response.End()
	End Sub

	Private Sub PublishManagePLDraft()
		Dim res As New CJsonResponse()
		Try
			Dim smBL As New SmBL()
			Dim oSerializer As New JavaScriptSerializer()
			Dim approvalMsg As String = Common.SafeString(Request.Form("approvalMsg"))
			Dim referredMsg As String = Common.SafeString(Request.Form("referredMsg"))
			Dim declinedMsg As String = Common.SafeString(Request.Form("declinedMsg"))
			Dim preQualifiedMsg As String = Common.SafeString(Request.Form("preQualifiedMsg"))
			Dim disclosures As List(Of String) = oSerializer.Deserialize(Of List(Of String))(Request.Form("disclosures"))
			Dim lenderConfigID As Guid = Common.SafeGUID(Request.Form("lenderconfigid"))
			Dim locTypes As List(Of SmLineOfCreditItem) = JsonConvert.DeserializeObject(Of List(Of SmLineOfCreditItem))(Request.Form("locTypes"))
			Dim loanPurposes As List(Of SmTextValueCatItem) = oSerializer.Deserialize(Of List(Of SmTextValueCatItem))(Request.Form("loanPurposes"))
			Dim draftRecordList As New List(Of SmLenderConfigDraft)

			If Not String.IsNullOrEmpty(approvalMsg) Then
				draftRecordList.Add(New SmLenderConfigDraft() With {
				.LenderConfigID = lenderConfigID,
				.CreatedByUserID = UserInfo.UserID,
				.NodePath = PREAPPROVED_MESSAGE("PERSONAL_LOAN"),
				.NodeGroup = SmSettings.NodeGroup.ManagePL,
				.NodeContent = SmUtil.BuildSuccessMessageXml("SUCCESS_MESSAGES", "PREAPPROVED_MESSAGE", approvalMsg)})
			End If
			If Not String.IsNullOrEmpty(referredMsg) Then
				draftRecordList.Add(New SmLenderConfigDraft() With {
					.LenderConfigID = lenderConfigID,
					.CreatedByUserID = UserInfo.UserID,
					.NodePath = SUBMITTED_MESSAGE("PERSONAL_LOAN"),
					.NodeGroup = SmSettings.NodeGroup.ManagePL,
					.NodeContent = SmUtil.BuildSuccessMessageXml("SUCCESS_MESSAGES", "SUBMITTED_MESSAGE", referredMsg)})
			End If
			draftRecordList.Add(New SmLenderConfigDraft() With {
				.LenderConfigID = lenderConfigID,
				.CreatedByUserID = UserInfo.UserID,
				.NodePath = DECLINED_MESSAGE("PERSONAL_LOAN"),
				.NodeGroup = SmSettings.NodeGroup.ManagePL,
				.NodeContent = SmUtil.BuildSuccessMessageXml("SUCCESS_MESSAGES", "DECLINED_MESSAGE", declinedMsg)})
			If Not String.IsNullOrEmpty(preQualifiedMsg) Then
				draftRecordList.Add(New SmLenderConfigDraft() With {
					.LenderConfigID = lenderConfigID,
					.CreatedByUserID = UserInfo.UserID,
					.NodePath = PREQUALIFIED_MESSAGE("PERSONAL_LOAN"),
					.NodeGroup = SmSettings.NodeGroup.ManagePL,
					.NodeContent = SmUtil.BuildSuccessMessageXml("SUCCESS_MESSAGES", "PREQUALIFIED_MESSAGE", preQualifiedMsg)})
			End If
			draftRecordList.Add(New SmLenderConfigDraft() With {
				.LenderConfigID = lenderConfigID,
				.CreatedByUserID = UserInfo.UserID,
				.NodePath = Sm.DISCLOSURES("PERSONAL_LOAN"),
				.NodeGroup = SmSettings.NodeGroup.ManagePL,
				.NodeContent = SmUtil.BuildDisclosuresXml(disclosures)})
			draftRecordList.Add(New SmLenderConfigDraft() With {
				.LenderConfigID = lenderConfigID,
				.CreatedByUserID = UserInfo.UserID,
				.NodePath = Sm.PERSONAL_LOC_TYPES(),
				.NodeGroup = SmSettings.NodeGroup.ManagePL,
				.NodeContent = SmUtil.BuildLocTypeXml("PERSONAL_LOC_PURPOSES", locTypes)})
			draftRecordList.Add(New SmLenderConfigDraft() With {
				.LenderConfigID = lenderConfigID,
				.CreatedByUserID = UserInfo.UserID,
				.NodePath = Sm.PERSONAL_LOAN_PURPOSES(),
				.NodeGroup = SmSettings.NodeGroup.ManagePL,
				.NodeContent = SmUtil.BuildTextValueCatXml("PERSONAL_LOAN_PURPOSES", loanPurposes)})
			Dim comment As String = Common.SafeStripHtmlString(Request.Form("comment"))
			Dim publishAll As Boolean = Common.SafeBoolean(Request.Form("publishAll"))
			Dim result = smBL.PublishNodes(lenderConfigID, UserInfo.UserID, Request.UserHostAddress, comment, publishAll, draftRecordList.ToArray())
			If result Then
				res = New CJsonResponse(True, "", "OK")
			Else
				res = New CJsonResponse(False, "", "FAILED")
			End If
		Catch ex As Exception
			_log.Error("Could not PublishManagePLDraft.", ex)
			res = New CJsonResponse(False, "Unable to publish data. Please try again", "FAILED")
		End Try
		Response.Write(JsonConvert.SerializeObject(res))
		Response.End()
	End Sub
    Private Sub SaveManageComboPLDraft()
        Dim res As New CJsonResponse()
        Try
            Dim smBL As New SmBL()
            Dim comboBothPreapprovedMsg As String = Common.SafeString(Request.Form("comboBothPreapprovedMsg"))
            Dim comboSubmittedMsg As String = Common.SafeString(Request.Form("comboSubmittedMsg"))
            Dim comboXAPreapprovedMsg As String = Common.SafeString(Request.Form("comboXAPreapprovedMsg"))
            Dim comboXASubmittedMsg As String = Common.SafeString(Request.Form("comboXASubmittedMsg"))
            Dim comboPreQualifiedMsg As String = Common.SafeString(Request.Form("comboPreQualifiedMsg"))
            Dim lenderConfigID As Guid = Common.SafeGUID(Request.Form("lenderconfigid"))
            Dim lenderConfig = smBL.GetLiveConfig(lenderConfigID)
            If lenderConfig Is Nothing Then
                Response.StatusCode = 404 '' Forbidden
                Response.StatusDescription = "Not found"
                Response.End() '' Terminate all the rest processes
            End If
            Dim apmComboEnable As Boolean = SmUtil.GetApmComboEnable(lenderConfig.ConfigData)
            If apmComboEnable = False Then
                Response.StatusCode = 404 '' Forbidden
                Response.StatusDescription = "Not found"
                Response.End() '' Terminate all the rest processes
            End If
            Dim draftRecordList As New List(Of SmLenderConfigDraft)
            If Not String.IsNullOrEmpty(comboBothPreapprovedMsg) Then
                draftRecordList.Add(New SmLenderConfigDraft() With {
                .LenderConfigID = lenderConfigID,
                .CreatedByUserID = UserInfo.UserID,
                .NodePath = COMBO_BOTH_PREAPPROVED_MESSAGE("PERSONAL_LOAN"),
                .NodeGroup = SmSettings.NodeGroup.ManageComboPL,
                .NodeContent = SmUtil.BuildSuccessMessageXml("SUCCESS_MESSAGES", "COMBO_BOTH_PREAPPROVED_MESSAGE", comboBothPreapprovedMsg)})
            End If
            If Not String.IsNullOrEmpty(comboSubmittedMsg) Then
                draftRecordList.Add(New SmLenderConfigDraft() With {
                    .LenderConfigID = lenderConfigID,
                    .CreatedByUserID = UserInfo.UserID,
                    .NodePath = COMBO_SUBMITTED_MESSAGE("PERSONAL_LOAN"),
                    .NodeGroup = SmSettings.NodeGroup.ManageComboPL,
                    .NodeContent = SmUtil.BuildSuccessMessageXml("SUCCESS_MESSAGES", "COMBO_SUBMITTED_MESSAGE", comboSubmittedMsg)})
            End If
            If Not String.IsNullOrEmpty(comboXAPreapprovedMsg) Then
                draftRecordList.Add(New SmLenderConfigDraft() With {
                    .LenderConfigID = lenderConfigID,
                    .CreatedByUserID = UserInfo.UserID,
                    .NodePath = COMBO_XA_PREAPPROVED_MESSAGE("PERSONAL_LOAN"),
                    .NodeGroup = SmSettings.NodeGroup.ManageComboPL,
                    .NodeContent = SmUtil.BuildSuccessMessageXml("SUCCESS_MESSAGES", "COMBO_XA_PREAPPROVED_MESSAGE", comboXAPreapprovedMsg)})
            End If
            If Not String.IsNullOrEmpty(comboXASubmittedMsg) Then
                draftRecordList.Add(New SmLenderConfigDraft() With {
                    .LenderConfigID = lenderConfigID,
                    .CreatedByUserID = UserInfo.UserID,
                    .NodePath = COMBO_XA_SUBMITTED_MESSAGE("PERSONAL_LOAN"),
                    .NodeGroup = SmSettings.NodeGroup.ManageComboPL,
                    .NodeContent = SmUtil.BuildSuccessMessageXml("SUCCESS_MESSAGES", "COMBO_XA_SUBMITTED_MESSAGE", comboXASubmittedMsg)})
            End If
            If Not String.IsNullOrEmpty(comboPreQualifiedMsg) Then
                draftRecordList.Add(New SmLenderConfigDraft() With {
                    .LenderConfigID = lenderConfigID,
                    .CreatedByUserID = UserInfo.UserID,
                    .NodePath = COMBO_PREQUALIFIED_MESSAGE("PERSONAL_LOAN"),
                    .NodeGroup = SmSettings.NodeGroup.ManageComboPL,
                    .NodeContent = SmUtil.BuildSuccessMessageXml("SUCCESS_MESSAGES", "COMBO_PREQUALIFIED_MESSAGE", comboPreQualifiedMsg)})
            End If
            Dim revisionID As Integer = Common.SafeInteger(Request.Form("revisionID"))
            Dim result = smBL.SaveNodesToDraft(revisionID, draftRecordList.ToArray())
            If result Then
                res = New CJsonResponse(True, "", "OK")
            Else
                res = New CJsonResponse(False, "", "FAILED")
            End If
        Catch ex As Exception
            _log.Error("Could not SaveManageComboPLDraft.", ex)
            res = New CJsonResponse(False, "Unable to save draft. Please try again", "FAILED")
        End Try
        Response.Write(JsonConvert.SerializeObject(res))
        Response.End()
    End Sub
    Private Sub PreviewManageComboPLDraft()
        Dim res As New CJsonResponse()
        Try
            Dim smBL As New SmBL()
            Dim comboBothPreapprovedMsg As String = Common.SafeString(Request.Form("comboBothPreapprovedMsg"))
            Dim comboSubmittedMsg As String = Common.SafeString(Request.Form("comboSubmittedMsg"))
            Dim comboXAPreapprovedMsg As String = Common.SafeString(Request.Form("comboXAPreapprovedMsg"))
            Dim comboXASubmittedMsg As String = Common.SafeString(Request.Form("comboXASubmittedMsg"))
            Dim comboPreQualifiedMsg As String = Common.SafeString(Request.Form("comboPreQualifiedMsg"))
            Dim lenderConfigID As Guid = Common.SafeGUID(Request.Form("lenderconfigid"))
            Dim lenderConfig = smBL.GetLiveConfig(lenderConfigID)
            If lenderConfig Is Nothing Then
                Response.StatusCode = 404 '' Forbidden
                Response.StatusDescription = "Not found"
                Response.End() '' Terminate all the rest processes
            End If
            Dim apmComboEnable As Boolean = SmUtil.GetApmComboEnable(lenderConfig.ConfigData)
            If apmComboEnable = False Then
                Response.StatusCode = 404 '' Forbidden
                Response.StatusDescription = "Not found"
                Response.End() '' Terminate all the rest processes
            End If
            Dim draftRecordList As New List(Of SmLenderConfigDraft)
            If Not String.IsNullOrEmpty(comboBothPreapprovedMsg) Then
                draftRecordList.Add(New SmLenderConfigDraft() With {
                .LenderConfigID = lenderConfigID,
                .CreatedByUserID = UserInfo.UserID,
                .NodePath = COMBO_BOTH_PREAPPROVED_MESSAGE("PERSONAL_LOAN"),
                .NodeGroup = SmSettings.NodeGroup.ManageComboPL,
                .NodeContent = SmUtil.BuildSuccessMessageXml("SUCCESS_MESSAGES", "COMBO_BOTH_PREAPPROVED_MESSAGE", comboBothPreapprovedMsg)})
            End If
            If Not String.IsNullOrEmpty(comboSubmittedMsg) Then
                draftRecordList.Add(New SmLenderConfigDraft() With {
                    .LenderConfigID = lenderConfigID,
                    .CreatedByUserID = UserInfo.UserID,
                    .NodePath = COMBO_SUBMITTED_MESSAGE("PERSONAL_LOAN"),
                    .NodeGroup = SmSettings.NodeGroup.ManageComboPL,
                    .NodeContent = SmUtil.BuildSuccessMessageXml("SUCCESS_MESSAGES", "COMBO_SUBMITTED_MESSAGE", comboSubmittedMsg)})
            End If
            If Not String.IsNullOrEmpty(comboXAPreapprovedMsg) Then
                draftRecordList.Add(New SmLenderConfigDraft() With {
                    .LenderConfigID = lenderConfigID,
                    .CreatedByUserID = UserInfo.UserID,
                    .NodePath = COMBO_XA_PREAPPROVED_MESSAGE("PERSONAL_LOAN"),
                    .NodeGroup = SmSettings.NodeGroup.ManageComboPL,
                    .NodeContent = SmUtil.BuildSuccessMessageXml("SUCCESS_MESSAGES", "COMBO_XA_PREAPPROVED_MESSAGE", comboXAPreapprovedMsg)})
            End If
            If Not String.IsNullOrEmpty(comboXASubmittedMsg) Then
                draftRecordList.Add(New SmLenderConfigDraft() With {
                    .LenderConfigID = lenderConfigID,
                    .CreatedByUserID = UserInfo.UserID,
                    .NodePath = COMBO_XA_SUBMITTED_MESSAGE("PERSONAL_LOAN"),
                    .NodeGroup = SmSettings.NodeGroup.ManageComboPL,
                    .NodeContent = SmUtil.BuildSuccessMessageXml("SUCCESS_MESSAGES", "COMBO_XA_SUBMITTED_MESSAGE", comboXASubmittedMsg)})
            End If
            If Not String.IsNullOrEmpty(comboPreQualifiedMsg) Then
                draftRecordList.Add(New SmLenderConfigDraft() With {
                    .LenderConfigID = lenderConfigID,
                    .CreatedByUserID = UserInfo.UserID,
                    .NodePath = COMBO_PREQUALIFIED_MESSAGE("PERSONAL_LOAN"),
                    .NodeGroup = SmSettings.NodeGroup.ManageComboPL,
                    .NodeContent = SmUtil.BuildSuccessMessageXml("SUCCESS_MESSAGES", "COMBO_PREQUALIFIED_MESSAGE", comboPreQualifiedMsg)})
            End If
            Dim revisionID As Integer = Common.SafeInteger(Request.Form("revisionID"))
            Dim result = smBL.SaveNodesToDraft(revisionID, draftRecordList.ToArray())
            If result Then
                Dim previewUrl As String = smBL.GeneratePreviewConfig(lenderConfigID, UserInfo.Email, Function(config, code) String.Format("{0}/pl/personalloan.aspx?lenderref={1}&type=1&autofill=true&previewCode={2}", _serverRoot, config.LenderRef, code), draftRecordList.ToArray())
                res = New CJsonResponse(True, "", "OK")
                res.Info = New With {.previewurl = previewUrl}
            Else
                res = New CJsonResponse(False, "", "FAILED")
            End If
        Catch ex As Exception
            _log.Error("Could not PreviewManageComboPLDraft.", ex)
            res = New CJsonResponse(False, "Unable to save and preview draft. Please try again", "FAILED")
        End Try
        Response.Write(JsonConvert.SerializeObject(res))
        Response.End()
    End Sub
    Private Sub PublishManageComboPLDraft()
        Dim res As New CJsonResponse()
        Try
            Dim smBL As New SmBL()
            Dim comboBothPreapprovedMsg As String = Common.SafeString(Request.Form("comboBothPreapprovedMsg"))
            Dim comboSubmittedMsg As String = Common.SafeString(Request.Form("comboSubmittedMsg"))
            Dim comboXAPreapprovedMsg As String = Common.SafeString(Request.Form("comboXAPreapprovedMsg"))
            Dim comboXASubmittedMsg As String = Common.SafeString(Request.Form("comboXASubmittedMsg"))
            Dim comboPreQualifiedMsg As String = Common.SafeString(Request.Form("comboPreQualifiedMsg"))
            Dim lenderConfigID As Guid = Common.SafeGUID(Request.Form("lenderconfigid"))
            Dim lenderConfig = smBL.GetLiveConfig(lenderConfigID)
            If lenderConfig Is Nothing Then
                Response.StatusCode = 404 '' Forbidden
                Response.StatusDescription = "Not found"
                Response.End() '' Terminate all the rest processes
            End If
            Dim apmComboEnable As Boolean = SmUtil.GetApmComboEnable(lenderConfig.ConfigData)
            If apmComboEnable = False Then
                Response.StatusCode = 404 '' Forbidden
                Response.StatusDescription = "Not found"
                Response.End() '' Terminate all the rest processes
            End If
            Dim draftRecordList As New List(Of SmLenderConfigDraft)
            If Not String.IsNullOrEmpty(comboBothPreapprovedMsg) Then
                draftRecordList.Add(New SmLenderConfigDraft() With {
                .LenderConfigID = lenderConfigID,
                .CreatedByUserID = UserInfo.UserID,
                .NodePath = COMBO_BOTH_PREAPPROVED_MESSAGE("PERSONAL_LOAN"),
                .NodeGroup = SmSettings.NodeGroup.ManageComboPL,
                .NodeContent = SmUtil.BuildSuccessMessageXml("SUCCESS_MESSAGES", "COMBO_BOTH_PREAPPROVED_MESSAGE", comboBothPreapprovedMsg)})
            End If
            If Not String.IsNullOrEmpty(comboSubmittedMsg) Then
                draftRecordList.Add(New SmLenderConfigDraft() With {
                    .LenderConfigID = lenderConfigID,
                    .CreatedByUserID = UserInfo.UserID,
                    .NodePath = COMBO_SUBMITTED_MESSAGE("PERSONAL_LOAN"),
                    .NodeGroup = SmSettings.NodeGroup.ManageComboPL,
                    .NodeContent = SmUtil.BuildSuccessMessageXml("SUCCESS_MESSAGES", "COMBO_SUBMITTED_MESSAGE", comboSubmittedMsg)})
            End If
            If Not String.IsNullOrEmpty(comboXAPreapprovedMsg) Then
                draftRecordList.Add(New SmLenderConfigDraft() With {
                    .LenderConfigID = lenderConfigID,
                    .CreatedByUserID = UserInfo.UserID,
                    .NodePath = COMBO_XA_PREAPPROVED_MESSAGE("PERSONAL_LOAN"),
                    .NodeGroup = SmSettings.NodeGroup.ManageComboPL,
                    .NodeContent = SmUtil.BuildSuccessMessageXml("SUCCESS_MESSAGES", "COMBO_XA_PREAPPROVED_MESSAGE", comboXAPreapprovedMsg)})
            End If
            If Not String.IsNullOrEmpty(comboPreQualifiedMsg) Then
                draftRecordList.Add(New SmLenderConfigDraft() With {
                    .LenderConfigID = lenderConfigID,
                    .CreatedByUserID = UserInfo.UserID,
                    .NodePath = COMBO_PREQUALIFIED_MESSAGE("PERSONAL_LOAN"),
                    .NodeGroup = SmSettings.NodeGroup.ManageComboPL,
                    .NodeContent = SmUtil.BuildSuccessMessageXml("SUCCESS_MESSAGES", "COMBO_PREQUALIFIED_MESSAGE", comboPreQualifiedMsg)})
            End If
            If Not String.IsNullOrEmpty(comboXASubmittedMsg) Then
                draftRecordList.Add(New SmLenderConfigDraft() With {
                    .LenderConfigID = lenderConfigID,
                    .CreatedByUserID = UserInfo.UserID,
                    .NodePath = COMBO_XA_SUBMITTED_MESSAGE("PERSONAL_LOAN"),
                    .NodeGroup = SmSettings.NodeGroup.ManageComboPL,
                    .NodeContent = SmUtil.BuildSuccessMessageXml("SUCCESS_MESSAGES", "COMBO_XA_SUBMITTED_MESSAGE", comboXASubmittedMsg)})
            End If
            Dim comment As String = Common.SafeStripHtmlString(Request.Form("comment"))
            Dim publishAll As Boolean = Common.SafeBoolean(Request.Form("publishAll"))
            Dim result = smBL.PublishNodes(lenderConfigID, UserInfo.UserID, Request.UserHostAddress, comment, publishAll, draftRecordList.ToArray())
            If result Then
                res = New CJsonResponse(True, "", "OK")
            Else
                res = New CJsonResponse(False, "", "FAILED")
            End If
        Catch ex As Exception
            _log.Error("Could not PublishManageComboPLDraft.", ex)
            res = New CJsonResponse(False, "Unable to publish data. Please try again", "FAILED")
        End Try
        Response.Write(JsonConvert.SerializeObject(res))
        Response.End()
    End Sub
#End Region
#Region "BL"
    Private Sub SaveManageBLDraft()
        Dim res As New CJsonResponse()
        Try
            Dim smBL As New SmBL()
            Dim oSerializer As New JavaScriptSerializer()
            Dim vlPreapprovedMsg As String = Common.SafeString(Request.Form("vlPreapprovedMsg"))
            Dim vlSubmittedMsg As String = Common.SafeString(Request.Form("vlSubmittedMsg"))
            Dim ccPreapprovedMsg As String = Common.SafeString(Request.Form("ccPreapprovedMsg"))
            Dim ccSubmittedMsg As String = Common.SafeString(Request.Form("ccSubmittedMsg"))
            Dim otherPreapprovedMsg As String = Common.SafeString(Request.Form("otherPreapprovedMsg"))
            Dim otherSubmittedMsg As String = Common.SafeString(Request.Form("otherSubmittedMsg"))
            Dim lenderConfigID As Guid = Common.SafeGUID(Request.Form("lenderconfigid"))

            Dim draftRecordList As New List(Of SmLenderConfigDraft)
            If Not String.IsNullOrEmpty(vlPreapprovedMsg) Then
                draftRecordList.Add(New SmLenderConfigDraft() With {
                .LenderConfigID = lenderConfigID,
                .CreatedByUserID = UserInfo.UserID,
                .NodePath = BL_VEHICLE_PREAPPROVED_MESSAGE(),
                .NodeGroup = SmSettings.NodeGroup.ManageBL,
                .NodeContent = SmUtil.BuildSuccessMessageXml("SUCCESS_MESSAGES", "VEHICLE_PREAPPROVED_MESSAGE", vlPreapprovedMsg)})
            End If
            If Not String.IsNullOrEmpty(vlSubmittedMsg) Then
                draftRecordList.Add(New SmLenderConfigDraft() With {
                .LenderConfigID = lenderConfigID,
                .CreatedByUserID = UserInfo.UserID,
                .NodePath = BL_VEHICLE_SUBMITTED_MESSAGE(),
                .NodeGroup = SmSettings.NodeGroup.ManageBL,
                .NodeContent = SmUtil.BuildSuccessMessageXml("SUCCESS_MESSAGES", "VEHICLE_SUBMITTED_MESSAGE", vlSubmittedMsg)})
            End If
            If Not String.IsNullOrEmpty(ccPreapprovedMsg) Then
                draftRecordList.Add(New SmLenderConfigDraft() With {
                .LenderConfigID = lenderConfigID,
                .CreatedByUserID = UserInfo.UserID,
                .NodePath = BL_CARD_PREAPPROVED_MESSAGE(),
                .NodeGroup = SmSettings.NodeGroup.ManageBL,
                .NodeContent = SmUtil.BuildSuccessMessageXml("SUCCESS_MESSAGES", "CARD_PREAPPROVED_MESSAGE", ccPreapprovedMsg)})
            End If
            If Not String.IsNullOrEmpty(ccSubmittedMsg) Then
                draftRecordList.Add(New SmLenderConfigDraft() With {
                .LenderConfigID = lenderConfigID,
                .CreatedByUserID = UserInfo.UserID,
                .NodePath = BL_CARD_SUBMITTED_MESSAGE(),
                .NodeGroup = SmSettings.NodeGroup.ManageBL,
                .NodeContent = SmUtil.BuildSuccessMessageXml("SUCCESS_MESSAGES", "CARD_SUBMITTED_MESSAGE", ccSubmittedMsg)})
            End If
            If Not String.IsNullOrEmpty(otherPreapprovedMsg) Then
                draftRecordList.Add(New SmLenderConfigDraft() With {
                .LenderConfigID = lenderConfigID,
                .CreatedByUserID = UserInfo.UserID,
                .NodePath = BL_OTHER_PREAPPROVED_MESSAGE(),
                .NodeGroup = SmSettings.NodeGroup.ManageBL,
                .NodeContent = SmUtil.BuildSuccessMessageXml("SUCCESS_MESSAGES", "OTHER_PREAPPROVED_MESSAGE", otherPreapprovedMsg)})
            End If
            If Not String.IsNullOrEmpty(otherSubmittedMsg) Then
                draftRecordList.Add(New SmLenderConfigDraft() With {
                .LenderConfigID = lenderConfigID,
                .CreatedByUserID = UserInfo.UserID,
                .NodePath = BL_OTHER_SUBMITTED_MESSAGE(),
                .NodeGroup = SmSettings.NodeGroup.ManageBL,
                .NodeContent = SmUtil.BuildSuccessMessageXml("SUCCESS_MESSAGES", "OTHER_SUBMITTED_MESSAGE", otherSubmittedMsg)})
            End If

            draftRecordList.Add(New SmLenderConfigDraft() With {
                .LenderConfigID = lenderConfigID,
                .CreatedByUserID = UserInfo.UserID,
                .NodePath = BL_CARD_DISCLOSURES(),
                .NodeGroup = SmSettings.NodeGroup.ManageBL,
                .NodeContent = SmUtil.BuildDisclosuresXml(oSerializer.Deserialize(Of List(Of String))(Request.Form("ccDisclosures")), "CARD_DISCLOSURES")})
            draftRecordList.Add(New SmLenderConfigDraft() With {
                .LenderConfigID = lenderConfigID,
                .CreatedByUserID = UserInfo.UserID,
                .NodePath = BL_VEHICLE_DISCLOSURES(),
                .NodeGroup = SmSettings.NodeGroup.ManageBL,
                .NodeContent = SmUtil.BuildDisclosuresXml(oSerializer.Deserialize(Of List(Of String))(Request.Form("vlDisclosures")), "VEHICLE_DISCLOSURES")})
            draftRecordList.Add(New SmLenderConfigDraft() With {
                .LenderConfigID = lenderConfigID,
                .CreatedByUserID = UserInfo.UserID,
                .NodePath = BL_OTHER_DISCLOSURES(),
                .NodeGroup = SmSettings.NodeGroup.ManageBL,
                .NodeContent = SmUtil.BuildDisclosuresXml(oSerializer.Deserialize(Of List(Of String))(Request.Form("otherDisclosures")), "OTHER_DISCLOSURES")})

            draftRecordList.Add(New SmLenderConfigDraft() With {
                .LenderConfigID = lenderConfigID,
                .CreatedByUserID = UserInfo.UserID,
                .NodePath = BL_CREDIT_CARD_TYPES(),
                .NodeGroup = SmSettings.NodeGroup.ManageBL,
                .NodeContent = SmUtil.BuildTextValueXml("BL_CREDIT_CARD_TYPES", oSerializer.Deserialize(Of List(Of SmTextValuePair))(Request.Form("cardTypes")))})

            draftRecordList.Add(New SmLenderConfigDraft() With {
            .LenderConfigID = lenderConfigID,
            .CreatedByUserID = UserInfo.UserID,
            .NodePath = BL_CREDIT_CARD_PURPOSES(),
            .NodeGroup = SmSettings.NodeGroup.ManageBL,
            .NodeContent = SmUtil.BuildTextValueCatXml("BL_CREDIT_CARD_PURPOSES", oSerializer.Deserialize(Of List(Of SmTextValueCatItem))(Request.Form("ccPurposes")))})
            draftRecordList.Add(New SmLenderConfigDraft() With {
            .LenderConfigID = lenderConfigID,
            .CreatedByUserID = UserInfo.UserID,
            .NodePath = BL_VEHICLE_PURPOSES(),
            .NodeGroup = SmSettings.NodeGroup.ManageBL,
            .NodeContent = SmUtil.BuildTextValueCatXml("BL_VEHICLE_PURPOSES", oSerializer.Deserialize(Of List(Of SmTextValueCatItem))(Request.Form("vlPurposes")))})
            draftRecordList.Add(New SmLenderConfigDraft() With {
            .LenderConfigID = lenderConfigID,
            .CreatedByUserID = UserInfo.UserID,
            .NodePath = BL_OTHER_PURPOSES(),
            .NodeGroup = SmSettings.NodeGroup.ManageBL,
            .NodeContent = SmUtil.BuildTextValueCatXml("BL_OTHER_PURPOSES", oSerializer.Deserialize(Of List(Of SmTextValueCatItem))(Request.Form("otherPurposes")))})

            Dim revisionID As Integer = Common.SafeInteger(Request.Form("revisionID"))
            Dim result = smBL.SaveNodesToDraft(revisionID, draftRecordList.ToArray()) ', cardNamesDraftRecord)
            If result Then
                res = New CJsonResponse(True, "", "OK")
            Else
                res = New CJsonResponse(False, "", "FAILED")
            End If
        Catch ex As Exception
            _log.Error("Could not SaveManageBLDraft.", ex)
            res = New CJsonResponse(False, "Unable to save draft. Please try again", "FAILED")
        End Try
        Response.Write(JsonConvert.SerializeObject(res))
        Response.End()
    End Sub
    Private Sub PreviewManageBLDraft()
        Dim res As New CJsonResponse()
        Try
            Dim smBL As New SmBL()
            Dim oSerializer As New JavaScriptSerializer()
            Dim vlPreapprovedMsg As String = Common.SafeString(Request.Form("vlPreapprovedMsg"))
            Dim vlSubmittedMsg As String = Common.SafeString(Request.Form("vlSubmittedMsg"))
            Dim ccPreapprovedMsg As String = Common.SafeString(Request.Form("ccPreapprovedMsg"))
            Dim ccSubmittedMsg As String = Common.SafeString(Request.Form("ccSubmittedMsg"))
            Dim otherPreapprovedMsg As String = Common.SafeString(Request.Form("otherPreapprovedMsg"))
            Dim otherSubmittedMsg As String = Common.SafeString(Request.Form("otherSubmittedMsg"))
            Dim lenderConfigID As Guid = Common.SafeGUID(Request.Form("lenderconfigid"))

            Dim draftRecordList As New List(Of SmLenderConfigDraft)
            If Not String.IsNullOrEmpty(vlPreapprovedMsg) Then
                draftRecordList.Add(New SmLenderConfigDraft() With {
                .LenderConfigID = lenderConfigID,
                .CreatedByUserID = UserInfo.UserID,
                .NodePath = BL_VEHICLE_PREAPPROVED_MESSAGE(),
                .NodeGroup = SmSettings.NodeGroup.ManageBL,
                .NodeContent = SmUtil.BuildSuccessMessageXml("SUCCESS_MESSAGES", "VEHICLE_PREAPPROVED_MESSAGE", vlPreapprovedMsg)})
            End If
            If Not String.IsNullOrEmpty(vlSubmittedMsg) Then
                draftRecordList.Add(New SmLenderConfigDraft() With {
                .LenderConfigID = lenderConfigID,
                .CreatedByUserID = UserInfo.UserID,
                .NodePath = BL_VEHICLE_SUBMITTED_MESSAGE(),
                .NodeGroup = SmSettings.NodeGroup.ManageBL,
                .NodeContent = SmUtil.BuildSuccessMessageXml("SUCCESS_MESSAGES", "VEHICLE_SUBMITTED_MESSAGE", vlSubmittedMsg)})
            End If
            If Not String.IsNullOrEmpty(ccPreapprovedMsg) Then
                draftRecordList.Add(New SmLenderConfigDraft() With {
                .LenderConfigID = lenderConfigID,
                .CreatedByUserID = UserInfo.UserID,
                .NodePath = BL_CARD_PREAPPROVED_MESSAGE(),
                .NodeGroup = SmSettings.NodeGroup.ManageBL,
                .NodeContent = SmUtil.BuildSuccessMessageXml("SUCCESS_MESSAGES", "CARD_PREAPPROVED_MESSAGE", ccPreapprovedMsg)})
            End If
            If Not String.IsNullOrEmpty(ccSubmittedMsg) Then
                draftRecordList.Add(New SmLenderConfigDraft() With {
                .LenderConfigID = lenderConfigID,
                .CreatedByUserID = UserInfo.UserID,
                .NodePath = BL_CARD_SUBMITTED_MESSAGE(),
                .NodeGroup = SmSettings.NodeGroup.ManageBL,
                .NodeContent = SmUtil.BuildSuccessMessageXml("SUCCESS_MESSAGES", "CARD_SUBMITTED_MESSAGE", ccSubmittedMsg)})
            End If
            If Not String.IsNullOrEmpty(otherPreapprovedMsg) Then
                draftRecordList.Add(New SmLenderConfigDraft() With {
                .LenderConfigID = lenderConfigID,
                .CreatedByUserID = UserInfo.UserID,
                .NodePath = BL_OTHER_PREAPPROVED_MESSAGE(),
                .NodeGroup = SmSettings.NodeGroup.ManageBL,
                .NodeContent = SmUtil.BuildSuccessMessageXml("SUCCESS_MESSAGES", "OTHER_PREAPPROVED_MESSAGE", otherPreapprovedMsg)})
            End If
            If Not String.IsNullOrEmpty(otherSubmittedMsg) Then
                draftRecordList.Add(New SmLenderConfigDraft() With {
                .LenderConfigID = lenderConfigID,
                .CreatedByUserID = UserInfo.UserID,
                .NodePath = BL_OTHER_SUBMITTED_MESSAGE(),
                .NodeGroup = SmSettings.NodeGroup.ManageBL,
                .NodeContent = SmUtil.BuildSuccessMessageXml("SUCCESS_MESSAGES", "OTHER_SUBMITTED_MESSAGE", otherSubmittedMsg)})
            End If

            draftRecordList.Add(New SmLenderConfigDraft() With {
                .LenderConfigID = lenderConfigID,
                .CreatedByUserID = UserInfo.UserID,
                .NodePath = BL_CARD_DISCLOSURES(),
                .NodeGroup = SmSettings.NodeGroup.ManageBL,
                .NodeContent = SmUtil.BuildDisclosuresXml(oSerializer.Deserialize(Of List(Of String))(Request.Form("ccDisclosures")), "CARD_DISCLOSURES")})
            draftRecordList.Add(New SmLenderConfigDraft() With {
                .LenderConfigID = lenderConfigID,
                .CreatedByUserID = UserInfo.UserID,
                .NodePath = BL_VEHICLE_DISCLOSURES(),
                .NodeGroup = SmSettings.NodeGroup.ManageBL,
                .NodeContent = SmUtil.BuildDisclosuresXml(oSerializer.Deserialize(Of List(Of String))(Request.Form("vlDisclosures")), "VEHICLE_DISCLOSURES")})
            draftRecordList.Add(New SmLenderConfigDraft() With {
                .LenderConfigID = lenderConfigID,
                .CreatedByUserID = UserInfo.UserID,
                .NodePath = BL_OTHER_DISCLOSURES(),
                .NodeGroup = SmSettings.NodeGroup.ManageBL,
                .NodeContent = SmUtil.BuildDisclosuresXml(oSerializer.Deserialize(Of List(Of String))(Request.Form("otherDisclosures")), "OTHER_DISCLOSURES")})

            draftRecordList.Add(New SmLenderConfigDraft() With {
                .LenderConfigID = lenderConfigID,
                .CreatedByUserID = UserInfo.UserID,
                .NodePath = BL_CREDIT_CARD_TYPES(),
                .NodeGroup = SmSettings.NodeGroup.ManageBL,
                .NodeContent = SmUtil.BuildTextValueXml("BL_CREDIT_CARD_TYPES", oSerializer.Deserialize(Of List(Of SmTextValuePair))(Request.Form("cardTypes")))})

            draftRecordList.Add(New SmLenderConfigDraft() With {
            .LenderConfigID = lenderConfigID,
            .CreatedByUserID = UserInfo.UserID,
            .NodePath = BL_CREDIT_CARD_PURPOSES(),
            .NodeGroup = SmSettings.NodeGroup.ManageBL,
            .NodeContent = SmUtil.BuildTextValueCatXml("BL_CREDIT_CARD_PURPOSES", oSerializer.Deserialize(Of List(Of SmTextValueCatItem))(Request.Form("ccPurposes")))})
            draftRecordList.Add(New SmLenderConfigDraft() With {
            .LenderConfigID = lenderConfigID,
            .CreatedByUserID = UserInfo.UserID,
            .NodePath = BL_VEHICLE_PURPOSES(),
            .NodeGroup = SmSettings.NodeGroup.ManageBL,
            .NodeContent = SmUtil.BuildTextValueCatXml("BL_VEHICLE_PURPOSES", oSerializer.Deserialize(Of List(Of SmTextValueCatItem))(Request.Form("vlPurposes")))})
            draftRecordList.Add(New SmLenderConfigDraft() With {
            .LenderConfigID = lenderConfigID,
            .CreatedByUserID = UserInfo.UserID,
            .NodePath = BL_OTHER_PURPOSES(),
            .NodeGroup = SmSettings.NodeGroup.ManageBL,
            .NodeContent = SmUtil.BuildTextValueCatXml("BL_OTHER_PURPOSES", oSerializer.Deserialize(Of List(Of SmTextValueCatItem))(Request.Form("otherPurposes")))})
            Dim revisionID As Integer = Common.SafeInteger(Request.Form("revisionID"))
            Dim result = smBL.SaveNodesToDraft(revisionID, draftRecordList.ToArray()) ', cardNamesDraftRecord)
            If result Then
                'Dim previewUrl As String = smBL.GeneratePreviewConfig(lenderConfigID, UserInfo.Email, Function(config, code) String.Format("{0}/bl/businessloan.aspx?lenderref={1}&autofill=true&previewCode={2}", _serverRoot, config.LenderRef, code), draftRecordList.ToArray()) ', cardNamesDraftRecord)
                'Dim previewUrl As String = smBL.GeneratePreviewConfig(lenderConfigID, UserInfo.Email, Function(config, code) String.Format("{0}/bl/businessloan.aspx?lenderref={1}&roles=Primary&loantype=card&autofill=true&previewCode={2}", _serverRoot, config.LenderRef, code), draftRecordList.ToArray()) ', cardNamesDraftRecord)
                Dim previewUrl As String = smBL.GeneratePreviewConfig(lenderConfigID, UserInfo.Email, Function(config, code) String.Format("{0}/apply.aspx?lenderref={1}&list=bl&autofill=true&previewCode={2}", _serverRoot, config.LenderRef, code), draftRecordList.ToArray()) ', cardNamesDraftRecord)

                res = New CJsonResponse(True, "", "OK")
                res.Info = New With {.previewurl = previewUrl}
            Else
                res = New CJsonResponse(False, "", "FAILED")
            End If
        Catch ex As Exception
            _log.Error("Could not PreviewManageBLDraft.", ex)
            res = New CJsonResponse(False, "Unable to save and preview draft. Please try again", "FAILED")
        End Try
        Response.Write(JsonConvert.SerializeObject(res))
        Response.End()
    End Sub
    Private Sub PublishManageBLDraft()
        Dim res As New CJsonResponse()
        Try
            Dim smBL As New SmBL()
            Dim oSerializer As New JavaScriptSerializer()
            Dim vlPreapprovedMsg As String = Common.SafeString(Request.Form("vlPreapprovedMsg"))
            Dim vlSubmittedMsg As String = Common.SafeString(Request.Form("vlSubmittedMsg"))
            Dim ccPreapprovedMsg As String = Common.SafeString(Request.Form("ccPreapprovedMsg"))
            Dim ccSubmittedMsg As String = Common.SafeString(Request.Form("ccSubmittedMsg"))
            Dim otherPreapprovedMsg As String = Common.SafeString(Request.Form("otherPreapprovedMsg"))
            Dim otherSubmittedMsg As String = Common.SafeString(Request.Form("otherSubmittedMsg"))
            Dim lenderConfigID As Guid = Common.SafeGUID(Request.Form("lenderconfigid"))

            Dim draftRecordList As New List(Of SmLenderConfigDraft)
            If Not String.IsNullOrEmpty(vlPreapprovedMsg) Then
                draftRecordList.Add(New SmLenderConfigDraft() With {
                .LenderConfigID = lenderConfigID,
                .CreatedByUserID = UserInfo.UserID,
                .NodePath = BL_VEHICLE_PREAPPROVED_MESSAGE(),
                .NodeGroup = SmSettings.NodeGroup.ManageBL,
                .NodeContent = SmUtil.BuildSuccessMessageXml("SUCCESS_MESSAGES", "VEHICLE_PREAPPROVED_MESSAGE", vlPreapprovedMsg)})
            End If
            If Not String.IsNullOrEmpty(vlSubmittedMsg) Then
                draftRecordList.Add(New SmLenderConfigDraft() With {
                .LenderConfigID = lenderConfigID,
                .CreatedByUserID = UserInfo.UserID,
                .NodePath = BL_VEHICLE_SUBMITTED_MESSAGE(),
                .NodeGroup = SmSettings.NodeGroup.ManageBL,
                .NodeContent = SmUtil.BuildSuccessMessageXml("SUCCESS_MESSAGES", "VEHICLE_SUBMITTED_MESSAGE", vlSubmittedMsg)})
            End If
            If Not String.IsNullOrEmpty(ccPreapprovedMsg) Then
                draftRecordList.Add(New SmLenderConfigDraft() With {
                .LenderConfigID = lenderConfigID,
                .CreatedByUserID = UserInfo.UserID,
                .NodePath = BL_CARD_PREAPPROVED_MESSAGE(),
                .NodeGroup = SmSettings.NodeGroup.ManageBL,
                .NodeContent = SmUtil.BuildSuccessMessageXml("SUCCESS_MESSAGES", "CARD_PREAPPROVED_MESSAGE", ccPreapprovedMsg)})
            End If
            If Not String.IsNullOrEmpty(ccSubmittedMsg) Then
                draftRecordList.Add(New SmLenderConfigDraft() With {
                .LenderConfigID = lenderConfigID,
                .CreatedByUserID = UserInfo.UserID,
                .NodePath = BL_CARD_SUBMITTED_MESSAGE(),
                .NodeGroup = SmSettings.NodeGroup.ManageBL,
                .NodeContent = SmUtil.BuildSuccessMessageXml("SUCCESS_MESSAGES", "CARD_SUBMITTED_MESSAGE", ccSubmittedMsg)})
            End If
            If Not String.IsNullOrEmpty(otherPreapprovedMsg) Then
                draftRecordList.Add(New SmLenderConfigDraft() With {
                .LenderConfigID = lenderConfigID,
                .CreatedByUserID = UserInfo.UserID,
                .NodePath = BL_OTHER_PREAPPROVED_MESSAGE(),
                .NodeGroup = SmSettings.NodeGroup.ManageBL,
                .NodeContent = SmUtil.BuildSuccessMessageXml("SUCCESS_MESSAGES", "OTHER_PREAPPROVED_MESSAGE", otherPreapprovedMsg)})
            End If
            If Not String.IsNullOrEmpty(otherSubmittedMsg) Then
                draftRecordList.Add(New SmLenderConfigDraft() With {
                .LenderConfigID = lenderConfigID,
                .CreatedByUserID = UserInfo.UserID,
                .NodePath = BL_OTHER_SUBMITTED_MESSAGE(),
                .NodeGroup = SmSettings.NodeGroup.ManageBL,
                .NodeContent = SmUtil.BuildSuccessMessageXml("SUCCESS_MESSAGES", "OTHER_SUBMITTED_MESSAGE", otherSubmittedMsg)})
            End If

            draftRecordList.Add(New SmLenderConfigDraft() With {
                .LenderConfigID = lenderConfigID,
                .CreatedByUserID = UserInfo.UserID,
                .NodePath = BL_CARD_DISCLOSURES(),
                .NodeGroup = SmSettings.NodeGroup.ManageBL,
                .NodeContent = SmUtil.BuildDisclosuresXml(oSerializer.Deserialize(Of List(Of String))(Request.Form("ccDisclosures")), "CARD_DISCLOSURES")})
            draftRecordList.Add(New SmLenderConfigDraft() With {
                .LenderConfigID = lenderConfigID,
                .CreatedByUserID = UserInfo.UserID,
                .NodePath = BL_VEHICLE_DISCLOSURES(),
                .NodeGroup = SmSettings.NodeGroup.ManageBL,
                .NodeContent = SmUtil.BuildDisclosuresXml(oSerializer.Deserialize(Of List(Of String))(Request.Form("vlDisclosures")), "VEHICLE_DISCLOSURES")})
            draftRecordList.Add(New SmLenderConfigDraft() With {
                .LenderConfigID = lenderConfigID,
                .CreatedByUserID = UserInfo.UserID,
                .NodePath = BL_OTHER_DISCLOSURES(),
                .NodeGroup = SmSettings.NodeGroup.ManageBL,
                .NodeContent = SmUtil.BuildDisclosuresXml(oSerializer.Deserialize(Of List(Of String))(Request.Form("otherDisclosures")), "OTHER_DISCLOSURES")})

            draftRecordList.Add(New SmLenderConfigDraft() With {
                .LenderConfigID = lenderConfigID,
                .CreatedByUserID = UserInfo.UserID,
                .NodePath = BL_CREDIT_CARD_TYPES(),
                .NodeGroup = SmSettings.NodeGroup.ManageBL,
                .NodeContent = SmUtil.BuildTextValueXml("BL_CREDIT_CARD_TYPES", oSerializer.Deserialize(Of List(Of SmTextValuePair))(Request.Form("cardTypes")))})

            draftRecordList.Add(New SmLenderConfigDraft() With {
            .LenderConfigID = lenderConfigID,
            .CreatedByUserID = UserInfo.UserID,
            .NodePath = BL_CREDIT_CARD_PURPOSES(),
            .NodeGroup = SmSettings.NodeGroup.ManageBL,
            .NodeContent = SmUtil.BuildTextValueCatXml("BL_CREDIT_CARD_PURPOSES", oSerializer.Deserialize(Of List(Of SmTextValueCatItem))(Request.Form("ccPurposes")))})
            draftRecordList.Add(New SmLenderConfigDraft() With {
            .LenderConfigID = lenderConfigID,
            .CreatedByUserID = UserInfo.UserID,
            .NodePath = BL_VEHICLE_PURPOSES(),
            .NodeGroup = SmSettings.NodeGroup.ManageBL,
            .NodeContent = SmUtil.BuildTextValueCatXml("BL_VEHICLE_PURPOSES", oSerializer.Deserialize(Of List(Of SmTextValueCatItem))(Request.Form("vlPurposes")))})
            draftRecordList.Add(New SmLenderConfigDraft() With {
            .LenderConfigID = lenderConfigID,
            .CreatedByUserID = UserInfo.UserID,
            .NodePath = BL_OTHER_PURPOSES(),
            .NodeGroup = SmSettings.NodeGroup.ManageBL,
            .NodeContent = SmUtil.BuildTextValueCatXml("BL_OTHER_PURPOSES", oSerializer.Deserialize(Of List(Of SmTextValueCatItem))(Request.Form("otherPurposes")))})
            Dim comment As String = Common.SafeStripHtmlString(Request.Form("comment"))
            Dim publishAll As Boolean = Common.SafeBoolean(Request.Form("publishAll"))
            Dim result = smBL.PublishNodes(lenderConfigID, UserInfo.UserID, Request.UserHostAddress, comment, publishAll, draftRecordList.ToArray())    ', cardNamesDraftRecord)
            If result Then
                res = New CJsonResponse(True, "", "OK")
            Else
                res = New CJsonResponse(False, "", "FAILED")
            End If
        Catch ex As Exception
            _log.Error("Could not PublishManageBLDraft.", ex)
            res = New CJsonResponse(False, "Unable to publish data. Please try again", "FAILED")
        End Try
        Response.Write(JsonConvert.SerializeObject(res))
        Response.End()
    End Sub
#End Region
#Region "CC"
    Private Sub SaveManageCCDraft()
        Dim res As New CJsonResponse()
        Try
            Dim smBL As New SmBL()
            Dim oSerializer As New JavaScriptSerializer()
            Dim approvalMsg As String = Common.SafeString(Request.Form("approvalMsg"))
            Dim referredMsg As String = Common.SafeString(Request.Form("referredMsg"))
            Dim declinedMsg As String = Common.SafeString(Request.Form("declinedMsg"))
            Dim preQualifiedMsg As String = Common.SafeString(Request.Form("preQualifiedMsg"))
            Dim disclosures As List(Of String) = oSerializer.Deserialize(Of List(Of String))(Request.Form("disclosures"))
            Dim lenderConfigID As Guid = Common.SafeGUID(Request.Form("lenderconfigid"))
            Dim cardTypes As List(Of SmTextValuePair) = oSerializer.Deserialize(Of List(Of SmTextValuePair))(Request.Form("cardTypes"))
            Dim loanPurposes As List(Of SmTextValueCatItem) = oSerializer.Deserialize(Of List(Of SmTextValueCatItem))(Request.Form("loanPurposes"))
            'Dim cardNames As List(Of SmCreditCardNameItem) = oSerializer.Deserialize(Of List(Of SmCreditCardNameItem))(Request.Form("cardNames"))

            Dim draftRecordList As New List(Of SmLenderConfigDraft)
            If Not String.IsNullOrEmpty(approvalMsg) Then
                draftRecordList.Add(New SmLenderConfigDraft() With {
                .LenderConfigID = lenderConfigID,
                .CreatedByUserID = UserInfo.UserID,
                .NodePath = PREAPPROVED_MESSAGE("CREDIT_CARD_LOAN"),
                .NodeGroup = SmSettings.NodeGroup.ManageCC,
                .NodeContent = SmUtil.BuildSuccessMessageXml("SUCCESS_MESSAGES", "PREAPPROVED_MESSAGE", approvalMsg)})
            End If
            If Not String.IsNullOrEmpty(referredMsg) Then
                draftRecordList.Add(New SmLenderConfigDraft() With {
                    .LenderConfigID = lenderConfigID,
                    .CreatedByUserID = UserInfo.UserID,
                    .NodePath = SUBMITTED_MESSAGE("CREDIT_CARD_LOAN"),
                    .NodeGroup = SmSettings.NodeGroup.ManageCC,
                    .NodeContent = SmUtil.BuildSuccessMessageXml("SUCCESS_MESSAGES", "SUBMITTED_MESSAGE", referredMsg)})
            End If
            draftRecordList.Add(New SmLenderConfigDraft() With {
                .LenderConfigID = lenderConfigID,
                .CreatedByUserID = UserInfo.UserID,
                .NodePath = DECLINED_MESSAGE("CREDIT_CARD_LOAN"),
                .NodeGroup = SmSettings.NodeGroup.ManageCC,
                .NodeContent = SmUtil.BuildSuccessMessageXml("SUCCESS_MESSAGES", "DECLINED_MESSAGE", declinedMsg)})
            If Not String.IsNullOrEmpty(preQualifiedMsg) Then
                draftRecordList.Add(New SmLenderConfigDraft() With {
                    .LenderConfigID = lenderConfigID,
                    .CreatedByUserID = UserInfo.UserID,
                    .NodePath = PREQUALIFIED_MESSAGE("CREDIT_CARD_LOAN"),
                    .NodeGroup = SmSettings.NodeGroup.ManageCC,
                    .NodeContent = SmUtil.BuildSuccessMessageXml("SUCCESS_MESSAGES", "PREQUALIFIED_MESSAGE", preQualifiedMsg)})
            End If
            draftRecordList.Add(New SmLenderConfigDraft() With {
                .LenderConfigID = lenderConfigID,
                .CreatedByUserID = UserInfo.UserID,
                .NodePath = Sm.DISCLOSURES("CREDIT_CARD_LOAN"),
                .NodeGroup = SmSettings.NodeGroup.ManageCC,
                .NodeContent = SmUtil.BuildDisclosuresXml(disclosures)})
            draftRecordList.Add(New SmLenderConfigDraft() With {
                .LenderConfigID = lenderConfigID,
                .CreatedByUserID = UserInfo.UserID,
                .NodePath = Sm.CREDIT_CARD_TYPES(),
                .NodeGroup = SmSettings.NodeGroup.ManageCC,
                .NodeContent = SmUtil.BuildTextValueXml("CREDIT_CARD_TYPES", cardTypes)})
            'If Not String.IsNullOrEmpty(referredMsg) Then
            'draftRecordList.Add(New SmLenderConfigDraft() With {
            '	.LenderConfigID = lenderConfigID,
            '	
            '	.CreatedBy = UserInfo.Email,
            '	.NodePath = Sm.CREDIT_CARD_NAMES(),
            '	.NodeGroup = SmSettings.NodeGroup.ManageCC,
            '	.NodeContent = SmUtil.BuildCreditCardNameItemXml("CREDIT_CARD_NAMES", cardNames)})
            'End if
            draftRecordList.Add(New SmLenderConfigDraft() With {
            .LenderConfigID = lenderConfigID,
            .CreatedByUserID = UserInfo.UserID,
            .NodePath = Sm.CREDIT_CARD_LOAN_PURPOSES(),
            .NodeGroup = SmSettings.NodeGroup.ManageCC,
            .NodeContent = SmUtil.BuildTextValueCatXml("CREDIT_CARD_LOAN_PURPOSES", loanPurposes)})
            Dim revisionID As Integer = Common.SafeInteger(Request.Form("revisionID"))
            Dim result = smBL.SaveNodesToDraft(revisionID, draftRecordList.ToArray()) ', cardNamesDraftRecord)
            If result Then
                res = New CJsonResponse(True, "", "OK")
            Else
                res = New CJsonResponse(False, "", "FAILED")
            End If
        Catch ex As Exception
            _log.Error("Could not SaveManageCCDraft.", ex)
            res = New CJsonResponse(False, "Unable to save draft. Please try again", "FAILED")
        End Try
        Response.Write(JsonConvert.SerializeObject(res))
        Response.End()
    End Sub
    Private Sub PreviewManageCCDraft()
        Dim res As New CJsonResponse()
        Try
            Dim smBL As New SmBL()
            Dim oSerializer As New JavaScriptSerializer()
            Dim approvalMsg As String = Common.SafeString(Request.Form("approvalMsg"))
            Dim referredMsg As String = Common.SafeString(Request.Form("referredMsg"))
            Dim declinedMsg As String = Common.SafeString(Request.Form("declinedMsg"))
            Dim preQualifiedMsg As String = Common.SafeString(Request.Form("preQualifiedMsg"))
            Dim disclosures As List(Of String) = oSerializer.Deserialize(Of List(Of String))(Request.Form("disclosures"))
            Dim lenderConfigID As Guid = Common.SafeGUID(Request.Form("lenderconfigid"))
            Dim cardTypes As List(Of SmTextValuePair) = oSerializer.Deserialize(Of List(Of SmTextValuePair))(Request.Form("cardTypes"))
            Dim loanPurposes As List(Of SmTextValueCatItem) = oSerializer.Deserialize(Of List(Of SmTextValueCatItem))(Request.Form("loanPurposes"))
            'Dim cardNames As List(Of SmCreditCardNameItem) = oSerializer.Deserialize(Of List(Of SmCreditCardNameItem))(Request.Form("cardNames"))

            Dim draftRecordList As New List(Of SmLenderConfigDraft)
            If Not String.IsNullOrEmpty(approvalMsg) Then
                draftRecordList.Add(New SmLenderConfigDraft() With {
                .LenderConfigID = lenderConfigID,
                .CreatedByUserID = UserInfo.UserID,
                .NodePath = PREAPPROVED_MESSAGE("CREDIT_CARD_LOAN"),
                .NodeGroup = SmSettings.NodeGroup.ManageCC,
                .NodeContent = SmUtil.BuildSuccessMessageXml("SUCCESS_MESSAGES", "PREAPPROVED_MESSAGE", approvalMsg)})
            End If
            If Not String.IsNullOrEmpty(referredMsg) Then
                draftRecordList.Add(New SmLenderConfigDraft() With {
                    .LenderConfigID = lenderConfigID,
                    .CreatedByUserID = UserInfo.UserID,
                    .NodePath = SUBMITTED_MESSAGE("CREDIT_CARD_LOAN"),
                    .NodeGroup = SmSettings.NodeGroup.ManageCC,
                    .NodeContent = SmUtil.BuildSuccessMessageXml("SUCCESS_MESSAGES", "SUBMITTED_MESSAGE", referredMsg)})
            End If
            draftRecordList.Add(New SmLenderConfigDraft() With {
                .LenderConfigID = lenderConfigID,
                .CreatedByUserID = UserInfo.UserID,
                .NodePath = DECLINED_MESSAGE("CREDIT_CARD_LOAN"),
                .NodeGroup = SmSettings.NodeGroup.ManageCC,
                .NodeContent = SmUtil.BuildSuccessMessageXml("SUCCESS_MESSAGES", "DECLINED_MESSAGE", declinedMsg)})
            If Not String.IsNullOrEmpty(preQualifiedMsg) Then
                draftRecordList.Add(New SmLenderConfigDraft() With {
                    .LenderConfigID = lenderConfigID,
                    .CreatedByUserID = UserInfo.UserID,
                    .NodePath = PREQUALIFIED_MESSAGE("CREDIT_CARD_LOAN"),
                    .NodeGroup = SmSettings.NodeGroup.ManageCC,
                    .NodeContent = SmUtil.BuildSuccessMessageXml("SUCCESS_MESSAGES", "PREQUALIFIED_MESSAGE", preQualifiedMsg)})
            End If
            draftRecordList.Add(New SmLenderConfigDraft() With {
                .LenderConfigID = lenderConfigID,
                .CreatedByUserID = UserInfo.UserID,
                .NodePath = Sm.DISCLOSURES("CREDIT_CARD_LOAN"),
                .NodeGroup = SmSettings.NodeGroup.ManageCC,
                .NodeContent = SmUtil.BuildDisclosuresXml(disclosures)})
            draftRecordList.Add(New SmLenderConfigDraft() With {
                .LenderConfigID = lenderConfigID,
                .CreatedByUserID = UserInfo.UserID,
                .NodePath = Sm.CREDIT_CARD_TYPES(),
                .NodeGroup = SmSettings.NodeGroup.ManageCC,
                .NodeContent = SmUtil.BuildTextValueXml("CREDIT_CARD_TYPES", cardTypes)})
            'If Not String.IsNullOrEmpty(referredMsg) Then
            'draftRecordList.Add(New SmLenderConfigDraft() With {
            '	.LenderConfigID = lenderConfigID,
            '	
            '	.CreatedBy = UserInfo.Email,
            '	.NodePath = Sm.CREDIT_CARD_NAMES(),
            '	.NodeGroup = SmSettings.NodeGroup.ManageCC,
            '	.NodeContent = SmUtil.BuildCreditCardNameItemXml("CREDIT_CARD_NAMES", cardNames)})
            'End if
            draftRecordList.Add(New SmLenderConfigDraft() With {
            .LenderConfigID = lenderConfigID,
            .CreatedByUserID = UserInfo.UserID,
            .NodePath = Sm.CREDIT_CARD_LOAN_PURPOSES(),
            .NodeGroup = SmSettings.NodeGroup.ManageCC,
            .NodeContent = SmUtil.BuildTextValueCatXml("CREDIT_CARD_LOAN_PURPOSES", loanPurposes)})
            Dim revisionID As Integer = Common.SafeInteger(Request.Form("revisionID"))
            Dim result = smBL.SaveNodesToDraft(revisionID, draftRecordList.ToArray()) ', cardNamesDraftRecord)
            If result Then
                Dim previewUrl As String = smBL.GeneratePreviewConfig(lenderConfigID, UserInfo.Email, Function(config, code) String.Format("{0}/cc/creditcard.aspx?lenderref={1}&autofill=true&previewCode={2}", _serverRoot, config.LenderRef, code), draftRecordList.ToArray())   ', cardNamesDraftRecord)
                res = New CJsonResponse(True, "", "OK")
                res.Info = New With {.previewurl = previewUrl}
            Else
                res = New CJsonResponse(False, "", "FAILED")
            End If
        Catch ex As Exception
            _log.Error("Could not PreviewManageCCDraft.", ex)
            res = New CJsonResponse(False, "Unable to save and preview draft. Please try again", "FAILED")
        End Try
        Response.Write(JsonConvert.SerializeObject(res))
        Response.End()
    End Sub
    Private Sub PublishManageCCDraft()
        Dim res As New CJsonResponse()
        Try
            Dim smBL As New SmBL()
            Dim oSerializer As New JavaScriptSerializer()
            Dim approvalMsg As String = Common.SafeString(Request.Form("approvalMsg"))
            Dim referredMsg As String = Common.SafeString(Request.Form("referredMsg"))
            Dim declinedMsg As String = Common.SafeString(Request.Form("declinedMsg"))
            Dim preQualifiedMsg As String = Common.SafeString(Request.Form("preQualifiedMsg"))
            Dim disclosures As List(Of String) = oSerializer.Deserialize(Of List(Of String))(Request.Form("disclosures"))
            Dim lenderConfigID As Guid = Common.SafeGUID(Request.Form("lenderconfigid"))
            Dim cardTypes As List(Of SmTextValuePair) = oSerializer.Deserialize(Of List(Of SmTextValuePair))(Request.Form("cardTypes"))
            Dim loanPurposes As List(Of SmTextValueCatItem) = oSerializer.Deserialize(Of List(Of SmTextValueCatItem))(Request.Form("loanPurposes"))
            'Dim cardNames As List(Of SmCreditCardNameItem) = oSerializer.Deserialize(Of List(Of SmCreditCardNameItem))(Request.Form("cardNames"))

            Dim draftRecordList As New List(Of SmLenderConfigDraft)
            If Not String.IsNullOrEmpty(approvalMsg) Then
                draftRecordList.Add(New SmLenderConfigDraft() With {
                .LenderConfigID = lenderConfigID,
                .CreatedByUserID = UserInfo.UserID,
                .NodePath = PREAPPROVED_MESSAGE("CREDIT_CARD_LOAN"),
                .NodeGroup = SmSettings.NodeGroup.ManageCC,
                .NodeContent = SmUtil.BuildSuccessMessageXml("SUCCESS_MESSAGES", "PREAPPROVED_MESSAGE", approvalMsg)})
            End If
            If Not String.IsNullOrEmpty(referredMsg) Then
                draftRecordList.Add(New SmLenderConfigDraft() With {
                    .LenderConfigID = lenderConfigID,
                    .CreatedByUserID = UserInfo.UserID,
                    .NodePath = SUBMITTED_MESSAGE("CREDIT_CARD_LOAN"),
                    .NodeGroup = SmSettings.NodeGroup.ManageCC,
                    .NodeContent = SmUtil.BuildSuccessMessageXml("SUCCESS_MESSAGES", "SUBMITTED_MESSAGE", referredMsg)})
            End If
            draftRecordList.Add(New SmLenderConfigDraft() With {
                .LenderConfigID = lenderConfigID,
                .CreatedByUserID = UserInfo.UserID,
                .NodePath = DECLINED_MESSAGE("CREDIT_CARD_LOAN"),
                .NodeGroup = SmSettings.NodeGroup.ManageCC,
                .NodeContent = SmUtil.BuildSuccessMessageXml("SUCCESS_MESSAGES", "DECLINED_MESSAGE", declinedMsg)})
            If Not String.IsNullOrEmpty(preQualifiedMsg) Then
                draftRecordList.Add(New SmLenderConfigDraft() With {
                    .LenderConfigID = lenderConfigID,
                    .CreatedByUserID = UserInfo.UserID,
                    .NodePath = PREQUALIFIED_MESSAGE("CREDIT_CARD_LOAN"),
                    .NodeGroup = SmSettings.NodeGroup.ManageCC,
                    .NodeContent = SmUtil.BuildSuccessMessageXml("SUCCESS_MESSAGES", "PREQUALIFIED_MESSAGE", preQualifiedMsg)})
            End If
            draftRecordList.Add(New SmLenderConfigDraft() With {
                .LenderConfigID = lenderConfigID,
                .CreatedByUserID = UserInfo.UserID,
                .NodePath = Sm.DISCLOSURES("CREDIT_CARD_LOAN"),
                .NodeGroup = SmSettings.NodeGroup.ManageCC,
                .NodeContent = SmUtil.BuildDisclosuresXml(disclosures)})
            draftRecordList.Add(New SmLenderConfigDraft() With {
                .LenderConfigID = lenderConfigID,
                .CreatedByUserID = UserInfo.UserID,
                .NodePath = Sm.CREDIT_CARD_TYPES(),
                .NodeGroup = SmSettings.NodeGroup.ManageCC,
                .NodeContent = SmUtil.BuildTextValueXml("CREDIT_CARD_TYPES", cardTypes)})
            'If Not String.IsNullOrEmpty(referredMsg) Then
            'draftRecordList.Add(New SmLenderConfigDraft() With {
            '	.LenderConfigID = lenderConfigID,
            '	
            '	.CreatedBy = UserInfo.Email,
            '	.NodePath = Sm.CREDIT_CARD_NAMES(),
            '	.NodeGroup = SmSettings.NodeGroup.ManageCC,
            '	.NodeContent = SmUtil.BuildCreditCardNameItemXml("CREDIT_CARD_NAMES", cardNames)})
            'End if
            draftRecordList.Add(New SmLenderConfigDraft() With {
            .LenderConfigID = lenderConfigID,
            .CreatedByUserID = UserInfo.UserID,
            .NodePath = Sm.CREDIT_CARD_LOAN_PURPOSES(),
            .NodeGroup = SmSettings.NodeGroup.ManageCC,
            .NodeContent = SmUtil.BuildTextValueCatXml("CREDIT_CARD_LOAN_PURPOSES", loanPurposes)})
            Dim comment As String = Common.SafeStripHtmlString(Request.Form("comment"))
            Dim publishAll As Boolean = Common.SafeBoolean(Request.Form("publishAll"))
            Dim result = smBL.PublishNodes(lenderConfigID, UserInfo.UserID, Request.UserHostAddress, comment, publishAll, draftRecordList.ToArray())    ', cardNamesDraftRecord)
            If result Then
                res = New CJsonResponse(True, "", "OK")
            Else
                res = New CJsonResponse(False, "", "FAILED")
            End If
        Catch ex As Exception
            _log.Error("Could not PublishManageCCDraft.", ex)
            res = New CJsonResponse(False, "Unable to publish data. Please try again", "FAILED")
        End Try
        Response.Write(JsonConvert.SerializeObject(res))
        Response.End()
    End Sub
    Private Sub SaveManageComboCCDraft()
        Dim res As New CJsonResponse()
        Try
            Dim smBL As New SmBL()
            Dim comboBothPreapprovedMsg As String = Common.SafeString(Request.Form("comboBothPreapprovedMsg"))
            Dim comboSubmittedMsg As String = Common.SafeString(Request.Form("comboSubmittedMsg"))
            Dim comboXAPreapprovedMsg As String = Common.SafeString(Request.Form("comboXAPreapprovedMsg"))
            Dim comboXASubmittedMsg As String = Common.SafeString(Request.Form("comboXASubmittedMsg"))
            Dim comboPreQualifiedMsg As String = Common.SafeString(Request.Form("comboPreQualifiedMsg"))
            Dim lenderConfigID As Guid = Common.SafeGUID(Request.Form("lenderconfigid"))
            Dim lenderConfig = smBL.GetLiveConfig(lenderConfigID)
            If lenderConfig Is Nothing Then
                Response.StatusCode = 404 '' Forbidden
                Response.StatusDescription = "Not found"
                Response.End() '' Terminate all the rest processes
            End If
            Dim apmComboEnable As Boolean = SmUtil.GetApmComboEnable(lenderConfig.ConfigData)
            If apmComboEnable = False Then
                Response.StatusCode = 404 '' Forbidden
                Response.StatusDescription = "Not found"
                Response.End() '' Terminate all the rest processes
            End If
            Dim draftRecordList As New List(Of SmLenderConfigDraft)
            If Not String.IsNullOrEmpty(comboBothPreapprovedMsg) Then
                draftRecordList.Add(New SmLenderConfigDraft() With {
                .LenderConfigID = lenderConfigID,
                .CreatedByUserID = UserInfo.UserID,
                .NodePath = COMBO_BOTH_PREAPPROVED_MESSAGE("CREDIT_CARD_LOAN"),
                .NodeGroup = SmSettings.NodeGroup.ManageComboCC,
                .NodeContent = SmUtil.BuildSuccessMessageXml("SUCCESS_MESSAGES", "COMBO_BOTH_PREAPPROVED_MESSAGE", comboBothPreapprovedMsg)})
            End If
            If Not String.IsNullOrEmpty(comboSubmittedMsg) Then
                draftRecordList.Add(New SmLenderConfigDraft() With {
                    .LenderConfigID = lenderConfigID,
                    .CreatedByUserID = UserInfo.UserID,
                    .NodePath = COMBO_SUBMITTED_MESSAGE("CREDIT_CARD_LOAN"),
                    .NodeGroup = SmSettings.NodeGroup.ManageComboCC,
                    .NodeContent = SmUtil.BuildSuccessMessageXml("SUCCESS_MESSAGES", "COMBO_SUBMITTED_MESSAGE", comboSubmittedMsg)})
            End If
            If Not String.IsNullOrEmpty(comboXAPreapprovedMsg) Then
                draftRecordList.Add(New SmLenderConfigDraft() With {
                    .LenderConfigID = lenderConfigID,
                    .CreatedByUserID = UserInfo.UserID,
                    .NodePath = COMBO_XA_PREAPPROVED_MESSAGE("CREDIT_CARD_LOAN"),
                    .NodeGroup = SmSettings.NodeGroup.ManageComboCC,
                    .NodeContent = SmUtil.BuildSuccessMessageXml("SUCCESS_MESSAGES", "COMBO_XA_PREAPPROVED_MESSAGE", comboXAPreapprovedMsg)})
            End If
            If Not String.IsNullOrEmpty(comboXASubmittedMsg) Then
                draftRecordList.Add(New SmLenderConfigDraft() With {
                    .LenderConfigID = lenderConfigID,
                    .CreatedByUserID = UserInfo.UserID,
                    .NodePath = COMBO_XA_SUBMITTED_MESSAGE("CREDIT_CARD_LOAN"),
                    .NodeGroup = SmSettings.NodeGroup.ManageComboCC,
                    .NodeContent = SmUtil.BuildSuccessMessageXml("SUCCESS_MESSAGES", "COMBO_XA_SUBMITTED_MESSAGE", comboXASubmittedMsg)})
            End If
            If Not String.IsNullOrEmpty(comboPreQualifiedMsg) Then
                draftRecordList.Add(New SmLenderConfigDraft() With {
                    .LenderConfigID = lenderConfigID,
                    .CreatedByUserID = UserInfo.UserID,
                    .NodePath = COMBO_PREQUALIFIED_MESSAGE("CREDIT_CARD_LOAN"),
                    .NodeGroup = SmSettings.NodeGroup.ManageComboCC,
                    .NodeContent = SmUtil.BuildSuccessMessageXml("SUCCESS_MESSAGES", "COMBO_PREQUALIFIED_MESSAGE", comboPreQualifiedMsg)})
            End If
            Dim revisionID As Integer = Common.SafeInteger(Request.Form("revisionID"))
            Dim result = smBL.SaveNodesToDraft(revisionID, draftRecordList.ToArray())
            If result Then
                res = New CJsonResponse(True, "", "OK")
            Else
                res = New CJsonResponse(False, "", "FAILED")
            End If
        Catch ex As Exception
            _log.Error("Could not SaveManageComboCCDraft.", ex)
            res = New CJsonResponse(False, "Unable to save draft. Please try again", "FAILED")
        End Try
        Response.Write(JsonConvert.SerializeObject(res))
        Response.End()
    End Sub
    Private Sub PreviewManageComboCCDraft()
        Dim res As New CJsonResponse()
        Try
            Dim smBL As New SmBL()
            Dim comboBothPreapprovedMsg As String = Common.SafeString(Request.Form("comboBothPreapprovedMsg"))
            Dim comboSubmittedMsg As String = Common.SafeString(Request.Form("comboSubmittedMsg"))
            Dim comboXAPreapprovedMsg As String = Common.SafeString(Request.Form("comboXAPreapprovedMsg"))
            Dim comboXASubmittedMsg As String = Common.SafeString(Request.Form("comboXASubmittedMsg"))
            Dim comboPreQualifiedMsg As String = Common.SafeString(Request.Form("comboPreQualifiedMsg"))
            Dim lenderConfigID As Guid = Common.SafeGUID(Request.Form("lenderconfigid"))
            Dim lenderConfig = smBL.GetLiveConfig(lenderConfigID)
            If lenderConfig Is Nothing Then
                Response.StatusCode = 404 '' Forbidden
                Response.StatusDescription = "Not found"
                Response.End() '' Terminate all the rest processes
            End If
            Dim apmComboEnable As Boolean = SmUtil.GetApmComboEnable(lenderConfig.ConfigData)
            If apmComboEnable = False Then
                Response.StatusCode = 404 '' Forbidden
                Response.StatusDescription = "Not found"
                Response.End() '' Terminate all the rest processes
            End If
            Dim draftRecordList As New List(Of SmLenderConfigDraft)
            If Not String.IsNullOrEmpty(comboBothPreapprovedMsg) Then
                draftRecordList.Add(New SmLenderConfigDraft() With {
                .LenderConfigID = lenderConfigID,
                .CreatedByUserID = UserInfo.UserID,
                .NodePath = COMBO_BOTH_PREAPPROVED_MESSAGE("CREDIT_CARD_LOAN"),
                .NodeGroup = SmSettings.NodeGroup.ManageComboCC,
                .NodeContent = SmUtil.BuildSuccessMessageXml("SUCCESS_MESSAGES", "COMBO_BOTH_PREAPPROVED_MESSAGE", comboBothPreapprovedMsg)})
            End If
            If Not String.IsNullOrEmpty(comboSubmittedMsg) Then
                draftRecordList.Add(New SmLenderConfigDraft() With {
                    .LenderConfigID = lenderConfigID,
                    .CreatedByUserID = UserInfo.UserID,
                    .NodePath = COMBO_SUBMITTED_MESSAGE("CREDIT_CARD_LOAN"),
                    .NodeGroup = SmSettings.NodeGroup.ManageComboCC,
                    .NodeContent = SmUtil.BuildSuccessMessageXml("SUCCESS_MESSAGES", "COMBO_SUBMITTED_MESSAGE", comboSubmittedMsg)})
            End If
            If Not String.IsNullOrEmpty(comboXAPreapprovedMsg) Then
                draftRecordList.Add(New SmLenderConfigDraft() With {
                    .LenderConfigID = lenderConfigID,
                    .CreatedByUserID = UserInfo.UserID,
                    .NodePath = COMBO_XA_PREAPPROVED_MESSAGE("CREDIT_CARD_LOAN"),
                    .NodeGroup = SmSettings.NodeGroup.ManageComboCC,
                    .NodeContent = SmUtil.BuildSuccessMessageXml("SUCCESS_MESSAGES", "COMBO_XA_PREAPPROVED_MESSAGE", comboXAPreapprovedMsg)})
            End If
            If Not String.IsNullOrEmpty(comboXASubmittedMsg) Then
                draftRecordList.Add(New SmLenderConfigDraft() With {
                    .LenderConfigID = lenderConfigID,
                    .CreatedByUserID = UserInfo.UserID,
                    .NodePath = COMBO_XA_SUBMITTED_MESSAGE("CREDIT_CARD_LOAN"),
                    .NodeGroup = SmSettings.NodeGroup.ManageComboCC,
                    .NodeContent = SmUtil.BuildSuccessMessageXml("SUCCESS_MESSAGES", "COMBO_XA_SUBMITTED_MESSAGE", comboXASubmittedMsg)})
            End If
            If Not String.IsNullOrEmpty(comboPreQualifiedMsg) Then
                draftRecordList.Add(New SmLenderConfigDraft() With {
                    .LenderConfigID = lenderConfigID,
                    .CreatedByUserID = UserInfo.UserID,
                    .NodePath = COMBO_PREQUALIFIED_MESSAGE("CREDIT_CARD_LOAN"),
                    .NodeGroup = SmSettings.NodeGroup.ManageComboCC,
                    .NodeContent = SmUtil.BuildSuccessMessageXml("SUCCESS_MESSAGES", "COMBO_PREQUALIFIED_MESSAGE", comboPreQualifiedMsg)})
            End If
            Dim revisionID As Integer = Common.SafeInteger(Request.Form("revisionID"))
            Dim result = smBL.SaveNodesToDraft(revisionID, draftRecordList.ToArray())
            If result Then
                Dim previewUrl As String = smBL.GeneratePreviewConfig(lenderConfigID, UserInfo.Email, Function(config, code) String.Format("{0}/cc/creditcard.aspx?lenderref={1}&type=1&autofill=true&previewCode={2}", _serverRoot, config.LenderRef, code), draftRecordList.ToArray())
                res = New CJsonResponse(True, "", "OK")
                res.Info = New With {.previewurl = previewUrl}
            Else
                res = New CJsonResponse(False, "", "FAILED")
            End If
        Catch ex As Exception
            _log.Error("Could not PreviewManageComboCCDraft.", ex)
            res = New CJsonResponse(False, "Unable to save and preview draft. Please try again", "FAILED")
        End Try
        Response.Write(JsonConvert.SerializeObject(res))
        Response.End()
    End Sub
    Private Sub PublishManageComboCCDraft()
        Dim res As New CJsonResponse()
        Try
            Dim smBL As New SmBL()
            Dim comboBothPreapprovedMsg As String = Common.SafeString(Request.Form("comboBothPreapprovedMsg"))
            Dim comboSubmittedMsg As String = Common.SafeString(Request.Form("comboSubmittedMsg"))
            Dim comboXAPreapprovedMsg As String = Common.SafeString(Request.Form("comboXAPreapprovedMsg"))
            Dim comboXASubmittedMsg As String = Common.SafeString(Request.Form("comboXASubmittedMsg"))
            Dim comboPreQualifiedMsg As String = Common.SafeString(Request.Form("comboPreQualifiedMsg"))
            Dim lenderConfigID As Guid = Common.SafeGUID(Request.Form("lenderconfigid"))
            Dim lenderConfig = smBL.GetLiveConfig(lenderConfigID)
            If lenderConfig Is Nothing Then
                Response.StatusCode = 404 '' Forbidden
                Response.StatusDescription = "Not found"
                Response.End() '' Terminate all the rest processes
            End If
            Dim apmComboEnable As Boolean = SmUtil.GetApmComboEnable(lenderConfig.ConfigData)
            If apmComboEnable = False Then
                Response.StatusCode = 404 '' Forbidden
                Response.StatusDescription = "Not found"
                Response.End() '' Terminate all the rest processes
            End If
            Dim draftRecordList As New List(Of SmLenderConfigDraft)
            If Not String.IsNullOrEmpty(comboBothPreapprovedMsg) Then
                draftRecordList.Add(New SmLenderConfigDraft() With {
                .LenderConfigID = lenderConfigID,
                .CreatedByUserID = UserInfo.UserID,
                .NodePath = COMBO_BOTH_PREAPPROVED_MESSAGE("CREDIT_CARD_LOAN"),
                .NodeGroup = SmSettings.NodeGroup.ManageComboCC,
                .NodeContent = SmUtil.BuildSuccessMessageXml("SUCCESS_MESSAGES", "COMBO_BOTH_PREAPPROVED_MESSAGE", comboBothPreapprovedMsg)})
            End If
            If Not String.IsNullOrEmpty(comboSubmittedMsg) Then
                draftRecordList.Add(New SmLenderConfigDraft() With {
                    .LenderConfigID = lenderConfigID,
                    .CreatedByUserID = UserInfo.UserID,
                    .NodePath = COMBO_SUBMITTED_MESSAGE("CREDIT_CARD_LOAN"),
                    .NodeGroup = SmSettings.NodeGroup.ManageComboCC,
                    .NodeContent = SmUtil.BuildSuccessMessageXml("SUCCESS_MESSAGES", "COMBO_SUBMITTED_MESSAGE", comboSubmittedMsg)})
            End If
            If Not String.IsNullOrEmpty(comboXAPreapprovedMsg) Then
                draftRecordList.Add(New SmLenderConfigDraft() With {
                    .LenderConfigID = lenderConfigID,
                    .CreatedByUserID = UserInfo.UserID,
                    .NodePath = COMBO_XA_PREAPPROVED_MESSAGE("CREDIT_CARD_LOAN"),
                    .NodeGroup = SmSettings.NodeGroup.ManageComboCC,
                    .NodeContent = SmUtil.BuildSuccessMessageXml("SUCCESS_MESSAGES", "COMBO_XA_PREAPPROVED_MESSAGE", comboXAPreapprovedMsg)})
            End If
            If Not String.IsNullOrEmpty(comboXASubmittedMsg) Then
                draftRecordList.Add(New SmLenderConfigDraft() With {
                    .LenderConfigID = lenderConfigID,
                    .CreatedByUserID = UserInfo.UserID,
                    .NodePath = COMBO_XA_SUBMITTED_MESSAGE("CREDIT_CARD_LOAN"),
                    .NodeGroup = SmSettings.NodeGroup.ManageComboCC,
                    .NodeContent = SmUtil.BuildSuccessMessageXml("SUCCESS_MESSAGES", "COMBO_XA_SUBMITTED_MESSAGE", comboXASubmittedMsg)})
            End If
            If Not String.IsNullOrEmpty(comboPreQualifiedMsg) Then
                draftRecordList.Add(New SmLenderConfigDraft() With {
                    .LenderConfigID = lenderConfigID,
                    .CreatedByUserID = UserInfo.UserID,
                    .NodePath = COMBO_PREQUALIFIED_MESSAGE("CREDIT_CARD_LOAN"),
                    .NodeGroup = SmSettings.NodeGroup.ManageComboCC,
                    .NodeContent = SmUtil.BuildSuccessMessageXml("SUCCESS_MESSAGES", "COMBO_PREQUALIFIED_MESSAGE", comboPreQualifiedMsg)})
            End If
            Dim comment As String = Common.SafeStripHtmlString(Request.Form("comment"))
            Dim publishAll As Boolean = Common.SafeBoolean(Request.Form("publishAll"))
            Dim result = smBL.PublishNodes(lenderConfigID, UserInfo.UserID, Request.UserHostAddress, comment, publishAll, draftRecordList.ToArray())
            If result Then
                res = New CJsonResponse(True, "", "OK")
            Else
                res = New CJsonResponse(False, "", "FAILED")
            End If
        Catch ex As Exception
            _log.Error("Could not PublishManageComboCCDraft.", ex)
            res = New CJsonResponse(False, "Unable to publish data. Please try again", "FAILED")
        End Try
        Response.Write(JsonConvert.SerializeObject(res))
        Response.End()
    End Sub
#End Region
#Region "HE"
    Private Sub SaveManageHEDraft()
        Dim res As New CJsonResponse()
        Try
            Dim smBL As New SmBL()
            Dim oSerializer As New JavaScriptSerializer()
            Dim approvalMsg As String = Common.SafeString(Request.Form("approvalMsg"))
            Dim referredMsg As String = Common.SafeString(Request.Form("referredMsg"))
            Dim declinedMsg As String = Common.SafeString(Request.Form("declinedMsg"))
			Dim preQualifiedMsg As String = Common.SafeString(Request.Form("preQualifiedMsg"))
			Dim demographicDescription As String = Common.SafeString(Request.Form("demographicDescription"))
			Dim demographicDisclosure As String = Common.SafeString(Request.Form("demographicDisclosure"))
			Dim disclosures As List(Of String) = oSerializer.Deserialize(Of List(Of String))(Request.Form("disclosures"))
            Dim disclosuresLoc As List(Of String) = oSerializer.Deserialize(Of List(Of String))(Request.Form("disclosuresLoc"))
            Dim lenderConfigID As Guid = Common.SafeGUID(Request.Form("lenderconfigid"))
            Dim loanPurposes As List(Of SmTextValuePair) = oSerializer.Deserialize(Of List(Of SmTextValuePair))(Request.Form("loanPurposes"))
            Dim loanReasons As List(Of SmTextValuePair) = oSerializer.Deserialize(Of List(Of SmTextValuePair))(Request.Form("loanReasons"))
            Dim loanProperties As List(Of SmTextValuePair) = oSerializer.Deserialize(Of List(Of SmTextValuePair))(Request.Form("loanProperties"))
            Dim draftRecordList As New List(Of SmLenderConfigDraft)
            If Not String.IsNullOrEmpty(approvalMsg) Then
                draftRecordList.Add(New SmLenderConfigDraft() With {
                .LenderConfigID = lenderConfigID,
                .CreatedByUserID = UserInfo.UserID,
                .NodePath = PREAPPROVED_MESSAGE("HOME_EQUITY_LOAN"),
                .NodeGroup = SmSettings.NodeGroup.ManageHE,
                .NodeContent = SmUtil.BuildSuccessMessageXml("SUCCESS_MESSAGES", "PREAPPROVED_MESSAGE", approvalMsg)})
            End If
            If Not String.IsNullOrEmpty(referredMsg) Then
                draftRecordList.Add(New SmLenderConfigDraft() With {
                    .LenderConfigID = lenderConfigID,
                    .CreatedByUserID = UserInfo.UserID,
                    .NodePath = SUBMITTED_MESSAGE("HOME_EQUITY_LOAN"),
                    .NodeGroup = SmSettings.NodeGroup.ManageHE,
                    .NodeContent = SmUtil.BuildSuccessMessageXml("SUCCESS_MESSAGES", "SUBMITTED_MESSAGE", referredMsg)})
            End If
            draftRecordList.Add(New SmLenderConfigDraft() With {
                .LenderConfigID = lenderConfigID,
                .CreatedByUserID = UserInfo.UserID,
                .NodePath = DECLINED_MESSAGE("HOME_EQUITY_LOAN"),
                .NodeGroup = SmSettings.NodeGroup.ManageHE,
                .NodeContent = SmUtil.BuildSuccessMessageXml("SUCCESS_MESSAGES", "DECLINED_MESSAGE", declinedMsg)})
			If Not String.IsNullOrEmpty(preQualifiedMsg) Then
				draftRecordList.Add(New SmLenderConfigDraft() With {
					.LenderConfigID = lenderConfigID,
					.CreatedByUserID = UserInfo.UserID,
					.NodePath = PREQUALIFIED_MESSAGE("HOME_EQUITY_LOAN"),
					.NodeGroup = SmSettings.NodeGroup.ManageHE,
					.NodeContent = SmUtil.BuildSuccessMessageXml("SUCCESS_MESSAGES", "PREQUALIFIED_MESSAGE", preQualifiedMsg)})
			End If
			If Not String.IsNullOrEmpty(demographicDescription) Then
				draftRecordList.Add(New SmLenderConfigDraft() With {
					.LenderConfigID = lenderConfigID,
					.CreatedByUserID = UserInfo.UserID,
					.NodePath = DEMOGRAPHIC_DESCRIPTION_MESSAGE("HOME_EQUITY_LOAN"),
					.NodeGroup = SmSettings.NodeGroup.ManageHE,
					.NodeContent = SmUtil.BuildSuccessMessageXml("SUCCESS_MESSAGES", "DEMOGRAPHIC_DESCRIPTION_MESSAGE", demographicDescription)
				})
			End If
			If Not String.IsNullOrEmpty(demographicDisclosure) Then
				draftRecordList.Add(New SmLenderConfigDraft() With {
					.LenderConfigID = lenderConfigID,
					.CreatedByUserID = UserInfo.UserID,
					.NodePath = DEMOGRAPHIC_DISCLOSURE_MESSAGE("HOME_EQUITY_LOAN"),
					.NodeGroup = SmSettings.NodeGroup.ManageHE,
					.NodeContent = SmUtil.BuildSuccessMessageXml("SUCCESS_MESSAGES", "DEMOGRAPHIC_DISCLOSURE_MESSAGE", demographicDisclosure)
				})
			End If
			draftRecordList.Add(New SmLenderConfigDraft() With {
                .LenderConfigID = lenderConfigID,
                .CreatedByUserID = UserInfo.UserID,
                .NodePath = Sm.DISCLOSURES("HOME_EQUITY_LOAN"),
                .NodeGroup = SmSettings.NodeGroup.ManageHE,
                .NodeContent = SmUtil.BuildDisclosuresXml(disclosures)})
            draftRecordList.Add(New SmLenderConfigDraft() With {
                .LenderConfigID = lenderConfigID,
                .CreatedByUserID = UserInfo.UserID,
                .NodePath = Sm.DISCLOSURES_LOC("HOME_EQUITY_LOAN"),
                .NodeGroup = SmSettings.NodeGroup.ManageHE,
                .NodeContent = SmUtil.BuildDisclosuresXml(disclosuresLoc, "DISCLOSURES_LOC")})
            draftRecordList.Add(New SmLenderConfigDraft() With {
                .LenderConfigID = lenderConfigID,
                .CreatedByUserID = UserInfo.UserID,
                .NodePath = Sm.HOME_EQUITY_LOAN_REASONS(),
                .NodeGroup = SmSettings.NodeGroup.ManageHE,
                .NodeContent = SmUtil.BuildTextValueXml("HOME_EQUITY_LOAN_REASONS", loanReasons)})
            draftRecordList.Add(New SmLenderConfigDraft() With {
                .LenderConfigID = lenderConfigID,
                .CreatedByUserID = UserInfo.UserID,
                .NodePath = Sm.PROPERTY_TYPES(),
                .NodeGroup = SmSettings.NodeGroup.ManageHE,
                .NodeContent = SmUtil.BuildTextValueXml("PROPERTY_TYPES", loanProperties)})
            draftRecordList.Add(New SmLenderConfigDraft() With {
                .LenderConfigID = lenderConfigID,
                .CreatedByUserID = UserInfo.UserID,
                .NodePath = Sm.HOME_EQUITY_LOAN_PURPOSES(),
                .NodeGroup = SmSettings.NodeGroup.ManageHE,
                .NodeContent = SmUtil.BuildTextValueXml("HOME_EQUITY_LOAN_PURPOSES", loanPurposes)})
            Dim revisionID As Integer = Common.SafeInteger(Request.Form("revisionID"))
            Dim result = smBL.SaveNodesToDraft(revisionID, draftRecordList.ToArray())
            If result Then
                res = New CJsonResponse(True, "", "OK")
            Else
                res = New CJsonResponse(False, "", "FAILED")
            End If
        Catch ex As Exception
            _log.Error("Could not SaveManageHEDraft.", ex)
            res = New CJsonResponse(False, "Unable to save draft. Please try again", "FAILED")
        End Try
        Response.Write(JsonConvert.SerializeObject(res))
        Response.End()
    End Sub
    Private Sub PreviewManageHEDraft()
        Dim res As New CJsonResponse()
        Try
            Dim smBL As New SmBL()
            Dim oSerializer As New JavaScriptSerializer()
            Dim approvalMsg As String = Common.SafeString(Request.Form("approvalMsg"))
            Dim referredMsg As String = Common.SafeString(Request.Form("referredMsg"))
            Dim declinedMsg As String = Common.SafeString(Request.Form("declinedMsg"))
			Dim preQualifiedMsg As String = Common.SafeString(Request.Form("preQualifiedMsg"))
			Dim demographicDescription As String = Common.SafeString(Request.Form("demographicDescription"))
			Dim demographicDisclosure As String = Common.SafeString(Request.Form("demographicDisclosure"))
			Dim disclosures As List(Of String) = oSerializer.Deserialize(Of List(Of String))(Request.Form("disclosures"))
            Dim disclosuresLoc As List(Of String) = oSerializer.Deserialize(Of List(Of String))(Request.Form("disclosuresLoc"))
            Dim lenderConfigID As Guid = Common.SafeGUID(Request.Form("lenderconfigid"))
            Dim loanPurposes As List(Of SmTextValuePair) = oSerializer.Deserialize(Of List(Of SmTextValuePair))(Request.Form("loanPurposes"))
            Dim loanReasons As List(Of SmTextValuePair) = oSerializer.Deserialize(Of List(Of SmTextValuePair))(Request.Form("loanReasons"))
            Dim loanProperties As List(Of SmTextValuePair) = oSerializer.Deserialize(Of List(Of SmTextValuePair))(Request.Form("loanProperties"))
            Dim draftRecordList As New List(Of SmLenderConfigDraft)
            If Not String.IsNullOrEmpty(approvalMsg) Then
                draftRecordList.Add(New SmLenderConfigDraft() With {
                .LenderConfigID = lenderConfigID,
                .CreatedByUserID = UserInfo.UserID,
                .NodePath = PREAPPROVED_MESSAGE("HOME_EQUITY_LOAN"),
                .NodeGroup = SmSettings.NodeGroup.ManageHE,
                .NodeContent = SmUtil.BuildSuccessMessageXml("SUCCESS_MESSAGES", "PREAPPROVED_MESSAGE", approvalMsg)})
            End If
            If Not String.IsNullOrEmpty(referredMsg) Then
                draftRecordList.Add(New SmLenderConfigDraft() With {
                    .LenderConfigID = lenderConfigID,
                    .CreatedByUserID = UserInfo.UserID,
                    .NodePath = SUBMITTED_MESSAGE("HOME_EQUITY_LOAN"),
                    .NodeGroup = SmSettings.NodeGroup.ManageHE,
                    .NodeContent = SmUtil.BuildSuccessMessageXml("SUCCESS_MESSAGES", "SUBMITTED_MESSAGE", referredMsg)})
            End If
            draftRecordList.Add(New SmLenderConfigDraft() With {
                .LenderConfigID = lenderConfigID,
                .CreatedByUserID = UserInfo.UserID,
                .NodePath = DECLINED_MESSAGE("HOME_EQUITY_LOAN"),
                .NodeGroup = SmSettings.NodeGroup.ManageHE,
                .NodeContent = SmUtil.BuildSuccessMessageXml("SUCCESS_MESSAGES", "DECLINED_MESSAGE", declinedMsg)})
			If Not String.IsNullOrEmpty(preQualifiedMsg) Then
				draftRecordList.Add(New SmLenderConfigDraft() With {
					.LenderConfigID = lenderConfigID,
					.CreatedByUserID = UserInfo.UserID,
					.NodePath = PREQUALIFIED_MESSAGE("HOME_EQUITY_LOAN"),
					.NodeGroup = SmSettings.NodeGroup.ManageHE,
					.NodeContent = SmUtil.BuildSuccessMessageXml("SUCCESS_MESSAGES", "PREQUALIFIED_MESSAGE", preQualifiedMsg)})
			End If
			If Not String.IsNullOrEmpty(demographicDescription) Then
				draftRecordList.Add(New SmLenderConfigDraft() With {
					.LenderConfigID = lenderConfigID,
					.CreatedByUserID = UserInfo.UserID,
					.NodePath = DEMOGRAPHIC_DESCRIPTION_MESSAGE("HOME_EQUITY_LOAN"),
					.NodeGroup = SmSettings.NodeGroup.ManageHE,
					.NodeContent = SmUtil.BuildSuccessMessageXml("SUCCESS_MESSAGES", "DEMOGRAPHIC_DESCRIPTION_MESSAGE", demographicDescription)
				})
			End If
			If Not String.IsNullOrEmpty(demographicDisclosure) Then
				draftRecordList.Add(New SmLenderConfigDraft() With {
					.LenderConfigID = lenderConfigID,
					.CreatedByUserID = UserInfo.UserID,
					.NodePath = DEMOGRAPHIC_DISCLOSURE_MESSAGE("HOME_EQUITY_LOAN"),
					.NodeGroup = SmSettings.NodeGroup.ManageHE,
					.NodeContent = SmUtil.BuildSuccessMessageXml("SUCCESS_MESSAGES", "DEMOGRAPHIC_DISCLOSURE_MESSAGE", demographicDisclosure)
				})
			End If
			draftRecordList.Add(New SmLenderConfigDraft() With {
                .LenderConfigID = lenderConfigID,
                .CreatedByUserID = UserInfo.UserID,
                .NodePath = Sm.DISCLOSURES("HOME_EQUITY_LOAN"),
                .NodeGroup = SmSettings.NodeGroup.ManageHE,
                .NodeContent = SmUtil.BuildDisclosuresXml(disclosures)})
            draftRecordList.Add(New SmLenderConfigDraft() With {
                .LenderConfigID = lenderConfigID,
                .CreatedByUserID = UserInfo.UserID,
                .NodePath = Sm.DISCLOSURES_LOC("HOME_EQUITY_LOAN"),
                .NodeGroup = SmSettings.NodeGroup.ManageHE,
                .NodeContent = SmUtil.BuildDisclosuresXml(disclosuresLoc, "DISCLOSURES_LOC")})
            draftRecordList.Add(New SmLenderConfigDraft() With {
                .LenderConfigID = lenderConfigID,
                .CreatedByUserID = UserInfo.UserID,
                .NodePath = Sm.HOME_EQUITY_LOAN_REASONS(),
                .NodeGroup = SmSettings.NodeGroup.ManageHE,
                .NodeContent = SmUtil.BuildTextValueXml("HOME_EQUITY_LOAN_REASONS", loanReasons)})
            draftRecordList.Add(New SmLenderConfigDraft() With {
                .LenderConfigID = lenderConfigID,
                .CreatedByUserID = UserInfo.UserID,
                .NodePath = Sm.PROPERTY_TYPES(),
                .NodeGroup = SmSettings.NodeGroup.ManageHE,
                .NodeContent = SmUtil.BuildTextValueXml("PROPERTY_TYPES", loanProperties)})
            draftRecordList.Add(New SmLenderConfigDraft() With {
                .LenderConfigID = lenderConfigID,
                .CreatedByUserID = UserInfo.UserID,
                .NodePath = Sm.HOME_EQUITY_LOAN_PURPOSES(),
                .NodeGroup = SmSettings.NodeGroup.ManageHE,
                .NodeContent = SmUtil.BuildTextValueXml("HOME_EQUITY_LOAN_PURPOSES", loanPurposes)})
            Dim revisionID As Integer = Common.SafeInteger(Request.Form("revisionID"))
            Dim result = smBL.SaveNodesToDraft(revisionID, draftRecordList.ToArray())
            If result Then
                Dim previewUrl As String = smBL.GeneratePreviewConfig(lenderConfigID, UserInfo.Email, Function(config, code) String.Format("{0}/he/homeequityloan.aspx?lenderref={1}&autofill=true&previewCode={2}", _serverRoot, config.LenderRef, code), draftRecordList.ToArray())
                res = New CJsonResponse(True, "", "OK")
                res.Info = New With {.previewurl = previewUrl}
            Else
                res = New CJsonResponse(False, "", "FAILED")
            End If
        Catch ex As Exception
            _log.Error("Could not PreviewManageHEDraft.", ex)
            res = New CJsonResponse(False, "Unable to save and preview draft. Please try again", "FAILED")
        End Try
        Response.Write(JsonConvert.SerializeObject(res))
        Response.End()
    End Sub
    Private Sub PublishManageHEDraft()
        Dim res As New CJsonResponse()
        Try
            Dim smBL As New SmBL()
            Dim oSerializer As New JavaScriptSerializer()
            Dim approvalMsg As String = Common.SafeString(Request.Form("approvalMsg"))
            Dim referredMsg As String = Common.SafeString(Request.Form("referredMsg"))
            Dim declinedMsg As String = Common.SafeString(Request.Form("declinedMsg"))
			Dim preQualifiedMsg As String = Common.SafeString(Request.Form("preQualifiedMsg"))
			Dim demographicDescription As String = Common.SafeString(Request.Form("demographicDescription"))
			Dim demographicDisclosure As String = Common.SafeString(Request.Form("demographicDisclosure"))
			Dim disclosures As List(Of String) = oSerializer.Deserialize(Of List(Of String))(Request.Form("disclosures"))
            Dim disclosuresLoc As List(Of String) = oSerializer.Deserialize(Of List(Of String))(Request.Form("disclosuresLoc"))
            Dim lenderConfigID As Guid = Common.SafeGUID(Request.Form("lenderconfigid"))
            Dim loanPurposes As List(Of SmTextValuePair) = oSerializer.Deserialize(Of List(Of SmTextValuePair))(Request.Form("loanPurposes"))
            Dim loanReasons As List(Of SmTextValuePair) = oSerializer.Deserialize(Of List(Of SmTextValuePair))(Request.Form("loanReasons"))
            Dim loanProperties As List(Of SmTextValuePair) = oSerializer.Deserialize(Of List(Of SmTextValuePair))(Request.Form("loanProperties"))
            Dim draftRecordList As New List(Of SmLenderConfigDraft)
            If Not String.IsNullOrEmpty(approvalMsg) Then
                draftRecordList.Add(New SmLenderConfigDraft() With {
                .LenderConfigID = lenderConfigID,
                .CreatedByUserID = UserInfo.UserID,
                .NodePath = PREAPPROVED_MESSAGE("HOME_EQUITY_LOAN"),
                .NodeGroup = SmSettings.NodeGroup.ManageHE,
                .NodeContent = SmUtil.BuildSuccessMessageXml("SUCCESS_MESSAGES", "PREAPPROVED_MESSAGE", approvalMsg)})
            End If
            If Not String.IsNullOrEmpty(referredMsg) Then
                draftRecordList.Add(New SmLenderConfigDraft() With {
                    .LenderConfigID = lenderConfigID,
                    .CreatedByUserID = UserInfo.UserID,
                    .NodePath = SUBMITTED_MESSAGE("HOME_EQUITY_LOAN"),
                    .NodeGroup = SmSettings.NodeGroup.ManageHE,
                    .NodeContent = SmUtil.BuildSuccessMessageXml("SUCCESS_MESSAGES", "SUBMITTED_MESSAGE", referredMsg)})
            End If
            draftRecordList.Add(New SmLenderConfigDraft() With {
                .LenderConfigID = lenderConfigID,
                .CreatedByUserID = UserInfo.UserID,
                .NodePath = DECLINED_MESSAGE("HOME_EQUITY_LOAN"),
                .NodeGroup = SmSettings.NodeGroup.ManageHE,
                .NodeContent = SmUtil.BuildSuccessMessageXml("SUCCESS_MESSAGES", "DECLINED_MESSAGE", declinedMsg)})
			If Not String.IsNullOrEmpty(preQualifiedMsg) Then
				draftRecordList.Add(New SmLenderConfigDraft() With {
					.LenderConfigID = lenderConfigID,
					.CreatedByUserID = UserInfo.UserID,
					.NodePath = PREQUALIFIED_MESSAGE("HOME_EQUITY_LOAN"),
					.NodeGroup = SmSettings.NodeGroup.ManageHE,
					.NodeContent = SmUtil.BuildSuccessMessageXml("SUCCESS_MESSAGES", "PREQUALIFIED_MESSAGE", preQualifiedMsg)})
			End If
			If Not String.IsNullOrEmpty(demographicDescription) Then
				draftRecordList.Add(New SmLenderConfigDraft() With {
					.LenderConfigID = lenderConfigID,
					.CreatedByUserID = UserInfo.UserID,
					.NodePath = DEMOGRAPHIC_DESCRIPTION_MESSAGE("HOME_EQUITY_LOAN"),
					.NodeGroup = SmSettings.NodeGroup.ManageHE,
					.NodeContent = SmUtil.BuildSuccessMessageXml("SUCCESS_MESSAGES", "DEMOGRAPHIC_DESCRIPTION_MESSAGE", demographicDescription)
				})
			End If
			If Not String.IsNullOrEmpty(demographicDisclosure) Then
				draftRecordList.Add(New SmLenderConfigDraft() With {
					.LenderConfigID = lenderConfigID,
					.CreatedByUserID = UserInfo.UserID,
					.NodePath = DEMOGRAPHIC_DISCLOSURE_MESSAGE("HOME_EQUITY_LOAN"),
					.NodeGroup = SmSettings.NodeGroup.ManageHE,
					.NodeContent = SmUtil.BuildSuccessMessageXml("SUCCESS_MESSAGES", "DEMOGRAPHIC_DISCLOSURE_MESSAGE", demographicDisclosure)
				})
			End If
			draftRecordList.Add(New SmLenderConfigDraft() With {
                .LenderConfigID = lenderConfigID,
                .CreatedByUserID = UserInfo.UserID,
                .NodePath = Sm.DISCLOSURES("HOME_EQUITY_LOAN"),
                .NodeGroup = SmSettings.NodeGroup.ManageHE,
                .NodeContent = SmUtil.BuildDisclosuresXml(disclosures)})
            draftRecordList.Add(New SmLenderConfigDraft() With {
                .LenderConfigID = lenderConfigID,
                .CreatedByUserID = UserInfo.UserID,
                .NodePath = Sm.DISCLOSURES_LOC("HOME_EQUITY_LOAN"),
                .NodeGroup = SmSettings.NodeGroup.ManageHE,
                .NodeContent = SmUtil.BuildDisclosuresXml(disclosuresLoc, "DISCLOSURES_LOC")})
            draftRecordList.Add(New SmLenderConfigDraft() With {
                .LenderConfigID = lenderConfigID,
                .CreatedByUserID = UserInfo.UserID,
                .NodePath = Sm.HOME_EQUITY_LOAN_REASONS(),
                .NodeGroup = SmSettings.NodeGroup.ManageHE,
                .NodeContent = SmUtil.BuildTextValueXml("HOME_EQUITY_LOAN_REASONS", loanReasons)})
            draftRecordList.Add(New SmLenderConfigDraft() With {
                .LenderConfigID = lenderConfigID,
                .CreatedByUserID = UserInfo.UserID,
                .NodePath = Sm.PROPERTY_TYPES(),
                .NodeGroup = SmSettings.NodeGroup.ManageHE,
                .NodeContent = SmUtil.BuildTextValueXml("PROPERTY_TYPES", loanProperties)})
            draftRecordList.Add(New SmLenderConfigDraft() With {
                .LenderConfigID = lenderConfigID,
                .CreatedByUserID = UserInfo.UserID,
                .NodePath = Sm.HOME_EQUITY_LOAN_PURPOSES(),
                .NodeGroup = SmSettings.NodeGroup.ManageHE,
                .NodeContent = SmUtil.BuildTextValueXml("HOME_EQUITY_LOAN_PURPOSES", loanPurposes)})
            Dim comment As String = Common.SafeStripHtmlString(Request.Form("comment"))
            Dim publishAll As Boolean = Common.SafeBoolean(Request.Form("publishAll"))
            Dim result = smBL.PublishNodes(lenderConfigID, UserInfo.UserID, Request.UserHostAddress, comment, publishAll, draftRecordList.ToArray())
            If result Then
                res = New CJsonResponse(True, "", "OK")
            Else
                res = New CJsonResponse(False, "", "FAILED")
            End If
        Catch ex As Exception
            _log.Error("Could not PublishManageHEDraft.", ex)
            res = New CJsonResponse(False, "Unable to publish data. Please try again", "FAILED")
        End Try
        Response.Write(JsonConvert.SerializeObject(res))
        Response.End()
    End Sub
    Private Sub SaveManageComboHEDraft()
        Dim res As New CJsonResponse()
        Try
            Dim smBL As New SmBL()
            Dim comboBothPreapprovedMsg As String = Common.SafeString(Request.Form("comboBothPreapprovedMsg"))
            Dim comboSubmittedMsg As String = Common.SafeString(Request.Form("comboSubmittedMsg"))
            Dim comboXAPreapprovedMsg As String = Common.SafeString(Request.Form("comboXAPreapprovedMsg"))
            Dim comboXASubmittedMsg As String = Common.SafeString(Request.Form("comboXASubmittedMsg"))
            Dim comboPreQualifiedMsg As String = Common.SafeString(Request.Form("comboPreQualifiedMsg"))
            Dim lenderConfigID As Guid = Common.SafeGUID(Request.Form("lenderconfigid"))
            Dim lenderConfig = smBL.GetLiveConfig(lenderConfigID)
            If lenderConfig Is Nothing Then
                Response.StatusCode = 404 '' Forbidden
                Response.StatusDescription = "Not found"
                Response.End() '' Terminate all the rest processes
            End If
            Dim apmComboEnable As Boolean = SmUtil.GetApmComboEnable(lenderConfig.ConfigData)
            If apmComboEnable = False Then
                Response.StatusCode = 404 '' Forbidden
                Response.StatusDescription = "Not found"
                Response.End() '' Terminate all the rest processes
            End If
            Dim draftRecordList As New List(Of SmLenderConfigDraft)
            If Not String.IsNullOrEmpty(comboBothPreapprovedMsg) Then
                draftRecordList.Add(New SmLenderConfigDraft() With {
                .LenderConfigID = lenderConfigID,
                .CreatedByUserID = UserInfo.UserID,
                .NodePath = COMBO_BOTH_PREAPPROVED_MESSAGE("HOME_EQUITY_LOAN"),
                .NodeGroup = SmSettings.NodeGroup.ManageComboHE,
                .NodeContent = SmUtil.BuildSuccessMessageXml("SUCCESS_MESSAGES", "COMBO_BOTH_PREAPPROVED_MESSAGE", comboBothPreapprovedMsg)})
            End If
            If Not String.IsNullOrEmpty(comboSubmittedMsg) Then
                draftRecordList.Add(New SmLenderConfigDraft() With {
                    .LenderConfigID = lenderConfigID,
                    .CreatedByUserID = UserInfo.UserID,
                    .NodePath = COMBO_SUBMITTED_MESSAGE("HOME_EQUITY_LOAN"),
                    .NodeGroup = SmSettings.NodeGroup.ManageComboHE,
                    .NodeContent = SmUtil.BuildSuccessMessageXml("SUCCESS_MESSAGES", "COMBO_SUBMITTED_MESSAGE", comboSubmittedMsg)})
            End If
            If Not String.IsNullOrEmpty(comboXAPreapprovedMsg) Then
                draftRecordList.Add(New SmLenderConfigDraft() With {
                    .LenderConfigID = lenderConfigID,
                    .CreatedByUserID = UserInfo.UserID,
                    .NodePath = COMBO_XA_PREAPPROVED_MESSAGE("HOME_EQUITY_LOAN"),
                    .NodeGroup = SmSettings.NodeGroup.ManageComboHE,
                    .NodeContent = SmUtil.BuildSuccessMessageXml("SUCCESS_MESSAGES", "COMBO_XA_PREAPPROVED_MESSAGE", comboXAPreapprovedMsg)})
            End If
            If Not String.IsNullOrEmpty(comboXASubmittedMsg) Then
                draftRecordList.Add(New SmLenderConfigDraft() With {
                    .LenderConfigID = lenderConfigID,
                    .CreatedByUserID = UserInfo.UserID,
                    .NodePath = COMBO_XA_SUBMITTED_MESSAGE("HOME_EQUITY_LOAN"),
                    .NodeGroup = SmSettings.NodeGroup.ManageComboHE,
                    .NodeContent = SmUtil.BuildSuccessMessageXml("SUCCESS_MESSAGES", "COMBO_XA_SUBMITTED_MESSAGE", comboXASubmittedMsg)})
            End If
            If Not String.IsNullOrEmpty(comboPreQualifiedMsg) Then
                draftRecordList.Add(New SmLenderConfigDraft() With {
                    .LenderConfigID = lenderConfigID,
                    .CreatedByUserID = UserInfo.UserID,
                    .NodePath = COMBO_PREQUALIFIED_MESSAGE("HOME_EQUITY_LOAN"),
                    .NodeGroup = SmSettings.NodeGroup.ManageComboHE,
                    .NodeContent = SmUtil.BuildSuccessMessageXml("SUCCESS_MESSAGES", "COMBO_PREQUALIFIED_MESSAGE", comboPreQualifiedMsg)})
            End If
            Dim revisionID As Integer = Common.SafeInteger(Request.Form("revisionID"))
            Dim result = smBL.SaveNodesToDraft(revisionID, draftRecordList.ToArray())
            If result Then
                res = New CJsonResponse(True, "", "OK")
            Else
                res = New CJsonResponse(False, "", "FAILED")
            End If
        Catch ex As Exception
            _log.Error("Could not SaveManageComboHEDraft.", ex)
            res = New CJsonResponse(False, "Unable to save draft. Please try again", "FAILED")
        End Try
        Response.Write(JsonConvert.SerializeObject(res))
        Response.End()
    End Sub
    Private Sub PreviewManageComboHEDraft()
        Dim res As New CJsonResponse()
        Try
            Dim smBL As New SmBL()
            Dim comboBothPreapprovedMsg As String = Common.SafeString(Request.Form("comboBothPreapprovedMsg"))
            Dim comboSubmittedMsg As String = Common.SafeString(Request.Form("comboSubmittedMsg"))
            Dim comboXAPreapprovedMsg As String = Common.SafeString(Request.Form("comboXAPreapprovedMsg"))
            Dim comboXASubmittedMsg As String = Common.SafeString(Request.Form("comboXASubmittedMsg"))
            Dim comboPreQualifiedMsg As String = Common.SafeString(Request.Form("comboPreQualifiedMsg"))
            Dim lenderConfigID As Guid = Common.SafeGUID(Request.Form("lenderconfigid"))
            Dim lenderConfig = smBL.GetLiveConfig(lenderConfigID)
            If lenderConfig Is Nothing Then
                Response.StatusCode = 404 '' Forbidden
                Response.StatusDescription = "Not found"
                Response.End() '' Terminate all the rest processes
            End If
            Dim apmComboEnable As Boolean = SmUtil.GetApmComboEnable(lenderConfig.ConfigData)
            If apmComboEnable = False Then
                Response.StatusCode = 404 '' Forbidden
                Response.StatusDescription = "Not found"
                Response.End() '' Terminate all the rest processes
            End If
            Dim draftRecordList As New List(Of SmLenderConfigDraft)
            If Not String.IsNullOrEmpty(comboBothPreapprovedMsg) Then
                draftRecordList.Add(New SmLenderConfigDraft() With {
                .LenderConfigID = lenderConfigID,
                .CreatedByUserID = UserInfo.UserID,
                .NodePath = COMBO_BOTH_PREAPPROVED_MESSAGE("HOME_EQUITY_LOAN"),
                .NodeGroup = SmSettings.NodeGroup.ManageComboHE,
                .NodeContent = SmUtil.BuildSuccessMessageXml("SUCCESS_MESSAGES", "COMBO_BOTH_PREAPPROVED_MESSAGE", comboBothPreapprovedMsg)})
            End If
            If Not String.IsNullOrEmpty(comboSubmittedMsg) Then
                draftRecordList.Add(New SmLenderConfigDraft() With {
                    .LenderConfigID = lenderConfigID,
                    .CreatedByUserID = UserInfo.UserID,
                    .NodePath = COMBO_SUBMITTED_MESSAGE("HOME_EQUITY_LOAN"),
                    .NodeGroup = SmSettings.NodeGroup.ManageComboHE,
                    .NodeContent = SmUtil.BuildSuccessMessageXml("SUCCESS_MESSAGES", "COMBO_SUBMITTED_MESSAGE", comboSubmittedMsg)})
            End If
            If Not String.IsNullOrEmpty(comboXAPreapprovedMsg) Then
                draftRecordList.Add(New SmLenderConfigDraft() With {
                    .LenderConfigID = lenderConfigID,
                    .CreatedByUserID = UserInfo.UserID,
                    .NodePath = COMBO_XA_PREAPPROVED_MESSAGE("HOME_EQUITY_LOAN"),
                    .NodeGroup = SmSettings.NodeGroup.ManageComboHE,
                    .NodeContent = SmUtil.BuildSuccessMessageXml("SUCCESS_MESSAGES", "COMBO_XA_PREAPPROVED_MESSAGE", comboXAPreapprovedMsg)})
            End If
            If Not String.IsNullOrEmpty(comboXASubmittedMsg) Then
                draftRecordList.Add(New SmLenderConfigDraft() With {
                    .LenderConfigID = lenderConfigID,
                    .CreatedByUserID = UserInfo.UserID,
                    .NodePath = COMBO_XA_SUBMITTED_MESSAGE("HOME_EQUITY_LOAN"),
                    .NodeGroup = SmSettings.NodeGroup.ManageComboHE,
                    .NodeContent = SmUtil.BuildSuccessMessageXml("SUCCESS_MESSAGES", "COMBO_XA_SUBMITTED_MESSAGE", comboXASubmittedMsg)})
            End If
            If Not String.IsNullOrEmpty(comboPreQualifiedMsg) Then
                draftRecordList.Add(New SmLenderConfigDraft() With {
                    .LenderConfigID = lenderConfigID,
                    .CreatedByUserID = UserInfo.UserID,
                    .NodePath = COMBO_PREQUALIFIED_MESSAGE("HOME_EQUITY_LOAN"),
                    .NodeGroup = SmSettings.NodeGroup.ManageComboHE,
                    .NodeContent = SmUtil.BuildSuccessMessageXml("SUCCESS_MESSAGES", "COMBO_PREQUALIFIED_MESSAGE", comboPreQualifiedMsg)})
            End If
            Dim revisionID As Integer = Common.SafeInteger(Request.Form("revisionID"))
            Dim result = smBL.SaveNodesToDraft(revisionID, draftRecordList.ToArray())
            If result Then
                Dim previewUrl As String = smBL.GeneratePreviewConfig(lenderConfigID, UserInfo.Email, Function(config, code) String.Format("{0}/he/homeequityloan.aspx?lenderref={1}&type=1&autofill=true&previewCode={2}", _serverRoot, config.LenderRef, code), draftRecordList.ToArray())
                res = New CJsonResponse(True, "", "OK")
                res.Info = New With {.previewurl = previewUrl}
            Else
                res = New CJsonResponse(False, "", "FAILED")
            End If
        Catch ex As Exception
            _log.Error("Could not PreviewManageComboHEDraft.", ex)
            res = New CJsonResponse(False, "Unable to save and preview draft. Please try again", "FAILED")
        End Try
        Response.Write(JsonConvert.SerializeObject(res))
        Response.End()
    End Sub
    Private Sub PublishManageComboHEDraft()
        Dim res As New CJsonResponse()
        Try
            Dim smBL As New SmBL()
            Dim comboBothPreapprovedMsg As String = Common.SafeString(Request.Form("comboBothPreapprovedMsg"))
            Dim comboSubmittedMsg As String = Common.SafeString(Request.Form("comboSubmittedMsg"))
            Dim comboXAPreapprovedMsg As String = Common.SafeString(Request.Form("comboXAPreapprovedMsg"))
            Dim comboXASubmittedMsg As String = Common.SafeString(Request.Form("comboXASubmittedMsg"))
            Dim comboPreQualifiedMsg As String = Common.SafeString(Request.Form("comboPreQualifiedMsg"))
            Dim lenderConfigID As Guid = Common.SafeGUID(Request.Form("lenderconfigid"))
            Dim lenderConfig = smBL.GetLiveConfig(lenderConfigID)
            If lenderConfig Is Nothing Then
                Response.StatusCode = 404 '' Forbidden
                Response.StatusDescription = "Not found"
                Response.End() '' Terminate all the rest processes
            End If
            Dim apmComboEnable As Boolean = SmUtil.GetApmComboEnable(lenderConfig.ConfigData)
            If apmComboEnable = False Then
                Response.StatusCode = 404 '' Forbidden
                Response.StatusDescription = "Not found"
                Response.End() '' Terminate all the rest processes
            End If
            Dim draftRecordList As New List(Of SmLenderConfigDraft)
            If Not String.IsNullOrEmpty(comboBothPreapprovedMsg) Then
                draftRecordList.Add(New SmLenderConfigDraft() With {
                .LenderConfigID = lenderConfigID,
                .CreatedByUserID = UserInfo.UserID,
                .NodePath = COMBO_BOTH_PREAPPROVED_MESSAGE("HOME_EQUITY_LOAN"),
                .NodeGroup = SmSettings.NodeGroup.ManageComboHE,
                .NodeContent = SmUtil.BuildSuccessMessageXml("SUCCESS_MESSAGES", "COMBO_BOTH_PREAPPROVED_MESSAGE", comboBothPreapprovedMsg)})
            End If
            If Not String.IsNullOrEmpty(comboSubmittedMsg) Then
                draftRecordList.Add(New SmLenderConfigDraft() With {
                    .LenderConfigID = lenderConfigID,
                    .CreatedByUserID = UserInfo.UserID,
                    .NodePath = COMBO_SUBMITTED_MESSAGE("HOME_EQUITY_LOAN"),
                    .NodeGroup = SmSettings.NodeGroup.ManageComboHE,
                    .NodeContent = SmUtil.BuildSuccessMessageXml("SUCCESS_MESSAGES", "COMBO_SUBMITTED_MESSAGE", comboSubmittedMsg)})
            End If
            If Not String.IsNullOrEmpty(comboXAPreapprovedMsg) Then
                draftRecordList.Add(New SmLenderConfigDraft() With {
                    .LenderConfigID = lenderConfigID,
                    .CreatedByUserID = UserInfo.UserID,
                    .NodePath = COMBO_XA_PREAPPROVED_MESSAGE("HOME_EQUITY_LOAN"),
                    .NodeGroup = SmSettings.NodeGroup.ManageComboHE,
                    .NodeContent = SmUtil.BuildSuccessMessageXml("SUCCESS_MESSAGES", "COMBO_XA_PREAPPROVED_MESSAGE", comboXAPreapprovedMsg)})
            End If
            If Not String.IsNullOrEmpty(comboXASubmittedMsg) Then
                draftRecordList.Add(New SmLenderConfigDraft() With {
                    .LenderConfigID = lenderConfigID,
                    .CreatedByUserID = UserInfo.UserID,
                    .NodePath = COMBO_XA_SUBMITTED_MESSAGE("HOME_EQUITY_LOAN"),
                    .NodeGroup = SmSettings.NodeGroup.ManageComboHE,
                    .NodeContent = SmUtil.BuildSuccessMessageXml("SUCCESS_MESSAGES", "COMBO_XA_SUBMITTED_MESSAGE", comboXASubmittedMsg)})
            End If
            If Not String.IsNullOrEmpty(comboPreQualifiedMsg) Then
                draftRecordList.Add(New SmLenderConfigDraft() With {
                    .LenderConfigID = lenderConfigID,
                    .CreatedByUserID = UserInfo.UserID,
                    .NodePath = COMBO_PREQUALIFIED_MESSAGE("HOME_EQUITY_LOAN"),
                    .NodeGroup = SmSettings.NodeGroup.ManageComboHE,
                    .NodeContent = SmUtil.BuildSuccessMessageXml("SUCCESS_MESSAGES", "COMBO_PREQUALIFIED_MESSAGE", comboPreQualifiedMsg)})
            End If
            Dim comment As String = Common.SafeStripHtmlString(Request.Form("comment"))
            Dim publishAll As Boolean = Common.SafeBoolean(Request.Form("publishAll"))
            Dim result = smBL.PublishNodes(lenderConfigID, UserInfo.UserID, Request.UserHostAddress, comment, publishAll, draftRecordList.ToArray())
            If result Then
                res = New CJsonResponse(True, "", "OK")
            Else
                res = New CJsonResponse(False, "", "FAILED")
            End If
        Catch ex As Exception
            _log.Error("Could not PublishManageComboHEDraft.", ex)
            res = New CJsonResponse(False, "Unable to publish data. Please try again", "FAILED")
        End Try
        Response.Write(JsonConvert.SerializeObject(res))
        Response.End()
	End Sub

	Private Sub SaveManageLQBDraft()
		Dim res As New CJsonResponse()
		Try
			Dim smBL As New SmBL()
			Dim contactInfoMsg As String = Common.SafeString(Request.Form("contactInfoMsg"))
			Dim onlineAppPrivacyPolicyMsg As String = Common.SafeString(Request.Form("onlineAppPrivacyPolicyMsg"))
			Dim fullAppSubmitConfirmationMsg As String = Common.SafeString(Request.Form("fullAppSubmitConfirmationMsg"))
			Dim signedOutPageMsg As String = Common.SafeString(Request.Form("signedOutPageMsg"))
			Dim getRatesPageDisClaimerMsg As String = Common.SafeString(Request.Form("getRatesPageDisClaimerMsg"))
			Dim eligibleLoanProgramDisclaimerMsg As String = Common.SafeString(Request.Form("eligibleLoanProgramDisclaimerMsg"))
			Dim noEligibleLoanProgramDisclaimerMsg As String = Common.SafeString(Request.Form("noEligibleLoanProgramDisclaimerMsg"))
			Dim lenderConfigID As Guid = Common.SafeGUID(Request.Form("lenderconfigid"))
			Dim lenderConfig = smBL.GetLiveConfig(lenderConfigID)
			If lenderConfig Is Nothing Then
				Response.StatusCode = 404 '' Forbidden
				Response.StatusDescription = "Not found"
				Response.End() '' Terminate all the rest processes
			End If

			Dim draftRecordList As New List(Of SmLenderConfigDraft)
			If Not String.IsNullOrEmpty(contactInfoMsg) Then
				draftRecordList.Add(New SmLenderConfigDraft() With {
				.LenderConfigID = lenderConfigID,
				.CreatedByUserID = UserInfo.UserID,
				.NodePath = LQB_MESSAGES_CONTACT_INFO(),
				.NodeGroup = SmSettings.NodeGroup.ManageLQB,
				.NodeContent = SmUtil.BuildContentNodeXml("CONTACT_INFO", contactInfoMsg)})
			End If
			If Not String.IsNullOrEmpty(onlineAppPrivacyPolicyMsg) Then
				draftRecordList.Add(New SmLenderConfigDraft() With {
				.LenderConfigID = lenderConfigID,
				.CreatedByUserID = UserInfo.UserID,
				.NodePath = LQB_MESSAGES_ONLINE_APP_PRIVACY_POLICY(),
				.NodeGroup = SmSettings.NodeGroup.ManageLQB,
				.NodeContent = SmUtil.BuildContentNodeXml("ONLINE_APP_PRIVACY_POLICY", onlineAppPrivacyPolicyMsg)})
			End If
			If Not String.IsNullOrEmpty(fullAppSubmitConfirmationMsg) Then
				draftRecordList.Add(New SmLenderConfigDraft() With {
				.LenderConfigID = lenderConfigID,
				.CreatedByUserID = UserInfo.UserID,
				.NodePath = LQB_MESSAGES_FULL_APP_SUBMIT_CONFIRMATION(),
				.NodeGroup = SmSettings.NodeGroup.ManageLQB,
				.NodeContent = SmUtil.BuildContentNodeXml("FULL_APP_SUBMIT_CONFIRMATION", fullAppSubmitConfirmationMsg)})
			End If
			If Not String.IsNullOrEmpty(signedOutPageMsg) Then
				draftRecordList.Add(New SmLenderConfigDraft() With {
				.LenderConfigID = lenderConfigID,
				.CreatedByUserID = UserInfo.UserID,
				.NodePath = LQB_MESSAGES_SIGNED_OUT_PAGE_MESSAGE(),
				.NodeGroup = SmSettings.NodeGroup.ManageLQB,
				.NodeContent = SmUtil.BuildContentNodeXml("SIGNED_OUT_PAGE_MESSAGE", signedOutPageMsg)})
			End If
			If Not String.IsNullOrEmpty(getRatesPageDisClaimerMsg) Then
				draftRecordList.Add(New SmLenderConfigDraft() With {
				.LenderConfigID = lenderConfigID,
				.CreatedByUserID = UserInfo.UserID,
				.NodePath = LQB_MESSAGES_GET_RATES_PAGE_DISCLAIMER(),
				.NodeGroup = SmSettings.NodeGroup.ManageLQB,
				.NodeContent = SmUtil.BuildContentNodeXml("GET_RATES_PAGE_DISCLAIMER", getRatesPageDisClaimerMsg)})
			End If
			If Not String.IsNullOrEmpty(eligibleLoanProgramDisclaimerMsg) Then
				draftRecordList.Add(New SmLenderConfigDraft() With {
				.LenderConfigID = lenderConfigID,
				.CreatedByUserID = UserInfo.UserID,
				.NodePath = LQB_MESSAGES_ELIGIBLE_LOAN_PROGRAM_DISCLAIMER(),
				.NodeGroup = SmSettings.NodeGroup.ManageLQB,
				.NodeContent = SmUtil.BuildContentNodeXml("ELIGIBLE_LOAN_PROGRAM_DISCLAIMER", eligibleLoanProgramDisclaimerMsg)})
			End If
			If Not String.IsNullOrEmpty(noEligibleLoanProgramDisclaimerMsg) Then
				draftRecordList.Add(New SmLenderConfigDraft() With {
				.LenderConfigID = lenderConfigID,
				.CreatedByUserID = UserInfo.UserID,
				.NodePath = LQB_MESSAGES_NO_ELIGIBLE_LOAN_PROGRAM_DISCLAIMER(),
				.NodeGroup = SmSettings.NodeGroup.ManageLQB,
				.NodeContent = SmUtil.BuildContentNodeXml("NO_ELIGIBLE_LOAN_PROGRAM", noEligibleLoanProgramDisclaimerMsg)})
			End If

			Dim revisionID As Integer = Common.SafeInteger(Request.Form("revisionID"))
			Dim result = smBL.SaveNodesToDraft(revisionID, draftRecordList.ToArray())
			If result Then
				res = New CJsonResponse(True, "", "OK")
			Else
				res = New CJsonResponse(False, "", "FAILED")
			End If
		Catch ex As Exception
			_log.Error("Could not SaveManageLQBDraft.", ex)
			res = New CJsonResponse(False, "Unable to save draft. Please try again", "FAILED")
		End Try
		Response.Write(JsonConvert.SerializeObject(res))
		Response.End()
	End Sub
	Private Sub PreviewManageLQBDraft()
		Dim res As New CJsonResponse()
		Try
			Dim smBL As New SmBL()
			Dim contactInfoMsg As String = Common.SafeString(Request.Form("contactInfoMsg"))
			Dim onlineAppPrivacyPolicyMsg As String = Common.SafeString(Request.Form("onlineAppPrivacyPolicyMsg"))
			Dim fullAppSubmitConfirmationMsg As String = Common.SafeString(Request.Form("fullAppSubmitConfirmationMsg"))
			Dim signedOutPageMsg As String = Common.SafeString(Request.Form("signedOutPageMsg"))
			Dim getRatesPageDisClaimerMsg As String = Common.SafeString(Request.Form("getRatesPageDisClaimerMsg"))
			Dim eligibleLoanProgramDisclaimerMsg As String = Common.SafeString(Request.Form("eligibleLoanProgramDisclaimerMsg"))
			Dim noEligibleLoanProgramDisclaimerMsg As String = Common.SafeString(Request.Form("noEligibleLoanProgramDisclaimerMsg"))
			Dim lenderConfigID As Guid = Common.SafeGUID(Request.Form("lenderconfigid"))
			Dim lenderConfig = smBL.GetLiveConfig(lenderConfigID)
			If lenderConfig Is Nothing Then
				Response.StatusCode = 404 '' Forbidden
				Response.StatusDescription = "Not found"
				Response.End() '' Terminate all the rest processes
			End If

			Dim draftRecordList As New List(Of SmLenderConfigDraft)
			If Not String.IsNullOrEmpty(contactInfoMsg) Then
				draftRecordList.Add(New SmLenderConfigDraft() With {
				.LenderConfigID = lenderConfigID,
				.CreatedByUserID = UserInfo.UserID,
				.NodePath = LQB_MESSAGES_CONTACT_INFO(),
				.NodeGroup = SmSettings.NodeGroup.ManageLQB,
				.NodeContent = SmUtil.BuildContentNodeXml("CONTACT_INFO", contactInfoMsg)})
			End If
			If Not String.IsNullOrEmpty(onlineAppPrivacyPolicyMsg) Then
				draftRecordList.Add(New SmLenderConfigDraft() With {
				.LenderConfigID = lenderConfigID,
				.CreatedByUserID = UserInfo.UserID,
				.NodePath = LQB_MESSAGES_ONLINE_APP_PRIVACY_POLICY(),
				.NodeGroup = SmSettings.NodeGroup.ManageLQB,
				.NodeContent = SmUtil.BuildContentNodeXml("ONLINE_APP_PRIVACY_POLICY", onlineAppPrivacyPolicyMsg)})
			End If
			If Not String.IsNullOrEmpty(fullAppSubmitConfirmationMsg) Then
				draftRecordList.Add(New SmLenderConfigDraft() With {
				.LenderConfigID = lenderConfigID,
				.CreatedByUserID = UserInfo.UserID,
				.NodePath = LQB_MESSAGES_FULL_APP_SUBMIT_CONFIRMATION(),
				.NodeGroup = SmSettings.NodeGroup.ManageLQB,
				.NodeContent = SmUtil.BuildContentNodeXml("FULL_APP_SUBMIT_CONFIRMATION", fullAppSubmitConfirmationMsg)})
			End If
			If Not String.IsNullOrEmpty(signedOutPageMsg) Then
				draftRecordList.Add(New SmLenderConfigDraft() With {
				.LenderConfigID = lenderConfigID,
				.CreatedByUserID = UserInfo.UserID,
				.NodePath = LQB_MESSAGES_SIGNED_OUT_PAGE_MESSAGE(),
				.NodeGroup = SmSettings.NodeGroup.ManageLQB,
				.NodeContent = SmUtil.BuildContentNodeXml("SIGNED_OUT_PAGE_MESSAGE", signedOutPageMsg)})
			End If
			If Not String.IsNullOrEmpty(getRatesPageDisClaimerMsg) Then
				draftRecordList.Add(New SmLenderConfigDraft() With {
				.LenderConfigID = lenderConfigID,
				.CreatedByUserID = UserInfo.UserID,
				.NodePath = LQB_MESSAGES_GET_RATES_PAGE_DISCLAIMER(),
				.NodeGroup = SmSettings.NodeGroup.ManageLQB,
				.NodeContent = SmUtil.BuildContentNodeXml("GET_RATES_PAGE_DISCLAIMER", getRatesPageDisClaimerMsg)})
			End If
			If Not String.IsNullOrEmpty(eligibleLoanProgramDisclaimerMsg) Then
				draftRecordList.Add(New SmLenderConfigDraft() With {
				.LenderConfigID = lenderConfigID,
				.CreatedByUserID = UserInfo.UserID,
				.NodePath = LQB_MESSAGES_ELIGIBLE_LOAN_PROGRAM_DISCLAIMER(),
				.NodeGroup = SmSettings.NodeGroup.ManageLQB,
				.NodeContent = SmUtil.BuildContentNodeXml("ELIGIBLE_LOAN_PROGRAM_DISCLAIMER", eligibleLoanProgramDisclaimerMsg)})
			End If
			If Not String.IsNullOrEmpty(noEligibleLoanProgramDisclaimerMsg) Then
				draftRecordList.Add(New SmLenderConfigDraft() With {
				.LenderConfigID = lenderConfigID,
				.CreatedByUserID = UserInfo.UserID,
				.NodePath = LQB_MESSAGES_NO_ELIGIBLE_LOAN_PROGRAM_DISCLAIMER(),
				.NodeGroup = SmSettings.NodeGroup.ManageLQB,
				.NodeContent = SmUtil.BuildContentNodeXml("NO_ELIGIBLE_LOAN_PROGRAM", noEligibleLoanProgramDisclaimerMsg)})
			End If
			Dim revisionID As Integer = Common.SafeInteger(Request.Form("revisionID"))
			Dim result = smBL.SaveNodesToDraft(revisionID, draftRecordList.ToArray())
			If result Then
				Dim previewUrl As String = smBL.GeneratePreviewConfig(lenderConfigID, UserInfo.Email, Function(config, code) String.Format("{0}/Account/SignUp?lenderref={1}&mode=777&apm=manage&quicknav=true&apmstub=true&previewCode={2}", Common.LQB_WEBSITE, config.LenderRef, code), draftRecordList.ToArray())
				res = New CJsonResponse(True, "", "OK")
				res.Info = New With {.previewurl = previewUrl}
			Else
				res = New CJsonResponse(False, "", "FAILED")
			End If
		Catch ex As Exception
			_log.Error("Could not PreviewManageLQBDraft.", ex)
			res = New CJsonResponse(False, "Unable to save and preview draft. Please try again", "FAILED")
		End Try
		Response.Write(JsonConvert.SerializeObject(res))
		Response.End()
	End Sub
	Private Sub PublishManageLQBDraft()
		Dim res As New CJsonResponse()
		Try
			Dim smBL As New SmBL()
			Dim contactInfoMsg As String = Common.SafeString(Request.Form("contactInfoMsg"))
			Dim onlineAppPrivacyPolicyMsg As String = Common.SafeString(Request.Form("onlineAppPrivacyPolicyMsg"))
			Dim fullAppSubmitConfirmationMsg As String = Common.SafeString(Request.Form("fullAppSubmitConfirmationMsg"))
			Dim signedOutPageMsg As String = Common.SafeString(Request.Form("signedOutPageMsg"))
			Dim getRatesPageDisClaimerMsg As String = Common.SafeString(Request.Form("getRatesPageDisClaimerMsg"))
			Dim eligibleLoanProgramDisclaimerMsg As String = Common.SafeString(Request.Form("eligibleLoanProgramDisclaimerMsg"))
			Dim noEligibleLoanProgramDisclaimerMsg As String = Common.SafeString(Request.Form("noEligibleLoanProgramDisclaimerMsg"))
			Dim lenderConfigID As Guid = Common.SafeGUID(Request.Form("lenderconfigid"))
			Dim lenderConfig = smBL.GetLiveConfig(lenderConfigID)
			If lenderConfig Is Nothing Then
				Response.StatusCode = 404 '' Forbidden
				Response.StatusDescription = "Not found"
				Response.End() '' Terminate all the rest processes
			End If
			Dim draftRecordList As New List(Of SmLenderConfigDraft)
			If Not String.IsNullOrEmpty(contactInfoMsg) Then
				draftRecordList.Add(New SmLenderConfigDraft() With {
				.LenderConfigID = lenderConfigID,
				.CreatedByUserID = UserInfo.UserID,
				.NodePath = LQB_MESSAGES_CONTACT_INFO(),
				.NodeGroup = SmSettings.NodeGroup.ManageLQB,
				.NodeContent = SmUtil.BuildContentNodeXml("CONTACT_INFO", contactInfoMsg)})
			End If
			If Not String.IsNullOrEmpty(onlineAppPrivacyPolicyMsg) Then
				draftRecordList.Add(New SmLenderConfigDraft() With {
				.LenderConfigID = lenderConfigID,
				.CreatedByUserID = UserInfo.UserID,
				.NodePath = LQB_MESSAGES_ONLINE_APP_PRIVACY_POLICY(),
				.NodeGroup = SmSettings.NodeGroup.ManageLQB,
				.NodeContent = SmUtil.BuildContentNodeXml("ONLINE_APP_PRIVACY_POLICY", onlineAppPrivacyPolicyMsg)})
			End If
			If Not String.IsNullOrEmpty(fullAppSubmitConfirmationMsg) Then
				draftRecordList.Add(New SmLenderConfigDraft() With {
				.LenderConfigID = lenderConfigID,
				.CreatedByUserID = UserInfo.UserID,
				.NodePath = LQB_MESSAGES_FULL_APP_SUBMIT_CONFIRMATION(),
				.NodeGroup = SmSettings.NodeGroup.ManageLQB,
				.NodeContent = SmUtil.BuildContentNodeXml("FULL_APP_SUBMIT_CONFIRMATION", fullAppSubmitConfirmationMsg)})
			End If
			If Not String.IsNullOrEmpty(signedOutPageMsg) Then
				draftRecordList.Add(New SmLenderConfigDraft() With {
				.LenderConfigID = lenderConfigID,
				.CreatedByUserID = UserInfo.UserID,
				.NodePath = LQB_MESSAGES_SIGNED_OUT_PAGE_MESSAGE(),
				.NodeGroup = SmSettings.NodeGroup.ManageLQB,
				.NodeContent = SmUtil.BuildContentNodeXml("SIGNED_OUT_PAGE_MESSAGE", signedOutPageMsg)})
			End If
			If Not String.IsNullOrEmpty(getRatesPageDisClaimerMsg) Then
				draftRecordList.Add(New SmLenderConfigDraft() With {
				.LenderConfigID = lenderConfigID,
				.CreatedByUserID = UserInfo.UserID,
				.NodePath = LQB_MESSAGES_GET_RATES_PAGE_DISCLAIMER(),
				.NodeGroup = SmSettings.NodeGroup.ManageLQB,
				.NodeContent = SmUtil.BuildContentNodeXml("GET_RATES_PAGE_DISCLAIMER", getRatesPageDisClaimerMsg)})
			End If
			If Not String.IsNullOrEmpty(eligibleLoanProgramDisclaimerMsg) Then
				draftRecordList.Add(New SmLenderConfigDraft() With {
				.LenderConfigID = lenderConfigID,
				.CreatedByUserID = UserInfo.UserID,
				.NodePath = LQB_MESSAGES_ELIGIBLE_LOAN_PROGRAM_DISCLAIMER(),
				.NodeGroup = SmSettings.NodeGroup.ManageLQB,
				.NodeContent = SmUtil.BuildContentNodeXml("ELIGIBLE_LOAN_PROGRAM_DISCLAIMER", eligibleLoanProgramDisclaimerMsg)})
			End If
			If Not String.IsNullOrEmpty(noEligibleLoanProgramDisclaimerMsg) Then
				draftRecordList.Add(New SmLenderConfigDraft() With {
				.LenderConfigID = lenderConfigID,
				.CreatedByUserID = UserInfo.UserID,
				.NodePath = LQB_MESSAGES_NO_ELIGIBLE_LOAN_PROGRAM_DISCLAIMER(),
				.NodeGroup = SmSettings.NodeGroup.ManageLQB,
				.NodeContent = SmUtil.BuildContentNodeXml("NO_ELIGIBLE_LOAN_PROGRAM", noEligibleLoanProgramDisclaimerMsg)})
			End If
			Dim comment As String = Common.SafeStripHtmlString(Request.Form("comment"))
			Dim publishAll As Boolean = Common.SafeBoolean(Request.Form("publishAll"))
			Dim result = smBL.PublishNodes(lenderConfigID, UserInfo.UserID, Request.UserHostAddress, comment, publishAll, draftRecordList.ToArray())
			If result Then
				res = New CJsonResponse(True, "", "OK")
			Else
				res = New CJsonResponse(False, "", "FAILED")
			End If
		Catch ex As Exception
			_log.Error("Could not PublishManageLQBDraft.", ex)
			res = New CJsonResponse(False, "Unable to publish data. Please try again", "FAILED")
		End Try
		Response.Write(JsonConvert.SerializeObject(res))
		Response.End()
	End Sub
#End Region

#Region "General Settings"
    Private Sub SaveGeneralSetttingsDraft()
        Dim res As CJsonResponse
        Try
            Dim smBL As New SmBL()
            Dim draftRecordList As New List(Of SmLenderConfigDraft)
            Dim lenderConfigID As Guid = Common.SafeGUID(Request.Form("lenderconfigid"))

            Dim lenderConfig As CLenderConfig = smBL.GetLiveConfig(lenderConfigID)
            If lenderConfig Is Nothing Then
                Response.StatusCode = 404 '' Forbidden
                Response.StatusDescription = "Not found"
                Response.End() '' Terminate all the rest processes
            End If
            Dim lenderConfigXml As New XmlDocument()
            lenderConfigXml.LoadXml(lenderConfig.ConfigData)

            ''----------------------------
            ''General
            draftRecordList.Add(New SmLenderConfigDraft() With {
                .LenderConfigID = lenderConfigID,
                .CreatedByUserID = UserInfo.UserID,
                .NodePath = FOREIGN_ADDRESS(),
                .NodeGroup = SmSettings.NodeGroup.GeneralSettings,
            .NodeContent = SmUtil.BuildNodeAttributeXml("VISIBLE", "foreign_address", IIf(Common.SafeBoolean(Request.Form("foreign_address")), "Y", "N").ToString())
            })
            draftRecordList.Add(New SmLenderConfigDraft() With {
                .LenderConfigID = lenderConfigID,
                .CreatedByUserID = UserInfo.UserID,
                .NodePath = FOREIGN_PHONE(),
                .NodeGroup = SmSettings.NodeGroup.GeneralSettings,
            .NodeContent = SmUtil.BuildNodeAttributeXml("VISIBLE", "foreign_phone", IIf(Common.SafeBoolean(Request.Form("foreign_phone")), "Y", "N").ToString())
            })
            Dim apmLinkedInEnable As Boolean = SmUtil.GetApmLinkedInEnable(lenderConfigXml)
            If apmLinkedInEnable Then
                If Request.Form("linkedin_enable") Is Nothing Then
                    'force reload page incase the content on page is out of date. Means it not reflect the current settings
                    Response.StatusCode = 405 '' Reset page
                    Response.StatusDescription = "Reset page"
                    Response.End() '' Terminate all the rest processes
                End If
                Dim linkedInEnable As Boolean = Common.SafeBoolean(Request.Form("linkedin_enable"))
                draftRecordList.Add(New SmLenderConfigDraft() With {
                    .LenderConfigID = lenderConfigID,
                    .CreatedByUserID = UserInfo.UserID,
                    .NodePath = LINKEDIN_ENABLE(),
                    .NodeGroup = SmSettings.NodeGroup.GeneralSettings,
                .NodeContent = SmUtil.BuildNodeAttributeXml("VISIBLE", "linkedin_enable", IIf(linkedInEnable, "Y", "N").ToString())
                })
            ElseIf Request.Form("linkedin_enable") IsNot Nothing Then
                'force reload page incase the content on page is out of date. Means it not reflect the current settings
                Response.StatusCode = 405 '' Reset page
                Response.StatusDescription = "Reset page"
                Response.End() '' Terminate all the rest processes
			End If
			If SmUtil.GetApmShowInstaTouchSwitchButton(lenderConfigXml) Then
				draftRecordList.Add(New SmLenderConfigDraft() With {
					.LenderConfigID = lenderConfigID,
					.CreatedByUserID = UserInfo.UserID,
					.NodePath = INSTATOUCH_ENABLED(),
					.NodeGroup = SmSettings.NodeGroup.GeneralSettings,
				.NodeContent = SmUtil.BuildNodeAttributeXml("WEBSITE", "instatouch_enabled", IIf(Common.SafeBoolean(Request.Form("instatouch_enabled")), "Y", "N").ToString())
				})
			End If
			draftRecordList.Add(New SmLenderConfigDraft() With {
				.LenderConfigID = lenderConfigID,
				.CreatedByUserID = UserInfo.UserID,
				.NodePath = MFA_EMAIL_ONLY(),
				.NodeGroup = SmSettings.NodeGroup.GeneralSettings,
			.NodeContent = SmUtil.BuildNodeAttributeXml("WEBSITE", "mfa_email_only", IIf(Common.SafeBoolean(Request.Form("mfa_email_only")), "Y", "N").ToString())
			})

			Dim apmSiteAnalyticsEnable As Boolean = SmUtil.GetApmSiteAnalyticsEnable(lenderConfigXml)
			If apmSiteAnalyticsEnable Then
				'If Request.Form("google_tag_manager_id") Is Nothing Then
				'	'force reload page incase the content on page is out of date. Means it not reflect the current settings
				'	Response.StatusCode = 405 '' Reset page
				'	Response.StatusDescription = "Reset page"
				'	Response.End() '' Terminate all the rest processes
				'End If
				If Request.Form("google_tag_manager_id") IsNot Nothing Then
					Dim googleTagManagerID As String = Common.SafeString(Request.Form("google_tag_manager_id"))
					draftRecordList.Add(New SmLenderConfigDraft() With {
						.LenderConfigID = lenderConfigID,
						.CreatedByUserID = UserInfo.UserID,
						.NodePath = GOOGLE_TAG_MANAGER_ID(),
						.NodeGroup = SmSettings.NodeGroup.GeneralSettings,
					.NodeContent = SmUtil.BuildNodeAttributeXml("WEBSITE", "google_tag_manager_id", googleTagManagerID)
					})
				End If
				If Request.Form("piwik_site_id") IsNot Nothing Then
					Dim piwikSiteID As String = Common.SafeString(Request.Form("piwik_site_id"))
					draftRecordList.Add(New SmLenderConfigDraft() With {
						.LenderConfigID = lenderConfigID,
						.CreatedByUserID = UserInfo.UserID,
						.NodePath = PIWIK_SITE_ID(),
						.NodeGroup = SmSettings.NodeGroup.GeneralSettings,
					.NodeContent = SmUtil.BuildNodeAttributeXml("WEBSITE", "piwik_site_id", piwikSiteID)
					})
				End If

			ElseIf Request.Form("google_tag_manager_id") IsNot Nothing Then
				'force reload page incase the content on page is out of date. Means it not reflect the current settings
				Response.StatusCode = 405 '' Reset page
				Response.StatusDescription = "Reset page"
				Response.End() '' Terminate all the rest processes
			End If


			If SmUtil.CheckRoleAccess(Context, SmSettings.Role.LegacyOperator, SmSettings.Role.SuperOperator) Then
				Dim homepageEditor As Boolean = Common.SafeBoolean(Request.Form("homepage_editor"))
				draftRecordList.Add(New SmLenderConfigDraft() With {
						.LenderConfigID = lenderConfigID,
						.CreatedByUserID = UserInfo.UserID,
						.NodePath = HOMEPAGE_EDITOR(),
						.NodeGroup = SmSettings.NodeGroup.GeneralSettings,
					.NodeContent = SmUtil.BuildNodeAttributeXml("WEBSITE", "homepage_editor", IIf(homepageEditor, "Y", "N").ToString())
					})
				Dim dlBarcodeScan As Boolean = Common.SafeBoolean(Request.Form("dl_barcode_scan"))
				draftRecordList.Add(New SmLenderConfigDraft() With {
					.LenderConfigID = lenderConfigID,
					.CreatedByUserID = UserInfo.UserID,
					.NodePath = DL_BARCODE_SCAN(),
					.NodeGroup = SmSettings.NodeGroup.GeneralSettings,
				.NodeContent = SmUtil.BuildNodeAttributeXml("WEBSITE", "dl_barcode_scan", IIf(dlBarcodeScan, "Y", "N").ToString())
				})
			End If
			''------

			Dim revisionID As Integer = Common.SafeInteger(Request.Form("revisionID"))
			Dim result = smBL.SaveNodesToDraft(revisionID, draftRecordList.ToArray())
			If result Then
				res = New CJsonResponse(True, "", "OK")
			Else
				res = New CJsonResponse(False, "", "FAILED")
			End If
		Catch ex As Exception
			_log.Error("Could not SaveGeneralSettingsDraft.", ex)
			res = New CJsonResponse(False, "Unable to save draft. Please try again", "FAILED")
		End Try
		Response.Write(JsonConvert.SerializeObject(res))
		Response.End()
	End Sub
	Private Sub PreviewGeneralSettingsDraft()
		Dim res As CJsonResponse
		Try
			Dim smBL As New SmBL()
			Dim draftRecordList As New List(Of SmLenderConfigDraft)
			Dim lenderConfigID As Guid = Common.SafeGUID(Request.Form("lenderconfigid"))
			Dim lenderConfig As CLenderConfig = smBL.GetLiveConfig(lenderConfigID)
			If lenderConfig Is Nothing Then
				Response.StatusCode = 404 '' Forbidden
				Response.StatusDescription = "Not found"
				Response.End() '' Terminate all the rest processes
			End If
			Dim lenderConfigXml As New XmlDocument()
			lenderConfigXml.LoadXml(lenderConfig.ConfigData)

			''General
			draftRecordList.Add(New SmLenderConfigDraft() With {
				.LenderConfigID = lenderConfigID,
				.CreatedByUserID = UserInfo.UserID,
				.NodePath = FOREIGN_ADDRESS(),
				.NodeGroup = SmSettings.NodeGroup.GeneralSettings,
			.NodeContent = SmUtil.BuildNodeAttributeXml("VISIBLE", "foreign_address", IIf(Common.SafeBoolean(Request.Form("foreign_address")), "Y", "N").ToString())
			})
			draftRecordList.Add(New SmLenderConfigDraft() With {
				.LenderConfigID = lenderConfigID,
				.CreatedByUserID = UserInfo.UserID,
				.NodePath = FOREIGN_PHONE(),
				.NodeGroup = SmSettings.NodeGroup.GeneralSettings,
			.NodeContent = SmUtil.BuildNodeAttributeXml("VISIBLE", "foreign_phone", IIf(Common.SafeBoolean(Request.Form("foreign_phone")), "Y", "N").ToString())
			})
			Dim apmLinkedInEnable As Boolean = SmUtil.GetApmLinkedInEnable(lenderConfigXml)
			If apmLinkedInEnable Then
				If Request.Form("linkedin_enable") Is Nothing Then
					'force reload page incase the content on page is out of date. Means it not reflect the current settings
					Response.StatusCode = 405 '' Reset page
					Response.StatusDescription = "Reset page"
					Response.End() '' Terminate all the rest processes
				End If
				Dim linkedInEnable As Boolean = Common.SafeBoolean(Request.Form("linkedin_enable"))
				draftRecordList.Add(New SmLenderConfigDraft() With {
					.LenderConfigID = lenderConfigID,
					.CreatedByUserID = UserInfo.UserID,
					.NodePath = LINKEDIN_ENABLE(),
					.NodeGroup = SmSettings.NodeGroup.GeneralSettings,
				.NodeContent = SmUtil.BuildNodeAttributeXml("VISIBLE", "linkedin_enable", IIf(linkedInEnable, "Y", "N").ToString())
				})
			ElseIf Request.Form("linkedin_enable") IsNot Nothing Then
				'force reload page incase the content on page is out of date. Means it not reflect the current settings
				Response.StatusCode = 405 '' Reset page
				Response.StatusDescription = "Reset page"
				Response.End() '' Terminate all the rest processes
			End If
			If SmUtil.GetApmShowInstaTouchSwitchButton(lenderConfigXml) Then
				draftRecordList.Add(New SmLenderConfigDraft() With {
					.LenderConfigID = lenderConfigID,
					.CreatedByUserID = UserInfo.UserID,
					.NodePath = INSTATOUCH_ENABLED(),
					.NodeGroup = SmSettings.NodeGroup.GeneralSettings,
				.NodeContent = SmUtil.BuildNodeAttributeXml("WEBSITE", "instatouch_enabled", IIf(Common.SafeBoolean(Request.Form("instatouch_enabled")), "Y", "N").ToString())
				})
			End If
			draftRecordList.Add(New SmLenderConfigDraft() With {
				.LenderConfigID = lenderConfigID,
				.CreatedByUserID = UserInfo.UserID,
				.NodePath = MFA_EMAIL_ONLY(),
				.NodeGroup = SmSettings.NodeGroup.GeneralSettings,
			.NodeContent = SmUtil.BuildNodeAttributeXml("WEBSITE", "mfa_email_only", IIf(Common.SafeBoolean(Request.Form("mfa_email_only")), "Y", "N").ToString())
			})

			Dim apmSiteAnalyticsEnable As Boolean = SmUtil.GetApmSiteAnalyticsEnable(lenderConfigXml)
			If apmSiteAnalyticsEnable Then
				'If Request.Form("google_tag_manager_id") Is Nothing Then
				'	'force reload page incase the content on page is out of date. Means it not reflect the current settings
				'	Response.StatusCode = 405 '' Reset page
				'	Response.StatusDescription = "Reset page"
				'	Response.End() '' Terminate all the rest processes
				'End If
				If Request.Form("google_tag_manager_id") IsNot Nothing Then
					Dim googleTagManagerID As String = Common.SafeString(Request.Form("google_tag_manager_id"))
					draftRecordList.Add(New SmLenderConfigDraft() With {
						.LenderConfigID = lenderConfigID,
						.CreatedByUserID = UserInfo.UserID,
						.NodePath = GOOGLE_TAG_MANAGER_ID(),
						.NodeGroup = SmSettings.NodeGroup.GeneralSettings,
					.NodeContent = SmUtil.BuildNodeAttributeXml("WEBSITE", "google_tag_manager_id", googleTagManagerID)
					})
				End If
				If Request.Form("piwik_site_id") IsNot Nothing Then
					Dim piwikSiteID As String = Common.SafeString(Request.Form("piwik_site_id"))
					draftRecordList.Add(New SmLenderConfigDraft() With {
						.LenderConfigID = lenderConfigID,
						.CreatedByUserID = UserInfo.UserID,
						.NodePath = PIWIK_SITE_ID(),
						.NodeGroup = SmSettings.NodeGroup.GeneralSettings,
					.NodeContent = SmUtil.BuildNodeAttributeXml("WEBSITE", "piwik_site_id", piwikSiteID)
					})
				End If
			ElseIf Request.Form("google_tag_manager_id") IsNot Nothing Then
				'force reload page incase the content on page is out of date. Means it not reflect the current settings
				Response.StatusCode = 405 '' Reset page
				Response.StatusDescription = "Reset page"
				Response.End() '' Terminate all the rest processes
			End If

			If SmUtil.CheckRoleAccess(Context, SmSettings.Role.LegacyOperator, SmSettings.Role.SuperOperator) Then
				Dim homepageEditor As Boolean = Common.SafeBoolean(Request.Form("homepage_editor"))
				draftRecordList.Add(New SmLenderConfigDraft() With {
						.LenderConfigID = lenderConfigID,
						.CreatedByUserID = UserInfo.UserID,
						.NodePath = HOMEPAGE_EDITOR(),
						.NodeGroup = SmSettings.NodeGroup.GeneralSettings,
					.NodeContent = SmUtil.BuildNodeAttributeXml("WEBSITE", "homepage_editor", IIf(homepageEditor, "Y", "N").ToString())
					})
				Dim dlBarcodeScan As Boolean = Common.SafeBoolean(Request.Form("dl_barcode_scan"))
				draftRecordList.Add(New SmLenderConfigDraft() With {
					.LenderConfigID = lenderConfigID,
					.CreatedByUserID = UserInfo.UserID,
					.NodePath = DL_BARCODE_SCAN(),
					.NodeGroup = SmSettings.NodeGroup.GeneralSettings,
				.NodeContent = SmUtil.BuildNodeAttributeXml("WEBSITE", "dl_barcode_scan", IIf(dlBarcodeScan, "Y", "N").ToString())
				})
			End If

			Dim revisionID As Integer = Common.SafeInteger(Request.Form("revisionID"))
			Dim result = smBL.SaveNodesToDraft(revisionID, draftRecordList.ToArray())
			If result Then
				Dim previewUrl As String = smBL.GeneratePreviewConfig(lenderConfigID, UserInfo.Email, Function(config, code) String.Format("{0}/pl/personalloan.aspx?lenderref={1}&autofill=true&previewCode={2}", _serverRoot, config.LenderRef, code), draftRecordList.ToArray())
				res = New CJsonResponse(True, "", "OK")
				res.Info = New With {.previewurl = previewUrl}
			Else
				res = New CJsonResponse(False, "", "FAILED")
			End If
		Catch ex As Exception
			_log.Error("Could not PreviewGeneralSettingsDraft.", ex)
			res = New CJsonResponse(False, "Unable to save and preview draft. Please try again", "FAILED")
		End Try
		Response.Write(JsonConvert.SerializeObject(res))
		Response.End()
	End Sub
	Private Sub PublishGeneralSettings()
		Dim res As CJsonResponse
		Try
			Dim smBL As New SmBL()
			Dim draftRecordList As New List(Of SmLenderConfigDraft)
			Dim lenderConfigID As Guid = Common.SafeGUID(Request.Form("lenderconfigid"))
			Dim lenderConfig As CLenderConfig = smBL.GetLiveConfig(lenderConfigID)
			If lenderConfig Is Nothing Then
				Response.StatusCode = 404 '' Forbidden
				Response.StatusDescription = "Not found"
				Response.End() '' Terminate all the rest processes
			End If
			Dim lenderConfigXml As New XmlDocument()
			lenderConfigXml.LoadXml(lenderConfig.ConfigData)
			''---------------
			''These are for the general -----
			draftRecordList.Add(New SmLenderConfigDraft() With {
				.LenderConfigID = lenderConfigID,
				.CreatedByUserID = UserInfo.UserID,
				.NodePath = FOREIGN_ADDRESS(),
				.NodeGroup = SmSettings.NodeGroup.GeneralSettings,
			.NodeContent = SmUtil.BuildNodeAttributeXml("VISIBLE", "foreign_address", IIf(Common.SafeBoolean(Request.Form("foreign_address")), "Y", "N").ToString())
			})
			draftRecordList.Add(New SmLenderConfigDraft() With {
				.LenderConfigID = lenderConfigID,
				.CreatedByUserID = UserInfo.UserID,
				.NodePath = FOREIGN_PHONE(),
				.NodeGroup = SmSettings.NodeGroup.GeneralSettings,
			.NodeContent = SmUtil.BuildNodeAttributeXml("VISIBLE", "foreign_phone", IIf(Common.SafeBoolean(Request.Form("foreign_phone")), "Y", "N").ToString())
			})

			draftRecordList.Add(New SmLenderConfigDraft() With {
				.LenderConfigID = lenderConfigID,
				.CreatedByUserID = UserInfo.UserID,
				.NodePath = MFA_EMAIL_ONLY(),
				.NodeGroup = SmSettings.NodeGroup.GeneralSettings,
			.NodeContent = SmUtil.BuildNodeAttributeXml("WEBSITE", "mfa_email_only", IIf(Common.SafeBoolean(Request.Form("mfa_email_only")), "Y", "N").ToString())
			})
			If SmUtil.GetApmShowInstaTouchSwitchButton(lenderConfigXml) Then
				draftRecordList.Add(New SmLenderConfigDraft() With {
					.LenderConfigID = lenderConfigID,
					.CreatedByUserID = UserInfo.UserID,
					.NodePath = INSTATOUCH_ENABLED(),
					.NodeGroup = SmSettings.NodeGroup.GeneralSettings,
				.NodeContent = SmUtil.BuildNodeAttributeXml("WEBSITE", "instatouch_enabled", IIf(Common.SafeBoolean(Request.Form("instatouch_enabled")), "Y", "N").ToString())
				})
			End If
			'https://denovu.atlassian.net/browse/APP-12
			'"Y" for all new deployment, null for legacy. On publish and value is null, if mpao_key2 is not null, set to Y. Else, set to N.   On pageload and value is null and mpao_key2 is not null, show this section. On pageload and value is Y, show this section
			Dim apmLicenseScanEnableNode = lenderConfigXml.SelectSingleNode(APM_DL_SCAN_ENABLE_N())
			If apmLicenseScanEnableNode Is Nothing Then
				Dim licenseScanEnableNode = lenderConfigXml.SelectSingleNode(LICENSE_SCAN())
				draftRecordList.Add(New SmLenderConfigDraft() With {
					.LenderConfigID = lenderConfigID,
					.CreatedByUserID = UserInfo.UserID,
					.NodePath = APM_DL_SCAN_ENABLE_N(),
					.NodeGroup = SmSettings.NodeGroup.GeneralSettings,
				.NodeContent = SmUtil.BuildNodeAttributeXml("VISIBLE", "APM_dl_scan_enable_N", IIf(licenseScanEnableNode IsNot Nothing, "Y", "N").ToString())
				})
			End If

			Dim apmLinkedInEnableNode = lenderConfigXml.SelectSingleNode(APM_LINKEDIN_ENABLE_N())
			If apmLinkedInEnableNode Is Nothing Then
				Dim linkedInEnableNode = lenderConfigXml.SelectSingleNode(LINKEDIN_ENABLE())
				draftRecordList.Add(New SmLenderConfigDraft() With {
					.LenderConfigID = lenderConfigID,
					.CreatedByUserID = UserInfo.UserID,
					.NodePath = APM_LINKEDIN_ENABLE_N(),
					.NodeGroup = SmSettings.NodeGroup.GeneralSettings,
				.NodeContent = SmUtil.BuildNodeAttributeXml("VISIBLE", "APM_linkedin_enable_N", IIf(linkedInEnableNode IsNot Nothing, "Y", "N").ToString())
				})
			End If


			'https://denovu.atlassian.net/browse/APP-12
			'"Y" for Plus, "N" for essential, null for legacy. On publish and value is null, if linkedin_enable is not null, set to Y. Else, set to N.   On pageload and value is null and linkedin_enable is not null, show this section. On pageload value is Y, show this section This should also respect existing driver license dependency logic.
			'<xs:attribute type="boolean_Y_N" name="APM_linkedin_enable_N" default="N" use="optional"/>
			Dim apmLinkedInEnable As Boolean = SmUtil.GetApmLinkedInEnable(lenderConfigXml)
			If apmLinkedInEnable Then
				If Request.Form("linkedin_enable") Is Nothing Then
					'force reload page incase the content on page is out of date. Means it not reflect the current settings
					Response.StatusCode = 405 '' Reset page
					Response.StatusDescription = "Reset page"
					Response.End() '' Terminate all the rest processes
				End If
				Dim linkedInEnable As Boolean = Common.SafeBoolean(Request.Form("linkedin_enable"))
				draftRecordList.Add(New SmLenderConfigDraft() With {
					.LenderConfigID = lenderConfigID,
					.CreatedByUserID = UserInfo.UserID,
					.NodePath = LINKEDIN_ENABLE(),
					.NodeGroup = SmSettings.NodeGroup.GeneralSettings,
				.NodeContent = SmUtil.BuildNodeAttributeXml("VISIBLE", "linkedin_enable", IIf(linkedInEnable, "Y", "N").ToString())
				})
			ElseIf Request.Form("linkedin_enable") IsNot Nothing Then
				'force reload page incase the content on page is out of date. Means it not reflect the current settings
				Response.StatusCode = 405 '' Reset page
				Response.StatusDescription = "Reset page"
				Response.End() '' Terminate all the rest processes
			End If

			'https://denovu.atlassian.net/browse/APP-12
			'"N" for essential, Y for plus,  Null for legacy. On publish and value is null, if piwik_site_id or google_tag_manager_id is not null, set to Y. Else, set to N.  On pageload and value is null and (piwik_site_id or google_tag_manager_id is not null), show this section. On pageload and value is Y, show this section. This "on-publish" logic is probably worth a code comment referencing back to this case so we know we can remove it in the future.
			'make the piwik_site_id and google_tag_manager_id visible in APM.
			'<xs:attribute type="boolean_Y_N" name="APM_site_analytics_enable_Y" default="Y" use="optional"/>
			Dim apmSiteAnalyticsEnableNode = lenderConfigXml.SelectSingleNode(APM_SITE_ANALYTICS_ENABLE_N())
			If apmSiteAnalyticsEnableNode Is Nothing Then
				Dim googleTagManagerIdNode = lenderConfigXml.SelectSingleNode(GOOGLE_TAG_MANAGER_ID())
				Dim piwikSiteIdNode = lenderConfigXml.SelectSingleNode(PIWIK_SITE_ID())
				draftRecordList.Add(New SmLenderConfigDraft() With {
					.LenderConfigID = lenderConfigID,
					.CreatedByUserID = UserInfo.UserID,
					.NodePath = APM_SITE_ANALYTICS_ENABLE_N(),
					.NodeGroup = SmSettings.NodeGroup.GeneralSettings,
				.NodeContent = SmUtil.BuildNodeAttributeXml("VISIBLE", "APM_site_analytics_enable_N", IIf(googleTagManagerIdNode IsNot Nothing OrElse piwikSiteIdNode IsNot Nothing, "Y", "N").ToString())
				})
			End If
			Dim apmSiteAnalyticsEnable As Boolean = SmUtil.GetApmSiteAnalyticsEnable(lenderConfigXml)
			If apmSiteAnalyticsEnable Then
				'If Request.Form("google_tag_manager_id") Is Nothing Then
				'	'force reload page incase the content on page is out of date. Means it not reflect the current settings
				'	Response.StatusCode = 405 '' Reset page
				'	Response.StatusDescription = "Reset page"
				'	Response.End() '' Terminate all the rest processes
				'End If
				If Request.Form("google_tag_manager_id") IsNot Nothing Then
					Dim googleTagManagerID As String = Common.SafeString(Request.Form("google_tag_manager_id"))
					draftRecordList.Add(New SmLenderConfigDraft() With {
						.LenderConfigID = lenderConfigID,
						.CreatedByUserID = UserInfo.UserID,
						.NodePath = GOOGLE_TAG_MANAGER_ID(),
						.NodeGroup = SmSettings.NodeGroup.GeneralSettings,
					.NodeContent = SmUtil.BuildNodeAttributeXml("WEBSITE", "google_tag_manager_id", googleTagManagerID)
					})
				End If
				If Request.Form("piwik_site_id") IsNot Nothing Then
					Dim piwikSiteID As String = Common.SafeString(Request.Form("piwik_site_id"))
					draftRecordList.Add(New SmLenderConfigDraft() With {
						.LenderConfigID = lenderConfigID,
						.CreatedByUserID = UserInfo.UserID,
						.NodePath = PIWIK_SITE_ID(),
						.NodeGroup = SmSettings.NodeGroup.GeneralSettings,
					.NodeContent = SmUtil.BuildNodeAttributeXml("WEBSITE", "piwik_site_id", piwikSiteID)
					})
				End If
			ElseIf Request.Form("google_tag_manager_id") IsNot Nothing Then
				'force reload page incase the content on page is out of date. Means it not reflect the current settings
				Response.StatusCode = 405 '' Reset page
				Response.StatusDescription = "Reset page"
				Response.End() '' Terminate all the rest processes
			End If

			If SmUtil.CheckRoleAccess(Context, SmSettings.Role.LegacyOperator, SmSettings.Role.SuperOperator) Then
				Dim homepageEditor As Boolean = Common.SafeBoolean(Request.Form("homepage_editor"))
				draftRecordList.Add(New SmLenderConfigDraft() With {
						.LenderConfigID = lenderConfigID,
						.CreatedByUserID = UserInfo.UserID,
						.NodePath = HOMEPAGE_EDITOR(),
						.NodeGroup = SmSettings.NodeGroup.GeneralSettings,
					.NodeContent = SmUtil.BuildNodeAttributeXml("WEBSITE", "homepage_editor", IIf(homepageEditor, "Y", "N").ToString())
					})
				Dim dlBarcodeScan As Boolean = Common.SafeBoolean(Request.Form("dl_barcode_scan"))
				draftRecordList.Add(New SmLenderConfigDraft() With {
					.LenderConfigID = lenderConfigID,
					.CreatedByUserID = UserInfo.UserID,
					.NodePath = DL_BARCODE_SCAN(),
					.NodeGroup = SmSettings.NodeGroup.GeneralSettings,
				.NodeContent = SmUtil.BuildNodeAttributeXml("WEBSITE", "dl_barcode_scan", IIf(dlBarcodeScan, "Y", "N").ToString())
				})
			End If
			''-----
			Dim comment As String = Common.SafeStripHtmlString(Request.Form("comment"))
			Dim publishAll As Boolean = Common.SafeBoolean(Request.Form("publishAll"))
			Dim result = smBL.PublishNodes(lenderConfigID, UserInfo.UserID, Request.UserHostAddress, comment, publishAll, draftRecordList.ToArray())
			If result Then
				res = New CJsonResponse(True, "", "OK")
			Else
				res = New CJsonResponse(False, "", "FAILED")
			End If
		Catch ex As Exception
			_log.Error("Could not PublishGeneralSettings.", ex)
			res = New CJsonResponse(False, "Unable to publish data. Please try again", "FAILED")
		End Try
		Response.Write(JsonConvert.SerializeObject(res))
		Response.End()
	End Sub
#End Region
#Region "XA Settings"
	Private Function GenerateRecordList(ByVal lenderConfig As CLenderConfig) As List(Of SmLenderConfigDraft)
		Dim draftRecordList As New List(Of SmLenderConfigDraft)
		Dim lenderConfigID As Guid = Common.SafeGUID(Request.Form("lenderconfigid"))

		Dim lenderConfigXml As New XmlDocument()
		lenderConfigXml.LoadXml(lenderConfig.ConfigData)
		Dim enableXA As Boolean = lenderConfigXml.SelectSingleNode("WEBSITE/XA_LOAN") IsNot Nothing
		''For XA loans
		If enableXA Then

			draftRecordList.Add(New SmLenderConfigDraft() With {
					.LenderConfigID = lenderConfigID,
					.CreatedByUserID = UserInfo.UserID,
					.NodePath = XA_UPLOAD_DOC_ENABLE(),
					.NodeGroup = SmSettings.NodeGroup.XaSettings,
				.NodeContent = SmUtil.BuildNodeAttributeXml("VISIBLE", "document_upload_enable", IIf(Common.SafeBoolean(Request.Form("xa_upload_doc_enable")), "Y", "N").ToString())
				})
			draftRecordList.Add(New SmLenderConfigDraft() With {
					.LenderConfigID = lenderConfigID,
					.CreatedByUserID = UserInfo.UserID,
					.NodePath = XA_UPLOAD_DOC_MESSAGE(),
					.NodeGroup = SmSettings.NodeGroup.XaSettings,
				.NodeContent = SmUtil.BuildContentNodeXml("UPLOAD_DOC_MESSAGE", Common.SafeString(Request.Form("xa_upload_doc_message")))
				})

			draftRecordList.Add(New SmLenderConfigDraft() With {
					.LenderConfigID = lenderConfigID,
					.CreatedByUserID = UserInfo.UserID,
					.NodePath = XA_SPECIAL_UPLOAD_DOC_ENABLE(),
					.NodeGroup = SmSettings.NodeGroup.XaSettings,
				.NodeContent = SmUtil.BuildNodeAttributeXml("VISIBLE", "document_special_upload_enable", IIf(Common.SafeBoolean(Request.Form("xa_special_upload_doc_enable")), "Y", "N").ToString())
				})
			draftRecordList.Add(New SmLenderConfigDraft() With {
					.LenderConfigID = lenderConfigID,
					.CreatedByUserID = UserInfo.UserID,
					.NodePath = XA_SPECIAL_UPLOAD_DOC_MESSAGE(),
					.NodeGroup = SmSettings.NodeGroup.XaSettings,
				.NodeContent = SmUtil.BuildContentNodeXml("SPECIAL_UPLOAD_DOC_MESSAGE", Common.SafeString(Request.Form("xa_special_upload_doc_message")))
				})

			draftRecordList.Add(New SmLenderConfigDraft() With {
					.LenderConfigID = lenderConfigID,
					.CreatedByUserID = UserInfo.UserID,
					.NodePath = XA_BUSINESS_UPLOAD_DOC_ENABLE(),
					.NodeGroup = SmSettings.NodeGroup.XaSettings,
				.NodeContent = SmUtil.BuildNodeAttributeXml("VISIBLE", "document_business_upload_enable", IIf(Common.SafeBoolean(Request.Form("xa_business_upload_doc_enable")), "Y", "N").ToString())
				})
			draftRecordList.Add(New SmLenderConfigDraft() With {
					.LenderConfigID = lenderConfigID,
					.CreatedByUserID = UserInfo.UserID,
					.NodePath = XA_BUSINESS_UPLOAD_DOC_MESSAGE(),
					.NodeGroup = SmSettings.NodeGroup.XaSettings,
				.NodeContent = SmUtil.BuildContentNodeXml("UPLOAD_BUSINESS_DOC_MESSAGE", Common.SafeString(Request.Form("xa_business_upload_doc_message")))
				})


			draftRecordList.Add(New SmLenderConfigDraft() With {
					.LenderConfigID = lenderConfigID,
					.CreatedByUserID = UserInfo.UserID,
					.NodePath = XA_JOINT_ENABLE(),
					.NodeGroup = SmSettings.NodeGroup.XaSettings,
				.NodeContent = SmUtil.BuildNodeAttributeXml("VISIBLE", "joint_enable", IIf(Common.SafeBoolean(Request.Form("xa_joint_enable")), "Y", "N").ToString())
				})
			draftRecordList.Add(New SmLenderConfigDraft() With {
					.LenderConfigID = lenderConfigID,
					.CreatedByUserID = UserInfo.UserID,
					.NodePath = XA_COLLECT_JOB_TITLE_ONLY(),
					.NodeGroup = SmSettings.NodeGroup.XaSettings,
				.NodeContent = SmUtil.BuildNodeAttributeXml("VISIBLE", "collect_job_title_only", IIf(Common.SafeBoolean(Request.Form("xa_collect_job_title_only")), "Y", "N").ToString())
				})
			draftRecordList.Add(New SmLenderConfigDraft() With {
					.LenderConfigID = lenderConfigID,
					.CreatedByUserID = UserInfo.UserID,
					.NodePath = SA_JOINT_ENABLE(),
					.NodeGroup = SmSettings.NodeGroup.XaSettings,
				.NodeContent = SmUtil.BuildNodeAttributeXml("VISIBLE", "joint_enable_sa", IIf(Common.SafeBoolean(Request.Form("sa_joint_enable")), "Y", "N").ToString())
				})
			draftRecordList.Add(New SmLenderConfigDraft() With {
					.LenderConfigID = lenderConfigID,
					.CreatedByUserID = UserInfo.UserID,
					.NodePath = XA_LOCATION_POOL(),
					.NodeGroup = SmSettings.NodeGroup.XaSettings,
				.NodeContent = SmUtil.BuildNodeAttributeXml("WEBSITE", "location_pool", IIf(Common.SafeBoolean(Request.Form("xa_location_pool")), "Y", "N").ToString())
				})

			draftRecordList.Add(New SmLenderConfigDraft() With {
					.LenderConfigID = lenderConfigID,
					.CreatedByUserID = UserInfo.UserID,
					.NodePath = CREDIT_PULL(),
					.NodeGroup = SmSettings.NodeGroup.XaSettings,
				.NodeContent = SmUtil.BuildNodeAttributeXml("WEBSITE", "credit_pull", IIf(Common.SafeBoolean(Request.Form("credit_pull")), "Y", "N").ToString())
				})

			draftRecordList.Add(New SmLenderConfigDraft() With {
					.LenderConfigID = lenderConfigID,
					.CreatedByUserID = UserInfo.UserID,
					.NodePath = DECISIONXA_ENABLE(),
					.NodeGroup = SmSettings.NodeGroup.XaSettings,
				.NodeContent = SmUtil.BuildNodeAttributeXml("WEBSITE", "decisionXA_enable", IIf(Common.SafeBoolean(Request.Form("decision_xa_enabled")), "Y", "N").ToString())
				})

			draftRecordList.Add(New SmLenderConfigDraft() With {
				.LenderConfigID = lenderConfigID,
				.CreatedByUserID = UserInfo.UserID,
				.NodePath = Sm.IDA(),
				.NodeGroup = SmSettings.NodeGroup.XaSettings,
				.NodeContent = SmUtil.BuildNodeAttributeXml("WEBSITE", "ida", Common.SafeStripHtmlString(Request.Form("ida")))
				})

			draftRecordList.Add(New SmLenderConfigDraft() With {
					.LenderConfigID = lenderConfigID,
					.CreatedByUserID = UserInfo.UserID,
					.NodePath = Sm.DEBIT(),
					.NodeGroup = SmSettings.NodeGroup.XaSettings,
				.NodeContent = SmUtil.BuildNodeAttributeXml("WEBSITE", "debit", Common.SafeStripHtmlString(Request.Form("debit")))
				})

			draftRecordList.Add(New SmLenderConfigDraft() With {
					.LenderConfigID = lenderConfigID,
					.CreatedByUserID = UserInfo.UserID,
					.NodePath = XA_STATUS(),
					.NodeGroup = SmSettings.NodeGroup.XaSettings,
				.NodeContent = SmUtil.BuildNodeAttributeXml("WEBSITE", "loan_status_enum", Common.SafeStripHtmlString(Request.Form("xa_status")))
				})
			draftRecordList.Add(New SmLenderConfigDraft() With {
					.LenderConfigID = lenderConfigID,
					.CreatedByUserID = UserInfo.UserID,
					.NodePath = XA_FUNDING_ENABLE(),
					.NodeGroup = SmSettings.NodeGroup.XaSettings,
				.NodeContent = SmUtil.BuildNodeAttributeXml("WEBSITE", "funding_enable", IIf(Common.SafeBoolean(Request.Form("xa_funding_enable")), "Y", "N").ToString())
				})

			draftRecordList.Add(New SmLenderConfigDraft() With {
					.LenderConfigID = lenderConfigID,
					.CreatedByUserID = UserInfo.UserID,
					.NodePath = XA_FUNDING_ACCOUNT_NUMBER_FREE_FORM(),
					.NodeGroup = SmSettings.NodeGroup.XaSettings,
				.NodeContent = SmUtil.BuildNodeAttributeXml("BEHAVIOR", "funding_account_number_free_form", IIf(Common.SafeBoolean(Request.Form("xa_funding_account_number_free_form_enable")), "Y", "N").ToString())
				})
			draftRecordList.Add(New SmLenderConfigDraft() With {
					.LenderConfigID = lenderConfigID,
					.CreatedByUserID = UserInfo.UserID,
					.NodePath = Sm.XA_BOOKING(),
					.NodeGroup = SmSettings.NodeGroup.XaSettings,
				.NodeContent = SmUtil.BuildNodeAttributeXml("WEBSITE", "booking", Common.SafeStripHtmlString(Request.Form("xa_booking")))
				})

			'draftRecordList.Add(New SmLenderConfigDraft() With {
			'	.LenderConfigID = lenderConfigID,
			'	.CreatedByUserID = UserInfo.UserID,
			'	.NodePath = Sm.XSELL("XA_LOAN"),
			'	.NodeGroup = SmSettings.NodeGroup.XaSettings,
			'.NodeContent = SmUtil.BuildNodeAttributeXml("VISIBLE", "auto_cross_qualify", IIf(Common.SafeBoolean(Request.Form("xa_xsell")), "Y", "N").ToString())
			'})

			draftRecordList.Add(New SmLenderConfigDraft() With {
					.LenderConfigID = lenderConfigID,
					.CreatedByUserID = UserInfo.UserID,
					.NodePath = Sm.DOCU_SIGN("XA_LOAN"),
					.NodeGroup = SmSettings.NodeGroup.XaSettings,
				.NodeContent = SmUtil.BuildNodeAttributeXml("VISIBLE", "in_session_docu_sign", IIf(Common.SafeBoolean(Request.Form("xa_docusign")), "Y", "N").ToString())
				})

			draftRecordList.Add(New SmLenderConfigDraft() With {
					.LenderConfigID = lenderConfigID,
					.CreatedByUserID = UserInfo.UserID,
					.NodePath = Sm.DOCU_SIGN_PDF_GROUP("XA_LOAN"),
					.NodeGroup = SmSettings.NodeGroup.XaSettings,
				.NodeContent = SmUtil.BuildNodeAttributeXml("VISIBLE", "in_session_docu_sign_pdf_group_override", Common.SafeStripHtmlString(Request.Form("xa_docusign_group")))
				})
			draftRecordList.Add(New SmLenderConfigDraft() With {
				  .LenderConfigID = lenderConfigID,
				  .CreatedByUserID = UserInfo.UserID,
				  .NodePath = Sm.PREVIOUS_ADDRESS_THRESHOLD("XA_LOAN"),
				  .NodeGroup = SmSettings.NodeGroup.XaSettings,
				  .NodeContent = SmUtil.BuildNodeAttributeXml("XA_LOAN", "previous_address_threshold", IIf(Common.SafeBoolean(Request.Form("xa_previous_address_enable")), Common.SafeInteger(Request.Form("xa_previous_address_threshold")), ""))
			  })
		End If 'end xa

		Return draftRecordList
	End Function
	Private Sub SaveXaSetttingsDraft()
		Dim res As CJsonResponse
		Try
			Dim smBL As New SmBL()
			Dim lenderConfigID As Guid = Common.SafeGUID(Request.Form("lenderconfigid"))

			'Dim XAXSell As Boolean = Common.SafeBoolean(Request.Form("xa_xsell"))

			Dim lenderConfig As CLenderConfig = smBL.GetLiveConfig(lenderConfigID)
			If lenderConfig Is Nothing Then
				Response.StatusCode = 404 '' Forbidden
				Response.StatusDescription = "Not found"
				Response.End() '' Terminate all the rest processes
			End If
			Dim draftRecordList As List(Of SmLenderConfigDraft) = GenerateRecordList(lenderConfig)

			Dim revisionID As Integer = Common.SafeInteger(Request.Form("revisionID"))
			Dim result = smBL.SaveNodesToDraft(revisionID, draftRecordList.ToArray())
			If result Then
				res = New CJsonResponse(True, "", "OK")
			Else
				res = New CJsonResponse(False, "", "FAILED")
			End If
		Catch ex As Exception
			_log.Error("Could not SaveXaSettingsDraft.", ex)
			res = New CJsonResponse(False, "Unable to save draft. Please try again", "FAILED")
		End Try
		Response.Write(JsonConvert.SerializeObject(res))
		Response.End()
	End Sub
	Private Sub PreviewXaSettingsDraft()
		Dim res As CJsonResponse
		Try
			Dim smBL As New SmBL()
			Dim lenderConfigID As Guid = Common.SafeGUID(Request.Form("lenderconfigid"))
			'Dim XAXSell As Boolean = Common.SafeBoolean(Request.Form("xa_xsell"))
			Dim lenderConfig As CLenderConfig = smBL.GetLiveConfig(lenderConfigID)
			If lenderConfig Is Nothing Then
				Response.StatusCode = 404 '' Forbidden
				Response.StatusDescription = "Not found"
				Response.End() '' Terminate all the rest processes
			End If
			Dim draftRecordList As List(Of SmLenderConfigDraft) = GenerateRecordList(lenderConfig)

			Dim revisionID As Integer = Common.SafeInteger(Request.Form("revisionID"))
			Dim result = smBL.SaveNodesToDraft(revisionID, draftRecordList.ToArray())
			If result Then
				Dim previewUrl As String = smBL.GeneratePreviewConfig(lenderConfigID, UserInfo.Email, Function(config, code) String.Format("{0}/xa/xpressapp.aspx?lenderref={1}&autofill=true&previewCode={2}", _serverRoot, config.LenderRef, code), draftRecordList.ToArray())
				res = New CJsonResponse(True, "", "OK")
				res.Info = New With {.previewurl = previewUrl}
			Else
				res = New CJsonResponse(False, "", "FAILED")
			End If
		Catch ex As Exception
			_log.Error("Could not PreviewXaSettingsDraft.", ex)
			res = New CJsonResponse(False, "Unable to save and preview draft. Please try again", "FAILED")
		End Try
		Response.Write(JsonConvert.SerializeObject(res))
		Response.End()
	End Sub
	Private Sub PublishXaSettings()
		Dim res As CJsonResponse
		Try
			Dim smBL As New SmBL()
			Dim lenderConfigID As Guid = Common.SafeGUID(Request.Form("lenderconfigid"))
			Dim lenderConfig As CLenderConfig = smBL.GetLiveConfig(lenderConfigID)
			If lenderConfig Is Nothing Then
				Response.StatusCode = 404 '' Forbidden
				Response.StatusDescription = "Not found"
				Response.End() '' Terminate all the rest processes
			End If
			Dim draftRecordList As List(Of SmLenderConfigDraft) = GenerateRecordList(lenderConfig)

			Dim comment As String = Common.SafeStripHtmlString(Request.Form("comment"))
			Dim publishAll As Boolean = Common.SafeBoolean(Request.Form("publishAll"))
			Dim result = smBL.PublishNodes(lenderConfigID, UserInfo.UserID, Request.UserHostAddress, comment, publishAll, draftRecordList.ToArray())
			If result Then
				res = New CJsonResponse(True, "", "OK")
			Else
				res = New CJsonResponse(False, "", "FAILED")
			End If
		Catch ex As Exception
			_log.Error("Could not PublishXaSettings.", ex)
			res = New CJsonResponse(False, "Unable to publish data. Please try again", "FAILED")
		End Try
		Response.Write(JsonConvert.SerializeObject(res))
		Response.End()
	End Sub
#End Region
#Region "All loans settings"
	Private Sub SaveAllLoansSetttingsDraft()
		Dim res As CJsonResponse
		Try
			Dim smBL As New SmBL()
			Dim draftRecordList As New List(Of SmLenderConfigDraft)
			Dim lenderConfigID As Guid = Common.SafeGUID(Request.Form("lenderconfigid"))
			Dim lenderConfig As CLenderConfig = smBL.GetLiveConfig(lenderConfigID)
			If lenderConfig Is Nothing Then
				Response.StatusCode = 404 '' Forbidden
				Response.StatusDescription = "Not found"
				Response.End() '' Terminate all the rest processes
			End If
			Dim lenderConfigXml As New XmlDocument()
			lenderConfigXml.LoadXml(lenderConfig.ConfigData)
			Dim enableCC As Boolean = lenderConfigXml.SelectSingleNode("WEBSITE/CREDIT_CARD_LOAN") IsNot Nothing
			Dim enableHE As Boolean = lenderConfigXml.SelectSingleNode("WEBSITE/HOME_EQUITY_LOAN") IsNot Nothing
			Dim enablePL As Boolean = lenderConfigXml.SelectSingleNode("WEBSITE/PERSONAL_LOAN") IsNot Nothing
			Dim enableVL As Boolean = lenderConfigXml.SelectSingleNode("WEBSITE/VEHICLE_LOAN") IsNot Nothing



			''for all loan
			If enableCC Or enableHE Or enablePL Or enableVL Then
				draftRecordList.Add(New SmLenderConfigDraft() With {
					.LenderConfigID = lenderConfigID,
					.CreatedByUserID = UserInfo.UserID,
					.NodePath = UPLOAD_DOC_ENABLE(),
					.NodeGroup = SmSettings.NodeGroup.AllLoansSettings,
				.NodeContent = SmUtil.BuildNodeAttributeXml("VISIBLE", "document_upload_enable", IIf(Common.SafeBoolean(Request.Form("upload_doc_enable")), "Y", "N").ToString())
				})
				draftRecordList.Add(New SmLenderConfigDraft() With {
					.LenderConfigID = lenderConfigID,
					.CreatedByUserID = UserInfo.UserID,
					.NodePath = UPLOAD_DOC_MESSAGE(),
					.NodeGroup = SmSettings.NodeGroup.AllLoansSettings,
				.NodeContent = SmUtil.BuildContentNodeXml("UPLOAD_DOC_MESSAGE", Common.SafeString(Request.Form("upload_doc_message")))
				})
			End If 'all loans

			Dim revisionID As Integer = Common.SafeInteger(Request.Form("revisionID"))
			Dim result = smBL.SaveNodesToDraft(revisionID, draftRecordList.ToArray())
			If result Then
				res = New CJsonResponse(True, "", "OK")
			Else
				res = New CJsonResponse(False, "", "FAILED")
			End If
		Catch ex As Exception
			_log.Error("Could not SaveAllLoansSettingsDraft.", ex)
			res = New CJsonResponse(False, "Unable to save draft. Please try again", "FAILED")
		End Try
		Response.Write(JsonConvert.SerializeObject(res))
		Response.End()
	End Sub
	Private Sub PreviewAllLoansSettingsDraft()
		Dim res As CJsonResponse
		Try
			Dim smBL As New SmBL()
			Dim draftRecordList As New List(Of SmLenderConfigDraft)
			Dim lenderConfigID As Guid = Common.SafeGUID(Request.Form("lenderconfigid"))

			Dim lenderConfig As CLenderConfig = smBL.GetLiveConfig(lenderConfigID)
			If lenderConfig Is Nothing Then
				Response.StatusCode = 404 '' Forbidden
				Response.StatusDescription = "Not found"
				Response.End() '' Terminate all the rest processes
			End If
			Dim lenderConfigXml As New XmlDocument()
			lenderConfigXml.LoadXml(lenderConfig.ConfigData)
			Dim enableCC As Boolean = lenderConfigXml.SelectSingleNode("WEBSITE/CREDIT_CARD_LOAN") IsNot Nothing
			Dim enableHE As Boolean = lenderConfigXml.SelectSingleNode("WEBSITE/HOME_EQUITY_LOAN") IsNot Nothing
			Dim enablePL As Boolean = lenderConfigXml.SelectSingleNode("WEBSITE/PERSONAL_LOAN") IsNot Nothing
			Dim enableVL As Boolean = lenderConfigXml.SelectSingleNode("WEBSITE/VEHICLE_LOAN") IsNot Nothing


			''for all loan
			If enableCC Or enableHE Or enablePL Or enableVL Then
				draftRecordList.Add(New SmLenderConfigDraft() With {
					.LenderConfigID = lenderConfigID,
					.CreatedByUserID = UserInfo.UserID,
					.NodePath = UPLOAD_DOC_ENABLE(),
					.NodeGroup = SmSettings.NodeGroup.AllLoansSettings,
				.NodeContent = SmUtil.BuildNodeAttributeXml("VISIBLE", "document_upload_enable", IIf(Common.SafeBoolean(Request.Form("upload_doc_enable")), "Y", "N").ToString())
				})
				draftRecordList.Add(New SmLenderConfigDraft() With {
					.LenderConfigID = lenderConfigID,
					.CreatedByUserID = UserInfo.UserID,
					.NodePath = UPLOAD_DOC_MESSAGE(),
					.NodeGroup = SmSettings.NodeGroup.AllLoansSettings,
				.NodeContent = SmUtil.BuildContentNodeXml("UPLOAD_DOC_MESSAGE", Common.SafeString(Request.Form("upload_doc_message")))
				})
			End If

			Dim revisionID As Integer = Common.SafeInteger(Request.Form("revisionID"))
			Dim result = smBL.SaveNodesToDraft(revisionID, draftRecordList.ToArray())
			If result Then
				Dim previewUrl As String = smBL.GeneratePreviewConfig(lenderConfigID, UserInfo.Email, Function(config, code) String.Format("{0}/pl/personalloan.aspx?lenderref={1}&autofill=true&previewCode={2}", _serverRoot, config.LenderRef, code), draftRecordList.ToArray())
				res = New CJsonResponse(True, "", "OK")
				res.Info = New With {.previewurl = previewUrl}
			Else
				res = New CJsonResponse(False, "", "FAILED")
			End If
		Catch ex As Exception
			_log.Error("Could not PreviewAllLoansSettingsDraft.", ex)
			res = New CJsonResponse(False, "Unable to save and preview draft. Please try again", "FAILED")
		End Try
		Response.Write(JsonConvert.SerializeObject(res))
		Response.End()
	End Sub
	Private Sub PublishAllLoansSettings()
		Dim res As CJsonResponse
		Try
			Dim smBL As New SmBL()
			Dim draftRecordList As New List(Of SmLenderConfigDraft)
			Dim lenderConfigID As Guid = Common.SafeGUID(Request.Form("lenderconfigid"))

			Dim lenderConfig As CLenderConfig = smBL.GetLiveConfig(lenderConfigID)
			If lenderConfig Is Nothing Then
				Response.StatusCode = 404 '' Forbidden
				Response.StatusDescription = "Not found"
				Response.End() '' Terminate all the rest processes
			End If
			Dim lenderConfigXml As New XmlDocument()
			lenderConfigXml.LoadXml(lenderConfig.ConfigData)
			Dim enableCC As Boolean = lenderConfigXml.SelectSingleNode("WEBSITE/CREDIT_CARD_LOAN") IsNot Nothing
			Dim enableHE As Boolean = lenderConfigXml.SelectSingleNode("WEBSITE/HOME_EQUITY_LOAN") IsNot Nothing
			Dim enablePL As Boolean = lenderConfigXml.SelectSingleNode("WEBSITE/PERSONAL_LOAN") IsNot Nothing
			Dim enableVL As Boolean = lenderConfigXml.SelectSingleNode("WEBSITE/VEHICLE_LOAN") IsNot Nothing


			''for all loan
			If enableCC Or enableHE Or enablePL Or enableVL Then
				draftRecordList.Add(New SmLenderConfigDraft() With {
					.LenderConfigID = lenderConfigID,
					.CreatedByUserID = UserInfo.UserID,
					.NodePath = UPLOAD_DOC_ENABLE(),
					.NodeGroup = SmSettings.NodeGroup.AllLoansSettings,
				.NodeContent = SmUtil.BuildNodeAttributeXml("VISIBLE", "document_upload_enable", IIf(Common.SafeBoolean(Request.Form("upload_doc_enable")), "Y", "N").ToString())
				})
				draftRecordList.Add(New SmLenderConfigDraft() With {
					.LenderConfigID = lenderConfigID,
					.CreatedByUserID = UserInfo.UserID,
					.NodePath = UPLOAD_DOC_MESSAGE(),
					.NodeGroup = SmSettings.NodeGroup.AllLoansSettings,
				.NodeContent = SmUtil.BuildContentNodeXml("UPLOAD_DOC_MESSAGE", Common.SafeString(Request.Form("upload_doc_message")))
				})
			End If
			Dim comment As String = Common.SafeStripHtmlString(Request.Form("comment"))
			Dim publishAll As Boolean = Common.SafeBoolean(Request.Form("publishAll"))
			Dim result = smBL.PublishNodes(lenderConfigID, UserInfo.UserID, Request.UserHostAddress, comment, publishAll, draftRecordList.ToArray())
			If result Then
				res = New CJsonResponse(True, "", "OK")
			Else
				res = New CJsonResponse(False, "", "FAILED")
			End If
		Catch ex As Exception
			_log.Error("Could not PublishAllLoansSettingsDraft.", ex)
			res = New CJsonResponse(False, "Unable to publish data. Please try again", "FAILED")
		End Try
		Response.Write(JsonConvert.SerializeObject(res))
		Response.End()
	End Sub
#End Region
#Region "BL settings"
	Private Sub SaveBLSetttingsDraft()
		Dim res As CJsonResponse
		Try
			Dim smBL As New SmBL()
			Dim draftRecordList As New List(Of SmLenderConfigDraft)
			Dim lenderConfigID As Guid = Common.SafeGUID(Request.Form("lenderconfigid"))
			Dim lenderConfig As CLenderConfig = smBL.GetLiveConfig(lenderConfigID)
			If lenderConfig Is Nothing Then
				Response.StatusCode = 404 '' Forbidden
				Response.StatusDescription = "Not found"
				Response.End() '' Terminate all the rest processes
			End If
			Dim lenderConfigXml As New XmlDocument()
			lenderConfigXml.LoadXml(lenderConfig.ConfigData)
			Dim enableBL As Boolean = lenderConfigXml.SelectSingleNode("WEBSITE/BUSINESS_LOAN") IsNot Nothing

			If enableBL Then
				draftRecordList.Add(New SmLenderConfigDraft() With {
					.LenderConfigID = lenderConfigID,
					.CreatedByUserID = UserInfo.UserID,
					.NodePath = BL_UPLOAD_DOC_ENABLE(),
					.NodeGroup = SmSettings.NodeGroup.BLSettings,
				.NodeContent = SmUtil.BuildNodeAttributeXml("VISIBLE", "document_upload_enable", IIf(Common.SafeBoolean(Request.Form("upload_doc_enable")), "Y", "N").ToString())
				})
				draftRecordList.Add(New SmLenderConfigDraft() With {
					.LenderConfigID = lenderConfigID,
					.CreatedByUserID = UserInfo.UserID,
					.NodePath = BL_UPLOAD_DOC_MESSAGE(),
					.NodeGroup = SmSettings.NodeGroup.BLSettings,
				.NodeContent = SmUtil.BuildContentNodeXml("UPLOAD_DOC_MESSAGE", Common.SafeString(Request.Form("upload_doc_message")))
				})
                draftRecordList.Add(New SmLenderConfigDraft() With {
                 .LenderConfigID = lenderConfigID,
                 .CreatedByUserID = UserInfo.UserID,
                 .NodePath = Sm.PREVIOUS_ADDRESS_THRESHOLD("BUSINESS_LOAN"),
                 .NodeGroup = SmSettings.NodeGroup.BLSettings,
                 .NodeContent = SmUtil.BuildNodeAttributeXml("BUSINESS_LOAN", "previous_address_threshold", IIf(Common.SafeBoolean(Request.Form("bl_previous_address_enable")), Common.SafeInteger(Request.Form("bl_previous_address_threshold")), ""))
                 })
                draftRecordList.Add(New SmLenderConfigDraft() With {
                    .LenderConfigID = lenderConfigID,
                    .CreatedByUserID = UserInfo.UserID,
                    .NodePath = Sm.PREVIOUS_EMPLOYMENT_THRESHOLD("BUSINESS_LOAN"),
                    .NodeGroup = SmSettings.NodeGroup.BLSettings,
                    .NodeContent = SmUtil.BuildNodeAttributeXml("BUSINESS_LOAN", "previous_employment_threshold", IIf(Common.SafeBoolean(Request.Form("bl_previous_employment_enable")), Common.SafeInteger(Request.Form("bl_previous_employment_threshold")), ""))
                })
            End If 'all loans

			Dim revisionID As Integer = Common.SafeInteger(Request.Form("revisionID"))
			Dim result = smBL.SaveNodesToDraft(revisionID, draftRecordList.ToArray())
			If result Then
				res = New CJsonResponse(True, "", "OK")
			Else
				res = New CJsonResponse(False, "", "FAILED")
			End If
		Catch ex As Exception
			_log.Error("Could not SaveBLSetttingsDraft.", ex)
			res = New CJsonResponse(False, "Unable to save draft. Please try again", "FAILED")
		End Try
		Response.Write(JsonConvert.SerializeObject(res))
		Response.End()
	End Sub
	Private Sub PreviewBLSettingsDraft()
		Dim res As CJsonResponse
		Try
			Dim smBL As New SmBL()
			Dim draftRecordList As New List(Of SmLenderConfigDraft)
			Dim lenderConfigID As Guid = Common.SafeGUID(Request.Form("lenderconfigid"))

			Dim lenderConfig As CLenderConfig = smBL.GetLiveConfig(lenderConfigID)
			If lenderConfig Is Nothing Then
				Response.StatusCode = 404 '' Forbidden
				Response.StatusDescription = "Not found"
				Response.End() '' Terminate all the rest processes
			End If
			Dim lenderConfigXml As New XmlDocument()
			lenderConfigXml.LoadXml(lenderConfig.ConfigData)
			Dim enableBL As Boolean = lenderConfigXml.SelectSingleNode("WEBSITE/BUSINESS_LOAN") IsNot Nothing
			If enableBL Then
				draftRecordList.Add(New SmLenderConfigDraft() With {
					.LenderConfigID = lenderConfigID,
					.CreatedByUserID = UserInfo.UserID,
					.NodePath = BL_UPLOAD_DOC_ENABLE(),
					.NodeGroup = SmSettings.NodeGroup.BLSettings,
				.NodeContent = SmUtil.BuildNodeAttributeXml("VISIBLE", "document_upload_enable", IIf(Common.SafeBoolean(Request.Form("upload_doc_enable")), "Y", "N").ToString())
				})
				draftRecordList.Add(New SmLenderConfigDraft() With {
					.LenderConfigID = lenderConfigID,
					.CreatedByUserID = UserInfo.UserID,
					.NodePath = BL_UPLOAD_DOC_MESSAGE(),
					.NodeGroup = SmSettings.NodeGroup.BLSettings,
				.NodeContent = SmUtil.BuildContentNodeXml("UPLOAD_DOC_MESSAGE", Common.SafeString(Request.Form("upload_doc_message")))
				})
                draftRecordList.Add(New SmLenderConfigDraft() With {
                 .LenderConfigID = lenderConfigID,
                 .CreatedByUserID = UserInfo.UserID,
                 .NodePath = Sm.PREVIOUS_ADDRESS_THRESHOLD("BUSINESS_LOAN"),
                 .NodeGroup = SmSettings.NodeGroup.BLSettings,
                 .NodeContent = SmUtil.BuildNodeAttributeXml("BUSINESS_LOAN", "previous_address_threshold", IIf(Common.SafeBoolean(Request.Form("bl_previous_address_enable")), Common.SafeInteger(Request.Form("bl_previous_address_threshold")), ""))
                 })
                draftRecordList.Add(New SmLenderConfigDraft() With {
                    .LenderConfigID = lenderConfigID,
                    .CreatedByUserID = UserInfo.UserID,
                    .NodePath = Sm.PREVIOUS_EMPLOYMENT_THRESHOLD("BUSINESS_LOAN"),
                    .NodeGroup = SmSettings.NodeGroup.BLSettings,
                    .NodeContent = SmUtil.BuildNodeAttributeXml("BUSINESS_LOAN", "previous_employment_threshold", IIf(Common.SafeBoolean(Request.Form("bl_previous_employment_enable")), Common.SafeInteger(Request.Form("bl_previous_employment_threshold")), ""))
                })
            End If

			Dim revisionID As Integer = Common.SafeInteger(Request.Form("revisionID"))
			Dim result = smBL.SaveNodesToDraft(revisionID, draftRecordList.ToArray())
			If result Then
				Dim previewUrl As String = smBL.GeneratePreviewConfig(lenderConfigID, UserInfo.Email, Function(config, code) String.Format("{0}/apply.aspx?lenderref={1}&list=bl&previewCode={2}", _serverRoot, config.LenderRef, code), draftRecordList.ToArray())
				res = New CJsonResponse(True, "", "OK")
				res.Info = New With {.previewurl = previewUrl}
			Else
				res = New CJsonResponse(False, "", "FAILED")
			End If
		Catch ex As Exception
			_log.Error("Could not PreviewBLSettingsDraft.", ex)
			res = New CJsonResponse(False, "Unable to save and preview draft. Please try again", "FAILED")
		End Try
		Response.Write(JsonConvert.SerializeObject(res))
		Response.End()
	End Sub
	Private Sub PublishBLSettings()
		Dim res As CJsonResponse
		Try
			Dim smBL As New SmBL()
			Dim draftRecordList As New List(Of SmLenderConfigDraft)
			Dim lenderConfigID As Guid = Common.SafeGUID(Request.Form("lenderconfigid"))

			Dim lenderConfig As CLenderConfig = smBL.GetLiveConfig(lenderConfigID)
			If lenderConfig Is Nothing Then
				Response.StatusCode = 404 '' Forbidden
				Response.StatusDescription = "Not found"
				Response.End() '' Terminate all the rest processes
			End If
			Dim lenderConfigXml As New XmlDocument()
			lenderConfigXml.LoadXml(lenderConfig.ConfigData)
			Dim enableBL As Boolean = lenderConfigXml.SelectSingleNode("WEBSITE/BUSINESS_LOAN") IsNot Nothing
			If enableBL Then
				draftRecordList.Add(New SmLenderConfigDraft() With {
					.LenderConfigID = lenderConfigID,
					.CreatedByUserID = UserInfo.UserID,
					.NodePath = BL_UPLOAD_DOC_ENABLE(),
					.NodeGroup = SmSettings.NodeGroup.BLSettings,
				.NodeContent = SmUtil.BuildNodeAttributeXml("VISIBLE", "document_upload_enable", IIf(Common.SafeBoolean(Request.Form("upload_doc_enable")), "Y", "N").ToString())
				})
				draftRecordList.Add(New SmLenderConfigDraft() With {
					.LenderConfigID = lenderConfigID,
					.CreatedByUserID = UserInfo.UserID,
					.NodePath = BL_UPLOAD_DOC_MESSAGE(),
					.NodeGroup = SmSettings.NodeGroup.BLSettings,
				.NodeContent = SmUtil.BuildContentNodeXml("UPLOAD_DOC_MESSAGE", Common.SafeString(Request.Form("upload_doc_message")))
				})
                draftRecordList.Add(New SmLenderConfigDraft() With {
                 .LenderConfigID = lenderConfigID,
                 .CreatedByUserID = UserInfo.UserID,
                 .NodePath = Sm.PREVIOUS_ADDRESS_THRESHOLD("BUSINESS_LOAN"),
                 .NodeGroup = SmSettings.NodeGroup.BLSettings,
                 .NodeContent = SmUtil.BuildNodeAttributeXml("BUSINESS_LOAN", "previous_address_threshold", IIf(Common.SafeBoolean(Request.Form("bl_previous_address_enable")), Common.SafeInteger(Request.Form("bl_previous_address_threshold")), ""))
                 })
                draftRecordList.Add(New SmLenderConfigDraft() With {
                    .LenderConfigID = lenderConfigID,
                    .CreatedByUserID = UserInfo.UserID,
                    .NodePath = Sm.PREVIOUS_EMPLOYMENT_THRESHOLD("BUSINESS_LOAN"),
                    .NodeGroup = SmSettings.NodeGroup.BLSettings,
                    .NodeContent = SmUtil.BuildNodeAttributeXml("BUSINESS_LOAN", "previous_employment_threshold", IIf(Common.SafeBoolean(Request.Form("bl_previous_employment_enable")), Common.SafeInteger(Request.Form("bl_previous_employment_threshold")), ""))
                })
            End If
			Dim comment As String = Common.SafeStripHtmlString(Request.Form("comment"))
			Dim publishAll As Boolean = Common.SafeBoolean(Request.Form("publishAll"))
			Dim result = smBL.PublishNodes(lenderConfigID, UserInfo.UserID, Request.UserHostAddress, comment, publishAll, draftRecordList.ToArray())
			If result Then
				res = New CJsonResponse(True, "", "OK")
			Else
				res = New CJsonResponse(False, "", "FAILED")
			End If
		Catch ex As Exception
			_log.Error("Could not PublishBLSettings.", ex)
			res = New CJsonResponse(False, "Unable to publish data. Please try again", "FAILED")
		End Try
		Response.Write(JsonConvert.SerializeObject(res))
		Response.End()
	End Sub
#End Region
#Region "Credit Card Settings"
	Private Sub SaveCCSetttingsDraft()
		Dim res As CJsonResponse
		Try
			Dim smBL As New SmBL()
			Dim draftRecordList As New List(Of SmLenderConfigDraft)
			Dim lenderConfigID As Guid = Common.SafeGUID(Request.Form("lenderconfigid"))
			'cc
			'Dim CCXSell As Boolean = Common.SafeBoolean(Request.Form("cc_xsell"))
			Dim declarations = JsonConvert.DeserializeObject(Of List(Of String))(Request.Form("cc_declarations"))

			Dim lenderConfig As CLenderConfig = smBL.GetLiveConfig(lenderConfigID)
			If lenderConfig Is Nothing Then
				Response.StatusCode = 404 '' Forbidden
				Response.StatusDescription = "Not found"
				Response.End() '' Terminate all the rest processes
			End If
			Dim lenderConfigXml As New XmlDocument()
			lenderConfigXml.LoadXml(lenderConfig.ConfigData)
			Dim enableCC As Boolean = lenderConfigXml.SelectSingleNode("WEBSITE/CREDIT_CARD_LOAN") IsNot Nothing
			If enableCC Then
				draftRecordList.Add(New SmLenderConfigDraft() With {
				.LenderConfigID = lenderConfigID,
				.CreatedByUserID = UserInfo.UserID,
				.NodePath = CC_JOINT_ENABLE(),
				.NodeGroup = SmSettings.NodeGroup.CCSettings,
			.NodeContent = SmUtil.BuildNodeAttributeXml("VISIBLE", "joint_enable_cc", IIf(Common.SafeBoolean(Request.Form("cc_joint_enable")), "Y", "N").ToString())
			})
				''cc
				'draftRecordList.Add(New SmLenderConfigDraft() With {
				'	.LenderConfigID = lenderConfigID,
				'	.CreatedByUserID = UserInfo.UserID,
				'	.NodePath = Sm.XSELL("CREDIT_CARD_LOAN"),
				'	.NodeGroup = SmSettings.NodeGroup.CCSettings,
				'.NodeContent = SmUtil.BuildNodeAttributeXml("VISIBLE", "auto_cross_qualify", IIf(CCXSell, "Y", "N").ToString())
				'})

				draftRecordList.Add(New SmLenderConfigDraft() With {
					.LenderConfigID = lenderConfigID,
					.CreatedByUserID = UserInfo.UserID,
					.NodePath = Sm.DOCU_SIGN("CREDIT_CARD_LOAN"),
					.NodeGroup = SmSettings.NodeGroup.CCSettings,
				.NodeContent = SmUtil.BuildNodeAttributeXml("VISIBLE", "in_session_docu_sign", IIf(Common.SafeBoolean(Request.Form("cc_docusign")), "Y", "N").ToString())
				})

				draftRecordList.Add(New SmLenderConfigDraft() With {
					.LenderConfigID = lenderConfigID,
					.CreatedByUserID = UserInfo.UserID,
					.NodePath = Sm.DOCU_SIGN_PDF_GROUP("CREDIT_CARD_LOAN"),
					.NodeGroup = SmSettings.NodeGroup.CCSettings,
				.NodeContent = SmUtil.BuildNodeAttributeXml("VISIBLE", "in_session_docu_sign_pdf_group_override", Common.SafeStripHtmlString(Request.Form("cc_docusign_group")))
				})
				draftRecordList.Add(New SmLenderConfigDraft() With {
				   .LenderConfigID = lenderConfigID,
				   .CreatedByUserID = UserInfo.UserID,
				   .NodePath = Sm.LOAN_LOCATION_POOL("CREDIT_CARD_LOAN"),
				   .NodeGroup = SmSettings.NodeGroup.CCSettings,
				   .NodeContent = SmUtil.BuildNodeAttributeXml("VISIBLE", "location_pool", IIf(Common.SafeBoolean(Request.Form("cc_location_pool")), "Y", "N").ToString())
				   })
				draftRecordList.Add(New SmLenderConfigDraft() With {
					.LenderConfigID = lenderConfigID,
					.CreatedByUserID = UserInfo.UserID,
					.NodePath = LOAN_DECLARATIONS("CREDIT_CARD_LOAN"),
					.NodeGroup = SmSettings.NodeGroup.CCSettings,
					.NodeContent = SmUtil.BuildDeclarationsXml(declarations)
				})
                draftRecordList.Add(New SmLenderConfigDraft() With {
                 .LenderConfigID = lenderConfigID,
                 .CreatedByUserID = UserInfo.UserID,
                 .NodePath = Sm.PREVIOUS_ADDRESS_THRESHOLD("CREDIT_CARD_LOAN"),
                 .NodeGroup = SmSettings.NodeGroup.CCSettings,
                 .NodeContent = SmUtil.BuildNodeAttributeXml("CREDIT_CARD_LOAN", "previous_address_threshold", IIf(Common.SafeBoolean(Request.Form("cc_previous_address_enable")), Common.SafeInteger(Request.Form("cc_previous_address_threshold")), ""))
                 })
                draftRecordList.Add(New SmLenderConfigDraft() With {
                    .LenderConfigID = lenderConfigID,
                    .CreatedByUserID = UserInfo.UserID,
                    .NodePath = Sm.PREVIOUS_EMPLOYMENT_THRESHOLD("CREDIT_CARD_LOAN"),
                    .NodeGroup = SmSettings.NodeGroup.CCSettings,
                    .NodeContent = SmUtil.BuildNodeAttributeXml("CREDIT_CARD_LOAN", "previous_employment_threshold", IIf(Common.SafeBoolean(Request.Form("cc_previous_employment_enable")), Common.SafeInteger(Request.Form("cc_previous_employment_threshold")), ""))
                })

            End If
			Dim revisionID As Integer = Common.SafeInteger(Request.Form("revisionID"))
			Dim result = smBL.SaveNodesToDraft(revisionID, draftRecordList.ToArray())
			If result Then
				res = New CJsonResponse(True, "", "OK")
			Else
				res = New CJsonResponse(False, "", "FAILED")
			End If
		Catch ex As Exception
			_log.Error("Could not SaveCCSettingsDraft.", ex)
			res = New CJsonResponse(False, "Unable to save draft. Please try again", "FAILED")
		End Try
		Response.Write(JsonConvert.SerializeObject(res))
		Response.End()
	End Sub
	Private Sub PreviewCCSettingsDraft()
		Dim res As CJsonResponse
		Try
			Dim smBL As New SmBL()
			Dim draftRecordList As New List(Of SmLenderConfigDraft)
			Dim lenderConfigID As Guid = Common.SafeGUID(Request.Form("lenderconfigid"))
			Dim CCXSell As Boolean = Common.SafeBoolean(Request.Form("cc_xsell"))
			Dim declarations = JsonConvert.DeserializeObject(Of List(Of String))(Request.Form("cc_declarations"))
			Dim lenderConfig As CLenderConfig = smBL.GetLiveConfig(lenderConfigID)
			If lenderConfig Is Nothing Then
				Response.StatusCode = 404 '' Forbidden
				Response.StatusDescription = "Not found"
				Response.End() '' Terminate all the rest processes
			End If
			Dim lenderConfigXml As New XmlDocument()
			lenderConfigXml.LoadXml(lenderConfig.ConfigData)
			Dim enableCC As Boolean = lenderConfigXml.SelectSingleNode("WEBSITE/CREDIT_CARD_LOAN") IsNot Nothing

			If enableCC Then
				draftRecordList.Add(New SmLenderConfigDraft() With {
				.LenderConfigID = lenderConfigID,
				.CreatedByUserID = UserInfo.UserID,
				.NodePath = CC_JOINT_ENABLE(),
				.NodeGroup = SmSettings.NodeGroup.CCSettings,
			.NodeContent = SmUtil.BuildNodeAttributeXml("VISIBLE", "joint_enable_cc", IIf(Common.SafeBoolean(Request.Form("cc_joint_enable")), "Y", "N").ToString())
			})

				''cc
				'draftRecordList.Add(New SmLenderConfigDraft() With {
				'	.LenderConfigID = lenderConfigID,
				'	.CreatedByUserID = UserInfo.UserID,
				'	.NodePath = Sm.XSELL("CREDIT_CARD_LOAN"),
				'	.NodeGroup = SmSettings.NodeGroup.CCSettings,
				'.NodeContent = SmUtil.BuildNodeAttributeXml("VISIBLE", "auto_cross_qualify", IIf(CCXSell, "Y", "N").ToString())
				'})

				draftRecordList.Add(New SmLenderConfigDraft() With {
					.LenderConfigID = lenderConfigID,
					.CreatedByUserID = UserInfo.UserID,
					.NodePath = Sm.DOCU_SIGN("CREDIT_CARD_LOAN"),
					.NodeGroup = SmSettings.NodeGroup.CCSettings,
				.NodeContent = SmUtil.BuildNodeAttributeXml("VISIBLE", "in_session_docu_sign", IIf(Common.SafeBoolean(Request.Form("cc_docusign")), "Y", "N").ToString())
				})

				draftRecordList.Add(New SmLenderConfigDraft() With {
					.LenderConfigID = lenderConfigID,
					.CreatedByUserID = UserInfo.UserID,
					.NodePath = Sm.DOCU_SIGN_PDF_GROUP("CREDIT_CARD_LOAN"),
					.NodeGroup = SmSettings.NodeGroup.CCSettings,
				.NodeContent = SmUtil.BuildNodeAttributeXml("VISIBLE", "in_session_docu_sign_pdf_group_override", Common.SafeStripHtmlString(Request.Form("cc_docusign_group")))
				})
				draftRecordList.Add(New SmLenderConfigDraft() With {
				 .LenderConfigID = lenderConfigID,
				 .CreatedByUserID = UserInfo.UserID,
				 .NodePath = Sm.LOAN_LOCATION_POOL("CREDIT_CARD_LOAN"),
				 .NodeGroup = SmSettings.NodeGroup.CCSettings,
				 .NodeContent = SmUtil.BuildNodeAttributeXml("VISIBLE", "location_pool", IIf(Common.SafeBoolean(Request.Form("cc_location_pool")), "Y", "N").ToString())
				 })

				draftRecordList.Add(New SmLenderConfigDraft() With {
					.LenderConfigID = lenderConfigID,
					.CreatedByUserID = UserInfo.UserID,
					.NodePath = LOAN_DECLARATIONS("CREDIT_CARD_LOAN"),
					.NodeGroup = SmSettings.NodeGroup.CCSettings,
					.NodeContent = SmUtil.BuildDeclarationsXml(declarations)
				})
                draftRecordList.Add(New SmLenderConfigDraft() With {
                 .LenderConfigID = lenderConfigID,
                 .CreatedByUserID = UserInfo.UserID,
                 .NodePath = Sm.PREVIOUS_ADDRESS_THRESHOLD("CREDIT_CARD_LOAN"),
                 .NodeGroup = SmSettings.NodeGroup.CCSettings,
                 .NodeContent = SmUtil.BuildNodeAttributeXml("CREDIT_CARD_LOAN", "previous_address_threshold", IIf(Common.SafeBoolean(Request.Form("cc_previous_address_enable")), Common.SafeInteger(Request.Form("cc_previous_address_threshold")), ""))
                 })
                draftRecordList.Add(New SmLenderConfigDraft() With {
                    .LenderConfigID = lenderConfigID,
                    .CreatedByUserID = UserInfo.UserID,
                    .NodePath = Sm.PREVIOUS_EMPLOYMENT_THRESHOLD("CREDIT_CARD_LOAN"),
                    .NodeGroup = SmSettings.NodeGroup.CCSettings,
                    .NodeContent = SmUtil.BuildNodeAttributeXml("CREDIT_CARD_LOAN", "previous_employment_threshold", IIf(Common.SafeBoolean(Request.Form("cc_previous_employment_enable")), Common.SafeInteger(Request.Form("cc_previous_employment_threshold")), ""))
                })
            End If


			Dim revisionID As Integer = Common.SafeInteger(Request.Form("revisionID"))
			Dim result = smBL.SaveNodesToDraft(revisionID, draftRecordList.ToArray())
			If result Then
				Dim previewUrl As String = smBL.GeneratePreviewConfig(lenderConfigID, UserInfo.Email, Function(config, code) String.Format("{0}/cc/creditcard.aspx?lenderref={1}&autofill=true&previewCode={2}", _serverRoot, config.LenderRef, code), draftRecordList.ToArray())
				res = New CJsonResponse(True, "", "OK")
				res.Info = New With {.previewurl = previewUrl}
			Else
				res = New CJsonResponse(False, "", "FAILED")
			End If
		Catch ex As Exception
			_log.Error("Could not PreviewCCSettingsDraft.", ex)
			res = New CJsonResponse(False, "Unable to save and preview draft. Please try again", "FAILED")
		End Try
		Response.Write(JsonConvert.SerializeObject(res))
		Response.End()
	End Sub
	Private Sub PublishCCSettings()
		Dim res As CJsonResponse
		Try
			Dim smBL As New SmBL()
			Dim draftRecordList As New List(Of SmLenderConfigDraft)
			Dim lenderConfigID As Guid = Common.SafeGUID(Request.Form("lenderconfigid"))
			'Dim CCXSell As Boolean = Common.SafeBoolean(Request.Form("cc_xsell"))
			Dim declarations = JsonConvert.DeserializeObject(Of List(Of String))(Request.Form("cc_declarations"))
			Dim lenderConfig As CLenderConfig = smBL.GetLiveConfig(lenderConfigID)
			If lenderConfig Is Nothing Then
				Response.StatusCode = 404 '' Forbidden
				Response.StatusDescription = "Not found"
				Response.End() '' Terminate all the rest processes
			End If
			Dim lenderConfigXml As New XmlDocument()
			lenderConfigXml.LoadXml(lenderConfig.ConfigData)
			Dim enableCC As Boolean = lenderConfigXml.SelectSingleNode("WEBSITE/CREDIT_CARD_LOAN") IsNot Nothing

			If enableCC Then
				draftRecordList.Add(New SmLenderConfigDraft() With {
				.LenderConfigID = lenderConfigID,
				.CreatedByUserID = UserInfo.UserID,
				.NodePath = CC_JOINT_ENABLE(),
				.NodeGroup = SmSettings.NodeGroup.CCSettings,
				.NodeContent = SmUtil.BuildNodeAttributeXml("VISIBLE", "joint_enable_cc", IIf(Common.SafeBoolean(Request.Form("cc_joint_enable")), "Y", "N").ToString())
				})
				''cc
				'draftRecordList.Add(New SmLenderConfigDraft() With {
				'	.LenderConfigID = lenderConfigID,
				'	.CreatedByUserID = UserInfo.UserID,
				'	.NodePath = Sm.XSELL("CREDIT_CARD_LOAN"),
				'	.NodeGroup = SmSettings.NodeGroup.CCSettings,
				'.NodeContent = SmUtil.BuildNodeAttributeXml("VISIBLE", "auto_cross_qualify", IIf(CCXSell, "Y", "N").ToString())
				'})

				draftRecordList.Add(New SmLenderConfigDraft() With {
					.LenderConfigID = lenderConfigID,
					.CreatedByUserID = UserInfo.UserID,
					.NodePath = Sm.DOCU_SIGN("CREDIT_CARD_LOAN"),
					.NodeGroup = SmSettings.NodeGroup.CCSettings,
				.NodeContent = SmUtil.BuildNodeAttributeXml("VISIBLE", "in_session_docu_sign", IIf(Common.SafeBoolean(Request.Form("cc_docusign")), "Y", "N").ToString())
				})

				draftRecordList.Add(New SmLenderConfigDraft() With {
					.LenderConfigID = lenderConfigID,
					.CreatedByUserID = UserInfo.UserID,
					.NodePath = Sm.DOCU_SIGN_PDF_GROUP("CREDIT_CARD_LOAN"),
					.NodeGroup = SmSettings.NodeGroup.CCSettings,
				.NodeContent = SmUtil.BuildNodeAttributeXml("VISIBLE", "in_session_docu_sign_pdf_group_override", Common.SafeStripHtmlString(Request.Form("cc_docusign_group")))
				})
				draftRecordList.Add(New SmLenderConfigDraft() With {
				 .LenderConfigID = lenderConfigID,
				 .CreatedByUserID = UserInfo.UserID,
				 .NodePath = Sm.LOAN_LOCATION_POOL("CREDIT_CARD_LOAN"),
				 .NodeGroup = SmSettings.NodeGroup.CCSettings,
				 .NodeContent = SmUtil.BuildNodeAttributeXml("VISIBLE", "location_pool", IIf(Common.SafeBoolean(Request.Form("cc_location_pool")), "Y", "N").ToString())
				 })

				draftRecordList.Add(New SmLenderConfigDraft() With {
					.LenderConfigID = lenderConfigID,
					.CreatedByUserID = UserInfo.UserID,
					.NodePath = LOAN_DECLARATIONS("CREDIT_CARD_LOAN"),
					.NodeGroup = SmSettings.NodeGroup.CCSettings,
					.NodeContent = SmUtil.BuildDeclarationsXml(declarations)
				})
                draftRecordList.Add(New SmLenderConfigDraft() With {
                 .LenderConfigID = lenderConfigID,
                 .CreatedByUserID = UserInfo.UserID,
                 .NodePath = Sm.PREVIOUS_ADDRESS_THRESHOLD("CREDIT_CARD_LOAN"),
                 .NodeGroup = SmSettings.NodeGroup.CCSettings,
                 .NodeContent = SmUtil.BuildNodeAttributeXml("CREDIT_CARD_LOAN", "previous_address_threshold", IIf(Common.SafeBoolean(Request.Form("cc_previous_address_enable")), Common.SafeInteger(Request.Form("cc_previous_address_threshold")), ""))
                 })
                draftRecordList.Add(New SmLenderConfigDraft() With {
                    .LenderConfigID = lenderConfigID,
                    .CreatedByUserID = UserInfo.UserID,
                    .NodePath = Sm.PREVIOUS_EMPLOYMENT_THRESHOLD("CREDIT_CARD_LOAN"),
                    .NodeGroup = SmSettings.NodeGroup.CCSettings,
                    .NodeContent = SmUtil.BuildNodeAttributeXml("CREDIT_CARD_LOAN", "previous_employment_threshold", IIf(Common.SafeBoolean(Request.Form("cc_previous_employment_enable")), Common.SafeInteger(Request.Form("cc_previous_employment_threshold")), ""))
                })
            End If
			Dim comment As String = Common.SafeStripHtmlString(Request.Form("comment"))
			Dim publishAll As Boolean = Common.SafeBoolean(Request.Form("publishAll"))
			Dim result = smBL.PublishNodes(lenderConfigID, UserInfo.UserID, Request.UserHostAddress, comment, publishAll, draftRecordList.ToArray())
			If result Then
				res = New CJsonResponse(True, "", "OK")
			Else
				res = New CJsonResponse(False, "", "FAILED")
			End If
		Catch ex As Exception
			_log.Error("Could not PublishCCSettings.", ex)
			res = New CJsonResponse(False, "Unable to publish data. Please try again", "FAILED")
		End Try
		Response.Write(JsonConvert.SerializeObject(res))
		Response.End()
	End Sub
#End Region
#Region "Home Equity Loan Settings"

	Private Sub SaveHESetttingsDraft()
		Dim res As CJsonResponse
		Try
			Dim smBL As New SmBL()
			Dim draftRecordList As New List(Of SmLenderConfigDraft)
			Dim lenderConfigID As Guid = Common.SafeGUID(Request.Form("lenderconfigid"))
			Dim loanInterestRateList As New Dictionary(Of String, Double)
			'Dim HEXSell As Boolean = Common.SafeBoolean(Request.Form("he_xsell"))

			Dim declarations = JsonConvert.DeserializeObject(Of List(Of String))(Request.Form("he_declarations"))

			Dim lenderConfig As CLenderConfig = smBL.GetLiveConfig(lenderConfigID)
			If lenderConfig Is Nothing Then
				Response.StatusCode = 404 '' Forbidden
				Response.StatusDescription = "Not found"
				Response.End() '' Terminate all the rest processes
			End If
			Dim lenderConfigXml As New XmlDocument()
			lenderConfigXml.LoadXml(lenderConfig.ConfigData)
			Dim enableHE As Boolean = lenderConfigXml.SelectSingleNode("WEBSITE/HOME_EQUITY_LOAN") IsNot Nothing

			If enableHE Then
				draftRecordList.Add(New SmLenderConfigDraft() With {
				.LenderConfigID = lenderConfigID,
				.CreatedByUserID = UserInfo.UserID,
				.NodePath = HE_JOINT_ENABLE(),
				.NodeGroup = SmSettings.NodeGroup.HESettings,
			.NodeContent = SmUtil.BuildNodeAttributeXml("VISIBLE", "joint_enable_he", IIf(Common.SafeBoolean(Request.Form("he_joint_enable")), "Y", "N").ToString())
			})
				'he
				'draftRecordList.Add(New SmLenderConfigDraft() With {
				'	.LenderConfigID = lenderConfigID,
				'	.CreatedByUserID = UserInfo.UserID,
				'	.NodePath = Sm.XSELL("HOME_EQUITY_LOAN"),
				'	.NodeGroup = SmSettings.NodeGroup.HESettings,
				'.NodeContent = SmUtil.BuildNodeAttributeXml("VISIBLE", "auto_cross_qualify", IIf(HEXSell, "Y", "N").ToString())
				'})

				draftRecordList.Add(New SmLenderConfigDraft() With {
					.LenderConfigID = lenderConfigID,
					.CreatedByUserID = UserInfo.UserID,
					.NodePath = Sm.DOCU_SIGN("HOME_EQUITY_LOAN"),
					.NodeGroup = SmSettings.NodeGroup.HESettings,
				.NodeContent = SmUtil.BuildNodeAttributeXml("VISIBLE", "in_session_docu_sign", IIf(Common.SafeBoolean(Request.Form("he_docusign")), "Y", "N").ToString())
				})

				draftRecordList.Add(New SmLenderConfigDraft() With {
					.LenderConfigID = lenderConfigID,
					.CreatedByUserID = UserInfo.UserID,
					.NodePath = Sm.DOCU_SIGN_PDF_GROUP("HOME_EQUITY_LOAN"),
					.NodeGroup = SmSettings.NodeGroup.HESettings,
				.NodeContent = SmUtil.BuildNodeAttributeXml("VISIBLE", "in_session_docu_sign_pdf_group_override", Common.SafeStripHtmlString(Request.Form("he_docusign_group")))
				})
				loanInterestRateList.Add("he", Common.SafeDouble(Request.Form("he_interest_rate")))


				If loanInterestRateList.Any() Then
					draftRecordList.Add(New SmLenderConfigDraft() With {
						.LenderConfigID = lenderConfigID,
						.CreatedByUserID = UserInfo.UserID,
						.NodePath = Sm.CUSTOM_LIST_RATES("he"),
						.NodeGroup = SmSettings.NodeGroup.HESettings,
					.NodeContent = SmUtil.BuildCustomListRateXml(loanInterestRateList)
					})
				End If
				draftRecordList.Add(New SmLenderConfigDraft() With {
				  .LenderConfigID = lenderConfigID,
				  .CreatedByUserID = UserInfo.UserID,
				  .NodePath = Sm.LOAN_LOCATION_POOL("HOME_EQUITY_LOAN"),
				  .NodeGroup = SmSettings.NodeGroup.HESettings,
				  .NodeContent = SmUtil.BuildNodeAttributeXml("VISIBLE", "location_pool", IIf(Common.SafeBoolean(Request.Form("he_location_pool")), "Y", "N").ToString())
				  })

				draftRecordList.Add(New SmLenderConfigDraft() With {
					.LenderConfigID = lenderConfigID,
					.CreatedByUserID = UserInfo.UserID,
					.NodePath = LOAN_DECLARATIONS("HOME_EQUITY_LOAN"),
					.NodeGroup = SmSettings.NodeGroup.HESettings,
					.NodeContent = SmUtil.BuildDeclarationsXml(declarations)
				})
                draftRecordList.Add(New SmLenderConfigDraft() With {
                 .LenderConfigID = lenderConfigID,
                 .CreatedByUserID = UserInfo.UserID,
                 .NodePath = Sm.PREVIOUS_ADDRESS_THRESHOLD("HOME_EQUITY_LOAN"),
                 .NodeGroup = SmSettings.NodeGroup.HESettings,
                 .NodeContent = SmUtil.BuildNodeAttributeXml("HOME_EQUITY_LOAN", "previous_address_threshold", IIf(Common.SafeBoolean(Request.Form("he_previous_address_enable")), Common.SafeInteger(Request.Form("he_previous_address_threshold")), ""))
                 })
                draftRecordList.Add(New SmLenderConfigDraft() With {
                    .LenderConfigID = lenderConfigID,
                    .CreatedByUserID = UserInfo.UserID,
                    .NodePath = Sm.PREVIOUS_EMPLOYMENT_THRESHOLD("HOME_EQUITY_LOAN"),
                    .NodeGroup = SmSettings.NodeGroup.HESettings,
                    .NodeContent = SmUtil.BuildNodeAttributeXml("HOME_EQUITY_LOAN", "previous_employment_threshold", IIf(Common.SafeBoolean(Request.Form("he_previous_employment_enable")), Common.SafeInteger(Request.Form("he_previous_employment_threshold")), ""))
                })

            End If

			Dim revisionID As Integer = Common.SafeInteger(Request.Form("revisionID"))
			Dim result = smBL.SaveNodesToDraft(revisionID, draftRecordList.ToArray())
			If result Then
				res = New CJsonResponse(True, "", "OK")
			Else
				res = New CJsonResponse(False, "", "FAILED")
			End If
		Catch ex As Exception
			_log.Error("Could not SaveHESettingsDraft.", ex)
			res = New CJsonResponse(False, "Unable to save draft. Please try again", "FAILED")
		End Try
		Response.Write(JsonConvert.SerializeObject(res))
		Response.End()
	End Sub
	Private Sub PreviewHESettingsDraft()
		Dim res As CJsonResponse
		Try
			Dim smBL As New SmBL()
			Dim draftRecordList As New List(Of SmLenderConfigDraft)
			Dim lenderConfigID As Guid = Common.SafeGUID(Request.Form("lenderconfigid"))
			Dim loanInterestRateList As New Dictionary(Of String, Double)
			'Dim HEXSell As Boolean = Common.SafeBoolean(Request.Form("he_xsell"))
			Dim declarations = JsonConvert.DeserializeObject(Of List(Of String))(Request.Form("he_declarations"))

			Dim lenderConfig As CLenderConfig = smBL.GetLiveConfig(lenderConfigID)
			If lenderConfig Is Nothing Then
				Response.StatusCode = 404 '' Forbidden
				Response.StatusDescription = "Not found"
				Response.End() '' Terminate all the rest processes
			End If
			Dim lenderConfigXml As New XmlDocument()
			lenderConfigXml.LoadXml(lenderConfig.ConfigData)
			Dim enableHE As Boolean = lenderConfigXml.SelectSingleNode("WEBSITE/HOME_EQUITY_LOAN") IsNot Nothing

			If enableHE Then
				draftRecordList.Add(New SmLenderConfigDraft() With {
				.LenderConfigID = lenderConfigID,
				.CreatedByUserID = UserInfo.UserID,
				.NodePath = HE_JOINT_ENABLE(),
				.NodeGroup = SmSettings.NodeGroup.HESettings,
			.NodeContent = SmUtil.BuildNodeAttributeXml("VISIBLE", "joint_enable_he", IIf(Common.SafeBoolean(Request.Form("he_joint_enable")), "Y", "N").ToString())
			})
				'he
				'draftRecordList.Add(New SmLenderConfigDraft() With {
				'	.LenderConfigID = lenderConfigID,
				'	.CreatedByUserID = UserInfo.UserID,
				'	.NodePath = Sm.XSELL("HOME_EQUITY_LOAN"),
				'	.NodeGroup = SmSettings.NodeGroup.HESettings,
				'.NodeContent = SmUtil.BuildNodeAttributeXml("VISIBLE", "auto_cross_qualify", IIf(HEXSell, "Y", "N").ToString())
				'})

				draftRecordList.Add(New SmLenderConfigDraft() With {
					.LenderConfigID = lenderConfigID,
					.CreatedByUserID = UserInfo.UserID,
					.NodePath = Sm.DOCU_SIGN("HOME_EQUITY_LOAN"),
					.NodeGroup = SmSettings.NodeGroup.HESettings,
				.NodeContent = SmUtil.BuildNodeAttributeXml("VISIBLE", "in_session_docu_sign", IIf(Common.SafeBoolean(Request.Form("he_docusign")), "Y", "N").ToString())
				})

				draftRecordList.Add(New SmLenderConfigDraft() With {
					.LenderConfigID = lenderConfigID,
					.CreatedByUserID = UserInfo.UserID,
					.NodePath = Sm.DOCU_SIGN_PDF_GROUP("HOME_EQUITY_LOAN"),
					.NodeGroup = SmSettings.NodeGroup.HESettings,
				.NodeContent = SmUtil.BuildNodeAttributeXml("VISIBLE", "in_session_docu_sign_pdf_group_override", Common.SafeStripHtmlString(Request.Form("he_docusign_group")))
				})

				loanInterestRateList.Add("he", Common.SafeDouble(Request.Form("he_interest_rate")))
				If loanInterestRateList.Any() Then
					draftRecordList.Add(New SmLenderConfigDraft() With {
						.LenderConfigID = lenderConfigID,
						.CreatedByUserID = UserInfo.UserID,
						.NodePath = Sm.CUSTOM_LIST_RATES("he"),
						.NodeGroup = SmSettings.NodeGroup.HESettings,
					.NodeContent = SmUtil.BuildCustomListRateXml(loanInterestRateList)
					})
				End If
				draftRecordList.Add(New SmLenderConfigDraft() With {
					 .LenderConfigID = lenderConfigID,
					 .CreatedByUserID = UserInfo.UserID,
					 .NodePath = Sm.LOAN_LOCATION_POOL("HOME_EQUITY_LOAN"),
					 .NodeGroup = SmSettings.NodeGroup.HESettings,
					 .NodeContent = SmUtil.BuildNodeAttributeXml("VISIBLE", "location_pool", IIf(Common.SafeBoolean(Request.Form("he_location_pool")), "Y", "N").ToString())
				 })

				draftRecordList.Add(New SmLenderConfigDraft() With {
					.LenderConfigID = lenderConfigID,
					.CreatedByUserID = UserInfo.UserID,
					.NodePath = LOAN_DECLARATIONS("HOME_EQUITY_LOAN"),
					.NodeGroup = SmSettings.NodeGroup.HESettings,
					.NodeContent = SmUtil.BuildDeclarationsXml(declarations)
				})
                draftRecordList.Add(New SmLenderConfigDraft() With {
                 .LenderConfigID = lenderConfigID,
                 .CreatedByUserID = UserInfo.UserID,
                 .NodePath = Sm.PREVIOUS_ADDRESS_THRESHOLD("HOME_EQUITY_LOAN"),
                 .NodeGroup = SmSettings.NodeGroup.HESettings,
                 .NodeContent = SmUtil.BuildNodeAttributeXml("HOME_EQUITY_LOAN", "previous_address_threshold", IIf(Common.SafeBoolean(Request.Form("he_previous_address_enable")), Common.SafeInteger(Request.Form("he_previous_address_threshold")), ""))
                 })
                draftRecordList.Add(New SmLenderConfigDraft() With {
                    .LenderConfigID = lenderConfigID,
                    .CreatedByUserID = UserInfo.UserID,
                    .NodePath = Sm.PREVIOUS_EMPLOYMENT_THRESHOLD("HOME_EQUITY_LOAN"),
                    .NodeGroup = SmSettings.NodeGroup.HESettings,
                    .NodeContent = SmUtil.BuildNodeAttributeXml("HOME_EQUITY_LOAN", "previous_employment_threshold", IIf(Common.SafeBoolean(Request.Form("he_previous_employment_enable")), Common.SafeInteger(Request.Form("he_previous_employment_threshold")), ""))
                })
            End If
			Dim revisionID As Integer = Common.SafeInteger(Request.Form("revisionID"))
			Dim result = smBL.SaveNodesToDraft(revisionID, draftRecordList.ToArray())
			If result Then
				Dim previewUrl As String = smBL.GeneratePreviewConfig(lenderConfigID, UserInfo.Email, Function(config, code) String.Format("{0}/he/homeequityloan.aspx?lenderref={1}&autofill=true&previewCode={2}", _serverRoot, config.LenderRef, code), draftRecordList.ToArray())
				res = New CJsonResponse(True, "", "OK")
				res.Info = New With {.previewurl = previewUrl}
			Else
				res = New CJsonResponse(False, "", "FAILED")
			End If
		Catch ex As Exception
			_log.Error("Could not PreviewHESettingsDraft.", ex)
			res = New CJsonResponse(False, "Unable to save and preview draft. Please try again", "FAILED")
		End Try
		Response.Write(JsonConvert.SerializeObject(res))
		Response.End()
	End Sub
	Private Sub PublishHESettings()
		Dim res As New CJsonResponse()
		Try
			Dim smBL As New SmBL()
			Dim draftRecordList As New List(Of SmLenderConfigDraft)
			Dim lenderConfigID As Guid = Common.SafeGUID(Request.Form("lenderconfigid"))
			Dim HEJointEnable As Boolean = Common.SafeBoolean(Request.Form("he_joint_enable"))
			Dim loanInterestRateList As New Dictionary(Of String, Double)
			'Dim HEXSell As Boolean = Common.SafeBoolean(Request.Form("he_xsell"))
			Dim HEDocuSign As Boolean = Common.SafeBoolean(Request.Form("he_docusign"))
			Dim HEDocuSignGroup As String = Common.SafeStripHtmlString(Request.Form("he_docusign_group"))

			Dim declarations = JsonConvert.DeserializeObject(Of List(Of String))(Request.Form("he_declarations"))

			Dim lenderConfig As CLenderConfig = smBL.GetLiveConfig(lenderConfigID)
			If lenderConfig Is Nothing Then
				Response.StatusCode = 404 '' Forbidden
				Response.StatusDescription = "Not found"
				Response.End() '' Terminate all the rest processes
			End If
			Dim lenderConfigXml As New XmlDocument()
			lenderConfigXml.LoadXml(lenderConfig.ConfigData)
			Dim enableHE As Boolean = lenderConfigXml.SelectSingleNode("WEBSITE/HOME_EQUITY_LOAN") IsNot Nothing

			If enableHE Then
				draftRecordList.Add(New SmLenderConfigDraft() With {
				.LenderConfigID = lenderConfigID,
				.CreatedByUserID = UserInfo.UserID,
				.NodePath = HE_JOINT_ENABLE(),
				.NodeGroup = SmSettings.NodeGroup.HESettings,
				.NodeContent = SmUtil.BuildNodeAttributeXml("VISIBLE", "joint_enable_he", IIf(HEJointEnable, "Y", "N").ToString())
				})

				'he
				'draftRecordList.Add(New SmLenderConfigDraft() With {
				'	.LenderConfigID = lenderConfigID,
				'	.CreatedByUserID = UserInfo.UserID,
				'	.NodePath = Sm.XSELL("HOME_EQUITY_LOAN"),
				'	.NodeGroup = SmSettings.NodeGroup.HESettings,
				'.NodeContent = SmUtil.BuildNodeAttributeXml("VISIBLE", "auto_cross_qualify", IIf(HEXSell, "Y", "N").ToString())
				'})

				draftRecordList.Add(New SmLenderConfigDraft() With {
					.LenderConfigID = lenderConfigID,
					.CreatedByUserID = UserInfo.UserID,
					.NodePath = Sm.DOCU_SIGN("HOME_EQUITY_LOAN"),
					.NodeGroup = SmSettings.NodeGroup.HESettings,
				.NodeContent = SmUtil.BuildNodeAttributeXml("VISIBLE", "in_session_docu_sign", IIf(HEDocuSign, "Y", "N").ToString())
				})

				draftRecordList.Add(New SmLenderConfigDraft() With {
					.LenderConfigID = lenderConfigID,
					.CreatedByUserID = UserInfo.UserID,
					.NodePath = Sm.DOCU_SIGN_PDF_GROUP("HOME_EQUITY_LOAN"),
					.NodeGroup = SmSettings.NodeGroup.HESettings,
				.NodeContent = SmUtil.BuildNodeAttributeXml("VISIBLE", "in_session_docu_sign_pdf_group_override", HEDocuSignGroup)
				})
				loanInterestRateList.Add("he", Common.SafeDouble(Request.Form("he_interest_rate")))
				If loanInterestRateList.Any() Then
					draftRecordList.Add(New SmLenderConfigDraft() With {
						.LenderConfigID = lenderConfigID,
						.CreatedByUserID = UserInfo.UserID,
						.NodePath = Sm.CUSTOM_LIST_RATES("he"),
						.NodeGroup = SmSettings.NodeGroup.HESettings,
					.NodeContent = SmUtil.BuildCustomListRateXml(loanInterestRateList)
					})
				End If
				draftRecordList.Add(New SmLenderConfigDraft() With {
					 .LenderConfigID = lenderConfigID,
					 .CreatedByUserID = UserInfo.UserID,
					 .NodePath = Sm.LOAN_LOCATION_POOL("HOME_EQUITY_LOAN"),
					 .NodeGroup = SmSettings.NodeGroup.HESettings,
					 .NodeContent = SmUtil.BuildNodeAttributeXml("VISIBLE", "location_pool", IIf(Common.SafeBoolean(Request.Form("he_location_pool")), "Y", "N").ToString())
				 })

				draftRecordList.Add(New SmLenderConfigDraft() With {
					.LenderConfigID = lenderConfigID,
					.CreatedByUserID = UserInfo.UserID,
					.NodePath = LOAN_DECLARATIONS("HOME_EQUITY_LOAN"),
					.NodeGroup = SmSettings.NodeGroup.HESettings,
					.NodeContent = SmUtil.BuildDeclarationsXml(declarations)
				})
                draftRecordList.Add(New SmLenderConfigDraft() With {
                 .LenderConfigID = lenderConfigID,
                 .CreatedByUserID = UserInfo.UserID,
                 .NodePath = Sm.PREVIOUS_ADDRESS_THRESHOLD("HOME_EQUITY_LOAN"),
                 .NodeGroup = SmSettings.NodeGroup.HESettings,
                 .NodeContent = SmUtil.BuildNodeAttributeXml("HOME_EQUITY_LOAN", "previous_address_threshold", IIf(Common.SafeBoolean(Request.Form("he_previous_address_enable")), Common.SafeInteger(Request.Form("he_previous_address_threshold")), ""))
                 })
                draftRecordList.Add(New SmLenderConfigDraft() With {
                    .LenderConfigID = lenderConfigID,
                    .CreatedByUserID = UserInfo.UserID,
                    .NodePath = Sm.PREVIOUS_EMPLOYMENT_THRESHOLD("HOME_EQUITY_LOAN"),
                    .NodeGroup = SmSettings.NodeGroup.HESettings,
                    .NodeContent = SmUtil.BuildNodeAttributeXml("HOME_EQUITY_LOAN", "previous_employment_threshold", IIf(Common.SafeBoolean(Request.Form("he_previous_employment_enable")), Common.SafeInteger(Request.Form("he_previous_employment_threshold")), ""))
                })
            End If

			Dim comment As String = Common.SafeStripHtmlString(Request.Form("comment"))
			Dim publishAll As Boolean = Common.SafeBoolean(Request.Form("publishAll"))
			Dim result = smBL.PublishNodes(lenderConfigID, UserInfo.UserID, Request.UserHostAddress, comment, publishAll, draftRecordList.ToArray())
			If result Then
				res = New CJsonResponse(True, "", "OK")
			Else
				res = New CJsonResponse(False, "", "FAILED")
			End If
		Catch ex As Exception
			_log.Error("Could not PublishHESettings.", ex)
			res = New CJsonResponse(False, "Unable to publish data. Please try again", "FAILED")
		End Try
		Response.Write(JsonConvert.SerializeObject(res))
		Response.End()
	End Sub
#End Region
#Region "Personal Settings"
	Private Sub SavePLSetttingsDraft()
		Dim res As CJsonResponse
		Try
			Dim smBL As New SmBL()
			Dim draftRecordList As New List(Of SmLenderConfigDraft)
			Dim lenderConfigID As Guid = Common.SafeGUID(Request.Form("lenderconfigid"))
			Dim loanInterestRateList As New Dictionary(Of String, Double)
			'Dim PLXSell As Boolean = Common.SafeBoolean(Request.Form("pl_xsell"))
			Dim declarations = JsonConvert.DeserializeObject(Of List(Of String))(Request.Form("pl_declarations"))

			Dim lenderConfig As CLenderConfig = smBL.GetLiveConfig(lenderConfigID)
			If lenderConfig Is Nothing Then
				Response.StatusCode = 404 '' Forbidden
				Response.StatusDescription = "Not found"
				Response.End() '' Terminate all the rest processes
			End If
			Dim lenderConfigXml As New XmlDocument()
			lenderConfigXml.LoadXml(lenderConfig.ConfigData)
			Dim enablePL As Boolean = lenderConfigXml.SelectSingleNode("WEBSITE/PERSONAL_LOAN") IsNot Nothing

			If enablePL Then
				draftRecordList.Add(New SmLenderConfigDraft() With {
				.LenderConfigID = lenderConfigID,
				.CreatedByUserID = UserInfo.UserID,
				.NodePath = PL_JOINT_ENABLE(),
				.NodeGroup = SmSettings.NodeGroup.PLSettings,
			.NodeContent = SmUtil.BuildNodeAttributeXml("VISIBLE", "joint_enable_pl", IIf(Common.SafeBoolean(Request.Form("pl_joint_enable")), "Y", "N").ToString())
			})
				'pl
				'draftRecordList.Add(New SmLenderConfigDraft() With {
				'	.LenderConfigID = lenderConfigID,
				'	.CreatedByUserID = UserInfo.UserID,
				'	.NodePath = Sm.XSELL("PERSONAL_LOAN"),
				'	.NodeGroup = SmSettings.NodeGroup.PLSettings,
				'.NodeContent = SmUtil.BuildNodeAttributeXml("VISIBLE", "auto_cross_qualify", IIf(PLXSell, "Y", "N").ToString())
				'})

				draftRecordList.Add(New SmLenderConfigDraft() With {
					.LenderConfigID = lenderConfigID,
					.CreatedByUserID = UserInfo.UserID,
					.NodePath = Sm.DOCU_SIGN("PERSONAL_LOAN"),
					.NodeGroup = SmSettings.NodeGroup.PLSettings,
				.NodeContent = SmUtil.BuildNodeAttributeXml("VISIBLE", "in_session_docu_sign", IIf(Common.SafeBoolean(Request.Form("pl_docusign")), "Y", "N").ToString())
				})

				draftRecordList.Add(New SmLenderConfigDraft() With {
					.LenderConfigID = lenderConfigID,
					.CreatedByUserID = UserInfo.UserID,
					.NodePath = Sm.DOCU_SIGN_PDF_GROUP("PERSONAL_LOAN"),
					.NodeGroup = SmSettings.NodeGroup.PLSettings,
				.NodeContent = SmUtil.BuildNodeAttributeXml("VISIBLE", "in_session_docu_sign_pdf_group_override", Common.SafeStripHtmlString(Request.Form("pl_docusign_group")))
				})
				loanInterestRateList.Add("pl", Common.SafeDouble(Request.Form("pl_interest_rate")))
				If loanInterestRateList.Any() Then
					draftRecordList.Add(New SmLenderConfigDraft() With {
						.LenderConfigID = lenderConfigID,
						.CreatedByUserID = UserInfo.UserID,
						.NodePath = Sm.CUSTOM_LIST_RATES("pl"),
						.NodeGroup = SmSettings.NodeGroup.PLSettings,
					.NodeContent = SmUtil.BuildCustomListRateXml(loanInterestRateList)
					})
				End If
                draftRecordList.Add(New SmLenderConfigDraft() With {
                .LenderConfigID = lenderConfigID,
                .CreatedByUserID = UserInfo.UserID,
                .NodePath = Sm.LOAN_LOCATION_POOL("PERSONAL_LOAN"),
                .NodeGroup = SmSettings.NodeGroup.PLSettings,
                .NodeContent = SmUtil.BuildNodeAttributeXml("VISIBLE", "location_pool", IIf(Common.SafeBoolean(Request.Form("pl_location_pool")), "Y", "N").ToString())
                })
                draftRecordList.Add(New SmLenderConfigDraft() With {
                    .LenderConfigID = lenderConfigID,
                    .CreatedByUserID = UserInfo.UserID,
                    .NodePath = LOAN_DECLARATIONS("PERSONAL_LOAN"),
                    .NodeGroup = SmSettings.NodeGroup.PLSettings,
                    .NodeContent = SmUtil.BuildDeclarationsXml(declarations)
                })
                draftRecordList.Add(New SmLenderConfigDraft() With {
                 .LenderConfigID = lenderConfigID,
                 .CreatedByUserID = UserInfo.UserID,
                 .NodePath = Sm.PREVIOUS_ADDRESS_THRESHOLD("PERSONAL_LOAN"),
                 .NodeGroup = SmSettings.NodeGroup.PLSettings,
                 .NodeContent = SmUtil.BuildNodeAttributeXml("PERSONAL_LOAN", "previous_address_threshold", IIf(Common.SafeBoolean(Request.Form("pl_previous_address_enable")), Common.SafeInteger(Request.Form("pl_previous_address_threshold")), ""))
                 })
                draftRecordList.Add(New SmLenderConfigDraft() With {
                    .LenderConfigID = lenderConfigID,
                    .CreatedByUserID = UserInfo.UserID,
                    .NodePath = Sm.PREVIOUS_EMPLOYMENT_THRESHOLD("PERSONAL_LOAN"),
                    .NodeGroup = SmSettings.NodeGroup.PLSettings,
                    .NodeContent = SmUtil.BuildNodeAttributeXml("PERSONAL_LOAN", "previous_employment_threshold", IIf(Common.SafeBoolean(Request.Form("pl_previous_employment_enable")), Common.SafeInteger(Request.Form("pl_previous_employment_threshold")), ""))
                })
            End If


			Dim revisionID As Integer = Common.SafeInteger(Request.Form("revisionID"))
			Dim result = smBL.SaveNodesToDraft(revisionID, draftRecordList.ToArray())
			If result Then
				res = New CJsonResponse(True, "", "OK")
			Else
				res = New CJsonResponse(False, "", "FAILED")
			End If
		Catch ex As Exception
			_log.Error("Could not SavePLSettingsDraft.", ex)
			res = New CJsonResponse(False, "Unable to save draft. Please try again", "FAILED")
		End Try
		Response.Write(JsonConvert.SerializeObject(res))
		Response.End()
	End Sub
	Private Sub PreviewPLSettingsDraft()
		Dim res As CJsonResponse
		Try
			Dim smBL As New SmBL()
			Dim draftRecordList As New List(Of SmLenderConfigDraft)
			Dim lenderConfigID As Guid = Common.SafeGUID(Request.Form("lenderconfigid"))
			Dim PLJointEnable As Boolean = Common.SafeBoolean(Request.Form("pl_joint_enable"))
			Dim loanInterestRateList As New Dictionary(Of String, Double)
			'Dim PLXSell As Boolean = Common.SafeBoolean(Request.Form("pl_xsell"))
			Dim declarations = JsonConvert.DeserializeObject(Of List(Of String))(Request.Form("pl_declarations"))
			Dim PLDocuSign As Boolean = Common.SafeBoolean(Request.Form("pl_docusign"))
			Dim PLDocuSignGroup As String = Common.SafeStripHtmlString(Request.Form("pl_docusign_group"))
			Dim lenderConfig As CLenderConfig = smBL.GetLiveConfig(lenderConfigID)
			If lenderConfig Is Nothing Then
				Response.StatusCode = 404 '' Forbidden
				Response.StatusDescription = "Not found"
				Response.End() '' Terminate all the rest processes
			End If
			Dim lenderConfigXml As New XmlDocument()
			lenderConfigXml.LoadXml(lenderConfig.ConfigData)
			Dim enablePL As Boolean = lenderConfigXml.SelectSingleNode("WEBSITE/PERSONAL_LOAN") IsNot Nothing


			If enablePL Then
				draftRecordList.Add(New SmLenderConfigDraft() With {
				.LenderConfigID = lenderConfigID,
				.CreatedByUserID = UserInfo.UserID,
				.NodePath = PL_JOINT_ENABLE(),
				.NodeGroup = SmSettings.NodeGroup.PLSettings,
				.NodeContent = SmUtil.BuildNodeAttributeXml("VISIBLE", "joint_enable_pl", IIf(PLJointEnable, "Y", "N").ToString())
			})
				'pl
				'draftRecordList.Add(New SmLenderConfigDraft() With {
				'	.LenderConfigID = lenderConfigID,
				'	.CreatedByUserID = UserInfo.UserID,
				'	.NodePath = Sm.XSELL("PERSONAL_LOAN"),
				'	.NodeGroup = SmSettings.NodeGroup.PLSettings,
				'.NodeContent = SmUtil.BuildNodeAttributeXml("VISIBLE", "auto_cross_qualify", IIf(PLXSell, "Y", "N").ToString())
				'})

				draftRecordList.Add(New SmLenderConfigDraft() With {
					.LenderConfigID = lenderConfigID,
					.CreatedByUserID = UserInfo.UserID,
					.NodePath = Sm.DOCU_SIGN("PERSONAL_LOAN"),
					.NodeGroup = SmSettings.NodeGroup.PLSettings,
				.NodeContent = SmUtil.BuildNodeAttributeXml("VISIBLE", "in_session_docu_sign", IIf(PLDocuSign, "Y", "N").ToString())
				})

				draftRecordList.Add(New SmLenderConfigDraft() With {
					.LenderConfigID = lenderConfigID,
					.CreatedByUserID = UserInfo.UserID,
					.NodePath = Sm.DOCU_SIGN_PDF_GROUP("PERSONAL_LOAN"),
					.NodeGroup = SmSettings.NodeGroup.PLSettings,
				.NodeContent = SmUtil.BuildNodeAttributeXml("VISIBLE", "in_session_docu_sign_pdf_group_override", PLDocuSignGroup)
				})
				loanInterestRateList.Add("pl", Common.SafeDouble(Request.Form("pl_interest_rate")))
				If loanInterestRateList.Any() Then
					draftRecordList.Add(New SmLenderConfigDraft() With {
						.LenderConfigID = lenderConfigID,
						.CreatedByUserID = UserInfo.UserID,
						.NodePath = Sm.CUSTOM_LIST_RATES("pl"),
						.NodeGroup = SmSettings.NodeGroup.PLSettings,
					.NodeContent = SmUtil.BuildCustomListRateXml(loanInterestRateList)
					})
				End If
                draftRecordList.Add(New SmLenderConfigDraft() With {
                    .LenderConfigID = lenderConfigID,
                    .CreatedByUserID = UserInfo.UserID,
                    .NodePath = Sm.LOAN_LOCATION_POOL("PERSONAL_LOAN"),
                    .NodeGroup = SmSettings.NodeGroup.PLSettings,
                    .NodeContent = SmUtil.BuildNodeAttributeXml("VISIBLE", "location_pool", IIf(Common.SafeBoolean(Request.Form("pl_location_pool")), "Y", "N").ToString())
                })
                draftRecordList.Add(New SmLenderConfigDraft() With {
                    .LenderConfigID = lenderConfigID,
                    .CreatedByUserID = UserInfo.UserID,
                    .NodePath = LOAN_DECLARATIONS("PERSONAL_LOAN"),
                    .NodeGroup = SmSettings.NodeGroup.PLSettings,
                    .NodeContent = SmUtil.BuildDeclarationsXml(declarations)
                })
                draftRecordList.Add(New SmLenderConfigDraft() With {
                 .LenderConfigID = lenderConfigID,
                 .CreatedByUserID = UserInfo.UserID,
                 .NodePath = Sm.PREVIOUS_ADDRESS_THRESHOLD("PERSONAL_LOAN"),
                 .NodeGroup = SmSettings.NodeGroup.PLSettings,
                 .NodeContent = SmUtil.BuildNodeAttributeXml("PERSONAL_LOAN", "previous_address_threshold", IIf(Common.SafeBoolean(Request.Form("pl_previous_address_enable")), Common.SafeInteger(Request.Form("pl_previous_address_threshold")), ""))
                 })
                draftRecordList.Add(New SmLenderConfigDraft() With {
                    .LenderConfigID = lenderConfigID,
                    .CreatedByUserID = UserInfo.UserID,
                    .NodePath = Sm.PREVIOUS_EMPLOYMENT_THRESHOLD("PERSONAL_LOAN"),
                    .NodeGroup = SmSettings.NodeGroup.PLSettings,
                    .NodeContent = SmUtil.BuildNodeAttributeXml("PERSONAL_LOAN", "previous_employment_threshold", IIf(Common.SafeBoolean(Request.Form("pl_previous_employment_enable")), Common.SafeInteger(Request.Form("pl_previous_employment_threshold")), ""))
                })
            End If
			Dim revisionID As Integer = Common.SafeInteger(Request.Form("revisionID"))
			Dim result = smBL.SaveNodesToDraft(revisionID, draftRecordList.ToArray())
			If result Then
				Dim previewUrl As String = smBL.GeneratePreviewConfig(lenderConfigID, UserInfo.Email, Function(config, code) String.Format("{0}/pl/personalloan.aspx?lenderref={1}&autofill=true&previewCode={2}", _serverRoot, config.LenderRef, code), draftRecordList.ToArray())
				res = New CJsonResponse(True, "", "OK")
				res.Info = New With {.previewurl = previewUrl}
			Else
				res = New CJsonResponse(False, "", "FAILED")
			End If
		Catch ex As Exception
			_log.Error("Could not PreviewPLSettingsDraft.", ex)
			res = New CJsonResponse(False, "Unable to save and preview draft. Please try again", "FAILED")
		End Try
		Response.Write(JsonConvert.SerializeObject(res))
		Response.End()
	End Sub
	Private Sub PublishPLSettings()
		Dim res As CJsonResponse
		Try
			Dim smBL As New SmBL()
			Dim draftRecordList As New List(Of SmLenderConfigDraft)
			Dim lenderConfigID As Guid = Common.SafeGUID(Request.Form("lenderconfigid"))
			Dim PLJointEnable As Boolean = Common.SafeBoolean(Request.Form("pl_joint_enable"))
			'Dim PLXSell As Boolean = Common.SafeBoolean(Request.Form("pl_xsell"))
			Dim declarations = JsonConvert.DeserializeObject(Of List(Of String))(Request.Form("pl_declarations"))
			Dim PLDocuSign As Boolean = Common.SafeBoolean(Request.Form("pl_docusign"))
			Dim PLDocuSignGroup As String = Common.SafeStripHtmlString(Request.Form("pl_docusign_group"))

			Dim loanInterestRateList As New Dictionary(Of String, Double)
			Dim lenderConfig As CLenderConfig = smBL.GetLiveConfig(lenderConfigID)
			If lenderConfig Is Nothing Then
				Response.StatusCode = 404 '' Forbidden
				Response.StatusDescription = "Not found"
				Response.End() '' Terminate all the rest processes
			End If
			Dim lenderConfigXml As New XmlDocument()
			lenderConfigXml.LoadXml(lenderConfig.ConfigData)
			Dim enablePL As Boolean = lenderConfigXml.SelectSingleNode("WEBSITE/PERSONAL_LOAN") IsNot Nothing
			If enablePL Then
				draftRecordList.Add(New SmLenderConfigDraft() With {
				.LenderConfigID = lenderConfigID,
				.CreatedByUserID = UserInfo.UserID,
				.NodePath = PL_JOINT_ENABLE(),
				.NodeGroup = SmSettings.NodeGroup.PLSettings,
				.NodeContent = SmUtil.BuildNodeAttributeXml("VISIBLE", "joint_enable_pl", IIf(PLJointEnable, "Y", "N").ToString())
				})
				'pl
				'draftRecordList.Add(New SmLenderConfigDraft() With {
				'	.LenderConfigID = lenderConfigID,
				'	.CreatedByUserID = UserInfo.UserID,
				'	.NodePath = Sm.XSELL("PERSONAL_LOAN"),
				'	.NodeGroup = SmSettings.NodeGroup.PLSettings,
				'.NodeContent = SmUtil.BuildNodeAttributeXml("VISIBLE", "auto_cross_qualify", IIf(PLXSell, "Y", "N").ToString())
				'})

				draftRecordList.Add(New SmLenderConfigDraft() With {
					.LenderConfigID = lenderConfigID,
					.CreatedByUserID = UserInfo.UserID,
					.NodePath = Sm.DOCU_SIGN("PERSONAL_LOAN"),
					.NodeGroup = SmSettings.NodeGroup.PLSettings,
				.NodeContent = SmUtil.BuildNodeAttributeXml("VISIBLE", "in_session_docu_sign", IIf(PLDocuSign, "Y", "N").ToString())
				})

				draftRecordList.Add(New SmLenderConfigDraft() With {
					.LenderConfigID = lenderConfigID,
					.CreatedByUserID = UserInfo.UserID,
					.NodePath = Sm.DOCU_SIGN_PDF_GROUP("PERSONAL_LOAN"),
					.NodeGroup = SmSettings.NodeGroup.PLSettings,
				.NodeContent = SmUtil.BuildNodeAttributeXml("VISIBLE", "in_session_docu_sign_pdf_group_override", PLDocuSignGroup)
				})
				loanInterestRateList.Add("pl", Common.SafeDouble(Request.Form("pl_interest_rate")))

				If loanInterestRateList.Any() Then
					draftRecordList.Add(New SmLenderConfigDraft() With {
						.LenderConfigID = lenderConfigID,
						.CreatedByUserID = UserInfo.UserID,
						.NodePath = Sm.CUSTOM_LIST_RATES("pl"),
						.NodeGroup = SmSettings.NodeGroup.PLSettings,
					.NodeContent = SmUtil.BuildCustomListRateXml(loanInterestRateList)
					})
				End If
                draftRecordList.Add(New SmLenderConfigDraft() With {
                    .LenderConfigID = lenderConfigID,
                    .CreatedByUserID = UserInfo.UserID,
                    .NodePath = Sm.LOAN_LOCATION_POOL("PERSONAL_LOAN"),
                    .NodeGroup = SmSettings.NodeGroup.PLSettings,
                    .NodeContent = SmUtil.BuildNodeAttributeXml("VISIBLE", "location_pool", IIf(Common.SafeBoolean(Request.Form("pl_location_pool")), "Y", "N").ToString())
                })
                draftRecordList.Add(New SmLenderConfigDraft() With {
                    .LenderConfigID = lenderConfigID,
                    .CreatedByUserID = UserInfo.UserID,
                    .NodePath = LOAN_DECLARATIONS("PERSONAL_LOAN"),
                    .NodeGroup = SmSettings.NodeGroup.PLSettings,
                    .NodeContent = SmUtil.BuildDeclarationsXml(declarations)
                })
                draftRecordList.Add(New SmLenderConfigDraft() With {
                 .LenderConfigID = lenderConfigID,
                 .CreatedByUserID = UserInfo.UserID,
                 .NodePath = Sm.PREVIOUS_ADDRESS_THRESHOLD("PERSONAL_LOAN"),
                 .NodeGroup = SmSettings.NodeGroup.PLSettings,
                 .NodeContent = SmUtil.BuildNodeAttributeXml("PERSONAL_LOAN", "previous_address_threshold", IIf(Common.SafeBoolean(Request.Form("pl_previous_address_enable")), Common.SafeInteger(Request.Form("pl_previous_address_threshold")), ""))
                 })
                draftRecordList.Add(New SmLenderConfigDraft() With {
                    .LenderConfigID = lenderConfigID,
                    .CreatedByUserID = UserInfo.UserID,
                    .NodePath = Sm.PREVIOUS_EMPLOYMENT_THRESHOLD("PERSONAL_LOAN"),
                    .NodeGroup = SmSettings.NodeGroup.PLSettings,
                    .NodeContent = SmUtil.BuildNodeAttributeXml("PERSONAL_LOAN", "previous_employment_threshold", IIf(Common.SafeBoolean(Request.Form("pl_previous_employment_enable")), Common.SafeInteger(Request.Form("pl_previous_employment_threshold")), ""))
                })
            End If
			Dim comment As String = Common.SafeStripHtmlString(Request.Form("comment"))
			Dim publishAll As Boolean = Common.SafeBoolean(Request.Form("publishAll"))
			Dim result = smBL.PublishNodes(lenderConfigID, UserInfo.UserID, Request.UserHostAddress, comment, publishAll, draftRecordList.ToArray())
			If result Then
				res = New CJsonResponse(True, "", "OK")
			Else
				res = New CJsonResponse(False, "", "FAILED")
			End If
		Catch ex As Exception
			_log.Error("Could not PublishPLSettingsDraft.", ex)
			res = New CJsonResponse(False, "Unable to publish data. Please try again", "FAILED")
		End Try
		Response.Write(JsonConvert.SerializeObject(res))
		Response.End()
	End Sub
#End Region
#Region "Vehicle Settings"
	Private Sub SaveVLSetttingsDraft()
		Dim res As CJsonResponse
		Try
			Dim smBL As New SmBL()
			Dim draftRecordList As New List(Of SmLenderConfigDraft)
			Dim lenderConfigID As Guid = Common.SafeGUID(Request.Form("lenderconfigid"))
			Dim loanInterestRateList As New Dictionary(Of String, Double)

			'Dim VLBooking As String = Common.SafeStripHtmlString(Request.Form("vl_booking"))
			'Dim VLXSell As Boolean = Common.SafeBoolean(Request.Form("vl_xsell"))
			Dim declarations = JsonConvert.DeserializeObject(Of List(Of String))(Request.Form("vl_declarations"))
			Dim lenderConfig As CLenderConfig = smBL.GetLiveConfig(lenderConfigID)
			If lenderConfig Is Nothing Then
				Response.StatusCode = 404 '' Forbidden
				Response.StatusDescription = "Not found"
				Response.End() '' Terminate all the rest processes
			End If
			Dim lenderConfigXml As New XmlDocument()
			lenderConfigXml.LoadXml(lenderConfig.ConfigData)
			Dim enableVL As Boolean = lenderConfigXml.SelectSingleNode("WEBSITE/VEHICLE_LOAN") IsNot Nothing
			If enableVL Then
				draftRecordList.Add(New SmLenderConfigDraft() With {
					.LenderConfigID = lenderConfigID,
					.CreatedByUserID = UserInfo.UserID,
					.NodePath = VL_JOINT_ENABLE(),
					.NodeGroup = SmSettings.NodeGroup.VLSettings,
					.NodeContent = SmUtil.BuildNodeAttributeXml("VISIBLE", "joint_enable_vl", IIf(Common.SafeBoolean(Request.Form("vl_joint_enable")), "Y", "N").ToString())
					})
				draftRecordList.Add(New SmLenderConfigDraft() With {
				.LenderConfigID = lenderConfigID,
				.CreatedByUserID = UserInfo.UserID,
				.NodePath = VL_TERM_FREE_FORM_MODE(),
				.NodeGroup = SmSettings.NodeGroup.VLSettings,
				.NodeContent = SmUtil.BuildNodeAttributeXml("VISIBLE", "term_free_form_mode_vl", IIf(Common.SafeBoolean(Request.Form("vl_term_free_form_mode")), "Y", "N").ToString())
				})
				draftRecordList.Add(New SmLenderConfigDraft() With {
				.LenderConfigID = lenderConfigID,
				.CreatedByUserID = UserInfo.UserID,
				.NodePath = VIN_BARCODE_SCAN("VEHICLE_LOAN"),
				.NodeGroup = SmSettings.NodeGroup.VLSettings,
				.NodeContent = SmUtil.BuildNodeAttributeXml("VISIBLE", "vin_barcode_scan", IIf(Common.SafeBoolean(Request.Form("vl_vin_barcode_scan_enable")), "Y", "N").ToString())
				})
				'vl
				'draftRecordList.Add(New SmLenderConfigDraft() With {
				'	.LenderConfigID = lenderConfigID,
				'	.CreatedByUserID = UserInfo.UserID,
				'	.NodePath = Sm.LOAN_BOOKING("VEHICLE_LOAN"),
				'	.NodeGroup = SmSettings.NodeGroup.VLSettings,
				'.NodeContent = SmUtil.BuildNodeAttributeXml("WEBSITE", "booking", VLBooking)
				'})
				'draftRecordList.Add(New SmLenderConfigDraft() With {
				'	.LenderConfigID = lenderConfigID,
				'	.CreatedByUserID = UserInfo.UserID,
				'	.NodePath = Sm.XSELL("VEHICLE_LOAN"),
				'	.NodeGroup = SmSettings.NodeGroup.VLSettings,
				'.NodeContent = SmUtil.BuildNodeAttributeXml("VISIBLE", "auto_cross_qualify", IIf(VLXSell, "Y", "N").ToString())
				'})

				draftRecordList.Add(New SmLenderConfigDraft() With {
					.LenderConfigID = lenderConfigID,
					.CreatedByUserID = UserInfo.UserID,
					.NodePath = Sm.DOCU_SIGN("VEHICLE_LOAN"),
					.NodeGroup = SmSettings.NodeGroup.VLSettings,
				.NodeContent = SmUtil.BuildNodeAttributeXml("VISIBLE", "in_session_docu_sign", IIf(Common.SafeBoolean(Request.Form("vl_docusign")), "Y", "N").ToString())
				})

				draftRecordList.Add(New SmLenderConfigDraft() With {
					.LenderConfigID = lenderConfigID,
					.CreatedByUserID = UserInfo.UserID,
					.NodePath = Sm.DOCU_SIGN_PDF_GROUP("VEHICLE_LOAN"),
					.NodeGroup = SmSettings.NodeGroup.VLSettings,
				.NodeContent = SmUtil.BuildNodeAttributeXml("VISIBLE", "in_session_docu_sign_pdf_group_override", Common.SafeStripHtmlString(Request.Form("vl_docusign_group")))
				})
				loanInterestRateList.Add("vl", Common.SafeDouble(Request.Form("vl_interest_rate")))

				If loanInterestRateList.Any() Then
					draftRecordList.Add(New SmLenderConfigDraft() With {
						.LenderConfigID = lenderConfigID,
						.CreatedByUserID = UserInfo.UserID,
						.NodePath = Sm.CUSTOM_LIST_RATES("vl"),
						.NodeGroup = SmSettings.NodeGroup.VLSettings,
					.NodeContent = SmUtil.BuildCustomListRateXml(loanInterestRateList)
					})
				End If
				draftRecordList.Add(New SmLenderConfigDraft() With {
				.LenderConfigID = lenderConfigID,
				.CreatedByUserID = UserInfo.UserID,
				.NodePath = Sm.LOAN_LOCATION_POOL("VEHICLE_LOAN"),
				.NodeGroup = SmSettings.NodeGroup.VLSettings,
				.NodeContent = SmUtil.BuildNodeAttributeXml("VISIBLE", "location_pool", IIf(Common.SafeBoolean(Request.Form("vl_location_pool")), "Y", "N").ToString())
				})

				draftRecordList.Add(New SmLenderConfigDraft() With {
					.LenderConfigID = lenderConfigID,
					.CreatedByUserID = UserInfo.UserID,
					.NodePath = LOAN_DECLARATIONS("VEHICLE_LOAN"),
					.NodeGroup = SmSettings.NodeGroup.VLSettings,
					.NodeContent = SmUtil.BuildDeclarationsXml(declarations)
				})
                draftRecordList.Add(New SmLenderConfigDraft() With {
                 .LenderConfigID = lenderConfigID,
                 .CreatedByUserID = UserInfo.UserID,
                 .NodePath = Sm.PREVIOUS_ADDRESS_THRESHOLD("VEHICLE_LOAN"),
                 .NodeGroup = SmSettings.NodeGroup.VLSettings,
                 .NodeContent = SmUtil.BuildNodeAttributeXml("VEHICLE_LOAN", "previous_address_threshold", IIf(Common.SafeBoolean(Request.Form("vl_previous_address_enable")), Common.SafeInteger(Request.Form("vl_previous_address_threshold")), ""))
                 })
                draftRecordList.Add(New SmLenderConfigDraft() With {
                    .LenderConfigID = lenderConfigID,
                    .CreatedByUserID = UserInfo.UserID,
                    .NodePath = Sm.PREVIOUS_EMPLOYMENT_THRESHOLD("VEHICLE_LOAN"),
                    .NodeGroup = SmSettings.NodeGroup.VLSettings,
                    .NodeContent = SmUtil.BuildNodeAttributeXml("VEHICLE_LOAN", "previous_employment_threshold", IIf(Common.SafeBoolean(Request.Form("vl_previous_employment_enable")), Common.SafeInteger(Request.Form("vl_previous_employment_threshold")), ""))
                })
            End If

			Dim revisionID As Integer = Common.SafeInteger(Request.Form("revisionID"))
			Dim result = smBL.SaveNodesToDraft(revisionID, draftRecordList.ToArray())
			If result Then
				res = New CJsonResponse(True, "", "OK")
			Else
				res = New CJsonResponse(False, "", "FAILED")
			End If
		Catch ex As Exception
			_log.Error("Could not SaveVLSettingsDraft.", ex)
			res = New CJsonResponse(False, "Unable to save draft. Please try again", "FAILED")
		End Try
		Response.Write(JsonConvert.SerializeObject(res))
		Response.End()
	End Sub
	Private Sub PreviewVLSettingsDraft()
		Dim res As CJsonResponse
		Try
			Dim smBL As New SmBL()
			Dim draftRecordList As New List(Of SmLenderConfigDraft)
			Dim lenderConfigID As Guid = Common.SafeGUID(Request.Form("lenderconfigid"))
			Dim loanInterestRateList As New Dictionary(Of String, Double)

			'Dim VLBooking As String = Common.SafeStripHtmlString(Request.Form("vl_booking"))
			'Dim VLXSell As Boolean = Common.SafeBoolean(Request.Form("vl_xsell"))
			Dim declarations = JsonConvert.DeserializeObject(Of List(Of String))(Request.Form("vl_declarations"))


			Dim lenderConfig As CLenderConfig = smBL.GetLiveConfig(lenderConfigID)
			If lenderConfig Is Nothing Then
				Response.StatusCode = 404 '' Forbidden
				Response.StatusDescription = "Not found"
				Response.End() '' Terminate all the rest processes
			End If
			Dim lenderConfigXml As New XmlDocument()
			lenderConfigXml.LoadXml(lenderConfig.ConfigData)
			Dim enableVL As Boolean = lenderConfigXml.SelectSingleNode("WEBSITE/VEHICLE_LOAN") IsNot Nothing

			If enableVL Then
				draftRecordList.Add(New SmLenderConfigDraft() With {
					.LenderConfigID = lenderConfigID,
					.CreatedByUserID = UserInfo.UserID,
					.NodePath = VL_JOINT_ENABLE(),
					.NodeGroup = SmSettings.NodeGroup.VLSettings,
				.NodeContent = SmUtil.BuildNodeAttributeXml("VISIBLE", "joint_enable_vl", IIf(Common.SafeBoolean(Request.Form("vl_joint_enable")), "Y", "N").ToString())
				})
				draftRecordList.Add(New SmLenderConfigDraft() With {
				 .LenderConfigID = lenderConfigID,
				 .CreatedByUserID = UserInfo.UserID,
				 .NodePath = VL_TERM_FREE_FORM_MODE(),
				 .NodeGroup = SmSettings.NodeGroup.VLSettings,
				 .NodeContent = SmUtil.BuildNodeAttributeXml("VISIBLE", "term_free_form_mode_vl", IIf(Common.SafeBoolean(Request.Form("vl_term_free_form_mode")), "Y", "N").ToString())
				 })
				draftRecordList.Add(New SmLenderConfigDraft() With {
					.LenderConfigID = lenderConfigID,
					.CreatedByUserID = UserInfo.UserID,
					.NodePath = VIN_BARCODE_SCAN("VEHICLE_LOAN"),
					.NodeGroup = SmSettings.NodeGroup.VLSettings,
					.NodeContent = SmUtil.BuildNodeAttributeXml("VISIBLE", "vin_barcode_scan", IIf(Common.SafeBoolean(Request.Form("vl_vin_barcode_scan_enable")), "Y", "N").ToString())
				})
				'vl
				'draftRecordList.Add(New SmLenderConfigDraft() With {
				'	.LenderConfigID = lenderConfigID,
				'	.CreatedByUserID = UserInfo.UserID,
				'	.NodePath = Sm.LOAN_BOOKING("VEHICLE_LOAN"),
				'	.NodeGroup = SmSettings.NodeGroup.VLSettings,
				'.NodeContent = SmUtil.BuildNodeAttributeXml("WEBSITE", "booking", VLBooking)
				'})
				'draftRecordList.Add(New SmLenderConfigDraft() With {
				'	.LenderConfigID = lenderConfigID,
				'	.CreatedByUserID = UserInfo.UserID,
				'	.NodePath = Sm.XSELL("VEHICLE_LOAN"),
				'	.NodeGroup = SmSettings.NodeGroup.VLSettings,
				'.NodeContent = SmUtil.BuildNodeAttributeXml("VISIBLE", "auto_cross_qualify", IIf(VLXSell, "Y", "N").ToString())
				'})

				draftRecordList.Add(New SmLenderConfigDraft() With {
					.LenderConfigID = lenderConfigID,
					.CreatedByUserID = UserInfo.UserID,
					.NodePath = Sm.DOCU_SIGN("VEHICLE_LOAN"),
					.NodeGroup = SmSettings.NodeGroup.VLSettings,
				.NodeContent = SmUtil.BuildNodeAttributeXml("VISIBLE", "in_session_docu_sign", IIf(Common.SafeBoolean(Request.Form("vl_docusign")), "Y", "N").ToString())
				})

				draftRecordList.Add(New SmLenderConfigDraft() With {
					.LenderConfigID = lenderConfigID,
					.CreatedByUserID = UserInfo.UserID,
					.NodePath = Sm.DOCU_SIGN_PDF_GROUP("VEHICLE_LOAN"),
					.NodeGroup = SmSettings.NodeGroup.VLSettings,
				.NodeContent = SmUtil.BuildNodeAttributeXml("VISIBLE", "in_session_docu_sign_pdf_group_override", Common.SafeStripHtmlString(Request.Form("vl_docusign_group")))
				})
				loanInterestRateList.Add("vl", Common.SafeDouble(Request.Form("vl_interest_rate")))

				If loanInterestRateList.Any() Then
					draftRecordList.Add(New SmLenderConfigDraft() With {
						.LenderConfigID = lenderConfigID,
						.CreatedByUserID = UserInfo.UserID,
						.NodePath = Sm.CUSTOM_LIST_RATES("vl"),
						.NodeGroup = SmSettings.NodeGroup.VLSettings,
					.NodeContent = SmUtil.BuildCustomListRateXml(loanInterestRateList)
					})
				End If
				draftRecordList.Add(New SmLenderConfigDraft() With {
				   .LenderConfigID = lenderConfigID,
				   .CreatedByUserID = UserInfo.UserID,
				   .NodePath = Sm.LOAN_LOCATION_POOL("VEHICLE_LOAN"),
				   .NodeGroup = SmSettings.NodeGroup.VLSettings,
				   .NodeContent = SmUtil.BuildNodeAttributeXml("VISIBLE", "location_pool", IIf(Common.SafeBoolean(Request.Form("vl_location_pool")), "Y", "N").ToString())
				})

				draftRecordList.Add(New SmLenderConfigDraft() With {
					.LenderConfigID = lenderConfigID,
					.CreatedByUserID = UserInfo.UserID,
					.NodePath = LOAN_DECLARATIONS("VEHICLE_LOAN"),
					.NodeGroup = SmSettings.NodeGroup.VLSettings,
					.NodeContent = SmUtil.BuildDeclarationsXml(declarations)
				})
                draftRecordList.Add(New SmLenderConfigDraft() With {
                 .LenderConfigID = lenderConfigID,
                 .CreatedByUserID = UserInfo.UserID,
                 .NodePath = Sm.PREVIOUS_ADDRESS_THRESHOLD("VEHICLE_LOAN"),
                 .NodeGroup = SmSettings.NodeGroup.VLSettings,
                 .NodeContent = SmUtil.BuildNodeAttributeXml("VEHICLE_LOAN", "previous_address_threshold", IIf(Common.SafeBoolean(Request.Form("vl_previous_address_enable")), Common.SafeInteger(Request.Form("vl_previous_address_threshold")), ""))
                 })
                draftRecordList.Add(New SmLenderConfigDraft() With {
                    .LenderConfigID = lenderConfigID,
                    .CreatedByUserID = UserInfo.UserID,
                    .NodePath = Sm.PREVIOUS_EMPLOYMENT_THRESHOLD("VEHICLE_LOAN"),
                    .NodeGroup = SmSettings.NodeGroup.VLSettings,
                    .NodeContent = SmUtil.BuildNodeAttributeXml("VEHICLE_LOAN", "previous_employment_threshold", IIf(Common.SafeBoolean(Request.Form("vl_previous_employment_enable")), Common.SafeInteger(Request.Form("vl_previous_employment_threshold")), ""))
                })
            End If

			Dim revisionID As Integer = Common.SafeInteger(Request.Form("revisionID"))
			Dim result = smBL.SaveNodesToDraft(revisionID, draftRecordList.ToArray())
			If result Then
				Dim previewUrl As String = smBL.GeneratePreviewConfig(lenderConfigID, UserInfo.Email, Function(config, code) String.Format("{0}/vl/vehicleloan.aspx?lenderref={1}&autofill=true&previewCode={2}", _serverRoot, config.LenderRef, code), draftRecordList.ToArray())
				res = New CJsonResponse(True, "", "OK")
				res.Info = New With {.previewurl = previewUrl}
			Else
				res = New CJsonResponse(False, "", "FAILED")
			End If
		Catch ex As Exception
			_log.Error("Could not PreviewVLSettingsDraft.", ex)
			res = New CJsonResponse(False, "Unable to save and preview draft. Please try again", "FAILED")
		End Try
		Response.Write(JsonConvert.SerializeObject(res))
		Response.End()
	End Sub
	Private Sub PublishVLSettings()
		Dim res As CJsonResponse
		Try
			Dim smBL As New SmBL()
			Dim draftRecordList As New List(Of SmLenderConfigDraft)
			Dim lenderConfigID As Guid = Common.SafeGUID(Request.Form("lenderconfigid"))
			Dim loanInterestRateList As New Dictionary(Of String, Double)
			'Dim VLBooking As String = Common.SafeStripHtmlString(Request.Form("vl_booking"))
			'Dim VLXSell As Boolean = Common.SafeBoolean(Request.Form("vl_xsell"))
			Dim declarations = JsonConvert.DeserializeObject(Of List(Of String))(Request.Form("vl_declarations"))
			Dim lenderConfig As CLenderConfig = smBL.GetLiveConfig(lenderConfigID)
			If lenderConfig Is Nothing Then
				Response.StatusCode = 404 '' Forbidden
				Response.StatusDescription = "Not found"
				Response.End() '' Terminate all the rest processes
			End If
			Dim lenderConfigXml As New XmlDocument()
			lenderConfigXml.LoadXml(lenderConfig.ConfigData)
			Dim enableVL As Boolean = lenderConfigXml.SelectSingleNode("WEBSITE/VEHICLE_LOAN") IsNot Nothing



			If enableVL Then

				draftRecordList.Add(New SmLenderConfigDraft() With {
					.LenderConfigID = lenderConfigID,
					.CreatedByUserID = UserInfo.UserID,
					.NodePath = VL_JOINT_ENABLE(),
					.NodeGroup = SmSettings.NodeGroup.VLSettings,
				.NodeContent = SmUtil.BuildNodeAttributeXml("VISIBLE", "joint_enable_vl", IIf(Common.SafeBoolean(Request.Form("vl_joint_enable")), "Y", "N").ToString())
				})
				draftRecordList.Add(New SmLenderConfigDraft() With {
				.LenderConfigID = lenderConfigID,
				.CreatedByUserID = UserInfo.UserID,
				.NodePath = VL_TERM_FREE_FORM_MODE(),
				.NodeGroup = SmSettings.NodeGroup.VLSettings,
				.NodeContent = SmUtil.BuildNodeAttributeXml("VISIBLE", "term_free_form_mode_vl", IIf(Common.SafeBoolean(Request.Form("vl_term_free_form_mode")), "Y", "N").ToString())
				})
				draftRecordList.Add(New SmLenderConfigDraft() With {
					.LenderConfigID = lenderConfigID,
					.CreatedByUserID = UserInfo.UserID,
					.NodePath = VIN_BARCODE_SCAN("VEHICLE_LOAN"),
					.NodeGroup = SmSettings.NodeGroup.VLSettings,
					.NodeContent = SmUtil.BuildNodeAttributeXml("VISIBLE", "vin_barcode_scan", IIf(Common.SafeBoolean(Request.Form("vl_vin_barcode_scan_enable")), "Y", "N").ToString())
				})

				'vl
				'draftRecordList.Add(New SmLenderConfigDraft() With {
				'	.LenderConfigID = lenderConfigID,
				'	.CreatedByUserID = UserInfo.UserID,
				'	.NodePath = Sm.LOAN_BOOKING("VEHICLE_LOAN"),
				'	.NodeGroup = SmSettings.NodeGroup.VLSettings,
				'.NodeContent = SmUtil.BuildNodeAttributeXml("WEBSITE", "booking", VLBooking)
				'})
				'draftRecordList.Add(New SmLenderConfigDraft() With {
				'	.LenderConfigID = lenderConfigID,
				'	.CreatedByUserID = UserInfo.UserID,
				'	.NodePath = Sm.XSELL("VEHICLE_LOAN"),
				'	.NodeGroup = SmSettings.NodeGroup.VLSettings,
				'.NodeContent = SmUtil.BuildNodeAttributeXml("VISIBLE", "auto_cross_qualify", IIf(VLXSell, "Y", "N").ToString())
				'})

				draftRecordList.Add(New SmLenderConfigDraft() With {
					.LenderConfigID = lenderConfigID,
					.CreatedByUserID = UserInfo.UserID,
					.NodePath = Sm.DOCU_SIGN("VEHICLE_LOAN"),
					.NodeGroup = SmSettings.NodeGroup.VLSettings,
				.NodeContent = SmUtil.BuildNodeAttributeXml("VISIBLE", "in_session_docu_sign", IIf(Common.SafeBoolean(Request.Form("vl_docusign")), "Y", "N").ToString())
				})

				draftRecordList.Add(New SmLenderConfigDraft() With {
					.LenderConfigID = lenderConfigID,
					.CreatedByUserID = UserInfo.UserID,
					.NodePath = Sm.DOCU_SIGN_PDF_GROUP("VEHICLE_LOAN"),
					.NodeGroup = SmSettings.NodeGroup.VLSettings,
				.NodeContent = SmUtil.BuildNodeAttributeXml("VISIBLE", "in_session_docu_sign_pdf_group_override", Common.SafeStripHtmlString(Request.Form("vl_docusign_group")))
				})
				loanInterestRateList.Add("vl", Common.SafeDouble(Request.Form("vl_interest_rate")))
				If loanInterestRateList.Any() Then
					draftRecordList.Add(New SmLenderConfigDraft() With {
						.LenderConfigID = lenderConfigID,
						.CreatedByUserID = UserInfo.UserID,
						.NodePath = Sm.CUSTOM_LIST_RATES("vl"),
						.NodeGroup = SmSettings.NodeGroup.VLSettings,
					.NodeContent = SmUtil.BuildCustomListRateXml(loanInterestRateList)
					})
				End If
				draftRecordList.Add(New SmLenderConfigDraft() With {
				   .LenderConfigID = lenderConfigID,
				   .CreatedByUserID = UserInfo.UserID,
				   .NodePath = Sm.LOAN_LOCATION_POOL("VEHICLE_LOAN"),
				   .NodeGroup = SmSettings.NodeGroup.VLSettings,
				   .NodeContent = SmUtil.BuildNodeAttributeXml("VISIBLE", "location_pool", IIf(Common.SafeBoolean(Request.Form("vl_location_pool")), "Y", "N").ToString())
				})

				draftRecordList.Add(New SmLenderConfigDraft() With {
					.LenderConfigID = lenderConfigID,
					.CreatedByUserID = UserInfo.UserID,
					.NodePath = LOAN_DECLARATIONS("VEHICLE_LOAN"),
					.NodeGroup = SmSettings.NodeGroup.VLSettings,
					.NodeContent = SmUtil.BuildDeclarationsXml(declarations)
				})
                draftRecordList.Add(New SmLenderConfigDraft() With {
                 .LenderConfigID = lenderConfigID,
                 .CreatedByUserID = UserInfo.UserID,
                 .NodePath = Sm.PREVIOUS_ADDRESS_THRESHOLD("VEHICLE_LOAN"),
                 .NodeGroup = SmSettings.NodeGroup.VLSettings,
                 .NodeContent = SmUtil.BuildNodeAttributeXml("VEHICLE_LOAN", "previous_address_threshold", IIf(Common.SafeBoolean(Request.Form("vl_previous_address_enable")), Common.SafeInteger(Request.Form("vl_previous_address_threshold")), ""))
                 })
                draftRecordList.Add(New SmLenderConfigDraft() With {
                    .LenderConfigID = lenderConfigID,
                    .CreatedByUserID = UserInfo.UserID,
                    .NodePath = Sm.PREVIOUS_EMPLOYMENT_THRESHOLD("VEHICLE_LOAN"),
                    .NodeGroup = SmSettings.NodeGroup.VLSettings,
                    .NodeContent = SmUtil.BuildNodeAttributeXml("VEHICLE_LOAN", "previous_employment_threshold", IIf(Common.SafeBoolean(Request.Form("vl_previous_employment_enable")), Common.SafeInteger(Request.Form("vl_previous_employment_threshold")), ""))
                })
            End If

			Dim comment As String = Common.SafeStripHtmlString(Request.Form("comment"))
			Dim publishAll As Boolean = Common.SafeBoolean(Request.Form("publishAll"))
			Dim result = smBL.PublishNodes(lenderConfigID, UserInfo.UserID, Request.UserHostAddress, comment, publishAll, draftRecordList.ToArray())
			If result Then
				res = New CJsonResponse(True, "", "OK")
			Else
				res = New CJsonResponse(False, "", "FAILED")
			End If
		Catch ex As Exception
			_log.Error("Could not PublishVLSettings.", ex)
			res = New CJsonResponse(False, "Unable to publish data. Please try again", "FAILED")
		End Try
		Response.Write(JsonConvert.SerializeObject(res))
		Response.End()
	End Sub
#End Region
#Region "LQB Settings"
	Private Sub SaveLQBSetttingsDraft()
		Dim res As CJsonResponse
		Try
			Dim smBL As New SmBL()
			Dim draftRecordList As New List(Of SmLenderConfigDraft)
			Dim lenderConfigID As Guid = Common.SafeGUID(Request.Form("lenderconfigid"))
			Dim defaultLoanTemplate = Common.SafeString(Request.Form("defaultLoanTemplate"))
			Dim defaultLoanOfficer = Common.SafeString(Request.Form("defaultLoanOfficer"))
			Dim creditReportingAgency = Common.SafeGUID(Request.Form("creditReportingAgency"))
			Dim allowComsumerToCreateApps = Common.SafeBoolean(Request.Form("allowComsumerToCreateApps"))
			Dim showPricerAtEnd = Common.SafeBoolean(Request.Form("showPricerAtEnd"))
			Dim lqbUser = Common.SafeString(Request.Form("lqbUser"))
			Dim lqbPassword = Common.SafeString(Request.Form("lqbPassword"))
			Dim piwikSiteId = Common.SafeString(Request.Form("piwikSiteId"))
			Dim googleTagManagerId = Common.SafeString(Request.Form("googleTagManagerId"))
			Dim instaTouchEnabled = Common.SafeBoolean(Request.Form("instaTouchEnabled"))
			Dim voaPrefillEnabled = Common.SafeBoolean(Request.Form("voaPrefillEnabled"))
			Dim creditPull = Common.SafeBoolean(Request.Form("creditPull"))
			Dim saveFullAppAs = Common.SafeString(Request.Form("saveFullAppAs"))

			Dim lenderConfig As CLenderConfig = smBL.GetLiveConfig(lenderConfigID)
			If lenderConfig Is Nothing Then
				Response.StatusCode = 404 '' Forbidden
				Response.StatusDescription = "Not found"
				Response.End() '' Terminate all the rest processes
			End If
			draftRecordList.Add(New SmLenderConfigDraft() With {
				.LenderConfigID = lenderConfigID,
				.CreatedByUserID = UserInfo.UserID,
				.NodePath = LQB_APPLICATION_DEFAULT_LOAN_TEMPLATE(),
				.NodeGroup = SmSettings.NodeGroup.LQBSettings,
			.NodeContent = SmUtil.BuildNodeAttributeXml("APPLICATION", "default_loan_template", defaultLoanTemplate)
			})
			draftRecordList.Add(New SmLenderConfigDraft() With {
				.LenderConfigID = lenderConfigID,
				.CreatedByUserID = UserInfo.UserID,
				.NodePath = LQB_APPLICATION_LOAN_OFFICER(),
				.NodeGroup = SmSettings.NodeGroup.LQBSettings,
			.NodeContent = SmUtil.BuildNodeAttributeXml("APPLICATION", "loan_officer", defaultLoanOfficer)
			})
			draftRecordList.Add(New SmLenderConfigDraft() With {
				.LenderConfigID = lenderConfigID,
				.CreatedByUserID = UserInfo.UserID,
				.NodePath = LQB_CREDIT_REPORTING_AGENCY(),
				.NodeGroup = SmSettings.NodeGroup.LQBSettings,
			.NodeContent = SmUtil.BuildNodeAttributeXml("LQB", "credit_reporting_agency", creditReportingAgency.ToString())
			})
			draftRecordList.Add(New SmLenderConfigDraft() With {
				.LenderConfigID = lenderConfigID,
				.CreatedByUserID = UserInfo.UserID,
				.NodePath = LQB_BEHAVIOR_ALLOW_CONSUMER_TO_CREATE_APPS(),
				.NodeGroup = SmSettings.NodeGroup.LQBSettings,
			.NodeContent = SmUtil.BuildNodeAttributeXml("BEHAVIOR", "allow_consumer_to_create_apps", IIf(allowComsumerToCreateApps, "Y", "N").ToString())
			})
			draftRecordList.Add(New SmLenderConfigDraft() With {
				.LenderConfigID = lenderConfigID,
				.CreatedByUserID = UserInfo.UserID,
				.NodePath = LQB_BEHAVIOR_SHOW_PRICER_AT_END(),
				.NodeGroup = SmSettings.NodeGroup.LQBSettings,
			.NodeContent = SmUtil.BuildNodeAttributeXml("BEHAVIOR", "show_pricer_at_end", IIf(showPricerAtEnd, "Y", "N").ToString())
			})
			draftRecordList.Add(New SmLenderConfigDraft() With {
				.LenderConfigID = lenderConfigID,
				.CreatedByUserID = UserInfo.UserID,
				.NodePath = LQB_USER(),
				.NodeGroup = SmSettings.NodeGroup.LQBSettings,
			.NodeContent = SmUtil.BuildNodeAttributeXml("LQB", "user", lqbUser)
			})
			draftRecordList.Add(New SmLenderConfigDraft() With {
				.LenderConfigID = lenderConfigID,
				.CreatedByUserID = UserInfo.UserID,
				.NodePath = LQB_PASSWORD(),
				.NodeGroup = SmSettings.NodeGroup.LQBSettings,
			.NodeContent = SmUtil.BuildNodeAttributeXml("LQB", "password", lqbPassword)
			})
			draftRecordList.Add(New SmLenderConfigDraft() With {
				.LenderConfigID = lenderConfigID,
				.CreatedByUserID = UserInfo.UserID,
				.NodePath = LQB_PIWIK_SITE_ID(),
				.NodeGroup = SmSettings.NodeGroup.LQBSettings,
			.NodeContent = SmUtil.BuildNodeAttributeXml("LQB", "piwik_site_id", piwikSiteId)
			})
			draftRecordList.Add(New SmLenderConfigDraft() With {
				.LenderConfigID = lenderConfigID,
				.CreatedByUserID = UserInfo.UserID,
				.NodePath = LQB_GOOGLE_TAG_MANAGER_ID(),
				.NodeGroup = SmSettings.NodeGroup.LQBSettings,
			.NodeContent = SmUtil.BuildNodeAttributeXml("LQB", "google_tag_manager_id", googleTagManagerId)
			})
			If SmUtil.GetApmShowInstaTouchSwitchButton(lenderConfig.ConfigData) Then
				draftRecordList.Add(New SmLenderConfigDraft() With {
					.LenderConfigID = lenderConfigID,
					.CreatedByUserID = UserInfo.UserID,
					.NodePath = LQB_INSTATOUCH_ENABLED(),
					.NodeGroup = SmSettings.NodeGroup.LQBSettings,
				.NodeContent = SmUtil.BuildNodeAttributeXml("LQB", "instatouch_enabled", IIf(instaTouchEnabled, "Y", "N").ToString())
				})
			End If
			draftRecordList.Add(New SmLenderConfigDraft() With {
				.LenderConfigID = lenderConfigID,
				.CreatedByUserID = UserInfo.UserID,
				.NodePath = LQB_VOA_PREFILL_ENABLED(),
				.NodeGroup = SmSettings.NodeGroup.LQBSettings,
			.NodeContent = SmUtil.BuildNodeAttributeXml("LQB", "voa_prefill_enabled", IIf(voaPrefillEnabled, "Y", "N").ToString())
			})
			draftRecordList.Add(New SmLenderConfigDraft() With {
				.LenderConfigID = lenderConfigID,
				.CreatedByUserID = UserInfo.UserID,
				.NodePath = LQB_BEHAVIOR_CREDIT_PULL(),
				.NodeGroup = SmSettings.NodeGroup.LQBSettings,
				.NodeContent = SmUtil.BuildNodeAttributeXml("LQB", "credit_pull", IIf(creditPull, "Y", "N").ToString())
			})
			draftRecordList.Add(New SmLenderConfigDraft() With {
				.LenderConfigID = lenderConfigID,
				.CreatedByUserID = UserInfo.UserID,
				.NodePath = LQB_APPLICATION_SAVE_FULL_APP_AS(),
				.NodeGroup = SmSettings.NodeGroup.LQBSettings,
			.NodeContent = SmUtil.BuildNodeAttributeXml("APPLICATION", "save_full_app_as", saveFullAppAs)
			})

			Dim revisionID As Integer = Common.SafeInteger(Request.Form("revisionID"))
			Dim result = smBL.SaveNodesToDraft(revisionID, draftRecordList.ToArray())
			If result Then
				res = New CJsonResponse(True, "", "OK")
			Else
				res = New CJsonResponse(False, "", "FAILED")
			End If
		Catch ex As Exception
			_log.Error("Could not SaveLQBSetttingsDraft.", ex)
			res = New CJsonResponse(False, "Unable to save draft. Please try again", "FAILED")
		End Try
		Response.Write(JsonConvert.SerializeObject(res))
		Response.End()
	End Sub
	Private Sub PreviewLQBSettingsDraft()
		Dim res As CJsonResponse
		Try
			Dim smBL As New SmBL()
			Dim draftRecordList As New List(Of SmLenderConfigDraft)
			Dim lenderConfigID As Guid = Common.SafeGUID(Request.Form("lenderconfigid"))
			Dim defaultLoanTemplate = Common.SafeString(Request.Form("defaultLoanTemplate"))
			Dim defaultLoanOfficer = Common.SafeString(Request.Form("defaultLoanOfficer"))
			Dim creditReportingAgency = Common.SafeGUID(Request.Form("creditReportingAgency"))
			Dim allowComsumerToCreateApps = Common.SafeBoolean(Request.Form("allowComsumerToCreateApps"))
			Dim showPricerAtEnd = Common.SafeBoolean(Request.Form("showPricerAtEnd"))
			Dim lqbUser = Common.SafeString(Request.Form("lqbUser"))
			Dim lqbPassword = Common.SafeString(Request.Form("lqbPassword"))
			Dim piwikSiteId = Common.SafeString(Request.Form("piwikSiteId"))
			Dim googleTagManagerId = Common.SafeString(Request.Form("googleTagManagerId"))
			Dim instaTouchEnabled = Common.SafeBoolean(Request.Form("instaTouchEnabled"))
			Dim voaPrefillEnabled = Common.SafeBoolean(Request.Form("voaPrefillEnabled"))
			Dim creditPull = Common.SafeBoolean(Request.Form("creditPull"))
			Dim saveFullAppAs = Common.SafeString(Request.Form("saveFullAppAs"))

			Dim lenderConfig As CLenderConfig = smBL.GetLiveConfig(lenderConfigID)
			If lenderConfig Is Nothing Then
				Response.StatusCode = 404 '' Forbidden
				Response.StatusDescription = "Not found"
				Response.End() '' Terminate all the rest processes
			End If
			draftRecordList.Add(New SmLenderConfigDraft() With {
				.LenderConfigID = lenderConfigID,
				.CreatedByUserID = UserInfo.UserID,
				.NodePath = LQB_APPLICATION_DEFAULT_LOAN_TEMPLATE(),
				.NodeGroup = SmSettings.NodeGroup.LQBSettings,
			.NodeContent = SmUtil.BuildNodeAttributeXml("APPLICATION", "default_loan_template", defaultLoanTemplate)
			})
			draftRecordList.Add(New SmLenderConfigDraft() With {
				.LenderConfigID = lenderConfigID,
				.CreatedByUserID = UserInfo.UserID,
				.NodePath = LQB_APPLICATION_LOAN_OFFICER(),
				.NodeGroup = SmSettings.NodeGroup.LQBSettings,
			.NodeContent = SmUtil.BuildNodeAttributeXml("APPLICATION", "loan_officer", defaultLoanOfficer)
			})
			draftRecordList.Add(New SmLenderConfigDraft() With {
				.LenderConfigID = lenderConfigID,
				.CreatedByUserID = UserInfo.UserID,
				.NodePath = LQB_CREDIT_REPORTING_AGENCY(),
				.NodeGroup = SmSettings.NodeGroup.LQBSettings,
			.NodeContent = SmUtil.BuildNodeAttributeXml("LQB", "credit_reporting_agency", creditReportingAgency.ToString())
			})
			draftRecordList.Add(New SmLenderConfigDraft() With {
				.LenderConfigID = lenderConfigID,
				.CreatedByUserID = UserInfo.UserID,
				.NodePath = LQB_BEHAVIOR_ALLOW_CONSUMER_TO_CREATE_APPS(),
				.NodeGroup = SmSettings.NodeGroup.LQBSettings,
			.NodeContent = SmUtil.BuildNodeAttributeXml("BEHAVIOR", "allow_consumer_to_create_apps", IIf(allowComsumerToCreateApps, "Y", "N").ToString())
			})
			draftRecordList.Add(New SmLenderConfigDraft() With {
				.LenderConfigID = lenderConfigID,
				.CreatedByUserID = UserInfo.UserID,
				.NodePath = LQB_BEHAVIOR_SHOW_PRICER_AT_END(),
				.NodeGroup = SmSettings.NodeGroup.LQBSettings,
			.NodeContent = SmUtil.BuildNodeAttributeXml("BEHAVIOR", "show_pricer_at_end", IIf(showPricerAtEnd, "Y", "N").ToString())
			})
			draftRecordList.Add(New SmLenderConfigDraft() With {
				.LenderConfigID = lenderConfigID,
				.CreatedByUserID = UserInfo.UserID,
				.NodePath = LQB_USER(),
				.NodeGroup = SmSettings.NodeGroup.LQBSettings,
			.NodeContent = SmUtil.BuildNodeAttributeXml("LQB", "user", lqbUser)
			})
			draftRecordList.Add(New SmLenderConfigDraft() With {
				.LenderConfigID = lenderConfigID,
				.CreatedByUserID = UserInfo.UserID,
				.NodePath = LQB_PASSWORD(),
				.NodeGroup = SmSettings.NodeGroup.LQBSettings,
			.NodeContent = SmUtil.BuildNodeAttributeXml("LQB", "password", lqbPassword)
			})
			draftRecordList.Add(New SmLenderConfigDraft() With {
				.LenderConfigID = lenderConfigID,
				.CreatedByUserID = UserInfo.UserID,
				.NodePath = LQB_PIWIK_SITE_ID(),
				.NodeGroup = SmSettings.NodeGroup.LQBSettings,
			.NodeContent = SmUtil.BuildNodeAttributeXml("LQB", "piwik_site_id", piwikSiteId)
			})
			draftRecordList.Add(New SmLenderConfigDraft() With {
				.LenderConfigID = lenderConfigID,
				.CreatedByUserID = UserInfo.UserID,
				.NodePath = LQB_GOOGLE_TAG_MANAGER_ID(),
				.NodeGroup = SmSettings.NodeGroup.LQBSettings,
			.NodeContent = SmUtil.BuildNodeAttributeXml("LQB", "google_tag_manager_id", googleTagManagerId)
			})
			If SmUtil.GetApmShowInstaTouchSwitchButton(lenderConfig.ConfigData) Then
				draftRecordList.Add(New SmLenderConfigDraft() With {
					.LenderConfigID = lenderConfigID,
					.CreatedByUserID = UserInfo.UserID,
					.NodePath = LQB_INSTATOUCH_ENABLED(),
					.NodeGroup = SmSettings.NodeGroup.LQBSettings,
				.NodeContent = SmUtil.BuildNodeAttributeXml("LQB", "instatouch_enabled", IIf(instaTouchEnabled, "Y", "N").ToString())
				})
			End If
			draftRecordList.Add(New SmLenderConfigDraft() With {
				.LenderConfigID = lenderConfigID,
				.CreatedByUserID = UserInfo.UserID,
				.NodePath = LQB_VOA_PREFILL_ENABLED(),
				.NodeGroup = SmSettings.NodeGroup.LQBSettings,
			.NodeContent = SmUtil.BuildNodeAttributeXml("LQB", "voa_prefill_enabled", IIf(voaPrefillEnabled, "Y", "N").ToString())
			})
			draftRecordList.Add(New SmLenderConfigDraft() With {
				.LenderConfigID = lenderConfigID,
				.CreatedByUserID = UserInfo.UserID,
				.NodePath = LQB_BEHAVIOR_CREDIT_PULL(),
				.NodeGroup = SmSettings.NodeGroup.LQBSettings,
				.NodeContent = SmUtil.BuildNodeAttributeXml("LQB", "credit_pull", IIf(creditPull, "Y", "N").ToString())
			})
			draftRecordList.Add(New SmLenderConfigDraft() With {
				.LenderConfigID = lenderConfigID,
				.CreatedByUserID = UserInfo.UserID,
				.NodePath = LQB_APPLICATION_SAVE_FULL_APP_AS(),
				.NodeGroup = SmSettings.NodeGroup.LQBSettings,
			.NodeContent = SmUtil.BuildNodeAttributeXml("APPLICATION", "save_full_app_as", saveFullAppAs)
			})

			Dim revisionID As Integer = Common.SafeInteger(Request.Form("revisionID"))
			Dim result = smBL.SaveNodesToDraft(revisionID, draftRecordList.ToArray())
			If result Then
				Dim previewUrl As String = smBL.GeneratePreviewConfig(lenderConfigID, UserInfo.Email, Function(config, code) String.Format("{0}/Account/SignUp?lenderref={1}&mode=777&apm=setting&quicknav=true&apmstub=true&previewCode={2}", Common.LQB_WEBSITE, config.LenderRef, code),
				draftRecordList.ToArray())
				res = New CJsonResponse(True, "", "OK")
				res.Info = New With {.previewurl = previewUrl}
			Else
				res = New CJsonResponse(False, "", "FAILED")
			End If
		Catch ex As Exception
			_log.Error("Could not PreviewLQBSettingsDraft.", ex)
			res = New CJsonResponse(False, "Unable to save and preview draft. Please try again", "FAILED")
		End Try
		Response.Write(JsonConvert.SerializeObject(res))
		Response.End()
	End Sub
	Private Sub PublishLQBSettings()
		Dim res As CJsonResponse
		Try
			Dim smBL As New SmBL()
			Dim draftRecordList As New List(Of SmLenderConfigDraft)
			Dim lenderConfigID As Guid = Common.SafeGUID(Request.Form("lenderconfigid"))

			Dim defaultLoanTemplate = Common.SafeString(Request.Form("defaultLoanTemplate"))
			Dim defaultLoanOfficer = Common.SafeString(Request.Form("defaultLoanOfficer"))
			Dim creditReportingAgency = Common.SafeGUID(Request.Form("creditReportingAgency"))
			Dim allowComsumerToCreateApps = Common.SafeBoolean(Request.Form("allowComsumerToCreateApps"))
			Dim showPricerAtEnd = Common.SafeBoolean(Request.Form("showPricerAtEnd"))
			Dim lqbUser = Common.SafeString(Request.Form("lqbUser"))
			Dim lqbPassword = Common.SafeString(Request.Form("lqbPassword"))
			Dim piwikSiteId = Common.SafeString(Request.Form("piwikSiteId"))
			Dim googleTagManagerId = Common.SafeString(Request.Form("googleTagManagerId"))
			Dim instaTouchEnabled = Common.SafeBoolean(Request.Form("instaTouchEnabled"))
			Dim voaPrefillEnabled = Common.SafeBoolean(Request.Form("voaPrefillEnabled"))
			Dim creditPull = Common.SafeBoolean(Request.Form("creditPull"))
			Dim saveFullAppAs = Common.SafeString(Request.Form("saveFullAppAs"))

			Dim lenderConfig As CLenderConfig = smBL.GetLiveConfig(lenderConfigID)
			If lenderConfig Is Nothing Then
				Response.StatusCode = 404 '' Forbidden
				Response.StatusDescription = "Not found"
				Response.End() '' Terminate all the rest processes
			End If
			draftRecordList.Add(New SmLenderConfigDraft() With {
				.LenderConfigID = lenderConfigID,
				.CreatedByUserID = UserInfo.UserID,
				.NodePath = LQB_APPLICATION_DEFAULT_LOAN_TEMPLATE(),
				.NodeGroup = SmSettings.NodeGroup.LQBSettings,
			.NodeContent = SmUtil.BuildNodeAttributeXml("APPLICATION", "default_loan_template", defaultLoanTemplate)
			})
			draftRecordList.Add(New SmLenderConfigDraft() With {
				.LenderConfigID = lenderConfigID,
				.CreatedByUserID = UserInfo.UserID,
				.NodePath = LQB_APPLICATION_LOAN_OFFICER(),
				.NodeGroup = SmSettings.NodeGroup.LQBSettings,
			.NodeContent = SmUtil.BuildNodeAttributeXml("APPLICATION", "loan_officer", defaultLoanOfficer)
			})
			draftRecordList.Add(New SmLenderConfigDraft() With {
				.LenderConfigID = lenderConfigID,
				.CreatedByUserID = UserInfo.UserID,
				.NodePath = LQB_CREDIT_REPORTING_AGENCY(),
				.NodeGroup = SmSettings.NodeGroup.LQBSettings,
			.NodeContent = SmUtil.BuildNodeAttributeXml("LQB", "credit_reporting_agency", creditReportingAgency.ToString())
			})
			draftRecordList.Add(New SmLenderConfigDraft() With {
				.LenderConfigID = lenderConfigID,
				.CreatedByUserID = UserInfo.UserID,
				.NodePath = LQB_BEHAVIOR_ALLOW_CONSUMER_TO_CREATE_APPS(),
				.NodeGroup = SmSettings.NodeGroup.LQBSettings,
			.NodeContent = SmUtil.BuildNodeAttributeXml("BEHAVIOR", "allow_consumer_to_create_apps", IIf(allowComsumerToCreateApps, "Y", "N").ToString())
			})
			draftRecordList.Add(New SmLenderConfigDraft() With {
				.LenderConfigID = lenderConfigID,
				.CreatedByUserID = UserInfo.UserID,
				.NodePath = LQB_BEHAVIOR_SHOW_PRICER_AT_END(),
				.NodeGroup = SmSettings.NodeGroup.LQBSettings,
			.NodeContent = SmUtil.BuildNodeAttributeXml("BEHAVIOR", "show_pricer_at_end", IIf(showPricerAtEnd, "Y", "N").ToString())
			})
			draftRecordList.Add(New SmLenderConfigDraft() With {
				.LenderConfigID = lenderConfigID,
				.CreatedByUserID = UserInfo.UserID,
				.NodePath = LQB_USER(),
				.NodeGroup = SmSettings.NodeGroup.LQBSettings,
			.NodeContent = SmUtil.BuildNodeAttributeXml("LQB", "user", lqbUser)
			})
			draftRecordList.Add(New SmLenderConfigDraft() With {
				.LenderConfigID = lenderConfigID,
				.CreatedByUserID = UserInfo.UserID,
				.NodePath = LQB_PASSWORD(),
				.NodeGroup = SmSettings.NodeGroup.LQBSettings,
			.NodeContent = SmUtil.BuildNodeAttributeXml("LQB", "password", lqbPassword)
			})
			draftRecordList.Add(New SmLenderConfigDraft() With {
				.LenderConfigID = lenderConfigID,
				.CreatedByUserID = UserInfo.UserID,
				.NodePath = LQB_PIWIK_SITE_ID(),
				.NodeGroup = SmSettings.NodeGroup.LQBSettings,
			.NodeContent = SmUtil.BuildNodeAttributeXml("LQB", "piwik_site_id", piwikSiteId)
			})
			draftRecordList.Add(New SmLenderConfigDraft() With {
				.LenderConfigID = lenderConfigID,
				.CreatedByUserID = UserInfo.UserID,
				.NodePath = LQB_GOOGLE_TAG_MANAGER_ID(),
				.NodeGroup = SmSettings.NodeGroup.LQBSettings,
			.NodeContent = SmUtil.BuildNodeAttributeXml("LQB", "google_tag_manager_id", googleTagManagerId)
			})
			If SmUtil.GetApmShowInstaTouchSwitchButton(lenderConfig.ConfigData) Then
				draftRecordList.Add(New SmLenderConfigDraft() With {
					.LenderConfigID = lenderConfigID,
					.CreatedByUserID = UserInfo.UserID,
					.NodePath = LQB_INSTATOUCH_ENABLED(),
					.NodeGroup = SmSettings.NodeGroup.LQBSettings,
				.NodeContent = SmUtil.BuildNodeAttributeXml("LQB", "instatouch_enabled", IIf(instaTouchEnabled, "Y", "N").ToString())
				})
			End If
			draftRecordList.Add(New SmLenderConfigDraft() With {
				.LenderConfigID = lenderConfigID,
				.CreatedByUserID = UserInfo.UserID,
				.NodePath = LQB_VOA_PREFILL_ENABLED(),
				.NodeGroup = SmSettings.NodeGroup.LQBSettings,
			.NodeContent = SmUtil.BuildNodeAttributeXml("LQB", "voa_prefill_enabled", IIf(voaPrefillEnabled, "Y", "N").ToString())
			})
			draftRecordList.Add(New SmLenderConfigDraft() With {
				.LenderConfigID = lenderConfigID,
				.CreatedByUserID = UserInfo.UserID,
				.NodePath = LQB_BEHAVIOR_CREDIT_PULL(),
				.NodeGroup = SmSettings.NodeGroup.LQBSettings,
				.NodeContent = SmUtil.BuildNodeAttributeXml("LQB", "credit_pull", IIf(creditPull, "Y", "N").ToString())
			})
			draftRecordList.Add(New SmLenderConfigDraft() With {
				.LenderConfigID = lenderConfigID,
				.CreatedByUserID = UserInfo.UserID,
				.NodePath = LQB_APPLICATION_SAVE_FULL_APP_AS(),
				.NodeGroup = SmSettings.NodeGroup.LQBSettings,
			.NodeContent = SmUtil.BuildNodeAttributeXml("APPLICATION", "save_full_app_as", saveFullAppAs)
			})
			Dim comment As String = Common.SafeStripHtmlString(Request.Form("comment"))
			Dim publishAll As Boolean = Common.SafeBoolean(Request.Form("publishAll"))
			Dim result = smBL.PublishNodes(lenderConfigID, UserInfo.UserID, Request.UserHostAddress, comment, publishAll, draftRecordList.ToArray())
			If result Then
				res = New CJsonResponse(True, "", "OK")
			Else
				res = New CJsonResponse(False, "", "FAILED")
			End If
		Catch ex As Exception
			_log.Error("Could not PublishLQBSettings.", ex)
			res = New CJsonResponse(False, "Unable to publish data. Please try again", "FAILED")
		End Try
		Response.Write(JsonConvert.SerializeObject(res))
		Response.End()
	End Sub
#End Region

#Region "Design Theme"
	Private Sub SaveDesignThemeDraft()
		Dim res As New CJsonResponse()
		Try
			Dim smBL As New SmBL()
			Dim draftRecordList As New List(Of SmLenderConfigDraft)
			Dim lenderConfigID As Guid = Common.SafeGUID(Request.Form("lenderconfigid"))
			Dim themeData = JsonConvert.DeserializeObject(Of SmThemeEditorModel)(Common.SafeString(Request.Form("themeData")), New JsonSerializerSettings() With {.NullValueHandling = NullValueHandling.Ignore})

			If Not String.IsNullOrEmpty(themeData.LogoUrl) Then
				draftRecordList.Add(New SmLenderConfigDraft() With {
				.LenderConfigID = lenderConfigID,
				.CreatedByUserID = UserInfo.UserID,
				.NodePath = LOGO_URL(),
				.NodeGroup = SmSettings.NodeGroup.DesignTheme,
				.NodeContent = SmUtil.BuildNodeAttributeXml("WEBSITE", "logo_url", Common.ExtractLogoUrl(themeData.LogoUrl))})
			End If
			If Not String.IsNullOrEmpty(themeData.FavIco) Then
				draftRecordList.Add(New SmLenderConfigDraft() With {
				.LenderConfigID = lenderConfigID,
				.CreatedByUserID = UserInfo.UserID,
				.NodePath = FAV_ICO(),
				.NodeGroup = SmSettings.NodeGroup.DesignTheme,
				.NodeContent = SmUtil.BuildNodeAttributeXml("WEBSITE", "fav_ico", Common.ExtractLogoUrl(themeData.FavIco))})
			End If
			If Regex.IsMatch(themeData.ButtonColor, "^#[A-Za-z0-9]{6}$") Then
				draftRecordList.Add(New SmLenderConfigDraft() With {
				.LenderConfigID = lenderConfigID,
				.CreatedByUserID = UserInfo.UserID,
				.NodePath = BUTTON_THEME(),
				.NodeGroup = SmSettings.NodeGroup.DesignTheme,
				.NodeContent = SmUtil.BuildNodeAttributeXml("COLOR_SCHEME", "button_theme", themeData.ButtonColor)})
			End If
			If Regex.IsMatch(themeData.ButtonFontColor, "^#[A-Za-z0-9]{6}$") Then
				draftRecordList.Add(New SmLenderConfigDraft() With {
				.LenderConfigID = lenderConfigID,
				.CreatedByUserID = UserInfo.UserID,
				.NodePath = BUTTON_FONT_THEME(),
				.NodeGroup = SmSettings.NodeGroup.DesignTheme,
				.NodeContent = SmUtil.BuildNodeAttributeXml("COLOR_SCHEME", "button_font_theme", themeData.ButtonFontColor)})
			End If
			If Regex.IsMatch(themeData.FooterColor, "^#[A-Za-z0-9]{6}$") Then
				draftRecordList.Add(New SmLenderConfigDraft() With {
				.LenderConfigID = lenderConfigID,
				.CreatedByUserID = UserInfo.UserID,
				.NodePath = FOOTER_THEME(),
				.NodeGroup = SmSettings.NodeGroup.DesignTheme,
				.NodeContent = SmUtil.BuildNodeAttributeXml("COLOR_SCHEME", "footer_theme", themeData.FooterColor)})
			End If
			If Regex.IsMatch(themeData.FooterFontColor, "^#[A-Za-z0-9]{6}$") Then
				draftRecordList.Add(New SmLenderConfigDraft() With {
				.LenderConfigID = lenderConfigID,
				.CreatedByUserID = UserInfo.UserID,
				.NodePath = FOOTER_FONT_THEME(),
				.NodeGroup = SmSettings.NodeGroup.DesignTheme,
				.NodeContent = SmUtil.BuildNodeAttributeXml("COLOR_SCHEME", "footer_font_theme", themeData.FooterFontColor)})
			End If
			If Regex.IsMatch(themeData.HeaderColor, "^#[A-Za-z0-9]{6}$") Then
				draftRecordList.Add(New SmLenderConfigDraft() With {
				.LenderConfigID = lenderConfigID,
				.CreatedByUserID = UserInfo.UserID,
				.NodePath = HEADER_THEME(),
				.NodeGroup = SmSettings.NodeGroup.DesignTheme,
				.NodeContent = SmUtil.BuildNodeAttributeXml("COLOR_SCHEME", "header_theme", themeData.HeaderColor)})
			End If
			If Regex.IsMatch(themeData.Header2Color, "^#[A-Za-z0-9]{6}$") Then
				draftRecordList.Add(New SmLenderConfigDraft() With {
				.LenderConfigID = lenderConfigID,
				.CreatedByUserID = UserInfo.UserID,
				.NodePath = HEADER2_THEME(),
				.NodeGroup = SmSettings.NodeGroup.DesignTheme,
				.NodeContent = SmUtil.BuildNodeAttributeXml("COLOR_SCHEME", "header_theme2", themeData.Header2Color)})
			End If
			If Regex.IsMatch(themeData.BackgroundColor, "^#[A-Za-z0-9]{6}$") Then
				draftRecordList.Add(New SmLenderConfigDraft() With {
				.LenderConfigID = lenderConfigID,
				.CreatedByUserID = UserInfo.UserID,
				.NodePath = BACKGROUND_THEME(),
				.NodeGroup = SmSettings.NodeGroup.DesignTheme,
				.NodeContent = SmUtil.BuildNodeAttributeXml("COLOR_SCHEME", "bg_theme", themeData.BackgroundColor)})
			End If
			If Regex.IsMatch(themeData.ContentColor, "^#[A-Za-z0-9]{6}$") Then
				draftRecordList.Add(New SmLenderConfigDraft() With {
				.LenderConfigID = lenderConfigID,
				.CreatedByUserID = UserInfo.UserID,
				.NodePath = CONTENT_THEME(),
				.NodeGroup = SmSettings.NodeGroup.DesignTheme,
				.NodeContent = SmUtil.BuildNodeAttributeXml("COLOR_SCHEME", "content_theme", themeData.ContentColor)})
			End If
			Dim revisionID As Integer = Common.SafeInteger(Request.Form("revisionID"))
			Dim result = smBL.SaveNodesToDraft(revisionID, draftRecordList.ToArray())
			If result Then
				res = New CJsonResponse(True, "", "OK")
			Else
				res = New CJsonResponse(False, "", "FAILED")
			End If
		Catch ex As Exception
			_log.Error("Could not SaveDesignThemeDraft.", ex)
			res = New CJsonResponse(False, "Unable to save draft. Please try again", "FAILED")
		End Try
		Response.Write(JsonConvert.SerializeObject(res))
		Response.End()
	End Sub
	'Private Sub PreviewDesignThemeDraft()
	'	Throw New NotImplementedException
	'End Sub
	Private Sub PublishDesignThemeDraft()
		Dim res As New CJsonResponse()
		Try
			Dim smBL As New SmBL()
			Dim lenderConfigID As Guid = Common.SafeGUID(Request.Form("lenderconfigid"))
			Dim draftRecordList As New List(Of SmLenderConfigDraft)
			Dim themeData = JsonConvert.DeserializeObject(Of SmThemeEditorModel)(Common.SafeString(Request.Form("themeData")), New JsonSerializerSettings() With {.NullValueHandling = NullValueHandling.Ignore})

			If Not String.IsNullOrEmpty(themeData.LogoUrl) Then
				draftRecordList.Add(New SmLenderConfigDraft() With {
				.LenderConfigID = lenderConfigID,
				.CreatedByUserID = UserInfo.UserID,
				.NodePath = LOGO_URL(),
				.NodeGroup = SmSettings.NodeGroup.DesignTheme,
				.NodeContent = SmUtil.BuildNodeAttributeXml("WEBSITE", "logo_url", Common.ExtractLogoUrl(themeData.LogoUrl))})
			End If
			If Not String.IsNullOrEmpty(themeData.FavIco) Then
				draftRecordList.Add(New SmLenderConfigDraft() With {
				.LenderConfigID = lenderConfigID,
				.CreatedByUserID = UserInfo.UserID,
				.NodePath = FAV_ICO(),
				.NodeGroup = SmSettings.NodeGroup.DesignTheme,
				.NodeContent = SmUtil.BuildNodeAttributeXml("WEBSITE", "fav_ico", Common.ExtractLogoUrl(themeData.FavIco))})
			End If
			If Regex.IsMatch(themeData.ButtonColor, "^#[A-Za-z0-9]{6}$") Then
				draftRecordList.Add(New SmLenderConfigDraft() With {
				.LenderConfigID = lenderConfigID,
				.CreatedByUserID = UserInfo.UserID,
				.NodePath = BUTTON_THEME(),
				.NodeGroup = SmSettings.NodeGroup.DesignTheme,
				.NodeContent = SmUtil.BuildNodeAttributeXml("COLOR_SCHEME", "button_theme", themeData.ButtonColor)})
			End If
			If Regex.IsMatch(themeData.ButtonFontColor, "^#[A-Za-z0-9]{6}$") Then
				draftRecordList.Add(New SmLenderConfigDraft() With {
				.LenderConfigID = lenderConfigID,
				.CreatedByUserID = UserInfo.UserID,
				.NodePath = BUTTON_FONT_THEME(),
				.NodeGroup = SmSettings.NodeGroup.DesignTheme,
				.NodeContent = SmUtil.BuildNodeAttributeXml("COLOR_SCHEME", "button_font_theme", themeData.ButtonFontColor)})
			End If
			If Regex.IsMatch(themeData.FooterColor, "^#[A-Za-z0-9]{6}$") Then
				draftRecordList.Add(New SmLenderConfigDraft() With {
				.LenderConfigID = lenderConfigID,
				.CreatedByUserID = UserInfo.UserID,
				.NodePath = FOOTER_THEME(),
				.NodeGroup = SmSettings.NodeGroup.DesignTheme,
				.NodeContent = SmUtil.BuildNodeAttributeXml("COLOR_SCHEME", "footer_theme", themeData.FooterColor)})
			End If
			If Regex.IsMatch(themeData.FooterFontColor, "^#[A-Za-z0-9]{6}$") Then
				draftRecordList.Add(New SmLenderConfigDraft() With {
				.LenderConfigID = lenderConfigID,
				.CreatedByUserID = UserInfo.UserID,
				.NodePath = FOOTER_FONT_THEME(),
				.NodeGroup = SmSettings.NodeGroup.DesignTheme,
				.NodeContent = SmUtil.BuildNodeAttributeXml("COLOR_SCHEME", "footer_font_theme", themeData.FooterFontColor)})
			End If
			If Regex.IsMatch(themeData.HeaderColor, "^#[A-Za-z0-9]{6}$") Then
				draftRecordList.Add(New SmLenderConfigDraft() With {
				.LenderConfigID = lenderConfigID,
				.CreatedByUserID = UserInfo.UserID,
				.NodePath = HEADER_THEME(),
				.NodeGroup = SmSettings.NodeGroup.DesignTheme,
				.NodeContent = SmUtil.BuildNodeAttributeXml("COLOR_SCHEME", "header_theme", themeData.HeaderColor)})
			End If
			If Regex.IsMatch(themeData.Header2Color, "^#[A-Za-z0-9]{6}$") Then
				draftRecordList.Add(New SmLenderConfigDraft() With {
				.LenderConfigID = lenderConfigID,
				.CreatedByUserID = UserInfo.UserID,
				.NodePath = HEADER2_THEME(),
				.NodeGroup = SmSettings.NodeGroup.DesignTheme,
				.NodeContent = SmUtil.BuildNodeAttributeXml("COLOR_SCHEME", "header_theme2", themeData.Header2Color)})
			End If
			If Regex.IsMatch(themeData.BackgroundColor, "^#[A-Za-z0-9]{6}$") Then
				draftRecordList.Add(New SmLenderConfigDraft() With {
				.LenderConfigID = lenderConfigID,
				.CreatedByUserID = UserInfo.UserID,
				.NodePath = BACKGROUND_THEME(),
				.NodeGroup = SmSettings.NodeGroup.DesignTheme,
				.NodeContent = SmUtil.BuildNodeAttributeXml("COLOR_SCHEME", "bg_theme", themeData.BackgroundColor)})
			End If
			If Regex.IsMatch(themeData.ContentColor, "^#[A-Za-z0-9]{6}$") Then
				draftRecordList.Add(New SmLenderConfigDraft() With {
				.LenderConfigID = lenderConfigID,
				.CreatedByUserID = UserInfo.UserID,
				.NodePath = CONTENT_THEME(),
				.NodeGroup = SmSettings.NodeGroup.DesignTheme,
				.NodeContent = SmUtil.BuildNodeAttributeXml("COLOR_SCHEME", "content_theme", themeData.ContentColor)})
			End If
			Dim comment As String = Common.SafeString(Request.Form("comment"))
			Dim publishAll As Boolean = Common.SafeBoolean(Request.Form("publishAll"))
			Dim result = smBL.PublishNodes(lenderConfigID, UserInfo.UserID, Request.UserHostAddress, comment, publishAll, draftRecordList.ToArray())
			If result Then
				res = New CJsonResponse(True, "", "OK")
			Else
				res = New CJsonResponse(False, "", "FAILED")
			End If
		Catch ex As Exception
			_log.Error("Could not PublishDesignThemeDraft.", ex)
			res = New CJsonResponse(False, "Unable to publish data. Please try again", "FAILED")
		End Try
		Response.Write(JsonConvert.SerializeObject(res))
		Response.End()
	End Sub
#End Region
#Region "Users"
	Private Sub LoadUserGrid()
		Try
			Dim paging = JsonConvert.DeserializeObject(Of SmPagingInfoModel)(Request.Form("paging_info"))
			Dim filter = JsonConvert.DeserializeObject(Of SmUserListFilter)(Request.Form("filter_info"))
			Dim control As Sm_Uc_UserList = CType(LoadControl("~/Sm/Uc/UserList.ascx"), Sm_Uc_UserList)
			control.PagingInfo = paging
			'if current user is in role officer, get user in role [User] only
			If filter Is Nothing AndAlso UserInfo.Role = SmSettings.Role.Officer Then
				filter = New SmUserListFilter()
				filter.Roles.Add(SmSettings.Role.SuperOperator)
				filter.Roles.Add(SmSettings.Role.LegacyOperator)
				filter.Roles.Add(SmSettings.Role.Officer)
			End If
			control.FilterInfo = filter
			Me.Controls.Add(control)
		Catch ex As Exception
			_log.Error("Could not load user grid", ex)
			Response.Write("<h3 style='text-align: center; color: red;'>Unable to render content. Please try again later</h3>")
		End Try
	End Sub
	Private Sub ChangeUserState()
		Dim res As CJsonResponse
		Try
			Dim smAuthBL As New SmAuthBL()
			Dim userID As Guid = Common.SafeGUID(Request.Form("userid"))
			Dim isEnabled As String = Common.SafeStripHtmlString(Request.Form("enable")).ToUpper()
			Dim ipAddress As String = Request.ServerVariables("REMOTE_ADDR")
			Dim u As SmUser = smAuthBL.GetUserByID(userID)
			If u Is Nothing Then
				Response.StatusCode = 404 '' Forbidden
				Response.StatusDescription = "Not found"
				Response.End() '' Terminate all the rest processes
			End If
			Dim result = smAuthBL.ChangeUserState(u, IIf(isEnabled = "Y", SmSettings.UserStatus.Active, SmSettings.UserStatus.Disabled), UserInfo.UserID, ipAddress)
			If result = "ok" Then
				If u.UserID = UserInfo.UserID Then
					res = New CJsonResponse(True, "", "RELOAD")
				Else
					res = New CJsonResponse(True, "", "OK")
				End If
			Else
				res = New CJsonResponse(False, "", "FAILED")
			End If
		Catch ex As Exception
			_log.Error("Could not ChangeUserState.", ex)
			res = New CJsonResponse(False, "Could not change state of user. Please try again", "FAILED")
		End Try
		Response.Write(JsonConvert.SerializeObject(res))
		Response.End()
	End Sub

	Private Sub UnlockUser()
		Dim res As CJsonResponse
		Try
			Dim smAuthBL As New SmAuthBL()
			Dim userID As Guid = Common.SafeGUID(Request.Form("userid"))
			Dim ipAddress As String = Request.ServerVariables("REMOTE_ADDR")
			Dim u As SmUser = smAuthBL.GetUserByID(userID)
			If u Is Nothing Then
				Response.StatusCode = 404 '' Forbidden
				Response.StatusDescription = "Not found"
				Response.End() '' Terminate all the rest processes
			End If
			If u.Status = SmSettings.UserStatus.Active AndAlso u.LoginFailedCount >= Common.SafeInteger(ConfigurationManager.AppSettings("LoginFailedCountExceed")) Then
				u.LoginFailedCount = 0
			End If
			If u.Status = SmSettings.UserStatus.Active AndAlso u.LastLoginDate.HasValue AndAlso u.LastLoginDate.Value.AddDays(u.ExpireDays) < DateTime.Now Then
				u.LastLoginDate = Now
			End If
			Dim result = smAuthBL.UnlockUser(u, UserInfo.UserID, ipAddress)
			If result = "ok" Then
				res = New CJsonResponse(True, "", "OK")
			Else
				res = New CJsonResponse(False, "", "FAILED")
			End If
		Catch ex As Exception
			_log.Error("Could not UnlockUser.", ex)
			res = New CJsonResponse(False, "Could not unlock user. Please try again", "FAILED")
		End Try
		Response.Write(JsonConvert.SerializeObject(res))
		Response.End()
	End Sub
	Private Sub ResetLoginFailureCount()
		Dim res As CJsonResponse
		Try
			Dim smAuthBL As New SmAuthBL()
			Dim userID As Guid = Common.SafeGUID(Request.Form("userid"))
			Dim ipAddress As String = Request.ServerVariables("REMOTE_ADDR")
			Dim u As SmUser = smAuthBL.GetUserByID(userID)
			If u Is Nothing Then
				Response.StatusCode = 404 '' Forbidden
				Response.StatusDescription = "Not found"
				Response.End() '' Terminate all the rest processes
			End If
			smAuthBL.ResetLoginFailureCount(u, UserInfo.UserID, ipAddress)
			res = New CJsonResponse(True, "", "OK")
		Catch ex As Exception
			_log.Error("Could not ResetLoginFailureCount.", ex)
			res = New CJsonResponse(False, "Could not reset login failure count. Please try again", "FAILED")
		End Try
		Response.Write(JsonConvert.SerializeObject(res))
		Response.End()
	End Sub
	Private Sub DeleteUser()
		Dim res As CJsonResponse
		Try
			Dim smAuthBL As New SmAuthBL()
			Dim userID As Guid = Common.SafeGUID(Request.Form("userid"))
			Dim ipAddress As String = Request.ServerVariables("REMOTE_ADDR")
			Dim u As SmUser = smAuthBL.GetUserByID(userID)
			If u Is Nothing Then
				Response.StatusCode = 404 '' Forbidden
				Response.StatusDescription = "Not found"
				Response.End() '' Terminate all the rest processes
			End If
			smAuthBL.DeleteUser(u, UserInfo.UserID, ipAddress)
			res = New CJsonResponse(True, "", IIf(u.UserID = UserInfo.UserID, "RELOAD", "OK"))
		Catch ex As Exception
			_log.Error("Could not ResetLoginFailureCount.", ex)
			res = New CJsonResponse(False, "Could not delete user. Please try again", "FAILED")
		End Try
		Response.Write(JsonConvert.SerializeObject(res))
		Response.End()
	End Sub


	Private Sub LoadAddUserForm()
		Try
			Dim control As Sm_Uc_AddUserForm = CType(LoadControl("~/Sm/Uc/AddUserForm.ascx"), Sm_Uc_AddUserForm)
			control.Email = Common.SafeStripHtmlString(Request.Form("email"))
			control.AvailableRoles.Add(SmSettings.Role.Officer)
			control.AvailableRoles.Add(SmSettings.Role.Accountant)
			control.AvailableRoles.Add(SmSettings.Role.LegacyOperator)
			control.AvailableRoles.Add(SmSettings.Role.SuperOperator)
			Me.Controls.Add(control)
		Catch ex As Exception
			_log.Error("Could not load add user form", ex)
			Response.Write("<h3 style='text-align: center; color: red;'>Unable to load add user form. Please try again later</h3>")
		End Try
	End Sub

	Private Sub LoadEditUserForm()
		Try
			Dim control As Sm_Uc_EditUserForm = CType(LoadControl("~/Sm/Uc/EditUserForm.ascx"), Sm_Uc_EditUserForm)
			control.AvailableRoles.Add(SmSettings.Role.User)
			control.AvailableRoles.Add(SmSettings.Role.Officer)
			control.AvailableRoles.Add(SmSettings.Role.Accountant)
			control.AvailableRoles.Add(SmSettings.Role.LegacyOperator)
			control.AvailableRoles.Add(SmSettings.Role.SuperOperator)
			Dim userID As Guid = Common.SafeGUID(Request.Form("userid"))
			Dim smAuthBL As New SmAuthBL
			Dim u As SmUser = smAuthBL.GetUserByID(userID)
			If u Is Nothing Then
				Response.StatusCode = 404 '' Forbidden
				Response.StatusDescription = "Not found"
				Response.End() '' Terminate all the rest processes
			End If
			control.UserData = u
			Me.Controls.Add(control)
		Catch ex As Exception
			_log.Error("Could not load edit user form", ex)
			Response.Write("<h3 style='text-align: center; color: red;'>Unable to load edit user form. Please try again later</h3>")
		End Try
	End Sub

	'Private Sub LoadAddLenderConfigUserForm()
	'	Try
	'		Dim control As Sm_Uc_AddUserForm = CType(LoadControl("~/Sm/Uc/AddUserForm.ascx"), Sm_Uc_AddUserForm)
	'		Dim email As String = Common.SafeString(Request.Form("email"))
	'		control.Email = email
	'		control.AvailableRoles.Add(SmSettings.Role.User)
	'		Me.Controls.Add(control)
	'	Catch ex As Exception
	'		_log.Error("Could not load add lender config userform", ex)
	'		Response.Write("<h3 style='text-align: center; color: red;'>Unable to load add user form. Please try again later</h3>")
	'	End Try
	'End Sub
	Private Sub SaveNewUser()
		Dim res As CJsonResponse
		Try
			Dim smAuthBL As New SmAuthBL()
			Dim userObj = JsonConvert.DeserializeObject(Of SmUser)(Common.SafeString(Request.Form("user_data")), New JsonSerializerSettings() With {.NullValueHandling = NullValueHandling.Ignore}).MakeItSafe()
			Dim existedUser As SmUser = smAuthBL.GetUserByEmail(userObj.Email)
			If existedUser IsNot Nothing Then
				res = New CJsonResponse(False, "Email has been used. Please choose another email.", "EXISTED")
			Else
				Dim ipAddress As String = Request.ServerVariables("REMOTE_ADDR")
				Dim token As String = Guid.NewGuid().ToString().Replace("-", "").Substring(0, 20)
				userObj.UserID = Guid.NewGuid()
				Dim result = smAuthBL.AddUser(userObj, UserInfo.UserID, ipAddress, token)
				If result = "ok" Then
					Dim emailContent As String = SmUtil.BuildActivateAccountEmail(token, _serverRoot)
					SmUtil.SendEmail(userObj.Email, "MeridianLink's APM Account Activation", emailContent)
					res = New CJsonResponse(True, "", "OK")
				Else
					res = New CJsonResponse(False, "", "FAILED")
				End If
			End If
		Catch ex As Exception
			_log.Error("Could not SaveNewUser.", ex)
			res = New CJsonResponse(False, "Could not add new user. Please try again", "FAILED")
		End Try
		Response.Write(JsonConvert.SerializeObject(res))
		Response.End()
	End Sub
	''' <summary>
	''' This UpdateUser should only be accessible by superOperator bc it allow changing the email
	''' </summary>
	Private Sub UpdateUser()
		Dim res As CJsonResponse
		Try
			Dim smAuthBL As New SmAuthBL()
			Dim userObj = JsonConvert.DeserializeObject(Of SmUser)(Common.SafeString(Request.Form("user_data")), New JsonSerializerSettings() With {.NullValueHandling = NullValueHandling.Ignore})
			If userObj Is Nothing OrElse userObj.UserID = Guid.Empty OrElse smAuthBL.GetUserByID(userObj.UserID) Is Nothing Then
				Response.StatusCode = 404 '' Forbidden
				Response.StatusDescription = "Not found"
				Response.End() '' Terminate all the rest processes
			End If
			Dim u As SmUser = smAuthBL.GetUserByID(userObj.UserID)
			If u Is Nothing Then
				Response.StatusCode = 404 '' Forbidden
				Response.StatusDescription = "Not found"
				Response.End() '' Terminate all the rest processes
				Return
			End If
			Dim existedUser As SmUser = smAuthBL.GetUserByEmail(userObj.Email)
			If existedUser IsNot Nothing AndAlso existedUser.UserID <> userObj.UserID Then
				res = New CJsonResponse(False, "Email has been used. Please choose another email.", "EXISTED")
			Else
				If existedUser Is Nothing Then
					existedUser = New SmUser
				End If
				existedUser.UserID = userObj.UserID

				''assuming chaning the email can only be done by super operator
				If userObj.Email <> "" Then
					existedUser.Email = userObj.Email
				End If

				existedUser.FirstName = userObj.FirstName
				existedUser.LastName = userObj.LastName
				existedUser.Phone = userObj.Phone
				existedUser.Avatar = userObj.Avatar
				existedUser.ExpireDays = userObj.ExpireDays
				existedUser.Role = userObj.Role
				Dim ipAddress As String = Request.ServerVariables("REMOTE_ADDR")
				Dim result = smAuthBL.UpdateUser(existedUser, UserInfo.UserID, ipAddress)
				If result = "ok" Then
					If existedUser.UserID = UserInfo.UserID Then
						existedUser.Password = String.Empty
						existedUser.Salt = String.Empty
						HttpContext.Current.Session("CURRENT_USER_INFO") = existedUser
						res = New CJsonResponse(True, "", New With {.avatar = IIf(String.IsNullOrEmpty(existedUser.Avatar.Trim()), "/images/avatar.jpg", existedUser.Avatar), .firstName = existedUser.FirstName})
					Else
						res = New CJsonResponse(True, "", "OK")
					End If

				Else
					res = New CJsonResponse(False, "Could not update user data", "FAILED")
				End If
			End If
		Catch ex As Exception
			_log.Error("Could not UpdateUser.", ex)
			res = New CJsonResponse(False, "Could not update user data. Please try again", "FAILED")
		End Try
		Response.Write(JsonConvert.SerializeObject(res))
		Response.End()
	End Sub
	Private Sub UpdatePortalUser()
		Dim res As CJsonResponse
		Try
			Dim smAuthBL As New SmAuthBL()
			Dim userObj = JsonConvert.DeserializeObject(Of SmUser)(Common.SafeString(Request.Form("user_data")), New JsonSerializerSettings() With {.NullValueHandling = NullValueHandling.Ignore})
			Dim smBL As New SmBL()
			Dim lenderConfigID As Guid = Common.SafeGUID(Request.Form("lenderConfigID"))

			If userObj Is Nothing OrElse Not smBL.CheckLenderExistedByLenderConfigID(lenderConfigID) OrElse userObj.UserID = Guid.Empty OrElse smAuthBL.GetUserByID(userObj.UserID) Is Nothing Then
				Response.StatusCode = 404 '' Forbidden
				Response.StatusDescription = "Not found"
				Response.End() '' Terminate all the rest processes
			End If
			Dim existedUser As SmUser = smAuthBL.GetUserByEmail(userObj.Email)
			If existedUser IsNot Nothing AndAlso existedUser.UserID <> userObj.UserID Then
				res = New CJsonResponse(False, "Email has been used. Please choose another email.", "EXISTED")
			Else
				Dim ipAddress As String = Request.ServerVariables("REMOTE_ADDR")
				userObj.Role = SmSettings.Role.User
				Dim result = smAuthBL.UpdateUser(userObj, UserInfo.UserID, ipAddress)
				If result = "ok" Then
					res = New CJsonResponse(True, "", "OK")
				Else
					res = New CJsonResponse(False, "", "FAILED")
				End If
			End If
		Catch ex As Exception
			_log.Error("Could not UpdatePortalUser.", ex)
			res = New CJsonResponse(False, "Could not update portal user data. Please try again", "FAILED")
		End Try
		Response.Write(JsonConvert.SerializeObject(res))
		Response.End()
	End Sub
#End Region

#Region "Revision"
	Private Sub LoadRevisionGrid()
		Try
			Dim paging = JsonConvert.DeserializeObject(Of SmPagingInfoModel)(Request.Form("paging_info"))
			Dim lenderConfigId As Guid = Common.SafeGUID(Request.Form("lenderConfigId"))
			Dim control As Sm_Uc_RevisionList = CType(LoadControl("~/Sm/Uc/RevisionList.ascx"), Sm_Uc_RevisionList)
			control.PagingInfo = paging
			control.LenderConfigID = lenderConfigId
			Me.Controls.Add(control)
		Catch ex As Exception
			_log.Error("Could not load revision grid", ex)
			Response.Write("<h3 style='text-align: center; color: red;'>Unable to render content. Please try again later</h3>")
		End Try
	End Sub
#End Region

#Region "Action Logs"
	Private Sub LoadActionLogGrid()
		Try
			Dim paging = JsonConvert.DeserializeObject(Of SmPagingInfoModel)(Request.Form("paging_info"))
			Dim filter = JsonConvert.DeserializeObject(Of SmActionLogListFilter)(Request.Form("filter_info"))
			If filter IsNot Nothing Then
				filter = filter.MakeItSafe()
			End If
			Dim control As Sm_Uc_ActionLogList = CType(LoadControl("~/Sm/Uc/ActionLogList.ascx"), Sm_Uc_ActionLogList)
			control.PagingInfo = paging
			control.FilterInfo = filter
			Me.Controls.Add(control)
		Catch ex As Exception
			_log.Error("Could not load action log grid", ex)
			Response.Write("<h3 style='text-align: center; color: red;'>Unable to render content. Please try again later</h3>")
		End Try
	End Sub
#End Region
#Region "Lender"
	Private Sub LoadLenderGrid()
		Try
			Dim paging = JsonConvert.DeserializeObject(Of SmPagingInfoModel)(Request.Form("paging_info"))
			Dim filter = JsonConvert.DeserializeObject(Of SmBackOfficeLenderListFilter)(Request.Form("filter_info"))
			If filter IsNot Nothing Then
				filter = filter.MakeItSafe()
			End If
			Dim control As Sm_Uc_LenderList = CType(LoadControl("~/Sm/Uc/LenderList.ascx"), Sm_Uc_LenderList)
			control.PagingInfo = paging
			control.FilterInfo = filter
			Me.Controls.Add(control)
		Catch ex As Exception
			_log.Error("Could not load lender grid", ex)
			Response.Write("<h3 style='text-align: center; color: red;'>Unable to render content. Please try again later</h3>")
		End Try
	End Sub
	Private Sub GetAllLenders()
		Try
			Dim smBL As New SmBL()
			Dim pagingInfo As New SmPagingInfoModel With {.PageIndex = 0, .PageSize = 0} 'get all
			Dim lenders As List(Of SmBackOfficeLender) = smBL.GetPortals(pagingInfo, Nothing)
			Response.Clear()
			Response.Write(JsonConvert.SerializeObject(LenderToObject(lenders)))
		Catch ex As Exception
			_log.Error("Could not get all lenders for search", ex)
			Response.Clear()
			Response.Write("")
		End Try
	End Sub

	Private Function LenderToObject(lst As List(Of SmBackOfficeLender)) As Object
		Dim result As New List(Of Object)
		If lst IsNot Nothing AndAlso lst.Any() Then
			For Each l In lst
				If l.PortalList IsNot Nothing AndAlso l.PortalList.Any() Then
					For Each portal In l.PortalList
						result.Add(New With {.id = portal.LenderConfigID, .name = String.Format("{0}({1}, {2}) - {3}", l.LenderName, l.City, l.State, portal.LenderRef)})
					Next
					'Else
					'	result.Add(New With {.id = "0", .name = String.Format("{0}({1}, {2})", l.LenderName, l.City, l.State)})
				End If
			Next
		End If
		Return result
	End Function

	Private Sub GetAllLenders(userID As Guid)
		Try
			Dim smBL As New SmBL()
			Dim pagingInfo As New SmPagingInfoModel With {.PageIndex = 0, .PageSize = 0} 'get all
			Dim lenders As List(Of SmBackOfficeLender) = smBL.GetPortals(pagingInfo, Nothing, userID)
			Response.Clear()
			Response.Write(JsonConvert.SerializeObject(LenderToObject(lenders)))
		Catch ex As Exception
			_log.Error("Could not get all lenders for search", ex)
			Response.Clear()
			Response.Write("")
		End Try
	End Sub

	Private Sub LoadLenderUserRoleGrid()
		Try
			Dim paging = JsonConvert.DeserializeObject(Of SmPagingInfoModel)(Request.Form("paging_info"))
			Dim filter = JsonConvert.DeserializeObject(Of SmLenderUserRoleListFilter)(Request.Form("filter_info"))
			Dim backOfficeLenderId As Guid = Common.SafeGUID(Request.Form("bid"))
			Dim smBL As New SmBL
			Dim backOfficeLender As SmBackOfficeLender = smBL.GetBackOfficeLenderByID(backOfficeLenderId)
			If backOfficeLender IsNot Nothing Then
				Dim control As Sm_Uc_LenderUserRoleList = CType(LoadControl("~/Sm/Uc/LenderUserRoleList.ascx"), Sm_Uc_LenderUserRoleList)
				control.PagingInfo = paging
				If filter Is Nothing Then
					filter = New SmLenderUserRoleListFilter()
				End If
				filter.LenderID = backOfficeLender.LenderID
				control.FilterInfo = filter.MakeItSafe()
				Me.Controls.Add(control)
			Else
				Response.StatusCode = 404 '' Forbidden
				Response.StatusDescription = "Not found"
				Response.End() '' Terminate all the rest processes
			End If
		Catch ex As Exception
			_log.Error("Could not load lender user role grid", ex)
			Response.Write("<h3 style='text-align: center; color: red;'>Unable to render content. Please try again later</h3>")
		End Try
	End Sub

	Private Sub LoadPortalUserRoleGrid()
		Try
			Dim paging = JsonConvert.DeserializeObject(Of SmPagingInfoModel)(Request.Form("paging_info"))
			Dim filter = JsonConvert.DeserializeObject(Of SmPortalUserRoleListFilter)(Request.Form("filter_info"))
			Dim lenderConfigID As Guid = Common.SafeGUID(Request.Form("lenderConfigID"))
			Dim smBL As New SmBL
			Dim portal As SmLenderConfigBasicInfo = smBL.GetPortalBasicInfoByLenderConfigID(lenderConfigID)
			If portal IsNot Nothing Then
				Dim control As Sm_Uc_PortalUserRoleList = CType(LoadControl("~/Sm/Uc/PortalUserRoleList.ascx"), Sm_Uc_PortalUserRoleList)
				control.PagingInfo = paging
				If filter Is Nothing Then
					filter = New SmPortalUserRoleListFilter()
				End If
				filter.LenderConfigID = portal.LenderConfigID
				filter.LenderID = portal.LenderID
				control.FilterInfo = filter.MakeItSafe()
				Me.Controls.Add(control)
			Else
				Response.StatusCode = 404 '' Forbidden
				Response.StatusDescription = "Not found"
				Response.End() '' Terminate all the rest processes
			End If
		Catch ex As Exception
			_log.Error("Could not load portal user role grid", ex)
			Response.Write("<h3 style='text-align: center; color: red;'>Unable to render content. Please try again later</h3>")
		End Try
	End Sub

	Private Sub ChangeLenderUserRoleState()
		Dim res As CJsonResponse = Nothing
		Try
			Dim smBL As New SmBL()
			Dim smAuthBL As New SmAuthBL()
			Dim userRoleID As Guid = Common.SafeGUID(Request.Form("userRoleID"))
			Dim comment As String = Common.SafeStripHtmlString(Request.Form("comment"))
			Dim isEnabled As String = Common.SafeStripHtmlString(Request.Form("enable")).ToUpper()
			Dim ipAddress As String = Request.ServerVariables("REMOTE_ADDR")
			Dim backOfficeLenderID As Guid = Common.SafeGUID(Request.Form("bid"))
			Dim backOfficeLender As SmBackOfficeLender = smBL.GetBackOfficeLenderByID(backOfficeLenderID)
			Dim userRole As SmUserRole = smBL.GetUserRoleByID(userRoleID)
			Dim u As SmUser = Nothing
			If userRole IsNot Nothing Then
				u = smAuthBL.GetUserByID(userRole.UserID)
			End If
			If backOfficeLender IsNot Nothing AndAlso userRole IsNot Nothing AndAlso u IsNot Nothing Then
				Dim result = smBL.ChangeLenderUserRoleState(userRoleID, IIf(isEnabled = "Y", SmSettings.UserRoleStatus.Active, SmSettings.UserRoleStatus.InActive), backOfficeLender, u, UserInfo.UserID, ipAddress, comment)
				If result = "ok" Then
					res = New CJsonResponse(True, "", IIf(u.UserID = UserInfo.UserID, "RELOAD", "OK"))
				Else
					res = New CJsonResponse(False, "", "FAILED")
				End If
			Else
				Response.StatusCode = 404 '' Forbidden
				Response.StatusDescription = "Not found"
				Response.End() '' Terminate all the rest processes
			End If
		Catch ex As Exception
			_log.Error("Could not ChangeLenderUserRoleState.", ex)
			res = New CJsonResponse(False, "Could not change role state of user. Please try again", "FAILED")
		End Try
		Response.Write(JsonConvert.SerializeObject(res))
		Response.End()
	End Sub

	Private Sub ChangePortalUserRoleState()
		Dim res As CJsonResponse = Nothing
		Try
			Dim smBL As New SmBL()
			Dim smAuthBL As New SmAuthBL()
			Dim userRoleID As Guid = Common.SafeGUID(Request.Form("userRoleID"))
			Dim comment As String = Common.SafeStripHtmlString(Request.Form("comment"))
			Dim isEnabled As String = Common.SafeStripHtmlString(Request.Form("enable")).ToUpper()
			Dim ipAddress As String = Request.ServerVariables("REMOTE_ADDR")
			Dim lenderConfigID As Guid = Common.SafeGUID(Request.Form("lenderConfigID"))
			Dim lenderConfig As SmLenderConfigBasicInfo = smBL.GetPortalBasicInfoByLenderConfigID(lenderConfigID)
			Dim userRole As SmUserRole = smBL.GetUserRoleByID(userRoleID)
			Dim u As SmUser = Nothing
			If userRole IsNot Nothing Then
				u = smAuthBL.GetUserByID(userRole.UserID)
			End If
			If lenderConfig IsNot Nothing AndAlso userRole IsNot Nothing AndAlso u IsNot Nothing Then
				Dim result = smBL.ChangePortalUserRoleState(userRoleID, IIf(isEnabled = "Y", SmSettings.UserRoleStatus.Active, SmSettings.UserRoleStatus.InActive), lenderConfig, u, UserInfo.UserID, ipAddress, comment)
				If result = "ok" Then
					res = New CJsonResponse(True, "", IIf(u.UserID = UserInfo.UserID, "RELOAD", "OK"))
				Else
					res = New CJsonResponse(False, "", "FAILED")
				End If
			Else
				Response.StatusCode = 404 '' Forbidden
				Response.StatusDescription = "Not found"
				Response.End() '' Terminate all the rest processes
			End If
		Catch ex As Exception
			_log.Error("Could not ChangePortalUserRoleState.", ex)
			res = New CJsonResponse(False, "Could not change role state of user. Please try again", "FAILED")
		End Try
		Response.Write(JsonConvert.SerializeObject(res))
		Response.End()
	End Sub
	Private Sub RemoveLenderUserRole()
		Dim res As CJsonResponse = Nothing
		Try
			Dim smBL As New SmBL()
			Dim smAuthBL As New SmAuthBL()
			Dim userRoleID As Guid = Common.SafeGUID(Request.Form("userRoleID"))
			Dim comment As String = Common.SafeString(Request.Form("comment"))
			Dim ipAddress As String = Request.ServerVariables("REMOTE_ADDR")
			Dim backOfficeLenderID As Guid = Common.SafeGUID(Request.Form("bid"))
			Dim backOfficeLender As SmBackOfficeLender = smBL.GetBackOfficeLenderByID(backOfficeLenderID)
			Dim userRole As SmUserRole = smBL.GetUserRoleByID(userRoleID)
			Dim u As SmUser = Nothing
			If userRole IsNot Nothing Then
				u = smAuthBL.GetUserByID(userRole.UserID)
			End If
			If backOfficeLender IsNot Nothing AndAlso userRole IsNot Nothing AndAlso u IsNot Nothing Then
				Dim result = smBL.RemoveLenderUserRole(userRoleID, comment, backOfficeLender, u, UserInfo.UserID, ipAddress)
				If result = "ok" Then
					res = New CJsonResponse(True, "", IIf(u.UserID = UserInfo.UserID, "RELOAD", "OK"))
				Else
					res = New CJsonResponse(False, "", "FAILED")
				End If
			Else
				Response.StatusCode = 404 '' Forbidden
				Response.StatusDescription = "Not found"
				Response.End() '' Terminate all the rest processes
			End If
		Catch ex As Exception
			_log.Error("Could not RemoveLenderUserRole.", ex)
			res = New CJsonResponse(False, "Could not remove lender user role. Please try again", "FAILED")
		End Try
		Response.Write(JsonConvert.SerializeObject(res))
		Response.End()
	End Sub

	Private Sub RemovePortalUserRole()
		Dim res As CJsonResponse = Nothing
		Try
			Dim smBL As New SmBL()
			Dim smAuthBL As New SmAuthBL()
			Dim userRoleID As Guid = Common.SafeGUID(Request.Form("userRoleID"))
			Dim comment As String = Common.SafeString(Request.Form("comment"))
			Dim ipAddress As String = Request.ServerVariables("REMOTE_ADDR")
			Dim lenderConfigID As Guid = Common.SafeGUID(Request.Form("lenderConfigID"))
			Dim lenderConfig As SmLenderConfigBasicInfo = smBL.GetPortalBasicInfoByLenderConfigID(lenderConfigID)
			Dim userRole As SmUserRole = smBL.GetUserRoleByID(userRoleID)
			Dim u As SmUser = Nothing
			If userRole IsNot Nothing Then
				u = smAuthBL.GetUserByID(userRole.UserID)
			End If
			If lenderConfig IsNot Nothing AndAlso userRole IsNot Nothing AndAlso u IsNot Nothing Then
				Dim result = smBL.RemovePortalUserRole(userRoleID, comment, lenderConfig, u, UserInfo.UserID, ipAddress)
				If result = "ok" Then
					res = New CJsonResponse(True, "", IIf(u.UserID = UserInfo.UserID, "RELOAD", "OK"))
				Else
					res = New CJsonResponse(False, "", "FAILED")
				End If
			Else
				Response.StatusCode = 404 '' Forbidden
				Response.StatusDescription = "Not found"
				Response.End() '' Terminate all the rest processes
			End If
		Catch ex As Exception
			_log.Error("Could not RemovePortalUserRole.", ex)
			res = New CJsonResponse(False, "Could not remove portal user role. Please try again", "FAILED")
		End Try
		Response.Write(JsonConvert.SerializeObject(res))
		Response.End()
	End Sub

	Private Sub GetAllLenderUsersForSuggest()
		Try
			Dim smBL As New SmBL()
			Dim pagingInfo As New SmPagingInfoModel With {.PageIndex = 0, .PageSize = 0} 'get all
			Dim filter As New SmLenderUserListFilter()
			filter.Status.Add(SmSettings.UserStatus.Active)
			filter.Status.Add(SmSettings.UserStatus.InActive)
			filter.LenderID = Common.SafeGUID(Request.Form("lenderID"))
			filter.Keyword = Common.SafeStripHtmlString(Request.Form("query"))
			If filter IsNot Nothing Then
				filter = filter.MakeItSafe()
			End If
			Dim lenders As List(Of SmUser) = smBL.GetLenderUsers(pagingInfo, filter)
			Response.Clear()
			Response.Write(JsonConvert.SerializeObject(lenders.Select(Function(l) New With {.id = l.Email, .name = l.Email})))
		Catch ex As Exception
			_log.Error("Could not get all lenders users for search", ex)
			Response.Clear()
			Response.Write("")
		End Try
	End Sub

	Private Sub AddLenderUser()
		Dim res As CJsonResponse
		Try
			Dim smBL As New SmBL()
			Dim smAuthBL As New SmAuthBL()
			Dim email As String = Common.SafeStripHtmlString(Request.Form("email"))
			Dim ipAddress As String = Request.ServerVariables("REMOTE_ADDR")
			Dim backOfficeLenderID As Guid = Common.SafeGUID(Request.Form("bid"))
			Dim backOfficeLender As SmBackOfficeLender = smBL.GetBackOfficeLenderByID(backOfficeLenderID)

			If backOfficeLender IsNot Nothing Then
				Dim u As SmUser = smAuthBL.GetUserByEmail(email)
				If u IsNot Nothing Then
					Dim roleList = smBL.GetLenderUserRoleList(u.UserID, backOfficeLender.LenderID)
					If roleList Is Nothing OrElse roleList.Count = 0 Then
						'Add record to UserRoles table
						Dim lenderUserRole As New SmUserRole
						lenderUserRole.UserRoleID = Guid.NewGuid()
						lenderUserRole.Role = SmSettings.Role.LenderAdmin
						lenderUserRole.LenderID = backOfficeLender.LenderID
						lenderUserRole.UserID = u.UserID
						Dim result = smBL.AddUserRole(lenderUserRole, UserInfo.UserID, ipAddress, True)
						If result = "ok" Then
							Dim emailContent As String = SmUtil.BuildLenderAdminRoleAssignedNotification(u.FirstName, backOfficeLender.LenderName)
							SmUtil.SendEmail(u.Email, "MeridianLink's APM Role Assign Notification", emailContent)
							res = New CJsonResponse(True, "", "OK")
						Else
							res = New CJsonResponse(False, result, "FAILED")
						End If
					ElseIf roleList.Any(Function(r) r.Role = SmSettings.Role.LenderAdmin) Then
						res = New CJsonResponse(False, String.Format("This email has been used by an user who is in role {0} already.", SmSettings.Role.LenderAdmin.GetEnumDescription()), "ALREADY")
					ElseIf roleList.Any(Function(r) r.Role = SmSettings.Role.PortalAdmin) Then
						res = New CJsonResponse(False, String.Format("This email has been used by an user who is in role {0}. Are you sure about upgrading this user to {1}?", SmSettings.Role.PortalAdmin.GetEnumDescription(), SmSettings.Role.LenderAdmin.GetEnumDescription()), "UPGRADE")
					ElseIf roleList.Any(Function(r) r.Role = SmSettings.Role.VendorGroupAdmin) Then
						res = New CJsonResponse(False, String.Format("This email has been used by an user who is in role {0}. Are you sure about upgrading this user to {1}?", SmSettings.Role.VendorGroupAdmin.GetEnumDescription(), SmSettings.Role.LenderAdmin.GetEnumDescription()), "UPGRADE")
					Else
						res = New CJsonResponse(False, String.Format("This email has been used by an user who is not in role {0}. Are you sure about granting the {0} role this user?", SmSettings.Role.LenderAdmin.GetEnumDescription()), "UPGRADE")
					End If
				Else
					Dim token As String = Guid.NewGuid().ToString().Replace("-", "").Substring(0, 20)
					Dim newUser As New SmUser()
					newUser.UserID = Guid.NewGuid()
					newUser.ExpireDays = 0 'by default
					newUser.Role = SmSettings.Role.User
					newUser.Email = email
					Dim result = smAuthBL.AddUser(newUser, UserInfo.UserID, ipAddress, token)
					If result = "ok" Then
						Dim lenderUserRole As New SmUserRole
						lenderUserRole.UserRoleID = Guid.NewGuid()
						lenderUserRole.Role = SmSettings.Role.LenderAdmin
						lenderUserRole.LenderID = backOfficeLender.LenderID
						lenderUserRole.UserID = newUser.UserID
						smBL.AddUserRole(lenderUserRole, UserInfo.UserID, ipAddress)
						Dim emailContent As String = SmUtil.BuildActivateAccountEmail(token, _serverRoot)
						SmUtil.SendEmail(newUser.Email, "MeridianLink's APM Account Activation", emailContent)
						'not sure why we need to send this 2nd email
						'emailContent = SmUtil.BuildLenderAdminRoleAssignedNotification(backOfficeLender.LenderName)
						'SmUtil.SendEmail(newUser.Email, "MeridianLink's APM Role Assign Notification", emailContent)
						res = New CJsonResponse(True, "", "OK")
					Else
						res = New CJsonResponse(False, "", "FAILED")
					End If
				End If
			Else
				Response.StatusCode = 404 '' Forbidden
				Response.StatusDescription = "Not found"
				Response.End() '' Terminate all the rest processes
				Return
			End If
		Catch ex As Exception
			_log.Error("Could not AddLenderUser.", ex)
			res = New CJsonResponse(False, "Could not add lender config user. Please try again", "FAILED")
		End Try
		Response.Write(JsonConvert.SerializeObject(res))
		Response.End()
	End Sub

	Private Sub AddPortalUser()
		Dim res As CJsonResponse
		Try
			Dim smBL As New SmBL()
			Dim smAuthBL As New SmAuthBL()
			Dim email As String = Common.SafeStripHtmlString(Request.Form("email"))
			Dim ipAddress As String = Request.ServerVariables("REMOTE_ADDR")
			Dim lenderConfigID As Guid = Common.SafeGUID(Request.Form("lenderConfigID"))
			Dim lenderConfig As SmLenderConfigBasicInfo = smBL.GetPortalBasicInfoByLenderConfigID(lenderConfigID)
			Dim backOfficeLender As SmBackOfficeLender
			If lenderConfig IsNot Nothing Then
				backOfficeLender = smBL.GetBackOfficeLenderByLenderConfigID(lenderConfig.LenderConfigID)
				Dim u As SmUser = smAuthBL.GetUserByEmail(email)
				If u IsNot Nothing Then
					Dim roleList = smBL.GetLenderUserRoleList(u.UserID, lenderConfig.LenderID)
					If roleList Is Nothing OrElse roleList.Count = 0 Then
						'Add record to UserRoles table
						Dim portalUserRole As New SmUserRole
						portalUserRole.UserRoleID = Guid.NewGuid()
						portalUserRole.Role = SmSettings.Role.PortalAdmin
						portalUserRole.LenderID = lenderConfig.LenderID
						portalUserRole.LenderConfigID = lenderConfig.LenderConfigID
						portalUserRole.UserID = u.UserID
						Dim result = smBL.AddUserRole(portalUserRole, UserInfo.UserID, ipAddress, True)

						If result = "ok" Then
							Dim emailContent As String = SmUtil.BuildPortalAdminRoleAssignedNotification(u.FirstName, backOfficeLender.LenderName, lenderConfig.Note)
							SmUtil.SendEmail(u.Email, "MeridianLink's APM Role Assign Notification", emailContent)
							res = New CJsonResponse(True, "", "OK")
						Else
							res = New CJsonResponse(False, result, "FAILED")
						End If
					ElseIf roleList.Any(Function(r) r.Role = SmSettings.Role.PortalAdmin) Then
						If roleList.Any(Function(r) r.Role = SmSettings.Role.PortalAdmin And r.LenderConfigID = lenderConfigID) Then
							res = New CJsonResponse(False, String.Format("This email has been used by an user who is in role {0} already.", SmSettings.Role.PortalAdmin.GetEnumDescription()), "ALREADY")
						Else
							'Add record to UserRoles table
							Dim portalUserRole As New SmUserRole
							portalUserRole.UserRoleID = Guid.NewGuid()
							portalUserRole.Role = SmSettings.Role.PortalAdmin
							portalUserRole.LenderID = lenderConfig.LenderID
							portalUserRole.LenderConfigID = lenderConfig.LenderConfigID
							portalUserRole.UserID = u.UserID
							Dim result = smBL.AddUserRole(portalUserRole, UserInfo.UserID, ipAddress, True)

							If result = "ok" Then
								backOfficeLender = smBL.GetBackOfficeLenderByLenderConfigID(lenderConfig.LenderConfigID)
								Dim emailContent As String = SmUtil.BuildPortalAdminRoleAssignedNotification(u.FirstName, backOfficeLender.LenderName, lenderConfig.Note)
								SmUtil.SendEmail(u.Email, "MeridianLink's APM Role Assign Notification", emailContent)
								res = New CJsonResponse(True, "", "OK")
							Else
								res = New CJsonResponse(False, result, "FAILED")
							End If
						End If
					ElseIf roleList.Any(Function(r) r.Role = SmSettings.Role.LenderAdmin) Then
						If SmUtil.CheckRoleAccess(HttpContext.Current, SmSettings.Role.SuperUser.ToString(), String.Format("LenderScope${0}${1}", SmSettings.Role.LenderAdmin.ToString(), backOfficeLender.LenderID.ToString())) Then
							res = New CJsonResponse(False, String.Format("This email has been used with role of {1}. Are you sure about downgrading this user to {0}?", SmSettings.Role.PortalAdmin.GetEnumDescription(), SmSettings.Role.LenderAdmin.GetEnumDescription()), "DOWNGRADE")
						Else
							res = New CJsonResponse(False, String.Format("This email has been used by an user who is in role {0} already.", SmSettings.Role.LenderAdmin.GetEnumDescription()), "ALREADY")
						End If
					ElseIf roleList.Any(Function(r) r.Role = SmSettings.Role.VendorGroupAdmin) Then
						res = New CJsonResponse(False, String.Format("This email has been used with role of {1}. Are you sure about downgrading this user to {0}?", SmSettings.Role.PortalAdmin.GetEnumDescription(), SmSettings.Role.VendorGroupAdmin.GetEnumDescription()), "UPGRADE")
					Else
						res = New CJsonResponse(False, String.Format("This email has been used by an user who is not in role {0}. Are you sure about granting the {0} role to this user?", SmSettings.Role.PortalAdmin.GetEnumDescription()), "UPGRADE")
					End If
				Else 'new user in the system
					Dim token As String = Guid.NewGuid().ToString().Replace("-", "").Substring(0, 20)
					Dim newUser As New SmUser()
					newUser.UserID = Guid.NewGuid()
					newUser.ExpireDays = 0 'by default
					newUser.Role = SmSettings.Role.User
					newUser.Email = email
					Dim result = smAuthBL.AddUser(newUser, UserInfo.UserID, ipAddress, token)
					If result = "ok" Then
						Dim lenderUserRole As New SmUserRole
						lenderUserRole.UserRoleID = Guid.NewGuid()
						lenderUserRole.Role = SmSettings.Role.PortalAdmin
						lenderUserRole.LenderConfigID = lenderConfig.LenderConfigID
						lenderUserRole.LenderID = lenderConfig.LenderID
						lenderUserRole.UserID = newUser.UserID
						smBL.AddUserRole(lenderUserRole, UserInfo.UserID, ipAddress)
						Dim emailContent As String = SmUtil.BuildActivateAccountEmail(token, _serverRoot)
						SmUtil.SendEmail(newUser.Email, "MeridianLink's APM Account Activation", emailContent)

						'not sure why system need to send this 2nd notification, Firstname is nost yet available
						'emailContent = SmUtil.BuildPortalAdminRoleAssignedNotification(u.FirstName, backOfficeLender.LenderName, lenderConfig.Note)
						'SmUtil.SendEmail(newUser.Email, "MeridianLink's APM Role Assign Notification", emailContent)
						res = New CJsonResponse(True, "", "OK")
					Else
						res = New CJsonResponse(False, "", "FAILED")
					End If
				End If
			Else
				Response.StatusCode = 404 '' Forbidden
				Response.StatusDescription = "Not found"
				Response.End() '' Terminate all the rest processes
				Return
			End If
		Catch ex As Exception
			_log.Error("Could not AddPortalUser.", ex)
			res = New CJsonResponse(False, "Could not add portal user. Please try again", "FAILED")
		End Try
		Response.Write(JsonConvert.SerializeObject(res))
		Response.End()
	End Sub
	Private Sub SaveLenderInfo()
		Dim res As CJsonResponse = Nothing
		Try
			Dim smBL As New SmBL()
			Dim backOfficeLenderObj = JsonConvert.DeserializeObject(Of SmBackOfficeLender)(Common.SafeString(Request.Form("lender_data")), New JsonSerializerSettings() With {.NullValueHandling = NullValueHandling.Ignore}).MakeItSafe()

			If Not String.IsNullOrEmpty(backOfficeLenderObj.CustomerInternalID) AndAlso smBL.CheckCustomerInternalIDInUse(backOfficeLenderObj.CustomerInternalID, backOfficeLenderObj.BackOfficeLenderID) Then
				res = New CJsonResponse(False, "Customer Internal ID is in use", "FAILED")
			Else
				Dim backOfficeLender As SmBackOfficeLender = smBL.GetBackOfficeLenderByID(backOfficeLenderObj.BackOfficeLenderID)
				If backOfficeLenderObj.BackOfficeLenderID = Guid.Empty Then	'create new entry bc backofficeLender hasn't been created for this lenderconfig 
					backOfficeLenderObj.BackOfficeLenderID = Guid.NewGuid
					Dim ipAddress As String = Request.ServerVariables("REMOTE_ADDR")
					Dim result = smBL.AddBackOfficeLenderInfo(backOfficeLenderObj, UserInfo.UserID, ipAddress)
					If result = "ok" Then
						res = New CJsonResponse(True, "", "OK")
					Else
						res = New CJsonResponse(False, "", "FAILED")
					End If
				ElseIf backOfficeLender Is Nothing Then
					Response.StatusCode = 404 '' Forbidden
					Response.StatusDescription = "Not found"
					Response.End() '' Terminate all the rest processes
				Else
					Dim ipAddress As String = Request.ServerVariables("REMOTE_ADDR")
					backOfficeLender.LenderName = backOfficeLenderObj.LenderName
					backOfficeLender.City = backOfficeLenderObj.City
					backOfficeLender.CustomerInternalID = backOfficeLenderObj.CustomerInternalID
					backOfficeLender.State = backOfficeLenderObj.State
					Dim result = smBL.UpdateBackOfficeLenderInfo(backOfficeLender, UserInfo.UserID, ipAddress)
					If result = "ok" Then
						res = New CJsonResponse(True, "OK", New With {.name = backOfficeLender.LenderName})
					Else
						res = New CJsonResponse(False, "FAILED", "")
					End If
				End If
			End If

		Catch ex As Exception
			_log.Error("Could not SaveLenderInfo.", ex)
			res = New CJsonResponse(False, "Could not add/update back office lender info. Please try again", "FAILED")
		End Try
		Response.Write(JsonConvert.SerializeObject(res))
		Response.End()
	End Sub

	Private Sub SaveLenderSecurityConfig()
		Dim res As CJsonResponse = Nothing
		Try
			Dim smBL As New SmBL()
			Dim backOfficeLender As SmBackOfficeLender = smBL.GetBackOfficeLenderByID(Common.SafeGUID(Request.Form("bid")))

			If backOfficeLender Is Nothing Then
				Response.StatusCode = 404 '' Forbidden
				Response.StatusDescription = "Not found"
				Response.End() '' Terminate all the rest processes
			Else
				Dim countriesAllowedAccessJson As String = Common.SafeString(Request.Form("CountriesAllowedAccess"))
				Dim excludedIPJson As String = Common.SafeString(Request.Form("ExcludedIP"))
				Dim submissionJson As String = Common.SafeString(Request.Form("Submission"))
				Dim instaTouchJson As String = Common.SafeString(Request.Form("InstaTouch"))
				Dim revisions As Dictionary(Of String, Integer) = JsonConvert.DeserializeObject(Of Dictionary(Of String, Integer))(Common.SafeString(Request.Form("revisions")))
				Dim ipAddress As String = Request.ServerVariables("REMOTE_ADDR")
				Dim forceSaving As Boolean = Common.SafeBoolean(Request.Form("forceSaving"))

				Dim configNames As New List(Of String)
				configNames.Add(GlobalConfigDictionary.GENERAL_COUNTRIES_ALLOWED_ACCESS)
				configNames.Add(GlobalConfigDictionary.GENERAL_EXCLUDED_IP)
				configNames.Add(GlobalConfigDictionary.GENERAL_INSTATOUCH)
				configNames.Add(GlobalConfigDictionary.GENERAL_SUBMISSION)
				Dim configData = smBL.ReadGlobalConfigItems(configNames, backOfficeLender.LenderID)
				Dim curRevisionList = New Dictionary(Of String, Integer) From {
					{GlobalConfigDictionary.GENERAL_COUNTRIES_ALLOWED_ACCESS, 0},
					{GlobalConfigDictionary.GENERAL_EXCLUDED_IP, 0},
					{GlobalConfigDictionary.GENERAL_INSTATOUCH, 0},
					{GlobalConfigDictionary.GENERAL_SUBMISSION, 0}
				}
				
				If configData IsNot Nothing AndAlso configData.ContainsKey(GlobalConfigDictionary.GENERAL_COUNTRIES_ALLOWED_ACCESS) Then
					curRevisionList(GlobalConfigDictionary.GENERAL_COUNTRIES_ALLOWED_ACCESS) = configData(GlobalConfigDictionary.GENERAL_COUNTRIES_ALLOWED_ACCESS).RevisionID
				End If

				If configData IsNot Nothing AndAlso configData.ContainsKey(GlobalConfigDictionary.GENERAL_EXCLUDED_IP) Then
					curRevisionList(GlobalConfigDictionary.GENERAL_EXCLUDED_IP) = configData(GlobalConfigDictionary.GENERAL_EXCLUDED_IP).RevisionID
				End If

				If configData IsNot Nothing AndAlso configData.ContainsKey(GlobalConfigDictionary.GENERAL_INSTATOUCH) Then
					curRevisionList(GlobalConfigDictionary.GENERAL_INSTATOUCH) = configData(GlobalConfigDictionary.GENERAL_INSTATOUCH).RevisionID
				End If

				If configData IsNot Nothing AndAlso configData.ContainsKey(GlobalConfigDictionary.GENERAL_SUBMISSION) Then
					curRevisionList(GlobalConfigDictionary.GENERAL_SUBMISSION) = configData(GlobalConfigDictionary.GENERAL_SUBMISSION).RevisionID
				End If

				If Not forceSaving AndAlso CheckOutOfDateData(curRevisionList, revisions) Then
					res = New CJsonResponse(False, "", "OUTOFDATE")
				Else
					Dim configItems As New Dictionary(Of String, SmGlobalConfigItem)
					'increase revision
					Dim newRevisions As Dictionary(Of String, Integer) = revisions.ToDictionary(Function(p) p.Key, Function(p) p.Value + 1)
					configItems.Add(GlobalConfigDictionary.GENERAL_COUNTRIES_ALLOWED_ACCESS, New SmGlobalConfigItem() With {
									.ID = Guid.NewGuid(),
									.LenderID = backOfficeLender.LenderID,
									.RevisionID = newRevisions(GlobalConfigDictionary.GENERAL_COUNTRIES_ALLOWED_ACCESS),
									.ConfigJson = countriesAllowedAccessJson,
									.Name = GlobalConfigDictionary.GENERAL_COUNTRIES_ALLOWED_ACCESS,
									.LastModifiedDate = DateTime.Now})

					configItems.Add(GlobalConfigDictionary.GENERAL_EXCLUDED_IP, New SmGlobalConfigItem() With {
									.ID = Guid.NewGuid(),
									.LenderID = backOfficeLender.LenderID,
									.RevisionID = newRevisions(GlobalConfigDictionary.GENERAL_EXCLUDED_IP),
									.ConfigJson = excludedIPJson,
									.Name = GlobalConfigDictionary.GENERAL_EXCLUDED_IP,
									.LastModifiedDate = DateTime.Now})
					configItems.Add(GlobalConfigDictionary.GENERAL_SUBMISSION, New SmGlobalConfigItem() With {
									.ID = Guid.NewGuid(),
									.LenderID = backOfficeLender.LenderID,
									.RevisionID = newRevisions(GlobalConfigDictionary.GENERAL_SUBMISSION),
									.ConfigJson = submissionJson,
									.Name = GlobalConfigDictionary.GENERAL_SUBMISSION,
									.LastModifiedDate = DateTime.Now})
					configItems.Add(GlobalConfigDictionary.GENERAL_INSTATOUCH, New SmGlobalConfigItem() With {
									.ID = Guid.NewGuid(),
									.LenderID = backOfficeLender.LenderID,
									.RevisionID = newRevisions(GlobalConfigDictionary.GENERAL_INSTATOUCH),
									.ConfigJson = instaTouchJson,
									.Name = GlobalConfigDictionary.GENERAL_INSTATOUCH,
									.LastModifiedDate = DateTime.Now})
					Dim result = smBL.WriteGlobalConfigItems(configItems, backOfficeLender.LenderID, UserInfo.UserID, ipAddress)
					If result Then
						res = New CJsonResponse(True, "OK", newRevisions)
					Else
						res = New CJsonResponse(False, "FAILED", "")
					End If
				End If
			End If

		Catch ex As Exception
			_log.Error("Could not saveLenderSecurityConfig.", ex)
			res = New CJsonResponse(False, "Could not save security config data. Please try again", "FAILED")
		End Try
		Response.Write(JsonConvert.SerializeObject(res))
		Response.End()
	End Sub

	Private Function CheckOutOfDateData(sourceList As Dictionary(Of String, Integer), targetList As Dictionary(Of String, Integer)) As Boolean
		Return sourceList.Any(Function(item) targetList.ContainsKey(item.Key) AndAlso item.Value <> targetList(item.Key))
	End Function

	Private Sub SavePortalInfo()
		Dim res As CJsonResponse
		Try
			Dim smBL As New SmBL()
			Dim portalObj = JsonConvert.DeserializeObject(Of SmLenderConfig)(Common.SafeString(Request.Form("portal_data")), New JsonSerializerSettings() With {.NullValueHandling = NullValueHandling.Ignore}).MakeItSafe()
			Dim lenderConfig = smBL.GetPortalBasicInfoByLenderConfigID(portalObj.LenderConfigID)

			If lenderConfig Is Nothing Then
				Response.StatusCode = 404 '' Forbidden
				Response.StatusDescription = "Not found"
				Response.End() '' Terminate all the rest processes
				Return
			Else
				Dim existedPortalWithLenderRef As SmLenderConfig = smBL.GetPortalByLenderRef(portalObj.LenderRef, pbDecrypt:=False)
				If existedPortalWithLenderRef IsNot Nothing AndAlso existedPortalWithLenderRef.LenderConfigID <> portalObj.LenderConfigID Then
					res = New CJsonResponse(False, "Lender ref has been used by another portal. Please chooose another lender ref", "EXISTED")
				ElseIf Not Common.IsValidLenderRef(portalObj.LenderRef) Then
					res = New CJsonResponse(False, "Error: Invalid Lender Ref")
				Else
					Dim ipAddress As String = Request.ServerVariables("REMOTE_ADDR")
					lenderConfig.Note = portalObj.Note
					lenderConfig.LenderRef = portalObj.LenderRef
					Dim result = smBL.UpdateLenderConfigInfo(lenderConfig, UserInfo.UserID, ipAddress)
					If result = "ok" Then
						res = New CJsonResponse(True, "OK", New With {.name = portalObj.Note, .ref = portalObj.LenderRef})
					Else
						res = New CJsonResponse(False, "FAILED", "")
					End If
				End If

			End If

		Catch ex As Exception
			_log.Error("Could not SavePortalInfo.", ex)
			res = New CJsonResponse(False, "Could not update lender config info. Please try again", "FAILED")
		End Try
		Response.Write(JsonConvert.SerializeObject(res))
		Response.End()
	End Sub

	Private Sub LoadCloneLenderForm()
		Try
			Dim control As Sm_Uc_CloneLenderForm = CType(LoadControl("~/Sm/Uc/CloneLenderForm.ascx"), Sm_Uc_CloneLenderForm)
			Dim lenderConfigID As Guid = Common.SafeGUID(Request.Form("lenderConfigID"))
			Dim lenderConfig As SmLenderConfigBasicInfo = New SmBL().GetPortalBasicInfoByLenderConfigID(lenderConfigID)
			If lenderConfigID = Guid.Empty OrElse lenderConfig Is Nothing Then
				Response.StatusCode = 404 '' Forbidden
				Response.StatusDescription = "Not found"
				Response.End() '' Terminate all the rest processes
			End If
			control.LenderConfig = lenderConfig
			Me.Controls.Add(control)
		Catch ex As Exception
			_log.Error("Could not load clone lender form", ex)
			Response.Write("<h3 style='text-align: center; color: red;'>Unable to render data. Please try again later</h3>")
        End Try
	End Sub

	Private Sub LoadClonePortalForm()
		Try
			Dim control As Sm_Uc_ClonePortalForm = CType(LoadControl("~/Sm/Uc/ClonePortalForm.ascx"), Sm_Uc_ClonePortalForm)
			Dim lenderConfigID As Guid = Common.SafeGUID(Request.Form("lenderConfigID"))
			Dim lenderConfig As SmLenderConfig = New SmBL().GetPortalByLenderConfigID(lenderConfigID)
			If lenderConfigID = Guid.Empty OrElse lenderConfig Is Nothing Then
				Response.StatusCode = 404 '' Forbidden
				Response.StatusDescription = "Not found"
				Response.End() '' Terminate all the rest processes
			End If
			control.LenderConfig = lenderConfig
			Me.Controls.Add(control)
		Catch ex As Exception
			_log.Error("Could not load clone portal form", ex)
			Response.Write("<h3 style='text-align: center; color: red;'>Unable to render data. Please try again later</h3>")
		End Try
	End Sub

	Private Sub CloneLender()
		Dim res As CJsonResponse = Nothing
		Try
			Dim smBL As New SmBL()
			Dim cloneLenderModelObj = JsonConvert.DeserializeObject(Of SmCloneLenderModel)(Common.SafeString(Request.Form("post_data")), New JsonSerializerSettings() With {.NullValueHandling = NullValueHandling.Ignore}).MakeItSafe()
			Dim lenderConfig As SmLenderConfig = smBL.GetPortalByLenderConfigID(cloneLenderModelObj.LenderConfigID)

			If lenderConfig Is Nothing Then
				Response.StatusCode = 404 '' Forbidden
				Response.StatusDescription = "Not found"
				Response.End() '' Terminate all the rest processes
			Else
				If smBL.CheckLenderExistedByLenderRef(cloneLenderModelObj.LenderRef) Then
					res = New CJsonResponse(False, "Lender Ref has been taken. Please choose another one", "EXISTED")
				Else
					Dim result = smBL.CloneLender(cloneLenderModelObj, lenderConfig, UserInfo, Request.ServerVariables("REMOTE_ADDR"))
					If result = "ok" Then
						res = New CJsonResponse(True, "OK", "")
					Else
						res = New CJsonResponse(False, "FAILED", "")
					End If
				End If
			End If
		Catch ex As Exception
			_log.Error("Could not CloneLender.", ex)
			res = New CJsonResponse(False, "Could not clone lender. Please try again", "FAILED")
		End Try
		Response.Write(JsonConvert.SerializeObject(res))
		Response.End()
	End Sub

	Private Sub ClonePortal()
		Dim res As CJsonResponse = Nothing
		Try
			Dim smBL As New SmBL()
			Dim clonePortalModelObj = JsonConvert.DeserializeObject(Of SmClonePortalModel)(Common.SafeString(Request.Form("post_data")), New JsonSerializerSettings() With {.NullValueHandling = NullValueHandling.Ignore}).MakeItSafe()
			Dim lenderConfig As SmLenderConfig = smBL.GetPortalByLenderConfigID(clonePortalModelObj.LenderConfigID)

			If lenderConfig Is Nothing Then
				Response.StatusCode = 404 '' Forbidden
				Response.StatusDescription = "Not found"
				Response.End() '' Terminate all the rest processes
			Else
				If smBL.CheckLenderExistedByLenderRef(clonePortalModelObj.LenderRef) Then
					res = New CJsonResponse(False, "Lender Ref has been taken. Please choose another one", "EXISTED")
				Else
					Dim result = smBL.ClonePortal(clonePortalModelObj, lenderConfig, UserInfo, Request.ServerVariables("REMOTE_ADDR"))
					If result = "ok" Then
						res = New CJsonResponse(True, "OK", "")
					Else
						res = New CJsonResponse(False, "FAILED", "")
					End If
				End If
			End If
		Catch ex As Exception
			_log.Error("Could not ClonePortal.", ex)
			res = New CJsonResponse(False, "Could not clone portal. Please try again", "FAILED")
		End Try
		Response.Write(JsonConvert.SerializeObject(res))
		Response.End()
	End Sub
#End Region

#Region "Renameable"


	Private Sub PublishRenameItemDraft()
		Dim res As New CJsonResponse()
		Try
			Dim smBL As New SmBL()
			Dim lenderConfigID As Guid = Common.SafeGUID(Request.Form("lenderconfigid"))
			Dim loanType = Common.SafeStripHtmlString(Request.Form("loantype"))
			Dim renameableList = JsonConvert.DeserializeObject(Of List(Of SmRenameableItemModel))(Common.SafeString(Request.Form("renameablelist")), New JsonSerializerSettings() With {.NullValueHandling = NullValueHandling.Ignore})
			Dim nodeGroup As SmSettings.NodeGroup
			'For backward compatibility and avoid the need for migration script, use values “xa_special” and “sa_special” for loan_type attribute for both XA Special and XA Minor - don't use values “xa_minor” or “sa_minor”
			'This may have the side effect that a change in XA Special may leak to XA Minor. We would have to ask client to use as-is.
			Dim convertedLoanType As String = loanType
			Select Case loanType.ToLower()
				Case "xa"
					nodeGroup = SmSettings.NodeGroup.ICERenameXA
				Case "cc"
					nodeGroup = SmSettings.NodeGroup.ICERenameCC
				Case "he"
					nodeGroup = SmSettings.NodeGroup.ICERenameHE
				Case "vl"
					nodeGroup = SmSettings.NodeGroup.ICERenameVL
				Case "bl"
					nodeGroup = SmSettings.NodeGroup.ICERenameBL
				Case "pl"
					nodeGroup = SmSettings.NodeGroup.ICERenamePL
				Case "sa"
					nodeGroup = SmSettings.NodeGroup.ICERenameSA
				Case "cc_combo"
					nodeGroup = SmSettings.NodeGroup.ICERenameCCCombo
				Case "he_combo"
					nodeGroup = SmSettings.NodeGroup.ICERenameHECombo
				Case "vl_combo"
					nodeGroup = SmSettings.NodeGroup.ICERenameVLCombo
				Case "pl_combo"
					nodeGroup = SmSettings.NodeGroup.ICERenamePLCombo
				Case "lp"
					nodeGroup = SmSettings.NodeGroup.ICERenameLanding
				Case "xa_special"
					nodeGroup = SmSettings.NodeGroup.ICERenameXASpecial
				Case "sa_special"
					nodeGroup = SmSettings.NodeGroup.ICERenameSASpecial
				Case "xa_business"
					nodeGroup = SmSettings.NodeGroup.ICERenameXABusiness
				Case "sa_business"
					nodeGroup = SmSettings.NodeGroup.ICERenameSABusiness
				Case "xa_minor"
					convertedLoanType = "XA_SPECIAL"
					nodeGroup = SmSettings.NodeGroup.ICERenameXASpecial
				Case "sa_minor"
					convertedLoanType = "SA_SPECIAL"
					nodeGroup = SmSettings.NodeGroup.ICERenameSASpecial
				Case "st"
					nodeGroup = SmSettings.NodeGroup.ICERenameCheckStatus
				Case "lqb_main"
					nodeGroup = SmSettings.NodeGroup.ICERenameLQBMain
				Case Else
					nodeGroup = SmSettings.NodeGroup.Undefinded
			End Select
			If nodeGroup = SmSettings.NodeGroup.Undefinded Then
				Throw New InvalidDataException("Bad Data")
			End If

			Dim lenderConfig = smBL.GetLiveConfig(lenderConfigID)
			If lenderConfig Is Nothing Then
				Response.StatusCode = 404 '' Forbidden
				Response.StatusDescription = "Not found"
				Response.End() '' Terminate all the rest processes
			End If
			Dim apmComboEnable As Boolean = SmUtil.GetApmComboEnable(lenderConfig.ConfigData)
			If apmComboEnable = False AndAlso Regex.IsMatch(loanType.ToUpper(), "^(CC_COMBO|HE_COMBO|PL_COMBO|VL_COMBO)$") Then
				Response.StatusCode = 404 '' Forbidden
				Response.StatusDescription = "Not found"
				Response.End() '' Terminate all the rest processes
			End If



			Dim nodeBatch As New SmLenderConfigDraft() With {
				.LenderConfigID = lenderConfigID,
				.CreatedByUserID = UserInfo.UserID,
				.NodeGroup = nodeGroup,
				.NodePath = RENAME_BUTTON_LABEL_ITEMS(convertedLoanType),
				.NodeContent = SmUtil.BuildRenameableNodeXml("RENAME_BUTTON_LABEL", convertedLoanType, renameableList)}
			Dim comment As String = Common.SafeStripHtmlString(Request.Form("comment"))
			Dim publishAll As Boolean = Common.SafeBoolean(Request.Form("publishAll"))
			Dim result = smBL.PublishNodes(lenderConfigID, UserInfo.UserID, Request.UserHostAddress, comment, publishAll, AddressOf SmUtil.MergeRenameableItems, nodeBatch)
			If result Then
				res = New CJsonResponse(True, "", "OK")
			Else
				res = New CJsonResponse(False, "", "FAILED")
			End If
		Catch ex As Exception
			_log.Error("Could not PublishManageXADraft.", ex)
			res = New CJsonResponse(False, "Unable to publish data. Please try again", "FAILED")
		End Try
		Response.Write(JsonConvert.SerializeObject(res))
		Response.End()
	End Sub

	Private Sub SaveRenameItemDraft()
		Dim res As New CJsonResponse()
		Try
			Dim smBL As New SmBL()
			Dim draftRecordList As New List(Of SmLenderConfigDraft)
			Dim lenderConfigID As Guid = Common.SafeGUID(Request.Form("lenderconfigid"))
			Dim loanType = Common.SafeStripHtmlString(Request.Form("loantype"))
			Dim renameableList = JsonConvert.DeserializeObject(Of List(Of SmRenameableItemModel))(Common.SafeString(Request.Form("renameablelist")), New JsonSerializerSettings() With {.NullValueHandling = NullValueHandling.Ignore})
			Dim nodeGroup As SmSettings.NodeGroup
			'For backward compatibility and avoid the need for migration script, use values “xa_special” and “sa_special” for loan_type attribute for both XA Special and XA Minor - don't use values “xa_minor” or “sa_minor”
			'This may have the side effect that a change in XA Special may leak to XA Minor. We would have to ask client to use as-is.
			Dim convertedLoanType As String = loanType
			Select Case Common.SafeString(loanType).ToLower()
				Case "xa"
					nodeGroup = SmSettings.NodeGroup.ICERenameXA
				Case "cc"
					nodeGroup = SmSettings.NodeGroup.ICERenameCC
				Case "he"
					nodeGroup = SmSettings.NodeGroup.ICERenameHE
				Case "vl"
					nodeGroup = SmSettings.NodeGroup.ICERenameVL
				Case "bl"
					nodeGroup = SmSettings.NodeGroup.ICERenameBL
				Case "pl"
					nodeGroup = SmSettings.NodeGroup.ICERenamePL
				Case "sa"
					nodeGroup = SmSettings.NodeGroup.ICERenameSA
				Case "cc_combo"
					nodeGroup = SmSettings.NodeGroup.ICERenameCCCombo
				Case "he_combo"
					nodeGroup = SmSettings.NodeGroup.ICERenameHECombo
				Case "vl_combo"
					nodeGroup = SmSettings.NodeGroup.ICERenameVLCombo
				Case "pl_combo"
					nodeGroup = SmSettings.NodeGroup.ICERenamePLCombo
				Case "lp"
					nodeGroup = SmSettings.NodeGroup.ICERenameLanding
				Case "xa_special"
					nodeGroup = SmSettings.NodeGroup.ICERenameXASpecial
				Case "sa_special"
					nodeGroup = SmSettings.NodeGroup.ICERenameSASpecial
				Case "xa_business"
					nodeGroup = SmSettings.NodeGroup.ICERenameXABusiness
				Case "sa_business"
					nodeGroup = SmSettings.NodeGroup.ICERenameSABusiness
				Case "xa_minor"
					convertedLoanType = "XA_SPECIAL"
					nodeGroup = SmSettings.NodeGroup.ICERenameXASpecial
				Case "sa_minor"
					convertedLoanType = "SA_SPECIAL"
					nodeGroup = SmSettings.NodeGroup.ICERenameSASpecial
				Case "st"
					nodeGroup = SmSettings.NodeGroup.ICERenameCheckStatus
				Case "lqb_main"
					nodeGroup = SmSettings.NodeGroup.ICERenameLQBMain
				Case Else
					nodeGroup = SmSettings.NodeGroup.Undefinded
			End Select
			If nodeGroup = SmSettings.NodeGroup.Undefinded Then
				Throw New InvalidDataException("Bad Data")
			End If

			Dim lenderConfig = smBL.GetLiveConfig(lenderConfigID)
			If lenderConfig Is Nothing Then
				Response.StatusCode = 404 '' Forbidden
				Response.StatusDescription = "Not found"
				Response.End() '' Terminate all the rest processes
			End If
			Dim apmComboEnable As Boolean = SmUtil.GetApmComboEnable(lenderConfig.ConfigData)
			If apmComboEnable = False AndAlso Regex.IsMatch(loanType.ToUpper(), "^(CC_COMBO|HE_COMBO|PL_COMBO|VL_COMBO)$") Then
				Response.StatusCode = 404 '' Forbidden
				Response.StatusDescription = "Not found"
				Response.End() '' Terminate all the rest processes
			End If


			draftRecordList.Add(New SmLenderConfigDraft() With {
				.LenderConfigID = lenderConfigID,
				.CreatedByUserID = UserInfo.UserID,
				.NodeGroup = nodeGroup,
				.NodePath = RENAME_BUTTON_LABEL_ITEMS(convertedLoanType),
				.NodeContent = SmUtil.BuildRenameableNodeXml("RENAME_BUTTON_LABEL", convertedLoanType, renameableList)})
			Dim revisionID As Integer = Common.SafeInteger(Request.Form("revisionID"))
			Dim result = smBL.SaveNodesToDraft(revisionID, draftRecordList.ToArray())
			If result Then
				res = New CJsonResponse(True, "", "OK")
			Else
				res = New CJsonResponse(False, "", "FAILED")
			End If
		Catch ex As Exception
			_log.Error("Could not SaveRenameItemDraft.", ex)
			res = New CJsonResponse(False, "Unable to save draft. Please try again", "FAILED")
		End Try
		Response.Write(JsonConvert.SerializeObject(res))
		Response.End()
	End Sub
#End Region

#Region "Visibility"

	Private Sub SaveShowFieldItemsDraft()
		Dim res As New CJsonResponse()
		Try
			Dim smBL As New SmBL()
			Dim draftRecordList As New List(Of SmLenderConfigDraft)
			Dim lenderConfigID As Guid = Common.SafeGUID(Request.Form("lenderconfigid"))
			Dim loanType = Common.SafeStripHtmlString(Request.Form("loantype"))
			Dim showFieldItemList = JsonConvert.DeserializeObject(Of List(Of SmShowFieldItemModel))(Common.SafeString(Request.Form("showFieldItemList")), New JsonSerializerSettings() With {.NullValueHandling = NullValueHandling.Ignore})
			Dim nodeGroup As SmSettings.NodeGroup
			'For backward compatibility and avoid the need for migration script, use values “xa_special” and “sa_special” for loan_type attribute for both XA Special and XA Minor - don't use values “xa_minor” or “sa_minor”
			'This may have the side effect that a change in XA Special may leak to XA Minor. We would have to ask client to use as-is.
			Dim convertedLoanType As String = loanType
			Select Case Common.SafeString(loanType).ToLower()
				Case "xa"
					nodeGroup = SmSettings.NodeGroup.ShowFieldXA
				Case "sa"
					nodeGroup = SmSettings.NodeGroup.ShowFieldSA
				Case "he"
					nodeGroup = SmSettings.NodeGroup.ShowFieldHE
				Case "cc"
					nodeGroup = SmSettings.NodeGroup.ShowFieldCC
				Case "vl"
					nodeGroup = SmSettings.NodeGroup.ShowFieldVL
				Case "pl"
					nodeGroup = SmSettings.NodeGroup.ShowFieldPL
				Case "he_combo"
					nodeGroup = SmSettings.NodeGroup.ShowFieldHECombo
				Case "vl_combo"
					nodeGroup = SmSettings.NodeGroup.ShowFieldVLCombo
				Case "pl_combo"
					nodeGroup = SmSettings.NodeGroup.ShowFieldPLCombo
				Case "cc_combo"
					nodeGroup = SmSettings.NodeGroup.ShowFieldCCCombo
				Case "xa_special"
					nodeGroup = SmSettings.NodeGroup.ShowFieldXASpecial
				Case "sa_special"
					nodeGroup = SmSettings.NodeGroup.ShowFieldSASpecial
				Case "xa_minor"
					convertedLoanType = "XA_SPECIAL"
					nodeGroup = SmSettings.NodeGroup.ShowFieldXASpecial
				Case "sa_minor"
					convertedLoanType = "SA_SPECIAL"
					nodeGroup = SmSettings.NodeGroup.ShowFieldSASpecial
				Case "xa_business"
					nodeGroup = SmSettings.NodeGroup.ShowFieldXABusiness
				Case "sa_business"
					nodeGroup = SmSettings.NodeGroup.ShowFieldSABusiness
				Case "bl"
					nodeGroup = SmSettings.NodeGroup.ShowFieldBL
				Case "lqb_main"
					nodeGroup = SmSettings.NodeGroup.ShowFieldLQBMain
				Case Else
					nodeGroup = SmSettings.NodeGroup.Undefinded
			End Select
			If nodeGroup = SmSettings.NodeGroup.Undefinded Then
				Throw New InvalidDataException("Bad Data")
			End If

			Dim lenderConfig = smBL.GetLiveConfig(lenderConfigID)
			If lenderConfig Is Nothing Then
				Response.StatusCode = 404 '' Forbidden
				Response.StatusDescription = "Not found"
				Response.End() '' Terminate all the rest processes
			End If
			Dim apmComboEnable As Boolean = SmUtil.GetApmComboEnable(lenderConfig.ConfigData)
			If apmComboEnable = False AndAlso Regex.IsMatch(loanType.ToUpper(), "^(CC_COMBO|HE_COMBO|PL_COMBO|VL_COMBO)$") Then
				Response.StatusCode = 404 '' Forbidden
				Response.StatusDescription = "Not found"
				Response.End() '' Terminate all the rest processes
			End If



			draftRecordList.Add(New SmLenderConfigDraft() With {
				.LenderConfigID = lenderConfigID,
				.CreatedByUserID = UserInfo.UserID,
				.NodeGroup = nodeGroup,
				.NodePath = SHOW_FIELDS_ITEMS(convertedLoanType),
				.NodeContent = SmUtil.BuildShowFieldNodeXml("SHOW_FIELDS", convertedLoanType, showFieldItemList)})
			Dim revisionID As Integer = Common.SafeInteger(Request.Form("revisionID"))
			Dim result = smBL.SaveNodesToDraft(revisionID, draftRecordList.ToArray())
			If result Then
				res = New CJsonResponse(True, "", "OK")
			Else
				res = New CJsonResponse(False, "", "FAILED")
			End If
		Catch ex As Exception
			_log.Error("Could not SaveShowFieldItemsDraft.", ex)
			res = New CJsonResponse(False, "Unable to save draft. Please try again", "FAILED")
		End Try
		Response.Write(JsonConvert.SerializeObject(res))
		Response.End()
	End Sub

	Private Sub PublishShowFieldItemsDraft()
		Dim res As New CJsonResponse()
		Try
			Dim smBL As New SmBL()
			Dim lenderConfigID As Guid = Common.SafeGUID(Request.Form("lenderconfigid"))
			Dim loanType = Common.SafeStripHtmlString(Request.Form("loantype"))
			Dim showFieldItemList = JsonConvert.DeserializeObject(Of List(Of SmShowFieldItemModel))(Common.SafeString(Request.Form("showFieldItemList")), New JsonSerializerSettings() With {.NullValueHandling = NullValueHandling.Ignore})
			Dim nodeGroup As SmSettings.NodeGroup
			'For backward compatibility and avoid the need for migration script, use values “xa_special” and “sa_special” for loan_type attribute for both XA Special and XA Minor - don't use values “xa_minor” or “sa_minor”
			'This may have the side effect that a change in XA Special may leak to XA Minor. We would have to ask client to use as-is.
			Dim convertedLoanType As String = loanType
			Select Case Common.SafeString(loanType).ToLower()
				Case "xa"
					nodeGroup = SmSettings.NodeGroup.ShowFieldXA
				Case "sa"
					nodeGroup = SmSettings.NodeGroup.ShowFieldSA
				Case "he"
					nodeGroup = SmSettings.NodeGroup.ShowFieldHE
				Case "cc"
					nodeGroup = SmSettings.NodeGroup.ShowFieldCC
				Case "vl"
					nodeGroup = SmSettings.NodeGroup.ShowFieldVL
				Case "pl"
					nodeGroup = SmSettings.NodeGroup.ShowFieldPL
				Case "he_combo"
					nodeGroup = SmSettings.NodeGroup.ShowFieldHECombo
				Case "vl_combo"
					nodeGroup = SmSettings.NodeGroup.ShowFieldVLCombo
				Case "pl_combo"
					nodeGroup = SmSettings.NodeGroup.ShowFieldPLCombo
				Case "cc_combo"
					nodeGroup = SmSettings.NodeGroup.ShowFieldCCCombo
				Case "xa_special"
					nodeGroup = SmSettings.NodeGroup.ShowFieldXASpecial
				Case "sa_special"
					nodeGroup = SmSettings.NodeGroup.ShowFieldSASpecial
				Case "xa_minor"
					convertedLoanType = "XA_SPECIAL"
					nodeGroup = SmSettings.NodeGroup.ShowFieldXASpecial
				Case "sa_minor"
					convertedLoanType = "SA_SPECIAL"
					nodeGroup = SmSettings.NodeGroup.ShowFieldSASpecial
				Case "xa_business"
					nodeGroup = SmSettings.NodeGroup.ShowFieldXABusiness
				Case "sa_business"
					nodeGroup = SmSettings.NodeGroup.ShowFieldSABusiness
				Case "bl"
					nodeGroup = SmSettings.NodeGroup.ShowFieldBL
				Case "lqb_main"
					nodeGroup = SmSettings.NodeGroup.ShowFieldLQBMain
				Case Else
					nodeGroup = SmSettings.NodeGroup.Undefinded
			End Select
			If nodeGroup = SmSettings.NodeGroup.Undefinded Then
				Throw New InvalidDataException("Bad Data")
			End If

			Dim lenderConfig = smBL.GetLiveConfig(lenderConfigID)
			If lenderConfig Is Nothing Then
				Response.StatusCode = 404 '' Forbidden
				Response.StatusDescription = "Not found"
				Response.End() '' Terminate all the rest processes
			End If
			Dim apmComboEnable As Boolean = SmUtil.GetApmComboEnable(lenderConfig.ConfigData)
			If apmComboEnable = False AndAlso Regex.IsMatch(loanType.ToUpper(), "^(CC_COMBO|HE_COMBO|PL_COMBO|VL_COMBO)$") Then
				Response.StatusCode = 404 '' Forbidden
				Response.StatusDescription = "Not found"
				Response.End() '' Terminate all the rest processes
			End If



			Dim nodeBatch As New SmLenderConfigDraft() With {
				.LenderConfigID = lenderConfigID,
				.CreatedByUserID = UserInfo.UserID,
				.NodeGroup = nodeGroup,
				.NodePath = SHOW_FIELDS_ITEMS(convertedLoanType),
				.NodeContent = SmUtil.BuildShowFieldNodeXml("SHOW_FIELDS", convertedLoanType, showFieldItemList)}
			Dim comment As String = Common.SafeStripHtmlString(Request.Form("comment"))
			Dim publishAll As Boolean = Common.SafeBoolean(Request.Form("publishAll"))
			Dim result = smBL.PublishNodes(lenderConfigID, UserInfo.UserID, Request.UserHostAddress, comment, publishAll, AddressOf SmUtil.MergeShowFieldItems, nodeBatch)
			If result Then
				res = New CJsonResponse(True, "", "OK")
			Else
				res = New CJsonResponse(False, "", "FAILED")
			End If
		Catch ex As Exception
			_log.Error("Could not PublishShowFieldItemsDraft.", ex)
			res = New CJsonResponse(False, "Unable to publish data. Please try again", "FAILED")
		End Try
		Response.Write(JsonConvert.SerializeObject(res))
		Response.End()
	End Sub
#End Region

#Region "Visibility"

	Private Sub SaveValidationItemsDraft()
		Dim res As New CJsonResponse()
		Try
			Dim smBL As New SmBL()
			Dim draftRecordList As New List(Of SmLenderConfigDraft)
			Dim lenderConfigID As Guid = Common.SafeGUID(Request.Form("lenderconfigid"))
			Dim loanType = Common.SafeStripHtmlString(Request.Form("loantype"))
			Dim requireFieldItemList = JsonConvert.DeserializeObject(Of List(Of SmValidationItemModel))(Common.SafeString(Request.Form("requireFieldItemList")), New JsonSerializerSettings() With {.NullValueHandling = NullValueHandling.Ignore})
			Dim nodeGroup As SmSettings.NodeGroup
			'For backward compatibility and avoid the need for migration script, use values “xa_special” and “sa_special” for loan_type attribute for both XA Special and XA Minor - don't use values “xa_minor” or “sa_minor”
			'This may have the side effect that a change in XA Special may leak to XA Minor. We would have to ask client to use as-is.
			Dim convertedLoanType As String = loanType
			Select Case Common.SafeString(loanType).ToLower()
				Case "xa"
					nodeGroup = SmSettings.NodeGroup.ValidationXA
				Case "sa"
					nodeGroup = SmSettings.NodeGroup.ValidationSA
				Case "he"
					nodeGroup = SmSettings.NodeGroup.ValidationHE
				Case "cc"
					nodeGroup = SmSettings.NodeGroup.ValidationCC
				Case "vl"
					nodeGroup = SmSettings.NodeGroup.ValidationVL
				Case "pl"
					nodeGroup = SmSettings.NodeGroup.ValidationPL
				Case "he_combo"
					nodeGroup = SmSettings.NodeGroup.ValidationHECombo
				Case "vl_combo"
					nodeGroup = SmSettings.NodeGroup.ValidationVLCombo
				Case "pl_combo"
					nodeGroup = SmSettings.NodeGroup.ValidationPLCombo
				Case "cc_combo"
					nodeGroup = SmSettings.NodeGroup.ValidationCCCombo
				Case "xa_special"
					nodeGroup = SmSettings.NodeGroup.ValidationXASpecial
				Case "sa_special"
					nodeGroup = SmSettings.NodeGroup.ValidationSASpecial
				Case "xa_minor"
					convertedLoanType = "XA_SPECIAL"
					nodeGroup = SmSettings.NodeGroup.ValidationXASpecial
				Case "sa_minor"
					convertedLoanType = "SA_SPECIAL"
					nodeGroup = SmSettings.NodeGroup.ValidationSASpecial
				Case "xa_business"
					nodeGroup = SmSettings.NodeGroup.ValidationXABusiness
				Case "sa_business"
					nodeGroup = SmSettings.NodeGroup.ValidationSABusiness
				Case "bl"
					nodeGroup = SmSettings.NodeGroup.ValidationBL
				Case "lqb_main"
					nodeGroup = SmSettings.NodeGroup.ValidationLQBMain
				Case Else
					nodeGroup = SmSettings.NodeGroup.Undefinded
			End Select
			If nodeGroup = SmSettings.NodeGroup.Undefinded Then
				Throw New InvalidDataException("Bad Data")
			End If

			Dim lenderConfig = smBL.GetLiveConfig(lenderConfigID)
			If lenderConfig Is Nothing Then
				Response.StatusCode = 404 '' Forbidden
				Response.StatusDescription = "Not found"
				Response.End() '' Terminate all the rest processes
			End If
			Dim apmComboEnable As Boolean = SmUtil.GetApmComboEnable(lenderConfig.ConfigData)
			If apmComboEnable = False AndAlso Regex.IsMatch(loanType.ToUpper(), "^(CC_COMBO|HE_COMBO|PL_COMBO|VL_COMBO)$") Then
				Response.StatusCode = 404 '' Forbidden
				Response.StatusDescription = "Not found"
				Response.End() '' Terminate all the rest processes
			End If

			draftRecordList.Add(New SmLenderConfigDraft() With {
				.LenderConfigID = lenderConfigID,
				.CreatedByUserID = UserInfo.UserID,
				.NodeGroup = nodeGroup,
				.NodePath = REQUIRE_FIELDS_ITEMS(convertedLoanType),
				.NodeContent = SmUtil.BuildRequireFieldNodeXml("REQUIRE_FIELDS", convertedLoanType, requireFieldItemList)})
			Dim revisionID As Integer = Common.SafeInteger(Request.Form("revisionID"))
			Dim result = smBL.SaveNodesToDraft(revisionID, draftRecordList.ToArray())
			If result Then
				res = New CJsonResponse(True, "", "OK")
			Else
				res = New CJsonResponse(False, "", "FAILED")
			End If
		Catch ex As Exception
			_log.Error("Could not SaveValidationItemsDraft.", ex)
			res = New CJsonResponse(False, "Unable to save draft. Please try again", "FAILED")
		End Try
		Response.Write(JsonConvert.SerializeObject(res))
		Response.End()
	End Sub

	Private Sub PublishValidationItemsDraft()
		Dim res As New CJsonResponse()
		Try
			Dim smBL As New SmBL()
			Dim lenderConfigID As Guid = Common.SafeGUID(Request.Form("lenderconfigid"))
			Dim loanType = Common.SafeStripHtmlString(Request.Form("loantype"))
			Dim requireFieldItemList = JsonConvert.DeserializeObject(Of List(Of SmValidationItemModel))(Common.SafeString(Request.Form("requireFieldItemList")), New JsonSerializerSettings() With {.NullValueHandling = NullValueHandling.Ignore})
			Dim nodeGroup As SmSettings.NodeGroup
			'For backward compatibility and avoid the need for migration script, use values “xa_special” and “sa_special” for loan_type attribute for both XA Special and XA Minor - don't use values “xa_minor” or “sa_minor”
			'This may have the side effect that a change in XA Special may leak to XA Minor. We would have to ask client to use as-is.
			Dim convertedLoanType As String = loanType
			Select Case Common.SafeString(loanType).ToLower()
				Case "xa"
					nodeGroup = SmSettings.NodeGroup.ValidationXA
				Case "sa"
					nodeGroup = SmSettings.NodeGroup.ValidationSA
				Case "he"
					nodeGroup = SmSettings.NodeGroup.ValidationHE
				Case "cc"
					nodeGroup = SmSettings.NodeGroup.ValidationCC
				Case "vl"
					nodeGroup = SmSettings.NodeGroup.ValidationVL
				Case "pl"
					nodeGroup = SmSettings.NodeGroup.ValidationPL
				Case "he_combo"
					nodeGroup = SmSettings.NodeGroup.ValidationHECombo
				Case "vl_combo"
					nodeGroup = SmSettings.NodeGroup.ValidationVLCombo
				Case "pl_combo"
					nodeGroup = SmSettings.NodeGroup.ValidationPLCombo
				Case "cc_combo"
					nodeGroup = SmSettings.NodeGroup.ValidationCCCombo
				Case "xa_special"
					nodeGroup = SmSettings.NodeGroup.ValidationXASpecial
				Case "sa_special"
					nodeGroup = SmSettings.NodeGroup.ValidationSASpecial
				Case "xa_minor"
					convertedLoanType = "XA_SPECIAL"
					nodeGroup = SmSettings.NodeGroup.ValidationXASpecial
				Case "sa_minor"
					convertedLoanType = "SA_SPECIAL"
					nodeGroup = SmSettings.NodeGroup.ValidationSASpecial
				Case "xa_business"
					nodeGroup = SmSettings.NodeGroup.ValidationXABusiness
				Case "sa_business"
					nodeGroup = SmSettings.NodeGroup.ValidationSABusiness
				Case "bl"
					nodeGroup = SmSettings.NodeGroup.ValidationBL
				Case "lqb_main"
					nodeGroup = SmSettings.NodeGroup.ValidationLQBMain
				Case Else
					nodeGroup = SmSettings.NodeGroup.Undefinded
			End Select
			If nodeGroup = SmSettings.NodeGroup.Undefinded Then
				Throw New InvalidDataException("Bad Data")
			End If

			Dim lenderConfig = smBL.GetLiveConfig(lenderConfigID)
			If lenderConfig Is Nothing Then
				Response.StatusCode = 404 '' Forbidden
				Response.StatusDescription = "Not found"
				Response.End() '' Terminate all the rest processes
			End If
			Dim apmComboEnable As Boolean = SmUtil.GetApmComboEnable(lenderConfig.ConfigData)
			If apmComboEnable = False AndAlso Regex.IsMatch(loanType.ToUpper(), "^(CC_COMBO|HE_COMBO|PL_COMBO|VL_COMBO)$") Then
				Response.StatusCode = 404 '' Forbidden
				Response.StatusDescription = "Not found"
				Response.End() '' Terminate all the rest processes
			End If



			Dim nodeBatch As New SmLenderConfigDraft() With {
				.LenderConfigID = lenderConfigID,
				.CreatedByUserID = UserInfo.UserID,
				.NodeGroup = nodeGroup,
				.NodePath = REQUIRE_FIELDS_ITEMS(convertedLoanType),
				.NodeContent = SmUtil.BuildRequireFieldNodeXml("REQUIRE_FIELDS", convertedLoanType, requireFieldItemList)}
			Dim comment As String = Common.SafeStripHtmlString(Request.Form("comment"))
			Dim publishAll As Boolean = Common.SafeBoolean(Request.Form("publishAll"))
			Dim result = smBL.PublishNodes(lenderConfigID, UserInfo.UserID, Request.UserHostAddress, comment, publishAll, AddressOf SmUtil.MergeRequireFieldItems, nodeBatch)
			If result Then
				res = New CJsonResponse(True, "", "OK")
			Else
				res = New CJsonResponse(False, "", "FAILED")
			End If
		Catch ex As Exception
			_log.Error("Could not PublishValidationItemsDraft.", ex)
			res = New CJsonResponse(False, "Unable to publish data. Please try again", "FAILED")
		End Try
		Response.Write(JsonConvert.SerializeObject(res))
		Response.End()
	End Sub
#End Region


#Region "Products"
    Private Sub SaveProductConfigure()
		Dim res As CJsonResponse
		Try
			Dim smBL As New SmBL()
			Dim draftRecordList As New List(Of SmLenderConfigDraft)
			Dim lenderConfigID As Guid = Common.SafeGUID(Request.Form("lenderconfigid"))
			Dim tab As String = Common.SafeString(Request.Form("tab")).ToUpper()
			Dim oSerializer As New JavaScriptSerializer()
			Dim lenderConfig As CLenderConfig = smBL.GetLiveConfig(lenderConfigID)
			If lenderConfig Is Nothing Then
				Response.StatusCode = 404 '' Forbidden
				Response.StatusDescription = "Not found"
				Response.End() '' Terminate all the rest processes
			End If
			Dim lenderConfigXml As New XmlDocument()
			lenderConfigXml.LoadXml(lenderConfig.ConfigData)
			Dim apmComboEnable As Boolean = SmUtil.GetApmComboEnable(lenderConfig.ConfigData)
			Dim loanNodeName As String = ""
			Dim nodeGroup As SmSettings.NodeGroup = SmSettings.NodeGroup.Undefinded
			Select Case tab
				Case "XA"
					loanNodeName = "XA_LOAN"
					nodeGroup = SmSettings.NodeGroup.XAProductsConfigure
				Case "CC"
					loanNodeName = "CREDIT_CARD_LOAN"
					nodeGroup = SmSettings.NodeGroup.CCComboProductsConfigure
				Case "HE"
					loanNodeName = "HOME_EQUITY_LOAN"
					nodeGroup = SmSettings.NodeGroup.HEComboProductsConfigure
				Case "PL"
					loanNodeName = "PERSONAL_LOAN"
					nodeGroup = SmSettings.NodeGroup.PLComboProductsConfigure
				Case "VL"
					loanNodeName = "VEHICLE_LOAN"
					nodeGroup = SmSettings.NodeGroup.VLComboProductsConfigure
			End Select

			Dim enableXA As Boolean = lenderConfigXml.SelectSingleNode("WEBSITE/XA_LOAN") IsNot Nothing

			If Not String.IsNullOrWhiteSpace(loanNodeName) AndAlso (lenderConfigXml.SelectSingleNode("WEBSITE/" & loanNodeName) IsNot Nothing AndAlso (tab = "XA" OrElse apmComboEnable)) AndAlso enableXA Then

				Dim selectedProducts As List(Of SmTextValuePair) = oSerializer.Deserialize(Of List(Of SmTextValuePair))(Request.Form("selectedProducts"))
				draftRecordList.Add(New SmLenderConfigDraft() With {
					.LenderConfigID = lenderConfigID,
					.CreatedByUserID = UserInfo.UserID,
					.NodePath = Sm.ACCOUNTS(loanNodeName),
					.NodeGroup = nodeGroup,
				.NodeContent = SmUtil.BuildAccountsXml(selectedProducts)
				})
				Dim revisionID As Integer = Common.SafeInteger(Request.Form("revisionID"))
				Dim result = smBL.SaveNodesToDraft(revisionID, draftRecordList.ToArray())
				If result Then
					res = New CJsonResponse(True, "", "OK")
				Else
					res = New CJsonResponse(False, "", "FAILED")
				End If
			Else
				res = New CJsonResponse(False, "", "FAILED")
			End If


		Catch ex As Exception
			_log.Error("Could not SaveProductConfigure.", ex)
			res = New CJsonResponse(False, "Unable to save draft. Please try again", "FAILED")
		End Try
		Response.Write(JsonConvert.SerializeObject(res))
		Response.End()
	End Sub

	Private Sub PreviewProductConfigure()
		Dim res As CJsonResponse
		Try
			Dim smBL As New SmBL()
			Dim draftRecordList As New List(Of SmLenderConfigDraft)
			Dim lenderConfigID As Guid = Common.SafeGUID(Request.Form("lenderconfigid"))
			Dim tab As String = Common.SafeString(Request.Form("tab")).ToUpper()
			Dim previewPage As String = ""
			Dim oSerializer As New JavaScriptSerializer()
			Dim lenderConfig As CLenderConfig = smBL.GetLiveConfig(lenderConfigID)

			If lenderConfig Is Nothing Then
				Response.StatusCode = 404 '' Forbidden
				Response.StatusDescription = "Not found"
				Response.End() '' Terminate all the rest processes
			End If
			Dim lenderConfigXml As New XmlDocument()
			lenderConfigXml.LoadXml(lenderConfig.ConfigData)
			Dim apmComboEnable As Boolean = SmUtil.GetApmComboEnable(lenderConfig.ConfigData)
			Dim loanNodeName As String = ""
			Dim nodeGroup As SmSettings.NodeGroup = SmSettings.NodeGroup.Undefinded
			Select Case tab
				Case "XA"
					loanNodeName = "XA_LOAN"
					nodeGroup = SmSettings.NodeGroup.XAProductsConfigure
					previewPage = "xa/xpressapp.aspx"
				Case "CC"
					loanNodeName = "CREDIT_CARD_LOAN"
					nodeGroup = SmSettings.NodeGroup.CCComboProductsConfigure
					previewPage = "cc/creditcard.aspx"
				Case "HE"
					loanNodeName = "HOME_EQUITY_LOAN"
					nodeGroup = SmSettings.NodeGroup.HEComboProductsConfigure
					previewPage = "he/homeequityloan.aspx"
				Case "PL"
					loanNodeName = "PERSONAL_LOAN"
					nodeGroup = SmSettings.NodeGroup.PLComboProductsConfigure
					previewPage = "pl/personalloan.aspx"
				Case "VL"
					loanNodeName = "VEHICLE_LOAN"
					nodeGroup = SmSettings.NodeGroup.VLComboProductsConfigure
					previewPage = "vl/vehicleloan.aspx"
			End Select

			Dim enableXA As Boolean = lenderConfigXml.SelectSingleNode("WEBSITE/XA_LOAN") IsNot Nothing

			If Not String.IsNullOrWhiteSpace(loanNodeName) AndAlso (lenderConfigXml.SelectSingleNode("WEBSITE/" & loanNodeName) IsNot Nothing AndAlso (tab = "XA" OrElse apmComboEnable)) AndAlso enableXA Then

				Dim selectedProducts As List(Of SmTextValuePair) = oSerializer.Deserialize(Of List(Of SmTextValuePair))(Request.Form("selectedProducts"))
				draftRecordList.Add(New SmLenderConfigDraft() With {
					.LenderConfigID = lenderConfigID,
					.CreatedByUserID = UserInfo.UserID,
					.NodePath = Sm.ACCOUNTS(loanNodeName),
					.NodeGroup = nodeGroup,
				.NodeContent = SmUtil.BuildAccountsXml(selectedProducts)
				})
				Dim revisionID As Integer = Common.SafeInteger(Request.Form("revisionID"))
				Dim result = smBL.SaveNodesToDraft(revisionID, draftRecordList.ToArray())
				If result Then
					Dim previewUrl As String = smBL.GeneratePreviewConfig(lenderConfigID, UserInfo.Email, Function(config, code) String.Format("{0}/{1}?lenderref={2}&autofill=true&previewCode={3}{4}", _serverRoot, previewPage, config.LenderRef, code, IIf(tab <> "XA", "&type=1", "")), draftRecordList.ToArray())
					res = New CJsonResponse(True, "", "OK")
					res.Info = New With {.previewurl = previewUrl}
				Else
					res = New CJsonResponse(False, "", "FAILED")
				End If
			Else
				res = New CJsonResponse(False, "", "FAILED")
			End If


		Catch ex As Exception
			_log.Error("Could not PreviewProductConfigure.", ex)
			res = New CJsonResponse(False, "Unable to save and preview draft. Please try again", "FAILED")
		End Try
		Response.Write(JsonConvert.SerializeObject(res))
		Response.End()
	End Sub

	Private Sub PublishProductConfigure()
		Dim res As CJsonResponse
		Try
			Dim smBL As New SmBL()
			Dim draftRecordList As New List(Of SmLenderConfigDraft)
			Dim lenderConfigID As Guid = Common.SafeGUID(Request.Form("lenderconfigid"))
			Dim tab As String = Common.SafeString(Request.Form("tab")).ToUpper()
			Dim oSerializer As New JavaScriptSerializer()
			Dim lenderConfig As CLenderConfig = smBL.GetLiveConfig(lenderConfigID)
			If lenderConfig Is Nothing Then
				Response.StatusCode = 404 '' Forbidden
				Response.StatusDescription = "Not found"
				Response.End() '' Terminate all the rest processes
			End If
			Dim lenderConfigXml As New XmlDocument()
			lenderConfigXml.LoadXml(lenderConfig.ConfigData)
			Dim apmComboEnable As Boolean = SmUtil.GetApmComboEnable(lenderConfig.ConfigData)
			Dim loanNodeName As String = ""
			Dim nodeGroup As SmSettings.NodeGroup = SmSettings.NodeGroup.Undefinded
			Select Case tab
				Case "XA"
					loanNodeName = "XA_LOAN"
					nodeGroup = SmSettings.NodeGroup.XAProductsConfigure
				Case "CC"
					loanNodeName = "CREDIT_CARD_LOAN"
					nodeGroup = SmSettings.NodeGroup.CCComboProductsConfigure
				Case "HE"
					loanNodeName = "HOME_EQUITY_LOAN"
					nodeGroup = SmSettings.NodeGroup.HEComboProductsConfigure
				Case "PL"
					loanNodeName = "PERSONAL_LOAN"
					nodeGroup = SmSettings.NodeGroup.PLComboProductsConfigure
				Case "VL"
					loanNodeName = "VEHICLE_LOAN"
					nodeGroup = SmSettings.NodeGroup.VLComboProductsConfigure
			End Select
			Dim enableXA As Boolean = lenderConfigXml.SelectSingleNode("WEBSITE/XA_LOAN") IsNot Nothing
			If Not String.IsNullOrWhiteSpace(loanNodeName) AndAlso (lenderConfigXml.SelectSingleNode("WEBSITE/" & loanNodeName) IsNot Nothing AndAlso (tab = "XA" OrElse apmComboEnable)) AndAlso enableXA Then
				Dim selectedProducts As List(Of SmTextValuePair) = oSerializer.Deserialize(Of List(Of SmTextValuePair))(Request.Form("selectedProducts"))
				draftRecordList.Add(New SmLenderConfigDraft() With {
					.LenderConfigID = lenderConfigID,
					.CreatedByUserID = UserInfo.UserID,
					.NodePath = Sm.ACCOUNTS(loanNodeName),
					.NodeGroup = nodeGroup,
				.NodeContent = SmUtil.BuildAccountsXml(selectedProducts)
				})
				Dim comment As String = Common.SafeStripHtmlString(Request.Form("comment"))
				Dim publishAll As Boolean = Common.SafeBoolean(Request.Form("publishAll"))
				Dim result = smBL.PublishNodes(lenderConfigID, UserInfo.UserID, Request.UserHostAddress, comment, publishAll, draftRecordList.ToArray())
				If result Then
					res = New CJsonResponse(True, "", "OK")
				Else
					res = New CJsonResponse(False, "", "FAILED")
				End If
			Else
				res = New CJsonResponse(False, "", "FAILED")
			End If
		Catch ex As Exception
			_log.Error("Could not PublishProductConfigure.", ex)
			res = New CJsonResponse(False, "Unable to publish data. Please try again", "FAILED")
		End Try
		Response.Write(JsonConvert.SerializeObject(res))
		Response.End()
	End Sub
#End Region

#Region "CustomQuestions"
    Private Sub SaveConfigureCQ()
		Dim res As CJsonResponse
		Try
			Dim smBL As New SmBL()
			Dim draftRecordList As New List(Of SmLenderConfigDraft)
			Dim lenderConfigID As Guid = Common.SafeGUID(Request.Form("lenderconfigid"))
			Dim tab As String = Common.SafeString(Request.Form("tab")).ToUpper()
			Dim oSerializer As New JavaScriptSerializer()
			Dim lenderConfig As CLenderConfig = smBL.GetLiveConfig(lenderConfigID)
			If lenderConfig Is Nothing Then
				Response.StatusCode = 404 '' Forbidden
				Response.StatusDescription = "Not found"
				Response.End() '' Terminate all the rest processes
			End If
			Dim lenderConfigXml As New XmlDocument()
			lenderConfigXml.LoadXml(lenderConfig.ConfigData)
			Dim loanNodeName As String = ""
			Dim nodeGroup As SmSettings.NodeGroup = SmSettings.NodeGroup.Undefinded
			Select Case tab
				Case "XA"
					loanNodeName = "XA_LOAN"
					nodeGroup = SmSettings.NodeGroup.XACustomQuestions
				Case "CC"
					loanNodeName = "CREDIT_CARD_LOAN"
					nodeGroup = SmSettings.NodeGroup.CCCustomQuestions
				Case "HE"
					loanNodeName = "HOME_EQUITY_LOAN"
					nodeGroup = SmSettings.NodeGroup.HECustomQuestions
				Case "PL"
					loanNodeName = "PERSONAL_LOAN"
					nodeGroup = SmSettings.NodeGroup.PLCustomQuestions
				Case "VL"
					loanNodeName = "VEHICLE_LOAN"
					nodeGroup = SmSettings.NodeGroup.VLCustomQuestions
				Case "BL"
					loanNodeName = "BUSINESS_LOAN"
					nodeGroup = SmSettings.NodeGroup.BLCustomQuestions
			End Select
			If Not String.IsNullOrWhiteSpace(loanNodeName) AndAlso lenderConfigXml.SelectSingleNode("WEBSITE/" & loanNodeName) IsNot Nothing Then
				Dim hideAllApplicantCQ As Boolean = Common.SafeBoolean(Request.Form("ApplicantHideAll"))
                Dim applicantCQ As New List(Of SmCQInformation)
                If Not hideAllApplicantCQ Then
                    applicantCQ = oSerializer.Deserialize(Of List(Of SmCQInformation))(Request.Form("ApplicantCQ"))
                End If
				draftRecordList.Add(New SmLenderConfigDraft() With {
					.LenderConfigID = lenderConfigID,
					.CreatedByUserID = UserInfo.UserID,
					.NodePath = CUSTOM_APPLICANT_QUESTIONS(loanNodeName),
					.NodeGroup = nodeGroup,
				.NodeContent = SmUtil.BuildCustomQuestionsXml("CUSTOM_APPLICANT_QUESTIONS", applicantCQ, hideAllApplicantCQ)
				})

				Dim hideAllApplicationCQ As Boolean = Common.SafeBoolean(Request.Form("ApplicationHideAll"))
                Dim cq As New List(Of SmCQInformation)
                If Not hideAllApplicationCQ Then
                    cq = oSerializer.Deserialize(Of List(Of SmCQInformation))(Request.Form("CQ"))
                End If
				draftRecordList.Add(New SmLenderConfigDraft() With {
					.LenderConfigID = lenderConfigID,
					.CreatedByUserID = UserInfo.UserID,
					.NodePath = Sm.CUSTOM_QUESTIONS(loanNodeName),
					.NodeGroup = nodeGroup,
				.NodeContent = SmUtil.BuildCustomQuestionsXml("CUSTOM_QUESTIONS", cq, hideAllApplicationCQ)
				})
				draftRecordList.Add(New SmLenderConfigDraft() With {
					.LenderConfigID = lenderConfigID,
					.CreatedByUserID = UserInfo.UserID,
					.NodePath = Sm.ENABLE_CUSTOM_QUESTION_NEW_API,
					.NodeGroup = nodeGroup,
				.NodeContent = SmUtil.BuildNodeAttributeXml("VISIBLE", "custom_question_new_api", "Y")
				})
				Dim revisionID As Integer = Common.SafeInteger(Request.Form("revisionID"))
				Dim result = smBL.SaveNodesToDraft(revisionID, draftRecordList.ToArray())
				If result Then
					res = New CJsonResponse(True, "", "OK")
				Else
					res = New CJsonResponse(False, "", "FAILED")
				End If
			Else
				res = New CJsonResponse(False, "", "FAILED")
			End If
		Catch ex As Exception
			_log.Error("Could not SaveConfigureCQ.", ex)
			res = New CJsonResponse(False, "Unable to save draft. Please try again", "FAILED")
		End Try
		Response.Write(JsonConvert.SerializeObject(res))
		Response.End()
	End Sub

	Private Sub PreviewConfigureCQ()
		Dim res As CJsonResponse
		Try
			Dim smBL As New SmBL()
			Dim draftRecordList As New List(Of SmLenderConfigDraft)
			Dim lenderConfigID As Guid = Common.SafeGUID(Request.Form("lenderconfigid"))
			Dim tab As String = Common.SafeString(Request.Form("tab")).ToUpper()
			Dim oSerializer As New JavaScriptSerializer()
			Dim lenderConfig As CLenderConfig = smBL.GetLiveConfig(lenderConfigID)
			If lenderConfig Is Nothing Then
				Response.StatusCode = 404 '' Forbidden
				Response.StatusDescription = "Not found"
				Response.End() '' Terminate all the rest processes
			End If
			Dim lenderConfigXml As New XmlDocument()
			lenderConfigXml.LoadXml(lenderConfig.ConfigData)
			Dim loanNodeName As String = ""
			Dim previewPage As String = ""
			Dim nodeGroup As SmSettings.NodeGroup = SmSettings.NodeGroup.Undefinded
			Select Case tab
				Case "XA"
					loanNodeName = "XA_LOAN"
					nodeGroup = SmSettings.NodeGroup.XACustomQuestions
					previewPage = "xa/xpressapp.aspx"
				Case "CC"
					loanNodeName = "CREDIT_CARD_LOAN"
					nodeGroup = SmSettings.NodeGroup.CCCustomQuestions
					previewPage = "cc/creditcard.aspx"
				Case "HE"
					loanNodeName = "HOME_EQUITY_LOAN"
					nodeGroup = SmSettings.NodeGroup.HECustomQuestions
					previewPage = "he/homeequityloan.aspx"
				Case "PL"
					loanNodeName = "PERSONAL_LOAN"
					nodeGroup = SmSettings.NodeGroup.PLCustomQuestions
					previewPage = "pl/personalloan.aspx"
				Case "VL"
					loanNodeName = "VEHICLE_LOAN"
					nodeGroup = SmSettings.NodeGroup.VLCustomQuestions
					previewPage = "vl/vehicleloan.aspx"
				Case "BL"
					loanNodeName = "BUSINESS_LOAN"
					nodeGroup = SmSettings.NodeGroup.BLCustomQuestions
					previewPage = "apply.aspx"
			End Select


			If Not String.IsNullOrWhiteSpace(loanNodeName) AndAlso lenderConfigXml.SelectSingleNode("WEBSITE/" & loanNodeName) IsNot Nothing Then
				Dim hideAllApplicantCQ As Boolean = Common.SafeBoolean(Request.Form("ApplicantHideAll"))
                Dim applicantCQ As New List(Of SmCQInformation)
                If Not hideAllApplicantCQ Then
                    applicantCQ = oSerializer.Deserialize(Of List(Of SmCQInformation))(Request.Form("ApplicantCQ"))
                End If
				draftRecordList.Add(New SmLenderConfigDraft() With {
					.LenderConfigID = lenderConfigID,
					.CreatedByUserID = UserInfo.UserID,
					.NodePath = CUSTOM_APPLICANT_QUESTIONS(loanNodeName),
					.NodeGroup = nodeGroup,
				.NodeContent = SmUtil.BuildCustomQuestionsXml("CUSTOM_APPLICANT_QUESTIONS", applicantCQ, hideAllApplicantCQ)
				})

				Dim hideAllApplicationCQ As Boolean = Common.SafeBoolean(Request.Form("ApplicationHideAll"))
                Dim cq As New List(Of SmCQInformation)
                If Not hideAllApplicationCQ Then
                    cq = oSerializer.Deserialize(Of List(Of SmCQInformation))(Request.Form("CQ"))
                End If
				draftRecordList.Add(New SmLenderConfigDraft() With {
					.LenderConfigID = lenderConfigID,
					.CreatedByUserID = UserInfo.UserID,
					.NodePath = Sm.CUSTOM_QUESTIONS(loanNodeName),
					.NodeGroup = nodeGroup,
				.NodeContent = SmUtil.BuildCustomQuestionsXml("CUSTOM_QUESTIONS", cq, hideAllApplicationCQ)
				})
				draftRecordList.Add(New SmLenderConfigDraft() With {
					.LenderConfigID = lenderConfigID,
					.CreatedByUserID = UserInfo.UserID,
					.NodePath = Sm.ENABLE_CUSTOM_QUESTION_NEW_API,
					.NodeGroup = nodeGroup,
				.NodeContent = SmUtil.BuildNodeAttributeXml("VISIBLE", "custom_question_new_api", "Y")
				})
				Dim revisionID As Integer = Common.SafeInteger(Request.Form("revisionID"))
				Dim result = smBL.SaveNodesToDraft(revisionID, draftRecordList.ToArray())
				If result Then
					Dim previewUrl As String = smBL.GeneratePreviewConfig(lenderConfigID, UserInfo.Email, Function(config, code) String.Format("{0}/{1}?lenderref={2}&autofill=true&previewCode={3}{4}", _serverRoot, previewPage, config.LenderRef, code, IIf(tab = "BL", "&list=bl", "")), draftRecordList.ToArray())
					res = New CJsonResponse(True, "", "OK")
					res.Info = New With {.previewurl = previewUrl}
				Else
					res = New CJsonResponse(False, "", "FAILED")
				End If
			Else
				res = New CJsonResponse(False, "", "FAILED")
			End If
		Catch ex As Exception
			_log.Error("Could not PreviewConfigureCQ.", ex)
			res = New CJsonResponse(False, "Unable to save and preview draft. Please try again", "FAILED")
		End Try
		Response.Write(JsonConvert.SerializeObject(res))
		Response.End()
	End Sub

	Private Sub PublishConfigureCQ()
		Dim res As CJsonResponse
		Try
			Dim smBL As New SmBL()
			Dim draftRecordList As New List(Of SmLenderConfigDraft)
			Dim lenderConfigID As Guid = Common.SafeGUID(Request.Form("lenderconfigid"))
			Dim tab As String = Common.SafeString(Request.Form("tab")).ToUpper()
			Dim oSerializer As New JavaScriptSerializer()
			Dim lenderConfig As CLenderConfig = smBL.GetLiveConfig(lenderConfigID)
			If lenderConfig Is Nothing Then
				Response.StatusCode = 404 '' Forbidden
				Response.StatusDescription = "Not found"
				Response.End() '' Terminate all the rest processes
			End If
			Dim lenderConfigXml As New XmlDocument()
			lenderConfigXml.LoadXml(lenderConfig.ConfigData)
			Dim loanNodeName As String = ""
			Dim nodeGroup As SmSettings.NodeGroup = SmSettings.NodeGroup.Undefinded
			Select Case tab
				Case "XA"
					loanNodeName = "XA_LOAN"
					nodeGroup = SmSettings.NodeGroup.XACustomQuestions
				Case "CC"
					loanNodeName = "CREDIT_CARD_LOAN"
					nodeGroup = SmSettings.NodeGroup.CCCustomQuestions
				Case "HE"
					loanNodeName = "HOME_EQUITY_LOAN"
					nodeGroup = SmSettings.NodeGroup.HECustomQuestions
				Case "PL"
					loanNodeName = "PERSONAL_LOAN"
					nodeGroup = SmSettings.NodeGroup.PLCustomQuestions
				Case "VL"
					loanNodeName = "VEHICLE_LOAN"
					nodeGroup = SmSettings.NodeGroup.VLCustomQuestions
				Case "BL"
					loanNodeName = "BUSINESS_LOAN"
					nodeGroup = SmSettings.NodeGroup.BLCustomQuestions
			End Select


			If Not String.IsNullOrWhiteSpace(loanNodeName) AndAlso lenderConfigXml.SelectSingleNode("WEBSITE/" & loanNodeName) IsNot Nothing Then
				Dim hideAllApplicantCQ As Boolean = Common.SafeBoolean(Request.Form("ApplicantHideAll"))
                Dim applicantCQ As New List(Of SmCQInformation)
                If Not hideAllApplicantCQ Then
                    applicantCQ = oSerializer.Deserialize(Of List(Of SmCQInformation))(Request.Form("ApplicantCQ"))
                End If
				draftRecordList.Add(New SmLenderConfigDraft() With {
					.LenderConfigID = lenderConfigID,
					.CreatedByUserID = UserInfo.UserID,
					.NodePath = CUSTOM_APPLICANT_QUESTIONS(loanNodeName),
					.NodeGroup = nodeGroup,
				.NodeContent = SmUtil.BuildCustomQuestionsXml("CUSTOM_APPLICANT_QUESTIONS", applicantCQ, hideAllApplicantCQ)
				})

				Dim hideAllApplicationCQ As Boolean = Common.SafeBoolean(Request.Form("ApplicationHideAll"))
                Dim cq As New List(Of SmCQInformation)
                If Not hideAllApplicationCQ Then
                    cq = oSerializer.Deserialize(Of List(Of SmCQInformation))(Request.Form("CQ"))
                End If
				draftRecordList.Add(New SmLenderConfigDraft() With {
					.LenderConfigID = lenderConfigID,
					.CreatedByUserID = UserInfo.UserID,
					.NodePath = Sm.CUSTOM_QUESTIONS(loanNodeName),
					.NodeGroup = nodeGroup,
				.NodeContent = SmUtil.BuildCustomQuestionsXml("CUSTOM_QUESTIONS", cq, hideAllApplicationCQ)
				})
				draftRecordList.Add(New SmLenderConfigDraft() With {
					.LenderConfigID = lenderConfigID,
					.CreatedByUserID = UserInfo.UserID,
					.NodePath = Sm.ENABLE_CUSTOM_QUESTION_NEW_API,
					.NodeGroup = nodeGroup,
				.NodeContent = SmUtil.BuildNodeAttributeXml("VISIBLE", "custom_question_new_api", "Y")
				})
				Dim comment As String = Common.SafeStripHtmlString(Request.Form("comment"))
				Dim publishAll As Boolean = Common.SafeBoolean(Request.Form("publishAll"))
				Dim result = smBL.PublishNodes(lenderConfigID, UserInfo.UserID, Request.UserHostAddress, comment, publishAll, draftRecordList.ToArray())
				If result Then
					res = New CJsonResponse(True, "", "OK")
				Else
					res = New CJsonResponse(False, "", "FAILED")
				End If
			Else
				res = New CJsonResponse(False, "", "FAILED")
			End If
		Catch ex As Exception
			_log.Error("Could not PublishConfigureCQ.", ex)
			res = New CJsonResponse(False, "Unable to publish data. Please try again", "FAILED")
		End Try
		Response.Write(JsonConvert.SerializeObject(res))
		Response.End()
	End Sub
#End Region


#Region "Advanced Logics"
	Private Sub SaveAdvancedLogicsDraft()
		Dim res As New CJsonResponse()
		Try
			Dim smBL As New SmBL()
			Dim draftRecordList As New List(Of SmLenderConfigDraft)
			Dim lenderConfigID As Guid = Common.SafeGUID(Request.Form("lenderconfigid"))
			Dim oSerializer As New JavaScriptSerializer()
			Dim submitData As Dictionary(Of String, List(Of SmAdvancedLogicItem)) = oSerializer.Deserialize(Of Dictionary(Of String, List(Of SmAdvancedLogicItem)))(Request.Form("data"))

			Dim lenderConfig As CLenderConfig = smBL.GetLiveConfig(lenderConfigID)
			If lenderConfig Is Nothing Then
				Response.StatusCode = 404 '' Forbidden
				Response.StatusDescription = "Not found"
				Response.End() '' Terminate all the rest processes
			End If
			Dim lenderConfigXml As New XmlDocument()
			lenderConfigXml.LoadXml(lenderConfig.ConfigData)
			Dim enableXA As Boolean = lenderConfigXml.SelectSingleNode("WEBSITE/XA_LOAN") IsNot Nothing
			Dim enableCC As Boolean = lenderConfigXml.SelectSingleNode("WEBSITE/CREDIT_CARD_LOAN") IsNot Nothing
			Dim enableHE As Boolean = lenderConfigXml.SelectSingleNode("WEBSITE/HOME_EQUITY_LOAN") IsNot Nothing
			Dim enablePL As Boolean = lenderConfigXml.SelectSingleNode("WEBSITE/PERSONAL_LOAN") IsNot Nothing
			Dim enableVL As Boolean = lenderConfigXml.SelectSingleNode("WEBSITE/VEHICLE_LOAN") IsNot Nothing
			Dim enableBL As Boolean = lenderConfigXml.SelectSingleNode("WEBSITE/BUSINESS_LOAN") IsNot Nothing

			Dim apmComboEnable As Boolean = SmUtil.GetApmComboEnable(lenderConfig.ConfigData)
			Dim enableCCCombo As Boolean = apmComboEnable And enableXA And enableCC
			Dim enableHECombo As Boolean = apmComboEnable And enableXA And enableHE
			Dim enablePLCombo As Boolean = apmComboEnable And enableXA And enablePL
			Dim enableVLCombo As Boolean = apmComboEnable And enableXA And enableVL

			If (enableCCCombo = False AndAlso submitData.Any(Function(p) p.Key = "cc_combo")) OrElse (enableHECombo = False AndAlso submitData.Any(Function(p) p.Key = "he_combo")) OrElse (enablePLCombo = False AndAlso submitData.Any(Function(p) p.Key = "pl_combo")) OrElse (enableVLCombo = False AndAlso submitData.Any(Function(p) p.Key = "vl_combo")) Then
				'force reload page incase the content on page is out of date. Means it not reflect the current settings
				Response.StatusCode = 405 '' Reset page
				Response.StatusDescription = "Reset page"
				Response.End() '' Terminate all the rest processes
			End If

			submitData = submitData.Where(Function(p) (p.Key = "xa" And enableXA) Or (p.Key = "cc" And enableCC) Or (p.Key = "he" And enableHE) Or (p.Key = "pl" And enablePL) Or (p.Key = "vl" And enableVL) Or (p.Key = "cc_combo" And enableCCCombo) Or (p.Key = "he_combo" And enableHECombo) Or (p.Key = "pl_combo" And enablePLCombo) Or (p.Key = "vl_combo" And enableVLCombo) Or (p.Key = "bl" And enableBL)).ToDictionary(Function(p) p.Key, Function(p) p.Value)
			draftRecordList.Add(New SmLenderConfigDraft() With {
				.LenderConfigID = lenderConfigID,
				.CreatedByUserID = UserInfo.UserID,
				.NodePath = Sm.ADVANCED_LOGICS(),
				.NodeGroup = SmSettings.NodeGroup.AdvancedLogics,
			.NodeContent = SmUtil.BuildAdvancedLogicNodeXml(submitData)
			})
			Dim revisionID As Integer = Common.SafeInteger(Request.Form("revisionID"))
			Dim result = smBL.SaveNodesToDraft(revisionID, draftRecordList.ToArray())
			If result Then
				res = New CJsonResponse(True, "", "OK")
			Else
				res = New CJsonResponse(False, "", "FAILED")
			End If
		Catch ex As Exception
			_log.Error("Could not SaveAdvancedLogicsDraft.", ex)
			res = New CJsonResponse(False, "Unable to save draft. Please try again", "FAILED")
		End Try
		Response.Write(JsonConvert.SerializeObject(res))
		Response.End()
	End Sub

	Private Sub PreviewAdvancedLogicsDraft()
		Dim res As New CJsonResponse()
		Try
			Dim previewType As String = Common.SafeString(Request.Form("preview_type"))
			Dim smBL As New SmBL()
			Dim draftRecordList As New List(Of SmLenderConfigDraft)
			Dim lenderConfigID As Guid = Common.SafeGUID(Request.Form("lenderconfigid"))
			Dim oSerializer As New JavaScriptSerializer()
			Dim submitData As Dictionary(Of String, List(Of SmAdvancedLogicItem)) = oSerializer.Deserialize(Of Dictionary(Of String, List(Of SmAdvancedLogicItem)))(Request.Form("data"))

			Dim lenderConfig As CLenderConfig = smBL.GetLiveConfig(lenderConfigID)
			If lenderConfig Is Nothing Then
				Response.StatusCode = 404 '' Forbidden
				Response.StatusDescription = "Not found"
				Response.End() '' Terminate all the rest processes
			End If
			Dim lenderConfigXml As New XmlDocument()
			lenderConfigXml.LoadXml(lenderConfig.ConfigData)
			Dim enableXA As Boolean = lenderConfigXml.SelectSingleNode("WEBSITE/XA_LOAN") IsNot Nothing
			Dim enableCC As Boolean = lenderConfigXml.SelectSingleNode("WEBSITE/CREDIT_CARD_LOAN") IsNot Nothing
			Dim enableHE As Boolean = lenderConfigXml.SelectSingleNode("WEBSITE/HOME_EQUITY_LOAN") IsNot Nothing
			Dim enablePL As Boolean = lenderConfigXml.SelectSingleNode("WEBSITE/PERSONAL_LOAN") IsNot Nothing
			Dim enableVL As Boolean = lenderConfigXml.SelectSingleNode("WEBSITE/VEHICLE_LOAN") IsNot Nothing
			Dim enableBL As Boolean = lenderConfigXml.SelectSingleNode("WEBSITE/BUSINESS_LOAN") IsNot Nothing

			Dim apmComboEnable As Boolean = SmUtil.GetApmComboEnable(lenderConfig.ConfigData)
			Dim enableCCCombo As Boolean = apmComboEnable And enableXA And enableCC
			Dim enableHECombo As Boolean = apmComboEnable And enableXA And enableHE
			Dim enablePLCombo As Boolean = apmComboEnable And enableXA And enablePL
			Dim enableVLCombo As Boolean = apmComboEnable And enableXA And enableVL

			If (enableCCCombo = False AndAlso submitData.Any(Function(p) p.Key = "cc_combo")) OrElse (enableHECombo = False AndAlso submitData.Any(Function(p) p.Key = "he_combo")) OrElse (enablePLCombo = False AndAlso submitData.Any(Function(p) p.Key = "pl_combo")) OrElse (enableVLCombo = False AndAlso submitData.Any(Function(p) p.Key = "vl_combo")) Then
				'force reload page incase the content on page is out of date. Means it not reflect the current settings
				Response.StatusCode = 405 '' Reset page
				Response.StatusDescription = "Reset page"
				Response.End() '' Terminate all the rest processes
			End If

			submitData = submitData.Where(Function(p) (p.Key = "xa" And enableXA) Or (p.Key = "cc" And enableCC) Or (p.Key = "he" And enableHE) Or (p.Key = "pl" And enablePL) Or (p.Key = "vl" And enableVL) Or (p.Key = "cc_combo" And enableCCCombo) Or (p.Key = "he_combo" And enableHECombo) Or (p.Key = "pl_combo" And enablePLCombo) Or (p.Key = "vl_combo" And enableVLCombo) Or (p.Key = "bl") And enableBL).ToDictionary(Function(p) p.Key, Function(p) p.Value)
			draftRecordList.Add(New SmLenderConfigDraft() With {
				.LenderConfigID = lenderConfigID,
				.CreatedByUserID = UserInfo.UserID,
				.NodePath = Sm.ADVANCED_LOGICS(),
				.NodeGroup = SmSettings.NodeGroup.AdvancedLogics,
			.NodeContent = SmUtil.BuildAdvancedLogicNodeXml(submitData)
			})
			Dim revisionID As Integer = Common.SafeInteger(Request.Form("revisionID"))
			Dim result = smBL.SaveNodesToDraft(revisionID, draftRecordList.ToArray())
			If result Then
				Dim previewPage As String = ""
				Select Case previewType
					Case "cc"
						previewPage = "cc/creditcard.aspx"
					Case "cccombo"
						previewPage = "cc/creditcard.aspx"
					Case "he"
						previewPage = "he/homeequityloan.aspx"
					Case "hecombo"
						previewPage = "he/homeequityloan.aspx"
					Case "pl"
						previewPage = "pl/personalloan.aspx"
					Case "plcombo"
						previewPage = "pl/personalloan.aspx"
					Case "vl"
						previewPage = "vl/vehicleloan.aspx"
					Case "bl"
						previewPage = "apply.aspx"
					Case "vlcombo"
						previewPage = "vl/vehicleloan.aspx"
					Case Else
						previewPage = "xa/xpressapp.aspx"
				End Select
				Dim previewUrl As String = smBL.GeneratePreviewConfig(lenderConfigID, UserInfo.Email, Function(config, code) String.Format("{0}/{1}?lenderref={2}&autofill=true&previewCode={3}{4}{5}", _serverRoot, previewPage, config.LenderRef, code, IIf(previewType.EndsWith("combo"), "&type=1", ""), IIf(previewType = "bl", "&list=bl", "")), draftRecordList.ToArray())
				res = New CJsonResponse(True, "", "OK")
				res.Info = New With {.previewurl = previewUrl}
			Else
				res = New CJsonResponse(False, "", "FAILED")
			End If
		Catch ex As Exception
			_log.Error("Could not PreviewAdvancedLogicsDraft.", ex)
			res = New CJsonResponse(False, "Unable to save and preview draft. Please try again", "FAILED")
		End Try
		Response.Write(JsonConvert.SerializeObject(res))
		Response.End()
	End Sub
	Private Sub PublishAdvancedLogicsDraft()
		Dim res As New CJsonResponse()
		Try
			Dim smBL As New SmBL()
			Dim draftRecordList As New List(Of SmLenderConfigDraft)
			Dim lenderConfigID As Guid = Common.SafeGUID(Request.Form("lenderconfigid"))
			Dim oSerializer As New JavaScriptSerializer()
			Dim submitData As Dictionary(Of String, List(Of SmAdvancedLogicItem)) = oSerializer.Deserialize(Of Dictionary(Of String, List(Of SmAdvancedLogicItem)))(Request.Form("data"))

			Dim lenderConfig As CLenderConfig = smBL.GetLiveConfig(lenderConfigID)
			If lenderConfig Is Nothing Then
				Response.StatusCode = 404 '' Forbidden
				Response.StatusDescription = "Not found"
				Response.End() '' Terminate all the rest processes
			End If
			Dim lenderConfigXml As New XmlDocument()
			lenderConfigXml.LoadXml(lenderConfig.ConfigData)
			Dim enableXA As Boolean = lenderConfigXml.SelectSingleNode("WEBSITE/XA_LOAN") IsNot Nothing
			Dim enableCC As Boolean = lenderConfigXml.SelectSingleNode("WEBSITE/CREDIT_CARD_LOAN") IsNot Nothing
			Dim enableHE As Boolean = lenderConfigXml.SelectSingleNode("WEBSITE/HOME_EQUITY_LOAN") IsNot Nothing
			Dim enablePL As Boolean = lenderConfigXml.SelectSingleNode("WEBSITE/PERSONAL_LOAN") IsNot Nothing
			Dim enableVL As Boolean = lenderConfigXml.SelectSingleNode("WEBSITE/VEHICLE_LOAN") IsNot Nothing
			Dim enableBL As Boolean = lenderConfigXml.SelectSingleNode("WEBSITE/BUSINESS_LOAN") IsNot Nothing
			Dim apmComboEnable As Boolean = SmUtil.GetApmComboEnable(lenderConfig.ConfigData)
			Dim enableCCCombo As Boolean = apmComboEnable And enableXA And enableCC
			Dim enableHECombo As Boolean = apmComboEnable And enableXA And enableHE
			Dim enablePLCombo As Boolean = apmComboEnable And enableXA And enablePL
			Dim enableVLCombo As Boolean = apmComboEnable And enableXA And enableVL

			If (enableCCCombo = False AndAlso submitData.Any(Function(p) p.Key = "cc_combo")) OrElse (enableHECombo = False AndAlso submitData.Any(Function(p) p.Key = "he_combo")) OrElse (enablePLCombo = False AndAlso submitData.Any(Function(p) p.Key = "pl_combo")) OrElse (enableVLCombo = False AndAlso submitData.Any(Function(p) p.Key = "vl_combo")) Then
				'force reload page incase the content on page is out of date. Means it not reflect the current settings
				Response.StatusCode = 405 '' Reset page
				Response.StatusDescription = "Reset page"
				Response.End() '' Terminate all the rest processes
			End If

			submitData = submitData.Where(Function(p) (p.Key = "xa" And enableXA) Or (p.Key = "cc" And enableCC) Or (p.Key = "he" And enableHE) Or (p.Key = "pl" And enablePL) Or (p.Key = "vl" And enableVL) Or (p.Key = "cc_combo" And enableCCCombo) Or (p.Key = "he_combo" And enableHECombo) Or (p.Key = "pl_combo" And enablePLCombo) Or (p.Key = "vl_combo" And enableVLCombo) Or (p.Key = "bl") And enableBL).ToDictionary(Function(p) p.Key, Function(p) p.Value)
			draftRecordList.Add(New SmLenderConfigDraft() With {
				.LenderConfigID = lenderConfigID,
				.CreatedByUserID = UserInfo.UserID,
				.NodePath = Sm.ADVANCED_LOGICS(),
				.NodeGroup = SmSettings.NodeGroup.AdvancedLogics,
			.NodeContent = SmUtil.BuildAdvancedLogicNodeXml(submitData)
			})
			Dim comment As String = Common.SafeStripHtmlString(Request.Form("comment"))
			Dim publishAll As Boolean = Common.SafeBoolean(Request.Form("publishAll"))
			Dim result = smBL.PublishNodes(lenderConfigID, UserInfo.UserID, Request.UserHostAddress, comment, publishAll, draftRecordList.ToArray())
			If result Then
				res = New CJsonResponse(True, "", "OK")
			Else
				res = New CJsonResponse(False, "", "FAILED")
			End If
		Catch ex As Exception
			_log.Error("Could not PublishAdvancedLogicsDraft.", ex)
			res = New CJsonResponse(False, "Unable to publish data. Please try again", "FAILED")
		End Try
		Response.Write(JsonConvert.SerializeObject(res))
		Response.End()
	End Sub
#End Region

#Region "Vendors"
	Private Sub LoadVendorGrid()
		Try
			Dim paging = JsonConvert.DeserializeObject(Of SmPagingInfoModel)(Request.Form("paging_info")).MakeItSafe()
			Dim filter = JsonConvert.DeserializeObject(Of SmVendorListFilter)(Request.Form("filter_info")).MakeItSafe()
			Dim backOfficeLenderID As Guid = Common.SafeGUID(Request.Form("bid"))
			Dim smBL As New SmBL
			Dim backOfficeLender As SmBackOfficeLender = smBL.GetBackOfficeLenderByID(backOfficeLenderID)
			If backOfficeLender IsNot Nothing Then
				Dim control As Sm_Uc_VendorList = CType(LoadControl("~/Sm/Uc/VendorList.ascx"), Sm_Uc_VendorList)
				control.PagingInfo = paging
				If filter Is Nothing Then
					filter = New SmVendorListFilter()
				End If
				filter.LenderID = backOfficeLender.LenderID
				control.FilterInfo = filter.MakeItSafe()
				Me.Controls.Add(control)
			Else
				Response.StatusCode = 404 '' Forbidden
				Response.StatusDescription = "Not found"
				Response.End() '' Terminate all the rest processes
			End If
		Catch ex As Exception
			_log.Error("Could not load vendor grid", ex)
			Response.Write("<h3 style='text-align: center; color: red;'>Unable to render content. Please try again later</h3>")
		End Try
	End Sub
	Private Sub LoadAddVendorForm()
		Try
			Dim control As Sm_Uc_AddLenderVendorForm = CType(LoadControl("~/Sm/Uc/AddLenderVendorForm.ascx"), Sm_Uc_AddLenderVendorForm)
			Me.Controls.Add(control)
		Catch ex As Exception
			_log.Error("Could not load add vendor form", ex)
			Response.Write("<h3 style='text-align: center; color: red;'>Unable to render content. Please try again later</h3>")
		End Try
	End Sub

	Private Sub SaveNewLenderVendor()
		Dim res As CJsonResponse
		Try
			Dim smBL As New SmBL()
			Dim lenderID As Guid = Common.SafeGUID(Request.Form("lenderID"))
			Dim backOfficeLenderID As Guid = Common.SafeGUID(Request.Form("bid"))
			Dim backOfficeLender As SmBackOfficeLender = smBL.GetBackOfficeLenderByID(backOfficeLenderID)
			If backOfficeLender Is Nothing Then
				Response.StatusCode = 404 '' Forbidden
				Response.StatusDescription = "Not found"
				Response.End() '' Terminate all the rest processes
			End If
			Dim vendorObj = JsonConvert.DeserializeObject(Of SmLenderVendor)(Common.SafeString(Request.Form("vendor_data")), New JsonSerializerSettings() With {.NullValueHandling = NullValueHandling.Ignore}).MakeItSafe()
			Dim existedLenderVendor As SmLenderVendor = smBL.GetLenderVendorByName(lenderID, vendorObj.VendorName)
			If existedLenderVendor IsNot Nothing Then
				res = New CJsonResponse(False, "Vendor Name has been used", "EXISTED")
			Else
				vendorObj.LenderVendorID = Guid.NewGuid()
				vendorObj.LenderID = backOfficeLender.LenderID
				vendorObj.VendorID = Guid.NewGuid().ToString()
				Dim result = smBL.AddLenderVendor(vendorObj, UserInfo.UserID, Request.ServerVariables("REMOTE_ADDR"))
				If result = "ok" Then
					res = New CJsonResponse(True, "OK", New With {.id = vendorObj.VendorID, .name = vendorObj.VendorName})
				Else
					res = New CJsonResponse(False, "FAILED", "")
				End If
			End If
		Catch ex As Exception
			_log.Error("Could not SaveNewLenderVendor.", ex)
			res = New CJsonResponse(False, "Could not add new vendor. Please try again", "FAILED")
		End Try
		Response.Write(JsonConvert.SerializeObject(res))
		Response.End()
	End Sub

	Private Sub ChangeLenderVendorState()
		Dim res As CJsonResponse = Nothing
		Try
			Dim smBL As New SmBL()
			Dim lenderVendorID As Guid = Common.SafeGUID(Request.Form("lenderVendorID"))
			Dim comment As String = Common.SafeStripHtmlString(Request.Form("comment"))
			Dim isEnabled As String = Common.SafeStripHtmlString(Request.Form("enable")).ToUpper()
			Dim ipAddress As String = Request.ServerVariables("REMOTE_ADDR")
			Dim backOfficeLenderID As Guid = Common.SafeGUID(Request.Form("bid"))
			Dim backOfficeLender As SmBackOfficeLender = smBL.GetBackOfficeLenderByID(backOfficeLenderID)
			Dim lenderVendor = smBL.GetLenderVendorByLenderVendorID(lenderVendorID)
			If backOfficeLender IsNot Nothing AndAlso lenderVendor IsNot Nothing Then
				Dim result = smBL.ChangeLenderVendorState(lenderVendor, IIf(isEnabled = "Y", SmSettings.LenderVendorStatus.Active, SmSettings.LenderVendorStatus.InActive), UserInfo.UserID, ipAddress, comment)
				If result = "ok" Then
					res = New CJsonResponse(True, "", "OK")
				Else
					res = New CJsonResponse(False, "", "FAILED")
				End If
			Else
				Response.StatusCode = 404 '' Forbidden
				Response.StatusDescription = "Not found"
				Response.End() '' Terminate all the rest processes
			End If
		Catch ex As Exception
			_log.Error("Could not ChangeLenderVendorState.", ex)
			res = New CJsonResponse(False, "Could not change state of vendor. Please try again", "FAILED")
		End Try
		Response.Write(JsonConvert.SerializeObject(res))
		Response.End()
	End Sub

	Private Sub LoadEditLenderVendorForm()
		Try
			Dim control As Sm_Uc_EditLenderVendorForm = CType(LoadControl("~/Sm/Uc/EditLenderVendorForm.ascx"), Sm_Uc_EditLenderVendorForm)
			Dim lenderVendorID As Guid = Common.SafeGUID(Request.Form("lenderVendorID"))
			Dim smBL As New SmBL
			Dim vendor = smBL.GetLenderVendorByLenderVendorID(lenderVendorID)
			If vendor Is Nothing Then
				Response.StatusCode = 404 '' Forbidden
				Response.StatusDescription = "Not found"
				Response.End() '' Terminate all the rest processes
			End If
			control.Vendor = vendor
			Me.Controls.Add(control)
		Catch ex As Exception
			_log.Error("Could not load edit lender vendor form", ex)
			Response.Write("<h3 style='text-align: center; color: red;'>Unable to load edit vendor form. Please try again later</h3>")
		End Try
	End Sub

	Private Sub UpdateLenderVendor()
		Dim res As CJsonResponse = Nothing
		Try
			Dim smBL As New SmBL()
			Dim vendorObj = JsonConvert.DeserializeObject(Of SmLenderVendor)(Common.SafeString(Request.Form("vendor_data")), New JsonSerializerSettings() With {.NullValueHandling = NullValueHandling.Ignore}).MakeItSafe()
			vendorObj.VendorID = Common.SafeStripHtmlString(vendorObj.VendorID)

			Dim existedLenderVendor As SmLenderVendor = smBL.GetLenderVendorByLenderVendorID(vendorObj.LenderVendorID)
			If vendorObj Is Nothing OrElse vendorObj.LenderVendorID = Guid.Empty OrElse existedLenderVendor Is Nothing Then
				Response.StatusCode = 404 '' Forbidden
				Response.StatusDescription = "Not found"
				Response.End() '' Terminate all the rest processes
			End If
			vendorObj.LenderID = existedLenderVendor.LenderID
			vendorObj.VendorID = existedLenderVendor.VendorID
			existedLenderVendor = smBL.GetLenderVendorByName(existedLenderVendor.LenderID, vendorObj.VendorName)
			If existedLenderVendor IsNot Nothing AndAlso existedLenderVendor.LenderVendorID <> vendorObj.LenderVendorID Then
				res = New CJsonResponse(False, "Vendor ID has been used", "EXISTED")
			Else
				Dim result = smBL.UpdateLenderVendor(vendorObj, UserInfo.UserID, Request.ServerVariables("REMOTE_ADDR"))
				If result = "ok" Then
					res = New CJsonResponse(True, "", "OK")
				Else
					res = New CJsonResponse(False, "", "FAILED")
				End If
			End If
		Catch ex As Exception
			_log.Error("Could not UpdateLenderVendor.", ex)
			res = New CJsonResponse(False, "Could not update vendor. Please try again", "FAILED")
		End Try
		Response.Write(JsonConvert.SerializeObject(res))
		Response.End()
	End Sub
	Private Sub UpgradeToLenderAdmin()
		Dim res As CJsonResponse = Nothing
		Try
			Dim smBL As New SmBL()
			Dim backOfficeLenderID As Guid = Common.SafeGUID(Request.Form("bid"))
			Dim email As String = Common.SafeStripHtmlString(Request.Form("email"))
			Dim backOfficeLender As SmBackOfficeLender = smBL.GetBackOfficeLenderByID(backOfficeLenderID)
			Dim u As SmUser = New SmAuthBL().GetUserByEmail(email)

			If backOfficeLender IsNot Nothing And u IsNot Nothing Then
				Dim userRole As New SmUserRole
				userRole.Role = SmSettings.Role.LenderAdmin
				userRole.LenderID = backOfficeLender.LenderID
				userRole.UserID = u.UserID
				userRole.UserRoleID = Guid.NewGuid()
				Dim result = smBL.UpgradeUserRoleToLenderAdmin(userRole, UserInfo.UserID, Request.ServerVariables("REMOTE_ADDR"))
				If result = "ok" Then
					res = New CJsonResponse(True, "", "OK")
				Else
					res = New CJsonResponse(False, "", "FAILED")
				End If
			Else
				Response.StatusCode = 404 '' Forbidden
				Response.StatusDescription = "Not found"
				Response.End() '' Terminate all the rest processes
			End If

		Catch ex As Exception
			_log.Error("Could not UpgradeToLenderAdmin.", ex)
			res = New CJsonResponse(False, String.Format("Could not upgrade role of user ""{0}"" to {1}. Please try again", Common.SafeStripHtmlString(Request.Form("email")), SmSettings.Role.LenderAdmin.GetEnumDescription()), "FAILED")
		End Try
		Response.Write(JsonConvert.SerializeObject(res))
		Response.End()
	End Sub
	Private Sub UpgradeToPortalAdmin()
		Dim res As CJsonResponse = Nothing
		Try
			Dim smBL As New SmBL()
			Dim lenderConfigID As Guid = Common.SafeGUID(Request.Form("lenderConfigID"))
			Dim email As String = Common.SafeStripHtmlString(Request.Form("email"))
			Dim lenderConfig As SmLenderConfigBasicInfo = smBL.GetPortalBasicInfoByLenderConfigID(lenderConfigID)
			Dim u As SmUser = New SmAuthBL().GetUserByEmail(email)

			If lenderConfig IsNot Nothing And u IsNot Nothing Then
				Dim userRole As New SmUserRole
				userRole.Role = SmSettings.Role.PortalAdmin
				userRole.LenderID = lenderConfig.LenderID
				userRole.UserID = u.UserID
				userRole.LenderConfigID = lenderConfigID
				userRole.UserRoleID = Guid.NewGuid()
				Dim result = smBL.UpgradeUserRoleToPortalAdmin(userRole, UserInfo.UserID, Request.ServerVariables("REMOTE_ADDR"))
				If result = "ok" Then
					res = New CJsonResponse(True, "", "OK")
				Else
					res = New CJsonResponse(False, "", "FAILED")
				End If
			Else
				Response.StatusCode = 404 '' Forbidden
				Response.StatusDescription = "Not found"
				Response.End() '' Terminate all the rest processes
			End If

		Catch ex As Exception
			_log.Error("Could not UpgradeToPortalAdmin.", ex)
			res = New CJsonResponse(False, String.Format("Could not upgrade role of user ""{0}"" to {1}. Please try again", Common.SafeStripHtmlString(Request.Form("email")), SmSettings.Role.PortalAdmin.GetEnumDescription()), "FAILED")
		End Try
		Response.Write(JsonConvert.SerializeObject(res))
		Response.End()
	End Sub

	Private Sub DowngradeToPortalAdmin()
		Dim res As CJsonResponse = Nothing
		Try
			Dim smBL As New SmBL()
			Dim lenderConfigID As Guid = Common.SafeGUID(Request.Form("lenderConfigID"))
			Dim email As String = Common.SafeStripHtmlString(Request.Form("email"))
			Dim lenderConfig As SmLenderConfigBasicInfo = smBL.GetPortalBasicInfoByLenderConfigID(lenderConfigID)
			Dim u As SmUser = New SmAuthBL().GetUserByEmail(email)

			If lenderConfig IsNot Nothing And u IsNot Nothing Then
				Dim userRole As New SmUserRole
				userRole.Role = SmSettings.Role.PortalAdmin
				userRole.LenderID = lenderConfig.LenderID
				userRole.UserID = u.UserID
				userRole.LenderConfigID = lenderConfigID
				userRole.UserRoleID = Guid.NewGuid()
				Dim result = smBL.DowngradeUserRoleToPortalAdmin(userRole, UserInfo.UserID, Request.ServerVariables("REMOTE_ADDR"))
				If result = "ok" Then
					res = New CJsonResponse(True, "", IIf(userRole.UserID = UserInfo.UserID, "RELOAD", "OK"))
				Else
					res = New CJsonResponse(False, "", "FAILED")
				End If
			Else
				Response.StatusCode = 404 '' Forbidden
				Response.StatusDescription = "Not found"
				Response.End() '' Terminate all the rest processes
			End If

		Catch ex As Exception
			_log.Error("Could not DowngradeToPortalAdmin.", ex)
			res = New CJsonResponse(False, String.Format("Could not downgrade role of user to {0}. Please try again", SmSettings.Role.PortalAdmin.GetEnumDescription()), "FAILED")
		End Try
		Response.Write(JsonConvert.SerializeObject(res))
		Response.End()
	End Sub
#End Region
#Region "Vendor Group Admin"
	Private Sub LoadVendorGroupAdminGrid()
		Try
			Dim paging = JsonConvert.DeserializeObject(Of SmPagingInfoModel)(Request.Form("paging_info")).MakeItSafe()
			Dim filter = JsonConvert.DeserializeObject(Of SmVendorGroupAdminUserRoleListFilter)(Request.Form("filter_info")).MakeItSafe()
			Dim lenderConfigID As Guid = Common.SafeGUID(Request.Form("lenderConfigID"))
			Dim smBL As New SmBL
			Dim lenderConfig As SmLenderConfigBasicInfo = smBL.GetPortalBasicInfoByLenderConfigID(lenderConfigID)
			If lenderConfig IsNot Nothing Then
				Dim control As Sm_Uc_VendorGroupAdminList = CType(LoadControl("~/Sm/Uc/VendorGroupAdminList.ascx"), Sm_Uc_VendorGroupAdminList)
				control.PagingInfo = paging
				If filter Is Nothing Then
					filter = New SmVendorGroupAdminUserRoleListFilter()
				End If
				filter.LenderConfigID = lenderConfig.LenderConfigID
				control.FilterInfo = filter.MakeItSafe()
				Me.Controls.Add(control)
			Else
				Response.StatusCode = 404 '' Forbidden
				Response.StatusDescription = "Not found"
				Response.End() '' Terminate all the rest processes
			End If
		Catch ex As Exception
			_log.Error("Could not load vendor group admin grid", ex)
			Response.Write("<h3 style='text-align: center; color: red;'>Unable to render content. Please try again later</h3>")
		End Try
	End Sub
	Private Sub LoadAddVendorGroupAdminForm()
		Try
			Dim smBL As New SmBL()
			Dim lenderConfigID As Guid = Common.SafeGUID(Request.Form("lenderConfigID"))

			If smBL.CheckLenderExistedByLenderConfigID(lenderConfigID) Then
				Dim control As Sm_Uc_AddVendorGroupAdminForm = CType(LoadControl("~/Sm/Uc/AddVendorGroupAdminForm.ascx"), Sm_Uc_AddVendorGroupAdminForm)
				control.AvailableRoles.Add(SmSettings.Role.VendorGroupAdmin)
				Me.Controls.Add(control)
			Else
				Response.StatusCode = 404 '' Forbidden
				Response.StatusDescription = "Not found"
				Response.End() '' Terminate all the rest processes
			End If

		Catch ex As Exception
			_log.Error("Could not LoadAddVendorGroupAdminForm.", ex)
			Response.Write("<h3 style='text-align: center; color: red;'>Unable to render content. Please try again later</h3>")
		End Try
	End Sub
	Private Sub GetAvailableTags()
		Try
			Dim smBL As New SmBL()
			Dim result = smBL.GetAvailableTagsByLenderID(Common.SafeGUID(Request.Form("lenderID")))
			If result Is Nothing Then result = New List(Of String)
			Response.Write(JsonConvert.SerializeObject(result))

		Catch ex As Exception
			_log.Error("Could not GetAvailableTags.", ex)
			Response.Write("")
		End Try
	End Sub

	Private Sub SaveNewVendorGroupAdmin()
		Dim res As CJsonResponse
		Try
			Dim smBL As New SmBL()
			Dim smAuthBL As New SmAuthBL()
			Dim email As String = Common.SafeStripHtmlString(Request.Form("email"))
			Dim tags As String = Common.SafeStripHtmlString(Request.Form("tags"))
			Dim ipAddress As String = Request.ServerVariables("REMOTE_ADDR")
			Dim lenderConfigID As Guid = Common.SafeGUID(Request.Form("lenderConfigID"))
			Dim lenderConfig As SmLenderConfigBasicInfo = smBL.GetPortalBasicInfoByLenderConfigID(lenderConfigID)
			Dim force As Boolean = Common.SafeBoolean(Request.Form("force"))
			Dim backOfficeLender As SmBackOfficeLender
			If lenderConfig IsNot Nothing Then
				backOfficeLender = smBL.GetBackOfficeLenderByLenderConfigID(lenderConfig.LenderConfigID)
				Dim u As SmUser = smAuthBL.GetUserByEmail(email)
				If u IsNot Nothing Then
					Dim roleList = smBL.GetLenderUserRoleList(u.UserID, lenderConfig.LenderID)
					If roleList Is Nothing OrElse roleList.Count = 0 OrElse force = True Then
						'Add record to UserRoles table
						Dim vendorGroupAdminUserRole As New SmUserRole
						vendorGroupAdminUserRole.UserRoleID = Guid.NewGuid()
						vendorGroupAdminUserRole.Role = SmSettings.Role.VendorGroupAdmin
						vendorGroupAdminUserRole.LenderID = lenderConfig.LenderID
						vendorGroupAdminUserRole.LenderConfigID = lenderConfig.LenderConfigID
						vendorGroupAdminUserRole.UserID = u.UserID
						vendorGroupAdminUserRole.AssignedVendorGroupByTags = tags
						Dim result = smBL.AddUserRole(vendorGroupAdminUserRole, UserInfo.UserID, ipAddress, True)

						If result = "ok" Then
							Dim emailContent As String = SmUtil.BuildVendorGroupAdminRoleAssignedNotification(u.FirstName, backOfficeLender.LenderName, lenderConfig.Note)
							SmUtil.SendEmail(u.Email, "MeridianLink's APM Role Assign Notification", emailContent)
							res = New CJsonResponse(True, "", "OK")
						Else
							res = New CJsonResponse(False, result, "FAILED")
						End If
					ElseIf roleList.Any(Function(r) r.Role = SmSettings.Role.VendorGroupAdmin) Then
						If roleList.Any(Function(r) r.Role = SmSettings.Role.VendorGroupAdmin And r.LenderConfigID = lenderConfigID) Then
							res = New CJsonResponse(False, String.Format("This email has been used by an user who is in role {0} already.", SmSettings.Role.VendorGroupAdmin.GetEnumDescription()), "ALREADY")
						Else
							'Add record to UserRoles table
							Dim vendorGroupAdminUserRole As New SmUserRole
							vendorGroupAdminUserRole.UserRoleID = Guid.NewGuid()
							vendorGroupAdminUserRole.Role = SmSettings.Role.VendorGroupAdmin
							vendorGroupAdminUserRole.LenderID = lenderConfig.LenderID
							vendorGroupAdminUserRole.LenderConfigID = lenderConfig.LenderConfigID
							vendorGroupAdminUserRole.UserID = u.UserID
							vendorGroupAdminUserRole.AssignedVendorGroupByTags = tags
							Dim result = smBL.AddUserRole(vendorGroupAdminUserRole, UserInfo.UserID, ipAddress, True)

							If result = "ok" Then
								backOfficeLender = smBL.GetBackOfficeLenderByLenderConfigID(lenderConfig.LenderConfigID)
								Dim emailContent As String = SmUtil.BuildVendorGroupAdminRoleAssignedNotification(u.FirstName, backOfficeLender.LenderName, lenderConfig.Note)
								SmUtil.SendEmail(u.Email, "MeridianLink's APM Role Assign Notification", emailContent)
								res = New CJsonResponse(True, "", "OK")
							Else
								res = New CJsonResponse(False, result, "FAILED")
							End If
						End If
					ElseIf roleList.Any(Function(r) r.Role = SmSettings.Role.LenderAdmin) Then
						If SmUtil.CheckRoleAccess(HttpContext.Current, SmSettings.Role.SuperUser.ToString(), String.Format("LenderScope${0}${1}", SmSettings.Role.LenderAdmin.ToString(), backOfficeLender.LenderID.ToString())) Then
							res = New CJsonResponse(False, String.Format("This email has been used with role of {1}. Are you sure about downgrading this user to {0}?", SmSettings.Role.VendorGroupAdmin.GetEnumDescription(), SmSettings.Role.LenderAdmin.GetEnumDescription()), "DOWNGRADE")
						Else
							res = New CJsonResponse(False, String.Format("This email has been used by an user who is in role {0} already.", SmSettings.Role.LenderAdmin.GetEnumDescription()), "ALREADY")
						End If
					ElseIf roleList.Any(Function(r) r.Role = SmSettings.Role.PortalAdmin) Then
						res = New CJsonResponse(False, String.Format("This email has been used with role of {1}. Are you sure about downgrading this user to {0}?", SmSettings.Role.VendorGroupAdmin.GetEnumDescription(), SmSettings.Role.PortalAdmin.GetEnumDescription()), "DOWNGRADE")
					Else
						res = New CJsonResponse(False, String.Format("This email has been used by an user who is not in role {0}. Are you sure about granting the {0} role to this user?", SmSettings.Role.VendorGroupAdmin.GetEnumDescription()), "UPGRADE")
					End If
				Else
					Dim token As String = Guid.NewGuid().ToString().Replace("-", "").Substring(0, 20)
					Dim newUser As New SmUser()
					newUser.UserID = Guid.NewGuid()
					newUser.ExpireDays = 0 'by default
					newUser.Role = SmSettings.Role.User
					newUser.Email = email
					Dim result = smAuthBL.AddUser(newUser, UserInfo.UserID, ipAddress, token)
					If result = "ok" Then
						Dim newUserRole As New SmUserRole
						newUserRole.UserRoleID = Guid.NewGuid()
						newUserRole.Role = SmSettings.Role.VendorGroupAdmin
						newUserRole.LenderConfigID = lenderConfig.LenderConfigID
						newUserRole.LenderID = lenderConfig.LenderID
						newUserRole.UserID = newUser.UserID
						newUserRole.AssignedVendorGroupByTags = tags
						smBL.AddUserRole(newUserRole, UserInfo.UserID, ipAddress)
						Dim emailContent As String = SmUtil.BuildActivateAccountEmail(token, _serverRoot)
						SmUtil.SendEmail(newUser.Email, "MeridianLink's APM Account Activation", emailContent)
						'not sure why system need to send this 2nd notification, Firstname is nost yet available
						'->To inform user about the role he/she has been assigned to
						emailContent = SmUtil.BuildVendorGroupAdminRoleAssignedNotification(String.Empty, backOfficeLender.LenderName, lenderConfig.Note)
						SmUtil.SendEmail(newUser.Email, "MeridianLink's APM Role Assign Notification", emailContent)
						res = New CJsonResponse(True, "", "OK")
					Else
						res = New CJsonResponse(False, "", "FAILED")
					End If
				End If
			Else
				Response.StatusCode = 404 '' Forbidden
				Response.StatusDescription = "Not found"
				Response.End() '' Terminate all the rest processes
				Return
			End If
		Catch ex As Exception
			_log.Error("Could not AddVendorGroupAdmin.", ex)
			res = New CJsonResponse(False, "Could not add vendor group admin. Please try again", "FAILED")
		End Try
		Response.Write(JsonConvert.SerializeObject(res))
		Response.End()
	End Sub
	Private Sub DowngradeToVendorGroupAdmin()
		Dim res As CJsonResponse = Nothing
		Try
			Dim smBL As New SmBL()
			Dim lenderConfigID As Guid = Common.SafeGUID(Request.Form("lenderConfigID"))
			Dim email As String = Common.SafeStripHtmlString(Request.Form("email"))
			Dim tags As String = Common.SafeStripHtmlString(Request.Form("tags"))
			Dim lenderConfig As SmLenderConfigBasicInfo = smBL.GetPortalBasicInfoByLenderConfigID(lenderConfigID)
			Dim u As SmUser = New SmAuthBL().GetUserByEmail(email)

			If lenderConfig IsNot Nothing And u IsNot Nothing Then
				Dim userRole As New SmUserRole
				userRole.Role = SmSettings.Role.VendorGroupAdmin
				userRole.LenderID = lenderConfig.LenderID
				userRole.UserID = u.UserID
				userRole.LenderConfigID = lenderConfigID
				userRole.AssignedVendorGroupByTags = tags
				userRole.UserRoleID = Guid.NewGuid()
				Dim result = smBL.DowngradeUserRoleToVendorGroupAdmin(userRole, UserInfo.UserID, Request.ServerVariables("REMOTE_ADDR"))
				If result = "ok" Then
					res = New CJsonResponse(True, "", IIf(userRole.UserID = UserInfo.UserID, "RELOAD", "OK"))
				Else
					res = New CJsonResponse(False, "", "FAILED")
				End If
			Else
				Response.StatusCode = 404 '' Forbidden
				Response.StatusDescription = "Not found"
				Response.End() '' Terminate all the rest processes
			End If

		Catch ex As Exception
			_log.Error("Could not DowngradeToVendorGroupAdmin.", ex)
			res = New CJsonResponse(False, String.Format("Could not downgrade role of user to {0}. Please try again", SmSettings.Role.VendorGroupAdmin.GetEnumDescription()), "FAILED")
		End Try
		Response.Write(JsonConvert.SerializeObject(res))
		Response.End()
	End Sub

	Private Sub ChangeVendorGroupAdminState()
		Dim res As CJsonResponse = Nothing
		Try
			Dim smBL As New SmBL()
			Dim smAuthBL As New SmAuthBL()
			Dim userRoleID As Guid = Common.SafeGUID(Request.Form("userRoleID"))
			Dim comment As String = Common.SafeStripHtmlString(Request.Form("comment"))
			Dim isEnabled As String = Common.SafeStripHtmlString(Request.Form("enable")).ToUpper()
			Dim ipAddress As String = Request.ServerVariables("REMOTE_ADDR")
			Dim lenderConfigID As Guid = Common.SafeGUID(Request.Form("lenderConfigID"))
			Dim lenderConfig As SmLenderConfigBasicInfo = smBL.GetPortalBasicInfoByLenderConfigID(lenderConfigID)
			Dim userRole As SmUserRole = smBL.GetUserRoleByID(userRoleID)
			Dim u As SmUser = Nothing
			If userRole IsNot Nothing Then
				u = smAuthBL.GetUserByID(userRole.UserID)
			End If
			If lenderConfig IsNot Nothing AndAlso userRole IsNot Nothing AndAlso u IsNot Nothing Then
				Dim result = smBL.ChangeVendorGroupAdminUserRoleState(userRoleID, IIf(isEnabled = "Y", SmSettings.UserRoleStatus.Active, SmSettings.UserRoleStatus.InActive), lenderConfig, u, UserInfo.UserID, ipAddress, comment)
				If result = "ok" Then
					res = New CJsonResponse(True, "", IIf(u.UserID = UserInfo.UserID, "RELOAD", "OK"))
				Else
					res = New CJsonResponse(False, "", "FAILED")
				End If
			Else
				Response.StatusCode = 404 '' Forbidden
				Response.StatusDescription = "Not found"
				Response.End() '' Terminate all the rest processes
			End If
		Catch ex As Exception
			_log.Error("Could not ChangeVendorGroupAdminState.", ex)
			res = New CJsonResponse(False, "Could not change role state of vendor group admin. Please try again", "FAILED")
		End Try
		Response.Write(JsonConvert.SerializeObject(res))
		Response.End()
	End Sub

	Private Sub LoadEditVendorGroupAdminForm()
		Try
			Dim control As Sm_Uc_EditVendorGroupAdminForm = CType(LoadControl("~/Sm/Uc/EditVendorGroupAdminForm.ascx"), Sm_Uc_EditVendorGroupAdminForm)
			Dim userRoleID As Guid = Common.SafeGUID(Request.Form("userRoleID"))
			Dim smBL As New SmBL
			Dim userRole = smBL.GetUserRoleByID(userRoleID)
			If userRole Is Nothing Then
				Response.StatusCode = 404 '' Forbidden
				Response.StatusDescription = "Not found"
				Response.End() '' Terminate all the rest processes
			End If
			Dim user = New SmAuthBL().GetUserByID(userRole.UserID)
			If user Is Nothing Then
				Response.StatusCode = 404 '' Forbidden
				Response.StatusDescription = "Not found"
				Response.End() '' Terminate all the rest processes
			End If
			control.AvailableRoles.Add(SmSettings.Role.VendorGroupAdmin)
			control.UserRole = userRole
			control.User = user
			Me.Controls.Add(control)
		Catch ex As Exception
			_log.Error("Could not LoadEditVendorGroupAdminForm", ex)
			Response.Write("<h3 style='text-align: center; color: red;'>Unable to load edit vendor group admin form. Please try again later</h3>")
		End Try
	End Sub

	Private Sub UpdateVendorGroupAdmin()
		Dim res As CJsonResponse = Nothing
		Try
			Dim smBL As New SmBL()
			Dim userRoleID As Guid = Common.SafeGUID(Request.Form("userRoleID"))
			Dim tags As String = Common.SafeString(Request.Form("tags"))
			Dim ipAddress As String = Request.ServerVariables("REMOTE_ADDR")

			Dim userRole = smBL.GetUserRoleByID(userRoleID)
			If userRole Is Nothing Then
				Response.StatusCode = 404 '' Forbidden
				Response.StatusDescription = "Not found"
				Response.End() '' Terminate all the rest processes
			Else
				userRole.AssignedVendorGroupByTags = tags
				Dim result = smBL.UpdateUserRole(userRole, UserInfo.UserID, ipAddress)
				If result = "ok" Then
					res = New CJsonResponse(True, "", "OK")
				Else
					res = New CJsonResponse(False, "", "FAILED")
				End If
			End If
		Catch ex As Exception
			_log.Error("Could not UpdateVendorGroupAdmin.", ex)
			res = New CJsonResponse(False, "Could not update vendor group admin data. Please try again", "FAILED")
		End Try
		Response.Write(JsonConvert.SerializeObject(res))
		Response.End()
	End Sub

	Private Sub RemoveVendorGroupAdmin()
		Dim res As CJsonResponse = Nothing
		Try
			Dim smBL As New SmBL()
			Dim smAuthBL As New SmAuthBL()
			Dim userRoleID As Guid = Common.SafeGUID(Request.Form("userRoleID"))
			Dim comment As String = Common.SafeString(Request.Form("comment"))
			Dim ipAddress As String = Request.ServerVariables("REMOTE_ADDR")
			Dim lenderConfigID As Guid = Common.SafeGUID(Request.Form("lenderConfigID"))
			Dim lenderConfig As SmLenderConfigBasicInfo = smBL.GetPortalBasicInfoByLenderConfigID(lenderConfigID)
			Dim userRole As SmUserRole = smBL.GetUserRoleByID(userRoleID)
			Dim u As SmUser = Nothing
			If userRole IsNot Nothing Then
				u = smAuthBL.GetUserByID(userRole.UserID)
			End If
			If lenderConfig IsNot Nothing AndAlso userRole IsNot Nothing AndAlso u IsNot Nothing Then
				Dim result = smBL.RemoveVendorGroupAdminUserRole(userRoleID, comment, lenderConfig, u, UserInfo.UserID, ipAddress)
				If result = "ok" Then
					res = New CJsonResponse(True, "", "OK")
				Else
					res = New CJsonResponse(False, "", "FAILED")
				End If
			Else
				Response.StatusCode = 404 '' Forbidden
				Response.StatusDescription = "Not found"
				Response.End() '' Terminate all the rest processes
			End If
		Catch ex As Exception
			_log.Error("Could not RemoveVendorGroupAdmin.", ex)
			res = New CJsonResponse(False, "Could not remove vendor group admin. Please try again", "FAILED")
		End Try
		Response.Write(JsonConvert.SerializeObject(res))
		Response.End()
	End Sub
#End Region
#Region "Vendor Users"
	Private Sub LoadVendorUserGrid()
		Try
			Dim paging = JsonConvert.DeserializeObject(Of SmPagingInfoModel)(Request.Form("paging_info")).MakeItSafe()
			Dim filter = JsonConvert.DeserializeObject(Of SmVendorUserRoleListFilter)(Request.Form("filter_info")).MakeItSafe()
			Dim lenderConfigID As Guid = Common.SafeGUID(Request.Form("lenderConfigID"))
			Dim smBL As New SmBL
			Dim lenderConfig As SmLenderConfigBasicInfo = smBL.GetPortalBasicInfoByLenderConfigID(lenderConfigID)
			If lenderConfig IsNot Nothing Then
				Dim control As Sm_Uc_VendorUserList = CType(LoadControl("~/Sm/Uc/VendorUserList.ascx"), Sm_Uc_VendorUserList)
				control.PagingInfo = paging
				If filter Is Nothing Then
					filter = New SmVendorUserRoleListFilter()
				End If
				filter.LenderConfigID = lenderConfig.LenderConfigID
				control.LenderConfig = lenderConfig
				filter.Roles.Add(SmSettings.Role.VendorAdmin)
				control.FilterInfo = filter.MakeItSafe()
				Me.Controls.Add(control)
			Else
				Response.StatusCode = 404 '' Forbidden
				Response.StatusDescription = "Not found"
				Response.End() '' Terminate all the rest processes
			End If
		Catch ex As Exception
			_log.Error("Could not load vendor user grid", ex)
			Response.Write("<h3 style='text-align: center; color: red;'>Unable to render content. Please try again later</h3>")
		End Try
	End Sub
	Private Sub LoadAddVendorUserForm()
		Dim res As CJsonResponse = Nothing
		Try
			Dim smBL As New SmBL()
			Dim lenderConfigID As Guid = Common.SafeGUID(Request.Form("lenderConfigID"))
			Dim lenderConfig As SmLenderConfigBasicInfo = smBL.GetPortalBasicInfoByLenderConfigID(lenderConfigID)

			If lenderConfig IsNot Nothing Then
				Dim control As Sm_Uc_AddVendorUserForm = CType(LoadControl("~/Sm/Uc/AddVendorUserForm.ascx"), Sm_Uc_AddVendorUserForm)
				control.LenderConfig = lenderConfig
				control.AvailableRoles.Add(SmSettings.Role.VendorAdmin)
				Me.Controls.Add(control)
			Else
				Response.StatusCode = 404 '' Forbidden
				Response.StatusDescription = "Not found"
				Response.End() '' Terminate all the rest processes
			End If

		Catch ex As Exception
			_log.Error("Could not LoadAddVendorUserForm.", ex)
			Response.Write("<h3 style='text-align: center; color: red;'>Unable to render content. Please try again later</h3>")
		End Try
	End Sub
	Private Sub SaveNewVendorUser()
		Dim res As CJsonResponse
		Try
			Dim smBL As New SmBL()
			Dim smAuthBL As New SmAuthBL()
			Dim lenderConfigID As Guid = Common.SafeGUID(Request.Form("lenderConfigID"))
			Dim lenderConfig As SmLenderConfigBasicInfo = smBL.GetPortalBasicInfoByLenderConfigID(lenderConfigID)
			Dim ipAddress As String = Request.ServerVariables("REMOTE_ADDR")
			Dim email = Common.SafeStripHtmlString(Request.Form("email"))
			Dim force As Boolean = Common.SafeBoolean(Request.Form("force"))
			Dim role As SmSettings.Role = CType([Enum].Parse(GetType(SmSettings.Role), Common.SafeStripHtmlString(Request.Form("role"))), SmSettings.Role)
			If lenderConfig Is Nothing OrElse role <> SmSettings.Role.VendorAdmin OrElse String.IsNullOrEmpty(email) Then
				Response.StatusCode = 404 '' Forbidden
				Response.StatusDescription = "Not found"
				Response.End() '' Terminate all the rest processes
				Return
			End If

			Dim vendor As SmLenderVendor = smBL.GetLenderVendorByVendorID(Common.SafeStripHtmlString(Request.Form("vendorID")))
			If vendor IsNot Nothing AndAlso lenderConfig.LenderID = vendor.LenderID Then
				Dim existedUser As SmUser = smAuthBL.GetUserByEmail(email)


				Dim lenderRoleList As New List(Of SmUserRole)
				If existedUser IsNot Nothing Then
					lenderRoleList = smBL.GetLenderUserRoleList(existedUser.UserID, lenderConfig.LenderID)
				End If
				Dim currentRoleOnVendor As SmUserRole = Nothing
				If lenderRoleList IsNot Nothing Then
					currentRoleOnVendor = lenderRoleList.FirstOrDefault(Function(r) r.LenderConfigID = lenderConfig.LenderConfigID AndAlso r.VendorID = vendor.VendorID)
				End If
				If currentRoleOnVendor IsNot Nothing AndAlso currentRoleOnVendor.Role = SmSettings.Role.VendorAdmin Then
					'check vendoradmin/vendoruser already existed
					res = New CJsonResponse(False, String.Format("This email has been being used by an {0} of {1} already.", SmSettings.Role.VendorAdmin.GetEnumDescription(), vendor.VendorName), "ALREADY")
				ElseIf force = False AndAlso currentRoleOnVendor IsNot Nothing AndAlso currentRoleOnVendor.Role = SmSettings.Role.VendorUser Then
					res = New CJsonResponse(False, String.Format("This email has been used by an user of {0} . Do you want to upgrade this user to {1}?", SmSettings.Role.VendorUser.GetEnumDescription(), SmSettings.Role.VendorAdmin.GetEnumDescription()), "UPGRADE")
				Else
					Dim userObj As New SmUser
					If existedUser Is Nothing Then
						userObj.UserID = Guid.NewGuid()
						userObj.Role = SmSettings.Role.User
						userObj.Email = email
					Else
						userObj = existedUser
					End If
					Dim userRole As New SmUserRole() With {
											  .LenderConfigID = lenderConfig.LenderConfigID,
											  .LenderID = lenderConfig.LenderID,
											  .Role = SmSettings.Role.VendorAdmin,
											  .Status = SmSettings.UserRoleStatus.Active,
											  .UserID = userObj.UserID,
											  .UserRoleID = Guid.NewGuid(),
											  .VendorID = vendor.VendorID,
											  .CreatedDate = Now
												}
					Dim result As String = ""
					If currentRoleOnVendor Is Nothing Then
						result = smBL.AddUserRole(userRole, UserInfo.UserID, ipAddress)
					Else
						currentRoleOnVendor.Role = SmSettings.Role.VendorAdmin
						result = smBL.UpdateUserRole(currentRoleOnVendor, UserInfo.UserID, ipAddress)
					End If

					If result = "ok" Then
						If existedUser Is Nothing Then
							Dim token As String = Guid.NewGuid().ToString().Replace("-", "").Substring(0, 20)
							userObj.ExpireDays = 0 ' leave it by default
							result = smAuthBL.AddUser(userObj, UserInfo.UserID, ipAddress, token)
							If result <> "ok" Then
								'rollback adding user role action
								smBL.RemoveUserRoleByUserRoleID(userRole.UserRoleID)
								Response.Write(JsonConvert.SerializeObject(New CJsonResponse(False, result, "FAILED")))
								Response.End()
								Return
							End If
							SmUtil.SendEmail(userObj.Email, "MeridianLink's APM Account Activation", SmUtil.BuildActivateAccountEmail(token, _serverRoot))
						End If
						Dim backOfficeLender = smBL.GetBackOfficeLenderByLenderConfigID(lenderConfig.LenderConfigID)
						Dim emailContent As String = SmUtil.BuildVendorRoleAssignedNotification(userObj.FirstName, backOfficeLender.LenderName, lenderConfig.Note, vendor.VendorName)
						SmUtil.SendEmail(userObj.Email, "MeridianLink's APM Role Assign Notification", emailContent)
						res = New CJsonResponse(True, "", "OK")
					Else
						res = New CJsonResponse(False, result, "FAILED")
					End If
				End If
			Else
				res = New CJsonResponse(False, "Bad data", "FAILED")
			End If
		Catch ex As Exception
			_log.Error("Could not SaveNewVendorUser.", ex)
			res = New CJsonResponse(False, "Could not add new vendor user. Please try again", "FAILED")
		End Try
		Response.Write(JsonConvert.SerializeObject(res))
		Response.End()
	End Sub

	Private Sub RemoveVendorUser()
		Dim res As CJsonResponse = Nothing
		Try
			Dim smBL As New SmBL()
			Dim smAuthBL As New SmAuthBL()
			Dim userRoleID As Guid = Common.SafeGUID(Request.Form("userRoleID"))
			Dim comment As String = Common.SafeString(Request.Form("comment"))
			Dim ipAddress As String = Request.ServerVariables("REMOTE_ADDR")
			Dim lenderConfigID As Guid = Common.SafeGUID(Request.Form("lenderConfigID"))
			Dim lenderConfig = smBL.GetPortalBasicInfoByLenderConfigID(lenderConfigID)
			Dim userRole As SmUserRole = smBL.GetUserRoleByID(userRoleID)
			Dim u As SmUser
			If userRole IsNot Nothing Then
				u = smAuthBL.GetUserByID(userRole.UserID)
			End If
			If lenderConfig IsNot Nothing AndAlso userRole IsNot Nothing AndAlso u IsNot Nothing Then
				Dim result = smBL.RemoveVendorUser(userRole, comment, lenderConfig, u, UserInfo.UserID, ipAddress)
				If result = "ok" Then
					res = New CJsonResponse(True, "", "OK")
				Else
					res = New CJsonResponse(False, "", "FAILED")
				End If
			Else
				Response.StatusCode = 404 '' Forbidden
				Response.StatusDescription = "Not found"
				Response.End() '' Terminate all the rest processes
			End If
		Catch ex As Exception
			_log.Error("Could not RemoveVendorUser.", ex)
			res = New CJsonResponse(False, "Could not remove vendor user. Please try again", "FAILED")
		End Try
		Response.Write(JsonConvert.SerializeObject(res))
		Response.End()
	End Sub

	Private Sub ChangeVendorUserState()
		Dim res As CJsonResponse = Nothing
		Try
			Dim smBL As New SmBL()
			Dim smAuthBL As New SmAuthBL()
			Dim userRoleID As Guid = Common.SafeGUID(Request.Form("userRoleID"))
			Dim comment As String = Common.SafeStripHtmlString(Request.Form("comment"))
			Dim isEnabled As String = Common.SafeStripHtmlString(Request.Form("enable")).ToUpper()
			Dim ipAddress As String = Request.ServerVariables("REMOTE_ADDR")
			Dim lenderConfigID As Guid = Common.SafeGUID(Request.Form("lenderConfigID"))
			Dim lenderConfig = smBL.GetPortalBasicInfoByLenderConfigID(lenderConfigID)
			Dim userRole As SmUserRole = smBL.GetUserRoleByID(userRoleID)
			Dim u As SmUser = Nothing
			If userRole IsNot Nothing Then
				u = smAuthBL.GetUserByID(userRole.UserID)
			End If
			If lenderConfig IsNot Nothing AndAlso userRole IsNot Nothing AndAlso u IsNot Nothing Then
				Dim result = smBL.ChangeVendorUserRoleState(userRoleID, IIf(isEnabled = "Y", SmSettings.UserRoleStatus.Active, SmSettings.UserRoleStatus.InActive), lenderConfig, u, userRole.Vendor, UserInfo.UserID, ipAddress, comment)
				If result = "ok" Then
					res = New CJsonResponse(True, "", "OK")
				Else
					res = New CJsonResponse(False, "", "FAILED")
				End If
			Else
				Response.StatusCode = 404 '' Forbidden
				Response.StatusDescription = "Not found"
				Response.End() '' Terminate all the rest processes
			End If
		Catch ex As Exception
			_log.Error("Could not ChangeVendorUserState.", ex)
			res = New CJsonResponse(False, "Could not change role state of vendor user. Please try again", "FAILED")
		End Try
		Response.Write(JsonConvert.SerializeObject(res))
		Response.End()
	End Sub
#End Region

#Region "Landing Pages"
	Private Sub LoadLandingPageGrid()
		Try
			Dim paging = JsonConvert.DeserializeObject(Of SmPagingInfoModel)(Request.Form("paging_info"))
			Dim filter = JsonConvert.DeserializeObject(Of SmLandingPageListFilter)(Request.Form("filter_info"))
			Dim lenderConfigID = Common.SafeGUID(Request.Form("lenderConfigID"))
			Dim control As Sm_Uc_LandingPageList = CType(LoadControl("~/Sm/Uc/LandingPageList.ascx"), Sm_Uc_LandingPageList)
			control.PagingInfo = paging
			If filter Is Nothing Then
				filter = New SmLandingPageListFilter()
			End If
			filter.LenderConfigID = lenderConfigID
			control.FilterInfo = filter.MakeItSafe()
			Me.Controls.Add(control)
		Catch ex As Exception
			_log.Error("Could not load landing page grid", ex)
			Response.Write("<h3 style='text-align: center; color: red;'>Unable to render content. Please try again later</h3>")
		End Try
	End Sub

	Private Sub LoadAddLandingPageForm()
		Try
			Dim control As Sm_Uc_AddLandingPageForm = CType(LoadControl("~/Sm/Uc/AddLandingPageForm.ascx"), Sm_Uc_AddLandingPageForm)
			Dim lenderConfigID As Guid = Common.SafeGUID(Request.Form("lenderConfigID"))
			control.LenderConfigID = lenderConfigID
			Me.Controls.Add(control)
		Catch ex As Exception
			_log.Error("Could not load add landing page form", ex)
			Response.Write("<h3 style='text-align: center; color: red;'>Unable to render data. Please try again later</h3>")
		End Try
	End Sub

	Private Sub SaveNewLandingPage()
		Dim res As CJsonResponse
		Try
			Dim smBL As New SmBL()
			Dim landingPageObj = JsonConvert.DeserializeObject(Of SmLandingPage)(Common.SafeString(Request.Form("landingpage_data")), New JsonSerializerSettings() With {.NullValueHandling = NullValueHandling.Ignore}).MakeItSafe()

			Dim landingPage = smBL.GetLandingPageByRefID(landingPageObj.RefID)

			If landingPage IsNot Nothing Then
				res = New CJsonResponse(False, "Home Page ID has been used.", "EXISTED")
			Else
				landingPageObj.LandingPageID = Guid.NewGuid()
				landingPageObj.PageData = ""
				landingPageObj.Status = SmSettings.LandingPageStatus.InActive
				landingPageObj.CreatedBy = UserInfo.UserID
				Dim template As String = Common.SafeStripHtmlString(Request.Form("template"))
				If Not String.IsNullOrEmpty(template) Then
					Dim encrypter As New CRijndaelManaged()
					template = encrypter.DecryptUtf8(template)
					If File.Exists(Server.MapPath("~/App_Data/LandingPageTemplates/" & template)) Then
						landingPageObj.PageData = File.ReadAllText(Server.MapPath("~/App_Data/LandingPageTemplates/" & template)) ' load default page template
					End If
				End If
				Dim result = smBL.AddLandingPage(landingPageObj, UserInfo.UserID, Request.ServerVariables("REMOTE_ADDR"))
				If result = "ok" Then
					res = New CJsonResponse(True, "OK", New With {.id = landingPageObj.LandingPageID})
				Else
					res = New CJsonResponse(False, "FAILED", "")
				End If
			End If
		Catch ex As Exception
			_log.Error("Could not SaveNewLandingPage.", ex)
			res = New CJsonResponse(False, "Could not add new landing page. Please try again", "FAILED")
		End Try
		Response.Write(JsonConvert.SerializeObject(res))
		Response.End()
	End Sub

	Private Sub LoadEditLandingPageForm()
		Try
			Dim control As Sm_Uc_EditLandingPageForm = CType(LoadControl("~/Sm/Uc/EditLandingPageForm.ascx"), Sm_Uc_EditLandingPageForm)
			Dim landingPageID As Guid = Common.SafeGUID(Request.Form("landingPageId"))
			Dim smBL As New SmBL
			Dim landingPage = smBL.GetLandingPageByID(landingPageID)
			If landingPage Is Nothing Then
				Response.StatusCode = 404 '' Forbidden
				Response.StatusDescription = "Not found"
				Response.End() '' Terminate all the rest processes
			End If
			control.LandingPage = landingPage
			Me.Controls.Add(control)
		Catch ex As Exception
			_log.Error("Could not load edit landing page form", ex)
			Response.Write("<h3 style='text-align: center; color: red;'>Unable to load edit user form. Please try again later</h3>")
		End Try
	End Sub

	Private Sub UpdateLandingPage()
		Dim res As CJsonResponse
		Try
			Dim smBL As New SmBL()
			Dim landingPageObj = JsonConvert.DeserializeObject(Of SmLandingPage)(Common.SafeString(Request.Form("landingpage_data")), New JsonSerializerSettings() With {.NullValueHandling = NullValueHandling.Ignore}).MakeItSafe()
			Dim landingPage = smBL.GetLandingPageByID(landingPageObj.LandingPageID)
			If landingPage Is Nothing Then
				Response.StatusCode = 404 '' Forbidden
				Response.StatusDescription = "Not found"
				Response.End() '' Terminate all the rest processes
			End If
			Dim landingPageWithRefID = smBL.GetLandingPageByRefID(landingPageObj.RefID)
			If landingPageWithRefID IsNot Nothing AndAlso landingPageWithRefID.LandingPageID <> landingPage.LandingPageID Then
				res = New CJsonResponse(False, "Home Page ID has been used", "EXISTED")
			Else
				landingPage.Title = landingPageObj.Title
				landingPage.RefID = landingPageObj.RefID
				Dim result = smBL.UpdateLandingPage(landingPage, UserInfo.UserID, Request.ServerVariables("REMOTE_ADDR"))
				If result = "ok" Then
					res = New CJsonResponse(True, "", "OK")
				Else
					res = New CJsonResponse(False, "", "FAILED")
				End If
			End If
		Catch ex As Exception
			_log.Error("Could not UpdateLandingPage.", ex)
			res = New CJsonResponse(False, "Could not update landing page. Please try again", "FAILED")
		End Try
		Response.Write(JsonConvert.SerializeObject(res))
		Response.End()
	End Sub

	Private Sub ChangeLandingPageStatus()
		Dim res As CJsonResponse = Nothing
		Try
			Dim smBL As New SmBL()
			Dim landingPageID As Guid = Common.SafeGUID(Request.Form("landingPageID"))
			Dim comment As String = Common.SafeStripHtmlString(Request.Form("comment"))
			Dim isEnabled As String = Common.SafeStripHtmlString(Request.Form("enable")).ToUpper()
			Dim ipAddress As String = Request.ServerVariables("REMOTE_ADDR")
			Dim landingPage As SmLandingPage = smBL.GetLandingPageByID(landingPageID)
			If landingPage IsNot Nothing Then
				Dim result = smBL.ChangeLandingPageStatus(landingPage, IIf(isEnabled = "Y", SmSettings.LandingPageStatus.Active, SmSettings.LandingPageStatus.InActive), UserInfo.UserID, ipAddress, comment)
				If result = "ok" Then
					res = New CJsonResponse(True, "", "OK")
				Else
					res = New CJsonResponse(False, "", "FAILED")
				End If
			Else
				Response.StatusCode = 404 '' Forbidden
				Response.StatusDescription = "Not found"
				Response.End() '' Terminate all the rest processes
			End If
		Catch ex As Exception
			_log.Error("Could not ChangeLandingPageStatus.", ex)
			res = New CJsonResponse(False, "Could not change status of landing page. Please try again", "FAILED")
		End Try
		Response.Write(JsonConvert.SerializeObject(res))
		Response.End()
	End Sub
	Private Sub DeleteLandingPage()
		Dim res As CJsonResponse = Nothing
		Try
			Dim smBL As New SmBL()
			Dim landingPageID As Guid = Common.SafeGUID(Request.Form("landingPageID"))
			Dim comment As String = Common.SafeStripHtmlString(Request.Form("comment"))
			Dim ipAddress As String = Request.ServerVariables("REMOTE_ADDR")
			Dim landingPage As SmLandingPage = smBL.GetLandingPageByID(landingPageID)
			If landingPage IsNot Nothing Then
				Dim result = smBL.ChangeLandingPageStatus(landingPage, SmSettings.LandingPageStatus.Deleted, UserInfo.UserID, ipAddress, comment)
				If result = "ok" Then
					res = New CJsonResponse(True, "", "OK")
				Else
					res = New CJsonResponse(False, "", "FAILED")
				End If
			Else
				Response.StatusCode = 404 '' Forbidden
				Response.StatusDescription = "Not found"
				Response.End() '' Terminate all the rest processes
			End If
		Catch ex As Exception
			_log.Error("Could not DeleteLandingPage.", ex)
			res = New CJsonResponse(False, "Could not delete landing page. Please try again", "FAILED")
		End Try
		Response.Write(JsonConvert.SerializeObject(res))
		Response.End()
	End Sub
	Private Sub SaveLandingPageContent()
		Dim res As CJsonResponse = Nothing
		Try
			Dim smBL As New SmBL
			Dim landingPageID = Common.SafeGUID(Request.Form("id"))
			Dim landingPage = smBL.GetLandingPageByID(landingPageID)
			If landingPage Is Nothing Then
				Response.StatusCode = 404 '' Forbidden
				Response.StatusDescription = "Not found"
				Response.End() '' Terminate all the rest processes
			Else
				''Dim assets = Common.SafeString(Request.Form("gjs-assets"))
				Dim css = Common.SafeString(Request.Form("gjs-css"))
				''Dim styles = Common.SafeString(Request.Form("gjs-styles"))
				Dim html = Common.SafeString(Request.Form("gjs-html"))
				'' remove scripts, css
				Dim eofIndex = html.IndexOf("<div id=""end_html""></div>", StringComparison.Ordinal)
				If (eofIndex > 0) And html.Length > eofIndex + 25 Then
					html = html.Substring(0, eofIndex + 25)
				End If
				''Dim components = Common.SafeString(Request.Form("gjs-components"))
				Dim pageData = String.Format("<style>{0}</style>{1}", css, html)
				landingPage.PageData = pageData
				Dim result = smBL.UpdateLandingPageContent(landingPage, UserInfo.UserID, Request.ServerVariables("REMOTE_ADDR"))
				If result = "ok" Then
					res = New CJsonResponse(True, "", "OK")
				Else
					res = New CJsonResponse(False, "", "FAILED")
				End If
			End If
		Catch ex As Exception
			_log.Error("Could not SaveLandingPageContent.", ex)
			res = New CJsonResponse(False, "Could not update landing page content. Please try again", "FAILED")
		End Try
		Response.Write(JsonConvert.SerializeObject(res))
		Response.End()
	End Sub

	Private Sub LoadCloneLandingPageForm()
		Try
			Dim control As Sm_Uc_CloneLandingPageForm = CType(LoadControl("~/Sm/Uc/CloneLandingPageForm.ascx"), Sm_Uc_CloneLandingPageForm)
			Dim landingPageID As Guid = Common.SafeGUID(Request.Form("landingPageId"))
			Dim landingPage As SmLandingPage = New SmBL().GetLandingPageByID(landingPageID)
			If landingPage Is Nothing Then
				Response.StatusCode = 404 '' Forbidden
				Response.StatusDescription = "Not found"
				Response.End() '' Terminate all the rest processes
			End If
			control.LandingPage = landingPage
			Me.Controls.Add(control)
		Catch ex As Exception
			_log.Error("Could not load clone landing page form", ex)
			Response.Write("<h3 style='text-align: center; color: red;'>Unable to render data. Please try again later</h3>")
		End Try
	End Sub

	Private Sub CloneLandingPage()
		Dim res As CJsonResponse = Nothing
		Try
			Dim smBL As New SmBL()
			Dim cloneLandingModelObj = JsonConvert.DeserializeObject(Of SmLandingPage)(Common.SafeString(Request.Form("post_data")), New JsonSerializerSettings() With {.NullValueHandling = NullValueHandling.Ignore}).MakeItSafe()
			Dim landingPage As SmLandingPage = smBL.GetLandingPageByID(cloneLandingModelObj.LandingPageID)

			If landingPage Is Nothing Then
				Response.StatusCode = 404 '' Forbidden
				Response.StatusDescription = "Not found"
				Response.End() '' Terminate all the rest processes
			Else
				Dim existedLandingPageWithRefId = smBL.GetLandingPageByRefID(cloneLandingModelObj.RefID)
				If existedLandingPageWithRefId IsNot Nothing Then
					res = New CJsonResponse(False, "Home Page ID has been taken. Please choose another one", "EXISTED")
				Else
					cloneLandingModelObj.LandingPageID = Guid.NewGuid()
					Dim result = smBL.CloneLandingPage(cloneLandingModelObj, landingPage, UserInfo, Request.ServerVariables("REMOTE_ADDR"))
					If result = "ok" Then
						res = New CJsonResponse(True, "OK", "")
					Else
						res = New CJsonResponse(False, "FAILED", "")
					End If
				End If
			End If
		Catch ex As Exception
			_log.Error("Could not CloneLandingPage.", ex)
			res = New CJsonResponse(False, "Could not clone landing page. Please try again", "FAILED")
		End Try
		Response.Write(JsonConvert.SerializeObject(res))
		Response.End()
	End Sub

	Private Sub GetUploadedAssets()
		Dim res = New AssetResponse()

		Try
			Dim folderPath = HttpContext.Current.Server.MapPath("~/Sm/content/images/upload/")

			If Directory.Exists(folderPath) Then
				Dim files = Directory.GetFiles(folderPath)

				For Each file As String In files
					Dim fileName = Path.GetFileName(file)
					' Add url to response
					res.data.Add(String.Format("{0}/Sm/content/images/upload/{1}", _serverRoot, fileName))
				Next
			End If
		Catch ex As Exception
			_log.Error("Could not GetUploadedAssets", ex)
		End Try

		HttpContext.Current.Response.Write(JsonConvert.SerializeObject(res))
		HttpContext.Current.Response.End()
	End Sub

#End Region
#Region "Profile"
	Private Sub SaveProfile()
		Dim res As CJsonResponse
		Try
			Dim smAuthBL As New SmAuthBL()
			Dim profileObj = JsonConvert.DeserializeObject(Of SmProfileModel)(Common.SafeString(Request.Form("profile_data")), New JsonSerializerSettings() With {.NullValueHandling = NullValueHandling.Ignore}).MakeItSafe()
			Dim currentUser As SmUser = smAuthBL.GetUserByID(UserInfo.UserID)
			If currentUser Is Nothing Then
				Response.StatusCode = 404 '' Forbidden
				Response.StatusDescription = "Not found"
				Response.End() '' Terminate all the rest processes
				Return
			End If
			Dim ipAddress As String = Request.ServerVariables("REMOTE_ADDR")
			currentUser.FirstName = profileObj.FirstName
			currentUser.LastName = profileObj.LastName
			currentUser.Phone = profileObj.Phone
			currentUser.Avatar = profileObj.Avatar
			Dim result = smAuthBL.UpdateUser(currentUser, UserInfo.UserID, ipAddress)
			If result = "ok" Then
				res = New CJsonResponse(True, "", New With {.avatar = IIf(String.IsNullOrEmpty(currentUser.Avatar.Trim()), "/images/avatar.jpg", currentUser.Avatar), .firstName = currentUser.FirstName})
				currentUser.Password = String.Empty
				currentUser.Salt = String.Empty
				HttpContext.Current.Session("CURRENT_USER_INFO") = currentUser
			Else
				res = New CJsonResponse(False, "Could not update your profile. Please try again later.", "FAILED")
			End If
		Catch ex As Exception
			_log.Error("Could not SaveProfile.", ex)
			res = New CJsonResponse(False, "Could not update your profile. Please try again", "FAILED")
		End Try
		Response.Write(JsonConvert.SerializeObject(res))
		Response.End()
	End Sub

	Private Sub ChangePassword()
		Dim res As CJsonResponse
		Try
			Dim smAuthBL As New SmAuthBL()
			Dim modelObj = JsonConvert.DeserializeObject(Of SmChangePasswordModel)(Common.SafeString(Request.Form("password_data")), New JsonSerializerSettings() With {.NullValueHandling = NullValueHandling.Ignore}).MakeItSafe()
			Dim currentUser As SmUser = smAuthBL.GetUserByID(UserInfo.UserID)
			If currentUser Is Nothing OrElse modelObj.NewPassword <> modelObj.ConfirmNewPassword Then
				Response.StatusCode = 404 '' Forbidden
				Response.StatusDescription = "Not found"
				Response.End() '' Terminate all the rest processes
				Return
			End If
			Dim hashPassword As String = SmUtil.GetSHA256Hash(modelObj.CurrentPassword, currentUser.Salt)
			If currentUser.LoginFailedCount >= Common.SafeInteger(ConfigurationManager.AppSettings("LoginFailedCountExceed")) Then
				res = New CJsonResponse(False, "Current password is not correct", "INCORRECT")
			ElseIf hashPassword <> currentUser.Password Then
				res = New CJsonResponse(False, "Current password is not correct", "INCORRECT")
				smAuthBL.UpdateLoginFailureCount(currentUser.UserID, currentUser.LoginFailedCount + 1)
			Else
				Dim ipAddress As String = Request.ServerVariables("REMOTE_ADDR")
				Dim newSalt As String = SmUtil.GetSalt()
				Dim checkValidPasswordResult = smAuthBL.CheckValidPassword(currentUser, modelObj.NewPassword)
				If checkValidPasswordResult = "" Then
					hashPassword = SmUtil.GetSHA256Hash(modelObj.NewPassword, newSalt)
					Dim result = smAuthBL.ChangePassword(currentUser.UserID, hashPassword, newSalt, UserInfo.UserID, ipAddress)
					If result = "ok" Then
						res = New CJsonResponse(True, "", "OK")
						smAuthBL.UpdateLoginFailureCount(currentUser.UserID, 0)
					Else
						res = New CJsonResponse(False, "Could not change you password. Please try again later.", "FAILED")
					End If
				Else
					res = New CJsonResponse(False, checkValidPasswordResult, "INVALID")
				End If
			End If
		Catch ex As Exception
			_log.Error("Could not ChangePassword.", ex)
			res = New CJsonResponse(False, "Could not change you password. Please try again", "FAILED")
		End Try
		Response.Write(JsonConvert.SerializeObject(res))
		Response.End()
	End Sub
#End Region
#Region "Broadcast"
	Private Sub SaveBroadcast()
		Dim res As CJsonResponse
		Try
			Dim smBL As New SmBL()
			Dim broadcastContent As String = Common.SafeString(Request.Form("content"))
			If Not SmUtil.CheckRoleAccess(HttpContext.Current, SmSettings.Role.SuperUser.ToString()) Then
				Response.StatusCode = 404 '' Forbidden
				Response.StatusDescription = "Not found"
				Response.End() '' Terminate all the rest processes
			End If
			Dim broadcastItem As New SmBroadcast
			broadcastItem.ID = Guid.NewGuid()
			broadcastItem.CreatedDate = Now
			broadcastItem.CreatedBy = UserInfo.UserID
			broadcastItem.BroadcastContent = broadcastContent
			Dim result = smBL.SaveBroadcast(broadcastItem, Request.ServerVariables("REMOTE_ADDR"))
			If result = "ok" Then
				res = New CJsonResponse(True, "OK", "")
			Else
				res = New CJsonResponse(False, "FAILED", "")
			End If
		Catch ex As Exception
			_log.Error("Could not SaveBroadcast.", ex)
			res = New CJsonResponse(False, "Could not save broadcast. Please try again", "FAILED")
		End Try
		Response.Write(JsonConvert.SerializeObject(res))
		Response.End()
	End Sub
#End Region
#Region "Report"
	Private Sub RunInstaTouchReport()
		Dim res As CJsonResponse
		Try
			Dim smBL As New SmBL()
			Dim startDate As DateTime = Common.SafeDate(Request.Form("start"))
			Dim endDate As DateTime = Common.SafeDate(Request.Form("end"))
			Dim lenderRef As String = Common.SafeString(Request.Form("lenderRef"))
			Dim lenderID As Guid = Common.SafeGUID(Request.Form("lenderID"))

			Dim logItems = smBL.GetEquifaxInstaTouchRecords(startDate, endDate, lenderRef, lenderID)

			
			Dim memoryStream As MemoryStream
			Using excelPackage = New OfficeOpenXml.ExcelPackage
				Dim excelWorksheet = excelPackage.Workbook.Worksheets.Add(String.Format("Mort_{0}_{1}", startDate.ToString("MM.dd.yy"), endDate.ToString("MM.dd.yy")))
				excelWorksheet.Cells("A1").Value = "Report"
				excelWorksheet.Cells("A1").Style.Font.Size = 18
				excelWorksheet.Cells("A1").Style.Font.Bold = True
				excelWorksheet.Cells("A2").Value = ""
				excelWorksheet.Cells("A2").Style.Font.Size = 16

				excelWorksheet.Cells("A5").LoadFromCollection(logItems, True)
				excelWorksheet.Row(5).Style.Font.Bold = True
				excelWorksheet.Column(5).Style.Numberformat.Format = "MM/dd/yyyy HH:mm:ss"
				excelWorksheet.Column(6).Style.Numberformat.Format = "MM/dd/yyyy HH:mm:ss"
				excelWorksheet.Cells.AutoFitColumns()
				memoryStream = New MemoryStream(excelPackage.GetAsByteArray())
			End Using

			Dim tempFileID As String = Guid.NewGuid().ToString()
			Session(tempFileID) = memoryStream

			Dim url = String.Format("/Downloader.aspx?tempFileID={0}&fileName=Mort_{1}_{2}.csv", tempFileID, startDate.ToString("MM.dd.yy"), endDate.ToString("MM.dd.yy"))
			res = New CJsonResponse(True, url)
		Catch ex As Exception
			_log.Error("Could not RunInstaTouchReport.", ex)
			res = New CJsonResponse(False, "Could not RunInstaTouchReport. Please try again", "FAILED")
		End Try
		Response.Write(JsonConvert.SerializeObject(res))
		Response.End()
	End Sub
	

	Private Sub RunInstaTouchAccountingReport()
		Dim res As CJsonResponse
		Try
			Dim smBL As New SmBL()
			Dim mo As DateTime = Common.SafeDate(Request.Form("mo"))
			Dim logItems = smBL.GetEquifaxInstaTouchAccountingReportData(mo)
			Dim memoryStream As MemoryStream

			Using excelPackage = New OfficeOpenXml.ExcelPackage
				Dim excelWorksheet = excelPackage.Workbook.Worksheets.Add(String.Format("InstaTouch-Summary-{0}-{1}", mo.Year, mo.Month))
				'excelWorksheet.Cells("A1").Value = "Report"
				'excelWorksheet.Cells("A1").Style.Font.Size = 18
				'excelWorksheet.Cells("A1").Style.Font.Bold = True
				'excelWorksheet.Cells("A2").Value = ""
				'excelWorksheet.Cells("A2").Style.Font.Size = 16

				excelWorksheet.Cells("A1").LoadFromCollection(logItems, True)
				excelWorksheet.Row(1).Style.Font.Bold = True
				excelWorksheet.Column(5).Style.Numberformat.Format = "#,##0"
				excelWorksheet.Column(6).Style.Numberformat.Format = "MM/dd/yyyy"
				excelWorksheet.Cells.AutoFitColumns()
				memoryStream = New MemoryStream(excelPackage.GetAsByteArray())
			End Using
			Dim tempFileID As String = Guid.NewGuid().ToString()
			Session(tempFileID) = memoryStream

			Dim url = String.Format("/Downloader.aspx?tempFileID={0}&fileName=InstaTouch-Summary-{1}-{2}.csv", tempFileID, mo.Year, mo.Month)
			res = New CJsonResponse(True, url)
		Catch ex As Exception
			_log.Error("Could not RunInstaTouchAccountingReport.", ex)
			res = New CJsonResponse(False, "Could not RunInstaTouchAccountingReport. Please try again", "FAILED")
		End Try
		Response.Write(JsonConvert.SerializeObject(res))
		Response.End()
	End Sub
#End Region
#Region "Support"
	Private Sub SearchMortgageLoans()
		Try
			Dim paging = JsonConvert.DeserializeObject(Of SmPagingInfoModel)(Request.Form("paging_info"))
			Dim filter = JsonConvert.DeserializeObject(Of SmGeneralListFilter)(Request.Form("filter_info"))
			If filter IsNot Nothing Then
				filter = filter.MakeItSafe()
			End If
			Dim control As Sm_Uc_MortgageLoanSearchResult = CType(LoadControl("~/Sm/Uc/MortgageLoanSearchResult.ascx"), Sm_Uc_MortgageLoanSearchResult)
			control.PagingInfo = paging
			control.FilterInfo = filter
			Me.Controls.Add(control)
		Catch ex As Exception
			_log.Error("Could not perform mortgage loan search", ex)
			Response.Write("<h3 style='text-align: center; color: red;'>Unable to render content. Please try again later</h3>")
		End Try
	End Sub
	Private Sub GenerateMLSearchToken()
		Dim res As CJsonResponse
		Try
			Dim smBL As New SmBL()
			Dim data As String = Common.SafeString(Request.Form("data"))
			Dim lenderRef As String = Common.SafeString(Request.Form("lenderRef"))
			Dim result = smBL.GenerateMLSearchToken(data, lenderRef)
			res = New CJsonResponse(True, "", result.Value)
		Catch ex As Exception
			_log.Error("Could not RunInstaTouchReport.", ex)
			res = New CJsonResponse(False, "Could not RunInstaTouchReport. Please try again", "FAILED")
		End Try
		Response.Write(JsonConvert.SerializeObject(res))
		Response.End()
	End Sub
#End Region
#Region "Custom List Rules"
	Private Sub SaveCustomListRules()
		Dim res As CJsonResponse
		Try
			Dim smBL As New SmBL()
			Dim draftRecordList As New List(Of SmLenderConfigDraft)
			Dim lenderConfigID As Guid = Common.SafeGUID(Request.Form("lenderconfigid"))
			Dim tab As String = Common.SafeString(Request.Form("tab")).ToUpper()
			Dim data As String = Common.SafeString(Request.Form("data"))
			'Dim oSerializer As New JavaScriptSerializer()
			Dim lenderConfig As CLenderConfig = smBL.GetLiveConfig(lenderConfigID)
			If lenderConfig Is Nothing Then
				Response.StatusCode = 404 '' Forbidden
				Response.StatusDescription = "Not found"
				Response.End() '' Terminate all the rest processes
			End If
			Dim lenderConfigXml As New XmlDocument()
			lenderConfigXml.LoadXml(lenderConfig.ConfigData)
			Dim loanNodeName As String = ""
			Dim nodeGroup As SmSettings.NodeGroup = SmSettings.NodeGroup.Undefinded
			Select Case tab
				Case "XA"
					loanNodeName = "XA_LOAN"
					nodeGroup = SmSettings.NodeGroup.XACustomListRules
				Case "BL"
					loanNodeName = "BUSINESS_LOAN"
					nodeGroup = SmSettings.NodeGroup.BLCustomListRules
				Case "CC"
					loanNodeName = "CREDIT_CARD_LOAN"
					nodeGroup = SmSettings.NodeGroup.CCCustomListRules
				Case "HE"
					loanNodeName = "HOME_EQUITY_LOAN"
					nodeGroup = SmSettings.NodeGroup.HECustomListRules
				Case "PL"
					loanNodeName = "PERSONAL_LOAN"
					nodeGroup = SmSettings.NodeGroup.PLCustomListRules
				Case "VL"
					loanNodeName = "VEHICLE_LOAN"
					nodeGroup = SmSettings.NodeGroup.VLCustomListRules
				Case "GENERAL"
					loanNodeName = ""
					nodeGroup = SmSettings.NodeGroup.GeneralCustomListRules
			End Select
			If nodeGroup = SmSettings.NodeGroup.GeneralCustomListRules OrElse (Not String.IsNullOrWhiteSpace(loanNodeName) AndAlso lenderConfigXml.SelectSingleNode("WEBSITE/" & loanNodeName) IsNot Nothing) Then

				'TODO
				draftRecordList.Add(New SmLenderConfigDraft() With {
					.LenderConfigID = lenderConfigID,
					.CreatedByUserID = UserInfo.UserID,
					.NodePath = CUSTOM_LIST_RULES(loanNodeName),
					.NodeGroup = nodeGroup,
					.NodeContent = SmUtil.BuildCDataXml("CUSTOM_LIST_RULES", data)
				})
				Dim revisionID As Integer = Common.SafeInteger(Request.Form("revisionID"))
				Dim result = smBL.SaveNodesToDraft(revisionID, draftRecordList.ToArray())
				If result Then
					res = New CJsonResponse(True, "", "OK")
				Else
					res = New CJsonResponse(False, "", "FAILED")
				End If
			Else
				res = New CJsonResponse(False, "", "FAILED")
			End If
		Catch ex As Exception
			_log.Error("Could not SaveCustomListRules.", ex)
			res = New CJsonResponse(False, "Unable to save draft. Please try again", "FAILED")
		End Try
		Response.Write(JsonConvert.SerializeObject(res))
		Response.End()
	End Sub

	Private Sub PreviewCustomListRules()
		Dim res As CJsonResponse
		Try
			Dim smBL As New SmBL()
			Dim draftRecordList As New List(Of SmLenderConfigDraft)
			Dim lenderConfigID As Guid = Common.SafeGUID(Request.Form("lenderconfigid"))
			Dim tab As String = Common.SafeString(Request.Form("tab")).ToUpper()
			Dim data As String = Common.SafeString(Request.Form("data"))
			'Dim oSerializer As New JavaScriptSerializer()
			Dim lenderConfig As CLenderConfig = smBL.GetLiveConfig(lenderConfigID)
			If lenderConfig Is Nothing Then
				Response.StatusCode = 404 '' Forbidden
				Response.StatusDescription = "Not found"
				Response.End() '' Terminate all the rest processes
			End If
			Dim lenderConfigXml As New XmlDocument()
			lenderConfigXml.LoadXml(lenderConfig.ConfigData)
			Dim loanNodeName As String = ""
			Dim previewPage As String = ""
			Dim nodeGroup As SmSettings.NodeGroup = SmSettings.NodeGroup.Undefinded
			Select Case tab
				Case "XA"
					loanNodeName = "XA_LOAN"
					nodeGroup = SmSettings.NodeGroup.XACustomListRules
					previewPage = "xa/xpressapp.aspx"
				Case "BL"
					loanNodeName = "BUSINESS_LOAN"
					nodeGroup = SmSettings.NodeGroup.BLCustomListRules
					previewPage = "apply.aspx"
				Case "CC"
					loanNodeName = "CREDIT_CARD_LOAN"
					nodeGroup = SmSettings.NodeGroup.CCCustomListRules
					previewPage = "cc/creditcard.aspx"
				Case "HE"
					loanNodeName = "HOME_EQUITY_LOAN"
					nodeGroup = SmSettings.NodeGroup.HECustomListRules
					previewPage = "he/homeequityloan.aspx"
				Case "PL"
					loanNodeName = "PERSONAL_LOAN"
					nodeGroup = SmSettings.NodeGroup.PLCustomListRules
					previewPage = "pl/personalloan.aspx"
				Case "VL"
					loanNodeName = "VEHICLE_LOAN"
					nodeGroup = SmSettings.NodeGroup.VLCustomListRules
					previewPage = "vl/vehicleloan.aspx"
				Case "GENERAL"
					loanNodeName = ""
					nodeGroup = SmSettings.NodeGroup.GeneralCustomListRules
					previewPage = "pl/personalloan.aspx"
			End Select
			If nodeGroup = SmSettings.NodeGroup.GeneralCustomListRules OrElse (Not String.IsNullOrWhiteSpace(loanNodeName) AndAlso lenderConfigXml.SelectSingleNode("WEBSITE/" & loanNodeName) IsNot Nothing) Then

				'TODO
				draftRecordList.Add(New SmLenderConfigDraft() With {
					.LenderConfigID = lenderConfigID,
					.CreatedByUserID = UserInfo.UserID,
					.NodePath = CUSTOM_LIST_RULES(loanNodeName),
					.NodeGroup = nodeGroup,
					.NodeContent = SmUtil.BuildCDataXml("CUSTOM_LIST_RULES", data)
				})
				Dim revisionID As Integer = Common.SafeInteger(Request.Form("revisionID"))
				Dim result = smBL.SaveNodesToDraft(revisionID, draftRecordList.ToArray())
				If result Then
					Dim previewUrl As String = smBL.GeneratePreviewConfig(lenderConfigID, UserInfo.Email, Function(config, code) String.Format("{0}/{1}?lenderref={2}&autofill=true&previewCode={3}", _serverRoot, previewPage, config.LenderRef, code), draftRecordList.ToArray())
					res = New CJsonResponse(True, "", "OK")
					res.Info = New With {.previewurl = previewUrl}
				Else
					res = New CJsonResponse(False, "", "FAILED")
				End If
			Else
				res = New CJsonResponse(False, "", "FAILED")
			End If
		Catch ex As Exception
			_log.Error("Could not PreviewCustomListRules.", ex)
			res = New CJsonResponse(False, "Unable to save and preview draft. Please try again", "FAILED")
		End Try
		Response.Write(JsonConvert.SerializeObject(res))
		Response.End()
	End Sub

	Private Sub PublishCustomListRules()
		Dim res As CJsonResponse
		Try
			Dim smBL As New SmBL()
			Dim draftRecordList As New List(Of SmLenderConfigDraft)
			Dim lenderConfigID As Guid = Common.SafeGUID(Request.Form("lenderconfigid"))
			Dim tab As String = Common.SafeString(Request.Form("tab")).ToUpper()
			Dim data As String = Common.SafeString(Request.Form("data"))
			'Dim oSerializer As New JavaScriptSerializer()
			Dim lenderConfig As CLenderConfig = smBL.GetLiveConfig(lenderConfigID)
			If lenderConfig Is Nothing Then
				Response.StatusCode = 404 '' Forbidden
				Response.StatusDescription = "Not found"
				Response.End() '' Terminate all the rest processes
			End If
			Dim lenderConfigXml As New XmlDocument()
			lenderConfigXml.LoadXml(lenderConfig.ConfigData)
			Dim loanNodeName As String = ""
			Dim nodeGroup As SmSettings.NodeGroup = SmSettings.NodeGroup.Undefinded
			Select Case tab
				Case "XA"
					loanNodeName = "XA_LOAN"
					nodeGroup = SmSettings.NodeGroup.XACustomListRules
				Case "BL"
					loanNodeName = "BUSINESS_LOAN"
					nodeGroup = SmSettings.NodeGroup.BLCustomListRules
				Case "CC"
					loanNodeName = "CREDIT_CARD_LOAN"
					nodeGroup = SmSettings.NodeGroup.CCCustomListRules
				Case "HE"
					loanNodeName = "HOME_EQUITY_LOAN"
					nodeGroup = SmSettings.NodeGroup.HECustomListRules
				Case "PL"
					loanNodeName = "PERSONAL_LOAN"
					nodeGroup = SmSettings.NodeGroup.PLCustomListRules
				Case "VL"
					loanNodeName = "VEHICLE_LOAN"
					nodeGroup = SmSettings.NodeGroup.VLCustomListRules
				Case "GENERAL"
					loanNodeName = ""
					nodeGroup = SmSettings.NodeGroup.GeneralCustomListRules
			End Select
			If nodeGroup = SmSettings.NodeGroup.GeneralCustomListRules OrElse (Not String.IsNullOrWhiteSpace(loanNodeName) AndAlso lenderConfigXml.SelectSingleNode("WEBSITE/" & loanNodeName) IsNot Nothing) Then

				'TODO
				draftRecordList.Add(New SmLenderConfigDraft() With {
					.LenderConfigID = lenderConfigID,
					.CreatedByUserID = UserInfo.UserID,
					.NodePath = CUSTOM_LIST_RULES(loanNodeName),
					.NodeGroup = nodeGroup,
					.NodeContent = SmUtil.BuildCDataXml("CUSTOM_LIST_RULES", data)
				})
				Dim comment As String = Common.SafeStripHtmlString(Request.Form("comment"))
				Dim publishAll As Boolean = Common.SafeBoolean(Request.Form("publishAll"))
				Dim result = smBL.PublishNodes(lenderConfigID, UserInfo.UserID, Request.UserHostAddress, comment, publishAll, draftRecordList.ToArray())
				If result Then
					res = New CJsonResponse(True, "", "OK")
				Else
					res = New CJsonResponse(False, "", "FAILED")
				End If
			Else
				res = New CJsonResponse(False, "", "FAILED")
			End If
		Catch ex As Exception
			_log.Error("Could not PublishCustomListRules.", ex)
			res = New CJsonResponse(False, "Unable to publish data. Please try again", "FAILED")
		End Try
		Response.Write(JsonConvert.SerializeObject(res))
		Response.End()
	End Sub
#End Region
#Region "Custom Application Scenarios"
	Private Sub SaveCustomApplicationScenarios()
		Dim res As CJsonResponse
		Try
			Dim smBL As New SmBL()
			Dim draftRecordList As New List(Of SmLenderConfigDraft)
			Dim lenderConfigID As Guid = Common.SafeGUID(Request.Form("lenderconfigid"))
			Dim tab As String = Common.SafeString(Request.Form("tab")).ToUpper()
			Dim data As String = Common.SafeString(Request.Form("data"))
			'Dim oSerializer As New JavaScriptSerializer()
			Dim lenderConfig As CLenderConfig = smBL.GetLiveConfig(lenderConfigID)
			If lenderConfig Is Nothing Then
				Response.StatusCode = 404 '' Forbidden
				Response.StatusDescription = "Not found"
				Response.End() '' Terminate all the rest processes
			End If
			Dim lenderConfigXml As New XmlDocument()
			lenderConfigXml.LoadXml(lenderConfig.ConfigData)
			Dim loanNodeName As String = ""
			Dim nodeGroup As SmSettings.NodeGroup = SmSettings.NodeGroup.Undefinded
			Select Case tab
				Case "XA"
					loanNodeName = "XA_LOAN"
					nodeGroup = SmSettings.NodeGroup.XACustomApplicationScenarios
			End Select
			If Not String.IsNullOrWhiteSpace(loanNodeName) AndAlso lenderConfigXml.SelectSingleNode("WEBSITE/" & loanNodeName) IsNot Nothing Then

				'TODO
				draftRecordList.Add(New SmLenderConfigDraft() With {
					.LenderConfigID = lenderConfigID,
					.CreatedByUserID = UserInfo.UserID,
					.NodePath = CUSTOM_APPLICATION_SCENARIOS(loanNodeName),
					.NodeGroup = nodeGroup,
					.NodeContent = SmUtil.BuildCDataXml("CUSTOM_APPLICATION_SCENARIOS", data)
				})
				Dim revisionID As Integer = Common.SafeInteger(Request.Form("revisionID"))
				Dim result = smBL.SaveNodesToDraft(revisionID, draftRecordList.ToArray())
				If result Then
					res = New CJsonResponse(True, "", "OK")
				Else
					res = New CJsonResponse(False, "", "FAILED")
				End If
			Else
				res = New CJsonResponse(False, "", "FAILED")
			End If
		Catch ex As Exception
			_log.Error("Could not SaveCustomApplicationScenarios.", ex)
			res = New CJsonResponse(False, "Unable to save draft. Please try again", "FAILED")
		End Try
		Response.Write(JsonConvert.SerializeObject(res))
		Response.End()
	End Sub

	Private Sub PreviewCustomApplicationScenarios()
		Dim res As CJsonResponse
		Try
			Dim smBL As New SmBL()
			Dim draftRecordList As New List(Of SmLenderConfigDraft)
			Dim lenderConfigID As Guid = Common.SafeGUID(Request.Form("lenderconfigid"))
			Dim tab As String = Common.SafeString(Request.Form("tab")).ToUpper()
			Dim data As String = Common.SafeString(Request.Form("data"))
			'Dim oSerializer As New JavaScriptSerializer()
			Dim lenderConfig As CLenderConfig = smBL.GetLiveConfig(lenderConfigID)
			If lenderConfig Is Nothing Then
				Response.StatusCode = 404 '' Forbidden
				Response.StatusDescription = "Not found"
				Response.End() '' Terminate all the rest processes
			End If
			Dim lenderConfigXml As New XmlDocument()
			lenderConfigXml.LoadXml(lenderConfig.ConfigData)
			Dim loanNodeName As String = ""
			Dim nodeGroup As SmSettings.NodeGroup = SmSettings.NodeGroup.Undefinded
			Select Case tab
				Case "XA"
					loanNodeName = "XA_LOAN"
					nodeGroup = SmSettings.NodeGroup.XACustomApplicationScenarios
			End Select
			If Not String.IsNullOrWhiteSpace(loanNodeName) AndAlso lenderConfigXml.SelectSingleNode("WEBSITE/" & loanNodeName) IsNot Nothing Then

				'TODO
				draftRecordList.Add(New SmLenderConfigDraft() With {
					.LenderConfigID = lenderConfigID,
					.CreatedByUserID = UserInfo.UserID,
					.NodePath = CUSTOM_APPLICATION_SCENARIOS(loanNodeName),
					.NodeGroup = nodeGroup,
					.NodeContent = SmUtil.BuildCDataXml("CUSTOM_APPLICATION_SCENARIOS", data)
				})
				Dim revisionID As Integer = Common.SafeInteger(Request.Form("revisionID"))
				Dim result = smBL.SaveNodesToDraft(revisionID, draftRecordList.ToArray())
				If result Then
					Dim previewCode As String = smBL.GeneratePreviewConfig(lenderConfigID, UserInfo.Email, Function(config, code) code, draftRecordList.ToArray())
					res = New CJsonResponse(True, "", "OK")
					res.Info = New With {.previewcode = previewCode}
				Else
					res = New CJsonResponse(False, "", "FAILED")
				End If
			Else
				res = New CJsonResponse(False, "", "FAILED")
			End If
		Catch ex As Exception
			_log.Error("Could not PreviewCustomApplicationScenarios.", ex)
			res = New CJsonResponse(False, "Unable to save and preview draft. Please try again", "FAILED")
		End Try
		Response.Write(JsonConvert.SerializeObject(res))
		Response.End()
	End Sub

	Private Sub PublishCustomApplicationScenarios()
		Dim res As CJsonResponse
		Try
			Dim smBL As New SmBL()
			Dim draftRecordList As New List(Of SmLenderConfigDraft)
			Dim lenderConfigID As Guid = Common.SafeGUID(Request.Form("lenderconfigid"))
			Dim tab As String = Common.SafeString(Request.Form("tab")).ToUpper()
			Dim data As String = Common.SafeString(Request.Form("data"))
			'Dim oSerializer As New JavaScriptSerializer()
			Dim lenderConfig As CLenderConfig = smBL.GetLiveConfig(lenderConfigID)
			If lenderConfig Is Nothing Then
				Response.StatusCode = 404 '' Forbidden
				Response.StatusDescription = "Not found"
				Response.End() '' Terminate all the rest processes
			End If
			Dim lenderConfigXml As New XmlDocument()
			lenderConfigXml.LoadXml(lenderConfig.ConfigData)
			Dim loanNodeName As String = ""
			Dim nodeGroup As SmSettings.NodeGroup = SmSettings.NodeGroup.Undefinded
			Select Case tab
				Case "XA"
					loanNodeName = "XA_LOAN"
					nodeGroup = SmSettings.NodeGroup.XACustomApplicationScenarios
			End Select
			If Not String.IsNullOrWhiteSpace(loanNodeName) AndAlso lenderConfigXml.SelectSingleNode("WEBSITE/" & loanNodeName) IsNot Nothing Then

				'TODO
				draftRecordList.Add(New SmLenderConfigDraft() With {
					.LenderConfigID = lenderConfigID,
					.CreatedByUserID = UserInfo.UserID,
					.NodePath = CUSTOM_APPLICATION_SCENARIOS(loanNodeName),
					.NodeGroup = nodeGroup,
					.NodeContent = SmUtil.BuildCDataXml("CUSTOM_APPLICATION_SCENARIOS", data)
				})
				Dim comment As String = Common.SafeStripHtmlString(Request.Form("comment"))
				Dim publishAll As Boolean = Common.SafeBoolean(Request.Form("publishAll"))
				Dim result = smBL.PublishNodes(lenderConfigID, UserInfo.UserID, Request.UserHostAddress, comment, publishAll, draftRecordList.ToArray())
				If result Then
					res = New CJsonResponse(True, "", "OK")
				Else
					res = New CJsonResponse(False, "", "FAILED")
				End If
			Else
				res = New CJsonResponse(False, "", "FAILED")
			End If
		Catch ex As Exception
			_log.Error("Could not PublishCustomApplicationScenarios.", ex)
			res = New CJsonResponse(False, "Unable to publish data. Please try again", "FAILED")
		End Try
		Response.Write(JsonConvert.SerializeObject(res))
		Response.End()
	End Sub
#End Region
End Class
