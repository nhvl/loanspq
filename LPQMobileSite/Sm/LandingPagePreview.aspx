﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="LandingPagePreview.aspx.vb" Inherits="Sm_LandingPagePreview" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>APM - Landing Page Preview</title>
    
    <link href="https://fonts.googleapis.com/css?family=Quattrocento+Sans:400,700" rel="stylesheet" type="text/css" />
    <link href="https://fonts.googleapis.com/css?family=Raleway:100,200,300,700,800,900" rel="stylesheet" type="text/css" />       
    <link href="/Sm/content/landingpages/bootstrap.css" rel="stylesheet" />
    <link href="/Sm/content/landingpages/font-awesome.min.css" rel="stylesheet" />
    <link href="/Sm/content/landingpages/main.css" rel="stylesheet" />
    <link href="/Sm/content/landingpages/carousel.css" rel="stylesheet" />
    <link href="/Sm/js/slick/slick.css" rel="stylesheet" type="text/css" />
    <link href="/Sm/js/slick/slick-theme.css" rel="stylesheet" type="text/css" />
    <link href="/Sm/content/landingpages/custom.css" rel="stylesheet" />
</head>
<body>
    <%=TemplateHtml%>
    
    <script src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
    <script src="https://code.jquery.com/ui/1.10.4/jquery-ui.js"></script>
    <script src="/Sm/js/bootstrap/bootstrap.js"></script>
    <script src="/Sm/js/carousel/owl.carousel.min.js"></script>
    <script type="text/javascript" src="/Sm/js/slick/slick.min.js"></script>
    <script src="/Sm/js/landingpages/main.js"></script>
    <script src="/Sm/js/landingpages/home.js"></script>
</body>
</html>
