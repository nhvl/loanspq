﻿
Imports System.IO
Imports LPQMobile.Utils
Imports System.Xml

Partial Class Sm_LandingPageEditor
	Inherits System.Web.UI.Page

	Protected LandingPage As SmLandingPage
	'Protected TemplateCss As String
	Protected TemplateHtml As String
	Public ReadOnly Property ServerRoot() As String
		Get
			Dim port As String = ""
			If (Request.Url.Scheme = "http" AndAlso Request.Url.Port <> 80) OrElse (Request.Url.Scheme = "https" AndAlso Request.Url.Port <> 443) Then
				port = ":" + Request.Url.Port.ToString()
			End If
			Return Request.Url.Scheme + "://" + Request.Url.Host + port
		End Get
	End Property

	Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
		If Not Page.IsPostBack Then
			If Not Page.IsPostBack Then
				Dim smBL As New SmBL
				Dim landingPageID As Guid = Common.SafeGUID(Request.Params("id"))
				LandingPage = smBL.GetLandingPageByID(landingPageID)
				If LandingPage IsNot Nothing Then
					Dim portal = smBL.GetPortalByLenderConfigID(LandingPage.LenderConfigID, pbDecrypt:=False)
					If portal IsNot Nothing Then
						Dim config As New XmlDocument
						config.LoadXml(portal.ConfigData)
						Dim homepageEditorNode As XmlNode = config.SelectSingleNode("WEBSITE/@homepage_editor")
						If homepageEditorNode IsNot Nothing AndAlso homepageEditorNode.Value.ToUpper().Equals("Y") Then
							Dim allowRoles As New List(Of String)
							allowRoles.Add(SmSettings.Role.SuperUser.ToString())
							allowRoles.Add(String.Format("LenderScope${0}${1}", SmSettings.Role.LenderAdmin.ToString(), portal.LenderID.ToString()))
							allowRoles.Add(String.Format("PortalScope${0}${1}${2}", SmSettings.Role.PortalAdmin, portal.LenderID.ToString(), portal.LenderConfigID.ToString()))
							If Not SmUtil.CheckRoleAccess(HttpContext.Current, allowRoles) Then
								Response.Redirect("~/NoAccess.aspx", True)
							End If
							'If (Not String.IsNullOrEmpty(landingPage.PageData)) Then
							'    Dim pattern = "<style>(.*?)</style>"
							'    Dim rgx As New Regex(pattern)
							'    ''Dim result As String = rgx.Replace(pageData, "")
							'    TemplateCss = rgx.Match(pageData).Groups(0).Value
							'    TemplateHtml = rgx.Replace(pageData, "")
							'End If
							TemplateHtml = LandingPage.PageData
						Else
							Response.Redirect("~/PageNotFound.aspx", True)
						End If
					Else
						Response.Redirect("~/PageNotFound.aspx", True)
					End If
				Else
					Response.Redirect("~/PageNotFound.aspx", True)
				End If
			End If

		End If
	End Sub
End Class
