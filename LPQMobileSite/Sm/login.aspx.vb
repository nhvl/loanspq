﻿
Partial Class Sm_login1
	Inherits System.Web.UI.Page

	Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
		If Not IsPostBack Then
			If Request.IsAuthenticated Then
				Response.Redirect("/agent/Default.aspx", True)
			Else
				Response.Redirect("/Login.aspx", True)
			End If
		End If
	End Sub
End Class
