﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="Revision.aspx.vb" Inherits="Sm_Revision" MasterPageFile="SiteManager.master" %>
<asp:Content runat="server" ContentPlaceHolderID="plBody">
	<div>
		<h1 class="page-header">Revisions</h1>
		<p>Help you undo changes from a revision</p>
		
		
		

		<div class="grid" id="divRevisionList">Loading</div>
	</div>
</asp:Content>
<asp:Content runat="server" ContentPlaceHolderID="plScripts">
	<script type="text/javascript">
		$(function () {
			var revisionGrid;
			_COMMON_PAGE_SIZE = 20;
			revisionGrid = new _COMMON.Grid("/sm/smhandler.aspx", "revisionGrid", "divRevisionList", manageRevisions.FACTORY, manageRevisions.FACTORY.onLoaded, { command: "loadRevisionGrid", lenderConfigId: '<%=LenderConfigID%>' });
			revisionGrid.init();
			manageRevisions.FACTORY.init(revisionGrid.GridObject);
		});
		(function (manageRevisions, $, undefined) {
			manageRevisions.DATA = {};
			manageRevisions.FACTORY = {};
			manageRevisions.FACTORY.init = function (gridInstance) {
				var self = this;
				manageRevisions.DATA.grid = gridInstance;
			};
			manageRevisions.FACTORY.getFilterInfo = function () {
				//implement this later
				return {};
			};
			manageRevisions.FACTORY.onLoaded = function () {
				$("a[data-command='showchanges']", "#divRevisionList").each(function (idx, ele) {
					var $self = $(ele);
					$self.on("click", function () {
						//title, dataId, containerId, htmlContent, getContentUrl, postData, onInitCallback, onClosingCallback
						var dataId = $self.data("id");
						_COMMON.showDialogClose("Changes", dataId, "showchanges_dialog", "", "", null, function (container) {
							//init grid
							$("#showchanges_dialog").closest("div.modal-dialog").css({ "width": "80%" });
							var showchangesGrid = new _COMMON.Grid("/sm/smhandler.aspx", "showchangesGrid", "showchanges_dialog", null, null, { command: "showChangesInRevision", lenderConfigLogId: dataId });
							showchangesGrid.init();
						}, null);
					});
				});
				$("a[data-command='preview']", "#divRevisionList").each(function (idx, ele) {
					var $self = $(ele);
					$self.on("click", function () {
						//title, dataId, containerId, htmlContent, getContentUrl, postData, onInitCallback, onClosingCallback
						var dataId = $self.data("id");
						_COMMON.showDialogClose("Preview", dataId, "preview_dialog", '<div class="loan-list"><div data-type="xa">Become a member</div><div data-type="sa">Open an account</div><div data-type="cc">Credit Card Loan</div><div data-type="cc_combo">Credit Card Loan Combo</div><div data-type="he">Home Equity Loan</div><div data-type="he_combo">Home Equity Loan Combo</div><div data-type="pl">Personal Loan</div><div data-type="pl_combo">Personal Loan Combo</div><div data-type="vl">Vehicle Loan</div><div data-type="vl_combo">Vehicle Loan Combo</div></div>', "", null, function (container) {
							var $container = $(container);
							$("div.loan-list>div", $container).each(function(i,e) {
								$(e).on("click", function() {
									var dataObj = {};
									dataObj.command = "previewRevision";
									dataObj.lenderConfigLogId = dataId;
									dataObj.type = $(e).data("type");
									$.ajax({
										url: "/sm/smhandler.aspx",
										async: true,
										cache: false,
										type: 'POST',
										dataType: 'html',
										data: dataObj,
										success: function (responseText) {
											var response = $.parseJSON(responseText);
											if (response.IsSuccess) {
												_COMMON.openInNewTab(response.Info.previewurl);
											} else {
												_COMMON.noty("error", "Error", 500);
											}
										}
									});
								});
							});
						}, null);
					});
				});
				$("a[data-command='revert']", "#divRevisionList").each(function (idx, ele) {
					var $self = $(ele);
					$self.on("click", function () {
						//title, dataId, containerId, htmlContent, getContentUrl, postData, onInitCallback, onClosingCallback
						var dataId = $self.data("id");
						_COMMON.showConfirmDialog("This change will impact the entire site.  Are you sure about reverting the system settings to this revision?.", function () {
							//yes
							var htmlContent = '<div class="row"><div class="col-12"><div class="form-group"><label class="required-field">Revert Note</label><textarea class="form-control" id="txtRevertComment" placeholder="Leave a note here" data-message-require="Please fill out this field" data-required="true" rows="5"></textarea></div></div></div>';
							htmlContent = '<div class="dialog-content">' + htmlContent + '</div>';
							_COMMON.showDialogSaveClose("Revert to revision", 0, "comment_dialog_0", htmlContent, "", null, function (container) {
								//on save
								if ($.smValidate("comment_dialog_0") == false) return;
								var dataObj = {};
								dataObj.command = "revertToRevision";
								dataObj.comment = $("#txtRevertComment").val();
								dataObj.lenderConfigLogId = dataId;
								$.ajax({
									url: "/sm/smhandler.aspx",
									async: true,
									cache: false,
									type: 'POST',
									dataType: 'html',
									data: dataObj,
									success: function (responseText) {
										var response = $.parseJSON(responseText);
										if (response.IsSuccess) {
											_COMMON.noty("success", "Revert successfully.", 500);
											//var pageIndex = manageRevisions.DATA.grid.getPaginationSetting().page;
											manageRevisions.DATA.grid.setFilter(manageRevisions.FACTORY.getFilterInfo()).setPageIndex(1).load();
										} else {
											_COMMON.noty("error", "Error", 500);
										}
										container.dialog.close();
									}
								});
							}, function (container) {
								//onready
								var $container = $(container);
								$("#txtRevertComment").focus();
								$("#txtRevertComment").observer({
									validators: [
										function (partial) {
											var $ele = $(this);
											if ($ele.data("required") && $.trim($ele.val()) === "") {
												if ($ele.data("message-require")) {
													return $ele.data("message-require");
												} else {
													return "This field is required";
												}
											}
											return "";
										}
									],
									validateOnBlur: true,
									group: "comment_dialog_0"
								});
							}, function () {
								//onclose
								$.smValidate.cleanGroup("comment_dialog_0");
							});
						}, function () { //no - do nothing
						}, "Yes, do it", "No, I'm not sure");
					});
				});

			};
			manageRevisions.FACTORY.search = function () {
				manageRevisions.DATA.grid.setFilter(manageRevisions.FACTORY.getFilterInfo()).setPageIndex(1).load();
			};
		}(window.manageRevisions = window.manageRevisions || {}, jQuery));
	</script>
</asp:Content>
<asp:Content runat="server" ContentPlaceHolderID="plActionButtons"></asp:Content>