﻿
Partial Class Sm_ManageLandingPages
	Inherits SmApmBasePage
	Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
		If Not Page.IsPostBack Then
			Dim smMasterPage = CType(Me.Master, Sm_SiteManager)
			smMasterPage.LenderConfigXml = LenderConfigXml
			smMasterPage.SelectedMainMenuItem = "design"
			smMasterPage.SelectedSubMenuItem = "landingpagelist"
			smMasterPage.ShowPreviewButton = False
			smMasterPage.ShowSaveDraftButton = False
			smMasterPage.ShowPublishButton = False
			If WebsiteConfig.HomepageEditor = False Then
				Response.Redirect("/PageNotFound.aspx", True)
			End If
		End If
	End Sub
End Class
