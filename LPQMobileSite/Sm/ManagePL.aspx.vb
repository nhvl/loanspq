﻿
Imports System.Xml
Imports DownloadedSettings
Imports Sm

Partial Class Sm_ManagePL
	Inherits SmApmBasePage
	Const LOANTYPE As String = "PERSONAL_LOAN"
	Protected Property ApprovalMessage As String
	Protected Property ReferredMessage As String
    Protected Property DeclinedMessage As String
    Protected Property PreQualifiedMessage As String
    Protected Property DisclosureList As New List(Of String)

	Protected Property EnabledPurposeList As New List(Of String)(System.StringComparison.OrdinalIgnoreCase)
	Protected Property PurposeList As New Dictionary(Of String, SmTextValueCatItem)(System.StringComparison.OrdinalIgnoreCase)
	Protected Property EnabledLOCTypeList As New List(Of String)(System.StringComparison.OrdinalIgnoreCase)
	Protected Property LOCTypeList As New Dictionary(Of String, SmLineOfCreditItem)(System.StringComparison.OrdinalIgnoreCase)

	Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
		If Not Page.IsPostBack Then
			Dim smMasterPage = CType(Me.Master, Sm_SiteManager)
			smMasterPage.LenderConfigXml = LenderConfigXml
			smMasterPage.RevisionID = LenderConfig.RevisionID
			smMasterPage.LastModifiedBy = LenderConfig.LastModifiedByUserFullName
			smMasterPage.LastModifiedByUserID = LenderConfig.LastModifiedByUserID
			smMasterPage.SelectedMainMenuItem = "manage"
			smMasterPage.SelectedSubMenuItem = "pl"
			smMasterPage.CurrentNodeGroup = SmSettings.NodeGroup.ManagePL
			smMasterPage.BreadCrumb = "Manage / Personal Loan"

			Dim preapprovedMessageNode As XmlNode = GetNode(PREAPPROVED_MESSAGE(LOANTYPE))
			If preapprovedMessageNode IsNot Nothing AndAlso preapprovedMessageNode.NodeType <> XmlNodeType.Comment Then
				ApprovalMessage = SmUtil.MergeMessageKey(preapprovedMessageNode.Attributes("message").InnerText, preapprovedMessageNode)
			End If

			Dim submittedMessageNode As XmlNode = GetNode(SUBMITTED_MESSAGE(LOANTYPE))
			If submittedMessageNode IsNot Nothing AndAlso submittedMessageNode.NodeType <> XmlNodeType.Comment Then
				ReferredMessage = SmUtil.MergeMessageKey(submittedMessageNode.Attributes("message").InnerText, submittedMessageNode)
			End If

			Dim declinedMessageNode As XmlNode = GetNode(DECLINED_MESSAGE(LOANTYPE))
			If declinedMessageNode IsNot Nothing AndAlso declinedMessageNode.NodeType <> XmlNodeType.Comment Then
				DeclinedMessage = SmUtil.MergeMessageKey(declinedMessageNode.Attributes("message").InnerText, declinedMessageNode)
			End If
			Dim preQualifiedMessageNode As XmlNode = GetNode(PREQUALIFIED_MESSAGE(LOANTYPE))
			If preQualifiedMessageNode IsNot Nothing AndAlso preQualifiedMessageNode.NodeType <> XmlNodeType.Comment Then
				PreQualifiedMessage = SmUtil.MergeMessageKey(preQualifiedMessageNode.Attributes("message").InnerText, preQualifiedMessageNode)
			End If
			Dim disclosuresNode As XmlNode = GetNode(DISCLOSURES(LOANTYPE))
			If disclosuresNode IsNot Nothing AndAlso disclosuresNode.HasChildNodes Then
				For Each node As XmlNode In disclosuresNode.ChildNodes
					If node.NodeType = XmlNodeType.Comment Then Continue For
					DisclosureList.Add(node.InnerText.Trim())
				Next
			End If

			'********************
			'Purpose
			'use download config first bc it is the most updated version
			Dim downloadPersonalLoanPurposes As List(Of DownloadedSettings.CListItem)
			downloadPersonalLoanPurposes = CDownloadedSettings.PersonalLoanPurposes(WebsiteConfig)

			If downloadPersonalLoanPurposes IsNot Nothing AndAlso downloadPersonalLoanPurposes.Any() Then
				For Each item As CListItem In downloadPersonalLoanPurposes
					If item.Value Is Nothing OrElse item.Value = "" Then Continue For
					If PurposeList.ContainsKey(item.Value) Then Continue For
					PurposeList.Add(item.Value, New SmTextValueCatItem() With {.text = item.Text, .value = item.Value, .category = ""})
				Next
			End If

			'update setup with xml config
			Dim purposesNode As XmlNode = GetNode(PERSONAL_LOAN_PURPOSES())
			If purposesNode IsNot Nothing AndAlso purposesNode.HasChildNodes Then
				For Each node As XmlNode In purposesNode.ChildNodes
					If node.NodeType = XmlNodeType.Comment Then Continue For
					Dim value As String = node.Attributes("value").Value
					If Not EnabledPurposeList.Contains(value) Then
						EnabledPurposeList.Add(value)
					End If

					'need to update download purposes with category from xml config
					If PurposeList.ContainsKey(value) AndAlso node.Attributes("category") IsNot Nothing Then
						Dim category As String = node.Attributes("category").Value
						PurposeList(value) = New SmTextValueCatItem() With {.text = PurposeList(value).text, .value = PurposeList(value).value, .category = category}
					End If

				Next
			End If

			'***************
			' LOC Purpose
			Dim downloadLocPurposes As List(Of CListItem)
			downloadLocPurposes = CDownloadedSettings.PersonalLoanPurposes(WebsiteConfig)
			If downloadLocPurposes IsNot Nothing AndAlso downloadLocPurposes.Any() Then
				For Each item As CListItem In downloadLocPurposes
					If LOCTypeList.ContainsKey(item.Value) Then Continue For
					LOCTypeList.Add(item.Value, New SmLineOfCreditItem() With {.Text = item.Text, .Value = item.Value, .Category = CEnum.LocCategory.UNSECURE, .OverDraftOption = CEnum.LocOverDraftOption.BOTH_OVERDRAFT_AND_NON_OVERDRAFT})
				Next
			End If

			Dim locTypesNode As XmlNode = GetNode(PERSONAL_LOC_TYPES())
			If locTypesNode IsNot Nothing AndAlso locTypesNode.HasChildNodes Then
				For Each node As XmlNode In locTypesNode.ChildNodes
					If node.NodeType = XmlNodeType.Comment Then Continue For
					Dim value As String = node.Attributes("value").Value
					If Not EnabledLOCTypeList.Contains(value) Then
						EnabledLOCTypeList.Add(value)
					End If

					'need to update download purposes with category from xml config
					If LOCTypeList.ContainsKey(value) AndAlso node.Attributes("category") IsNot Nothing Then
						Dim category = CEnum.LocCategory.UNSECURE
						If node.Attributes("category") IsNot Nothing Then
							category = CEnum.SafeParseWithDefault(node.Attributes("category").Value, CEnum.LocCategory.UNSECURE)
						End If
						Dim overDraftOption As CEnum.LocOverDraftOption = CEnum.LocOverDraftOption.BOTH_OVERDRAFT_AND_NON_OVERDRAFT
						If node.Attributes("overdraft_option") IsNot Nothing Then
							overDraftOption = CEnum.SafeParseWithDefault(node.Attributes("overdraft_option").Value, CEnum.LocOverDraftOption.BOTH_OVERDRAFT_AND_NON_OVERDRAFT)
						End If
						LOCTypeList(value) = New SmLineOfCreditItem() With {.Text = LOCTypeList(value).Text, .Value = LOCTypeList(value).Value, .Category = category, .OverDraftOption = overDraftOption}
					End If

					'if purposes from xml config is not in download then remove them bc they are no longer exist in-branch
					If Not LOCTypeList.ContainsKey(value) Then
						LOCTypeList.Remove(value)
					End If

				Next
			End If

		End If
	End Sub
End Class
