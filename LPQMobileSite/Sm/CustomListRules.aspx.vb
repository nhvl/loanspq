﻿Imports LPQMobile.BO
Imports DownloadedSettings
Imports Newtonsoft.Json
Imports LPQMobile.Utils
Imports System.Xml
Imports Sm
Imports Microsoft.Ajax.Utilities

Partial Class Sm_CustomListRules
	Inherits SmApmBasePage

	Protected Property CurrentTab As String = ""
	Protected Property CurrentModule As SmSettings.ModuleName
	Protected Property TabTitle As String = ""
	Protected Property CustomListRulesJson As String

	Protected Property BLBusinessAccountTypesJson As String = "[]"
	Protected Property XABusinessAccountTypesJson As String = "[]"
	Protected Property VehicleTypesJson As String = "[]"
	Protected Property CreditCardPurposesJson As String = "[]"
	Protected Property CreditCardTypesJson As String = "[]"
	Protected Property HomeEquityPurposesJson As String = "[]"
	Protected Property HomeEquityReasonsJson As String = "[]"
	Protected Property HomeEquityPropertiesJson As String = "[]"
	Protected Property PersonalLoanPurposesJson As String = "[]"
	Protected Property PersonalLineOfCreditsJson As String = "[]"
	Protected Property VehicleLoanPurposesJson As String = "[]"
	Protected Property OtherLoanPurposesJson As String = "[]"
	Protected Property CitizenshipJson As String = "[]"
	Protected Property EmployeeOfLenderJson As String = "[]"
	Protected Property LocationPoolJson As String = "[]"
	Protected Property OccupancyStatusJson As String = "[]"
	Protected Property SpecialAccountTypesJson As String = "[]"
	Protected Property ProductCodesJson As String = "[]"
	Protected Property StatesJson As String = "[]"

	Protected Property ApplicationCQFilterItems As List(Of SmCustomListFilterItem)
	Protected Property ApplicantCQFilterItems As List(Of SmCustomListFilterItem)
	Protected Property ProductQuestionsFilterItems As List(Of SmCustomListFilterItem)

	Private Sub LoadModule(smMasterPage As Sm_SiteManager, ByRef loanNodeName As String, ByRef tabName As String)
		Select Case CurrentTab
			Case "CC"
				CurrentModule = SmSettings.ModuleName.CC
				loanNodeName = "CREDIT_CARD_LOAN"
				smMasterPage.CurrentNodeGroup = SmSettings.NodeGroup.CCCustomListRules
				tabName = "Credit Card"
				TabTitle = "Credit Card Loan Lists"
			Case "HE"
				CurrentModule = SmSettings.ModuleName.HE
				loanNodeName = "HOME_EQUITY_LOAN"
				smMasterPage.CurrentNodeGroup = SmSettings.NodeGroup.HECustomListRules
				tabName = "Home Equity Loan"
				TabTitle = "Home Equity Loan Lists"
			Case "PL"
				CurrentModule = SmSettings.ModuleName.PL
				loanNodeName = "PERSONAL_LOAN"
				smMasterPage.CurrentNodeGroup = SmSettings.NodeGroup.PLCustomListRules
				tabName = "Personal Loan"
				TabTitle = "Personal Loan Lists"
			Case "VL"
				CurrentModule = SmSettings.ModuleName.VL
				loanNodeName = "VEHICLE_LOAN"
				smMasterPage.CurrentNodeGroup = SmSettings.NodeGroup.VLCustomListRules
				tabName = "Vehicle Loan"
				TabTitle = "Vehicle Loan Lists"
			Case "BL"
				CurrentModule = SmSettings.ModuleName.BL
				loanNodeName = "BUSINESS_LOAN"
				smMasterPage.CurrentNodeGroup = SmSettings.NodeGroup.BLCustomListRules
				tabName = "BL"
				TabTitle = "Business Loan Lists"
			Case "XA"
				CurrentTab = "XA"
				CurrentModule = SmSettings.ModuleName.XA
				loanNodeName = "XA_LOAN"
				smMasterPage.CurrentNodeGroup = SmSettings.NodeGroup.XACustomListRules
				tabName = "XA"
				TabTitle = "Xpress Account Lists"
			Case Else
				Throw New Exception(String.Format("Unknown tab value '{0}'", TAB))
		End Select
	End Sub

	Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
		If Not Page.IsPostBack Then
			Dim smMasterPage = CType(Me.Master, Sm_SiteManager)
			CurrentTab = Common.SafeEncodeString(Request.QueryString("clrtab")).ToUpper()
			Dim loanNodeName As String = ""
			Dim tabName As String = ""
			Select Case CurrentTab
				' If a specific module was specified, load that
				Case "CC", "HE", "PL", "VL", "BL", "XA"
					LoadModule(smMasterPage, loanNodeName, tabName)
				Case Else
					' Else, a specific was not specified, and we should try defaulting to the first available enabled module
					If EnableXA Then
						CurrentTab = "XA"
					ElseIf EnableBL Then
						CurrentTab = "BL"
					ElseIf EnableCC Then
						CurrentTab = "CC"
					ElseIf EnableHE Then
						CurrentTab = "HE"
					ElseIf EnablePL Then
						CurrentTab = "PL"
					ElseIf EnableVL Then
						CurrentTab = "VL"
					End If

					If Not String.IsNullOrEmpty(CurrentTab) Then
						LoadModule(smMasterPage, loanNodeName, tabName)
					End If
			End Select

			If String.IsNullOrEmpty(CurrentTab) OrElse Not CheckEnabledModule(CurrentModule) Then
				'terminate current page
				Response.Redirect(BuildUrl("/sm/index.aspx", New List(Of String) From {"clrtab"}), True)
			End If

			Select Case CurrentTab
				Case "CC"
					CreditCardPurposesJson = GetCreditCardPurposesJson()
					CreditCardTypesJson = GetCreditCardTypesJson()
				Case "HE"
					HomeEquityPurposesJson = GetHomeEquityPurposesJson()
					HomeEquityReasonsJson = GetHomeEquityReasonsJson()
					HomeEquityPropertiesJson = GetHomeEquityPropertiesJson()
					StatesJson = GetStatesJson()
				Case "PL"
					PersonalLoanPurposesJson = GetPersonalLoanPurposesJson()
					PersonalLineOfCreditsJson = GetPersonalLineOfCreditsJson()
				Case "VL"
					VehicleLoanPurposesJson = GetVehicleLoanPurposesJson()
					VehicleTypesJson = GetVehicleTypesJson()
				Case "BL"
					CreditCardTypesJson = GetBLCreditCardTypesJson()
					CreditCardPurposesJson = GetBLCreditCardPurposesJson()
					VehicleLoanPurposesJson = GetBLVehicleLoanPurposesJson()
					OtherLoanPurposesJson = GetBLOtherLoanPurposesJson()
					BLBusinessAccountTypesJson = GetBLBusinessAccountTypesJson()
				Case Else
					XABusinessAccountTypesJson = GetXABusinessAccountTypesJson()
					SpecialAccountTypesJson = GetSpecialAccountTypesJson()
					Dim productList = CProduct.GetAllProducts(WebsiteConfig)
					ProductCodesJson = GetProductCodesJson(productList)
					ProductQuestionsFilterItems = GetProductQuestions(productList)
			End Select

			CitizenshipJson = GetCitizenshipJson()
			EmployeeOfLenderJson = GetEmployeeOfLenderJson()
			LocationPoolJson = GetLocationPoolJson()
			OccupancyStatusJson = GetOccupancyStatusJson()

			'load CQ
			Dim copiedWebConfig As CWebsiteConfig = WebsiteConfig
			Dim visibleNode = copiedWebConfig._webConfigXML.SelectSingleNode("VISIBLE")
			If visibleNode Is Nothing Then
				Dim node As XmlNode = copiedWebConfig._webConfigXML.OwnerDocument.CreateElement("VISIBLE")
				copiedWebConfig._webConfigXML.AppendChild(node)
				visibleNode = copiedWebConfig._webConfigXML.SelectSingleNode("VISIBLE")
			End If
			Dim customQuestionNewApiAttrNode As XmlNode = copiedWebConfig._webConfigXML.SelectSingleNode("VISIBLE/@custom_question_new_api")
			If customQuestionNewApiAttrNode Is Nothing Then
				Dim attr As XmlAttribute = copiedWebConfig._webConfigXML.OwnerDocument.CreateAttribute("custom_question_new_api")
				attr.Value = "Y"
				visibleNode.Attributes.Append(attr)
			ElseIf customQuestionNewApiAttrNode.Value <> "Y" Then
				customQuestionNewApiAttrNode.Value = "Y"
			End If

			ApplicationCQFilterItems = GetApplicationCustomQuestions(loanNodeName, copiedWebConfig)
			ApplicantCQFilterItems = GetApplicantCustomQuestions(loanNodeName, copiedWebConfig)
			'end load CQ



			smMasterPage.LenderConfigXml = LenderConfigXml
			smMasterPage.SelectedMainMenuItem = "config"
			smMasterPage.RevisionID = LenderConfig.RevisionID
			smMasterPage.LastModifiedBy = LenderConfig.LastModifiedByUserFullName
			smMasterPage.LastModifiedByUserID = LenderConfig.LastModifiedByUserID
			smMasterPage.SelectedSubMenuItem = "customlistrules"
			smMasterPage.BreadCrumb = "Configure / Custom List Rules/ " & tabName
			Try
				Dim customListRulesNode As XmlNode = GetNode(Sm.CUSTOM_LIST_RULES(loanNodeName))
				If customListRulesNode IsNot Nothing Then
					CustomListRulesJson = customListRulesNode.InnerText
				End If
			Catch ex As Exception
				log.Error("Could not load Custom List Rules page", ex)
			End Try
		End If
	End Sub
	Private Function GetBLBusinessAccountTypesJson() As String
		Dim result As New Dictionary(Of String, String)
		Dim lst = CEnum.BUSINESSTYPEVALUE
		If lst IsNot Nothing AndAlso lst.Count > 0 Then
			For Each item In lst
				If result.ContainsKey(item) Then Continue For
				result.Add(item, CEnum.MapBusinessTypeName(item))
			Next
		End If
		Return JsonConvert.SerializeObject(result)
	End Function
	Private Function GetXABusinessAccountTypesJson() As String
		Dim result As New Dictionary(Of String, String)
		Dim lst = Common.GetBusinessAccountTypeList(WebsiteConfig)
		If lst IsNot Nothing AndAlso lst.Count > 0 Then
			For Each item In lst
				If result.ContainsKey(item.AccountCode) Then Continue For
				result.Add(item.AccountCode, CEnum.MapBusinessTypeName(item.BusinessType))
			Next
		End If
		Return JsonConvert.SerializeObject(result)
	End Function
	Private Function GetVehicleTypesJson() As String
		Dim vehicleTypes = WebsiteConfig.GetEnumItems("VEHICLE_TYPES")
		If vehicleTypes Is Nothing Then vehicleTypes = New Dictionary(Of String, String)
		Return JsonConvert.SerializeObject(vehicleTypes)
	End Function
	Private Function GetBLCreditCardTypesJson() As String
		Dim cardTypes = WebsiteConfig.GetEnumItems("BL_CREDIT_CARD_TYPES")
		If cardTypes Is Nothing Then cardTypes = New Dictionary(Of String, String)
		Return JsonConvert.SerializeObject(cardTypes)
	End Function
	Private Function GetBLCreditCardPurposesJson() As String
		Dim cardPurposes = WebsiteConfig.GetEnumItems("BL_CREDIT_CARD_PURPOSES")
		If cardPurposes Is Nothing Then cardPurposes = New Dictionary(Of String, String)
		Return JsonConvert.SerializeObject(cardPurposes)
	End Function
	Private Function GetBLVehicleLoanPurposesJson() As String
		Dim cardPurposes = WebsiteConfig.GetEnumItems("BL_VEHICLE_PURPOSES")
		If cardPurposes Is Nothing Then cardPurposes = New Dictionary(Of String, String)
		Return JsonConvert.SerializeObject(cardPurposes)
	End Function
	Private Function GetBLOtherLoanPurposesJson() As String
		Dim cardPurposes = WebsiteConfig.GetEnumItems("BL_OTHER_PURPOSES")
		If cardPurposes Is Nothing Then cardPurposes = New Dictionary(Of String, String)
		Return JsonConvert.SerializeObject(cardPurposes)
	End Function

	Private Function GetCreditCardPurposesJson() As String
		Dim cardPurposes = WebsiteConfig.GetEnumItems("CREDIT_CARD_LOAN_PURPOSES")
		If cardPurposes Is Nothing Then cardPurposes = New Dictionary(Of String, String)
		Return JsonConvert.SerializeObject(cardPurposes)
	End Function

	Private Function GetHomeEquityPurposesJson() As String
		Dim purposes = WebsiteConfig.GetEnumItems("HOME_EQUITY_LOAN_PURPOSES")
		If purposes Is Nothing Then purposes = New Dictionary(Of String, String)
		Return JsonConvert.SerializeObject(purposes)
	End Function
	Private Function GetHomeEquityReasonsJson() As String
		Dim reasons = WebsiteConfig.GetEnumItems("HOME_EQUITY_LOAN_REASONS")
		If reasons Is Nothing Then reasons = New Dictionary(Of String, String)
		Return JsonConvert.SerializeObject(reasons)
	End Function
	Private Function GetHomeEquityPropertiesJson() As String
		Dim properties = WebsiteConfig.GetEnumItems("PROPERTY_TYPES")
		If properties Is Nothing Then properties = New Dictionary(Of String, String)
		Return JsonConvert.SerializeObject(properties)
	End Function

	Private Function GetStatesJson() As String
		Return JsonConvert.SerializeObject(CEnum.STATES.ToDictionary(Function(s) s, Function(s) CEnum.MapStatesToString(s)))
	End Function
	Private Function GetPersonalLineOfCreditsJson() As String
		Dim purposes = WebsiteConfig.GetEnumItems("PERSONAL_LOC_PURPOSES")
		If purposes Is Nothing Then purposes = New Dictionary(Of String, String)
		Return JsonConvert.SerializeObject(purposes)
	End Function
	Private Function GetPersonalLoanPurposesJson() As String
		Dim purposes = WebsiteConfig.GetEnumItems("PERSONAL_LOAN_PURPOSES")
		If purposes Is Nothing Then purposes = New Dictionary(Of String, String)
		Return JsonConvert.SerializeObject(purposes)
	End Function
	Private Function GetVehicleLoanPurposesJson() As String
		Dim purposes = WebsiteConfig.GetEnumItems("VEHICLE_LOAN_PURPOSES")
		If purposes Is Nothing Then purposes = New Dictionary(Of String, String)
		Return JsonConvert.SerializeObject(purposes)
	End Function
	Private Function GetEmployeeOfLenderJson() As String
		Dim employeeOfLenderType = WebsiteConfig.GetEnumItems("EMPLOYEE_OF_LENDER_TYPE")
		If employeeOfLenderType Is Nothing Then Return "[]"
		Return JsonConvert.SerializeObject(employeeOfLenderType)
	End Function
	Private Function GetLocationPoolJson() As String
		Dim currentLenderServiceConfigInfo = CDownloadedSettings.DownloadLenderServiceConfigInfo(WebsiteConfig)
		Return JsonConvert.SerializeObject(currentLenderServiceConfigInfo.ZipCodePoolName)
	End Function
	Private Function GetOccupancyStatusJson() As String
		Dim result As New Dictionary(Of String, String)
		For Each s In CEnum.OCCUPY_LOCATIONS
			If result.ContainsKey(s) Then Continue For
			result.Add(s, StrConv(s, VbStrConv.ProperCase))
		Next
		Return JsonConvert.SerializeObject(result)
	End Function
	Private Function GetSpecialAccountTypesJson() As String
		Dim result As New Dictionary(Of String, String)
		Dim specialAccountTypeList = Common.GetSpecialAccountTypeList(WebsiteConfig)
		If specialAccountTypeList IsNot Nothing AndAlso specialAccountTypeList.Count > 0 Then
			For Each item In specialAccountTypeList
				If result.ContainsKey(item.AccountCode) Then Continue For
				result.Add(item.AccountCode, item.AccountName)
			Next
		End If
		Return JsonConvert.SerializeObject(result)
	End Function
	Private Function GetProductCodesJson(productList As List(Of CProduct)) As String
		Dim result As New Dictionary(Of String, String)
		If productList IsNot Nothing AndAlso productList.Count > 0 Then
			For Each item In productList
				If result.ContainsKey(item.ProductCode) Then Continue For
				result.Add(item.ProductCode, item.AccountName)
			Next
		End If
		Return JsonConvert.SerializeObject(result)
	End Function
	Private Function GetProductQuestions(productList As List(Of CProduct)) As List(Of SmCustomListFilterItem)
		Dim result As New List(Of SmCustomListFilterItem)
		Try
			For Each prod As CProduct In productList
				If prod.CustomQuestions IsNot Nothing AndAlso prod.CustomQuestions.Count > 0 Then
					result.AddRange(From question In prod.CustomQuestions Select TranslateProductQuestionsToFilterItem(question.XAProductQuestionID, question.QuestionName, question.QuestionText, question.AnswerType, question.RestrictedAnswers))
				End If
			Next
		Catch ex As Exception
			log.Error("Could not load GetProductQuestions", ex)
		End Try
		Return result.DistinctBy(Function(p) p.id).OrderBy(Function(r) r.label).ToList()
	End Function

	Private Function GetCitizenshipJson() As String
		Dim citizenshipList = WebsiteConfig.GetEnumItems("CITIZENSHIP_TYPES")
		If citizenshipList Is Nothing OrElse citizenshipList.Count = 0 Then	 ' not available in  LPQMobileWebsiteConfig
			Dim defaultCitizenshipValues = CEnum.CITIZENSHIP_STATUS.OrderBy(Function(x) x).ToArray
			For Each item In defaultCitizenshipValues
				If Not citizenshipList.ContainsKey(item) Then
					citizenshipList.Add(item, CEnum.MapBusinessTypeName(item))
				End If
			Next	''from hard code
		End If
		Return JsonConvert.SerializeObject(citizenshipList)
	End Function
	Private Function GetCreditCardTypesJson() As String
		Dim creditCardTypes = WebsiteConfig.GetEnumItems("CREDIT_CARD_TYPES")
		If creditCardTypes Is Nothing Then creditCardTypes = New Dictionary(Of String, String)
		Return JsonConvert.SerializeObject(creditCardTypes)
	End Function

	Private Function GetApplicantCustomQuestions(loanNodeName As String, config As CWebsiteConfig) As List(Of SmCustomListFilterItem)
		Dim result As New List(Of SmCustomListFilterItem)
		Try
			Dim applicantCQ As XmlDocument = CDownloadedSettings.CustomApplicantQuestions(config)
			Dim enabledApplicantCQs = ReadEnabledCQFromLiveConfig(CUSTOM_APPLICANT_QUESTIONS(loanNodeName))
			result.AddRange(From question In parseCustomQuestionXml(applicantCQ, CurrentTab) Where enabledApplicantCQs.Contains(question.name) Select TranslateToFilterItem("AQ", question.name, question.text, question.dataType, question.answerType, question.CustomQuestionOptions))
		Catch ex As Exception
			log.Error("Could not load GetApplicantCustomQuestions", ex)
		End Try
		Return result.OrderBy(Function(r) r.label).ToList()
	End Function
	Private Function GetApplicationCustomQuestions(loanNodeName As String, config As CWebsiteConfig) As List(Of SmCustomListFilterItem)
		Dim result As New List(Of SmCustomListFilterItem)
		Try
			Dim applicationCQ As XmlDocument = CDownloadedSettings.CustomApplicationQuestions(config)
			Dim enabledApplicationCQs = ReadEnabledCQFromLiveConfig(CUSTOM_QUESTIONS(loanNodeName))
			result.AddRange(From question In parseCustomQuestionXml(applicationCQ, CurrentTab) Where enabledApplicationCQs.Contains(question.name) Select TranslateToFilterItem("CQ", question.name, question.text, question.dataType, question.answerType, question.CustomQuestionOptions))
		Catch ex As Exception
			log.Error("Could not load GetApplicationCustomQuestions", ex)
		End Try
		Return result.OrderBy(Function(r) r.label).ToList()
	End Function
	Private Function parseCustomQuestionXml(xmlDoc As XmlDocument, loanType As String) As List(Of CCustomQuestionNewAPI)
		Dim questions As New List(Of CCustomQuestionNewAPI)
		If xmlDoc IsNot Nothing Then
			Dim questionNodes As XmlNodeList = xmlDoc.SelectNodes("OUTPUT/CUSTOM_QUESTIONS/CUSTOM_QUESTION[contains(@app_source, 'consumer') and @is_active='true']")
			If questionNodes IsNot Nothing AndAlso questionNodes.Count > 0 Then
				For Each question As XmlElement In questionNodes
					Dim headerAnswerType As XmlNodeList = question.SelectNodes("ANSWER_TYPE[text()='HEADER']")
					If headerAnswerType IsNot Nothing AndAlso headerAnswerType.Count > 0 Then Continue For

					Dim filterByLoanType As XmlNodeList = question.SelectNodes("APP_TYPES_SUPPORTED/APP_TYPE[text()='" & loanType & "']")
					If filterByLoanType IsNot Nothing AndAlso filterByLoanType.Count > 0 Then
						questions.Add(New CCustomQuestionNewAPI(question))
					End If
				Next
			End If
		End If
		Return questions
	End Function
	Private Function ReadEnabledCQFromLiveConfig(xPath As String) As List(Of String)
		Dim result As New List(Of String)
		Dim cqNode As XmlNode = GetNode(xPath)
		If cqNode IsNot Nothing AndAlso cqNode.HasChildNodes Then
			result.AddRange(From node As XmlNode In cqNode.ChildNodes Where node.Attributes("name").Value IsNot Nothing Select node.Attributes("name").Value)
		End If
		Return result
	End Function

	Private Function TranslateToFilterItem(idPrefix As String, name As String, text As String, dataType As String, uiType As String, questionOptions As SerializableList(Of CCustomQuestionOption)) As SmCustomListFilterItem
		Dim item As New SmCustomListFilterItem
		item.id = String.Format("%{0}_{1}%", idPrefix, name.Replace(" ", "_"))
		item.label = String.Format("{0} (""{1}"")", name, text.Replace(vbTab, "").Replace(vbCr, "").Replace(vbLf, "").Replace(vbCrLf, "").Truncate(120))
		Select Case dataType
			Case "N"
				item.type = "integer"
				item.operators = "numeric"
			Case "C"
				item.type = "double"
				item.operators = "numeric"
			Case Else
				item.type = "string"
				item.operators = "string"
		End Select
		Select Case uiType.ToUpper()
			Case "CHECKBOX"
				item.input = "checkbox"
				item.values = questionOptions.ToDictionary(Function(p) p.Value, Function(q As CCustomQuestionOption) q.Text)
				'item.multiple = "false"
				item.operators = "checkbox"
			Case "RADIO"
				item.input = "radio"
				item.values = questionOptions.ToDictionary(Function(p) p.Value, Function(q As CCustomQuestionOption) q.Text)
				'item.multiple = "false"
			Case "DROPDOWN"
				item.input = "select"
				item.values = questionOptions.ToDictionary(Function(p) p.Value, Function(q As CCustomQuestionOption) q.Text)
				'item.multiple = "false"
			Case Else
				item.input = "text"
				'item.multiple = "false"
		End Select
		Return item
	End Function
	Private Function TranslateProductQuestionsToFilterItem(id As String, name As String, text As String, uiType As String, questionOptions As List(Of CProductRestrictedAnswer)) As SmCustomListFilterItem
		Dim item As New SmCustomListFilterItem
		item.id = String.Format("%PQ_{0}%", id.Replace("-", "_"))
		item.label = String.Format("{0} (""{1}"")", name, Common.CleanupQuestionText(text).Truncate(120))
		Select Case uiType.ToUpper()
			Case "CHECKBOX"
				item.input = "checkbox"
				item.values = questionOptions.ToDictionary(Function(p) p.Value, Function(q As CProductRestrictedAnswer) q.Text)
				'item.multiple = "false"
				item.type = "string"
				item.operators = "checkbox"
			Case "RADIO"
				item.input = "radio"
				item.values = questionOptions.ToDictionary(Function(p) p.Value, Function(q As CProductRestrictedAnswer) q.Text)
				'item.multiple = "false"
				item.type = "string"
				item.operators = "dropdown"
			Case "DROPDOWN"
				item.input = "select"
				item.values = questionOptions.ToDictionary(Function(p) p.Value, Function(q As CProductRestrictedAnswer) q.Text)
				'item.multiple = "false"
				item.type = "string"
				item.operators = "dropdown"
			Case "DATE"
				'item.multiple = "false"
				item.type = "date"
				item.operators = "string"
			Case Else
				item.input = "text"
				'item.multiple = "false"
				item.type = "string"
				item.operators = "string"
		End Select
		Return item
	End Function
End Class