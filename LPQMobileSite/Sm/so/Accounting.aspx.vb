﻿
Partial Class Sm_so_Accounting
	Inherits SmBasePage
	Protected TimePeriodLst As List(Of Date)


	Protected Sub Page_PreLoad(sender As Object, e As EventArgs) Handles Me.PreLoad
		Dim smMasterPage = CType(Me.Master, Sm_MasterPage)
		smMasterPage.SelectedMainMenuItem = "accounting"
		AllowedRoles.Add(SmSettings.Role.SuperOperator.ToString())
		AllowedRoles.Add(SmSettings.Role.LegacyOperator.ToString())
		AllowedRoles.Add(SmSettings.Role.Accountant.ToString())
		NotAllowedAccessRedirectUrl = "/Sm/Default.aspx"
	End Sub
	Protected Sub Page_Load1(sender As Object, e As EventArgs) Handles Me.Load
		If Not IsPostBack Then
			TimePeriodLst = New List(Of Date)()
			Dim currentMonth = New DateTime(Now.Year, Now.Month, 1)
			For i As Integer = 1 To 6
				Dim m = currentMonth.AddMonths(-1 * i)
				TimePeriodLst.Add(m)
			Next
		End If
	End Sub
End Class