﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="ManageUsers.aspx.vb" Inherits="Sm_so_ManageUsers" MasterPageFile="../MasterPage.master" %>

<asp:Content runat="server" ContentPlaceHolderID="plBody">
	<div>
		<h1 class="page-header">Manage Users</h1>
		<p>Use this page to manage all users who can access Application Portal Management System.</p>
		<div>
			<button type="button" id="btnAddNew" class="btn btn-primary bottom10 pull-right">Add User</button>
		</div>
		<div class="search-bar">
			<div class="input-group">
				<input id="txtSearch" type="text" class="form-control" placeholder="Search for..." />
				<span class="input-group-btn"><button class="btn btn-secondary" type="button" id="btnSearch">Search</button></span>
			</div>
		</div>
		<div class="grid" id="divUserList">
			<div class="spinner">
				<div class="rect1"></div>
				<div class="rect2"></div>
				<div class="rect3"></div>
				<div class="rect4"></div>
				<div class="rect5"></div>
			</div>
			<p>Loading...</p>
		</div>
	</div>
</asp:Content>
<asp:Content runat="server" ContentPlaceHolderID="plScripts">
	<script type="text/javascript">
		$(function () {
			var userGrid;
			userGrid = new _COMMON.Grid("/sm/smhandler.aspx", "userGrid", "divUserList", manageUsers.FACTORY, manageUsers.FACTORY.onLoaded, { command: "loadUserGrid" });
			userGrid.init();
			manageUsers.FACTORY.init(userGrid.GridObject);
			$("#btnSearch").on("click", manageUsers.FACTORY.search);
			$('#txtSearch').on("keypress", function (e) {
				var code = (e.keyCode ? e.keyCode : e.which);
				if (code == 13) {
					manageUsers.FACTORY.search();
				}
			});
		});
		(function (manageUsers, $, undefined) {
			manageUsers.DATA = {};
			manageUsers.FACTORY = {};
			manageUsers.FACTORY.init = function (gridInstance) {
				var self = this;
				manageUsers.DATA.grid = gridInstance;
				$("#btnAddNew").on("click", manageUsers.FACTORY.showAddUserForm);
			};
			manageUsers.FACTORY.getFilterInfo = function () {
				//implement this later
				return { Keyword: $.trim($("#txtSearch").val()) };
			};
			manageUsers.FACTORY.onLoaded = function() {
				$("input:checkbox[data-command='enable-btn']", $("#divUserList")).each(function(idx, ele) {
					var $self = $(ele);
					$self.on("change", function () {
						var userid = $self.closest("tr").find("input:hidden[data-field='userid']").val();
						manageUsers.FACTORY.ChangeState(userid, $self.is(":checked"), $self);
					});
				});
				$("a[data-command='resetRetry']", $("#divUserList")).each(function (idx, ele) {
					var $self = $(ele);
					$self.on("click", function () {
						var userid = $self.closest("tr").find("input:hidden[data-field='userid']").val();
						manageUsers.FACTORY.ResetRetry(userid, $self);
					});
				});
				$("a[data-command='delete']", $("#divUserList")).each(function (idx, ele) {
					var $self = $(ele);
					$self.on("click", function () {
						var userid = $self.closest("tr").find("input:hidden[data-field='userid']").val();
						manageUsers.FACTORY.DeleteUser(userid, $self);
					});
				});
				$("a[data-command='edit']", $("#divUserList")).each(function (idx, ele) {
					var $self = $(ele);
					$self.on("click", function () {
						var userid = $self.closest("tr").find("input:hidden[data-field='userid']").val();
						manageUsers.FACTORY.showEditUserForm(userid);
					});
				});
			};
			manageUsers.FACTORY.ResetRetry = function (userid, sourceEle) {
				var dataObj = { userid: userid, command: "resetLoginFailureCount" }
				$.ajax({
					url: "/sm/smhandler.aspx",
					async: true,
					cache: false,
					type: 'POST',
					dataType: 'html',
					data: dataObj,
					success: function (responseText) {
						var response = $.parseJSON(responseText);
						if (response.IsSuccess) {
							_COMMON.noty("success", "Success", 500);
							var pageIndex = manageUsers.DATA.grid.getPaginationSetting().page;
							manageUsers.DATA.grid.setFilter(manageUsers.FACTORY.getFilterInfo()).setPageIndex(pageIndex).load();
						} else {
							_COMMON.noty("error", "Error", 500);
						}
					}
				});
			}
			manageUsers.FACTORY.DeleteUser = function (userid, sourceEle) {
				var dataObj = { userid: userid, command: "deleteUser" }
				$.ajax({
					url: "/sm/smhandler.aspx",
					async: true,
					cache: false,
					type: 'POST',
					dataType: 'html',
					data: dataObj,
					success: function (responseText) {
						var response = $.parseJSON(responseText);
						if (response.IsSuccess) {
							_COMMON.noty("success", "Success", 500);
							if (response.Info == "RELOAD") {
								window.location = "/logout.aspx";
							}
							var pageIndex = manageUsers.DATA.grid.getPaginationSetting().page;
							manageUsers.DATA.grid.setFilter(manageUsers.FACTORY.getFilterInfo()).setPageIndex(pageIndex).load();
						} else {
							_COMMON.noty("error", "Error", 500);
						}
					}
				});
			}
			manageUsers.FACTORY.ChangeState = function(userid, isEnabled, sourceEle) {
				var dataObj = { userid: userid, enable: isEnabled ? "Y" : "N", command: "changeUserState" }
				$.ajax({
					url: "/sm/smhandler.aspx",
					async: true,
					cache: false,
					type: 'POST',
					dataType: 'html',
					data: dataObj,
					success: function(responseText) {
						var response = $.parseJSON(responseText);
						if (response.IsSuccess) {
							_COMMON.noty("success", "Success", 500);
							if (response.Info == "RELOAD") {
								window.location = window.location.href.replace(/#\w*$/, "");
							}
							var pageIndex = manageUsers.DATA.grid.getPaginationSetting().page;
							manageUsers.DATA.grid.setFilter(manageUsers.FACTORY.getFilterInfo()).setPageIndex(pageIndex).load();
						} else {
							_COMMON.noty("error", "Error", 500);
							$(sourceEle).prop("checked", !isEnabled);
						}
					}
				});
			};
			manageUsers.FACTORY.showAddUserForm = function() {
				_COMMON.showDialogSaveClose("Add New User", 0, "adduser_dialog_0", "", "/sm/smhandler.aspx", {command:"loadAddUserForm"}, manageUsers.FACTORY.saveNewUser, function (container) {
					//add validator
					registerDataValidator($("#adduser_dialog_0"), "adduser_dialog_0");
					$("#adduser_dialog_0 .input-group.date").each(function () {
						$(this).datepicker({
							clearBtn: true,
							autoclose: true,
							todayHighlight: true
						});
					});
				}, function () {
					//reload grid when dialog closed
					$.smValidate.cleanGroup("adduser_dialog_0");
					var pageIndex = manageUsers.DATA.grid.getPaginationSetting().page;
					manageUsers.DATA.grid.setFilter(manageUsers.FACTORY.getFilterInfo()).setPageIndex(pageIndex).load();
				});
			};
			manageUsers.FACTORY.saveNewUser = function (obj) {
				if ($.smValidate("adduser_dialog_0") == false) return;
				var userData = obj.container.find("form").serializeJSON({ parseAll: true, checkboxUncheckedValue: "false" });
				//update data
				var postData = {
					command: "saveNewUser",
					user_data: _COMMON.toJSON(userData)
				};
				$.ajax({
					url: "/sm/smhandler.aspx",
					async: true,
					cache: false,
					type: 'POST',
					dataType: 'html',
					data: postData,
					success: function (responseText) {
						var response = $.parseJSON(responseText);
						if (response.IsSuccess) {
							_COMMON.noty("success", "Success", 500);
							obj.dialog.close();
						} else if (response.Info == "EXISTED") {
							_COMMON.showAlertDialog(response.Message);
						} else {
							var errorMsg = response.Message !== "" ? response.Message : "Error";
							_COMMON.noty("error", errorMsg, 500);
						}
					}
				});
			};
			manageUsers.FACTORY.showEditUserForm = function (userId) {
				_COMMON.showDialogSaveClose("Edit User", 0, "edituser_dialog_0", "", "/sm/smhandler.aspx", { command: "loadEditUserForm", userId: userId }, manageUsers.FACTORY.updateUser, function (container) {
					//add validator
					registerDataValidator($("#edituser_dialog_0"), "edituser_dialog_0");
					initEditUploadUserAvatar();
					$("#edituser_dialog_0 .input-group.date").each(function () {
						$(this).datepicker({
							clearBtn: true,
							autoclose: true,
							todayHighlight: true
						});
					});
				}, function () {
					//reload grid when dialog closed
					$.smValidate.cleanGroup("edituser_dialog_0");
					var pageIndex = manageUsers.DATA.grid.getPaginationSetting().page;
					manageUsers.DATA.grid.setFilter(manageUsers.FACTORY.getFilterInfo()).setPageIndex(pageIndex).load();
				});
			};
			manageUsers.FACTORY.updateUser = function (obj) {
				if ($.smValidate("edituser_dialog_0") == false) return;
				var userData = obj.container.find("form").serializeJSON({ parseAll: true, checkboxUncheckedValue: "false" });
				//update data
				var postData = {
					command: "updateUser",
					user_data: _COMMON.toJSON(userData)
				};
				$.ajax({
					url: "/sm/smhandler.aspx",
					async: true,
					cache: false,
					type: 'POST',
					dataType: 'html',
					data: postData,
					success: function (responseText) {
						var response = $.parseJSON(responseText);
						if (response.IsSuccess) {
							_COMMON.noty("success", "Success", 500);
							obj.dialog.close();
							if (response.Info.avatar) {
								$(".sm-header .profile-block span.avatar").css("background-image", "url('" + response.Info.avatar + "')");
								$(".sm-header .profile-block span.js-profile-firstname").text(response.Info.firstName);
							}
						} else if (response.Info == "EXISTED") {
							_COMMON.showAlertDialog(response.Message);
						} else {
							var errorMsg = response.Message !== "" ? response.Message : "Error";
							_COMMON.noty("error", errorMsg, 500);
						}
					}
				});
			};
			manageUsers.FACTORY.search = function () {
				manageUsers.DATA.grid.setFilter(manageUsers.FACTORY.getFilterInfo()).setPageIndex(1).load();
			};
		}(window.manageUsers = window.manageUsers || {}, jQuery));
		function registerDataValidator(container, groupName) {
			var $container = $(container);
			$.smValidate.removeValidationGroup(groupName);
			$("[data-required='true'],[data-format]", $container).each(function (idx, ele) {
				$(ele).observer({
					validators: [
						function (partial) {
							var $self = $(this);
							if ($self.data("required") && $.trim($self.val()) === "") {
								if ($self.data("message-require")) {
									return $self.data("message-require");
								} else {
									return "This field is required";
								}
							}
							var format = $self.data("format");
							if (format) {
								var msg = "Invalid format";
								if ($self.data("message-invalid-data")) {
									msg = $self.data("message-invalid-data");
								}
								if (format === "email") {
									if (_COMMON.ValidateEmail($self.val()) === false) {
										return msg;
									}		
								}else if (format === "phone") {
									if (_COMMON.ValidatePhone($self.val()) === false) {
										return msg;
									}
								}
							}
							return "";
						}
					],
					validateOnBlur: true,
					group: groupName
				});
			});
		}
	</script>
</asp:Content>
<asp:Content runat="server" ContentPlaceHolderID="plActionButtons"></asp:Content>