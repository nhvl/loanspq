﻿
Imports DBUtils
Imports LPQMobile.BO
Imports Newtonsoft.Json
Imports System.Xml
Imports Sm.BO
Imports System.Data
Imports LPQMobile.Utils

Partial Class Sm_so_Configure
	Inherits SmBasePage
	Protected LPQMOBILE_CONNECTIONSTRING As String = System.Web.Configuration.WebConfigurationManager.ConnectionStrings("LPQMOBILE_CONNECTIONSTRING").ConnectionString

	Protected UnEncryptedPasswordRecordCounter As Integer
	'Protected UnCompressedRecordCounter As Integer
	Protected UnDiffedAuditLogsCounter As Integer
	Protected Const NumberOfLogItemWillBeProcessed As Integer = 1000
	Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
		Dim smMasterPage = CType(Me.Master, Sm_MasterPage)
		smMasterPage.SelectedMainMenuItem = "config"

		If Not SmUtil.CheckRoleAccess(HttpContext.Current, SmSettings.Role.SuperOperator) Then
			Response.Redirect("/sm/default.aspx")
			Return
		End If

		If Not Page.IsPostBack Then
			Dim cmd As String = Common.SafeString(Request.Params("command"))
			If cmd = "runencryptPasswords" Then
				Dim res As CJsonResponse
				Try

					Dim lenderList As Dictionary(Of Guid, XmlDocument)
					lenderList = GetAllLenderConfigDatas()
					log.Debug("Begin encrypting " & lenderList.Count.ToString)
					Dim nCount As Integer = 0
					If lenderList IsNot Nothing AndAlso lenderList.Any() Then
						For Each item As KeyValuePair(Of Guid, XmlDocument) In lenderList
							Try
								nCount += 1
								RunBatchEncryptAllPasswords(item.Key)
								If nCount Mod 20 = 0 Then
									log.Debug("Encrypted " & nCount.ToString)
								End If
							Catch ex As Exception
								log.Error("Could not run batch to encrypt all passwords in config xml, lenderref: ", ex)
							End Try
						Next
					End If
					res = New CJsonResponse(True, "", "OK")
				Catch ex As Exception
					log.Error("Could not run batch to encrypt all passwords in config xml.", ex)
					res = New CJsonResponse(False, "Could not run batch. Please try again", True)
				End Try
				Response.Write(JsonConvert.SerializeObject(res))
				Response.End()
				Return
			ElseIf cmd = "runEncryptPasswordsOfLatestNLogRecords" Then
				Dim res As CJsonResponse
				Try
					Dim records As Dictionary(Of String, Tuple(Of String, String))
					records = GetLatestUnEncryptedLogRecord(NumberOfLogItemWillBeProcessed)
					log.Debug("Begin encrypting passwords in log records")
					If records IsNot Nothing AndAlso records.Any() Then
						Dim oDB As New CSQLDBUtils(LPQMOBILE_CONNECTIONSTRING)
						Try
							Dim oWhere As CSQLWhereStringBuilder
							Dim oUpdate As cSQLUpdateStringBuilder
							For Each record In records
								Try
									oWhere = New CSQLWhereStringBuilder
									oWhere.AppendAndCondition("LenderConfigLogID={0}", New CSQLParamValue(record.Key))
									oUpdate = New cSQLUpdateStringBuilder("LenderConfigLogs", oWhere)
									oUpdate.appendvalue("ConfigData", New CSQLParamValue(CStringCompressor.Compress(Common.EncryptAllPasswordInConfigXml(record.Value.Item2).OuterXml)))
									oUpdate.appendvalue("LPQPW", New CSQLParamValue(Common.EncryptPassword(record.Value.Item1)))
									log.Debug(oUpdate.SQL)
									oDB.executeNonQuery(oUpdate.SQL, False)
								Catch ex As Exception
									log.Error("Error while runEncryptPasswordsOfLatestNLogRecords , partial fail", ex)
								End Try
							Next
							'oDB.commitTransactionIfOpen()
						Catch ex As Exception
							log.Error("Error while runEncryptPasswordsOfLatestNLogRecords", ex)
							'oDB.rollbackTransactionIfOpen()
						Finally
							oDB.closeConnection()
						End Try

					End If
					res = New CJsonResponse(True, "", New With {.count = CountUnEncryptedPasswordsLenderConfigLogsRecords()})
				Catch ex As Exception
					log.Error("Could not run batch to encrypt all passwords in config xml.", ex)
					res = New CJsonResponse(False, "Could not run batch. Please try again", True)
				End Try
				Response.Write(JsonConvert.SerializeObject(res))
				Response.End()
				Return

				' I think we do not need this anymore. ConfigData is compressed when running "runEncryptPasswordsOfLatestNLogRecords" task
				'ElseIf cmd = "compressLenderConfigLogs" Then
				'	Dim res As CJsonResponse
				'	Try
				'		Dim records As Dictionary(Of String, String)
				'		records = GetLatestUnCompressedLogRecord(NumberOfLogItemWillBeProcessed, True)
				'		log.Debug("Begin compress configdata in log records")
				'		If records IsNot Nothing AndAlso records.Any() Then
				'			Dim oDB As New CSQLDBUtils(LPQMOBILE_CONNECTIONSTRING)
				'			Try
				'				Dim oWhere As CSQLWhereStringBuilder
				'				Dim oUpdate As cSQLUpdateStringBuilder
				'				For Each record In records
				'					oWhere = New CSQLWhereStringBuilder
				'					oWhere.AppendAndCondition("LenderConfigLogID={0}", New CSQLParamValue(record.Key))
				'					oUpdate = New cSQLUpdateStringBuilder("LenderConfigLogs", oWhere)
				'					oUpdate.appendvalue("ConfigData", New CSQLParamValue(CStringCompressor.Compress(Common.EncryptAllPasswordInConfigXml(record.Value).OuterXml)))
				'					log.Debug(oUpdate.SQL)
				'					oDB.executeNonQuery(oUpdate.SQL, False)
				'				Next
				'				'oDB.commitTransactionIfOpen()
				'			Catch ex As Exception
				'				log.Error("Error while compressLenderConfigLogs", ex)
				'				'oDB.rollbackTransactionIfOpen()
				'			Finally
				'				oDB.closeConnection()
				'			End Try

				'		End If
				'		res = New CJsonResponse(True, "", New With {.count = CountUnCompressedLenderConfigLogsRecords()})
				'	Catch ex As Exception
				'		log.Error("Error while compressLenderConfigLogs", ex)
				'		res = New CJsonResponse(False, "Could not run batch. Please try again", True)
				'	End Try
				'	Response.Write(JsonConvert.SerializeObject(res))
				'	Response.End()
				'	Return


				'	'Dim res As CJsonResponse
				'	'Try
				'	'	compressLenderConfigLogs()
				'	'	res = New CJsonResponse(True, "", "OK")
				'	'Catch ex As Exception
				'	'	log.Error("Could not run batch to encrypt all passwords in config xml.", ex)
				'	'	res = New CJsonResponse(False, "Could not run batch. Please try again", True)
				'	'End Try
				'	'Response.Write(JsonConvert.SerializeObject(res))
				'	'Response.End()
				'	'Return
			ElseIf cmd = "runCalculateAuditChanges" Then
				Dim res As CJsonResponse
				Try
					Dim records As List(Of CLenderConfigLog)
					records = GetLatestUnCalculatedAuditLogs(NumberOfLogItemWillBeProcessed)

					Dim groupedRecords As New Dictionary(Of Guid, List(Of CLenderConfigLog))()
					For Each configLog In records
						If Not groupedRecords.ContainsKey(configLog.LenderConfigID) Then
							groupedRecords.Add(configLog.LenderConfigID, New List(Of CLenderConfigLog))
						End If
						groupedRecords(configLog.LenderConfigID).Add(configLog)
					Next

					log.Debug("Begin calculating audit changes")
					If records IsNot Nothing AndAlso records.Any() Then
						Dim oDB As New CSQLDBUtils(LPQMOBILE_CONNECTIONSTRING)
						Try
							Dim oWhere As CSQLWhereStringBuilder
							Dim oUpdate As cSQLUpdateStringBuilder
							For Each configLog In records
								Try
									oWhere = New CSQLWhereStringBuilder

									' Get the previous, either from what we already have or from the DB
									Dim sDiff = ""
									Dim previousConfigLog = GetPreviousLenderConfigLog(configLog, groupedRecords)

									If previousConfigLog IsNot Nothing Then
										' Calculate the difference
										Dim sbDiff As New StringBuilder()
										Dim diffGramStr As String = ""
										Dim xmlComparer As New CXmlComparer(previousConfigLog.ConfigData, configLog.ConfigData)
										If xmlComparer.Compare(diffGramStr) = False Then
											xmlComparer.ParseDiffGram(diffGramStr, sbDiff)
										End If
										sDiff = sbDiff.ToString()
									End If
									' Else, write an empty string

									If String.IsNullOrWhiteSpace(sDiff) Then sDiff = "No changes"

									' Write to DB
									oWhere.AppendAndCondition("LenderConfigLogID={0}", New CSQLParamValue(configLog.LenderConfigLogID))

									oUpdate = New cSQLUpdateStringBuilder("LenderConfigLogs", oWhere)
									oUpdate.appendvalue("CachedDiff", New CSQLParamValue(sDiff))

									log.Debug(oUpdate.SQL)
									oDB.executeNonQuery(oUpdate.SQL, False)
								Catch ex As Exception
									log.Error("Error while runCalculateAuditChanges , partial fail", ex)
								End Try
							Next
							'oDB.commitTransactionIfOpen()
						Catch ex As Exception
							log.Error("Error while runCalculateAuditChanges", ex)
							'oDB.rollbackTransactionIfOpen()
						Finally
							oDB.closeConnection()
						End Try

					End If
					res = New CJsonResponse(True, "", New With {.count = CountUnDiffedAuditLogsRecords()})
				Catch ex As Exception
					log.Error("Could not run runCalculateAuditChanges.", ex)
					res = New CJsonResponse(False, "Could not run batch. Please try again", True)
				End Try
				log.Debug("End calculating audit changes")
				Response.Write(JsonConvert.SerializeObject(res))
				Response.End()
				Return
			End If

			UnEncryptedPasswordRecordCounter = CountUnEncryptedPasswordsLenderConfigLogsRecords()
			'UnCompressedRecordCounter = CountUnCompressedLenderConfigLogsRecords()
			UnDiffedAuditLogsCounter = CountUnDiffedAuditLogsRecords()

			' these already been run so don't allow to execute again 
			'If cmd = "runmigratebatchallportals" Then
			'	Dim res As CJsonResponse
			'	Try
			'		Dim lenderList As Dictionary(Of Guid, XmlDocument)

			'		'live migration fix
			'		'101'
			'		lenderList = GetCustomLenderConfigDatas()

			'		'local migration testing
			'		'16
			'		'lenderList = GetAllLenderConfigDatas()
			'		If lenderList IsNot Nothing AndAlso lenderList.Any() Then
			'			Dim i As Integer = 0
			'			For Each item As KeyValuePair(Of Guid, XmlDocument) In lenderList
			'				RunFixMigrationBatch(item.Key, item.Value)
			'				log.Debug("Debug migrate config:" & item.Key.ToString() & " " & item.Value.InnerXml)
			'				i += 1
			'			Next
			'			log.Debug("total migration run: " & i.ToString)
			'		End If
			'		res = New CJsonResponse(True, "", "OK")
			'	Catch ex As Exception
			'		log.Error("Could not run batch to migrate the show field configuration data.", ex)
			'		res = New CJsonResponse(False, "Could not run batch. Please try again", True)
			'	End Try
			'	Response.Write(JsonConvert.SerializeObject(res))
			'	Response.End()
			'	Return
			'ElseIf cmd = "runmigratebatchallportals_old" Then
			'	Dim res As CJsonResponse
			'	Try

			'		Dim lenderList As Dictionary(Of Guid, XmlDocument)
			'		lenderList = GetAllLenderConfigDatas()
			'		If lenderList IsNot Nothing AndAlso lenderList.Any() Then
			'			For Each item As KeyValuePair(Of Guid, XmlDocument) In lenderList
			'				RunBatch(item.Key, item.Value)
			'				log.Debug("Debug migrate config:" & item.Key.ToString() & " " & item.Value.InnerXml)
			'			Next
			'		End If
			'		res = New CJsonResponse(True, "", "OK")
			'	Catch ex As Exception
			'		log.Error("Could not run batch to migrate the show field configuration data.", ex)
			'		res = New CJsonResponse(False, "Could not run batch. Please try again", True)
			'	End Try
			'	Response.Write(JsonConvert.SerializeObject(res))
			'	Response.End()
			'	Return
			'ElseIf cmd = "runmigrateHeLocbatchallportals" Then
			'	Dim res As CJsonResponse
			'	Try

			'		Dim lenderList As Dictionary(Of Guid, XmlDocument)
			'		lenderList = GetAllLenderConfigDatas()
			'		If lenderList IsNot Nothing AndAlso lenderList.Any() Then
			'			For Each item As KeyValuePair(Of Guid, XmlDocument) In lenderList
			'				RunBatchHeLoc(item.Key, item.Value)
			'				log.Debug("Debug migrate config for HE LOC:" & item.Key.ToString() & " " & item.Value.InnerXml)
			'			Next
			'		End If
			'		res = New CJsonResponse(True, "", "OK")
			'	Catch ex As Exception
			'		log.Error("Could not run batch to migrate the show field configuration data.", ex)
			'		res = New CJsonResponse(False, "Could not run batch. Please try again", True)
			'	End Try
			'	Response.Write(JsonConvert.SerializeObject(res))
			'	Response.End()
			'	Return
			'ElseIf cmd = "runmigrateLenderConfigUsers" Then
			'	Dim res As CJsonResponse
			'	Try
			'		Dim result As String = RunbatchMigrateLenderConfigUsersToUserRoles()
			'		If result = "ok" Then
			'			res = New CJsonResponse(True, "", "OK")
			'		Else
			'			res = New CJsonResponse(False, "Error", "FAILED")
			'		End If

			'	Catch ex As Exception
			'		log.Error("Could not run batch to migrate LenderConfigUsers to UserRoles table.", ex)
			'		res = New CJsonResponse(False, "Could not run batch. Please try again", True)
			'	End Try
			'	Response.Write(JsonConvert.SerializeObject(res))
			'	Response.End()
			'	Return
			'End If
		End If
	End Sub

	'Private Sub compressLenderConfigLogs()
	'	Dim sSQL As String = "select LenderConfigLogID, ConfigData from LenderConfigLogs"
	'	Dim oDB As New CSQLDBUtils(System.Web.Configuration.WebConfigurationManager.ConnectionStrings("LPQMOBILE_CONNECTIONSTRING").ConnectionString)
	'	Try
	'		Dim oData = oDB.getDataTable(sSQL)
	'		Dim nCount As Integer = 0
	'		If oData IsNot Nothing AndAlso oData.Rows.Count > 0 Then
	'			Dim oWhere As CSQLWhereStringBuilder
	'			For Each row As DataRow In oData.Rows
	'				Dim id As String = Common.SafeString(row("LenderConfigLogID"))
	'				Dim configStr As String = CStringCompressor.Decompress(Common.SafeString(row("ConfigData")))
	'				'configStr =
	'				oWhere = New CSQLWhereStringBuilder()
	'				oWhere.AppendAndCondition("LenderConfigLogID={0}", New CSQLParamValue(id))
	'				Dim oUpdate As New cSQLUpdateStringBuilder("LenderConfigLogs", oWhere)

	'				oUpdate.appendvalue("ConfigData", New CSQLParamValue(CStringCompressor.Compress(configStr)))
	'				oDB.executeNonQuery(oUpdate.SQL, False)

	'				If nCount Mod 20 = 0 Then
	'					log.Debug("Compressed for: " & nCount.ToString)
	'				End If
	'			Next
	'			'oDB.commitTransactionIfOpen()
	'		End If
	'	Catch ex As Exception
	'		log.Error("Error while compressLenderConfigLogs", ex)
	'		'oDB.rollbackTransactionIfOpen()
	'	Finally
	'		oDB.closeConnection()
	'	End Try
	'End Sub

	Private Function GetLatestUnEncryptedLogRecord(numberOfRecords As Integer) As Dictionary(Of String, Tuple(Of String, String))
		Dim sSQL As String = String.Format("select top {0} LenderConfigLogID, LPQPW, ConfigData from LenderConfigLogs  where LPQPW not like 'Encrypted%' order by LogTime desc", numberOfRecords)
		Dim oDB As New CSQLDBUtils(System.Web.Configuration.WebConfigurationManager.ConnectionStrings("LPQMOBILE_CONNECTIONSTRING").ConnectionString)
		Dim result As New Dictionary(Of String, Tuple(Of String, String))
		Try
			Dim oData = oDB.getDataTable(sSQL)
			If oData IsNot Nothing AndAlso oData.Rows.Count > 0 Then
				For Each row As DataRow In oData.Rows
					result.Add(Common.SafeString(row("LenderConfigLogID")), New Tuple(Of String, String)(Common.SafeString(row("LPQPW")), CStringCompressor.Decompress(Common.SafeString(row("ConfigData")))))
				Next
			End If
		Catch ex As Exception
			log.Error("Error while GetLatestUnEncryptedLogRecord", ex)
		Finally
			oDB.closeConnection()
		End Try
		Return result
	End Function
	Private Function CountUnEncryptedPasswordsLenderConfigLogsRecords() As Integer
		Dim result As Integer = 0
		Dim sSQL As String = "select count(*) from LenderConfigLogs where LPQPW not like 'Encrypted%'"
		Dim oDB As New CSQLDBUtils(System.Web.Configuration.WebConfigurationManager.ConnectionStrings("LPQMOBILE_CONNECTIONSTRING").ConnectionString)
		Try
			Dim queryResult As String = oDB.getScalerValue(sSQL)
			If Not String.IsNullOrWhiteSpace(queryResult) Then
				result = CInt(queryResult)
			End If
		Catch ex As Exception
			log.Error("Error while CountUnEncryptedPasswordsLenderConfigLogsRecords", ex)
		Finally
			oDB.closeConnection()
		End Try
		Return result
	End Function

	Private Function GetLatestUnCompressedLogRecord(numberOfRecords As Integer, Optional reCompressAll As Boolean = False) As Dictionary(Of String, String)
		Dim sSQL As String = String.Format("select top {0} LenderConfigLogID, ConfigData from LenderConfigLogs {1} order by LogTime desc", numberOfRecords, IIf(reCompressAll, "", "where ConfigData not like '**COMPRESSED**%'"))
		Dim oDB As New CSQLDBUtils(System.Web.Configuration.WebConfigurationManager.ConnectionStrings("LPQMOBILE_CONNECTIONSTRING").ConnectionString)
		Dim result As New Dictionary(Of String, String)
		Try
			Dim oData = oDB.getDataTable(sSQL)
			If oData IsNot Nothing AndAlso oData.Rows.Count > 0 Then
				For Each row As DataRow In oData.Rows
					result.Add(Common.SafeString(row("LenderConfigLogID")), CStringCompressor.Decompress(Common.SafeString(row("ConfigData"))))
				Next
			End If
		Catch ex As Exception
			log.Error("Error while GetLatestUnCompressedLogRecord", ex)
		Finally
			oDB.closeConnection()
		End Try
		Return result
	End Function

	Private Function CountUnCompressedLenderConfigLogsRecords() As Integer
		Dim result As Integer = 0
		Dim sSQL As String = "select count(*) from LenderConfigLogs where ConfigData not like '**COMPRESSED**%'"
		Dim oDB As New CSQLDBUtils(System.Web.Configuration.WebConfigurationManager.ConnectionStrings("LPQMOBILE_CONNECTIONSTRING").ConnectionString)
		Try
			Dim queryResult As String = oDB.getScalerValue(sSQL)
			If Not String.IsNullOrWhiteSpace(queryResult) Then
				result = CInt(queryResult)
			End If
		Catch ex As Exception
			log.Error("Error while CountUnCompressedLenderConfigLogsRecords", ex)
		Finally
			oDB.closeConnection()
		End Try
		Return result
	End Function

#Region "Calculate Audit Logs"
	Private Function CountUnDiffedAuditLogsRecords() As Integer
		Dim result As Integer = 0
		Dim sSQL As String = "SELECT COUNT(*) FROM LenderConfigLogs"
		Dim oWhere As CSQLWhereStringBuilder = GetAuditLogChangesWhereStatement()

		Dim oDB As New CSQLDBUtils(LPQMOBILE_CONNECTIONSTRING)
		Try
			result = Common.SafeInteger(oDB.getScalerValue(sSQL & oWhere.SQL))
		Catch ex As Exception
			log.Error("Error while running CountUnDiffedAuditLogsRecords", ex)
		Finally
			oDB.closeConnection()
		End Try
		Return result
	End Function

	Private Function GetAuditLogChangesWhereStatement() As CSQLWhereStringBuilder
		Dim oWhere As New CSQLWhereStringBuilder()
		oWhere.AppendANDCondition("CachedDiff IS NULL")
		oWhere.AppendAndCondition("LogTime >= {0}", New CSQLParamValue(Date.Today.AddYears(-1)))
		Return oWhere
	End Function

	''' <summary>
	''' Returns a dictionary of LenderConfigID -> List of lender configs for that LenderConfigID.
	''' </summary>
	''' <param name="numberOfRecords"></param>
	''' <returns></returns>
	Private Function GetLatestUnCalculatedAuditLogs(numberOfRecords As Integer) As List(Of CLenderConfigLog)
		Dim sSQL As String = String.Format("SELECT TOP {0} *  FROM LenderConfigLogs", numberOfRecords)
		Dim oWhere As CSQLWhereStringBuilder = GetAuditLogChangesWhereStatement()

		Dim oDB As New CSQLDBUtils(LPQMOBILE_CONNECTIONSTRING)
		Dim result As New List(Of CLenderConfigLog)
		Try
			Dim oData = oDB.getDataTable(sSQL & oWhere.SQL & " ORDER BY Logtime DESC;")
			If oData IsNot Nothing AndAlso oData.Rows.Count > 0 Then
				For Each row As DataRow In oData.Rows
					Dim configLig = GetLenderConfigLog(row)
					result.Add(configLig)
				Next
			End If
		Catch ex As Exception
			log.Error("Error while GetLatestUnCalculatedAuditLogs", ex)
		Finally
			oDB.closeConnection()
		End Try
		Return result
	End Function

	Private Function GetPreviousLenderConfigLog(targetConfigLog As CLenderConfigLog, collectedLogs As Dictionary(Of Guid, List(Of CLenderConfigLog))) As CLenderConfigLog
		' Try getting it from the collection we pulled earlier
		Dim previousLog As CLenderConfigLog = collectedLogs(targetConfigLog.LenderConfigID).FirstOrDefault(Function(configLog) configLog.RevisionID = targetConfigLog.RevisionID - 1)
		If previousLog Is Nothing Then
			' Get it from the database
			Dim sSQL As String = "SELECT TOP 1 * FROM LenderConfigLogs "
			Dim oWhere As New CSQLWhereStringBuilder()
			' It may be possible one with RevisionID = target.RevisionID - 1 doesn't exist, so get the first with a RevisionID
			' lower than the target, with the highest RevisionID.
			oWhere.AppendAndCondition("LenderConfigID = {0}", New CSQLParamValue(targetConfigLog.LenderConfigID))
			oWhere.AppendAndCondition("RevisionID < {0}", New CSQLParamValue(targetConfigLog.RevisionID))

			Dim oDB As New CSQLDBUtils(LPQMOBILE_CONNECTIONSTRING)
			Dim result As New Dictionary(Of Guid, List(Of CLenderConfigLog))
			Try
				Dim oData = oDB.getDataTable(sSQL & oWhere.SQL & " ORDER BY RevisionID DESC;")
				If oData IsNot Nothing AndAlso oData.Rows.Count > 0 Then
					previousLog = GetLenderConfigLog(oData.Rows(0))
				End If
			Catch ex As Exception
				log.Error("Error while GetLatestUnCalculatedAuditLogs", ex)
			Finally
				oDB.closeConnection()
			End Try
		End If

		Return previousLog

	End Function

	Private Function GetLenderConfigLog(row As DataRow) As CLenderConfigLog
		Dim logItem As New CLenderConfigLog
		logItem.AuditChanges = ""
		If Not IsDBNull(row("ConfigData")) Then
			logItem.ConfigData = CStringCompressor.Decompress(row("ConfigData").ToString())
			Dim configXml As New XmlDocument()
			configXml.LoadXml(logItem.ConfigData)
			configXml = Common.EncryptAllPasswordInConfigXml(configXml)
			logItem.ConfigData = configXml.OuterXml
		End If
		If Not IsDBNull(row("LenderConfigID")) Then
			logItem.LenderConfigID = Common.SafeGUID(row("LenderConfigID"))
		End If
		If Not IsDBNull(row("LenderConfigLogID")) Then
			logItem.LenderConfigLogID = Common.SafeGUID(row("LenderConfigLogID"))
		End If
		If Not IsDBNull(row("LenderRef")) Then
			logItem.LenderRef = row("LenderRef").ToString()
		End If
		If Not IsDBNull(row("LogTime")) Then
			logItem.LogTime = Common.SafeDate(row("LogTime"))
		End If
		If Not IsDBNull(row("RevisionID")) Then
			logItem.RevisionID = Common.SafeInteger(row("RevisionID"))
		End If
		If Not IsDBNull(row("CachedDiff")) Then
			logItem.AuditChanges = row("CachedDiff").ToString()
		End If
		Return logItem
	End Function
#End Region 'Calculate Audit Logs

	Private Function GetAllLenderConfigDatas() As Dictionary(Of Guid, XmlDocument)
		Dim result As New Dictionary(Of Guid, XmlDocument)
		Dim oData As New DataTable()
		Dim sSQL As String = "select LenderConfigID, ConfigData, lenderRef from LenderConfigs"
		Dim oDB As New CSQLDBUtils(System.Web.Configuration.WebConfigurationManager.ConnectionStrings("LPQMOBILE_CONNECTIONSTRING").ConnectionString)
		Try
			oData = oDB.getDataTable(sSQL)
			If oData IsNot Nothing AndAlso oData.Rows.Count > 0 Then
				For Each row As DataRow In oData.Rows
					Dim lenderConfigId As Guid = Common.SafeGUID(row("LenderConfigID"))
					Dim lenderConfigData As String = Common.SafeString(row("ConfigData"))
					Dim lenderref As String = Common.SafeString(row("LenderRef"))

					''skip these lender bc they are still on legacy system can't use new schema
					'If lenderref <> "" AndAlso (lenderref = "cloneref" Or lenderref = "testbo") Then
					If lenderref <> "" AndAlso (lenderref = "cacu120415" Or lenderref = "Wffcu91316") Then
						'log.Warn("Skip migration for lenderref: " & lenderref)
						Continue For
					End If

					If lenderref.StartsWith("zzz") Then
						'log.Warn("Skip migration for lenderref: " & lenderref)
						Continue For
					End If

					'Icb051717 chcek existing cuctomlist
					'If lenderref <> "Wffcu91316" Then
					'	Continue For
					'End If

					If lenderConfigId <> Guid.Empty AndAlso Not String.IsNullOrEmpty(lenderConfigData) Then
						Try
							Dim lenderConfigXml As New XmlDocument
							lenderConfigXml.LoadXml(lenderConfigData)
							result.Add(lenderConfigId, lenderConfigXml)
						Catch ex As Exception
							log.Error(String.Format("Error while loading ConfigData for lenderref {0}", lenderref), ex)
						End Try
					End If
				Next
			End If
		Catch ex As Exception
			log.Error("Error while GetAllLenderConfigDatas", ex)
		Finally
			oDB.closeConnection()
		End Try
		Return result
	End Function

	''' <summary>
	''' 136 portal from legacy that has occupancy_status="Y"
	''' </summary>
	''' <returns></returns>
	''' <remarks></remarks>
	Private Function GetBkLenderConfigDatas() As Dictionary(Of Guid, XmlDocument)
		Dim result As New Dictionary(Of Guid, XmlDocument)
		Dim oData As New DataTable()
		Dim sSQL As String = "SELECT  LenderConfigID, ConfigData, lenderRef FROM [LenderConfigs] where [ConfigData] like '%occupancy_status=""Y""%' and ([ConfigData] not like '%occupancy_secondary%' or [ConfigData] like '%occupancy_secondary=""N""%')"
		Dim oDB As New CSQLDBUtils(System.Web.Configuration.WebConfigurationManager.ConnectionStrings("LPQMOBILE_CONNECTIONSTRING_bk").ConnectionString)
		Try
			oData = oDB.getDataTable(sSQL)
			If oData IsNot Nothing AndAlso oData.Rows.Count > 0 Then
				For Each row As DataRow In oData.Rows
					Dim lenderConfigId As Guid = Common.SafeGUID(row("LenderConfigID"))
					Dim lenderConfigData As String = Common.SafeString(row("ConfigData"))
					Dim lenderref As String = Common.SafeString(row("LenderRef"))

					''skip these lender bc they are still on legacy system can't use new schema
					'If lenderref <> "" AndAlso (lenderref = "cloneref" Or lenderref = "testbo") Then
					If lenderref <> "" AndAlso (lenderref = "cacu120415" Or lenderref = "Wffcu91316") Then
						'log.Warn("Skip migration for lenderref: " & lenderref)
						Continue For
					End If

					'Icb051717 chcek existing cuctomlist
					'If lenderref <> "Wffcu91316" Then
					'	Continue For
					'End If

					If lenderConfigId <> Guid.Empty AndAlso Not String.IsNullOrEmpty(lenderConfigData) Then
						Dim lenderConfigXml As New XmlDocument
						lenderConfigXml.LoadXml(lenderConfigData)
						result.Add(lenderConfigId, lenderConfigXml)
					End If
				Next
			End If
		Catch ex As Exception
			log.Error("Error while GetAllLenderConfigDatas", ex)
		Finally
			oDB.closeConnection()
		End Try
		Return result
	End Function

	''' <summary>
	''' return list of specific lenders, 201
	''' </summary>
	''' <returns></returns>
	''' <remarks></remarks>
	Private Function GetCustomLenderConfigDatas() As Dictionary(Of Guid, XmlDocument)
		Dim resultBK As Dictionary(Of Guid, XmlDocument) = GetBkLenderConfigDatas()
		log.Debug("number of bk portal with occupany=y: " & resultBK.Count)

		Dim result As New Dictionary(Of Guid, XmlDocument)
		Dim oData As New DataTable()
		Dim sSQL As String = "SELECT  LenderConfigID, ConfigData, lenderRef FROM [LenderConfigs] where [ConfigData] like '%XA"" controller_id=""divOccupancyStatusSection"" controller_name=""Occupancy Status"" is_visible=""N%' "
		Dim oDB As New CSQLDBUtils(System.Web.Configuration.WebConfigurationManager.ConnectionStrings("LPQMOBILE_CONNECTIONSTRING").ConnectionString)
		Try
			oData = oDB.getDataTable(sSQL)
			If oData IsNot Nothing AndAlso oData.Rows.Count > 0 Then
				log.Debug("number of live portal with occupany=n: " & oData.Rows.Count)
				For Each row As DataRow In oData.Rows
					Dim lenderConfigId As Guid = Common.SafeGUID(row("LenderConfigID"))
					Dim lenderConfigData As String = Common.SafeString(row("ConfigData"))
					Dim lenderref As String = Common.SafeString(row("LenderRef"))

					If Not resultBK.ContainsKey(lenderConfigId) Then Continue For

					''skip these lender bc they are still on legacy system can't use new schema
					'If lenderref <> "" AndAlso (lenderref = "cloneref" Or lenderref = "testbo") Then
					If lenderref <> "" AndAlso (lenderref = "cacu120415" Or lenderref = "Wffcu91316") Then
						'log.Warn("Skip migration for lenderref: " & lenderref)
						Continue For
					End If

					'Icb051717 chcek existing cuctomlist
					'If lenderref <> "Wffcu91316" Then
					'	Continue For
					'End If

					If lenderConfigId <> Guid.Empty AndAlso Not String.IsNullOrEmpty(lenderConfigData) Then
						Dim lenderConfigXml As New XmlDocument
						lenderConfigXml.LoadXml(lenderConfigData)
						result.Add(lenderConfigId, lenderConfigXml)
					End If
				Next
			End If
		Catch ex As Exception
			log.Error("Error while GetAllLenderConfigDatas", ex)
		Finally
			oDB.closeConnection()
		End Try
		log.Debug("number of portal that need to be changed: " & result.Count)
		Return result
	End Function



	Private Sub RunBatch(lenderConfigId As Guid, lenderConfigXmlDoc As XmlDocument)
		Dim smBL As New SmBL
		Dim XaBranchSelectionDropdown As String = ""
		Dim XaMotherMaidenName As String = ""
		Dim XaGender As String = ""
		Dim XaGenderSecondary As String = ""
		Dim Citizenship As String = ""
		Dim XaCitizenship As String = ""
		Dim XaEmployment As String = ""
		Dim XaMaritalStatus As String = ""
		Dim XaMotherMaidenNameSecondary As String = ""
		Dim XaCitizenshipSecondary As String = ""
		Dim XaEmploymentSecondary As String = ""
		Dim XaGrossIncomeSecondary As String = ""
		Dim XaIDPageSecondary As String = ""
		Dim XaOccupancySecondary As String = ""
		Dim XaOccupancy As String = ""
		Dim XaDisclosuresSecondaryEnable As String = ""
		Dim XaEmploymentMinor As String = ""
		Dim XaEnableMemberNumber As String = ""
		Dim LoanBranchSelectionDropdown As String = ""
		Dim CCIdentificationSection As String = ""
		Dim HEIdentificationSection As String = ""
		'Dim HELOCIdentificationSection As String = ""
		Dim PLIdentificationSection As String = ""
		Dim VLIdentificationSection As String = ""

		'Dim SAShowFieldDisclosure As XmlNode
		'SAShowFieldDisclosure = lenderConfigXmlDoc.SelectSingleNode("WEBSITE/CUSTOM_LIST/SHOW_FIELDS/ITEM[@controller_id='divDisclosureSection']")


		Dim xaBranchRequired As String = GetData(lenderConfigXmlDoc, "WEBSITE/XA_LOAN/BEHAVIOR/@branch_selection_dropdown_required")
		If xaBranchRequired = "Y" Then
			XaBranchSelectionDropdown = "Y"
		Else
			XaBranchSelectionDropdown = GetData(lenderConfigXmlDoc, "WEBSITE/XA_LOAN/VISIBLE/@branch_selection_dropdown")
		End If

		XaMotherMaidenName = GetData(lenderConfigXmlDoc, "WEBSITE/XA_LOAN/VISIBLE/@mother_maiden_name")
		XaGender = GetData(lenderConfigXmlDoc, "WEBSITE/XA_LOAN/VISIBLE/@gender")
		XaGenderSecondary = GetData(lenderConfigXmlDoc, "WEBSITE/XA_LOAN/VISIBLE/@gender_secondary")
		Citizenship = GetData(lenderConfigXmlDoc, "WEBSITE/VISIBLE/@citizenship")
		XaCitizenship = GetData(lenderConfigXmlDoc, "WEBSITE/XA_LOAN/VISIBLE/@citizenship")
		XaEmployment = GetData(lenderConfigXmlDoc, "WEBSITE/XA_LOAN/VISIBLE/@employment")

		XaMotherMaidenNameSecondary = GetData(lenderConfigXmlDoc, "WEBSITE/XA_LOAN/VISIBLE/@mother_maiden_name_secondary")
		XaCitizenshipSecondary = GetData(lenderConfigXmlDoc, "WEBSITE/XA_LOAN/VISIBLE/@citizenship_secondary")
		XaEmploymentSecondary = GetData(lenderConfigXmlDoc, "WEBSITE/XA_LOAN/VISIBLE/@employment_secondary")
		XaGrossIncomeSecondary = GetData(lenderConfigXmlDoc, "WEBSITE/XA_LOAN/VISIBLE/@gross_income_secondary")
		XaIDPageSecondary = GetData(lenderConfigXmlDoc, "WEBSITE/XA_LOAN/VISIBLE/@id_page_secondary")
		XaOccupancySecondary = GetData(lenderConfigXmlDoc, "WEBSITE/XA_LOAN/VISIBLE/@occupancy_secondary")
		XaDisclosuresSecondaryEnable = GetData(lenderConfigXmlDoc, "WEBSITE/XA_LOAN/VISIBLE/@disclosures_secondary_enable")

		XaEmploymentMinor = GetData(lenderConfigXmlDoc, "WEBSITE/XA_LOAN/VISIBLE/@minor_employment")

		XaMaritalStatus = GetData(lenderConfigXmlDoc, "WEBSITE/XA_LOAN/VISIBLE/@marital_status")

		XaEnableMemberNumber = GetData(lenderConfigXmlDoc, "WEBSITE/XA_LOAN/VISIBLE/@mem_number_secondary")

		XaOccupancy = GetData(lenderConfigXmlDoc, "WEBSITE/XA_LOAN/VISIBLE/@occupancy_status")

		Dim loanBranchRequired As String = GetData(lenderConfigXmlDoc, "WEBSITE/BEHAVIOR/@branch_selection_dropdown_required")
		If loanBranchRequired = "Y" Then
			LoanBranchSelectionDropdown = "Y"
		Else
			LoanBranchSelectionDropdown = GetData(lenderConfigXmlDoc, "WEBSITE/VISIBLE/@branch_selection_dropdown")
		End If

		CCIdentificationSection = GetData(lenderConfigXmlDoc, "WEBSITE/CREDIT_CARD_LOAN/VISIBLE/@identification_section")
		HEIdentificationSection = GetData(lenderConfigXmlDoc, "WEBSITE/HOME_EQUITY_LOAN/VISIBLE/@identification_section")
		'HELOCIdentificationSection = GetData(lenderConfigXmlDoc, "WEBSITE/HOME_EQUITY_LOC/VISIBLE/@identification_section")
		PLIdentificationSection = GetData(lenderConfigXmlDoc, "WEBSITE/PERSONAL_LOAN/VISIBLE/@identification_section")
		VLIdentificationSection = GetData(lenderConfigXmlDoc, "WEBSITE/VEHICLE_LOAN/VISIBLE/@identification_section")


		'TODO: should do this only for non default state so it doesn't take up space
		'----
		Dim showFieldList As New List(Of SmShowFieldItemModel)
		If Not String.IsNullOrEmpty(XaBranchSelectionDropdown) AndAlso XaBranchSelectionDropdown = "Y" Then
			showFieldList.Add(New SmShowFieldItemModel() With {.ControllerId = "divBranchSection", .ControllerName = "Branch", .LoanType = "XA", .IsVisible = IIf(XaBranchSelectionDropdown = "Y", "Y", "N").ToString()})
			showFieldList.Add(New SmShowFieldItemModel() With {.ControllerId = "divBranchSection", .ControllerName = "Branch", .LoanType = "SA", .IsVisible = IIf(XaBranchSelectionDropdown = "Y", "Y", "N").ToString()})
			showFieldList.Add(New SmShowFieldItemModel() With {.ControllerId = "divBranchSection", .ControllerName = "Branch", .LoanType = "XA_SPECIAL", .IsVisible = IIf(XaBranchSelectionDropdown = "Y", "Y", "N").ToString()})
			''sa_special
			showFieldList.Add(New SmShowFieldItemModel() With {.ControllerId = "divBranchSection", .ControllerName = "Branch", .LoanType = "SA_SPECIAL", .IsVisible = IIf(XaBranchSelectionDropdown = "Y", "Y", "N").ToString()})
		End If

		If Not String.IsNullOrEmpty(XaMotherMaidenName) AndAlso XaMotherMaidenName = "N" Then
			showFieldList.Add(New SmShowFieldItemModel() With {.ControllerId = "divMotherMaidenName", .ControllerName = "Mother Maiden Name", .LoanType = "XA", .IsVisible = IIf(XaMotherMaidenName = "Y", "Y", "N").ToString()})
			showFieldList.Add(New SmShowFieldItemModel() With {.ControllerId = "co_divMotherMaidenName", .ControllerName = "Co-App Mother Maiden Name", .LoanType = "XA", .IsVisible = IIf(XaMotherMaidenName = "Y", "Y", "N").ToString()})
			showFieldList.Add(New SmShowFieldItemModel() With {.ControllerId = "divMotherMaidenName", .ControllerName = "Mother Maiden Name", .LoanType = "XA_SPECIAL", .IsVisible = IIf(XaMotherMaidenName = "Y", "Y", "N").ToString()})
			showFieldList.Add(New SmShowFieldItemModel() With {.ControllerId = "co_divMotherMaidenName", .ControllerName = "Co-App Mother Maiden Name", .LoanType = "XA_SPECIAL", .IsVisible = IIf(XaMotherMaidenName = "Y", "Y", "N").ToString()})
			showFieldList.Add(New SmShowFieldItemModel() With {.ControllerId = "m_divMotherMaidenName", .ControllerName = "Minor Mother Maiden Name", .LoanType = "XA_SPECIAL", .IsVisible = IIf(XaMotherMaidenName = "Y", "Y", "N").ToString()})
			''sa_special
			showFieldList.Add(New SmShowFieldItemModel() With {.ControllerId = "divMotherMaidenName", .ControllerName = "Mother Maiden Name", .LoanType = "SA_SPECIAL", .IsVisible = IIf(XaMotherMaidenName = "Y", "Y", "N").ToString()})
			showFieldList.Add(New SmShowFieldItemModel() With {.ControllerId = "co_divMotherMaidenName", .ControllerName = "Co-App Mother Maiden Name", .LoanType = "SA_SPECIAL", .IsVisible = IIf(XaMotherMaidenName = "Y", "Y", "N").ToString()})
			showFieldList.Add(New SmShowFieldItemModel() With {.ControllerId = "m_divMotherMaidenName", .ControllerName = "Minor Mother Maiden Name", .LoanType = "SA_SPECIAL", .IsVisible = IIf(XaMotherMaidenName = "Y", "Y", "N").ToString()})

		End If

		If Not String.IsNullOrEmpty(XaGender) AndAlso XaGender = "N" Then
			showFieldList.Add(New SmShowFieldItemModel() With {.ControllerId = "divGender", .ControllerName = "Gender", .LoanType = "XA", .IsVisible = IIf(XaGender = "Y", "Y", "N").ToString()})
			showFieldList.Add(New SmShowFieldItemModel() With {.ControllerId = "co_divGender", .ControllerName = "Co-App Gender", .LoanType = "XA", .IsVisible = IIf(XaGender = "Y", "Y", "N").ToString()})
			showFieldList.Add(New SmShowFieldItemModel() With {.ControllerId = "divGender", .ControllerName = "Gender", .LoanType = "XA_SPECIAL", .IsVisible = IIf(XaGender = "Y", "Y", "N").ToString()})
			showFieldList.Add(New SmShowFieldItemModel() With {.ControllerId = "co_divGender", .ControllerName = "Co-App Gender", .LoanType = "XA_SPECIAL", .IsVisible = IIf(XaGender = "Y", "Y", "N").ToString()})
			showFieldList.Add(New SmShowFieldItemModel() With {.ControllerId = "m_divGender", .ControllerName = "Minor Gender", .LoanType = "XA_SPECIAL", .IsVisible = IIf(XaGender = "Y", "Y", "N").ToString()})
			''sa_special
			showFieldList.Add(New SmShowFieldItemModel() With {.ControllerId = "divGender", .ControllerName = "Gender", .LoanType = "SA_SPECIAL", .IsVisible = IIf(XaGender = "Y", "Y", "N").ToString()})
			showFieldList.Add(New SmShowFieldItemModel() With {.ControllerId = "co_divGender", .ControllerName = "Co-App Gender", .LoanType = "SA_SPECIAL", .IsVisible = IIf(XaGender = "Y", "Y", "N").ToString()})
			showFieldList.Add(New SmShowFieldItemModel() With {.ControllerId = "m_divGender", .ControllerName = "Minor Gender", .LoanType = "SA_SPECIAL", .IsVisible = IIf(XaGender = "Y", "Y", "N").ToString()})
		End If
		If Not String.IsNullOrEmpty(XaGenderSecondary) AndAlso XaGenderSecondary = "N" Then
			showFieldList.Add(New SmShowFieldItemModel() With {.ControllerId = "divGender", .ControllerName = "Gender", .LoanType = "SA", .IsVisible = IIf(XaGenderSecondary = "Y", "Y", "N").ToString()})
			showFieldList.Add(New SmShowFieldItemModel() With {.ControllerId = "co_divGender", .ControllerName = "Co-App Gender", .LoanType = "SA", .IsVisible = IIf(XaGenderSecondary = "Y", "Y", "N").ToString()})
		End If

		If Not String.IsNullOrEmpty(XaEnableMemberNumber) Then
			showFieldList.Add(New SmShowFieldItemModel() With {.ControllerId = "divMemberNumber", .ControllerName = "Member Number", .LoanType = "SA", .IsVisible = IIf(XaEnableMemberNumber = "Y", "Y", "N").ToString()})
			showFieldList.Add(New SmShowFieldItemModel() With {.ControllerId = "co_divMemberNumber", .ControllerName = "Co-App Member Number", .LoanType = "SA", .IsVisible = IIf(XaEnableMemberNumber = "Y", "Y", "N").ToString()})
			showFieldList.Add(New SmShowFieldItemModel() With {.ControllerId = "m_divMemberNumber", .ControllerName = "Minor Member Number", .LoanType = "SA_SPECIAL", .IsVisible = IIf(XaEnableMemberNumber = "Y", "Y", "N").ToString()})
		End If

		If Not String.IsNullOrEmpty(XaMaritalStatus) AndAlso XaMaritalStatus = "Y" Then
			showFieldList.Add(New SmShowFieldItemModel() With {.ControllerId = "divMaritalStatus", .ControllerName = "Marital Status", .LoanType = "XA", .IsVisible = IIf(XaMaritalStatus = "Y", "Y", "N").ToString()})
			showFieldList.Add(New SmShowFieldItemModel() With {.ControllerId = "co_divMaritalStatus", .ControllerName = "Co-App Marital Status", .LoanType = "XA", .IsVisible = IIf(XaMaritalStatus = "Y", "Y", "N").ToString()})
			showFieldList.Add(New SmShowFieldItemModel() With {.ControllerId = "divMaritalStatus", .ControllerName = "Marital Status", .LoanType = "SA", .IsVisible = IIf(XaMaritalStatus = "Y", "Y", "N").ToString()})
			showFieldList.Add(New SmShowFieldItemModel() With {.ControllerId = "co_divMaritalStatus", .ControllerName = "Co-App Marital Status", .LoanType = "SA", .IsVisible = IIf(XaMaritalStatus = "Y", "Y", "N").ToString()})
			showFieldList.Add(New SmShowFieldItemModel() With {.ControllerId = "divMaritalStatus", .ControllerName = "Marital Status", .LoanType = "XA_SPECIAL", .IsVisible = IIf(XaMaritalStatus = "Y", "Y", "N").ToString()})
			showFieldList.Add(New SmShowFieldItemModel() With {.ControllerId = "co_divMaritalStatus", .ControllerName = "Co-App Marital Status", .LoanType = "XA_SPECIAL", .IsVisible = IIf(XaMaritalStatus = "Y", "Y", "N").ToString()})
			showFieldList.Add(New SmShowFieldItemModel() With {.ControllerId = "m_divMaritalStatus", .ControllerName = "Minor Marital Status", .LoanType = "XA_SPECIAL", .IsVisible = IIf(XaMaritalStatus = "Y", "Y", "N").ToString()})

		End If


		If Not String.IsNullOrEmpty(Citizenship) AndAlso Citizenship = "N" Then
			showFieldList.Add(New SmShowFieldItemModel() With {.ControllerId = "divCitizenshipStatus", .ControllerName = "Citizenship Status", .LoanType = "PL", .IsVisible = IIf(Citizenship = "Y", "Y", "N").ToString()})
			showFieldList.Add(New SmShowFieldItemModel() With {.ControllerId = "co_divCitizenshipStatus", .ControllerName = "Co-App Citizenship Status", .LoanType = "PL", .IsVisible = IIf(Citizenship = "Y", "Y", "N").ToString()})
			showFieldList.Add(New SmShowFieldItemModel() With {.ControllerId = "divCitizenshipStatus", .ControllerName = "Citizenship Status", .LoanType = "CC", .IsVisible = IIf(Citizenship = "Y", "Y", "N").ToString()})
			showFieldList.Add(New SmShowFieldItemModel() With {.ControllerId = "co_divCitizenshipStatus", .ControllerName = "Co-App Citizenship Status", .LoanType = "CC", .IsVisible = IIf(Citizenship = "Y", "Y", "N").ToString()})
			showFieldList.Add(New SmShowFieldItemModel() With {.ControllerId = "divCitizenshipStatus", .ControllerName = "Citizenship Status", .LoanType = "VL", .IsVisible = IIf(Citizenship = "Y", "Y", "N").ToString()})
			showFieldList.Add(New SmShowFieldItemModel() With {.ControllerId = "co_divCitizenshipStatus", .ControllerName = "Co-App Citizenship Status", .LoanType = "VL", .IsVisible = IIf(Citizenship = "Y", "Y", "N").ToString()})
			showFieldList.Add(New SmShowFieldItemModel() With {.ControllerId = "divCitizenshipStatus", .ControllerName = "Citizenship Status", .LoanType = "HE", .IsVisible = IIf(Citizenship = "Y", "Y", "N").ToString()})
			showFieldList.Add(New SmShowFieldItemModel() With {.ControllerId = "co_divCitizenshipStatus", .ControllerName = "Co-App Citizenship Status", .LoanType = "HE", .IsVisible = IIf(Citizenship = "Y", "Y", "N").ToString()})

		End If

		If Not String.IsNullOrEmpty(XaCitizenship) AndAlso XaCitizenship = "N" Then
			showFieldList.Add(New SmShowFieldItemModel() With {.ControllerId = "divCitizenshipStatus", .ControllerName = "Citizenship Status", .LoanType = "XA", .IsVisible = IIf(XaCitizenship = "Y", "Y", "N").ToString()})
			showFieldList.Add(New SmShowFieldItemModel() With {.ControllerId = "co_divCitizenshipStatus", .ControllerName = "Co-App Citizenship Status", .LoanType = "XA", .IsVisible = IIf(XaCitizenship = "Y", "Y", "N").ToString()})
			showFieldList.Add(New SmShowFieldItemModel() With {.ControllerId = "divCitizenshipStatus", .ControllerName = "Citizenship Status", .LoanType = "XA_SPECIAL", .IsVisible = IIf(XaCitizenship = "Y", "Y", "N").ToString()})
			showFieldList.Add(New SmShowFieldItemModel() With {.ControllerId = "co_divCitizenshipStatus", .ControllerName = "Co-App Citizenship Status", .LoanType = "XA_SPECIAL", .IsVisible = IIf(XaCitizenship = "Y", "Y", "N").ToString()})
			showFieldList.Add(New SmShowFieldItemModel() With {.ControllerId = "m_divCitizenshipStatus", .ControllerName = "Minor Citizenship Status", .LoanType = "XA_SPECIAL", .IsVisible = IIf(XaCitizenship = "Y", "Y", "N").ToString()})
		End If


		If Not String.IsNullOrEmpty(XaMotherMaidenNameSecondary) Then
			showFieldList.Add(New SmShowFieldItemModel() With {.ControllerId = "divMotherMaidenName", .ControllerName = "Mother Maiden Name", .LoanType = "SA", .IsVisible = IIf(XaMotherMaidenNameSecondary = "Y", "Y", "N").ToString()})
			showFieldList.Add(New SmShowFieldItemModel() With {.ControllerId = "co_divMotherMaidenName", .ControllerName = "Co-App Mother Maiden Name", .LoanType = "SA", .IsVisible = IIf(XaMotherMaidenNameSecondary = "Y", "Y", "N").ToString()})
		End If

		If Not String.IsNullOrEmpty(XaCitizenshipSecondary) AndAlso XaCitizenshipSecondary = "N" Then
			showFieldList.Add(New SmShowFieldItemModel() With {.ControllerId = "divCitizenshipStatus", .ControllerName = "Citizenship Status", .LoanType = "SA", .IsVisible = IIf(XaCitizenshipSecondary = "Y", "Y", "N").ToString()})
			showFieldList.Add(New SmShowFieldItemModel() With {.ControllerId = "co_divCitizenshipStatus", .ControllerName = "Co-App Citizenship Status", .LoanType = "SA", .IsVisible = IIf(XaCitizenshipSecondary = "Y", "Y", "N").ToString()})
		End If

		If Not String.IsNullOrEmpty(XaEmployment) AndAlso XaEmployment = "N" Then
			showFieldList.Add(New SmShowFieldItemModel() With {.ControllerId = "divEmploymentSection", .ControllerName = "Employment", .LoanType = "XA", .IsVisible = IIf(XaEmployment = "Y", "Y", "N").ToString()})
			showFieldList.Add(New SmShowFieldItemModel() With {.ControllerId = "co_divEmploymentSection", .ControllerName = "Co-App Employment", .LoanType = "XA", .IsVisible = IIf(XaEmployment = "Y", "Y", "N").ToString()})
		End If

		If Not String.IsNullOrEmpty(XaEmploymentSecondary) AndAlso XaEmploymentSecondary = "N" Then
			showFieldList.Add(New SmShowFieldItemModel() With {.ControllerId = "divEmploymentSection", .ControllerName = "Employment", .LoanType = "SA", .IsVisible = IIf(XaEmploymentSecondary = "Y", "Y", "N").ToString()})
			showFieldList.Add(New SmShowFieldItemModel() With {.ControllerId = "co_divEmploymentSection", .ControllerName = "Co-App Employment", .LoanType = "SA", .IsVisible = IIf(XaEmploymentSecondary = "Y", "Y", "N").ToString()})
		End If

		If Not String.IsNullOrEmpty(XaEmploymentMinor) Then	 'default is no
			showFieldList.Add(New SmShowFieldItemModel() With {.ControllerId = "divEmploymentSection", .ControllerName = "Employment", .LoanType = "XA_SPECIAL", .IsVisible = IIf(XaEmploymentMinor = "Y", "Y", "N").ToString()})
			showFieldList.Add(New SmShowFieldItemModel() With {.ControllerId = "co_divEmploymentSection", .ControllerName = "Co-App Employment", .LoanType = "XA_SPECIAL", .IsVisible = IIf(XaEmploymentMinor = "Y", "Y", "N").ToString()})
		End If

		If Not String.IsNullOrEmpty(XaGrossIncomeSecondary) AndAlso XaGrossIncomeSecondary = "N" Then
			showFieldList.Add(New SmShowFieldItemModel() With {.ControllerId = "divGrossMonthlyIncome", .ControllerName = "Gross Monthly Income", .LoanType = "SA", .IsVisible = IIf(XaGrossIncomeSecondary = "Y", "Y", "N").ToString()})
			showFieldList.Add(New SmShowFieldItemModel() With {.ControllerId = "co_divGrossMonthlyIncome", .ControllerName = "Co-App Gross Monthly Income", .LoanType = "SA", .IsVisible = IIf(XaGrossIncomeSecondary = "Y", "Y", "N").ToString()})
		End If

		If Not String.IsNullOrEmpty(XaIDPageSecondary) AndAlso XaIDPageSecondary = "N" Then
			showFieldList.Add(New SmShowFieldItemModel() With {.ControllerId = "divApplicantIdentification", .ControllerName = "Applicant Identification", .LoanType = "SA", .IsVisible = IIf(XaIDPageSecondary = "Y", "Y", "N").ToString()})
			showFieldList.Add(New SmShowFieldItemModel() With {.ControllerId = "co_divApplicantIdentification", .ControllerName = "Co-App Applicant Identification", .LoanType = "SA", .IsVisible = IIf(XaIDPageSecondary = "Y", "Y", "N").ToString()})
		End If

		If Not String.IsNullOrEmpty(XaOccupancySecondary) Then
			showFieldList.Add(New SmShowFieldItemModel() With {.ControllerId = "divOccupancyStatusSection", .ControllerName = "Occupancy Status", .LoanType = "SA", .IsVisible = IIf(XaOccupancySecondary = "Y", "Y", "N").ToString()})
			showFieldList.Add(New SmShowFieldItemModel() With {.ControllerId = "co_divOccupancyStatusSection", .ControllerName = "Co-App Occupancy Status", .LoanType = "SA", .IsVisible = IIf(XaOccupancySecondary = "Y", "Y", "N").ToString()})
		End If

		If Not String.IsNullOrEmpty(XaOccupancy) Then
			showFieldList.Add(New SmShowFieldItemModel() With {.ControllerId = "divOccupancyStatusSection", .ControllerName = "Occupancy Status", .LoanType = "XA", .IsVisible = IIf(XaOccupancy = "Y", "Y", "N").ToString()})
			showFieldList.Add(New SmShowFieldItemModel() With {.ControllerId = "co_divOccupancyStatusSection", .ControllerName = "Co-App Occupancy Status", .LoanType = "XA", .IsVisible = IIf(XaOccupancy = "Y", "Y", "N").ToString()})
		End If


		'If Not String.IsNullOrEmpty(XaDisclosuresSecondaryEnable) Then
		'	showFieldList.Add(New SmShowFieldItemModel() With {.ControllerId = "divDisclosureSection", .ControllerName = "Disclosure", .LoanType = "SA", .IsVisible = IIf(XaDisclosuresSecondaryEnable = "Y", "Y", "N").ToString()})
		'End If


		If Not String.IsNullOrEmpty(LoanBranchSelectionDropdown) AndAlso LoanBranchSelectionDropdown = "Y" Then
			showFieldList.Add(New SmShowFieldItemModel() With {.ControllerId = "divBranchSection", .ControllerName = "Branch", .LoanType = "CC", .IsVisible = IIf(LoanBranchSelectionDropdown = "Y", "Y", "N").ToString()})
			showFieldList.Add(New SmShowFieldItemModel() With {.ControllerId = "divBranchSection", .ControllerName = "Branch", .LoanType = "CC_COMBO", .IsVisible = IIf(LoanBranchSelectionDropdown = "Y", "Y", "N").ToString()})
			showFieldList.Add(New SmShowFieldItemModel() With {.ControllerId = "divBranchSection", .ControllerName = "Branch", .LoanType = "HE", .IsVisible = IIf(LoanBranchSelectionDropdown = "Y", "Y", "N").ToString()})
			showFieldList.Add(New SmShowFieldItemModel() With {.ControllerId = "divBranchSection", .ControllerName = "Branch", .LoanType = "HE_COMBO", .IsVisible = IIf(LoanBranchSelectionDropdown = "Y", "Y", "N").ToString()})
			showFieldList.Add(New SmShowFieldItemModel() With {.ControllerId = "divBranchSection", .ControllerName = "Branch", .LoanType = "PL", .IsVisible = IIf(LoanBranchSelectionDropdown = "Y", "Y", "N").ToString()})
			showFieldList.Add(New SmShowFieldItemModel() With {.ControllerId = "divBranchSection", .ControllerName = "Branch", .LoanType = "PL_COMBO", .IsVisible = IIf(LoanBranchSelectionDropdown = "Y", "Y", "N").ToString()})
			showFieldList.Add(New SmShowFieldItemModel() With {.ControllerId = "divBranchSection", .ControllerName = "Branch", .LoanType = "VL", .IsVisible = IIf(LoanBranchSelectionDropdown = "Y", "Y", "N").ToString()})
			showFieldList.Add(New SmShowFieldItemModel() With {.ControllerId = "divBranchSection", .ControllerName = "Branch", .LoanType = "VL_COMBO", .IsVisible = IIf(LoanBranchSelectionDropdown = "Y", "Y", "N").ToString()})
		End If

		If Not String.IsNullOrEmpty(CCIdentificationSection) AndAlso CCIdentificationSection = "Y" Then
			showFieldList.Add(New SmShowFieldItemModel() With {.ControllerId = "divApplicantIdentification", .ControllerName = "Applicant Identification", .LoanType = "CC", .IsVisible = IIf(CCIdentificationSection = "Y", "Y", "N").ToString()})
			showFieldList.Add(New SmShowFieldItemModel() With {.ControllerId = "co_divApplicantIdentification", .ControllerName = "Co-App Applicant Identification", .LoanType = "CC", .IsVisible = IIf(CCIdentificationSection = "Y", "Y", "N").ToString()})
			showFieldList.Add(New SmShowFieldItemModel() With {.ControllerId = "divApplicantIdentification", .ControllerName = "Applicant Identification", .LoanType = "CC_COMBO", .IsVisible = IIf(CCIdentificationSection = "Y", "Y", "N").ToString()})
			showFieldList.Add(New SmShowFieldItemModel() With {.ControllerId = "co_divApplicantIdentification", .ControllerName = "Co-App Applicant Identification", .LoanType = "CC_COMBO", .IsVisible = IIf(CCIdentificationSection = "Y", "Y", "N").ToString()})
		End If

		If Not String.IsNullOrEmpty(HEIdentificationSection) AndAlso HEIdentificationSection = "Y" Then
			showFieldList.Add(New SmShowFieldItemModel() With {.ControllerId = "divApplicantIdentification", .ControllerName = "Applicant Identification", .LoanType = "HE", .IsVisible = IIf(HEIdentificationSection = "Y", "Y", "N").ToString()})
			showFieldList.Add(New SmShowFieldItemModel() With {.ControllerId = "co_divApplicantIdentification", .ControllerName = "Co-App Applicant Identification", .LoanType = "HE", .IsVisible = IIf(HEIdentificationSection = "Y", "Y", "N").ToString()})
			showFieldList.Add(New SmShowFieldItemModel() With {.ControllerId = "divApplicantIdentification", .ControllerName = "Applicant Identification", .LoanType = "HE_COMBO", .IsVisible = IIf(HEIdentificationSection = "Y", "Y", "N").ToString()})
			showFieldList.Add(New SmShowFieldItemModel() With {.ControllerId = "co_divApplicantIdentification", .ControllerName = "Co-App Applicant Identification", .LoanType = "HE_COMBO", .IsVisible = IIf(HEIdentificationSection = "Y", "Y", "N").ToString()})
		End If

		If Not String.IsNullOrEmpty(PLIdentificationSection) AndAlso PLIdentificationSection = "Y" Then
			showFieldList.Add(New SmShowFieldItemModel() With {.ControllerId = "divApplicantIdentification", .ControllerName = "Applicant Identification", .LoanType = "PL", .IsVisible = IIf(PLIdentificationSection = "Y", "Y", "N").ToString()})
			showFieldList.Add(New SmShowFieldItemModel() With {.ControllerId = "co_divApplicantIdentification", .ControllerName = "Co-App Applicant Identification", .LoanType = "PL", .IsVisible = IIf(PLIdentificationSection = "Y", "Y", "N").ToString()})
			showFieldList.Add(New SmShowFieldItemModel() With {.ControllerId = "divApplicantIdentification", .ControllerName = "Applicant Identification", .LoanType = "PL_COMBO", .IsVisible = IIf(PLIdentificationSection = "Y", "Y", "N").ToString()})
			showFieldList.Add(New SmShowFieldItemModel() With {.ControllerId = "co_divApplicantIdentification", .ControllerName = "Co-App Applicant Identification", .LoanType = "PL_COMBO", .IsVisible = IIf(PLIdentificationSection = "Y", "Y", "N").ToString()})
		End If

		If Not String.IsNullOrEmpty(VLIdentificationSection) AndAlso VLIdentificationSection = "Y" Then
			showFieldList.Add(New SmShowFieldItemModel() With {.ControllerId = "divApplicantIdentification", .ControllerName = "Applicant Identification", .LoanType = "VL", .IsVisible = IIf(VLIdentificationSection = "Y", "Y", "N").ToString()})
			showFieldList.Add(New SmShowFieldItemModel() With {.ControllerId = "co_divApplicantIdentification", .ControllerName = "Co-App Applicant Identification", .LoanType = "VL", .IsVisible = IIf(VLIdentificationSection = "Y", "Y", "N").ToString()})
			showFieldList.Add(New SmShowFieldItemModel() With {.ControllerId = "divApplicantIdentification", .ControllerName = "Applicant Identification", .LoanType = "VL_COMBO", .IsVisible = IIf(VLIdentificationSection = "Y", "Y", "N").ToString()})
			showFieldList.Add(New SmShowFieldItemModel() With {.ControllerId = "co_divApplicantIdentification", .ControllerName = "Co-App Applicant Identification", .LoanType = "VL_COMBO", .IsVisible = IIf(VLIdentificationSection = "Y", "Y", "N").ToString()})
		End If

		If showFieldList IsNot Nothing AndAlso showFieldList.Any() Then
			Dim loanTypeList As New List(Of String)(New String() {"XA", "SA", "XA_SPECIAL", "CC", "CC_COMBO", "HE", "HE_COMBO", "PL", "PL_COMBO", "VL", "VL_COMBO"})
			For Each loanType As String In loanTypeList
				Dim nodeBatch As New SmLenderConfigDraft() With {
				.LenderConfigID = lenderConfigId,
				.CreatedByUserID = UserInfo.UserID,
				.NodePath = Sm.SHOW_FIELDS_ITEMS(loanType),
				.NodeContent = SmUtil.BuildShowFieldNodeXml("SHOW_FIELDS", loanType, showFieldList.Where(Function(p) p.LoanType = loanType).ToList())}
				smBL.MigrateNodes(lenderConfigId, Guid.Empty, Request.UserHostAddress, "migrate configurations", AddressOf SmUtil.MergeShowFieldItems, nodeBatch)
			Next
			'remove legacy attributes after migrated
			smBL.MigrateNodes(lenderConfigId, Guid.Empty, Request.UserHostAddress, "delete legacy attributes after migrated", AddressOf RemoveLegacyAttributes, New SmLenderConfigDraft() With {
					.LenderConfigID = lenderConfigId,
					.CreatedByUserID = UserInfo.UserID,
					.NodePath = String.Empty,
					.NodeContent = String.Empty})
		End If

		'code to remove one legacy item
		'If HELOCIdentificationSection <> "" Then
		'	smBL.MigrateNodes(lenderConfigId, "system", Request.UserHostAddress, "delete legacy attributes after migrated", AddressOf RemoveLegacyAttributes, New SmLenderConfigDraft() With {
		'				.LenderConfigID = lenderConfigId,
		'				.CreatedByUserID = UserInfo.UserID,
		'				.NodePath = String.Empty,
		'				.NodeContent = String.Empty})
		'End If

		'code for remove certain item from showfield, need to comment out code from the showFieldList loop first
		'If SAShowFieldDisclosure IsNot Nothing Then
		'	smBL.MigrateNodes(lenderConfigId, "system", Request.UserHostAddress, "delete nodes that are wrongly created after migrated", AddressOf RemoveShowField, New SmLenderConfigDraft() With {
		'			.LenderConfigID = lenderConfigId,
		'			.CreatedByUserID = UserInfo.UserID,
		'			.NodePath = String.Empty,
		'			.NodeContent = String.Empty})
		'End If

	End Sub



	Private Sub RunFixMigrationBatch(lenderConfigId As Guid, lenderConfigXmlDoc As XmlDocument)
		Dim smBL As New SmBL
		'----
		Dim showFieldList As New List(Of SmShowFieldItemModel)

		showFieldList.Add(New SmShowFieldItemModel() With {.ControllerId = "divOccupancyStatusSection", .ControllerName = "Occupancy Status", .LoanType = "XA", .IsVisible = "Y"})
		showFieldList.Add(New SmShowFieldItemModel() With {.ControllerId = "co_divOccupancyStatusSection", .ControllerName = "Co-App Occupancy Status", .LoanType = "XA", .IsVisible = "Y"})


		If showFieldList IsNot Nothing AndAlso showFieldList.Any() Then
			Dim loanTypeList As New List(Of String)(New String() {"XA"})
			For Each loanType As String In loanTypeList
				Dim nodeBatch As New SmLenderConfigDraft() With {
				.LenderConfigID = lenderConfigId,
				.CreatedByUserID = UserInfo.UserID,
				.NodePath = Sm.SHOW_FIELDS_ITEMS(loanType),
				.NodeContent = SmUtil.BuildShowFieldNodeXml("SHOW_FIELDS", loanType, showFieldList.Where(Function(p) p.LoanType = loanType).ToList())}
				smBL.MigrateNodes(lenderConfigId, Guid.Empty, Request.UserHostAddress, "migrate configurations", AddressOf SmUtil.UpdateShowFieldItems, nodeBatch)
			Next
		End If
	End Sub

	Private Function GetData(configXmlDoc As XmlDocument, path As String) As String
		Dim node As XmlNode = configXmlDoc.SelectSingleNode(path)
		If node IsNot Nothing Then
			Return node.Value.ToUpper()
		End If
		Return String.Empty
	End Function


	Private Function RemoveLegacyAttributes(liveConfigXmlDoc As XmlDocument, nodeBatch As SmLenderConfigDraft) As XmlDocument
		'Dim xaBranchSelectionDropdownNode = CType(liveConfigXmlDoc.SelectSingleNode("WEBSITE/XA_LOAN/VISIBLE/@branch_selection_dropdown"), XmlAttribute)
		'If xaBranchSelectionDropdownNode IsNot Nothing Then
		'	xaBranchSelectionDropdownNode.OwnerElement.RemoveAttribute("branch_selection_dropdown")
		'End If

		Dim xaVisibleNode = liveConfigXmlDoc.SelectSingleNode("WEBSITE/XA_LOAN/VISIBLE")
		If xaVisibleNode IsNot Nothing Then
			xaVisibleNode.Attributes.RemoveNamedItem("branch_selection_dropdown")
			xaVisibleNode.Attributes.RemoveNamedItem("mother_maiden_name")
			xaVisibleNode.Attributes.RemoveNamedItem("gender")
			xaVisibleNode.Attributes.RemoveNamedItem("gender_secondary")
			xaVisibleNode.Attributes.RemoveNamedItem("citizenship")
			xaVisibleNode.Attributes.RemoveNamedItem("employment")
			xaVisibleNode.Attributes.RemoveNamedItem("mother_maiden_name_secondary")
			xaVisibleNode.Attributes.RemoveNamedItem("citizenship_secondary")
			xaVisibleNode.Attributes.RemoveNamedItem("employment_secondary")
			xaVisibleNode.Attributes.RemoveNamedItem("gross_income_secondary")
			xaVisibleNode.Attributes.RemoveNamedItem("id_page_secondary")
			xaVisibleNode.Attributes.RemoveNamedItem("occupancy_secondary")
			xaVisibleNode.Attributes.RemoveNamedItem("disclosures_secondary_enable")
			xaVisibleNode.Attributes.RemoveNamedItem("minor_employment")
			xaVisibleNode.Attributes.RemoveNamedItem("marital_status")
			xaVisibleNode.Attributes.RemoveNamedItem("mem_number_secondary")
			xaVisibleNode.Attributes.RemoveNamedItem("occupancy_status")
		End If

		Dim websiteVisibleNode = liveConfigXmlDoc.SelectSingleNode("WEBSITE/VISIBLE")
		If websiteVisibleNode IsNot Nothing Then
			websiteVisibleNode.Attributes.RemoveNamedItem("branch_selection_dropdown")
			websiteVisibleNode.Attributes.RemoveNamedItem("citizenship")
		End If

		Dim ccVisibleNode = liveConfigXmlDoc.SelectSingleNode("WEBSITE/CREDIT_CARD_LOAN/VISIBLE")
		If ccVisibleNode IsNot Nothing Then
			ccVisibleNode.Attributes.RemoveNamedItem("identification_section")
		End If

		Dim heVisibleNode = liveConfigXmlDoc.SelectSingleNode("WEBSITE/HOME_EQUITY_LOAN/VISIBLE")
		If heVisibleNode IsNot Nothing Then
			heVisibleNode.Attributes.RemoveNamedItem("identification_section")
		End If

		'Dim helocVisibleNode = liveConfigXmlDoc.SelectSingleNode("WEBSITE/HOME_EQUITY_LOC/VISIBLE")
		'If helocVisibleNode IsNot Nothing Then
		'	helocVisibleNode.Attributes.RemoveNamedItem("identification_section")
		'End If

		Dim plVisibleNode = liveConfigXmlDoc.SelectSingleNode("WEBSITE/PERSONAL_LOAN/VISIBLE")
		If plVisibleNode IsNot Nothing Then
			plVisibleNode.Attributes.RemoveNamedItem("identification_section")
		End If

		Dim vlVisibleNode = liveConfigXmlDoc.SelectSingleNode("WEBSITE/VEHICLE_LOAN/VISIBLE")
		If vlVisibleNode IsNot Nothing Then
			vlVisibleNode.Attributes.RemoveNamedItem("identification_section")
		End If
		Return liveConfigXmlDoc
	End Function


	Private Function RemoveShowField(liveConfigXmlDoc As XmlDocument, nodeBatch As SmLenderConfigDraft) As XmlDocument
		Dim SAShowFieldDisclosure = liveConfigXmlDoc.SelectSingleNode("WEBSITE/CUSTOM_LIST/SHOW_FIELDS/ITEM[@controller_id='divDisclosureSection']")
		If SAShowFieldDisclosure IsNot Nothing Then
			SAShowFieldDisclosure.ParentNode.RemoveChild(SAShowFieldDisclosure)
		End If
		Return liveConfigXmlDoc
	End Function


	Private Function MoveHeLocDisclosures(liveConfigXmlDoc As XmlDocument, nodeBatch As SmLenderConfigDraft) As XmlDocument
		Dim heNode As XmlNode = liveConfigXmlDoc.SelectSingleNode("WEBSITE/HOME_EQUITY_LOAN")
		Dim heDisclosuresLocNode As XmlNode = heNode.SelectSingleNode("DISCLOSURES_LOC")
		If heDisclosuresLocNode Is Nothing AndAlso nodeBatch.NodeContent <> "" Then
			heDisclosuresLocNode = liveConfigXmlDoc.CreateElement("DISCLOSURES_LOC")
			heNode.AppendChild(heDisclosuresLocNode)
		End If
		If nodeBatch.NodeContent <> "" Then
			heDisclosuresLocNode.InnerXml = nodeBatch.NodeContent
		End If

		'delete HOME_EQUITY_LOC node
		Dim heLocNode As XmlNode = liveConfigXmlDoc.SelectSingleNode("WEBSITE/HOME_EQUITY_LOC")
		heLocNode.ParentNode.RemoveChild(heLocNode)
		Return liveConfigXmlDoc
	End Function
	Private Sub RunBatchHeLoc(lenderConfigId As Guid, lenderConfigXmlDoc As XmlDocument)
		Dim smBL As New SmBL
		Dim heLocNode As XmlNode = lenderConfigXmlDoc.SelectSingleNode("WEBSITE/HOME_EQUITY_LOC")
		Dim heNode As XmlNode = lenderConfigXmlDoc.SelectSingleNode("WEBSITE/HOME_EQUITY_LOAN")

		'TODO: 
		'case1 HOME_EQUITY_LOC exist but HOME_EQUITY_LOAN is not available,
		'case 2 HOME_EQUITY_LOC exist but has no disclosure
		'check HE and HELOC enabled
		If heLocNode IsNot Nothing AndAlso heNode IsNot Nothing Then
			Dim heLocDisclosures As XmlNode = heLocNode.SelectSingleNode("DISCLOSURES")
			If heLocDisclosures IsNot Nothing AndAlso heLocDisclosures.SelectNodes("DISCLOSURE").Count > 0 Then	'move disclosure from HOME_EQUITY_LOC to  HOME_EQUITY_LOAN
				Dim nodeBatch As New SmLenderConfigDraft() With {
				.LenderConfigID = lenderConfigId,
				.CreatedByUserID = UserInfo.UserID,
				.NodePath = "WEBSITE/HOME_EQUITY_LOAN/DISCLOSURES_LOC",
				.NodeContent = heLocDisclosures.InnerXml}
				smBL.MigrateNodes(lenderConfigId, Guid.Empty, Request.UserHostAddress, "migrate HELOC configurations", AddressOf MoveHeLocDisclosures, nodeBatch)
			Else ' case #2, delete HOME_EQUITY_LOC, the one without disclosure
				Dim nodeBatch As New SmLenderConfigDraft() With {
				.LenderConfigID = lenderConfigId,
				.CreatedByUserID = UserInfo.UserID,
				.NodePath = "WEBSITE/HOME_EQUITY_LOAN/DISCLOSURES_LOC",
				.NodeContent = ""}
				smBL.MigrateNodes(lenderConfigId, Guid.Empty, Request.UserHostAddress, "migrate HELOC configurations", AddressOf MoveHeLocDisclosures, nodeBatch)
			End If
		ElseIf heLocNode IsNot Nothing AndAlso heNode Is Nothing Then ' case #1,remove un-unsed HOME_EQUITY_LOC
			Dim nodeBatch As New SmLenderConfigDraft() With {
				.LenderConfigID = lenderConfigId,
				.CreatedByUserID = UserInfo.UserID,
				.NodePath = "WEBSITE/HOME_EQUITY_LOAN/DISCLOSURES_LOC",
				.NodeContent = ""}
			smBL.MigrateNodes(lenderConfigId, Guid.Empty, Request.UserHostAddress, "migrate HELOC configurations", AddressOf MoveHeLocDisclosures, nodeBatch)
		End If
	End Sub


	Private Sub RunBatchEncryptAllPasswords(lenderConfigId As Guid)

		Dim oDB As New CSQLDBUtils(LPQMOBILE_CONNECTIONSTRING)
		Try
			Dim smBL As New SmBL
			Dim configStr As String = ""
			Dim liveConfig As CLenderConfig = smBL.GetLiveConfig(lenderConfigId)
			If liveConfig IsNot Nothing Then
				Dim configXml As New XmlDocument()
				configXml.LoadXml(liveConfig.ConfigData)
				configXml = Common.EncryptAllPasswordInConfigXml(configXml)
				Dim sValidationResult = SmUtil.ValidateXMLConstruction(configXml)
				Dim sLenderref As String = "unknow"
				If liveConfig.LenderRef IsNot Nothing Then
					sLenderref = liveConfig.LenderRef
				End If
				If sValidationResult <> "" Then
					log.Warn(String.Format("XML Construction failed XSD Validation for {0}: {1} ", sLenderref, sValidationResult))
					Throw New Exception(String.Format("XML Construction failed XSD Validation for {0}: {1} ", sLenderref, sValidationResult))
				End If
				configStr = configXml.OuterXml
			End If

			''update live config data
			Dim oWhere As New CSQLWhereStringBuilder()
			oWhere.AppendAndCondition("LenderConfigID={0}", New CSQLParamValue(lenderConfigId))
			Dim oUpdate As New cSQLUpdateStringBuilder("LenderConfigs", oWhere)
			oUpdate.appendvalue("ConfigData", New CSQLParamValue(configStr))
			oUpdate.appendvalue("LPQPW", New CSQLParamValue(Common.EncryptPassword(liveConfig.LPQPW)))
			'don't need to update this since audit log will not show any modification
			'oUpdate.appendvalue("LastModifiedDate", New CSQLParamValue(Now))
			'oUpdate.appendvalue("LastModifiedByUserID", New CSQLParamValue(UserInfo.UserID))
			'oUpdate.appendvalue("RevisionID", New CSQLParamValue(liveConfig.RevisionID + 1))
			oDB.executeNonQuery(oUpdate.SQL, True)

			'insert log, no point of doing this since the log also has encrypted pw
			'Dim oInsert As New cSQLInsertStringBuilder("LenderConfigLogs")
			'oInsert.AppendValue("LenderConfigLogID", New CSQLParamValue(System.Guid.NewGuid))
			'oInsert.AppendValue("LenderConfigID", New CSQLParamValue(liveConfig.ID))
			'oInsert.AppendValue("OrganizationID", New CSQLParamValue(liveConfig.OrganizationId))
			'oInsert.AppendValue("LenderID", New CSQLParamValue(Common.SafeGUID(liveConfig.LenderId)))
			'oInsert.AppendValue("LenderRef", New CSQLParamValue(liveConfig.LenderRef))
			'oInsert.AppendValue("ReturnURL", New CSQLParamValue(liveConfig.ReturnURL))
			'oInsert.AppendValue("LPQLogin", New CSQLParamValue(liveConfig.LPQLogin))
			'oInsert.AppendValue("LPQPW", New CSQLParamValue(Common.EncryptPassword(liveConfig.LPQPW)))
			'oInsert.AppendValue("LPQURL", New CSQLParamValue(liveConfig.LPQURL))
			'oInsert.AppendValue("ConfigData", New CSQLParamValue(CStringCompressor.Compress(configStr)))
			'oInsert.AppendValue("LogTime", New CSQLParamValue(Now))
			'oInsert.AppendValue("RemoteIP", New CSQLParamValue(Request.UserHostAddress))
			'         oInsert.AppendValue("LastModifiedByUserID", New CSQLParamValue(UserInfo.UserID))
			'         oInsert.AppendValue("note", New CSQLParamValue(""))
			'oInsert.AppendValue("Comment", New CSQLParamValue("encrypt all passwords in config xml"))
			'oInsert.AppendValue("RevisionID", New CSQLParamValue(liveConfig.RevisionID + 1))
			'oDB.executeNonQuery(oInsert.SQL, True)

			oDB.commitTransactionIfOpen()
		Catch ex As Exception
			log.Error("Error while RunBatchEncryptAllPasswords", ex)
			oDB.rollbackTransactionIfOpen()
		Finally
			oDB.closeConnection()
		End Try
	End Sub
	'Private Function RunbatchMigrateLenderConfigUsersToUserRoles() As String
	'	Dim result As String = ""
	'	Dim sSQL As String = String.Format("insert into UserRoles(UserRoleID, Role, LenderID, LenderConfigID, VendorID, UserID, Status) select newid(), Role, LenderID, null, null, UserID, Status from (select distinct '{0}' as Role, L.LenderID, LC.UserID, 1 as Status from LenderConfigUsers LC inner join LenderConfigs L  on L.LenderConfigID = LC.LenderConfigID inner join Users U on LC.UserID = U.UserID where LC.Status = 1 and U.Status = 1 and LC.UserID not in(select UserID from UserRoles)) T", SmSettings.Role.LenderAdmin.ToString())
	'	Dim oDB As New CSQLDBUtils(LPQMOBILE_CONNECTIONSTRING)
	'	Try
	'		oDB.executeNonQuery(sSQL, False)
	'		result = "ok"
	'	Catch ex As Exception
	'		log.Error("Error while RunbatchMigrateLenderConfigUsersToUserRoles", ex)
	'		result = "error"
	'	Finally
	'		oDB.closeConnection()
	'	End Try
	'	Return result
	'End Function

	Protected Sub Page_PreLoad1(sender As Object, e As EventArgs) Handles Me.PreLoad
		AllowedRoles.Add(SmSettings.Role.SuperOperator.ToString())
		NotAllowedAccessRedirectUrl = "/Sm/Default.aspx"
	End Sub
End Class
