﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="Accounting.aspx.vb" Inherits="Sm_so_Accounting"   MasterPageFile="../MasterPage.master" %>

<asp:Content runat="server" ContentPlaceHolderID="plBody">
	<div>
		<h1 class="page-header">Accounting</h1>
		<p>App Portal usage reports for external services.</p>
		<div class="row">
			<div class="col-lg-2 col-sm-4 form-group">
				<p>Report of Successful InstaTouch Prefills</p>
				<label for="ddlTimePeriod">Time Period</label>
				<select class="form-control" id="ddlTimePeriod">
					<%	For Each item In TimePeriodLst%>
					<option value="<%=item.ToString("MM/dd/yyyy")%>"><%=item.ToString("MMMM yyyy")%></option>
					<%Next%>
				</select>
			</div> 
		</div>
		<div class="row">
			<div class="col-sm-12">
				<button id="btnDownload" class="btn btn-primary mb10 mr5">Download</button>
			</div>
		</div>
	</div>
</asp:Content>
<asp:Content runat="server" ContentPlaceHolderID="plScripts">
	<script type="text/javascript">
		$(function () {
			$("#btnDownload").on("click", function () {
				var downloadExcelPage = window.open('/sm/loading.aspx');
				$.ajax({
					url: "/sm/smhandler.aspx",
					async: true,
					cache: false,
					type: 'POST',
					dataType: 'json',
					data: { mo: $("#ddlTimePeriod").val(), command: 'runinstatouchaccountingreport' },
					success: function (response) {
						if (response.IsSuccess) {
							downloadExcelPage.location.href = response.Message;
						} else {
							downloadExcelPage.close();
							_COMMON.noty("error", "Error", 500);
						}
					}
				});
			});
		});
	</script>
</asp:Content>
<asp:Content runat="server" ContentPlaceHolderID="plActionButtons">
</asp:Content>