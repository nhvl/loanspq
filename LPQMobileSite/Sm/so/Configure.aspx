﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="Configure.aspx.vb" Inherits="Sm_so_Configure" MasterPageFile="../MasterPage.master" %>

<asp:Content runat="server" ContentPlaceHolderID="plBody">
	<div>
		<h1 class="page-header">Configuration</h1>
		<p>Changes are made from this section will be applied to all portals and only Super Operators and Legacy Operators can access this section.</p>
		
		<%--<section class="migrate-settings" data-name="MigrateShowFieldsData">
			<h2 class="section-title">Migrate Show-Fields Configuration Data For All Portals</h2>
			<p>Migrate Show-Fields Configuration Data For All Portals.</p>
			<button id="btnMigrateShowFieldsData" class="btn btn-warning mb10 mr5" data-loading-text="<i class='fa fa-circle-o-notch fa-spin'></i> Running - Please wait"><i class="fa fa-play" aria-hidden="true"></i>RUN BATCH - ALL PORTALS</button>
		</section>
		
		<section class="migrate-settings" data-name="MigrateHELocData">
			<h2 class="section-title">Migrate HELOC Configuration Data For All Portals</h2>
			<p>Copy disclosures from HOME_EQUITY_LOC to HOME_EQUITY_LOAN/DICLOSURES_LOC and delete HOME_EQUITY_LOC/DISCLOSURES. Effect to all portals.</p>
			<button id="btnMigrateHELocData" class="btn btn-warning mb10 mr5" data-loading-text="<i class='fa fa-circle-o-notch fa-spin'></i> Running - Please wait"><i class="fa fa-play" aria-hidden="true"></i>RUN BATCH - ALL PORTALS</button>
		</section>
		
		<section class="migrate-settings" data-name="MigrateLenderConfigUsers">
			<h2 class="section-title">Migrate Data of LenderConfigUsers table to UserRoles table</h2>
			<p>Copy and translate all data of LenderConfigUsers table to UserRoles table..</p>
			<button id="btnMigrateLenderConfigUsers" class="btn btn-warning mb10 mr5" data-loading-text="<i class='fa fa-circle-o-notch fa-spin'></i> Running - Please wait"><i class="fa fa-play" aria-hidden="true"></i>RUN BATCH - ALL PORTALS</button>
		</section>--%>
		
		<section class="migrate-settings" data-name="EncryptLenderConfigPasswords">
			<h2 class="section-title">Encrypt Passwords</h2>
			<p>Encrypt all password stored in lender config xml</p>
			<button id="btnEncryptLenderConfigPasswords" class="btn btn-warning mb10 mr5" data-loading-text="<i class='fa fa-circle-o-notch fa-spin'></i> Running - Please wait"><i class="fa fa-play" aria-hidden="true"></i>RUN BATCH - ALL PORTALS</button>
		</section>
		<section class="migrate-settings" data-name="EncryptLenderConfigLogsPasswords">
			<h2 class="section-title">Encrypt Passwords And Compress Records in LenderConfigLogs</h2>
			<div>There are <strong id="stUnEncryptedPasswordCounter"><%=UnEncryptedPasswordRecordCounter%></strong> log records need to process</div>
			<p>Each time run this command. <%=NumberOfLogItemWillBeProcessed %> latest records will be processed.</p>
			<button id="btnEncryptLenderConfigLogsPasswords" class="btn btn-warning mb10 mr5" data-loading-text="<i class='fa fa-circle-o-notch fa-spin'></i> Running - Please wait"><i class="fa fa-play" aria-hidden="true"></i>RUN COMMAND</button>
		</section>
		<%--<section class="migrate-settings" data-name="CompressLenderConfigLogs">
			<h2 class="section-title">Compress LenderConfigLogs Data</h2>
			<p>All record of LenderConfigLogs will be compressed to reduce the allocated space in database</p>
			<button id="btnCompressLenderConfigLogs" class="btn btn-warning mb10 mr5" data-loading-text="<i class='fa fa-circle-o-notch fa-spin'></i> Running - Please wait"><i class="fa fa-play" aria-hidden="true"></i>RUN BATCH - ALL PORTALS</button>
		</section>--%>
		<section class="migrate-settings" data-name="CalculateAuditChanges">
			<h2 class="section-title">Calculate Audit Log Changes</h2>
			<div>There are <strong id="stUnDiffedAudits"><%=UnDiffedAuditLogsCounter %></strong> audit logs/modification history entries from the last 1 year that need to be processed.</div>
			<p>Each time this command is run, <%=NumberOfLogItemWillBeProcessed %> of the latest records will be processed.</p>
			<button id="btnCalculateAuditChanges" class="btn btn-warning mb10 mr5" data-loading-text="<i class='fa fa-circle-o-notch fa-spin'></i> Running - Please wait"><i class="fa fa-play" aria-hidden="true"></i>RUN COMMAND</button>
		</section>
	</div>
</asp:Content>
<asp:Content runat="server" ContentPlaceHolderID="plScripts">
	<script type="text/javascript">
		$(function () {
			/*$("#btnMigrateShowFieldsData").on("click", function () {
				_COMMON.showConfirmDialog("Are you sure you want to perform this action? <br/> By doing this, all data related to show-fields will be changed and effect to all portals.", function () {
					//yes
					var dataObj = {};
					$("#btnMigrateShowFieldsData").button("loading");
					dataObj.command = "runmigratebatchallportals";
					$.ajax({
						url: "configure.aspx",
						async: true,
						cache: false,
						type: 'POST',
						dataType: 'html',
						data: dataObj,
						success: function (responseText) {
							var response = $.parseJSON(responseText);
							if (response.IsSuccess) {
								_COMMON.noty("success", "Success.", 500);
							} else {
								_COMMON.noty("error", "Error", 500);
							}
							$("#btnMigrateShowFieldsData").button("reset");
						}
					});
				}, function () {//no
				}, "Yes, do it", "Cancel");
			});

			$("#btnMigrateLenderConfigUsers").on("click", function () {
				_COMMON.showConfirmDialog("Are you sure you want to perform this action? <br/> By doing this, all data of LenderConfigUsers will be copied and translate to UserRoles table.", function () {
					//yes
					var dataObj = {};
					$("#btnMigrateLenderConfigUsers").button("loading");
					dataObj.command = "runmigrateLenderConfigUsers";
					$.ajax({
						url: "configure.aspx",
						async: true,
						cache: false,
						type: 'POST',
						dataType: 'html',
						data: dataObj,
						success: function (responseText) {
							var response = $.parseJSON(responseText);
							if (response.IsSuccess) {
								_COMMON.noty("success", "Success.", 500);
							} else {
								_COMMON.noty("error", response.Message, 500);
							}
							$("#btnMigrateLenderConfigUsers").button("reset");
						}
					});
				}, function () {//no
				}, "Yes, do it", "Cancel");
			});

			$("#btnMigrateHELocData").on("click", function () {
				_COMMON.showConfirmDialog("Are you sure you want to perform this action? <br/> By doing this, all disclosures from HOME_EQUITY_LOC will be copied to HOME_EQUITY_LOAN/DICLOSURES_LOC and delete HOME_EQUITY_LOC/DISCLOSURES. This action will effect to all portals.", function () {
					//yes
					var dataObj = {};
					$("#btnMigrateHELocData").button("loading");
					dataObj.command = "runmigrateHeLocbatchallportals";
					$.ajax({
						url: "configure.aspx",
						async: true,
						cache: false,
						type: 'POST',
						dataType: 'html',
						data: dataObj,
						success: function (responseText) {
							var response = $.parseJSON(responseText);
							if (response.IsSuccess) {
								_COMMON.noty("success", "Success.", 500);
							} else {
								_COMMON.noty("error", "Error", 500);
							}
							$("#btnMigrateHELocData").button("reset");
						}
					});
				}, function () {//no
				}, "Yes, do it", "Cancel");
			});*/
			$("#btnEncryptLenderConfigPasswords").on("click", function () {
				_COMMON.showConfirmDialog("Are you sure you want to perform this action? <br/> By doing this, all passwords in lender config xml will be encrypted. This action will effect to all portals.", function () {
					//yes
					var dataObj = {};
					$("#btnEncryptLenderConfigPasswords").button("loading");
					dataObj.command = "runencryptPasswords";
					$.ajax({
						url: "configure.aspx",
						async: true,
						cache: false,
						type: 'POST',
						dataType: 'html',
						data: dataObj,
						success: function (responseText) {
							var response = $.parseJSON(responseText);
							if (response.IsSuccess) {
								_COMMON.noty("success", "Success.", 500);
							} else {
								_COMMON.noty("error", "Error", 500);
							}
							$("#btnEncryptLenderConfigPasswords").button("reset");
						}
					});
				}, function () {//no
				}, "Yes, do it", "Cancel");
			});
			$("#btnEncryptLenderConfigLogsPasswords").on("click", function () {
				_COMMON.showConfirmDialog("Are you sure you want to perform this action? <br/> By doing this, all passwords of LenderConfigLog record will be encrypted and Config Data will be compressed.", function () {
					//yes
					var dataObj = {};
					$("#btnEncryptLenderConfigLogsPasswords").button("loading");
					dataObj.command = "runEncryptPasswordsOfLatestNLogRecords";
					$.ajax({
						url: "configure.aspx",
						async: true,
						cache: false,
						type: 'POST',
						dataType: 'html',
						data: dataObj,
						success: function (responseText) {
							var response = $.parseJSON(responseText);
							if (response.IsSuccess) {
								_COMMON.noty("success", "Success.", 500);
								$("#stUnEncryptedPasswordCounter").text(response.Info.count);
							} else {
								_COMMON.noty("error", "Error", 500);
							}
							$("#btnEncryptLenderConfigLogsPasswords").button("reset");
						}
					});
				}, function () {//no
				}, "Yes, do it", "Cancel");
			});

			//$("#btnCompressLenderConfigLogs").on("click", function () {
			//	_COMMON.showConfirmDialog("Are you sure you want to perform this action? <br/> By doing this, all lenderconfiglogs records will be compressed. This action will effect to all portals.", function () {
			//		//yes
			//		var dataObj = {};
			//		$("#btnCompressLenderConfigLogs").button("loading");
			//		dataObj.command = "compressLenderConfigLogs";
			//		$.ajax({
			//			url: "configure.aspx",
			//			async: true,
			//			cache: false,
			//			type: 'POST',
			//			dataType: 'html',
			//			data: dataObj,
			//			success: function (responseText) {
			//				var response = $.parseJSON(responseText);
			//				if (response.IsSuccess) {
			//					_COMMON.noty("success", "Success.", 500);
			//				} else {
			//					_COMMON.noty("error", "Error", 500);
			//				}
			//				$("#btnCompressLenderConfigLogs").button("reset");
			//			}
			//		});
			//	}, function () {//no
			//	}, "Yes, do it", "Cancel");
			//});

			$("#btnCalculateAuditChanges").on("click", function () {
				_COMMON.showConfirmDialog("Are you sure you want to perform this action? <br/> By doing this, <%=NumberOfLogItemWillBeProcessed%> audit logs will be processed to calculate their differences.", function () {
					//yes
					var dataObj = {};
					$("#btnCalculateAuditChanges").button("loading");
					dataObj.command = "runCalculateAuditChanges";
					$.ajax({
						url: "configure.aspx",
						async: true,
						cache: false,
						type: 'POST',
						dataType: 'html',
						data: dataObj,
						success: function (responseText) {
							var response = $.parseJSON(responseText);
							if (response.IsSuccess) {
								_COMMON.noty("success", "Success.", 500);
								$("#stUnDiffedAudits").text(response.Info.count);
							} else {
								_COMMON.noty("error", "Error", 500);
							}
							$("#btnCalculateAuditChanges").button("reset");
						}
					});
				}, function () {//no
				}, "Yes, do it", "Cancel");
			});
		});

	</script>
</asp:Content>
<asp:Content runat="server" ContentPlaceHolderID="plActionButtons">
</asp:Content>