﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="ManageLogs.aspx.vb" Inherits="Sm_so_ManageLogs" MasterPageFile="../MasterPage.master" %>

<asp:Content runat="server" ContentPlaceHolderID="plBody">
	<div>
		<h1 class="page-header">History</h1>
		<p>Manage all action logs.  (Beta, only available to legacy operator.)</p>
		<div class="row">
			<div class="col-sm-2 form-group">
				<label for="txtToDate">From Date</label>
				<div class='input-group date' id="txtFromDate">
                    <input type='text' class="form-control" placeholder="From Date"/>
                    <span class="input-group-addon">
                    <span class="glyphicon glyphicon-calendar"></span>
                    </span>
                </div>
			</div>
			<div class="col-sm-2 form-group">
				<label for="txtToDate">To Date</label>
				<div class='input-group date' id="txtToDate">
                    <input type='text' class="form-control" placeholder="To Date"/>
                    <span class="input-group-addon">
                    <span class="glyphicon glyphicon-calendar"></span>
                    </span>
                </div>
			</div>
			<div class="col-sm-2 form-group">
				<label for="txtIpAddress">IP address</label>
				<input type="text" class="form-control" id="txtIpAddress" placeholder="IP Address">
			</div>
			<div class="col-sm-4 form-group">
				<label for="txtKeywork">Keyword</label>
				<input type="text" class="form-control" id="txtKeyword" placeholder="@[Column Name]: keyword for searching in specified column">
			</div>
			<div class="col-sm-2 form-group">
				<button type="button" id="btnSearch" class="btn btn-primary mt25">Search</button>
			</div>
		</div>
        <div class="grid" id="divLenderList">
			<div class="spinner">
				<div class="rect1"></div>
				<div class="rect2"></div>
				<div class="rect3"></div>
				<div class="rect4"></div>
				<div class="rect5"></div>
			</div>
			<p>Loading...</p>
		</div>
	</div>
</asp:Content>
<asp:Content runat="server" ContentPlaceHolderID="plScripts">
	<%--<script type="text/javascript" src="../js/bootstrap/bootstrap-datetimepicker.min.js"></script>--%>
	<script type="text/javascript">
		_COMMON_PAGE_SIZE = 20;

		$(function () {
			var actionLogGrid;
			actionLogGrid = new _COMMON.Grid("/sm/smhandler.aspx", "actionLogGrid", "divLenderList", manageLogs.FACTORY, manageLogs.FACTORY.onLoaded, { command: "loadActionLogGrid" });
			actionLogGrid.init();
			manageLogs.FACTORY.init(actionLogGrid.GridObject);
			$("#btnSearch").on("click", manageLogs.FACTORY.search);
		});
		(function (manageLogs, $, undefined) {
			manageLogs.DATA = {};
			manageLogs.FACTORY = {};
			manageLogs.FACTORY.init = function (gridInstance) {
				var self = this;
				manageLogs.DATA.grid = gridInstance;
				$('#txtFromDate, #txtToDate').datepicker();
			};
			manageLogs.FACTORY.getFilterInfo = function () {
				//implement this later
				return {
					FromDate: $.trim($("#txtFromDate input").val()),
					ToDate: $.trim($("#txtToDate input").val()),
					IPAddress: $.trim($("#txtIpAddress").val()),
					Keyword: $.trim($("#txtKeyword").val())
				};
			};
			manageLogs.FACTORY.onLoaded = function () {
				
			};
			manageLogs.FACTORY.search = function () {
				manageLogs.DATA.grid.setFilter(manageLogs.FACTORY.getFilterInfo()).setPageIndex(1).load();
			};
		}(window.manageLogs = window.manageLogs || {}, jQuery));
	</script>
</asp:Content>
<asp:Content runat="server" ContentPlaceHolderID="plActionButtons">
</asp:Content>