﻿
Imports System.Xml
Imports Sm
Partial Class Sm_ManageSpecialXA
	Inherits SmApmBasePage
	Protected Property EligibilityMessage As String
	Protected Property ApprovalMessage As String
	Protected Property ReferredMessage As String
	Protected Property DeclinedMessage As String

	Protected Property AdditionalDisclosureList As New List(Of String)
	Protected Property FundingDislosure As String

	Private _log As log4net.ILog = log4net.LogManager.GetLogger(Me.GetType)

	Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
		If Not Page.IsPostBack Then
			Dim smMasterPage = CType(Me.Master, Sm_SiteManager)
			smMasterPage.LenderConfigXml = LenderConfigXml
			smMasterPage.RevisionID = LenderConfig.RevisionID
			smMasterPage.LastModifiedBy = LenderConfig.LastModifiedByUserFullName
			smMasterPage.LastModifiedByUserID = LenderConfig.LastModifiedByUserID
			smMasterPage.SelectedMainMenuItem = "manage"
			smMasterPage.SelectedSubMenuItem = "xa"
			smMasterPage.CurrentNodeGroup = SmSettings.NodeGroup.ManageSpecialXA
			smMasterPage.BreadCrumb = String.Format("Manage / Xpress Account (new {0})", IIf(IsBankPortal, "account", "member"))

			Dim eligibilityMessageNode As XmlNode = GetNode(XA_SPECIAL_ACCOUNT_ELIGIBILITY_MESSAGE())
			If eligibilityMessageNode IsNot Nothing AndAlso eligibilityMessageNode.NodeType <> XmlNodeType.Comment Then
				EligibilityMessage = eligibilityMessageNode.InnerText
			End If

			Dim preapprovedMessageNode As XmlNode = GetNode(XA_SPECIAL_ACCOUNT_PREAPPROVED_MESSAGE())
			If preapprovedMessageNode IsNot Nothing AndAlso preapprovedMessageNode.NodeType <> XmlNodeType.Comment Then
				ApprovalMessage = SmUtil.MergeMessageKey(preapprovedMessageNode.Attributes("message").InnerText, preapprovedMessageNode)
			End If

			Dim submittedMessageNode As XmlNode = GetNode(XA_SPECIAL_ACCOUNT_SUBMITTED_MESSAGE())
			If submittedMessageNode IsNot Nothing AndAlso submittedMessageNode.NodeType <> XmlNodeType.Comment Then
				ReferredMessage = SmUtil.MergeMessageKey(submittedMessageNode.Attributes("message").InnerText, submittedMessageNode)
			End If
			Dim disclosuresNode As XmlNode = GetNode(XA_SPECIAL_ACCOUNT_DISCLOSURES())
			If disclosuresNode IsNot Nothing AndAlso disclosuresNode.HasChildNodes Then
				For Each node As XmlNode In disclosuresNode.ChildNodes
					If node.NodeType = XmlNodeType.Comment Then Continue For
					AdditionalDisclosureList.Add(node.InnerText.Trim())
				Next
			End If
			Dim fundingDisclosureNode As XmlNode = GetNode(XA_SPECIAL_ACCOUNT_FUNDING_DISCLOSURE())
			If fundingDisclosureNode IsNot Nothing AndAlso fundingDisclosureNode.NodeType <> XmlNodeType.Comment Then
				FundingDislosure = fundingDisclosureNode.InnerText
			End If
		End If
	End Sub
End Class

