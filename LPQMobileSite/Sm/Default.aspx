﻿<%@ Page Language="VB" AutoEventWireup="false" MasterPageFile="MasterPage.master" CodeFile="Default.aspx.vb" Inherits="Sm_Default" %>

<asp:Content runat="server" ContentPlaceHolderID="plBody">
	<div class="homepage operator-context">
		<div class="top-section">
			<div>
				<img src="/Sm/content/images/logo.png"/>
				<h1>Welcome to the Application Portal Manager</h1>
				<p>Manage the design and configuration of your application portal</p>
			</div>
		</div>
		<div class="broadcast-section">
			<%If SmUtil.CheckRoleAccess(HttpContext.Current, SmSettings.Role.SuperUser.ToString()) Then%>
			<div class="html-editor min-height-300" contenteditable="true" data-required="true" id="txtBroadcastMsg"><%=IIf(String.IsNullOrEmpty(BroadcastMsg.Trim()), "Enter your messages...", BroadcastMsg)%></div>
			<div><button class="btn btn-secondary" type="button" id="btnSave">Save</button></div>
			<%ElseIf Not String.IsNullOrEmpty(BroadcastMsg.Trim()) Then%>
			<div class="viewer min-height-300"><%=BroadcastMsg%></div>
			<%End If%>
		</div>
	</div>
</asp:Content>
<asp:Content runat="server" ContentPlaceHolderID="plScripts">
	<script type="text/javascript" src="js/ckeditor/ckeditor.js"></script>
	<script type="text/javascript">
		CKEDITOR.disableAutoInline = true;
		$(function () {
			$(".html-editor").ckEditor({
				onContentChanged: function (editor) { master.FACTORY.documentChanged(editor); },
				onCommandClicked: function (editor, commandBtn) { master.FACTORY.documentChanged(editor); }
			});
			$("#btnSave").on("click", function() {
				var dataObj = collectSubmitData();
				dataObj.command = 'saveBroadcast';
				$.ajax({
					url: "/sm/smhandler.aspx",
					async: true,
					cache: false,
					type: 'POST',
					dataType: 'html',
					data: dataObj,
					success: function (responseText) {
						var response = $.parseJSON(responseText);
						if (response.IsSuccess) {
							master.FACTORY.resetDocumentChanged();
							_COMMON.noty("success", "Broadcast successfully.", 500);
						} else {
							_COMMON.noty("error", "Error", 500);
						}
					}
				});
			});
		});
		(function (master, $, undefined) {
			
		}(window.master = window.master || {}, jQuery));
		function collectSubmitData() {
			var result = {};
			result.content = _COMMON.getEditorValue("txtBroadcastMsg");
			return result;
		}
	</script>
</asp:Content>