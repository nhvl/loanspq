﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="BLSettings.aspx.vb" Inherits="Sm_BLSettings" MasterPageFile="SiteManager.master"  %>

<asp:Content runat="server" ContentPlaceHolderID="plBody">
	<div>
		<h1 class="page-header">Settings</h1>
		<p>Configure settings for your Application Portal</p>
		<section>
			<ul class="nav nav-tabs" id="mainTabs" role="tablist">
				<li role="presentation"><a href="<%=BuildUrl("/sm/generalsettings.aspx")%>" role="tab">General</a></li>
				<li role="presentation" <%=IIf(EnableXA, "", "class='hidden'")%>><a href="<%=BuildUrl("/sm/xasettings.aspx")%>" role="tab">XA</a></li>
				<li role="presentation" <%=IIf(EnableCC Or EnableHE Or EnablePL Or EnableVL, "", "class='hidden'")%>><a href="<%=BuildUrl("/sm/loanssettings.aspx")%>" role="tab">All Loans</a></li>
				<li role="presentation" <%=IIf(EnableCC, "", "class='hidden'")%>><a href="<%=BuildUrl("/sm/ccsettings.aspx")%>" role="tab">Credit Card Loan</a></li>
				<li role="presentation" <%=IIf(EnableHE, "", "class='hidden'")%>><a href="<%=BuildUrl("/sm/hesettings.aspx")%>" role="tab">Home Equity Loan</a></li>
				<li role="presentation" <%=IIf(EnableLQB, "", "class='hidden'")%>><a href="<%=BuildUrl("/sm/mlsettings.aspx")%>" role="tab">Mortgage Loan</a></li>
				<li role="presentation" <%=IIf(EnablePL, "", "class='hidden'")%>><a href="<%=BuildUrl("/sm/plsettings.aspx")%>" role="tab">Personal Loan</a></li>
				<li role="presentation" <%=IIf(EnableVL, "", "class='hidden'")%>><a href="<%=BuildUrl("/sm/vlsettings.aspx")%>" role="tab">Vehicle Loan</a></li>
				<li role="presentation" class="active"><a href="#" aria-controls="blloans" role="tab">Business Loan</a></li>
			</ul>
			<div class="tab-content">
				<%If EnableBL Then%>
				<div role="tabpanel" class="tab-pane active" id="blloans">
					<h2 class="section-title">Business Loans</h2>
					<p>These settings will only effect Business Loan applications.</p>
					<div class="bottom30">
						<div class="group-heading">INTEGRATIONS</div>
						<div class="input-block">
							<h3 class="property-title">Document Upload</h3>
							<p>Allow your applicants to capture/upload images or documents</p>
							<label class="toggle-btn <%=BindCheckbox(UploadDocEnable)%>" data-on="ON" data-off="OFF">
								<input type="checkbox" id="chkUploadDocEnable" value="" <%=BindCheckbox(UploadDocEnable)%>/>
								<span class="button-checkbox"></span>
							</label>
							<div class="hint-content">
								<p>Message (required for feature to show)</p>
								<div class="html-editor" id="uploadDocMsg" contenteditable="true" ><%=UploadDocMsg%></div>
							</div>
						</div>
					</div>
                     <div class="bottom30">
                       <div class="input-block">
							<h3 class="property-title">Previous Address</h3>
							<p>Require previous address if duration at current address does not meet a specified minimum duration.</p>
							<label class="toggle-btn" data-on="ON" data-off="OFF">
								<input type="checkbox" id="chkBLPreviousAddress" value="" <%=BindCheckbox(BLPreviousAddressEnable)%>/>
								<span class="button-checkbox"></span>
							</label>
						</div>
                       <div class="input-block <%=IIf(BLPreviousAddressEnable, "", "hidden")%>">						
						    <p>Minimum Duration (months)</p>
						    <input type="text" class="form-control number" id="txtBLPreviousAddressThreshold" value="<%=BLPreviousAddressThreshold%>" maxlength ="3" />							
					    </div>
                    </div>
                   
                     <div class="bottom30">
                       <div class="input-block">
							<h3 class="property-title">Previous Employment</h3>
							<p>Require previous employment if duration at current employment does not meet a specified minimum duration.</p>
							<label class="toggle-btn" data-on="ON" data-off="OFF">
								<input type="checkbox" id="chkBLPreviousEmployment" value="" <%=BindCheckbox(BLPreviousEmploymentEnable)%>/>
								<span class="button-checkbox"></span>
							</label>
						</div>
                       <div class="input-block <%=IIf(BLPreviousEmploymentEnable, "", "hidden")%>" >						
						    <p>Minimum Duration (months)</p>
						    <input type="text" class="form-control number" id="txtBLPreviousEmploymentThreshold" value="<%=BLPreviousEmploymentThreshold%>" maxlength="3"/>							
					   </div>
                    </div>
				</div>
				<%End If%>
			</div>
		</section>
	</div>
</asp:Content>
<asp:Content runat="server" ContentPlaceHolderID="plScripts">
	<script type="text/javascript" src="js/ckeditor/ckeditor.js"></script>
	<script type="text/javascript">
		CKEDITOR.disableAutoInline = true;
		$(function () {
			registerDataValidator();
			$("select,input:checkbox").on("change", function () {
				master.FACTORY.documentChanged(this);
			});
			$(".html-editor").ckEditor({
				onContentChanged: function (editor) { master.FACTORY.documentChanged(editor); },
				onCommandClicked: function (editor, commandBtn) { master.FACTORY.documentChanged(editor); }
			});
			$("input:text").each(function (idx, ele) {
				$(ele).on({
					keypress: function () { master.FACTORY.documentChanged(ele); },
					paste: function () { master.FACTORY.documentChanged(ele); },
					cut: function () { master.FACTORY.documentChanged(ele); }
				});
			});
			$("#chkUploadDocEnable").on("change", function () {
				var $self = $(this);
				if ($self.is(":checked")) {
					$self.closest(".toggle-btn").addClass("checked");
				} else {
					$self.closest(".toggle-btn").removeClass("checked");
				}
            });
               //show minimum address duration input field only when the chkPreviousAddress is on 
            $("#chkBLPreviousAddress").on("change", function () {
                var $self = $(this);
                var $previousAddressThreshold = $('#txtBLPreviousAddressThreshold');
                if ($self.is(":checked")) {
                    $previousAddressThreshold.closest('div').removeClass('hidden');
                } else {
                    //clear and hide the field
                    $previousAddressThreshold.val(""); 
                    $previousAddressThreshold.closest('div').addClass('hidden');
                }
            });
            
            //show minimum employment duration input field only when the chkPreviousEmployement is on 
            $("#chkBLPreviousEmployment").on("change", function () {
                var $self = $(this);
                var $previousEmploymentThreshold = $('#txtBLPreviousEmploymentThreshold');
                if ($self.is(":checked")) {
                    $previousEmploymentThreshold.closest('div').removeClass('hidden');
                } else {
                    //clear and hide the field
                    $previousEmploymentThreshold.val(""); 
                    $previousEmploymentThreshold.closest('div').addClass('hidden');
                }
            });
		});
		(function (master, $, undefined) {
			master.FACTORY.SaveDraft = function (revisionId) {
				if ($.smValidate("ValidateData") == false) {
					_COMMON.noty("error", "Error: please check all fields.", 500);
					return;
				}

				var dataObj = collectSubmitData();
				dataObj.revisionId = revisionId;
				dataObj.command = 'saveblsettings';
				$.ajax({
					url: "/sm/smhandler.aspx",
					async: true,
					cache: false,
					type: 'POST',
					dataType: 'html',
					data: dataObj,
					success: function (responseText) {
						var response = $.parseJSON(responseText);
						if (response.IsSuccess) {
							master.FACTORY.onSaveDraftSuccess();
						} else {
							_COMMON.noty("error", "Error", 500);
						}
					}
				});
			};
			master.FACTORY.Preview = function (revisionId) {
				if ($.smValidate("ValidateData") == false) {
					_COMMON.noty("error", "Error: please check all fields.", 500);
					return;
				}
				var dataObj = collectSubmitData();
				dataObj.revisionId = revisionId;
				dataObj.command = 'previewblsettings';
				$.ajax({
					url: "/sm/smhandler.aspx",
					async: true,
					cache: false,
					type: 'POST',
					dataType: 'html',
					data: dataObj,
					success: function (responseText) {
						var response = $.parseJSON(responseText);
						if (response.IsSuccess) {
							master.FACTORY.onPreviewSuccess();
							_COMMON.openInNewTab(response.Info.previewurl);
						} else {
							_COMMON.noty("error", "Error", 500);
						}
					}
				});
			};
			master.FACTORY.validateBeforePublish = function () {
				var result = $.smValidate("ValidateData");
				if (result == false) {
					_COMMON.noty("error", "Error: please check all fields.", 500);
				}
				return result;
			}
			master.FACTORY.Publish = function (comment, publishAll) {
				if ($.smValidate("ValidateData") == false) {
					_COMMON.noty("error", "Error: please check all fields.", 500);
					return;
				}
				var dataObj = collectSubmitData();
				dataObj.command = 'publishblsettings';
				dataObj.comment = comment;
				dataObj.publishAll = publishAll;
				$.ajax({
					url: "/sm/smhandler.aspx",
					async: true,
					cache: false,
					type: 'POST',
					dataType: 'html',
					data: dataObj,
					success: function (responseText) {
						var response = $.parseJSON(responseText);
						if (response.IsSuccess) {
							master.FACTORY.onPublishSuccess();
						} else {
							_COMMON.noty("error", "Error", 500);
						}
					}
				});
			};
		}(window.master = window.master || {}, jQuery));
		function registerDataValidator() {
			$.smValidate.removeValidationGroup("ValidateData");
			$("input.float-number").each(function (idx, input) {
				$(input).observer({
					validators: [
						function (partial) {
							var $self = $(this);
							if (/^[0-9]+(\.[0-9]+)*$/.test($self.val()) == false) {
								return "Invalid data";
							}
							return "";
						}
					],
					validateOnBlur: true,
					group: "ValidateData"
				});
			});
              //validate minimum employment duration
            $('#txtBLPreviousEmploymentThreshold').observer({
					validators: [
						function (partial) {
                            var $self = $(this);
                            if ($self.closest('div').hasClass('hidden')) return ""; //skip validating if the field is hidden
						    if (($self.val().trim() !="" && /^[0-9]+$/.test($self.val()) == false) ||parseInt($self.val()) >= 120) {
								return "Invalid minimum duration";
							}
							return "";
						}
					],
					validateOnBlur: true,
					group: "ValidateData"
            });
             //validate minimum address duration
            $('#txtBLPreviousAddressThreshold').observer({
					validators: [
						function (partial) {
                            var $self = $(this);
                           if ($self.closest('div').hasClass('hidden')) return ""; //skip validating if the field is hidden							
                           if (($self.val().trim() !="" && /^[0-9]+$/.test($self.val()) == false) ||parseInt($self.val()) >= 120) {
								return "Invalid minimum duration";
						    }
							return "";
						}
					],
					validateOnBlur: true,
					group: "ValidateData"
			});
			$("#uploadDocMsg").observer({
				validators: [
					function (partial) {
						var $self = $(this);
						var content = CKEDITOR.instances[$self.attr("id")].getData();
						var decodedContent = $("<div />").html(content).text();
						if ($("#chkUploadDocEnable").is(":checked") && $.trim(decodedContent) === "") {
							return "A non-blank message is required when the document upload feature is active";
						}
						return "";
					}
				],
				validateOnBlur: true,
				group: "ValidateData"
			});
		}
		function collectSubmitData() {
			var result = {};
			result.lenderconfigid = '<%=LenderConfigID%>';
			result.upload_doc_enable = $("#chkUploadDocEnable").is(":checked");
            result.upload_doc_message = CKEDITOR.instances["uploadDocMsg"].getData();
            result.bl_previous_employment_threshold = $("#txtBLPreviousEmploymentThreshold").val();
            result.bl_previous_address_threshold = $("#txtBLPreviousAddressThreshold").val();
            result.bl_previous_address_enable = $('#chkBLPreviousAddress').is(":checked");
            result.bl_previous_employment_enable = $('#chkBLPreviousEmployment').is(":checked");
			return result;
		}
	</script>
</asp:Content>
<asp:Content runat="server" ContentPlaceHolderID="plActionButtons"></asp:Content>