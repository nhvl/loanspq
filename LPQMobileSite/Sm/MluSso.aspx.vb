﻿
Imports System.Net
Imports System.IO
Imports Newtonsoft.Json
Imports System.Xml

''' <summary>
''' This is the initial entry point for MLU SSO
''' 1) Determine user type: direct or sso 
''' 2) If user is SSO with email that is not confirmed, send email
''' 3) If uses is SSO with confirmed email, sso to MLU(if failed, create new mlu account and sso)
''' 4) If uses is direct login, sso to MLU(if failed, create new mlu account and sso)
''' </summary>
Partial Class Sm_MluSso
	Inherits SmBasePage

	
	Protected Sub Page_PreLoad(sender As Object, e As EventArgs) Handles Me.PreLoad
		Dim smMasterPage = CType(Me.Master, Sm_MasterPage)
		smMasterPage.SelectedMainMenuItem = "mlu"
		AllowedRoles.Add(SmSettings.Role.SuperUser.ToString())
		AllowedRoles.Add(SmSettings.Role.LenderAdmin.ToString())
		AllowedRoles.Add(SmSettings.Role.PortalAdmin.ToString())
		AllowedRoles.Add(SmSettings.Role.VendorGroupAdmin.ToString())
		AllowedRoles.Add(SmSettings.Role.SSOUser.ToString())
		NotAllowedAccessRedirectUrl = "/Sm/Default.aspx"
	End Sub
	Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
		If Request.HttpMethod = "POST" Then
			Dim res As CJsonResponse
            If User.Identity.Name.Contains("@") OrElse (UserInfo.Role = SmSettings.Role.SSOUser AndAlso UserInfo.IsSSOEmailConfirmed = "Y") Then 'Direct login or sso with confirmed email
                'res = New CJsonResponse(True, "", SmMluHandler.MLU_SSO_URL)
                Dim smAuth As New SmAuthBL
                Dim SSOLoginDocRequest = SmMluHandler.PrepareDocRequest("sso", UserInfo)
                Dim redirectUrl As String = ""
                If SmMluHandler.PostRequest(SSOLoginDocRequest, redirectUrl) Then
                    res = New CJsonResponse(True, "", "https://" & redirectUrl)
                Else
                    'Login failed already, lets try creating user & sso again
                    Dim createUserAndSSODocRequest = SmMluHandler.PrepareDocRequest("create", UserInfo)
                    SmMluHandler.PostRequest(createUserAndSSODocRequest, "")

                    SSOLoginDocRequest = SmMluHandler.PrepareDocRequest("sso", UserInfo)
                    redirectUrl = ""
                    If SmMluHandler.PostRequest(SSOLoginDocRequest, redirectUrl) Then
                        res = New CJsonResponse(True, "", "https://" & redirectUrl)
                    Else
                        res = New CJsonResponse(False, "Unable process this request. Please contact APM's Adminstrator.", "")
                    End If
                End If
                Response.Write(JsonConvert.SerializeObject(res))
                Response.End()
                Return
            ElseIf UserInfo.Role = SmSettings.Role.SSOUser Then  'sso user with email not confirmed
                Dim smAuth As New SmAuthBL
                Dim result As String = smAuth.SendMluSsoUrl(HttpContext.Current, UserInfo, Request.ServerVariables("REMOTE_ADDR"), ServerRoot)
                If result = "ok" Then
                    res = New CJsonResponse(False, String.Format("New email address found. A confirmation email was sent to {0}.", UserInfo.SSOEmail), "C")
                    Response.Write(JsonConvert.SerializeObject(res))
                    Response.End()
                    Return
                End If

                res = New CJsonResponse(False, "Unable process this request. Please contact APM's Adminstrator.", "")
			End If
			Response.Write(JsonConvert.SerializeObject(res))
			Response.End()
		End If
	End Sub

	
End Class
