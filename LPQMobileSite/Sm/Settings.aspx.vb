﻿Imports Sm
Imports System.Xml

'DEPRECATED
'TODO: should be removed later
Partial Class Sm_Settings
	Inherits SmApmBasePage

	Protected Property LicenseScan As Boolean = False

	Protected Property DLBarcodeScan As Boolean = False

	Protected Property ForeignAddress As Boolean = False
	Protected Property ForeignPhone As Boolean = False

	Protected Property MfaEmailOnly As Boolean = False
	Protected Property LinkedInEnable As Boolean = False

	Protected Property AddressKey As Boolean = False

	Protected Property HomepageEditor As Boolean = False

	Protected Property XAUploadDocEnable As Boolean = True	'set this to true for backward compatible, featue only visible if both XAUploadDocEnable and XAUploadDocMsg <> ""
	Protected Property XAUploadDocMsg As String

	Protected Property UploadDocEnable As Boolean = True
	Protected Property UploadDocMsg As String

	Protected Property XAJointEnable As Boolean = True
	Protected Property SAJointEnable As Boolean = True

	Protected Property XALocationPool As Boolean = False

	Protected Property CCJointEnable As Boolean = True

	Protected Property HEJointEnable As Boolean = True

	Protected Property PLJointEnable As Boolean = True

    Protected Property VLJointEnable As Boolean = True

	Protected Property VLTermFreeTextMode As Boolean = False

    Protected Property VLVinBarcodeScan As Boolean = False

    Protected Property Debit As String
    Protected Property Ida As String

    Protected Property CreditPull As Boolean = False

    Protected Property DecisionXAEnabled As Boolean = False

    'TODO: get config from xml
    Protected Property XALoanStatus As String
    Protected Property XAFunding As Boolean = False
    Protected Property XABooking As String

    Protected Property XAXSell As Boolean = False
    Protected Property XADocuSign As Boolean = False
    Protected Property XADocuSignPDFGroup As String

    '--
    Protected Property PLBooking As String
    Protected Property PLXSell As Boolean = False
    Protected Property PLDocuSign As Boolean = False
    Protected Property PLDocuSignPDFGroup As String

    Protected Property CCBooking As String
    Protected Property CCXSell As Boolean = False
    Protected Property CCDocuSign As Boolean = False
    Protected Property CCDocuSignPDFGroup As String

    Protected Property VLBooking As String
    Protected Property VLXSell As Boolean = False
    Protected Property VLDocuSign As Boolean = False
    Protected Property VLDocuSignPDFGroup As String

    Protected Property HEBooking As String
    Protected Property HEXSell As Boolean = False
    Protected Property HEDocuSign As Boolean = False
    Protected Property HEDocuSignPDFGroup As String


    Protected Property PLInterestRate As String = "2.5"
    Protected Property VLInterestRate As String = "2.5"
    Protected Property HEInterestRate As String = "2.5"

    Protected Property DefaultPreviewModuleName As String
	Protected Property DefaultPreviewLoanName As String

	Protected Property GoogleTagManagerId As String = ""
	Protected Property PiwikSiteId As String = ""

    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then
            Dim smMasterPage = CType(Me.Master, Sm_SiteManager)
            smMasterPage.LenderConfigXml = LenderConfigXml
			smMasterPage.RevisionID = LenderConfig.RevisionID
			smMasterPage.LastModifiedBy = LenderConfig.LastModifiedByUserFullName
			smMasterPage.LastModifiedByUserID = LenderConfig.LastModifiedByUserID
            smMasterPage.SelectedMainMenuItem = "config"
            smMasterPage.SelectedSubMenuItem = "settings"
            smMasterPage.CurrentNodeGroup = SmSettings.NodeGroup.Settings
			If ApmDLScanEnable Then
				Dim licenseNode As XmlNode = GetNode(LICENSE_SCAN)
				If licenseNode IsNot Nothing Then
					LicenseScan = Not String.IsNullOrEmpty(licenseNode.Value)
				End If
			End If

			If SmUtil.CheckRoleAccess(Context, SmSettings.Role.LegacyOperator, SmSettings.Role.SuperOperator) Then
				Dim dlBarCodeScanNode As XmlNode = GetNode(DL_BARCODE_SCAN())
				If dlBarCodeScanNode IsNot Nothing Then
					DLBarcodeScan = dlBarCodeScanNode.Value.ToUpper().Equals("Y")
				End If
			End If

			If ApmSiteAnalyticsEnable Then
				Dim gtmNode As XmlNode = GetNode(GOOGLE_TAG_MANAGER_ID)
				If gtmNode IsNot Nothing Then
					GoogleTagManagerId = gtmNode.Value
				End If

				Dim piwikSiteIdNode As XmlNode = GetNode(PIWIK_SITE_ID())
				If piwikSiteIdNode IsNot Nothing Then
					PiwikSiteId = piwikSiteIdNode.Value
				End If
			End If

			Dim foreignAddressNode As XmlNode = GetNode(FOREIGN_ADDRESS())
			If foreignAddressNode IsNot Nothing Then
				ForeignAddress = foreignAddressNode.Value.ToUpper().Equals("Y")
			End If

			Dim foreignPhoneNode As XmlNode = GetNode(FOREIGN_PHONE())
			If foreignPhoneNode IsNot Nothing Then
				ForeignPhone = foreignPhoneNode.Value.ToUpper().Equals("Y")
			End If

			Dim mfaEmailOnlyNode As XmlNode = GetNode(MFA_EMAIL_ONLY())
			If mfaEmailOnlyNode IsNot Nothing Then
				MfaEmailOnly = mfaEmailOnlyNode.Value.ToUpper().Equals("Y")
			End If
			If ApmLinkedInEnable Then
				Dim linkedInEnableNode As XmlNode = GetNode(LINKEDIN_ENABLE())
				If linkedInEnableNode IsNot Nothing Then
					LinkedInEnable = linkedInEnableNode.Value.ToUpper().Equals("Y")
				End If
			End If

			Dim addressKeyNode As XmlNode = GetNode(ADDRESS_KEY())
			If addressKeyNode IsNot Nothing Then
				AddressKey = Not String.IsNullOrEmpty(addressKeyNode.Value)
			End If
			If SmUtil.CheckRoleAccess(Context, SmSettings.Role.LegacyOperator, SmSettings.Role.SuperOperator) Then
				Dim homepageEditorNode As XmlNode = GetNode(HOMEPAGE_EDITOR())
				If homepageEditorNode IsNot Nothing Then
					HomepageEditor = homepageEditorNode.Value.ToUpper().Equals("Y")
				End If
			End If

			Dim xaUploadDocEnableNode As XmlNode = GetNode(XA_UPLOAD_DOC_ENABLE())
			If xaUploadDocEnableNode IsNot Nothing Then
				XAUploadDocEnable = Not xaUploadDocEnableNode.Value.ToUpper().Equals("N")
			End If

			Dim xaUploadDocNode As XmlNode = GetNode(XA_UPLOAD_DOC_MESSAGE())
			If xaUploadDocNode IsNot Nothing Then
				XAUploadDocMsg = xaUploadDocNode.InnerText.Trim()
			End If


			Dim loanUploadDocEnableNode As XmlNode = GetNode(UPLOAD_DOC_ENABLE())
			If loanUploadDocEnableNode IsNot Nothing Then
				UploadDocEnable = Not loanUploadDocEnableNode.Value.ToUpper().Equals("N")
			End If

			Dim loanUploadDocNode As XmlNode = GetNode(UPLOAD_DOC_MESSAGE())
			If loanUploadDocNode IsNot Nothing Then
				UploadDocMsg = loanUploadDocNode.InnerText.Trim()
			End If

			Dim xaJointEnableNode As XmlNode = GetNode(XA_JOINT_ENABLE())
			If xaJointEnableNode IsNot Nothing Then
				XAJointEnable = Not xaJointEnableNode.Value.ToUpper().Equals("N")
			End If

			Dim saJointEnableNode As XmlNode = GetNode(SA_JOINT_ENABLE())
			If saJointEnableNode IsNot Nothing Then
				SAJointEnable = Not saJointEnableNode.Value.ToUpper().Equals("N")
			Else
				'Note: if there is no joint_enable_sa the App Portal would also read from joint_enable to see if secondary accounts should allow joint
				SAJointEnable = XAJointEnable
			End If

			Dim xaLocationPoolNode As XmlNode = GetNode(XA_LOCATION_POOL())
			If xaLocationPoolNode IsNot Nothing Then
				XALocationPool = Not xaLocationPoolNode.Value.ToUpper().Equals("N")
			End If

			Dim debitNode As XmlNode = GetNode(Sm.DEBIT())
			If debitNode IsNot Nothing Then
				Debit = debitNode.Value
			End If
			Dim idaNode As XmlNode = GetNode(Sm.IDA())
			If idaNode IsNot Nothing Then
				Ida = idaNode.Value
			End If

			Dim creditPullNode As XmlNode = GetNode(CREDIT_PULL())
			If creditPullNode IsNot Nothing Then
				CreditPull = creditPullNode.Value.ToUpper().Equals("Y")
			End If

			Dim decisionXAEnabledNode As XmlNode = GetNode(DECISIONXA_ENABLE())
			If decisionXAEnabledNode IsNot Nothing Then
				DecisionXAEnabled = decisionXAEnabledNode.Value.ToUpper().Equals("Y")
			End If

			'---
			Dim XAStatusNode As XmlNode = GetNode(Sm.XA_STATUS())
			If XAStatusNode IsNot Nothing Then
				XALoanStatus = XAStatusNode.Value
			End If

			Dim fundingXAEnabledNode As XmlNode = GetNode(XA_FUNDING_ENABLE())
			If fundingXAEnabledNode IsNot Nothing Then
				XAFunding = fundingXAEnabledNode.Value.ToUpper().Equals("Y")
			End If

			Dim XABookingNode As XmlNode = GetNode(Sm.XA_BOOKING())
			If XABookingNode IsNot Nothing Then
				XABooking = XABookingNode.Value
			End If

			'Dim customListRateNode As XmlNode = GetNode(CUSTOM_LIST_RATES())

			If CheckEnabledModule(SmSettings.ModuleName.XA) Then

				Dim xsellXAEnabledNode As XmlNode = GetNode(XSELL("XA_LOAN"))
				If xsellXAEnabledNode IsNot Nothing Then
					XAXSell = xsellXAEnabledNode.Value.ToUpper().Equals("Y")
				End If

				Dim docusignXAEnabledNode As XmlNode = GetNode(DOCU_SIGN("XA_LOAN"))
				If docusignXAEnabledNode IsNot Nothing Then
					XADocuSign = docusignXAEnabledNode.Value.ToUpper().Equals("Y")
				End If

				Dim docusignPDFGroupNode As XmlNode = GetNode(DOCU_SIGN_PDF_GROUP("XA_LOAN"))
				If docusignPDFGroupNode IsNot Nothing Then
					XADocuSignPDFGroup = docusignPDFGroupNode.Value.Trim()
				End If
			End If
			If CheckEnabledModule(SmSettings.ModuleName.VL) Then
				''VL
				Dim VLBookingNode As XmlNode = GetNode(Sm.LOAN_BOOKING("VEHICLE_LOAN"))
				If VLBookingNode IsNot Nothing Then
					VLBooking = VLBookingNode.Value
				End If

				Dim VLxsellEnabledNode As XmlNode = GetNode(XSELL("VEHICLE_LOAN"))
				If VLxsellEnabledNode IsNot Nothing Then
					VLXSell = VLxsellEnabledNode.Value.ToUpper().Equals("Y")
				End If

				Dim VLdocusignEnabledNode As XmlNode = GetNode(DOCU_SIGN("VEHICLE_LOAN"))
				If VLdocusignEnabledNode IsNot Nothing Then
					VLDocuSign = VLdocusignEnabledNode.Value.ToUpper().Equals("Y")
				End If

				Dim VLdocusignPDFGroupNode As XmlNode = GetNode(DOCU_SIGN_PDF_GROUP("VEHICLE_LOAN"))
				If VLdocusignPDFGroupNode IsNot Nothing Then
					VLDocuSignPDFGroup = VLdocusignPDFGroupNode.Value.Trim()
				End If

				'If customListRateNode IsNot Nothing Then
				'	Dim vlRateNode As XmlNode = customListRateNode.SelectSingleNode("ITEM[@loan_type='vl']/@rate")
				'	If vlRateNode IsNot Nothing Then
				'		VLInterestRate = vlRateNode.Value
				'	End If
				'End If
			End If

			If CheckEnabledModule(SmSettings.ModuleName.CC) Then
				''CC
				Dim CCBookingNode As XmlNode = GetNode(Sm.LOAN_BOOKING("CREDIT_CARD_LOAN"))
				If CCBookingNode IsNot Nothing Then
					CCBooking = CCBookingNode.Value
				End If

				Dim CCxsellEnabledNode As XmlNode = GetNode(XSELL("CREDIT_CARD_LOAN"))
				If CCxsellEnabledNode IsNot Nothing Then
					CCXSell = CCxsellEnabledNode.Value.ToUpper().Equals("Y")
				End If

				Dim CCdocusignEnabledNode As XmlNode = GetNode(DOCU_SIGN("CREDIT_CARD_LOAN"))
				If CCdocusignEnabledNode IsNot Nothing Then
					CCDocuSign = CCdocusignEnabledNode.Value.ToUpper().Equals("Y")
				End If

				Dim CCdocusignPDFGroupNode As XmlNode = GetNode(DOCU_SIGN_PDF_GROUP("CREDIT_CARD_LOAN"))
				If CCdocusignPDFGroupNode IsNot Nothing Then
					CCDocuSignPDFGroup = CCdocusignPDFGroupNode.Value.Trim()
				End If
			End If
			If CheckEnabledModule(SmSettings.ModuleName.PL) Then
				''PL
				Dim PLBookingNode As XmlNode = GetNode(Sm.LOAN_BOOKING("PERSONAL_LOAN"))
				If PLBookingNode IsNot Nothing Then
					PLBooking = PLBookingNode.Value
				End If

				Dim PLxsellEnabledNode As XmlNode = GetNode(XSELL("PERSONAL_LOAN"))
				If PLxsellEnabledNode IsNot Nothing Then
					PLXSell = PLxsellEnabledNode.Value.ToUpper().Equals("Y")
				End If

				Dim PLdocusignEnabledNode As XmlNode = GetNode(DOCU_SIGN("PERSONAL_LOAN"))
				If PLdocusignEnabledNode IsNot Nothing Then
					PLDocuSign = PLdocusignEnabledNode.Value.ToUpper().Equals("Y")
				End If

				Dim PLdocusignPDFGroupNode As XmlNode = GetNode(DOCU_SIGN_PDF_GROUP("PERSONAL_LOAN"))
				If PLdocusignPDFGroupNode IsNot Nothing Then
					PLDocuSignPDFGroup = PLdocusignPDFGroupNode.Value.Trim()
				End If

				'If customListRateNode IsNot Nothing Then
				'	Dim plRateNode As XmlNode = customListRateNode.SelectSingleNode("ITEM[@loan_type='pl']/@rate")
				'	If plRateNode IsNot Nothing Then
				'		PLInterestRate = plRateNode.Value
				'	End If
				'End If
			End If
			If CheckEnabledModule(SmSettings.ModuleName.HE) Then
				''HE
				Dim HEBookingNode As XmlNode = GetNode(Sm.LOAN_BOOKING("HOME_EQUITY_LOAN"))
				If HEBookingNode IsNot Nothing Then
					HEBooking = HEBookingNode.Value
				End If

				Dim HExsellEnabledNode As XmlNode = GetNode(XSELL("HOME_EQUITY_LOAN"))
				If HExsellEnabledNode IsNot Nothing Then
					HEXSell = HExsellEnabledNode.Value.ToUpper().Equals("Y")
				End If

				Dim HEdocusignEnabledNode As XmlNode = GetNode(DOCU_SIGN("HOME_EQUITY_LOAN"))
				If HEdocusignEnabledNode IsNot Nothing Then
					HEDocuSign = HEdocusignEnabledNode.Value.ToUpper().Equals("Y")
				End If

				Dim HEdocusignPDFGroupNode As XmlNode = GetNode(DOCU_SIGN_PDF_GROUP("HOME_EQUITY_LOAN"))
				If HEdocusignPDFGroupNode IsNot Nothing Then
					HEDocuSignPDFGroup = HEdocusignPDFGroupNode.Value.Trim()
				End If
				'If customListRateNode IsNot Nothing Then
				'	Dim heRateNode As XmlNode = customListRateNode.SelectSingleNode("ITEM[@loan_type='he']/@rate")
				'	If heRateNode IsNot Nothing Then
				'		HEInterestRate = heRateNode.Value
				'	End If
				'End If
			End If
			''********************
			'work flow
			Dim ccJointEnableNode As XmlNode = GetNode(CC_JOINT_ENABLE())
			If ccJointEnableNode IsNot Nothing Then
				CCJointEnable = Not ccJointEnableNode.Value.ToUpper().Equals("N") 'default = Y
			End If

			Dim heJointEnableNode As XmlNode = GetNode(HE_JOINT_ENABLE())
			If heJointEnableNode IsNot Nothing Then
				HEJointEnable = Not heJointEnableNode.Value.ToUpper().Equals("N")
			End If

			Dim plJointEnableNode As XmlNode = GetNode(PL_JOINT_ENABLE())
			If plJointEnableNode IsNot Nothing Then
				PLJointEnable = Not plJointEnableNode.Value.ToUpper().Equals("N")
			End If

			Dim vlJointEnableNode As XmlNode = GetNode(VL_JOINT_ENABLE())
			If vlJointEnableNode IsNot Nothing Then
				VLJointEnable = Not vlJointEnableNode.Value.ToUpper().Equals("N")
			End If
			Dim vlAutoTermTextboxEnableNode As XmlNode = GetNode(VL_TERM_FREE_FORM_MODE())
			If vlAutoTermTextboxEnableNode IsNot Nothing Then
				VLTermFreeTextMode = vlAutoTermTextboxEnableNode.Value.ToUpper().Equals("Y")
			End If
			Dim vlVinBarcodeScanNode As XmlNode = GetNode(VIN_BARCODE_SCAN("VEHICLE_LOAN"))
			If vlVinBarcodeScanNode IsNot Nothing Then
				VLVinBarcodeScan = vlVinBarcodeScanNode.Value.ToUpper().Equals("Y")
			End If

			If EnablePL Then
				DefaultPreviewLoanName = "pl"
				DefaultPreviewModuleName = "pl"
			ElseIf EnableCC Then
				DefaultPreviewLoanName = "cc"
				DefaultPreviewModuleName = "cc"
			ElseIf EnableHE Then
				DefaultPreviewLoanName = "he"
				DefaultPreviewModuleName = "he"
			ElseIf EnableVL Then
				DefaultPreviewLoanName = "vl"
				DefaultPreviewModuleName = "vl"
			ElseIf EnableXA Then
				DefaultPreviewLoanName = "xa"
				DefaultPreviewModuleName = "xa"
			End If
		End If
	End Sub

End Class
