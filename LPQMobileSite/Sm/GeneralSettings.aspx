﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="GeneralSettings.aspx.vb" Inherits="Sm_GeneralSettings" MasterPageFile="SiteManager.master"  %>

<asp:Content runat="server" ContentPlaceHolderID="plBody">
	<div>
		<h1 class="page-header">Settings</h1>
		<p>Configure settings for your Application Portal</p>
		<section>
			<ul class="nav nav-tabs" id="mainTabs" role="tablist">
				<li role="presentation" <%=IIf(EnableCC Or EnableHE Or EnablePL Or EnableVL Or EnableBL Or EnableXA, "class='active'", "class='hidden'")%> ><a href="#" role="tab">General</a></li>
				<li role="presentation" <%=IIf(EnableXA, "", "class='hidden'")%>><a href="<%=BuildUrl("/sm/xasettings.aspx")%>" aria-controls="xa" role="tab">XA</a></li>
				<li role="presentation" <%=IIf(EnableCC Or EnableHE Or EnablePL Or EnableVL, "", "class='hidden'")%>><a href="<%=BuildUrl("/sm/loanssettings.aspx")%>" role="tab">All Loans</a></li>
				<li role="presentation" <%=IIf(EnableCC, "", "class='hidden'")%>><a href="<%=BuildUrl("/sm/ccsettings.aspx")%>" role="tab">Credit Card Loan</a></li>
				<li role="presentation" <%=IIf(EnableHE, "", "class='hidden'")%>><a href="<%=BuildUrl("/sm/hesettings.aspx")%>" role="tab">Home Equity Loan</a></li>
				<%If Not (EnableCC Or EnableHE Or EnablePL Or EnableVL Or EnableXA) And EnableLQB Then %>
                <li role="presentation" class="active"><a href="<%=BuildUrl("/sm/mlsettings.aspx")%>" role="tab">Mortgage Loan</a></li>
				<% Else %>
                <li role="presentation" <%=IIf(EnableLQB, "", "class='hidden'")%>><a href="<%=BuildUrl("/sm/mlsettings.aspx")%>" role="tab">Mortgage Loan</a></li>
				<%End If %>
                <li role="presentation" <%=IIf(EnablePL, "", "class='hidden'")%>><a href="<%=BuildUrl("/sm/plsettings.aspx")%>" role="tab">Personal Loan</a></li>
				<li role="presentation" <%=IIf(EnableVL, "", "class='hidden'")%>><a href="<%=BuildUrl("/sm/vlsettings.aspx")%>" role="tab">Vehicle Loan</a></li>
				<li role="presentation" <%=IIf(EnableBL, "", "class='hidden'")%>><a href="<%=BuildUrl("/sm/blsettings.aspx")%>" role="tab">Business Loan</a></li>
			</ul>
			<div class="tab-content">
				<div role="tabpanel" class="tab-pane active" id="general">
					<h2 class="section-title">General Settings</h2>
					<p>These settings will affect both XA and loan applications.</p>
					<div class="bottom30">
						<div class="group-heading">INTEGRATIONS</div>
						<%If ApmDLScanEnable Then%>
						<div class="input-block">
							<h3 class="property-title">License Scan</h3>
							<p>This feature allows users to pre-fill their applications by scanning their driver's license.</p>
							<label class="toggle-btn" data-on="ON" data-off="OFF">
								<input type="checkbox" value="license_scan" id="chkLicenseScan" <%=BindCheckbox(LicenseScan)%> />
								<span class="button-checkbox"></span>
							</label>
						</div>
						<%End If%>
						<%If SmUtil.CheckRoleAccess(Context, SmSettings.Role.LegacyOperator, SmSettings.Role.SuperOperator) Then%>
						<div class="input-block">
							<h3 class="property-title">Laser License Scan</h3>
							<p>Enterprise-Grade barcode scanner with performance and precision of dedicated laser scanner <br />(beta, only visible to legacy operator)</p>
							<label class="toggle-btn" data-on="ON" data-off="OFF">
								<input type="checkbox" value="dl_barcode_scan" id="chkDLBarcodeScan" <%=BindCheckbox(DLBarcodeScan)%>/>
								<span class="button-checkbox"></span>
							</label>
						</div>
						<%End If%>
						<%If ApmLinkedInEnable Then %>
						<div class="input-block">
							<h3 class="property-title">LinkedIn Login</h3>
							<p>This feature allows applicants to pre-fill their application with information from LinkedIn.</p>
							<label class="toggle-btn" data-on="ON" data-off="OFF">
								<input type="checkbox" id="chkLinkedInEnable" value="linkedin_enable" <%=BindCheckbox(LinkedInEnable)%>/>
								<span class="button-checkbox"></span>
							</label>
						</div>
						<%End If%>
						<%If ApmShowInstaTouchSwitchButton Then%>
						<div class="input-block">
							<h3 class="property-title">InstaTouch</h3>
							<p>This feature allows applicants to pre-fill their application using Equifax InstaTouch.</p>
							<label class="toggle-btn disabled" data-on="ON" data-off="OFF">
								<input type="checkbox" id="chkInstaTouchEnable" value="instatouch_enabled" <%=BindCheckbox(InstaTouchEnable)%>/>
								<span class="button-checkbox"></span>
							</label>
							<%--<div class="button-caption">(If interested, please contact Support via support@loanspq.com. <br />SOW is required if this hasn't been enabled for in-branch.)</div>--%>
						</div>
						<%End If%>
						<div class="input-block">
							<h3 class="property-title">MeridianLink Address Verification</h3>
							<p>This feature enables the system to auto verify addresses.</p>
							<label class="toggle-btn disabled" data-on="ON" data-off="OFF">
								<input type="checkbox" disabled="disabled" value="address_key" <%=BindCheckbox(AddressKey)%>/>
								<span class="button-checkbox"></span>
							</label>
							<%--<div class="button-caption">(If interested, please contact Support via support@loanspq.com. <br />SOW is required if this hasn't been enabled for in-branch.)</div>--%>
						</div>

                        <%If ApmSiteAnalyticsEnable Then%>
					    <div class="input-block">
								<h3 class="property-title">Site Analytics</h3>
								<p>This feature enables site analytics for your Application Portal.</p>
                                <p>MeridianLink Analytics ID</p>
								<input type="text" class="form-control" id="txtPiwikSiteId" value="<%=PiwikSiteId%>" />
								<br/>
                                <p>Google Tag Manager ID</p>
								<input type="text" class="form-control" id="txtGoogleTagManagerId" value="<%=GoogleTagManagerId%>" />
						</div>									    
					    <%End If%>

						<%If SmUtil.CheckRoleAccess(Context, SmSettings.Role.LegacyOperator, SmSettings.Role.SuperOperator) Then%>
						<div class="input-block">
							<h3 class="property-title">Homepage Editor</h3>
							<p>This feature allows users to create new or select existing one for edit with drag & drop.(beta, only visible to legacy operator)</p>
							<label class="toggle-btn" data-on="ON" data-off="OFF">
								<input type="checkbox" value="homepage_editor" id="chkHomepageEditor" <%=BindCheckbox(HomepageEditor)%>/>
								<span class="button-checkbox"></span>
							</label>
							<%--<div class="button-caption">(If interested, please contact Support via support@loanspq.com. <br />SOW is required if this hasn't been enabled for in-branch.)</div>--%>
						</div>
						<%End If%>
					</div>
					<div class="bottom30">
						<div class="group-heading">WORKFLOWS</div>
						<div class="input-block">
							<h3 class="property-title">Foreign Address</h3>
							<p>Allow your applicants to apply with a foreign address</p>
							<label class="toggle-btn" data-on="ON" data-off="OFF">
								<input type="checkbox" id="chkForeignAddress" value="foreign_address" <%=BindCheckbox(ForeignAddress)%>/>
								<span class="button-checkbox"></span>
							</label>
						</div>
						<div class="input-block">
							<h3 class="property-title">Foreign Phone</h3>
							<p>Allow your applicants to apply with a foreign phone</p>
							<label class="toggle-btn" data-on="ON" data-off="OFF">
								<input type="checkbox" id="chkForeignPhone" value="foreign_phone" <%=BindCheckbox(ForeignPhone)%>/>
								<span class="button-checkbox"></span>
							</label>
						</div>
						<div class="input-block">
							<h3 class="property-title">MFA Email Only</h3>
							<p>This will use email field and will hide "Confirm your identity via SSN instead of email?" link button in the Status page.</p>
							<label class="toggle-btn" data-on="ON" data-off="OFF">
								<input type="checkbox" id="chkMfaEmailOnly" value="mfa_email_only" <%=BindCheckbox(MfaEmailOnly)%>/>
								<span class="button-checkbox"></span>
							</label>
						</div>
					</div>
					
					<%--<div class="bottom30">
						<div class="input-block">
							<div class="group-heading">SOCIAL & TRACKING</div>
							<h3 class="property-title">Piwik Site</h3>
							<p>Enter Piwik Site ID here.</p>
							<input type="text" data-name="piwik-site-id" class="sm-textbox form-control"/>
						</div>
						<div class="input-block">
							<h3 class="property-title">Google Tag Manager</h3>
							<p>Enter Google Tag Manager ID here.</p>
							<input type="text" data-name="google-tag-manager-id" class="sm-textbox form-control"/>	
						</div>
					</div>--%>
				</div>
			</div>
		</section>
	</div>
</asp:Content>
<asp:Content runat="server" ContentPlaceHolderID="plScripts">
	<script type="text/javascript">
		$(function () {
			registerDataValidator();
			$("select,input:checkbox").on("change", function () {
				master.FACTORY.documentChanged(this);
			});
			$("input:text").each(function (idx, ele) {
				$(ele).on({
					keypress: function () { master.FACTORY.documentChanged(ele); },
					paste: function () { master.FACTORY.documentChanged(ele); },
					cut: function () { master.FACTORY.documentChanged(ele); }
				});
			});
		});
		(function (master, $, undefined) {
			master.FACTORY.SaveDraft = function (revisionId) {
				if ($.smValidate("ValidateData") == false) {
					_COMMON.noty("error", "Error: please check all fields.", 500);
					return;
				}

				var dataObj = collectSubmitData();
				dataObj.revisionId = revisionId;
				dataObj.command = 'savegeneralsettings';
				$.ajax({
					url: "/sm/smhandler.aspx",
					async: true,
					cache: false,
					type: 'POST',
					dataType: 'html',
					data: dataObj,
					success: function (responseText) {
						var response = $.parseJSON(responseText);
						if (response.IsSuccess) {
							master.FACTORY.onSaveDraftSuccess();
						} else {
							_COMMON.noty("error", "Error", 500);
						}
					}
				});
			};
			master.FACTORY.Preview = function (revisionId) {
				if ($.smValidate("ValidateData") == false) {
					_COMMON.noty("error", "Error: please check all fields.", 500);
					return;
				}
				var dataObj = collectSubmitData();
				dataObj.revisionId = revisionId;
				dataObj.command = 'previewgeneralsettings';
				$.ajax({
					url: "/sm/smhandler.aspx",
					async: true,
					cache: false,
					type: 'POST',
					dataType: 'html',
					data: dataObj,
					success: function (responseText) {
						var response = $.parseJSON(responseText);
						if (response.IsSuccess) {
							master.FACTORY.onPreviewSuccess();
							_COMMON.openInNewTab(response.Info.previewurl);
						} else {
							_COMMON.noty("error", "Error", 500);
						}
					}
				});
			};
			master.FACTORY.validateBeforePublish = function () {
				var result = $.smValidate("ValidateData");
				if (result == false) {
					_COMMON.noty("error", "Error: please check all fields.", 500);
				}
				return result;
			}
			master.FACTORY.Publish = function (comment, publishAll) {
				if ($.smValidate("ValidateData") == false) {
					_COMMON.noty("error", "Error: please check all fields.", 500);
					return;
				}
				var dataObj = collectSubmitData();
				dataObj.command = 'publishgeneralsettings';
				dataObj.comment = comment;
				dataObj.publishAll = publishAll;
				$.ajax({
					url: "/sm/smhandler.aspx",
					async: true,
					cache: false,
					type: 'POST',
					dataType: 'html',
					data: dataObj,
					success: function (responseText) {
						var response = $.parseJSON(responseText);
						if (response.IsSuccess) {
							master.FACTORY.onPublishSuccess();
						} else {
							_COMMON.noty("error", "Error", 500);
						}
					}
				});
			};
		}(window.master = window.master || {}, jQuery));
		function registerDataValidator() {
			$.smValidate.removeValidationGroup("ValidateData");
			$("input.float-number").each(function (idx, input) {
				$(input).observer({
					validators: [
						function (partial) {
							var $self = $(this);
							if (/^[0-9]+(\.[0-9]+)*$/.test($self.val()) == false) {
								return "Invalid data";
							}
							return "";
						}
					],
					validateOnBlur: true,
					group: "ValidateData"
				});
			});
		}
		function collectSubmitData() {
			var result = {};
			result.lenderconfigid = '<%=LenderConfigID%>';
			result.license_scan = $("#chkLicenseScan").is(":checked");
			result.dl_barcode_scan = $("#chkDLBarcodeScan").is(":checked");
			result.mfa_email_only = $("#chkMfaEmailOnly").is(":checked");
			result.foreign_address = $("#chkForeignAddress").is(":checked");
			result.foreign_phone = $("#chkForeignPhone").is(":checked");
			<%If ApmLinkedInEnable Then %>
			result.linkedin_enable = $("#chkLinkedInEnable").is(":checked");
			<%End If%>
			<%If ApmShowInstaTouchSwitchButton Then%>
			result.instatouch_enabled = $("#chkInstaTouchEnable").is(":checked");
			<%End If%>
			result.homepage_editor = $("#chkHomepageEditor").is(":checked");
			<%If ApmSiteAnalyticsEnable Then%>
			result.google_tag_manager_id = $("#txtGoogleTagManagerId").val();
			result.piwik_site_id = $("#txtPiwikSiteId").val();
			<%End If%>
			return result;
		}
	</script>
</asp:Content>
<asp:Content runat="server" ContentPlaceHolderID="plActionButtons"></asp:Content>