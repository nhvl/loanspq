﻿Imports Sm
Imports System.Xml
Imports LPQMobile.Utils

Partial Class Sm_CCSettings
	Inherits SmApmBasePage

	Private Const LOAN_NODE_NAME As String = "CREDIT_CARD_LOAN"
	'Protected Property CCBooking As String
	Protected Property CCXSell As Boolean = False
	Protected Property CCDocuSign As Boolean = False
	Protected Property CCDocuSignPDFGroup As String
	Protected Property CCJointEnable As Boolean = True
	Protected Property CCLocationPool As Boolean = False
    Protected Property CCPreviousAddressEnable As Boolean = False
    Protected Property CCPreviousEmploymentEnable As Boolean = False
    Protected Property CCPreviousAddressThreshold As String = ""
    Protected Property CCPreviousEmploymentThreshold As String = ""
    Protected Property DeclarationList As Dictionary(Of String, SmDeclarationItem)
	Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
		If Not EnableCC Then
			Response.Redirect(BuildUrl("/sm/generalsettings.aspx"), True)
		End If
		If Not Page.IsPostBack Then
			Dim smMasterPage = CType(Me.Master, Sm_SiteManager)
			smMasterPage.LenderConfigXml = LenderConfigXml
			smMasterPage.RevisionID = LenderConfig.RevisionID
			smMasterPage.LastModifiedBy = LenderConfig.LastModifiedByUserFullName
			smMasterPage.LastModifiedByUserID = LenderConfig.LastModifiedByUserID
			smMasterPage.SelectedMainMenuItem = "config"
			smMasterPage.SelectedSubMenuItem = "settings"
			smMasterPage.CurrentNodeGroup = SmSettings.NodeGroup.CCSettings
			smMasterPage.BreadCrumb = "Configure / Settings / Credit Card"

			'Dim CCBookingNode As XmlNode = GetNode(LOAN_BOOKING("CREDIT_CARD_LOAN"))
			'If CCBookingNode IsNot Nothing Then
			'	CCBooking = CCBookingNode.Value
			'End If

			Dim CCxsellEnabledNode As XmlNode = GetNode(XSELL(LOAN_NODE_NAME))
			If CCxsellEnabledNode IsNot Nothing Then
				CCXSell = CCxsellEnabledNode.Value.ToUpper().Equals("Y")
			End If

			Dim CCdocusignEnabledNode As XmlNode = GetNode(DOCU_SIGN(LOAN_NODE_NAME))
			If CCdocusignEnabledNode IsNot Nothing Then
				CCDocuSign = CCdocusignEnabledNode.Value.ToUpper().Equals("Y")
			End If

			Dim CCdocusignPDFGroupNode As XmlNode = GetNode(DOCU_SIGN_PDF_GROUP(LOAN_NODE_NAME))
			If CCdocusignPDFGroupNode IsNot Nothing Then
				CCDocuSignPDFGroup = CCdocusignPDFGroupNode.Value.Trim()
			End If
			'work flow
			Dim ccJointEnableNode As XmlNode = GetNode(CC_JOINT_ENABLE())
			If ccJointEnableNode IsNot Nothing Then
				CCJointEnable = Not ccJointEnableNode.Value.ToUpper().Equals("N") 'default = Y
			End If
			Dim CCLocationPoolNode As XmlNode = GetNode(LOAN_LOCATION_POOL(LOAN_NODE_NAME))
			If CCLocationPoolNode IsNot Nothing Then
				CCLocationPool = CCLocationPoolNode.Value.ToUpper().Equals("Y")
			End If
			
			Dim loanDeclarationList = Common.GetAllDeclarations(LOAN_NODE_NAME)
            If loanDeclarationList IsNot Nothing AndAlso loanDeclarationList.Count > 0 Then
                DeclarationList = (From item In loanDeclarationList Select New SmDeclarationItem() With {.Key = item.Key, .DisplayText = item.Value, .Active = False}).ToDictionary(Function(p) p.Key, Function(p) p)
                Dim declarationsDraftNode As XmlNode = GetNode(LOAN_DECLARATIONS(LOAN_NODE_NAME))
                If declarationsDraftNode IsNot Nothing Then
                    Dim declarationDraftNodes = declarationsDraftNode.SelectNodes("DECLARATION")
                    If declarationDraftNodes IsNot Nothing AndAlso declarationDraftNodes.Count > 0 Then
                        For Each node As XmlNode In declarationDraftNodes
                            Dim key As String = node.Attributes("key").Value
                            If DeclarationList.ContainsKey(key) Then
                                DeclarationList.Item(key).Active = True
                            End If
                        Next
                    End If
                End If
            End If

            Dim ccPreviousEmploymentThresholdNode As XmlNode = GetNode(PREVIOUS_EMPLOYMENT_THRESHOLD("CREDIT_CARD_LOAN"))
            If ccPreviousEmploymentThresholdNode IsNot Nothing AndAlso Common.SafeString(ccPreviousEmploymentThresholdNode.Value) <> "" Then
                CCPreviousEmploymentThreshold = Common.SafeString(ccPreviousEmploymentThresholdNode.Value)
                CCPreviousEmploymentEnable = True
            End If
            Dim ccPreviousAddressThresholdNode As XmlNode = GetNode(PREVIOUS_ADDRESS_THRESHOLD("CREDIT_CARD_LOAN"))
            If ccPreviousAddressThresholdNode IsNot Nothing AndAlso Common.SafeString(ccPreviousAddressThresholdNode.Value) <> "" Then
                CCPreviousAddressThreshold = Common.SafeString(ccPreviousAddressThresholdNode.Value)
                CCPreviousAddressEnable = True
            End If
        End If
	End Sub

End Class

