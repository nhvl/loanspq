﻿
Imports Sm.BO
Imports LPQMobile.Utils
Imports System.Xml

Partial Class Sm_SiteManager
	Inherits System.Web.UI.MasterPage

	Public Property LenderInfo As String = ""
	Public Property InBranchHost As String = ""

	Public Property EnableHomepageEditor As Boolean = False

	Public Property SelectedMainMenuItem As String
	Public Property SelectedSubMenuItem As String
	Public Property BreadCrumb As String = ""

	Public Property HideSubMenuItem As Boolean = False

	Protected Function GenerateMenuSelectedClass(itemName As String) As String
		If itemName = SelectedMainMenuItem Then Return "active"
		Return ""
	End Function

	Protected Function GenerateSubMenuSelectedClass(itemName As String) As String
		If itemName = SelectedSubMenuItem Then Return "active"
		Return ""
	End Function

	Public Property ShowPreviewButton As Boolean = True
	Public Property ShowSaveDraftButton As Boolean = True
	Public Property ShowPublishButton As Boolean = True
	Public Property ChangesPending As Boolean = False

	Protected QueryStringDic As Dictionary(Of String, String)

	Public Property LenderConfigXml As New XmlDocument
	Public Property WebsiteConfig As CWebsiteConfig
	Public ReadOnly Property IsBankPortal As Boolean
		Get
			Return WebsiteConfig.InstitutionType = CEnum.InstitutionType.BANK
		End Get
	End Property

	Private _enableXA As Boolean = False
	Protected ReadOnly Property EnableXA As Boolean
		Get
			Return _enableXA
		End Get
	End Property

	Private _enableHE As Boolean = False
	Protected ReadOnly Property EnableHE As Boolean
		Get
			Return _enableHE
		End Get
	End Property

	Private _enableCC As Boolean = False
	Protected ReadOnly Property EnableCC As Boolean
		Get
			Return _enableCC
		End Get
	End Property

	Private _enableBL As Boolean = False
	Protected ReadOnly Property EnableBL As Boolean
		Get
			Return _enableBL
		End Get
	End Property

	Private _enableVL As Boolean = False
	Protected ReadOnly Property EnableVL As Boolean
		Get
			Return _enableVL
		End Get
	End Property

	Private _enablePL As Boolean = False
    Protected ReadOnly Property EnablePL As Boolean
        Get
            Return _enablePL
        End Get
	End Property

	Private _enableBusinessRules As Boolean = False
	Protected ReadOnly Property EnableBusinessRules As Boolean
		Get
			Return _enableBusinessRules
		End Get
	End Property

    Private _enableCQ As Boolean = False
	Protected ReadOnly Property EnableCQ As Boolean
		Get
			Return _enableCQ
		End Get
	End Property

	Private _enableLegacyAC As Boolean = False
	Protected ReadOnly Property EnableLegacyAC As Boolean
		Get
			Return _enableLegacyAC
		End Get
	End Property
	Private _enableLQB As Boolean = False
	Protected ReadOnly Property EnableLQB As Boolean
		Get
			Return _enableLQB
		End Get
	End Property
	Private _enableCLR As Boolean = False
	Protected ReadOnly Property EnableCLR As Boolean
		Get
			Return _enableCLR
		End Get
	End Property
	Private _enableCAS As Boolean = False
	Protected ReadOnly Property EnableCAS As Boolean
		Get
			Return _enableCAS
		End Get
	End Property

	Public Property RevisionID As Integer
	Public Property LastModifiedBy As String
	Public Property LastModifiedByUserID As Guid
	Public ReadOnly Property LenderConfigID As Guid
		Get
			Return Common.SafeGUID(Request.QueryString("lenderconfigid"))
		End Get
	End Property
	Public Property CurrentNodeGroup As Nullable(Of SmSettings.NodeGroup) = Nothing

	Public Property RemoveQueryStringList As New List(Of String)
	Private _draftRevision As New KeyValuePair(Of Integer, DateTime)(-1, Now)

	Protected ReadOnly Property DraftRevision As KeyValuePair(Of Integer, DateTime)
		Get
			If CurrentNodeGroup.HasValue = False Then
				Return New KeyValuePair(Of Integer, Date)(0, Now)
			End If
			If _draftRevision.Key = -1 Then
				_draftRevision = New SmBL().GetDraftRevisionID(CurrentNodeGroup.Value, LenderConfigID, UserInfo.UserID)
			End If
			Return _draftRevision
		End Get
	End Property

	Private _instaTouchShowReports As Boolean = False
	Public ReadOnly Property InstaTouchShowReports() As Boolean
		Get
			Return _instaTouchShowReports
		End Get
	End Property

	Private Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
		QueryStringDic = New Dictionary(Of String, String)()
		For Each key As String In Request.QueryString.AllKeys
			If key Is Nothing Or key = "" Then Continue For
			QueryStringDic.Add(key, Request.QueryString(key))
		Next
		If Not IsPostBack Then
			WebsiteConfig = New CWebsiteConfig(LenderConfigXml.DocumentElement)
			If LenderConfigXml.SelectSingleNode("WEBSITE/XA_LOAN") IsNot Nothing Then
				_enableXA = True
			End If
			If LenderConfigXml.SelectSingleNode("WEBSITE/CREDIT_CARD_LOAN") IsNot Nothing Then
				_enableCC = True
			End If
			If LenderConfigXml.SelectSingleNode("WEBSITE/BUSINESS_LOAN") IsNot Nothing Then
				_enableBL = True
			End If
			If LenderConfigXml.SelectSingleNode("WEBSITE/HOME_EQUITY_LOAN") IsNot Nothing Then
				_enableHE = True
			End If
			If LenderConfigXml.SelectSingleNode("WEBSITE/PERSONAL_LOAN") IsNot Nothing Then
				_enablePL = True
			End If

			If LenderConfigXml.SelectSingleNode("WEBSITE/VEHICLE_LOAN") IsNot Nothing Then
				_enableVL = True
			End If

			If LenderConfigXml.SelectSingleNode("WEBSITE/LQB") IsNot Nothing Then
				_enableLQB = True
			End If

			If LenderConfigXml.SelectSingleNode("WEBSITE//CUSTOM_CONDITIONED_QUESTIONS") IsNot Nothing OrElse LenderConfigXml.SelectSingleNode("WEBSITE//CUSTOM_APPLICANT_CONDITIONED_QUESTIONS") IsNot Nothing Then
				_enableLegacyAC = True
			End If

			Dim instaTouchShowReportsNode = LenderConfigXml.SelectSingleNode("WEBSITE/VISIBLE/@APM_instatouch_reports_N")
			_instaTouchShowReports = instaTouchShowReportsNode IsNot Nothing AndAlso instaTouchShowReportsNode.Value.ToUpper() = "Y"

			_enableBusinessRules = CheckEnabledModule(SmSettings.ModuleName.XA) _
				OrElse CheckEnabledModule(SmSettings.ModuleName.CC) _
				OrElse CheckEnabledModule(SmSettings.ModuleName.CC_COMBO) _
				OrElse CheckEnabledModule(SmSettings.ModuleName.HE) _
				OrElse CheckEnabledModule(SmSettings.ModuleName.HE_COMBO) _
				OrElse CheckEnabledModule(SmSettings.ModuleName.PL) _
				OrElse CheckEnabledModule(SmSettings.ModuleName.PL_COMBO) _
				OrElse CheckEnabledModule(SmSettings.ModuleName.VL) _
				OrElse CheckEnabledModule(SmSettings.ModuleName.VL_COMBO) _
				OrElse CheckEnabledModule(SmSettings.ModuleName.BL)
			_enableCQ = CheckEnabledModule(SmSettings.ModuleName.XA) _
				OrElse CheckEnabledModule(SmSettings.ModuleName.CC) _
				OrElse CheckEnabledModule(SmSettings.ModuleName.HE) _
				OrElse CheckEnabledModule(SmSettings.ModuleName.PL) _
				OrElse CheckEnabledModule(SmSettings.ModuleName.VL) _
				OrElse CheckEnabledModule(SmSettings.ModuleName.BL)
			' AP-2068 - Don't show Custom List Rules for Mortage-only portals
			_enableCLR = CheckEnabledModule(SmSettings.ModuleName.XA) _
				OrElse CheckEnabledModule(SmSettings.ModuleName.CC) _
				OrElse CheckEnabledModule(SmSettings.ModuleName.HE) _
				OrElse CheckEnabledModule(SmSettings.ModuleName.PL) _
				OrElse CheckEnabledModule(SmSettings.ModuleName.VL) _
				OrElse CheckEnabledModule(SmSettings.ModuleName.BL)

			_enableCAS = CheckEnabledModule(SmSettings.ModuleName.XA)


			'for testing, set this to false to alway enable Business Rule Tab
			'_enableLegacyAC = False

			_apmComboEnable = SmUtil.GetApmComboEnable(LenderConfigXml)
			_apmSiteAnalyticsEnable = SmUtil.GetApmSiteAnalyticsEnable(LenderConfigXml)
			_apmDLScanEnable = SmUtil.GetApmDLScanEnable(LenderConfigXml)
			_apmLinkedInEnable = SmUtil.GetApmLinkedInEnable(LenderConfigXml)

			Dim homepageEditorNode = LenderConfigXml.SelectSingleNode("WEBSITE/@homepage_editor")
			If homepageEditorNode IsNot Nothing Then
				EnableHomepageEditor = homepageEditorNode.Value.ToUpper().Equals("Y")
			End If

			If CurrentNodeGroup IsNot Nothing AndAlso CurrentNodeGroup.HasValue Then
				Dim isAvailable As Boolean = True

				Select Case CurrentNodeGroup
					Case SmSettings.NodeGroup.ManageXA
						isAvailable = _enableXA
					Case SmSettings.NodeGroup.ManageSpecialXA
						isAvailable = _enableXA
					Case SmSettings.NodeGroup.ManageBusinessXA
						isAvailable = _enableXA
					Case SmSettings.NodeGroup.ManageXASecondary
						isAvailable = _enableXA
					Case SmSettings.NodeGroup.ManageCC
						isAvailable = _enableCC
					Case SmSettings.NodeGroup.ManageBL
						isAvailable = _enableBL
					Case SmSettings.NodeGroup.ManageHE
						isAvailable = _enableHE
					Case SmSettings.NodeGroup.ManagePL
						isAvailable = _enablePL
					Case SmSettings.NodeGroup.ManageVL
						isAvailable = _enableVL
					Case SmSettings.NodeGroup.ManageLQB
						isAvailable = _enableLQB
					Case SmSettings.NodeGroup.ManageComboCC
						isAvailable = _enableCC AndAlso _enableXA AndAlso ApmComboEnable
					Case SmSettings.NodeGroup.ManageComboHE
						isAvailable = _enableHE AndAlso _enableXA AndAlso ApmComboEnable
					Case SmSettings.NodeGroup.ManageComboPL
						isAvailable = _enablePL AndAlso _enableXA AndAlso ApmComboEnable
					Case SmSettings.NodeGroup.ManageComboVL
						isAvailable = _enableVL AndAlso _enableXA AndAlso ApmComboEnable
					Case SmSettings.NodeGroup.XaSettings
						isAvailable = _enableXA
					Case SmSettings.NodeGroup.AllLoansSettings
						isAvailable = _enableCC OrElse _enableHE OrElse _enablePL Or _enableVL
					Case SmSettings.NodeGroup.CCSettings
						isAvailable = _enableCC
					Case SmSettings.NodeGroup.HESettings
						isAvailable = _enableHE
					Case SmSettings.NodeGroup.PLSettings
						isAvailable = _enablePL
					Case SmSettings.NodeGroup.VLSettings
						isAvailable = _enableVL
					Case SmSettings.NodeGroup.BLSettings
						isAvailable = _enableBL
					Case SmSettings.NodeGroup.LQBSettings
						isAvailable = _enableLQB
					Case SmSettings.NodeGroup.ICERenameXA
						isAvailable = _enableXA
					Case SmSettings.NodeGroup.ICERenameCC
						isAvailable = _enableCC
					Case SmSettings.NodeGroup.ICERenameHE
						isAvailable = _enableHE
					Case SmSettings.NodeGroup.ICERenamePL
						isAvailable = _enablePL
					Case SmSettings.NodeGroup.ICERenameVL
						isAvailable = _enableVL
					Case SmSettings.NodeGroup.ICERenameBL
						isAvailable = _enableBL
					Case SmSettings.NodeGroup.ICERenameCCCombo
						isAvailable = _enableCC AndAlso _enableXA AndAlso ApmComboEnable
					Case SmSettings.NodeGroup.ICERenamePLCombo
						isAvailable = _enablePL AndAlso _enableXA AndAlso ApmComboEnable
					Case SmSettings.NodeGroup.ICERenameHECombo
						isAvailable = _enableHE AndAlso _enableXA AndAlso ApmComboEnable
					Case SmSettings.NodeGroup.ICERenameVLCombo
						isAvailable = _enableVL AndAlso _enableXA AndAlso ApmComboEnable
					Case SmSettings.NodeGroup.ICERenameSA
						isAvailable = _enableXA
					Case SmSettings.NodeGroup.ICERenameXASpecial
						isAvailable = _enableXA
					Case SmSettings.NodeGroup.ICERenameSASpecial
						isAvailable = _enableXA
					Case SmSettings.NodeGroup.ICERenameXABusiness
						isAvailable = _enableXA
					Case SmSettings.NodeGroup.ICERenameSABusiness
						isAvailable = _enableXA
					Case SmSettings.NodeGroup.ICERenameLQBMain
						isAvailable = _enableLQB
					Case SmSettings.NodeGroup.XAProductsConfigure
						isAvailable = _enableXA
					Case SmSettings.NodeGroup.CCComboProductsConfigure
						isAvailable = _enableCC
					Case SmSettings.NodeGroup.HEComboProductsConfigure
						isAvailable = _enableHE
					Case SmSettings.NodeGroup.PLComboProductsConfigure
						isAvailable = _enablePL
					Case SmSettings.NodeGroup.VLComboProductsConfigure
						isAvailable = _enableVL

					Case SmSettings.NodeGroup.XACustomQuestions
						isAvailable = _enableXA
					Case SmSettings.NodeGroup.CCCustomQuestions
						isAvailable = _enableCC
					Case SmSettings.NodeGroup.HECustomQuestions
						isAvailable = _enableHE
					Case SmSettings.NodeGroup.PLCustomQuestions
						isAvailable = _enablePL
					Case SmSettings.NodeGroup.VLCustomQuestions
						isAvailable = _enableVL
					Case SmSettings.NodeGroup.BLCustomQuestions
						isAvailable = _enableBL

					Case SmSettings.NodeGroup.ShowFieldXA
						isAvailable = _enableXA
					Case SmSettings.NodeGroup.ShowFieldCC
						isAvailable = _enableCC
					Case SmSettings.NodeGroup.ShowFieldPL
						isAvailable = _enablePL
					Case SmSettings.NodeGroup.ShowFieldVL
						isAvailable = _enableVL
					Case SmSettings.NodeGroup.ShowFieldHE
						isAvailable = _enableHE
					Case SmSettings.NodeGroup.ShowFieldSA
						isAvailable = _enableXA
					Case SmSettings.NodeGroup.ShowFieldXASpecial
						isAvailable = _enableXA
					Case SmSettings.NodeGroup.ShowFieldSASpecial
						isAvailable = _enableXA
					Case SmSettings.NodeGroup.ShowFieldXABusiness
						isAvailable = _enableXA
					Case SmSettings.NodeGroup.ShowFieldSABusiness
						isAvailable = _enableXA
					Case SmSettings.NodeGroup.ShowFieldPLCombo
						isAvailable = _enableXA AndAlso _enablePL AndAlso ApmComboEnable
					Case SmSettings.NodeGroup.ShowFieldVLCombo
						isAvailable = _enableXA AndAlso _enableVL AndAlso ApmComboEnable
					Case SmSettings.NodeGroup.ShowFieldHECombo
						isAvailable = _enableXA AndAlso _enableHE AndAlso ApmComboEnable
					Case SmSettings.NodeGroup.ShowFieldCCCombo
						isAvailable = _enableXA AndAlso _enableCC AndAlso ApmComboEnable
					Case SmSettings.NodeGroup.ShowFieldBL
						isAvailable = _enableBL
					Case SmSettings.NodeGroup.ShowFieldLQBMain
						isAvailable = _enableLQB

					Case SmSettings.NodeGroup.ValidationXA
						isAvailable = _enableXA
					Case SmSettings.NodeGroup.ValidationSA
						isAvailable = _enableXA
					Case SmSettings.NodeGroup.ValidationXASpecial
						isAvailable = _enableXA
					Case SmSettings.NodeGroup.ValidationSASpecial
						isAvailable = _enableXA
					Case SmSettings.NodeGroup.ValidationXABusiness
						isAvailable = _enableXA
					Case SmSettings.NodeGroup.ValidationSABusiness
						isAvailable = _enableXA
					Case SmSettings.NodeGroup.ValidationCC
						isAvailable = _enableCC
					Case SmSettings.NodeGroup.ValidationHE
						isAvailable = _enableHE
					Case SmSettings.NodeGroup.ValidationPL
						isAvailable = _enablePL
					Case SmSettings.NodeGroup.ValidationVL
						isAvailable = _enableVL
					Case SmSettings.NodeGroup.ValidationCCCombo
						isAvailable = _enableCC AndAlso _enableXA AndAlso ApmComboEnable
					Case SmSettings.NodeGroup.ValidationHECombo
						isAvailable = _enableHE AndAlso _enableXA AndAlso ApmComboEnable
					Case SmSettings.NodeGroup.ValidationPLCombo
						isAvailable = _enablePL AndAlso _enableXA AndAlso ApmComboEnable
					Case SmSettings.NodeGroup.ValidationVLCombo
						isAvailable = _enableVL AndAlso _enableXA AndAlso ApmComboEnable
					Case SmSettings.NodeGroup.ValidationBL
						isAvailable = _enableBL
					Case SmSettings.NodeGroup.ValidationLQBMain
						isAvailable = _enableLQB
					Case SmSettings.NodeGroup.XAApplicationBlockRules, SmSettings.NodeGroup.XACustomListRules
						isAvailable = _enableXA
					Case SmSettings.NodeGroup.BLApplicationBlockRules, SmSettings.NodeGroup.BLCustomListRules
						isAvailable = _enableBL
					Case SmSettings.NodeGroup.CCApplicationBlockRules, SmSettings.NodeGroup.CCCustomListRules
						isAvailable = _enableCC
					Case SmSettings.NodeGroup.HEApplicationBlockRules, SmSettings.NodeGroup.HECustomListRules
						isAvailable = _enableHE
					Case SmSettings.NodeGroup.PLApplicationBlockRules, SmSettings.NodeGroup.PLCustomListRules
						isAvailable = _enablePL
					Case SmSettings.NodeGroup.VLApplicationBlockRules, SmSettings.NodeGroup.VLCustomListRules
						isAvailable = _enableVL
					Case SmSettings.NodeGroup.MLSupport
						isAvailable = _enableLQB
				End Select


				If isAvailable = False Then
					Response.Redirect("~/NoAccess.aspx", True)
				End If
				Dim smBL As New SmBL
				Dim draftNodes As List(Of SmLenderConfigDraft) = smBL.GetLenderConfigDrafts(LenderConfigID, UserInfo.UserID, CurrentNodeGroup.Value)
				If draftNodes IsNot Nothing AndAlso draftNodes.Count > 0 Then ChangesPending = True
			End If

			'show lender info next to login info
			Dim smBL2 As New SmBL
			Dim BackOfficeLender As SmBackOfficeLender = smBL2.GetBackOfficeLenderByLenderConfigID(LenderConfigID)
			LenderInfo = "(" & BackOfficeLender.LenderName & ")"
			InBranchHost = BackOfficeLender.Host
		End If
	End Sub

	Public Function GetPageUrl(pageName As String, ParamArray excludeQueryString() As String) As String
		If Request.QueryString.Count > 0 Then
			Dim queryItems As New Dictionary(Of String, String)
			For Each key As String In Request.QueryString.AllKeys
				If RemoveQueryStringList.Contains(key.ToLower()) OrElse excludeQueryString.Contains(key.ToLower()) OrElse queryItems.ContainsKey(key.ToLower()) Then Continue For
				queryItems.Add(key.ToLower(), Common.SafeEncodeString(Request.QueryString(key)))
			Next
			Return pageName & "?" & String.Join("&", queryItems.Select(Function(kvp) String.Format("{0}={1}", kvp.Key, kvp.Value)))
		End If
		Return pageName
	End Function


	Public ReadOnly Property UserInfo() As SmUser
		Get
			Dim user As SmUser
			If Session("CURRENT_USER_INFO") IsNot Nothing Then
				user = DirectCast(Session("CURRENT_USER_INFO"), SmUser)
			Else
				Dim smAuthBL As New SmAuthBL()
				user = smAuthBL.GetCurrentUserFromContext(Context)
				user.Password = String.Empty
				user.Salt = String.Empty
				HttpContext.Current.Session.Add("CURRENT_USER_INFO", user)
			End If
			Return user
		End Get
	End Property

	Public Function CheckEnabledModule(moduleName As SmSettings.ModuleName) As Boolean
		Select Case moduleName
			Case SmSettings.ModuleName.XA
				Return _enableXA
			Case SmSettings.ModuleName.CC
				Return _enableCC
			Case SmSettings.ModuleName.BL
				Return _enableBL
			Case SmSettings.ModuleName.HE
				Return _enableHE
			Case SmSettings.ModuleName.PL
				Return _enablePL
			Case SmSettings.ModuleName.VL
				Return _enableVL
			Case SmSettings.ModuleName.CC_COMBO
				Return _enableXA And _enableCC And ApmComboEnable
			Case SmSettings.ModuleName.HE_COMBO
				Return _enableXA And _enableHE And ApmComboEnable
			Case SmSettings.ModuleName.PL_COMBO
				Return _enableXA And _enablePL And ApmComboEnable
			Case SmSettings.ModuleName.VL_COMBO
				Return _enableXA And _enableVL And ApmComboEnable
			Case SmSettings.ModuleName.LQB
				Return _enableLQB
			Case Else
				Return False
		End Select
	End Function

	'APM properties to support Essential(Free) or Plus(Paid) version

	'=="N" for essential, null or Y for Plus and legacy
	Private _apmComboEnable As Boolean = True
	Protected ReadOnly Property ApmComboEnable() As Boolean
		Get
			Return _apmComboEnable
		End Get
	End Property

	'=="N" for essential, null or Y for plus and legacy->
	Private _apmSiteAnalyticsEnable As Boolean = True
	Protected ReadOnly Property ApmSiteAnalyticsEnable() As Boolean
		Get
			Return _apmSiteAnalyticsEnable
		End Get
	End Property

	'=="Y" for all new deployment, legacy client will have this set to Y if mpao_key2 is not null during publish, this will be "OR" with mpao_key2 to control visibility
	Private _apmDLScanEnable As Boolean = False
	Protected ReadOnly Property ApmDLScanEnable() As Boolean
		Get
			Return _apmDLScanEnable
		End Get
	End Property

	'=="Y" for all Plus, legacy client will have this set to Y if linkedin_enable is Y null during publish, this will be "OR" with linkedin_enable to control visibility
	Private _apmLinkedInEnable As Boolean = False
	Protected ReadOnly Property ApmLinkedInEnable() As Boolean
		Get
			Return _apmLinkedInEnable
		End Get
	End Property
	'End APM properties
End Class

