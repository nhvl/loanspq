﻿
Partial Class Sm_MasterPage
    Inherits System.Web.UI.MasterPage

	Public Property LenderInfo As String = ""
	Public Property SelectedMainMenuItem As String

	Protected Function GenerateMenuSelectedClass(itemName As String) As String
		If itemName = SelectedMainMenuItem Then Return "active"
		Return ""
	End Function

	Public ReadOnly Property UserInfo() As SmUser
		Get
			Dim user As SmUser
			If Session("CURRENT_USER_INFO") IsNot Nothing Then
				user = DirectCast(Session("CURRENT_USER_INFO"), SmUser)
			Else
				Dim smAuthBL As New SmAuthBL()
				user = smAuthBL.GetCurrentUserFromContext(Context)
				user.Password = String.Empty
				user.Salt = String.Empty
				HttpContext.Current.Session.Add("CURRENT_USER_INFO", user)
			End If
			Return user
		End Get
	End Property

End Class

