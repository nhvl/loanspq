﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="MigrateConfig.aspx.vb" Inherits="Sm_MigrateConfig"  masterPageFile="SiteManager.master" %>


<asp:Content runat="server" ContentPlaceHolderID="plBody">
	<div>
		<h1 class="page-header">Migrate Configuration</h1>
		<p>Migrate Configuration</p>
		<section class="migrate-settings" data-name="MigrateShowFieldsData">
			<h2 class="section-title">Migrate Show-Fields Configuration Data</h2>
			<p>Migrate Show-Fields Configuration Data.</p>
			<div class="checkbox-group">
				<p><i class="fa fa-caret-right" aria-hidden="true"></i><span>Xpress Account</span></p>
				<%If Not String.IsNullOrEmpty(XaBranchSelectionDropdown) Then%>
				<div class="checkbox-btn-wrapper">
					<span>BRANCH_SELECTION_DROPDOWN</span>
					<label class="toggle-btn pull-right <%=XaBranchSelectionDropdown %>-flag"><%=XaBranchSelectionDropdown %></label>
				</div>
				<%End If%>
				<%If Not String.IsNullOrEmpty(XaMotherMaidenName) Then%>
				<div class="checkbox-btn-wrapper">
					<span>MOTHER_MAIDEN_NAME</span>
					<label class="toggle-btn pull-right <%=XaMotherMaidenName%>-flag"><%=XaMotherMaidenName%></label>
				</div>
				<%End If%>
				<%If Not String.IsNullOrEmpty(XaGender) Then%>
				<div class="checkbox-btn-wrapper">
					<span>GENDER</span>
					<label class="toggle-btn pull-right <%=XaGender%>-flag"><%=XaGender%></label>
				</div>
				<%End If%>
				<%If Not String.IsNullOrEmpty(XaCitizenship) Then%>
				<div class="checkbox-btn-wrapper">
					<span>CITIZENSHIP</span>
					<label class="toggle-btn pull-right <%=XaCitizenship%>-flag"><%=XaCitizenship%></label>
				</div>
				<%End If%>
				<%If Not String.IsNullOrEmpty(XaOccupancy) Then%>
				<div class="checkbox-btn-wrapper">
					<span>OCCUPANCY_SECONDARY</span>
					<label class="toggle-btn pull-right <%=XaOccupancy%>-flag"><%=XaOccupancy%></label>
				</div>
				<%End If%>

				<%If Not String.IsNullOrEmpty(XaMaritalStatus) Then%>
				<div class="checkbox-btn-wrapper">
					<span>MARITAL STATUS</span>
					<label class="toggle-btn pull-right <%=XaMaritalStatus%>-flag"><%=XaMaritalStatus%></label>
				</div>
				<%End If%>
				
				<%If Not String.IsNullOrEmpty(XaEmployment) Then%>
				<div class="checkbox-btn-wrapper">
					<span>EMPLOYMENT</span>
					<label class="toggle-btn pull-right <%=XaEmployment%>-flag"><%=XaEmployment%></label>
				</div>
				<%End If%>
				<%If Not String.IsNullOrEmpty(XaOccupancy) Then%>
				<div class="checkbox-btn-wrapper">
					<span>OCCUPANCY STATUS</span>
					<label class="toggle-btn pull-right <%=XaOccupancy%>-flag"><%=XaOccupancy%></label>
				</div>
				<%End If%>
			</div>
			<div class="checkbox-group">
				<p><i class="fa fa-caret-right" aria-hidden="true"></i><span>Minor Xpress Account</span></p>
				<%If Not String.IsNullOrEmpty(XaEmploymentMinor) Then%>
				<div class="checkbox-btn-wrapper">
					<span>EMPLOYMENT</span>
					<label class="toggle-btn pull-right <%=XaEmploymentMinor%>-flag"><%=XaEmploymentMinor%></label>
				</div>
				<%End If%>
			</div>
			<div class="checkbox-group">
				<p><i class="fa fa-caret-right" aria-hidden="true"></i><span>Secondary Xpress Account</span></p>
				<%If Not String.IsNullOrEmpty(XaMotherMaidenNameSecondary) Then%>
				<div class="checkbox-btn-wrapper">
					<span>MOTHER_MAIDEN_NAME_SECONDARY</span>
					<label class="toggle-btn pull-right <%=XaMotherMaidenNameSecondary %>-flag"><%=XaMotherMaidenNameSecondary %></label>
				</div>
				<%End If%>
				<%If Not String.IsNullOrEmpty(XaCitizenshipSecondary) Then%>
				<div class="checkbox-btn-wrapper">
					<span>CITIZENSHIP_SECONDARY</span>
					<label class="toggle-btn pull-right <%=XaCitizenshipSecondary%>-flag"><%=XaCitizenshipSecondary%></label>
				</div>
				<%End If%>
				<%If Not String.IsNullOrEmpty(XaEmploymentSecondary) Then%>
				<div class="checkbox-btn-wrapper">
					<span>EMPLOYMENT_SECONDARY</span>
					<label class="toggle-btn pull-right <%=XaEmploymentSecondary%>-flag"><%=XaEmploymentSecondary%></label>
				</div>
				<%End If%>
				<%If Not String.IsNullOrEmpty(XaGrossIncomeSecondary) Then%>
				<div class="checkbox-btn-wrapper">
					<span>GROSS_INCOME_SECONDARY</span>
					<label class="toggle-btn pull-right <%=XaGrossIncomeSecondary %>-flag"><%=XaGrossIncomeSecondary %></label>
				</div>
				<%End If%>
				<%If Not String.IsNullOrEmpty(XaIDPageSecondary) Then%>
				<div class="checkbox-btn-wrapper">
					<span>ID_PAGE_SECONDARY</span>
					<label class="toggle-btn pull-right <%=XaIDPageSecondary %>-flag"><%=XaIDPageSecondary %></label>
				</div>
				<%End If%>
				<%If Not String.IsNullOrEmpty(XaOccupancySecondary) Then%>
				<div class="checkbox-btn-wrapper">
					<span>OCCUPANCY_SECONDARY</span>
					<label class="toggle-btn pull-right <%=XaOccupancySecondary%>-flag"><%=XaOccupancySecondary%></label>
				</div>
				<%End If%>
				<%If Not String.IsNullOrEmpty(XaDisclosuresSecondaryEnable) Then%>
				<div class="checkbox-btn-wrapper">
					<span>DISCLOSURES_SECONDARY_ENABLE</span>
					<label class="toggle-btn pull-right <%=XaDisclosuresSecondaryEnable %>-flag"><%=XaDisclosuresSecondaryEnable %></label>
				</div>
				<%End If%>
				<%If Not String.IsNullOrEmpty(XaMaritalStatus) Then%>
				<div class="checkbox-btn-wrapper">
					<span>MARITAL STATUS</span>
					<label class="toggle-btn pull-right <%=XaMaritalStatus%>-flag"><%=XaMaritalStatus%></label>
				</div>
				<%End If%>
				<%If Not String.IsNullOrEmpty(XaEnableMemberNumber) Then%>
				<div class="checkbox-btn-wrapper">
					<span>MEMBER NUMBER</span>
					<label class="toggle-btn pull-right <%=XaEnableMemberNumber%>-flag"><%=XaEnableMemberNumber%></label>
				</div>
				<%End If%>
			</div>
			<div class="checkbox-group">
				<p><i class="fa fa-caret-right" aria-hidden="true"></i><span>Loans</span></p>
				<%If Not String.IsNullOrEmpty(LoanBranchSelectionDropdown) Then%>
				<div class="checkbox-btn-wrapper">
					<span>BRANCH_SELECTION_DROPDOWN</span>
					<label class="toggle-btn pull-right <%=LoanBranchSelectionDropdown %>-flag"><%=LoanBranchSelectionDropdown %></label>
				</div>
				<%End If%>
			</div>
			<%If Not String.IsNullOrEmpty(CCIdentificationSection) Then%>
			<div class="checkbox-group">
				<p><i class="fa fa-caret-right" aria-hidden="true"></i><span>Credit Card Loan</span></p>
				<div class="checkbox-btn-wrapper">
					<span>IDENTIFICATION_SECTION</span>
					<label class="toggle-btn pull-right <%=CCIdentificationSection%>-flag"><%=CCIdentificationSection%></label>
				</div>
			</div>
			<%End If%>
			<%If Not String.IsNullOrEmpty(HEIdentificationSection) Then%>
			<div class="checkbox-group">
				<p><i class="fa fa-caret-right" aria-hidden="true"></i><span>Home Equity Loan</span></p>
				<div class="checkbox-btn-wrapper">
					<span>IDENTIFICATION_SECTION</span>
					<label class="toggle-btn pull-right <%=HEIdentificationSection%>-flag"><%=HEIdentificationSection%></label>
				</div>
			</div>
			<%End If%>
			<%If Not String.IsNullOrEmpty(PLIdentificationSection) Then%>
			<div class="checkbox-group">
				<p><i class="fa fa-caret-right" aria-hidden="true"></i><span>Personal Loan</span></p>
				<div class="checkbox-btn-wrapper">
					<span>IDENTIFICATION_SECTION</span>
					<label class="toggle-btn pull-right <%=PLIdentificationSection %>-flag"><%=PLIdentificationSection %></label>
				</div>
			</div>
			<%End If%>
			<%If Not String.IsNullOrEmpty(VLIdentificationSection) Then%>
			<div class="checkbox-group">
				<p><i class="fa fa-caret-right" aria-hidden="true"></i><span>Vehicle Loan</span></p>
				<div class="checkbox-btn-wrapper">
					<span>IDENTIFICATION_SECTION</span>
					<label class="toggle-btn pull-right <%=VLIdentificationSection %>-flag"><%=VLIdentificationSection %></label>
				</div>
			</div>
			<%End If%>
			<button id="btnMigrateShowFieldsData" class="btn btn-warning mb10 mr5"><i class="fa fa-play" aria-hidden="true"></i>RUN BATCH</button>
		</section>
	</div>
</asp:Content>
<asp:Content runat="server" ContentPlaceHolderID="plScripts">
	<script type="text/javascript">
		$(function () {
			$("#btnMigrateShowFieldsData").on("click", function() {
				_COMMON.showConfirmDialog("Are you sure you want to perform this action?", function () {
					//yes
					var dataObj = {};
					dataObj.command = "runmigratebatch";
					dataObj.lenderconfigid = '<%=LenderConfigID%>';
					$.ajax({
						url: "/sm/migrateconfig.aspx",
						async: true,
						cache: false,
						type: 'POST',
						dataType: 'html',
						data: dataObj,
						success: function (responseText) {
							var response = $.parseJSON(responseText);
							if (response.IsSuccess) {
								_COMMON.noty("success", "Success.", 500);
							} else {
								_COMMON.noty("error", "Error", 500);
							}
						}
					});
				}, function () {//no
				}, "Yes, do it", "Cancel");
			});
		});
		
	</script>
</asp:Content>
<asp:Content runat="server" ContentPlaceHolderID="plActionButtons"></asp:Content>