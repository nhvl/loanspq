﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="Settings.aspx.vb" Inherits="Sm_Settings" MasterPageFile="SiteManager.master"  %>

<asp:Content runat="server" ContentPlaceHolderID="plBody">
	<div>
		<h1 class="page-header">Settings</h1>
		<p>Configure settings for your Application Portal</p>
		<section>
			<ul class="nav nav-tabs" id="mainTabs" role="tablist">
				<li role="presentation" class="active" data-preview-type="<%=DefaultPreviewModuleName%>"><a href="#general" aria-controls="general" role="tab" data-toggle="tab">General Settings</a></li>
				<li role="presentation" <%=IIf(CheckEnabledModule(SmSettings.ModuleName.XA), "", "class='hidden'")%> data-preview-type="xa"><a href="#xa" aria-controls="accounts" role="tab" data-toggle="tab">XA</a></li>
				<li role="presentation" <%=IIf(CheckEnabledModule(SmSettings.ModuleName.CC) Or CheckEnabledModule(SmSettings.ModuleName.PL) Or CheckEnabledModule(SmSettings.ModuleName.VL) Or CheckEnabledModule(SmSettings.ModuleName.HE), "", "class='hidden'")%>    data-preview-type="<%=DefaultPreviewLoanName%>"><a href="#loans" aria-controls="loans" role="tab" data-toggle="tab">All Loans</a></li>
				<li role="presentation" <%=IIf(CheckEnabledModule(SmSettings.ModuleName.CC), "", "class='hidden'")%> data-preview-type="cc"><a href="#ccloans" aria-controls="loans" role="tab" data-toggle="tab">Credit Card</a></li>
				<li role="presentation" <%=IIf(CheckEnabledModule(SmSettings.ModuleName.HE), "", "class='hidden'")%> data-preview-type="he"><a href="#heloans" aria-controls="loans" role="tab" data-toggle="tab">Home Equity Loan</a></li>
				<li role="presentation" <%=IIf(CheckEnabledModule(SmSettings.ModuleName.PL), "", "class='hidden'")%> data-preview-type="pl"><a href="#plloans" aria-controls="loans" role="tab" data-toggle="tab">Personal Loan</a></li>
				<li role="presentation" <%=IIf(CheckEnabledModule(SmSettings.ModuleName.VL), "", "class='hidden'")%> data-preview-type="vl"><a href="#vlloans" aria-controls="loans" role="tab" data-toggle="tab">Vehicle Loan</a></li>
			    <li role="presentation" <%=IIf(CheckEnabledModule(SmSettings.ModuleName.BL), "", "class='hidden'")%> data-preview-type="bl"><a href="#blloans" aria-controls="loans" role="tab" data-toggle="tab">Business Loan</a></li>

            </ul>
			<div class="tab-content">
				<div role="tabpanel" class="tab-pane active" id="general">
					<h2 class="section-title">General Settings</h2>
					<p>These settings will affect both XA and loan applications.</p>
					<div class="bottom30">
						<div class="group-heading">INTEGRATIONS</div>
						<%If ApmDLScanEnable Then%>
						<div class="input-block">
							<h3 class="property-title">License Scan</h3>
							<p>This feature allows users to pre-fill their applications by scanning their driver's license.</p>
							<label class="toggle-btn" data-on="ON" data-off="OFF">
								<input type="checkbox" value="" <%=BindCheckbox(LicenseScan)%> />
								<span class="button-checkbox"></span>
							</label>
						</div>
						<%End If%>
						<%If SmUtil.CheckRoleAccess(Context, SmSettings.Role.LegacyOperator, SmSettings.Role.SuperOperator) Then%>
						<div class="input-block">
							<h3 class="property-title">Laser License Scan</h3>
							<p>Enterprise-Grade barcode scanner with performance and precision of dedicated laser scanner <br />(beta, only visible to legacy operator)</p>
							<label class="toggle-btn" data-on="ON" data-off="OFF">
								<input type="checkbox" value="dl_barcode_scan" id="chkDLBarcodeScan" <%=BindCheckbox(DLBarcodeScan)%>/>
								<span class="button-checkbox"></span>
							</label>
						</div>
						<%End If%>
						<%If ApmLinkedInEnable Then %>
						<div class="input-block">
							<h3 class="property-title">LinkedIn Login</h3>
							<p>This feature allows applicants to pre-fill their application with information from LinkedIn.</p>
							<label class="toggle-btn" data-on="ON" data-off="OFF">
								<input type="checkbox" id="chkLinkedInEnable" value="linkedin_enable" <%=BindCheckbox(LinkedInEnable)%>/>
								<span class="button-checkbox"></span>
							</label>
						</div>
						<%End If%>

						<div class="input-block">
							<h3 class="property-title">MeridianLink Address Verification</h3>
							<p>This feature enables the system to auto verify addresses.</p>
							<label class="toggle-btn disabled" data-on="ON" data-off="OFF">
								<input type="checkbox" disabled="disabled" value="address_key" <%=BindCheckbox(AddressKey)%>/>
								<span class="button-checkbox"></span>
							</label>
							<%--<div class="button-caption">(If interested, please contact Support via support@loanspq.com. <br />SOW is required if this hasn't been enabled for in-branch.)</div>--%>
						</div>

                        <%If ApmSiteAnalyticsEnable Then%>
					    <div class="input-block">
								<h3 class="property-title">Site Analytics</h3>
								<p>This feature enables site analytics for your Application Portal.</p>
                                <p>MeridianLink Analytics ID</p>
                                <label class="">
									<input type="text" class="form-control" id="txtPiwikSiteId" value="<%=PiwikSiteId%>" />
								    <span class="button-checkbox"></span>
							    </label>
                                <p>Google Tag Manager ID</p>
								<label class="">
									<input type="text" class="form-control" id="txtGoogleTagManagerId" value="<%=GoogleTagManagerId%>" />
									<span class="button-checkbox"></span>
								</label>							
						</div>									    
					    <%End If%>

						<%If SmUtil.CheckRoleAccess(Context, SmSettings.Role.LegacyOperator, SmSettings.Role.SuperOperator) Then%>
						<div class="input-block">
							<h3 class="property-title">Homepage Editor</h3>
							<p>This feature allows users to create new or select existing one for edit with drag & drop.(beta, only visible to legacy operator)</p>
							<label class="toggle-btn" data-on="ON" data-off="OFF">
								<input type="checkbox" value="homepage_editor" id="chkHomepageEditor" <%=BindCheckbox(HomepageEditor)%>/>
								<span class="button-checkbox"></span>
							</label>
							<%--<div class="button-caption">(If interested, please contact Support via support@loanspq.com. <br />SOW is required if this hasn't been enabled for in-branch.)</div>--%>
						</div>
						<%End If%>
					</div>
					<div class="bottom30">
						<div class="group-heading">WORKFLOWS</div>
						<div class="input-block">
							<h3 class="property-title">Foreign Address</h3>
							<p>Allow your applicants to apply with a foreign address</p>
							<label class="toggle-btn" data-on="ON" data-off="OFF">
								<input type="checkbox" id="chkForeignAddress" value="foreign_address" <%=BindCheckbox(ForeignAddress)%>/>
								<span class="button-checkbox"></span>
							</label>
						</div>
						<div class="input-block">
							<h3 class="property-title">Foreign Phone</h3>
							<p>Allow your applicants to apply with a foreign phone</p>
							<label class="toggle-btn" data-on="ON" data-off="OFF">
								<input type="checkbox" id="chkForeignPhone" value="foreign_phone" <%=BindCheckbox(ForeignPhone)%>/>
								<span class="button-checkbox"></span>
							</label>
						</div>
						<div class="input-block">
							<h3 class="property-title">MFA Email Only</h3>
							<p>This will use email field and will hide "Confirm your identity via SSN instead of email?" link button in the Status page.</p>
							<label class="toggle-btn" data-on="ON" data-off="OFF">
								<input type="checkbox" id="chkMfaEmailOnly" value="mfa_email_only" <%=BindCheckbox(MfaEmailOnly)%>/>
								<span class="button-checkbox"></span>
							</label>
						</div>
					</div>
					
					<%--<div class="bottom30">
						<div class="input-block">
							<div class="group-heading">SOCIAL & TRACKING</div>
							<h3 class="property-title">Piwik Site</h3>
							<p>Enter Piwik Site ID here.</p>
							<input type="text" data-name="piwik-site-id" class="sm-textbox form-control"/>
						</div>
						<div class="input-block">
							<h3 class="property-title">Google Tag Manager</h3>
							<p>Enter Google Tag Manager ID here.</p>
							<input type="text" data-name="google-tag-manager-id" class="sm-textbox form-control"/>	
						</div>
					</div>--%>
				</div>

				<%If EnableXA Then%>
				<div role="tabpanel" class="tab-pane" id="xa">
					<h2 class="section-title">Xpress Account</h2>
					<p>These settings will only affect Xpress Account (XA).</p>
					<div class="bottom30">
						<div class="group-heading">INTEGRATIONS</div>
						<div class="input-block">
							<h3 class="property-title">Document Upload</h3>
							<p>Allow your applicants to capture/upload images or documents.<%--<br />(This feature is enabled automatically when the message is not empty.)--%></p>
							<label class="toggle-btn <%=BindCheckbox(XAUploadDocEnable)%>" data-on="ON" data-off="OFF">
								<input type="checkbox" id="chkXAUploadDocEnable" value="" <%=BindCheckbox(XAUploadDocEnable)%>/>
								<span class="button-checkbox"></span>
							</label>
							<div class="hint-content">
								<p>Message (required for feature to show)</p>
								<div class="html-editor" id="accountUploadDocMsg" contenteditable="true" ><%=XAUploadDocMsg%></div>
							</div>
						</div>
					</div>
					
					<div class="bottom30">
						<div class="group-heading">IN-BRANCH CONFIGURE SITE</div>	
						<div class="input-block">
							<h3 class="property-title">ID Authentication</h3>
							<p>Verify your applicants' identities through a third party authentication vendor  <br />(This must be enabled with credentials in Interface Preferences as well as the Integration Info tab per product.)</p>
							<select id="ddlIDAuth" class="form-control inline-select">
								<option value="" <%=BindSelectbox(Ida, "")%>>Not set</option>
								<% For Each item As KeyValuePair(Of String, String) In SmSettings.IDAList%>
								<option <%=BindSelectbox(Ida, item.Key)%> value="<%=item.Key%>"><%=item.Value%></option>
								<%Next%>
							</select>
							<%--<label class="toggle-btn" data-on="ON" data-off="OFF">
								<input type="checkbox" value=""/>
								<span class="button-checkbox"></span>
							</label>--%>
							
						</div>
						<div class="input-block">
							<h3 class="property-title">Debit Bureau</h3>
							<p>Run Debit Bureau checks through a third party vendor  <br />(This must be enabled with credentials in Interface Preferences as well as the Integration Info tab per product.)</p>
							<%--<label class="toggle-btn disabled" data-on="ON" data-off="OFF">
								<input type="checkbox" disabled="disabled" value="" <%=BindCheckbox(Debit)%>/>
								<span class="button-checkbox"></span>
							</label>--%>
							<select id="ddlDebitBureau" class="form-control inline-select">
								<option value="" <%=BindSelectbox(Debit, "")%>>Not set</option>
								<% For Each item As KeyValuePair(Of String, String) In SmSettings.DebitList%>
								<option <%=BindSelectbox(Debit, item.Key)%> value="<%=item.Key%>"><%=item.Value%></option>
								<%Next%>
							</select>
							
						</div>
						<div class="input-block">
							<h3 class="property-title">Credit Pull</h3>
							<p>Pull applicant's credit report  <br />(This must be enabled with credentials in Interface Preferences as well as the Integration Info tab per product.)</p>
							<label class="toggle-btn <%=BindCheckbox(CreditPull)%>" data-on="ON" data-off="OFF">
								<input type="checkbox" id="chkCreditPull" value="" <%=BindCheckbox(CreditPull)%>/>
								<span class="button-checkbox"></span>
							</label>
							<div class="hint-content">  <%--this is controlled by credit pull--%>
								<h3 class="property-title">Decision XA</h3>
								<p>Underwrite product</p>
								<label class="toggle-btn" data-on="ON" data-off="OFF">
									<input type="checkbox" id="chkDecisionXaEnabled" value="" <%=BindCheckbox(DecisionXAEnabled)%>/>
									<span class="button-checkbox"></span>
								</label>
							</div>
						</div>

						<div class="input-block">
							<h3 class="property-title">XA Status</h3>
							<p>System shall change XA to this status after successfully completed underwrite process.  <br />If underwrite process fails, system will automatically set  XA status to referral.</p>
							<select id="ddlXAStatus" class="form-control inline-select">
								<option value="" <%=BindSelectbox(XALoanStatus, "")%>>Not set</option>
								<% For Each item As KeyValuePair(Of String, String) In SmSettings.LoanStatusList%>
								<option <%=BindSelectbox(XALoanStatus, item.Key)%> value="<%=item.Key%>"><%=item.Value%></option>
								<%Next%>
							</select>							
						</div>

						<div class="input-block">
								<h3 class="property-title">Funding</h3>
								<p>Stand-alone funding without booking</p>
								<label class="toggle-btn" data-on="ON" data-off="OFF">
									<input type="checkbox" id="chkXAFunding" value="" <%=BindCheckbox(XAFunding)%>/>
								<span class="button-checkbox"></span>
							</label>							
						</div>

						<div class="input-block">
							<h3 class="property-title">Booking</h3>
							<p>Book application to core</p>
							<select id="ddlXABooking" class="form-control inline-select disabled" disabled="disabled">
								<option value="" <%=BindSelectbox(XABooking, "")%>>Not set</option>
								<% For Each item As KeyValuePair(Of String, String) In SmSettings.BookList%>
								<option <%=BindSelectbox(XABooking, item.Key)%> value="<%=item.Key%>"><%=item.Value%></option>
								<%Next%>
							</select>
												
						</div>

						<%--<div class="input-block">
								<h3 class="property-title">Auto Cross Qualify</h3>
								<p>Allow the system to show other products that applicant may qualify</p>
								<label class="toggle-btn" data-on="ON" data-off="OFF">
									<input type="checkbox" id="chkXAXSell" value="" <%=BindCheckbox(XAXSell)%>/>
								<span class="button-checkbox"></span>
							</label>							
						</div>--%>
						
						<div class="input-block">
								<h3 class="property-title">In-Session DocuSign</h3>
								<p>Show document for applicant to sign <br />(This must be enabled with credentials for in-branch.)</p>
								<label class="toggle-btn" data-on="ON" data-off="OFF">
									<input type="checkbox" id="chkXADocuSign" value="" <%=BindCheckbox(XADocuSign)%>/>
								<span class="button-checkbox"></span>
							</label>							
						</div>
						
						<div class="input-block">
								<h3 class="property-title">In-Session DocuSign PDF Group Override</h3>
								<p>System uses this PDF group instead of the default AUTO_CONSUMER_DOCUSIGN PDF group.</p>
								<p>The Application Portal requires all PDFs that are tied to account level to be in this PDF group - use any other PDF groups for PDFs that are tied to product(s).</p> 
								<label class="">
									<input type="text" class="form-control" id="txtXADocuSignGroup" value="<%=XADocuSignPDFGroup%>" />
								<span class="button-checkbox"></span>
							</label>							
						</div>

					</div> <%--''BACK OFFICE INTEGRATIONS--%>

					<div class="bottom30">
						<div class="group-heading">WORKFLOWS</div>
						<div class="input-block">
							<h3 class="property-title">Joint Applicants (New <%:IIf(IsBankPortal, "Account", "Member")%>)</h3>
							<p>Allow your applicants to apply for a joint Xpress Account.</p>
							<label class="toggle-btn" data-on="ON" data-off="OFF">
								<input type="checkbox" id="chkXaJointEnable" value="" <%=BindCheckbox(XAJointEnable)%>/>
								<span class="button-checkbox"></span>
							</label>
						</div>
						<div class="input-block">
							<h3 class="property-title">Joint Applicants (Existing <%:IIf(IsBankPortal, "Account", "Member")%>)</h3>
							<p>Allow your applicants to apply for a joint secondary Xpress Account.</p>
							<label class="toggle-btn" data-on="ON" data-off="OFF">
								<input type="checkbox" id="chkSaJointEnable" value="" <%=BindCheckbox(SAJointEnable)%>/>
								<span class="button-checkbox"></span>
							</label>
						</div>
						<div class="input-block">
							<h3 class="property-title">Location Pool Filter</h3>
							<p>Show available product(s) based on zip code. Zip pools and applicable settings are configured in the in-branch settings.</p>
							<label class="toggle-btn" data-on="ON" data-off="OFF">
								<input type="checkbox" id="chkXaLocationPool" value="" <%=BindCheckbox(XALocationPool)%>/>
								<span class="button-checkbox"></span>
							</label>
						</div>
						<%--<div class="input-block">
							<h3 class="property-title">Allows Minor Accounts</h3>
							<p>Allow your applicants to apply for a minor account. (Note: make sure you activate your minor account products, if enabled)</p>
							<label class="toggle-btn" data-on="ON" data-off="OFF">
								<input type="checkbox" value=""/>
								<span class="button-checkbox"></span>
							</label>
						</div>
						<div class="input-block">
							<h3 class="property-title">Allows Funding via Mail In Check</h3>
							<p>Allow your applicants to fund their account by sending a check to your institution.</p>
							<label class="toggle-btn" data-on="ON" data-off="OFF">
								<input type="checkbox" value=""/>
								<span class="button-checkbox"></span>
							</label>
						</div>--%>
					</div>
				</div>
				<%End If%>

				<%If EnableCC Or EnableHE Or EnablePL Or EnableVL Then%>
				<div role="tabpanel" class="tab-pane" id="loans">
					<h2 class="section-title">Loans</h2>
					<p>These settings will only affect Loan applications (CC, HE, PL, VL).</p>
					<div class="bottom30">
						<div class="group-heading">INTEGRATIONS</div>
						<div class="input-block">
							<h3 class="property-title">Document Upload</h3>
							<p>Allow your applicants to capture/upload images or documents<%--<br />(This feature is enabled automatically when the message is not empty.)--%></p>
							<label class="toggle-btn <%=BindCheckbox(UploadDocEnable)%>" data-on="ON" data-off="OFF">
								<input type="checkbox" id="chkUploadDocEnable" value="" <%=BindCheckbox(UploadDocEnable)%>/>
								<span class="button-checkbox"></span>
							</label>
							<div class="hint-content">
								<p>Message (required for feature to show)</p>
								<div class="html-editor" id="uploadDocMsg" contenteditable="true" ><%=UploadDocMsg%></div>
							</div>
						</div>
					</div>
				</div>
				<%End If%>

				<%If EnableCC Then%>
				<div role="tabpanel" class="tab-pane" id="ccloans">
					<h2 class="section-title">Credit Cards</h2>
					<p>These settings will only effect Credit Card applications.</p>
					<div class="bottom30">
						<div class="group-heading">IN-BRANCH CONFIGURE SITE</div>	
						
						<%--<div class="input-block">
								<h3 class="property-title">Auto Cross Qualify</h3>
								<p>Allow the system to show other products that applicant may qualify</p>
								<label class="toggle-btn" data-on="ON" data-off="OFF">
									<input type="checkbox" id="chkCCXSell" value="" <%=BindCheckbox(CCXSell)%>/>
								<span class="button-checkbox"></span>
							</label>							
						</div>--%>
						
						<div class="input-block">
								<h3 class="property-title">In-Session DocuSign</h3>
								<p>Show document for applicant to sign <br />(This must be enabled with credentials for in-branch.)</p>
								<label class="toggle-btn" data-on="ON" data-off="OFF">
									<input type="checkbox" id="chkCCDocuSign" value="" <%=BindCheckbox(CCDocuSign)%>/>
								<span class="button-checkbox"></span>
							</label>							
						</div>

						<div class="input-block">
								<h3 class="property-title">In-Session DocuSign PDF Group Override</h3>
								<p>System uses this PDF group instead of the default.</p>
								<label class="">
									<input type="text" class="form-control" id="txtCCDocuSignGroup" value="<%=CCDocuSignPDFGroup%>" />
								<span class="button-checkbox"></span>
							</label>							
						</div>
					</div>

					<div class="bottom30">
						<div class="group-heading">WORKFLOWS</div>
						<div class="input-block">
							<h3 class="property-title">Joint Applicants</h3>
							<p>Allow your applicants to apply for a joint credit card.</p>
							<label class="toggle-btn" data-on="ON" data-off="OFF">
								<input type="checkbox" id="chkCcJointEnable" value="" <%=BindCheckbox(CCJointEnable)%>/>
								<span class="button-checkbox"></span>
							</label>
						</div>
					</div>
				</div>
				<%End If%>

				<%If EnableHE Then%>
				<div role="tabpanel" class="tab-pane" id="heloans">
					<h2 class="section-title">Home Equity Loans</h2>
					<p>These settings will only affect Home Equity Loan applications.</p>
					<div class="bottom30">
						<div class="group-heading">IN-BRANCH CONFIGURE SITE</div>							
						<%--<div class="input-block">
							<h3 class="property-title">Booking</h3>
							<p>Book application to core <br/>(If interested, please contact Support via support@loanspq.com. SOW is required.)</p>
							<select id="Select1" class="form-control inline-select" disabled="disabled">
								<option value="" <%=BindSelectbox(HEBooking, "")%>>Not set</option>
								<% For Each item As KeyValuePair(Of String, String) In SmSettings.BookList%>
								<option <%=BindSelectbox(HEBooking, item.Key)%> value="<%=item.Key%>"><%=item.Value%></option>
								<%Next%>
							</select>
												
						</div>
						
						<div class="input-block">
								<h3 class="property-title">Auto Cross Qualify</h3>
								<p>Allow the system to show other products that applicant may qualify</p>
								<label class="toggle-btn" data-on="ON" data-off="OFF">
									<input type="checkbox" id="chkHEXSell" value="" <%=BindCheckbox(HEXSell)%>/>
								<span class="button-checkbox"></span>
							</label>							
						</div>--%>
						
						<div class="input-block">
								<h3 class="property-title">In-Session DocuSign</h3>
								<p>Show document for applicant to sign <br />(This must be enabled with credentials for in-branch.)</p>
								<label class="toggle-btn" data-on="ON" data-off="OFF">
									<input type="checkbox" id="chkHEDocuSign" value="" <%=BindCheckbox(HEDocuSign)%>/>
								<span class="button-checkbox"></span>
							</label>							
						</div>

						<div class="input-block">
								<h3 class="property-title">In-Session DocuSign PDF Group Override</h3>
								<p>System uses this PDF group instead of the default</p>
								<label class="">
									<input type="text" class="form-control" id="txtHEDocuSignGroup" value="<%=HEDocuSignPDFGroup%>" />
								<span class="button-checkbox"></span>
							</label>							
						</div>
					</div>
					<div class="bottom30">
						<div class="group-heading">WORKFLOWS</div>
						<div class="input-block">
							<h3 class="property-title">Joint Applicants</h3>
							<p>Allow your applicants to apply for a joint loan.</p>
							<label class="toggle-btn" data-on="ON" data-off="OFF">
								<input type="checkbox" id="chkHeJointEnable" value="" <%=BindCheckbox(HEJointEnable)%>/>
								<span class="button-checkbox"></span>
							</label>
						</div>
					</div>
					<div class="bottom30">
						<div class="group-heading">MISC VALUES</div>
						<div class="input-block">
							<h3 class="property-title">Interest Rate</h3>
							<p>Estimated payment calculator default rate.</p>
							<input type="text" class="form-control inline-textbox number float-number" id="txtHERate" value="<%=HEInterestRate%>" /><span> %</span>
						</div>
					</div>
				</div>
				<%End If%>

				<%If EnablePL Then%>
				<div role="tabpanel" class="tab-pane" id="plloans">
					<h2 class="section-title">Personal Loans</h2>
					<p>These settings will only affect Personal Loan applications.</p>
					<div class="bottom30">
						<div class="group-heading">IN-BRANCH CONFIGURE SITE</div>						
						<%--<div class="input-block">
							<h3 class="property-title">Booking</h3>
							<p>Book application to core <br/>(If interested, please contact Support via support@loanspq.com. SOW is required.)</p>
							<select id="Select2" class="form-control inline-select" disabled="disabled">
								<option value="" <%=BindSelectbox(PLBooking, "")%>>Not set</option>
								<% For Each item As KeyValuePair(Of String, String) In SmSettings.BookList%>
								<option <%=BindSelectbox(PLBooking, item.Key)%> value="<%=item.Key%>"><%=item.Value%></option>
								<%Next%>
							</select>
												
						</div>
						
						<div class="input-block">
								<h3 class="property-title">Auto Cross Qualify</h3>
								<p>Allow the system to show other products that applicant may qualify</p>
								<label class="toggle-btn" data-on="ON" data-off="OFF">
									<input type="checkbox" id="chkPLXSell" value="" <%=BindCheckbox(PLXSell)%>/>
								<span class="button-checkbox"></span>
							</label>							
						</div>--%>
						
						<div class="input-block">
								<h3 class="property-title">In-Session DocuSign</h3>
								<p>Show document for applicant to sign <br />(This must be enabled with credentials for in-branch.)</p>
								<label class="toggle-btn" data-on="ON" data-off="OFF">
									<input type="checkbox" id="chkPLDocuSign" value="" <%=BindCheckbox(PLDocuSign)%>/>
								<span class="button-checkbox"></span>
							</label>							
						</div>

						<div class="input-block">
								<h3 class="property-title">In-Session DocuSign PDF Group Override</h3>
								<p>System uses this PDF group instead of the default.</p>
								<label class="">
									<input type="text" class="form-control" id="txtPLDocuSignGroup" value="<%=PLDocuSignPDFGroup%>" />
								<span class="button-checkbox"></span>
							</label>							
						</div>
					</div>
					<div class="bottom30">
						<div class="group-heading">WORKFLOWS</div>
						<div class="input-block">
							<h3 class="property-title">Joint Applicants</h3>
							<p>Allow your applicants to apply for a joint loan.</p>
							<label class="toggle-btn" data-on="ON" data-off="OFF">
								<input type="checkbox" id="chkPlJointEnable" value="" <%=BindCheckbox(PLJointEnable)%>/>
								<span class="button-checkbox"></span>
							</label>
						</div>
					</div>
					<div class="bottom30">
						<div class="group-heading">MISC VALUES</div>
						<div class="input-block">
							<h3 class="property-title">Interest Rate</h3>
							<p>Estimated payment calculator default rate.</p>
							<input type="text" class="form-control inline-textbox number float-number" id="txtPLRate" value="<%=PLInterestRate%>" /><span> %</span>
						</div>
						
					</div>
				</div>
				<%End If%>

				<%If EnableVL Then%>
				<div role="tabpanel" class="tab-pane" id="vlloans">
					<h2 class="section-title">Vehicle Loans</h2>
					<p>These settings will only affect Vehicle Loan applications.</p>
					<%If SmUtil.CheckRoleAccess(Context, SmSettings.Role.LegacyOperator, SmSettings.Role.SuperOperator) Then%>
					<div class="bottom30">
						<div class="group-heading">INTEGRATIONS</div>
						<div class="input-block">
							<h3 class="property-title">Vin Number Laser Barcode Scan</h3>
							<p>Enterprise-Grade barcode scanner with performance and precision of dedicated laser scanner</p>
							<label class="toggle-btn disabled" data-on="ON" data-off="OFF">
								<input type="checkbox" id="chkVLVinBarcodeScan" <%=BindCheckbox(VLVinBarcodeScan)%>/>
								<span class="button-checkbox"></span>
							</label>
							
						</div>
					</div>
					<%End If%>
					<div class="bottom30">
						<div class="group-heading">IN-BRANCH CONFIGURE SITE</div>						
						<%--<div class="input-block">
							<h3 class="property-title">Booking</h3>
							<p>Book application to core <br/>(If interested, please contact Support via support@loanspq.com. SOW is required.)</p>
							<select id="ddlVLBooking" class="form-control inline-select" disabled="disabled">
								<option value="" <%=BindSelectbox(VLBooking, "")%>>Not set</option>
								<% For Each item As KeyValuePair(Of String, String) In SmSettings.BookList%>
								<option <%=BindSelectbox(VLBooking, item.Key)%> value="<%=item.Key%>"><%=item.Value%></option>
								<%Next%>
							</select>
												
						</div>--%>
						
						<%--<div class="input-block">
								<h3 class="property-title">Auto Cross Qualify</h3>
								<p>Allow the system to show other products that applicant may qualify</p>
								<label class="toggle-btn" data-on="ON" data-off="OFF">
									<input type="checkbox" id="chkVLXSell" value="" <%=BindCheckbox(VLXSell)%>/>
								<span class="button-checkbox"></span>
							</label>							
						</div>--%>
						
						<div class="input-block">
								<h3 class="property-title">In-Session DocuSign</h3>
								<p>Show document for applicant to sign <br />(This must be enabled with credentials for in-branch.)</p>
								<label class="toggle-btn" data-on="ON" data-off="OFF">
									<input type="checkbox" id="chkVLDocuSign" value="" <%=BindCheckbox(VLDocuSign)%>/>
								<span class="button-checkbox"></span>
							</label>							
						</div>

						<div class="input-block">
								<h3 class="property-title">In-Session DocuSign PDF Group Override</h3>
								<p>System uses this PDF group instead of the default.</p>
								<label class="">
									<input type="text" class="form-control" id="txtVLDocuSignGroup" value="<%=VLDocuSignPDFGroup%>" />
								<span class="button-checkbox"></span>
							</label>							
						</div>
					</div>

					<div class="bottom30">
						<div class="group-heading">WORKFLOWS</div>
						<div class="input-block">
							<h3 class="property-title">Joint Applicants</h3>
							<p>Allow your applicants to apply for a joint loan.</p>
							<label class="toggle-btn" data-on="ON" data-off="OFF">
								<input type="checkbox" id="chkVlJointEnable" value="" <%=BindCheckbox(VLJointEnable)%>/>
								<span class="button-checkbox"></span>
							</label>
						</div>
                        <div class="input-block">
							<h3 class="property-title">Loan Term Free Form Text</h3>
							<p>Enable free form text instead of dropdown for Loan Term field.  This only apply for Auto/Pickup Vehicle Type.</p>
							<label class="toggle-btn" data-on="ON" data-off="OFF">
								<input type="checkbox" id="chkVLTermFreeFormMode" value="" <%=BindCheckbox(VLTermFreeTextMode)%>/>
								<span class="button-checkbox"></span>
							</label>
						</div>
					</div>
					<div class="bottom30">
						<div class="group-heading">MISC VALUES</div>
						<div class="input-block">
							<h3 class="property-title">Interest Rate</h3>
							<p>Estimated payment calculator default rate.</p>
							<input type="text" class="form-control inline-textbox number float-number" id="txtVLRate" value="<%=VLInterestRate%>" /><span> %</span>
						</div>
						
					</div>
				</div>
				<%End If%>
			</div>
		</section>
	</div>
</asp:Content>
<asp:Content runat="server" ContentPlaceHolderID="plScripts">
	<script type="text/javascript" src="js/ckeditor/ckeditor.js"></script>
	<script type="text/javascript">
		CKEDITOR.disableAutoInline = true;
		$(function () {
			//$(".dynamic-key-panel").dynamicKey();
			$("#chkCreditPull").on("change", function () {
				var $self = $(this);
				if ($self.is(":checked")) {
					$self.closest(".toggle-btn").addClass("checked");
				} else {
					$self.closest(".toggle-btn").removeClass("checked");
				}
			});
			registerDataValidator();
			$('.nav-tabs a').on('shown.bs.tab', function(e) {
				window.location.hash = e.target.hash;
				window.scrollTo(0, 0);
			});
			var url = document.location.toString();
			if (url.match('#')) {
				$('.nav-tabs a[href="#' + url.split('#')[1] + '"]').tab('show');
			}
			$("select,input:checkbox").on("change", function () {
				master.FACTORY.documentChanged(this);
			});
			$(".html-editor").ckEditor({
				onContentChanged: function (editor) { master.FACTORY.documentChanged(editor); },
				onCommandClicked: function (editor, commandBtn) { master.FACTORY.documentChanged(editor); }
			});
			$("input:text").each(function (idx, ele) {
				$(ele).on({
					keypress: function () { master.FACTORY.documentChanged(ele); },
					paste: function () { master.FACTORY.documentChanged(ele); },
					cut: function () { master.FACTORY.documentChanged(ele); }
				});
			});
			$("#chkXAUploadDocEnable").on("change", function() {
				var $self = $(this);
				if ($self.is(":checked")) {
					$self.closest(".toggle-btn").addClass("checked");
				} else {
					$self.closest(".toggle-btn").removeClass("checked");
				}
			});
			$("#chkUploadDocEnable").on("change", function () {
				var $self = $(this);
				if ($self.is(":checked")) {
					$self.closest(".toggle-btn").addClass("checked");
				} else {
					$self.closest(".toggle-btn").removeClass("checked");
				}
			});
		});
		(function (master, $, undefined) {
			master.FACTORY.SaveDraft = function (revisionId) {
				if ($.smValidate("ValidateData") == false) {
					_COMMON.noty("error", "Error: please check all fields.", 500);
					return;
				}

				var dataObj = collectSubmitData();
				dataObj.revisionId = revisionId;
				dataObj.command = 'savesettings';
				$.ajax({
					url: "/sm/smhandler.aspx",
					async: true,
					cache: false,
					type: 'POST',
					dataType: 'html',
					data: dataObj,
					success: function (responseText) {
						var response = $.parseJSON(responseText);
						if (response.IsSuccess) {
							master.FACTORY.onSaveDraftSuccess();
						} else {
							_COMMON.noty("error", "Error", 500);
						}
					}
				});
			};
			master.FACTORY.Preview = function (revisionId) {
				if ($.smValidate("ValidateData") == false) {
					_COMMON.noty("error", "Error: please check all fields.", 500);
					return
				}
				var dataObj = collectSubmitData();
				dataObj.revisionId = revisionId;
				dataObj.command = 'previewsettings';
				$.ajax({
					url: "/sm/smhandler.aspx",
					async: true,
					cache: false,
					type: 'POST',
					dataType: 'html',
					data: dataObj,
					success: function (responseText) {
						var response = $.parseJSON(responseText);
						if (response.IsSuccess) {
							master.FACTORY.onPreviewSuccess();
							_COMMON.openInNewTab(response.Info.previewurl);
						} else {
							_COMMON.noty("error", "Error", 500);
						}
					}
				});
			};
			master.FACTORY.validateBeforePublish = function () {
				var result = $.smValidate("ValidateData");
				if (result == false) {
					_COMMON.noty("error", "Error: please check all fields.", 500);
				}
				return result;
			}
			master.FACTORY.Publish = function (comment, publishAll) {
				if ($.smValidate("ValidateData") == false) {
					_COMMON.noty("error", "Error: please check all fields.", 500);
					return;
				}
				var dataObj = collectSubmitData();
				dataObj.command = 'publishsettings';
				dataObj.comment = comment;
				dataObj.publishAll = publishAll;
				$.ajax({
					url: "/sm/smhandler.aspx",
					async: true,
					cache: false,
					type: 'POST',
					dataType: 'html',
					data: dataObj,
					success: function (responseText) {
						var response = $.parseJSON(responseText);
						if (response.IsSuccess) {
							master.FACTORY.onPublishSuccess();
						} else {
							_COMMON.noty("error", "Error", 500);
						}
					}
				});
			};
		}(window.master = window.master || {}, jQuery));
		function registerDataValidator() {
			$.smValidate.removeValidationGroup("ValidateData");
			$("input.float-number").each(function(idx, input) {
				$(input).observer({
					validators: [
						function (partial) {
							var $self = $(this);							
							if (/^[0-9]+(\.[0-9]+)*$/.test($self.val()) == false) {
								return "Invalid data";
							}
							return "";
						}
					],
					validateOnBlur: true,
					group: "ValidateData"
				});
			});
			
			$("#accountUploadDocMsg").observer({
				validators: [
					function (partial) {
						var $self = $(this);
						var content = CKEDITOR.instances[$self.attr("id")].getData();
						//if ($("#chkXAUploadDocEnable").is(":checked") && $.trim(content) === "") {
						//	return "This field is required";
						//}
						return "";
					}
				],
				validateOnBlur: true,
				group: "ValidateData"
			});

			$("#uploadDocMsg").observer({
				validators: [
					function (partial) {
						var $self = $(this);
						var content = CKEDITOR.instances[$self.attr("id")].getData();
						//if ($("#chkUploadDocEnable").is(":checked") && $.trim(content) === "") {
						//	return "This field is required";
						//}
						return "";
					}
				],
				validateOnBlur: true,
				group: "ValidateData"
			});
		}
		function collectSubmitData() {
			var result = {};
			result.lenderconfigid = '<%=LenderConfigID%>';
			result.dl_barcode_scan = $("#chkDLBarcodeScan").is(":checked");
			result.mfa_email_only = $("#chkMfaEmailOnly").is(":checked");
			result.foreign_address = $("#chkForeignAddress").is(":checked");
			result.foreign_phone = $("#chkForeignPhone").is(":checked");
			<%If ApmLinkedInEnable Then %>
			result.linkedin_enable = $("#chkLinkedInEnable").is(":checked");
			<%End If%>
			result.homepage_editor = $("#chkHomepageEditor").is(":checked");
			<%If ApmSiteAnalyticsEnable Then%>
			result.google_tag_manager_id = $("#txtGoogleTagManagerId").val();
			result.piwik_site_id = $("#txtPiwikSiteId").val();
			<%End If%>
			result.preview_type = $("#mainTabs >li.active").data("preview-type");

			<%If EnableXA Then%>
			result.xa_upload_doc_enable = $("#chkXAUploadDocEnable").is(":checked");
			result.xa_upload_doc_message = CKEDITOR.instances["accountUploadDocMsg"].getData();			
			result.xa_joint_enable = $("#chkXaJointEnable").is(":checked");
			result.sa_joint_enable = $("#chkSaJointEnable").is(":checked");
			result.xa_location_pool = $("#chkXaLocationPool").is(":checked");
			result.credit_pull = $("#chkCreditPull").is(":checked");
			result.decision_xa_enabled = $("#chkDecisionXaEnabled").is(":checked");
			result.ida = $("#ddlIDAuth").val();
			result.debit = $("#ddlDebitBureau").val();
			result.xa_status = $("#ddlXAStatus").val();
			result.xa_funding_enable = $("#chkXAFunding").is(":checked");
			result.xa_booking = $("#ddlXABooking").val();
			//result.xa_xsell = $("#chkXAXSell").is(":checked");
			result.xa_docusign = $("#chkXADocuSign").is(":checked");
			result.xa_docusign_group = $("#txtXADocuSignGroup").val();
			<%end if %>

			<%If EnableCC Or EnableHE Or EnablePL Or EnableVL Then%>
			result.upload_doc_enable = $("#chkUploadDocEnable").is(":checked");
			result.upload_doc_message = CKEDITOR.instances["uploadDocMsg"].getData();
			<%end if %>	

			<%If EnableVL Then%>
			//result.vl_booking = $("#ddlVLBooking").val();
			//result.vl_xsell = $("#chkVLXSell").is(":checked");
			result.vl_docusign = $("#chkVLDocuSign").is(":checked");
			result.vl_docusign_group = $("#txtVLDocuSignGroup").val();
			result.vl_interest_rate = parseFloat($("#txtVLRate").val());
			result.vl_joint_enable = $("#chkVlJointEnable").is(":checked");
			result.vl_vin_barcode_scan_enable = $("#chkVLVinBarcodeScan").is(":checked");
			result.vl_term_free_form_mode = $("#chkVLTermFreeFormMode").is(":checked");
			<%end if %>
			<%If EnableCC Then%>		
			//result.cc_xsell = $("#chkCCXSell").is(":checked");
			result.cc_docusign = $("#chkCCDocuSign").is(":checked");
			result.cc_docusign_group = $("#txtCCDocuSignGroup").val();
			result.cc_joint_enable = $("#chkCcJointEnable").is(":checked");
			<%end if %>

			<%If EnablePL Then%>		
			//result.pl_xsell = $("#chkPLXSell").is(":checked");
			result.pl_docusign = $("#chkPLDocuSign").is(":checked");
			result.pl_docusign_group = $("#txtPLDocuSignGroup").val();
			result.pl_interest_rate = parseFloat($("#txtPLRate").val());
			result.pl_joint_enable = $("#chkPlJointEnable").is(":checked");
			<%end if %>
			<%If EnableHE Then%>	
			//result.he_xsell = $("#chkHEXSell").is(":checked");
			result.he_docusign = $("#chkHEDocuSign").is(":checked");
			result.he_docusign_group = $("#txtHEDocuSignGroup").val();
			result.he_interest_rate = parseFloat($("#txtHERate").val());
			result.he_joint_enable = $("#chkHeJointEnable").is(":checked");
			<%end if %>
			return result;
		}
	</script>
</asp:Content>
<asp:Content runat="server" ContentPlaceHolderID="plActionButtons"></asp:Content>