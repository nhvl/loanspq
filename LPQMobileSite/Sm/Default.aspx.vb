﻿
Partial Class Sm_Default
	Inherits SmBasePage

	Protected BroadcastMsg As String
	Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
		If Not Page.IsPostBack Then
			Dim smMasterPage = CType(Me.Master, Sm_MasterPage)
			smMasterPage.SelectedMainMenuItem = "home"
			Dim smBl As New SmBL()
			BroadcastMsg = smBl.GetLatestBroadcast()
		End If
	End Sub

	Protected Function GetPageUrl(pageName As String) As String
		Return String.Format("{0}{1}", pageName, System.Web.Security.AntiXss.AntiXssEncoder.HtmlEncode(Request.Url.Query, True))
	End Function
End Class
