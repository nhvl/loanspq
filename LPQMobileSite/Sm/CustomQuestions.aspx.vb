﻿
Imports LPQMobile.Utils
Imports System.Xml
Imports Sm
Partial Class Sm_CustomQuestions
	Inherits SmApmBasePage

	
	Protected Property CustomQuestions As New List(Of String)
	Protected Property CustomApplicantQuestions As New List(Of String)
	Protected Property EnabledApplicantCQs As New List(Of String)
    Protected Property EnabledCQs As New List(Of String)
    Protected Property CurrentTab As String = ""
    Protected Property CurrentModule As SmSettings.ModuleName
    Protected Property TabTitle As String = "Custom Questions for"
    Protected Property DisplayPageApplicantOptions As Dictionary(Of String, String)
    Protected Property DisplayPageOptions As Dictionary(Of String, String)
    Private Property ApplicantCQsInfo As New List(Of SmCQInformation)
    Private Property CQsInfo As New List(Of SmCQInformation)

    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then
            Dim smMasterPage = CType(Me.Master, Sm_SiteManager)
            CurrentTab = Common.SafeEncodeString(Request.QueryString("cqtab")).ToUpper()
            If String.IsNullOrWhiteSpace(CurrentTab) Then
                ' detect default loan type in case it's not specified by query string
                If EnableXA Then
                    CurrentTab = "XA"
                ElseIf EnableCC Then
                    CurrentTab = "CC"
                ElseIf EnableHE Then
                    CurrentTab = "HE"
                ElseIf EnablePL Then
                    CurrentTab = "PL"
                ElseIf EnableVL Then
                    CurrentTab = "VL"
                ElseIf EnableBL Then
                    CurrentTab = "BL"
                End If
            End If
            Dim loanNodeName As String = ""
            Dim tabName As String = ""
            Select Case CurrentTab
                Case "CC"
                    CurrentModule = SmSettings.ModuleName.CC
                    loanNodeName = "CREDIT_CARD_LOAN"
                    smMasterPage.CurrentNodeGroup = SmSettings.NodeGroup.CCCustomQuestions
                    tabName = "Credit Card"
                    TabTitle = TabTitle & " Credit Card Applications"
                Case "HE"
                    CurrentModule = SmSettings.ModuleName.HE
                    loanNodeName = "HOME_EQUITY_LOAN"
                    smMasterPage.CurrentNodeGroup = SmSettings.NodeGroup.HECustomQuestions
                    tabName = "Home Equity Loan"
                    TabTitle = TabTitle & " Home Equity Loans"
                Case "PL"
                    CurrentModule = SmSettings.ModuleName.PL
                    loanNodeName = "PERSONAL_LOAN"
                    smMasterPage.CurrentNodeGroup = SmSettings.NodeGroup.PLCustomQuestions
                    tabName = "Personal Loan"
                    TabTitle = TabTitle & " Personal Loans"
                Case "VL"
                    CurrentModule = SmSettings.ModuleName.VL
                    loanNodeName = "VEHICLE_LOAN"
                    smMasterPage.CurrentNodeGroup = SmSettings.NodeGroup.VLCustomQuestions
                    tabName = "Vehicle Loan"
                    TabTitle = TabTitle & " Vehicle Loans"
                Case "XA"
                    CurrentModule = SmSettings.ModuleName.XA
                    loanNodeName = "XA_LOAN"
                    smMasterPage.CurrentNodeGroup = SmSettings.NodeGroup.XACustomQuestions
                    tabName = "XA"
                    TabTitle = TabTitle & " XA"
                Case "BL"
                    CurrentModule = SmSettings.ModuleName.BL
                    loanNodeName = "BUSINESS_LOAN"
                    smMasterPage.CurrentNodeGroup = SmSettings.NodeGroup.BLCustomQuestions
                    tabName = "BL"
                    TabTitle = TabTitle & " Business Loan"
                Case Else
                    CurrentModule = SmSettings.ModuleName.UNDEFINDED
            End Select
            If CurrentModule = SmSettings.ModuleName.UNDEFINDED OrElse Not CheckEnabledModule(CurrentModule) Then
                'terminate current page
                Response.Redirect(BuildUrl("/sm/index.aspx", New List(Of String) From {"cqtab"}), True)
            End If

            smMasterPage.LenderConfigXml = LenderConfigXml
            smMasterPage.SelectedMainMenuItem = "config"
            smMasterPage.RevisionID = LenderConfig.RevisionID
            smMasterPage.LastModifiedBy = LenderConfig.LastModifiedByUserFullName
            smMasterPage.LastModifiedByUserID = LenderConfig.LastModifiedByUserID
            smMasterPage.SelectedSubMenuItem = "customquestion"
            smMasterPage.BreadCrumb = "Configure / Custom Questions / " & tabName
            Try
                Dim copiedWebConfig As CWebsiteConfig = WebsiteConfig
                Dim visibleNode = copiedWebConfig._webConfigXML.SelectSingleNode("VISIBLE")
                If visibleNode Is Nothing Then
                    Dim node As XmlNode = copiedWebConfig._webConfigXML.OwnerDocument.CreateElement("VISIBLE")
                    copiedWebConfig._webConfigXML.AppendChild(node)
                    visibleNode = copiedWebConfig._webConfigXML.SelectSingleNode("VISIBLE")
                End If
                Dim customQuestionNewApiAttrNode As XmlNode = copiedWebConfig._webConfigXML.SelectSingleNode("VISIBLE/@custom_question_new_api")
                If customQuestionNewApiAttrNode Is Nothing Then
                    Dim attr As XmlAttribute = copiedWebConfig._webConfigXML.OwnerDocument.CreateAttribute("custom_question_new_api")
                    attr.Value = "Y"
                    visibleNode.Attributes.Append(attr)
                ElseIf customQuestionNewApiAttrNode.Value <> "Y" Then
                    customQuestionNewApiAttrNode.Value = "Y"
                End If
                Dim applicantCQ As XmlDocument = CDownloadedSettings.CustomApplicantQuestions(copiedWebConfig)
                Dim applicationCQ As XmlDocument = CDownloadedSettings.CustomApplicationQuestions(copiedWebConfig)
                CustomApplicantQuestions = parseCustomQuestionXml(applicantCQ, CurrentTab)
                CustomQuestions = parseCustomQuestionXml(applicationCQ, CurrentTab)

                If CheckEnabledModule(CurrentModule) Then
                    ReadCQInfoFromLiveConfig(CUSTOM_APPLICANT_QUESTIONS(loanNodeName), ApplicantCQsInfo)
                    ReadCQInfoFromLiveConfig(CUSTOM_QUESTIONS(loanNodeName), CQsInfo)
                    DisplayPageApplicantOptions = getDisplayPageOptions(CurrentTab, True)
                    DisplayPageOptions = getDisplayPageOptions(CurrentTab, False)
                End If
            Catch ex As Exception
                log.Error("Could not load CustomQuestions page", ex)
            End Try
        End If
    End Sub
    Private Function parseCustomQuestionXml(xmlDoc As XmlDocument, loanType As String) As List(Of String)
        Dim questions As New List(Of String)
        If xmlDoc IsNot Nothing Then
            Dim questionNodes As XmlNodeList = xmlDoc.SelectNodes("OUTPUT/CUSTOM_QUESTIONS/CUSTOM_QUESTION[contains(@app_source, 'consumer') and @is_active='true']")
            If questionNodes IsNot Nothing AndAlso questionNodes.Count > 0 Then
                For Each question As XmlNode In questionNodes
                    Dim headerAnswerType As XmlNodeList = question.SelectNodes("ANSWER_TYPE[text()='HEADER']")
                    If headerAnswerType IsNot Nothing AndAlso headerAnswerType.Count > 0 Then Continue For

                    Dim xaQuestion As XmlNodeList = question.SelectNodes("APP_TYPES_SUPPORTED/APP_TYPE[text()='" & loanType & "']")
                    If xaQuestion IsNot Nothing AndAlso xaQuestion.Count > 0 Then
                        questions.Add(question.SelectSingleNode("NAME").InnerText)
                    End If
                Next
            End If
        End If
        Return questions
    End Function
    Protected Function GetEnableCQs(ByVal question As String, ByVal isApplicant As Boolean) As Boolean
        If isApplicant Then
            For Each cqInfo As SmCQInformation In ApplicantCQsInfo
                If cqInfo.Question = question Then
                    Return cqInfo.IsEnable
                End If
            Next
        Else
            For Each cqInfo As SmCQInformation In CQsInfo
                If cqInfo.Question = question Then
                    Return cqInfo.IsEnable
                End If
            Next
        End If
        Return False
    End Function
    Protected Function GetPageDisplayCQs(ByVal question As String, ByVal isApplicant As Boolean) As String
        If isApplicant Then
            For Each cqInfo As SmCQInformation In ApplicantCQsInfo
                If cqInfo.Question = question Then
                    Return cqInfo.DisplayPage
                End If
            Next
        Else
            For Each cqInfo As SmCQInformation In CQsInfo
                If cqInfo.Question = question Then
                    Return cqInfo.DisplayPage
                End If
            Next
        End If
        Return SmCQInformation.DefaultDisplayPage
    End Function
    Private Function getDisplayPageOptions(ByVal loanType As String, ByVal isApplicant As Boolean) As Dictionary(Of String, String)
        Dim displayPagesDic As New Dictionary(Of String, String)
        Select Case loanType
            Case "CC"
                displayPagesDic.Add(SmCQInformation.FirstDisplayPage, "Credit Card Info Page")
            Case "XA"
                displayPagesDic.Add(SmCQInformation.FirstDisplayPage, "Product Selection Page")
            Case Else
                displayPagesDic.Add(SmCQInformation.FirstDisplayPage, "Loan Info Page")
        End Select
        displayPagesDic.Add(SmCQInformation.DefaultDisplayPage, If(isApplicant, "Applicant Info Page", "Review Page"))
        Return displayPagesDic
    End Function
    Private Sub ReadCQInfoFromLiveConfig(xPath As String, ByRef destList As List(Of SmCQInformation))
        Dim cqNode As XmlNode = GetNode(xPath)
        If destList Is Nothing Then destList = New List(Of SmCQInformation)
        If cqNode IsNot Nothing AndAlso cqNode.HasChildNodes Then
            For Each node As XmlNode In cqNode.ChildNodes
                If node.Attributes("name").Value Is Nothing Then Continue For
                Dim cqInfo As SmCQInformation = New SmCQInformation With {
                    .Question = node.Attributes("name").Value,
                    .IsEnable = (node.Attributes("is_enable") Is Nothing OrElse node.Attributes("is_enable").Value <> "N"),
                    .DisplayPage = SmCQInformation.DefaultDisplayPage}
                If node.Attributes("display_page") IsNot Nothing AndAlso node.Attributes("display_page").Value <> SmCQInformation.DefaultDisplayPage Then
                    cqInfo.DisplayPage = SmCQInformation.FirstDisplayPage
                End If
                destList.Add(cqInfo)
            Next
        End If
    End Sub
    Protected Function BindHideAllCheckbox(ByVal isApplicant As Boolean) As String
        Dim result As String = ""
        If SmCQInformation.IsHideAll(If(isApplicant, ApplicantCQsInfo, CQsInfo)) Then
            result = "checked='checked'"
        End If
        Return result
    End Function

    Protected Function BindShowAllCheckbox(ByVal isApplicant As Boolean) As String
        Dim result As String = ""
        If SmCQInformation.IsShowAll(If(isApplicant, ApplicantCQsInfo, CQsInfo)) Then
            result = "checked='checked'"
        End If
        Return result
    End Function
End Class
