﻿Imports System.Xml
Imports DownloadedSettings
Imports Sm
Partial Class Sm_ManageCC
	Inherits SmApmBasePage
	Const LOANTYPE As String = "CREDIT_CARD_LOAN"
	Protected Property ApprovalMessage As String
	Protected Property ReferredMessage As String
    Protected Property DeclinedMessage As String
    Protected Property PreQualifiedMessage As String
    Protected Property DisclosureList As New List(Of String)

	Protected Property PurposeList As New Dictionary(Of String, SmTextValueCatItem)(System.StringComparison.OrdinalIgnoreCase)

	Protected Property EnabledPurposeList As New List(Of String)(System.StringComparison.OrdinalIgnoreCase)

	Protected Property CardTypeList As New List(Of String)
	'Protected Property CardNameList As New List(Of String)

	Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
		If Not Page.IsPostBack Then
			Dim smMasterPage = CType(Me.Master, Sm_SiteManager)
			smMasterPage.LenderConfigXml = LenderConfigXml
			smMasterPage.RevisionID = LenderConfig.RevisionID
			smMasterPage.LastModifiedBy = LenderConfig.LastModifiedByUserFullName
			smMasterPage.LastModifiedByUserID = LenderConfig.LastModifiedByUserID
			smMasterPage.SelectedMainMenuItem = "manage"
			smMasterPage.SelectedSubMenuItem = "cc"
			smMasterPage.CurrentNodeGroup = SmSettings.NodeGroup.ManageCC
			smMasterPage.BreadCrumb = "Manage / Credit Card"
			Dim preapprovedMessageNode As XmlNode = GetNode(PREAPPROVED_MESSAGE(LOANTYPE))
			If preapprovedMessageNode IsNot Nothing AndAlso preapprovedMessageNode.NodeType <> XmlNodeType.Comment Then
				ApprovalMessage = SmUtil.MergeMessageKey(preapprovedMessageNode.Attributes("message").InnerText, preapprovedMessageNode)
			End If
			
			Dim submittedMessageNode As XmlNode = GetNode(SUBMITTED_MESSAGE(LOANTYPE))
			If submittedMessageNode IsNot Nothing AndAlso submittedMessageNode.NodeType <> XmlNodeType.Comment Then
				ReferredMessage = SmUtil.MergeMessageKey(submittedMessageNode.Attributes("message").InnerText, submittedMessageNode)
			End If

			Dim declinedMessageNode As XmlNode = GetNode(DECLINED_MESSAGE(LOANTYPE))
			If declinedMessageNode IsNot Nothing AndAlso declinedMessageNode.NodeType <> XmlNodeType.Comment Then
				DeclinedMessage = SmUtil.MergeMessageKey(declinedMessageNode.Attributes("message").InnerText, declinedMessageNode)
			End If
            Dim preQualifiedMessageNode As XmlNode = GetNode(PREQUALIFIED_MESSAGE(LOANTYPE))
            If preQualifiedMessageNode IsNot Nothing AndAlso preQualifiedMessageNode.NodeType <> XmlNodeType.Comment Then
                PreQualifiedMessage = SmUtil.MergeMessageKey(preQualifiedMessageNode.Attributes("message").InnerText, preQualifiedMessageNode)
            End If
            Dim disclosuresNode As XmlNode = GetNode(DISCLOSURES(LOANTYPE))
			If disclosuresNode IsNot Nothing AndAlso disclosuresNode.HasChildNodes Then
				For Each node As XmlNode In disclosuresNode.ChildNodes
					If node.NodeType = XmlNodeType.Comment Then Continue For
					DisclosureList.Add(node.InnerText.Trim())
				Next
			End If


			''********
			''Purpose

			Dim downloadCreditCardPurposes As List(Of DownloadedSettings.CListItem)
			'<OUTPUT><RESPONSE><FIELD_LIST name="Loan Purposes"><LIST_ITEM text="NEW CARD" value="NEW CARD" /><LIST_ITEM text="CREDIT LIMIT INCREASE" value="CREDIT LIMIT INCREASE" /></FIELD_LIST></RESPONSE></OUTPUT>
			downloadCreditCardPurposes = CDownloadedSettings.CreditCardPurposes(WebsiteConfig)
			If downloadCreditCardPurposes IsNot Nothing AndAlso downloadCreditCardPurposes.Any() Then
				For Each item As CListItem In downloadCreditCardPurposes
					If item.Value Is Nothing OrElse item.Value = "" Then Continue For
					If PurposeList.ContainsKey(item.Value) Then Continue For
					PurposeList.Add(item.Value, New SmTextValueCatItem() With {.text = item.Text, .value = item.Value, .category = ""})
				Next
			End If

			Dim enabledPurposesNode As XmlNode = GetNode(CREDIT_CARD_LOAN_PURPOSES())
			If enabledPurposesNode IsNot Nothing AndAlso enabledPurposesNode.HasChildNodes Then
				For Each node As XmlNode In enabledPurposesNode.ChildNodes
					If node.NodeType = XmlNodeType.Comment Then Continue For
					Dim value As String = node.Attributes("value").Value
					If Not EnabledPurposeList.Contains(value) Then
						EnabledPurposeList.Add(value)
					End If

					'need to update download purposes with category from xml config
					If PurposeList.ContainsKey(value) AndAlso node.Attributes("category") IsNot Nothing Then
						Dim _category As String = node.Attributes("category").Value
						PurposeList(value) = New SmTextValueCatItem() With {.text = PurposeList(value).text, .value = PurposeList(value).value, .category = _category}
					End If

				Next
			End If

			


			Dim cardTypesNode As XmlNode = GetNode(CREDIT_CARD_TYPES())
			If cardTypesNode IsNot Nothing AndAlso cardTypesNode.HasChildNodes Then
				For Each node As XmlNode In cardTypesNode.ChildNodes
					If node.NodeType = XmlNodeType.Comment Then Continue For
					CardTypeList.Add(node.Attributes("value").InnerText)
				Next
			End If

			'Dim cardNamesNode As XmlNode = GetNode(CREDIT_CARD_NAMES())
			'If cardNamesNode IsNot Nothing AndAlso cardNamesNode.HasChildNodes Then
			'	For Each node As XmlNode In cardNamesNode.ChildNodes
			'		If node.NodeType = XmlNodeType.Comment Then Continue For
			'		CardNameList.Add(node.Attributes("credit_card_name").InnerText)
			'	Next
			'End If
		End If
	End Sub

End Class
