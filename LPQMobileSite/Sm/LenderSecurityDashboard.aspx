﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="LenderSecurityDashboard.aspx.vb" Inherits="Sm_LenderSecurityDashboard" MasterPageFile="MasterPage.master" %>
<%@ Import Namespace="Newtonsoft.Json" %>
<%@ Import Namespace="LPQMobile.Utils" %>

<asp:Content runat="server" ContentPlaceHolderID="plBody">
	<div>
		<h1 class="page-header">Lender Dashboard - <%=BackOfficeLender.LenderName %></h1>
		<p>Manage information for this lender.</p>
		<section id="dashboard">
			<ul class="nav nav-tabs" role="tablist">
				<%-- Synchronize permissions with LenderDashboard.aspx --%>
				<%If LenderAdminAllowed AndAlso SmUtil.CheckRoleAccess(HttpContext.Current, SmSettings.Role.SuperOperator, SmSettings.Role.LegacyOperator) Then%>
					<li role="presentation" ><a href="<%=BuildUrl("/sm/lenderdashboard.aspx")%>#info" aria-controls="info" role="tab">Profile</a></li>
				<%End If%>
				<%If LenderAdminAllowed AndAlso CanManageUsers Then%>
				    <li role="presentation"><a href="<%=BuildUrl("/sm/lenderdashboard.aspx")%>#users" aria-controls="users" role="tab">Users</a></li>
				<%End If%>
				<%--TODO: remove IsInEnvironment when ready to deploy--%>
				<%If LenderAdminAllowed AndAlso IsInEnvironment("DEV", "STAGE") Then%>
				<li role="presentation"><a href="<%=BuildUrl("/sm/lenderdashboard.aspx")%>#vendors" aria-controls="vendors" role="tab">Vendors</a></li>
				<%End If%>
				<li role="presentation" class="active"><a href="#" aria-controls="security" role="tab" data-toggle="tab">Security</a></li>
			</ul>
			<div class="tab-content">
				<div role="tabpanel" class="tab-pane active" id="security">
					<h2 class="section-title">Security</h2>
					<div class="bottom30">
						<div class="group-heading">COUNTRIES ALLOWED ACCESS</div>
						<p>Select countries allowed access to the Application Portal</p>
						<div class="checkbox checkbox-success">
							<input type="checkbox" value="" id="chkAllCountriesAllowedAccess"/>
							<label for="chkAllCountriesAllowedAccess">All countries</label>
						</div>
						<div class="scrollbar-outer country-access-lst" id="divCountryList">
							<div class="row no-margin">
								<%For Each item In Common.CountryList.Where(Function(p) p.Value.Status = 1)%>
								<div class="checkbox checkbox-success col-3" style="margin: 10px 0 0;">
									<input type="checkbox" value="<%=item.Value.Code2%>" id="chkCountryAllowAccess_<%=item.Key%>" <%=BindCountriesAllowed(item.Value.Code2)%>/>
									<label for="chkCountryAllowAccess_<%=item.Key%>"><%=item.Value.Name%></label>
								</div>
								<%Next%>
							</div>
						</div>
					</div>
					<br/>
					<br/>
					<div class="bottom30">
						<div class="group-heading">EXCLUDED IP ADDRESSES</div>
						<p>Add any IP addresses excluded from accessing the Application Portal</p>
						<button class="btn btn-secondary" type="button" onclick="master.FACTORY.showAddEditExcludedIPPopup()"><i class="fa fa-plus" aria-hidden="true"></i>Add Excluded IP</button>
						<br/>
						<br/>
						<table id="tblExcludedIPs" class="table simple-table ips-table<%=IIf(ExcludedIPs IsNot Nothing AndAlso ExcludedIPs.Count > 0, "", " hidden")%>">
							<thead>
								<tr>
									<th>IPs</th>
									<th>Comments</th>
									<th></th>
									<th></th>
								</tr>
							</thead>
							<tbody>
								<%If ExcludedIPs IsNot Nothing AndAlso ExcludedIPs.Count > 0 Then
										  For Each item In ExcludedIPs%>
								<tr>
									<td><%=item.IP%></td>
									<td <%=IIf(String.IsNullOrWhiteSpace(item.Comments), "class=""no-comments""", "")%>><%=IIf(String.IsNullOrWhiteSpace(item.Comments), "No comments added", item.Comments)%></td>
									<td><a href="javascript:;" onclick="master.FACTORY.showAddEditExcludedIPPopup(this)">Edit IP</a></td>
									<td><a href="javascript:;" onclick="master.FACTORY.deleteIPItem(this)">Delete IP</a></td>
								</tr>
								<%Next
												 End If%>
							</tbody>
						</table>
						<a href="javascript:;" onclick="master.FACTORY.toggleIPAddressTable(this)" class="<%=IIf(ExcludedIPs IsNot Nothing AndAlso ExcludedIPs.Count > 5, "", "hidden")%>"></a>
						<br/>
						<br/>
						<div>
							<h3 class="property-title">Excluded IP Message</h3>
							<p>This message will display to an applicant when his/her IP address is excluded</p>
							<div class="html-editor" contenteditable="true" data-required="false"  id="txtExcludedIPMsg"><%=ExcludedIPMessage%></div>			
						</div>
					</div>
					<br/>
					<br/>
					<div class="bottom30">
						<div class="group-heading">APPLICATION SUBMISSION RATE LIMITER</div>
						<p>Use this feature to control the maximum number of applications that can be submitted within a 60-minute period from the same IP address. Set the number of applications allowed before a notification email is sent to the specified internal email addresses. Once the maximum number of applications are received, no applications will be accepted from that IP address until after the 60-minute period lapses.</p>
						<div class="row">
							<div class="col-lg-3 col-12">
								<div class="form-group">
									<label>Max Number of Applications Before Notification</label>
									<input type="text" class="form-control numeric-input" data-max="10000" value="<%=IIf(MaxNumberOfAppBeforeNotification < 0, "", MaxNumberOfAppBeforeNotification)%>" id="txtMaxAppBeforeNotification"/>
								</div>
							</div>
							<div class="col-lg-3 col-12">
								<div class="form-group">
									<label>Max Number of Applications Before Blocking</label>
									<input type="text" class="form-control numeric-input" data-max="10000" value="<%=IIf(MaxNumberOfAppBeforeBlocking < 0, "", MaxNumberOfAppBeforeBlocking)%>" id="txtMaxAppBeforeBlocking"/>
								</div>
							</div>
							<div class="col-lg-6 col-12">
								<div class="form-group">
									<label>Internal Email Address(es)</label>
									<input type="text" class="form-control" data-format="emails" value="<%=InternalEmailAddresses%>" id="txtInternalEmailAddresses" placeholder="Separated email addresses with a semicolon(;)"/>
								</div>
							</div>
						</div>
						<br/>
						<p>Add any IP addresses exempt from the submission rate limiter</p>
						<button class="btn btn-secondary" type="button" onclick="master.FACTORY.showAddEditSubmissionRateLimiterIPPopup()"><i class="fa fa-plus" aria-hidden="true"></i>Add IP Address</button>
						<br/>
						<br/>
						<table id="tblSubmissionRateLimiterIPs" class="table simple-table ips-table<%=IIf(SubmissionRateLimiterIPs IsNot Nothing AndAlso SubmissionRateLimiterIPs.Count > 0, "", " hidden")%>">
							<thead>
								<tr>
									<th>IPs</th>
									<th>Comments</th>
									<th></th>
									<th></th>
								</tr>
							</thead>
							<tbody>
								<%If SubmissionRateLimiterIPs IsNot Nothing AndAlso SubmissionRateLimiterIPs.Count > 0 Then
										For Each item In SubmissionRateLimiterIPs%>
								<tr>
									<td><%=item.IP%></td>
									<td <%=IIf(String.IsNullOrWhiteSpace(item.Comments), "class=""no-comments""", "")%>><%=IIf(String.IsNullOrWhiteSpace(item.Comments), "No comments added", item.Comments)%></td>
									<td><a href="javascript:;" onclick="master.FACTORY.showAddEditSubmissionRateLimiterIPPopup(this)">Edit IP</a></td>
									<td><a href="javascript:;" onclick="master.FACTORY.deleteIPItem(this)">Delete IP</a></td>
								</tr>
								<%Next
												 End If%>
							</tbody>
						</table>
						<a href="javascript:;" onclick="master.FACTORY.toggleIPAddressTable(this)" class="<%=IIf(SubmissionRateLimiterIPs IsNot Nothing AndAlso SubmissionRateLimiterIPs.Count > 5, "", "hidden")%>"></a>
					</div>
					<%-- AP-1935 - Temporarily remove this section so that clients do not see this. A future case will re-establish this section
						if any of the lender's portals have the InstaTouch flag enabled, indicating they've signed the contract.--%>
					<br/>
					<br/>
					<div class="bottom30 hidden">
						<div class="group-heading">EQUIFAX INSTATOUCH RESTRICTION</div>
						<p>Use this feature to control the maximum number of times InstaTouch can be accessed within a 60-minute period from the same IP address. Set the number of times allowed before a notification email is sent to the specified internal email addresses. Once the maximum number of times are reached, no applications will be accepted from that IP address until after the 60-minute period lapses.</p>
						<div class="row">
							<div class="col-lg-3 col-12">
								<div class="form-group">
									<label>Max Number of Times Before Notification</label>
									<input type="text" class="form-control numeric-input" data-max="10000" value="<%=IIf(InstaTouchMaxTimesBeforeNotification < 0, "", InstaTouchMaxTimesBeforeNotification)%>" id="txtInstaTouchMaxTimesBeforeNotification"/>
								</div>
							</div>
							<div class="col-lg-3 col-12">
								<div class="form-group">
									<label>Max Number of Times Before Blocking</label>
									<input type="text" class="form-control numeric-input" data-max="10000" value="<%=IIf(InstaTouchMaxTimeBeforeBlocking < 0, "", InstaTouchMaxTimeBeforeBlocking)%>" id="txtInstaTouchMaxTimesBeforeBlocking"/>
								</div>
							</div>
							<div class="col-lg-6 col-12">
								<div class="form-group">
									<label>Internal Email Address(es)</label>
									<input type="text" class="form-control" data-format="emails" value="<%=InstaTouchInternalEmailAddresses%>" id="txtInstaTouchInternalEmailAddresses" placeholder="Separated email addresses with a semicolon(;)"/>
								</div>
							</div>
						</div>
						<br/>
						<p>Add any IP addresses exempt from the restriction</p>
						<button class="btn btn-secondary" type="button" onclick="master.FACTORY.showAddEditInstaTouchRestrictionIPPopup()"><i class="fa fa-plus" aria-hidden="true"></i>Add IP Address</button>
						<br/>
						<br/>
						<table id="tblInstaTouchRestrictionIPs" class="table simple-table ips-table<%=IIf(InstaTouchRestrictionIPs IsNot Nothing AndAlso InstaTouchRestrictionIPs.Count > 0, "", " hidden")%>">
							<thead>
								<tr>
									<th>IPs</th>
									<th>Comments</th>
									<th></th>
									<th></th>
								</tr>
							</thead>
							<tbody>
								<%If InstaTouchRestrictionIPs IsNot Nothing AndAlso InstaTouchRestrictionIPs.Count > 0 Then
										For Each item In InstaTouchRestrictionIPs%>
								<tr>
									<td><%=item.IP%></td>
									<td <%=IIf(String.IsNullOrWhiteSpace(item.Comments), "class=""no-comments""", "")%>><%=IIf(String.IsNullOrWhiteSpace(item.Comments), "No comments added", item.Comments)%></td>
									<td><a href="javascript:;" onclick="master.FACTORY.showAddEditInstaTouchRestrictionIPPopup(this)">Edit IP</a></td>
									<td><a href="javascript:;" onclick="master.FACTORY.deleteIPItem(this)">Delete IP</a></td>
								</tr>
								<%Next
												 End If%>
							</tbody>
						</table>
						<a href="javascript:;" onclick="master.FACTORY.toggleIPAddressTable(this)" class="<%=IIf(InstaTouchRestrictionIPs IsNot Nothing AndAlso InstaTouchRestrictionIPs.Count > 5, "", "hidden")%>"></a>
					</div>
					<br/>
					<br/>
					<div class="text-center margin-top-bottom-20">
						<button class="btn btn-secondary" type="button" onclick="master.FACTORY.saveAllChanges()"><i class="fa fa-save" aria-hidden="true"></i>Save All Changes</button>
					</div>
				</div>
			</div>
		</section>
	</div>
</asp:Content>
<asp:Content runat="server" ContentPlaceHolderID="plScripts">
	<script type="text/javascript" src="js/ckeditor/ckeditor.js"></script>
	<script type="text/javascript">
		CKEDITOR.disableAutoInline = true;
		$(function () {
			$('.nav-tabs a').on('shown.bs.tab', function (e) {
				window.location.hash = e.target.hash;
				window.scrollTo(0, 0);
			});
			var url = document.location.toString();
			if (url.match('#') && $.trim(url.split('#')[1]) != "") {
				$('.nav-tabs a[href="#' + url.split('#')[1] + '"]').tab('show');
			} else {
				var tab = $("div.tab-content >.tab-pane:first-child").attr("id");
				$('.nav-tabs a[href="#' + tab + '"]').tab('show');
			}
			master.FACTORY.init();
		});
		(function (master, $, undefined) {
			var currentRevisions = <%=JsonConvert.SerializeObject(RevisionList)%>;
			master.FACTORY.init = function () {
				$(".country-access-lst.scrollbar-outer").scrollbar();
				$("#chkAllCountriesAllowedAccess").on("change", function () {
					var $self = $(this);
					if ($self.is(":checked")) {
						$("input:checkbox", "#divCountryList").prop("checked", true);
					} else {
						$("input:checkbox", "#divCountryList").prop("checked", false);
					}
					master.FACTORY.documentChanged();
				});
				$("input:checkbox", "#divCountryList").on("change", function () {
					var $self = $(this);
					if (!$self.is(":checked")) {
						$("#chkAllCountriesAllowedAccess").prop("checked", false);
					} else {
						if ($("input:checkbox", "#divCountryList").length == $("input:checkbox:checked", "#divCountryList").length) {
							$("#chkAllCountriesAllowedAccess").prop("checked", true);
						}
					}
					master.FACTORY.documentChanged();
				});
				$("#chkAllCountriesAllowedAccess").prop("checked", $("input:checkbox:not(:checked)", "#divCountryList").length == 0);
				$(".html-editor").ckEditor({
					onContentChanged: function (editor) { master.FACTORY.documentChanged(editor); },
					onCommandClicked: function (editor, commandBtn) { master.FACTORY.documentChanged(editor); },
					placeholder: "Type here..."
				});
				$("#txtMaxAppBeforeNotification, #txtMaxAppBeforeBlocking, #txtInternalEmailAddresses, #txtInstaTouchMaxTimesBeforeNotification, #txtInstaTouchMaxTimesBeforeBlocking, #txtInstaTouchInternalEmailAddresses, #tblExcludedIPs, #tblSubmissionRateLimiterIPs, #tblInstaTouchRestrictionIPs").on("change", function () {
					master.FACTORY.documentChanged();
				});
				$(".numeric-input").on("keypress", function (e) {
					if (isNaN(e.key) || e.key == " ") return false;
				}).on("keydown", function (e) {
					var keyCode = e.which || e.keyCode;
					var $self = $(this);
					var v = parseInt($self.val());
					if (isNaN(v)) v = 0;
					if (keyCode == 38) {
						$self.val(v + 1);
						master.FACTORY.documentChanged();
					} else if (keyCode == 40) {
						if (v > 0) {
							$self.val(v - 1);
						}
						master.FACTORY.documentChanged();
					}
				});
				registerDataValidator("#security", "ValidateData");
			};
			master.FACTORY.toggleIPAddressTable = function (srcEle) {
				$(srcEle).prev("table").toggleClass("show-all");
			}
			master.FACTORY.deleteIPItem = function (srcEle) {
				var $srcEle = $(srcEle);
				var rowCount = $srcEle.closest("tbody").find("tr").length;
				if (rowCount == 1) {
					$srcEle.closest("table").addClass("hidden");
				} else if (rowCount <= 6) {
					$srcEle.closest("table").next("a").addClass("hidden");
				}
				$srcEle.closest("tr").remove();
				master.FACTORY.documentChanged();
			}

			master.FACTORY.showAddEditExcludedIPPopup = function (srcEle) {
				var title = (typeof srcEle != "undefined" ? "Edit" : "Add") + " Excluded IP Address";
				var description = "Use a wildcard(*) if you would like to exclude a range of IP addresses(e.g. 128.201.200.* will exclude all IP addresses in the 128.201.200 subnet access)";
				showEditIPPopup(srcEle, title, description, function (data) {
					var $table = $("#tblExcludedIPs");
					$table.removeClass("hidden");
					var html = "";
					var comments = $.trim(data.Comments);
					html += '<tr>';
					html += '<td>' + _COMMON.HtmlEncode(data.IP) + '</td>';
					html += '<td ' + (comments.length == 0 ? 'class="no-comments"' : '') + '>' + (comments.length == 0 ? 'No comments added' : _COMMON.HtmlEncode(comments)) + '</td>';
					html += '<td><a href="javascript:;" onclick="master.FACTORY.showAddEditExcludedIPPopup(this)">Edit IP</a></td>';
					html += '<td><a href="javascript:;" onclick="master.FACTORY.deleteIPItem(this)">Delete IP</a></td>';
					html += '</tr>';
					$("tbody", $table).append(html);
					if ($("tbody>tr", $table).length > 5) {
						$table.next("a").removeClass("hidden");
					}
					master.FACTORY.documentChanged();
				});
			}

			master.FACTORY.showAddEditSubmissionRateLimiterIPPopup = function (srcEle) {
				var title = (typeof srcEle != "undefined" ? "Edit" : "Add") + " Excempt IP Address";
				var description = "Use a wildcard(*) if you would like to exempt a range of IP addresses(e.g. 128.201.200.* will exclude all IP addresses in the 128.201.200 subnet access)";
				showEditIPPopup(srcEle, title, description, function (data) {
					var $table = $("#tblSubmissionRateLimiterIPs");
					$table.removeClass("hidden");
					var html = "";
					var comments = $.trim(data.Comments);
					html += '<tr>';
					html += '<td>' + _COMMON.HtmlEncode(data.IP) + '</td>';
					html += '<td ' + (comments.length == 0 ? 'class="no-comments"' : '') + '>' + (comments.length == 0 ? 'No comments added' : _COMMON.HtmlEncode(comments)) + '</td>';
					html += '<td><a href="javascript:;" onclick="master.FACTORY.showAddEditSubmissionRateLimiterIPPopup(this)">Edit IP</a></td>';
					html += '<td><a href="javascript:;" onclick="master.FACTORY.deleteIPItem(this)">Delete IP</a></td>';
					html += '</tr>';
					$("tbody", $table).append(html);
					if ($("tbody>tr", $table).length > 5) {
						$table.next("a").removeClass("hidden");
					}
					master.FACTORY.documentChanged();
				});
			}
			master.FACTORY.showAddEditInstaTouchRestrictionIPPopup = function (srcEle) {
				var title = (typeof srcEle != "undefined" ? "Edit" : "Add") + " Excempt IP Address";
				var description = "Use a wildcard(*) if you would like to exempt a range of IP addresses(e.g. 128.201.200.* will exclude all IP addresses in the 128.201.200 subnet access)";
				showEditIPPopup(srcEle, title, description, function (data) {
					var $table = $("#tblInstaTouchRestrictionIPs");
					$table.removeClass("hidden");
					var html = "";
					var comments = $.trim(data.Comments);
					html += '<tr>';
					html += '<td>' + _COMMON.HtmlEncode(data.IP) + '</td>';
					html += '<td ' + (comments.length == 0 ? 'class="no-comments"' : '') + '>' + (comments.length == 0 ? 'No comments added' : _COMMON.HtmlEncode(comments)) + '</td>';
					html += '<td><a href="javascript:;" onclick="master.FACTORY.showAddEditInstaTouchRestrictionIPPopup(this)">Edit IP</a></td>';
					html += '<td><a href="javascript:;" onclick="master.FACTORY.deleteIPItem(this)">Delete IP</a></td>';
					html += '</tr>';
					$("tbody", $table).append(html);
					if ($("tbody>tr", $table).length > 5) {
						$table.next("a").removeClass("hidden");
					}
					master.FACTORY.documentChanged();
				});
			}
			function showEditIPPopup(srcEle, title, description, addHandler) {
				var ipText = "...";
				var comments = "";
				if (typeof srcEle != "undefined") {
					var $srcEle = $(srcEle);
					ipText = $srcEle.closest("tr").find("td:first-child").text();
					var $comments = $srcEle.closest("tr").find("td:nth-child(2)");
					if (!$comments.hasClass("no-comments")) {
						comments = $comments.text();
					}
				}
				var ipArr = ipText.split(".");
				var htmlContent = "";
				htmlContent += '<div class="row">';
				htmlContent += '	<div class="col-12">';
				htmlContent += '		<p>' + description + '</p>';
				htmlContent += '	</div>';
				htmlContent += '</div>';
				htmlContent += '<div class="row">';
				htmlContent += '	<div class="col-12">';
				htmlContent += '		<div class="form-group ip-address" id="divIPAddress">';
				htmlContent += '			<label class="required-field">IP Address</label>';
				htmlContent += '			<input type="text" class="form-control" maxlength="3" value="' + ipArr[0] + '" data-field="txtIP1" id="txtIP1"/>';
				htmlContent += '			<span>.</span>';
				htmlContent += '			<input type="text" class="form-control" maxlength="3" value="' + ipArr[1] + '" data-field="txtIP2" id="txtIP2"/>';
				htmlContent += '			<span>.</span>';
				htmlContent += '			<input type="text" class="form-control" maxlength="3" value="' + ipArr[2] + '" data-field="txtIP3" id="txtIP3"/>';
				htmlContent += '			<span>.</span>';
				htmlContent += '			<input type="text" class="form-control" maxlength="3" value="' + ipArr[3] + '" data-field="txtIP4" id="txtIP4"/>';
				htmlContent += '		</div>';
				htmlContent += '	</div>';
				htmlContent += '</div>';
				htmlContent += '<div class="row">';
				htmlContent += '	<div class="col-12">';
				htmlContent += '		<div class="form-group">';
				htmlContent += '			<label>Comments</label>';
				htmlContent += '			<textarea class="form-control" rows="3" data-field="txtIPComments" id="txtIPComments">' + comments + '</textarea>';
				htmlContent += '		</div>';
				htmlContent += '	</div>';
				htmlContent += '</div>';
				htmlContent = '<div style="padding: 0 20px 20px; overflow: auto; max-height: 500px;">' + htmlContent + '</div>';
				_COMMON.showDialogSaveClose(title, 0, "edit_ip_address_dialog_0", htmlContent, "", null, function (dlg) {
					//on save
					if (!$.smValidate("edit_ip_address_dialog_0")) return;
					var data = {};
					var ip1 = $("#txtIP1", "#edit_ip_address_dialog_0").val();
					var ip2 = $("#txtIP2", "#edit_ip_address_dialog_0").val();
					var ip3 = $("#txtIP3", "#edit_ip_address_dialog_0").val();
					var ip4 = $("#txtIP4", "#edit_ip_address_dialog_0").val();
					data.IP = ip1 + "." + ip2 + "." + ip3 + "." + ip4;
					data.Comments = $("#txtIPComments", "#edit_ip_address_dialog_0").val();
					if (typeof srcEle != "undefined") {
						var $srcEle = $(srcEle);
						$srcEle.closest("tr").find("td:first-child").text(data.IP);
						var $comments = $srcEle.closest("tr").find("td:nth-child(2)");
						if (data.Comments == "") {
							$comments.addClass("no-comments");
							$comments.text("No comments added");
						} else {
							$comments.removeClass("no-comments");
							$comments.text(data.Comments);
						}
						master.FACTORY.documentChanged();
					} else {
						addHandler(data);
					}
					var $container = dlg.container;
					dlg.dialog.close();
				}, function (container) {
					$.smValidate.removeValidationGroup("edit_ip_address_dialog_0");
					$("#txtIP1", "#edit_ip_address_dialog_0").on("keypress", function (e) {
						if (e.key == ".") {
							$("#txtIP2", "#edit_ip_address_dialog_0").focus();
						}
						if (isNaN(e.key)) return false;
						/*if ($(this).val().length == 2) {
							$("#txtIP2", "#edit_ip_address_dialog_0").focus();
						}*/
					});
					$("#txtIP2", "#edit_ip_address_dialog_0").on("keypress", function (e) {
						if (e.key == ".") {
							$("#txtIP3", "#edit_ip_address_dialog_0").focus();
						}
						if (isNaN(e.key)) return false;
						/*if ($(this).val().length == 2) {
							$("#txtIP3", "#edit_ip_address_dialog_0").focus();
						}*/
					});
					$("#txtIP3", "#edit_ip_address_dialog_0").on("keypress", function (e) {
						if (e.key == ".") {
							$("#txtIP4", "#edit_ip_address_dialog_0").focus();
						}
						if (isNaN(e.key)) return false;
						/*if ($(this).val().length == 2) {
							$("#txtIP4", "#edit_ip_address_dialog_0").focus();
						}*/
					});
					$("#txtIP4", "#edit_ip_address_dialog_0").on("keypress", function (e) {
						//var keyCode = e.which || e.keyCode;
						if ($(this).val() == "*" || (e.key != "*" && isNaN(e.key))) return false;
					});
					$("#divIPAddress", "#edit_ip_address_dialog_0").observer({
						validators: [
							function (partial, evt) {
								var ip1 = $("#txtIP1", "#edit_ip_address_dialog_0").val();
								var ip2 = $("#txtIP2", "#edit_ip_address_dialog_0").val();
								var ip3 = $("#txtIP3", "#edit_ip_address_dialog_0").val();
								var ip4 = $("#txtIP4", "#edit_ip_address_dialog_0").val();
								var ip = ip1 + "." + ip2 + "." + ip3 + "." + ip4;
								if (ip == "...") {
									return "Please add an IP address";
								} else if (!/^([1-9][0-9]{0,2}.){3}([1-9][0-9]{0,2}|\*)$/.test(ip) || parseInt(ip1) > 255 || parseInt(ip2) > 255 || parseInt(ip3) > 255 || (ip4 != "*" && parseInt(ip4) > 255)) {
									return "Invalid IP address";
								}
								return "";
							}
						],
						validateOnBlur: false,
						group: "edit_ip_address_dialog_0"
					});
				}, function () {
					//onclose
					$.smValidate.cleanGroup("edit_ip_address_dialog_0");
				}, BootstrapDialog.SIZE_NORMAL, false);
			};
			function collectSubmitData() {
				var result = {};

				const excludedIPs = collectIPAddresses("#tblExcludedIPs");
				if (ipAddressesContainsDuplicates(excludedIPs)) {
					_COMMON.showAlertDialog("There are duplicate IP addresses in the Excluded IP Addresses exemptions table.", "Validation Error");
					return null;
				}
				const submissionRateLimiterExemptIPs = collectIPAddresses("#tblSubmissionRateLimiterIPs");
				if (ipAddressesContainsDuplicates(submissionRateLimiterExemptIPs)) {
					_COMMON.showAlertDialog("There are duplicate IP addresses in the Submission Rate Limiter exemptions table.", "Validation Error");
					return null;
				}
				const instaTouchExemptIPs = collectIPAddresses("#tblInstaTouchRestrictionIPs");
				if (ipAddressesContainsDuplicates(instaTouchExemptIPs)) {
					_COMMON.showAlertDialog("There are duplicate IP addresses in the InstaTouch exemptions table.", "Validation Error");
					return null;
				}

				result.CountriesAllowedAccess = JSON.stringify(_.map($("input:checked:checked", "#divCountryList"), function (r) { return $(r).val(); }));
				result.ExcludedIP = JSON.stringify({ ExcludedIPMessage: _COMMON.getEditorValue("txtExcludedIPMsg"), ExcludedIPs: excludedIPs });
				result.Submission = JSON.stringify({
					MaxAppBeforeNotification: ($("#txtMaxAppBeforeNotification").val() == "" ? -1 : $("#txtMaxAppBeforeNotification").val()), 
					MaxAppBeforeBlocking: ($("#txtMaxAppBeforeBlocking").val() == "" ? -1 : $("#txtMaxAppBeforeBlocking").val()), 
					InternalEmailAddresses: $("#txtInternalEmailAddresses").val(), 
					SubmissionRateLimiterIPs: submissionRateLimiterExemptIPs
				});
				result.InstaTouch = JSON.stringify({
					InstaTouchMaxTimesBeforeNotification: ($("#txtInstaTouchMaxTimesBeforeNotification").val() == "" ? -1 : $("#txtInstaTouchMaxTimesBeforeNotification").val()), 
					InstaTouchMaxTimesBeforeBlocking: ($("#txtInstaTouchMaxTimesBeforeBlocking").val() == "" ? -1 : $("#txtInstaTouchMaxTimesBeforeBlocking").val()), 
					InstaTouchInternalEmailAddresses: $("#txtInstaTouchInternalEmailAddresses").val(), 
					InstaTouchRestrictionIPs: instaTouchExemptIPs
				});
				result.Revisions = JSON.stringify(currentRevisions);
				return result;
			}
			function collectIPAddresses(container) {
				return _.map($("tbody>tr", $(container)), function (r) {
					var $row = $(r);
					var item = {};
					item.IP = $row.find("td:first-child").text();
					var $tdComments = $row.find("td:nth-child(2)");
					item.Comments = "";
					if (!$tdComments.hasClass("no-comments")) {
						item.Comments = $tdComments.text();
					}
					return item;
				});	
			}
			function ipAddressesContainsDuplicates(ips) {
				const dict = {};
				for (var i = 0; i < ips.length; i++) {
					const ip = ips[i];
					dict[ip.IP] = true;
				}
				return ips.length != Object.keys(dict).length;
			}
			master.FACTORY.saveAllChanges = function(force) {
				force = force || false;
				if ($.smValidate("ValidateData") == false) return;
				var dataObj = collectSubmitData();
				if (dataObj == null) return;
				dataObj.command = "saveLenderSecurityConfig";
				dataObj.lenderID = '<%=LenderID%>';
				dataObj.bid = '<%=BackOfficeLenderID%>';
				dataObj.forceSaving = force;
				$.ajax({
					url: "/sm/smhandler.aspx",
					async: true,
					cache: false,
					type: 'POST',
					dataType: 'json',
					data: dataObj,
					success: function (response) {
						if (response.IsSuccess) {
							//updateRevisionNumber(resoinse
							_COMMON.noty("success", "Save successfully.", 500);
							currentRevisions = response.Info;
							master.FACTORY.resetDocumentChanged();
						} else if (response.Info == "OUTOFDATE") {
							_COMMON.showConfirmDialog("Current configuration is out of date. Do you want to continue saving it?", function() {
								//yes
								master.FACTORY.saveAllChanges(true);
							}, function() {
								//no
								window.location = window.location.href;
							}, "Yes, do it", "No, reload it");
						} else {
							_COMMON.noty("error", "Error", 500);
						}
					}
				});
			}
		}(window.master = window.master || {}, jQuery));
		function registerDataValidator(container, groupName) {
			var $container = $(container);
			$.smValidate.removeValidationGroup(groupName);
			$("[data-required='true'],[data-format],[data-max]", $container).each(function (idx, ele) {
				$(ele).observer({
					validators: [
						function (partial) {
							var $self = $(this);
							if ($self.data("required") && $.trim($self.val()) === "") {
								if ($self.data("message-require")) {
									return $self.data("message-require");
								} else {
									return "This field is required";
								}
							}
							var format = $self.data("format");
							if (format) {
								var msg = "Invalid format";
								if ($self.data("message-invalid-data")) {
									msg = $self.data("message-invalid-data");
								}
								if (format === "emails") {
									if ($.trim($self.val()) != "") {
										var emails = $self.val().split(";");
										var ret = true;
										_.forEach(emails, function(item) {
											if (!_COMMON.ValidateEmail(item)) ret = false;
										});
										if (!ret) {
											return "Invalid email addresses";
										}
									}
								}
								if (format === "email") {
									if (_COMMON.ValidateEmail($self.val()) === false) {
										return msg;
									}
								} else if (format === "phone") {
									if (_COMMON.ValidatePhone($self.val()) === false) {
										return msg;
									}
								} else if ($self.val() != "" && format === "positive-number") {
									if ((parseInt($self.val()) > 0) == false) {
										return msg;
									}
								}
							}
							var max = $self.data("max");
							if (!isNaN(max)) {
								max = parseInt(max);
								if (max <= parseInt($self.val())) {
									return "Maximum value allowed is " + ($self.data("max") + "").replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,");
								}
							}
							return "";
						}
					],
					validateOnBlur: true,
					group: groupName
				});
			});
		}
	</script>
</asp:Content>
<asp:Content runat="server" ContentPlaceHolderID="plActionButtons">
</asp:Content>