﻿Imports System.Xml
Imports DownloadedSettings
Imports Sm
Partial Class Sm_ManageVL
	Inherits SmApmBasePage

	Const LOANTYPE As String = "VEHICLE_LOAN"
	Protected Property ApprovalMessage As String
	Protected Property ReferredMessage As String
    Protected Property DeclinedMessage As String
    Protected Property PreQualifiedMessage As String
    Protected Property DisclosureList As New List(Of String)

	Protected Property EnabledPurposeList As New List(Of String)(System.StringComparison.OrdinalIgnoreCase)
	Protected Property PurposeList As New Dictionary(Of String, SmTextValueCatItem)(System.StringComparison.OrdinalIgnoreCase)
	Protected Property EnabledVehicleTypeList As New List(Of String)(System.StringComparison.OrdinalIgnoreCase)
	Protected Property VehicleTypeList As New Dictionary(Of String, SmTextValuePair)(System.StringComparison.OrdinalIgnoreCase)

	Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
		If Not Page.IsPostBack Then
			Dim smMasterPage = CType(Me.Master, Sm_SiteManager)
			smMasterPage.LenderConfigXml = LenderConfigXml
			smMasterPage.RevisionID = LenderConfig.RevisionID
			smMasterPage.LastModifiedBy = LenderConfig.LastModifiedByUserFullName
			smMasterPage.LastModifiedByUserID = LenderConfig.LastModifiedByUserID
			smMasterPage.SelectedMainMenuItem = "manage"
			smMasterPage.SelectedSubMenuItem = "vl"
			smMasterPage.CurrentNodeGroup = SmSettings.NodeGroup.ManageVL
			smMasterPage.BreadCrumb = "Manage / Vehicle Loan"

			Dim preapprovedMessageNode As XmlNode = GetNode(PREAPPROVED_MESSAGE(LOANTYPE))
			If preapprovedMessageNode IsNot Nothing AndAlso preapprovedMessageNode.NodeType <> XmlNodeType.Comment Then
				ApprovalMessage = SmUtil.MergeMessageKey(preapprovedMessageNode.Attributes("message").InnerText, preapprovedMessageNode)
			End If

			Dim submittedMessageNode As XmlNode = GetNode(SUBMITTED_MESSAGE(LOANTYPE))
			If submittedMessageNode IsNot Nothing AndAlso submittedMessageNode.NodeType <> XmlNodeType.Comment Then
				ReferredMessage = SmUtil.MergeMessageKey(submittedMessageNode.Attributes("message").InnerText, submittedMessageNode)
			End If

			Dim declinedMessageNode As XmlNode = GetNode(DECLINED_MESSAGE(LOANTYPE))
			If declinedMessageNode IsNot Nothing AndAlso declinedMessageNode.NodeType <> XmlNodeType.Comment Then
				DeclinedMessage = SmUtil.MergeMessageKey(declinedMessageNode.Attributes("message").InnerText, declinedMessageNode)
			End If

            Dim preQualifiedMessageNode As XmlNode = GetNode(PREQUALIFIED_MESSAGE(LOANTYPE))
            If preQualifiedMessageNode IsNot Nothing AndAlso preQualifiedMessageNode.NodeType <> XmlNodeType.Comment Then
                PreQualifiedMessage = SmUtil.MergeMessageKey(preQualifiedMessageNode.Attributes("message").InnerText, preQualifiedMessageNode)
            End If

            Dim disclosuresNode As XmlNode = GetNode(DISCLOSURES(LOANTYPE))
			If disclosuresNode IsNot Nothing AndAlso disclosuresNode.HasChildNodes Then
				For Each node As XmlNode In disclosuresNode.ChildNodes
					If node.NodeType = XmlNodeType.Comment Then Continue For
					DisclosureList.Add(node.InnerText.Trim())
				Next
			End If

			'***********
			'Purpose
			Dim downloadVehicleLoanPurposes As List(Of DownloadedSettings.CListItem)
			downloadVehicleLoanPurposes = CDownloadedSettings.VehicleLoanPurposes(WebsiteConfig)
			'<OUTPUT><RESPONSE><FIELD_LIST name="Loan Purposes"><LIST_ITEM text="AUTO ADVANTAGE REFINANCE" value="AUTO ADVANTAGE REFINANCE" /><LIST_ITEM text="BALLOON" value="BALLOON" /><LIST_ITEM text="AUTO ADVANTAGE PURCHASE" value="AUTO ADVANTAGE PURCHASE" /><LIST_ITEM text="CONSOLIDATION" value="CONSOLIDATION" /><LIST_ITEM text="AUTO PURCHASE" value="AUTO PURCHASE" /><LIST_ITEM text="AUTO REFINANCE" value="AUTO REFINANCE" /><LIST_ITEM text="PURCHASE" value="PURCHASE" /><LIST_ITEM text="LEASE BUYOUT" value="LEASE BUYOUT" /><LIST_ITEM text="REFINANCE" value="REFINANCE" /><LIST_ITEM text="Loanstar" value="Loanstar" /></FIELD_LIST></RESPONSE></OUTPUT>
			If downloadVehicleLoanPurposes IsNot Nothing AndAlso downloadVehicleLoanPurposes.Any() Then
				For Each item As CListItem In downloadVehicleLoanPurposes
                    If item.Value Is Nothing OrElse item.Value = "" Then Continue For
                    If PurposeList.ContainsKey(item.Value) Then Continue For
                    PurposeList.Add(item.Value, New SmTextValueCatItem() With {.text = item.Text, .value = item.Value, .category = ""})
				Next
			End If

			Dim enabledPurposesNode As XmlNode = GetNode(VEHICLE_LOAN_PURPOSES())
			If enabledPurposesNode IsNot Nothing AndAlso enabledPurposesNode.HasChildNodes Then
				For Each node As XmlNode In enabledPurposesNode.ChildNodes
					If node.NodeType = XmlNodeType.Comment Then Continue For
                    Dim value As String = node.Attributes("value").Value
					If Not EnabledPurposeList.Contains(value) Then
						EnabledPurposeList.Add(value)
					End If

					'need to update download purposes with category from xml config
					If PurposeList.ContainsKey(value) AndAlso node.Attributes("category") IsNot Nothing Then
						Dim _category As String = node.Attributes("category").Value
						PurposeList(value) = New SmTextValueCatItem() With {.text = PurposeList(value).text, .value = PurposeList(value).value, .category = _category}
					End If

				Next
			End If

			

			Dim downloadVehicleTypes As List(Of DownloadedSettings.CListItem)
			downloadVehicleTypes = CDownloadedSettings.VehicleTypes(WebsiteConfig)
			'<OUTPUT><RESPONSE><FIELD_LIST name="Vehicle Types"><LIST_ITEM text="AUTO/PICKUP-TRUCK" value="CAR" /></FIELD_LIST></RESPONSE></OUTPUT>

			If downloadVehicleTypes IsNot Nothing AndAlso downloadVehicleTypes.Any() Then
				For Each item As CListItem In downloadVehicleTypes
					If VehicleTypeList.ContainsKey(item.Value.ToUpper()) Then Continue For
					VehicleTypeList.Add(item.Value.ToUpper(), New SmTextValuePair() With {.Text = item.Text, .Value = item.Value.ToUpper()})
				Next
			End If

			Dim enabledTypesNode As XmlNode = GetNode(VEHICLE_TYPES())
			If enabledTypesNode IsNot Nothing AndAlso enabledTypesNode.HasChildNodes Then
				For Each node As XmlNode In enabledTypesNode.ChildNodes
					If node.NodeType = XmlNodeType.Comment Then Continue For
					Dim value As String = node.Attributes("value").Value.ToUpper()
					If Not EnabledVehicleTypeList.Contains(value) Then
						EnabledVehicleTypeList.Add(value)
					End If
					If VehicleTypeList.ContainsKey(value) Then Continue For
					VehicleTypeList.Add(value, New SmTextValuePair() With {.Text = node.Attributes("text").Value, .Value = value})
				Next
			End If
		End If
	End Sub

End Class
