﻿Imports Sm
Imports System.Xml

Partial Class Sm_GeneralSettings
	Inherits SmApmBasePage

	Protected Property LicenseScan As Boolean = False
	Protected Property DLBarcodeScan As Boolean = False
	Protected Property ForeignAddress As Boolean = False
	Protected Property ForeignPhone As Boolean = False
	Protected Property MfaEmailOnly As Boolean = False
	Protected Property LinkedInEnable As Boolean = False
	Protected Property InstaTouchEnable As Boolean = False
	Protected Property AddressKey As Boolean = False
	Protected Property HomepageEditor As Boolean = False
	Protected Property GoogleTagManagerId As String = ""
	Protected Property PiwikSiteId As String = ""
	Protected Property ApmShowInstaTouchSwitchButton As Boolean = False

	Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
		If Not Page.IsPostBack Then
			Dim smMasterPage = CType(Me.Master, Sm_SiteManager)
			smMasterPage.LenderConfigXml = LenderConfigXml
			smMasterPage.RevisionID = LenderConfig.RevisionID
			smMasterPage.LastModifiedBy = LenderConfig.LastModifiedByUserFullName
			smMasterPage.LastModifiedByUserID = LenderConfig.LastModifiedByUserID
			smMasterPage.SelectedMainMenuItem = "config"
			smMasterPage.SelectedSubMenuItem = "settings"
			smMasterPage.CurrentNodeGroup = SmSettings.NodeGroup.GeneralSettings
			smMasterPage.BreadCrumb = "Configure / Settings / General"

			If Not (EnableCC Or EnableHE Or EnablePL Or EnableVL Or EnableBL Or EnableXA) Then
				Response.Redirect("/sm/MLSettings.aspx?lenderconfigid=" & LenderConfigID.ToString())
				Return
			End If

			If ApmDLScanEnable Then
				Dim licenseNode As XmlNode = GetNode(LICENSE_SCAN)
				If licenseNode IsNot Nothing Then
					LicenseScan = Not String.IsNullOrEmpty(licenseNode.Value)
				End If
			End If

			If SmUtil.CheckRoleAccess(Context, SmSettings.Role.LegacyOperator, SmSettings.Role.SuperOperator) Then
				Dim dlBarCodeScanNode As XmlNode = GetNode(DL_BARCODE_SCAN())
				If dlBarCodeScanNode IsNot Nothing Then
					DLBarcodeScan = dlBarCodeScanNode.Value.ToUpper().Equals("Y")
				End If
			End If

			If ApmSiteAnalyticsEnable Then
				Dim gtmNode As XmlNode = GetNode(GOOGLE_TAG_MANAGER_ID)
				If gtmNode IsNot Nothing Then
					GoogleTagManagerId = gtmNode.Value
				End If

				Dim piwikSiteIdNode As XmlNode = GetNode(PIWIK_SITE_ID())
				If piwikSiteIdNode IsNot Nothing Then
					PiwikSiteId = piwikSiteIdNode.Value
				End If
			End If

			Dim foreignAddressNode As XmlNode = GetNode(FOREIGN_ADDRESS())
			If foreignAddressNode IsNot Nothing Then
				ForeignAddress = foreignAddressNode.Value.ToUpper().Equals("Y")
			End If

			Dim foreignPhoneNode As XmlNode = GetNode(FOREIGN_PHONE())
			If foreignPhoneNode IsNot Nothing Then
				ForeignPhone = foreignPhoneNode.Value.ToUpper().Equals("Y")
			End If

			Dim mfaEmailOnlyNode As XmlNode = GetNode(MFA_EMAIL_ONLY())
			If mfaEmailOnlyNode IsNot Nothing Then
				MfaEmailOnly = mfaEmailOnlyNode.Value.ToUpper().Equals("Y")
			End If
			If ApmLinkedInEnable Then
				Dim linkedInEnableNode As XmlNode = GetNode(LINKEDIN_ENABLE())
				If linkedInEnableNode IsNot Nothing Then
					LinkedInEnable = linkedInEnableNode.Value.ToUpper().Equals("Y")
				End If
			End If

			Dim instaTouchEnableNode As XmlNode = GetNode(INSTATOUCH_ENABLED())
			If instaTouchEnableNode IsNot Nothing Then
				InstaTouchEnable = instaTouchEnableNode.Value.ToUpper().Equals("Y")
			End If

			Dim addressKeyNode As XmlNode = GetNode(ADDRESS_KEY())
			If addressKeyNode IsNot Nothing Then
				AddressKey = Not String.IsNullOrEmpty(addressKeyNode.Value)
			End If
			If SmUtil.CheckRoleAccess(Context, SmSettings.Role.LegacyOperator, SmSettings.Role.SuperOperator) Then
				Dim homepageEditorNode As XmlNode = GetNode(HOMEPAGE_EDITOR())
				If homepageEditorNode IsNot Nothing Then
					HomepageEditor = homepageEditorNode.Value.ToUpper().Equals("Y")
				End If
			End If
			ApmShowInstaTouchSwitchButton = SmUtil.GetApmShowInstaTouchSwitchButton(LenderConfigXml)
		End If
	End Sub

End Class
