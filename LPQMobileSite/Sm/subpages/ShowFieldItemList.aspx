﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="ShowFieldItemList.aspx.vb" Inherits="Sm_subpages_ShowFieldItemList" %>
<%@ Import Namespace="Newtonsoft.Json" %>

<% For Each item As KeyValuePair(Of String, SmShowFieldItemModel) In ShowFieldList%>
<div class="checkbox-btn-wrapper <%=IIf(item.Value.IsVisible = "D", " hidden", "")%>" data-controller-id="<%=item.Value.ControllerId%>" data-controller-name="<%=item.Value.ControllerName%>">
	<% If VisibilityWarnings.ContainsKey(item.Value.ControllerId) Then %>
	<a href="#" class="visibility-warning" onclick="showVisibilityWarning(this)" style="display: none;">
		<i class="fa fa-warning" title="<%=HttpUtility.HtmlAttributeEncode(VisibilityWarnings(item.Value.ControllerId).WarningText)%>"></i>
	</a>
	<% End If %>
	<span><%=item.Value.ControllerName%></span>
	<label class="toggle-btn pull-right" data-on="YES" data-off="NO">
        <input type="checkbox" data-text="<%=item.Value.ControllerName%>" value="<%=item.Value.ControllerId%>" <%=IIf(item.Value.IsVisible = "Y", "checked", "")%>/>
        <span class="button-checkbox"></span>
    </label>
</div>
<%Next%>