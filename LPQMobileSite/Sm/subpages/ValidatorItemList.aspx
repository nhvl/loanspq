﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="ValidatorItemList.aspx.vb" Inherits="Sm_subpages_ValidatorItemList" %>
<% For Each item As KeyValuePair(Of String, SmValidationItemModel) In ValidationList%>
<div class="checkbox-btn-wrapper <%=IIf(item.Value.IsRequire = "D", " hidden", "")%>" data-controller-id="<%=item.Value.ControllerId%>" data-controller-name="<%=item.Value.ControllerName%>">
	<span><%=item.Value.ControllerName%></span>
	<label class="toggle-btn pull-right" data-on="YES" data-off="NO">
        <input type="checkbox" data-text="<%=item.Value.ControllerName%>" value="<%=item.Value.ControllerId%>" <%=IIf(item.Value.IsRequire = "Y", "checked", "")%>/>
        <span class="button-checkbox"></span>
    </label>
</div>
<%Next%>