﻿
Imports System.Xml
Imports Newtonsoft.Json
Imports iTextSharp.text.pdf.qrcode
Imports Sm
Imports LPQMobile.Utils

Partial Class Sm_subpages_RenameItemList
	Inherits SmApmBasePage

	Protected RenameItemList As New Dictionary(Of String, SmRenameableItemModel)
	Protected HasDraft As Boolean = True
	Protected AbortItemList As New List(Of String)

	Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
		If Not IsPostBack Then
			Try
				Dim allItem As New Dictionary(Of String, SmRenameableItemModel)
				Dim type As String = Common.SafeString(Request.Params("type"))

				'For backward compatibility and avoid the need for migration script, use values “xa_special” and “sa_special” for loan_type attribute for both XA Special and XA Minor - don't use values “xa_minor” or “sa_minor”
				'This may have the side effect that a change in XA Special may leak to XA Minor. We would have to ask client to use as-is.
				Dim convertedType As String = type
				If type.ToUpper() = "XA_MINOR" Then
					convertedType = "XA_SPECIAL"
				ElseIf type.ToUpper() = "SA_MINOR" Then
					convertedType = "SA_SPECIAL"
				End If

				Dim draftNode As XmlNode = smBL.GetNodeFromDraft(RENAME_BUTTON_LABEL_ITEMS(convertedType), UserInfo.UserID, LenderConfigID)
				If draftNode IsNot Nothing AndAlso draftNode.NodeType <> XmlNodeType.Comment AndAlso draftNode.HasChildNodes Then
					For Each item As XmlNode In draftNode.SelectNodes("ITEM")
						Dim originalText As String = item.Attributes("original_text").Value
						If Not allItem.ContainsKey(originalText) Then
							allItem.Add(originalText, New SmRenameableItemModel() With {.LoanType = type, .OriginalText = originalText, .NewText = item.Attributes("new_text").Value, .Mode = "draft"})
						End If
					Next
				End If
				Dim liveNodeList As XmlNodeList = LenderConfigXml.SelectNodes(RENAME_BUTTON_LABEL_ITEMS(convertedType))
				If liveNodeList IsNot Nothing AndAlso liveNodeList.Count > 0 Then
					For Each item As XmlNode In liveNodeList
						Dim originalText As String = item.Attributes("original_text").Value
						If Not allItem.ContainsKey(originalText) Then
							allItem.Add(originalText, New SmRenameableItemModel() With {.LoanType = type, .OriginalText = originalText, .NewText = item.Attributes("new_text").Value, .Mode = "live"})
						End If
					Next
				End If

				''support legacy where loan_type is concatenated
				'Dim liveNodeLegacyList As XmlNodeList = LenderConfigXml.SelectNodes(RENAME_BUTTON_LEGACY_LABEL)
				'If liveNodeLegacyList IsNot Nothing AndAlso liveNodeLegacyList.Count > 0 Then
				'	For Each item As XmlNode In liveNodeLegacyList
				'		If item.Attributes("loan_type") IsNot Nothing AndAlso Not item.Attributes("loan_type").Value.NullSafeToUpper_.Contains(type.NullSafeToUpper_) Then
				'			Continue For
				'		End If
				'		Dim originalText As String = item.Attributes("original_text").Value
				'		If Not allItem.ContainsKey(originalText) Then
				'			allItem.Add(originalText, New SmRenameableItemModel() With {.LoanType = type, .OriginalText = originalText, .NewText = item.Attributes("new_text").Value, .Mode = "live"})
				'		End If
				'	Next
				'End If

				If draftNode Is Nothing Then
					HasDraft = False
					RenameItemList = allItem
				Else
					RenameItemList = allItem.Where(Function(item) item.Value.Mode = "draft").ToDictionary(Function(p) p.Key, Function(p) p.Value)
					AbortItemList = allItem.Where(Function(item) item.Value.Mode = "live").Select(Function(p) p.Key).ToList()
				End If
			Catch ex As Exception
				log.Error("Could not load rename item list", ex)
			End Try
		End If
	End Sub
End Class
