﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="RenameItemList.aspx.vb" Inherits="Sm_subpages_RenameItemList" %>
<%@ Import Namespace="Newtonsoft.Json" %>

<table class="table table-hover">
    <thead>
        <tr>
			<th>Original</th>
			<th></th>
			<th>New</th>
			<th></th>
		</tr>
    </thead>
    <tbody>
        <%If RenameItemList IsNot Nothing AndAlso RenameItemList.Any() Then
        		For Each item In RenameItemList
        %>
		<tr class="renameable-item">
			<td data-field="original"><%=System.Web.HttpUtility.HtmlEncode(item.Key)%></td>
			<td align="center"><i class="fa fa-arrow-right" aria-hidden="true"></i></td>
			<td data-field="new"><%=System.Web.HttpUtility.HtmlEncode(item.Value.NewText)%></td>
			<td align="center">
				<a href="#" data-command="edit">Edit</a>
				<span class="separator">|</span>
				<a href="#" data-command="delete">Delete</a>
			</td>
		</tr>
        <%Next
        Else%>
		<tr class="no-row">
			<td colspan="4" align="center">No item found</td>
		</tr>
	<%End If%>
    </tbody>
</table>
<script type="text/javascript">
	var RENAMEABLE_ABORT_LIST = null;
	<%If AbortItemList IsNot Nothing AndAlso AbortItemList.Any() Then%>
	RENAMEABLE_ABORT_LIST = <%=JsonConvert.SerializeObject(AbortItemList)%>;
	<%End If%>
</script>