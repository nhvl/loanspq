﻿Imports LPQMobile.Utils
Imports System.Xml
Imports Sm
Partial Class Sm_subpages_ShowFieldItemList
	Inherits SmApmBasePage

	Protected ShowFieldList As New Dictionary(Of String, SmShowFieldItemModel)
	Protected VisibilityWarnings As Dictionary(Of String, SmVisibilityWarning)

	Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
		If Not IsPostBack Then
			Try
				Dim type As String = Common.SafeString(Request.Params("type"))

				'For backward compatibility and avoid the need for migration script, use values “xa_special” and “sa_special” for loan_type attribute for both XA Special and XA Minor - don't use values “xa_minor” or “sa_minor”
				'This may have the side effect that a change in XA Special may leak to XA Minor. We would have to ask client to use as-is.
				Dim convertedType As String = type
				If type.ToUpper() = "XA_MINOR" Then
					convertedType = "XA_SPECIAL"
				ElseIf type.ToUpper() = "SA_MINOR" Then
					convertedType = "SA_SPECIAL"
				End If

                VisibilityWarnings = SmUtil.GetVisibilityWarnings(convertedType, WebsiteConfig)

                Dim draftNode As XmlNode = smBL.GetNodeFromDraft(SHOW_FIELDS_ITEMS(convertedType), UserInfo.UserID, LenderConfigID)
				If draftNode IsNot Nothing AndAlso draftNode.NodeType <> XmlNodeType.Comment AndAlso draftNode.HasChildNodes Then
					For Each item As XmlNode In draftNode.SelectNodes("ITEM")
						Dim targetSectionId As String = item.Attributes("controller_id").Value
						If Not ShowFieldList.ContainsKey(targetSectionId) Then
							ShowFieldList.Add(targetSectionId, New SmShowFieldItemModel() With {.LoanType = type, .ControllerId = targetSectionId, .IsVisible = item.Attributes("is_visible").Value, .ControllerName = item.Attributes("controller_name").Value, .Mode = "draft"})
						End If
					Next
				End If
				Dim liveNodeList As XmlNodeList = LenderConfigXml.SelectNodes(SHOW_FIELDS_ITEMS(convertedType))
				If liveNodeList IsNot Nothing AndAlso liveNodeList.Count > 0 Then
					For Each item As XmlNode In liveNodeList
						Dim targetSectionId As String = item.Attributes("controller_id").Value
						If Not ShowFieldList.ContainsKey(targetSectionId) Then
							ShowFieldList.Add(targetSectionId, New SmShowFieldItemModel() With {.LoanType = type, .ControllerId = targetSectionId, .IsVisible = item.Attributes("is_visible").Value, .ControllerName = item.Attributes("controller_name").Value, .Mode = "live"})
						End If
					Next
				End If
			Catch ex As Exception
				log.Error("Could not load show field item list", ex)
			End Try
		End If
	End Sub
End Class
