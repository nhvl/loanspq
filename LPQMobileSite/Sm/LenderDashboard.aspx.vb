﻿
Imports LPQMobile.Utils
Imports Sm
Partial Class Sm_LenderDashboard
	Inherits SmBasePage
	Protected Property BackOfficeLenderID As Guid
	Protected Property LenderID As Guid
	Protected Property BackOfficeLender As SmBackOfficeLender
	Protected Property LenderAdminAllowed As Boolean = False
	Protected Property CanManageUsers As Boolean = True

	Protected Sub Page_PreLoad(sender As Object, e As EventArgs) Handles Me.PreLoad
		Dim smMasterPage = CType(Me.Master, Sm_MasterPage)
		smMasterPage.SelectedMainMenuItem = "lender"
		BackOfficeLenderID = Common.SafeGUID(Request.Params("id"))
		If BackOfficeLenderID = Guid.Empty Then
			Response.Redirect("/Sm/ManageLenders.aspx", True)
		End If
		Dim smBL As New SmBL()
		BackOfficeLender = smBL.GetBackOfficeLenderByID(BackOfficeLenderID)
		If BackOfficeLender Is Nothing Then
			Response.Redirect("/Sm/ManageLenders.aspx", True)
		End If
		LenderID = BackOfficeLender.LenderID
		AllowedRoles.Add(SmSettings.Role.SuperUser.ToString())
		AllowedRoles.Add(String.Format("LenderScope${0}${1}", SmSettings.Role.LenderAdmin.ToString(), LenderID.ToString()))
		NotAllowedAccessRedirectUrl = "/Sm/ManageLenders.aspx"
		smMasterPage.LenderInfo = "(<span id='spLenderName'>" & BackOfficeLender.LenderName & "</span>)"

		LenderAdminAllowed = SmUtil.CheckRoleAccess(HttpContext.Current, SmSettings.Role.SuperUser.ToString(), String.Format("LenderScope${0}${1}", SmSettings.Role.LenderAdmin.ToString(), LenderID.ToString()))
		If UserInfo.SSOAccessRights IsNot Nothing Then
			CanManageUsers = UserInfo.SSOAccessRights.CanManageUsers
		End If

	End Sub
End Class
