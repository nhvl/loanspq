﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="VLSettings.aspx.vb" Inherits="Sm_VLSettings" MasterPageFile="SiteManager.master"  %>

<asp:Content runat="server" ContentPlaceHolderID="plBody">
	<div>
		<h1 class="page-header">Settings</h1>
		<p>Configure settings for your Application Portal</p>
		<section>
			<ul class="nav nav-tabs" id="mainTabs" role="tablist">
				<li role="presentation"><a href="<%=BuildUrl("/sm/generalsettings.aspx")%>" role="tab">General</a></li>
				<li role="presentation" <%=IIf(EnableXA, "", "class='hidden'")%>><a href="<%=BuildUrl("/sm/xasettings.aspx")%>" role="tab">XA</a></li>
				<li role="presentation" <%=IIf(EnableCC Or EnableHE Or EnablePL Or EnableVL, "", "class='hidden'")%>><a href="<%=BuildUrl("/sm/loanssettings.aspx")%>" role="tab">All Loans</a></li>
				<li role="presentation" <%=IIf(EnableCC, "", "class='hidden'")%>><a href="<%=BuildUrl("/sm/ccsettings.aspx")%>" role="tab">Credit Card Loan</a></li>
				<li role="presentation" <%=IIf(EnableHE, "", "class='hidden'")%>><a href="<%=BuildUrl("/sm/hesettings.aspx")%>" role="tab">Home Equity Loan</a></li>
				<li role="presentation" <%=IIf(EnableLQB, "", "class='hidden'")%>><a href="<%=BuildUrl("/sm/mlsettings.aspx")%>" role="tab">Mortgage Loan</a></li>
				<li role="presentation" <%=IIf(EnablePL, "", "class='hidden'")%>><a href="<%=BuildUrl("/sm/plsettings.aspx")%>" role="tab">Personal Loan</a></li>
				<li role="presentation" class="active"><a href="#" aria-controls="vlloans" role="tab">Vehicle Loan</a></li>
				<li role="presentation" <%=IIf(EnableBL, "", "class='hidden'")%>><a href="<%=BuildUrl("/sm/blsettings.aspx")%>" role="tab">Business Loan</a></li>
			</ul>
			<div class="tab-content">
				<div role="tabpanel" class="tab-pane active" id="vlloans">
					<h2 class="section-title">Vehicle Loans</h2>
					<p>These settings will only affect Vehicle Loan applications.</p>
					<%If SmUtil.CheckRoleAccess(Context, SmSettings.Role.LegacyOperator, SmSettings.Role.SuperOperator) Then%>
					<div class="bottom30">
						<div class="group-heading">INTEGRATIONS</div>
						<div class="input-block">
							<h3 class="property-title">Vin Number Laser Barcode Scan</h3>
							<p>Enterprise-Grade barcode scanner with performance and precision of dedicated laser scanner</p>
							<label class="toggle-btn disabled" data-on="ON" data-off="OFF">
								<input type="checkbox" id="chkVLVinBarcodeScan" <%=BindCheckbox(VLVinBarcodeScan)%>/>
								<span class="button-checkbox"></span>
							</label>
							
						</div>
					</div>
					<%End If%>
					<div class="bottom30">
						<div class="group-heading">IN-BRANCH CONFIGURE SITE</div>						
						<%--<div class="input-block">
							<h3 class="property-title">Booking</h3>
							<p>Book application to core <br/>(If interested, please contact Support via support@loanspq.com. SOW is required.)</p>
							<select id="ddlVLBooking" class="form-control inline-select" disabled="disabled">
								<option value="" <%=BindSelectbox(VLBooking, "")%>>Not set</option>
								<% For Each item As KeyValuePair(Of String, String) In SmSettings.BookList%>
								<option <%=BindSelectbox(VLBooking, item.Key)%> value="<%=item.Key%>"><%=item.Value%></option>
								<%Next%>
							</select>
												
						</div>--%>
						
						<%--<div class="input-block">
								<h3 class="property-title">Auto Cross Qualify</h3>
								<p>Allow the system to show other products that applicant may qualify</p>
								<label class="toggle-btn" data-on="ON" data-off="OFF">
									<input type="checkbox" id="chkVLXSell" value="" <%=BindCheckbox(VLXSell)%>/>
								<span class="button-checkbox"></span>
							</label>							
						</div>--%>
						
						<div class="input-block">
								<h3 class="property-title">In-Session DocuSign</h3>
								<p>Show document for applicant to sign <br />(This must be enabled with credentials for in-branch.)</p>
								<label class="toggle-btn" data-on="ON" data-off="OFF">
									<input type="checkbox" id="chkVLDocuSign" value="" <%=BindCheckbox(VLDocuSign)%>/>
								<span class="button-checkbox"></span>
							</label>							
						</div>

						<div class="input-block">
							<h3 class="property-title">In-Session DocuSign PDF Group Override</h3>
							<p>System uses this PDF group instead of the default.</p>
							<input type="text" class="form-control" id="txtVLDocuSignGroup" value="<%=VLDocuSignPDFGroup%>" />							
						</div>
					</div>

					<div class="bottom30">
						<div class="group-heading">WORKFLOWS</div>
						<div class="input-block">
							<h3 class="property-title">Joint Applicants</h3>
							<p>Allow your applicants to apply for a joint loan.</p>
							<label class="toggle-btn" data-on="ON" data-off="OFF">
								<input type="checkbox" id="chkVlJointEnable" value="" <%=BindCheckbox(VLJointEnable)%>/>
								<span class="button-checkbox"></span>
							</label>
						</div>
                        <div class="input-block">
							<h3 class="property-title">Loan Term Free Form Text</h3>
							<p>Enable free form text instead of dropdown for Loan Term field.  This only apply for Auto/Pickup Vehicle Type.</p>
							<label class="toggle-btn" data-on="ON" data-off="OFF">
								<input type="checkbox" id="chkVLTermFreeFormMode" value="" <%=BindCheckbox(VLTermFreeTextMode)%>/>
								<span class="button-checkbox"></span>
							</label>
						</div>
					</div>
					<div class="bottom30">
						<div class="group-heading">MISC VALUES</div>
						<div class="input-block">
							<h3 class="property-title">Interest Rate</h3>
							<p>Estimated payment calculator default rate.</p>
							<input type="text" class="form-control inline-textbox number float-number" id="txtVLRate" value="<%=VLInterestRate%>" /><span> %</span>
						</div>
						
					</div>
                    <div class="bottom30">
                       <div class="input-block">
							<h3 class="property-title">Location Pool Filter</h3>
							<p>Show available product(s) based on zip code. Zip pools and applicable settings are configured in the in-branch settings.</p>
							<label class="toggle-btn" data-on="ON" data-off="OFF">
								<input type="checkbox" id="chkVLLocationPool" value="" <%=BindCheckbox(VLLocationPool)%>/>
								<span class="button-checkbox"></span>
							</label>
						</div>
                    </div>
                     <div class="bottom30">
                       <div class="input-block">
							<h3 class="property-title">Previous Address</h3>
							<p>Require previous address if duration at current address does not meet a specified minimum duration.</p>
							<label class="toggle-btn" data-on="ON" data-off="OFF">
								<input type="checkbox" id="chkVLPreviousAddress" value="" <%=BindCheckbox(VLPreviousAddressEnable)%>/>
								<span class="button-checkbox"></span>
							</label>
						</div>
                       <div class="input-block <%=IIf(VLPreviousAddressEnable, "", "hidden")%>">						
						    <p>Minimum Duration (months)</p>
						    <input type="text" class="form-control number" id="txtVLPreviousAddressThreshold" value="<%=VLPreviousAddressThreshold%>" maxlength ="3" />							
					    </div>
                    </div>
                   
                     <div class="bottom30">
                       <div class="input-block">
							<h3 class="property-title">Previous Employment</h3>
							<p>Require previous employment if duration at current employment does not meet a specified minimum duration.</p>
							<label class="toggle-btn" data-on="ON" data-off="OFF">
								<input type="checkbox" id="chkVLPreviousEmployment" value="" <%=BindCheckbox(VLPreviousEmploymentEnable)%>/>
								<span class="button-checkbox"></span>
							</label>
						</div>
                       <div class="input-block <%=IIf(VLPreviousEmploymentEnable, "", "hidden")%>" >						
						    <p>Minimum Duration (months)</p>
						    <input type="text" class="form-control number" id="txtVLPreviousEmploymentThreshold" value="<%=VLPreviousEmploymentThreshold%>" maxlength="3"/>							
					   </div>
                    </div>
					<div data-name="Declarations">
						<div class="group-heading">DECLARATIONS</div>
						<div class="input-block">
							<p>Enable declarations to display in the Vehicle Loan application.</p>
							<%If DeclarationList IsNot Nothing AndAlso DeclarationList.Count > 0 Then%>
							<%	 
								Dim index As Integer = 0
								For Each item In DeclarationList
									index += 1
									%>
							<div class="checkbox-btn-wrapper">
								<span><%=String.Format("{0}. {1}", index, item.Value.DisplayText)%></span>
								<label class="toggle-btn pull-right" data-on="ON" data-off="OFF">
									<input type="checkbox" value="<%=item.Key%>" <%=IIf(item.Value.Active, "checked='checked'", "")%>/>
									<span class="button-checkbox"></span>
								</label>
							</div>
							<%Next%>
							<%End If%>
						</div>
					</div>
				</div>
			</div>
		</section>
	</div>
</asp:Content>
<asp:Content runat="server" ContentPlaceHolderID="plScripts">
	<script type="text/javascript">
		$(function () {
			registerDataValidator();
			$("input:text").each(function (idx, ele) {
				$(ele).on({
					keypress: function () { master.FACTORY.documentChanged(ele); },
					paste: function () { master.FACTORY.documentChanged(ele); },
					cut: function () { master.FACTORY.documentChanged(ele); }
				});
            });
               //show minimum address duration input field only when the chkPreviousAddress is on 
            $("#chkVLPreviousAddress").on("change", function () {
                var $self = $(this);
                var $previousAddressThreshold = $('#txtVLPreviousAddressThreshold');
                if ($self.is(":checked")) {
                    $previousAddressThreshold.closest('div').removeClass('hidden');
                } else {
                    //clear and hide the field
                    $previousAddressThreshold.val(""); 
                    $previousAddressThreshold.closest('div').addClass('hidden');
                }
            });
            
            //show minimum employment duration input field only when the chkPreviousEmployement is on 
            $("#chkVLPreviousEmployment").on("change", function () {
                var $self = $(this);
                var $previousEmploymentThreshold = $('#txtVLPreviousEmploymentThreshold');
                if ($self.is(":checked")) {
                    $previousEmploymentThreshold.closest('div').removeClass('hidden');
                } else {
                    //clear and hide the field
                    $previousEmploymentThreshold.val(""); 
                    $previousEmploymentThreshold.closest('div').addClass('hidden');
                }
            });
		});
		(function (master, $, undefined) {
			master.FACTORY.SaveDraft = function (revisionId) {
				if ($.smValidate("ValidateData") == false) {
					_COMMON.noty("error", "Error: please check all fields.", 500);
					return;
				}

				var dataObj = collectSubmitData();
				dataObj.revisionId = revisionId;
				dataObj.command = 'savevlsettings';
				$.ajax({
					url: "/sm/smhandler.aspx",
					async: true,
					cache: false,
					type: 'POST',
					dataType: 'html',
					data: dataObj,
					success: function (responseText) {
						var response = $.parseJSON(responseText);
						if (response.IsSuccess) {
							master.FACTORY.onSaveDraftSuccess();
						} else {
							_COMMON.noty("error", "Error", 500);
						}
					}
				});
			};
			master.FACTORY.Preview = function (revisionId) {
				if ($.smValidate("ValidateData") == false) {
					_COMMON.noty("error", "Error: please check all fields.", 500);
					return;
				}
				var dataObj = collectSubmitData();
				dataObj.revisionId = revisionId;
				dataObj.command = 'previewvlsettings';
				$.ajax({
					url: "/sm/smhandler.aspx",
					async: true,
					cache: false,
					type: 'POST',
					dataType: 'html',
					data: dataObj,
					success: function (responseText) {
						var response = $.parseJSON(responseText);
						if (response.IsSuccess) {
							master.FACTORY.onPreviewSuccess();
							_COMMON.openInNewTab(response.Info.previewurl);
						} else {
							_COMMON.noty("error", "Error", 500);
						}
					}
				});
			};
			master.FACTORY.validateBeforePublish = function () {
				var result = $.smValidate("ValidateData");
				if (result == false) {
					_COMMON.noty("error", "Error: please check all fields.", 500);
				}
				return result;
			}
			master.FACTORY.Publish = function (comment, publishAll) {
				if ($.smValidate("ValidateData") == false) {
					_COMMON.noty("error", "Error: please check all fields.", 500);
					return;
				}
				var dataObj = collectSubmitData();
				dataObj.command = 'publishvlsettings';
				dataObj.comment = comment;
				dataObj.publishAll = publishAll;
				$.ajax({
					url: "/sm/smhandler.aspx",
					async: true,
					cache: false,
					type: 'POST',
					dataType: 'html',
					data: dataObj,
					success: function (responseText) {
						var response = $.parseJSON(responseText);
						if (response.IsSuccess) {
							master.FACTORY.onPublishSuccess();
						} else {
							_COMMON.noty("error", "Error", 500);
						}
					}
				});
			};
		}(window.master = window.master || {}, jQuery));
		function registerDataValidator() {
			$.smValidate.removeValidationGroup("ValidateData");
			$("input.float-number").each(function (idx, input) {
				$(input).observer({
					validators: [
						function (partial) {
							var $self = $(this);
							if (/^[0-9]+(\.[0-9]+)*$/.test($self.val()) == false ) {
								return "Invalid data";
							}
							return "";
						}
					],
					validateOnBlur: true,
					group: "ValidateData"
				});
            });
             //validate minimum employment duration
            $('#txtVLPreviousEmploymentThreshold').observer({
					validators: [
						function (partial) {
                            var $self = $(this);
                            if ($self.closest('div').hasClass('hidden')) return ""; //skip validating if the field is hidden
							if (($self.val().trim() !="" && /^[0-9]+$/.test($self.val()) == false) ||parseInt($self.val()) >= 120) {
								return "Invalid minimum duration";
						    }
							return "";
						}
					],
					validateOnBlur: true,
					group: "ValidateData"
            });
             //validate minimum address duration
            $('#txtVLPreviousAddressThreshold').observer({
					validators: [
						function (partial) {
                            var $self = $(this);
                            if ($self.closest('div').hasClass('hidden')) return ""; //skip validating if the field is hidden
						    if (($self.val().trim() !="" && /^[0-9]+$/.test($self.val()) == false) ||parseInt($self.val()) >= 120) {
								return "Invalid minimum duration";
						    }
							return "";
						}
					],
					validateOnBlur: true,
					group: "ValidateData"
			});
		}
		function collectSubmitData() {
			var result = {};
			result.lenderconfigid = '<%=LenderConfigID%>';
			//result.vl_booking = $("#ddlVLBooking").val();
			//result.vl_xsell = $("#chkVLXSell").is(":checked");
			result.vl_docusign = $("#chkVLDocuSign").is(":checked");
			result.vl_docusign_group = $("#txtVLDocuSignGroup").val();
			result.vl_interest_rate = parseFloat($("#txtVLRate").val());
			result.vl_joint_enable = $("#chkVlJointEnable").is(":checked");
			result.vl_vin_barcode_scan_enable = $("#chkVLVinBarcodeScan").is(":checked");
            result.vl_term_free_form_mode = $("#chkVLTermFreeFormMode").is(":checked");
            result.vl_location_pool = $("#chkVLLocationPool").is(":checked");
            result.vl_previous_employment_threshold = $("#txtVLPreviousEmploymentThreshold").val();
            result.vl_previous_address_threshold = $("#txtVLPreviousAddressThreshold").val();
            result.vl_previous_address_enable = $('#chkVLPreviousAddress').is(":checked");
            result.vl_previous_employment_enable = $('#chkVLPreviousEmployment').is(":checked");
            var activeDeclarationList = [];
            $(".checkbox-btn-wrapper input[type='checkbox']:checked", "div[data-name='Declarations']").each(function (idx, ele) {
            	activeDeclarationList.push($(ele).val());
            });
            result.vl_declarations = JSON.stringify(activeDeclarationList);
			return result;
		}
	</script>
</asp:Content>
<asp:Content runat="server" ContentPlaceHolderID="plActionButtons"></asp:Content>