﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="ManageLenders.aspx.vb" Inherits="Sm_ManageLenders" MasterPageFile="MasterPage.master" %>

<asp:Content runat="server" ContentPlaceHolderID="plBody">
	<div>
		<h1 class="page-header">Lender - Portal List</h1>
        <%If Not (SmUtil.CheckRoleAccess(HttpContext.Current, SmSettings.Role.SSOUser)) Then%>
		<p>Search for Application Portal via lender name, lenderref, city, or state</p>
        <div class="search-bar">
			<div class="input-group">
				<input id="txtSearch" type="text" class="form-control" placeholder="Search for..." />
				<span class="input-group-btn"><button class="btn btn-secondary" type="button" id="btnSearch">Search</button></span>
			</div>
		</div>
        <%  Else %>
        <p />
        <p />
        <%End If%>
		<div class="grid" id="divLenderList">
			<div class="spinner">
				<div class="rect1"></div>
				<div class="rect2"></div>
				<div class="rect3"></div>
				<div class="rect4"></div>
				<div class="rect5"></div>
			</div>
			<p>Loading...</p>
		</div>
	</div>
</asp:Content>
<asp:Content runat="server" ContentPlaceHolderID="plScripts">
	<script type="text/javascript" src="/Sm/js/bootstrap/bootstrap-typeahead.js"></script>
	<script type="text/javascript">
		_COMMON_PAGE_SIZE = 10;
		var defaultRedirectUrl = "/Sm/Index.aspx";
		<%--<%If HttpContext.Current.User.IsInRole(SmSettings.Role.SuperOperator.ToString()) Or HttpContext.Current.User.IsInRole(SmSettings.Role.LegacyOperator.ToString()) Or HttpContext.Current.User.IsInRole(SmSettings.Role.Officer.ToString()) Then%>
		defaultRedirectUrl = "/sm/lenderdashboard.aspx";
		<%end if%>--%>
		

		$(function () {
			var lenderGrid;
			lenderGrid = new _COMMON.Grid("/sm/smhandler.aspx", "lenderGrid", "divLenderList", manageLenders.FACTORY, manageLenders.FACTORY.onLoaded, { command: "loadLenderGrid" });
			lenderGrid.init();
			manageLenders.FACTORY.init(lenderGrid.GridObject);
			$("#btnSearch").on("click", manageLenders.FACTORY.search);
			$('#txtSearch').typeahead({
				ajax: {
					url: '/sm/smhandler.aspx',
					method: 'POST',
					displayField: "name",
					valueField: "id",
					dataType: 'json',
					preDispatch: function() {
						return { query: $('#txtSearch').val(), command: "getAllLenders" };
					},
					preProcess: function(data) {
						return data;
					}
				},
				scrollBar: true,
				onSelect: function (item) {
					var win = window.open(defaultRedirectUrl + "?lenderconfigid=" + item.value, '_blank');
					win.focus();
				}
			});
		});
		(function (manageLenders, $, undefined) {
			manageLenders.DATA = {};
			manageLenders.FACTORY = {};
			manageLenders.FACTORY.init = function (gridInstance) {
				var self = this;
				manageLenders.DATA.grid = gridInstance;
			};
			manageLenders.FACTORY.getFilterInfo = function () {
				//implement this later
				return {Keyword: $.trim($("#txtSearch").val())};
			};
			manageLenders.FACTORY.onLoaded = function () {
				$("a[data-command='audit-logs']", "#divLenderList").each(function(idx, ele) {
					var $self = $(ele);
					$self.on("click", function () {
						//title, dataId, containerId, htmlContent, getContentUrl, postData, onInitCallback, onClosingCallback, size, closable, hasCloseButton
                        var portalId = $self.closest("tr").data("portal-id");
                        var lenderRef = $self.closest("tr").data("lender-ref");
                        _COMMON.showDialog("Modification History&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;(" + lenderRef + ")"
                            , portalId
                            , "loadauditlogs_dialog"
                            , '<div class="spinner"><div class="rect1"></div><div class="rect2"></div><div class="rect3"></div><div class="rect4"></div><div class="rect5"></div></div><p>Loading...</p>'
                            , ""
                            , null
                            , function (container) {
                                //init grid
                                $("#loadauditlogs_dialog").closest("div.modal-dialog").css({ "width": "80%" });
                                var auditLogsGrid = new _COMMON.Grid("/sm/smhandler.aspx", "auditLogsGrid", "loadauditlogs_dialog", null, null, { command: "loadAuditLogs", lenderConfigId: portalId });
                                auditLogsGrid.init();
                            }
                            , null
                            , BootstrapDialog.SIZE_WIDE
                            , false
                            , false);
					});
				});
				$("a[data-command='clone']", "#divLenderList").each(function (idx, ele) {
					var $self = $(ele);
					$self.on("click", function () {
						//title, dataId, containerId, htmlContent, getContentUrl, postData, onInitCallback, onClosingCallback
						var portalId = $self.closest("tr").data("portal-id");
						if ($self.closest("tr").hasClass("lender-row")) {
							_COMMON.showDialogSaveClose("Clone lender", 0, "clonelender_dialog_0", "", "/sm/smhandler.aspx", { command: "loadCloneLenderForm", lenderConfigId: portalId }, manageLenders.FACTORY.cloneLender, function (container) {
								//add validator
								registerDataValidator($("#clonelender_dialog_0"), "clonelender_dialog_0");
								$("#btnGenerateLenderID").on("click", function() {
									manageLenders.FACTORY.generateGuid(this, "#txtLenderID");
								});
								$("#btnGenerateOrganizationID").on("click", function () {
									manageLenders.FACTORY.generateGuid(this, "#txtOrganizationID");
								});

							}, function () {
								//reload grid when dialog closed
								var pageIndex = manageLenders.DATA.grid.getPaginationSetting().page;
								manageLenders.DATA.grid.setFilter(manageLenders.FACTORY.getFilterInfo()).setPageIndex(pageIndex).load();
							}, BootstrapDialog.SIZE_NORMAL, false);
						} else {
							_COMMON.showDialogSaveClose("Clone portal", 0, "cloneportal_dialog_0", "", "/sm/smhandler.aspx", { command: "loadClonePortalForm", lenderConfigId: portalId }, manageLenders.FACTORY.clonePortal, function (container) {
								//add validator
								registerDataValidator($("#cloneportal_dialog_0"), "cloneportal_dialog_0");

							}, function () {
								//reload grid when dialog closed
								var pageIndex = manageLenders.DATA.grid.getPaginationSetting().page;
								manageLenders.DATA.grid.setFilter(manageLenders.FACTORY.getFilterInfo()).setPageIndex(pageIndex).load();
							}, BootstrapDialog.SIZE_NORMAL, false);
						}
						
					});
				});
			};
			manageLenders.FACTORY.cloneLender = function(obj) {
				if ($.smValidate("clonelender_dialog_0") == false) return;
				var data = obj.container.find("form").serializeJSON({ parseAll: true, checkboxUncheckedValue: "false" });
				//update data
				var postData = {
					command: "cloneLender",
					post_data: _COMMON.toJSON(data)
				};
				$.ajax({
					url: "/sm/smhandler.aspx",
					async: true,
					cache: false,
					type: 'POST',
					dataType: 'html',
					data: postData,
					success: function (responseText) {
						var response = $.parseJSON(responseText);
						if (response.IsSuccess) {
							_COMMON.noty("success", "Success", 500);
							obj.dialog.close();
						}else if (response.Info == "EXISTED") {
							_COMMON.showAlertDialog(response.Message);
						} else {
							var errorMsg = response.Message !== "" ? response.Message : "Error";
							_COMMON.noty("error", errorMsg, 500);
						}
					}
				});
			}
			manageLenders.FACTORY.clonePortal = function (obj) {
				if ($.smValidate("cloneportal_dialog_0") == false) return;
				var data = obj.container.find("form").serializeJSON({ parseAll: true, checkboxUncheckedValue: "false" });
				//update data
				var postData = {
					command: "clonePortal",
					post_data: _COMMON.toJSON(data)
				};
				$.ajax({
					url: "/sm/smhandler.aspx",
					async: true,
					cache: false,
					type: 'POST',
					dataType: 'html',
					data: postData,
					success: function (responseText) {
						var response = $.parseJSON(responseText);
						if (response.IsSuccess) {
							_COMMON.noty("success", "Success", 500);
							obj.dialog.close();
						} else if (response.Info == "EXISTED") {
							_COMMON.showAlertDialog(response.Message);
						} else {
							var errorMsg = response.Message !== "" ? response.Message : "Error";
							_COMMON.noty("error", errorMsg, 500);
						}
					}
				});
			}
			manageLenders.FACTORY.search = function () {
				manageLenders.DATA.grid.setFilter(manageLenders.FACTORY.getFilterInfo()).setPageIndex(1).load();
			};
			manageLenders.FACTORY.generateGuid = function (btn, txt) {
				var $self = $(btn);
				$self.button("loading");
				$.ajax({
					url: "/sm/smhandler.aspx",
					async: true,
					cache: false,
					type: 'POST',
					dataType: 'html',
					data: {command: "generateGuid"},
					success: function (responseText) {
						$self.button("reset");
						var response = $.parseJSON(responseText);
						if (response.IsSuccess) {
							$(txt).val(response.Message);
							$.smValidate.hideValidation(txt);
						} else {
							_COMMON.noty("error", "Error", 500);
						}
					}
				});
			}
		}(window.manageLenders = window.manageLenders || {}, jQuery));
		function registerDataValidator(container, groupName) {
			var $container = $(container);
			$.smValidate.removeValidationGroup(groupName);
			$("[data-required='true'],[data-format]", $container).each(function (idx, ele) {
				$(ele).observer({
					validators: [
						function (partial) {
							var $self = $(this);
							if ($self.data("required") && $.trim($self.val()) === "") {
								if ($self.data("message-require")) {
									return $self.data("message-require");
								} else {
									return "This field is required";
								}
							}
							var format = $self.data("format");
							if (format) {
								var msg = "Invalid format";
								if ($self.data("message-invalid-data")) {
									msg = $self.data("message-invalid-data");
								}
								if (format === "email") {
									if (_COMMON.ValidateEmail($self.val()) === false) {
										return msg;
									}
								} else if (format === "phone") {
									if (_COMMON.ValidatePhone($self.val()) === false) {
										return msg;
									}
								} else if ($self.val() != "" && format === "positive-number") {
									if ((parseInt($self.val()) > 0) == false) {
										return msg;
									}
								}else if (format === "guid") {
									if (/^[0-9A-Za-z]{8}-[0-9A-Za-z]{4}-[0-9A-Za-z]{4}-[0-9A-Za-z]{4}-[0-9A-Za-z]{12}$/.test($self.val()) == false) {
										return msg;
									}
								}
							}
							return "";
						}
					],
					validateOnBlur: true,
					group: groupName
				});
			});
		}
	</script>
</asp:Content>
<asp:Content runat="server" ContentPlaceHolderID="plActionButtons">
</asp:Content>