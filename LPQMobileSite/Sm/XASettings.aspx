﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="XASettings.aspx.vb" Inherits="Sm_XASettings" MasterPageFile="SiteManager.master"  %>

<asp:Content runat="server" ContentPlaceHolderID="plBody">
	<div>
		<h1 class="page-header">Settings</h1>
		<p>Configure settings for your Application Portal</p>
		<section>
			<ul class="nav nav-tabs" id="mainTabs" role="tablist">
				<li role="presentation"><a href="<%=BuildUrl("/sm/generalsettings.aspx")%>" role="tab">General</a></li>
				<li role="presentation" class="active"><a href="#" aria-controls="xa" role="tab">XA</a></li>
				<li role="presentation" <%=IIf(EnableCC Or EnableHE Or EnablePL Or EnableVL, "", "class='hidden'")%>><a href="<%=BuildUrl("/sm/loanssettings.aspx")%>" role="tab">All Loans</a></li>
				<li role="presentation" <%=IIf(EnableCC, "", "class='hidden'")%>><a href="<%=BuildUrl("/sm/ccsettings.aspx")%>" role="tab">Credit Card Loan</a></li>
				<li role="presentation" <%=IIf(EnableHE, "", "class='hidden'")%>><a href="<%=BuildUrl("/sm/hesettings.aspx")%>" role="tab">Home Equity Loan</a></li>
				<li role="presentation" <%=IIf(EnableLQB, "", "class='hidden'")%>><a href="<%=BuildUrl("/sm/mlsettings.aspx")%>" role="tab">Mortgage Loan</a></li>
				<li role="presentation" <%=IIf(EnablePL, "", "class='hidden'")%>><a href="<%=BuildUrl("/sm/plsettings.aspx")%>" role="tab">Personal Loan</a></li>
				<li role="presentation" <%=IIf(EnableVL, "", "class='hidden'")%>><a href="<%=BuildUrl("/sm/vlsettings.aspx")%>" role="tab">Vehicle Loan</a></li>
				<li role="presentation" <%=IIf(EnableBL, "", "class='hidden'")%>><a href="<%=BuildUrl("/sm/blsettings.aspx")%>" role="tab">Business Loan</a></li>
			</ul>
			<div class="tab-content">
				<div role="tabpanel" class="tab-pane active" id="xa">
					<h2 class="section-title">Xpress Account</h2>
					<p>These settings will only affect Xpress Account (XA).</p>
					<div class="bottom30">
						<div class="group-heading">INTEGRATIONS</div>
						<div class="input-block">
							<h3 class="property-title">Document Upload</h3>
                            <div id="docUploadTemplate" class="hidden">
                                <p name="docUpload-title"></p>
                                <p name="docUpload-sub-title">Allow your applicants to capture/upload images or documents.<%--<br />(This feature is enabled automatically when the message is not empty.)--%></p>
                                <label name="docUpload-label" class="toggle-btn" data-on="ON" data-off="OFF">
                                    <input name="docUpload-input" type="checkbox" value="" onchange="toggleChange(event);"/>
                                    <span class="button-checkbox"></span>
                                </label>
                                <div class="hint-content">
                                    <p>Message (required for feature to show)</p>
                                    <div name="docUpload-msg" class="html-editor" contenteditable="true" id="accountUploadDocMsgTmpl"></div>
                                </div>
                            </div>
                            <div id="docUploadContainer"></div>
						</div>
					</div>
					
					<div class="bottom30">
						<div class="group-heading">IN-BRANCH CONFIGURE SITE</div>	
						<div class="input-block">
							<h3 class="property-title">ID Authentication</h3>
							<p>Verify your applicants' identities through a third party authentication vendor  <br />(This must be enabled with credentials in Interface Preferences as well as the Integration Info tab per product.)</p>
							<select id="ddlIDAuth" class="form-control inline-select">
								<option value="" <%=BindSelectbox(Ida, "")%>>Not set</option>
								<% For Each item As KeyValuePair(Of String, String) In SmSettings.IDAList%>
								<option <%=BindSelectbox(Ida, item.Key)%> value="<%=item.Key%>"><%=item.Value%></option>
								<%Next%>
							</select>
							<%--<label class="toggle-btn" data-on="ON" data-off="OFF">
								<input type="checkbox" value=""/>
								<span class="button-checkbox"></span>
							</label>--%>
							
						</div>
						<div class="input-block">
							<h3 class="property-title">Debit Bureau</h3>
							<p>Run Debit Bureau checks through a third party vendor  <br />(This must be enabled with credentials in Interface Preferences as well as the Integration Info tab per product.)</p>
							<%--<label class="toggle-btn disabled" data-on="ON" data-off="OFF">
								<input type="checkbox" disabled="disabled" value="" <%=BindCheckbox(Debit)%>/>
								<span class="button-checkbox"></span>
							</label>--%>
							<select id="ddlDebitBureau" class="form-control inline-select">
								<option value="" <%=BindSelectbox(Debit, "")%>>Not set</option>
								<% For Each item As KeyValuePair(Of String, String) In SmSettings.DebitList%>
								<option <%=BindSelectbox(Debit, item.Key)%> value="<%=item.Key%>"><%=item.Value%></option>
								<%Next%>
							</select>
							
						</div>
						<div class="input-block">
							<h3 class="property-title">Credit Pull</h3>
							<p>Pull applicant's credit report  <br />(This must be enabled with credentials in Interface Preferences as well as the Integration Info tab per product.)</p>
							<label class="toggle-btn <%=BindCheckbox(CreditPull)%>" data-on="ON" data-off="OFF">
								<input type="checkbox" id="chkCreditPull" value="" <%=BindCheckbox(CreditPull)%>/>
								<span class="button-checkbox"></span>
							</label>
							<div class="hint-content">  <%--this is controlled by credit pull--%>
								<h3 class="property-title">Decision XA</h3>
								<p>Underwrite product</p>
								<label class="toggle-btn" data-on="ON" data-off="OFF">
									<input type="checkbox" id="chkDecisionXaEnabled" value="" <%=BindCheckbox(DecisionXAEnabled)%>/>
									<span class="button-checkbox"></span>
								</label>
							</div>
						</div>

						<div class="input-block">
							<h3 class="property-title">XA Status</h3>
							<p>System shall change XA to this status after successfully completed underwrite process.  <br />If underwrite process fails, system will automatically set  XA status to referral.</p>
							<select id="ddlXAStatus" class="form-control inline-select">
								<option value="" <%=BindSelectbox(XALoanStatus, "")%>>Not set</option>
								<% For Each item As KeyValuePair(Of String, String) In SmSettings.LoanStatusList%>
								<option <%=BindSelectbox(XALoanStatus, item.Key)%> value="<%=item.Key%>"><%=item.Value%></option>
								<%Next%>
							</select>							
						</div>

						<div class="input-block">
								<h3 class="property-title">Funding</h3>
								<p>Stand-alone funding without booking</p>
								<label class="toggle-btn" data-on="ON" data-off="OFF">
									<input type="checkbox" id="chkXAFunding" value="" <%=BindCheckbox(XAFunding)%>/>
								<span class="button-checkbox"></span>
							</label>							
						</div>
						<div class="input-block">
								<h3 class="property-title">Free-Form Account Numbers for Funding</h3>
								<p>Turning on this feature will allow account numbers to contain decimals, dashes, spaces, and letters. Be sure the in-branch system also supports free-form account numbers.</p>
								<label class="toggle-btn" data-on="ON" data-off="OFF">
									<input type="checkbox" id="chkXAFundingAccountNumberFreeForm" value="" <%=BindCheckbox(XAFundingAccountNumberFreeForm)%>/>
								<span class="button-checkbox"></span>
							</label>							
						</div>
						<div class="input-block">
							<h3 class="property-title">Booking</h3>
							<p>Book application to core</p>
							<select id="ddlXABooking" class="form-control inline-select disabled" disabled="disabled">
								<option value="" <%=BindSelectbox(XABooking, "")%>>Not set</option>
								<% For Each item As KeyValuePair(Of String, String) In SmSettings.BookList%>
								<option <%=BindSelectbox(XABooking, item.Key)%> value="<%=item.Key%>"><%=item.Value%></option>
								<%Next%>
							</select>
												
						</div>

						<%--<div class="input-block">
								<h3 class="property-title">Auto Cross Qualify</h3>
								<p>Allow the system to show other products that applicant may qualify</p>
								<label class="toggle-btn" data-on="ON" data-off="OFF">
									<input type="checkbox" id="chkXAXSell" value="" <%=BindCheckbox(XAXSell)%>/>
								<span class="button-checkbox"></span>
							</label>							
						</div>--%>
						
						<div class="input-block">
							<h3 class="property-title">In-Session DocuSign</h3>
							<p>Show document for applicant to sign <br />(This must be enabled with credentials for in-branch.)</p>
							<label class="toggle-btn" data-on="ON" data-off="OFF">
									<input type="checkbox" id="chkXADocuSign" value="" <%=BindCheckbox(XADocuSign)%>/>
								<span class="button-checkbox"></span>
							</label>							
						</div>
						
						<div class="input-block">
							<h3 class="property-title">In-Session DocuSign PDF Group Override</h3>
							<p>System uses this PDF group instead of the default AUTO_CONSUMER_DOCUSIGN PDF group.</p>
							<p>The Application Portal requires all PDFs that are tied to account level to be in this PDF group - use any other PDF groups for PDFs that are tied to product(s).</p> 
							<input type="text" class="form-control" id="txtXADocuSignGroup" value="<%=XADocuSignPDFGroup%>" />						
						</div>

					</div> <%--''BACK OFFICE INTEGRATIONS--%>

					<div class="bottom30">
						<div class="group-heading">WORKFLOWS</div>
						<div class="input-block">
							<h3 class="property-title">Professional/Job Title for Employment Information</h3>
							<p>Collect only an applicant's profession/job title in the employment section. If the applicant is unemployed or retired, he/she will be asked for his/her former profession/job title.</p>
							<label class="toggle-btn" data-on="ON" data-off="OFF">
								<input type="checkbox" id="chkXaCollectJobTitleOnly" value="" <%=BindCheckbox(XACollectJobTitleOnly)%>/>
								<span class="button-checkbox"></span>
							</label>
						</div>
						<div class="input-block">
							<h3 class="property-title">Joint Applicants (New <%:IIf(IsBankPortal, "Account", "Member")%>)</h3>
							<p>Allow your applicants to apply for a joint Xpress Account.</p>
							<label class="toggle-btn" data-on="ON" data-off="OFF">
								<input type="checkbox" id="chkXaJointEnable" value="" <%=BindCheckbox(XAJointEnable)%>/>
								<span class="button-checkbox"></span>
							</label>
						</div>
						<div class="input-block">
							<h3 class="property-title">Joint Applicants (Existing <%:IIf(IsBankPortal, "Account", "Member")%>)</h3>
							<p>Allow your applicants to apply for a joint secondary Xpress Account.</p>
							<label class="toggle-btn" data-on="ON" data-off="OFF">
								<input type="checkbox" id="chkSaJointEnable" value="" <%=BindCheckbox(SAJointEnable)%>/>
								<span class="button-checkbox"></span>
							</label>
						</div>
						<div class="input-block">
							<h3 class="property-title">Location Pool Filter</h3>
							<p>Show available product(s) based on zip code. Zip pools and applicable settings are configured in the in-branch settings.</p>
							<label class="toggle-btn" data-on="ON" data-off="OFF">
								<input type="checkbox" id="chkXaLocationPool" value="" <%=BindCheckbox(XALocationPool)%>/>
								<span class="button-checkbox"></span>
							</label>
						</div>
                        
                    <div class="bottom30">
                       <div class="input-block">
							<h3 class="property-title">Previous Address</h3>
							<p>Require previous address if duration at current address does not meet a specified minimum duration.</p>
							<label class="toggle-btn" data-on="ON" data-off="OFF">
								<input type="checkbox" id="chkXAPreviousAddress" value="" <%=BindCheckbox(XAPreviousAddressEnable)%>/>
								<span class="button-checkbox"></span>
							</label>
						</div>
                       <div class="input-block <%=IIf(XAPreviousAddressEnable, "", "hidden")%>">						
						    <p>Minimum Duration (months)</p>
						    <input type="text" class="form-control number" id="txtXAPreviousAddressThreshold" value="<%=XAPreviousAddressThreshold%>" maxlength ="3" />							
					    </div>
                    </div>
						<%--<div class="input-block">
							<h3 class="property-title">Allows Minor Accounts</h3>
							<p>Allow your applicants to apply for a minor account. (Note: make sure you activate your minor account products, if enabled)</p>
							<label class="toggle-btn" data-on="ON" data-off="OFF">
								<input type="checkbox" value=""/>
								<span class="button-checkbox"></span>
							</label>
						</div>
						<div class="input-block">
							<h3 class="property-title">Allows Funding via Mail In Check</h3>
							<p>Allow your applicants to fund their account by sending a check to your institution.</p>
							<label class="toggle-btn" data-on="ON" data-off="OFF">
								<input type="checkbox" value=""/>
								<span class="button-checkbox"></span>
							</label>
						</div>--%>
					</div>
				</div>
			</div>
		</section>
	</div>
</asp:Content>
<asp:Content runat="server" ContentPlaceHolderID="plScripts">
	<script type="text/javascript" src="js/ckeditor/ckeditor.js"></script>
	<script type="text/javascript">
		CKEDITOR.disableAutoInline = true;
		$(function () {
            function addDocUploadComponent(id, title, subTitle, checkedStatus, uploadDocMsg) {
                if ($("#docUploadContainer").find("#" + id).length != 0) return;
                var item = $('<div name="docUpload-section" class="pt15" />').prop("id", id).append($('#docUploadTemplate').children().clone(true));
                $('p[name="docUpload-title"]', item).text(title);
                $('p[name="docUpload-sub-title"]', item).text(subTitle);
                $('p[name="docUpload-sub-title"]', item).text(subTitle);
                $('label[name="docUpload-label"]', item).addClass(checkedStatus);
                $('input[name="docUpload-input"]', item).prop("checked", checkedStatus == "checked");
                $('div[name="docUpload-msg"]', item).prop("id", id + "Msg").html(uploadDocMsg);
                $('#docUploadContainer').append(item);
            }
            addDocUploadComponent("personal_xa", "Personal XA", "Allow your applicants to capture/upload images or documents for the Personal XA application", "<%=BindCheckbox(XAUploadDocEnable)%>", "<%=XAUploadDocMsg%>");
            addDocUploadComponent("special_xa", "Special XA", "Allow your applicants to capture/upload images or documents for the Special XA application", "<%=BindCheckbox(XASpecialUploadDocEnable)%>", "<%=XASpecialUploadDocMsg%>");
            addDocUploadComponent("business_xa", "Business XA", "Allow your applicants to capture/upload images or documents for the Business XA application", "<%=BindCheckbox(XABusinessUploadDocEnable)%>", "<%=XABusinessUploadDocMsg%>");
            
			//$(".dynamic-key-panel").dynamicKey();
			$("#chkCreditPull").on("change", function () {
				var $self = $(this);
				if ($self.is(":checked")) {
					$self.closest(".toggle-btn").addClass("checked");
				} else {
					$self.closest(".toggle-btn").removeClass("checked");
				}
			});
			registerDataValidator();
			$("select,input:checkbox").on("change", function () {
				master.FACTORY.documentChanged(this);
			});
			$(".html-editor").ckEditor({
				onContentChanged: function (editor) { master.FACTORY.documentChanged(editor); },
				onCommandClicked: function (editor, commandBtn) { master.FACTORY.documentChanged(editor); }
			});
			$("input:text").each(function (idx, ele) {
				$(ele).on({
					keypress: function () { master.FACTORY.documentChanged(ele); },
					paste: function () { master.FACTORY.documentChanged(ele); },
					cut: function () { master.FACTORY.documentChanged(ele); }
				});
			});
             //show minimum address duration input field only when the chkPreviousAddress is on 
            $("#chkXAPreviousAddress").on("change", function () {
                var $self = $(this);
                var $previousAddressThreshold = $('#txtXAPreviousAddressThreshold');
                if ($self.is(":checked")) {
                    $previousAddressThreshold.closest('div').removeClass('hidden');
                } else {
                    //clear and hide the field
                    $previousAddressThreshold.val(""); 
                    $previousAddressThreshold.closest('div').addClass('hidden');
                }
            });
		});
        function toggleChange(event) {
            var $self = $(event.target);
            if ($self.is(":checked")) {
                $self.closest(".toggle-btn").addClass("checked");
            } else {
                $self.closest(".toggle-btn").removeClass("checked");
            }
        }
		(function (master, $, undefined) {
			master.FACTORY.SaveDraft = function (revisionId) {
				if ($.smValidate("ValidateData") == false) {
					_COMMON.noty("error", "Error: please check all fields.", 500);
					return;
				}

				var dataObj = collectSubmitData();
				dataObj.revisionId = revisionId;
				dataObj.command = 'savexasettings';
				$.ajax({
					url: "/sm/smhandler.aspx",
					async: true,
					cache: false,
					type: 'POST',
					dataType: 'html',
					data: dataObj,
					success: function (responseText) {
						var response = $.parseJSON(responseText);
						if (response.IsSuccess) {
							master.FACTORY.onSaveDraftSuccess();
						} else {
							_COMMON.noty("error", "Error", 500);
						}
					}
				});
			};
			master.FACTORY.Preview = function (revisionId) {
				if ($.smValidate("ValidateData") == false) {
					_COMMON.noty("error", "Error: please check all fields.", 500);
					return;
				}
				var dataObj = collectSubmitData();
				dataObj.revisionId = revisionId;
				dataObj.command = 'previewxasettings';
				$.ajax({
					url: "/sm/smhandler.aspx",
					async: true,
					cache: false,
					type: 'POST',
					dataType: 'html',
					data: dataObj,
					success: function (responseText) {
						var response = $.parseJSON(responseText);
						if (response.IsSuccess) {
							master.FACTORY.onPreviewSuccess();
							_COMMON.openInNewTab(response.Info.previewurl);
						} else {
							_COMMON.noty("error", "Error", 500);
						}
					}
				});
			};
			master.FACTORY.validateBeforePublish = function () {
				var result = $.smValidate("ValidateData");
				if (result == false) {
					_COMMON.noty("error", "Error: please check all fields.", 500);
				}
				return result;
			}
			master.FACTORY.Publish = function (comment, publishAll) {
				if ($.smValidate("ValidateData") == false) {
					_COMMON.noty("error", "Error: please check all fields.", 500);
					return;
				}
				var dataObj = collectSubmitData();
				dataObj.command = 'publishxasettings';
				dataObj.comment = comment;
				dataObj.publishAll = publishAll;
				$.ajax({
					url: "/sm/smhandler.aspx",
					async: true,
					cache: false,
					type: 'POST',
					dataType: 'html',
					data: dataObj,
					success: function (responseText) {
						var response = $.parseJSON(responseText);
						if (response.IsSuccess) {
							master.FACTORY.onPublishSuccess();
						} else {
							_COMMON.noty("error", "Error", 500);
						}
					}
				});
			};
		}(window.master = window.master || {}, jQuery));
		function registerDataValidator() {
			$.smValidate.removeValidationGroup("ValidateData");
			$("input.float-number").each(function (idx, input) {
				$(input).observer({
					validators: [
						function (partial) {
							var $self = $(this);
							if (/^[0-9]+(\.[0-9]+)*$/.test($self.val()) == false) {
								return "Invalid data";
							}
							return "";
						}
					],
					validateOnBlur: true,
					group: "ValidateData"
				});
			});
                 //validate minimum address duration
            $('#txtXAPreviousAddressThreshold').observer({
					validators: [
						function (partial) {
                            var $self = $(this);
                            if ($self.closest('div').hasClass('hidden')) return ""; //skip validating if the field is hidden
							if (/^[0-9]+$/.test($self.val()) == false || parseInt($self.val()) >= 120 || parseInt($self.val()) < 2) {
								return "Invalid minimum duration";
							}
							return "";
						}
					],
					validateOnBlur: true,
					group: "ValidateData"
			});
			$('#docUploadContainer div[name="docUpload-msg"]').each(function(){
                $(this).observer({
                    validators: [
                        function (partial) {
                            if(!$(this).is(":visible")) return "";
                            var $self = $(this);
							var content = CKEDITOR.instances[$self.attr("id")].getData();
							var decodedContent = $("<div />").html(content).text();
                            if ($('input[name="docUpload-input"]', $self.closest('div[name="docUpload-section"]')).is(":checked") && $.trim(decodedContent) === "") {
                                return "A non-blank message is required when the document upload feature is active";
                            }
                            return "";
                        }
                    ],
                    validateOnBlur: true,
                    group: "ValidateData"
                });
            });
		}

        function collectUploadDocData(data){
            data.xa_upload_doc_enable = $('#personal_xa input[name="docUpload-input"]').is(":checked");
            data.xa_upload_doc_message = CKEDITOR.instances["personal_xaMsg"].getData();
            data.xa_special_upload_doc_enable = $('#special_xa input[name="docUpload-input"]').is(":checked");
            data.xa_special_upload_doc_message = CKEDITOR.instances["special_xaMsg"].getData();
            data.xa_business_upload_doc_enable = $('#business_xa input[name="docUpload-input"]').is(":checked");
            data.xa_business_upload_doc_message = CKEDITOR.instances["business_xaMsg"].getData();
        }

		function collectSubmitData() {
			var result = {};
			result.lenderconfigid = '<%=LenderConfigID%>';
            collectUploadDocData(result);
			result.xa_joint_enable = $("#chkXaJointEnable").is(":checked");
			result.xa_collect_job_title_only = $("#chkXaCollectJobTitleOnly").is(":checked");
			result.sa_joint_enable = $("#chkSaJointEnable").is(":checked");
			result.xa_location_pool = $("#chkXaLocationPool").is(":checked");
			result.credit_pull = $("#chkCreditPull").is(":checked");
			result.decision_xa_enabled = $("#chkDecisionXaEnabled").is(":checked");
			result.ida = $("#ddlIDAuth").val();
			result.debit = $("#ddlDebitBureau").val();
			result.xa_status = $("#ddlXAStatus").val();
			result.xa_funding_enable = $("#chkXAFunding").is(":checked");
			result.xa_booking = $("#ddlXABooking").val();
			//result.xa_xsell = $("#chkXAXSell").is(":checked");
			result.xa_docusign = $("#chkXADocuSign").is(":checked");
			result.xa_docusign_group = $("#txtXADocuSignGroup").val();
            result.xa_funding_account_number_free_form_enable = $("#chkXAFundingAccountNumberFreeForm").is(":checked");
            result.xa_previous_address_threshold = $("#txtXAPreviousAddressThreshold").val();
            result.xa_previous_address_enable = $("#chkXAPreviousAddress").is(":checked");
			return result;
		}
	</script>
</asp:Content>
<asp:Content runat="server" ContentPlaceHolderID="plActionButtons"></asp:Content>