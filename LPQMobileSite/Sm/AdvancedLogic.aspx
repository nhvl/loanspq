﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="AdvancedLogic.aspx.vb" Inherits="Sm_AdvancedLogic" MasterPageFile="SiteManager.master" %>
<%@ Import Namespace="Newtonsoft.Json" %>

<asp:Content runat="server" ContentPlaceHolderID="plBody">
	<div>
		<h1 class="page-header">Business Rules (Advanced Conditions)</h1>
		<p>Build rules to control field visibility</p>
		<section class="advanced-logics">
			<ul class="nav nav-tabs" id="mainTabs" role="tablist">
				<%If (CheckEnabledModule(SmSettings.ModuleName.XA)) Then%>
				<li role="presentation" data-preview-type="xa"><a href="#xa" aria-controls="xa" role="tab" data-toggle="tab">XA</a></li>
				<%End If %>
				<%If (CheckEnabledModule(SmSettings.ModuleName.CC)) Then%>
				<li role="presentation" data-preview-type="cc"><a href="#cc" aria-controls="loans" role="tab" data-toggle="tab">CC</a></li>
				<%End If %>
				<%If (CheckEnabledModule(SmSettings.ModuleName.CC_COMBO)) Then%>
				<li role="presentation" data-preview-type="cccombo"><a href="#cc_combo" aria-controls="loans" role="tab" data-toggle="tab">CC Combo</a></li>
				<%End If %>
				
				<%If (CheckEnabledModule(SmSettings.ModuleName.HE)) Then%>
				<li role="presentation" data-preview-type="he"><a href="#he" aria-controls="loans" role="tab" data-toggle="tab">HE</a></li>
				<%End If %>
				<%If (CheckEnabledModule(SmSettings.ModuleName.HE_COMBO)) Then%>
				<li role="presentation" data-preview-type="hecombo"><a href="#he_combo" aria-controls="loans" role="tab" data-toggle="tab">HE Combo</a></li>
				<%End If %>
				<%If (CheckEnabledModule(SmSettings.ModuleName.PL)) Then%>
				<li role="presentation" data-preview-type="pl"><a href="#pl" aria-controls="loans" role="tab" data-toggle="tab">PL</a></li>
				<%End If %>
				<%If (CheckEnabledModule(SmSettings.ModuleName.PL_COMBO)) Then%>
				<li role="presentation" data-preview-type="plcombo"><a href="#pl_combo" aria-controls="loans" role="tab" data-toggle="tab">PL Combo</a></li>
				<%End If %>
				<%If (CheckEnabledModule(SmSettings.ModuleName.VL)) Then%>
				<li role="presentation" data-preview-type="vl"><a href="#vl" aria-controls="loans" role="tab" data-toggle="tab">VL</a></li>
				<%End If %>
				<%If (CheckEnabledModule(SmSettings.ModuleName.VL_COMBO)) Then%>
				<li role="presentation" data-preview-type="vlcombo"><a href="#vl_combo" aria-controls="loans" role="tab" data-toggle="tab">VL Combo</a></li>
				<%End If %>
				<%If (CheckEnabledModule(SmSettings.ModuleName.BL)) Then%>
				<li role="presentation" data-preview-type="bl"><a href="#bl" aria-controls="loans" role="tab" data-toggle="tab">BL</a></li>
				<%End If %>
			</ul>
			<div class="tab-content">
				<%If CheckEnabledModule(SmSettings.ModuleName.XA) Then%>
				<div role="tabpanel" class="tab-pane active" id="xa">
						<div class="text-right">
							<button type="button" data-command="add-item" class="btn btn-primary mb10"><i class="fa fa-plus" aria-hidden="true"></i>Add Rule</button>
						</div>
						<div class="no-data">There is no rule for this module. Click "Add Rule" button to create one.</div>
				</div>
				<%End If%>
				<%If CheckEnabledModule(SmSettings.ModuleName.PL) Then%>
				<div role="tabpanel" class="tab-pane fade" id="pl">
					<div class="text-right">
						<button type="button" data-command="add-item" class="btn btn-primary mb10"><i class="fa fa-plus" aria-hidden="true"></i>Add Rule</button>
					</div>
					<div class="no-data">There is no rule for this module. Click "Add Rule" button to create one.</div>
				</div>
				<%End If%>
				<%If CheckEnabledModule(SmSettings.ModuleName.PL_COMBO) Then%>
				<div role="tabpanel" class="tab-pane fade" id="pl_combo">
					<div class="text-right">
						<button type="button" data-command="add-item" class="btn btn-primary mb10"><i class="fa fa-plus" aria-hidden="true"></i>Add Rule</button>
					</div>
					<div class="no-data">There is no rule for this module. Click "Add Rule" button to create one.</div>
				</div>
				<%End If%>
				<%If CheckEnabledModule(SmSettings.ModuleName.VL) Then%>
				<div role="tabpanel" class="tab-pane fade" id="vl">
					<div class="text-right">
						<button type="button" data-command="add-item" class="btn btn-primary mb10"><i class="fa fa-plus" aria-hidden="true"></i>Add Rule</button>
					</div>
					<div class="no-data">There is no rule for this module. Click "Add Rule" button to create one.</div>
				</div>
				<%End If%>
				<%If CheckEnabledModule(SmSettings.ModuleName.VL_COMBO) Then%>
				<div role="tabpanel" class="tab-pane fade" id="vl_combo">
					<div class="text-right">
						<button type="button" data-command="add-item" class="btn btn-primary mb10"><i class="fa fa-plus" aria-hidden="true"></i>Add Rule</button>
					</div>
					<div class="no-data">There is no rule for this module. Click "Add Rule" button to create one.</div>
				</div>
				<%End If%>
				<%If CheckEnabledModule(SmSettings.ModuleName.CC) Then%>
				<div role="tabpanel" class="tab-pane" id="cc">
					<div class="text-right">
						<button type="button" data-command="add-item" class="btn btn-primary mb10"><i class="fa fa-plus" aria-hidden="true"></i>Add Rule</button>
					</div>
					<div class="no-data">There is no rule for this module. Click "Add Rule" button to create one.</div>
				</div>
				<%End If%>
				<%If CheckEnabledModule(SmSettings.ModuleName.CC_COMBO) Then%>
				<div role="tabpanel" class="tab-pane" id="cc_combo">
					<div class="text-right">
						<button type="button" data-command="add-item" class="btn btn-primary mb10"><i class="fa fa-plus" aria-hidden="true"></i>Add Rule</button>
					</div>
					<div class="no-data">There is no rule for this module. Click "Add Rule" button to create one.</div>
				</div>
				<%End If%>
				<%If CheckEnabledModule(SmSettings.ModuleName.HE) Then%>
				<div role="tabpanel" class="tab-pane" id="he">
					<div class="text-right">
						<button type="button" data-command="add-item" class="btn btn-primary mb10"><i class="fa fa-plus" aria-hidden="true"></i>Add Rule</button>
					</div>
					<div class="no-data">There is no rule for this module. Click "Add Rule" button to create one.</div>
				</div>
				<%End If%>
				<%If CheckEnabledModule(SmSettings.ModuleName.HE_COMBO) Then%>
				<div role="tabpanel" class="tab-pane" id="he_combo">
					<div class="text-right">
						<button type="button" data-command="add-item" class="btn btn-primary mb10"><i class="fa fa-plus" aria-hidden="true"></i>Add Rule</button>
					</div>
					<div class="no-data">There is no rule for this module. Click "Add Rule" button to create one.</div>
				</div>
				<%End If%>
				<%If CheckEnabledModule(SmSettings.ModuleName.BL) Then%>
				<div role="tabpanel" class="tab-pane" id="bl">
					<div class="text-right">
						<button type="button" data-command="add-item" class="btn btn-primary mb10"><i class="fa fa-plus" aria-hidden="true"></i>Add Rule</button>
					</div>
					<div class="no-data">There is no rule for this module. Click "Add Rule" button to create one.</div>
				</div>
				<%End If%>
			</div>
		</section>
	</div>
</asp:Content>
<asp:Content runat="server" ContentPlaceHolderID="plScripts">
	<script type="text/javascript" src="js/ckeditor/ckeditor.js"></script>
	<script type="text/javascript">
		var CustomQuestionList = {};
		CustomQuestionList["XA"] = <%=JsonConvert.SerializeObject(CustomQuestions.Where(Function(p) p.LoanType.ToUpper() = "XA").Select(Function(q) New With {Key .Name = q.Name, .Text = q.Text}).Distinct())%>;
		CustomQuestionList["CC"] = <%=JsonConvert.SerializeObject(CustomQuestions.Where(Function(p) p.LoanType.ToUpper() = "CC").Select(Function(q) New With {Key .Name = q.Name, .Text = q.Text}).Distinct())%>;
		CustomQuestionList["HE"] = <%=JsonConvert.SerializeObject(CustomQuestions.Where(Function(p) p.LoanType.ToUpper() = "HE").Select(Function(q) New With {Key .Name = q.Name, .Text = q.Text}).Distinct())%>;
		CustomQuestionList["PL"] = <%=JsonConvert.SerializeObject(CustomQuestions.Where(Function(p) p.LoanType.ToUpper() = "PL").Select(Function(q) New With {Key .Name = q.Name, .Text = q.Text}).Distinct())%>;
		CustomQuestionList["VL"] = <%=JsonConvert.SerializeObject(CustomQuestions.Where(Function(p) p.LoanType.ToUpper() = "VL").Select(Function(q) New With {Key .Name = q.Name, .Text = q.Text}).Distinct())%>;
		CustomQuestionList["BL"] = <%=JsonConvert.SerializeObject(CustomQuestions.Where(Function(p) p.LoanType.ToUpper() = "BL").Select(Function(q) New With {Key .Name = q.Name, .Text = q.Text}).Distinct())%>;
		CustomQuestionList["CC_COMBO"] = <%=JsonConvert.SerializeObject(CustomQuestions.Where(Function(p) p.LoanType.ToUpper() = "CC" Or p.LoanType.ToUpper() = "XA").Select(Function(q) New With {Key .Name = q.Name, .Text = q.Text}).Distinct())%>;
		CustomQuestionList["HE_COMBO"] = <%=JsonConvert.SerializeObject(CustomQuestions.Where(Function(p) p.LoanType.ToUpper() = "HE" Or p.LoanType.ToUpper() = "XA").Select(Function(q) New With {Key .Name = q.Name, .Text = q.Text}).Distinct())%>;
		CustomQuestionList["PL_COMBO"] = <%=JsonConvert.SerializeObject(CustomQuestions.Where(Function(p) p.LoanType.ToUpper() = "PL" Or p.LoanType.ToUpper() = "XA").Select(Function(q) New With {Key .Name = q.Name, .Text = q.Text}).Distinct())%>;
		CustomQuestionList["VL_COMBO"] = <%=JsonConvert.SerializeObject(CustomQuestions.Where(Function(p) p.LoanType.ToUpper() = "VL" Or p.LoanType.ToUpper() = "XA").Select(Function(q) New With {Key .Name = q.Name, .Text = q.Text}).Distinct())%>;


		var ApplicantQuestionList = {};
		ApplicantQuestionList["XA"] = <%=JsonConvert.SerializeObject(ApplicantQuestions.Where(Function(p) p.LoanType.ToUpper() = "XA").Select(Function(q) New With {Key .Name = q.Name, .Text = q.Text}).Distinct())%>;
		ApplicantQuestionList["CC"] = <%=JsonConvert.SerializeObject(ApplicantQuestions.Where(Function(p) p.LoanType.ToUpper() = "CC").Select(Function(q) New With {Key .Name = q.Name, .Text = q.Text}).Distinct())%>;
		ApplicantQuestionList["HE"] = <%=JsonConvert.SerializeObject(ApplicantQuestions.Where(Function(p) p.LoanType.ToUpper() = "HE").Select(Function(q) New With {Key .Name = q.Name, .Text = q.Text}).Distinct())%>;
		ApplicantQuestionList["PL"] = <%=JsonConvert.SerializeObject(ApplicantQuestions.Where(Function(p) p.LoanType.ToUpper() = "PL").Select(Function(q) New With {Key .Name = q.Name, .Text = q.Text}).Distinct())%>;
		ApplicantQuestionList["VL"] = <%=JsonConvert.SerializeObject(ApplicantQuestions.Where(Function(p) p.LoanType.ToUpper() = "VL").Select(Function(q) New With {Key .Name = q.Name, .Text = q.Text}).Distinct())%>;
		ApplicantQuestionList["BL"] = <%=JsonConvert.SerializeObject(ApplicantQuestions.Where(Function(p) p.LoanType.ToUpper() = "BL").Select(Function(q) New With {Key .Name = q.Name, .Text = q.Text}).Distinct())%>;
		ApplicantQuestionList["CC_COMBO"] = <%=JsonConvert.SerializeObject(ApplicantQuestions.Where(Function(p) p.LoanType.ToUpper() = "CC_COMBO").Select(Function(q) New With {Key .Name = q.Name, .Text = q.Text}).Distinct())%>;
		ApplicantQuestionList["HE_COMBO"] = <%=JsonConvert.SerializeObject(ApplicantQuestions.Where(Function(p) p.LoanType.ToUpper() = "HE_COMBO").Select(Function(q) New With {Key .Name = q.Name, .Text = q.Text}).Distinct())%>;
		ApplicantQuestionList["PL_COMBO"] = <%=JsonConvert.SerializeObject(ApplicantQuestions.Where(Function(p) p.LoanType.ToUpper() = "PL_COMBO").Select(Function(q) New With {Key .Name = q.Name, .Text = q.Text}).Distinct())%>;
		ApplicantQuestionList["VL_COMBO"] = <%=JsonConvert.SerializeObject(ApplicantQuestions.Where(Function(p) p.LoanType.ToUpper() = "VL_COMBO").Select(Function(q) New With {Key .Name = q.Name, .Text = q.Text}).Distinct())%>;

		var LoanPurposeList = {};
		LoanPurposeList["CC"] = <%=JsonConvert.SerializeObject(CCLoanPurposes)%>;
		LoanPurposeList["HE"] = <%=JsonConvert.SerializeObject(HELoanPurposes)%>;
		LoanPurposeList["PL"] = <%=JsonConvert.SerializeObject(PLLoanPurposes)%>;
		LoanPurposeList["VL"] = <%=JsonConvert.SerializeObject(VLLoanPurposes)%>;
		LoanPurposeList["BL"] = <%=JsonConvert.SerializeObject(BLLoanPurposes)%>;

		var ProductQuestionList = {};
		ProductQuestionList["XA"] = <%=JsonConvert.SerializeObject(ProductQuestions.Where(Function(p) p.LoanType.ToUpper() = "XA").Select(Function(q) New With {Key .Name = q.Name, .Text = q.Text}))%>;
		ProductQuestionList["COMBO"] = <%=JsonConvert.SerializeObject(ProductQuestions.Where(Function(p) p.LoanType.ToUpper() = "COMBO").Select(Function(q) New With {Key .Name = q.Name, .Text = q.Text}))%>;

		var LoanTypeList = {};
		LoanTypeList["CC"] = <%=JsonConvert.SerializeObject(CCTypes)%>;
		LoanTypeList["VL"] = <%=JsonConvert.SerializeObject(VLTypes)%>;

		$(function () {
			var url = document.location.toString();
			if (url.match('#')) {
				var $defaultTab = $('.nav-tabs li:not(".hidden") a[href="#' + url.split('#')[1] + '"]');
				if ($defaultTab.length == 0) {
					$defaultTab = $('.nav-tabs li:not(".hidden")').first().find("a[href]");
				}
				$defaultTab.tab('show');
			} else {
				$('.nav-tabs li:not(".hidden")').first().find("a[href]").tab('show');
			}
			$('.nav-tabs a').on('shown.bs.tab', function (e) {
				window.location.hash = e.target.hash;
				window.scrollTo(0, 0);
			});
			var xaItems = <%=JsonConvert.SerializeObject(AdvancedLogicItems.Where(Function(p) p.Key.StartsWith("XA|")).Select(Function(q) q.Value))%>;
			initAdvancedLogicItems("#xa", xaItems, "xa");
			var plItems = <%=JsonConvert.SerializeObject(AdvancedLogicItems.Where(Function(p) p.Key.StartsWith("PL|")).Select(Function(q) q.Value))%>;
			initAdvancedLogicItems("#pl", plItems, "pl");
			var plComboItems = <%=JsonConvert.SerializeObject(AdvancedLogicItems.Where(Function(p) p.Key.StartsWith("PL_COMBO|")).Select(Function(q) q.Value))%>;
			initAdvancedLogicItems("#pl_combo", plComboItems, "pl_combo");
			var vlItems = <%=JsonConvert.SerializeObject(AdvancedLogicItems.Where(Function(p) p.Key.StartsWith("VL|")).Select(Function(q) q.Value))%>;
			initAdvancedLogicItems("#vl", vlItems, "vl");
			var vlComboItems = <%=JsonConvert.SerializeObject(AdvancedLogicItems.Where(Function(p) p.Key.StartsWith("VL_COMBO|")).Select(Function(q) q.Value))%>;
			initAdvancedLogicItems("#vl_combo", vlComboItems, "vl_combo");
			var heItems = <%=JsonConvert.SerializeObject(AdvancedLogicItems.Where(Function(p) p.Key.StartsWith("HE|")).Select(Function(q) q.Value))%>;
			initAdvancedLogicItems("#he", heItems, "he");
			var heComboItems = <%=JsonConvert.SerializeObject(AdvancedLogicItems.Where(Function(p) p.Key.StartsWith("HE_COMBO|")).Select(Function(q) q.Value))%>;
			initAdvancedLogicItems("#he_combo", heComboItems, "he_combo");
			var ccItems = <%=JsonConvert.SerializeObject(AdvancedLogicItems.Where(Function(p) p.Key.StartsWith("CC|")).Select(Function(q) q.Value))%>;
			initAdvancedLogicItems("#cc", ccItems, "cc");
			var blItems = <%=JsonConvert.SerializeObject(AdvancedLogicItems.Where(Function(p) p.Key.StartsWith("BL|")).Select(Function(q) q.Value))%>;
			initAdvancedLogicItems("#bl", blItems, "bl");
			var ccComboItems = <%=JsonConvert.SerializeObject(AdvancedLogicItems.Where(Function(p) p.Key.StartsWith("CC_COMBO|")).Select(Function(q) q.Value))%>;
			initAdvancedLogicItems("#cc_combo", ccComboItems, "cc_combo");
			$("button.btn[data-command='add-item']", $("div.tab-content")).each(function(idx, btn) {
				var $btn = $(btn);
				var loanType = $btn.closest("div.tab-pane").attr("id");
				$btn.on("click", function() {
					master.FACTORY.showAddNewLogicPopup(loanType);
				});
			});
		});
		(function (master, $, undefined) {
			master.FACTORY.SaveDraft = function (revisionId) {
				var dataObj = collectSubmitData();
				dataObj.command = "saveAdvancedLogics";
				dataObj.revisionId = revisionId;
				$.ajax({
					url: "/sm/smhandler.aspx",
					async: true,
					cache: false,
					type: 'POST',
					dataType: 'html',
					data: dataObj,
					success: function (responseText) {
						var response = $.parseJSON(responseText);
						if (response.IsSuccess) {
							master.FACTORY.onSaveDraftSuccess();
						} else {
							_COMMON.noty("error", "Error", 500);
						}
					}
				});
			};
			master.FACTORY.Preview = function (revisionId) {
				var dataObj = collectSubmitData();
				dataObj.command = "previewAdvancedLogics";
				dataObj.revisionId = revisionId;
				$.ajax({
					url: "/sm/smhandler.aspx",
					async: true,
					cache: false,
					type: 'POST',
					dataType: 'html',
					data: dataObj,
					success: function (responseText) {
						var response = $.parseJSON(responseText);
						if (response.IsSuccess) {
							master.FACTORY.onPreviewSuccess();
							_COMMON.openInNewTab(response.Info.previewurl);
						} else {
							_COMMON.noty("error", "Error", 500);
						}
					}
				});
			};
			master.FACTORY.Publish = function (comment, publishAll) {
				var dataObj = collectSubmitData();
				dataObj.command = "publishAdvancedLogics";
				dataObj.comment = comment; 
				dataObj.publishAll = publishAll;
				$.ajax({
					url: "/sm/smhandler.aspx",
					async: true,
					cache: false,
					type: 'POST',
					dataType: 'html',
					data: dataObj,
					success: function (responseText) {
						var response = $.parseJSON(responseText);
						if (response.IsSuccess) {
							master.FACTORY.onPublishSuccess();
						} else {
							_COMMON.noty("error", "Error", 500);
						}
					}
				});
			};
			function hideAllFields(container){
				$("select[data-field='ddlCqName']", $(container)).closest("div.col-12").toggleClass("hidden", true);
				$("select[data-field='ddlAqName']", $(container)).closest("div.col-12").toggleClass("hidden", true);
				$("select[data-field='ddlPqName']", $(container)).closest("div.col-12").toggleClass("hidden", true);
				$("select[data-field='ddlPurposeValue']", $(container)).closest("div.col-12").toggleClass("hidden", true);
				$("select[data-field='ddlLoanTypeValue']", $(container)).closest("div.col-12").toggleClass("hidden", true);
				$("select[data-field='ddlLineofCreditValue']", $(container)).closest("div.col-12").toggleClass("hidden", true);
				$("select[data-field='ddlVehicleTypeValue']", $(container)).closest("div.col-12").toggleClass("hidden", true);
				$("select[data-field='ddlComparer']", $(container)).closest("div.col-12").toggleClass("hidden", true);
				$("select[data-field='ddlComparer'] option", $(container)).toggleClass("hidden", false);
				$("input[data-field='txtValue']", $(container)).closest("div.col-12").toggleClass("hidden", true);
			}
            function getCommonHtml(loanType){
                return [
                    $("<div />",{class:"col-12"}).append(
                        $("<p />",{text: "will be visible if"}))
                    , $("<div />",{class:"col-12"}).append(
                        $("<div />", {class: "form-group"}).append(
                            $("<select />",{class: "form-control", "data-field": "ddlTriggerType", "data-message-require": "Please select", "data-required": "true", id: "ddlTriggerType"}).append(buildTriggerType(loanType))))
                    , $("<div />",{class:"col-12 hidden"}).append(
                        $("<div />", {class: "form-group"}).append(
                            $("<select />",{class: "form-control", "data-field": "ddlCqName", "data-message-require": "Please select", "data-required": "true", id: "ddlCqName"}).append(buildCustomQuestionOptionsList(loanType))))
                    , $("<div />",{class:"col-12 hidden"}).append(
                        $("<div />", {class: "form-group"}).append(
                            $("<select />",{class: "form-control", "data-field": "ddlAqName", "data-message-require": "Please select", "data-required": "true", id: "ddlAqName"}).append(buildApplicantQuestionOptionsList(loanType))))
                    , $("<div />",{class:"col-12 hidden"}).append(
                        $("<div />", {class: "form-group"}).append(
                            $("<select />",{class: "form-control", "data-field": "ddlPqName", "data-message-require": "Please select", "data-required": "true", id: "ddlPqName"}).append(buildProductQuestionOptionsList(loanType))))
                    , $("<div />",{class:"col-12 hidden"}).append(
                        $("<div />", {class: "form-group"}).append(
                            $("<select />",{class: "form-control", "data-field": "ddlComparer", style: "display:inline-block; width:300px;"}).append(buildComparerOptionList())))
                    , loanType.toUpperCase() == "XA" ? "" : $("<div />",{class:"col-12 hidden"}).append(
                        $("<div />", {class: "form-group"}).append(
                            $("<select />",{class: "form-control", "data-field": "ddlPurposeValue"}).append(buildLoanPurposeOptionsList(loanType))))
                    , loanType.toUpperCase() == "XA" ? "" : $("<div />",{class:"col-12 hidden"}).append(
                        $("<div />", {class: "form-group"}).append(
                            $("<select />",{class: "form-control", "data-field": "ddlLoanTypeValue"}).append(buildLoanTypeOptionsList(loanType))))
                    , $("<div />",{class:"col-12 hidden"}).append(
                        $("<div />", {class: "form-group"}).append(
                            $("<select />",{class: "form-control", "data-field": "ddlLineofCreditValue", "data-message-require": "Please select", "data-required": "true", id: "ddlLineofCreditValue"}).append(buildLineofCreditOptionsList())))
                    , $("<div />",{class:"col-12 hidden"}).append(
                        $("<div />", {class: "form-group"}).append(
                            $("<select />",{class: "form-control", "data-field": "ddlVehicleTypeValue", "data-message-require": "Please select", "data-required": "true", id: "ddlVehicleTypeValue"}).append(buildVehicleTypeOptionsList(loanType))))
                    , $("<div />",{class:"col-12 hidden"}).append(
                        $("<div />", {class: "form-group"}).append(
                            $("<input />",{class: "form-control", "data-field": "txtValue",  type: "text"})))
                    ];
            }
            function getHtmlContent(isConditionPopup, loanType, targetItem, targetText, targetType){
                var htmlElem = 
                $("<div />", {class : "row"}).append(
                    $("<div />", {class : "col-12"}).append(
                        $("<div />", {class: "form-group"}).append(
                            isConditionPopup
                            ? [$("<label />",{class: "required-field", text: "Target Item"})
                                , $("<div />", {class: "custom-question-title"}).append(
                                    [$("<p />", {class: "target-item", "data-target-type": targetType, text: _COMMON.HtmlEncode(targetItem)}),
                                    $("<p />", {text:_COMMON.HtmlEncode(targetText)})])
                                ]
                            : [$("<label />",{class: "required-field", text: "Rule for"})
                                , $("<select />",{class: "form-control", "data-field": "ddlTargetItem", "data-message-require": "Please select target item", "data-required": "true", id: "ddlTargetItem"}).append(buildTargetItemOptionsList(loanType))]
                            )
                        )
                    );
                if(isConditionPopup){
                    htmlElem.append(getCommonHtml(loanType));
                }
                else{
                    htmlElem.append($("<div />", {class : "condition-panel hidden"}).append(getCommonHtml(loanType)));
                }
                return htmlElem.prop('outerHTML');
            }
			master.FACTORY.showAddNewLogicPopup = function(loanType) {
				var htmlContent = getHtmlContent(false, loanType);
				htmlContent = '<div class="dialog-content">' + htmlContent + '</div>';
				_COMMON.showDialogSaveClose("Add Rule", 0, "addlogicitem_dialog_0", htmlContent, "", null, function(dlg) {
					//data = {TargetText: "", TargetItem: "", TargetType: "", Conditions: [{Text: "", Name: "", Comparer: "", Value: "", Type: "", LogicalOperator: ""}]}
					if ($.smValidate("addlogicitem_dialog_0") == false) return;
					var data = {};
					var $container = dlg.container;
					var $ddlTargetItem = $("select[data-field='ddlTargetItem']",$container);
					data.TargetText = $ddlTargetItem.find("option:selected").text();
					data.TargetItem = $ddlTargetItem.find("option:selected").val();
					data.TargetType = $ddlTargetItem.find("option:selected").data("target-type");
					data.Conditions = [];
					var conditionItem = {};
					conditionItem.LogicalOperator = "";
					conditionItem.LogicalIndent = 0;
					if ($("select[data-field='ddlTriggerType']", $container).length == 0) {
						conditionItem.Type = "CQ";
					} else {
						conditionItem.Type = $("select[data-field='ddlTriggerType']", $container).val();
					}
					if (conditionItem.Type == "CQ") {
						var $ddlCqName = $("select[data-field='ddlCqName']",$container);
						conditionItem.Name = $ddlCqName.find("option:selected").val();
						conditionItem.Text = $ddlCqName.find("option:selected").text();
						conditionItem.Value = $("input[data-field='txtValue']",$container).val();
					}else if (conditionItem.Type == "AQ") {
						var $ddlAqName = $("select[data-field='ddlAqName']",$container);
						conditionItem.Name = $ddlAqName.find("option:selected").val();
						conditionItem.Text = $ddlAqName.find("option:selected").text();
						conditionItem.Value = $("input[data-field='txtValue']",$container).val();
					}else if (conditionItem.Type == "PQ") {
						var $ddlPqName = $("select[data-field='ddlPqName']",$container);
						conditionItem.Name = $ddlPqName.find("option:selected").val();
						conditionItem.Text = $ddlPqName.find("option:selected").text();
						conditionItem.Value = $("input[data-field='txtValue']",$container).val();
					} else if (conditionItem.Type == "PP") {
						conditionItem.Value = $("select[data-field='ddlPurposeValue']", $container).val();
					} else if (conditionItem.Type == "LC") {
						conditionItem.Value = $("select[data-field='ddlLineofCreditValue']", $container).val();
					} else if (conditionItem.Type == "VT") {
						conditionItem.Value = $("select[data-field='ddlVehicleTypeValue']", $container).val();
					} else {
						conditionItem.Value = $("select[data-field='ddlLoanTypeValue']", $container).val();
					}
					conditionItem.Comparer = $("select[data-field='ddlComparer']",$container).val();
					data.Conditions.push(conditionItem);
					buildAdvancedLogicItem("#"+loanType.toLowerCase(), data, loanType);
					dlg.dialog.close();
				}, function(container) {
					//onready
					$("select[data-field='ddlTargetItem']", $(container)).on("change", function() {
						var $self = $(this);
						if ($self.val() == "") {
							$("div.condition-panel", $(container)).addClass("hidden");
						} else {
							$("div.condition-panel", $(container)).removeClass("hidden");
						}
						var $ddlTriggerType = $("select[data-field='ddlTriggerType']", $(container));
						if ($self.find("option:selected").data("target-type") == "PQ") {
							$ddlTriggerType.find("option").addClass("hidden");
							$ddlTriggerType.find("option[value='']").removeClass("hidden");
							$ddlTriggerType.find("option[value='PQ']").removeClass("hidden");
						} else {
							$ddlTriggerType.find("option").removeClass("hidden");
							$ddlTriggerType.find("option[value='PQ']").addClass("hidden");
						}
					});
					
					$("select[data-field='ddlTriggerType']", $(container)).on("change", function() {
						var $self = $(this);
						hideAllFields(container);
						if ($self.val() == "CQ") {
							$("select[data-field='ddlCqName']", $(container)).closest("div.col-12").removeClass("hidden");
						} else if ($self.val() == "AQ") {
							$("select[data-field='ddlAqName']", $(container)).closest("div.col-12").removeClass("hidden");
						} else if ($self.val() == "PQ") {
							$("select[data-field='ddlPqName']", $(container)).closest("div.col-12").removeClass("hidden");
						} else if ($self.val() == "PP") {
							$("select[data-field='ddlPurposeValue']", $(container)).closest("div.col-12").removeClass("hidden");
							$("select[data-field='ddlCqName']", $(container)).val("");
							$("select[data-field='ddlComparer']", $(container)).closest("div.col-12").removeClass("hidden");
						} else if ($self.val() == "VT") {
							$("select[data-field='ddlVehicleTypeValue']", $(container)).closest("div.col-12").removeClass("hidden");
							$("select[data-field='ddlCqName']", $(container)).val("");
							$("select[data-field='ddlComparer']", $(container)).closest("div.col-12").removeClass("hidden");
							$("select[data-field='ddlComparer'] option[data-type='nonBooleanValue']", $(container)).toggleClass("hidden", true);
						} else if ($self.val() == "LT") {
							$("select[data-field='ddlLoanTypeValue']", $(container)).closest("div.col-12").removeClass("hidden");
							$("select[data-field='ddlCqName']", $(container)).val("");
							$("select[data-field='ddlAqName']", $(container)).val("");
							$("select[data-field='ddlPqName']", $(container)).val("");
							$("select[data-field='ddlComparer']", $(container)).closest("div.col-12").removeClass("hidden");
						} else if ($self.val() == "LC") {
							$("select[data-field='ddlLineofCreditValue']", $(container)).closest("div.col-12").removeClass("hidden");
							$("select[data-field='ddlCqName']", $(container)).val("");
							$("select[data-field='ddlComparer']", $(container)).closest("div.col-12").removeClass("hidden");
							$("select[data-field='ddlComparer'] option[data-type='nonBooleanValue']", $(container)).toggleClass("hidden", true);
						} else {
							$("select[data-field='ddlCqName']", $(container)).val("");
							$("select[data-field='ddlAqName']", $(container)).val("");
							$("select[data-field='ddlPqName']", $(container)).val("");
						}
					});
					
					$("select[data-field='ddlCqName'],select[data-field='ddlAqName'],select[data-field='ddlPqName']", $(container)).on("change", function() {
						var $self = $(this);
						if ($self.val() == "") {
							$("select[data-field='ddlComparer']", $(container)).closest("div.col-12").addClass("hidden");
							$("input[data-field='txtValue']", $(container)).closest("div.col-12").addClass("hidden");
						} else {
							$("select[data-field='ddlComparer']", $(container)).closest("div.col-12").removeClass("hidden");
							$("input[data-field='txtValue']", $(container)).closest("div.col-12").removeClass("hidden");
						}
						
					});
					registerDataValidator($("#addlogicitem_dialog_0"), "addlogicitem_dialog_0");
				}, function() {
					//onclose
					$.smValidate.cleanGroup("addlogicitem_dialog_0");
				});
			};
			master.FACTORY.showAddNewConditionPopup = function(loanType, logicItemContainer, targetItem, targetText, targetType) {
                var htmlContent = getHtmlContent(true, loanType, targetItem, targetText, targetType);
				htmlContent = '<div class="dialog-content">' + htmlContent + '</div>';
				_COMMON.showDialogSaveClose("Add Condition", 0, "addconditionitem_dialog_0", htmlContent, "", null, function(dlg) {
					//data = {TargetText: "", TargetItem: "", Conditions: [{Text: "", Name: "", Comparer: "", Value: "", Type: "", LogicalOperator: ""}]}
					if ($.smValidate("addconditionitem_dialog_0") == false) return;
					var $container = dlg.container;
					var conditionItem = {};
					conditionItem.LogicalOperator = "AND";
					conditionItem.LogicalIndent = 0;
					if ($("select[data-field='ddlTriggerType']", $container).length == 0) {
						conditionItem.Type = "CQ";
					} else {
						conditionItem.Type = $("select[data-field='ddlTriggerType']", $container).val();
					}
					if (conditionItem.Type == "CQ") {
						var $ddlCqName = $("select[data-field='ddlCqName']",$container);
						conditionItem.Name = $ddlCqName.find("option:selected").val();
						conditionItem.Text = $ddlCqName.find("option:selected").text();
						conditionItem.Value = $("input[data-field='txtValue']",$container).val();
					} else if (conditionItem.Type == "AQ") {
						var $ddlAqName = $("select[data-field='ddlAqName']",$container);
						conditionItem.Name = $ddlAqName.find("option:selected").val();
						conditionItem.Text = $ddlAqName.find("option:selected").text();
						conditionItem.Value = $("input[data-field='txtValue']",$container).val();
					} else if (conditionItem.Type == "PQ") {
						var $ddlPqName = $("select[data-field='ddlPqName']",$container);
						conditionItem.Name = $ddlPqName.find("option:selected").val();
						conditionItem.Text = $ddlPqName.find("option:selected").text();
						conditionItem.Value = $("input[data-field='txtValue']",$container).val();
					} else if (conditionItem.Type == "PP") {
						conditionItem.Value = $("select[data-field='ddlPurposeValue']", $container).val();
					} else if (conditionItem.Type == "VT") {
						conditionItem.Value = $("select[data-field='ddlVehicleTypeValue']", $container).val();
					} else if (conditionItem.Type == "LC") {
						conditionItem.Value = $("select[data-field='ddlLineofCreditValue']", $container).val();
					} else {
						conditionItem.Value = $("select[data-field='ddlLoanTypeValue']", $container).val();
					}

					conditionItem.Comparer = $("select[data-field='ddlComparer']",$container).val();
					addConditionItem(conditionItem, logicItemContainer, loanType, 0);
					translateToExpression($(logicItemContainer));
					dlg.dialog.close();
				}, function(container) {
					//onready
					var $ddlTriggerType = $("select[data-field='ddlTriggerType']", $(container));
					if (targetType == "PQ") {
						$ddlTriggerType.find("option").addClass("hidden");
						$ddlTriggerType.find("option[value='PQ']").removeClass("hidden");
					} else {
						$ddlTriggerType.find("option").removeClass("hidden");
						$ddlTriggerType.find("option[value='PQ']").addClass("hidden");
					}
					$("select[data-field='ddlTriggerType']", $(container)).on("change", function() {
						var $self = $(this);
						hideAllFields(container);
						if ($self.val() == "CQ") {
							$("select[data-field='ddlCqName']", $(container)).closest("div.col-12").removeClass("hidden");
						}else if ($self.val() == "AQ") {
							$("select[data-field='ddlAqName']", $(container)).closest("div.col-12").removeClass("hidden");
						}else if ($self.val() == "PQ") {
							$("select[data-field='ddlPqName']", $(container)).closest("div.col-12").removeClass("hidden");
						}else if ($self.val() == "PP") {
							$("select[data-field='ddlPurposeValue']", $(container)).closest("div.col-12").removeClass("hidden");
							$("select[data-field='ddlCqName']", $(container)).val("");
							$("select[data-field='ddlAqName']", $(container)).val("");
							$("select[data-field='ddlPqName']", $(container)).val("");
							$("select[data-field='ddlComparer']", $(container)).closest("div.col-12").removeClass("hidden");
						} else if ($self.val() == "VT") {
							$("select[data-field='ddlVehicleTypeValue']", $(container)).closest("div.col-12").removeClass("hidden");
							$("select[data-field='ddlCqName']", $(container)).val("");
							$("select[data-field='ddlAqName']", $(container)).val("");
							$("select[data-field='ddlPqName']", $(container)).val("");
							$("select[data-field='ddlComparer']", $(container)).closest("div.col-12").removeClass("hidden");
							$("select[data-field='ddlComparer'] option[data-type='nonBooleanValue']", $(container)).toggleClass("hidden", true);
						} else if ($self.val() == "LT") {
							$("select[data-field='ddlLoanTypeValue']", $(container)).closest("div.col-12").removeClass("hidden");
							$("select[data-field='ddlCqName']", $(container)).val("");
							$("select[data-field='ddlAqName']", $(container)).val("");
							$("select[data-field='ddlPqName']", $(container)).val("");
							$("select[data-field='ddlComparer']", $(container)).closest("div.col-12").removeClass("hidden");
						}else if ($self.val() == "LC") {
							$("select[data-field='ddlLineofCreditValue']", $(container)).closest("div.col-12").removeClass("hidden");
							$("select[data-field='ddlCqName']", $(container)).val("");
							$("select[data-field='ddlAqName']", $(container)).val("");
							$("select[data-field='ddlPqName']", $(container)).val("");
							$("select[data-field='ddlComparer']", $(container)).closest("div.col-12").removeClass("hidden");
							$("select[data-field='ddlComparer'] option[data-type='nonBooleanValue']", $(container)).toggleClass("hidden", true);
						} else {
							$("select[data-field='ddlCqName']", $(container)).val("");
							$("select[data-field='ddlAqName']", $(container)).val("");
							$("select[data-field='ddlPqName']", $(container)).val("");
						}
					});

					
					$("select[data-field='ddlCqName'],select[data-field='ddlAqName'],select[data-field='ddlPqName']", $(container)).on("change", function() {
						var $self = $(this);
						if ($self.val() == "") {
							$("select[data-field='ddlComparer']", $(container)).closest("div.col-12").addClass("hidden");
							$("input[data-field='txtValue']", $(container)).closest("div.col-12").addClass("hidden");
						} else {
							$("select[data-field='ddlComparer']", $(container)).closest("div.col-12").removeClass("hidden");
							$("input[data-field='txtValue']", $(container)).closest("div.col-12").removeClass("hidden");
						}
						
					});
					registerDataValidator($("#addconditionitem_dialog_0"), "addconditionitem_dialog_0");
				}, function() {
					//onclose
					$.smValidate.cleanGroup("addconditionitem_dialog_0");
				});
			};
			master.FACTORY.showEditConditionPopup = function(loanType, parentElement, targetItem, targetText, data, targetType) {
                var htmlContent = getHtmlContent(true, loanType, targetItem, targetText, targetType);
				htmlContent = '<div class="dialog-content">' + htmlContent + '</div>';
				_COMMON.showDialogSaveClose("Edit condition", 0, "editconditionitem_dialog_0", htmlContent, "", null, function(dlg) {
					//data = {TargetText: "", TargetItem: "", Conditions: [{Text: "", Name: "", Comparer: "", Value: "", Type: "", LogicalOperator: ""}]}
					if ($.smValidate("editconditionitem_dialog_0") == false) return;
					var $container = dlg.container;
					var conditionItem = {};
					if ($("select[data-field='ddlTriggerType']", $container).length == 0) {
						conditionItem.Type = "CQ";
					} else {
						conditionItem.Type = $("select[data-field='ddlTriggerType']", $container).val();
					}
					if (conditionItem.Type == "CQ") {
						var $ddlCqName = $("select[data-field='ddlCqName']",$container);
						conditionItem.Name = $ddlCqName.find("option:selected").val();
						conditionItem.Text = $ddlCqName.find("option:selected").text();
						conditionItem.Value = $("input[data-field='txtValue']",$container).val();
					} else if (conditionItem.Type == "AQ") {
						var $ddlAqName = $("select[data-field='ddlAqName']",$container);
						conditionItem.Name = $ddlAqName.find("option:selected").val();
						conditionItem.Text = $ddlAqName.find("option:selected").text();
						conditionItem.Value = $("input[data-field='txtValue']",$container).val();
					} else if (conditionItem.Type == "PQ") {
						var $ddlPqName = $("select[data-field='ddlPqName']",$container);
						conditionItem.Name = $ddlPqName.find("option:selected").val();
						conditionItem.Text = $ddlPqName.find("option:selected").text();
						conditionItem.Value = $("input[data-field='txtValue']",$container).val();
					} else if (conditionItem.Type == "PP") {
						conditionItem.Value = $("select[data-field='ddlPurposeValue']", $container).val();
					} else if (conditionItem.Type == "VT") {
						conditionItem.Value = $("select[data-field='ddlVehicleTypeValue']", $container).val();
					} else if (conditionItem.Type == "LC") {
						conditionItem.Value = $("select[data-field='ddlLineofCreditValue']", $container).val();
					} else {
						conditionItem.Value = $("select[data-field='ddlLoanTypeValue']", $container).val();
					}

					conditionItem.Comparer = $("select[data-field='ddlComparer']",$container).val();

					var $parentElement = $(parentElement);
					$parentElement.html("");

					if (conditionItem.Type == "CQ") {
						$parentElement.append($("<div>the answer of custom question <span class=\"target-item\">" + _COMMON.HtmlEncode(conditionItem.Name) + "</span> <span><i>" + _COMMON.HtmlEncode(conditionItem.Text) + "</i></span> <strong>" + translateOperator(conditionItem.Comparer) + "</strong> \"<span class='condition-item-value'>" + _COMMON.HtmlEncode(conditionItem.Value) + "</span>\"</div>"));
					} else if (conditionItem.Type == "AQ") {
						$parentElement.append($("<div>the answer of applicant question <span class=\"target-item\">" + _COMMON.HtmlEncode(conditionItem.Name) + "</span> <span><i>" + _COMMON.HtmlEncode(conditionItem.Text) + "</i></span> <strong>" + translateOperator(conditionItem.Comparer) + "</strong> \"<span class='condition-item-value'>" + _COMMON.HtmlEncode(conditionItem.Value) + "</span>\"</div>"));
					} else if (conditionItem.Type == "PQ") {
						$parentElement.append($("<div>the answer of product question <span class=\"target-item\">" + _COMMON.HtmlEncode(conditionItem.Name) + "</span> <span><i>" + _COMMON.HtmlEncode(conditionItem.Text) + "</i></span> <strong>" + translateOperator(conditionItem.Comparer) + "</strong> \"<span class='condition-item-value'>" + _COMMON.HtmlEncode(conditionItem.Value) + "</span>\"</div>"));
					} else if (conditionItem.Type == "PP") {
						$parentElement.append($("<div>" + translateLoanTypeName(loanType) + " Purpose <strong>" + translateOperator(conditionItem.Comparer) + "</strong> \"<span class='condition-item-value'>" + _COMMON.HtmlEncode(conditionItem.Value) + "</span>\"</div>"));
					} else if (conditionItem.Type == "VT") {
						$parentElement.append($("<div> Vehicle Type <strong>" + translateOperator(conditionItem.Comparer) + "</strong> \"<span class='condition-item-value'>" + _COMMON.HtmlEncode(conditionItem.Value) + "</span>\"</div>"));
					} else if (conditionItem.Type == "LC") {
						$parentElement.append($("<div>" + translateLoanTypeName(loanType) + " Is a Line of Credit <strong>" + translateOperator(data.Comparer) + "</strong> \"<span class='condition-item-value'>" + _COMMON.HtmlEncode(data.Value) + "</span>\"</div>"));
					} else {
						$parentElement.append($("<div>" + translateLoanTypeName(loanType) + " Type <strong>" + translateOperator(conditionItem.Comparer) + "</strong> \"<span class='condition-item-value'>" + _COMMON.HtmlEncode(conditionItem.Value) + "</span>\"</div>"));
					}
					$parentElement.data("type", conditionItem.Type);
					var $editBtn = $("<a href='javascript:void(0)'/>").text("Edit").on("click", function() {
						master.FACTORY.showEditConditionPopup(loanType, $parentElement,targetItem, targetText, conditionItem, targetType);
					});
					var $deleteBtn = $("<a href='javascript:void(0)'/>").text("Delete").on("click", function () {
						deleteConditionItem(this);
					});

					var $increaseIndentBtn = $("<a href='javascript:void(0)'/>").text("+ Group").on("click", function () {
						var $self = $(this);
						var $conditionItem = $self.closest("div.condition-item");
						increaseIndent($conditionItem);
					});
					var $decreaseIndentBtn = $("<a href='javascript:void(0)'/>").text("- Group").on("click", function () {
						var $self = $(this);
						var $conditionItem = $self.closest("div.condition-item");
						decreaseIndent($conditionItem);
					});
					
					$("<div/>").append($editBtn).append($("<span> | </span>")).append($deleteBtn).append($("<span> | </span>")).append($increaseIndentBtn).append($("<span> | </span>")).append($decreaseIndentBtn).appendTo($parentElement);
					dlg.dialog.close();
				}, function(container) {
					//onready
					var $ddlTriggerType = $("select[data-field='ddlTriggerType']", $(container));
					if (targetType == "PQ") {
						$ddlTriggerType.find("option").addClass("hidden");
						$ddlTriggerType.find("option[value='PQ']").removeClass("hidden");
					} else {
						$ddlTriggerType.find("option").removeClass("hidden");
						$ddlTriggerType.find("option[value='PQ']").addClass("hidden");
					}
					$("select[data-field='ddlTriggerType']", $(container)).on("change", function() {
						var $self = $(this);
						hideAllFields(container);
						if ($self.val() == "CQ") {
							$("select[data-field='ddlCqName']", $(container)).closest("div.col-12").removeClass("hidden");
						}else if ($self.val() == "AQ") {
							$("select[data-field='ddlAqName']", $(container)).closest("div.col-12").removeClass("hidden");
						}else if ($self.val() == "PQ") {
							$("select[data-field='ddlPqName']", $(container)).closest("div.col-12").removeClass("hidden");
						}else if ($self.val() == "PP") {
							$("select[data-field='ddlPurposeValue']", $(container)).closest("div.col-12").removeClass("hidden");
							$("select[data-field='ddlCqName']", $(container)).val("");
							$("select[data-field='ddlAqName']", $(container)).val("");
							$("select[data-field='ddlPqName']", $(container)).val("");
							$("select[data-field='ddlComparer']", $(container)).closest("div.col-12").removeClass("hidden");
						} else if ($self.val() == "VT") {
							$("select[data-field='ddlVehicleTypeValue']", $(container)).closest("div.col-12").removeClass("hidden");
							$("select[data-field='ddlCqName']", $(container)).val("");
							$("select[data-field='ddlAqName']", $(container)).val("");
							$("select[data-field='ddlPqName']", $(container)).val("");
							$("select[data-field='ddlComparer']", $(container)).closest("div.col-12").removeClass("hidden");
						}else if ($self.val() == "LT") {
							$("select[data-field='ddlLoanTypeValue']", $(container)).closest("div.col-12").removeClass("hidden");
							$("select[data-field='ddlCqName']", $(container)).val("");
							$("select[data-field='ddlAqName']", $(container)).val("");
							$("select[data-field='ddlPqName']", $(container)).val("");
							$("select[data-field='ddlComparer']", $(container)).closest("div.col-12").removeClass("hidden");
						}else if ($self.val() == "LC") {
							$("select[data-field='ddlLineofCreditValue']", $(container)).closest("div.col-12").removeClass("hidden");
							$("select[data-field='ddlCqName']", $(container)).val("");
							$("select[data-field='ddlAqName']", $(container)).val("");
							$("select[data-field='ddlPqName']", $(container)).val("");
							$("select[data-field='ddlComparer']", $(container)).closest("div.col-12").removeClass("hidden");
							$("select[data-field='ddlComparer'] option[data-type='nonBooleanValue']", $(container)).toggleClass("hidden", true);
						} else {
							$("select[data-field='ddlCqName']", $(container)).val("");
							$("select[data-field='ddlAqName']", $(container)).val("");
							$("select[data-field='ddlPqName']", $(container)).val("");
						}
					});
					$("select[data-field='ddlTriggerType']", $(container)).val(data.Type).trigger("change");
					
					$("select[data-field='ddlCqName'],select[data-field='ddlAqName'],select[data-field='ddlPqName']", $(container)).on("change", function() {
						var $self = $(this);
						if ($self.val() == "") {
							$("select[data-field='ddlComparer']", $(container)).closest("div.col-12").addClass("hidden");
							$("input[data-field='txtValue']", $(container)).closest("div.col-12").addClass("hidden");
						} else {
							$("select[data-field='ddlComparer']", $(container)).closest("div.col-12").removeClass("hidden");
							$("input[data-field='txtValue']", $(container)).closest("div.col-12").removeClass("hidden");
						}
						
					});
					if (data.Type == "CQ") {
						$("input[data-field='txtValue']", $(container)).val(data.Value);
						$("select[data-field='ddlCqName']", $(container)).val(data.Name).trigger("change");
					} else if (data.Type == "AQ") {
						$("input[data-field='txtValue']", $(container)).val(data.Value);
						$("select[data-field='ddlAqName']", $(container)).val(data.Name).trigger("change");
					} else if (data.Type == "PQ") {
						$("input[data-field='txtValue']", $(container)).val(data.Value);
						$("select[data-field='ddlPqName']", $(container)).val(data.Name).trigger("change");
					} else if (data.Type == "PP") {
						$("select[data-field='ddlPurposeValue']", $(container)).val(data.Value);
					} else if (data.Type == "VT") {
						$("select[data-field='ddlVehicleTypeValue']", $(container)).val(data.Value);
					} else if (data.Type == "LC") {
						$("select[data-field='ddlLineofCreditValue']", $(container)).val(data.Value);
					} else {
						$("select[data-field='ddlLoanTypeValue']", $(container)).val(data.Value);
					}
					$("select[data-field='ddlComparer']", $(container)).val(data.Comparer);
					registerDataValidator($("#editconditionitem_dialog_0"), "editconditionitem_dialog_0");
				}, function() {
					//onclose
					$.smValidate.cleanGroup("editconditionitem_dialog_0");
				});
			};
		}(window.master = window.master || {}, jQuery));
		function initAdvancedLogicItems(container, items, loanType) {
			for (var i = 0; i < items.length; i++) {
				buildAdvancedLogicItem(container, items[i], loanType);
			}
		}
		function buildCustomQuestionOptionsList(loanType) {
			var result = "";
			result = result + '<option value="">Please select a question</option>';
			$.each(CustomQuestionList[loanType.toUpperCase()], function(idx, item) {
				result = result + '<option value="' + item.Name + '">' + item.Text + '</option>';
			});
			return result;
		}
		function buildApplicantQuestionOptionsList(loanType) {
			var result = "";
			result = result + '<option value="">Please select a question</option>';
			$.each(ApplicantQuestionList[loanType.toUpperCase()], function(idx, item) {
				result = result + '<option value="' + item.Name + '">' + item.Text + '</option>';
			});
			return result;
		}
		function buildProductQuestionOptionsList(loanType) {
			var result = "";
			result = result + '<option value="">Please select a question</option>';
			if (loanType.toUpperCase().match(/(XA|COMBO)$/)) {
				$.each(ProductQuestionList[RegExp.$1.toUpperCase()], function(idx, item) {
					result = result + '<option value="' + item.Name + '">' + item.Text + '</option>';
				});
			}
			return result;
		}
		function truncatedTargetOption(value, text, target ){
            const truncatedLevel = 80;
            var outputText = text;
            if (outputText.length > truncatedLevel) {
                outputText = outputText.substring(0, truncatedLevel-1) + '...';
            }
			return '<option value="' + value + '" data-target-type="' + target + '" ' + (text != outputText ? ('title="' + text + '"') : '') + '>' + outputText + '</option>';
		}
		function buildTargetItemOptionsList(loanType) {
			var result = "";
			//for now, it's all about custom question
			result = result + '<option value="">Please select a target item</option>';
			result = result + '<optgroup label="Custom Questions">';
			$.each(CustomQuestionList[loanType.toUpperCase()], function(idx, item) {
				result = result + truncatedTargetOption(item.Name, item.Text, "CQ");
			});
			result = result + '</optgroup>';
			result = result + '<optgroup label="Applicant Questions">';
			$.each(ApplicantQuestionList[loanType.toUpperCase()], function(idx, item) {
				result = result + truncatedTargetOption(item.Name, item.Text, "AQ");
			});
			result = result + '</optgroup>';
			if(loanType.toUpperCase().match(/(XA|COMBO)$/)){
				if(ProductQuestionList[RegExp.$1.toUpperCase()].length > 0) {
					result = result + '<optgroup label="Product Questions">';
					$.each(ProductQuestionList[RegExp.$1.toUpperCase()], function(idx, item) {
						result = result + truncatedTargetOption(item.Name, item.Text, "PQ");
					});
					result = result + '</optgroup>';
				}
			}
			if (loanType.toUpperCase() != "XA") {
				result = result + '<optgroup label="Loan Purposes">';
				$.each(LoanPurposeList[loanType.toUpperCase().replace("_COMBO", "")], function(idx, item) {
					result = result + truncatedTargetOption(item.value, item.text, "PP");
				});
				result = result + '</optgroup>';
			}
			return result;
		}
		function buildComparerOptionList() {
			var result = "";
			var options = [{text: "is", value: "equal"},{text: "is not", value: "notEqual"},{text: "is greater than", value: "greaterThan"},{text: "is less than", value: "lessThan"},{text: "is greater than or equal to", value: "greaterThanOrEqual"},{text: "is less than or equal to", value: "lessThanOrEqual"}];
			$.each(options, function(idx, item) {
				result = result + '<option value="' + item.value + '" data-type="' + ((item.value=="equal" || item.value=="notEqual") ? "booleanValue" : "nonBooleanValue") +'">' + item.text + '</option>';
			});
			return result;
		}
		function buildLoanPurposeOptionsList(loanType) {
			var result = "";
			//result = result + '<option value="">Please select a purpose</option>';
			$.each(LoanPurposeList[loanType.toUpperCase().replace("_COMBO", "")], function(idx, item) {
				result = result + '<option value="' + item.value + '">' + item.text + '</option>';
			});
			return result;
		}
		function buildLoanTypeOptionsList(loanType) {
			var result = "";
			result = result + '<option value="">Please select an item</option>';
			$.each(LoanTypeList[loanType.toUpperCase().replace("_COMBO", "")], function(idx, item) {
				result = result + '<option value="' + item.Value + '">' + item.Text + '</option>';
			});
			return result;
		}
		function buildLineofCreditOptionsList(){
			return '<option value="No">No</option><option value="Yes">Yes</option>';
		}

		function buildVehicleTypeOptionsList(loanType){
			var result = "";
			$.each(LoanTypeList[loanType.toUpperCase().replace("_COMBO", "")], function(idx, item) {
				result = result + '<option value="' + item.Value + '">' + item.Text + '</option>';
			});
			return result;
		}

		function buildTriggerType(loanType) {
			var result = "";
			result = result + '<option value="">Please select a trigger</option>';
			result = result + '<option value="CQ">Answer of Custom Question</option>';
			result = result + '<option value="AQ">Answer of Applicant Question</option>';
			if (/(XA|COMBO)$/.test(loanType.toUpperCase())) {
				result = result + '<option value="PQ">Answer of Product Question</option>';
			}
            if(/(PL|PL_COMBO)$/.test(loanType.toUpperCase())) {
                result = result + '<option value="LC">Is Line of Credit?</option>';
			}
			if(/(VL|VL_COMBO)$/.test(loanType.toUpperCase())) {
                result = result + '<option value="VT">Vehicle Type</option>';
			}
			if (loanType.toUpperCase() !== "XA") {
				result = result + '<option value="PP">' + translateLoanTypeName(loanType) + ' Purpose</option>';
			}
			if (loanType.toUpperCase() == "CC" || loanType.toUpperCase() == "CC_COMBO") {
				result = result + '<option value="LT">' + translateLoanTypeName(loanType) + ' Type</option>';
			}
			return result;
		}
		function translateLoanTypeName(loanType) {
			switch (loanType.toUpperCase().replace("_COMBO", "")) {
				case "CC":
					return "Credit Card Loan";
				case "HE":
					return "Home Equity Loan";
				case "PL":
					return "Personal Loan";
				case "VL":
					return "Vehicle Loan";
				default:
					return "Loan";
			}
		}
		function buildAdvancedLogicItem(container, data, loanType) {
			//data = {TargetText: "", TargetItem: "", Conditions: [{Text: "", Name: "", Comparer: "", Value: "", Type: "", LogicalOperator: ""}]}
			var $item = $("<div/>", { "class": "logic-item-container" });
			var $title = $("<div/>", { "class": "custom-question-title" });
			$("<div/>", { "class": "target-item", "data-target-type" : data.TargetType }).text(data.TargetItem).appendTo($title);
			$("<div/>").html('<i>' + data.TargetText + '</i>').appendTo($title);
			$("<span/>", { "class": "remove-btn" }).appendTo($title).on("click", function () {
				var $self = $(this);
				$self.closest("div.logic-item-container").remove();
				if ($(container).find("div.logic-item-container").length == 0) {
					$(container).find("div.no-data").removeClass("hidden");
				}
			});
			var $expression = $("<div/>", { "class": "expression-text", "style": "visibility: hidden"});
			$title.append($expression);
			$item.append($title);
			$("<p/>").text("is visible when").appendTo($item);
			/*if (data.TargetType == "CQ" || data.TargetType == "AQ" || data.TargetType == "PQ") {
				$("<p/>").text("is visible when").appendTo($item);
			} else {
				$("<p/>").text("is visible when").appendTo($item);
			}*/
			for (var i = 0; i < data.Conditions.length; i++) {
				var indent = 0;
				if (i > 0) {
					indent = data.Conditions[i].LogicalIndent > data.Conditions[i - 1].LogicalIndent ? data.Conditions[i - 1].LogicalIndent : data.Conditions[i].LogicalIndent;
				}
				addConditionItem(data.Conditions[i], $item, loanType, indent);
			}
			$("<div class=\"add-new-condition-panel\"><a href='javascript:void(0)'>Add additional condition</a></div>").appendTo($item).find("a").on("click", function() {
				var $self = $(this);
				var $con = $self.closest("div.logic-item-container");
				var targetItem = $con.find("div.target-item").text();
				var targetType = $con.find("div.target-item").data("target-type");
				var targetText = $con.find("div.target-item+div").text();
				master.FACTORY.showAddNewConditionPopup(loanType, $con, targetItem, targetText, targetType);
			});
			$(container).find("div.no-data").addClass("hidden");
			$(container).append($item);
			translateToExpression($item);

		}
		function addConditionItem(data, con, loanType, indent) {
			var $con = $(con);
			if ($con.find("div.condition-item").length > 0) {
				var $logical = $("<div/>", { "class": "logical-item" }).append($("<span/>").text(data.LogicalOperator.toUpperCase()));
				setIndent($logical, indent, indent);
				$("<a href='javascript:void(0)'/>").text("Switch to " + (data.LogicalOperator.toUpperCase() == "AND" ? "OR" : "AND")).appendTo($logical).on("click", function() {
					var $self = $(this);
					var $span = $self.prev("span");
					if ($span.text() == "AND") {
						$span.text("OR");
						$self.text("Switch to AND");
					} else {
						$span.text("AND");
						$self.text("Switch to OR");
					}
					translateToExpression($con);
				});
				if ($con.find("div.add-new-condition-panel").length > 0) {
					$con.find("div.add-new-condition-panel").before($logical);
				} else {
					$con.append($logical);
				}
			}
			var $condition = $("<div/>", { "class": "condition-item" });
			setIndent($condition, data.LogicalIndent, data.LogicalIndent);
			if (data.Type == "CQ") {
				$condition.append($("<div>the answer of custom question <span class=\"target-item\"><strong>" + _COMMON.HtmlEncode(data.Name) + "</strong></span> <span><i>" + _COMMON.HtmlEncode(data.Text) + "</i></span> <strong>" + translateOperator(data.Comparer) + "</strong> \"<span class='condition-item-value'>" + _COMMON.HtmlEncode(data.Value) + "</span>\"</div>"));
			} else if (data.Type == "AQ") {
				$condition.append($("<div>the answer of applicant question <span class=\"target-item\"><strong>" + _COMMON.HtmlEncode(data.Name) + "</strong></span> <span><i>" + _COMMON.HtmlEncode(data.Text) + "</i></span> <strong>" + translateOperator(data.Comparer) + "</strong> \"<span class='condition-item-value'>" + _COMMON.HtmlEncode(data.Value) + "</span>\"</div>"));
			} else if (data.Type == "PQ") {
				$condition.append($("<div>the answer of product question <span class=\"target-item\"><strong>" + _COMMON.HtmlEncode(data.Name) + "</strong></span> <span><i>" + /*_COMMON.HtmlEncode(data.Text)*/ data.Text + "</i></span> <strong>" + translateOperator(data.Comparer) + "</strong> \"<span class='condition-item-value'>" + _COMMON.HtmlEncode(data.Value) + "</span>\"</div>"));
			} else if (data.Type == "PP") {
				$condition.append($("<div>" + translateLoanTypeName(loanType) + " purpose <strong>" + translateOperator(data.Comparer) + "</strong> \"<span class='condition-item-value'>" + _COMMON.HtmlEncode(data.Value) + "</span>\"</div>"));
			} else if (data.Type == "VT") {
				$condition.append($("<div>Vehicle Type <strong>" + translateOperator(data.Comparer) + "</strong> \"<span class='condition-item-value'>" + _COMMON.HtmlEncode(data.Value) + "</span>\"</div>"));
			} else if (data.Type == "LC") {
				$condition.append($("<div>" + translateLoanTypeName(loanType) + " Is a Line of Credit <strong>" + translateOperator(data.Comparer) + "</strong> \"<span class='condition-item-value'>" + _COMMON.HtmlEncode(data.Value) + "</span>\"</div>"));
			} else { //LT: loan type
				$condition.append($("<div>" + translateLoanTypeName(loanType) + " type <strong>" + translateOperator(data.Comparer) + "</strong> \"<span class='condition-item-value'>" + _COMMON.HtmlEncode(data.Value) + "</span>\"</div>"));
			}
			$condition.data("type", data.Type);
			var $editBtn = $("<a href='javascript:void(0)'/>").text("Edit").on("click", function() {
				var $self = $(this);
				var $logicItemContainer = $self.closest("div.logic-item-container");
				var targetItem = $logicItemContainer.find("div.target-item").text();
				var targetType = $logicItemContainer.find("div.target-item").data("target-type");
				var targetText = $logicItemContainer.find("div.target-item+div").text();
				master.FACTORY.showEditConditionPopup(loanType, $condition,targetItem, targetText, data, targetType);
			});
			var $deleteBtn = $("<a href='javascript:void(0)'/>").text("Delete").on("click", function () {
				deleteConditionItem(this);
			});

			var $increaseIndentBtn = $("<a href='javascript:void(0)'/>").text("+ Group").on("click", function () {
				var $self = $(this);
				var $conditionItem = $self.closest("div.condition-item");
				increaseIndent($conditionItem);
			});
			var $decreaseIndentBtn = $("<a href='javascript:void(0)'/>").text("- Group").on("click", function () {
				var $self = $(this);
				var $conditionItem = $self.closest("div.condition-item");
				decreaseIndent($conditionItem);
			});
			$("<div/>").append($editBtn).append($("<span> | </span>")).append($deleteBtn).append($("<span> | </span>")).append($increaseIndentBtn).append($("<span> | </span>")).append($decreaseIndentBtn).appendTo($condition);
			if ($con.find("div.add-new-condition-panel").length > 0) {
				$con.find("div.add-new-condition-panel").before($condition);
			} else {
				$con.append($condition);
			}
		}
		function increaseIndent(container) {
			var $container = $(container);
			var indent = $container.data("indent");
			if (indent > 5) return;
			setIndent($container, indent, indent + 1);
			var $logicalItem, $nextConditionItem, $prevConditionItem, $nextLogicalItem;
			if ($container.is(".condition-item:first")) {
				$logicalItem = $container.next(".logical-item");
				$nextConditionItem = $logicalItem.next(".condition-item");
				if ($logicalItem.data("indent") < $nextConditionItem.data("indent")) {
					setIndent($logicalItem, indent, indent + 1);
				}
			} else if ($container.is(".condition-item:last")) {
				$logicalItem = $container.prev(".logical-item");
				$prevConditionItem = $logicalItem.prev(".condition-item");
				if ($logicalItem.data("indent") < $prevConditionItem.data("indent")) {
					setIndent($logicalItem, indent, indent + 1);
				}
			} else {
				$logicalItem = $container.prev(".logical-item");
				$nextLogicalItem = $container.next(".logical-item");
				$prevConditionItem = $logicalItem.prev(".condition-item");
				$nextConditionItem = $nextLogicalItem.next(".condition-item");
				if ($logicalItem.data("indent") < $prevConditionItem.data("indent")) {
					setIndent($logicalItem, indent, indent + 1);
				}
				if ($nextLogicalItem.data("indent") < $nextConditionItem.data("indent")) {
					setIndent($nextLogicalItem, indent, indent + 1);
				}
			}
			translateToExpression($container.closest("div.logic-item-container"));
		}
		function setIndent(ele, currentIndent, newIndent) {
			var $ele = $(ele);
			$ele.removeClass("indent" + currentIndent).addClass("indent" + (newIndent)).data("indent", newIndent);
		}
		function decreaseIndent(container) {
			var $container = $(container);
			var indent = $container.data("indent");
			if (indent == 0) return;
			setIndent($container, indent, indent - 1);
			var $logicalItem, $nextConditionItem, $prevConditionItem, $nextLogicalItem;
			if ($container.is(".condition-item:first")) {
				$logicalItem = $container.next(".logical-item");
				$nextConditionItem = $logicalItem.next(".condition-item");
				setIndent($logicalItem, indent, $nextConditionItem.data("indent") < $container.data("indent") ? $nextConditionItem.data("indent") : $container.data("indent") );
			} else if ($container.is(".condition-item:last")) {
				$logicalItem = $container.prev(".logical-item");
				$prevConditionItem = $logicalItem.prev(".condition-item");
				setIndent($logicalItem, indent, $prevConditionItem.data("indent") < $container.data("indent") ? $prevConditionItem.data("indent") : $container.data("indent") );
			} else {
				$logicalItem = $container.prev(".logical-item");
				$nextLogicalItem = $container.next(".logical-item");
				$prevConditionItem = $logicalItem.prev(".condition-item");
				$nextConditionItem = $nextLogicalItem.next(".condition-item");
				setIndent($nextLogicalItem, indent, $nextConditionItem.data("indent") < $container.data("indent") ? $nextConditionItem.data("indent") : $container.data("indent") );
				setIndent($logicalItem, indent, $prevConditionItem.data("indent") < $container.data("indent") ? $prevConditionItem.data("indent") : $container.data("indent") );
			}
			translateToExpression($container.closest("div.logic-item-container"));
		}
		function deleteConditionItem(ele) {
			var $ele = $(ele);
			var $conditionItem = $ele.closest("div.condition-item");
			var $container = $conditionItem.closest("div.logic-item-container");
			if ($conditionItem.prev("div.logical-item").length == 0) {
				$conditionItem.next("div.logical-item").remove();
			} else {
				$conditionItem.prev("div.logical-item").remove();
			}
			$conditionItem.remove();
			translateToExpression($container);
			
		}
		function translateOperator(op) {
			var result = "";
			switch (op) {
			case "equal":
				result =  "is";
				break;
			case "greaterThan":
				result =  "is greater than";
				break;
			case "lessThan":
				result =  "is less than";
				break;
			case "greaterThanOrEqual":
				result =  "is greater than or equal to";
				break;
			case "lessThanOrEqual":
				result =  "is less than or equal to";
				break;
			case "notEqual":
				result =  "is not";
				break;
			default:
				result =  "";
				break;
			}
			return "<span data-value=\"" + op + "\" class=\"comparer\">" + result + "</span>";
		}
		function registerDataValidator(container, groupName) {
			var $container = $(container);
			$.smValidate.removeValidationGroup(groupName);
			$("[data-required='true'],[data-format]", $container).each(function (idx, ele) {
				if ($(ele).is(":radio")) {
					var $placeHolder = $(ele).closest("div");
					$(ele).observer({
						validators: [
							function (partial) {
								var $self = $(this);
								if ($self.is(":visible") == false) return "";
								if ($("input[name='" + $self.attr("name") + "']:checked", $(container)).length == 0) {
									return "Please select an option";
								}
								return "";
							}
						],
						validateOnBlur: true,
						placeHolder: $placeHolder,
						group: groupName
					});
				}else{
					$(ele).observer({
						validators: [
							function (partial) {
								var $self = $(this);
								if ($self.is(":visible") == false) return "";
								if ($self.data("required") && $.trim($self.val()) === "") {
									if ($self.data("message-require")) {
										return $self.data("message-require");
									} else {
										return "This field is required";
									}
								}
								var format = $self.data("format");
								if (format) {
									var msg = "Invalid format";
									if ($self.data("message-invalid-data")) {
										msg = $self.data("message-invalid-data");
									}
									if (format === "email") {
										if (_COMMON.ValidateEmail($self.val()) === false) {
											return msg;
										}
									} else if (format === "phone") {
										if (_COMMON.ValidatePhone($self.val()) === false) {
											return msg;
										}
									}
								}
								return "";
							}
						],
						validateOnBlur: true,
						group: groupName
					});
				}
			});
		}
		function translateToExpression(container) {
			var expression = "";
			var prefix = "$exp{index}";
			var $container = $(container);
			var conditionList = $("div.condition-item", $container);
			conditionList.each(function(idx, item) {
				var $item = $(item);
				var $prevConditionItem = $(conditionList[idx - 1]);
				if (idx == 0) {
					expression += generateNumberOfCharacters("(", $(item).data("indent"));
				} else {
					if ($item.data("indent") < $prevConditionItem.data("indent")) {
						expression += generateNumberOfCharacters(")", $prevConditionItem.data("indent") - $(item).data("indent"));
						expression += " <span>" + $item.prev(".logical-item").find(">span").text() + "</span> ";
					}else if ($item.data("indent") > $prevConditionItem.data("indent")) {
						expression += " <span>" + $item.prev(".logical-item").find(">span").text() + "</span> ";
						expression += generateNumberOfCharacters("(", $(item).data("indent") - $prevConditionItem.data("indent"));
					} else {
						expression += " <span>" + $item.prev(".logical-item").find(">span").text() + "</span> ";
					}
				}
				expression += prefix.replace("{index}", idx + 1);
				if (idx === conditionList.length-1) {
					expression += generateNumberOfCharacters(")", $(item).data("indent"));
				}
			});
			$container.find("div.expression-text").html(expression);
		}
		function generateNumberOfCharacters(c, count) {
			var result = "";
			for (var i = 0; i < count; i++) {
				result = result + c;
			}
			return result;
		}
		function refineIndent(container) {
			//todo
			console.log(container);
		}
		function collectSubmitData() {
			var result = {};
			var loanTypeList = <%=JsonConvert.SerializeObject(LoanTypeList)%>;

			var data = {};
			$.each(loanTypeList, function(idx, loanType) {
				data[loanType] = collectLogicItem(loanType);
			});
			result.data = _COMMON.toJSON(data);
			result.lenderconfigid = '<%=LenderConfigID%>';
			result.preview_type = $("#mainTabs >li.active").data("preview-type");
			return result;
		}
		function collectLogicItem(loanType) {
			var result = [];
			$("div.logic-item-container", $("#" + loanType)).each(function(idx, ele) {
				var $ele = $(ele);
				if ($ele.find("div.condition-item").length > 0) {
					var logicItem = {};
					var targetItem = $ele.find("div.target-item").text();
					var targetText = $ele.find("div.target-item+div").text();
					var targetType = $ele.find("div.target-item").data("target-type");
					var expression = $ele.find("div.expression-text").text();
					logicItem.TargetItem = targetItem;
					logicItem.TargetText = targetText;
					logicItem.TargetType = targetType;
					logicItem.Expression = expression;
					logicItem.Conditions = [];
					$("div.condition-item", $ele).each(function(i, condition) {
						var $condition = $(condition);
						var conditionItem = {};

						conditionItem.Type = $condition.data("type");
						if (conditionItem.Type == "CQ" || conditionItem.Type == "AQ" || conditionItem.Type == "PQ") {
							conditionItem.Name = $("span.target-item", $condition).text();
							conditionItem.Text = $("span.target-item+span", $condition).text();
						}
						conditionItem.Value = $("span.condition-item-value",$condition).text();
						conditionItem.Comparer = $("span.comparer",$condition).data("value");
						conditionItem.LogicalOperator = "";
						if (i > 0) {
							var $logicalItem = $condition.prev("div.logical-item");
							conditionItem.LogicalOperator = $logicalItem.find("span").text();
						}

						conditionItem.LogicalIndent = $condition.data("indent");
						logicItem.Conditions.push(conditionItem);
					});
					result.push(logicItem);
				}
			});
			return result;
		}
	</script>
</asp:Content>
<asp:Content runat="server" ContentPlaceHolderID="plActionButtons"></asp:Content>