﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="ManagePL.aspx.vb" Inherits="Sm_ManagePL" MasterPageFile="SiteManager.master" %>
<%@ Import Namespace="Sm" %>

<asp:Content runat="server" ContentPlaceHolderID="plBody">
	<div>
		<h1 class="page-header">Manage Personal Loan Settings</h1>
		<p>Use this page to manage what’s available for your Application Portal including Purpose Types & Decision Messages, and Disclosures</p>
		<section data-name="PersonalLoanPurposes">
			<h2 class="section-title">Purpose(non-LOC)</h2>
			<p>Select which purposes you want to make available. Values for these purposes are configured in your In-Branch Configure Site settings.</p>
			<%If PurposeList IsNot Nothing AndAlso PurposeList.Any() Then%>
			<div class="checkbox-btn-wrapper text-bold row">
				<div class="col-sm-7">Purpose Type</div>
				<div class="col-sm-4">Personal Loan Type</div>
				<div class="col-sm-1"></div>
			</div>
			<%For Each item As KeyValuePair(Of String, SmTextValueCatItem) In PurposeList.OrderBy(Function(p) p.Value.value)%>
			<div class="checkbox-btn-wrapper row">
				<div class="col-sm-7"><%=item.Value.text%></div>
				<div class="col-sm-4">
					<select class="form-control inline-select <%=IIf(BindCheckbox(item.Value.value, EnabledPurposeList) = "checked", "", "hidden")%>">
						<%--<option <%=BindSelectbox(item.Value.category, "")%> value="UNSECURE"></option>--%>
						<option <%=BindSelectbox(item.Value.category, "UNSECURE")%> value="UNSECURE">Unsecured</option>
						<option <%=BindSelectbox(item.Value.category, "SECURE")%> value="SECURE">Secured</option>
						<option <%=BindSelectbox(item.Value.category, "SHARES")%> value="SHARES"><%:IIf(IsBankPortal, "Secured with Savings", "Secured with Shares")%></option>
						<option <%=BindSelectbox(item.Value.category, "CD")%> value="CD">Secured with CD</option>
					</select>
				</div>
				<div class="col-sm-1">
					<label class="toggle-btn pull-right" data-on="ON" data-off="OFF">
						<input type="checkbox" data-text="<%=item.Value.text%>" value="<%=item.Value.value%>" <%=BindCheckbox(item.Value.value, EnabledPurposeList)%>/>
						<span class="button-checkbox"></span>
					</label>
				</div>
			</div>
			<%Next%>
			<%End If%>
		</section>
		<section data-name="PersonalLoanLOC">
			<h2 class="section-title">Purpose(LOC)</h2>
			<p>Select which purposes you want to make available. Select the person loan type and specify which overdraft options will display the purpose type.</p>
			<%If LOCTypeList IsNot Nothing AndAlso LOCTypeList.Any() Then%>
			<div class="checkbox-btn-wrapper text-bold row">
				<div class="col-sm-5">Purpose Type</div>
				<div class="col-sm-3">Personal Loan Type</div>
				<div class="col-sm-3">Display for the following options</div>
				<div class="col-sm-1"></div>
			</div>
			<%For Each item As KeyValuePair(Of String, SmLineOfCreditItem) In LOCTypeList.OrderBy(Function(p) p.Value.Value)%>
			<div class="checkbox-btn-wrapper row">
				<div class="col-sm-5"><%=item.Value.text%></div>
				<div class="col-sm-3">
					<select data-field="line-of-credit-category" class="form-control no-margin inline-select <%=IIf(BindCheckbox(item.Value.value, EnabledLOCTypeList) = "checked", "", "hidden")%>">
						<%For Each cat As CEnum.LocCategory In [Enum].GetValues(GetType(CEnum.LocCategory))%>
						<option <%=If(item.Value.Category = cat, "selected", "")%> value="<%=cat.ToString()%>"><%=cat.GetEnumDescription()%></option>
						<%Next%>
					</select>
				</div>
				<div class="col-sm-3">
					<select data-field="line-of-credit-type" class="form-control no-margin inline-select <%=IIf(BindCheckbox(item.Value.value, EnabledLOCTypeList) = "checked", "", "hidden")%>">
						<%	For Each overDraftOption As CEnum.LocOverDraftOption In [Enum].GetValues(GetType(CEnum.LocOverDraftOption))%>
						<option <%=If(item.Value.OverDraftOption = overDraftOption, "selected", "")%> value="<%=overDraftOption.ToString()%>"><%=overDraftOption.GetEnumDescription()%></option>
						<%Next%>
					</select>
				</div>
				<div class="col-sm-1">
					<label class="toggle-btn pull-right" data-on="ON" data-off="OFF">
						<input type="checkbox" data-text="<%=item.Value.text%>" value="<%=item.Value.value%>" <%=BindCheckbox(item.Value.Value, EnabledLOCTypeList)%>/>
						<span class="button-checkbox"></span>
					</label>
				</div>
			</div>
			<%Next%>
			<%End If%>
		</section>
		<section>
			<h2 class="section-title">Messages</h2>
			<p>These are the messages that display depending on the applicant’s status.</p>
			<div>
				<h3 class="property-title">Approved Message</h3>
				<div class="html-editor" contenteditable="true" data-required="true" placeholder="Enter your approved message..." id="txtApprovalMsg"><%=ApprovalMessage%></div>
				<%--<div class="dynamic-key-panel">
					Add a dynamic key:
					<span data-key="MEMBER_NUMBER"><i class="fa fa-plus" aria-hidden="true"></i>Application Number</span>
					<span data-key="FIRST_NAME"><i class="fa fa-plus" aria-hidden="true"></i>Application First Name</span>
					<span data-key="FULL_NAME"><i class="fa fa-plus" aria-hidden="true"></i>Application Full Name</span>
				</div>--%>
			</div>
			<div>
				<h3 class="property-title">Referred Message</h3>
				<div class="html-editor" contenteditable="true" data-required="true" placeholder="Enter your referral message..." id="txtReferredMsg"><%=ReferredMessage%></div>
				<%--<div class="dynamic-key-panel">
					Add a dynamic key:
					<span data-key="MEMBER_NUMBER"><i class="fa fa-plus" aria-hidden="true"></i>Application Number</span>
					<span data-key="FIRST_NAME"><i class="fa fa-plus" aria-hidden="true"></i>Application First Name</span>
					<span data-key="FULL_NAME"><i class="fa fa-plus" aria-hidden="true"></i>Application Full Name</span>
				</div>--%>
			</div>
			<div>
				<h3 class="property-title">Declined Message</h3>
				<div class="html-editor" contenteditable="true" data-required="false"  id="txtDeclinedMsg"><%=DeclinedMessage%></div>
				<%--<div class="dynamic-key-panel">
					Add a dynamic key:
					<span data-key="MEMBER_NUMBER"><i class="fa fa-plus" aria-hidden="true"></i>Application Number</span>
					<span data-key="FIRST_NAME"><i class="fa fa-plus" aria-hidden="true"></i>Application First Name</span>
					<span data-key="FULL_NAME"><i class="fa fa-plus" aria-hidden="true"></i>Application Full Name</span>
				</div>--%>
			</div>
            <div>
				<h3 class="property-title">Pre-qualified Message</h3>
				<div class="html-editor" contenteditable="true" data-required="false"  id="txtPreQualifiedMsg"><%=PreQualifiedMessage%></div>			
			</div>
		</section>
		<br />
		<section data-name="disclosures" data-section-type="disclosure">
			<div class="section-heading-btn clearfix">
				<div class="pull-left">
					<h2 class="section-title">Disclosures</h2>
					<p>These are the messages that display in Submit & Sign section and require applicant's consent via check boxes. Any stand-alone link, link without text before or after will be displayed as a button in your Application Portal.</p>
				</div>
				<div class="pull-right buttons">
					<button type="button" class="btn btn-primary add-disclosure mb10"><i class="fa fa-plus" aria-hidden="true"></i>Add Disclosure</button>
					<%--<button class="sm-btn" data-toggle="modal" data-target="#mdlAddDisclosure"><i class="fa fa-plus" aria-hidden="true"></i>Add Disclosure</button>--%>
				</div>
			</div>			
			<%If DisclosureList IsNot Nothing AndAlso DisclosureList.Any() Then
					Dim idx As Integer = 0%>
			<%For each disclosure as string in DisclosureList %>
			<div class="disclosure-block">
				<div id="disclosureEditor<%=idx%>" data-required="true" class="html-editor" contenteditable="true" placeholder="Enter your disclosure..."><%=disclosure%></div>
				<div class="disclosure-commands">
					<button type="button" class="btn btn-default remove-btn"><i class="fa fa-trash-o" aria-hidden="true"></i>Remove</button>
					<button type="button" class="btn btn-default moveup-btn"><i class="fa fa-chevron-circle-up" aria-hidden="true"></i>Move up</button>
					<button type="button" class="btn btn-default movedown-btn"><i class="fa fa-chevron-circle-down" aria-hidden="true"></i>Move down</button>
				</div>		
			</div>
			<% idx = idx + 1
				Next%>
			<%End If%>
			<div class="no-item <%=IIf(DisclosureList Is Nothing OrElse Not DisclosureList.Any(), "hidden", "")%>">
				<div>
					<i class="fa fa-file-o" aria-hidden="true"></i>
					<p>These are currently no disclosures added for this loan type.</p>
					<a href="#" class="add-disclosure">Add Disclosure</a>
				</div>
			</div>
			<%--<div class="modal fade" id="mdlAddDisclosure" tabindex="-1" role="dialog">
				<div class="modal-dialog modal-lg" role="document">
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
							<h4 class="modal-title">Add/Edit Disclosure</h4>
						</div>
						<div class="modal-body">
							<textarea rows="10" class="sm-textarea"></textarea>
						</div>
						<div class="modal-footer">
							<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
							<button type="button" class="btn btn-primary">Save changes</button>
						</div>
					</div>
				</div>
			</div>--%>
		</section>
	</div>
</asp:Content>
<asp:Content runat="server" ContentPlaceHolderID="plScripts">
	<script type="text/javascript" src="js/ckeditor/ckeditor.js"></script>
	<script type="text/javascript">
		CKEDITOR.disableAutoInline = true;
		$(function () {
			//var modelAddDisclosure = $("#mdlAddDisclosure");
			//modelAddDisclosure.on('hidden.bs.modal', function () {
			//	//to sonething here
			//	console.log(new Date());
			//});
			//$(".dynamic-key-panel").dynamicKey();
			$("section[data-name=disclosures]").on("change", function () {
				$(this).find(".disclosure-commands button").show();
				var $firstBlock = $(this).find(".disclosure-block:first");
				$firstBlock.find("button.moveup-btn").hide();
				var $lastBlock = $(this).find(".disclosure-block:last");
				$lastBlock.find("button.movedown-btn").hide();
				if ($(this).find(".disclosure-block").length == 0) {
					$(this).find(".no-item").removeClass("hidden");
				} else {
					$(this).find(".no-item").addClass("hidden");
				}
			}).trigger("change");
			$(".html-editor").ckEditor({
				onContentChanged: function (editor) { master.FACTORY.documentChanged(editor); },
				onCommandClicked: function (editor, commandBtn) { master.FACTORY.documentChanged(editor); }
			});

			$(".add-disclosure").click(function (e) {
				var $section = $(this).closest("section");
				var htmlStr = '<div class="disclosure-block"><div id="disclosureEditor' + new Date().getTime() + '" class="html-editor" data-required="true" contenteditable="true" placeholder="Enter your disclosure..."></div><div class="disclosure-commands"><button type="button" class="btn btn-default remove-btn"><i class="fa fa-trash-o" aria-hidden="true"></i>Remove</button><button type="button" class="btn btn-default moveup-btn"><i class="fa fa-chevron-circle-up" aria-hidden="true"></i>Move up</button><button type="button" class="btn btn-default movedown-btn"><i class="fa fa-chevron-circle-down" aria-hidden="true"></i>Move down</button></div></div>';
				var $item = $(htmlStr).insertBefore($(".no-item", $section));
				$item.find(".html-editor").ckEditor({
					onContentChanged: function (editor) { master.FACTORY.documentChanged(editor); },
					onCommandClicked: function (editor, commandBtn) { master.FACTORY.documentChanged(editor); }
				});
				$(this).closest("section").trigger("change");
				$('html, body').scrollTop($(this).closest("section").offset().top - 40);
				setTimeout(function () {
					$item.find(".html-editor").focus().trigger("focus");
				}, 200);
				e.preventDefault();
				registerDataValidator();
			});
			registerDataValidator();
			$("select,input:checkbox", "div.checkbox-btn-wrapper").on("change", function () {
				var $self = $(this);
				master.FACTORY.documentChanged(this);
				if ($self.is(":checkbox")) {
					if ($self.is(":checked")) {
						$self.closest("div.checkbox-btn-wrapper").find("select.inline-select").removeClass("hidden");
					} else {
						$self.closest("div.checkbox-btn-wrapper").find("select.inline-select").addClass("hidden");
					}
				}
			});
		});
		(function (master, $, undefined) {
			master.FACTORY.SaveDraft = function (revisionId) {
				if ($.smValidate("ValidateData") == false) return;
				var dataObj = collectSubmitData();
				dataObj.command = "savemanagepl";
				dataObj.revisionId = revisionId;
				$.ajax({
					url: "/sm/smhandler.aspx",
					async: true,
					cache: false,
					type: 'POST',
					dataType: 'html',
					data: dataObj,
					success: function (responseText) {
						var response = $.parseJSON(responseText);
						if (response.IsSuccess) {
							master.FACTORY.onSaveDraftSuccess();
						} else {
							_COMMON.noty("error", "Error", 500);
						}
					}
				});
			};
			master.FACTORY.Preview = function (revisionId) {
				if ($.smValidate("ValidateData") == false) return;
				var dataObj = collectSubmitData();
				dataObj.command = "previewmanagepl";
				dataObj.revisionId = revisionId;
				$.ajax({
					url: "/sm/smhandler.aspx",
					async: true,
					cache: false,
					type: 'POST',
					dataType: 'html',
					data: dataObj,
					success: function (responseText) {
						var response = $.parseJSON(responseText);
						if (response.IsSuccess) {
							master.FACTORY.onPreviewSuccess();
							_COMMON.openInNewTab(response.Info.previewurl);
						} else {
							_COMMON.noty("error", "Error", 500);
						}
					}
				});
			};
			master.FACTORY.validateBeforePublish = function () {
				return $.smValidate("ValidateData");
			}
			master.FACTORY.Publish = function (comment, publishAll) {
				if ($.smValidate("ValidateData") == false) return;
				var dataObj = collectSubmitData();
				dataObj.command = "publishmanagepl";
				dataObj.comment = comment;
				dataObj.publishAll = publishAll;
				$.ajax({
					url: "/sm/smhandler.aspx",
					async: true,
					cache: false,
					type: 'POST',
					dataType: 'html',
					data: dataObj,
					success: function (responseText) {
						var response = $.parseJSON(responseText);
						if (response.IsSuccess) {
							master.FACTORY.onPublishSuccess();
						} else {
							_COMMON.noty("error", "Error", 500);
						}
					}
				});
			};
		}(window.master = window.master || {}, jQuery));
		function registerDataValidator() {
			$.smValidate.removeValidationGroup("ValidateData");
			$("[data-required='true']").each(function (idx, ele) {
				$(ele).observer({
					validators: [
						function (partial) {
							var $self = $(this);
							var content = "";
							if ($self.hasClass("html-editor")) {
								content = CKEDITOR.instances[$self.attr("id")].getData();
								if (content == $self.attr("placeholder")) content = "";
							} else if ($self.hasClass("sm-textarea")) {
								content = $self.val();
							}
							if ($.trim(content) === "") return "This field is required";
							return "";
						}
					],
					validateOnBlur: true,
					group: "ValidateData"
				});
			});
		}
		function collectSubmitData() {
			var result = {};
			result.approvalMsg = _COMMON.getEditorValue("txtApprovalMsg");
			result.referredMsg = _COMMON.getEditorValue("txtReferredMsg");
            result.declinedMsg = _COMMON.getEditorValue("txtDeclinedMsg");
            result.preQualifiedMsg = _COMMON.getEditorValue("txtPreQualifiedMsg"); 
			result.disclosures = JSON.stringify(_COMMON.collectDisclosureData($("section[data-name=disclosures]")));
			result.lenderconfigid = '<%=LenderConfigID%>';
			result.locTypes = JSON.stringify(collectLocTypesData());
			result.loanPurposes = JSON.stringify(_COMMON.collectTextValueCatCheckboxGroupData($("section[data-name='PersonalLoanPurposes']")));
			return result;
		}
		function collectLocTypesData() {
			var $container = $("section[data-name='PersonalLoanLOC']");
			var result = [];
			$(".checkbox-btn-wrapper input[type='checkbox']:checked", $container).each(function (idx, ele) {
				var $ele = $(ele);
				var $chkWrapper = $ele.closest(".checkbox-btn-wrapper");
				var item = {}
				item.Text = $ele.data("text");
				item.Value = $ele.val();
				item.Category = $chkWrapper.find("select[data-field='line-of-credit-category']").val();
				item.OverDraftOption = $chkWrapper.find("select[data-field='line-of-credit-type']").val();
				result.push(item);
			});
			return result;
		}
	</script>
</asp:Content>
<asp:Content runat="server" ContentPlaceHolderID="plActionButtons"></asp:Content>