﻿Imports Sm
Imports System.Xml
Imports LPQMobile.Utils

Partial Class Sm_VLSettings
	Inherits SmApmBasePage
	Private Const LOAN_NODE_NAME As String = "VEHICLE_LOAN"
	Protected Property VLJointEnable As Boolean = True


	Protected Property VLBooking As String
	Protected Property VLXSell As Boolean = False
	Protected Property VLDocuSign As Boolean = False
	Protected Property VLDocuSignPDFGroup As String
	Protected Property VLInterestRate As String = "2.5"
	Protected Property VLTermFreeTextMode As Boolean = False

    Protected Property VLVinBarcodeScan As Boolean = False
    Protected Property VLLocationPool As Boolean = False
    Protected Property VLPreviousAddressEnable As Boolean = False
    Protected Property VLPreviousEmploymentEnable As Boolean = False
    Protected Property VLPreviousAddressThreshold As String = ""
    Protected Property VLPreviousEmploymentThreshold As String = ""
    Protected Property DeclarationList As Dictionary(Of String, SmDeclarationItem)
    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
		If Not EnableVL Then
			Response.Redirect(BuildUrl("/sm/generalsettings.aspx"), True)
		End If
		If Not Page.IsPostBack Then
			Dim smMasterPage = CType(Me.Master, Sm_SiteManager)
			smMasterPage.LenderConfigXml = LenderConfigXml
			smMasterPage.RevisionID = LenderConfig.RevisionID
			smMasterPage.LastModifiedBy = LenderConfig.LastModifiedByUserFullName
			smMasterPage.LastModifiedByUserID = LenderConfig.LastModifiedByUserID
			smMasterPage.SelectedMainMenuItem = "config"
			smMasterPage.SelectedSubMenuItem = "settings"
			smMasterPage.CurrentNodeGroup = SmSettings.NodeGroup.VLSettings
			smMasterPage.BreadCrumb = "Configure / Settings / Vehicle Loan"


			Dim VLBookingNode As XmlNode = GetNode(Sm.LOAN_BOOKING(LOAN_NODE_NAME))
			If VLBookingNode IsNot Nothing Then
				VLBooking = VLBookingNode.Value
			End If

			Dim VLxsellEnabledNode As XmlNode = GetNode(XSELL(LOAN_NODE_NAME))
			If VLxsellEnabledNode IsNot Nothing Then
				VLXSell = VLxsellEnabledNode.Value.ToUpper().Equals("Y")
			End If

			Dim VLdocusignEnabledNode As XmlNode = GetNode(DOCU_SIGN(LOAN_NODE_NAME))
			If VLdocusignEnabledNode IsNot Nothing Then
				VLDocuSign = VLdocusignEnabledNode.Value.ToUpper().Equals("Y")
			End If

			Dim VLdocusignPDFGroupNode As XmlNode = GetNode(DOCU_SIGN_PDF_GROUP(LOAN_NODE_NAME))
			If VLdocusignPDFGroupNode IsNot Nothing Then
				VLDocuSignPDFGroup = VLdocusignPDFGroupNode.Value.Trim()
			End If

			Dim customListRateNode As XmlNode = GetNode(CUSTOM_LIST_RATES("vl"))
			If customListRateNode IsNot Nothing Then
				If customListRateNode.Name = "RATES" Then
					customListRateNode = customListRateNode.SelectSingleNode("ITEM[@loan_type='vl']")
				End If
				Dim vlRateNode As XmlNode = customListRateNode.Attributes("rate")
				If vlRateNode IsNot Nothing Then
					VLInterestRate = vlRateNode.Value
				End If
			End If
			Dim vlJointEnableNode As XmlNode = GetNode(VL_JOINT_ENABLE())
			If vlJointEnableNode IsNot Nothing Then
				VLJointEnable = Not vlJointEnableNode.Value.ToUpper().Equals("N")
			End If
			Dim vlAutoTermTextboxEnableNode As XmlNode = GetNode(VL_TERM_FREE_FORM_MODE())
			If vlAutoTermTextboxEnableNode IsNot Nothing Then
				VLTermFreeTextMode = vlAutoTermTextboxEnableNode.Value.ToUpper().Equals("Y")
			End If
			Dim vlVinBarcodeScanNode As XmlNode = GetNode(VIN_BARCODE_SCAN(LOAN_NODE_NAME))
			If vlVinBarcodeScanNode IsNot Nothing Then
				VLVinBarcodeScan = vlVinBarcodeScanNode.Value.ToUpper().Equals("Y")
			End If
			Dim VLLocationPoolNode As XmlNode = GetNode(LOAN_LOCATION_POOL(LOAN_NODE_NAME))
            If VLLocationPoolNode IsNot Nothing Then
                VLLocationPool = VLLocationPoolNode.Value.ToUpper().Equals("Y")
			End If
			Dim loanDeclarationList = Common.GetAllDeclarations(LOAN_NODE_NAME)
            If loanDeclarationList IsNot Nothing AndAlso loanDeclarationList.Count > 0 Then
                DeclarationList = (From item In loanDeclarationList Select New SmDeclarationItem() With {.Key = item.Key, .DisplayText = item.Value, .Active = False}).ToDictionary(Function(p) p.Key, Function(p) p)
                Dim declarationsDraftNode As XmlNode = GetNode(LOAN_DECLARATIONS(LOAN_NODE_NAME))
                If declarationsDraftNode IsNot Nothing Then
                    Dim declarationDraftNodes = declarationsDraftNode.SelectNodes("DECLARATION")
                    If declarationDraftNodes IsNot Nothing AndAlso declarationDraftNodes.Count > 0 Then
                        For Each node As XmlNode In declarationDraftNodes
                            Dim key As String = node.Attributes("key").Value
                            If DeclarationList.ContainsKey(key) Then
                                DeclarationList.Item(key).Active = True
                            End If
                        Next
                    End If
                End If
            End If

            Dim vlPreviousEmploymentThresholdNode As XmlNode = GetNode(PREVIOUS_EMPLOYMENT_THRESHOLD("VEHICLE_LOAN"))
            If vlPreviousEmploymentThresholdNode IsNot Nothing AndAlso Common.SafeString(vlPreviousEmploymentThresholdNode.Value) <> "" Then
                VLPreviousEmploymentThreshold = Common.SafeString(vlPreviousEmploymentThresholdNode.Value)
                VLPreviousEmploymentEnable = True
            End If
            Dim vlPreviousAddressThresholdNode As XmlNode = GetNode(PREVIOUS_ADDRESS_THRESHOLD("VEHICLE_LOAN"))
            If vlPreviousAddressThresholdNode IsNot Nothing AndAlso Common.SafeString(vlPreviousAddressThresholdNode.Value) <> "" Then
                VLPreviousAddressThreshold = Common.SafeString(vlPreviousAddressThresholdNode.Value)
                VLPreviousAddressEnable = True
            End If
        End If
	End Sub

End Class
