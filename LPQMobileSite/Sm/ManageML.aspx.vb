﻿Imports System.Xml
Imports Sm
Imports LPQMobile.Utils

Partial Class Sm_ManageML

	Inherits SmApmBasePage
	Protected Property ContactInfo As String = "Please contact your site administrator at 555-555-5555."
	Protected Property OnlineApplicationPrivacyPolicy As String = "This is the privacy policy for the LQB App."
	Protected Property FullApplicationSubmissionConfirmation As String = "This is the confirmation message for the LQB app submission."
	Protected Property SignedOutPageMessage As String
	Protected Property GetRatesPageDisclaimer As String = "This is the disclaimer for the LQB Pricer."
	Protected Property EligibleLoanProgramDisclaimer As String = "This is the default disclaimer for the new eligible loan program in the LQB Pricer."
	Protected Property NoEligibleLoanProgramDisclaimer As String = "This is the default disclaimer for the no loan program in the LQB Pricer."

	Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
		If Not Page.IsPostBack Then
			Dim smMasterPage = CType(Me.Master, Sm_SiteManager)
			smMasterPage.LenderConfigXml = LenderConfigXml
			smMasterPage.RevisionID = LenderConfig.RevisionID
			smMasterPage.LastModifiedBy = LenderConfig.LastModifiedByUserFullName
			smMasterPage.LastModifiedByUserID = LenderConfig.LastModifiedByUserID
			smMasterPage.SelectedMainMenuItem = "manage"
			smMasterPage.SelectedSubMenuItem = "lqb"
			smMasterPage.CurrentNodeGroup = SmSettings.NodeGroup.ManageLQB
			smMasterPage.BreadCrumb = "Manage / Mortgage Loan"

			'Set the default SignedOutPageMessage here because it depends on the SmApmBasePage
			SignedOutPageMessage = "You have been successfully logged out. If you didn't mean to log out, you can log back in again <a href=""/Account/Login?lenderref=" + LenderRef + """>here</a>."

			Dim contactInfoNode As XmlNode = GetNode(LQB_MESSAGES_CONTACT_INFO)
			If contactInfoNode IsNot Nothing AndAlso contactInfoNode.NodeType <> XmlNodeType.Comment Then
				ContactInfo = Common.SafeString(contactInfoNode.InnerText)
			End If

			Dim eligibleLoanProgramDisclaimerNode As XmlNode = GetNode(LQB_MESSAGES_ELIGIBLE_LOAN_PROGRAM_DISCLAIMER())
			If eligibleLoanProgramDisclaimerNode IsNot Nothing AndAlso eligibleLoanProgramDisclaimerNode.NodeType <> XmlNodeType.Comment Then
				EligibleLoanProgramDisclaimer = Common.SafeString(eligibleLoanProgramDisclaimerNode.InnerText)
			End If
			Dim fullAppSubmitConfirmationNode As XmlNode = GetNode(LQB_MESSAGES_FULL_APP_SUBMIT_CONFIRMATION())
			If fullAppSubmitConfirmationNode IsNot Nothing AndAlso fullAppSubmitConfirmationNode.NodeType <> XmlNodeType.Comment Then
				FullApplicationSubmissionConfirmation = Common.SafeString(fullAppSubmitConfirmationNode.InnerText)
			End If
			Dim signedOutPageMessageNode As XmlNode = GetNode(LQB_MESSAGES_SIGNED_OUT_PAGE_MESSAGE())
			If signedOutPageMessageNode IsNot Nothing AndAlso signedOutPageMessageNode.NodeType <> XmlNodeType.Comment Then
				SignedOutPageMessage = Common.SafeString(signedOutPageMessageNode.InnerText)
			End If
			Dim getRatesPageDisclaimerNode As XmlNode = GetNode(LQB_MESSAGES_GET_RATES_PAGE_DISCLAIMER())
			If getRatesPageDisclaimerNode IsNot Nothing AndAlso getRatesPageDisclaimerNode.NodeType <> XmlNodeType.Comment Then
				GetRatesPageDisclaimer = Common.SafeString(getRatesPageDisclaimerNode.InnerText)
			End If
			Dim noEligibleLoanProgramDisclaimerNode As XmlNode = GetNode(LQB_MESSAGES_NO_ELIGIBLE_LOAN_PROGRAM_DISCLAIMER())
			If noEligibleLoanProgramDisclaimerNode IsNot Nothing AndAlso noEligibleLoanProgramDisclaimerNode.NodeType <> XmlNodeType.Comment Then
				NoEligibleLoanProgramDisclaimer = Common.SafeString(noEligibleLoanProgramDisclaimerNode.InnerText)
			End If
			Dim onlineAppPrivacyPolicyNode As XmlNode = GetNode(LQB_MESSAGES_ONLINE_APP_PRIVACY_POLICY())
			If onlineAppPrivacyPolicyNode IsNot Nothing AndAlso onlineAppPrivacyPolicyNode.NodeType <> XmlNodeType.Comment Then
				OnlineApplicationPrivacyPolicy = Common.SafeString(onlineAppPrivacyPolicyNode.InnerText)
			End If

		End If
	End Sub

End Class
