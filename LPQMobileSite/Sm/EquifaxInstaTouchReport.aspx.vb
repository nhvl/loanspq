﻿
Partial Class Sm_EquifaxInstaTouchReport
	Inherits SmApmBasePage

	Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        Dim instaTouchShowReportsNode = LenderConfigXml.SelectSingleNode("WEBSITE/VISIBLE/@APM_instatouch_reports_N")
        If instaTouchShowReportsNode Is Nothing OrElse instaTouchShowReportsNode.Value.ToUpper() <> "Y" Then
			Response.Redirect(String.Format("~/Sm/Index.aspx?lenderconfigid={0}", LenderConfigID), True)
		End If
		If Not Page.IsPostBack Then
			Dim smMasterPage = CType(Me.Master, Sm_SiteManager)
			smMasterPage.LenderConfigXml = LenderConfigXml
			smMasterPage.RevisionID = LenderConfig.RevisionID
			smMasterPage.LastModifiedBy = LenderConfig.LastModifiedByUserFullName
			smMasterPage.LastModifiedByUserID = LenderConfig.LastModifiedByUserID
			smMasterPage.SelectedMainMenuItem = "report"
			smMasterPage.SelectedSubMenuItem = "reportequifax"
			smMasterPage.CurrentNodeGroup = SmSettings.NodeGroup.ReportEquifaxInstatouch
			smMasterPage.ShowPreviewButton = False
			smMasterPage.ShowSaveDraftButton = False
			smMasterPage.ShowPublishButton = False
		End If
	End Sub
End Class