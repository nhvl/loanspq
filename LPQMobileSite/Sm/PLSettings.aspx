﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="PLSettings.aspx.vb" Inherits="Sm_PLSettings" MasterPageFile="SiteManager.master"  %>

<asp:Content runat="server" ContentPlaceHolderID="plBody">
	<div>
		<h1 class="page-header">Settings</h1>
		<p>Configure settings for your Application Portal</p>
		<section>
			<ul class="nav nav-tabs" id="mainTabs" role="tablist">
				<li role="presentation"><a href="<%=BuildUrl("/sm/generalsettings.aspx")%>" role="tab">General</a></li>
				<li role="presentation" <%=IIf(EnableXA, "", "class='hidden'")%>><a href="<%=BuildUrl("/sm/xasettings.aspx")%>" role="tab">XA</a></li>
				<li role="presentation" <%=IIf(EnableCC Or EnableHE Or EnablePL Or EnableVL, "", "class='hidden'")%>><a href="<%=BuildUrl("/sm/loanssettings.aspx")%>" role="tab">All Loans</a></li>
				<li role="presentation" <%=IIf(EnableCC, "", "class='hidden'")%>><a href="<%=BuildUrl("/sm/ccsettings.aspx")%>" role="tab">Credit Card Loan</a></li>
				<li role="presentation" <%=IIf(EnableHE, "", "class='hidden'")%>><a href="<%=BuildUrl("/sm/hesettings.aspx")%>" role="tab">Home Equity Loan</a></li>
				<li role="presentation" <%=IIf(EnableLQB, "", "class='hidden'")%>><a href="<%=BuildUrl("/sm/mlsettings.aspx")%>" role="tab">Mortgage Loan</a></li>
				<li role="presentation" class="active"><a href="#" aria-controls="plloans" role="tab">Personal Loan</a></li>
				<li role="presentation" <%=IIf(EnableVL, "", "class='hidden'")%>><a href="<%=BuildUrl("/sm/vlsettings.aspx")%>" role="tab">Vehicle Loan</a></li>
				<li role="presentation" <%=IIf(EnableBL, "", "class='hidden'")%>><a href="<%=BuildUrl("/sm/blsettings.aspx")%>" role="tab">Business Loan</a></li>
			</ul>
			<div class="tab-content">
				<div role="tabpanel" class="tab-pane active" id="plloans">
					<h2 class="section-title">Personal Loans</h2>
					<p>These settings will only affect Personal Loan applications.</p>
					<div class="bottom30">
						<div class="group-heading">IN-BRANCH CONFIGURE SITE</div>						
						<%--<div class="input-block">
							<h3 class="property-title">Booking</h3>
							<p>Book application to core <br/>(If interested, please contact Support via support@loanspq.com. SOW is required.)</p>
							<select id="Select2" class="form-control inline-select" disabled="disabled">
								<option value="" <%=BindSelectbox(PLBooking, "")%>>Not set</option>
								<% For Each item As KeyValuePair(Of String, String) In SmSettings.BookList%>
								<option <%=BindSelectbox(PLBooking, item.Key)%> value="<%=item.Key%>"><%=item.Value%></option>
								<%Next%>
							</select>
												
						</div>
						
						<div class="input-block">
								<h3 class="property-title">Auto Cross Qualify</h3>
								<p>Allow the system to show other products that applicant may qualify</p>
								<label class="toggle-btn" data-on="ON" data-off="OFF">
									<input type="checkbox" id="chkPLXSell" value="" <%=BindCheckbox(PLXSell)%>/>
								<span class="button-checkbox"></span>
							</label>							
						</div>--%>
						
						<div class="input-block">
								<h3 class="property-title">In-Session DocuSign</h3>
								<p>Show document for applicant to sign <br />(This must be enabled with credentials for in-branch.)</p>
								<label class="toggle-btn" data-on="ON" data-off="OFF">
									<input type="checkbox" id="chkPLDocuSign" value="" <%=BindCheckbox(PLDocuSign)%>/>
								<span class="button-checkbox"></span>
							</label>							
						</div>

						<div class="input-block">
							<h3 class="property-title">In-Session DocuSign PDF Group Override</h3>
							<p>System uses this PDF group instead of the default.</p>
							<input type="text" class="form-control" id="txtPLDocuSignGroup" value="<%=PLDocuSignPDFGroup%>" />							
						</div>
					</div>
					<div class="bottom30">
						<div class="group-heading">WORKFLOWS</div>
						<div class="input-block">
							<h3 class="property-title">Joint Applicants</h3>
							<p>Allow your applicants to apply for a joint loan.</p>
							<label class="toggle-btn" data-on="ON" data-off="OFF">
								<input type="checkbox" id="chkPlJointEnable" value="" <%=BindCheckbox(PLJointEnable)%>/>
								<span class="button-checkbox"></span>
							</label>
						</div>
					</div>
					<div class="bottom30">
						<div class="group-heading">MISC VALUES</div>
						<div class="input-block">
							<h3 class="property-title">Interest Rate</h3>
							<p>Estimated payment calculator default rate.</p>
							<input type="text" class="form-control inline-textbox number float-number" id="txtPLRate" value="<%=PLInterestRate%>" /><span> %</span>
						</div>
					</div>
                    <div class="bottom30">
                       <div class="input-block">
							<h3 class="property-title">Location Pool Filter</h3>
							<p>Show available product(s) based on zip code. Zip pools and applicable settings are configured in the in-branch settings.</p>
							<label class="toggle-btn" data-on="ON" data-off="OFF">
								<input type="checkbox" id="chkPLLocationPool" value="" <%=BindCheckbox(PLLocationPool)%>/>
								<span class="button-checkbox"></span>
							</label>
						</div>
                    </div>

                    <div class="bottom30">
                       <div class="input-block">
							<h3 class="property-title">Previous Address</h3>
							<p>Require previous address if duration at current address does not meet a specified minimum duration.</p>
							<label class="toggle-btn" data-on="ON" data-off="OFF">
								<input type="checkbox" id="chkPLPreviousAddress" value="" <%=BindCheckbox(PLPreviousAddressEnable)%>/>
								<span class="button-checkbox"></span>
							</label>
						</div>
                       <div class="input-block <%=IIf(PLPreviousAddressEnable, "", "hidden")%>">						
						    <p>Minimum Duration (months)</p>
						    <input type="text" class="form-control number" id="txtPLPreviousAddressThreshold" value="<%=PLPreviousAddressThreshold%>" maxlength ="3" />							
					    </div>
                    </div>
                   
                     <div class="bottom30">
                       <div class="input-block">
							<h3 class="property-title">Previous Employment</h3>
							<p>Require previous employment if duration at current employment does not meet a specified minimum duration.</p>
							<label class="toggle-btn" data-on="ON" data-off="OFF">
								<input type="checkbox" id="chkPLPreviousEmployment" value="" <%=BindCheckbox(PLPreviousEmploymentEnable)%>/>
								<span class="button-checkbox"></span>
							</label>
						</div>
                       <div class="input-block <%=IIf(PLPreviousEmploymentEnable, "", "hidden")%>" >						
						    <p>Minimum Duration (months)</p>
						    <input type="text" class="form-control number" id="txtPLPreviousEmploymentThreshold" value="<%=PLPreviousEmploymentThreshold%>" maxlength="3"/>							
					   </div>
                    </div>
                   
					<div data-name="Declarations">
						<div class="group-heading">DECLARATIONS</div>
						<div class="input-block">
							<p>Enable declarations to display in the Personal Loan application.</p>
							<%If DeclarationList IsNot Nothing AndAlso DeclarationList.Count > 0 Then%>
							<%	 
								Dim index As Integer = 0
								For Each item In DeclarationList
									index += 1
									%>
							<div class="checkbox-btn-wrapper">
								<span><%=String.Format("{0}. {1}", index, item.Value.DisplayText)%></span>
								<label class="toggle-btn pull-right" data-on="ON" data-off="OFF">
									<input type="checkbox" value="<%=item.Key%>" <%=IIf(item.Value.Active, "checked='checked'", "")%>/>
									<span class="button-checkbox"></span>
								</label>
							</div>
							<%Next%>
							<%End If%>
						</div>
					</div>
				</div>
			</div>
		</section>
	</div>
</asp:Content>
<asp:Content runat="server" ContentPlaceHolderID="plScripts">
	<script type="text/javascript">
		CKEDITOR.disableAutoInline = true;
		$(function () {
			registerDataValidator();
			$("select,input:checkbox").on("change", function () {
				master.FACTORY.documentChanged(this);
			});
			$(".html-editor").ckEditor({
				onContentChanged: function (editor) { master.FACTORY.documentChanged(editor); },
				onCommandClicked: function (editor, commandBtn) { master.FACTORY.documentChanged(editor); }
			});
			$("input:text").each(function (idx, ele) {
				$(ele).on({
					keypress: function () { master.FACTORY.documentChanged(ele); },
					paste: function () { master.FACTORY.documentChanged(ele); },
					cut: function () { master.FACTORY.documentChanged(ele); }
				});
            });
            //show minimum address duration input field only when the chkPreviousAddress is on 
            $("#chkPLPreviousAddress").on("change", function () {
                var $self = $(this);
                var $previousAddressThreshold = $('#txtPLPreviousAddressThreshold');
                if ($self.is(":checked")) {
                    $previousAddressThreshold.closest('div').removeClass('hidden');
                } else {
                    //clear and hide the field
                    $previousAddressThreshold.val(""); 
                    $previousAddressThreshold.closest('div').addClass('hidden');
                }
            });
            
            //show minimum employment duration input field only when the chkPreviousEmployement is on 
            $("#chkPLPreviousEmployment").on("change", function () {
                var $self = $(this);
                var $previousEmploymentThreshold = $('#txtPLPreviousEmploymentThreshold');
                if ($self.is(":checked")) {
                    $previousEmploymentThreshold.closest('div').removeClass('hidden');
                } else {
                    //clear and hide the field
                    $previousEmploymentThreshold.val(""); 
                    $previousEmploymentThreshold.closest('div').addClass('hidden');
                }
            });
            
		});
		(function (master, $, undefined) {
			master.FACTORY.SaveDraft = function (revisionId) {
				if ($.smValidate("ValidateData") == false) {
					_COMMON.noty("error", "Error: please check all fields.", 500);
					return;
				}

				var dataObj = collectSubmitData();
				dataObj.revisionId = revisionId;
				dataObj.command = 'saveplsettings';
				$.ajax({
					url: "/sm/smhandler.aspx",
					async: true,
					cache: false,
					type: 'POST',
					dataType: 'html',
					data: dataObj,
					success: function (responseText) {
						var response = $.parseJSON(responseText);
						if (response.IsSuccess) {
							master.FACTORY.onSaveDraftSuccess();
						} else {
							_COMMON.noty("error", "Error", 500);
						}
					}
				});
			};
			master.FACTORY.Preview = function (revisionId) {
				if ($.smValidate("ValidateData") == false) {
					_COMMON.noty("error", "Error: please check all fields.", 500);
					return;
				}
				var dataObj = collectSubmitData();
				dataObj.revisionId = revisionId;
				dataObj.command = 'previewplsettings';
				$.ajax({
					url: "/sm/smhandler.aspx",
					async: true,
					cache: false,
					type: 'POST',
					dataType: 'html',
					data: dataObj,
					success: function (responseText) {
						var response = $.parseJSON(responseText);
						if (response.IsSuccess) {
							master.FACTORY.onPreviewSuccess();
							_COMMON.openInNewTab(response.Info.previewurl);
						} else {
							_COMMON.noty("error", "Error", 500);
						}
					}
				});
			};
			master.FACTORY.validateBeforePublish = function () {
				var result = $.smValidate("ValidateData");
				if (result == false) {
					_COMMON.noty("error", "Error: please check all fields.", 500);
				}
				return result;
			}
			master.FACTORY.Publish = function (comment, publishAll) {
				if ($.smValidate("ValidateData") == false) {
					_COMMON.noty("error", "Error: please check all fields.", 500);
					return;
				}
				var dataObj = collectSubmitData();
				dataObj.command = 'publishplsettings';
				dataObj.comment = comment;
				dataObj.publishAll = publishAll;
				$.ajax({
					url: "/sm/smhandler.aspx",
					async: true,
					cache: false,
					type: 'POST',
					dataType: 'html',
					data: dataObj,
					success: function (responseText) {
						var response = $.parseJSON(responseText);
						if (response.IsSuccess) {
							master.FACTORY.onPublishSuccess();
						} else {
							_COMMON.noty("error", "Error", 500);
						}
					}
				});
			};
		}(window.master = window.master || {}, jQuery));
		function registerDataValidator() {
			$.smValidate.removeValidationGroup("ValidateData");
			$("input.float-number").each(function (idx, input) {
				$(input).observer({
					validators: [
						function (partial) {
							var $self = $(this);
							if (/^[0-9]+(\.[0-9]+)*$/.test($self.val()) == false) {
								return "Invalid data";
							}
							return "";
						}
					],
					validateOnBlur: true,
					group: "ValidateData"
				});
            });
            //validate minimum employment duration
            $('#txtPLPreviousEmploymentThreshold').observer({
					validators: [
						function (partial) {
                            var $self = $(this);
                            if ($self.closest('div').hasClass('hidden')) return ""; //skip validating if the field is hidden
							if (($self.val().trim() !="" && /^[0-9]+$/.test($self.val()) == false) ||parseInt($self.val()) >= 120) {
								return "Invalid minimum duration";
						    }
							return "";
						}
					],
					validateOnBlur: true,
					group: "ValidateData"
            });
             //validate minimum address duration
            $('#txtPLPreviousAddressThreshold').observer({
					validators: [
						function (partial) {
                            var $self = $(this);
                            if ($self.closest('div').hasClass('hidden')) return ""; //skip validating if the field is hidden
							if (($self.val().trim() !="" && /^[0-9]+$/.test($self.val()) == false) ||parseInt($self.val()) >= 120) {
								return "Invalid minimum duration";
						    }
							return "";
						}
					],
					validateOnBlur: true,
					group: "ValidateData"
			});
		}
		function collectSubmitData() {
			var result = {};
			result.lenderconfigid = '<%=LenderConfigID%>';
			//result.pl_xsell = $("#chkPLXSell").is(":checked");
			result.pl_docusign = $("#chkPLDocuSign").is(":checked");
			result.pl_docusign_group = $("#txtPLDocuSignGroup").val();
			result.pl_interest_rate = parseFloat($("#txtPLRate").val());
            result.pl_joint_enable = $("#chkPlJointEnable").is(":checked");
            result.pl_location_pool = $("#chkPLLocationPool").is(":checked");
            result.pl_previous_employment_threshold = $("#txtPLPreviousEmploymentThreshold").val();
            result.pl_previous_address_threshold = $("#txtPLPreviousAddressThreshold").val();
            result.pl_previous_address_enable = $('#chkPLPreviousAddress').is(":checked");
            result.pl_previous_employment_enable = $('#chkPLPreviousEmployment').is(":checked");
            var activeDeclarationList = [];
            $(".checkbox-btn-wrapper input[type='checkbox']:checked", "div[data-name='Declarations']").each(function (idx, ele) {
            	activeDeclarationList.push($(ele).val());
            });
            result.pl_declarations = JSON.stringify(activeDeclarationList);
			return result;
		}
	</script>
</asp:Content>
<asp:Content runat="server" ContentPlaceHolderID="plActionButtons"></asp:Content>