﻿
Partial Class Sm_ManageLenders
	Inherits SmBasePage

	Protected Sub Page_PreLoad(sender As Object, e As EventArgs) Handles Me.PreLoad
		Dim smMasterPage = CType(Me.Master, Sm_MasterPage)
		smMasterPage.SelectedMainMenuItem = "lender"
		AllowedRoles.Add(SmSettings.Role.SuperUser.ToString())
		AllowedRoles.Add(SmSettings.Role.LenderAdmin.ToString())
		AllowedRoles.Add(SmSettings.Role.PortalAdmin.ToString())
		AllowedRoles.Add(SmSettings.Role.VendorGroupAdmin.ToString())
		AllowedRoles.Add(SmSettings.Role.SSOUser.ToString())
		NotAllowedAccessRedirectUrl = "/Sm/Default.aspx"
	End Sub
End Class
