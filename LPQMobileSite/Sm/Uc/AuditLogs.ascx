﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="AuditLogs.ascx.vb" Inherits="Sm_Uc_AuditLogs" %>
<script src="js/tablefilter/tablefilter.js"></script>
<script type="text/javascript">
    var tfConfig = {
        base_path: 'js/tablefilter/',
        filters_cell_tag: 'th',
        themes: [{ name: 'transparent' }],
        popup_filters: {
            image_html: '<span class="filter-icon"></span>'
        },
        mark_active_columns:true,
        loader:false,
        responsive: true,
        sticky_headers: false,
        no_results_message:{
            content: 'There are no results that match your search.'
        },
        case_sensitive: false,
        col_0:"checklist",
        col_2:"none",
        col_3:"none",
        col_types: [
            'string',
            { type: 'date', locale: 'en-US' }
        ],
        on_after_filter: function(o){
            $('table.fixed-table-audit-log').toggleClass("min-height-300", tf.filteredData().length != 0);
            for(var i = 0; i < o.fltIds.length; i++) {
                $('div[id^="popup_tf_"][id$="_'+i+'"] a').toggleClass("disabled", tf.getFilterValue(i) == "");
            }
        },
        on_filters_loaded: function(tf){
            //Custom User filter
			var userPopup = document.querySelector('div[id^="popup_tf_"][id$="_0"]');
            if(userPopup == null) return;
			const $clearFilterHtml =
				$("<div />", { class: "clearUserFilter" }).append(
					$("<a />", { onclick: "onClearUserFilterClick();", id: "clearUserFilter" }).text("Clear Filter")
				);
			userPopup.insertBefore($clearFilterHtml[0], userPopup.firstChild);
            document.querySelector('input[id^="flt0_tf_"][id$="_0"]').closest("li").style.display = 'none';
            
            //Custom Date filter
            var maxDate = new Date();
            var minDate = new Date();
            var minHistoryDate = new Date();
            minHistoryDate.setFullYear(minHistoryDate.getFullYear() - _COMMON_FILTER_MAX_TIME_SPAN);
            minDate.setDate(maxDate.getDate()-_COMMON_FILTER_DATE_TIME_SPAN);
            setFilterDateRange(minDate, maxDate);

            var datePopup = document.querySelector('div[id^="popup_tf_"][id$="_1"]');
            if(datePopup == null) return;
            $('div[id^="popup_tf_"][id$="_1"]').append(
                "<div class='dateFilter'><div><a id='clearDateFilter'>Clear Filter</a></div>"
                +"<label class='pt20'>Start Date</label>"
                +"<div class='input-group'>"
                    +"<input type='text' id='from' name='from' class='form-control'>"
                    +"<label class='input-group-addon btn' for='from'>"
                        +"<i class='fa fa-calendar'></i>"
                    +"</label></div>"
                +"<label class='pt20'>End Date</label>"
                +"<div class='input-group'>"
                    +"<input type='text' id='to' name='to' class='form-control'>"
                    +"<label class='input-group-addon btn' for='to'>"
                        +"<i class='fa fa-calendar'></i>"
                    +"</label></div></div>");

            //Hide default input of filter date. This input will be filled up based on Start and End Date.
            document.querySelector('div[id^="popup_tf_"][id$="_1"] input[ct="1"]').style.display = 'none';

            $("#from").datepicker({startDate: '-1y', endDate: '+0d', autoclose: true,}).datepicker("setDate", minDate).on("changeDate", function(selected){
                    var startDate = selected.date 
                        ? new Date(Math.max.apply(null, [new Date(selected.date.valueOf()), minHistoryDate])) 
                        : "";
                    if(startDate!="" && startDate.valueOf() != selected.date.valueOf()){
                        $("#from").datepicker("setDate", startDate);
                    }
                    $("#to").datepicker('setStartDate', startDate);
                    setFilterDateRange($("#from").val(), $("#to").val());
                    reOpenDateFilter();
                });
            $("#to").datepicker({startDate: '-1y', endDate: '+1d', autoclose: true,}).datepicker("setDate", maxDate).on("changeDate", function (selected) {
                    var endDate = selected.date 
                        ? new Date(Math.min.apply(null, [new Date(selected.date.valueOf()), new Date()])) 
                        : "";
                    if(endDate!="" && endDate.valueOf() != selected.date.valueOf()){
                        $("#to").datepicker("setDate", endDate);
                    }
                    $("#from").datepicker('setEndDate', endDate == "" ? new Date() : endDate);
                    setFilterDateRange($("#from").val(), $("#to").val());
                    reOpenDateFilter();
                });
            $("#clearDateFilter").click(function(){
                $("#to").datepicker("setDate", "");
                $("#from").datepicker("setDate", "");
            });
            function reOpenDateFilter(){
                document.querySelector('span[ci="1"] span.filter-icon').click();
            }
            function setFilterDateRange(start, end) {
                tf.setFilterValue(1, 
                    ((end != "") 
                        ? ("<" + changeDate(1, end))
                        : "")
                    + ((start != "" && end != "" ) ? "&&" : "" )
                    + ((start != "") 
                        ? (">=" + new Date(start).toLocaleDateString())
                        : ""));
                tf.filter();
            }
            function changeDate(changeAmount, dateString){
                var date = dateString ? new Date(dateString) : new Date();
                date.setDate(date.getDate() + changeAmount);
                return date.toLocaleDateString();
            }
        },
        extensions: [{ name: 'sort'
        , types:['none',{ type: 'date', locale: 'en-US' },'none','none' ]
        , sort_col_at_start:[1, true]
        , on_after_sort: function(o, colIndex){
            for(var i = 0; i < o.fltIds.length; i++) 
            {
                if($(o.getHeaderElement(i)).find("img.ascending").length!=0){
                    $(o.getHeaderElement(i)).toggleClass("sort-descending", false);
                    $(o.getHeaderElement(i)).toggleClass("sort-ascending", true);
                    continue;
                }
                if($(o.getHeaderElement(i)).find("img.descending").length!=0){
                    $(o.getHeaderElement(i)).toggleClass("sort-descending", true);
                    $(o.getHeaderElement(i)).toggleClass("sort-ascending", false);
                    continue;
                }
                $(o.getHeaderElement(i)).toggleClass("sort-descending", false);
                $(o.getHeaderElement(i)).toggleClass("sort-ascending", false);
            };
        }        
        }]
    }
    function onClearUserFilterClick(){
        tf.setFilterValue(0, "");
        tf.filter();
    }
    function onSearch(value){
        if(value=="") 
            document.getElementById("clearSearch").style.visibility = "hidden";
        else 
            document.getElementById("clearSearch").style.visibility = "visible";
        if((value.match(new RegExp("\"", "g")) || []).length % 2 != 0){
            alert('Unmatched quotation mark found.');
            return;
        }
        var newString = value.replace(/\s+/g,' ').trim();
        var quotationMarkArray = newString.match(/(?:[^\s"]+|"[^"]*")+/g);
        if(quotationMarkArray){
            newString = quotationMarkArray.join('&&').replace(/['"]+/g, '');    
        }
        else{
            newString = newString.split(' ').join('&&');
        }
        tf.setFilterValue(4, newString);
        tf.filter();
    }
    function onKeyDownEvent(event){
        if(event.keyCode == 13)
            onSearch($("#txtHistorySearch").val());
    }
    function onHistorySearchClick(){
        onSearch($("#txtHistorySearch").val());
    }
    var tf = new TableFilter(document.querySelector('#loadauditlogs_dialog > table'), tfConfig);
    tf.init();
</script>
<div class="history-search-header">
    <div class="search-bar">
        <div class="input-group">
            <input id="txtHistorySearch" onkeydown="onKeyDownEvent(event)" type="text" class="form-control" placeholder="Search modification history" />
            <span class="input-group-btn"><button onclick="onHistorySearchClick();" class="btn btn-secondary" type="button" id="btnHistorySearch">Search</button></span>
        </div>
    </div>
    <div class="link-clear-search">
        <a id="clearSearch" style="visibility: hidden;" onclick="$('#txtHistorySearch').val('');onSearch('');">Clear Search</a>    
    </div>
</div>
<table class="table table-hover fixed-table-audit-log" cellpadding="0" cellspacing="0">
    <thead>
        <tr>
            <th>User</th>
            <th>Modified Date</th>
            <th>Publish Notes</th>
            <th>Modifications</th>
            <th style="display: none">All Data</th>
        </tr>
    </thead>
    <%If LogList IsNot Nothing AndAlso LogList.Count > 0 Then%>
    <tbody>
    <asp:Repeater runat="server" ID="rptAuditLogs" >
        <ItemTemplate>
            <tr class='<%#If(Container.ItemIndex Mod 2 = 0, "", "alternativeRow")%>'>
                <td>
                    <%#Eval("LastModifiedByUserName") %>
                </td>
                <td nowrap="nowrap">
                    <%#CDate(Eval("LogTime")).ToString("MM/dd/yyyy hh:mm tt")%>
                </td>
                <td>
                    <%#Eval("Comment")%>
                </td>
                <td>
                    <%#Eval("AuditChanges")%>
                </td> 
                <td style="display: none">
                    <!--This column is used in txtHistorySearch to search all data in table-->
                    <%#Eval("LastModifiedByUserName") %> <%#CDate(Eval("LogTime")).ToString("MM/dd/yyyy hh:mm tt")%> <%#Eval("Comment")%> <%#Eval("AuditChanges")%>
                </td>                                   
            </tr>
        </ItemTemplate>
    </asp:Repeater>
    </tbody>
    <%Else%>
    <tfoot>
        <tr>
            <td colspan="3" style="text-align: center;">No item found</td>
        </tr>
    </tfoot>
    <%End If%>
</table>