﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="AddLandingPageForm.ascx.vb" Inherits="Sm_Uc_AddLandingPageForm" %>
<div style="padding: 20px; overflow: auto; max-height: 500px;">
<form id="frm_addlandingpage">
    <div class="row">
        <div class="col-12">
            <div class="form-group">
                <label class="required-field">Title</label>
                <input type="text" class="form-control" name="Title" id="txtTitle" placeholder="Title" data-message-require="Please fill out this field" data-required="true" maxlength="200" />
				<input type="hidden" name="LenderConfigID" id="hdLenderConfigID" value="<%=LenderConfigID%>"/>
            </div>
        </div>
		<div class="col-12">
            <div class="form-group">
                <label class="required-field">Home Page ID</label>
                <input type="text" class="form-control" name="RefID" id="txtRefID" placeholder="Home Page ID" data-message-require="Please fill out this field" data-required="true" maxlength="200" />
            </div>
        </div>
		<div class="col-12 <%=IIf(TemplateList.Count = 0, "hidden", "")%>">
            <div class="form-group">
                <label class="required-field">Template</label>
                <select class="form-control" name="Template" id="ddlTemplate" data-message-require="Please select template" data-required="true">
	                <option value="">Select template</option>
					<%For Each template In TemplateList%>
					<option value="<%=template.Value%>"><%=template.Key%></option>
					<%Next%>
                </select>
            </div>
        </div>
    </div>
</form>
</div>