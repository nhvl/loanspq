﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="UserList.ascx.vb" Inherits="Sm_Uc_UserList" %>
<%@Import Namespace="Sm" %>
<%@ Import Namespace="LPQMobile.Utils" %>
<%@ Register Src="~/Sm/Uc/Paging.ascx" TagPrefix="uc" TagName="GridFooter" %>
<table class="table table-hover user-list">
    <thead>
        <tr>
			<th>#</th>
			<th>User</th>
			<th>Roles</th>
			<th class="center">Last Login</th>
			<th class="center">Login Failed</th>
			<th class="center">Days Till Expire</th>
			<th class="center">Status</th>
			<th class="center">Enable</th>
			<th></th>
		</tr>
    </thead>
    <tbody>
        <%If UserList IsNot Nothing AndAlso UserList.Any() Then
        		Dim idx As Integer = (PagingInfo.PageIndex - 1) * PagingInfo.PageSize
        		Dim loginFailedCountExceed = Common.SafeInteger(ConfigurationManager.AppSettings("LoginFailedCountExceed"))
        		For Each usr As SmUser In UserList
        			idx = idx + 1
        			Dim userStatus = usr.Status
        			If userStatus = SmSettings.UserStatus.Active AndAlso (usr.LoginFailedCount >= loginFailedCountExceed OrElse (usr.LastLoginDate.HasValue AndAlso usr.LastLoginDate.Value.AddDays(usr.ExpireDays) < DateTime.Now)) Then
        				userStatus = SmSettings.UserStatus.Locked
        			End If
        %>
		 <tr>
				<td><%=idx%></td>
				<td>
					<div class="user-info">
						<div>
							<div class="avatar-wrapper"><span class="avatar" style="background-image: url('<%=IIf(String.IsNullOrEmpty(usr.Avatar), "/images/avatar.jpg", usr.Avatar)%>')"></span></div>
						</div>
						<div>
							<%If Not String.IsNullOrEmpty(usr.FullName.Trim()) Then%>
							<p><%=usr.FullName%></p>
							<%End If%>
							<p><i class="fa fa-envelope" aria-hidden="true"></i><span data-field="email"><%=usr.Email%></span></p>
							<%If Not String.IsNullOrEmpty(usr.Phone.Trim()) Then%>
							<p><i class="fa fa-phone" aria-hidden="true"></i> <%=Regex.Replace(usr.Phone, "(\d{3})(\d{3})(\d{4})", "$1-$2-$3")%></p>
							<%End If%>
						</div>
					</div>

					<input type="hidden" data-field="userid" value="<%=usr.UserID%>"/>
				</td>
				<td><%=String.Join(",", usr.Role.GetEnumDescription())%></td>
				<td class="center">
					<%If usr.LastLoginDate.HasValue Then%>
					<%=usr.LastLoginDate.Value.ToString("MM/dd/yyyy HH:mm:ss")%>
					<%Else %>
					-
					<%End If%>
				</td>
				<td class="center"><%=usr.LoginFailedCount%></td>
				<td class="center">
					<%=usr.DaysTillExpire%>
				</td>
				<td class="center"><%=userStatus.GetEnumDescription()%></td>
				<td class="center">
					<%If usr.Status = SmSettings.UserStatus.Active Or usr.Status = SmSettings.UserStatus.Disabled Then%>
					<label class="toggle-btn" data-on="ON" data-off="OFF">
						<input type="checkbox" data-command="enable-btn" <%=SmUtil.BindCheckbox(IIf(usr.Status = SmSettings.UserStatus.Active, True, False))%>/>
						<span class="button-checkbox"></span>
					</label>
					<%End If%>
				</td>
				<td style="text-align: right;">
					<a href="#" data-command="resetRetry">
						<span>Reset Retry</span>
					</a>
					<%If usr.Status <> SmSettings.UserStatus.InActive Then%>
					<span>|</span>
					<a href="#" data-command="edit">
						<span>Edit</span>
					</a>
					<%End If %>
					<span>|</span>
					<a href="#" data-command="delete">
						<span>Delete</span>
					</a>
				</td>
			</tr>
        <%
        	Next
	End If
        %>
    </tbody>
    <tfoot>
        <tr>
            <td colspan="10">
                <uc:GridFooter ID="ucGridFooter" runat="server"></uc:GridFooter>
            </td>
        </tr>
    </tfoot>
</table>
