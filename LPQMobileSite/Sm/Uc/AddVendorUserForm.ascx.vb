﻿
Imports LPQMobile.Utils

Partial Class Sm_Uc_AddVendorUserForm
	Inherits SmBaseUserControl

	Public Property AvailableRoles As New List(Of SmSettings.Role)
	'Protected Usr As SmUser = Nothing
	Protected LenderVendorList As List(Of SmLenderVendor)
	Public Property LenderConfig As SmLenderConfigBasicInfo
	Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
		If Not IsPostBack Then
			Try
				'Usr = smAuthBL.GetUserByEmail(Email)
				Dim pagingInfo As New SmPagingInfoModel With {.PageIndex = 0, .PageSize = 0} 'get all
				Dim filter As New SmVendorListFilter()
				filter.Status.Add(SmSettings.LenderVendorStatus.Active)
				filter.LenderID = LenderConfig.LenderID
				'If SmUtil.CheckRoleAccess(HttpContext.Current, String.Format("PortalScope${0}${1}${2}", SmSettings.Role.VendorAdmin.ToString(), LenderConfig.LenderID.ToString(), LenderConfig.LenderConfigID.ToString())) Then
				'	'when VendorAdmin create and tie an user to specified vendor, restrict that vendors to those that VendorAdmin tied to
				'	'For example: userA is a VendorAdmin and being tied to vendor1. Then, userA creates/tie an userB - VendorUser. There is only one option that userA can select is vendor1.
				'	Dim f = New SmVendorUserRoleListFilter()
				'	f.LenderConfigID = LenderConfig.LenderConfigID
				'	f.Roles.Add(SmSettings.Role.VendorAdmin)
				'	Dim userRoleList = smBL.GetVendorUserRoles(pagingInfo, f).Where(Function(r) r.UserID = CurrentUserInfo.UserID AndAlso r.Status = SmSettings.UserRoleStatus.Active).ToList()
				'	If userRoleList IsNot Nothing AndAlso userRoleList.Any() Then
				'		filter.VendorIDList = userRoleList.Select(Function(p) p.VendorID).ToList()
				'	Else
				'		filter.VendorIDList.Add(Guid.NewGuid().ToString())
				'	End If
				'End If
				If SmUtil.CheckRoleAccess(HttpContext.Current, String.Format("PortalScope${0}${1}${2}", SmSettings.Role.VendorGroupAdmin.ToString(), LenderConfig.LenderID.ToString(), LenderConfig.LenderConfigID.ToString())) Then
					Dim userRole = smBL.GetUserRolesByUserID(CurrentUserInfo.UserID)
					If userRole IsNot Nothing Then
						Dim vendorGroupAdminRole = userRole.FirstOrDefault(Function(r) r.Role = SmSettings.Role.VendorGroupAdmin AndAlso r.LenderConfigID = LenderConfig.LenderConfigID)
						If vendorGroupAdminRole IsNot Nothing Then
							filter.FilterByTags = True
							filter.TagList = vendorGroupAdminRole.AssignedVendorGroupByTagList
						End If
					End If
				End If
				LenderVendorList = smBL.GetLenderVendors(pagingInfo, filter)
			Catch ex As Exception
				log.Error("Could not load AddVendorUserForm", ex)
			End Try
		End If
	End Sub
End Class
