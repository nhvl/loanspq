﻿
Partial Class Sm_Uc_VendorUserList
	Inherits SmBaseUserControl

	Protected UserRoleList As List(Of SmUserRole)
	Public Property PagingInfo As New SmPagingInfoModel
	Public Property FilterInfo As New SmVendorUserRoleListFilter
	Public Property LenderConfig As SmLenderConfigBasicInfo

	Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
		If Not IsPostBack Then
			Try
				'If SmUtil.CheckRoleAccess(HttpContext.Current, SmSettings.Role.User) AndAlso SmUtil.CheckRoleAccess(HttpContext.Current, String.Format("PortalScope${0}${1}${2}", SmSettings.Role.VendorAdmin.ToString(), LenderConfig.LenderID.ToString(), LenderConfig.LenderConfigID.ToString())) Then
				'	'VendorAdmin can add users with role VendorUser only
				'	FilterInfo.Roles.Add(SmSettings.Role.VendorUser)
				'	'VendorAdmin can only see those VendorUser that tie to the vendor it tie
				'	Dim urList = smBL.GetActiveUserRolesByUserID(CurrentUserInfo.UserID)
				'	If urList IsNot Nothing Then
				'		Dim vIdList = (From userRole As SmUserRole In urList Where userRole.LenderConfigID = FilterInfo.LenderConfigID AndAlso Not String.IsNullOrEmpty(userRole.VendorID) Select userRole.VendorID).ToList()
				'		If vIdList IsNot Nothing AndAlso vIdList.Any() Then
				'			FilterInfo.VendorIDList = vIdList
				'		Else
				'			'prevent loading vendorIDs when the VendorAdmin role of current user is disabled
				'			FilterInfo.VendorIDList.Add(Guid.NewGuid().ToString())
				'		End If
				'	End If
				'End If



				If SmUtil.CheckRoleAccess(HttpContext.Current, String.Format("PortalScope${0}${1}${2}", SmSettings.Role.VendorGroupAdmin.ToString(), LenderConfig.LenderID.ToString(), LenderConfig.LenderConfigID.ToString())) Then
					Dim userRole = smBL.GetUserRolesByUserID(CurrentUserInfo.UserID)
					If userRole IsNot Nothing Then
						Dim vendorGroupAdminRole = userRole.FirstOrDefault(Function(r) r.Role = SmSettings.Role.VendorGroupAdmin AndAlso r.LenderConfigID = LenderConfig.LenderConfigID)
						If vendorGroupAdminRole IsNot Nothing Then
							FilterInfo.FilterByTags = True
							FilterInfo.TagList = vendorGroupAdminRole.AssignedVendorGroupByTagList
						End If
					End If
				End If
				UserRoleList = smBL.GetVendorUserRoles(PagingInfo, FilterInfo)
				ucGridFooter.PagingInfo = PagingInfo
			Catch ex As Exception
				log.Error("Could not load vendor user role list", ex)
			End Try
		End If
	End Sub
End Class
