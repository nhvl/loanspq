﻿Imports Sm

Partial Public Class Sm_Uc_UserList
	Inherits SmBaseUserControl

	Protected UserList As List(Of SmUser)
	Public Property PagingInfo As SmPagingInfoModel
	Public Property FilterInfo As SmUserListFilter

	Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
		If Not IsPostBack Then
			Try
				UserList = smAuthBL.GetAllUsers(PagingInfo, FilterInfo)
				ucGridFooter.PagingInfo = PagingInfo
			Catch ex As Exception
				log.Error("Could not load user list", ex)
			End Try
		End If
	End Sub
End Class
