﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="EditLenderVendorForm.ascx.vb" Inherits="Sm_Uc_EditLenderVendorForm" %>
<%@ Import Namespace="Sm" %>
<div style="padding: 20px; overflow: auto; max-height: 500px;">
<form id="frm_editlendervendor">
    <div class="row">
        <div class="col-12">
            <div class="form-group">
                <label class="required-field">Vendor Name</label>
                <input type="text" class="form-control" name="VendorName" id="txtVendorName" value="<%=Vendor.VendorName%>" placeholder="Vendor Name" data-message-require="Please fill out this field" data-required="true" maxlength="50" />
				<input type="hidden" name="LenderVendorID" id="hdLenderVendorID" value="<%=Vendor.LenderVendorID%>"/>
            </div>
        </div>
		<div class="col-12">
            <div class="form-group">
                <label>Vendor Groups<span></span></label>
                <input type="text" class="form-control" name="Tags" id="txtTags" data-role="tagsinput" value="<%=Vendor.Tags%>" placeholder="Press enter after each vendor group to confirm" data-required="false" maxlength="500" />
            </div>
        </div>
		<div class="col-12">
            <div class="form-group">
                <label class="required-field">Type</label>
                <%--<select class="form-control" name="Type" id="ddlVendorType" data-message-require="Please select type" data-required="true">
	                <option value="">Please select</option>
					<%=SmUtil.RenderDropdownFromEnum(GetType(SmSettings.LenderVendorType), Vendor.Type.ToString())%>
                </select>--%>
				<div class="checkbox-group vendor-type-lst">
					<div class="checkbox-group-header">
						<div class="checkbox checkbox-success checkbox-inline">
							<input type="checkbox" name="Type[]" value="<%=SmSettings.LenderVendorType.CC.ToString()%>" id="chkLenderVendorType_<%=SmSettings.LenderVendorType.CC.ToString()%>" <%=BinLenderVendorTypeCheckbox(SmSettings.LenderVendorType.CC)%>/>
							<label for="chkLenderVendorType_<%=SmSettings.LenderVendorType.CC.ToString()%>"><%=SmSettings.LenderVendorType.CC.GetEnumDescription()%></label>
						</div>
						<div class="checkbox checkbox-success checkbox-inline">
							<input type="checkbox" name="Type[]" value="<%=SmSettings.LenderVendorType.CC_COMBO.ToString()%>" id="chkLenderVendorType_<%=SmSettings.LenderVendorType.CC_COMBO.ToString()%>" <%=BinLenderVendorTypeCheckbox(SmSettings.LenderVendorType.CC_COMBO)%>/>
							<label for="chkLenderVendorType_<%=SmSettings.LenderVendorType.CC_COMBO.ToString()%>"><%=SmSettings.LenderVendorType.CC_COMBO.GetEnumDescription()%></label>
						</div>
						<div class="checkbox checkbox-success checkbox-inline">
							<input type="checkbox" name="Type[]" value="<%=SmSettings.LenderVendorType.PL.ToString()%>" id="chkLenderVendorType_<%=SmSettings.LenderVendorType.PL.ToString()%>" <%=BinLenderVendorTypeCheckbox(SmSettings.LenderVendorType.PL)%>/>
							<label for="chkLenderVendorType_<%=SmSettings.LenderVendorType.PL.ToString()%>"><%=SmSettings.LenderVendorType.PL.GetEnumDescription()%></label>
						</div>
						<div class="checkbox checkbox-success checkbox-inline">
							<input type="checkbox" name="Type[]" value="<%=SmSettings.LenderVendorType.PL_COMBO.ToString()%>" id="chkLenderVendorType_<%=SmSettings.LenderVendorType.PL_COMBO.ToString()%>" <%=BinLenderVendorTypeCheckbox(SmSettings.LenderVendorType.PL_COMBO)%>/>
							<label for="chkLenderVendorType_<%=SmSettings.LenderVendorType.PL_COMBO.ToString()%>"><%=SmSettings.LenderVendorType.PL_COMBO.GetEnumDescription()%></label>
						</div>
						<div class="checkbox checkbox-success checkbox-inline">
							<input type="checkbox" name="Type[]" value="<%=SmSettings.LenderVendorType.VL.ToString()%>" id="chkLenderVendorType_<%=SmSettings.LenderVendorType.VL.ToString()%>" <%=BinLenderVendorTypeCheckbox(SmSettings.LenderVendorType.VL)%>/>
							<label for="chkLenderVendorType_<%=SmSettings.LenderVendorType.VL.ToString()%>"><%=SmSettings.LenderVendorType.VL.GetEnumDescription()%></label>
						</div>
						<div class="checkbox checkbox-success checkbox-inline">
							<input type="checkbox" name="Type[]" value="<%=SmSettings.LenderVendorType.VL_COMBO.ToString()%>" id="chkLenderVendorType_<%=SmSettings.LenderVendorType.VL_COMBO.ToString()%>" <%=BinLenderVendorTypeCheckbox(SmSettings.LenderVendorType.VL_COMBO)%>/>
							<label for="chkLenderVendorType_<%=SmSettings.LenderVendorType.VL_COMBO.ToString()%>"><%=SmSettings.LenderVendorType.VL_COMBO.GetEnumDescription()%></label>
						</div>
					</div>
				</div>
            </div>
        </div>
		<div class="col-12">
            <div class="form-group">
                <label class="required-field">Logo</label>
                <div class="logo-upload-zone dropzone" id="divVendorLogo" data-current-logo=""></div>
				<%If LogoFileInfo IsNot Nothing Then%>
				<input type="hidden" value="<%=Vendor.Logo%>" name="Logo" id="hdLogo" data-file-ext="<%=LogoFileInfo.Extension%>" data-file-name="<%=LogoFileInfo.Name%>" data-file-size="<%=LogoFileInfo.Length%>"/>
				<%Else%>
				<input type="hidden" value="<%=Vendor.Logo%>" name="Logo" id="hdLogo" data-file-ext="" data-file-name="filename" data-file-size="0"/>
				<%End If%>
            </div>
        </div>
		<div class="col-12">
            <div class="form-group">
                <label class="required-field">Address</label>
                <input type="text" class="form-control" name="Address" id="txtAddress" placeholder="Address" value="<%=Vendor.Address%>" data-message-require="Please fill out this field" data-required="true" maxlength="50" />
            </div>
        </div>
		<div class="col-12">
            <div class="form-group">
                <label class="required-field">City</label>
                <input type="text" class="form-control" name="City" id="txtCity" placeholder="City" value="<%=Vendor.City%>" data-message-require="Please fill out this field" data-required="true" maxlength="50" />
            </div>
        </div>
		<div class="col-12">
            <div class="form-group">
                <label class="required-field">State</label>
                <select class="form-control" name="State" id="ddlState" data-message-require="Please select state" data-required="true">
					<%=SmUtil.RenderDropdownStateOptions(Vendor.State)%>
                </select>
            </div>
        </div>
		<div class="col-12">
            <div class="form-group">
                <label class="required-field">Zip</label>
                <input type="text" class="form-control" name="Zip" id="txtZip" value="<%=Vendor.Zip%>" placeholder="Zip" data-message-require="Please fill out this field" data-required="true" maxlength="50" />
            </div>
        </div>
		<div class="col-12">
            <div class="form-group">
                <label class="required-field">Phone</label>
                <input type="text" class="form-control" name="Phone" id="txtPhone" value="<%=Vendor.Phone%>" data-message-invalid-data="Phone is invalid" data-format="phone" placeholder="Phone Number" maxlength="20" />
            </div>
        </div>
		<div class="col-12">
            <div class="form-group">
                <label>Fax</label>
                <input type="text" class="form-control" name="Fax" id="txtFax" placeholder="Fax" value="<%=Vendor.Fax%>"   maxlength="20" />
            </div>
        </div>
        <div class="col-12">
            <div class="form-group">
                <label>Default LPQ WorkerID</label>
                <input type="text" class="form-control" name="DefaultLpqWorkerID" id="txtDefaultLpqWorkerID" value="<%=Vendor.DefaultLpqWorkerID%>" maxlength="50" />
            </div>
        </div>
		<div class="col-12">
            <div class="form-group">
                <label class="required-field">LPQ Dealer Number or ClinicID</label>
                <input type="text" class="form-control" name="DealerNumberLpq" id="txtDealerNumberLpq" value="<%=Vendor.DealerNumberLpq%>" maxlength="50" />
            </div>
        </div>
    </div>
</form>
</div>