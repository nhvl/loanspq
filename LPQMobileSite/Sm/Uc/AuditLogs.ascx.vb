﻿
Imports System.Data
Imports LPQMobile.DBManager
Partial Class Sm_Uc_AuditLogs
	Inherits SmBaseUserControl

	Public Property LenderConfigId As Guid
	Protected LogList As List(Of CLenderConfigLog)
	Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
		If Not IsPostBack Then
			Try
				Dim oLender As New CLenderConfigManager()
                LogList = oLender.GetAuditLogs(LenderConfigId)
                '            If LogList.Count > 1 Then
                '                LogList = ConfigComparer(LogList)
                '            End If
                'If LogList.Count > 0 Then
                '	LogList.RemoveAt(LogList.Count - 1)
                'End If
                rptAuditLogs.DataSource = LogList
				rptAuditLogs.DataBind()

			Catch ex As Exception
				log.Error("Could not load audit logs", ex)
			End Try
		End If
	End Sub

    ''Function ConfigComparer(logList As List(Of CLenderConfigLog)) As List(Of CLenderConfigLog)
    ''	Dim idx As Integer = 0
    ''	For Each logItem As CLenderConfigLog In logList
    ''		idx += 1
    ''           If idx = logList.Count Then
    ''               Exit For
    ''           End If
    ''           Dim sbDiff As New StringBuilder()
    ''           Dim prevLogItem As CLenderConfigLog = logList.Item(idx)
    ''           Dim diffGramStr As String = ""
    ''           Dim xmlComparer As New CXmlComparer(prevLogItem.ConfigData, logItem.ConfigData)
    ''           If xmlComparer.Compare(diffGramStr) = False Then
    ''               xmlComparer.ParseDiffGram(diffGramStr, sbDiff)
    ''           End If
    ''           If sbDiff IsNot Nothing AndAlso sbDiff.Length > 0 Then
    ''               logItem.AuditChanges = sbDiff.ToString()
    ''           Else
    ''               logItem.AuditChanges = "No changes"
    ''           End If
    ''       Next
    ''	Return logList
    ''End Function

    Sub PropertyComparer(sourceValue As String, destValue As String, propName As String, ByRef sbDiff As StringBuilder)
		If sourceValue <> destValue Then
			sbDiff.AppendFormat("<p>{0}: {1} --> {2}</p>", propName, sourceValue, destValue)
		End If
	End Sub
End Class
