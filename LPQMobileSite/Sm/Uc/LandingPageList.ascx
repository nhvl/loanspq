﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="LandingPageList.ascx.vb" Inherits="Sm_Uc_LandingPageList" %>
<%@ Register Src="~/Sm/Uc/Paging.ascx" TagPrefix="uc" TagName="GridFooter" %>
<table class="table table-hover">
    <thead>
        <tr>
			<th>#</th>
			<th>HomepageID</th>
			<th>Title</th>
			<th>Last Modified</th>
			<th>Modified By</th>
			<th class="center">Active</th>
			<th>Actions</th>
		</tr>
    </thead>
    <tbody>
        <%If LandingPagesItems IsNot Nothing AndAlso LandingPagesItems.Any() Then
        		Dim idx As Integer = (PagingInfo.PageIndex - 1) * PagingInfo.PageSize
        		For Each page As SmLandingPageModel In LandingPagesItems
        			idx = idx + 1
        %>
		 <tr data-id="<%=page.LandingPageID%>">
			<td><%=idx%></td>
			<td><%=page.RefID%></td>
			<td><%=page.Title%></td>
			<td>
				<%If page.LastModifiedDate.HasValue Then%>
				<%=page.LastModifiedDate.Value.ToString("MM/dd/yyyy HH:mm")%>
				<%Else%>
				<%=page.CreatedDate.ToString("MM/dd/yyyy HH:mm")%>
				<%End If%>
			</td>
			 <td>
				<%If Not String.IsNullOrWhiteSpace(page.LastModifiedByUserWithEmail) Then%>
				<%=page.LastModifiedByUserWithEmail%>
				<%Else%>
				<%=page.CreatedByUserWithEmail%>
				<%End If%>
			</td>
			 <td class="center toggle-btn-container">
				<label class="toggle-btn" data-on="ON" data-off="OFF">
					<input type="checkbox" data-command="enable-btn" <%=SmUtil.BindCheckbox(IIf(page.Status = SmSettings.LandingPageStatus.Active, True, False))%>/>
					<span class="button-checkbox"></span>
				</label>
			</td>
			<td nowrap="nowrap">
				<div class="dropdown inline-drop-menu">
					<button class="btn dropdown-toggle btn-dropdown" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
						<i><img src="/sm/content/images/fa_circle_grey.png"/></i>
						<i><img src="/sm/content/images/fa_circle_grey.png"/></i>
						<i><img src="/sm/content/images/fa_circle_grey.png"/></i>
					</button>
					<ul class="dropdown-menu dropdown-menu-right">
						<li><a href="#" data-command="edit">Page Settings</a></li>
						<li><a target="_blank" href="/Sm/LandingPageEditor.aspx?id=<%=page.LandingPageID%>">Page Editor</a></li>
						<li><a target="_blank" href="/Sm/LandingPagePreview.aspx?id=<%=page.LandingPageID%>">Preview Page</a></li>
						<li><a href="#" data-command="clone">Clone Page</a></li>
						<li><a href="#" data-command="delete">Delete Page</a></li>
						<%--<li><a href="#" data-command="audit-logs">Audit</a></li>--%>
					</ul>
				</div>
			</td>
		</tr>
        <%
        	Next
	End If
        %>
    </tbody>
    <tfoot>
        <tr>
            <td colspan="7">
                <uc:GridFooter ID="ucGridFooter" runat="server"></uc:GridFooter>
            </td>
        </tr>
    </tfoot>
</table>