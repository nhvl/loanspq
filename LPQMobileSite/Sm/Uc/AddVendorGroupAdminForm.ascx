﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="AddVendorGroupAdminForm.ascx.vb" Inherits="Sm_Uc_AddVendorGroupAdminForm" %>
<div style="padding: 20px; max-height: 500px; min-height: 200px; overflow: auto;">
<form id="frm_addVendorGroupAdmin">
    <div class="row">
        <div class="col-12">
            <div class="form-group">
                <label class="required-field">Email</label>
                <input type="text" class="form-control" value="" name="Email" id="txtEmail" data-type="email" placeholder="Email" data-message-invalid-data="Email is invalid" data-format="email" data-message-require="Please fill out this field" data-required="true" maxlength="50" />
            </div>
        </div>
    </div>
	<div class="row">
		<div class="col-12">
            <div class="form-group">
                <label>Tags</label>
                <input type="text" class="form-control" name="Tags" id="txtTags" data-role="tagsinput" placeholder="Press enter after each group name to confirm" data-required="false" maxlength="500" />
            </div>
        </div>
	</div>
	<div class="row clearfix">
		<div class="col-12 <%=IIf(AvailableRoles.Count = 1, "hidden", "")%>">
            <div class="form-group">
                <label class="required-field">Role</label>
                <select class="form-control" name="Role" id="ddlRole" data-message-require="Please select role" data-required="true">
					<%=SmUtil.RenderDropdownFromRoleEnum(AvailableRoles)%>
                </select>
            </div>
        </div>
	</div>
</form>
</div>