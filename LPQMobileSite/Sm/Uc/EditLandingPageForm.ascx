﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="EditLandingPageForm.ascx.vb" Inherits="Sm_Uc_EditLandingPageForm" %>
<div style="padding: 20px; overflow: auto; max-height: 500px;">
<form id="frm_editlandingpage">
    <div class="row">
        <div class="col-12">
            <div class="form-group">
                <label class="required-field">Title</label>
                <input type="text" class="form-control" name="Title" id="txtTitle" value="<%=LandingPage.Title%>" placeholder="Title" data-message-require="Please fill out this field" data-required="true" maxlength="200" />
				<input type="hidden" name="LandingPageID" id="hdLandingPageID" value="<%=LandingPage.LandingPageID%>"/>
            </div>
        </div>
		<div class="col-12">
            <div class="form-group">
                <label class="required-field">Home Page ID</label>
                <input type="text" class="form-control" name="RefID" id="txtRefID" value="<%=LandingPage.RefID%>" placeholder="Home Page ID" data-message-require="Please fill out this field" data-required="true" maxlength="200" />
            </div>
        </div>
    </div>
</form>
</div>