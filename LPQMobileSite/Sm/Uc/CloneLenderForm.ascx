﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="CloneLenderForm.ascx.vb" Inherits="Sm_Uc_CloneLenderForm" %>
<div style="padding: 20px; overflow: auto; max-height: 500px;">
<form id="frm_clonelender">
    <div class="row">
	    <div class="col-12">
            <div class="form-group">
                <label class="required-field">LOS Domain</label>
                <select class="form-control" name="LosDomain" id="ddlLosDomain" data-message-require="Please select an option" data-required="true">
	                <option value="">Please select</option>
					<%=SmUtil.RenderDropdownFromEnum(GetType(SmSettings.LosDomain))%>
                </select>
            </div>
        </div>
        <div class="col-12">
            <div class="form-group" style="display: flow-root;">
                <label class="required-field">Organization ID</label>
				<div>
					<div class="col-10 no-padding">
						<input type="text" class="form-control" data-format="guid" name="OrganizationID" id="txtOrganizationID" value="" placeholder="Organization ID" data-message-require="Please fill out this field" data-required="true" maxlength="50" />
					</div>	
					<div class="col-2"><button class="btn btn-secondary" style="padding: 6px 10px;" type="button" id="btnGenerateOrganizationID">Generate</button></div>	
				</div>
				<input type="hidden" name="LenderConfigID" id="hdLenderConfigID" value="<%=LenderConfig.LenderConfigID%>"/>
            </div>
        </div>
		<div class="col-12">
            <div class="form-group" style="display: flow-root;">
                <label class="required-field">Lender ID</label>
				<div>
					<div class="col-10 no-padding">
						<input type="text" class="form-control" data-format="guid" name="LenderID" id="txtLenderID" value="" placeholder="Lender ID" data-message-require="Please fill out this field" data-required="true" maxlength="50" />
					</div>	
					<div class="col-2"><button class="btn btn-secondary" style="padding: 6px 10px;" type="button" id="btnGenerateLenderID">Generate</button></div>	
				</div>
            </div>
        </div>
        <div class="col-12">
            <div class="form-group">
                <label class="required-field">Organization Code</label>
                <input type="text" class="form-control" name="OrganizationCode" id="txtOrganizationCode" value="" placeholder="Organization Code" data-message-require="Please fill out this field" data-required="true" maxlength="50" />
            </div>
        </div>
		<div class="col-12">
            <div class="form-group">
                <label class="required-field">Lender Code</label>
                <input type="text" class="form-control" name="LenderCode" id="txtLenderCode" value="" placeholder="Lender Code" data-message-require="Please fill out this field" data-required="true" maxlength="50" />
            </div>
        </div>
		<div class="col-12">
            <div class="form-group">
                <label class="required-field">API Login</label>
                <input type="text" class="form-control" name="ApiLogin" id="txtApiLogin" value="" placeholder="API Login" data-message-require="Please fill out this field" data-required="true" maxlength="50" />
            </div>
        </div>
		<div class="col-12">
            <div class="form-group">
                <label class="required-field">API Password</label>
                <input type="password" class="form-control" name="ApiPassword" id="txtApiPassword" value="" placeholder="API Password" data-message-require="Please fill out this field" data-required="true" maxlength="50" />
            </div>
        </div>
		<div class="col-12">
            <div class="form-group">
                <label>LQB Login</label>
                <input type="text" class="form-control" name="Login" id="txtLogin" value="" placeholder="Login" data-message-require="Please fill out this field" data-required="false" maxlength="50" />
            </div>
        </div>
		<div class="col-12">
            <div class="form-group">
                <label>LQB Password</label>
                <input type="password" class="form-control" name="Password" id="txtPassword" value="" placeholder="Password" data-message-require="Please fill out this field" data-required="false" maxlength="50" />
            </div>
        </div>
		<div class="col-12">
            <div class="form-group">
                <label class="required-field">Name</label>
                <input type="text" class="form-control" name="LenderName" id="txtLenderName" value="" placeholder="Name" data-message-require="Please fill out this field" data-required="true" maxlength="50" />
            </div>
        </div>
		<div class="col-12">
            <div class="form-group">
                <label class="required-field">Lender Ref for the First Portal</label>
                <input type="text" class="form-control" name="LenderRef" id="txtLenderRef" value="" placeholder="Lender Ref" data-message-require="Please fill out this field" data-required="true" maxlength="50" />
            </div>
        </div>
		<div class="col-12">
            <div class="form-group">
                <label class="required-field">City</label>
                <input type="text" class="form-control" name="City" id="txtCity" value="<%=BackOfficeLender.City %>" placeholder="City" data-message-require="Please fill out this field" data-required="true" maxlength="50" />
            </div>
        </div>
		<div class="col-12">
            <div class="form-group">
                <label class="required-field">State</label>
                <select class="form-control" name="State" id="ddlState" data-message-require="Please select state" data-required="true">
					<%=SmUtil.RenderDropdownStateOptions(BackOfficeLender.State)%>
				</select>
            </div>
        </div>
    </div>
</form>
</div>