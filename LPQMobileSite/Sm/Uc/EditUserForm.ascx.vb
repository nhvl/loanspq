﻿
Imports System.IO

Partial Class Sm_Uc_EditUserForm
	Inherits SmBaseUserControl

	Public Property AvatarFileInfo As FileInfo
	Public Property AvailableRoles As New List(Of SmSettings.Role)
	Public Property UserData As SmUser
	Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
		If Not IsPostBack Then
			If UserData IsNot Nothing AndAlso Not String.IsNullOrEmpty(UserData.Avatar) Then
				If UserData.Avatar.StartsWith("/images/avatars/") AndAlso File.Exists(Server.MapPath(UserData.Avatar)) Then
					AvatarFileInfo = New FileInfo(Server.MapPath(UserData.Avatar))
				End If
			End If
		End If
	End Sub
End Class

