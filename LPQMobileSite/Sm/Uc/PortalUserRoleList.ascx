﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="PortalUserRoleList.ascx.vb" Inherits="Sm_Uc_PortalUserRoleList" %>

<%@Import Namespace="Sm" %>
<%@ Import Namespace="LPQMobile.Utils" %>
<%@ Register Src="~/Sm/Uc/Paging.ascx" TagPrefix="uc" TagName="GridFooter" %>
<table class="table table-hover">
    <thead>
        <tr>
			<th>#</th>
			<th style="width: 100px;"></th>
			<%--<th>Role</th>--%>
			<th>Full Name</th>
			<th>Email</th>
			<th>Phone</th>
			<th class="center">Status</th>
			<th class="center">Active</th>
			<th></th>
		</tr>
    </thead>
    <tbody>
        <%If PortalUserRoleList IsNot Nothing AndAlso PortalUserRoleList.Any() Then
        		Dim idx As Integer = (PagingInfo.PageIndex - 1) * PagingInfo.PageSize
        		Dim loginFailedCountExceed = Common.SafeInteger(ConfigurationManager.AppSettings("LoginFailedCountExceed"))
        		For Each userRole As SmUserRole In PortalUserRoleList
        			idx = idx + 1
        			Dim userStatus = userRole.User.Status
        			If string.IsNullOrEmpty(userRole.User.FullName.Trim()) Then
        				userStatus = SmSettings.UserStatus.Inactive
        			ElseIf userStatus = SmSettings.UserStatus.Active AndAlso (userRole.User.LoginFailedCount >= loginFailedCountExceed OrElse (userRole.User.LastLoginDate.HasValue AndAlso userRole.User.LastLoginDate.Value.AddDays(userRole.User.ExpireDays) < DateTime.Now)) Then
        				userStatus = SmSettings.UserStatus.Locked
        			End If
        %>
		 <tr data-id="<%=userRole.UserRoleID%>">
			<td><%=idx%></td>
			<td>
				<div class="avatar-wrapper">
					<span class="avatar" style="background-image: url('<%=IIf(String.IsNullOrEmpty(userRole.User.Avatar.Trim()), "/images/avatar.jpg", userRole.User.Avatar)%>');"></span>
				</div>
			</td>
			<%--<td><%=userRole.Role.ToString()%></td>--%>
			<td><%=IIf(string.IsNullOrEmpty(userRole.User.FullName.Trim()), "-", userRole.User.FullName)%></td>
			<td><%=userRole.User.Email%></td>
			<%If String.IsNullOrEmpty(userRole.User.Phone.Trim()) Then%>
			<td>-</td>
			<%Else%>
			<td><%=Regex.Replace(userRole.User.Phone, "(\d{3})(\d{3})(\d{4})", "$1-$2-$3")%></td>
			<%End If %>
			<td class="center"><%=userStatus.GetEnumDescription()%></td>
			<td class="center">
				<label class="toggle-btn" data-on="ON" data-off="OFF">
					<input type="checkbox" data-command="enable-btn" <%=SmUtil.BindCheckbox(userRole.Status = SmSettings.UserRoleStatus.Active)%>/>
					<span class="button-checkbox"></span>
				</label>
			</td>
			<td style="text-align: right;" data-id="<%=userRole.UserID%>">
				<%If userStatus = SmSettings.UserStatus.Locked Then%>
				<a href="#" data-command="unlock">
					<span>Unlock</span>
				</a>
				<span>|</span>
				<%End If %>
				<a href="#" data-command="delete">
					<span>Delete</span>
				</a>
			</td>
		</tr>
        <%
        	Next
	End If
        %>
    </tbody>
    <tfoot>
        <tr>
            <td colspan="8">
                <uc:GridFooter ID="ucGridFooter" runat="server"></uc:GridFooter>
            </td>
        </tr>
    </tfoot>
</table>