﻿Imports System.IO

Partial Class Sm_Uc_EditLenderVendorForm
	Inherits SmBaseUserControl

	Public Property Vendor As SmLenderVendor
	Public Property LogoFileInfo As FileInfo

	Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
		If Not IsPostBack Then
			If Vendor IsNot Nothing AndAlso Not String.IsNullOrEmpty(Vendor.Logo) Then
				Dim filePath = Server.MapPath(Vendor.Logo)
				If File.Exists(filePath) Then
					LogoFileInfo = New FileInfo(filePath)
				End If
			End If
		End If
	End Sub

	Protected Function BinLenderVendorTypeCheckbox(type As SmSettings.LenderVendorType) As String
		If Vendor IsNot Nothing AndAlso Vendor.Type IsNot Nothing AndAlso Vendor.Type.Any(Function(p) p = type.ToString()) Then
			Return " checked='checked' "

		End If
		Return ""
	End Function
End Class
