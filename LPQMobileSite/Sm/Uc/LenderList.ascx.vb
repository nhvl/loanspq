﻿
Partial Class Sm_Uc_LenderList
	Inherits SmBaseUserControl

	Protected LenderList As List(Of SmBackOfficeLender)

	Public Property PagingInfo As SmPagingInfoModel
	Public Property FilterInfo As SmBackOfficeLenderListFilter
	Protected CanManageUsers As Boolean = True


	Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
		If Not IsPostBack Then
			Try
				If SmUtil.CheckRoleAccess(Context, SmSettings.Role.SuperOperator.ToString(), SmSettings.Role.LegacyOperator.ToString(), SmSettings.Role.Officer.ToString()) Then
					'If HttpContext.Current.User.IsInRole(SmSettings.Role.SuperOperator.ToString()) OrElse HttpContext.Current.User.IsInRole(SmSettings.Role.LegacyOperator.ToString()) OrElse HttpContext.Current.User.IsInRole(SmSettings.Role.Officer.ToString()) Then
					LenderList = smBL.GetPortals(PagingInfo, FilterInfo)
				Else
					If Not Context.User.Identity.Name.Contains("@") Then
						Dim ssoSessionData = smAuthBL.GetSSOSessionData(Guid.Parse(Context.User.Identity.Name))
						If ssoSessionData IsNot Nothing Then
							LenderList = smBL.GetLendersBySSOSession(PagingInfo, FilterInfo, ssoSessionData.WorkingUrl, ssoSessionData.LenderCode, ssoSessionData.OrganizationCode, pbDecrypt:=False)
							If CurrentUserInfo.SSOAccessRights IsNot Nothing Then
								CanManageUsers = CurrentUserInfo.SSOAccessRights.CanManageUsers
							End If
						End If
					Else
						LenderList = smBL.GetPortals(PagingInfo, FilterInfo, CurrentUserInfo.UserID)
					End If

				End If
				ucGridFooter.PagingInfo = PagingInfo
			Catch ex As Exception
				log.Error("Could not load lender list", ex)
			End Try
		End If
	End Sub
End Class
