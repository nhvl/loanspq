﻿
Imports System.IO

Partial Class Sm_Uc_AddLandingPageForm
	Inherits System.Web.UI.UserControl
	Public Property LenderConfigID As Guid
	Public Property TemplateList As Dictionary(Of String, String)

	Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
		If Not IsPostBack Then
			TemplateList = New Dictionary(Of String, String)
			Dim path As String = Server.MapPath("~/App_Data/LandingPageTemplates/")
			Dim templateFiles = Directory.GetFiles(path)
			If templateFiles IsNot Nothing AndAlso templateFiles.Any() Then
				Dim encrypter As New CRijndaelManaged()
				For Each file As String In templateFiles
					Dim match = Regex.Match(file, "(?<filenamewithext>(?<filename>[\w\s-\.]+)\.\w+)$")
					If match.Success Then
						If Not TemplateList.ContainsKey(match.Groups("filename").Value) Then
							TemplateList.Add(match.Groups("filename").Value, encrypter.EncryptUtf8(match.Groups("filenamewithext").Value))
						End If
					End If
				Next
			End If
		End If
	End Sub
End Class
