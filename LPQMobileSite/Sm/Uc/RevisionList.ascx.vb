﻿
Partial Class Sm_Uc_RevisionList
	Inherits SmBaseUserControl

	Protected LogList As List(Of SmLenderConfigLogItem)
	Public Property LenderConfigID As Guid
	Public Property PagingInfo As SmPagingInfoModel

	Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
		If Not IsPostBack Then
			Try
				LogList = smBL.GetLenderConfigLogs(PagingInfo, LenderConfigID)
				ucGridFooter.PagingInfo = PagingInfo
			Catch ex As Exception
				log.Error("Could not load revision list", ex)
			End Try
		End If
	End Sub
End Class
