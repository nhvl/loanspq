﻿
Partial Class Sm_Uc_LandingPageList
	Inherits SmBaseUserControl

	Protected LandingPagesItems As List(Of SmLandingPageModel)
	Public Property PagingInfo As New SmPagingInfoModel
	Public Property FilterInfo As New SmLandingPageListFilter
	Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
		If Not IsPostBack Then
			Try
				LandingPagesItems = smBL.GetLandingPages(PagingInfo, FilterInfo)
				ucGridFooter.PagingInfo = PagingInfo
			Catch ex As Exception
				log.Error("Could not load landing page list", ex)
			End Try
		End If
	End Sub
End Class
