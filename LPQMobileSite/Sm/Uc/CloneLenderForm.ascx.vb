﻿
Partial Class Sm_Uc_CloneLenderForm
	Inherits SmBaseUserControl

	Public Property LenderConfig As SmLenderConfigBasicInfo
	Public Property BackOfficeLender As SmBackOfficeLender
	Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
		If Not IsPostBack Then
			Try
				If LenderConfig IsNot Nothing Then
					Dim smBl As New SmBL()
					BackOfficeLender = smBl.GetBackOfficeLenderByLenderConfigID(LenderConfig.LenderConfigID)
					If BackOfficeLender Is Nothing Then
						Throw New Exception("Bad data")
					End If
				Else
					Throw New Exception("Bad data")
				End If
			Catch ex As Exception
				log.Error(ex)
				Response.StatusCode = 404 '' Forbidden
				Response.StatusDescription = "Not found"
				Response.End() '' Terminate all the rest processes
			End Try
		End If
	End Sub
End Class
