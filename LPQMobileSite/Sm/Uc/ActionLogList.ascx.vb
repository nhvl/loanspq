﻿
Partial Class Sm_Uc_ActionLogList
	Inherits SmBaseUserControl

	Protected logList As List(Of SmActionLogView)

	Public Property PagingInfo As SmPagingInfoModel
	Public Property FilterInfo As SmActionLogListFilter

	Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
		If Not IsPostBack Then
			Try
				logList = smBL.GetActionLogs(PagingInfo, FilterInfo)
				ucGridFooter.PagingInfo = PagingInfo
			Catch ex As Exception
				log.Error("Could not load action log list", ex)
			End Try
		End If
	End Sub
End Class
