﻿
Partial Class Sm_Uc_Paging
    Inherits System.Web.UI.UserControl

	Public PagingInfo As SmPagingInfoModel
	Protected PagingLabel As String = ""
	Protected PagingVisible As Boolean = True

	Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
		If PagingInfo Is Nothing OrElse PagingInfo.TotalRecord = 0 Then
			PagingVisible = False
		Else
			If PagingInfo.TotalRecord > PagingInfo.PageSize Then
				Dim delta = PagingInfo.PageSize * PagingInfo.PageIndex
				Dim showing = IIf(delta > PagingInfo.TotalRecord, PagingInfo.TotalRecord, delta)
				PagingLabel = String.Format("Showing {0}/{1} {2}", showing, PagingInfo.TotalRecord.ToString("N0"), IIf(PagingInfo.TotalRecord > 1, "items", "item"))

			Else
				PagingVisible = False
			End If
		End If
	End Sub
End Class
