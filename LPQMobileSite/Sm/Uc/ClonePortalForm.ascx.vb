﻿
Partial Class Sm_Uc_ClonePortalForm
	Inherits SmBaseUserControl

	Public Property LenderConfig As SmLenderConfig
	Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
		If Not IsPostBack Then
			Try
				If LenderConfig Is Nothing Then
					Throw New Exception("Bad data")
				End If
			Catch ex As Exception
				log.Error(ex)
				Response.StatusCode = 404 '' Forbidden
				Response.StatusDescription = "Not found"
				Response.End() '' Terminate all the rest processes
			End Try
		End If
	End Sub
End Class
