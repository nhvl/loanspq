﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="LenderList.ascx.vb" Inherits="Sm_Uc_LenderList" %>
<%@Import Namespace="Sm" %>
<%@ Register Src="~/Sm/Uc/Paging.ascx" TagPrefix="uc" TagName="GridFooter" %>
<table class="table table-hover lender-list">
    <thead>
        <tr>
			<th>#</th>
			<th width="40%">Lender</th>
			<th>Host</th>
			<th>Last Modified</th>
			<th>Modified By</th>
			<th>Actions</th>
		</tr>
    </thead>
    <tbody>
        <%If LenderList IsNot Nothing AndAlso LenderList.Any() Then
        		Dim idx As Integer = (PagingInfo.PageIndex - 1) * PagingInfo.PageSize
        		For Each lender As SmBackOfficeLender In LenderList.Take(PagingInfo.PageSize)
        			idx = idx + 1
        			If lender.PortalList Is Nothing OrElse lender.PortalList.Count = 0 Then Continue For
        			If Not lender.PortalList.Any(Function(p) SmUtil.CheckRoleAccess(HttpContext.Current, SmSettings.Role.SuperUser.ToString(), String.Format("LenderScope${0}${1}", SmSettings.Role.LenderAdmin.ToString(), p.LenderID.ToString()), String.Format("PortalScope${0}${1}${2}", SmSettings.Role.PortalAdmin.ToString(), p.LenderID.ToString(), p.LenderConfigID.ToString()), String.Format("PortalScope${0}${1}${2}", SmSettings.Role.VendorGroupAdmin.ToString(), p.LenderID.ToString(), p.LenderConfigID.ToString()))) Then Continue For
        %>
		 <tr class="lender-row" data-lender-id="<%=lender.LenderID%>" data-portal-id="<%=lender.PortalList.First().LenderConfigID%>">
				<td><%=idx%></td>
				<td data-toggle="tooltip" data-placement="bottom" title="Lender Name"><%=lender.LenderName%> (<%=lender.City%>, <%=lender.State%>)</td>
				<td><%=lender.Host%></td>
				<%If lender.LastModifiedDate.HasValue Then%>
				<%--<td nowrap="nowrap"><%=lender.LastModifiedDate%></td>--%>
			     <td nowrap="nowrap"></td>
				 <%Else%>
				<td nowrap="nowrap"></td>
				 <%End If%>
				<%--<td><%=lender.LastModifiedBy%></td>--%>
				<td></td>
				<td nowrap="nowrap">
					<%If SmUtil.CheckRoleAccess(HttpContext.Current, SmSettings.Role.SuperUser.ToString(), String.Format("LenderScope${0}${1}", SmSettings.Role.LenderAdmin.ToString(), lender.LenderID.ToString())) Then%>
					<div class="dropdown inline-drop-menu">
					  <button class="btn dropdown-toggle btn-dropdown" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
							<img src="/sm/content/images/fa_circle_grey.png"/>
							<img src="/sm/content/images/fa_circle_grey.png"/>
							<img src="/sm/content/images/fa_circle_grey.png"/>
					  </button>
					  <ul class="dropdown-menu dropdown-menu-right">
						  <%If SmUtil.CheckRoleAccess(HttpContext.Current, SmSettings.Role.SuperUser.ToString(), String.Format("LenderScope${0}${1}", SmSettings.Role.LenderAdmin.ToString(), lender.LenderID.ToString())) Then%>
							<li><a href="/sm/lenderdashboard.aspx?id=<%=lender.BackOfficeLenderID%>">Dashboard</a></li>
							<%End If%>
							<%If SmUtil.CheckRoleAccess(HttpContext.Current, SmSettings.Role.SuperOperator, SmSettings.Role.LegacyOperator) Then%>
							<li><a href="#" data-command="clone">Clone</a></li>
							<%End If%>
					  </ul>
					</div>
					<%End If%>
					
				</td>
			</tr>
		<%--<%If lender.PortalList IsNot Nothing AndAlso lender.PortalList.Any() Then%>--%>
		<%--<tr class="portal-row row-title">
			<td></td>
			<td>LenderRef</td>
			<td>Name</td>
			<td>Created Date</td>
			<td>Create By</td>
			<td nowrap="nowrap">Actions</td>
		</tr>--%>
		<%For Each portal As SmLenderConfig In lender.PortalList%>
		<tr class="portal-row" data-portal-id="<%=portal.LenderConfigID%>" data-lender-ref="<%=HttpUtility.HtmlAttributeEncode(portal.LenderRef)%>">
			<td></td>
			<%--<td><i class="fa fa-globe mr5"> </i><%=portal.Note%></td>--%>
			<%--don't want to use internal note bc it is for internal use only--%>
			<td data-toggle="tooltip" data-placement="bottom" title="Lenderref, unique identifier for each portal (site).  There may be multiple portals per lender."><i><img src="/sm/content/images/fa_globe_g.png" width="12px" height="12px"/></i> &nbsp;<%=portal.LenderRef%></td>
			<td></td>
			<%If portal.LastModifiedDate.HasValue Then%>
				<td><%=portal.LastModifiedDate%></td>
				 <%Else%>
				<td></td>
				 <%End If%>
				<td><%=portal.LastModifiedByUserName%></td>
				<td nowrap="nowrap">
					
					<div class="dropdown inline-drop-menu">
					  <button class="btn dropdown-toggle btn-dropdown" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
							<img src="/sm/content/images/fa_circle_grey.png" />
							<img src="/sm/content/images/fa_circle_grey.png" />
							<img src="/sm/content/images/fa_circle_grey.png" />
					  </button>
					  <ul class="dropdown-menu dropdown-menu-right">
						   <%If SmUtil.CheckRoleAccess(HttpContext.Current, SmSettings.Role.SuperUser.ToString(), String.Format("LenderScope${0}${1}", SmSettings.Role.LenderAdmin.ToString(), portal.LenderID.ToString()), String.Format("PortalScope${0}${1}${2}", SmSettings.Role.PortalAdmin, portal.LenderID.ToString(), portal.LenderConfigID.ToString())) Then%>
							<li><a target="_blank" href="/Sm/Index.aspx?lenderconfigid=<%=portal.LenderConfigID%>">Edit Settings</a></li>
							<%End If%>
						    <li><a target="_blank" href="/apply.aspx?lenderref=<%=portal.LenderRef%>">Landing Page</a></li>
						   
						    <%If SmUtil.CheckRoleAccess(HttpContext.Current, SmSettings.Role.SuperUser.ToString(), String.Format("LenderScope${0}${1}", SmSettings.Role.LenderAdmin.ToString(), portal.LenderID.ToString()), String.Format("PortalScope${0}${1}${2}", SmSettings.Role.PortalAdmin, portal.LenderID.ToString(), portal.LenderConfigID.ToString()), String.Format("PortalScope${0}${1}${2}", SmSettings.Role.VendorGroupAdmin, portal.LenderID.ToString(), portal.LenderConfigID.ToString())) Andalso CanManageUsers Then%>
								 <li><a href="/sm/portaldashboard.aspx?lenderconfigid=<%=portal.LenderConfigID%>">Dashboard</a></li>
							<%End If%>
							
						  <%If SmUtil.CheckRoleAccess(HttpContext.Current, SmSettings.Role.User.ToString(), SmSettings.Role.SuperUser.ToString(), String.Format("LenderScope${0}${1}", SmSettings.Role.LenderAdmin.ToString(), portal.LenderID.ToString())) Then%>
								<%If SmUtil.CheckRoleAccess(HttpContext.Current, SmSettings.Role.User.ToString(), SmSettings.Role.SuperUser.ToString(), SmSettings.Role.SSOUser.ToString(), String.Format("LenderScope${0}${1}", SmSettings.Role.LenderAdmin.ToString(), portal.LenderID.ToString()), String.Format("PortalScope${0}${1}${2}", SmSettings.Role.PortalAdmin, portal.LenderID.ToString(), portal.LenderConfigID.ToString())) Then%>
									<li><a href="#" data-command="audit-logs" data-id="<%=portal.LenderConfigID%>">Modification History</a></li>
								<%End If%>
								<%If SmUtil.CheckRoleAccess(HttpContext.Current, SmSettings.Role.SuperUser) Then%>
									<li><a href="#" data-command="clone">Clone</a></li>
								<%End If%>
							<%End If%>
						  <%If SmUtil.CheckRoleAccess(HttpContext.Current, SmSettings.Role.VendorAdmin, SmSettings.Role.VendorUser) Then%>
							<li><a href="/agent/Default.aspx">Vendor</a></li>
						  <%End If%>
					  </ul>
					</div>
				</td>
		</tr>
		<%Next%>
		<%--<%End If%>--%>
        <%
        	Next
	End If
        %>
    </tbody>
    <tfoot>
        <tr>
            <td colspan="6">
                <uc:GridFooter ID="ucGridFooter" runat="server"></uc:GridFooter>
            </td>
        </tr>
    </tfoot>
</table>
