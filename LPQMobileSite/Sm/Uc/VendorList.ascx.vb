﻿
Imports Sm

Partial Class Sm_Uc_VendorList
	Inherits SmBaseUserControl

	Protected LenderVendorList As List(Of SmLenderVendor)
	Public Property PagingInfo As New SmPagingInfoModel
	Public Property FilterInfo As New SmVendorListFilter
	Public Property LenderConfigID As Guid
	Public Property LenderID As Guid

	Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
		If Not IsPostBack Then
			Try
				LenderVendorList = smBL.GetLenderVendors(PagingInfo, FilterInfo)
				ucGridFooter.PagingInfo = PagingInfo
			Catch ex As Exception
				log.Error("Could not load lender vendor user list", ex)
			End Try
		End If
	End Sub

End Class

