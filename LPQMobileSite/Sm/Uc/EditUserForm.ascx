﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="EditUserForm.ascx.vb" Inherits="Sm_Uc_EditUserForm" %>
<div style="padding: 20px; max-height: 500px; overflow: auto;">
<form id="frm_adduser">
    <div class="row">
        <div class="col-12">
            <div class="form-group">
                <label class="required-field">First Name</label>
                <input type="text" class="form-control" name="FirstName" id="txtFirstName" value="<%=UserData.FirstName%>" placeholder="First Name" data-message-require="Please fill out this field" data-required="true" maxlength="50" />
				<input type="hidden" name="UserID" id="hdUserID" value="<%=UserData.UserID%>"/>
            </div>
        </div>
        <div class="col-12">
            <div class="form-group">
                <label class="required-field">LastName</label>
                <input type="text" class="form-control" name="LastName" id="txtLastName" value="<%=UserData.LastName%>" placeholder="Last Name" data-message-require="Please fill out this field" data-required="true" maxlength="50" />
            </div>
        </div>
        <div class="col-12">
            <div class="form-group">
                <label class="required-field">Email</label>
                <input type="text" class="form-control" value="<%=UserData.Email%>" name="Email" id="txtEmail" data-type="email" placeholder="Email" data-message-invalid-data="Email is invalid" data-format="email" data-message-require="Please fill out this field" data-required="true" maxlength="50" />
            </div>
        </div>
        <div class="col-12">
            <div class="form-group">
                <label>Phone Number</label>
                <input type="text" class="form-control" value="<%=UserData.Phone%>" name="Phone" id="txtPhone" data-message-invalid-data="Phone is invalid" data-format="phone" placeholder="Phone Number" maxlength="20" />
            </div>
        </div>
		<div class="col-12">
            <div class="form-group">
                <label>Avatar</label>
                <div class="logo-upload-zone dropzone" id="divAvatar" data-current-logo=""></div>
				<%If AvatarFileInfo IsNot Nothing Then%>
				<input type="hidden" value="<%=UserData.Avatar%>" name="Avatar" id="hdAvatar" data-file-ext="<%=AvatarFileInfo.Extension%>" data-file-name="<%=AvatarFileInfo.Name%>" data-file-size="<%=AvatarFileInfo.Length%>"/>
				<%Else%>
				<input type="hidden" value="<%=UserData.Avatar%>" name="Avatar" id="hdAvatar" data-file-ext="" data-file-name="filename" data-file-size="0"/>
				<%End If%>
            </div>
        </div>
        <div class="col-12">
            <div class="form-group">
                <label>Expire Days</label>
				<input type="text" class="form-control" name="ExpireDays" value="<%=UserData.ExpireDays%>" id="txtExpireDays" placeholder="Expire Days" data-message-invalid-data="Expire Days is invalid" data-format="positive-number">
            </div>
        </div>
        <div class="col-12 <%=IIf(AvailableRoles.Count = 1, "hidden", "")%>">
            <div class="form-group">
                <label class="required-field">Role</label>
                <select class="form-control" name="Role" id="ddlRole" data-message-require="Please select role" data-required="true">
                    <%--<%=SmUtil.RenderDropdownFromEnum(GetType(SmSettings.Role))%>--%>
					<%=SmUtil.RenderDropdownFromRoleEnum(AvailableRoles, UserData.Role.ToString())%>
                </select>
            </div>
        </div>
    </div>
</form>
</div>