﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="RevisionList.ascx.vb" Inherits="Sm_Uc_RevisionList" %>
<%@ Register Src="~/Sm/Uc/Paging.ascx" TagPrefix="uc" TagName="GridFooter" %>
<table class="table table-hover">
    <thead>
        <tr>
			<th>#</th>
			<th>Comment</th>
			<th style="text-align: center;">Log Time</th>
			<th style="text-align: center;">Modified By</th>
			<th style="text-align: center;">Remote IP</th>
			<th style="text-align: center;">Revision #</th>
			<th style="text-align: center;">Actions</th>
		</tr>
    </thead>
    <tbody>
        <%If LogList IsNot Nothing AndAlso LogList.Any() Then
        		Dim idx As Integer = (PagingInfo.PageIndex - 1) * PagingInfo.PageSize
        		For Each logItem As SmLenderConfigLogItem In LogList
        			idx = idx + 1
        			
        %>
		 <tr>
				<td style="text-align: center;min-width: 50px;"><%=idx%></td>
				<td style="width: 30%;"><%=logItem.Comment%></td>
				<td style="text-align: center;"><%=logItem.LogTime%></td>
				<%If logItem.LastModifiedByUserID <> Guid.Empty AndAlso logItem.LastModifiedBy IsNot Nothing Then%>
				<td style="text-align: center;"><%=logItem.LastModifiedBy.FullName%></td>
				<%Else%>
				<td style="text-align: center;"></td>
				<%End If%>
				<td style="text-align: center;"><%=logItem.RemoteIP%></td>
				<td style="text-align: center;"><%=logItem.RevisionID%></td>
				<td style="min-width: 100px; text-align: right">
					<a href="#" data-command="revert" data-id="<%=logItem.LenderConfigLogID%>">
						<span>Revert to this revision</span>
					</a>
					<%If idx < PagingInfo.TotalRecord Then%>
					<span>|</span>
					<a href="#" data-command="showchanges" data-id="<%=logItem.LenderConfigLogID%>">
						<span>Show changes</span>
					</a>
					<%End If %>
					<span>|</span>
					<a href="#" data-command="preview" data-id="<%=logItem.LenderConfigLogID%>" title="Preview">
						<span>Preview</span>
					</a>
				</td>
			</tr>
        <%
        	Next
	End If
        %>
    </tbody>
    <tfoot>
        <tr>
            <td colspan="10">
                <uc:GridFooter ID="ucGridFooter" runat="server"></uc:GridFooter>
            </td>
        </tr>
    </tfoot>
</table>
