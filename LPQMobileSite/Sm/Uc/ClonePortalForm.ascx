﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="ClonePortalForm.ascx.vb" Inherits="Sm_Uc_ClonePortalForm" %>
<div style="padding: 20px; overflow: auto; max-height: 500px;">
<form id="frm_cloneportal">
    <div class="row">
        <div class="col-12">
            <div class="form-group">
                <label class="required-field">Name</label>
                <input type="text" class="form-control" name="PortalName" id="txtPortalName" value="" placeholder="Name" data-message-require="Please fill out this field" data-required="true" maxlength="50" />
				<input type="hidden" name="LenderConfigID" id="hdLenderConfigID" value="<%=LenderConfig.LenderConfigID%>"/>
            </div>
        </div>
		<div class="col-12">
            <div class="form-group">
                <label class="required-field">Lender Ref</label>
                <input type="text" class="form-control" name="LenderRef" id="txtLenderRef" value="" placeholder="Lender Ref" data-message-require="Please fill out this field" data-required="true" maxlength="50" />
            </div>
        </div>
		<div class="col-12">
            <div class="form-group">
                <label>API Login</label>
                <input type="text" class="form-control" name="ApiLogin" id="txtApiLogin" value="<%=LenderConfig.LPQLogin%>" placeholder="API Login" data-message-require="Please fill out this field" data-required="false" maxlength="50" />
            </div>
        </div>
		<div class="col-12">
            <div class="form-group">
                <label>API Password</label>
                <input type="password" class="form-control" name="ApiPassword" id="txtApiPassword" value="<%=LenderConfig.LPQPW%>" placeholder="API Password" data-message-require="Please fill out this field" data-required="false" maxlength="50" />
            </div>
        </div>
		<div class="col-12">
            <div class="form-group">
                <label>LQB Login</label>
                <input type="text" class="form-control" name="Login" id="txtLogin" value="" placeholder="Login" data-message-require="Please fill out this field" data-required="false" maxlength="50" />
            </div>
        </div>
		<div class="col-12">
            <div class="form-group">
                <label>LQB Password</label>
                <input type="password" class="form-control" name="Password" id="txtPassword" value="" placeholder="Password" data-message-require="Please fill out this field" data-required="false" maxlength="50" />
            </div>
        </div>
    </div>
</form>
</div>