﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="MortgageLoanSearchResult.ascx.vb" Inherits="Sm_Uc_MortgageLoanSearchResult" %>
<%@ Import Namespace="Newtonsoft.Json" %>
<%@ Register Src="~/Sm/Uc/Paging.ascx" TagPrefix="uc" TagName="GridFooter" %>
<p>Search Results for "<%=FilterInfo.Keyword%>" as of <%=Now.ToString("MM/dd/yyyy hh:mm tt")%></p>
<br/>
<%If ConsumerList Is Nothing OrElse ConsumerList.Count = 0 Then%>
<p>No applications were found.</p>
<%Else%>
<script type="text/javascript">
	var MORTGAGE_LOAN_SEARCH_RESULT = <%=JsonConvert.SerializeObject(ConsumerList)%>
</script>
<table class="table ml-app-table">
    <tbody>
        <%
        	Dim index As Integer = 0
        	For Each item As SmMortgageLoanConsumerInfo In ConsumerList%>
		 <tr class="row-info">
			 <td>
				 <div>Loan Number</div>
				 <div><%=LoanNumber%></div>
			 </td>
			 <td>
				 <div>Applicant</div>
				 <div><%=String.Format("{0} {1} ({2})", item.FirstName, item.LastName, IIf(item.IsPrimaryConsumer, "primary", "co-applicant"))%></div>
			 </td>
			 <td>
				 <div>Email</div>
				 <div><%=item.Email%></div>
			 </td>
			 <td>
				 <div>Phone Number</div>
				 <div><%=item.Phone%></div>
			 </td>
			 <td>
				 <a href="#" data-command="generate-url" onclick="mlSupport.FACTORY.generateUrl(this, '<%=LoanNumber%>', <%=index%>)">Generate URL</a>
				 <a href="#" target="_blank" data-command="open-application">Open Application</a>
			 </td>
			</tr>
			<tr class="app-lnk-container">
				<td colspan="5"></td>
			</tr>
        <%
        	index += 1
        	Next
        %>
    </tbody>
	<%If PagingInfo.TotalPage > 1 Then%>
    <tfoot>
        <tr>
            <td colspan="10">
                <uc:GridFooter ID="ucGridFooter" runat="server"></uc:GridFooter>
            </td>
        </tr>
    </tfoot>
	<%End If %>
</table>
<%End If%>