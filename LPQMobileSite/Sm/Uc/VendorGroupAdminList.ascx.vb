﻿
Partial Class Sm_Uc_VendorGroupAdminList
	Inherits SmBaseUserControl

	Protected VendorGroupAdminUserRoleList As List(Of SmUserRole)
	Public Property PagingInfo As SmPagingInfoModel
	Public Property FilterInfo As SmVendorGroupAdminUserRoleListFilter
	Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
		If Not IsPostBack Then
			Try
				VendorGroupAdminUserRoleList = smBL.GetVendorGroupAdminUserRoles(PagingInfo, FilterInfo)
				ucGridFooter.PagingInfo = PagingInfo
			Catch ex As Exception
				log.Error("Could not load vendor group admin user role list", ex)
			End Try
		End If
	End Sub
End Class