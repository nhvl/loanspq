﻿
Partial Class Sm_Uc_MortgageLoanSearchResult
	Inherits SmBaseUserControl

	Protected ConsumerList As List(Of SmMortgageLoanConsumerInfo)
	Public Property PagingInfo As SmPagingInfoModel
	Public Property FilterInfo As SmGeneralListFilter

	Protected LoanNumber As String

	Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
		If Not IsPostBack Then
			Try
				If SmUtil.CheckRoleAccess(Context, SmSettings.Role.SuperUser.ToString()) Then
					Dim result = smBL.SearchMortgageLoan(PagingInfo, FilterInfo)
					If result IsNot Nothing AndAlso Not String.IsNullOrWhiteSpace(result.LoanNumber) Then
						LoanNumber = result.LoanNumber
						ConsumerList = result.Consumers
					End If
				End If
				ucGridFooter.PagingInfo = PagingInfo
			Catch ex As Exception
				log.Error("Could not load mortgage loan search result", ex)
			End Try
		End If
	End Sub
End Class

