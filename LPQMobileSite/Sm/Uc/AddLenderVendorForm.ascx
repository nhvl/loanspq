﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="AddLenderVendorForm.ascx.vb" Inherits="Sm_Uc_AddLenderVendorForm" %>
<%@ Import Namespace="Sm" %>
<div style="padding: 20px; overflow: auto; max-height: 500px;">
<form id="frm_addvendor">
    <div class="row">
        <div class="col-12">
            <div class="form-group">
                <label class="required-field">Vendor Name</label>
                <input type="text" class="form-control" name="VendorName" id="txtVendorName" placeholder="Vendor Name" data-message-require="Please fill out this field" data-required="true" maxlength="50" />
            </div>
        </div>
		<div class="col-12">
            <div class="form-group">
                <label>Vendor Groups</label>
                <input type="text" class="form-control" name="Tags" id="txtTags" data-role="tagsinput" placeholder="Press enter after each group name to confirm" data-required="false" maxlength="500" />
            </div>
        </div>
		<div class="col-12">
            <div class="form-group">
                <label class="required-field">Type</label>
                <%--<select class="form-control" name="Type" id="ddlVendorType" data-message-require="Please select type" data-required="true">
	                <option value="">Please select</option>
					<%=SmUtil.RenderDropdownFromEnum(GetType(SmSettings.LenderVendorType))%>
                </select>--%>
				<div class="checkbox-group vendor-type-lst">
					<div class="checkbox-group-header">
						<div class="checkbox checkbox-success checkbox-inline">
							<input type="checkbox" name="Type[]" value="<%=SmSettings.LenderVendorType.CC.ToString()%>" id="chkLenderVendorType_<%=SmSettings.LenderVendorType.CC.ToString()%>"/>
							<label for="chkLenderVendorType_<%=SmSettings.LenderVendorType.CC.ToString()%>"><%=SmSettings.LenderVendorType.CC.GetEnumDescription()%></label>
						</div>
						<div class="checkbox checkbox-success checkbox-inline">
							<input type="checkbox" name="Type[]" value="<%=SmSettings.LenderVendorType.CC_COMBO.ToString()%>" id="chkLenderVendorType_<%=SmSettings.LenderVendorType.CC_COMBO.ToString()%>"/>
							<label for="chkLenderVendorType_<%=SmSettings.LenderVendorType.CC_COMBO.ToString()%>"><%=SmSettings.LenderVendorType.CC_COMBO.GetEnumDescription()%></label>
						</div>
						<div class="checkbox checkbox-success checkbox-inline">
							<input type="checkbox" name="Type[]" value="<%=SmSettings.LenderVendorType.PL.ToString()%>" id="chkLenderVendorType_<%=SmSettings.LenderVendorType.PL.ToString()%>"/>
							<label for="chkLenderVendorType_<%=SmSettings.LenderVendorType.PL.ToString()%>"><%=SmSettings.LenderVendorType.PL.GetEnumDescription()%></label>
						</div>
						<div class="checkbox checkbox-success checkbox-inline">
							<input type="checkbox" name="Type[]" value="<%=SmSettings.LenderVendorType.PL_COMBO.ToString()%>" id="chkLenderVendorType_<%=SmSettings.LenderVendorType.PL_COMBO.ToString()%>"/>
							<label for="chkLenderVendorType_<%=SmSettings.LenderVendorType.PL_COMBO.ToString()%>"><%=SmSettings.LenderVendorType.PL_COMBO.GetEnumDescription()%></label>
						</div>
						<div class="checkbox checkbox-success checkbox-inline">
							<input type="checkbox" name="Type[]" value="<%=SmSettings.LenderVendorType.VL.ToString()%>" id="chkLenderVendorType_<%=SmSettings.LenderVendorType.VL.ToString()%>"/>
							<label for="chkLenderVendorType_<%=SmSettings.LenderVendorType.VL.ToString()%>"><%=SmSettings.LenderVendorType.VL.GetEnumDescription()%></label>
						</div>
						<div class="checkbox checkbox-success checkbox-inline">
							<input type="checkbox" name="Type[]" value="<%=SmSettings.LenderVendorType.VL_COMBO.ToString()%>" id="chkLenderVendorType_<%=SmSettings.LenderVendorType.VL_COMBO.ToString()%>"/>
							<label for="chkLenderVendorType_<%=SmSettings.LenderVendorType.VL_COMBO.ToString()%>"><%=SmSettings.LenderVendorType.VL_COMBO.GetEnumDescription()%></label>
						</div>
					</div>
				</div>
            </div>
        </div>
		<div class="col-12">
            <div class="form-group">
                <label class="required-field">Logo</label>
                <div class="logo-upload-zone dropzone" id="divVendorLogo"></div>
				<input type="hidden" value="" name="Logo" id="hdLogo"/>
            </div>
        </div>
		<div class="col-12">
            <div class="form-group">
                <label class="required-field">Address</label>
                <input type="text" class="form-control" name="Address" id="txtAddress" placeholder="Address" data-message-require="Please fill out this field" data-required="true" maxlength="200" />
            </div>
        </div>
		<div class="col-12">
            <div class="form-group">
                <label class="required-field">City</label>
                <input type="text" class="form-control" name="City" id="txtCity" placeholder="City" data-message-require="Please fill out this field" data-required="true" maxlength="50" />
            </div>
        </div>
		<div class="col-12">
            <div class="form-group">
                <label class="required-field">State</label>
                <select class="form-control" name="State" id="ddlState" data-message-require="Please select state" data-required="true">
					<%=SmUtil.RenderDropdownStateOptions()%>
                </select>
            </div>
        </div>
		<div class="col-12">
            <div class="form-group">
                <label class="required-field">Zip</label>
                <input type="text" class="form-control" name="Zip" id="txtZip" placeholder="Zip" data-message-require="Please fill out this field" data-required="true" maxlength="50" />
            </div>
        </div>
		<div class="col-12">
            <div class="form-group">
                <label class="required-field">Phone</label>
                <input type="text" class="form-control" name="Phone" id="txtPhone" data-message-invalid-data="Phone is invalid" data-format="phone" placeholder="Phone Number" maxlength="20" />
            </div>
        </div>
		<div class="col-12">
            <div class="form-group">
                <label>Fax</label>
                <input type="text" class="form-control" name="Fax" id="txtFax" placeholder="Fax" maxlength="20" />
            </div>
        </div>
        <div class="col-12">
            <div class="form-group">
                <label>Default LPQ WorkerID</label>
                <input type="text" class="form-control" name="DefaultLpqWorkerID" id="txtDefaultLpqWorkerID" maxlength="50" />
            </div>
        </div>
		<div class="col-12">
            <div class="form-group">
                <label>LPQ Dealer Number or ClinicID</label>
                <input type="text" class="form-control" name="DealerNumberLpq" id="txtDealerNumberLpq" maxlength="50" />
            </div>
        </div>
    </div>
</form>
</div>