﻿
Partial Class Sm_Uc_RevisionChanges
	Inherits SmBaseUserControl

	Public Property LenderConfigLogId As Guid
	Protected LogList As List(Of CLenderConfigLog)
	Protected LogItem As SmLenderConfigLogItem
	Protected PrevLogItem As SmLenderConfigLogItem
	Protected Changes As String
	Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
		If Not IsPostBack Then
			Try
				LogItem = smBL.GetLenderConfigLogItemByID(LenderConfigLogId)
				If LogItem IsNot Nothing Then
					PrevLogItem = smBL.GetLenderConfigLogItemByRevisionID(LogItem.LenderConfigID, LogItem.RevisionID - 1)
					If PrevLogItem IsNot Nothing Then
						Changes = ConfigComparer(LogItem, PrevLogItem)
					End If
				End If
				
			Catch ex As Exception
				log.Error("Could not load revision changes", ex)
			End Try
		End If
	End Sub

	Function ConfigComparer(logItem As SmLenderConfigLogItem, previousLogItem As SmLenderConfigLogItem) As String
		Dim sbDiff As New StringBuilder()
		Dim diffGramStr As String = ""
		Dim xmlComparer As New CXmlComparer(previousLogItem.ConfigData, logItem.ConfigData)
		If xmlComparer.Compare(diffGramStr) = False Then
			xmlComparer.ParseDiffGram(diffGramStr, sbDiff)
		End If
		Return sbDiff.ToString()
	End Function

	Sub PropertyComparer(sourceValue As String, destValue As String, propName As String, ByRef sbDiff As StringBuilder)
		If sourceValue <> destValue Then
			sbDiff.AppendFormat("<p>{0}: {1} --> {2}</p>", propName, sourceValue, destValue)
		End If
	End Sub
End Class
