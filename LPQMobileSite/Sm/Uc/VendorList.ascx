﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="VendorList.ascx.vb" Inherits="Sm_Uc_VendorList" %>

<%@Import Namespace="Sm" %>
<%@ Register Src="~/Sm/Uc/Paging.ascx" TagPrefix="uc" TagName="GridFooter" %>
<table class="table table-hover">
    <thead>
        <tr>
			<th>#</th>
			<th>Vendor</th>
			<th>Type</th>
			<th>Logo</th>
			<th>Address</th>
			<th>Phone/Fax</th>
			<th>Vendor Groups</th>
			<th>Default LPQ WorkerID</th>
			<th>Dealer Number/ClinicID</th>
			<th class="center">Active</th>
			<th></th>
		</tr>
    </thead>
    <tbody>
        <%If LenderVendorList IsNot Nothing AndAlso LenderVendorList.Any() Then
        		Dim idx As Integer = (PagingInfo.PageIndex - 1) * PagingInfo.PageSize
        		For Each vendor As SmLenderVendor In LenderVendorList
        			idx = idx + 1
        %>
		 <tr data-id="<%=vendor.LenderVendorID%>">
				<td><%=idx%></td>
				<td><%=vendor.VendorName%></td>
				 <td style="max-width: 250px">
					 <%If vendor.Type IsNot Nothing AndAlso vendor.Type.Any() Then
					 		 For Each item In vendor.Type
					 			 
					 %>
					 <span class="item-inline color-<%=item.Substring(0, 2)%>"><%=CType([Enum].Parse(GetType(SmSettings.LenderVendorType), item), SmSettings.LenderVendorType).GetEnumDescription()%></span>
					 <%--<%=String.Join(", ", vendor.Type.Select(Function(p) CType([Enum].Parse(GetType(SmSettings.LenderVendorType), p), SmSettings.LenderVendorType).GetEnumDescription()).ToList())%>--%>
					 <%
					 Next
					End If%>
				 </td>
				 <td>
					 <%If Not String.IsNullOrEmpty(vendor.Logo) Then%>
					 <img width="90" src="<%=vendor.Logo%>" class="img-responsive"/>
					 <%End If%>
				 </td>
				<td>
					<p class="no-margin"><%=vendor.Address%></p>
					<p class="no-margin"><%=vendor.City%>, <%=vendor.State%> <%=vendor.Zip%></p>
				</td>
				<td>
					<p><%=Regex.Replace(vendor.Phone, "(\d{3})(\d{3})(\d{4})", "Phone: $1-$2-$3")%></p>
					<p><%=Regex.Replace(vendor.Fax, "(\d{3})(\d{3})(\d{4})", "Fax: $1-$2-$3")%></p>
				</td>
				<td style="max-width: 250px">
					 <%If vendor.TagList.Any() Then
					 		 For Each item In vendor.TagList
					 			 
					 %>
					 <span class="item-inline color-vl"><%=item%></span>
					 <%
					 Next
					End If%>
				 </td>
				 <td><%=vendor.DefaultLpqWorkerID%></td>
				 <td><%=vendor.DealerNumberLpq%></td>
				<td class="center">
					<label class="toggle-btn" data-on="ON" data-off="OFF">
						<input type="checkbox" data-command="enable-btn" <%=SmUtil.BindCheckbox(IIf(vendor.Status = SmSettings.LenderVendorStatus.Active, True, False))%>/>
						<span class="button-checkbox"></span>
					</label>
				</td>
				<td>
					<a href="#" data-command="edit">
						<span>Edit</span>
					</a>
				</td>
			</tr>
        <%
        	Next
	End If
        %>
    </tbody>
    <tfoot>
        <tr>
            <td colspan="11">
                <uc:GridFooter ID="ucGridFooter" runat="server"></uc:GridFooter>
            </td>
        </tr>
    </tfoot>
</table>