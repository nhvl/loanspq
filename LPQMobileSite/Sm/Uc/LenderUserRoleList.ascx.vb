﻿
Partial Class Sm_Uc_LenderUserRoleList
	Inherits SmBaseUserControl

	Protected LenderUserRoleList As List(Of SmUserRole)
	Public Property PagingInfo As SmPagingInfoModel
	Public Property FilterInfo As SmLenderUserRoleListFilter
	Public Property LenderConfigID As Guid

	Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
		If Not IsPostBack Then
			Try
				LenderUserRoleList = smBL.GetLenderUserRoles(PagingInfo, FilterInfo)
				ucGridFooter.PagingInfo = PagingInfo
			Catch ex As Exception
				log.Error("Could not load lender user role list", ex)
			End Try
		End If
	End Sub
End Class
