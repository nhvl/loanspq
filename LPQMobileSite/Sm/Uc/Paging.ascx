﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="Paging.ascx.vb" Inherits="Sm_Uc_Paging" %>
	<%If PagingVisible = true then %>
    <div class="form-row">
        <div class="col-sm-4 col-12">
            <div class="paging-info"><label><%=PagingLabel%></label></div>
        </div>
        <div class="pagination-wrapper col-12 col-sm-8" total-page="<%=PagingInfo.TotalPage%>">
        </div>
    </div>    
	<%End If %>
<%If PagingVisible = False AndAlso PagingInfo.TotalRecord = 0 Then%>
<div id="divNoRecordFound" class="no-record" style="text-align: center; color: forestgreen;">No record found.</div>
<%End If%>
