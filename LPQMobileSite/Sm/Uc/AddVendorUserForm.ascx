﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="AddVendorUserForm.ascx.vb" Inherits="Sm_Uc_AddVendorUserForm" %>
<div style="padding: 20px; max-height: 500px; overflow: auto;">
<form id="frm_addVendorUser">
	<%--<%If Usr Is Nothing Then%>--%>
    <div class="row">
        <div class="col-12">
            <div class="form-group">
                <label class="required-field">Email</label>
                <input type="text" class="form-control" value="" name="Email" id="txtEmail" data-type="email" placeholder="Email" data-message-invalid-data="Email is invalid" data-format="email" data-message-require="Please fill out this field" data-required="true" maxlength="50" />
            </div>
        </div>
    </div>
	<%--<%Else%>
	<div class="row">
		<div class="col-12">
            <div class="form-group">
                <label class="required-field">Email</label>
				<input type="text" readonly="readonly" class="form-control" value="<%=IIf(Not String.IsNullOrEmpty(Email), Email, "")%>" name="Email" id="txtEmail" data-type="email" placeholder="Email" data-message-invalid-data="Email is invalid" data-format="email" data-message-require="Please fill out this field" data-required="true" maxlength="50" />
				<input type="hidden" name="UserID" value="<%=Usr.UserID%>"/>
            </div>
        </div>
	</div>
	<%End If%>--%>
	<div class="row">
		<div class="col-12">
			<%--<%If SmUtil.CheckRoleAccess(HttpContext.Current, SmSettings.Role.VendorAdmin) Then%>
			<div class="form-group">
                <label class="required-field">Vendor</label>
				<select class="form-control" name="VendorID" id="ddlVendor" data-message-require="Please select vendor" data-required="true">
					<option value="">Select vendor</option>
					<%For Each v In LenderVendorList%>
					<option value="<%=v.VendorID%>"><%=v.VendorName%></option>
					<%Next%>
				</select>
            </div>
			<%Else%>--%>
			<div class="form-group" style="display: flow-root;">
                <label class="required-field">Vendor</label>
				<div>
					<%If SmUtil.CheckRoleAccess(HttpContext.Current, SmSettings.Role.SuperUser.ToString(), String.Format("LenderScope${0}${1}", SmSettings.Role.LenderAdmin.ToString(), LenderConfig.LenderID.ToString()), String.Format("PortalScope${0}${1}${2}", SmSettings.Role.PortalAdmin.ToString(), LenderConfig.LenderID.ToString(), LenderConfig.LenderConfigID.ToString())) Then%>	
					<div style="float: left; width: 75%">
					<%Else %>
					<div>
					<%End If%>
					
						<select class="form-control" name="VendorID" id="ddlVendor" data-message-require="Please select vendor" data-required="true">
						   <option value="">Select vendor</option>
							<%For Each v In LenderVendorList%>
						   <option value="<%=v.VendorID%>"><%=v.VendorName%></option>
						   <%Next%>
						</select>
					</div>
					<%If SmUtil.CheckRoleAccess(HttpContext.Current, SmSettings.Role.SuperUser.ToString(), String.Format("LenderScope${0}${1}", SmSettings.Role.LenderAdmin.ToString(), LenderConfig.LenderID.ToString()), String.Format("PortalScope${0}${1}${2}", SmSettings.Role.PortalAdmin.ToString(), LenderConfig.LenderID.ToString(), LenderConfig.LenderConfigID.ToString())) Then%>	
					<div style="float: right"><button class="btn btn-secondary" type="button" id="btnAddNewVendor">Add New</button></div>	
					<%End If%>
				</div>
            </div>
			<%--<%End If%>--%>
            
        </div>
	</div>
	<div class="row clearfix">
		<div class="col-12 <%=IIf(AvailableRoles.Count = 1, "hidden", "")%>">
            <div class="form-group">
                <label class="required-field">Role</label>
                <select class="form-control" name="Role" id="ddlRole" data-message-require="Please select role" data-required="true">
					<%=SmUtil.RenderDropdownFromRoleEnum(AvailableRoles)%>
                </select>
            </div>
        </div>
	</div>
</form>
</div>