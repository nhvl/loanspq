﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="RevisionChanges.ascx.vb" Inherits="Sm_Uc_RevisionChanges" %>
<table class="table table-hover" cellpadding="0" cellspacing="0">
	<thead>
        <tr>
			<th>Modified Date</th>
			<th>Modifier By</th>
			<th>Revision</th>
			<th>Changes</th>
		</tr>
    </thead>
	<%If Not String.IsNullOrEmpty(Changes) Then%>
	<tbody>
		<tr>
			<td nowrap="nowrap">
				<%=LogItem.LogTime.ToString("MM/dd/yyyy hh:mm tt")%>
			</td>
			<%If LogItem.LastModifiedByUserID <> Guid.Empty AndAlso LogItem.LastModifiedBy IsNot Nothing Then%>
			<td><%=LogItem.LastModifiedBy.FullName%></td>
			<%Else%>
			<td></td>
			<%End If%>
			<td>
				<%=LogItem.RevisionID%>
			</td>
			<td>
				<%=Changes%>
			</td>                                    
		</tr>
	</tbody>
	<%Else%>
	<tfoot>
        <tr>
            <td colspan="3" style="text-align: center;">No item found</td>
        </tr>
    </tfoot>
	<%End If%>
</table>