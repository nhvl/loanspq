﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="ActionLogList.ascx.vb" Inherits="Sm_Uc_ActionLogList" %>
<%@ Register Src="~/Sm/Uc/Paging.ascx" TagPrefix="uc" TagName="GridFooter" %>
<table class="table table-hover lender-list">
    <thead>
        <tr>
			<th style="min-width: 40px;">#</th>
			<th>Log Date</th>
			<th>Action</th>
			<th>Note</th>
			<th>By</th>
			<th>IP Address</th>
			<th>Category</th>
		</tr>
    </thead>
    <tbody>
        <%If logList IsNot Nothing AndAlso logList.Any() Then
        		Dim idx As Integer = (PagingInfo.PageIndex - 1) * PagingInfo.PageSize
        		For Each log As SmActionLogView In logList.Take(PagingInfo.PageSize)
        			idx = idx + 1
        %>
		 <tr>
			 <td><%=idx%></td>
			 <td nowrap="nowrap"><%=log.TransactionDate.ToString("MM/dd/yyyy HH:mm:ss")%></td>
			 <td nowrap="nowrap"><%=log.Action%></td>
			 <td nowrap="nowrap"><%=log.ActionNote%></td>
			 <td nowrap="nowrap"><%=log.ActionBy%></td>
			 <td nowrap="nowrap"><%=log.IpAddress%></td>
			 <td nowrap="nowrap"><%=log.Category%></td>
		</tr>
        <%
        	Next
	End If
        %>
    </tbody>
    <tfoot>
        <tr>
            <td colspan="7">
                <uc:GridFooter ID="ucGridFooter" runat="server"></uc:GridFooter>
            </td>
        </tr>
    </tfoot>
</table>