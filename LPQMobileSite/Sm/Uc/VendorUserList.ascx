﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="VendorUserList.ascx.vb" Inherits="Sm_Uc_VendorUserList" %>

<%@Import Namespace="Sm" %>
<%@ Import Namespace="LPQMobile.Utils" %>
<%@ Register Src="~/Sm/Uc/Paging.ascx" TagPrefix="uc" TagName="GridFooter" %>
<table class="table table-hover user-list">
    <thead>
        <tr>
			<th>#</th>
			<th>User</th>
			<th>Vendor</th>
			<th>Default LPQ WorkerID</th>
			<th>Dealer Number/ClinicID</th>
			<th>Status</th>
			<th class="center">Active</th>
			<th></th>
		</tr>
    </thead>
    <tbody>
        <%If UserRoleList IsNot Nothing AndAlso UserRoleList.Any() Then
        		Dim idx As Integer = (PagingInfo.PageIndex - 1) * PagingInfo.PageSize
        		Dim loginFailedCountExceed = Common.SafeInteger(ConfigurationManager.AppSettings("LoginFailedCountExceed"))
        		For Each userRole As SmUserRole In UserRoleList
        			idx = idx + 1
        			Dim userStatus = userRole.User.Status
        			If String.IsNullOrEmpty(userRole.User.FullName.Trim()) Then
        				userStatus = SmSettings.UserStatus.InActive
        			ElseIf userStatus = SmSettings.UserStatus.Active AndAlso (userRole.User.LoginFailedCount >= loginFailedCountExceed OrElse (userRole.User.LastLoginDate.HasValue AndAlso userRole.User.LastLoginDate.Value.AddDays(userRole.User.ExpireDays) < DateTime.Now)) Then
        				userStatus = SmSettings.UserStatus.Locked
        			End If
        %>
		 <tr data-id="<%=userRole.UserRoleID%>">
				<td><%=idx%></td>
				<td>
					<div class="user-info">
						<div>
							<div class="avatar-wrapper"><span class="avatar" style="background-image: url('<%=IIf(String.IsNullOrEmpty(userRole.User.Avatar.Trim()), "/images/avatar.jpg", userRole.User.Avatar)%>')"></span></div>
						</div>
						<div>
							<p style="font-weight: bold; margin-bottom: 4px;"><%=userRole.User.Email%></p>
							<p style="font-size: 0.9em; font-style: italic;">
								<%If not String.IsNullOrEmpty(userRole.User.FullName.Trim()) Then%>
								<%=userRole.User.FullName%> - <i class="fa fa-phone" style="width: auto;" aria-hidden="true"></i> <%=Regex.Replace(userRole.User.Phone, "(\d{3})(\d{3})(\d{4})", "$1-$2-$3")%>
								<%End If%>
							</p>
						</div>
					</div>
					
				</td>
			 	<td>
					<div style="font-weight: bold; margin-bottom: 4px;"><%=userRole.Vendor.VendorName%> (<%=userRole.Vendor.City%>, <%=userRole.Vendor.State%> <%=userRole.Vendor.Zip%>)</div>
					<div style="font-size: 0.9em; font-style: italic;"><i class="fa fa-phone" aria-hidden="true"></i> <%=Regex.Replace(userRole.Vendor.Phone, "(\d{3})(\d{3})(\d{4})", "$1-$2-$3")%>, <i class="fa fa-fax" aria-hidden="true"></i>  <%=Regex.Replace(userRole.Vendor.Fax, "(\d{3})(\d{3})(\d{4})", "$1-$2-$3")%></div>
				</td>
				<td><%=userRole.Vendor.DefaultLpqWorkerID%></td>
				<td><%=userRole.Vendor.DealerNumberLpq%></td>
				<td class="center"><%=userStatus.GetEnumDescription()%></td>
				<td class="center">
					<label class="toggle-btn" data-on="ON" data-off="OFF">
						<input type="checkbox" data-command="enable-btn" <%=SmUtil.BindCheckbox(userRole.Status = SmSettings.UserRoleStatus.Active)%>/>
						<span class="button-checkbox"></span>
					</label>
				</td>
				<td style="text-align: right;" data-id="<%=userRole.UserID%>">
					<%If userStatus = SmSettings.UserStatus.Locked Then%>
					<a href="#" data-command="unlock">
						<span>Unlock</span>
					</a>
					<span>|</span>
					<%End If %>
					<a href="#" data-command="delete">
						<span>Delete</span>
					</a>
				</td>
			</tr>
        <%
        	Next
	End If
        %>
    </tbody>
    <tfoot>
        <tr>
            <td colspan="8">
                <uc:GridFooter ID="ucGridFooter" runat="server"></uc:GridFooter>
            </td>
        </tr>
    </tfoot>
</table>