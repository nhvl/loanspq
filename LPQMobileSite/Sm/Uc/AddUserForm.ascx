﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="AddUserForm.ascx.vb" Inherits="Sm_Uc_AddUserForm" %>
<div style="padding: 20px; max-height: 500px; overflow: auto;">
<form id="frm_adduser">
    <div class="row">
        <%--<div class="col-12">
            <div class="form-group">
                <label class="required-field">First Name</label>
                <input type="text" class="form-control" name="FirstName" id="txtFirstName" placeholder="First Name" data-message-require="Please fill out this field" data-required="true" maxlength="50" />
            </div>
        </div>
        <div class="col-12">
            <div class="form-group">
                <label class="required-field">Last Name</label>
                <input type="text" class="form-control" name="LastName" id="txtLastName" placeholder="Last Name" data-message-require="Please fill out this field" data-required="true" maxlength="50" />
            </div>
        </div>--%>
        <div class="col-12">
            <div class="form-group">
                <label class="required-field">Email</label>
                <input type="text" class="form-control" value="<%=IIf(Not String.IsNullOrEmpty(Email), Email, "")%>" name="Email" id="txtEmail" data-type="email" placeholder="Email" data-message-invalid-data="Email is invalid" data-format="email" data-message-require="Please fill out this field" data-required="true" maxlength="50" />
            </div>
        </div>
        <%--<div class="col-12">
            <div class="form-group">
                <label class="required-field">Phone Number</label>
                <input type="text" class="form-control" name="Phone" id="txtPhone" data-message-invalid-data="Phone is invalid" data-format="phone" placeholder="Phone Number" maxlength="20" />
            </div>
        </div>
		<div class="col-12">
            <div class="form-group">
                <label>Avatar</label>
                <div class="logo-upload-zone dropzone" id="divAvatar"></div>
				<input type="hidden" value="" name="Avatar" id="hdAvatar"/>
            </div>
        </div>--%>
        <div class="col-12">
            <div class="form-group">
                <label>Expire Days</label>
                <input type="text" class="form-control" name="ExpireDays" id="txtExpireDays" placeholder="Expire Days" data-message-invalid-data="Expire Days is invalid" data-format="positive-number"/>
            </div>
        </div>
        <div class="col-12 <%=IIf(AvailableRoles.Count = 1, "hidden", "")%>">
            <div class="form-group">
                <label class="required-field">Role</label>
                <select class="form-control" name="Role" id="ddlRole" data-message-require="Please select role" data-required="true">
                    <%--<%=SmUtil.RenderDropdownFromEnum(GetType(SmSettings.Role))%>--%>
					<%=SmUtil.RenderDropdownFromRoleEnum(AvailableRoles)%>
                </select>
            </div>
        </div>
    </div>
</form>
</div>