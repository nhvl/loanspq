﻿
Imports System.Xml
Imports DownloadedSettings
Imports Sm
Partial Class Sm_ManageBL
	Inherits SmApmBasePage
	Protected Property VehiclePreapprovedMessage As String
	Protected Property VehicleSubmittedMessage As String
	Protected Property CreditCardPreapprovedMessage As String
	Protected Property CreditCardSubmittedMessage As String
	Protected Property OtherPreapprovedMessage As String
	Protected Property OtherSubmittedMessage As String
	Protected Property VehicleDisclosureList As New List(Of String)
	Protected Property CreditCardDisclosureList As New List(Of String)
	Protected Property OtherDisclosureList As New List(Of String)

	Protected Property BusinessPurposeList As New Dictionary(Of String, SmTextValueCatItem)(System.StringComparison.OrdinalIgnoreCase)

	Protected Property VehicleEnabledPurposeList As New List(Of String)(System.StringComparison.OrdinalIgnoreCase)
	Protected Property OtherEnabledPurposeList As New List(Of String)(System.StringComparison.OrdinalIgnoreCase)
	Protected Property CreditCardEnabledPurposeList As New List(Of String)(System.StringComparison.OrdinalIgnoreCase)

	Protected Property CardTypeList As New List(Of String)

	Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
		If Not Page.IsPostBack Then
			Dim smMasterPage = CType(Me.Master, Sm_SiteManager)
			smMasterPage.LenderConfigXml = LenderConfigXml
			smMasterPage.RevisionID = LenderConfig.RevisionID
			smMasterPage.LastModifiedBy = LenderConfig.LastModifiedByUserFullName
			smMasterPage.LastModifiedByUserID = LenderConfig.LastModifiedByUserID
			smMasterPage.SelectedMainMenuItem = "manage"
			smMasterPage.SelectedSubMenuItem = "bl"
			smMasterPage.CurrentNodeGroup = SmSettings.NodeGroup.ManageBL
			smMasterPage.BreadCrumb = "Manage / Business Loan"
			Dim vehiclePreapprovedMessageNode As XmlNode = GetNode(BL_VEHICLE_PREAPPROVED_MESSAGE)
			If vehiclePreapprovedMessageNode IsNot Nothing AndAlso vehiclePreapprovedMessageNode.NodeType <> XmlNodeType.Comment Then
				VehiclePreapprovedMessage = SmUtil.MergeMessageKey(vehiclePreapprovedMessageNode.Attributes("message").InnerText, vehiclePreapprovedMessageNode)
			End If
			Dim vehicleSubmittedMessageNode As XmlNode = GetNode(BL_VEHICLE_SUBMITTED_MESSAGE)
			If vehicleSubmittedMessageNode IsNot Nothing AndAlso vehicleSubmittedMessageNode.NodeType <> XmlNodeType.Comment Then
				VehicleSubmittedMessage = SmUtil.MergeMessageKey(vehicleSubmittedMessageNode.Attributes("message").InnerText, vehicleSubmittedMessageNode)
			End If
			Dim creditCardPreapprovedMessageNode As XmlNode = GetNode(BL_CARD_PREAPPROVED_MESSAGE())
			If creditCardPreapprovedMessageNode IsNot Nothing AndAlso creditCardPreapprovedMessageNode.NodeType <> XmlNodeType.Comment Then
				CreditCardPreapprovedMessage = SmUtil.MergeMessageKey(creditCardPreapprovedMessageNode.Attributes("message").InnerText, creditCardPreapprovedMessageNode)
			End If
			Dim creditCardSubmittedMessageNode As XmlNode = GetNode(BL_CARD_SUBMITTED_MESSAGE())
			If creditCardSubmittedMessageNode IsNot Nothing AndAlso creditCardSubmittedMessageNode.NodeType <> XmlNodeType.Comment Then
				CreditCardSubmittedMessage = SmUtil.MergeMessageKey(creditCardSubmittedMessageNode.Attributes("message").InnerText, creditCardSubmittedMessageNode)
			End If

			Dim otherPreapprovedMessageNode As XmlNode = GetNode(BL_OTHER_PREAPPROVED_MESSAGE())
			If otherPreapprovedMessageNode IsNot Nothing AndAlso otherPreapprovedMessageNode.NodeType <> XmlNodeType.Comment Then
				OtherPreapprovedMessage = SmUtil.MergeMessageKey(otherPreapprovedMessageNode.Attributes("message").InnerText, otherPreapprovedMessageNode)
			End If
			Dim otherSubmittedMessageNode As XmlNode = GetNode(BL_OTHER_SUBMITTED_MESSAGE())
			If otherSubmittedMessageNode IsNot Nothing AndAlso otherSubmittedMessageNode.NodeType <> XmlNodeType.Comment Then
				OtherSubmittedMessage = SmUtil.MergeMessageKey(otherSubmittedMessageNode.Attributes("message").InnerText, otherSubmittedMessageNode)
			End If


			Dim vehicleDisclosuresNode As XmlNode = GetNode(BL_VEHICLE_DISCLOSURES())
			If vehicleDisclosuresNode IsNot Nothing AndAlso vehicleDisclosuresNode.HasChildNodes Then
				For Each node As XmlNode In vehicleDisclosuresNode.ChildNodes
					If node.NodeType = XmlNodeType.Comment Then Continue For
					VehicleDisclosureList.Add(node.InnerText.Trim())
				Next
			End If
			Dim creditCardDisclosuresNode As XmlNode = GetNode(BL_CARD_DISCLOSURES())
			If creditCardDisclosuresNode IsNot Nothing AndAlso creditCardDisclosuresNode.HasChildNodes Then
				For Each node As XmlNode In creditCardDisclosuresNode.ChildNodes
					If node.NodeType = XmlNodeType.Comment Then Continue For
					CreditCardDisclosureList.Add(node.InnerText.Trim())
				Next
			End If
			Dim otherDisclosuresNode As XmlNode = GetNode(BL_OTHER_DISCLOSURES())
			If otherDisclosuresNode IsNot Nothing AndAlso otherDisclosuresNode.HasChildNodes Then
				For Each node As XmlNode In otherDisclosuresNode.ChildNodes
					If node.NodeType = XmlNodeType.Comment Then Continue For
					OtherDisclosureList.Add(node.InnerText.Trim())
				Next
			End If


			''********
			''Purpose

			Dim downloadBusinessPurposes As List(Of DownloadedSettings.CListItem)
			'<OUTPUT><RESPONSE><FIELD_LIST name="Loan Purposes"><LIST_ITEM text="NEW CARD" value="NEW CARD" /><LIST_ITEM text="CREDIT LIMIT INCREASE" value="CREDIT LIMIT INCREASE" /></FIELD_LIST></RESPONSE></OUTPUT>
			downloadBusinessPurposes = CDownloadedSettings.BusinessPurposes(WebsiteConfig)
			If downloadBusinessPurposes IsNot Nothing AndAlso downloadBusinessPurposes.Any() Then
				For Each item As CListItem In downloadBusinessPurposes
					If item.Value Is Nothing OrElse item.Value = "" Then Continue For
					If BusinessPurposeList.ContainsKey(item.Value) Then Continue For
					BusinessPurposeList.Add(item.Value, New SmTextValueCatItem() With {.text = item.Text, .value = item.Value, .category = ""})
				Next
			End If

			Dim enabledVehiclePurposesNode As XmlNode = GetNode(BL_VEHICLE_PURPOSES())
			If enabledVehiclePurposesNode IsNot Nothing AndAlso enabledVehiclePurposesNode.HasChildNodes Then
				For Each node As XmlNode In enabledVehiclePurposesNode.ChildNodes
					If node.NodeType = XmlNodeType.Comment Then Continue For
					Dim value As String = node.Attributes("value").Value
					If Not VehicleEnabledPurposeList.Contains(value) Then
						VehicleEnabledPurposeList.Add(value)
					End If

					'need to update download purposes with category from xml config
					If BusinessPurposeList.ContainsKey(value) AndAlso node.Attributes("category") IsNot Nothing Then
						Dim category As String = node.Attributes("category").Value
						BusinessPurposeList(value) = New SmTextValueCatItem() With {.text = BusinessPurposeList(value).text, .value = BusinessPurposeList(value).value, .category = category}
					End If

				Next
			End If
			Dim enabledCreditCardPurposesNode As XmlNode = GetNode(BL_CREDIT_CARD_PURPOSES())
			If enabledCreditCardPurposesNode IsNot Nothing AndAlso enabledCreditCardPurposesNode.HasChildNodes Then
				For Each node As XmlNode In enabledCreditCardPurposesNode.ChildNodes
					If node.NodeType = XmlNodeType.Comment Then Continue For
					Dim value As String = node.Attributes("value").Value
					If Not CreditCardEnabledPurposeList.Contains(value) Then
						CreditCardEnabledPurposeList.Add(value)
					End If

					'need to update download purposes with category from xml config
					If BusinessPurposeList.ContainsKey(value) AndAlso node.Attributes("category") IsNot Nothing Then
						Dim category As String = node.Attributes("category").Value
						BusinessPurposeList(value) = New SmTextValueCatItem() With {.text = BusinessPurposeList(value).text, .value = BusinessPurposeList(value).value, .category = category}
					End If
				Next
			End If
			Dim enabledOtherPurposesNode As XmlNode = GetNode(BL_OTHER_PURPOSES())
			If enabledOtherPurposesNode IsNot Nothing AndAlso enabledOtherPurposesNode.HasChildNodes Then
				For Each node As XmlNode In enabledOtherPurposesNode.ChildNodes
					If node.NodeType = XmlNodeType.Comment Then Continue For
					Dim value As String = node.Attributes("value").Value
					If Not OtherEnabledPurposeList.Contains(value) Then
						OtherEnabledPurposeList.Add(value)
					End If

					'need to update download purposes with category from xml config
					If BusinessPurposeList.ContainsKey(value) AndAlso node.Attributes("category") IsNot Nothing Then
						Dim category As String = node.Attributes("category").Value
						BusinessPurposeList(value) = New SmTextValueCatItem() With {.text = BusinessPurposeList(value).text, .value = BusinessPurposeList(value).value, .category = category}
					End If
				Next
			End If

			Dim cardTypesNode As XmlNode = GetNode(BL_CREDIT_CARD_TYPES())
			If cardTypesNode IsNot Nothing AndAlso cardTypesNode.HasChildNodes Then
				For Each node As XmlNode In cardTypesNode.ChildNodes
					If node.NodeType = XmlNodeType.Comment Then Continue For
					CardTypeList.Add(node.Attributes("value").InnerText)
				Next
			End If
		End If
	End Sub

End Class

