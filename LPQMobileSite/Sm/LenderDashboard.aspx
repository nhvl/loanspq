﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="LenderDashboard.aspx.vb" Inherits="Sm_LenderDashboard" MasterPageFile="MasterPage.master" %>
<%@ Import Namespace="LPQMobile.Utils" %>

<asp:Content runat="server" ContentPlaceHolderID="plBody">
	<div>
		<h1 class="page-header">Lender Dashboard - <%=BackOfficeLender.LenderName %></h1>
		<p>Manage information for this lender.</p>
		<section id="dashboard">
			<ul class="nav nav-tabs" role="tablist">
				<%-- Synchronize permissions with LenderSecurityDashboard.aspx and LenderList.ascx (for whether or not to show dropdown/Dashboard link) --%>
				<%If LenderAdminAllowed AndAlso SmUtil.CheckRoleAccess(HttpContext.Current, SmSettings.Role.SuperOperator, SmSettings.Role.LegacyOperator) Then%>
					<li role="presentation" ><a href="#info" aria-controls="info" role="tab" data-toggle="tab">Profile</a></li>
				<%End If%>
				<%If LenderAdminAllowed AndAlso CanManageUsers Then%>
				    <li role="presentation"><a href="#users" aria-controls="users" role="tab" data-toggle="tab">Users</a></li>
				<%End If%>
				<%--TODO: remove IsInEnvironment when ready to deploy--%>
				<%If LenderAdminAllowed AndAlso IsInEnvironment("DEV", "STAGE") Then%>
				<li role="presentation"><a href="#vendors" aria-controls="vendors" role="tab" data-toggle="tab">Vendors</a></li>
				<%End If%>
				<li role="presentation"><a href="<%=BuildUrl("/sm/lendersecuritydashboard.aspx")%>#security" aria-controls="security" role="tab">Security</a></li>

				<%--<li role="presentation"><a href="#history" aria-controls="history" role="tab" data-toggle="tab">History</a></li>--%>
			</ul>
			<div class="tab-content">
				<%If LenderAdminAllowed AndAlso SmUtil.CheckRoleAccess(HttpContext.Current, SmSettings.Role.SuperOperator, SmSettings.Role.LegacyOperator) Then%>
				<div role="tabpanel" class="tab-pane " id="info">
					<%--<h2 class="section-title">Edit profile</h2>--%>
					<form id="frmLenderInfo">
						<div class="row">
							<div class="col-sm-6 col-lg-4">
								<div class="form-group">
									This panel is only visible for legacy operator. <br /><br />
									<label for="txtLenderName">Lender Name</label>
									<input type="text" name="LenderName" data-message-require="Please fill out this field" data-required="true" class="form-control" value="<%=BackOfficeLender.LenderName%>" id="txtLenderName" placeholder="Enter lender name">
									<input type="hidden" name="BackOfficeLenderID" value="<%=BackOfficeLender.BackOfficeLenderID%>"/>
									<input type="hidden" name="LenderID" value="<%=BackOfficeLender.LenderID%>"/>
									<input type="hidden" name="LpqUrl" value="<%=BackOfficeLender.LpqUrl%>"/>									
								</div>
								<div class="form-group">
									<label for="txtCustomerInternalID">Customer Internal ID</label>
									<input type="text" class="form-control" name="CustomerInternalID" id="txtCustomerInternalID" placeholder="Enter Customer Internal ID" value="<%=BackOfficeLender.CustomerInternalID%>">
								</div>
								<div class="form-group">
									<label for="txtCity">City</label>
									<input type="text" class="form-control" name="City" id="txtCity" data-message-require="Please fill out this field" data-required="true" placeholder="Enter city" value="<%=BackOfficeLender.City%>">
								</div>
								<div class="form-group">
									<label for="ddlState">State</label>
									<select class="form-control" name="State" id="ddlState" data-message-require="Please select state" data-required="true">
										<%=SmUtil.RenderDropdownStateOptions(BackOfficeLender.State)%>
									</select>
								</div>
								<div>
									<button type="button" id="btnSaveLenderProfile" class="btn btn-primary pull-right">Save changes</button>
								</div>
							</div>
						</div>
					</form>
				</div>
				<%End If%>
				<%If LenderAdminAllowed AndAlso CanManageUsers Then%>
				<div role="tabpanel" class="tab-pane active" id="users">
					<h2 class="section-title">List of users who have permission to setup all portals belonging to this lender</h2>
					<div>
						<div class="pull-left search-bar">
							<div class="input-group">
								<input id="txtSearch" type="text" class="form-control" placeholder="Search for..." />
								<span class="input-group-btn"><button class="btn btn-secondary" type="button" id="btnSearch">Search</button></span>
							</div>	
						</div>
						<div class="pull-right add-panel">
							<div class="input-group">
								<input id="txtAddLenderUser" type="text" class="form-control" placeholder="Email" />
								<span class="input-group-btn"><button class="btn btn-secondary" type="button" id="btnAddLenderUser">Add</button></span>
							</div>
						</div>
					</div>
					<div class="row grid" id="divLenderUserRoleList">
						<div class="spinner">
							<div class="rect1"></div>
							<div class="rect2"></div>
							<div class="rect3"></div>
							<div class="rect4"></div>
							<div class="rect5"></div>
						</div>
						<p>Loading...</p>
					</div>
				</div>
				<%End If%>
				<%If LenderAdminAllowed AndAlso IsInEnvironment("DEV", "STAGE") Then%>
				<div role="tabpanel" class="tab-pane" id="vendors">
					<h2 class="section-title">List of vendors that are associated with this lender(dev phase, only available on appstage)</h2>
					Enter @[vendor group name] to search vendors by group name <br />
					<div>
						<div class="pull-left search-bar">
							<div class="input-group">                                
								<input id="txtSearchVendor" type="text" class="form-control" placeholder="Search for..." />
								<span class="input-group-btn"><button class="btn btn-secondary" type="button" id="btnSearchVendor">Search</button></span>
							    
                            </div>	
						</div>
                        
						<div class="pull-right add-panel">
							<button class="btn btn-secondary" type="button" id="btnAddVendor">Add Vendor</button>
						</div>
					</div>
					<div class="row grid" id="divVendorList"></div>
				</div>
				<%End If%>
				<%--<div role="tabpanel" class="tab-pane fade" id="history">
					<h2 class="section-title">The activity logs list actions performed on current lender</h2>
				</div>--%>
			</div>
		</section>
	</div>
</asp:Content>
<asp:Content runat="server" ContentPlaceHolderID="plScripts">
	<script type="text/javascript">
	
		$(function () {
			$('.nav-tabs a').on('shown.bs.tab', function (e) {
				window.location.hash = e.target.hash;
				window.scrollTo(0, 0);
			});
			<%If LenderAdminAllowed Then%>
			manageLenderUserRoles.FACTORY.init();
			<%End If%>
            //Remove IsInEnvironment for deployment
			<%If LenderAdminAllowed AndAlso IsInEnvironment("DEV", "STAGE") Then%>
			manageVendors.FACTORY.init();
			<%End If%>

			registerDataValidator($("#frmLenderInfo"), "lender_info");
			$("#btnSaveLenderProfile").on("click", function() {
				if ($.smValidate("lender_info") == false) return;
				var lenderData = $("#frmLenderInfo").serializeJSON({ parseAll: true, checkboxUncheckedValue: "false" });
				//update data
				var postData = {
					command: "saveLenderInfo",
					lender_data: _COMMON.toJSON(lenderData)
				};
				$.ajax({
					url: "/sm/smhandler.aspx",
					async: true,
					cache: false,
					type: 'POST',
					dataType: 'html',
					data: postData,
					success: function (responseText) {
						var response = $.parseJSON(responseText);
						if (response.IsSuccess) {
							_COMMON.noty("success", "Success", 500);
							$("h1.page-header").text("Lender Dashboard - " + response.Info.name);
							$("#spLenderName").text(response.Info.name);
						} else {
							var errorMsg = response.Message !== "" ? response.Message : "Error";
							_COMMON.noty("error", errorMsg, 500);
						}
					}
				});
			});

			var url = document.location.toString();
			if (url.match('#') && $.trim(url.split('#')[1]) != "") {
				$('.nav-tabs a[href="#' + url.split('#')[1] + '"]').tab('show');
			} else {
				var tab = $("div.tab-content >.tab-pane:first-child").attr("id");
				if (tab != null && tab != "") {
					$('.nav-tabs a[href="#' + tab + '"]').tab('show');
				} else {
					<%-- Manually click first tab, which may lead to another page --%>
					var $firstLink = $(".nav-tabs>li:first-child").find("a");
					if ($firstLink.length > 0) {
						$firstLink[0].click();
					}
				}
			}
		});
		<%If LenderAdminAllowed Then%>
		(function (manageLenderUserRoles, $, undefined) {
			manageLenderUserRoles.DATA = {};
			manageLenderUserRoles.FACTORY = {};
			manageLenderUserRoles.FACTORY.init = function () {
				var lenderUserRoleGrid;
				lenderUserRoleGrid = new _COMMON.Grid("/sm/smhandler.aspx", "lenderUserRoleGrid", "divLenderUserRoleList", manageLenderUserRoles.FACTORY, manageLenderUserRoles.FACTORY.onLoaded, { command: "loadLenderUserRoleGrid", bid: '<%=BackOfficeLenderID%>' });
				lenderUserRoleGrid.init();
				manageLenderUserRoles.DATA.grid = lenderUserRoleGrid.GridObject;
				<%If SmUtil.CheckRoleAccess(HttpContext.current, SmSettings.Role.SuperUser) Then %>
				$('#txtAddLenderUser').typeahead({
					ajax: {
						url: '/sm/smhandler.aspx',
						method: 'POST',
						displayField: "name",
						valueField: "id",
						dataType: 'json',
						preDispatch: function () {
							return { query: $('#txtAddLenderUser').val(), command: "getAllLenderUsersForSuggest", lenderID: '<%=LenderID%>' };
						},
						preProcess: function (data) {
							return data;
						}
					},
					scrollBar: true,
					onSelect: function(item) {
					}
				});
				<%End If%>
				$('#txtAddLenderUser').on("focus", function() {
					$.smValidate.hideValidation($(this));
				});
				$('#btnAddLenderUser').on("click", function() {
					var email = $.trim($("#txtAddLenderUser").val());
					if (_COMMON.ValidateEmail(email) == true) {
						manageLenderUserRoles.FACTORY.AddLenderUser(email);
					} else {
						$.smValidate.showValidation($("#txtAddLenderUser"), "Invalid email");
					}
				});
				$("#btnSearch").on("click", manageLenderUserRoles.FACTORY.search);

				$('#txtSearch').on("keypress", function (e) {
					var code = (e.keyCode ? e.keyCode : e.which);
					if (code == 13) {
						manageLenderUserRoles.FACTORY.search();
					}
				});
			};
			manageLenderUserRoles.FACTORY.getFilterInfo = function () {
				//implement this later
				return { Keyword: $.trim($("#txtSearch").val()) };
			};
			manageLenderUserRoles.FACTORY.onLoaded = function (container) {
				$("input:checkbox[data-command='enable-btn']", $("#divLenderUserRoleList")).each(function (idx, ele) {
					var $self = $(ele);
					$self.on("change", function () {
						manageLenderUserRoles.FACTORY.ShowChangeStateDialog($self.closest("tr").data("id"), $self);
					});
				});
				$("a[data-command='delete']", $("#divLenderUserRoleList")).each(function (idx, ele) {
					var $self = $(ele);
					$self.on("click", function () {
						manageLenderUserRoles.FACTORY.ShowRemoveDialog($self.closest("tr").data("id"), $self);
					});
				});
				$("a[data-command='unlock']", $("#divLenderUserRoleList")).each(function (idx, ele) {
					var $self = $(ele);
					$self.on("click", function () {
						manageLenderUserRoles.FACTORY.UnlockUser($self.closest("td").data("id"), $self);
					});
				});
			};
			manageLenderUserRoles.FACTORY.ShowChangeStateDialog = function (id, srcEle) {
				_COMMON.showDialogSaveClose("Leave a comment", 0, "takenote_dialog_0", '<div style="margin:10px 20px;"><textarea data-id="txtComment" class="form-control required" rows="3" data-message-require="Comment cannot be empty" placeholder="Write a comment" maxlength="1000" display-counter-control="spanCommentCounter"></textarea><div class="text-right char-count"><span id="spanCommentCounter"></span></div><label for="txtComment" class="has-error" style="display:none"></label></div>', "", null, function(container) {
					var comment = $("textarea[data-id='txtComment']", $("#takenote_dialog_0")).val();
					manageLenderUserRoles.FACTORY.ChangeState(id, $(srcEle).is(":checked"), srcEle, comment, container);
				}, function(container) {
					$("textarea[data-id='txtComment']", $(container)).counter();
				}, function() {
					//reload grid when dialog closed
					var pageIndex = manageLenderUserRoles.DATA.grid.getPaginationSetting().page;
					manageLenderUserRoles.DATA.grid.setFilter(manageLenderUserRoles.FACTORY.getFilterInfo()).setPageIndex(pageIndex).load();
				});
			};
			manageLenderUserRoles.FACTORY.ChangeState = function (id, isEnabled, sourceEle, comment, container) {
				var dataObj = { userRoleID: id, comment: comment, lenderID: '<%=LenderID%>', bid: '<%=BackOfficeLenderID%>', enable: isEnabled ? "Y" : "N", command: "changeLenderUserRoleState" }
				$.ajax({
					url: "/sm/smhandler.aspx",
					async: true,
					cache: false,
					type: 'POST',
					dataType: 'html',
					data: dataObj,
					success: function (responseText) {
						var response = $.parseJSON(responseText);
						if (response.IsSuccess) {
							_COMMON.noty("success", "Success", 500);
							if (response.Info == "RELOAD") {
								window.location = window.location.href.replace(/#\w*$/, "");
							}
							var pageIndex = manageLenderUserRoles.DATA.grid.getPaginationSetting().page;
							manageLenderUserRoles.DATA.grid.setFilter(manageLenderUserRoles.FACTORY.getFilterInfo()).setPageIndex(pageIndex).load();
							container.dialog.close();
						} else {
							_COMMON.noty("error", "Error", 500);
							$(sourceEle).prop("checked", !isEnabled);
						}
					}
				});
			};
			manageLenderUserRoles.FACTORY.UnlockUser = function (id, sourceEle) {
				var dataObj = { userid: id, command: "unlockUser" }
				$.ajax({
					url: "/sm/smhandler.aspx",
					async: true,
					cache: false,
					type: 'POST',
					dataType: 'html',
					data: dataObj,
					success: function (responseText) {
						var response = $.parseJSON(responseText);
						if (response.IsSuccess) {
							_COMMON.noty("success", "Success", 500);
							var pageIndex = manageLenderUserRoles.DATA.grid.getPaginationSetting().page;
							manageLenderUserRoles.DATA.grid.setFilter(manageLenderUserRoles.FACTORY.getFilterInfo()).setPageIndex(pageIndex).load();
							container.dialog.close();
						} else {
							_COMMON.noty("error", "Error", 500);
							$(sourceEle).prop("checked", !isEnabled);
						}
					}
				});
			};
			manageLenderUserRoles.FACTORY.ShowRemoveDialog = function (id, srcEle) {
				_COMMON.showDialogSaveClose("Leave a comment", 0, "takenote_dialog_0", '<div style="margin:10px 20px;"><textarea data-id="txtComment" class="form-control required" rows="3" data-message-require="Comment cannot be empty" placeholder="Write a comment" maxlength="1000" display-counter-control="spanCommentCounter"></textarea><div class="text-right char-count"><span id="spanCommentCounter"></span></div><label for="txtComment" class="has-error" style="display:none"></label></div>', "", null, function (container) {
					var comment = $("textarea[data-id='txtComment']", $("#takenote_dialog_0")).val();
					manageLenderUserRoles.FACTORY.DeleteUser(id, srcEle, comment, container);
				}, function (container) {
					$("textarea[data-id='txtComment']", $(container)).counter();
				}, function () {
					//reload grid when dialog closed
					var pageIndex = manageLenderUserRoles.DATA.grid.getPaginationSetting().page;
					manageLenderUserRoles.DATA.grid.setFilter(manageLenderUserRoles.FACTORY.getFilterInfo()).setPageIndex(pageIndex).load();
				});
			};
			manageLenderUserRoles.FACTORY.DeleteUser = function (id, sourceEle, comment, container) {
				var dataObj = { userRoleID: id, comment: comment, lenderID: '<%=LenderID%>', bid: '<%=BackOfficeLenderID%>', command: "removeLenderUserRole" }
				$.ajax({
					url: "/sm/smhandler.aspx",
					async: true,
					cache: false,
					type: 'POST',
					dataType: 'html',
					data: dataObj,
					success: function(responseText) {
						var response = $.parseJSON(responseText);
						if (response.IsSuccess) {
							_COMMON.noty("success", "Success", 500);
							if (response.Info == "RELOAD") {
								window.location = window.location.href.replace(/#\w*$/, "");
							}
							var pageIndex = manageLenderUserRoles.DATA.grid.getPaginationSetting().page;
							manageLenderUserRoles.DATA.grid.setFilter(manageLenderUserRoles.FACTORY.getFilterInfo()).setPageIndex(pageIndex).load();
							container.dialog.close();
						} else {
							_COMMON.noty("error", "Error", 500);
						}
					}
				});
			};
			manageLenderUserRoles.FACTORY.AddLenderUser = function (email) {
				$("#btnAddLenderUser").button("loading");
				var dataObj = { email: email, lenderID: '<%=LenderID%>', bid: '<%=BackOfficeLenderID%>', command: "addLenderUser" }
				$.ajax({
					url: "/sm/smhandler.aspx",
					async: true,
					cache: false,
					type: 'POST',
					dataType: 'html',
					data: dataObj,
					success: function (responseText) {
						$("#btnAddLenderUser").button("reset");
						var response = $.parseJSON(responseText);
						if (response.IsSuccess) {
							_COMMON.noty("success", "Success", 500);
							var pageIndex = manageLenderUserRoles.DATA.grid.getPaginationSetting().page;
							manageLenderUserRoles.DATA.grid.setFilter(manageLenderUserRoles.FACTORY.getFilterInfo()).setPageIndex(pageIndex).load();
							$("#txtAddLenderUser").val("");
						} else if (response.Info == "ALREADY") {
							_COMMON.showAlertDialog(response.Message);
						} else if (response.Info == "UPGRADE") {
							//show confirm popup
							_COMMON.showConfirmDialog(response.Message, function () {
								//yes
								$.ajax({
									url: "/sm/smhandler.aspx",
									async: true,
									cache: false,
									type: 'POST',
									dataType: 'html',
									data: { email: email, command: 'upgradeToLenderAdmin', lenderID: '<%=LenderID%>', bid: '<%=BackOfficeLenderID%>' },
									success: function (respText) {
										var resp = $.parseJSON(respText);
										if (resp.IsSuccess) {
											_COMMON.noty("success", "Success.", 500);
											var pIndex = manageLenderUserRoles.DATA.grid.getPaginationSetting().page;
											manageLenderUserRoles.DATA.grid.setFilter(manageLenderUserRoles.FACTORY.getFilterInfo()).setPageIndex(pIndex).load();
											$("#txtAddLenderUser").val("");
										} else {
											_COMMON.noty("error", "Error", 500);
										}
									}
								});
							}, function() {
								//no
							}, "YES", "NO");
						} else {
							_COMMON.noty("error", response.Message, 500);
						}
					}
				});
			};
			manageLenderUserRoles.FACTORY.search = function () {
				manageLenderUserRoles.DATA.grid.setFilter(manageLenderUserRoles.FACTORY.getFilterInfo()).setPageIndex(1).load();
			};
		}(window.manageLenderUserRoles = window.manageLenderUserRoles || {}, jQuery));
		<%End If%>
		<%If LenderAdminAllowed AndAlso IsInEnvironment("DEV", "STAGE") Then%>
		(function (manageVendors, $, undefined) {
			function checkSelectedType() {
				var isValidType;
				if ($("input:checkbox:checked", ".vendor-type-lst").length == 0) {
					$.smValidate.showValidation(".vendor-type-lst", "Please select a type");
					isValidType = false;
				} else {
					$.smValidate.hideValidation(".vendor-type-lst");
					isValidType = true;
				}
				return isValidType;
			}
			manageVendors.DATA = {};
			manageVendors.FACTORY = {};
			manageVendors.FACTORY.init = function () {
				var vendorGrid;
				vendorGrid = new _COMMON.Grid("/sm/smhandler.aspx", "vendorGrid", "divVendorList", manageVendors.FACTORY, manageVendors.FACTORY.onLoaded, { command: "loadVendorGrid", bid: '<%=BackOfficeLenderID%>' });
				vendorGrid.init();
				manageVendors.DATA.grid = vendorGrid.GridObject;
				$('#btnAddVendor').on("click", manageVendors.FACTORY.showAddVendorForm);
				$("#btnSearchVendor").on("click", manageVendors.FACTORY.search);
				$('#txtSearchVendor').on("keypress", function (e) {
					var code = (e.keyCode ? e.keyCode : e.which);
					if (code == 13) {
						manageVendors.FACTORY.search();
					}
				});
			};
			manageVendors.FACTORY.getFilterInfo = function () {
				//implement this later
				return { Keyword: $.trim($("#txtSearchVendor").val()) };
			};
			manageVendors.FACTORY.onLoaded = function (container) {
				$("input:checkbox[data-command='enable-btn']", $("#divVendorList")).each(function (idx, ele) {
					var $self = $(ele);
					$self.on("change", function () {
						var lenderVendorId = $self.closest("tr").data("id");
						manageVendors.FACTORY.ShowChangeStateDialog(lenderVendorId, $self);
					});
				});
				$("a[data-command='edit']", $("#divVendorList")).each(function (idx, ele) {
					var $self = $(ele);
					$self.on("click", function () {
						var lenderVendorId = $self.closest("tr").data("id");
						manageVendors.FACTORY.showEditLenderVendorForm(lenderVendorId);
					});
				});

			};
			manageVendors.FACTORY.showEditLenderVendorForm = function (lenderVendorId) {
				_COMMON.showDialogSaveClose("Edit Vendor", 0, "editvendor_dialog_0", "", "/sm/smhandler.aspx", { command: "loadEditLenderVendorForm", lenderVendorID: lenderVendorId }, manageVendors.FACTORY.updateLenderVendor, function (container) {
					//add validator
					registerDataValidator($("#editvendor_dialog_0"), "editvendor_dialog_0");
					$("#divVendorLogo").dropzone({
						url: "/sm/smhandler.aspx",
						dictDefaultMessage: "<p>Drop logo file here or click to select image from your computer</p><p>Compatible file types: JPG, PNG. Recommended approximate dimension in pixel: 200 x 200.</p><p>Recommended file size: 10KB.</p>",
						maxFilesize: 2,
						acceptedFiles: "image/png,image/jpeg",
						params: { "command": "uploadlogo" },
						maxFiles: 1,
						addRemoveLinks: true,
						dictRemoveFile: "Remove",
						maxfilesexceeded: function (file) {
							this.removeAllFiles();
							this.addFile(file);
						},
						thumbnailWidth: 200,
						thumbnailHeight: null,
						init: function () {
							var $hdLogo = $("#hdLogo");
							manageVendors.DATA.currentLogoFile = null;
							if ($hdLogo.val() != "") {
								var mockFile = { name: $hdLogo.data("file-name"), size: $hdLogo.data("file-size") /*, type: 'image/png'*/, url: $("#hdLogo").val() };
								this.options.addedfile.call(this, mockFile);
								this.options.thumbnail.call(this, mockFile, $("#hdLogo").val());
								mockFile.previewElement.classList.add('dz-success');
								mockFile.previewElement.classList.add('dz-complete');
								$(mockFile.previewElement).find("img").attr("width", 200);
								manageVendors.DATA.currentLogoFile = mockFile;
							}
							this.on("addedfile", function (file) {
								if (manageVendors.DATA.currentLogoFile) {
									this.removeFile(manageVendors.DATA.currentLogoFile);
								}
								manageVendors.DATA.currentLogoFile = file;
							});
							this.on("success", function (file, response) {
								$("#hdLogo").val(response);
								$.smValidate.hideValidation($("#hdLogo").closest(".form-group"));
								manageVendors.DATA.currentLogoFile = file;
							});
							this.on("removedfile", function (file) {
								manageVendors.DATA.currentLogoFile = null;
								if (file.xhr) {
									$.ajax({
										url: "/sm/smhandler.aspx",
										async: true,
										cache: false,
										type: 'POST',
										dataType: 'html',
										data: { command: 'deletelogo', logourl: file.xhr.responseText },
										success: function(responseText) {
											$("#hdLogo").val("");
										}
									});
								} else {
									$("#hdLogo").val("");
								}
								
							});
						}
					});
					$("#editvendor_dialog_0 .input-group.date").each(function () {
						$(this).datepicker({
							clearBtn: true,
							autoclose: true,
							todayHighlight: true
						});
					});
					$("#editvendor_dialog_0 input:checkbox", "#editvendor_dialog_0 .vendor-type-lst").on("change", checkSelectedType);
					$("#editvendor_dialog_0 input[data-role=tagsinput]").tagsinput({
						trimValue: true,
						typeahead: {
							ajax: {
								url: '/sm/smhandler.aspx',
								method: 'POST',
								displayField: "name",
								valueField: "id",
								dataType: 'json',
								preDispatch: function () {
									return { query: "", command: "getAvailableTags", lenderID: '<%=LenderID%>' };
								},
								preProcess: function (data) {
									return data;
								}
							}
						}
					});
				}, function () {
					//reload grid when dialog closed
					var pageIndex = manageVendors.DATA.grid.getPaginationSetting().page;
					manageVendors.DATA.grid.setFilter(manageVendors.FACTORY.getFilterInfo()).setPageIndex(pageIndex).load();
				});
			};
			manageVendors.FACTORY.updateLenderVendor = function (obj) {
				if ($("#hdLogo").val() == "") {
					$.smValidate.showValidation($("#hdLogo").closest(".form-group"), "Please upload logo");
				} else {
					$.smValidate.hideValidation($("#hdLogo").closest(".form-group"));
				}
				var isValidType = checkSelectedType();
				if ($.smValidate("editvendor_dialog_0") == false || $("#hdLogo").val() == "" || isValidType == false) return;
				var vendorData = obj.container.find("form").serializeJSON({ parseAll: true });
				//update data
				var postData = {
					command: "updateLenderVendor",
					vendor_data: _COMMON.toJSON(vendorData)
				};
				$.ajax({
					url: "/sm/smhandler.aspx",
					async: true,
					cache: false,
					type: 'POST',
					dataType: 'html',
					data: postData,
					success: function (responseText) {
						var response = $.parseJSON(responseText);
						if (response.IsSuccess) {
							_COMMON.noty("success", "Success", 500);
							obj.dialog.close();
						} else {
							var errorMsg = response.Message !== "" ? response.Message : "Error";
							_COMMON.noty("error", errorMsg, 500);
						}
					}
				});
			};
			manageVendors.FACTORY.ShowChangeStateDialog = function (lenderVendorId, srcEle) {
				_COMMON.showDialogSaveClose("Leave a comment", 0, "takenote_dialog_0", '<div style="margin:10px 20px;"><textarea data-id="txtComment" class="form-control required" rows="3" data-message-require="Comment cannot be empty" placeholder="Write a comment" maxlength="1000" display-counter-control="spanCommentCounter"></textarea><div class="text-right char-count"><span id="spanCommentCounter"></span></div><label for="txtComment" class="has-error" style="display:none"></label></div>', "", null, function (container) {
					var comment = $("textarea[data-id='txtComment']", $("#takenote_dialog_0")).val();
					manageVendors.FACTORY.ChangeState(lenderVendorId, $(srcEle).is(":checked"), srcEle, comment, container);
				}, function (container) {
					$("textarea[data-id='txtComment']", $(container)).counter();
				}, function () {
					//reload grid when dialog closed
					var pageIndex = manageVendors.DATA.grid.getPaginationSetting().page;
					manageVendors.DATA.grid.setFilter(manageVendors.FACTORY.getFilterInfo()).setPageIndex(pageIndex).load();
				});
			};

			manageVendors.FACTORY.ChangeState = function (lenderVendorId, isEnabled, sourceEle, comment, container) {
				var dataObj = { lenderVendorID: lenderVendorId, comment: comment, bid: '<%=BackOfficeLenderID%>', lenderID: '<%=LenderID%>', enable: isEnabled ? "Y" : "N", command: "changeLenderVendorState" }
				$.ajax({
					url: "/sm/smhandler.aspx",
					async: true,
					cache: false,
					type: 'POST',
					dataType: 'html',
					data: dataObj,
					success: function (responseText) {
						var response = $.parseJSON(responseText);
						if (response.IsSuccess) {
							_COMMON.noty("success", "Success", 500);
							var pageIndex = manageVendors.DATA.grid.getPaginationSetting().page;
							manageVendors.DATA.grid.setFilter(manageVendors.FACTORY.getFilterInfo()).setPageIndex(pageIndex).load();
							container.dialog.close();
						} else {
							_COMMON.noty("error", "Error", 500);
							$(sourceEle).prop("checked", !isEnabled);
						}
					}
				});
			};
			
			manageVendors.FACTORY.showAddVendorForm = function () {
				_COMMON.showDialogSaveClose("Add New Vendor", 0, "addvendor_dialog_0", "", "/sm/smhandler.aspx", { command: "loadAddVendorForm" }, manageVendors.FACTORY.saveNewLenderVendor, function (container) {
					//add validator
					registerDataValidator($("#addvendor_dialog_0"), "addvendor_dialog_0");
					$("#divVendorLogo").dropzone({
						url: "/sm/smhandler.aspx",
						dictDefaultMessage: "<p>Drop logo file here or click to select image from your computer</p><p>Compatible file types: JPG, PNG. Recommended approximate dimension: 350 x 200.</p><p>Recommended file size: 10KB.</p>",
						maxFilesize: 2,
						acceptedFiles: "image/png,image/jpeg",
						params: { "command": "uploadlogo" },
						maxFiles: 1,
						addRemoveLinks: true,
						dictRemoveFile: "Remove",
						maxfilesexceeded: function (file) {
							this.removeAllFiles();
							this.addFile(file);
						},
						thumbnailWidth: 200,
						thumbnailHeight: null,
						init: function () {
							manageVendors.DATA.currentLogoFile = null;
							this.on("addedfile", function (file) {
								if (manageVendors.DATA.currentLogoFile) {
									this.removeFile(manageVendors.DATA.currentLogoFile);
								}
								manageVendors.DATA.currentLogoFile = file;
							});
							this.on("success", function (file, response) {
								$("#hdLogo").val(response);
								$.smValidate.hideValidation($("#hdLogo").closest(".form-group"));
								manageVendors.DATA.currentLogoFile = file;
							});
							this.on("removedfile", function (file) {
								manageVendors.DATA.currentLogoFile = null;
								if (file.xhr) {
									$.ajax({
										url: "/sm/smhandler.aspx",
										async: true,
										cache: false,
										type: 'POST',
										dataType: 'html',
										data: { command: 'deletelogo', logourl: file.xhr.responseText },
										success: function(responseText) {
											$("#hdLogo").val("");
										}
									});
								} else {
									$("#hdAvatar").val("");
								}
								
							});
						}
					});
					$("#addvendor_dialog_0 .input-group.date").each(function () {
						$(this).datepicker({
							clearBtn: true,
							autoclose: true,
							todayHighlight: true
						});
					});
					$("#addvendor_dialog_0 input:checkbox", "#addvendor_dialog_0 .vendor-type-lst").on("change", checkSelectedType);
					$("#addvendor_dialog_0 input[data-role=tagsinput]").tagsinput({
						trimValue: true,
						typeahead: {
							ajax: {
								url: '/sm/smhandler.aspx',
								method: 'POST',
								displayField: "name",
								valueField: "id",
								dataType: 'json',
								preDispatch: function () {
									return { query: "", command: "getAvailableTags", lenderID: '<%=LenderID%>' };
								},
								preProcess: function (data) {
									return data;
								}
							}
						}
					});

				}, function () {
					//reload grid when dialog closed
					var pageIndex = manageVendors.DATA.grid.getPaginationSetting().page;
					manageVendors.DATA.grid.setFilter(manageVendors.FACTORY.getFilterInfo()).setPageIndex(pageIndex).load();
				});
			};
			
			manageVendors.FACTORY.saveNewLenderVendor = function (obj) {
				if ($("#hdLogo").val() == "") {
					$.smValidate.showValidation($("#hdLogo").closest(".form-group"), "Please upload logo");
				} else {
					$.smValidate.hideValidation($("#hdLogo").closest(".form-group"));
				}
				var isValidType = checkSelectedType();
				if ($.smValidate("addvendor_dialog_0") == false || $("#hdLogo").val() == "" || isValidType == false) return;
				var vendorData = obj.container.find("form").serializeJSON({ parseAll: true });
				//update data
				var postData = {
					command: "saveNewLenderVendor",
					vendor_data: _COMMON.toJSON(vendorData),
					lenderID: '<%=LenderID%>',
					bid: '<%=BackOfficeLenderID%>'
				};
				$.ajax({
					url: "/sm/smhandler.aspx",
					async: true,
					cache: false,
					type: 'POST',
					dataType: 'html',
					data: postData,
					success: function (responseText) {
						var response = $.parseJSON(responseText);
						if (response.IsSuccess) {
							_COMMON.noty("success", "Success", 500);
							obj.dialog.close();
						} else {
							var errorMsg = response.Message !== "" ? response.Message : "Error";
							_COMMON.noty("error", errorMsg, 500);
						}
					}
				});
			};
			manageVendors.FACTORY.search = function () {
				manageVendors.DATA.grid.setFilter(manageVendors.FACTORY.getFilterInfo()).setPageIndex(1).load();
			};
			
		}(window.manageVendors = window.manageVendors || {}, jQuery));
		<%End If%>
		function registerDataValidator(container, groupName) {
			var $container = $(container);
			$.smValidate.removeValidationGroup(groupName);
			$("[data-required='true'],[data-format]", $container).each(function (idx, ele) {
				$(ele).observer({
					validators: [
						function (partial) {
							var $self = $(this);
							if ($self.data("required") && $.trim($self.val()) === "") {
								if ($self.data("message-require")) {
									return $self.data("message-require");
								} else {
									return "This field is required";
								}
							}
							var format = $self.data("format");
							if (format) {
								var msg = "Invalid format";
								if ($self.data("message-invalid-data")) {
									msg = $self.data("message-invalid-data");
								}
								if (format === "email") {
									if (_COMMON.ValidateEmail($self.val()) === false) {
										return msg;
									}
								} else if (format === "phone") {
									if (_COMMON.ValidatePhone($self.val()) === false) {
										return msg;
									}
								}else if ($self.val() != "" && format === "positive-number") {
									if ((parseInt($self.val()) > 0) == false) {
										return msg;
									}
								}
							}
							return "";
						}
					],
					validateOnBlur: true,
					group: groupName
				});
			});
		}
	</script>
</asp:Content>
<asp:Content runat="server" ContentPlaceHolderID="plActionButtons">
	<%--<div class="top-action-button save-changes-btn"><span>Save Changes</span></div>--%>
</asp:Content>