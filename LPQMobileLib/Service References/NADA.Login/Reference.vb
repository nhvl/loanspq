﻿'------------------------------------------------------------------------------
' <auto-generated>
'     This code was generated by a tool.
'     Runtime Version:4.0.30319.42000
'
'     Changes to this file may cause incorrect behavior and will be lost if
'     the code is regenerated.
' </auto-generated>
'------------------------------------------------------------------------------

Option Strict On
Option Explicit On

Imports System
Imports System.Runtime.Serialization

Namespace NADA.Login
    
    <System.Diagnostics.DebuggerStepThroughAttribute(),  _
     System.CodeDom.Compiler.GeneratedCodeAttribute("System.Runtime.Serialization", "4.0.0.0"),  _
     System.Runtime.Serialization.DataContractAttribute(Name:="GetTokenRequest", [Namespace]:="http://webservice.nada.com/"),  _
     System.SerializableAttribute()>  _
    Partial Public Class GetTokenRequest
        Inherits Object
        Implements System.Runtime.Serialization.IExtensibleDataObject, System.ComponentModel.INotifyPropertyChanged
        
        <System.NonSerializedAttribute()>  _
        Private extensionDataField As System.Runtime.Serialization.ExtensionDataObject
        
        <System.Runtime.Serialization.OptionalFieldAttribute()>  _
        Private UsernameField As String
        
        <System.Runtime.Serialization.OptionalFieldAttribute()>  _
        Private PasswordField As String
        
        <Global.System.ComponentModel.BrowsableAttribute(false)>  _
        Public Property ExtensionData() As System.Runtime.Serialization.ExtensionDataObject Implements System.Runtime.Serialization.IExtensibleDataObject.ExtensionData
            Get
                Return Me.extensionDataField
            End Get
            Set
                Me.extensionDataField = value
            End Set
        End Property
        
        <System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue:=false)>  _
        Public Property Username() As String
            Get
                Return Me.UsernameField
            End Get
            Set
                If (Object.ReferenceEquals(Me.UsernameField, value) <> true) Then
                    Me.UsernameField = value
                    Me.RaisePropertyChanged("Username")
                End If
            End Set
        End Property
        
        <System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue:=false, Order:=1)>  _
        Public Property Password() As String
            Get
                Return Me.PasswordField
            End Get
            Set
                If (Object.ReferenceEquals(Me.PasswordField, value) <> true) Then
                    Me.PasswordField = value
                    Me.RaisePropertyChanged("Password")
                End If
            End Set
        End Property
        
        Public Event PropertyChanged As System.ComponentModel.PropertyChangedEventHandler Implements System.ComponentModel.INotifyPropertyChanged.PropertyChanged
        
        Protected Sub RaisePropertyChanged(ByVal propertyName As String)
            Dim propertyChanged As System.ComponentModel.PropertyChangedEventHandler = Me.PropertyChangedEvent
            If (Not (propertyChanged) Is Nothing) Then
                propertyChanged(Me, New System.ComponentModel.PropertyChangedEventArgs(propertyName))
            End If
        End Sub
    End Class
    
    <System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0"),  _
     System.ServiceModel.ServiceContractAttribute([Namespace]:="http://webservice.nada.com/", ConfigurationName:="NADA.Login.SecureLoginSoap")>  _
    Public Interface SecureLoginSoap
        
        'CODEGEN: Generating message contract since element name tokenRequest from namespace http://webservice.nada.com/ is not marked nillable
        <System.ServiceModel.OperationContractAttribute(Action:="http://webservice.nada.com/getToken", ReplyAction:="*")>  _
        Function getToken(ByVal request As NADA.Login.getTokenRequest1) As NADA.Login.getTokenResponse
    End Interface
    
    <System.Diagnostics.DebuggerStepThroughAttribute(),  _
     System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0"),  _
     System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced),  _
     System.ServiceModel.MessageContractAttribute(IsWrapped:=false)>  _
    Partial Public Class getTokenRequest1
        
        <System.ServiceModel.MessageBodyMemberAttribute(Name:="getToken", [Namespace]:="http://webservice.nada.com/", Order:=0)>  _
        Public Body As NADA.Login.getTokenRequest1Body
        
        Public Sub New()
            MyBase.New
        End Sub
        
        Public Sub New(ByVal Body As NADA.Login.getTokenRequest1Body)
            MyBase.New
            Me.Body = Body
        End Sub
    End Class
    
    <System.Diagnostics.DebuggerStepThroughAttribute(),  _
     System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0"),  _
     System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced),  _
     System.Runtime.Serialization.DataContractAttribute([Namespace]:="http://webservice.nada.com/")>  _
    Partial Public Class getTokenRequest1Body
        
        <System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue:=false, Order:=0)>  _
        Public tokenRequest As NADA.Login.GetTokenRequest
        
        Public Sub New()
            MyBase.New
        End Sub
        
        Public Sub New(ByVal tokenRequest As NADA.Login.GetTokenRequest)
            MyBase.New
            Me.tokenRequest = tokenRequest
        End Sub
    End Class
    
    <System.Diagnostics.DebuggerStepThroughAttribute(),  _
     System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0"),  _
     System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced),  _
     System.ServiceModel.MessageContractAttribute(IsWrapped:=false)>  _
    Partial Public Class getTokenResponse
        
        <System.ServiceModel.MessageBodyMemberAttribute(Name:="getTokenResponse", [Namespace]:="http://webservice.nada.com/", Order:=0)>  _
        Public Body As NADA.Login.getTokenResponseBody
        
        Public Sub New()
            MyBase.New
        End Sub
        
        Public Sub New(ByVal Body As NADA.Login.getTokenResponseBody)
            MyBase.New
            Me.Body = Body
        End Sub
    End Class
    
    <System.Diagnostics.DebuggerStepThroughAttribute(),  _
     System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0"),  _
     System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced),  _
     System.Runtime.Serialization.DataContractAttribute([Namespace]:="http://webservice.nada.com/")>  _
    Partial Public Class getTokenResponseBody
        
        <System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue:=false, Order:=0)>  _
        Public getTokenResult As String
        
        Public Sub New()
            MyBase.New
        End Sub
        
        Public Sub New(ByVal getTokenResult As String)
            MyBase.New
            Me.getTokenResult = getTokenResult
        End Sub
    End Class
    
    <System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")>  _
    Public Interface SecureLoginSoapChannel
        Inherits NADA.Login.SecureLoginSoap, System.ServiceModel.IClientChannel
    End Interface
    
    <System.Diagnostics.DebuggerStepThroughAttribute(),  _
     System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")>  _
    Partial Public Class SecureLoginSoapClient
        Inherits System.ServiceModel.ClientBase(Of NADA.Login.SecureLoginSoap)
        Implements NADA.Login.SecureLoginSoap
        
        Public Sub New()
            MyBase.New
        End Sub
        
        Public Sub New(ByVal endpointConfigurationName As String)
            MyBase.New(endpointConfigurationName)
        End Sub
        
        Public Sub New(ByVal endpointConfigurationName As String, ByVal remoteAddress As String)
            MyBase.New(endpointConfigurationName, remoteAddress)
        End Sub
        
        Public Sub New(ByVal endpointConfigurationName As String, ByVal remoteAddress As System.ServiceModel.EndpointAddress)
            MyBase.New(endpointConfigurationName, remoteAddress)
        End Sub
        
        Public Sub New(ByVal binding As System.ServiceModel.Channels.Binding, ByVal remoteAddress As System.ServiceModel.EndpointAddress)
            MyBase.New(binding, remoteAddress)
        End Sub
        
        <System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)>  _
        Function NADA_Login_SecureLoginSoap_getToken(ByVal request As NADA.Login.getTokenRequest1) As NADA.Login.getTokenResponse Implements NADA.Login.SecureLoginSoap.getToken
            Return MyBase.Channel.getToken(request)
        End Function
        
        Public Function getToken(ByVal tokenRequest As NADA.Login.GetTokenRequest) As String
            Dim inValue As NADA.Login.getTokenRequest1 = New NADA.Login.getTokenRequest1()
            inValue.Body = New NADA.Login.getTokenRequest1Body()
            inValue.Body.tokenRequest = tokenRequest
            Dim retVal As NADA.Login.getTokenResponse = CType(Me,NADA.Login.SecureLoginSoap).getToken(inValue)
            Return retVal.Body.getTokenResult
        End Function
    End Class
End Namespace
