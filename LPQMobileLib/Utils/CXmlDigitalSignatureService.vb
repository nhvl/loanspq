﻿Imports log4net
Imports System.Configuration.ConfigurationManager
Imports System.Security.Cryptography
Imports System.Security.Cryptography.X509Certificates
Imports System.Security.Cryptography.Xml
Imports System.Xml

''' <summary>
''' This is inherited from LPQ because the 2 system need to sync
''' This service is used to apply and verify XML digital signatures when sending/receiving requests to internal LoansPQ
''' web services.  We created this service because we wanted a way to make sure that web services that are only used
''' internally (e.g. WebMSRelay) can't be called by anyone but our own web servers.
''' 
''' This service only works when both the sender and receiver are using the same digital certificate to apply and verify
''' the digital signature.  Keep this in mind if you use it on a web service that needs to be called from other LoansPQ
''' servers that may, or may not, have the same digital certificates.
''' </summary>
Public Class CXmlDigitalSignatureService
    Private log As ILog = LogManager.GetLogger(Me.GetType())

    Public Sub AttachSignature(ByRef pRequest As System.Net.HttpWebRequest, pXMLString As String, pCert As X509Certificate2)

        Dim sig = GenerateDigitalSignature(pXMLString, pCert)
        pRequest.Headers.Add("LPQ_DIGITAL_SIGNATURE_ID", pCert.Thumbprint)
        pRequest.Headers.Add("LPQ_DIGITAL_SIGNATURE_VERSION", "1")
        pRequest.Headers.Add("LPQ_DIGITAL_SIGNATURE", sig)
    End Sub

    ''' <summary>
    ''' Generate a digital signature
    ''' </summary>
    ''' <param name="pText">Text to generate digital signature from</param>
    ''' <param name="pCertificate">Digital certificate</param>
    ''' <returns>Digital signature</returns>
    Public Function GenerateDigitalSignature(ByVal pText As String, ByVal pCertificate As X509Certificate2) As String
        If pCertificate Is Nothing Then
            Throw New Exception("Could not load certificate to sign request with.")
        End If

        If pCertificate.PrivateKey Is Nothing Then
            Throw New Exception("Unable to load certificate's private key. Make sure permissions are set up correctly.")
        End If

        log.Info("Generating digital signature for: " & pText)

        ' Wrap the text in an XmlDocument
        Dim utcNow As String = DateTime.UtcNow.ToString("u")

        Dim requestDoc As New XmlDocument()
        requestDoc.LoadXml("<LPQ_SIGNED_REQUEST id=""lpq_signed_request"" timestamp=""" & utcNow & """ />")

        requestDoc.DocumentElement.InnerText = pText

        Dim signedXml As New SignedXml(requestDoc)
        signedXml.SigningKey = pCertificate.PrivateKey

        Dim keyInfo As New KeyInfo()
        Dim keyInfoData As New KeyInfoX509Data()
        keyInfoData.AddIssuerSerial(pCertificate.Issuer, pCertificate.GetSerialNumberString())
        keyInfo.AddClause(keyInfoData)

        signedXml.KeyInfo = keyInfo
        signedXml.SignedInfo.CanonicalizationMethod = SignedXml.XmlDsigExcC14NTransformUrl

        Dim reference As New Reference()
        reference.Uri = "#lpq_signed_request"
        reference.AddTransform(New XmlDsigExcC14NTransform())

        signedXml.AddReference(reference)
        signedXml.ComputeSignature()

        Dim signature As String = signedXml.GetXml().OuterXml

        ' Prepend a timestamp to prevent replay attacks
        signature = utcNow & "|" & signature

        log.Info("Digital signature generated for request: " & requestDoc.OuterXml & ". Signature: " & signature)

        Return signature
    End Function

    ''' <summary>
    ''' Verifies that the signature matches the text
    ''' </summary>
    ''' <param name="pText">Text that was signed</param>
    ''' <param name="pSignature">Digital signature</param>
    ''' <param name="pCertificate">Certificate that was used to generate the digital signature</param>
    Public Function VerifyDigitalSignature(ByVal pText As String, ByVal pSignature As String, ByVal pCertificate As X509Certificate2, pVersion As Integer) As Boolean



        If pVersion = 0 Then
            ' If digital signatures are disabled on this server, don't bother verifying
            If AppSettings("DISABLE_LPQ_DIGITAL_SIGNATURE_CHECK") = "Y" Then
                Return True
            End If

            Try
                ' Decrypt signature
                pSignature = Decrypt("LpqDigSigKey", pSignature)
            Catch ex As Exception
                log.Warn("Error while decrypting digital signature: " & ex.Message, ex)
                Return False
            End Try
        End If


        If pSignature.IndexOf("|") = -1 Then
            log.Warn("Unable to find pipe character (|) in signature")
            Return False
        End If

        ' Check if the request was sent within a certain threshold (prevents replay attacks)
        Dim timestamp As String = Left(pSignature, pSignature.IndexOf("|"))

        ' If the time has been over 5 minutes, reject this submission
        Dim replayAttackThresholdSeconds As Integer = CType(AppSettings("LPQ_DIGITAL_SIGNATURE_REPLAY_ATTACK_THRESHOLD_SECONDS"), Integer)

        If DateTime.UtcNow.Subtract(DateTime.ParseExact(timestamp, "u", Nothing)).Seconds > replayAttackThresholdSeconds Then
            log.Warn("Timestamp of request: " & timestamp & " is more than " & replayAttackThresholdSeconds &
                " seconds from now: " & DateTime.UtcNow.ToString("u") & ". Rejecting digital signature.")
            Return False
        End If

        ' Strip timestamp out of signature
        pSignature = Right(pSignature, pSignature.Length - pSignature.IndexOf("|") - 1)

        Dim requestDoc As New XmlDocument()
        requestDoc.LoadXml("<Root><LPQ_SIGNED_REQUEST id=""lpq_signed_request"" timestamp=""" & timestamp & """ />" & pSignature & "</Root>")

        Dim lpqSignedRequest As XmlElement = DirectCast(requestDoc.DocumentElement.SelectSingleNode("LPQ_SIGNED_REQUEST"), XmlElement)
        lpqSignedRequest.InnerText = pText

        log.Info("Verifying digital signature for: " & lpqSignedRequest.OuterXml & ". Signature: " & pSignature)

        Dim signedXml As New SignedXml(requestDoc)

        Dim nodeList As XmlNodeList = requestDoc.GetElementsByTagName("Signature")
        signedXml.LoadXml(DirectCast(nodeList(0), XmlElement))

        Dim isValid As Boolean = signedXml.CheckSignature(pCertificate, True)

        log.Info("Digital signature was " & IIf(isValid, "", "NOT ").ToString() & "valid")

        Return isValid
    End Function

    ''' <summary>
    ''' Helper function for retrieving a digital certificate out by "Friendly Name"
    ''' </summary>
    Public Function GetCertificateByFriendlyName(ByVal pFriendlyName As String, ByVal pStoreName As StoreName, ByVal pStoreLocation As StoreLocation) As X509Certificate2
        Dim cert As X509Certificate2 = Nothing

        Dim store As New X509Store(pStoreName, pStoreLocation)
        store.Open(OpenFlags.ReadOnly Or OpenFlags.OpenExistingOnly)

        For Each existingCert As X509Certificate2 In store.Certificates
            If existingCert.FriendlyName = pFriendlyName Then
                cert = existingCert
                Exit For
            End If
        Next

        Return cert
    End Function

    Private Function Encrypt(ByVal key As String, ByVal s As String) As String
        Dim ret As String = ""
        Dim nLen As Integer = Len(s)
        Dim i As Integer, j As Integer = 0

        'pad the key so its at least same length as what we're trying to hash
        If Len(s) > Len(key) Then
            key = key.PadRight(Len(s), "*"c)
        End If

        For i = nLen To 1 Step -1
            j += 1

            Dim k As Integer = Asc(key.Chars(j - 1)) + 129

            ret += Hex(Asc(Mid(s, i, 1)) Xor (k))
            'traceA(Mid(s, i, 1) & " ++ " & k)
        Next

        Return ret
    End Function

    Private Function Decrypt(ByVal key As String, ByVal s As String) As String
        Dim ret As String = ""
        Dim nLen As Integer = Len(s)
        Dim i As Integer, j As Integer = 0

        'pad the key so its at least same length as what we're trying to hash
        If Len(s) > Len(key) Then
            key = key.PadRight(Len(s), "*"c)
        End If

        For i = (nLen - 1) To 1 Step -2
            j += 1

            Dim k As Integer = Asc(key.Chars(CType(nLen / 2 - j, Integer))) + 129

            ret += Chr(CInt("&H" & Mid(s, i, 2)) Xor CInt(k))
            'traceA(Mid(s, i, 2) & " ** " & k)
        Next

        Return ret
    End Function
End Class
